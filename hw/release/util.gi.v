// test_lib.v Frank Berndt
// test library functions;

// delay task;
// granularity is 10ns, plus one clk/pci_clk for edge sync;

task delay;
	input [31:0] ticks;			// in gi clk;
	begin
		repeat(ticks) @(posedge clk);	// basic delay;
		@(posedge `xclk);				// edge sync;
	end
endtask

// space something by a number of system clks;
// either gi or pci clock;
// limit to 7 clocks;

task space8;
	input [2:0] nclks;
	begin
		repeat(nclks) @(posedge `xclk);
	end
endtask

// align address to cacheline boundary;

task baddr;
	inout [31:0] addr;
	input [3:0] bsize;
	begin
		addr = addr & ~((bsize - 1) << 2);
	end
endtask

// get a random sub-block order address;
// sequential for PCI, else sub-block;

task sbo_addr;
	inout [31:0] addr;
	input [3:0] bsize;
	reg [4:0] mask;
	begin
		mask = (bsize - 1) << 2;
		addr = addr & ~mask;
`ifdef	GI_PCI
`else	// GI_PCI
		addr = addr | ($random & mask);
`endif	// GI_PCI
	end
endtask

// get next word pattern;
// uses simple crc16 method;

function [31:0] next_pat;
	input [31:0] pat;
	reg bit;
	begin
		bit = pat[15] ^ pat[0] ^ 1;
		next_pat = { bit, pat[31:1] };
	end
endfunction

// check expected single read data;

task sexp;
	input [31:0] exp;
	input [31:0] got;
	input [31:0] mask;
	begin
		if(mask === 32'h0)
			$display("ERROR: %t: %M: mask=%b", $time, mask);
		if((exp & mask) !== (got & mask)) begin
			$display("ERROR: %t: %M: mask 0x%h mismatch 0x%h/%b exp 0x%h/%b",
				$time, mask, got, got, exp, exp);
		end
	end
endtask

// fill block with pattern;

task bpat;
	inout [31:0] pat;		// start pattern;
	input [3:0] bsize;		// size in words;
	integer n;
	begin
		for(n = 0; n < `BSIZE_MAX; n = n + 1) begin
			`blk[n] = (n < bsize)? pat : 32'bx;
			pat = next_pat(pat);
		end
	end
endtask

// check pattern in block;
// consider sub-block order;

task bexp;
	input [31:0] addr;		// address;
	input [3:0] bsize;		// size in words;
	inout [31:0] pat;		// start pattern;
	input [4:2] sbo;		// sub-block order;
	integer n;
	reg [3:0] off;			// block offset;
	reg [31:0] got;
	reg [31:0] exp [0:`BSIZE_MAX-1];	// expected data;
	begin
		for(n = 0; n < bsize; n = n + 1) begin
			exp[n] = pat;
			pat = next_pat(pat);
		end
		baddr(addr, bsize);
		sbo = sbo & (bsize - 1);
		for(n = 0; n < bsize; n = n + 1) begin
			off = n ^ sbo;
			got = `blk[off];
			if(got !== exp[n]) begin
				$display("ERROR: %t: %M: addr 0x%h blk[%0d] mismatch 0x%h/%b exp 0x%h/%b",
					$time, addr + (off << 2), off, got, got, exp[n], exp[n]);
			end
		end
	end
endtask

// test a register;
// regs are assumed to be up to 32 bits in width;
// can only test read/write bits that do not have side effect;

task test_reg;
	input [31:0] addr;		// address;
	input [31:0] mask;		// bit mask;
	input [31:0] set;		// bits that must be 1;
	integer b;
	reg [31:0] r;			// read data;
	begin
		$display("%M: addr 0x%h mask 0x%h", addr, mask);
		for(b = 1; b != 0; b = b << 1) begin
			if(b & mask) begin
				`swrite(addr, b | set);
				`sread(addr, r);
				sexp(b, r, mask);
			end
		end
	end
endtask

// check memory with single accesses;
// writes are done first, followed by read checks;
// sparse check that exercises all address bits;
// does not use memory backdoor for running on cpu;

task check_mem_sgl;
	input [31:0] space;		// address space;
	input [31:0] size;		// size of space;
	input reqspc;			// random request spacing;
	input wa;				// address pattern;
	reg [31:0] addr;		// address;
	reg [31:0] past;		// end of space;
	integer n, i, woff;
	reg [31:0] r;			// rom read data;
	reg [31:0] pat [31:0];	// data patterns;
	begin
		// do all writes first;

		$display("%t: %M: write, reqspc=%b, wa=%b", $time, reqspc, wa);
		past = space + size;
		woff = wa << 2;
		addr = space;
		i = 0;
		for(n = 4; addr < past; n = woff | (n << 1)) begin
			if(reqspc)
				space8($random);
			pat[i] = $random;
			`swrite(addr, pat[i]);
			addr = space + n;
			i = i + 1;
		end

		// then check with reads;

		$display("%t: %M: read, reqspc=%b, wa=%b", $time, reqspc, wa);
		addr = space;
		i = 0;
		for(n = 4; addr < past; n = woff | (n << 1)) begin
			if(reqspc)
				space8($random);
			`sread(addr, r);
			sexp(pat[i], r, 32'hffff_ffff);
			addr = space + n;
			i = i + 1;
		end
	end
endtask

// check memory with block accesses;
// writes are done first, followed by read checks;
// sparse check that exercises all address bits;
// uses random sub-block order;
// does not use memory backdoor for running on cpu;

task check_mem_blk;
	input [31:0] space;		// address space;
	input [31:0] size;		// size of space;
	input [3:0] bsize;		// block size;
	input reqspc;			// random request spacing;
	reg [31:0] addr;		// address;
	reg [31:0] past;		// end of space;
	reg [31:0] pat [31:0];	// data patterns;
	reg [31:0] npat;		// new pattern;
	integer n, i;
	begin
		// do all writes first, in sequential order;

		$display("%t: %M: write, bsize=%0d, reqspc=%b", $time, bsize, reqspc);
		past = space + size;
		addr = space;
		i = 0;
		for(n = 32; addr < past; n = n | (n << 1)) begin
			if(reqspc)
				space8($random);
			baddr(addr, bsize);
			npat = $random;
			pat[i] = npat;
			bpat(npat, bsize);
			`bwrite(addr, bsize);
			addr = space + n;
			i = i + 1;
		end

		// read and check, in random sub-block order;

		$display("%t: %M: read, bsize=%0d, reqspc=%b", $time, bsize, reqspc);
		addr = space;
		i = 0;
		for(n = 32; addr < past; n = n | (n << 1)) begin
			if(reqspc)
				space8($random);
			sbo_addr(addr, bsize);
			`bread(addr, bsize);
			npat = pat[i];
			bexp(addr, bsize, npat, addr[3:2]);
			addr = space + n;
			i = i + 1;
		end
	end
endtask

// check state of secure exception;

task check_sec_excp;
	input exp;			// expected value;
	begin
		//XXX
	end
endtask

// check state of secure bit;

task check_sec_bit;
	input exp;			// expected value;
	begin
		if(`sec_bit !== exp)
			$display("ERROR: %t: %M: secure mode %b exp %b", $time, `sec_bit, exp);
	end
endtask

// enter secure mode;
// fetch from secure exception vector;

task sec_mode_enter;
	input [31:0] evec;	// exception vector;
	input sec_excp;		// expected state of secure exception bit;
	reg [31:0] r;		// read data;
	begin
		// the exception signal should be as expected;
		// however, secure mode is not on until after the fetch;
		// read must return valid word for exception handler;

		$display("%t: %M", $time);
		check_sec_excp(sec_excp);
		check_sec_bit(0);
		`sread(evec, r);
		sexp(`rom_get(evec[12:2]), r, 32'hffff_ffff);
		check_sec_excp(sec_excp);
		check_sec_bit(1);
	end
endtask

// fill dma buffer with pattern;
// use cpu instead of backdoor for portability;
// based on cacheline size;
// rotate left seed pattern for each cacheline;

task dma_bpat;
	input [31:0] addr;	// address, aligned to cacheline;
	input [15:0] size;	// # of buffers;
	input [31:0] pat;	// pattern seed;
	integer n;
	begin
		baddr(addr, `BSIZE_MAX);
		for(n = size * 32 / `BSIZE_MAX; n > 0; n = n - 1) begin
			bpat(pat, `BSIZE_MAX);
			`bwrite(addr, `BSIZE_MAX);
			addr = addr + (`BSIZE_MAX << 2);
		end
	end
endtask

// check expected pattern in sram;
// use cpu to check instead of backdoor;
// based on cacheline size;
// rotate left seed pattern for each cacheline;

task dma_bexp;
	input [31:0] addr;	// address, aligned to cacheline;
	input [15:0] size;	// # of buffers;
	input [31:0] pat;	// pattern seed;
	integer n;
	begin
		baddr(addr, `BSIZE_MAX);
		for(n = size * 32 / `BSIZE_MAX; n > 0; n = n - 1) begin
			`bread(addr, `BSIZE_MAX);
			bexp(addr, `BSIZE_MAX, pat, 0);
			addr = addr + (`BSIZE_MAX << 2);
		end
	end
endtask

// setup pci host for gi dma;

task sys_dma_setup;
	input enable;
	begin
`ifdef	PCI_HOST
		if(enable)
			sim.sys.setup(`REQ_GI, `REQ_TARGET_CMDS, 16'b1101_0000_1100_0000);
		sim.sys.setup(`REQ_GI, `REQ_BE_TARGET, enable);
`endif	// PCI_HOST
	end
endtask

// setup system bus monitor for gi dma;

task sys_mon_setup;
	input enable;
	begin
`ifdef	GI_PCI
		`gi_sim_top.pci_mon.cmd_mask(`REQ_GI, enable? 16'b0000_0000_1100_0000 : 16'd0);
`endif	// GI_PCI
	end
endtask

// test_bd.v v1 Frank Berndt
// collection of backdoors;
// here, because we don't want to modify vendor models;

// virage backdoor;

task v_put;
	input [6:0] addr;
	input [31:0] data;
	begin
		`gi_top.v.novea.uut.earom[addr] = data;
	end
endtask

function [31:0] v_get;
	input [6:0] addr;
	begin
		v_get = `gi_top.v.novea.uut.earom[addr];
	end
endfunction

// set initial virage content;
// written to earom area, need recall to xfer to sram;

reg [31:0] v_pat;			// initial virage pattern;

task v_init;
	input [31:0] pat;		// initial pattern;
	integer n;
	begin
		$display("%M: pat=0x%h", pat);
		v_pat = pat;
		for(n = 0; n < `GI_V_SIZE; n = n + 1) begin
			`v_put(n, pat);
			pat = next_pat(pat);
		end
	end
endtask

// test_gi.v Frank Berndt;
// GI module tests;

// host commands;

`define	HDIR_OUT	0		// host out direction;
`define	HDIR_IN		1		// host in direction;

`define	BRK_ACK		0		// ack break;
`define	BRK_TERM	1		// terminate break;

// test gi registers after reset;

task gi_regs_after_reset;
	reg [31:0] r;		// read data;
	reg [31:0] mask;	// bit mask;
	begin
		// BI secure mode registers;

		$display("%M: SEC_MODE");
		`sread(`SEC_MODE, r);
		sexp(`SEC_MODE_RESET | `SEC_MODE_ON, r, 32'hffff_ffdf); //XXX check test_ena;
		$display("%M: SEC_TIMER");
		`sread(`SEC_TIMER, r);
		sexp(0, r, 32'hffff_ffff);
		$display("%M: SEC_SRAM");
		`sread(`SEC_SRAM, r);
		sexp(0, r, 32'hffff_ffff);

		// virage registers;

		$display("%M: V_CRSTO_0");
		`sread(`V_CRSTO_0, r);
		sexp(`V_CRSTO_0_DEF, r, 32'hffff_ffff);
		$display("%M: V_CRSTO_1");
		`sread(`V_CRSTO_1, r);
		sexp(`V_CRSTO_1_DEF, r, 32'hffff_ffff);
		$display("%M: V_CRM_0");
		`sread(`V_CRM_0, r);
		sexp(`V_CRM_0_DEF, r, 32'hffff_ffff);
		$display("%M: V_CRM_1");
		`sread(`V_CRM_1, r);
		sexp(`V_CRM_1_DEF, r, 32'hffff_ffff);
		$display("%M: V_CRM_2");
		`sread(`V_CRM_2, r);
		sexp(`V_CRM_2_DEF, r, 32'hffff_ffff);
		$display("%M: V_CRM_3");
		`sread(`V_CRM_3, r);
		sexp(`V_CRM_3_DEF, r, 32'hffff_ffff);
		$display("%M: V_REG_6");
		`sread(`V_REG_6, r);
		sexp(`V_REG_6_DEF, r, 32'hffff_ffff);
		$display("%M: V_REG_7");
		`sread(`V_REG_7, r);
		sexp(`V_REG_7_DEF, r, 32'hffff_ffff);

		$display("%M: V_CMD");
		`sread(`V_CMD, r);
		sexp({ 16'd0, 16'h8062 }, r, 32'hffff_f8ff);

		// BI dma registers;

		$display("%M: BI_CTRL");
		`sread(`BI_CTRL, r);
		mask = `BI_CTRL_SIZE | `BI_CTRL_WRITE | `BI_CTRL_PTR | `BI_CTRL_ERRS;
		sexp(0, r, ~mask);
		$display("%M: BI_MEM");
		`sread(`BI_MEM, r);
		mask = `BI_MEM_PTR;
		sexp(0, r, ~mask);
		$display("%M: BI_INTR");
		`sread(`BI_INTR, r);
		sexp(0, r, 32'hffff_ffff);

		// DI registers;

		$display("%M: DI_CONF");
		`sread(`DI_CONF, r);
		sexp(32'ha800_0070, r, 32'hffff_ffff);
		$display("%M: DI_CTRL");
		`sread(`DI_CTRL, r);
		mask = `DI_CTRL_COVER;
		sexp(`DI_CTRL_RST_INTR | `DI_CTRL_CMD_IN, r, ~mask);
		$display("%M: DI_BE");
		`sread(`DI_BE, r);
		mask = `DI_BE_SIZE | `DI_BE_WRITE | `DI_BE_PTR;
		sexp(0, r, ~mask);

		// AIS registers;
		// EXEC & INTR must be 0;

		$display("%M: AIS_CTRL");
		`sread(`AIS_CTRL, r);
		mask = `AIS_CTRL_BUF_OFF | `AIS_CTRL_ERRS;
		sexp(32'h0, r, ~mask);
		mask = `AIS_PTR_ADDR | `AIS_PTR_EMPTY;
		$display("%M: AIS_PTR0");
		`sread(`AIS_PTR0, r);
		sexp(32'h0, r, ~mask);
		$display("%M: AIS_PTR1");
		`sread(`AIS_PTR1, r);
		sexp(32'h0, r, ~mask);

		// AES regs; XXX
		// SHA1 regs; XXX
		// GC regs; XXX

	end
endtask

// test bi registers;
// must pass for secure and non-secure mode, since
// all register bits are accessible in both modes;
// do not run when bi dma is busy;
// dma operations are checked in another test;

task gi_test_bi_regs;
	reg [31:0] r;			// read data;
	begin
		`sread(`BI_CTRL, r);
		if(r[31] !== 1'b0)
			$display("ERROR: %t: %M: bi busy, BI_CTRL=0x%h", $time, r);
		r = `BI_CTRL_SIZE | `BI_CTRL_WRITE | `BI_CTRL_PTR;
		test_reg(`BI_CTRL, r, 0);
		test_reg(`BI_MEM, `BI_MEM_PTR, 0);
	end
endtask

// issue bi dma;
// it is assumed that bi is not busy;

task gi_bi_dma;
	input [31:0] mem;		// memory address;
	input [16:0] off;		// buffer offset;
	input [9:0] size;		// size in blocks;
	input wr;				// dma to memory;
	input intr;				// expect interrupt;
	input [2:0] err;		// expected err state;
	input [31:0] to;		// timeout in usec;
	input kill;				// kill after start;
	reg [31:0] ctrl;		// control register;
	reg [31:0] r;			// read data;
	reg [31:0] mask;		// read check mask;
	begin
		// write memory address;
		// write unused bits with Xs;
		// start dma;

		$display("%t: %M: mem=0x%h off=0x%h blks=%0d wr=%b intr=%b to=%0dus",
			$time, mem, off, size + 1, wr, intr, to);
		`swrite(`BI_MEM, mem);
		ctrl = 32'bx;
		ctrl[31] = 1;
		ctrl[30] = intr;
		ctrl[29:20] = size;
		ctrl[19] = wr;
		ctrl[16:4] = off[16:4];
		`swrite(`BI_CTRL, ctrl);

		// read and check that dma is busy;
		// dma could be done before read of BI_CTRL;
		// if dma is already done, then BI_SIZE should be all 1s;

		`sread(`BI_CTRL, r);
		mask = `BI_CTRL_EXEC | `BI_CTRL_SIZE;
		if((r & mask) !== 32'h3ff0_0000) begin
			mask = `BI_CTRL_EXEC | `BI_CTRL_WRITE;
			if((r & mask) != (ctrl & mask)) begin
				$display("ERROR: %t: %M: BI_CTRL=0x%h/%b exp 0x%h/%b",
					$time, r, r, ctrl, ctrl);
			end
		end

		// check kill operation;
		// bi must become unbusy right away;
		// interrupt should not have been issued;

		if(kill) begin
			ctrl[31] = 0;
			`swrite(`BI_CTRL, ctrl);
			`sread(`BI_CTRL, r);
			intr = 0;
		end

		// wait until dma is finished;
		// a 128-byte dma across pci takes ~1usec;

		while((r[31] === 1'b1) && (to > 0)) begin
			delay(100);
			`sread(`BI_CTRL, r);
			to = to - 1;
		end

		// post DMA checks;
		// dma must have finished with correct intr flag;
		// clear pending intr, and kill still going dma;

		if(r[31] !== 1'b0)
			$display("ERROR: %t: %M: timeout, BI_CTRL=%b", $time, r);
		if(r[30] !== intr)
			$display("ERROR: %t: %M: intr=%b exp %b", $time, r[30], intr);
		if(r[2:0] !== err)
			$display("ERROR: %t: %M: err=%b exp %b", $time, r[2:0], err);
		ctrl[31] = 0;
		ctrl[30] = 0;
		`swrite(`BI_CTRL, ctrl);
		`sread(`BI_CTRL, r);
		if(r[31:30] !== 2'd0)
			$display("ERROR: %t: %M: not cleared, BI_CTRL=%b", $time, r);
	end
endtask

// check rom with single reads;
// must be in secure mode;

task gi_check_rom_sgl;
	input [31:0] space;		// address space;
	input reqspc;			// random request spacing;
	input wa;				// addr walking type;
	reg [31:0] past;		// end of space;
	reg [31:0] addr;		// address;
	integer woff, n;
	reg [31:0] r;			// rom read data;
	reg [31:0] e;			// expecetd data;
	begin
		$display("%t: %M: reqspc=%b, wa=%b", $time, reqspc, wa);
		past = space + `GI_ROM_SIZE;
		woff = wa << 2;
		addr = space;
		for(n = 4; addr < past; n = woff | (n << 1)) begin
			if(reqspc)
				space8($random);
			`sread(addr, r);
			e = `rom_get(addr[12:2]);
			sexp(e, r, 32'hffff_ffff);
			addr = space + n;
		end
	end
endtask

// check rom with block reads;
// check critial word order at random;
// must be in secure mode;

task gi_check_rom_blk;
	input [31:0] space;		// address space;
	input [3:0] bsize;		// block size in words;
	input reqspc;			// random request spacing;
	reg [31:0] addr;		// address;
	reg [31:0] past;		// end of space;
	integer n, w;
	reg [31:0] e;			// expecetd data;
	begin
		$display("%t: %M: bsize=%0d, reqspc=%b, wa=1", $time, bsize, reqspc);
		past = space + `GI_ROM_SIZE;
		addr = space;
		for(n = 32; addr < past; n = n | (n << 1)) begin
			if(reqspc)
				space8($random);
			sbo_addr(addr, bsize);
			`bread(addr, bsize);
			for(w = 0; w < bsize; w = w + 1) begin
				e = `rom_get(addr[12:2] ^ w);
				sexp(e, `blk[w], 32'hffff_ffff);
			end
			addr = space + n;
		end
	end
endtask

// check rom reads in non-secure mode;
// entire space should read 0;

task gi_check_rom_zero;
	input [31:0] space;		// address space;
	input [3:0] bsize;		// block size in words;
	input reqspc;			// random request spacing;
	reg [31:0] addr;		// address;
	reg [31:0] past;		// end of space;
	integer n, w;
	begin
		// read entire rom via block requests;

		$display("%t: %M: bsize=%0d, reqspc=%b", $time, bsize, reqspc);
		past = space + `GI_ROM_SIZE;
		addr = space;
		for(n = 32; addr < past; n = n + 32) begin
			if(reqspc)
				space8($random);
			sbo_addr(addr, bsize);
			`bread(addr, bsize);
			for(w = 0; w < bsize; w = w + 1)
				sexp(0, `blk[w], 32'hffff_ffff);
			addr = space + n;
		end
	end
endtask

// check rom content;
// must be in secure mode;
// complete checking is left to system dv;

task gi_check_rom;
	input [31:0] space;
	begin
		$display("%M: space=0x%h", space);
		gi_check_rom_sgl(space, 0, 0);
		gi_check_rom_sgl(space, 0, 1);
		gi_check_rom_sgl(space, 1, 0);
		gi_check_rom_sgl(space, 1, 1);
		gi_check_rom_blk(space, `BSIZE_MAX, 0);
		gi_check_rom_blk(space, `BSIZE_MAX, 1);
	end
endtask

// check sram;
// avoid using the sram backdoor for this;

task gi_check_sram;
	input [31:0] space;
	begin
		$display("%M: space=0x%h", space);
		check_mem_sgl(space, `GI_SRAM_SIZE, 0, 0);
		check_mem_sgl(space, `GI_SRAM_SIZE, 0, 1);
		check_mem_sgl(space, `GI_SRAM_SIZE, 1, 0);
		check_mem_sgl(space, `GI_SRAM_SIZE, 1, 1);
		check_mem_blk(space, `GI_SRAM_SIZE, `BSIZE_MAX, 0);
		check_mem_blk(space, `GI_SRAM_SIZE, `BSIZE_MAX, 1);
	end
endtask

// check virage content after reset recall;

task gi_check_vinit;
	input [31:0] pat;		// initial pattern;
	integer n;
	reg [31:0] r;			// read data;
	reg [31:0] pat;			// fill pattern;
	begin
		$display("%M");
		for(n = 0; n < `GI_V_SIZE; n = n + 4) begin
			`sread(`V_MEM + n, r);
			sexp(pat, r, 'hffff_ffff);
			pat = next_pat(pat);
		end
	end
endtask

// check virage sram;
// avoid using the backdoor for this;

task gi_check_vram;
	input [31:0] space;
	begin
		$display("%M: space=0x%h", space);
		check_mem_sgl(space, `GI_V_SIZE, 0, 0);
		check_mem_sgl(space, `GI_V_SIZE, 0, 1);
		check_mem_sgl(space, `GI_V_SIZE, 1, 0);
		check_mem_sgl(space, `GI_V_SIZE, 1, 1);
		check_mem_blk(space, `GI_V_SIZE, `BSIZE_MAX, 0);
		check_mem_blk(space, `GI_V_SIZE, `BSIZE_MAX, 1);
	end
endtask

// test gi after reset;

task gi_after_reset;
	reg [31:0] r;			// read data;
	begin
		$display("%M");

		// init rom and X sram;

		`rom_random;					// fill rom with randoms;
		`sram_fillx;

		// must read from reset vector to enter secure mode;
		// check defaults after reset;

		sec_mode_enter(`GI_ROM_RST, 0);
		gi_regs_after_reset;			// check register defaults;

		// check rom and sram in reset space;

		//gi_check_rom(`GI_ROM_RST);		// check rom in reset space;
		gi_check_sram(`GI_SRAM_RST);	// check sram in flip space;

		// flip rom and sram and recheck;

		`swrite(`SEC_MODE, `SEC_MODE_ON);
		//gi_check_rom(`GI_ROM_SWP);		// check rom in flip space;
		gi_check_sram(`GI_SRAM_SWP);	// check sram in reset space;

		// check virage sram;
		// check content after reset recall;

		`swrite(`V_CMD, 'h0);			// set store ctrl idle;
		gi_check_vram(`GI_V);			// only one address space;
	end
endtask

// bi dma tests;
// DMA is always to SRAM;
// however, cpu must go through right space to check;

task gi_test_bi_dma;
	input [31:0] base;		// base address of sram;
	reg [31:0] mem;			// memory address;
	reg [16:0] ptr;			// sram offset;
	reg [31:0] pat;			// data pattern seed;
	reg intr;				// random interrupt check;
	integer size;			// size in dma blocks;
	integer over;			// ptr beyond sram;
	begin
		// setup system environment;
		// single block mem->gi;
		// walk all BI_MEM/BI_CTRL(PTR) address bits;
		// remember, memory model mirrors physical memory;
		// ptr must be aligned to cacheline and cannot wrap sram;

		$display("%M: single block, mem->gi, walk mem addr");
		sys_dma_setup(1);
		sys_mon_setup(1);
		ptr = 0;
		mem = `MEM_BASE;
		while(mem < `MEM_MAX) begin
			`sram_fillx;					// fill sram with Xs;
			pat = $random;
			dma_bpat(mem, 1, pat);			// fill memory pattern;
			intr = ^pat;
			gi_bi_dma(mem, ptr, 0, 0, intr, 0, 2, 0);
			dma_bexp(base | ptr, 1, pat);
			mem = (mem << 1) | 128;
			ptr = (ptr << 1) | 128;
			if(ptr >= `GI_SRAM_SIZE)
				ptr = 0;					// don't walk out of sram;
		end

		// gi->mem, vary size;
		// however, limit size for short runtime;
		// pick random mem and ptr;
		// mem can wrap (enough physical), but not ptr;

		$display("%M: varying size, mem<-gi, random mem addr");
		for(size = 1; size <= 512; size = (size << 1) + 1) begin
			mem = $random & `MEM_MASK;
			mem[6:0] = 0;
			ptr = { $random, 7'd0 };		// force to cacheline;
			over = ptr + size*128 - `GI_SRAM_SIZE;
			if(over > 0)
				ptr = ptr - over;			// align to end;
			pat = $random;
			dma_bpat(base | ptr, size, pat);
			intr = ^pat;
			gi_bi_dma(mem, ptr, size - 1, 1, intr, 0, 2 * size, 0);
			dma_bexp(mem, size, pat);
		end

		// start dma, then kill it;
		// see that it does not dma more than 1 block;
		// must give it timeout of at least one dma block;
		// dma re-arbitration is delayed until bus flushes transaction;

		$display("%M: start & kill");
		intr = 1;
		gi_bi_dma(0, 0, 0, 0, intr, 0, 2, 1);		// read dma;
		gi_bi_dma(0, 0, 0, 1, intr, 0, 2, 1);		// write dma;

		// dma should not write spaces other than sram;
		// dma should not write sram when other spaces are addressed;
		// XXX virage
		// XXX regs;

		// check that it cannot overwrite DI CRB;
		// moving CRB into main memory is allowed;
		// XXX

		// check that non-secure dma does not write to
		// secure sram area, set by SEC_SRAM;
		// XXX

		// tear down dma setup;

		sys_dma_setup(0);
	end
endtask


// bi dma test;
// write/read sequences;

task gi_bi_dma_wrseq;
	input [15:0] runs;		// # of runs;
	reg [31:0] mem;			// memory address;
	reg [31:0] base;		// base of GI;
	reg [31:0] pat [0:1];	// buf 0/1 patterns;
	reg [31:0] ctrl;		// bi ctrl reg;
	reg [31:0] r;			// read data;
	integer n;
	begin
		$display("%M: %0d runs", runs);
		base = `GI_SRAM_SWP;
		//`sram_fill(32'bx);				// fill sram with Xs;
		sys_dma_setup(1);
		sys_mon_setup(1);
		for(n = 0; n < runs; n = n + 1) begin
			// pick random memory address and patterns;

			mem = $random & `MEM_MASK;
			mem[6:0] = 0;
			`swrite(`BI_MEM, mem);
			pat[0] = $random;
			pat[1] = $random;
			$display("%t: %M: run %0d, mem=%h pat=%h/%h", $time, n, mem, pat[0], pat[1]);

			// seed memories with pattern;

			dma_bpat(base, 1, pat[0]);
			`sread(base, r);
			dma_bpat(mem + 128, 1, pat[1]);
			`sread(mem + 128, r);

			// start write dma, wait for completion;
			// delay for n clocks, no stress;
			// start read dma, wait for completion;

			$display("%t: %M: start write", $time);
			ctrl = 'h8000_0000;
			ctrl[30] = 0;
			ctrl[29:20] = 0;
			ctrl[19] = 1;
			ctrl[16:4] = 0;
			`swrite(`BI_CTRL, ctrl);
			`sread(`BI_CTRL, r);
			while(r[31] === 1'b1)
				`sread(`BI_CTRL, r);

			delay(n );

			$display("%t: %M: start read", $time);
			ctrl = 'h8000_0000;
			ctrl[30] = 0;
			ctrl[29:20] = 0;
			ctrl[19] = 0;
			ctrl[16:4] = 0;
			`swrite(`BI_CTRL, ctrl);
			`sread(`BI_CTRL, r);
			while(r[31] === 1'b1)
				`sread(`BI_CTRL, r);

			// check expecetd patterns;

			`sread(mem, r);
			dma_bexp(mem, 1, pat[0]);
			dma_bexp(base, 1, pat[1]);
		end
		sys_dma_setup(0);
		sys_mon_setup(0);
	end
endtask


// do a di interface reset;
// test various bits and the reset interrupt;

task gi_di_reset;
	reg [31:0] ctrl;		// di control;
	reg [31:0] r;			// read data;
	begin
		$display("%t: %M", $time);
		`di_reset(1);					// assert reset;
		ctrl[31] = 1;					// write interrupt masks;
		ctrl[30:24] = 7'b000_0001;		// enable RST_MASK;
		ctrl[23:16] = 8'hff;			// clear all pending intr;
		ctrl[15:0] = 0;					// don't affect other bits;
		`swrite(`DI_CTRL, ctrl);
		`sread(`DI_CTRL, r);
		if(r[30:24] !== ctrl[30:24]) begin
			$display("ERROR: %t: %M: INTR_MASK=%b exp %b",
				$time, r[30:24], ctrl[30:24]);
		end
		if(r[16] !== 1'b1)
			$display("ERROR: %t: %M: RST=%b exp 1", $time, r[16]);

		// RST bit is active until reset is released;

		`di_reset(2);					// deassert reset;
		`sread(`DI_CTRL, r);
		if(r[16] !== 1'b1)
			$display("ERROR: %t: %M: RST=%b exp 1", $time, r[16]);

		// clear RST interrupt;
		// RST bit should go back to 0;

		`swrite(`DI_CTRL, ctrl);		// clear RST bit;
		`sread(`DI_CTRL, r);
		if(r[16] !== 1'b0)
			$display("ERROR: %t: %M: RST=%b exp 0", $time, r[16]);

		// should be in CMD_IN state with DIR=0;

		if(r[1] !== 1'b1)
			$display("ERROR: %t: %M: CMD_IN=%b exp 1", $time, r[1]);
		if(r[0] !== 1'b0)
			$display("ERROR: %t: %M: DIR=%b exp 0", $time, r[0]);

		// config DI for fastest GC speed;

		ctrl[31:24] = 8'ha8;
		ctrl[23:16] = 8'h00;
		ctrl[15:0] = `DI_TIM_GC162;
		`swrite(`DI_CONF, ctrl);
		`sread(`DI_CONF, r);
		if(r !== ctrl) begin
			$display("ERROR: %t: %M: DI_CONF=0x%h/%b exp 0x%h/%b",
				$time, r, r, ctrl, ctrl);
		end
	end
endtask

// check di control state;

task gi_di_ctrl;
	input [31:0] mask;
	input [31:0] val;
	reg [31:0] r;
	begin
		`sread(`DI_CTRL, r);
		if((r & mask) !== (val & mask))
			$display("ERROR: %t: %M: DI_CTRL=0x%h exp 0x%h mask 0x%h",
				$time, r, val, mask);
	end
endtask

// start BE burst;
// it is assumed that be is not busy;
// don't wait for BE to complete;

task gi_be_start;
	input [11:0] size;		// size in bytes;
	input out;				// out phase;
	input [16:0] ptr;		// sram pointer;
	reg [31:0] mask;		// ctrl mask;
	reg [31:0] ctrl;		// expected ctrl values;
	reg [31:0] be;			// be cmd code;
	reg [31:0] r;			// read data;
	begin
		// issue BE burst request;

		$display("%t: %M: size=%0d, out=%b, ptr=0x%h",
			$time, size, out, ptr);
		mask = `DI_CTRL_BREAK_STATE | `DI_CTRL_DIR_STATE;
		ctrl = 0;
		ctrl[0] = out;
		gi_di_ctrl(mask, ctrl);
			
		be[31:20] = size - 1;
		be[19] = out;
		be[18] = 1;
		be[17] = 0;
		be[16:0] = ptr;
		`swrite(`DI_BE, be);
		`sread(`DI_BE, r);
		if(r[19] !== out)
			$display("ERROR: %t: %M: BE_OUT=%b exp %b", $time, r[19], out);
		if(r[18] !== 1'b1)
			$display("ERROR: %t: %M: BE_EXEC=%b exp 1", $time, r[18]);
	end
endtask

// wait for BE to finish;
// timeout if it takes longer than to;
// or stop on first wanted interrupt;

task gi_di_wait;
	input [15:0] to;		// timeout in usec;
	input [31:0] intr;		// wanted interrupt;
	reg [31:0] ctrl;		// di ctrl;
	reg [31:0] r;			// read data;
	reg done;				// done with burst;
	begin
		$display("%t: %M: to=%0dus, intr=0x%h", $time, to, intr);
		done = 0;
		while((done !== 1'b1) && (to > 0)) begin
			delay(100);
			if(intr != 0) begin
				`sread(`DI_CTRL, ctrl);
				done = |(ctrl[19:16] & intr[19:16]);
			end else begin
				`sread(`DI_BE, r);
				if(r[18] === 1'b0)
					done = 1;
			end
			to = to - 1;
		end
		if(done !== 1)
			$display("ERROR: %t: %M: timeout, DI_BE=%b", $time, r);
		$display("%t: %M: intr=0x%h size=%0d, out=%b, ptr=0x%h",
			$time, ctrl[19:16], r[31:20], r[19], r[16:0]);
	end
endtask

// formulate command in host;

task gi_di_make_cmd;
	input [31:0] pat;			// pattern seed;
	input [15:0] size;			// size in bytes;
	integer n;
	begin
		$display("%t: %M: pat=0x%h, size=%0d",
			$time, pat, size);
		for(n = 0; n < size; n = n + 4) begin
			`di_put(n, pat);
			pat = next_pat(pat);
		end
	end
endtask

// check cmd in sram;

task gi_di_check_cmd;
	input [31:0] addr;			// address in buffer;
	input [31:0] pat;			// pattern seed;
	input [15:0] size;			// size in bytes;
	integer n;
	reg [31:0] r;				// read data;
	begin
		$display("%t: %M: addr=0x%h pat=0x%h, size=%0d",
			$time, addr, pat, size);
		for(n = 0; n < size; n = n + 4) begin
			`sread(addr, r);
			if(r !== pat) begin
				$display("ERROR: %t: %M: [0x%h] [%0d] 0x%h exp 0x%h",
					$time, addr, n, r, pat);
			end
			pat = next_pat(pat);
			addr = addr + 4;
		end
	end
endtask

// formulate reply data in sram;

task gi_di_make_reply;
	input [31:0] addr;			// sram address;
	input [31:0] pat;			// pattern seed;
	input [15:0] size;			// size in bytes;
	integer n;
	begin
		$display("%t: %M: addr=0x%h pat=0x%h, size=%0d",
			$time, addr, pat, size);
		for(n = 0; n < size; n = n + 4) begin
			`swrite(addr, pat);
			pat = next_pat(pat);
			addr = addr + 4;
		end
	end
endtask

// check reply data in host buffer;

task gi_di_check_reply;
	input [31:0] pat;			// pattern seed;
	input [15:0] size;			// size in bytes;
	integer n;
	reg [31:0] r;				// read data;
	begin
		$display("%t: %M: pat=0x%h, size=%0d",
			$time, pat, size);
		for(n = 0; n < size; n = n + 4) begin
			`di_get(n, r);
			if(r !== pat) begin
				$display("ERROR: %t: %M: [%0d] 0x%h exp 0x%h",
					$time, n, r, pat);
			end
			pat = next_pat(pat);
		end
	end
endtask

// force ptr alignment;
// make sure we don't walk out of sram;

function [16:0] gi_ptr;
	input [16:0] ptr;
	input [15:0] size;
	integer over;
	begin
		ptr[1:0] = 0;
		if(ptr[16])
			ptr[15] = 0;
		over = ptr + size - `GI_SRAM_SIZE;
		if(over > 0)
			ptr = ptr - over;
		gi_ptr = ptr;
	end
endfunction

// start host di transaction;

task di_host_start;
	input dir;					// host->gi;
	input [15:0] tsize;			// host transmit size;
	input [15:0] rsize;			// host response size;
	input wr_cmd;				// is a write transaction;
	input rss;
	begin
		$display("%t: %M: dir=%b, wr_cmd=%b, tsize=%0d, rsize=%0d",
			$time, dir, wr_cmd, tsize, rsize);
		`di_xfer(dir, tsize, rsize, wr_cmd, rss);
	end
endtask
	

// start gi di transaction;

task gi_di_start;
	input dir;					// host->gi;
	input [16:0] ptr;			// sram offset;
	input [15:0] size;			// be size;
	begin
		// start either host or BE first;
		// one should always wait for the other;

		$display("%t: %M: dir=%b, ptr=0x%h, size=%0d",
			$time, dir, ptr, size);
		gi_be_start(size, dir, ptr);
	end
endtask

// be interrupt environment;

task gi_di_intr;
	input setup;				// setup interrupts;
	input [31:0] intr;			// interrupt 
	reg [31:0] r;				// status;
	reg [27:24] mask;			// intr mask;
	begin
		$display("%t: %M: setup=%b, intr=0x%h",
			$time, setup, intr);
		if(setup) begin
			intr[31] = 1;				// write intr mask;
			intr[23:16] = 8'hff;		// clear all pending;
			intr[15:0] = 0;				// do not touch anything else;
			`swrite(`DI_CTRL, intr);
			`sread(`DI_CTRL, r);
			if(r[19:16] !== 4'd0)
				$display("ERROR: %t: %M: intr not cleared, %b", $time, r[19:16]);
		end else begin
			`sread(`DI_CTRL, r);
			mask = intr[27:24];
			if((r[19:16] & mask) !== (intr[19:16] & mask)) begin
				$display("ERROR: %t: %M: mask %b, intr %b exp %b",
					$time, mask, r[19:16], intr[19:16]);
			end
		end
	end
endtask

// simple di request/reply tests;
// tests do not use sha/aes, nor gi ctrl;
// no breaks, errors or BE kills;
// tests BE polling and DONE_INTR;

task gi_di_simple;
	reg [16:0] ptr;				// sram offset;
	reg [31:0] pat;				// di command/reply pattern;
	reg rss;					// random strobes/stalls;
	begin
		// fully reset the di interface;

		$display("%M: reset");
		gi_di_reset;

		// issue inquiry command before di is ready;
		// let device stall host;
		// dstrb should stall the host sending the command;
		// turn around for reception of response;
		// poll the BE EXEC bit for end of bursts;

		$display("%M: inquiry");
		ptr = gi_ptr($random, 12);
		rss = 0;
		pat = 'h1200_0000;
		gi_di_make_cmd(pat, 12);
		gi_di_intr(1, 0);
		di_host_start(`HDIR_OUT, 12, 32, 0, rss);
		gi_di_start(`HDIR_OUT, ptr, 12);
		gi_di_wait(1, 0);
		gi_di_intr(0, `DI_CTRL_ALL_MASK);
		gi_di_check_cmd(`GI_SRAM_SWP + ptr, pat, 12);

		ptr = gi_ptr($random, 32);
		pat = $random;
		gi_di_make_reply(`GI_SRAM_SWP + ptr, pat, 32);
		gi_di_intr(1, 0);
		di_host_start(`HDIR_IN, 12, 32, 0, rss);
		gi_di_start(`HDIR_IN, ptr, 32);
		gi_di_wait(5, 0);
		gi_di_intr(0, `DI_CTRL_ALL_MASK);
		gi_di_check_reply(pat, 32);

		// test simple command with shortest 4-byte response;
		// let host stall device;
		// use the DONE_INTR for end of BE bursts;

		$display("%M: shortest response");
		ptr = gi_ptr($random, 12);
		rss = 1;
		pat = 'he012_3456;
		gi_di_make_cmd(pat, 12);
		gi_di_intr(1, `DI_CTRL_DONE_MASK);
		gi_di_start(`HDIR_OUT, ptr, 12);
		di_host_start(`HDIR_OUT, 12, 4, 0, rss);
		gi_di_wait(1, `DI_CTRL_DONE_INTR);
		gi_di_intr(0, `DI_CTRL_ALL_MASK | `DI_CTRL_DONE_INTR);
		gi_di_check_cmd(`GI_SRAM_SWP + ptr, pat, 12);

		ptr = gi_ptr($random, 4);
		pat = $random;
		gi_di_make_reply(`GI_SRAM_SWP + ptr, pat, 4);
		gi_di_intr(1, `DI_CTRL_DONE_MASK);
		gi_di_start(`HDIR_IN, ptr, 4);
		di_host_start(`HDIR_IN, 12, 4, 0, rss);
		gi_di_wait(2, `DI_CTRL_DONE_INTR);
		gi_di_intr(0, `DI_CTRL_ALL_MASK | `DI_CTRL_DONE_INTR);
		gi_di_check_reply(pat, 4);

		// test command with two BE responses;
		// cmd uses DONE_INTR;
		// first response polls, second uses DONE_INTR;

		$display("%M: two response bursts");
		ptr = gi_ptr($random, 12);
		rss = $random;
		pat = 'h7f80_01fe;
		gi_di_make_cmd(pat, 12);
		gi_di_intr(1, `DI_CTRL_DONE_MASK);
		gi_di_start(`HDIR_OUT, ptr, 12);
		di_host_start(`HDIR_OUT, 12, 40+24, 0, rss);
		gi_di_wait(1, `DI_CTRL_DONE_INTR);
		gi_di_intr(0, `DI_CTRL_ALL_MASK | `DI_CTRL_DONE_INTR);
		gi_di_check_cmd(`GI_SRAM_SWP + ptr, pat, 12);

		ptr = gi_ptr($random, 40+24);
		pat = $random;
		gi_di_make_reply(`GI_SRAM_SWP + ptr, pat, 40+24);
		gi_di_intr(1, 0);
		di_host_start(`HDIR_IN, 12, 40+24, 0, rss);
		gi_di_start(`HDIR_IN, ptr + 0, 40);
		gi_di_wait(5, 0);
		gi_di_intr(0, `DI_CTRL_ALL_MASK);
		gi_di_intr(1, `DI_CTRL_DONE_MASK);
		gi_di_start(`HDIR_IN, ptr + 40, 24);
		gi_di_wait(2, `DI_CTRL_DONE_INTR);
		gi_di_intr(0, `DI_CTRL_ALL_MASK | `DI_CTRL_DONE_INTR);
		gi_di_check_reply(pat, 40+24);

		// test a new write command;
		// the in phase is longer than 12 bytes;
		// first cmd uses DONE_INTR, second polls;
		// response uses DONE_INTR;

		$display("%M: two cmd bursts");
		ptr = gi_ptr($random, 12+32+12);
		rss = $random;
		pat = 'h0123_4567;
		gi_di_make_cmd(pat, 12+32+12);
		gi_di_intr(1, `DI_CTRL_DONE_MASK);
		di_host_start(`HDIR_OUT, 12+32+12, 4, 1, rss);
		gi_di_start(`HDIR_OUT, ptr + 0, 12);
		gi_di_wait(2, `DI_CTRL_DONE_INTR);
		gi_di_intr(0, `DI_CTRL_ALL_MASK | `DI_CTRL_DONE_INTR);
		gi_di_intr(1, 0);
		gi_di_start(`HDIR_OUT, ptr + 12, 32+12);
		gi_di_wait(3, 0);
		gi_di_intr(0, `DI_CTRL_ALL_MASK);
		gi_di_check_cmd(`GI_SRAM_SWP + ptr, pat, 12+32+12);

		ptr = gi_ptr($random, 4);
		pat = $random;
		gi_di_make_reply(`GI_SRAM_SWP + ptr, pat, 4);
		gi_di_intr(1, `DI_CTRL_DONE_MASK);
		di_host_start(`HDIR_IN, 12+32+12, 4, 0, rss);
		gi_di_start(`HDIR_IN, ptr, 4);
		gi_di_wait(1, `DI_CTRL_DONE_INTR);
		gi_di_intr(0, `DI_CTRL_ALL_MASK | `DI_CTRL_DONE_INTR);
		gi_di_check_reply(pat, 4);
	end
endtask

// set error signal;

task gi_di_error;
	input set;
	reg [31:0] ctrl;
	begin
		$display("%t: %M: set=%b", $time, set);
		ctrl = set? `DI_CTRL_ERROR : 0;
		`swrite(`DI_CTRL, `DI_CTRL_ERROR_WE | ctrl);
	end
endtask

// test di response with error;
// also tests the DIR_INTR;

task gi_di_rsp_error;
	reg [16:0] ptr;				// sram offset;
	reg [31:0] pat;				// di command/reply pattern;
	reg rss;					// random strobes/stalls;
	begin
		// error after command;

		$display("%M: error after command");
		ptr = gi_ptr($random, 12);
		rss = $random;
		pat = 'hf055_aa0f;
		gi_di_make_cmd(pat, 12);
		gi_di_intr(1, 0);
		di_host_start(`HDIR_OUT, 12, 32, 0, rss);
		gi_di_start(`HDIR_OUT, ptr, 12);
		gi_di_wait(2, 0);
		gi_di_check_cmd(`GI_SRAM_SWP + ptr, pat, 12);

		// tell host to begin an IN phase;
		// however, do not start BE to send response;
		// instead, assert error;
		// interface should go back to OUT state;
		// errror can stay asserted until next cmd is in;

		rss = $random;
		di_host_start(`HDIR_IN, 12, 32, 0, rss);
		gi_di_intr(1, `DI_CTRL_DIR_MASK);
		gi_di_error(1);
		gi_di_wait(2, `DI_CTRL_DIR_INTR);
		gi_di_intr(0, `DI_CTRL_ALL_MASK | `DI_CTRL_DIR_INTR);

		// get next command;
		// release error;

		$display("%M: next command");
		ptr = gi_ptr($random, 12);
		rss = $random;
		pat = 'he0f5_5aa7;
		gi_di_make_cmd(pat, 12);
		gi_di_intr(1, `DI_CTRL_DONE_MASK);
		di_host_start(`HDIR_OUT, 12, 4, 0, rss);
		gi_di_start(`HDIR_OUT, ptr, 12);
		gi_di_wait(2, `DI_CTRL_DONE_INTR);
		gi_di_error(0);
		gi_di_intr(0, `DI_CTRL_ALL_MASK | `DI_CTRL_DONE_INTR);
		gi_di_check_cmd(`GI_SRAM_SWP + ptr, pat, 12);

		// handle response;

		ptr = gi_ptr($random, 4);
		pat = $random;
		gi_di_make_reply(`GI_SRAM_SWP + ptr, pat, 4);
		gi_di_intr(1, 0);
		gi_di_start(`HDIR_IN, ptr, 4);
		di_host_start(`HDIR_IN, 12, 4, 0, rss);
		gi_di_wait(2, 0);
		gi_di_intr(0, `DI_CTRL_ALL_MASK);
		gi_di_check_reply(pat, 4);
	end
endtask

// test di break protocol;

task gi_di_break;
	reg [16:0] ptr;				// sram offset;
	reg [31:0] pat;				// di command/reply pattern;
	reg rss;					// random strobes/stalls;
	reg [7:0] rdel;				// random length of break phase 4;
	begin
		// issue normal command;

		$display("%M: issue cmd");
		pat = $random;
		ptr = gi_ptr(pat, 12);
		rss = pat[31];
		pat[31:24] = 'h01;
		gi_di_make_cmd(pat, 12);
		gi_di_intr(1, `DI_CTRL_BREAK_MASK);
		di_host_start(`HDIR_OUT, 12, 64, 0, rss);
		gi_di_start(`HDIR_OUT, ptr, 12);
		gi_di_wait(2, 0);
		gi_di_check_cmd(`GI_SRAM_SWP + ptr, pat, 12);

		// set random break position;
		// check only bytes up to the break position;

		$display("%M: issue break");
		`di_break(32);
		pat = $random;
		ptr = gi_ptr(pat, 64);
		rss = pat[31];
		gi_di_make_reply(`GI_SRAM_SWP + ptr, pat, 64);
		gi_di_intr(1, `DI_CTRL_DONE_MASK | `DI_CTRL_BREAK_MASK);
		gi_di_start(`HDIR_IN, ptr, 64);
		di_host_start(`HDIR_IN, 12, 64, 0, rss);
		gi_di_wait(2, `DI_CTRL_DONE_INTR | `DI_CTRL_BREAK_INTR);
		gi_di_intr(0, `DI_CTRL_ALL_MASK | `DI_CTRL_BREAK_INTR);
		gi_di_check_reply(pat, 32);

		// acknowledge break;
		// kill BE burst;
		// terminate break;

		$display("%M: ack break");
		gi_di_ctrl(`DI_CTRL_BREAK_STATE, `DI_CTRL_BREAK_STATE);
		`swrite(`DI_CTRL, `DI_CTRL_BREAK_INTR);
		`swrite(`DI_BE, 'h0);
		`sread(`DI_CTRL, pat);
		gi_di_ctrl(`DI_CTRL_BREAK_STATE, 0);
		rdel = $random;
		#(77 + rdel);
		`swrite(`DI_CTRL, `DI_CTRL_BREAK_TERM);

		// issue new command;

		$display("%M: new command");
		pat = $random;
		ptr = gi_ptr(pat, 12);
		rss = pat[31];
		pat[31:24] = 'h20;
		gi_di_make_cmd(pat, 12);
		gi_di_intr(1, `DI_CTRL_DONE_MASK);
		gi_di_start(`HDIR_OUT, ptr, 12);
		di_host_start(`HDIR_OUT, 12, 4, 0, rss);
		gi_di_wait(2, `DI_CTRL_DONE_INTR);
		gi_di_check_cmd(`GI_SRAM_SWP + ptr, pat, 12);

		ptr = gi_ptr($random, 4);
		pat = $random;
		gi_di_make_reply(`GI_SRAM_SWP + ptr, pat, 4);
		gi_di_intr(1, 0);
		di_host_start(`HDIR_IN, 12, 4, 0, rss);
		gi_di_start(`HDIR_IN, ptr, 4);
		gi_di_wait(2, 0);
		gi_di_intr(0, `DI_CTRL_ALL_MASK);
		gi_di_check_reply(pat, 4);
	end
endtask



// test ais registers;
// only bits without side effects;

task gi_ais_regs;
	reg [31:0] r;			// read data;
	begin
		`sread(`AIS_CTRL, r);
		if(r[31] !== 1'b0)
			$display("ERROR: %t: %M: ais busy, BI_CTRL=0x%h", $time, r);
		$display("%M: AIS_CTRL");
		test_reg(`AIS_CTRL, `AIS_CTRL_MASK | `AIS_CTRL_BUF_OFF, `AIS_CTRL_BUF_WE);
		$display("%M: AIS_PTR0");
		test_reg(`AIS_PTR0, `AIS_PTR_ADDR | `AIS_PTR_EMPTY, 0);
		$display("%M: AIS_PTR1");
		test_reg(`AIS_PTR1, `AIS_PTR_ADDR | `AIS_PTR_EMPTY, 0);
	end
endtask

// test ais controller start/stop;

task gi_ais_kill;
	reg [31:0] mem;		// memory address;
	reg [31:0] pat;		// seed pattern;
	reg [31:0] r;		// read data;
	begin
		// fill one audio buffer with pattern;

		mem = `MEM_BASE;
		pat = $random;
		dma_bpat(mem, 4096/128, pat);			// fill memory pattern;

		// ais_clk from host, but no ais_lr;

		$display("%M: start, host idle");
		`swrite(`AIS_PTR0, `AIS_PTR_FULL | mem);
		`swrite(`AIS_PTR1, `AIS_PTR_EMPTY | 0);
		`swrite(`AIS_CTRL, `AIS_CTRL_EXEC | `AIS_CTRL_BUF_WE | 0);
		`sread(`AIS_CTRL, r);
		if(r[31] !== 1'b1)
			$display("ERROR: %t: %M: EXEC=%b exp 1", $time, r[31]);
		`swrite(`AIS_CTRL, 0);
		`sread(`AIS_CTRL, r);
		if(r[31] !== 1'b0)
			$display("ERROR: %t: %M: EXEC=%b exp 0", $time, r[31]);
	end
endtask

// fill ais sample buffer with expected pattern;

task gi_ais_pat;
	input [15:0] ns;	// # of samples;
	input [31:0] pat;	// pattern seed;
	integer n;
	begin
		for(n = 0; n < ns; n = n + 1) begin
			`ais_put(n, pat);
			pat = next_pat(pat);
		end
	end
endtask

// check ais interrupt;

task gi_ais_intr;
	input exp;			// expected value;
	reg [31:0] r;		// read data;
	begin
		`sread(`BI_INTR, r);
		if(r[2] !== exp)
			$display("ERROR: %t: %M: ais intr=%b exp %b", $time, r[2], exp);
	end
endtask

// test ais streaming audio;
// clock ais_host as fast as gi can handle;
// ais_host only has a 64kB buffer;

task gi_ais_stream;
	input [3:0] nbuf;		// # of 4kB blocks to stream;
	input fast;				// run ais_host faster than normal;
	input intr;				// use interrupt;
	reg [31:0] mem;			// memory address;
	reg [31:0] pat;			// seed pattern;
	reg [31:0] r;			// read data;
	reg [31:0] ctrl;		// control register;
	integer nb;				// # of bytes;
	integer n;
	reg [31:0] bufs [0:15];	// all 4kB buffers;
	reg [31:0] ptr;			// buffer pointer;
	integer to;				// timeout;
	reg done;				// done with buffer;
	begin
		// run ais_host with optional fast clock,
		// to save time in the simulator;

		$display("%M: nbuf=%0d fast=%b", nbuf, fast);
		`ais_set(fast);
		sys_dma_setup(1);
		sys_mon_setup(1);

		// fill audio buffers with pattern;
		// setup buffer list;

		mem = `MEM_BASE;
		mem[19:12] = $random;				// random 4kB block;
		pat = $random;
		nb = nbuf * 4096;
		gi_ais_pat(nb / 4, pat);			// fill ais_host buffer;
		dma_bpat(mem, nb / 128, pat);		// fill memory pattern;
		for(n = 0; n < 16; n = n + 1) begin
			ptr = mem + n * 4096;
			if(n >= nbuf)
				ptr[0] = 1;					// empty buffer;
			bufs[n] = ptr;
		end

		// start ais_host, toggles ais_lr;
		// pick random buffer in sram;
		// start ais stream;

		`ais_start(nb / 4, 1);
		gi_ais_intr(0);
		`swrite(`AIS_PTR0, bufs[0]);
		`swrite(`AIS_PTR1, bufs[1]);
		ptr = gi_ptr($random, 256);
		ctrl = `AIS_CTRL_EXEC | `AIS_CTRL_BUF_WE | ptr;
		if(intr)
			ctrl = ctrl | `AIS_CTRL_MASK;
		`swrite(`AIS_CTRL, ctrl);

		// ais should be busy now;
		// but still no interrupt;

		`sread(`AIS_CTRL, r);
		if(r[31] !== 1'b1)
			$display("ERROR: %t: %M: start EXEC=%b exp 1", $time, r[31]);
		gi_ais_intr(0);

		// wait for buffers to become empty;
		// check interrupts;
		// supply next buffer;

		ptr = `AIS_PTR0;
		for(n = 0; n < nbuf; n = n + 1) begin
			to = 1024 * 33 * `ais_period / 1000;
			done = 0;
			while( !done && (to > 0)) begin
				delay(100);
				`sread(ptr, r);
				done = (r[0] === 1'b1);
				to = to - 1;
			end
//XXX check AIS_CTRL for dma errors;
			if(done) begin
				gi_ais_intr(intr);
				`swrite(ptr, bufs[n+2]);
				gi_ais_intr(0);
				ptr = n[0]? `AIS_PTR0 : `AIS_PTR1;
			end else
				n = nbuf;
		end
		if( !done)
			$display("ERROR: %t: %M: timeout buf=%0d", $time, n);

		// ais should still be busy until the last
		// samples have been sent out;

		`sread(`AIS_CTRL, r);
		if(r[31] !== 1'b1)
			$display("ERROR: %t: %M: last EXEC=%b exp 1", $time, r[31]);
		gi_ais_intr(0);

		// dma end is two sram buffers ahead;
		// 2 buffers * 32 samples * 32 bits/sample * cycle time;

		to = 2 * 32 * 32 * `ais_period / 10;
		delay(to);
		`sread(`AIS_CTRL, r);
		if(r[31] !== 1'b0)
			$display("ERROR: %t: %M: done EXEC=%b exp 0", $time, r[31]);
		gi_ais_intr(0);

		// tear down dma channel;

		sys_dma_setup(0);
	end
endtask

// load an aes key and expand;

task gi_aes_key;
	input [127:0] key;		// key;
	input [1:0] which;		// key index;
	reg [31:0] r;			// read data;
	begin
		$display("%t: %M: kidx=%d key=0x%h", $time, which, key);
		`swrite(`AES_KEY, key[127:96]);
		`swrite(`AES_KEY, key[95:64]);
		`swrite(`AES_KEY, key[63:32]);
		`swrite(`AES_KEY, key[31:0]);
		`swrite(`AES_KEXP, { 30'd0, which });
		`sread(`AES_KEXP, r);

		// key expansion should be done in 15 clocks;

		delay(15);
		`sread(`AES_KEXP, r);
		if(r !== which)
			$display("ERROR: %t: %M: AES_KEXP 0x%x exp 0x%x", $time, r, which);
	end
endtask

// test aes key expansion;
// read expanded keys from hw and compare;

task gi_aes_kexp;
	reg [127:0] key;		// key;
	integer i;				// key index;
	begin
		for(i = `AESD_KEXP_MC; i <= `AESE_KEXP_MC; i = i + 1) begin
			$display("%M: kidx %0d", i);
			key = { $random, $random, $random, $random };
			gi_aes_key(key, i);
		end
	end
endtask


// all gi tests;

task test_gi;
	input all;			// run all tests;
	reg test_gi_bi;		// run bi tests;
	reg test_gi_di;		// run di tests;
	reg test_gi_ais;	// run ais tests;
	reg test_gi_aes;	// run aes tests;
	reg test_pci_bug;       // run pci bug
	begin
		$display("%M");
		test_gi_bi = all | $test$plusargs("test_gi_bi");
		test_gi_di = all | $test$plusargs("test_gi_di");
		test_gi_ais = all | $test$plusargs("test_gi_ais");
		test_gi_aes = all | $test$plusargs("test_gi_aes");
		test_pci_bug = all | $test$plusargs("test_pci_bug");

		// always test defaults after reset;
		// they don't need much run time;

		gi_after_reset;

		if(test_pci_bug) begin
			gi_bi_dma_wrseq(32);
		end

		// bi and bi dma tests;

		if(test_gi_bi) begin
			gi_test_bi_regs; //XXX
			gi_test_bi_dma(`GI_SRAM_SWP);
		end

		// di tests;

		if(test_gi_di) begin
			gi_di_simple;				// simple di transactions;
			gi_di_rsp_error;			// check error protocol;
			gi_di_break;				// check break protocol;
		end

		// ais tests;

		if(test_gi_ais) begin
			gi_ais_regs;				// test ais registers;
			gi_ais_kill;				// start and kill;
			gi_ais_stream(1, 0, 0);		// one buffer, normal speed, no intr;
			gi_ais_stream(2, 1, 0);		// two buffers, fast, no intr;
			gi_ais_stream(3, 1, 1);		// three buffers, fast, intr;
		end

		// aes tests;

		if(test_gi_aes) begin
			gi_aes_kexp;				// test key expander;
		end

	end
endtask

reg [31:0] blk [0:3];
`ifdef INC_GI

task gi_swrite;
	input [31:0] addr;
	input [31:0] data;

	begin
		top.`gi_cpu_write1w(addr, 4'b1111, data);
	end
endtask


task gi_sread;
	input [31:0] addr;
	output [31:0] data;
	begin
		top.`gi_cpu_readbk1w(addr, 4'b1111, data);
	end
endtask

task gi_bread;
	input [31:0] addr;
	input [2:0] size;
	begin
		case (size)
			1: top.`gi_cpu_readbk1w(addr, 4'b1111, `blk[0]);
			2: top.`gi_cpu_readbk2w(addr, `blk[0], `blk[1]);
			4: top.`gi_cpu_readbk4w(addr, `blk[0], `blk[1], `blk[2], `blk[3]);
		endcase
	end
endtask

task gi_bwrite;
	input [31:0] addr;
	input [2:0] size;
	begin
		case (size)
			1: top.`gi_cpu_write1w(addr, 4'b1111, `blk[0]);
			2: top.`gi_cpu_write2w(addr, `blk[0], `blk[1]);
			4: top.`gi_cpu_write4w(addr, `blk[0], `blk[1], `blk[2], `blk[3]);
		endcase
	end
endtask

`endif
function [31:0]  sgl_mask;
	input [1:0] addr;
	input [1:0] size;
	reg [31:0] mask;
	begin
		case ({size,addr})
			4'b0000: mask = 32'hff000000;
			4'b0001: mask = 32'h00ff0000;
			4'b0010: mask = 32'h0000ff00;
			4'b0011: mask = 32'h000000ff;
			4'b0100: mask = 32'hffff0000;
			4'b0110: mask = 32'h0000ffff;
			4'b1000: mask = 32'hffffff00;
			4'b1001: mask = 32'h00ffffff;
			4'b1100: mask = 32'hffffffff;
			default: begin
				mask = 32'h00000000;
				$display("ERROR: %t: %M: illegal addr %b, size %b", 
					 $time, addr, size);
			end
		endcase
		sgl_mask = mask;
	end
endfunction

function [31:0] rtl_to_c;
	input [1:0] addr;
	input [1:0] size;
	input [31:0] data;
	reg [31:0] ret;
	begin
		case ({size,addr})
			4'b0000: ret = (data >> 24) & 32'hff;
			4'b0001: ret = (data >> 16) & 32'hff;
			4'b0010: ret = (data >> 8) & 32'hff;
			4'b0011: ret = data & 32'hff;
			4'b0100: ret = (data >> 16) & 32'hffff;
			4'b0110: ret = data & 32'hffff;
			4'b1000: ret = (data >> 8) & 32'hffffff;
			4'b1001: ret = data & 32'hffffff;
			4'b1100: ret = data;
			default: begin
				$display("ERROR: %t: %M: illegal addr %b, size %b",
					$time, addr, size);
				ret = 32'hxxxxxxxx;
			end
		endcase
		rtl_to_c = ret;	
	end
endfunction

function [31:0] c_to_rtl;
	input [1:0] addr;
	input [1:0] size;
	input [31:0] data;
	reg [31:0] ret;
	begin
		case ({size,addr})
			4'b0000: ret = (data & 32'hff) << 24;
			4'b0001: ret = (data & 32'hff) << 16;
			4'b0010: ret = (data & 32'hff) << 8;
			4'b0011: ret = data & 32'hff;
			4'b0100: ret = (data & 32'hffff) << 16;
			4'b0110: ret = data & 32'hffff;
			4'b1000: ret = (data & 32'hffffff) << 8;
			4'b1001: ret = data & 32'hffffff;
			4'b1100: ret = data;
			default: begin
				$display("ERROR: %t: %M: illegal addr %b, size %b",
					$time, addr, size);
				ret = 32'hxxxxxxxx;
			end
		endcase
		c_to_rtl = ret;	
	end
endfunction

task bd_sread;
	input [31:0] addr;
	input [1:0]  size;
	output [31:0] rdata;
	reg [31:0] mask;	
	reg [31:0] old_addr;	

	begin
		old_addr = addr;
		if (addr[31:27] === 5'h0) begin /* memory */
`ifdef ALI_GI
			addr[1:0] = 0;
			addr = addr % top.mem_max_size;
			SDRAM_dump_array(addr, 1);
			rdata = top.mem_array[0];
`else
			addr[31:20] = 0;
			addr = addr >> 2;
			rdata = `mem[addr];
`endif
		end

		if (addr[31:20] === (`GI_BASE >> 20)) begin /* sram */
			addr = addr & 32'h1ffff;
			addr = addr >> 2;
			rdata = `sram_get(addr);
		end

		rdata = rtl_to_c(old_addr, size, rdata); 
	end
endtask

task bd_swrite;
	input [31:0] addr;
	input [1:0]  size;
	input [31:0] wdata;
	reg [31:0] mask;
	reg [31:0] rdata;	

	begin
		wdata = c_to_rtl(addr, size, wdata);
		mask = sgl_mask(addr[1:0], size);
		if (addr[31:27] === 5'h0) begin /* memory */
`ifdef ALI_GI
			addr[1:0] = 0;
			addr = addr % top.mem_max_size;
			SDRAM_dump_array(addr, 1);
			top.mem_array[0] = (top.mem_array[0] & (~mask)) | (wdata & mask);
			SDRAM_load_memory(addr, 1);
`else
			addr[31:20] = 0;
			addr = addr >> 2;
			`mem[addr] = (`mem[addr] & (~mask)) | (wdata & mask);
`endif
		end

		if (addr[31:20] === (`GI_BASE >> 20)) begin /* sram */
			addr = addr & 32'h1FFFF;
			addr = addr >> 2;
			rdata = `sram_get(addr);
			`sram_put(addr, (rdata & (~mask)) | (wdata & mask));
		end
	end
endtask

task bd_bwrite;
	input [31:0] addr;
	input [3:0]  size;
	input [31:0] data0;
	input [31:0] data1;
	input [31:0] data2;
	input [31:0] data3;
	input [31:0] data4;
	input [31:0] data5;
	input [31:0] data6;
	input [31:0] data7;
	reg[31:0] data[0:7];
	integer i;

	begin
		data[0] = data0;
		data[1] = data1;
		data[2] = data2;
		data[3] = data3;
		data[4] = data4;
		data[5] = data5;
		data[6] = data6;
		data[7] = data7;

		if(addr[31:27] === 5'h0) begin /* Memory */
`ifdef ALI_GI
			addr[1:0] = 0;
			addr = addr % top.mem_max_size;
			for (i=0; i<size; i = i+1) begin
				top.mem_array[i] = data[i];
			end
			SDRAM_load_memory(addr, size);
`else
			addr[31:20] = 0;
			addr = addr >> 2;
			for (i=0; i<size; i = i+1) begin
				`mem[addr+i] = data[i];
			end
`endif
		end

		if (addr[31:20] === (`GI_BASE >> 20)) begin /* sram */
			addr = addr & 32'h1FFFF;
			addr = addr >> 2;
			for (i=0; i<size; i = i+1)
				`sram_put(addr+i, data[i]);
		end
	end
endtask
 

task bd_bread;
	input [31:0] addr;
	input [3:0]  size;
	output [31:0] data0;
	output [31:0] data1;
	output [31:0] data2;
	output [31:0] data3;
	output [31:0] data4;
	output [31:0] data5;
	output [31:0] data6;
	output [31:0] data7;
	reg[31:0] data[0:7];
	integer i;

	begin
		if(addr[31:27] === 5'h0) begin /* Memory */
`ifdef ALI_GI
			addr[1:0] = 0;
			addr = addr % top.mem_max_size;
			SDRAM_dump_array(addr, size);
			for (i=0; i<size; i = i+1)
				data[i] = top.mem_array[i];
`else
			addr[31:20] = 0;
			addr = addr >> 2;
			for (i=0; i<size; i = i+1)
				data[i] = `mem[addr+i];
`endif
		end

		if (addr[31:20] === (`GI_BASE >> 20)) begin /* sram */
			addr = addr & 32'h1ffff;
			addr = addr >> 2;
			for (i=0; i<size; i = i+1) 
				data[i] = `sram_get(addr+i);
		end

		data0 = data[0];
		data1 = data[1];
		data2 = data[2];
		data3 = data[3];
		data4 = data[4];
		data5 = data[5];
		data6 = data[6];
		data7 = data[7];
	end
endtask

