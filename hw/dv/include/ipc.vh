//
//   Port from BB project
//
// :set tabstop=4

`ifdef ALI_GI
`define gi_sim_top                top
`define gi_top                    top.CHIP.CORE.T2_CHIPSET.P_BIU.GI.gi
`define xclk                      `gi_sim_top.pci_clk
`define ipc_clk                   `gi_sim_top.pci_clk
`define blk                       `gi_sim_top.blk

`define	swrite                    gi_swrite
`define	sread                     gi_sread
`define	bwrite                    gi_bwrite
`define	bread                     gi_bread
`define total_time                top.di_mon.total_time
`define err_acc                   top.di_mon.err_acc
`define dir0_count                top.di_mon.dir0_count
`define dir1_count                top.di_mon.dir1_count
`define dir0_time                 top.di_mon.dir0_time
`define dir1_time                 top.di_mon.dir1_time
`define dir0_acc                  top.di_mon.dir0_acc
`define dir1_acc                  top.di_mon.dir1_acc
`define dir0_eff                  top.di_mon.dir0_eff
`define dir1_eff                  top.di_mon.dir1_eff
`define brk_acc                   top.di_mon.brk_acc
`define total_eff                 top.di_mon.total_eff
`define di_brk                    top.gc_dierrb
`define di_err                    top.gc_dibrk
`define send                      top.di_host.send

`else
`define gi_sim_top                sim	
`define ipc_clk                   sim.ipc.sysclk

`define	swrite                    sim.sys.swrite
`define	sread                     sim.sys.sread
`define	bwrite                    sim.sys.bwrite
`define	bread                     sim.sys.bread
`define	blk                       sim.sys.blk
`define mem                       sim.sys.mem

`define total_time                sim.di_mon.total_time
`define err_acc                   sim.di_mon.err_acc
`define dir0_count                sim.di_mon.dir0_count
`define dir1_count                sim.di_mon.dir1_count
`define dir0_time                 sim.di_mon.dir0_time
`define dir1_time                 sim.di_mon.dir1_time
`define dir0_acc                  sim.di_mon.dir0_acc
`define dir1_acc                  sim.di_mon.dir1_acc
`define dir0_eff                  sim.di_mon.dir0_eff
`define dir1_eff                  sim.di_mon.dir1_eff
`define brk_acc                   sim.di_mon.brk_acc
`define total_eff                 sim.di_mon.total_eff
`define di_brk                    sim.di_brk
`define di_err                    sim.di_err
`define send                      sim.di_host.send

`endif

`define sec_bit                   `gi_top.secure
`define sram_put                  `gi_top.sram.put
`define sram_get                  `gi_top.sram.get
`define sram_fill                 `gi_top.sram.fill

`define rom_put                   `gi_top.rom.put
`define rom_get                   `gi_top.rom.get
`define rom_random                `gi_top.rom.random
`define rom_fill                  `gi_top.rom.fill

`define di_reset                  `gi_sim_top.di_host.rst
`define di_xfer                   `gi_sim_top.di_host.xfer
`define di_get                    `gi_sim_top.di_host.get
`define di_put                    `gi_sim_top.di_host.put
`define di_break                  `gi_sim_top.di_host.break
`define di_break_now              `gi_sim_top.di_host.break_now

`define ais_rst                   `gi_sim_top.ais_host.rst
`define ais_check                 `gi_sim_top.ais_host.check
`define ais_set                   `gi_sim_top.ais_host.set
`define ais_get                   `gi_sim_top.ais_host.get
`define ais_put                   `gi_sim_top.ais_host.put
`define ais_start                 `gi_sim_top.ais_host.start
`define ais_period                `gi_sim_top.ais_host.period
`define ais_pos                   `gi_sim_top.ais_host.loop.n

`define v_put                     `gi_sim_top.v_put
`define v_get                     `gi_sim_top.v_get

// Request code
// Regular bus request
`define REQ_SINGLE_READ           1
`define REQ_SINGLE_WRITE          2
`define REQ_BLOCK_READ            3
`define REQ_BLOCK_WRITE           4
`define REQ_STALL                 5
`define REQ_QUIT                  100

// special request for North bridge
`ifdef ALI_GI 

`define REQ_RESET_CHIP            32'h1000
`define REQ_INIT                  32'h1001
`define REQ_POST_INIT             32'h1002
`define REQ_PCI_PRE_FETCH         32'h1003
`define REQ_SET_POS_WR_TIMER      32'h1004
`define REQ_STRAP_PIN_INFO        32'h1005
`define REQ_SDRAM_INIT            32'h1006
`define REQ_SDRAM_SET_ARBT        32'h1007
`define REQ_ROM_INIT              32'h1008
`define REQ_FLASH_TEST_LD_DATA    32'h1009

`define REQ_GET_cpu_vec_02_end    32'h40000002

`endif

// Backdoor request
`define BD_REQ_HANDSHAKE          32'h80000000
`define BD_REQ_SINGLE_READ        32'h80000001
`define BD_REQ_SINGLE_WRITE       32'h80000002
`define BD_REQ_BLOCK_READ         32'h80000003
`define BD_REQ_BLOCK_WRITE        32'h80000004
`define BD_REQ_LOG                32'h80000007
`define BD_REQ_PERSIST_SOCKET     32'h80000100
`define BD_REQ_DISPLAY_MSG        32'h80000101
`define BD_REQ_RTL_TIME           32'h80000400
`define BD_REQ_DUMP               32'h80000200
`define BD_PAT_FILL               32'h80000300
`define BD_PAT_CHECK              32'h80000301


`define BD_SET_ALT_BOOT           32'h80000A00
`define BD_GET_ALT_BOOT           32'h80000A01
`define BD_SET_TEST_IN            32'h80000B00
`define BD_GET_TEST_IN            32'h80000B01

// Backdoor GI request
`define BD_GI_SECURE_BIT          32'h80001000

// Backdoor DI tasks 
`define BD_DI_RESET               32'h80002000
`define BD_DI_XFER                32'h80002001
`define BD_DI_GET                 32'h80002002
`define BD_DI_PUT                 32'h80002003
`define BD_DI_BREAK_AT            32'h80002004
`define BD_DI_BREAK_NOW           32'h80002005

// Backdoor AIS tasks
`define BD_AIS_RESET              32'h80003000
`define BD_AIS_SET                32'h80003001
`define BD_AIS_GET                32'h80003002
`define BD_AIS_PUT                32'h80003003
`define BD_AIS_START              32'h80003004
`define BD_AIS_SET_PERIOD         32'h80003005
`define BD_AIS_GET_PERIOD         32'h80003006
`define BD_AIS_GET_POS            32'h80003007
`define BD_AIS_CHECK              32'h80003008

// Backdoor PCI  
`define BD_SYS_DMA_SETUP          32'h80004000
`define BD_SYS_DMA_CMD_MON        32'h80004001
`define BD_BI_SIZE                32'h80004002

`define BD_REQ_JTAG_WRITE         32'h80010000
`define BD_REQ_JTAG_READ          32'h80010001

// Reply Code 
`define RSP_OK                    1
`define RSP_DATA                  2
`define RSP_ERROR                 100

