/* iosim test sample code
    
   :set tabstop=4
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <unistd.h>
#include <netinet/in.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <getopt.h>
#include "iosim.h"
#include "utils.h"

#include <bsl.h>

#include <gi.h>
#include <sechw.h>

#include <sha1.h>



/* test routines : -1 is fail, 0 is pass */
int do_register_tests(){
  u32 val, val_read, pass;
  int i;
  pass = 0;
  for(i=0; i< 50; i++){
    val = rand() & GI_SHA_BUF_OFF_MASK;
    IO_WRITE(GI_SHA_BUF_REG, val);
    val_read = IO_READ(GI_SHA_BUF_REG);
    if(val  != val_read){
      printf("reg = %08x val = %08x val_read = %08x\n", GI_SHA_BUF_REG, val, val_read);
      pass = -1;
    }
  }
	
  /* register tests: bit walk-throughs */
	
  val = 0x2; /* shift this bit through */
       
  for(i=0; i< 14; i++){
    val = val << 1;
    IO_WRITE(GI_SHA_BUF_REG, val);
    val_read = IO_READ(GI_SHA_BUF_REG);
    if(val != val_read){
      pass = -1;
    }	  
  }
  return pass;
}

u32 get_checksum(u32 start_addr, u32 region_size, u32 data_offset, u32 size, u32 hwdigest_offset, u32 digest_size){
  u32 checksum, val;
  int i;
  checksum = 0;
  for(i=start_addr; i < start_addr + region_size; i= i+4){
    /* exclude the two regions */
    if((i >= start_addr + data_offset) &&
       ( i < start_addr + data_offset + size)){
      ;
    }
    else if((i >= start_addr + hwdigest_offset) &&
       ( i < start_addr + hwdigest_offset + digest_size)){
      ;
    }
    else{ 
      val = IO_READ(i);
      checksum = checksum ^ val;
    }
  }
  return checksum;
}
    

#define MAX_DATA_SIZE ((100*1024)/4)

/* 0 is pass, -1 is fail with respect to software model, -2: data lost
 */
int sha_check(u32 *data, u32 data_offset, u32 size, u32 hwdigest_offset, u32 chaining, u32 save){

  int k, i;
  int pass = 0;
  u32 hwdigest[5];
  int result = 0;
  u32 datacopy[MAX_DATA_SIZE];
  SHA1Context sha;
  
  u32 swdigest[5];
  u32 checksum;

  /* before anything, get checksum of region outside the data and digest
   * buffers 
   */
  checksum = get_checksum(GI_SRAM_START, 96*1024, data_offset, size, hwdigest_offset, 20);
  printf("before checksum = %08x\n",  checksum);
  if((chaining == 0x1) && (size % 128 == 0)){
    result = hw_sha_compute(data, data_offset, size/2, hwdigest, hwdigest_offset, 0x0, save);
    result = hw_sha_compute(data + size/2/4, data_offset + size/2, size/2, hwdigest, hwdigest_offset, 0x1, save);
  }
    
    
  else {
    result = hw_sha_compute(data, data_offset, size, hwdigest, hwdigest_offset, 0x0, save);
    if( result != 0){
      printf("timed out!\n");
      return result;
    }
  }
  
  
  /* check data is still there */
  
  k = 0;
  for(i = 0; i < size; i = i+ 4){
    datacopy[k] = IO_READ(GI_SRAM_START + data_offset + i);
    k++;
  }
  k = 0;
  for(i =0;  i< size; i = i + 4){
    if(data[k] != datacopy[k]){
      pass = -2; 
      return pass;
    }
    k++;
  }

  /* check rest of SRAM is untouched */
  if(get_checksum(GI_SRAM_START, 96*1024, data_offset, size, hwdigest_offset, 20) != checksum){
    pass = -2;
    return pass;
  }


  
  printf("after checksum = %08x\n", get_checksum(GI_SRAM_START, 96*1024, data_offset, size, hwdigest_offset, 20));

  /* check against software model */
  
  k= 0;
  for(i=0; i< size; i= i+4){
    data[k] = htonl(data[k]);
    k++;
  }
  
  
  SHA1Reset(&sha);
  SHA1SetNoPadding(&sha);
  SHA1Input(&sha, (u8 *) data, size);
  SHA1Result(&sha, (u8 *) swdigest);
  
  /* compare */
  pass = 0;
  for(i =0; i< 5; i++){
    printf("sw digest = %08x hw digest = %08x  \n", swdigest[i], hwdigest[i]);
    if(htonl(hwdigest[i]) != swdigest[i]){
      pass = -1;
    }
  }
  
  return pass;
}

/* size in bytes */
void get_random_data(u32 *data, u32 size){
  int k;
  	  
  /* write data */
  for(k=0; k < size/4; k++){
    data[k] = rand();
  }
}

void fill_random_data(u32 start_addr, u32 size){
  int i;
  for(i=start_addr; i < start_addr + size; i= i+4){
    IO_WRITE(i, rand());
  }
}
  

int main(int argc, char **argv)
{
	int quit = 0, c;
	int j;
	extern char *optarg;
	extern int optind, opterr, optopt;
	u32 data[MAX_DATA_SIZE];
	int pass = 0;
	u32 data_offset = 64;
	u32 size = 32;
	u32 chaining = 0;
	u32 hwdigest_offset =0;

	
	/* enter secure mode */
	srand(getpid());
	while ((c = getopt(argc, argv, "q")) != EOF) {
		switch (c) {
			case 'q':
				quit = 1;
				break;
			case 'h':
			case '?': 
			default:
				/* Your help function */
				break;
		}
	}

	if (IpcInit()) 	return -1;		

	  /*  Make persistence connection */
	do_keep_alive_socket(1, 1);

	/* fill GI SRAM with random data before starting */
	
	fill_random_data(GI_SRAM_START, 96*1024);


	IO_WRITE(GI_SHA_CTRL_REG, 0x0);
	
	while((IO_READ(GI_SHA_CTRL_REG) & 0x80000000) !=0){
	  ;
	}

	/* register tests: randoms */
	pass = do_register_tests();

	if ( pass == 0){
	  printf("Register read/write tests PASS\n");
	}
	else {
	  printf("Register read/write tests FAIL\n");
	}
		

	/* size test */
	data_offset = 64;

	size = 64;
	
	for(j =0; j < 11; j++){	  
	  printf("size = %d\n", size);

	  get_random_data(data, size);

	  pass = sha_check(data, data_offset, size, hwdigest_offset, chaining, 0x1);

	  if( pass == 0){
	    printf("Size walk-through test: SHA verify PASS\n");
	  }
	  else if (pass == -1){
	    printf("Size walk-through test: SHA verify FAIL\n");
	  }
	  else if (pass == -2){
	    printf("Size walk-through test: SHA verify FAIL in data\n");
	  }
	  

	  /* for size walk through test: */
	  size = size << 1;
	}
	
	
	/* test corner case: data offset = 0, size = 64; 
	 */
	data_offset = 0;
	hwdigest_offset = 256;
	size = 64;
	get_random_data(data, size);
	chaining = 0;
	pass = sha_check(data, data_offset, size, hwdigest_offset, chaining, 0x1);

	if(pass == 0){
	  printf("Size corner test: SHA verify PASS\n");
	}
	else if (pass == -1){
	  printf("Size corner test: SHA verify FAIL\n");
	}
	
	else if (pass == -2){
	  printf("Size corner test: SHA verify FAIL in data\n");
	}

	

	
	/* size: random tests */
	data_offset = 64;
	hwdigest_offset = 0; 
	
	for(j =0; j < 5; j++){	
	  
	  size = (rand()& 0x3fc0);
	  /* make sure entire data is in GI_SRAM */
	  if( data_offset + size > 0x00017ff0 ){
	    data_offset -= size;
	  }
	  if(data_offset < 64){
	    data_offset = 64;
	  }
	  
	  get_random_data(data, size);

	  pass = sha_check(data, data_offset, size, hwdigest_offset, chaining, 0x1);

	  if( pass == 0){
	    printf("Size randoms test: SHA verify PASS\n");
	  }
	  else if (pass == -1){
	    printf("Size randoms test: SHA verify FAIL\n");
	  }
	  else if (pass == -2){
	    printf("Size corner test: SHA verify FAIL in data\n");
	  }
	
	}

	
	/* data offset test*/
	data_offset = 64;

	size = 64;
	hwdigest_offset = 0;
	
	for(j =0; j < 10; j++){	  
	  printf("size = %d\n", size);

	  get_random_data(data, size);

	  pass = sha_check(data, data_offset, size, hwdigest_offset, chaining, 0x1);

	  if( pass == 0){
	    printf("data-offset walk through test: SHA verify PASS\n");
	  }
	  else if (pass == -1){
	    printf("data-offset walk-through test: SHA verify FAIL\n");
	  }
	  else if (pass == -2){
	    printf("data-offset  walk-through test: SHA verify FAIL in data\n");
	  }
	  

	  /* for data offset walk through test */
	  data_offset = data_offset << 1;
	}
	
	/* data offset random tests */
	hwdigest_offset = 0;
	data_offset = 64;
	size = 128;

	for(j = 0; j < 10; j++){
	  data_offset = rand() & 0x00017fc0;
	  if( data_offset + size > 0x00017fc0 ){
	    data_offset -= size;
	  }
	  if(data_offset < 64){
	    data_offset = 64;
	  }
	  
	  get_random_data(data, size);

	  pass = sha_check(data, data_offset, size, hwdigest_offset, chaining, 0x1);

	  if( pass == 0){
	    printf("data-offset randoms test: SHA verify PASS\n");
	  }
	  else if (pass == -1){
	    printf("data-offset randoms test: SHA verify FAIL\n");
	  }
	  else if (pass == -2){
	    printf("data-offset randoms test: SHA verify FAIL in data\n");
	  }
	  

	}
	
	  
	
	/* hwdigest walk through test */
	data_offset = 0;

	size = 64;
	hwdigest_offset = 64;
	
	for(j =0; j < 10; j++){	  
	  printf("size = %d\n", size);

	  get_random_data(data, size);

	  pass = sha_check(data, data_offset, size, hwdigest_offset, chaining, 0x1);

	  if( pass == 0){
	    printf("digest-offset  walk through test: SHA verify PASS\n");
	  }
	  else if (pass == -1){
	    printf("digest-offset walk-through test: SHA verify FAIL\n");
	  }
	  else if (pass == -2){
	    printf("digest-offset walk-through test: SHA verify FAIL in data\n");
	  }
	  

	  /* for hwdigest offset walk through test */
	  hwdigest_offset = hwdigest_offset << 1;
	}
	
	/* hwdigest randoms test */
	data_offset = 0;

	size = 64;
	hwdigest_offset = 64;
	
	for(j =0; j < 10; j++){	  
	  
	  if( hwdigest_offset + 20 > 0x00017ff0 ){
	    hwdigest_offset -= 20;
	  }
	  if(hwdigest_offset < 64){
	    hwdigest_offset = 64;
	  }
	  get_random_data(data, size);

	  pass = sha_check(data, data_offset, size, hwdigest_offset, chaining, 0x1);

	  if( pass == 0){
	    printf("hwdigest-offset randoms test: SHA verify PASS\n");
	  }
	  else if (pass == -1){
	    printf("hwdigest-offset randoms test: SHA verify FAIL\n");
	  }
	  else if (pass == -2){
	    printf("digest-offset randoms test: SHA verify FAIL in data\n");
	  }
	  
	}
	
	
	/* save, negative test: set save = 0 and check that hwdigest is not
	 written */
	data_offset = 64;

	size = 64;
	hwdigest_offset = 0;
	
	for(j =0; j < 10; j++){	  
	  printf("size = %d\n", size);

	  get_random_data(data, size);

	  pass = sha_check(data, data_offset, size, hwdigest_offset, chaining, 0x0);

	  if( pass == -1){
	    printf("data-offset walk through neg test: SHA verify PASS\n");
	  }
	  else if (pass == 0){
	    printf("data-offset walk-through neg test: SHA verify FAIL\n");
	  }
	  else if (pass == -2){
	    printf("data-offset  walk-through neg test: SHA verify FAIL in data\n");
	  }
	  

	  /* for data offset walk through test */
	  data_offset = data_offset << 1;
	}
	
	/* chaining  test : save =1 each time, split into two */
	
	
	/* size: random tests */
	data_offset = 64;
	hwdigest_offset = 0; 
	
	for(j =0; j < 5; j++){	
	  
	  size = (rand()& 0x3f80);
	  /* make sure entire data is in GI_SRAM */
	  if( data_offset + size > 0x00017ff0 ){
	    data_offset -= size;
	  }
	  if(data_offset < 64){
	    data_offset = 64;
	  }
	  
	  get_random_data(data, size);

	  pass = sha_check(data, data_offset, size, hwdigest_offset, 0x1, 0x1);

	  if( pass == 0){
	    printf("Chaining test: SHA verify PASS\n");
	  }
	  else if (pass == -1){
	    printf("Chaining test: SHA verify FAIL\n");
	  }
	  else if (pass == -2){
	    printf("Chaining test: SHA verify FAIL in data\n");
	  }
	
	}


	
	  /* Close persistence connection */
	do_keep_alive_socket(0, 1);
	if (quit) IO_QUIT();

	return 0;
}
