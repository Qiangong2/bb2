/* iosim test sample code
    
   :set tabstop=4
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <unistd.h>
#include <netinet/in.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <getopt.h>
#include "iosim.h"
#include "utils.h"

#include <bsl.h>

#include <gi.h>
#include "../../lib/sechw/sechw.h"

#include <sha1.h>



/* test routines : -1 is fail, 0 is pass */
int do_register_tests(){
  u32 val, val_read, pass;
  int i;
  pass = 0;

  /* MC_BUF_REG */
  for(i=0; i< 50; i++){
    val = rand() & GI_MC_BUF_OFF_MASK;
    IO_WRITE(GI_MC_BUF_REG, val);
    val_read = IO_READ(GI_MC_BUF_REG);
    if(val  != val_read){
      printf("reg = %08x val = %08x val_read = %08x\n", GI_MC_BUF_REG, val, val_read);
      pass = -1;
    }
  }
	
  /* register tests: bit walk-throughs */
	
  val = 0x40; /* shift this bit through */
       
  for(i=0; i< 10; i++){
    val = val << 1;
    IO_WRITE(GI_MC_BUF_REG, val);
    val_read = IO_READ(GI_MC_BUF_REG);
    if(val != val_read){
      pass = -1;
    }	  
  }

  /* MC_ADDR */
  for(i=0; i< 50; i++){
    val = rand() & GI_MC_ADDR_MEM_MASK;
    IO_WRITE(GI_MC_ADDR_REG, val);
    val_read = IO_READ(GI_MC_ADDR_REG);
    if(val  != val_read){
      printf("reg = %08x val = %08x val_read = %08x\n", GI_MC_ADDR_REG, val, val_read);
      pass = -1;
    }
  }
	
  /* register tests: bit walk-throughs */
	
  val = 0x20; /* shift this bit through */
       
  for(i=0; i< 22; i++){
    val = val << 1;
    IO_WRITE(GI_MC_ADDR_REG, val);
    val_read = IO_READ(GI_MC_ADDR_REG);
    if(val != val_read){
      pass = -1;
    }	  
  }

  
  return pass;
}


u32 get_checksum(u32 start_addr, u32 region_size, u32 data_offset, u32 size, u32 hwdigest_offset, u32 digest_size){
  u32 checksum, val;
  int i;
  checksum = 0;
  for(i=start_addr; i < start_addr + region_size; i= i+4){
    /* exclude the two regions */
    if((i >= start_addr + data_offset) &&
       ( i < start_addr + data_offset + size)){
      ;
    }
    else if((i >= start_addr + hwdigest_offset) &&
       ( i < start_addr + hwdigest_offset + digest_size)){
      ;
    }
    else{ 
      val = IO_READ(i);
      checksum = checksum ^ val;
    }
  }
  return checksum;
}
    

#define MAX_DATA_SIZE ((100*1024)/4)

/* 0 is pass, -1 is fail with respect to software model, -2: data lost
 */
int mc_sha_check(u32 *data, u32 data_offset, u32 size, u32 hwdigest_offset, u32 chaining, u32 drop){

  int k, i;
  int pass = 0;
  u32 hwdigest[5];
  int result = 0;
  SHA1Context sha;
  u32 data_swap[MAX_DATA_SIZE];
  
  u32 swdigest[5];
  

  if(((chaining == 0x1) && (size % 128 == 0))&&(drop ==0)){
    result = hw_mc_sha_compute(data, data_offset, size/2, hwdigest, hwdigest_offset, 0x0, drop, 0);
    result = hw_mc_sha_compute(data + size/2/4, data_offset + size/2, size/2, hwdigest, hwdigest_offset, 0x1, drop, 0);
  }
    
    
  else {
    if(drop ==0){
      printf("came here \n");
      result = hw_mc_sha_compute(data, data_offset, size, hwdigest, hwdigest_offset, 0x0, drop, 0);
    }
    else if(drop == 1){
      /* treat as drop = 0*/
      result = hw_mc_sha_compute(data, data_offset, size, hwdigest, hwdigest_offset, 0x0, drop, 0);
    }
    else if(drop == 2){
      result = hw_mc_sha_compute(data + 16, data_offset, size - 64, hwdigest, hwdigest_offset, 0x0, 02, data);
    }
    else if(drop = 3){
      result = hw_mc_sha_compute(data + 32, data_offset, size - 128, hwdigest, hwdigest_offset, 0x0, 03, data);
    }

    if( result != 0){
      printf("timed out!\n");
      return result;
    }
  }
    
 

  /* check against software model */
  
  k= 0;
  for(i=0; i< size; i= i+4){
    data_swap[k] = htonl(data[k]);
    k++;
  }
  
  
  SHA1Reset(&sha);
  SHA1SetNoPadding(&sha);
  SHA1Input(&sha, (u8 *) data_swap, size);
  SHA1Result(&sha, (u8 *) swdigest);
  
  /* compare */
  pass = 0;
  for(i =0; i< 5; i++){
    printf("sw digest = %08x hw digest = %08x  \n", swdigest[i], hwdigest[i]);
    if(htonl(hwdigest[i]) != swdigest[i]){
      pass = -1;
    }
  }
  
  return pass;
}



int sw_hmac_compute(u32 *data, u32 data_offset, u32 *key, u32 keylen, u32 size, u32 mcbuf_offset, u32 keystore_offset, u32 *sw_hmac){
  int k, i, pass;
  u32 ipad[16];
  u32 opad[16];
  SHA1Context sha;
  u32 swdigest[32];
  /* create data for first hash */
  u32 newdata[MAX_DATA_SIZE + 16];
   for(i=0; i< 16; i++){
    ipad[i] = 0x36363636;
    opad[i] = 0x5c5c5c5c;
  }

  for(i=0; i < keylen/4; i++){
    ipad[i] = ipad[i] ^ key[i];
    opad[i] = opad[i] ^ key[i];
  }

  /* copy first 16 words from k xor ipad */
  for(i =0; i < 16; i++){
    newdata[i] = htonl(ipad[i]);
  }
  
  k=16; /* 16*4 = 64 bytes */
  for(i=0; i< size; i= i+4){
    newdata[k] = htonl(data[k-16]);
    k++;
  }
  
  
  SHA1Reset(&sha);
  SHA1SetNoPadding(&sha);
  SHA1Input(&sha, (u8 *) newdata, size + 64);
  SHA1Result(&sha, (u8 *) swdigest);
  
  /* create newdata again */
  for(i=0; i< 32; i++){
    newdata[i] = 0;
  }
  for(i =0; i < 16; i++){
    newdata[i] = htonl(opad[i]);
  }
  for(i = 16; i < 21; i++){
    newdata[i] = swdigest[i-16];
  }
  newdata[21] = htonl(0x80000000);
  /* add the length in bits : 512 + 160 */
  newdata[31] = htonl(0x2a0);

  /* let the software pad and determine the hash */
  SHA1Reset(&sha);
  SHA1SetNoPadding(&sha);
  SHA1Input(&sha, (u8 *) newdata, 128);
  SHA1Result(&sha, (u8 *) sw_hmac);
    
  return pass;
}


/* size in bytes */
void get_random_data(u32 *data, u32 size){
  int k;
  	  
  /* write data */
  for(k=0; k < size/4; k++){
    data[k] = rand();
  }
}

void fill_random_data(u32 start_addr, u32 size){
  int i;
  for(i=start_addr; i < start_addr + size; i= i+4){
    IO_WRITE(i, rand());
  }
}
  


#define MAX_DATA_SIZE ((100*1024)/4)

/* 0 is pass, -1 is encrypt failed, -2 is decrypt failed 
 */
int mc_enc_dec_check(u32 *iv, u32 iv_offset, u32 *key, u32 *data, u32 data_offset, u32 dmabuf_offset, u32 *dataenc_hw, u32 size,  u32 chaining){
  u32 key_swap[4]; 
  u32 iv_swap[4];
  int i, k;
  unsigned long datadec[MAX_DATA_SIZE], datares[MAX_DATA_SIZE];
  unsigned long dataenc[MAX_DATA_SIZE];
  int pass = 0;
  

  if((chaining == 1)&&((size & 0x10)==0)){ /* make sure its a multiple of
					      32 */
	  /* split for chaining */
    hw_mc_encrypt(iv, iv_offset, key, data, data_offset, dmabuf_offset, dataenc_hw, size/2, 0x0);
    hw_mc_encrypt(iv, iv_offset, key, data + size/2/4, data_offset + size/2, dmabuf_offset, dataenc_hw + size/2/4, size/2, 0x1);
  }
  else{
    hw_mc_encrypt(iv, iv_offset, key, data, data_offset, dmabuf_offset, dataenc_hw, size, 0x0);
 }
 

	  /* check against software model */
  key_swap[0] = htonl(key[0]);
  key_swap[1] = htonl(key[1]);
  key_swap[2] = htonl(key[2]);
  key_swap[3] = htonl(key[3]);

  iv_swap[0] = htonl(iv[0]);
  iv_swap[1] = htonl(iv[1]);
  iv_swap[2] = htonl(iv[2]);
  iv_swap[3] = htonl(iv[3]);

  k= 0;
  for(i=0; i< size; i= i+4){
    data[k] = htonl(data[k]); k++;
  }
  aes_SwEncrypt((u8 *) key_swap, (u8 *) iv_swap, (u8 *)data, size, (u8 *)dataenc);
  
	
  /* compare */
  pass = 0;
  for(i =0; i< size/4; i++){
    if(htonl(dataenc[i]) != dataenc_hw[i]){
      
      pass = -1;
      return pass;
    }
  }

  /* decrypt */
  if((chaining == 1)&&((size & 0x10)==0)){ 
    /* make sure its a multiple of 32 */
  
    hw_mc_decrypt(iv, iv_offset, key, dataenc_hw, data_offset, dmabuf_offset, datadec, size/2, 0x0, 0);
    hw_mc_decrypt(iv, iv_offset, key, dataenc_hw + size/2/4, data_offset + size/2, dmabuf_offset, datadec + size/2/4, size/2, 0x1, 0);
  }
  else {
    hw_mc_decrypt(iv, iv_offset, key, dataenc_hw, data_offset, dmabuf_offset, datadec, size, 0x0, 0);
  }

  aes_SwDecrypt((u8 *) key_swap, (u8 *) iv_swap, (u8 *)dataenc, size, (u8 *)datares);
  for(i=0; i< size/4; i++){
    if(datadec[i] != htonl(datares[i])){
      pass = -2;
    }
  }

  return pass;
}



/* 0 is pass, -1 is encrypt failed, -2 is decrypt failed 
 */
int dec_check(u32 *iv, u32 iv_offset, u32 *key, u32 *data, u32 data_offset, u32 *dataenc_hw, u32 size,  u32 chaining, u32 *key1){
  u32 key_swap[4]; 
  u32 iv_swap[4];
  int i;
  unsigned long datadec[MAX_DATA_SIZE], datares[MAX_DATA_SIZE];
  unsigned long dataenc[MAX_DATA_SIZE];
  int pass = 0;
  
   
	  /* check against software model */
  key_swap[0] = htonl(key[0]);
  key_swap[1] = htonl(key[1]);
  key_swap[2] = htonl(key[2]);
  key_swap[3] = htonl(key[3]);

  iv_swap[0] = htonl(iv[0]);
  iv_swap[1] = htonl(iv[1]);
  iv_swap[2] = htonl(iv[2]);
  iv_swap[3] = htonl(iv[3]);


  for(i=0; i< size/4; i++){
    dataenc[i] = htonl(data[i]);
  }
	  
  /* decrypt */
  if((chaining == 1)&&((size & 0x10)==0)){ 
    /* make sure its a multiple of 32 */
  
    hw_aes_decrypt(iv, iv_offset, key, dataenc_hw, data_offset, datadec, size/2, 0x0, key1);
    hw_aes_decrypt(iv, iv_offset, key, dataenc_hw + size/2/4, data_offset + size/2, datadec + size/2/4, size/2, 0x1, key1);
  }
  else {
    
    hw_aes_decrypt(iv, iv_offset, key, data, data_offset, datadec, size, 0x0, key1);
  }
  
  aes_SwDecrypt((u8 *) key_swap, (u8 *) iv_swap, (u8 *)dataenc, size, (u8 *)datares);
  for(i=0; i< size/4; i++){
    if(datadec[i] != htonl(datares[i])){
      pass = -2;
    }
  }
  
  return pass;
}


/* 0 is pass, -1 is encrypt failed, -2 is decrypt failed 
 */
int enc_check(u32 *iv, u32 iv_offset, u32 *key, u32 *data, u32 data_offset, u32 *dataenc_hw, u32 size,  u32 chaining){
  u32 key_swap[4]; 
  u32 iv_swap[4];
  int k, i;
  unsigned long data_swap[MAX_DATA_SIZE], datares[MAX_DATA_SIZE], dataenc[MAX_DATA_SIZE];

  int pass = 0;
  
   
	  /* check against software model */
  key_swap[0] = htonl(key[0]);
  key_swap[1] = htonl(key[1]);
  key_swap[2] = htonl(key[2]);
  key_swap[3] = htonl(key[3]);

  iv_swap[0] = htonl(iv[0]);
  iv_swap[1] = htonl(iv[1]);
  iv_swap[2] = htonl(iv[2]);
  iv_swap[3] = htonl(iv[3]);


  for(i=0; i< size/4; i++){
    data_swap[i] = htonl(data[i]);
  }
	  
  /* decrypt */
  if((chaining == 1)&&((size & 0x10)==0)){ 
    /* make sure its a multiple of 32 */
  
    hw_aes_encrypt(iv, iv_offset, key, data, data_offset, dataenc, size/2, 0x0);
    hw_aes_encrypt(iv, iv_offset, key, data + size/2/4, data_offset + size/2, dataenc + size/2/4, size/2, 0x1);
  }
  else {
    
    hw_aes_encrypt(iv, iv_offset, key, data, data_offset, dataenc, size, 0x0);
  }
  
  aes_SwEncrypt((u8 *) key_swap, (u8 *) iv_swap, (u8 *)data_swap, size, (u8 *)datares);
  for(i=0; i< size/4; i++){
    if(dataenc[i] != htonl(datares[i])){
      pass = -2;
    }
  }
  
  return pass;
}

int sha_check(u32 *data, u32 data_offset, u32 size, u32 hwdigest_offset, u32 chaining, u32 save){

  int k, i;
  int pass = 0;
  u32 hwdigest[5];
  int result = 0;
  u32 data_copy[MAX_DATA_SIZE];
  SHA1Context sha;
  
  u32 swdigest[5];

  if((chaining == 0x1) && (size % 128 == 0)){
    result = hw_sha_compute(data, data_offset, size/2, hwdigest, hwdigest_offset, 0x0, save);
    result = hw_sha_compute(data + size/2/4, data_offset + size/2, size/2, hwdigest, hwdigest_offset, 0x1, save);
  }
    
    
  else {
    result = hw_sha_compute(data, data_offset, size, hwdigest, hwdigest_offset, 0x0, save);
    if( result != 0){
      printf("timed out!\n");
      return result;
    }
  }
  

  /* check against software model */
  
  k= 0;
  for(i=0; i< size; i= i+4){
    data_copy[k] = htonl(data[k]);
    k++;
  }
  
  
  SHA1Reset(&sha);
  SHA1SetNoPadding(&sha);
  SHA1Input(&sha, (u8 *) data_copy, size);
  SHA1Result(&sha, (u8 *) swdigest);
  
  /* compare */
  pass = 0;
  for(i =0; i< 5; i++){
    printf("sw digest = %08x hw digest = %08x  \n", swdigest[i], hwdigest[i]);
    if(htonl(hwdigest[i]) != swdigest[i]){
      pass = -1;
    }
  }
  
  return pass;
}
  

int main(int argc, char **argv)
{
  int quit = 0, c;
  extern char *optarg;
  extern int optind, opterr, optopt;
  u32 data[MAX_DATA_SIZE];
  u32 hwdigest[5],swdigest[5], hw_hmac[5], sw_hmac[5];
  SHA1Context sha;
  int pass = 0;
  unsigned long key[100], iv[4], key1[4], key2[4];
  unsigned long key1_swap[4], iv_swap[4];
  unsigned long dataenc_hw[MAX_DATA_SIZE], datadec[MAX_DATA_SIZE];
  u32  iv_offset = 0, data_offset = 0;
  u32 size = 32;
  u32 chaining = 0;
  u32 hwdigest_offset =0;
  u32 keystore_offset = 0;
  int k = 0, i = 0, val = 0;
  int drop = 0;
  int sweep;
  unsigned long data_swap[MAX_DATA_SIZE], dataenc[MAX_DATA_SIZE], datares[MAX_DATA_SIZE];
  int count;
  
  
  /* enter secure mode */
  srand(getpid());
  while ((c = getopt(argc, argv, "q")) != EOF) {
    switch (c) {
    case 'q':
      quit = 1;
      break;
    case 'h':
    case '?': 
    default:
      /* Your help function */
      break;
    }
  }
  
  if (IpcInit()) 	return -1;		
  
  /*  Make persistence connection */
  do_keep_alive_socket(1, 1);
  
  /* fill GI SRAM with random data before starting */
  
#if 0
  fill_random_data(GI_SRAM_START, 96*1024);
  
#endif
  

	/* register tests: randoms */
  pass = do_register_tests();

  if ( pass == 0){
    printf("Register read/write tests PASS\n");
  }
  else {
    printf("Register read/write tests FAIL\n");
  }
  
  ipc_set_data(BD_SYS_DMA_SETUP, 1);
  ipc_set_data(BD_SYS_DMA_CMD_MON, 1);
  

  /* trying hmac test */
  iv_offset = 2048;
  data_offset = 0;
  size = 128;
  chaining = 0;
  hwdigest_offset = 0x80;
  drop = 0;
  count = 0;
  size = 64;
  get_random_data(data, size);
  keystore_offset = 4*1024;
  get_random_data(key, 16);
  
  mc_hmac_compute(data, data_offset, key, 16, size, hwdigest_offset, keystore_offset, hw_hmac);
  
  sw_hmac_compute(data, data_offset, key, 16, size, hwdigest_offset, keystore_offset, sw_hmac);
  
  pass = 0;
  for(i =0; i < 5; i++){
    if(hw_hmac[i] != htonl(sw_hmac[i])){
      pass = -1;
    }
  }
  if(pass == 0){
    printf("HMAC test PASS\n");
  }
  else{
    printf("HMAC test FAIL\n");
  }
  

	iv_offset = 2048;
	data_offset = 0;
	size = 128;
	chaining = 0;
	hwdigest_offset = 0x80;
	drop = 0;
	count = 0;
	/* for(size = 64; size < 16*4096; size = size + 64){*/
	for(size = 64; size < 64*4; size = size + 64){
	  
	  get_random_data(data, size);
	  pass = mc_sha_check(data, data_offset, size, hwdigest_offset, chaining, 0);
	
	
	  if( pass == 0){
	    printf("Size %d walk-through test: MC-SHA verify PASS\n", size);
	  }
	  else if (pass == -1){
	    printf("Size %d walk-through test: MC-SHA verify FAIL\n", size);
	  }
	  else if (pass == -2){
	    printf("Size %d walk-through test: MC-SHA verify FAIL in data\n", size);
	  }


	  /* test of sha : test of drop= 2 */

	  get_random_data(data, size);
	  
	  pass = mc_sha_check(data, data_offset, size, hwdigest_offset, chaining, 2);
	
	
	  if( pass == 0){
	    printf("Size %d walk through test: MC-SHA drop2 verify PASS\n", size);
	  }
	  else if (pass == -1){
	    printf("Size %d walk-through test: MC-SHA drop2 FAIL\n", size);
	  }
	  else if (pass == -2){
	    printf("Size %d walk-through test: MC-SHA drop2 FAIL in data\n", size);
	  }

	  if(size > 64){
	    get_random_data(data, size);
	    pass = mc_sha_check(data, data_offset, size, hwdigest_offset, chaining, 03);
	
	
	    if( pass == 0){
	      printf("Size %d walk through test: MC-SHA drop3 verify PASS\n", size);
	    }
	    else if (pass == -1){
	      printf("Size %d walk-through test: MC-SHA drop3 FAIL\n", size);
	    }
	    else if (pass == -2){
	      printf("Size %d walk-through test: MC-SHA drop3 FAIL in data\n", size);
	    }
	  }

	  count++;
	  

	}
        
	/* test of encryption/decryption */

	
	iv_offset = 0;
	data_offset = 0;
	size = 512;
	chaining = 0;
	hwdigest_offset = 2048;

	get_random_data(data, size);
	
	get_random_data(key, 16);
	get_random_data(iv, 16);


	pass = mc_enc_dec_check(iv, iv_offset, key, data, data_offset, hwdigest_offset, dataenc_hw, size,  chaining);
	
	if( pass == 0){
	  printf("MC test: Encrypt Decrypt PASS\n");
	}
	else if (pass == -1){
	  printf("MC test: Encrypt FAIL\n");
	}
	else if (pass == -2){
	  printf("MC test: Decrypt FAIL\n");
	}
	


	/* test of stall encryption and switch to SHA or decryption */
	size = 512;
	get_random_data(data, 512);
	for(sweep = 0; sweep < 30; sweep++){
	
	  get_random_data(key1, 16);
	  get_random_data(key2, 16);
	  get_random_data(iv, 16);

	
	  iv_offset = 7*1024;
	  data_offset = 8*1024;
	  chaining = 0;
	  hwdigest_offset = 10*1024;

	
	/* start encryption and return */
	  /* write data into dram */
	  k = 0;
	  for(i=0; i < size; i = i+ 4){
	    IO_WRITE(data_offset + i, data[k]);
	    k++;
	  }

	  hw_mc_encrypt_iv_setup(iv, hwdigest_offset, iv_offset);
	  hw_mc_encrypt_key_setup(key1);
	  hw_mc_encrypt_start( size, data_offset, chaining);
	
	  /* just testing */

	  for(i=0; i< sweep; i++){
#define GI_MC_AES_POLL_CYCLES 50
	    IO_STALL(GI_MC_AES_POLL_CYCLES);
	    printf("doing encryption before stalling\n");
	  }

	
	  if((IO_READ(GI_MC_CTRL_REG) & 0x80000000 )!= 0){
	    printf(" Did not complete encryption: going to stall... \n");
	  }


	/* do another operation on the aes/sha  engine */

	  iv_offset = 0;
	  data_offset = 2*1024;
	  size = 512;
	  chaining = 0;
	  hwdigest_offset = 4*1024;
	
	  if((rand() % 2 ) == 0){
	
	  pass = dec_check(iv, iv_offset, key2, data, data_offset, dataenc_hw, size,  chaining, key1);
	  if( pass == 0){
	    printf("Decrypt inbetween PASS\n");
	  }
	  else {
	    printf("Decrypt inbetween FAIL\n");
	  }
	  
	}
	else{
	


	  pass = sha_check(data, data_offset, size, hwdigest_offset, chaining, 0x1);

	  if( pass == 0){
	    printf("SHA inbetween PASS\n");
	  }
	  else if (pass == -1){
	    printf("SHA inbetween FAIL\n");
	  }
	  else if (pass == -2){
	    printf("SHA inbetween FAIL in data\n");
	  }


	  
	    }
	  
	  

	  while ((IO_READ(GI_MC_CTRL_REG) & 0x80000000) != 0x0){
	    IO_STALL(GI_MC_AES_POLL_CYCLES);
	    printf("doing encryption \n");
	}
	  
	  data_offset = 8*1024;
	  size = 512;
	  chaining = 0;
	  

	  k = 0;
	  for(i=0; i < size; i = i+ 4){
	    val = IO_READ(data_offset + i);
	    dataenc[k] = val; k++;
	  }

	  /* check result */
	
	/* swap the key1 and iv */
	  key1_swap[0] = htonl(key1[0]);
	  key1_swap[1] = htonl(key1[1]);
	  key1_swap[2] = htonl(key1[2]);
	  key1_swap[3] = htonl(key1[3]);
	  
	  iv_swap[0] = htonl(iv[0]);
	  iv_swap[1] = htonl(iv[1]);
	  iv_swap[2] = htonl(iv[2]);
	  iv_swap[3] = htonl(iv[3]);
	  
	
	  k= 0;
	  for(i=0; i< size; i= i+4){
	    data_swap[k] = htonl(data[k]); k++;
	  }
	  pass = 0;
	  aes_SwEncrypt((u8 *) key1_swap, (u8 *) iv_swap, (u8 *)data_swap, size, (u8 *)datares);
	  for(i=0; i< size/4; i++){
	    if(dataenc[i] != htonl(datares[i])){
	      printf("%d dataenc =  %08x datares = %08x\n", i, dataenc[i], datares[i]);
	      pass = -2;
	    }
	  }

	  if(pass ==0){
	    printf("The interrupted encryption PASSed, sweep = %d\n", sweep);
	  }
	  else{
	    printf("The interrupted encryption FAILed, sweep = %d\n", sweep);
	  }
	}
  

	/* SHA operation, then decryption */
	

	for(sweep = 0; sweep < 20; sweep++){

	  data_offset = 512;
	  size = 512;
	  chaining = 0;
	  hwdigest_offset = 2048;
	  get_random_data(data, size);
	
	  /* start SHA and return */
	  /* write data into dram */
	  k = 0;
	  for(i=0; i < size; i = i+ 4){
	    IO_WRITE(data_offset + i, data[k]);
	    k++;
	  }

	  hw_mc_sha_compute_setup(data, data_offset, hwdigest_offset, size, 
				0, 0);
	  hw_mc_sha_compute_input(size, chaining, 0);
	  
#define GI_MC_SHA_POLL_CYCLES 350
	  for(i=0; i< sweep; i++){
	    IO_STALL(GI_MC_SHA_POLL_CYCLES);
	    printf("doing encryption before stalling\n");
	  }

	
	  if((IO_READ(GI_MC_CTRL_REG) & 0x80000000 )!= 0){
	    printf(" Did not complete SHA: going to start decryption... \n");
	  }

	/* do another operation on the aes engine */
	  
	  iv_offset = 512;
	  data_offset = 4096;
	  size = 512;
	  chaining = 0;
	  get_random_data(key, 16);
	
	  
	  if(rand() % 2 == 1){ /* alternate encrypt and decrypt */
	    pass = dec_check(iv, iv_offset, key, data, data_offset, dataenc_hw, size,  chaining, 0);
	  }
	  else{
	    pass = enc_check(iv, iv_offset, key, data, data_offset, dataenc_hw, size,  chaining);
	  }
	  if( pass == 0){
	    printf("Decrypt inbetween PASS\n");
	  }
	  else {
	    printf("Decrypt inbetween FAIL\n");
	  }

	  hw_mc_sha_compute_result(hwdigest, hwdigest_offset, size);
	  data_offset = 512;
	  size = 512;
	  chaining = 0;
	

	/* check result */
		
	  k= 0;
	  for(i=0; i< size; i= i+4){
	    data_swap[k] = htonl(data[k]); k++;
	  }
	
	  SHA1Reset(&sha);
	  SHA1SetNoPadding(&sha);
	  SHA1Input(&sha, (u8 *) data_swap, size);
	  SHA1Result(&sha, (u8 *) swdigest);
	  
	  pass = 0;
	  for(i=0; i< 5; i++){
	    if(hwdigest[i] != htonl(swdigest[i])){
	      printf("%d hwdigest =  %08x swdigest = %08x\n", i, hwdigest[i], swdigest[i]);
	      pass = -2;
	    }
	  }
	  
	  if(pass ==0){
	    printf("The interrupted SHA PASSed, sweep = %d\n", sweep);
	  }
	  else{
	    printf("The interrupted SHA FAILed, sweep = %d\n", sweep);
	  }
	}
  

	/* MC decryption + sha and encryption in between */
	size = 512;
	get_random_data(data, 512);
	for(sweep = 0; sweep < 30; sweep++){
	
	  get_random_data(key1, 16);
	  get_random_data(key2, 16);
	  get_random_data(iv, 16);

	
	  iv_offset = 7*1024;
	  data_offset = 8*1024;
	  chaining = 0;
	  hwdigest_offset = 10*1024;

	
	/* start encryption and return */
	  /* write data into dram */
	  k = 0;
	  for(i=0; i < size; i = i+ 4){
	    IO_WRITE(data_offset + i, data[k]);
	    k++;
	  }

	  hw_mc_decrypt_iv_setup(iv, hwdigest_offset, iv_offset);
	  hw_mc_decrypt_key_setup(key1, 0);
	  hw_mc_decrypt_start( size, data_offset, chaining);
	
	  /* just testing */

	  for(i=0; i< sweep; i++){
#define GI_MC_AES_POLL_CYCLES 50
	    IO_STALL(GI_MC_AES_POLL_CYCLES);
	    printf("doing decryption before interrupting\n");
	  }

	
	  if((IO_READ(GI_MC_CTRL_REG) & 0x80000000 )!= 0){
	    printf(" Did not complete decryption: going to interrupt... \n");
	  }


	/* do another operation on the aes/sha  engine */

	  iv_offset = 0;
	  data_offset = 2*1024;
	  size = 512;
	  chaining = 0;
	  hwdigest_offset = 4*1024;
	
	  if((rand() % 2 ) == 0){
	
	    pass = enc_check(iv, iv_offset, key2, data, data_offset, dataenc_hw, size,  chaining);
	  
	    if( pass == 0){
	    printf("Encrypt inbetween PASS\n");
	    }
	    else {
	      printf("Encrypt inbetween FAIL\n");
	    }
	  
	  }
	  else{

	    pass = sha_check(data, data_offset, size, hwdigest_offset, chaining, 0x1);

	    if( pass == 0){
	      printf("SHA inbetween PASS\n");
	    }
	    else if (pass == -1){
	      printf("SHA inbetween FAIL\n");
	    }
	    else if (pass == -2){
	      printf("SHA inbetween FAIL in data\n");
	    }	  
	  }
	  
	  while ((IO_READ(GI_MC_CTRL_REG) & 0x80000000) != 0x0){
	    IO_STALL(GI_MC_AES_POLL_CYCLES);
	    printf("doing decryption \n");
	  }
	  
	  data_offset = 8*1024;
	  size = 512;
	  chaining = 0;
	  

	  k = 0;
	  for(i=0; i < size; i = i+ 4){
	    val = IO_READ(data_offset + i);
	    datadec[k] = val; k++;
	  }

	  /* check result */
	
	/* swap the key1 and iv */
	  key1_swap[0] = htonl(key1[0]);
	  key1_swap[1] = htonl(key1[1]);
	  key1_swap[2] = htonl(key1[2]);
	  key1_swap[3] = htonl(key1[3]);
	  
	  iv_swap[0] = htonl(iv[0]);
	  iv_swap[1] = htonl(iv[1]);
	  iv_swap[2] = htonl(iv[2]);
	  iv_swap[3] = htonl(iv[3]);
	  
	
	  k= 0;
	  for(i=0; i< size; i= i+4){
	    data_swap[k] = htonl(data[k]); k++;
	  }
	  pass = 0;
	  aes_SwDecrypt((u8 *) key1_swap, (u8 *) iv_swap, (u8 *)data_swap, size, (u8 *)datares);
	  for(i=0; i< size/4; i++){
	    if(datadec[i] != htonl(datares[i])){
	      printf("%d dataenc =  %08x datares = %08x\n", i, dataenc[i], datares[i]);
	      pass = -2;
	    }
	  }

	  if(pass ==0){
	    printf("The interrupted decryption PASSed, sweep = %d\n", sweep);
	  }
	  else{
	    printf("The interrupted decryption FAILed, sweep = %d\n", sweep);
	  }
	}
  

	ipc_set_data(BD_SYS_DMA_SETUP, 0);
	ipc_set_data(BD_SYS_DMA_CMD_MON, 0);

	  /* Close persistent connection */
	do_keep_alive_socket(0, 1);
	if (quit) IO_QUIT();

	return 0;
}
