/* iosim test sample code
    
   :set tabstop=4
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <unistd.h>
#include <netinet/in.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <getopt.h>
#include "iosim.h"
#include "utils.h"
#include "aes.h"
#include <bsl.h>

#include <gi.h>
#include <sechw.h>


  
  

/* test routines : -1 is fail, 0 is pass */
int do_register_tests(){
  u32 val, val_read, pass;
  int i;
	pass = 0;
	for(i=0; i< 50; i++){
	  val = rand() & GI_AESD_IV_IV_OFF_MASK ;
	  IO_WRITE(GI_AESD_IV_REG, val);
	  val_read = IO_READ(GI_AESD_IV_REG);
	  if(val  != val_read){
	    printf("reading %08x val = %08x val_read = %08x\n", GI_AESD_IV_REG, (val & GI_AESD_IV_IV_OFF_MASK), val_read);
	    pass = -1;
	  }
	  IO_WRITE(GI_AESE_IV_REG,(val & GI_AESE_IV_IV_OFF_MASK ));
	  val_read = IO_READ(GI_AESE_IV_REG);
	  if(val != val_read){
	    printf("val = %08x val_read = %08x\n", (val & GI_AESE_IV_IV_OFF_MASK), val_read);
	    pass = -1;
	  }
	  
	}
	
	/* register tests: bit walk-throughs */
	
	val = 0x8; /* shift this bit through */
       
	for(i=0; i< 14; i++){
	  val = val << 1;
	  IO_WRITE(GI_AESD_IV_REG,(val & GI_AESD_IV_IV_OFF_MASK ));
	  val_read = IO_READ(GI_AESD_IV_REG);
	  if((val & GI_AESD_IV_IV_OFF_MASK) != val_read){
	    printf("val = %08x val_read = %08x\n", (val & GI_AESD_IV_IV_OFF_MASK), val_read);
	    pass = -1;
	  }
	  IO_WRITE(GI_AESE_IV_REG,(val & GI_AESE_IV_IV_OFF_MASK ));
	  val_read = IO_READ(GI_AESE_IV_REG);
	  if((val & GI_AESE_IV_IV_OFF_MASK) != val_read){
	    printf("val = %08x val_read = %08x\n", (val & GI_AESE_IV_IV_OFF_MASK), val_read);
	    pass = -1;
	  }
	  
	}
	return pass;
}


#define MAX_DATA_SIZE ((100*1024)/4)

/* 0 is pass, -1 is encrypt failed, -2 is decrypt failed 
 */
int enc_dec_check(u32 *iv, u32 iv_offset, u32 *key, u32 *data, u32 data_offset, u32 *dataenc_hw, u32 size,  u32 chaining){
  u32 key_swap[4]; 
  u32 iv_swap[4];
  int k, i;
  unsigned long datadec[MAX_DATA_SIZE], datares[MAX_DATA_SIZE];
  unsigned long dataenc[MAX_DATA_SIZE];
  int pass = 0;
  
  if((chaining == 1)&&((size & 0x10)==0)){ /* make sure its a multiple of
					      32 */
	  /* split for chaining */
	  hw_aes_encrypt(iv, iv_offset, key, data, data_offset,dataenc_hw, size/2, 0x0);
	  hw_aes_encrypt(iv, iv_offset, key, data + size/2/4, data_offset + size/2,dataenc_hw + size/2/4, size/2, 0x1);
  }
  else{
    hw_aes_encrypt(iv, iv_offset, key, data, data_offset, dataenc_hw, size, 0x0);
  }
 
	  
	  /* check against software model */
	  key_swap[0] = htonl(key[0]);
	  key_swap[1] = htonl(key[1]);
	  key_swap[2] = htonl(key[2]);
	  key_swap[3] = htonl(key[3]);

	  iv_swap[0] = htonl(iv[0]);
	  iv_swap[1] = htonl(iv[1]);
	  iv_swap[2] = htonl(iv[2]);
	  iv_swap[3] = htonl(iv[3]);

	  k= 0;
	  for(i=0; i< size; i= i+4){
	    data[k] = htonl(data[k]); k++;
	  }
	  aes_SwEncrypt((u8 *) key_swap, (u8 *) iv_swap, (u8 *)data, size, (u8 *)dataenc);

	
	  /* compare */
	  pass = 0;
	  for(i =0; i< size/4; i++){
	    if(htonl(dataenc[i]) != dataenc_hw[i]){
	      pass = -1;
	    }
	  }


	  /* decrypt */
	  if((chaining == 1)&&((size & 0x10)==0)){ 
	    /* make sure its a multiple of 32 */
  
	    hw_aes_decrypt(iv, iv_offset, key, dataenc_hw, data_offset, datadec, size/2, 0x0, 0);
	    hw_aes_decrypt(iv, iv_offset, key, dataenc_hw + size/2/4, data_offset + size/2, datadec + size/2/4, size/2, 0x1, 0);
	  }
	  else {
	    hw_aes_decrypt(iv, iv_offset, key, dataenc_hw, data_offset, datadec, size, 0x0, 0);
	  }

	  aes_SwDecrypt((u8 *) key_swap, (u8 *) iv_swap, (u8 *)dataenc, size, (u8 *)datares);
	  for(i=0; i< size/4; i++){
	    if(datadec[i] != htonl(datares[i])){
	      pass = -2;
	    }
	  }

	  return pass;
}

void get_random_data(u32 *iv, u32 *key, u32 *data, u32 size){
  int k;
  
  for (k=0; k < 4; k++){
    iv[k] = rand();
  }

  /* init key */
  for(k=0; k < 4; k++){
    key[k] = rand();
  }

	  
  /* write data */
  for(k=0; k < size/4; k++){
    data[k] = rand();
  }
}


int main(int argc, char **argv)
{
	int quit = 0, c;
	int j;
	extern char *optarg;
	extern int optind, opterr, optopt;
	unsigned long key[4], iv[4];
	unsigned long data[MAX_DATA_SIZE], dataenc_hw[MAX_DATA_SIZE];
	int pass = 0;
	int iv_offset = 0, data_offset = 0;
	int size = 32;
	int chaining;
	u32 val, val_read;
	
	/* enter secure mode */
	srand(getpid());
	while ((c = getopt(argc, argv, "q")) != EOF) {
		switch (c) {
			case 'q':
				quit = 1;
				break;
			case 'h':
			case '?': 
			default:
				/* Your help function */
				break;
		}
	}

	if (IpcInit()) 	return -1;		
	


	  /*  Make persistence connection */
	do_keep_alive_socket(1, 1);
	
	/* register tests: randoms */
	pass = do_register_tests();
	
	if ( pass == 0){
	  printf("Register read/write tests PASS\n");
	}
	else {
	  printf("Register read/write tests FAIL\n");
	}
	
	
	/* size test */

	iv_offset = 0*1024;
	data_offset = 16;
	chaining = 0x0;

	size = (0x10 & 0x3ff0); /* actual data 16 bytes */
	
	for(j =0; j < 11; j++){	   /* go upto < 11 */
	  printf("size = %d\n", size);

	  get_random_data(iv, key, data, size);

	  pass = enc_dec_check(iv, iv_offset, key, data, data_offset, dataenc_hw, size,  chaining);
	  if( pass == 0){
	    printf("Size walk-through test: Encrypt Decrypt PASS\n");
	  }
	  else if (pass == -1){
	    printf("Size walk-through test: Encrypt FAIL\n");
	  }
	  else if (pass == -2){
	    printf("Size walk-through test: Decrypt FAIL\n");
	  }

	  /* for size walk through test: */
	  size = size << 1;
	  /* toggle chaining */
	  
	  if(chaining == 0){
	    chaining = 1;
	  }
	  else {
	    chaining = 0;
	  }
	  
	  
	}

	/* size: random tests */
	
	
	for(j =0; j < 5; j++){	  
	  
	  size = (rand()& 0x3ff0); /* actual data 16 bytes */
	  printf("size = %d\n", size);
	  if( data_offset + size > 0x00017ff0 ){
	    data_offset -= size;
	  }
	  printf("data_offset = %08x\n", data_offset);
	  if(data_offset < 16){
	    data_offset = 16;
	  }
	  get_random_data(iv, key, data, size);

	  pass = enc_dec_check(iv, iv_offset, key, data, data_offset, dataenc_hw, size,  chaining);
	  if( pass == 0){
	    printf("Size random test: Encrypt Decrypt PASS\n");
	  }
	  else if (pass == -1){
	    printf("Size random test: Encrypt FAIL\n");
	  }
	  else if (pass == -2){
	    printf("Size random test: Decrypt FAIL\n");
	  }

	  /* toggle chaining */
	  
	  if(chaining == 0){
	    chaining = 1;
	  }
	  else {
	    chaining = 0;
	  }
	  
	}

	/* iv offset test */
	iv_offset = 0x20; /* later switch and test 0 */
	data_offset = 0;
	chaining = 0;
	size = 16; /* random size*/
	
	while(iv_offset < 96*1024){
	  printf("iv offset = %08x\n", iv_offset);
	  
	  get_random_data(iv, key, data, size);
	  pass = enc_dec_check(iv, iv_offset, key, data, data_offset, dataenc_hw, size,  chaining);
	  if( pass == 0){
	    printf("IV walk through offset test: Encrypt Decrypt PASS\n");
	  }
	  else if (pass == -1){
	    printf("IV walk through offset test: Encrypt FAIL\n");
	  }
	  else if (pass == -2){
	    printf("IV walk through offset test: Decrypt FAIL\n");
	  }
	  
	  /* for iv walk through test */
	  
	  iv_offset = iv_offset <<1;
	  

	  /* toggle chaining */
	  
	  if( chaining == 0){
	    chaining = 1;
	  }
	  else {
	    chaining = 0;
	  }
	  
	}

	/* corner case: iv_offset = 0; */
	iv_offset = 0; /* later switch and test 0 */
	data_offset = 16;
	
	size = 16; 
	get_random_data(iv, key, data, size);

	pass = enc_dec_check(iv, iv_offset, key, data, data_offset, dataenc_hw, size,  0x0);
	if( pass == 0){
	  printf("IV walk-through offset test: Encrypt Decrypt PASS\n");
	}
	else if (pass == -1){
	  printf("IV walk-through offset test: Encrypt FAIL\n");
	  }
	else if (pass == -2){
	  printf("IV walk-through offset test: Decrypt FAIL\n");
	}
	  
	/* iv offset random test */
	
	data_offset = 0;
	chaining = 0;
	size = 16; 
	
	for(j = 0; j < 5; j++){
	  iv_offset = rand() & (0x00017ff0);
	  /* make sure entire IV is in GI_SRAM */
	  if( iv_offset == 0x00017ff0 ){
	    iv_offset -= 16;
	  }
	  get_random_data(iv, key, data, size);

	  pass = enc_dec_check(iv, iv_offset, key, data, data_offset, dataenc_hw, size,  chaining);
	  if( pass == 0){
	    printf("IV random offset test: Encrypt Decrypt PASS\n");
	  }
	  else if (pass == -1){
	    printf("IV random offset test: Encrypt FAIL\n");
	  }
	  else if (pass == -2){
	    printf("IV random offset test: Decrypt FAIL\n");
	  }
	  
	  /* toggle chaining */
	  
	  if( chaining == 0){
	    chaining = 1;
	  }
	  else {
	    chaining = 0;
	  }
	  
	}


	/* data offset walk through test */
	iv_offset = 0;
	data_offset = 16; /* test data_offset = 0 later */
	chaining = 0;
	size = 16;
	
	while(data_offset < 96*1024){

	  get_random_data(iv, key, data, size);

	  pass = enc_dec_check(iv, iv_offset, key, data, data_offset, dataenc_hw, size,  chaining);
	  if( pass == 0){
	    printf("Data Offset walk-through test: Encrypt Decrypt PASS\n");
	  }
	  else if (pass == -1){
	    printf("Data Offset walk-through test: Encrypt FAIL\n");
	  }
	  else if (pass == -2){
	    printf("Data Offset walk-through test: Decrypt FAIL\n");
	  }

	  /* for data offset walk through test */
	  data_offset = data_offset << 1;
	  /* toggle chaining */
	  
	  if(chaining == 0){
	    chaining = 1;
	  }
	  else {
	    chaining = 0;
	  }
	  

	}


	/* test corner case data_offset = 0 */
	iv_offset = 16;
	data_offset = 0;

	size = 16;
	
	get_random_data(iv, key, data, size);

	pass = enc_dec_check(iv, iv_offset, key, data, data_offset, dataenc_hw, size,  0x0);
	if( pass == 0){
	  printf("Data Offset walk-through test: Encrypt Decrypt PASS\n");
	}
	else if (pass == -1){
	  printf("Data Offset walk-through test: Encrypt FAIL\n");
	}
	else if (pass == -2){
	  printf("Data Offset walk-through test: Decrypt FAIL\n");
	}

	/* data offset random test */
		iv_offset = 0;
	data_offset = 16; /* test data_offset = 0 later */
	chaining = 0;
	size = 16;
	
	for(j=0; j< 5; j++){
	  data_offset = rand() & (0x00017ff0);
	  /* make sure entire data is in GI_SRAM */
	  if( data_offset + size > 0x00017ff0 ){
	    data_offset -= size;
	  }
	  if(data_offset < 16){
	    data_offset = 16;
	  }
	  get_random_data(iv, key, data, size);

	  pass = enc_dec_check(iv, iv_offset, key, data, data_offset, dataenc_hw, size,  chaining);
	  if( pass == 0){
	    printf("Data Offset random test: Encrypt Decrypt PASS\n");
	  }
	  else if (pass == -1){
	    printf("Data Offset random test: Encrypt FAIL\n");
	  }
	  else if (pass == -2){
	    printf("Data Offset random test: Decrypt FAIL\n");
	  }


	  /* toggle chaining */
	  
	  if(chaining == 0){
	    chaining = 1;
	  }
	  else {
	    chaining = 0;
	  }
	  
	}


	  /* Close persistence connection */
	do_keep_alive_socket(0, 1);
	if (quit) IO_QUIT();

	return 0;
}
