#ifndef __IOSIM_CMD
#define __IOSIM_CMD

char *skip_blank(char *str);
char *skip_nonblank(char *str);

unsigned int get_cmd_arg_num(char *str);
char *get_cmd_arg_str(char *str);

int single_read_rtl(char *cmd, int verbose);
int single_write_rtl(char *cmd, int verbose);
int block_read_rtl(char *cmd, int verbose);
int block_write_rtl(char *cmd, int verbose);

int compare_with_mask(char *cmd, int verbose);
int loop_until(char *cmd, int verbose);

void ipc_set_cmd(char *cmd, int verbose);
void ipc_get_cmd(char *cmd, int verbose);

int mem_write(char *cmd, int backdoor, int verbose);
int mem_read(char *cmd, int backdoor, int verbose);

void check_interrupt(char *cmd, int verbose);

#endif
