/* iosim 
   shell command interpreter 
   :set tabstop=4
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <unistd.h>
#include <netinet/in.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <readline/readline.h>
#include <readline/history.h>
#include "iosim.h"
#include "utils.h"
#include "cmd.h"

void Usage();
void Usage_set();
void Usage_get();

int main(int argc, char **argv)
{
	char *line=NULL, *expansion = NULL;
	int result;
	char *p;
	int loop = 1, verbose = 0;
	unsigned int num;
	char history_file[256];
	FILE *fp=NULL;

	if (getenv("HOME")) {
		strcpy(history_file, getenv("HOME"));
	} else {
		history_file[0] = '.';
		history_file[0] = '\0';
	}
	strcat(history_file, "/.bb2ipc_history");
	read_history(history_file);

	if (IpcInit()) return -1;
	for (;loop;) {
		if (line) free(line);
		if ((line = readline("> ")) == NULL) break;
		result = history_expand(line, &expansion);
		if (expansion) {
			free(line);
			line = expansion;
		}

		if (result < 0 || result == 2) {
			printf("%s \n", line);
			continue;
		}

		if ((p = strrchr(line, '\n'))) *p = '\0';
		for (p = line; *p && isspace(*p); ++p);
		if (*p) add_history(p);
		if (fp) fprintf(fp, "%s\n", p);

		num = get_cmd_arg_num(p+1);
		switch (p[0]) {
			case 'a': /* advance RTL # of clocks */
				if (verbose) printf("RTL advance %d cycles \n", num);
				IO_STALL(num);
				break;
			case 'c': /* compare */
				compare_with_mask(p, verbose);
				break;
			case 'd': /* dump control */
				if (verbose) printf("Turn RTL dump %s \n", num ? "on" : "off");
				IO_DUMP(num);
				break;
			case 'f': /* Record command to file */
				if (fp) fclose(fp);
				fp = NULL;
				p = get_cmd_arg_str(p+1);
				if (p[0] != '\0') {
					fp = fopen(p, "wb");
					if (fp != NULL) 
						printf("Start recording to %s\n", p);
				}
				break;
			case 'g': /* Ipc get command */
				ipc_get_cmd(p+1, verbose);
				break;
			case 'h':
				if (p[1] == 's') {
					Usage_set();
					break;
				}
				if (p[1] == 'g') {
					Usage_get();
					break;
				}
			case '?':
				Usage();
				break;
			case 'i': /* Print Msg in RTL log */
				if (verbose) printf("Display: %s \n", get_cmd_arg_str(p+1));
				displayMsg(get_cmd_arg_str(p+1));
				break;
			case 'I': /* Interrupt check */
				if (verbose) printf("Check interrupt \n");
				check_interrupt(p+1, verbose);	
				break;
			case 'k': /* persistence connection */
				if (verbose) printf("Keep socket %s \n", num ? "alive" : "off");
				do_keep_alive_socket(num, 1);
				break;
			case 'l': /* Turn on ipc log */
				if (verbose) printf("Turn ipc mon %s \n", num ? "on" : "off");
				IO_IPC_MON(num ? 1 : 0);	
				break;
			case 'm': /* memory Read/write */
				if (p[1] == 'w') mem_write(skip_nonblank(p), (p[2]=='b'), verbose);
				if (p[1] == 'r') mem_read(skip_nonblank(p), (p[2]=='b'), verbose);
				break;
			case 'p': /* ping command */
				if (FindServer()) 
					fprintf(stderr, "Error: Cannot talk to RTL\n");
				else
					printf("RTL is alive \n");
				break;
			case 'Q': /* quit RTL as well */
				IO_QUIT();	
			case 'q':
				loop = 0;
				do_keep_alive_socket(0, 0);
				if (fp) fclose(fp);
				break;
			case 'r': /* single read */
				single_read_rtl(p, verbose);
				break;
			case 'R': /* block read */
				block_read_rtl(p, verbose);
				break;
			case 's': /* IPC set command */
				ipc_set_cmd(p+1, verbose);
				break;
			case 't':
				printf("%lld ns \n", rtl_time());
				break;
			case 'u': /* loop until */
				loop_until(p, verbose);
				break;
			case 'v':
				verbose = num ? 1 : 0;
				break;
			case 'w': /* single write */
				single_write_rtl(p, verbose);
				break;
			case 'W': /* block write */
				block_write_rtl(p, verbose);
				break;
			default:
				break;
		} /* end of switch */
	}

	if (line) free(line);
	write_history(history_file);
	history_truncate_file(history_file, 100);	
	return 0;
}

void Usage()
{
	printf("\ta #_of_clocks             - RTL advance (a.k.a stall) \n"
	       "\tc addr value mask         - compare data @ addr with v by mask \n"
	       "\td 0/1                     - Turn on/off rtl dump \n"
	       "\tf file                    - Record shell command to file \n"
	       "\tg request_code [exp mask] - Ipc request command \n"
	       "\th                         - Help \n"
	       "\th[s|g]                    - Help for ipc set/get command \n"
	       "\ti msg                     - display msg via rtl \n"
	       "\tI [exp mask]              - check interrupt \n"
	       "\tk 0/1                     - Turn on/off persistence tcp socket\n"
	       "\tl 0/1                     - Turn on/off ipc mon \n"
	       "\tmw[b] addr file           - Write data(from file) @addr  \n"
	       "\tmr[b] addr #data file     - Write #data word from addr to file \n"
	       "\tp                         - Ping RTL server\n"
	       "\tq                         - Exit shell\n"
	       "\tQ                         - Exit shell & RTL \n"
	       "\tr[attr] address           - Single read command \n"
	       "\tR[b] addr size            - Block read command\n"
	       "\ts req_code value          - Ipc set command\n"
	       "\tt                         - RTL time\n"
	       "\tv 0/1                     - turn on/off verbose \n"
	       "\tu addr v mask timeout(ns) - polling until (data & mask = v & mask)\n"
	       "\tw[attr] addr data         - Single write command \n"
	       "\tW[b] addr size data ...   - Block write command \n"
	       " * * * Note * * * \n"
	       "\t attr: w(word) 3(3 bytes) h(half word) b(byte)\n"
	       "\t       wb 3b hb bb (same as above, but via backdoor \n"
	       " * * * Data format * * * \n"
	       "\t       0x --- hex    0b --- binary    else--decimal\n"
	      );
}


void Usage_set()
{
	printf("Request code can be either a number or following strings:  \n"
	       "\tRESET_CHIP \n"
	       "\tINIT \n"
	       "\tPOST_INIT \n"
	       "\tPCI_PRE_FETCH \n"
	       "\tSET_POS_WR_TIMER \n"
	       "\tSTRAP_PIN_INFO \n"
	       "\tSDRAM_INIT \n"
	       "\tSDRAM_SET_ARBT \n"
	       "\tROM_INIT \n"
	       "\tFLASH_TEST_LD_DATA  \n"
	       "\n * * Backdoor set command * * \n"
	       "\tBD_DI_RESET 1|2|3    --  1:assert 2:deassert 3:full sequence \n"
	       "\tBD_DI_XFER  (f)rev? send_size rcv_size wr_cmd(new for bb2) random_stall? \n"
	       "\tBD_DI_PUT   offset words  -- write a word to di buffer\n"
	       "\tBD_DI_BREAK_AT pos   -- break at given pos(receive only) \n"
	       "\tBD_DI_BREAK_NOW      -- set break pos to be current + 1\n"
	       "\tBD_AIS_RESET         -- AIS reset\n"
	       "\tBD_AIS_SET fast      -- set clock period \n"
	       "\tBD_AIS_PUT offset data  --- write a word to ais buffer \n" 
	       "\tBD_AIS_START #sample skip -- AIS start \n"
	       "\tBD_AIS_SET_PERIOD  value  -- set clock period \n"
	      );
}

void Usage_get()
{
	printf("Request code can be either a number or following strings:  \n"
	       "\tcpu_vec_02_end    --- read value of cpu_vec_02_end\n"
	       "\n * * Backdoor get command * * \n"
	       "\tBD_GI_SECURE_BIT  --- read GI secure bit\n"
	       "\tBD_DI_GET offset  --- read word at given offset from DI buffer\n"
	       "\tBD_AIS_GET offset --- read word at given offset from AIS buffer\n"
	       "\tBD_AIS_GET_PERIOD --- read AIS clock period \n"
	      );
}

