/* iosim 
   cmd function file 
   :set tabstop=4
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <unistd.h>
#include <netinet/in.h>
#include <sys/stat.h>
#include <sys/time.h>
#include "iosim.h"
#include "utils.h"
#include "cmd.h"

char *skip_blank(char *str)
{
	char *tmp = str;

	while (tmp[0] == ' ' || tmp[0] == '\t')  tmp++;

	return tmp;
}

char *skip_nonblank(char *str)
{
	char *tmp = str;

	while (tmp[0] != ' ' && tmp[0] != '\t' && tmp[0]!='\0')
		 tmp++;

	return tmp;
}

unsigned int get_cmd_arg_num(char *str)
{
	char *tmp = skip_blank(str);
	
	if (tmp[0] == '0' && ((tmp[1] == 'x') || (tmp[1] == 'X'))) 
		return strtoul(tmp, NULL, 16);
	
	return strtoul(tmp, NULL, 10);
}

char *get_cmd_arg_str(char *str)
{
	return skip_blank(str);
}

int print_xz_data(char *str)
{
	int i, j;
	unsigned int data = 0;

	if (str == NULL) return -1;
	
	if (strstr(str, "x") || strstr(str, "z")) {
		printf("Binary:");
		for (i=0; i<32; i+=4) { 
			printf(" ");
			for (j=i; j<(i+4); j++) 
				printf("%c", str[j]);
		}
	} else {
		for (i=0; i<32; i++) 
			data = (data << 1) | ((str[i] - '0') & 1);
		printf("Hex: 0x%8.8x", data);
	}
	printf("\n");

	return 0;
}

int single_read_rtl(char *cmd, int verbose)
{
	char *tmp;
	char read_data[64];
	unsigned int addr;
	int backdoor = 0, size = REQ_SZ_WORD;

	tmp = skip_nonblank(cmd);
	tmp = skip_blank(tmp);
	addr = get_cmd_arg_num(tmp);
	
	if ((cmd[2] == 'b') && (cmd[1] != ' ') && (cmd[1] != '\t')) backdoor=1;
	switch (cmd[1]) {
		case '3' :
			size = REQ_SZ_3BYTE;
			break;
		case 'h' :
			size = REQ_SZ_HALF;
			break;
		case 'b' :
			size =  REQ_SZ_BYTE;
			break;
	}

        if (backdoor) {
		if (verbose) printf("Backdoor read %dBytes @0x%x \n", size, addr);	
		bd_sim_read_v(addr, size, read_data);
	} else {
		if (verbose) printf("Ipc read %dBytes @0x%x \n", size, addr);	
		sim_read_v(addr, size, read_data);
	}
	
	return print_xz_data(read_data);
}


int single_write_rtl(char *cmd, int verbose)
{

	char *tmp;
	unsigned int addr;
	int backdoor = 0, size = REQ_SZ_WORD;

	tmp = skip_nonblank(cmd);
	tmp = skip_blank(tmp);
	addr = get_cmd_arg_num(tmp);

	tmp = skip_nonblank(tmp);
	tmp = skip_blank(tmp);
	
	if ((cmd[2] == 'b') && (cmd[1] != ' ') && (cmd[1] != '\t')) backdoor=1;
	switch (cmd[1]) {
		case '3' :
			size = REQ_SZ_3BYTE;
			break;
		case 'h' :
			size = REQ_SZ_HALF;
			break;
		case 'b' :
			size =  REQ_SZ_BYTE;
			break;
	}

        if (backdoor) {
		if (verbose) printf("Backdoor write %dBytes @0x%x data=%s\n", size, addr, tmp);	
		bd_sim_write_v(addr, size, tmp);
	} else {
		if (verbose) printf("Ipc write %dBytes @0x%x data=%s\n", size, addr, tmp);
		sim_write_v(addr, size, tmp);
	}
	
	return 0;
}

void check_interrupt(char *cmd, int verbose)
{
	int mask, exp, ret;
	char *tmp;

	tmp = skip_blank(cmd);
	exp = get_cmd_arg_num(tmp);

	tmp = skip_nonblank(tmp);
	tmp = skip_blank(tmp);
	mask = get_cmd_arg_num(tmp);

	ret = BD_IO_INTERRUPT; 
	if (verbose) printf("Interrupt line : 0x%x \n", ret);

	if (mask) {
		if ((ret & mask) != (exp & mask)) 
			printf("Failed: interrupt exp=0x%x ret=0x%x mask=0x%x\n", exp, ret, mask);
		else
			printf("Succeed: interrupt exp=0x%x ret=0x%x mask=0x%x\n", exp, ret, mask);
	} else printf("Interrupt line = 0x%x\n", ret);

	return;
}

void ipc_get_cmd(char *cmd, int verbose)
{
	int code, mask=0, value=0, ret, param0=0;
	char *tmp;

	if ((tmp = strstr(cmd, "cpu_vec_02_end"))) code = REQ_GET_cpu_vec_02_end;
	else if ((tmp = strstr(cmd, "BD_GI_SECURE_BIT"))) code = BD_GI_SECURE_BIT;
	else if ((tmp = strstr(cmd, "BD_DI_GET"))) code = BD_DI_GET;	
	else if ((tmp = strstr(cmd, "BD_AIS_GET"))) code = BD_AIS_GET;
	else if ((tmp = strstr(cmd, "BD_AIS_GET_PERIOD"))) code = BD_AIS_GET_PERIOD;
	else {
		tmp = skip_blank(cmd);
		code = get_cmd_arg_num(tmp);
	}
	if (verbose) printf("Ipc Get Req = 0x%x\n", code);
	if (code == BD_DI_GET || code == BD_AIS_GET) {
		tmp = skip_nonblank(tmp);
		tmp = skip_blank(tmp);
		param0 = get_cmd_arg_num(tmp);
	}

	tmp = skip_nonblank(tmp);
	tmp = skip_blank(tmp);
	value = get_cmd_arg_num(tmp);
	tmp = skip_nonblank(tmp);
	tmp = skip_blank(tmp);
	mask = get_cmd_arg_num(tmp);

	ret = ipc_get_data(code, param0);
	if (mask) { 
		if ((ret & mask) == (value & mask)) 
 			printf("Succeed: get 0x%x exp 0x%x mask 0x%x \n",
			          ret, value, mask);
		else
			printf("Failed: get 0x%x exp 0x%x mask 0x%x \n",
			          ret, value, mask);
	} else printf("Data = 0x%x \n", ret);

	return;	
}

void ipc_set_cmd(char *cmd, int verbose)
{
	char *tmp;
	unsigned int code, v[PKT_DATA_WORD];
	int num, i;

	if ((tmp = strstr(cmd, "BD_DI_RESET"))) code = BD_DI_RESET;
	else if ((tmp = strstr(cmd, "BD_DI_XFER"))) code = BD_DI_XFER;
	else if ((tmp = strstr(cmd, "BD_DI_PUT"))) code = BD_DI_PUT;
	else if ((tmp = strstr(cmd, "BD_DI_BREAK_AT"))) code = BD_DI_BREAK_AT;
	else if ((tmp = strstr(cmd, "BD_DI_BREAK_NOW"))) code = BD_DI_BREAK_NOW;
	else if ((tmp = strstr(cmd, "BD_AIS_RESET"))) code = BD_AIS_RESET;
	else if ((tmp = strstr(cmd, "BD_AIS_SET"))) code = BD_AIS_SET;
	else if ((tmp = strstr(cmd, "BD_AIS_PUT"))) code = BD_AIS_PUT;
	else if ((tmp = strstr(cmd, "BD_AIS_START"))) code = BD_AIS_START;
	else if ((tmp = strstr(cmd, "BD_AIS_SET_PERIOD"))) code = BD_AIS_SET_PERIOD;
	else if ((tmp = strstr(cmd, "RESET_CHIP"))) code = REQ_RESET_CHIP;
	else if ((tmp = strstr(cmd, "POST_INIT"))) code = REQ_POST_INIT;
	else if ((tmp = strstr(cmd, "PCI_PRE_FETCH"))) code = REQ_PCI_PRE_FETCH;
	else if ((tmp = strstr(cmd, "SET_POS_WR_TIMER"))) code = REQ_SET_POS_WR_TIMER;
	else if ((tmp = strstr(cmd, "STRAP_PIN_INFO"))) code = REQ_STRAP_PIN_INFO;
	else if ((tmp = strstr(cmd, "SDRAM_INIT"))) code = REQ_SDRAM_INIT;
	else if ((tmp = strstr(cmd, "SDRAM_SET_ARBT"))) code = REQ_SDRAM_SET_ARBT;
	else if ((tmp = strstr(cmd, "ROM_INIT"))) code = REQ_ROM_INIT;
	else if ((tmp = strstr(cmd, "FLASH_TEST_LD_DATA"))) code = REQ_FLASH_TEST_LD_DATA;
	else if ((tmp = strstr(cmd, "INIT"))) code = REQ_INIT;
	else {
		tmp = skip_blank(cmd);
		code = get_cmd_arg_num(tmp);
	}

	tmp = skip_nonblank(tmp);
	tmp = skip_blank(tmp);
	num = v[0] = 0;
	while ((tmp[0] != '\r') && (tmp[0] != '\n') && (tmp[0] != '\0')) {
		v[num++] = get_cmd_arg_num(tmp);
		tmp = skip_nonblank(tmp);
		tmp = skip_blank(tmp);
		if (num >= PKT_DATA_WORD) break;
	}		
	
	if (verbose) {
		printf("IPC set code=0x%x #data = %d\n", code, num);
		for (i=0; i<num; i++) 
			printf("Data %d v = 0x%x\n", i, v[i]);
	}
	if (num <= 1) ipc_set_data(code, v[0]);
	else ipc_set_multi_data(code, v, num);
	
	return;
}

int block_read_rtl(char *cmd, int verbose)
{
	char *tmp, read_data[64];
	unsigned int addr;
	int size, i;
	IpcPkt r;

	tmp = skip_nonblank(cmd);
	tmp = skip_blank(tmp);
	addr = get_cmd_arg_num(tmp);

	tmp = skip_nonblank(tmp);
	tmp = skip_blank(tmp);
	size = get_cmd_arg_num(tmp);

	if (addr & 0x3) {
		printf("Error: Block read address 0x%x \n", addr);
		return -1;
	}
	if ((size > 8) || size < 0) {
		printf("Error: wrong block size %d \n", size);
		return -1;
	}

	if (cmd[1] == 'b') {
		if (verbose) printf("BD Block read @0x%x size=%d \n", addr, size);
		bd_sim_bread(addr, size, r.data);
	} else {
		if (verbose) printf("Block read @0x%x size=%d \n", addr, size);
		sim_bread(addr, size, r.data);
	}

	for (i=0; i<size; i++) {
		data_to_xz(i, r.data, read_data);
		printf("Data %d -- ", i);
		print_xz_data(read_data);
	}
		
	return size;
}


int block_write_rtl(char *cmd, int verbose)
{
	char *tmp, read_data[64];
	unsigned int addr;
	int size, i;
	IpcPkt r;

	tmp = skip_nonblank(cmd);
	tmp = skip_blank(tmp);
	addr = get_cmd_arg_num(tmp);

	tmp = skip_nonblank(tmp);
	tmp = skip_blank(tmp);
	size = get_cmd_arg_num(tmp);

	if (addr & 0x3) {
		printf("Error: Block write address 0x%x \n", addr);
		return -1;
	}
	if ((size > 8) || size < 0) {
		printf("Error: wrong block size %d \n", size);
		return -1;
	}

	for (i=0; i<size; i++) {
		tmp = skip_nonblank(tmp);
		tmp = skip_blank(tmp);
		xz_to_data(i, tmp, r.data);
	}

	if (cmd[1] == 'b') {
		if (verbose) printf("BD Block write @0x%x size=%d \n", addr, size);
		bd_sim_bwrite(addr, size, r.data);
	} else {
		if (verbose) printf("Block write @0x%x size=%d \n", addr, size);
		sim_bwrite(addr, size, r.data);
	}

	if (verbose)
		for (i=0; i<size; i++) {
			data_to_xz(i, r.data, read_data);
			printf("Data %d -- ", i);
			print_xz_data(read_data);
		}
		
	return size;
}

int compare_with_mask(char *cmd, int verbose)
{
	char *tmp, read_data[64], exp[64];
	unsigned int addr, mask;
	IpcPkt r;

	tmp = skip_nonblank(cmd);
	tmp = skip_blank(tmp);
	addr = get_cmd_arg_num(tmp);

	tmp = skip_nonblank(tmp);
	tmp = skip_blank(tmp);
	xz_to_data(0, tmp, r.data);

	tmp = skip_nonblank(tmp);
	tmp = skip_blank(tmp);
	mask = get_cmd_arg_num(tmp);
	if (mask == 0) mask = 0xffffffff;

	data_to_xz(0, r.data, exp);
	if (verbose) {
		printf("Compare data @ address 0x%x \n", addr);
		printf("     Exp  == ");
		print_xz_data(exp);
        }

	read_data[0] = '0';
	read_data[1] = 'b';
	sim_read_v(addr, REQ_SZ_WORD, read_data+2);
	xz_to_data(1, read_data, r.data);
	if (verbose) {
		printf("     Read == ");
		print_xz_data(read_data+2);
		printf("     Mask == 0x%x\n", mask);
	}

	if ( ( (DATA_PART(0, r.data) & mask) != (DATA_PART(1, r.data) & mask) ) ||
	     ( (XZ_PART(0, r.data) & mask) != (XZ_PART(1, r.data) & mask) ) ) 
		printf("Failed: mismatch \n");
	else
		printf("Succeed. \n");
	
	return 0;
}

int loop_until(char *cmd, int verbose)
{
	char *tmp, read_data[64], exp[64];
	unsigned int addr, mask, timeout;
	IpcPkt r;
	long long cur, end;

	tmp = skip_nonblank(cmd);
	tmp = skip_blank(tmp);
	addr = get_cmd_arg_num(tmp);

	tmp = skip_nonblank(tmp);
	tmp = skip_blank(tmp);
	xz_to_data(0, tmp, r.data);

	tmp = skip_nonblank(tmp);
	tmp = skip_blank(tmp);
	mask = get_cmd_arg_num(tmp);
	if (mask == 0) mask = 0xffffffff;

	tmp = skip_nonblank(tmp);
	tmp = skip_blank(tmp);
	timeout = get_cmd_arg_num(tmp);
	cur = rtl_time();
	end  = cur + timeout;

	data_to_xz(0, r.data, exp);
	if (verbose) {
		printf("Wait till data @ address 0x%x \n", addr);
		printf("     Exp  == ");
		print_xz_data(exp);
		printf("     Mask == 0x%x\n", mask);
		printf("     Current time = %llu ns \n", cur);
		printf("     Timeout == %s\n", timeout==0?"Infinit":tmp);
        }

	while ((timeout==0) || (cur < end)) {
		read_data[0] = '0';
		read_data[1] = 'b';
		sim_read_v(addr, REQ_SZ_WORD, read_data+2);
		xz_to_data(1, read_data, r.data);
		cur = rtl_time();

		if (verbose) {
			printf("     Read == ");
			print_xz_data(read_data+2);
			printf("     Rtl time = %llu ns\n", cur);
		}
		

		if ( ( (DATA_PART(0, r.data) & mask) == (DATA_PART(1, r.data) & mask) ) &&
		     ( (XZ_PART(0, r.data) & mask) == (XZ_PART(1, r.data) & mask) ) ) 
			return 0;
	}
	
	printf("Failed: Timeout\n");
	return -1;
}

int mem_write(char *cmd, int backdoor, int verbose)
{
	
	char *tmp,  line[256];
	unsigned int addr;
	IpcPkt r;
	FILE *f;
	int dn;

	tmp = skip_blank(cmd);
	addr = get_cmd_arg_num(tmp);

	tmp = skip_nonblank(tmp);
	tmp = skip_blank(tmp);
	f = fopen(tmp, "rb");

	if (f == NULL) {
		printf("Error: cannot open file <%s>\n", tmp);
		return -1;
	} 

	if (verbose) 
		printf("Read data from file %s and write @0x%x \n", tmp,  addr);

	dn = 0;
	memset(r.data, 0, sizeof(r.data));
	while (!feof(f)) {
		fgets(line, sizeof(line), f);
		tmp = skip_blank(line);
		if (feof(f)) break;
		if (tmp[0] == '\0' || tmp[0] == '\n' || tmp[0] == '\r')
			continue;
		
		xz_to_data(dn, line, r.data);
		dn++;
		if (dn >= 8) {
			if (backdoor) bd_sim_bwrite(addr, 8, r.data);
			else sim_bwrite(addr, 8, r.data);
			dn = 0;
			memset(r.data, 0, sizeof(r.data));
		}		
	}

	if (dn) {
		if (backdoor) bd_sim_bwrite(addr, dn, r.data);
		else sim_bwrite(addr, 8, r.data);
	}

	fclose(f);
	return 0;
}

int mem_read(char *cmd, int backdoor, int verbose)
{
	
	char *tmp, data[64];
	unsigned int addr;
	IpcPkt r;
	FILE *f;
	int i, count, num, j;

	tmp = skip_blank(cmd);
	addr = get_cmd_arg_num(tmp);

	tmp = skip_nonblank(tmp);
	tmp = skip_blank(tmp);
	count = get_cmd_arg_num(tmp);

	tmp = skip_nonblank(tmp);
	tmp = skip_blank(tmp);
	f = fopen(tmp, "wb");

	if (f == NULL) f = stdin;

	if (verbose) 
		printf("Read %d words from 0x%x and write to %s \n",
			count, addr, tmp);

	for (i=0; i<count; i+=8) {
		num = (count -i) > 8 ? 8 : (count -i);
		if (backdoor) bd_sim_bread(addr+i*4, num, r.data);
		else sim_bread(addr+i*4, 8, r.data);
		
		for (j=0; j< num; j++) {
			if (XZ_PART(j, r.data)) {
				data_to_xz(j, r.data, data);
				fprintf(f, "0b%s\n", data);
			} else fprintf(f, "0x%x\n", DATA_PART(j, r.data));
		}
	}

	if (f != stdin) fclose(f);
	else fflush(stdin);
	return 0;
}

