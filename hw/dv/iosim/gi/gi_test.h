/* BB2 gi test headfile */

#ifndef _BB2_GI_TEST_HEADER
#define _BB2_GI_TEST_HEADER

#ifdef ALI_GI
	#define SYS_SDRAM_SIZE              0x00800000
	#define SYS_SDRAM_MASK              0x007FFFFF
#else
	#define SYS_SDRAM_SIZE              0x00100000
	#define SYS_SDRAM_MASK              0x000FFFFF
#endif

#define WORD_MASK                   0xffffffff
#define SZ_64K                      0x10000

#define GI_64K_BMASK                0xFFF0
#define GI_SRAM_BMASK               0x17FF0
#define GI_SRAM_BADDR_MASK          0x1FFF0
#define GI_SRAM_WADDR_MASK          0x1FFFC

#define GI_ROM_SIZE                 0x2000     /* 8*1024 */
#define GI_ROM_BMASK                0x1FF0

   /* Make 1MByte space for CPU Tests */ 
#define CPU_TEST_SDRAM_ADDR         0x00100000
#define CPU_TEST_SDRAM_SIZE         0x00100000
#define CPU_TEST_SDRAM_MASK         0x000FFFFF

#define DI_RESET_ASSERT             1
#define DI_RESET_DEASSERT           2
#define DI_RESET_FULLSEQ            3

#define GI_INT_POLLING_TIMEOUT      0x40000
#define GI_DI_CMD_BYTE_MASK         0xFF000000
#define GI_DI_CMD_BYTE_SHIFT        24

#define ERR_BEFORE_CMD              1
#define ERR_IN_CMD                  2
#define ERR_CMD                     3
#define ERR_SEND_DATA               4
#define ERR_RESPOND_OUT             5
#define BRK_DI_SEND                 0x1004

#define MAX_NUM_AIS_DV_BLK          16      /* Frank's 64K bytes buffer */
#define GI_AIS_BLK_SZ               4096
#define GI_AIS_BLK_WSZ              1024
#define GI_AIS_BLK_TO               (1024 * 33 * 651 / 10)
#define GI_AIS_SRAM_BUF_SZ          256
#define GI_AIS_BLK_MASK             0xFFFFFF00
#define GI_AIS_MBLK_MASK            0xFFFFF000
#define SYS_SDRAM_GI_ONLY_SIZE      0x00100000

int gi_reset_checkup();
int gi_sram_tests();
int gi_rom_tests();
int di_xfer_tests();
int di_reg_test(int );
int bi_reg_test(int verbose);
int bi_dma_test();
int ais_reg_test(int verbose);
int ais_tests();

int dc_reg_test(int verbose);

#endif
