
/* BB2 AIS register test */

#ifndef T2RISC
	#include <stdlib.h>
	#include <stdio.h>
	#include <string.h>
	#include "iosim.h"
#endif

#include "utils.h"
#include "gi.h"
#include "gi_test.h"
#include "gi_utils.h"

int dc_reg_test(int verbose)
{
	int ret = 0;
	unsigned int v;

#ifndef T2RISC
	int rd = rand_data();
	int rd_s = rand_data();
	bd_fill_pattern(0, SYS_SDRAM_SIZE, rd, 0);
	bd_fill_pattern(GI_SRAM_START, GI_SRAM_SIZE, rd_s, 0);
#endif
	v = GI_DC_CTRL_OP_MASK | GI_DC_CTRL_SG_CUR_MASK | GI_DC_CTRL_SG_OFF_MASK;
	//ret |= reg_rw_test(GI_DC_CTRL_REG, v, v, verbose);
	v =  GI_DC_BUF_BUF_OFF_MASK | GI_DC_BUF_BUF_OFF_MASK;
	ret |= reg_rw_test(GI_DC_BUF_REG, v, v, verbose);
	v = GI_DC_HADDR_H0_ADDR_MASK | GI_DC_HADDR_H0_ADDR_MASK;
	ret |= reg_rw_test(GI_DC_HADDR_REG, v, v, verbose);
	ret |= reg_rw_test(GI_DC_HIDX_REG, GI_DC_HIDX_H0_IDX_MASK | GI_DC_HIDX_H1_PTR_MASK,
			GI_DC_HIDX_H0_IDX_MASK | GI_DC_HIDX_H1_PTR_MASK, verbose);
	ret |= reg_rw_test(GI_DC_DIOFF_REG, GI_DC_DIOFF_DI_LAST_MASK | GI_DC_DIOFF_DI_FIRST_MASK,
			GI_DC_DIOFF_DI_LAST_MASK | GI_DC_DIOFF_DI_FIRST_MASK, verbose);
	ret |= reg_rw_test(GI_DC_IV_REG, GI_DC_IV_IV_OFF_MASK, GI_DC_IV_IV_OFF_MASK, verbose);
	ret |= reg_rw_test(GI_DC_H0_REG, GI_DC_H0_H0_BASE_MASK, GI_DC_H0_H0_BASE_MASK, verbose);
	ret |= reg_rw_test(GI_DC_H1_REG, GI_DC_H1_H1_BASE_MASK, GI_DC_H1_H1_BASE_MASK, verbose);

#ifndef T2RISC
	bd_check_pattern(0, SYS_SDRAM_SIZE, rd, 0);
	bd_check_pattern(GI_SRAM_START, GI_SRAM_SIZE, rd_s, 0);
#endif

#ifdef  T2RISC
	if (IO_READ(GI_SEC_MODE_REG) & GI_SEC_MODE_SECURE_MASK) 
#else
	if (ipc_get_data(BD_GI_SECURE_BIT, 0)) 	
#endif
		leave_sec_mode();
	else enter_sec_mode(0);

	v = GI_DC_CTRL_OP_MASK | GI_DC_CTRL_SG_CUR_MASK | GI_DC_CTRL_SG_OFF_MASK;
	//ret |= reg_rw_test(GI_DC_CTRL_REG, v, v, verbose);
	v =  GI_DC_BUF_BUF_OFF_MASK | GI_DC_BUF_BUF_OFF_MASK;
	ret |= reg_rw_test(GI_DC_BUF_REG, v, v, verbose);
	v = GI_DC_HADDR_H0_ADDR_MASK | GI_DC_HADDR_H0_ADDR_MASK;
	ret |= reg_rw_test(GI_DC_HADDR_REG, v, v, verbose);
	ret |= reg_rw_test(GI_DC_HIDX_REG, GI_DC_HIDX_H0_IDX_MASK | GI_DC_HIDX_H1_PTR_MASK,
			GI_DC_HIDX_H0_IDX_MASK | GI_DC_HIDX_H1_PTR_MASK, verbose);
	ret |= reg_rw_test(GI_DC_DIOFF_REG, GI_DC_DIOFF_DI_LAST_MASK | GI_DC_DIOFF_DI_FIRST_MASK,
			GI_DC_DIOFF_DI_LAST_MASK | GI_DC_DIOFF_DI_FIRST_MASK, verbose);
	ret |= reg_rw_test(GI_DC_IV_REG, GI_DC_IV_IV_OFF_MASK, GI_DC_IV_IV_OFF_MASK, verbose);
	ret |= reg_rw_test(GI_DC_H0_REG, GI_DC_H0_H0_BASE_MASK, GI_DC_H0_H0_BASE_MASK, verbose);
	ret |= reg_rw_test(GI_DC_H1_REG, GI_DC_H1_H1_BASE_MASK, GI_DC_H1_H1_BASE_MASK, verbose);
	
#ifndef T2RISC
	bd_check_pattern(0, SYS_SDRAM_SIZE, rd, 0);
	bd_check_pattern(GI_SRAM_START, GI_SRAM_SIZE, rd_s, 0);
#endif

#ifdef  T2RISC
	if (IO_READ(GI_SEC_MODE_REG) & GI_SEC_MODE_SECURE_MASK) 
#else
	if (ipc_get_data(BD_GI_SECURE_BIT, 0)) 	
#endif
		enter_sec_mode(0);
	else leave_sec_mode();

	return ret;
}
