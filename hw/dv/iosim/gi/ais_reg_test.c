
/* BB2 AIS register test */

#ifndef T2RISC
	#include <stdlib.h>
	#include <stdio.h>
	#include <string.h>
	#include "iosim.h"
#endif

#include "utils.h"
#include "gi.h"
#include "gi_test.h"
#include "gi_utils.h"

int ais_reg_test(int verbose)
{
	int ret = 0;

#ifndef T2RISC
	int rd = rand_data();
	int rd_s = rand_data();
	bd_fill_pattern(0, SYS_SDRAM_SIZE, rd, 0);
	bd_fill_pattern(GI_SRAM_START, GI_SRAM_SIZE, rd_s, 0);
#endif

#ifdef  T2RISC
	if (IO_READ(GI_SEC_MODE_REG) & GI_SEC_MODE_SECURE_MASK) { 
#else
	if (ipc_get_data(BD_GI_SECURE_BIT, 0)) 	{
#endif
		ret |= reg_rw_test(GI_AIS_BUF_REG, 0, 
			   GI_AIS_BUF_BUF_OFF_MASK, verbose);
	} else {
		ret |= reg_ro_test(GI_AIS_BUF_REG, 0, 
			   GI_AIS_BUF_BUF_OFF_MASK, verbose);
	}

	ret |= reg_rw_test(GI_AIS_PTR0_REG, 0, 
			   GI_AIS_PTR0_MEM_PTR0_MASK | GI_AIS_PTR0_EMPTY_MASK, verbose);
	ret |= reg_rw_test(GI_AIS_PTR1_REG, 0, 
			   GI_AIS_PTR1_MEM_PTR1_MASK | GI_AIS_PTR1_EMPTY_MASK, verbose);

#ifndef T2RISC
	bd_check_pattern(0, SYS_SDRAM_SIZE, rd, 0);
	bd_check_pattern(GI_SRAM_START, GI_SRAM_SIZE, rd_s, 0);
#endif

#ifdef  T2RISC
	if (IO_READ(GI_SEC_MODE_REG) & GI_SEC_MODE_SECURE_MASK) {
#else
	if (ipc_get_data(BD_GI_SECURE_BIT, 0)) 	{
#endif
		leave_sec_mode();
		ret |= reg_ro_test(GI_AIS_BUF_REG, 0, 
			   GI_AIS_BUF_BUF_OFF_MASK, verbose);
	} else { 
		enter_sec_mode(0);
		ret |= reg_rw_test(GI_AIS_BUF_REG, 0, 
			   GI_AIS_BUF_BUF_OFF_MASK, verbose);
	}

	ret |= reg_rw_test(GI_AIS_PTR0_REG, 0, 
			   GI_AIS_PTR0_MEM_PTR0_MASK | GI_AIS_PTR0_EMPTY_MASK, verbose);
	ret |= reg_rw_test(GI_AIS_PTR1_REG, 0, 
			   GI_AIS_PTR1_MEM_PTR1_MASK | GI_AIS_PTR1_EMPTY_MASK, verbose);
	
#ifndef T2RISC
	bd_check_pattern(0, SYS_SDRAM_SIZE, rd, 0);
	bd_check_pattern(GI_SRAM_START, GI_SRAM_SIZE, rd_s, 0);
#endif

#ifdef  T2RISC
	if (IO_READ(GI_SEC_MODE_REG) & GI_SEC_MODE_SECURE_MASK) 
#else
	if (ipc_get_data(BD_GI_SECURE_BIT, 0)) 	
#endif
		leave_sec_mode();
	else enter_sec_mode(0);

	return ret;
}
