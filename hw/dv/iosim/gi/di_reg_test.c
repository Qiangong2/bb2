
/* BB2 DI register test */

#ifndef T2RISC
	#include <stdlib.h>
	#include <stdio.h>
	#include <string.h>
	#include "iosim.h"
#endif

#include "utils.h"
#include "gi.h"
#include "gi_test.h"
#include "gi_utils.h"

int di_reg_test(int verbose)
{
	unsigned int gi_di_conf_rw_mask, gi_di_ctrl_rw_mask, gi_di_be_rw_mask;
	int ret = 0;

#ifndef T2RISC
	int rd = rand_data();
	int rd_s = rand_data();
	bd_fill_pattern(0, SYS_SDRAM_SIZE, rd, 0);
	bd_fill_pattern(GI_SRAM_START, GI_SRAM_SIZE, rd_s, 0);
#endif

	gi_di_conf_rw_mask = GI_DI_CONF_SEC_CMD0_MASK | GI_DI_CONF_SEC_CMD1_MASK |
			     GI_DI_CONF_TIM_END_MASK | GI_DI_CONF_TIM_START_MASK;
	gi_di_ctrl_rw_mask = GI_DI_CTRL_BREAK_MASK_MASK | GI_DI_CTRL_DIR_MASK_MASK |
			     GI_DI_CTRL_DONE_MASK_MASK | GI_DI_CTRL_RST_MASK_MASK;
	gi_di_be_rw_mask = GI_DI_BE_SIZE_MASK | GI_DI_BE_OUT_MASK | GI_DI_BE_PTR_MASK;

#ifdef T2RISC
	if (IO_READ(GI_SEC_MODE_REG) & GI_SEC_MODE_SECURE_MASK) {
#else
	if (ipc_get_data(BD_GI_SECURE_BIT, 0)) { /* already in secure mode */
#endif
		ret |= reg_rw_test(GI_DI_CONF_REG, 0, gi_di_conf_rw_mask, verbose);
		ret |= reg_rw_test(GI_DI_CTRL_REG, GI_DI_CTRL_MASK_WE_MASK, gi_di_ctrl_rw_mask, verbose);
		ret |= reg_rw_test(GI_DI_BE_REG, 0, gi_di_be_rw_mask, verbose);
#ifndef T2RISC
		bd_check_pattern(0, SYS_SDRAM_SIZE, rd, 0);
		bd_check_pattern(GI_SRAM_START, GI_SRAM_SIZE, rd_s, 0);
#endif
		leave_sec_mode();
		ret |= reg_ro_test(GI_DI_CONF_REG, 0, gi_di_conf_rw_mask, verbose);	
		ret |= reg_rw_test(GI_DI_CTRL_REG, GI_DI_CTRL_MASK_WE_MASK, gi_di_ctrl_rw_mask, verbose);
		ret |= reg_rw_test(GI_DI_BE_REG, 0, gi_di_be_rw_mask, verbose);
#ifndef T2RISC
		bd_check_pattern(0, SYS_SDRAM_SIZE, rd, 0);
		bd_check_pattern(GI_SRAM_START, GI_SRAM_SIZE, rd_s, 0);
#endif
		enter_sec_mode(0);	
	} else { 
		ret |= reg_ro_test(GI_DI_CONF_REG, 0, gi_di_conf_rw_mask, verbose);	
		ret |= reg_rw_test(GI_DI_CTRL_REG, GI_DI_CTRL_MASK_WE_MASK, gi_di_ctrl_rw_mask, verbose);
		ret |= reg_rw_test(GI_DI_BE_REG, 0, gi_di_be_rw_mask, verbose);
#ifndef T2RISC
		bd_check_pattern(0, SYS_SDRAM_SIZE, rd, 0);
		bd_check_pattern(GI_SRAM_START, GI_SRAM_SIZE, rd_s, 0);
#endif
		enter_sec_mode(0);
		ret |= reg_rw_test(GI_DI_CONF_REG, 0, gi_di_conf_rw_mask, verbose);
		ret |= reg_rw_test(GI_DI_CTRL_REG, GI_DI_CTRL_MASK_WE_MASK, gi_di_ctrl_rw_mask, verbose);
		ret |= reg_rw_test(GI_DI_BE_REG, 0, gi_di_be_rw_mask, verbose);
#ifndef T2RISC
		bd_check_pattern(0, SYS_SDRAM_SIZE, rd, 0);
		bd_check_pattern(GI_SRAM_START, GI_SRAM_SIZE, rd_s, 0);
#endif
		leave_sec_mode();
	}

	return ret;
}
