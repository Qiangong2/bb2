/* BB2 GI test
   :set tabstop=4
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <unistd.h>
#include <netinet/in.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <getopt.h>
#include "gi.h"
#include "iosim.h"
#include "utils.h"
#include "gi_test.h"

void test_info(char *msg)
{
	printf("%s \n", msg);
	displayMsg(msg);
	return;
}

int main(int argc, char **argv)
{
	int quit = 0, c, seed=0, verbose=0;
	extern char *optarg;
	extern int optind, opterr, optopt;

	while ((c = getopt(argc, argv, "qhv?r:")) != EOF) {
		switch (c) {
			case 'q':
				quit = 1;
				break;
			case 'v':
				verbose = 1;
				break;
			case 'r':
				seed = atoi(optarg);
				break;
			case 'h':
			case '?': 
			default:
				printf("gi_test [-r rand_seed] [-q(uit)] [-h(elp)]\n");
				/* Your help function */
				return 0;
		}
	}

	if (IpcInit()) 	return -1;		
	FindServer();
	printf("Random seed = %d \n", seed);
	srand(seed);

	  /*  Make persistence connection */
	do_keep_alive_socket(1, 1);
	//IO_READ(GI_BASE);	

          /* Test is written here */
	test_info("TEST: Register check after reset");
	gi_reset_checkup();

	//test_info("TEST: DC register test");
	//dc_reg_test(0);   

	test_info("TEST: AIS register test");
	ais_reg_test(0);

	test_info("TEST: AIS transaction test");
	ais_tests();

	test_info("TEST: sram test");
	gi_sram_tests();

	test_info("TEST: rom test");
	gi_rom_tests();

	//test_info("TEST: DI transaction test");
	//di_xfer_tests();   

	test_info("TEST: DI register test");
	di_reg_test(verbose);
	
	test_info("TEST: BI register test");
	bi_reg_test(verbose);

	test_info("TEST: BI dma test");
	bi_dma_test();

	  /* Close persistence connection */
	do_keep_alive_socket(0, 1);
	if (quit) IO_QUIT();

	return 0;
}
