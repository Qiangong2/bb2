
/* BB2 GI sram test */

#ifndef T2RISC
	#include <stdlib.h>
	#include <stdio.h>
	#include <string.h>
	#include "iosim.h"
#else
	#include "t2os.h"
#endif

#include "utils.h"
#include "gi.h"
#include "gi_utils.h"
#include "gi_test.h"

int get_test_addresses(           /* return how many addresses */
	unsigned int *paddr,      /* created address */
	unsigned int start_addr,  /* start address */
	unsigned int size,        /* mem size */
	int walk_1)               /* 1 walk through all the bits */
{
	int num=0, i, j;
	unsigned int mask=0;
	
	if (walk_1) {
		paddr[num++] = start_addr;
		for (i=2; i<32; i++) 
			if ((1<<i) < size) 
				paddr[num++] = (1<<i) + start_addr;
	} else {
		for (i=31; i>=2; i--)
			if (size & (1<<i)) {
				mask = ((1<<i) - 1) & (~3);
				break;
			}

		for (j=2; j<i; j++)  
			paddr[num++] = ((~(1<<j)) & mask) + start_addr;
	}

	return num;
}

/* Single walk through test */
int test_mem_addrline(            /* return -1, if failed */ 
	unsigned int start_addr,  /* Test start address */   
	unsigned int size,        /* Test memory size */
	int walk_1,               /* 1 walk through each bit */
	int bd_wr,                /* Backdoor write data ? */
	int bd_rd)                /* backdoor read data ? */
{
	unsigned int data[32], addr[32], rback, rdata=0;
	int i, num, ret = 0;

	for (i=0; i<32; i++)  /* create random data */
#ifndef T2RISC
		data[i] = rand_data();
#else
		data[i] = (0xF0A50F5A << i) | (0xF0A50F5A >> (32-i));
#endif
	num = get_test_addresses(addr, start_addr, size, walk_1);

#ifndef T2RISC
	rdata = rand_data();
	bd_fill_pattern(start_addr, size, rdata, 0);
#endif

	for (i=0; i<num; i++) 
#ifdef T2RISC
		IO_WRITE(addr[i], data[i]);
#else
		if (bd_wr) BD_IO_WRITE(addr[i], data[i]);
		else IO_WRITE(addr[i], data[i]);

	if ((!bd_wr) & bd_rd)  /* flush write */
		IO_READ(start_addr+4); 
#endif

	for (i=0; i<num; i++) {
#ifdef T2RISC
		rback = IO_READ(addr[i]);
		if (rback != data[i]) return -1; 
#else
		rback = bd_rd ? BD_IO_READ(addr[i]) : IO_READ(addr[i]);
		if (rback != data[i]) {
			printf("Failed: address=0x%x data=0x%x exp=0x%x \n", 
				addr[i], rback, data[i]);
			return -1;
		}
#endif
	}

#ifndef T2RISC
	{
		int j, k;
		int st, ed;

		for (i=0; i<num; i++) {
			k = i;
			for (j=i+1; j<num; j++) 
				if (addr[j] < addr[k]) k = j;

			if (k != i) {
				j = addr[k];
				addr[k] = addr[i];
				addr[i] = j;
			}
		}

		for (i=0; i<num; i++) {
			st = (i==0) ? 0 : addr[i-1] - start_addr + 4;
			ed = addr[i] - start_addr;
			if (st < ed)
				ret |= bd_check_pattern(start_addr, ed-st, rdata, st);
		}
		ret |= bd_check_pattern(start_addr, size, rdata, addr[num-1]-start_addr+4);
	}
#endif	

	return ret;
}

int test_mem_dataline(              /* return -1 if failed */
	unsigned int start_addr,    /* Need at least 32 word space */
	int walk_1,                 /* 1 walk through each bit */
	int bd_wr,                  /* backdoor write */
	int bd_rd)                  /* backdoor read */
{
	unsigned int i, addr, rback, data, rdata;

	rdata = rand_data();
	bd_fill_pattern(start_addr, GI_SRAM_SIZE, rdata, 0);
	for (i=0, addr=start_addr; i<32; i++, addr+=4) {
		data = (walk_1) ? 1<<i : ~(1<<i);
#ifdef T2RISC
		IO_WRITE(addr, data);
#else
		if (bd_wr) BD_IO_WRITE(addr, data);
		else IO_WRITE(addr, data);
#endif
	}

#ifndef T2RISC
	if ((!bd_wr) & bd_rd)  /* flush write */
		IO_READ(start_addr+4); 
#endif

	for (i=0, addr=start_addr; i<32; i++, addr+=4) {
		data = (walk_1) ? 1<<i : ~(1<<i);
#ifdef T2RISC
		rback = IO_READ(addr);
		if (rback != data) return -1;
#else
		rback = bd_rd ? BD_IO_READ(addr) : IO_READ(addr);
		if (rback != data) {
			printf("Failed: address=0x%x data=0x%x exp=0x%x \n",
					addr, rback, data);
		}
#endif
	}

	bd_check_pattern(start_addr, GI_SRAM_SIZE, rdata, 32*4);
 	return 0;	
}

#ifndef T2RISC
int test_mem_random(
	unsigned int start_addr,        /* Memory starts */
	unsigned int mem_size,          /* Test memory size */
	int num,                        /* # of r/w */
	int random_stall,               /* random stall ? */
	int bd_wr,
	int bd_rd)
{
	unsigned int addr, data, rback, rd;
	int i;
	for (i=0; i<num; i++) {
		rd = rand_data();
		bd_fill_pattern(start_addr, mem_size, rd, 0);
		addr = (rand_data() % mem_size) & (~3);
		data = rand_data();
		addr += start_addr;

		if (bd_wr) BD_IO_WRITE(addr, data);
		else {
			IO_WRITE(addr, data);
			if (random_stall) IO_STALL(rand_data() & 3);
		}
		
		if ((!bd_wr) & bd_rd)  /* flush write */
			IO_READ(start_addr);

		rback = bd_rd ? BD_IO_READ(addr) : IO_READ(addr);
		if (bd_rd) rback = BD_IO_READ(addr);
		else {
			rback = IO_READ(addr);
			if (random_stall) IO_STALL(rand_data() & 3);
		}
		if ( rback != data) {
			printf("Failed: address=0x%x data=0x%x exp=0x%x \n",
					addr, rback, data);
		}
		if ((addr - start_addr) > 0) bd_check_pattern(start_addr, addr-start_addr, rd, 0);
		if ((addr - start_addr) < (GI_SRAM_SIZE-4)) 
			bd_check_pattern(start_addr, mem_size, rd, addr-start_addr+4);
	}
	return 0;
}
#endif	

int test_sram_single_rw(unsigned int sram_saddr, char *testn, int bd_wr, int bd_rd)
{
	int ret; 
#ifndef T2RISC
	char msg[64], bd[16]="via backdoor";

	if (!(bd_wr | bd_rd)) bd[0]= '\0';

	sprintf(msg, "Test(%s): 1 walk through addrline %s", testn, bd);
	displayMsg(msg);
	printf("%s\n", msg);
#endif
	ret = test_mem_addrline(sram_saddr, GI_SRAM_SIZE, 1, bd_wr, bd_rd);
	
#ifndef T2RISC
	sprintf(msg, "Test(%s): 0 walk through addrline %s", testn, bd);
	displayMsg(msg);
	printf("%s\n", msg);
#endif
	ret |= test_mem_addrline(sram_saddr, SZ_64K , 0, bd_wr, bd_rd);
	ret |= test_mem_addrline(sram_saddr + SZ_64K, GI_SRAM_SIZE - SZ_64K, 0, bd_wr, bd_rd);

#ifndef T2RISC
	sprintf(msg, "Test(%s): 1 walk through data line %s", testn, bd);
	displayMsg(msg);
	printf("%s\n", msg);
#endif
	ret |= test_mem_dataline(sram_saddr, 1, bd_wr, bd_rd);

#ifndef T2RISC
	sprintf(msg, "Test(%s): 0 walk through data line %s", testn, bd);
	displayMsg(msg);
	printf("%s\n", msg);
#endif
	ret |= test_mem_dataline(sram_saddr, 0, bd_wr, bd_rd);


#ifndef T2RISC
	    /* Random test 16 times*/
	sprintf(msg, "Test(%s): random test %s", testn, bd);	
	displayMsg(msg);
	printf("%s\n", msg);
	ret |= test_mem_random(sram_saddr, GI_SRAM_SIZE, 0x10, 0, bd_wr, bd_rd);

	sprintf(msg, "Test(%s): random test %s w random stall", testn, bd);	
	displayMsg(msg);
	printf("%s\n", msg);
	ret |= test_mem_random(sram_saddr, GI_SRAM_SIZE, 0x10, 1, bd_wr, bd_rd);
#endif

	return ret;
}

#ifndef T2RISC
void test_sram_single_rw_all(unsigned int sram_saddr, char *testn, int bd_wr, int bd_rd)
{
	unsigned int *pdata, addr, end, ret;	
	int i;

	displayMsg(testn);
	printf("%s\n", testn);

	pdata = (unsigned int *) malloc(GI_SRAM_SIZE);
	end = sram_saddr + GI_SRAM_SIZE;
	for (addr = sram_saddr, i=0; addr < end; addr += 4, i++) {
		pdata[i] = rand_data();
		if (bd_wr) BD_IO_WRITE(addr, pdata[i]);
		else IO_WRITE(addr, pdata[i]);
	}

	if ((!bd_wr) & bd_rd)  /* flush */
		IO_READ(sram_saddr);

	for (addr = sram_saddr, i=0; addr < end; addr += 4, i++) {
		if (bd_rd) ret = BD_IO_READ(addr);
		else ret = IO_READ(addr);
		
		if (ret != pdata[i]) {
			printf("Failed: sram compare error addr=0x%x exp=0x%x data=0x%x\n",
				addr, pdata[i], ret);
			break;
		} 
	}

	return;
}
#endif

int test_one_block_rd(unsigned int saddr)
{
	unsigned int data[4], i, rback[16];

#ifdef T2RISC
	InvalidateDCache(saddr, 16);
#endif
	for (i=0; i<4; i++) {
		data[i] = rand_data();
#ifndef T2RISC
		BD_IO_WRITE(saddr+(i<<2), data[i]);
#else
		IO_WRITE(saddr+(i<<2), data[i]);
#endif			
	}

#ifndef T2RISC		
	sim_bread(saddr, 4, rback);
#endif

	for (i=0; i<4; i++)  {
#ifdef T2RISC		
		rback[i] = IO_CACHE_READ(saddr+(i<<2));
#endif	
		if (rback[i] != data[i]) {
#ifndef T2RISC		
			printf("Failed: bread err @0x%x exp=0x%x, rb=0x%x", 
				saddr+(i<<2), data[i], rback[i]);
#endif
			return i+1;
		}
	}

	return 0;
}

int test_sram_block_rd(unsigned int sram_saddr)
{
	unsigned int ret=0, sz, addr;
	int rd=0;

	for (sz=16; sz < GI_SRAM_SIZE; sz<<=1) {
#ifndef T2RISC
		rd = rand_data();
		bd_fill_pattern(sram_saddr, GI_SRAM_SIZE, rd, 0);
#endif
		ret |= test_one_block_rd(sram_saddr+sz);
#ifndef T2RISC
		bd_check_pattern(sram_saddr, sz, rd, 0);
		bd_check_pattern(sram_saddr, GI_SRAM_SIZE, rd, sz+16);
#endif
	}

	for (sz=16; sz <= SZ_64K; sz<<=1) {
#ifndef T2RISC
		rd = rand_data();
                bd_fill_pattern(sram_saddr, GI_SRAM_SIZE, rd, 0);
#endif
		addr = (sz == SZ_64K) ? 0 : (~sz) & GI_64K_BMASK;
		ret |= test_one_block_rd(addr + sram_saddr);
#ifndef T2RISC
		if (addr != 0) bd_check_pattern(sram_saddr, addr, rd, 0);
		if ((addr+16) < GI_SRAM_SIZE)
			bd_check_pattern(sram_saddr, GI_SRAM_SIZE, rd, addr+16);
#endif
	}

	for (sz=16; sz < SZ_64K/2; sz<<=1) {
#ifndef T2RISC
		rd = rand_data();
                bd_fill_pattern(sram_saddr, GI_SRAM_SIZE, rd, 0);
#endif
		addr = (~sz) & GI_SRAM_BMASK;
		ret |= test_one_block_rd(addr + sram_saddr);
#ifndef T2RISC
		if (addr != 0) bd_check_pattern(sram_saddr, addr, rd, 0);
		if ((addr+16) < GI_SRAM_SIZE)
			bd_check_pattern(sram_saddr, GI_SRAM_SIZE, rd, addr+16);
#endif
	}

	return 0;
}


int test_one_block_wr(unsigned int saddr)
{
	unsigned int data[16], i, rback[16];

#ifndef T2RISC		
	for (i=4; i<16; i++) data[i] = 0;
#endif
	for (i=0; i<4; i++) {
		data[i] = rand_data();
#ifdef T2RISC
		IO_CACHE_WRITE(saddr+(i<<2), data[i]);
#endif			
	}

#ifndef T2RISC		
	sim_bwrite(saddr, 4, data);
	IO_READ(saddr);
#else
	FlushDCache(saddr, 16);
#endif

	for (i=0; i<4; i++)  {
#ifdef T2RISC		
		rback[i] = IO_READ(saddr+(i<<2));
#else
		rback[i] = BD_IO_READ(saddr+(i<<2));		
#endif	
		if (rback[i] != data[i]) {
#ifndef T2RISC		
			printf("Failed: bread err @0x%x exp=0x%x, rb=0x%x", 
				saddr+(i<<2), data[i], rback[i]);
#endif
			return i+1;
		}
	}

	return 0;
}

int test_sram_block_wr(unsigned int sram_saddr)
{
	unsigned int ret=0, sz, addr;

	for (sz=16; sz < GI_SRAM_SIZE; sz<<=1) {
		ret |= test_one_block_wr(sram_saddr+sz);
	}

	for (sz=16; sz <= SZ_64K; sz<<=1) {
		addr = (sz == SZ_64K) ? 0 : (~sz) & GI_64K_BMASK;
		ret |= test_one_block_wr(addr + sram_saddr);
	}

	for (sz=16; sz < SZ_64K/2; sz<<=1) {
		addr = (~sz) & GI_SRAM_BMASK;
		ret |= test_one_block_wr(addr + sram_saddr);
	}

	return 0;
}

int test_sram_mix(unsigned int sram_addr, int times)
{
	int ret = 0;

#ifndef T2RISC
	int i, type, k;
	unsigned int addr, data[16], rb, rbd[16];

	displayMsg("Sram random mix test");
	for (i=4; i<16; i++) data[i] = 0;
	
	for (k=0; k<times; k++) {
		type = rand_data() & 0x3;
		addr = (rand_data() % GI_SRAM_SIZE);

		switch (type) {
			case 3: /* block write */
				addr &= GI_SRAM_BADDR_MASK;
				for (i=0; i<4; i++) 
					data[i] = rand_data();
				sim_bwrite(addr + sram_addr, 4, data);
				IO_READ(sram_addr); /* Flush write */
				for (i=0; i<4; i++) {
					rb = IO_READ(sram_addr+addr+i*4);
					if (rb != data[i]) {
						printf("Failed(mix bw), @0x%x exp=0x%x d=0x%x \n",
							sram_addr+addr+i*4, data[i], rb);
						return -1;
					}
				}
				break;
			case 2: /* block bread */
				addr &= GI_SRAM_BADDR_MASK;
				for (i=0; i<4; i++) {
					data[i] = rand_data();
					BD_IO_WRITE(sram_addr+addr+i*4, data[i]);
				}
				sim_bread(addr + sram_addr, 4, rbd);
				for (i=0; i<4; i++) {
					if (rbd[i] != data[i]) {
						printf("Failed(mix br), @0x%x exp=0x%x d=0x%x \n",
							sram_addr+addr+i*4, data[i], rbd[i]);
						return -1;
					}
				}
				break;
			case 1: /* block single write */
				addr &= GI_SRAM_WADDR_MASK;
				data[0] = rand_data();
				IO_WRITE(addr+sram_addr, data[0]);
				IO_READ(sram_addr);
				rb = IO_READ(addr+sram_addr);
				if (rb != data[0]) {
					printf("Failed(mix sw), @0x%x exp=0x%x d=0x%x \n",
						addr+sram_addr, data[0], rb);
					return -1;
				}
				break;
			case 0: /* block single read */
				addr &= GI_SRAM_WADDR_MASK;
				data[0] = rand_data();
				BD_IO_WRITE(addr+sram_addr, data[0]);
				rb = IO_READ(addr+sram_addr);
				if (rb != data[0]) {
					printf("Failed(mix sr), @0x%x exp=0x%x d=0x%x \n",
						addr+sram_addr, data[0], rb);
					return -1;
				}
				break;
		}
		IO_STALL(rand_data() & 0xf);
	}
#endif

	return ret;
}

int gi_sram_tests()
{
	int ret = 0;
	unsigned int rdata = 0;

#ifndef T2RISC
	displayMsg("GI SRAM Single R/W test");
	rdata = rand_data();
	bd_fill_pattern(0, SYS_SDRAM_SIZE, rdata, 0);
#endif

	ret |= test_sram_single_rw(GI_SRAM_START, "sread",  1,  0);
	ret |= test_sram_single_rw(GI_SRAM_START, "swrite", 0,  1);
	ret |= test_sram_single_rw(GI_SRAM_START, "wr_rd",  0,  0);

	ret |= test_sram_block_rd(GI_SRAM_START);
	ret |= test_sram_block_wr(GI_SRAM_START);

#ifndef T2RISC
	test_sram_single_rw_all(GI_SRAM_START, "Sread all sram  W(bd) R", 1, 0);
	test_sram_single_rw_all(GI_SRAM_START, "sread all sram W R(bd)", 0, 1);
	test_sram_single_rw_all(GI_SRAM_START, "sread all sram W R", 0, 0);
	ret |= test_sram_mix(GI_SRAM_START, 0x80);

	displayMsg("GI SRAM Block R/W test");
#endif

	/* Test  Memory SRAM swapping */
#ifndef T2RISC
	displayMsg("Test GI SRAM memory swapping");
#endif
	IO_WRITE(GI_SEC_MODE_REG, GI_SEC_MODE_SECURE_MASK);
	IO_READ(GI_SEC_MODE_REG);   /* flush */

	ret |= test_sram_single_rw(GI_ROM_START, "sread",  1,  0);
	ret |= test_sram_single_rw(GI_ROM_START, "swrite", 0,  1);
	ret |= test_sram_single_rw(GI_ROM_START, "wr_rd",  0,  0);

	ret |= test_sram_block_rd(GI_ROM_START);
	ret |= test_sram_block_wr(GI_ROM_START);

#ifndef  T2RISC
	ret |= test_sram_mix(GI_ROM_START, 0x20);

	if (bd_check_pattern(0, SYS_SDRAM_SIZE, rdata, 0)) {
		 printf("Failed: SDRAM change during sram test \n");
	}
#endif

	IO_WRITE(GI_SEC_MODE_REG, 
		 GI_SEC_MODE_SECURE_MASK | GI_SEC_MODE_RESET_MASK);
	IO_READ(GI_SEC_MODE_REG);   /* flush */

	return ret;
}
