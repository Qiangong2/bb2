/* BB2 GI reset checkup */

#ifndef T2RISC
	#include <stdlib.h>
	#include <stdio.h>
	#include <string.h>
	#include "iosim.h"
#endif

#include "utils.h"
#include "gi.h"
#include "gi_test.h"
#include "gi_utils.h"
#include "gi_reg_default.h"

int check_sec_reg(int sec_mode)
{
	int ret, deflt_mask, deflt;

	if (sec_mode) {
		deflt = GI_SEC_MODE_REG_SEC_DFLT;
		deflt_mask = GI_SEC_MODE_REG_SEC_DFLT_MASK;
#ifndef T2RISC
		if (ipc_get_data(BD_GET_ALT_BOOT, 0)) {
			deflt |=  GI_SEC_MODE_ALT_BOOT_MASK;
			deflt_mask |= GI_SEC_MODE_ALT_BOOT_MASK;
		}

		if (ipc_get_data(BD_GET_TEST_IN, 0)) {
			deflt |= GI_SEC_MODE_TEST_ENA_MASK;
			deflt_mask |= GI_SEC_MODE_TEST_ENA_MASK;
		}
#endif
		ret  = read_compare(GI_SEC_MODE_REG, deflt, deflt_mask);
		ret |= read_compare(GI_SEC_TIMER_REG, GI_SEC_TIMER_REG_SEC_DFLT, 
					GI_SEC_TIMER_REG_SEC_DFLT_MASK);
		ret |= read_compare(GI_SEC_SRAM_REG, GI_SEC_SRAM_REG_SEC_DFLT, 
					GI_SEC_SRAM_REG_SEC_DFLT_MASK);
		ret |= read_compare(GI_SEC_ENTER_REG, GI_SEC_ENTER_REG_DFLT, 
					GI_SEC_ENTER_REG_DFLT_MASK);
		ret = (ret) ? GI_SEC_REG_SEC_RST_FAIL : 0;
	} else {
		ret  = read_compare(GI_SEC_MODE_REG, GI_SEC_MODE_REG_NS_DFLT, 
					GI_SEC_MODE_REG_NS_DFLT_MASK);
		ret |= read_compare(GI_SEC_TIMER_REG, GI_SEC_TIMER_REG_NS_DFLT, 
					GI_SEC_TIMER_REG_NS_DFLT_MASK);
		ret |= read_compare(GI_SEC_SRAM_REG, GI_SEC_SRAM_REG_NS_DFLT, 
					GI_SEC_SRAM_REG_NS_DFLT_MASK);
		ret |= read_compare(GI_SEC_ENTER_REG, GI_SEC_ENTER_REG_DFLT, 
					GI_SEC_ENTER_REG_DFLT_MASK);
		ret = (ret) ? GI_SEC_REG_NS_RST_FAIL : 0;
	}
	
	return ret;
}

/* Please be aware di host/flipper also after reset */
int check_di_reg(int sec_mode)
{
	int ret;

	ret  = read_compare(GI_DI_CONF_REG, GI_DI_CONF_REG_DFLT, 
				GI_DI_CONF_REG_DEFL_MASK);	
	ret |= read_compare(GI_DI_CTRL_REG, GI_DI_CTRL_REG_DFLT, 
				GI_DI_CTRL_REG_DEFL_MASK);
	ret |= read_compare(GI_DI_BE_REG, GI_DI_BE_REG_DFLT, 
				GI_DI_BE_REG_DFLT_MASK);
	
	return ret ? GI_DI_REG_RST_FAIL : 0;
}

int check_bi_reg(int sec_mode)
{
	int ret;

	ret  = read_compare(GI_BI_INTR_REG, GI_BI_CTRL_REG_DFLT,
				GI_BI_CTRL_REG_DFLT_MASK);	
	ret |= read_compare(GI_BI_MEM_REG, GI_BI_MEM_REG_DFLT, 
				GI_BI_MEM_REG_DFLT_MASK);
	ret |= read_compare(GI_BI_INTR_REG, GI_BI_INTR_REG_DFLT,
				GI_BI_INTR_REG_DFLT_MASK);
	
	return ret ? GI_BI_REG_RST_FAIL : 0;
}

int check_aes_reg(int sec_mode)
{
	int ret;
	
	ret  = read_compare(GI_AESD_CTRL_REG, GI_AESD_CTRL_REG_DFLT,
				GI_AESD_CTRL_REG_DFLT_MASK);
	ret |= read_compare(GI_AESD_IV_REG, GI_AESD_IV_REG_DFLT,
				GI_AESD_IV_REG_DFLT_MASK);
	ret |= read_compare(GI_AESE_CTRL_REG, GI_AESE_CTRL_REG_DFLT,
				GI_AESE_CTRL_REG_DFLT_MASK);
	ret |= read_compare(GI_AESE_IV_REG, GI_AESE_IV_REG_DFLT,
				GI_AESE_IV_REG_DFLT_MASK);
	ret |= read_compare(GI_AES_KEXP_REG, GI_AES_KEXP_REG_DFLT,
				GI_AES_KEXP_REG_DFLT_MASK);

	return ret ? GI_AES_REG_RST_FAIL : 0;
}

int check_mc_reg(int sec_mode)
{
	int ret;

	ret  = read_compare(GI_MC_CTRL_REG, GI_MC_CTRL_REG_DFLT,
				GI_MC_CTRL_REG_DFLT_MASK);
	ret |= read_compare(GI_MC_ADDR_REG, GI_MC_ADDR_REG_DFLT,
				GI_MC_ADDR_REG_DFLT_MASK);
	ret |= read_compare(GI_MC_BUF_REG, GI_MC_BUF_REG_DFLT,
				GI_MC_BUF_REG_DFLT_MASK);

	return ret ? GI_MC_REG_RST_FAIL : 0;
}

int check_sha_reg(int sec_mode)
{
	int ret;
	
	ret  = read_compare(GI_SHA_CTRL_REG, GI_SHA_CTRL_REG_DFLT,
				GI_SHA_CTRL_REG_DFLT_MASK);
	ret  = read_compare(GI_SHA_BUF_REG, GI_SHA_BUF_REG_DFLT,
				GI_SHA_BUF_REG_DFLT_MASK);

	return ret ? GI_SHA_REG_RST_FAIL : 0;
}

int check_gi_reg(int sec_mode)
{
	int ret;
	
	ret  = read_compare(GI_DC_CTRL_REG, GI_DC_CTRL_REG_DFLT,
				GI_DC_CTRL_REG_DFLT_MASK);
	ret |= read_compare(GI_DC_BUF_REG, GI_DC_BUF_REG_DFLT,
				GI_DC_BUF_REG_DFLT_MASK);
	ret |= read_compare(GI_DC_HADDR_REG, GI_DC_HADDR_REG_DFLT,
				GI_DC_HADDR_REG_DFLT_MASK);
	ret |= read_compare(GI_DC_HIDX_REG, GI_DC_HIDX_REG_DFLT,
				GI_DC_HIDX_REG_DFLT_MASK);
	ret |= read_compare(GI_DC_DIOFF_REG, GI_DC_DIOFF_REG_DFLT,
				GI_DC_DIOFF_REG_DFLT_MASK);
	ret |= read_compare(GI_DC_IV_REG, GI_DC_IV_REG_DFLT,
				GI_DC_IV_REG_DFLT_MASK);
	ret |= read_compare(GI_DC_H0_REG, GI_DC_H0_REG_DFLT,
				GI_DC_H0_REG_DFLT_MASK);
	ret |= read_compare(GI_DC_H1_REG, GI_DC_H1_REG_DFLT,
				GI_DC_H1_REG_DFLT_MASK);
	
	return ret ? GI_DC_REG_RST_FAIL : 0;
}

int check_ais_reg(int sec_mode)
{
	int ret;

	ret  = read_compare(GI_AIS_CTRL_REG, GI_AIS_CTRL_REG_DFLT,
				GI_AIS_CTRL_REG_DFLT_MASK);
	ret |= read_compare(GI_AIS_PTR0_REG,  GI_AIS_PTR0_REG_DFLT,
				GI_AIS_PTR0_REG_DFLT_MASK);
	ret |= read_compare(GI_AIS_PTR1_REG, GI_AIS_PTR1_REG_DFLT,
				GI_AIS_PTR1_REG_DFLT_MASK);

	return ret ? GI_AIS_REG_RST_FAIL : 0;
	
}

/* Checkup GI register after reset */
int gi_reset_checkup()
{
	int ret, sec;

#ifndef T2RISC
	int rd = rand_data();
	int rd_s = rand_data();
	bd_fill_pattern(0, SYS_SDRAM_SIZE, rd, 0);
	bd_fill_pattern(GI_SRAM_START, GI_SRAM_SIZE, rd_s, 0);

	displayMsg("GI checkup after reset ");	
	sec = ipc_get_data(BD_GET_TEST_IN, 0);
	if (sec) {
		displayMsg("In Secure mode since test_in is active");
		ipc_get_compare(BD_GI_SECURE_BIT, 0, 1, WORD_MASK);	
	}
	

#else
	sec = IO_READ(GI_SEC_MODE_REG) & GI_SEC_MODE_SECURE_MASK;
#endif

	ret |= check_sec_reg(sec);
	ret |= check_di_reg(sec);
	ret |= check_bi_reg(sec);
	ret |= check_aes_reg(sec);
	ret |= check_mc_reg(sec);
	ret |= check_sha_reg(sec);
	ret |= check_ais_reg(sec);

#ifndef T2RISC
	bd_check_pattern(0, SYS_SDRAM_SIZE, rd, 0);
	bd_check_pattern(GI_SRAM_START, GI_SRAM_SIZE, rd_s, 0);
#endif
	if (sec) leave_sec_mode();
	else enter_sec_mode(1);

#ifndef T2RISC
	if (sec) {
		displayMsg("Should be in non-secure mode");
		ipc_get_compare(BD_GI_SECURE_BIT, 0, 0, WORD_MASK); 		
	}
#endif	
	sec = (sec) ? 0 : 1;
	ret |= check_sec_reg(sec);
	ret |= check_di_reg(sec);
	ret |= check_bi_reg(sec);
	ret |= check_aes_reg(sec);
	ret |= check_mc_reg(sec);
	ret |= check_sha_reg(sec);
	ret |= check_ais_reg(sec);

#ifndef T2RISC
	bd_check_pattern(0, SYS_SDRAM_SIZE, rd, 0);
	bd_check_pattern(GI_SRAM_START, GI_SRAM_SIZE, rd_s, 0);
#endif
	if (sec) leave_sec_mode();
	else enter_sec_mode(0);

	return 0;
}
