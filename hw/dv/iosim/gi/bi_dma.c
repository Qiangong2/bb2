/* BB2 BI DMA test */

#ifndef T2RISC
	#include <stdlib.h>
	#include <stdio.h>
	#include <string.h>
	#include "iosim.h"
#endif

#include "utils.h"
#include "gi.h"
#include "gi_test.h"
#include "gi_utils.h"

int g_rand_seed = 0;
int g_mrand_seed = 0;
int g_pat_check = 0;
int polling_bi_intr(int to)
{
	int i;
	unsigned int ret;
	
	for (i=0; i<to; i+=10) {
		ret = IO_READ(GI_BI_INTR_REG);
#ifdef BB2_FPGA
		//soc_printf("Check bi intr @0x%x return 0x%x \n", GI_BI_INTR_REG, ret);
		if (i > 768*200) break;
#endif
		if (ret & GI_BI_INTR_BI_INTR_MASK) 
			return 0;
#ifndef T2RISC   
		IO_STALL(10);
#endif
	}

	if (to == 0)  return 0;
#ifndef T2RISC   
	printf("Failed: polling BI interrupt timeout last=0x%x\n", ret);
#endif

#ifdef BB2_FPGA
	soc_printf("Failed: polling BI interrupt timeout last=0x%x\n", ret);
#endif
	return -1;
}

int polling_bi_nobusy(int to)
{
	int i;
	unsigned int ret;
	
	for (i=0; i<to; i+=10) {
		ret = IO_READ(GI_BI_CTRL_REG);
#ifdef BB2_FPGA
		//soc_printf("Check bi busy @0x%x return 0x%x \n", GI_BI_CTRL_REG, ret);
		if (i > 768*200) break;
#endif
		if (!(ret & GI_BI_CTRL_EXEC_MASK)) 
			return 0;
#ifndef T2RISC   
		IO_STALL(10);
#endif
	}

	if (to == 0)  return 0;
#ifndef T2RISC   
	printf("Failed: polling BI busy bit timeout last=0x%x\n", ret);
#endif

#ifdef BB2_FPGA  
	soc_printf("Failed: polling BI busy bit timeout last=0x%x\n", ret);
#endif
	return -1;
}

int mmmm = 0;
int do_bi_dma(            /* BI DMA transactions */
	unsigned int sram_off,      /* ptr in gi buffer */
	unsigned int mem_off,       /* sdram offset */
	unsigned int size,          /* size of data(bytes) */
	int sram_to_mem,            /* sram to main memory */ 
	int wait_intr)              /* 1-interrupt 0-polling */
{
	unsigned int ctrl;
	int sz;
#ifndef T2RISC
	char msg[64];

	sprintf(msg, "BI DMA(%d) sram=0x%x mem=0x%x sz=%d %s", mmmm++,
		sram_off, mem_off, size, sram_to_mem ? "to_mem" : "to_gi");
	displayMsg(msg);
	printf("%s\n", msg);
#endif

#ifdef BB2_FPGA
	mmmm++;
	if ((mmmm % 10000) == 0) 
	   soc_printf("BI DMA(%d) sram=0x%x mem=0x%x sz=%d %s\n", mmmm,
		sram_off, mem_off, size, sram_to_mem ? "to_mem" : "to_gi"); 
#endif

	IO_WRITE(GI_BI_MEM_REG, mem_off & GI_BI_MEM_MEM_MASK);

	  /* assemble ctrl register */
	ctrl = GI_BI_CTRL_EXEC_MASK;
	if (wait_intr) ctrl |= GI_BI_CTRL_INTR_MASK;
	sz = (size >> 7) - 1; 	/* how many blocks */
	if (sz < 0) sz = 0;
	ctrl |= (sz << GI_BI_CTRL_SIZE_SHIFT) & GI_BI_CTRL_SIZE_MASK;
	if (sram_to_mem) ctrl |= GI_BI_CTRL_WRITE_MASK;
	ctrl |= sram_off & GI_BI_CTRL_PTR_MASK;
#ifdef BB2_FPGA
	//soc_printf("Write BI ctrl register 0x%x 0x%x\n", GI_BI_CTRL_REG, ctrl);
#endif
	IO_WRITE(GI_BI_CTRL_REG, ctrl);

	return 0;	
}


int bi_dma_issue(                 /* issue bi DMA */
	int sz,                   /* dma size */
	unsigned int sram_off,    /* sram offset */
	unsigned int mem_off,     /* mem offset */  
	int sram_to_mem,          /* sram to memory */
	unsigned int *pdata,      /* random data */
	int wait_intr,            /* interrupt enable? */
	int bd_setup)             /* back door setup data */
{
	int wsz;
		
	wsz = sz / sizeof(int);	
	if (bd_setup) {
		create_random_data(pdata, sz/sizeof(int));
#ifndef T2RISC	
		if (sram_to_mem) bd_write_to_mem(sram_off + GI_SRAM_START, pdata, wsz);
		else bd_write_to_mem(mem_off, pdata, wsz);
#else 
#ifdef BB2_FPGA
		if (sram_to_mem) write_to_mem(sram_off + GI_SRAM_START, pdata, wsz);
		else write_to_mem(mem_off | 0xA0000000, pdata, wsz);
#else
		if (sram_to_mem) write_to_mem(sram_off + GI_SRAM_START, pdata, wsz);
		else write_to_mem(mem_off, pdata, wsz); 
#endif
#endif	
	}

	do_bi_dma(sram_off, mem_off, sz, sram_to_mem, wait_intr);

	return 0;
}

int bi_dma_check(                 /* BI dma check result */
	int sz,                   /* size */
	unsigned int sram_off,    /* sram offset */
	unsigned int mem_off,     /* mem offset */  
	int sram_to_mem,          /* sram to memory */
	unsigned int *pdata,      /* random data */
	int wait_intr,            /* wait for interrupt */
	int bd_checkup)           /* backdoor checkup */
{
	int wsz, i;
	unsigned int ret;
		
	wsz = sz / sizeof(int);		
	if (wait_intr) polling_bi_intr(GI_BI_BLK_TO * sz / GI_BI_BLK_SZ);
	else polling_bi_nobusy(GI_BI_BLK_TO * sz / GI_BI_BLK_SZ);

	if (bd_checkup) {
#ifdef T2RISC
		if (sram_to_mem) {
			for (i=0; i<sz; i += sizeof(int) )
#ifdef BB2_FPGA 
				if (IO_READ(mem_off+i+0xA0000000) != pdata[i>>2]) {
#else
				if (IO_READ(mem_off+i) != pdata[i>>2]) {
#endif
					return i+1;
				}
		} else {
			for (i=0; i<sz; i += sizeof(int) )
				if (IO_READ(sram_off+i+GI_SRAM_START) != pdata[i>>2]) {
					return i+1;
				}
		}
#else
		IO_STALL(100);
		if (sram_to_mem) bd_check_mem_data(mem_off, pdata, wsz);
		else bd_check_mem_data(sram_off + GI_SRAM_START, pdata, wsz);
#endif
	}
		
	if (wait_intr) {  /* clear bi interrupt */
		IO_WRITE(GI_BI_CTRL_REG, 0);
		for (i=0; i<3; i++) {
			ret = IO_READ(GI_BI_INTR_REG);
			if (!(ret & GI_BI_INTR_BI_INTR_MASK)) break;
		}
#ifndef T2RISC
		if (i >= 3) printf("Failed: clear interrupt timeout \n");
#endif

#ifdef BB2_FPGA
		if (i >= 3) soc_printf("Failed: clear interrupt timeout \n");
#endif
	}
#ifndef T2RISC
	if (g_pat_check) {
		if (sram_off) 
			bd_check_pattern(GI_SRAM_START, sram_off, g_rand_seed, 0);
		if ((sram_off+sz) < GI_SRAM_SIZE)
			bd_check_pattern(GI_SRAM_START, GI_SRAM_SIZE, g_rand_seed, sram_off+sz);

		if (mem_off)
			bd_check_pattern(0, mem_off, g_mrand_seed, 0);

		if ((mem_off + sz) < SYS_SDRAM_SIZE)
			bd_check_pattern(0, SYS_SDRAM_SIZE, g_mrand_seed, mem_off + sz);
	}
#endif

	return 0;
}

#ifndef T2RISC
int polling_dma_blk(int stop_blk, int total_blk)
{
	unsigned int ret, chk;
	int to, i;

	to = GI_BI_BLK_TO * stop_blk;
	chk = total_blk - 1 - stop_blk;
	printf("chk = %d \n", chk);
	for (i=0; i<to; i+=10) {
		ret = ipc_get_data(BD_BI_SIZE, 0);
		if (ret == 0x3ff) { /* dma done */
			printf("Failed: DMA already done!!!\n");
		}
		if (ret <= chk) return 0;
		IO_STALL(10);
	}

	if (to == 0) return 0;	
	printf("Failed: polling BI DMA stop blk %d (cur=%d) timeout\n", stop_blk, ret);
	return -1;
}
#else
	char gi_sram_test_buf[GI_SRAM_SIZE];
#endif

extern int rand_times;
int bi_dma_test_overlap(int times)
{
	int max_blk, i, blk, sz, stop_blk, ret = 0;
	unsigned int sram_off, mem_off, sram_to_mem, intr;
	unsigned int *pdata;

#ifndef T2RISC
	displayMsg("BI regular DMA stopping test");
	pdata = (unsigned int *) malloc(GI_SRAM_SIZE);
	if (pdata == NULL) {
		printf("Failed: cannot alloc enough memory ... \n");
		return -1;
	}
#else
	pdata = (unsigned int *) gi_sram_test_buf;
#endif

#ifdef BB2_FPGA
	soc_printf("BI regular DMA stopping test\n");
#endif
	max_blk = GI_SRAM_SIZE / GI_BI_BLK_SZ;

	for (i=0; i<=times; i++) {
#ifndef T2RISC
		printf(" #### %d %d ##### \n", i, rand_times);
#endif
		blk = rand_data() % max_blk + 1;
		sz = blk * GI_BI_BLK_SZ;
	
		sram_off = get_random_addr(0, GI_SRAM_SIZE, sz) & GI_BI_BLK_MASK;
#ifndef T2RISC
		mem_off  = get_random_addr(0, SYS_SDRAM_SIZE, sz) & GI_BI_BLK_MASK;
#else
		sz = GI_BI_BLK_SZ * 8;
		mem_off  = get_random_addr(CPU_TEST_SDRAM_ADDR,CPU_TEST_SDRAM_SIZE,sz)	
			   & GI_BI_BLK_MASK;	
#endif
		sram_to_mem = rand_data() & 1;
		intr = rand_data() & 1;
		bi_dma_issue(sz, sram_off, mem_off, sram_to_mem, pdata, intr, 1);

#ifndef T2RISC
		stop_blk = rand_data() % blk;
		IO_READ(GI_BI_CTRL_REG);
		polling_dma_blk(stop_blk, blk);
#else
		/* Just wait a while */
		IO_READ(GI_BI_CTRL_REG);
#endif
		/* issue another dma */
#ifndef T2RISC
		if ((blk - stop_blk) > 4) {
			displayMsg("Issue overlapped DMA request");
#else 
		{  /* 8 block dma, should be enough time */
#endif

#ifdef BB2_FPGA
			soc_printf("Issue overlapped DMA request\n");
#endif

			bi_dma_issue((rand_data() % max_blk + 1) * GI_BI_BLK_SZ, 
				get_random_addr(0, GI_SRAM_SIZE, sz) & GI_BI_BLK_MASK,
				get_random_addr(0, SYS_SDRAM_SIZE, sz) & GI_BI_BLK_MASK,
				rand_data() & 1, pdata, rand_data() & 1, 0);
		}

		intr = bi_dma_check(sz, sram_off, mem_off, sram_to_mem, pdata, intr, 1);
		ret |= intr;
#ifdef BB2_FPGA
		if (intr) soc_printf("DMA data check failed ret =%d \n", intr);
#endif
		
	}

#ifndef T2RISC
	free(pdata);
#endif
	return ret;
}

int bi_dma_test_stop(int times)
{
	int max_blk, i, blk, sz, stop_blk, ret=0;
	unsigned int sram_off, mem_off, sram_to_mem, intr;
	unsigned int *pdata;

#ifndef T2RISC
	displayMsg("BI regular DMA stopping test");
	pdata = (unsigned int *) malloc(GI_SRAM_SIZE);
	if (pdata == NULL) {
		printf("Failed: cannot alloc enough memory ... \n");
		return -1;
	}
#else
	pdata = (unsigned int *) gi_sram_test_buf;	
#endif
	
#ifdef BB2_FPGA
	soc_printf("BI regular DMA stopping test\n");
#endif
	max_blk = GI_SRAM_SIZE / GI_BI_BLK_SZ;

	for (i=0; i<=times; i++) {
#ifndef T2RISC
		printf(" #### %d %d ##### \n", i, rand_times);
		printf("(%d) Start regular dam and stop \n", i);
#endif
#ifdef BB2_FPGA
		soc_printf("(%d) Start regular dam and stop \n", i);
#endif
		blk = rand_data() % max_blk + 1;
		sz = blk * GI_BI_BLK_SZ;
	
		sram_off = get_random_addr(0, GI_SRAM_SIZE, sz) & GI_BI_BLK_MASK;
#ifndef T2RISC
		mem_off  = get_random_addr(0, SYS_SDRAM_SIZE, sz) & GI_BI_BLK_MASK;
#else
		sz = GI_BI_BLK_SZ;
		mem_off  = get_random_addr(CPU_TEST_SDRAM_ADDR,CPU_TEST_SDRAM_SIZE,sz)	
			   & GI_BI_BLK_MASK;	
#endif
		sram_to_mem = rand_data() & 1;
		intr = rand_data() & 1;
		bi_dma_issue(sz, sram_off, mem_off, sram_to_mem, pdata, intr, 1);

		stop_blk = rand_data() % blk;
		sz = stop_blk * GI_BI_BLK_SZ / sizeof(int);
#ifndef T2RISC
		IO_READ(GI_BI_CTRL_REG);
		polling_dma_blk(stop_blk, blk);
#else
		/* Just wait a while */
                IO_READ(GI_BI_CTRL_REG);
#endif

		IO_WRITE(GI_BI_CTRL_REG, 0);   

#ifndef T2RISC
		polling_bi_nobusy(GI_BI_BLK_TO * (blk - stop_blk + 1)); 
		if ((blk - stop_blk) > 4) {
			if (sram_to_mem) bd_check_mem_data(mem_off, pdata, sz);
			else bd_check_mem_data(sram_off + GI_SRAM_START, pdata, sz);
		}
#else
		polling_bi_nobusy(GI_BI_BLK_TO); 
#endif

		/* Check regular dma */
#ifndef T2RISC
		printf("Sanity check \n");
#endif
#ifdef BB2_FPGA
		soc_printf("Sanity check \n");
#endif

		sz = (rand_data() % max_blk + 1) * GI_BI_BLK_SZ;
		sram_off = get_random_addr(0, GI_SRAM_SIZE, sz) & GI_BI_BLK_MASK;
#ifndef T2RISC
		mem_off  = get_random_addr(0, SYS_SDRAM_SIZE, sz) & GI_BI_BLK_MASK;
#else
		sz = GI_BI_BLK_SZ;
		mem_off  = get_random_addr(CPU_TEST_SDRAM_ADDR,CPU_TEST_SDRAM_SIZE,sz)	
			   & GI_BI_BLK_MASK;	
#endif
		sram_to_mem = rand_data() & 1;
		intr = rand_data() & 1;

		bi_dma_issue(sz, sram_off, mem_off, sram_to_mem, pdata, intr, 1);
		intr = bi_dma_check(sz, sram_off, mem_off, sram_to_mem, pdata, intr, 1);
		ret |= intr;
#ifdef BB2_FPGA
		if (intr) soc_printf("DMA data check failed ret =%d \n", intr);
#endif
	}

#ifndef T2RISC
	free(pdata);
#endif
	return ret;
}

int bi_dma_test_typical()
{
	int max_blk, sz, cur_max, ret=0;
	unsigned int sram_off, mem_off, sram_to_mem, intr, addr;
	unsigned int *pdata;

#ifdef T2RISC
	unsigned int offs;

	pdata = (unsigned int *) gi_sram_test_buf;	
#else
	displayMsg("BI typical(DRAM lines) DMA test");
	pdata = (unsigned int *) malloc(GI_SRAM_SIZE);
	if (pdata == NULL) {
		printf("Failed: cannot alloc enough memory ... \n");
		return -1;
	}
	g_pat_check = 1;
#endif
#ifdef BB2_FPGA
	soc_printf("BI typical(DRAM lines) DMA test\n");
#endif	
	max_blk = GI_SRAM_SIZE / GI_BI_BLK_SZ;

#ifdef BB2_FPGA
	soc_printf("1 cross each SDRAM address line\n");
#endif
#ifdef T2RISC
	for (offs=128; offs<CPU_TEST_SDRAM_SIZE; offs<<=1) {
		mem_off = offs + CPU_TEST_SDRAM_ADDR;
		mem_off = 0x70000;
		soc_printf("Set mem offset=0x70000\n");
		cur_max = (CPU_TEST_SDRAM_SIZE - mem_off) / GI_BI_BLK_SZ;
		sz = GI_BI_BLK_SZ;
#else
	printf("1 cross each SDRAM address line\n");
	for (mem_off=128; mem_off<SYS_SDRAM_SIZE; mem_off<<=1) {
		cur_max = (SYS_SDRAM_SIZE - mem_off) / GI_BI_BLK_SZ;
		if (cur_max > max_blk) cur_max = max_blk;
		if (cur_max > 1) sz = (rand_data() % cur_max + 1) * GI_BI_BLK_SZ;
		else sz = GI_BI_BLK_SZ;
		g_rand_seed = rand_data();
		g_mrand_seed = rand_data();
		bd_fill_pattern(GI_SRAM_START, GI_SRAM_SIZE, g_rand_seed, 0);
		bd_fill_pattern(0, SYS_SDRAM_SIZE, g_mrand_seed, 0);
#endif
		sram_off = get_random_addr(0, GI_SRAM_SIZE, sz) & GI_BI_BLK_MASK;

		sram_to_mem = rand_data() & 1;
		intr = rand_data() & 1;

		bi_dma_issue(sz, sram_off, mem_off, sram_to_mem, pdata, intr, 1);
		intr = bi_dma_check(sz, sram_off, mem_off, sram_to_mem, pdata, intr, 1);
		ret |= intr;
#ifdef BB2_FPGA
		if (intr) soc_printf("DMA data check failed ret =%d \n", intr);
#endif
	}

#ifndef T2RISC
	printf("0 cross each SDRAM address line\n");
	for (addr=128; addr<SYS_SDRAM_SIZE; addr<<=1) {
		mem_off = ((~addr) & SYS_SDRAM_MASK) & GI_BI_BLK_MASK;
		cur_max = (SYS_SDRAM_SIZE - mem_off) / GI_BI_BLK_SZ;
		if (cur_max > max_blk) cur_max = max_blk;
		if (cur_max <= 1) sz = GI_BI_BLK_SZ; 
		else sz = (rand_data() % cur_max + 1) * GI_BI_BLK_SZ;
		g_rand_seed = rand_data();
		g_mrand_seed = rand_data();
		bd_fill_pattern(GI_SRAM_START, GI_SRAM_SIZE, g_rand_seed, 0);
		bd_fill_pattern(0, SYS_SDRAM_SIZE, g_mrand_seed, 0);
#else
	for (addr=128; addr<CPU_TEST_SDRAM_SIZE; addr<<=1) {
		mem_off = ((~addr) & CPU_TEST_SDRAM_MASK) & GI_BI_BLK_MASK;
		mem_off += CPU_TEST_SDRAM_ADDR;
		sz = GI_BI_BLK_SZ;
#endif
		sram_off = get_random_addr(0, GI_SRAM_SIZE, sz) & GI_BI_BLK_MASK;

		sram_to_mem = rand_data() & 1;
		intr = rand_data() & 1;

		bi_dma_issue(sz, sram_off, mem_off, sram_to_mem, pdata, intr, 1);
		intr = bi_dma_check(sz, sram_off, mem_off, sram_to_mem, pdata, intr, 1);
		ret |= intr;
#ifdef BB2_FPGA
		if (intr) soc_printf("DMA data check failed ret =%d \n", intr);
#endif
	}
	/* different size */
#ifndef T2RISC
	printf("Check different DMA size\n");
#endif

	for (sz=128; sz<=GI_SRAM_SIZE; sz<<=1) {
#ifndef T2RISC
		mem_off  = get_random_addr(0, SYS_SDRAM_SIZE, sz) & GI_BI_BLK_MASK;
		g_rand_seed = rand_data();
		g_mrand_seed = rand_data();
		bd_fill_pattern(GI_SRAM_START, GI_SRAM_SIZE, g_rand_seed, 0);
		bd_fill_pattern(0, SYS_SDRAM_SIZE, g_mrand_seed, 0);
#else
		mem_off  = get_random_addr(CPU_TEST_SDRAM_ADDR,CPU_TEST_SDRAM_SIZE,sz)	
			   & GI_BI_BLK_MASK;	
#endif
		sram_off = get_random_addr(0, GI_SRAM_SIZE, sz) & GI_BI_BLK_MASK;

		sram_to_mem = rand_data() & 1;
		intr = rand_data() & 1;
		bi_dma_issue(sz, sram_off, mem_off, sram_to_mem, pdata, intr, 1);
		intr = bi_dma_check(sz, sram_off, mem_off, sram_to_mem, pdata, intr, 1);
		ret |= intr;
#ifdef BB2_FPGA
		if (intr) soc_printf("DMA data check failed ret =%d \n", intr);
#endif
	}
	sz = GI_SRAM_SIZE;
	sram_off = 0;
#ifndef T2RISC
	g_rand_seed = rand_data();
	g_mrand_seed = rand_data();
	bd_fill_pattern(GI_SRAM_START, GI_SRAM_SIZE, g_rand_seed, 0);
	bd_fill_pattern(0, SYS_SDRAM_SIZE, g_mrand_seed, 0);
	mem_off  = get_random_addr(0, SYS_SDRAM_SIZE, sz) & GI_BI_BLK_MASK;
#else
	mem_off  = get_random_addr(CPU_TEST_SDRAM_ADDR,CPU_TEST_SDRAM_SIZE,sz)	
		   & GI_BI_BLK_MASK;	
#endif
	sram_to_mem = rand_data() & 1;
	intr = rand_data() & 1;
	bi_dma_issue(sz, sram_off, mem_off, sram_to_mem, pdata, intr, 1);
	intr = bi_dma_check(sz, sram_off, mem_off, sram_to_mem, pdata, intr, 1);
	ret |= intr;
#ifdef BB2_FPGA
	if (intr) soc_printf("DMA data check failed ret =%d \n", intr);
#endif

#ifndef T2RISC
	printf("1 -- cross sram address line \n");
#endif
	for (sram_off=128; sram_off<GI_SRAM_SIZE; sram_off<<=1) {
		cur_max = (GI_SRAM_SIZE - sram_off) / GI_BI_BLK_SZ;
		if (cur_max > max_blk) cur_max = max_blk;
		if (cur_max > 1) sz = (rand_data() % cur_max + 1) * GI_BI_BLK_SZ;
		else sz = GI_BI_BLK_SZ;
#ifndef T2RISC
		mem_off  = get_random_addr(0, SYS_SDRAM_SIZE, sz) & GI_BI_BLK_MASK;
		g_rand_seed = rand_data();
		g_mrand_seed = rand_data();
		bd_fill_pattern(GI_SRAM_START, GI_SRAM_SIZE, g_rand_seed, 0);
		bd_fill_pattern(0, SYS_SDRAM_SIZE, g_mrand_seed, 0);
#else
		mem_off  = get_random_addr(CPU_TEST_SDRAM_ADDR,CPU_TEST_SDRAM_SIZE,sz)	
			   & GI_BI_BLK_MASK;	
		sz = GI_BI_BLK_SZ;	
#endif
		sram_to_mem = rand_data() & 1;
		intr = rand_data() & 1;

		bi_dma_issue(sz, sram_off, mem_off, sram_to_mem, pdata, intr, 1);
		intr = bi_dma_check(sz, sram_off, mem_off, sram_to_mem, pdata, intr, 1);
		ret |= intr;
#ifdef BB2_FPGA
		if (intr) soc_printf("DMA data check failed ret =%d \n", intr);
#endif
	}

#ifndef T2RISC
	printf("0 - cross sram address line \n");
#endif
	for (addr=128; addr<SZ_64K; addr<<=1) {
		sram_off = ((~addr) & (SZ_64K - 1)) & GI_BI_BLK_MASK;
		cur_max = (GI_SRAM_SIZE - sram_off) / GI_BI_BLK_SZ;
		if (cur_max > max_blk) cur_max = max_blk;
		if (cur_max > 1) sz = (rand_data() % cur_max + 1) * GI_BI_BLK_SZ;
		else sz = GI_BI_BLK_SZ;
#ifndef T2RISC
		mem_off  = get_random_addr(0, SYS_SDRAM_SIZE, sz) & GI_BI_BLK_MASK;
		g_rand_seed = rand_data();
		g_mrand_seed = rand_data();
		bd_fill_pattern(GI_SRAM_START, GI_SRAM_SIZE, g_rand_seed, 0);
		bd_fill_pattern(0, SYS_SDRAM_SIZE, g_mrand_seed, 0);
#else
		mem_off  = get_random_addr(CPU_TEST_SDRAM_ADDR,CPU_TEST_SDRAM_SIZE,sz)	
			   & GI_BI_BLK_MASK;	
		sz = GI_BI_BLK_SZ;
#endif

		sram_to_mem = rand_data() & 1;
		intr = rand_data() & 1;

		bi_dma_issue(sz, sram_off, mem_off, sram_to_mem, pdata, intr, 1);
		intr = bi_dma_check(sz, sram_off, mem_off, sram_to_mem, pdata, intr, 1);
		ret |= intr;
#ifdef BB2_FPGA
		if (intr) soc_printf("DMA data check failed ret =%d \n", intr);
#endif
	}

#ifndef T2RISC
	free(pdata);
	g_pat_check = 0;
#endif
	return ret;
}


int bi_dma_test_regular(int times)
{
	int max_blk, i, sz, ret=0;
	unsigned int sram_off, mem_off, sram_to_mem, intr;
	unsigned int *pdata;

#ifndef T2RISC
	displayMsg("BI regular DMA test");
	pdata = (unsigned int *) malloc(GI_SRAM_SIZE);
	if (pdata == NULL) {
		printf("Failed: cannot alloc enough memory ... \n");
		return -1;
	}
#else
	pdata = (unsigned int *) gi_sram_test_buf;	
#endif

#ifdef BB2_FPGA
	soc_printf("BI regular DMA test\n");
#endif

	max_blk = GI_SRAM_SIZE / GI_BI_BLK_SZ;
	
	for (i=0; i<=times; i++) {
		sz = (rand_data() % max_blk + 1) * GI_BI_BLK_SZ;
	
		sram_off = get_random_addr(0, GI_SRAM_SIZE, sz) & GI_BI_BLK_MASK;
#ifndef T2RISC
		mem_off  = get_random_addr(0, SYS_SDRAM_SIZE, sz) & GI_BI_BLK_MASK;
#else
		mem_off  = get_random_addr(CPU_TEST_SDRAM_ADDR,CPU_TEST_SDRAM_SIZE,sz)	
			   & GI_BI_BLK_MASK;	
#endif
		sram_to_mem = rand_data() & 1;
		intr = rand_data() & 1;
		intr = i & 1;

		bi_dma_issue(sz, sram_off, mem_off, sram_to_mem, pdata, intr, 1);
		ret = bi_dma_check(sz, sram_off, mem_off, sram_to_mem, pdata, intr, 1);
	}

#ifndef T2RISC
	free(pdata);
#endif
	return ret;
}

int bi_dma_test()
{
	int ret = 0;

#ifndef T2RISC
	ipc_set_data(BD_SYS_DMA_SETUP, 1);
	ipc_set_data(BD_SYS_DMA_CMD_MON, 1);

	printf("Rand times = %d \n", rand_times);
	ret |= bi_dma_test_typical();
	printf("Rand times = %d \n", rand_times);
	ret |= bi_dma_test_overlap(0);
	printf("Rand times = %d \n", rand_times);
	ret |= bi_dma_test_overlap(0x20);
	printf("Rand times = %d \n", rand_times);
	ret |= bi_dma_test_stop(0x20);
	printf("Rand times = %d \n", rand_times);
	ret |= bi_dma_test_regular(0x20);

	ipc_set_data(BD_SYS_DMA_SETUP, 0);
	ipc_set_data(BD_SYS_DMA_CMD_MON, 0);
#else
	ret |= bi_dma_test_typical();
#ifdef BB2_FPGA  /* delete overlap and stop */
	ret |= bi_dma_test_regular(20000000);
#else
	ret |= bi_dma_test_overlap(2);
	ret |= bi_dma_test_stop(2);
#endif
#endif

	return ret;
}
