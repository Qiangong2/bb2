
/* BB2 BI(PCI) register test */

#ifndef T2RISC
	#include <stdlib.h>
	#include <stdio.h>
	#include <string.h>
	#include "iosim.h"
#endif

#include "utils.h"
#include "gi.h"
#include "gi_test.h"
#include "gi_utils.h"

int bi_reg_test(int verbose)
{
	int ctrl_mask, mem_mask, intr_mask, ret;

#ifndef T2RISC
	int rd = rand_data();
	int rd_s = rand_data();
	bd_fill_pattern(0, SYS_SDRAM_SIZE, rd, 0);
	bd_fill_pattern(GI_SRAM_START, GI_SRAM_SIZE, rd_s, 0);
#endif

	ctrl_mask = GI_BI_CTRL_SIZE_MASK | GI_BI_CTRL_WRITE_MASK | GI_BI_CTRL_PTR_MASK;
	mem_mask = GI_BI_MEM_MEM_MASK;
	intr_mask = GI_BI_INTR_AIS_INTR_MASK | GI_BI_INTR_DI_INTR_MASK | GI_BI_INTR_BI_INTR_MASK;

	ret = reg_rw_test(GI_BI_CTRL_REG, 0, ctrl_mask, verbose);
	ret |= reg_rw_test(GI_BI_MEM_REG, 0, mem_mask, verbose);
	ret |= reg_ro_test(GI_BI_INTR_REG, 0, intr_mask, verbose);

#ifndef T2RISC
	bd_check_pattern(0, SYS_SDRAM_SIZE, rd, 0);
	bd_check_pattern(GI_SRAM_START, GI_SRAM_SIZE, rd_s, 0);
#endif

#ifdef  T2RISC
	if (IO_READ(GI_SEC_MODE_REG) & GI_SEC_MODE_SECURE_MASK) 
#else
	if (ipc_get_data(BD_GI_SECURE_BIT, 0)) 	
#endif
		leave_sec_mode();
	else enter_sec_mode(0);

	ret |= reg_rw_test(GI_BI_CTRL_REG, 0, ctrl_mask, verbose);
	ret |= reg_rw_test(GI_BI_MEM_REG, 0, mem_mask, verbose);
	ret |= reg_ro_test(GI_BI_INTR_REG, 0, intr_mask, verbose);
	
#ifndef T2RISC
	bd_check_pattern(0, SYS_SDRAM_SIZE, rd, 0);
	bd_check_pattern(GI_SRAM_START, GI_SRAM_SIZE, rd_s, 0);
#endif

#ifdef  T2RISC
	if (IO_READ(GI_SEC_MODE_REG) & GI_SEC_MODE_SECURE_MASK) 
#else
	if (ipc_get_data(BD_GI_SECURE_BIT, 0)) 	
#endif
		enter_sec_mode(0);
	else leave_sec_mode();

	return ret;
}
