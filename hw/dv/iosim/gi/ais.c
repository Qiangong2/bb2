
/* BB2 AIS test */

#ifndef T2RISC
	#include <stdlib.h>
	#include <stdio.h>
	#include <string.h>
	#include "iosim.h"
#endif

#include "utils.h"
#include "gi.h"
#include "gi_test.h"
#include "gi_utils.h"

#define AIS_ISSUE(sram_off, wait_int) { \
		IO_WRITE(GI_AIS_BUF_REG, GI_AIS_BUF_BUF_OFF_MASK & sram_off); \ 
		IO_WRITE(GI_AIS_CTRL_REG, \
			 ((wait_int) ? GI_AIS_CTRL_MASK_MASK: 0) | GI_AIS_CTRL_EXEC_MASK); }

#define AIS_SETUP_MEM_PTR(wh, addr) IO_WRITE( \
		((wh) ? GI_AIS_PTR1_REG : GI_AIS_PTR0_REG) , \
		(addr) & (GI_AIS_PTR0_MEM_PTR0_MASK | GI_AIS_PTR0_EMPTY_MASK))

   /* polling ais buffer empty */ 
int polling_ais_buf_empty(int wh, int stop_word, int to)
{
	int i, cur_pos;
	unsigned int rb;

	for (i=0; i<to; i+=100) {
		IO_STALL(100);
#ifndef T2RISC
		cur_pos = ipc_get_data(BD_AIS_GET_POS, 0);
		if ((cur_pos >= stop_word) && (stop_word >= 0)) 
			return 0;
#endif
		rb = IO_READ(wh ? GI_AIS_PTR1_REG : GI_AIS_PTR0_REG);
#ifdef T2RISC
		if (stop_word) return 0;
#endif
		if (rb & GI_AIS_PTR0_EMPTY_MASK) 
			return 0;
	}

#ifndef T2RISC
	printf("Failed: Polling AIS Mem Ptr %d empty timeout\n", wh ? 0 : 1);
#endif
	return -1;
}

int polling_for_ais_host(int buf_word, int to)
{
#ifndef T2RISC
	int cur_pos;
	int i;

	for (i=0; i<to; i+=GI_AIS_BLK_TO/512) {
		cur_pos = ipc_get_data(BD_AIS_GET_POS, 0);
		IO_STALL(GI_AIS_BLK_TO/512);

		if (cur_pos >= (buf_word-1)) 
			return 0;		
	}

	printf("Failed: Polling AIS transction timeout\n");
#endif
	return -1;
}

extern int rand_times;
  /* AIS transcations */
int ais_transaction(
	int nb,                    /* # of ais blocks (4K each) */
	int delay,                 /* # cycles between hw and external module*/
	int stop_word,             /* Stop at where */
	unsigned int buf_off,      /* sram offset */
	unsigned int *mem_ptr,     /* sdram address array */
	int check_int)             /* check interrupt ? */
{
	unsigned int *pdata, param[2];
	int i, sz, stop_blk, ret, rb, st;

#ifndef T2RISC
	int rd = rand_data();
	int rd_s = rand_data();
	bd_fill_pattern(0, SYS_SDRAM_SIZE, rd, 0);
	bd_fill_pattern(GI_SRAM_START, GI_SRAM_SIZE, rd_s, 0);
#endif

	if (nb > MAX_NUM_AIS_DV_BLK) nb = MAX_NUM_AIS_DV_BLK;
	
	sz = nb * GI_AIS_BLK_SZ;
	
#ifndef T2RISC
	pdata = (unsigned int *) malloc(sz);
	if (pdata == NULL) {
		printf("Failed: cannot alloc memory to do AIS transaction test \n");
		return -1;
	}
#endif
	create_random_data(pdata, sz/sizeof(int));
	
	for (i=st=0; i<nb; i++,  st+=GI_AIS_BLK_WSZ) {
#ifndef T2RISC
		printf("Mpter[%d] = 0x%x\n", i, mem_ptr[i]); 
		bd_write_to_mem(mem_ptr[i], pdata+st, GI_AIS_BLK_WSZ);
		for (rb = 0; rb<GI_AIS_BLK_WSZ; rb++) {
			param[0] = st + rb;	
			param[1] = pdata[st+rb];
			ipc_set_multi_data(BD_AIS_PUT, param, 2);
		} /* grep ERROR from rtl log to see if data mismatched */
#else
		write_to_mem(mem_ptr[i], pdata+st, GI_AIS_BLK_WSZ);
#endif
	}

	param[0] = sz / sizeof(int);
	param[1] = 1;
	if (delay < 0) {
		IO_STALL(-delay);
		ipc_set_multi_data(BD_AIS_START, param, 2);
	}

	AIS_SETUP_MEM_PTR(0, mem_ptr[0]);
	AIS_SETUP_MEM_PTR(1, mem_ptr[1]);

#ifdef  T2RISC
	if (!(IO_READ(GI_SEC_MODE_REG) & GI_SEC_MODE_SECURE_MASK)) {
#else
	if (!ipc_get_data(BD_GI_SECURE_BIT, 0)) {
#endif
		unsigned int sroff;
		sroff = IO_READ(GI_AIS_BUF_REG) & GI_AIS_BUF_BUF_OFF_MASK;

		AIS_ISSUE(buf_off, check_int);	
		if (sroff != (IO_READ(GI_AIS_BUF_REG) & GI_AIS_BUF_BUF_OFF_MASK)) {
			printf("Failed: sram buffer cannot change in no-sec mode\n");
			return -1;
		}
	} else 	AIS_ISSUE(buf_off, check_int);

	if (delay >= 0) {
		if (delay) IO_STALL(delay);
		ipc_set_multi_data(BD_AIS_START, param, 2);
	}

	stop_blk = (stop_word >= 0) ? stop_word / GI_AIS_BLK_WSZ : -1;
	for (i=0; i<nb; i++) {
		ret = polling_ais_buf_empty(i & 1, (i!=stop_blk)?-1:stop_word, GI_AIS_BLK_TO);
		if (ret || (i == stop_blk)) break;

		rb = IO_READ(GI_BI_INTR_REG) & GI_BI_INTR_AIS_INTR_MASK;

		if (check_int && (!rb)) {
#ifndef T2RISC
			printf("Failed: Interrrupt is not assert\n");
#endif
			ret |= 0x80000000;
			break;
		} 
		if ((!check_int) && rb) {
#ifndef T2RISC
			printf("Failed: Interrrupt should not turned on\n");
#endif
			ret |= 0x880000;
			break;
		}

		if (stop_word == (i+1) * GI_AIS_BLK_WSZ) 
			break; /* Quit without clear interrupt */

		AIS_SETUP_MEM_PTR(i&1, mem_ptr[i+2]);
		
		if (check_int) {
			rb = IO_READ(GI_BI_INTR_REG) & GI_BI_INTR_AIS_INTR_MASK;
			if (rb) {
#ifndef T2RISC
				printf("Failed: Interrrupt should be cleared\n");
#endif
				ret |= 0x40000000;
				break;
			}
		}
	}

	if (stop_word < 0) { /* check done or not */
		if (!(IO_READ(GI_AIS_CTRL_REG) & GI_AIS_CTRL_EXEC_MASK)) {
			ret |= 0x20000000;
#ifndef T2RISC  
			printf("Failed: AIS should be busy when buffer is empty\n");
#endif
		}

		for (i=0; i<GI_AIS_BLK_TO; i+=100) {
			if (!(IO_READ(GI_AIS_CTRL_REG) & GI_AIS_CTRL_EXEC_MASK))
				break;
			IO_STALL(100);
		}

		if (i == GI_AIS_BLK_TO)  {
			ret |= 0x10000000;
#ifndef T2RISC  
			printf("Failed: AIS should not be busy any more\n");
#endif
		}
		/* Wait for last word shift out */
		polling_for_ais_host(1024*nb, GI_AIS_BLK_TO*64/1024*2);
		ipc_set_data(BD_AIS_CHECK, 0);
	}
	
#ifndef T2RISC 
	if ((stop_word < 0) && ipc_get_data(BD_GI_SECURE_BIT, 0)) { // XXXXX sort and check
		int j, mm;
		for (i=0;i<nb; i++) pdata[i] = mem_ptr[i];
		for (i=0; i<nb; i++) {
			st = i;
			for (j=i+1; j<nb; j++) 
				if (pdata[j] < pdata[st]) 
					st = j;

			if (st != i) {
				mm = pdata[st];
				pdata[st]= pdata[i];
				pdata[i] = mm;
			}
		}

		for (i=0; i<nb; i++) {
			if (i==0) {
				if (pdata[0] == 0) continue;
				else st = 0;
			} else st = pdata[i-1] + GI_AIS_BLK_SZ;
			if (st < pdata[i])
				bd_check_pattern(0, pdata[i], rd, st);
		}
		if ((pdata[nb-1] + GI_AIS_BLK_SZ) < SYS_SDRAM_SIZE)
			bd_check_pattern(0, SYS_SDRAM_SIZE, rd, pdata[nb-1] + GI_AIS_BLK_SZ);

		if (buf_off != 0) 
			bd_check_pattern(GI_SRAM_START, buf_off, rd_s, 0);

		if ((buf_off+256) < GI_SRAM_SIZE)
			bd_check_pattern(GI_SRAM_START, GI_SRAM_SIZE, rd_s, buf_off+256);
		
	} 
	free(pdata);
#endif
	return ret;
}

void get_random_sramaddr(unsigned int *mptr, int num)
{
	int i, j;
	unsigned int addr;

	for (i=0; i<num; i++) {
		while (1) {
#ifndef T2RISC
#ifdef ALI_GI
			addr = get_random_addr(0, SYS_SDRAM_SIZE, GI_AIS_BLK_SZ)
				& GI_AIS_MBLK_MASK;
#else
			addr = get_random_addr(0, SYS_SDRAM_GI_ONLY_SIZE, GI_AIS_BLK_SZ)
				& GI_AIS_MBLK_MASK;
#endif
#else
			addr = get_random_addr(CPU_TEST_SDRAM_ADDR,CPU_TEST_SDRAM_SIZE,
				I_AIS_BLK_SZ) & GI_AIS_MBLK_MASK;
#endif
			for (j=0; j<i; j++) 
				if (mptr[j] == addr) break;

			if ((j<i) && (i!=0)) continue;
			else {
				mptr[i] = addr;	
				break;
			}
		}
	}

	return;
}

int ais_test_typical()
{
	int ret =0, i, delay;
	unsigned int mptr[MAX_NUM_AIS_DV_BLK + 2];
	unsigned int sram_off, mem_off, offs, addr;
#ifndef T2RISC
	printf("Typical AIS transaction tests\n");
	displayMsg("Typical AIS transaction tests\n");
#endif

	for (i=0; i<MAX_NUM_AIS_DV_BLK + 2; i++)
		mptr[i] = 1;

#ifdef T2RISC
        for (offs=GI_AIS_BLK_SZ; offs<CPU_TEST_SDRAM_SIZE; offs<<=1) {
		mem_off = offs + CPU_TEST_SDRAM_ADDR;
#else
	for (mem_off=GI_AIS_BLK_SZ; mem_off<SYS_SDRAM_SIZE; mem_off<<=1) {
#endif	
		mptr[0] = mem_off;
		  /* only one block to save dv times */
		if (rand_data() & 1) delay = rand_data() % 1000;
		else delay = -rand_data() % 1000;
		sram_off = get_random_addr(0, GI_SRAM_SIZE, GI_AIS_SRAM_BUF_SZ) & GI_AIS_BLK_MASK;

		ret |= ais_transaction(1, delay, -1, sram_off, mptr, rand_data() & 1); 
	}		

#ifndef ALI_GI

#ifdef T2RISC
        for (offs=GI_AIS_BLK_SZ; offs<CPU_TEST_SDRAM_SIZE; offs<<=1) {
		addr = ((~offs) & CPU_TEST_SDRAM_MASK) + CPU_TEST_SDRAM_ADDR;
#else
	for (mem_off=GI_AIS_BLK_SZ; mem_off<SYS_SDRAM_SIZE; mem_off<<=1) {
		addr = (~mem_off) & SYS_SDRAM_MASK;
#endif	
		mptr[0] = addr & GI_AIS_MBLK_MASK;
		if (rand_data() & 1) delay = rand_data() % 1000;
		else delay = -rand_data() % 1000;
		sram_off = get_random_addr(0, GI_SRAM_SIZE, GI_AIS_SRAM_BUF_SZ) & GI_AIS_BLK_MASK;

		ret |= ais_transaction(1, delay, -1, sram_off, mptr, rand_data() & 1); 
	}		
#endif

	   /* different sizes */
#ifndef ALI_GI
	for (i=2; i<=MAX_NUM_AIS_DV_BLK; i++) {
#else
	for (i=2; i<=4; i++) {
#endif 
		printf("%d rand=0x%x \n", i, rand_times);
		get_random_sramaddr(mptr, i);
		if (rand_data() & 1) delay = rand_data() % 1000;
		else delay = -rand_data() % 1000;
		sram_off = get_random_addr(0, GI_SRAM_SIZE, GI_AIS_SRAM_BUF_SZ) & GI_AIS_BLK_MASK;
		printf("sram_off = 0x%x \n", sram_off);
		
		ret |= ais_transaction(i, delay, -1, sram_off, mptr, rand_data() & 1);
	}

	for (i=0; i<MAX_NUM_AIS_DV_BLK + 2; i++)
		mptr[i] = 1;	
	
#ifndef ALI_GI
	for (sram_off=GI_AIS_SRAM_BUF_SZ; sram_off<GI_SRAM_SIZE; sram_off<<=1) {
#ifndef T2RISC
		mptr[0] = get_random_addr(0, SYS_SDRAM_SIZE, GI_AIS_BLK_SZ)
			& GI_AIS_MBLK_MASK;
#else
		mptr[0] = get_random_addr(CPU_TEST_SDRAM_ADDR,CPU_TEST_SDRAM_SIZE, 
			GI_AIS_BLK_SZ) & GI_AIS_MBLK_MASK;
#endif
		if (rand_data() & 1) delay = rand_data() % 1000;
		else delay = -rand_data() % 1000;
		ret |= ais_transaction(1, delay, -1, sram_off, mptr, rand_data() & 1);
	}

	for (addr=GI_AIS_SRAM_BUF_SZ; addr<SZ_64K; addr<<=1) {
		sram_off = ((~addr) & (SZ_64K - 1)) & GI_AIS_BLK_MASK;
#ifndef T2RISC
		mptr[0] = get_random_addr(0, SYS_SDRAM_SIZE, GI_AIS_BLK_SZ)
			& GI_AIS_MBLK_MASK;
#else
		mptr[0] = get_random_addr(CPU_TEST_SDRAM_ADDR,CPU_TEST_SDRAM_SIZE, 
			GI_AIS_BLK_SZ) & GI_AIS_MBLK_MASK;
#endif
		if (rand_data() & 1) delay = rand_data() % 1000;
		else delay = -rand_data() % 1000;
		ret |= ais_transaction(1, delay, -1, sram_off, mptr, rand_data() & 1);
	}		
#endif		
	return ret;
}

int ais_overlap_test(int times)
{
	int ret = 0, i, delay, j, nb, stop, stop_blk, intr;
	unsigned int mptr[MAX_NUM_AIS_DV_BLK + 2], rb;
	unsigned int sram_off, sz, ctrl, k;

	for (i=0; i<times; i++) {
		nb = rand_data() % MAX_NUM_AIS_DV_BLK + 1;
		sz = nb * GI_AIS_BLK_WSZ;
		
		for (k=nb; k<MAX_NUM_AIS_DV_BLK + 2; k++)
			mptr[k] = 1;
		get_random_sramaddr(mptr, nb);

		if (rand_data() & 1) delay = rand_data() % 1000;
		else delay = -rand_data() % 1000;
		sram_off = get_random_addr(0, GI_SRAM_SIZE, GI_AIS_SRAM_BUF_SZ) & GI_AIS_BLK_MASK;	

		stop = rand_data() % (sz - 128);  /* given enough time for test */ 	 
		stop_blk = stop / GI_AIS_BLK_WSZ; 
		intr = rand_data() & 1;
		ret |= ais_transaction(nb, delay, stop, sram_off, mptr, intr);

		/* check overlapped transaction will be dropped */
		ctrl = IO_READ(GI_AIS_CTRL_REG); 
		if (!(ctrl & GI_AIS_CTRL_EXEC_MASK)) {
#ifdef T2RISC
			printf("Failed: AIS is pending, but busy bit is unset \n");
#endif			
			ret |= 0x8000000;
		}

		sram_off = get_random_addr(0, GI_SRAM_SIZE, GI_AIS_SRAM_BUF_SZ) & GI_AIS_BLK_MASK;	
		IO_WRITE(GI_AIS_CTRL_REG, \
                         (intr ? GI_AIS_CTRL_MASK_MASK: 0) | GI_AIS_CTRL_EXEC_MASK);

		stop = IO_READ(GI_AIS_CTRL_REG);
		if (ctrl != IO_READ(GI_AIS_CTRL_REG)) {
#ifdef T2RISC
			printf("Failed: AIS is pending, but busy bit is unset \n");
#endif			
			ret |= 0x4000000;
		}

		  /* checking rest transactions */
		for (j=stop_blk; j<nb; j++) {
			ret |= polling_ais_buf_empty(j&1, -1, GI_AIS_BLK_TO);
			if (ret) break;
			
			rb = IO_READ(GI_BI_INTR_REG) & GI_BI_INTR_AIS_INTR_MASK;
			if (intr && (!rb)) {
#ifndef T2RISC
				printf("Failed: Interrrupt is not assert\n");
#endif
				ret |= 0x800000;
				break;
			} 

			if ((!intr) && rb) {
#ifndef T2RISC
				printf("Failed: Interrrupt should not turned on\n");
#endif
				ret |= 0x400000;
				break;
			}

			AIS_SETUP_MEM_PTR(j&1, mptr[j+2]);
			if (intr) {
				rb = IO_READ(GI_BI_INTR_REG) & GI_BI_INTR_AIS_INTR_MASK;
				if (rb) {
#ifndef T2RISC
					printf("Failed: Interrrupt should be cleared\n");
#endif
					ret |= 0x200000;
					break;
				}
			}
		}
			
		if (!(IO_READ(GI_AIS_CTRL_REG) & GI_AIS_CTRL_EXEC_MASK)) {
			ret |= 0x100000;	
#ifndef T2RISC
			printf("Failed: AIS should be busy when buffer is empty\n");
#endif
		}

		for (i=0; i<GI_AIS_BLK_TO; i+=100) {
			if (!(IO_READ(GI_AIS_CTRL_REG) & GI_AIS_CTRL_EXEC_MASK))
				break;
			IO_STALL(100);
		}

		if (i == GI_AIS_BLK_TO)  {
			ret |= 0x10000;
#ifndef T2RISC
			printf("Failed: AIS should not be busy any more\n");
#endif
		}
		IO_STALL(GI_AIS_BLK_TO/1024);
	}
		
	return ret;
}

int ais_stop_test(int times)
{
	int ret = 0, i, delay, nb, stop, stop_blk, intr;
	unsigned int mptr[MAX_NUM_AIS_DV_BLK + 2];
	unsigned int sram_off, sz, ctrl, k;

	for (i=0; i<times; i++) {
		nb = rand_data() % MAX_NUM_AIS_DV_BLK + 1;
		sz = nb * GI_AIS_BLK_WSZ;

		for (k=nb; k<MAX_NUM_AIS_DV_BLK + 2; k++)
			mptr[k] = 1;
		get_random_sramaddr(mptr, nb);

		if (rand_data() & 1) delay = rand_data() % 1000;	
		else delay = -rand_data() % 1000;
		sram_off = get_random_addr(0, GI_SRAM_SIZE, GI_AIS_SRAM_BUF_SZ) & GI_AIS_BLK_MASK;

		stop = rand_data() % (sz - 128);  /* given enough time for test */
		stop_blk = stop / GI_AIS_BLK_WSZ;
		intr = rand_data() & 1;
		ret |= ais_transaction(nb, delay, stop, sram_off, mptr, intr);

		/* check AIS stop */		
		ctrl = IO_READ(GI_AIS_CTRL_REG);
		if (!(ctrl & GI_AIS_CTRL_EXEC_MASK)) {
#ifdef T2RISC
			printf("Failed:AIS is pending(stop), busy bit is unset \n");
#endif
			ret |= 0x8000;
		}			

		IO_WRITE(GI_AIS_CTRL_REG, 0); 	
		ctrl = IO_READ(GI_AIS_CTRL_REG);
		if (ctrl & GI_AIS_CTRL_EXEC_MASK) {
#ifdef T2RISC
			printf("Failed: Stop did not clear busy bit \n");
#endif
			ret |= 0x4000;
		}			

#ifndef T2RISC
		ipc_get_data(BD_AIS_RESET, 0);
#endif
		IO_STALL(GI_AIS_BLK_TO*32/1024);
		
		get_random_sramaddr(mptr, 1);
		mptr[1] = mptr[2] = mptr[3] = 1;
		sram_off = get_random_addr(0, GI_SRAM_SIZE, GI_AIS_SRAM_BUF_SZ) & GI_AIS_BLK_MASK;
		ret |= ais_transaction(1, 0, -1, sram_off, mptr, rand_data()&1);		 
	}	 

	return ret;
}


int ais_stop_clear_intr(int times)
{
	int ret = 0, i, delay, nb, stop, stop_blk, intr;
	unsigned int mptr[MAX_NUM_AIS_DV_BLK + 2];
	unsigned int sram_off, sz, ctrl, k, rb;

	for (i=0; i<times; i++) {
		nb = rand_data() % MAX_NUM_AIS_DV_BLK + 1;
		sz = nb * GI_AIS_BLK_WSZ;

		for (k=nb; k<MAX_NUM_AIS_DV_BLK + 2; k++)
			mptr[k] = 1;
		get_random_sramaddr(mptr, nb);

		if (rand_data() & 1) delay = rand_data() % 1000;	
		else delay = -rand_data() % 1000;
		sram_off = get_random_addr(0, GI_SRAM_SIZE, GI_AIS_SRAM_BUF_SZ) & GI_AIS_BLK_MASK;

		stop_blk = (rand_data() % nb + 1);
		stop = stop_blk * GI_AIS_BLK_WSZ;
		intr = 1;
		ret |= ais_transaction(nb, delay, stop, sram_off, mptr, intr);

		/* check AIS stop */		
		ctrl = IO_READ(GI_AIS_CTRL_REG);
		if (nb != stop_blk) {
			if (!(ctrl & GI_AIS_CTRL_EXEC_MASK)) {
#ifdef T2RISC
				printf("Failed:AIS is pending(stop), busy bit is unset \n");
#endif	
				ret |= 0x8000;
			}			
		}
		rb = IO_READ(GI_BI_INTR_REG) & GI_BI_INTR_AIS_INTR_MASK;
		if (!rb) {
#ifdef T2RISC
			printf("Interrupt should be set\n");
#endif	
			ret |= 0x8000;
		}
		
		IO_WRITE(GI_AIS_CTRL_REG, 0); 	
		ctrl = IO_READ(GI_AIS_CTRL_REG);
		if (ctrl & GI_AIS_CTRL_EXEC_MASK) {
#ifdef T2RISC
			printf("Failed: Stop did not clear busy bit \n");
#endif
			ret |= 0x4000;
		}			

#ifndef T2RISC
		ipc_get_data(BD_AIS_RESET, 0);
#endif
		IO_STALL(GI_AIS_BLK_TO*32/1024);
		rb = IO_READ(GI_BI_INTR_REG) & GI_BI_INTR_AIS_INTR_MASK;
		if (rb) {
#ifdef T2RISC
			printf("Interrupt should be cleared\n");
#endif	
			ret |= 0x800;
		}
		
		get_random_sramaddr(mptr, 1);
		mptr[1] = mptr[2] = mptr[3] = 1;
		sram_off = get_random_addr(0, GI_SRAM_SIZE, GI_AIS_SRAM_BUF_SZ) & GI_AIS_BLK_MASK;
		ret |= ais_transaction(1, 0, -1, sram_off, mptr, rand_data()&1);		 
	}	 

	return ret;
}

int ais_regular_test(int times)
{
	int ret = 0, i, delay, nb, k;
	unsigned int mptr[MAX_NUM_AIS_DV_BLK + 2];
	unsigned int sram_off;

	for (i=0; i<times; i++) {
		nb = rand_data() % MAX_NUM_AIS_DV_BLK + 1;

		for (k=nb; k<MAX_NUM_AIS_DV_BLK + 2; k++)
			mptr[k] = 1;
		get_random_sramaddr(mptr, nb);
		if (rand_data() & 1) delay = rand_data() % 1000;
		else delay = -rand_data() % 1000;
		sram_off = get_random_addr(0, GI_SRAM_SIZE, GI_AIS_SRAM_BUF_SZ) & GI_AIS_BLK_MASK;		

		ret |= ais_transaction(nb, delay, -1, sram_off, mptr, rand_data()&1);
	}

	return ret;
}

int ais_tests()
{
	int ret =0;

	IO_STALL(10000);
	ipc_set_data(BD_SYS_DMA_SETUP, 1);
	ipc_set_data(BD_SYS_DMA_CMD_MON, 1);

	ret |= ais_test_typical();

#ifndef ALI_GI
	  // XXXXX Franks need to fix hw
	  // to make AIS control reg cannot change while AIS transaction is pending. 
	printf("AIS Overlap test \n");
	ret |= ais_overlap_test(3);

	printf("AIS Stop test \n");
	ret |= ais_stop_test(3);

	printf("AIS Random test \n");
	ret |= ais_regular_test(3);

	  // Change ais timing 
	ipc_set_data(BD_AIS_SET, 1);
	printf("AIS Random test (Fast timing)\n");
	ret |= ais_regular_test(3);
	ipc_set_data(BD_AIS_SET, 0);

	  // check stop will clear int as well	
	printf("AIS stop anc intr clear test \n");
	ais_stop_clear_intr(2);

	  // Non secure mode test
	  // Frank should put in hw to let sram_off not change
	leave_sec_mode();
	printf("AIS non-secure mode test\n");
	ret |= ais_regular_test(3);
	enter_sec_mode(0);
#endif

	ipc_set_data(BD_SYS_DMA_SETUP, 0);
	ipc_set_data(BD_SYS_DMA_CMD_MON, 0);

	return ret;
}
