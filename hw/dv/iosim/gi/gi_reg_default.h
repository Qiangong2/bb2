/* * * * * *  GI register default * * * * * * * */

#ifndef _GI_REGISTER_DEFAULT
#define _GI_REGISTER_DEFAULT

   /* * *  GI BI register * * */
#define GI_BI_CTRL_REG_DFLT           0
#define GI_BI_CTRL_REG_DFLT_MASK      (GI_BI_CTRL_EXEC_MASK | GI_BI_CTRL_INTR_MASK)

#define GI_BI_MEM_REG_DFLT            0
#define GI_BI_MEM_REG_DFLT_MASK       0

#define GI_BI_INTR_REG_DFLT           0
#define GI_BI_INTR_REG_DFLT_MASK      (GI_BI_INTR_MC_INTR_MASK | GI_BI_INTR_AESE_INTR_MASK | \
                                       GI_BI_INTR_AESD_INTR_MASK | GI_BI_INTR_AIS_INTR_MASK | \
                                       GI_BI_INTR_DI_INTR_MASK | GI_BI_INTR_BI_INTR_MASK)
#define GI_BI_REG_RST_FAIL            0x1

 
  /* * *  GI DI register * * */
#define GI_DI_CONF_REG_DFLT           0xA8000070
#define GI_DI_CONF_REG_DEFL_MASK      (GI_DI_CONF_SEC_CMD0_MASK | GI_DI_CONF_SEC_CMD1_MASK | \
                                       GI_DI_CONF_TIM_END_MASK | GI_DI_CONF_TIM_START_MASK)   

#define GI_DI_CTRL_REG_DFLT            GI_DI_CTRL_RST_MASK | GI_DI_CTRL_CMD_IN_MASK
#define GI_DI_CTRL_REG_DEFL_MASK      (GI_DI_CTRL_MASK_WE_MASK | GI_DI_CTRL_BREAK_MASK_MASK | \
                                       GI_DI_CTRL_DIR_MASK_MASK | GI_DI_CTRL_DONE_MASK_MASK | \
                                       GI_DI_CTRL_RST_MASK_MASK | GI_DI_CTRL_BREAK_INTR_MASK | \
                                       GI_DI_CTRL_DIR_INTR_MASK |GI_DI_CTRL_DONE_INTR_MASK | \
                                       GI_DI_CTRL_RST_MASK | GI_DI_CTRL_BREAK_TERM_MASK | \
                                       GI_DI_CTRL_COVER_WE_MASK | GI_DI_CTRL_ERROR_WE_MASK | \
                                       GI_DI_CTRL_BREAK_STATE_MASK | GI_DI_CTRL_CMD_IN_MASK | \
                                       GI_DI_CTRL_DIR_STATE_MASK)

#define GI_DI_BE_REG_DFLT             0
#define GI_DI_BE_REG_DFLT_MASK        GI_DI_BE_EXEC_MASK 

#define GI_DI_REG_RST_FAIL            0x10

   /* * *  GI AES register * * */
#define GI_AESD_CTRL_REG_DFLT         0
#define GI_AESD_CTRL_REG_DFLT_MASK    (GI_AESD_CTRL_EXEC_MASK | GI_AESD_CTRL_MASK_MASK)

#define GI_AESD_IV_REG_DFLT           0
#define GI_AESD_IV_REG_DFLT_MASK      0

#define GI_AESE_CTRL_REG_DFLT         0
#define GI_AESE_CTRL_REG_DFLT_MASK    (GI_AESE_CTRL_EXEC_MASK | GI_AESE_CTRL_MASK_MASK)

#define GI_AESE_IV_REG_DFLT           0
#define GI_AESE_IV_REG_DFLT_MASK      0

#define GI_AES_KEXP_REG_DFLT          GI_AES_KEXP_READY_MASK
#define GI_AES_KEXP_REG_DFLT_MASK     (GI_AES_KEXP_STALL_MASK | GI_AES_KEXP_READY_MASK | GI_AES_KEXP_EXEC_MASK)

#define GI_AES_REG_RST_FAIL           0x100

  /* * * GI MC register * * */
#define GI_MC_CTRL_REG_DFLT          0 
#define GI_MC_CTRL_REG_DFLT_MASK     (GI_MC_CTRL_EXEC_MASK | GI_MC_CTRL_MASK_MASK)

#define GI_MC_ADDR_REG_DFLT          0
#define GI_MC_ADDR_REG_DFLT_MASK     0

#define GI_MC_BUF_REG_DFLT           0
#define GI_MC_BUF_REG_DFLT_MASK      0

#define GI_MC_REG_RST_FAIL            0x1000

  /* * * GI SHA1 register * * */
#define GI_SHA_CTRL_REG_DFLT         0
#define GI_SHA_CTRL_REG_DFLT_MASK    (GI_SHA_CTRL_EXEC_MASK | GI_SHA_CTRL_INTR_MASK)
#define GI_SHA_BUF_REG_DFLT          0
#define GI_SHA_BUF_REG_DFLT_MASK     0


#define GI_SHA_REG_RST_FAIL          0x10000

  /* * * DC register * * */

#define GI_DC_CTRL_REG_DFLT          0    
#define GI_DC_CTRL_REG_DFLT_MASK     (GI_DC_CTRL_EXEC_MASK | GI_DC_CTRL_MASK_MASK)  
#define GI_DC_BUF_REG_DFLT           0
#define GI_DC_BUF_REG_DFLT_MASK      0
#define GI_DC_HADDR_REG_DFLT         0 
#define GI_DC_HADDR_REG_DFLT_MASK    0
#define GI_DC_HIDX_REG_DFLT          0
#define GI_DC_HIDX_REG_DFLT_MASK     0
#define GI_DC_DIOFF_REG_DFLT         0
#define GI_DC_DIOFF_REG_DFLT_MASK    0
#define GI_DC_IV_REG_DFLT         0
#define GI_DC_IV_REG_DFLT_MASK    0
#define GI_DC_H0_REG_DFLT         0
#define GI_DC_H0_REG_DFLT_MASK    0
#define GI_DC_H1_REG_DFLT         0
#define GI_DC_H1_REG_DFLT_MASK    0
#define GI_DC_REG_RST_FAIL

#define GI_GI_REG_RST_FAIL            0x100000

  /* * *  GI AIS register * * */
#define GI_AIS_CTRL_REG_DFLT          0
#define GI_AIS_CTRL_REG_DFLT_MASK     (GI_AIS_CTRL_EXEC_MASK | GI_AIS_CTRL_MASK_MASK)

#define GI_AIS_PTR0_REG_DFLT           0
#define GI_AIS_PTR0_REG_DFLT_MASK      0

#define GI_AIS_PTR1_REG_DFLT           0
#define GI_AIS_PTR1_REG_DFLT_MASK      0

#define GI_AIS_REG_RST_FAIL            0x1000000

  /* * * GI Security register * * */ 
#define GI_SEC_MODE_REG_SEC_DFLT       (GI_SEC_MODE_RESET_MASK | GI_SEC_MODE_SECURE_MASK)
#define GI_SEC_MODE_REG_SEC_DFLT_MASK  (GI_SEC_MODE_SGI_MASK | GI_SEC_MODE_STIMER_MASK | \
                                        GI_SEC_MODE_SAPP_MASK | GI_SEC_MODE_RESET_MASK | \
                                        GI_SEC_MODE_SECURE_MASK)

#define GI_SEC_MODE_REG_NS_DFLT        0
#define GI_SEC_MODE_REG_NS_DFLT_MASK   0xFFFFFFFF 

#define GI_SEC_TIMER_REG_SEC_DFLT      0
#define GI_SEC_TIMER_REG_SEC_DFLT_MASK (GI_SEC_TIMER_TRESET_MASK | GI_SEC_TIMER_VAL_MASK)

#define GI_SEC_TIMER_REG_NS_DFLT       0
#define GI_SEC_TIMER_REG_NS_DFLT_MASK  0xFFFFFFFF

#define GI_SEC_SRAM_REG_SEC_DFLT       0
#define GI_SEC_SRAM_REG_SEC_DFLT_MASK  0

#define GI_SEC_SRAM_REG_NS_DFLT        0
#define GI_SEC_SRAM_REG_NS_DFLT_MASK   0xFFFFFFFF

#define GI_SEC_ENTER_REG_DFLT          0
#define GI_SEC_ENTER_REG_DFLT_MASK     0xFFFFFFFF

#define GI_SEC_REG_SEC_RST_FAIL        0x10000000
#define GI_SEC_REG_NS_RST_FAIL         0x20000000

#endif
