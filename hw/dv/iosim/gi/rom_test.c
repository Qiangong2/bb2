
/* BB2 GI sram test */

#ifndef T2RISC
	#include <stdlib.h>
	#include <stdio.h>
	#include <string.h>
	#include "iosim.h"
#else
	#include "t2os.h"
#endif

#include "utils.h"
#include "gi.h"
#include "gi_utils.h"
#include "gi_test.h"

int check_readonly(unsigned int addr, unsigned int data)
{
	int ret=0, rb, d[16];

	IO_WRITE(addr, rand_data());
	rb = IO_READ(addr);

	if (rb != data) {
#ifndef T2RISC
		printf("Failed: Readonly check @0x%x\n", addr);
#endif
		return -1;
	}

#ifndef T2RISC
	for (rb=4; rb<16; rb++) d[rb] = 0;
	for (rb=0; rb<4; rb++) d[rb] = rand_data();
	sim_bwrite(addr & (GI_ROM_BMASK | GI_SRAM_START), 4, d);

	rb = IO_READ(addr);
	if (rb != data) {
		printf("Failed: Readonly check(blk) @0x%x\n", addr);
		return -2;
	}
#endif
	return ret;
}

int check_rom_data(unsigned int saddr, unsigned int *pdata)
{
	int ret = 0, i, j;
	unsigned int d[16], rb;

	for (i=0; i<GI_ROM_SIZE/sizeof(int); i++) {
		rb = IO_READ(saddr + (i<<2));
		if (rb != pdata[i]) {
#ifndef T2RISC
			printf("Failed: Rom mismatch @0x%x exp=0x%x rb=0x%x\n", 
				saddr + (i<<2), pdata[i], rb);
#endif
			return i+1;
		} 
	}

	for (i=0; i<GI_ROM_SIZE/(sizeof(int)*4); i++) {
#ifndef T2RISC
		sim_bread(saddr + (i << 4), 4, d);
#else
		d[0] = IO_CACHE_READ(saddr + (i<<4));
		d[1] = IO_CACHE_READ(saddr + (i<<4) + 4);
		d[2] = IO_CACHE_READ(saddr + (i<<4) + 8);
		d[3] = IO_CACHE_READ(saddr + (i<<4) + 12);
#endif
		for (j=0; j<4; j++) {
			if (d[j] != pdata[i*4+j]) {
#ifndef T2RISC
				printf("Failed: Rom mismatch @0x%x exp=0x%x rb=0x%x\n", 
					saddr + (i<<4) + (j<<2), pdata[i*4+j], d[j]);
#endif
				return GI_ROM_SIZE + i*4 + j;
			}
		}
	}

	    /* Read only check */
	i = (rand_data() % GI_ROM_SIZE) & (~3);
	ret |= check_readonly(saddr + i, pdata[i>>2]);
	i = (rand_data() % GI_ROM_SIZE) & (~3);
	ret |= check_readonly(saddr + i, pdata[i>>2]);

	return ret;
}

int check_rom_data_inv(unsigned saddr, unsigned int *pdata)
{
	int ret = 0, i, j, n;
	unsigned int d[16], rb;

	n = GI_ROM_SIZE/sizeof(int) - 1;
	for (i=0; i<=n; i++) {
		rb = IO_READ(saddr + (i<<2));
		if (rb != pdata[n-i]) {
#ifndef T2RISC
			printf("Failed: Rom mismatch @0x%x exp=0x%x rb=0x%x\n", 
				saddr + (i<<2), pdata[n-i], rb);
#endif
			return i+1;
		} 
	}

	for (i=0; i<GI_ROM_SIZE/(sizeof(int)*4); i++) {
#ifndef T2RISC
		sim_bread(saddr + (i << 4), 4, d);
#else
		d[0] = IO_CACHE_READ(saddr + (i<<4));
		d[1] = IO_CACHE_READ(saddr + (i<<4) + 4);
		d[2] = IO_CACHE_READ(saddr + (i<<4) + 8);
		d[3] = IO_CACHE_READ(saddr + (i<<4) + 12);
#endif
		for (j=0; j<4; j++) {
			if (d[j] != pdata[n - i*4 - j]) {
#ifndef T2RISC
				printf("Failed: Rom mismatch @0x%x exp=0x%x rb=0x%x\n", 
					saddr + (i<<4) + (j<<2), pdata[n-i*4-j], d[j]);
#endif
				return GI_ROM_SIZE + i*4 + j;
			}
		}
	}

	    /* Read only check */
	i = (rand_data() % GI_ROM_SIZE) & (~3);
	ret |= check_readonly(saddr + i, pdata[n-(i>>2)]);
	i = (rand_data() % GI_ROM_SIZE) & (~3);
	ret |= check_readonly(saddr + i, pdata[n-(i>>2)]);

	return ret;
}

int test_rom_data(unsigned int saddr, int inverse)
{
	int ret = 0, i;

#ifndef T2RISC
	unsigned int *pdata;
	displayMsg("GI ROM test");
	pdata = (unsigned int *) malloc(GI_ROM_SIZE);

	if (pdata == NULL) {
		printf("Cannot alloc memory to do the test\n");
		return -1;
	}

	for (i=0; i<GI_ROM_SIZE/sizeof(int); i++) {
		pdata[i] = rand_data();
		BD_IO_WRITE(saddr + (i<<2), pdata[i]);
	}

	if (inverse) check_rom_data_inv(saddr, pdata);
	else check_rom_data(saddr, pdata);

	free(pdata);
#endif

	return ret;
}

int do_gi_rom_test(unsigned int saddr)
{
	int ret=0, testen;
	int reverse, v;

	testen = IO_READ(GI_SEC_MODE_REG);
	reverse = GI_SEC_MODE_TEST_ENA_MASK | GI_SEC_MODE_ALT_BOOT_MASK;

	if ((testen & reverse) == reverse) {
		ret |= test_rom_data(saddr, 1);
		
		/* sec_mode: testen=0 alt_boot=1(unchange) */
		v = testen & (~GI_SEC_MODE_ALT_BOOT_MASK);
		IO_WRITE(GI_SEC_MODE_REG, v); 
		ret |= read_compare(GI_SEC_MODE_REG, GI_SEC_MODE_ALT_BOOT_MASK, reverse);
		ret |= test_rom_data(saddr, 0);

		/* sec_mode: test_en=1 alt_boot=0 both flip*/
		IO_WRITE(GI_SEC_MODE_REG, testen);
		ret |= read_compare(GI_SEC_MODE_REG, GI_SEC_MODE_TEST_ENA_MASK, reverse);
		ret |= test_rom_data(saddr, 0);
		
		/* sec_mode: test_en=0 alt_boot=0 */
		v = testen & (~GI_SEC_MODE_ALT_BOOT_MASK);
		IO_WRITE(GI_SEC_MODE_REG, v);
		ret |= read_compare(GI_SEC_MODE_REG, 0, reverse);
		ret |= test_rom_data(saddr, 0);

		/* back to orignal*/ 
		IO_WRITE(GI_SEC_MODE_REG, testen);
		ret |= read_compare(GI_SEC_MODE_REG, testen, reverse);
	} else {
		ret |= test_rom_data(saddr, 0);

		/* flip to set rom reversed */
		v = (testen & GI_SEC_MODE_TEST_ENA_MASK) ? 0 : GI_SEC_MODE_TEST_ENA_MASK;
		v |= (testen & GI_SEC_MODE_ALT_BOOT_MASK) ? 0 : GI_SEC_MODE_ALT_BOOT_MASK;
		IO_WRITE(GI_SEC_MODE_REG, (testen & (~reverse))  | v);
		ret |= test_rom_data(saddr, 1);
		ret |= read_compare(GI_SEC_MODE_REG, reverse, reverse);

		/* back to original */
		IO_WRITE(GI_SEC_MODE_REG, (testen & (~reverse))  | v);
		ret |= read_compare(GI_SEC_MODE_REG, testen, reverse);	
	}

	return ret;
}

int gi_rom_tests()
{
	int ret = 0;
	int rdata = 0, rdata_mem=0;

#ifndef T2RISC
	rdata = rand_data();
	rdata_mem = rand_data();
	bd_fill_pattern(GI_SRAM_START, GI_SRAM_SIZE, rdata, 0);
	bd_fill_pattern(0, SYS_SDRAM_SIZE, rdata_mem, 0);
#endif
	ret |= do_gi_rom_test(GI_ROM_START);

#ifndef T2RISC
	bd_check_pattern(GI_SRAM_START, GI_SRAM_SIZE, rdata, 0);
	bd_check_pattern(0, SYS_SDRAM_SIZE, rdata_mem, 0);
#endif
	
	IO_WRITE(GI_SEC_MODE_REG, GI_SEC_MODE_SECURE_MASK);

	ret |= do_gi_rom_test(GI_SRAM_START);
	IO_WRITE(GI_SEC_MODE_REG, GI_SEC_MODE_RESET_MASK | GI_SEC_MODE_SECURE_MASK);

#ifndef T2RISC
	IO_READ(GI_SEC_MODE_REG);
	bd_check_pattern(GI_SRAM_START, GI_SRAM_SIZE, rdata, 0);
	bd_check_pattern(0, SYS_SDRAM_SIZE, rdata_mem, 0);
#endif

	return ret;
}
