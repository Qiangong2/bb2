
/* BB2 DI transaction tests */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "iosim.h"
#include "utils.h"
#include "gi.h"
#include "gi_test.h"
#include "gi_utils.h"

void di_reset(              /* reset DI host(flipper) */
	int type)     
{
	ipc_set_data(BD_DI_RESET, type);

	return;
}

void bd_put_data_to_di(      /* Bd put data into DI host/flipper buf */
	unsigned int offset,     /* offset */ 
	unsigned int *pdata,   
	int num)                 /* number of data */
{
	int i;
	unsigned int v[2];

	v[0] = offset;
	for (i=0; i<num; i++) {
		v[1] = pdata[i];
		ipc_set_multi_data(BD_DI_PUT, v, 2);
		v[0] += 4;
	}

	return;
}

void bd_put_zero_to_di(      /* Bd put data into DI host/flipper buf */
	unsigned int offset,     /* offset */ 
	int num)                 /* number of data */
{
	int i;
	unsigned int v[2];

	v[0] = offset;
	for (i=0; i<num; i++) {
		v[1] = 0;
		ipc_set_multi_data(BD_DI_PUT, v, 2);
		v[0] += 4;
	}

	return;
}

void bd_di_xfer(               /* Ask DI/FLIPPER to do bd xfer */ 
	int rcv,	       /* Host/Flipper recv ? */
	int send_size,
	int recv_size,
	int wr_cmd,	       /* Write command ? */
	int random_strober_stall)
{
	int v[5];
	
	v[0] = rcv;
	v[1] = send_size;
	v[2] = recv_size;
	v[3] = wr_cmd;
	v[4] = random_strober_stall;

	ipc_set_multi_data(BD_DI_XFER, v, 5);

	return;
}

void di_burst(              /* start di burst */
	int  size,          /* busrt length */
	int  gi_to_di,      /* data from gi to di ? */ 
	int  offset)        /* sram offset */
{
	unsigned int v;

	v = (offset & GI_DI_BE_PTR_MASK) | 
	    (1 << GI_DI_BE_EXEC_SHIFT) |    
	    ((gi_to_di  << GI_DI_BE_OUT_SHIFT) & GI_DI_BE_OUT_MASK) |
	    (((size-1) << GI_DI_BE_SIZE_SHIFT) & GI_DI_BE_SIZE_MASK);

	IO_WRITE(GI_DI_BE_REG, v); 

	return;
}

int bd_check_di_data(
	unsigned int offset,   /* DI Host/Flipper buffer offset */
	unsigned int *pdata,
	int num)
{
	int i;
	unsigned int rdata, addr;

	for (i=0, addr=offset; i<num; i++, addr+=4) {
		rdata = ipc_get_data(BD_DI_GET, addr);
		if (rdata != pdata[i]) {
			printf("Failed: miscompare offset=0x%x exp=0x%x read=0x%x\n",
				addr, pdata[i], rdata);
			return -1;
		}
	}
	
	return 0;
}

/* Create simple command other than 0xa8 and 0x00
   Should backdoor read from DI_CONF reg later
 */
unsigned char random_simple_cmd()
{
	unsigned char ret, cmd0=0xa8, cmd1=0x00;

	for (; ;) {
		ret = rand_data() & 0xff;
		if ((ret != cmd0) && (ret != cmd1)) 
			break;
	}

	return ret;
}

int di_polling_interrupt(             /* return -1, if timeout */
	int int_mask,                 /* interrupt mask in di ctrl */
	int later)                    /* will be assert later */
{
	int timeout;
	unsigned int ret;

	IO_HANDSHAKE();
	if ((BD_IO_INTERRUPT & GI_SYS_INTERRUPT_MASK) & later) {
		displayMsg("Warning: Interrupt is already asserted");
		printf("Warning: Interrupt is already asserted !\n");
	}
	
	for (timeout=0; timeout<GI_INT_POLLING_TIMEOUT; timeout += 10) {
		IO_STALL(10);
		if (BD_IO_INTERRUPT & GI_SYS_INTERRUPT_MASK) {
			ret = IO_READ(GI_DI_CTRL_REG);
			if ((ret & int_mask) == int_mask) {
				displayMsg("Interrupt polling succeed");
				return 0;
			}
		}
	}
	printf("Failed: Polling DI interrupt timeout exp=0x%x di_ctrl_last=0x%x\n",
	       int_mask, ret);
		
	return -1;
}

int di_clear_interrupt(
	int int_bits)
{
	int timeout;

	IO_HANDSHAKE();
	if (!(BD_IO_INTERRUPT & GI_SYS_INTERRUPT_MASK))
		printf("Warning: No GI system interrupt \n");

	IO_WRITE(GI_DI_CTRL_REG, int_bits | GI_DI_CTRL_ERROR_WE_MASK);
	for (timeout=0; timeout<GI_INT_POLLING_TIMEOUT; timeout += 10) {
		IO_STALL(10);
		if (!(BD_IO_INTERRUPT & GI_SYS_INTERRUPT_MASK)) {
			return 0;
		}
	}

	printf("Failed: Clear interrupt bits timeout current di ctrl=0x%x clr=0x%x\n", 
	       IO_READ(GI_DI_CTRL_REG), int_bits);

	return -1;
}

int di_err(unsigned int *p_exp, int rand_stall)
{
	if (rand_stall) { 
		if (rand_data() & 1) IO_STALL(rand_data() & 0xfff);
		else IO_STALL(rand_data() & 0xf);
	}

	if (rand_data() & 1) {
		IO_WRITE(GI_DI_CTRL_REG, GI_DI_CTRL_ERROR_WE_MASK | GI_DI_CTRL_ERROR_MASK);
		IO_WRITE(GI_DI_BE_REG, 0);
	} else {
		IO_WRITE(GI_DI_BE_REG, 0);
		IO_WRITE(GI_DI_CTRL_REG, GI_DI_CTRL_ERROR_WE_MASK | GI_DI_CTRL_ERROR_MASK);
	}
	
	free(p_exp);
	return -1;
}

int di_brk(                              /* Di break test */
	unsigned int *p_exp,             /* expect Data  */
	unsigned int sram_addr,          /* sram addr */
	int snd_size,                    /* Host/Flipper snd size */
	int rcv_size)                    /* Host/Flipper recv size */
{
	int len, i;
	char msg[64];
	unsigned int mask = 0, ctrl;

	if (snd_size) {    /* deal with send command */
	}	

	if (rcv_size) {
		len = rand_data() % rcv_size;
		sprintf(msg, "Filpper/Host break at %d", len);
		displayMsg(msg);
		printf("%s\n", msg);
		ipc_set_data(BD_DI_BREAK_AT, len);
		mask = GI_DI_CTRL_BREAK_INTR_MASK ;

		/* expected break interrupt */
		di_polling_interrupt(mask, 1);
		bd_check_di_data(0, p_exp, len / sizeof(int));

		/* clear break interrupt and stop BE*/
		IO_WRITE(GI_DI_CTRL_REG, GI_DI_CTRL_BREAK_INTR_MASK);
		IO_WRITE(GI_DI_BE_REG, 0);

		/* Dir interrupt could happen before BRK termination */
		ctrl = IO_READ(GI_DI_CTRL_REG); 
		/* Change from OUT(1)  --> IN(0) */
		if ((ctrl & GI_DI_CTRL_DIR_STATE_MASK) == 0) 
			if (!(ctrl & GI_DI_CTRL_DIR_INTR_MASK)) 
				printf("Failed: Dir interrupt should on 0x%x\n", ctrl);

		/* Terminate Break */
		IO_WRITE(GI_DI_CTRL_REG, GI_DI_CTRL_BREAK_TERM_MASK);
		
		/* waiting for break state is clear*/
		for (i=0; i<3; i++) {
			ctrl = IO_READ(GI_DI_CTRL_REG);
			if (!(ctrl & GI_DI_CTRL_BREAK_STATE_MASK))
				break;
		}
		if (i>=3) printf("Failed: Break termination timeout 0x%x\n", ctrl);

		di_polling_interrupt(GI_DI_CTRL_DIR_INTR_MASK, 0);
		di_clear_interrupt(GI_DI_CTRL_DIR_INTR_MASK | GI_DI_CTRL_DONE_INTR_MASK);
	}

	free(p_exp);
	return -1;
}

/*  
    Pursuit a DI transaction
    For simple command, 12 byte IN, 4 bytes out
    Read command: 12 bytes in, 32*N bytes out
    Write command: 12+32+12 bytes IN, 4 bytes out
 */
int di_transaction(
	unsigned char cmd_byte,           /* cmd type byte */
	unsigned int  snd_size,           /* request size   */
	unsigned int  rcv_size,           /* response size */
	int snd_stall,                    /* snd stall # clk */
	int rcv_stall,                    /* rcv stall # clk */
	int err_brk_exit)                 /* err and break exit */
{
	unsigned int *pdata, size;
	char msg[64];
	unsigned int di_host_offset=0, gi_sram_offset; 

	  /* allocate memory for test */
	size = (snd_size > rcv_size) ? snd_size : rcv_size;
	size = (size > DI_SIMPLE_CMD_SIZE) ? size : DI_SIMPLE_CMD_SIZE;
	pdata = (unsigned int *) malloc(size);
	if (pdata == NULL) {
		printf("Failed: cannot alloc memory to do DI tests\n");
		return -1;
	}

	sprintf(msg, "DI_xfer: cmd=0x%x tx=%d rcv=%x, stall (%d %d) err=%d", 
		cmd_byte, snd_size, rcv_size, snd_stall, rcv_stall, err_brk_exit);
	printf("%s \n", msg);
	displayMsg(msg);

	   /* backdoor setup DI_HOST and start transfer*/
	create_random_data(pdata, snd_size/sizeof(int));
	pdata[0] = (pdata[0] & (~GI_DI_CMD_BYTE_MASK)) | (cmd_byte << GI_DI_CMD_BYTE_SHIFT);
	bd_put_data_to_di(di_host_offset, pdata, snd_size/sizeof(int));   
	gi_sram_offset = get_random_addr(GI_SRAM_START, GI_SRAM_SIZE, DI_SIMPLE_CMD_SIZE);
	
	if (err_brk_exit == ERR_BEFORE_CMD)  return di_err(pdata, 0);
	if (snd_stall >= 0) {
		bd_di_xfer(DIR_FLIPPER_TO_GI, snd_size, rcv_size, 
			   (snd_size>DI_SIMPLE_CMD_SIZE), rand_data() & 1);
		if (snd_stall) IO_STALL(snd_stall);
		di_burst(DI_SIMPLE_CMD_SIZE,  DIR_FLIPPER_TO_GI, gi_sram_offset);
		if (err_brk_exit == ERR_IN_CMD)  return di_err(pdata, 1);
	} else {
		di_burst(DI_SIMPLE_CMD_SIZE,  DIR_FLIPPER_TO_GI, gi_sram_offset);
		if (err_brk_exit == ERR_IN_CMD)  return di_err(pdata, 1);
		IO_STALL(-snd_stall);
		bd_di_xfer(DIR_FLIPPER_TO_GI, snd_size, rcv_size, 
			   (snd_size>DI_SIMPLE_CMD_SIZE), rand_data() & 1);
	}

	   /* polling interrupt line */
	if (snd_size > DI_SIMPLE_CMD_SIZE) di_polling_interrupt(GI_DI_CTRL_DONE_INTR_MASK, 1);
	else di_polling_interrupt(GI_DI_CTRL_DIR_INTR_MASK | GI_DI_CTRL_DONE_INTR_MASK, 1);

	   /* backdoor checkup cmd data */
	bd_check_mem_data(gi_sram_offset, pdata, DI_SIMPLE_CMD_SIZE/sizeof(int));

	   /* clear interrupts */
	di_clear_interrupt(GI_DI_CTRL_DIR_INTR_MASK | GI_DI_CTRL_DONE_INTR_MASK);

	if (snd_size > DI_SIMPLE_CMD_SIZE) { /* recv more data */
		gi_sram_offset = get_random_addr(GI_SRAM_START, GI_SRAM_SIZE, snd_size-DI_SIMPLE_CMD_SIZE);
		di_burst(snd_size - DI_SIMPLE_CMD_SIZE,  DIR_FLIPPER_TO_GI, gi_sram_offset);
		if (err_brk_exit == ERR_SEND_DATA)  return di_err(pdata, 1);
		
		di_polling_interrupt(GI_DI_CTRL_DIR_INTR_MASK | GI_DI_CTRL_DONE_INTR_MASK, 1);
		bd_check_mem_data(gi_sram_offset, pdata+DI_SIMPLE_CMD_SIZE/sizeof(int),
		                   (snd_size-DI_SIMPLE_CMD_SIZE)/sizeof(int));
		di_clear_interrupt(GI_DI_CTRL_DIR_INTR_MASK | GI_DI_CTRL_DONE_INTR_MASK);
	}
	
	   /* send out response */
	gi_sram_offset = get_random_addr(GI_SRAM_START, GI_SRAM_SIZE, rcv_size);	
	create_random_data(pdata, rcv_size/sizeof(int));
	bd_write_to_mem(gi_sram_offset, pdata, rcv_size/sizeof(int));

	if (rcv_stall >= 0) { 
		bd_di_xfer(DIR_GI_TO_FLIPPER, 0, rcv_size,
			   (snd_size>DI_SIMPLE_CMD_SIZE), rand_data() & 1);
		if (rcv_stall) IO_STALL(rcv_stall>0 ? rcv_stall : -rcv_stall);
		di_burst(rcv_size, DIR_GI_TO_FLIPPER, gi_sram_offset);
		if (err_brk_exit == ERR_RESPOND_OUT)  return di_err(pdata, 1);
		if (err_brk_exit == BRK_DI_SEND) return di_brk(pdata, 0, 0, rcv_size); 
	} else printf("Failed: case not supported by flipper\n");

	  /* check data + interrupt polling/clear */
	di_polling_interrupt(GI_DI_CTRL_DONE_INTR_MASK, 1);	
	bd_check_di_data(di_host_offset, pdata, rcv_size/sizeof(int));	
	di_clear_interrupt(GI_DI_CTRL_DIR_INTR_MASK | GI_DI_CTRL_DONE_INTR_MASK);

	free(pdata);
	return 0;
}

int di_simple_xfer(int rand_timing)
{
	int rx_sz, snd_sz, st, end, tim, mask;
	unsigned int conf;

	if (rand_timing) {
		conf = IO_READ(GI_DI_CONF_REG);
		end = rand_data() % 7 + 1;
		st = rand_data() % 6;
		tim = (end << GI_DI_CONF_TIM_END_SHIFT) | (st << GI_DI_CONF_TIM_START_SHIFT);
		mask = GI_DI_CONF_TIM_START_MASK | GI_DI_CONF_TIM_END_MASK; 
		conf = (conf & (~mask)) | tim;
		printf("Change DI conf to 0x%x\n", conf);
		IO_WRITE(GI_DI_CONF_REG, conf);
	}

	   /* simple command test */
	snd_sz = DI_SIMPLE_CMD_SIZE;
	rx_sz = DI_SIMPLE_RESPOND_SIZE;

	displayMsg(" Simple di cmd");
	di_transaction(0xab, snd_sz, rx_sz, 0, 0, 0);		
	di_transaction(0xe0, snd_sz, rx_sz, 0, 0, 0);		
	di_transaction(0xe1, snd_sz, rx_sz, 0, 0, 0);		
	di_transaction(0xe2, snd_sz, rx_sz, 0, 0, 0);		
	di_transaction(0xe3, snd_sz, rx_sz, 0, 0, 0);		
	di_transaction(0xe4, snd_sz, rx_sz, 0, 0, 0);		
	di_transaction(random_simple_cmd(), snd_sz, rx_sz, 0, 0, 0);

	  /* simple command with stall */
	displayMsg("simple di cmd with random stall ");
	di_transaction(random_simple_cmd(), snd_sz, rx_sz, 
		rand_data() & 0x1ff, 0, 0);
	di_transaction(random_simple_cmd(), snd_sz, rx_sz, 
		-(rand_data() & 0x1ff), 0, 0);
	di_transaction(random_simple_cmd(), snd_sz, rx_sz, 
		0, rand_data() & 0x1ff, 0);
	di_transaction(random_simple_cmd(), snd_sz, rx_sz, 
		rand_data() & 0x1ff, rand_data() & 0x1ff, 0);
	di_transaction(random_simple_cmd(), snd_sz, rx_sz, 
		-(rand_data() & 0x1ff), rand_data() & 0x1ff, 0);

	  /* read data command (12 byte IN, 32*N bytes out) */
	displayMsg("DI read DMA ");
	rx_sz = ((rand_data() % 127) + 1) *  32; 
	di_transaction(0x12, DI_SIMPLE_CMD_SIZE, rx_sz, 0, 0, 0);
	rx_sz = ((rand_data() % 127) + 1) * 32; 
	di_transaction(0x12, DI_SIMPLE_CMD_SIZE, 4096, 0, 0, 0);
	rx_sz = ((rand_data() % 127) + 1)* 32; 

	displayMsg("DI read DMA with random stall");
	rx_sz = ((rand_data() % 127) + 1) * 32; 
	di_transaction(random_simple_cmd(), DI_SIMPLE_CMD_SIZE, rx_sz, rand_data() & 0x1ff, 0, 0);
	rx_sz = ((rand_data() % 127) + 1) * 32; 
	di_transaction(random_simple_cmd(), DI_SIMPLE_CMD_SIZE, rx_sz, -rand_data() & 0x1ff, 0, 0);
	rx_sz = ((rand_data() % 127) + 1)* 32; 
	di_transaction(random_simple_cmd(), DI_SIMPLE_CMD_SIZE, rx_sz, 0, rand_data() & 0x1ff, 0);
	rx_sz = ((rand_data() % 127) + 1) * 32; 
	di_transaction(random_simple_cmd(), DI_SIMPLE_CMD_SIZE, rx_sz, 
		rand_data() & 0x1ff, rand_data() & 0x1ff, 0);
	rx_sz = ((rand_data() % 127) + 1) * 32; 
	di_transaction(random_simple_cmd(), DI_SIMPLE_CMD_SIZE, rx_sz, 
		-rand_data() & 0x1ff, rand_data() & 0x1ff, 0);

	  /* write data command (12 + 32*N bytes IN, 4 bytes out) */
	displayMsg("DI write DMA");
	snd_sz = 12 + 4064 + 12; 
	di_transaction(random_simple_cmd(), snd_sz, DI_SIMPLE_RESPOND_SIZE, 0, 0, 0);
	snd_sz = 12 + ((rand_data() % 127) + 1) *  32 + 12; 
	di_transaction(random_simple_cmd(), snd_sz, DI_SIMPLE_RESPOND_SIZE, 0, 0, 0);
	snd_sz = 12 + ((rand_data() % 127) + 1) *  32 + 12; 
	di_transaction(random_simple_cmd(), snd_sz, DI_SIMPLE_RESPOND_SIZE, 0, 0, 0);
	displayMsg("DI write DMA with random stall");
	snd_sz = 12 + ((rand_data() % 127) + 1) *  32 + 12; 
	di_transaction(random_simple_cmd(), snd_sz, DI_SIMPLE_RESPOND_SIZE, 
		rand_data() & 0x1ff, 0, 0);
	snd_sz = 12 + ((rand_data() % 127) +1 )*  32 + 12; 
	di_transaction(random_simple_cmd(), snd_sz, DI_SIMPLE_RESPOND_SIZE, 
		-rand_data() & 0x1ff, 0, 0);
	snd_sz = 12 + ((rand_data() % 127) + 1) *  32 + 12; 
	di_transaction(random_simple_cmd(), snd_sz, DI_SIMPLE_RESPOND_SIZE, 
		0, rand_data() & 0x1ff, 0);
	snd_sz = 12 + ((rand_data() % 127) + 1) *  32 + 12; 
	di_transaction(random_simple_cmd(), snd_sz, DI_SIMPLE_RESPOND_SIZE, 
		rand_data() & 0x1ff, rand_data() & 0x1ff, 0);
	snd_sz = 12 + ((rand_data() % 127) + 1) *  32 + 12; 
	di_transaction(random_simple_cmd(), snd_sz, DI_SIMPLE_RESPOND_SIZE, 
		-rand_data() & 0x1ff, rand_data() & 0x1ff, 0);

	return 0;
}

int di_rand_simple_xfer(int rand_timing, int num)
{
	int rx_sz, snd_sz, st, end, tim, mask;
	unsigned int conf;

	displayMsg("Random order DI transactions");
	if (rand_timing) {
		conf = IO_READ(GI_DI_CONF_REG);
		end = rand_data() % 7 + 1;
		st = rand_data() % 6;
		tim = (end << GI_DI_CONF_TIM_END_SHIFT) | (st << GI_DI_CONF_TIM_START_SHIFT);
		mask = GI_DI_CONF_TIM_START_MASK | GI_DI_CONF_TIM_END_MASK; 
		conf = (conf & (~mask)) | tim;
		printf("Change DI(r) conf to 0x%x\n", conf);
		IO_WRITE(GI_DI_CONF_REG, conf);
	}

	for (st=0; st<num; st++) {
		snd_sz = DI_SIMPLE_CMD_SIZE;
		rx_sz = DI_SIMPLE_RESPOND_SIZE;
		tim = rand_data() % 3;
		if (tim) {
			if (tim == 1)
				rx_sz = ((rand_data() % 127) + 1) *  32;
			else snd_sz = 12 + ((rand_data() % 127) + 1) *  32 + 12;
		}
		
		tim = mask = 0;
		if (rand_data() & 1) tim = (rand_data() & 0x3ff) - 0x1ff;
		if (rand_data() & 1) mask = rand_data() & 0x1ff; 
		di_transaction(random_simple_cmd(), snd_sz, rx_sz, tim, mask, 0);
	} 

	return 0;
}

int di_cmd01_set_xfer(int which)
{
	unsigned int cmd, snd_sz, rx_sz, sd_stall, rx_stall, mask;
	unsigned int conf;

	displayMsg("Change cmd01 to be different than 0xa8 and 0");
	if (which == 0) {
		cmd = (random_simple_cmd() & 0xff) << GI_DI_CONF_SEC_CMD0_SHIFT;
		mask = GI_DI_CONF_SEC_CMD0_MASK;
	} else {
		cmd = (random_simple_cmd() & 0xff) << GI_DI_CONF_SEC_CMD1_SHIFT;
		mask = GI_DI_CONF_SEC_CMD1_MASK;
		if (which != 1) {
			cmd |= (random_simple_cmd() & 0xff) << GI_DI_CONF_SEC_CMD0_SHIFT;
			mask |= GI_DI_CONF_SEC_CMD0_MASK;
		}
	}
	conf = IO_READ(GI_DI_CONF_REG);
	cmd |= conf & (~mask);
	printf("Change DI conf(cmd01) to 0x%x\n", cmd);
	IO_WRITE(GI_DI_CONF_REG, cmd);

	snd_sz = DI_SIMPLE_CMD_SIZE;
	rx_sz = DI_SIMPLE_RESPOND_SIZE;
	sd_stall = rand_data() % 3;
	if (sd_stall) {
		if (sd_stall == 1)
			rx_sz = ((rand_data() % 127) + 1) *  32;
		else snd_sz = 12 + ((rand_data() % 127) + 1) *  32 + 12;
	}
		
	sd_stall = rx_stall = 0;
	if (rand_data() & 1) sd_stall = (rand_data() & 0x3ff) - 0x1ff;
	if (rand_data() & 1) rx_stall = rand_data() & 0x1ff; 
	if (which != 1) di_transaction(0xa8, snd_sz, rx_sz, sd_stall, rx_stall, 0);
	if (which != 0) di_transaction(0x0, snd_sz, rx_sz, sd_stall, rx_stall, 0);

	IO_WRITE(GI_DI_CONF_REG, conf);
	return 0;
}

int di_cmd01_rsp_xfer(int which, int change)
{
	unsigned int cmd,  mask, shft, sd_sz, rx_sz;
	unsigned int conf, pdata[3], gi_sram_offset;

	displayMsg("Check cmd01 for security response");
	conf = IO_READ(GI_DI_CONF_REG);
	mask = which ? GI_DI_CONF_SEC_CMD1_MASK : GI_DI_CONF_SEC_CMD0_MASK;
	shft = which ? GI_DI_CONF_SEC_CMD1_SHIFT: GI_DI_CONF_SEC_CMD0_SHIFT;

	if (change) {
		cmd = random_simple_cmd() & 0xff;
		IO_WRITE(GI_DI_CONF_REG, (conf & (~mask)) | (cmd << shft));
	} else cmd = (conf & mask) >> shft;

	create_random_data(pdata, 3);
	pdata[0] = (pdata[0] & (~GI_DI_CMD_BYTE_MASK)) | (cmd << GI_DI_CMD_BYTE_SHIFT);

	sd_sz = DI_SIMPLE_CMD_SIZE;
	rx_sz = DI_SIMPLE_RESPOND_SIZE;
	if (rand_data() & 1) {
		if (rand_data() & 1) rx_sz = ((rand_data() % 127) + 1) * 32;
		else sd_sz = 12 + ((rand_data() % 127) + 1) * 32 + 12;
	}	

	bd_put_zero_to_di(0, sd_sz/sizeof(int));
	bd_put_data_to_di(0, pdata, 3);

	bd_di_xfer(DIR_FLIPPER_TO_GI, sd_sz, rx_sz, 
		   sd_sz>DI_SIMPLE_CMD_SIZE, rand_data() & 1);
	
	gi_sram_offset = get_random_addr(GI_SRAM_START, GI_SRAM_SIZE, sd_sz);	
	di_burst(DI_SIMPLE_CMD_SIZE,  DIR_FLIPPER_TO_GI, gi_sram_offset);

	di_polling_interrupt(GI_DI_CTRL_DONE_INTR_MASK, 1);
	di_clear_interrupt(GI_DI_CTRL_DIR_INTR_MASK | GI_DI_CTRL_DONE_INTR_MASK);

	displayMsg("Check security response is assert");
	read_compare(GI_DI_CTRL_REG, GI_DI_CTRL_SEC_RSP_MASK, GI_DI_CTRL_SEC_RSP_MASK);

	if (sd_sz > DI_SIMPLE_CMD_SIZE) { 
		di_burst(sd_sz - DI_SIMPLE_CMD_SIZE, DIR_FLIPPER_TO_GI, gi_sram_offset);
		/* pull in data is ok */
		di_polling_interrupt(GI_DI_CTRL_DONE_INTR_MASK, 1);
		di_clear_interrupt(GI_DI_CTRL_DIR_INTR_MASK | GI_DI_CTRL_DONE_INTR_MASK);

		/* send out response */
		bd_di_xfer(DIR_GI_TO_FLIPPER, 0, DI_SIMPLE_RESPOND_SIZE, 0, rand_data() & 1);
		di_burst(DI_SIMPLE_RESPOND_SIZE, DIR_GI_TO_FLIPPER, gi_sram_offset);
	} else {
		bd_di_xfer(DIR_GI_TO_FLIPPER, 0, rx_sz, 0, rand_data() & 1);
		di_burst(rx_sz, DIR_GI_TO_FLIPPER, gi_sram_offset);
	}

	cmd = IO_READ(GI_DI_BE_REG);
	IO_STALL(0x1000);
	read_compare(GI_DI_BE_REG, cmd, WORD_MASK);

	if (change) IO_WRITE(GI_DI_CONF_REG, conf);
	di_reset(DI_RESET_FULLSEQ);
	read_compare(GI_DI_CTRL_REG, 0, GI_DI_CTRL_SEC_RSP_MASK);
	CLEAR_DI_INTR;

	return 0;
}

  /* DI xfer error case */
int di_xfer_err(int rand_timing)
{
	int rx_sz, snd_sz, st, end, tim, err=0;
	unsigned int conf, mask;
	int err_cmd[5] = {ERR_BEFORE_CMD,  ERR_IN_CMD, ERR_CMD, ERR_RESPOND_OUT};

	displayMsg("DI xfer Error case test");
	if (rand_timing) {
		conf = IO_READ(GI_DI_CONF_REG);
		end = rand_data() % 7 + 1;
		st = rand_data() % 6;
		tim = (end << GI_DI_CONF_TIM_END_SHIFT) | (st << GI_DI_CONF_TIM_START_SHIFT);
		mask = GI_DI_CONF_TIM_START_MASK | GI_DI_CONF_TIM_END_MASK; 
		conf = (conf & (~mask)) | tim;
		printf("Change DI(r) conf to 0x%x\n", conf);
		IO_WRITE(GI_DI_CONF_REG, conf);
	}

	snd_sz = DI_SIMPLE_CMD_SIZE;
	rx_sz = DI_SIMPLE_RESPOND_SIZE;
	tim = rand_data() % 3;
	err = err_cmd[rand_data() % 4];
	if (tim) {
		if (tim == 1)
			rx_sz = ((rand_data() % 127) + 1) *  32;
		else {
			snd_sz = 12 + ((rand_data() % 127) + 1) *  32 + 12;
			if (rand_data() & 1) err = ERR_SEND_DATA;
		}
	}
		
	tim = mask = 0;
	if (rand_data() & 1) tim = (rand_data() & 0x3ff) - 0x1ff;
	if (rand_data() & 1) mask = rand_data() & 0x1ff; 
	di_transaction(random_simple_cmd(), snd_sz, rx_sz, tim, mask, err);

	CLEAR_DI_INTR;	
	di_rand_simple_xfer(rand_data()&1, 1);
	return 0;
}

  /* DI xfer error cases */
int di_xfer_brk(int rand_timing)
{
	int rx_sz, snd_sz, st, end, tim, brk=0;
	unsigned int conf, mask;
	int brk_cmd[1] = {BRK_DI_SEND};

	displayMsg("DI xfer Break case test");
	if (rand_timing) {
		conf = IO_READ(GI_DI_CONF_REG);
		end = rand_data() % 7 + 1;
		st = rand_data() % 6;
		tim = (end << GI_DI_CONF_TIM_END_SHIFT) | (st << GI_DI_CONF_TIM_START_SHIFT);
		mask = GI_DI_CONF_TIM_START_MASK | GI_DI_CONF_TIM_END_MASK; 
		conf = (conf & (~mask)) | tim;
		printf("Change DI(r) conf to 0x%x\n", conf);
		IO_WRITE(GI_DI_CONF_REG, conf);
	}

	snd_sz = DI_SIMPLE_CMD_SIZE;
	rx_sz = DI_SIMPLE_RESPOND_SIZE;
	tim = rand_data() % 3;
	brk = brk_cmd[rand_data() % (sizeof(brk_cmd)/sizeof(int))];
	if (tim) {
		if (tim == 1)
			rx_sz = ((rand_data() % 127) + 1) *  32;
		else {
			snd_sz = 12 + ((rand_data() % 127) + 1) *  32 + 12;
		}
	}
		
	tim = mask = 0;
	if (rand_data() & 1) tim = (rand_data() & 0x3ff) - 0x1ff;
	if (rand_data() & 1) mask = rand_data() & 0x1ff; 
	di_transaction(random_simple_cmd(), snd_sz, rx_sz, tim, mask, brk);

	CLEAR_DI_INTR;	
	di_rand_simple_xfer(rand_data()&1, 1);
	return 0;
}

int di_xfer_tests()
{
	int i;
	printf("Bring DI host/flipper out of reset \n");
	displayMsg("Bring DI Host/Flipper out of reset");
 	di_reset(DI_RESET_FULLSEQ);
	
	displayMsg("Enable and clear nterrupts");
	ENABLE_DI_INTR;	
	CLEAR_DI_INTR;	
 	
	di_simple_xfer(0);
	di_simple_xfer(1);
	di_simple_xfer(1);
	di_simple_xfer(1);
	di_simple_xfer(1);

	di_rand_simple_xfer(0, 8);
	di_rand_simple_xfer(1, 0x10); 

	for (i=0; i<4; i++)  di_cmd01_set_xfer(rand_data() % 0x3); 

	for (i=0; i<16; i++)  
		di_cmd01_rsp_xfer(rand_data() & 1, rand_data() & 0x1); 

	// Not passed since 
	// Frank/Bob need to fix di mon for write DMA error
	// Frank/Bob need to fix dstrb not change while xfering.
	//for (i=0; i<0x3; i++) 
	//	di_xfer_err(rand_data() & 1); 
#if 0
	for (i=0; i<0x10; i++) 
		di_xfer_brk(rand_data() & 1); 
#endif
	
	return 0;
}
