
/* BB2 GI utils file */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "iosim.h"
#include "utils.h"
#include "gi.h"
#include "gi_utils.h"

#define T2RISC_PAT(x) (((x) >> 1) | ((((x) ^ ((x) >> 15) ^ 1) & 1) << 31))

  /* Default pat seed */
unsigned int t2risc_data = 0x20031015;  
void t2risc_pat_seed(unsigned int x)
{
	t2risc_data =  x;
}

int rand_times = 0;
unsigned int rand_data()
{
#ifndef T2RISC
	rand_times += 2;   /* Trace */
	return  ((rand() & 0xFFFF) << 16) | (rand() & 0xFFFF);
#else
	rand_times++;   /* Trace */
	t2risc_data = T2RISC_PAT(t2risc_data);
	return T2RISC_PAT(t2risc_data);
#endif
}

void create_random_data(
	unsigned int *pdata,   /* Random data array */
	int num)               /* number of data */
{
	int i;
	
	for (i=0; i<num; i++) 
		pdata[i] = rand_data();

	return; 
}

unsigned int get_random_addr(
	unsigned int start,             /* start address */
	unsigned int range,             /* range of space */
	unsigned int hold)              /* need to hold how many bytes */
{
	unsigned int addr;

	if (range == hold) addr = 0;
	else addr = (rand_data() % (range - hold)) & (~0x3);

	return start + addr;
} 

int read_compare(	          /* return -1, if failed */
	unsigned int addr,        /* register address */
	unsigned int exp,         /* register expect value */
	unsigned int mask)        /* mask bits */
{
	unsigned int rdata;

	rdata = IO_READ(addr);
	
	if ((rdata & mask) != (exp & mask)) {
#ifndef T2RISC
		printf("Failed: mismatch @ 0x%x r=0x%x exp=0x%x mask=0x%x\n",
		       addr, rdata, exp, mask);
#endif

#ifdef BB2_FPGA
		soc_printf("Failed: mismatch @ 0x%x r=0x%x exp=0x%x mask=0x%x\n",
			addr, rdata, exp, mask);
#endif
		return -1;
	}

	return 0;
}

int enter_sec_mode(int after_reset)
{
	if (!after_reset) IO_READ(GI_SEC_ENTER_REG);
#ifndef T2RISC
	displayMsg("Enter security mode ");
#endif
	IO_READ(GI_ROM_START);
	
	return 0;
}

int leave_sec_mode()
{
	unsigned int v;

	v = IO_READ(GI_SEC_MODE_REG);
	IO_WRITE(GI_SEC_MODE_REG, v & (~GI_SEC_MODE_SECURE_MASK));
	IO_READ(GI_SEC_MODE_REG);

	return 0;
}


/* check up r_w bits */
int reg_rw_test(
	unsigned int reg,             /* register address */
	unsigned int wen_mask,        /* Write enable mask */
	unsigned int rw_mask,         /* which bits are r/w */
	int verbose)                  
{
	unsigned int i, cur_mask, cur_value;
	unsigned int old_value, chk, rback;
	
	old_value = IO_READ(reg);

#ifndef T2RISC
	if (verbose) printf("Info: reg=0x%x v=0x%x \n", reg, old_value);
#endif

#ifdef BB2_FPGA
	if (verbose) soc_printf("Info: reg=0x%x v=0x%x \n", reg, old_value);
#endif

	for (i=0; i<32; i++) {
		cur_mask = 1 << i;
		cur_value = cur_mask &  old_value;

		if (rw_mask & cur_mask) {
			chk = (old_value & (~cur_mask)) | ((~cur_value) & cur_mask);
			IO_WRITE(reg, (chk | wen_mask));
			rback = IO_READ(reg);
#ifndef T2RISC
			if (verbose) printf("      write 0x%x rb 0x%x \n", chk, rback);
#endif

#ifdef BB2_FPGA
			if (verbose) soc_printf("      write 0x%x rb 0x%x \n", chk, rback);
#endif
			if (rback != chk) {
#ifndef T2RISC
				printf("Failed: 0x%x change %d bit to %d  error.\n", 
					reg, i, cur_value?0:1);
#endif

#ifdef BB2_FPGA
				soc_printf("Failed: 0x%x change %d bit to %d  error.\n",
					reg, i, cur_value?0:1);
#endif	
				return -1;
			}

			IO_WRITE(reg, old_value | wen_mask);
			rback = IO_READ(reg);
#ifndef T2RISC
			if (verbose) printf("      write 0x%x rb 0x%x \n", old_value, rback);
#endif

#ifdef BB2_FPGA
			if (verbose) soc_printf("      write 0x%x rb 0x%x \n", old_value, rback);
#endif
			if (rback != old_value) {
#ifndef T2RISC
				printf("Failed: 0x%x %d bit back_to %d error.\n", 
					reg, i, cur_value?1:0);
#endif

#ifdef BB2_FPGA
				soc_printf("Failed: 0x%x %d bit back_to %d error.\n", reg, i, cur_value?1:0);
#endif
				return -2;
			}
			
			if (wen_mask) {
				chk = (old_value & (~cur_mask)) | ((~cur_value) & cur_mask);
				IO_WRITE(reg, chk);
				rback = IO_READ(reg);
#ifndef T2RISC
				if (verbose) printf("      write 0x%x rb 0x%x \n", chk, rback);
#endif

#ifdef BB2_FPGA
				if (verbose) soc_printf("      write 0x%x rb 0x%x \n", chk, rback);
#endif
				if (rback == chk) {
#ifndef T2RISC
					printf("Failed: 0x%x change(w/o en) %d bit to %d error.\n", 
						reg, i, cur_value?0:1);
#endif

#ifdef BB2_FPGA
					soc_printf("Failed: 0x%x change(w/o en) %d bit to %d error.\n",
						 reg, i, cur_value?0:1);
#endif
					return -1;
				}
			}
		}
	}
	return 0;
}

/* test read only bits */
int reg_ro_test(
	unsigned int reg,             /* register address */
	unsigned int wen_mask,        /* Write enable mask */
	unsigned int ro_mask,         /* which bits are read only */
	int verbose)
{
	unsigned int i, cur_mask, cur_value;
	unsigned int old_value, chk, rback;
	
	old_value = IO_READ(reg);
#ifndef T2RISC
	if (verbose) printf("Info: reg=0x%x v=0x%x \n", reg, old_value);
#endif
	for (i=0; i<32; i++) {
		cur_mask = 1 << i;
		cur_value = cur_mask &  old_value;

		if (ro_mask & cur_mask) {
			chk = (old_value & (~cur_mask)) | ((~cur_value) & cur_mask);
			IO_WRITE(reg, chk | wen_mask);
			rback = IO_READ(reg);
#ifndef T2RISC
			if (verbose) printf("      write 0x%x rob 0x%x \n", chk, rback);
#endif

#ifdef BB2_FPGA
			if (verbose) soc_printf("      write 0x%x rob 0x%x \n", chk, rback);
#endif

			if (rback != old_value) {
#ifndef T2RISC
				printf("Failed(ro 0x%x): change %d bit to %d  error.\n", 
					reg, i, cur_value?0:1);
#endif
#ifdef BB2_FPGA
				soc_printf("Failed(ro 0x%x): change %d bit to %d  error.\n",
					reg, i, cur_value?0:1);
#endif
				return -1;
			}

			IO_WRITE(reg, old_value | wen_mask);
			rback = IO_READ(reg);
#ifndef T2RISC
			if (verbose) printf("      write 0x%x rob 0x%x \n", old_value, rback);
#endif
			if (rback != old_value) {
#ifndef T2RISC
				printf("Failed(ro 0x%x): %d bit back_to %d error.\n", 
					reg, i, cur_value?1:0);
#endif

#ifdef BB2_FPGA
				soc_printf("Failed(ro 0x%x): %d bit back_to %d error.\n",
					reg, i, cur_value?1:0);		
#endif
				return -2;
			}
		}
	}
	return 0;
}

int write_to_mem(          /* backdoor write data to sram/sDRAM */
	unsigned int offset,   /* SRAM address offset */
	unsigned int *pdata,
	int num)
{
	int i;
	unsigned int addr;

	for (i=0, addr=offset; i<num; i++, addr+=4)
		IO_WRITE(addr, pdata[i]);

	return 0;
}

