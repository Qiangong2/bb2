/* 
 *  general ipc request 
 *      
 *  :set tabstop=4
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include "iosim.h"

#define MAX_TRY  3
extern int keep_socket_alive;
unsigned int interrupt_line;

int sim_request_wait(IpcPkt *req, IpcPkt *rsp, int change_persist_socket, int wait)
{
	int i, fd;
	unsigned int *tmp;
	
	for (i = 0; (i < MAX_TRY) || wait; i++) {
		if ((fd = IpcOpen()) < 0) return -1;

		if (wait) 
			fprintf(stderr, "Try to connect to RTL server ...");
		if (IpcConnect(fd) < 0) {
			IpcClose(fd);
			sleep(3);
			if (errno == ECONNREFUSED) 
				continue;
		} else break;
	}
	if ((i == MAX_TRY) && !wait) return -1;
	
	tmp = (unsigned int *) (req);
	for (i=0; i<sizeof(IpcPkt)/sizeof(int); i++)
		tmp[i] = htonl(tmp[i]);
	if (IpcSend(fd, (char *)req, sizeof(IpcPkt)) < 0) {
		IpcClose(fd);
		return -1;
	}

	if (IpcReceive(fd, (char *)rsp, sizeof(IpcPkt)) < 0) {
		IpcClose(fd);
		return -1;
	}
	tmp = (unsigned int *) (rsp);
	for (i=0; i<sizeof(IpcPkt)/sizeof(int); i++)
		tmp[i] = ntohl(tmp[i]);
	interrupt_line = rsp->size;

	if (change_persist_socket == SOCKET_ALIVE) 
		keep_socket_alive = fd;
	
	if (change_persist_socket == SOCKET_CLOSE) 
		keep_socket_alive = 0;

	IpcClose(fd);
	return 0;
}

int sim_request(IpcPkt *req, IpcPkt *rsp, int change_persist_socket)
{
	return sim_request_wait(req, rsp, change_persist_socket, 0);
}
