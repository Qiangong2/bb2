
/* BB2 GI utils file */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "iosim.h"
#include "utils.h"
#include "gi.h"
#include "gi_utils.h"

int ipc_get_compare(	          /* return -1, if failed */
	unsigned int req_code,    /* register address */
	unsigned int param0,      /* parameter of ipc get*/
	unsigned int exp,         /* register expect value */
	unsigned int mask)        /* mask bits */
{
	unsigned int rdata;

	rdata = ipc_get_data(req_code, param0);
	
	if ((rdata & mask) != (exp & mask)) {
		printf("Failed: mismatch ipc_get 0x%x arg=0x%x r=0x%x exp=0x%x mask=0x%x\n",
		       req_code, param0, rdata, exp, mask);
		return -1;
	}

	return 0;
}

int bd_write_to_mem(          /* backdoor write data to sram/sDRAM */
	unsigned int offset,   /* SRAM address offset */
	unsigned int *pdata,
	int num)
{
	int i;
	unsigned int addr;

	for (i=0, addr=offset; i<num; i++, addr+=4) 
		BD_IO_WRITE(addr, pdata[i]);

	return 0;
}

int bd_check_mem_data(
	unsigned int offset,   /* SRAM/SDRAM address offset */
	unsigned int *pdata,
	int num)
{
	int i;
	unsigned int rdata, addr;

	for (i=0, addr=offset; i<num; i++, addr+=4) {
		rdata = BD_IO_READ(addr);
		if (rdata != pdata[i]) {
			printf("Failed: miscompare addr=0x%x exp=0x%x read=0x%x\n",
				addr, pdata[i], rdata);
			return -1;
		}
	}
	
	return 0;
}

int bd_fill_pattern(
	unsigned int addr,
	int size,
	unsigned int pat_seed,
	int skip)
{
	unsigned int data[4];

	data[0] = addr;
	data[1] = size;
	data[2] = pat_seed;
	data[3] = skip;
	
	ipc_set_multi_data(BD_PAT_FILL, data, 4);
	return 0;
}

int bd_check_pattern(
	unsigned int addr,
	int size,
	unsigned int pat_seed,
	int skip)
{
	unsigned int data[4];
	int ret;

	data[0] = addr;
	data[1] = size;
	data[2] = pat_seed;
	data[3] = skip;
	
	ret =  ipc_get_multi_param(BD_PAT_CHECK, data, 4);
	if (ret != 0)
		printf("Failed: Check pattern @0x%x size=0x%x skip=0x%x filed @ %d\n",
			addr, size, skip, ret);

	return ret;
}
