/* 
 *  Socket client to connect to simulator 
 *      
 *  :set tabstop=4
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "iosim.h"

    /* global variable to setup socket connection */
int keep_socket_alive = 0;
char *iosim_server = NULL;
int iosim_port = 0;

int IpcInit(void)
{
	char *sport;

	iosim_server = getenv("IOSIM_SERVER");
	sport = getenv("IOSIM_PORT");

	if (sport == NULL) {
		fprintf(stderr, "\n\nFatal error: please define IOSIM_PORT \n");
		return -1;
	}
	iosim_port = strtoul(sport, NULL, 0);
	
	if ((iosim_server == NULL) || (iosim_port <= 0)) {
		fprintf(stderr, "\n\nFatal error: please set environment variable: \n");
		fprintf(stderr, "             IOSIM_SERVER and IOSIM_PORT\n\n\n");
		return -1;
	}

	fprintf(stderr, "IOSIM server=%s port=%d\n", iosim_server, iosim_port);
	return 0;
}

int IpcReceive(int fd, char *buf, int nbytes)
{
	int nleft, nread;
	
	bzero(buf, nbytes);
	nleft = nbytes;
	while (nleft > 0) {
		nread = recv(fd, buf, nleft, 0);
		if (nread < 0) {
			fprintf(stderr, "IpcReceive: recv failed, errno=%d\n", errno);
			return -1;
		}
		nleft -= nread;
		buf += nread;
	}

	return( (nleft > 0) ? -1 : 0);
}

int IpcSend(int fd, char *buf, int nbytes)
{
	int nleft, nwritten;

	nleft = nbytes;
	while (nleft > 0) {
		nwritten = send(fd, buf, nleft, 0);
		if (nwritten <= 0) {
			fprintf(stderr, "IpcSend: send failed, errno=%d\n", errno);
			return -1;
		}
		nleft -= nwritten;
		buf += nwritten;
	}

	return( (nleft > 0) ? -1 : 0);
}

int IpcOpen(void)
{
	int fd;

	if (keep_socket_alive) 
		fd = keep_socket_alive;
	else fd = socket(AF_INET, SOCK_STREAM, 0);

	if (fd <= 0) {
		fprintf(stderr, "Cannot open AF_INET socket\n");
		return 0;
	}

	return fd;
}

void IpcClose(fd)
{
	if (!keep_socket_alive)	
		close(fd);

	return;
}

int IpcConnect(int fd)
{
	struct sockaddr_in      addr;
	int                     addrlen = sizeof (addr);

	if (keep_socket_alive) 
		return 0;

	memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = inet_addr(iosim_server);
	addr.sin_port = htons(iosim_port);

	if (connect(fd, (struct sockaddr *)&addr, addrlen) < 0) {  
		fprintf(stderr, "IpcConnect: connect failed, errno=%d\n", errno);
		return -1;
	}

	return 0;
}


	
