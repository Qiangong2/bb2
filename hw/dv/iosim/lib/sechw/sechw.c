/* iosim test sample code
    
   :set tabstop=4
 */

#include <iosim.h>
#include <utils.h>

#include <gi.h>
#include "sechw.h"
#include <aes.h>
#include <bsl.h>
#include <sha1.h>

/* MC unit functions */
#define PCI_DMA_BUF_SIZE 64
void hw_mc_sha_compute_setup(u32 *data, u32 data_offset, u32 hwdigest_offset, u32 size, u32 drop, u32 *dropdata){

  int k = 0;
  int i = 0;

  if(drop == 2){
    if(data != 0){ /* allow for no additional data in dram */
      IO_WRITE(GI_MC_ADDR_REG, (data_offset - PCI_DMA_BUF_SIZE)& GI_MC_ADDR_MEM_MASK);
    }
    k = 0;
    for(i = 0; i < PCI_DMA_BUF_SIZE; i=i+4){
      IO_WRITE(GI_SRAM_START + hwdigest_offset + i, dropdata[k]);
      k++;
    }
    
  }
  else if (drop == 3){
    if( data != 0){
      IO_WRITE(GI_MC_ADDR_REG, (data_offset - (PCI_DMA_BUF_SIZE*2))& GI_MC_ADDR_MEM_MASK);
    }
    k = 0;
    for(i = 0; i < (PCI_DMA_BUF_SIZE*2); i=i+4){
      IO_WRITE(GI_SRAM_START + hwdigest_offset + i, dropdata[k]);
      k++;
    }
  }
  /* write in MC addr */
  else { /* drop = 0 */
    IO_WRITE(GI_MC_ADDR_REG, (data_offset)& GI_MC_ADDR_MEM_MASK);
  }
  IO_WRITE(GI_MC_BUF_REG, hwdigest_offset & GI_MC_BUF_OFF_MASK);
  if(data != 0){
    k = 0;
    for(i = 0; i < size; i = i+ 4){
      IO_WRITE(data_offset + i, data[k]);
      k++;
    }
  }
  
}




void hw_mc_sha_compute_input(u32 bytes, u32 chaining, u32 drop){
  
  unsigned int size;
    

  if(drop ==2){
    bytes = bytes + PCI_DMA_BUF_SIZE;
  }
  else if(drop == 3){
    bytes = bytes + (PCI_DMA_BUF_SIZE*2);
  }

  size = (((bytes/BSL_AES_BLOCKSIZE_BYTES) -1)<< GI_MC_CTRL_SIZE_SHIFT) 
     & GI_MC_CTRL_SIZE_MASK;
  
  chaining = chaining << GI_MC_CTRL_CHAIN_SHIFT;

  drop = drop << GI_MC_CTRL_DROP_SHIFT;
  
  /* start SHA */
  IO_WRITE(GI_MC_CTRL_REG, (0xffffffff & GI_MC_CTRL_EXEC_MASK) | size | chaining | drop); 
  
  return;
}

#define GI_MC_SHA_POLL_CYCLES 350
int hw_mc_sha_compute_result(u32 *hwdigest, u32 hwdigest_offset, u32 size){
/* 
 * poll and wait for result and return it, if not yet done, return -1
 */

  int k =0;
  int blocks = 0;

  while (((IO_READ(GI_MC_CTRL_REG) & 0x80000000 )!= 0) /*&& 
							  blocks < size/BSL_SHA1_BLOCKSIZE*/){
    IO_STALL(GI_MC_SHA_POLL_CYCLES);
    printf("waiting...\n");

    blocks ++;
  }
  if((IO_READ(GI_MC_CTRL_REG) & 0x80000000 )!= 0){
    return -1;
  }
  /* read out result from second PCI buffer */
 
  for(k =0; k < 5; k++){
    hwdigest[k] = IO_READ(GI_SRAM_START + (hwdigest_offset) + (4*k) + 64);

  }
  return 0;
}
	  
/* functions for encryption/decryption */

void hw_mc_encrypt_iv_setup(u32 *iv, u32 dmabuf_offset, u32 iv_offset){
  int k = 0;
  int i = 0;

  IO_WRITE(GI_AESE_IV_REG,(iv_offset & GI_AESE_IV_IV_OFF_MASK ));
  IO_WRITE(GI_MC_BUF_REG, dmabuf_offset & GI_MC_BUF_OFF_MASK);


  /* write the iv */
  k =0;
  for(i = 0; i < BSL_AES_IVSIZE_BYTES; i= i+4){
    IO_WRITE(GI_SRAM_START + iv_offset + i, iv[k]);
    k++;
  }

}

/* just wrapper to set underlying unit key */

void hw_mc_encrypt_key_setup(u32 *key){
  
  hw_aes_encrypt_key_setup(key);

}
/* assume data is already present in the dram */

void hw_mc_encrypt_start(u32 bytes, u32 data_offset, u32 chaining){
  u32 size;
  u32 operation = 0x010000000; /* this is encryption */
  
  IO_WRITE(GI_MC_ADDR_REG, data_offset & GI_MC_ADDR_MEM_MASK);

  size = (((bytes/BSL_AES_BLOCKSIZE_BYTES) -1)<< GI_MC_CTRL_SIZE_SHIFT) 
     & GI_MC_CTRL_SIZE_MASK;
  
  chaining = chaining << GI_MC_CTRL_CHAIN_SHIFT;
  
  /* start AES */
  IO_WRITE(GI_MC_CTRL_REG, (0xffffffff & GI_MC_CTRL_EXEC_MASK) | size | chaining| operation); 
  
  return;
}


/* 
 * poll and wait for result and return it, if not yet done, return -1
 * results are left in dram
 */
#define GI_MC_AES_POLL_CYCLES 350
int hw_mc_encrypt_decrypt_result(u32 size){
  int blocks = 0;


  while (((IO_READ(GI_MC_CTRL_REG) & 0x80000000 )!= 0) 
&& blocks < size/BSL_AES_BLOCKSIZE_BYTES){
    IO_STALL(GI_MC_AES_POLL_CYCLES);
  }
  if((IO_READ(GI_MC_CTRL_REG) & 0x80000000 )!= 0){
    return -1;
  }

  return 0;
}
	  


void hw_mc_decrypt_iv_setup(u32 *iv, u32 dmabuf_offset, u32 iv_offset){
  int k = 0;
  int i = 0;

  IO_WRITE(GI_AESD_IV_REG,(iv_offset & GI_AESD_IV_IV_OFF_MASK ));
  IO_WRITE(GI_MC_BUF_REG, dmabuf_offset & GI_MC_BUF_OFF_MASK);

  /* write the iv */
  k =0;
  for(i = 0; i < BSL_AES_IVSIZE_BYTES; i= i+4){
    IO_WRITE(GI_SRAM_START + iv_offset + i, iv[k]);
    
    k++;
  }

}


#define GI_MC_KEY_STALL 100
void hw_mc_decrypt_key_setup(u32 *key, u32 *key2){
  
#if 0
  /* first make sure READY = 1, later change to context switch */
  IO_WRITE(GI_AES_KEXP_REG, 0x10);

  while((IO_READ(GI_AES_KEXP_REG) & GI_AES_KEXP_READY_MASK) != GI_AES_KEXP_READY_MASK){
    ;
  }

   

  IO_WRITE(GI_AES_KEY_REG, key[0]);
  IO_WRITE(GI_AES_KEY_REG, key[1]);
  IO_WRITE(GI_AES_KEY_REG, key[2]);
  IO_WRITE(GI_AES_KEY_REG, key[3]);

  IO_WRITE(GI_AES_KEXP_REG, 0x1c);
  
  /* poll till completion of key expansion */
  while((IO_READ(GI_AES_KEXP_REG) & GI_AES_KEXP_EXEC_MASK) != GI_AES_KEXP_EXEC_MASK){
    IO_STALL(GI_MC_KEY_STALL);
  }
  IO_WRITE(GI_AES_KEXP_REG, 0x0);

#endif

  hw_aes_decrypt_key_setup(key, key2);

  
  
}


void hw_mc_decrypt_start(u32 bytes, u32 data_offset, u32 chaining){
  u32 size;
  u32 operation = 0x20000000; /* this is decryption */


  IO_WRITE(GI_MC_ADDR_REG, data_offset & GI_MC_ADDR_MEM_MASK);
  
  size = (((bytes/BSL_AES_BLOCKSIZE_BYTES) -1)<< GI_MC_CTRL_SIZE_SHIFT) 
     & GI_MC_CTRL_SIZE_MASK;
  
  chaining = chaining << GI_MC_CTRL_CHAIN_SHIFT;
  
  /* start AES */
  IO_WRITE(GI_MC_CTRL_REG, (0xffffffff & GI_MC_CTRL_EXEC_MASK) | size | chaining| operation); 

  return;
}



/* high level functions */

int hw_mc_encrypt(u32 *iv, u32 iv_offset, u32 * key, u32 * data, u32 data_offset,u32 dmabuf_offset, u32 *dataenc,  u32 size, u32 chaining){
  u32 result, i, k, val;
  /* write data into dram */
  k = 0;
  for(i=0; i < size; i = i+ 4){
    IO_WRITE(data_offset + i, data[k]);
    k++;
  }
  hw_mc_encrypt_iv_setup(iv, dmabuf_offset, iv_offset);
  hw_mc_encrypt_key_setup(key);
  hw_mc_encrypt_start( size, data_offset, chaining);
  result =  hw_mc_encrypt_decrypt_result(size);
  /* extract result from dram */
  k = 0;
  for(i=0; i < size; i = i+ 4){
    val = IO_READ(data_offset + i);
    dataenc[k] = val; k++;
  }
	  
  return result;

}

int  hw_mc_decrypt(u32 *iv, u32 iv_offset, u32 *key, u32 * dataenc, u32 data_offset, u32 dmabuf_offset, u32 *datadec, u32 size, u32 chaining, u32 *key2){
  u32 result=0, i, k, val;
  /* write data into dram */
  
  k = 0;
  for(i=0; i < size; i = i+ 4){
    IO_WRITE(data_offset + i, dataenc[k]);
    k++;
  }
  
  hw_mc_decrypt_iv_setup(iv, dmabuf_offset, iv_offset);
  hw_mc_decrypt_key_setup(key, key2);
  hw_mc_decrypt_start( size, data_offset, chaining);
  
  result =  hw_mc_encrypt_decrypt_result(size);
  
  
  k = 0;
  for(i=0; i < size; i = i+ 4){
    val = IO_READ(data_offset + i);
    datadec[k] = val; k++;
  }
  
  return result;

}

/* hmac function */
  


void 
mc_hmac_key_setup(u32 *key, u32 keylen, u32 keystore_offset){
  u32 ipad[16];
  u32 opad[16];
  int i, k;
  for(i=0; i< 16; i++){
    ipad[i] = 0x36363636;
    opad[i] = 0x5c5c5c5c;
  }

  for(i=0; i < keylen/4; i++){
    ipad[i] = ipad[i] ^ key[i];
    opad[i] = opad[i] ^ key[i];
  }

  /* write to some location in GI SRAM */
  k = 0;
  for(i=0; i< 64; i= i+4){
    IO_WRITE(GI_SRAM_START + keystore_offset + i, ipad[k]);
    k++;
  }
  k = 0;
  for(i=64; i< 128; i= i+4){
    IO_WRITE(GI_SRAM_START + keystore_offset + i, opad[k]);
    k++;
  }
}


void
mc_hmac_compute(u32 *data, u32 data_offset, u32 *key, u32 keylen, u32 size, u32 mcbuf_offset, u32 keystore_offset, u32 *hw_hmac){
  
  int i, k;
 
  u32 hwdigest[5];
  u32 dropdata[32];
  u32 readtopad[32];
  mc_hmac_key_setup(key, keylen, keystore_offset);
  
  /* for now copy back from the keystore_offset to dropdata and call */
  /* do two SHA ops */
  /* copy from keystore_offset the first half to first half of mcbuf */
  k = 0;
  for(i= 0; i < 64; i= i +4){
    dropdata[k] = IO_READ(GI_SRAM_START + keystore_offset +i);
    k++;
  }


  hw_mc_sha_compute(data, data_offset, size, hwdigest, mcbuf_offset, 0x0, 0x2, dropdata);

  /* pad second PCI buffer : number of bits = 512 +160 */
  /* read in the values and pad */
  k = 0;
  for(i =0; i < 64; i = i+4){
    readtopad[k] = IO_READ(GI_SRAM_START + mcbuf_offset + 64 + i);
    k++;
  }
  for(i = 5; i < 16; i++){
    readtopad[i] = 0;
  }
  readtopad[5] = readtopad[5] | 0x80000000;
  /* add the length in bits : 512 + 160 */
  readtopad[15] = readtopad[15] | 0x2a0; 

  /* copy the first buffer, opad xor key to dropdata */
  for(i=0; i< 32; i++){
    dropdata[i] = 0x0;
  }
  k = 0;
  for(i =0; i < 64; i = i+4){
    dropdata[k] = IO_READ(GI_SRAM_START + keystore_offset + 64 + i);
    k++;
  }
  /* rest of dropdata has the padded hash */
  for(i = 0; i < 64; i = i +4){
    dropdata[k] = readtopad[k - 16];
    k++;
  }

  /* now call the sha a second time */
  hw_mc_sha_compute(0, 0, 0, hw_hmac, mcbuf_offset, 0x0, 0x3, dropdata);

  /* result is in hwdigest */
  
}


int hw_mc_sha_compute(u32 *data, u32 data_offset, u32 size, u32 *hwdigest, u32 hwdigest_offset, u32 chaining, u32 drop, u32 *dropdata){
  hw_mc_sha_compute_setup(data, data_offset, hwdigest_offset, size, drop, dropdata);
  hw_mc_sha_compute_input(size, chaining, drop);
  return hw_mc_sha_compute_result(hwdigest, hwdigest_offset, size);
}


/* SHA unit functions */

void hw_sha_compute_setup(u32 *data, u32 data_offset, u32 size){

  int k = 0;
  int i = 0;


  k = 0;
  for(i = 0; i < size; i = i+ 4){
    IO_WRITE(GI_SRAM_START + data_offset + i, data[k]);
    k++;
  }

}

void hw_sha_compute_input(u32 data_offset, u32 bytes, u32 hwdigest_offset, u32 save, u32 chaining){
  
  unsigned int size, ptr;
    
  size = (((bytes/BSL_SHA1_BLOCKSIZE) -1)<< GI_SHA_CTRL_SIZE_SHIFT) 
     & GI_SHA_CTRL_SIZE_MASK;
  
  ptr = data_offset  & GI_SHA_CTRL_PTR_MASK;
  save = save << GI_SHA_CTRL_SAVE_SHIFT;
  IO_WRITE(GI_SHA_BUF_REG, hwdigest_offset & GI_SHA_BUF_OFF_MASK);
  
  /* start SHA */
  
  IO_WRITE(GI_SHA_CTRL_REG, (0xffffffff & GI_SHA_CTRL_EXEC_MASK) | size | ptr | save | chaining); 
  
  return;
}

#define GI_SHA_POLL_CYCLES 150
int hw_sha_compute_result(u32 *hwdigest, u32 hwdigest_offset, u32 size){
/* 
 * poll and wait for result and return it, if not yet done, return -1
 */

  int k =0;
  int blocks = 0;

  while (((IO_READ(GI_SHA_CTRL_REG) & 0x80000000 )!= 0) /*&& 
							  blocks < size/BSL_SHA1_BLOCKSIZE*/){
    IO_STALL(GI_SHA_POLL_CYCLES);
    blocks ++;
  }
  if((IO_READ(GI_SHA_CTRL_REG) & 0x80000000 )!= 0){
    return -1;
  }
  /* read out result */
 
  for(k =0; k < 5; k++){
    hwdigest[k] = IO_READ(GI_SRAM_START + (hwdigest_offset) + (4*k));

  }
  return 0;
}
	  






/* high level functions */

  int hw_sha_compute(u32 *data, u32 data_offset, u32 size, u32 *hwdigest, u32 hwdigest_offset, u32 chaining, u32 save){
  hw_sha_compute_setup(data, data_offset, size);

  hw_sha_compute_input(data_offset, size, hwdigest_offset, save, chaining);
  return hw_sha_compute_result(hwdigest, hwdigest_offset, size);
}


int hw_aes_encrypt(u32 *iv, u32 iv_offset, u32 * key, u32 * data, u32 data_offset, u32 *dataenc_hw, u32 size, u32 chaining){
  hw_aes_encrypt_setup(iv, iv_offset, data, data_offset, size);
  hw_aes_encrypt_key_setup(key);
  hw_aes_encrypt_start(size,  data_offset, chaining);
  return hw_aes_encrypt_result(dataenc_hw, data_offset, size);
}
	  
int hw_aes_decrypt(u32 *iv, u32 iv_offset, u32 *key, u32 *dataenc_hw, u32 data_offset, u32 *datadec, u32 size, u32 chaining, u32 *key1){
  hw_aes_decrypt_setup(iv, iv_offset, dataenc_hw, data_offset, size);
  hw_aes_decrypt_key_setup(key, key1);
  hw_aes_decrypt_start(size, data_offset, chaining);
  return hw_aes_decrypt_result(datadec, data_offset, size);
}


/* 
 * iv = 16 bytes of IV data
 * iv_offset = offset in PI buffer where it is stored, in kbytes
 * data = size bytes of data
 * data_offset = offset in PI buffer where it is stored, in kbytes
 * size = bytes of data
 * chain = 1 if we want hardware chaining from second block onwards
 * if IV = NULL chain = 1 always
 */
void hw_aes_encrypt_setup(u32 *iv, u32 iv_offset, u32 *data, u32 data_offset, u32 size){
  int k = 0;
  int i = 0;

  IO_WRITE(GI_AESE_IV_REG,(iv_offset & GI_AESE_IV_IV_OFF_MASK ));

  /* write the iv */
  k =0;
  for(i = 0; i < BSL_AES_IVSIZE_BYTES; i= i+4){
    IO_WRITE(GI_SRAM_START + iv_offset + i, iv[k]);
    k++;
  }

  k = 0;
  for(i = 0; i < size; i = i+ 4){
    IO_WRITE(GI_SRAM_START + data_offset + i, data[k]);
    k++;
  }

}


void hw_aes_encrypt_key_setup(u32 *key){
 
  /* first make sure READY = 1*/
  while((IO_READ(GI_AES_KEXP_REG) & GI_AES_KEXP_READY_MASK) != GI_AES_KEXP_READY_MASK){
   ;
  }

/* write key, and expand it */

  IO_WRITE(GI_AES_KEY_REG, key[0]);
  IO_WRITE(GI_AES_KEY_REG, key[1]);
  IO_WRITE(GI_AES_KEY_REG, key[2]);
  IO_WRITE(GI_AES_KEY_REG, key[3]);
}


/* 
 * enter key and kick off encryption, return 
 */
void hw_aes_encrypt_start(u32 bytes, u32 data_offset, u32 chaining){
 
  unsigned size, ptr;

  size = (((bytes/BSL_AES_BLOCKSIZE_BYTES) -1)<< GI_AESE_CTRL_SIZE_SHIFT) 
     & GI_AESE_CTRL_SIZE_MASK;
  ptr = data_offset  & GI_AESE_CTRL_PTR_MASK;

  /* start AES */
  IO_WRITE(GI_AESE_CTRL_REG, (0xffffffff & GI_AESE_CTRL_EXEC_MASK) | size | ptr | chaining); 
}


/* 
 * poll and wait for result and return it, if not yet done, return -1
 */
#define GI_AES_POLL_CYCLES 50
int hw_aes_encrypt_result(u32 *dataenc, u32 data_offset, u32 size){
  int i =0; 
  int k =0;
  int blocks = 0;
  u32 val;

  while (((IO_READ(GI_AESE_CTRL_REG) & 0x80000000 )!= 0)&& 
	 blocks < size/BSL_AES_BLOCKSIZE_BYTES){
    IO_STALL(GI_AES_POLL_CYCLES);
  }
  if((IO_READ(GI_AESE_CTRL_REG) & 0x80000000 )!= 0){
    return -1;
  }
  /* read out result */
  for(i=0; i < size; i = i+ 4){
    val = IO_READ(GI_SRAM_START + data_offset + i);
    dataenc[k] = val; k++;
  }
  return 0;
}
	  


/* 
 * iv = 16 bytes of IV data
 * iv_offset = offset in PI buffer where it is stored, in kbytes
 * data = size bytes of data
 * data_offset = offset in PI buffer where it is stored, in kbytes
 * size = bytes of data
 * chain = 1 if we want hardware chaining from second block onwards
 * if IV = NULL chain = 1 always
 */
void hw_aes_decrypt_setup(u32 *iv, u32 iv_offset, u32 *data_enc, u32 data_offset, u32 size){
  int k = 0;
  int i = 0;

  IO_WRITE(GI_AESD_IV_REG,(iv_offset & GI_AESD_IV_IV_OFF_MASK ));

  /* write the iv */
  k =0;
  for(i = 0; i < BSL_AES_IVSIZE_BYTES; i= i+4){
    IO_WRITE(GI_SRAM_START + iv_offset + i, iv[k]);
    k++;
  }

  k = 0;
  for(i = 0; i < size; i = i+ 4){
    IO_WRITE(GI_SRAM_START + data_offset + i, data_enc[k]);
    k++;
  }

}

 
int hw_aes_decrypt_start(u32 bytes, u32 data_offset, u32 chaining){

  unsigned size, ptr;

  size = (((bytes/BSL_AES_BLOCKSIZE_BYTES) -1)<< GI_AESD_CTRL_SIZE_SHIFT) 
     & GI_AESD_CTRL_SIZE_MASK;
  ptr = data_offset  & GI_AESD_CTRL_PTR_MASK;

  /* start AES */
  IO_WRITE(GI_AESD_CTRL_REG, (0xffffffff & GI_AESD_CTRL_EXEC_MASK) | size | ptr | chaining); 

  return 0;
}

/* 
 * enter key and kick off decryption, return -1 if key expander is busy 
 * add new key key2 if one existed before unstalling
 * context switch existing to key, and then restore key2
 * if nothing to restore, pass key2 = 0
 * 
 */
int hw_aes_decrypt_key_setup(u32 *key, u32 *key2){
  
/* write key, and expand it */

  IO_WRITE(GI_AES_KEXP_REG, 0x10);

  while((IO_READ(GI_AES_KEXP_REG) & GI_AES_KEXP_READY_MASK) != GI_AES_KEXP_READY_MASK){
    ;
  }

   
  /* write the key needed */
  IO_WRITE(GI_AES_KEY_REG, key[0]);
  IO_WRITE(GI_AES_KEY_REG, key[1]);
  IO_WRITE(GI_AES_KEY_REG, key[2]);
  IO_WRITE(GI_AES_KEY_REG, key[3]);


  IO_WRITE(GI_AES_KEXP_REG, 0x1c);
  
  /* poll till completion of key expansion */
  while((IO_READ(GI_AES_KEXP_REG) & GI_AES_KEXP_EXEC_MASK) != GI_AES_KEXP_EXEC_MASK){
    IO_STALL(GI_MC_KEY_STALL);
  }

  /* if there is a key to be restored, restore it */
  if(key2){
    IO_WRITE(GI_AES_KEY_REG, key2[0]);
    IO_WRITE(GI_AES_KEY_REG, key2[1]);
    IO_WRITE(GI_AES_KEY_REG, key2[2]);
    IO_WRITE(GI_AES_KEY_REG, key2[3]);
  }
    
  /*unstall */
  IO_WRITE(GI_AES_KEXP_REG, 0x0);
  
	
  return 0;
}


/* 
 * poll and wait for result and return it, if not yet done, return -1
 */
#define GI_AES_POLL_CYCLES 50
int hw_aes_decrypt_result(u32 *data, u32 data_offset, u32 size){
  int i =0; 
  int k =0;
  int blocks = 0;
  u32 val;

  while (((IO_READ(GI_AESD_CTRL_REG) & 0x80000000 )!= 0)&& 
	 blocks < size/BSL_AES_BLOCKSIZE_BYTES){
    IO_STALL(GI_AES_POLL_CYCLES);
  }
  if((IO_READ(GI_AESD_CTRL_REG) & 0x80000000 )!= 0){
    return -1;
  }
  /* read out result */
  for(i=0; i < size; i = i+ 4){
    val = IO_READ(GI_SRAM_START + data_offset + i);
    data[k] = val; k++;
  }
  return 0;
}
	  
