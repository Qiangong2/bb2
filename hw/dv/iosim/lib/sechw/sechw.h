/* iosim test sample code
    
   :set tabstop=4
 */

#include <iosim.h>
#include <utils.h>
#include <bbtypes.h>

#include <gi.h>

/* MC functions */
/* high level functions */
int hw_mc_sha_compute(u32 *data, u32 data_offset, u32 size, u32 *hwdigest, u32 hwdigest_offset, u32 chaining, u32 drop, u32 *dropdata);
int hw_mc_encrypt(u32 *iv, u32 iv_offset, u32 * key, u32 * data, u32 data_offset,u32 dmabuf_offset, u32 *dataenc,  u32 size, u32 chaining);
int  hw_mc_decrypt(u32 *iv, u32 iv_offset, u32 *key, u32 * dataenc, u32 data_offset, u32 dmabuf_offset, u32 *datadec, u32 size, u32 chaining, u32 *key2);

/* keylen is length of key in bytes, multiple of 4 */
void mc_hmac_key_setup(u32 *key, u32 keylen, u32 keystore_offset);
void mc_hmac_compute(u32 *data, u32 data_offset, u32 *key, u32 keylen,  u32 size, u32 mcbuf_offset, u32 keystore_offset, u32 *hw_hmac);

/* lower level MC functions */
void hw_mc_sha_compute_setup(u32 *data, u32 data_offset, u32 hwdigest_offset, u32 size, u32 drop, u32 *dropdata);
void hw_mc_sha_compute_input(u32 bytes, u32 chaining, u32 drop);
int hw_mc_sha_compute_result(u32 *hwdigest, u32 hwdigest_offset, u32 size);

void hw_mc_encrypt_iv_setup(u32 *iv, u32 dmabuf_offset, u32 iv_offset);
void hw_mc_encrypt_key_setup(u32 *key);
void hw_mc_encrypt_start(u32 bytes, u32 data_offset, u32 chaining);
int hw_mc_encrypt_decrypt_result(u32 size);
void hw_mc_decrypt_iv_setup(u32 *iv, u32 dmabuf_offset, u32 iv_offset);
void hw_mc_decrypt_key_setup(u32 *key, u32 *key2);
void hw_mc_decrypt_start(u32 bytes, u32 data_offset, u32 chaining);




/* SHA functions */
/* SHA high level function */
int hw_sha_compute(u32 *data, u32 data_offset, u32 size, u32 *hwdigest, u32 hwdigest_offset, u32 chaining, u32 save);
  
/* SHA block based processing */
void hw_sha_compute_setup(u32 *data, u32 data_offset, u32 size);

void hw_sha_compute_input(u32 data_offset, u32 bytes, u32 hwdigest_offset, u32 save, u32 chaining);

int hw_sha_compute_result(u32 *hwdigest, u32 hwdigest_offset, u32 size);

/* AES functions */
/* AES high level functions */
int hw_aes_encrypt(u32 *iv, u32 iv_offset, u32 * key, u32 * data, u32 data_offset, u32 *dataenc_hw, u32 size, u32 chaining);
	  
int hw_aes_decrypt(u32 *iv, u32 iv_offset, u32 *key, u32 *dataenc_hw, u32 data_offset, u32 *datadec, u32 size, u32 chaining, u32 *key1);

/* AES block processing functions */
void hw_aes_encrypt_setup(u32 *iv, u32 iv_offset, u32 *data, 
			  u32 data_offset, u32 size);
void hw_aes_encrypt_key_setup(u32 *key);
void hw_aes_encrypt_start(u32 bytes, u32 data_offset, u32 chaining);

int hw_aes_encrypt_result(u32 *dataenc, u32 data_offset, u32 size);

void hw_aes_decrypt_setup(u32 *iv, u32 iv_offset, u32 *data_enc, 
			  u32 data_offset, u32 size);

int hw_aes_decrypt_key_setup(u32 *key, u32 *key2);
int hw_aes_decrypt_start(u32 bytes, u32 data_offset, u32 chaining);

int hw_aes_decrypt_result(u32 *data, u32 data_offset, u32 size);
