/*
 *  general ipc request 
 *      
 *  :set tabstop=4
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <ctype.h>
#include "iosim.h"
#include "utils.h"

/*  Try to connect with ipc simulator
    Return 0 if succeed.
 */
int sim_request_wait(IpcPkt *req, IpcPkt *rsp, int change_persist_socket, int wait);
int FindServer(void)
{
	IpcPkt req, rsp;

	memset(&req, 0, sizeof(IpcPkt));
	memset(&rsp, 0, sizeof(IpcPkt));
	req.length = sizeof(IpcPkt);
	req.code = BD_REQ_HANDSHAKE;
	
	if (sim_request_wait(&req, &rsp, 0, 1)) 
		return -1;
	
	return (rsp.code != RSP_OK); 
}

void sim_write(unsigned int addr, int size, unsigned int data)
{
	IpcPkt   req, rsp;

	memset(&req, 0, sizeof(IpcPkt));
	memset(&rsp, 0, sizeof(IpcPkt));

	req.length = sizeof(IpcPkt);
	req.size = size;
	req.code = REQ_SINGLE_WRITE;
	req.address = addr;
	req.data[0] = data;

	if (sim_request(&req, &rsp, 0)) 
		fprintf(stderr, "Error : Ipc Single Write \n");
	
	return; 
}

void bd_sim_write(unsigned int addr, int size, unsigned int data)
{
	IpcPkt   req, rsp;

	memset(&req, 0, sizeof(IpcPkt));
	memset(&rsp, 0, sizeof(IpcPkt));

	req.length = sizeof(IpcPkt);
	req.size = size;
	req.code = BD_REQ_SINGLE_WRITE;
	req.address = addr;
	req.data[0] = data; 

	if (sim_request(&req, &rsp, 0)) 
		fprintf(stderr, "Error : Ipc Single Write (Backdoor) \n");
	
	return; 
}

unsigned int sim_read(unsigned int addr, int size)
{
	IpcPkt   req, rsp;

	memset(&req, 0, sizeof(IpcPkt));
	memset(&rsp, 0, sizeof(IpcPkt));

	req.length = sizeof(IpcPkt);
	req.size = size;
	req.code = REQ_SINGLE_READ;
	req.address = addr; 

	if (sim_request(&req, &rsp, 0)) 
		fprintf(stderr, "Error : Ipc Single Read \n");
	
	return rsp.data[0]; 
}

unsigned int bd_sim_read(unsigned int addr, int size)
{
	IpcPkt   req, rsp;

	memset(&req, 0, sizeof(IpcPkt));
	memset(&rsp, 0, sizeof(IpcPkt));

	req.length = sizeof(IpcPkt);
	req.size = size;
	req.code = BD_REQ_SINGLE_READ;
	req.address = addr; 

	if (sim_request(&req, &rsp, 0)) 
		fprintf(stderr, "Error : Ipc Single Read(Backdoor) \n");
	
	return rsp.data[0]; 
}

void ipc_set_data(unsigned int req_code, unsigned int data)
{
	IpcPkt   req, rsp;

	memset(&req, 0, sizeof(IpcPkt));
	memset(&rsp, 0, sizeof(IpcPkt));

	req.length = sizeof(IpcPkt);
	req.code = req_code;
	req.data[0] = data;

	if (sim_request(&req, &rsp, 0)) 
		fprintf(stderr, "Error : Ipc Set Data \n");
	
	return; 
}

void ipc_set_multi_data(unsigned int req_code, unsigned int *pdata, int num)
{
	IpcPkt   req, rsp;
	int i;

	memset(&req, 0, sizeof(IpcPkt));
	memset(&rsp, 0, sizeof(IpcPkt));

	req.length = sizeof(IpcPkt);
	req.code = req_code;
	for (i=0; i<num && i<PKT_DATA_WORD; i++)
		req.data[i] = pdata[i];

	if (sim_request(&req, &rsp, 0)) 
		fprintf(stderr, "Error : Ipc Set Data \n");
	
	return; 
}

unsigned int ipc_get_data(unsigned int req_code, int param0)
{
	IpcPkt   req, rsp;

	memset(&req, 0, sizeof(IpcPkt));
	memset(&rsp, 0, sizeof(IpcPkt));

	req.length = sizeof(IpcPkt);
	req.code = req_code;
	req.data[0] = param0;

	if (sim_request(&req, &rsp, 0)) 
		fprintf(stderr, "Error : Ipc Get Data \n");
	
	return rsp.data[0]; 
}

unsigned int ipc_get_multi_param(unsigned int req_code, unsigned int *param, int num)
{
	IpcPkt   req, rsp;
	int i;

	memset(&req, 0, sizeof(IpcPkt));
	memset(&rsp, 0, sizeof(IpcPkt));

	req.length = sizeof(IpcPkt);
	req.code = req_code;
	for (i=0; i<8 && i<num; i++)
		req.data[i] = param[i];

	if (sim_request(&req, &rsp, 0)) 
		fprintf(stderr, "Error : Ipc Get Data \n");
	
	return rsp.data[0]; 
}

long long rtl_time()
{
	IpcPkt   req, rsp;
	long long tt;

	memset(&req, 0, sizeof(IpcPkt));
	memset(&rsp, 0, sizeof(IpcPkt));

	req.length = sizeof(IpcPkt);
	req.code = BD_REQ_RTL_TIME;

	if (sim_request(&req, &rsp, 0)) 
		fprintf(stderr, "Error : Ipc Get RTL time \n");
	
	tt = rsp.data[0];
	tt = (tt << 32) | rsp.data[1];
	return tt;
}

void sim_write_v(unsigned int addr, int size, char *xz)
{
	IpcPkt   req, rsp;

	memset(&req, 0, sizeof(IpcPkt));
	memset(&rsp, 0, sizeof(IpcPkt));

	req.length = sizeof(IpcPkt);
	req.size = size;
	req.code = REQ_SINGLE_WRITE;
	xz_to_data(0, xz, req.data);
	req.address = addr; 

	if (sim_request(&req, &rsp, 0)) 
		fprintf(stderr, "Error : Ipc Write_xz \n");

	return;
}

void bd_sim_write_v(unsigned int addr, int size, char *xz)
{
	IpcPkt   req, rsp;

	memset(&req, 0, sizeof(IpcPkt));
	memset(&rsp, 0, sizeof(IpcPkt));

	req.length = sizeof(IpcPkt);
	req.size = size;
	req.code = BD_REQ_SINGLE_WRITE;
	xz_to_data(0, xz, req.data);
	req.address = addr; 

	if (sim_request(&req, &rsp, 0)) 
		fprintf(stderr, "Error : Ipc Write_xz(Backdoor) \n");

	return;
}

void sim_read_v(unsigned int addr, int size, char *xz)
{
	IpcPkt   req, rsp;

	memset(&req, 0, sizeof(IpcPkt));
	memset(&rsp, 0, sizeof(IpcPkt));

	req.length = sizeof(IpcPkt);
	req.size = size;
	req.code = REQ_SINGLE_READ;
	req.address = addr; 

	if (sim_request(&req, &rsp, 0)) 
		fprintf(stderr, "Error : Ipc Read_xz \n");

	data_to_xz(0, rsp.data, xz);
	return; 
}

void bd_sim_read_v(unsigned int addr, int size, char *xz)
{
	IpcPkt   req, rsp;

	memset(&req, 0, sizeof(IpcPkt));
	memset(&rsp, 0, sizeof(IpcPkt));

	req.length = sizeof(IpcPkt);
	req.size = size;
	req.code = BD_REQ_SINGLE_READ;
	req.address = addr; 

	if (sim_request(&req, &rsp, 0)) 
		fprintf(stderr, "Error : Ipc Read_xz (Backdoor) \n");

	data_to_xz(0, rsp.data, xz);
	return; 
}

void sim_bwrite(unsigned int addr, int size, unsigned int *pdata)
{
	IpcPkt   req, rsp;
	int i;

	memset(&req, 0, sizeof(IpcPkt));
	memset(&rsp, 0, sizeof(IpcPkt));

	req.length = sizeof(IpcPkt);
	req.size = size;
	req.code = REQ_BLOCK_WRITE;
	req.address = addr; 

	for (i=0; (i<size) && (i<PKT_DATA_WORD); i++) {
		DATA_PART(i, req.data) = DATA_PART(i, pdata);
		XZ_PART(i, req.data) = XZ_PART(i, pdata);
	}

	if (sim_request(&req, &rsp, 0)) 
		fprintf(stderr, "Error : Ipc Block Write \n");

	return;
}

void bd_sim_bwrite(unsigned int addr, int size, unsigned int *pdata)
{
	IpcPkt   req, rsp;
	int i;

	memset(&req, 0, sizeof(IpcPkt));
	memset(&rsp, 0, sizeof(IpcPkt));

	req.length = sizeof(IpcPkt);
	req.size = size;
	req.code = BD_REQ_BLOCK_WRITE;
	req.address = addr; 

	for (i=0; (i<size) && (i<PKT_DATA_WORD); i++) {
		DATA_PART(i, req.data) = DATA_PART(i, pdata);
		XZ_PART(i, req.data) = XZ_PART(i, pdata);
	}

	if (sim_request(&req, &rsp, 0)) 
		fprintf(stderr, "Error : Ipc Block Write(backdoor) \n");

	return;
}

void sim_bread(unsigned int addr, int size, unsigned int *pdata)
{
	IpcPkt   req, rsp;
	int i;

	memset(&req, 0, sizeof(IpcPkt));
	memset(&rsp, 0, sizeof(IpcPkt));

	req.length = sizeof(IpcPkt);
	req.size = size;
	req.code = REQ_BLOCK_READ;
	req.address = addr; 

	if (sim_request(&req, &rsp, 0)) 
		fprintf(stderr, "Error : Ipc Block Read \n");

	for (i=0; (i<size) && (i<PKT_DATA_WORD); i++) {
		DATA_PART(i, pdata) = DATA_PART(i, rsp.data);
		XZ_PART(i, pdata) = XZ_PART(i, rsp.data);
	}
	
	return; 
}

void bd_sim_bread(unsigned int addr, int size, unsigned int *pdata)
{
	IpcPkt   req, rsp;
	int i;

	memset(&req, 0, sizeof(IpcPkt));
	memset(&rsp, 0, sizeof(IpcPkt));

	req.length = sizeof(IpcPkt);
	req.size = size;
	req.code = BD_REQ_BLOCK_READ;
	req.address = addr; 

	if (sim_request(&req, &rsp, 0)) 
		fprintf(stderr, "Error : Ipc block Read(backdoor) \n");

	for (i=0; (i<size) && (i<PKT_DATA_WORD); i++) {
		DATA_PART(i, pdata) = DATA_PART(i, rsp.data);
		XZ_PART(i, pdata) = XZ_PART(i, rsp.data);
	}
	
	return; 
}

/* Convert single hex char to hex value
   return:
        -1 if failed
        (xz << 4) | (data) 
 */
inline int convert_char_to_hex(char ch)
{
	switch (ch) {
		case 'x':
		case 'X':
			return 0xff;
		case 'z':
		case 'Z':
			return 0xf0;
		default:
			if (ch >= '0' && ch <= '9') return ch - '0';
			if (ch >= 'a' && ch <= 'f') return ch - 'a' + 10;
			if (ch >= 'A' && ch <= 'F') return ch - 'A' + 10;
			break;
	}

	return -1;
} 

/* Convert single binary char to binary value
   return:
        -1 if failed
        (xz << 1) | (data) 
 */
inline int convert_char_to_bin(char ch)
{
	switch (ch) {
		case 'x':
		case 'X':
			return 3;
		case 'z':
		case 'Z':
			return 2;
		case '0':
		case '1':
			return ch - '0';
	}

	return -1;
} 

/*  Convert xz string to data 
 *  Assuming: hex --- starting from 0x / 0X
              binary --- starting from 0b / 0B
 */
void xz_to_data(int which, char *s_xz, unsigned int *pdata)
{
	unsigned int data = 0, xz = 0, i;
	int ret;
	char *start;

	start = s_xz;
	while (start[0]==' ' || start[0] == '\t') start++; 
	
	if ((start[0] == '0') && ((start[1] == 'x') || (start[1] == 'X'))) {
		for (i=0; i<8; i++) { /* convert to hex */
			ret = convert_char_to_hex(start[i+2]);
			if (ret < 0) break;
			data = (data << 4) | (ret & 0xf);
			xz = (xz << 4) | ( (ret >> 4) & 0xf);
		}
	} else {
		if ((start[0] == '0') && ((start[1] == 'b') || (start[1] == 'B'))) {
			for (i=0; i<32; i++) { /* convert to hex */
				ret = convert_char_to_bin(start[i+2]);
				if (ret < 0) break;
				data = (data << 1) | (ret & 0x1);
				xz = (xz << 1) | ( (ret >> 1) & 0x1);
			}
		} else data = strtoul(start, NULL, 10);
	}

	DATA_PART(which, pdata) = data;
	XZ_PART(which, pdata) = xz;

	return;
}

/*  Convert data to xz binary string
 */
char * data_to_xz(int which, unsigned int *pdata, char *s_xz)
{
	unsigned int data, xz;
	int i, j;

	data = DATA_PART(which, pdata);
	xz = XZ_PART(which, pdata);

	for (i=31, j=0; i>=0; i--, j++) {
		if ((xz >> i) & 1) 
			s_xz[j] = ((data >> i) & 1) ? 'x' : 'z';
		else
			s_xz[j] = ((data >> i) & 1) + '0';
	}

	s_xz[32] = '\0';
	return s_xz;
}

int displayMsg(char *msg)
{
	IpcPkt  req, rsp;
	int len = strlen(msg);

	memset(&req, 0, sizeof(IpcPkt));
	memset(&rsp, 0, sizeof(IpcPkt));

	if (len >= (sizeof(IpcPkt)-17)) {
		len = (sizeof(IpcPkt)-17);
		fprintf(stderr, "\nWarning: msg truncated to %d Bytes \n", len);
	}
	
	memcpy((char*)(req.data), msg, len);
	if (len < 62) /* XXX verilog format */
		memset(((char*)(req.data))+len, ' ', 62-len);
	req.length = sizeof(IpcPkt);
	req.code   = BD_REQ_DISPLAY_MSG;

	return sim_request(&req, &rsp, 0);	
}

extern int keep_socket_alive;
int do_keep_alive_socket(int open, int warning)
{
	IpcPkt   req, rsp;

	if ((keep_socket_alive !=0 ) && open) {
		if (warning)
			fprintf(stderr, "Warning: already set to alive\n");
		return keep_socket_alive;
	}

	if ((keep_socket_alive == 0 ) && !open) {
		if (warning)
			fprintf(stderr, "Warning: already set to no alive \n");
		return 0;
	}
	
	memset(&req, 0, sizeof(IpcPkt));
	memset(&rsp, 0, sizeof(IpcPkt));
	req.length = sizeof(IpcPkt);
	req.code   = BD_REQ_PERSIST_SOCKET;
	req.data[0] = open ? 1 : 0;
	
	return sim_request(&req, &rsp, open ? SOCKET_ALIVE : SOCKET_CLOSE);
}
