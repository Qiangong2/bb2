/* BB2 gi utility headfile */

#ifndef _BB2_GI_UTILITY
#define _BB2_GI_UTILITY

void t2risc_pat_seed(unsigned int x);
unsigned int rand_data();
void create_random_data(unsigned int *pdata, int num);
unsigned int get_random_addr(unsigned int start,
	unsigned int range, unsigned int hold);
int read_compare(unsigned int addr, unsigned int exp, unsigned int mask);
int ipc_get_compare(	          /* return -1, if failed */
	unsigned int req_code,    /* register address */
	unsigned int param0,      /* parameter of ipc get*/
	unsigned int exp,         /* register expect value */
	unsigned int mask);        /* mask bits */
int bd_write_to_mem(unsigned int offset, unsigned int *pdata, int num);
int write_to_mem(unsigned int offset, unsigned int *pdata, int num);
int bd_check_mem_data(unsigned int offset, unsigned int *pdata, int num);

int bd_fill_pattern(unsigned int addr, int size, unsigned int pat_seed, int skip);
int bd_check_pattern(unsigned int addr, int size, unsigned int pat_seed, int skip);

   /* enter and leave secure mode */
int enter_sec_mode(int after_reset);
int leave_sec_mode();

   /* General register test */
int reg_rw_test(unsigned int reg, unsigned int wen_mask,
        unsigned int rw_mask, int verbose);
int reg_ro_test(unsigned int reg, unsigned int wen_mask,
        unsigned int rw_mask, int verbose);

#endif
