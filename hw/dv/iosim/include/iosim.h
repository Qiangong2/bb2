//
//   Port from BB project
// :set tabstop=4

#ifndef __IOSIM_H
#define __IOSIM_H

#define SOCKET_ALIVE              1
#define SOCKET_CLOSE              2
   
   /* regular request */
#define REQ_SINGLE_READ           1
#define REQ_SINGLE_WRITE          2
#define REQ_BLOCK_READ            3
#define REQ_BLOCK_WRITE           4
#define REQ_STALL                 5
#define REQ_QUIT                  100
#define BD_PAT_FILL               0x80000300
#define BD_PAT_CHECK              0x80000301


   /* Ali */	
#define REQ_RESET_CHIP            0x1000
#define REQ_INIT                  0x1001
#define REQ_POST_INIT             0x1002
#define REQ_PCI_PRE_FETCH         0x1003
#define REQ_SET_POS_WR_TIMER      0x1004
#define REQ_STRAP_PIN_INFO        0x1005
#define REQ_SDRAM_INIT            0x1006
#define REQ_SDRAM_SET_ARBT        0x1007
#define REQ_ROM_INIT              0x1008
#define REQ_FLASH_TEST_LD_DATA    0x1009

#define REQ_GET_cpu_vec_02_end    0x40000002

   /* Backdoor request */
#define BD_REQ_HANDSHAKE             0x80000000
#define BD_REQ_SINGLE_READ        0x80000001
#define BD_REQ_SINGLE_WRITE       0x80000002
#define BD_REQ_BLOCK_READ         0x80000003
#define BD_REQ_BLOCK_WRITE        0x80000004
#define BD_REQ_LOG                0x80000007
#define BD_REQ_PERSIST_SOCKET     0x80000100
#define BD_REQ_DISPLAY_MSG        0x80000101      
#define BD_REQ_DUMP               0x80000200
#define BD_REQ_RTL_TIME           0x80000400      

   /* Backdoor request for GI peripheral modules */
#define BD_GI_SECURE_BIT          0x80001000

// Backdoor DI tasks
#define BD_DI_RESET               0x80002000
#define BD_DI_XFER                0x80002001
#define BD_DI_GET                 0x80002002
#define BD_DI_PUT                 0x80002003
#define BD_DI_BREAK_AT            0x80002004
#define BD_DI_BREAK_NOW           0x80002005

#define BD_AIS_RESET              0x80003000
#define BD_AIS_SET                0x80003001
#define BD_AIS_GET                0x80003002
#define BD_AIS_PUT                0x80003003
#define BD_AIS_START              0x80003004
#define BD_AIS_SET_PERIOD         0x80003005
#define BD_AIS_GET_PERIOD         0x80003006
#define BD_AIS_GET_POS            0x80003007
#define BD_AIS_CHECK              0x80003008

#define BD_SYS_DMA_SETUP          0x80004000
#define BD_SYS_DMA_CMD_MON        0x80004001
#define BD_BI_SIZE                0x80004002

#define BD_SET_ALT_BOOT           0x80000A00
#define BD_GET_ALT_BOOT           0x80000A01
#define BD_SET_TEST_IN            0x80000B00
#define BD_GET_TEST_IN            0x80000B01

#define BD_REQ_JTAG_WRITE         0x80010000
#define BD_REQ_JTAG_READ          0x80010001

   /* Reply Code */
#define RSP_OK                    1
#define RSP_DATA                  2
#define RSP_ERROR                 100

#define REQ_SZ_BYTE               1
#define REQ_SZ_HALF               2
#define REQ_SZ_3BYTE              3
#define REQ_SZ_WORD               4
#define REQ_SZ_DWORD              8

#define PKT_DATA_WORD   8

typedef struct _IpcPktStruct {
        unsigned int    length;
        unsigned int    code;
        unsigned int    size;
        unsigned int    address;
        unsigned int    data[PKT_DATA_WORD<<1];    
} IpcPkt;

extern int IpcInit();
extern int  IpcOpen();
extern int  IpcReceive(int fd, char *buf, int nbytes);
extern int  IpcSend(int fd, char *buf, int nbytes);
extern int  IpcConnect(int fd);
extern void  IpcClose(int fd);

#endif
