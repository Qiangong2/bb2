#ifndef __IOSIM_UTILS
#define __IOSIM_UTILS

#ifdef T2RISC

#define UCACHE_SEG       0xA0000000
#define CACHE_SEG        0x80000000

#ifdef BB2_FPGA
#define IO_WRITE(addr,x) (*(volatile unsigned int *) (addr) = (x))
#define IO_READ(addr) (*(volatile unsigned int *) (addr))
#else
#define IO_WRITE(addr,x) (*(volatile unsigned int *) ((addr) | UCACHE_SEG) = (x))
#define IO_READ(addr) (*(volatile unsigned int *) ((addr) | UCACHE_SEG))
#endif

#define IO_CACHE_WRITE(addr,x) (*(volatile unsigned int *) ((addr) | CACHE_SEG) = (x))
#define IO_CACHE_READ(addr) (*(volatile unsigned int *) ((addr) | CACHE_SEG))

#else
#define IO_WRITE(addr, x)         sim_write(addr, REQ_SZ_WORD, x)
#define IO_WRITE_HFWD(addr, x)    sim_write(addr, REQ_SZ_HALF, x)
#define IO_WRITE_BYTE(addr, x)    sim_write(addr, REQ_SZ_BYTE, x)

#define BD_IO_WRITE(addr, x)      bd_sim_write(addr, REQ_SZ_WORD, x)
#define BD_IO_WRITE_HFWD(addr, x) bd_sim_write(addr, REQ_SZ_HALF, x)
#define BD_IO_WRITE_BYTE(addr, x) bd_sim_write(addr, REQ_SZ_BYTE, x)

#define IO_READ(addr)             sim_read(addr, REQ_SZ_WORD)
#define IO_READ_HFWD(addr)        sim_read(addr, REQ_SZ_HALF)
#define IO_READ_BYTE(addr)        sim_read(addr, REQ_SZ_BYTE)

#define BD_IO_READ(addr)          bd_sim_read(addr, REQ_SZ_WORD)
#define BD_IO_READ_HFWD(addr)     bd_sim_read(addr, REQ_SZ_HALF)
#define BD_IO_READ_BYTE(addr)     bd_sim_read(addr, REQ_SZ_BYTE)

#define IO_STALL(n)               ipc_set_data(REQ_STALL, n)
#define IO_QUIT()                 ipc_set_data(REQ_QUIT, 0)                
#define IO_HANDSHAKE()            ipc_set_data(BD_REQ_HANDSHAKE, 0)
#define IO_IPC_MON(on)            ipc_set_data(BD_REQ_LOG, on)
#define IO_DUMP(on)               ipc_set_data(BD_REQ_DUMP, on)

#define DATA_PART(which, pdata)   (pdata[which])
#define XZ_PART(which, pdata)     (pdata[which + PKT_DATA_WORD])
#define SET_VDATA(which, data, xz, pdata)  \
		DATA_PART(which, pdata) = data;  \
		XZ_PART(whihc, pdata) = xz;
		 
int  sim_request(IpcPkt *req, IpcPkt *rsp, int change_persist_socket);
int  FindServer(void);

  /* Simple IPC request */
void ipc_set_data(unsigned int req_code, unsigned int data);
void ipc_set_multi_data(unsigned int req_code, unsigned int *data, int n);
unsigned int ipc_get_data(unsigned int req_code, int param0);
unsigned int ipc_get_multi_param(unsigned int req_code, unsigned int *param, int num);

  /* single rw function */
void sim_write(unsigned int addr, int size, unsigned int data);
void bd_sim_write(unsigned int addr, int size, unsigned int data);
unsigned int sim_read(unsigned int addr, int size);
unsigned int bd_sim_read(unsigned int addr, int size);

void sim_write_v(unsigned int addr, int size, char *xz);
void bd_sim_write_v(unsigned int addr, int size, char *xz);
void sim_read_v(unsigned int addr, int size, char *xz);
void bd_sim_read_v(unsigned int addr, int size, char *xz);

  /* block rw functions */
void sim_bwrite(unsigned int addr, int size, unsigned int *pdata);
void sim_bread(unsigned int addr,  int size, unsigned int *pdata);
void bd_sim_bwrite(unsigned int addr, int size, unsigned int *pdata);
void bd_sim_bread(unsigned int addr,  int size, unsigned int *pdata);

void xz_to_data(int which, char *xz, unsigned int *pdata);
char * data_to_xz(int which, unsigned int *pdata, char *xz);

  /* socket persistence connection */
int do_keep_alive_socket(int open, int warning);
  /* display message through verilog simulator */
int displayMsg(char *msg);
long long rtl_time();

extern unsigned int interrupt_line;
#define BD_IO_INTERRUPT             (interrupt_line)
#define GI_SYS_INTERRUPT_MASK       0x80000000
#endif

#endif

