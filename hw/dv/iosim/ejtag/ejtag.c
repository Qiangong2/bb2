
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    File ejtag.c

    ejtag code for ALI DV
    
    Reference :
       (1) MIPS ejtag solution (PDF) from ALI
       (2) BB jtag  
       (3) BB2 Linux driver (BB debug board + ALI system)

 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */ 

#include <stdio.h>
#include <stdlib.h>
#include "iosim.h"
#include "utils.h"
#include "ejtag.h"

int bd_ejtag_write(int x)
{
	ipc_set_data(BD_REQ_JTAG_WRITE, x);

	return 0;
}

int bd_ejtag_read()
{
	return ipc_get_data(BD_REQ_JTAG_READ, 0);
}

int ejtag_tapmove(int tms, int tdi)
{
	int d;

	d = EJTAG_TRSTN_BIT(1) | EJTAG_TMS_BIT(tms) | EJTAG_TDO_BIT(tdi) | EJTAG_TCK_BIT(0);
	EJTAG_WRITE(d);
	d = EJTAG_TRSTN_BIT(1) | EJTAG_TMS_BIT(tms) | EJTAG_TDO_BIT(tdi) | EJTAG_TCK_BIT(1);
	EJTAG_WRITE(d);

	d = EJTAG_READ();
	return d ? 1 : 0;
}

int ejtag_tapreset()
{
	int d, i;
	
	for (i=0;i<5;i++) {
		d =  EJTAG_TRSTN_BIT(1) | EJTAG_TMS_BIT(1) | EJTAG_TDI_BIT(0) | EJTAG_TCK_BIT(0);
		EJTAG_WRITE(d);
		d =  EJTAG_TRSTN_BIT(1) | EJTAG_TMS_BIT(1) | EJTAG_TDI_BIT(0) | EJTAG_TCK_BIT(1);
		EJTAG_WRITE(d);
	}
	return 0;
}

/* scan data into the tap controller's data register */
int ejtag_data(unsigned int x)
{
	unsigned int i, dout, bit;

	ejtag_tapmove(0,0);             /* runtestidle             */
	ejtag_tapmove(0,0);             /* runtestidle             */
	ejtag_tapmove(0,0);             /* runtestidle             */
	ejtag_tapmove(1,0);             /* enter select-dr-scan    */
	ejtag_tapmove(0,0);             /* enter capture-dir       */	
	ejtag_tapmove(0,0);             /* enter shift-dr          */

	for (i=0, dout=0; i<31; i++) {
		bit = ejtag_tapmove(0, x & 0x1);  /* enter shift-dr */
		x >>= 1;
		dout |= bit ? (1<<i) : 0;
	}
	bit=ejtag_tapmove(1, x & 0x1);
	dout |= bit ? (1<<i) : 0;       /*  enter shift-dr */

	ejtag_tapmove(1,0);             /* enter update-dr         */
	ejtag_tapmove(0,0);             /* enter runtest-idle      */
	ejtag_tapmove(0,0);             /* runtestidle             */
	ejtag_tapmove(0,0);             /* runtestidle             */

	return dout;
}


int ejtag_instruction(int x)
{
	unsigned dout, bit;

	ejtag_tapmove(0,0);             /* runtestidle             */
	ejtag_tapmove(0,0);             /* runtestidle             */
	ejtag_tapmove(0,0);             /* runtestidle             */
	ejtag_tapmove(1,0);             /* enter select-dr-scan    */
	ejtag_tapmove(1,0);             /* enter select-ir-scan    */
	ejtag_tapmove(0,0);             /* enter capture-ir        */
	ejtag_tapmove(0,0);             /* enter shift-ir (dummy)  */

	dout=0;
	bit=ejtag_tapmove(0,x&0x1);   /* enter shift-ir */
	x >>= 1;
	dout |= dout|(bit ? 1 : 0);
	
	bit=ejtag_tapmove(0,x&0x1);   /* enter shift-ir */
	x >>= 1;
	dout |= dout|(bit ? 1 << 1: 0);

	bit=ejtag_tapmove(0,x&0x1);   /* enter shift-ir */
	x >>= 1;
	dout |= dout|(bit ? 1 << 2: 0);

	bit=ejtag_tapmove(0,x&0x1);   /* enter shift-ir */
	x >>= 1;
	dout |= dout|(bit ? 1 << 3: 0);

	bit=ejtag_tapmove(0,x&0x1);   /* enter shift-ir */
	x >>= 1;
	dout |= dout|(bit ? 1 << 4: 0);

	ejtag_tapmove(1,0);             /* enter update-ir */
	ejtag_tapmove(0,0);             /* enter runtest-idle */
	ejtag_tapmove(0,0);             /* runtestidle             */
	ejtag_tapmove(0,0);             /* runtestidle             */

	return dout ;
}

int ejtag_version()
{
	ejtag_instruction(EJTAG_IDCODE);
	
	return ejtag_data(0);
}

int ejtag_implementation()
{
	ejtag_instruction(EJTAG_IMPCODE);

	return ejtag_data(0);
}

