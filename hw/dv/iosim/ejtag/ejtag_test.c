/* ejtag test
    
   :set tabstop=4
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <unistd.h>
#include <netinet/in.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <getopt.h>
#include "iosim.h"
#include "utils.h"

int main(int argc, char **argv)
{
	int quit = 0, c;
	extern char *optarg;
	extern int optind, opterr, optopt;

	while ((c = getopt(argc, argv, "q")) != EOF) {
		switch (c) {
			case 'q':
				quit = 1;
				break;
			case 'h':
			case '?': 
			default:
				/* Your help function */
				break;
		}
	}

	if (IpcInit()) 	return -1;		

	  /*  Make persistence connection */
	do_keep_alive_socket(1, 1);

	ejtag_tapreset();


	  /* Close persistence connection */
	do_keep_alive_socket(0, 1);
	if (quit) IO_QUIT();

	return 0;
}
