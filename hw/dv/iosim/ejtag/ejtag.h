/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    File ejtag.h

    ejtag header files for ALI DV
    
    Reference :
       (1) MIPS ejtag solution (PDF) from ALI
       (2) BB jtag  
       (3) BB2 Linux driver (BB debug board + ALI system)

 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */ 

#ifndef _EJTAG_LIB_HEADER
#define _EJTAG_LIB_HEADER

   /* ---- Target JTAG INSTRUCTION IEEE 1149.1 ---- */
#define EJTAG_EXTEST              0x00    /* TAP select boundaryscan register */
#define EJTAG_IDCODE              0x01    /* TAP select chip ID register */
#define EJTAG_SAMPLE_PRELOAD      0x02    /* TAP select boundaryscan register */ 
#define EJTAG_IMPCODE             0x03    /* TAP select chip implementation code */
#define EJTAG_HI_Z                0x05    /* TAP Tristate outputs disabled */
#define EJTAG_BYPASS              0x07    /* TAP Bypass mode (no registers in chain) */
#define EJTAG_ADDRESS_IR          0x08    /* JTAG address register (DMA) */
#define EJTAG_DATA_IR             0x09    /* JTAG data register (DMA) */ 
#define EJTAG_CONTROL_IR          0x0A    /* JTAG control register (DMA/DSU) */
#define EJTAG_JTAG_ALL_IR         0x0B    /* JTAG all IR regs. linked together */
#define EJTAG_PCTRACE             0x10    /* JTAG PC-trace enabled */
#define EJTAG_BYPASS2             0x1F    /* JTAG bypass mode */

  /* ---- EJTAG signals --- */
#define EJTAG_TRSTN_BIT_MASK      (1 << 0)
#define EJTAG_TDI_BIT_MASK        (1 << 1)
#define EJTAG_TMS_BIT_MASK        (1 << 2)
#define EJTAG_TCK_BIT_MASK        (1 << 3)
#define EJTAG_TDO_BIT_MASK        (1 << 4)

#define EJTAG_TRSTN_BIT(x)        ((x) ? EJTAG_TRSTN_BIT_MASK : 0)
#define EJTAG_TDI_BIT(x)          ((x) ? EJTAG_TDI_BIT_MASK : 0)
#define EJTAG_TMS_BIT(x)          ((x) ? EJTAG_TMS_BIT_MASK : 0)
#define EJTAG_TCK_BIT(x)          ((x) ? EJTAG_TCK_BIT_MASK : 0)
#define EJTAG_TDO_BIT(x)          ((x) ? EJTAG_TDO_BIT_MASK : 0)

#define EJTAG_WAIT()              IO_STALL((x) * 20)
#define EJTAG_WRITE(x)            bd_ejtag_write(x)
#define EJTAG_READ()              bd_ejtag_read()

int bd_ejtag_write(int x);
int bd_ejtag_read();

int ejtag_tapmove(int tms, int tdi);
int ejtag_tapreset();
int ejtag_data(unsigned int x);
int ejtag_instruction(int x);

int ejtag_version();
int ejtag_implementation();

#endif
