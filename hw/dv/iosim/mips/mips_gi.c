#include "t2os.h"
#include "gi.h"
#include "gi_utils.h"
#include "utils.h"
#include "gi_test.h"

#define ERR_ADDR  0x80000
#define FAIL(ret) IO_WRITE(ERR_ADDR, ret)
#define PASS(ret) IO_WRITE(ERR_ADDR, ret)

void cpu_test()
{
	int ret = 0;
	
	enter_sec_mode(0);
	ret |= bi_reg_test(0); 
	ret |= di_reg_test(0);
	// XXX Suspend MIPS when call invalidate cache
	ret |= gi_sram_tests();
	ret |= bi_dma_test();
	ret |= ais_reg_test(0);

	if (ret) FAIL(ret);
	else PASS(ret);

	for ( ; ; ) ;
}
