function [31:0]  sgl_mask;
	input [1:0] addr;
	input [1:0] size;
	reg [31:0] mask;
	begin
		case ({size,addr})
			4'b0000: mask = 32'hff000000;
			4'b0001: mask = 32'h00ff0000;
			4'b0010: mask = 32'h0000ff00;
			4'b0011: mask = 32'h000000ff;
			4'b0100: mask = 32'hffff0000;
			4'b0110: mask = 32'h0000ffff;
			4'b1000: mask = 32'hffffff00;
			4'b1001: mask = 32'h00ffffff;
			4'b1100: mask = 32'hffffffff;
			default: begin
				mask = 32'h00000000;
				$display("ERROR: %t: %M: illegal addr %b, size %b", 
					 $time, addr, size);
			end
		endcase
		sgl_mask = mask;
	end
endfunction

function [31:0] rtl_to_c;
	input [1:0] addr;
	input [1:0] size;
	input [31:0] data;
	reg [31:0] ret;
	begin
		case ({size,addr})
			4'b0000: ret = (data >> 24) & 32'hff;
			4'b0001: ret = (data >> 16) & 32'hff;
			4'b0010: ret = (data >> 8) & 32'hff;
			4'b0011: ret = data & 32'hff;
			4'b0100: ret = (data >> 16) & 32'hffff;
			4'b0110: ret = data & 32'hffff;
			4'b1000: ret = (data >> 8) & 32'hffffff;
			4'b1001: ret = data & 32'hffffff;
			4'b1100: ret = data;
			default: begin
				$display("ERROR: %t: %M: illegal addr %b, size %b",
					$time, addr, size);
				ret = 32'hxxxxxxxx;
			end
		endcase
		rtl_to_c = ret;	
	end
endfunction

function [31:0] c_to_rtl;
	input [1:0] addr;
	input [1:0] size;
	input [31:0] data;
	reg [31:0] ret;
	begin
		case ({size,addr})
			4'b0000: ret = (data & 32'hff) << 24;
			4'b0001: ret = (data & 32'hff) << 16;
			4'b0010: ret = (data & 32'hff) << 8;
			4'b0011: ret = data & 32'hff;
			4'b0100: ret = (data & 32'hffff) << 16;
			4'b0110: ret = data & 32'hffff;
			4'b1000: ret = (data & 32'hffffff) << 8;
			4'b1001: ret = data & 32'hffffff;
			4'b1100: ret = data;
			default: begin
				$display("ERROR: %t: %M: illegal addr %b, size %b",
					$time, addr, size);
				ret = 32'hxxxxxxxx;
			end
		endcase
		c_to_rtl = ret;	
	end
endfunction

task bd_sread;
	input [31:0] addr;
	input [1:0]  size;
	output [31:0] rdata;
	reg [31:0] mask;	
	reg [31:0] old_addr;
	reg sram;	

	begin
		old_addr = addr;
		if (addr[31:27] === 5'h0) begin /* memory */
`ifdef ALI_GI
			addr[1:0] = 0;
			addr = addr % top.mem_max_size;
			SDRAM_dump_array(addr, 1);
			rdata = top.mem_array[0];
`else
			addr[31:20] = 0;
			addr = addr >> 2;
			rdata = `mem[addr];
`endif
		end

		if (addr[31:20] === (`GI_BASE >> 20)) begin /* sram */
			sram = addr[17];
			addr = addr & 32'h1ffff;
			addr = addr >> 2;
			if (`gi_top.bi.sec_mode[1] ^ sram) begin
				$display("%t %M rom bd read @0x%x", $time, addr);
				rdata = `rom_get(addr);
			end else rdata = `sram_get(addr);
		end

		rdata = rtl_to_c(old_addr, size, rdata); 
	end
endtask

task bd_swrite;
	input [31:0] addr;
	input [1:0]  size;
	input [31:0] wdata;
	reg [31:0] mask;
	reg [31:0] rdata;	
	reg sram;

	begin
		wdata = c_to_rtl(addr, size, wdata);
		mask = sgl_mask(addr[1:0], size);
		if (addr[31:27] === 5'h0) begin /* memory */
`ifdef ALI_GI
			addr[1:0] = 0;
			addr = addr % top.mem_max_size;
			SDRAM_dump_array(addr, 1);
			top.mem_array[0] = (top.mem_array[0] & (~mask)) | (wdata & mask);
			SDRAM_load_memory(addr, 1);
`else
			addr[31:20] = 0;
			addr = addr >> 2;
			`mem[addr] = (`mem[addr] & (~mask)) | (wdata & mask);
`endif
		end

		if (addr[31:20] === (`GI_BASE >> 20)) begin /* sram */
			sram = addr[17];
			addr = addr & 32'h1FFFF;
			addr = addr >> 2;
			if (`gi_top.bi.sec_mode[1] ^ sram) begin
				$display("%t %M rom bd write @0x%x", $time, addr);
				rdata = `rom_get(addr);
				`rom_put(addr, (rdata & (~mask)) | (wdata & mask));
			end else begin
				rdata = `sram_get(addr);
				`sram_put(addr, (rdata & (~mask)) | (wdata & mask));
			end
		end
	end
endtask

task bd_bwrite;
	input [31:0] addr;
	input [3:0]  size;
	input [31:0] data0;
	input [31:0] data1;
	input [31:0] data2;
	input [31:0] data3;
	input [31:0] data4;
	input [31:0] data5;
	input [31:0] data6;
	input [31:0] data7;
	reg[31:0] data[0:7];
	integer i;

	begin
		data[0] = data0;
		data[1] = data1;
		data[2] = data2;
		data[3] = data3;
		data[4] = data4;
		data[5] = data5;
		data[6] = data6;
		data[7] = data7;

		if(addr[31:27] === 5'h0) begin /* Memory */
`ifdef ALI_GI
			addr[1:0] = 0;
			addr = addr % top.mem_max_size;
			for (i=0; i<size; i = i+1) begin
				top.mem_array[i] = data[i];
			end
			SDRAM_load_memory(addr, size);
`else
			addr[31:20] = 0;
			addr = addr >> 2;
			for (i=0; i<size; i = i+1) begin
				`mem[addr+i] = data[i];
			end
`endif
		end

		if (addr[31:20] === (`GI_BASE >> 20)) begin /* sram */
			addr = addr & 32'h1FFFF;
			addr = addr >> 2;
			for (i=0; i<size; i = i+1)
				`sram_put(addr+i, data[i]);
		end
	end
endtask
 

task bd_bread;
	input [31:0] addr;
	input [3:0]  size;
	output [31:0] data0;
	output [31:0] data1;
	output [31:0] data2;
	output [31:0] data3;
	output [31:0] data4;
	output [31:0] data5;
	output [31:0] data6;
	output [31:0] data7;
	reg[31:0] data[0:7];
	integer i;

	begin
		if(addr[31:27] === 5'h0) begin /* Memory */
`ifdef ALI_GI
			addr[1:0] = 0;
			addr = addr % top.mem_max_size;
			SDRAM_dump_array(addr, size);
			for (i=0; i<size; i = i+1)
				data[i] = top.mem_array[i];
`else
			addr[31:20] = 0;
			addr = addr >> 2;
			for (i=0; i<size; i = i+1)
				data[i] = `mem[addr+i];
`endif
		end

		if (addr[31:20] === (`GI_BASE >> 20)) begin /* sram */
			addr = addr & 32'h1ffff;
			addr = addr >> 2;
			for (i=0; i<size; i = i+1) 
				data[i] = `sram_get(addr+i);
		end

		data0 = data[0];
		data1 = data[1];
		data2 = data[2];
		data3 = data[3];
		data4 = data[4];
		data5 = data[5];
		data6 = data[6];
		data7 = data[7];
	end
endtask

task bd_fill_pat;
	input [31:0] addr;
	input [31:0] size;
	input [31:0] pat_seed;
	input [31:0] pat_skip;
	integer next;
	reg   bit;
	integer i;
	 
	begin
		$display("backdoor fill pat @ 0x%x size=%d seed=%d skip=%d", 
			addr, size, pat_seed, pat_skip); 
		bit = pat_seed[15] ^ pat_seed[0] ^ 1;
		next = {bit, pat_seed[31:1]};

		for (i=0; i<pat_skip; i=i+4) begin
			bit = next[15] ^ next[0] ^ 1;
			next = {bit, next[31:1]};
		end

		for (i=pat_skip; i<size; i=i+4) begin
			bit = next[15] ^ next[0] ^ 1;
			next = {bit, next[31:1]};
`ifdef ALI_GI
			if (addr[31:27] === 5'b0) 
				top.mem_array[i >> 2] = next;
			else 
`endif
				bd_swrite(addr+i, 3, next);

		end
`ifdef ALI_GI
		if (addr[31:27] === 5'b0) 
			SDRAM_load_memory(addr, size >> 2);
`endif
	end
endtask

task bd_check_pat;
	input [31:0] addr;
	input [31:0] size;
	input [31:0] pat_seed;
	input [31:0] pat_skip;
	output [31:0] ret;

	integer next;
	reg   bit;
	integer i;
	integer rb;
	
	begin
		$display("backdoor check pat @ 0x%x size=%d seed=%d skip=%d", 
			addr, size, pat_seed, pat_skip); 
		ret = 0;
		bit = pat_seed[15] ^ pat_seed[0] ^ 1;
		next = {bit, pat_seed[31:1]};

		for (i=0; i<pat_skip; i=i+4) begin
			bit = next[15] ^ next[0] ^ 1;
			next = {bit, next[31:1]};
		end

`ifdef ALI_GI
		if (addr[31:27] === 5'b0) 
			SDRAM_dump_array(addr, size >> 2);
`endif
		for (i=pat_skip; (i<size) && (ret == 0); i=i+4) begin
			bit = next[15] ^ next[0] ^ 1;
			next = {bit, next[31:1]};
`ifdef ALI_GI
			if (addr[31:27] === 5'b0)
				rb = top.mem_array[i>>2];
			else 
				bd_sread(addr+i, 3, rb);
`else
			bd_sread(addr+i, 3, rb);
`endif

			if (rb != next) ret = i + 1;
			if (rb != next)
				$display("Error @ 0x%x exp=0x%x get=0x%x", i, next, rb);
		end
		
		$display("%t backdoor check pattern ret=%d", $time, ret);
	end
endtask
