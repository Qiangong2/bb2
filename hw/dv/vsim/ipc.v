//
//   ipc.v   
//   Port from BB project   
// 
//   :set tabstop=4

`include "ipc.vh"

`timescale 1ns/1ps
`define NO_CPU
module ipc(sysclk, coldrst_l, warmrst_l);

input sysclk;
input coldrst_l;
input warmrst_l;

	wire reset_l;
	reg ready;

	assign reset_l = coldrst_l & warmrst_l;  // Whatever reset

	`include "backdoor.v"
	`include "gi_task.v"

	initial begin 
		ready = 0;
		while (ready == 0) begin
			if((reset_l === 0) | (reset_l === 1))
				ready = reset_l;
			
			@ (posedge sysclk);	
		end

		`include "ipc_module.v"
		$finish;
	end 
endmodule
