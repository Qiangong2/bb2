/*****************************************************************************
 * 
 *   (C) 2004 BroadOn INC..  ALL RIGHTS RESERVED.  UNPUBLISHED -- RIGHTS
 *   RESERVED UNDER THE COPYRIGHT LAWS OF THE UNITED STATES.  USE OF A
 *   COPYRIGHT NOTICE IS PRECAUTIONARY ONLY AND DOES NOT IMPLY PUBLICATION
 *   OR DISCLOSURE.
 *
 *   THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF
 *   BroadOn INC..  USE, DISCLOSURE, OR REPRODUCTION IS PROHIBITED WITHOUT
 *   THE PRIOR EXPRESS WRITTEN PERMISSION OF BroadOn INC..
 *
 *                   RESTRICTED RIGHTS LEGEND
 *
 *   Use, duplication, or disclosure by the Government is subject to
 *   restrictions as set forth in subparagraph (c)(1)(ii) of the Rights
 *   in Technical Data and Computer Software clause at DFARS 252.227-7013
 *   or subparagraphs (c)(1) and (2) of Commercial Computer Software --
 *   Restricted Rights at 48 CFR 52.227-19, as applicable. 
 *
 *   BroadOn Inc.
 *   3400 Hillview Ave, Bldg 5
 *   Palo Alto, CA 94304
 *
 ****************************************************************************/

`timescale 1ns/1ns

// syn myclk = io_clk

module di_flipper (/*AUTOARG*/

    // Output to External DVD drive
    didir, dihstrbb, dirstb,

    // General Input
    io_clk,

    // Input from External DVD drive
    didstrbb, dierrb, dicover,

    // Inout from/to External DVD drive
    di_dd, di_brk

);

    			// Output to external device

    output
        didir;                  // Data direction 1 for input, 0 for output

    output
        dirstb;                 // Reset, active low

    output
        dihstrbb;               // Host strobe, active low

    			// General inputs
    input
        io_clk;                 // Clock

    			// Input from external DVD driver
    input
        didstrbb;               // Device (DVD) strobe, active low

    input
        dierrb;                 // Error, active low

    input
        dicover;                // Cover, high for cover open

    			// Inout from/to External DVD drive
    inout [7:0]	di_dd;

    inout	di_brk;

    			// from DI to PI or interrupt controller
    wire [15:0]
	DiPiData;  		// Data bus to PI

    wire 
	di_piInt;   		// Interrupt to PI

    			// from DI to Mem
    wire  [63:0]
	DiMemData;		// Data to Mem

    wire  [25:5]
	DiMemAddr;		// Address to Mem

    wire 
	DiMemReq;		// Request for 32B transfer

    wire 
	DiMemRd;		// Request for data read, 0 for write

    wire 
	DiMemFlush;		// Request write flush at the end
				// of the memory transfer

    			// from DI to external device
    wire 
	didir;			// Data direction 1 for input, 0 for output

    wire 
    	dirstb;			// Reset, active low

    wire 
	dihstrbb;		// Host strobe, active low

    			// DI Output for reset configuration
    wire  [7:0]
	di_config;		// Configuration during reset

    			// General inputs
    wire
	io_clk;			// Clock

    wire
	resetb;			// Reset, low active

    			// to DI from PI or reset register
    wire [15:0]
	PiData;  		// Data bus from PI

    wire [19:1]
	PiAddr;  		// Address from PI, [19:1]
				// Only bits 5:2 are needed for 9 reg decoding

    wire
	PiLoad;			// Write from PI, 0 for read
				// Used for writing to EI, AI and SI also

    wire
	PiDiSel;		// Selecting DI from PI(not EXI, AI or SI)

    reg
   	pi_ioDiRstb;		// DI Reset from PI reset register, low active

    			// to DI from Mem
    wire [63:0]
	MemData;		// Data from Mem (used for DI and EX)

    wire
	DiMemAck;		// Ack from Mem

    wire
	MemFlushAck;		// Flush Ack from Mem (used for DI and EX)

    wire
	didstrbb;		// Device (DVD) strobe, active low

    wire
	dierrb;			// Error, active low

    wire
	dicover;		// Cover, high for cover open

    			// inout from/to External Drive

    wire [7:0]	didOut, didIn;
    
    wire	didOE;

    wire	dibrkOut, dibrkIn;

    wire	dibrkOE;

    	// tristate assigns

    assign	didIn	=	di_dd;
    assign	di_dd	=	didOE	?	didOut	:	8'bz;
    assign	dibrkIn	=	di_brk;
    assign	di_brk	=	dibrkOE	?	dibrkOut:	1'bz;

        integer tRST;                   // reset active time;
        initial
        begin
                tRST = 2000;
        end
	
	reg	reset;

        initial
                rst(1);

        // reset task;
        // guarantees minimum reset time;

        task rst;
                input [1:0] cmd;        // 01=assert, 10=deassert, 11=full sequence;
                begin
                        $display("%t: %M: cmd=%b", $time, cmd);
                        if(cmd[0]) begin
                                reset = 1'b0;
                        end
                        if(cmd[1]) begin
                                #(tRST);
                                reset = 1'b1;
                        end
                end
        endtask

	assign resetb = reset;

	reg send;
	reg recv;
	reg brk_busy;                   // busy with break protocol;
	reg [15:0] txsize, rxsize;
	reg write_cmd;
	reg wdma_done;

	initial
		send = 0;
	initial
		recv = 0;
	initial
		wdma_done = 0;

// for Flipper, xfer only calls xmt routine (command:response is atomic op in GC)

        task xfer;
                input rcv;                              // receive;
                input [15:0] tsize;                     // # of bytes to transmit;
                input [15:0] rsize;                     // # of bytes to receive;
		input wr_cmd;				// broadon write command extension;
                input rss;                              // randomize strobes/stalls;

		reg [15:0] tsize, rsize;
                begin
			txsize = tsize;
			rxsize = rsize;
			write_cmd = wr_cmd;
			wdma_done = 0;

			if ((send == 1) | (recv == 1)) begin
			$display("%t: %M: ERROR: new flipper command before finished: rcv=%b, wr_cmd=%b, txsize=%d, rxsize=%d, send=%b, recv=%b",
						$time, rcv, write_cmd, txsize, rxsize, send, recv);
			end

                        if(rcv == 0) begin
                                send = 1;
                        end else begin
                                recv = 1;
                        end
                end
        endtask

	task get;
		input [15:0] off;
		output [31:0] word;
		reg [63:0] data;
		begin
			data = di_mem.memarray[off>>3];
			if (off[2]) word = data[31:0];
			else word = data[63:32];
		end
	endtask

	task put;
		input [15:0] off;
		input [31:0] word;
		reg [63:0] data;
		begin
			data = di_mem.memarray[off>>3];
			if (off[2]) data[31:0] = word;
			else data[63:32] = word; 
			di_mem.memarray[off>>3] = data;
		end
	endtask

        // set break position;

        integer brk_pos;                                // break at this byte position;

        task break;
                input [31:0] pos;                       // byte position;
                begin
                        brk_pos = pos;
                        $display("%t: %M: pos %0d", $time, pos);
                end
        endtask

	task break_now;
		begin
		  if (recv)
			brk_pos = rcv.n + 1;
			$display("%t: %M: pos %0d, send=%b, recv=%b", $time, brk_pos, send, recv);
		  if (send)
                        brk_pos = snd.m + 1;
			$display("%t: %M: pos %0d, send=%b, recv=%b", $time, brk_pos, send, recv);
		end
	endtask

        reg     [24:0]  instruction;
	reg	[15:0]	delay, sustain;
        reg     [15:0]  srdelay, srsustain;
	reg		sreset_en;		// task call marker for waveform (di_reset lags by 1 clk)
	reg		softreset;
	

        initial
                pi_ioDiRstb	<= 1'b1;
	initial
		sreset_en	<= 1'b0;
	initial
		softreset	<= 1'b0;

	task sreset;				// assert soft reset
		input [15:0]	delay;		// delay before soft reset assert, in io_clk cycles
		input [15:0]	sustain;	// duration minus 1 of soft reset assert, in io_clk cycles
		begin
			srdelay		<=	delay;
			srsustain	<=	sustain;
			softreset 	<= 	1'b1;
		end
	endtask

	always @(posedge softreset) begin
		        	sreset_en       <=      1'b1;
                        repeat(srdelay) @(posedge io_clk);
                                pi_ioDiRstb     <=	1'b0;	// assert di_reset
                        repeat(srsustain) @(posedge io_clk);              
                                pi_ioDiRstb     <=      1'b1;   // deassert di_reset
                         @(posedge io_clk);
                                sreset_en       <=      1'b0;
		softreset <= 1'b0;
	end

	always @(posedge io_clk) begin
		if (!resetb) instruction	<=	25'h1000000;
	end

	wire [15:0] dma_origin;
       	assign dma_origin = txsize + 16'h8;

        always @(posedge send)
        begin : xmt

                integer i;
		reg [5:0] addr_left, addr_right;
		reg [31:0] rdata;
		reg	done;
		reg [31:0] imm_response;
		recv = 0;

          if (!write_cmd & (txsize == 16'hc) & (rxsize == 16'h4)) begin
		/*      immediate read	*/
// XXX must copy Immediate data registers to memory using put task after response completion
// XXX cannot rely on DISR transfer complete bit because it waits for GI DSTRB=0, and that
// XXX will only be attained with a GI timeout error.  so, using recv negedge = done instead.
		for(i = 0; i < 12; i = i + 4) begin
			get(i, rdata);

			addr_left 	= (i + 6'h8);
			addr_right	= (addr_left + 6'h2);

			@(posedge io_clk);
                        	instruction <= {1'b1, 1'b1, 1'b1, addr_left,  rdata[31:16]};
			@(posedge io_clk);
                        	instruction <= {1'b1, 1'b1, 1'b1, addr_right, rdata[15:0]};
		end

		@(posedge io_clk);
			instruction <= {1'b1, 1'b1, 1'b1, 6'h1e, 16'h1};
                @(posedge io_clk);
                        instruction <= {1'b1, 1'b0, 1'b0, 6'h0, 16'hdead};

		@(negedge recv);
                @(posedge io_clk);
                @(posedge io_clk);
                @(posedge io_clk);
                @(posedge io_clk);
			//read Immediate data, put into memory for check_reply
		@(posedge io_clk);
                        instruction <= {1'b1, 1'b1, 1'b0, 6'h20, 16'hx};
                @(posedge io_clk);
                        instruction <= {1'b1, 1'b1, 1'b0, 6'h22, 16'hx};
                @(posedge io_clk);
                        instruction <= {1'b1, 1'b0, 1'b0, 6'h0, 16'hdead};
			imm_response[31:16] <= DiPiData;
		@(posedge io_clk);
			imm_response[15:0] <= DiPiData;
		@(posedge io_clk);
			put(0, imm_response);
	  end
          else if (write_cmd & (txsize == 16'h10) & (rxsize == 16'h0)) begin
		/*      immediate write	*/

                for(i = 0; i < 12; i = i + 4) begin
                        get(i, rdata);

                        addr_left       = (i + 6'h8);
                        addr_right      = (addr_left + 6'h2);

                        @(posedge io_clk);
                                instruction <= {1'b1, 1'b1, 1'b1, addr_left,  rdata[31:16]};
                        @(posedge io_clk);
                                instruction <= {1'b1, 1'b1, 1'b1, addr_right, rdata[15:0]};
                end
			get(12, rdata);

                @(posedge io_clk);
                        instruction <= {1'b1, 1'b1, 1'b1, 6'h20, rdata[31:16]};
                @(posedge io_clk);
                        instruction <= {1'b1, 1'b1, 1'b1, 6'h22, rdata[15:0]};

		@(posedge io_clk);
                        instruction <= {1'b1, 1'b1, 1'b1, 6'h1e, 16'h5};
	  end
          else if (write_cmd & (txsize[4:0] == 5'hc) & (|txsize[15:5]) & (rxsize == 16'h0)) begin
		/*      dma write	*/

                for(i = 0; i < 12; i = i + 4) begin
                        get(i, rdata);

                        addr_left       = (i + 6'h8);
                        addr_right      = (addr_left + 6'h2);

                        @(posedge io_clk);
                                instruction <= {1'b1, 1'b1, 1'b1, addr_left,  rdata[31:16]};
                        @(posedge io_clk);
                                instruction <= {1'b1, 1'b1, 1'b1, addr_right, rdata[15:0]};
                end

                for(i = txsize; i > 12; i = i - 1) begin   //copy wdata to aligned 32byte
                        get(i-1, rdata);
                        put(i+txsize+7, rdata);
                end

                @(posedge io_clk);
                        instruction <= {1'b1, 1'b1, 1'b1, 6'h14, 16'h0};
                @(posedge io_clk);				//wdata addr%32=0
                        instruction <= {1'b1, 1'b1, 1'b1, 6'h16, ((txsize + 16'h1f) & 16'hffe0)};
                @(posedge io_clk);
                        instruction <= {1'b1, 1'b1, 1'b1, 6'h18, 16'h0};
                @(posedge io_clk);
                        instruction <= {1'b1, 1'b1, 1'b1, 6'h1a, txsize};
                @(posedge io_clk);
                        instruction <= {1'b1, 1'b1, 1'b1, 6'h1e, 16'h7};
	  end
	  else if (!write_cmd & (txsize == 16'hc) & (|rxsize[15:5]) & (rxsize[4:0] == 5'h0)) begin
		/*	dma read 	*/

                for(i = 0; i < 12; i = i + 4) begin
                        get(i, rdata);

                        addr_left       = (i + 6'h8);
                        addr_right      = (addr_left + 6'h2);

                        @(posedge io_clk);
                                instruction <= {1'b1, 1'b1, 1'b1, addr_left,  rdata[31:16]};
                        @(posedge io_clk);
                                instruction <= {1'b1, 1'b1, 1'b1, addr_right, rdata[15:0]};
                end

                @(posedge io_clk);
                        instruction <= {1'b1, 1'b1, 1'b1, 6'h14, 16'h0};
                @(posedge io_clk);
                        instruction <= {1'b1, 1'b1, 1'b1, 6'h16, 16'h0};
                @(posedge io_clk);
                        instruction <= {1'b1, 1'b1, 1'b1, 6'h18, 16'h0};
		@(posedge io_clk);
                        instruction <= {1'b1, 1'b1, 1'b1, 6'h1a, rxsize};
                @(posedge io_clk);
                        instruction <= {1'b1, 1'b1, 1'b1, 6'h1e, 16'h3};
          end
	  else if (write_cmd & (rxsize == 16'h4) & (|txsize[15:5]) & (txsize[4:0] == 5'h18)) begin
                /*      write dma followed by immediate read	*/

                for(i = 0; i < 12; i = i + 4) begin
                        get(i, rdata);

                        addr_left       = (i + 6'h8);
                        addr_right      = (addr_left + 6'h2);

                        @(posedge io_clk);
                                instruction <= {1'b1, 1'b1, 1'b1, addr_left,  rdata[31:16]};
                        @(posedge io_clk);
                                instruction <= {1'b1, 1'b1, 1'b1, addr_right, rdata[15:0]};
                end

		for(i = txsize - 12; i > 12; i = i - 1) begin	//copy wdata to aligned 32byte
			get(i-1, rdata);
			put(i+txsize-5, rdata);
		end

                @(posedge io_clk);
                        instruction <= {1'b1, 1'b1, 1'b1, 6'h14, 16'h0};
			done <= 1'b0;
                @(posedge io_clk);				//wdata addr%32=0
                        instruction <= {1'b1, 1'b1, 1'b1, 6'h16, ((txsize + 16'h1f) & 16'hffe0)};
                @(posedge io_clk);
                        instruction <= {1'b1, 1'b1, 1'b1, 6'h18, 16'h0};
                @(posedge io_clk);
                        instruction <= {1'b1, 1'b1, 1'b1, 6'h1a, txsize};
                @(posedge io_clk);
                        instruction <= {1'b1, 1'b1, 1'b1, 6'h1e, 16'h7};
                @(posedge io_clk);
                        instruction <= {1'b1, 1'b0, 1'b0, 6'h0, 16'hdead};

		@(posedge wdma_done or negedge send);	//wait until last 12 bytes to start immediate read cmd
							//or exit if send reset (e.g. break, error assertion)
	     if (send) begin
                for(i = 0; i < 12; i = i + 4) begin

		get(i+txsize-12, rdata);

                        addr_left       = (i + 6'h8);
                        addr_right      = (addr_left + 6'h2);

                        @(posedge io_clk);
                                instruction <= {1'b1, 1'b1, 1'b1, addr_left,  rdata[31:16]};
                        @(posedge io_clk);
                                instruction <= {1'b1, 1'b1, 1'b1, addr_right, rdata[15:0]};
                end

                @(posedge io_clk);
                        instruction <= {1'b1, 1'b1, 1'b1, 6'h1e, 16'h1};

                @(posedge io_clk);
                        instruction <= {1'b1, 1'b0, 1'b0, 6'h0, 16'hdead};

                @(negedge recv);
                @(posedge io_clk);
                @(posedge io_clk);
                @(posedge io_clk);
                @(posedge io_clk);
                        //read Immediate data, put into memory for check_reply
                @(posedge io_clk);
                        instruction <= {1'b1, 1'b1, 1'b0, 6'h20, 16'hx};
                @(posedge io_clk);
                        instruction <= {1'b1, 1'b1, 1'b0, 6'h22, 16'hx};
                @(posedge io_clk);
                        instruction <= {1'b1, 1'b0, 1'b0, 6'h0, 16'hdead};
                        imm_response[31:16] <= DiPiData;
                @(posedge io_clk);
                        imm_response[15:0] <= DiPiData;
                @(posedge io_clk);
                        put(0, imm_response);
	     end
          end
          else if (!write_cmd & (txsize == 16'hc) & (rxsize == 16'h0)) begin
                /*      zero length dma read        */

                for(i = 0; i < 12; i = i + 4) begin
                        get(i, rdata);

                        addr_left       = (i + 6'h8);
                        addr_right      = (addr_left + 6'h2);

                        @(posedge io_clk);
                                instruction <= {1'b1, 1'b1, 1'b1, addr_left,  rdata[31:16]};
                        @(posedge io_clk);
                                instruction <= {1'b1, 1'b1, 1'b1, addr_right, rdata[15:0]};
                end

                @(posedge io_clk);      
                        instruction <= {1'b1, 1'b1, 1'b1, 6'h14, 16'h0};
                @(posedge io_clk);
                        instruction <= {1'b1, 1'b1, 1'b1, 6'h16, 16'h0};
                @(posedge io_clk);
                        instruction <= {1'b1, 1'b1, 1'b1, 6'h18, 16'h0};
                @(posedge io_clk);
                        instruction <= {1'b1, 1'b1, 1'b1, 6'h1a, rxsize};
                @(posedge io_clk);
                        instruction <= {1'b1, 1'b1, 1'b1, 6'h1e, 16'h3};
          end
          else if (write_cmd & (txsize == 5'hc) & (rxsize == 16'h0)) begin
                /*      zero length dma write       */

                for(i = 0; i < 12; i = i + 4) begin
                        get(i, rdata);

                        addr_left       = (i + 6'h8);
                        addr_right      = (addr_left + 6'h2);

                        @(posedge io_clk);
                                instruction <= {1'b1, 1'b1, 1'b1, addr_left,  rdata[31:16]};
                        @(posedge io_clk);
                                instruction <= {1'b1, 1'b1, 1'b1, addr_right, rdata[15:0]};
                end

                @(posedge io_clk);      
                        instruction <= {1'b1, 1'b1, 1'b1, 6'h14, 16'h0};
                @(posedge io_clk);
                        instruction <= {1'b1, 1'b1, 1'b1, 6'h16, 16'h0};
                @(posedge io_clk);
                        instruction <= {1'b1, 1'b1, 1'b1, 6'h18, 16'h0};
                @(posedge io_clk);
                        instruction <= {1'b1, 1'b1, 1'b1, 6'h1a, txsize};
                @(posedge io_clk);
                        instruction <= {1'b1, 1'b1, 1'b1, 6'h1e, 16'h7};
          end
	  else begin
		$display("%t: %M: ERROR: illegal flipper command : wr_cmd=%b, txsize=%d, rxsize=%d", $time, write_cmd, txsize, rxsize); 
	  end

                @(posedge io_clk);
                        instruction <= {1'b1, 1'b0, 1'b0, 6'h00, 16'hdead};	//idle when done

        end

        assign  PiDiSel         =       instruction[23];
        assign  PiLoad          =       instruction[22];
        assign  PiAddr          =       {14'b0, instruction[21:17]};
        assign  PiData          =       instruction[15:0];

        // break asserts during receive data packet; XXXX HW not necessarily during rcv pkt XXXX
	// exit/reenter with next posedge recv after break or error case

        always @(posedge recv)
        begin : rcv
                integer n;

                for(n = 0; n < rxsize; n = n + 1) begin
                        @(posedge didstrbb or negedge dierrb or posedge di_brk);
				if((recv == 0) | (dierrb == 0) | (di_brk == 1)) begin
                                        n = rxsize;
				end
                                if(brk_pos == n) begin
                                        brk_pos = -1;                   // single issue;
                                        brk_busy <= 1;                  // break here;
                                end
                end
                recv = 0;
        end

	// break assert during transmit packet (e.g. test write dma with break)

        always @(posedge send)
        begin : snd
                integer m;

                for(m = 0; m < txsize; m = m + 1) begin
                        @(posedge dihstrbb or negedge dierrb or posedge di_brk);
                                if((send == 0) | (dierrb == 0) | (di_brk == 1)) begin
                                        m = txsize;
                                end
                                if(brk_pos == m) begin
                                        brk_pos = -1;                   // single issue;
                                        brk_busy <= 1;                  // break here;
                                end
				if(txsize - m == 13) begin		// dma wr + imm rd separation
					wdma_done <= 1;
				end
                end
                send = 0;
        end

        // issue break to Flipper via PI interface; CAREFUL:  *don't overlap with PI command setup*;

	always @(posedge brk_busy) begin

               	@(posedge io_clk);
                       	instruction <= {1'b1, 1'b1, 1'b1, 6'h2, 16'h1};
		@(posedge io_clk);
                	brk_busy <= 0; 
                @(posedge io_clk);
                	instruction <= {1'b1, 1'b1, 1'b1, 6'h2, 16'h0};
		@(posedge io_clk);
                        instruction <= {1'b1, 1'b0, 1'b0, 6'h0, 16'h0};
        end

   /*
    * Instantiations
    */

	di_mem	di_mem	(
		.io_clk(io_clk),
		.resetb(resetb),

		.DiMemData(DiMemData),
		.DiMemAddr(DiMemAddr),
		.DiMemReq(DiMemReq),
		.DiMemRd(DiMemRd),
		.DiMemFlush(DiMemFlush),

                .MemData(MemData),
                .DiMemAck(DiMemAck),
                .MemFlushAck(MemFlushAck)
		);

	io_di	io_di	(
		.io_clk(io_clk),
		.resetb(resetb),

		.PiData(PiData),
		.PiAddr(PiAddr),
		.PiLoad(PiLoad),
		.PiDiSel(PiDiSel),
		.pi_ioDiRstb(pi_ioDiRstb),
		.DiPiData(DiPiData),	
		.di_piInt(di_piInt),

                .didir(didir),
                .didIn(didIn),
                .didOut(didOut),
                .didOE(didOE),
                .dihstrbb(dihstrbb),
                .didstrbb(didstrbb),
                .dierrb(dierrb),
                .dibrkIn(dibrkIn),
                .dibrkOut(dibrkOut),
                .dibrkOE(dibrkOE),
                .dirstb(dirstb),
                .dicover(dicover),


                .MemData(MemData),
                .DiMemAck(DiMemAck),
                .MemFlushAck(MemFlushAck),

                .DiMemData(DiMemData),
                .DiMemAddr(DiMemAddr),
                .DiMemReq(DiMemReq),
                .DiMemRd(DiMemRd),
                .DiMemFlush(DiMemFlush),

		.di_config(di_config)
		);

endmodule // di_flipper
