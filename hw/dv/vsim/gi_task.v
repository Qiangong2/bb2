`ifdef INC_GI

task gi_swrite;
	input [31:0] addr;
	input [31:0] data;

	begin
		top.`gi_cpu_write1w(addr, 4'b1111, data);
	end
endtask


task gi_sread;
	input [31:0] addr;
	output [31:0] data;
	begin
		top.`gi_cpu_readbk1w(addr, 4'b1111, data);
	end
endtask

task gi_bread;
	input [31:0] addr;
	input [2:0] size;
	begin
		case (size)
			1: top.`gi_cpu_readbk1w(addr, 4'b1111, `blk[0]);
			2: top.`gi_cpu_readbk2w(addr, `blk[0], `blk[1]);
			4: top.`gi_cpu_readbk4w(addr, `blk[0], `blk[1], `blk[2], `blk[3]);
		endcase
	end
endtask

task gi_bwrite;
	input [31:0] addr;
	input [2:0] size;
	begin
		case (size)
			1: top.`gi_cpu_write1w(addr, 4'b1111, `blk[0]);
			2: top.`gi_cpu_write2w(addr, `blk[0], `blk[1]);
			4: top.`gi_cpu_write4w(addr, `blk[0], `blk[1], `blk[2], `blk[3]);
		endcase
	end
endtask

`endif
