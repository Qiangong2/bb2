//
//   ipc_module.v
//
//   ipc module body
//   
//   :set tabstop=4


begin : bb2_ipc

	reg ipc_mon, ipc_msg;
	reg live_socket;	// Persistent connection
	reg done;
	reg ztime;

	integer ipc_fd, new_fd;         // socket file descriptors;
	integer ret;                    // pli return code;
	integer i, interrupts;

	reg [31:0] req_code;            // request code;
	reg [31:0] reply_code;          // reply code;
	reg [31:0] req_size;            // request size;
	reg [31:0] req_address;         // request address;

	reg [31:0] req_data0, rsp_data0;
	reg [31:0] req_data1, rsp_data1;
	reg [31:0] req_data2, rsp_data2;
	reg [31:0] req_data3, rsp_data3;
	reg [31:0] req_data4, rsp_data4;
	reg [31:0] req_data5, rsp_data5;
	reg [31:0] req_data6, rsp_data6;
	reg [31:0] req_data7, rsp_data7;
	reg [68*8:1] disp_msg;

`ifdef ALI_GI
	`gi_sim_top.p_target_off = 1'b1;
`endif
	$display("%M: Iosim simulator start");
	ipc_mon = $test$plusargs("ipc_mon");
	live_socket = 0;	// default close socket for each call
	done = 0;	
	
	ret = $ipc_init();
	if (ret < 0) begin
		$display("ERROR: %t: %M: cannot init ipc PLI", $time);
		$finish;
	end

	ipc_fd = $ipc_open();		/* Open server socket */
	if (ipc_fd < 0) begin
		$display("ERROR: %t: %M: ipc_open", $time);
		$finish;
	end

	while (done == 0) begin 

		ztime = 1;
		while (ztime) begin
			if (live_socket) begin
				if (ipc_mon)
					$display("%t: %M: Use old socket", $time);
			end else begin
				if (ipc_mon) 
					$display("%t: %M: open new socket", $time);
				new_fd = $ipc_accept(ipc_fd);
			end

			if (new_fd < 0) begin
				$display("ERROR: %t: %M: ipc_accept", $time);
				$finish;  /* nothing we can do */
			end

			ret = $ipc_receive(new_fd,
					req_code, req_size, req_address,
					req_data0, req_data1, req_data2, req_data3,
					req_data4, req_data5, req_data6, req_data7, disp_msg);	

			if (ret < 0) begin
				$display("ERROR: %t: %M: ipc recv", $time);
				$finish;
			end

			if (ipc_mon) begin
				$display("%t: %M: ipc request code=0x%x", $time, req_code);
				$display("%t: %M: ipc request address=0x%x", $time, req_address);
				$display("%t: %M: req_data0 = 0x%x",  $time, req_data0);
				$display("%t: %M: req_data1 = 0x%x",  $time, req_data1);
				$display("%t: %M: req_data2 = 0x%x",  $time, req_data2);
				$display("%t: %M: req_data3 = 0x%x",  $time, req_data3);
				$display("%t: %M: req_data4 = 0x%x",  $time, req_data4);
				$display("%t: %M: req_data5 = 0x%x",  $time, req_data5);
				$display("%t: %M: req_data6 = 0x%x",  $time, req_data6);
				$display("%t: %M: req_data7 = 0x%x",  $time, req_data7);
			end
			rsp_data0 = 32'hxxxxxxxx;
			rsp_data1 = 32'hxxxxxxxx;
			rsp_data2 = 32'hxxxxxxxx;
			rsp_data3 = 32'hxxxxxxxx;
			rsp_data4 = 32'hxxxxxxxx;
			rsp_data5 = 32'hxxxxxxxx;
			rsp_data6 = 32'hxxxxxxxx;
			rsp_data7 = 32'hxxxxxxxx;

			ztime = req_code[31];
			case (req_code) 
					// regular request 
				`REQ_STALL: begin
						repeat(req_data0-1) @(posedge `ipc_clk);
						reply_code = `RSP_OK;	
					end
`ifdef NO_CPU
				`REQ_SINGLE_READ: begin
						if (req_size != 4) 
							$display("%t: %M: Error: word only", $time);
						else if (req_address[1:0] == 2'b00) 
							`sread(req_address, rsp_data0);
						else rsp_data0 = 32'hxxxxxxxx;
						reply_code = `RSP_DATA;	
					end

				`REQ_BLOCK_READ: begin
`ifdef ALI_GI
						`bread(req_address, req_size);
`else	
						if ((req_size != 4) || (req_address[3:0] != 5'b0))
							$display("%t: %M: Error: bread a=0x%x s=0x%x", 
								  $time, req_address, req_size);
						else begin
							`bread(req_address, req_size);	
						end
`endif
						rsp_data0 = `blk[0];
						rsp_data1 = `blk[1];
						rsp_data2 = `blk[2];
						rsp_data3 = `blk[3];
						reply_code = `RSP_DATA;	
					end

				`REQ_SINGLE_WRITE: begin
						if (req_size != 4) 
							$display("%t: %M: Error: word only", $time);
						else `swrite(req_address, req_data0);
						reply_code = `RSP_OK;	
					end

				`REQ_BLOCK_WRITE: begin
						`blk[0] = req_data0;
						`blk[1] = req_data1;
						`blk[2] = req_data2;
						`blk[3] = req_data3;
`ifdef ALI_GI
						`bwrite(req_address, req_size);
`else
						if ((req_size != 4) || (req_address[3:0] != 5'b0))
							$display("%t: %M: Error: bwrite a=0x%x s=0x%x", 
								  $time, req_address, req_size);
						else begin
							`bwrite(req_address, req_size);
						end
`endif
						reply_code = `RSP_OK;	
					end
`endif
				`REQ_QUIT: begin
						reply_code = `RSP_OK;	
						ztime = 0;
						live_socket = 0;
						done = 1;
					end


`ifdef INC_NB  /* North Bridge calls */
				`REQ_GET_cpu_vec_02_end: begin
						rsp_data0 = top.cpu_vec_02_end;
						reply_code = `RSP_DATA;	
					end

				`REQ_RESET_CHIP: begin
						top.ResetChip;
						reply_code = `RSP_OK;	
					end		

				`REQ_INIT: begin
						top.Initialize;
						reply_code = `RSP_OK;	
					end

				`REQ_POST_INIT: begin
						top.POST_initial;
						reply_code = `RSP_OK;	
					end

				`REQ_PCI_PRE_FETCH: begin
						top.PCI_pre_fetch(req_data0);
						reply_code = `RSP_OK;	
					end

				`REQ_SET_POS_WR_TIMER: begin
						top.Set_pos_write_timer(req_data0);
						reply_code = `RSP_OK;	
					end

				`REQ_STRAP_PIN_INFO: begin
						top.STRAP_Pin_Info;
						reply_code = `RSP_OK;	
					end

				`REQ_SDRAM_INIT: begin
						top.SDRAM_Init;
						reply_code = `RSP_OK;	
					end

				`REQ_SDRAM_SET_ARBT: begin
						top.SDRAM_Set_Arbt(req_data0, req_data1, req_data2,
								req_data3, req_data4, req_data5);
						reply_code = `RSP_OK;	
					end

				`REQ_ROM_INIT: begin
						top.ROM_Initialize;
						reply_code = `RSP_OK;	
					end

				`REQ_FLASH_TEST_LD_DATA: begin
						top.flash_test_load_data;
						reply_code = `RSP_OK;	
					end
`endif

					// Backdoor request 
				`BD_REQ_HANDSHAKE: begin
						reply_code = `RSP_OK;	
					end

				`BD_REQ_SINGLE_READ: begin
						// backdoor single read here
						bd_sread(req_address, req_size-1, rsp_data0);
						reply_code = `RSP_DATA;	
					end

				`BD_REQ_SINGLE_WRITE: begin
						// backdoor single write here
						bd_swrite(req_address, req_size-1, req_data0);
						reply_code = `RSP_OK;	
					end

				`BD_REQ_BLOCK_READ: begin
						// backdoor block read
						bd_bread(req_address, req_size, rsp_data0, rsp_data1, rsp_data2,
							rsp_data3, rsp_data4, rsp_data5, rsp_data6, rsp_data7);
						reply_code = `RSP_DATA;	
					end	

				`BD_REQ_BLOCK_WRITE: begin
						bd_bwrite(req_address, req_size, req_data0, req_data1, req_data2,
							req_data3, req_data4, req_data5, req_data6, req_data7);
						reply_code = `RSP_OK;	
					end

				`BD_REQ_LOG: begin
						reply_code = `RSP_OK;	
						ipc_mon = req_data0 ? 1'b1 : 1'b0;
						$display("%t: %M: ipc monitor is %s",
							$time, ipc_mon ? "ON" : "OFF");
					end

				`BD_REQ_PERSIST_SOCKET: begin
						live_socket = req_data0 ? 1'b1 : 1'b0;
						reply_code = `RSP_OK;	
					end

				`BD_REQ_DISPLAY_MSG: begin
						reply_code = `RSP_OK;	
						$display("%t BB2-Info: %s",$time,disp_msg);
					end

				`BD_REQ_DUMP: begin
						$display("%t BB2-Info: dump %s",
							$time, req_data0 ? "on" : "off");
`ifdef ALI_GI
`else
						sim.dump.dump = req_data0 ? 1'b1 : 1'b0;
`endif
						reply_code = `RSP_OK;	
					end

				`BD_REQ_RTL_TIME: begin : rtl_time
						reg [63:0] tt;
						tt = $time;
						rsp_data0 = tt[63:32];
						rsp_data1 = tt[31:0];
						reply_code = `RSP_OK;	
					end

				`BD_PAT_FILL: begin
						bd_fill_pat(req_data0, req_data1, req_data2, req_data3);
						reply_code = `RSP_OK;
					end

				`BD_PAT_CHECK: begin
						bd_check_pat(req_data0, req_data1, req_data2, 
								req_data3, rsp_data0);
						reply_code = `RSP_DATA;
					end

				`BD_GI_SECURE_BIT: begin
						rsp_data0 = `sec_bit;
						reply_code = `RSP_DATA;	
					end

				`BD_DI_RESET: begin
						`di_reset(req_data0);
						reply_code = `RSP_OK;	
					end

				`BD_DI_XFER: begin
						`di_xfer(req_data0, req_data1, req_data2, req_data3, req_data4);
						reply_code = `RSP_OK;	
					end

				`BD_DI_GET: begin
						`di_get(req_data0, rsp_data0);
						reply_code = `RSP_DATA;	
					end

				`BD_DI_PUT: begin
						`di_put(req_data0, req_data1);
						reply_code = `RSP_OK;	
					end

				`BD_DI_BREAK_AT: begin
						`di_break(req_data0);
						reply_code = `RSP_OK;	
					end

				`BD_DI_BREAK_NOW: begin
						`di_break_now;
						reply_code = `RSP_OK;	
					end

				`BD_AIS_RESET : begin
						`ais_rst;
						reply_code = `RSP_OK;	
					end

				`BD_AIS_SET : begin
						`ais_set(req_data0);
						reply_code = `RSP_OK;	
					end

				`BD_AIS_GET: begin
						`ais_get(req_data0, rsp_data0);
						reply_code = `RSP_DATA;	
					end

				`BD_AIS_PUT : begin
						`ais_put(req_data0, req_data1);
						reply_code = `RSP_OK;	
					end	

				`BD_AIS_START: begin
						`ais_start(req_data0, req_data1);
						reply_code = `RSP_OK;	
					end

				`BD_AIS_GET_PERIOD: begin
						rsp_data0 = `ais_period;
						reply_code = `RSP_DATA;	
					end

				`BD_AIS_SET_PERIOD: begin
						`ais_period = req_data0;
						reply_code = `RSP_OK;	
					end
				`BD_AIS_GET_POS: begin
						rsp_data0 = `ais_pos;
						reply_code = `RSP_DATA;
					end

				`BD_AIS_CHECK : begin
						`ais_check;
						reply_code = `RSP_OK;	
					end


				`BD_SYS_DMA_SETUP: begin
`ifdef PCI_HOST
						$display("%t %M SYS DMA setup %d", $time, req_data0);
						if (req_data0) 
							`gi_sim_top.sys.setup(`REQ_GI, `REQ_TARGET_CMDS, 
									16'b1101_0000_1100_0000);
						`gi_sim_top.sys.setup(`REQ_GI, `REQ_BE_TARGET, req_data0);
`endif
						reply_code = `RSP_OK;	
					end
				`BD_SYS_DMA_CMD_MON: begin
`ifdef ALI_GI
						`gi_sim_top.pci_mon_int.cmd_mask(`REQ_GI, req_data0? 
								16'b0000_0000_1100_0000 : 16'd0);
`endif

`ifdef GI_PCI 
						`gi_sim_top.pci_mon_ext.cmd_mask(`REQ_GI, req_data0? 
								16'b0000_0000_1100_0000 : 16'd0);
`endif
						reply_code = `RSP_OK;	
					end

				`BD_SET_ALT_BOOT: begin
`ifdef ALI_GI
						`gi_sim_top.alt_boot = req_data0 ? 1 : 0;
`endif
						reply_code = `RSP_OK;	
					end

				`BD_GET_ALT_BOOT: begin
`ifdef ALI_GI
						rsp_data0 = `gi_sim_top.alt_boot;
`else
						rsp_data0 = 0; 
`endif
						reply_code = `RSP_DATA;	
					end

				`BD_SET_TEST_IN: begin
`ifdef ALI_GI
						`gi_sim_top.test_in = req_data0 ? 1 : 0;
`endif
						reply_code = `RSP_OK;	
					end

				`BD_GET_TEST_IN: begin
`ifdef ALI_GI
						rsp_data0 = `gi_sim_top.test_in;
`endif
						reply_code = `RSP_DATA;	
					end

				`BD_BI_SIZE: begin
						rsp_data0 = `gi_top.bi.bi_size;
						reply_code = `RSP_DATA;	
					end

				`BD_REQ_JTAG_WRITE: begin
`ifdef ALI_GI
						`gi_sim_top.jtag_tests.jtag_driver = req_data0[4:0];
`endif
						reply_code = `RSP_DATA;	
					end

				`BD_REQ_JTAG_READ: begin
`ifdef ALI_GI
						rsp_data0[4:0] = `gi_sim_top.jtag_tests.jtag_driver;
`endif
						reply_code = `RSP_DATA;	
					end
			
					// invalid request 	
				default: begin
						reply_code = `RSP_ERROR;	
						$display("ERROR: %t: %M: ipc req %0d",
								$time, req_code);
					end
			endcase	 /* end of case */
		
			interrupts = {`gi_top.sys_intr, 31'b0};	
			if (ipc_mon) begin
				$display("%t: %M: ipc reply code=0x%x", $time, reply_code);
				$display("%t: %M: ipc reply interrupt=0x%x", $time, interrupts);
				$display("%t: %M: ipc reply address=0x%x", $time, req_address);
				$display("%t: %M: rsp_data0 = 0x%x",  $time, rsp_data0);
				$display("%t: %M: rsp_data1 = 0x%x",  $time, rsp_data1);
				$display("%t: %M: rsp_data2 = 0x%x",  $time, rsp_data2);
				$display("%t: %M: rsp_data3 = 0x%x",  $time, rsp_data3);
				$display("%t: %M: rsp_data4 = 0x%x",  $time, rsp_data4);
				$display("%t: %M: rsp_data5 = 0x%x",  $time, rsp_data5);
				$display("%t: %M: rsp_data6 = 0x%x",  $time, rsp_data6);
				$display("%t: %M: rsp_data7 = 0x%x",  $time, rsp_data7);
			end
			
			ret = $ipc_send(new_fd,
					reply_code, interrupts, req_address,
					rsp_data0, rsp_data1, rsp_data2, rsp_data3,
					rsp_data4, rsp_data5, rsp_data6, rsp_data7);

			if (ret < 0) begin
				$display("ERROR: %t: %M: ipc send", $time);
				$finish;
			end 
			
			if (!live_socket) begin
				if (ipc_mon)
					$display("%t: %M: socket closed", $time);
				ret = $ipc_close(new_fd);
			end
		end /* end of while ztime */
		@(posedge `ipc_clk);
	end /* end of while done */
`ifdef NO_CPU
`else
	$finish;
`endif
end
