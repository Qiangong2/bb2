// JTAG external module

module jtag_tests(
	trst,
	tdi,
	tms,
	tck,
	tdo
);
	output trst;
	output tdi;
	output tms;
	output tck;
	input tdo;

	reg[4:0] jtag_driver;
	reg jtag_mon;

	initial
	begin
		jtag_mon = $test$plusargs("jtag_mon");
		$display("%M: enabled %b", jtag_mon);
		jtag_driver = 4'h0;
	end

	assign	trst = jtag_driver[0];
	assign	tdi = jtag_driver[1];
	assign	tms = jtag_driver[2];
	assign	tck = jtag_driver[3];
	always @(tdo) 
		jtag_driver[4] = tdo;

	always @(jtag_driver)
	if (jtag_mon) begin
		$display("%t: %M: Signals: trst %b tdi %b tms %b tck %b tdo %b", $time, trst, tdi, tms, tck, tdo);
	end

endmodule // module jtag tests
