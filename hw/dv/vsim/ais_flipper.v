/*****************************************************************************
 * 
 *   (C) 2004 BroadOn INC..  ALL RIGHTS RESERVED.  UNPUBLISHED -- RIGHTS
 *   RESERVED UNDER THE COPYRIGHT LAWS OF THE UNITED STATES.  USE OF A
 *   COPYRIGHT NOTICE IS PRECAUTIONARY ONLY AND DOES NOT IMPLY PUBLICATION
 *   OR DISCLOSURE.
 *
 *   THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF
 *   BroadOn INC..  USE, DISCLOSURE, OR REPRODUCTION IS PROHIBITED WITHOUT
 *   THE PRIOR EXPRESS WRITTEN PERMISSION OF BroadOn INC..
 *
 *                   RESTRICTED RIGHTS LEGEND
 *
 *   Use, duplication, or disclosure by the Government is subject to
 *   restrictions as set forth in subparagraph (c)(1)(ii) of the Rights
 *   in Technical Data and Computer Software clause at DFARS 252.227-7013
 *   or subparagraphs (c)(1) and (2) of Commercial Computer Software --
 *   Restricted Rights at 48 CFR 52.227-19, as applicable. 
 *
 *   BroadOn Inc.
 *   3400 Hillview Ave, Bldg 5
 *   Palo Alto, CA 94304
 *
 ****************************************************************************/

`timescale 1ns/1ns

// syn myclk = `io_clk

// NOTES:
// the following must be added to Makefile to compile this:
/*
	-y ../../lib/nintendo/AI/hw/ai \
        -v ../../lib/nintendo/DI/hw/lib/memories/MemS1R1W96x16M1.v \
        -v ../../lib/nintendo/DI/hw/cmn/io_reg.v \
*/
// also, ais_host must be replaced with ais_flipper in sim.v 
// (same port names, so very easy)
// HOWEVER some NEC Memory task is not available so I did surgery to
// kill the Memory ref, but you can use all from tree and figure it out right

// the idea here is to use ais_flipper in parallel with ais_host,
// we still use ais_host.v so we can monitor values input;
// now we need to capture output values and compare too.
// this code sets volume to max = .ffff so there is sometimes
// mismatch in data (output may match or be 1 lsb less than input).

// frank's environment (gi/dv/test_gi.v) has ais tests in it,
// and they compare data at the input to ais so that checking
// is still to be used.  (and ais_mon.v)

// this testbench configures AIS and it passes the data thru to the DAC.
// the data out capture and compare with input is not yet implemented.

// note glitchy output to audio DAC!  (AID) we can sample it OK but
// it will generate some noise on the board we could have done without

module ais_flipper (/*AUTOARG*/

    // Output to External DVD drive
    ais_clk, ais_lr,

    // General Input
    reset,

    // Input from External DVD drive
    ais_d

);

    			// Output to external device

    output
        ais_clk;		// ais clock

    output
        ais_lr;			// left/right signal

    			// General inputs
    input
        reset;			// interface reset

    			// Input from external DVD driver
    input
        ais_d;			// audio bitstream

   // General
   wire		reset;
   //wire		io_clk;
   wire		io_VidClk;
   wire [15:0]	dsp_ioL		=	16'h0000;	//dsp sound source
   wire [15:0]	dsp_ioR		=	16'h0000;	//dsp sound source
   wire		io_dspPop;

   // PI - Host Interface
   wire	[4:1]	 PiAddr;    	// Address from io_pi
   wire	 	 PiLoad;    	// Load Register from io_pi
   wire	[15:0]   PiData;    	// Data bus in from io_pi
   wire	[15:0]   AiPiData;  	// Data bus out to io_pi
   wire	 	 PiAiSel;   	// Ai select
   wire	 	 ai_piInt;  	// Ai Interrupt

   // AI - Audio DAC Interface and streaming audio interface
   wire	 	aid;		// dac data 
   wire	 	ailr;		// dac left-right
   wire	 	aiclk;		// dac clock 
   wire	 	ais_d;		// stream data 
   wire	 	ais_lr;		// stream left-right
   wire	 	ais_clk;	// stream clock 

   // BIST
   wire	 	BistEnable	=	1'b0;	
   wire	 	BistAddr;
   wire	 	BistData1;
   wire	 	BistRdn;
   wire	 	BistWen96;
   wire	 	BistCmp96;
   wire	[1:0] 	BistErr;

   // dvd audio
   wire 	dvdaudio_mode;		//if set, 96MHz DSP audio src AIIT

// define test environment

`define	io_clk		sim.clk
`define AisLRMuxSel	sim.ais_host2.io_Ai_top.io_Ai_dsp48.io_AiClks0.AisLRMuxSel

	clkgen ais_VidClk	(.period(37000), .clk(io_VidClk));

	reg     [24:0]  instruction;

//XXX	assign  pi_ioAiRstb     =       instruction[24];
        assign  PiAiSel         =       instruction[23];
        assign  PiLoad          =       instruction[22];
        assign  PiAddr          =       instruction[20:17];
        assign  PiData          =       instruction[15:0];

        always @(posedge `io_clk) begin	//set PI bus to idle
                if (!reset)     instruction     <=      25'h1000000;
        end

	always @(posedge reset) begin	//initialize AICR mode
		@(posedge `io_clk);
                        instruction <= {1'b1, 1'b1, 1'b1, 6'h02, 16'h003e};
                @(posedge `io_clk);
		@(posedge `io_clk);
                        instruction <= {1'b1, 1'b0, 1'b0, 6'hxx, 16'hxxxx};
		@(posedge `io_clk);
                        instruction <= {1'b1, 1'b1, 1'b1, 6'h06, 16'hffff};	//need aux volume? AIVR
                @(posedge `io_clk);
                @(posedge `io_clk);
                        instruction <= {1'b1, 1'b0, 1'b0, 6'hxx, 16'hxxxx};
	end
	
        // start streaming audio;
        // samples[] should have been loaded with expected data;

        // dv accessible variables;

        reg [31:0] samples [0:16*1024-1];       // sample buffer;

        integer nsamples;         			// # of expected samples;
        reg skip;                                       // skip initial 0 samples;
        reg stream;                                     // start streaming;

        initial
                stream = 0;

        task start;
                input [15:0] ns;                // # of samples;
                input skip0;                    // skip 0s;
                begin
                        $display("%t: %M: n=%0d skip=%b", $time, ns, skip0);
                        nsamples = ns;
                        skip = skip0;
                        stream = 1;
                end
        endtask

        always @(posedge stream) begin

                @(posedge `io_clk);
                        instruction <= {1'b1, 1'b1, 1'b1, 6'h02, 16'h003f};
                @(posedge `io_clk);
                @(posedge `io_clk);
                        instruction <= {1'b1, 1'b0, 1'b0, 6'hxx, 16'hxxxx};

//
        // streaming loop;

	@(posedge `io_clk);
        begin : loop
                integer n, bit;
                reg [31:0] sample, exp;
                reg s0;

                @(posedge ais_lr);
                n = 0;
                while(n < nsamples) begin
                        for(bit = 0; bit < 32; bit = bit + 1) begin
				  @(posedge `io_clk);
//                                @(posedge ais_clk or posedge ais_rst);
//                                if(ais_rst == 1) begin
//                                        bit = 32;
//                                        nsamples = 0;
//                                end
                                sample = { sample[30:0], ais_d };
                        end
                        s0 = skip & (sample === 32'd0);
                        if( !s0 ) begin
                                skip = 0;
                                exp = samples[n];
                                if(sample !== exp) begin
                                        $display("ERROR: in to Flipper: %t: %M: sample[%0d]=0x%h exp 0x%h",
                                                $time, n, sample, exp);
                                end
                                n = n + 1;
                        end
                end
	end
//

                stream = 0;
        end

/*
 * Instantiations
*/

	io_Ai_top	io_Ai_top  (
   		.io_dspPop(io_dspPop), .AiPiData(AiPiData), .ai_piInt(ai_piInt), .aid(aid), 
		.ailr(ailr), .aiclk(aiclk), .aislr(ais_lr), .aisclk(ais_clk),
		.BistErr(BistErr), .dvdaudio_mode(dvdaudio_mode),

		.io_clk(`io_clk), .io_VidClk(io_VidClk), .resetb(reset), .dsp_ioL(dsp_ioL),
		.dsp_ioR(dsp_ioR), .PiAddr(PiAddr), .PiLoad(PiLoad), .PiData(PiData),
		.PiAiSel(PiAiSel), .aisd(ais_d), .BistEnable(BistEnable), .BistAddr(BistAddr),
		.BistData1(BistData1), .BistRdn(BistRdn), .BistWen96(BistWen96), .BistCmp96(BistCmp96)
	);

/*
//	XXXXXXXXXXXXXXXXXXXXXXXX frank's host code begin

	// monitor changes in period;
	// 250ns is fastest gi can handle;

	task set;
		input fast;		// set fast clock;
		begin
			period = fast? 250 : 651;
		end
	endtask

	always @(period)
		$display("%t: %M: period=%0dns", $time, period);

	// reset task;
	// guarantees minimum reset time;

	task rst;
		begin
			$display("%t: %M", $time);
			ais_lr = 0;
			ais_rst = 1;
		end
	endtask

	// dv accessible variables;

	reg [31:0] samples [0:16*1024-1];	// sample buffer;

	// get sample;

	task get;
		input [13:0] off;		// sample offset;
		output [31:0] word;		// word;
		begin
			word = samples[off];
		end
	endtask

	// put sample;
	// will be compared to incoming sample;

	task put;
		input [13:0] off;		// sample offset;
		input [31:0] word;		// word;
		begin
			samples[off] = word;
		end
	endtask

	// start streaming audio;
	// samples[] should have been loaded with expected data;

	integer nsamples;			// # of expected samples;
	reg skip;						// skip initial 0 samples;
	reg stream;						// start streaming;

	initial
		stream = 0;

	task start;
		input [15:0] ns;		// # of samples;
		input skip0;			// skip 0s;
		begin
			$display("%t: %M: n=%0d skip=%b", $time, ns, skip0);
			nsamples = ns;
			skip = skip0;
			stream = 1;
		end
	endtask

	// streaming loop;

	always @(posedge stream)
	begin : loop
		integer n, bit;
		reg [31:0] sample, exp;
		reg s0;

		@(posedge ais_clk);
		n = 0;
		while(n < nsamples) begin
			for(bit = 0; bit < 32; bit = bit + 1) begin
				if(bit == 0)
					ais_lr = 1;
				else if(bit == 16)
					ais_lr = 0;
				@(posedge ais_clk or posedge ais_rst);
				if(ais_rst == 1) begin
					bit = 32;
					nsamples = 0;
				end
				sample = { sample[30:0], ais_d };
			end
			s0 = skip & (sample === 32'd0);
			if( !s0 & !ais_rst) begin
				skip = 0;
				exp = samples[n];
				if(sample !== exp) begin
					$display("ERROR: %t: %M: sample[%0d]=0x%h exp 0x%h",
						$time, n, sample, exp);
				end
				n = n + 1;
			end
		end
		ais_lr = 0;
		ais_rst = 0;
		stream = 0;
	end
//	XXXXXXXXXXXXXXXXXXXXXXXXXXXX	old code end
*/

endmodule
