/*
 * File: socket.c

 * PLI socket connection library
 :set tabstop=4
 */

#include <sys/types.h>
#include <sys/prctl.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netinet/in.h>

#include "acc_user.h"
#ifdef VCS
#include "vcsuser.h"
#else
#include "veriuser.h"
#endif
static s_setval_value value_s = { accIntVal };
static s_setval_delay delay_s = { { accRealTime, 0, 0, 0 }, accNoDelay };

    /* global variable to setup socket connection */
char *iosim_server=NULL;
int iosim_port=0;

int IpcInit(void)
{
	char *sport;
	acc_initialize();
	acc_configure(accDevelopmentVersion, "1.6");

	iosim_server = getenv("IOSIM_SERVER");
	sport = getenv("IOSIM_PORT");
	if (sport == NULL) {
		fprintf(stderr, "\n\nFatal error: please define IOSIM_PORT \n");
		tf_putp(0, -1);
		return -1;
	}
	iosim_port = strtoul(sport, NULL, 0);
	
	if ((iosim_server == NULL) || (iosim_port <= 0)) {
		fprintf(stderr, "\n\nFatal error: please set environment variable: \n");
		fprintf(stderr, "             IOSIM_SERVER and IOSIM_PORT\n\n\n");
		tf_putp(0, -1);
		return -1;
	}

	fprintf(stderr, "IOSIM server=%s port=%d\n", iosim_server, iosim_port);
	tf_putp(0, 0);
	return 0;
}

int IpcReceive(int fd, char *buf, int nbytes)
{
	int nleft, nread;
	
	bzero(buf, nbytes);
	nleft = nbytes;
	while (nleft > 0) {
		nread = recv(fd, buf, nleft, 0);
		if (nread < 0) {
			fprintf(stderr, "IpcReceive: recv failed, errno=%d\n", errno);
			return -1;
		}
		nleft -= nread;
		buf += nread;
	}

	return( (nleft > 0) ? -1 : 0);
}

int IpcSend(int fd, char *buf, int nbytes)
{
	int nleft, nwritten;
	int ret;

	nleft = nbytes;
	while (nleft > 0) {
		nwritten = send(fd, buf, nleft, 0);
		if (nwritten <= 0) {
			fprintf(stderr, "IpcSend: send failed, errno=%d\n", errno);
			return -1;
		}
		nleft -= nwritten;
		buf += nwritten;
	}

	return( (nleft > 0) ? -1 : 0);
}

int IpcOpen(void)
{
	int fd;
	struct sockaddr_in addr;
	int on;

#ifdef VCS
	if (tf_nump() != 0) {
		fprintf(stderr, "illegal #of arg(ipc_open): %d\n", tf_nump());
#else
	if (acc_fetch_tfarg_int(1) != 0) {
		fprintf(stderr, "illegal #of arg(ipc_open): %d\n", acc_fetch_tfarg_int(1));
#endif
		tf_putp(0, -1);
		return -1;
	}
	
	fd = socket(AF_INET, SOCK_STREAM, 0);
	if (fd < 0) {
		fprintf(stderr, "Cannot open AF_INET socket");
		tf_putp(0, -1);
		return -1;
	}

	memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = htonl(INADDR_ANY);
	addr.sin_port = htons(iosim_port);

	if (setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof on) < 0) {
		fprintf(stderr,"SO_REUSEADDR stream socket");
		tf_putp(0, -1);
		return -1;
	}

	if (bind(fd, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
		fprintf(stderr,"bind failed, errno=%d\n", errno);
		close(fd);
		tf_putp(0, -1);
		return -1;
	}

	if (listen(fd, 5) < 0) {
		fprintf(stderr,"listen failed, errno=%d\n", errno);
		close(fd);
		tf_putp(0, -1);
		return -1;
	}

	tf_putp(0, fd);
	return fd;
}

int IpcAccept(void)
{
	int  newfd=-1, fd;
	struct sockaddr_in from;
	int fromlen = sizeof(from);

	if (tf_nump() != 1) {
		fprintf(stderr, "illegal # of argv(accept): %d", tf_nump());
		tf_putp(0, -1);
		return -1;
	}
	fd = (int)tf_getp(1);

	newfd = accept(fd, (struct sockaddr *)&from, &fromlen);
	if (newfd < 0) {
		fprintf(stderr, "accept failed, errno=%d\n", errno);
		tf_putp(0, -1);
	} else tf_putp(0, newfd);

	return newfd;
}

int IpcClose(void)
{
	int fd;

	if (tf_nump() != 1) {
		fprintf(stderr, "illegal # of argv(close): %d", tf_nump());
		tf_putp(0, -1);
		return 0;
	}
	fd = (int)tf_getp(1);
	
	close(fd);
	tf_putp(0, 0);
	return 0;
}
	
int IpcReceivePli(void)
{
	handle hparam[12];
	int i,fd;
	unsigned int buf[20];
	s_acc_vecval tmp;
	char *str;

	if (tf_nump() != 13) {
		fprintf(stderr, "illegal # of argv(receive): %d", tf_nump());	
		tf_putp(0, -1);
		return;
	}
	fd   = (int)tf_getp(1);

	for (i=0; i<12; i++)  /* handle after socket fd */
		hparam[i] = acc_handle_tfarg(2 + i);

	if (IpcReceive(fd, (char *)buf, sizeof(buf)) < 0) {
		fprintf(stderr, "IpcReceivePli: IpcReceive failed");
		tf_putp(0, -1);
	} else {
		value_s.format = accIntVal;
		for (i=0; i<3; i++) { /*code, size, addr, skip length*/
			value_s.value.integer=ntohl((int)buf[i+1]);
			acc_set_value(hparam[i], &value_s, &delay_s);
		}

		value_s.format = accVectorVal;
		for (i=3; i<11; i++) { /* 8 word data */
			tmp.aval = ntohl((int)buf[i+1]);
			tmp.bval = ntohl((int)buf[i+9]);
			value_s.value.vector = &tmp;
			acc_set_value(hparam[i], &value_s, &delay_s);
		}

		  /* convert data into string */
		value_s.format = accStringVal;  
		for (i=4; i<20; i++) buf[i] = ntohl(buf[i]);
		str = (char *)(buf+4);
		value_s.value.str = str;
		acc_set_value(hparam[11], &value_s, &delay_s);

		tf_putp(0, 0);
	}
	
	return;
}

int IpcSendPli(void)
{
	handle hdata[8];
	int i, fd;
	unsigned int buf[20];
	s_acc_vecval tmp;
	s_acc_value v;

	if (tf_nump() != 12) {
		fprintf(stderr, "illegal # of argv(receive): %d", tf_nump());	
		tf_putp(0, -1);
		return;
	}
	fd   = (int)tf_getp(1);

	memset(buf, 0, sizeof(buf));
	buf[0] = htonl(sizeof(buf));            /* length */
	buf[1] = htonl((int) tf_getp(2));       /* code */
	buf[2] = htonl((int) tf_getp(3));       /* size */ 
	buf[3] = htonl((int) tf_getp(4));       /* address */

	v.value.vector = &tmp;
	v.format = accVectorVal;
	for (i=0; i<8; i++) { /* split 8 word */
		hdata[i] = acc_handle_tfarg(5+i);
		acc_fetch_value(hdata[i], "%%", &v);
		buf[4+i] = htonl(v.value.vector->aval);
		buf[12+i] = htonl(v.value.vector->bval);
	}

	if (IpcSend(fd, (char *)buf, sizeof(buf)) < 0) {
		fprintf(stderr, "IpcSendPli: IpcReceive failed");
		tf_putp(0, -1);
	} else tf_putp(0, 0);
	
	return;
}
