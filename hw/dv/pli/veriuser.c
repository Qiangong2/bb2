/*
 * Dynamic loading Pli interface for NC-verilog
 *
 * :set tabstop=4
 */
#include <vxl_veriuser.h>
#include "ipc.h"
 
p_tfcell bb2_boot_func()
{
	static s_tfcell bb2_tfs[] = {
		{userfunction, 0, 0, 0, IpcInit, 0, "$ipc_init"},
		{userfunction, 0, 0, 0, IpcOpen, 0, "$ipc_open"},
		{userfunction, 0, 0, 0, IpcClose, 0, "$ipc_close"},
		{userfunction, 0, 0, 0, IpcAccept, 0, "$ipc_accept"},
		{userfunction, 0, 0, 0, IpcReceivePli, 0, "$ipc_receive"},
		{userfunction, 0, 0, 0, IpcSendPli, 0, "$ipc_send"},
		{userfunction, 0, 0, 0, getstr_plusarg_call, 0, "$getstr_plusarg_call"},
		{userfunction, 0, 0, 0, getnum_plusarg_call, 0, "$getnum_plusarg_call"},
		{0}
	};

	printf(" # # # BB2 PLI registered # # #\n");

	return bb2_tfs;
}
