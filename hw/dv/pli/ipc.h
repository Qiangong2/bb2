/*
 * File: ipc.h

 * PLI header
 */

#ifndef __PLI_HEADER
#define __PLI_HEADER

int IpcInit(void);
int IpcOpen(void);
int IpcAccept(void);
int IpcClose(void);
int IpcReceivePli(void);
int IpcSendPli(void);
int getnum_plusarg_call(void);
int getstr_plusarg_call(void);

#endif
