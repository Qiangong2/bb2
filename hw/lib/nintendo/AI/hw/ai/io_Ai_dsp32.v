/*******************************************************************************
 * 
 *   (C) 1999 ARTX INC..  ALL RIGHTS RESERVED.  UNPUBLISHED -- RIGHTS
 *   RESERVED UNDER THE COPYRIGHT LAWS OF THE UNITED STATES.  USE OF A
 *   COPYRIGHT NOTICE IS PRECAUTIONARY ONLY AND DOES NOT IMPLY PUBLICATION
 *   OR DISCLOSURE.
 *
 *   THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF
 *   ARTX INC..  USE, DISCLOSURE, OR REPRODUCTION IS PROHIBITED WITHOUT
 *   THE PRIOR EXPRESS WRITTEN PERMISSION OF ARTX INC..
 *
 *                   RESTRICTED RIGHTS LEGEND
 *
 *   Use, duplication, or disclosure by the Government is subject to
 *   restrictions as set forth in subparagraph (c)(1)(ii) of the Rights
 *   in Technical Data and Computer Software clause at DFARS 252.227-7013
 *   or subparagraphs (c)(1) and (2) of Commercial Computer Software --
 *   Restricted Rights at 48 CFR 52.227-19, as applicable. 
 *
 *   ArtX Inc.
 *   3400 Hillview Ave, Bldg 5
 *   Palo Alto, CA 94304
 *
 ******************************************************************************/

`define N	7'd128	// the number of FIR filter taps.
`define N_width 7
`define K	7'd43	// the number of input samples needed for src
`define K_width 7
`define K_minus1 7'd42

module io_Ai_dsp32 (/*AUTOARG*/
   // Outputs
//   io_dspPop, AiPiData, ai_piInt, aid, ailr, aiclk, aislr, aisclk, 
//   io_dspPop, aid, ailr, aiclk, aislr, aisclk, 
   io_dspPop, 
//   BistErr, dsp32, SRC_result, SRC_done, AiLRPos, AiLRNeg, AiClkNeg_out,
   BistErr, SRC_result,

   // Inputs
   io_clk, resetb_sync, dsp_ioL, dsp_ioR, 
   BistEnable, BistAddr, BistData1, BistRdn, BistWen96, BistCmp96,
   dsp32, AiLR_sync, AiLRPos_ioclk, AiLRNeg_ioclk, AisLR_sync, AisLRPos_ioclk, 
   AisLRNeg_ioclk, AisClkPos_ioclk, AisClkNeg_ioclk
   );

// syn myclk = io_clk

   //////////////////////
   // I/O Declarations //
   //////////////////////

   // General
   input io_clk;    		// System clock
   input resetb_sync;    		// System reset

   // from DSP_AI
   input[15:0] 	dsp_ioL;
   input[15:0] 	dsp_ioR;
   output 	io_dspPop;

   // BIST
   input 	BistEnable;	
   input 	BistAddr;
   input 	BistData1;
   input 	BistRdn;
   input 	BistWen96;
   input 	BistCmp96;
   output 	BistErr;

   // new for revB
   input	dsp32;
   input	AiLR_sync;
   input	AiLRPos_ioclk;
   input	AiLRNeg_ioclk;
   input	AisLR_sync;
   input	AisLRPos_ioclk;
   input	AisLRNeg_ioclk;
   input	AisClkPos_ioclk;
   input	AisClkNeg_ioclk;
   output[15:0]	SRC_result;
   
   ///////////////////
   // Internal Vars //
   ///////////////////
   wire[15:0]	read_data;      // sample RAM output
   reg 	      	io_dspPop_int;
   reg 	      	d1io_dspPop;
   reg 	      	d2io_dspPop;
   reg[15:0] 	d1dsp_ioR;
   reg[15:0] 	d1dsp_ioL;

   wire		BistErr_int;
   wire		AisSample;
   reg		counter_val;

   reg		PStat;

   // SRC signals
   wire[5:0]	read_addr;	// address for sample RAM
   wire		read_enable;
   wire[5:0]	newest;
   wire[5:0]	oldest;
   wire		go;
   wire[1:0]	mode;
   wire		clear;
   wire[1:0]	period;   

   wire afr = 0;

always @(posedge io_clk)
begin
  if(~resetb_sync)
    PStat <= 0;
  else
    if(dsp32)
      PStat <= dsp32;
    else
      if(AiLRPos_ioclk)
        PStat <= dsp32;
end

always @(posedge io_clk)
  begin
    if(~resetb_sync)
      counter_val <= 0;
    else
      if(~dsp32)
	counter_val <= 0;
      else
        if(counter_val == 0)
	  begin
	    if(AisLRPos_ioclk)
	      counter_val <= 1;
	  end
  end

assign AisSample = counter_val ? AisLRPos_ioclk: 0 ;

always @(posedge io_clk)
  begin
    if(~resetb_sync)
      begin
	io_dspPop_int   <= 0;
	d1io_dspPop 	<= 0;
	d2io_dspPop 	<= 0;
	d1dsp_ioL 	<= 0;
	d1dsp_ioR 	<= 0;
       end
    else
      begin
	if(counter_val==1)
	  begin
	    io_dspPop_int   <= AisLRPos_ioclk; 	// Pop new sample from 
	  end					// dsp, assert for only 
						// one 200 MHz cycle
	d1io_dspPop <= io_dspPop_int;   	// Delayed version used 
						// to clock in data for 
						// monitor
	d2io_dspPop <= d1io_dspPop; 		// Data is available here
	if(d2io_dspPop)
	  begin
	    d1dsp_ioL <= dsp_ioL;
	    d1dsp_ioR <= dsp_ioR;
	  end
      end
   end // always @ (posedge io_clk)

// these outputs already synch. reset
assign io_dspPop = io_dspPop_int;

// BistErr should already have correct POR value?
assign BistErr = BistErr_int;

  // module instantiations

  //////////////////////////////////		       
  // sample rate conversion block //
  //////////////////////////////////		       
  io_AiSRC io_AiSRC 	(
			.Result(SRC_result),
			.read_addr(read_addr),
			.Done(SRC_done),
			.read_enable(read_enable),
			.io_clk(io_clk),
			.resetb(resetb_sync),
			.AisData(read_data),
			.VR(8'hff),
			.VL(8'hff),
			.AiDR(16'h0000),
			.AiDL(16'h0000),
			.freq(afr),
			.newest(newest),
			.oldest(oldest),
			.go(go),
			.mode(mode),
			.LR(AiLR_sync),
			.period(period),
			.AiLRNeg_ioclk(AiLRNeg_ioclk),
			.clear(clear)
			);

  ////////////////////////////////////////////////////
  // Memory block, contains ram to hold stream data //  
  ////////////////////////////////////////////////////
  io_AiMem_dsp32 io_AiMem (
			.read_data(read_data),
			.newest(newest),
			.oldest(oldest),
			.BistErr(BistErr_int),
			.io_clk(io_clk),
			.resetb(resetb_sync),
			.clear(clear),
			.AisClkPos_ioclk(AisClkPos_ioclk),
			.AisClkNeg_ioclk(AisClkNeg_ioclk),
			.AisLR(AisLR_sync),
			.AisLRPos(AisLRPos_ioclk),
			.AisLRNeg(AisLRNeg_ioclk),
			.AisD(1'b0),
			.read_addr(read_addr),
			.BistEnable(BistEnable),
			.BistAddr(BistAddr),
			.BistData1(BistData1),
			.BistRdn(BistRdn),
			.BistWen96(BistWen96),
			.BistCmp96(BistCmp96),
			.afr(afr),
			.AisSample(AisSample),
			.AiLR(AiLR_sync),
			.read_enable(read_enable),
			.AiDR(d1dsp_ioR),
			.AiDL(d1dsp_ioL)
			);  

  //////////////////////
  // top level Ai FSM //
  //////////////////////
  io_Ai_fsm_dsp32 io_Ai_fsm	(
			.AisClkEnable(),
			.dsp_bypass(),
			.clear(clear),
			.mode(mode),
			.go(go),
			.period(period),
			.io_clk(io_clk),
			.resetb(resetb_sync),
			.PStat(PStat),
			.SRC_done(SRC_done),
			.AiLRPos_ioclk(AiLRPos_ioclk),
			.AiLRNeg_ioclk(AiLRNeg_ioclk),
			.afr(afr),
			.AiLR(AiLR_sync)
			);
endmodule // io_Ai

