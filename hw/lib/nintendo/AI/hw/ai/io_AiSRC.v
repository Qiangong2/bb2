// src model 

module io_AiSRC (
  // Outputs
  Result, read_addr, Done, read_enable,
  // Inputs, general
  io_clk, resetb,  
  // Inputs, data path
  AisData, VR, VL, AiDR, AiDL,
  // Inputs, control
  freq, newest, oldest, go, mode, LR, period,
  AiLRNeg_ioclk, clear
);

  // Outputs
  output[15:0]	Result;
  output[5:0]	read_addr;
  output	Done;
  output	read_enable;

  // Inputs, General
  input 	resetb;
  input		io_clk;
 
  // Inputs, data path
  input [15:0]	AisData;	// 16 bit res. spec'ed here will prob change
  input [7:0]   VR;		// volume right
  input [7:0]	VL;		// volume left
  input [15:0]	AiDR;		// AiD right sample
  input [15:0]  AiDL;		// AiD left sample

  // Inputs, control
  input 	freq;		// 0=32khz, 1=48khz
  input [5:0]	newest;		// newest sample
  input [5:0]	oldest;		// oldest sample
  input 	go;		// start
  input [1:0]	mode;		// 0=none, 1=conv, 2=vol_mult, 3=dsp_add
  input		LR;
  input[1:0]	period;
  input		AiLRNeg_ioclk;
  input		clear;		// clear the mac result

  // Internal data path signals
  wire[30:0]	MAC_result;
  wire		MAC_done;
  wire[15:0]	MAC_op0;
  wire[15:0]	MAC_op1;	// coeff resolution depends on this
  wire		MAC_cin;
  wire		sign_ext;
  reg[15:0]	Op0;
  reg[15:0]	Op1;

  // Internal control signals
  wire		MAC_add;
  wire		MAC_start;
  wire[2:0]	Select_op0;
  wire[6:0]	Select_op1;
  wire		temp_reg_clear;
  wire		temp_reg_enable;
  wire		Accum_enable;
  wire[1:0]	Accum_select;
  wire		Accum_clear;
  wire		Accum_clamp;

  // Internal registers
  reg[47:0]	Accum;
  reg[30:0]	temp_reg;
  reg[15:0]	Result_int;
  reg		chk_over_under;

  // coefficients for a 128 tap FIR filter
  // only 64 because filter is symmetric

// New fir coefficients for 128 tap filter with a multiplier of 2.995 and 
// bits tweaked. Here are some stats:
//With i=0, the sum of every third fir coefficient = 1.000000000000010
//With i=1, the sum of every third fir coefficient = 1.000000000000010
//With i=2, the sum of every third fir coefficient = 0.999999999999960
//WARNING: with i=0, the sum of the absolute value of every third fir coefficient = 1.822326660156250
//WARNING: with i=1, the sum of the absolute value of every third fir coefficient = 1.822326660156250
//WARNING: with i=2, the sum of the absolute value of every third fir coefficient = 2.585083007812480
// (1.000000000000010 is actually one because we are only 
// precise enough to represent 0.000030517578125). 
`define FIR_COEFF0 16'h0000
`define FIR_COEFF1 16'h0000
`define FIR_COEFF2 16'h0001
`define FIR_COEFF3 16'h0005
`define FIR_COEFF4 16'h000C
`define FIR_COEFF5 16'h0016
`define FIR_COEFF6 16'h0023
`define FIR_COEFF7 16'h0025
`define FIR_COEFF8 16'h0014
`define FIR_COEFF9 16'hFFF0
`define FIR_COEFF10 16'hFFCE
`define FIR_COEFF11 16'hFFCD
`define FIR_COEFF12 16'hFFF6
`define FIR_COEFF13 16'h0031
`define FIR_COEFF14 16'h004B
`define FIR_COEFF15 16'h0022
`define FIR_COEFF16 16'hFFCF
`define FIR_COEFF17 16'hFF9D
`define FIR_COEFF18 16'hFFC6
`define FIR_COEFF19 16'h0034
`define FIR_COEFF20 16'h0081
`define FIR_COEFF21 16'h0054
`define FIR_COEFF22 16'hFFC3
`define FIR_COEFF23 16'hFF56
`define FIR_COEFF24 16'hFF8C
`define FIR_COEFF25 16'h004D
`define FIR_COEFF26 16'h00E1
`define FIR_COEFF27 16'h009C
`define FIR_COEFF28 16'hFF9C
`define FIR_COEFF29 16'hFED6
`define FIR_COEFF30 16'hFF31
`define FIR_COEFF31 16'h0084
`define FIR_COEFF32 16'h0189
`define FIR_COEFF33 16'h010E
`define FIR_COEFF34 16'hFF50
`define FIR_COEFF35 16'hFDFE
`define FIR_COEFF36 16'hFEA4
`define FIR_COEFF37 16'h00EB
`define FIR_COEFF38 16'h029E
`define FIR_COEFF39 16'h01BF
`define FIR_COEFF40 16'hFEC8
`define FIR_COEFF41 16'hFC9B
`define FIR_COEFF42 16'hFDC6
`define FIR_COEFF43 16'h019F
`define FIR_COEFF44 16'h0468
`define FIR_COEFF45 16'h02D6
`define FIR_COEFF46 16'hFDD1
`define FIR_COEFF47 16'hFA3D
`define FIR_COEFF48 16'hFC5B
`define FIR_COEFF49 16'h02F7
`define FIR_COEFF50 16'h07A6
`define FIR_COEFF51 16'h04C4
`define FIR_COEFF52 16'hFBD8
`define FIR_COEFF53 16'hF57C
`define FIR_COEFF54 16'hF980
`define FIR_COEFF55 16'h0620
`define FIR_COEFF56 16'h0F75
`define FIR_COEFF57 16'h09A5
`define FIR_COEFF58 16'hF5D3
`define FIR_COEFF59 16'hE55F
`define FIR_COEFF60 16'hEE21
`define FIR_COEFF61 16'h17A4
`define FIR_COEFF62 16'h5137
`define FIR_COEFF63 16'h7AAF

assign MAC_op0 = Op0;
assign MAC_op1 = Op1;

  // Mux for MAC_op0 select
  always @(AisData or VR or VL or AiDR or AiDL or Select_op0 or temp_reg)
    case(Select_op0)
      3'b000: Op0 = AisData;
// originally forgot to do sign extension of temp register correctly. 
// That's why things are out of order
      3'b001: Op0 = {16{temp_reg[30]}};	// sign extension of temp register
					// so we do the third add correctly
      // Volume is only 8 bits so play some tricks here
      3'b010: Op0 = |VL ? {1'b0, VL, 7'b1111111} : 16'h0000;
      3'b011: Op0 = |VR ? {1'b0, VR, 7'b1111111} : 16'h0000;
      3'b100: Op0 = AiDR;
      3'b101: Op0 = AiDL;
      3'b110: Op0 = temp_reg[15:0];
      3'b111: Op0 = {temp_reg[30], temp_reg[30:16]};	// sign extention
      default: Op0 = 16'hxxxx;
    endcase  

  // Mux for MAC_op1 select
  always @ (Select_op1 or AisData or Accum)
    case(Select_op1)
      7'b0000000: Op1 = `FIR_COEFF0;	// also coeff 127
      7'b0000001: Op1 = `FIR_COEFF1;	// also coeff 126
      7'b0000010: Op1 = `FIR_COEFF2;	// also coeff 125
      7'b0000011: Op1 = `FIR_COEFF3;	// also coeff 124
      7'b0000100: Op1 = `FIR_COEFF4;	// also coeff 123
      7'b0000101: Op1 = `FIR_COEFF5;	// also coeff 122
      7'b0000110: Op1 = `FIR_COEFF6;	// also coeff 121
      7'b0000111: Op1 = `FIR_COEFF7;	// also coeff 120
      7'b0001000: Op1 = `FIR_COEFF8;	// also coeff 119
      7'b0001001: Op1 = `FIR_COEFF9;	// also coeff 118
      7'b0001010: Op1 = `FIR_COEFF10;	// also coeff 117
      7'b0001011: Op1 = `FIR_COEFF11;	// also coeff 116
      7'b0001100: Op1 = `FIR_COEFF12;	// also coeff 115
      7'b0001101: Op1 = `FIR_COEFF13;	// also coeff 115
      7'b0001110: Op1 = `FIR_COEFF14;	// also coeff 113
      7'b0001111: Op1 = `FIR_COEFF15;	// also coeff 112
      7'b0010000: Op1 = `FIR_COEFF16;	// also coeff 111
      7'b0010001: Op1 = `FIR_COEFF17;	// also coeff 110
      7'b0010010: Op1 = `FIR_COEFF18;	// also coeff 109
      7'b0010011: Op1 = `FIR_COEFF19;	// also coeff 108
      7'b0010100: Op1 = `FIR_COEFF20;	// also coeff 107
      7'b0010101: Op1 = `FIR_COEFF21;	// also coeff 106
      7'b0010110: Op1 = `FIR_COEFF22;	// also coeff 105
      7'b0010111: Op1 = `FIR_COEFF23;	// also coeff 104
      7'b0011000: Op1 = `FIR_COEFF24;	// also coeff 102
      7'b0011001: Op1 = `FIR_COEFF25;	// also coeff 102
      7'b0011010: Op1 = `FIR_COEFF26;	// also coeff 101
      7'b0011011: Op1 = `FIR_COEFF27;	// also coeff 100
      7'b0011100: Op1 = `FIR_COEFF28;	// also coeff 99
      7'b0011101: Op1 = `FIR_COEFF29;	// also coeff 98
      7'b0011110: Op1 = `FIR_COEFF30;	// also coeff 97
      7'b0011111: Op1 = `FIR_COEFF31;	// also coeff 96
      7'b0100000: Op1 = `FIR_COEFF32;	// also coeff 95
      7'b0100001: Op1 = `FIR_COEFF33;	// also coeff 94
      7'b0100010: Op1 = `FIR_COEFF34;	// also coeff 93
      7'b0100011: Op1 = `FIR_COEFF35;	// also coeff 92
      7'b0100100: Op1 = `FIR_COEFF36;	// also coeff 91
      7'b0100101: Op1 = `FIR_COEFF37;	// also coeff 90
      7'b0100110: Op1 = `FIR_COEFF38;	// also coeff 89
      7'b0100111: Op1 = `FIR_COEFF39;	// also coeff 88
      7'b0101000: Op1 = `FIR_COEFF40;	// also coeff 87
      7'b0101001: Op1 = `FIR_COEFF41;	// also coeff 86
      7'b0101010: Op1 = `FIR_COEFF42;	// also coeff 85
      7'b0101011: Op1 = `FIR_COEFF43;	// also coeff 84
      7'b0101100: Op1 = `FIR_COEFF44;	// also coeff 83
      7'b0101101: Op1 = `FIR_COEFF45;	// also coeff 82
      7'b0101110: Op1 = `FIR_COEFF46;	// also coeff 81
      7'b0101111: Op1 = `FIR_COEFF47;	// also coeff 80
      7'b0110000: Op1 = `FIR_COEFF48;	// also coeff 79
      7'b0110001: Op1 = `FIR_COEFF49;	// also coeff 78
      7'b0110010: Op1 = `FIR_COEFF50;	// also coeff 77
      7'b0110011: Op1 = `FIR_COEFF51;	// also coeff 76
      7'b0110100: Op1 = `FIR_COEFF52;	// also coeff 75
      7'b0110101: Op1 = `FIR_COEFF53;	// also coeff 74
      7'b0110110: Op1 = `FIR_COEFF54;	// also coeff 73
      7'b0110111: Op1 = `FIR_COEFF55;	// also coeff 72
      7'b0111000: Op1 = `FIR_COEFF56;	// also coeff 71
      7'b0111001: Op1 = `FIR_COEFF57;	// also coeff 70
      7'b0111010: Op1 = `FIR_COEFF58;	// also coeff 69
      7'b0111011: Op1 = `FIR_COEFF59;	// also coeff 68
      7'b0111100: Op1 = `FIR_COEFF60;	// also coeff 67
      7'b0111101: Op1 = `FIR_COEFF61;	// also coeff 66
      7'b0111110: Op1 = `FIR_COEFF62;	// also coeff 65
      7'b0111111: Op1 = `FIR_COEFF63;	// also coeff 64
      7'b1000000: Op1 = AisData;
      7'b1000001: Op1 = Accum[15:0];
      7'b1000010: Op1 = Accum[31:16];
      7'b1000011: Op1 = Accum[47:32];
      7'b1000100: Op1 = Accum[30:15];
      default: Op1 = 16'hxxxx;
    endcase

// Accum register management
always @(posedge io_clk)
  begin
    if(Accum_clear)
      Accum <= 0;
    else
      if(Accum_enable)
        case(Accum_select)
          2'b00:			// First Mult result 
            begin
              Accum[30:0] <= MAC_result[30:0];
	      Accum[47:31] <= 	{		// sign extension	
		  		MAC_result[30], MAC_result[30], 
				MAC_result[30], MAC_result[30], 
				MAC_result[30], MAC_result[30], 
				MAC_result[30], MAC_result[30], 
				MAC_result[30], MAC_result[30], 
				MAC_result[30], MAC_result[30], 
				MAC_result[30], MAC_result[30], 
				MAC_result[30], MAC_result[30], 
				MAC_result[30]
				};
            end
	  2'b01: Accum[15:0]  <= MAC_result[15:0];	// First Add result
	  2'b10: Accum[31:16] <= MAC_result[15:0];	// Second Add result
	  2'b11:
	    begin
	      if(Accum_clamp)
	        begin 
		  // check to see if we have all zeros or all ones
		  // in the top most bits
	          if(~((|{MAC_result[15:0], Accum[31:30]}==0) || (&{MAC_result[15:0], Accum[31:30]}==1))) // overflow or underflow
	            begin
		      // if not then check the sign bit to see if the 
		      // number is positive or negative
		      // Since based on our coefficients we know
		      // that the max value cannot exceed 2.5, the
		      // 32nd bit of the accum is the sign bit,
		      // which is the same as the 0th bit of the
		      // last MAC addition.
	              if(MAC_result[0]) //underflow, clamp to 0x8000
	                Accum <= 48'hffff40000000;
	              else
		        // overflow, clamp to 7fff
	                Accum <= 48'h00003fff8000;
	            end
	          else		
	            Accum[47:32] <= MAC_result[15:0];  // Final Add result
	        end
	      else
		Accum[47:32] <= MAC_result[15:0];  // Final Add result
	    end
	  default: 
            Accum <= 0;
        endcase
  end
 
// temp register management
always @(posedge io_clk)
  begin
    if(temp_reg_clear)
      temp_reg <= 0;
    else if(temp_reg_enable)
      temp_reg <= MAC_result;
  end

// check for underflow and overflow cases and clamp accordingly
// otherwise then just spit out the result of the DSP add
always @(posedge io_clk)
begin
  if(~resetb)
   begin
    Result_int <= 16'b0000000000000000;
    chk_over_under <= 0;
   end
  else
   begin
    if(MAC_add && MAC_start)
	chk_over_under <= ~(MAC_op0[15] ^ MAC_op1[15]);
    Result_int <= chk_over_under ? 
			         (MAC_result[16] ^ MAC_result[15]) ? MAC_result[16] ? {1'b1, 15'b000000000000000} // under
					                                            : {1'b0, 15'b111111111111111} // over
			   	                                   : MAC_result[15:0] 
			   	 : MAC_result[15:0];
   end
end

assign Result = Result_int;

// SRC Control State Machine
io_AiSRC_fsm io_AiSRC_fsm	(
				.read_addr(read_addr),
				.MAC_add(MAC_add),
				.MAC_start(MAC_start),
				.MAC_cin(MAC_cin),
				.Select_op0(Select_op0),
				.Select_op1(Select_op1),
				.temp_reg_clear(temp_reg_clear),
				.temp_reg_enable(temp_reg_enable),
				.Accum_enable(Accum_enable),
				.Accum_select(Accum_select),
				.Accum_clear(Accum_clear),
				.Done(Done),
				.sign_ext(sign_ext),
				.Accum_clamp(Accum_clamp),
				.read_enable(read_enable),
				.io_clk(io_clk),
				.resetb(resetb),
				.newest(newest),
				.oldest(oldest),
				.go(go),
				.mode(mode),
				.MAC_done(MAC_done),
				.MAC_cout(MAC_result[16]),
				.freq(freq),
				.LR(LR),
				.period(period),
				.AiLRNeg_ioclk(AiLRNeg_ioclk)
				);

// Multiply/Accumulate module
io_AiMAC16x16 io_AiMAC16x16 	(
				.Result(MAC_result),
				.Done(MAC_done),
				.Op0(MAC_op0),
				.Op1(MAC_op1),
				.Add(MAC_add),
				.Start(MAC_start),
				.resetb(resetb),
				.io_clk(io_clk),
				.Cin(MAC_cin),
				.sign_ext(sign_ext),
				.clear(clear)		
				);


endmodule 