/*******************************************************************************
 * 
 *   (C) 1999 ARTX INC..  ALL RIGHTS RESERVED.  UNPUBLISHED -- RIGHTS
 *   RESERVED UNDER THE COPYRIGHT LAWS OF THE UNITED STATES.  USE OF A
 *   COPYRIGHT NOTICE IS PRECAUTIONARY ONLY AND DOES NOT IMPLY PUBLICATION
 *   OR DISCLOSURE.
 *
 *   THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF
 *   ARTX INC..  USE, DISCLOSURE, OR REPRODUCTION IS PROHIBITED WITHOUT
 *   THE PRIOR EXPRESS WRITTEN PERMISSION OF ARTX INC..
 *
 *                   RESTRICTED RIGHTS LEGEND
 *
 *   Use, duplication, or disclosure by the Government is subject to
 *   restrictions as set forth in subparagraph (c)(1)(ii) of the Rights
 *   in Technical Data and Computer Software clause at DFARS 252.227-7013
 *   or subparagraphs (c)(1) and (2) of Commercial Computer Software --
 *   Restricted Rights at 48 CFR 52.227-19, as applicable. 
 *
 *   ArtX Inc.
 *   3400 Hillview Ave, Bldg 5
 *   Palo Alto, CA 94304
 *
 ******************************************************************************/

// AI Clocks Generator (Ai clock and Ais clock)
module io_AiClks (/*AUTOARG*/
   // Outputs
   AiClk, AiClkNeg, AiLR,
   AisClk, AisClkPos, AisClkNeg, AisLR, AisLR_32khz, AisClk_32khz,
   // Inputs
   io_VidClk, resetb, AisClkEnable, Ais48KHz
   );

// syn myclk = io_VidClk

   //////////////////////
   // I/O Declarations //
   //////////////////////

   // General
   input io_VidClk;
   input resetb;
   // Inputs
   input 	 AisClkEnable;   // Enable AIS Clock
   input 	 Ais48KHz;       // Set AIS to 48KHz clock or 32KHz clock
   // Outputs
   output 	 AiClk;      // Ai Clk output to pins (AI bit clock)
   output 	 AiClkNeg;   // Ai Clk negative edge
   output 	 AiLR;       // Ai LR clock
   output 	 AisClk;     // Ais Clk output to pins (AIS bit clock)
   output 	 AisClkPos;  // Ais Clk positive edge
   output 	 AisClkNeg;  // Ais Clk negative edge
   output 	 AisLR;      // Ais LR clock
   output	 AisLR_32khz; // AisLR signal that's always 32khz
   output	 AisClk_32khz; // AisClk signal that's always 32khz

`define AI_LOOP0 8'd8
`define AI_LOOP1 8'd7
`define AI_BITCOUNT_END   4'd15
`define AI_BITCOUNT_START 4'd0
`define AIS_LOOP0 8'd12
`define AIS_LOOP1 8'd13
`define AIS_BITCOUNT_END   4'd15
`define AIS_BITCOUNT_START 4'd0
`define AI_CHANGE  4'd8
`define AIS_CHANGE 4'd10
 
   ///////////////////
   // Internal Vars //
   ///////////////////
   reg [7:0]    AiDivider;
   reg [7:0] 	AiLoop;
   reg 		e1AiClk;       // Register outputs to pins
   reg [3:0] 	AiBitCount;    // Ai current bit count

   reg [7:0]    AisDivider;
   reg [7:0] 	AisLoop;
   reg 		AisClk;
   reg 		AisLRMux;
   reg [1:0] 	AisLRMuxSel;   // Select for above mux
   reg [1:0] 	AisLRMuxSelIn;   // Input to above register
   reg 		e1AisClk;       // Register outputs to pins
   reg [3:0] 	AisBitCount;    // Ais current bit count
   reg 		AisClkMux;      // Ais clock mux (e1AisClk, e1AiClk, 0)
   reg [1:0] 	AisClkMuxSel;   // Select for above mux
   reg [1:0] 	AisClkMuxSelIn; // Input to above register
   reg 		d1Ais48KHz;     // Delayed version of Ais clock control input

   // internal signals for outputs
   reg 		AiClk_int;
   reg 		AiLR_int;
   reg 		AisLR_int_48;
   reg 		AiClkPos_int;
   reg 		AiClkNeg_int;
   reg 		AisLR_int;
   reg 		AisClkPos_int;
   reg 		AisClkNeg_int;
   reg		AisLR_enable;
   reg		AisLR_enable_int;

   reg		AiClkNeg_out;
   reg		AisClkPos_out;
   reg		AisClkNeg_out;
   reg		AisLR_32khz_int;
   reg		AisClk_32khz_int;

//   wire		Ais48KHz_latch;
   reg		Ais48KHz_latch;

// Defines for AisClkMuxSel   
`define AIS_32KHZ_SEL 2'b00
`define AIS_48KHZ_SEL 2'b01
`define AIS_0_SEL     2'b10
  
// Output assignments
assign AiClk = AiClk_int;
assign AiLR = AiLR_int;
assign AisLR = AisLR_enable ? AisLR_int : 0;
assign AisLR_32khz = AisLR_32khz_int;
assign AisClk_32khz = AisClk_32khz_int;

always @(posedge io_VidClk)
begin
  if(~resetb)
    begin
      AiClkNeg_out <= 0;
      AisClkPos_out <= 0;
      AisClkNeg_out <= 0;
    end
  else
    begin
      AiClkNeg_out <= AiClkNeg_int;
      AisClkPos_out <= AisClkMuxSel[1] ? 0 : AisClkMuxSel[0] ? AiClkPos_int : AisClkPos_int;
      AisClkNeg_out <=AisClkMuxSel[1] ? 0 : AisClkMuxSel[0] ? AiClkNeg_int : AisClkNeg_int;
    end
end

assign AiClkNeg = AiClkNeg_out;
assign AisClkPos = AisClkPos_out;
assign AisClkNeg = AisClkNeg_out;

   wire AisLRIn_32khz = ~resetb ? 1 : AisClkPos_int && (AisBitCount == `AIS_BITCOUNT_END) ? ~AisLR_32khz_int : AisLR_32khz_int;
   wire AisLRIn_32khzPos = ~resetb ? 1 : ~AisLR_32khz_int && AisClkPos_int && (AisBitCount == `AIS_BITCOUNT_END) ? 1 : 0;
   wire AisLRIn_32khzNeg = ~resetb ? 1 : AisLR_32khz_int && AisClkPos_int && (AisBitCount == `AIS_BITCOUNT_END) ? 1 : 0;

   wire AisLRIn_48 = ~resetb ? 1 : AiClkPos_int && (AiBitCount == `AI_BITCOUNT_END) ? ~AisLR_int_48 : AisLR_int_48;
   wire AisLRInPos_48 = ~resetb ? 1 : ~AisLR_int_48 && AiClkPos_int && (AiBitCount == `AIS_BITCOUNT_END) ? 1 : 0;
   wire AisLRInNeg_48 = ~resetb ? 1 : AisLR_int_48 && AiClkPos_int && (AiBitCount == `AIS_BITCOUNT_END) ? 1 : 0;

// Endpoint for Ai counter   
always @(AiBitCount or e1AiClk)
  if (~e1AiClk &&  // Loop count changes when on last three bits and doing low clock pulse
      (AiBitCount > `AI_CHANGE))
    AiLoop = `AI_LOOP1;
  else
    AiLoop = `AI_LOOP0;

   wire AiLRIn = ~resetb ? 1 : AiClkNeg_int && (AiBitCount == `AI_BITCOUNT_START) ? ~AiLR_int : AiLR_int;

   wire AiLRInNeg = ~resetb ? 1 : AiLR_int && AiClkNeg_int && (AiBitCount == `AI_BITCOUNT_START) ? 1 : 0;

always @(posedge io_VidClk)
begin
  if(~resetb)
    AisLR_enable <= 0;
  else
    if(~Ais48KHz_latch)
      begin
        if(AisLRIn_32khzPos && AisClkEnable && AisLR_enable_int)
    	  AisLR_enable <= 1;
        else
	  if(~AisClkEnable && (AisLRIn_32khzNeg || AisLRIn_32khzPos) && AisLR_enable_int)
      	    AisLR_enable <= 0;
      end
    else
      begin
        if(AisLRInPos_48 && AisClkEnable && AisLR_enable_int)
    	  AisLR_enable <= 1;
        else
	  if(~AisClkEnable && (AisLRInNeg_48 || AisLRInPos_48) && AisLR_enable_int)
      	    AisLR_enable <= 0;
      end
end
   
// Ai Bit counter and AiLR clock
always @(posedge io_VidClk)
  begin
   if(~resetb)
     begin
	    AiBitCount <= 0;
	    AiLR_int   <= 0;
	    AisLR_int_48   <= 0;
     end
   else    
     begin
	    AiBitCount <= AiClkPos_int ? AiBitCount + 1 : AiBitCount;
	    AiLR_int   <= AiLRIn;
	    AisLR_int_48   <= AisLRIn_48;
     end   
  end

   ////////////////////////////
   // AI 48KHz Clock Divider //
   ////////////////////////////
   always @( posedge io_VidClk)
     begin
	if (~resetb)
	  begin
	     e1AiClk   <= 1'b0;
	     AiDivider <= 8'd0;
	     AiClkPos_int  <= 1'b0;
	     AiClkNeg_int  <= 1'b0;
	  end // if (~resetb)
	else if (AiDivider == AiLoop) // Reach end toggle clock
	  begin
	     e1AiClk  <= ~e1AiClk;  // Toggle clock
	     AiDivider <= 8'd0;
	     if (AiClk_int)
	       begin // Negative Edge
		  AiClkNeg_int <= 1'b1;
		  AiClkPos_int <= 1'b0;
	       end // if (AiClk_int)
	     else
	       begin // Positive Edge
		  AiClkNeg_int <= 1'b0;
		  AiClkPos_int <= 1'b1;
	       end // else: !if(AiClk_int)
	  end // if (AiDivider == AiLoop)
	else
	  begin
	     e1AiClk   <= e1AiClk;
	     AiDivider <= AiDivider + 1;
	     AiClkNeg_int  <= 1'b0;
	     AiClkPos_int  <= 1'b0;
	  end // else: !if(AiDivider == AiLoop)
     end // always @ ( posedge io_VidClk)

   // AIS Clock
   
   // Endpoint for Ais counter   
////   always @(AisBitCount or e1AisClk or AisLR_int)  // Loop count changes when on last four bits and doing low clock pulse
   always @(AisBitCount or e1AisClk or AisLR_32khz_int)  // Loop count changes when on last four bits and doing low clock pulse
                                      // or last five bits for right channel
////     if (~e1AisClk && ((AisBitCount > `AIS_CHANGE) || ((AisBitCount == `AIS_CHANGE) && AisLR_int)))
     if (~e1AisClk && ((AisBitCount > `AIS_CHANGE) || ((AisBitCount == `AIS_CHANGE) && AisLR_32khz_int)))
       AisLoop = `AIS_LOOP1;
     else
       AisLoop = `AIS_LOOP0;

   
   // Ais Bit counter and AisLR clock
always @(posedge io_VidClk)
begin
  if(~resetb)
     begin
	AisBitCount <= 0;
	AisLR_int   <= 0;
	AisLR_32khz_int <= 0;
     end
  else
     begin
	AisBitCount <= AisClkPos_int ? AisBitCount + 1 : AisBitCount;
	AisLR_int   <= AisLRMux;
	AisLR_32khz_int <= AisLRIn_32khz;
     end
end   
   /////////////////////////////
   // AIS 32KHZ Clock Divider //
   /////////////////////////////
   always @( posedge io_VidClk)
     begin
	if (~resetb)
	  begin
	     e1AisClk   <= 1'b0;
	     AisDivider <= 8'd0;
	     AisClkPos_int  <= 1'b0;
	     AisClkNeg_int  <= 1'b0;
	  end // if (~resetb)
	else if (AisDivider == AisLoop) // Reach end toggle clock
	  begin
	     e1AisClk  <= ~e1AisClk;  // Toggle clock
	     AisDivider <= 8'd0;
	     if (e1AisClk)
	       begin // Negative Edge
		  AisClkNeg_int <= 1'b1;
		  AisClkPos_int <= 1'b0;
	       end // if (AisClk)
	     else
	       begin // Positive Edge
		  AisClkNeg_int <= 1'b0;
		  AisClkPos_int <= 1'b1;
	       end // else: !if(AiClk)
	  end // if (AisDivider == AisLoop)
	else
	  begin
	     e1AisClk   <= e1AisClk;
	     AisDivider <= AisDivider + 1;
	     AisClkNeg_int  <= 1'b0;
	     AisClkPos_int  <= 1'b0;
	  end // else: !if(AisDivider == AisLoop)
     end // always @ ( posedge io_VidClk)   
   
   always @(posedge io_VidClk)
     if (~resetb)
       begin
	  AiClk_int  <= 0;
	  AisClk <= 0;
	  AisClk_32khz_int <= 0;
       end
     else
       begin
	  AiClk_int  <= e1AiClk;   // Output is one clock later
	  AisClk <= AisClkMux; // Output is one clock later
	  AisClk_32khz_int <= e1AisClk;
       end
   // AisClkMux
   always @(e1AiClk or e1AisClk or AisClkMuxSel)
     case (AisClkMuxSel) // syn full_case parallel_case
       `AIS_32KHZ_SEL: AisClkMux = e1AisClk;
       `AIS_48KHZ_SEL: AisClkMux = e1AiClk;
       `AIS_0_SEL:     AisClkMux = 0;
       default:       AisClkMux = e1AisClk;
     endcase // case(AisClkMuxSel)

   // AisLRMux
   always @(AisLRIn_32khz or AisLRMuxSel or AisLRIn_48)
     case (AisLRMuxSel)
       `AIS_32KHZ_SEL: AisLRMux = AisLRIn_32khz;
       `AIS_48KHZ_SEL: AisLRMux = AisLRIn_48;
       `AIS_0_SEL:     AisLRMux = 0;
       default:       AisLRMux = AisLRIn_32khz;
     endcase // case(AisLRMuxSel)

   // Delayed version of AisClk32KHz, so we can check for change
always @(posedge io_VidClk)
 begin
  if (~resetb)
     begin
     	d1Ais48KHz <= 0;
	AisClkMuxSel  <= `AIS_32KHZ_SEL;
	AisLRMuxSel <= `AIS_32KHZ_SEL;
	Ais48KHz_latch <= 0;
     end
  else
     begin
     	d1Ais48KHz <= Ais48KHz;
	AisClkMuxSel  <= AisClkMuxSelIn; // Reset to 32KHz, set by SM below
	AisLRMuxSel  <= AisLRMuxSelIn; // Reset to 32KHz, set by SM below
	if(~AisLR_enable_int)
	   Ais48KHz_latch <= Ais48KHz;
	else
	   Ais48KHz_latch <= Ais48KHz_latch;
     end
 end	    

//assign Ais48KHz_latch = ~resetb ? 0 : ~AisLR_enable_int ? Ais48KHz : Ais48KHz_latch;


   /////////////////////////
   // Change AIS Clock SM //
   /////////////////////////
     
   // Cleanly switch from 32KHz to 48KHz with no runt pulse on clocks
     
   parameter [2:0]  //cisco_fsm AiClk declare
                    AI_IDLE    = 3'b000,
                    AI_32KHZ_0 = 3'b001,
                    AI_32KHZ_1 = 3'b010,
                    AI_48KHZ_0 = 3'b011,
                    AI_48KHZ_1 = 3'b100,
                    AI_XXX     = 3'bxxx;

   reg [2:0]        AiClkState;  // cisco_fsm AiClk current_state 
   reg [2:0]        AiClkStateN; // cisco_fsm AiClk next_state
   reg  freq_flag;
   reg  freq_flag_d1;

always @(posedge io_VidClk)
begin
  if(~resetb)
    freq_flag_d1 <= 0;
  else
    freq_flag_d1 <= freq_flag;
end

   always @(AiClkState or d1Ais48KHz or Ais48KHz or AisClkMuxSel or AiClkNeg_int or AisClkNeg_int or freq_flag_d1 or Ais48KHz_latch)
     begin
        // Set default outputs
        AisClkMuxSelIn = AisClkMuxSel; // Default is no change
        freq_flag = 0;
        case (AiClkState) // syn parallel_case full_case
          AI_IDLE: // Idle, no freq change requested
            begin
               freq_flag = freq_flag_d1;
               if ((d1Ais48KHz && ~ Ais48KHz) || (freq_flag_d1 && ~Ais48KHz))  
   // 48KHz -> 32KHz
                 AiClkStateN = AI_32KHZ_0;
               else if ((~d1Ais48KHz && Ais48KHz) || (~freq_flag_d1 && Ais48KHz)) // 32KHz -> 48KHz
                 AiClkStateN = AI_48KHZ_0;
               else
                 AiClkStateN = AI_IDLE;
            end
          AI_32KHZ_0: // Wait for NegEdge of AI Clk
            begin
               freq_flag = 0;
               if (AiClkNeg_int && ~Ais48KHz_latch)
                 AiClkStateN = AI_32KHZ_1;
               else
                 AiClkStateN = AI_32KHZ_0;
            end
          AI_32KHZ_1: // Wait for NegEdge of AIS Clk
            begin
               freq_flag = 0;
               if (AisClkNeg_int)
                 begin
                    AiClkStateN = AI_IDLE;           // Done with change
                    AisClkMuxSelIn = `AIS_32KHZ_SEL; // Set output to 32KHz clock
                 end
               else
                 begin
                    AiClkStateN = AI_32KHZ_1;
                    AisClkMuxSelIn = `AIS_0_SEL; // Set output to zero, no runt pulses
                 end
            end // case: AI_32KHZ_1
          AI_48KHZ_0: // Wait for NegEdge of AIS Clk
            begin
               freq_flag = 1;
               if (AisClkNeg_int && Ais48KHz_latch)
                 AiClkStateN = AI_48KHZ_1;
               else
                 AiClkStateN = AI_48KHZ_0;
            end
          AI_48KHZ_1: // Wait for NegEdge of AI Clk
            begin
               freq_flag = 1;
               if (AiClkNeg_int)
                 begin
                    AiClkStateN = AI_IDLE;           // Done with change
                    AisClkMuxSelIn = `AIS_48KHZ_SEL; // Set output to 48KHz clock
                 end
               else
                 begin
                    AiClkStateN = AI_48KHZ_1;
                    AisClkMuxSelIn = `AIS_0_SEL; // Set output to zero, no runt pulses
                 end
            end // case: AI_48KHZ_1
          default: AiClkStateN = AI_XXX;
        endcase // case(AiClkState)
     end // always @ (AiClkState or d1Ais48KHz or Ais48KHz)

   // Register State
   always @(posedge io_VidClk)
     if (~resetb)
       AiClkState <= AI_IDLE;
     else
       AiClkState <= AiClkStateN;
   


   /////////////////////////
   // Change AISLR     SM //
   /////////////////////////
     
   // Cleanly switch from 32KHz to 48KHz with no runt pulse on clocks
     
   parameter [2:0]  //cisco_fsm AiLR declare
		    AILR_IDLE    = 3'b000,
		    AILR_32KHZ_0 = 3'b001,
		    AILR_32KHZ_1 = 3'b010,
		    AILR_48KHZ_0 = 3'b011,
		    AILR_48KHZ_1 = 3'b100,
		    AILR_XXX     = 3'bxxx;

   reg [2:0] 	    AiLRState;  // cisco_fsm AiLR current_state 
   reg [2:0] 	    AiLRStateN; // cisco_fsm AiLR next_state
   reg  freq_flag_LR;
   reg  freq_flag_LR_d1;

always @(posedge io_VidClk)
begin
  if(~resetb)
    freq_flag_LR_d1 <= 0;
  else
    freq_flag_LR_d1 <= freq_flag_LR;
end


   always @(AiLRState or d1Ais48KHz or Ais48KHz or AisLRMuxSel or AisLRInNeg_48 or AisLRIn_32khzNeg or freq_flag_LR_d1)
     begin
      	// Set default outputs
      	AisLRMuxSelIn = AisLRMuxSel; // Default is no change
	freq_flag_LR = 0;
        AisLR_enable_int = 0;
      	case (AiLRState) // syn parallel_case full_case
	  AILR_IDLE: // Idle, no freq change requested
	    begin
	       freq_flag_LR = freq_flag_LR_d1;
	       AisLR_enable_int = 1;
	       if ((d1Ais48KHz && ~ Ais48KHz) || (freq_flag_LR_d1 && ~Ais48KHz))     // 48KHz -> 32KHz
		 AiLRStateN = AILR_32KHZ_0;
	       else if ((~d1Ais48KHz && Ais48KHz) || (~freq_flag_LR_d1 && Ais48KHz)) // 32KHz -> 48KHz
		 AiLRStateN = AILR_48KHZ_0;
	       else
		 AiLRStateN = AILR_IDLE;
	    end
	  AILR_32KHZ_0: // Wait for NegEdge of AI LR
	    begin
	       freq_flag_LR = 0;
	       AisLR_enable_int = 1;
	       if (AisLRInNeg_48)
		 AiLRStateN = AILR_32KHZ_1;
	       else
		 AiLRStateN = AILR_32KHZ_0;
	    end
	  AILR_32KHZ_1: // Wait for NegEdge of AIS LR
	    begin
	       freq_flag_LR = 0;
	       AisLR_enable_int = 0;
	       if (AisLRIn_32khzNeg)
		 begin
		    AiLRStateN = AILR_IDLE;           // Done with change
		    AisLRMuxSelIn = `AIS_32KHZ_SEL; // Set output to 32KHz clock
		 end
	       else
		 begin
		    AiLRStateN = AILR_32KHZ_1;
		    AisLRMuxSelIn = `AIS_0_SEL; // Set output to zero, no runt pulses
		 end
	    end // case: AILR_32KHZ_1
	  AILR_48KHZ_0: // Wait for NegEdge of AIS LR
	    begin
	       freq_flag_LR = 1;
	       AisLR_enable_int = 1;
	       if (AisLRIn_32khzNeg)
		 AiLRStateN = AILR_48KHZ_1;
	       else
		 AiLRStateN = AILR_48KHZ_0;
	    end
	  AILR_48KHZ_1: // Wait for NegEdge of AI LR
	    begin
	       freq_flag_LR = 1;
	       AisLR_enable_int = 0;
	       if (AisLRInNeg_48)
		 begin
		    AiLRStateN = AILR_IDLE;           // Done with change
		    AisLRMuxSelIn = `AIS_48KHZ_SEL; // Set output to 48KHz clock
		 end
	       else
		 begin
		    AiLRStateN = AILR_48KHZ_1;
		    AisLRMuxSelIn = `AIS_0_SEL; // Set output to zero, no runt pulses
		 end
	    end // case: AILR_48KHZ_1
	  default: AiLRStateN = AILR_XXX;
	endcase // case(AiLRState)
     end // always @ (AiLRState or d1Ais48KHz or Ais48KHz)

   // Register State
   always @(posedge io_VidClk)
     if (~resetb)
       AiLRState <= AILR_IDLE;
     else
       AiLRState <= AiLRStateN;
   


endmodule // io_AiClks
