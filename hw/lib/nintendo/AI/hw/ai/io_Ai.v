/*******************************************************************************
 * 
 *   (C) 1999 ARTX INC..  ALL RIGHTS RESERVED.  UNPUBLISHED -- RIGHTS
 *   RESERVED UNDER THE COPYRIGHT LAWS OF THE UNITED STATES.  USE OF A
 *   COPYRIGHT NOTICE IS PRECAUTIONARY ONLY AND DOES NOT IMPLY PUBLICATION
 *   OR DISCLOSURE.
 *
 *   THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF
 *   ARTX INC..  USE, DISCLOSURE, OR REPRODUCTION IS PROHIBITED WITHOUT
 *   THE PRIOR EXPRESS WRITTEN PERMISSION OF ARTX INC..
 *
 *                   RESTRICTED RIGHTS LEGEND
 *
 *   Use, duplication, or disclosure by the Government is subject to
 *   restrictions as set forth in subparagraph (c)(1)(ii) of the Rights
 *   in Technical Data and Computer Software clause at DFARS 252.227-7013
 *   or subparagraphs (c)(1) and (2) of Commercial Computer Software --
 *   Restricted Rights at 48 CFR 52.227-19, as applicable. 
 *
 *   ArtX Inc.
 *   3400 Hillview Ave, Bldg 5
 *   Palo Alto, CA 94304
 *
 ******************************************************************************/

`define N	7'd128	// the number of FIR filter taps.
`define N_width 7
`define K	7'd43	// the number of input samples needed for src
`define K_width 7
`define K_minus1 7'd42

module io_Ai (/*AUTOARG*/
   // Outputs
   io_dspPop, AiPiData, ai_piInt, aid, ailr, aiclk, aislr, aisclk, 
   BistErr, dsp32, AiLR_out, AiLRPos, AiLRNeg, AiClkNeg_out, 
   resetb_sync_out, AisLR_32khz, AisLR_32khzPos, AisLR_32khzNeg,
   AisClk_32khzPos, AisClk_32khzNeg, SRC_result_out, dvdaudio_mode,
   // Inputs
   io_clk, io_VidClk, resetb, dsp_ioL, dsp_ioR, PiAddr, PiLoad, 
   PiData, PiAiSel, aisd, BistEnable, BistAddr, BistData1, BistRdn, 
   BistWen96, BistCmp96
   );

// syn myclk = io_clk

   //////////////////////
   // I/O Declarations //
   //////////////////////

   // General
   input io_clk;    		// System clock
   input io_VidClk; 		// 27MHz clock
   input resetb;    		// System reset

   // from DSP_AI
   input[15:0] 	dsp_ioL;
   input[15:0] 	dsp_ioR;
   output 	io_dspPop;

   // io_pi
   input[4:1]	 PiAddr;    	// Address from io_pi
   input 	 PiLoad;    	// Load Register from io_pi
   input[15:0]   PiData;    	// Data bus in from io_pi
   output[15:0]  AiPiData;  	// Data bus out to io_pi
   input 	 PiAiSel;   	// Ai select
   output 	 ai_piInt;  	// Ai Interrupt

   // AI - Audio DAC Interface and streaming audio interface
   output 	aid;		// dac data 
   output 	ailr;		// dac left-right
   output 	aiclk;		// dac clock 
   input 	aisd;		// stream data 
   output 	aislr;		// stream left-right
   output 	aisclk;		// stream clock 

   // BIST
   input 	BistEnable;	
   input 	BistAddr;
   input 	BistData1;
   input 	BistRdn;
   input 	BistWen96;
   input 	BistCmp96;
   output 	BistErr;

   // new for revB
   output 	dsp32;
   output[15:0]	SRC_result_out;
   
   output	AiLR_out;
   output	AiLRPos;
   output	AiLRNeg;
   output	AiClkNeg_out;
   output	resetb_sync_out;
   output	AisLR_32khz;
   output	AisLR_32khzPos;
   output	AisLR_32khzNeg;
   output	AisClk_32khzPos;
   output	AisClk_32khzNeg;

   // new for revC
   output 	dvdaudio_mode;

   ///////////////////
   // Internal Vars //
   ///////////////////
   reg [15:0]   AiPiData_int; 	// Register before going external for timing
   reg [15:0] 	e1AiPiData;
   wire[15:0]	read_data;      // sample RAM output
   reg 	      	io_dspPop_int;
   reg 	      	d1io_dspPop;
   reg 	      	d2io_dspPop;
   reg[15:0] 	d1dsp_ioR;
   reg[15:0] 	d1dsp_ioL;
   reg[15:0] 	d2dsp_ioR;
   reg[15:0] 	d2dsp_ioL;
   reg[15:0]	AiDL_out;	// shift register for DAC interface
   reg[15:0]	AiDR_out;	// shift register for DAC interface
   wire		AiDL_out_load;

   wire		AiDL_out_shift;
   wire		AiDR_out_load;
   wire		AiDR_out_shift;
   
   reg		AiDL_out_load_latch;
   reg		AiDR_out_load_latch;
   reg		AiDL_out_load_reg;
   reg		AiDR_out_load_reg;

   wire		AiClk;
   wire		AisClk;
   wire		AisLR;
   wire 	AiLR;
   wire		AisClkEnable;
   wire		BistErr_int;
   wire		AisSample;
   reg		counter_val;
   wire 	afr_sync;   
   reg  	ai_piInt_int; 

   reg		bypass_d2;
   reg		PStat;

   // SRC signals
   wire[5:0]	read_addr;	// address for sample RAM
   wire		read_enable;
   wire[15:0]	SRC_result;	
   reg[15:0]	SRC_result_delay;	
   wire[5:0]	newest;
   wire[5:0]	oldest;
   wire		go;
   wire[1:0]	mode;
   wire		clear;
   wire[1:0]	period;   
   wire[15:0]	bypass_out;
   wire[1:0]	bypass;

   // signals/registers for synchronizing signals between 200Mhz
   // and 27Mhz clock domains.
   reg		AiLR_temp;
   reg		AiLRPos_ioclk;
   reg		AiLRNeg_ioclk;
   reg		AiClkNeg_ioclk;
   reg		AiClkNeg_temp;

   reg		AisLR_temp;
   reg		AisLRPos_ioclk;
   reg		AisLRNeg_ioclk;
   reg		AisClkPos_ioclk;
   reg		AisClkPos_temp;
   reg		AisClkNeg_ioclk;
   reg		AisClkNeg_temp;

   wire		AiClkNeg;
   wire		AisClkPos;
   wire		AisClkNeg;

   wire		AiLR_sync;
   wire		AiClkNeg_sync;
   wire		AisClkPos_sync;
   wire		AisLR_sync;
   wire		AisClkEnable_sync;

   wire		AisClkNeg_sync;
   reg	AiDL_out_load_temp;
   reg	AiDL_out_load_negedge;
   reg	AiDR_out_load_temp;
   reg	AiDR_out_load_negedge;

   wire		AisLR_32khz_VidClk;
   wire		AisLR_32khz_sync;
   reg		AisLR_32khz_temp;
   reg		AisLR_32khzPos_ioclk;
   reg		AisLR_32khzNeg_ioclk;

   wire		AisClk_32khz_VidClk;
   wire		AisClk_32khz_sync;
   reg		AisClk_32khz_temp;
   reg		AisClk_32khzPos_ioclk;
   reg		AisClk_32khzNeg_ioclk;

   // AI Registers
   wire [`AI_VR_TOTAL_RANGE] AiVr;     // AiVr Volume register
   wire [`AI_CR_TOTAL_RANGE] AiCr;     // AiCr Control register
   wire [`AI_SCNT_TOTAL_RANGE] AiScnt; // AiScnt Sample count register
   wire [`AI_IT_TOTAL_RANGE]   AiIt;   // AiIt Interrupt register

   
   // Sync reset
   rst_sync rst_syncAi (.clk(io_clk), .rstinB(resetb), .rstoutB(resetb_sync));
   rst_sync rst_sync_27 (.clk(io_VidClk), .rstinB(resetb), .rstoutB(resetb_sync_VidClk));

   ////////////////////////
   // AI Register Loads  //		
   ////////////////////////
   wire RegLoad = PiAiSel && PiLoad; // Load Register
   wire Lo = PiAddr[1];
   wire Hi = ~PiAddr[1];
   wire LdAiVrL = (PiAddr[4:2] == `AI_VR_IDX) && Lo && RegLoad;
   wire LdAiCrL = (PiAddr[4:2] == `AI_CR_IDX) && Lo && RegLoad;
   wire LdAiItH = (PiAddr[4:2] == `AI_IT_IDX) && Hi && RegLoad;
   wire LdAiItL = (PiAddr[4:2] == `AI_IT_IDX) && Lo && RegLoad;

   wire afr = AiCr[`AI_CR_AFR];
   
assign dsp32 = AiCr[`AI_CR_DSP32];

assign AiLR_out = AiLR_sync;
assign AiLRPos = AiLRPos_ioclk;
assign AiLRNeg = AiLRNeg_ioclk;
assign AiClkNeg_out = AiClkNeg_ioclk;
assign resetb_sync_out = resetb_sync;
assign AisLR_32khz = AisLR_32khz_sync;
assign AisLR_32khzPos = AisLR_32khzPos_ioclk;
assign AisLR_32khzNeg = AisLR_32khzNeg_ioclk;
assign AisClk_32khzPos = AisClk_32khzPos_ioclk;
assign AisClk_32khzNeg = AisClk_32khzNeg_ioclk;
assign SRC_result_out = SRC_result;


always @(posedge io_clk)
begin
  if(~resetb_sync)
    PStat <= 0;
  else
    if(AiCr[`AI_CR_PSTAT])
      PStat <= AiCr[`AI_CR_PSTAT];
    else
      if(AiLRPos_ioclk)
        PStat <= AiCr[`AI_CR_PSTAT];
end

always @(posedge io_clk)
  begin
    if(~resetb_sync)
      counter_val <= 0;
    else
      if(~AiCr[`AI_CR_PSTAT])
	counter_val <= 0;
      else
        if(counter_val == 0)
	  begin
	    if(AisLRPos_ioclk)
	      counter_val <= 1;
	  end
  end

assign AisSample = counter_val ? AisLRPos_ioclk: 0 ;

   /////////////////////////
   // Register Output Mux //
   /////////////////////////
   always @(   AiCr or AiVr or AiScnt or AiIt
	    or PiAddr)
     case (PiAddr[4:1]) // synopsys parallel_case full_case
       {`AI_CR_IDX,1'b1}: e1AiPiData = {9'd0,AiCr[6:0]};
       {`AI_VR_IDX,1'b1}: e1AiPiData = AiVr[15:0];
       {`AI_SCNT_IDX,1'b0}: e1AiPiData = AiScnt[30:16];
       {`AI_SCNT_IDX,1'b1}: e1AiPiData = AiScnt[15:0];
       {`AI_IT_IDX,1'b0}:   e1AiPiData = AiIt[31:16];
       {`AI_IT_IDX,1'b1}:   e1AiPiData = AiIt[15:0];
       default: e1AiPiData = 16'h0000;
     endcase // case(PiAddr[4:1])

always @(posedge io_clk)
  begin
    if(~resetb_sync)
      begin
	AiPiData_int 	<= 0;
	io_dspPop_int   <= 0;
	d1io_dspPop 	<= 0;
	d2io_dspPop 	<= 0;
	d1dsp_ioL 	<= 0;
	d1dsp_ioR 	<= 0;
	d2dsp_ioL 	<= 0;
	d2dsp_ioR 	<= 0;
       end
    else
      begin
	AiPiData_int <= e1AiPiData;     	// Register for timing
	io_dspPop_int   <= AiLRPos_ioclk; 	// Pop new sample from 
						// dsp, assert for only 
						// one 200 MHz cycle
	d1io_dspPop <= io_dspPop_int;   	// Delayed version used 
						// to clock in data for 
						// monitor
	d2io_dspPop <= d1io_dspPop; 		// Data is available here
	if(d2io_dspPop)
	  begin
	    d1dsp_ioL <= dsp_ioL;
	    d1dsp_ioR <= dsp_ioR;
	  end
	// after the right channel has loaded the old data, latch in the 
	// new one. AiDR_out_load_d2 is just a delayed version of 
	// AiLRNeg_ioclk
	if(AiDR_out_load)
	  begin
            d2dsp_ioL <= d1dsp_ioL;
	    d2dsp_ioR <= d1dsp_ioR;
          end
      end
   end // always @ (posedge io_clk)

  // synchronize the signals out of the AiClks block
  io_AiSync AiClkNeg_SYNC  (.D(AiClkNeg),  .CK(io_clk), .Q(AiClkNeg_sync));
  io_AiSync AiLR_SYNC      (.D(AiLR),      .CK(io_clk), .Q(AiLR_sync));
  io_AiSync AisClkPos_SYNC (.D(AisClkPos), .CK(io_clk), .Q(AisClkPos_sync));
  io_AiSync AisLR_SYNC     (.D(AisLR),	   .CK(io_clk), .Q(AisLR_sync));

  io_AiSync AisClkNeg_SYNC (.D(AisClkNeg), .CK(io_clk), .Q(AisClkNeg_sync));

  // sync signals going into the AiClks block
  io_AiSync Ais48KHz_SYNC(.D(afr),     .CK(io_VidClk), .Q(afr_sync));
  io_AiSync AisClkEnable_SYNC(.D(AisClkEnable), .CK(io_VidClk), .Q(AisClkEnable_sync));

  io_AiSync AisLR_32khz_SYNC (.D(AisLR_32khz_VidClk), .CK(io_clk), .Q(AisLR_32khz_sync));
  io_AiSync AisClk_32khz_SYNC (.D(AisClk_32khz_VidClk), .CK(io_clk), .Q(AisClk_32khz_sync));


// AiLR[Pos/Neg] signals of one io_clk cycle (instead of one Vid_Clk cycle)
always @(posedge io_clk)
  begin
    if(~resetb_sync)
      begin
        AiLR_temp <= 0;
	AiLRPos_ioclk <= 0;
	AiLRNeg_ioclk <= 0;
	AiClkNeg_temp <= 0;
	AiClkNeg_ioclk <= 0;
	AisLR_temp <= 0;
	AisLRPos_ioclk <= 0;
	AisLRNeg_ioclk <= 0;
	AisClkPos_temp <= 0;
	AisClkPos_ioclk <= 0;
	AisLR_32khz_temp <= 0;
	AisLR_32khzPos_ioclk <= 0;
	AisLR_32khzNeg_ioclk <= 0;
	AisClk_32khz_temp <= 0;
	AisClk_32khzPos_ioclk <= 0;
	AisClk_32khzNeg_ioclk <= 0;
      end
    else
      begin
        AiLR_temp <= AiLR_sync;
	AiLRPos_ioclk <= ~AiLR_temp && AiLR_sync;
	AiLRNeg_ioclk <= AiLR_temp && ~AiLR_sync;
	AiClkNeg_temp <= AiClkNeg_sync;
	AiClkNeg_ioclk <= ~AiClkNeg_temp && AiClkNeg_sync;
        AisLR_temp <= AisLR_sync;
	AisLRPos_ioclk <= ~AisLR_temp && AisLR_sync;
	AisLRNeg_ioclk <= AisLR_temp && ~AisLR_sync;
	AisClkPos_temp <= AisClkPos_sync;
	AisClkPos_ioclk <= ~AisClkPos_temp && AisClkPos_sync;
	AisClkNeg_temp <= AisClkNeg_sync;
	AisClkNeg_ioclk <= ~AisClkNeg_temp && AisClkNeg_sync;
        AisLR_32khz_temp <= AisLR_32khz_sync;
	AisLR_32khzPos_ioclk <= ~AisLR_32khz_temp && AisLR_32khz_sync;
	AisLR_32khzNeg_ioclk <= AisLR_32khz_temp && ~AisLR_32khz_sync;
        AisClk_32khz_temp <= AisClk_32khz_sync;
	AisClk_32khzPos_ioclk <= ~AisClk_32khz_temp && AisClk_32khz_sync;
	AisClk_32khzNeg_ioclk <= AisClk_32khz_temp && ~AisClk_32khz_sync;
      end
  end
  
  // DAC interface structures/logic
  assign AiDR_out_shift = AiClkNeg_ioclk ? 1 : 0;
  assign AiDL_out_shift = AiClkNeg_ioclk ? 1 : 0;

  assign AiDL_out_load = ~AiClk & AiDL_out_load_latch;
  assign AiDR_out_load = ~AiClk & AiDR_out_load_latch;

always @(posedge io_clk)
begin
  if(~resetb_sync)
    begin
      AiDL_out <= 0;
      AiDR_out <= 0;
      bypass_d2 <= 0;
      AiDL_out_load_reg <= 0;
      AiDR_out_load_reg <= 0;
      AiDL_out_load_latch <= 0;
      AiDR_out_load_latch <= 0;
      AiDL_out_load_temp <= 0;
      AiDR_out_load_temp <= 0;
      AiDL_out_load_negedge <= 0;
      AiDR_out_load_negedge <= 0;
    end
  else
    begin
        AiDL_out_load_reg <= AiDL_out_load;
        AiDR_out_load_reg <= AiDR_out_load;
	if(AiDL_out_load==1)
	  AiDL_out_load_latch <= 0;
	else
	  if(AiLRPos_ioclk)
	    AiDL_out_load_latch <= 1;

	if(AiDR_out_load==1)
	  AiDR_out_load_latch <= 0;
	else
	  if(AiLRNeg_ioclk)
	    AiDR_out_load_latch <= 1;

	AiDL_out_load_temp <= AiDL_out_load;
	AiDL_out_load_negedge <= AiDL_out_load_temp && ~AiDL_out_load;
	AiDR_out_load_temp <= AiDR_out_load;
	AiDR_out_load_negedge <= AiDR_out_load_temp && ~AiDR_out_load;

	if((AiDL_out_load_negedge==1) || (AiDR_out_load_negedge==1))
	  bypass_d2 <= bypass[1];

	if(AiDL_out_shift || AiDR_out_shift)
	  SRC_result_delay <= SRC_result;

        if(AiDL_out_load)
	  AiDL_out <= bypass_out;
	else if(AiDL_out_shift)
	  AiDL_out <= {AiDL_out[14:0], 1'b0};
        if(AiDR_out_load)
	  AiDR_out <= bypass_out;
	else if(AiDR_out_shift)
	  AiDR_out <= {AiDR_out[14:0], 1'b0};	
    end
end

// need to delay bypass_d2 because we delayed the load of the 
// AiD[L/R] registers. This can be seen best when a stream 
// transaction is restarted. Since for the the first half clock 
// cycle the src output is zero, the Ai output should be the 
// dsp input for that half cycle. 
//assign bypass_out = bypass_d2 ? bypass[0] ? d2dsp_ioR : d2dsp_ioL : SRC_result_delay;
assign bypass_out = bypass_d2 ? bypass[0] ? d2dsp_ioL : d2dsp_ioR : SRC_result_delay;
assign bypass[0] = AiLR_sync;

// Assign outputs, zero when resetb_sync = 0, asynch for those going to pads
assign aid = ~resetb_sync ? 0 : (AiLR_sync ? AiDL_out[15] : AiDR_out[15]);
assign ailr = ~resetb_sync ? 0 : AiLR;
assign aiclk = ~resetb_sync ? 0 : AiClk;
assign aislr = ~resetb_sync ? 0 : AisLR;
assign aisclk = ~resetb_sync ? 0 : AisClk;

// these outputs already synch. reset
assign io_dspPop = io_dspPop_int;
assign AiPiData = AiPiData_int;
assign ai_piInt = ai_piInt_int;

// BistErr should already have correct POR value?
assign BistErr = BistErr_int;

always @(posedge io_clk)
begin
  if(~resetb_sync)
    begin
      ai_piInt_int <= 0;
    end 
  else
    begin
      ai_piInt_int <= AiCr[`AI_CR_AIINT] && AiCr[`AI_CR_AIINTMSK];
    end
end

  // module instantiations

  ///////////////
  // AI Clocks //
  ///////////////
  io_AiClks io_AiClks0 	(
			.AiClk(AiClk),
			.AiClkNeg(AiClkNeg),
			.AiLR(AiLR),
			.AisClk(AisClk),
			.AisClkPos(AisClkPos),
			.AisClkNeg(AisClkNeg),
			.AisLR(AisLR),
			.AisLR_32khz(AisLR_32khz_VidClk),
			.AisClk_32khz(AisClk_32khz_VidClk),
			.io_VidClk(io_VidClk),
			.resetb(resetb_sync_VidClk),
			.Ais48KHz(afr_sync),
			.AisClkEnable(AisClkEnable_sync)
			);

  ///////////////////////////////////
  // Control status register block //
  ///////////////////////////////////
  io_AiCSR io_AiCSR0 	(
			.io_clk(io_clk),
		        .resetb(resetb_sync),
		        .PiData(PiData),
		        .LdAiVrL(LdAiVrL),
		        .LdAiCrL(LdAiCrL),
		        .LdAiItH(LdAiItH),
		        .LdAiItL(LdAiItL),
		        .AisSample(AisSample),
		        .AiVr(AiVr),
		        .AiCr(AiCr),
		        .AiScnt(AiScnt),
		        .AiIt(AiIt),
			.dvdaudio_mode(dvdaudio_mode)
		        );

  //////////////////////////////////		       
  // sample rate conversion block //
  //////////////////////////////////		       
  io_AiSRC io_AiSRC 	(
			.Result(SRC_result),
			.read_addr(read_addr),
			.Done(SRC_done),
			.read_enable(read_enable),
			.io_clk(io_clk),
			.resetb(resetb_sync),
			.AisData(read_data),
			.VR(AiVr[`AI_VR_AVRR]),
			.VL(AiVr[`AI_VR_AVRL]),
			.AiDR(d2dsp_ioR),
			.AiDL(d2dsp_ioL),
			.freq(afr),
			.newest(newest),
			.oldest(oldest),
			.go(go),
			.mode(mode),
			.LR(AiLR_sync),
			.period(period),
			.AiLRNeg_ioclk(AiLRNeg_ioclk),
			.clear(clear)
			);

  ////////////////////////////////////////////////////
  // Memory block, contains ram to hold stream data //  
  ////////////////////////////////////////////////////
  io_AiMem io_AiMem	(
			.read_data(read_data),
			.newest(newest),
			.oldest(oldest),
			.BistErr(BistErr_int),
			.io_clk(io_clk),
			.resetb(resetb_sync),
			.clear(clear),
			.AisClkPos_ioclk(AisClkPos_ioclk),
			.AisClkNeg_ioclk(AisClkNeg_ioclk),
			.AisLR(AisLR_sync),
			.AisLRPos(AisLRPos_ioclk),
			.AisLRNeg(AisLRNeg_ioclk),
			.AisD(aisd),
			.read_addr(read_addr),
			.BistEnable(BistEnable),
			.BistAddr(BistAddr),
			.BistData1(BistData1),
			.BistRdn(BistRdn),
			.BistWen96(BistWen96),
			.BistCmp96(BistCmp96),
			.afr(afr),
			.AisSample(AisSample),
			.AiLR(AiLR_sync),
			.read_enable(read_enable)
			);  

  //////////////////////
  // top level Ai FSM //
  //////////////////////
  io_Ai_fsm io_Ai_fsm	(
			.AisClkEnable(AisClkEnable),
			.dsp_bypass(bypass[1]),
			.clear(clear),
			.mode(mode),
			.go(go),
			.period(period),
			.io_clk(io_clk),
			.resetb(resetb_sync),
			.PStat(PStat),
			.SRC_done(SRC_done),
			.AiLRPos_ioclk(AiLRPos_ioclk),
			.AiLRNeg_ioclk(AiLRNeg_ioclk),
			.afr(afr),
			.AiLR(AiLR_sync)
			);
endmodule // io_Ai

