#!/usr/local/bin/perl
#assumes you are in $ROOT/hw/chipA/io/ai/test

#usage: ai-sa-regress.pl <vol> <short> <first 32khz input file> <second 32khz input file> <first 48khz input file> <second 32khz input file>

print "WARNING: aidac_stream_32.v and aidac_stream_48.v will be opened for p4 edited and then p4 reverted. Look for current versions to be saved to *aidac_stream_*.v.old\n";

############################
#setup for 32khz mode tests#
############################
system("p4 edit aidac_stream_32.v");

print "running 32khz tests\n";
print "saving aidac_stream_32.v to aidac_stream_32.v.old\n";
system("cp aidac_stream_32.v aidac_stream_32.v.old\n");

if($ARGV[1] eq "short"){
  print "running short regressions...\n";
  
  #this will cause each test to run for 2 milliseconds simulation time
  system("cp aidac_stream_32_regress_short.v aidac_stream_32.v");
  
}else{
  print "running long regressions. Total run time will be approximately 6 hours...\n";
  #this will cause each test to run for .5 seconds simulation time
  system("cp aidac_stream_32_regress.v aidac_stream_32.v");
  
} 

#run the first test.
#Script is dumb, so you'll have to look in ai-sa-regress_0.log for the result
system("run_aisa.pl aidac_stream_32 $ARGV[2] $ARGV[0] 0 > ai-sa-regress_32_0.log");

#save off the diag directory and out.wav file
system("rm -rf diag_32_0"); 
system("cp -r diag diag_32_0");
system("cp out.wav diag_32_0/out.wav");

#run the second test. 
#Script is dumb, so you'll have to look in ai-sa-regress_1.log for the result
system("run_aisa.pl aidac_stream_32 $ARGV[3] $ARGV[0] 0 > ai-sa-regress_32_1.log");

#save off the diag directory and out.wav file
system("rm -rf diag_32_1"); 
system("cp -r diag diag_32_1");
system("cp out.wav diag_32_1/out.wav");

############################
#setup for 48khz mode tests#
############################
system("p4 edit aidac_stream_48.v");

print "running 48khz tests\n";
print "saving aidac_stream_48.v to aidac_stream_48.v.old\n";
system("cp aidac_stream_48.v aidac_stream_48.v.old\n");

if($ARGV[1] eq "short"){
  print "running short regressions...\n";
  
  #this will cause each test to run for 2 milliseconds simulation time
  system("cp aidac_stream_48_regress_short.v aidac_stream_48.v");
  
}else{
  print "running long regressions. Total run time will be approximately 3 hours...\n";
  #this will cause each test to run for .5 seconds simulation time
  system("cp aidac_stream_48_regress.v aidac_stream_48.v");
  
} 

#run the first test.
#Script is dumb, so you'll have to look in ai-sa-regress_0.log for the result
system("run_aisa.pl aidac_stream_48 $ARGV[4] $ARGV[0] 1 > ai-sa-regress_48_0.log");

#save off the diag directory and out.wav file
system("rm -rf diag_48_0"); 
system("cp -r diag diag_48_0");
system("cp out.wav diag_48_0/out.wav");

#run the second test. 
#Script is dumb, so you'll have to look in ai-sa-regress_1.log for the result
system("run_aisa.pl aidac_stream_48 $ARGV[5] $ARGV[0] 1 > ai-sa-regress_48_1.log");

#save off the diag directory and out.wav file
system("rm -rf diag_48_1"); 
system("cp -r diag diag_48_1");
system("cp out.wav diag_48_1/out.wav");

system("p4 revert aidac_stream_32.v");
system("p4 revert aidac_stream_48.v");

print "RESULTS\n";
print "=======\n";

system("echo \"\n32 khz mode, input file = $ARGV[2]:\"");
system("grep PASSED ai-sa-regress_32_0.log");
system("grep FAILED ai-sa-regress_32_0.log");

system("echo \"\n32 khz mode, input file = $ARGV[3]:\"");
system("grep PASSED ai-sa-regress_32_1.log");
system("grep FAILED ai-sa-regress_32_1.log");

system("echo \"\n48 khz mode, input file = $ARGV[4]:\"");
system("grep PASSED ai-sa-regress_48_0.log");
system("grep FAILED ai-sa-regress_48_0.log");

system("echo \"\n48 khz mode, input file = $ARGV[5]:\"");
system("grep PASSED ai-sa-regress_48_1.log");
system("grep FAILED ai-sa-regress_48_1.log");

print "\n";
print "look in ai-sa-regress_<32/48>_<0/1>.log for complete log files\n";
print "look in diag_<32/48>_<0/1>.log for results\n";


