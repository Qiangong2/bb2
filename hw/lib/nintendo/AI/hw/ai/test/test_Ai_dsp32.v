// Testbench for revB AI with dsp32

`timescale 1ns/100ps

module test_Ai_dsp32;

   // General inputs
   reg io_clk;    		// System clock
   reg io_VidClk; 		// 27MHz clock
   reg resetb;    		// System reset
   reg resetb_sync_VidClk;    	// reset for 27MHz logic

   // from DSP_AI
   wire[15:0] 	dsp_ioL;
   wire[15:0] 	dsp_ioR;
   wire 	io_dspPop;

   // io_pi
   reg[4:1]	PiAddr;    	// Address from io_pi
   reg 	 	PiLoad;    	// Load Register from io_pi
   reg[15:0]   	PiData;    	// Data bus in from io_pi
   wire[15:0]  	AiPiData;  	// Data bus out to io_pi
   reg 	 	PiAiSel;   	// Ai select
   wire 	ai_piInt;  	// Ai Interrupt

   // AI - Audio DAC Interface and streaming audio interface
   wire 	aid;		// dac data 
   wire 	ailr;		// dac left-right
   wire 	aiclk;		// dac clock 
   wire		aisd;		// stream data 
   wire 	aislr;		// stream left-right
   wire 	aisclk;		// stream clock 

   // BIST
   reg 	BistEnable;	
   reg 	BistAddr;
   reg 	BistData1;
   reg 	BistRdn;
   reg 	BistWen96;
   reg 	BistCmp96;
   wire[1:0] BistErr;

   // Ai Module Instantiation
   io_Ai_top  io_Ai_top (
		.io_dspPop(io_dspPop),
		.AiPiData(AiPiData),
		.ai_piInt(ai_piInt),
		.aid(aid),
		.ailr(ailr),
		.aiclk(aiclk),
		.aislr(aislr),
		.aisclk(aisclk),
		.BistErr(BistErr),
		.io_clk(io_clk),
		.io_VidClk(io_VidClk),
		.resetb(resetb),
//		.resetb_sync_VidClk(resetb_sync_VidClk),
		.dsp_ioL(dsp_ioL),
		.dsp_ioR(dsp_ioR),
		.PiAddr(PiAddr),
		.PiLoad(PiLoad),
		.PiData(PiData),
		.PiAiSel(PiAiSel),
		.aisd(aisd),
		.BistEnable(BistEnable),
		.BistAddr(BistAddr),
		.BistData1(BistData1),
		.BistRdn(BistRdn),
		.BistWen96(BistWen96),
		.BistCmp96(BistCmp96)
		);

   dsp32_data dsp32_data0 (
			.io_clk(io_clk),
			.resetb(resetb),
			.io_dspPop(io_dspPop),
			.dsp_ioL(dsp_ioL),
			.dsp_ioR(dsp_ioR)
			);
   

   di_dvdtop di_dvdtop0 (.did(),
                         .dibrk(),
                         .didstrbb(),
                         .dierrb(),
                         .dicover(),
                         .aisd(aisd),
                         .didir(),
                         .dihstrbb(),
                         .dirstb(resetb),
                         .aisclk(aisclk),
                         .aislr(aislr)
                         );

tri_dac u131 (
	.aiclk(aiclk),
	.aid(aid),
	.ailr(ailr),
	.vclk27(),
	.vclk54(),
	.vicr(),
	.yuv(),
	.sclk_mpalsel(),
	.sdat_ntscsel(),
	.visel0(),
	.visel1()
);

parameter 	IO_CLK_CYCLE = 2.5;
parameter 	IO_CLK_CYCLE2 = 5;
parameter	IO_VIDCLK_CYCLE = 18.52;
parameter	DELAY_32KHZ_HALF = 7805; // 1/4 of 32khz
parameter	DELAY_48KHZ_HALF = 5200; // 1/4 of 48khz
parameter	DELAY_32KHZ = 15615; // 1/2 of 32khz, 3123 io_clk cycles 
parameter	DELAY_48KHZ = 10400; // 1/2 of 48khz, 2080 io_clk cycles
parameter	DELAY_32KHZ2 = 31230; // 32khz
parameter	DELAY_48KHZ2 = 20800; // 48khz
parameter	delay = 5;
parameter	mult_delay = 300;
integer i, j, k;


// clk generation

always #IO_CLK_CYCLE io_clk = ~io_clk;
always #IO_VIDCLK_CYCLE io_VidClk = ~io_VidClk;

initial
  begin
    $vcdpluson();
    fork

#0 	begin 
	  resetb=1'b0; resetb_sync_VidClk=1'b0;io_clk=1'b0; io_VidClk=1'b0;
	  BistAddr=1'b0; BistCmp96=1'b0; BistData1=1'b0;
	  BistEnable=1'b0; BistRdn=1'b1; BistWen96=1'b1;
	  PiAddr=0; PiLoad=0; PiData=0; PiAiSel=0;
	end

#6316.76 

	begin 
	  resetb=1'b1; 
	end

#6383.14 
	begin
	  resetb_sync_VidClk=1'b1;
	end

//#7002.5	  Start_dsp32_data;
#6982.5	Set_Volume(16'hffff);
//#7002.5	Start_Stream(0);
#7002.5	  Start_dsp32_data;

//#7002.5	Set_Volume(16'hffff);

//#7022.5	Set_AIIT(32'hffffffff);

//#7042.5	Start_Stream(0);

#19002.5 stop48start32dsp32;
#107002.5 stop32start48dsp32;

//#19002.5 Stop_Stream(0);
//#56002.5 Stop_Stream(0);

//#20002.5 Restart_Stream(0);
//#70002.5 Restart_Stream(0);

//#100002.5 Stop_Stream(1);
//#100002.5 Stop_Stream(0);

//#105002.5 Restart_Stream(1);
//#105002.5 Start_Stream(1);
//#106002.5 Start_Stream(1);

//#49002.5 Stop_Stream(1);

//#50002.5 Restart_Stream(1);

//#1900002.5 Pause_Stream(0);

//#2000002.5 Restart_Stream(0);

//#3900002.5 Stop_Stream(0);

//#4000000 	$finish;

#2000000 $finish;
//#500000 $finish;

join
end

task stop32start48dsp32;
begin
  PiAddr = 4'b0001; PiLoad = 1'b1; PiAiSel = 1; PiData = {25'h0, 1'b1, 4'b1110, 1'b0, 1'b0};
  #IO_CLK_CYCLE2
  PiAddr = 4'b0000; PiLoad = 1'b0; PiAiSel = 0; PiData = 16'h0000;
  #DELAY_32KHZ
  PiAddr = 4'b0001; PiLoad = 1'b1; PiAiSel = 1; PiData = {25'h0, 1'b1, 4'b1011, 1'b1, 1'b1};
  #IO_CLK_CYCLE2
  PiAddr = 4'b0000; PiLoad = 1'b0; PiAiSel = 0; PiData = 16'h0000;
end
endtask

task stop32start48dsp48;
begin
  PiAddr = 4'b0001; PiLoad = 1'b1; PiAiSel = 1; PiData = {25'h0, 1'b0, 4'b1110, 1'b0, 1'b0};
  #IO_CLK_CYCLE2
  PiAddr = 4'b0000; PiLoad = 1'b0; PiAiSel = 0; PiData = 16'h0000;
  #DELAY_32KHZ
  PiAddr = 4'b0001; PiLoad = 1'b1; PiAiSel = 1; PiData = {25'h0, 1'b0, 4'b1011, 1'b1, 1'b1};
  #IO_CLK_CYCLE2
  PiAddr = 4'b0000; PiLoad = 1'b0; PiAiSel = 0; PiData = 16'h0000;
end
endtask

task stop48start32dsp32;
begin
  PiAddr = 4'b0001; PiLoad = 1'b1; PiAiSel = 1; PiData = {25'h0, 1'b1, 4'b1110, 1'b1, 1'b0};
  #IO_CLK_CYCLE2
  PiAddr = 4'b0000; PiLoad = 1'b0; PiAiSel = 0; PiData = 16'h0000;
  #DELAY_48KHZ_HALF
  PiAddr = 4'b0001; PiLoad = 1'b1; PiAiSel = 1; PiData = {25'h0, 1'b1, 4'b1011, 1'b0, 1'b1};
  #IO_CLK_CYCLE2
  PiAddr = 4'b0000; PiLoad = 1'b0; PiAiSel = 0; PiData = 16'h0000;
end
endtask

task stop48start32dsp48;
begin
  PiAddr = 4'b0001; PiLoad = 1'b1; PiAiSel = 1; PiData = {25'h0, 1'b0, 4'b1110, 1'b1, 1'b0};
  #IO_CLK_CYCLE2
  PiAddr = 4'b0000; PiLoad = 1'b0; PiAiSel = 0; PiData = 16'h0000;
  #DELAY_48KHZ
  PiAddr = 4'b0001; PiLoad = 1'b1; PiAiSel = 1; PiData = {25'h0, 1'b0, 4'b1011, 1'b0, 1'b1};
  #IO_CLK_CYCLE2
  PiAddr = 4'b0000; PiLoad = 1'b0; PiAiSel = 0; PiData = 16'h0000;
end
endtask

task Start_dsp32_data;
begin
//  PiAddr = 4'b0001; PiLoad = 1'b1; PiAiSel = 1; PiData = {25'h0, 7'h40};
//  PiAddr = 4'b0001; PiLoad = 1'b1; PiAiSel = 1; PiData = {25'h0, 7'h6d};
  PiAddr = 4'b0001; PiLoad = 1'b1; PiAiSel = 1; PiData = {25'h0, 7'h6f};
  #IO_CLK_CYCLE2
  PiAddr = 4'b0000; PiLoad = 1'b0; PiAiSel = 0; PiData = 16'h0000;
end
endtask

task Start_Stream;
input freq;
begin
  PiAddr = 4'b0001; PiLoad = 1'b1; PiAiSel = 1; PiData = {26'h0, 4'b1011, freq, 1'b1};
  #IO_CLK_CYCLE2
  PiAddr = 4'b0000; PiLoad = 1'b0; PiAiSel = 0; PiData = 16'h0000;
end
endtask

task Pause_Stream;
input freq;
begin
  PiAddr = 4'b0001; PiLoad = 1'b1; PiAiSel = 1; PiData = {26'h0, 4'b0000, freq, 1'b0};
  #IO_CLK_CYCLE2
  PiAddr = 4'b0000; PiLoad = 1'b0; PiAiSel = 0; PiData = 16'h0000;
end
endtask

task Restart_Stream;
input freq;
begin
  PiAddr = 4'b0001; PiLoad = 1'b1; PiAiSel = 1; PiData = {26'h0, 4'b0001, freq, 1'b1};
  #IO_CLK_CYCLE2
  PiAddr = 4'b0000; PiLoad = 1'b0; PiAiSel = 0; PiData = 16'h0000;
end
endtask

task Stop_Stream;
input freq;
begin
  PiAddr = 4'b0001; PiLoad = 1'b1; PiAiSel = 1; PiData = {26'h0, 4'b1110, freq, 1'b0};
  #IO_CLK_CYCLE2
  PiAddr = 4'b0000; PiLoad = 1'b0; PiAiSel = 0; PiData = 16'h0000;
end
endtask

task Set_Volume;
input[15:0] reg_data;
begin
  PiAddr = 4'b0011; PiLoad = 1'b1; PiAiSel = 1; PiData = reg_data;
  #IO_CLK_CYCLE2
  PiAddr = 4'b0000; PiLoad = 1'b0; PiAiSel = 0; PiData = 16'h0000;
end
endtask

task Set_AIIT;
input[31:0] reg_data;
begin
  PiAddr = 4'b0111; PiLoad = 1'b1; PiAiSel = 1; PiData = reg_data[15:0];
  #IO_CLK_CYCLE2
  PiAddr = 4'b0110; PiLoad = 1'b1; PiAiSel = 1; PiData = reg_data[31:16];
  #IO_CLK_CYCLE2
  PiAddr = 4'b0000; PiLoad = 1'b0; PiAiSel = 0; PiData = 16'h0000;
end
endtask

endmodule