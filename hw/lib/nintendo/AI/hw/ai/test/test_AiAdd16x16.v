// Testbench for AiAdd16x16 standalone testing

`timescale 1ns/100ps

module test_AiAdd16x16;

reg Start, resetb, io_clk, Cin;
reg [15:0] Op0, Op1;
wire[15:0] Sum;
wire Done, Cout;


io_AiAdd16x16 io_AiAdd16x16 	(
				.Start(Start),
				.Op0(Op0),
				.Op1(Op1),
				.Cin(Cin),
				.resetb(resetb),
				.io_clk(io_clk),
				.Done(Done),
				.Sum(Sum),
				.Cout(Cout)
				);

parameter 	IO_CLK_CYCLE = 2.5;
parameter	delay = 5;
parameter	add_delay = 100;
parameter	big_delay = 1800;
integer i, j, k, gold, sim;


// clk generation

always #IO_CLK_CYCLE io_clk = ~io_clk;

initial
  begin
    $vcdpluson();
    fork

#0 	begin 
	   io_clk=1'b0; Start=1'b0; resetb=1'b1; Op0=16'b0; Op1=16'b0; Cin=1'b0; 
	  gold = $fopen("add.gold");
	  sim = $fopen("add.sim");
	end

#100    resetb=1'b0;

#200	resetb=1'b1;

// add +4/2^15 and +4/2^15 
#300	add_sequence(4, 4, 0); 

// add -4/2^15 and -4/2^15
#600 	add_sequence(16'hfffc, 16'hfffc, 0);

// add +4/2^15 and -4/2^15
#900	add_sequence(4, 16'hfffc, 0);

// add -4/2^15 and +4/2^15
#1200	add_sequence(16'hfffc, 4, 0);

// add -1 and -1
#1500	add_sequence(16'h8000, 16'h8000, 0);

#big_delay 
	begin
	for(i=0; i<65535; i=i+1)
	  for(j=0; j<65535; j=j+1)
	    for(k=0; k<1; k=k+1)
		add_sequence(i, j, k);
	end

#38000 	begin
	$finish;
	$fclose(gold);
	$fclose(sim);
	end

join
end

task add_sequence;
input [15:0] Op0_in, Op1_in;
input Cin_in;

begin
	Start=1'b1; Op0=Op0_in; Op1=Op1_in; Cin=Cin_in; 
	#delay Start=1'b0;
	#add_delay;
	$fdisplay(gold, Op0+Op1);
//	$fdisplay(gold, (Op0+Op1)>>16);
	$fdisplay(sim, Sum);
//	$fdisplay(sim, Cout);
end
endtask

endmodule