// Testbench for AiMult16x16 standalone testing

`timescale 1ns/100ps

module test_AiMult16x16;

reg Start, resetb, io_clk;
wire Done;
reg[15:0] Op0, Op1;
wire [30:0] Product;

io_AiMult16x16 io_AiMult16x16 (
                               .Start(Start),
                               .Op0(Op0),
                               .Op1(Op1),
                               .resetb(resetb),
                               .io_clk(io_clk),
                               .Done(Done),
                               .Product(Product)
                              );


parameter 	IO_CLK_CYCLE = 2.5;
parameter	delay = 5;
parameter	mult_delay = 300;
integer i, j, k;


// clk generation

always #IO_CLK_CYCLE io_clk = ~io_clk;

initial
  begin
    $vcdpluson();
    fork

#0 	begin 
	   io_clk=1'b0; Start=1'b0; resetb=1'b1; Op0=16'b0; Op1=16'b0; 
	end

#100    resetb=1'b0;

#200	resetb=1'b1;

// multiply +4 and +4
#300	mult_sequence(16'h0004, 16'h0004);

// multiply -4 and -4
#600	mult_sequence(16'hfffc, 16'hfffc);

// multiply +4 and -4
#900	mult_sequence(16'h0004, 16'hfffc);

// multiply -4 and +4
#1200	mult_sequence(16'hfffc, 16'h0004);

// multiply -1 and -1
#1500	mult_sequence(16'h8000, 16'h8000);

#1800	
	begin
	  for(i=0; i<1; i=i+1)
	    for(j=0; j<10; j=j+1)
	      mult_sequence(i, j);
	end

#6000	begin
	  for(i=32768; i<32769; i=i+1)
	    for(j=0; j<10; j=j+1)
	      mult_sequence(i, j);
	end

#18000 	$finish;

join
end

task mult_sequence;
input [15:0] Op0_in, Op1_in;
   begin
      Start=1'b1; Op0=Op0_in; Op1=Op1_in;
      #delay Start=1'b0;
      #mult_delay;
   end
endtask

endmodule