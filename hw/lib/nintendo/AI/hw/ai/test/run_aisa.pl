#!/usr/local/bin/perl

#ARGV[0] = testname
#ARGV[1] = input wave or ascii file for stream interface
#ARGV[2] = volume setting (in decimal)
#ARGV[3] = freq
#ARGV[4] = ascii?

#ARGV[5] = dsp32?
#ARGV[6] = input for dsp32
#ARGV[7] = ascii for dsp32?
#ARGV[8] = hex for dsp32?

$depth = "../../../";

system("mkdir -p diag");
system("rm -f diag/*");
open(SIMV_LOG, ">simv.log");
printf SIMV_LOG "fake simv.log for standalone\n";
close(SIMV_LOG);
if($ARGV[4] eq "ascii"){
  print "in ascii mode\n";
  system("cp $ARGV[1] diag/input.txt");
}else{
  print "in wav mode\n";
  system("gcc $depth../tools/ai/wav2txt.c $depth../tools/ai/wave.c -o diag/wav2txt -lm");
  system("gcc $depth../tools/ai/txt2wav.c $depth../tools/ai/wave.c -o diag/txt2wav -lm");
  system("diag/wav2txt $ARGV[1] diag/input.txt diag/minnow.txt");
}

system("$depth../tools/ai/src_model.pl $ARGV[3] $ARGV[2] $ARGV[2] $depth");

if($ARGV[5] eq "dsp32") {
  if($ARGV[7] eq "ascii"){
    print "in ascii mode\n";
    system("cp $ARGV[6] diag/dspdataL.txt");
    system("cp $ARGV[6] diag/dspdataR.txt");
  }else{
    print "in wav mode\n";
    system("gcc $depth../tools/ai/wav2txt.c $depth../tools/ai/wave.c -o diag/wav2txt -lm");
    system("gcc $depth../tools/ai/txt2wav.c $depth../tools/ai/wave.c -o diag/txt2wav -lm");
    system("diag/wav2txt $ARGV[6] diag/input.txt diag/minnow.txt");
  }
system("$depth../tools/ai/src_model.pl 0 255 255 $depth $ARGV[5] $ARGV[8]");
}

system("make $ARGV[0]");
system("$depth../diag/ai0/script/aidac_chkr.pl $ARGV[0] $depth");
system("diag/txt2wav diag/aisink_wav.txt");
