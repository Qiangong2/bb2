// Testbench for AiMult16x16 standalone testing

`timescale 1ns/100ps

module test_Ai;

   // General inputs
   reg io_clk;    		// System clock
   reg io_VidClk; 		// 27MHz clock
   reg resetb;    		// System reset
   reg resetb_sync_VidClk;    	// reset for 27MHz logic

   // from DSP_AI
   reg[15:0] 	dsp_ioL;
   reg[15:0] 	dsp_ioR;
   wire 	io_dspPop;

   // io_pi
   reg[4:1]	PiAddr;    	// Address from io_pi
   reg 	 	PiLoad;    	// Load Register from io_pi
   reg[15:0]   	PiData;    	// Data bus in from io_pi
   wire[15:0]  	AiPiData;  	// Data bus out to io_pi
   reg 	 	PiAiSel;   	// Ai select
   wire 	ai_piInt;  	// Ai Interrupt

   // AI - Audio DAC Interface and streaming audio interface
   wire 	aid;		// dac data 
   wire 	ailr;		// dac left-right
   wire 	aiclk;		// dac clock 
   wire		aisd;		// stream data 
   wire 	aislr;		// stream left-right
   wire 	aisclk;		// stream clock 

   // BIST
   reg 	BistEnable;	
   reg 	BistAddr;
   reg 	BistData1;
   reg 	BistRdn;
   reg 	BistWen96;
   reg 	BistCmp96;
   wire 	BistErr;

   // Ai Module Instantiation
   io_Ai_top  io_Ai_top (
                .io_dspPop(io_dspPop),
                .AiPiData(AiPiData),
                .ai_piInt(ai_piInt),
                .aid(aid),
                .ailr(ailr),
                .aiclk(aiclk),
                .aislr(aislr),
                .aisclk(aisclk),
                .BistErr(BistErr),
                .io_clk(io_clk),
                .io_VidClk(io_VidClk),
                .resetb(resetb),
//              .resetb_sync_VidClk(resetb_sync_VidClk),
                .dsp_ioL(dsp_ioL),
                .dsp_ioR(dsp_ioR),
                .PiAddr(PiAddr),
                .PiLoad(PiLoad),
                .PiData(PiData),
                .PiAiSel(PiAiSel),
                .aisd(aisd),
                .BistEnable(BistEnable),
                .BistAddr(BistAddr),
                .BistData1(BistData1),
                .BistRdn(BistRdn),
                .BistWen96(BistWen96),
                .BistCmp96(BistCmp96)
                );

//   dsp32_data dsp32_data0 (
//                        .io_clk(io_clk),
//                        .resetb(resetb),
//                        .io_dspPop(io_dspPop),
//                        .dsp_ioL(dsp_ioL),
//                        .dsp_ioR(dsp_ioR)
//                        );
   

   di_dvdtop di_dvdtop0 (.did(),
                         .dibrk(),
                         .didstrbb(),
                         .dierrb(),
                         .dicover(),
                         .aisd(aisd),
                         .didir(),
                         .dihstrbb(),
                         .dirstb(resetb),
                         .aisclk(aisclk),
                         .aislr(aislr)
                         );

tri_dac u131 (
        .aiclk(aiclk),
        .aid(aid),
        .ailr(ailr),
        .vclk27(),
        .vclk54(),
        .vicr(),
        .yuv(),
        .sclk_mpalsel(),
        .sdat_ntscsel(),
        .visel0(),
        .visel1()
);

parameter 	IO_CLK_CYCLE = 2.5;
parameter 	IO_CLK_CYCLE2 = 5;
parameter	IO_VIDCLK_CYCLE = 18.52;
parameter	delay = 5;
parameter	mult_delay = 300;
integer i, j, k;


// clk generation

// this is about 25% faster than initializing
// the value and then using an always statement
always 
begin
  io_clk=0;
  forever #IO_CLK_CYCLE io_clk = ~io_clk;
end 

always 
begin
  io_VidClk=0;
  forever #IO_VIDCLK_CYCLE io_VidClk = ~io_VidClk;
end 

initial
  begin
//    $vcdpluson();

#0 	begin 
	  resetb=1'b0; resetb_sync_VidClk=1'b0; 
	  BistAddr=1'b0; BistCmp96=1'b0; BistData1=1'b0;
	  BistEnable=1'b0; BistRdn=1'b1; BistWen96=1'b1;
	  PiAddr=0; PiLoad=0; PiData=0; PiAiSel=0;
	  dsp_ioL=16'h0000; dsp_ioR=16'h0000;
	end

#6316.76 

	begin 
	  resetb=1'b1; 
	end

#66.38
	begin
	  resetb_sync_VidClk=1'b1;
	end

#616.86	Set_Volume(16'hffff);

#20     Set_AIIT(32'hffffffff);

#20	Start_Stream(1);

#499992960	$finish;	// half sec. marker

end

task Start_Stream;
input freq;
begin
  PiAddr = 4'b0001; PiData = {26'h0, 4'b1011, freq, 1'b1}; PiLoad = 1'b1; PiAiSel = 1;
  #IO_CLK_CYCLE2
  PiAddr = 4'b0000; PiData = 16'h0000; PiLoad = 1'b0; PiAiSel = 0;
end
endtask

task Pause_Stream;
input freq;
begin
  PiAddr = 4'b0001; PiLoad = 1'b1; PiAiSel = 1; PiData = {26'h0, 4'b0000, freq, 1'b0};
  #IO_CLK_CYCLE2
  PiAddr = 4'b0000; PiLoad = 1'b0; PiAiSel = 0; PiData = 16'h0000;
end
endtask

task Restart_Stream;
input freq;
begin
  PiAddr = 4'b0001; PiLoad = 1'b1; PiAiSel = 1; PiData = {26'h0, 4'b0001, freq, 1'b1};
  #IO_CLK_CYCLE2
  PiAddr = 4'b0000; PiLoad = 1'b0; PiAiSel = 0; PiData = 16'h0000;
end
endtask

task Stop_Stream;
input freq;
begin
  PiAddr = 4'b0001; PiLoad = 1'b1; PiAiSel = 1; PiData = {26'h0, 4'b1110, freq, 1'b0};
  #IO_CLK_CYCLE2
  PiAddr = 4'b0000; PiLoad = 1'b0; PiAiSel = 0; PiData = 16'h0000;
end
endtask

task Set_Volume;
input[15:0] reg_data;
begin
  PiAddr = 4'b0011; PiLoad = 1'b1; PiAiSel = 1; PiData = reg_data;
  #IO_CLK_CYCLE2
  PiAddr = 4'b0000; PiLoad = 1'b0; PiAiSel = 0; PiData = 16'h0000;
end
endtask

task Set_AIIT;
input[31:0] reg_data;
begin
  PiAddr = 4'b0111; PiLoad = 1'b1; PiAiSel = 1; PiData = reg_data[15:0];
  #IO_CLK_CYCLE2
  PiAddr = 4'b0110; PiLoad = 1'b1; PiAiSel = 1; PiData = reg_data[31:16];
  #IO_CLK_CYCLE2
  PiAddr = 4'b0000; PiLoad = 1'b0; PiAiSel = 0; PiData = 16'h0000;
end
endtask

endmodule