module dsp32_data(
  //Inputs
  io_clk, resetb, io_dspPop, data_pattern,
  //Outputs
  dsp_ioL, dsp_ioR
);

  input io_clk;
  input resetb;
  input io_dspPop;
  input[3:0] data_pattern;
  output[15:0] dsp_ioL;
  output[15:0] dsp_ioR;

  reg[15:0] dsp_ioL_data;
  reg[15:0] dsp_ioR_data;
  reg[15:0] dsp_ioL_int;
  reg[15:0] dsp_ioR_int;

// the data pattern that will be sent to the ai will 
// be based on what data_pattern's value is. When we test
// on minnow, we will wire these to pins so that the
// data can be programmable
  reg[15:0] dsp_ioL_data_pattern;
  reg[15:0] dsp_ioR_data_pattern;

assign dsp_ioL = dsp_ioL_int;
assign dsp_ioR = dsp_ioR_int;

// right now, we just have one pattern
always @(posedge io_clk)
begin
  case(data_pattern)
    4'b0000:
      begin
	if(~resetb)
	  begin
	    dsp_ioL_data_pattern <= 16'h0000;
	    dsp_ioR_data_pattern <= 16'h0000;
	  end
	else 
	  if(io_dspPop)
            begin
	      dsp_ioL_data_pattern <= 16'h0000;
	      dsp_ioR_data_pattern <= 16'h0000;
	    end
      end
    4'b0001:
      begin
	if(~resetb)
	  begin
	    dsp_ioL_data_pattern <= 16'h0000;
	    dsp_ioR_data_pattern <= 16'h0000;
	  end
	else 
	  if(io_dspPop)
            begin
	      dsp_ioL_data_pattern <= 16'h8001;
	      dsp_ioR_data_pattern <= 16'h8001;
	    end
      end
    4'b0010:
      begin
	if(~resetb)
	  begin
	    dsp_ioL_data_pattern <= 16'h0000;
	    dsp_ioR_data_pattern <= 16'h0000;
	  end
	else 
	  if(io_dspPop)
            begin
	      dsp_ioL_data_pattern <= 16'h7ffe;
	      dsp_ioR_data_pattern <= 16'h7ffe;
	    end
      end
    4'b0011:
      begin
	if(~resetb)
	  begin
	    dsp_ioL_data_pattern <= 16'h0000;
	    dsp_ioR_data_pattern <= 16'h0000;
	  end
	else 
	  if(io_dspPop)
            begin
	      dsp_ioL_data_pattern <= 16'h8000;
	      dsp_ioR_data_pattern <= 16'h8000;
	    end
      end
    4'b0100:
      begin
	if(~resetb)
	  begin
	    dsp_ioL_data_pattern <= 16'h0000;
	    dsp_ioR_data_pattern <= 16'h0000;
	  end
	else 
	  if(io_dspPop)
            begin
	      dsp_ioL_data_pattern <= 16'h7fff;
	      dsp_ioR_data_pattern <= 16'h7fff;
	    end
      end
    4'b0101:
      begin
	if(~resetb)
	  begin
	    dsp_ioL_data_pattern <= 16'h0000;
	    dsp_ioR_data_pattern <= 16'h0000;
	  end
	else 
	  if(io_dspPop)
            begin
	      dsp_ioL_data_pattern <= dsp_ioL_data_pattern+1;
	      dsp_ioR_data_pattern <= dsp_ioR_data_pattern+1;
	    end
      end
    default:
      begin
	dsp_ioL_data_pattern <= 16'h0000;
	dsp_ioR_data_pattern <= 16'h0000;	
      end
  endcase
end

always @(posedge io_clk)
begin
  if (~resetb)
    begin
      dsp_ioL_data <= 16'h0000;
      dsp_ioR_data <= 16'h0000;
      dsp_ioL_int <= 16'h0000;
      dsp_ioR_int <= 16'h0000;
    end
  else
    begin
      dsp_ioL_int <= dsp_ioL_data;
      dsp_ioR_int <= dsp_ioR_data;
      if (io_dspPop)
	begin
	  dsp_ioL_data <= dsp_ioL_data_pattern;
	  dsp_ioR_data <= dsp_ioR_data_pattern;
        end
    end
end

endmodule