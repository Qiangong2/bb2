NOTE: Most of my scripts assume that you have $ROOT set to your dolphn root.

////////////////////////////////////////
To get FIR coefficients from ScopeFir to decimal format that src_model can use:
////////////////////////////////////////

1) save the fir coefficients in scopefir in HEX (not dec) format
2) run $ROOT/hw/tools/ai/controlm.pl <scopefir hex file> <new hexfile> <multiplier>
3) run $ROOT/hw/tools/ai/hex2dec.pl <fir coeffs hex in> <fir coeffs dec out>

where <new hexfile> = <fir coeffs hex in>

NOTE: script assumes that coefficients are in $ROOT/hw/tools/ai/coeffs/


////////////////////////////////////////
To run AI rundiag based regressions:
////////////////////////////////////////

1) cd to your hw/run directory
2) ../tools/ai-regress.pl > <some log file>

NOTE: rundiag based regressions usually take about 3.5 hours.

////////////////////////////////////////
To run AI standalone regressions
////////////////////////////////////////

1) cd to $ROOT/hw/chipA/io/ai/test
2) make sure you don't have changes in aidac_stream_32.v or
aidac_stream_48.v that you care about. They will be saved to
aidac_stream_*.v.bak, but it might get confusing. Better to
check in what you need first.
3) For short regressions:
	ai-sa-regress.pl <vol> <short> <first input file> <second input file>

	ex: ai-sa-regress.pl 255 short
    
   For long regressions (~6 hours):
	ai-sa-regress.pl <vol> <long> <first input file> <second input file>

	vol = 0-255 (min-max)

   NOTE: REMEMBER THAT THE VOLUME IS HARDCODED IN THE STANDALONE 
	 TESTBENCH AS 255. IF YOU WANT TO TEST AT A DIFFERENT
	 VOLUME, YOU UNFORTUNATELY HAVE TO p4 edit 
	 aidac_stream_<32/48>_regress_<short/long>.v TO GET
	 THE VOLUMES (the one passed at the command line and the
	 one hardcoded in the testbench) CONSISTENT.

input files can be found in /home/marklee/wav_files/

Runs 4 tests 32KHz (SRC) 2 tests, 48KHz (bypass) 2 tests

4) The script is dumb, so look in ai-sa-regress_<32/48>_<0/1>.log
for the results.

NOTE: be aware that aidac_stream_32.v and aidac_stream_48.v 
will be opened for p4 edited and then p4 reverted. Look for 
current versions to be saved to *aidac_stream_*.v.old

////////////////////////////////////////
To run standalone tests (C model vs verilog standalone, no mixing)
////////////////////////////////////////

1) cd to $ROOT/hw/chipA/io/ai/test
2) run_aisa.pl aidac_stream_32 <input wave file or ascii file> <volume in decimal> 0 <ascii OR nothing>

aidac_stream_XX is test bench top, to change run time, dumping, etc. edit this file

So, an example of this is:

	run_aisa.pl aidac_stream_32 ~/wav_files/cardigan.wav 255 0

For wave mode, just don't specify ascii for the last argument.

For ascii mode, just do the same thing except pass in an ascii file instead
of a wav file and add the ascii tag as the last (5th) argument to run_aisa.pl.
Here's an example:

	run_aisa.pl aidac_stream_32 $ROOT/hw/tools/aisin1khz_32.txt 255 0 ascii

NOTE: One problem is that the standalone test bench has a hardcoded time at
      which it will stop simulation. So, if you want to change the time 
      of how long something runs, you have to manually edit this file.
      This file is named <testname>.v in the standalone environment.
      So for the case above, you need to manually edit aidac_stream_32.v

NOTE: My most recent run of 2 seconds of audio data took 5.7 hours.

aisnk.tr = verilog output
aisnkC.tr = C sim + scripts output

////////////////////////////////////////
To run the c model by itself, compile it (if necessary) and then call it
as follows:
////////////////////////////////////////
hw/tools/ai

gcc wav_src_model.c wave.c -o wav_src_model -lm
wav_src_model <mode> -i <input file> -n <num of coeffs> -c <coeffs file> -o <output file> -f <freq> -v <volume>

One example of this would be:

wav_src_model -wav -i ~/wav_files/lauryn_startat20.wav -n 128 -c coeffs/fir128_hanning_16_3006_dec_1.txt -o temp.wav -f 0 -v 255

-ascii = list of FP decimal values in text file

////////////////////////////////////////
Filter Information
////////////////////////////////////////

Values used for original filter:

sampling freq: 96khz
number of taps: 128
grid: 16
passband freq: 15
stopband freq: 17
passband ripple in db: 1
stopband attenuation in db: 100
window: hanning
multiplier 2.97
quantiziation: 16 bits

Values used for new filter:
sampling freq: 96khz
number of taps: 128
grid: 16
passband freq: 15.6
stopband freq: 17.6
passband ripple in db: 1
stopband attenuation in db: 100
window: hanning
multiplier 2.99595
quantiziation: 16 bits







