// 16x16 Multiplier for A(0, 15) numbers in the SRC
//

module io_AiMult16x16 (
   //Inputs
   Start, Op0, Op1, resetb, io_clk,
   //Outputs
   Done, Product
);

`define COEFF_WIDTH 16
`define COUNTER_WIDTH 4 // log 2 of COEFF_WIDTH

`define S0	2'd0
`define S1	2'd1
`define S2	2'd2
`define S3	2'd3

   // General
   input 	resetb;   // Reset
   input 	io_clk;   // Clock	

   // Control and operands
   input 	Start;    // Start Multiplication
   input [15:0]	Op0; 	  // Operand 0
   input [15:0]	Op1;      // Operand 1
   output	Done;	  // Done with Multiplication
   output[30:0]	Product;  // Multiplication result

   reg		Mult_done; 
   reg	 	Load;	  // load registers with operand values 
   reg	 	Comp;	  // complement multiplicand
   reg  	AddShift; // add and shift
   reg  	Shift;    // just shift
   wire  	Mbit;     // multiplier bit used for this cycle

   reg [15:0]	Accum;	  // accumulator
   wire [15:0]  Accum_out; 
   reg [`COEFF_WIDTH-1:0]  Multiplier;     
   reg [15:0]	Multiplicand;
 
   reg [1:0]	State, Next_State;	  // State of the Mult Controller

   reg [`COUNTER_WIDTH-1:0] cntr_val;
   reg  	cntr_enable;
   wire		cntr_done;

// Counter

always @(posedge io_clk)
 begin
   if(~resetb)
     cntr_val <= 0;
   else
     begin
       if(cntr_enable)
         begin
          if(Load || cntr_done)
	     cntr_val <= 0;
          else
             cntr_val <= cntr_val + 1;
         end 
     end
 end 

assign cntr_done = (cntr_val == 4'b1110); // 14 == `COEFF_WIDTH-2

        
assign Accum_out = Accum[15:0] + (Comp ? ~Multiplicand[15:0] + 16'd1 : Multiplicand[15:0]);

// Datapath

assign Mbit = Multiplier[0];

always @(posedge io_clk)
  begin
   if(~resetb)
     begin
       State <= `S0;
       Accum <= 0;
       Multiplier <= 0;
       Multiplicand <= 0;
     end
   else
    begin        
      State <= Next_State;
      if(Load)
	begin
	 Accum <= 0;
	 Multiplier <= Op1;
	 Multiplicand <= Op0;
	end
      else
	begin
         if (AddShift)
	   begin
             Accum [15] <= Comp^Multiplicand[15];
	     Accum [14:0] <= Accum_out[15:1];
	     Multiplier[`COEFF_WIDTH-1] <= Accum_out[0];
	     Multiplier[`COEFF_WIDTH-2:0] <= Multiplier[`COEFF_WIDTH-1:1];
	   end
         else if(Shift)
	   begin
             Accum [15] <= Accum[15];
	     Accum [14:0] <= Accum [15:1];
	     Multiplier[`COEFF_WIDTH-1] <= Accum[0];
	     Multiplier[`COEFF_WIDTH-2:0] <= Multiplier[`COEFF_WIDTH-1:1];
   	   end
	end
     end
   end

// FSM for Control Logic 
always @(State or Start or Mbit or cntr_done)
   begin
      // default values for control signals
      AddShift = 0;
      Shift = 0;
      Load = 0;
      Comp = 0;
      Mult_done = 0;
      cntr_enable = 0;
      Next_State = `S0;

         case(State)
	   `S0:
	      if(Start)
		begin
	         Next_State = `S1;
	         AddShift = 0;
	         Shift = 0;
                 Load = 1;
	     	 Comp = 0;
	     	 Mult_done = 0;
		 cntr_enable = 1;
		end
              else
		begin
	     	 Next_State = `S0;
	     	 AddShift = 0;
	     	 Shift = 0;
             	 Load = 0;
	     	 Comp = 0;
	     	 Mult_done = 0;
		 cntr_enable = 0;
	        end
           `S1:
		begin
	         if(Mbit)
		   begin
               	    AddShift = 1;
	            Shift = 0;
                    Load = 0;
		   end
                 else
		   begin
                    AddShift = 0;
                    Shift = 1;
                    Load = 0;
		   end

	      if(cntr_done)
		begin
                 Next_State = `S2;
		 cntr_enable = 0;
		end
              else
		begin
                 Next_State = `S1;
		 cntr_enable = 1;
		end
             end 
           `S2: 
	     begin
	      Next_State = `S3;
              if(Mbit)
		begin
                 Comp = 1;
                 AddShift = 1;
                 Shift = 0;
		end
              else
		begin
	         Comp = 0;
                 AddShift = 0;
                 Shift = 1;
                end
	     end
           `S3:
	     begin
              Next_State = `S0;
              Mult_done = 1;
	      AddShift = 0;
	      Shift = 0;
	      Comp = 0;
	     end
	   default:
	     Next_State = `S0;
         endcase
      end 

assign Done = Mult_done;
assign Product = {Accum[14:0], Multiplier[15:0]};

endmodule