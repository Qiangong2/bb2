// Testbench for AiClks generation
module io_AiClksTest( /*AUTOARG*/
   // Outputs
   AiClk, AiLR, AisClk, AisLR
   );
   
   output AiClk;
   output AiLR;
   output AisClk;
   output AisLR;

   reg clk;
   reg resetb;
   reg Ais48KHz;
   initial
     begin
	$vcdpluson();
	clk = 1'b0;
	resetb = 1'b0;
	Ais48KHz = 1'b0;
	#200 resetb = 1'b1;
	#100000 Ais48KHz = 1'b1;
	#100000 Ais48KHz = 1'b0;
	#020002 Ais48KHz = 1'b1;
	#020001 Ais48KHz = 1'b0;
	#100000 $finish;
     end // initial begin

   always #19 clk = ~clk; // ~27MHz

   io_AiClks io_AiClks0 (.io_clk(clk),
			 .resetb(resetb),
			 .AiLR(AiLR),
			 .AiClk(AiClk),
			 .AisClk(AisClk),
			 .AisLR(AisLR),
			 .Ais48KHz(Ais48KHz),
			 .AisClkEnable(1'b1)
			 );
			 
endmodule // io_AiClksTest
