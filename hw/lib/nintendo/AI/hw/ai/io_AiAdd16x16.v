// 16x16 Adder for the SRC
//

module io_AiAdd16x16 (
   // Inputs
   Start, Op0, Op1, Cin, resetb, io_clk, sign_ext,
   // Outputs
   Done, Sum, Cout
);

   // General
   input 	resetb;   // Reset
   input 	io_clk;   // Clock	

   // Control and operands
   input 	Start;    // Start Addition
   input [15:0]	Op0; 	  // Operand 0
   input [15:0]	Op1;      // Operand 1
   input 	Cin; 	  // Carry In
   input	sign_ext; // whether sign extension should be done or not
   output	Done;	  // Done with Addition
   output[15:0]	Sum;	  // Addition result
   output	Cout;	  // Carry Out

   reg [16:0]   Result;	  // Registered Addition result
   reg [15:0]	Addend_0;  
   reg [15:0]	Addend_1;
   reg		Carry_in;
   reg		add_done;
   reg		add_done_d1;

always @(posedge io_clk)
   begin
      if(~resetb)
        begin
          Result <= 0;
	  Addend_0 <= 0;
	  Addend_1 <= 0;
	  Carry_in <= 0;
	  add_done <= 0;
	  add_done_d1 <= 0;
        end
      else 
	begin
	  add_done <= Start;
	  add_done_d1 <= add_done;
	  if(Start)
	    begin
	      Addend_0 <= Op0;
              Addend_1 <= Op1;
              Carry_in <= Cin;
	    end
	  if(sign_ext)
            Result <= {Addend_0[15], Addend_0} + 	// sign extension
		      {Addend_1[15], Addend_1} + 	// sign extension
		      {16'b0000000000000000, Carry_in};
	  else
            Result <= {1'b0, Addend_0} + 	// sign extension
		      {1'b0, Addend_1} + 	// sign extension
		      {16'b0000000000000000, Carry_in};
        end
   end

assign Sum = Result[15:0];
assign Cout = Result[16];
assign Done = add_done_d1;

endmodule
