// control fsm for top level Ai

module io_Ai_fsm (
  // Outputs
  AisClkEnable, dsp_bypass, clear, mode, go, period, 
  // Inputs, general
  io_clk, resetb, 
  // Inputs, control
  PStat, SRC_done, AiLRPos_ioclk, AiLRNeg_ioclk, afr, AiLR
);

  // Outputs 
  output	AisClkEnable;
  output	dsp_bypass;	// msb of top level bypass signal
  output	clear;
  output[1:0]	mode;
  output	go;
  output[1:0]	period;

  // Inputs, general
  input		io_clk;
  input		resetb;

  // Inputs, control
  input		PStat;
  input		SRC_done;
  input		AiLRPos_ioclk;	
  input		AiLRNeg_ioclk;
  input		afr;
  input		AiLR; 		// used to tell if we are 
				//processing L or R data

  // Internal registers
  reg[2:0]	state;		//cisco_fsm io_Ai_fsm current_state
  reg[2:0]	next_state;	//cisco_fsm io_Ai_fsm next_state
  reg[6:0]	cntr_value;
  wire		cntr_done;  
  reg		cntr_clear;

  reg 		AisClkEnable;
  reg		dsp_bypass;
  reg 		clear;
  reg[1:0]	period_int;
  reg[1:0]	period_int_d1;
  reg		go;
  reg[1:0]	mode;

  // State definitions
parameter [2:0]  //cisco_fsm io_Ai_fsm declare
 START 		=	3'b000,
 WAIT_START	=	3'b001,
 CONV		=	3'b010,
 VOL_MULT	=	3'b011,
 DSP_ADD	=	3'b100,
 WAIT_DONE	=	3'b101,
 UNREACHABLE_0_AI =	3'b110,
 UNREACHABLE_1_AI = 	3'b111;

// output assignments
assign period = period_int_d1;

  // Internal Counter 
  // want to zero out the first 64 samples to the DAC.
  // This means that we have to wait for 43 samples from
  // the stream interface. 
  // Remember though that there is a two clock cycle delay
  // from the time we start up aisclk to when we get a sample
  // from the stream interface. Therefore, we have to wait 
  // for 43+2=45 cycles of aisclk, which means 45*1.5 = 67.5
  // cycles of aiclk (1.5 is the ratio of aiclk to aisclk). 
  // So, we set the counter to be 68. 
always @(posedge io_clk)
  begin
    if(~resetb)
      cntr_value <= 0;
    else
      if(~PStat)
	cntr_value <= 0;
      else
        begin
         if(cntr_done || cntr_clear)
           cntr_value <= 0;
         else
           if(AiLRNeg_ioclk)
             cntr_value <= cntr_value + 1;
        end
    end

  assign cntr_done = (cntr_value == 7'd68);

  // Control FSM

always @(posedge io_clk)
  begin
    if(~resetb)
      begin
        state <= START;
        period_int_d1 <= 0;
      end
    else
      begin
        state <= next_state;
        period_int_d1 <= period_int;
      end
  end

always @(state or PStat or cntr_done or AiLRPos_ioclk or AiLRNeg_ioclk or SRC_done or period_int_d1 or afr or AiLR)
begin
  // default values for control signals
  AisClkEnable = 0;
  dsp_bypass = 0;
  clear = 0;
  mode = 0;
  period_int = 0;
  go = 0;
  cntr_clear = 0;

    case(state)
      START:
       begin
	clear = 1;
	if(AiLRPos_ioclk && PStat && ~afr)
	  begin
  	    next_state = WAIT_START;
	    
	    AisClkEnable = 1;
	    dsp_bypass = 1; 
	    cntr_clear = 1;
	  end
	else
          if(AiLRPos_ioclk && PStat && afr)
            begin
 	      next_state = VOL_MULT;
	
	      go = 1;
	      AisClkEnable = 1;
	      dsp_bypass = 1; 
	      mode = 2;
            end
          else
	    begin
	      next_state = START; 

	      dsp_bypass = 1;
	      AisClkEnable = 0;
	      clear = 1;
	    end
       end
      WAIT_START:
	if(cntr_done && PStat)
	  begin
	    next_state = CONV;

	    go = 1;
 	    AisClkEnable = 1;
	    mode = 1;
	    period_int = 1;	// period=1 on the 64th cycle. 64/3=Remainder 1
	  end
	else
	  if(~PStat)
	     begin
	       next_state = START;

	       AisClkEnable = 0;
	       dsp_bypass = 1;
	     end
	  else
	    begin
	      next_state = WAIT_START;

	      AisClkEnable = 1;
	      dsp_bypass = 1;
	  end

      CONV:
	if(SRC_done)
	  begin
 	    next_state = VOL_MULT;
	
	    go = 1;
	    if(AiLR)
              begin
	        if(period_int_d1==2)
	          period_int = 0;
	        else
	          period_int = period_int_d1 + 1;
	      end
	    else 
	      period_int = period_int_d1;

	    AisClkEnable = 1;
	    mode = 2;
	  end
	else
	  begin
	    next_state = CONV; 

	    AisClkEnable = 1;
	    period_int = period_int_d1;
	  end

      VOL_MULT:
	if(SRC_done)
	  begin
	    next_state = DSP_ADD;

	    go = 1;
	    AisClkEnable = 1;
	    mode = 3;
	    period_int = period_int_d1;
	  end
	else
	  begin
	    next_state = VOL_MULT;

	    AisClkEnable = 1;
	    period_int = period_int_d1;
	  end

      DSP_ADD:
	if(SRC_done)
	  begin
	    next_state = WAIT_DONE;

	    go = 1;
	    dsp_bypass = 0;
	    AisClkEnable = 1;
	    period_int = period_int_d1;
	  end
	else 
	  begin
	    next_state = DSP_ADD;

	    AisClkEnable = 1;
	    period_int = period_int_d1;
	  end

      WAIT_DONE:
	if(AiLRPos_ioclk || AiLRNeg_ioclk)
	  if(PStat && ~afr)
	    begin
	      next_state = CONV;

	      go = 1;
	      AisClkEnable = 1;
  	      mode = 1;
	    period_int = period_int_d1;
	    end
	  else 
	    if(PStat && afr)
              begin
 	        next_state = VOL_MULT;
	
	        go = 1;
 	        period_int = period_int_d1;
	        AisClkEnable = 1;
	        mode = 2;
              end
	    else
	      begin
                next_state = START;
	      
                go = 0;
	        AisClkEnable = 0;
  	        period_int = period_int_d1;
	      end
        else
	  begin
	    next_state = WAIT_DONE;

	    dsp_bypass = 0;
	    AisClkEnable = 1;
	    period_int = period_int_d1;
	  end
      UNREACHABLE_0_AI:
	next_state = START;
      UNREACHABLE_1_AI:
        next_state = START;
      default:
	next_state = START;
    endcase
end

endmodule

