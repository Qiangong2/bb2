module io_AiMAC16x16 (/*AUTOARG*/
   // Outputs
   Result, Done, 
   // Inputs
   Op0, Op1, Add, Start, resetb, io_clk, Cin, sign_ext, clear
   );

   // General
   input 	 resetb;   // Reset
   input 	 io_clk;   // Clock
   // Control and operands
   input  [15:0] Op0;      // Operand 0
   input  [15:0] Op1;      // Operand 1
   input 	 Add;      // Add value to ACC, left shifted to match decimal point
   input 	 Start;    // Start Multiply
   input	 Cin;	   // Carry In for Adder
   input	 sign_ext; // sign_extention bit
   input	 clear;	   // clear the result

   output [30:0] Result;   // Truncated result
   output 	 Done;     // Multiply is complete

   wire 	Mult_Start, Add_Start;
   wire [30:0]	Product;
   wire [15:0]	Sum;
   wire		Cout, Cin;
   wire 	Mult_Done, Add_Done;

   reg		Done_internal;
   reg		Add_Start_Latch, Mult_Start_Latch;
   reg[30:0]	Result_int;

assign Mult_Start = Start & ~Add;
assign Add_Start = Start & Add;
assign Done = Done_internal;

assign Result = Result_int;

always @(posedge io_clk)
  begin
    if(~resetb)
      begin
        Done_internal <= 0;
	Add_Start_Latch <= 0;
	Mult_Start_Latch <= 0;
	Result_int <= 31'b0000000000000000000000000000000;
      end
    else 
      begin
	if(clear)
          begin
            Done_internal <= 0;
	    Add_Start_Latch <= 0;
	    Mult_Start_Latch <= 0;
	    Result_int <= 31'b0000000000000000000000000000000;
	  end
	else
          if((Mult_Start_Latch && Mult_Done) || (Add_Start_Latch && Add_Done))
            begin
	      Done_internal <= 1;
 	      Add_Start_Latch <= 0;
	      Mult_Start_Latch <= 0;
              Result_int[15:0] <= Mult_Start_Latch ? Product[15:0] : Add_Start_Latch ? Sum : 0;
              Result_int[16] <= Mult_Start_Latch ? Product[16] : Add_Start_Latch ? Cout : 0;
              Result_int[30:17] <= Mult_Start_Latch ? Product[30:17] : 0 ;
	    end
        else 
	  begin
	    Done_internal <= 0;
            if(Start & ~Done_internal)
              begin
                Add_Start_Latch <= Add_Start;
                Mult_Start_Latch <= Mult_Start;
              end
	  end
      end  
  end

io_AiMult16x16 io_AiMult16x16 (
				.Start(Mult_Start),
				.Op0(Op0),
				.Op1(Op1),
				.resetb(resetb),
				.io_clk(io_clk),
				.Done(Mult_Done),
				.Product(Product)
				);

io_AiAdd16x16 io_AiAdd16x16	(
				.Start(Add_Start),
				.Op0(Op0),
				.Op1(Op1),
				.Cin(Cin),
				.resetb(resetb),
				.io_clk(io_clk),
				.Done(Add_Done),
				.Sum(Sum),
				.Cout(Cout),
				.sign_ext(sign_ext)
				);




endmodule // io_AiMAC16x16

