/*******************************************************************************
 * 
 *   (C) 1999 ARTX INC..  ALL RIGHTS RESERVED.  UNPUBLISHED -- RIGHTS
 *   RESERVED UNDER THE COPYRIGHT LAWS OF THE UNITED STATES.  USE OF A
 *   COPYRIGHT NOTICE IS PRECAUTIONARY ONLY AND DOES NOT IMPLY PUBLICATION
 *   OR DISCLOSURE.
 *
 *   THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF
 *   ARTX INC..  USE, DISCLOSURE, OR REPRODUCTION IS PROHIBITED WITHOUT
 *   THE PRIOR EXPRESS WRITTEN PERMISSION OF ARTX INC..
 *
 *                   RESTRICTED RIGHTS LEGEND
 *
 *   Use, duplication, or disclosure by the Government is subject to
 *   restrictions as set forth in subparagraph (c)(1)(ii) of the Rights
 *   in Technical Data and Computer Software clause at DFARS 252.227-7013
 *   or subparagraphs (c)(1) and (2) of Commercial Computer Software --
 *   Restricted Rights at 48 CFR 52.227-19, as applicable. 
 *
 *   ArtX Inc.
 *   3400 Hillview Ave, Bldg 5
 *   Palo Alto, CA 94304
 *
 ******************************************************************************/
// AI Control Status Registers 

module io_AiCSR (/*AUTOARG*/
   // Outputs
   AiVr, AiCr, AiScnt, AiIt, 
   // Inputs
   io_clk, resetb, PiData, LdAiVrL, LdAiCrL, LdAiItH, LdAiItL, 
   AisSample, dvdaudio_mode
   );

// syn myclk = io_clk

   //////////////////////
   // I/O Declarations //
   //////////////////////

   // General
   input io_clk;
   input resetb;
   // Control Register Interfaces
   input [15:0] PiData;    // Data bus in, for reg writes
   input 	LdAiVrL;   // Load low 16-bit of AIVrL (volume control) register
   input 	LdAiCrL;   // Load low 16-bit of AICR (control) register
   input 	LdAiItH;   // Load high 16-bit of AiIt (interrupt) register
   input 	LdAiItL;   // Load low 16-bit fo AiIt (interrupt) register
   input 	AisSample; // When active, new Ais stereo sample processed, increment AiScnt
   output [`AI_VR_TOTAL_RANGE]   AiVr;    // AIVR value
   output [`AI_CR_TOTAL_RANGE]   AiCr;    // AICR value
   output [`AI_SCNT_TOTAL_RANGE] AiScnt;  // AISCNT value
   output [`AI_IT_TOTAL_RANGE] 	 AiIt;    // AIIT value
   output dvdaudio_mode;
   
   wire 		      SetAiInt; // Set AiInt when AiIt and AiScnt match
   wire [31:16] 	      PiDataH = PiData[15:0]; // Used for hi word reg PI loads
   wire	 AiInt_active;
   wire	 AiInt_active_temp;
   reg   AiInt_active_reg;

// internal signals for outputs
   reg [`AI_VR_TOTAL_RANGE]   AiVr_int;   // AiVr register
   reg [`AI_CR_TOTAL_RANGE]   AiCr_int;   // AiCr register
   reg [`AI_SCNT_TOTAL_RANGE] AiScnt_int; // AiVr register
   reg [`AI_IT_TOTAL_RANGE]   AiIt_int;   // AiIt register


// output assigments
assign AiVr = AiVr_int;
assign AiCr = AiCr_int;
assign AiScnt = AiScnt_int;
assign AiIt = AiIt_int;

   always @(posedge io_clk)
     begin
	// AIVR Definition
	AiVr_int[`AI_VR_AVRR] <= ~resetb ? 8'h00 : // Right - Reset to 0 volume (mute)
			     LdAiVrL ? PiData[`AI_VR_AVRR] : // Load from PI
			     AiVr_int[`AI_VR_AVRR];  // No change
	AiVr_int[`AI_VR_AVRL] <= ~resetb ? 8'h00 : // Left - Reset to 0 volume (mute)
			     LdAiVrL ? PiData[`AI_VR_AVRL] : // Load from PI
			     AiVr_int[`AI_VR_AVRL];  // No change
	
	// AICR Definition
	AiCr_int[`AI_CR_DSP32] <= ~resetb ? `AI_DSP32_OFF : // Reset to dsp=48KHz
			    LdAiCrL ? PiData[`AI_CR_DSP32] : // Load from PI
			    AiCr_int[`AI_CR_DSP32]; // No Change
	AiCr_int[`AI_CR_SCRST] <= ~resetb ? `AI_SCRST_NONE :         // Reset to deasserted
			      LdAiCrL ? PiData[`AI_CR_SCRST] :   // Load from PI
			      `AI_SCRST_NONE;                    // Register only gets set for 1 clock
	AiCr_int[`AI_CR_AIINTVLD] <= ~resetb ? `AI_INTVLD_VALID :    // Reset to interrupt valid
				 LdAiCrL ? PiData[`AI_CR_AIINTVLD] : // Load from PI
				 AiCr_int[`AI_CR_AIINTVLD]; // No Change
	AiCr_int[`AI_CR_AIINT] <= ~resetb ? `IO_INT_NO_REQUEST :     // Reset to no interrupt
			      SetAiInt ? `IO_INT_REQUEST :       // Set interrupt
			      (LdAiCrL && PiData[`AI_CR_AIINT]) ? `IO_INT_NO_REQUEST : // Clear interrupt
			      AiCr_int[`AI_CR_AIINT];  // No Change
	AiCr_int[`AI_CR_AIINTMSK] <= ~resetb ? `IO_INTMSK_MASKED : // Reset to masked
				 LdAiCrL ? PiData[`AI_CR_AIINTMSK] : // Load from PI
				 AiCr_int[`AI_CR_AIINTMSK]; // No Change
	AiCr_int[`AI_CR_AFR] <= ~resetb ? `AI_AFR_32K : // Reset to 32KHz
			    LdAiCrL ? PiData[`AI_CR_AFR] : // Load from PI
			    AiCr_int[`AI_CR_AFR]; // No Change
	AiCr_int[`AI_CR_PSTAT] <= ~resetb ? `AI_PSTAT_STOP : // Reset to stopped
			      LdAiCrL ? PiData[`AI_CR_PSTAT] : // Load from PI
			      AiCr_int[`AI_CR_PSTAT]; // No change
	AiIt_int[`IO_REG_HI] <= ~resetb ? 0 : // Reset to 0
			    LdAiItH ? PiDataH[`IO_REG_HI] : // Load from PI
			    AiIt_int[`IO_REG_HI]; // No change
	AiIt_int[`IO_REG_LO] <= ~resetb ? 0 : // Reset to 0
			    LdAiItL ? PiData[`IO_REG_LO] : // Load from PI
			    AiIt_int[`IO_REG_LO]; // No change

	// AiScnt
	AiScnt_int <= ~resetb || AiCr_int[`AI_CR_SCRST] ? 0 : // Reset to 0 on reset and SCRESET
		  AisSample ? AiScnt_int + 1 : // If AisSample output increment
		  AiScnt_int; // No Change
     end // always @ (posedge io_clk)

   // The interrupt will only be set for one clock cycle
   assign SetAiInt = AiInt_active;

   always @ (posedge io_clk)
     begin
       if(~resetb)
          AiInt_active_reg <= 0;
       else
          AiInt_active_reg <= AiInt_active_temp;
     end

   assign AiInt_active = ~AiInt_active_reg && AiInt_active_temp;

   assign AiInt_active_temp = (AiIt_int[`AI_IT_AIIT] == AiScnt_int[`AI_SCNT_AISCNT]) && ~AiCr_int[`AI_CR_AIINTVLD] && ~((|AiScnt_int)==0);   

   assign dvdaudio_mode = AiIt_int[`AI_IT_DVDAUDIO];

endmodule // io_AiCSR
