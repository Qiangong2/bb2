// src control fsm

module io_AiSRC_fsm (
  // Outputs
  read_addr, MAC_add, MAC_start, MAC_cin, Select_op0, Select_op1, temp_reg_clear, temp_reg_enable, Accum_enable, Accum_select, Accum_clear, Done, sign_ext, read_enable, Accum_clamp,
  // Inputs, general
  io_clk, resetb,
  // Inputs, control
  newest, oldest, go, mode, MAC_done, MAC_cout, freq, LR, period, AiLRNeg_ioclk
);

  // Outputs
  output[5:0]	read_addr;
  output	read_enable;
  output	MAC_add;
  output	MAC_start;
  output	MAC_cin;
  output[2:0]	Select_op0;
  output[6:0]	Select_op1;
  output	temp_reg_clear;
  output	temp_reg_enable;
  output	Accum_enable;
  output[1:0]	Accum_select;
  output	Accum_clear;
  output	Done;
  output	sign_ext;
  output 	Accum_clamp;

  // Inputs, general
  input		io_clk;
  input		resetb;

  // Inputs, control
  input[5:0]	newest;
  input[5:0]	oldest;
  input		go;
  input[1:0]	mode;
  input		MAC_done;
  input		MAC_cout;
  input		freq;
  input		LR;
  input[1:0]	period;
  input		AiLRNeg_ioclk;

  // Internal registers
  reg[3:0]	state;		//cisco_fsm io_AiSRC_fsm current_state
  reg[3:0]	next_state;	//cisco_fsm io_AiSRC_fsm next_state
  reg[6:0]	newest_ptr;
  reg[6:0]	newest_ptr_reg;
  reg[6:0]	newest_ptr_minus_1_reg;
  reg[5:0]	newest_reg;
  reg[5:0]	oldest_reg;
  reg		mult_flag;
  reg		mult_flag_reg;
  reg		mult_flag_enable;
  reg[5:0]	i; 
  reg[5:0]	i_reg;
  reg		i_reg_enable;
  reg[8:0]	three_i;
  reg[8:0]	three_i_reg;
  reg[1:0]	two_j_mod_three_reg;
  reg[8:0]	index;		// index = 3i + (2j mod 3)

  // Internal signals which will eventually be outputs
  reg[2:0]	Select_op0_int;
  reg[6:0]	Select_op1_int;
  reg[5:0]	read_addr_int;
  reg		read_enable_int;
  reg		MAC_add_int;
  reg		MAC_start_int;
  reg		MAC_cin_int;
  reg		temp_reg_clear_int;
  reg		temp_reg_enable_int;
  reg		Accum_enable_int;
  reg[1:0]	Accum_select_int;
  reg		Accum_clear_int;
  reg		Done_int;
  reg		sign_ext_int;
  reg		Accum_clamp_int;

  // registers for outputs
  reg[2:0]	Select_op0_reg;
  reg[6:0]	Select_op1_reg;
  reg[5:0]	read_addr_reg;
  reg		read_enable_reg;
  reg		MAC_add_reg;
  reg		MAC_start_reg;
  reg		MAC_cin_reg;
  reg		temp_reg_clear_reg;
  reg		temp_reg_enable_reg;
  reg		Accum_enable_reg;
  reg[1:0]	Accum_select_reg;
  reg		Accum_clear_reg;
  reg		Done_reg;
  reg		sign_ext_reg;
  reg		Accum_clamp_reg;
  
// State definitions
parameter [3:0]  //cisco_fsm io_AiSRC_fsm declare
SRC_START		=4'b0000,
SRC_VOL_MULT		=4'b0001,
SRC_VOL_MULT_WAIT	=4'b0010,
UNREACHABLE_0		=4'b0011,
SRC_DSP_ADD		=4'b0100,
SRC_DSP_ADD_WAIT	=4'b0101,
UNREACHABLE_1		=4'b0110,
UNREACHABLE_2		=4'b0111,
SRC_CONV		=4'b1000,
SRC_CONV_MULT_WAIT	=4'b1001,
SRC_CONV_READ_SETUP	=4'b1010,
SRC_CONV_ADD_WAIT_0	=4'b1011,
SRC_CONV_ADD_WAIT_1 	=4'b1100,
SRC_CONV_ADD_WAIT_2 	=4'b1101,
UNREACHABLE_3		=4'b1110,
UNREACHABLE_4		=4'b1111;

// Output assignments
assign Select_op0 = Select_op0_reg;
assign Select_op1 = Select_op1_reg;
assign read_addr = read_addr_reg;
assign read_enable = read_enable_reg;
assign MAC_add = MAC_add_reg;
assign MAC_start = MAC_start_reg;
assign MAC_cin = MAC_cin_reg;
assign temp_reg_clear = temp_reg_clear_reg;
assign temp_reg_enable = temp_reg_enable_reg;
assign Accum_enable = Accum_enable_reg;
assign Accum_select = Accum_select_reg;
assign Accum_clear = Accum_clear_reg;
assign Done = Done_reg;
assign sign_ext = sign_ext_reg;
assign Accum_clamp = Accum_clamp_reg;

// register for timing
always @(posedge io_clk)
  begin
    if(~resetb)
      begin
	Select_op0_reg <= 3'b000;
	Select_op1_reg <= 7'b0000000;
	read_addr_reg <= 6'b000000;
	read_enable_reg <= 1;
	MAC_add_reg <= 1'b0;
	MAC_start_reg <= 1'b0;
	MAC_cin_reg <= 1'b0;
	temp_reg_clear_reg <= 1'b1;
	temp_reg_enable_reg <= 1'b0;
	Accum_enable_reg <= 1'b0;
	Accum_select_reg <= 2'b00;
	Accum_clear_reg <= 1'b1;
	Done_reg <= 1'b0;
	sign_ext_reg <= 1'b0;
	Accum_clamp_reg <= 1'b0;
      end
    else
      begin
	Select_op0_reg <= Select_op0_int;
	Select_op1_reg <= Select_op1_int;
	read_addr_reg <= read_addr_int;
	read_enable_reg <= read_enable_int;
	MAC_add_reg <= MAC_add_int;
	MAC_start_reg <= MAC_start_int;
	MAC_cin_reg <= MAC_cin_int;
	temp_reg_clear_reg <= temp_reg_clear_int;
	temp_reg_enable_reg <= temp_reg_enable_int;
	Accum_enable_reg <= Accum_enable_int;
	Accum_select_reg <= Accum_select_int;
	Accum_clear_reg <= Accum_clear_int;
	Done_reg <= Done_int;
	sign_ext_reg <= sign_ext_int;
	Accum_clamp_reg <= Accum_clamp_int;
      end
  end

// Internal data structures
always @(posedge io_clk)
  begin
    if(~resetb)
      begin
        newest_reg <= 0;
        oldest_reg <= 0;
//        index <= 0;
	newest_ptr_reg <= 0;
	newest_ptr_minus_1_reg <= 0;
        mult_flag_reg <= 0;
	two_j_mod_three_reg <= 2'b00;
	i_reg <= 6'b000000;
	three_i_reg <= 9'b000000000;
	index <= 9'b000000000;
      end
    else
      begin
	// if we are in start waiting for L data to get shifted in then keep grabbing pointer info
	if((state == SRC_START) && AiLRNeg_ioclk)	
	  begin
	    newest_reg <= newest;
	    oldest_reg <= oldest;
	  end
        if(i_reg_enable)
	  begin
            i_reg <= i;
            three_i_reg <= three_i;
          end
	// switch bits of period because:
	// j	2j	2jmod3
	// 0	0	0
	// 1	2	2
	// 2	4	1
	// 0	0	0
	two_j_mod_three_reg[0] <= period[1];
	two_j_mod_three_reg[1] <= period[0];

        index <= three_i_reg + {7'b0000000, two_j_mod_three_reg};
        newest_ptr_reg <= newest_ptr;
	if(newest_ptr == 0)
	  newest_ptr_minus_1_reg <= 7'd42;
	else
	  newest_ptr_minus_1_reg <= newest_ptr - 7'd1;
  	if(mult_flag_enable)
          mult_flag_reg <= mult_flag;
      end
  end

// Control FSM
always @(posedge io_clk)
  begin
    if(~resetb)
      state <= SRC_START;
    else
      state <= next_state;
  end

always @(state or go or mode or MAC_done or newest_ptr_reg or newest_ptr_minus_1_reg or i_reg or three_i_reg or oldest_reg or mult_flag_reg or LR or newest or oldest or index or MAC_cout or newest_reg or AiLRNeg_ioclk or read_addr_reg or freq)
  begin
    // default values for control signals
    newest_ptr = 0;
    read_addr_int = 0;
    read_enable_int = 1;
    mult_flag = 0;
    mult_flag_enable = 0;
    Accum_clear_int = 1;
    Accum_enable_int = 0;
    Accum_select_int = 0;
    Accum_clamp_int = 0;
    MAC_add_int = 0;
    MAC_start_int = 0;
    MAC_cin_int = 0;
    sign_ext_int = 0;    
    Select_op0_int = 0;
    Select_op1_int = 0;
    Done_int = 0;
    three_i = 0;   
    i = 0;
    i_reg_enable = 0;
    temp_reg_clear_int = 0;
    temp_reg_enable_int = 0;
    next_state = SRC_START;

      case(state)
        SRC_START: 
	  if(~go || (go&&(mode==0)))
	    next_state = SRC_START;
          else
	   begin 
	     if(mode==1)
	       begin
                 next_state = SRC_CONV;

	         i = 0;
	         Accum_clear_int = 1;
		 mult_flag_enable = 1;
	         mult_flag = 0;

		 // only change newest and oldest values when processing L
		 // need to get from newest instead of newest_reg when
		 // processing L due to timing reasons.
		 if(AiLRNeg_ioclk)
                  begin
	           if(newest < oldest)
		      newest_ptr = {1'b0, newest} + `K;
	           else
		      newest_ptr = {1'b0, newest};
	           read_addr_int = newest;
		   read_enable_int = 1'b0;
		  end
		 else
		  begin
	           if(newest_reg < oldest_reg)
		      newest_ptr = {1'b0, newest_reg} + `K;
	           else
		      newest_ptr = {1'b0, newest_reg};
	           read_addr_int = newest_reg;
		   read_enable_int = 1'b0;
		  end
	       end
	     else
	       begin 
	         if(mode==2)
	           begin
	             next_state = SRC_VOL_MULT;
	      
// when LR is high, that means we're reading in the Left sample.
// Therefore, we should be processing the Right sample. and vice versa.
	             if(LR)	    
		       Select_op0_int = 3'b011;	// VR
	             else
		       Select_op0_int = 3'b010;	// VL
		     if(freq)
		       Select_op1_int = 7'b1000000;
		     else
	      	       Select_op1_int = 7'b1000100;	// Accum[30:15]
	             MAC_add_int = 0;
	             MAC_start_int = 1;
	             MAC_cin_int = 0;
	             Accum_clear_int = 0;
	           end
	         else 
	           begin
	             next_state = SRC_DSP_ADD;

	             if(LR)
		       Select_op0_int = 3'b100;	// AIDR
	             else
		       Select_op0_int = 3'b101;	// AIDL
		     if(freq)
		       Select_op1_int = 7'b1000100;
		     else
	               Select_op1_int = 7'b1000100;	// Accum[30:15]
	             MAC_add_int = 1; 
	             MAC_start_int = 1;
	             MAC_cin_int = 0;
	             Accum_clear_int = 0;
		     sign_ext_int = 1;
	           end
               end
	   end
	SRC_VOL_MULT:
	  begin
	    next_state = SRC_VOL_MULT_WAIT;

	    if(LR)
	      Select_op0_int = 3'b100;	// AIDR
	    else
	      Select_op0_int = 3'b101;	// AIDL
	    if(freq)
	      Select_op1_int = 7'b1000000;
	    else
	      Select_op1_int = 7'b1000100;	// Accum[30:15]
            Accum_clear_int = 0;
          end
	SRC_VOL_MULT_WAIT:
	  if(~MAC_done)
	    begin
	      next_state = SRC_VOL_MULT_WAIT;

	      if(LR)
		Select_op0_int = 3'b100;	// AIDR
	      else
		Select_op0_int = 3'b101;	// AIDL
	      if(freq)
	        Select_op1_int = 7'b1000000;
	      else
                Select_op1_int = 7'b1000100;	// Accum[30:15]
              Accum_clear_int = 0;
            end
	  else 
	    begin
	      next_state = SRC_START;

	      Done_int = 1;
	      Accum_enable_int = 1;
	      Accum_select_int = 0;
              Accum_clear_int = 0;
	    end

	SRC_DSP_ADD:
	   begin
	     next_state = SRC_DSP_ADD_WAIT;

	     if(LR)
	       Select_op0_int = 3'b100;	// AIDR
             else
	       Select_op0_int = 3'b101;	// AIDL
	     if(freq)
	       Select_op1_int = 7'b1000100;
	     else
               Select_op1_int = 7'b1000100;	// Accum[30:15]
             Accum_clear_int = 0;
           end
	SRC_DSP_ADD_WAIT:
	  if(~MAC_done)
	    begin
	      next_state = SRC_DSP_ADD_WAIT;

	      if(LR)
	        Select_op0_int = 3'b100;	// AIDR
              else
	        Select_op0_int = 3'b101;	// AIDL
	      if(freq)
	        Select_op1_int = 7'b1000100;
	      else
                Select_op1_int = 7'b1000100;	// Accum[30:15]
              Accum_clear_int = 0;
	    end
	  else
	    begin
	      next_state = SRC_START;

	      Done_int = 1;
              Accum_clear_int = 0;
	    end

	SRC_CONV:
	    begin
	      next_state = SRC_CONV_MULT_WAIT;

	      read_addr_int = read_addr_reg;
	      Select_op0_int = 3'b000;		// AISdata;
	      if(index < 9'd64)
		Select_op1_int = index[6:0];
	      else
	        begin
		  if(index > 9'd127)
		    // index > 127 means that we've tried to 
		    // access a filter coefficient that doesn't
		    // exist. We should return 0 for this case
		    // Making the Select_op1_int select FIR
		    // coefficient 0 only works in this case
		    // because the value of FIR0 is actually 0.
		    // if it wasn't we'd have to put a zero entry
		    // in the Op1 mux.
		    Select_op1_int = 7'd0000000; 
		  else
		    Select_op1_int = 7'd127-index;
		end
	      MAC_add_int = 0;
	      MAC_start_int = 1;
	      MAC_cin_int = 0;
              Accum_clear_int = 0;
	      newest_ptr = newest_ptr_reg;
	    end

	SRC_CONV_MULT_WAIT:
	  if(~MAC_done)
           begin
	    next_state = SRC_CONV_MULT_WAIT;

	      newest_ptr = newest_ptr_reg;
	      read_addr_int = read_addr_reg;
              Accum_clear_int = 0;
           end
	  else 
	    begin
	      next_state = SRC_CONV_READ_SETUP;

	      newest_ptr = newest_ptr_reg;
	      read_addr_int = read_addr_reg;
	      i = i_reg + 1;
	      three_i = three_i_reg + 3;
              i_reg_enable = 1;

	      if(mult_flag_reg)
		begin
		  Accum_enable_int = 0;
		  temp_reg_enable_int = 1;
		end
	      else
		begin
		  Accum_enable_int = 1;
		  Accum_select_int = 0;
		end
              Accum_clear_int = 0;
	    end
	SRC_CONV_READ_SETUP:
	  if(mult_flag_reg)
	    begin
	      next_state = SRC_CONV_ADD_WAIT_0;

	      newest_ptr = newest_ptr_reg;
	      read_addr_int = read_addr_reg;
	      Select_op0_int = 3'b110;		// temp_reg[15:0]
	      Select_op1_int = 7'b1000001;	// Accum[15:0]
	      MAC_add_int = 1;
	      MAC_start_int = 1;
	      MAC_cin_int = 0;
              Accum_clear_int = 0;
	      sign_ext_int = 0;
	    end
	  else 
	    begin
	      next_state = SRC_CONV;

	      newest_ptr = newest_ptr_reg;
	      if(newest_ptr_minus_1_reg < `K)
		read_addr_int = newest_ptr_minus_1_reg;
	      else
		read_addr_int = newest_ptr_minus_1_reg - 7'd43;
	      read_enable_int = 1'b0;
	      mult_flag_enable = 1;
	      mult_flag = 1;
	      newest_ptr = newest_ptr_minus_1_reg;
              Accum_clear_int = 0;
	    end
	SRC_CONV_ADD_WAIT_0:
	  if(~MAC_done)
            begin
  	      next_state = SRC_CONV_ADD_WAIT_0;

	      newest_ptr = newest_ptr_reg;
	      read_addr_int = read_addr_reg;
              Accum_clear_int = 0;
	      Select_op0_int = 3'b110;		// temp_reg[15:0]
	      Select_op1_int = 7'b1000001;	// Accum[15:0]
            end
	  else 
	    begin
	      next_state = SRC_CONV_ADD_WAIT_1;

	      newest_ptr = newest_ptr_reg;
	      read_addr_int = read_addr_reg;
	      Select_op0_int = 3'b111;		// {temp_reg[30], temp_reg[30:16]
	      Select_op1_int = 7'b1000010;	// Accum[31:16]
	      MAC_add_int = 1;
	      MAC_start_int = 1;
	      MAC_cin_int = MAC_cout;
	      sign_ext_int = 0;
	      Accum_enable_int = 1;
	      Accum_select_int = 1;
              Accum_clear_int = 0;
	    end

	SRC_CONV_ADD_WAIT_1:
	  if(~MAC_done)
           begin
	     next_state = SRC_CONV_ADD_WAIT_1;

	      newest_ptr = newest_ptr_reg;
	      read_addr_int = read_addr_reg;
             Accum_clear_int = 0;
           end
	  else 
	    begin
	      next_state = SRC_CONV_ADD_WAIT_2;

	      newest_ptr = newest_ptr_reg;
	      read_addr_int = read_addr_reg;
	      Select_op0_int = 3'b001;		// {16{temp_reg[30]}} - sign extension
	      Select_op1_int = 7'b1000011;	// Accum[47:32]
	      MAC_add_int = 1;
	      MAC_start_int = 1;
	      MAC_cin_int = MAC_cout;
	      sign_ext_int = 1;
	      Accum_enable_int = 1;
	      Accum_select_int = 2;
              Accum_clear_int = 0;
	    end

	SRC_CONV_ADD_WAIT_2:
	  if(~MAC_done)
            begin
	      next_state = SRC_CONV_ADD_WAIT_2;

	      newest_ptr = newest_ptr_reg;
	      read_addr_int = read_addr_reg;
              Accum_clear_int = 0;
	    end
	  else 
	     begin
	       Accum_enable_int = 1;
	       Accum_select_int = 3;
               Accum_clear_int = 0;
  	       temp_reg_clear_int = 1;  	// do the clear of temp reg now

	       if(newest_ptr_reg == {1'b0, oldest_reg})
	         begin
	           next_state = SRC_START;
		
		   Accum_clamp_int = 1;
		   Done_int = 1;	// indicate that the conv is done
		   // reset i and three_i (and therefore the index)
		   i = 0;			
		   three_i = 0;
		   i_reg_enable = 1;
		 end
	       else 				// if(i < `K)
	         begin
	           next_state = SRC_CONV; 

		   if(newest_ptr_minus_1_reg < `K)
		     read_addr_int = newest_ptr_minus_1_reg;
		   else
		     read_addr_int = newest_ptr_minus_1_reg - 7'd43;	    
		   read_enable_int = 1'b0;
		   newest_ptr = newest_ptr_minus_1_reg;
	         end
	     end
	UNREACHABLE_0:
          next_state = SRC_START;
	UNREACHABLE_1:
          next_state = SRC_START;
	UNREACHABLE_2:
          next_state = SRC_START;
	UNREACHABLE_3:
          next_state = SRC_START;
	UNREACHABLE_4:
          next_state = SRC_START;
        default: 
          next_state = SRC_START;
      endcase
  end

endmodule
