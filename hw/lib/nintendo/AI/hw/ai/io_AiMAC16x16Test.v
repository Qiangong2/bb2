module io_Ai8x16Test( /*AUTOARG*/
   // Outputs
   Result, Done
   );
   
   output [15:0] Result;
   output 	 Done;

   
   reg clk;
   reg resetb;
   reg [15:0] Volume, Sample;
   reg Start;
   wire Done;

   initial
     begin
	$vcdpluson();
	clk = 1'b0;
	resetb = 1'b0;
	#40 resetb = 1'b1;
     end // initial begin

   always #5 clk = ~clk;
   reg [15:0] i;
   reg [31:0] Check;

   initial
     begin
	Start = 0;
	Check = 0;
	Volume = 16'h0102;
	Sample = 16'h8120;
	#100;
	for (i=0;i<8000;i=i+1)
	  begin
	     Volume = i;
	     Sample = i+1;
	     Check = (Volume * Sample) + Check;
	     Start = 1;
	     #10;
	     Start = 0;
	     while (!Done)
	       begin
		  #1 Sample = i+1;
	       end // while (!Done)
	     if (Result != Check[31:16])
	       $display("Error %d %h %h",$time,Result,Check);
	  end // for (i=0;i<100;i++)
        #50; $finish();
    end // initial begin

   io_AiMAC16x16 io_AiMAC16x160 (.io_clk(clk),
				 .resetb(resetb),
				 .Sample(Sample),
				 .Volume(Volume),
				 .Done(Done),
				 .Start(Start),
				 .Result(Result),
				 .ClearAcc(0),
				 .Add(0)
				 );
			 
endmodule // io_Ai8x16Test
