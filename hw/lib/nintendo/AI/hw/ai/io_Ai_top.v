/*******************************************************************************
 * 
 *   (C) 1999 ARTX INC..  ALL RIGHTS RESERVED.  UNPUBLISHED -- RIGHTS
 *   RESERVED UNDER THE COPYRIGHT LAWS OF THE UNITED STATES.  USE OF A
 *   COPYRIGHT NOTICE IS PRECAUTIONARY ONLY AND DOES NOT IMPLY PUBLICATION
 *   OR DISCLOSURE.
 *
 *   THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF
 *   ARTX INC..  USE, DISCLOSURE, OR REPRODUCTION IS PROHIBITED WITHOUT
 *   THE PRIOR EXPRESS WRITTEN PERMISSION OF ARTX INC..
 *
 *                   RESTRICTED RIGHTS LEGEND
 *
 *   Use, duplication, or disclosure by the Government is subject to
 *   restrictions as set forth in subparagraph (c)(1)(ii) of the Rights
 *   in Technical Data and Computer Software clause at DFARS 252.227-7013
 *   or subparagraphs (c)(1) and (2) of Commercial Computer Software --
 *   Restricted Rights at 48 CFR 52.227-19, as applicable. 
 *
 *   ArtX Inc.
 *   3400 Hillview Ave, Bldg 5
 *   Palo Alto, CA 94304
 *
 ******************************************************************************/

module io_Ai_top (/*AUTOARG*/
   // Outputs
   io_dspPop, AiPiData, ai_piInt, aid, ailr, aiclk, aislr, aisclk, 
   BistErr, dvdaudio_mode,
   // Inputs
   io_clk, io_VidClk, resetb, dsp_ioL, dsp_ioR, PiAddr, PiLoad, 
   PiData, PiAiSel, aisd, BistEnable, BistAddr, BistData1, BistRdn, 
   BistWen96, BistCmp96
   );

// syn myclk = io_clk

   //////////////////////
   // I/O Declarations //
   //////////////////////

   // General
   input io_clk;    		// System clock
   input io_VidClk; 		// 27MHz clock
   input resetb;    		// System reset

   // from DSP_AI
   input[15:0] 	dsp_ioL;
   input[15:0] 	dsp_ioR;
   output 	io_dspPop;

   // io_pi
   input[4:1]	 PiAddr;    	// Address from io_pi
   input 	 PiLoad;    	// Load Register from io_pi
   input[15:0]   PiData;    	// Data bus in from io_pi
   output[15:0]  AiPiData;  	// Data bus out to io_pi
   input 	 PiAiSel;   	// Ai select
   output 	 ai_piInt;  	// Ai Interrupt

   // AI - Audio DAC Interface and streaming audio interface
   output 	aid;		// dac data 
   output 	ailr;		// dac left-right
   output 	aiclk;		// dac clock 
   input 	aisd;		// stream data 
   output 	aislr;		// stream left-right
   output 	aisclk;		// stream clock 

   // BIST
   input 	BistEnable;	
   input 	BistAddr;
   input 	BistData1;
   input 	BistRdn;
   input 	BistWen96;
   input 	BistCmp96;
   output[1:0] 	BistErr;

   // dvd audio
   output	dvdaudio_mode;

   wire		dsp48_aid;
   wire[15:0]	dsp48_dsp_ioL;
   wire[15:0]	dsp48_dsp_ioR;
   wire		dsp48_io_dspPop;
   wire 	dsp48_BistErr;
   wire[15:0]	dsp48_src_result;

   wire[15:0]	dsp32_dsp_ioL;
   wire[15:0]	dsp32_dsp_ioR;
   wire		dsp32_io_dspPop;
   wire 	dsp32_BistErr;
   wire[15:0]	dsp32_src_result;

   wire		dsp32;
   reg[15:0]	aid_sum;
   wire		resetb_sync;

   // Adder Signals
   wire[15:0]	Sum;
   wire		Cout;
   wire		sign_ext;
   wire		Add_Done;

   wire 	AiLRPos;
   wire 	AiLRNeg;
   wire		AiClkNeg;
   reg[15:0]	Sum_reg;
   reg		chk_over_under;
   reg		dsp32_msb_d1;
   reg		dsp48_msb_d1;

   reg 		dsp32_delay;

assign BistErr = {dsp32_BistErr, dsp48_BistErr};

// do the muxing based on dsp32_delay

// inputs
assign dsp48_dsp_ioL = dsp32_delay ? 16'h0000 : dsp_ioL;
assign dsp48_dsp_ioR = dsp32_delay ? 16'h0000 : dsp_ioR;
assign dsp32_dsp_ioL = dsp32_delay ? dsp_ioL : 16'h0000;
assign dsp32_dsp_ioR = dsp32_delay ? dsp_ioR : 16'h0000;

// outputs
assign io_dspPop = dsp32_delay ? dsp32_io_dspPop : dsp48_io_dspPop;
assign aid = dsp32_delay? aid_sum[15] : dsp48_aid;

assign sign_ext = 0;

always @(posedge io_clk)
  begin
    if(~resetb_sync)
	dsp32_delay <= 0;
    else
	if(AiLRPos)
	  dsp32_delay <= dsp32;
  end

always @(posedge io_clk)
begin
  if(~resetb_sync)
    aid_sum <= 16'h0000;
  else
   begin
     if(AiLRPos | AiLRNeg)
       begin
         aid_sum <=  Sum_reg;
       end
     else
       begin
	 if(AiClkNeg)
           aid_sum <= {aid_sum[14:0], 1'b0};	 
       end
   end
end

// register the sum and do the overflow/underflow clamping
always @(posedge io_clk)
begin
  if(~resetb_sync)
    begin
      Sum_reg <= 16'h0000;
      chk_over_under <= 0;
    end
  else
    begin
      dsp32_msb_d1 <= dsp32_src_result[15];
      dsp48_msb_d1 <= dsp48_src_result[15];
      chk_over_under <= ~(dsp32_msb_d1 ^ dsp48_msb_d1);
      Sum_reg <= chk_over_under ?
				(Cout ^ Sum[15]) ? Cout ? 16'h8000 // under
							: 16'h7fff // over
						 : Sum 
				: Sum;
    end
end

  io_Ai io_Ai_dsp48	(
			//outputs from module
			.io_dspPop(dsp48_io_dspPop),
			.AiPiData(AiPiData),
			.ai_piInt(ai_piInt),
			.aid(dsp48_aid),
			.ailr(ailr),
			.aiclk(aiclk),
			.aislr(aislr),
			.aisclk(aisclk),
			.BistErr(dsp48_BistErr),
			.dsp32(dsp32),
			.AiLR_out(AiLR_out),
			.AiLRPos(AiLRPos),
			.AiLRNeg(AiLRNeg),
			.AiClkNeg_out(AiClkNeg),
			.resetb_sync_out(resetb_sync),
			.AisLR_32khz(AisLR_32khz),
			.AisLR_32khzPos(AisLR_32khzPos),
			.AisLR_32khzNeg(AisLR_32khzNeg),
			.AisClk_32khzPos(AisClk_32khzPos),
			.AisClk_32khzNeg(AisClk_32khzNeg),
			.dvdaudio_mode(dvdaudio_mode),
			//inputs to module
			.io_clk(io_clk),
			.io_VidClk(io_VidClk),
			.resetb(resetb),
			.dsp_ioL(dsp48_dsp_ioL),
			.dsp_ioR(dsp48_dsp_ioR),
			.PiAddr(PiAddr),
			.PiLoad(PiLoad),
			.PiData(PiData),
			.PiAiSel(PiAiSel),
			.aisd(aisd),
			.BistEnable(BistEnable),
			.BistAddr(BistAddr),
			.BistData1(BistData1),
			.BistRdn(BistRdn),
			.BistWen96(BistWen96),
			.BistCmp96(BistCmp96),
			.SRC_result_out(dsp48_src_result)
			);

  io_Ai_dsp32 io_Ai_dsp32 (
			//outputs from module
			.io_dspPop(dsp32_io_dspPop),
			.BistErr(dsp32_BistErr),
			.SRC_result(dsp32_src_result),
			.io_clk(io_clk),
			.resetb_sync(resetb_sync),
			.dsp_ioL(dsp32_dsp_ioL),
			.dsp_ioR(dsp32_dsp_ioR),
			.BistEnable(BistEnable),
			.BistAddr(BistAddr),
			.BistData1(BistData1),
			.BistRdn(BistRdn),
			.BistWen96(BistWen96),
			.BistCmp96(BistCmp96),	
			.dsp32(dsp32),
			.AiLR_sync(AiLR_out),
			.AiLRPos_ioclk(AiLRPos),
			.AiLRNeg_ioclk(AiLRNeg),
			.AisLR_sync(AisLR_32khz),
			.AisLRPos_ioclk(AisLR_32khzPos),
			.AisLRNeg_ioclk(AisLR_32khzNeg),
			.AisClkPos_ioclk(AisClk_32khzPos),
			.AisClkNeg_ioclk(AisClk_32khzNeg)
			);


io_AiAdd16x16 io_AiAdd16x16     (
                                .Start(1'b1),
                                .Op0(dsp32_src_result),
                                .Op1(dsp48_src_result),
                                .Cin(1'b0),
                                .resetb(resetb_sync),
                                .io_clk(io_clk),
                                .Done(Add_Done),
                                .Sum(Sum),
                                .Cout(Cout),
                                .sign_ext(sign_ext)
                                );

endmodule // io_Ai_top

