// AI Memory and Control

//`timescale 1ns/100ps		// just for standalone compilation purposes

module io_AiMem (
  // Outputs
  read_data, newest, oldest, BistErr,
  // Inputs, general
  io_clk, resetb, 
  // Inputs
  clear, AisClkPos_ioclk, AisClkNeg_ioclk, AisLR, AisD, read_addr, 
  AisLRPos, AisLRNeg,
  BistEnable, BistAddr, BistData1, BistRdn, BistWen96, BistCmp96, afr,
  AisSample, AiLR, read_enable
);

  // Outputs
  output[15:0]	read_data;
  output[5:0]	newest;
  output[5:0]	oldest;

  // Inputs, general
  input		io_clk;
  input		resetb;

  // Inputs
  input		clear;
  input		AisClkPos_ioclk;
  input		AisClkNeg_ioclk;
  input 	AisLR;
  input 	AisLRPos;
  input 	AisLRNeg;
  input		AisD;
  input[5:0]	read_addr;
  input		afr;
  input		AisSample;
  input		AiLR;
  input		read_enable;

  // BIST
  input 	BistEnable;
  input 	BistAddr;
  input 	BistData1;
  input 	BistRdn;
  input 	BistWen96;
  input 	BistCmp96;
  output 	BistErr;

  // Bist
  reg 		d1BistEnable;
  reg 		d1BistAddr;
  reg 		d1BistData1;
  reg 		d1BistRdn;
  reg 		d1BistWen96;
  reg 		d1BistCmp96;

  // Internal signals
  reg		write_enable;
  reg		write_enable_d1;
  wire[6:0]	write_addr;
  reg		write_data_select;
  wire[6:0]	read_addr_int;
  wire[15:0]	write_data;
  wire[15:0]	read_data_mem;
  reg[6:0]	newest_temp;
  reg[6:0]	oldest_temp;
  
  // Internal registers and structures
  reg[15:0]	AisDL;
  reg[15:0]	AisDR;
  reg		LR_int;
  reg		AiLR_int;
  reg[6:0]	newest_L;
  reg[6:0]	newest_L_out;
  reg[6:0]	newest_R;
  reg[6:0]	oldest_L;
  reg[6:0]	oldest_L_out;
  reg[6:0]	oldest_R;
  reg		counter_val;
  reg		AisLR_d1;
  reg		AisLR_d2;
  reg		AisLR_latch;

  // logic to figure when to shift in new data on the stream interface..

  // AISD input registers
always @(posedge io_clk)
  begin
    if(~resetb)
      begin
        AisDL <= 16'h0000;
        AisDR <= 16'h0000;
	AisLR_latch <= 0;
      end
    else
      if(clear)
	begin
          AisDL <= 16'h0000;
          AisDR <= 16'h0000;
	  AisLR_latch <= 0;
        end
      else
	begin
	  if(AisClkPos_ioclk) 
	    begin
	      // need delayed version of aislr since we need to clock in
	      // data on the first rising edge of aisclk AFTER the 
	      // change in aislr
              if(AisLR_latch)
                AisDL[15:0] <= {AisDL[14:0], AisD};
              else
                AisDR[15:0] <= {AisDR[14:0], AisD};	
	    end
	  if(AisClkNeg_ioclk)
	    AisLR_latch <= AisLR;
	end
  end

  assign write_data = write_data_select ? AisDL : AisDR;

  // internal signal generation
  // write stuff for memory

  assign write_addr = LR_int ? newest_L : newest_R + `K;

always @(posedge io_clk)
  begin
    if(~resetb)
      counter_val <= 0;
    else
      if(clear)
	counter_val <= 0;
      else
        if(counter_val == 0)
          if(AisSample)
             counter_val <= 1;
  end

  always @(posedge io_clk)
    begin
      if(~resetb)
        begin
	  write_data_select <= 0;
          LR_int <= 0;
          AiLR_int <= 0;
          write_enable <= 1;
	  write_enable_d1 <= 1;
        end
      else
        begin
          write_data_select <= ~AisLR;
          LR_int <= ~AisLR; 
          AiLR_int <= AiLR;
	  write_enable_d1 <= write_enable; 
	  if(counter_val==1)     
            write_enable <= ~ (AisLRPos || AisLRNeg);
        end
    end



  // pointer logic for L
always @(posedge io_clk)
  begin
    if(~resetb)
      begin
        newest_L <= 7'd96;
        oldest_L <= 7'd96;
        newest_L_out <= 7'd96;
	oldest_L_out <= 7'd96;
      end
    else
      if(clear)
        begin
          newest_L <= 7'd96;
          oldest_L <= 7'd96;
          newest_L_out <= 7'd96;
	  oldest_L_out <= 7'd96;
        end
      else
        if(~write_enable && LR_int)
          begin
	    // either uninitialized or at top of memory block
	    if((newest_L[6] && newest_L[5]) || (newest_L == `K_minus1))
	      newest_L <= 0; 
            else
              newest_L <= newest_L + 7'd1; // just increment

	    // either uninitialized or at top of memory block
            if((oldest_L[6] && oldest_L[5]) || (oldest_L == `K_minus1))
	      oldest_L <= 0;
	    else
	      if(((newest_L > oldest_L) && (newest_L-oldest_L == `K_minus1)) || ((newest_L < oldest_L) && (oldest_L-newest_L == 7'd1)))
                oldest_L <= oldest_L + 7'd1;
          end
	else
	  if(~write_enable && ~LR_int)
	    begin
	      newest_L_out <= newest_L;
	      oldest_L_out <= oldest_L;
	    end
    end

  // pointer logic for R
always @(posedge io_clk)
  begin
    if(~resetb)
      begin
        newest_R <= 7'd96;
        oldest_R <= 7'd96;
      end
    else
      if(clear)
        begin
          newest_R <= 7'd96;
          oldest_R <= 7'd96;
        end
      else
        if(~write_enable && ~LR_int)
          begin
	    // either uninitialized or at top of memory block
	    if((newest_R[6] && newest_R[5]) || (newest_R == `K_minus1))
	      newest_R <= 0; 
            else
              newest_R <= newest_R + 7'd1; // just increment

	    // either uninitialized or at top of memory block
            if((oldest_R[6] && oldest_R[5]) || (oldest_R == `K_minus1))
	      oldest_R <= 0;
	    else
	      if(((newest_R > oldest_R) && (newest_R-oldest_R == `K_minus1)) || ((newest_R < oldest_R) && (oldest_R-newest_R == 7'd1)))
                oldest_R <= oldest_R + 7'd1;
          end
    end

  // output muxes for newest and oldest pointers
  // need to not update pointers to SRC until R data is in.
  // register to improve timing
  always @(posedge io_clk)
  begin
   if(LR_int)
     begin
       newest_temp <= newest_L_out;
       oldest_temp <= oldest_L_out;
     end
   else
     begin
       newest_temp <= newest_R;
       oldest_temp <= oldest_R;
     end    
  end

  assign newest = newest_temp;
  assign oldest = oldest_temp;

  // read address computation
  assign read_addr_int =(newest_L[6] && newest_L[5]) ? 7'b1111111 : 
			~AiLR_int ? {1'b0, read_addr} : {1'b0, read_addr} + `K;

  // Register all bist input signals for timing
  always @(posedge io_clk)
    begin
      if(~resetb)
        begin
          d1BistEnable <= 0;
          d1BistAddr   <= 0;
          d1BistData1  <= 0;
          d1BistRdn    <= 0;
          d1BistWen96  <= 0;
          d1BistCmp96  <= 0;
        end
      else
        begin
          d1BistEnable <= BistEnable;
          d1BistAddr   <= BistAddr;
          d1BistData1  <= BistData1;
          d1BistRdn    <= BistRdn;
          d1BistWen96  <= BistWen96;
          d1BistCmp96  <= BistCmp96;
        end
    end // always @ (io_clk)

assign read_data = ~resetb || clear ? 0 : afr ? AisLR ? AisDR : AisDL : read_data_mem;

  // module instantiation for memory
  MemS1R1W96x16M1 InputRam	(
				.RCLK(io_clk),
			   	.RA(read_addr_int), 
				.REN(read_enable),
			   	.Q(read_data_mem),
			   	.BYP(1'b0),
			   	.WCLK(io_clk),
			   	.WA(write_addr), 
			   	.WEN(write_enable_d1),
			   	.D(write_data),
			   	.ScanIn(1'b0),
			   	.ScanOut(),
			   	.ScanMode(1'b0),
			   	.ScanEnable(1'b0),
			   	.ScanNoRam(1'b0),
			   	.BistEnable(d1BistEnable),
			   	.BistAddr(d1BistAddr),
			   	.BistData1(d1BistData1),
			   	.BistRdn(d1BistRdn),
			   	.BistWen96(d1BistWen96),
			   	.BistCmp96(d1BistCmp96),
			   	.BistErr(BistErr)
			   	);

endmodule

