#!/usr/bin/perl

require fileio;
require convert;

@flagNames = qw/NonInterlaced PALMode BurstBlank Burst HorizontalSync VerticalSync Field CompositeSync/;
@flagNames = qw/CompositeSync Field VerticalSync HorizontalSync Burst BurstBlank NTSCMode Interlaced/;
@flagShortNames = qw/C F V H B K N I/;

$firstSample = 1;

$csave = -1;
@finit = (-1,-1,-1,-1,-1,-1,-1,-1);
%fsave = map { @flagShortNames[$_] => @finit[$_] } (0..7);

$iActivePix = 0;
$iTotalPix = 0;
$totalLineCount = 0;
$activeLineCount = 0;
$fieldCount = 0;

$activePixPerLine = 0;
$totalPixPerLine = 0;
$activeLinePerField = 0;
$totalLinePerField = 0;

$showBlanking = 0;

$ipix = 0;

$memFile = "-";
$ppmFile = "-";

$verbose = 0;

@RGB = ();

while(@ARGV) {
    $_ = shift @ARGV; 
    if ( /^-b$/ ) { $showBlanking = 1; }
    elsif ( /^-mem$/ ) { $memFile = shift; }
    elsif ( /^-ppm$/ ) { $ppmFile = shift; }
    elsif ( /^-v$/ ) { $verbose = 1; }
    else {
        printf ("$0: unknown commandline option \"%s\"\n\n",$_);
        usage();
        exit 1;
    }
}

@mem = readMemFile($memFile,0,"a(a2)*");

while($#mem >= 0) {

    ($p1,$y1,$c1) = splice(@mem, 0, 3);
    ($p2,$y2,$c2) = splice(@mem, 0, 3);

    die (sprintf "data error: y1=%02x y2=%02x\n",$y1,$y2)
        if ( ($y1 == 0 && $y2 != 0) || ($y1 != 0 && $y2 == 0) );
    
    printf "ipix=%d #samp=%d totLine=%d activeLine=%d totPix=%d activePix=%d\n",
    $ipix,($#RGB+1)/3,$totalLineCount,$activeLineCount,$iTotalPix,$iActivePix
        if ( 0 && $verbose );

    @rgb = pyc2rgb($p1,$y1,$c1,$p2,$y2,$c2);

#    push @RGB,@rgb if ( $showBlanking || (!$showBlanking && $y1) );

    if ( $y1 == 0x00 ) {

        if ( $firstSample ) {
            $csave = $c1;
            my @f = decodeFlags($c1);
            %fsave = map { @flagShortNames[$_] => @f[$_] } (0..7);
            $firstSample = 0;
            printFlags(\%fsave,"@ t=$ipix") if ( $verbose );

            %flatch = %fcurr;
         }

        # check whether flags have changed

        if ( $c1 != $csave ) {
            
            my @f = decodeFlags($c1);
            %fcurr = map { @flagShortNames[$_] => @f[$_] } (0..7);

            printFlagChanges(\%fcurr,\%fsave,"@ t=$ipix")
                if ( $verbose );

            # check for beginning of hsync

            if ( $fcurr{H} != $fsave{H} && $fcurr{H}==0 ) { 
                printf "LineLength = %d active, %d total\n",$iActivePix,$iTotalPix
                    if ( $verbose );
                $totalPixPerLine = $iTotalPix if ( $totalPixPerLine == 0 );
                $activePixPerLine = $iActivePix if ( $activePixPerLine == 0 );

                die "activePixPerLine: expected $activePixPerLine, got $iActivePix \@ line $totalLineCount\n"
                    if ( $iActivePix != $activePixPerLine && $iActivePix != 0 );

                die "totalPixPerLine: expected $totalPixPerLine, got $iTotalPix \@ line $totalLineCount\n"
                    if ( $iTotalPix != $totalPixPerLine );

                $totalLineCount += 1;
                $activeLineCount += 1 if ( $iActivePix );

                $iActivePix = 0;
                $iTotalPix = 0;

                # check for beginning of vsync
                #  vsync isn't necessarily aligned with hsync, so we latch the flags on
                #  each hsync and then compare the vsync values between successive hsync's

                printf "V: latch = %d, curr = %d\n", $flatch{V}, $fcurr{V}
                    if ( $verbose );

                if ( $fcurr{V} != $flatch{V} && $fcurr{V}==0 ) { 
                    printf "FieldLength = %d active, %d total\n",$activeLineCount,$totalLineCount
                        if ( $verbose );
                    $totalLinePerField = $totalLineCount if ( $totalLinePerField == 0 );
                    $activeLinePerField = $activeLineCount if ( $activeLinePerField == 0 );
                    $fieldCount += 1;
                }

                %flatch = %fcurr;
            }

            # save current flags for next iteration
            
            $csave = $c1;
            %fsave = %fcurr;
            
        }
        
    }

    if ( $showBlanking ) {
        splice @RGB, ($totalLineCount*$totalPixPerLine+$iTotalPix)*3, 6, @rgb;
    } elsif ( !$showBlanking && $y1 ) {
        splice @RGB, ($activeLineCount*$activePixPerLine+$iActivePix)*3, 6, @rgb;
    }

    $ipix+=2;

    $iTotalPix += 2;
    $iActivePix += 2 if ( $y1 );
    
}

# Note: last line doesn't get counted.  Since last line isn't active,
#       activeLineCount doesn't need to be incremented, but 
#       totalLineCount does
$totalLineCount++;

printf "$0: pix tot:%d: act:%d:  line tot:%d: act:%d: line/field tot:%d: act:%d\n",
    $totalPixPerLine , $activePixPerLine,
    $totalLineCount, $activeLineCount,
    $totalLinePerField, $activeLinePerField
    if ( 1 || $verbose );
 
if ( 1 || ! $fcurr{N} ) {
    my $stride = $showBlanking ? $totalPixPerLine : $activePixPerLine;
    my $nlines = $showBlanking ? $totalLineCount : $activeLineCount;
    my $base2 = $showBlanking ? $totalLinePerField : $activeLinePerField;
    my @tRGB = ();

    for ( my $i=0; $i<$nlines; $i++ ) {
        my $j = int($i/2);
        $j += $base2 if ( $i % 2 != 0 );
        my @q = @RGB[ $j*$stride*3 .. ($j+1)*$stride*3-1 ];
        printf "i=%d, j=%d #q=%d\n",$i,$j,$#q;
        push @tRGB, @q;
    }
    @RGB = @tRGB;
}

writePPMFile($ppmFile,
             $showBlanking ? $totalPixPerLine : $activePixPerLine,
             $showBlanking ? $totalLineCount : $activeLineCount,  
             @RGB);


sub decodeFlags {
    my $cin = @_[0];
    my ($c,$f,$v,$h,$b,$k,$n,$i);

    $c = $cin & 0x80 ? 1 : 0;
    $f = $cin & 0x40 ? 1 : 0;
    $v = $cin & 0x20 ? 1 : 0;
    $h = $cin & 0x10 ? 1 : 0;
    $b = $cin & 0x08 ? 1 : 0;
    $k = $cin & 0x04 ? 1 : 0;
    $n = $cin & 0x02 ? 1 : 0;
    $i = $cin & 0x01 ? 1 : 0;

    return ($c,$f,$v,$h,$b,$k,$n,$i);
}

sub printFlags {
    my %f = %{@_[0]};
    my $suffix = $#_ >= 1 ? @_[1] : "";

    print "flags: ";
    for ( my $i=0; $i<8; $i++ ) {
        printf "%s=%d ", $flagShortNames[$i],$f{$flagShortNames[$i]};
    }
    print "$suffix\n";
}

sub printFlagChanges {
    my %fcurr = %{@_[0]};
    my %fsave = %{@_[1]};
    my $suffix = $#_ >= 2 ? @_[2] : "";
   
    print "flagChange: ";
    for ( my $i=0; $i<8; $i++ ) {
        my $t = $flagShortNames[$i];
         if ( $fcurr{$t} != $fsave{$t} ) {
            printf "%s goes %s : ",$t,$fcurr{$t}==1?"Hi":"Lo";
        }
    }
    print "$suffix\n";
}

sub usage {
    print <<END;
usage: $0 [options] < <memfile> > <ppmfile>

   -b         include blanking region in the output file [default=no]
   -mem       input .mem file [default=stdin]
   -ppm       output .ppm file [default=stdout]
   -v         verbose

END
}

