// clkgen.v Frank Berndt
// simulation clock generator;
// :set tabstop=4

`timescale 1ps/1ps

module clkgen ( period, clk, pci_clk );
	input [31:0] period;
	output clk;
	output pci_clk;

	// programmable clock generator;

	reg running;
	reg clk;
	reg pci_clk;

	initial
	begin
		running = 'bx;
		clk = 'bx;
		pci_clk = 'bx;
		#(period * 13);
		running = 1;
		clk = 0;
		pci_clk = 0;
	end

	always
		#(period / 2) clk <= running & ~clk;

	always @(posedge clk)
	begin
		pci_clk = 1;
		repeat(3) @(clk);
		pci_clk = 0;
		repeat(2) @(clk);
	end

	// report changes in clock period;

	always @(period)
		 $display("%t: %M: period %0dps", $time, period);

endmodule 	//clkgen
