#!/usr/bin/perl

require fileio;
require convert;

while(@ARGV) {
    $_ = shift @ARGV; 
    if ( 0 ) { }
    else {
        printf ("ppmtoycrycb: unknown commandline option \"%s\"\n\n",$_);
        usage();
        exit 1;
    }
}

($w,$h,@rgb) = readPPMFile("-");
#for ($i=0;$i<20;$i++) {
#    print STDERR "rgb :${i}:  :$rgb[$i]:\n";
#}

@ycrcb = rgb2ycrcb(@rgb);
#for ($i=0;$i<20;$i++) {
#    print STDERR "ycrcb :${i}:  :$ycrcb[$i]:\n";
#}

for ($i=0; $i<$#ycrcb; $i+=6) {
    my $cr0 = $ycrcb[$i+1];
    my $cb0 = $ycrcb[$i+2];
    my $cr1 = $ycrcb[$i+4];
    my $cb1 = $ycrcb[$i+5];
    my $cr = ($cr0+$cr1)/2;
    my $cb = ($cb0+$cb1)/2;

#    @ycrcb1 = $ycrcb[($i .. $i+2)];
#    @ycrcb2 = $ycrcb[$i+3 .. $i+5];
#    print join(":",@ycrcb1),"\n";

    # round away from zero
    $cb = $cb >= 128 ? int( $cb + 0.5 ) : 128 - int( 128 - $cb + 0.5 );
    $cr = $cr >= 128 ? int( $cr + 0.5 ) : 128 - int( 128 - $cr + 0.5 );
    push @ycrycb, ( $ycrcb[$i], $cr, $ycrcb[$i+3], $cb );
}

#for ($i=0;$i<20;$i++) {
#    print STDERR "ycrycb :${i}:  :$ycrycb[$i]:\n";
#}


writeYCrYCbFile("-",$w,$h,@ycrycb);

#printf "done\n";
exit;

sub usage {
    print <<END;
usage: ppmtoycrcb.pl < <ppmfile> > <memfile>

   -stride x      stride for packing image into memory, in bytes [default=768]
   -topBase x     base address for top field of image in memory, in bytes [default=0]
   -bottomBase x  base address for bottom field of image in memory, in bytes [default=0x4000]
                  [only used for interlaced video formats]
   -i             interlaced video [default]
   -n             non-interlaced video

END
}
