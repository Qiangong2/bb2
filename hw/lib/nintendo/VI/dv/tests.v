/*****************************************************************************
 * 
 *   (C) 2004 BroadOn INC..  ALL RIGHTS RESERVED.  UNPUBLISHED -- RIGHTS
 *   RESERVED UNDER THE COPYRIGHT LAWS OF THE UNITED STATES.  USE OF A
 *   COPYRIGHT NOTICE IS PRECAUTIONARY ONLY AND DOES NOT IMPLY PUBLICATION
 *   OR DISCLOSURE.
 *
 *   THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF
 *   BroadOn INC..  USE, DISCLOSURE, OR REPRODUCTION IS PROHIBITED WITHOUT
 *   THE PRIOR EXPRESS WRITTEN PERMISSION OF BroadOn INC..
 *
 *                   RESTRICTED RIGHTS LEGEND
 *
 *   Use, duplication, or disclosure by the Government is subject to
 *   restrictions as set forth in subparagraph (c)(1)(ii) of the Rights
 *   in Technical Data and Computer Software clause at DFARS 252.227-7013
 *   or subparagraphs (c)(1) and (2) of Commercial Computer Software --
 *   Restricted Rights at 48 CFR 52.227-19, as applicable. 
 *
 *   BroadOn Inc.
 *   3400 Hillview Ave, Bldg 5
 *   Palo Alto, CA 94304
 *
 ****************************************************************************/
`timescale 1ns/1ns
`include "vi_reg.v"

module tests ( clk, pci_clk, go );

	input clk;
	input pci_clk;
	input go;

// test module wrapper;
//	define mode settings for each display format

//	note:  INT stride STD = 2*WPL;  fix WPL at max, so
//	use single source image config. *may be a problem, precludes correct source data fetching?*
//	assume !INT stride = INT stride/2 (PROG, 3D)
//	let source image width=64 words (1024 pixels)
//	therefore stride (!INT)=64, stride (INT)=128.
   
`define POLARITY 16'd0		//override later if desired to invert any flags [7:0]

`ifdef NTSC_EURGB60_INT         //NTSC or EURGB60 interlaced

 `define EQU     4'd6
 `define ACV     12'd240
 `define PRBodd  16'd24
 `define PRBeven 16'd25
 `define PSBodd  16'd3
 `define PSBeven 16'd2
 `define BS1     5'd12
 `define BS2     5'd13
 `define BS3     5'd12
 `define BS4     5'd13
 `define BE1     11'd520
 `define BE2     11'd519
 `define BE3     11'd520
 `define BE4     11'd519
 `define NHLINES 10'd525 	//define total # lines, unused
 `define HLW     16'd429
 `define HSY     7'd64
 `define HCS     8'd71
 `define HCE     8'd105
 `define HBE     10'd162
 `define HBS     10'd373
 `define HBED    15'd122 	//debug h border end
 `define HBEN    1'd0            //debug h border enable
 `define HBSD    16'd412 	//debug h border start
 `define WPL     8'd40           //#words read per line          XXX
 `define STD     8'd48		//# words stride                XXX
 `define NIN     1'd0    	//noninterlace enable
 `define FMT     2'd0    	//format NTSC
 `define VCLKSEL 1'd0		//use 27MHz video clock

`endif //  `ifdef NTSC_EURGB60_INT


`ifdef NTSC_EURGB60_DS		//NTSC or EURGB60 double strike

 `define EQU     4'd6
 `define ACV     12'd240
 `define PRBodd  16'd24
 `define PRBeven 16'd24
 `define PSBodd  16'd4
 `define PSBeven 16'd4
 `define BS1     5'd12
 `define BS2     5'd12
 `define BS3     5'd12
 `define BS4     5'd12
 `define BE1     11'd520
 `define BE2     11'd520
 `define BE3     11'd520
 `define BE4     11'd520
 `define NHLINES 10'd526 	//define total # lines, unused
 `define HLW     16'd429
 `define HSY     7'd64
 `define HCS     8'd71
 `define HCE     8'd105
 `define HBE     10'd162
 `define HBS     10'd373
 `define HBED    15'd122 	//debug h border end
 `define HBEN    1'd0            //debug h border enable
 `define HBSD    16'd412 	//debug h border start
 `define WPL     8'd40           //#words read per line          XXX
 `define STD     8'd48           //# words stride                XXX
 `define NIN     1'd1    	//noninterlace enable
 `define FMT     2'd0    	//format NTSC
 `define VCLKSEL 1'd0		//use 27MHz video clock

`endif //  `ifdef NTSC_EURGB60_DS


`ifdef	PAL_INT			//PAL interlaced

 `define EQU     4'd5
 `define ACV     12'd287
 `define PRBodd  16'd35
 `define PRBeven 16'd36
 `define PSBodd  16'd1
 `define PSBeven 16'd0
 `define BS1     5'd13
 `define BS2     5'd12
 `define BS3     5'd11
 `define BS4     5'd10
 `define BE1     11'd619
 `define BE2     11'd618
 `define BE3     11'd617
 `define BE4     11'd620
 `define NHLINES 10'd625 	//define total # lines, unused
 `define HLW     16'd432
 `define HSY     7'd64
 `define HCS     8'd71
 `define HCE     8'd105
 `define HBE     10'd162
 `define HBS     10'd373
 `define HBED    15'd133 	//debug h border end
 `define HBEN    1'd0            //debug h border enable
 `define HBSD    16'd420 	//debug h border start
 `define WPL     8'd40           //#words read per line          XXX
 `define STD     8'd48		//# words stride                XXX
 `define NIN     1'd0    	//noninterlace enable
 `define FMT     2'd1		//format PAL (2'd1) -- originally was 2'd2 PS
 `define VCLKSEL 1'd0		//use 27MHz video clock

`endif //  `ifdef PAL_INT


`ifdef PAL1_INT			//PAL var interlaced

 `define EQU     4'd5
 `define ACV     12'd287
 `define PRBodd  16'd35
 `define PRBeven 16'd36
 `define PSBodd  16'd1
 `define PSBeven 16'd0
 `define BS1     5'd13
 `define BS2     5'd12
 `define BS3     5'd11
 `define BS4     5'd10
 `define BE1     11'd619
 `define BE2     11'd618
 `define BE3     11'd617
 `define BE4     11'd620
 `define NHLINES 10'd625 	//define total # lines, unused
 `define HLW     16'd432
 `define HSY     7'd64
 `define HCS     8'd75
 `define HCE     8'd106
 `define HBE     10'd172
 `define HBS     10'd380
 `define HBED    15'd133 	//debug h border end
 `define HBEN    1'd0            //debug h border enable
 `define HBSD    16'd420 	//debug h border start
 `define WPL     8'd40           //#words read per line          XXX
 `define STD     8'd48           //# words stride                XXX
 `define NIN     1'd0    	//noninterlace enable
 `define FMT     2'd1		//format PAL (2'd1) -- originally was 2'd2 PS
 `define VCLKSEL 1'd0		//use 27MHz video clock

`endif //  `ifdef PAL1_INT


`ifdef PAL_DS         		//PAL double strike

 `define EQU     4'd5
 `define ACV     12'd287
 `define PRBodd  16'd33
 `define PRBeven 16'd33
 `define PSBodd  16'd2
 `define PSBeven 16'd2
 `define BS1     5'd13
 `define BS2     5'd11
 `define BS3     5'd13
 `define BS4     5'd11
 `define BE1     11'd619
 `define BE2     11'd621
 `define BE3     11'd619 
 `define BE4     11'd621
 `define NHLINES 10'd624 	//define total # lines, unused
 `define HLW     16'd432
 `define HSY     7'd64
 `define HCS     8'd71
 `define HCE     8'd105
 `define HBE     10'd162
 `define HBS     10'd373
 `define HBED    15'd133 	//debug h border end
 `define HBEN    1'd0            //debug h border enable
 `define HBSD    16'd420 	//debug h border start
 `define WPL     8'd40           //#words read per line          XXX
 `define STD     8'd48		//# words stride                XXX
 `define NIN     1'd1    	//noninterlace enable
 `define FMT     2'd1    	//format PAL
 `define VCLKSEL 1'd0		//use 27MHz video clock

`endif //  `ifdef PAL_DS


`ifdef PAL1_DS         		//PAL var double strike

 `define EQU     4'd5
 `define ACV     12'd287
 `define PRBodd  16'd33
 `define PRBeven 16'd33
 `define PSBodd  16'd2
 `define PSBeven 16'd2
 `define BS1     5'd13
 `define BS2     5'd11
 `define BS3     5'd13
 `define BS4     5'd11
 `define BE1     11'd619
 `define BE2     11'd621
 `define BE3     11'd619
 `define BE4     11'd621
 `define NHLINES 10'd624 	//define total # lines, unused
 `define HLW     16'd432
 `define HSY     7'd64
 `define HCS     8'd75
 `define HCE     8'd106
 `define HBE     10'd172
 `define HBS     10'd380
 `define HBED    15'd133 	//debug h border end
 `define HBEN    1'd0            //debug h border enable
 `define HBSD    16'd420 	//debug h border start
 `define WPL     8'd40           //#words read per line          XXX
 `define STD     8'd48           //# words stride                XXX
 `define NIN     1'd1    	//noninterlace enable
 `define FMT	  2'd1		//format PAL (2'd1) -- originally was 2'd2 PS
 `define VCLKSEL 1'd0		//use 27MHz video clock

`endif //  `ifdef PAL1_DS


`ifdef MPAL_INT         	//MPAL interlaced

 `define EQU     4'd6
 `define ACV     12'd240
 `define PRBodd  16'd24
 `define PRBeven 16'd25
 `define PSBodd  16'd3
 `define PSBeven 16'd2
 `define BS1     5'd16
 `define BS2     5'd15
 `define BS3     5'd14
 `define BS4     5'd13
 `define BE1     11'd518
 `define BE2     11'd517
 `define BE3     11'd516
 `define BE4     11'd519
 `define NHLINES 10'd525 	//define total # lines, unused
 `define HLW     16'd429
 `define HSY     7'd64
 `define HCS     8'd78
 `define HCE     8'd112
 `define HBE     10'd162
 `define HBS     10'd373
 `define HBED    15'd122 	//debug h border end
 `define HBEN    1'd0            //debug h border enable
 `define HBSD    16'd412 	//debug h border start
 `define WPL     8'd40           //#words read per line          XXX
 `define STD     8'd48           //# words stride                XXX
 `define NIN     1'd0    	//noninterlace enable
 `define FMT     2'd2    	//format MPAL
 `define VCLKSEL 1'd0		//use 27MHz video clock

`endif //  `ifdef MPAL_INT


`ifdef MPAL_DS         		//MPAL double strike

 `define EQU     4'd6
 `define ACV     12'd240
 `define PRBodd  16'd24
 `define PRBeven 16'd24
 `define PSBodd  16'd4
 `define PSBeven 16'd4
 `define BS1     5'd16
 `define BS2     5'd14
 `define BS3     5'd16
 `define BS4     5'd14
 `define BE1     11'd518
 `define BE2     11'd520
 `define BE3     11'd518
 `define BE4     11'd520
 `define NHLINES 10'd526 	//define total # lines, unused
 `define HLW     16'd429
 `define HSY     7'd64
 `define HCS     8'd78
 `define HCE     8'd112
 `define HBE     10'd162
 `define HBS     10'd373
 `define HBED    15'd122 	//debug h border end
 `define HBEN    1'd0            //debug h border enable
 `define HBSD    16'd412 	//debug h border start
 `define WPL     8'd40           //#words read per line          XXX
 `define STD     8'd48           //# words stride                XXX
 `define NIN     1'd1    	//noninterlace enable
 `define FMT     2'd2    	//format MPAL
 `define VCLKSEL 1'd0		//use 27MHz video clock

`endif //  `ifdef MPAL_DS


`ifdef NTSC_PROG         	//NTSC progressive

 `define EQU     4'd12
 `define ACV     12'd480
 `define PRBodd  16'd48
 `define PRBeven 16'd48
 `define PSBodd  16'd6
 `define PSBeven 16'd6
 `define BS1     5'd24
 `define BS2     5'd24
 `define BS3     5'd24
 `define BS4     5'd24
 `define BE1     11'd1038
 `define BE2     11'd1038
 `define BE3     11'd1038
 `define BE4     11'd1038
 `define NHLINES 11'd1050 	//define total # lines, unused
 `define HLW     16'd429
 `define HSY     7'd64
 `define HCS     8'd71
 `define HCE     8'd105
 `define HBE     10'd162
 `define HBS     10'd373
 `define HBED    15'd122 	//debug h border end
 `define HBEN    1'd0            //debug h border enable
 `define HBSD    16'd412 	//debug h border start
 `define WPL     8'd40           //#words read per line          XXX
 `define STD     8'd48           //# words stride                XXX
 `define NIN     1'd1    	//noninterlace enable
 `define FMT     2'd0    	//format NTSC
 `define VCLKSEL 1'd1   		//use 54MHz video clock

`endif //  `ifdef NTSC_PROG


`ifdef NTSC_3D			//NTSC stereo

 `define EQU     4'd12
 `define ACV     12'd480
 `define PRBodd  16'd44
 `define PRBeven 16'd44
 `define PSBodd  16'd10
 `define PSBeven 16'd10
 `define BS1     5'd24
 `define BS2     5'd24
 `define BS3     5'd24
 `define BS4     5'd24
 `define BE1     11'd1038
 `define BE2     11'd1038
 `define BE3     11'd1038
 `define BE4     11'd1038
 `define NHLINES 11'd1050 	//define total # lines, unused
 `define HLW     16'd429
 `define HSY     7'd64
 `define HCS     8'd71
 `define HCE     8'd105
 `define HBE     10'd168
 `define HBS     10'd379
 `define HBED    15'd122 	//debug h border end
 `define HBEN    1'd0            //debug h border enable
 `define HBSD    16'd412 	//debug h border start
 `define WPL     8'd127          //#words read per line          XXX
 `define STD     8'd128          //# words stride                XXX
 `define NIN     1'd1    	//noninterlace enable
 `define FMT     2'd0    	//format NTSC
 `define VCLKSEL 1'd1           	//use 54MHz video clock

`endif //  `ifdef NTSC_3D


`ifdef GCA_INT         		//GCA interlaced

 `define EQU     4'd6
 `define ACV     12'd241
 `define PRBodd  16'd24
 `define PRBeven 16'd25
 `define PSBodd  16'd1
 `define PSBeven 16'd0
 `define BS1     5'd12
 `define BS2     5'd13
 `define BS3     5'd12
 `define BS4     5'd13   
 `define BE1     11'd520 
 `define BE2     11'd519
 `define BE3     11'd520
 `define BE4     11'd519
 `define NHLINES 10'd525 	//define total # lines, unused
 `define HLW     16'd429
 `define HSY     7'd64
 `define HCS     8'd71
 `define HCE     8'd105
 `define HBE     10'd159
 `define HBS     10'd370
 `define HBED    15'd122 	//debug h border end
 `define HBEN    1'd0            //debug h border enable
 `define HBSD    16'd412 	//debug h border start
 `define WPL     8'd64           //#words read per line          XXX
 `define STD     8'd128          //# words stride                XXX
 `define NIN     1'd0    	//noninterlace enable
 `define FMT     2'd0    	//format NTSC
 `define VCLKSEL 1'd0		//use 27MHz video clock

`endif //  `ifdef GCA_INT


`ifdef GCA_PROG         	//GCA progressive

 `define EQU     4'd12            
 `define ACV     12'd480
 `define PRBodd  16'd48
 `define PRBeven 16'd48
 `define PSBodd  16'd6
 `define PSBeven 16'd6
 `define BS1     5'd24
 `define BS2     5'd24
 `define BS3     5'd24   
 `define BS4     5'd24   
 `define BE1     11'd1038 
 `define BE2     11'd1038
 `define BE3     11'd1038
 `define BE4     11'd1038         
 `define NHLINES 10'd1050 	//define total # lines, unused
 `define HLW     16'd429
 `define HSY     7'd64
 `define HCS     8'd71
 `define HCE     8'd105
 `define HBE     10'd180
 `define HBS     10'd391
 `define HBED    15'd122 	//debug h border end
 `define HBEN    1'd0            //debug h border enable
 `define HBSD    16'd412 	//debug h border start                          
 `define WPL     8'd127          //#words read per line          XXX
 `define STD     8'd128          //# words stride                XXX
 `define NIN     1'd1    	//noninterlace enable
 `define FMT     2'd0    	//format NTSC   
 `define VCLKSEL 1'd1           	//use 54MHz video clock

`endif //  `ifdef GCA_PROG

wire [10:0]	BE1, BE2, BE3, BE4;	//define new vars to support V_SHORT use below

`ifdef	V_SHORT			//option to display only 8 lines per field/frame
 `define ACV_NEW 12'd24
   
	assign BE1 = `BE1 - 2*`ACV + 2*`ACV_NEW;
   assign BE2 = `BE2 - 2*`ACV + 2*`ACV_NEW;
   assign BE3 = `BE3 - 2*`ACV + 2*`ACV_NEW;
   assign BE4 = `BE4 - 2*`ACV + 2*`ACV_NEW;

 `undef ACV
 `define	ACV     `ACV_NEW
`else
	assign  BE1     =       `BE1;
	assign  BE2     =       `BE2;
	assign  BE3     =       `BE3;
	assign  BE4     =       `BE4;
`endif

`ifdef	VI_DEBUG		//option invokes border (for fixed length 720 encoders)
	`define FMT     2'd3
	`define HBE	    `HBED
	`define HBS	    `HBSD
	`define HBEN    1'b1
`endif

   wire [9:0] HBEw	=	`HBE;
   wire [7:0] stride	=	`STD;
   reg [31:0] instruction;
   reg        early, done;

`define piWrite16 sim.piWrite16
`define piRead16 sim.piRead16
`define piNOP sim.piNOP
`define pi_viClk sim.pi_viClk
`define vi_piData sim.vi_piData

   wire     resetb = ~go;
   
   initial begin
		early		<=	1'b0;
		done		<=	1'b0;
      `piNOP;  // initialize the PI bus to idle
   end

	always @(posedge resetb) begin					//setup VI after reset ends
		@(posedge `pi_viClk);

      `piWrite16( `VI_PIC_BASE_LFT_U, 16'h0000 );  //top field L *always used*
      `piWrite16( `VI_PIC_BASE_LFT_L, 16'h0000 );
      `piWrite16( `VI_PIC_BASE_RGT_U, 16'h0000 );  //top field R
      `piWrite16( `VI_PIC_BASE_RGT_L, 16'h4000 );	// XXX set at 16KB (interlaced)
      `piWrite16( `VI_PIC_BASE_LFT_BOT_U, 16'h0010 );  //bottom field L @0x10_0000
      `piWrite16( `VI_PIC_BASE_LFT_BOT_L, 16'h0000 );
      `piWrite16( `VI_PIC_BASE_RGT_BOT_U, 16'h0000 );  //bottom field R FBBhi
      `piWrite16( `VI_PIC_BASE_RGT_BOT_L, 16'h0000 );  //FBBlo
      `piWrite16( `VI_DSP_POS_U, 16'h0000 );  //HCT *doesn't exist!*
      `piWrite16( `VI_DSP_POS_L, 16'h0000 );  //VCT *doesn't exist!*
      `piWrite16( `VI_DSP_INT0_U, 16'h0002 );  //begin display int
      `piWrite16( `VI_DSP_INT0_L, 16'h0001 );
      `piWrite16( `VI_DSP_INT1_U, 16'h1002 );
      `piWrite16( `VI_DSP_INT1_L, 16'h0002 );
      `piWrite16( `VI_DSP_INT2_U, 16'h0000 );
      `piWrite16( `VI_DSP_INT2_L, 16'h0000 );
      `piWrite16( `VI_DSP_INT3_U, 16'h0000 );
      `piWrite16( `VI_DSP_INT3_L, 16'h0000 );  //end display int
      `piWrite16( `VI_DSP_LATCH0_U, 16'h0000 );  //begin display latch
      `piWrite16( `VI_DSP_LATCH0_L, 16'h0000 );
      `piWrite16( `VI_DSP_LATCH1_U, 16'h0000 );
      `piWrite16( `VI_DSP_LATCH1_L, 16'h0000 );  //end display latch
      `piWrite16( `VI_HSCALE, 16'h0100 );  //horizontal scale HS_EN, STP

      `piWrite16( `VI_FLTR_COEFF0_U, 16'h0000 );  //begin filter coeff
      `piWrite16( `VI_FLTR_COEFF0_L, 16'h0000 );
      `piWrite16( `VI_FLTR_COEFF1_U, 16'h0000 );
      `piWrite16( `VI_FLTR_COEFF1_L, 16'h0000 );
      `piWrite16( `VI_FLTR_COEFF2_U, 16'h0000 );
      `piWrite16( `VI_FLTR_COEFF2_L, 16'h0000 );
      `piWrite16( `VI_FLTR_COEFF3_U, 16'h0000 );
      `piWrite16( `VI_FLTR_COEFF3_L, 16'h0000 );
      `piWrite16( `VI_FLTR_COEFF4_U, 16'h0000 );
      `piWrite16( `VI_FLTR_COEFF4_L, 16'h0000 );
      `piWrite16( `VI_FLTR_COEFF5_U, 16'h0000 );
      `piWrite16( `VI_FLTR_COEFF5_L, 16'h0000 );
      `piWrite16( `VI_FLTR_COEFF6_U, 16'h0000 );
      `piWrite16( `VI_FLTR_COEFF6_L, 16'h0000 );  //end filter coeff

      `piWrite16( 8'h34, 16'h0000 );  //unused
      `piWrite16( `VI_OUTPOL, `POLARITY ); //polarity out
      `piWrite16( `VI_CLKSEL, {15'h0000, `VCLKSEL} );  //clk select VCLKSEL
      `piWrite16( `VI_DTVSTATUS, 16'h0000 );  //DTV status VISEL
      `piWrite16( `VI_WIDTH, 16'h0000 );  //scaling width SRCWIDTH
      
		`piWrite16( `VI_VER_TIM, {`ACV, `EQU} );	//ACV, EQU
      `piWrite16( `VI_HOR_TIM0_U, {`HCS, `HCE} );	//HCE, HCS
      `piWrite16( `VI_HOR_TIM0_L, `HLW );		//HLW
      `piWrite16( `VI_HOR_TIM1_U, {5'h0, `HBS, HBEw[9]} );	//HBS, HBEhi
      `piWrite16( `VI_HOR_TIM1_L, {HBEw[8:0], `HSY} );	//HBElo, HSY
      `piWrite16( `VI_VER_ODD_TIM_U, `PSBodd );		//PSB odd
      `piWrite16( `VI_VER_ODD_TIM_L, `PRBodd );		//PRB odd
      `piWrite16( `VI_VER_EVN_TIM_U, `PSBeven );		//PSB even
      `piWrite16( `VI_VER_EVN_TIM_L, `PRBeven );		//PRB even
      `piWrite16( `VI_ODD_BBLNK_INTVL_U,  {BE3, `BS3} );	//BE3, BS3
      `piWrite16( `VI_ODD_BBLNK_INTVL_L,  {BE1, `BS1} );	//BE1, BS1
      `piWrite16( `VI_EVN_BBLNK_INTVL_U,  {BE4, `BS4} );	//BE4, BS4
      `piWrite16( `VI_EVN_BBLNK_INTVL_L,  {BE2, `BS2} );	//BE2, BS2
    	`piWrite16( `VI_HBE656, {`HBEN, `HBED} ); 	//HBE debug en/border
      `piWrite16( `VI_HBS656, `HBSD );      	//HBS debug border
      `piWrite16( `VI_PIC_CFG, {`WPL, `STD} );  	//picture config WPL, STD XXX??
    	`piWrite16( `VI_DSP_CFG, {6'b0, `FMT, 5'b0, `NIN, 2'b1} );	//start

      while ( 1 ) begin

		   `piRead16( 8'h34 );  	//return PI bus data to idle
         while ( early !== 1'b1 ) begin
            `piRead16( `VI_DSP_INT1_U );   //poll for vertical count=2 (early display)
            early <= `vi_piData[15];
         end
         `piRead16( 8'h34 );  	//return PI bus data to idle
         `piWrite16( `VI_DSP_INT0_U, 16'h1002 );  	//reset interrupt status

      end // while ( 1 )

   end // always @ (posedge resetb)
   
endmodule 	//tests
