#!/usr/bin/perl

while(@ARGV) {
    $_ = shift @ARGV; 
    if ( /^-stride$/ ) { $stride = shift @ARGV; }
    elsif ( /^-base$/ ) { $base = shift @ARGV; }
    elsif ( /^-bgRGB$/ ) { @bg = splice(@ARGV,0,3); }
    elsif ( /^-bgYCrCb$/ ) { @bg = splice(@ARGV,0,3); }
    else {
        printf ("ppmtomem: unknown commandline option \"%s\"\n\n",$_);
        usage();
        exit 1;
    }
}

($w,$h,@rgb) = readPPM("-");

#for ($iw=0; $iw
#printf "done\n";
exit;


sub usage {
    print <<END;
usage: ppmtomem.pl [-stride x] [-base x] [-bgRGB r g b | -bgYCrCb y cr cb] < <ppmfile> > <memfile>

   -stride x      stride for packing image into memory, in bytes [default=768]
   -topBase x     base address for top field of image in memory, in bytes [default=0]
   -bottomBase x  base address for bottom field of image in memory, in bytes [default=0x4000]
                  [only used for interlaced video formats]
   -i             interlaced video [default]
   -n             non-interlaced video

END
}

sub readPPM {
    my $fileName = @_[0];
    my $line;
    my ($magic,$w,$h,$maxColor);
    my @data;

    open PPM, "<$fileName";

    $_ = <PPM>;
    chomp;
    s/\#.*$//;
    s/\s+//g;
    $magic = $_;
    die "ReadPPM: $fileName is not a PPM file (expected magic=\"P6\", got \"${magic}\"\n" if ( $magic ne "P6" );
    
    $_ = <PPM>;
    chomp;
    s/\#.*$//;
    s/\s+//g;
    $w = $_;
    print "ReadPPM: $fileName width is $w\n";

    $_ = <PPM>;
    chomp;
    s/\#.*$//;
    s/\s+//g;
    $h = $_;
    print "ReadPPM: $fileName height is $h\n";

    $_ = <PPM>;
    chomp;
    s/\#.*$//;
    s/\s+//g;
    $maxColor = $_;
    die "ReadPPM: $fileName has an unsupported max color of $maxColor (should be 255)\n"
        if ( $maxColor != 255 );
    print "ReadPPM: $fileName maximum color is $maxColor\n";

    $_ = read PPM, $line, 3*$h*$w;
    if ( $_ != 3*$h*$w ) {
        die sprintf("ReadPPM: expected %d bytes of data, only got %d\n",3*$h*$w,$_);
    }
    @data = unpack "C*", $line;

    close PPM;

    return ($w,$h,@data);
}
