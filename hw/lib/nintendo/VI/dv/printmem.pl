#!/usr/bin/perl

require fileio;

print "$#ARGV \n";
if ( $#ARGV < 0 ) { push @ARGV,"-"; }

foreach $_ (@ARGV) {
    print "ARGV: $_ \n";
    if ( $0 =~ /printppm/ ) { readPPMFile($_,1); }
    if ( $0 =~ /printycrycb/ ) { readYCrYCbFile($_,1); }
    if ( $0 =~ /printmem/ ) { readMemFile($_,1); }
}

