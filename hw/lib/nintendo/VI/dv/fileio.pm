#package fileio;

#
# routines to read/write ycrycb, ppm, and mem files
#

sub readYCrYCbFile {
    my $fileName = @_[0];
    my $verbose = $#_ > 0 ? @_[1] : 0;
    my $line;
    my ($magic,$w,$h,$maxColor);
    my @data;
    my @tokens;

    open PPM, "<$fileName";

    $_ = <PPM>;
    chomp;
    s/\#.*$//;
    s/\s+//g;
    $magic = $_;
    die "readYCrYCbFile: $fileName is not a YCrYCb file (expected magic=\"P6YCrYCb\", got \"${magic}\"\n" 
        if ( $magic ne "P6YCrYCb" );
    
    if ( $#tokens < 0 ) {
        $_ = <PPM>;
        chomp;
        s/\#.*$//;
        s/^\s+//g;
        @tokens = split /\s+/;
    }
    $w = shift @tokens;
    print STDERR "readYCrYCbFile: $fileName width is $w\n" if ( $verbose );

    if ( $#tokens < 0 ) {
        $_ = <PPM>;
        chomp;
        s/\#.*$//;
        s/^\s+//g;
        @tokens = split /\s+/;
    }
    $h = shift @tokens;
    print STDERR "readYCrYCbFile: $fileName height is $h\n" if ( $verbose );

    if ( $#tokens < 0 ) {
        $_ = <PPM>;
        chomp;
        s/\#.*$//;
        s/^\s+//g;
        @tokens = split /\s+/;
    }
    $maxColor = shift @tokens;
    die "readYCrYCbFile: $fileName has an unsupported max color of $maxColor (should be 255)\n"
        if ( $maxColor != 255 );
    print STDERR "readYCrYCbFile: $fileName maximum color is $maxColor\n" if ( $verbose );

    binmode PPM;
    $_ = read PPM, $line, 2*$h*$w;
    if ( $_ != 2*$h*$w ) {
        die sprintf("readYCrYCbFile: expected %d bytes of data, only got %d\n",2*$h*$w,$_);
    }
    @data = unpack "C*", $line;

    if ( $verbose ) {
        for ( my $ih=0; $ih<$h; $ih++ ) {
            for ( my $iw=0; $iw<$w; $iw++ ) {
                my $t = ($ih*$w + $iw)*2;
                printf STDERR "%d,%d  :y:%s: :0x%02x:0x%02x: :%d:%d:\n",
                $ih,$iw,$iw%2==0?"cr":"cb",$data[$t],$data[$t+1],$data[$t],$data[$t+1];
            }
        }
    }

    close PPM;

    return ($w,$h,@data);
}

sub writeYCrYCbFile
{
    my ($fileName,$width,$height,@ycrycb) = @_;

#    printf STDERR "#ycrycb = %d\n",$#ycrycb;

#    printf STDERR "writeYCrYCbFile: opening $filename\n";
    open(PPM,">$fileName");
    
    printf PPM "P6YCrYCb\n"; # Hacked PPM magic number for custom YCrYCb format
    printf PPM "%d\n",$width; # width
    printf PPM "%d\n",$height; # height
    printf PPM "255\n"; # max color value

    binmode PPM;
    print PPM pack("C*",@ycrycb);
    printf PPM "\n";

#    printf STDERR "writeYCrYCbFile: closing $filename\n";
    close PPM;
}

sub readPPMFile {
    my $fileName = @_[0];
    my $verbose = $#_ > 0 ? @_[1] : 0;
#    print STDERR "readPPMFile: $#_\n";

    my $line;
    my ($magic,$w,$h,$maxColor);
    my @data;
    my @tokens;

    open PPM, "<$fileName";

    $_ = <PPM>;
    chomp;
    s/\#.*$//;
    s/^\s+//g;
    @tokens = split /\s+/;
    $magic = shift @tokens;
    die "readPPMFile: $fileName is not a PPM file (expected magic=\"P6\", got \"${magic}\"\n" if ( $magic ne "P6" );
    
    if ( $#tokens < 0 ) {
        $_ = <PPM>;
        chomp;
        s/\#.*$//;
        s/^\s+//g;
        @tokens = split /\s+/;
    }
    $w = shift @tokens;
    print STDERR "readPPMFile: $fileName width is $w\n" if ( $verbose );

    if ( $#tokens < 0 ) {
        $_ = <PPM>;
        chomp;
        s/\#.*$//;
        s/^\s+//g;
        @tokens = split /\s+/;
    }
    $h = shift @tokens;
    print STDERR "readPPMFile: $fileName height is $h\n" if ( $verbose );

    if ( $#tokens < 0 ) {
        $_ = <PPM>;
        chomp;
        s/\#.*$//;
        s/^\s+//g;
        @tokens = split /\s+/;
    }
    $maxColor = shift @tokens;
    die "readPPMFile: $fileName has an unsupported max color of $maxColor (should be 255)\n"
        if ( $maxColor != 255 );
    print STDERR "readPPMFile: $fileName maximum color is $maxColor\n" if ( $verbose );

    binmode PPM;
    $_ = read PPM, $line, 3*$h*$w;
    if ( $_ != 3*$h*$w ) {
        die sprintf("readPPMFile: expected %d bytes of data, only got %d\n",3*$h*$w,$_);
    }
    @data = unpack "C*", $line;

    if ( $verbose ) {
        for ( my $ih=0; $ih<$h; $ih++ ) {
            for ( my $iw=0; $iw<$w; $iw++ ) {
                my $t = ($ih*$w + $iw)*3;
                printf STDERR "%d,%d  :0x%02x:0x%02x:0x%02x: :%d:%d:%d:\n",
                $ih,$iw,$data[$t],$data[$t+1],$data[$t+2],$data[$t],$data[$t+1],$data[$t+2];
            }
        }
    }

    close PPM;

    return ($w,$h,@data);
}

sub writePPMFile
{
    my ($fileName,$width,$height,@rgb) = @_;
    my $verbose = 0;

#    printf STDERR "#rgb = %d\n",$#rgb;

    my $exp = $width * $height * 3;
    my $got = $#rgb + 1;

    die "writePPMFile: $fileName, data mismatch, expect $exp, got $got\n" if ( $exp != $got );

    if ( $verbose ) {
        for ( my $ih=0; $ih<$height; $ih++ ) {
            for ( my $iw=0; $iw<$width; $iw++ ) {
                my $t = ($ih*$width + $iw)*3;
                printf STDERR "%d,%d  :0x%02x:0x%02x:0x%02x: :%d:%d:%d:\n",
                $ih,$iw,$rgb[$t],$rgb[$t+1],$rgb[$t+2],$rgb[$t],$rgb[$t+1],$rgb[$t+2];
            }
        }
    }

#    printf STDERR "writePPMFile: opening $filename\n";
    open(PPM,">$fileName");
    
    printf PPM "P6\n"; # PPM magic number
    printf PPM "%d\n",$width; # width
    printf PPM "%d\n",$height; # height
    printf PPM "255\n"; # max color value

    binmode PPM;
    my $t = pack("C*",@rgb);
    my $tt = length($t);
    die "writePPMFile: error writing data, only wrote $tt (should have been $got)\n" if ( $tt != $got );
    print PPM $t;
    printf PPM "\n";

#    printf STDERR "writePPMFile: closing $filename\n";
    close PPM;
}

sub writeMemFile
{
    my ($fileName,@mem) = @_;

#    printf STDERR "#mem = %d\n",$#mem;

#    printf STDERR "writeMemFile: opening $filename\n";
    open(MEM,">$fileName");
    
    for (my $i=0; $i<$#mem; $i+=8) {
        for (my $j=0; $j<8; $j++) {
            printf MEM "%02x",$mem[$i+$j];
        }
        printf MEM "\n";
    }
#    printf STDERR "writeMemFile: closing $filename\n";
    close MEM;
}

sub readMemFile
{
    my $fileName = @_[0];
    my $verbose = $#_ > 0 ? @_[1] : 0;
    my $fmt = $#_ > 1 ? @_[2] : "(a2)*";
    my $i = 0;
 
#    print STDERR "readMemFile: $#_\n";

#    printf STDERR "readMemFile: opening $filename\n";
    open(MEM,"<$fileName");
    
    while(<MEM>) {
        chomp;
#        print STDERR "line = :",$_,":\n";
#        my @t = unpack("(a2)*",$_);
#        printf STDERR "#t = $#t \n";
#        for ( my $i=0; $i<=$#t; $i++ ) { print STDERR " t[$i] = ",$t[$i],"\n"; }

#        push @mem, @t("(a2)*",$_);
        my @buf = unpack($fmt,$_);

        if ( $#buf >= 0 ) {

            printf STDERR "%d(0x%x) ", $#mem+1, $#mem+1 if ($verbose);
            for (my $i=0; $i<=$#buf; $i++) {
                die "readMemFile: illegal value \"$buf[$i]\" at line $. of file \"$fileName\"\n" 
                    if ( $buf[$i] !~ m/^[0-9a-fA-F]+$/ );
                my $t = hex($buf[$i]);
                printf STDERR ":%02x",$t if ($verbose);
                push @mem, $t;
            }
            printf STDERR ":\n" if ($verbose);
            
        }
#        printf STDERR ("%d: %s\n",$i++,$_) if ($verbose);
    }
    
#    for (my $i=0; $i<=$#mem; $i++) { 
#        $mem[$i] = hex($mem[$i]); 
##        printf STDERR "%d: %d\n", $i, $mem[$i]; 
#    }

#    if ( $verbose ) {
#        for (my $i=0; $i<=$#mem; $i+=16) {
#            for ( my $j=0; $j<16; $j++ ) {
#                printf STDERR ":%02x",$mem[$i*16+$j];
#            }
#            printf STDERR ":\n";
#        }
#    }

#    printf STDERR "#mem = %d\n",$#mem;
#    printf STDERR "readMemFile: closing $filename\n";
    close MEM;

    return @mem;
}

1;
