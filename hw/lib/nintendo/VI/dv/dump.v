// dump.v Frank Berndt
// dump control module;
// :set tabstop=4

module dump;

	// turn on dumping globally or on a per-module basis;
	// set the dump flag instead of $dumpon or $dumpoff;

	reg dump;

	initial
	begin
		dump = 0;
      $display("dump: checking plusargs @ t=%t",$time);
      
		if($test$plusargs("dump_all")) begin
         $display("dump: dump_all recognized @ t=%t",$time);
         $dumpvars;
      end

		// dump clocks and reset for any dump on top level;

		if($test$plusargs("dump_toplevel")) begin
         $display("dump: dump_toplevel recognized @ t=%t",$time);
			dump = 1;
			$dumpvars(1, sim);
         $dumpvars(1, sim.tests);
			$dumpvars(1, sim.clk);
			$dumpvars(1, sim.resetb);
		end

		// dump clocks and reset for any dump on top level;

		if($test$plusargs("dump")) begin
         $display("dump: dump recognized @ t=%t",$time);
			dump = 1;
			$dumpvars(1, sim.clk);
			$dumpvars(1, sim.resetb);
		end
	end

	// simulator dumping flag;

	always @(dump)
	begin
		if(dump)
			$dumpon;
		else
			$dumpoff;
		$display("%t: %M: dumping %0s", $time, dump? "on" : "off");
	end

endmodule 	//dump
