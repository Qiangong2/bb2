#!/usr/bin/perl

require fileio;
require convert;

while(@ARGV) {
    $_ = shift @ARGV; 
    if ( 0 ) { }
    else {
        printf ("ycrycbtoppm: unknown commandline option \"%s\"\n\n",$_);
        usage();
        exit 1;
    }
}

($w,$h,@ycrycb) = readYCrYCbFile("-");
#for ($i=0;$i<20;$i++) {
#    print STDERR "ycrycb :${i}:  :$ycrycb[$i]:\n";
#}

for ( $ih=0; $ih<$h; $ih++ ) {
    for ($iw=0; $iw<$w; $iw+=2) {
        $y1 = shift @ycrycb;
        $cr = shift @ycrycb;
        $y2 = shift @ycrycb;
        $cb = shift @ycrycb;
#        printf STDERR "%d,%d\n",$ih,$iw;
        push @rgb, ycrcb2rgb($y1,$cr,$cb);
#        printf STDERR "%d,%d\n",$ih,$iw+1;
        push @rgb, ycrcb2rgb($y2,$cr,$cb);
    }
}
#for ($i=0;$i<20;$i++) {
#    print STDERR "ycrcb :${i}:  :$ycrcb[$i]:\n";
#}

#printf STDERR "info: pix=%d  ycrycb=%d  ycrcb=%s\n",$h*$w,$#ycrycb/2,$#ycrcb/3;
#@rgb = ycrcb2rgb(@ycrcb);

#printf STDERR "ycrycbtoppm: returned from ycrcb2rgb\n";

#for ($i=0;$i<20;$i++) {
#    print STDERR "rgb :${i}:  :$rgb[$i]:\n";
#}

writePPMFile("-",$w,$h,@rgb);

#printf "done\n";
exit;

sub usage {
    print <<END;
usage: ppmtoycrcb.pl < <ppmfile> > <memfile>

   -stride x      stride for packing image into memory, in bytes [default=768]
   -topBase x     base address for top field of image in memory, in bytes [default=0]
   -bottomBase x  base address for bottom field of image in memory, in bytes [default=0x4000]
                  [only used for interlaced video formats]
   -i             interlaced video [default]
   -n             non-interlaced video

END
}
