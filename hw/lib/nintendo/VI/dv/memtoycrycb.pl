#!/usr/bin/perl

require fileio;

$stride = 48 * 32;
$bottomBase = 0x100000;
$topBase = 0;
$interlaced = 0;
$memSize = 0;
$bytesPerPixel = 2;
@bg = (16, 128, 16, 128);
$h = 480;
$w = 640;

while(@ARGV) {
    $_ = shift @ARGV; 
    if ( /^-stride$/ ) { $stride = shift @ARGV;  
                         $stride = hex2dec($1) if ( $stride =~ m/^\s*0[xX]([0-9A-Fa-f]*)\s*$/ );
                         die "memtoycrycb: illegal stride = $stride, must be a multiple of 32\n" 
                             if ( $stride % 32 != 0 ); }
    elsif ( /^-top$/ ) { $topBase = shift @ARGV; 
                         $topBase = hex2dec($1) if ( $topBase =~ m/^\s*0[xX]([0-9A-Fa-f]*)\s*$/ ); }
    elsif ( /^-bottom$/ ) { $bottomBase = shift @ARGV; 
                            $bottomBase = hex2dec($1) if ( $bottomBase =~ m/^\s*0[xX]([0-9A-Fa-f]*)\s*$/ ); }
#    elsif ( /^-memsize$/ ) { $memSize = shift @ARGV; 
#                            $memSize = hex2dec($1) if ( $memSize =~ m/^\s*0[xX]([0-9A-Fa-f]*)\s*$/ ); }
    elsif ( /^-bgRGB$/ ) { my @rgb = splice(@ARGV,0,3); 
                           my @ycrcb = rgb2ycrcb(@rgb); 
                           @bg = ($ycrcb[0], $ycrcb[1], $ycrcb[0], $ycrcb[2]); }
    elsif ( /^-bgYCrCb$/ ) { my @ycrcb = rgb2ycrcb(@rgb); 
                             @bg = ($ycrcb[0], $ycrcb[1], $ycrcb[0], $ycrcb[2]); }
    elsif ( /^-i$/ ) { $interlaced = 1; }
    elsif ( /^-n$/ ) { $interlaced = 0; }
    elsif ( /^-w$/ ) { $w = shift @ARGV; }
    elsif ( /^-h$/ ) { $h = shift @ARGV; }

    else {
        printf ("memtoycrycb: unknown commandline option \"%s\"\n\n",$_);
        usage();
        exit 1;
    }
}

@mem = readMemFile("-");

$memSize = $#mem + 1;

#print ":w:${w}:\n";
#print ":h:${h}:\n";
#for ($i=0; $i<20; $i++) {
#   printf STDERR ":i:mem[i]: :${i}:$mem[$i]:\n";
#}
if ( $interlaced ) {
    my $topSize = $stride * ( int($h/2) + ($h%2) );
    my $bottomSize = $stride * int($h/2);
    
    if ( ($topBase <= $bottomBase && $topBase + $topSize >= $bottomBase) ||
         ($topBase <= $bottomBase && $bottomBase + $bottomSize >= $memSize ) ||
         ($topBase >= $bottomBase && $topSize + $topSize >= $memSize) ||
         ($topBase >= $bottomBase && $bottomBase + $bottomSize >= $topSize ) ) {
        my $t = sprintf("memory overlap: topBase=0x%x, bottomBase=0x%x, topSize=0x%x, bottomSize=0x%x, memSize=0x%x",
                        $topBase,$bottomBase,$topSize,$bottomSize,$memSize);
        die "memtoycrycb: $t\n";
    }
} else {
    my $topSize = $stride * $h;
    
    if ( $topBase + $topSize >= $memSize ) {
        my $t = sprintf("memory overlap: topBase=0x%x, topSize=0x%x, memSize=0x%x",
                        $topBase,$topSize,$memSize);
        die "memtoycrycb: $t\n";
    }
}

if ( $w * $bytesPerPixel > $stride ) {
    my $t = sprintf("stride too small (%d), needs to be at least %d",$stride,$w*$bytesPerPixel);
    die "memtoycrycb: $t\n";
}

for ( $ih=0; $ih<$h; $ih++ ) {
    for ($iw=0; $iw<$w; $iw++) {
        push @ycrycb, @bg;
    }
}

for ( $ih=0; $ih<$h; $ih++ ) {

    if ( $interlaced ) {
        $rowBase = ($ih%2==0 ? $topBase : $bottomBase) + int($ih/2)*$stride;
    } else {
        $rowBase = $topBase + $ih*$stride;
    }

    my $nb = $w*$bytesPerPixel < $stride ? $w*$bytesPerPixel : $stride;
    for ($iw=0; $iw<$nb; $iw++) {
        $ycrycb[$ih*$w*$bytesPerPixel+$iw] = $mem[$rowBase+$iw];
#        print STDERR "ycrycb[$ih*$w*$bytesPerPixel+$iw] = mem[$rowBase+$iw] = ",$mem[$rowBase+$iw],"\n";
    }      

}

writeYCrYCbFile("-",$w,$h,@ycrycb);

#printf STDERR "done\n";
exit;


sub usage {
    print <<END;
usage: memtoycrycb.pl [options] < <ycrycbfile> > <memfile>

   -stride x         stride for packing image into memory, in bytes [default=$stride]
                     Note: must be a multiple of 32
   -topBase x        base address for top field of image in memory, in bytes [default=0]
   -bottomBase x     base address for bottom field of image in memory, in bytes [default=0x100000]
                     [only used for interlaced video formats]
   -bgRGB r g b      background color, specified as RGB [default: 0, 0, 0]
   -bgYCrCb y cr cb  background color, specified as YCrCb [default: 16, 128, 128]
   -i                interlaced video [default]
   -n                non-interlaced video
   -h x              image height, in pixels [default=$h]
   -w x              image width, in pixels [default=$w]

END
}

