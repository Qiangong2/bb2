#!/usr/bin/perl

require fileio;

$stride = 48 * 32;
$bottomBase = 0x100000;
$topBase = 0;
$interlaced = 0;
$memSize = 0x200000;
$bytesPerPixel = 2;
@bg = (16, 128, 16, 128);
$w = -1;
$h = -1;

while(@ARGV) {
    $_ = shift @ARGV; 
    if ( /^-stride$/ ) { $stride = shift @ARGV;  
                         $stride = hex2dec($1) if ( $stride =~ m/^\s*0[xX]([0-9A-Fa-f]*)\s*$/ ); 
                         die "ycrycbtomem: illegal stride = $stride, must be a multiple of 32\n" 
                             if ( $stride % 32 != 0 ); }
    elsif ( /^-top$/ ) { $topBase = shift @ARGV; 
                         $topBase = hex2dec($1) if ( $topBase =~ m/^\s*0[xX]([0-9A-Fa-f]*)\s*$/ ); }
    elsif ( /^-bottom$/ ) { $bottomBase = shift @ARGV; 
                            $bottomBase = hex2dec($1) if ( $bottomBase =~ m/^\s*0[xX]([0-9A-Fa-f]*)\s*$/ ); }
    elsif ( /^-memsize$/ ) { $memSize = shift @ARGV; 
                            $memSize = hex2dec($1) if ( $memSize =~ m/^\s*0[xX]([0-9A-Fa-f]*)\s*$/ ); }
    elsif ( /^-bgRGB$/ ) { my @rgb = splice(@ARGV,0,3); 
                           my @ycrcb = rgb2ycrcb(@rgb); 
                           @bg = ($ycrcb[0], $ycrcb[1], $ycrcb[0], $ycrcb[2]); }
    elsif ( /^-bgYCrCb$/ ) { my @ycrcb = rgb2ycrcb(@rgb); 
                             @bg = ($ycrcb[0], $ycrcb[1], $ycrcb[0], $ycrcb[2]); }
    elsif ( /^-i$/ ) { $interlaced = 1; }
    elsif ( /^-n$/ ) { $interlaced = 0; }
    elsif ( /^-w$/ ) { $w = shift @ARGV; }
    elsif ( /^-h$/ ) { $h = shift @ARGV; }
    else {
        printf ("ycrycbtomem: unknown commandline option \"%s\"\n\n",$_);
        usage();
        exit 1;
    }
}

($tw,$th,@ycrycb) = readYCrYCbFile("-");

$w = $tw if ( $w < 0 );
$h = $th if ( $h < 0 );

if ( $interlaced ) {
    my $topSize = $stride * ( int($h/2) + ($h%2) );
    my $bottomSize = $stride * int($h/2);
    
    if ( ($topBase <= $bottomBase && $topBase + $topSize >= $bottomBase) ||
         ($topBase <= $bottomBase && $bottomBase + $bottomSize >= $memSize ) ||
         ($topBase >= $bottomBase && $topSize + $topSize >= $memSize) ||
         ($topBase >= $bottomBase && $bottomBase + $bottomSize >= $topSize ) ) {
        my $t = sprintf("memory overlap: topBase=0x%x, bottomBase=0x%x, topSize=0x%x, bottomSize=0x%x, memSize=0x%x",
                        $topBase,$bottomBase,$topSize,$bottomSize,$memSize);
        die "ycrycbtomem: $t\n";
    }
} else {
    my $topSize = $stride * $h;
    
    if ( $topBase + $topSize >= $memSize ) {
        my $t = sprintf("memory overlap: topBase=0x%x, topSize=0x%x, memSize=0x%x",
                        $topBase,$topSize,$memSize);
        die "ycrycbtomem: $t\n";
    }
}

if ( $w * $bytesPerPixel > $stride ) {
    my $t = sprintf("stride too small (%d), needs to be at least %d",$stride,$w*$bytesPerPixel);
    die "memtoycrycb: $t\n";
}

#print ":w:${w}:\n";
#print ":h:${h}:\n";

#for ($i=0; $i<20; $i++) {
#    print STDERR ":i:ycrycb[i]: :${i}:$ycrycb[$i]:\n";
#}

for ($i=0; $i<$memSize; $i+=4) {
    for ($j=0; $j<4; $j++) {
        $mem[$i+$j] = $bg[$j];
    }
}
#writeMemFile("-",@mem);

for ( $ih=0; $ih<$h; $ih++ ) {

    if ( $interlaced ) {
        $rowBase = ($ih%2==0 ? $topBase : $bottomBase) + int($ih/2)*$stride;
    } else {
        $rowBase = $topBase + $ih*$stride;
    }

#    print STDERR "ih=$ih, rowBase = $rowBase\n";
    my $nb = $w*$bytesPerPixel < $stride ? $w*$bytesPerPixel : $stride;
    for ($i=0; $i<$nb; $i++) {
        $mem[$rowBase+$i] = $ycrycb[$ih*$tw*$bytesPerPixel+$i];
    }      

}

writeMemFile("-",@mem);

#printf STDERR "done\n";
exit;


sub usage {
    print <<END;
usage: ycrycbtomem.pl [options] < <ycrycbfile> > <memfile>

   -memsize x        size of memory image to generate, in bytes [default=0x200000]
   -stride x         stride for packing image into memory, in bytes [default=$stride]
                     Note: must be a multiple of 32
   -topBase x        base address for top field of image in memory, in bytes [default=0]
   -bottomBase x     base address for bottom field of image in memory, in bytes [default=0x100000]
                     [only used for interlaced video formats]
   -bgRGB r g b      background color, specified as RGB [default: 0, 0, 0]
   -bgYCrCb y cr cb  background color, specified as YCrCb [default: 16, 128, 128]
   -i                interlaced video [default]
   -n                non-interlaced video
   -h x              image height in <memfile>, in pixels [default=same as <ycrycbfile>]
   -w x              image width in <memfile>, in pixels [default=same as <ycrycbfile>]

END
}

