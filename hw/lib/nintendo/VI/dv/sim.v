/*****************************************************************************
 * 
 *   (C) 2004 BroadOn INC..  ALL RIGHTS RESERVED.  UNPUBLISHED -- RIGHTS
 *   RESERVED UNDER THE COPYRIGHT LAWS OF THE UNITED STATES.  USE OF A
 *   COPYRIGHT NOTICE IS PRECAUTIONARY ONLY AND DOES NOT IMPLY PUBLICATION
 *   OR DISCLOSURE.
 *
 *   THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF
 *   BroadOn INC..  USE, DISCLOSURE, OR REPRODUCTION IS PROHIBITED WITHOUT
 *   THE PRIOR EXPRESS WRITTEN PERMISSION OF BroadOn INC..
 *
 *                   RESTRICTED RIGHTS LEGEND
 *
 *   Use, duplication, or disclosure by the Government is subject to
 *   restrictions as set forth in subparagraph (c)(1)(ii) of the Rights
 *   in Technical Data and Computer Software clause at DFARS 252.227-7013
 *   or subparagraphs (c)(1) and (2) of Commercial Computer Software --
 *   Restricted Rights at 48 CFR 52.227-19, as applicable. 
 *
 *   BroadOn Inc.
 *   3400 Hillview Ave, Bldg 5
 *   Palo Alto, CA 94304
 *
 ****************************************************************************/

// sim.v
// top level vi stand-alone simulator;
// :set tabstop=4

`timescale 1ps/1ps

`define MAX_CHAR 30
`define HPIX_MAX   864
`define VLINE_MAX  625
`define UINT_CHAR_SIZE 10  // a 32-bit unsigned int has at most 10 decimal digits

module sim;
	// simulator environment;

	initial
	begin
		$timeformat(-9, 2, "ns", 13);
	end

	// system globals;

	wire clk;				// system clock;
   
	reg resetb;

   // VI signals
   wire        pi_viClk;	// Processor interface clock
   reg         pi_viReq;       // Processor register transaction start
   reg         pi_viRd;        // When asserted, processor transaction is a read
   wire        vi_piAck;       // Processor register transaction complete
   reg [7:0]   pi_viAddr;	// Register address
   reg [15:0]  pi_viData;      // Write data, bound for register
   wire [15:0] vi_piData;      // Read data, from register
   wire        vi_piVrtIntr;   // Vertical Interrupt
   
   wire        vi_memReq;      // Assert to enqueue a memory request
   wire [25:5] vi_memAddr;     // Cache line aligned addr, valid when memReq asserted
   wire        mem_viAck;      // Asserted by memory controller to end transaction
   wire [63:0] mem_viData;     // Data from memory controller
   
   wire        vi_ioVsync;     // Vertical sync
   wire        vi_ioHsync;     // Horizontal sync
   
   wire [1:0]  ioGun_trg;      // Light gun inputs from controller
   
   reg         clk27;
   wire        clk54;   	// Input to video crystal oscillator/mux
   wire        viclk;      // either clk27 or clk54, depending on viclksel
   wire        vicr_topad;     // Distinguishes YCr phase from YCb phase
   wire [7:0]  vid_topad;      // Video output data
   wire [1:0]  visel_tocore;   // Status of DTV pin
   wire        viclksel;       // VI Clk select (54MHz or 27MHz)
   reg         clksel;
   
   // testbench variables;
   
	reg [31:0]  frame_count;
	reg [31:0]  field_count;
	reg [31:0]  line_count;         // lines since last VSync
	reg [31:0]  pixel_count;        // pixels since last HSync
	reg [31:0]  total_pixel_count;  // pixels since last VSync
   reg [31:0]  num_frames_to_generate;
   
   reg [7:0]   y;
   reg [7:0]   cbcr;
   reg         phase;
   reg         vicr_d1;
   reg         vicr_d2;
   reg [7:0]   vid_d1;
   reg [7:0]   vid_d2;
   reg [7:0]   vid_d3;
   reg [7:0]   vid_d4;
   reg         pixDone;
   reg         compositeSyncb,field,vSyncb,hSyncb,burstFlagb,burstBlankb,modeFlag,nonInterlaced;
   reg         flagsValid;
   reg         vSyncbSave;
   
   reg [`MAX_CHAR*8-1:0] filename;
   reg [`MAX_CHAR*8-1:0] basename;
   
	// instantiate dump control module;
   
	dump dump ();
   
	// clock generator;
   
	clkgen sysclkgen ( .period(10000), .clk(clk) );
   
	// power-on reset;
	// start tests;
   
	reg                   go;					// when to start the tests;
   
	initial
	  begin
		  go = 1'b0;
		  resetb = #1000 1'b0;
		  repeat(20) @(posedge clk);
		  resetb = 1'b1;
		  @(posedge clk);
		  go = 1;
	  end
   
	// monitor reset signal

	always @(resetb)
		$display("%t: %M: resetb=%b", $time, resetb);

	// instantiate tests;
	tests tests (
		.clk(clk),
		.go(~resetb)
	);

	initial begin
      field_count = 0;
      frame_count = 0;
      line_count = 0;
      total_pixel_count = 0;
      if ( ! $value$plusargs("framename=%s", basename) )
        basename = "frame"; //default
      if ( ! $value$plusargs("numframes=%0d", num_frames_to_generate) )
        num_frames_to_generate = 32'd5; //default
      $display("info: simulation will stop after %0d frames have been generated", num_frames_to_generate);
	end

   always @(posedge viclk) begin

      vid_d1 <= vid_topad;
      vid_d2 <= vid_d1;
      vid_d3 <= vid_d2;
      vid_d4 <= vid_d3;
      
      vicr_d1 <= vicr_topad;
      vicr_d2 <= vicr_d1;
      
      pixDone = ( vicr_topad != vicr_d1 ) ? 1'b1 : 1'b0;

      if (pixDone) begin
         y = vid_d2;
         cbcr = vid_d1;
         phase = vicr_d1;
         if ( ^{phase,y,cbcr} === 1'bx )
           $display("ERROR: invalid VID pixel (y=8'b%b,cbcr=8'b%b) @ t=%t",y,cbcr,$time);
         putPixel(total_pixel_count,phase,y,cbcr);
         total_pixel_count = total_pixel_count + 1;
         pixel_count = pixel_count + 1;
      end

   end
   
   always @(posedge pixDone) begin
      
      if ( y == 8'h00 ) begin
         {compositeSyncb,field,vSyncb,hSyncb,burstFlagb,burstBlankb,modeFlag,nonInterlaced} = cbcr;
         flagsValid = 1'b1;
         
      end else begin
         flagsValid = 1'b0;
      end

   end // always @ (posedge pixDone)

   ////////////////////////////////////////////////////////////////////////
   //
   // notify user of any status changes in the flags
   //
   always @(compositeSyncb)
     $display("flagChange: compositeSyncb=1'b%b %0d,%0d @ t=%t",compositeSyncb,line_count,pixel_count,$time);

   always @(field)
     $display("flagChange: field=1'b%b %0d,%0d @ t=%t",field,line_count,pixel_count,$time);

   always @(vSyncb)
     $display("flagChange: vSyncb=1'b%b %0d,%0d @ t=%t",vSyncb,line_count,pixel_count,$time);

   always @(hSyncb)
     $display("flagChange: hSyncb=1'b%b %0d,%0d @ t=%t",hSyncb,line_count,pixel_count,$time);

   always @(burstFlagb)
     $display("flagChange: burstFlagb=1'b%b %0d,%0d @ t=%t",burstFlagb,line_count,pixel_count,$time);

   always @(burstBlankb)
     $display("flagChange: burstBlankb=1'b%b %0d,%0d @ t=%t",burstBlankb,line_count,pixel_count,$time);

   always @(modeFlag)
     $display("flagChange: modeFlag=1'b%b %0d,%0d @ t=%t",modeFlag,line_count,pixel_count,$time);

   always @(nonInterlaced)
     $display("flagChange: nonInterlaced=1'b%b %0d,%0d @ t=%t",nonInterlaced,line_count,pixel_count,$time);

   always @(flagsValid)
      if (flagsValid)
         $display("activeRegion: ends @ t=%t",$time);
      else
         $display("activeRegion: begins @ t=%t",$time);

   ////////////////////////////////////////////////////////////////////////
   //
   // count frames, fields, lines, pixels, etc.
   //
   // dump pixels to a file at the end of each field
   //

   always @(negedge hSyncb) begin

      if ( vSyncb == 1'b0 && vSyncbSave == 1'b1 ) begin
         if ( total_pixel_count > 1 ) begin
            //      $display("vSyncb: %h @ t=%t",vSyncb,$time);
            $display("saveField: frame=#%0d, field=#%0d pixel=%0d @ t=%t",
                     frame_count,field_count,total_pixel_count,$time);
            
            filename = genFileName(basename,frame_count,field_count);
            // don't store the final (phase,y,crcb) sample with the current frame;
            // it's the sample in which VSyncB goes low.  Save everything but this
            // sample, then clear the storage buffer and put the sample in it.
            writeFrameToFile(filename,total_pixel_count-1);
            
            field_count = field_count + 1;
            if ( nonInterlaced || ( !nonInterlaced && field == 1'b0 ) ) begin
               frame_count = frame_count + 1;
            end

            total_pixel_count = 0;
            line_count = 0;

            if ( frame_count >= num_frames_to_generate ) begin
               $display("%0d frames generated, %0d requested -- now exiting simulation @ t=%t",
                        frame_count,num_frames_to_generate,$time);
               $finish;
            end
            
            putPixel(total_pixel_count,phase,y,cbcr);
            total_pixel_count = total_pixel_count + 1;
            
         end // if ( total_pixel_count > 1 )
         
      end else begin // if ( vSyncb == 1'b0 && vSyncbSave == 1'b1 )

         line_count = line_count + 1;

      end

      vSyncbSave = vSyncb;
      
      pixel_count = 1;
      
   end

   // a buffer for temporarily storing the pixels output from VI
   
   reg [16:0] pixBuf [`HPIX_MAX*`VLINE_MAX+2:0]; 

   ////////////////////////////////////////////////////////////////////////
   //
   // putPixel
   //
   // store a (phase, y, cbcr) pixel into a temporary buffer
   //
   task putPixel;

      input [31:0] addr;
      input phase;
      input [7:0] y;
      input [7:0] cbcr;

      begin
         pixBuf[addr] = { phase, y, cbcr };
      end
   endtask // putPixel

   ////////////////////////////////////////////////////////////////////////
   //
   // getPixel
   //
   // retrieve a (y, cbcr) pixel from that temporary buffer
   //
   function [15:0] getPixel;

      input [31:0] addr;

      getPixel = pixBuf[addr];
      
   endfunction // getPixel

   ////////////////////////////////////////////////////////////////////////
   //
   // writeFrameToFile
   //
   // store captured pixel data (which has been temporary stored in a buffer)
   // to a file
   //
   task writeFrameToFile;

      input [`MAX_CHAR*8-1:0] fname;
      input [31:0] size;
      integer i;
      
      begin
         $display("writeFrameToFile: writing %0d pixels to file %s at t=%t",size,fname,$time);
         
         if ( size > `HPIX_MAX*`VLINE_MAX ) begin
            $display("writeFrameToFile: error size=%0h (>=%0h, too big)",size,`HPIX_MAX*`VLINE_MAX);
            $finish;
         end
         
         $writememh( fname, pixBuf, 0, size-1 );  // store pixel buffer to a file

         // clear memory contents
         for (i=0; i<`HPIX_MAX*`VLINE_MAX; i=i+1 )
           pixBuf[i] = 16'hxxx;
      end

   endtask

   ////////////////////////////////////////////////////////////////////////
   //
   // genFileName
   //
   // generate a string containing the filename for the next video field
   //
   function [`MAX_CHAR*8-1:0] genFileName;

      input [`MAX_CHAR*8-1:0] root;
      input [31:0] frame;
      input [31:0] field;

      reg [`UINT_CHAR_SIZE*8-1:0] fr;
      reg [`UINT_CHAR_SIZE*8-1:0] fi;


      begin
         fr = uint2asc(frame);
         fi = uint2asc(field);
         
//         $display("genFileName: root=%s (%h), frame=%d, field=%d",root,root,frame,field);
//         $display("genFileName: fr=%s (%h)",fr,fr);
//         $display("genFileName: fi=%s (%h)",fi,fi);
         
         genFileName={ root, "_", fr[15:0], "_", fi[15:0] };
         $display("genFileName: %s (%h) @ t=%t",genFileName,genFileName,$time);
         
      end
   endfunction // genFileName

   ////////////////////////////////////////////////////////////////////////
   //
   // a few test cases for the uint2asc() routine
   //
   `undef __UINT2ASC_TEST__
   `ifdef __UINT2ASC_TEST__
   initial begin
      t=uint2asc(27);
      $display("XXXXXXXXXXXXXXXXXXXXXXXX:%s:%h: should be 27",t,t);
      t=uint2asc(-27);
      $display("XXXXXXXXXXXXXXXXXXXXXXXX:%s:%h: should be %ud",t,t,-27);
      t=uint2asc(32'h7fff_ffff);
      $display("XXXXXXXXXXXXXXXXXXXXXXXX:%s:%h: should be %d",t,t,32'h7fff_ffff);
      t=uint2asc(32'h7fff_ffff+1);
      $display("XXXXXXXXXXXXXXXXXXXXXXXX:%s:%h: should be %d",t,t,32'h7fff_ffff+1);
      $finish;
   end
   `endif   

   ////////////////////////////////////////////////////////////////////////
   //
   // uint2asc
   //
   // convert a 32-bit unsigned integer to an ASCII string
   // the number right justified and left-filled ASCII zero characters
   //
   function [`UINT_CHAR_SIZE*8-1:0] uint2asc;  // big enough for a 32-bit value
      input [31:0] int;
      
      reg [31:0] intSave;
      integer dig, idig;
      integer zeroFill;
      
      begin
         zeroFill = 1;
         
         uint2asc = { {(`UINT_CHAR_SIZE-1){" "}}, "0" };
         
         $display("uint2asc: begin with :%s:%h:",uint2asc,uint2asc);

         intSave = int;
                
         for (idig=0; idig<`UINT_CHAR_SIZE; idig=idig+1) begin
            dig = int % 10;
            if ( zeroFill || int != 0 )
              uint2asc[7:0] = dig+"0";
            int = int/10;
            uint2asc = { uint2asc[7:0], uint2asc[`UINT_CHAR_SIZE*8-1:1*8]};
         end
         
         $display("uint2asc: end with :%s:%h:",uint2asc,uint2asc);

      end
   endfunction
   
   /////////////////////////////////////////////////////////////////////////
   //
   // instatiation of Flipper's VI unit and its local memory
   //
   //
   
   
	clkgen viclk54gen ( .period(18500), .clk(clk54) );    // XXX we choose async vs. flipper
   clkgen piclkgen   ( .period(6172), .clk(pi_viClk) ); // 162MHz
   
	initial 
	  clk27	<=	1'b0;

	always @(posedge clk54)
	  clk27	<=	~clk27;
   
	always @(negedge clk54)
	  clksel	<=	viclksel;

	assign viclk	=	(!resetb | !clksel) ? clk27 : clk54;

   assign ioGun_trg       =       2'b0;
   assign visel_tocore    =       2'bxx;            // XXX

   vi		vi(
            
		      .viResetb(resetb), 
		      .pi_viClk(pi_viClk), 
		      .pi_viReq(pi_viReq),
		      .pi_viRd(pi_viRd), 
		      .pi_viAddr(pi_viAddr), 
		      .pi_viData(pi_viData),
            
		      .mem_viAck(mem_viAck), 
		      .mem_viData(mem_viData),
            
		      .ioGun_trg(ioGun_trg),
            
		      .clk27(viclk),
		      .viclksel(viclksel),
		      .visel_tocore(visel_tocore),
            
    		   .vi_piAck(vi_piAck), 
		      .vi_piData(vi_piData), 
		      .vi_piVrtIntr(vi_piVrtIntr),
            
		      .vi_memReq(vi_memReq), 
		      .vi_memAddr(vi_memAddr),
            
		      .vi_ioVsync(vi_ioVsync), 
		      .vi_ioHsync(vi_ioHsync),
            
		      .vicr_topad(vicr_topad), 
		      .vid_topad(vid_topad)
            );

   // make memory large enough for two full 640x625 images
   //
   // frame = 640 pix/line * 2 Bytes/pix * 625 lines = 800,000 = 0xc3_500 Bytes
   //
   
   vi_mem #('h200_000>>3,64)	vi_mem	(  // 0x200_000 Bytes @ 0x40_000 by 8 Bytes
                      .io_clk(pi_viClk),		// host i/f clock
                      .resetb(resetb),
                      
		                .DiMemAddr(vi_memAddr),
		                .DiMemReq(vi_memReq),
		                .DiMemRd(1'b1),
		                .DiMemData(mem_viData),
		                .DiMemFlush(1'b0),
                      
                      .DiMemAck(mem_viAck),
                      .MemData(mem_viData),
		                .MemFlushAck()
                      );

`ifdef VI_VERBOSE
	reg [63:0]	rdata_d1, rdata_d2, rdata_d3;
	reg		req_d1, req_d2, req_d3, req_d4;
	reg [20:0]	read_addr;

	always@(posedge pi_viClk) begin
		if (resetb) begin
			rdata_d1	<=	mem_viData;
			rdata_d2 <= rdata_d1;
         rdata_d3 <= rdata_d2;
			req_d1	<=	vi_memReq;
			req_d2   <= req_d1;
         req_d3   <= req_d2;
         req_d4   <= req_d3;
		end
		if (vi_memReq) begin
			read_addr	<=	vi_memAddr;
		end
		if (req_d4) begin
			$display("%t: read fb0 %M: address=%0d read_data=%h", $time, ((read_addr << 2) + 0), rdata_d3);
			$display("%t: read fb1 %M: address=%0d read_data=%h", $time, ((read_addr << 2) + 1), rdata_d2);
			$display("%t: read fb2 %M: address=%0d read_data=%h", $time, ((read_addr << 2) + 2), rdata_d1);
			$display("%t: read fb3 %M: address=%0d read_data=%h", $time, ((read_addr << 2) + 3), mem_viData);
		end
	end
`endif

   //////////////////////////////////////////////////////////////////////////

   task piWrite16;
      
      input [7:0] addr;
      input [15:0] data;
      
      begin
         {pi_viReq,pi_viRd,pi_viAddr,pi_viData} <= {1'b1, 1'b0, addr, data};
         @(posedge pi_viClk);
      end
      
   endtask // piWrite16
   
   task piRead16;
      
      input [7:0] addr;
      //      reg [15:0] data;
      
      begin
         if ( pi_viReq != 1'b1 || pi_viRd != 1'b1 || pi_viAddr != addr ) begin
            {pi_viReq,pi_viRd,pi_viAddr,pi_viData} <= {1'b1, 1'b1, addr, 16'bx};
         end
         @(posedge pi_viClk);
         //         piRead16 = vi_piData;
      end
      
   endtask // piWrite16
   
   task piNOP;
      
      begin
         {pi_viReq,pi_viRd,pi_viAddr,pi_viData} <= {1'b0, 1'b1, 8'hxx, 16'bx};
         @(posedge pi_viClk);
      end
      
   endtask // piNOP
   
endmodule 	//sim
