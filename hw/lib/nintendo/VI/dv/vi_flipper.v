/*****************************************************************************
 * 
 *   (C) 2004 BroadOn INC..  ALL RIGHTS RESERVED.  UNPUBLISHED -- RIGHTS
 *   RESERVED UNDER THE COPYRIGHT LAWS OF THE UNITED STATES.  USE OF A
 *   COPYRIGHT NOTICE IS PRECAUTIONARY ONLY AND DOES NOT IMPLY PUBLICATION
 *   OR DISCLOSURE.
 *
 *   THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF
 *   BroadOn INC..  USE, DISCLOSURE, OR REPRODUCTION IS PROHIBITED WITHOUT
 *   THE PRIOR EXPRESS WRITTEN PERMISSION OF BroadOn INC..
 *
 *                   RESTRICTED RIGHTS LEGEND
 *
 *   Use, duplication, or disclosure by the Government is subject to
 *   restrictions as set forth in subparagraph (c)(1)(ii) of the Rights
 *   in Technical Data and Computer Software clause at DFARS 252.227-7013
 *   or subparagraphs (c)(1) and (2) of Commercial Computer Software --
 *   Restricted Rights at 48 CFR 52.227-19, as applicable. 
 *
 *   BroadOn Inc.
 *   3400 Hillview Ave, Bldg 5
 *   Palo Alto, CA 94304
 *
 ****************************************************************************/

`timescale 1ns/1ns

// syn myclk = vi_clk

module vi_flipper (/*AUTOARG*/

    // General Input
    resetb, pi_viClk

);

    				// General inputs
    input
	resetb;			// Reset
    input
	pi_viClk;		// PI Clock = io_clk = gfx_clk

    wire	resetb;		// Reset, low active

    wire        pi_viClk;	// Processor interface clock
    wire        pi_viReq;       // Processor register transaction start
    wire        pi_viRd;        // When asserted, processor transaction is a read
    wire        vi_piAck;       // Processor register transaction complete
    wire [7:0]  pi_viAddr;	// Register address
    wire [15:0] pi_viData;      // Write data, bound for register
    wire [15:0] vi_piData;      // Read data, from register
    wire        vi_piVrtIntr;   // Vertical Interrupt

    wire        vi_memReq;      // Assert to enqueue a memory request
    wire [25:5] vi_memAddr;     // Cache line aligned addr, valid when memReq asserted
    wire        mem_viAck;      // Asserted by memory controller to end transaction
    wire [63:0] mem_viData;     // Data from memory controller

    wire        vi_ioVsync;     // Vertical sync
    wire        vi_ioHsync;     // Horizontal sync

    wire [1:0]  ioGun_trg;      // Light gun inputs from controller

    reg		clk27;
    wire        clk54;   	// Input to video crystal oscillator/mux
    wire        vicr_topad;     // Distinguishes YCr phase from YCb phase
    wire [7:0]  vid_topad;      // Video output data
    wire [1:0]  visel_tocore;   // Status of DTV pin
    wire        viclksel;       // VI Clk select (54MHz or 27MHz)
    reg		clksel;

//      clkgen viclk27 ( .period(37000), .clk(clk27));
	clkgen viclk54 ( .period(18500), .clk(clk54));    // XXX we choose async vs. flipper

	initial begin
		clk27	<=	1'b0;
	end
	always@(posedge clk54) begin
		clk27	<=	~clk27;
	end
	always@(negedge clk54) begin
		clksel	<=	viclksel;
	end

	wire	viclk	=	(!resetb | !clksel) ? clk27 : clk54;

        assign  ioGun_trg       =       2'b0;
        assign  visel_tocore    =       'bx;            // XXX

//	define mode settings for each display format

//	note:  INT stride STD = 2*WPL;  fix WPL at max, so
//	use single source image config. *may be a problem, precludes correct source data fetching?*
//	assume !INT stride = INT stride/2 (PROG, 3D)
//	let source image width=64 words (1024 pixels)
//	therefore stride (!INT)=64, stride (INT)=128.

`define	mem_dumpfile	vi_flipper.mem_dumpfile
`define memarray	vi_flipper.memarray

`define	VCLKSEL	1'd0		//use 27MHz video clock (if progressive or 3D formats, override later)
`define POLARITY 16'd0		//override later if desired to invert any flags [7:0]

`ifdef NTSC_EURGB60_INT         //NTSC or EURGB60 interlaced

`define EQU     4'd6
`define ACV     12'd240
`define PRBodd  16'd24
`define PRBeven 16'd25
`define PSBodd  16'd3
`define PSBeven 16'd2
`define BS1     5'd12
`define BS2     5'd13
`define BS3     5'd12
`define BS4     5'd13
`define BE1     11'd520
`define BE2     11'd519
`define BE3     11'd520
`define BE4     11'd519
`define NHLINES 10'd525 	//define total # lines, unused
`define HLW     16'd429
`define HSY     7'd64
`define HCS     8'd71
`define HCE     8'd105
`define HBE     10'd162
`define HBS     10'd373
`define HBED    15'd122 	//debug h border end
`define HBEN    1'd0            //debug h border enable
`define HBSD    16'd412 	//debug h border start
`define WPL     8'd40           //#words read per line          XXX
`define STD     8'd48		//# words stride                XXX
`define NIN     1'd0    	//noninterlace enable
`define FMT     2'd0    	//format NTSC

`endif

`ifdef NTSC_EURGB60_DS		//NTSC or EURGB60 double strike

`define EQU     4'd6
`define ACV     12'd240
`define PRBodd  16'd24
`define PRBeven 16'd24
`define PSBodd  16'd4
`define PSBeven 16'd4
`define BS1     5'd12
`define BS2     5'd12
`define BS3     5'd12
`define BS4     5'd12
`define BE1     11'd520
`define BE2     11'd520
`define BE3     11'd520
`define BE4     11'd520
`define NHLINES 10'd526 	//define total # lines, unused
`define HLW     16'd429
`define HSY     7'd64
`define HCS     8'd71
`define HCE     8'd105
`define HBE     10'd162
`define HBS     10'd373
`define HBED    15'd122 	//debug h border end
`define HBEN    1'd0            //debug h border enable
`define HBSD    16'd412 	//debug h border start
`define WPL     8'd40           //#words read per line          XXX
`define STD     8'd48           //# words stride                XXX
`define NIN     1'd1    	//noninterlace enable
`define FMT     2'd0    	//format NTSC

`endif

`ifdef	PAL_INT			//PAL interlaced

`define EQU     4'd5
`define ACV     12'd287
`define PRBodd  16'd35
`define PRBeven 16'd36
`define PSBodd  16'd1
`define PSBeven 16'd0
`define BS1     5'd13
`define BS2     5'd12
`define BS3     5'd11
`define BS4     5'd10
`define BE1     11'd619
`define BE2     11'd618
`define BE3     11'd617
`define BE4     11'd620
`define NHLINES 10'd625 	//define total # lines, unused
`define HLW     16'd432
`define HSY     7'd64
`define HCS     8'd71
`define HCE     8'd105
`define HBE     10'd162
`define HBS     10'd373
`define HBED    15'd133 	//debug h border end
`define HBEN    1'd0            //debug h border enable
`define HBSD    16'd420 	//debug h border start
`define WPL     8'd40           //#words read per line          XXX
`define STD     8'd48		//# words stride                XXX
`define NIN     1'd0    	//noninterlace enable
`define FMT     2'd2    	//format PAL

`endif

`ifdef PAL1_INT			//PAL var interlaced

`define EQU     4'd5
`define ACV     12'd287
`define PRBodd  16'd35
`define PRBeven 16'd36
`define PSBodd  16'd1
`define PSBeven 16'd0
`define BS1     5'd13
`define BS2     5'd12
`define BS3     5'd11
`define BS4     5'd10
`define BE1     11'd619
`define BE2     11'd618
`define BE3     11'd617
`define BE4     11'd620
`define NHLINES 10'd625 	//define total # lines, unused
`define HLW     16'd432
`define HSY     7'd64
`define HCS     8'd75
`define HCE     8'd106
`define HBE     10'd172
`define HBS     10'd380
`define HBED    15'd133 	//debug h border end
`define HBEN    1'd0            //debug h border enable
`define HBSD    16'd420 	//debug h border start
`define WPL     8'd40           //#words read per line          XXX
`define STD     8'd48           //# words stride                XXX
`define NIN     1'd0    	//noninterlace enable
`define FMT     2'd2    	//format PAL

`endif

`ifdef PAL_DS         		//PAL double strike

`define EQU     4'd5
`define ACV     12'd287
`define PRBodd  16'd33
`define PRBeven 16'd33
`define PSBodd  16'd2
`define PSBeven 16'd2
`define BS1     5'd13
`define BS2     5'd11
`define BS3     5'd13
`define BS4     5'd11
`define BE1     11'd619
`define BE2     11'd621
`define BE3     11'd619 
`define BE4     11'd621
`define NHLINES 10'd624 	//define total # lines, unused
`define HLW     16'd432
`define HSY     7'd64
`define HCS     8'd71
`define HCE     8'd105
`define HBE     10'd162
`define HBS     10'd373
`define HBED    15'd133 	//debug h border end
`define HBEN    1'd0            //debug h border enable
`define HBSD    16'd420 	//debug h border start
`define WPL     8'd40           //#words read per line          XXX
`define STD     8'd48		//# words stride                XXX
`define NIN     1'd1    	//noninterlace enable
`define FMT     2'd2    	//format PAL

`endif

`ifdef PAL1_DS         		//PAL var double strike

`define EQU     4'd5
`define ACV     12'd287
`define PRBodd  16'd33
`define PRBeven 16'd33
`define PSBodd  16'd2
`define PSBeven 16'd2
`define BS1     5'd13
`define BS2     5'd11
`define BS3     5'd13
`define BS4     5'd11
`define BE1     11'd619
`define BE2     11'd621
`define BE3     11'd619
`define BE4     11'd621
`define NHLINES 10'd624 	//define total # lines, unused
`define HLW     16'd432
`define HSY     7'd64
`define HCS     8'd75
`define HCE     8'd106
`define HBE     10'd172
`define HBS     10'd380
`define HBED    15'd133 	//debug h border end
`define HBEN    1'd0            //debug h border enable
`define HBSD    16'd420 	//debug h border start
`define WPL     8'd40           //#words read per line          XXX
`define STD     8'd48           //# words stride                XXX
`define NIN     1'd1    	//noninterlace enable
`define FMT	2'd2		//format PAL

`endif

`ifdef MPAL_INT         	//MPAL interlaced

`define EQU     4'd6
`define ACV     12'd240
`define PRBodd  16'd24
`define PRBeven 16'd25
`define PSBodd  16'd3
`define PSBeven 16'd2
`define BS1     5'd16
`define BS2     5'd15
`define BS3     5'd14
`define BS4     5'd13
`define BE1     11'd518
`define BE2     11'd517
`define BE3     11'd516
`define BE4     11'd519
`define NHLINES 10'd525 	//define total # lines, unused
`define HLW     16'd429
`define HSY     7'd64
`define HCS     8'd78
`define HCE     8'd112
`define HBE     10'd162
`define HBS     10'd373
`define HBED    15'd122 	//debug h border end
`define HBEN    1'd0            //debug h border enable
`define HBSD    16'd412 	//debug h border start
`define WPL     8'd40           //#words read per line          XXX
`define STD     8'd48           //# words stride                XXX
`define NIN     1'd0    	//noninterlace enable
`define FMT     2'd2    	//format MPAL

`endif

`ifdef MPAL_DS         		//MPAL double strike

`define EQU     4'd6
`define ACV     12'd240
`define PRBodd  16'd24
`define PRBeven 16'd24
`define PSBodd  16'd4
`define PSBeven 16'd4
`define BS1     5'd16
`define BS2     5'd14
`define BS3     5'd16
`define BS4     5'd14
`define BE1     11'd518
`define BE2     11'd520
`define BE3     11'd518
`define BE4     11'd520
`define NHLINES 10'd526 	//define total # lines, unused
`define HLW     16'd429
`define HSY     7'd64
`define HCS     8'd78
`define HCE     8'd112
`define HBE     10'd162
`define HBS     10'd373
`define HBED    15'd122 	//debug h border end
`define HBEN    1'd0            //debug h border enable
`define HBSD    16'd412 	//debug h border start
`define WPL     8'd40           //#words read per line          XXX
`define STD     8'd48           //# words stride                XXX
`define NIN     1'd1    	//noninterlace enable
`define FMT     2'd2    	//format MPAL

`endif

`ifdef NTSC_PROG         	//NTSC progressive

`define EQU     4'd12
`define ACV     12'd480
`define PRBodd  16'd48
`define PRBeven 16'd48
`define PSBodd  16'd6
`define PSBeven 16'd6
`define BS1     5'd24
`define BS2     5'd24
`define BS3     5'd24
`define BS4     5'd24
`define BE1     11'd1038
`define BE2     11'd1038
`define BE3     11'd1038
`define BE4     11'd1038
`define NHLINES 11'd1050 	//define total # lines, unused
`define HLW     16'd429
`define HSY     7'd64
`define HCS     8'd71
`define HCE     8'd105
`define HBE     10'd162
`define HBS     10'd373
`define HBED    15'd122 	//debug h border end
`define HBEN    1'd0            //debug h border enable
`define HBSD    16'd412 	//debug h border start
`define WPL     8'd40           //#words read per line          XXX
`define STD     8'd48           //# words stride                XXX
`define NIN     1'd1    	//noninterlace enable
`define FMT     2'd0    	//format NTSC
`define VCLKSEL	1'd1   		//use 27MHz video clock

`endif

`ifdef NTSC_3D			//NTSC stereo

`define EQU     4'd12
`define ACV     12'd480
`define PRBodd  16'd44
`define PRBeven 16'd44
`define PSBodd  16'd10
`define PSBeven 16'd10
`define BS1     5'd24
`define BS2     5'd24
`define BS3     5'd24
`define BS4     5'd24
`define BE1     11'd1038
`define BE2     11'd1038
`define BE3     11'd1038
`define BE4     11'd1038
`define NHLINES 11'd1050 	//define total # lines, unused
`define HLW     16'd429
`define HSY     7'd64
`define HCS     8'd71
`define HCE     8'd105
`define HBE     10'd168
`define HBS     10'd379
`define HBED    15'd122 	//debug h border end
`define HBEN    1'd0            //debug h border enable
`define HBSD    16'd412 	//debug h border start
`define WPL     8'd127          //#words read per line          XXX
`define STD     8'd128          //# words stride                XXX
`define NIN     1'd1    	//noninterlace enable
`define FMT     2'd0    	//format NTSC
`define VCLKSEL 1'd1           	//use 27MHz video clock

`endif

`ifdef GCA_INT         		//GCA interlaced

`define EQU     4'd6
`define ACV     12'd241
`define PRBodd  16'd24
`define PRBeven 16'd25
`define PSBodd  16'd1
`define PSBeven 16'd0
`define BS1     5'd12
`define BS2     5'd13
`define BS3     5'd12
`define BS4     5'd13   
`define BE1     11'd520 
`define BE2     11'd519
`define BE3     11'd520
`define BE4     11'd519
`define NHLINES 10'd525 	//define total # lines, unused
`define HLW     16'd429
`define HSY     7'd64
`define HCS     8'd71
`define HCE     8'd105
`define HBE     10'd159
`define HBS     10'd370
`define HBED    15'd122 	//debug h border end
`define HBEN    1'd0            //debug h border enable
`define HBSD    16'd412 	//debug h border start
`define WPL     8'd64           //#words read per line          XXX
`define STD     8'd128          //# words stride                XXX
`define NIN     1'd0    	//noninterlace enable
`define FMT     2'd0    	//format NTSC

`endif

`ifdef GCA_PROG         	//GCA progressive

`define EQU     4'd12            
`define ACV     12'd480
`define PRBodd  16'd48
`define PRBeven 16'd48
`define PSBodd  16'd6
`define PSBeven 16'd6
`define BS1     5'd24
`define BS2     5'd24
`define BS3     5'd24   
`define BS4     5'd24   
`define BE1     11'd1038 
`define BE2     11'd1038
`define BE3     11'd1038
`define BE4     11'd1038         
`define NHLINES 10'd1050 	//define total # lines, unused
`define HLW     16'd429
`define HSY     7'd64
`define HCS     8'd71
`define HCE     8'd105
`define HBE     10'd180
`define HBS     10'd391
`define HBED    15'd122 	//debug h border end
`define HBEN    1'd0            //debug h border enable
`define HBSD    16'd412 	//debug h border start                          
`define WPL     8'd127          //#words read per line          XXX
`define STD     8'd128          //# words stride                XXX
`define NIN     1'd1    	//noninterlace enable
`define FMT     2'd0    	//format NTSC   
`define VCLKSEL 1'd1           	//use 27MHz video clock

`endif

wire [10:0]	BE1, BE2, BE3, BE4;	//define new vars to support V_SHORT use below

`ifdef	V_SHORT			//option to display only 8 lines per field/frame
	assign	BE1	=	`BE1 - 2*(`ACV - 12'd8);
        assign	BE2     =       `BE2 - 2*(`ACV - 12'd8);
        assign	BE3     =       `BE3 - 2*(`ACV - 12'd8);
        assign	BE4     =       `BE4 - 2*(`ACV - 12'd8);
        `define	ACV     12'd8
`else
	assign  BE1     =       `BE1;
	assign  BE2     =       `BE2;
	assign  BE3     =       `BE3;
	assign  BE4     =       `BE4;
`endif

`ifdef	VI_DEBUG		//option invokes border (for fixed length 720 encoders)
	`define FMT     2'd3
	`define HBE	`HBED
	`define HBS	`HBSD
	`define HBEN	1'b1
`endif

    wire[9:0]	HBEw	=	`HBE;
    wire[7:0]   stride	=	`STD;
    reg [31:0]	instruction;
    reg		early, done;

    assign	pi_viReq	=	instruction[28];
    assign      pi_viRd		=       instruction[24];
    assign      pi_viAddr	=       instruction[23:16];
    assign      pi_viData	=       instruction[15:0];

  	always @(negedge resetb) begin
		instruction	<=	{8'b0, 8'b1, 8'hxx, 16'hxxxx};	//PI bus to idle
		early		<=	1'b0;
		done		<=	1'b0;
        end

	always @(posedge resetb) begin					//setup VI after reset ends
		@(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b0, 8'h0e, 16'h0000};  //top field L *always used*
                @(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b0, 8'h0f, 16'h0000};
                @(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b0, 8'h10, 16'h0000};  //top field R
                @(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b0, 8'h11, 16'h4000};	// XXX set at 16KB (interlaced)
                @(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b0, 8'h12, 16'h0000};  //bottom field L
                @(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b0, 8'h13, 16'h0000};
                @(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b0, 8'h14, 16'h0000};  //bottom field R FBBhi
                @(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b0, 8'h15, 16'h0000};  //FBBlo
                @(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b0, 8'h16, 16'h0000};  //HCT *doesn't exist!*
                @(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b0, 8'h17, 16'h0000};  //VCT *doesn't exist!*
                @(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b0, 8'h18, 16'h0002};  //begin display int
                @(posedge pi_viClk);					//1 vcount fails PAL
                instruction     <=      {4'b1, 4'b0, 8'h19, 16'h0001};
                @(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b0, 8'h1a, 16'h1002};
                @(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b0, 8'h1b, 16'h0002};
                @(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b0, 8'h1c, 16'h0000};
                @(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b0, 8'h1d, 16'h0000};
                @(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b0, 8'h1e, 16'h0000};
                @(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b0, 8'h1f, 16'h0000};  //end display int
                @(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b0, 8'h20, 16'h0000};  //begin display latch
                @(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b0, 8'h21, 16'h0000};
                @(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b0, 8'h22, 16'h0000};
                @(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b0, 8'h23, 16'h0000};  //end display latch
                @(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b0, 8'h25, 16'h0100};  //horizontal scale HS_EN, STP
                @(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b0, 8'h26, 16'h0000};  //begin filter coeff
                @(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b0, 8'h27, 16'h0000};
                @(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b0, 8'h28, 16'h0000};
                @(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b0, 8'h29, 16'h0000};
                @(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b0, 8'h2a, 16'h0000};
                @(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b0, 8'h2b, 16'h0000};
                @(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b0, 8'h2c, 16'h0000};
                @(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b0, 8'h2d, 16'h0000};
                @(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b0, 8'h2e, 16'h0000};
                @(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b0, 8'h2f, 16'h0000};
                @(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b0, 8'h30, 16'h0000};
                @(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b0, 8'h31, 16'h0000};
                @(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b0, 8'h32, 16'h0000};
                @(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b0, 8'h33, 16'h0000};  //end filter coeff
                @(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b0, 8'h34, 16'h0000};  //unused
                @(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b0, 8'h35, `POLARITY}; //polarity out
                @(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b0, 8'h36, 15'h0000, `VCLKSEL};  //clk select VCLKSEL
                @(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b0, 8'h37, 16'h0000};  //DTV status VISEL
                @(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b0, 8'h38, 16'h0000};  //scaling width SRCWIDTH
                @(posedge pi_viClk);

                @(posedge pi_viClk);
		instruction     <=      {4'b1, 4'b0, 8'h00, `ACV, `EQU};	//ACV, EQU
                @(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b0, 8'h02, `HCS, `HCE};	//HCE, HCS
                @(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b0, 8'h03, `HLW};		//HLW
                @(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b0, 8'h04, 5'h0, `HBS, HBEw[9]};	//HBS, HBEhi
                @(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b0, 8'h05, HBEw[8:0], `HSY};	//HBElo, HSY
                @(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b0, 8'h06, `PSBodd};		//PSB odd
                @(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b0, 8'h07, `PRBodd};		//PRB odd
                @(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b0, 8'h08, `PSBeven};		//PSB even
                @(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b0, 8'h09, `PRBeven};		//PRB even
                @(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b0, 8'h0a,  BE3, `BS3};	//BE3, BS3
                @(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b0, 8'h0b,  BE1, `BS1};	//BE1, BS1
                @(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b0, 8'h0c,  BE4, `BS4};	//BE4, BS4
                @(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b0, 8'h0d,  BE2, `BS2};	//BE2, BS2
                @(posedge pi_viClk);
		instruction     <=      {4'b1, 4'b0, 8'h39, `HBEN, `HBED}; 	//HBE debug en/border
                @(posedge pi_viClk);    
                instruction     <=      {4'b1, 4'b0, 8'h3a, `HBSD};      	//HBS debug border
                @(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b0, 8'h24, `WPL, `STD};  	//picture config WPL, STD XXX??
                @(posedge pi_viClk);
		instruction     <=      {4'b1, 4'b0, 8'h01, 6'b0, `FMT, 5'b0, `NIN, 2'b1};	//start
                @(posedge pi_viClk);
		instruction     <=      {4'b1, 4'b1, 8'h34, 16'hxxxx};  	//return PI bus data to idle
                @(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b1, 8'h1a, 16'hxxxx};  	//poll for vertical count=2 (early display)
                  while (early !== 1'b1) begin
                        @(posedge pi_viClk);
                                early    <=	vi_piData[15];
                  end
                @(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b1, 8'h34, 16'hxxxx};  	//return PI bus data to idle

		@(posedge pi_viClk);
                instruction     <=      {4'b1, 4'b0, 8'h18, 16'h1002};  	//reset interrupt status
                @(posedge pi_viClk);						//1 vcount fails PAL
                instruction     <=      {4'b1, 4'b1, 8'h34, 16'hxxxx};  	//return PI bus data to idle
		@(posedge pi_viClk);
		instruction     <=      {4'b1, 4'b1, 8'h18, 16'hxxxx};  	//poll for vertical count=1 to end sim
		  while (done !== 1'b1) begin
			@(posedge pi_viClk);
				done	<=	vi_piData[15];
		  end
//**add finish command here if desire to stop after 2 fields/frames, else adjust for desired # frames
//**currently finish is controlled globally in tests.v file
	end

   /*
    * Instantiations
    */

    vi		vi(
    
		.viResetb(resetb), 
		.pi_viClk(pi_viClk), 
		.pi_viReq(pi_viReq),
		.pi_viRd(pi_viRd), 
		.pi_viAddr(pi_viAddr), 
		.pi_viData(pi_viData),

		.mem_viAck(mem_viAck), 
		.mem_viData(mem_viData),

		.ioGun_trg(ioGun_trg),

		.clk27(viclk),
		.viclksel(viclksel),
		.visel_tocore(visel_tocore),

    		.vi_piAck(vi_piAck), 
		.vi_piData(vi_piData), 
		.vi_piVrtIntr(vi_piVrtIntr),

		.vi_memReq(vi_memReq), 
		.vi_memAddr(vi_memAddr),

		.vi_ioVsync(vi_ioVsync), 
		.vi_ioHsync(vi_ioHsync),

		.vicr_topad(vicr_topad), 
		.vid_topad(vid_topad)
                );

        vi_mem	vi_mem	(
                .io_clk(pi_viClk),		// XXXX host i/f clock
                .resetb(resetb),

		.DiMemAddr(vi_memAddr),
		.DiMemReq(vi_memReq),
		.DiMemRd(1'b1),
		.DiMemData(mem_viData),
		.DiMemFlush(1'b0),

                .DiMemAck(mem_viAck),
                .MemData(mem_viData),
		.MemFlushAck()
                );

`ifdef VI_VERBOSE
	reg [63:0]	rdata_d1, rdata_d2, rdata_d3;
	reg		req_d1, req_d2, req_d3, req_d4;
	reg [20:0]	read_addr;

	always@(posedge pi_viClk) begin
		if (resetb) begin
			rdata_d1	<=	mem_viData;
			rdata_d2        <=      rdata_d1;
                        rdata_d3        <=      rdata_d2;
			req_d1		<=	vi_memReq;
			req_d2          <=      req_d1;
                        req_d3          <=      req_d2;
                        req_d4          <=      req_d3;
		end
		if (vi_memReq) begin
			read_addr	<=	vi_memAddr;
		end
		if (req_d4) begin
			$display("%t: read fb0 %M: address=%d read_data=%h", $time, ((read_addr << 2) + 0), rdata_d3);
			$display("%t: read fb1 %M: address=%d read_data=%h", $time, ((read_addr << 2) + 1), rdata_d2);
			$display("%t: read fb2 %M: address=%d read_data=%h", $time, ((read_addr << 2) + 2), rdata_d1);
			$display("%t: read fb3 %M: address=%d read_data=%h", $time, ((read_addr << 2) + 3), mem_viData);
		end
	end
`endif

    reg [32*8:1] mem_initfile;
    reg [32*8:1] mem_dumpfile;

    reg [63:0]
        memarray[0:262143];

    initial
      begin
        mem_initfile = "vi_mem2.init";
        mem_dumpfile = "vi_mem2.dump";
        $readmemh( mem_initfile, memarray );
     	//#6600;
      	//$writememh( mem_dumpfile, memarray );
      end


	task vget;
		input [15:0] off;
		output [31:0] word;
		reg [63:0] data;
		begin
			data = memarray[off>>3];
			if (off[2]) word = data[31:0];
			else word = data[63:32];
		end
	endtask

	task vput;				// big endian for now
		input [11:0] count;
		input [15:0] off;
		input [7:0] pix;
		reg [63:0] data;
		begin
`ifdef	VI_VERBOSE
			$display("%t: output pixel %M: pixel_count=%d offset=%d pixel_data=%h", $time, count, off, pix);
`endif
			data = memarray[off>>3];
			if 	(off[2:0] == 3'h0) data[63:56] = pix;
			else if (off[2:0] == 3'h1) data[55:48] = pix;
                        else if (off[2:0] == 3'h2) data[47:40] = pix;
                        else if (off[2:0] == 3'h3) data[39:32] = pix;
                        else if (off[2:0] == 3'h4) data[31:24] = pix;
                        else if (off[2:0] == 3'h5) data[23:16] = pix;
                        else if (off[2:0] == 3'h6) data[15:08] = pix;
                        else 	data[07:00] = pix;
`ifdef  VI_VERBOSE
			$display("%t: write memarray word  %M: data=%h", $time, data);
`endif
			memarray[off>>3] = data;
		end
	endtask

endmodule // vi_flipper
