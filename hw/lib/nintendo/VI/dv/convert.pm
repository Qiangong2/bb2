#package convert;

#
# routines for converting between rgb, ycrycb, pyc, and ppm pixel formats
#

# r,g,b [0,255]
# y [0,235]
# cr, cb [16,240]

sub rgb2ycrcb {
    my @rgb = @_;
    my @ycrcb;
    my $verbose = 0;

    while (@rgb) {
        my ($r,$g,$b) = splice(@rgb,0,3);
        my ($y,$cr,$cb);

        printf STDERR "rgb2ycrcb: rgb :r:g:b:  :%f:%f:%f:\n",$r,$g,$b if ($verbose);

        # generic equations from Video Demysitified
#        $y = 0.257*$r + 0.504*$g + 0.098*$b + 16;
#        $cr = 0.439*$r - 0.368*$g - 0.071*$b + 128;
#        $cb = -0.148*$r - 0.291*$g + 0.439*$b + 128;

        # inverse of ALI YCrCb->RGB equations
        $y = 0.2571309*$r + 0.5043721*$g + 0.0975575*$b + 16;
        $cr = 0.4396446*$r - 0.3683894*$g - 0.0712552*$b + 128;
        $cb = -0.1482108*$r - 0.2907212*$g + 0.4389321*$b + 128;

        printf STDERR "rgb2ycrcb: float :y:cr:cb:  :%f:%f:%f:\n",$y,$cr,$cb if ($verbose);

        # round away from zero
        $y = int($y+0.5);
        $cb = $cb >= 128 ? int( $cb + 0.5 ) : 128 - int( 128 - $cb + 0.5 );
        $cr = $cr >= 128 ? int( $cr + 0.5 ) : 128 - int( 128 - $cr + 0.5 );

        printf STDERR "rgb2ycrcb: pre-sat :y:cr:cb:  :%02x:%02x:%02x:\n",$y,$cr,$cb if ($verbose);

        $y = saturate($y,0,235);
        $cr = saturate($cr,16,240);
        $cb = saturate($cb,16,240);

        push @ycrcb,($y,$cr,$cb);
        printf STDERR "rgb2ycrcb: :y:cr:cb:  :%02x:%02x:%02x:\n",$y,$cr,$cb if ($verbose);
    }

    return @ycrcb;
}

# y []
# cr, cb []
# r, g, b [0,255]

sub ycrcb2rgb {
    my @ycrcb = @_;
    my @rgb;

    my $nintendo = 0;
    my $verbose = 0;

    while (@ycrcb) {
        my ($y, $cr, $cb) = splice(@ycrcb,0,3);
        my ($r, $g, $b);

        printf STDERR "ycrcb2rgb:  ycrcb :%2x:%2x:%2x: \n",$y,$cr,$cb if ($verbose);
        
        # correct for bias
        
        if ( $y >= 16 ) {
            $y -= 16;
            $cr -= 128;
            $cb -= 128;
        }
        
        printf STDERR "ycrcb2rgb:  post-fix ycrcb :%2x:%2x:%2x: \n",$y,$cr,$cb if ($verbose);
        
        if ( $nintendo ) {
            
            # Nintendo's YCrCb->RGB equations
            
            # R = (1.171875 * Y) + (1.59375 * Cr)
            # G = (1.171875 * Y) - (0.390625 * Cb) - (0.8125 * Cr)
            # B = (1.171875 * Y) + (2.015625 * Cb)
            
            $r = (1.171875 * $y) + (1.59375 * $cr);
            $g = (1.171875 * $y) - (0.390625 * $cb) - (0.8125 * $cr);
            $b = (1.171875 * $y) + (2.015625 * $cb);

        } else {
            
            # ALi's YCrCb->RGB equations
            
            # R = 1.1640625 * Y + 1.59375 * Cr
            # G = 1.1640625 * Y - 0.8125 * Cr  - 0.390625 * Cb
            # B = 1.1640625 * Y               + 2.01953125*Cb
            
            $r = 1.1640625 * $y + 1.59375 * $cr;
            $g = 1.1640625 * $y - 0.8125 * $cr  - 0.390625 * $cb;
            $b = 1.1640625 * $y               + 2.01953125*$cb;
            
        }

        printf STDERR "ycrcb2rgb:  float ycrcb :%2x:%2x:%2x: rgb :%f:%f:%f:\n",$y,$cr,$cb,$r,$g,$b if ($verbose);
        
        # round up
        
        $r = int( $r + 0.5 );
        $g = int( $g + 0.5 );
        $b = int( $b + 0.5 );
        
        printf STDERR "ycrcb2rgb:  rounded ycrcb :%2x:%2x:%2x: rgb :%x:%x:%x: rgb :%f:%f:%f:\n",$y,$cr,$cb,$r,$g,$b,$r,$g,$b if ($verbose);

        # saturate 
        
        $r = saturate( $r, 0, 255 );
        $g = saturate( $g, 0, 255 );
        $b = saturate( $b, 0, 255 );
        
        printf STDERR "ycrcb2rgb: soln ycrcb :%2x:%2x:%2x: rgb :%2x:%2x:%2x:\n",$y,$cr,$cb,$r,$g,$b if ($verbose);
        
        push @rgb, ($r, $g, $b);
    }

#    printf STDERR "ycrcb2rgb: returning to caller\n";

    return @rgb;

}

sub saturate {
    my ($val,$min,$max) = @_;
    
    if ($val < $min) { $val = $min; }
    if ($val > $max) { $val = $max; }

    return $val;
}

sub pyc2rgb {
    my @pyc = @_;
    my @rgb;

    my $verbose = 0;
    
    while (@pyc) {
        my ($p1, $y1, $c1, $p2, $y2, $c2) = splice(@pyc,0,6);
        my ($r1, $g1, $b1, $r2, $g2, $b2);

        if ( $verbose ) {
            printf "pyc1 :%02x:%02x:%02x:   :%d:%d:%d:\n",$p1,$y1,$c1,$p1,$y1,$c1;
            printf "pyc2 :%02x:%02x:%02x:   :%d:%d:%d:\n",$p2,$y2,$c2,$p2,$y2,$c2;
        }

        if ( $p1 == 1 ) {
            print STDERR "pyc2rgb: warning, YCr and YCb samples are out of order (assuming HBE is even)\n";
            ($r1, $g1, $b1) = ycrcb2rgb($y1,$c1,$c2);
            ($r2, $g2, $b2) = ycrcb2rgb($y2,$c1,$c2);
        } else {
            ($r1, $g1, $b1) = ycrcb2rgb($y1,$c2,$c1);
            ($r2, $g2, $b2) = ycrcb2rgb($y2,$c2,$c1);
        }

        if ( $verbose ) {
            printf "rgb1 :%02x:%02x:%02x:   :%d:%d:%d:\n",$rgb[0],$rgb[1],$rgb[2],$rgb[0],$rgb[1],$rgb[5];
            printf "rgb2 :%02x:%02x:%02x:   :%d:%d:%d:\n",$rgb[3],$rgb[4],$rgb[5],$rgb[3],$rgb[4],$rgb[5];
        }
        
        push @rgb, ($r1, $g1, $b1, $r2, $g2, $b2);
    }

    return @rgb;
}

1;
