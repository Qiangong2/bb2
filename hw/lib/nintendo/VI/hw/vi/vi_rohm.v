///////////////////////////////////////////////////////////////////////////////
//
//   (C) 1999 ARTX INC..  ALL RIGHTS RESERVED.  UNPUBLISHED -- RIGHTS
//   RESERVED UNDER THE COPYRIGHT LAWS OF THE UNITED STATES.  USE OF A
//   COPYRIGHT NOTICE IS PRECAUTIONARY ONLY AND DOES NOT IMPLY PUBLICATION
//   OR DISCLOSURE.
//
//   THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF
//   ARTX INC..  USE, DISCLOSURE, OR REPRODUCTION IS PROHIBITED WITHOUT
//   THE PRIOR EXPRESS WRITTEN PERMISSION OF ARTX INC..
//
//                   RESTRICTED RIGHTS LEGEND
//
//   Use, duplication, or disclosure by the Government is subject to
//   restrictions as set forth in subparagraph (c)(1)(ii) of the Rights
//   in Technical Data and Computer Software clause at DFARS 252.227-7013
//   or subparagraphs (c)(1) and (2) of Commercial Computer Software --
//   Restricted Rights at 48 CFR 52.227-19, as applicable.
//
//   ArtX Inc.
//   3400 Hillview Ave, Bldg 5
//   Palo Alto, CA 94304
//
///////////////////////////////////////////////////////////////////////////////
`include "vi_state.v"
`include "vi_reg.v"


module vi_rohm (
	csyncb, field, field_601, vsyncb, hsyncb, clr_brstb, brst_blk, vi_mode, nintl,
	vClk, viResetb, pixel_in, spixel_in, dsp_en, out_data, vblank, fld_cnt,
	vphase_out, hblank_e, vi_dbg, hdsp_en, vi_reset, hsync_out, vsync_out,
	hscaler_en, ipol, npol, kpol, bpol, hpol, vpol, fpol, cpol, rst_phase_cnt,
	incr_line, V_601, hbe_odd, hlw_odd, dtv, hdsp_en656, dsp_en656, hblank656_e,
	brdr_en);

input 		vClk, viResetb;
input		csyncb, field, vsyncb, hsyncb, clr_brstb, 
		brst_blk, nintl, hblank_e, vi_dbg,
		vblank, hdsp_en, vi_reset, field_601;
input [7:0]	pixel_in, spixel_in;
input 		dsp_en, hscaler_en;
input [1:0]	fld_cnt;
input [1:0]	vi_mode;
input 		V_601;
input 		dtv;
output [7:0]	out_data;
output 		vphase_out, hsync_out, vsync_out;
input             ipol, npol, kpol, bpol, hpol, vpol, fpol, cpol;
input 		rst_phase_cnt, incr_line;
input 		hbe_odd;
input 		hlw_odd;
input 		hdsp_en656, dsp_en656;
input 		hblank656_e;
input 		brdr_en;

wire [7:0]	flag;
reg  [7:0]	flag_d1, flag_d2, flag_d3, flag_d4, flag_d5, flag_d6, flag_d7, flag_d8;
wire [1:0]	fld_cnt;
wire 		vphase, luma;
reg		vphase_out, hsync_out, vsync_out, vphase_out_e;
reg [4:0]		current_phase, next_phase;
reg [7:0]	vdata, pixel_in_d1, pixel_in_d2, 
		data_in_601, data_out_601, pixel_in_d3, pixel_in_d4, pixel_in_d5, pixel_in_d6,
		pixel_in_d7, pixel_in_d8;
reg		toggle, hdsp_en_d1, hdsp_en_d2, dsp_en_d1, dsp_en_d2, hdsp_en_d3,
		hdsp_en_d4, hdsp_en_d5, hdsp_en_d6, hdsp_en_d7, hdsp_en_d8, hdsp_en_d9,
		dsp_en_d3, dsp_en_d4, dsp_en_d5, dsp_en_d6, dsp_en_d7, dsp_en_d8,
		field_601_d1, field_601_d2, field_601_d3, field_601_d4, field_601_d5, 
		field_601_d6, V_601_d1, V_601_d2, V_601_d3, V_601_d4, V_601_d5, V_601_d6,
	hdsp_en656_d1, hdsp_en656_d2, hdsp_en656_d3, hdsp_en656_d4, hdsp_en656_d5, hdsp_en656_d6,
	hdsp_en656_d7, hdsp_en656_d8, hdsp_en656_d9, hblank656_e_d1, hblank656_e_d2, hblank656_e_d3,
	hblank656_e_d4, hblank656_e_d5, hblank656_e_d6, hblank656_e_d7;

reg [3:0]	tim_ref_state, nxt_ref_state;
reg		vblank_d1;
reg		hblank_e_d1, hblank_e_d2, hblank_e_d3, hblank_e_d4, hblank_e_d5, hblank_e_d6,
		hblank_e_d7;
wire 		V, F, P0, P1, P2, P3;
reg 		H;
wire [7:0]	blank_pixel;
reg [1:0]	vphase_cnt;
reg		vphase_d1, vphase_d2;
reg 		luma_d1, luma_d2, luma_d3, luma_d4, luma_d5, luma_d6, luma_d7, luma_d8;
reg 		hdsp_en_detected;
reg 		dsp_en_sel, strt_hblank, end_hblank;
reg		vblank_d2, vblank_d3;
wire		pal;
reg 		dsp_en_d10, dsp_en_d9;
wire [7:0]	out_data_e;
reg  [7:0]       out_data;


assign pal = ((vi_mode == 2'b01) || (vi_mode == 2'b10));
assign flag[7] = csyncb ^ cpol;
assign flag[6] = ((field && ~dtv) ^ fpol);
assign flag[5] = vsyncb ^ vpol;
assign flag[4] = hsyncb ^ hpol;
assign flag[3] = clr_brstb ^ bpol;
assign flag[2] = brst_blk ^ kpol;
assign flag[1] = pal ^ npol;
assign flag[0] = nintl ^ ipol;


parameter	inactive = `INACTIVE, luma_cb = `LUMA_CB, cb = `CB,
		luma_cr = `LUMA_CR, cr = `CR, inactive_pix = `INACTIVE_PIX,
		active_pix = `ACTIVE_PIX, preamble0 = `PREAMBLE0,
		preamble1 = `PREAMBLE1, preamble2 = `PREAMBLE2, 
		status_word = `STATUS_WORD;

always @ (posedge vClk)
 begin
	pixel_in_d1 <= hscaler_en ? spixel_in : pixel_in;
	pixel_in_d2 <= pixel_in_d1;
	pixel_in_d3 <= pixel_in_d2;
	pixel_in_d4 <= pixel_in_d3;
	pixel_in_d5 <= pixel_in_d4;
	pixel_in_d6 <= pixel_in_d5;
	pixel_in_d7 <= pixel_in_d6;
	pixel_in_d8 <= pixel_in_d7;
	hblank_e_d1 <= hblank_e;
	hblank_e_d2 <= hblank_e_d1;
	hblank_e_d3 <= hblank_e_d2;
	hblank_e_d4 <= hblank_e_d3;
	hblank_e_d5 <= hblank_e_d4;
	hblank_e_d6 <= hblank_e_d5;
	hblank_e_d7 <= hblank_e_d6;
	vblank_d1 <= vblank;
	vblank_d2 <= vblank_d1;
	vblank_d3 <= vblank_d2;
	dsp_en_d1 <= dsp_en;
	dsp_en_d2 <= dsp_en_d1;
	dsp_en_d3 <= dsp_en_d2;
	dsp_en_d4 <= dsp_en_d3;
	dsp_en_d5 <= dsp_en_d4;
	dsp_en_d6 <= dsp_en_d5;
	dsp_en_d7 <= dsp_en_d6;
	dsp_en_d8 <= dsp_en_d7;
	dsp_en_d9 <= dsp_en_d8;
	dsp_en_d10 <= dsp_en_d9;
	hdsp_en_d1 <= hdsp_en;
	hdsp_en_d2 <= hdsp_en_d1;
	hdsp_en_d3 <= hdsp_en_d2;
	hdsp_en_d4 <= hdsp_en_d3;
	hdsp_en_d5 <= hdsp_en_d4;
	hdsp_en_d6 <= hdsp_en_d5;
	hdsp_en_d7 <= hdsp_en_d6;
	hdsp_en_d8 <= hdsp_en_d7;
	hdsp_en_d9 <= hdsp_en_d8;
	hdsp_en656_d1 <= hdsp_en656;
	hdsp_en656_d2 <= hdsp_en656_d1;
	hdsp_en656_d3 <= hdsp_en656_d2;
	hdsp_en656_d4 <= hdsp_en656_d3;
	hdsp_en656_d5 <= hdsp_en656_d4;
	hdsp_en656_d6 <= hdsp_en656_d5;
	hdsp_en656_d7 <= hdsp_en656_d6;
	hdsp_en656_d8 <= hdsp_en656_d7;
	hdsp_en656_d9 <= hdsp_en656_d8;
	hblank656_e_d1 <= hblank656_e;
	hblank656_e_d2 <= hblank656_e_d1;
	hblank656_e_d3 <= hblank656_e_d2;
	hblank656_e_d4 <= hblank656_e_d3;
	hblank656_e_d5 <= hblank656_e_d4;
	hblank656_e_d6 <= hblank656_e_d5;
	hblank656_e_d7 <= hblank656_e_d6;
	
	
	field_601_d1 <= field_601;
	field_601_d2 <= field_601_d1;
	field_601_d3 <= field_601_d2;
	field_601_d4 <= field_601_d3;
	field_601_d5 <= field_601_d4;
	field_601_d6 <= field_601_d5;
	flag_d1 <= flag;
	flag_d2 <= flag_d1;
	flag_d3 <= flag_d2;
	flag_d4 <= flag_d3;
	flag_d5 <= flag_d4;
	flag_d6 <= flag_d5;
	flag_d7 <= flag_d6;
	flag_d8 <= flag_d7;
	V_601_d1 <= V_601;
	V_601_d2 <= V_601_d1;
	V_601_d3 <= V_601_d2;
	V_601_d4 <= V_601_d3;
	V_601_d5 <= V_601_d4;
	V_601_d6 <= V_601_d5;
	hsync_out <= hpol ? flag_d8[4] : ~flag_d8[4];
	vsync_out <= vpol ? flag_d8[5] : ~flag_d8[5];
 end

// could use flag_d2[?]

//assign dsp_en_sel = vi_dbg ? dsp_en_d2 : dsp_en_d1;

// MAYBE NORMALIZE THIS AT VI_HSCALER
always @ (vi_dbg or dsp_en_d8 or dsp_en_d7)
 begin
	dsp_en_sel = vi_dbg ? dsp_en_d8 : dsp_en_d7;
 end

always @ (current_phase or dsp_en_sel)
 begin
	case (current_phase)
	 inactive :  		//00001

		if (dsp_en_sel)
			next_phase = luma_cb;
		else
			next_phase = inactive;
		
	 luma_cb : 		//00010
		next_phase = cb;
	 cb :  			//00100
		next_phase = luma_cr;
	 luma_cr :  		//01000
		next_phase = cr;
	 cr : 			//10000
		if (~dsp_en_sel)
			next_phase = inactive;
		else
			next_phase = luma_cb;
	 default:
		next_phase = inactive;
	endcase
 end

//assign vphase = current_phase[1] || current_phase[3];
//assign luma = current_phase[1] || current_phase[3];

assign vphase = vphase_cnt[1];
assign luma = (vphase_cnt== 2'b00) || (vphase_cnt==2'b10);

//assign vsyncb_detected = ~vsyncb && flag_d1[5];

// note this works only because there is a three clock delay through
// vi pixel pipe
always @ (posedge vClk)
 begin

/*
	if ((rst_phase_cnt) || (~viResetb))
	 begin
		if ((fld_cnt== 2'b00) || (nintl))
			if (hbe_odd)
				vphase_cnt <= 2'b10;
			else
				vphase_cnt <= 2'b00;
		else if (fld_cnt == 2'b01)
			if (hbe_odd ^ hlw_odd)
				vphase_cnt <= 2'b10;
			else
				vphase_cnt <= 2'b00;
		else if (fld_cnt == 2'b10)
			if (hbe_odd)
				vphase_cnt <= 2'b10;
			else
				vphase_cnt <= 2'b00;
		else if (fld_cnt == 2'b11)
			if (hbe_odd ^ hlw_odd)
				vphase_cnt <= 2'b10;
			else
				vphase_cnt <= 2'b00;
	 end
	else
		vphase_cnt <= vphase_cnt +1;
*/

	if ((rst_phase_cnt) || (~viResetb))
		vphase_cnt <= 2'b00;
	else
		vphase_cnt <= vphase_cnt +1;


	luma_d1 <= luma;
	luma_d2 <= luma_d1;
	luma_d3 <= luma_d2;
	luma_d4 <= luma_d3;
	luma_d5 <= luma_d4;
	luma_d6 <= luma_d5;
	luma_d7 <= luma_d6;
	luma_d8 <= luma_d7;
	vphase_d1 <= vphase;
	vphase_d2 <= vphase_d1;
 end




always @ (posedge vClk)
 begin
	if (~viResetb)
		current_phase <= inactive;
	else
		current_phase <= next_phase;

	// dsp_en_d6 because we are using hbe[9:1] and if odd, we get 1 extra pixel unless we cut off early
//		if ((~hbe_odd && dsp_en_d8) || (hbe_odd && dsp_en_d10 && dsp_en_d6))
		if (dsp_en_d8)
			vdata <= hscaler_en ? spixel_in : pixel_in_d6;			
		else if (luma_d8)
			vdata <= 8'b0;
		else
			vdata <= flag_d8;
		
		vphase_out_e <= vphase;



 end

assign out_data_e = vi_dbg ? data_out_601 : vdata;

always @ (posedge vClk)
 begin
	out_data <= out_data_e;
	vphase_out <= vphase_out_e;
 end


// hblank has to be in line with status word
//assign H = hblank_e_d1;
assign V = V_601_d6;
assign F = field_601_d6; 


always @ (hdsp_en_d6 or hdsp_en656_d6 or brdr_en)
 begin
	if (brdr_en)
		H = ~hdsp_en656_d6;
	else
		H = ~hdsp_en_d6;
 end

// even though F & V are one clock delayed from 
// H, this should work since the 3 preamble states
// should allow the F&V to be "caught up" before
// they are captured

assign P0 = F ^ V ^ H;
assign P1 = F ^ V;
assign P2 = F ^ H;
assign P3 = V ^ H;

// using the fact that this is one clk delayed


always @ (current_phase or pixel_in_d2 or blank_pixel or pixel_in_d8 or hscaler_en or spixel_in or pixel_in_d6)
	case (current_phase)
	 luma_cb:
	// cb for 656
		data_in_601 = hscaler_en ? spixel_in : pixel_in_d6;
	 cb:
	// luma for 656
		data_in_601 =  hscaler_en ? pixel_in_d2 : pixel_in_d8;
	 luma_cr:
	// cr for 656
		data_in_601 = hscaler_en ? spixel_in : pixel_in_d6;
	 cr:
	// luma for 656
		data_in_601 =  hscaler_en ? pixel_in_d2 : pixel_in_d8;
	 inactive:
		data_in_601 = blank_pixel;
	 default:
		data_in_601 = hscaler_en ? spixel_in : pixel_in_d6;
	endcase


/*
always @ (current_phase or pixel_in_d2 or blank_pixel or pixel_in)
	case (current_phase)
	 luma_cb:
	  begin
	// cb for 656
		data_in_601 = hscaler_en ? spixel_in : pixel_in;
	 cb:
	// luma for 656
		data_in_601 = pixel_in_d2;
	 luma_cr:
	// cr for 656
		data_in_601 = hscaler_en ? spixel_in : pixel_in;
	 cr:
	// luma for 656
		data_in_601 = pixel_in_d2;
	 inactive:
		data_in_601 = blank_pixel;
	 default:
		data_in_601 = hscaler_en ? spixel_in : pixel_in;
	endcase
*/


always @ (posedge vClk)
	if (hdsp_en_detected || (~viResetb))
		toggle <= 1'b0;
	else
		toggle <= (~toggle);

//assign blank_pixel = toggle ? 8'h80 : 8'h10;
assign blank_pixel = toggle ? 8'h10 : 8'h80;

always @ (hdsp_en_d8 or hdsp_en_d9 or hblank_e_d6 or hblank_e_d7 or brdr_en
	or hdsp_en656_d8 or hdsp_en656_d9 or hblank656_e_d6 or hblank656_e_d7 or
	hdsp_en656_d8 or hdsp_en656_d9)
 begin

	if (brdr_en)
         begin
			strt_hblank = ~hdsp_en656_d8 && hdsp_en656_d9;
			end_hblank = ~hblank656_e_d6 && hblank656_e_d7;
			hdsp_en_detected = hdsp_en656_d8 && ~hdsp_en656_d9;
	 end
	else
	 begin
                        strt_hblank = ~hdsp_en_d8 && hdsp_en_d9;
                        end_hblank = ~hblank_e_d6 && hblank_e_d7;
                        hdsp_en_detected = hdsp_en_d8 && ~hdsp_en_d9;
         end

 end

always @ (tim_ref_state or hblank_e or F or V or H or P3 or P2 or P1 or P0 or data_in_601 or strt_hblank or end_hblank) 
	case (tim_ref_state)
	active_pix : 
		begin
		if (strt_hblank || end_hblank)
			nxt_ref_state = preamble0;
		else
			nxt_ref_state = active_pix;
		data_out_601 = data_in_601;
		end
	preamble0:
		begin
		nxt_ref_state = preamble1;
		data_out_601 = 8'hFF;
		end
	preamble1:
		begin
		nxt_ref_state = preamble2;
		data_out_601 = 8'h00;
		end
	preamble2:
		begin
		data_out_601 = 8'h00;
		nxt_ref_state = status_word;
		end
	status_word:
		begin
		data_out_601 = {1'b1, F, V, H, P3, P2, P1, P0};
		nxt_ref_state = active_pix;
		end
/*
	inactive_pix:
		begin
		if (~hblank_e)
			nxt_ref_state = preamble0;
		else
			nxt_ref_state = inactive_pix;
		data_out_601 = data_in_601;
	 	end
*/
	default:
		begin
		data_out_601 = 8'hFF;
		nxt_ref_state = active_pix;
		end
	endcase

always @ (posedge vClk)
	if ((~viResetb) || vi_reset)
		tim_ref_state <= active_pix;
	else
		tim_ref_state <= nxt_ref_state;

		
	
endmodule


		
