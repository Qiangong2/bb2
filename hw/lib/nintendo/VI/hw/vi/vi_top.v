///////////////////////////////////////////////////////////////////////////////
//
//   (C) 1999 ARTX INC..  ALL RIGHTS RESERVED.  UNPUBLISHED -- RIGHTS
//   RESERVED UNDER THE COPYRIGHT LAWS OF THE UNITED STATES.  USE OF A
//   COPYRIGHT NOTICE IS PRECAUTIONARY ONLY AND DOES NOT IMPLY PUBLICATION
//   OR DISCLOSURE.
//
//   THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF
//   ARTX INC..  USE, DISCLOSURE, OR REPRODUCTION IS PROHIBITED WITHOUT
//   THE PRIOR EXPRESS WRITTEN PERMISSION OF ARTX INC..
//
//                   RESTRICTED RIGHTS LEGEND
//
//   Use, duplication, or disclosure by the Government is subject to
//   restrictions as set forth in subparagraph (c)(1)(ii) of the Rights
//   in Technical Data and Computer Software clause at DFARS 252.227-7013
//   or subparagraphs (c)(1) and (2) of Commercial Computer Software --
//   Restricted Rights at 48 CFR 52.227-19, as applicable.
//
//   ArtX Inc.
//   3400 Hillview Ave, Bldg 5
//   Palo Alto, CA 94304
//
///////////////////////////////////////////////////////////////////////////////

// Todo:
// - resetb, synchronous?
// - /*AUTOARG*/ ???
// - memory port
// - memory map
// - registers
// Need to synchronize vi_reset & vi_enable

// *************************
// Includes 
// *************************
`include "vi_state.v"
`include "vi_reg.v"
`timescale 1 ns / 10 ps

module vi (
    // Outputs
    vi_piAck, vi_piData, vi_piVrtIntr,
    vi_memReq, vi_memAddr,
    vi_ioVsync, vi_ioHsync,
    vicr_topad, vid_topad,
    // Inputs
    viResetb, pi_viClk, pi_viReq, pi_viRd, pi_viAddr, pi_viData,
    mem_viAck, mem_viData,
    ioGun_trg,
    clk27,
    viclksel,
    visel_tocore
    );

    input          viResetb;
    input          pi_viClk;

    input          pi_viReq;       // Processor register transaction start
    input          pi_viRd;        // When asserted, processor transaction is a read
    output         vi_piAck;       // Processor register transaction complete
    input    [8:1] pi_viAddr;      // Register address
    input   [15:0] pi_viData;      // Write data, bound for register
    output  [15:0] vi_piData;      // Read data, from register
    output         vi_piVrtIntr;   // Vertical Interrupt

    output         vi_memReq;      // Assert to enqueue a memory request
    output  [25:5] vi_memAddr;     // Cache line aligned addr, valid when memReq asserted
    input          mem_viAck;      // Asserted by memory controller to end transaction
    input   [63:0] mem_viData;     // Data from memory controller

    output          vi_ioVsync;    // Vertical sync
    output          vi_ioHsync;    // Horizontal sync

    input    [1:0] ioGun_trg;  // Light gun inputs from controller
	
    input 	clk27;	// Input to video crystal oscialltor
    output         vicr_topad;     // Distinguishes YCr phase from YCb phase
    output   [7:0] vid_topad;      // Video output data
    input    [1:0] visel_tocore;	// Status of DTV pin
    output	viclksel;	// VI Clk select (54 or 27)

	




wire 		viclksel;
wire [1:0]	visel_tocore;
wire [15:0] 	pi_viData;
wire [8:1]	pi_viAddr;
wire 		vi_reset_vid;
wire		vphase;
wire 		vClk;
wire [4:0]	bs4, bs1, bs2, bs3;
wire [10:0]	be4, be1, be2, be3;
wire 		hsyncb, vi_enable, vi_reset, vi_dbg, hdsp_en, vdsp_en;
wire [3:0]	equ;
wire [9:0]	acv;
wire [9:0]     	opsb, oprb, epsb, eprb;
wire 		bblnkb, fld, fld601, csyncb, vblank,
		EqVCnt0, EqVCnt1, EqVCnt2, EqVCnt3, hblank_e;
wire 		clr_brstb, csync_equ, csync_serr,
		dsp_en;		
wire [9:0]     	hlw;
wire [6:0]     	hce;
wire [6:0]     	hcs;
wire [6:0]     	hsy;
wire [9:0]     	hbe;
wire [9:0]     	hbs;
wire [9:0]     	hbe656;
wire [9:0]     	hbs656;
wire [10:0]    	hct_out, hct0, hct1, hct2, hct3, gun0_hct, gun1_hct;
wire [10:0]    	vct, vct0, vct1, vct2, vct3, gun0_vct, gun1_vct;
wire            vsyncb, hblank;
wire            vi_dlr;
wire [3:0]     	xoff;
wire 		high_addr;
wire [63:0]    	data_quad, data_quad_rgt;
wire [1:0]    	glogical_rptr, glogical_rptr_rgt;
wire          	allow_req, allow_req_rgt;
wire [7:0]   	data_out;
wire [23:0]    fbb_lft;          
wire [23:0]    fbb_rgt;         
wire [7:0]     std;               
wire [6:0]     wpl;               
wire [6:0]     wpl_pst;               
wire [2:0]     rd_ptr, rd_ptr_rgt;
wire 		gun0_htrg, gun1_htrg, gun0_vtrg, gun1_vtrg;
wire [1:0]	vi_gamma;
wire [1:0] 	g0md_pst, g1md_pst;		
wire [7:0]	out_data;
wire 		incr_line, incr_hline;
wire 		synced_gun_trig0, synced_gun_trig1;
wire [9:0]     T3_0, T3_1, T3_2, T3_3, T3_4, T3_5, T3_6, T3_7;
wire [9:0] 	T4_0;
wire [7:0]     T4_1, T4_2, T4_3, T4_4, T4_5, T4_6, T4_7, T5_0, T5_1,
               T5_2, T5_3, T5_4, T5_5, T5_6, T5_7, T24;
wire [8:0]	step;
wire [23:0] 	fbb_lft_bot, fbb_rgt_bot;
wire 		frst_pxl, lst_pxl;
//reg		tst_clk;
wire [7:0] 	pout;
wire [1:0] 	csel, uvshf, yshf; 
wire [1:0]      uvshf_rgt, yshf_rgt;
wire 		uvodd;
wire [2:0] 	psel;
wire 		hsync_out, vsync_out;
wire [1:0] 	fld_cnt;
wire 		req_pix;
wire		hscaler_en;
wire [31:0]	cur_double_lft, nxt_double_lft;
wire [31:0]	cur_double_rgt, nxt_double_rgt;
wire [7:0]	sy_out, scb_out, scr_out;
wire [7:0]	sy_out_rgt, scb_out_rgt, scr_out_rgt;
wire 		ipol, npol, kpol, bpol, hpol, vpol, fpol, cpol;
wire		V_601;
wire [1:0]	g0trgmd_pst, g1trgmd_pst;
wire [1:0]	vi_mode;
wire 		g0trgclr, g1trgclr;
wire		int0, int1, int2, int3;
wire		left;
wire 		viResetb_gfx, viResetb_vid;
wire 		incr_dbl_count_rgt, incr_dbl_count;
wire 		vi_nintl, vi_nintl_pst;
wire 		vsyncb_detected;
reg 		dsp_en_d1;
wire 		last_double;
wire		last_double_persistant;
wire 		hbe_odd;
wire [9:0] 	hbs_pst, hbe_pst;
wire [9:0]	vi_srcwidth;
wire 		hdsp_en656, dsp_en656, hblank656_e;
wire 		brdr_en;

/* VI/PI Interface
*/

assign 		vi_ioHsync = hsync_out;
assign 		vi_ioVsync = vsync_out;
assign 		vid_topad = out_data;
assign 		vicr_topad = vphase;
assign 		vClk = clk27;

/*
always
begin
 tst_clk = 1'b0;
 #18.52;
 tst_clk = 1'b1;
 #18.52;
end
*/

//module rst_sync (clk, rstinB, rstoutB);

rst_sync gfx_rst_sync(.clk(pi_viClk), .rstinB(viResetb), .rstoutB(viResetb_gfx));
rst_sync vid_rst_sync(.clk(vClk), .rstinB(viResetb), .rstoutB(viResetb_vid));

vi_pi vi_pi (
      	// Inputs to VI
		.viResetb_vid(viResetb_vid),
		.viResetb_gfx(viResetb_gfx),
		.gfxClk(pi_viClk),
		.vClk(vClk),
		.pi_viAddr(pi_viAddr),
		.pi_viRd(pi_viRd),
		.pi_viReq(pi_viReq),
		.pi_viData(pi_viData),
		.vsyncb_d1(vsyncb_d1),
		// Outputs to PI
		.vi_piAck(vi_piAck),
		.vi_piData(vi_piData),
		.vi_piVrtIntr(vi_piVrtIntr),
		.gun0_htrg(gun0_htrg), 
		.gun0_vtrg(gun0_vtrg), 
		.gun1_htrg(gun1_htrg),
		.gun1_vtrg(gun1_vtrg),
        	.gun0_vct(gun0_vct), 
	        .gun1_vct(gun1_vct),
        	.gun0_hct(gun0_hct), 
	        .gun1_hct(gun1_hct),		  
        	.vct(vct),
        	.int0(int0),
        	.int1(int1),
        	.int2(int2),
        	.int3(int3),
       		.hct(hct_out),
		.vi_enable(vi_enable),
		.vi_reset(vi_reset), 
		.vi_reset_vid(vi_reset_vid), 
		.vi_nintl(vi_nintl), 
		.vi_dlr(vi_dlr), 
		.vi_dbg(vi_dbg),
        	.g0md_pst(g0md_pst), 
		.g1md_pst(g1md_pst), 
        	.vi_mode(vi_mode), 
		.hlw(hlw), .hce(hce), .hcs(hcs), .hsy(hsy), 
		.hbe(hbe), .hbs(hbs), .equ(equ), .acv(acv),  
        	.oprb(oprb), .opsb(opsb), .eprb(eprb), .epsb(epsb), 
		.bs1(bs1), .be1(be1), .bs3(bs3), .be3(be3), 
		.bs2(bs2), .be2(be2), .bs4(bs4), .be4(be4), 
		.fbb_lft(fbb_lft), .xoff(xoff), .fbb_rgt(fbb_rgt), 
		.std(std), .wpl(wpl), .high_addr(high_addr),
		.hct0(hct0), .vct0(vct0), 
		.hct1(hct1), .vct1(vct1), 
		.hct2(hct2), .vct2(vct2), 
		.hct3(hct3), .vct3(vct3), 
		.g0trgclr(g0trgclr), .g1trgclr(g1trgclr),
		.fbb_lft_bot(fbb_lft_bot), .fbb_rgt_bot(fbb_rgt_bot),
		.T3_0(T3_0), .T3_1(T3_1), .T3_2(T3_2), .T3_3(T3_3), .T3_4(T3_4), .T3_5(T3_5), .T3_6(T3_6),
		.T3_7(T3_7), .T4_0(T4_0), .T4_1(T4_1), .T4_2(T4_2), .T4_3(T4_3), .T4_4(T4_4), .T4_5(T4_5),
		.T4_6(T4_6), .T4_7(T4_7), .T5_0(T5_0), .T5_1(T5_1), .T5_2(T5_2), .T5_3(T5_3), .T5_4(T5_4),
		.T5_5(T5_5), .T5_6(T5_6), .T5_7(T5_7), .T24(T24), .step(step), .hscaler_en(hscaler_en),
		.ipol(ipol), .npol(npol), .kpol(kpol), .bpol(bpol), .hpol(hpol), .vpol(vpol), 
		.fpol(fpol), .cpol(cpol), .i0enb(i0enb), .i1enb(i1enb), .i2enb(i2enb), .i3enb(i3enb),
		.i0clr(i0clr), .i1clr(i1clr), .i2clr(i2clr), .i3clr(i3clr),
		.visel_tocore(visel_tocore), .viclksel(viclksel),
		.vi_srcwidth(vi_srcwidth), .hbe656(hbe656), .hbs656(hbs656),
		.brdr_en(brdr_en)

      ); 


// double register asyncronous trigger signals from guns
sync gun0_sync (.D(ioGun_trg[0]), .CK(vClk), .Q(synced_gun_trig0));
sync gun1_sync (.D(ioGun_trg[1]), .CK(vClk), .Q(synced_gun_trig1));

always @ (posedge vClk)
 dsp_en_d1 <= dsp_en;

assign dsp_en = vdsp_en && hdsp_en;
assign dsp_en656 = vdsp_en && hdsp_en656;

vi_vercnt vi_vercnt (
        // Inputs from outside
        .viResetb(viResetb_vid),
	.vClk(vClk),
        // Inputs from PI interface
        .bs4(bs4), .be4(be4),.bs1(bs1), .be1(be1), .bs2(bs2), 
	.be2(be2), .bs3(bs3), .be3(be3), 
	.vi_enable(vi_enable), .vi_reset(vi_reset_vid), 
	.vct0(vct0), .vct1(vct1), .vct2(vct2), .vct3(vct3),
	.equ(equ), .acv(acv), .opsb(opsb), .epsb(epsb), 
	.eprb(eprb), .oprb(oprb), .vi_dbg(vi_dbg),
	.fld_cnt(fld_cnt), .rst_phase_cnt(rst_phase_cnt),
	.vi_nintl(vi_nintl),
	.vi_nintl_pst(vi_nintl_pst),
        .g0md_pst(g0md_pst), 
	.g1md_pst(g1md_pst),
        // Inputs from Horizontal Counters
        .incr_line(incr_line), 
        .incr_hline(incr_hline), 
	.hsyncb(hsyncb),
	.csync_equ(csync_equ), 
	.csync_serr(csync_serr),
	.eav(eav),
	// Inputs from guns
	.synced_gun_trig0(synced_gun_trig0),
	.synced_gun_trig1(synced_gun_trig1),
	// Gun Outputs to Horcnt
        .gun0_trg_edge(gun0_trg_edge), 
        .gun1_trg_edge(gun1_trg_edge), 
        // Outputs  (timing signals)
        .bblnkb(bblnkb), 
	.fld(fld),
	.fld601(fld601), 
	.vsyncb(vsyncb), 
	.vsyncb_d1(vsyncb_d1), 
 	.csyncb(csyncb),
	.vblank(vblank),
	.vblank_d1(vblank_d1),
	.vdsp_en(vdsp_en),
	.top_pic_sel(top_pic_sel),
	.V_601(V_601),
	.vsyncb_detected(vsyncb_detected),
        // Outputs to PI interface
	.gun0_vct(gun0_vct),
	.gun1_vct(gun1_vct),
	.gun0_vtrg(gun0_vtrg),
	.gun1_vtrg(gun1_vtrg),
	.g0trgclr(g0trgclr), .g1trgclr(g1trgclr),
        .vct(vct),
	.EqVCnt0(EqVCnt0),
	.EqVCnt1(EqVCnt1),
	.EqVCnt2(EqVCnt2),
	.EqVCnt3(EqVCnt3)
);


vi_horcnt vi_horcnt (
        // Inputs from outside
        .viResetb(viResetb_vid), 
	.vClk(vClk),
        // Inputs from PI interface
        .hsy(hsy), .hcs(hcs), .hce(hce), 
	.hbe(hbe), .hbs(hbs), .hlw(hlw),
	.hbe656(hbe656), .hbs656(hbs656),
	.vi_reset(vi_reset_vid), .vi_enable(vi_enable),
	.vi_nintl_pst(vi_nintl_pst),
	.g0md_pst(g0md_pst), .g1md_pst(g1md_pst),        
	// Inputs from guns
        .gun0_trg_edge(gun0_trg_edge), 
        .gun1_trg_edge(gun1_trg_edge), 
        // Output
        .hsyncb(hsyncb), 
	.clr_brstb(clr_brstb), 
	.hblank(hblank), 
	.vi_mode(vi_mode), 
	.hdsp_en(hdsp_en),
	.csync_equ(csync_equ), 
	.csync_serr(csync_serr),
        .incr_line(incr_line), 
        .incr_hline(incr_hline), 
	.hblank_e(hblank_e), .eav(eav),
        .hct_out(hct_out), 
	.vsyncb_detected(vsyncb_detected),
        .EqVCnt0(EqVCnt0),
        .EqVCnt1(EqVCnt1),
        .EqVCnt2(EqVCnt2),
        .EqVCnt3(EqVCnt3), 
	.int0(int0),
	.int1(int1),
	.int2(int2),
	.int3(int3),
	.gun0_hct(gun0_hct), .gun1_hct(gun1_hct),
	.hct0(hct0), .hct1(hct1), .hct2(hct2), .hct3(hct3),
	.g0trgclr(g0trgclr), .g1trgclr(g1trgclr),
	.gun0_htrg(gun0_htrg),
	.gun1_htrg(gun1_htrg),
	.i0enb(i0enb), .i1enb(i1enb), .i2enb(i2enb), .i3enb(i3enb),
        .i0clr(i0clr), .i1clr(i1clr), .i2clr(i2clr), .i3clr(i3clr),
	.dtv(viclksel), .hbe_odd(hbe_odd), .hbs_pst(hbs_pst), .hbe_pst(hbe_pst),
	.hdsp_en656(hdsp_en656), .hblank656_e(hblank656_e)
 );

vi_mem_read vi_mem_read (
        // Inputs from outside
        .vClk(vClk), 
	.viResetb(viResetb_vid),
        // Inputs from timing generators
        .hsyncb(hsyncb), 
	.hblank(hblank), .dsp_en(dsp_en),
	.vdsp_en(vdsp_en),
	.vblank(vblank),
	.vsyncb_detected(vsyncb_detected),
        // Inputs from config regs
        .dlr(vi_dlr), .xoff(xoff), .wpl(wpl), 
	.vi_reset(vi_reset_vid), .vi_enable(vi_enable),
	.hscaler_en(hscaler_en), .vi_srcwidth(vi_srcwidth),
        // Inputs from Mem Write   
        .data_quad(data_quad), 
	.data_quad_rgt(data_quad_rgt),
	// Inputs from Hscaler
	.incr_dbl_count(incr_dbl_count),
	.incr_dbl_count_rgt(incr_dbl_count_rgt),
	// Timing inputs
	.hbs_pst(hbs_pst), .hbe_pst(hbe_pst),
	.hlw(hlw),
        // Outputs to Mem Write
	.rd_ptr(rd_ptr), .rd_ptr_rgt(rd_ptr_rgt),
        .glogical_rptr(glogical_rptr),
        .glogical_rptr_rgt(glogical_rptr_rgt), 
        .allow_req(allow_req), 
	.allow_req_rgt(allow_req_rgt),
	.wpl_pst(wpl_pst),
        // Outputs to data path
        .data_out(data_out),
	.frst_pxl(frst_pxl), .lst_pxl(lst_pxl), .hbe_odd(hbe_odd),
	.hscaler_en_pst(hscaler_en_pst),
	// Outputs to Hscaler
	.cur_double_lft(cur_double_lft),
        .nxt_double_lft(nxt_double_lft),
	.cur_double_rgt(cur_double_rgt),
        .nxt_double_rgt(nxt_double_rgt),
	.last_double(last_double),
	// Input from hscaler
	.toggle_detected(toggle_detected), 
	.last_double_persistant(last_double_persistant));


vi_mem_write vi_mem_write (
        // Inputs from outside
        .viResetb_gfx(viResetb_gfx),
	.gfxClk(pi_viClk), 
	.vClk(vClk),
        // Inputs from Mem
        .mem_vi_ack(mem_viAck), 
	.mem_vi_data(mem_viData),
        // Inputs from config regs
        .fbb_lft(fbb_lft), .high_addr(high_addr),
	.fbb_rgt(fbb_rgt), .std(std), 
	.fbb_lft_bot(fbb_lft_bot), .fbb_rgt_bot(fbb_rgt_bot),
	.dlr(vi_dlr), .vi_reset(vi_reset),
	.vi_nintl(vi_nintl),
        // Inputs from timing
        .vsyncb_d1(vsyncb_d1), .hblank(hblank), 
	.vblank_d1(vblank_d1),
	.dsp_en_d1(dsp_en_d1), .field(fld),
	.top_pic_sel(top_pic_sel), .hsyncb(hsyncb),
	.wpl_pst(wpl_pst),
        // Inputs from Read side
        .rd_ptr(rd_ptr), .rd_ptr_rgt(rd_ptr_rgt), 
	.allow_req(allow_req), .allow_req_rgt(allow_req_rgt),
        .glogical_rptr(glogical_rptr), 
	.glogical_rptr_rgt(glogical_rptr_rgt),
        // Outputs to Read Side
        .data_quad(data_quad),
        .data_quad_rgt(data_quad_rgt),
        // Outputs to Mem
        .vi_mem_addr(vi_memAddr), .vi_mem_req(vi_memReq)
        );


vi_rohm vi_rohm (
	// Inputs from outside
	.vClk(vClk), 
	.viResetb(viResetb_vid),
	// Inputs (timing signals)
        .csyncb(csyncb), .field(fld),
	.field_601(fld601), .fld_cnt(fld_cnt),
	.vsyncb(vsyncb), .hsyncb(hsyncb), 
	.hdsp_en656(hdsp_en656), .dsp_en656(dsp_en656), .hblank656_e(hblank656_e),
	.clr_brstb(clr_brstb), .brst_blk(bblnkb), .dsp_en(dsp_en), 
	.hblank_e(hblank_e), .vblank(vblank), .hdsp_en(hdsp_en),
	.vi_reset(vi_reset_vid), .hscaler_en(hscaler_en_pst),
	.rst_phase_cnt(rst_phase_cnt), .incr_line(incr_line),
	.V_601(V_601),
	.spixel_in(pout),
	// Inputs from config regs
	.vi_mode(vi_mode), .nintl(vi_nintl_pst), .vi_dbg(vi_dbg),
	.ipol(ipol), .npol(npol), .kpol(kpol), .bpol(bpol), .hpol(hpol), .vpol(vpol), 
         .fpol(fpol), .cpol(cpol), .hbe_odd(hbe[0]), .hlw_odd(hlw[0]), .dtv(viclksel),
	.brdr_en(brdr_en),
	// Inputs from pixel pipe
        .pixel_in(data_out), 
	// Outputs 
	.out_data(out_data), .vphase_out(vphase),
	.hsync_out(hsync_out), .vsync_out(vsync_out)
	);

 
vi_hscaler vi_hscaler (
	.vClk(vClk), .viResetb(viResetb_vid), .dsp_en(dsp_en), .lst_pxl(lst_pxl), 
	.frst_pxl(frst_pxl), .csel(csel), .psel(psel), .uvodd(uvodd), .vblank(vblank),
	.uvshf(uvshf), .yshf(yshf), .vi_reset(vi_reset_vid), .step(step),
	.incr_dbl_count(incr_dbl_count), .sy_out(sy_out),
	.scb_out(scb_out), .scr_out(scr_out), .cur_double_lft(cur_double_lft), .hsyncb(hsyncb),
	.nxt_double_lft(nxt_double_lft), .cur_double_rgt(cur_double_rgt), .nxt_double_rgt(nxt_double_rgt),
	.vi_dlr(vi_dlr), .sy_out_rgt(sy_out_rgt), .scb_out_rgt(scb_out_rgt), 
	.scr_out_rgt(scr_out_rgt), .left(left), .uvshf_rgt(uvshf_rgt), .yshf_rgt(yshf_rgt),
	.incr_dbl_count_rgt(incr_dbl_count_rgt), .last_double(last_double),
	.toggle_detected(toggle_detected), .last_double_persistant(last_double_persistant));


vi_hf vi_hf (
	 .csel(csel), .left(left), .psel(psel), .ypxel(sy_out), .upxel(scb_out), .vpxel(scr_out),
	.uvodd(uvodd), .uvshf(uvshf), .ypxel_rgt(sy_out_rgt), .upxel_rgt(scb_out_rgt), .vpxel_rgt(scr_out_rgt),
	.yshf(yshf), .pout(pout), .reg_t00(T3_0), .reg_t01(T3_1), .reg_t02(T3_2), .reg_t03(T3_3),
.reg_t04(T3_4), .reg_t05(T3_5), .reg_t06(T3_6), .reg_t07(T3_7), .reg_t08(T4_0), .reg_t09(T4_1),
.reg_t10(T4_2), .reg_t11(T4_3), .reg_t12(T4_4), .reg_t13(T4_5), .reg_t14(T4_6), .reg_t15(T4_7),
.reg_t16(T5_0), .reg_t17(T5_1), .reg_t18(T5_2), .reg_t19(T5_3), .reg_t20(T5_4), .reg_t21(T5_5),
.reg_t22(T5_6), .reg_t23(T5_7), .CLK(vClk), .uvshf_rgt(uvshf_rgt), .yshf_rgt(yshf_rgt)
);




endmodule  // vi
