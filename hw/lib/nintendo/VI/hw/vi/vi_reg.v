/*
 *  vi_reg.v
 *
 *  NOTE: This is a generated file.  DO NOT HAND EDIT.
 *
 *  Generated from vi_reg.fdl
 */


/*
 *  vi_hostreg value
 */
`define VI_VER_TIM	8'h00
`define VI_DSP_CFG	8'h01
`define VI_HOR_TIM0_U	8'h02
`define VI_HOR_TIM0_L	8'h03
`define VI_HOR_TIM1_U	8'h04
`define VI_HOR_TIM1_L	8'h05
`define VI_VER_ODD_TIM_U	8'h06
`define VI_VER_ODD_TIM_L	8'h07
`define VI_VER_EVN_TIM_U	8'h08
`define VI_VER_EVN_TIM_L	8'h09
`define VI_ODD_BBLNK_INTVL_U	8'h0a
`define VI_ODD_BBLNK_INTVL_L	8'h0b
`define VI_EVN_BBLNK_INTVL_U	8'h0c
`define VI_EVN_BBLNK_INTVL_L	8'h0d
`define VI_PIC_BASE_LFT_U	8'h0e
`define VI_PIC_BASE_LFT_L	8'h0f
`define VI_PIC_BASE_RGT_U	8'h10
`define VI_PIC_BASE_RGT_L	8'h11
`define VI_PIC_BASE_LFT_BOT_U	8'h12
`define VI_PIC_BASE_LFT_BOT_L	8'h13
`define VI_PIC_BASE_RGT_BOT_U	8'h14
`define VI_PIC_BASE_RGT_BOT_L	8'h15
`define VI_DSP_POS_U	8'h16
`define VI_DSP_POS_L	8'h17
`define VI_DSP_INT0_U	8'h18
`define VI_DSP_INT0_L	8'h19
`define VI_DSP_INT1_U	8'h1a
`define VI_DSP_INT1_L	8'h1b
`define VI_DSP_INT2_U	8'h1c
`define VI_DSP_INT2_L	8'h1d
`define VI_DSP_INT3_U	8'h1e
`define VI_DSP_INT3_L	8'h1f
`define VI_DSP_LATCH0_U	8'h20
`define VI_DSP_LATCH0_L	8'h21
`define VI_DSP_LATCH1_U	8'h22
`define VI_DSP_LATCH1_L	8'h23
`define VI_PIC_CFG	8'h24
`define VI_HSCALE	8'h25
`define VI_FLTR_COEFF0_U	8'h26
`define VI_FLTR_COEFF0_L	8'h27
`define VI_FLTR_COEFF1_U	8'h28
`define VI_FLTR_COEFF1_L	8'h29
`define VI_FLTR_COEFF2_U	8'h2a
`define VI_FLTR_COEFF2_L	8'h2b
`define VI_FLTR_COEFF3_U	8'h2c
`define VI_FLTR_COEFF3_L	8'h2d
`define VI_FLTR_COEFF4_U	8'h2e
`define VI_FLTR_COEFF4_L	8'h2f
`define VI_FLTR_COEFF5_U	8'h30
`define VI_FLTR_COEFF5_L	8'h31
`define VI_FLTR_COEFF6_U	8'h32
`define VI_FLTR_COEFF6_L	8'h33
`define VI_OUTPOL	8'h35
`define VI_CLKSEL	8'h36
`define VI_DTVSTATUS	8'h37
`define VI_WIDTH	8'h38
`define VI_HBE656	8'h39
`define VI_HBS656	8'h3a

/*
 *  vi_hbe656_reg struct
 */
`define VI_HBE656_REG_HBE656	9:0
`define VI_HBE656_REG_PAD0	14:10
`define VI_HBE656_REG_BRDR_EN	15:15
`define VI_HBE656_REG_TOTAL_SIZE	32'd16
`define VI_HBE656_REG_TOTAL_RANGE	15:0

/*
 *  vi_hbs656_reg struct
 */
`define VI_HBS656_REG_HBS656	9:0
`define VI_HBS656_REG_TOTAL_SIZE	32'd10
`define VI_HBS656_REG_TOTAL_RANGE	9:0

/*
 *  vi_width_reg struct
 */
`define VI_WIDTH_REG_SRCWIDTH	9:0
`define VI_WIDTH_REG_TOTAL_SIZE	32'd10
`define VI_WIDTH_REG_TOTAL_RANGE	9:0

/*
 *  vi_outpol_reg struct
 */
`define VI_OUTPOL_REG_IPOL	0:0
`define VI_OUTPOL_REG_NPOL	1:1
`define VI_OUTPOL_REG_KPOL	2:2
`define VI_OUTPOL_REG_BPOL	3:3
`define VI_OUTPOL_REG_HPOL	4:4
`define VI_OUTPOL_REG_VPOL	5:5
`define VI_OUTPOL_REG_FPOL	6:6
`define VI_OUTPOL_REG_CPOL	7:7
`define VI_OUTPOL_REG_TOTAL_SIZE	32'd8
`define VI_OUTPOL_REG_TOTAL_RANGE	7:0

/*
 *  vi_dsp_cfg_reg struct
 */
`define VI_DSP_CFG_REG_ENB	0:0
`define VI_DSP_CFG_REG_RST	1:1
`define VI_DSP_CFG_REG_INT	2:2
`define VI_DSP_CFG_REG_DLR	3:3
`define VI_DSP_CFG_REG_LE0	5:4
`define VI_DSP_CFG_REG_LE1	7:6
`define VI_DSP_CFG_REG_MODE	9:8
`define VI_DSP_CFG_REG_TOTAL_SIZE	32'd10
`define VI_DSP_CFG_REG_TOTAL_RANGE	9:0

/*
 *  vi_hor_tim0_l_reg struct
 */
`define VI_HOR_TIM0_L_REG_HLW	9:0
`define VI_HOR_TIM0_L_REG_TOTAL_SIZE	32'd10
`define VI_HOR_TIM0_L_REG_TOTAL_RANGE	9:0

/*
 *  vi_hor_tim0_u_reg struct
 */
`define VI_HOR_TIM0_U_REG_HCE	6:0
`define VI_HOR_TIM0_U_REG_PAD0	7:7
`define VI_HOR_TIM0_U_REG_HCS	14:8
`define VI_HOR_TIM0_U_REG_TOTAL_SIZE	32'd15
`define VI_HOR_TIM0_U_REG_TOTAL_RANGE	14:0

/*
 *  vi_hor_tim1_l_reg struct
 */
`define VI_HOR_TIM1_L_REG_HSY	6:0
`define VI_HOR_TIM1_L_REG_HBE_L	15:7
`define VI_HOR_TIM1_L_REG_TOTAL_SIZE	32'd16
`define VI_HOR_TIM1_L_REG_TOTAL_RANGE	15:0

/*
 *  vi_hor_tim1_u_reg struct
 */
`define VI_HOR_TIM1_U_REG_HBE_U	0:0
`define VI_HOR_TIM1_U_REG_HBS	10:1
`define VI_HOR_TIM1_U_REG_TOTAL_SIZE	32'd11
`define VI_HOR_TIM1_U_REG_TOTAL_RANGE	10:0

/*
 *  vi_ver_tim_reg struct
 */
`define VI_VER_TIM_REG_EQU	3:0
`define VI_VER_TIM_REG_ACV	13:4
`define VI_VER_TIM_REG_TOTAL_SIZE	32'd14
`define VI_VER_TIM_REG_TOTAL_RANGE	13:0

/*
 *  vi_ver_odd_tim_l_reg struct
 */
`define VI_VER_ODD_TIM_L_REG_OPRB	9:0
`define VI_VER_ODD_TIM_L_REG_TOTAL_SIZE	32'd10
`define VI_VER_ODD_TIM_L_REG_TOTAL_RANGE	9:0

/*
 *  vi_ver_odd_tim_u_reg struct
 */
`define VI_VER_ODD_TIM_U_REG_OPSB	9:0
`define VI_VER_ODD_TIM_U_REG_TOTAL_SIZE	32'd10
`define VI_VER_ODD_TIM_U_REG_TOTAL_RANGE	9:0

/*
 *  vi_ver_evn_tim_l_reg struct
 */
`define VI_VER_EVN_TIM_L_REG_EPRB	9:0
`define VI_VER_EVN_TIM_L_REG_TOTAL_SIZE	32'd10
`define VI_VER_EVN_TIM_L_REG_TOTAL_RANGE	9:0

/*
 *  vi_ver_evn_tim_u_reg struct
 */
`define VI_VER_EVN_TIM_U_REG_EPSB	9:0
`define VI_VER_EVN_TIM_U_REG_TOTAL_SIZE	32'd10
`define VI_VER_EVN_TIM_U_REG_TOTAL_RANGE	9:0

/*
 *  vi_odd_bblnk_intvl_l_reg struct
 */
`define VI_ODD_BBLNK_INTVL_L_REG_BS1	4:0
`define VI_ODD_BBLNK_INTVL_L_REG_BE1	15:5
`define VI_ODD_BBLNK_INTVL_L_REG_TOTAL_SIZE	32'd16
`define VI_ODD_BBLNK_INTVL_L_REG_TOTAL_RANGE	15:0

/*
 *  vi_odd_bblnk_intvl_u_reg struct
 */
`define VI_ODD_BBLNK_INTVL_U_REG_BS3	4:0
`define VI_ODD_BBLNK_INTVL_U_REG_BE3	15:5
`define VI_ODD_BBLNK_INTVL_U_REG_TOTAL_SIZE	32'd16
`define VI_ODD_BBLNK_INTVL_U_REG_TOTAL_RANGE	15:0

/*
 *  vi_evn_bblnk_intvl_l_reg struct
 */
`define VI_EVN_BBLNK_INTVL_L_REG_BS2	4:0
`define VI_EVN_BBLNK_INTVL_L_REG_BE2	15:5
`define VI_EVN_BBLNK_INTVL_L_REG_TOTAL_SIZE	32'd16
`define VI_EVN_BBLNK_INTVL_L_REG_TOTAL_RANGE	15:0

/*
 *  vi_evn_bblnk_intvl_u_reg struct
 */
`define VI_EVN_BBLNK_INTVL_U_REG_BS4	4:0
`define VI_EVN_BBLNK_INTVL_U_REG_BE4	15:5
`define VI_EVN_BBLNK_INTVL_U_REG_TOTAL_SIZE	32'd16
`define VI_EVN_BBLNK_INTVL_U_REG_TOTAL_RANGE	15:0

/*
 *  vi_pic_base_lft_l_reg struct
 */
`define VI_PIC_BASE_LFT_L_REG_FBB_LFT_L	15:0
`define VI_PIC_BASE_LFT_L_REG_TOTAL_SIZE	32'd16
`define VI_PIC_BASE_LFT_L_REG_TOTAL_RANGE	15:0

/*
 *  vi_pic_base_lft_u_reg struct
 */
`define VI_PIC_BASE_LFT_U_REG_FBB_LFT_U	7:0
`define VI_PIC_BASE_LFT_U_REG_XOF	11:8
`define VI_PIC_BASE_LFT_U_REG_HIGH_ADDR	12:12
`define VI_PIC_BASE_LFT_U_REG_TOTAL_SIZE	32'd13
`define VI_PIC_BASE_LFT_U_REG_TOTAL_RANGE	12:0

/*
 *  vi_pic_base_rgt_l_reg struct
 */
`define VI_PIC_BASE_RGT_L_REG_FBB_RGT_L	15:0
`define VI_PIC_BASE_RGT_L_REG_TOTAL_SIZE	32'd16
`define VI_PIC_BASE_RGT_L_REG_TOTAL_RANGE	15:0

/*
 *  vi_pic_base_rgt_u_reg struct
 */
`define VI_PIC_BASE_RGT_U_REG_FBB_RGT_U	7:0
`define VI_PIC_BASE_RGT_U_REG_TOTAL_SIZE	32'd8
`define VI_PIC_BASE_RGT_U_REG_TOTAL_RANGE	7:0

/*
 *  vi_pic_base_lft_bot_l_reg struct
 */
`define VI_PIC_BASE_LFT_BOT_L_REG_FBB_LFT_BOT_L	15:0
`define VI_PIC_BASE_LFT_BOT_L_REG_TOTAL_SIZE	32'd16
`define VI_PIC_BASE_LFT_BOT_L_REG_TOTAL_RANGE	15:0

/*
 *  vi_pic_base_lft_bot_u_reg struct
 */
`define VI_PIC_BASE_LFT_BOT_U_REG_FBB_LFT_BOT_U	7:0
`define VI_PIC_BASE_LFT_BOT_U_REG_TOTAL_SIZE	32'd8
`define VI_PIC_BASE_LFT_BOT_U_REG_TOTAL_RANGE	7:0

/*
 *  vi_pic_base_rgt_bot_l_reg struct
 */
`define VI_PIC_BASE_RGT_BOT_L_REG_FBB_RGT_BOT_L	15:0
`define VI_PIC_BASE_RGT_BOT_L_REG_TOTAL_SIZE	32'd16
`define VI_PIC_BASE_RGT_BOT_L_REG_TOTAL_RANGE	15:0

/*
 *  vi_pic_base_rgt_bot_u_reg struct
 */
`define VI_PIC_BASE_RGT_BOT_U_REG_FBB_RGT_BOT_U	7:0
`define VI_PIC_BASE_RGT_BOT_U_REG_TOTAL_SIZE	32'd8
`define VI_PIC_BASE_RGT_BOT_U_REG_TOTAL_RANGE	7:0

/*
 *  vi_pic_cfg_reg struct
 */
`define VI_PIC_CFG_REG_STD	7:0
`define VI_PIC_CFG_REG_WPL	14:8
`define VI_PIC_CFG_REG_TOTAL_SIZE	32'd15
`define VI_PIC_CFG_REG_TOTAL_RANGE	14:0

/*
 *  vi_dsp_pos_l_reg struct
 */
`define VI_DSP_POS_L_REG_HCT	10:0
`define VI_DSP_POS_L_REG_TOTAL_SIZE	32'd11
`define VI_DSP_POS_L_REG_TOTAL_RANGE	10:0

/*
 *  vi_dsp_pos_u_reg struct
 */
`define VI_DSP_POS_U_REG_VCT	10:0
`define VI_DSP_POS_U_REG_TOTAL_SIZE	32'd11
`define VI_DSP_POS_U_REG_TOTAL_RANGE	10:0

/*
 *  vi_dsp_int0_l_reg struct
 */
`define VI_DSP_INT0_L_REG_HCT0	10:0
`define VI_DSP_INT0_L_REG_TOTAL_SIZE	32'd11
`define VI_DSP_INT0_L_REG_TOTAL_RANGE	10:0

/*
 *  vi_dsp_int0_u_reg struct
 */
`define VI_DSP_INT0_U_REG_VCT0	10:0
`define VI_DSP_INT0_U_REG_PAD0	11:11
`define VI_DSP_INT0_U_REG_ENB0	12:12
`define VI_DSP_INT0_U_REG_PAD1	14:13
`define VI_DSP_INT0_U_REG_INT0	15:15
`define VI_DSP_INT0_U_REG_TOTAL_SIZE	32'd16
`define VI_DSP_INT0_U_REG_TOTAL_RANGE	15:0

/*
 *  vi_dsp_int1_l_reg struct
 */
`define VI_DSP_INT1_L_REG_HCT1	10:0
`define VI_DSP_INT1_L_REG_TOTAL_SIZE	32'd11
`define VI_DSP_INT1_L_REG_TOTAL_RANGE	10:0

/*
 *  vi_dsp_int1_u_reg struct
 */
`define VI_DSP_INT1_U_REG_VCT1	10:0
`define VI_DSP_INT1_U_REG_PAD0	11:11
`define VI_DSP_INT1_U_REG_ENB1	12:12
`define VI_DSP_INT1_U_REG_PAD1	14:13
`define VI_DSP_INT1_U_REG_INT1	15:15
`define VI_DSP_INT1_U_REG_TOTAL_SIZE	32'd16
`define VI_DSP_INT1_U_REG_TOTAL_RANGE	15:0

/*
 *  vi_dsp_int2_l_reg struct
 */
`define VI_DSP_INT2_L_REG_HCT2	10:0
`define VI_DSP_INT2_L_REG_TOTAL_SIZE	32'd11
`define VI_DSP_INT2_L_REG_TOTAL_RANGE	10:0

/*
 *  vi_dsp_int2_u_reg struct
 */
`define VI_DSP_INT2_U_REG_VCT2	10:0
`define VI_DSP_INT2_U_REG_PAD0	11:11
`define VI_DSP_INT2_U_REG_ENB2	12:12
`define VI_DSP_INT2_U_REG_PAD1	14:13
`define VI_DSP_INT2_U_REG_INT2	15:15
`define VI_DSP_INT2_U_REG_TOTAL_SIZE	32'd16
`define VI_DSP_INT2_U_REG_TOTAL_RANGE	15:0

/*
 *  vi_dsp_int3_l_reg struct
 */
`define VI_DSP_INT3_L_REG_HCT3	10:0
`define VI_DSP_INT3_L_REG_TOTAL_SIZE	32'd11
`define VI_DSP_INT3_L_REG_TOTAL_RANGE	10:0

/*
 *  vi_dsp_int3_u_reg struct
 */
`define VI_DSP_INT3_U_REG_VCT3	10:0
`define VI_DSP_INT3_U_REG_PAD0	11:11
`define VI_DSP_INT3_U_REG_ENB3	12:12
`define VI_DSP_INT3_U_REG_PAD1	14:13
`define VI_DSP_INT3_U_REG_INT3	15:15
`define VI_DSP_INT3_U_REG_TOTAL_SIZE	32'd16
`define VI_DSP_INT3_U_REG_TOTAL_RANGE	15:0

/*
 *  vi_dsp_latch0_l_reg struct
 */
`define VI_DSP_LATCH0_L_REG_GUN0_HCT	10:0
`define VI_DSP_LATCH0_L_REG_TOTAL_SIZE	32'd11
`define VI_DSP_LATCH0_L_REG_TOTAL_RANGE	10:0

/*
 *  vi_dsp_latch0_u_reg struct
 */
`define VI_DSP_LATCH0_U_REG_GUN0_VCT	10:0
`define VI_DSP_LATCH0_U_REG_PAD0	14:11
`define VI_DSP_LATCH0_U_REG_GUN0_TRG	15:15
`define VI_DSP_LATCH0_U_REG_TOTAL_SIZE	32'd16
`define VI_DSP_LATCH0_U_REG_TOTAL_RANGE	15:0

/*
 *  vi_dsp_latch1_l_reg struct
 */
`define VI_DSP_LATCH1_L_REG_GUN1_HCT	10:0
`define VI_DSP_LATCH1_L_REG_TOTAL_SIZE	32'd11
`define VI_DSP_LATCH1_L_REG_TOTAL_RANGE	10:0

/*
 *  vi_dsp_latch1_u_reg struct
 */
`define VI_DSP_LATCH1_U_REG_GUN1_VCT	10:0
`define VI_DSP_LATCH1_U_REG_PAD0	14:11
`define VI_DSP_LATCH1_U_REG_GUN1_TRG	15:15
`define VI_DSP_LATCH1_U_REG_TOTAL_SIZE	32'd16
`define VI_DSP_LATCH1_U_REG_TOTAL_RANGE	15:0

/*
 *  vi_hscale_reg struct
 */
`define VI_HSCALE_REG_STEP	8:0
`define VI_HSCALE_REG_PAD0	11:9
`define VI_HSCALE_REG_HSCALER_EN	12:12
`define VI_HSCALE_REG_TOTAL_SIZE	32'd13
`define VI_HSCALE_REG_TOTAL_RANGE	12:0

/*
 *  vi_fltr_coeff0_l_reg struct
 */
`define VI_FLTR_COEFF0_L_REG_T0	9:0
`define VI_FLTR_COEFF0_L_REG_T1_L	15:10
`define VI_FLTR_COEFF0_L_REG_TOTAL_SIZE	32'd16
`define VI_FLTR_COEFF0_L_REG_TOTAL_RANGE	15:0

/*
 *  vi_fltr_coeff0_u_reg struct
 */
`define VI_FLTR_COEFF0_U_REG_T1_U	3:0
`define VI_FLTR_COEFF0_U_REG_T2	13:4
`define VI_FLTR_COEFF0_U_REG_TOTAL_SIZE	32'd14
`define VI_FLTR_COEFF0_U_REG_TOTAL_RANGE	13:0

/*
 *  vi_fltr_coeff1_l_reg struct
 */
`define VI_FLTR_COEFF1_L_REG_T3	9:0
`define VI_FLTR_COEFF1_L_REG_T4_L	15:10
`define VI_FLTR_COEFF1_L_REG_TOTAL_SIZE	32'd16
`define VI_FLTR_COEFF1_L_REG_TOTAL_RANGE	15:0

/*
 *  vi_fltr_coeff1_u_reg struct
 */
`define VI_FLTR_COEFF1_U_REG_T4_U	3:0
`define VI_FLTR_COEFF1_U_REG_T5	13:4
`define VI_FLTR_COEFF1_U_REG_TOTAL_SIZE	32'd14
`define VI_FLTR_COEFF1_U_REG_TOTAL_RANGE	13:0

/*
 *  vi_fltr_coeff2_l_reg struct
 */
`define VI_FLTR_COEFF2_L_REG_T6	9:0
`define VI_FLTR_COEFF2_L_REG_T7_L	15:10
`define VI_FLTR_COEFF2_L_REG_TOTAL_SIZE	32'd16
`define VI_FLTR_COEFF2_L_REG_TOTAL_RANGE	15:0

/*
 *  vi_fltr_coeff2_u_reg struct
 */
`define VI_FLTR_COEFF2_U_REG_T7_U	3:0
`define VI_FLTR_COEFF2_U_REG_T8	13:4
`define VI_FLTR_COEFF2_U_REG_TOTAL_SIZE	32'd14
`define VI_FLTR_COEFF2_U_REG_TOTAL_RANGE	13:0

/*
 *  vi_fltr_coeff3_l_reg struct
 */
`define VI_FLTR_COEFF3_L_REG_T9	7:0
`define VI_FLTR_COEFF3_L_REG_T10	15:8
`define VI_FLTR_COEFF3_L_REG_TOTAL_SIZE	32'd16
`define VI_FLTR_COEFF3_L_REG_TOTAL_RANGE	15:0

/*
 *  vi_fltr_coeff3_u_reg struct
 */
`define VI_FLTR_COEFF3_U_REG_T11	7:0
`define VI_FLTR_COEFF3_U_REG_T12	15:8
`define VI_FLTR_COEFF3_U_REG_TOTAL_SIZE	32'd16
`define VI_FLTR_COEFF3_U_REG_TOTAL_RANGE	15:0

/*
 *  vi_fltr_coeff4_l_reg struct
 */
`define VI_FLTR_COEFF4_L_REG_T13	7:0
`define VI_FLTR_COEFF4_L_REG_T14	15:8
`define VI_FLTR_COEFF4_L_REG_TOTAL_SIZE	32'd16
`define VI_FLTR_COEFF4_L_REG_TOTAL_RANGE	15:0

/*
 *  vi_fltr_coeff4_u_reg struct
 */
`define VI_FLTR_COEFF4_U_REG_T15	7:0
`define VI_FLTR_COEFF4_U_REG_T16	15:8
`define VI_FLTR_COEFF4_U_REG_TOTAL_SIZE	32'd16
`define VI_FLTR_COEFF4_U_REG_TOTAL_RANGE	15:0

/*
 *  vi_fltr_coeff5_l_reg struct
 */
`define VI_FLTR_COEFF5_L_REG_T17	7:0
`define VI_FLTR_COEFF5_L_REG_T18	15:8
`define VI_FLTR_COEFF5_L_REG_TOTAL_SIZE	32'd16
`define VI_FLTR_COEFF5_L_REG_TOTAL_RANGE	15:0

/*
 *  vi_fltr_coeff5_u_reg struct
 */
`define VI_FLTR_COEFF5_U_REG_T19	7:0
`define VI_FLTR_COEFF5_U_REG_T20	15:8
`define VI_FLTR_COEFF5_U_REG_TOTAL_SIZE	32'd16
`define VI_FLTR_COEFF5_U_REG_TOTAL_RANGE	15:0

/*
 *  vi_fltr_coeff6_l_reg struct
 */
`define VI_FLTR_COEFF6_L_REG_T21	7:0
`define VI_FLTR_COEFF6_L_REG_T22	15:8
`define VI_FLTR_COEFF6_L_REG_TOTAL_SIZE	32'd16
`define VI_FLTR_COEFF6_L_REG_TOTAL_RANGE	15:0

/*
 *  vi_fltr_coeff6_u_reg struct
 */
`define VI_FLTR_COEFF6_U_REG_T23	7:0
`define VI_FLTR_COEFF6_U_REG_T24	15:8
`define VI_FLTR_COEFF6_U_REG_TOTAL_SIZE	32'd16
`define VI_FLTR_COEFF6_U_REG_TOTAL_RANGE	15:0

/*
 *  vi_clksel_reg struct
 */
`define VI_CLKSEL_REG_VICLKSEL	0:0
`define VI_CLKSEL_REG_TOTAL_SIZE	32'd1
`define VI_CLKSEL_REG_TOTAL_RANGE	0:0

/*
 *  vi_dtvstatus_reg struct
 */
`define VI_DTVSTATUS_REG_VISEL	1:0
`define VI_DTVSTATUS_REG_TOTAL_SIZE	32'd2
`define VI_DTVSTATUS_REG_TOTAL_RANGE	1:0

