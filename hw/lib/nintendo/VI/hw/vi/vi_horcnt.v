///////////////////////////////////////////////////////////////////////////////
//
//   (C) 1999 ARTX INC..  ALL RIGHTS RESERVED.  UNPUBLISHED -- RIGHTS
//   RESERVED UNDER THE COPYRIGHT LAWS OF THE UNITED STATES.  USE OF A
//   COPYRIGHT NOTICE IS PRECAUTIONARY ONLY AND DOES NOT IMPLY PUBLICATION
//   OR DISCLOSURE.
//
//   THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF
//   ARTX INC..  USE, DISCLOSURE, OR REPRODUCTION IS PROHIBITED WITHOUT
//   THE PRIOR EXPRESS WRITTEN PERMISSION OF ARTX INC..
//
//                   RESTRICTED RIGHTS LEGEND
//
//   Use, duplication, or disclosure by the Government is subject to
//   restrictions as set forth in subparagraph (c)(1)(ii) of the Rights
//   in Technical Data and Computer Software clause at DFARS 252.227-7013
//   or subparagraphs (c)(1) and (2) of Commercial Computer Software --
//   Restricted Rights at 48 CFR 52.227-19, as applicable.
//
//   ArtX Inc.
//   3400 Hillview Ave, Bldg 5
//   Palo Alto, CA 94304
//
///////////////////////////////////////////////////////////////////////////////

// Todo:
// - resetb, synchronous?
// - powerup defaults????
// - need to do interrupts
// What is that vi_reset going to actually reset? vi_enable???

module vi_horcnt (
	// Inputs from outside
	viResetb, vClk,
	// Inputs from PI interface
	hsy, hcs, hce, hbe, hbs, hlw,
	vi_reset, vi_enable,
	hct0, hct1, hct2, hct3,
       	i0clr, i1clr, i2clr, i3clr,
        i0enb, i1enb, i2enb, i3enb,
	 EqVCnt0, EqVCnt1, EqVCnt2, EqVCnt3,
	g0trgclr, g1trgclr,
	vi_nintl_pst, g0md_pst, g1md_pst,
	vi_mode,
	hbe656, hbs656,
	// Inputs from guns 
	gun0_trg_edge, gun1_trg_edge,
	// Inputs from Vercnt
	vsyncb_detected,
	// Output
	hsyncb, clr_brstb, hblank, csync_equ, csync_serr,
	hct_out, int0, int1, int2, int3,
	gun0_hct, gun1_hct, gun0_htrg, gun1_htrg,
	incr_hline, incr_line, hdsp_en, hblank_e, eav, dtv, hbe_odd, hbe_pst ,hbs_pst,
	hdsp_en656, hblank656_e
 );

`include "vi_state.v"
`include "vi_reg.v"


input		viResetb, vClk;
input 		gun0_trg_edge, gun1_trg_edge;		// Gun trigger edges (via vi_vercnt)  synchronized to vClk
input		vi_reset;				// Resets VI regs, state machines, counters...
input		vi_enable; 				// Enables counters
input		g0trgclr, g1trgclr;			// Read to gun status registers clears interrupt
input 		vi_nintl_pst;				// 0 = Interlaced    1 = Non Interlaced
input [9:0]  	hlw;					// Half Line Width (in pixels) NOTE: 1 pixel = 2 vClks
input [6:0]    	hce;					// Color Burst End (in pixels)
input [6:0]    	hcs;					// Color Burst Start (in pixels)
input [6:0]    	hsy;					// HSync Width (in pixels)
input [9:0]    	hbe;					// Horizontal Blank End (in pixels)
input [9:0]    	hbs;					// Horizontal Blank Start (in pixels)
input [9:0]    	hbe656;					// Horizontal Blank End (in pixels) for debug mode
input [9:0]    	hbs656;					// Horizontal Blank Start (in pixels) for debug mode
input [10:0]	hct0, hct1, hct2, hct3;			// Hcounts on which to generate interrupts 0-3 (in pixels)
input [1:0]	g0md_pst, g1md_pst;
input [1:0]	vi_mode;
input           i0clr, i1clr, i2clr, i3clr;
input           i0enb, i1enb, i2enb, i3enb;
input 		EqVCnt0, EqVCnt1, EqVCnt2, EqVCnt3;
input		vsyncb_detected;
input 		dtv;

output		hsyncb, clr_brstb, hblank, csync_equ, csync_serr,
		int0, int1, int2, int3, gun0_htrg, gun1_htrg,
		incr_hline, incr_line, hdsp_en, hblank_e, eav, hbe_odd,
		hdsp_en656, hblank656_e;

output [10:0]	gun0_hct, gun1_hct, hct_out;
output [9:0]	hbe_pst, hbs_pst;

wire [10:0]	eol;

reg 		hdsp_en656, hblank656_e;

reg [9:0]    	hbe_pst;					
reg [9:0]    	hbs_pst;					
reg [9:0]	hbe656_pst, hbs656_pst;

reg		hsyncb, clr_brstb, hblank, csync_equ, csync_serr,
		int0, int1, int2, int3, gun0_htrg, gun1_htrg, hblank_e;
reg [10:0]	gun0_hct, gun1_hct;

wire 		move_hstate, hstate0, EqHSyncStr, EqHSyncEnd,
		EqClrBrstStr, EqClrBrstEnd, EqHBlnkStr, EqHBlnkEnd, 
		EqCEqStr, EqCEqEnd, EqCSerrStr, EqCSerrEnd, EqHBlnkErlyEnd,
		EqHBlnkErlyStr;

reg 		current_hstate, hdsp_en_d1;
wire 		EqHCnt0, EqHCnt1, EqHCnt2, EqHCnt3, Eq656HBlnkStr, Eq656HBlnkEnd,
		Eq656HBlnkErlyStr, Eq656HBlnkErlyEnd;
reg  [11:0]	hct; 			//A full line HCount NOTE: 2 clks = 1 pixel
reg  [10:0]	hcount;			//A half line Horzicount NOTE: 2 clks = 1 pixel
wire [11:0]	hct_w;

wire [9:0]	eqPulLen;
parameter 	second_hl = `SECOND_HL,
		first_hl = `FIRST_HL;


// This was done so that an hct of 1,2,3,4,5,6
// will equal	 	hct_out of 1,1,2,2,3,3
assign hct_w = hct + 1;
assign hct_out = hct_w[11:1];

assign eav = ~hdsp_en && hdsp_en_d1;
assign hbe_odd = hbe_pst[0];


always @ (posedge vClk)
 begin
	
	if ((vi_reset) || (~viResetb))
         	gun0_hct <= 11'b0;
        if ((gun0_trg_edge) && (~gun0_htrg) && (g0md_pst != 2'b00))	
		gun0_hct <= hct[11:1];

	if ((vi_reset) || (~viResetb))
                gun1_hct <= 11'b0;
        if ((gun1_trg_edge) && (~gun1_htrg) && (g1md_pst != 2'b00))
		gun1_hct <= hct[11:1];

	if ((g1trgclr) || (vi_reset) || (~viResetb))
                gun1_htrg <= 1'b0;
        else if (gun1_trg_edge && (g1md_pst != 2'b00) && (~gun1_htrg))
                gun1_htrg <= 1'b1;

	if ((g0trgclr)  || (vi_reset) || (~viResetb))
                gun0_htrg <= 1'b0;
        else if (gun0_trg_edge && (g0md_pst != 2'b00) && (~gun0_htrg))
                gun0_htrg <= 1'b1;
 end    

// double register interrupts registers
// MAY NOT HAVE TO DO THIS
always @ (posedge vClk)
 begin
	if (vi_reset || ~viResetb)
		int0 <= 1'b0;
	else if (i0clr)
		int0 <= 1'b0;
	else if (EqHCnt0 && i0enb && EqVCnt0 && vi_enable)
		int0 <= 1'b1;

	if (vi_reset || ~viResetb)
		int1 <= 1'b0;
	else if (i1clr)
		int1 <= 1'b0;
	else if (EqHCnt1 && i1enb && EqVCnt1 && vi_enable)
		int1 <= 1'b1;

	if (vi_reset || ~viResetb)
		int2 <= 1'b0;
	else if (i2clr)
		int2 <= 1'b0;
	else if (EqHCnt2 && i2enb && EqVCnt2 && vi_enable)
		int2 <= 1'b1;

	if (vi_reset || ~viResetb)
		int3 <= 1'b0;
	else if (i3clr)
		int3 <= 1'b0;
	else if (EqHCnt3 && i3enb && EqVCnt3 && vi_enable)
		int3 <= 1'b1;
 end

assign incr_line = ((current_hstate == second_hl) && (move_hstate));
assign incr_hline = move_hstate;
assign hdsp_en = (~hblank);
assign eol = {hlw, 1'b0};
assign move_hstate = (hcount == eol);

assign EqHCnt0 = (hct0 == hct_w[11:1]);
assign EqHCnt1 = (hct1 == hct_w[11:1]);
assign EqHCnt2 = (hct2 == hct_w[11:1]);
assign EqHCnt3 = (hct3 == hct_w[11:1]);

assign EqHSyncStr = (hcount == eol) && ~hstate0;
assign EqHSyncEnd = (hcount[10:1] == {3'b0,hsy}) &&  hstate0;
assign EqClrBrstStr = (hcount[10:1] == {3'b0,hcs}) && hstate0;
assign EqClrBrstEnd = (hcount[10:1] == {3'b0,hce}) && hstate0;
assign EqHBlnkStr = (hcount[10:1] == hbs_pst) && ~hstate0;
assign EqHBlnkEnd = (hcount[10:1] == hbe_pst) && hstate0;

assign Eq656HBlnkStr = (hcount[10:1] == hbs656_pst) && ~hstate0;
assign Eq656HBlnkEnd = (hcount[10:1] == hbe656_pst) && hstate0;

assign Eq656HBlnkErlyStr = (hcount[10:1] == (hbs656_pst-1)) && ~hstate0;
assign Eq656HBlnkErlyEnd = (hcount[10:1] == (hbe656_pst-1)) && hstate0;


assign EqHBlnkErlyEnd = (hcount[10:1] == (hbe_pst-1)) && hstate0;
assign EqHBlnkErlyStr = (hcount[10:1] == (hbs_pst-1)) && ~hstate0;

// Equalization Pulses should be generated only in 1sthalf line if non-interlaced
assign eqPulLen = dtv ? ({3'b0, hsy[6:0]}) : ({3'b0,hsy[6:1]}-1);
assign EqCEqEnd = (hcount[10:1] == eqPulLen) && (hstate0 || ~vi_nintl_pst);
assign EqCEqStr = (hcount == eol) && (~hstate0 || ~vi_nintl_pst); 

// Serration Pulses should be generated only in 2nd half line if non-interlaced
assign EqCSerrStr = (hcount == (eol - {hsy, 1'b0})) && (~hstate0 || ~vi_nintl_pst);
assign EqCSerrEnd = (hcount == eol) && (~hstate0 || ~vi_nintl_pst);		



always @ (posedge vClk)
	if ((~viResetb) || (vi_reset))
	begin
		hcount <= 11'h01;
		hct <= 12'h01;
	end
	else
		if (vi_enable)
		 begin
			if (move_hstate) 
				hcount <= 11'h01;
			else
				hcount <= hcount + 1;
			
			if (EqHSyncStr)
				hct <= 12'h01;
			else
				hct <= hct + 1;
		 end
	
		

always @ (posedge vClk)
 	if ((~ viResetb) || (vi_reset) || (~vi_enable))
	// Coming out of reset, we start in serration (vsync), first field (odd),
	// when in PAL mode, serration starts on a half line, otherwise
	// serration starts on a full line
		current_hstate <= (~dtv && (vi_mode == 2'b01)) ? second_hl : first_hl;
	else if (move_hstate)
		current_hstate <= ~current_hstate;


// increment line at end of every 2nd half line

// SHOULD BE SYNCHRONOUS TO VSYNC< HSYNC
assign hstate0 = (current_hstate == first_hl);




always @ (posedge vClk)
 begin
	hdsp_en_d1 <= hdsp_en;

	if (EqHSyncEnd || vi_reset || (~viResetb))
		hsyncb <= 1'b1;
	else if ((EqHSyncStr) && vi_enable)
		hsyncb <= 1'b0;
	
	if (EqClrBrstEnd || vi_reset || (~viResetb))
		clr_brstb <= 1'b1;
	if ((EqClrBrstStr) && vi_enable)
		clr_brstb <= 1'b0;

	if (EqHBlnkStr || vi_reset || (~viResetb))
		hblank <= 1'b1;
	else if ((EqHBlnkEnd) && vi_enable)
		hblank <= 1'b0;

	if (Eq656HBlnkStr || vi_reset || (~viResetb))
		hdsp_en656 <= 1'b0;
	else if ((Eq656HBlnkEnd) && vi_enable)
		hdsp_en656 <= 1'b1;

	if (Eq656HBlnkErlyStr || vi_reset || (~viResetb))
		hblank656_e <= 1'b1;
	else if ((Eq656HBlnkErlyEnd) && vi_enable)
		hblank656_e <= 1'b0;
	
	if (EqHBlnkErlyStr || vi_reset || (~viResetb))
		hblank_e <= 1'b1;
	else if ((EqHBlnkErlyEnd) && vi_enable)
		hblank_e <= 1'b0;
 
	if (EqCEqEnd || vi_reset || (~viResetb))
		csync_equ <= 1'b1;
	else if ((EqCEqStr) && vi_enable)
		csync_equ <= 1'b0;

	if (EqCSerrEnd || vi_reset || (~viResetb))
		csync_serr <= 1'b0;
	else if ((EqCSerrStr) && vi_enable)	
		csync_serr <= 1'b1;
	
	if (~viResetb)
	 begin
		hbe_pst <= 10'b0;
		hbs_pst <= 10'b0;
		hbs656_pst <= 10'b0;
		hbe656_pst <= 10'b0;
	 end
	else if (vsyncb_detected)
	 begin
//		hbe_pst <= {hbe[9:1],1'b0};	
		hbe_pst <= hbe;	
		hbs_pst <= hbs;	
		hbs656_pst <= hbs656;
		hbe656_pst <= hbe656;
	 end
 end 

endmodule


