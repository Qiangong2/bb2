///////////////////////////////////////////////////////////////////////////////
//
//   (C) 1999 ARTX INC..  ALL RIGHTS RESERVED.  UNPUBLISHED -- RIGHTS
//   RESERVED UNDER THE COPYRIGHT LAWS OF THE UNITED STATES.  USE OF A
//   COPYRIGHT NOTICE IS PRECAUTIONARY ONLY AND DOES NOT IMPLY PUBLICATION
//   OR DISCLOSURE.
//
//   THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF
//   ARTX INC..  USE, DISCLOSURE, OR REPRODUCTION IS PROHIBITED WITHOUT
//   THE PRIOR EXPRESS WRITTEN PERMISSION OF ARTX INC..
//
//                   RESTRICTED RIGHTS LEGEND
//
//   Use, duplication, or disclosure by the Government is subject to
//   restrictions as set forth in subparagraph (c)(1)(ii) of the Rights
//   in Technical Data and Computer Software clause at DFARS 252.227-7013
//   or subparagraphs (c)(1) and (2) of Commercial Computer Software --
//   Restricted Rights at 48 CFR 52.227-19, as applicable.
//
//   ArtX Inc.
//   3400 Hillview Ave, Bldg 5
//   Palo Alto, CA 94304
//
///////////////////////////////////////////////////////////////////////////////

// Todo:
// - resetb, synchronous?
// - powerup defaults????
// - need to do interrupts
// - vi_reset <-- still up in the air
// - vi_enable <-- will that reset everything? or will it be preceded/followed
//		by a vi_reset?
// What happens if any of these are zero?

module vi_vercnt (
	// Inputs from outside
	viResetb, vClk,
	// Inputs from PI interface
	bs4, be4, bs1, be1, bs2, be2, bs3, be3, 
	vi_enable, vi_reset, vct0, vct1, vct2, vct3,
	equ, acv, opsb, epsb,
	eprb, oprb, g0trgclr, g1trgclr, vi_dbg,
	vi_nintl, g0md_pst, g1md_pst,
	// Inputs from guns
	synced_gun_trig0, synced_gun_trig1,
	// Inputs from Horizontal Counters
	incr_line, hsyncb, csync_equ, csync_serr,
	incr_hline, eav, 
	// Outputs (timing signals)
	bblnkb, fld, fld601, vsyncb, csyncb, vblank, vdsp_en,
	fld_cnt, rst_phase_cnt, V_601, gun0_trg_edge, gun1_trg_edge,
	top_pic_sel, vi_nintl_pst, vsyncb_detected, vblank_d1, vsyncb_d1, 
	// Outputs to PI interface
	vct, EqVCnt0, EqVCnt1, EqVCnt2, EqVCnt3,
	gun0_vct, gun1_vct, 
	gun0_vtrg, gun1_vtrg
 );

`include "vi_state.v"
`include "vi_reg.v"

input 		viResetb, vClk;
input [4:0]	bs4, bs1, bs2, bs3;
input [10:0]	be4, be1, be2, be3;
input 		incr_line, hsyncb, vi_enable, vi_reset, incr_hline,
		synced_gun_trig0, synced_gun_trig1, csync_equ, vi_dbg,
		csync_serr, g0trgclr, g1trgclr, vi_nintl;
input [1:0]	g0md_pst, g1md_pst;
input [3:0]	equ;
input [9:0]	acv;
input [9:0]	opsb;
input [9:0]	oprb;
input [9:0]	epsb;
input [9:0]	eprb;
input [10:0]	vct0, vct1, vct2, vct3;
input 		eav;
output		bblnkb, fld, fld601, vsyncb, csyncb,
		vblank, vdsp_en, V_601;
output 		vblank_d1, vsyncb_d1;
output		EqVCnt0, EqVCnt1, EqVCnt2, EqVCnt3;
output [10:0] 	vct;
output 		top_pic_sel;
output [10:0]	gun0_vct, gun1_vct;
output 		gun0_vtrg, gun1_vtrg;
output [1:0]	fld_cnt;
output 		rst_phase_cnt;
output 		gun0_trg_edge, gun1_trg_edge;
output 		vi_nintl_pst;
output 		vsyncb_detected;

reg 		vblank_d1;
reg 		vi_nintl_pst;
reg [10:0]	gun0_vct, gun1_vct;
reg 		gun0_vtrg, gun1_vtrg;
reg		start;
reg		csyncb, bblnkb, vdsp_en_d1;
wire 		vdsp_en_detected;
reg [10:0]	HL_count;
reg [10:0]	vcount;
wire 		EqBBlnkStr, EqBBlnkEnd;
wire		move_vstates, EqVCnt0, EqVCnt1,
		EqVCnt2, EqVCnt3;
wire		fld; 
reg		fld601;
wire [9:0]	prb, psb;
reg [9:0]	prb_pst, psb_pst;
reg [1:0]	fld_cnt;
reg [9:0]	acv_pst;
reg [4:0]	cur_bs;
reg [10:0]	cur_be;
reg [10:0]	compare_value;
wire 		top_pic_sel;
wire 		gun0_trg_edge, gun1_trg_edge;
reg 		synced_gun_trig0_d1, synced_gun_trig1_d1;
reg		V_601;
wire		vsyncb_detected;
reg		vsyncb_d1;
reg 		fld_cnt_lsb_d1;
wire		new_field_detected;
reg [10:0]	cb_hlcount;
reg [1:0]	clr_brst_sel;

// May have an include here
reg [2:0]	current_vstate, next_vstate, last_vstate;

parameter 	idle = `IDLE,
		pre_eq = `PRE_EQ,
		serr = `SERR,
		post_eq = `POST_EQ,
		pre_blnk = `PRE_BLNK,
		acv_state = `ACV,
		post_blnk = `POST_BLNK;

assign top_pic_sel = vsyncb_detected && (~hsyncb);


always @ (posedge vClk)
 begin
        synced_gun_trig0_d1 <= synced_gun_trig0;
        synced_gun_trig1_d1 <= synced_gun_trig1;
        vsyncb_d1 <= vsyncb;
	vblank_d1 <= vblank;
	
	if (~viResetb)
	 begin
		vi_nintl_pst <= 1'b0;
		acv_pst <= 10'b0;
		prb_pst <= 10'b0;
		psb_pst <= 10'b0;
         end
	else if (vsyncb_detected)
	begin
		vi_nintl_pst <= vi_nintl;
		acv_pst <= acv;
		prb_pst <= prb;
		psb_pst <= psb;
	end

end

//sync g0trgclr_sync(.D(gun0_trg_clr), .CK(vClk), .Q(vid_gun0_trg_clr))
//sync g1trgclr_sync(.D(gun1_trg_clr), .CK(vClk), .Q(vid_gun1_trg_clr))

always @ (posedge vClk)
 begin
	
	if ((vi_reset) || (~viResetb))	
	 begin
		gun0_vct <= 11'b0;
	 end
	else if ((gun0_trg_edge) && (~gun0_vtrg) && (g0md_pst != 2'b00))
	 begin
		gun0_vct <= vcount;
	 end
	
	if ((g0trgclr) || (vi_reset) || (~viResetb))
		gun0_vtrg <= 1'b0;
	else if ((gun0_trg_edge) && (~gun0_vtrg) && (g0md_pst != 2'b00))
		gun0_vtrg <= 1'b1;
	
	if ((vi_reset) || (~viResetb))	
	 begin
		gun1_vct <= 11'b0;
	 end
	else if ((gun1_trg_edge) && (~gun1_vtrg) && (g1md_pst != 2'b00))
	 begin
		gun1_vct <= vcount;
	 end

	if ((g1trgclr) || (vi_reset) || (~viResetb))
		gun1_vtrg <= 1'b0;
	else if ((gun1_trg_edge) && (~gun1_vtrg) && (g1md_pst != 2'b00))
		gun1_vtrg <= 1'b1;

 end

/*
case (g1trgmd_pst)
	2'b00: 	allow_trg <= 1'b0;
	2'b01:	allow_trg <= (fld_cnt==g0fld_pst)
*/

assign vsyncb_detected = ~vsyncb && vsyncb_d1;
assign gun0_trg_edge = (synced_gun_trig0 && (~synced_gun_trig0_d1));
assign gun1_trg_edge = (synced_gun_trig1 && (~synced_gun_trig1_d1));




// DO I NEED TO DOUBLE REGISTER VCT???
assign EqVCnt0 = (vcount == vct0);
assign EqVCnt1 = (vcount == vct1);
assign EqVCnt2 = (vcount == vct2);
assign EqVCnt3 = (vcount == vct3);

// double register interrupts registers
// MAY NOT HAVE TO DO THIS
// IF DO ADD VFLD ONES AS WELL


assign vct = vcount;

assign vsyncb = ~(current_vstate == serr);
assign psb = (fld) ? epsb : opsb;
assign prb = (fld) ? eprb : oprb;

assign vblank = ~(current_vstate == acv_state);
assign vdsp_en = (~vblank);

// THERE IS A BETTER WAY TO DO THIS

always @ (posedge vClk)
	if (~viResetb || vi_reset)
		start <= 1'b0;
// Now can leave idle on half line (PAL) since we start on serr
//	else if (incr_hline && ~incr_line)
	else if (incr_hline)
		start <= vi_enable;


assign new_field_detected = fld_cnt_lsb_d1 ^ fld_cnt[0];

always @ (posedge vClk)
 begin
	fld_cnt_lsb_d1 <= fld_cnt[0];

	if ((~ viResetb) || vi_reset)
	begin
		fld_cnt <= 2'b00;
	end
	else if (incr_hline)
	 begin
// since PSB can equal zero, the state before PRE_EQ could be POST_BLNK or ACV
		if ( ( (current_vstate == post_blnk)  || (current_vstate == acv_state)) && (next_vstate == pre_eq) && move_vstates)
                	fld_cnt <= fld_cnt + 1;
        end



	if ((~ viResetb) || vi_reset)
	begin
		fld601 <= 1'b0;
		V_601 <= 1'b1;
	end
	else if (eav)
	 begin
		if (current_vstate == pre_eq)
		 begin		
			if ((HL_count == equ) || (HL_count == equ-1))
				fld601 <= ~fld601;
		 end
		
		if (current_vstate == acv_state)
		 begin
			if (HL_count == {acv_pst, 1'b0})
				V_601 <= 1'b1;
		 end
		else if (last_vstate == acv_state)
                 begin
                        if (HL_count == 11'b01)  
                                V_601 <= 1'b1;
		 end
		else if (current_vstate == pre_blnk)
		 begin
			if (HL_count == prb_pst)
				V_601 <= 1'b0;
		 end
	end
 end


// SHOULD I RESET??
// Need field flag to occur one half line early (on a line boundary) for 601

// Odd fields 1,3  Even fields 2,4 
// Or in this world: Odd fields 0,2 Even Fields 1,3 
// Encoder wants: 0- odd  1- even

// Probably should organize this better

assign fld = fld_cnt[0]; 


//assign rst_phase_cnt = move_vstates && (next_vstate == serr) && (fld_cnt == 2'b0);		
assign rst_phase_cnt = incr_hline && move_vstates && (next_vstate == serr) && (fld_cnt == 2'b00);
//assign rst_phase_cnt = incr_hline && move_vstates && (next_vstate == serr);

// NNED TO KEEP AN EYE ON CSYNCB
// COULD IMPLEMENT THIS 1 HOT
always @ (current_vstate or move_vstates or start or csync_equ or
		csync_serr or hsyncb or equ or prb_pst or acv_pst or psb_pst)
case (current_vstate)
	idle: begin
		csyncb = 1'b1;
		compare_value = 11'b01;
		if (start)  
			next_vstate = serr;
		else
			next_vstate = idle;
	 end
	
	pre_eq: begin
		csyncb = csync_equ;
                compare_value = {7'b0,equ};
                if (move_vstates)   
			next_vstate = serr;
		else
			next_vstate = pre_eq;
	 end

	serr: begin
		csyncb = csync_serr;
		compare_value = {7'b0,equ};
		if (move_vstates)
                        next_vstate = post_eq;
                else
                        next_vstate = serr;
         end

	post_eq: begin
		csyncb = csync_equ;
		compare_value = {7'b0,equ};
                if (move_vstates)
			next_vstate = pre_blnk;
                else
                        next_vstate = post_eq;  
	 end

	pre_blnk: begin
		csyncb = hsyncb;		
		compare_value = {1'b0, prb_pst};
                if (move_vstates)
			if (acv_pst == 10'b0)
				next_vstate = post_blnk;
			else                        
				next_vstate = acv_state;
                else
                        next_vstate = pre_blnk;

         end

        acv_state: begin
		csyncb = hsyncb;		
		compare_value = {acv_pst, 1'b0};
		if (move_vstates)
			if (psb_pst == 10'b0)
				next_vstate = pre_eq;
			else
	                        next_vstate = post_blnk;
                else    
                        next_vstate = acv_state;    
         end

        post_blnk: begin
		csyncb = hsyncb;		
		compare_value = {1'b0, psb_pst};
                if (move_vstates)
                        next_vstate = pre_eq;
                else    
                        next_vstate = post_blnk;    
         end



// MAYBE SOMETHING ELSE
	default:
		begin
		 next_vstate = idle;
		csyncb = 1'b1;
		compare_value = 11'b01;
		end


 endcase


always @ (posedge vClk)
	if (~ viResetb || vi_reset)
	 begin
		current_vstate <= idle;
		last_vstate <= idle;
	 end
	else if (incr_hline && move_vstates)
	 begin
		current_vstate <= next_vstate;
		last_vstate <= current_vstate;
	 end




// THIS MIGHT NOT WORK, WILL NEED TO CHECK THIS RTL AND GATE
assign move_vstates =  (HL_count == compare_value);



always @ (posedge vClk)
 vdsp_en_d1 <= vdsp_en;

assign vdsp_en_detected = ((~vdsp_en_d1) && (vdsp_en));

always @ (posedge vClk)
 begin
	if ((vi_reset) || (incr_hline && move_vstates) || (~viResetb))
		HL_count <= 11'h01;
	else if (incr_hline && vi_enable)
		HL_count <= HL_count + 1; 
end

always @ (posedge vClk)
 begin
	if ((~ viResetb) || (vi_reset)) 
		vcount <= 11'h01;
	// This will count lines in terms of frames!!!
	// vcount=0 should be start of pre-eq
	// In non-interlace mode, it will reset every field
	// Going by Nintendo, A frame will always start on a line
	// basically when fld_cnt is about to change (when the state is about to become PRE_EQ) AND
	// a frame boundary (fld is even and is about to become odd) AND its non-interlaced
	// if its interlaced then we reset vcount to 1 every field
	// Note: we are using incr_hline, but incr_line could have been used as well since frame (field for non-interaced)
	// changes will only occur on line boundaries

 	else if ((incr_hline && ( ((current_vstate == post_blnk)  || (current_vstate == acv_state))  && (next_vstate == pre_eq) && move_vstates)) 
		&& (fld_cnt[0] || 1'b0))
		vcount <= 11'b01;
	else if (incr_line && vi_enable)
		vcount <= vcount + 1;

	if ((~viResetb) || (vi_reset))
                cb_hlcount <= 11'h01;
// one clock before vsyncb, so cb_hlcount will be in line with vsyncb
        else if ((next_vstate == serr) && (incr_hline && move_vstates))
                cb_hlcount <= 11'b01;
        else if (incr_hline && vi_enable)
                cb_hlcount <= cb_hlcount + 1;
 end
/*
begin  
        if ((vi_reset) || (incr_hline && move_vstates) || (~viResetb))
                HL_count <= 11'h01;  
        else if (incr_hline && vi_enable)
                HL_count <= HL_count + 1;
end

*/
/*
always @ (cur_be or psb_pst or HL_count or current_vstate)
	if (cur_be == psb_pst)
		EqBBlnkStr <= (HL_count == 11'h001) && (current_vstate == pre_eq);
	else
		EqBBlnkStr <= (HL_count == (cur_be+1)) && (current_vstate == post_blnk);

always @ (cur_bs or prb_pst or HL_count or current_vstate)
	if (cur_bs == prb_pst)
		EqBBlnkEnd <= (HL_count == 11'h001) && (current_vstate == acv_state);
	else
		EqBBlnkEnd <= (HL_count == (cur_bs+1)) && (current_vstate == pre_blnk); 
*/


//assign EqBBlnkEnd = (HL_count == (cur_bs+1)) && (current_vstate == pre_blnk);
//assign EqBBlnkStr = (HL_count == (cur_be+1)) && (current_vstate == post_blnk);

// so when cb_hlcount is about to change from cur_bs to cur_bs+1, EqBBlnkEnd=1
assign EqBBlnkEnd = (cb_hlcount == cur_bs) && incr_hline;
assign EqBBlnkStr = (cb_hlcount == cur_be) && incr_hline;

always @ (posedge vClk)
 begin
	if ((EqBBlnkStr && vi_enable) || vi_reset || (~viResetb))
		bblnkb <= 1'b0;
	else if (EqBBlnkEnd) 
		bblnkb <= 1'b1;

// We cant use fld_cnt to select the color burst because if field and burst blank
// are supposed to occur simultaneously or burst blank becomes active after the field
// flag changes (as in field 2 of NTSC Interlaced), then the wrong be/bs combination
// will be selected
	if (~viResetb || vi_reset)
		clr_brst_sel <= 2'b00;
	else if (vsyncb_detected)
		clr_brst_sel <= fld_cnt;

 end 




always @ (clr_brst_sel or bs4 or be4 or bs1 or be1 or bs2 or be2 or bs3 or be3 )
  begin
	case (clr_brst_sel)
		2'b00 : begin
			cur_bs = bs1;
			cur_be = be1;			
		 end
		2'b01 : begin
			cur_bs = bs2;
			cur_be = be2;
		 end
		2'b10 : begin
			cur_bs = bs3;
			cur_be = be3;
		 end
		2'b11 : begin
			cur_bs = bs4;
			cur_be = be4;
		 end
	endcase
 end 



endmodule
