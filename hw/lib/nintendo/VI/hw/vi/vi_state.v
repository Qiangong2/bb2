/*
 *  vi_state.v
 *
 *  NOTE: This is a generated file.  DO NOT HAND EDIT.
 *
 *  Generated from vi_state.fdl
 */


/*
 *  vcounter enum
 */
`define IDLE	3'b000
`define PRE_EQ	3'b001
`define SERR	3'b010
`define POST_EQ	3'b011
`define PRE_BLNK	3'b100
`define ACV	3'b101
`define POST_BLNK	3'b110
`define VCOUNTER_UNUSED_7	3'b111

/*
 *  hcounter enum
 */
`define FIRST_HL	1'b0
`define SECOND_HL	1'b1

/*
 *  ack_states enum
 */
`define S0	3'b000
`define S1	3'b001
`define S2	3'b010
`define S3	3'b011
`define S4	3'b100
`define ACK_STATES_UNUSED_5	3'b101
`define ACK_STATES_UNUSED_6	3'b110
`define ACK_STATES_UNUSED_7	3'b111

/*
 *  req_states enum
 */
`define REQ_IDLE	2'b00
`define SEND_REQ	2'b01
`define WAIT_FOR_ACK	2'b10
`define GOT_ACK	2'b11

/*
 *  enc_states enum
 */
`define ENC_STATES_UNUSED_0	5'b00000
`define INACTIVE	5'b00001
`define LUMA_CB	5'b00010
`define ENC_STATES_UNUSED_3	5'b00011
`define CB	5'b00100
`define ENC_STATES_UNUSED_5	5'b00101
`define ENC_STATES_UNUSED_6	5'b00110
`define ENC_STATES_UNUSED_7	5'b00111
`define LUMA_CR	5'b01000
`define ENC_STATES_UNUSED_9	5'b01001
`define ENC_STATES_UNUSED_10	5'b01010
`define ENC_STATES_UNUSED_11	5'b01011
`define ENC_STATES_UNUSED_12	5'b01100
`define ENC_STATES_UNUSED_13	5'b01101
`define ENC_STATES_UNUSED_14	5'b01110
`define ENC_STATES_UNUSED_15	5'b01111
`define CR	5'b10000
`define ENC_STATES_UNUSED_17	5'b10001
`define ENC_STATES_UNUSED_18	5'b10010
`define ENC_STATES_UNUSED_19	5'b10011
`define ENC_STATES_UNUSED_20	5'b10100
`define ENC_STATES_UNUSED_21	5'b10101
`define ENC_STATES_UNUSED_22	5'b10110
`define ENC_STATES_UNUSED_23	5'b10111
`define ENC_STATES_UNUSED_24	5'b11000
`define ENC_STATES_UNUSED_25	5'b11001
`define ENC_STATES_UNUSED_26	5'b11010
`define ENC_STATES_UNUSED_27	5'b11011
`define ENC_STATES_UNUSED_28	5'b11100
`define ENC_STATES_UNUSED_29	5'b11101
`define ENC_STATES_UNUSED_30	5'b11110
`define ENC_STATES_UNUSED_31	5'b11111

/*
 *  timing_ref_states enum
 */
`define ACTIVE_PIX	3'b000
`define PREAMBLE0	3'b001
`define PREAMBLE1	3'b010
`define PREAMBLE2	3'b011
`define STATUS_WORD	3'b100
`define INACTIVE_PIX	3'b101
`define TIMING_REF_STATES_UNUSED_6	3'b110
`define TIMING_REF_STATES_UNUSED_7	3'b111

/*
 *  hscale_states enum
 */
`define SC_INACTIVE	3'b000
`define LOAD	3'b001
`define SHIFT1	3'b010
`define SHIFT2	3'b011
`define CALC_Y0	3'b100
`define CALC_CB	3'b101
`define CALC_Y1	3'b110
`define CALC_CR	3'b111

/*
 *  gun_states enum
 */
`define GUN_OFF	3'b000
`define GUN_ONE	3'b001
`define GUN_TWOA	3'b010
`define GUN_ALWAYS	3'b011
`define GUN_TWOB	3'b100
`define GUN_STATES_UNUSED_5	3'b101
`define GUN_STATES_UNUSED_6	3'b110
`define GUN_STATES_UNUSED_7	3'b111

