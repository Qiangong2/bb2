
///////////////////////////////////////////////////////////////////////////////
//
//   (C) 1999 ARTX INC..  ALL RIGHTS RESERVED.  UNPUBLISHED -- RIGHTS
//   RESERVED UNDER THE COPYRIGHT LAWS OF THE UNITED STATES.  USE OF A
//   COPYRIGHT NOTICE IS PRECAUTIONARY ONLY AND DOES NOT IMPLY PUBLICATION
//   OR DISCLOSURE.
//
//   THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF
//   ARTX INC..  USE, DISCLOSURE, OR REPRODUCTION IS PROHIBITED WITHOUT
//   THE PRIOR EXPRESS WRITTEN PERMISSION OF ARTX INC..
//
//                   RESTRICTED RIGHTS LEGEND
//
//   Use, duplication, or disclosure by the Government is subject to
//   restrictions as set forth in subparagraph (c)(1)(ii) of the Rights
//   in Technical Data and Computer Software clause at DFARS 252.227-7013
//   or subparagraphs (c)(1) and (2) of Commercial Computer Software --
//   Restricted Rights at 48 CFR 52.227-19, as applicable.
//
//   ArtX Inc.
//   3400 Hillview Ave, Bldg 5
//   Palo Alto, CA 94304
//
//////////////////////////////////////////////////////////////////////////////
// Todo
// step will have to be brought in as hct was brought out

module vi_hscaler (vClk, viResetb, dsp_en, lst_pxl, frst_pxl, vblank,
		   csel, psel, uvodd, uvshf, yshf, vi_reset, step,
		incr_dbl_count, sy_out, scb_out, 
		scr_out, hsyncb, cur_double_lft, nxt_double_lft,
        	cur_double_rgt, nxt_double_rgt, vi_dlr,
		sy_out_rgt, scb_out_rgt, scr_out_rgt, left,
		uvshf_rgt, yshf_rgt, incr_dbl_count_rgt, last_double, toggle_detected,
		last_double_persistant);

`include "vi_state.v"
`include "vi_reg.v"

input 		vClk, viResetb, vi_reset;
input [8:0]	step;
input 		hsyncb, vblank;
input 		dsp_en, lst_pxl, frst_pxl;
input 		last_double;
output [1:0] 	csel, uvshf, yshf;
output [1:0]	uvshf_rgt, yshf_rgt; 
output 		uvodd;
output [2:0] 	psel;
output		incr_dbl_count;
output		incr_dbl_count_rgt;
output [7:0]	sy_out, scb_out, scr_out;
output [7:0]	sy_out_rgt, scb_out_rgt, scr_out_rgt;
output 		left;
input [31:0]	cur_double_lft, nxt_double_lft;
input [31:0]	cur_double_rgt, nxt_double_rgt;
input 		vi_dlr;
output 		toggle_detected;
output 		last_double_persistant;

reg [2:0]		psel;
reg [1:0]		csel;
reg [1:0]		yshf, uvshf;
/*
input [9:0]	T3_0, T3_1, T3_2, T3_3, T3_4, T3_5, T3_6, T3_7, T4_0;
input [7:0]	T4_1, T4_2, T4_3, T4_4, T4_5, T4_6, T4_7, T5_0, T5_1, 
		T5_2, T5_3, T5_4, T5_5, T5_6, T5_7, T_24;
*/


reg 		ld_dbl_cntr; 
wire [7:0]	first_sy, second_sy, scb, scr;
wire [7:0]	first_sy_rgt, second_sy_rgt, scb_rgt, scr_rgt;
reg [7:0]	nxt_sy, nxt_nxt_sy; 
wire [7:0]	cur_sy;
reg [7:0]	sy_out, scb_out, scr_out;
reg [7:0]	sy_out_rgt, scb_out_rgt, scr_out_rgt;
reg 		scaling;
reg [8:0]	fir_cntr, old_fir_cntr;
wire [8:0] 	csel_cntr, old_csel_cntr;
wire [8:0]	toggle_cntr;
wire [2:0]	coeff_select, old_coeff_select;
wire 		toggle_bit, toggle_detected, allow_shift, ld_regs;
reg		toggle_bit_d1, dsp_en_d1;
reg [3:0]	cur_scaler_phase, nxt_scaler_phase;
reg 		right;
wire 		luma_shift, chroma_shift, allow_shift0, allow_shift1;
reg		chroma_ld, luma_ld;
reg		frst_pxl_d1, frst_pxl_d2, lst_pxl_d1, lst_pxl_d2, lst_pxl_persistant;
parameter	sc_inactive = `SC_INACTIVE, load = `LOAD, shift1 = `SHIFT1, shift2 = `SHIFT2,
		calc_y0 = `CALC_Y0, calc_cr = `CALC_CR, calc_y1 = `CALC_Y1,
		calc_cb = `CALC_CB;
reg	 	uvtoggle_pending;

reg		uvodd_e2;
reg [2:0] 	psel_e2;
reg  [2:0]	psel_e;
reg  [1:0]	csel_e;
reg		dsp_en_d2;
wire [8:0]	oscr, oscb;
wire [8:0]	oscr_rgt, oscb_rgt;
wire [7:0]	nxt_sy_w;
wire [7:0]	nxt_sy_w_rgt;
reg 		nxt_sy_sel;
reg 		incr_dbl_count_mask;
reg		nxt_state_shift;
wire [7:0]	scb_out_e, scr_out_e;
wire [7:0]	scb_out_e_rgt, scr_out_e_rgt;
wire		left_e2;
reg 		left_e;
reg 		left;
reg [1:0]	uvshf_d1, uvshf_d2, uvshf_d3, uvshf_rgt; 
reg [1:0]	yshf_d1, yshf_d2, yshf_d3, yshf_rgt; 
reg		ld_dbl_cntr_rgt;
reg		toggle_detected_d1, toggle_detected_d2, toggle_detected_d3, 
		toggle_detected_rgt;
reg 		incr_dbl_count_mask_rgt;
wire 		incr_dbl_count_rgt;
reg [3:0]       cur_scaler_phase_d1, cur_scaler_phase_d2, cur_scaler_phase_d3, cur_scaler_phase_rgt;
reg 		nxt_sy_sel_d1, nxt_sy_sel_d2, nxt_sy_sel_d3, nxt_sy_sel_rgt;
reg 		uvodd_d1, uvodd_d2, uvodd_d3, uvodd_rgt;
reg [2:0]	psel_d1, psel_d2;
wire 		hsyncb_detected;
reg		hsyncb_d1;
reg		last_double_persistant;
reg		vblank_d1;
wire		vblank_end_detected;
reg [8:0]	step_pst;

assign 	left_e2 = ~right;

// THIS IS WRONG FOR RIGHT
// i need to switch to old stuff for right y0,cb,cr and cur stuff for right y1
// this assumes cur stuff doesnt get incremented 

//assign psel_e2 = right ? old_coeff_select : (cur_scaler_phase == calc_cr) ? old_coeff_select : coeff_select;

// the fir_cntr shouldnt be toggling when on the right pixels, so y0,cr,cb right should
// use "old" values (i.e. values off the first increment on the left y0) and y1 right should use the
// current fir_cntr value (the value for the y1 left pixel)
always @ (right or cur_scaler_phase or coeff_select or old_coeff_select)
 begin
	if (right)
			// the psel_e2 for the left pixels (delayed by 4 clocks)
			psel_e2 = psel_d2;
	else
		if (cur_scaler_phase == calc_cr)
			psel_e2 = old_coeff_select;
		else
			psel_e2 = coeff_select;
 end

//assign uvodd_e2 = fir_cntr[8];
//assign uvodd_e2 = csel_cntr[8];


/*
csel: 	00 y
	01 u
	10 v
	11 n/a
*/

assign vblank_end_detected = ~vblank && vblank_d1;

// Align controls with data
// last_double_e1 occurs if the current chroma_shift shifts to the last
// chroma value, without checking for this condition since I change uvodd_e2 early
// wrt the chroma shift (i.e. uvodd_e2 is inverted ahead of time for the next chroma_shift)
// uvodd_e2 would mistakenly report

always @ (posedge vClk)
 begin
	if (~viResetb || ~hsyncb)
		uvodd_e2 <= 1'b0;
//	else if (chroma_shift && ~(last_double_persistant || last_double || last_double_e1))
	else if (chroma_shift)
		uvodd_e2 <= ~uvodd_e2;

        if (~viResetb)
         begin
                step_pst <= 9'b0;   
         end
        else if (vblank_end_detected)
         begin
                step_pst <= step;   
         end

	
	vblank_d1 <= vblank;
	frst_pxl_d1 <= frst_pxl;
	frst_pxl_d2 <= frst_pxl_d1;
	lst_pxl_d1 <= lst_pxl;
	lst_pxl_d2 <= lst_pxl_d1;
// ok so this is inelegant, but the bottom
// line is that the right will do the exact same
// things as the left, but 4 clocks (2pixels) later
	nxt_sy_sel_d1 <= nxt_sy_sel;
	nxt_sy_sel_d2 <= nxt_sy_sel_d1;
	nxt_sy_sel_d3 <= nxt_sy_sel_d2;
	nxt_sy_sel_rgt <= nxt_sy_sel_d3;
	uvshf_d1 <= uvshf;
	uvshf_d2 <= uvshf_d1;
	uvshf_d3 <= uvshf_d2;
	uvshf_rgt <= uvshf_d3;
	yshf_d1 <= yshf;
	yshf_d2 <= yshf_d1;
	yshf_d3 <= yshf_d2;
	yshf_rgt <= yshf_d3;
	toggle_detected_d1 <= toggle_detected;
	toggle_detected_d2 <= toggle_detected_d1;
	toggle_detected_d3 <= toggle_detected_d2;
	toggle_detected_rgt <= toggle_detected_d3;
	uvodd_d1 <= uvodd_e2;
	uvodd_d2 <= uvodd_d1;
	uvodd_d3 <= uvodd_d2;
	uvodd_rgt <= uvodd_d3;
	cur_scaler_phase_d1 <= cur_scaler_phase;
	cur_scaler_phase_d2 <= cur_scaler_phase_d1;
	cur_scaler_phase_d3 <= cur_scaler_phase_d2;
	cur_scaler_phase_rgt <= cur_scaler_phase_d3;
	psel_d1 <= psel;
	psel_d2 <= psel_d1;

 end



//always @ (cur_scaler_phase or calc_cr or calc_cb)
always @ (posedge vClk)
 begin
	left_e <= left_e2;
	left <= left_e;
	psel_e <= psel_e2;
	psel <= psel_e;
	csel <= csel_e;
	if (cur_scaler_phase == calc_cr)
		csel_e <= 2'b10;
	else if (cur_scaler_phase == calc_cb)
		csel_e <= 2'b01;
	else 	
		csel_e <= 2'b00;
 end


always @ (posedge vClk)
 begin

	dsp_en_d2 <= dsp_en_d1;

	// THIS IS UGLY, REAL UGLY
	 if (~viResetb || vi_reset || ~scaling)
		fir_cntr <= {1'b0, step_pst[8:1]};
	 else if (allow_shift0 || allow_shift1)
	  begin
		fir_cntr <= (fir_cntr + step_pst);
		if (allow_shift1)
			old_fir_cntr <= fir_cntr;
	  end

	// HOPEFULLY THIS SYNTHS TO 1 ADDER	
//	 if (~viResetb || vi_reset || ~scaling)
//	        old_fir_cntr <= {1'b0, step[8:1]};
//	 else if (allow_shift0)
//		old_fir_cntr <= (fir_cntr + step);
 end

// maybe i can get this to select old_fir_cntr and fir_cntr and then do add...
// or maybe i can just grab old versions of csel_cntr and coeff_select


assign old_csel_cntr = 144 - old_fir_cntr;
assign old_coeff_select = old_csel_cntr[7:5];

assign csel_cntr = (144 - fir_cntr);
assign toggle_cntr = fir_cntr + 111;
assign coeff_select = csel_cntr[7:5];
assign toggle_bit = toggle_cntr[8];

//// COULD MAKE THIS ONE ADDER
//assign coeff_select = fir_cntr[4] ? (fir_cntr[7:5]+1) : fir_cntr[7:5];
//assign old_coeff_select = old_fir_cntr[4] ? (old_fir_cntr[7:5]+1) : old_fir_cntr[7:5];

//assign toggle_bit = fir_cntr[8];
//// We don't want a toggle detected when the fir_cnt is being reset...
assign toggle_detected = ((toggle_bit ^ toggle_bit_d1) && scaling);


always @ (posedge vClk)
 begin
	toggle_bit_d1 <= toggle_bit;
	dsp_en_d1 <= dsp_en;
 end


//assign req_pix = (luma_shift || ~hscaler_en);
assign luma_shift = luma_ld || toggle_detected;
//assign chroma_shift = chroma_ld || (toggle_detected && ~uvodd);
// need to do this because if a toggle has occured on y1, we dont
// want the chroma values to shift until after calc_cr, otherwise
// cb and cr will be based on different pixels
//assign chroma_shift = (chroma_ld || (toggle_detected && cur_scaler_phase == calc_y0) 
//			|| (uvtoggle_pending && cur_scaler_phase == calc_y0));

// there is a chroma shift when there is a toggle or a toggle pending, if there are both
// then two shifts have to occur
assign chroma_shift = (chroma_ld || (toggle_detected && cur_scaler_phase == calc_y0) 
			|| (uvtoggle_pending && ((cur_scaler_phase == calc_cb) || (cur_scaler_phase == calc_y0))));

//always @ (luma_shift or lst_pxl_d2 or frst_pxl_d2)
always @ (posedge vClk)
 begin
	// Need this because this is the last source pixel to be used
	// filter needs to know to keep using this as the "next pixel"
	// instead of going past the source image size
	
	if (vi_reset || ~viResetb || ~scaling)
		lst_pxl_persistant <= 1'b0;
	else if (lst_pxl_d1)
		lst_pxl_persistant <= 1'b1;
	
	if (vi_reset || ~viResetb || ~scaling)
		last_double_persistant <= 1'b0;
	else if (last_double)
		last_double_persistant <= 1'b1;
	
	

	if (luma_shift)
		if (frst_pxl_d2)
			yshf <= 2'b01;
		else if (lst_pxl_persistant) 
			yshf <= 2'b11;
		else
			yshf <= 2'b10;
	else
		yshf <= 2'b00;

	if (chroma_shift)
		if (frst_pxl_d2)
			uvshf <= 2'b01;
                else if (lst_pxl_persistant)
                        uvshf <= 2'b11;
                else
                        uvshf <= 2'b10;
        else
                uvshf <= 2'b00;

//       if ((toggle_detected) && (cur_scaler_phase == calc_y1))
//                uvtoggle_pending <= 1'b1;
//        else if (cur_scaler_phase == calc_y0)
//                uvtoggle_pending <= 1'b0;
//       else if (~hsyncb)
//                uvtoggle_pending <= 1'b0;

// This is done to fix not shifting enough bug
// if toggle_pending & new toggle detected then it was only shifting once instead of twice
// this will fix that problem
	if ((toggle_detected) && (cur_scaler_phase == calc_y1))
		uvtoggle_pending <= 1'b1;
	else if (((cur_scaler_phase == calc_y0) && ~toggle_detected) ||  (cur_scaler_phase == calc_cb))
		uvtoggle_pending <= 1'b0;
	else if (~hsyncb)
		uvtoggle_pending <= 1'b0;


 end

/*
first_pixel ld  x  x  y0 y0 y0 y0
yshift 		x  y0 y0 y0 y0 y1
yshift		y0 y0 y0 y0 y1 y2

first_pixel ld	x   uv0 uv0 uv0
chrshft		uv0 uv0 uv0 uv2
*/

assign allow_shift0 = (nxt_scaler_phase == calc_y0) && ~right && (cur_scaler_phase != shift2); 
assign allow_shift1 = (nxt_scaler_phase == calc_y1) && ~right;

// When toggle is detected, then the nxt_double will be shifted over to the
// cur_double on the next clock, but for now nxt_double is the actual
// "current" double
//assign first_sy = toggle_detected ? nxt_double[31:24] : cur_double[31:24];
//assign second_sy = toggle_detected ? nxt_double[15:8] : cur_double[15:8];
//assign first_sy = ld_dbl_cntr ? nxt_double[31:24] : cur_double[31:24];
//assign second_sy = ld_dbl_cntr ? nxt_double[15:8] : cur_double[15:8];
//assign first_sy = (incr_dbl_count && ld_dbl_cntr) ? nxt_double[31:24] : cur_double[31:24];
//assign second_sy = (incr_dbl_count && ld_dbl_cntr) ? nxt_double[15:8] : cur_double[15:8];
//assign first_sy = cur_double[31:24];
//assign second_sy = cur_double[15:8];
//assign scb = toggle_detected ? nxt_double[23:16] : cur_double[23:16];
//assign scr = toggle_detected ? nxt_double[7:0] : cur_double[7:0];

assign first_sy = (incr_dbl_count) ? nxt_double_lft[31:24] : cur_double_lft[31:24];
assign second_sy = (incr_dbl_count) ? nxt_double_lft[15:8] : cur_double_lft[15:8];
assign scb = incr_dbl_count ? nxt_double_lft[23:16] : cur_double_lft[23:16];
assign scr = incr_dbl_count ? nxt_double_lft[7:0] : cur_double_lft[7:0];
assign oscb = cur_double_lft[23:16] + nxt_double_lft[23:16];
assign oscr = cur_double_lft[7:0] + nxt_double_lft[7:0];
assign scb_out_e = uvodd_e2 ? oscb[8:1] : scb;
assign scr_out_e = uvodd_e2 ? oscr[8:1] : scr;


assign first_sy_rgt = (incr_dbl_count_rgt) ? nxt_double_rgt[31:24] : cur_double_rgt[31:24];
assign second_sy_rgt = (incr_dbl_count_rgt) ? nxt_double_rgt[15:8] : cur_double_rgt[15:8];
assign scb_rgt = incr_dbl_count_rgt ? nxt_double_rgt[23:16] : cur_double_rgt[23:16];
assign scr_rgt = incr_dbl_count_rgt ? nxt_double_rgt[7:0] : cur_double_rgt[7:0];
assign oscb_rgt = cur_double_rgt[23:16] + nxt_double_rgt[23:16];
assign oscr_rgt = cur_double_rgt[7:0] + nxt_double_rgt[7:0];
assign scb_out_e_rgt = uvodd_rgt ? oscb_rgt[8:1] : scb_rgt;
assign scr_out_e_rgt = uvodd_rgt ? oscr_rgt[8:1] : scr_rgt;



// incr_dbl_count_mask forces the scaler to request more pixels
// only after two toggles 
// at any given time we have cur_double (y0, u0, y1, v0)
// and nxt_double (y2, u2, y3, v2)
// for pixel averaging for two pixels we only need 
// 1) y0,u0,v0
// 2) y1,(u0+u2)/2, (v0+v2)/2
// a pixel is needed for every toggle
// so 2 toggles = 2 pixels = 1 double

assign 	incr_dbl_count = ((ld_dbl_cntr || toggle_detected) &&  incr_dbl_count_mask && ~lst_pxl);
assign 	incr_dbl_count_rgt = ((ld_dbl_cntr_rgt || toggle_detected_rgt) &&  incr_dbl_count_mask_rgt && ~lst_pxl);



// This will walk across the doubles, 1) second_sy 2) first_sy 3)second_sy etc...
assign nxt_sy_w = nxt_sy_sel ? second_sy : first_sy;
assign nxt_sy_w_rgt = nxt_sy_sel_rgt ? second_sy_rgt : first_sy_rgt;
assign hsyncb_detected = hsyncb_d1 && ~hsyncb;

always @ (posedge vClk)
 begin
	hsyncb_d1 <= hsyncb;
	
	if (vi_reset || ~viResetb || ~dsp_en_d1)
		incr_dbl_count_mask <= 1'b0;
	else if (ld_dbl_cntr || toggle_detected)
		incr_dbl_count_mask <= ~incr_dbl_count_mask;

        if (vi_reset || ~viResetb || ~dsp_en_d1)
                incr_dbl_count_mask_rgt <= 1'b0;
        else if (ld_dbl_cntr_rgt || toggle_detected_rgt)
                incr_dbl_count_mask_rgt <= ~incr_dbl_count_mask_rgt;


//	if (vi_reset || ~viResetb || ~dsp_en_d1)
	if (vi_reset || ~viResetb || hsyncb_detected)
		nxt_sy_sel <= 1'b0;
	else if ((toggle_detected || nxt_state_shift) && ~last_double)
		nxt_sy_sel <= ~nxt_sy_sel;


	if (cur_scaler_phase == load)
	 begin
		sy_out <= first_sy; 	
		scb_out <= scb;		
		scr_out <= scr;		
		ld_dbl_cntr <=  1'b1;	
	 end
	else if (cur_scaler_phase == shift1)
	 begin
		sy_out <= second_sy;		
		scb_out <=  oscb[8:1];		
	        scr_out <= oscr[8:1];		
		ld_dbl_cntr <=  1'b1;
	 end
	else if (cur_scaler_phase == shift2)
	 begin
	        sy_out <= first_sy;		 
	        scb_out <=  scb;		
	        scr_out <= scr;			
		ld_dbl_cntr <=  1'b0;
	  end
	else 
	 begin
	      if (toggle_detected)
		begin
			sy_out <= nxt_sy_w;		
		end
		scb_out <= scb_out_e;			
		scr_out <= scr_out_e;			
		ld_dbl_cntr <=  1'b0;

	 end

	if (cur_scaler_phase_rgt == load)
	 begin
		sy_out_rgt <= first_sy_rgt;     
                scb_out_rgt <= scb_rgt;         
                scr_out_rgt <= scr_rgt;         

		ld_dbl_cntr_rgt <=  1'b1;	
	 end
	else if (cur_scaler_phase_rgt == shift1)
	 begin
		sy_out_rgt <= second_sy_rgt;		
		scb_out_rgt <=  oscb_rgt[8:1];		
	        scr_out_rgt <= oscr_rgt[8:1];		
		ld_dbl_cntr_rgt <=  1'b1;
	 end
	else if (cur_scaler_phase_rgt == shift2)
	 begin
	        sy_out_rgt <= first_sy_rgt;		 
	        scb_out_rgt <=  scb_rgt;		
	        scr_out_rgt <= scr_rgt;			
		ld_dbl_cntr_rgt <=  1'b0;
	  end
	else 
	 begin
	      if (toggle_detected_rgt)
		begin
			sy_out_rgt <= nxt_sy_w_rgt;		
		end
		scb_out_rgt <= scb_out_e_rgt;			
		scr_out_rgt <= scr_out_e_rgt;			
		ld_dbl_cntr_rgt <=  1'b0;

	 end
end 

always @ (cur_scaler_phase or dsp_en_d1)
	case (cur_scaler_phase)
	 sc_inactive:
		begin
		luma_ld = 1'b0;
		chroma_ld = 1'b0;
		scaling = 1'b0;
		if (dsp_en_d1)
		 begin
			nxt_state_shift = 1'b1;
			nxt_scaler_phase = load;
		 end
		else
		 begin
			nxt_state_shift = 1'b0;
			nxt_scaler_phase = sc_inactive;	
		 end
		end
	 load:
		begin
		scaling = 1'b1;
		luma_ld = 1'b1;
		chroma_ld = 1'b1;
		nxt_state_shift = 1'b1;
		nxt_scaler_phase = shift1;
		end
	 shift1:
		begin
		scaling = 1'b1;
		chroma_ld = 1'b1;
		luma_ld = 1'b1;
		nxt_state_shift = 1'b1;
		nxt_scaler_phase = shift2;
		end
	 shift2:
		begin
		scaling = 1'b1;
		chroma_ld = 1'b1;
		luma_ld = 1'b1;
		nxt_state_shift = 1'b0;
		nxt_scaler_phase = calc_y0;
		end
	 calc_y0 :
		begin
		scaling = 1'b1;
		chroma_ld = 1'b0;
		luma_ld = 1'b0;
		nxt_state_shift = 1'b0;
		nxt_scaler_phase = calc_cb;
		end
	 calc_cb:
		begin
		scaling = 1'b1;
		chroma_ld = 1'b0;
		luma_ld = 1'b0;
		nxt_state_shift = 1'b0;
		nxt_scaler_phase = calc_y1;	
		end
	 calc_y1 :
		begin
		scaling = 1'b1;
		chroma_ld = 1'b0;
		luma_ld = 1'b0;
		nxt_state_shift = 1'b0;
		 nxt_scaler_phase = calc_cr;
		end
	 calc_cr:
		begin
		scaling = 1'b1;
		chroma_ld = 1'b0;
		luma_ld = 1'b0;
		if (~dsp_en_d1)
		 begin
			nxt_state_shift = 1'b0;
			nxt_scaler_phase = sc_inactive;
		end
		else
		 begin
			nxt_scaler_phase = calc_y0;
			nxt_state_shift = 1'b0;
		 end
		end
	 default :
		begin
		scaling = 1'b0;
		chroma_ld = 1'b0;
		luma_ld = 1'b0;
		nxt_state_shift = 1'b0;
		nxt_scaler_phase = sc_inactive;
		end
	endcase

always @ (posedge vClk)
 begin

 if (~viResetb || vi_reset)
	cur_scaler_phase <= sc_inactive;
 else
	cur_scaler_phase <= nxt_scaler_phase;

 if ((cur_scaler_phase == sc_inactive) || (~viResetb) || (vi_reset))
		right <= 1'b0;
 else if ((cur_scaler_phase == calc_cr) && vi_dlr)
		right <= ~right;
	
end 
endmodule

