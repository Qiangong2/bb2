/*******************************************************************************
 * 
 *   (C) 1999 ARTX INC..  ALL RIGHTS RESERVED.  UNPUBLISHED -- RIGHTS
 *   RESERVED UNDER THE COPYRIGHT LAWS OF THE UNITED STATES.  USE OF A
 *   COPYRIGHT NOTICE IS PRECAUTIONARY ONLY AND DOES NOT IMPLY PUBLICATION
 *   OR DISCLOSURE.
 *
 *   THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF
 *   ARTX INC..  USE, DISCLOSURE, OR REPRODUCTION IS PROHIBITED WITHOUT
 *   THE PRIOR EXPRESS WRITTEN PERMISSION OF ARTX INC..
 *
 *                   RESTRICTED RIGHTS LEGEND
 *
 *   Use, duplication, or disclosure by the Government is subject to
 *   restrictions as set forth in subparagraph (c)(1)(ii) of the Rights
 *   in Technical Data and Computer Software clause at DFARS 252.227-7013
 *   or subparagraphs (c)(1) and (2) of Commercial Computer Software --
 *   Restricted Rights at 48 CFR 52.227-19, as applicable. 
 *
 *   ArtX Inc.
 *   3400 Hillview Ave, Bldg 5
 *   Palo Alto, CA 94304
 *
 ******************************************************************************/

/******************************************************************************
 * Revision History
 * 
 * Person	Date		Comments
 * jdm		3-19-99		Initial revision
 * ff           4-13-99         disposable io and pi port 
 * ds           7-12-99         hacks to use with exi only and back to reg array
 ******************************************************************************/

module mem ( /*AUTOARG*/
	     // Outputs
	     mem_ioAck, mem_ioData, mem_ioFlushWrAck, 
	     // Inputs
	     resetb, mem_clk, io_memAddr, io_memReq, io_memRd, io_memData, 
	     io_memFlushWrBuf
	     );
   
   // Misc inputs
   
   input        	resetb;
   input 		mem_clk;
   

   // Mem - Io interface
   
   output 		mem_ioAck;
   output [63:0] 	mem_ioData;
   output 		mem_ioFlushWrAck;
   
   input [25:5] 	io_memAddr;
   input 		io_memReq;
   input 		io_memRd;
   input [63:0] 	io_memData;
   input 		io_memFlushWrBuf;
   
`define STRING_SIZE 64     // In chars
`define MEMORY_SIZE 8192   // In 16-bit words

   reg [15:0] 		mem_array [`MEMORY_SIZE-1:0]; // Memory Array
   reg [`STRING_SIZE*8:1] mem_initfile;
   reg [`STRING_SIZE*8:1] mem_dumpfile;
   reg 			  pending_finish; // Flag that simulation is going to end, dump memory on end
   
   initial
     begin
	pending_finish = 1'b0;
	mem_initfile = "eximem.init";
	mem_dumpfile = "eximem.dump";
	$readmemh(mem_initfile,mem_array); // Load memory array
     end // initial begin
   
   // Dump memory when sim completes
   always @(posedge pending_finish)
     begin
	$writememh(mem_dumpfile,mem_array);
     end // always @ (posedge pending_finish)
   
   // IO interface :
   reg d1mem_wr_io,d2mem_wr_io,d3mem_wr_io,d4mem_wr_io;
   reg d1mem_rd_io,d2mem_rd_io,d3mem_rd_io,d4mem_rd_io;
   reg 	      d1mask0_io,d1mask1_io;
   reg [63:0] d1mem_wrdata_io;
   reg [25:5] d1mem_addr_io;
   reg [63:0] mem_ioData;
   reg 	      mem_ioAck;
   reg 	      d1io_memFlushWrBuf;
   reg 	      mem_ioFlushWrAck;
   wire [4:3] bit43_io;
   
   always @(posedge mem_clk) begin
      d1mem_wr_io     <= ~io_memRd & io_memReq;
      d1mem_rd_io     <=  io_memRd & io_memReq;
      d1mem_wrdata_io <= io_memData;
      d2mem_wr_io     <= d1mem_wr_io;
      d3mem_wr_io     <= d2mem_wr_io;
      d4mem_wr_io     <= d3mem_wr_io;
      d2mem_rd_io     <= d1mem_rd_io;
      d3mem_rd_io     <= d2mem_rd_io;
      d4mem_rd_io     <= d3mem_rd_io;
      
      if(io_memReq)
	d1mem_addr_io <= io_memAddr;
      else
	d1mem_addr_io <= d1mem_addr_io;
      
      if (d1mem_wr_io || d2mem_wr_io || d3mem_wr_io || d4mem_wr_io) begin
	 mem_array[{d1mem_addr_io,bit43_io,2'b10}] = d1mem_wrdata_io[31:16];
	 mem_array[{d1mem_addr_io,bit43_io,2'b11}] = d1mem_wrdata_io[15:00]; 
	 mem_array[{d1mem_addr_io,bit43_io,2'b00}] = d1mem_wrdata_io[63:48]; 
	 mem_array[{d1mem_addr_io,bit43_io,2'b01}] = d1mem_wrdata_io[47:32];
      end // if (d1mem_wr_io || d2mem_wr_io || d3mem_wr_io || d4mem_wr_io)
      
      mem_ioAck <=  (d1mem_wr_io | d1mem_rd_io) ;
      
      if (d1mem_rd_io || d2mem_rd_io || d3mem_rd_io || d4mem_rd_io) begin
	 mem_ioData[63:48] <= mem_array[{d1mem_addr_io,bit43_io,2'b00}];
	 mem_ioData[47:32] <= mem_array[{d1mem_addr_io,bit43_io,2'b01}];
	 mem_ioData[31:16] <= mem_array[{d1mem_addr_io,bit43_io,2'b10}];
	 mem_ioData[15:00] <= mem_array[{d1mem_addr_io,bit43_io,2'b11}];
      end else begin
	 mem_ioData <= 64'hxxxxxxxxxxxxxxxx;
      end
      
      d1io_memFlushWrBuf <=   io_memFlushWrBuf; 
      mem_ioFlushWrAck  <= d1io_memFlushWrBuf; 
   end
   
   assign bit43_io[3] = d2mem_wr_io | d4mem_wr_io | d2mem_rd_io | d4mem_rd_io;
   assign bit43_io[4] = d3mem_wr_io | d4mem_wr_io | d3mem_rd_io | d4mem_rd_io;
   
endmodule // mem

