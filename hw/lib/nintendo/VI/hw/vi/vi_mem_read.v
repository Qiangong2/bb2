///////////////////////////////////////////////////////////////////////////////
//
//   (C) 1999 ARTX INC..  ALL RIGHTS RESERVED.  UNPUBLISHED -- RIGHTS
//   RESERVED UNDER THE COPYRIGHT LAWS OF THE UNITED STATES.  USE OF A
//   COPYRIGHT NOTICE IS PRECAUTIONARY ONLY AND DOES NOT IMPLY PUBLICATION
//   OR DISCLOSURE.
//
//   THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF
//   ARTX INC..  USE, DISCLOSURE, OR REPRODUCTION IS PROHIBITED WITHOUT
//   THE PRIOR EXPRESS WRITTEN PERMISSION OF ARTX INC..
//
//                   RESTRICTED RIGHTS LEGEND
//
//   Use, duplication, or disclosure by the Government is subject to
//   restrictions as set forth in subparagraph (c)(1)(ii) of the Rights
//   in Technical Data and Computer Software clause at DFARS252.227-701
//   or subparagraphs (c)(1) and (2) of Commercial Computer Software --
//   Restricted Rights at 48 CFR 52.227-19, as applicable.
//
//   ArtX Inc.
//   3400 Hillview Ave, Bldg 5
//   Palo Alto, CA 94304
//
///////////////////////////////////////////////////////////////////////////////

// will have to delay all timing signals to have same delay as data
// will bring ALLOW_REQ and friends mess up after double registering?
// maybe easier to take 32 bit at a time rather than 16

module vi_mem_read (
	// Inputs from outside
	vClk, viResetb, 
	// Inputs from timing generators
	hsyncb, hblank, dsp_en, vdsp_en,
	vblank,
	// Inputs from config regs
	dlr, xoff, wpl, vi_reset, vi_enable,
	hscaler_en, vsyncb_detected, vi_srcwidth,
	// Inputs from Mem Write 
	data_quad, data_quad_rgt,
	// Inputs from Scaler
	incr_dbl_count,
	incr_dbl_count_rgt,
	// Outputs to Mem Write
	glogical_rptr,
	glogical_rptr_rgt,
	allow_req, allow_req_rgt,
	rd_ptr, rd_ptr_rgt,
	// Outputs to data path
	data_out,
	// Outputs to scaler
	cur_double_lft, nxt_double_lft,
	cur_double_rgt, nxt_double_rgt,
	lst_pxl,
	frst_pxl,
	last_double,
	last_double_persistant,
	toggle_detected,
	hbe_odd, 
	hbs_pst, hbe_pst, hlw,
	hscaler_en_pst,
	wpl_pst
);


input 		vblank;
input  	 	incr_dbl_count;
input  	 	incr_dbl_count_rgt;
input		vClk, viResetb;
input		hsyncb, hblank, dsp_en, vdsp_en;
input		dlr, vi_reset, vi_enable, hscaler_en;
input [6:0]	wpl;
input [3:0]	xoff;
input [63:0]	data_quad, data_quad_rgt;
input 		vsyncb_detected;
input 		toggle_detected;

output [1:0]	glogical_rptr, glogical_rptr_rgt;
output [2:0]	rd_ptr, rd_ptr_rgt;
output		allow_req, allow_req_rgt, lst_pxl, frst_pxl;
output [7:0] 	data_out;
output [31:0]	cur_double_lft, nxt_double_lft;
output [31:0]	cur_double_rgt, nxt_double_rgt;
output 		last_double;
output 		hscaler_en_pst;
input 		last_double_persistant;
input 		hbe_odd;
input [9:0]	hbs_pst, hbe_pst, hlw;
input [9:0]	vi_srcwidth;
output	[6:0]   wpl_pst;


reg [7:0]	data_out;
reg [2:0]	rd_ptr, rd_ptr_rgt;
reg [15:0]   	active_pixel_data;
reg [1:0]    	glogical_rptr, glogical_rptr_rgt;
reg [1:0]    	glogical_rptr_pre, glogical_rptr_rgt_pre;
reg 		allow_req, allow_req_rgt;
reg 		vblank_d1;
wire [31:0]	nxt_double_lft_pre, nxt_double_rgt_pre;

reg 		hscaler_en_pst;
reg [3:0]	xoff_pst;
reg [6:0]	wpl_pst;
reg		first_pixel;
wire [1:0]	quad_select;
wire		incr_rd_ptr, incr_rd_ptr_rgt, xoff_zero;
wire		right_read;
reg [15:0]	pixel_data;
wire 		hblank_detected, active_hsync_detected;
reg		hsyncb_d1, hblank_d1, dsp_en_d1;
reg [63:0]	cur_quad, cur_quad_rgt;
wire [63:0]	nxt_quad, nxt_quad_rgt;
reg [1:0]	quad_count, quad_count_rgt;
reg [6:0]	word_count, word_count_rgt;
reg [1:0]	logical_rptr, logical_rptr_rgt;
wire 		last_word, last_word_rgt, last_quad, last_quad_rgt; 
reg 		last_pixel, last_double;
reg 		last_scaled_pixel;
wire [7:0]	y0, y1, y0_rgt, y1_rgt;
wire [8:0]	cb, cr, cb_rgt, cr_rgt;
wire [7:0]	nxt_y0, nxt_cb, nxt_y1, nxt_cr, nxt_y0_rgt, nxt_cb_rgt, 
		nxt_y1_rgt, nxt_cr_rgt;
wire [7:0]	cur_y0, cur_cb, cur_y1, cur_cr, cur_y0_rgt, cur_cb_rgt, 
		cur_y1_rgt, cur_cr_rgt;
wire [7:0] 	y0_out, y1_out, cb_out, cr_out;
wire [31:0]	cur_double_lft, nxt_double_lft, cur_double_rgt, nxt_double_rgt;
wire [7:0]	y0_pre, y1_pre, cb_pre, cr_pre;
wire 		vblank_detected;
wire 		hsyncb_detected;

reg [5:0] 	rd_pix_count;
reg [2:0]	pixel_select, pixel_select_last, pixel_select_rgt, pixel_select_rgt_last;
reg [2:0]	r_l_cntr;
reg 		dbl_count, dbl_count_rgt;	
wire [1:0]	pixel_select_out;
wire [7:0]	cb_crpre, cr_cbpre;
wire [63:0]	cbcr_cur_quad_rgt, cbcr_cur_quad, cbcr_nxt_quad_rgt, cbcr_nxt_quad;
wire [9:0] 	line_len;
reg [9:0] 	dst_line_len, vi_srcwidth_pst;
reg [3:0] 	last_word_pix;
wire 		good_len;
wire [3:0]	last_word_pix_plus_one;
wire [6:0] 	wpl_pst_m_2;
reg [3:0]		xoff_inv;
//assign 		last_double_e1 = incr_dbl_count && ~dbl_count && last_quad;

//assign last_word = xoff_zero ? (word_count == (wpl_pst - 1)) : (word_count == wpl_pst);
//assign last_word_rgt = xoff_zero ? (word_count_rgt == (wpl_pst - 1)) : (word_count_rgt == wpl_pst);
assign last_word = (word_count == (wpl_pst - 1));
assign last_word_rgt = (word_count_rgt == (wpl_pst-1));

wire 		incr_dbl_ptr, incr_dbl_ptr_rgt;

// increment fifo read ptr when 16 pixels have been read
// one pixel is read every two clocks


// maybe all these counters are not necessary



// xoffset chooses which quad (64 bit fifo entry) the first pixel for every field will
// be in. This assumes that for every hsync the read and write pointers are reset
// to 0. i.e. the first word fetched will be in fifo locations 0-3 

assign quad_select = xoff_pst[3:2];

// ********************************************
// Should the rd_ptr be going regardless of wr?
// ********************************************

assign frst_pxl = first_pixel && dsp_en;
assign lst_pxl = last_pixel;


assign nxt_quad = data_quad;
assign nxt_quad_rgt = data_quad_rgt;
assign vblank_detected = vblank_d1 && ~vblank; 

always @ (posedge vClk)
 begin

	vblank_d1 <= vblank;
 	pixel_select_last <= pixel_select;
// 	pixel_select_rgt_last <= pixel_rgt_select;
	
	if (~viResetb)
	 begin
		wpl_pst <= 7'b0;
		xoff_pst <= 4'b0;
		vi_srcwidth_pst <= 10'b0;
		hscaler_en_pst <= 1'b0;
	 end
	else if (vblank_detected)
	 begin
		wpl_pst <= wpl;
		xoff_pst <= xoff;
		vi_srcwidth_pst <= vi_srcwidth;
		hscaler_en_pst <=  hscaler_en;
	 end
 end

always @ (posedge vClk)
 if (~viResetb || ~dsp_en_d1 || ~dlr || vi_reset)
	r_l_cntr <= 3'b0;
 else
	r_l_cntr <= r_l_cntr + 1;

//assign left = ~r_l_cntr[2];
assign right_read = r_l_cntr[2];
assign xoff_zero = &(~xoff_pst);

/*

	if (line_len[2:0] == 3'b0)
		old way
	else
		new way


new way:
	line_len1 = line_len + 1;
	last_quad =  quad_count == last_word_pix[3:2];
	last_double = hscaler_en && (dbl_count == ~last_word_pix_plus_one[1])  || 
			~hscaler_en && (pixel_select[2]==~last_word_pix[1];
	last_pixel = ~hscaler_en && pixel_select[1] == last_word_pix[0];
	

*/

//assign last_quad = (last_word && (xoff_zero ? (&quad_count) : (quad_count == xoff_pst[3:2])));
wire [3:0] xoff_d1 = (xoff_pst - 2);
//assign last_quad = (quad_count == (xoff[3:2] - 1));
assign last_quad = good_len ? ((quad_count == xoff_d1[3:2]) && last_word) : (last_word && (quad_count==last_word_pix[3:2]));
assign last_quad_rgt = good_len ? ((quad_count_rgt == xoff_d1) && last_word_rgt) : 
			(last_word_rgt && (quad_count_rgt==last_word_pix[3:2]));
// xoff : 	xx00 pixel_select for last_double = 1xx
//		xx01 pixel_select for last_double = 0xx
//		xx10 pixel_select for last_double = 0xx
//		xx11 pixel_select for last_double = 1xx

always @ (posedge vClk)
 begin
	dst_line_len <= (hbs_pst + hlw - hbe_pst);
	xoff_inv <= ~xoff_pst +1;
	last_word_pix <= (line_len - {wpl_pst_m_2, 4'b0} - {6'b0, xoff_inv} - 1'b1);
 end


assign line_len = hscaler_en_pst ? vi_srcwidth_pst : dst_line_len; 
assign good_len = (line_len[2:0] == 3'b0);
assign last_word_pix_plus_one = last_word_pix + 1;
assign wpl_pst_m_2 = wpl_pst - 2;

always @ (hscaler_en_pst or last_quad or dbl_count or pixel_select or xoff_pst or good_len or last_quad or last_word_pix_plus_one
		or last_word_pix)
 begin
	if (good_len)
	 begin
		if (hscaler_en_pst)
			last_double = last_quad && (dbl_count == ~xoff_pst[1]);
		else
			last_double = last_quad && (pixel_select[2] == (xoff_pst[0] ~^ xoff_pst[1]));
	 end
	else
	 begin
		if (hscaler_en_pst)
			last_double = last_quad && (dbl_count == ~last_word_pix_plus_one[1]);
		else
		   	last_double = last_quad && (pixel_select[2] == last_word_pix[1]);
	 end
 end

always @ (hscaler_en_pst or last_scaled_pixel or last_double or pixel_select or last_word_pix or good_len)
 begin
	if (good_len)
         begin
		if (hscaler_en_pst)
			last_pixel = last_scaled_pixel;
		else
			last_pixel = last_double && (pixel_select[1] == ~xoff_pst[0]);
	 end
	else
	 begin
		if (hscaler_en_pst)
			last_pixel = last_scaled_pixel;
		else
			last_pixel = last_double && (pixel_select[1] == last_word_pix[0]);
	 end
 end

always @ (posedge vClk)
 begin
	if (vi_reset || ~viResetb || ~last_double)
		last_scaled_pixel <= 1'b0;
	else if (toggle_detected)
		last_scaled_pixel <= 1'b1;
 end


assign cbcr_cur_quad = hbe_odd ? {cur_quad[63:56], cur_quad[39:32], cur_quad[47:40], cur_quad[55:48],
				 cur_quad[31:24], cur_quad[7:0],  cur_quad[15:8], cur_quad[23:16]} :
				cur_quad;
assign cbcr_cur_quad_rgt = hbe_odd ? {cur_quad_rgt[63:56], cur_quad_rgt[39:32], cur_quad_rgt[47:40], cur_quad_rgt[55:48],
				 cur_quad_rgt[31:24], cur_quad_rgt[7:0],  cur_quad_rgt[15:8], cur_quad_rgt[23:16]} :
				cur_quad_rgt;
assign cbcr_nxt_quad = hbe_odd ? {nxt_quad[63:56], nxt_quad[39:32], nxt_quad[47:40], nxt_quad[55:48],
                                 nxt_quad[31:24], nxt_quad[7:0],  nxt_quad[15:8], nxt_quad[23:16]} :
                                nxt_quad;
assign cbcr_nxt_quad_rgt = hbe_odd ? {nxt_quad_rgt[63:56], nxt_quad_rgt[39:32], nxt_quad_rgt[47:40], nxt_quad_rgt[55:48],
                                 nxt_quad_rgt[31:24], nxt_quad_rgt[7:0],  nxt_quad_rgt[15:8], nxt_quad_rgt[23:16]} :
                                nxt_quad_rgt;

assign cur_double_lft = dbl_count ? cbcr_cur_quad[31:0] : cbcr_cur_quad[63:32];
assign nxt_double_lft_pre = dbl_count ? cbcr_nxt_quad[63:32] : cbcr_cur_quad[31:0];
assign cur_double_rgt = dbl_count_rgt ? cbcr_cur_quad_rgt[31:0] : cbcr_cur_quad_rgt[63:32];
assign nxt_double_rgt_pre = dbl_count_rgt ? cbcr_nxt_quad_rgt[63:32] : cbcr_cur_quad_rgt[31:0];
assign nxt_double_lft = (last_double_persistant || last_double) ? cur_double_lft : nxt_double_lft_pre;
assign nxt_double_rgt = (last_double_persistant || last_double) ? cur_double_rgt : nxt_double_rgt_pre;



/*
assign cur_double_lft = dbl_count ? cur_quad[31:0] : cur_quad[63:32];
assign nxt_double_lft = (dbl_count && ~(last_double_persistant || last_double)) ? nxt_quad[63:32] : cur_quad[31:0];
assign cur_double_rgt = dbl_count_rgt ? cur_quad_rgt[31:0] : cur_quad_rgt[63:32];
assign nxt_double_rgt = (dbl_count_rgt && ~(last_double_persistant || last_double)) ? nxt_quad_rgt[63:32] : cur_quad_rgt[31:0];
//assign cur_double = right_read ? cur_double_rgt : cur_double_lft;
//assign nxt_double = right_read ? nxt_double_rgt : nxt_double_lft;
*/

assign y0 = pixel_select[2] ? cur_quad[31:24] : cur_quad[63:56];
assign y1 = pixel_select[2] ? cur_quad[15:8] : cur_quad[47:40];
assign cur_cb = pixel_select[2] ? cur_quad[23:16] : cur_quad[55:48];
assign nxt_cb = last_double ? cur_cb : pixel_select[2] ? nxt_quad[55:48] : cur_quad[23:16];
assign cur_cr = pixel_select[2] ? cur_quad[7:0] : cur_quad[39:32];
assign nxt_cr = last_double ? cur_cr : pixel_select[2] ? nxt_quad[39:32] : cur_quad[7:0];
// THIS MIGHT SYNTHESIZE AN EXTRA ADDER
assign cb = xoff_pst[0] ? (cur_cb + nxt_cb) : {cur_cb, 1'b0};
assign cr = xoff_pst[0] ? (cur_cr + nxt_cr) : {cur_cr, 1'b0};


assign y0_rgt = pixel_select_rgt[2] ? cur_quad_rgt[31:24] : cur_quad_rgt[63:56];
assign y1_rgt = pixel_select_rgt[2] ? cur_quad_rgt[15:8] : cur_quad_rgt[47:40];
assign cur_cb_rgt = pixel_select_rgt[2] ? cur_quad_rgt[23:16] : cur_quad_rgt[55:48];
assign nxt_cb_rgt = pixel_select_rgt[2] ? nxt_quad_rgt[55:48] : cur_quad_rgt[23:16];
assign cur_cr_rgt = pixel_select_rgt[2] ? cur_quad_rgt[7:0] : cur_quad_rgt[39:32];
assign nxt_cr_rgt = pixel_select_rgt[2] ? nxt_quad_rgt[39:32] : cur_quad_rgt[7:0];
// THIS MIGHT SYNTHESIZE AN EXTRA ADDER
assign cb_rgt = xoff_pst[0] ? (cur_cb_rgt + nxt_cb_rgt) : {cur_cb_rgt, 1'b0};
assign cr_rgt = xoff_pst[0] ? (cur_cr_rgt + nxt_cr_rgt) : {cur_cr_rgt, 1'b0};


//always @ (posedge vClk)
// 	if (pixel_select[0])
//		sdata_out <= {y1_out, cb_out[7:0], cr_out[7:0]};
//	else
//		sdata_out <= {y0_out, cb_out[7:0], cr_out[7:0]};


assign y0_pre = right_read ? y0_rgt : y0;
assign y1_pre = right_read ? y1_rgt : y1;
assign cb_pre[7:0] = right_read ? cb_rgt[8:1] : cb[8:1];
assign cr_pre[7:0] = right_read ? cr_rgt[8:1] : cr[8:1];


assign cb_crpre = hbe_odd ? cr_pre : cb_pre;
assign cr_cbpre = hbe_odd ? cb_pre : cr_pre;

// clamp y [16,235] chroma[16,240]
assign y0_out = (y0_pre < 8'h10) ? 8'h10 : ((y0_pre > 8'heb) ? 8'heb : y0_pre);
assign y1_out = (y1_pre < 8'h10) ? 8'h10 : ((y1_pre > 8'heb) ? 8'heb : y1_pre);
assign cb_out = (cb_crpre < 8'h10) ? 8'h10 : ((cb_crpre > 8'hf0) ? 8'hf0 : cb_crpre);
assign cr_out = (cr_cbpre < 8'h10) ? 8'h10 : ((cr_cbpre > 8'hf0) ? 8'hf0 : cr_cbpre);



/*
// clamp y [16,235] chroma[16,240]
assign y0_out = (y0_pre < 8'h10) ? 8'h10 : ((y0_pre > 8'heb) ? 8'heb : y0_pre);
assign y1_out = (y1_pre < 8'h10) ? 8'h10 : ((y1_pre > 8'heb) ? 8'heb : y1_pre);
assign cb_out = (cb_pre < 8'h10) ? 8'h10 : ((cb_pre > 8'hf0) ? 8'hf0 : cb_pre);
assign cr_out = (cr_pre < 8'h10) ? 8'h10 : ((cr_pre > 8'hf0) ? 8'hf0 : cr_pre);
*/

assign pixel_select_out = right_read ? pixel_select_rgt[1:0] : pixel_select[1:0];


always @ (posedge vClk)
	case (pixel_select_out)
		2'b00:	data_out <= xoff_pst[0] ? y1_out : y0_out;
		2'b01:	data_out <= xoff_pst[0] ? cr_out[7:0] : cb_out[7:0];
		2'b10:	data_out <= xoff_pst[0] ? y0_out : y1_out;
		2'b11:	data_out <= xoff_pst[0] ? cb_out[7:0] : cr_out[7:0];
	endcase
			


always @ (posedge vClk)
 begin
 	hsyncb_d1 <= hsyncb;
	hblank_d1 <= hblank;
 end

assign hblank_detected = hblank && ~hblank_d1;

assign	active_hsync_detected = ~hsyncb_d1 && hsyncb && vdsp_en;
assign hsyncb_detected = ~hsyncb && hsyncb_d1;



// it takes one extra cycle from rd_ptr to actual data
// old && (~&pixel_select_last) was added becasue of scaling, now will use doubl_cntr for scaling
//assign incr_rd_ptr = ((&pixel_select) && dsp_en && ~right_read && (~&pixel_select_last) 
//			&& (~last_quad)) || (first_pixel && dsp_en);

// pixel_select should not be incremented when Hscaler is enabled
// || first_quad in line,

// we want to ask for a new quad (64 bits = 4 pixels) only after
// the two doubles have been used of the current quad
// double_count=0 means on first of two doubles in quad
// double_count=1 means on second of two doubles in quad
assign incr_dbl_ptr = incr_dbl_count && dbl_count;
assign incr_dbl_ptr_rgt = incr_dbl_count_rgt && dbl_count_rgt;

// when the scaler is ON we use incr_dbl_ptr to ask for new quads
// when scaler is OFF we use pixel_select to ask for new quads
// NOTE: when scaler is on we are using incr_dbl_ptr for both left and right
//	therefore they will both read data from their respective fifos at the
//	same time
//       all this should mean is that we are getting the right pixel data early
//	this shouldnt mess anything up, but we should make sure that we can do
//	a 3d scale with the xoff=15 just in case
/*
assign incr_rd_ptr = ( ( (&pixel_select && ~hscaler_en) || (incr_dbl_ptr && hscaler_en)) 
			&& dsp_en && ~right_read && (~last_quad)) || (first_pixel && dsp_en);
assign incr_rd_ptr_rgt = (((&pixel_select_rgt && ~hscaler_en) || (incr_dbl_ptr_rgt && hscaler_en)) 
			&& dsp_en && right_read && (~last_quad_rgt)) || (first_pixel && dsp_en);
*/
// I should be able to take right_read out
assign incr_rd_ptr = ( ( (&pixel_select && ~hscaler_en_pst && ~right_read) || (incr_dbl_ptr && hscaler_en_pst)) 
			&& dsp_en && (~last_quad)) || (first_pixel && dsp_en);
assign incr_rd_ptr_rgt = (((&pixel_select_rgt && ~hscaler_en_pst && right_read) || (incr_dbl_ptr_rgt && hscaler_en_pst)) 
			&& dsp_en && (~last_quad_rgt)) || (first_pixel && dsp_en);


always @ (posedge vClk)
	if (~viResetb || vi_reset || dsp_en)
		first_pixel <= 1'b0;
	else if (~hsyncb)
		first_pixel <= 1'b1;


always @ (posedge vClk)
 begin

/* Below is nol longer true since 2 clks per pixel  */
// the read pointer will need to be incremented every 16 pixels
// (hpixel_count = 4'b1111) but if the it is the first pixel of the
// field AND the xoffset is 4'hF (meaning there is only one valid pixel
// in the first fetched word) then 
// (first_pixel && (&xoff) && dsp_en) because dsp_en & first_pixel are active
// for only one pixel (the first pixel)  
	
	dsp_en_d1 <= dsp_en;

	if (incr_rd_ptr)
 		cur_quad <= data_quad;
	if (incr_rd_ptr_rgt)
 		cur_quad_rgt <= data_quad_rgt;

	if ((~ viResetb) || hblank_detected || vi_reset)
		rd_ptr <= 3'b0;
	else if (first_pixel && ~dsp_en)
		rd_ptr <= {1'b0,quad_select};
	else if (incr_rd_ptr)
		rd_ptr <= rd_ptr + 1;

	if ((~viResetb) || hblank_detected || vi_reset)
		rd_ptr_rgt <= 3'b0;
	else if (first_pixel && ~dsp_en)
		rd_ptr_rgt <= {1'b0,quad_select};
	else if (incr_rd_ptr_rgt)
		rd_ptr_rgt <= rd_ptr_rgt + 1;

// dsp_en_d1 is used since dsp_en is used to increment the pointer 
// and it takes one clk cycle to get out the data 
	if (~viResetb || hblank_detected || vi_reset)
                pixel_select <= 3'b0;
        else if (first_pixel && (~dsp_en))
                pixel_select <= {xoff_pst[1:0], 1'b0};
//        else if (~right_read && req_pix && (~last_pixel) && dsp_en_d1)
        else if (~right_read && dsp_en_d1)
                pixel_select <= pixel_select + 1;

	if (~viResetb || hblank_detected || vi_reset)
                pixel_select_rgt <= 3'b0;
        else if (first_pixel && (~dsp_en))
                pixel_select_rgt <= {xoff_pst[1:0], 1'b0};
//        else if (dsp_en_d1 && req_pix && right_read)
        else if (dsp_en_d1 && right_read)
                pixel_select_rgt <= pixel_select_rgt + 1;

//	if ((~viResetb) || hblank_detected || vi_reset)
	if ((~viResetb) || hsyncb_detected || vi_reset)
		quad_count <= 2'b0;
// The first quad incr_rd_ptr doesnt increment the read pointer because
// we have a one quad buffer between the output of the pixel fifo (data_quad)
// and the actual pixel stream (cur_quad)
	else if (first_pixel)
		quad_count <= xoff_pst[3:2];
	else if (incr_rd_ptr)
		quad_count <= quad_count + 1;

	if ((~viResetb) || hsyncb_detected || vi_reset)
		dbl_count <= 1'b0;
//	else if (first_pixel && ~dsp_en)
	else if (first_pixel)
		dbl_count <= xoff_pst[1];
	else if (incr_dbl_count && ~last_double)
		dbl_count <= ~dbl_count;

//	if ((~viResetb) || hblank_detected || vi_reset)
	if ((~viResetb) || hsyncb_detected || vi_reset)
		dbl_count_rgt <= 1'b0;
//	else if (first_pixel && ~dsp_en)
	else if (first_pixel)
		dbl_count_rgt <= xoff_pst[1];
	else if (incr_dbl_count_rgt)
		dbl_count_rgt <= ~dbl_count_rgt;


//	if (~hsyncb || hblank_detected || vi_reset)
	if ((~viResetb) || hblank_detected || vi_reset)
//	if ((~viResetb) || hsyncb_detected || vi_reset)
 		word_count <= 7'b0;
	else if (&quad_count && incr_rd_ptr && ~first_pixel)
		if (last_word)
			word_count <= 7'b0;
		else
			word_count <= word_count + 1;


        if (hblank_detected || (~ viResetb) || vi_reset)   
		logical_rptr <= 2'b0;
//	else if (&quad_count && incr_rd_ptr)
	else if ((rd_ptr[1:0] == 2'b11) && incr_rd_ptr)
		logical_rptr <= logical_rptr + 1;


//	if ((~viResetb) || hblank_detected || vi_reset)
	if ((~viResetb) || hsyncb_detected || vi_reset)
                quad_count_rgt <= 2'b0;
//        else if (first_pixel && ~dsp_en)
        else if (first_pixel)
                quad_count_rgt <= xoff_pst[3:2];
        else if (incr_rd_ptr_rgt)
                quad_count_rgt <= quad_count_rgt + 1;

	if (hblank_detected || (~viResetb) || vi_reset)
//	if (hsyncb_detected || (~viResetb) || vi_reset)
 		word_count_rgt <= 7'b0;
	else if (&quad_count_rgt && incr_rd_ptr_rgt && ~first_pixel)
		if (last_word_rgt)
			word_count_rgt <= 7'b0;
		else
			word_count_rgt <= word_count_rgt + 1;

        if (hblank_detected || (~viResetb) || vi_reset)   
		logical_rptr_rgt <= 2'b0;
//	else if (&quad_count_rgt && incr_rd_ptr_rgt)
	else if ((rd_ptr_rgt[1:0] == 2'b11) && incr_rd_ptr_rgt)
		logical_rptr_rgt <= logical_rptr_rgt + 1;

end

always @ (logical_rptr)
        case (logical_rptr)
                2'b00 :         glogical_rptr_pre = 2'b00;
                2'b01 :         glogical_rptr_pre = 2'b01;
                2'b10 :         glogical_rptr_pre = 2'b11;
                2'b11 :         glogical_rptr_pre = 2'b10;
        endcase


// THERE MIGHT BE A BETTER WAY TO DO THIS
always @ (logical_rptr_rgt)
        case (logical_rptr_rgt)
                2'b00 :         glogical_rptr_rgt_pre = 2'b00;
                2'b01 :         glogical_rptr_rgt_pre = 2'b01;
                2'b10 :         glogical_rptr_rgt_pre = 2'b11;
                2'b11 :         glogical_rptr_rgt_pre = 2'b10;
        endcase

always @ (posedge vClk)
 begin
	glogical_rptr <= glogical_rptr_pre;
	glogical_rptr_rgt <= glogical_rptr_rgt_pre;

 end


always @ (posedge vClk)
	if ((~viResetb) || last_word || vi_reset)
		allow_req <= 1'b0;
	else if (active_hsync_detected && vi_enable)
		allow_req <= 1'b1;

always @ (posedge vClk)
	if ((~viResetb) || last_word_rgt || vi_reset)
		allow_req_rgt <= 1'b0;
	else if (active_hsync_detected && vi_enable)
		allow_req_rgt <= 1'b1;



endmodule

