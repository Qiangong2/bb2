///////////////////////////////////////////////////////////////////////////////
//
//   (C) 1999 ARTX INC..  ALL RIGHTS RESERVED.  UNPUBLISHED -- RIGHTS
//   RESERVED UNDER THE COPYRIGHT LAWS OF THE UNITED STATES.  USE OF A
//   COPYRIGHT NOTICE IS PRECAUTIONARY ONLY AND DOES NOT IMPLY PUBLICATION
//   OR DISCLOSURE.
//
//   THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF
//   ARTX INC..  USE, DISCLOSURE, OR REPRODUCTION IS PROHIBITED WITHOUT
//   THE PRIOR EXPRESS WRITTEN PERMISSION OF ARTX INC..
//
//                   RESTRICTED RIGHTS LEGEND
//
//   Use, duplication, or disclosure by the Government is subject to
//   restrictions as set forth in subparagraph (c)(1)(ii) of the Rights
//   in Technical Data and Computer Software clause at DFARS 252.227-7013
//   or subparagraphs (c)(1) and (2) of Commercial Computer Software --
//   Restricted Rights at 48 CFR 52.227-19, as applicable.
//
//   ArtX Inc.
//   3400 Hillview Ave, Bldg 5
//   Palo Alto, CA 94304
//
///////////////////////////////////////////////////////////////////////////////


module vi_mem_write (
	// Inputs from outside
	viResetb_gfx, gfxClk, vClk,
	// Inputs from Mem
	mem_vi_ack, mem_vi_data, 
	// Inputs from config regs
	fbb_lft, fbb_rgt, high_addr, 
	std, dlr, vi_reset,
	fbb_lft_bot, fbb_rgt_bot,
	vi_nintl,
	// Inputs from timing 
	vsyncb_d1, hblank, dsp_en_d1, field,
	top_pic_sel, vblank_d1, hsyncb, wpl_pst,
	// Inputs from Read side
	rd_ptr, rd_ptr_rgt, allow_req, allow_req_rgt,
	glogical_rptr, glogical_rptr_rgt,
	// Outputs to Read Side
	data_quad, data_quad_rgt,
	// Outputs to Mem
	vi_mem_addr, vi_mem_req  
	);
`include "vi_state.v"
`include "vi_reg.v"
//`include "Mem1R1W8x64.v"

input 		gfxClk, viResetb_gfx, vClk;
input		mem_vi_ack;			// Mem sends acknowledgement along with data
input [63:0]	mem_vi_data;			// Data from Mem
input [23:0]	fbb_lft;			// Base Address of external frame buffer
input [23:0]	fbb_rgt;			// Base Address for the right picture in 3d mode
input [23:0]	fbb_lft_bot;			
input [23:0]	fbb_rgt_bot;			
input [7:0]	std;				// Stride (in words)
input 		dlr;				// 1 if 3d mode
input 		high_addr;			// 
input 		hsyncb;
input [6:0]	wpl_pst;
input 		vi_reset, vi_nintl;
input		vsyncb_d1, hblank, dsp_en_d1, field;
input [2:0]	rd_ptr, rd_ptr_rgt;
input 		allow_req, allow_req_rgt;
input 		top_pic_sel;
input [1:0]	glogical_rptr, glogical_rptr_rgt;
input 		vblank_d1;
output [63:0]	data_quad, data_quad_rgt;
output [25:5] 	vi_mem_addr;			// VI sends address to Mem
output 		vi_mem_req;			// VI sends a request for data to Mem

reg [25:5]   	vi_mem_addr;  
reg          	vi_mem_req;
reg 		mem_vi_ack_d1, gfx_hblank_d1, mem_vi_ack_d2;
wire		valid_ack;
reg [63:0] 	mem_vi_data_d1;
reg [2:0]	cur_req_state, next_req_state;
reg [3:0]	current_state, next_state;

wire 		gfx_allow_req, gfx_allow_req_rgt;
wire		wen, wen_rgt;
reg [2:0]	wr_ptr, wr_ptr_rgt;
wire		gfx_vsyncb, gfx_hblank,
		gfx_dsp_en; 
reg		gfx_dsp_en_d1;
reg [1:0]       logical_wptr, logical_wptr_rgt, glogical_rptr_d1, glogical_rptr_d2,
                glogical_rptr_rgt_d1, glogical_rptr_rgt_d2;
wire 		fifo_full, fifo_full_rgt;
reg		gfx_vsyncb_d1;
reg		right, req_rdy, req_pending;
wire 		mem_req_rgt, valid_req, mem_req;
reg [8:0]	line_count;
reg [25:0]	req_addr_rgt, req_addr;
wire		active_hblank_detected, hblank_detected;
wire 		gfx_vsyncb_detected;
wire [23:0]	next_req_addr, next_req_addr_rgt;
wire [25:0]     addr_rdy;
reg [25:0]	tmp_addr_rgt, tmp_addr;
reg [25:0]	base_addr_rgt, base_addr;
reg [5:0]	word_count, word_count_rgt;
reg		req_rdy_d1;
wire 		field_sel;
wire 		gfx_field;
wire [23:0]	cur_fbb_lft, cur_fbb_rgt;
reg [25:0]	line_addr;
reg [5:0]	word_count_d1, word_count_rgt_d1;
wire [25:0]	line_addr_tmp;
reg 		wen_right;
wire 		gfx_vblank, gfx_vblank_end_detected;
wire 		gfx_hsyncb, gfx_hsyncb_detected;
reg		gfx_hsyncb_d1;
reg [6:0]	req_count;
wire 		req_detected;
reg 		gfx_vblank_d1;
reg [7:0] 	std_pst;
parameter	s0 =`S0, s1 =`S1, s2 =`S2, s3 =`S3, s4 =`S4, 
		req_idle =`REQ_IDLE, send_req = `SEND_REQ, 
		wait_for_ack =`WAIT_FOR_ACK, got_ack = `GOT_ACK;
		

// Register Inputs 
always @ (posedge gfxClk)
 begin
	mem_vi_ack_d1 <= mem_vi_ack;
	mem_vi_ack_d2 <= mem_vi_ack_d1;
	mem_vi_data_d1 <= mem_vi_data;
 end


// Pixel data from the external frame buffer should be arriving
// from memory 256 bits per ACK in 4 64 bit increments
 
// State machine creates a a four cycle write enable (WEN) derived from
// the ACK 

// A valid ACK is one that comes in when the VI is expecting it
// (after it has sent out a REQ). This also handles the problem if I get 
// reset after sending a REQ but before receiving the ACK. I will drop the 
// next ACK, assuming that the vi_reset is long enough that I dont send
// out another REQ before getting the first ACK



assign valid_ack = mem_vi_ack_d1 && (cur_req_state == wait_for_ack);

always @ (posedge gfxClk)
 begin
   if (hblank_detected || vi_reset || ~viResetb_gfx)
	word_count <= 6'b0;
   else if (valid_ack && ~right)
	word_count <= word_count + 1;

   if (hblank_detected || vi_reset || ~viResetb_gfx)
	word_count_rgt <= 6'b0;
   else if (valid_ack && right)
	word_count_rgt <= word_count_rgt + 1;
 end

always @ (current_state or valid_ack)
 begin
	case (current_state)
		s0: begin
			if (valid_ack)
				next_state = s1;
			else 
				next_state = s0;
		 end
		s1: 
			next_state = s2;
		s2:
			next_state = s3;
		s3:
			next_state = s4;
		s4:
			next_state = s0;
		default:
			next_state = s0;
	endcase	
 end

always @ (posedge gfxClk)
	if (vi_reset || (~viResetb_gfx))
		current_state <= s0;
	else
		current_state <= next_state;

// MAYBE MAKE SIGNALS IN STATE
assign wen = (~(next_state == s0) && ~wen_right);
assign wen_rgt = (~(next_state == s0) && wen_right);


// 8 deep 64 bit wide fifo (or 2 deep 256 bit fifo)
// Write Clock - gfxClk (200 Mhz)
// Read Clock - video_clk (27 Mhz)
// need right fifo as well

Mem1R1W8x64 lpixel_fifo (
	.RdClk (vClk),
        .WrClk(gfxClk),
        .ScanMode(1'b0),
        .e1WrEn (wen),
        .e1WrAdr (wr_ptr[2:0]),
        .e1WrDat (mem_vi_data_d1),
        .e1RdAdr (rd_ptr[2:0]),
        .RdDat   (data_quad) );


// pixel fifo for right picture if in 3d mode
Mem1R1W8x64 rpixel_fifo (   
        .RdClk (vClk),
        .WrClk(gfxClk),
        .ScanMode(1'b0),
        .e1WrEn (wen_rgt),
        .e1WrAdr (wr_ptr_rgt[2:0]),
        .e1WrDat (mem_vi_data_d1),
        .e1RdAdr (rd_ptr_rgt[2:0]),
        .RdDat   (data_quad_rgt) );
                   


// double register signal from dot clock domain
sync gfx_pic_sel_sync (.D(top_pic_sel), .CK(gfxClk), .Q(gfx_top_pic_sel));
sync gfx_vsync_sync (.D(vsyncb_d1), .CK(gfxClk), .Q(gfx_vsyncb));
sync gfx_hsync_sync (.D(hsyncb), .CK(gfxClk), .Q(gfx_hsyncb));
sync gfx_hblank_sync (.D(hblank), .CK(gfxClk), .Q(gfx_hblank));
sync gfx_vblank_sync (.D(vblank_d1), .CK(gfxClk), .Q(gfx_vblank));
sync gfx_dsp_en_sync (.D(dsp_en_d1), .CK(gfxClk), .Q(gfx_dsp_en));
sync gfx_field_sync (.D(field), .CK(gfxClk), .Q(gfx_field));
// logical_wptr could just be wr_ptr[2]

assign gfx_vsyncb_detected = gfx_vsyncb_d1 && ~gfx_vsyncb;
assign gfx_hsyncb_detected = gfx_hsyncb_d1 && ~gfx_hsyncb;
assign req_detected = mem_vi_ack_d2 && ~mem_vi_ack_d1; 

always @ (posedge gfxClk)
 begin
	gfx_hblank_d1 <= gfx_hblank;
	gfx_vsyncb_d1 <= gfx_vsyncb;
	gfx_hsyncb_d1 <= gfx_hsyncb;
	
	if (~viResetb_gfx)
		std_pst <= 8'b0;
	else if (gfx_vblank_end_detected)
		std_pst <= std;

 	if ((~viResetb_gfx) || gfx_hsyncb_detected || vi_reset)
		logical_wptr <= 2'b0;
	else if (valid_ack && ~(right))
		logical_wptr <= logical_wptr + 1;
	
	if ((~viResetb_gfx) || gfx_hsyncb_detected || vi_reset)
		logical_wptr_rgt <= 2'b0;
	else if (valid_ack && right)
		logical_wptr_rgt <= logical_wptr_rgt + 1;

        if ((~viResetb_gfx) || gfx_hsyncb_detected || vi_reset)
		wr_ptr <= 2'b0;
	else if (wen)
		wr_ptr <= wr_ptr + 1;

        if ((~viResetb_gfx) || gfx_hsyncb_detected || vi_reset)
		wr_ptr_rgt <= 2'b0;
	else if (wen_rgt)
		wr_ptr_rgt <= wr_ptr_rgt + 1;
 end



// brings gray coded read address over to write side
// worst case a full_fifo flag appears early

always @ (posedge gfxClk)  
 begin
        glogical_rptr_d1 <= glogical_rptr;
        glogical_rptr_d2 <= glogical_rptr_d1;
        glogical_rptr_rgt_d1 <= glogical_rptr_rgt;
        glogical_rptr_rgt_d2 <= glogical_rptr_rgt_d1;
 end



// fifo_full if rd  wr
//		00  10
//		01  11
//		11  00
//		10  01
// Note: rd is gray coded


assign fifo_full = ((~glogical_rptr_d2[1] && logical_wptr[1]) && (glogical_rptr_d2[0] ^~
	logical_wptr[0])) || ((glogical_rptr_d2[1] && ~logical_wptr[1]) && (glogical_rptr_d2[0] ^
	logical_wptr[0]));

assign fifo_full_rgt = ((~glogical_rptr_rgt_d2[1] && logical_wptr_rgt[1]) &&
			(glogical_rptr_rgt_d2[0] ^~ logical_wptr_rgt[0])) ||
			((glogical_rptr_rgt_d2[1] && ~logical_wptr_rgt[1]) &&
			(glogical_rptr_rgt_d2[0] ^ logical_wptr_rgt[0]));

// will have to make sure the pending req (if any) has been ACKed
// new requests will be made at the start of hsync on every active line, and will
// continue until WPL requests have been made 

sync allow_req_sync(.D(allow_req), .CK(gfxClk), .Q(gfx_allow_req));
sync allow_req_rgt_sync(.D(allow_req_rgt), .CK(gfxClk), .Q(gfx_allow_req_rgt));

assign mem_req = gfx_allow_req && ~fifo_full && (wpl_pst > req_count);
assign mem_req_rgt = gfx_allow_req_rgt && ~fifo_full_rgt && (wpl_pst > req_count);
assign valid_req = right ? mem_req_rgt : mem_req;

// ***********************************************************
// ***********************************************************
// ***********************************************************
// ***********************************************************
// NEEED TO MAKE SURE THAT NOTHING DEPENDS ON RELATION BETWEEN 
// active_hblank_detected && hblank_detected
// *********************************************************** 
// *********************************************************** 
// *********************************************************** 
// *********************************************************** 

assign active_hblank_detected = (gfx_dsp_en_d1 && ~gfx_dsp_en);
assign hblank_detected = (~gfx_hblank_d1 && gfx_hblank);


always @ (posedge gfxClk)
// NEED TO ADD RIGHT
	if (vi_reset || ~viResetb_gfx || gfx_hsyncb_detected)
		req_count <= 7'b0;
	else if (req_detected && (right || ~dlr))
		req_count <= req_count + 1;



always @ (posedge gfxClk)
	if (vi_reset || ~viResetb_gfx)
		req_pending <= 1'b0;
	else if ((cur_req_state == wait_for_ack) && (active_hblank_detected))
		req_pending <= 1'b1;
	else if ((cur_req_state == req_idle) && (mem_vi_ack_d1))
		req_pending <= 1'b0;


// DLR CROSSING ZONES
// If ack comes in at wrong time at will mess things up
always @ (cur_req_state or valid_req or mem_vi_ack_d1 or dlr)
	case (cur_req_state)
	 req_idle : begin
			req_rdy = 1'b0;	
			if (valid_req)
				next_req_state = send_req;
			else
				next_req_state = req_idle;
		end
	 send_req : begin
			req_rdy = 1'b1;	
			next_req_state = wait_for_ack;
		end	
	 wait_for_ack : begin
			req_rdy = 1'b0;
			if (mem_vi_ack_d1)
				next_req_state = got_ack;
			else
				if (active_hblank_detected)
					next_req_state = req_idle;
				else 
					next_req_state = wait_for_ack;
		end
	 got_ack : begin
			req_rdy = 1'b0;
			next_req_state = req_idle;
		end
	default : begin
			req_rdy = 1'b0;
			next_req_state = req_idle;
		end
			
	endcase


always @ (posedge gfxClk)
 begin
	if ((~viResetb_gfx) || vi_reset)
		cur_req_state <= req_idle;
	else
		cur_req_state <= next_req_state;

	if ( (~viResetb_gfx) || vi_reset || active_hblank_detected)
         begin
                right <= 1'b0;
                wen_right <= 1'b0;
         end
        else
         begin
                if ((cur_req_state == got_ack) && dlr)
                        right <=  (~right);
                if ((current_state == s4) && dlr)
                        wen_right <= ~wen_right;
         end
end


// register outputs
always @ (posedge gfxClk)
        if (~viResetb_gfx)
	 begin
		vi_mem_req <= 1'b0;
		vi_mem_addr <= 20'b0;
	 end
	else
	 begin
		vi_mem_req <= req_rdy;
		vi_mem_addr <= addr_rdy[25:5];
	 end

// double register req signal from dot clock domain
// to bring it into gfx clock domain
// the req signal will be valid for one dot clk (37 ns)
// this should be sufficient time for the gfx_clk (5 ns) to pick it up

always @ (posedge gfxClk)
 begin
 	gfx_dsp_en_d1 <= gfx_dsp_en;
	gfx_vblank_d1 <= gfx_vblank;
 end

assign gfx_vblank_end_detected = gfx_vblank_d1 && ~gfx_vblank;

always @ (posedge gfxClk)
	if ((~viResetb_gfx) || ~gfx_vsyncb || vi_reset)
		line_count <= 9'b0;
	else if (active_hblank_detected)
		line_count <= line_count + 1;


//// THIS IS NOT GOOD< NOTBEING REGISTERED
//assign next_req_addr_rgt = (fbb_rgt + ({(line_count * std), 5'b0}));
//assign next_req_addr = (fbb_lft + ( {(line_count * std), 5'b0}) );
// an accumulator for the addr which increments every active hblank




always @ (posedge gfxClk)
 begin
//	if (~gfx_allow_req || vi_reset || (~viResetb))
//		req_addr <= next_req_addr;
//	else if (req_rdy && ~right)
//		req_addr <= req_addr + 32;
//	if (~gfx_allow_req || vi_reset || (~viResetb))
//		req_addr_rgt <= next_req_addr_rgt;
//	else if (req_rdy && right)
//		req_addr_rgt <= req_addr_rgt + 1;


// allow_req goes high at active_hsync_detected
// so we have from begining of hblank (give or take a clock)
// to beginning of hsync (give or take a clock) to load base_addr
// into req_addr
	//req_rdy_d1 <= req_rdy;

	if (~gfx_allow_req || vi_reset || (~viResetb_gfx))
		req_addr <= base_addr;
	else if (req_rdy && ~right)
		req_addr <= req_addr + 32;
	

	if (~gfx_allow_req_rgt || vi_reset || (~viResetb_gfx))
		req_addr_rgt <= base_addr_rgt;
	else if (req_rdy && right)
		req_addr_rgt <= req_addr_rgt + 32;

// Also cur_fbb_lft & cur_fbb_rgt change at beginning of pre_eq
// so there should be a lot of time between gfx_vblank_detected
// 

	if (~viResetb_gfx) 
		base_addr <= 26'b0;
	else if (gfx_vblank_end_detected)
		if (high_addr == 1'b1)
			base_addr <= {cur_fbb_lft, 5'b0};
		else
			base_addr <= cur_fbb_lft;
	else if (active_hblank_detected)
		base_addr <= base_addr + {std_pst, 5'b0};

	if (~viResetb_gfx)
		base_addr_rgt <= 26'b0;
	else if (gfx_vblank_end_detected)
		if (high_addr == 1'b1)
			base_addr_rgt <= {cur_fbb_rgt, 5'b0};
		else
			base_addr_rgt <= cur_fbb_rgt;
	else if (active_hblank_detected)
		base_addr_rgt <= base_addr_rgt + {std_pst, 5'b0};

/*
	if (gfx_vsyncb_detected)
		base_addr <= cur_fbb_lft;
	else if (active_hblank_detected)
		base_addr <= base_addr + {std_pst, 5'b0};

	if (gfx_vsyncb_detected)
		base_addr_rgt <= cur_fbb_rgt;
	else if (active_hblank_detected)
		base_addr_rgt <= base_addr_rgt + {std_pst, 5'b0};
*/
 end


assign addr_rdy = right ? req_addr_rgt : req_addr;
assign line_addr_tmp = {line_addr, 5'b0};
//assign field_sel = gfx_field && ~vi_nintl;
assign field_sel = gfx_field;
// PAL: odd field -> bottom picture
// PAL: even field -> top picture
// NTSC: odd field -> top picture
// NTSC: even  field -> bottom picture
//assign cur_fbb_lft = (field_sel ^ vi_pal) ? fbb_lft_bot : fbb_lft;
//assign cur_fbb_rgt = (field_sel ^ vi_pal) ? fbb_rgt_bot : fbb_rgt;
// needed to change above since for MPAL field0 was top field
// currently, 'top_pic_sel' is high if VSync & HSync are coincident
//assign cur_fbb_lft = gfx_top_pic_sel ? fbb_lft : fbb_lft_bot;
//assign cur_fbb_rgt = gfx_top_pic_sel ? fbb_rgt : fbb_rgt_bot;
assign cur_fbb_lft = (field_sel) ? fbb_lft_bot : fbb_lft;
assign cur_fbb_rgt = (field_sel) ? fbb_rgt_bot : fbb_rgt;



always @ (posedge gfxClk)
 begin
	line_addr <= (line_count * {2'b0,std_pst});
	word_count_d1 <= word_count;
	word_count_rgt_d1 <= word_count_rgt;
 end

always @ (posedge gfxClk)
 begin
	tmp_addr <= (cur_fbb_lft + line_addr_tmp + {word_count_d1, 5'b0});
	tmp_addr_rgt <= (cur_fbb_rgt + line_addr_tmp + {word_count_rgt_d1, 5'b0});
 end

endmodule


