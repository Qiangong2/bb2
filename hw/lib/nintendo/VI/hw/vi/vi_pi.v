///////////////////////////////////////////////////////////////////////////////
//
//   (C) 1999 ARTX INC..  ALL RIGHTS RESERVED.  UNPUBLISHED -- RIGHTS
//   RESERVED UNDER THE COPYRIGHT LAWS OF THE UNITED STATES.  USE OF A
//   COPYRIGHT NOTICE IS PRECAUTIONARY ONLY AND DOES NOT IMPLY PUBLICATION
//   OR DISCLOSURE.
//
//   THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF
//   ARTX INC..  USE, DISCLOSURE, OR REPRODUCTION IS PROHIBITED WITHOUT
//   THE PRIOR EXPRESS WRITTEN PERMISSION OF ARTX INC..
//
//                   RESTRICTED RIGHTS LEGEND
//
//   Use, duplication, or disclosure by the Government is subject to
//   restrictions as set forth in subparagraph (c)(1)(ii) of the Rights
//   in Technical Data and Computer Software clause at DFARS 252.227-7013
//   or subparagraphs (c)(1) and (2) of Commercial Computer Software --
//   Restricted Rights at 48 CFR 52.227-19, as applicable.
//
//   ArtX Inc.
//   3400 Hillview Ave, Bldg 5
//   Palo Alto, CA 94304
//
///////////////////////////////////////////////////////////////////////////////

// Todo:
// - resetb, synchronous?
// - powerup defaults????
// - fanout on the address decode
// - need to double register non static boundary crossings
// - vi_piVrtIntr????
// - get rid of gamma and replace with scaling
// - vi_reset & vi_enable
// - get rid of vi_chroma 
// - need to see which bits are read once and cleared, etc..
// - holding reads in fifo wasting locations?

module vi_pi (
    	// Inputs from outside
    	viResetb_vid, viResetb_gfx, gfxClk, vClk,
    	// Inputs from PI
	pi_viAddr,pi_viRd, pi_viReq, pi_viData,
	// Outputs to PI
    	vi_piAck, vi_piData, vi_piVrtIntr,
	// Inputs from VI
	int0, int1, int2, int3,
	vi_enable, 
	hct, vct, gun0_vct,
	gun1_vct, gun0_hct, gun1_hct, gun0_htrg, gun1_htrg,
	gun0_vtrg, gun1_vtrg,
	vsyncb_d1,
	// Outputs to VI
	vi_reset_vid, vi_reset, vi_nintl, vi_dlr, vi_dbg,
	g0md_pst, g1md_pst,
	vi_mode, hlw, hce, hcs, hsy, hbe, hbs, equ, acv, 
	oprb, opsb, eprb, epsb, bs1, be1, bs3, be3, bs2, 
	be2, bs4, be4, fbb_lft, xoff, fbb_rgt, std, wpl, hct0,		
	vct0, hct1, vct1, hct2, vct2, high_addr,
	hct3, vct3, g0trgclr, g1trgclr,
// gun0_trg_clr are too short 
	fbb_lft_bot, fbb_rgt_bot, 
	T3_0, T3_1, T3_2, T3_3, T3_4, T3_5, T3_6, T3_7, T4_0,
	T4_1, T4_2, T4_3, T4_4, T4_5, T4_6, T4_7, T5_0, T5_1,
	T5_2, T5_3, T5_4, T5_5, T5_6, T5_7, T24, step, hscaler_en,
	ipol, npol, kpol, bpol, hpol, vpol, fpol, cpol,
	i0enb, i1enb, i2enb, i3enb,
	i0clr, i1clr, i2clr, i3clr,
	visel_tocore, viclksel, vi_srcwidth,
	hbe656, hbs656, brdr_en
    );


`include "vi_state.v"
`include "vi_reg.v"
	input		vClk;
    	input		viResetb_gfx, viResetb_vid;
    	input       	gfxClk;
    	input [8:1] 	pi_viAddr;      // Register address
    	input       	pi_viRd;        // When asserted, processor transaction is a read
    	input       	pi_viReq;       // Processor register transaction start
	input [15:0] 	pi_viData;      // Write data, bound for register
	input		vsyncb_d1;	
	input [1:0]	visel_tocore;	// Status of DTV I/O pin
	output 		viclksel;	// VI Clk select (54 or 27)
	output  	vi_piAck;       // Processor register transaction complete
    	output [15:0] 	vi_piData;      // Read data, from register
    	output         	vi_piVrtIntr;   // Vertical Interrupt
	

output 		vi_reset_vid, vi_enable, vi_reset;
output		vi_nintl;
output		vi_dlr;
output [1:0]	g0md_pst;
output [1:0]	g1md_pst;
output 		vi_dbg;
output [9:0]	hlw;
output [6:0]	hce;
output [6:0]	hcs;
output [6:0]	hsy;
output [9:0]	hbe;
output [9:0]	hbs;
output [3:0]	equ;
output [9:0]	acv;
output [9:0]	oprb;
output [9:0]	opsb;
output [9:0]	eprb;
output [9:0]	epsb;
output [4:0]	bs1;
output [10:0]	be1;
output [4:0]	bs3;
output [10:0]	be3;
output [4:0]	bs2;
output [10:0]	be2;
output [4:0]	bs4;
output [10:0]	be4;
output [23:0]	fbb_lft;
output [3:0]	xoff;
output 		high_addr;
output [23:0]	fbb_rgt;
output [23:0]	fbb_lft_bot;
output [23:0]	fbb_rgt_bot;
output [7:0]	std;
output [6:0]	wpl;
output [10:0]	hct0;
output [10:0]	vct0;
output [10:0]	hct1;
output [10:0]	vct1;
output [10:0]	hct2;
output [10:0]	vct2;
output [10:0]	hct3;
output [10:0]	vct3;
output 		g0trgclr, g1trgclr;
output [9:0]	T3_0, T3_1, T3_2, T3_3, T3_4, T3_5, T3_6, T3_7, T4_0;
output [7:0]    T4_1, T4_2, T4_3, T4_4, T4_5, T4_6, T4_7, T5_0, T5_1,
                T5_2, T5_3, T5_4, T5_5, T5_6, T5_7, T24;
output [8:0]    step;
output 		hscaler_en;
output [1:0]	vi_mode;
output 		ipol, npol, kpol, 
		bpol, hpol, vpol, fpol, cpol;
output 		i0enb, i1enb, i2enb, i3enb;
output 		i0clr, i1clr, i2clr, i3clr;
output [9:0]	vi_srcwidth;
output [9:0] 	hbe656, hbs656;
output		brdr_en;

// inputs to sync to gfxClk
input 		int0;		
input 		int1;		
input 		int2;		
input 		int3;		
input [10:0]	gun0_hct;
input [10:0] 	gun1_hct;
input [10:0]	gun0_vct, gun1_vct;
input 		gun0_htrg, gun1_htrg;
input           gun0_vtrg, gun1_vtrg;
input [10:0]	hct; //INPUT
input [10:0]	vct; //INPUT

// redeclare outputs
reg 		viclksel;
reg  [15:0]   	vi_piData;
reg 		vi_piAck;
reg		vi_reset, vi_nintl, vi_dlr;
wire 		vi_reset_vid;
wire		vi_dbg;
reg [1:0]	vi_gun0_trig_mode, vi_gun1_trig_mode;
reg [1:0]	vi_mode;
reg [9:0]    	hlw; 
reg [6:0]    	hce;    
reg [6:0]    	hcs;
reg [6:0]    	hsy;
reg [9:0]    	hbe;
reg [9:0]    	hbs;
reg [3:0]    	equ;
reg [9:0]    	acv;
reg [9:0]    	oprb;
reg [9:0]    	opsb;
reg [9:0]    	eprb;
reg [9:0]    	epsb;
reg [4:0]    	bs1;
reg [10:0]    	be1;
reg [4:0]    	bs3;
reg [10:0]    	be3;
reg [4:0]    	bs2;
reg [10:0]    	be2;
reg [4:0]    	bs4;
reg [10:0]    	be4;
reg [23:0]   	fbb_lft;
reg [3:0]    	xoff;
reg 		high_addr;
reg [23:0]   	fbb_rgt;
reg [23:0]   	fbb_lft_bot;
reg [23:0]   	fbb_rgt_bot;
reg [7:0]    	std;
reg [6:0]    	wpl;
reg [10:0]   	hct0;
reg [10:0]    	vct0;
reg           	enb0;
reg [10:0]   	hct1;
reg [10:0]    	vct1;
reg           	enb1;
reg [10:0]   	hct2;
reg [10:0]    	vct2;
reg          	enb2;
reg [10:0]   	hct3;
reg [10:0]    	vct3;
reg [9:0]    vi_srcwidth; 
reg          	enb3;
reg 		ipol, npol, kpol, bpol, hpol, vpol, fpol, cpol;
wire 		gfx_g0clr_ack, gfx_g1clr_ack;	
wire 		gfx_int0, gfx_int1, gfx_int2, gfx_int3;
wire 		pre_int0, pre_int1, pre_int2, pre_int3;
wire [15:0] 	vi_dsp_cfg_reg;
wire [15:0] 	vi_hor_tim0_l_reg;
wire [15:0] 	vi_hor_tim0_u_reg;
wire [15:0] 	vi_hor_tim1_l_reg;
wire [15:0] 	vi_hor_tim1_u_reg;
wire [15:0] 	vi_ver_tim_reg;
wire [15:0] 	vi_ver_odd_tim_l_reg;
wire [15:0] 	vi_ver_odd_tim_u_reg;
wire [15:0] 	vi_ver_evn_tim_l_reg;
wire [15:0] 	vi_ver_evn_tim_u_reg;
wire [15:0] 	vi_odd_bblnk_intvl_l_reg;
wire [15:0] 	vi_odd_bblnk_intvl_u_reg;
wire [15:0] 	vi_evn_bblnk_intvl_l_reg;
wire [15:0] 	vi_evn_bblnk_intvl_u_reg;
wire [15:0] 	vi_pic_base_lft_l_reg;
wire [15:0] 	vi_pic_base_lft_u_reg;
wire [15:0] 	vi_pic_base_rgt_l_reg;
wire [15:0] 	vi_pic_base_rgt_u_reg;
wire [15:0] 	vi_pic_base_lft_bot_l_reg;
wire [15:0] 	vi_pic_base_lft_bot_u_reg;
wire [15:0] 	vi_pic_base_rgt_bot_l_reg;
wire [15:0] 	vi_pic_base_rgt_bot_u_reg;
wire [15:0] 	vi_pic_cfg_reg;
wire [15:0] 	vi_dsp_pos_l_reg;
wire [15:0] 	vi_dsp_pos_u_reg;
wire [15:0] 	vi_dsp_int0_l_reg;
wire [15:0] 	vi_dsp_int0_u_reg;
wire [15:0] 	vi_dsp_int1_l_reg;
wire [15:0] 	vi_dsp_int1_u_reg;
wire [15:0] 	vi_dsp_int2_l_reg;
wire [15:0] 	vi_dsp_int2_u_reg;
wire [15:0] 	vi_dsp_int3_l_reg;
wire [15:0] 	vi_dsp_int3_u_reg;
wire [15:0] 	vi_dsp_latch0_l_reg;
wire [15:0] 	vi_dsp_latch0_u_reg;
wire [15:0] 	vi_dsp_latch1_l_reg;
wire [15:0] 	vi_dsp_latch1_u_reg;
wire [15:0]	vi_hscale_reg;
wire [15:0]	vi_srcwidth_reg;
wire [15:0]	vi_fltr_coeff0_l_reg;
wire [15:0]	vi_fltr_coeff0_u_reg;
wire [15:0]	vi_fltr_coeff1_l_reg;
wire [15:0]	vi_fltr_coeff1_u_reg;
wire [15:0]	vi_fltr_coeff2_l_reg;
wire [15:0]	vi_fltr_coeff2_u_reg;
wire [15:0]	vi_fltr_coeff3_l_reg;
wire [15:0]	vi_fltr_coeff3_u_reg;
wire [15:0]	vi_fltr_coeff4_l_reg;
wire [15:0]	vi_fltr_coeff4_u_reg;
wire [15:0]	vi_fltr_coeff5_l_reg;
wire [15:0]	vi_fltr_coeff5_u_reg;
wire [15:0]	vi_fltr_coeff6_l_reg;
wire [15:0]	vi_fltr_coeff6_u_reg;
wire [15:0]	vi_outpol_reg;
wire [15:0]	vi_clksel_reg;
wire [15:0]	vi_dtvstatus_reg;
wire [15:0]	vi_hbe656_reg;
wire [15:0]	vi_hbs656_reg;

reg 		gun0_trg_clr, gun1_trg_clr;
wire		g0trgclr, g1trgclr;
reg 		gun0_clrb, gun1_clrb;
wire 		int0, int1, int2, int3;
reg [8:1]	pi_viAddr_d1;
reg [8:1]	read_addr;
reg 		pi_viRd_d1;
reg		pi_viReq_d1;
reg [15:0]	pi_viData_d1;
wire 		vi_enable;
reg 		vi_enable_in;
wire 		pi_write, pi_read;
reg [10:0]	vct_out, gun1_vct_out, gun0_vct_out;
reg		gun0_trg_out, gun1_trg_out;
reg		hgun0_wptr, hgun0_rptr, 
		hgun1_wptr, hgun1_rptr, 
		vgun0_wptr, vgun0_rptr, 
		vgun1_wptr, vgun1_rptr, 
		hct_rptr, hct_wptr, vct_rptr, vct_wptr;
wire 		hgun0_full, hgun0_empty, vgun0_full, vgun0_empty,
		hgun1_full, hgun1_empty, vgun1_full, vgun1_empty,
		hct_full, vct_full, hct_empty, vct_empty, hgun0_rptr_d2, hgun1_rptr_d2,
		vgun0_rptr_d2, vgun1_rptr_d2, vgun1_wptr_d2, vgun0_wptr_d2, hgun1_wptr_d2,
		hgun0_wptr_d2, hct_wptr_d2, vct_wptr_d2, hct_rptr_d2, vct_rptr_d2;
wire 		rst_g0_write, rst_g1_write, rst_g0md, rst_g1md, gfx_vsyncb_detected;
reg 		g0_write, g1_write;
reg [2:0] 	cur_gun0_state, nxt_gun0_state, cur_gun1_state, nxt_gun1_state;
wire 		gfx_vsyncb;

reg [9:0]	T3_0, T3_1, T3_2, T3_3, T3_4, T3_5, T3_6, T3_7, T4_0;
reg [7:0]     	T4_1, T4_2, T4_3, T4_4, T4_5, T4_6, T4_7, T5_0, T5_1,
                T5_2, T5_3, T5_4, T5_5, T5_6, T5_7, T24;
reg [8:0]	step;
reg		hscaler_en;
reg 		gfx_vsyncb_d1, gfx_vsyncb_detected_d1;
reg [1:0]	g1md_pst, g0md_pst;
wire 		gun1_trg, gun0_trg;
		
wire 		vi_dsp_latch0_l_ren, vi_dsp_latch0_l_wen, vi_dsp_latch0_u_ren, vi_dsp_latch0_u_wen,
 		vi_dsp_latch1_l_ren, vi_dsp_latch1_l_wen, vi_dsp_latch1_u_ren, vi_dsp_latch1_u_wen,
		vi_dsp_pos_l_ren, vi_dsp_pos_l_wen, vi_dsp_pos_u_ren, vi_dsp_pos_u_wen;
//new
wire            vid_gun0_trg_clr, vid_gun1_trg_clr;
reg             g0clr_ack, g1clr_ack;
reg             vid_gun0_trg_clr_d1, vid_gun1_trg_clr_d1;

wire            g0hct_req, g0vct_req, g1hct_req, g1vct_req,
                hct_req, vct_req ;
wire            gfx_g0hct_AckState, vid_g0hct_ReqState;
reg             g0hct_AckState;
wire            g0hct_req_edge, g0hct_Ack;
reg             g0hct_ReqState;
reg  [10:0]      gfx_g0hct;
reg             gfx_g0hct_AckState_d1;
reg [10:0]       g0hct_cap;
reg             vid_g0hct_ReqState_d1;


wire            gfx_g0vct_AckState;
wire            vid_g0vct_ReqState;
wire            g0vct_req_edge;
wire            g0vct_Ack;
reg             g0vct_ReqState;
reg [10:0]       gfx_g0vct;
reg             g0trg_cap;


wire            gfx_g1hct_AckState, vid_g1hct_ReqState;
reg             g1hct_AckState;
wire            g1hct_req_edge, g1hct_Ack;
reg             g1hct_ReqState;
reg [10:0]      gfx_g1hct;
reg             gfx_g1hct_AckState_d1;
reg [10:0]       g1hct_cap;
reg             vid_g1hct_ReqState_d1;
reg		vid_g0vct_ReqState_d1;
reg		vid_g1vct_ReqState_d1;
reg		vid_vct_ReqState_d1;

wire            gfx_g1vct_AckState;
wire            vid_g1vct_ReqState;
wire            g1vct_req_edge;
wire            g1vct_Ack;
reg             g1vct_ReqState;  
reg [10:0]       gfx_g1vct;   
reg             g1trg_cap;   

wire            gfx_hct_AckState, vid_hct_ReqState;
reg             hct_AckState;
wire            hct_req_edge, hct_Ack;
reg             hct_ReqState;
reg  [10:0]      gfx_hct;
reg             gfx_hct_AckState_d1;
reg [10:0]       hct_cap;
reg             vid_hct_ReqState_d1;
wire            pos_cap;
reg             pos_rd_pending;

wire            gfx_vct_AckState;
wire            vid_vct_ReqState;
wire            vct_req_edge;
wire            vct_Ack;
reg             vct_ReqState;
reg [10:0]       gfx_vct;

reg             gfx_i0clr;
wire            gfx_i0clr_ack;
reg             int0_clrb;
wire            vid_i0clr;   
wire            i0clr;
reg             vid_i0clr_d1;
reg             i0clr_ack;

reg             gfx_i1clr;
wire            gfx_i1clr_ack;
reg             int1_clrb;
wire            vid_i1clr;
wire            i1clr;
reg             vid_i1clr_d1;
reg             i1clr_ack;

reg             gfx_i2clr;
wire            gfx_i2clr_ack;
reg             int2_clrb;
wire            vid_i2clr;
wire            i2clr;
reg             vid_i2clr_d1;
reg             i2clr_ack;

reg             gfx_i3clr;
wire            gfx_i3clr_ack;
reg             int3_clrb;
wire            vid_i3clr;
wire            i3clr;
reg             vid_i3clr_d1;
reg             i3clr_ack;

reg [10:0]	g0vct_cap;
reg [10:0]	g1vct_cap;
reg [10:0]	vct_cap;
reg 		gfx_g0trg;
reg 		gfx_g1trg;
reg 		gfx_g0vct_AckState_d1;
reg 		gfx_g1vct_AckState_d1;
reg 		gfx_vct_AckState_d1;
reg 		g0vct_AckState;
reg 		g1vct_AckState;
reg 		vct_AckState;

reg 		g0hct_Ack_d1, g0vct_Ack_d1;
reg 		g1hct_Ack_d1, g1vct_Ack_d1;
reg 		hct_Ack_d1, vct_Ack_d1;
reg 		g0hct_Ack_d2, g0vct_Ack_d2;
reg 		g1hct_Ack_d2, g1vct_Ack_d2;
reg 		hct_Ack_d2, vct_Ack_d2;
wire 		vi_piAck_pre;
reg  [15:0]	vi_piData_pre;
reg [15:0] 	vi_piData_e;
reg 		rd_ack;
reg		wr_ack;
reg 		gen_rd_ack;
reg 		rdwr_acksel;
reg 		int0_d1, int1_d1, int2_d1, int3_d1; 
reg [9:0]	hbs656, hbe656;
reg 		brdr_en;


parameter       gun_off = `GUN_OFF,
                gun_one = `GUN_ONE,
                gun_twoa = `GUN_TWOA,  
                gun_twob = `GUN_TWOB,
                gun_always = `GUN_ALWAYS;

assign vi_dbg = (vi_mode == 2'b11);
assign pi_write = pi_viReq_d1 && ~pi_viRd_d1;
assign pi_read = pi_viReq_d1 && pi_viRd_d1;

// Memory Mapped Registers
assign vi_dsp_cfg_reg = {6'b0, vi_mode, vi_gun1_trig_mode, vi_gun0_trig_mode,
			vi_dlr, vi_nintl, vi_reset, vi_enable_in};
assign vi_hor_tim0_l_reg = {6'b0, hlw};
assign vi_hor_tim0_u_reg = {1'b0, hcs, 1'b0, hce};
assign vi_hor_tim1_l_reg = {hbe[8:0], hsy};
assign vi_hor_tim1_u_reg = {5'b0, hbs, hbe[9]};
assign vi_ver_tim_reg = {2'b0, acv, equ};
assign vi_ver_odd_tim_l_reg = {6'b0, oprb};
assign vi_ver_odd_tim_u_reg = {6'b0, opsb};
assign vi_ver_evn_tim_l_reg = {6'b0, eprb};
assign vi_ver_evn_tim_u_reg = {6'b0, epsb};
assign vi_odd_bblnk_intvl_l_reg = {be1, bs1};
assign vi_odd_bblnk_intvl_u_reg = {be3, bs3};
assign vi_evn_bblnk_intvl_l_reg = {be2, bs2};
assign vi_evn_bblnk_intvl_u_reg = {be4, bs4};
assign vi_pic_base_lft_l_reg = fbb_lft[15:0];
assign vi_pic_base_lft_u_reg = {3'b0, high_addr, xoff, fbb_lft[23:16]};
assign vi_pic_base_rgt_l_reg = fbb_rgt[15:0];
assign vi_pic_base_rgt_u_reg = {8'b0, fbb_rgt[23:16]};
assign vi_pic_base_lft_bot_l_reg = fbb_lft_bot[15:0];
assign vi_pic_base_lft_bot_u_reg = {8'b0, fbb_lft_bot[23:16]};
assign vi_pic_base_rgt_bot_l_reg = fbb_rgt_bot[15:0];
assign vi_pic_base_rgt_bot_u_reg = {8'b0, fbb_rgt_bot[23:16]};
assign vi_pic_cfg_reg = {1'b0, wpl, std};
assign vi_dsp_pos_l_reg = {5'b0, gfx_hct};
assign vi_dsp_pos_u_reg = {5'b0, gfx_vct};

assign vi_dsp_int0_l_reg = {5'b0, hct0};
assign vi_dsp_int0_u_reg = {gfx_int0, 2'b0, enb0, 1'b0, vct0};

assign vi_dsp_int1_l_reg = {5'b0, hct1};
assign vi_dsp_int1_u_reg = {gfx_int1, 2'b0, enb1, 1'b0, vct1};

assign vi_dsp_int2_l_reg = {5'b0, hct2};
assign vi_dsp_int2_u_reg = {gfx_int2, 2'b0, enb2, 1'b0, vct2};

assign vi_dsp_int3_l_reg = {5'b0, hct3};
assign vi_dsp_int3_u_reg = {gfx_int3, 2'b0, enb3, 1'b0, vct3};

assign vi_dsp_latch0_l_reg = {5'b0, gfx_g0hct};
assign vi_dsp_latch0_u_reg = {gfx_g0trg, 4'b0, gfx_g0vct};
assign vi_dsp_latch1_l_reg = {5'b0, gfx_g1hct};
assign vi_dsp_latch1_u_reg = {gfx_g1trg, 4'b0, gfx_g1vct};
assign vi_hscale_reg = {3'b0, hscaler_en, 3'b0, step};
assign vi_srcwidth_reg = {5'b0, vi_srcwidth};
assign vi_fltr_coeff0_l_reg = {T3_1[5:0], T3_0};
assign vi_fltr_coeff0_u_reg = {T3_2, T3_1[9:6]};
assign vi_fltr_coeff1_l_reg = {T3_4[5:0], T3_3};
assign vi_fltr_coeff1_u_reg = {T3_5, T3_4[9:6]};
assign vi_fltr_coeff2_l_reg = {T3_7[5:0], T3_6};
assign vi_fltr_coeff2_u_reg = {T4_0, T3_7[9:6]};
assign vi_fltr_coeff3_l_reg = {T4_2, T4_1};
assign vi_fltr_coeff3_u_reg = {T4_4, T4_3};
assign vi_fltr_coeff4_l_reg = {T4_6, T4_5};
assign vi_fltr_coeff4_u_reg = {T5_0, T4_7};
assign vi_fltr_coeff5_l_reg = {T5_2, T5_1};
assign vi_fltr_coeff5_u_reg = {T5_4, T5_3};
assign vi_fltr_coeff6_l_reg = {T5_6, T5_5};
assign vi_fltr_coeff6_u_reg = {T24, T5_7};
assign vi_outpol_reg = {8'b0, cpol, fpol, vpol, hpol, bpol, kpol, npol, ipol};
assign vi_clksel_reg = {15'b0, viclksel};
assign vi_dtvstatus_reg = {14'b0, visel_tocore};
assign vi_hbe656_reg = {brdr_en, 5'b0, hbe656};
assign vi_hbs656_reg = {6'b0, hbs656};

sync reset_sync(.D(vi_reset), .CK(vClk), .Q(vi_reset_vid));
sync enable_sync(.D(vi_enable_in), .CK(vClk), .Q(vi_enable));

assign i0enb = enb0;
assign i1enb = enb1;
assign i2enb = enb2;
assign i3enb = enb3;

sync int0_sync (.D(int0), .CK(gfxClk), .Q(pre_int0));
sync int1_sync (.D(int1), .CK(gfxClk), .Q(pre_int1));
sync int2_sync (.D(int2), .CK(gfxClk), .Q(pre_int2));
sync int3_sync (.D(int3), .CK(gfxClk), .Q(pre_int3));

assign gfx_int0 = pre_int0 && ~gfx_i0clr;
assign gfx_int1 = pre_int1 && ~gfx_i1clr;
assign gfx_int2 = pre_int2 && ~gfx_i2clr;
assign gfx_int3 = pre_int3 && ~gfx_i3clr;

assign vi_piVrtIntr = gfx_int0 || gfx_int1 || gfx_int2 || gfx_int3;
assign gun0_trg = gun0_vtrg && gun0_htrg;
assign gun1_trg = gun1_vtrg && gun1_htrg;

assign vi_piAck_pre = rdwr_acksel ? wr_ack : rd_ack;


always @ (posedge gfxClk)
 begin
  	pi_viAddr_d1 <= pi_viAddr;
	pi_viRd_d1 <= pi_viRd;
	pi_viReq_d1 <= pi_viReq;
	pi_viData_d1 <= pi_viData;
	
	if (pi_read)
		read_addr <= pi_viAddr_d1;

       // pi_viReq_d2 <= pi_viReq_d1;
        wr_ack <= pi_viReq_d1;
	gen_rd_ack <= pi_viRd_d1 && pi_viReq_d1;

	// The PI spec states that the pi_viReq signal and the vi_piAck signal
	// should be separated by 1 clock 
	if (~viResetb_gfx)
		rdwr_acksel <= 1'b0;
        else if (pi_viReq_d1 && pi_viRd_d1)
                rdwr_acksel <= 1'b0;
        else if (pi_viReq_d1 && ~pi_viRd_d1)
                rdwr_acksel <= 1'b1;
        
        vi_piAck <= vi_piAck_pre;
        vi_piData_e <= vi_piData_pre;
//        vi_piData <= vi_piData_e;
        vi_piData <= vi_piData_pre;


// Gun trigger clearing mechanism
	if (vi_reset || (~viResetb_gfx))
		gun0_trg_clr <= 1'b0;
	else if  (gfx_g0clr_ack)
		gun0_trg_clr <= 1'b0;
	else if (~gun0_clrb)
	 	gun0_trg_clr <= 1'b1;

	if (vi_reset || (~viResetb_gfx))
		gun1_trg_clr <= 1'b0;
	else if (gfx_g1clr_ack)
		gun1_trg_clr <= 1'b0;
	else if (~gun1_clrb)
	 	gun1_trg_clr <= 1'b1;	

// Interrupt clearing mechanism

	if (vi_reset || (~viResetb_gfx))
                gfx_i0clr <= 1'b0;   
        else if  (gfx_i0clr_ack)
                gfx_i0clr <= 1'b0;
        else if (~int0_clrb)
                gfx_i0clr <= 1'b1;

        if (vi_reset || (~viResetb_gfx))
                gfx_i1clr <= 1'b0;
        else if (gfx_i1clr_ack)
                gfx_i1clr <= 1'b0;
        else if (~int1_clrb)
                gfx_i1clr <= 1'b1;

	if (vi_reset || (~viResetb_gfx))
                gfx_i2clr <= 1'b0;   
        else if  (gfx_i2clr_ack)
                gfx_i2clr <= 1'b0;
        else if (~int2_clrb)
                gfx_i2clr <= 1'b1;

        if (vi_reset || (~viResetb_gfx))
                gfx_i3clr <= 1'b0;
        else if (gfx_i3clr_ack)
                gfx_i3clr <= 1'b0;
        else if (~int3_clrb)
                gfx_i3clr <= 1'b1;


 end


sync g0trgclr_sync(.D(gun0_trg_clr), .CK(vClk), .Q(vid_gun0_trg_clr));
sync g1trgclr_sync(.D(gun1_trg_clr), .CK(vClk), .Q(vid_gun1_trg_clr));
sync g0clr_ack_sync (.D(g0clr_ack), .CK(gfxClk), .Q(gfx_g0clr_ack));
sync g1clr_ack_sync (.D(g1clr_ack), .CK(gfxClk), .Q(gfx_g1clr_ack));
assign g0trgclr = vid_gun0_trg_clr && ~vid_gun0_trg_clr_d1;
assign g1trgclr = vid_gun1_trg_clr && ~vid_gun1_trg_clr_d1;


sync i0clr_sync(.D(gfx_i0clr), .CK(vClk), .Q(vid_i0clr));
sync i1clr_sync(.D(gfx_i1clr), .CK(vClk), .Q(vid_i1clr));
sync i2clr_sync(.D(gfx_i2clr), .CK(vClk), .Q(vid_i2clr));
sync i3clr_sync(.D(gfx_i3clr), .CK(vClk), .Q(vid_i3clr));
sync i0clr_ack_sync (.D(i0clr_ack), .CK(gfxClk), .Q(gfx_i0clr_ack));
sync i1clr_ack_sync (.D(i1clr_ack), .CK(gfxClk), .Q(gfx_i1clr_ack));
sync i2clr_ack_sync (.D(i2clr_ack), .CK(gfxClk), .Q(gfx_i2clr_ack));
sync i3clr_ack_sync (.D(i3clr_ack), .CK(gfxClk), .Q(gfx_i3clr_ack));
assign i0clr = vid_i0clr && ~vid_i0clr_d1;
assign i1clr = vid_i1clr && ~vid_i1clr_d1;
assign i2clr = vid_i2clr && ~vid_i2clr_d1;
assign i3clr = vid_i3clr && ~vid_i3clr_d1;



always @ (posedge vClk)
 begin
//	hbe656 <= 3'd122;
//	hbs656 <= 3'd413;
	int0_d1 <= int0;
	int1_d1 <= int1;
	int2_d1 <= int2;
	int3_d1 <= int3;

	vid_gun0_trg_clr_d1 <= vid_gun0_trg_clr;
	vid_gun1_trg_clr_d1 <= vid_gun1_trg_clr;

	if (vi_reset_vid || ~viResetb_vid)
		g0clr_ack <= 1'b0;
	else if (vid_gun0_trg_clr != g0clr_ack)
		g0clr_ack <= ~g0clr_ack;

	if (vi_reset_vid || ~viResetb_vid)
		g1clr_ack <= 1'b0;
	else if (vid_gun1_trg_clr != g1clr_ack)
		g1clr_ack <= ~g1clr_ack;


	vid_i0clr_d1 <= vid_i0clr;
	vid_i1clr_d1 <= vid_i1clr;
	vid_i2clr_d1 <= vid_i2clr;
	vid_i3clr_d1 <= vid_i3clr;
	
	if (vi_reset_vid || ~viResetb_vid)
                i0clr_ack <= 1'b0;
        else if (vid_i0clr != i0clr_ack)
                i0clr_ack <= ~i0clr_ack;
	if (vi_reset_vid || ~viResetb_vid)
                i1clr_ack <= 1'b0;
        else if (vid_i1clr != i1clr_ack)
                i1clr_ack <= ~i1clr_ack;
	if (vi_reset_vid || ~viResetb_vid)
                i2clr_ack <= 1'b0;
        else if (vid_i2clr != i2clr_ack)
                i2clr_ack <= ~i2clr_ack;
	if (vi_reset_vid || ~viResetb_vid)
                i3clr_ack <= 1'b0;
        else if (vid_i3clr != i3clr_ack)
                i3clr_ack <= ~i3clr_ack;


 end
		

sync vsyncb_sync (.D(vsyncb_d1), .CK(gfxClk), .Q(gfx_vsyncb));
assign gfx_vsyncb_detected = ~gfx_vsyncb && gfx_vsyncb_d1;


always @ (cur_gun0_state or vi_gun0_trig_mode)
        case (cur_gun0_state)
                gun_off:  nxt_gun0_state = {1'b0, vi_gun0_trig_mode};
                gun_one:  nxt_gun0_state = {1'b0, vi_gun0_trig_mode};
                gun_twoa: nxt_gun0_state = gun_twob;
                gun_twob: nxt_gun0_state = {1'b0, vi_gun0_trig_mode};
                gun_always: nxt_gun0_state = {1'b0, vi_gun0_trig_mode};
		default : nxt_gun0_state = gun_off;
        endcase

always @ (cur_gun1_state or vi_gun1_trig_mode)
        case (cur_gun1_state)
                gun_off:  nxt_gun1_state = {1'b0, vi_gun1_trig_mode};
                gun_one:  nxt_gun1_state = {1'b0, vi_gun1_trig_mode};
                gun_twoa: nxt_gun1_state = gun_twob;
                gun_twob: nxt_gun1_state = {1'b0, vi_gun1_trig_mode};
                gun_always: nxt_gun1_state = {1'b0, vi_gun1_trig_mode};
		default : nxt_gun1_state = gun_off;
        endcase

// g0_write and g1_write  will make sure that if a write has occured to the Display Configuration Register
// it will not be overwritten when the alotted time is up, and the trigger mode tries to
// return to 'off'. The signals will be reset by rst_g0_write and rst_g1_write (respectively) at every
// vsync unless it is on the first field of the 2 field mode


assign rst_g0_write = gfx_vsyncb_detected && (cur_gun0_state != gun_twoa);
assign rst_g1_write = gfx_vsyncb_detected && (cur_gun1_state != gun_twoa);

// The trigger mode for the guns will return to off after their allotted time is up and there has 
// been no writes to the Display Configuration Register and it is not in 'always' mode
assign rst_g0md = (~g0_write) && gfx_vsyncb_detected && (cur_gun0_state != gun_always) && (cur_gun0_state != gun_twoa);
assign rst_g1md = (~g1_write) && gfx_vsyncb_detected && (cur_gun1_state != gun_always) && (cur_gun1_state != gun_twoa);


always @ (posedge gfxClk)
 begin
	gfx_vsyncb_d1 <= gfx_vsyncb;
	gfx_vsyncb_detected_d1 <= gfx_vsyncb_detected;

	if (rst_g0_write)
		g0_write <= 1'b0;
	else if ((pi_write) && (pi_viAddr_d1 == `VI_DSP_CFG))
		g0_write <= 1'b1;

	if (rst_g1_write)
		g1_write <= 1'b0;
	else if ((pi_write) && (pi_viAddr_d1 == `VI_DSP_CFG))
		g1_write <= 1'b1;
	
	if (~viResetb_gfx)
	 begin
	 	cur_gun0_state <= gun_off;
	 	cur_gun1_state <= gun_off;
	 end	
	// using d1 here because, nxt_gun_state changes with vi_gun_trig_mode
	// which changes at vsync, the changes arent seen until one clk later
	else if (gfx_vsyncb_detected_d1)	
	 begin
		cur_gun0_state <= nxt_gun0_state;
		cur_gun1_state <= nxt_gun1_state;
	 end

// The trigger mode are posted one clock after gfx_vsyncb to get the
// latest changes 
	if (~viResetb_gfx)
	 begin
		g0md_pst <= 2'b0;
		g1md_pst <= 2'b0;
	 end
	else if (gfx_vsyncb_detected_d1)
	 begin
		if (cur_gun0_state != gun_twoa)
			g0md_pst <= vi_gun0_trig_mode;
		if (cur_gun1_state != gun_twoa)
			g1md_pst <= vi_gun1_trig_mode;
	 end

	if ((~viResetb_gfx) || (rst_g0md))
		vi_gun0_trig_mode <= 2'b00;
	else if (pi_write)
		vi_gun0_trig_mode <= (pi_viAddr_d1 == `VI_DSP_CFG)
			? pi_viData_d1[`VI_DSP_CFG_REG_LE0] : vi_gun0_trig_mode; 

	if ((~viResetb_gfx) || (rst_g1md))
                vi_gun1_trig_mode <= 2'b00;
        else if (pi_write)
		vi_gun1_trig_mode <= (pi_viAddr_d1 == `VI_DSP_CFG) 
			? pi_viData_d1[`VI_DSP_CFG_REG_LE1] : vi_gun1_trig_mode; 

	if (~viResetb_gfx)
	 begin
		gun0_clrb <= 1'b1;
		gun1_clrb <= 1'b1;
		int0_clrb <= 1'b1;
		int1_clrb <= 1'b1;
		int2_clrb <= 1'b1;
		int3_clrb <= 1'b1;

	 end
	else if (pi_write)
	 begin
		if (pi_viAddr_d1 == `VI_DSP_LATCH0_U)
			gun0_clrb <= pi_viData_d1[`VI_DSP_LATCH0_U_REG_GUN0_TRG];
		
		if (pi_viAddr_d1 == `VI_DSP_LATCH1_U)
			gun1_clrb <= pi_viData_d1[`VI_DSP_LATCH1_U_REG_GUN1_TRG];

		if (pi_viAddr_d1 == `VI_DSP_INT0_U)
			int0_clrb <= pi_viData_d1[`VI_DSP_INT0_U_REG_INT0]; 
		if (pi_viAddr_d1 == `VI_DSP_INT1_U)
			int1_clrb <= pi_viData_d1[`VI_DSP_INT1_U_REG_INT1]; 
		if (pi_viAddr_d1 == `VI_DSP_INT2_U)
			int2_clrb <= pi_viData_d1[`VI_DSP_INT2_U_REG_INT2]; 
		if (pi_viAddr_d1 == `VI_DSP_INT3_U)
			int3_clrb <= pi_viData_d1[`VI_DSP_INT3_U_REG_INT3]; 

	 end
	else 
	 begin
		gun0_clrb <= 1'b1;
		gun1_clrb <= 1'b1;
		int0_clrb <= 1'b1;
		int1_clrb <= 1'b1;
		int2_clrb <= 1'b1;
		int3_clrb <= 1'b1;
	 end
end


always @ (posedge gfxClk)
	if (~viResetb_gfx)
	 begin
			vi_enable_in <= 1'b0;
			vi_reset <= 1'b0;
			vi_nintl <= 1'b0;
			vi_dlr <= 1'b0;
			vi_mode <= 2'b0;
			hlw <= 10'h0;
			hce <= 7'h0;
			hcs <= 7'h0;
			hsy <= 7'h0;
			hbe <= 10'h0;
			hbs <= 10'h0;
			equ <= 4'b0;
			acv <= 10'h0;
			oprb <= 10'h0;
			opsb <= 10'h0;
			eprb <= 10'h0;
			epsb <= 10'h0;
			bs1 <= 5'h0;
			be1 <= 11'h0;
			bs3 <= 5'h0;
			be3 <= 11'h0;
			bs2 <= 5'h0;
			be2 <= 11'h0;
			bs4 <= 5'h0;
			be4 <= 11'h0;
			fbb_lft <= 24'h0;
			fbb_lft_bot <= 24'h0;
			xoff <= 4'h0;
			high_addr <= 1'b0;
			fbb_rgt <= 24'h0;
			fbb_rgt_bot <= 24'h0;
			std <= 8'h0;
			wpl <= 7'h0;
			hct0 <= 11'h1;
			vct0 <= 11'h1;
			enb0 <= 1'b0;
			hct1 <= 11'h1;
			vct1 <= 11'h1;
			enb1 <= 1'b0;
			hct2 <= 11'h1;
			vct2 <= 11'h1;
			enb2 <= 1'b0;
			hct3 <= 11'h1;
			vct3 <= 11'h1;
			enb3 <= 1'b0;
			step <= 9'h100;
			hscaler_en <= 1'b0;
			vi_srcwidth <= 10'b0;
			brdr_en <= 1'b0;
			hbe656 <= 10'b0;
			hbs656 <= 10'b0;
			T3_0 <= 10'b0;
			T3_1 <= 10'b0;
			T3_2 <= 10'b0;
			T3_3 <= 10'b0;
			T3_4 <= 10'b0;
			T3_5 <= 10'b0;
			T3_6 <= 10'b0;
			T3_7 <= 10'b0;
			T4_0 <= 10'b0;
			T4_1 <= 8'b0;
			T4_2 <= 8'b0;
			T4_3 <= 8'b0;
			T4_4 <= 8'b0;
			T4_5 <= 8'b0;
			T4_6 <= 8'b0;
			T4_7 <= 8'b0;
			T5_0 <= 8'b0;
			T5_1 <= 8'b0;
			T5_2 <= 8'b0;
			T5_3 <= 8'b0;
			T5_4 <= 8'b0;
			T5_5 <= 8'b0;
			T5_6 <= 8'b0;
			T5_7 <= 8'b0;
			ipol <= 1'b0;
			npol <= 1'b0;
			kpol <= 1'b0;
			bpol <= 1'b0;
			hpol <= 1'b0;
			vpol <= 1'b0;
			fpol <= 1'b0;
			cpol <= 1'b0;
			viclksel <= 1'b0;
	 end
	else

		if (pi_write) 
		 begin

	// Display Configuration Register
			vi_enable_in <=  (pi_viAddr_d1 == `VI_DSP_CFG) 
				? pi_viData_d1[`VI_DSP_CFG_REG_ENB] : vi_enable_in;
			vi_reset <=  (pi_viAddr_d1 == `VI_DSP_CFG) 
				? pi_viData_d1[`VI_DSP_CFG_REG_RST] : vi_reset;
			vi_nintl <=  (pi_viAddr_d1 == `VI_DSP_CFG)
				? pi_viData_d1[`VI_DSP_CFG_REG_INT] : vi_nintl;
			// gun modes are done above
			vi_dlr <=  (pi_viAddr_d1 == `VI_DSP_CFG) 
				? pi_viData_d1[`VI_DSP_CFG_REG_DLR] : vi_dlr;
			vi_mode <=  (pi_viAddr_d1 == `VI_DSP_CFG) 
				? pi_viData_d1[`VI_DSP_CFG_REG_MODE] : vi_mode; 
	// Horizontal Timing 0 Register
			hlw <=  (pi_viAddr_d1 == `VI_HOR_TIM0_L)
				? pi_viData_d1[`VI_HOR_TIM0_L_REG_HLW] : hlw;
			hce <=  (pi_viAddr_d1 == `VI_HOR_TIM0_U)
				? pi_viData_d1[`VI_HOR_TIM0_U_REG_HCE] : hce;
			hcs <=  (pi_viAddr_d1 == `VI_HOR_TIM0_U)
				? pi_viData_d1[`VI_HOR_TIM0_U_REG_HCS] : hcs;

	// Horizontal Timing 1 Register
			hsy <=  (pi_viAddr_d1 == `VI_HOR_TIM1_L)
				? pi_viData_d1[`VI_HOR_TIM1_L_REG_HSY] : hsy;
			hbe[8:0] <=  (pi_viAddr_d1 == `VI_HOR_TIM1_L)
				? pi_viData_d1[`VI_HOR_TIM1_L_REG_HBE_L] : hbe[8:0];
			hbe[9] <=  (pi_viAddr_d1 == `VI_HOR_TIM1_U)
				? pi_viData_d1[`VI_HOR_TIM1_U_REG_HBE_U] : hbe[9];
			hbs <=  (pi_viAddr_d1 == `VI_HOR_TIM1_U)
				? pi_viData_d1[`VI_HOR_TIM1_U_REG_HBS] : hbs;

	// Vertical Timing Register		
			equ <=  (pi_viAddr_d1 == `VI_VER_TIM)
				? pi_viData_d1[`VI_VER_TIM_REG_EQU] : equ;	
			acv <=  (pi_viAddr_d1 == `VI_VER_TIM)
				? pi_viData_d1[`VI_VER_TIM_REG_ACV] : acv;

	// Odd Field Vertical Timing Register
			oprb <=  (pi_viAddr_d1 == `VI_VER_ODD_TIM_L)
				? pi_viData_d1[`VI_VER_ODD_TIM_L_REG_OPRB] : oprb;
			opsb <=  (pi_viAddr_d1 == `VI_VER_ODD_TIM_U)
				? pi_viData_d1[`VI_VER_ODD_TIM_U_REG_OPSB] : opsb;
	
     // Even Field Vertical Timing Register
			eprb <=  (pi_viAddr_d1 == `VI_VER_EVN_TIM_L)
				? pi_viData_d1[`VI_VER_EVN_TIM_L_REG_EPRB] : eprb;
			epsb <=  (pi_viAddr_d1 == `VI_VER_EVN_TIM_U)
				? pi_viData_d1[`VI_VER_EVN_TIM_U_REG_EPSB] : epsb;

 	// Odd Field Burst Blanking Interval Register  
			bs1 <=  (pi_viAddr_d1 == `VI_ODD_BBLNK_INTVL_L)
				? pi_viData_d1[`VI_ODD_BBLNK_INTVL_L_REG_BS1] : bs1;
			be1 <=  (pi_viAddr_d1 == `VI_ODD_BBLNK_INTVL_L)
				? pi_viData_d1[`VI_ODD_BBLNK_INTVL_L_REG_BE1] : be1;
			bs3 <=  (pi_viAddr_d1 == `VI_ODD_BBLNK_INTVL_U)
				? pi_viData_d1[`VI_ODD_BBLNK_INTVL_U_REG_BS3] : bs3;
			be3 <=  (pi_viAddr_d1 == `VI_ODD_BBLNK_INTVL_U)
				? pi_viData_d1[`VI_ODD_BBLNK_INTVL_U_REG_BE3] : be3;

	// Even Field Burst Blanking Interval Register  
			bs2 <=  (pi_viAddr_d1 == `VI_EVN_BBLNK_INTVL_L)
				? pi_viData_d1[`VI_EVN_BBLNK_INTVL_L_REG_BS2] : bs2;
			be2 <=  (pi_viAddr_d1 == `VI_EVN_BBLNK_INTVL_L)
				? pi_viData_d1[`VI_EVN_BBLNK_INTVL_L_REG_BE2] : be2;
			bs4 <=  (pi_viAddr_d1 == `VI_EVN_BBLNK_INTVL_U)
				? pi_viData_d1[`VI_EVN_BBLNK_INTVL_U_REG_BS4] : bs4;
			be4 <=  (pi_viAddr_d1 == `VI_EVN_BBLNK_INTVL_U)
				? pi_viData_d1[`VI_EVN_BBLNK_INTVL_U_REG_BE4] : be4;

	// Picture Base Register L
			fbb_lft[15:0] <=  (pi_viAddr_d1 == `VI_PIC_BASE_LFT_L) 
				? pi_viData_d1[`VI_PIC_BASE_LFT_L_REG_FBB_LFT_L] : fbb_lft[15:0];
			fbb_lft[23:16] <=  (pi_viAddr_d1 == `VI_PIC_BASE_LFT_U)
				? pi_viData_d1[`VI_PIC_BASE_LFT_U_REG_FBB_LFT_U] : fbb_lft[23:16];
			xoff <= #1 (pi_viAddr_d1 == `VI_PIC_BASE_LFT_U)
				? pi_viData_d1[`VI_PIC_BASE_LFT_U_REG_XOF] : xoff;
			high_addr <= (pi_viAddr_d1 == `VI_PIC_BASE_LFT_U)
                                ? pi_viData_d1[`VI_PIC_BASE_LFT_U_REG_HIGH_ADDR] : high_addr;
	// Picture Base Register R
			fbb_rgt[15:0] <=  (pi_viAddr_d1 == `VI_PIC_BASE_RGT_L)
				? pi_viData_d1[`VI_PIC_BASE_RGT_L_REG_FBB_RGT_L] : fbb_rgt[15:0];
			fbb_rgt[23:16] <=  (pi_viAddr_d1 == `VI_PIC_BASE_RGT_U)
				? pi_viData_d1[`VI_PIC_BASE_RGT_U_REG_FBB_RGT_U] : fbb_rgt[23:16];

	// Picture Base Register L Bottom Field
			fbb_lft_bot[15:0] <= (pi_viAddr_d1 == `VI_PIC_BASE_LFT_BOT_L) 
				? pi_viData_d1[`VI_PIC_BASE_LFT_BOT_L_REG_FBB_LFT_BOT_L] : fbb_lft_bot[15:0];
			fbb_lft_bot[23:16] <=  (pi_viAddr_d1 == `VI_PIC_BASE_LFT_BOT_U)
				? pi_viData_d1[`VI_PIC_BASE_LFT_BOT_U_REG_FBB_LFT_BOT_U] : fbb_lft_bot[23:16];

	// Picture Base Register R Bottom Field
			fbb_rgt_bot[15:0] <=  (pi_viAddr_d1 == `VI_PIC_BASE_RGT_BOT_L)
				? pi_viData_d1[`VI_PIC_BASE_RGT_BOT_L_REG_FBB_RGT_BOT_L] : fbb_rgt_bot[15:0];
			fbb_rgt_bot[23:16] <=  (pi_viAddr_d1 == `VI_PIC_BASE_RGT_BOT_U)
				? pi_viData_d1[`VI_PIC_BASE_RGT_BOT_U_REG_FBB_RGT_BOT_U] : fbb_rgt_bot[23:16];
		
	// Picture Configuration Register
			std <=  (pi_viAddr_d1 == `VI_PIC_CFG) 
				? pi_viData_d1[`VI_PIC_CFG_REG_STD] : std;
			wpl <=  (pi_viAddr_d1 == `VI_PIC_CFG) 
				? pi_viData_d1[`VI_PIC_CFG_REG_WPL] : wpl;


	/* READ ONLY
	// Display Postion Register			
			hct <=  (pi_viAddr_d1 == `VI_DSP_POS_L) 
				? pi_viData_d1[`VI_DSP_POS_L_REG_HCT] : hct;
			vct <=  (pi_viAddr_d1 == `VI_DSP_POS_U) 
				? pi_viData_d1[`VI_DSP_POS_U_REG_VCT] : vct;
	*/

	// Display Interrupt Register 0

			hct0 <=  (pi_viAddr_d1 == `VI_DSP_INT0_L)
				? pi_viData_d1[`VI_DSP_INT0_L_REG_HCT0] : hct0;
			vct0 <=  (pi_viAddr_d1 == `VI_DSP_INT0_U)
				? pi_viData_d1[`VI_DSP_INT0_U_REG_VCT0] : vct0;
			enb0 <=  (pi_viAddr_d1 == `VI_DSP_INT0_U)
				? pi_viData_d1[`VI_DSP_INT0_U_REG_ENB0] : enb0;
	// writing a zero to the int0 trigger regsiter clears the trigger flag
	// decoding for this is done above

	// Display Interrupt Register 1			
			hct1 <=  (pi_viAddr_d1 == `VI_DSP_INT1_L)
				? pi_viData_d1[`VI_DSP_INT1_L_REG_HCT1] : hct1;
			vct1 <=  (pi_viAddr_d1 == `VI_DSP_INT1_U)
				? pi_viData_d1[`VI_DSP_INT1_U_REG_VCT1] : vct1;
			enb1 <=  (pi_viAddr_d1 == `VI_DSP_INT1_U)
				? pi_viData_d1[`VI_DSP_INT1_U_REG_ENB1] : enb1;
	// writing a zero to the int1 trigger regsiter clears the trigger flag
	// decoding for this is done above

	// Display Interrupt Register 2
			hct2 <=  (pi_viAddr_d1 == `VI_DSP_INT2_L)
				? pi_viData_d1[`VI_DSP_INT2_L_REG_HCT2] : hct2;
			vct2 <=  (pi_viAddr_d1 == `VI_DSP_INT2_U)
				? pi_viData_d1[`VI_DSP_INT2_U_REG_VCT2] : vct2;
			enb2 <=  (pi_viAddr_d1 == `VI_DSP_INT2_U)
				? pi_viData_d1[`VI_DSP_INT2_U_REG_ENB2] : enb2;
	// writing a zero to the int2 trigger regsiter clears the trigger flag
	// decoding for this is done above

	// Display Interrupt Register 3
			hct3 <=  (pi_viAddr_d1 == `VI_DSP_INT3_L)
				? pi_viData_d1[`VI_DSP_INT3_L_REG_HCT3] : hct3;
			vct3 <=  (pi_viAddr_d1 == `VI_DSP_INT3_U)
				? pi_viData_d1[`VI_DSP_INT3_U_REG_VCT3] : vct3;
			enb3 <=  (pi_viAddr_d1 == `VI_DSP_INT3_U)
				? pi_viData_d1[`VI_DSP_INT3_U_REG_ENB3] : enb3;
	// writing a zero to the int3 trigger regsiter clears the trigger flag
	// decding for this is done above

	/* READ ONLY
	// Display Latch Register 0
			gun0_hct <=  (pi_viAddr_d1 == `VI_DSP_LATCH0_L)
				? pi_viData_d1[`VI_DSP_LATCH0_L_REG_GUN0_HCT] : gun0_hct;
			gun0_vct <=  (pi_viAddr_d1 == `VI_DSP_LATCH0_U)
				? pi_viData_d1[`VI_DSP_LATCH0_U_REG_GUN0_VCT] : gun0_vct;
	*/

	/* READ ONLY
	// Display Latch Register 1			
			gun1_hct <=  (pi_viAddr_d1 == `VI_DSP_LATCH1_L)
				? pi_viData_d1[`VI_DSP_LATCH1_L_REG_GUN1_HCT] : gun1_hct;
			gun1_vct <=  (pi_viAddr_d1 == `VI_DSP_LATCH1_U)
				? pi_viData_d1[`VI_DSP_LATCH1_U_REG_GUN1_VCT] : gun1_vct;
	*/			

	// Horizontal Scale Register
			step <=  (pi_viAddr_d1 == `VI_HSCALE)
				? pi_viData_d1[`VI_HSCALE_REG_STEP] : step;
			hscaler_en <=  (pi_viAddr_d1 == `VI_HSCALE)
				? pi_viData_d1[`VI_HSCALE_REG_HSCALER_EN] : hscaler_en;
	// Source Width Register
			vi_srcwidth <=  (pi_viAddr_d1 == `VI_WIDTH)
				? pi_viData_d1[`VI_WIDTH_REG_SRCWIDTH] : vi_srcwidth;
	// 656 HBE Register
			hbe656 <= (pi_viAddr_d1 == `VI_HBE656)
                                ? pi_viData_d1[`VI_HBE656_REG_HBE656] : hbe656;
			brdr_en <= (pi_viAddr_d1 == `VI_HBE656)
                                ? pi_viData_d1[`VI_HBE656_REG_BRDR_EN] : brdr_en;
	// 656 HBS Register
			hbs656 <= (pi_viAddr_d1 == `VI_HBS656)
                                ? pi_viData_d1[`VI_HBS656_REG_HBS656] : hbs656;

	// Filter Coefficient Table 0
			T3_0 <=  (pi_viAddr_d1 == `VI_FLTR_COEFF0_L)
				? pi_viData_d1[`VI_FLTR_COEFF0_L_REG_T0] : T3_0;
			T3_1[5:0] <=  (pi_viAddr_d1 == `VI_FLTR_COEFF0_L)
				? pi_viData_d1[`VI_FLTR_COEFF0_L_REG_T1_L] : T3_1[5:0];
			T3_1[9:6] <=  (pi_viAddr_d1 == `VI_FLTR_COEFF0_U)
				? pi_viData_d1[`VI_FLTR_COEFF0_U_REG_T1_U] : T3_1[9:6];
			T3_2 <=  (pi_viAddr_d1 == `VI_FLTR_COEFF0_U)
				? pi_viData_d1[`VI_FLTR_COEFF0_U_REG_T2] : T3_2;
 	// Filter Coefficient Table 1
			T3_3 <=  (pi_viAddr_d1 == `VI_FLTR_COEFF1_L)
				? pi_viData_d1[`VI_FLTR_COEFF1_L_REG_T3] : T3_3;
			T3_4[5:0] <=  (pi_viAddr_d1 == `VI_FLTR_COEFF1_L)
				? pi_viData_d1[`VI_FLTR_COEFF1_L_REG_T4_L] : T3_4[5:0];
			T3_4[9:6] <=  (pi_viAddr_d1 == `VI_FLTR_COEFF1_U)
				? pi_viData_d1[`VI_FLTR_COEFF1_U_REG_T4_U] : T3_4[9:6];
			T3_5 <=  (pi_viAddr_d1 == `VI_FLTR_COEFF1_U)
				? pi_viData_d1[`VI_FLTR_COEFF1_U_REG_T5] : T3_5;
	// Filter Coefficient Table 2
			T3_6 <=  (pi_viAddr_d1 == `VI_FLTR_COEFF2_L)
				? pi_viData_d1[`VI_FLTR_COEFF2_L_REG_T6] : T3_6;
			T3_7[5:0] <=  (pi_viAddr_d1 == `VI_FLTR_COEFF2_L)
				? pi_viData_d1[`VI_FLTR_COEFF2_L_REG_T7_L] : T3_7[5:0];
			T3_7[9:6] <=  (pi_viAddr_d1 == `VI_FLTR_COEFF2_U)
				? pi_viData_d1[`VI_FLTR_COEFF2_U_REG_T7_U] : T3_7[9:6];
			T4_0 <=  (pi_viAddr_d1 == `VI_FLTR_COEFF2_U)
				? pi_viData_d1[`VI_FLTR_COEFF2_U_REG_T8] : T4_0;
	// Filter Coefficient Table 3
			T4_1 <=  (pi_viAddr_d1 == `VI_FLTR_COEFF3_L)
				? pi_viData_d1[`VI_FLTR_COEFF3_L_REG_T9] : T4_1;
			T4_2 <=  (pi_viAddr_d1 == `VI_FLTR_COEFF3_L)
				? pi_viData_d1[`VI_FLTR_COEFF3_L_REG_T10] : T4_2;
			T4_3 <=  (pi_viAddr_d1 == `VI_FLTR_COEFF3_U)
				? pi_viData_d1[`VI_FLTR_COEFF3_U_REG_T11] : T4_3;
			T4_4 <=  (pi_viAddr_d1 == `VI_FLTR_COEFF3_U)
				? pi_viData_d1[`VI_FLTR_COEFF3_U_REG_T12] : T4_4;
	// Filter Coefficient Table 4
			T4_5 <=  (pi_viAddr_d1 == `VI_FLTR_COEFF4_L)
                                ? pi_viData_d1[`VI_FLTR_COEFF4_L_REG_T13] : T4_5;
                        T4_6 <=  (pi_viAddr_d1 == `VI_FLTR_COEFF4_L)
                                ? pi_viData_d1[`VI_FLTR_COEFF4_L_REG_T14] : T4_6;
                        T4_7 <=  (pi_viAddr_d1 == `VI_FLTR_COEFF4_U)
                                ? pi_viData_d1[`VI_FLTR_COEFF4_U_REG_T15] : T4_7;
                        T5_0 <=  (pi_viAddr_d1 == `VI_FLTR_COEFF4_U)
                                ? pi_viData_d1[`VI_FLTR_COEFF4_U_REG_T16] : T5_0;
	// Filter Coefficient Table 5
			T5_1 <=  (pi_viAddr_d1 == `VI_FLTR_COEFF5_L)
                                ? pi_viData_d1[`VI_FLTR_COEFF5_L_REG_T17] : T5_1;
                        T5_2 <=  (pi_viAddr_d1 == `VI_FLTR_COEFF5_L)
                                ? pi_viData_d1[`VI_FLTR_COEFF5_L_REG_T18] : T5_2;
                        T5_3 <=  (pi_viAddr_d1 == `VI_FLTR_COEFF5_U)
                                ? pi_viData_d1[`VI_FLTR_COEFF5_U_REG_T19] : T5_3;
                        T5_4 <=  (pi_viAddr_d1 == `VI_FLTR_COEFF5_U)
				? pi_viData_d1[`VI_FLTR_COEFF5_U_REG_T20] : T5_4;
	// Filter Coefficient Table 6
			T5_5 <=  (pi_viAddr_d1 == `VI_FLTR_COEFF6_L)
                                ? pi_viData_d1[`VI_FLTR_COEFF6_L_REG_T21] : T5_5;
                        T5_6 <=  (pi_viAddr_d1 == `VI_FLTR_COEFF6_L)
                                ? pi_viData_d1[`VI_FLTR_COEFF6_L_REG_T22] : T5_6;
                        T5_7 <=  (pi_viAddr_d1 == `VI_FLTR_COEFF6_U)
                                ? pi_viData_d1[`VI_FLTR_COEFF6_U_REG_T23] : T5_7;
			// Hard wired to zero
                        T24  <= 8'b0; 
	// Output Polarity
			ipol <=  (pi_viAddr_d1 == `VI_OUTPOL)
				 ? pi_viData_d1[`VI_OUTPOL_REG_IPOL] : ipol;
			npol <=  (pi_viAddr_d1 == `VI_OUTPOL)
				 ? pi_viData_d1[`VI_OUTPOL_REG_NPOL] : npol;
			kpol <=  (pi_viAddr_d1 == `VI_OUTPOL)
				 ? pi_viData_d1[`VI_OUTPOL_REG_KPOL] : kpol;
			bpol <=  (pi_viAddr_d1 == `VI_OUTPOL)
				 ? pi_viData_d1[`VI_OUTPOL_REG_BPOL] : bpol;
			hpol <=  (pi_viAddr_d1 == `VI_OUTPOL)
				 ? pi_viData_d1[`VI_OUTPOL_REG_HPOL] : hpol;
			vpol <=  (pi_viAddr_d1 == `VI_OUTPOL)
				 ? pi_viData_d1[`VI_OUTPOL_REG_VPOL] : vpol;
			fpol <=  (pi_viAddr_d1 == `VI_OUTPOL)
				 ? pi_viData_d1[`VI_OUTPOL_REG_FPOL] : fpol;
			cpol <=  (pi_viAddr_d1 == `VI_OUTPOL)
				 ? pi_viData_d1[`VI_OUTPOL_REG_CPOL] : cpol;
			viclksel <= (pi_viAddr_d1 == `VI_CLKSEL)
				? pi_viData_d1[`VI_CLKSEL_REG_VICLKSEL] : viclksel;
		end
	 
		



//always @ (posedge gfxClk)
always @  (read_addr or vi_ver_tim_reg or gen_rd_ack or vi_dsp_cfg_reg or vi_hor_tim0_l_reg or vi_hor_tim0_u_reg
	or vi_hor_tim1_l_reg or vi_hor_tim1_u_reg or vi_ver_odd_tim_l_reg or vi_ver_odd_tim_u_reg or 
	vi_ver_evn_tim_l_reg or vi_ver_evn_tim_u_reg or vi_odd_bblnk_intvl_l_reg or vi_odd_bblnk_intvl_u_reg
	or vi_evn_bblnk_intvl_l_reg or vi_evn_bblnk_intvl_u_reg or vi_pic_base_lft_l_reg or
	vi_pic_base_lft_u_reg or vi_pic_base_rgt_l_reg or vi_pic_base_rgt_u_reg or vi_pic_base_lft_bot_l_reg
	or vi_pic_base_lft_bot_u_reg or vi_pic_base_rgt_bot_l_reg or vi_pic_base_rgt_bot_u_reg or
	vi_hscale_reg or vi_pic_cfg_reg or vi_dsp_pos_l_reg or vi_dsp_pos_u_reg or vi_dsp_int0_l_reg or
	vi_dsp_int0_u_reg or vi_dsp_int1_l_reg or vi_dsp_int1_u_reg or vi_dsp_int2_l_reg or
	vi_dsp_int2_u_reg or vi_dsp_int3_l_reg or vi_dsp_int3_u_reg or vi_dsp_latch0_l_reg or
	vi_dsp_latch0_u_reg or vi_dsp_latch1_l_reg or vi_dsp_latch1_u_reg or vi_fltr_coeff0_l_reg or
	vi_fltr_coeff0_u_reg or vi_fltr_coeff1_l_reg or vi_fltr_coeff1_u_reg or vi_fltr_coeff2_l_reg or
	vi_fltr_coeff2_u_reg or vi_fltr_coeff3_l_reg or vi_fltr_coeff3_u_reg or vi_fltr_coeff4_l_reg or
	vi_fltr_coeff4_u_reg or vi_fltr_coeff5_l_reg or vi_fltr_coeff5_u_reg or vi_fltr_coeff6_l_reg or
	vi_fltr_coeff6_u_reg or vi_outpol_reg or g0hct_Ack_d2 or g0vct_Ack_d2 or g1hct_Ack_d2 or
	g1vct_Ack_d2 or hct_Ack_d2 or vct_Ack_d2 or vi_clksel_reg or vi_dtvstatus_reg or vi_srcwidth_reg or
	vi_hbe656_reg or vi_hbs656_reg) 

	// vi_piData  should be aligned with vi_Ack
	 case (read_addr[8:1])
                `VI_OUTPOL:
                        begin
                                vi_piData_pre = vi_outpol_reg;
                                rd_ack = gen_rd_ack;
                        end
                `VI_VER_TIM :
                        begin
                                vi_piData_pre  = vi_ver_tim_reg;
                                rd_ack = gen_rd_ack;
                        end
                `VI_DSP_CFG :
                        begin
                                vi_piData_pre = vi_dsp_cfg_reg;
                                rd_ack = gen_rd_ack;
                        end
                `VI_HOR_TIM0_L :    
                        begin
                                vi_piData_pre = vi_hor_tim0_l_reg;    
                                rd_ack = gen_rd_ack;
                        end
                `VI_HOR_TIM0_U :    
                        begin
                                vi_piData_pre = vi_hor_tim0_u_reg;    
                                rd_ack = gen_rd_ack;
                        end
                `VI_HOR_TIM1_L :
                        begin
                                vi_piData_pre = vi_hor_tim1_l_reg;
                                rd_ack = gen_rd_ack;
                        end
                `VI_HOR_TIM1_U :
                        begin
                                vi_piData_pre = vi_hor_tim1_u_reg;
                                rd_ack = gen_rd_ack;
                        end
                `VI_VER_ODD_TIM_L :
                        begin
                                vi_piData_pre = vi_ver_odd_tim_l_reg;
                                rd_ack = gen_rd_ack;
                        end
                `VI_VER_ODD_TIM_U :
                        begin
                                vi_piData_pre = vi_ver_odd_tim_u_reg;
                                rd_ack = gen_rd_ack;
                        end
                `VI_VER_EVN_TIM_L : 
                        begin
                                vi_piData_pre = vi_ver_evn_tim_l_reg; 
                                rd_ack = gen_rd_ack;
                        end
                `VI_VER_EVN_TIM_U : 
                        begin
                                vi_piData_pre = vi_ver_evn_tim_u_reg; 
                                rd_ack = gen_rd_ack;
                        end
                `VI_ODD_BBLNK_INTVL_L :
                        begin
                                vi_piData_pre = vi_odd_bblnk_intvl_l_reg;
                                rd_ack = gen_rd_ack;
                        end
                `VI_ODD_BBLNK_INTVL_U :
                        begin
                                vi_piData_pre = vi_odd_bblnk_intvl_u_reg;
                                rd_ack = gen_rd_ack;
                        end
                `VI_EVN_BBLNK_INTVL_L : 
                        begin
                                vi_piData_pre = vi_evn_bblnk_intvl_l_reg; 
                                rd_ack = gen_rd_ack;
                        end
                `VI_EVN_BBLNK_INTVL_U : 
                        begin
                                vi_piData_pre = vi_evn_bblnk_intvl_u_reg;
                                rd_ack = gen_rd_ack;
                        end
                `VI_PIC_BASE_LFT_L :
                        begin
                                vi_piData_pre = vi_pic_base_lft_l_reg;
                                rd_ack = gen_rd_ack;
                        end
                `VI_PIC_BASE_LFT_U :
                        begin
                                vi_piData_pre = vi_pic_base_lft_u_reg;
                                rd_ack = gen_rd_ack;
                        end
                `VI_PIC_BASE_RGT_L :
                        begin
                                vi_piData_pre = vi_pic_base_rgt_l_reg;
                                rd_ack = gen_rd_ack;
                        end
                `VI_PIC_BASE_RGT_U :
                        begin
                                vi_piData_pre = vi_pic_base_rgt_u_reg;
                                rd_ack = gen_rd_ack;
                        end
                `VI_PIC_BASE_LFT_BOT_L :
                        begin
                                vi_piData_pre = vi_pic_base_lft_bot_l_reg;
                                rd_ack = gen_rd_ack;
                        end
                `VI_PIC_BASE_LFT_BOT_U :
                        begin
                                vi_piData_pre = vi_pic_base_lft_bot_u_reg;
                                rd_ack = gen_rd_ack;
                        end
                `VI_PIC_BASE_RGT_BOT_L :
                        begin
                                vi_piData_pre = vi_pic_base_rgt_bot_l_reg;
                                rd_ack = gen_rd_ack;
                        end
                `VI_PIC_BASE_RGT_BOT_U :
                        begin
                                vi_piData_pre = vi_pic_base_rgt_bot_u_reg;
                                rd_ack = gen_rd_ack;
                        end
                `VI_HSCALE :
                        begin
                                vi_piData_pre = vi_hscale_reg;
                                rd_ack = gen_rd_ack;
                        end
                `VI_WIDTH :
                        begin
                                vi_piData_pre = vi_srcwidth_reg;
                                rd_ack = gen_rd_ack;
                        end
                `VI_HBE656 :
                        begin
                                vi_piData_pre = vi_hbe656_reg;
                                rd_ack = gen_rd_ack;
                        end
                `VI_HBS656 :
                        begin
                                vi_piData_pre = vi_hbs656_reg;
                                rd_ack = gen_rd_ack;
                        end
                `VI_PIC_CFG :
                        begin
                                vi_piData_pre = vi_pic_cfg_reg;
                                rd_ack = gen_rd_ack;
                        end
                `VI_DSP_POS_L :
                        begin
                                vi_piData_pre = vi_dsp_pos_l_reg;
                                rd_ack = hct_Ack_d2;
                        end
                `VI_DSP_POS_U :
                        begin
                                vi_piData_pre = vi_dsp_pos_u_reg;
                                rd_ack = vct_Ack_d2;
                        end
                `VI_DSP_INT0_L :
                        begin
                                vi_piData_pre = vi_dsp_int0_l_reg;
                                rd_ack = gen_rd_ack;
                        end
                `VI_DSP_INT0_U :
                        begin
                                vi_piData_pre = vi_dsp_int0_u_reg;
                                rd_ack = gen_rd_ack;
                        end
                `VI_DSP_INT1_L :
                        begin
                                vi_piData_pre = vi_dsp_int1_l_reg;
                                rd_ack = gen_rd_ack;
                        end
                `VI_DSP_INT1_U :
                        begin
                                vi_piData_pre = vi_dsp_int1_u_reg;
                                rd_ack = gen_rd_ack;
                        end
                `VI_DSP_INT2_L :
                        begin
                                vi_piData_pre = vi_dsp_int2_l_reg;
                                rd_ack = gen_rd_ack;
                        end
                `VI_DSP_INT2_U :
                        begin
                                vi_piData_pre = vi_dsp_int2_u_reg;
                                rd_ack = gen_rd_ack;
                        end
                `VI_DSP_INT3_L :
                        begin
                                vi_piData_pre = vi_dsp_int3_l_reg;
                                rd_ack = gen_rd_ack;
                        end
                `VI_DSP_INT3_U :
                        begin
                                vi_piData_pre = vi_dsp_int3_u_reg;
                                rd_ack = gen_rd_ack;
                        end
               `VI_DSP_LATCH0_L :
                        begin
                                vi_piData_pre = vi_dsp_latch0_l_reg; 
                                rd_ack = g0hct_Ack_d2;
                        end
                `VI_DSP_LATCH0_U :
                        begin
                                vi_piData_pre = vi_dsp_latch0_u_reg; 
                                rd_ack = g0vct_Ack_d2;
                        end
                `VI_DSP_LATCH1_L :
                        begin
                                vi_piData_pre = vi_dsp_latch1_l_reg; 
                                rd_ack = g1hct_Ack_d2;
                        end
                `VI_DSP_LATCH1_U :
                        begin
                                vi_piData_pre = vi_dsp_latch1_u_reg; 
                                rd_ack = g1vct_Ack_d2;
                        end
                `VI_FLTR_COEFF0_U:
                        begin
                                vi_piData_pre = vi_fltr_coeff0_u_reg;
                                rd_ack = gen_rd_ack;
                        end
                `VI_FLTR_COEFF0_L:
                        begin
                                vi_piData_pre = vi_fltr_coeff0_l_reg;
                                rd_ack = gen_rd_ack;
                        end
                `VI_FLTR_COEFF1_U:
                        begin
                                vi_piData_pre = vi_fltr_coeff1_u_reg;
                                rd_ack = gen_rd_ack;
                        end
                `VI_FLTR_COEFF1_L:
                        begin
                                vi_piData_pre = vi_fltr_coeff1_l_reg;
                                rd_ack = gen_rd_ack;
                        end
                `VI_FLTR_COEFF2_U:
                        begin
                                vi_piData_pre = vi_fltr_coeff2_u_reg;
                                rd_ack = gen_rd_ack;
                        end
                `VI_FLTR_COEFF2_L:
                        begin
                                vi_piData_pre = vi_fltr_coeff2_l_reg;
                                rd_ack = gen_rd_ack;
                        end
                `VI_FLTR_COEFF3_U:
                        begin
                                vi_piData_pre = vi_fltr_coeff3_u_reg;
                                rd_ack = gen_rd_ack;
                        end
                `VI_FLTR_COEFF3_L:
                        begin
                                vi_piData_pre = vi_fltr_coeff3_l_reg;
                                rd_ack = gen_rd_ack;
                        end
                `VI_FLTR_COEFF4_U:
                        begin
                                vi_piData_pre = vi_fltr_coeff4_u_reg;
                                rd_ack = gen_rd_ack;
                        end
                `VI_FLTR_COEFF4_L:
                        begin
                                vi_piData_pre = vi_fltr_coeff4_l_reg;
                                rd_ack = gen_rd_ack;
                        end
                `VI_FLTR_COEFF5_U:
                        begin
                                vi_piData_pre = vi_fltr_coeff5_u_reg;
                                rd_ack = gen_rd_ack;
                        end
                `VI_FLTR_COEFF5_L:
                        begin
                                vi_piData_pre = vi_fltr_coeff5_l_reg;
                                rd_ack = gen_rd_ack;
                        end
                `VI_FLTR_COEFF6_U:
                        begin
                                vi_piData_pre = vi_fltr_coeff6_u_reg;
                                rd_ack = gen_rd_ack;
                        end
                `VI_FLTR_COEFF6_L:
                        begin
                                vi_piData_pre = vi_fltr_coeff6_l_reg;
                                rd_ack = gen_rd_ack;
                        end
		`VI_CLKSEL:
			begin
				vi_piData_pre = vi_clksel_reg;
                                rd_ack = gen_rd_ack;
			end
		`VI_DTVSTATUS:
			begin
				vi_piData_pre = vi_dtvstatus_reg;
                                rd_ack = gen_rd_ack;
			end						
                default :
                        begin
                                vi_piData_pre = 16'hFF;
                                rd_ack = gen_rd_ack;
                        end
                                
                           
         endcase

assign g0hct_req = pi_read && (pi_viAddr_d1 == `VI_DSP_LATCH0_L);
assign g0vct_req = pi_read && (pi_viAddr_d1 ==`VI_DSP_LATCH0_U);
assign g1hct_req = pi_read && (pi_viAddr_d1 == `VI_DSP_LATCH1_L);
assign g1vct_req = pi_read && (pi_viAddr_d1 == `VI_DSP_LATCH1_U);
assign hct_req = pi_read && (pi_viAddr_d1 == `VI_DSP_POS_L);
assign vct_req = pi_read && (pi_viAddr_d1 == `VI_DSP_POS_U);

// I could use only one write register to save gates, but this
// late into it, I would rather choose the more conservative way 

// ************************* GUN0 HCT ********************************
sync g0hct_ack_sync (.D(g0hct_AckState), .CK(gfxClk), .Q(gfx_g0hct_AckState));
sync g0hct_req_sync (.D(g0hct_ReqState), .CK(vClk), .Q(vid_g0hct_ReqState));
assign g0hct_req_edge = ~vid_g0hct_ReqState_d1 && vid_g0hct_ReqState;
//assign g0hct_Ack = ~gfx_g0hct_AckState_d1 && gfx_g0hct_AckState;
assign g0hct_Ack = gfx_g0hct_AckState_d1 && ~gfx_g0hct_AckState;


always @ (posedge gfxClk)
 begin
	// This is necessary to get the ack aligned with the data
	g0hct_Ack_d1 <= g0hct_Ack;
	g0hct_Ack_d2 <= g0hct_Ack_d1;

        if (vi_reset || ~viResetb_gfx)
                g0hct_ReqState <= 1'b0;
        else if (g0hct_req)
                g0hct_ReqState <= 1'b1;
        else if (gfx_g0hct_AckState)
                g0hct_ReqState <= 1'b0;

        if (g0hct_Ack)
                gfx_g0hct <= g0hct_cap;

        gfx_g0hct_AckState_d1 <= gfx_g0hct_AckState;
 end

always @ (posedge vClk)
 begin
        vid_g0hct_ReqState_d1 <= vid_g0hct_ReqState;

        if (vi_reset_vid || ~viResetb_vid)
         begin
                g0hct_AckState <= 1'b0;
         end
        else if (g0hct_AckState != vid_g0hct_ReqState)
         begin
                g0hct_AckState <= ~g0hct_AckState;
         end

        if (g0hct_req_edge)
                g0hct_cap <= gun0_hct;
 end

// ************************* GUN0 VCT ********************************
sync g0vct_ack_sync (.D(g0vct_AckState), .CK(gfxClk), .Q(gfx_g0vct_AckState));
sync g0vct_req_sync (.D(g0vct_ReqState), .CK(vClk), .Q(vid_g0vct_ReqState));
assign g0vct_req_edge = ~vid_g0vct_ReqState_d1 && vid_g0vct_ReqState;
//assign g0vct_Ack = ~gfx_g0vct_AckState_d1 && gfx_g0vct_AckState;
assign g0vct_Ack = gfx_g0vct_AckState_d1 && ~gfx_g0vct_AckState;


always @ (posedge gfxClk)
 begin
	// This is necessary to get the ack aligned with the data
	g0vct_Ack_d1 <= g0vct_Ack;
	g0vct_Ack_d2 <= g0vct_Ack_d1;

        if (vi_reset || ~viResetb_gfx)
                g0vct_ReqState <= 1'b0;
        else if (g0vct_req)
                g0vct_ReqState <= 1'b1;
        else if (gfx_g0vct_AckState)
                g0vct_ReqState <= 1'b0;

        if (g0vct_Ack)
	 begin
                gfx_g0vct <= g0vct_cap;
		gfx_g0trg <= g0trg_cap;
	 end

        gfx_g0vct_AckState_d1 <= gfx_g0vct_AckState;
 end

always @ (posedge vClk)
 begin
        vid_g0vct_ReqState_d1 <= vid_g0vct_ReqState;

        if (vi_reset_vid || ~viResetb_vid)
         begin
                g0vct_AckState <= 1'b0;
         end
        else if (g0vct_AckState != vid_g0vct_ReqState)
         begin
                g0vct_AckState <= ~g0vct_AckState;
         end

        if (g0vct_req_edge)
	 begin
                g0vct_cap <= gun0_vct;
		g0trg_cap <= gun0_trg;
	end
 end
// ************************* GUN1 HCT ********************************
sync g1hct_ack_sync (.D(g1hct_AckState), .CK(gfxClk), .Q(gfx_g1hct_AckState));
sync g1hct_req_sync (.D(g1hct_ReqState), .CK(vClk), .Q(vid_g1hct_ReqState));
assign g1hct_req_edge = ~vid_g1hct_ReqState_d1 && vid_g1hct_ReqState;
//assign g1hct_Ack = ~gfx_g1hct_AckState_d1 && gfx_g1hct_AckState;
assign g1hct_Ack = gfx_g1hct_AckState_d1 && ~gfx_g1hct_AckState;


always @ (posedge gfxClk)
 begin
	// This is necessary to get the ack aligned with the data
	g1hct_Ack_d1 <= g1hct_Ack;
	g1hct_Ack_d2 <= g1hct_Ack_d1;


        if (vi_reset || ~viResetb_gfx)
                g1hct_ReqState <= 1'b0;
        else if (g1hct_req)
                g1hct_ReqState <= 1'b1;
        else if (gfx_g1hct_AckState)
                g1hct_ReqState <= 1'b0;

        if (g1hct_Ack)
                gfx_g1hct <= g1hct_cap;

        gfx_g1hct_AckState_d1 <= gfx_g1hct_AckState;
 end

always @ (posedge vClk)
 begin
        vid_g1hct_ReqState_d1 <= vid_g1hct_ReqState;

        if (vi_reset_vid || ~viResetb_vid)
         begin
                g1hct_AckState <= 1'b0;
         end
        else if (g1hct_AckState != vid_g1hct_ReqState)
         begin
                g1hct_AckState <= ~g1hct_AckState;
         end

        if (g1hct_req_edge)
                g1hct_cap <= gun1_hct;
 end

// ************************* GUN1 VCT ********************************
sync g1vct_ack_sync (.D(g1vct_AckState), .CK(gfxClk), .Q(gfx_g1vct_AckState));
sync g1vct_req_sync (.D(g1vct_ReqState), .CK(vClk), .Q(vid_g1vct_ReqState));
assign g1vct_req_edge = ~vid_g1vct_ReqState_d1 && vid_g1vct_ReqState;
//assign g1vct_Ack = ~gfx_g1vct_AckState_d1 && gfx_g1vct_AckState;
assign g1vct_Ack = gfx_g1vct_AckState_d1 && ~gfx_g1vct_AckState;


always @ (posedge gfxClk)
 begin
	// This is necessary to get the ack aligned with the data
	g1vct_Ack_d1 <= g1vct_Ack;
	g1vct_Ack_d2 <= g1vct_Ack_d1;

        if (vi_reset || ~viResetb_gfx)
                g1vct_ReqState <= 1'b0;
        else if (g1vct_req)
                g1vct_ReqState <= 1'b1;
        else if (gfx_g1vct_AckState)
                g1vct_ReqState <= 1'b0;

        if (g1vct_Ack)
	 begin
                gfx_g1vct <= g1vct_cap;
		gfx_g1trg <= g1trg_cap;
	 end

        gfx_g1vct_AckState_d1 <= gfx_g1vct_AckState;
 end

always @ (posedge vClk)
 begin
        vid_g1vct_ReqState_d1 <= vid_g1vct_ReqState;

        if (vi_reset_vid || ~viResetb_vid)
         begin
                g1vct_AckState <= 1'b0;
         end
        else if (g1vct_AckState != vid_g1vct_ReqState)
         begin
                g1vct_AckState <= ~g1vct_AckState;
         end

        if (g1vct_req_edge)
	 begin
                g1vct_cap <= gun1_vct;
		g1trg_cap <= gun1_trg;
	end
 end


// *************************  HCT ********************************
sync hct_ack_sync (.D(hct_AckState), .CK(gfxClk), .Q(gfx_hct_AckState));
sync hct_req_sync (.D(hct_ReqState), .CK(vClk), .Q(vid_hct_ReqState));
assign hct_req_edge = ~vid_hct_ReqState_d1 && vid_hct_ReqState;
//assign hct_Ack = ~gfx_hct_AckState_d1 && gfx_hct_AckState;
assign hct_Ack = gfx_hct_AckState_d1 && ~gfx_hct_AckState;
assign pos_cap = (hct_req_edge || vct_req_edge) && ~pos_rd_pending;

always @ (posedge gfxClk)
 begin
	// This is necessary to get the ack aligned with the data
	hct_Ack_d1 <= hct_Ack;
	hct_Ack_d2 <= hct_Ack_d1;

        if (vi_reset || ~viResetb_gfx)
                hct_ReqState <= 1'b0;
        else if (hct_req)
                hct_ReqState <= 1'b1;
        else if (gfx_hct_AckState)
                hct_ReqState <= 1'b0;

        if (hct_Ack)
                gfx_hct <= hct_cap;

        gfx_hct_AckState_d1 <= gfx_hct_AckState;
 end

always @ (posedge vClk)
 begin

	if (~viResetb_vid || vi_reset_vid)
		pos_rd_pending <= 1'b0;
	else if (pos_rd_pending && (hct_req_edge || vct_req_edge))
	 	pos_rd_pending <= 1'b0;
	else if (~pos_rd_pending && (hct_req_edge || vct_req_edge))
		pos_rd_pending <= 1'b1;

        vid_hct_ReqState_d1 <= vid_hct_ReqState;

        if (vi_reset_vid || ~viResetb_vid)
         begin
                hct_AckState <= 1'b0;
         end
        else if (hct_AckState != vid_hct_ReqState)
         begin
                hct_AckState <= ~hct_AckState;
         end
	
	// The capturing mechanism is explained below
        if (pos_cap)
                hct_cap <= hct;
 end

// ************************* VCT ********************************
sync vct_ack_sync (.D(vct_AckState), .CK(gfxClk), .Q(gfx_vct_AckState));
sync vct_req_sync (.D(vct_ReqState), .CK(vClk), .Q(vid_vct_ReqState));
assign vct_req_edge = ~vid_vct_ReqState_d1 && vid_vct_ReqState;
//assign vct_Ack = ~gfx_vct_AckState_d1 && gfx_vct_AckState;
assign vct_Ack = gfx_vct_AckState_d1 && ~gfx_vct_AckState;


always @ (posedge gfxClk)
 begin

	// This is necessary to get the ack aligned with the data
	vct_Ack_d1 <= vct_Ack;
	vct_Ack_d2 <= vct_Ack_d1;

        if (vi_reset || ~viResetb_gfx)
                vct_ReqState <= 1'b0;
        else if (vct_req)
                vct_ReqState <= 1'b1;
        else if (gfx_vct_AckState)
                vct_ReqState <= 1'b0;

        if (vct_Ack)
	 begin
                gfx_vct <= vct_cap;
	 end

        gfx_vct_AckState_d1 <= gfx_vct_AckState;
 end

always @ (posedge vClk)
 begin
        vid_vct_ReqState_d1 <= vid_vct_ReqState;

        if (vi_reset_vid || ~viResetb_vid)
         begin
                vct_AckState <= 1'b0;
         end
        else if (vct_AckState != vid_vct_ReqState)
         begin
                vct_AckState <= ~vct_AckState;
         end

	// This captures hct and vct at the same time
	// This assumes that there will be two reads of 
	// the position registers (a total of two reads of 
	// any of the two registers- dsp_pos_u or dsp_pos_l
	// i.e. 2 reads of dsp_pos_u will work as well as
	// 1 rd of dsp_pos_u and one of dsp_pos_l.
	// The first read captures the data, and the second
	// read, reads back the captured data. Then the cycle
	// begins again, the next read captures, the read after
	// that reads back captured data, etc...

        if (pos_cap)
	 begin
                vct_cap <= vct;
	end
 end



endmodule  // vi_pi	
