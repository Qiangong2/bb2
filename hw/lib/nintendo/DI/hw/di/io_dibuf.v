/*****************************************************************************
 * 
 *   (C) 1999 ARTX INC..  ALL RIGHTS RESERVED.  UNPUBLISHED -- RIGHTS
 *   RESERVED UNDER THE COPYRIGHT LAWS OF THE UNITED STATES.  USE OF A
 *   COPYRIGHT NOTICE IS PRECAUTIONARY ONLY AND DOES NOT IMPLY PUBLICATION
 *   OR DISCLOSURE.
 *
 *   THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF
 *   ARTX INC..  USE, DISCLOSURE, OR REPRODUCTION IS PROHIBITED WITHOUT
 *   THE PRIOR EXPRESS WRITTEN PERMISSION OF ARTX INC..
 *
 *                   RESTRICTED RIGHTS LEGEND
 *
 *   Use, duplication, or disclosure by the Government is subject to
 *   restrictions as set forth in subparagraph (c)(1)(ii) of the Rights
 *   in Technical Data and Computer Software clause at DFARS 252.227-7013
 *   or subparagraphs (c)(1) and (2) of Commercial Computer Software --
 *   Restricted Rights at 48 CFR 52.227-19, as applicable. 
 *
 *   ArtX Inc.
 *   3400 Hillview Ave, Bldg 5
 *   Palo Alto, CA 94304
 *
 ****************************************************************************/

// syn myclk = io_clk

module io_dibuf (/*AUTOARG*/
    // Output to io_dipctl
    b2p_rddata,

    // Output to io_didctl
    b2d_rddata,

    // General Input
    io_clk, resetb,

    // Input from io_dipctl
    PiData, p2b_cmdadr, p2b_immadr, piwrite,

    // Input from io_didctl
    d2b_wrdata, d2b_immrdadr, d2b_immwradr, d2b_immwr, d2b_cmdadr
);

    /*
     * Input, Output, Inout Ports declaration
     */
    // To io_dipctl
    output [15:0]
	b2p_rddata;		// Data for CPU from command/immed buffers

    // To io_didctl
    output [7:0]
	b2d_rddata;		// Read data from cmd/immed buffers

    // General inputs
    input
	io_clk;			// Clock

    input
	resetb;			// Reset, low active

    // From io_dipctl
    input [15:0]
    	PiData;			// Data from CPU for command/immed buffers

    input [5:0]
	p2b_cmdadr;		// Read/Write address for command buffers

    input [1:0]
	p2b_immadr;		// Read/Write address for immed buffers

    input
   	piwrite;		// Write signal for command/immed buffers

    // From io_didctl
    input [7:0]
	d2b_wrdata;		// Data for immediate buffer

    input [3:0]
	d2b_immwradr,		// Write address for immed buffer
	d2b_immrdadr;		// Read address for immed buffer
				// Bit3 for msb, bit0 for lsb

    input
	d2b_immwr;		// Write signal for immed buffer

    input [11:0]
	d2b_cmdadr;		// Read/Write address for command buffers


   /*
    * Wires, Registers declaration
    */
    reg [31:0]
	CMD0_reg,		// Command Buffer 0
	CMD1_reg,		// Command Buffer 1
	CMD2_reg;		// Command Buffer 2

    reg [31:0]
	IMM_reg;		// Immediate Buffer

   /*
    * Assignments
    */
    // For PI read register data
    assign
	// Big endian, 8-1 mux for output data
	b2p_rddata = p2b_cmdadr[5] ? CMD2_reg[31:16] :
		     p2b_cmdadr[4] ? CMD2_reg[15:0]  :
		     p2b_cmdadr[3] ? CMD1_reg[31:16] :
		     p2b_cmdadr[2] ? CMD1_reg[15:0]  :
		     p2b_cmdadr[1] ? CMD0_reg[31:16] :
		     p2b_cmdadr[0] ? CMD0_reg[15:0]  :
		     p2b_immadr[1] ? IMM_reg[31:16]  :
		                     IMM_reg[15:0];

    // For device read data
    assign
	b2d_rddata = d2b_cmdadr[8]  ? CMD2_reg[31:24] :
		     d2b_cmdadr[9]  ? CMD2_reg[23:16] :
		     d2b_cmdadr[10] ? CMD2_reg[15: 8] :
		     d2b_cmdadr[11] ? CMD2_reg[ 7: 0] :
	             d2b_cmdadr[4]  ? CMD1_reg[31:24] :
		     d2b_cmdadr[5]  ? CMD1_reg[23:16] :
		     d2b_cmdadr[6]  ? CMD1_reg[15: 8] :
		     d2b_cmdadr[7]  ? CMD1_reg[ 7: 0] :
	             d2b_cmdadr[0]  ? CMD0_reg[31:24] :
		     d2b_cmdadr[1]  ? CMD0_reg[23:16] :
		     d2b_cmdadr[2]  ? CMD0_reg[15: 8] :
		     d2b_cmdadr[3]  ? CMD0_reg[ 7: 0] :
	             d2b_immrdadr[0] ? IMM_reg[31:24] :
	             d2b_immrdadr[1] ? IMM_reg[23:16] :
	             d2b_immrdadr[2] ? IMM_reg[15: 8] :
				       IMM_reg[ 7: 0];
	


   /*
    * Always Block
    */
    always @(posedge io_clk)
      if (~resetb)
	begin
	  CMD0_reg <= 32'h0;
	  CMD1_reg <= 32'h0;
	  CMD2_reg <= 32'h0;
	end
      else
	begin
	  // Written by CPU via io_dipctl wradr and wr signals
	  // Written by device via io_didctl
	  CMD0_reg[31:24] <= (p2b_cmdadr[1] & piwrite) 	? PiData[15:8] 
						     	: CMD0_reg[31:24];
	  CMD0_reg[23:16] <= (p2b_cmdadr[1] & piwrite) 	? PiData[7:0]
						     	: CMD0_reg[23:16];
	  CMD0_reg[15: 8] <= (p2b_cmdadr[0] & piwrite) 	? PiData[15:8]
							: CMD0_reg[15:8];
	  CMD0_reg[ 7: 0] <= (p2b_cmdadr[0] & piwrite) 	? PiData[7:0]
							: CMD0_reg[7:0];

	  CMD1_reg[31:24] <= (p2b_cmdadr[3] & piwrite) 	? PiData[15:8]
						     	: CMD1_reg[31:24];
	  CMD1_reg[23:16] <= (p2b_cmdadr[3] & piwrite) 	? PiData[7:0]
						     	: CMD1_reg[23:16];
	  CMD1_reg[15: 8] <= (p2b_cmdadr[2] & piwrite) 	? PiData[15:8]
							: CMD1_reg[15:8];
	  CMD1_reg[ 7: 0] <= (p2b_cmdadr[2] & piwrite) 	? PiData[7:0]
							: CMD1_reg[7:0];

	  CMD2_reg[31:24] <= (p2b_cmdadr[5] & piwrite) 	? PiData[15:8]
						     	: CMD2_reg[31:24];
	  CMD2_reg[23:16] <= (p2b_cmdadr[5] & piwrite) 	? PiData[7:0]
						     	: CMD2_reg[23:16];
	  CMD2_reg[15: 8] <= (p2b_cmdadr[4] & piwrite) 	? PiData[15:8]
							: CMD2_reg[15:8];
	  CMD2_reg[ 7: 0] <= (p2b_cmdadr[4] & piwrite) 	? PiData[7:0]
							: CMD2_reg[7:0];

	end

    always @(posedge io_clk)
      if (~resetb)
	begin
	  IMM_reg <= 32'h0;
	end
      else
	begin
	  IMM_reg[31:24] <=  (p2b_immadr[1] & piwrite) 	? PiData[15:8] :
			  (d2b_immwradr[0] & d2b_immwr) ? d2b_wrdata
						     	: IMM_reg[31:24];
	  IMM_reg[23:16] <= (p2b_immadr[1] & piwrite) 	? PiData[7:0] :
			  (d2b_immwradr[1] & d2b_immwr) ? d2b_wrdata
						     	: IMM_reg[23:16];
	  IMM_reg[15: 8] <= (p2b_immadr[0] & piwrite) 	? PiData[15:8] :
			  (d2b_immwradr[2] & d2b_immwr) ? d2b_wrdata
							: IMM_reg[15:8];
	  IMM_reg[ 7: 0] <= (p2b_immadr[0] & piwrite) 	? PiData[7:0] :
			  (d2b_immwradr[3] & d2b_immwr) ? d2b_wrdata
							: IMM_reg[7:0];

	end


endmodule // io_dibuf





