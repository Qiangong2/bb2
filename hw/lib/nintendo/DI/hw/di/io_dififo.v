/*****************************************************************************
 * 
 *   (C) 1999 ARTX INC..  ALL RIGHTS RESERVED.  UNPUBLISHED -- RIGHTS
 *   RESERVED UNDER THE COPYRIGHT LAWS OF THE UNITED STATES.  USE OF A
 *   COPYRIGHT NOTICE IS PRECAUTIONARY ONLY AND DOES NOT IMPLY PUBLICATION
 *   OR DISCLOSURE.
 *
 *   THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF
 *   ARTX INC..  USE, DISCLOSURE, OR REPRODUCTION IS PROHIBITED WITHOUT
 *   THE PRIOR EXPRESS WRITTEN PERMISSION OF ARTX INC..
 *
 *                   RESTRICTED RIGHTS LEGEND
 *
 *   Use, duplication, or disclosure by the Government is subject to
 *   restrictions as set forth in subparagraph (c)(1)(ii) of the Rights
 *   in Technical Data and Computer Software clause at DFARS 252.227-7013
 *   or subparagraphs (c)(1) and (2) of Commercial Computer Software --
 *   Restricted Rights at 48 CFR 52.227-19, as applicable. 
 *
 *   ArtX Inc.
 *   3400 Hillview Ave, Bldg 5
 *   Palo Alto, CA 94304
 *
 ****************************************************************************/

// syn myclk = io_clk

module io_dififo (/*AUTOARG*/
    // Output to io_dimctl
    f2m_rddata,

    // Output to io_didctl
    f2d_rddata,

    // General Input
    io_clk,

    // Input from io_dimctl
    MemData, m2f_adr, m2f_wr,

    // Input from io_didctl
    d2f_wrdata, d2f_wradr, d2f_rdadr, d2f_wr
);

    /*
     * Input, Output, Inout Ports declaration
     */
    // Output to io_dimctl
    output [63:0]
    	f2m_rddata;		// Data from DMA FIFO to memory

    output[7:0]
	f2d_rddata;		// Data to device from DMA FIFO

    // General inputs
    input
	io_clk;			// Clock

    // From io_dimctl
    input [63:0]
    	MemData;		// Mem data sent to DMA fifo

    input [2:0]
	m2f_adr;		// DMA fifo address for read/write

    input
	m2f_wr;			// Write signal from memory

    // From io_didctl
    input [7:0]
	d2f_wrdata;		// Device Data to DMA fifo

    input [5:0]
	d2f_wradr,		// Write address for DMA fifo
	d2f_rdadr;		// Read address for DMA fifo

    input
	d2f_wr;			// Write signal from device

   /*
    * Wires, Registers declaration
    */
    reg [63:0]
	fifo0_reg,		// FIFO0
	fifo1_reg,		// FIFO1
	fifo2_reg,		// FIFO2
	fifo3_reg;		// FIFO3

    reg [63:0]
	fifo4_reg,		// FIFO4
	fifo5_reg,		// FIFO5
	fifo6_reg,		// FIFO6
	fifo7_reg;		// FIFO7

    reg [7:0]
	fifo0mx,		// 8 bit mux output from fifo0_reg
	fifo1mx,		// 8 bit mux output from fifo1_reg
	fifo2mx,		// 8 bit mux output from fifo2_reg
	fifo3mx;		// 8 bit mux output from fifo3_reg

    reg [7:0]
	fifo4mx,		// 8 bit mux output from fifo4_reg
	fifo5mx,		// 8 bit mux output from fifo5_reg
	fifo6mx,		// 8 bit mux output from fifo6_reg
	fifo7mx;		// 8 bit mux output from fifo7_reg

    reg [7:0]
	f2d_rddata;		// Data to device from DMA FIFO

    wire [7:0]			// One bit per byte, 8 bytes per entry
	mem0_wr,		// Write From Memory
	mem1_wr,		// Write From Memory
	mem2_wr,		// Write From Memory
	mem3_wr,		// Write From Memory
	dvd0_wr,		// Write From DVD
	dvd1_wr,		// Write From DVD
	dvd2_wr,		// Write From DVD
	dvd3_wr;		// Write From DVD

    wire [7:0]			// One bit per byte, 8 bytes per entry
	mem4_wr,		// Write From Memory
	mem5_wr,		// Write From Memory
	mem6_wr,		// Write From Memory
	mem7_wr,		// Write From Memory
	dvd4_wr,		// Write From DVD
	dvd5_wr,		// Write From DVD
	dvd6_wr,		// Write From DVD
	dvd7_wr;		// Write From DVD

   /*
    * Assignments
    */
    assign
	// 64 bits data bus for sending to main memory
	f2m_rddata = m2f_adr[2] ? 
			( m2f_adr[1] ? (m2f_adr[0] ? fifo7_reg : fifo6_reg) :
				       (m2f_adr[0] ? fifo5_reg : fifo4_reg) ) :
				
		        ( m2f_adr[1] ? (m2f_adr[0] ? fifo3_reg : fifo2_reg) :
				       (m2f_adr[0] ? fifo1_reg : fifo0_reg) );

    assign
	mem0_wr = {8{m2f_wr & (m2f_adr == 3'b000)}},
	mem1_wr = {8{m2f_wr & (m2f_adr == 3'b001)}},
	mem2_wr = {8{m2f_wr & (m2f_adr == 3'b010)}},
	mem3_wr = {8{m2f_wr & (m2f_adr == 3'b011)}};

    assign
	mem4_wr = {8{m2f_wr & (m2f_adr == 3'b100)}},
	mem5_wr = {8{m2f_wr & (m2f_adr == 3'b101)}},
	mem6_wr = {8{m2f_wr & (m2f_adr == 3'b110)}},
	mem7_wr = {8{m2f_wr & (m2f_adr == 3'b111)}};

    assign
	dvd0_wr[7] = d2f_wr & (d2f_wradr == 6'h0),
	dvd0_wr[6] = d2f_wr & (d2f_wradr == 6'h1),
	dvd0_wr[5] = d2f_wr & (d2f_wradr == 6'h2),
	dvd0_wr[4] = d2f_wr & (d2f_wradr == 6'h3),
	dvd0_wr[3] = d2f_wr & (d2f_wradr == 6'h4),
	dvd0_wr[2] = d2f_wr & (d2f_wradr == 6'h5),
	dvd0_wr[1] = d2f_wr & (d2f_wradr == 6'h6),
	dvd0_wr[0] = d2f_wr & (d2f_wradr == 6'h7),
	dvd1_wr[7] = d2f_wr & (d2f_wradr == 6'h8),
	dvd1_wr[6] = d2f_wr & (d2f_wradr == 6'h9),
	dvd1_wr[5] = d2f_wr & (d2f_wradr == 6'ha),
	dvd1_wr[4] = d2f_wr & (d2f_wradr == 6'hb),
	dvd1_wr[3] = d2f_wr & (d2f_wradr == 6'hc),
	dvd1_wr[2] = d2f_wr & (d2f_wradr == 6'hd),
	dvd1_wr[1] = d2f_wr & (d2f_wradr == 6'he),
	dvd1_wr[0] = d2f_wr & (d2f_wradr == 6'hf),
	dvd2_wr[7] = d2f_wr & (d2f_wradr == 6'h10),
	dvd2_wr[6] = d2f_wr & (d2f_wradr == 6'h11),
	dvd2_wr[5] = d2f_wr & (d2f_wradr == 6'h12),
	dvd2_wr[4] = d2f_wr & (d2f_wradr == 6'h13),
	dvd2_wr[3] = d2f_wr & (d2f_wradr == 6'h14),
	dvd2_wr[2] = d2f_wr & (d2f_wradr == 6'h15),
	dvd2_wr[1] = d2f_wr & (d2f_wradr == 6'h16),
	dvd2_wr[0] = d2f_wr & (d2f_wradr == 6'h17),
	dvd3_wr[7] = d2f_wr & (d2f_wradr == 6'h18),
	dvd3_wr[6] = d2f_wr & (d2f_wradr == 6'h19),
	dvd3_wr[5] = d2f_wr & (d2f_wradr == 6'h1a),
	dvd3_wr[4] = d2f_wr & (d2f_wradr == 6'h1b),
	dvd3_wr[3] = d2f_wr & (d2f_wradr == 6'h1c),
	dvd3_wr[2] = d2f_wr & (d2f_wradr == 6'h1d),
	dvd3_wr[1] = d2f_wr & (d2f_wradr == 6'h1e),
	dvd3_wr[0] = d2f_wr & (d2f_wradr == 6'h1f);

    assign
	dvd4_wr[7] = d2f_wr & (d2f_wradr == 6'h20),
	dvd4_wr[6] = d2f_wr & (d2f_wradr == 6'h21),
	dvd4_wr[5] = d2f_wr & (d2f_wradr == 6'h22),
	dvd4_wr[4] = d2f_wr & (d2f_wradr == 6'h23),
	dvd4_wr[3] = d2f_wr & (d2f_wradr == 6'h24),
	dvd4_wr[2] = d2f_wr & (d2f_wradr == 6'h25),
	dvd4_wr[1] = d2f_wr & (d2f_wradr == 6'h26),
	dvd4_wr[0] = d2f_wr & (d2f_wradr == 6'h27),
	dvd5_wr[7] = d2f_wr & (d2f_wradr == 6'h28),
	dvd5_wr[6] = d2f_wr & (d2f_wradr == 6'h29),
	dvd5_wr[5] = d2f_wr & (d2f_wradr == 6'h2a),
	dvd5_wr[4] = d2f_wr & (d2f_wradr == 6'h2b),
	dvd5_wr[3] = d2f_wr & (d2f_wradr == 6'h2c),
	dvd5_wr[2] = d2f_wr & (d2f_wradr == 6'h2d),
	dvd5_wr[1] = d2f_wr & (d2f_wradr == 6'h2e),
	dvd5_wr[0] = d2f_wr & (d2f_wradr == 6'h2f),
	dvd6_wr[7] = d2f_wr & (d2f_wradr == 6'h30),
	dvd6_wr[6] = d2f_wr & (d2f_wradr == 6'h31),
	dvd6_wr[5] = d2f_wr & (d2f_wradr == 6'h32),
	dvd6_wr[4] = d2f_wr & (d2f_wradr == 6'h33),
	dvd6_wr[3] = d2f_wr & (d2f_wradr == 6'h34),
	dvd6_wr[2] = d2f_wr & (d2f_wradr == 6'h35),
	dvd6_wr[1] = d2f_wr & (d2f_wradr == 6'h36),
	dvd6_wr[0] = d2f_wr & (d2f_wradr == 6'h37),
	dvd7_wr[7] = d2f_wr & (d2f_wradr == 6'h38),
	dvd7_wr[6] = d2f_wr & (d2f_wradr == 6'h39),
	dvd7_wr[5] = d2f_wr & (d2f_wradr == 6'h3a),
	dvd7_wr[4] = d2f_wr & (d2f_wradr == 6'h3b),
	dvd7_wr[3] = d2f_wr & (d2f_wradr == 6'h3c),
	dvd7_wr[2] = d2f_wr & (d2f_wradr == 6'h3d),
	dvd7_wr[1] = d2f_wr & (d2f_wradr == 6'h3e),
	dvd7_wr[0] = d2f_wr & (d2f_wradr == 6'h3f);


   /*
    * Always Block
    */
    // verilint 488 off
    // verilint 69 off
    // Not all bits are used in sensitivity list
    // No default case, all case covered
    always @(d2f_rdadr or fifo0_reg)
      begin
	// 8 bits data bus for sending to DVD external device
	case(d2f_rdadr[2:0])
	3'b000:	fifo0mx = fifo0_reg[63:56];
	3'b001:	fifo0mx = fifo0_reg[55:48];
	3'b010:	fifo0mx = fifo0_reg[47:40];
	3'b011:	fifo0mx = fifo0_reg[39:32];
	3'b100:	fifo0mx = fifo0_reg[31:24];
	3'b101:	fifo0mx = fifo0_reg[23:16];
	3'b110:	fifo0mx = fifo0_reg[15: 8];
	3'b111:	fifo0mx = fifo0_reg[ 7: 0];
	endcase
      end

    always @(d2f_rdadr or fifo1_reg)
      begin
	// 8 bits data bus for sending to DVD external device
	case(d2f_rdadr[2:0])
	3'b000:	fifo1mx = fifo1_reg[63:56];
	3'b001:	fifo1mx = fifo1_reg[55:48];
	3'b010:	fifo1mx = fifo1_reg[47:40];
	3'b011:	fifo1mx = fifo1_reg[39:32];
	3'b100:	fifo1mx = fifo1_reg[31:24];
	3'b101:	fifo1mx = fifo1_reg[23:16];
	3'b110:	fifo1mx = fifo1_reg[15: 8];
	3'b111:	fifo1mx = fifo1_reg[ 7: 0];
	endcase
      end

    always @(d2f_rdadr or fifo2_reg)
      begin
	// 8 bits data bus for sending to DVD external device
	case(d2f_rdadr[2:0])
	3'b000:	fifo2mx = fifo2_reg[63:56];
	3'b001:	fifo2mx = fifo2_reg[55:48];
	3'b010:	fifo2mx = fifo2_reg[47:40];
	3'b011:	fifo2mx = fifo2_reg[39:32];
	3'b100:	fifo2mx = fifo2_reg[31:24];
	3'b101:	fifo2mx = fifo2_reg[23:16];
	3'b110:	fifo2mx = fifo2_reg[15: 8];
	3'b111:	fifo2mx = fifo2_reg[ 7: 0];
	endcase
      end

    always @(d2f_rdadr or fifo3_reg)
      begin
	// 8 bits data bus for sending to DVD external device
	case(d2f_rdadr[2:0])
	3'b000:	fifo3mx = fifo3_reg[63:56];
	3'b001:	fifo3mx = fifo3_reg[55:48];
	3'b010:	fifo3mx = fifo3_reg[47:40];
	3'b011:	fifo3mx = fifo3_reg[39:32];
	3'b100:	fifo3mx = fifo3_reg[31:24];
	3'b101:	fifo3mx = fifo3_reg[23:16];
	3'b110:	fifo3mx = fifo3_reg[15: 8];
	3'b111:	fifo3mx = fifo3_reg[ 7: 0];
	endcase
      end

    always @(d2f_rdadr or fifo4_reg)
      begin
	// 8 bits data bus for sending to DVD external device
	case(d2f_rdadr[2:0])
	3'b000:	fifo4mx = fifo4_reg[63:56];
	3'b001:	fifo4mx = fifo4_reg[55:48];
	3'b010:	fifo4mx = fifo4_reg[47:40];
	3'b011:	fifo4mx = fifo4_reg[39:32];
	3'b100:	fifo4mx = fifo4_reg[31:24];
	3'b101:	fifo4mx = fifo4_reg[23:16];
	3'b110:	fifo4mx = fifo4_reg[15: 8];
	3'b111:	fifo4mx = fifo4_reg[ 7: 0];
	endcase
      end

    always @(d2f_rdadr or fifo5_reg)
      begin
	// 8 bits data bus for sending to DVD external device
	case(d2f_rdadr[2:0])
	3'b000:	fifo5mx = fifo5_reg[63:56];
	3'b001:	fifo5mx = fifo5_reg[55:48];
	3'b010:	fifo5mx = fifo5_reg[47:40];
	3'b011:	fifo5mx = fifo5_reg[39:32];
	3'b100:	fifo5mx = fifo5_reg[31:24];
	3'b101:	fifo5mx = fifo5_reg[23:16];
	3'b110:	fifo5mx = fifo5_reg[15: 8];
	3'b111:	fifo5mx = fifo5_reg[ 7: 0];
	endcase
      end

    always @(d2f_rdadr or fifo6_reg)
      begin
	// 8 bits data bus for sending to DVD external device
	case(d2f_rdadr[2:0])
	3'b000:	fifo6mx = fifo6_reg[63:56];
	3'b001:	fifo6mx = fifo6_reg[55:48];
	3'b010:	fifo6mx = fifo6_reg[47:40];
	3'b011:	fifo6mx = fifo6_reg[39:32];
	3'b100:	fifo6mx = fifo6_reg[31:24];
	3'b101:	fifo6mx = fifo6_reg[23:16];
	3'b110:	fifo6mx = fifo6_reg[15: 8];
	3'b111:	fifo6mx = fifo6_reg[ 7: 0];
	endcase
      end

    always @(d2f_rdadr or fifo7_reg)
      begin
	// 8 bits data bus for sending to DVD external device
	case(d2f_rdadr[2:0])
	3'b000:	fifo7mx = fifo7_reg[63:56];
	3'b001:	fifo7mx = fifo7_reg[55:48];
	3'b010:	fifo7mx = fifo7_reg[47:40];
	3'b011:	fifo7mx = fifo7_reg[39:32];
	3'b100:	fifo7mx = fifo7_reg[31:24];
	3'b101:	fifo7mx = fifo7_reg[23:16];
	3'b110:	fifo7mx = fifo7_reg[15: 8];
	3'b111:	fifo7mx = fifo7_reg[ 7: 0];
	endcase
      end

    always @(d2f_rdadr or fifo0mx or fifo1mx or fifo2mx or fifo3mx or
	     fifo4mx or fifo5mx or fifo6mx or fifo7mx)
      begin
	// 8 bits data bus for sending to DVD external device
	case(d2f_rdadr[5:3])
	3'b000: 	f2d_rddata = fifo0mx;
	3'b001: 	f2d_rddata = fifo1mx;
	3'b010: 	f2d_rddata = fifo2mx;
	3'b011: 	f2d_rddata = fifo3mx;
	3'b100: 	f2d_rddata = fifo4mx;
	3'b101: 	f2d_rddata = fifo5mx;
	3'b110: 	f2d_rddata = fifo6mx;
	3'b111: 	f2d_rddata = fifo7mx;
	endcase
      end
    // verilint 488 on
    // verilint 69 on

				 	

    always @(posedge io_clk)
      begin
  	fifo0_reg[63:56] <= mem0_wr[7] ? MemData[63:56] :
			    dvd0_wr[7] ? d2f_wrdata     : fifo0_reg[63:56];
	fifo0_reg[55:48] <= mem0_wr[6] ? MemData[55:48] :
			    dvd0_wr[6] ? d2f_wrdata     : fifo0_reg[55:48];
	fifo0_reg[47:40] <= mem0_wr[5] ? MemData[47:40] :
			    dvd0_wr[5] ? d2f_wrdata     : fifo0_reg[47:40];
	fifo0_reg[39:32] <= mem0_wr[4] ? MemData[39:32] :
			    dvd0_wr[4] ? d2f_wrdata     : fifo0_reg[39:32];
	fifo0_reg[31:24] <= mem0_wr[3] ? MemData[31:24] :
			    dvd0_wr[3] ? d2f_wrdata     : fifo0_reg[31:24];
	fifo0_reg[23:16] <= mem0_wr[2] ? MemData[23:16] :
			    dvd0_wr[2] ? d2f_wrdata     : fifo0_reg[23:16];
	fifo0_reg[15: 8] <= mem0_wr[1] ? MemData[15: 8] :
			    dvd0_wr[1] ? d2f_wrdata     : fifo0_reg[15: 8];
	fifo0_reg[ 7: 0] <= mem0_wr[0] ? MemData[ 7: 0] :
			    dvd0_wr[0] ? d2f_wrdata     : fifo0_reg[ 7: 0];

  	fifo1_reg[63:56] <= mem1_wr[7] ? MemData[63:56] :
			    dvd1_wr[7] ? d2f_wrdata     : fifo1_reg[63:56];
	fifo1_reg[55:48] <= mem1_wr[6] ? MemData[55:48] :
			    dvd1_wr[6] ? d2f_wrdata     : fifo1_reg[55:48];
	fifo1_reg[47:40] <= mem1_wr[5] ? MemData[47:40] :
			    dvd1_wr[5] ? d2f_wrdata     : fifo1_reg[47:40];
	fifo1_reg[39:32] <= mem1_wr[4] ? MemData[39:32] :
			    dvd1_wr[4] ? d2f_wrdata     : fifo1_reg[39:32];
	fifo1_reg[31:24] <= mem1_wr[3] ? MemData[31:24] :
			    dvd1_wr[3] ? d2f_wrdata     : fifo1_reg[31:24];
	fifo1_reg[23:16] <= mem1_wr[2] ? MemData[23:16] :
			    dvd1_wr[2] ? d2f_wrdata     : fifo1_reg[23:16];
	fifo1_reg[15: 8] <= mem1_wr[1] ? MemData[15: 8] :
			    dvd1_wr[1] ? d2f_wrdata     : fifo1_reg[15: 8];
	fifo1_reg[ 7: 0] <= mem1_wr[0] ? MemData[ 7: 0] :
			    dvd1_wr[0] ? d2f_wrdata     : fifo1_reg[ 7: 0];

  	fifo2_reg[63:56] <= mem2_wr[7] ? MemData[63:56] :
			    dvd2_wr[7] ? d2f_wrdata     : fifo2_reg[63:56];
	fifo2_reg[55:48] <= mem2_wr[6] ? MemData[55:48] :
			    dvd2_wr[6] ? d2f_wrdata     : fifo2_reg[55:48];
	fifo2_reg[47:40] <= mem2_wr[5] ? MemData[47:40] :
			    dvd2_wr[5] ? d2f_wrdata     : fifo2_reg[47:40];
	fifo2_reg[39:32] <= mem2_wr[4] ? MemData[39:32] :
			    dvd2_wr[4] ? d2f_wrdata     : fifo2_reg[39:32];
	fifo2_reg[31:24] <= mem2_wr[3] ? MemData[31:24] :
			    dvd2_wr[3] ? d2f_wrdata     : fifo2_reg[31:24];
	fifo2_reg[23:16] <= mem2_wr[2] ? MemData[23:16] :
			    dvd2_wr[2] ? d2f_wrdata     : fifo2_reg[23:16];
	fifo2_reg[15: 8] <= mem2_wr[1] ? MemData[15: 8] :
			    dvd2_wr[1] ? d2f_wrdata     : fifo2_reg[15: 8];
	fifo2_reg[ 7: 0] <= mem2_wr[0] ? MemData[ 7: 0] :
			    dvd2_wr[0] ? d2f_wrdata     : fifo2_reg[ 7: 0];

  	fifo3_reg[63:56] <= mem3_wr[7] ? MemData[63:56] :
			    dvd3_wr[7] ? d2f_wrdata     : fifo3_reg[63:56];
	fifo3_reg[55:48] <= mem3_wr[6] ? MemData[55:48] :
			    dvd3_wr[6] ? d2f_wrdata     : fifo3_reg[55:48];
	fifo3_reg[47:40] <= mem3_wr[5] ? MemData[47:40] :
			    dvd3_wr[5] ? d2f_wrdata     : fifo3_reg[47:40];
	fifo3_reg[39:32] <= mem3_wr[4] ? MemData[39:32] :
			    dvd3_wr[4] ? d2f_wrdata     : fifo3_reg[39:32];
	fifo3_reg[31:24] <= mem3_wr[3] ? MemData[31:24] :
			    dvd3_wr[3] ? d2f_wrdata     : fifo3_reg[31:24];
	fifo3_reg[23:16] <= mem3_wr[2] ? MemData[23:16] :
			    dvd3_wr[2] ? d2f_wrdata     : fifo3_reg[23:16];
	fifo3_reg[15: 8] <= mem3_wr[1] ? MemData[15: 8] :
			    dvd3_wr[1] ? d2f_wrdata     : fifo3_reg[15: 8];
	fifo3_reg[ 7: 0] <= mem3_wr[0] ? MemData[ 7: 0] :
			    dvd3_wr[0] ? d2f_wrdata     : fifo3_reg[ 7: 0];

  	fifo4_reg[63:56] <= mem4_wr[7] ? MemData[63:56] :
			    dvd4_wr[7] ? d2f_wrdata     : fifo4_reg[63:56];
	fifo4_reg[55:48] <= mem4_wr[6] ? MemData[55:48] :
			    dvd4_wr[6] ? d2f_wrdata     : fifo4_reg[55:48];
	fifo4_reg[47:40] <= mem4_wr[5] ? MemData[47:40] :
			    dvd4_wr[5] ? d2f_wrdata     : fifo4_reg[47:40];
	fifo4_reg[39:32] <= mem4_wr[4] ? MemData[39:32] :
			    dvd4_wr[4] ? d2f_wrdata     : fifo4_reg[39:32];
	fifo4_reg[31:24] <= mem4_wr[3] ? MemData[31:24] :
			    dvd4_wr[3] ? d2f_wrdata     : fifo4_reg[31:24];
	fifo4_reg[23:16] <= mem4_wr[2] ? MemData[23:16] :
			    dvd4_wr[2] ? d2f_wrdata     : fifo4_reg[23:16];
	fifo4_reg[15: 8] <= mem4_wr[1] ? MemData[15: 8] :
			    dvd4_wr[1] ? d2f_wrdata     : fifo4_reg[15: 8];
	fifo4_reg[ 7: 0] <= mem4_wr[0] ? MemData[ 7: 0] :
			    dvd4_wr[0] ? d2f_wrdata     : fifo4_reg[ 7: 0];

  	fifo5_reg[63:56] <= mem5_wr[7] ? MemData[63:56] :
			    dvd5_wr[7] ? d2f_wrdata     : fifo5_reg[63:56];
	fifo5_reg[55:48] <= mem5_wr[6] ? MemData[55:48] :
			    dvd5_wr[6] ? d2f_wrdata     : fifo5_reg[55:48];
	fifo5_reg[47:40] <= mem5_wr[5] ? MemData[47:40] :
			    dvd5_wr[5] ? d2f_wrdata     : fifo5_reg[47:40];
	fifo5_reg[39:32] <= mem5_wr[4] ? MemData[39:32] :
			    dvd5_wr[4] ? d2f_wrdata     : fifo5_reg[39:32];
	fifo5_reg[31:24] <= mem5_wr[3] ? MemData[31:24] :
			    dvd5_wr[3] ? d2f_wrdata     : fifo5_reg[31:24];
	fifo5_reg[23:16] <= mem5_wr[2] ? MemData[23:16] :
			    dvd5_wr[2] ? d2f_wrdata     : fifo5_reg[23:16];
	fifo5_reg[15: 8] <= mem5_wr[1] ? MemData[15: 8] :
			    dvd5_wr[1] ? d2f_wrdata     : fifo5_reg[15: 8];
	fifo5_reg[ 7: 0] <= mem5_wr[0] ? MemData[ 7: 0] :
			    dvd5_wr[0] ? d2f_wrdata     : fifo5_reg[ 7: 0];


  	fifo6_reg[63:56] <= mem6_wr[7] ? MemData[63:56] :
			    dvd6_wr[7] ? d2f_wrdata     : fifo6_reg[63:56];
	fifo6_reg[55:48] <= mem6_wr[6] ? MemData[55:48] :
			    dvd6_wr[6] ? d2f_wrdata     : fifo6_reg[55:48];
	fifo6_reg[47:40] <= mem6_wr[5] ? MemData[47:40] :
			    dvd6_wr[5] ? d2f_wrdata     : fifo6_reg[47:40];
	fifo6_reg[39:32] <= mem6_wr[4] ? MemData[39:32] :
			    dvd6_wr[4] ? d2f_wrdata     : fifo6_reg[39:32];
	fifo6_reg[31:24] <= mem6_wr[3] ? MemData[31:24] :
			    dvd6_wr[3] ? d2f_wrdata     : fifo6_reg[31:24];
	fifo6_reg[23:16] <= mem6_wr[2] ? MemData[23:16] :
			    dvd6_wr[2] ? d2f_wrdata     : fifo6_reg[23:16];
	fifo6_reg[15: 8] <= mem6_wr[1] ? MemData[15: 8] :
			    dvd6_wr[1] ? d2f_wrdata     : fifo6_reg[15: 8];
	fifo6_reg[ 7: 0] <= mem6_wr[0] ? MemData[ 7: 0] :
			    dvd6_wr[0] ? d2f_wrdata     : fifo6_reg[ 7: 0];

  	fifo7_reg[63:56] <= mem7_wr[7] ? MemData[63:56] :
			    dvd7_wr[7] ? d2f_wrdata     : fifo7_reg[63:56];
	fifo7_reg[55:48] <= mem7_wr[6] ? MemData[55:48] :
			    dvd7_wr[6] ? d2f_wrdata     : fifo7_reg[55:48];
	fifo7_reg[47:40] <= mem7_wr[5] ? MemData[47:40] :
			    dvd7_wr[5] ? d2f_wrdata     : fifo7_reg[47:40];
	fifo7_reg[39:32] <= mem7_wr[4] ? MemData[39:32] :
			    dvd7_wr[4] ? d2f_wrdata     : fifo7_reg[39:32];
	fifo7_reg[31:24] <= mem7_wr[3] ? MemData[31:24] :
			    dvd7_wr[3] ? d2f_wrdata     : fifo7_reg[31:24];
	fifo7_reg[23:16] <= mem7_wr[2] ? MemData[23:16] :
			    dvd7_wr[2] ? d2f_wrdata     : fifo7_reg[23:16];
	fifo7_reg[15: 8] <= mem7_wr[1] ? MemData[15: 8] :
			    dvd7_wr[1] ? d2f_wrdata     : fifo7_reg[15: 8];
	fifo7_reg[ 7: 0] <= mem7_wr[0] ? MemData[ 7: 0] :
			    dvd7_wr[0] ? d2f_wrdata     : fifo7_reg[ 7: 0];


      end

endmodule // io_dififo













