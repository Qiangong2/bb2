/****************************************************************************
 * 
 *   (C) 1999 ARTX INC..  ALL RIGHTS RESERVED.  UNPUBLISHED -- RIGHTS
 *   RESERVED UNDER THE COPYRIGHT LAWS OF THE UNITED STATES.  USE OF A
 *   COPYRIGHT NOTICE IS PRECAUTIONARY ONLY AND DOES NOT IMPLY PUBLICATION
 *   OR DISCLOSURE.
 *
 *   THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF
 *   ARTX INC..  USE, DISCLOSURE, OR REPRODUCTION IS PROHIBITED WITHOUT
 *   THE PRIOR EXPRESS WRITTEN PERMISSION OF ARTX INC..
 *
 *                   RESTRICTED RIGHTS LEGEND
 *
 *   Use, duplication, or disclosure by the Government is subject to
 *   restrictions as set forth in subparagraph (c)(1)(ii) of the Rights
 *   in Technical Data and Computer Software clause at DFARS 252.227-7013
 *   or subparagraphs (c)(1) and (2) of Commercial Computer Software --
 *   Restricted Rights at 48 CFR 52.227-19, as applicable. 
 *
 *   ArtX Inc.
 *   3400 Hillview Ave, Bldg 5
 *   Palo Alto, CA 94304
 *
 ****************************************************************************/

`include "io_reg.v"

// syn myclk = io_clk

/*
 * Control Module for the Processor Interface in DI module in io subsystem
 */
module io_dipctl (/*AUTOARG*/
    // Output to PI or interrupt controller
    DiPiData, di_piInt,

    // Output to io_didctl
    p2d_rstb, p2d_break, p2d_immed, p2d_fromdvd,

    // Output to io_dimctl
    p2m_maradr, p2m_lgthadr, piwrite, p2m_dma, p2m_fromdvd,

    // Output to io_dibuf
    p2b_cmdadr, p2b_immadr,

    // General Input
    io_clk, resetb,

    // Input from PI or reset register
    PiData, PiAddr, PiLoad, PiDiSel, pi_ioDiRstb,

    // Input from io_didctl
    d2p_brkack, d2p_error, d2p_cover, d2p_tdone,
    di_config,

    // Input from io_dimctl
    m2p_rddata,

    // Input from io_dibuf
    b2p_rddata

);

    /*
     * Input, Output, Inout Ports declaration
     */
    // Output to PI or interrupt controller
    output[15:0]
	DiPiData;  		// Data bus to PI

    output
	di_piInt;   		// Interrupt to PI

    // Output to io_didctl
    output
    	p2d_rstb;		// Soft reset signal, from reset register

    output
    	p2d_break;		// Break signal (in Status register)

    output
	p2d_immed,		// Valid Immediate mode read/write
	p2d_fromdvd;		// 1 for read from dvd, 0 for write to dvd

    // Output to io_dimctl
    output [1:0]
	p2m_maradr,		// Address for MAR or Length registers
	p2m_lgthadr;		// 2 bits needed for 16 msbs or 16 lsbs

    output
	p2m_dma,		// Valid DMA mode read/write
	p2m_fromdvd;		// 1 for read from dvd, 0 for write to dvd

    output
	piwrite;		// Write for MAR,LENGTH or CR registers
				// Write for IMM/COMMAND buffers

    // Output to io_dibuf
    output [5:0]
	p2b_cmdadr;		// Read/Write address for command buffers

    output [1:0]
	p2b_immadr;		// Read/Write address for immed buffers

    // General inputs
    input
	io_clk;			// Clock

    input
	resetb;			// Reset, low active

    // Input from PI or reset register
    input [15:0]
	PiData;  		// Data bus from PI

    input [19:1]
	PiAddr;  		// Address from PI, [19:1]
				// Only bits 5:2 are needed for 9 reg decoding

    input
	PiLoad;			// Write from PI, 0 for read
				// Used for writing to EI, AI and SI also

    input
	PiDiSel;		// Selecting DI from PI(not EXI, AI or SI)

    input
   	pi_ioDiRstb;		// DI Reset from PI reset register, low active

    // Input from io_didctl
    input
	d2p_brkack;		// Break acknowledge

    input
	d2p_error,		// Error from external drive
	d2p_cover;		// Disc Cover status from external drive

    input
	d2p_tdone;		// Immediate mode or DMA mode transfer done
				// Delayed until the device strobe is low

    input [7:0]
	di_config;		// DI Configuration, set during reset phase

    // Input from io_dimctl
    input [15:0]
	m2p_rddata;		// Read data from MAR,LENGTH or CR registers

    // Input from io_dibuf
    input [15:0]
	b2p_rddata;		// Read data from command/immed buffers


   /*
    * Wires, Registers Delaration
    */
    reg [15:0]
	DiPiData;  		// Data bus to PI

    reg
	di_piInt;   		// Interrupt to PI

    reg
    	p2d_break;		// Break signal (in Status register)

    reg
	p2d_immed,		// Valid Immediate mode read/write
	p2d_fromdvd;		// 1 for read from dvd, 0 for write

    reg
	p2m_dma,		// Valid DMA mode read/write
	p2m_fromdvd;		// 1 for read from dvd, 0 for write

    reg
	p2d_rstb;		// Reset signal

    wire [15:0]
	regdata;		// Muxing all registers data for read

    wire [15:0]
	pi_rddata;		// Muxing DISR, DICVR registers data for read

    // Decoding address from PI
    wire
	piwrite,		// Writing PI register
	piread;			// Reading PI registers

    wire
	selSR,			// Selecting STATUS register
	selCVR,			// Selecting COVER register
	selCMD0,		// Selecting CMDBUF0 buffer
	selCMD1,		// Selecting CMDBUF1 buffer
	selCMD2,		// Selecting CMDBUF2 buffer
	selMAR,			// Selecting MAR register
	selLGTH,		// Selecting LENGTH register
	selCR,			// Selecting Control register
	selIMM;			// Selecting IMMEDIATE buffer

    wire
	selCFG;			// Selecting DI Config register

    wire
	selmsb,			// Selecting 16b msb for a 32b register
	sellsb;			// Selecting 16b lsb for a 32b register

    wire
	disr_wr,		// Write signal for DISR
	clr_srbrk,		// Clear DISR break interrupt bit
	clr_srtc,		// Clear DISR transfer complete interrupt bit
	clr_srde;		// Clear DISR Error interrupt bit

    wire
	dicvr_wr,		// Write signal for DICVR
	clr_cvr;		// Clear DICVR cvr interrupt bit

    wire
	dicr_wr;		// Write signal for DICR

    // DISR DI Status register
    reg [6:0]
	DISR_reg;		// DI Status register

    // DICVR DI Cover register
    reg [2:0]
	DICVR_reg;		// DI Cover register

    // DICR DI Control register
    reg [2:0]
	DICR_reg;		// DI Control register


   /*
    * Assignments
    */
    // Decoding from PI
    assign
	// Data from PI to DI
	piwrite = PiLoad & PiDiSel,

	// Data from DI to PI
	piread  = ~PiLoad & PiDiSel;

    // Read/Write address decoding
    assign
	// Selecting for 32b register
	selSR 	= (PiAddr[5:2] == `DI_SR_IDX),
	selCVR 	= (PiAddr[5:2] == `DI_CVR_IDX),
	selCMD0	= (PiAddr[5:2] == `DI_CMDBUF0_IDX),
	selCMD1	= (PiAddr[5:2] == `DI_CMDBUF1_IDX),
	selCMD2	= (PiAddr[5:2] == `DI_CMDBUF2_IDX),
	selMAR	= (PiAddr[5:2] == `DI_MAR_IDX),
	selLGTH	= (PiAddr[5:2] == `DI_LENGTH_IDX),
	selCR 	= (PiAddr[5:2] == `DI_CR_IDX),
	selIMM	= (PiAddr[5:2] == `DI_IMMBUF_IDX);

    assign
	selCFG  = (PiAddr[5:2] == `DI_CONFIG_IDX);

    assign
	// Selecting 16bits from the register, either upper or lower half
	selmsb  = ~PiAddr[1],
	sellsb  =  PiAddr[1];


    // For io_dimctl module
    assign
	p2m_maradr[1]  = selMAR & selmsb,
	p2m_maradr[0]  = selMAR & sellsb,
	p2m_lgthadr[1] = selLGTH & selmsb,
	p2m_lgthadr[0] = selLGTH & sellsb;

    // For io_dibuf module
    assign
	p2b_cmdadr[5]  = selCMD2 & selmsb,
	p2b_cmdadr[4]  = selCMD2 & sellsb,
	p2b_cmdadr[3]  = selCMD1 & selmsb,
	p2b_cmdadr[2]  = selCMD1 & sellsb,
	p2b_cmdadr[1]  = selCMD0 & selmsb,
	p2b_cmdadr[0]  = selCMD0 & sellsb,
	p2b_immadr[1]  = selIMM & selmsb,
	p2b_immadr[0]  = selIMM & sellsb;

    // For DISR DI status register
    assign
	// Write signal for DISR
	disr_wr   = piwrite & selSR & sellsb,

	// Clear DISR[BRKINT] bit by writing a one to this bit
        clr_srbrk = disr_wr & PiData[6],

	// Clear DISR[TCINT] bit by writing a one to this bit
	clr_srtc  = disr_wr & PiData[4],

	// Clear DISR[DEINT] bit by writing a one to this bit
	clr_srde  = disr_wr & PiData[2];

    // For DICVR DI cover register
    assign
	// Write signal for DICVR
	dicvr_wr = piwrite & selCVR & sellsb,

	// Clear DICVR[CVRINT] bit by writing a one to this bit
	clr_cvr  = dicvr_wr & PiData[2];

    // For DICR DI Control register
    assign
	dicr_wr = piwrite & selCR & sellsb;

    // For PI interface
    assign
	// pi_rddata for DISR, DICVR or others
	pi_rddata = (selSR & sellsb)  ?  {9'h0, DISR_reg}  :
		    (selCVR & sellsb) ? {13'h0, DICVR_reg} :
		    (selCR & sellsb)  ? {13'h0, DICR_reg}  : 
		    (selCFG & sellsb) ?  {8'h0, di_config} : 16'h0,
			
	// Selecting data from dibuf or dimctl or DISR or DICVR register
	regdata = ((|p2b_cmdadr) | (|p2b_immadr))  ? b2p_rddata :
		  ((|p2m_maradr) | (|p2m_lgthadr)) ? m2p_rddata : pi_rddata;


   /*
    * Always Block
    */
   // For PI interface
    always @(posedge io_clk)
      begin
	DiPiData <= piread ? regdata : DiPiData;
      end

    // For break logic and interrupt logic
    always @(posedge io_clk)
      if (~resetb)
	begin
	  p2d_break <= 1'b0;
	  p2d_rstb  <= 1'b0;
	  di_piInt  <= 1'b0;
	end
      else
	begin
	  p2d_break <= disr_wr & PiData[0];
	  p2d_rstb  <= pi_ioDiRstb;

	  di_piInt  <=  (DISR_reg[6] & DISR_reg[5]) |  // Break
			(DISR_reg[4] & DISR_reg[3]) |  // TC
		   	(DISR_reg[2] & DISR_reg[1]) |  // Error
			(DICVR_reg[2] & DICVR_reg[1]); // Cover

	 end

    // For Immediate Mode logic and for DMA Mode logic
    always @(posedge io_clk)
      if (~resetb)
	begin
	  p2d_immed   <= 1'b0;
    	  p2d_fromdvd <= 1'b0;
	  p2m_dma     <= 1'b0;
    	  p2m_fromdvd <= 1'b0;
	end
      else
	begin
	  // DICR[2] = 0 for read
	  // DICR[1] = 0 for Immed mode
	  // DICR[0] = 1 for start transfer
	  p2d_immed 	<= dicr_wr & PiData[0] & ~PiData[1];
	  p2d_fromdvd 	<= dicr_wr & ~PiData[2];
	  p2m_dma   	<= dicr_wr & PiData[0] & PiData[1];
	  p2m_fromdvd	<= dicr_wr & ~PiData[2];
	end

    // DISR Register
    always @(posedge io_clk)
      if (~resetb)
	begin
	  DISR_reg <= 7'h0;
	end
      else
	begin
	  // Break Interrupt
	  // Write DISR with 1 clear this bit, with 0 has no effect
	  // Set by break acknowledge from DVD drive
	  DISR_reg[6] <= DISR_reg[6] ? ~clr_srbrk : d2p_brkack;

	  // Break Interrupt mask
	  DISR_reg[5] <= disr_wr ? PiData[5] : DISR_reg[5];

	  // Transfer Complete Interrupt
	  // Write DISR with 1 clear this bit, with 0 has no effect
	  // Set by transfer complete in immed or DMA mode
	  DISR_reg[4] <= DISR_reg[4] ? ~clr_srtc : d2p_tdone;

	  // Transfer Complete Interrupt mask
	  DISR_reg[3] <= disr_wr ? PiData[3] : DISR_reg[3];

	  // Error Interupt
	  // Write DISR with 1 clear this bit, with 0 has no effect
	  // Set by error from DVD drive
	  DISR_reg[2] <= DISR_reg[2] ? ~clr_srde : (d2p_error);

	  // Error Interrupt mask
	  DISR_reg[1] <= disr_wr ? PiData[1] : DISR_reg[1];

	  // Break Status
	  // Set by writing a one to this bit
	  // Cleared by break acknowedge from DVD
	  DISR_reg[0] <= DISR_reg[0] ? ~d2p_brkack : (disr_wr & PiData[0]);

	end


    // DICVR DI Cover Register
    always @(posedge io_clk)
      if (~resetb)
	begin
	  DICVR_reg[2:1] <= 2'h0;
	end
      else
	begin
	  // Cover Interrupt
	  // Write DISR with 1 clear this bit, with 0 has no effect
	  // Set by status of DICOVER changes
	  // DICVR_reg[2] <= DICVR_reg[2] ? ~clr_cvr : (d2p_cover ^ DICVR_reg[0]);
	  // Changed to generate interrupt only when cover is closed
	  DICVR_reg[2] <= DICVR_reg[2] ? ~clr_cvr : (~d2p_cover & DICVR_reg[0]);

	  // Cover Interrupt mask
	  DICVR_reg[1] <= dicvr_wr ? PiData[1] : DICVR_reg[1];
	end

    always @(posedge io_clk)
      begin
	// Cover Status, Read only, state of DICOVER signal
	DICVR_reg[0] <= d2p_cover;
      end


    // DICR DI Control Register
    always @(posedge io_clk)
      if (~resetb)
 	begin
	  DICR_reg <= 3'h0;
	end
      else
	begin
	  // Transfer Read/Write 0 for read 1 for write
	  DICR_reg[2] <= dicr_wr ? PiData[2] : DICR_reg[2];

	  // DMA Mode, 1 for DMA mode, 0 for Immed mode
	  DICR_reg[1] <= dicr_wr ? PiData[1] : DICR_reg[1];

	  // TSTART
	  // Set by writing a one to this bit
	  // Cleared by transfer complete
	  // wwy Cleared by break acknowledge as well??
	  // Cleared by Error ??
	  DICR_reg[0] <= DICR_reg[0] ? ~(d2p_tdone | d2p_error | d2p_brkack)
				     : (dicr_wr & PiData[0]);
	end

endmodule // io_dipctl








