/*****************************************************************************
 * 
 *   (C) 1999 ARTX INC..  ALL RIGHTS RESERVED.  UNPUBLISHED -- RIGHTS
 *   RESERVED UNDER THE COPYRIGHT LAWS OF THE UNITED STATES.  USE OF A
 *   COPYRIGHT NOTICE IS PRECAUTIONARY ONLY AND DOES NOT IMPLY PUBLICATION
 *   OR DISCLOSURE.
 *
 *   THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF
 *   ARTX INC..  USE, DISCLOSURE, OR REPRODUCTION IS PROHIBITED WITHOUT
 *   THE PRIOR EXPRESS WRITTEN PERMISSION OF ARTX INC..
 *
 *                   RESTRICTED RIGHTS LEGEND
 *
 *   Use, duplication, or disclosure by the Government is subject to
 *   restrictions as set forth in subparagraph (c)(1)(ii) of the Rights
 *   in Technical Data and Computer Software clause at DFARS 252.227-7013
 *   or subparagraphs (c)(1) and (2) of Commercial Computer Software --
 *   Restricted Rights at 48 CFR 52.227-19, as applicable. 
 *
 *   ArtX Inc.
 *   3400 Hillview Ave, Bldg 5
 *   Palo Alto, CA 94304
 *
 ****************************************************************************/

`include "misc.gv"

// syn myclk = io_clk

/*
 * Control module for the external device interface in DI in io subsystem
 */
module io_didctl (/*AUTOARG*/
    // Output to External DVD drive
    didOut, didOE, didir, dihstrbb, dibrkOut, dibrkOE, dirstb,

    // Output for reset configuration
    di_config,

    // Output to io_dipctl
    d2p_brkack, d2p_error, d2p_cover, d2p_tdone,

    // Output to io_dimctl
    d2m_tdone, d2m_reset,

    // Output to io_dibuf
    d2b_wrdata, d2b_immrdadr, d2b_cmdadr, d2b_immwradr, d2b_immwr,

    // Output to io_dififo
    d2f_wrdata, d2f_wradr, d2f_rdadr, d2f_wr,

    // General Input
    io_clk, resetb,

    // Input from External DVD drive
    didIn, didstrbb, dierrb, dibrkIn, dicover,

    // Input from io_dipctl
    p2d_rstb, p2d_break, p2d_immed, p2d_fromdvd,

    // Input from io_dimctl
    m2d_dma, m2d_fromdvd, m2d_cmdonly, m2d_dmadone,
    m2d_last, m2d_bufvld,

    // Input from io_dibuf
    b2d_rddata,

    // Input from io_dififo
    f2d_rddata
);

    /*
     * Input, Output, Inout Ports declaration
     */
    // Output to external device
    output [7:0]
	didOut;			// Data output to DVD driver (inout DIDD)

    output
	didOE;			// Output Enable for DIDD inout bus

    output
	didir;			// Data direction 1 for input, 0 for output

    output
	dibrkOut,		// Break (inout DIBRK)
    	dibrkOE;		// Output Enable for DIBRK inout port

    output
    	dirstb;			// Reset, active low

    output
	dihstrbb;		// Host strobe, active low

    // Output for reset configuration
    output [7:0]
	di_config;		// Value get set during reset

    // Output to io_dipctl
    output
        d2p_brkack;             // Break acknowledge

    output
        d2p_error,              // Error from external drive
        d2p_cover;              // Disc Cover status from external drive

    output
        d2p_tdone;              // Immediate mode transfer done

    // Output to io_dimctl
    output
	d2m_tdone,		// Complete one 32Bytes DMA transfer
	d2m_reset;		// Break, Error or SW reset, used to reset
				// SM in dimctl

    // Output to io_dibuf
    output [7:0]
	d2b_wrdata;		// Data for immediate buffer

    output [3:0]
	d2b_immrdadr;		// Read address for immed buffer

    output [11:0]
	d2b_cmdadr;		// Read address for command buffers

    output [3:0]
	d2b_immwradr;		// Write address for immed buffer

    output
	d2b_immwr;		// Write signal for immed buffer

    // Output to io_dififo
    output [7:0]
	d2f_wrdata;		// Data to DMA fifo

    output [5:0]
	d2f_wradr,		// Write address for DMA fifo
	d2f_rdadr;		// Read address for DMA fifo

    output
	d2f_wr;			// Write signal for DMA fifo

    // General inputs
    input
	io_clk;			// Clock

    input
	resetb;			// Reset, low active

    // Input from external DVD driver
    input  [7:0]
	didIn;			// Data input from DVD (inout DIDD)

    input
	didstrbb;		// Device (DVD) strobe, active low

    input
	dierrb;			// Error, active low

    input
	dibrkIn;		// Break, active low (inout DIBRK)

    input
	dicover;		// Cover, high for cover open

    // Input from io_dipctl
    input
        p2d_rstb;               // Soft reset signal, from reset register

    input
        p2d_break;              // Break signal (in Status register)

    input
        p2d_immed,              // Valid Immediate mode read/write
        p2d_fromdvd;            // 1 for read from dvd, 0 for write

    // Input from io_dimctl
    input
	m2d_dma,		// Valid DMA mode read/write
	m2d_fromdvd;		// 1 for read from dvd, 0 for write

    input
	m2d_cmdonly;		// Command only request

    input
	m2d_dmadone;		// All DMA data has been completed

    input
	m2d_last;		// Last 32Bytes

    input [1:0]
	m2d_bufvld;		// Data in Buffer is valid

    // Input from io_dibuf
    input [7:0]
	b2d_rddata;		// Read data from cmd/immed buffers

    // Input from io_dififo
    input [7:0]
	f2d_rddata;		// DMA data from DMA fifo


   /*
    * Wires, Registers declaration
    */
    parameter [3:0]
	IDLE 	  = 4'b0000,	// IDLE state
	IMMTDVDC  = 4'b0001,	// Immed Mode, ASIC->DVD, Command packet
	IMMTDVDD  = 4'b0010,	// Immed Mode, ASIC->DVD, Data packet
	IMMDONE   = 4'b0011,	// Immed Mode, transfer done
	IMMFDVDC  = 4'b0101,	// Immed Mode, DVD->ASIC, Command packet
	IMMFDVDD  = 4'b0110,	// Immed Mode, DVD->ASIC, Data packet
	DMATDVDC  = 4'b1001,	// DMA Mode, ASIC->DVD, Command packet
	DMATDVDD  = 4'b1010,	// DMA Mode, ASIC->DVD, Data packet
	DMADONE   = 4'b1011,	// DMA Mode, transfer done
	DMAFDVDC  = 4'b1101,	// DMA Mode, DVD->ASIC, Command packet
	DMAFDVDD  = 4'b1110; 	// DMA Mode, DVD->ASIC, Data packet

    reg [3:0]
	cstate,			// Current State
	nstate;			// Next State

    reg
	cmdonly;		// Zero transfer length for DMA,
				// ie command only xfer

    reg
	prev_idle;		// Previous is idle cycle

    reg
	pend_donedmafdvd,	// Pending done_dmafdvd
	d1done_dmafdvd,		// One cycle delayed of d1done_dmafdvd
	d1done_dmatdvd;		// One cycle delayed of d1done_dmatdvd

    reg
	buf01_vld;		// Both buffer valid for dma from dvd to mem

    reg
	pend_done;		// Pending done signal, delayed until device
				// strobe is low

    wire
	smreset;		// Reseting SM due to break,error or sw reset

    wire
	imm_mode,		// Immediate mode
	cmd_mode,		// Command Mode
	immtdvd_mode,		// Sending IMM Data to DVD
	dmatdvd_mode,		// Sending DMA Data to DVD
	tdvd_mode,		// Sending Data to DVD
	immfdvd_mode,		// Getting IMM data from DVD
	dmafdvd_mode,		// Getting DMA data from DVD
	fdvd_mode;		// Getting from DVD

    wire
	done_cmdxfer,		// Done sending command to DVD
	done_immtdvd,		// Done sending data to DVD, imm mode
	done_immfdvd,		// Done getting data from DVD, imm mode
	done_dmatdvd,		// Done sending data to DVD, DMA mode
	done_dmafdvd;		// Done getting data from DVD, DMA mode

    wire
	done_alldmatdvd,	// Done all sending data to DVD, DMA mode
	done_alldmafdvd;	// Done all getting data from DVD, DMA mode

    parameter [2:0]		// Parameters for Break State Machine
	BRKIDLE	    = 3'b000,	// Idle state
	BRKWAITCMD  = 3'b001,	// Waiting for command transfer complete
	BRKWAITFDVD = 3'b010,   // Waiting for right cycle for data from dvd
	BRKWAITTDVD = 3'b011,   // Waiting for right cycle for data to dvd
	BRKCNTRL    = 3'b100, 	// Send control signals to dipctl,dimctl
	BRKRELEASE  = 3'b101,	// Release break control to DVD
	BRKDVDDRIVE = 3'b110,	// DVD driving break low
	BRKDVDACK   = 3'b111;	// DVD Acknowledge break completion

    reg [2:0]
	cbrkstate,		// Current state for break SM
	nbrkstate;		// Next state for break SM

    wire
	brkidle_mode,		// Break idle mode
	brkclr_mode,		// Break clear mode
	brkack_mode;		// Break acknowledge mode

    parameter [1:0]		// Parameters for Error/Reset State Machine
	ERRSTIDLE     = 2'b00,	// Idle state
	ERRSTWAITFDVD = 2'b01,	// Waiting for right cycle for data from dvd
	ERRSTWAITTDVD = 2'b10,	// Waiting for right cycle for data to dvd
				// or command transfer to DVD
	ERRSTCNTRL    = 2'b11; 	// Send control signal to reset state

    reg [1:0]
	cerrststate,		// Current state for error/reset SM
	nerrststate;		// Next state for error/reset SM

    wire
	errstidle_mode,		// Error/Reset idle mode
	errstclr_mode;		// Reset DI state machine


    // Reset logic
    reg
	d1p2drstb;		// One cycle delay of p2d_rstb

    wire
	p2drstbfall;		// Falling edge of p2d_rstb signal


    // Data sending to DVD logic
    reg [7:0]
	didOut;			// Data output to DVD driver (inout DIDD)

    reg [7:0]
	di_config;		// Get set during reset with didIn

    reg
	didOE_reg;		// Output Enable for DIDD inout bus

    reg
	didOE_mask;		// Masking didOE_reg when didir switches to low

    reg
	didir_reg;		// Dir for data flow, 1(device->asic)
				// Set to 0 during s/w reset

    reg
	didir_extend;		// Extended didir to obey the break protocol

    reg
	dihstrbb_reg;		// Host strobe, active low

    wire
	e1didir;		// One cycle early of didir

    reg [8:0]
	cnt9bit;		// 9 bit counters for data/cmd to DVD

    wire [8:0]
	e1cnt9bit;		// One cycle eary of cnt9bit

    reg [3:0]
	cnt4bit;		// 4 bit counters for stalling send to DVD

    wire
	done_alltdvd,		// Clearing cnt9bit to 0x1ff
	start_alltdvd;		// Starting cnt9bit to 0x000

    reg
	denbb,			// Device Enable, used to set d15enableb
	d15denbb,		// Mask for host strobe
	henbb;			// Host Enable

    wire
	e1hstrbb,		// One cycle early of dihstrbb
	e1henbb;		// One cycle early of henbb

    reg
	henbb_extend;		// Extended henbb to obey the break protocol

    // Error logic
    wire
	d2dierrb;

    reg
	/* Nov24,99
	 * Replaced with sync
	d1dierrb,		// One cycle delay of dierrb
	d2dierrb,		// Two cycles delay of dierrb
	 */
	d3dierrb;		// Three cycles delay of dierrb

    wire
	dierrfall;		// Falling edge of dierrb signal

    reg
	d2p_error;		// Error Detection send to PI

    // Break logic
    reg
	dibrkOE_reg;		// Break Output Enable

    wire
	d2dibrkIn;

    reg
	/* Nov24,99
	 * Replaced with sync
	d1dibrkIn,		// One cycle delay of dibrkIn
	d2dibrkIn,		// Two cycles delay of dibrkIn
	 */
	d3dibrkIn;		// Three cycles delay of dibrkIn

    wire
	brkInrise,		// Rising edge of dibrkIn signal
	brkInfall;		// Falling edge of dibrkIn signal

    // Disk cover logic
    /* Nov24,99
     * Replaced with sync
     reg
	d1dicover,		// One cycle delay of dicover
	d2dicover;		// Two cycles delay of dicover
     */
    wire
	d2dicover;

    // Device strobe logic
    wire
	d2didstrbb;

    reg
	/* Nov24,99
	 * Replaced with sync
	d1didstrbb,		// One cycle delay of didstrbb
	d2didstrbb,		// Two cycles delay of didstrbb
	 */
	d3didstrbb;		// Three cycles delay of didstrbb

    // Input data bus from external device
    /* Nov24,99
     * Replaced with sync
    reg [7:0]
	d1didIn,		// One cycle delay of didIn
	d2didIn;		// Two cycles delay of didIn
     */	
    wire [7:0]
	d2didIn;

    wire
	din_vld;		// Valid didIn data

    // For io_dibuf module
    reg [3:0]
	d2b_immwradr;		// Write address for immed buffer

    // For io_dififo module
    reg [4:0]
	dma_wradr;		// Write address for DMA fifo

    reg
	dma_adrmsb;		// MSB for FIFO address


   /*
    * Assignments
    */
    /*
     * For io_dipctl module
     */
    // Disk Cover Logic
    assign
	// State of disc cover, using two flops for synchronization
	d2p_cover = d2dicover;

    // Done Immediate mode transfer or DMA mode transfer, delayed by dstrbb
    assign
	d2p_tdone = pend_done & ~d2didstrbb;

    /*
     * For io_dimctl module
     */
    assign
	// d2m_tdone = (cstate == DMADONE),
	// Set at the end of each 32bytes transferred or at the end
	// of command only transfer
	d2m_tdone = d1done_dmatdvd | d1done_dmafdvd | (cstate == DMADONE),

	// When Break or ErrorReset State Machine not in idle mode
	d2m_reset = ~brkidle_mode | ~errstidle_mode;

    /*
     * Disk Error Logic
     */
    assign
	// negative edge detection
	dierrfall = ~d2dierrb & d3dierrb;

    assign
	// Error/Reset idle mode, used to reset sm in dimctl
	errstidle_mode = (cerrststate == ERRSTIDLE),

	errstclr_mode = &cerrststate;

    /*
     * Reset logic
     */
    assign
	// Falling edge of p2d_rstb
	p2drstbfall = ~p2d_rstb & d1p2drstb;

    assign
	dirstb = resetb ? d1p2drstb : 1'b0;

    /*
     * Break Logic
     */
    assign
	// Break idle Mode
	brkidle_mode = (cbrkstate == BRKIDLE),

	// Break clear mode
	brkclr_mode = (cbrkstate == BRKCNTRL),

	// Break Acknowledge mode.
	brkack_mode = (cbrkstate == BRKDVDACK);

    assign
	// If ASIC drives this signal, it is always high, external
	//  pullup resistor is required to set it to high 
	dibrkOut = 1'b0;

    assign
	// Rising edge of dibrkIn, used for break acknowledge detection
	brkInrise = d2dibrkIn & ~d3dibrkIn,

	// Falling edge of dibrkIn, detecting device as master of break signal
	brkInfall = ~d2dibrkIn & d3dibrkIn,

	// Break acknowledge signal, when break SM is at BRKDVDACK
	d2p_brkack = brkack_mode & (cnt4bit == 4'h0);

    /*
     * State Machine logic
     */
    assign
	// Reseting state machine for break, error or s/w reset detection
	// For break, reset it when cpu requests the break
	// For error, reset it when disk assert the error
	// For sw rst, reset it when cpu requests the soft reset
	smreset = brkclr_mode | errstclr_mode;

    assign
	// Immediate Mode
	imm_mode  = ~cstate[3] & (|cstate[2:0]),

	// Command Mode (always sending to DVD)
	cmd_mode  = (cstate[1:0] == 2'b01),

	// Immed data to DVD mode
	immtdvd_mode   = (cstate == IMMTDVDD),

	// DMA data to DVD mode
	dmatdvd_mode   = (cstate == DMATDVDD),

	// Sending data to DVD mode
	tdvd_mode = immtdvd_mode | dmatdvd_mode,

	// Immed data from DVD mode
	immfdvd_mode   = (cstate == IMMFDVDD),

	// DMA data from DVD mode
	dmafdvd_mode   = (cstate == DMAFDVDD),

	// From DVD Mode (data only)
	fdvd_mode = immfdvd_mode | dmafdvd_mode;

    assign
	// Done command packet transfer
    	// 12 bytes transfer for command packet
	done_cmdxfer = cmd_mode & (cnt9bit == 9'h5f),

	// Done Immed mode, data packet transfer
    	// 4 bytes transfer for Immediate mode data
	done_immtdvd = immtdvd_mode & (cnt9bit == 9'h1f),

	// Done DMA mode, data packet transfer
    	// 32 bytes transfer for DMA mode data
	done_dmatdvd  = dmatdvd_mode & (cnt9bit == 9'hff);

    assign
	// Done DMA mode, data packet transfer, last 32Bytes
	done_alldmatdvd = done_dmatdvd & m2d_last;

    /*
     * Control for sending data to external device
     */
    assign
	done_alltdvd =
	    // During command transfer, reset when counter reaches 0x5f
	    done_cmdxfer |

	    // During immed mode data transfer, reset when counter reaches 0x1f
	    done_immtdvd |

	    // During dma mode data transfer, reset when counter reaches 0xff
	    done_dmatdvd;

    assign
	// During command mode, immtdvd or dmatdvd mode, device enable
	// start_alltdvd = ~denbb & (cnt4bit == 4'h0) & (cmd_mode | tdvd_mode);
	start_alltdvd = ~denbb & ~d15denbb & (cnt4bit == 4'h0) &
			( cmd_mode | 
			 // Dec1, 99 (dmatdvd_mode & (|m2d_bufvld)) | 
			 // bufvld cleared one cycle too late
			 (dmatdvd_mode & (|m2d_bufvld) & ~d2m_tdone) | 
			  immtdvd_mode);

    assign
    	// Data/Command Counting Logic
	e1cnt9bit = {9{(done_alltdvd | smreset)}} | (
	    // Start counting from zero
	    cnt9bit[8] ? {9{~start_alltdvd}} :

	    // Every 16 counts, ie 2 periods, check the device enable
	    // If set (ie disabled), repeat 8 counts, ie 1 period
	    (cnt9bit[3:0] == 4'hf) & denbb ? {cnt9bit[8:3], 3'h0} :

	    // Increment counter
	    (cnt9bit + 9'h1));

    assign
	// Directional change, updating base on cnt4bit cycles
	/* 05/23/00 e1didir	= ~cnt4bit[3] ? fdvd_mode : didir,
 	 */
	e1didir	= ~cnt4bit[3] ? fdvd_mode | didir_extend : didir,

	// Host strobe when sending data to DVD
	e1hstrbb = cnt9bit[8] | cnt9bit[2],


	e1henbb =
	    // Set and Updated when cnt4bit==1 or 0 and getting data from DVD
	    // henbb ? ~((cnt4bit[3:1] == 3'h0) & fdvd_mode & (cnt4bit[0] | prev_idle)) :
	    (henbb ? ~(  (cnt4bit[3:1] == 3'h0) & fdvd_mode &
			// Dec1, 99 (cnt4bit[0] | prev_idle) &
			// Dec9,99 (cnt4bit[0] | prev_idle | ~(|m2d_bufvld) & buf01_vld) &
			// Dec13,99 (cnt4bit[0] | prev_idle | ~(|m2d_bufvld) & dmafdvd_mode & buf01_vld & ~(m2d_last & &d2f_wradr[4:2])) &
			(cnt4bit[0] | prev_idle | ~(|m2d_bufvld) & dmafdvd_mode & buf01_vld & ~(m2d_last & &d2f_wradr[4:2])) &
			(~pend_donedmafdvd & ~(&m2d_bufvld))) :
	
	    // Reset by end of response packet
	    // Imm mode: first data valid, coz device checks this every 2B
	    ((d2b_immwr & d2b_immwradr[0]) |

	    // DMA mode: 28th valid data
	    (d2f_wr & (d2f_wradr[4:0] == 5'h1c) & (m2d_last | (|m2d_bufvld)))) ) |

	    // Reset also when in Error clear mode
	    errstclr_mode | 

	    // Reset also when DVD is driving the break signal to low
	    (cbrkstate == BRKDVDDRIVE);

    assign
	// Address for command buffer (Read only)
	// bit0 to bit3 for cmd buffer0, bit0 for msb, big endian
	// bit4 to bit7 for cmd buffer1, bit4 for msb, big endian
	// bit8 to bitb for cmd buffer2, bit8 for msb, big endian
	d2b_cmdadr[0] = (cnt9bit[6:3] == 4'h0) & cmd_mode,
	d2b_cmdadr[1] = (cnt9bit[6:3] == 4'h1) & cmd_mode, 
	d2b_cmdadr[2] = (cnt9bit[6:3] == 4'h2) & cmd_mode, 
	d2b_cmdadr[3] = (cnt9bit[6:3] == 4'h3) & cmd_mode, 
	d2b_cmdadr[4] = (cnt9bit[6:3] == 4'h4) & cmd_mode, 
	d2b_cmdadr[5] = (cnt9bit[6:3] == 4'h5) & cmd_mode, 
	d2b_cmdadr[6] = (cnt9bit[6:3] == 4'h6) & cmd_mode, 
	d2b_cmdadr[7] = (cnt9bit[6:3] == 4'h7) & cmd_mode, 
	d2b_cmdadr[8] = (cnt9bit[6:3] == 4'h8) & cmd_mode, 
	d2b_cmdadr[9] = (cnt9bit[6:3] == 4'h9) & cmd_mode, 
	d2b_cmdadr[10] = (cnt9bit[6:3] == 4'ha) & cmd_mode, 
	d2b_cmdadr[11] = (cnt9bit[6:3] == 4'hb) & cmd_mode;

    assign
	// Address for immediate buffer, read
	// bit0 for reading the MSB for big endian
	d2b_immrdadr[0] = (cnt9bit[4:3] == 2'b00) & immtdvd_mode, 
	d2b_immrdadr[1] = (cnt9bit[4:3] == 2'b01) & immtdvd_mode, 
	d2b_immrdadr[2] = (cnt9bit[4:3] == 2'b10) & immtdvd_mode, 
	d2b_immrdadr[3] = (cnt9bit[4:3] == 2'b11) & immtdvd_mode;

    assign
	// Address for DMA FIFO, Read
	d2f_rdadr = {dma_adrmsb, cnt9bit[7:3]};

    assign
	d2f_wradr = {dma_adrmsb, dma_wradr};


    /*
     * Control for getting data from external device
     */
    assign
	// Posedge detection of didstrbb
	din_vld = d2didstrbb & ~d3didstrbb,

	d2b_immwr = din_vld & immfdvd_mode,
	d2f_wr    = din_vld & dmafdvd_mode;

    assign
	d2b_wrdata = d2didIn,
	d2f_wrdata = d2didIn;

    assign
	// Done getting data from DVD in immediate mode when
	// getting the last response data back
	done_immfdvd = d2b_immwr & d2b_immwradr[3],

	// Done getting data from DVD in DMA mode when
	// getting the last response data back (address = 5'h1f)
	done_dmafdvd = d2f_wr & (&d2f_wradr[4:0]);

    assign
	done_alldmafdvd = done_dmafdvd & m2d_last;

   /*
    * For all the output signals, during reset, it needs to
    * have a default value
    */
    assign
	didir = resetb ? didir_reg : 1'b0,

	didOE = resetb ? didOE_reg : 1'b0,

	// During reset, ASIC drives it, hence 0 during reset
	dibrkOE  = resetb ? dibrkOE_reg : 1'b1,

	dihstrbb = resetb ? dihstrbb_reg : 1'b1;


   /*
    * Always Block
    */
    /*
     * Pending Transfer Complete Logic
     */
    always @(posedge io_clk)
      if (~resetb)
	begin
	  pend_done <= 1'b0;
	end
      else
	begin
	  // During Immediate mode, Set by cstate reaching IMMDONE state
	  // During DMA mode, Set by transfer done signal from io_dimctl
	  // Reset by done or error being sent to io_dipctl
	  pend_done <= pend_done  ? ~(d2p_tdone | d2p_error)
				  : ((cstate == IMMDONE) | m2d_dmadone);
	end

    /*
     * State Machine for data transfer
     */
    always @(posedge io_clk)
      if (~resetb)
	begin
	  cstate <= 4'h0;
	  cmdonly <= 1'b0;
	  prev_idle <= 1'b0;
	  pend_donedmafdvd <= 1'b0;
	  d1done_dmafdvd <= 1'b0;
	  d1done_dmatdvd <= 1'b0;
	  buf01_vld      <= 1'b0;
	end
      else
	begin
	  cstate <= nstate & {4{~smreset}};
	  cmdonly <= (cmdonly ? ~(cstate == DMADONE) : m2d_cmdonly) & ~smreset;
	  prev_idle <= ~(|cstate);
	  pend_donedmafdvd <= pend_donedmafdvd  ? (&m2d_bufvld)
						: (done_dmafdvd & ~m2d_last);
	  d1done_dmafdvd <= done_dmafdvd;
	  d1done_dmatdvd <= done_dmatdvd;
	  // Dec10, 99 buf01_vld      <= fdvd_mode & (buf01_vld | &m2d_bufvld);
	  buf01_vld      <= fdvd_mode & (buf01_vld | e1henbb & ~henbb);
	end

    always @(cstate or p2d_immed or p2d_fromdvd or m2d_dma or m2d_fromdvd
	     or done_cmdxfer or done_immtdvd or done_immfdvd
	     or done_alldmatdvd or done_alldmafdvd or brkidle_mode
	     or m2d_cmdonly or cmdonly)
      begin
	case(cstate)
	IDLE:	  nstate = ~brkidle_mode ? IDLE :
			   p2d_immed ? (p2d_fromdvd ? IMMFDVDC : IMMTDVDC) :
			   m2d_dma   ? (m2d_fromdvd ? DMAFDVDC : DMATDVDC) :
			  // Adding command only mode
		     	   m2d_cmdonly  ?  DMATDVDC : IDLE;
	IMMTDVDC: nstate = done_cmdxfer  ? IMMTDVDD : IMMTDVDC;
	IMMTDVDD: nstate = done_immtdvd  ? IMMDONE  : IMMTDVDD;
	IMMDONE:  nstate = IDLE;
	IMMFDVDC: nstate = done_cmdxfer  ? IMMFDVDD : IMMFDVDC;
	IMMFDVDD: nstate = done_immfdvd  ? IMMDONE  : IMMFDVDD;
	DMATDVDC: nstate = done_cmdxfer  ? 
			   // Adding cmdonly for skipping data transfer
			   (cmdonly ? DMADONE : DMATDVDD) : DMATDVDC;
	DMATDVDD: nstate = done_alldmatdvd  ? DMADONE  : DMATDVDD;
	DMADONE:  nstate = IDLE;
	DMAFDVDC: nstate = done_cmdxfer  ? DMAFDVDD : DMAFDVDC;
	DMAFDVDD: nstate = done_alldmafdvd  ? DMADONE  : DMAFDVDD;
	default:  nstate = IDLE;
	endcase
      end


    /*
     * Logic for sending commands to DVD device
     */
    always @(posedge io_clk)
      if (~resetb)
      	begin
	  cnt9bit <= 9'h1ff;
	  cnt4bit <= 4'hf;
	end
      else
	begin
	  // Use for command/data transfer counting
  	  cnt9bit <= e1cnt9bit;

	  // Use for stall cycles counting during direction changes
	  // and starting for the new transfer
	  // Decrementing cnt4bit until zero
	  cnt4bit <= {4{(
			// After sending command or data packets to DVD
	      		done_cmdxfer | done_immtdvd | done_alldmatdvd |

	      		// After getting data from DVD
	      		done_immfdvd | done_alldmafdvd |

	      		// After break acknowledge detection (by DVD)
			(brkInrise & (cbrkstate == BRKDVDDRIVE)) |
	
		   	// After detection device enable to hstrbb
			(~denbb & d15denbb) |
	
	  	   	// After the drive error detection (by DVD)
			// After the CPU soft reset detection (by host)
			errstclr_mode

		      )}} | ((|cnt4bit) ? (cnt4bit - 4'h1) : cnt4bit);
	end


    /*
     * State Machine for break control logic
     */
    always @(posedge io_clk)
      if (~resetb)
	begin
	  cbrkstate <= 3'h0;
	end
      else
	begin
	  cbrkstate <= nbrkstate;
	end


    // verilint 488 off
    // 488 for not all bits used in the sensitivity list
    // wwy Sep17, 99 changed using d2didstrbb instead of din_vld
    always @(cbrkstate or p2d_break or cmd_mode or fdvd_mode or tdvd_mode
	     or d2didstrbb or brkInfall or brkInrise or e1cnt9bit or cnt4bit)
    // verilint 488 on
      begin
	case(cbrkstate)
	BRKIDLE:	nbrkstate = p2d_break 			? 
			    (cmd_mode  		 		? BRKWAITCMD :
			 (fdvd_mode & ~|cnt4bit & ~d2didstrbb) 	? BRKWAITFDVD :
			     tdvd_mode 		 		? BRKWAITTDVD :
							     	   BRKCNTRL)
					      : BRKIDLE;
	BRKWAITCMD:	nbrkstate = cmd_mode ? BRKWAITCMD : BRKCNTRL;
	BRKWAITFDVD:	nbrkstate = d2didstrbb ? BRKCNTRL   : BRKWAITFDVD;
	BRKWAITTDVD:	nbrkstate = (e1cnt9bit[3:0] == 4'hf)  ? BRKCNTRL :
								BRKWAITTDVD;
	BRKCNTRL:	nbrkstate = BRKRELEASE;
	BRKRELEASE:	nbrkstate = brkInfall ? BRKDVDDRIVE : BRKRELEASE;
	BRKDVDDRIVE:	nbrkstate = brkInrise ? BRKDVDACK   : BRKDVDDRIVE;
	BRKDVDACK:	nbrkstate = |(cnt4bit) ? BRKDVDACK  : BRKIDLE;
	endcase
      end


    /*
     * State Machine for error/reset control logic
     */
    always @(posedge io_clk)
      if (~resetb)
	begin
	  cerrststate <= 2'h0;
	end
      else
	begin
	  cerrststate <= nerrststate;
	end

    // verilint 488 off
    // 488 for not all bits used in the sensitivity list
    always @(cerrststate or p2drstbfall or cmd_mode or fdvd_mode or tdvd_mode
	     or e1cnt9bit or dierrfall or cnt4bit or d2didstrbb)
    // verilint 488 on
      begin
	case(cerrststate)
	ERRSTIDLE:	nerrststate = (p2drstbfall | dierrfall)    ?
			    ((cmd_mode | tdvd_mode)  		   ? ERRSTWAITTDVD :
			     (fdvd_mode & ~|cnt4bit & ~d2didstrbb) ? ERRSTWAITFDVD :
			      ERRSTCNTRL) 			   : ERRSTIDLE;
	// Sep17,99 wwy: Used d2didstrbb instead of din_vld
	ERRSTWAITFDVD:	nerrststate = d2didstrbb  ? ERRSTCNTRL   : ERRSTWAITFDVD;
	ERRSTWAITTDVD:	nerrststate = (e1cnt9bit[3:0] == 4'hf)  ? ERRSTCNTRL :
								ERRSTWAITTDVD;
	ERRSTCNTRL:	nerrststate = ERRSTIDLE;
	default:	nerrststate = ERRSTIDLE;
	endcase
      end


    /*
     * Output to external device
     */
    // Reset Logic, s/w reset (not just Power on Reset)
    // Active low, should reset didir and dihstrb signal
    always @(posedge io_clk)
      if (~resetb)
	begin
	  d1p2drstb <= 1'b0;
	end
      else
	begin
	  d1p2drstb <= p2d_rstb;
	end

    // Host strobe and Direction signal
    always @(posedge io_clk)
      if (~resetb)
	begin
	  didir_reg 	<= 1'b0;
	  didir_extend  <= 1'b0;
	  didOE_reg    	<= 1'b0;
	  didOE_mask	<= 1'b0;
	  dibrkOE_reg  	<= 1'b1;
	  denbb    	<= 1'b1;
	  d15denbb 	<= 1'b1;
	  dihstrbb_reg 	<= 1'b1;
	  henbb    	<= 1'b1;
	  henbb_extend 	<= 1'b0;
	end
      else
	begin
	  didir_reg   	<= e1didir;

	  // Extended didir = 1 during break protocol
	  didir_extend  <= ~brkidle_mode & ( didir_extend ? 
		~(cbrkstate == BRKDVDACK) : 
	        e1didir & ((cbrkstate == BRKWAITFDVD) | brkclr_mode)); 

	  // Output Enabled when not idle and sending data to DVD;
	  /* Nov2,99
 	   * didOE_reg     <= ~e1didir & ~cnt9bit[8];
	   * Changed such that it always drive when direction is from host to drive
	   */
	  didOE_mask    <= e1didir | (didOE_mask & |cnt4bit);  
	  didOE_reg     <= ~e1didir & ~didOE_mask;

	  // When break SM is idle, break OE should be set
	  // Reset by break protocol handling (BRKCNTRL)
	  dibrkOE_reg   <=  dibrkOE ? ~brkclr_mode : brkidle_mode;
	
	  // Update denbb with new value only when it used as enable strobe,
	  // ie sending command packets to DVD or sending data to DVD
	  // denbb    <= ((cnt9bit[3:0] == 4'hf) & (tdvd_mode | cmd_mode)) ? 
	  // Moved this to rising edge of the first 8 byte transfer instead
	  // of the beginning of the falling edge of the first 8 byte
	  // denbb    <= ((cnt9bit[8] | (cnt9bit[3:0] == 4'h3)) & 
	  //		(tdvd_mode | cmd_mode)) ? 
	  //	      d2didstrbb : denbb;
	  denbb       <= ((tdvd_mode | cmd_mode) & (denbb ? (cnt9bit[3:0] == 4'hf) :
							    (cnt9bit[3:0] == 4'h3))) ?
				d2didstrbb : denbb;

	  // 15 cycles delayed of denbb
	  d15denbb <= (cnt9bit[3:1] == 3'h7) ? denbb      : d15denbb;

	  // Host strobe when sending to DVD
	  // May26,00 dihstrbb_reg <= e1didir ? henbb : (e1hstrbb | d15denbb);
	  // Extended henbb during the break protocol
	  dihstrbb_reg <= e1didir ? henbb | henbb_extend : (e1hstrbb | d15denbb);

	  henbb <= e1henbb;

	  // Extended henbb = 1 during break protocol
	  henbb_extend  <= ~brkidle_mode & ( henbb_extend ? 
		~(cbrkstate == BRKDVDDRIVE) : e1henbb & ((cbrkstate == BRKWAITFDVD) | brkclr_mode)); 
	end

    always @(resetb or didIn)
      if (~resetb)
	begin
	  // Used for special test logic
	  di_config = didIn;
        end

    // Host driving the data bus
    always @(posedge io_clk)
      begin
  	// command packet or immediate mode, select data from io_dibuf
	didOut <= cmd_mode | imm_mode ? b2d_rddata : f2d_rddata;
      end


    /*
     * Inputs from external device
     */
    sync dierrb_sync (.D(dierrb), .CK(io_clk), .Q(d2dierrb));
    sync dibrkIn_sync (.D(dibrkIn), .CK(io_clk), .Q(d2dibrkIn));
    sync dicover_sync (.D(dicover), .CK(io_clk), .Q(d2dicover));
    sync didstrbb_sync (.D(didstrbb), .CK(io_clk), .Q(d2didstrbb));
    sync didIn0_sync (.D(didIn[0]), .CK(io_clk), .Q(d2didIn[0]));
    sync didIn1_sync (.D(didIn[1]), .CK(io_clk), .Q(d2didIn[1]));
    sync didIn2_sync (.D(didIn[2]), .CK(io_clk), .Q(d2didIn[2]));
    sync didIn3_sync (.D(didIn[3]), .CK(io_clk), .Q(d2didIn[3]));
    sync didIn4_sync (.D(didIn[4]), .CK(io_clk), .Q(d2didIn[4]));
    sync didIn5_sync (.D(didIn[5]), .CK(io_clk), .Q(d2didIn[5]));
    sync didIn6_sync (.D(didIn[6]), .CK(io_clk), .Q(d2didIn[6]));
    sync didIn7_sync (.D(didIn[7]), .CK(io_clk), .Q(d2didIn[7]));

    // Error logic, first two flops for two freq domain
    always @(posedge io_clk)
      if (~resetb)
	begin
	  /* Nov24,99
	  d1dierrb  <= 1'b1;
	  d2dierrb  <= 1'b1;
	  */
	  d3dierrb  <= 1'b1;
	  d2p_error <= 1'b0;
	end
      else
	begin
	  /* Nov24,99
	  d1dierrb  <= dierrb;
	  d2dierrb  <= d1dierrb;
	  */
	  // Use for edge detection logic, used with d2dierrb 
	  d3dierrb  <= d2dierrb;
	  d2p_error <= dierrfall;
	end

    // Break logic, two flops for two freq domain
    always @(posedge io_clk)
      if (~resetb)
	begin
	  /* Nov24,99
	  d1dibrkIn <= 1'b0;
	  d2dibrkIn <= 1'b0;
	   */
	  d3dibrkIn <= 1'b0;
        end
      else
	begin
	  /* Nov24,99
	  d1dibrkIn <= dibrkIn;
	  d2dibrkIn <= d1dibrkIn;
	   */
	  d3dibrkIn <= d2dibrkIn;
     	end

    // Disc Cover logic (1 for open), two flops for two freq domain
    /* Nov24,99
    always @(posedge io_clk)
      begin
	d1dicover <= dicover;
	d2dicover <= d1dicover;
      end
    */

    // Device Strobe logic, two flops for two freq domain
    always @(posedge io_clk)
      if (~resetb)
	begin
	  /* Nov24,99
	  d1didstrbb  <= 1'b1;
	  d2didstrbb  <= 1'b1;
	   */
	  d3didstrbb  <= 1'b1;
	end
      else
        begin
	  /* Nov24,99
	  d1didstrbb  <= didstrbb;
	  d2didstrbb  <= d1didstrbb;
	  */
	  d3didstrbb  <= d2didstrbb;
	end
	
    /* Nov24,99
    always @(posedge io_clk)
      begin
	d1didIn <= didIn;
	d2didIn <= d1didIn;
      end
    */

    // Logic for getting data from external device
    always @(posedge io_clk)
      if (~resetb)
	begin
	  d2b_immwradr <= 4'h1;
	  dma_wradr    <= 5'h0;
	  dma_adrmsb   <= 1'b0;
	end
      else
	begin
	  // bit 0 correspond to the msb for big endian mode
	  // Should be reset by sw reset, break protocol
	  d2b_immwradr <= smreset    ? 4'h1 :
			  d2b_immwr  ? {d2b_immwradr[2:0], d2b_immwradr[3]}
				     : d2b_immwradr;
	  // verilint 484 off
	  // Loss of carry/borrow
	  // Should be reset by sw reset, break protocol
	  dma_wradr    <= {5{~smreset}} & (d2f_wr ? (dma_wradr + 5'h1) : dma_wradr);
	  // verilint 484 on

	  dma_adrmsb <= ~smreset & ((done_dmafdvd | done_dmatdvd) ? 
				~dma_adrmsb : dma_adrmsb);
	  // d2f_rdadrmsb <= ~smreset & (done_dmatdvd ? ~d2f_rdadrmsb
	  //					   : d2f_rdadrmsb);
	end

	
endmodule // io_didctl












