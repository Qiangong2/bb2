/*****************************************************************************
 * 
 *   (C) 1999 ARTX INC..  ALL RIGHTS RESERVED.  UNPUBLISHED -- RIGHTS
 *   RESERVED UNDER THE COPYRIGHT LAWS OF THE UNITED STATES.  USE OF A
 *   COPYRIGHT NOTICE IS PRECAUTIONARY ONLY AND DOES NOT IMPLY PUBLICATION
 *   OR DISCLOSURE.
 *
 *   THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF
 *   ARTX INC..  USE, DISCLOSURE, OR REPRODUCTION IS PROHIBITED WITHOUT
 *   THE PRIOR EXPRESS WRITTEN PERMISSION OF ARTX INC..
 *
 *                   RESTRICTED RIGHTS LEGEND
 *
 *   Use, duplication, or disclosure by the Government is subject to
 *   restrictions as set forth in subparagraph (c)(1)(ii) of the Rights
 *   in Technical Data and Computer Software clause at DFARS 252.227-7013
 *   or subparagraphs (c)(1) and (2) of Commercial Computer Software --
 *   Restricted Rights at 48 CFR 52.227-19, as applicable. 
 *
 *   ArtX Inc.
 *   3400 Hillview Ave, Bldg 5
 *   Palo Alto, CA 94304
 *
 ****************************************************************************/

// syn myclk = io_clk

module io_di (/*AUTOARG*/
    // Output to PI and interrupt controller
    DiPiData, di_piInt,

    // Output to Mem
    DiMemData, DiMemAddr, DiMemReq, DiMemRd, DiMemFlush,

    // Output to External DVD drive
    didOut, didOE, didir, dihstrbb, dibrkOut, dibrkOE, dirstb,

    // Output for reset configuration
    di_config,

    // General Input
    io_clk, resetb,

    // Input from PI and reset register
    PiData, PiAddr, PiLoad, PiDiSel, pi_ioDiRstb,

    // Input from Mem
    MemData, DiMemAck, MemFlushAck,

    // Input from External DVD drive
    didIn, didstrbb, dierrb, dibrkIn, dicover
);

    /*
     * Input, Output, Inout Ports declaration
     */
    // Output to PI or interrupt controller
    output[15:0]
	DiPiData;  		// Data bus to PI

    output
	di_piInt;   		// Interrupt to PI

    // Output to Mem
    output [63:0]
	DiMemData;		// Data to Mem

    output [25:5]
	DiMemAddr;		// Address to Mem

    output
	DiMemReq;		// Request for 32B transfer

    output
	DiMemRd;		// Request for data read, 0 for write

    output
	DiMemFlush;		// Request write flush at the end
				// of the memory transfer

    // Output to external device
    output [7:0]
	didOut;			// Data output to DVD driver (inout DIDD)

    output
	didOE;			// Output Enable for DIDD inout bus

    output
	didir;			// Data direction 1 for input, 0 for output

    output
	dibrkOut,		// Break (inout DIBRK)
    	dibrkOE;		// Output Enable for DIBRK inout port

    output
    	dirstb;			// Reset, active low

    output
	dihstrbb;		// Host strobe, active low

    // Output for reset configuration
    output [7:0]
	di_config;		// Configuration during reset

    // General inputs
    input
	io_clk;			// Clock

    input
	resetb;			// Reset, low active

    // Input from PI or reset register
    input [15:0]
	PiData;  		// Data bus from PI

    input [19:1]
	PiAddr;  		// Address from PI, [19:1]
				// Only bits 5:2 are needed for 9 reg decoding

    input
	PiLoad;			// Write from PI, 0 for read
				// Used for writing to EI, AI and SI also

    input
	PiDiSel;		// Selecting DI from PI(not EXI, AI or SI)

    input
   	pi_ioDiRstb;		// DI Reset from PI reset register, low active

    // Input from Mem
    input [63:0]
	MemData;		// Data from Mem (used for DI and EX)

    input
	DiMemAck;		// Ack from Mem

    input
	MemFlushAck;		// Flush Ack from Mem (used for DI and EX)

    // Input from external DVD driver
    input  [7:0]
	didIn;			// Data input from DVD (inout DIDD)

    input
	didstrbb;		// Device (DVD) strobe, active low

    input
	dierrb;			// Error, active low

    input
	dibrkIn;		// Break, active low (inout DIBRK)

    input
	dicover;		// Cover, high for cover open

   /*
    * Wires, Registers declaration
    */
    // From io_dipctl
    wire [1:0]
	p2m_maradr,		// Address for MAR or Length registers
	p2m_lgthadr;		// 2 bits needed for 16 msbs or 16 lsbs

    wire [5:0]
	p2b_cmdadr;		// Read/Write address for command buffers

    wire [1:0]
	p2b_immadr;		// Read/Write address for immediate buffers

    // From io_dimctl
    wire [2:0]
	m2f_adr;		// DMA fifo address for write/read

    wire [15:0]
	m2p_rddata;		// Data for CPU from MAR/Length/CR registers

    wire [1:0]
	m2d_bufvld;		// Data in buffer is valid

    // From io_didctl
    wire [7:0]
	d2b_wrdata;		// Data for immediate buffer

    wire [3:0]
	d2b_immrdadr,		// Read address for immed buffer
	d2b_immwradr;		// Write address for immed buffer

    wire [11:0]
	d2b_cmdadr;		// Read address for command buffers

    wire [7:0]
	d2f_wrdata;		// Data to DMA fifo

    wire [5:0]
	d2f_wradr,		// Write address for DMA fifo
	d2f_rdadr;		// Read address for DMA fifo

    // From io_dibuf
    wire [15:0]
	b2p_rddata;		// Data for CPU from command/immed buffers

    wire [7:0]
	b2d_rddata;		// Read data from cmd/immed buffers

    // From io_dififo
    wire [63:0]
    	f2m_rddata;		// Data to DMA FIFO from memory

    wire [7:0]
	f2d_rddata;		// Data to device from DMA FIFO

    // Sync reset
    wire
	resetb_sync;

   /*
    * Instantiations
    */
    rst_sync rst_sync (
                .clk(io_clk),
                .rstinB(resetb),
                .rstoutB(resetb_sync)
                );

    io_dipctl io_dipctl(
	 	.DiPiData(DiPiData),
	 	.di_piInt(di_piInt),
	 	.piwrite(piwrite),
	 	.p2d_rstb(p2d_rstb),
	 	.p2d_break(p2d_break),
	 	.p2d_immed(p2d_immed),
	 	.p2d_fromdvd(p2d_fromdvd),
	 	.p2m_maradr(p2m_maradr),
	 	.p2m_lgthadr(p2m_lgthadr),
	 	.p2m_dma(p2m_dma),
	 	.p2m_fromdvd(p2m_fromdvd),
	 	.p2b_cmdadr(p2b_cmdadr),
	 	.p2b_immadr(p2b_immadr),
	 	.io_clk(io_clk),
	 	.resetb(resetb_sync),
	 	.PiData(PiData),
	 	.PiAddr(PiAddr),
	 	.PiLoad(PiLoad),
	 	.PiDiSel(PiDiSel),
	 	.pi_ioDiRstb(pi_ioDiRstb),
	 	.d2p_brkack(d2p_brkack),
	 	.d2p_error(d2p_error),
	 	.d2p_cover(d2p_cover),
	 	.d2p_tdone(d2p_tdone),
		.di_config(di_config),
	 	.m2p_rddata(m2p_rddata),
    		.b2p_rddata(b2p_rddata)
		);


    io_dimctl iodimctl(
	 	.DiMemData(DiMemData),
	 	.DiMemAddr(DiMemAddr),
	 	.DiMemReq(DiMemReq),
	 	.DiMemRd(DiMemRd),
	 	.DiMemFlush(DiMemFlush),
	 	.m2p_rddata(m2p_rddata),
		.m2d_dma(m2d_dma),
		.m2d_fromdvd(m2d_fromdvd),
		.m2d_cmdonly(m2d_cmdonly),
	 	.m2d_dmadone(m2d_dmadone),
		.m2d_last(m2d_last),
		.m2d_bufvld(m2d_bufvld),
	 	.m2f_adr(m2f_adr),
	 	.m2f_wr(m2f_wr),
	 	.io_clk(io_clk),
	 	.resetb(resetb_sync),
	 	.DiMemAck(DiMemAck),
	 	.MemFlushAck(MemFlushAck),
		.PiData(PiData),
		.piwrite(piwrite),
	 	.p2m_maradr(p2m_maradr),
	 	.p2m_lgthadr(p2m_lgthadr),
	 	.p2m_dma(p2m_dma),
	 	.p2m_fromdvd(p2m_fromdvd),
		.d2m_tdone(d2m_tdone),
		.d2m_reset(d2m_reset),
    		.f2m_rddata(f2m_rddata)
		);

    io_didctl iodidctl(
		.didOut(didOut),
		.didOE(didOE),
		.didir(didir),
		.dihstrbb(dihstrbb),
		.dibrkOut(dibrkOut),
		.dibrkOE(dibrkOE),	
		.dirstb(dirstb),
		.di_config(di_config),
		.d2p_brkack(d2p_brkack),
		.d2p_error(d2p_error),
		.d2p_cover(d2p_cover),
		.d2p_tdone(d2p_tdone),
	 	.d2m_tdone(d2m_tdone),
	 	.d2m_reset(d2m_reset),
	 	.d2b_wrdata(d2b_wrdata),
	 	.d2b_immwradr(d2b_immwradr),
		.d2b_immwr(d2b_immwr),
	 	.d2b_immrdadr(d2b_immrdadr),
	 	.d2b_cmdadr(d2b_cmdadr),
	 	.d2f_wrdata(d2f_wrdata),
	 	.d2f_wradr(d2f_wradr),
	 	.d2f_rdadr(d2f_rdadr),
	 	.d2f_wr(d2f_wr),
	 	.io_clk(io_clk),
	 	.resetb(resetb_sync),
	 	.didIn(didIn),
	 	.didstrbb(didstrbb),
	 	.dierrb(dierrb),
	 	.dibrkIn(dibrkIn),
	 	.dicover(dicover),
	 	.p2d_rstb(p2d_rstb),
	 	.p2d_break(p2d_break),
	 	.p2d_immed(p2d_immed),
	 	.p2d_fromdvd(p2d_fromdvd),
	 	.m2d_dma(m2d_dma),
	 	.m2d_fromdvd(m2d_fromdvd),
	 	.m2d_cmdonly(m2d_cmdonly),
	 	.m2d_dmadone(m2d_dmadone),
		.m2d_last(m2d_last),
		.m2d_bufvld(m2d_bufvld),
	 	.b2d_rddata(b2d_rddata),
    		.f2d_rddata(f2d_rddata)
		);

    io_dibuf io_dibuf(
	 	.b2p_rddata(b2p_rddata),
	 	.b2d_rddata(b2d_rddata),
	 	.io_clk(io_clk),
	 	.resetb(resetb_sync),
	 	.PiData(PiData),
	 	.piwrite(piwrite),
	 	.p2b_cmdadr(p2b_cmdadr),
	 	.p2b_immadr(p2b_immadr),
	 	.d2b_wrdata(d2b_wrdata),
	 	.d2b_immwradr(d2b_immwradr),
		.d2b_immwr(d2b_immwr),
	 	.d2b_immrdadr(d2b_immrdadr),
	 	.d2b_cmdadr(d2b_cmdadr)
		);

    io_dififo io_dififo (
	 	.f2m_rddata(f2m_rddata),
	 	.f2d_rddata(f2d_rddata),
	 	.io_clk(io_clk),
	 	.MemData(MemData),
	 	.m2f_adr(m2f_adr),
	 	.m2f_wr(m2f_wr),
	 	.d2f_wrdata(d2f_wrdata),
	 	.d2f_wradr(d2f_wradr),
	 	.d2f_rdadr(d2f_rdadr),
	 	.d2f_wr(d2f_wr)
		);


endmodule // io_di




