/*****************************************************************************
 * 
 *   (C) 1999 ARTX INC..  ALL RIGHTS RESERVED.  UNPUBLISHED -- RIGHTS
 *   RESERVED UNDER THE COPYRIGHT LAWS OF THE UNITED STATES.  USE OF A
 *   COPYRIGHT NOTICE IS PRECAUTIONARY ONLY AND DOES NOT IMPLY PUBLICATION
 *   OR DISCLOSURE.
 *
 *   THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF
 *   ARTX INC..  USE, DISCLOSURE, OR REPRODUCTION IS PROHIBITED WITHOUT
 *   THE PRIOR EXPRESS WRITTEN PERMISSION OF ARTX INC..
 *
 *                   RESTRICTED RIGHTS LEGEND
 *
 *   Use, duplication, or disclosure by the Government is subject to
 *   restrictions as set forth in subparagraph (c)(1)(ii) of the Rights
 *   in Technical Data and Computer Software clause at DFARS 252.227-7013
 *   or subparagraphs (c)(1) and (2) of Commercial Computer Software --
 *   Restricted Rights at 48 CFR 52.227-19, as applicable. 
 *
 *   ArtX Inc.
 *   3400 Hillview Ave, Bldg 5
 *   Palo Alto, CA 94304
 *
 ****************************************************************************/

// syn myclk = io_clk

/*
 * Control module for memory interface in DI module in io subsystem
 */
module io_dimctl (/*AUTOARG*/
    // Output to Mem
    DiMemData, DiMemAddr, DiMemReq, DiMemRd, DiMemFlush,

    // Output to io_dipctl
    m2p_rddata,

    // Output to io_didctl
    m2d_dma, m2d_fromdvd, m2d_cmdonly, m2d_dmadone,
    m2d_last, m2d_bufvld,

    // Output to io_dififo
    m2f_adr, m2f_wr,

    // Input from Mem
    DiMemAck, MemFlushAck,

    // Input from io_dipctl
    PiData, p2m_maradr, p2m_lgthadr, piwrite, p2m_dma, p2m_fromdvd,

    // Input from io_didctl
    d2m_tdone, d2m_reset,

    // Input from io_dififo
    f2m_rddata,

    // General Input
    io_clk, resetb

);

    /*
     * Input, Output, Inout Ports declaration
     */
    // Output to Mem
    output [63:0]
	DiMemData;		// Data to Mem

    output [25:5]
	DiMemAddr;		// Address to Mem

    output
	DiMemReq;		// Request for 32B transfer

    output
	DiMemRd;		// Request for data read, 0 for write

    output
	DiMemFlush;		// Request write flush at the end
				// of the memory transfer


    // Output to io_dipctl
    output [15:0]
	m2p_rddata;		// Read data from MAR, LENGTH or CR registers


    // Output to io_didctl
    output
	m2d_dma,		// DMA mode transfer starts, rd/wr
        m2d_fromdvd,		// 1 for read from dvd, 0 for write to dvd
	m2d_last;		// DMA mode transfer last data packet

    output [1:0]
	m2d_bufvld;		// Data in Buffer is valid

    output
	m2d_cmdonly;		// Command only transfer

    output
	m2d_dmadone;		// DMA transfer completed

    // Output to io_dififo
    output [2:0]
	m2f_adr;		// DMA fifo address for read/write

    output
	m2f_wr;			// Write signal to DMA fifo


    // General inputs
    input
	io_clk;			// Clock

    input
	resetb;			// Reset, low active

    // Input from Mem
    input
	DiMemAck;		// Ack from Mem

    input
	MemFlushAck;		// Flush Ack from Mem (used for DI and EX)


    // Input from io_dipctl
    input [15:0]
	PiData;			// Data for MAR, LENGTH or Control registers

    input [1:0]
	p2m_maradr,		// Address for MAR or Length registers
	p2m_lgthadr;		// 2 bits needed for 16 msbs or 16 lsbs

    input
	p2m_dma,		// Valid DMA Mode start
	p2m_fromdvd;		// 1 for Read from dvd, 0 for write to dvd

    input
	piwrite;		// Write for CR,MAR,LENGTH registers

    // Input from io_didctl
    input
	d2m_tdone;		// 32Bytes DMA transfered done

    input
	d2m_reset;		// Break,Error or SRst mode, should reset the SM

    // Input from io_dififo
    input [63:0]
    	f2m_rddata;		// Data from DMA FIFO to memory


   /*
    * Wires, Registers, Parameters declaration
    */
    parameter [3:0]
	IDLE 	  = 4'b0000,	// Idle State
	DVDRD	  = 4'b0001,	// Sending read request to DVD
	DVDRDWAIT = 4'b0010,	// Waiting for read data from DVD
	MEMWR0    = 4'b0100,	// Memory Write Data 0
	MEMWR1    = 4'b0101,	// Memory Write Data 1
	MEMWR2    = 4'b0110,	// Memory Write Data 2
	MEMWR3    = 4'b0111,	// Memory Write Data 3
	FACKWAIT  = 4'b0011,	// Waiting for Flush Acknowledge
	MEMRDADDR = 4'b1000,	// Updating memory address for read
	MEMRDWAIT = 4'b1001,	// Waiting for read data from Memory
	MEMRD0	  = 4'b1100,    // Memory Read Data 0
	MEMRD1	  = 4'b1101,    // Memory Read Data 1
	MEMRD2	  = 4'b1110,    // Memory Read Data 2
	MEMRD3	  = 4'b1111,    // Memory Read Data 3
	DVDWRWAIT = 4'b1010,	// Waiting for DVD to complete write
	DONE      = 4'b1011;	// Done DMA transfer

    parameter [1:0]
	ZIDLE	  = 2'b00,	// Idle state for zero length SM
	ZSTART    = 2'b01,	// Start for zero length SM
	ZWAIT	  = 2'b10,	// Wait for zero length SM
	ZDONE	  = 2'b11;	// Done for zero length SM

    reg [3:0]
	cstate,			// Current state
	nstate;			// Next state

    reg
	rdcyc,			// DVD->MEM
	wrcyc;			// MEM->DVD

    reg
	selwbuf,		// 0 for selecting buf0 during write
	selrbuf;		// 0 for selecting buf0 during read

    reg
	buf0_v,			// Data in buf0 is valid
	buf1_v;			// Data in buf1 is valid

    wire
	dvddata_tdone,		// Transfer done for 32bytes data
	memdata_tdone;		// Memory data transfer done


    wire
	dvdread,		// Start reading from DVD, set by DICR write
	dvdwrite;		// Start writing to DVD, set by DICR write

    wire
	reg_wr,			// Updating MAR,LGTH registers per DMA
	endofdma;		// All DMA transferred has been completed

    // DIMAR DI MAR register
    reg [25:5]
	DIMAR_reg;		// DI DMA Memory Address Register

    reg [25:5]
	DILGTH_reg;		// DI DMA Transfer Length Register

    reg
	lastdma;		// Last DMA request (Length == 1)

    wire
	memwrite;		// Data DVD->Memory, for updating lastdma

    wire [25:5]
	new_mardata,		// New DIMAR_reg data
	new_lgthdata;		// New DILGTH_reg data

    // For io_dififo logic
    wire
	dvddata_wr;		// Updating data register for data from DVD

    reg [63:0]
	DiMemData;		// Data to Mem

    // Special logic for zero length transfer
    reg [1:0]
	czstate,		// Current state for zero length xfer, ie cmd only
	nzstate;		// Next state for zero length xfer, ie cmd only

    // Special logic for error,break etc to reset SM
    reg
	pend_reset;		// Pending reset

    wire
	smreset;		// Reseting SM

    wire
	sm_idle;		// Both SM are idle

    reg
	pend_dma,		// Pending DMA Request
	pend_fromdvd;		// Pending DMA Request from DVD

    reg
	pend_dmadone;		// Pending DMA Done

   /*
    * Assignments
    */
    // Use to start the DMA transfer, as control signals for the SM
    assign
	dvdread  = ((p2m_dma & p2m_fromdvd) | (pend_dma & pend_fromdvd)) & (|DILGTH_reg),
	dvdwrite = ((p2m_dma & ~p2m_fromdvd) | (pend_dma & ~pend_fromdvd)) & (|DILGTH_reg);

    // For io_dipctl register read logic
    assign
	m2p_rddata = p2m_maradr[1]  ? {6'h0, DIMAR_reg[25:16]} :
		     p2m_maradr[0]  ? {DIMAR_reg[15:5], 5'h0} :
		     p2m_lgthadr[1] ? {6'h0, DILGTH_reg[25:16]} :
				      {DILGTH_reg[15:5], 5'h0};

    // For io_didctl module logic
    assign
	// Base on the state, send start signal to didctl module
	// Start at the beginning for DVD read
	// Start at the end for memory read
	m2d_dma = ((cstate == DVDRD) | (cstate == MEMRD3)) & ~smreset,

	// Set for reading from DVD
	m2d_fromdvd = (cstate == DVDRD);

    assign
	// For DVD->MEM, length decrement after data sent to memory and
	// m2d_last will be used after 28B being read from DVD.
	// - So if one buffer is valid, then no more 32B needed to be fetched
	//   when the length is 2 (1 for the valid buffer, 1 for current one)
	// - So if no buffer is valid, then no more 32B needed to be fetched
	//   when the length is 1. (1 for current one)
	// For MEM->DVD, length decrement after data fetched from memory
	// and m2d_last will be used after 28B being sent to DVD.
	// - So if one buffer is valid, then no more 32B needed to be fetched
	//   when the length is 0.
	m2d_last = (rdcyc & (buf0_v ^ buf1_v)) ? (DILGTH_reg[25:5] <= 21'h2) :
		    rdcyc                      ? (DILGTH_reg[25:5] <= 21'h1) :
		    ~(buf0_v & buf1_v) & ~(|DILGTH_reg[25:5]);

    assign
	// For data DVD->MEM:
	// When selwbuf = 0 (currently writes to buf0), use buf1_v
	// For data MEM->DVD:
	// When selrbuf = 0 (currently reads from buf0), used buf1_v
	m2d_bufvld = {buf1_v, buf0_v};

    // For io_didctl module logic for command only transfer
    assign
	m2d_cmdonly = (czstate == ZSTART) & ~smreset;

    assign
	dvddata_tdone = d2m_tdone & (czstate == ZIDLE),
	memdata_tdone = (cstate == MEMWR3) | (cstate == MEMRD3);

    // For MAR and LENGTH registers updates
    assign
	// To MEM: updates during 3rd Memory write cycle  (MEMWR2)
	// Even if break or error or reset occurs, data transfer is 
	// considered to have been completed
	// From MEM: updates after data being sent to DVD
	// If break or error or soft reset occurs, data from memory is
	// still in the dififo, data transfer has not been completed
	// and hence should not update the registers
	reg_wr = (cstate == MEMRDADDR) | (cstate == MEMWR2),

	// Should be used when cstate is at state MDMRD3 or MEMWR3 only
	// When Length == 0
	// Use at state MEMRDADDR, end of dma when length equals one
	endofdma = ~(|DILGTH_reg[25:6]) & ((cstate == MEMRDADDR) | ~DILGTH_reg[5]);

    assign
	// verilint 484 off
	// Lost of carry/borrow warnings
	new_mardata  = DIMAR_reg + 21'h1,
	new_lgthdata = DILGTH_reg - 21'h1;
    	// verilint 484 on

    // For io_dififo module logic
    assign
	// Updating Data for memory for sending data from DVD
	dvddata_wr = ~nstate[3] & nstate[2],

	// Selecting the four locations
	// During dvd read cycles, use selrbuf
	m2f_adr    = {(rdcyc ? selrbuf : selwbuf), nstate[1:0]},

	// Updating Data from memory for sending data to DVD
	m2f_wr     = nstate[3] & nstate[2];

    // For Memory interface
    assign
	// From DVD, first cycle of MEMWR, To DVD, waiting from memory
	DiMemReq = (cstate == MEMWR0) | (cstate == MEMRDWAIT),

	// Set when data is reading from memory to DVD
	DiMemRd = cstate[3],

	DiMemAddr = DIMAR_reg[25:5],

	DiMemFlush = DiMemReq & ~DiMemRd & lastdma;

    assign
	// Data from DVD to to memory
	memwrite = ~cstate[3] & (|cstate[2:0]);

    assign
	// Done when DMA complete or command only transfer complete
	m2d_dmadone = pend_dmadone & ~buf1_v & ~buf0_v | (czstate == ZDONE);

    // For SM reset logic
    assign
	smreset = d2m_reset | pend_reset;

    assign
	sm_idle = (cstate == IDLE) & (czstate == ZIDLE);

   /*
    * Always Block
    */

    // State Machine for controlling the memory interface and the
    // DVD drive DMA transfers
    always @(posedge io_clk)
      if (~resetb)
	begin
	  cstate  <= 4'h0;
	  rdcyc   <= 1'b0;
	  wrcyc   <= 1'b0;
	  selwbuf <= 1'b0;
	  selrbuf <= 1'b0;
	  buf0_v  <= 1'b0;
	  buf1_v  <= 1'b0;
	end
      else
	begin
	  cstate <= nstate;

	  // Set by dvdread, reset by cstate == DONE
	  rdcyc  <= ~smreset & ~(cstate == DONE) & (dvdread | rdcyc);

	  // Set by dvdwrite, reset by cstate == DONE
	  wrcyc  <= ~smreset & ~(pend_dmadone & ~buf0_v & ~buf1_v) 
		    & (dvdwrite | wrcyc);

	  // During dvd read, set by dvddata_tdone, reset by memdata_tdone
	  // During dvd write, set by memdata_tdone, reset by dvddata_tdone
	  buf0_v <= ~smreset & (buf0_v ?
		~(~selrbuf & ((dvddata_tdone & wrcyc) | (memdata_tdone & rdcyc))) :
		(~selwbuf & ((dvddata_tdone & rdcyc) | (memdata_tdone & wrcyc)))
				);

	  buf1_v <= ~smreset & (buf1_v ?
		~(selrbuf & ((dvddata_tdone & wrcyc) | (memdata_tdone & rdcyc))) :
		(selwbuf & ((dvddata_tdone & rdcyc) | (memdata_tdone & wrcyc)))
				);

	  // Toggle selwbuf after writes occur
	  selwbuf <= ~smreset & (
			((dvddata_tdone & rdcyc) | (memdata_tdone & wrcyc)) ? 
				~selwbuf : selwbuf); 

	  // Toggle selrbuf after reads occur
	  selrbuf <= ~smreset & (
			((dvddata_tdone & wrcyc) | (memdata_tdone & rdcyc)) ? 
				~selrbuf : selrbuf); 

	end

    always @(cstate or DiMemAck or dvdread or dvdwrite or buf0_v or buf1_v or
	     endofdma or MemFlushAck or smreset)
      begin
	case(cstate)
	  IDLE:		nstate = dvdread  ? DVDRD :
				 dvdwrite ? MEMRDWAIT : IDLE;
	  DVDRD:	nstate = DVDRDWAIT;
	  DVDRDWAIT:    nstate = smreset           ? IDLE :
				 (buf0_v | buf1_v) ? MEMWR0 : DVDRDWAIT;
	  MEMWR0: 	nstate = DiMemAck ? MEMWR1 : MEMWR0;
	  MEMWR1:	nstate = MEMWR2;
	  MEMWR2:	nstate = MEMWR3;
	  MEMWR3:	nstate = endofdma ? FACKWAIT : DVDRDWAIT;
	  FACKWAIT:	nstate = MemFlushAck ? DONE : FACKWAIT;
	  MEMRDWAIT:	nstate = DiMemAck ? MEMRD0 : MEMRDWAIT;
	  MEMRD0:	nstate = MEMRD1;
	  MEMRD1: 	nstate = MEMRD2;
	  MEMRD2:	nstate = MEMRD3;
	  MEMRD3:	nstate = DVDWRWAIT;
	  DVDWRWAIT:	nstate = smreset           ? IDLE :
				 (buf0_v & buf1_v) ? DVDWRWAIT : MEMRDADDR;
	  MEMRDADDR:	nstate = endofdma ? DONE : MEMRDWAIT;
	  DONE:		nstate = IDLE;
	  default: 	nstate = IDLE;
	endcase
      end

    // DI Registers
    always @(posedge io_clk)
      if (~resetb)
	begin
	  DIMAR_reg  <= 21'h0;
	  DILGTH_reg <= 21'h0;
	  lastdma    <= 1'b0;
	  // m2d_dmacmd <= 1'b0;
	end
      else
	begin
	  // DIMAR DI DMA Memory Address Register
	  DIMAR_reg[25:16] <= (p2m_maradr[1] & piwrite) ? PiData[9:0] :
			      reg_wr			? new_mardata[25:16]
							: DIMAR_reg[25:16];

	  DIMAR_reg[15: 5] <= (p2m_maradr[0] & piwrite) ? PiData[15:5] :
			      reg_wr			? new_mardata[15:5]
							: DIMAR_reg[15:5];

	  // DILENGTH DI DMA Transfer Length Register
	  DILGTH_reg[25:16] <= (p2m_lgthadr[1] & piwrite) ? PiData[9:0] :
			       reg_wr			  ? new_lgthdata[25:16]
							  : DILGTH_reg[25:16];

	  DILGTH_reg[15: 5] <= (p2m_lgthadr[0] & piwrite) ? PiData[15:5] :
			       reg_wr			  ? new_lgthdata[15:5]
							  : DILGTH_reg[15:5];

   	  // Use for generating the Flush signal
	  // Set When DILGTH == 1 when SM is for memory write (dvd -> mem)
	  // Reset after the flush has been acknowledge
	  // lastdma <= lastdma 	? ~MemFlushAck :
	  // Need to hold lastdma until DiMemReq deasserted
	  lastdma <= lastdma 	? (~MemFlushAck | DiMemReq) :
		     memwrite	? ~(|DILGTH_reg[25:6]) & DILGTH_reg[5] :
				  lastdma;

    	  // Used in didctl to send 12 bytes command followed by 32 bytes data
	  // during the first DMA transfer, any subsequence DMA transfer will
	  // not have the 12 bytes command packets
	  // Set by p2m_dma signal, reset by the first completion of DMA transfer
	  // ie d2m_tdone
	  // m2d_dmacmd <= m2d_dmacmd ? ~d2m_tdone : p2m_dma;
	end


    always @(posedge io_clk)
      begin
	DiMemData <= dvddata_wr ? f2m_rddata : DiMemData;
      end

    /*
     * Zero length Logic, ie Command Only
     */
    always @(posedge io_clk)
      if (~resetb)
	czstate <= ZIDLE;
      else
	czstate <= nzstate;

    always @(czstate or p2m_dma or smreset or DILGTH_reg or d2m_tdone)
      begin
	case(czstate)
	ZIDLE:	 nzstate = p2m_dma & ~(|DILGTH_reg) ? ZSTART : ZIDLE;
	ZSTART:  nzstate = ZWAIT;
	ZWAIT:   nzstate = smreset ? ZIDLE : d2m_tdone ? ZDONE : ZWAIT;
	ZDONE:	 nzstate = ZIDLE;
	default: nzstate = ZIDLE;
        endcase // czstate
      end

   /*
    * Error, Break, Reset Logic
    */
    always @(posedge io_clk)
      if (~resetb)
	begin
	  pend_reset   <= 1'b0;
	  pend_dma     <= 1'b0;
	  pend_fromdvd <= 1'b0;
	  pend_dmadone <= 1'b0;
	end
      else
	begin
	  pend_reset   <= pend_reset ? ~sm_idle : d2m_reset;
	  pend_dma     <= ~sm_idle  & (pend_dma | p2m_dma);
	  pend_fromdvd <= ~sm_idle  & (pend_fromdvd | p2m_fromdvd);
	  pend_dmadone <= pend_dmadone ? (buf0_v | buf1_v) : (cstate == DONE); 
	end				

endmodule // io_dimctl


