//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       ADFULD1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:53:29 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// 1-Bit Full Adder
// S  = ((A ^ B) ^ CI);CO = (A & B) | (CI & (A | B))
module ADFULD1 (CO, S, A, B, CI);
	output CO;
	output S;
	input  A;
	input  B;
	input  CI;
	xor _i0 (S,CI,A,B);
	and _i1 (_n2,A,B);
	or _i2 (_n4,A,B);
	and _i3 (_n3,CI,_n4);
	or _i4 (CO,_n2,_n3);
	specify		(A => CO) = (0,0);
	(A => S) = (0,0);
	(B => CO) = (0,0);
	(B => S) = (0,0);
	(CI => CO) = (0,0);
	(CI => S) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       ADFULD2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:53:25 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-Bit Full Adder
// S  = ((A ^ B) ^ CI);CO = (A & B) | (CI & (A | B))
module ADFULD2 (CO, S, A, B, CI);
	output CO;
	output S;
	input  A;
	input  B;
	input  CI;
	xor _i0 (S,CI,A,B);
	and _i1 (_n2,A,B);
	or _i2 (_n4,A,B);
	and _i3 (_n3,CI,_n4);
	or _i4 (CO,_n2,_n3);
	specify		(A => CO) = (0,0);
	(A => S) = (0,0);
	(B => CO) = (0,0);
	(B => S) = (0,0);
	(CI => CO) = (0,0);
	(CI => S) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       ADFULD4.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:53:25 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-Bit Full Adder
// S  = ((A ^ B) ^ CI);CO = (A & B) | (CI & (A | B))
module ADFULD4 (CO, S, A, B, CI);
	output CO;
	output S;
	input  A;
	input  B;
	input  CI;
	xor _i0 (S,CI,A,B);
	and _i1 (_n2,A,B);
	or _i2 (_n4,A,B);
	and _i3 (_n3,CI,_n4);
	or _i4 (CO,_n2,_n3);
	specify		(A => CO) = (0,0);
	(A => S) = (0,0);
	(B => CO) = (0,0);
	(B => S) = (0,0);
	(CI => CO) = (0,0);
	(CI => S) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       ADFULPD1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:53:23 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-Bit Full Adder with Propagate
// S  = ( ( A ^ B ) ^ CI  );CO = ( A & B ) | ( CI & (A | B) );P  = A ^ B
module ADFULPD1 (CO, P, S, A, B, CI);
	output CO;
	output P;
	output S;
	input  A;
	input  B;
	input  CI;
	xor _i0 (S,CI,A,B);
	and _i1 (_n2,A,B);
	or _i2 (_n4,A,B);
	and _i3 (_n3,CI,_n4);
	or _i4 (CO,_n2,_n3);
	xor _i5 (P,A,B);
	specify		(A => CO) = (0,0);
	(A => P) = (0,0);
	(A => S) = (0,0);
	(B => CO) = (0,0);
	(B => P) = (0,0);
	(B => S) = (0,0);
	(CI => CO) = (0,0);
	(CI => S) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       ADHALFD1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:53:17 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-Bit Half Adder
// S  = ( A ^ B );CO = ( A & B )
module ADHALFD1 (CO, S, A, B);
	output CO;
	output S;
	input  A;
	input  B;
	xor _i0 (S,A,B);
	and _i1 (CO,A,B);
	specify		(A => CO) = (0,0);
	(A => S) = (0,0);
	(B => CO) = (0,0);
	(B => S) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AND2D0.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:53:20 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-Input AND Gate
// Z = ( A1 & A2  )
module AND2D0 (Z, A1, A2);
	output Z;
	input  A1;
	input  A2;
	and _i0 (Z,A1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AND2D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:53:23 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-Input AND Gate
// Z = ( A1 & A2  )
module AND2D1 (Z, A1, A2);
	output Z;
	input  A1;
	input  A2;
	and _i0 (Z,A1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AND2D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:53:17 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-Input AND Gate
// Z = ( A1 & A2  )
module AND2D2 (Z, A1, A2);
	output Z;
	input  A1;
	input  A2;
	and _i0 (Z,A1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AND2D4.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:53:13 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-Input AND Gate
// Z = ( A1 & A2  )
module AND2D4 (Z, A1, A2);
	output Z;
	input  A1;
	input  A2;
	and _i0 (Z,A1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AND2M1D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:53:08 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-Input AND Gate with Input A1 Inverted
// Z = ( ! A1 & A2  )
module AND2M1D1 (Z, A1, A2);
	output Z;
	input  A1;
	input  A2;
	not _i0 (_n1,A1);
	and _i1 (Z,_n1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AND2M1D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:53:09 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-Input AND Gate with Input A1 Inverted
// Z = ( ! A1 & A2  )
module AND2M1D2 (Z, A1, A2);
	output Z;
	input  A1;
	input  A2;
	not _i0 (_n1,A1);
	and _i1 (Z,_n1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AND3D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:53:15 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 3-Input AND Gate
// Z = ( A1 & A2 & A3  )
module AND3D1 (Z, A1, A2, A3);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	and _i0 (Z,A3,A1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AND3D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:53:12 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 3-Input AND Gate
// Z = ( A1 & A2 & A3  )
module AND3D2 (Z, A1, A2, A3);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	and _i0 (Z,A3,A1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AND3D4.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:53:08 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 3-Input AND Gate
// Z = ( A1 & A2 & A3  )
module AND3D4 (Z, A1, A2, A3);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	and _i0 (Z,A3,A1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AND3M1D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:53:06 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 3-Input AND Gate with Input A1 Inverted
// Z = ( ! A1 & A2 & A3  )
module AND3M1D1 (Z, A1, A2, A3);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	not _i0 (_n1,A1);
	and _i1 (Z,A3,_n1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AND3M1D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:53:04 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 3-Input AND Gate with Input A1 Inverted
// Z = ( ! A1 & A2 & A3  )
module AND3M1D2 (Z, A1, A2, A3);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	not _i0 (_n1,A1);
	and _i1 (Z,A3,_n1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AND3M2D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:53:00 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 3-Input AND Gate with Inputs A1 and A2 Inverted
// Z = ( ! A1 & ! A2 & A3  )
module AND3M2D1 (Z, A1, A2, A3);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	not _i0 (_n1,A1);
	not _i1 (_n2,A2);
	and _i2 (Z,A3,_n1,_n2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AND3M2D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:53:07 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 3-Input AND Gate with Inputs A1 and A2 Inverted
// Z = ( ! A1 & ! A2 & A3  )
module AND3M2D2 (Z, A1, A2, A3);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	not _i0 (_n1,A1);
	not _i1 (_n2,A2);
	and _i2 (Z,A3,_n1,_n2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AND4D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:53:02 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 4-Input AND Gate
// Z = ( A1 & A2 & A3 & A4  )
module AND4D1 (Z, A1, A2, A3, A4);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  A4;
	and _i0 (Z,A4,A3,A1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(A4 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AND4D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:53:03 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 4-Input AND Gate
// Z = ( A1 & A2 & A3 & A4  )
module AND4D2 (Z, A1, A2, A3, A4);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  A4;
	and _i0 (Z,A4,A3,A1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(A4 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AND4D4.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:53:01 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 4-Input AND Gate
// Z = ( A1 & A2 & A3 & A4  )
module AND4D4 (Z, A1, A2, A3, A4);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  A4;
	and _i0 (Z,A4,A3,A1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(A4 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AND4M1D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:55 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 4-Input AND Gate with Input A1 Inverted
// Z = ( ! A1 & A2 & A3 & A4  )
module AND4M1D1 (Z, A1, A2, A3, A4);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  A4;
	not _i0 (_n1,A1);
	and _i1 (Z,A4,A3,_n1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(A4 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AND4M1D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:53:03 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 4-Input AND Gate with Input A1 Inverted
// Z = ( ! A1 & A2 & A3 & A4  )
module AND4M1D2 (Z, A1, A2, A3, A4);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  A4;
	not _i0 (_n1,A1);
	and _i1 (Z,A4,A3,_n1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(A4 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AND4M2D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:56 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 4-Input AND Gate with Inputs A1 and A2 Inverted
// Z = ( ! A1 & ! A2 & A3 & A4  )
module AND4M2D1 (Z, A1, A2, A3, A4);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  A4;
	not _i0 (_n1,A1);
	not _i1 (_n2,A2);
	and _i2 (Z,A4,A3,_n1,_n2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(A4 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AND4M2D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:53:00 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 4-Input AND Gate with Inputs A1 and A2 Inverted
// Z = ( ! A1 & ! A2 & A3 & A4  )
module AND4M2D2 (Z, A1, A2, A3, A4);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  A4;
	not _i0 (_n1,A1);
	not _i1 (_n2,A2);
	and _i2 (Z,A4,A3,_n1,_n2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(A4 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AND4M3D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:57 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 4-Input AND Gate with Inputs A1, A2 and A3 Inverted
// Z = ( ! A1 & ! A2 & ! A3 & A4  )
module AND4M3D1 (Z, A1, A2, A3, A4);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  A4;
	not _i0 (_n1,A3);
	not _i1 (_n2,A1);
	not _i2 (_n3,A2);
	and _i3 (Z,A4,_n1,_n2,_n3);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(A4 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AND4M3D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:53:03 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 4-Input AND Gate with Inputs A1, A2 and A3 Inverted
// Z = ( ! A1 & ! A2 & ! A3 & A4  )
module AND4M3D2 (Z, A1, A2, A3, A4);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  A4;
	not _i0 (_n1,A3);
	not _i1 (_n2,A1);
	not _i2 (_n3,A2);
	and _i3 (Z,A4,_n1,_n2,_n3);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(A4 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AO211D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:55 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-1-2 AND-OR Gate
// Z = ( A1 & A2  ) | B | C
module AO211D1 (Z, A1, A2, B, C);
	output Z;
	input  A1;
	input  A2;
	input  B;
	input  C;
	and _i0 (_n1,A1,A2);
	or _i1 (Z,C,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B => Z) = (0,0);
	(C => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AO211D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:50 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-1-2 AND-OR Gate
// Z = ( A1 & A2  ) | B | C
module AO211D2 (Z, A1, A2, B, C);
	output Z;
	input  A1;
	input  A2;
	input  B;
	input  C;
	and _i0 (_n1,A1,A2);
	or _i1 (Z,C,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B => Z) = (0,0);
	(C => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AO21D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:54 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-2 AND-OR Gate
// Z = ( A1 & A2  ) | B
module AO21D1 (Z, A1, A2, B);
	output Z;
	input  A1;
	input  A2;
	input  B;
	and _i0 (_n1,A1,A2);
	or _i1 (Z,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AO21D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:49 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-2 AND-OR Gate
// Z = ( A1 & A2  ) | B
module AO21D2 (Z, A1, A2, B);
	output Z;
	input  A1;
	input  A2;
	input  B;
	and _i0 (_n1,A1,A2);
	or _i1 (Z,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AO21M10D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:49 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-2 AND-OR Gate with Input A1 Inverted
// Z = ( ! A1 & A2  ) | B
module AO21M10D1 (Z, A1, A2, B);
	output Z;
	input  A1;
	input  A2;
	input  B;
	not _i0 (_n2,A1);
	and _i1 (_n1,_n2,A2);
	or _i2 (Z,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AO21M10D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:49 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-2 AND-OR Gate with Input A1 Inverted
// Z = ( ! A1 & A2  ) | B
module AO21M10D2 (Z, A1, A2, B);
	output Z;
	input  A1;
	input  A2;
	input  B;
	not _i0 (_n2,A1);
	and _i1 (_n1,_n2,A2);
	or _i2 (Z,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AO21M20D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:48 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-2 AND-OR Gate with Inputs A1 and A2 Inverted
// Z = ( ! A1 & ! A2  ) | B
module AO21M20D1 (Z, A1, A2, B);
	output Z;
	input  A1;
	input  A2;
	input  B;
	not _i0 (_n2,A1);
	not _i1 (_n3,A2);
	and _i2 (_n1,_n2,_n3);
	or _i3 (Z,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AO21M20D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:52 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-2 AND-OR Gate with Inputs A1 and A2 Inverted
// Z = ( ! A1 & ! A2  ) | B
module AO21M20D2 (Z, A1, A2, B);
	output Z;
	input  A1;
	input  A2;
	input  B;
	not _i0 (_n2,A1);
	not _i1 (_n3,A2);
	and _i2 (_n1,_n2,_n3);
	or _i3 (Z,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AO221D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:43 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-2-2 AND-OR Gate
// Z = ( A1 & A2  ) | ( B1 & B2 ) | C
module AO221D1 (Z, A1, A2, B1, B2, C);
	output Z;
	input  A1;
	input  A2;
	input  B1;
	input  B2;
	input  C;
	and _i0 (_n1,A1,A2);
	and _i1 (_n2,B1,B2);
	or _i2 (Z,C,_n1,_n2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(C => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AO221D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:48 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-2-2 AND-OR Gate
// Z = ( A1 & A2  ) | ( B1 & B2 ) | C
module AO221D2 (Z, A1, A2, B1, B2, C);
	output Z;
	input  A1;
	input  A2;
	input  B1;
	input  B2;
	input  C;
	and _i0 (_n1,A1,A2);
	and _i1 (_n2,B1,B2);
	or _i2 (Z,C,_n1,_n2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(C => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AO222D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:47 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-2-2 AND-OR Gate
// Z = ( A1 & A2  ) | ( B1 & B2 ) | ( C1 & C2 )
module AO222D1 (Z, A1, A2, B1, B2, C1, C2);
	output Z;
	input  A1;
	input  A2;
	input  B1;
	input  B2;
	input  C1;
	input  C2;
	and _i0 (_n1,A1,A2);
	and _i1 (_n2,B1,B2);
	and _i2 (_n3,C1,C2);
	or _i3 (Z,_n1,_n2,_n3);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(C1 => Z) = (0,0);
	(C2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AO222D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:43 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-2-2 AND-OR Gate
// Z = ( A1 & A2  ) | ( B1 & B2 ) | ( C1 & C2 )
module AO222D2 (Z, A1, A2, B1, B2, C1, C2);
	output Z;
	input  A1;
	input  A2;
	input  B1;
	input  B2;
	input  C1;
	input  C2;
	and _i0 (_n1,A1,A2);
	and _i1 (_n2,B1,B2);
	and _i2 (_n3,C1,C2);
	or _i3 (Z,_n1,_n2,_n3);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(C1 => Z) = (0,0);
	(C2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AO311D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:48 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-1-3 AND-OR Gate
// Z = ( A1 & A2 & A3  ) | B | C
module AO311D1 (Z, A1, A2, A3, B, C);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B;
	input  C;
	and _i0 (_n1,A3,A1,A2);
	or _i1 (Z,C,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B => Z) = (0,0);
	(C => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AO311D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:41 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-1-3 AND-OR Gate
// Z = ( A1 & A2 & A3  ) | B | C
module AO311D2 (Z, A1, A2, A3, B, C);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B;
	input  C;
	and _i0 (_n1,A3,A1,A2);
	or _i1 (Z,C,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B => Z) = (0,0);
	(C => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AO31D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:43 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-3 AND-OR Gate
// Z = ( A1 & A2 & A3  ) | B
module AO31D1 (Z, A1, A2, A3, B);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B;
	and _i0 (_n1,A3,A1,A2);
	or _i1 (Z,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AO31D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:43 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-3 AND-OR Gate
// Z = ( A1 & A2 & A3  ) | B
module AO31D2 (Z, A1, A2, A3, B);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B;
	and _i0 (_n1,A3,A1,A2);
	or _i1 (Z,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AO31M10D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:39 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-3 AND-OR Gate with Input A1 Inverted
// Z = ( ! A1 & A2 & A3  ) | B
module AO31M10D1 (Z, A1, A2, A3, B);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B;
	not _i0 (_n2,A1);
	and _i1 (_n1,A3,_n2,A2);
	or _i2 (Z,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AO31M10D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:36 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-3 AND-OR Gate with Input A1 Inverted
// Z = ( ! A1 & A2 & A3  ) | B
module AO31M10D2 (Z, A1, A2, A3, B);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B;
	not _i0 (_n2,A1);
	and _i1 (_n1,A3,_n2,A2);
	or _i2 (Z,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AO31M20D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:36 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-3 AND-OR Gate with Inputs A1 and A2 Inverted
// Z = ( ! A1 & ! A2 & A3  ) | B
module AO31M20D1 (Z, A1, A2, A3, B);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B;
	not _i0 (_n2,A1);
	not _i1 (_n3,A2);
	and _i2 (_n1,A3,_n2,_n3);
	or _i3 (Z,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AO31M20D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:36 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-3 AND-OR Gate with Inputs A1 and A2 Inverted
// Z = ( ! A1 & ! A2 & A3  ) | B
module AO31M20D2 (Z, A1, A2, A3, B);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B;
	not _i0 (_n2,A1);
	not _i1 (_n3,A2);
	and _i2 (_n1,A3,_n2,_n3);
	or _i3 (Z,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AO31M30D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:34 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-3 AND-OR Gate with Inputs A1, A2 and A3 Inverted
// Z = ( ! A1 & ! A2 & ! A3  ) | B
module AO31M30D1 (Z, A1, A2, A3, B);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B;
	not _i0 (_n2,A3);
	not _i1 (_n3,A1);
	not _i2 (_n4,A2);
	and _i3 (_n1,_n2,_n3,_n4);
	or _i4 (Z,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AO31M30D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:30 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-3 AND-OR Gate with Inputs A1, A2 and A3 Inverted
// Z = ( ! A1 & ! A2 & ! A3  ) | B
module AO31M30D2 (Z, A1, A2, A3, B);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B;
	not _i0 (_n2,A3);
	not _i1 (_n3,A1);
	not _i2 (_n4,A2);
	and _i3 (_n1,_n2,_n3,_n4);
	or _i4 (Z,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AO321D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:36 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-2-3 AND-OR Gate
// Z = ( A1 & A2 & A3  ) | ( B1 & B2 ) | C
module AO321D1 (Z, A1, A2, A3, B1, B2, C);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B1;
	input  B2;
	input  C;
	and _i0 (_n1,A3,A1,A2);
	and _i1 (_n2,B1,B2);
	or _i2 (Z,C,_n1,_n2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(C => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AO321D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:28 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-2-3 AND-OR Gate
// Z = ( A1 & A2 & A3  ) | ( B1 & B2 ) | C
module AO321D2 (Z, A1, A2, A3, B1, B2, C);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B1;
	input  B2;
	input  C;
	and _i0 (_n1,A3,A1,A2);
	and _i1 (_n2,B1,B2);
	or _i2 (Z,C,_n1,_n2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(C => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AO322D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:32 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-2-3 AND-OR Gate
// Z = ( A1 & A2 & A3  ) | ( B1 & B2 ) | ( C1 & C2 )
module AO322D1 (Z, A1, A2, A3, B1, B2, C1, C2);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B1;
	input  B2;
	input  C1;
	input  C2;
	and _i0 (_n1,A3,A1,A2);
	and _i1 (_n2,B1,B2);
	and _i2 (_n3,C1,C2);
	or _i3 (Z,_n1,_n2,_n3);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(C1 => Z) = (0,0);
	(C2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AO322D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:32 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-2-3 AND-OR Gate
// Z = ( A1 & A2 & A3  ) | ( B1 & B2 ) | ( C1 & C2 )
module AO322D2 (Z, A1, A2, A3, B1, B2, C1, C2);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B1;
	input  B2;
	input  C1;
	input  C2;
	and _i0 (_n1,A3,A1,A2);
	and _i1 (_n2,B1,B2);
	and _i2 (_n3,C1,C2);
	or _i3 (Z,_n1,_n2,_n3);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(C1 => Z) = (0,0);
	(C2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AO32D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:30 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-3 AND-OR Gate
// Z = ( A1 & A2 & A3  ) | ( B1 & B2 )
module AO32D1 (Z, A1, A2, A3, B1, B2);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B1;
	input  B2;
	and _i0 (_n1,A3,A1,A2);
	and _i1 (_n2,B1,B2);
	or _i2 (Z,_n1,_n2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AO32D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:32 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-3 AND-OR Gate
// Z = ( A1 & A2 & A3  ) | ( B1 & B2 )
module AO32D2 (Z, A1, A2, A3, B1, B2);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B1;
	input  B2;
	and _i0 (_n1,A3,A1,A2);
	and _i1 (_n2,B1,B2);
	or _i2 (Z,_n1,_n2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AO332D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:28 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-3-3 AND-OR Gate
// Z = ( A1 & A2 & A3  ) | ( B1 & B2 & B3 ) | ( C1 & C2 )
module AO332D1 (Z, A1, A2, A3, B1, B2, B3, C1, C2);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B1;
	input  B2;
	input  B3;
	input  C1;
	input  C2;
	and _i0 (_n1,A3,A1,A2);
	and _i1 (_n2,B3,B1,B2);
	and _i2 (_n3,C1,C2);
	or _i3 (Z,_n1,_n2,_n3);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(B3 => Z) = (0,0);
	(C1 => Z) = (0,0);
	(C2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AO332D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:27 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-3-3 AND-OR Gate
// Z = ( A1 & A2 & A3  ) | ( B1 & B2 & B3 ) | ( C1 & C2 )
module AO332D2 (Z, A1, A2, A3, B1, B2, B3, C1, C2);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B1;
	input  B2;
	input  B3;
	input  C1;
	input  C2;
	and _i0 (_n1,A3,A1,A2);
	and _i1 (_n2,B3,B1,B2);
	and _i2 (_n3,C1,C2);
	or _i3 (Z,_n1,_n2,_n3);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(B3 => Z) = (0,0);
	(C1 => Z) = (0,0);
	(C2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AO333D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:28 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 3-3-3 AND-OR Gate
// Z = ( A1 & A2 & A3  ) | ( B1 & B2 & B3 ) | ( C1 & C2 & C3 )
module AO333D1 (Z, A1, A2, A3, B1, B2, B3, C1, C2, C3);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B1;
	input  B2;
	input  B3;
	input  C1;
	input  C2;
	input  C3;
	and _i0 (_n1,A3,A1,A2);
	and _i1 (_n2,B3,B1,B2);
	and _i2 (_n3,C3,C1,C2);
	or _i3 (Z,_n1,_n2,_n3);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(B3 => Z) = (0,0);
	(C1 => Z) = (0,0);
	(C2 => Z) = (0,0);
	(C3 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AO333D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:24 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 3-3-3 AND-OR Gate
// Z = ( A1 & A2 & A3  ) | ( B1 & B2 & B3 ) | ( C1 & C2 & C3 )
module AO333D2 (Z, A1, A2, A3, B1, B2, B3, C1, C2, C3);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B1;
	input  B2;
	input  B3;
	input  C1;
	input  C2;
	input  C3;
	and _i0 (_n1,A3,A1,A2);
	and _i1 (_n2,B3,B1,B2);
	and _i2 (_n3,C3,C1,C2);
	or _i3 (Z,_n1,_n2,_n3);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(B3 => Z) = (0,0);
	(C1 => Z) = (0,0);
	(C2 => Z) = (0,0);
	(C3 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AO33D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:20 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 3-3 AND-OR Gate
// Z = ( A1 & A2 & A3  ) | ( B1 & B2 & B3 )
module AO33D1 (Z, A1, A2, A3, B1, B2, B3);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B1;
	input  B2;
	input  B3;
	and _i0 (_n1,A3,A1,A2);
	and _i1 (_n2,B3,B1,B2);
	or _i2 (Z,_n1,_n2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(B3 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AO33D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:19 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 3-3 AND-OR Gate
// Z = ( A1 & A2 & A3  ) | ( B1 & B2 & B3 )
module AO33D2 (Z, A1, A2, A3, B1, B2, B3);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B1;
	input  B2;
	input  B3;
	and _i0 (_n1,A3,A1,A2);
	and _i1 (_n2,B3,B1,B2);
	or _i2 (Z,_n1,_n2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(B3 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AOI211D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:23 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-1-2 AND-OR-Invert Gate
// Z = ! ( ( A1 & A2  ) | B | C  )
module AOI211D1 (Z, A1, A2, B, C);
	output Z;
	input  A1;
	input  A2;
	input  B;
	input  C;
	and _i0 (_n1,A1,A2);
	nor _i1 (Z,C,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B => Z) = (0,0);
	(C => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AOI211D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:19 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-1-2 AND-OR-Invert Gate
// Z = ! ( ( A1 & A2  ) | B | C  )
module AOI211D2 (Z, A1, A2, B, C);
	output Z;
	input  A1;
	input  A2;
	input  B;
	input  C;
	and _i0 (_n1,A1,A2);
	nor _i1 (Z,C,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B => Z) = (0,0);
	(C => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AOI21D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:17 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-2 AND-OR-Invert Gate
// Z = ! ( B | ( A1 & A2  )  )
module AOI21D1 (Z, A1, A2, B);
	output Z;
	input  A1;
	input  A2;
	input  B;
	and _i0 (_n1,A1,A2);
	nor _i1 (Z,B,_n1);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AOI21D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:15 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-2 AND-OR-Invert Gate
// Z = ! ( B | ( A1 & A2  )  )
module AOI21D2 (Z, A1, A2, B);
	output Z;
	input  A1;
	input  A2;
	input  B;
	and _i0 (_n1,A1,A2);
	nor _i1 (Z,B,_n1);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AOI21M10D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:21 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-2 AND-OR-Invert Gate with Input A1 Inverted
// Z = ! ( B | ( ! A1 & A2  )  )
module AOI21M10D1 (Z, A1, A2, B);
	output Z;
	input  A1;
	input  A2;
	input  B;
	not _i0 (_n2,A1);
	and _i1 (_n1,_n2,A2);
	nor _i2 (Z,B,_n1);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AOI21M10D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:17 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-2 AND-OR-Invert Gate with Input A1 Inverted
// Z = ! ( B | ( ! A1 & A2  )  )
module AOI21M10D2 (Z, A1, A2, B);
	output Z;
	input  A1;
	input  A2;
	input  B;
	not _i0 (_n2,A1);
	and _i1 (_n1,_n2,A2);
	nor _i2 (Z,B,_n1);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AOI21M20D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:17 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-2 AND-OR-Invert Gate with Inputs A1 and A2 Inverted
// Z = ! ( B | ( ! A1 & ! A2  )  )
module AOI21M20D1 (Z, A1, A2, B);
	output Z;
	input  A1;
	input  A2;
	input  B;
	not _i0 (_n2,A1);
	not _i1 (_n3,A2);
	and _i2 (_n1,_n2,_n3);
	nor _i3 (Z,B,_n1);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AOI21M20D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:11 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-2 AND-OR-Invert Gate with Inputs A1 and A2 Inverted
// Z = ! ( B | ( ! A1 & ! A2  )  )
module AOI21M20D2 (Z, A1, A2, B);
	output Z;
	input  A1;
	input  A2;
	input  B;
	not _i0 (_n2,A1);
	not _i1 (_n3,A2);
	and _i2 (_n1,_n2,_n3);
	nor _i3 (Z,B,_n1);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AOI221D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:17 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-2-2 AND-OR-Invert Gate
// Z = ! ( ( A1 & A2  ) | ( B1 & B2 ) | C  )
module AOI221D1 (Z, A1, A2, B1, B2, C);
	output Z;
	input  A1;
	input  A2;
	input  B1;
	input  B2;
	input  C;
	and _i0 (_n1,A1,A2);
	and _i1 (_n2,B1,B2);
	nor _i2 (Z,C,_n1,_n2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(C => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AOI221D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:14 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-2-2 AND-OR-Invert Gate
// Z = ! ( ( A1 & A2  ) | ( B1 & B2 ) | C  )
module AOI221D2 (Z, A1, A2, B1, B2, C);
	output Z;
	input  A1;
	input  A2;
	input  B1;
	input  B2;
	input  C;
	and _i0 (_n1,A1,A2);
	and _i1 (_n2,B1,B2);
	nor _i2 (Z,C,_n1,_n2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(C => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AOI2222D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:16 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-2-2-2 AND-OR-Invert Gate
// Z = ! ( ( A1 & A2  ) | ( B1 & B2 ) | ( C1 & C2 ) | ( D1 & D2 )  )
module AOI2222D1 (Z, A1, A2, B1, B2, C1, C2, D1, D2);
	output Z;
	input  A1;
	input  A2;
	input  B1;
	input  B2;
	input  C1;
	input  C2;
	input  D1;
	input  D2;
	and _i0 (_n1,A1,A2);
	and _i1 (_n2,B1,B2);
	and _i2 (_n3,C1,C2);
	and _i3 (_n4,D1,D2);
	nor _i4 (Z,_n1,_n2,_n3,_n4);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(C1 => Z) = (0,0);
	(C2 => Z) = (0,0);
	(D1 => Z) = (0,0);
	(D2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AOI2222D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:09 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-2-2-2 AND-OR-Invert Gate
// Z = ! ( ( A1 & A2  ) | ( B1 & B2 ) | ( C1 & C2 ) | ( D1 & D2 )  )
module AOI2222D2 (Z, A1, A2, B1, B2, C1, C2, D1, D2);
	output Z;
	input  A1;
	input  A2;
	input  B1;
	input  B2;
	input  C1;
	input  C2;
	input  D1;
	input  D2;
	and _i0 (_n1,A1,A2);
	and _i1 (_n2,B1,B2);
	and _i2 (_n3,C1,C2);
	and _i3 (_n4,D1,D2);
	nor _i4 (Z,_n1,_n2,_n3,_n4);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(C1 => Z) = (0,0);
	(C2 => Z) = (0,0);
	(D1 => Z) = (0,0);
	(D2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AOI222D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:04 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-2-2 AND-OR-Invert Gate
// Z = ! ( ( A1 & A2  ) | ( B1 & B2 ) | ( C1 & C2 )  )
module AOI222D1 (Z, A1, A2, B1, B2, C1, C2);
	output Z;
	input  A1;
	input  A2;
	input  B1;
	input  B2;
	input  C1;
	input  C2;
	and _i0 (_n1,A1,A2);
	and _i1 (_n2,B1,B2);
	and _i2 (_n3,C1,C2);
	nor _i3 (Z,_n1,_n2,_n3);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(C1 => Z) = (0,0);
	(C2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AOI222D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:09 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-2-2 AND-OR-Invert Gate
// Z = ! ( ( A1 & A2  ) | ( B1 & B2 ) | ( C1 & C2 )  )
module AOI222D2 (Z, A1, A2, B1, B2, C1, C2);
	output Z;
	input  A1;
	input  A2;
	input  B1;
	input  B2;
	input  C1;
	input  C2;
	and _i0 (_n1,A1,A2);
	and _i1 (_n2,B1,B2);
	and _i2 (_n3,C1,C2);
	nor _i3 (Z,_n1,_n2,_n3);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(C1 => Z) = (0,0);
	(C2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AOI22D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:08 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-2 AND-OR-Invert Gate
// Z = ! ( ( A1 & A2  ) | ( B1 & B2 )  )
module AOI22D1 (Z, A1, A2, B1, B2);
	output Z;
	input  A1;
	input  A2;
	input  B1;
	input  B2;
	and _i0 (_n1,A1,A2);
	and _i1 (_n2,B1,B2);
	nor _i2 (Z,_n1,_n2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AOI22D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:09 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-2 AND-OR-Invert Gate
// Z = ! ( ( A1 & A2  ) | ( B1 & B2 )  )
module AOI22D2 (Z, A1, A2, B1, B2);
	output Z;
	input  A1;
	input  A2;
	input  B1;
	input  B2;
	and _i0 (_n1,A1,A2);
	and _i1 (_n2,B1,B2);
	nor _i2 (Z,_n1,_n2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AOI22M10D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:09 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-2 AND-OR-Invert Gate with Input A1 Inverted
// Z = ! ( ( ! A1 & A2  ) | ( B1 & B2 )  )
module AOI22M10D1 (Z, A1, A2, B1, B2);
	output Z;
	input  A1;
	input  A2;
	input  B1;
	input  B2;
	not _i0 (_n2,A1);
	and _i1 (_n1,_n2,A2);
	and _i2 (_n3,B1,B2);
	nor _i3 (Z,_n1,_n3);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AOI22M10D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:05 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-2 AND-OR-Invert Gate with Input A1 Inverted
// Z = ! ( ( ! A1 & A2  ) | ( B1 & B2 )  )
module AOI22M10D2 (Z, A1, A2, B1, B2);
	output Z;
	input  A1;
	input  A2;
	input  B1;
	input  B2;
	not _i0 (_n2,A1);
	and _i1 (_n1,_n2,A2);
	and _i2 (_n3,B1,B2);
	nor _i3 (Z,_n1,_n3);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AOI22M11D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:05 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-2 AND-OR-Invert Gate with Inputs A1 and B1 Inverted
// Z = ! ( ( ! A1 & A2  ) | ( ! B1 & B2 )  )
module AOI22M11D1 (Z, A1, A2, B1, B2);
	output Z;
	input  A1;
	input  A2;
	input  B1;
	input  B2;
	not _i0 (_n2,A1);
	and _i1 (_n1,_n2,A2);
	not _i2 (_n4,B1);
	and _i3 (_n3,_n4,B2);
	nor _i4 (Z,_n1,_n3);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AOI22M11D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:00 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-2 AND-OR-Invert Gate with Inputs A1 and B1 Inverted
// Z = ! ( ( ! A1 & A2  ) | ( ! B1 & B2 )  )
module AOI22M11D2 (Z, A1, A2, B1, B2);
	output Z;
	input  A1;
	input  A2;
	input  B1;
	input  B2;
	not _i0 (_n2,A1);
	and _i1 (_n1,_n2,A2);
	not _i2 (_n4,B1);
	and _i3 (_n3,_n4,B2);
	nor _i4 (Z,_n1,_n3);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AOI22M20D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:04 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-2 AND-OR-Invert Gate with Inputs A1 and A2 Inverted
// Z = ! ( ( ! A1 & ! A2  ) | ( B1 & B2 )  )
module AOI22M20D1 (Z, A1, A2, B1, B2);
	output Z;
	input  A1;
	input  A2;
	input  B1;
	input  B2;
	not _i0 (_n2,A1);
	not _i1 (_n3,A2);
	and _i2 (_n1,_n2,_n3);
	and _i3 (_n4,B1,B2);
	nor _i4 (Z,_n1,_n4);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AOI22M20D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:07 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-2 AND-OR-Invert Gate with Inputs A1 and A2 Inverted
// Z = ! ( ( ! A1 & ! A2  ) | ( B1 & B2 )  )
module AOI22M20D2 (Z, A1, A2, B1, B2);
	output Z;
	input  A1;
	input  A2;
	input  B1;
	input  B2;
	not _i0 (_n2,A1);
	not _i1 (_n3,A2);
	and _i2 (_n1,_n2,_n3);
	and _i3 (_n4,B1,B2);
	nor _i4 (Z,_n1,_n4);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AOI22M21D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:01 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-2 AND-OR-Invert Gate with Inputs A1, A2 and B1 Inverted
// Z = ! ( ( ! A1 & ! A2  ) | ( ! B1 & B2 )  )
module AOI22M21D1 (Z, A1, A2, B1, B2);
	output Z;
	input  A1;
	input  A2;
	input  B1;
	input  B2;
	not _i0 (_n2,A1);
	not _i1 (_n3,A2);
	and _i2 (_n1,_n2,_n3);
	not _i3 (_n5,B1);
	and _i4 (_n4,_n5,B2);
	nor _i5 (Z,_n1,_n4);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AOI22M21D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:01 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-2 AND-OR-Invert Gate with Inputs A1, A2 and B1 Inverted
// Z = ! ( ( ! A1 & ! A2  ) | ( ! B1 & B2 )  )
module AOI22M21D2 (Z, A1, A2, B1, B2);
	output Z;
	input  A1;
	input  A2;
	input  B1;
	input  B2;
	not _i0 (_n2,A1);
	not _i1 (_n3,A2);
	and _i2 (_n1,_n2,_n3);
	not _i3 (_n5,B1);
	and _i4 (_n4,_n5,B2);
	nor _i5 (Z,_n1,_n4);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AOI22M22D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:00 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-2 AND-OR-Invert Gate with all Inputs Inverted
// Z = ! ( ( ! A1 & ! A2  ) | ( ! B1 & ! B2 )  )
module AOI22M22D1 (Z, A1, A2, B1, B2);
	output Z;
	input  A1;
	input  A2;
	input  B1;
	input  B2;
	not _i0 (_n2,A1);
	not _i1 (_n3,A2);
	and _i2 (_n1,_n2,_n3);
	not _i3 (_n5,B1);
	not _i4 (_n6,B2);
	and _i5 (_n4,_n5,_n6);
	nor _i6 (Z,_n1,_n4);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AOI22M22D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:52:01 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-2 AND-OR-Invert Gate with all Inputs Inverted
// Z = ! ( ( ! A1 & ! A2  ) | ( ! B1 & ! B2 )  )
module AOI22M22D2 (Z, A1, A2, B1, B2);
	output Z;
	input  A1;
	input  A2;
	input  B1;
	input  B2;
	not _i0 (_n2,A1);
	not _i1 (_n3,A2);
	and _i2 (_n1,_n2,_n3);
	not _i3 (_n5,B1);
	not _i4 (_n6,B2);
	and _i5 (_n4,_n5,_n6);
	nor _i6 (Z,_n1,_n4);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AOI311D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:55 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-1-3 AND-OR-Invert Gate
// Z = ! ( ( A1 & A2 & A3  ) | B | C  )
module AOI311D1 (Z, A1, A2, A3, B, C);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B;
	input  C;
	and _i0 (_n1,A3,A1,A2);
	nor _i1 (Z,C,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B => Z) = (0,0);
	(C => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AOI311D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:53 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-1-3 AND-OR-Invert Gate
// Z = ! ( ( A1 & A2 & A3  ) | B | C  )
module AOI311D2 (Z, A1, A2, A3, B, C);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B;
	input  C;
	and _i0 (_n1,A3,A1,A2);
	nor _i1 (Z,C,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B => Z) = (0,0);
	(C => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AOI31D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:57 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-3 AND-OR-Invert Gate
// Z = ! ( ( A1 & A2 & A3  ) | B  )
module AOI31D1 (Z, A1, A2, A3, B);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B;
	and _i0 (_n1,A3,A1,A2);
	nor _i1 (Z,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AOI31D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:50 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-3 AND-OR-Invert Gate
// Z = ! ( ( A1 & A2 & A3  ) | B  )
module AOI31D2 (Z, A1, A2, A3, B);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B;
	and _i0 (_n1,A3,A1,A2);
	nor _i1 (Z,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AOI31M10D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:51 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-3 AND-OR-Invert Gate with Input A1 Inverted
// Z = ! ( ( ! A1 & A2 & A3  ) | B  )
module AOI31M10D1 (Z, A1, A2, A3, B);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B;
	not _i0 (_n2,A1);
	and _i1 (_n1,A3,_n2,A2);
	nor _i2 (Z,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AOI31M10D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:49 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-3 AND-OR-Invert Gate with Input A1 Inverted
// Z = ! ( ( ! A1 & A2 & A3  ) | B  )
module AOI31M10D2 (Z, A1, A2, A3, B);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B;
	not _i0 (_n2,A1);
	and _i1 (_n1,A3,_n2,A2);
	nor _i2 (Z,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AOI31M20D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:50 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-3 AND-OR-Invert Gate with Inputs A1 and A2 Inverted
// Z = ! ( ( ! A1 & ! A2 & A3  ) | B  )
module AOI31M20D1 (Z, A1, A2, A3, B);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B;
	not _i0 (_n2,A1);
	not _i1 (_n3,A2);
	and _i2 (_n1,A3,_n2,_n3);
	nor _i3 (Z,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AOI31M20D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:55 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-3 AND-OR-Invert Gate with Inputs A1 and A2 Inverted
// Z = ! ( ( ! A1 & ! A2 & A3  ) | B  )
module AOI31M20D2 (Z, A1, A2, A3, B);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B;
	not _i0 (_n2,A1);
	not _i1 (_n3,A2);
	and _i2 (_n1,A3,_n2,_n3);
	nor _i3 (Z,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AOI31M30D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:46 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-3 AND-OR-Invert Gate with Inputs A1, A2 and A3 Inverted
// Z = ! ( ( ! A1 & ! A2 & ! A3  ) | B  )
module AOI31M30D1 (Z, A1, A2, A3, B);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B;
	not _i0 (_n2,A3);
	not _i1 (_n3,A1);
	not _i2 (_n4,A2);
	and _i3 (_n1,_n2,_n3,_n4);
	nor _i4 (Z,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AOI31M30D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:49 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-3 AND-OR-Invert Gate with Inputs A1, A2 and A3 Inverted
// Z = ! ( ( ! A1 & ! A2 & ! A3  ) | B  )
module AOI31M30D2 (Z, A1, A2, A3, B);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B;
	not _i0 (_n2,A3);
	not _i1 (_n3,A1);
	not _i2 (_n4,A2);
	and _i3 (_n1,_n2,_n3,_n4);
	nor _i4 (Z,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AOI321D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:49 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-2-3 AND-OR-Invert Gate
// Z = ! ( ( A1 & A2 & A3  ) | ( B1 & B2 ) | C  )
module AOI321D1 (Z, A1, A2, A3, B1, B2, C);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B1;
	input  B2;
	input  C;
	and _i0 (_n1,A3,A1,A2);
	and _i1 (_n2,B1,B2);
	nor _i2 (Z,C,_n1,_n2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(C => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AOI321D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:47 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-2-3 AND-OR-Invert Gate
// Z = ! ( ( A1 & A2 & A3  ) | ( B1 & B2 ) | C  )
module AOI321D2 (Z, A1, A2, A3, B1, B2, C);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B1;
	input  B2;
	input  C;
	and _i0 (_n1,A3,A1,A2);
	and _i1 (_n2,B1,B2);
	nor _i2 (Z,C,_n1,_n2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(C => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AOI322D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:50 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-2-3 AND-OR-Invert Gate
// Z = ! ( ( A1 & A2 & A3  ) | ( B1 & B2 ) | ( C1 & C2 )  )
module AOI322D1 (Z, A1, A2, A3, B1, B2, C1, C2);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B1;
	input  B2;
	input  C1;
	input  C2;
	and _i0 (_n1,A3,A1,A2);
	and _i1 (_n2,B1,B2);
	and _i2 (_n3,C1,C2);
	nor _i3 (Z,_n1,_n2,_n3);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(C1 => Z) = (0,0);
	(C2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AOI322D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:46 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-2-3 AND-OR-Invert Gate
// Z = ! ( ( A1 & A2 & A3  ) | ( B1 & B2 ) | ( C1 & C2 )  )
module AOI322D2 (Z, A1, A2, A3, B1, B2, C1, C2);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B1;
	input  B2;
	input  C1;
	input  C2;
	and _i0 (_n1,A3,A1,A2);
	and _i1 (_n2,B1,B2);
	and _i2 (_n3,C1,C2);
	nor _i3 (Z,_n1,_n2,_n3);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(C1 => Z) = (0,0);
	(C2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AOI32D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:46 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-3 AND-OR-Invert Gate
// Z = ! ( ( A1 & A2 & A3  ) | ( B1 & B2 )  )
module AOI32D1 (Z, A1, A2, A3, B1, B2);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B1;
	input  B2;
	and _i0 (_n1,A3,A1,A2);
	and _i1 (_n2,B1,B2);
	nor _i2 (Z,_n1,_n2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AOI32D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:42 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-3 AND-OR-Invert Gate
// Z = ! ( ( A1 & A2 & A3  ) | ( B1 & B2 )  )
module AOI32D2 (Z, A1, A2, A3, B1, B2);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B1;
	input  B2;
	and _i0 (_n1,A3,A1,A2);
	and _i1 (_n2,B1,B2);
	nor _i2 (Z,_n1,_n2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AOI332D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:39 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-3-3 AND-OR-Invert Gate
// Z = ! ( ( A1 & A2 & A3  ) | ( B1 & B2 & B3 ) | ( C1 & C2 )  )
module AOI332D1 (Z, A1, A2, A3, B1, B2, B3, C1, C2);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B1;
	input  B2;
	input  B3;
	input  C1;
	input  C2;
	and _i0 (_n1,A3,A1,A2);
	and _i1 (_n2,B3,B1,B2);
	and _i2 (_n3,C1,C2);
	nor _i3 (Z,_n1,_n2,_n3);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(B3 => Z) = (0,0);
	(C1 => Z) = (0,0);
	(C2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AOI332D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:37 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-3-3 AND-OR-Invert Gate
// Z = ! ( ( A1 & A2 & A3  ) | ( B1 & B2 & B3 ) | ( C1 & C2 )  )
module AOI332D2 (Z, A1, A2, A3, B1, B2, B3, C1, C2);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B1;
	input  B2;
	input  B3;
	input  C1;
	input  C2;
	and _i0 (_n1,A3,A1,A2);
	and _i1 (_n2,B3,B1,B2);
	and _i2 (_n3,C1,C2);
	nor _i3 (Z,_n1,_n2,_n3);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(B3 => Z) = (0,0);
	(C1 => Z) = (0,0);
	(C2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AOI333D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:40 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 3-3-3 AND-OR-Invert Gate
// Z = ! ( ( A1 & A2 & A3  ) | ( B1 & B2 & B3 ) | ( C1 & C2 & C3 )  )
module AOI333D1 (Z, A1, A2, A3, B1, B2, B3, C1, C2, C3);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B1;
	input  B2;
	input  B3;
	input  C1;
	input  C2;
	input  C3;
	and _i0 (_n1,A3,A1,A2);
	and _i1 (_n2,B3,B1,B2);
	and _i2 (_n3,C3,C1,C2);
	nor _i3 (Z,_n1,_n2,_n3);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(B3 => Z) = (0,0);
	(C1 => Z) = (0,0);
	(C2 => Z) = (0,0);
	(C3 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AOI333D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:39 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 3-3-3 AND-OR-Invert Gate
// Z = ! ( ( A1 & A2 & A3  ) | ( B1 & B2 & B3 ) | ( C1 & C2 & C3 )  )
module AOI333D2 (Z, A1, A2, A3, B1, B2, B3, C1, C2, C3);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B1;
	input  B2;
	input  B3;
	input  C1;
	input  C2;
	input  C3;
	and _i0 (_n1,A3,A1,A2);
	and _i1 (_n2,B3,B1,B2);
	and _i2 (_n3,C3,C1,C2);
	nor _i3 (Z,_n1,_n2,_n3);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(B3 => Z) = (0,0);
	(C1 => Z) = (0,0);
	(C2 => Z) = (0,0);
	(C3 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AOI33D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:33 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 3-3 AND-OR-Invert Gate
// Z = ! ( ( A1 & A2 & A3  ) | ( B1 & B2 & B3 )  )
module AOI33D1 (Z, A1, A2, A3, B1, B2, B3);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B1;
	input  B2;
	input  B3;
	and _i0 (_n1,A3,A1,A2);
	and _i1 (_n2,B3,B1,B2);
	nor _i2 (Z,_n1,_n2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(B3 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AOI33D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:37 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 3-3 AND-OR-Invert Gate
// Z = ! ( ( A1 & A2 & A3  ) | ( B1 & B2 & B3 )  )
module AOI33D2 (Z, A1, A2, A3, B1, B2, B3);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B1;
	input  B2;
	input  B3;
	and _i0 (_n1,A3,A1,A2);
	and _i1 (_n2,B3,B1,B2);
	nor _i2 (Z,_n1,_n2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(B3 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AON211D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:40 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-1-2 AND-OR-NAND Gate
// Z = ! ( C & ( B | A1 & A2  )  )
module AON211D1 (Z, A1, A2, B, C);
	output Z;
	input  A1;
	input  A2;
	input  B;
	input  C;
	and _i0 (_n2,A1,A2);
	or _i1 (_n1,B,_n2);
	nand _i2 (Z,C,_n1);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B => Z) = (0,0);
	(C => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       AON211D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:37 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-1-2 AND-OR-NAND Gate
// Z = ! ( C & ( B | A1 & A2  )  )
module AON211D2 (Z, A1, A2, B, C);
	output Z;
	input  A1;
	input  A2;
	input  B;
	input  C;
	and _i0 (_n2,A1,A2);
	or _i1 (_n1,B,_n2);
	nand _i2 (Z,C,_n1);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B => Z) = (0,0);
	(C => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       BUFBD16.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:33 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// Buffers
// Z = A
module BUFBD16 (Z, A);
	output Z;
	input  A;
	buf _i0 (Z,A);
	specify		(A => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       BUFBD32.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:33 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// Buffers
// Z = A
module BUFBD32 (Z, A);
	output Z;
	input  A;
	buf _i0 (Z,A);
	specify		(A => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       BUFBD4.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:30 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// Buffers
// Z = A
module BUFBD4 (Z, A);
	output Z;
	input  A;
	buf _i0 (Z,A);
	specify		(A => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       BUFBD8.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:34 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// Buffers
// Z = A
module BUFBD8 (Z, A);
	output Z;
	input  A;
	buf _i0 (Z,A);
	specify		(A => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       BUFD1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:29 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// Buffers
// Z = A
module BUFD1 (Z, A);
	output Z;
	input  A;
	buf _i0 (Z,A);
	specify		(A => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       BUFD3.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:29 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// Buffers
// Z = A
module BUFD3 (Z, A);
	output Z;
	input  A;
	buf _i0 (Z,A);
	specify		(A => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       BUFD7.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:29 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// Buffers
// Z = A
module BUFD7 (Z, A);
	output Z;
	input  A;
	buf _i0 (Z,A);
	specify		(A => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       BUFDA.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:19 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// Buffers
// Z = A
module BUFDA (Z, A);
	output Z;
	input  A;
	buf _i0 (Z,A);
	specify		(A => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       BUFDA.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:19 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// Buffers
// Z = A
module ARTX_BUFDA (Z, A);
	output Z;
	input  A;
	buf _i0 (Z,A);
	specify		(A => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       BUFTBD16.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:19 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 3-State Buffers with Active-Low Enable
// Z = ENB ? 'z' : A
module BUFTBD16 (Z, A, ENB);
	output Z;
	input  A;
	input  ENB;
	bufif0 _i0 (Z, A, ENB);
	specify		(A => Z) = (0,0);
	(ENB => Z) = (0,0,0,0,0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       BUFTBD32.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:24 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 3-State Buffers with Active-Low Enable
// Z = ENB ? 'z' : A
module BUFTBD32 (Z, A, ENB);
	output Z;
	input  A;
	input  ENB;
	bufif0 _i0 (Z, A, ENB);
	specify		(A => Z) = (0,0);
	(ENB => Z) = (0,0,0,0,0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       BUFTBD4.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:27 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 3-State Buffers with Active-Low Enable
// Z = ENB ? 'z' : A
module BUFTBD4 (Z, A, ENB);
	output Z;
	input  A;
	input  ENB;
	bufif0 _i0 (Z, A, ENB);
	specify		(A => Z) = (0,0);
	(ENB => Z) = (0,0,0,0,0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       BUFTBD8.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:25 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 3-State Buffers with Active-Low Enable
// Z = ENB ? 'z' : A
module BUFTBD8 (Z, A, ENB);
	output Z;
	input  A;
	input  ENB;
	bufif0 _i0 (Z, A, ENB);
	specify		(A => Z) = (0,0);
	(ENB => Z) = (0,0,0,0,0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       BUFTD1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:27 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 3-State Buffers with Active-Low Enable
// Z = ENB ? 'z' : A
module BUFTD1 (Z, A, ENB);
	output Z;
	input  A;
	input  ENB;
	bufif0 _i0 (Z, A, ENB);
	specify		(A => Z) = (0,0);
	(ENB => Z) = (0,0,0,0,0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       BUFTD2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:27 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 3-State Buffers with Active-Low Enable
// Z = ENB ? 'z' : A
module BUFTD2 (Z, A, ENB);
	output Z;
	input  A;
	input  ENB;
	bufif0 _i0 (Z, A, ENB);
	specify		(A => Z) = (0,0);
	(ENB => Z) = (0,0,0,0,0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       BUFTD4.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:24 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 3-State Buffers with Active-Low Enable
// Z = ENB ? 'z' : A
module BUFTD4 (Z, A, ENB);
	output Z;
	input  A;
	input  ENB;
	bufif0 _i0 (Z, A, ENB);
	specify		(A => Z) = (0,0);
	(ENB => Z) = (0,0,0,0,0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       BUFTD7.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:19 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 3-State Buffers with Active-Low Enable
// Z = ENB ? 'z' : A
module BUFTD7 (Z, A, ENB);
	output Z;
	input  A;
	input  ENB;
	bufif0 _i0 (Z, A, ENB);
	specify		(A => Z) = (0,0);
	(ENB => Z) = (0,0,0,0,0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       BUFTDA.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:16 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 3-State Buffers with Active-Low Enable
// Z = ENB ? 'z' : A
module BUFTDA (Z, A, ENB);
	output Z;
	input  A;
	input  ENB;
	bufif0 _i0 (Z, A, ENB);
	specify		(A => Z) = (0,0);
	(ENB => Z) = (0,0,0,0,0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       BUFTDC.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:19 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 3-State Buffers with Active-Low Enable
// Z = ENB ? 'z' : A
module BUFTDC (Z, A, ENB);
	output Z;
	input  A;
	input  ENB;
	bufif0 _i0 (Z, A, ENB);
	specify		(A => Z) = (0,0);
	(ENB => Z) = (0,0,0,0,0,0);
	endspecify
endmodule
`endcelldefine
module CC_WEND4 ( SE_0, SD_0, QB_0, Q_0, D_0, CK_0, Z_1, A_1, Z_2, A2_2, A1_2, Z_3, A_3 );

   input  SE_0;
   input  SD_0;
   output QB_0;
   output Q_0;
   input  D_0;
   input  CK_0;
   output Z_1;
   input  A_1;
   output Z_2;
   input  A2_2;
   input  A1_2;
   output Z_3;
   input  A_3;

   SDFPB2 X_0 ( .CK(CK_0 ) , .D(D_0 ) , .SD(SD_0 ) , .SE(SE_0 ) , .Q(Q_0 ) , .QB(QB_0 ) );
   DLY3DA X_1 ( .A(A_1 ) , .Z(Z_1 ) );
   NAN2D4 X_2 ( .A1(A1_2 ) , .A2(A2_2 ) , .Z(Z_2 ) );
   INVD1 X_3 ( .A(A_3 ) , .Z(Z_3 ) );

endmodule
module CC_WLD7 ( SE_0, SD_0, QB_0, Q_0, D_0, CK_0, Z_1, B_1, A2_1, A1_1, Z_2, A_2 );

   input  SE_0;
   input  SD_0;
   output QB_0;
   output Q_0;
   input  D_0;
   input  CK_0;
   output Z_1;
   input  B_1;
   input  A2_1;
   input  A1_1;
   output Z_2;
   input  A_2;

   SDFPB2 X_0 ( .CK(CK_0 ) , .D(D_0 ) , .SD(SD_0 ) , .SE(SE_0 ) , .Q(Q_0 ) , .QB(QB_0 ) );
   AO21M20D2 X_1 ( .A1(A1_1 ) , .A2(A2_1 ) , .B(B_1 ) , .Z(Z_1 ) );
   INVD7 X_2 ( .A(A_2 ) , .Z(Z_2 ) );

endmodule
module CKGATD4 ( Q_0, GB_0, D_0, Z_1, A3_1, A2_1, A1_1 );

   output Q_0;
   input  GB_0;
   input  D_0;
   output Z_1;
   input  A3_1;
   input  A2_1;
   input  A1_1;

   LATPQ1 X_0 ( .D(D_0 ) , .GB(GB_0 ) , .Q(Q_0 ) );
   NAN3D4 X_1 ( .A1(A1_1 ) , .A2(A2_1 ) , .A3(A3_1 ) , .Z(Z_1 ) );

endmodule
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       CKLATD4.v 
// Library Name:    pez_d18_sc_necned3_c7 
// Library Release: 1.2 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/29/99 17:32:43 
//  
// verigen patch-level 1.131-x 05/11/99 19:44:59
`celldefine
// Custom Cell
// EO = !(CK) ? D : 'p';Z = !(CK & ENB & EI)
module CKLATD4 (EO, Z, CK, D, EI, ENB);
	output EO;
	output Z;
	input  CK;
	input  D;
	input  EI;
	input  ENB;
	reg notifier;
	not _i0 (_n1,CK);
	p_latch _i1 (EO, D, _n1, notifier);
	nand _i2 (Z,EI,CK,ENB);
`ifdef no_tchk
`else
	not _wi0 (_wn1,EI);
	not _wi1 (_wn2,ENB);
	and _wi2 (shcheckCKDlh,_wn1,_wn2);
`endif
	specify		(CK => EO) = (1,1);
	(CK => Z) = (0,0);
	(D => EO) = (0,0);
	(EI => Z) = (0,0);
	(ENB => Z) = (0,0);
`ifdef no_tchk
`else
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,posedge D,0,notifier);
	$setup(negedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$width(negedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       CLOAD.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:11 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// Clock Load Cell
// DUMMY = A
module CLOAD (A);
	input  A;
	buf _i0 (DUMMY,A);
	specify		endspecify
endmodule
`endcelldefine
//  
// Copyright (C) 2000 Virtual Silicon Technology Inc.. All Rights Reserved. 
// "eSilicon", "eSi", "eSi-Route", "eSi-Pad", "eSi-RAM", "eSi-ROM", "eSi-PLL",  
// "The Heart of Great Silicon(R)", "Silicon Ready(R)", "IP Ambassador",  
// "Design Service Ambassador", and "Virtual Silicon" are trademarks  
// of Virtual Silicon Technology Inc.  
//  
// Virtual Silicon Technology Inc. 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       DEL100D4.v 
// Library Name:    NECD18N392T2_1.0 
// Library Release: 1.0 
// Process:         TBA NEC NED3 
// Generated:       06/06/2000 11:33:03 
//  
// verigen patch-level 1.132-m 02/27/2000 13:49:14
`celldefine
// Delays
// Z = A
module DEL100D4 (Z, A);
	output Z;
	input  A;
	buf _i0 (Z,A);
	specify		(A => Z) = (1,1);
	endspecify
endmodule
`endcelldefine
//  
// Copyright (C) 2000 Virtual Silicon Technology Inc.. All Rights Reserved. 
// "eSilicon", "eSi", "eSi-Route", "eSi-Pad", "eSi-RAM", "eSi-ROM", "eSi-PLL",  
// "The Heart of Great Silicon(R)", "Silicon Ready(R)", "IP Ambassador",  
// "Design Service Ambassador", and "Virtual Silicon" are trademarks  
// of Virtual Silicon Technology Inc.  
//  
// Virtual Silicon Technology Inc. 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       DEL150D4.v 
// Library Name:    NECD18N392T2_1.0 
// Library Release: 1.0 
// Process:         TBA NEC NED3 
// Generated:       06/06/2000 11:33:04 
//  
// verigen patch-level 1.132-m 02/27/2000 13:49:14
`celldefine
// Delays
// Z = A
module DEL150D4 (Z, A);
	output Z;
	input  A;
	buf _i0 (Z,A);
	specify		(A => Z) = (1,1);
	endspecify
endmodule
`endcelldefine
//  
// Copyright (C) 2000 Virtual Silicon Technology Inc.. All Rights Reserved. 
// "eSilicon", "eSi", "eSi-Route", "eSi-Pad", "eSi-RAM", "eSi-ROM", "eSi-PLL",  
// "The Heart of Great Silicon(R)", "Silicon Ready(R)", "IP Ambassador",  
// "Design Service Ambassador", and "Virtual Silicon" are trademarks  
// of Virtual Silicon Technology Inc.  
//  
// Virtual Silicon Technology Inc. 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       DEL200D4.v 
// Library Name:    NECD18N392T2_1.0 
// Library Release: 1.0 
// Process:         TBA NEC NED3 
// Generated:       06/06/2000 11:33:05 
//  
// verigen patch-level 1.132-m 02/27/2000 13:49:14
`celldefine
// Delays
// Z = A
module DEL200D4 (Z, A);
	output Z;
	input  A;
	buf _i0 (Z,A);
	specify		(A => Z) = (1,1);
	endspecify
endmodule
`endcelldefine
//  
// Copyright (C) 2000 Virtual Silicon Technology Inc.. All Rights Reserved. 
// "eSilicon", "eSi", "eSi-Route", "eSi-Pad", "eSi-RAM", "eSi-ROM", "eSi-PLL",  
// "The Heart of Great Silicon(R)", "Silicon Ready(R)", "IP Ambassador",  
// "Design Service Ambassador", and "Virtual Silicon" are trademarks  
// of Virtual Silicon Technology Inc.  
//  
// Virtual Silicon Technology Inc. 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       DEL400D4.v 
// Library Name:    NECD18N392T2_1.0 
// Library Release: 1.0 
// Process:         TBA NEC NED3 
// Generated:       06/06/2000 11:33:07 
//  
// verigen patch-level 1.132-m 02/27/2000 13:49:14
`celldefine
// Delays
// Z = A
module DEL400D4 (Z, A);
	output Z;
	input  A;
	buf _i0 (Z,A);
	specify		(A => Z) = (1,1);
	endspecify
endmodule
`endcelldefine
//  
// Copyright (C) 2000 Virtual Silicon Technology Inc.. All Rights Reserved. 
// "eSilicon", "eSi", "eSi-Route", "eSi-Pad", "eSi-RAM", "eSi-ROM", "eSi-PLL",  
// "The Heart of Great Silicon(R)", "Silicon Ready(R)", "IP Ambassador",  
// "Design Service Ambassador", and "Virtual Silicon" are trademarks  
// of Virtual Silicon Technology Inc.  
//  
// Virtual Silicon Technology Inc. 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       DEL600D4.v 
// Library Name:    NECD18N392T2_1.0 
// Library Release: 1.0 
// Process:         TBA NEC NED3 
// Generated:       06/06/2000 11:33:04 
//  
// verigen patch-level 1.132-m 02/27/2000 13:49:14
`celldefine
// Delays
// Z = A
module DEL600D4 (Z, A);
	output Z;
	input  A;
	buf _i0 (Z,A);
	specify		(A => Z) = (1,1);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       DFEPQ1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:16 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Enabled, Positive Edge, D Flip-Flop; Q Output
// Q  = rising(CK) ? (CEB ? 's' : D) : 'p'
module DFEPQ1 (Q, CEB, CK, D);
	output Q;
	input  CEB;
	input  CK;
	input  D;
	reg notifier;
	p_mux21 _i0 (_n1, buf_Q, D, CEB);
	p_ff _i1 (buf_Q, _n1, CK, notifier);
	buf _iQ(Q, buf_Q);
`ifdef no_tchk
`else
	not _wi0 (shcheckCKDlh,CEB);
`endif
	specify		(CK => Q) = (1,1);
`ifdef no_tchk
`else
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge CK,negedge CEB,0,notifier);
	$hold(posedge CK,posedge CEB,0,notifier);
	$setup(negedge CEB,posedge CK,0,notifier);
	$setup(negedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge CEB,posedge CK,0,notifier);
	$setup(posedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$width(negedge CK,1,0,notifier);
	$width(posedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       DFEPQ2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:12 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Enabled, Positive Edge, D Flip-Flop; Q Output
// Q  = rising(CK) ? (CEB ? 's' : D) : 'p'
module DFEPQ2 (Q, CEB, CK, D);
	output Q;
	input  CEB;
	input  CK;
	input  D;
	reg notifier;
	p_mux21 _i0 (_n1, buf_Q, D, CEB);
	p_ff _i1 (buf_Q, _n1, CK, notifier);
	buf _iQ(Q, buf_Q);
`ifdef no_tchk
`else
	not _wi0 (shcheckCKDlh,CEB);
`endif
	specify		(CK => Q) = (1,1);
`ifdef no_tchk
`else
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge CK,negedge CEB,0,notifier);
	$hold(posedge CK,posedge CEB,0,notifier);
	$setup(negedge CEB,posedge CK,0,notifier);
	$setup(negedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge CEB,posedge CK,0,notifier);
	$setup(posedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$width(negedge CK,1,0,notifier);
	$width(posedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       DFEPQ4.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:16 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Enabled, Positive Edge, D Flip-Flop; Q Output
// Q  = rising(CK) ? (CEB ? 's' : D) : 'p'
module DFEPQ4 (Q, CEB, CK, D);
	output Q;
	input  CEB;
	input  CK;
	input  D;
	reg notifier;
	p_mux21 _i0 (_n1, buf_Q, D, CEB);
	p_ff _i1 (buf_Q, _n1, CK, notifier);
	buf _iQ(Q, buf_Q);
`ifdef no_tchk
`else
	not _wi0 (shcheckCKDlh,CEB);
`endif
	specify		(CK => Q) = (1,1);
`ifdef no_tchk
`else
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge CK,negedge CEB,0,notifier);
	$hold(posedge CK,posedge CEB,0,notifier);
	$setup(negedge CEB,posedge CK,0,notifier);
	$setup(negedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge CEB,posedge CK,0,notifier);
	$setup(posedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$width(negedge CK,1,0,notifier);
	$width(posedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       DFERPB1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:08 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Enabled, Positive Edge, D Flip-Flop with Clear; Q, QB Output
// Q  = !RB ? 0 : rising(CK) ? (CEB ? 's' : D) : 'p';QB = !Q
module DFERPB1 (Q, QB, CEB, CK, D, RB);
	output Q;
	output QB;
	input  CEB;
	input  CK;
	input  D;
	input  RB;
	reg notifier;
	p_mux21 _i0 (_n1, buf_Q, D, CEB);
	not _i1 (_n2,RB);
	p_ffr _i2 (buf_Q, _n1, CK, _n2, notifier);
	not _i3 (QB,buf_Q);
	buf _iQ(Q, buf_Q);
`ifdef no_tchk
`else
	not _wi0 (_wn1,CEB);
	and _wi1 (shcheckCKDlh,_wn1,RB);
	and _wi3 (shcheckCKRBlh,_wn1,D);
	buf _wi4 (shcheckCKCEBlh,RB);
`endif
	specify		(CK => Q) = (1,1);
	(CK => QB) = (1,1);
	(RB => Q) = (0,0);
	(RB => QB) = (0,0);
`ifdef no_tchk
`else
	$hold(posedge CK &&& shcheckCKCEBlh === 1'b1,negedge CEB,0,notifier);
	$hold(posedge CK &&& shcheckCKCEBlh === 1'b1,posedge CEB,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge CK,posedge RB,0,notifier);
	$setup(negedge CEB,posedge CK &&& shcheckCKCEBlh === 1'b1,0,notifier);
	$setup(negedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge CEB,posedge CK &&& shcheckCKCEBlh === 1'b1,0,notifier);
	$setup(posedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge CK &&& shcheckCKRBlh === 1'b1,0,notifier);
	$width(negedge CK,1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(posedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       DFERPB2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:08 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Enabled, Positive Edge, D Flip-Flop with Clear; Q, QB Output
// Q  = !RB ? 0 : rising(CK) ? (CEB ? 's' : D) : 'p';QB = !Q
module DFERPB2 (Q, QB, CEB, CK, D, RB);
	output Q;
	output QB;
	input  CEB;
	input  CK;
	input  D;
	input  RB;
	reg notifier;
	p_mux21 _i0 (_n1, buf_Q, D, CEB);
	not _i1 (_n2,RB);
	p_ffr _i2 (buf_Q, _n1, CK, _n2, notifier);
	not _i3 (QB,buf_Q);
	buf _iQ(Q, buf_Q);
`ifdef no_tchk
`else
	not _wi0 (_wn1,CEB);
	and _wi1 (shcheckCKDlh,_wn1,RB);
	and _wi3 (shcheckCKRBlh,_wn1,D);
	buf _wi4 (shcheckCKCEBlh,RB);
`endif
	specify		(CK => Q) = (1,1);
	(CK => QB) = (1,1);
	(RB => Q) = (0,0);
	(RB => QB) = (0,0);
`ifdef no_tchk
`else
	$hold(posedge CK &&& shcheckCKCEBlh === 1'b1,negedge CEB,0,notifier);
	$hold(posedge CK &&& shcheckCKCEBlh === 1'b1,posedge CEB,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge CK,posedge RB,0,notifier);
	$setup(negedge CEB,posedge CK &&& shcheckCKCEBlh === 1'b1,0,notifier);
	$setup(negedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge CEB,posedge CK &&& shcheckCKCEBlh === 1'b1,0,notifier);
	$setup(posedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge CK &&& shcheckCKRBlh === 1'b1,0,notifier);
	$width(negedge CK,1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(posedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       DFERPQ1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:11 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Enabled, Positive Edge, D Flip-Flop with Clear; Q Output
// Q  = !RB ? 0 : rising(CK) ? (CEB ? 's' : D) : 'p'
module DFERPQ1 (Q, CEB, CK, D, RB);
	output Q;
	input  CEB;
	input  CK;
	input  D;
	input  RB;
	reg notifier;
	p_mux21 _i0 (_n1, buf_Q, D, CEB);
	not _i1 (_n2,RB);
	p_ffr _i2 (buf_Q, _n1, CK, _n2, notifier);
	buf _iQ(Q, buf_Q);
`ifdef no_tchk
`else
	not _wi0 (_wn1,CEB);
	and _wi1 (shcheckCKDlh,_wn1,RB);
	and _wi3 (shcheckCKRBlh,_wn1,D);
	buf _wi4 (shcheckCKCEBlh,RB);
`endif
	specify		(CK => Q) = (1,1);
	(RB => Q) = (0,0);
`ifdef no_tchk
`else
	$hold(posedge CK &&& shcheckCKCEBlh === 1'b1,negedge CEB,0,notifier);
	$hold(posedge CK &&& shcheckCKCEBlh === 1'b1,posedge CEB,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge CK,posedge RB,0,notifier);
	$setup(negedge CEB,posedge CK &&& shcheckCKCEBlh === 1'b1,0,notifier);
	$setup(negedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge CEB,posedge CK &&& shcheckCKCEBlh === 1'b1,0,notifier);
	$setup(posedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge CK &&& shcheckCKRBlh === 1'b1,0,notifier);
	$width(negedge CK,1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(posedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       DFERPQ2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:11 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Enabled, Positive Edge, D Flip-Flop with Clear; Q Output
// Q  = !RB ? 0 : rising(CK) ? (CEB ? 's' : D) : 'p'
module DFERPQ2 (Q, CEB, CK, D, RB);
	output Q;
	input  CEB;
	input  CK;
	input  D;
	input  RB;
	reg notifier;
	p_mux21 _i0 (_n1, buf_Q, D, CEB);
	not _i1 (_n2,RB);
	p_ffr _i2 (buf_Q, _n1, CK, _n2, notifier);
	buf _iQ(Q, buf_Q);
`ifdef no_tchk
`else
	not _wi0 (_wn1,CEB);
	and _wi1 (shcheckCKDlh,_wn1,RB);
	and _wi3 (shcheckCKRBlh,_wn1,D);
	buf _wi4 (shcheckCKCEBlh,RB);
`endif
	specify		(CK => Q) = (1,1);
	(RB => Q) = (0,0);
`ifdef no_tchk
`else
	$hold(posedge CK &&& shcheckCKCEBlh === 1'b1,negedge CEB,0,notifier);
	$hold(posedge CK &&& shcheckCKCEBlh === 1'b1,posedge CEB,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge CK,posedge RB,0,notifier);
	$setup(negedge CEB,posedge CK &&& shcheckCKCEBlh === 1'b1,0,notifier);
	$setup(negedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge CEB,posedge CK &&& shcheckCKCEBlh === 1'b1,0,notifier);
	$setup(posedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge CK &&& shcheckCKRBlh === 1'b1,0,notifier);
	$width(negedge CK,1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(posedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       DFERSNB1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:08 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Enabled, Negative Edge, D Flip-Flop with Set, Clear; Q, QB Outputs
// Q  = !RB ? 0 : !SB ? 1 : falling(CKB) ? (CEB ? 's' : D) : 'p';QB = !Q & SB
module DFERSNB1 (Q, QB, CEB, CKB, D, RB, SB);
	output Q;
	output QB;
	input  CEB;
	input  CKB;
	input  D;
	input  RB;
	input  SB;
	reg notifier;
	not _i0 (_n1,CKB);
	p_mux21 _i1 (_n2, buf_Q, D, CEB);
	not _i2 (_n3,SB);
	not _i3 (_n4,RB);
	p_ffrs _i4 (buf_Q, _n2, _n1, _n4, _n3, notifier);
	not _i5 (_n6,buf_Q);
	and _i6 (QB,_n6,SB);
	buf _iQ(Q, buf_Q);
`ifdef no_tchk
`else
	not _wi0 (_wn1,CEB);
	and _wi1 (shcheckCKBRBhl,SB,_wn1,D);
	not _wi3 (_wn4,D);
	and _wi4 (shcheckCKBSBhl,RB,_wn1,_wn4);
	and _wi6 (shcheckCKBDhl,SB,_wn1,RB);
	and _wi7 (shcheckCKBCEBhl,RB,SB);
`endif
	specify		(CKB => Q) = (1,1);
	(CKB => QB) = (1,1);
	(RB => Q) = (0,0);
	(RB => QB) = (0,0);
	(SB => Q) = (0,0);
	(SB => QB) = (0,0);
`ifdef no_tchk
`else
	$hold(negedge CKB &&& shcheckCKBCEBhl === 1'b1,negedge CEB,0,notifier);
	$hold(negedge CKB &&& shcheckCKBCEBhl === 1'b1,posedge CEB,0,notifier);
	$hold(negedge CKB &&& shcheckCKBDhl === 1'b1,negedge D,0,notifier);
	$hold(negedge CKB &&& shcheckCKBDhl === 1'b1,posedge D,0,notifier);
	$hold(negedge CKB,posedge RB,0,notifier);
	$hold(negedge CKB,posedge SB,0,notifier);
	$hold(posedge SB,posedge RB,0,notifier);
	$setup(negedge CEB,negedge CKB &&& shcheckCKBCEBhl === 1'b1,0,notifier);
	$setup(negedge D,negedge CKB &&& shcheckCKBDhl === 1'b1,0,notifier);
	$setup(posedge CEB,negedge CKB &&& shcheckCKBCEBhl === 1'b1,0,notifier);
	$setup(posedge D,negedge CKB &&& shcheckCKBDhl === 1'b1,0,notifier);
	$setup(posedge RB,negedge CKB &&& shcheckCKBRBhl === 1'b1,0,notifier);
	$setup(posedge RB,posedge SB,0,notifier);
	$setup(posedge SB,negedge CKB &&& shcheckCKBSBhl === 1'b1,0,notifier);
	$width(negedge CKB,1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(negedge SB,1,0,notifier);
	$width(posedge CKB,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       DFERSNB2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:11 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Enabled, Negative Edge, D Flip-Flop with Set, Clear; Q, QB Outputs
// Q  = !RB ? 0 : !SB ? 1 : falling(CKB) ? (CEB ? 's' : D) : 'p';QB = !Q & SB
module DFERSNB2 (Q, QB, CEB, CKB, D, RB, SB);
	output Q;
	output QB;
	input  CEB;
	input  CKB;
	input  D;
	input  RB;
	input  SB;
	reg notifier;
	not _i0 (_n1,CKB);
	p_mux21 _i1 (_n2, buf_Q, D, CEB);
	not _i2 (_n3,SB);
	not _i3 (_n4,RB);
	p_ffrs _i4 (buf_Q, _n2, _n1, _n4, _n3, notifier);
	not _i5 (_n6,buf_Q);
	and _i6 (QB,_n6,SB);
	buf _iQ(Q, buf_Q);
`ifdef no_tchk
`else
	not _wi0 (_wn1,CEB);
	and _wi1 (shcheckCKBRBhl,SB,_wn1,D);
	not _wi3 (_wn4,D);
	and _wi4 (shcheckCKBSBhl,RB,_wn1,_wn4);
	and _wi6 (shcheckCKBDhl,SB,_wn1,RB);
	and _wi7 (shcheckCKBCEBhl,RB,SB);
`endif
	specify		(CKB => Q) = (1,1);
	(CKB => QB) = (1,1);
	(RB => Q) = (0,0);
	(RB => QB) = (0,0);
	(SB => Q) = (0,0);
	(SB => QB) = (0,0);
`ifdef no_tchk
`else
	$hold(negedge CKB &&& shcheckCKBCEBhl === 1'b1,negedge CEB,0,notifier);
	$hold(negedge CKB &&& shcheckCKBCEBhl === 1'b1,posedge CEB,0,notifier);
	$hold(negedge CKB &&& shcheckCKBDhl === 1'b1,negedge D,0,notifier);
	$hold(negedge CKB &&& shcheckCKBDhl === 1'b1,posedge D,0,notifier);
	$hold(negedge CKB,posedge RB,0,notifier);
	$hold(negedge CKB,posedge SB,0,notifier);
	$hold(posedge SB,posedge RB,0,notifier);
	$setup(negedge CEB,negedge CKB &&& shcheckCKBCEBhl === 1'b1,0,notifier);
	$setup(negedge D,negedge CKB &&& shcheckCKBDhl === 1'b1,0,notifier);
	$setup(posedge CEB,negedge CKB &&& shcheckCKBCEBhl === 1'b1,0,notifier);
	$setup(posedge D,negedge CKB &&& shcheckCKBDhl === 1'b1,0,notifier);
	$setup(posedge RB,negedge CKB &&& shcheckCKBRBhl === 1'b1,0,notifier);
	$setup(posedge RB,posedge SB,0,notifier);
	$setup(posedge SB,negedge CKB &&& shcheckCKBSBhl === 1'b1,0,notifier);
	$width(negedge CKB,1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(negedge SB,1,0,notifier);
	$width(posedge CKB,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       DFERSPB1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:07 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Enabled, Positive Edge, D Flip-Flop with Set, Clear; Q, QB Outputs
// Q  = !RB ? 0 : !SB ? 1 : rising(CK) ? (CEB ? 's' : D) : 'p';QB = !Q & SB
module DFERSPB1 (Q, QB, CEB, CK, D, RB, SB);
	output Q;
	output QB;
	input  CEB;
	input  CK;
	input  D;
	input  RB;
	input  SB;
	reg notifier;
	p_mux21 _i0 (_n1, buf_Q, D, CEB);
	not _i1 (_n2,SB);
	not _i2 (_n3,RB);
	p_ffrs _i3 (buf_Q, _n1, CK, _n3, _n2, notifier);
	not _i4 (_n5,buf_Q);
	and _i5 (QB,_n5,SB);
	buf _iQ(Q, buf_Q);
`ifdef no_tchk
`else
	not _wi0 (_wn1,CEB);
	not _wi1 (_wn2,D);
	and _wi2 (shcheckCKSBlh,RB,_wn1,_wn2);
	and _wi4 (shcheckCKDlh,SB,_wn1,RB);
	and _wi6 (shcheckCKRBlh,SB,_wn1,D);
	and _wi7 (shcheckCKCEBlh,RB,SB);
`endif
	specify		(CK => Q) = (1,1);
	(CK => QB) = (1,1);
	(RB => Q) = (0,0);
	(RB => QB) = (0,0);
	(SB => Q) = (0,0);
	(SB => QB) = (0,0);
`ifdef no_tchk
`else
	$hold(posedge CK &&& shcheckCKCEBlh === 1'b1,negedge CEB,0,notifier);
	$hold(posedge CK &&& shcheckCKCEBlh === 1'b1,posedge CEB,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge CK,posedge RB,0,notifier);
	$hold(posedge CK,posedge SB,0,notifier);
	$hold(posedge SB,posedge RB,0,notifier);
	$setup(negedge CEB,posedge CK &&& shcheckCKCEBlh === 1'b1,0,notifier);
	$setup(negedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge CEB,posedge CK &&& shcheckCKCEBlh === 1'b1,0,notifier);
	$setup(posedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge CK &&& shcheckCKRBlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge SB,0,notifier);
	$setup(posedge SB,posedge CK &&& shcheckCKSBlh === 1'b1,0,notifier);
	$width(negedge CK,1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(negedge SB,1,0,notifier);
	$width(posedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       DFERSPB2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:05 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Enabled, Positive Edge, D Flip-Flop with Set, Clear; Q, QB Outputs
// Q  = !RB ? 0 : !SB ? 1 : rising(CK) ? (CEB ? 's' : D) : 'p';QB = !Q & SB
module DFERSPB2 (Q, QB, CEB, CK, D, RB, SB);
	output Q;
	output QB;
	input  CEB;
	input  CK;
	input  D;
	input  RB;
	input  SB;
	reg notifier;
	p_mux21 _i0 (_n1, buf_Q, D, CEB);
	not _i1 (_n2,SB);
	not _i2 (_n3,RB);
	p_ffrs _i3 (buf_Q, _n1, CK, _n3, _n2, notifier);
	not _i4 (_n5,buf_Q);
	and _i5 (QB,_n5,SB);
	buf _iQ(Q, buf_Q);
`ifdef no_tchk
`else
	not _wi0 (_wn1,CEB);
	not _wi1 (_wn2,D);
	and _wi2 (shcheckCKSBlh,RB,_wn1,_wn2);
	and _wi4 (shcheckCKDlh,SB,_wn1,RB);
	and _wi6 (shcheckCKRBlh,SB,_wn1,D);
	and _wi7 (shcheckCKCEBlh,RB,SB);
`endif
	specify		(CK => Q) = (1,1);
	(CK => QB) = (1,1);
	(RB => Q) = (0,0);
	(RB => QB) = (0,0);
	(SB => Q) = (0,0);
	(SB => QB) = (0,0);
`ifdef no_tchk
`else
	$hold(posedge CK &&& shcheckCKCEBlh === 1'b1,negedge CEB,0,notifier);
	$hold(posedge CK &&& shcheckCKCEBlh === 1'b1,posedge CEB,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge CK,posedge RB,0,notifier);
	$hold(posedge CK,posedge SB,0,notifier);
	$hold(posedge SB,posedge RB,0,notifier);
	$setup(negedge CEB,posedge CK &&& shcheckCKCEBlh === 1'b1,0,notifier);
	$setup(negedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge CEB,posedge CK &&& shcheckCKCEBlh === 1'b1,0,notifier);
	$setup(posedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge CK &&& shcheckCKRBlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge SB,0,notifier);
	$setup(posedge SB,posedge CK &&& shcheckCKSBlh === 1'b1,0,notifier);
	$width(negedge CK,1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(negedge SB,1,0,notifier);
	$width(posedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       DFERSPQ1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:01 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Enabled, Positive Edge, D Flip-Flop with Set, Clear; Q Output
// Q  = !RB ? 0 : !SB ? 1 : rising(CK) ? (CEB ? 's' : D) : 'p'
module DFERSPQ1 (Q, CEB, CK, D, RB, SB);
	output Q;
	input  CEB;
	input  CK;
	input  D;
	input  RB;
	input  SB;
	reg notifier;
	p_mux21 _i0 (_n1, buf_Q, D, CEB);
	not _i1 (_n2,SB);
	not _i2 (_n3,RB);
	p_ffrs _i3 (buf_Q, _n1, CK, _n3, _n2, notifier);
	buf _iQ(Q, buf_Q);
`ifdef no_tchk
`else
	not _wi0 (_wn1,CEB);
	not _wi1 (_wn2,D);
	and _wi2 (shcheckCKSBlh,RB,_wn1,_wn2);
	and _wi4 (shcheckCKDlh,SB,_wn1,RB);
	and _wi6 (shcheckCKRBlh,SB,_wn1,D);
	and _wi7 (shcheckCKCEBlh,RB,SB);
`endif
	specify		(CK => Q) = (1,1);
	(RB => Q) = (0,0);
	(SB => Q) = (0,0);
`ifdef no_tchk
`else
	$hold(posedge CK &&& shcheckCKCEBlh === 1'b1,negedge CEB,0,notifier);
	$hold(posedge CK &&& shcheckCKCEBlh === 1'b1,posedge CEB,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge CK,posedge RB,0,notifier);
	$hold(posedge CK,posedge SB,0,notifier);
	$hold(posedge SB,posedge RB,0,notifier);
	$setup(negedge CEB,posedge CK &&& shcheckCKCEBlh === 1'b1,0,notifier);
	$setup(negedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge CEB,posedge CK &&& shcheckCKCEBlh === 1'b1,0,notifier);
	$setup(posedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge CK &&& shcheckCKRBlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge SB,0,notifier);
	$setup(posedge SB,posedge CK &&& shcheckCKSBlh === 1'b1,0,notifier);
	$width(negedge CK,1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(negedge SB,1,0,notifier);
	$width(posedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       DFERSPQ2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:01 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Enabled, Positive Edge, D Flip-Flop with Set, Clear; Q Output
// Q  = !RB ? 0 : !SB ? 1 : rising(CK) ? (CEB ? 's' : D) : 'p'
module DFERSPQ2 (Q, CEB, CK, D, RB, SB);
	output Q;
	input  CEB;
	input  CK;
	input  D;
	input  RB;
	input  SB;
	reg notifier;
	p_mux21 _i0 (_n1, buf_Q, D, CEB);
	not _i1 (_n2,SB);
	not _i2 (_n3,RB);
	p_ffrs _i3 (buf_Q, _n1, CK, _n3, _n2, notifier);
	buf _iQ(Q, buf_Q);
`ifdef no_tchk
`else
	not _wi0 (_wn1,CEB);
	not _wi1 (_wn2,D);
	and _wi2 (shcheckCKSBlh,RB,_wn1,_wn2);
	and _wi4 (shcheckCKDlh,SB,_wn1,RB);
	and _wi6 (shcheckCKRBlh,SB,_wn1,D);
	and _wi7 (shcheckCKCEBlh,RB,SB);
`endif
	specify		(CK => Q) = (1,1);
	(RB => Q) = (0,0);
	(SB => Q) = (0,0);
`ifdef no_tchk
`else
	$hold(posedge CK &&& shcheckCKCEBlh === 1'b1,negedge CEB,0,notifier);
	$hold(posedge CK &&& shcheckCKCEBlh === 1'b1,posedge CEB,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge CK,posedge RB,0,notifier);
	$hold(posedge CK,posedge SB,0,notifier);
	$hold(posedge SB,posedge RB,0,notifier);
	$setup(negedge CEB,posedge CK &&& shcheckCKCEBlh === 1'b1,0,notifier);
	$setup(negedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge CEB,posedge CK &&& shcheckCKCEBlh === 1'b1,0,notifier);
	$setup(posedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge CK &&& shcheckCKRBlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge SB,0,notifier);
	$setup(posedge SB,posedge CK &&& shcheckCKSBlh === 1'b1,0,notifier);
	$width(negedge CK,1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(negedge SB,1,0,notifier);
	$width(posedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       DFFPB1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:58 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Positive Edge, D Flip-Flop; Q, QB Outputs
// Q  = rising(CK) ?  D : 'p';QB = !Q
module DFFPB1 (Q, QB, CK, D);
	output Q;
	output QB;
	input  CK;
	input  D;
	reg notifier;
	p_ff _i0 (Q, D, CK, notifier);
	not _i1 (QB,Q);
	specify		(CK => Q) = (1,1);
	(CK => QB) = (1,1);
`ifdef no_tchk
`else
	$hold(posedge CK,negedge D,0,notifier);
	$hold(posedge CK,posedge D,0,notifier);
	$setup(negedge D,posedge CK,0,notifier);
	$setup(posedge D,posedge CK,0,notifier);
	$width(negedge CK,1,0,notifier);
	$width(posedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       DFFPB2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:58 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Positive Edge, D Flip-Flop; Q, QB Outputs
// Q  = rising(CK) ?  D : 'p';QB = !Q
module DFFPB2 (Q, QB, CK, D);
	output Q;
	output QB;
	input  CK;
	input  D;
	reg notifier;
	p_ff _i0 (Q, D, CK, notifier);
	not _i1 (QB,Q);
	specify		(CK => Q) = (1,1);
	(CK => QB) = (1,1);
`ifdef no_tchk
`else
	$hold(posedge CK,negedge D,0,notifier);
	$hold(posedge CK,posedge D,0,notifier);
	$setup(negedge D,posedge CK,0,notifier);
	$setup(posedge D,posedge CK,0,notifier);
	$width(negedge CK,1,0,notifier);
	$width(posedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       DFFPQ1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:05 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Positive Edge, D Flip-Flop; Q Output
// Q  = rising(CK) ?  D : 'p'
module DFFPQ1 (Q, CK, D);
	output Q;
	input  CK;
	input  D;
	reg notifier;
	p_ff _i0 (Q, D, CK, notifier);
	specify		(CK => Q) = (1,1);
`ifdef no_tchk
`else
	$hold(posedge CK,negedge D,0,notifier);
	$hold(posedge CK,posedge D,0,notifier);
	$setup(negedge D,posedge CK,0,notifier);
	$setup(posedge D,posedge CK,0,notifier);
	$width(negedge CK,1,0,notifier);
	$width(posedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       DFFPQ2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:01 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Positive Edge, D Flip-Flop; Q Output
// Q  = rising(CK) ?  D : 'p'
module DFFPQ2 (Q, CK, D);
	output Q;
	input  CK;
	input  D;
	reg notifier;
	p_ff _i0 (Q, D, CK, notifier);
	specify		(CK => Q) = (1,1);
`ifdef no_tchk
`else
	$hold(posedge CK,negedge D,0,notifier);
	$hold(posedge CK,posedge D,0,notifier);
	$setup(negedge D,posedge CK,0,notifier);
	$setup(posedge D,posedge CK,0,notifier);
	$width(negedge CK,1,0,notifier);
	$width(posedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       DFFPQ4.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:01 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Positive Edge, D Flip-Flop; Q Output
// Q  = rising(CK) ?  D : 'p'
module DFFPQ4 (Q, CK, D);
	output Q;
	input  CK;
	input  D;
	reg notifier;
	p_ff _i0 (Q, D, CK, notifier);
	specify		(CK => Q) = (1,1);
`ifdef no_tchk
`else
	$hold(posedge CK,negedge D,0,notifier);
	$hold(posedge CK,posedge D,0,notifier);
	$setup(negedge D,posedge CK,0,notifier);
	$setup(posedge D,posedge CK,0,notifier);
	$width(negedge CK,1,0,notifier);
	$width(posedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       DFFRNQ1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:51:07 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Negative Edge, D Flip-Flop with Clear; Q Output
// Q  = !RB ? 0 : falling(CKB) ?  D : 'p'
module DFFRNQ1 (Q, CKB, D, RB);
	output Q;
	input  CKB;
	input  D;
	input  RB;
	reg notifier;
	not _i0 (_n1,CKB);
	not _i1 (_n2,RB);
	p_ffr _i2 (Q, D, _n1, _n2, notifier);
`ifdef no_tchk
`else
	buf _wi0 (shcheckCKBRBhl,D);
	buf _wi1 (shcheckCKBDhl,RB);
`endif
	specify		(CKB => Q) = (1,1);
	(RB => Q) = (0,0);
`ifdef no_tchk
`else
	$hold(negedge CKB &&& shcheckCKBDhl === 1'b1,negedge D,0,notifier);
	$hold(negedge CKB &&& shcheckCKBDhl === 1'b1,posedge D,0,notifier);
	$hold(negedge CKB,posedge RB,0,notifier);
	$setup(negedge D,negedge CKB &&& shcheckCKBDhl === 1'b1,0,notifier);
	$setup(posedge D,negedge CKB &&& shcheckCKBDhl === 1'b1,0,notifier);
	$setup(posedge RB,negedge CKB &&& shcheckCKBRBhl === 1'b1,0,notifier);
	$width(negedge CKB,1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(posedge CKB,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       DFFRNQ2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:58 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Negative Edge, D Flip-Flop with Clear; Q Output
// Q  = !RB ? 0 : falling(CKB) ?  D : 'p'
module DFFRNQ2 (Q, CKB, D, RB);
	output Q;
	input  CKB;
	input  D;
	input  RB;
	reg notifier;
	not _i0 (_n1,CKB);
	not _i1 (_n2,RB);
	p_ffr _i2 (Q, D, _n1, _n2, notifier);
`ifdef no_tchk
`else
	buf _wi0 (shcheckCKBRBhl,D);
	buf _wi1 (shcheckCKBDhl,RB);
`endif
	specify		(CKB => Q) = (1,1);
	(RB => Q) = (0,0);
`ifdef no_tchk
`else
	$hold(negedge CKB &&& shcheckCKBDhl === 1'b1,negedge D,0,notifier);
	$hold(negedge CKB &&& shcheckCKBDhl === 1'b1,posedge D,0,notifier);
	$hold(negedge CKB,posedge RB,0,notifier);
	$setup(negedge D,negedge CKB &&& shcheckCKBDhl === 1'b1,0,notifier);
	$setup(posedge D,negedge CKB &&& shcheckCKBDhl === 1'b1,0,notifier);
	$setup(posedge RB,negedge CKB &&& shcheckCKBRBhl === 1'b1,0,notifier);
	$width(negedge CKB,1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(posedge CKB,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       DFFRPB1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:57 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Positive Edge, D Flip-Flop with Clear; Q, QB Outputs
// Q  = !RB ? 0 : rising(CK) ?  D : 'p';QB = !Q
module DFFRPB1 (Q, QB, CK, D, RB);
	output Q;
	output QB;
	input  CK;
	input  D;
	input  RB;
	reg notifier;
	not _i0 (_n1,RB);
	p_ffr _i1 (Q, D, CK, _n1, notifier);
	not _i2 (QB,Q);
`ifdef no_tchk
`else
	buf _wi0 (shcheckCKDlh,RB);
	buf _wi1 (shcheckCKRBlh,D);
`endif
	specify		(CK => Q) = (1,1);
	(CK => QB) = (1,1);
	(RB => Q) = (0,0);
	(RB => QB) = (0,0);
`ifdef no_tchk
`else
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge CK,posedge RB,0,notifier);
	$setup(negedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge CK &&& shcheckCKRBlh === 1'b1,0,notifier);
	$width(negedge CK,1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(posedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       DFFRPB2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:57 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Positive Edge, D Flip-Flop with Clear; Q, QB Outputs
// Q  = !RB ? 0 : rising(CK) ?  D : 'p';QB = !Q
module DFFRPB2 (Q, QB, CK, D, RB);
	output Q;
	output QB;
	input  CK;
	input  D;
	input  RB;
	reg notifier;
	not _i0 (_n1,RB);
	p_ffr _i1 (Q, D, CK, _n1, notifier);
	not _i2 (QB,Q);
`ifdef no_tchk
`else
	buf _wi0 (shcheckCKDlh,RB);
	buf _wi1 (shcheckCKRBlh,D);
`endif
	specify		(CK => Q) = (1,1);
	(CK => QB) = (1,1);
	(RB => Q) = (0,0);
	(RB => QB) = (0,0);
`ifdef no_tchk
`else
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge CK,posedge RB,0,notifier);
	$setup(negedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge CK &&& shcheckCKRBlh === 1'b1,0,notifier);
	$width(negedge CK,1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(posedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       DFFRPQ1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:49 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Positive Edge, D Flip-Flop with Clear; Q Output
// Q  = !RB ? 0 : rising(CK) ?  D : 'p'
module DFFRPQ1 (Q, CK, D, RB);
	output Q;
	input  CK;
	input  D;
	input  RB;
	reg notifier;
	not _i0 (_n1,RB);
	p_ffr _i1 (Q, D, CK, _n1, notifier);
`ifdef no_tchk
`else
	buf _wi0 (shcheckCKDlh,RB);
	buf _wi1 (shcheckCKRBlh,D);
`endif
	specify		(CK => Q) = (1,1);
	(RB => Q) = (0,0);
`ifdef no_tchk
`else
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge CK,posedge RB,0,notifier);
	$setup(negedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge CK &&& shcheckCKRBlh === 1'b1,0,notifier);
	$width(negedge CK,1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(posedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       DFFRPQ2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:49 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Positive Edge, D Flip-Flop with Clear; Q Output
// Q  = !RB ? 0 : rising(CK) ?  D : 'p'
module DFFRPQ2 (Q, CK, D, RB);
	output Q;
	input  CK;
	input  D;
	input  RB;
	reg notifier;
	not _i0 (_n1,RB);
	p_ffr _i1 (Q, D, CK, _n1, notifier);
`ifdef no_tchk
`else
	buf _wi0 (shcheckCKDlh,RB);
	buf _wi1 (shcheckCKRBlh,D);
`endif
	specify		(CK => Q) = (1,1);
	(RB => Q) = (0,0);
`ifdef no_tchk
`else
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge CK,posedge RB,0,notifier);
	$setup(negedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge CK &&& shcheckCKRBlh === 1'b1,0,notifier);
	$width(negedge CK,1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(posedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       DFFRSNB1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:57 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Negative Edge, D Flip-Flop with Set, Clear; Q, QB Outputs
// Q  = !RB ? 0 : !SB ? 1 : falling(CKB) ?  D : 'p';QB = !Q & SB
module DFFRSNB1 (Q, QB, CKB, D, RB, SB);
	output Q;
	output QB;
	input  CKB;
	input  D;
	input  RB;
	input  SB;
	reg notifier;
	not _i0 (_n1,CKB);
	not _i1 (_n2,SB);
	not _i2 (_n3,RB);
	p_ffrs _i3 (Q, D, _n1, _n3, _n2, notifier);
	not _i4 (_n5,Q);
	and _i5 (QB,_n5,SB);
`ifdef no_tchk
`else
	and _wi0 (shcheckCKBRBhl,D,SB);
	not _wi1 (_wn2,D);
	and _wi2 (shcheckCKBSBhl,_wn2,RB);
	and _wi3 (shcheckCKBDhl,RB,SB);
`endif
	specify		(CKB => Q) = (1,1);
	(CKB => QB) = (1,1);
	(RB => Q) = (0,0);
	(RB => QB) = (0,0);
	(SB => Q) = (0,0);
	(SB => QB) = (0,0);
`ifdef no_tchk
`else
	$hold(negedge CKB &&& shcheckCKBDhl === 1'b1,negedge D,0,notifier);
	$hold(negedge CKB &&& shcheckCKBDhl === 1'b1,posedge D,0,notifier);
	$hold(negedge CKB,posedge RB,0,notifier);
	$hold(negedge CKB,posedge SB,0,notifier);
	$hold(posedge SB,posedge RB,0,notifier);
	$setup(negedge D,negedge CKB &&& shcheckCKBDhl === 1'b1,0,notifier);
	$setup(posedge D,negedge CKB &&& shcheckCKBDhl === 1'b1,0,notifier);
	$setup(posedge RB,negedge CKB &&& shcheckCKBRBhl === 1'b1,0,notifier);
	$setup(posedge RB,posedge SB,0,notifier);
	$setup(posedge SB,negedge CKB &&& shcheckCKBSBhl === 1'b1,0,notifier);
	$width(negedge CKB,1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(negedge SB,1,0,notifier);
	$width(posedge CKB,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       DFFRSNB2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:48 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Negative Edge, D Flip-Flop with Set, Clear; Q, QB Outputs
// Q  = !RB ? 0 : !SB ? 1 : falling(CKB) ?  D : 'p';QB = !Q & SB
module DFFRSNB2 (Q, QB, CKB, D, RB, SB);
	output Q;
	output QB;
	input  CKB;
	input  D;
	input  RB;
	input  SB;
	reg notifier;
	not _i0 (_n1,CKB);
	not _i1 (_n2,SB);
	not _i2 (_n3,RB);
	p_ffrs _i3 (Q, D, _n1, _n3, _n2, notifier);
	not _i4 (_n5,Q);
	and _i5 (QB,_n5,SB);
`ifdef no_tchk
`else
	and _wi0 (shcheckCKBRBhl,D,SB);
	not _wi1 (_wn2,D);
	and _wi2 (shcheckCKBSBhl,_wn2,RB);
	and _wi3 (shcheckCKBDhl,RB,SB);
`endif
	specify		(CKB => Q) = (1,1);
	(CKB => QB) = (1,1);
	(RB => Q) = (0,0);
	(RB => QB) = (0,0);
	(SB => Q) = (0,0);
	(SB => QB) = (0,0);
`ifdef no_tchk
`else
	$hold(negedge CKB &&& shcheckCKBDhl === 1'b1,negedge D,0,notifier);
	$hold(negedge CKB &&& shcheckCKBDhl === 1'b1,posedge D,0,notifier);
	$hold(negedge CKB,posedge RB,0,notifier);
	$hold(negedge CKB,posedge SB,0,notifier);
	$hold(posedge SB,posedge RB,0,notifier);
	$setup(negedge D,negedge CKB &&& shcheckCKBDhl === 1'b1,0,notifier);
	$setup(posedge D,negedge CKB &&& shcheckCKBDhl === 1'b1,0,notifier);
	$setup(posedge RB,negedge CKB &&& shcheckCKBRBhl === 1'b1,0,notifier);
	$setup(posedge RB,posedge SB,0,notifier);
	$setup(posedge SB,negedge CKB &&& shcheckCKBSBhl === 1'b1,0,notifier);
	$width(negedge CKB,1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(negedge SB,1,0,notifier);
	$width(posedge CKB,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       DFFRSPB1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:49 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Positive Edge, D Flip-Flop with Set, Clear; Q, QB Outputs
// Q  = !RB ? 0 : !SB ? 1 : rising(CK) ?  D : 'p';QB = !Q & SB
module DFFRSPB1 (Q, QB, CK, D, RB, SB);
	output Q;
	output QB;
	input  CK;
	input  D;
	input  RB;
	input  SB;
	reg notifier;
	not _i0 (_n1,SB);
	not _i1 (_n2,RB);
	p_ffrs _i2 (Q, D, CK, _n2, _n1, notifier);
	not _i3 (_n4,Q);
	and _i4 (QB,_n4,SB);
`ifdef no_tchk
`else
	not _wi0 (_wn1,D);
	and _wi1 (shcheckCKSBlh,_wn1,RB);
	and _wi2 (shcheckCKDlh,RB,SB);
	and _wi3 (shcheckCKRBlh,D,SB);
`endif
	specify		(CK => Q) = (1,1);
	(CK => QB) = (1,1);
	(RB => Q) = (0,0);
	(RB => QB) = (0,0);
	(SB => Q) = (0,0);
	(SB => QB) = (0,0);
`ifdef no_tchk
`else
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge CK,posedge RB,0,notifier);
	$hold(posedge CK,posedge SB,0,notifier);
	$hold(posedge SB,posedge RB,0,notifier);
	$setup(negedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge CK &&& shcheckCKRBlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge SB,0,notifier);
	$setup(posedge SB,posedge CK &&& shcheckCKSBlh === 1'b1,0,notifier);
	$width(negedge CK,1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(negedge SB,1,0,notifier);
	$width(posedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       DFFRSPB2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:50 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Positive Edge, D Flip-Flop with Set, Clear; Q, QB Outputs
// Q  = !RB ? 0 : !SB ? 1 : rising(CK) ?  D : 'p';QB = !Q & SB
module DFFRSPB2 (Q, QB, CK, D, RB, SB);
	output Q;
	output QB;
	input  CK;
	input  D;
	input  RB;
	input  SB;
	reg notifier;
	not _i0 (_n1,SB);
	not _i1 (_n2,RB);
	p_ffrs _i2 (Q, D, CK, _n2, _n1, notifier);
	not _i3 (_n4,Q);
	and _i4 (QB,_n4,SB);
`ifdef no_tchk
`else
	not _wi0 (_wn1,D);
	and _wi1 (shcheckCKSBlh,_wn1,RB);
	and _wi2 (shcheckCKDlh,RB,SB);
	and _wi3 (shcheckCKRBlh,D,SB);
`endif
	specify		(CK => Q) = (1,1);
	(CK => QB) = (1,1);
	(RB => Q) = (0,0);
	(RB => QB) = (0,0);
	(SB => Q) = (0,0);
	(SB => QB) = (0,0);
`ifdef no_tchk
`else
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge CK,posedge RB,0,notifier);
	$hold(posedge CK,posedge SB,0,notifier);
	$hold(posedge SB,posedge RB,0,notifier);
	$setup(negedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge CK &&& shcheckCKRBlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge SB,0,notifier);
	$setup(posedge SB,posedge CK &&& shcheckCKSBlh === 1'b1,0,notifier);
	$width(negedge CK,1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(negedge SB,1,0,notifier);
	$width(posedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       DFFRSPQ1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:49 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Positive Edge, D Flip-Flop with Set, Clear; Q Output
// Q  = !RB ? 0 : !SB ? 1 : rising(CK) ?  D : 'p'
module DFFRSPQ1 (Q, CK, D, RB, SB);
	output Q;
	input  CK;
	input  D;
	input  RB;
	input  SB;
	reg notifier;
	not _i0 (_n1,SB);
	not _i1 (_n2,RB);
	p_ffrs _i2 (Q, D, CK, _n2, _n1, notifier);
`ifdef no_tchk
`else
	not _wi0 (_wn1,D);
	and _wi1 (shcheckCKSBlh,_wn1,RB);
	and _wi2 (shcheckCKDlh,RB,SB);
	and _wi3 (shcheckCKRBlh,D,SB);
`endif
	specify		(CK => Q) = (1,1);
	(RB => Q) = (0,0);
	(SB => Q) = (0,0);
`ifdef no_tchk
`else
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge CK,posedge RB,0,notifier);
	$hold(posedge CK,posedge SB,0,notifier);
	$hold(posedge SB,posedge RB,0,notifier);
	$setup(negedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge CK &&& shcheckCKRBlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge SB,0,notifier);
	$setup(posedge SB,posedge CK &&& shcheckCKSBlh === 1'b1,0,notifier);
	$width(negedge CK,1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(negedge SB,1,0,notifier);
	$width(posedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       DFFRSPQ2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:44 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Positive Edge, D Flip-Flop with Set, Clear; Q Output
// Q  = !RB ? 0 : !SB ? 1 : rising(CK) ?  D : 'p'
module DFFRSPQ2 (Q, CK, D, RB, SB);
	output Q;
	input  CK;
	input  D;
	input  RB;
	input  SB;
	reg notifier;
	not _i0 (_n1,SB);
	not _i1 (_n2,RB);
	p_ffrs _i2 (Q, D, CK, _n2, _n1, notifier);
`ifdef no_tchk
`else
	not _wi0 (_wn1,D);
	and _wi1 (shcheckCKSBlh,_wn1,RB);
	and _wi2 (shcheckCKDlh,RB,SB);
	and _wi3 (shcheckCKRBlh,D,SB);
`endif
	specify		(CK => Q) = (1,1);
	(RB => Q) = (0,0);
	(SB => Q) = (0,0);
`ifdef no_tchk
`else
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge CK,posedge RB,0,notifier);
	$hold(posedge CK,posedge SB,0,notifier);
	$hold(posedge SB,posedge RB,0,notifier);
	$setup(negedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge CK &&& shcheckCKRBlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge SB,0,notifier);
	$setup(posedge SB,posedge CK &&& shcheckCKSBlh === 1'b1,0,notifier);
	$width(negedge CK,1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(negedge SB,1,0,notifier);
	$width(posedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       DLY1DA.v 
// Library Name:    pez_d18_sc_necned3_c7 
// Library Release: 1.2 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/29/99 17:32:43 
//  
// verigen patch-level 1.131-x 05/11/99 19:44:59
`celldefine
// Delay Buffers
// Z = A
module DLY1DA (Z, A);
	output Z;
	input  A;
	buf _i0 (Z,A);
	specify		(A => Z) = (1,1);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       DLY2DA.v 
// Library Name:    pez_d18_sc_necned3_c7 
// Library Release: 1.2 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/29/99 17:32:42 
//  
// verigen patch-level 1.131-x 05/11/99 19:44:59
`celldefine
// Delay Buffers
// Z = A
module DLY2DA (Z, A);
	output Z;
	input  A;
	buf _i0 (Z,A);
	specify		(A => Z) = (1,1);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       DLY3DA.v 
// Library Name:    pez_d18_sc_necned3_c7 
// Library Release: 1.2 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/29/99 17:32:41 
//  
// verigen patch-level 1.131-x 05/11/99 19:44:59
`celldefine
// Delay Buffers
// Z = A
module DLY3DA (Z, A);
	output Z;
	input  A;
	buf _i0 (Z,A);
	specify		(A => Z) = (1,1);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       DLY4DA.v 
// Library Name:    pez_d18_sc_necned3_c7 
// Library Release: 1.2 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/29/99 17:32:43 
//  
// verigen patch-level 1.131-x 05/11/99 19:44:59
`celldefine
// Delay Buffers
// Z = A
module DLY4DA (Z, A);
	output Z;
	input  A;
	buf _i0 (Z,A);
	specify		(A => Z) = (1,1);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       EXNOR2D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:49 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-Input XNOR Gate
// Z = ! ( A1 ^ A2  )
module EXNOR2D1 (Z, A1, A2);
	output Z;
	input  A1;
	input  A2;
	xnor _i0 (Z,A1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       EXNOR2D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:43 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-Input XNOR Gate
// Z = ! ( A1 ^ A2  )
module EXNOR2D2 (Z, A1, A2);
	output Z;
	input  A1;
	input  A2;
	xnor _i0 (Z,A1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       EXNOR2D4.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:39 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-Input XNOR Gate
// Z = ! ( A1 ^ A2  )
module EXNOR2D4 (Z, A1, A2);
	output Z;
	input  A1;
	input  A2;
	xnor _i0 (Z,A1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       EXOR2D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:47 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-Input XOR Gate
// Z = ( A1 ^ A2  )
module EXOR2D1 (Z, A1, A2);
	output Z;
	input  A1;
	input  A2;
	xor _i0 (Z,A1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       EXOR2D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:46 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-Input XOR Gate
// Z = ( A1 ^ A2  )
module EXOR2D2 (Z, A1, A2);
	output Z;
	input  A1;
	input  A2;
	xor _i0 (Z,A1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       EXOR2D4.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:46 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-Input XOR Gate
// Z = ( A1 ^ A2  )
module EXOR2D4 (Z, A1, A2);
	output Z;
	input  A1;
	input  A2;
	xor _i0 (Z,A1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       EXOR3D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:40 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 3-Input XOR Gate
// Z = ( ( A1 ^ A2 )^ A3  )
module EXOR3D1 (Z, A1, A2, A3);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	xor _i0 (Z,A3,A1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       EXOR3D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:40 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 3-Input XOR Gate
// Z = ( (A1 ^ A2) ^ A3 )
module EXOR3D2 (Z, A1, A2, A3);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	xor _i0 (Z,A3,A1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       EXOR3D4.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:33 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 3-Input XOR Gate
// Z = ( (A1 ^ A2) ^ A3  )
module EXOR3D4 (Z, A1, A2, A3);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	xor _i0 (Z,A3,A1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
module FILL1 ();
endmodule
module FILL2 ();
endmodule
module FILL4 ();
endmodule
module FILL8 ();
endmodule
module FILL16 ();
endmodule
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       INVBD16.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:34 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// Inverting Buffers
// Z = ! A
module INVBD16 (Z, A);
	output Z;
	input  A;
	not _i0 (Z,A);
	specify		(A => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       INVBD32.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:43 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// Inverting Buffers
// Z = ! A
module INVBD32 (Z, A);
	output Z;
	input  A;
	not _i0 (Z,A);
	specify		(A => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       INVBD4.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:33 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// Inverting Buffers
// Z = ! A
module INVBD4 (Z, A);
	output Z;
	input  A;
	not _i0 (Z,A);
	specify		(A => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       INVBD8.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:37 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// Inverting Buffers
// Z = ! A
module INVBD8 (Z, A);
	output Z;
	input  A;
	not _i0 (Z,A);
	specify		(A => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       INVD0.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:39 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// Inverting Buffers
// Z = ! A
module INVD0 (Z, A);
	output Z;
	input  A;
	not _i0 (Z,A);
	specify		(A => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       INVD1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:37 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// Inverting Buffers
// Z = ! A
module INVD1 (Z, A);
	output Z;
	input  A;
	not _i0 (Z,A);
	specify		(A => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       INVD2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:33 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// Inverting Buffers
// Z = ! A
module INVD2 (Z, A);
	output Z;
	input  A;
	not _i0 (Z,A);
	specify		(A => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       INVD4.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:30 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// Inverting Buffers
// Z = ! A
module INVD4 (Z, A);
	output Z;
	input  A;
	not _i0 (Z,A);
	specify		(A => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       INVD7.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:30 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// Inverting Buffers
// Z = ! A
module INVD7 (Z, A);
	output Z;
	input  A;
	not _i0 (Z,A);
	specify		(A => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       INVDA.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:29 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// Inverting Buffers
// Z = ! A
module INVDA (Z, A);
	output Z;
	input  A;
	not _i0 (Z,A);
	specify		(A => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       INVDA.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:29 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// Inverting Buffers
// Z = ! A
module ARTX_INVDA (Z, A);
	output Z;
	input  A;
	not _i0 (Z,A);
	specify		(A => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       INVTD1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:29 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 3-State Inverting Buffers with Active-Low Enable
// Z = ENB ? 'z' : !A
module INVTD1 (Z, A, ENB);
	output Z;
	input  A;
	input  ENB;
	notif0 _i0 (Z, A, ENB);
	specify		(A => Z) = (0,0);
	(ENB => Z) = (0,0,0,0,0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       INVTD2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:26 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 3-State Inverting Buffers with Active-Low Enable
// Z = ENB ? 'z' : !A
module INVTD2 (Z, A, ENB);
	output Z;
	input  A;
	input  ENB;
	notif0 _i0 (Z, A, ENB);
	specify		(A => Z) = (0,0);
	(ENB => Z) = (0,0,0,0,0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       INVTD4.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:25 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 3-State Inverting Buffers with Active-Low Enable
// Z = ENB ? 'z' : !A
module INVTD4 (Z, A, ENB);
	output Z;
	input  A;
	input  ENB;
	notif0 _i0 (Z, A, ENB);
	specify		(A => Z) = (0,0);
	(ENB => Z) = (0,0,0,0,0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       INVTD7.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:22 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 3-State Inverting Buffers with Active-Low Enable
// Z = ENB ? 'z' : !A
module INVTD7 (Z, A, ENB);
	output Z;
	input  A;
	input  ENB;
	notif0 _i0 (Z, A, ENB);
	specify		(A => Z) = (0,0);
	(ENB => Z) = (0,0,0,0,0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       INVTDA.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:29 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 3-State Inverting Buffers with Active-Low Enable
// Z = ENB ? 'z' : !A
module INVTDA (Z, A, ENB);
	output Z;
	input  A;
	input  ENB;
	notif0 _i0 (Z, A, ENB);
	specify		(A => Z) = (0,0);
	(ENB => Z) = (0,0,0,0,0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       KEEPER.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/18/99 10:15:42 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
`celldefine
// KEEPER(bushold) Cell
// Z = DUMMY;DUMMY = Z
module KEEPER (Z);
	inout  Z;
	nmos (Z, IINT, 1'b1);
	not (X, Z);
	not (pull1,pull0) #(0.0) (IINT, X);
	specify		endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       LATNB1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:20 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Active-High Enable, Latch; Q, QB Outputs
// Q  = G ?  D : 'p';QB = !Q
module LATNB1 (Q, QB, D, G);
	output Q;
	output QB;
	input  D;
	input  G;
	reg notifier;
	p_latch _i0 (Q, D, G, notifier);
	not _i1 (QB,Q);
	specify		(D => Q) = (0,0);
	(D => QB) = (0,0);
	(G => Q) = (1,1);
	(G => QB) = (1,1);
`ifdef no_tchk
`else
	$hold(negedge G,negedge D,0,notifier);
	$hold(negedge G,posedge D,0,notifier);
	$setup(negedge D,negedge G,0,notifier);
	$setup(posedge D,negedge G,0,notifier);
	$width(posedge G,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       LATNB2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:22 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Active-High Enable, Latch; Q, QB Outputs
// Q  = G ?  D : 'p';QB = !Q
module LATNB2 (Q, QB, D, G);
	output Q;
	output QB;
	input  D;
	input  G;
	reg notifier;
	p_latch _i0 (Q, D, G, notifier);
	not _i1 (QB,Q);
	specify		(D => Q) = (0,0);
	(D => QB) = (0,0);
	(G => Q) = (1,1);
	(G => QB) = (1,1);
`ifdef no_tchk
`else
	$hold(negedge G,negedge D,0,notifier);
	$hold(negedge G,posedge D,0,notifier);
	$setup(negedge D,negedge G,0,notifier);
	$setup(posedge D,negedge G,0,notifier);
	$width(posedge G,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       LATNT1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:20 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Active-High Enable, Tri-state Latch; Z output
// Z  = EN ? (G ?  D : 'p') : 'z'
module LATNT1 (Z, D, EN, G);
	output Z;
	input  D;
	input  EN;
	input  G;
	reg notifier;
	p_latch _i0 (_n1, D, G, notifier);
	bufif1 _i1 (Z, _n1, EN);
`ifdef no_tchk
`else
	buf _wi0 (shcheckGDhl,EN);
`endif
	specify		(D => Z) = (0,0);
	(EN => Z) = (0,0,0,0,0,0);
	(G => Z) = (1,1);
`ifdef no_tchk
`else
	$hold(negedge G &&& shcheckGDhl === 1'b1,negedge D,0,notifier);
	$hold(negedge G &&& shcheckGDhl === 1'b1,posedge D,0,notifier);
	$setup(negedge D,negedge G &&& shcheckGDhl === 1'b1,0,notifier);
	$setup(posedge D,negedge G &&& shcheckGDhl === 1'b1,0,notifier);
	$width(posedge G,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       LATNT2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:16 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Active-High Enable, Tri-state Latch; Z output
// Z  = EN ? (G ?  D : 'p') : 'z'
module LATNT2 (Z, D, EN, G);
	output Z;
	input  D;
	input  EN;
	input  G;
	reg notifier;
	p_latch _i0 (_n1, D, G, notifier);
	bufif1 _i1 (Z, _n1, EN);
`ifdef no_tchk
`else
	buf _wi0 (shcheckGDhl,EN);
`endif
	specify		(D => Z) = (0,0);
	(EN => Z) = (0,0,0,0,0,0);
	(G => Z) = (1,1);
`ifdef no_tchk
`else
	$hold(negedge G &&& shcheckGDhl === 1'b1,negedge D,0,notifier);
	$hold(negedge G &&& shcheckGDhl === 1'b1,posedge D,0,notifier);
	$setup(negedge D,negedge G &&& shcheckGDhl === 1'b1,0,notifier);
	$setup(posedge D,negedge G &&& shcheckGDhl === 1'b1,0,notifier);
	$width(posedge G,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       LATNT4.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:20 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Active-High Enable, Tri-state Latch; Z output
// Z  = EN ? (G ?  D : 'p') : 'z'
module LATNT4 (Z, D, EN, G);
	output Z;
	input  D;
	input  EN;
	input  G;
	reg notifier;
	p_latch _i0 (_n1, D, G, notifier);
	bufif1 _i1 (Z, _n1, EN);
`ifdef no_tchk
`else
	buf _wi0 (shcheckGDhl,EN);
`endif
	specify		(D => Z) = (0,0);
	(EN => Z) = (0,0,0,0,0,0);
	(G => Z) = (1,1);
`ifdef no_tchk
`else
	$hold(negedge G &&& shcheckGDhl === 1'b1,negedge D,0,notifier);
	$hold(negedge G &&& shcheckGDhl === 1'b1,posedge D,0,notifier);
	$setup(negedge D,negedge G &&& shcheckGDhl === 1'b1,0,notifier);
	$setup(posedge D,negedge G &&& shcheckGDhl === 1'b1,0,notifier);
	$width(posedge G,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       LATPB1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:18 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Active-Low Enable, Latch; Q, QB Outputs
// Q  = !GB ? D : 'p';QB = !Q
module LATPB1 (Q, QB, D, GB);
	output Q;
	output QB;
	input  D;
	input  GB;
	reg notifier;
	not _i0 (_n1,GB);
	p_latch _i1 (Q, D, _n1, notifier);
	not _i2 (QB,Q);
	specify		(D => Q) = (0,0);
	(D => QB) = (0,0);
	(GB => Q) = (1,1);
	(GB => QB) = (1,1);
`ifdef no_tchk
`else
	$hold(posedge GB,negedge D,0,notifier);
	$hold(posedge GB,posedge D,0,notifier);
	$setup(negedge D,posedge GB,0,notifier);
	$setup(posedge D,posedge GB,0,notifier);
	$width(negedge GB,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       LATPB2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:16 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Active-Low Enable, Latch; Q, QB Outputs
// Q  = !GB ? D : 'p';QB = !Q
module LATPB2 (Q, QB, D, GB);
	output Q;
	output QB;
	input  D;
	input  GB;
	reg notifier;
	not _i0 (_n1,GB);
	p_latch _i1 (Q, D, _n1, notifier);
	not _i2 (QB,Q);
	specify		(D => Q) = (0,0);
	(D => QB) = (0,0);
	(GB => Q) = (1,1);
	(GB => QB) = (1,1);
`ifdef no_tchk
`else
	$hold(posedge GB,negedge D,0,notifier);
	$hold(posedge GB,posedge D,0,notifier);
	$setup(negedge D,posedge GB,0,notifier);
	$setup(posedge D,posedge GB,0,notifier);
	$width(negedge GB,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       LATPQ1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:17 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Active-Low Enable, Latch; Q Output
// Q  = !GB ? D : 'p'
module LATPQ1 (Q, D, GB);
	output Q;
	input  D;
	input  GB;
	reg notifier;
	not _i0 (_n1,GB);
	p_latch _i1 (Q, D, _n1, notifier);
	specify		(D => Q) = (0,0);
	(GB => Q) = (1,1);
`ifdef no_tchk
`else
	$hold(posedge GB,negedge D,0,notifier);
	$hold(posedge GB,posedge D,0,notifier);
	$setup(negedge D,posedge GB,0,notifier);
	$setup(posedge D,posedge GB,0,notifier);
	$width(negedge GB,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       LATPQ2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:12 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Active-Low Enable, Latch; Q Output
// Q  = !GB ? D : 'p'
module LATPQ2 (Q, D, GB);
	output Q;
	input  D;
	input  GB;
	reg notifier;
	not _i0 (_n1,GB);
	p_latch _i1 (Q, D, _n1, notifier);
	specify		(D => Q) = (0,0);
	(GB => Q) = (1,1);
`ifdef no_tchk
`else
	$hold(posedge GB,negedge D,0,notifier);
	$hold(posedge GB,posedge D,0,notifier);
	$setup(negedge D,posedge GB,0,notifier);
	$setup(posedge D,posedge GB,0,notifier);
	$width(negedge GB,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       LATRNB1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:14 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Active-High Enable, Latch with Clear; Q, QB Outputs
// Q  = !RB ? 0 : G ?  D : 'p';QB = !Q
module LATRNB1 (Q, QB, D, G, RB);
	output Q;
	output QB;
	input  D;
	input  G;
	input  RB;
	reg notifier;
	not _i0 (_n1,RB);
	p_latchr _i1 (Q, D, G, _n1, notifier);
	not _i2 (QB,Q);
`ifdef no_tchk
`else
	buf _wi0 (shcheckGDhl,RB);
	buf _wi1 (shcheckGRBhl,D);
`endif
	specify		(D => Q) = (0,0);
	(D => QB) = (0,0);
	(G => Q) = (1,1);
	(G => QB) = (1,1);
	(RB => Q) = (0,0);
	(RB => QB) = (0,0);
`ifdef no_tchk
`else
	$hold(negedge G &&& shcheckGDhl === 1'b1,negedge D,0,notifier);
	$hold(negedge G &&& shcheckGDhl === 1'b1,posedge D,0,notifier);
	$hold(negedge G,posedge RB,0,notifier);
	$setup(negedge D,negedge G &&& shcheckGDhl === 1'b1,0,notifier);
	$setup(posedge D,negedge G &&& shcheckGDhl === 1'b1,0,notifier);
	$setup(posedge RB,negedge G &&& shcheckGRBhl === 1'b1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(posedge G,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       LATRNB2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:16 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Active-High Enable, Latch with Clear; Q, QB Outputs
// Q  = !RB ? 0 : G ?  D : 'p';QB = !Q
module LATRNB2 (Q, QB, D, G, RB);
	output Q;
	output QB;
	input  D;
	input  G;
	input  RB;
	reg notifier;
	not _i0 (_n1,RB);
	p_latchr _i1 (Q, D, G, _n1, notifier);
	not _i2 (QB,Q);
`ifdef no_tchk
`else
	buf _wi0 (shcheckGDhl,RB);
	buf _wi1 (shcheckGRBhl,D);
`endif
	specify		(D => Q) = (0,0);
	(D => QB) = (0,0);
	(G => Q) = (1,1);
	(G => QB) = (1,1);
	(RB => Q) = (0,0);
	(RB => QB) = (0,0);
`ifdef no_tchk
`else
	$hold(negedge G &&& shcheckGDhl === 1'b1,negedge D,0,notifier);
	$hold(negedge G &&& shcheckGDhl === 1'b1,posedge D,0,notifier);
	$hold(negedge G,posedge RB,0,notifier);
	$setup(negedge D,negedge G &&& shcheckGDhl === 1'b1,0,notifier);
	$setup(posedge D,negedge G &&& shcheckGDhl === 1'b1,0,notifier);
	$setup(posedge RB,negedge G &&& shcheckGRBhl === 1'b1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(posedge G,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       LATRPB1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:12 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Active-Low Enable, Latch with Clear; Q, QB Outputs
// Q  = !RB ? 0 : !GB ? D : 'p';QB = !Q
module LATRPB1 (Q, QB, D, GB, RB);
	output Q;
	output QB;
	input  D;
	input  GB;
	input  RB;
	reg notifier;
	not _i0 (_n1,GB);
	not _i1 (_n2,RB);
	p_latchr _i2 (Q, D, _n1, _n2, notifier);
	not _i3 (QB,Q);
`ifdef no_tchk
`else
	buf _wi0 (shcheckGBDlh,RB);
	buf _wi1 (shcheckGBRBlh,D);
`endif
	specify		(D => Q) = (0,0);
	(D => QB) = (0,0);
	(GB => Q) = (1,1);
	(GB => QB) = (1,1);
	(RB => Q) = (0,0);
	(RB => QB) = (0,0);
`ifdef no_tchk
`else
	$hold(posedge GB &&& shcheckGBDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge GB &&& shcheckGBDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge GB,posedge RB,0,notifier);
	$setup(negedge D,posedge GB &&& shcheckGBDlh === 1'b1,0,notifier);
	$setup(posedge D,posedge GB &&& shcheckGBDlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge GB &&& shcheckGBRBlh === 1'b1,0,notifier);
	$width(negedge GB,1,0,notifier);
	$width(negedge RB,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       LATRPB2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:08 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Active-Low Enable, Latch with Clear; Q, QB Outputs
// Q  = !RB ? 0 : !GB ? D : 'p';QB = !Q
module LATRPB2 (Q, QB, D, GB, RB);
	output Q;
	output QB;
	input  D;
	input  GB;
	input  RB;
	reg notifier;
	not _i0 (_n1,GB);
	not _i1 (_n2,RB);
	p_latchr _i2 (Q, D, _n1, _n2, notifier);
	not _i3 (QB,Q);
`ifdef no_tchk
`else
	buf _wi0 (shcheckGBDlh,RB);
	buf _wi1 (shcheckGBRBlh,D);
`endif
	specify		(D => Q) = (0,0);
	(D => QB) = (0,0);
	(GB => Q) = (1,1);
	(GB => QB) = (1,1);
	(RB => Q) = (0,0);
	(RB => QB) = (0,0);
`ifdef no_tchk
`else
	$hold(posedge GB &&& shcheckGBDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge GB &&& shcheckGBDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge GB,posedge RB,0,notifier);
	$setup(negedge D,posedge GB &&& shcheckGBDlh === 1'b1,0,notifier);
	$setup(posedge D,posedge GB &&& shcheckGBDlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge GB &&& shcheckGBRBlh === 1'b1,0,notifier);
	$width(negedge GB,1,0,notifier);
	$width(negedge RB,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       LATRSNB1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:05 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Active-High Enable, Latch with Set, Clear; Q, QB Outputs
// Q  = !RB ? 0 : !SB ? 1 : G ?  D : 'p';QB = !Q & SB
module LATRSNB1 (Q, QB, D, G, RB, SB);
	output Q;
	output QB;
	input  D;
	input  G;
	input  RB;
	input  SB;
	reg notifier;
	not _i0 (_n1,SB);
	not _i1 (_n2,RB);
	p_latchrs _i2 (Q, D, G, _n1, _n2, notifier);
	not _i3 (_n4,Q);
	and _i4 (QB,_n4,SB);
`ifdef no_tchk
`else
	and _wi0 (shcheckGDhl,RB,SB);
	not _wi1 (_wn2,D);
	and _wi2 (shcheckGSBhl,_wn2,RB);
	not _wi3 (shcheckSBRBlh,G);
	and _wi4 (shcheckGRBhl,D,SB);
`endif
	specify		(D => Q) = (0,0);
	(D => QB) = (0,0);
	(G => Q) = (1,1);
	(G => QB) = (1,1);
	(RB => Q) = (0,0);
	(RB => QB) = (0,0);
	(SB => Q) = (0,0);
	(SB => QB) = (0,0);
`ifdef no_tchk
`else
	$hold(negedge G &&& shcheckGDhl === 1'b1,negedge D,0,notifier);
	$hold(negedge G &&& shcheckGDhl === 1'b1,posedge D,0,notifier);
	$hold(negedge G,posedge RB,0,notifier);
	$hold(negedge G,posedge SB,0,notifier);
	$hold(posedge SB,posedge RB,0,notifier);
	$setup(negedge D,negedge G &&& shcheckGDhl === 1'b1,0,notifier);
	$setup(posedge D,negedge G &&& shcheckGDhl === 1'b1,0,notifier);
	$setup(posedge RB,negedge G &&& shcheckGRBhl === 1'b1,0,notifier);
	$setup(posedge RB,posedge SB &&& shcheckSBRBlh === 1'b1,0,notifier);
	$setup(posedge SB,negedge G &&& shcheckGSBhl === 1'b1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(negedge SB,1,0,notifier);
	$width(posedge G,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       LATRSNB2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:02 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Active-High Enable, Latch with Set, Clear; Q, QB Outputs
// Q  = !RB ? 0 : !SB ? 1 : G ?  D : 'p';QB = !Q & SB
module LATRSNB2 (Q, QB, D, G, RB, SB);
	output Q;
	output QB;
	input  D;
	input  G;
	input  RB;
	input  SB;
	reg notifier;
	not _i0 (_n1,SB);
	not _i1 (_n2,RB);
	p_latchrs _i2 (Q, D, G, _n1, _n2, notifier);
	not _i3 (_n4,Q);
	and _i4 (QB,_n4,SB);
`ifdef no_tchk
`else
	and _wi0 (shcheckGDhl,RB,SB);
	not _wi1 (_wn2,D);
	and _wi2 (shcheckGSBhl,_wn2,RB);
	not _wi3 (shcheckSBRBlh,G);
	and _wi4 (shcheckGRBhl,D,SB);
`endif
	specify		(D => Q) = (0,0);
	(D => QB) = (0,0);
	(G => Q) = (1,1);
	(G => QB) = (1,1);
	(RB => Q) = (0,0);
	(RB => QB) = (0,0);
	(SB => Q) = (0,0);
	(SB => QB) = (0,0);
`ifdef no_tchk
`else
	$hold(negedge G &&& shcheckGDhl === 1'b1,negedge D,0,notifier);
	$hold(negedge G &&& shcheckGDhl === 1'b1,posedge D,0,notifier);
	$hold(negedge G,posedge RB,0,notifier);
	$hold(negedge G,posedge SB,0,notifier);
	$hold(posedge SB,posedge RB,0,notifier);
	$setup(negedge D,negedge G &&& shcheckGDhl === 1'b1,0,notifier);
	$setup(posedge D,negedge G &&& shcheckGDhl === 1'b1,0,notifier);
	$setup(posedge RB,negedge G &&& shcheckGRBhl === 1'b1,0,notifier);
	$setup(posedge RB,posedge SB &&& shcheckSBRBlh === 1'b1,0,notifier);
	$setup(posedge SB,negedge G &&& shcheckGSBhl === 1'b1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(negedge SB,1,0,notifier);
	$width(posedge G,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       LATRSPB1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:08 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Active-Low Enable, Latch with Set, Clear; Q, QB Outputs
// Q  = !RB ? 0 : !SB ? 1 : !GB ? D : 'p';QB = !Q & SB
module LATRSPB1 (Q, QB, D, GB, RB, SB);
	output Q;
	output QB;
	input  D;
	input  GB;
	input  RB;
	input  SB;
	reg notifier;
	not _i0 (_n1,GB);
	not _i1 (_n2,SB);
	not _i2 (_n3,RB);
	p_latchrs _i3 (Q, D, _n1, _n2, _n3, notifier);
	not _i4 (_n5,Q);
	and _i5 (QB,_n5,SB);
`ifdef no_tchk
`else
	and _wi0 (shcheckGBDlh,RB,SB);
	buf _wi1 (shcheckSBRBlh,GB);
	not _wi2 (_wn2,D);
	and _wi3 (shcheckGBSBlh,_wn2,RB);
	and _wi4 (shcheckGBRBlh,D,SB);
`endif
	specify		(D => Q) = (0,0);
	(D => QB) = (0,0);
	(GB => Q) = (1,1);
	(GB => QB) = (1,1);
	(RB => Q) = (0,0);
	(RB => QB) = (0,0);
	(SB => Q) = (0,0);
	(SB => QB) = (0,0);
`ifdef no_tchk
`else
	$hold(posedge GB &&& shcheckGBDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge GB &&& shcheckGBDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge GB,posedge RB,0,notifier);
	$hold(posedge GB,posedge SB,0,notifier);
	$hold(posedge SB,posedge RB,0,notifier);
	$setup(negedge D,posedge GB &&& shcheckGBDlh === 1'b1,0,notifier);
	$setup(posedge D,posedge GB &&& shcheckGBDlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge GB &&& shcheckGBRBlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge SB &&& shcheckSBRBlh === 1'b1,0,notifier);
	$setup(posedge SB,posedge GB &&& shcheckGBSBlh === 1'b1,0,notifier);
	$width(negedge GB,1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(negedge SB,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       LATRSPB2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:08 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Active-Low Enable, Latch with Set, Clear; Q, QB Outputs
// Q  = !RB ? 0 : !SB ? 1 : !GB ? D : 'p';QB = !Q & SB
module LATRSPB2 (Q, QB, D, GB, RB, SB);
	output Q;
	output QB;
	input  D;
	input  GB;
	input  RB;
	input  SB;
	reg notifier;
	not _i0 (_n1,GB);
	not _i1 (_n2,SB);
	not _i2 (_n3,RB);
	p_latchrs _i3 (Q, D, _n1, _n2, _n3, notifier);
	not _i4 (_n5,Q);
	and _i5 (QB,_n5,SB);
`ifdef no_tchk
`else
	and _wi0 (shcheckGBDlh,RB,SB);
	buf _wi1 (shcheckSBRBlh,GB);
	not _wi2 (_wn2,D);
	and _wi3 (shcheckGBSBlh,_wn2,RB);
	and _wi4 (shcheckGBRBlh,D,SB);
`endif
	specify		(D => Q) = (0,0);
	(D => QB) = (0,0);
	(GB => Q) = (1,1);
	(GB => QB) = (1,1);
	(RB => Q) = (0,0);
	(RB => QB) = (0,0);
	(SB => Q) = (0,0);
	(SB => QB) = (0,0);
`ifdef no_tchk
`else
	$hold(posedge GB &&& shcheckGBDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge GB &&& shcheckGBDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge GB,posedge RB,0,notifier);
	$hold(posedge GB,posedge SB,0,notifier);
	$hold(posedge SB,posedge RB,0,notifier);
	$setup(negedge D,posedge GB &&& shcheckGBDlh === 1'b1,0,notifier);
	$setup(posedge D,posedge GB &&& shcheckGBDlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge GB &&& shcheckGBRBlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge SB &&& shcheckSBRBlh === 1'b1,0,notifier);
	$setup(posedge SB,posedge GB &&& shcheckGBSBlh === 1'b1,0,notifier);
	$width(negedge GB,1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(negedge SB,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       MUX2D0.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:02 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-to-1 Multiplexer
// Z = SL ? A1 : A0
module MUX2D0 (Z, A0, A1, SL);
	output Z;
	input  A0;
	input  A1;
	input  SL;
	p_mux21 _i0 (Z, A1, A0, SL);
	specify		(A0 => Z) = (0,0);
	(A1 => Z) = (0,0);
	(SL => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       MUX2D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:07 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-to-1 Multiplexer
// Z = SL ? A1 : A0
module MUX2D1 (Z, A0, A1, SL);
	output Z;
	input  A0;
	input  A1;
	input  SL;
	p_mux21 _i0 (Z, A1, A0, SL);
	specify		(A0 => Z) = (0,0);
	(A1 => Z) = (0,0);
	(SL => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       MUX2D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:05 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-to-1 Multiplexer
// Z = SL ? A1 : A0
module MUX2D2 (Z, A0, A1, SL);
	output Z;
	input  A0;
	input  A1;
	input  SL;
	p_mux21 _i0 (Z, A1, A0, SL);
	specify		(A0 => Z) = (0,0);
	(A1 => Z) = (0,0);
	(SL => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       MUX2D4.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:59 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-to-1 Multiplexer
// Z = SL ? A1 : A0
module MUX2D4 (Z, A0, A1, SL);
	output Z;
	input  A0;
	input  A1;
	input  SL;
	p_mux21 _i0 (Z, A1, A0, SL);
	specify		(A0 => Z) = (0,0);
	(A1 => Z) = (0,0);
	(SL => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       MUX4D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:59 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// 4-to-1 Multiplexer
// 
module MUX4D1 (Z, A0, A1, A2, A3, SL0, SL1);
	output Z;
	input  A0;
	input  A1;
	input  A2;
	input  A3;
	input  SL0;
	input  SL1;
	mux4 udp_Z(Z, A0, A1, A2, A3, SL1, SL0);
	specify		(A0 => Z) = (0,0);
	(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(SL0 => Z) = (0,0);
	(SL1 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       MUX4D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:50:05 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// 4-to-1 Multiplexer
// 
module MUX4D2 (Z, A0, A1, A2, A3, SL0, SL1);
	output Z;
	input  A0;
	input  A1;
	input  A2;
	input  A3;
	input  SL0;
	input  SL1;
	mux4 udp_Z(Z, A0, A1, A2, A3, SL1, SL0);
	specify		(A0 => Z) = (0,0);
	(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(SL0 => Z) = (0,0);
	(SL1 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       MUX4D4.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:59 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// 4-to-1 Multiplexer
// 
module MUX4D4 (Z, A0, A1, A2, A3, SL0, SL1);
	output Z;
	input  A0;
	input  A1;
	input  A2;
	input  A3;
	input  SL0;
	input  SL1;
	mux4 udp_Z(Z, A0, A1, A2, A3, SL1, SL0);
	specify		(A0 => Z) = (0,0);
	(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(SL0 => Z) = (0,0);
	(SL1 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       MUXB2D0.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:59 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-to-1 Multiplexer with Inverted Output
// Z = ! (SL ? A1 : A0)
module MUXB2D0 (Z, A0, A1, SL);
	output Z;
	input  A0;
	input  A1;
	input  SL;
	p_mux21 _i0 (_n1, A1, A0, SL);
	not _i1 (Z,_n1);
	specify		(A0 => Z) = (0,0);
	(A1 => Z) = (0,0);
	(SL => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       MUXB2D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:59 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-to-1 Multiplexer with Inverted Output
// Z = ! (SL ? A1 : A0)
module MUXB2D1 (Z, A0, A1, SL);
	output Z;
	input  A0;
	input  A1;
	input  SL;
	p_mux21 _i0 (_n1, A1, A0, SL);
	not _i1 (Z,_n1);
	specify		(A0 => Z) = (0,0);
	(A1 => Z) = (0,0);
	(SL => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       MUXB2D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:59 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-to-1 Multiplexer with Inverted Output
// Z = ! (SL ? A1 : A0)
module MUXB2D2 (Z, A0, A1, SL);
	output Z;
	input  A0;
	input  A1;
	input  SL;
	p_mux21 _i0 (_n1, A1, A0, SL);
	not _i1 (Z,_n1);
	specify		(A0 => Z) = (0,0);
	(A1 => Z) = (0,0);
	(SL => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       MUXB2D4.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:53 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-to-1 Multiplexer with Inverted Output
// Z = ! (SL ? A1 : A0)
module MUXB2D4 (Z, A0, A1, SL);
	output Z;
	input  A0;
	input  A1;
	input  SL;
	p_mux21 _i0 (_n1, A1, A0, SL);
	not _i1 (Z,_n1);
	specify		(A0 => Z) = (0,0);
	(A1 => Z) = (0,0);
	(SL => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       NAN2D0.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:57 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-Input NAND Gate
// Z = ! ( A1 & A2  )
module NAN2D0 (Z, A1, A2);
	output Z;
	input  A1;
	input  A2;
	nand _i0 (Z,A1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       NAN2D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:53 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-Input NAND Gate
// Z = ! ( A1 & A2  )
module NAN2D1 (Z, A1, A2);
	output Z;
	input  A1;
	input  A2;
	nand _i0 (Z,A1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       NAN2D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:54 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-Input NAND Gate
// Z = ! ( A1 & A2  )
module NAN2D2 (Z, A1, A2);
	output Z;
	input  A1;
	input  A2;
	nand _i0 (Z,A1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       NAN2D4.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:51 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-Input NAND Gate
// Z = ! ( A1 & A2  )
module NAN2D4 (Z, A1, A2);
	output Z;
	input  A1;
	input  A2;
	nand _i0 (Z,A1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       NAN2M1D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:52 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-Input NAND Gate with Input A1 Inverted
// Z = ! ( ! A1 & A2  )
module NAN2M1D1 (Z, A1, A2);
	output Z;
	input  A1;
	input  A2;
	not _i0 (_n1,A1);
	nand _i1 (Z,_n1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       NAN2M1D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:46 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-Input NAND Gate with Input A1 Inverted
// Z = ! ( ! A1 & A2  )
module NAN2M1D2 (Z, A1, A2);
	output Z;
	input  A1;
	input  A2;
	not _i0 (_n1,A1);
	nand _i1 (Z,_n1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       NAN3D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:47 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 3-Input NAND Gate
// Z = ! ( A1 & A2 & A3  )
module NAN3D1 (Z, A1, A2, A3);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	nand _i0 (Z,A3,A1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       NAN3D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:46 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 3-Input NAND Gate
// Z = ! ( A1 & A2 & A3  )
module NAN3D2 (Z, A1, A2, A3);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	nand _i0 (Z,A3,A1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       NAN3D4.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:46 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 3-Input NAND Gate
// Z = ! ( A1 & A2 & A3  )
module NAN3D4 (Z, A1, A2, A3);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	nand _i0 (Z,A3,A1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       NAN3M1D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:46 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 3-Input NAND Gate with Input A1 Inverted
// Z = ! ( ! A1 & A2 & A3  )
module NAN3M1D1 (Z, A1, A2, A3);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	not _i0 (_n1,A1);
	nand _i1 (Z,A3,_n1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       NAN3M1D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:49 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 3-Input NAND Gate with Input A1 Inverted
// Z = ! ( ! A1 & A2 & A3  )
module NAN3M1D2 (Z, A1, A2, A3);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	not _i0 (_n1,A1);
	nand _i1 (Z,A3,_n1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       NAN3M2D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:46 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 3-Input NAND Gate with Inputs A1 and A2 Inverted
// Z = ! ( ! A1 & ! A2 & A3  )
module NAN3M2D1 (Z, A1, A2, A3);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	not _i0 (_n1,A1);
	not _i1 (_n2,A2);
	nand _i2 (Z,A3,_n1,_n2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       NAN3M2D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:42 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 3-Input NAND Gate with Inputs A1 and A2 Inverted
// Z = ! ( ! A1 & ! A2 & A3  )
module NAN3M2D2 (Z, A1, A2, A3);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	not _i0 (_n1,A1);
	not _i1 (_n2,A2);
	nand _i2 (Z,A3,_n1,_n2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       NAN4D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:42 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 4-Input NAND Gate
// Z = ! ( A1 & A2 & A3 & A4  )
module NAN4D1 (Z, A1, A2, A3, A4);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  A4;
	nand _i0 (Z,A4,A3,A1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(A4 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       NAN4D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:42 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 4-Input NAND Gate
// Z = ! ( A1 & A2 & A3 & A4  )
module NAN4D2 (Z, A1, A2, A3, A4);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  A4;
	nand _i0 (Z,A4,A3,A1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(A4 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       NAN4D4.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:39 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 4-Input NAND Gate
// Z = ! ( A1 & A2 & A3 & A4  )
module NAN4D4 (Z, A1, A2, A3, A4);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  A4;
	nand _i0 (Z,A4,A3,A1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(A4 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       NAN4M1D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:39 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 4-Input NAND Gate with Input A1 Inverted
// Z = ! ( ! A1 & A2 & A3 & A4  )
module NAN4M1D1 (Z, A1, A2, A3, A4);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  A4;
	not _i0 (_n1,A1);
	nand _i1 (Z,A4,A3,_n1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(A4 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       NAN4M1D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:41 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 4-Input NAND Gate with Input A1 Inverted
// Z = ! ( ! A1 & A2 & A3 & A4  )
module NAN4M1D2 (Z, A1, A2, A3, A4);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  A4;
	not _i0 (_n1,A1);
	nand _i1 (Z,A4,A3,_n1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(A4 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       NAN4M2D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:33 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 4-Input NAND Gate with Inputs A1 and A2Inverted
// Z = ! ( ! A1 & ! A2 & A3 & A4  )
module NAN4M2D1 (Z, A1, A2, A3, A4);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  A4;
	not _i0 (_n1,A1);
	not _i1 (_n2,A2);
	nand _i2 (Z,A4,A3,_n1,_n2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(A4 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       NAN4M2D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:39 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 4-Input NAND Gate with Inputs A1 and A2Inverted
// Z = ! ( ! A1 & ! A2 & A3 & A4  )
module NAN4M2D2 (Z, A1, A2, A3, A4);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  A4;
	not _i0 (_n1,A1);
	not _i1 (_n2,A2);
	nand _i2 (Z,A4,A3,_n1,_n2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(A4 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       NAN4M3D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:33 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 4-Input NAND Gate with Inputs A1, A2 and A3 Inverted
// Z = ! ( ! A1 & ! A2 & ! A3 & A4 )
module NAN4M3D1 (Z, A1, A2, A3, A4);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  A4;
	not _i0 (_n1,A3);
	not _i1 (_n2,A1);
	not _i2 (_n3,A2);
	nand _i3 (Z,A4,_n1,_n2,_n3);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(A4 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       NAN4M3D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:38 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 4-Input NAND Gate with Inputs A1, A2 and A3 Inverted
// Z = ! ( ! A1 & ! A2 & ! A3 & A4 )
module NAN4M3D2 (Z, A1, A2, A3, A4);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  A4;
	not _i0 (_n1,A3);
	not _i1 (_n2,A1);
	not _i2 (_n3,A2);
	nand _i3 (Z,A4,_n1,_n2,_n3);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(A4 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       NOR2D0.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:30 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-Input NOR Gate
// Z = ! ( A1 | A2  )
module NOR2D0 (Z, A1, A2);
	output Z;
	input  A1;
	input  A2;
	nor _i0 (Z,A1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       NOR2D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:30 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-Input NOR Gate
// Z = ! ( A1 | A2  )
module NOR2D1 (Z, A1, A2);
	output Z;
	input  A1;
	input  A2;
	nor _i0 (Z,A1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       NOR2D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:30 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-Input NOR Gate
// Z = ! ( A1 | A2  )
module NOR2D2 (Z, A1, A2);
	output Z;
	input  A1;
	input  A2;
	nor _i0 (Z,A1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       NOR2D4.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:27 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-Input NOR Gate
// Z = ! ( A1 | A2  )
module NOR2D4 (Z, A1, A2);
	output Z;
	input  A1;
	input  A2;
	nor _i0 (Z,A1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       NOR3D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:32 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 3-Input NOR Gate
// Z = ! ( A1 | A2 | A3  )
module NOR3D1 (Z, A1, A2, A3);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	nor _i0 (Z,A3,A1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       NOR3D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:27 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 3-Input NOR Gate
// Z = ! ( A1 | A2 | A3  )
module NOR3D2 (Z, A1, A2, A3);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	nor _i0 (Z,A3,A1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       NOR3D4.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:32 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 3-Input NOR Gate
// Z = ! ( A1 | A2 | A3  )
module NOR3D4 (Z, A1, A2, A3);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	nor _i0 (Z,A3,A1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       NOR4D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:26 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 4-Input NOR Gate
// Z = ! ( A1 | A2 | A3 | A4  )
module NOR4D1 (Z, A1, A2, A3, A4);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  A4;
	nor _i0 (Z,A4,A3,A1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(A4 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       NOR4D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:26 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 4-Input NOR Gate
// Z = ! ( A1 | A2 | A3 | A4  )
module NOR4D2 (Z, A1, A2, A3, A4);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  A4;
	nor _i0 (Z,A4,A3,A1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(A4 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       NOR4D4.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:23 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 4-Input NOR Gate
// Z = ! ( A1 | A2 | A3 | A4  )
module NOR4D4 (Z, A1, A2, A3, A4);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  A4;
	nor _i0 (Z,A4,A3,A1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(A4 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OA211D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:22 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-1-2 OR-AND Gate
// Z = ( A1 | A2  ) & B & C
module OA211D1 (Z, A1, A2, B, C);
	output Z;
	input  A1;
	input  A2;
	input  B;
	input  C;
	or _i0 (_n1,A1,A2);
	and _i1 (Z,C,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B => Z) = (0,0);
	(C => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OA211D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:22 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-1-2 OR-AND Gate
// Z = ( A1 | A2  ) & B & C
module OA211D2 (Z, A1, A2, B, C);
	output Z;
	input  A1;
	input  A2;
	input  B;
	input  C;
	or _i0 (_n1,A1,A2);
	and _i1 (Z,C,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B => Z) = (0,0);
	(C => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OA21D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:15 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-2 OR-AND Gate
// Z = ( A1 | A2  ) & B
module OA21D1 (Z, A1, A2, B);
	output Z;
	input  A1;
	input  A2;
	input  B;
	or _i0 (_n1,A1,A2);
	and _i1 (Z,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OA21D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:23 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-2 OR-AND Gate
// Z = ( A1 | A2  ) & B
module OA21D2 (Z, A1, A2, B);
	output Z;
	input  A1;
	input  A2;
	input  B;
	or _i0 (_n1,A1,A2);
	and _i1 (Z,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OA21M10D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:20 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-2 OR-AND Gate with Input A1 Inverted
// Z = ( ! A1 | A2  ) & B
module OA21M10D1 (Z, A1, A2, B);
	output Z;
	input  A1;
	input  A2;
	input  B;
	not _i0 (_n2,A1);
	or _i1 (_n1,_n2,A2);
	and _i2 (Z,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OA21M10D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:19 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-2 OR-AND Gate with Input A1 Inverted
// Z = ( ! A1 | A2  ) & B
module OA21M10D2 (Z, A1, A2, B);
	output Z;
	input  A1;
	input  A2;
	input  B;
	not _i0 (_n2,A1);
	or _i1 (_n1,_n2,A2);
	and _i2 (Z,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OA21M20D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:18 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-2 OR-AND Gate with Inputs A1 and A2 Inverted
// Z = ( ! A1 | ! A2  ) & B
module OA21M20D1 (Z, A1, A2, B);
	output Z;
	input  A1;
	input  A2;
	input  B;
	not _i0 (_n2,A1);
	not _i1 (_n3,A2);
	or _i2 (_n1,_n2,_n3);
	and _i3 (Z,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OA21M20D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:17 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-2 OR-AND Gate with Inputs A1 and A2 Inverted
// Z = ( ! A1 | ! A2  ) & B
module OA21M20D2 (Z, A1, A2, B);
	output Z;
	input  A1;
	input  A2;
	input  B;
	not _i0 (_n2,A1);
	not _i1 (_n3,A2);
	or _i2 (_n1,_n2,_n3);
	and _i3 (Z,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OA221D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:13 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-2-2 OR-AND Gate
// Z = ( A1 | A2  ) & ( B1 | B2 ) & C
module OA221D1 (Z, A1, A2, B1, B2, C);
	output Z;
	input  A1;
	input  A2;
	input  B1;
	input  B2;
	input  C;
	or _i0 (_n1,A1,A2);
	or _i1 (_n2,B1,B2);
	and _i2 (Z,C,_n1,_n2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(C => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OA221D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:15 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-2-2 OR-AND Gate
// Z = ( A1 | A2  ) & ( B1 | B2 ) & C
module OA221D2 (Z, A1, A2, B1, B2, C);
	output Z;
	input  A1;
	input  A2;
	input  B1;
	input  B2;
	input  C;
	or _i0 (_n1,A1,A2);
	or _i1 (_n2,B1,B2);
	and _i2 (Z,C,_n1,_n2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(C => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OA222D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:11 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-2-2 OR-AND Gate
// Z = ( A1 | A2  ) & ( B1 | B2 ) & ( C1 | C2 )
module OA222D1 (Z, A1, A2, B1, B2, C1, C2);
	output Z;
	input  A1;
	input  A2;
	input  B1;
	input  B2;
	input  C1;
	input  C2;
	or _i0 (_n1,A1,A2);
	or _i1 (_n2,B1,B2);
	or _i2 (_n3,C1,C2);
	and _i3 (Z,_n1,_n2,_n3);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(C1 => Z) = (0,0);
	(C2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OA222D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:08 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-2-2 OR-AND Gate
// Z = ( A1 | A2  ) & ( B1 | B2 ) & ( C1 | C2 )
module OA222D2 (Z, A1, A2, B1, B2, C1, C2);
	output Z;
	input  A1;
	input  A2;
	input  B1;
	input  B2;
	input  C1;
	input  C2;
	or _i0 (_n1,A1,A2);
	or _i1 (_n2,B1,B2);
	or _i2 (_n3,C1,C2);
	and _i3 (Z,_n1,_n2,_n3);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(C1 => Z) = (0,0);
	(C2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OA311D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:05 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-1-3 OR-AND Gate
// Z = ( A1 | A2 | A3  ) & B & C
module OA311D1 (Z, A1, A2, A3, B, C);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B;
	input  C;
	or _i0 (_n1,A3,A1,A2);
	and _i1 (Z,C,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B => Z) = (0,0);
	(C => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OA311D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:15 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-1-3 OR-AND Gate
// Z = ( A1 | A2 | A3  ) & B & C
module OA311D2 (Z, A1, A2, A3, B, C);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B;
	input  C;
	or _i0 (_n1,A3,A1,A2);
	and _i1 (Z,C,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B => Z) = (0,0);
	(C => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OA31D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:05 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-3 OR-AND Gate
// Z = ( A1 | A2 | A3  ) & B
module OA31D1 (Z, A1, A2, A3, B);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B;
	or _i0 (_n1,A3,A1,A2);
	and _i1 (Z,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OA31D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:04 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-3 OR-AND Gate
// Z = ( A1 | A2 | A3  ) & B
module OA31D2 (Z, A1, A2, A3, B);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B;
	or _i0 (_n1,A3,A1,A2);
	and _i1 (Z,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OA31M10D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:10 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-3 OR-AND Gate with Input A1 Inverted
// Z = ( ! A1 | A2 | A3  ) & B
module OA31M10D1 (Z, A1, A2, A3, B);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B;
	not _i0 (_n2,A1);
	or _i1 (_n1,A3,_n2,A2);
	and _i2 (Z,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OA31M10D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:17 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-3 OR-AND Gate with Input A1 Inverted
// Z = ( ! A1 | A2 | A3  ) & B
module OA31M10D2 (Z, A1, A2, A3, B);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B;
	not _i0 (_n2,A1);
	or _i1 (_n1,A3,_n2,A2);
	and _i2 (Z,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OA31M20D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:11 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-3 OR-AND Gate with Inputs A1 and A2 Inverted
// Z = ( ! A1 | ! A2 | A3  ) & B
module OA31M20D1 (Z, A1, A2, A3, B);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B;
	not _i0 (_n2,A1);
	not _i1 (_n3,A2);
	or _i2 (_n1,A3,_n2,_n3);
	and _i3 (Z,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OA31M20D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:02 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-3 OR-AND Gate with Inputs A1 and A2 Inverted
// Z = ( ! A1 | ! A2 | A3  ) & B
module OA31M20D2 (Z, A1, A2, A3, B);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B;
	not _i0 (_n2,A1);
	not _i1 (_n3,A2);
	or _i2 (_n1,A3,_n2,_n3);
	and _i3 (Z,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OA31M30D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:04 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-3 OR-AND Gate with Inputs A1, A2 and A3 Inverted
// Z = ( ! A1 | ! A2 | ! A3  ) & B
module OA31M30D1 (Z, A1, A2, A3, B);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B;
	not _i0 (_n2,A3);
	not _i1 (_n3,A1);
	not _i2 (_n4,A2);
	or _i3 (_n1,_n2,_n3,_n4);
	and _i4 (Z,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OA31M30D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:08 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-3 OR-AND Gate with Inputs A1, A2 and A3 Inverted
// Z = ( ! A1 | ! A2 | ! A3  ) & B
module OA31M30D2 (Z, A1, A2, A3, B);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B;
	not _i0 (_n2,A3);
	not _i1 (_n3,A1);
	not _i2 (_n4,A2);
	or _i3 (_n1,_n2,_n3,_n4);
	and _i4 (Z,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OA321D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:02 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-2-2 OR-AND Gate
// Z = ( A1 | A2 | A3  ) & ( B1 | B2 ) & C
module OA321D1 (Z, A1, A2, A3, B1, B2, C);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B1;
	input  B2;
	input  C;
	or _i0 (_n1,A3,A1,A2);
	or _i1 (_n2,B1,B2);
	and _i2 (Z,C,_n1,_n2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(C => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OA321D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:05 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-2-2 OR-AND Gate
// Z = ( A1 | A2 | A3  ) & ( B1 | B2 ) & C
module OA321D2 (Z, A1, A2, A3, B1, B2, C);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B1;
	input  B2;
	input  C;
	or _i0 (_n1,A3,A1,A2);
	or _i1 (_n2,B1,B2);
	and _i2 (Z,C,_n1,_n2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(C => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OA322D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:49:02 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-2-3 OR-AND Gate
// Z = ( A1 | A2 | A3  ) & ( B1 | B2 ) & ( C1 | C2 )
module OA322D1 (Z, A1, A2, A3, B1, B2, C1, C2);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B1;
	input  B2;
	input  C1;
	input  C2;
	or _i0 (_n1,A3,A1,A2);
	or _i1 (_n2,B1,B2);
	or _i2 (_n3,C1,C2);
	and _i3 (Z,_n1,_n2,_n3);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(C1 => Z) = (0,0);
	(C2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OA322D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:54 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-2-3 OR-AND Gate
// Z = ( A1 | A2 | A3  ) & ( B1 | B2 ) & ( C1 | C2 )
module OA322D2 (Z, A1, A2, A3, B1, B2, C1, C2);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B1;
	input  B2;
	input  C1;
	input  C2;
	or _i0 (_n1,A3,A1,A2);
	or _i1 (_n2,B1,B2);
	or _i2 (_n3,C1,C2);
	and _i3 (Z,_n1,_n2,_n3);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(C1 => Z) = (0,0);
	(C2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OA32D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:56 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-3 OR-AND Gate
// Z = ( A1 | A2 | A3  ) & ( B1 | B2 )
module OA32D1 (Z, A1, A2, A3, B1, B2);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B1;
	input  B2;
	or _i0 (_n1,A3,A1,A2);
	or _i1 (_n2,B1,B2);
	and _i2 (Z,_n1,_n2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OA32D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:56 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-3 OR-AND Gate
// Z = ( A1 | A2 | A3  ) & ( B1 | B2 )
module OA32D2 (Z, A1, A2, A3, B1, B2);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B1;
	input  B2;
	or _i0 (_n1,A3,A1,A2);
	or _i1 (_n2,B1,B2);
	and _i2 (Z,_n1,_n2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OA332D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:54 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-3-3 OR-AND Gate
// Z = ( A1 | A2 | A3  ) & ( B1 | B2 | B3 ) & ( C1 | C2 )
module OA332D1 (Z, A1, A2, A3, B1, B2, B3, C1, C2);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B1;
	input  B2;
	input  B3;
	input  C1;
	input  C2;
	or _i0 (_n1,A3,A1,A2);
	or _i1 (_n2,B3,B1,B2);
	or _i2 (_n3,C1,C2);
	and _i3 (Z,_n1,_n2,_n3);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(B3 => Z) = (0,0);
	(C1 => Z) = (0,0);
	(C2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OA332D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:54 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-3-3 OR-AND Gate
// Z = ( A1 | A2 | A3  ) & ( B1 | B2 | B3 ) & ( C1 | C2 )
module OA332D2 (Z, A1, A2, A3, B1, B2, B3, C1, C2);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B1;
	input  B2;
	input  B3;
	input  C1;
	input  C2;
	or _i0 (_n1,A3,A1,A2);
	or _i1 (_n2,B3,B1,B2);
	or _i2 (_n3,C1,C2);
	and _i3 (Z,_n1,_n2,_n3);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(B3 => Z) = (0,0);
	(C1 => Z) = (0,0);
	(C2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OA333D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:54 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 3-3-3 OR-AND Gate
// Z = ( A1 | A2 | A3  ) & ( B1 | B2 | B3 ) & ( C1 | C2 | C3 )
module OA333D1 (Z, A1, A2, A3, B1, B2, B3, C1, C2, C3);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B1;
	input  B2;
	input  B3;
	input  C1;
	input  C2;
	input  C3;
	or _i0 (_n1,A3,A1,A2);
	or _i1 (_n2,B3,B1,B2);
	or _i2 (_n3,C3,C1,C2);
	and _i3 (Z,_n1,_n2,_n3);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(B3 => Z) = (0,0);
	(C1 => Z) = (0,0);
	(C2 => Z) = (0,0);
	(C3 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OA333D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:51 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 3-3-3 OR-AND Gate
// Z = ( A1 | A2 | A3  ) & ( B1 | B2 | B3 ) & ( C1 | C2 | C3 )
module OA333D2 (Z, A1, A2, A3, B1, B2, B3, C1, C2, C3);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B1;
	input  B2;
	input  B3;
	input  C1;
	input  C2;
	input  C3;
	or _i0 (_n1,A3,A1,A2);
	or _i1 (_n2,B3,B1,B2);
	or _i2 (_n3,C3,C1,C2);
	and _i3 (Z,_n1,_n2,_n3);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(B3 => Z) = (0,0);
	(C1 => Z) = (0,0);
	(C2 => Z) = (0,0);
	(C3 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OA33D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:51 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 3-3 OR-AND Gate
// Z = ( A1 | A2 | A3  ) & ( B1 | B2 | B3 )
module OA33D1 (Z, A1, A2, A3, B1, B2, B3);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B1;
	input  B2;
	input  B3;
	or _i0 (_n1,A3,A1,A2);
	or _i1 (_n2,B3,B1,B2);
	and _i2 (Z,_n1,_n2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(B3 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OA33D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:48 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 3-3 OR-AND Gate
// Z = ( A1 | A2 | A3  ) & ( B1 | B2 | B3 )
module OA33D2 (Z, A1, A2, A3, B1, B2, B3);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B1;
	input  B2;
	input  B3;
	or _i0 (_n1,A3,A1,A2);
	or _i1 (_n2,B3,B1,B2);
	and _i2 (Z,_n1,_n2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(B3 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OAI211D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:46 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-1-2 OR-AND-Invert Gate
// Z = ! ( ( A1 | A2  ) & B & C  )
module OAI211D1 (Z, A1, A2, B, C);
	output Z;
	input  A1;
	input  A2;
	input  B;
	input  C;
	or _i0 (_n1,A1,A2);
	nand _i1 (Z,C,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B => Z) = (0,0);
	(C => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OAI211D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:54 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-1-2 OR-AND-Invert Gate
// Z = ! ( ( A1 | A2  ) & B & C  )
module OAI211D2 (Z, A1, A2, B, C);
	output Z;
	input  A1;
	input  A2;
	input  B;
	input  C;
	or _i0 (_n1,A1,A2);
	nand _i1 (Z,C,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B => Z) = (0,0);
	(C => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OAI21D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:46 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-2 OR-AND-Invert Gate
// Z = ! ( ( A1 | A2  ) & B  )
module OAI21D1 (Z, A1, A2, B);
	output Z;
	input  A1;
	input  A2;
	input  B;
	or _i0 (_n1,A1,A2);
	nand _i1 (Z,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OAI21D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:49 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-2 OR-AND-Invert Gate
// Z = ! ( ( A1 | A2  ) & B  )
module OAI21D2 (Z, A1, A2, B);
	output Z;
	input  A1;
	input  A2;
	input  B;
	or _i0 (_n1,A1,A2);
	nand _i1 (Z,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OAI21M10D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:49 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-2 OR-AND-Invert Gate with Input A1 Inverted
// Z = ! ( ( ! A1 | A2  ) & B  )
module OAI21M10D1 (Z, A1, A2, B);
	output Z;
	input  A1;
	input  A2;
	input  B;
	not _i0 (_n2,A1);
	or _i1 (_n1,_n2,A2);
	nand _i2 (Z,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OAI21M10D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:49 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-2 OR-AND-Invert Gate with Input A1 Inverted
// Z = ! ( ( ! A1 | A2  ) & B  )
module OAI21M10D2 (Z, A1, A2, B);
	output Z;
	input  A1;
	input  A2;
	input  B;
	not _i0 (_n2,A1);
	or _i1 (_n1,_n2,A2);
	nand _i2 (Z,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OAI21M20D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:41 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-2 OR-AND-Invert Gate with Inputs A1 and A2 Inverted
// Z = ! ( ( ! A1 | ! A2  ) & B  )
module OAI21M20D1 (Z, A1, A2, B);
	output Z;
	input  A1;
	input  A2;
	input  B;
	not _i0 (_n2,A1);
	not _i1 (_n3,A2);
	or _i2 (_n1,_n2,_n3);
	nand _i3 (Z,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OAI21M20D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:39 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-2 OR-AND-Invert Gate with Inputs A1 and A2 Inverted
// Z = ! ( ( ! A1 | ! A2  ) & B  )
module OAI21M20D2 (Z, A1, A2, B);
	output Z;
	input  A1;
	input  A2;
	input  B;
	not _i0 (_n2,A1);
	not _i1 (_n3,A2);
	or _i2 (_n1,_n2,_n3);
	nand _i3 (Z,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OAI221D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:44 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-2-2 OR-AND-Invert Gate
// Z = ! ( ( A1 | A2  ) & ( B1 | B2 ) & C  )
module OAI221D1 (Z, A1, A2, B1, B2, C);
	output Z;
	input  A1;
	input  A2;
	input  B1;
	input  B2;
	input  C;
	or _i0 (_n1,A1,A2);
	or _i1 (_n2,B1,B2);
	nand _i2 (Z,C,_n1,_n2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(C => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OAI221D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:41 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-2-2 OR-AND-Invert Gate
// Z = ! ( ( A1 | A2  ) & ( B1 | B2 ) & C  )
module OAI221D2 (Z, A1, A2, B1, B2, C);
	output Z;
	input  A1;
	input  A2;
	input  B1;
	input  B2;
	input  C;
	or _i0 (_n1,A1,A2);
	or _i1 (_n2,B1,B2);
	nand _i2 (Z,C,_n1,_n2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(C => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OAI222D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:41 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-2-2 OR-AND-Invert Gate
// Z = ! ( ( A1 | A2  ) & ( B1 | B2 ) & ( C1 | C2 )  )
module OAI222D1 (Z, A1, A2, B1, B2, C1, C2);
	output Z;
	input  A1;
	input  A2;
	input  B1;
	input  B2;
	input  C1;
	input  C2;
	or _i0 (_n1,A1,A2);
	or _i1 (_n2,B1,B2);
	or _i2 (_n3,C1,C2);
	nand _i3 (Z,_n1,_n2,_n3);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(C1 => Z) = (0,0);
	(C2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OAI222D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:35 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-2-2 OR-AND-Invert Gate
// Z = ! ( ( A1 | A2  ) & ( B1 | B2 ) & ( C1 | C2 )  )
module OAI222D2 (Z, A1, A2, B1, B2, C1, C2);
	output Z;
	input  A1;
	input  A2;
	input  B1;
	input  B2;
	input  C1;
	input  C2;
	or _i0 (_n1,A1,A2);
	or _i1 (_n2,B1,B2);
	or _i2 (_n3,C1,C2);
	nand _i3 (Z,_n1,_n2,_n3);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(C1 => Z) = (0,0);
	(C2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OAI22D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:39 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-2 OR-AND-Invert Gate
// Z = ! ( ( A1 | A2  ) & ( B1 | B2 )  )
module OAI22D1 (Z, A1, A2, B1, B2);
	output Z;
	input  A1;
	input  A2;
	input  B1;
	input  B2;
	or _i0 (_n1,A1,A2);
	or _i1 (_n2,B1,B2);
	nand _i2 (Z,_n1,_n2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OAI22D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:41 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-2 OR-AND-Invert Gate
// Z = ! ( ( A1 | A2  ) & ( B1 | B2 )  )
module OAI22D2 (Z, A1, A2, B1, B2);
	output Z;
	input  A1;
	input  A2;
	input  B1;
	input  B2;
	or _i0 (_n1,A1,A2);
	or _i1 (_n2,B1,B2);
	nand _i2 (Z,_n1,_n2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OAI22M10D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:35 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-2 OR-AND-Invert Gate with Input A1 Inverted
// Z = ! ( ( ! A1 | A2  ) & ( B1 | B2 )  )
module OAI22M10D1 (Z, A1, A2, B1, B2);
	output Z;
	input  A1;
	input  A2;
	input  B1;
	input  B2;
	not _i0 (_n2,A1);
	or _i1 (_n1,_n2,A2);
	or _i2 (_n3,B1,B2);
	nand _i3 (Z,_n1,_n3);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OAI22M10D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:33 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-2 OR-AND-Invert Gate with Input A1 Inverted
// Z = ! ( ( ! A1 | A2  ) & ( B1 | B2 )  )
module OAI22M10D2 (Z, A1, A2, B1, B2);
	output Z;
	input  A1;
	input  A2;
	input  B1;
	input  B2;
	not _i0 (_n2,A1);
	or _i1 (_n1,_n2,A2);
	or _i2 (_n3,B1,B2);
	nand _i3 (Z,_n1,_n3);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OAI22M11D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:31 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-2 OR-AND-Invert Gate with Inputs A1 and B1 Inverted
// Z = ! ( ( ! A1 | A2  ) & ( ! B1 | B2 )  )
module OAI22M11D1 (Z, A1, A2, B1, B2);
	output Z;
	input  A1;
	input  A2;
	input  B1;
	input  B2;
	not _i0 (_n2,A1);
	or _i1 (_n1,_n2,A2);
	not _i2 (_n4,B1);
	or _i3 (_n3,_n4,B2);
	nand _i4 (Z,_n1,_n3);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OAI22M11D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:29 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-2 OR-AND-Invert Gate with Inputs A1 and B1 Inverted
// Z = ! ( ( ! A1 | A2  ) & ( ! B1 | B2 )  )
module OAI22M11D2 (Z, A1, A2, B1, B2);
	output Z;
	input  A1;
	input  A2;
	input  B1;
	input  B2;
	not _i0 (_n2,A1);
	or _i1 (_n1,_n2,A2);
	not _i2 (_n4,B1);
	or _i3 (_n3,_n4,B2);
	nand _i4 (Z,_n1,_n3);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OAI22M20D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:26 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-2 OR-AND-Invert Gate with Input A1 and A2 Inverted
// Z = ! ( ( ! A1 | ! A2  ) & ( B1 | B2 )  )
module OAI22M20D1 (Z, A1, A2, B1, B2);
	output Z;
	input  A1;
	input  A2;
	input  B1;
	input  B2;
	not _i0 (_n2,A1);
	not _i1 (_n3,A2);
	or _i2 (_n1,_n2,_n3);
	or _i3 (_n4,B1,B2);
	nand _i4 (Z,_n1,_n4);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OAI22M20D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:29 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-2 OR-AND-Invert Gate with Input A1 and A2 Inverted
// Z = ! ( ( ! A1 | ! A2  ) & ( B1 | B2 )  )
module OAI22M20D2 (Z, A1, A2, B1, B2);
	output Z;
	input  A1;
	input  A2;
	input  B1;
	input  B2;
	not _i0 (_n2,A1);
	not _i1 (_n3,A2);
	or _i2 (_n1,_n2,_n3);
	or _i3 (_n4,B1,B2);
	nand _i4 (Z,_n1,_n4);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OAI22M21D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:35 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-2 OR-AND-Invert Gate with Inputs A1, A2 and B1 Inverted
// Z = ! ( ( ! A1 | ! A2  ) & ( ! B1 | B2 )  )
module OAI22M21D1 (Z, A1, A2, B1, B2);
	output Z;
	input  A1;
	input  A2;
	input  B1;
	input  B2;
	not _i0 (_n2,A1);
	not _i1 (_n3,A2);
	or _i2 (_n1,_n2,_n3);
	not _i3 (_n5,B1);
	or _i4 (_n4,_n5,B2);
	nand _i5 (Z,_n1,_n4);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OAI22M21D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:31 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-2 OR-AND-Invert Gate with Inputs A1, A2 and B1 Inverted
// Z = ! ( ( ! A1 | ! A2  ) & ( ! B1 | B2 )  )
module OAI22M21D2 (Z, A1, A2, B1, B2);
	output Z;
	input  A1;
	input  A2;
	input  B1;
	input  B2;
	not _i0 (_n2,A1);
	not _i1 (_n3,A2);
	or _i2 (_n1,_n2,_n3);
	not _i3 (_n5,B1);
	or _i4 (_n4,_n5,B2);
	nand _i5 (Z,_n1,_n4);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OAI22M22D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:29 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-2 OR-AND-Invert Gate with all Inputs Inverted
// Z = ! ( ( ! A1 | ! A2  ) & ( ! B1 | ! B2 )  )
module OAI22M22D1 (Z, A1, A2, B1, B2);
	output Z;
	input  A1;
	input  A2;
	input  B1;
	input  B2;
	not _i0 (_n2,A1);
	not _i1 (_n3,A2);
	or _i2 (_n1,_n2,_n3);
	not _i3 (_n5,B1);
	not _i4 (_n6,B2);
	or _i5 (_n4,_n5,_n6);
	nand _i6 (Z,_n1,_n4);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OAI22M22D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:27 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-2 OR-AND-Invert Gate with all Inputs Inverted
// Z = ! ( ( ! A1 | ! A2  ) & ( ! B1 | ! B2 )  )
module OAI22M22D2 (Z, A1, A2, B1, B2);
	output Z;
	input  A1;
	input  A2;
	input  B1;
	input  B2;
	not _i0 (_n2,A1);
	not _i1 (_n3,A2);
	or _i2 (_n1,_n2,_n3);
	not _i3 (_n5,B1);
	not _i4 (_n6,B2);
	or _i5 (_n4,_n5,_n6);
	nand _i6 (Z,_n1,_n4);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OAI311D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:29 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-1-3 OR-AND-Invert Gate
// Z = ! ( ( A1 | A2 | A3  ) & B & C  )
module OAI311D1 (Z, A1, A2, A3, B, C);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B;
	input  C;
	or _i0 (_n1,A3,A1,A2);
	nand _i1 (Z,C,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B => Z) = (0,0);
	(C => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OAI311D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:31 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-1-3 OR-AND-Invert Gate
// Z = ! ( ( A1 | A2 | A3  ) & B & C  )
module OAI311D2 (Z, A1, A2, A3, B, C);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B;
	input  C;
	or _i0 (_n1,A3,A1,A2);
	nand _i1 (Z,C,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B => Z) = (0,0);
	(C => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OAI31D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:26 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-3 OR-AND-Invert Gate
// Z = ! ( ( A1 | A2 | A3  ) & B  )
module OAI31D1 (Z, A1, A2, A3, B);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B;
	or _i0 (_n1,A3,A1,A2);
	nand _i1 (Z,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OAI31D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:21 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-3 OR-AND-Invert Gate
// Z = ! ( ( A1 | A2 | A3  ) & B  )
module OAI31D2 (Z, A1, A2, A3, B);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B;
	or _i0 (_n1,A3,A1,A2);
	nand _i1 (Z,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OAI31M10D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:26 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-3 OR-AND-Invert Gate with Input A1 Inverted
// Z = ! ( ( ! A1 | A2 | A3  ) & B  )
module OAI31M10D1 (Z, A1, A2, A3, B);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B;
	not _i0 (_n2,A1);
	or _i1 (_n1,A3,_n2,A2);
	nand _i2 (Z,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OAI31M10D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:21 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-3 OR-AND-Invert Gate with Input A1 Inverted
// Z = ! ( ( ! A1 | A2 | A3  ) & B  )
module OAI31M10D2 (Z, A1, A2, A3, B);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B;
	not _i0 (_n2,A1);
	or _i1 (_n1,A3,_n2,A2);
	nand _i2 (Z,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OAI31M20D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:21 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-3 OR-AND-Invert Gate with Inputs A1 and A2 Inverted
// Z = ! ( ( ! A1 | ! A2 | A3  ) & B  )
module OAI31M20D1 (Z, A1, A2, A3, B);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B;
	not _i0 (_n2,A1);
	not _i1 (_n3,A2);
	or _i2 (_n1,A3,_n2,_n3);
	nand _i3 (Z,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OAI31M20D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:19 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-3 OR-AND-Invert Gate with Inputs A1 and A2 Inverted
// Z = ! ( ( ! A1 | ! A2 | A3  ) & B  )
module OAI31M20D2 (Z, A1, A2, A3, B);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B;
	not _i0 (_n2,A1);
	not _i1 (_n3,A2);
	or _i2 (_n1,A3,_n2,_n3);
	nand _i3 (Z,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OAI31M30D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:14 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-3 OR-AND-Invert Gate with Inputs A1, A2 and A3 Inverted
// Z = ! ( ( ! A1 | ! A2 | ! A3  ) & B  )
module OAI31M30D1 (Z, A1, A2, A3, B);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B;
	not _i0 (_n2,A3);
	not _i1 (_n3,A1);
	not _i2 (_n4,A2);
	or _i3 (_n1,_n2,_n3,_n4);
	nand _i4 (Z,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OAI31M30D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:17 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-3 OR-AND-Invert Gate with Inputs A1, A2 and A3 Inverted
// Z = ! ( ( ! A1 | ! A2 | ! A3  ) & B  )
module OAI31M30D2 (Z, A1, A2, A3, B);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B;
	not _i0 (_n2,A3);
	not _i1 (_n3,A1);
	not _i2 (_n4,A2);
	or _i3 (_n1,_n2,_n3,_n4);
	nand _i4 (Z,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OAI321D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:14 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-2-3 OR-AND-Invert Gate
// Z = ! ( ( A1 | A2 | A3  ) & ( B1 | B2 ) & C  )
module OAI321D1 (Z, A1, A2, A3, B1, B2, C);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B1;
	input  B2;
	input  C;
	or _i0 (_n1,A3,A1,A2);
	or _i1 (_n2,B1,B2);
	nand _i2 (Z,C,_n1,_n2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(C => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OAI321D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:15 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-2-3 OR-AND-Invert Gate
// Z = ! ( ( A1 | A2 | A3  ) & ( B1 | B2 ) & C  )
module OAI321D2 (Z, A1, A2, A3, B1, B2, C);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B1;
	input  B2;
	input  C;
	or _i0 (_n1,A3,A1,A2);
	or _i1 (_n2,B1,B2);
	nand _i2 (Z,C,_n1,_n2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(C => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OAI322D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:19 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-2-3 OR-AND-Invert Gate
// Z = ! ( ( A1 | A2 | A3  ) & ( B1 | B2 ) & ( C1 | C2 )  )
module OAI322D1 (Z, A1, A2, A3, B1, B2, C1, C2);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B1;
	input  B2;
	input  C1;
	input  C2;
	or _i0 (_n1,A3,A1,A2);
	or _i1 (_n2,B1,B2);
	or _i2 (_n3,C1,C2);
	nand _i3 (Z,_n1,_n2,_n3);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(C1 => Z) = (0,0);
	(C2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OAI322D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:14 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-2-3 OR-AND-Invert Gate
// Z = ! ( ( A1 | A2 | A3  ) & ( B1 | B2 ) & ( C1 | C2 )  )
module OAI322D2 (Z, A1, A2, A3, B1, B2, C1, C2);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B1;
	input  B2;
	input  C1;
	input  C2;
	or _i0 (_n1,A3,A1,A2);
	or _i1 (_n2,B1,B2);
	or _i2 (_n3,C1,C2);
	nand _i3 (Z,_n1,_n2,_n3);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(C1 => Z) = (0,0);
	(C2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OAI32D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:14 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-3 OR-AND-Invert Gate
// Z = ! ( ( A1 | A2 | A3  ) & ( B1 | B2 )  )
module OAI32D1 (Z, A1, A2, A3, B1, B2);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B1;
	input  B2;
	or _i0 (_n1,A3,A1,A2);
	or _i1 (_n2,B1,B2);
	nand _i2 (Z,_n1,_n2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OAI32D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:09 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-3 OR-AND-Invert Gate
// Z = ! ( ( A1 | A2 | A3  ) & ( B1 | B2 )  )
module OAI32D2 (Z, A1, A2, A3, B1, B2);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B1;
	input  B2;
	or _i0 (_n1,A3,A1,A2);
	or _i1 (_n2,B1,B2);
	nand _i2 (Z,_n1,_n2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OAI332D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:09 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-3-3 OR-AND-Invert Gate
// Z = ! ( ( A1 | A2 | A3  ) & ( B1 | B2 | B3 ) & ( C1 | C2 )  )
module OAI332D1 (Z, A1, A2, A3, B1, B2, B3, C1, C2);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B1;
	input  B2;
	input  B3;
	input  C1;
	input  C2;
	or _i0 (_n1,A3,A1,A2);
	or _i1 (_n2,B3,B1,B2);
	or _i2 (_n3,C1,C2);
	nand _i3 (Z,_n1,_n2,_n3);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(B3 => Z) = (0,0);
	(C1 => Z) = (0,0);
	(C2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OAI332D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:07 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-3-3 OR-AND-Invert Gate
// Z = ! ( ( A1 | A2 | A3  ) & ( B1 | B2 | B3 ) & ( C1 | C2 )  )
module OAI332D2 (Z, A1, A2, A3, B1, B2, B3, C1, C2);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B1;
	input  B2;
	input  B3;
	input  C1;
	input  C2;
	or _i0 (_n1,A3,A1,A2);
	or _i1 (_n2,B3,B1,B2);
	or _i2 (_n3,C1,C2);
	nand _i3 (Z,_n1,_n2,_n3);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(B3 => Z) = (0,0);
	(C1 => Z) = (0,0);
	(C2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OAI333D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:04 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 3-3-3 OR-AND-Invert Gate
// Z = ! ( ( A1 | A2 | A3  ) & ( B1 | B2 | B3 ) & ( C1 | C2 | C3 )  )
module OAI333D1 (Z, A1, A2, A3, B1, B2, B3, C1, C2, C3);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B1;
	input  B2;
	input  B3;
	input  C1;
	input  C2;
	input  C3;
	or _i0 (_n1,A3,A1,A2);
	or _i1 (_n2,B3,B1,B2);
	or _i2 (_n3,C3,C1,C2);
	nand _i3 (Z,_n1,_n2,_n3);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(B3 => Z) = (0,0);
	(C1 => Z) = (0,0);
	(C2 => Z) = (0,0);
	(C3 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OAI333D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:07 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 3-3-3 OR-AND-Invert Gate
// Z = ! ( ( A1 | A2 | A3  ) & ( B1 | B2 | B3 ) & ( C1 | C2 | C3 )  )
module OAI333D2 (Z, A1, A2, A3, B1, B2, B3, C1, C2, C3);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B1;
	input  B2;
	input  B3;
	input  C1;
	input  C2;
	input  C3;
	or _i0 (_n1,A3,A1,A2);
	or _i1 (_n2,B3,B1,B2);
	or _i2 (_n3,C3,C1,C2);
	nand _i3 (Z,_n1,_n2,_n3);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(B3 => Z) = (0,0);
	(C1 => Z) = (0,0);
	(C2 => Z) = (0,0);
	(C3 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OAI33D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:03 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 3-3 OR-AND-Invert Gate
// Z = ! ( ( A1 | A2 | A3  ) & ( B1 | B2 | B3 )  )
module OAI33D1 (Z, A1, A2, A3, B1, B2, B3);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B1;
	input  B2;
	input  B3;
	or _i0 (_n1,A3,A1,A2);
	or _i1 (_n2,B3,B1,B2);
	nand _i2 (Z,_n1,_n2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(B3 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OAI33D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:00 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 3-3 OR-AND-Invert Gate
// Z = ! ( ( A1 | A2 | A3  ) & ( B1 | B2 | B3 )  )
module OAI33D2 (Z, A1, A2, A3, B1, B2, B3);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  B1;
	input  B2;
	input  B3;
	or _i0 (_n1,A3,A1,A2);
	or _i1 (_n2,B3,B1,B2);
	nand _i2 (Z,_n1,_n2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	(B3 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OAN211D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:04 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-1-2 OR-AND-NOR Gate
// Z = ! ( C | ( B & ( A1 | A2  )  )  )
module OAN211D1 (Z, A1, A2, B, C);
	output Z;
	input  A1;
	input  A2;
	input  B;
	input  C;
	or _i0 (_n2,A1,A2);
	and _i1 (_n1,B,_n2);
	nor _i2 (Z,C,_n1);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B => Z) = (0,0);
	(C => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OAN211D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:08 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-1-2 OR-AND-NOR Gate
// Z = ! ( C | ( B & ( A1 | A2  )  )  )
module OAN211D2 (Z, A1, A2, B, C);
	output Z;
	input  A1;
	input  A2;
	input  B;
	input  C;
	or _i0 (_n2,A1,A2);
	and _i1 (_n1,B,_n2);
	nor _i2 (Z,C,_n1);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B => Z) = (0,0);
	(C => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OR2D0.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:08 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-Input OR Gate
// Z = A1 | A2
module OR2D0 (Z, A1, A2);
	output Z;
	input  A1;
	input  A2;
	or _i0 (Z,A1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OR2D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:04 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-Input OR Gate
// Z = A1 | A2
module OR2D1 (Z, A1, A2);
	output Z;
	input  A1;
	input  A2;
	or _i0 (Z,A1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OR2D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:03 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-Input OR Gate
// Z = A1 | A2
module OR2D2 (Z, A1, A2);
	output Z;
	input  A1;
	input  A2;
	or _i0 (Z,A1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OR2D4.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:48:03 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 2-Input OR Gate
// Z = A1 | A2
module OR2D4 (Z, A1, A2);
	output Z;
	input  A1;
	input  A2;
	or _i0 (Z,A1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OR3D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:59 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 3-Input OR Gate
// Z = A1 | A2 | A3
module OR3D1 (Z, A1, A2, A3);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	or _i0 (Z,A3,A1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OR3D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:54 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 3-Input OR Gate
// Z = A1 | A2 | A3
module OR3D2 (Z, A1, A2, A3);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	or _i0 (Z,A3,A1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OR3D4.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:59 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 3-Input OR Gate
// Z = A1 | A2 | A3
module OR3D4 (Z, A1, A2, A3);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	or _i0 (Z,A3,A1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OR4D1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:49 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 4-Input OR Gate
// Z = A1 | A2 | A3 | A4
module OR4D1 (Z, A1, A2, A3, A4);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  A4;
	or _i0 (Z,A4,A3,A1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(A4 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OR4D2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:49 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 4-Input OR Gate
// Z = A1 | A2 | A3 | A4
module OR4D2 (Z, A1, A2, A3, A4);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  A4;
	or _i0 (Z,A4,A3,A1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(A4 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       OR4D4.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:54 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 4-Input OR Gate
// Z = A1 | A2 | A3 | A4
module OR4D4 (Z, A1, A2, A3, A4);
	output Z;
	input  A1;
	input  A2;
	input  A3;
	input  A4;
	or _i0 (Z,A4,A3,A1,A2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(A4 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       SDDEPQ1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:56 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Enabled, Positive Edge, Scan Mux Data Flip-Flop; Q Output
// Q  = rising(CK) ? (SE ? SD : (CEB ? 's' : (DSL ? D1 : D0))) : 'p'
module SDDEPQ1 (Q, CEB, CK, D0, D1, DSL, SD, SE);
	output Q;
	input  CEB;
	input  CK;
	input  D0;
	input  D1;
	input  DSL;
	input  SD;
	input  SE;
	reg notifier;
	p_mux21 _i0 (_n3, D1, D0, DSL);
	p_mux21 _i1 (_n2, buf_Q, _n3, CEB);
	p_mux21 _i2 (_n1, SD, _n2, SE);
	p_ff _i3 (buf_Q, _n1, CK, notifier);
	buf _iQ(Q, buf_Q);
`ifdef no_tchk
`else
	not _wi0 (_wn2,CEB);
	not _wi1 (_wn5,D0);
	not _wi2 (_wn8,D1);
	and _wi3 (_wn7,_wn8,SD);
	not _wi4 (_wn12,DSL);
	and _wi5 (_wn11,_wn12,SD);
	not _wi6 (_wn14,SD);
	and _wi7 (_wn13,DSL,_wn14);
	or _wi8 (_wn10,_wn11,_wn13);
	and _wi9 (_wn9,D1,_wn10);
	or _wi10 (_wn6,_wn7,_wn9);
	and _wi11 (_wn4,_wn5,_wn6);
	and _wi15 (_wn20,_wn12,_wn14);
	and _wi16 (_wn23,DSL,SD);
	or _wi17 (_wn19,_wn20,_wn23);
	and _wi18 (_wn17,_wn8,_wn19);
	and _wi20 (_wn24,D1,_wn14);
	or _wi21 (_wn16,_wn17,_wn24);
	and _wi22 (_wn15,D0,_wn16);
	or _wi23 (_wn3,_wn4,_wn15);
	and _wi24 (_wn1,_wn2,_wn3);
	or _wi25 (shcheckCKSElh,_wn1,CEB);
	not _wi27 (_wn30,SE);
	and _wi29 (_wn29,_wn30,_wn5,D1);
	and _wi32 (_wn32,_wn30,D0,_wn8);
	or _wi33 (_wn28,_wn29,_wn32);
	and _wi34 (shcheckCKDSLlh,_wn2,_wn28);
	and _wi37 (shcheckCKD1lh,_wn30,_wn2,DSL);
	buf _wi38 (shcheckCKSDlh,SE);
	not _wi39 (shcheckCKCEBlh,SE);
	and _wi43 (shcheckCKD0lh,shcheckCKCEBlh,_wn2,_wn12);
`endif
	specify		(CK => Q) = (1,1);
`ifdef no_tchk
`else
	$hold(posedge CK &&& shcheckCKCEBlh === 1'b1,negedge CEB,0,notifier);
	$hold(posedge CK &&& shcheckCKCEBlh === 1'b1,posedge CEB,0,notifier);
	$hold(posedge CK &&& shcheckCKD0lh === 1'b1,negedge D0,0,notifier);
	$hold(posedge CK &&& shcheckCKD0lh === 1'b1,posedge D0,0,notifier);
	$hold(posedge CK &&& shcheckCKD1lh === 1'b1,negedge D1,0,notifier);
	$hold(posedge CK &&& shcheckCKD1lh === 1'b1,posedge D1,0,notifier);
	$hold(posedge CK &&& shcheckCKDSLlh === 1'b1,negedge DSL,0,notifier);
	$hold(posedge CK &&& shcheckCKDSLlh === 1'b1,posedge DSL,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,negedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,posedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,negedge SE,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,posedge SE,0,notifier);
	$setup(negedge CEB,posedge CK &&& shcheckCKCEBlh === 1'b1,0,notifier);
	$setup(negedge D0,posedge CK &&& shcheckCKD0lh === 1'b1,0,notifier);
	$setup(negedge D1,posedge CK &&& shcheckCKD1lh === 1'b1,0,notifier);
	$setup(negedge DSL,posedge CK &&& shcheckCKDSLlh === 1'b1,0,notifier);
	$setup(negedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(negedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$setup(posedge CEB,posedge CK &&& shcheckCKCEBlh === 1'b1,0,notifier);
	$setup(posedge D0,posedge CK &&& shcheckCKD0lh === 1'b1,0,notifier);
	$setup(posedge D1,posedge CK &&& shcheckCKD1lh === 1'b1,0,notifier);
	$setup(posedge DSL,posedge CK &&& shcheckCKDSLlh === 1'b1,0,notifier);
	$setup(posedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(posedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$width(negedge CK,1,0,notifier);
	$width(posedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       SDDEPQ2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:49 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Enabled, Positive Edge, Scan Mux Data Flip-Flop; Q Output
// Q  = rising(CK) ? (SE ? SD : (CEB ? 's' : (DSL ? D1 : D0))) : 'p'
module SDDEPQ2 (Q, CEB, CK, D0, D1, DSL, SD, SE);
	output Q;
	input  CEB;
	input  CK;
	input  D0;
	input  D1;
	input  DSL;
	input  SD;
	input  SE;
	reg notifier;
	p_mux21 _i0 (_n3, D1, D0, DSL);
	p_mux21 _i1 (_n2, buf_Q, _n3, CEB);
	p_mux21 _i2 (_n1, SD, _n2, SE);
	p_ff _i3 (buf_Q, _n1, CK, notifier);
	buf _iQ(Q, buf_Q);
`ifdef no_tchk
`else
	not _wi0 (_wn2,CEB);
	not _wi1 (_wn5,D0);
	not _wi2 (_wn8,D1);
	and _wi3 (_wn7,_wn8,SD);
	not _wi4 (_wn12,DSL);
	and _wi5 (_wn11,_wn12,SD);
	not _wi6 (_wn14,SD);
	and _wi7 (_wn13,DSL,_wn14);
	or _wi8 (_wn10,_wn11,_wn13);
	and _wi9 (_wn9,D1,_wn10);
	or _wi10 (_wn6,_wn7,_wn9);
	and _wi11 (_wn4,_wn5,_wn6);
	and _wi15 (_wn20,_wn12,_wn14);
	and _wi16 (_wn23,DSL,SD);
	or _wi17 (_wn19,_wn20,_wn23);
	and _wi18 (_wn17,_wn8,_wn19);
	and _wi20 (_wn24,D1,_wn14);
	or _wi21 (_wn16,_wn17,_wn24);
	and _wi22 (_wn15,D0,_wn16);
	or _wi23 (_wn3,_wn4,_wn15);
	and _wi24 (_wn1,_wn2,_wn3);
	or _wi25 (shcheckCKSElh,_wn1,CEB);
	not _wi27 (_wn30,SE);
	and _wi29 (_wn29,_wn30,_wn5,D1);
	and _wi32 (_wn32,_wn30,D0,_wn8);
	or _wi33 (_wn28,_wn29,_wn32);
	and _wi34 (shcheckCKDSLlh,_wn2,_wn28);
	and _wi37 (shcheckCKD1lh,_wn30,_wn2,DSL);
	buf _wi38 (shcheckCKSDlh,SE);
	not _wi39 (shcheckCKCEBlh,SE);
	and _wi43 (shcheckCKD0lh,shcheckCKCEBlh,_wn2,_wn12);
`endif
	specify		(CK => Q) = (1,1);
`ifdef no_tchk
`else
	$hold(posedge CK &&& shcheckCKCEBlh === 1'b1,negedge CEB,0,notifier);
	$hold(posedge CK &&& shcheckCKCEBlh === 1'b1,posedge CEB,0,notifier);
	$hold(posedge CK &&& shcheckCKD0lh === 1'b1,negedge D0,0,notifier);
	$hold(posedge CK &&& shcheckCKD0lh === 1'b1,posedge D0,0,notifier);
	$hold(posedge CK &&& shcheckCKD1lh === 1'b1,negedge D1,0,notifier);
	$hold(posedge CK &&& shcheckCKD1lh === 1'b1,posedge D1,0,notifier);
	$hold(posedge CK &&& shcheckCKDSLlh === 1'b1,negedge DSL,0,notifier);
	$hold(posedge CK &&& shcheckCKDSLlh === 1'b1,posedge DSL,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,negedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,posedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,negedge SE,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,posedge SE,0,notifier);
	$setup(negedge CEB,posedge CK &&& shcheckCKCEBlh === 1'b1,0,notifier);
	$setup(negedge D0,posedge CK &&& shcheckCKD0lh === 1'b1,0,notifier);
	$setup(negedge D1,posedge CK &&& shcheckCKD1lh === 1'b1,0,notifier);
	$setup(negedge DSL,posedge CK &&& shcheckCKDSLlh === 1'b1,0,notifier);
	$setup(negedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(negedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$setup(posedge CEB,posedge CK &&& shcheckCKCEBlh === 1'b1,0,notifier);
	$setup(posedge D0,posedge CK &&& shcheckCKD0lh === 1'b1,0,notifier);
	$setup(posedge D1,posedge CK &&& shcheckCKD1lh === 1'b1,0,notifier);
	$setup(posedge DSL,posedge CK &&& shcheckCKDSLlh === 1'b1,0,notifier);
	$setup(posedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(posedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$width(negedge CK,1,0,notifier);
	$width(posedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       SDDEPQ4.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:54 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Enabled, Positive Edge, Scan Mux Data Flip-Flop; Q Output
// Q  = rising(CK) ? (SE ? SD : (CEB ? 's' : (DSL ? D1 : D0))) : 'p'
module SDDEPQ4 (Q, CEB, CK, D0, D1, DSL, SD, SE);
	output Q;
	input  CEB;
	input  CK;
	input  D0;
	input  D1;
	input  DSL;
	input  SD;
	input  SE;
	reg notifier;
	p_mux21 _i0 (_n3, D1, D0, DSL);
	p_mux21 _i1 (_n2, buf_Q, _n3, CEB);
	p_mux21 _i2 (_n1, SD, _n2, SE);
	p_ff _i3 (buf_Q, _n1, CK, notifier);
	buf _iQ(Q, buf_Q);
`ifdef no_tchk
`else
	not _wi0 (_wn2,CEB);
	not _wi1 (_wn5,D0);
	not _wi2 (_wn8,D1);
	and _wi3 (_wn7,_wn8,SD);
	not _wi4 (_wn12,DSL);
	and _wi5 (_wn11,_wn12,SD);
	not _wi6 (_wn14,SD);
	and _wi7 (_wn13,DSL,_wn14);
	or _wi8 (_wn10,_wn11,_wn13);
	and _wi9 (_wn9,D1,_wn10);
	or _wi10 (_wn6,_wn7,_wn9);
	and _wi11 (_wn4,_wn5,_wn6);
	and _wi15 (_wn20,_wn12,_wn14);
	and _wi16 (_wn23,DSL,SD);
	or _wi17 (_wn19,_wn20,_wn23);
	and _wi18 (_wn17,_wn8,_wn19);
	and _wi20 (_wn24,D1,_wn14);
	or _wi21 (_wn16,_wn17,_wn24);
	and _wi22 (_wn15,D0,_wn16);
	or _wi23 (_wn3,_wn4,_wn15);
	and _wi24 (_wn1,_wn2,_wn3);
	or _wi25 (shcheckCKSElh,_wn1,CEB);
	not _wi27 (_wn30,SE);
	and _wi29 (_wn29,_wn30,_wn5,D1);
	and _wi32 (_wn32,_wn30,D0,_wn8);
	or _wi33 (_wn28,_wn29,_wn32);
	and _wi34 (shcheckCKDSLlh,_wn2,_wn28);
	and _wi37 (shcheckCKD1lh,_wn30,_wn2,DSL);
	buf _wi38 (shcheckCKSDlh,SE);
	not _wi39 (shcheckCKCEBlh,SE);
	and _wi43 (shcheckCKD0lh,shcheckCKCEBlh,_wn2,_wn12);
`endif
	specify		(CK => Q) = (1,1);
`ifdef no_tchk
`else
	$hold(posedge CK &&& shcheckCKCEBlh === 1'b1,negedge CEB,0,notifier);
	$hold(posedge CK &&& shcheckCKCEBlh === 1'b1,posedge CEB,0,notifier);
	$hold(posedge CK &&& shcheckCKD0lh === 1'b1,negedge D0,0,notifier);
	$hold(posedge CK &&& shcheckCKD0lh === 1'b1,posedge D0,0,notifier);
	$hold(posedge CK &&& shcheckCKD1lh === 1'b1,negedge D1,0,notifier);
	$hold(posedge CK &&& shcheckCKD1lh === 1'b1,posedge D1,0,notifier);
	$hold(posedge CK &&& shcheckCKDSLlh === 1'b1,negedge DSL,0,notifier);
	$hold(posedge CK &&& shcheckCKDSLlh === 1'b1,posedge DSL,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,negedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,posedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,negedge SE,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,posedge SE,0,notifier);
	$setup(negedge CEB,posedge CK &&& shcheckCKCEBlh === 1'b1,0,notifier);
	$setup(negedge D0,posedge CK &&& shcheckCKD0lh === 1'b1,0,notifier);
	$setup(negedge D1,posedge CK &&& shcheckCKD1lh === 1'b1,0,notifier);
	$setup(negedge DSL,posedge CK &&& shcheckCKDSLlh === 1'b1,0,notifier);
	$setup(negedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(negedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$setup(posedge CEB,posedge CK &&& shcheckCKCEBlh === 1'b1,0,notifier);
	$setup(posedge D0,posedge CK &&& shcheckCKD0lh === 1'b1,0,notifier);
	$setup(posedge D1,posedge CK &&& shcheckCKD1lh === 1'b1,0,notifier);
	$setup(posedge DSL,posedge CK &&& shcheckCKDSLlh === 1'b1,0,notifier);
	$setup(posedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(posedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$width(negedge CK,1,0,notifier);
	$width(posedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       SDDFPQ1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:49 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Positive Edge, Scan Mux Data Flip-Flop; Q Output
// Q  = rising(CK) ?  (SE ? SD : (DSL ? D1 : D0)) : 'p'
module SDDFPQ1 (Q, CK, D0, D1, DSL, SD, SE);
	output Q;
	input  CK;
	input  D0;
	input  D1;
	input  DSL;
	input  SD;
	input  SE;
	reg notifier;
	p_mux21 _i0 (_n2, D1, D0, DSL);
	p_mux21 _i1 (_n1, SD, _n2, SE);
	p_ff _i2 (Q, _n1, CK, notifier);
`ifdef no_tchk
`else
	not _wi0 (_wn2,D0);
	not _wi1 (_wn5,D1);
	and _wi2 (_wn4,_wn5,SD);
	not _wi3 (_wn9,DSL);
	and _wi4 (_wn8,_wn9,SD);
	not _wi5 (_wn11,SD);
	and _wi6 (_wn10,DSL,_wn11);
	or _wi7 (_wn7,_wn8,_wn10);
	and _wi8 (_wn6,D1,_wn7);
	or _wi9 (_wn3,_wn4,_wn6);
	and _wi10 (_wn1,_wn2,_wn3);
	and _wi14 (_wn17,_wn9,_wn11);
	and _wi15 (_wn20,DSL,SD);
	or _wi16 (_wn16,_wn17,_wn20);
	and _wi17 (_wn14,_wn5,_wn16);
	and _wi19 (_wn21,D1,_wn11);
	or _wi20 (_wn13,_wn14,_wn21);
	and _wi21 (_wn12,D0,_wn13);
	or _wi22 (shcheckCKSElh,_wn1,_wn12);
	not _wi23 (_wn25,SE);
	and _wi25 (_wn24,_wn25,_wn2,D1);
	and _wi28 (_wn27,_wn25,D0,_wn5);
	or _wi29 (shcheckCKDSLlh,_wn24,_wn27);
	and _wi31 (shcheckCKD1lh,DSL,_wn25);
	buf _wi32 (shcheckCKSDlh,SE);
	and _wi35 (shcheckCKD0lh,_wn9,_wn25);
`endif
	specify		(CK => Q) = (1,1);
`ifdef no_tchk
`else
	$hold(posedge CK &&& shcheckCKD0lh === 1'b1,negedge D0,0,notifier);
	$hold(posedge CK &&& shcheckCKD0lh === 1'b1,posedge D0,0,notifier);
	$hold(posedge CK &&& shcheckCKD1lh === 1'b1,negedge D1,0,notifier);
	$hold(posedge CK &&& shcheckCKD1lh === 1'b1,posedge D1,0,notifier);
	$hold(posedge CK &&& shcheckCKDSLlh === 1'b1,negedge DSL,0,notifier);
	$hold(posedge CK &&& shcheckCKDSLlh === 1'b1,posedge DSL,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,negedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,posedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,negedge SE,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,posedge SE,0,notifier);
	$setup(negedge D0,posedge CK &&& shcheckCKD0lh === 1'b1,0,notifier);
	$setup(negedge D1,posedge CK &&& shcheckCKD1lh === 1'b1,0,notifier);
	$setup(negedge DSL,posedge CK &&& shcheckCKDSLlh === 1'b1,0,notifier);
	$setup(negedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(negedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$setup(posedge D0,posedge CK &&& shcheckCKD0lh === 1'b1,0,notifier);
	$setup(posedge D1,posedge CK &&& shcheckCKD1lh === 1'b1,0,notifier);
	$setup(posedge DSL,posedge CK &&& shcheckCKDSLlh === 1'b1,0,notifier);
	$setup(posedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(posedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$width(negedge CK,1,0,notifier);
	$width(posedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       SDDFPQ2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:50 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Positive Edge, Scan Mux Data Flip-Flop; Q Output
// Q  = rising(CK) ?  (SE ? SD : (DSL ? D1 : D0)) : 'p'
module SDDFPQ2 (Q, CK, D0, D1, DSL, SD, SE);
	output Q;
	input  CK;
	input  D0;
	input  D1;
	input  DSL;
	input  SD;
	input  SE;
	reg notifier;
	p_mux21 _i0 (_n2, D1, D0, DSL);
	p_mux21 _i1 (_n1, SD, _n2, SE);
	p_ff _i2 (Q, _n1, CK, notifier);
`ifdef no_tchk
`else
	not _wi0 (_wn2,D0);
	not _wi1 (_wn5,D1);
	and _wi2 (_wn4,_wn5,SD);
	not _wi3 (_wn9,DSL);
	and _wi4 (_wn8,_wn9,SD);
	not _wi5 (_wn11,SD);
	and _wi6 (_wn10,DSL,_wn11);
	or _wi7 (_wn7,_wn8,_wn10);
	and _wi8 (_wn6,D1,_wn7);
	or _wi9 (_wn3,_wn4,_wn6);
	and _wi10 (_wn1,_wn2,_wn3);
	and _wi14 (_wn17,_wn9,_wn11);
	and _wi15 (_wn20,DSL,SD);
	or _wi16 (_wn16,_wn17,_wn20);
	and _wi17 (_wn14,_wn5,_wn16);
	and _wi19 (_wn21,D1,_wn11);
	or _wi20 (_wn13,_wn14,_wn21);
	and _wi21 (_wn12,D0,_wn13);
	or _wi22 (shcheckCKSElh,_wn1,_wn12);
	not _wi23 (_wn25,SE);
	and _wi25 (_wn24,_wn25,_wn2,D1);
	and _wi28 (_wn27,_wn25,D0,_wn5);
	or _wi29 (shcheckCKDSLlh,_wn24,_wn27);
	and _wi31 (shcheckCKD1lh,DSL,_wn25);
	buf _wi32 (shcheckCKSDlh,SE);
	and _wi35 (shcheckCKD0lh,_wn9,_wn25);
`endif
	specify		(CK => Q) = (1,1);
`ifdef no_tchk
`else
	$hold(posedge CK &&& shcheckCKD0lh === 1'b1,negedge D0,0,notifier);
	$hold(posedge CK &&& shcheckCKD0lh === 1'b1,posedge D0,0,notifier);
	$hold(posedge CK &&& shcheckCKD1lh === 1'b1,negedge D1,0,notifier);
	$hold(posedge CK &&& shcheckCKD1lh === 1'b1,posedge D1,0,notifier);
	$hold(posedge CK &&& shcheckCKDSLlh === 1'b1,negedge DSL,0,notifier);
	$hold(posedge CK &&& shcheckCKDSLlh === 1'b1,posedge DSL,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,negedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,posedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,negedge SE,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,posedge SE,0,notifier);
	$setup(negedge D0,posedge CK &&& shcheckCKD0lh === 1'b1,0,notifier);
	$setup(negedge D1,posedge CK &&& shcheckCKD1lh === 1'b1,0,notifier);
	$setup(negedge DSL,posedge CK &&& shcheckCKDSLlh === 1'b1,0,notifier);
	$setup(negedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(negedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$setup(posedge D0,posedge CK &&& shcheckCKD0lh === 1'b1,0,notifier);
	$setup(posedge D1,posedge CK &&& shcheckCKD1lh === 1'b1,0,notifier);
	$setup(posedge DSL,posedge CK &&& shcheckCKDSLlh === 1'b1,0,notifier);
	$setup(posedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(posedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$width(negedge CK,1,0,notifier);
	$width(posedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       SDDFPQ4.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:48 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Positive Edge, Scan Mux Data Flip-Flop; Q Output
// Q  = rising(CK) ?  (SE ? SD : (DSL ? D1 : D0)) : 'p'
module SDDFPQ4 (Q, CK, D0, D1, DSL, SD, SE);
	output Q;
	input  CK;
	input  D0;
	input  D1;
	input  DSL;
	input  SD;
	input  SE;
	reg notifier;
	p_mux21 _i0 (_n2, D1, D0, DSL);
	p_mux21 _i1 (_n1, SD, _n2, SE);
	p_ff _i2 (Q, _n1, CK, notifier);
`ifdef no_tchk
`else
	not _wi0 (_wn2,D0);
	not _wi1 (_wn5,D1);
	and _wi2 (_wn4,_wn5,SD);
	not _wi3 (_wn9,DSL);
	and _wi4 (_wn8,_wn9,SD);
	not _wi5 (_wn11,SD);
	and _wi6 (_wn10,DSL,_wn11);
	or _wi7 (_wn7,_wn8,_wn10);
	and _wi8 (_wn6,D1,_wn7);
	or _wi9 (_wn3,_wn4,_wn6);
	and _wi10 (_wn1,_wn2,_wn3);
	and _wi14 (_wn17,_wn9,_wn11);
	and _wi15 (_wn20,DSL,SD);
	or _wi16 (_wn16,_wn17,_wn20);
	and _wi17 (_wn14,_wn5,_wn16);
	and _wi19 (_wn21,D1,_wn11);
	or _wi20 (_wn13,_wn14,_wn21);
	and _wi21 (_wn12,D0,_wn13);
	or _wi22 (shcheckCKSElh,_wn1,_wn12);
	not _wi23 (_wn25,SE);
	and _wi25 (_wn24,_wn25,_wn2,D1);
	and _wi28 (_wn27,_wn25,D0,_wn5);
	or _wi29 (shcheckCKDSLlh,_wn24,_wn27);
	and _wi31 (shcheckCKD1lh,DSL,_wn25);
	buf _wi32 (shcheckCKSDlh,SE);
	and _wi35 (shcheckCKD0lh,_wn9,_wn25);
`endif
	specify		(CK => Q) = (1,1);
`ifdef no_tchk
`else
	$hold(posedge CK &&& shcheckCKD0lh === 1'b1,negedge D0,0,notifier);
	$hold(posedge CK &&& shcheckCKD0lh === 1'b1,posedge D0,0,notifier);
	$hold(posedge CK &&& shcheckCKD1lh === 1'b1,negedge D1,0,notifier);
	$hold(posedge CK &&& shcheckCKD1lh === 1'b1,posedge D1,0,notifier);
	$hold(posedge CK &&& shcheckCKDSLlh === 1'b1,negedge DSL,0,notifier);
	$hold(posedge CK &&& shcheckCKDSLlh === 1'b1,posedge DSL,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,negedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,posedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,negedge SE,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,posedge SE,0,notifier);
	$setup(negedge D0,posedge CK &&& shcheckCKD0lh === 1'b1,0,notifier);
	$setup(negedge D1,posedge CK &&& shcheckCKD1lh === 1'b1,0,notifier);
	$setup(negedge DSL,posedge CK &&& shcheckCKDSLlh === 1'b1,0,notifier);
	$setup(negedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(negedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$setup(posedge D0,posedge CK &&& shcheckCKD0lh === 1'b1,0,notifier);
	$setup(posedge D1,posedge CK &&& shcheckCKD1lh === 1'b1,0,notifier);
	$setup(posedge DSL,posedge CK &&& shcheckCKDSLlh === 1'b1,0,notifier);
	$setup(posedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(posedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$width(negedge CK,1,0,notifier);
	$width(posedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       SDEPQ1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:51 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Enabled, Positive Edge, Scan D Flip-Flop; Q Output
// Q  = rising(CK) ? (SE ? SD : (CEB ? 's' : D)) : 'p'
module SDEPQ1 (Q, CEB, CK, D, SD, SE);
	output Q;
	input  CEB;
	input  CK;
	input  D;
	input  SD;
	input  SE;
	reg notifier;
	p_mux21 _i0 (_n2, buf_Q, D, CEB);
	p_mux21 _i1 (_n1, SD, _n2, SE);
	p_ff _i2 (buf_Q, _n1, CK, notifier);
	buf _iQ(Q, buf_Q);
`ifdef no_tchk
`else
	not _wi0 (_wn2,CEB);
	not _wi1 (_wn5,D);
	and _wi2 (_wn4,_wn5,SD);
	not _wi3 (_wn7,SD);
	and _wi4 (_wn6,D,_wn7);
	or _wi5 (_wn3,_wn4,_wn6);
	and _wi6 (_wn1,_wn2,_wn3);
	or _wi7 (shcheckCKSElh,_wn1,CEB);
	not _wi9 (_wn10,SE);
	and _wi10 (shcheckCKDlh,_wn2,_wn10);
	buf _wi11 (shcheckCKSDlh,SE);
	not _wi12 (shcheckCKCEBlh,SE);
`endif
	specify		(CK => Q) = (1,1);
`ifdef no_tchk
`else
	$hold(posedge CK &&& shcheckCKCEBlh === 1'b1,negedge CEB,0,notifier);
	$hold(posedge CK &&& shcheckCKCEBlh === 1'b1,posedge CEB,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,negedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,posedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,negedge SE,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,posedge SE,0,notifier);
	$setup(negedge CEB,posedge CK &&& shcheckCKCEBlh === 1'b1,0,notifier);
	$setup(negedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(negedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(negedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$setup(posedge CEB,posedge CK &&& shcheckCKCEBlh === 1'b1,0,notifier);
	$setup(posedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(posedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$width(negedge CK,1,0,notifier);
	$width(posedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       SDEPQ2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:48 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Enabled, Positive Edge, Scan D Flip-Flop; Q Output
// Q  = rising(CK) ? (SE ? SD : (CEB ? 's' : D)) : 'p'
module SDEPQ2 (Q, CEB, CK, D, SD, SE);
	output Q;
	input  CEB;
	input  CK;
	input  D;
	input  SD;
	input  SE;
	reg notifier;
	p_mux21 _i0 (_n2, buf_Q, D, CEB);
	p_mux21 _i1 (_n1, SD, _n2, SE);
	p_ff _i2 (buf_Q, _n1, CK, notifier);
	buf _iQ(Q, buf_Q);
`ifdef no_tchk
`else
	not _wi0 (_wn2,CEB);
	not _wi1 (_wn5,D);
	and _wi2 (_wn4,_wn5,SD);
	not _wi3 (_wn7,SD);
	and _wi4 (_wn6,D,_wn7);
	or _wi5 (_wn3,_wn4,_wn6);
	and _wi6 (_wn1,_wn2,_wn3);
	or _wi7 (shcheckCKSElh,_wn1,CEB);
	not _wi9 (_wn10,SE);
	and _wi10 (shcheckCKDlh,_wn2,_wn10);
	buf _wi11 (shcheckCKSDlh,SE);
	not _wi12 (shcheckCKCEBlh,SE);
`endif
	specify		(CK => Q) = (1,1);
`ifdef no_tchk
`else
	$hold(posedge CK &&& shcheckCKCEBlh === 1'b1,negedge CEB,0,notifier);
	$hold(posedge CK &&& shcheckCKCEBlh === 1'b1,posedge CEB,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,negedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,posedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,negedge SE,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,posedge SE,0,notifier);
	$setup(negedge CEB,posedge CK &&& shcheckCKCEBlh === 1'b1,0,notifier);
	$setup(negedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(negedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(negedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$setup(posedge CEB,posedge CK &&& shcheckCKCEBlh === 1'b1,0,notifier);
	$setup(posedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(posedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$width(negedge CK,1,0,notifier);
	$width(posedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       SDEPQ4.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:48 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Enabled, Positive Edge, Scan D Flip-Flop; Q Output
// Q  = rising(CK) ? (SE ? SD : (CEB ? 's' : D)) : 'p'
module SDEPQ4 (Q, CEB, CK, D, SD, SE);
	output Q;
	input  CEB;
	input  CK;
	input  D;
	input  SD;
	input  SE;
	reg notifier;
	p_mux21 _i0 (_n2, buf_Q, D, CEB);
	p_mux21 _i1 (_n1, SD, _n2, SE);
	p_ff _i2 (buf_Q, _n1, CK, notifier);
	buf _iQ(Q, buf_Q);
`ifdef no_tchk
`else
	not _wi0 (_wn2,CEB);
	not _wi1 (_wn5,D);
	and _wi2 (_wn4,_wn5,SD);
	not _wi3 (_wn7,SD);
	and _wi4 (_wn6,D,_wn7);
	or _wi5 (_wn3,_wn4,_wn6);
	and _wi6 (_wn1,_wn2,_wn3);
	or _wi7 (shcheckCKSElh,_wn1,CEB);
	not _wi9 (_wn10,SE);
	and _wi10 (shcheckCKDlh,_wn2,_wn10);
	buf _wi11 (shcheckCKSDlh,SE);
	not _wi12 (shcheckCKCEBlh,SE);
`endif
	specify		(CK => Q) = (1,1);
`ifdef no_tchk
`else
	$hold(posedge CK &&& shcheckCKCEBlh === 1'b1,negedge CEB,0,notifier);
	$hold(posedge CK &&& shcheckCKCEBlh === 1'b1,posedge CEB,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,negedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,posedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,negedge SE,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,posedge SE,0,notifier);
	$setup(negedge CEB,posedge CK &&& shcheckCKCEBlh === 1'b1,0,notifier);
	$setup(negedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(negedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(negedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$setup(posedge CEB,posedge CK &&& shcheckCKCEBlh === 1'b1,0,notifier);
	$setup(posedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(posedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$width(negedge CK,1,0,notifier);
	$width(posedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       SDERPB1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:41 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Enabled, Positive Edge, Scan D Flip-Flop with Clear; Q, QB Outputs
// Q  = !RB ? 0 : rising(CK) ? (SE ? SD : (CEB ? 's' : D)) : 'p';QB = !Q
module SDERPB1 (Q, QB, CEB, CK, D, RB, SD, SE);
	output Q;
	output QB;
	input  CEB;
	input  CK;
	input  D;
	input  RB;
	input  SD;
	input  SE;
	reg notifier;
	p_mux21 _i0 (_n2, buf_Q, D, CEB);
	p_mux21 _i1 (_n1, SD, _n2, SE);
	not _i2 (_n3,RB);
	p_ffr _i3 (buf_Q, _n1, CK, _n3, notifier);
	not _i4 (QB,buf_Q);
	buf _iQ(Q, buf_Q);
`ifdef no_tchk
`else
	not _wi0 (_wn2,CEB);
	not _wi1 (_wn5,D);
	and _wi2 (_wn4,SD,_wn5,RB);
	not _wi3 (_wn7,SD);
	and _wi4 (_wn6,_wn7,D,RB);
	or _wi5 (_wn3,_wn4,_wn6);
	and _wi6 (_wn1,_wn2,_wn3);
	and _wi7 (_wn8,CEB,RB);
	or _wi8 (shcheckCKSElh,_wn1,_wn8);
	not _wi9 (_wn10,SE);
	and _wi11 (shcheckCKDlh,_wn10,_wn2,RB);
	and _wi14 (_wn16,SE,_wn5,SD);
	and _wi17 (_wn20,_wn7,_wn10);
	or _wi18 (_wn19,_wn20,SD);
	and _wi19 (_wn18,D,_wn19);
	or _wi20 (_wn15,_wn16,_wn18);
	and _wi21 (_wn13,_wn2,_wn15);
	and _wi22 (_wn23,SE,CEB,SD);
	or _wi23 (shcheckCKRBlh,_wn13,_wn23);
	and _wi24 (shcheckCKSDlh,RB,SE);
	and _wi26 (shcheckCKCEBlh,RB,_wn10);
`endif
	specify		(CK => Q) = (1,1);
	(CK => QB) = (1,1);
	(RB => Q) = (0,0);
	(RB => QB) = (0,0);
`ifdef no_tchk
`else
	$hold(posedge CK &&& shcheckCKCEBlh === 1'b1,negedge CEB,0,notifier);
	$hold(posedge CK &&& shcheckCKCEBlh === 1'b1,posedge CEB,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,negedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,posedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,negedge SE,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,posedge SE,0,notifier);
	$hold(posedge CK,posedge RB,0,notifier);
	$setup(negedge CEB,posedge CK &&& shcheckCKCEBlh === 1'b1,0,notifier);
	$setup(negedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(negedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(negedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$setup(posedge CEB,posedge CK &&& shcheckCKCEBlh === 1'b1,0,notifier);
	$setup(posedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge CK &&& shcheckCKRBlh === 1'b1,0,notifier);
	$setup(posedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(posedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$width(negedge CK,1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(posedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       SDERPB2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:44 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Enabled, Positive Edge, Scan D Flip-Flop with Clear; Q, QB Outputs
// Q  = !RB ? 0 : rising(CK) ? (SE ? SD : (CEB ? 's' : D)) : 'p';QB = !Q
module SDERPB2 (Q, QB, CEB, CK, D, RB, SD, SE);
	output Q;
	output QB;
	input  CEB;
	input  CK;
	input  D;
	input  RB;
	input  SD;
	input  SE;
	reg notifier;
	p_mux21 _i0 (_n2, buf_Q, D, CEB);
	p_mux21 _i1 (_n1, SD, _n2, SE);
	not _i2 (_n3,RB);
	p_ffr _i3 (buf_Q, _n1, CK, _n3, notifier);
	not _i4 (QB,buf_Q);
	buf _iQ(Q, buf_Q);
`ifdef no_tchk
`else
	not _wi0 (_wn2,CEB);
	not _wi1 (_wn5,D);
	and _wi2 (_wn4,SD,_wn5,RB);
	not _wi3 (_wn7,SD);
	and _wi4 (_wn6,_wn7,D,RB);
	or _wi5 (_wn3,_wn4,_wn6);
	and _wi6 (_wn1,_wn2,_wn3);
	and _wi7 (_wn8,CEB,RB);
	or _wi8 (shcheckCKSElh,_wn1,_wn8);
	not _wi9 (_wn10,SE);
	and _wi11 (shcheckCKDlh,_wn10,_wn2,RB);
	and _wi14 (_wn16,SE,_wn5,SD);
	and _wi17 (_wn20,_wn7,_wn10);
	or _wi18 (_wn19,_wn20,SD);
	and _wi19 (_wn18,D,_wn19);
	or _wi20 (_wn15,_wn16,_wn18);
	and _wi21 (_wn13,_wn2,_wn15);
	and _wi22 (_wn23,SE,CEB,SD);
	or _wi23 (shcheckCKRBlh,_wn13,_wn23);
	and _wi24 (shcheckCKSDlh,RB,SE);
	and _wi26 (shcheckCKCEBlh,RB,_wn10);
`endif
	specify		(CK => Q) = (1,1);
	(CK => QB) = (1,1);
	(RB => Q) = (0,0);
	(RB => QB) = (0,0);
`ifdef no_tchk
`else
	$hold(posedge CK &&& shcheckCKCEBlh === 1'b1,negedge CEB,0,notifier);
	$hold(posedge CK &&& shcheckCKCEBlh === 1'b1,posedge CEB,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,negedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,posedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,negedge SE,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,posedge SE,0,notifier);
	$hold(posedge CK,posedge RB,0,notifier);
	$setup(negedge CEB,posedge CK &&& shcheckCKCEBlh === 1'b1,0,notifier);
	$setup(negedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(negedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(negedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$setup(posedge CEB,posedge CK &&& shcheckCKCEBlh === 1'b1,0,notifier);
	$setup(posedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge CK &&& shcheckCKRBlh === 1'b1,0,notifier);
	$setup(posedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(posedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$width(negedge CK,1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(posedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       SDERPQ1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:43 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Enabled, Positive Edge, Scan D Flip-Flop with Clear; Q Output
// Q  = !RB ? 0 : rising(CK) ? (SE ? SD : (CEB ? 's' : D)) : 'p'
module SDERPQ1 (Q, CEB, CK, D, RB, SD, SE);
	output Q;
	input  CEB;
	input  CK;
	input  D;
	input  RB;
	input  SD;
	input  SE;
	reg notifier;
	p_mux21 _i0 (_n2, buf_Q, D, CEB);
	p_mux21 _i1 (_n1, SD, _n2, SE);
	not _i2 (_n3,RB);
	p_ffr _i3 (buf_Q, _n1, CK, _n3, notifier);
	buf _iQ(Q, buf_Q);
`ifdef no_tchk
`else
	not _wi0 (_wn2,CEB);
	not _wi1 (_wn5,D);
	and _wi2 (_wn4,SD,_wn5,RB);
	not _wi3 (_wn7,SD);
	and _wi4 (_wn6,_wn7,D,RB);
	or _wi5 (_wn3,_wn4,_wn6);
	and _wi6 (_wn1,_wn2,_wn3);
	and _wi7 (_wn8,CEB,RB);
	or _wi8 (shcheckCKSElh,_wn1,_wn8);
	not _wi9 (_wn10,SE);
	and _wi11 (shcheckCKDlh,_wn10,_wn2,RB);
	and _wi14 (_wn16,SE,_wn5,SD);
	and _wi17 (_wn20,_wn7,_wn10);
	or _wi18 (_wn19,_wn20,SD);
	and _wi19 (_wn18,D,_wn19);
	or _wi20 (_wn15,_wn16,_wn18);
	and _wi21 (_wn13,_wn2,_wn15);
	and _wi22 (_wn23,SE,CEB,SD);
	or _wi23 (shcheckCKRBlh,_wn13,_wn23);
	and _wi24 (shcheckCKSDlh,RB,SE);
	and _wi26 (shcheckCKCEBlh,RB,_wn10);
`endif
	specify		(CK => Q) = (1,1);
	(RB => Q) = (0,0);
`ifdef no_tchk
`else
	$hold(posedge CK &&& shcheckCKCEBlh === 1'b1,negedge CEB,0,notifier);
	$hold(posedge CK &&& shcheckCKCEBlh === 1'b1,posedge CEB,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,negedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,posedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,negedge SE,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,posedge SE,0,notifier);
	$hold(posedge CK,posedge RB,0,notifier);
	$setup(negedge CEB,posedge CK &&& shcheckCKCEBlh === 1'b1,0,notifier);
	$setup(negedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(negedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(negedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$setup(posedge CEB,posedge CK &&& shcheckCKCEBlh === 1'b1,0,notifier);
	$setup(posedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge CK &&& shcheckCKRBlh === 1'b1,0,notifier);
	$setup(posedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(posedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$width(negedge CK,1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(posedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       SDERPQ2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:38 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Enabled, Positive Edge, Scan D Flip-Flop with Clear; Q Output
// Q  = !RB ? 0 : rising(CK) ? (SE ? SD : (CEB ? 's' : D)) : 'p'
module SDERPQ2 (Q, CEB, CK, D, RB, SD, SE);
	output Q;
	input  CEB;
	input  CK;
	input  D;
	input  RB;
	input  SD;
	input  SE;
	reg notifier;
	p_mux21 _i0 (_n2, buf_Q, D, CEB);
	p_mux21 _i1 (_n1, SD, _n2, SE);
	not _i2 (_n3,RB);
	p_ffr _i3 (buf_Q, _n1, CK, _n3, notifier);
	buf _iQ(Q, buf_Q);
`ifdef no_tchk
`else
	not _wi0 (_wn2,CEB);
	not _wi1 (_wn5,D);
	and _wi2 (_wn4,SD,_wn5,RB);
	not _wi3 (_wn7,SD);
	and _wi4 (_wn6,_wn7,D,RB);
	or _wi5 (_wn3,_wn4,_wn6);
	and _wi6 (_wn1,_wn2,_wn3);
	and _wi7 (_wn8,CEB,RB);
	or _wi8 (shcheckCKSElh,_wn1,_wn8);
	not _wi9 (_wn10,SE);
	and _wi11 (shcheckCKDlh,_wn10,_wn2,RB);
	and _wi14 (_wn16,SE,_wn5,SD);
	and _wi17 (_wn20,_wn7,_wn10);
	or _wi18 (_wn19,_wn20,SD);
	and _wi19 (_wn18,D,_wn19);
	or _wi20 (_wn15,_wn16,_wn18);
	and _wi21 (_wn13,_wn2,_wn15);
	and _wi22 (_wn23,SE,CEB,SD);
	or _wi23 (shcheckCKRBlh,_wn13,_wn23);
	and _wi24 (shcheckCKSDlh,RB,SE);
	and _wi26 (shcheckCKCEBlh,RB,_wn10);
`endif
	specify		(CK => Q) = (1,1);
	(RB => Q) = (0,0);
`ifdef no_tchk
`else
	$hold(posedge CK &&& shcheckCKCEBlh === 1'b1,negedge CEB,0,notifier);
	$hold(posedge CK &&& shcheckCKCEBlh === 1'b1,posedge CEB,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,negedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,posedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,negedge SE,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,posedge SE,0,notifier);
	$hold(posedge CK,posedge RB,0,notifier);
	$setup(negedge CEB,posedge CK &&& shcheckCKCEBlh === 1'b1,0,notifier);
	$setup(negedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(negedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(negedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$setup(posedge CEB,posedge CK &&& shcheckCKCEBlh === 1'b1,0,notifier);
	$setup(posedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge CK &&& shcheckCKRBlh === 1'b1,0,notifier);
	$setup(posedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(posedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$width(negedge CK,1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(posedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       SDERSNB1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:38 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Enabled, Negative Edge, Scan D Flip-Flop with Set, Clear; Q, QB Outputs
// Q  = !RB ? 0 : !SB ? 1 : falling(CKB) ? (SE ? SD : (CEB ? 's' : D)) : 'p';QB = !Q & SB
module SDERSNB1 (Q, QB, CEB, CKB, D, RB, SB, SD, SE);
	output Q;
	output QB;
	input  CEB;
	input  CKB;
	input  D;
	input  RB;
	input  SB;
	input  SD;
	input  SE;
	reg notifier;
	not _i0 (_n1,CKB);
	p_mux21 _i1 (_n3, buf_Q, D, CEB);
	p_mux21 _i2 (_n2, SD, _n3, SE);
	not _i3 (_n4,SB);
	not _i4 (_n5,RB);
	p_ffrs _i5 (buf_Q, _n2, _n1, _n5, _n4, notifier);
	not _i6 (_n7,buf_Q);
	and _i7 (QB,_n7,SB);
	buf _iQ(Q, buf_Q);
`ifdef no_tchk
`else
	not _wi0 (_wn2,CEB);
	not _wi1 (_wn5,D);
	and _wi2 (_wn4,SE,SD,_wn5,SB);
	not _wi3 (_wn9,SD);
	not _wi4 (_wn10,SE);
	and _wi5 (_wn8,_wn9,_wn10);
	or _wi6 (_wn7,_wn8,SD);
	and _wi7 (_wn6,D,SB,_wn7);
	or _wi8 (_wn3,_wn4,_wn6);
	and _wi9 (_wn1,_wn2,_wn3);
	and _wi10 (_wn11,SE,SD,CEB,SB);
	or _wi11 (shcheckCKBRBhl,_wn1,_wn11);
	and _wi15 (_wn19,SD,_wn10);
	or _wi17 (_wn18,_wn19,_wn9);
	and _wi18 (_wn16,_wn5,RB,_wn18);
	and _wi20 (_wn22,SE,_wn9,D,RB);
	or _wi21 (_wn15,_wn16,_wn22);
	and _wi22 (_wn13,_wn2,_wn15);
	and _wi24 (_wn24,SE,_wn9,CEB,RB);
	or _wi25 (shcheckCKBSBhl,_wn13,_wn24);
	and _wi26 (shcheckCKBSDhl,SE,RB,SB);
	and _wi29 (shcheckCKBDhl,_wn10,SB,_wn2,RB);
	and _wi32 (_wn34,SD,SB,_wn5,RB);
	and _wi34 (_wn36,_wn9,SB,D,RB);
	or _wi35 (_wn33,_wn34,_wn36);
	and _wi36 (_wn31,_wn2,_wn33);
	and _wi37 (_wn38,SB,CEB,RB);
	or _wi38 (shcheckCKBSEhl,_wn31,_wn38);
	and _wi40 (shcheckCKBCEBhl,_wn10,RB,SB);
`endif
	specify		(CKB => Q) = (1,1);
	(CKB => QB) = (1,1);
	(RB => Q) = (0,0);
	(RB => QB) = (0,0);
	(SB => Q) = (0,0);
	(SB => QB) = (0,0);
`ifdef no_tchk
`else
	$hold(negedge CKB &&& shcheckCKBCEBhl === 1'b1,negedge CEB,0,notifier);
	$hold(negedge CKB &&& shcheckCKBCEBhl === 1'b1,posedge CEB,0,notifier);
	$hold(negedge CKB &&& shcheckCKBDhl === 1'b1,negedge D,0,notifier);
	$hold(negedge CKB &&& shcheckCKBDhl === 1'b1,posedge D,0,notifier);
	$hold(negedge CKB &&& shcheckCKBSDhl === 1'b1,negedge SD,0,notifier);
	$hold(negedge CKB &&& shcheckCKBSDhl === 1'b1,posedge SD,0,notifier);
	$hold(negedge CKB &&& shcheckCKBSEhl === 1'b1,negedge SE,0,notifier);
	$hold(negedge CKB &&& shcheckCKBSEhl === 1'b1,posedge SE,0,notifier);
	$hold(negedge CKB,posedge RB,0,notifier);
	$hold(negedge CKB,posedge SB,0,notifier);
	$hold(posedge SB,posedge RB,0,notifier);
	$setup(negedge CEB,negedge CKB &&& shcheckCKBCEBhl === 1'b1,0,notifier);
	$setup(negedge D,negedge CKB &&& shcheckCKBDhl === 1'b1,0,notifier);
	$setup(negedge SD,negedge CKB &&& shcheckCKBSDhl === 1'b1,0,notifier);
	$setup(negedge SE,negedge CKB &&& shcheckCKBSEhl === 1'b1,0,notifier);
	$setup(posedge CEB,negedge CKB &&& shcheckCKBCEBhl === 1'b1,0,notifier);
	$setup(posedge D,negedge CKB &&& shcheckCKBDhl === 1'b1,0,notifier);
	$setup(posedge RB,negedge CKB &&& shcheckCKBRBhl === 1'b1,0,notifier);
	$setup(posedge RB,posedge SB,0,notifier);
	$setup(posedge SB,negedge CKB &&& shcheckCKBSBhl === 1'b1,0,notifier);
	$setup(posedge SD,negedge CKB &&& shcheckCKBSDhl === 1'b1,0,notifier);
	$setup(posedge SE,negedge CKB &&& shcheckCKBSEhl === 1'b1,0,notifier);
	$width(negedge CKB,1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(negedge SB,1,0,notifier);
	$width(posedge CKB,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       SDERSNB2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:35 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Enabled, Negative Edge, Scan D Flip-Flop with Set, Clear; Q, QB Outputs
// Q  = !RB ? 0 : !SB ? 1 : falling(CKB) ? (SE ? SD : (CEB ? 's' : D)) : 'p';QB = !Q & SB
module SDERSNB2 (Q, QB, CEB, CKB, D, RB, SB, SD, SE);
	output Q;
	output QB;
	input  CEB;
	input  CKB;
	input  D;
	input  RB;
	input  SB;
	input  SD;
	input  SE;
	reg notifier;
	not _i0 (_n1,CKB);
	p_mux21 _i1 (_n3, buf_Q, D, CEB);
	p_mux21 _i2 (_n2, SD, _n3, SE);
	not _i3 (_n4,SB);
	not _i4 (_n5,RB);
	p_ffrs _i5 (buf_Q, _n2, _n1, _n5, _n4, notifier);
	not _i6 (_n7,buf_Q);
	and _i7 (QB,_n7,SB);
	buf _iQ(Q, buf_Q);
`ifdef no_tchk
`else
	not _wi0 (_wn2,CEB);
	not _wi1 (_wn5,D);
	and _wi2 (_wn4,SE,SD,_wn5,SB);
	not _wi3 (_wn9,SD);
	not _wi4 (_wn10,SE);
	and _wi5 (_wn8,_wn9,_wn10);
	or _wi6 (_wn7,_wn8,SD);
	and _wi7 (_wn6,D,SB,_wn7);
	or _wi8 (_wn3,_wn4,_wn6);
	and _wi9 (_wn1,_wn2,_wn3);
	and _wi10 (_wn11,SE,SD,CEB,SB);
	or _wi11 (shcheckCKBRBhl,_wn1,_wn11);
	and _wi15 (_wn19,SD,_wn10);
	or _wi17 (_wn18,_wn19,_wn9);
	and _wi18 (_wn16,_wn5,RB,_wn18);
	and _wi20 (_wn22,SE,_wn9,D,RB);
	or _wi21 (_wn15,_wn16,_wn22);
	and _wi22 (_wn13,_wn2,_wn15);
	and _wi24 (_wn24,SE,_wn9,CEB,RB);
	or _wi25 (shcheckCKBSBhl,_wn13,_wn24);
	and _wi26 (shcheckCKBSDhl,SE,RB,SB);
	and _wi29 (shcheckCKBDhl,_wn10,SB,_wn2,RB);
	and _wi32 (_wn34,SD,SB,_wn5,RB);
	and _wi34 (_wn36,_wn9,SB,D,RB);
	or _wi35 (_wn33,_wn34,_wn36);
	and _wi36 (_wn31,_wn2,_wn33);
	and _wi37 (_wn38,SB,CEB,RB);
	or _wi38 (shcheckCKBSEhl,_wn31,_wn38);
	and _wi40 (shcheckCKBCEBhl,_wn10,RB,SB);
`endif
	specify		(CKB => Q) = (1,1);
	(CKB => QB) = (1,1);
	(RB => Q) = (0,0);
	(RB => QB) = (0,0);
	(SB => Q) = (0,0);
	(SB => QB) = (0,0);
`ifdef no_tchk
`else
	$hold(negedge CKB &&& shcheckCKBCEBhl === 1'b1,negedge CEB,0,notifier);
	$hold(negedge CKB &&& shcheckCKBCEBhl === 1'b1,posedge CEB,0,notifier);
	$hold(negedge CKB &&& shcheckCKBDhl === 1'b1,negedge D,0,notifier);
	$hold(negedge CKB &&& shcheckCKBDhl === 1'b1,posedge D,0,notifier);
	$hold(negedge CKB &&& shcheckCKBSDhl === 1'b1,negedge SD,0,notifier);
	$hold(negedge CKB &&& shcheckCKBSDhl === 1'b1,posedge SD,0,notifier);
	$hold(negedge CKB &&& shcheckCKBSEhl === 1'b1,negedge SE,0,notifier);
	$hold(negedge CKB &&& shcheckCKBSEhl === 1'b1,posedge SE,0,notifier);
	$hold(negedge CKB,posedge RB,0,notifier);
	$hold(negedge CKB,posedge SB,0,notifier);
	$hold(posedge SB,posedge RB,0,notifier);
	$setup(negedge CEB,negedge CKB &&& shcheckCKBCEBhl === 1'b1,0,notifier);
	$setup(negedge D,negedge CKB &&& shcheckCKBDhl === 1'b1,0,notifier);
	$setup(negedge SD,negedge CKB &&& shcheckCKBSDhl === 1'b1,0,notifier);
	$setup(negedge SE,negedge CKB &&& shcheckCKBSEhl === 1'b1,0,notifier);
	$setup(posedge CEB,negedge CKB &&& shcheckCKBCEBhl === 1'b1,0,notifier);
	$setup(posedge D,negedge CKB &&& shcheckCKBDhl === 1'b1,0,notifier);
	$setup(posedge RB,negedge CKB &&& shcheckCKBRBhl === 1'b1,0,notifier);
	$setup(posedge RB,posedge SB,0,notifier);
	$setup(posedge SB,negedge CKB &&& shcheckCKBSBhl === 1'b1,0,notifier);
	$setup(posedge SD,negedge CKB &&& shcheckCKBSDhl === 1'b1,0,notifier);
	$setup(posedge SE,negedge CKB &&& shcheckCKBSEhl === 1'b1,0,notifier);
	$width(negedge CKB,1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(negedge SB,1,0,notifier);
	$width(posedge CKB,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       SDERSPB1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:35 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Enabled, Positive Edge, Scan D Flip-Flop with Set, Clear; Q, QB Outputs
// Q  = !RB ? 0 : !SB ? 1 : rising(CK) ? (SE ? SD : (CEB ? 's' : D)) : 'p';QB = !Q & SB
module SDERSPB1 (Q, QB, CEB, CK, D, RB, SB, SD, SE);
	output Q;
	output QB;
	input  CEB;
	input  CK;
	input  D;
	input  RB;
	input  SB;
	input  SD;
	input  SE;
	reg notifier;
	p_mux21 _i0 (_n2, buf_Q, D, CEB);
	p_mux21 _i1 (_n1, SD, _n2, SE);
	not _i2 (_n3,SB);
	not _i3 (_n4,RB);
	p_ffrs _i4 (buf_Q, _n1, CK, _n4, _n3, notifier);
	not _i5 (_n6,buf_Q);
	and _i6 (QB,_n6,SB);
	buf _iQ(Q, buf_Q);
`ifdef no_tchk
`else
	not _wi0 (_wn2,CEB);
	not _wi1 (_wn5,D);
	and _wi2 (_wn4,SD,SB,_wn5,RB);
	not _wi3 (_wn7,SD);
	and _wi4 (_wn6,_wn7,SB,D,RB);
	or _wi5 (_wn3,_wn4,_wn6);
	and _wi6 (_wn1,_wn2,_wn3);
	and _wi7 (_wn8,SB,CEB,RB);
	or _wi8 (shcheckCKSElh,_wn1,_wn8);
	not _wi11 (_wn17,SE);
	and _wi12 (_wn16,SD,_wn17);
	or _wi14 (_wn15,_wn16,_wn7);
	and _wi15 (_wn13,_wn5,RB,_wn15);
	and _wi17 (_wn19,SE,_wn7,D,RB);
	or _wi18 (_wn12,_wn13,_wn19);
	and _wi19 (_wn10,_wn2,_wn12);
	and _wi21 (_wn21,SE,_wn7,CEB,RB);
	or _wi22 (shcheckCKSBlh,_wn10,_wn21);
	and _wi25 (_wn27,SE,SD,_wn5,SB);
	and _wi28 (_wn31,_wn7,_wn17);
	or _wi29 (_wn30,_wn31,SD);
	and _wi30 (_wn29,D,SB,_wn30);
	or _wi31 (_wn26,_wn27,_wn29);
	and _wi32 (_wn24,_wn2,_wn26);
	and _wi33 (_wn34,SE,SD,CEB,SB);
	or _wi34 (shcheckCKRBlh,_wn24,_wn34);
	and _wi37 (shcheckCKDlh,_wn17,SB,_wn2,RB);
	and _wi38 (shcheckCKSDlh,SE,RB,SB);
	and _wi40 (shcheckCKCEBlh,_wn17,RB,SB);
`endif
	specify		(CK => Q) = (1,1);
	(CK => QB) = (1,1);
	(RB => Q) = (0,0);
	(RB => QB) = (0,0);
	(SB => Q) = (0,0);
	(SB => QB) = (0,0);
`ifdef no_tchk
`else
	$hold(posedge CK &&& shcheckCKCEBlh === 1'b1,negedge CEB,0,notifier);
	$hold(posedge CK &&& shcheckCKCEBlh === 1'b1,posedge CEB,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,negedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,posedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,negedge SE,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,posedge SE,0,notifier);
	$hold(posedge CK,posedge RB,0,notifier);
	$hold(posedge CK,posedge SB,0,notifier);
	$hold(posedge SB,posedge RB,0,notifier);
	$setup(negedge CEB,posedge CK &&& shcheckCKCEBlh === 1'b1,0,notifier);
	$setup(negedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(negedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(negedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$setup(posedge CEB,posedge CK &&& shcheckCKCEBlh === 1'b1,0,notifier);
	$setup(posedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge CK &&& shcheckCKRBlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge SB,0,notifier);
	$setup(posedge SB,posedge CK &&& shcheckCKSBlh === 1'b1,0,notifier);
	$setup(posedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(posedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$width(negedge CK,1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(negedge SB,1,0,notifier);
	$width(posedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       SDERSPB2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:35 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Enabled, Positive Edge, Scan D Flip-Flop with Set, Clear; Q, QB Outputs
// Q  = !RB ? 0 : !SB ? 1 : rising(CK) ? (SE ? SD : (CEB ? 's' : D)) : 'p';QB = !Q & SB
module SDERSPB2 (Q, QB, CEB, CK, D, RB, SB, SD, SE);
	output Q;
	output QB;
	input  CEB;
	input  CK;
	input  D;
	input  RB;
	input  SB;
	input  SD;
	input  SE;
	reg notifier;
	p_mux21 _i0 (_n2, buf_Q, D, CEB);
	p_mux21 _i1 (_n1, SD, _n2, SE);
	not _i2 (_n3,SB);
	not _i3 (_n4,RB);
	p_ffrs _i4 (buf_Q, _n1, CK, _n4, _n3, notifier);
	not _i5 (_n6,buf_Q);
	and _i6 (QB,_n6,SB);
	buf _iQ(Q, buf_Q);
`ifdef no_tchk
`else
	not _wi0 (_wn2,CEB);
	not _wi1 (_wn5,D);
	and _wi2 (_wn4,SD,SB,_wn5,RB);
	not _wi3 (_wn7,SD);
	and _wi4 (_wn6,_wn7,SB,D,RB);
	or _wi5 (_wn3,_wn4,_wn6);
	and _wi6 (_wn1,_wn2,_wn3);
	and _wi7 (_wn8,SB,CEB,RB);
	or _wi8 (shcheckCKSElh,_wn1,_wn8);
	not _wi11 (_wn17,SE);
	and _wi12 (_wn16,SD,_wn17);
	or _wi14 (_wn15,_wn16,_wn7);
	and _wi15 (_wn13,_wn5,RB,_wn15);
	and _wi17 (_wn19,SE,_wn7,D,RB);
	or _wi18 (_wn12,_wn13,_wn19);
	and _wi19 (_wn10,_wn2,_wn12);
	and _wi21 (_wn21,SE,_wn7,CEB,RB);
	or _wi22 (shcheckCKSBlh,_wn10,_wn21);
	and _wi25 (_wn27,SE,SD,_wn5,SB);
	and _wi28 (_wn31,_wn7,_wn17);
	or _wi29 (_wn30,_wn31,SD);
	and _wi30 (_wn29,D,SB,_wn30);
	or _wi31 (_wn26,_wn27,_wn29);
	and _wi32 (_wn24,_wn2,_wn26);
	and _wi33 (_wn34,SE,SD,CEB,SB);
	or _wi34 (shcheckCKRBlh,_wn24,_wn34);
	and _wi37 (shcheckCKDlh,_wn17,SB,_wn2,RB);
	and _wi38 (shcheckCKSDlh,SE,RB,SB);
	and _wi40 (shcheckCKCEBlh,_wn17,RB,SB);
`endif
	specify		(CK => Q) = (1,1);
	(CK => QB) = (1,1);
	(RB => Q) = (0,0);
	(RB => QB) = (0,0);
	(SB => Q) = (0,0);
	(SB => QB) = (0,0);
`ifdef no_tchk
`else
	$hold(posedge CK &&& shcheckCKCEBlh === 1'b1,negedge CEB,0,notifier);
	$hold(posedge CK &&& shcheckCKCEBlh === 1'b1,posedge CEB,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,negedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,posedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,negedge SE,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,posedge SE,0,notifier);
	$hold(posedge CK,posedge RB,0,notifier);
	$hold(posedge CK,posedge SB,0,notifier);
	$hold(posedge SB,posedge RB,0,notifier);
	$setup(negedge CEB,posedge CK &&& shcheckCKCEBlh === 1'b1,0,notifier);
	$setup(negedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(negedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(negedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$setup(posedge CEB,posedge CK &&& shcheckCKCEBlh === 1'b1,0,notifier);
	$setup(posedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge CK &&& shcheckCKRBlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge SB,0,notifier);
	$setup(posedge SB,posedge CK &&& shcheckCKSBlh === 1'b1,0,notifier);
	$setup(posedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(posedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$width(negedge CK,1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(negedge SB,1,0,notifier);
	$width(posedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       SDERSPQ1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:35 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Enabled, Positive Edge, Scan D Flip-Flop with Set, Clear; Q Output
// Q  = !RB ? 0 : !SB ? 1 : rising(CK) ? (SE ? SD : (CEB ? 's' : D)) : 'p'
module SDERSPQ1 (Q, CEB, CK, D, RB, SB, SD, SE);
	output Q;
	input  CEB;
	input  CK;
	input  D;
	input  RB;
	input  SB;
	input  SD;
	input  SE;
	reg notifier;
	p_mux21 _i0 (_n2, buf_Q, D, CEB);
	p_mux21 _i1 (_n1, SD, _n2, SE);
	not _i2 (_n3,SB);
	not _i3 (_n4,RB);
	p_ffrs _i4 (buf_Q, _n1, CK, _n4, _n3, notifier);
	buf _iQ(Q, buf_Q);
`ifdef no_tchk
`else
	not _wi0 (_wn2,CEB);
	not _wi1 (_wn5,D);
	and _wi2 (_wn4,SD,SB,_wn5,RB);
	not _wi3 (_wn7,SD);
	and _wi4 (_wn6,_wn7,SB,D,RB);
	or _wi5 (_wn3,_wn4,_wn6);
	and _wi6 (_wn1,_wn2,_wn3);
	and _wi7 (_wn8,SB,CEB,RB);
	or _wi8 (shcheckCKSElh,_wn1,_wn8);
	not _wi11 (_wn17,SE);
	and _wi12 (_wn16,SD,_wn17);
	or _wi14 (_wn15,_wn16,_wn7);
	and _wi15 (_wn13,_wn5,RB,_wn15);
	and _wi17 (_wn19,SE,_wn7,D,RB);
	or _wi18 (_wn12,_wn13,_wn19);
	and _wi19 (_wn10,_wn2,_wn12);
	and _wi21 (_wn21,SE,_wn7,CEB,RB);
	or _wi22 (shcheckCKSBlh,_wn10,_wn21);
	and _wi25 (_wn27,SE,SD,_wn5,SB);
	and _wi28 (_wn31,_wn7,_wn17);
	or _wi29 (_wn30,_wn31,SD);
	and _wi30 (_wn29,D,SB,_wn30);
	or _wi31 (_wn26,_wn27,_wn29);
	and _wi32 (_wn24,_wn2,_wn26);
	and _wi33 (_wn34,SE,SD,CEB,SB);
	or _wi34 (shcheckCKRBlh,_wn24,_wn34);
	and _wi37 (shcheckCKDlh,_wn17,SB,_wn2,RB);
	and _wi38 (shcheckCKSDlh,SE,RB,SB);
	and _wi40 (shcheckCKCEBlh,_wn17,RB,SB);
`endif
	specify		(CK => Q) = (1,1);
	(RB => Q) = (0,0);
	(SB => Q) = (0,0);
`ifdef no_tchk
`else
	$hold(posedge CK &&& shcheckCKCEBlh === 1'b1,negedge CEB,0,notifier);
	$hold(posedge CK &&& shcheckCKCEBlh === 1'b1,posedge CEB,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,negedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,posedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,negedge SE,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,posedge SE,0,notifier);
	$hold(posedge CK,posedge RB,0,notifier);
	$hold(posedge CK,posedge SB,0,notifier);
	$hold(posedge SB,posedge RB,0,notifier);
	$setup(negedge CEB,posedge CK &&& shcheckCKCEBlh === 1'b1,0,notifier);
	$setup(negedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(negedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(negedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$setup(posedge CEB,posedge CK &&& shcheckCKCEBlh === 1'b1,0,notifier);
	$setup(posedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge CK &&& shcheckCKRBlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge SB,0,notifier);
	$setup(posedge SB,posedge CK &&& shcheckCKSBlh === 1'b1,0,notifier);
	$setup(posedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(posedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$width(negedge CK,1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(negedge SB,1,0,notifier);
	$width(posedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       SDERSPQ2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:38 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Enabled, Positive Edge, Scan D Flip-Flop with Set, Clear; Q Output
// Q  = !RB ? 0 : !SB ? 1 : rising(CK) ? (SE ? SD : (CEB ? 's' : D)) : 'p'
module SDERSPQ2 (Q, CEB, CK, D, RB, SB, SD, SE);
	output Q;
	input  CEB;
	input  CK;
	input  D;
	input  RB;
	input  SB;
	input  SD;
	input  SE;
	reg notifier;
	p_mux21 _i0 (_n2, buf_Q, D, CEB);
	p_mux21 _i1 (_n1, SD, _n2, SE);
	not _i2 (_n3,SB);
	not _i3 (_n4,RB);
	p_ffrs _i4 (buf_Q, _n1, CK, _n4, _n3, notifier);
	buf _iQ(Q, buf_Q);
`ifdef no_tchk
`else
	not _wi0 (_wn2,CEB);
	not _wi1 (_wn5,D);
	and _wi2 (_wn4,SD,SB,_wn5,RB);
	not _wi3 (_wn7,SD);
	and _wi4 (_wn6,_wn7,SB,D,RB);
	or _wi5 (_wn3,_wn4,_wn6);
	and _wi6 (_wn1,_wn2,_wn3);
	and _wi7 (_wn8,SB,CEB,RB);
	or _wi8 (shcheckCKSElh,_wn1,_wn8);
	not _wi11 (_wn17,SE);
	and _wi12 (_wn16,SD,_wn17);
	or _wi14 (_wn15,_wn16,_wn7);
	and _wi15 (_wn13,_wn5,RB,_wn15);
	and _wi17 (_wn19,SE,_wn7,D,RB);
	or _wi18 (_wn12,_wn13,_wn19);
	and _wi19 (_wn10,_wn2,_wn12);
	and _wi21 (_wn21,SE,_wn7,CEB,RB);
	or _wi22 (shcheckCKSBlh,_wn10,_wn21);
	and _wi25 (_wn27,SE,SD,_wn5,SB);
	and _wi28 (_wn31,_wn7,_wn17);
	or _wi29 (_wn30,_wn31,SD);
	and _wi30 (_wn29,D,SB,_wn30);
	or _wi31 (_wn26,_wn27,_wn29);
	and _wi32 (_wn24,_wn2,_wn26);
	and _wi33 (_wn34,SE,SD,CEB,SB);
	or _wi34 (shcheckCKRBlh,_wn24,_wn34);
	and _wi37 (shcheckCKDlh,_wn17,SB,_wn2,RB);
	and _wi38 (shcheckCKSDlh,SE,RB,SB);
	and _wi40 (shcheckCKCEBlh,_wn17,RB,SB);
`endif
	specify		(CK => Q) = (1,1);
	(RB => Q) = (0,0);
	(SB => Q) = (0,0);
`ifdef no_tchk
`else
	$hold(posedge CK &&& shcheckCKCEBlh === 1'b1,negedge CEB,0,notifier);
	$hold(posedge CK &&& shcheckCKCEBlh === 1'b1,posedge CEB,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,negedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,posedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,negedge SE,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,posedge SE,0,notifier);
	$hold(posedge CK,posedge RB,0,notifier);
	$hold(posedge CK,posedge SB,0,notifier);
	$hold(posedge SB,posedge RB,0,notifier);
	$setup(negedge CEB,posedge CK &&& shcheckCKCEBlh === 1'b1,0,notifier);
	$setup(negedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(negedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(negedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$setup(posedge CEB,posedge CK &&& shcheckCKCEBlh === 1'b1,0,notifier);
	$setup(posedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge CK &&& shcheckCKRBlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge SB,0,notifier);
	$setup(posedge SB,posedge CK &&& shcheckCKSBlh === 1'b1,0,notifier);
	$setup(posedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(posedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$width(negedge CK,1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(negedge SB,1,0,notifier);
	$width(posedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       SDFPB1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:40 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Positive Edge, Scan D Flip-Flop; Q, QB  Outputs
// Q  = rising(CK) ?  (SE ? SD : D) : 'p';QB = !Q
module SDFPB1 (Q, QB, CK, D, SD, SE);
	output Q;
	output QB;
	input  CK;
	input  D;
	input  SD;
	input  SE;
	reg notifier;
	p_mux21 _i0 (_n1, SD, D, SE);
	p_ff _i1 (Q, _n1, CK, notifier);
	not _i2 (QB,Q);
`ifdef no_tchk
`else
	not _wi0 (_wn2,D);
	and _wi1 (_wn1,_wn2,SD);
	not _wi2 (_wn4,SD);
	and _wi3 (_wn3,D,_wn4);
	or _wi4 (shcheckCKSElh,_wn1,_wn3);
	not _wi5 (shcheckCKDlh,SE);
	buf _wi6 (shcheckCKSDlh,SE);
`endif
	specify		(CK => Q) = (1,1);
	(CK => QB) = (1,1);
`ifdef no_tchk
`else
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,negedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,posedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,negedge SE,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,posedge SE,0,notifier);
	$setup(negedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(negedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(negedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$setup(posedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(posedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$width(negedge CK,1,0,notifier);
	$width(posedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       SDFPB2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:35 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Positive Edge, Scan D Flip-Flop; Q, QB  Outputs
// Q  = rising(CK) ?  (SE ? SD : D) : 'p';QB = !Q
module SDFPB2 (Q, QB, CK, D, SD, SE);
	output Q;
	output QB;
	input  CK;
	input  D;
	input  SD;
	input  SE;
	reg notifier;
	p_mux21 _i0 (_n1, SD, D, SE);
	p_ff _i1 (Q, _n1, CK, notifier);
	not _i2 (QB,Q);
`ifdef no_tchk
`else
	not _wi0 (_wn2,D);
	and _wi1 (_wn1,_wn2,SD);
	not _wi2 (_wn4,SD);
	and _wi3 (_wn3,D,_wn4);
	or _wi4 (shcheckCKSElh,_wn1,_wn3);
	not _wi5 (shcheckCKDlh,SE);
	buf _wi6 (shcheckCKSDlh,SE);
`endif
	specify		(CK => Q) = (1,1);
	(CK => QB) = (1,1);
`ifdef no_tchk
`else
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,negedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,posedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,negedge SE,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,posedge SE,0,notifier);
	$setup(negedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(negedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(negedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$setup(posedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(posedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$width(negedge CK,1,0,notifier);
	$width(posedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       SDFPQ1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:32 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Positive Edge, Scan D Flip-Flop; Q Output
// Q  = rising(CK) ?  (SE ? SD : D) : 'p'
module SDFPQ1 (Q, CK, D, SD, SE);
	output Q;
	input  CK;
	input  D;
	input  SD;
	input  SE;
	reg notifier;
	p_mux21 _i0 (_n1, SD, D, SE);
	p_ff _i1 (Q, _n1, CK, notifier);
`ifdef no_tchk
`else
	not _wi0 (_wn2,D);
	and _wi1 (_wn1,_wn2,SD);
	not _wi2 (_wn4,SD);
	and _wi3 (_wn3,D,_wn4);
	or _wi4 (shcheckCKSElh,_wn1,_wn3);
	not _wi5 (shcheckCKDlh,SE);
	buf _wi6 (shcheckCKSDlh,SE);
`endif
	specify		(CK => Q) = (1,1);
`ifdef no_tchk
`else
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,negedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,posedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,negedge SE,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,posedge SE,0,notifier);
	$setup(negedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(negedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(negedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$setup(posedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(posedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$width(negedge CK,1,0,notifier);
	$width(posedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       SDFPQ2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:29 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Positive Edge, Scan D Flip-Flop; Q Output
// Q  = rising(CK) ?  (SE ? SD : D) : 'p'
module SDFPQ2 (Q, CK, D, SD, SE);
	output Q;
	input  CK;
	input  D;
	input  SD;
	input  SE;
	reg notifier;
	p_mux21 _i0 (_n1, SD, D, SE);
	p_ff _i1 (Q, _n1, CK, notifier);
`ifdef no_tchk
`else
	not _wi0 (_wn2,D);
	and _wi1 (_wn1,_wn2,SD);
	not _wi2 (_wn4,SD);
	and _wi3 (_wn3,D,_wn4);
	or _wi4 (shcheckCKSElh,_wn1,_wn3);
	not _wi5 (shcheckCKDlh,SE);
	buf _wi6 (shcheckCKSDlh,SE);
`endif
	specify		(CK => Q) = (1,1);
`ifdef no_tchk
`else
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,negedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,posedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,negedge SE,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,posedge SE,0,notifier);
	$setup(negedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(negedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(negedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$setup(posedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(posedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$width(negedge CK,1,0,notifier);
	$width(posedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       SDFPQ4.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:23 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Positive Edge, Scan D Flip-Flop; Q Output
// Q  = rising(CK) ?  (SE ? SD : D) : 'p'
module SDFPQ4 (Q, CK, D, SD, SE);
	output Q;
	input  CK;
	input  D;
	input  SD;
	input  SE;
	reg notifier;
	p_mux21 _i0 (_n1, SD, D, SE);
	p_ff _i1 (Q, _n1, CK, notifier);
`ifdef no_tchk
`else
	not _wi0 (_wn2,D);
	and _wi1 (_wn1,_wn2,SD);
	not _wi2 (_wn4,SD);
	and _wi3 (_wn3,D,_wn4);
	or _wi4 (shcheckCKSElh,_wn1,_wn3);
	not _wi5 (shcheckCKDlh,SE);
	buf _wi6 (shcheckCKSDlh,SE);
`endif
	specify		(CK => Q) = (1,1);
`ifdef no_tchk
`else
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,negedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,posedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,negedge SE,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,posedge SE,0,notifier);
	$setup(negedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(negedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(negedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$setup(posedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(posedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$width(negedge CK,1,0,notifier);
	$width(posedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       SDFRNQ1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:24 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Negative Edge, Scan D Flip-Flop with Clear; Q Output
// Q  = !RB ? 0 : falling(CKB) ?  (SE ? SD : D) : 'p'
module SDFRNQ1 (Q, CKB, D, RB, SD, SE);
	output Q;
	input  CKB;
	input  D;
	input  RB;
	input  SD;
	input  SE;
	reg notifier;
	not _i0 (_n1,CKB);
	p_mux21 _i1 (_n2, SD, D, SE);
	not _i2 (_n3,RB);
	p_ffr _i3 (Q, _n2, _n1, _n3, notifier);
`ifdef no_tchk
`else
	not _wi0 (_wn2,D);
	and _wi1 (_wn1,SE,_wn2,SD);
	not _wi2 (_wn6,SD);
	not _wi3 (_wn7,SE);
	and _wi4 (_wn5,_wn6,_wn7);
	or _wi5 (_wn4,_wn5,SD);
	and _wi6 (_wn3,D,_wn4);
	or _wi7 (shcheckCKBRBhl,_wn1,_wn3);
	and _wi8 (shcheckCKBSDhl,RB,SE);
	and _wi10 (shcheckCKBDhl,RB,_wn7);
	and _wi12 (_wn12,SD,_wn2,RB);
	and _wi14 (_wn14,_wn6,D,RB);
	or _wi15 (shcheckCKBSEhl,_wn12,_wn14);
`endif
	specify		(CKB => Q) = (1,1);
	(RB => Q) = (0,0);
`ifdef no_tchk
`else
	$hold(negedge CKB &&& shcheckCKBDhl === 1'b1,negedge D,0,notifier);
	$hold(negedge CKB &&& shcheckCKBDhl === 1'b1,posedge D,0,notifier);
	$hold(negedge CKB &&& shcheckCKBSDhl === 1'b1,negedge SD,0,notifier);
	$hold(negedge CKB &&& shcheckCKBSDhl === 1'b1,posedge SD,0,notifier);
	$hold(negedge CKB &&& shcheckCKBSEhl === 1'b1,negedge SE,0,notifier);
	$hold(negedge CKB &&& shcheckCKBSEhl === 1'b1,posedge SE,0,notifier);
	$hold(negedge CKB,posedge RB,0,notifier);
	$setup(negedge D,negedge CKB &&& shcheckCKBDhl === 1'b1,0,notifier);
	$setup(negedge SD,negedge CKB &&& shcheckCKBSDhl === 1'b1,0,notifier);
	$setup(negedge SE,negedge CKB &&& shcheckCKBSEhl === 1'b1,0,notifier);
	$setup(posedge D,negedge CKB &&& shcheckCKBDhl === 1'b1,0,notifier);
	$setup(posedge RB,negedge CKB &&& shcheckCKBRBhl === 1'b1,0,notifier);
	$setup(posedge SD,negedge CKB &&& shcheckCKBSDhl === 1'b1,0,notifier);
	$setup(posedge SE,negedge CKB &&& shcheckCKBSEhl === 1'b1,0,notifier);
	$width(negedge CKB,1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(posedge CKB,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       SDFRNQ2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:38 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Negative Edge, Scan D Flip-Flop with Clear; Q Output
// Q  = !RB ? 0 : falling(CKB) ?  (SE ? SD : D) : 'p'
module SDFRNQ2 (Q, CKB, D, RB, SD, SE);
	output Q;
	input  CKB;
	input  D;
	input  RB;
	input  SD;
	input  SE;
	reg notifier;
	not _i0 (_n1,CKB);
	p_mux21 _i1 (_n2, SD, D, SE);
	not _i2 (_n3,RB);
	p_ffr _i3 (Q, _n2, _n1, _n3, notifier);
`ifdef no_tchk
`else
	not _wi0 (_wn2,D);
	and _wi1 (_wn1,SE,_wn2,SD);
	not _wi2 (_wn6,SD);
	not _wi3 (_wn7,SE);
	and _wi4 (_wn5,_wn6,_wn7);
	or _wi5 (_wn4,_wn5,SD);
	and _wi6 (_wn3,D,_wn4);
	or _wi7 (shcheckCKBRBhl,_wn1,_wn3);
	and _wi8 (shcheckCKBSDhl,RB,SE);
	and _wi10 (shcheckCKBDhl,RB,_wn7);
	and _wi12 (_wn12,SD,_wn2,RB);
	and _wi14 (_wn14,_wn6,D,RB);
	or _wi15 (shcheckCKBSEhl,_wn12,_wn14);
`endif
	specify		(CKB => Q) = (1,1);
	(RB => Q) = (0,0);
`ifdef no_tchk
`else
	$hold(negedge CKB &&& shcheckCKBDhl === 1'b1,negedge D,0,notifier);
	$hold(negedge CKB &&& shcheckCKBDhl === 1'b1,posedge D,0,notifier);
	$hold(negedge CKB &&& shcheckCKBSDhl === 1'b1,negedge SD,0,notifier);
	$hold(negedge CKB &&& shcheckCKBSDhl === 1'b1,posedge SD,0,notifier);
	$hold(negedge CKB &&& shcheckCKBSEhl === 1'b1,negedge SE,0,notifier);
	$hold(negedge CKB &&& shcheckCKBSEhl === 1'b1,posedge SE,0,notifier);
	$hold(negedge CKB,posedge RB,0,notifier);
	$setup(negedge D,negedge CKB &&& shcheckCKBDhl === 1'b1,0,notifier);
	$setup(negedge SD,negedge CKB &&& shcheckCKBSDhl === 1'b1,0,notifier);
	$setup(negedge SE,negedge CKB &&& shcheckCKBSEhl === 1'b1,0,notifier);
	$setup(posedge D,negedge CKB &&& shcheckCKBDhl === 1'b1,0,notifier);
	$setup(posedge RB,negedge CKB &&& shcheckCKBRBhl === 1'b1,0,notifier);
	$setup(posedge SD,negedge CKB &&& shcheckCKBSDhl === 1'b1,0,notifier);
	$setup(posedge SE,negedge CKB &&& shcheckCKBSEhl === 1'b1,0,notifier);
	$width(negedge CKB,1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(posedge CKB,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       SDFRPB1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:20 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Positive Edge, Scan D Flip-Flop with Clear; Q, QB Outputs
// Q  = !RB ? 0 : rising(CK) ?  (SE ? SD : D) : 'p';QB = !Q
module SDFRPB1 (Q, QB, CK, D, RB, SD, SE);
	output Q;
	output QB;
	input  CK;
	input  D;
	input  RB;
	input  SD;
	input  SE;
	reg notifier;
	p_mux21 _i0 (_n1, SD, D, SE);
	not _i1 (_n2,RB);
	p_ffr _i2 (Q, _n1, CK, _n2, notifier);
	not _i3 (QB,Q);
`ifdef no_tchk
`else
	not _wi0 (_wn2,D);
	and _wi1 (_wn1,SD,_wn2,RB);
	not _wi2 (_wn4,SD);
	and _wi3 (_wn3,_wn4,D,RB);
	or _wi4 (shcheckCKSElh,_wn1,_wn3);
	not _wi5 (_wn6,SE);
	and _wi6 (shcheckCKDlh,RB,_wn6);
	and _wi8 (_wn8,SE,_wn2,SD);
	and _wi11 (_wn12,_wn4,_wn6);
	or _wi12 (_wn11,_wn12,SD);
	and _wi13 (_wn10,D,_wn11);
	or _wi14 (shcheckCKRBlh,_wn8,_wn10);
	and _wi15 (shcheckCKSDlh,RB,SE);
`endif
	specify		(CK => Q) = (1,1);
	(CK => QB) = (1,1);
	(RB => Q) = (0,0);
	(RB => QB) = (0,0);
`ifdef no_tchk
`else
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,negedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,posedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,negedge SE,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,posedge SE,0,notifier);
	$hold(posedge CK,posedge RB,0,notifier);
	$setup(negedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(negedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(negedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$setup(posedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge CK &&& shcheckCKRBlh === 1'b1,0,notifier);
	$setup(posedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(posedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$width(negedge CK,1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(posedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       SDFRPB2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:24 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Positive Edge, Scan D Flip-Flop with Clear; Q, QB Outputs
// Q  = !RB ? 0 : rising(CK) ?  (SE ? SD : D) : 'p';QB = !Q
module SDFRPB2 (Q, QB, CK, D, RB, SD, SE);
	output Q;
	output QB;
	input  CK;
	input  D;
	input  RB;
	input  SD;
	input  SE;
	reg notifier;
	p_mux21 _i0 (_n1, SD, D, SE);
	not _i1 (_n2,RB);
	p_ffr _i2 (Q, _n1, CK, _n2, notifier);
	not _i3 (QB,Q);
`ifdef no_tchk
`else
	not _wi0 (_wn2,D);
	and _wi1 (_wn1,SD,_wn2,RB);
	not _wi2 (_wn4,SD);
	and _wi3 (_wn3,_wn4,D,RB);
	or _wi4 (shcheckCKSElh,_wn1,_wn3);
	not _wi5 (_wn6,SE);
	and _wi6 (shcheckCKDlh,RB,_wn6);
	and _wi8 (_wn8,SE,_wn2,SD);
	and _wi11 (_wn12,_wn4,_wn6);
	or _wi12 (_wn11,_wn12,SD);
	and _wi13 (_wn10,D,_wn11);
	or _wi14 (shcheckCKRBlh,_wn8,_wn10);
	and _wi15 (shcheckCKSDlh,RB,SE);
`endif
	specify		(CK => Q) = (1,1);
	(CK => QB) = (1,1);
	(RB => Q) = (0,0);
	(RB => QB) = (0,0);
`ifdef no_tchk
`else
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,negedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,posedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,negedge SE,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,posedge SE,0,notifier);
	$hold(posedge CK,posedge RB,0,notifier);
	$setup(negedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(negedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(negedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$setup(posedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge CK &&& shcheckCKRBlh === 1'b1,0,notifier);
	$setup(posedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(posedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$width(negedge CK,1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(posedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       SDFRPQ1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:23 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Positive Edge, Scan D Flip-Flop with Clear; Q Output
// Q  = !RB ? 0 : rising(CK) ?  (SE ? SD : D) : 'p'
module SDFRPQ1 (Q, CK, D, RB, SD, SE);
	output Q;
	input  CK;
	input  D;
	input  RB;
	input  SD;
	input  SE;
	reg notifier;
	p_mux21 _i0 (_n1, SD, D, SE);
	not _i1 (_n2,RB);
	p_ffr _i2 (Q, _n1, CK, _n2, notifier);
`ifdef no_tchk
`else
	not _wi0 (_wn2,D);
	and _wi1 (_wn1,SD,_wn2,RB);
	not _wi2 (_wn4,SD);
	and _wi3 (_wn3,_wn4,D,RB);
	or _wi4 (shcheckCKSElh,_wn1,_wn3);
	not _wi5 (_wn6,SE);
	and _wi6 (shcheckCKDlh,RB,_wn6);
	and _wi8 (_wn8,SE,_wn2,SD);
	and _wi11 (_wn12,_wn4,_wn6);
	or _wi12 (_wn11,_wn12,SD);
	and _wi13 (_wn10,D,_wn11);
	or _wi14 (shcheckCKRBlh,_wn8,_wn10);
	and _wi15 (shcheckCKSDlh,RB,SE);
`endif
	specify		(CK => Q) = (1,1);
	(RB => Q) = (0,0);
`ifdef no_tchk
`else
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,negedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,posedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,negedge SE,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,posedge SE,0,notifier);
	$hold(posedge CK,posedge RB,0,notifier);
	$setup(negedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(negedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(negedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$setup(posedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge CK &&& shcheckCKRBlh === 1'b1,0,notifier);
	$setup(posedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(posedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$width(negedge CK,1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(posedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       SDFRPQ2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:23 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Positive Edge, Scan D Flip-Flop with Clear; Q Output
// Q  = !RB ? 0 : rising(CK) ?  (SE ? SD : D) : 'p'
module SDFRPQ2 (Q, CK, D, RB, SD, SE);
	output Q;
	input  CK;
	input  D;
	input  RB;
	input  SD;
	input  SE;
	reg notifier;
	p_mux21 _i0 (_n1, SD, D, SE);
	not _i1 (_n2,RB);
	p_ffr _i2 (Q, _n1, CK, _n2, notifier);
`ifdef no_tchk
`else
	not _wi0 (_wn2,D);
	and _wi1 (_wn1,SD,_wn2,RB);
	not _wi2 (_wn4,SD);
	and _wi3 (_wn3,_wn4,D,RB);
	or _wi4 (shcheckCKSElh,_wn1,_wn3);
	not _wi5 (_wn6,SE);
	and _wi6 (shcheckCKDlh,RB,_wn6);
	and _wi8 (_wn8,SE,_wn2,SD);
	and _wi11 (_wn12,_wn4,_wn6);
	or _wi12 (_wn11,_wn12,SD);
	and _wi13 (_wn10,D,_wn11);
	or _wi14 (shcheckCKRBlh,_wn8,_wn10);
	and _wi15 (shcheckCKSDlh,RB,SE);
`endif
	specify		(CK => Q) = (1,1);
	(RB => Q) = (0,0);
`ifdef no_tchk
`else
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,negedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,posedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,negedge SE,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,posedge SE,0,notifier);
	$hold(posedge CK,posedge RB,0,notifier);
	$setup(negedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(negedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(negedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$setup(posedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge CK &&& shcheckCKRBlh === 1'b1,0,notifier);
	$setup(posedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(posedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$width(negedge CK,1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(posedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       SDFRSNB1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:23 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Negative Edge, Scan D Flip-Flop with Set, Clear; Q, QB Outputs
// Q  = !RB ? 0 : !SB ? 1 : falling(CKB) ?  (SE ? SD : D) : 'p';QB = !Q & SB
module SDFRSNB1 (Q, QB, CKB, D, RB, SB, SD, SE);
	output Q;
	output QB;
	input  CKB;
	input  D;
	input  RB;
	input  SB;
	input  SD;
	input  SE;
	reg notifier;
	not _i0 (_n1,CKB);
	p_mux21 _i1 (_n2, SD, D, SE);
	not _i2 (_n3,SB);
	not _i3 (_n4,RB);
	p_ffrs _i4 (Q, _n2, _n1, _n4, _n3, notifier);
	not _i5 (_n6,Q);
	and _i6 (QB,_n6,SB);
`ifdef no_tchk
`else
	not _wi0 (_wn2,D);
	and _wi1 (_wn1,SE,SD,_wn2,SB);
	not _wi2 (_wn6,SD);
	not _wi3 (_wn7,SE);
	and _wi4 (_wn5,_wn6,_wn7);
	or _wi5 (_wn4,_wn5,SD);
	and _wi6 (_wn3,D,SB,_wn4);
	or _wi7 (shcheckCKBRBhl,_wn1,_wn3);
	and _wi10 (_wn12,SD,_wn7);
	or _wi12 (_wn11,_wn12,_wn6);
	and _wi13 (_wn9,_wn2,RB,_wn11);
	and _wi15 (_wn15,SE,_wn6,D,RB);
	or _wi16 (shcheckCKBSBhl,_wn9,_wn15);
	and _wi17 (shcheckCKBSDhl,SE,RB,SB);
	and _wi19 (shcheckCKBDhl,_wn7,RB,SB);
	and _wi21 (_wn21,SD,SB,_wn2,RB);
	and _wi23 (_wn23,_wn6,SB,D,RB);
	or _wi24 (shcheckCKBSEhl,_wn21,_wn23);
`endif
	specify		(CKB => Q) = (1,1);
	(CKB => QB) = (1,1);
	(RB => Q) = (0,0);
	(RB => QB) = (0,0);
	(SB => Q) = (0,0);
	(SB => QB) = (0,0);
`ifdef no_tchk
`else
	$hold(negedge CKB &&& shcheckCKBDhl === 1'b1,negedge D,0,notifier);
	$hold(negedge CKB &&& shcheckCKBDhl === 1'b1,posedge D,0,notifier);
	$hold(negedge CKB &&& shcheckCKBSDhl === 1'b1,negedge SD,0,notifier);
	$hold(negedge CKB &&& shcheckCKBSDhl === 1'b1,posedge SD,0,notifier);
	$hold(negedge CKB &&& shcheckCKBSEhl === 1'b1,negedge SE,0,notifier);
	$hold(negedge CKB &&& shcheckCKBSEhl === 1'b1,posedge SE,0,notifier);
	$hold(negedge CKB,posedge RB,0,notifier);
	$hold(negedge CKB,posedge SB,0,notifier);
	$hold(posedge SB,posedge RB,0,notifier);
	$setup(negedge D,negedge CKB &&& shcheckCKBDhl === 1'b1,0,notifier);
	$setup(negedge SD,negedge CKB &&& shcheckCKBSDhl === 1'b1,0,notifier);
	$setup(negedge SE,negedge CKB &&& shcheckCKBSEhl === 1'b1,0,notifier);
	$setup(posedge D,negedge CKB &&& shcheckCKBDhl === 1'b1,0,notifier);
	$setup(posedge RB,negedge CKB &&& shcheckCKBRBhl === 1'b1,0,notifier);
	$setup(posedge RB,posedge SB,0,notifier);
	$setup(posedge SB,negedge CKB &&& shcheckCKBSBhl === 1'b1,0,notifier);
	$setup(posedge SD,negedge CKB &&& shcheckCKBSDhl === 1'b1,0,notifier);
	$setup(posedge SE,negedge CKB &&& shcheckCKBSEhl === 1'b1,0,notifier);
	$width(negedge CKB,1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(negedge SB,1,0,notifier);
	$width(posedge CKB,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       SDFRSNB2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:23 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Negative Edge, Scan D Flip-Flop with Set, Clear; Q, QB Outputs
// Q  = !RB ? 0 : !SB ? 1 : falling(CKB) ?  (SE ? SD : D) : 'p';QB = !Q & SB
module SDFRSNB2 (Q, QB, CKB, D, RB, SB, SD, SE);
	output Q;
	output QB;
	input  CKB;
	input  D;
	input  RB;
	input  SB;
	input  SD;
	input  SE;
	reg notifier;
	not _i0 (_n1,CKB);
	p_mux21 _i1 (_n2, SD, D, SE);
	not _i2 (_n3,SB);
	not _i3 (_n4,RB);
	p_ffrs _i4 (Q, _n2, _n1, _n4, _n3, notifier);
	not _i5 (_n6,Q);
	and _i6 (QB,_n6,SB);
`ifdef no_tchk
`else
	not _wi0 (_wn2,D);
	and _wi1 (_wn1,SE,SD,_wn2,SB);
	not _wi2 (_wn6,SD);
	not _wi3 (_wn7,SE);
	and _wi4 (_wn5,_wn6,_wn7);
	or _wi5 (_wn4,_wn5,SD);
	and _wi6 (_wn3,D,SB,_wn4);
	or _wi7 (shcheckCKBRBhl,_wn1,_wn3);
	and _wi10 (_wn12,SD,_wn7);
	or _wi12 (_wn11,_wn12,_wn6);
	and _wi13 (_wn9,_wn2,RB,_wn11);
	and _wi15 (_wn15,SE,_wn6,D,RB);
	or _wi16 (shcheckCKBSBhl,_wn9,_wn15);
	and _wi17 (shcheckCKBSDhl,SE,RB,SB);
	and _wi19 (shcheckCKBDhl,_wn7,RB,SB);
	and _wi21 (_wn21,SD,SB,_wn2,RB);
	and _wi23 (_wn23,_wn6,SB,D,RB);
	or _wi24 (shcheckCKBSEhl,_wn21,_wn23);
`endif
	specify		(CKB => Q) = (1,1);
	(CKB => QB) = (1,1);
	(RB => Q) = (0,0);
	(RB => QB) = (0,0);
	(SB => Q) = (0,0);
	(SB => QB) = (0,0);
`ifdef no_tchk
`else
	$hold(negedge CKB &&& shcheckCKBDhl === 1'b1,negedge D,0,notifier);
	$hold(negedge CKB &&& shcheckCKBDhl === 1'b1,posedge D,0,notifier);
	$hold(negedge CKB &&& shcheckCKBSDhl === 1'b1,negedge SD,0,notifier);
	$hold(negedge CKB &&& shcheckCKBSDhl === 1'b1,posedge SD,0,notifier);
	$hold(negedge CKB &&& shcheckCKBSEhl === 1'b1,negedge SE,0,notifier);
	$hold(negedge CKB &&& shcheckCKBSEhl === 1'b1,posedge SE,0,notifier);
	$hold(negedge CKB,posedge RB,0,notifier);
	$hold(negedge CKB,posedge SB,0,notifier);
	$hold(posedge SB,posedge RB,0,notifier);
	$setup(negedge D,negedge CKB &&& shcheckCKBDhl === 1'b1,0,notifier);
	$setup(negedge SD,negedge CKB &&& shcheckCKBSDhl === 1'b1,0,notifier);
	$setup(negedge SE,negedge CKB &&& shcheckCKBSEhl === 1'b1,0,notifier);
	$setup(posedge D,negedge CKB &&& shcheckCKBDhl === 1'b1,0,notifier);
	$setup(posedge RB,negedge CKB &&& shcheckCKBRBhl === 1'b1,0,notifier);
	$setup(posedge RB,posedge SB,0,notifier);
	$setup(posedge SB,negedge CKB &&& shcheckCKBSBhl === 1'b1,0,notifier);
	$setup(posedge SD,negedge CKB &&& shcheckCKBSDhl === 1'b1,0,notifier);
	$setup(posedge SE,negedge CKB &&& shcheckCKBSEhl === 1'b1,0,notifier);
	$width(negedge CKB,1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(negedge SB,1,0,notifier);
	$width(posedge CKB,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       SDFRSPB1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:20 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Positive Edge, Scan D Flip-Flop with Set, Clear; Q, QB Outputs
// Q  = !RB ? 0 : !SB ? 1 : rising(CK) ?  (SE ? SD : D) : 'p';QB = !Q & SB
module SDFRSPB1 (Q, QB, CK, D, RB, SB, SD, SE);
	output Q;
	output QB;
	input  CK;
	input  D;
	input  RB;
	input  SB;
	input  SD;
	input  SE;
	reg notifier;
	p_mux21 _i0 (_n1, SD, D, SE);
	not _i1 (_n2,SB);
	not _i2 (_n3,RB);
	p_ffrs _i3 (Q, _n1, CK, _n3, _n2, notifier);
	not _i4 (_n5,Q);
	and _i5 (QB,_n5,SB);
`ifdef no_tchk
`else
	not _wi0 (_wn2,D);
	and _wi1 (_wn1,SD,SB,_wn2,RB);
	not _wi2 (_wn4,SD);
	and _wi3 (_wn3,_wn4,SB,D,RB);
	or _wi4 (shcheckCKSElh,_wn1,_wn3);
	not _wi6 (_wn10,SE);
	and _wi7 (_wn9,SD,_wn10);
	or _wi9 (_wn8,_wn9,_wn4);
	and _wi10 (_wn6,_wn2,RB,_wn8);
	and _wi12 (_wn12,SE,_wn4,D,RB);
	or _wi13 (shcheckCKSBlh,_wn6,_wn12);
	and _wi15 (_wn15,SE,SD,_wn2,SB);
	and _wi18 (_wn19,_wn4,_wn10);
	or _wi19 (_wn18,_wn19,SD);
	and _wi20 (_wn17,D,SB,_wn18);
	or _wi21 (shcheckCKRBlh,_wn15,_wn17);
	and _wi23 (shcheckCKDlh,_wn10,RB,SB);
	and _wi24 (shcheckCKSDlh,SE,RB,SB);
`endif
	specify		(CK => Q) = (1,1);
	(CK => QB) = (1,1);
	(RB => Q) = (0,0);
	(RB => QB) = (0,0);
	(SB => Q) = (0,0);
	(SB => QB) = (0,0);
`ifdef no_tchk
`else
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,negedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,posedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,negedge SE,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,posedge SE,0,notifier);
	$hold(posedge CK,posedge RB,0,notifier);
	$hold(posedge CK,posedge SB,0,notifier);
	$hold(posedge SB,posedge RB,0,notifier);
	$setup(negedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(negedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(negedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$setup(posedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge CK &&& shcheckCKRBlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge SB,0,notifier);
	$setup(posedge SB,posedge CK &&& shcheckCKSBlh === 1'b1,0,notifier);
	$setup(posedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(posedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$width(negedge CK,1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(negedge SB,1,0,notifier);
	$width(posedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       SDFRSPB2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:23 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Positive Edge, Scan D Flip-Flop with Set, Clear; Q, QB Outputs
// Q  = !RB ? 0 : !SB ? 1 : rising(CK) ?  (SE ? SD : D) : 'p';QB = !Q & SB
module SDFRSPB2 (Q, QB, CK, D, RB, SB, SD, SE);
	output Q;
	output QB;
	input  CK;
	input  D;
	input  RB;
	input  SB;
	input  SD;
	input  SE;
	reg notifier;
	p_mux21 _i0 (_n1, SD, D, SE);
	not _i1 (_n2,SB);
	not _i2 (_n3,RB);
	p_ffrs _i3 (Q, _n1, CK, _n3, _n2, notifier);
	not _i4 (_n5,Q);
	and _i5 (QB,_n5,SB);
`ifdef no_tchk
`else
	not _wi0 (_wn2,D);
	and _wi1 (_wn1,SD,SB,_wn2,RB);
	not _wi2 (_wn4,SD);
	and _wi3 (_wn3,_wn4,SB,D,RB);
	or _wi4 (shcheckCKSElh,_wn1,_wn3);
	not _wi6 (_wn10,SE);
	and _wi7 (_wn9,SD,_wn10);
	or _wi9 (_wn8,_wn9,_wn4);
	and _wi10 (_wn6,_wn2,RB,_wn8);
	and _wi12 (_wn12,SE,_wn4,D,RB);
	or _wi13 (shcheckCKSBlh,_wn6,_wn12);
	and _wi15 (_wn15,SE,SD,_wn2,SB);
	and _wi18 (_wn19,_wn4,_wn10);
	or _wi19 (_wn18,_wn19,SD);
	and _wi20 (_wn17,D,SB,_wn18);
	or _wi21 (shcheckCKRBlh,_wn15,_wn17);
	and _wi23 (shcheckCKDlh,_wn10,RB,SB);
	and _wi24 (shcheckCKSDlh,SE,RB,SB);
`endif
	specify		(CK => Q) = (1,1);
	(CK => QB) = (1,1);
	(RB => Q) = (0,0);
	(RB => QB) = (0,0);
	(SB => Q) = (0,0);
	(SB => QB) = (0,0);
`ifdef no_tchk
`else
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,negedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,posedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,negedge SE,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,posedge SE,0,notifier);
	$hold(posedge CK,posedge RB,0,notifier);
	$hold(posedge CK,posedge SB,0,notifier);
	$hold(posedge SB,posedge RB,0,notifier);
	$setup(negedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(negedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(negedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$setup(posedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge CK &&& shcheckCKRBlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge SB,0,notifier);
	$setup(posedge SB,posedge CK &&& shcheckCKSBlh === 1'b1,0,notifier);
	$setup(posedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(posedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$width(negedge CK,1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(negedge SB,1,0,notifier);
	$width(posedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       SDFRSPQ1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:23 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Positive Edge, Scan D Flip-Flop with Set, Clear; Q Output
// Q  = !RB ? 0 : !SB ? 1 : rising(CK) ?  (SE ? SD : D) : 'p'
module SDFRSPQ1 (Q, CK, D, RB, SB, SD, SE);
	output Q;
	input  CK;
	input  D;
	input  RB;
	input  SB;
	input  SD;
	input  SE;
	reg notifier;
	p_mux21 _i0 (_n1, SD, D, SE);
	not _i1 (_n2,SB);
	not _i2 (_n3,RB);
	p_ffrs _i3 (Q, _n1, CK, _n3, _n2, notifier);
`ifdef no_tchk
`else
	not _wi0 (_wn2,D);
	and _wi1 (_wn1,SD,SB,_wn2,RB);
	not _wi2 (_wn4,SD);
	and _wi3 (_wn3,_wn4,SB,D,RB);
	or _wi4 (shcheckCKSElh,_wn1,_wn3);
	not _wi6 (_wn10,SE);
	and _wi7 (_wn9,SD,_wn10);
	or _wi9 (_wn8,_wn9,_wn4);
	and _wi10 (_wn6,_wn2,RB,_wn8);
	and _wi12 (_wn12,SE,_wn4,D,RB);
	or _wi13 (shcheckCKSBlh,_wn6,_wn12);
	and _wi15 (_wn15,SE,SD,_wn2,SB);
	and _wi18 (_wn19,_wn4,_wn10);
	or _wi19 (_wn18,_wn19,SD);
	and _wi20 (_wn17,D,SB,_wn18);
	or _wi21 (shcheckCKRBlh,_wn15,_wn17);
	and _wi23 (shcheckCKDlh,_wn10,RB,SB);
	and _wi24 (shcheckCKSDlh,SE,RB,SB);
`endif
	specify		(CK => Q) = (1,1);
	(RB => Q) = (0,0);
	(SB => Q) = (0,0);
`ifdef no_tchk
`else
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,negedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,posedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,negedge SE,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,posedge SE,0,notifier);
	$hold(posedge CK,posedge RB,0,notifier);
	$hold(posedge CK,posedge SB,0,notifier);
	$hold(posedge SB,posedge RB,0,notifier);
	$setup(negedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(negedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(negedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$setup(posedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge CK &&& shcheckCKRBlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge SB,0,notifier);
	$setup(posedge SB,posedge CK &&& shcheckCKSBlh === 1'b1,0,notifier);
	$setup(posedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(posedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$width(negedge CK,1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(negedge SB,1,0,notifier);
	$width(posedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       SDFRSPQ2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:28 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Positive Edge, Scan D Flip-Flop with Set, Clear; Q Output
// Q  = !RB ? 0 : !SB ? 1 : rising(CK) ?  (SE ? SD : D) : 'p'
module SDFRSPQ2 (Q, CK, D, RB, SB, SD, SE);
	output Q;
	input  CK;
	input  D;
	input  RB;
	input  SB;
	input  SD;
	input  SE;
	reg notifier;
	p_mux21 _i0 (_n1, SD, D, SE);
	not _i1 (_n2,SB);
	not _i2 (_n3,RB);
	p_ffrs _i3 (Q, _n1, CK, _n3, _n2, notifier);
`ifdef no_tchk
`else
	not _wi0 (_wn2,D);
	and _wi1 (_wn1,SD,SB,_wn2,RB);
	not _wi2 (_wn4,SD);
	and _wi3 (_wn3,_wn4,SB,D,RB);
	or _wi4 (shcheckCKSElh,_wn1,_wn3);
	not _wi6 (_wn10,SE);
	and _wi7 (_wn9,SD,_wn10);
	or _wi9 (_wn8,_wn9,_wn4);
	and _wi10 (_wn6,_wn2,RB,_wn8);
	and _wi12 (_wn12,SE,_wn4,D,RB);
	or _wi13 (shcheckCKSBlh,_wn6,_wn12);
	and _wi15 (_wn15,SE,SD,_wn2,SB);
	and _wi18 (_wn19,_wn4,_wn10);
	or _wi19 (_wn18,_wn19,SD);
	and _wi20 (_wn17,D,SB,_wn18);
	or _wi21 (shcheckCKRBlh,_wn15,_wn17);
	and _wi23 (shcheckCKDlh,_wn10,RB,SB);
	and _wi24 (shcheckCKSDlh,SE,RB,SB);
`endif
	specify		(CK => Q) = (1,1);
	(RB => Q) = (0,0);
	(SB => Q) = (0,0);
`ifdef no_tchk
`else
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,negedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,posedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,negedge SE,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,posedge SE,0,notifier);
	$hold(posedge CK,posedge RB,0,notifier);
	$hold(posedge CK,posedge SB,0,notifier);
	$hold(posedge SB,posedge RB,0,notifier);
	$setup(negedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(negedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(negedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$setup(posedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge CK &&& shcheckCKRBlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge SB,0,notifier);
	$setup(posedge SB,posedge CK &&& shcheckCKSBlh === 1'b1,0,notifier);
	$setup(posedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(posedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$width(negedge CK,1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(negedge SB,1,0,notifier);
	$width(posedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       SLATNB1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:23 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Active-High Enable, Scan Latch; Q, QB Outputs
// Q  = (G | SE) ?  D : 'p';QB = !Q
module SLATNB1 (Q, QB, D, G, SE);
	output Q;
	output QB;
	input  D;
	input  G;
	input  SE;
	reg notifier;
	or _i0 (_n1,G,SE);
	p_latch _i1 (Q, D, _n1, notifier);
	not _i2 (QB,Q);
`ifdef no_tchk
`else
	not _wi0 (shcheckGDhl,SE);
	not _wi1 (shcheckSEDhl,G);
`endif
	specify		(D => Q) = (0,0);
	(D => QB) = (0,0);
	(G => Q) = (1,1);
	(G => QB) = (1,1);
	(SE => Q) = (1,1);
	(SE => QB) = (1,1);
`ifdef no_tchk
`else
	$hold(negedge G &&& shcheckGDhl === 1'b1,negedge D,0,notifier);
	$hold(negedge G &&& shcheckGDhl === 1'b1,posedge D,0,notifier);
	$hold(negedge SE &&& shcheckSEDhl === 1'b1,negedge D,0,notifier);
	$hold(negedge SE &&& shcheckSEDhl === 1'b1,posedge D,0,notifier);
	$setup(negedge D,negedge G &&& shcheckGDhl === 1'b1,0,notifier);
	$setup(negedge D,negedge SE &&& shcheckSEDhl === 1'b1,0,notifier);
	$setup(posedge D,negedge G &&& shcheckGDhl === 1'b1,0,notifier);
	$setup(posedge D,negedge SE &&& shcheckSEDhl === 1'b1,0,notifier);
	$width(posedge G,1,0,notifier);
	$width(posedge SE,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       SLATNB2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:09 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Active-High Enable, Scan Latch; Q, QB Outputs
// Q  = (G | SE) ?  D : 'p';QB = !Q
module SLATNB2 (Q, QB, D, G, SE);
	output Q;
	output QB;
	input  D;
	input  G;
	input  SE;
	reg notifier;
	or _i0 (_n1,G,SE);
	p_latch _i1 (Q, D, _n1, notifier);
	not _i2 (QB,Q);
`ifdef no_tchk
`else
	not _wi0 (shcheckGDhl,SE);
	not _wi1 (shcheckSEDhl,G);
`endif
	specify		(D => Q) = (0,0);
	(D => QB) = (0,0);
	(G => Q) = (1,1);
	(G => QB) = (1,1);
	(SE => Q) = (1,1);
	(SE => QB) = (1,1);
`ifdef no_tchk
`else
	$hold(negedge G &&& shcheckGDhl === 1'b1,negedge D,0,notifier);
	$hold(negedge G &&& shcheckGDhl === 1'b1,posedge D,0,notifier);
	$hold(negedge SE &&& shcheckSEDhl === 1'b1,negedge D,0,notifier);
	$hold(negedge SE &&& shcheckSEDhl === 1'b1,posedge D,0,notifier);
	$setup(negedge D,negedge G &&& shcheckGDhl === 1'b1,0,notifier);
	$setup(negedge D,negedge SE &&& shcheckSEDhl === 1'b1,0,notifier);
	$setup(posedge D,negedge G &&& shcheckGDhl === 1'b1,0,notifier);
	$setup(posedge D,negedge SE &&& shcheckSEDhl === 1'b1,0,notifier);
	$width(posedge G,1,0,notifier);
	$width(posedge SE,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       SLATPB1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:07 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Active-Low Enable, Scan Latch; Q, QB Outputs
// Q  = !(GB & SEB) ?  D : 'p';QB = !Q
module SLATPB1 (Q, QB, D, GB, SEB);
	output Q;
	output QB;
	input  D;
	input  GB;
	input  SEB;
	reg notifier;
	nand _i0 (_n1,GB,SEB);
	p_latch _i1 (Q, D, _n1, notifier);
	not _i2 (QB,Q);
`ifdef no_tchk
`else
	buf _wi0 (shcheckGBDlh,SEB);
	buf _wi1 (shcheckSEBDlh,GB);
`endif
	specify		(D => Q) = (0,0);
	(D => QB) = (0,0);
	(GB => Q) = (1,1);
	(GB => QB) = (1,1);
	(SEB => Q) = (1,1);
	(SEB => QB) = (1,1);
`ifdef no_tchk
`else
	$hold(posedge GB &&& shcheckGBDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge GB &&& shcheckGBDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge SEB &&& shcheckSEBDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge SEB &&& shcheckSEBDlh === 1'b1,posedge D,0,notifier);
	$setup(negedge D,posedge GB &&& shcheckGBDlh === 1'b1,0,notifier);
	$setup(negedge D,posedge SEB &&& shcheckSEBDlh === 1'b1,0,notifier);
	$setup(posedge D,posedge GB &&& shcheckGBDlh === 1'b1,0,notifier);
	$setup(posedge D,posedge SEB &&& shcheckSEBDlh === 1'b1,0,notifier);
	$width(negedge GB,1,0,notifier);
	$width(negedge SEB,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       SLATPB2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:10 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Active-Low Enable, Scan Latch; Q, QB Outputs
// Q  = !(GB & SEB) ?  D : 'p';QB = !Q
module SLATPB2 (Q, QB, D, GB, SEB);
	output Q;
	output QB;
	input  D;
	input  GB;
	input  SEB;
	reg notifier;
	nand _i0 (_n1,GB,SEB);
	p_latch _i1 (Q, D, _n1, notifier);
	not _i2 (QB,Q);
`ifdef no_tchk
`else
	buf _wi0 (shcheckGBDlh,SEB);
	buf _wi1 (shcheckSEBDlh,GB);
`endif
	specify		(D => Q) = (0,0);
	(D => QB) = (0,0);
	(GB => Q) = (1,1);
	(GB => QB) = (1,1);
	(SEB => Q) = (1,1);
	(SEB => QB) = (1,1);
`ifdef no_tchk
`else
	$hold(posedge GB &&& shcheckGBDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge GB &&& shcheckGBDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge SEB &&& shcheckSEBDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge SEB &&& shcheckSEBDlh === 1'b1,posedge D,0,notifier);
	$setup(negedge D,posedge GB &&& shcheckGBDlh === 1'b1,0,notifier);
	$setup(negedge D,posedge SEB &&& shcheckSEBDlh === 1'b1,0,notifier);
	$setup(posedge D,posedge GB &&& shcheckGBDlh === 1'b1,0,notifier);
	$setup(posedge D,posedge SEB &&& shcheckSEBDlh === 1'b1,0,notifier);
	$width(negedge GB,1,0,notifier);
	$width(negedge SEB,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       SLATPQ1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:10 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Active-Low Enable, Scan Latch; Q Output
// Q  = !(GB & SEB) ?  D : 'p'
module SLATPQ1 (Q, D, GB, SEB);
	output Q;
	input  D;
	input  GB;
	input  SEB;
	reg notifier;
	nand _i0 (_n1,GB,SEB);
	p_latch _i1 (Q, D, _n1, notifier);
`ifdef no_tchk
`else
	buf _wi0 (shcheckGBDlh,SEB);
	buf _wi1 (shcheckSEBDlh,GB);
`endif
	specify		(D => Q) = (0,0);
	(GB => Q) = (1,1);
	(SEB => Q) = (1,1);
`ifdef no_tchk
`else
	$hold(posedge GB &&& shcheckGBDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge GB &&& shcheckGBDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge SEB &&& shcheckSEBDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge SEB &&& shcheckSEBDlh === 1'b1,posedge D,0,notifier);
	$setup(negedge D,posedge GB &&& shcheckGBDlh === 1'b1,0,notifier);
	$setup(negedge D,posedge SEB &&& shcheckSEBDlh === 1'b1,0,notifier);
	$setup(posedge D,posedge GB &&& shcheckGBDlh === 1'b1,0,notifier);
	$setup(posedge D,posedge SEB &&& shcheckSEBDlh === 1'b1,0,notifier);
	$width(negedge GB,1,0,notifier);
	$width(negedge SEB,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       SLATPQ2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:09 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Active-Low Enable, Scan Latch; Q Output
// Q  = !(GB & SEB) ?  D : 'p'
module SLATPQ2 (Q, D, GB, SEB);
	output Q;
	input  D;
	input  GB;
	input  SEB;
	reg notifier;
	nand _i0 (_n1,GB,SEB);
	p_latch _i1 (Q, D, _n1, notifier);
`ifdef no_tchk
`else
	buf _wi0 (shcheckGBDlh,SEB);
	buf _wi1 (shcheckSEBDlh,GB);
`endif
	specify		(D => Q) = (0,0);
	(GB => Q) = (1,1);
	(SEB => Q) = (1,1);
`ifdef no_tchk
`else
	$hold(posedge GB &&& shcheckGBDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge GB &&& shcheckGBDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge SEB &&& shcheckSEBDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge SEB &&& shcheckSEBDlh === 1'b1,posedge D,0,notifier);
	$setup(negedge D,posedge GB &&& shcheckGBDlh === 1'b1,0,notifier);
	$setup(negedge D,posedge SEB &&& shcheckSEBDlh === 1'b1,0,notifier);
	$setup(posedge D,posedge GB &&& shcheckGBDlh === 1'b1,0,notifier);
	$setup(posedge D,posedge SEB &&& shcheckSEBDlh === 1'b1,0,notifier);
	$width(negedge GB,1,0,notifier);
	$width(negedge SEB,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       SLATRNB1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:07 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Active-High Enable, Scan Latch with Clear; Q, QB Outputs
// Q  = !RB ? 0 : (G | SE) ?  D : 'p';QB = !Q
module SLATRNB1 (Q, QB, D, G, RB, SE);
	output Q;
	output QB;
	input  D;
	input  G;
	input  RB;
	input  SE;
	reg notifier;
	or _i0 (_n1,G,SE);
	not _i1 (_n2,RB);
	p_latchr _i2 (Q, D, _n1, _n2, notifier);
	not _i3 (QB,Q);
`ifdef no_tchk
`else
	not _wi0 (_wn1,SE);
	and _wi1 (shcheckGDhl,RB,_wn1);
	and _wi3 (shcheckGRBhl,D,_wn1);
	not _wi4 (_wn5,G);
	and _wi5 (shcheckSERBhl,D,_wn5);
	and _wi7 (shcheckSEDhl,_wn5,RB);
`endif
	specify		(D => Q) = (0,0);
	(D => QB) = (0,0);
	(G => Q) = (1,1);
	(G => QB) = (1,1);
	(RB => Q) = (0,0);
	(RB => QB) = (0,0);
	(SE => Q) = (1,1);
	(SE => QB) = (1,1);
`ifdef no_tchk
`else
	$hold(negedge G &&& shcheckGDhl === 1'b1,negedge D,0,notifier);
	$hold(negedge G &&& shcheckGDhl === 1'b1,posedge D,0,notifier);
	$hold(negedge G,posedge RB,0,notifier);
	$hold(negedge SE &&& shcheckSEDhl === 1'b1,negedge D,0,notifier);
	$hold(negedge SE &&& shcheckSEDhl === 1'b1,posedge D,0,notifier);
	$hold(negedge SE,posedge RB,0,notifier);
	$setup(negedge D,negedge G &&& shcheckGDhl === 1'b1,0,notifier);
	$setup(negedge D,negedge SE &&& shcheckSEDhl === 1'b1,0,notifier);
	$setup(posedge D,negedge G &&& shcheckGDhl === 1'b1,0,notifier);
	$setup(posedge D,negedge SE &&& shcheckSEDhl === 1'b1,0,notifier);
	$setup(posedge RB,negedge G &&& shcheckGRBhl === 1'b1,0,notifier);
	$setup(posedge RB,negedge SE &&& shcheckSERBhl === 1'b1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(posedge G,1,0,notifier);
	$width(posedge SE,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       SLATRNB2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:13 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Active-High Enable, Scan Latch with Clear; Q, QB Outputs
// Q  = !RB ? 0 : (G | SE) ?  D : 'p';QB = !Q
module SLATRNB2 (Q, QB, D, G, RB, SE);
	output Q;
	output QB;
	input  D;
	input  G;
	input  RB;
	input  SE;
	reg notifier;
	or _i0 (_n1,G,SE);
	not _i1 (_n2,RB);
	p_latchr _i2 (Q, D, _n1, _n2, notifier);
	not _i3 (QB,Q);
`ifdef no_tchk
`else
	not _wi0 (_wn1,SE);
	and _wi1 (shcheckGDhl,RB,_wn1);
	and _wi3 (shcheckGRBhl,D,_wn1);
	not _wi4 (_wn5,G);
	and _wi5 (shcheckSERBhl,D,_wn5);
	and _wi7 (shcheckSEDhl,_wn5,RB);
`endif
	specify		(D => Q) = (0,0);
	(D => QB) = (0,0);
	(G => Q) = (1,1);
	(G => QB) = (1,1);
	(RB => Q) = (0,0);
	(RB => QB) = (0,0);
	(SE => Q) = (1,1);
	(SE => QB) = (1,1);
`ifdef no_tchk
`else
	$hold(negedge G &&& shcheckGDhl === 1'b1,negedge D,0,notifier);
	$hold(negedge G &&& shcheckGDhl === 1'b1,posedge D,0,notifier);
	$hold(negedge G,posedge RB,0,notifier);
	$hold(negedge SE &&& shcheckSEDhl === 1'b1,negedge D,0,notifier);
	$hold(negedge SE &&& shcheckSEDhl === 1'b1,posedge D,0,notifier);
	$hold(negedge SE,posedge RB,0,notifier);
	$setup(negedge D,negedge G &&& shcheckGDhl === 1'b1,0,notifier);
	$setup(negedge D,negedge SE &&& shcheckSEDhl === 1'b1,0,notifier);
	$setup(posedge D,negedge G &&& shcheckGDhl === 1'b1,0,notifier);
	$setup(posedge D,negedge SE &&& shcheckSEDhl === 1'b1,0,notifier);
	$setup(posedge RB,negedge G &&& shcheckGRBhl === 1'b1,0,notifier);
	$setup(posedge RB,negedge SE &&& shcheckSERBhl === 1'b1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(posedge G,1,0,notifier);
	$width(posedge SE,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       SLATRPB1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:09 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Active-Low Enable, Scan Latch with Clear; Q, QB Outputs
// Q  = !RB ? 0 : !(GB & SEB) ?  D : 'p';QB = !Q
module SLATRPB1 (Q, QB, D, GB, RB, SEB);
	output Q;
	output QB;
	input  D;
	input  GB;
	input  RB;
	input  SEB;
	reg notifier;
	nand _i0 (_n1,GB,SEB);
	not _i1 (_n2,RB);
	p_latchr _i2 (Q, D, _n1, _n2, notifier);
	not _i3 (QB,Q);
`ifdef no_tchk
`else
	and _wi0 (shcheckGBDlh,RB,SEB);
	and _wi1 (shcheckSEBRBlh,D,GB);
	and _wi2 (shcheckGBRBlh,D,SEB);
	and _wi3 (shcheckSEBDlh,GB,RB);
`endif
	specify		(D => Q) = (0,0);
	(D => QB) = (0,0);
	(GB => Q) = (1,1);
	(GB => QB) = (1,1);
	(RB => Q) = (0,0);
	(RB => QB) = (0,0);
	(SEB => Q) = (1,1);
	(SEB => QB) = (1,1);
`ifdef no_tchk
`else
	$hold(posedge GB &&& shcheckGBDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge GB &&& shcheckGBDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge GB,posedge RB,0,notifier);
	$hold(posedge SEB &&& shcheckSEBDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge SEB &&& shcheckSEBDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge SEB,posedge RB,0,notifier);
	$setup(negedge D,posedge GB &&& shcheckGBDlh === 1'b1,0,notifier);
	$setup(negedge D,posedge SEB &&& shcheckSEBDlh === 1'b1,0,notifier);
	$setup(posedge D,posedge GB &&& shcheckGBDlh === 1'b1,0,notifier);
	$setup(posedge D,posedge SEB &&& shcheckSEBDlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge GB &&& shcheckGBRBlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge SEB &&& shcheckSEBRBlh === 1'b1,0,notifier);
	$width(negedge GB,1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(negedge SEB,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       SLATRPB2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:07 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Active-Low Enable, Scan Latch with Clear; Q, QB Outputs
// Q  = !RB ? 0 : !(GB & SEB) ?  D : 'p';QB = !Q
module SLATRPB2 (Q, QB, D, GB, RB, SEB);
	output Q;
	output QB;
	input  D;
	input  GB;
	input  RB;
	input  SEB;
	reg notifier;
	nand _i0 (_n1,GB,SEB);
	not _i1 (_n2,RB);
	p_latchr _i2 (Q, D, _n1, _n2, notifier);
	not _i3 (QB,Q);
`ifdef no_tchk
`else
	and _wi0 (shcheckGBDlh,RB,SEB);
	and _wi1 (shcheckSEBRBlh,D,GB);
	and _wi2 (shcheckGBRBlh,D,SEB);
	and _wi3 (shcheckSEBDlh,GB,RB);
`endif
	specify		(D => Q) = (0,0);
	(D => QB) = (0,0);
	(GB => Q) = (1,1);
	(GB => QB) = (1,1);
	(RB => Q) = (0,0);
	(RB => QB) = (0,0);
	(SEB => Q) = (1,1);
	(SEB => QB) = (1,1);
`ifdef no_tchk
`else
	$hold(posedge GB &&& shcheckGBDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge GB &&& shcheckGBDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge GB,posedge RB,0,notifier);
	$hold(posedge SEB &&& shcheckSEBDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge SEB &&& shcheckSEBDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge SEB,posedge RB,0,notifier);
	$setup(negedge D,posedge GB &&& shcheckGBDlh === 1'b1,0,notifier);
	$setup(negedge D,posedge SEB &&& shcheckSEBDlh === 1'b1,0,notifier);
	$setup(posedge D,posedge GB &&& shcheckGBDlh === 1'b1,0,notifier);
	$setup(posedge D,posedge SEB &&& shcheckSEBDlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge GB &&& shcheckGBRBlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge SEB &&& shcheckSEBRBlh === 1'b1,0,notifier);
	$width(negedge GB,1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(negedge SEB,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       SLATRSNB1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:10 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Active-High Enable, Scan Latch with Set, Clear; Q, QB Outputs
// Q  = !RB ? 0 : !SB ? 1 : (G | SE) ?  D : 'p';QB = !Q & SB
module SLATRSNB1 (Q, QB, D, G, RB, SB, SE);
	output Q;
	output QB;
	input  D;
	input  G;
	input  RB;
	input  SB;
	input  SE;
	reg notifier;
	or _i0 (_n1,G,SE);
	not _i1 (_n2,SB);
	not _i2 (_n3,RB);
	p_latchrs _i3 (Q, D, _n1, _n2, _n3, notifier);
	not _i4 (_n5,Q);
	and _i5 (QB,_n5,SB);
`ifdef no_tchk
`else
	not _wi0 (_wn1,SE);
	and _wi1 (shcheckGDhl,_wn1,RB,SB);
	not _wi3 (_wn4,D);
	and _wi4 (shcheckGSBhl,_wn1,_wn4,RB);
	not _wi5 (_wn6,G);
	and _wi7 (shcheckSBRBlh,_wn6,_wn1);
	and _wi10 (shcheckSESBhl,RB,_wn4,_wn6);
	and _wi12 (shcheckGRBhl,_wn1,D,SB);
	and _wi14 (shcheckSERBhl,SB,D,_wn6);
	and _wi16 (shcheckSEDhl,SB,_wn6,RB);
`endif
	specify		(D => Q) = (0,0);
	(D => QB) = (0,0);
	(G => Q) = (1,1);
	(G => QB) = (1,1);
	(RB => Q) = (0,0);
	(RB => QB) = (0,0);
	(SB => Q) = (0,0);
	(SB => QB) = (0,0);
	(SE => Q) = (1,1);
	(SE => QB) = (1,1);
`ifdef no_tchk
`else
	$hold(negedge G &&& shcheckGDhl === 1'b1,negedge D,0,notifier);
	$hold(negedge G &&& shcheckGDhl === 1'b1,posedge D,0,notifier);
	$hold(negedge G,posedge RB,0,notifier);
	$hold(negedge G,posedge SB,0,notifier);
	$hold(negedge SE &&& shcheckSEDhl === 1'b1,negedge D,0,notifier);
	$hold(negedge SE &&& shcheckSEDhl === 1'b1,posedge D,0,notifier);
	$hold(negedge SE,posedge RB,0,notifier);
	$hold(negedge SE,posedge SB,0,notifier);
	$hold(posedge SB,posedge RB,0,notifier);
	$setup(negedge D,negedge G &&& shcheckGDhl === 1'b1,0,notifier);
	$setup(negedge D,negedge SE &&& shcheckSEDhl === 1'b1,0,notifier);
	$setup(posedge D,negedge G &&& shcheckGDhl === 1'b1,0,notifier);
	$setup(posedge D,negedge SE &&& shcheckSEDhl === 1'b1,0,notifier);
	$setup(posedge RB,negedge G &&& shcheckGRBhl === 1'b1,0,notifier);
	$setup(posedge RB,negedge SE &&& shcheckSERBhl === 1'b1,0,notifier);
	$setup(posedge RB,posedge SB &&& shcheckSBRBlh === 1'b1,0,notifier);
	$setup(posedge SB,negedge G &&& shcheckGSBhl === 1'b1,0,notifier);
	$setup(posedge SB,negedge SE &&& shcheckSESBhl === 1'b1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(negedge SB,1,0,notifier);
	$width(posedge G,1,0,notifier);
	$width(posedge SE,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       SLATRSNB2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:09 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Active-High Enable, Scan Latch with Set, Clear; Q, QB Outputs
// Q  = !RB ? 0 : !SB ? 1 : (G | SE) ?  D : 'p';QB = !Q & SB
module SLATRSNB2 (Q, QB, D, G, RB, SB, SE);
	output Q;
	output QB;
	input  D;
	input  G;
	input  RB;
	input  SB;
	input  SE;
	reg notifier;
	or _i0 (_n1,G,SE);
	not _i1 (_n2,SB);
	not _i2 (_n3,RB);
	p_latchrs _i3 (Q, D, _n1, _n2, _n3, notifier);
	not _i4 (_n5,Q);
	and _i5 (QB,_n5,SB);
`ifdef no_tchk
`else
	not _wi0 (_wn1,SE);
	and _wi1 (shcheckGDhl,_wn1,RB,SB);
	not _wi3 (_wn4,D);
	and _wi4 (shcheckGSBhl,_wn1,_wn4,RB);
	not _wi5 (_wn6,G);
	and _wi7 (shcheckSBRBlh,_wn6,_wn1);
	and _wi10 (shcheckSESBhl,RB,_wn4,_wn6);
	and _wi12 (shcheckGRBhl,_wn1,D,SB);
	and _wi14 (shcheckSERBhl,SB,D,_wn6);
	and _wi16 (shcheckSEDhl,SB,_wn6,RB);
`endif
	specify		(D => Q) = (0,0);
	(D => QB) = (0,0);
	(G => Q) = (1,1);
	(G => QB) = (1,1);
	(RB => Q) = (0,0);
	(RB => QB) = (0,0);
	(SB => Q) = (0,0);
	(SB => QB) = (0,0);
	(SE => Q) = (1,1);
	(SE => QB) = (1,1);
`ifdef no_tchk
`else
	$hold(negedge G &&& shcheckGDhl === 1'b1,negedge D,0,notifier);
	$hold(negedge G &&& shcheckGDhl === 1'b1,posedge D,0,notifier);
	$hold(negedge G,posedge RB,0,notifier);
	$hold(negedge G,posedge SB,0,notifier);
	$hold(negedge SE &&& shcheckSEDhl === 1'b1,negedge D,0,notifier);
	$hold(negedge SE &&& shcheckSEDhl === 1'b1,posedge D,0,notifier);
	$hold(negedge SE,posedge RB,0,notifier);
	$hold(negedge SE,posedge SB,0,notifier);
	$hold(posedge SB,posedge RB,0,notifier);
	$setup(negedge D,negedge G &&& shcheckGDhl === 1'b1,0,notifier);
	$setup(negedge D,negedge SE &&& shcheckSEDhl === 1'b1,0,notifier);
	$setup(posedge D,negedge G &&& shcheckGDhl === 1'b1,0,notifier);
	$setup(posedge D,negedge SE &&& shcheckSEDhl === 1'b1,0,notifier);
	$setup(posedge RB,negedge G &&& shcheckGRBhl === 1'b1,0,notifier);
	$setup(posedge RB,negedge SE &&& shcheckSERBhl === 1'b1,0,notifier);
	$setup(posedge RB,posedge SB &&& shcheckSBRBlh === 1'b1,0,notifier);
	$setup(posedge SB,negedge G &&& shcheckGSBhl === 1'b1,0,notifier);
	$setup(posedge SB,negedge SE &&& shcheckSESBhl === 1'b1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(negedge SB,1,0,notifier);
	$width(posedge G,1,0,notifier);
	$width(posedge SE,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       SLATRSPB1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:07 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Active-Low Enable, Scan Latch with Set, Clear; Q, QB Outputs
// Q  = !RB ? 0 : !SB ? 1 : !(GB & SEB) ?  D : 'p';QB = !Q & SB
module SLATRSPB1 (Q, QB, D, GB, RB, SB, SEB);
	output Q;
	output QB;
	input  D;
	input  GB;
	input  RB;
	input  SB;
	input  SEB;
	reg notifier;
	nand _i0 (_n1,GB,SEB);
	not _i1 (_n2,SB);
	not _i2 (_n3,RB);
	p_latchrs _i3 (Q, D, _n1, _n2, _n3, notifier);
	not _i4 (_n5,Q);
	and _i5 (QB,_n5,SB);
`ifdef no_tchk
`else
	and _wi0 (shcheckSBRBlh,GB,SEB);
	and _wi1 (shcheckSEBRBlh,SB,D,GB);
	not _wi2 (_wn3,D);
	and _wi3 (shcheckSEBSBlh,RB,_wn3,GB);
	and _wi5 (shcheckGBSBlh,SEB,_wn3,RB);
	and _wi6 (shcheckGBDlh,SEB,RB,SB);
	and _wi7 (shcheckGBRBlh,SEB,D,SB);
	and _wi8 (shcheckSEBDlh,SB,GB,RB);
`endif
	specify		(D => Q) = (0,0);
	(D => QB) = (0,0);
	(GB => Q) = (1,1);
	(GB => QB) = (1,1);
	(RB => Q) = (0,0);
	(RB => QB) = (0,0);
	(SB => Q) = (0,0);
	(SB => QB) = (0,0);
	(SEB => Q) = (1,1);
	(SEB => QB) = (1,1);
`ifdef no_tchk
`else
	$hold(posedge GB &&& shcheckGBDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge GB &&& shcheckGBDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge GB,posedge RB,0,notifier);
	$hold(posedge GB,posedge SB,0,notifier);
	$hold(posedge SB,posedge RB,0,notifier);
	$hold(posedge SEB &&& shcheckSEBDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge SEB &&& shcheckSEBDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge SEB,posedge RB,0,notifier);
	$hold(posedge SEB,posedge SB,0,notifier);
	$setup(negedge D,posedge GB &&& shcheckGBDlh === 1'b1,0,notifier);
	$setup(negedge D,posedge SEB &&& shcheckSEBDlh === 1'b1,0,notifier);
	$setup(posedge D,posedge GB &&& shcheckGBDlh === 1'b1,0,notifier);
	$setup(posedge D,posedge SEB &&& shcheckSEBDlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge GB &&& shcheckGBRBlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge SB &&& shcheckSBRBlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge SEB &&& shcheckSEBRBlh === 1'b1,0,notifier);
	$setup(posedge SB,posedge GB &&& shcheckGBSBlh === 1'b1,0,notifier);
	$setup(posedge SB,posedge SEB &&& shcheckSEBSBlh === 1'b1,0,notifier);
	$width(negedge GB,1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(negedge SB,1,0,notifier);
	$width(negedge SEB,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       SLATRSPB2.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:10 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 08/20/99 16:28:41
`celldefine
// Active-Low Enable, Scan Latch with Set, Clear; Q, QB Outputs
// Q  = !RB ? 0 : !SB ? 1 : !(GB & SEB) ?  D : 'p';QB = !Q & SB
module SLATRSPB2 (Q, QB, D, GB, RB, SB, SEB);
	output Q;
	output QB;
	input  D;
	input  GB;
	input  RB;
	input  SB;
	input  SEB;
	reg notifier;
	nand _i0 (_n1,GB,SEB);
	not _i1 (_n2,SB);
	not _i2 (_n3,RB);
	p_latchrs _i3 (Q, D, _n1, _n2, _n3, notifier);
	not _i4 (_n5,Q);
	and _i5 (QB,_n5,SB);
`ifdef no_tchk
`else
	and _wi0 (shcheckSBRBlh,GB,SEB);
	and _wi1 (shcheckSEBRBlh,SB,D,GB);
	not _wi2 (_wn3,D);
	and _wi3 (shcheckSEBSBlh,RB,_wn3,GB);
	and _wi5 (shcheckGBSBlh,SEB,_wn3,RB);
	and _wi6 (shcheckGBDlh,SEB,RB,SB);
	and _wi7 (shcheckGBRBlh,SEB,D,SB);
	and _wi8 (shcheckSEBDlh,SB,GB,RB);
`endif
	specify		(D => Q) = (0,0);
	(D => QB) = (0,0);
	(GB => Q) = (1,1);
	(GB => QB) = (1,1);
	(RB => Q) = (0,0);
	(RB => QB) = (0,0);
	(SB => Q) = (0,0);
	(SB => QB) = (0,0);
	(SEB => Q) = (1,1);
	(SEB => QB) = (1,1);
`ifdef no_tchk
`else
	$hold(posedge GB &&& shcheckGBDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge GB &&& shcheckGBDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge GB,posedge RB,0,notifier);
	$hold(posedge GB,posedge SB,0,notifier);
	$hold(posedge SB,posedge RB,0,notifier);
	$hold(posedge SEB &&& shcheckSEBDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge SEB &&& shcheckSEBDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge SEB,posedge RB,0,notifier);
	$hold(posedge SEB,posedge SB,0,notifier);
	$setup(negedge D,posedge GB &&& shcheckGBDlh === 1'b1,0,notifier);
	$setup(negedge D,posedge SEB &&& shcheckSEBDlh === 1'b1,0,notifier);
	$setup(posedge D,posedge GB &&& shcheckGBDlh === 1'b1,0,notifier);
	$setup(posedge D,posedge SEB &&& shcheckSEBDlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge GB &&& shcheckGBRBlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge SB &&& shcheckSBRBlh === 1'b1,0,notifier);
	$setup(posedge RB,posedge SEB &&& shcheckSEBRBlh === 1'b1,0,notifier);
	$setup(posedge SB,posedge GB &&& shcheckGBSBlh === 1'b1,0,notifier);
	$setup(posedge SB,posedge SEB &&& shcheckSEBSBlh === 1'b1,0,notifier);
	$width(negedge GB,1,0,notifier);
	$width(negedge RB,1,0,notifier);
	$width(negedge SB,1,0,notifier);
	$width(negedge SEB,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       SUBFULD1.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:10 
//  
// 
// $RCSfile: stdcell.v,v $ 
// $Source: /Users/kurtstine/Downloads/depot/hw/lib/nintendo/DI/hw/lib/stdcell.v,v $ 
// $Date: 2004/03/29 20:21:24 $ 
// $Revision: 1.1 $ 
// 
// ---------------------- 
// verigen patch-level 1.131-y 05/24/99 12:18:16
`celldefine
// 1-Bit Full Subtractor
// S  = ( ( A ^ !B ) ^ CI  );CO = ( A & !B ) | ( CI & (A | !B) )
module SUBFULD1 (CO, S, A, B, CI);
	output CO;
	output S;
	input  A;
	input  B;
	input  CI;
	not _i0 (_n1,B);
	xor _i1 (S,CI,A,_n1);
	and _i3 (_n3,A,_n1);
	or _i5 (_n6,A,_n1);
	and _i6 (_n5,CI,_n6);
	or _i7 (CO,_n3,_n5);
	specify		(A => CO) = (0,0);
	(A => S) = (0,0);
	(B => CO) = (0,0);
	(B => S) = (0,0);
	(CI => CO) = (0,0);
	(CI => S) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       SYNC.v 
// Library Name:    pez_d18_sc_necned3_c7 
// Library Release: 1.2 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/29/99 17:32:43 
//  
// verigen patch-level 1.131-x 05/11/99 19:44:59
`celldefine
// custom cell
// EO  = rising(CK) ?  D : 'p';Q  = rising(CK) ?  EI : 'p'
module SYNC (EO, Q, CK, D, EI);
	output EO;
	output Q;
	input  CK;
	input  D;
	input  EI;
	reg notifier;
	p_ff _i0 (EO, D, CK, notifier);
	p_ff _i1 (Q, EI, CK, notifier);
`ifdef no_tchk
`else
	not _wi0 (shcheckCKDlh,EI);
	not _wi1 (shcheckCKEIlh,D);
`endif
	specify		(CK => EO) = (1,1);
	(CK => Q) = (1,1);
`ifdef no_tchk
`else
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKEIlh === 1'b1,negedge EI,0,notifier);
	$hold(posedge CK &&& shcheckCKEIlh === 1'b1,posedge EI,0,notifier);
	$setup(negedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(negedge EI,posedge CK &&& shcheckCKEIlh === 1'b1,0,notifier);
	$setup(posedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge EI,posedge CK &&& shcheckCKEIlh === 1'b1,0,notifier);
	$width(negedge CK,1,0,notifier);
	$width(posedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       WCKWED4.v 
// Library Name:    pez_d18_sc_necned3_c7 
// Library Release: 1.2 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/29/99 17:32:42 
//  
// verigen patch-level 1.131-x 05/11/99 19:44:59
`celldefine
// Custom cell
// EO  = rising(CK) ?  (SE ? SD : D) : 'p';WE = !(DCK & EI)
module WCKWED4 (EO, WE, CK, D, DCK, EI, SD, SE);
	output EO;
	output WE;
	input  CK;
	input  D;
	input  DCK;
	input  EI;
	input  SD;
	input  SE;
	reg notifier;
	p_mux21 _i0 (_n1, SD, D, SE);
	p_ff _i1 (EO, _n1, CK, notifier);
	nand _i2 (WE,DCK,EI);
`ifdef no_tchk
`else
	not _wi0 (_wn1,SD);
	not _wi1 (_wn2,EI);
	not _wi2 (_wn3,DCK);
	and _wi3 (shcheckCKSElh,_wn1,_wn2,D,_wn3);
	not _wi4 (_wn5,SE);
	and _wi8 (shcheckCKDlh,_wn5,_wn1,_wn3,_wn2);
	and _wi10 (shcheckCKSDlh,SE,EI,D,_wn3);
`endif
	specify		(CK => EO) = (1,1);
	(DCK => WE) = (0,0);
	(EI => WE) = (0,0);
`ifdef no_tchk
`else
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,negedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSDlh === 1'b1,posedge SD,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,negedge SE,0,notifier);
	$hold(posedge CK &&& shcheckCKSElh === 1'b1,posedge SE,0,notifier);
	$setup(negedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(negedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(negedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$setup(posedge D,posedge CK &&& shcheckCKDlh === 1'b1,0,notifier);
	$setup(posedge SD,posedge CK &&& shcheckCKSDlh === 1'b1,0,notifier);
	$setup(posedge SE,posedge CK &&& shcheckCKSElh === 1'b1,0,notifier);
	$width(negedge CK,1,0,notifier);
	$width(posedge CK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       WCKWLD2.v 
// Library Name:    pez_d18_sc_necned3_c7 
// Library Release: 1.2 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/29/99 17:32:41 
//  
// verigen patch-level 1.131-x 05/11/99 19:44:59
`celldefine
// Custom Cell
// EO  = rising(WCK) ?  ! (SE ? SD : D) : 'p';WL = (!WE & !EI ) | SE
module WCKWLD2 (EO, WL, D, EI, SD, SE, WCK, WE);
	output EO;
	output WL;
	input  D;
	input  EI;
	input  SD;
	input  SE;
	input  WCK;
	input  WE;
	reg notifier;
	p_mux21 _i0 (_n2, SD, D, SE);
	not _i1 (_n1,_n2);
	p_ff _i2 (EO, _n1, WCK, notifier);
	not _i3 (_n5,WE);
	not _i4 (_n6,EI);
	and _i5 (_n4,_n5,_n6);
	or _i6 (WL,_n4,SE);
`ifdef no_tchk
`else
	not _wi0 (_wn1,WE);
	not _wi1 (_wn2,SE);
	and _wi2 (shcheckWCKDlh,_wn1,_wn2,EI,SD);
	and _wi3 (shcheckWCKSDlh,WE,SE,D,EI);
	not _wi5 (_wn6,SD);
	and _wi6 (shcheckWCKSElh,_wn1,_wn6,D,EI);
`endif
	specify		(EI => WL) = (0,0);
	(SE => WL) = (0,0);
	(WCK => EO) = (1,1);
	(WE => WL) = (0,0);
`ifdef no_tchk
`else
	$hold(posedge WCK &&& shcheckWCKDlh === 1'b1,negedge D,0,notifier);
	$hold(posedge WCK &&& shcheckWCKDlh === 1'b1,posedge D,0,notifier);
	$hold(posedge WCK &&& shcheckWCKSDlh === 1'b1,negedge SD,0,notifier);
	$hold(posedge WCK &&& shcheckWCKSDlh === 1'b1,posedge SD,0,notifier);
	$hold(posedge WCK &&& shcheckWCKSElh === 1'b1,negedge SE,0,notifier);
	$hold(posedge WCK &&& shcheckWCKSElh === 1'b1,posedge SE,0,notifier);
	$setup(negedge D,posedge WCK &&& shcheckWCKDlh === 1'b1,0,notifier);
	$setup(negedge SD,posedge WCK &&& shcheckWCKSDlh === 1'b1,0,notifier);
	$setup(negedge SE,posedge WCK &&& shcheckWCKSElh === 1'b1,0,notifier);
	$setup(posedge D,posedge WCK &&& shcheckWCKDlh === 1'b1,0,notifier);
	$setup(posedge SD,posedge WCK &&& shcheckWCKSDlh === 1'b1,0,notifier);
	$setup(posedge SE,posedge WCK &&& shcheckWCKSElh === 1'b1,0,notifier);
	$width(negedge WCK,1,0,notifier);
	$width(posedge WCK,1,0,notifier);
`endif
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       XADFULD1.v 
// Library Name:    pez_d18_sc_necned3_c7 
// Library Release: 1.2 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/29/99 17:32:42 
//  
// verigen patch-level 1.131-x 05/11/99 19:44:59
`celldefine
// 1-Bit Full Adder
// S  = ((A ^ B) ^ CI);CO = (A & B) | (CI & (A | B))
module XADFULD1 (CO, S, A, B, CI);
	output CO;
	output S;
	input  A;
	input  B;
	input  CI;
	xor _i0 (S,CI,A,B);
	and _i1 (_n2,A,B);
	or _i2 (_n4,A,B);
	and _i3 (_n3,CI,_n4);
	or _i4 (CO,_n2,_n3);
	specify		(A => CO) = (0,0);
	(A => S) = (0,0);
	(B => CO) = (0,0);
	(B => S) = (0,0);
	(CI => CO) = (0,0);
	(CI => S) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       XADFULD2.v 
// Library Name:    pez_d18_sc_necned3_c7 
// Library Release: 1.2 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/29/99 17:32:42 
//  
// verigen patch-level 1.131-x 05/11/99 19:44:59
`celldefine
// 1-Bit Full Adder
// S  = ((A ^ B) ^ CI);CO = (A & B) | (CI & (A | B))
module XADFULD2 (CO, S, A, B, CI);
	output CO;
	output S;
	input  A;
	input  B;
	input  CI;
	xor _i0 (S,CI,A,B);
	and _i1 (_n2,A,B);
	or _i2 (_n4,A,B);
	and _i3 (_n3,CI,_n4);
	or _i4 (CO,_n2,_n3);
	specify		(A => CO) = (0,0);
	(A => S) = (0,0);
	(B => CO) = (0,0);
	(B => S) = (0,0);
	(CI => CO) = (0,0);
	(CI => S) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       XADFULD4.v 
// Library Name:    pez_d18_sc_necned3_c7 
// Library Release: 1.2 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/29/99 17:32:43 
//  
// verigen patch-level 1.131-x 05/11/99 19:44:59
`celldefine
// 1-Bit Full Adder
// S  = ((A ^ B) ^ CI);CO = (A & B) | (CI & (A | B))
module XADFULD4 (CO, S, A, B, CI);
	output CO;
	output S;
	input  A;
	input  B;
	input  CI;
	xor _i0 (S,CI,A,B);
	and _i1 (_n2,A,B);
	or _i2 (_n4,A,B);
	and _i3 (_n3,CI,_n4);
	or _i4 (CO,_n2,_n3);
	specify		(A => CO) = (0,0);
	(A => S) = (0,0);
	(B => CO) = (0,0);
	(B => S) = (0,0);
	(CI => CO) = (0,0);
	(CI => S) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       XADHALFD1.v 
// Library Name:    pez_d18_sc_necned3_c7 
// Library Release: 1.2 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/29/99 17:32:41 
//  
// verigen patch-level 1.131-x 05/11/99 19:44:59
`celldefine
// 1-Bit Half Adder
// S  = ( A ^ B );CO = ( A & B )
module XADHALFD1 (CO, S, A, B);
	output CO;
	output S;
	input  A;
	input  B;
	xor _i0 (S,A,B);
	and _i1 (CO,A,B);
	specify		(A => CO) = (0,0);
	(A => S) = (0,0);
	(B => CO) = (0,0);
	(B => S) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       XAOI22D1.v 
// Library Name:    pez_d18_sc_necned3_c7 
// Library Release: 1.2 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/29/99 17:32:42 
//  
// verigen patch-level 1.131-x 05/11/99 19:44:59
`celldefine
// 2-2 AND-OR-Invert Gate
// Z = ! ( ( A1 & A2  ) | ( B1 & B2 )  )
module XAOI22D1 (Z, A1, A2, B1, B2);
	output Z;
	input  A1;
	input  A2;
	input  B1;
	input  B2;
	and _i0 (_n1,A1,A2);
	and _i1 (_n2,B1,B2);
	nor _i2 (Z,_n1,_n2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       XAOI22D2.v 
// Library Name:    pez_d18_sc_necned3_c7 
// Library Release: 1.2 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/29/99 17:32:42 
//  
// verigen patch-level 1.131-x 05/11/99 19:44:59
`celldefine
// 2-2 AND-OR-Invert Gate
// Z = ! ( ( A1 & A2  ) | ( B1 & B2 )  )
module XAOI22D2 (Z, A1, A2, B1, B2);
	output Z;
	input  A1;
	input  A2;
	input  B1;
	input  B2;
	and _i0 (_n1,A1,A2);
	and _i1 (_n2,B1,B2);
	nor _i2 (Z,_n1,_n2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       XAOI22D4.v 
// Library Name:    pez_d18_sc_necned3_c7 
// Library Release: 1.2 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/29/99 17:32:42 
//  
// verigen patch-level 1.131-x 05/11/99 19:44:59
`celldefine
// 2-2 AND-OR-Invert Gate
// Z = ! ( ( A1 & A2  ) | ( B1 & B2 )  )
module XAOI22D4 (Z, A1, A2, B1, B2);
	output Z;
	input  A1;
	input  A2;
	input  B1;
	input  B2;
	and _i0 (_n1,A1,A2);
	and _i1 (_n2,B1,B2);
	nor _i2 (Z,_n1,_n2);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B1 => Z) = (0,0);
	(B2 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       XMUX4D1.v 
// Library Name:    pez_d18_sc_necned3_c7 
// Library Release: 1.2 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/29/99 17:32:41 
//  
// verigen patch-level 1.131-x 05/11/99 19:44:59
`celldefine
// 4-to-1 Multiplexer
// Z = SL1 & SL0 & A3 | SL1 & ! SL0 & A2 | ! SL1 & SL0 & A1 | ! SL1 & ! SL0 & A0
module XMUX4D1 (Z, A0, A1, A2, A3, SL0, SL1);
	output Z;
	input  A0;
	input  A1;
	input  A2;
	input  A3;
	input  SL0;
	input  SL1;
	and _i0 (_n1,A3,SL1,SL0);
	not _i1 (_n3,SL0);
	and _i2 (_n2,A2,SL1,_n3);
	not _i3 (_n5,SL1);
	and _i4 (_n4,A1,_n5,SL0);
	and _i7 (_n6,A0,_n5,_n3);
	or _i8 (Z,_n1,_n2,_n4,_n6);
	specify		(A0 => Z) = (0,0);
	(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(SL0 => Z) = (0,0);
	(SL1 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       XMUX4D2.v 
// Library Name:    pez_d18_sc_necned3_c7 
// Library Release: 1.2 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/29/99 17:32:42 
//  
// verigen patch-level 1.131-x 05/11/99 19:44:59
`celldefine
// 4-to-1 Multiplexer
// Z = SL1 & SL0 & A3 | SL1 & ! SL0 & A2 | ! SL1 & SL0 & A1 | ! SL1 & ! SL0 & A0
module XMUX4D2 (Z, A0, A1, A2, A3, SL0, SL1);
	output Z;
	input  A0;
	input  A1;
	input  A2;
	input  A3;
	input  SL0;
	input  SL1;
	and _i0 (_n1,A3,SL1,SL0);
	not _i1 (_n3,SL0);
	and _i2 (_n2,A2,SL1,_n3);
	not _i3 (_n5,SL1);
	and _i4 (_n4,A1,_n5,SL0);
	and _i7 (_n6,A0,_n5,_n3);
	or _i8 (Z,_n1,_n2,_n4,_n6);
	specify		(A0 => Z) = (0,0);
	(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(SL0 => Z) = (0,0);
	(SL1 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       XMUX4D4.v 
// Library Name:    pez_d18_sc_necned3_c7 
// Library Release: 1.2 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/29/99 17:32:41 
//  
// verigen patch-level 1.131-x 05/11/99 19:44:59
`celldefine
// 4-to-1 Multiplexer
// Z = SL1 & SL0 & A3 | SL1 & ! SL0 & A2 | ! SL1 & SL0 & A1 | ! SL1 & ! SL0 & A0
module XMUX4D4 (Z, A0, A1, A2, A3, SL0, SL1);
	output Z;
	input  A0;
	input  A1;
	input  A2;
	input  A3;
	input  SL0;
	input  SL1;
	and _i0 (_n1,A3,SL1,SL0);
	not _i1 (_n3,SL0);
	and _i2 (_n2,A2,SL1,_n3);
	not _i3 (_n5,SL1);
	and _i4 (_n4,A1,_n5,SL0);
	and _i7 (_n6,A0,_n5,_n3);
	or _i8 (Z,_n1,_n2,_n4,_n6);
	specify		(A0 => Z) = (0,0);
	(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(A3 => Z) = (0,0);
	(SL0 => Z) = (0,0);
	(SL1 => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       XOAI21D1.v 
// Library Name:    pez_d18_sc_necned3_c7 
// Library Release: 1.2 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/29/99 17:32:42 
//  
// verigen patch-level 1.131-x 05/11/99 19:44:59
`celldefine
// 1-2 OR-AND-Invert Gate
// Z = ! ( ( A1 | A2  ) & B  )
module XOAI21D1 (Z, A1, A2, B);
	output Z;
	input  A1;
	input  A2;
	input  B;
	or _i0 (_n1,A1,A2);
	nand _i1 (Z,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       XOAI21D2.v 
// Library Name:    pez_d18_sc_necned3_c7 
// Library Release: 1.2 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/29/99 17:32:43 
//  
// verigen patch-level 1.131-x 05/11/99 19:44:59
`celldefine
// 1-2 OR-AND-Invert Gate
// Z = ! ( ( A1 | A2  ) & B  )
module XOAI21D2 (Z, A1, A2, B);
	output Z;
	input  A1;
	input  A2;
	input  B;
	or _i0 (_n1,A1,A2);
	nand _i1 (Z,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       XOAI21D4.v 
// Library Name:    pez_d18_sc_necned3_c7 
// Library Release: 1.2 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/29/99 17:32:42 
//  
// verigen patch-level 1.131-x 05/11/99 19:44:59
`celldefine
// 1-2 OR-AND-Invert Gate
// Z = ! ( ( A1 | A2  ) & B  )
module XOAI21D4 (Z, A1, A2, B);
	output Z;
	input  A1;
	input  A2;
	input  B;
	or _i0 (_n1,A1,A2);
	nand _i1 (Z,_n1,B);
	specify		(A1 => Z) = (0,0);
	(A2 => Z) = (0,0);
	(B => Z) = (0,0);
	endspecify
endmodule
`endcelldefine
module decap8 ();
endmodule
module decap16 ();
endmodule
//  
//  Copyright (C) 1998 Virtual Silicon Technology Inc.. All Rights Reserved.  
//  Diplomat, Silicon Ready and IP Ambassador are trademarks of Virtual Silicon Technology Inc..  
//  
// Virtual Silicon Technology 
// 1200 Crossman Ave Suite 200 
// Sunnyvale, CA 94089-1116 
// Phone : 408-548-2700 
// Fax   : 408-548-2750 
// Web Site : www.virtual-silicon.com 
//  
// File Name:       UDPS.v 
// Library Name:    vst_d18_sc_necned3_c7 
// Library Release: 2.1 
// Process:         Diplomat-18 NEC NED3 
// Generated:       10/17/99 18:47:10 
//  
`define libtechudps 1
primitive p_mux21 (q, data1, data0, dselect);
    output q;
    input data1, data0, dselect;

// FUNCTION :  TWO TO ONE MULTIPLEXER
table
//data1 data0 dselect :   q
	0     0       ?   :   0 ;
	1     1       ?   :   1 ;

	0     ?       1   :   0 ;
	1     ?       1   :   1 ;

	?     0       0   :   0 ;
	?     1       0   :   1 ;
endtable
endprimitive

primitive mux4 (Z, A0, A1, A2, A3, SL1, SL0);
   output Z;
   input A0, A1, A2, A3, SL1, SL0;
   table
   //  A0  A1  A2  A3  SL1 SL0 n  : Z

        0   0   0   0   ?   ?  : 0;
        1   1   1   1   ?   ?  : 1;

        0   ?   ?   ?   0   0  : 0;
        1   ?   ?   ?   0   0  : 1;
        ?   0   ?   ?   0   1  : 0;
        ?   1   ?   ?   0   1  : 1;
        ?   ?   0   ?   1   0  : 0;
        ?   ?   1   ?   1   0  : 1;
        ?   ?   ?   0   1   1  : 0;
        ?   ?   ?   1   1   1  : 1;

        0   0   ?   ?   0   ?  : 0;
        1   1   ?   ?   0   ?  : 1;
        ?   ?   0   0   1   ?  : 0;
        ?   ?   1   1   1   ?  : 1;
        0   ?   0   ?   ?   0  : 0;
        1   ?   1   ?   ?   0  : 1;
        ?   0   ?   0   ?   1  : 0;
        ?   1   ?   1   ?   1  : 1;

   endtable
endprimitive


primitive p_sr(Q,S,R,notifier);
output Q; reg Q;
input S,R, notifier;
// set-reset latch, set dominant
table
// S    R notifier : Qt : Qt+1
   0    0 ?        : ?  : - ;
   0    1 ?        : ?  : 0 ;
   1    0 ?        : ?  : 1 ;
   1    1 ?        : ?  : 1 ;
   1    x ?        : ?  : 1 ;
   x    0 ?        : 1  : 1 ;
   0    x ?        : 0  : 0 ;
   ?    ? *        : ?  : x ;
endtable
endprimitive

primitive p_rs(Q,S,R,notifier);
output Q; reg Q;
input S,R, notifier;
// set-reset latch, reset dominant
table
// S    R notifier : Qt : Qt+1
   0    0 ?        : ?  : - ;
   0    1 ?        : ?  : 0 ;
   1    0 ?        : ?  : 1 ;
   1    1 ?        : ?  : 0 ;
   1    x ?        : ?  : 1 ;
   x    0 ?        : 1  : 1 ;
   0    x ?        : 0  : 0 ;
   ?    ? *        : ?  : x ;
endtable
endprimitive

primitive p_latch(Q,D,G,notifier);
output Q; reg Q;
input D,G,notifier;
table
	// D  G No   : Qt  : Qt+1
	   ?  0 ?   : ?   : - ;  // clock disabled
	   0  1 ?   : ?   : 0 ;  // clock enabled
	   1  1 ?   : ?   : 1 ;  // transparent data
	   1  x ?   : 1   : 1 ;  // reducing pessimism
	   0  x ?   : 0   : 0 ;
	   ?  ? *   : ?   : x ;
endtable
endprimitive

primitive p_latchr(Q,D,G,R,notifier);
output Q; reg Q;
input D,G,R,notifier;
table
	// D  G R No   : Qt  : Qt+1
	   ?  0 0 ?   : ?   : - ;  // clock disabled
	   0  1 0 ?   : ?   : 0 ;  // clock enabled
	   1  1 0 ?   : ?   : 1 ;  // transparent data
	   1  x 0 ?   : 1   : 1 ;  // reducing pessimism
	   0  x 0 ?   : 0   : 0 ;
	   ?  ? 1 ?   : ?   : 0 ;  // clear
	   0  1 x ?   : ?   : 0 ;  // red pessimism
	   0  ? x ?   : 0   : 0 ;
	   ?  0 x ?   : 0   : 0 ;
	   ?  ? ? *   : ?   : x ;
endtable
endprimitive

primitive p_latchs(Q,D,G,S,notifier);
output Q; reg Q;
input D,G,S,notifier;
table
	// D  G S No   : Qt  : Qt+1
	   ?  0 0 ?   : ?   : - ;  // clock disabled
	   0  1 0 ?   : ?   : 0 ;  // clock enabled
	   1  1 0 ?   : ?   : 1 ;  // transparent data
	   1  x 0 ?   : 1   : 1 ;  // reducing pessimism
	   0  x 0 ?   : 0   : 0 ;
	   ?  n 0 ?   : ?   : - ;
	   ?  ? 1 ?   : ?   : 1 ;  // set
	   0  1 x ?   : ?   : 0 ;  // red pessimism
	   0  ? x ?   : 0   : 0 ;
	   ?  0 x ?   : 0   : 0 ;
	   ?  ? ? *   : ?   : x ;
endtable
endprimitive

primitive p_latchsr(Q,D,G,S,R,notifier);
output Q; reg Q;
input D,G,S,R,notifier; // set dominant
table
	// D  G S R No  : Qt  : Qt+1
	   ?  0 0 0 ?   : ?   : - ;  // clock disabled
	   0  1 0 0 ?   : ?   : 0 ;  // clock enabled
	   1  1 0 0 ?   : ?   : 1 ;  // transparent data
	   ?  ? 0 1 ?   : ?   : 0 ;  // clear
	   ?  ? 1 ? ?   : ?   : 1 ;  // set overrides
	   1  x 0 0 ?   : 1   : 1 ;  // reducing pessimism
	   0  x 0 0 ?   : 0   : 0 ;
	   0  1 0 x ?   : ?   : 0 ;  // red pessimism
	   0  ? 0 x ?   : 0   : 0 ;
	   1  1 x 0 ?   : ?   : 1 ;
	   1  ? x 0 ?   : 1   : 1 ;

	   ?  n 0 0 ?   : ?   : - ;
	   ?  ? ? ? *   : ?   : x ;
endtable
endprimitive

primitive p_latchrs(Q,D,G,S,R,notifier);
	output Q; reg Q;
	input D,G,S,R,notifier; // reset dominant
table
	// D  G S R No  : Qt  : Qt+1
	   ?  0 0 0 ?   : ?   : - ;  // clock disabled
	   0  1 0 0 ?   : ?   : 0 ;  // clock enabled
	   1  1 0 0 ?   : ?   : 1 ;  // transparent data
	   ?  ? 1 0 ?   : ?   : 1 ;  // set
	   ?  ? ? 1 ?   : ?   : 0 ;  // reset overrides
	   1  x 0 0 ?   : 1   : 1 ;  // reducing pessimism
	   0  x 0 0 ?   : 0   : 0 ;
	   0  1 0 x ?   : ?   : 0 ;
           ?  0 0 x ?	: 0   : 0 ;
	   0  ? 0 x ?   : 0   : 0 ;
	   1  1 x 0 ?   : ?   : 1 ;
           ?  0 x 0 ?	: 1   : 1 ;
	   1  ? x 0 ?   : 1   : 1 ;
	   ?  ? ? ? *   : ?   : x ;
endtable
endprimitive

// POSITIVE EDGE TRIGGERED D FLIP-FLOP
primitive p_ff  (Q, D, CP,notifier);
    output Q;  reg    Q;
    input  D, CP,notifier;

    table
    //  D   CP     No :   Qt  :   Qt+1
        1   (01)   ?  :   ?   :   1;  // clocked data
        0   (01)   ?  :   ?   :   0;
        1   (x1)   ?  :   1   :   1;  // reducing pessimism
        0   (x1)   ?  :   0   :   0;
        1   (bx)   ?  :   1   :   1;
        0   (bx)   ?  :   0   :   0;
        ?   (?0)   ?  :   ?   :   -;
        *    b     ?  :   ?   :   -;  // ignore edges on data
	?    ?     *  :   ?   :   x;
    endtable
endprimitive

primitive p_ffr  (Q, D, CP, R, notifier);
    output Q;  reg    Q;
    input  D, CP, R, notifier;

    table
    //   D  CP  R   No : Qt: Qt+1
      //-------------------------
         ?   ?  1   ?  : ? : 0 ;  // reset active
         0   r  0   ?  : ? : 0 ;  // normal clocking
         1   r  0   ?  : ? : 1 ;
         0 (BX) ?   ?  : 0 : - ; // clock going unknown with D=Qo
         1 (BX) 0   ?  : 1 : - ;
         ? (?0) ?   ?  : ? : - ; // ignore all clock->0 transitions
         1 (X1) 0   ?  : 1 : - ; // clock going unknown to active D=Qo=1
         0 (X1) ?   ?  : 0 : - ; // clock going unknown to active D=Qo=0
         0  r   x   ?  : ? : 0 ; // reset unknown, but clocking in 0
         ?  ?  (?X) ?  : 1 : x ; // reset going unknown
         1  X  (?X) ?  : 0 : x ;
         x  X  (?X) ?  : 0 : x ;
         0  X  (?X) ?  : 0 : - ;
         ?  b  (?X) ?  : 0 : - ;
         *  b   ?   ?  : ? : - ;  // ignore data changes if clock known
         ?  b  (?0) ?  : ? : - ; // ignore reset falling
         0  ?  (?0) ?  : ? : - ;
         ?  ?   ?   *  : ? : x ;  // timing violation
 
    endtable
endprimitive


primitive p_ffrs (Q, D, CP, R, S, notifier);
    output Q;  reg    Q;
    input  D, CP, R, S,notifier;
    table
    //   D  CP    R   S    No  :Qt :Qt+1

      //-------------------------------
         ?   ?    0    1    ?  : ? : 1 ; // set active
         ?   ?    1    ?    ?  : ? : 0 ;  // reset active (W/ OR W/O SET ACTIVE)
         1   r    0    0    ?  : ? : 1 ;  // normal clocking
         0   r    0    0    ?  : ? : 0 ;
         0  (BX)  ?    0    ?  : 0 : - ; // clock b->U,but D = Q
         1  (BX)  0    ?    ?  : 1 : - ;
         ?  (?0)  ?    ?    ?  : ? : - ; // ignore all clock->0 transitions
         1  (X1)  0    ?    ?  : 1 : - ; // clock going unknown to active D=Qo=1
         0  (X1)  ?    0    ?  : 0 : - ; // clock going unknown to active D=Qo=0
         0   r    x    0    ?  : ? : 0 ;  // reset unknown, but clocking in 0
         1   r    0    x    ?  : ? : 1 ;  // set unknown, but clocking in 1
         ?   ?   (?X)  1    ?  : 0 : x ; // reset going unknown
         ?   b   (?X)  0    ?  : 0 : - ;
         0   x   (?X)  0    ?  : 0 : - ;
         1   x   (?X)  b    ?  : 0 : x ;
         x   x   (?X)  b    ?  : 0 : x ;
         ?   ?    x    0    ?  : 1 : x ;
         ?   ?    ?   (?X)  ?  : 0 : x ; // set going unknown
         ?   b    b   (?X)  ?  : 1 : - ;
         0   x    b   (?X)  ?  : 1 : x ;
         1   x    b   (?X)  ?  : 1 : - ;
         x   x    b   (?X)  ?  : 1 : x ;
         ?   ?    0    x    ?  : 0 : x ;
         *   b    ?    ?    ?  : ? : - ;  // ignore data changes
         ?   b   (?0)  ?    ?  : ? : - ; // ignore reset falling
         0   ?   (?0)  ?    ?  : ? : - ;
         ?   b    ?   (?0)  ?  : ? : - ; // ignore set falling
         1   ?    ?   (?0)  ?  : ? : - ;
         ?   ?    ?    ?    *  : ? : x ;  // timing violation
    endtable
endprimitive


primitive p_ffsr (Q, D, CP, R, S, notifier);
    output Q;  reg    Q;
    input D, CP, R, S,notifier;
    table
    //  D   CP      R   S      No  :   Qt  :   Qt+1
        1   (01)    0   0      ?   :   ?   :   1;  // clocked data
        1   (01)    0   x      ?   :   ?   :   1;  // pessimism

        1    ?      0   x      ?   :   1   :   1;  // pessimism

        0    0      0   x      ?   :   1   :   1;  // pessimism
        0    x      0 (?x)     ?   :   1   :   1;  // pessimism
        0    1      0 (?x)     ?   :   1   :   1;  // pessimism

        x    0      0   x      ?   :   1   :   1;  // pessimism
        x    x      0 (?x)     ?   :   1   :   1;  // pessimism
        x    1      0 (?x)     ?   :   1   :   1;  // pessimism

        0   (01)    0   0      ?   :   ?   :   0;  // clocked data
        0   (01)    x   0      ?   :   ?   :   0;  // pessimism

        0    ?      x   0      ?   :   0   :   0;  // pessimism

        1    0      x   0      ?   :   0   :   0;  // pessimism
        1    x    (?x)  0      ?   :   0   :   0;  // pessimism
        1    1    (?x)  0      ?   :   0   :   0;  // pessimism

        x    0      x   0      ?   :   0   :   0;  // pessimism
        x    x    (?x)  0      ?   :   0   :   0;  // pessimism
        x    1    (?x)  0      ?   :   0   :   0;  // pessimism

        1   (x1)    0   0      ?   :   1   :   1;  // reducing pessimism
        0   (x1)    0   0      ?   :   0   :   0;
        1   (0x)    0   0      ?   :   1   :   1;
        0   (0x)    0   0      ?   :   0   :   0;

        ?   ?       1   0      ?   :   ?   :   0;  // asynchronous clear
        ?   ?       ?   1      ?   :   ?   :   1;  // asynchronous set

        ?   (?0)    ?   ?      ?   :   ?   :   -;  //ignore falling clock
        ?   (1x)    ?   ?      ?   :   ?   :   -;  //ignore falling clock
        *    ?      ?   ?      ?   :   ?   :   -;  // ignore data edges

        ?   ?     (?0)  0      ?   :   ?   :   -;  // ignore the edges on
        ?   ?       ?  (?0)    ?   :   ?   :   -;  // set and clear

        ?   ?       ?   ?      *   :   ?   :   x;
    endtable
endprimitive

primitive p_ffs (Q, D, CP, S, notifier);
    output Q;  reg    Q;
    input  D, CP, S, notifier;
    table
    //  D   CP      S     No   :   Qt  :   Qt+1

        1   (01)    0     ?    :   ?   :   1;  // clocked data
        0   (01)    0     ?    :   ?   :   0;
        1   (01)    x     ?    :   ?   :   1;  // reducing pessimism
        1    ?      x     ?    :   1   :   1;  // pessimism

        0    0      x     ?    :   1   :   1;  // pessimism
        0    x    (?x)    ?    :   1   :   1;  // pessimism
        0    1    (?x)    ?    :   1   :   1;  // pessimism

        x    0      x     ?    :   1   :   1;  // pessimism
        x    x    (?x)    ?    :   1   :   1;  // pessimism
        x    1    (?x)    ?    :   1   :   1;  // pessimism

        1   (x1)    0     ?    :   1   :   1;  // reducing pessimism
        0   (x1)    0     ?    :   0   :   0;
        1   (0x)    0     ?    :   1   :   1;
        0   (0x)    0     ?    :   0   :   0;

        ?    ?      1     ?    :   ?   :   1;  // asynchronous clear

        ?   (?0)    ?     ?    :   ?   :   -;  // ignore falling clock
        ?   (1x)    ?     ?    :   ?   :   -;  // ignore falling clock
        *    ?      ?     ?    :   ?   :   -;  // ignore the data edges

        ?   ?     (?0)    ?    :   ?   :   -;  // ignore the edges on set

        ?   ?       ?     *    :   ?   :   x;

    endtable
endprimitive

primitive ip_sr(Q,SB,RB,notifier);
output Q; reg Q;
input SB,RB, notifier;
// set-reset latch, set dominant
table
// SB    RB notifier : Qt : Qt+1
   1 1 ? : ? : -;
   1 0 ? : ? : 0;
   0 1 ? : ? : 1;
   0 0 ? : ? : 1;
   0 x ? : ? : 1;
   x 1 ? : 1 : 1;
   1 x ? : 0 : 0;
   ? ? * : ? : x;
endtable
endprimitive

primitive ip_rs(Q,SB,RB,notifier);
output Q; reg Q;
input SB,RB, notifier;
// set-reset latch, reset dominant
table
// SB    RB notifier : Qt : Qt+1
   1 1 ? : ? : -;
   1 0 ? : ? : 0;
   0 1 ? : ? : 1;
   0 0 ? : ? : 0;
   0 x ? : ? : 1;
   x 1 ? : 1 : 1;
   1 x ? : 0 : 0;
   ? ? * : ? : x;
endtable
endprimitive

primitive ip_latchr(Q,D,G,RB,notifier);
output Q; reg Q;
input D,G,RB,notifier;
table
	// D  G RB No   : Qt  : Qt+1
	   ? 0 1 ? : ? : -;  // clock disabled
	   0 1 1 ? : ? : 0;  // clock enabled
	   1 1 1 ? : ? : 1;  // transparent data
	   1 x 1 ? : 1 : 1;  // reducing pessimism
	   0 x 1 ? : 0 : 0;
	   ? n 1 ? : ? : -;
	   ? ? 0 ? : ? : 0;  // clear
	   0 1 x ? : ? : 0;  // red pessimism
	   0 ? x ? : 0 : 0;
	   ? 0 x ? : 0 : 0;
	   ? ? ? * : ? : x;
endtable
endprimitive

primitive ip_latchs(Q,D,G,SB,notifier);
output Q; reg Q;
input D,G,SB,notifier;
table
	// D  G SB No   : Qt  : Qt+1
	   ? 0 1 ? : ? : -;  // clock disabled
	   0 1 1 ? : ? : 0;  // clock enabled
	   1 1 1 ? : ? : 1;  // transparent data
	   1 x 1 ? : 1 : 1;  // reducing pessimism
	   0 x 1 ? : 0 : 0;
	   ? n 1 ? : ? : -;
	   ? ? 0 ? : ? : 1;  // set
	   0 1 x ? : ? : 0;  // red pessimism
	   0 ? x ? : 0 : 0;
	   ? 0 x ? : 0 : 0;
	   ? ? ? * : ? : x;
endtable
endprimitive

primitive ip_latchsr(Q,D,G,SB,RB,notifier);
output Q; reg Q;
input D,G,SB,RB,notifier; // set dominant
table
	// D  G SB RB No  : Qt  : Qt+1
	   ? 0 1 1 ? : ? : -;  // clock disabled
	   0 1 1 1 ? : ? : 0;  // clock enabled
	   1 1 1 1 ? : ? : 1;  // transparent data
	   ? ? 1 0 ? : ? : 0;  // clear
	   ? ? 0 ? ? : ? : 1;  // set overrides
	   1 x 1 1 ? : 1 : 1;  // reducing pessimism
	   0 x 1 1 ? : 0 : 0;
	   0 1 1 x ? : ? : 0;  // red pessimism
	   0 ? 1 x ? : 0 : 0;
	   1 1 x 1 ? : ? : 1;
	   1 ? x 1 ? : 1 : 1;

	   ? n 1 1 ? : ? : -;
	   ? ? ? ? * : ? : x;
endtable
endprimitive

primitive ip_latchrs(Q,D,G,SB,RB,notifier);
	output Q; reg Q;
	input D,G,SB,RB,notifier; // reset dominant
table
	// D  G SB RB No  : Qt  : Qt+1
	   ? 0 0 1 ? : ? : -;  // clock disabled
	   0 1 0 1 ? : ? : 0;  // clock enabled
	   1 1 0 1 ? : ? : 1;  // transparent data
	   ? ? 1 1 ? : ? : 1;  // set
	   ? ? ? 0 ? : ? : 0;  // reset overrides
	   1 x 0 1 ? : 1 : 1;  // reducing pessimism
	   0 x 0 1 ? : 0 : 0;
	   0 1 0 x ? : ? : 0;
	   0 ? 0 x ? : 0 : 0;
	   1 1 x 1 ? : ? : 1;
	   1 ? x 1 ? : 1 : 1;

	   ? n 0 1 ? : ? : -;
	   ? ? ? ? * : ? : x;
endtable
endprimitive

primitive ip_ffr  (Q, D, CP, RB, notifier);
    output Q;  reg    Q;
    input  D, CP, RB, notifier;

    table
    //  D   CP      RB No :   Qt  :   Qt+1

        1 (01) 0 ? : ? : 1;  // clocked data
        0 (01) 0 ? : ? : 0;

        0 (01) x ? : ? : 0;  // pessimism
        0 ? x ? : 0 : 0;  // pessimism

        1 0 x ? : 0 : 0;  // pessimism
        1 x (?x) ? : 0 : 0;  // pessimism
        1 1 (?x) ? : 0 : 0;  // pessimism

        x 0 x ? : 0 : 0;  // pessimism
        x x (?x) ? : 0 : 0;  // pessimism
        x 1 (?x) ? : 0 : 0;  // pessimism

        1 (x1) 0 ? : 1 : 1;  // reducing pessimism
        0 (x1) 0 ? : 0 : 0;
        1 (0x) 0 ? : 1 : 1;
        0 (0x) 0 ? : 0 : 0;

        ? ? 1 ? : ? : 0;  // asynchronous clear

        ? (?0) ? ? : ? : -;  // ignore falling clock
        ? (1x) ? ? : ? : -;  // ignore falling clock
        * ? ? ? : ? : -;  // ignore the edges on data

        ? ? (?0) ? : ? : -;  // ignore the edges on clear

		? ? ? * : ? : x;
    endtable
endprimitive


primitive ip_ffrs (Q, D, CP, RB, SB, notifier);
    output Q;  reg    Q;
    input  D, CP, RB, SB,notifier;
    table
    //  D   CP      RB   SB    No  :   Qt  :   Qt+1
        1 (01) 0 1 ? : ? : 1;  // clocked data
        1 (01) 0 x ? : ? : 1;  // pessimism

        1 ? 0 x ? : 1 : 1;  // pessimism

        0 0 0 x ? : 1 : 1;  // pessimism
        0 x 0 (?x) ? : 1 : 1;  // pessimism
        0 1 0 (?x) ? : 1 : 1;  // pessimism

        x 0 0 x ? : 1 : 1;  // pessimism
        x x 0 (?x) ? : 1 : 1;  // pessimism
        x 1 0 (?x) ? : 1 : 1;  // pessimism

        0 (01) 0 1 ? : ? : 0;  // clocked data
        0 (01) x 1 ? : ? : 0;  // pessimism

        0 ? x 1 ? : 0 : 0;  // pessimism

        1 0 x 1 ? : 0 : 0;  // pessimism
        1 x (?x) 1 ? : 0 : 0;  // pessimism
        1 1 (?x) 1 ? : 0 : 0;  // pessimism

        x 0 x 1 ? : 0 : 0;  // pessimism
        x x (?x) 1 ? : 0 : 0;  // pessimism
        x 1 (?x) 1 ? : 0 : 0;  // pessimism

        1 (x1) 0 1 ? : 1 : 1;  // reducing pessimism
        0 (x1) 0 1 ? : 0 : 0;
        1 (0x) 0 1 ? : 1 : 1;
        0 (0x) 0 1 ? : 0 : 0;

        ? ? 1 ? ? : ? : 0;  // asynchronous clear
        ? ? 0 0 ? : ? : 1;  // asynchronous set

        ? (?0) ? ? ? : ? : -;  // ignore falling clock
        ? (1x) ? ? ? : ? : -;  // ignore falling clock
        * ? ? ? ? : ? : -;  // ignore data edges

        ? ? (?0) 1 ? : ? : -;  // ignore the edges on
        ? ? ? (?1) ? : ? : -;  // set and clear

        ? ? ? ? * : ? : x;
    endtable
endprimitive


primitive ip_ffsr (Q, D, CP, RB, SB, notifier);
    output Q;  reg    Q;
    input D, CP, RB, SB,notifier;
    table
    //  D   CP      RB   SB      No  :   Qt  :   Qt+1
        1 (01) 0 1 ? : ? : 1;  // clocked data
        1 (01) 0 x ? : ? : 1;  // pessimism

        1 ? 0 x ? : 1 : 1;  // pessimism

        0 0 0 x ? : 1 : 1;  // pessimism
        0 x 0 (?x) ? : 1 : 1;  // pessimism
        0 1 0 (?x) ? : 1 : 1;  // pessimism

        x 0 0 x ? : 1 : 1;  // pessimism
        x x 0 (?x) ? : 1 : 1;  // pessimism
        x 1 0 (?x) ? : 1 : 1;  // pessimism

        0 (01) 0 1 ? : ? : 0;  // clocked data
        0 (01) x 1 ? : ? : 0;  // pessimism

        0 ? x 1 ? : 0 : 0;  // pessimism

        1 0 x 1 ? : 0 : 0;  // pessimism
        1 x (?x) 1 ? : 0 : 0;  // pessimism
        1 1 (?x) 1 ? : 0 : 0;  // pessimism

        x 0 x 1 ? : 0 : 0;  // pessimism
        x x (?x) 1 ? : 0 : 0;  // pessimism
        x 1 (?x) 1 ? : 0 : 0;  // pessimism

        1 (x1) 0 1 ? : 1 : 1;  // reducing pessimism
        0 (x1) 0 1 ? : 0 : 0;
        1 (0x) 0 1 ? : 1 : 1;
        0 (0x) 0 1 ? : 0 : 0;

        ? ? 1 1 ? : ? : 0;  // asynchronous clear
        ? ? ? 0 ? : ? : 1;  // asynchronous set

        ? (?0) ? ? ? : ? : -;  //ignore falling clock
        ? (1x) ? ? ? : ? : -;  //ignore falling clock
        * ? ? ? ? : ? : -;  // ignore data edges

        ? ? (?0) 1 ? : ? : -;  // ignore the edges on
        ? ? ? (?1) ? : ? : -;  // set and clear

        ? ? ? ? * : ? : x;
    endtable
endprimitive

primitive ip_ffs (Q, D, CP, SB, notifier);
    output Q;  reg    Q;
    input  D, CP, SB, notifier;
    table
    //  D   CP      SB     No   :   Qt  :   Qt+1

        1 (01) 0 ? : ? : 1;  // clocked data
        0 (01) 0 ? : ? : 0;
        1 (01) x ? : ? : 1;  // reducing pessimism
        1 ? x ? : 1 : 1;  // pessimism

        0 0 x ? : 1 : 1;  // pessimism
        0 x (?x) ? : 1 : 1;  // pessimism
        0 1 (?x) ? : 1 : 1;  // pessimism

        x 0 x ? : 1 : 1;  // pessimism
        x x (?x) ? : 1 : 1;  // pessimism
        x 1 (?x) ? : 1 : 1;  // pessimism

        1 (x1) 0 ? : 1 : 1;  // reducing pessimism
        0 (x1) 0 ? : 0 : 0;
        1 (0x) 0 ? : 1 : 1;
        0 (0x) 0 ? : 0 : 0;

        ? ? 1 ? : ? : 1;  // asynchronous clear

        ? (?0) ? ? : ? : -;  // ignore falling clock
        ? (1x) ? ? : ? : -;  // ignore falling clock
        * ? ? ? : ? : -;  // ignore the data edges

        ? ? (?0) ? : ? : -;  // ignore the edges on set

        ? ? ? * : ? : x;

    endtable
endprimitive
