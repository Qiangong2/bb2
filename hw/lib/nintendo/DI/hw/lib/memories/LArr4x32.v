// This file was automatically derived from "LArr4x32.vpp"
// using vpp on Thu May 18 23:07:35 2000.



// syn myclk = NullClk


module LArr4x32 (
	NullClk,
	WWL,
	WrDat,
	RWL,
	RdDat);

	input NullClk;
	input [3:0]WWL;
	input [31:0] WrDat;
	input [3:0]RWL;
	output [31:0] RdDat;


       wire [31:0] RdCol0Line;

  
            //
            // ReadLineBuffers
            //
	      
	    INVDA RWLR0Buf0 ( .A ( RWL[0] ), .Z ( RWLR0Wire0 ) );
	    INVDA RWLR0Buf1 ( .A ( RWL[0] ), .Z ( RWLR0Wire1 ) );
	    INVDA RWLR1Buf0 ( .A ( RWL[1] ), .Z ( RWLR1Wire0 ) );
	    INVDA RWLR1Buf1 ( .A ( RWL[1] ), .Z ( RWLR1Wire1 ) );
	    INVDA RWLR2Buf0 ( .A ( RWL[2] ), .Z ( RWLR2Wire0 ) );
	    INVDA RWLR2Buf1 ( .A ( RWL[2] ), .Z ( RWLR2Wire1 ) );
	    INVDA RWLR3Buf0 ( .A ( RWL[3] ), .Z ( RWLR3Wire0 ) );
	    INVDA RWLR3Buf1 ( .A ( RWL[3] ), .Z ( RWLR3Wire1 ) );
  
            //
            // WriteLineBuffers
            //
	      
	    INVDA WWLW0Buf0 ( .A ( WWL[0] ), .Z ( WWLW0Wire0 ) );
	    INVDA WWLW0Buf1 ( .A ( WWL[0] ), .Z ( WWLW0Wire1 ) );
	    INVDA WWLW1Buf0 ( .A ( WWL[1] ), .Z ( WWLW1Wire0 ) );
	    INVDA WWLW1Buf1 ( .A ( WWL[1] ), .Z ( WWLW1Wire1 ) );
	    INVDA WWLW2Buf0 ( .A ( WWL[2] ), .Z ( WWLW2Wire0 ) );
	    INVDA WWLW2Buf1 ( .A ( WWL[2] ), .Z ( WWLW2Wire1 ) );
	    INVDA WWLW3Buf0 ( .A ( WWL[3] ), .Z ( WWLW3Wire0 ) );
	    INVDA WWLW3Buf1 ( .A ( WWL[3] ), .Z ( WWLW3Wire1 ) );

//
//
// this is the array of latches itself.
     
            //
            // Row 0
	    //
	      
 	    LATNT1 LatR0C0B0( .G(WWLW0Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR0C0B1( .G(WWLW0Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR0C0B2( .G(WWLW0Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR0C0B3( .G(WWLW0Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR0C0B4( .G(WWLW0Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[4]) );
 	    LATNT1 LatR0C0B5( .G(WWLW0Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[5]) );
 	    LATNT1 LatR0C0B6( .G(WWLW0Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[6]) );
 	    LATNT1 LatR0C0B7( .G(WWLW0Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[7]) );
 	    LATNT1 LatR0C0B8( .G(WWLW0Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[8]) );
 	    LATNT1 LatR0C0B9( .G(WWLW0Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[9]) );
 	    LATNT1 LatR0C0B10( .G(WWLW0Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[10]) );
 	    LATNT1 LatR0C0B11( .G(WWLW0Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[11]) );
 	    LATNT1 LatR0C0B12( .G(WWLW0Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[12]) );
 	    LATNT1 LatR0C0B13( .G(WWLW0Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[13]) );
 	    LATNT1 LatR0C0B14( .G(WWLW0Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[14]) );
 	    LATNT1 LatR0C0B15( .G(WWLW0Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[15]) );
 	    LATNT1 LatR0C0B16( .G(WWLW0Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[16]) );
 	    LATNT1 LatR0C0B17( .G(WWLW0Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[17]) );
 	    LATNT1 LatR0C0B18( .G(WWLW0Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[18]) );
 	    LATNT1 LatR0C0B19( .G(WWLW0Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[19]) );
 	    LATNT1 LatR0C0B20( .G(WWLW0Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[20]) );
 	    LATNT1 LatR0C0B21( .G(WWLW0Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[21]) );
 	    LATNT1 LatR0C0B22( .G(WWLW0Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[22]) );
 	    LATNT1 LatR0C0B23( .G(WWLW0Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[23]) );
 	    LATNT1 LatR0C0B24( .G(WWLW0Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[24]) );
 	    LATNT1 LatR0C0B25( .G(WWLW0Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[25]) );
 	    LATNT1 LatR0C0B26( .G(WWLW0Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[26]) );
 	    LATNT1 LatR0C0B27( .G(WWLW0Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[27]) );
 	    LATNT1 LatR0C0B28( .G(WWLW0Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[28]) );
 	    LATNT1 LatR0C0B29( .G(WWLW0Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[29]) );
 	    LATNT1 LatR0C0B30( .G(WWLW0Wire1), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[30]) );
 	    LATNT1 LatR0C0B31( .G(WWLW0Wire1), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[31]) );
     
            //
            // Row 1
	    //
	      
 	    LATNT1 LatR1C0B0( .G(WWLW1Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR1C0B1( .G(WWLW1Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR1C0B2( .G(WWLW1Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR1C0B3( .G(WWLW1Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR1C0B4( .G(WWLW1Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[4]) );
 	    LATNT1 LatR1C0B5( .G(WWLW1Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[5]) );
 	    LATNT1 LatR1C0B6( .G(WWLW1Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[6]) );
 	    LATNT1 LatR1C0B7( .G(WWLW1Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[7]) );
 	    LATNT1 LatR1C0B8( .G(WWLW1Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[8]) );
 	    LATNT1 LatR1C0B9( .G(WWLW1Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[9]) );
 	    LATNT1 LatR1C0B10( .G(WWLW1Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[10]) );
 	    LATNT1 LatR1C0B11( .G(WWLW1Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[11]) );
 	    LATNT1 LatR1C0B12( .G(WWLW1Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[12]) );
 	    LATNT1 LatR1C0B13( .G(WWLW1Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[13]) );
 	    LATNT1 LatR1C0B14( .G(WWLW1Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[14]) );
 	    LATNT1 LatR1C0B15( .G(WWLW1Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[15]) );
 	    LATNT1 LatR1C0B16( .G(WWLW1Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[16]) );
 	    LATNT1 LatR1C0B17( .G(WWLW1Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[17]) );
 	    LATNT1 LatR1C0B18( .G(WWLW1Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[18]) );
 	    LATNT1 LatR1C0B19( .G(WWLW1Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[19]) );
 	    LATNT1 LatR1C0B20( .G(WWLW1Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[20]) );
 	    LATNT1 LatR1C0B21( .G(WWLW1Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[21]) );
 	    LATNT1 LatR1C0B22( .G(WWLW1Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[22]) );
 	    LATNT1 LatR1C0B23( .G(WWLW1Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[23]) );
 	    LATNT1 LatR1C0B24( .G(WWLW1Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[24]) );
 	    LATNT1 LatR1C0B25( .G(WWLW1Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[25]) );
 	    LATNT1 LatR1C0B26( .G(WWLW1Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[26]) );
 	    LATNT1 LatR1C0B27( .G(WWLW1Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[27]) );
 	    LATNT1 LatR1C0B28( .G(WWLW1Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[28]) );
 	    LATNT1 LatR1C0B29( .G(WWLW1Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[29]) );
 	    LATNT1 LatR1C0B30( .G(WWLW1Wire1), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[30]) );
 	    LATNT1 LatR1C0B31( .G(WWLW1Wire1), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[31]) );
     
            //
            // Row 2
	    //
	      
 	    LATNT1 LatR2C0B0( .G(WWLW2Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR2C0B1( .G(WWLW2Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR2C0B2( .G(WWLW2Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR2C0B3( .G(WWLW2Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR2C0B4( .G(WWLW2Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[4]) );
 	    LATNT1 LatR2C0B5( .G(WWLW2Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[5]) );
 	    LATNT1 LatR2C0B6( .G(WWLW2Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[6]) );
 	    LATNT1 LatR2C0B7( .G(WWLW2Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[7]) );
 	    LATNT1 LatR2C0B8( .G(WWLW2Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[8]) );
 	    LATNT1 LatR2C0B9( .G(WWLW2Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[9]) );
 	    LATNT1 LatR2C0B10( .G(WWLW2Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[10]) );
 	    LATNT1 LatR2C0B11( .G(WWLW2Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[11]) );
 	    LATNT1 LatR2C0B12( .G(WWLW2Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[12]) );
 	    LATNT1 LatR2C0B13( .G(WWLW2Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[13]) );
 	    LATNT1 LatR2C0B14( .G(WWLW2Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[14]) );
 	    LATNT1 LatR2C0B15( .G(WWLW2Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[15]) );
 	    LATNT1 LatR2C0B16( .G(WWLW2Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[16]) );
 	    LATNT1 LatR2C0B17( .G(WWLW2Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[17]) );
 	    LATNT1 LatR2C0B18( .G(WWLW2Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[18]) );
 	    LATNT1 LatR2C0B19( .G(WWLW2Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[19]) );
 	    LATNT1 LatR2C0B20( .G(WWLW2Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[20]) );
 	    LATNT1 LatR2C0B21( .G(WWLW2Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[21]) );
 	    LATNT1 LatR2C0B22( .G(WWLW2Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[22]) );
 	    LATNT1 LatR2C0B23( .G(WWLW2Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[23]) );
 	    LATNT1 LatR2C0B24( .G(WWLW2Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[24]) );
 	    LATNT1 LatR2C0B25( .G(WWLW2Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[25]) );
 	    LATNT1 LatR2C0B26( .G(WWLW2Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[26]) );
 	    LATNT1 LatR2C0B27( .G(WWLW2Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[27]) );
 	    LATNT1 LatR2C0B28( .G(WWLW2Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[28]) );
 	    LATNT1 LatR2C0B29( .G(WWLW2Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[29]) );
 	    LATNT1 LatR2C0B30( .G(WWLW2Wire1), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[30]) );
 	    LATNT1 LatR2C0B31( .G(WWLW2Wire1), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[31]) );
     
            //
            // Row 3
	    //
	      
 	    LATNT1 LatR3C0B0( .G(WWLW3Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR3C0B1( .G(WWLW3Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR3C0B2( .G(WWLW3Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR3C0B3( .G(WWLW3Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR3C0B4( .G(WWLW3Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[4]) );
 	    LATNT1 LatR3C0B5( .G(WWLW3Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[5]) );
 	    LATNT1 LatR3C0B6( .G(WWLW3Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[6]) );
 	    LATNT1 LatR3C0B7( .G(WWLW3Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[7]) );
 	    LATNT1 LatR3C0B8( .G(WWLW3Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[8]) );
 	    LATNT1 LatR3C0B9( .G(WWLW3Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[9]) );
 	    LATNT1 LatR3C0B10( .G(WWLW3Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[10]) );
 	    LATNT1 LatR3C0B11( .G(WWLW3Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[11]) );
 	    LATNT1 LatR3C0B12( .G(WWLW3Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[12]) );
 	    LATNT1 LatR3C0B13( .G(WWLW3Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[13]) );
 	    LATNT1 LatR3C0B14( .G(WWLW3Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[14]) );
 	    LATNT1 LatR3C0B15( .G(WWLW3Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[15]) );
 	    LATNT1 LatR3C0B16( .G(WWLW3Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[16]) );
 	    LATNT1 LatR3C0B17( .G(WWLW3Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[17]) );
 	    LATNT1 LatR3C0B18( .G(WWLW3Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[18]) );
 	    LATNT1 LatR3C0B19( .G(WWLW3Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[19]) );
 	    LATNT1 LatR3C0B20( .G(WWLW3Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[20]) );
 	    LATNT1 LatR3C0B21( .G(WWLW3Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[21]) );
 	    LATNT1 LatR3C0B22( .G(WWLW3Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[22]) );
 	    LATNT1 LatR3C0B23( .G(WWLW3Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[23]) );
 	    LATNT1 LatR3C0B24( .G(WWLW3Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[24]) );
 	    LATNT1 LatR3C0B25( .G(WWLW3Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[25]) );
 	    LATNT1 LatR3C0B26( .G(WWLW3Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[26]) );
 	    LATNT1 LatR3C0B27( .G(WWLW3Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[27]) );
 	    LATNT1 LatR3C0B28( .G(WWLW3Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[28]) );
 	    LATNT1 LatR3C0B29( .G(WWLW3Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[29]) );
 	    LATNT1 LatR3C0B30( .G(WWLW3Wire1), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[30]) );
 	    LATNT1 LatR3C0B31( .G(WWLW3Wire1), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[31]) );

//
//
// the psuedo sense amp (and column muxing if necessary)
         BUFD3 rd0buf(.A(RdCol0Line[0]), .Z(RdDat[0]) );
         BUFD3 rd1buf(.A(RdCol0Line[1]), .Z(RdDat[1]) );
         BUFD3 rd2buf(.A(RdCol0Line[2]), .Z(RdDat[2]) );
         BUFD3 rd3buf(.A(RdCol0Line[3]), .Z(RdDat[3]) );
         BUFD3 rd4buf(.A(RdCol0Line[4]), .Z(RdDat[4]) );
         BUFD3 rd5buf(.A(RdCol0Line[5]), .Z(RdDat[5]) );
         BUFD3 rd6buf(.A(RdCol0Line[6]), .Z(RdDat[6]) );
         BUFD3 rd7buf(.A(RdCol0Line[7]), .Z(RdDat[7]) );
         BUFD3 rd8buf(.A(RdCol0Line[8]), .Z(RdDat[8]) );
         BUFD3 rd9buf(.A(RdCol0Line[9]), .Z(RdDat[9]) );
         BUFD3 rd10buf(.A(RdCol0Line[10]), .Z(RdDat[10]) );
         BUFD3 rd11buf(.A(RdCol0Line[11]), .Z(RdDat[11]) );
         BUFD3 rd12buf(.A(RdCol0Line[12]), .Z(RdDat[12]) );
         BUFD3 rd13buf(.A(RdCol0Line[13]), .Z(RdDat[13]) );
         BUFD3 rd14buf(.A(RdCol0Line[14]), .Z(RdDat[14]) );
         BUFD3 rd15buf(.A(RdCol0Line[15]), .Z(RdDat[15]) );
         BUFD3 rd16buf(.A(RdCol0Line[16]), .Z(RdDat[16]) );
         BUFD3 rd17buf(.A(RdCol0Line[17]), .Z(RdDat[17]) );
         BUFD3 rd18buf(.A(RdCol0Line[18]), .Z(RdDat[18]) );
         BUFD3 rd19buf(.A(RdCol0Line[19]), .Z(RdDat[19]) );
         BUFD3 rd20buf(.A(RdCol0Line[20]), .Z(RdDat[20]) );
         BUFD3 rd21buf(.A(RdCol0Line[21]), .Z(RdDat[21]) );
         BUFD3 rd22buf(.A(RdCol0Line[22]), .Z(RdDat[22]) );
         BUFD3 rd23buf(.A(RdCol0Line[23]), .Z(RdDat[23]) );
         BUFD3 rd24buf(.A(RdCol0Line[24]), .Z(RdDat[24]) );
         BUFD3 rd25buf(.A(RdCol0Line[25]), .Z(RdDat[25]) );
         BUFD3 rd26buf(.A(RdCol0Line[26]), .Z(RdDat[26]) );
         BUFD3 rd27buf(.A(RdCol0Line[27]), .Z(RdDat[27]) );
         BUFD3 rd28buf(.A(RdCol0Line[28]), .Z(RdDat[28]) );
         BUFD3 rd29buf(.A(RdCol0Line[29]), .Z(RdDat[29]) );
         BUFD3 rd30buf(.A(RdCol0Line[30]), .Z(RdDat[30]) );
         BUFD3 rd31buf(.A(RdCol0Line[31]), .Z(RdDat[31]) );

   endmodule

