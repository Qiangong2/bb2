// This file was automatically derived from "LArr16x61.vpp"
// using vpp on Thu May 18 23:07:22 2000.



// syn myclk = NullClk


module LArr16x61 (
	NullClk,
	WWL,
	WrDat,
	RWL,
	ColSelect,
	RdDat);

	input NullClk;
	input [15:0]WWL;
	input [60:0] WrDat;
	input [7:0]RWL;
	   input ColSelect;
	output [60:0] RdDat;


       wire [60:0] RdCol0Line;
       wire [60:0] RdCol1Line;

  
            //
            // ReadLineBuffers
            //
	      
	    INVDA RWLR0Buf0 ( .A ( RWL[0] ), .Z ( RWLR0Wire0 ) );
	    INVDA RWLR0Buf1 ( .A ( RWL[0] ), .Z ( RWLR0Wire1 ) );
	    INVDA RWLR0Buf2 ( .A ( RWL[0] ), .Z ( RWLR0Wire2 ) );
	    INVDA RWLR0Buf3 ( .A ( RWL[0] ), .Z ( RWLR0Wire3 ) );
	    INVDA RWLR0Buf4 ( .A ( RWL[0] ), .Z ( RWLR0Wire4 ) );
	    INVDA RWLR0Buf5 ( .A ( RWL[0] ), .Z ( RWLR0Wire5 ) );
	    INVDA RWLR0Buf6 ( .A ( RWL[0] ), .Z ( RWLR0Wire6 ) );
	    INVDA RWLR0Buf7 ( .A ( RWL[0] ), .Z ( RWLR0Wire7 ) );
	    INVDA RWLR1Buf0 ( .A ( RWL[1] ), .Z ( RWLR1Wire0 ) );
	    INVDA RWLR1Buf1 ( .A ( RWL[1] ), .Z ( RWLR1Wire1 ) );
	    INVDA RWLR1Buf2 ( .A ( RWL[1] ), .Z ( RWLR1Wire2 ) );
	    INVDA RWLR1Buf3 ( .A ( RWL[1] ), .Z ( RWLR1Wire3 ) );
	    INVDA RWLR1Buf4 ( .A ( RWL[1] ), .Z ( RWLR1Wire4 ) );
	    INVDA RWLR1Buf5 ( .A ( RWL[1] ), .Z ( RWLR1Wire5 ) );
	    INVDA RWLR1Buf6 ( .A ( RWL[1] ), .Z ( RWLR1Wire6 ) );
	    INVDA RWLR1Buf7 ( .A ( RWL[1] ), .Z ( RWLR1Wire7 ) );
	    INVDA RWLR2Buf0 ( .A ( RWL[2] ), .Z ( RWLR2Wire0 ) );
	    INVDA RWLR2Buf1 ( .A ( RWL[2] ), .Z ( RWLR2Wire1 ) );
	    INVDA RWLR2Buf2 ( .A ( RWL[2] ), .Z ( RWLR2Wire2 ) );
	    INVDA RWLR2Buf3 ( .A ( RWL[2] ), .Z ( RWLR2Wire3 ) );
	    INVDA RWLR2Buf4 ( .A ( RWL[2] ), .Z ( RWLR2Wire4 ) );
	    INVDA RWLR2Buf5 ( .A ( RWL[2] ), .Z ( RWLR2Wire5 ) );
	    INVDA RWLR2Buf6 ( .A ( RWL[2] ), .Z ( RWLR2Wire6 ) );
	    INVDA RWLR2Buf7 ( .A ( RWL[2] ), .Z ( RWLR2Wire7 ) );
	    INVDA RWLR3Buf0 ( .A ( RWL[3] ), .Z ( RWLR3Wire0 ) );
	    INVDA RWLR3Buf1 ( .A ( RWL[3] ), .Z ( RWLR3Wire1 ) );
	    INVDA RWLR3Buf2 ( .A ( RWL[3] ), .Z ( RWLR3Wire2 ) );
	    INVDA RWLR3Buf3 ( .A ( RWL[3] ), .Z ( RWLR3Wire3 ) );
	    INVDA RWLR3Buf4 ( .A ( RWL[3] ), .Z ( RWLR3Wire4 ) );
	    INVDA RWLR3Buf5 ( .A ( RWL[3] ), .Z ( RWLR3Wire5 ) );
	    INVDA RWLR3Buf6 ( .A ( RWL[3] ), .Z ( RWLR3Wire6 ) );
	    INVDA RWLR3Buf7 ( .A ( RWL[3] ), .Z ( RWLR3Wire7 ) );
	    INVDA RWLR4Buf0 ( .A ( RWL[4] ), .Z ( RWLR4Wire0 ) );
	    INVDA RWLR4Buf1 ( .A ( RWL[4] ), .Z ( RWLR4Wire1 ) );
	    INVDA RWLR4Buf2 ( .A ( RWL[4] ), .Z ( RWLR4Wire2 ) );
	    INVDA RWLR4Buf3 ( .A ( RWL[4] ), .Z ( RWLR4Wire3 ) );
	    INVDA RWLR4Buf4 ( .A ( RWL[4] ), .Z ( RWLR4Wire4 ) );
	    INVDA RWLR4Buf5 ( .A ( RWL[4] ), .Z ( RWLR4Wire5 ) );
	    INVDA RWLR4Buf6 ( .A ( RWL[4] ), .Z ( RWLR4Wire6 ) );
	    INVDA RWLR4Buf7 ( .A ( RWL[4] ), .Z ( RWLR4Wire7 ) );
	    INVDA RWLR5Buf0 ( .A ( RWL[5] ), .Z ( RWLR5Wire0 ) );
	    INVDA RWLR5Buf1 ( .A ( RWL[5] ), .Z ( RWLR5Wire1 ) );
	    INVDA RWLR5Buf2 ( .A ( RWL[5] ), .Z ( RWLR5Wire2 ) );
	    INVDA RWLR5Buf3 ( .A ( RWL[5] ), .Z ( RWLR5Wire3 ) );
	    INVDA RWLR5Buf4 ( .A ( RWL[5] ), .Z ( RWLR5Wire4 ) );
	    INVDA RWLR5Buf5 ( .A ( RWL[5] ), .Z ( RWLR5Wire5 ) );
	    INVDA RWLR5Buf6 ( .A ( RWL[5] ), .Z ( RWLR5Wire6 ) );
	    INVDA RWLR5Buf7 ( .A ( RWL[5] ), .Z ( RWLR5Wire7 ) );
	    INVDA RWLR6Buf0 ( .A ( RWL[6] ), .Z ( RWLR6Wire0 ) );
	    INVDA RWLR6Buf1 ( .A ( RWL[6] ), .Z ( RWLR6Wire1 ) );
	    INVDA RWLR6Buf2 ( .A ( RWL[6] ), .Z ( RWLR6Wire2 ) );
	    INVDA RWLR6Buf3 ( .A ( RWL[6] ), .Z ( RWLR6Wire3 ) );
	    INVDA RWLR6Buf4 ( .A ( RWL[6] ), .Z ( RWLR6Wire4 ) );
	    INVDA RWLR6Buf5 ( .A ( RWL[6] ), .Z ( RWLR6Wire5 ) );
	    INVDA RWLR6Buf6 ( .A ( RWL[6] ), .Z ( RWLR6Wire6 ) );
	    INVDA RWLR6Buf7 ( .A ( RWL[6] ), .Z ( RWLR6Wire7 ) );
	    INVDA RWLR7Buf0 ( .A ( RWL[7] ), .Z ( RWLR7Wire0 ) );
	    INVDA RWLR7Buf1 ( .A ( RWL[7] ), .Z ( RWLR7Wire1 ) );
	    INVDA RWLR7Buf2 ( .A ( RWL[7] ), .Z ( RWLR7Wire2 ) );
	    INVDA RWLR7Buf3 ( .A ( RWL[7] ), .Z ( RWLR7Wire3 ) );
	    INVDA RWLR7Buf4 ( .A ( RWL[7] ), .Z ( RWLR7Wire4 ) );
	    INVDA RWLR7Buf5 ( .A ( RWL[7] ), .Z ( RWLR7Wire5 ) );
	    INVDA RWLR7Buf6 ( .A ( RWL[7] ), .Z ( RWLR7Wire6 ) );
	    INVDA RWLR7Buf7 ( .A ( RWL[7] ), .Z ( RWLR7Wire7 ) );
  
            //
            // WriteLineBuffers
            //
	      
	    INVDA WWLW0Buf0 ( .A ( WWL[0] ), .Z ( WWLW0Wire0 ) );
	    INVDA WWLW0Buf1 ( .A ( WWL[0] ), .Z ( WWLW0Wire1 ) );
	    INVDA WWLW0Buf2 ( .A ( WWL[0] ), .Z ( WWLW0Wire2 ) );
	    INVDA WWLW0Buf3 ( .A ( WWL[0] ), .Z ( WWLW0Wire3 ) );
	    INVDA WWLW1Buf0 ( .A ( WWL[1] ), .Z ( WWLW1Wire0 ) );
	    INVDA WWLW1Buf1 ( .A ( WWL[1] ), .Z ( WWLW1Wire1 ) );
	    INVDA WWLW1Buf2 ( .A ( WWL[1] ), .Z ( WWLW1Wire2 ) );
	    INVDA WWLW1Buf3 ( .A ( WWL[1] ), .Z ( WWLW1Wire3 ) );
	    INVDA WWLW2Buf0 ( .A ( WWL[2] ), .Z ( WWLW2Wire0 ) );
	    INVDA WWLW2Buf1 ( .A ( WWL[2] ), .Z ( WWLW2Wire1 ) );
	    INVDA WWLW2Buf2 ( .A ( WWL[2] ), .Z ( WWLW2Wire2 ) );
	    INVDA WWLW2Buf3 ( .A ( WWL[2] ), .Z ( WWLW2Wire3 ) );
	    INVDA WWLW3Buf0 ( .A ( WWL[3] ), .Z ( WWLW3Wire0 ) );
	    INVDA WWLW3Buf1 ( .A ( WWL[3] ), .Z ( WWLW3Wire1 ) );
	    INVDA WWLW3Buf2 ( .A ( WWL[3] ), .Z ( WWLW3Wire2 ) );
	    INVDA WWLW3Buf3 ( .A ( WWL[3] ), .Z ( WWLW3Wire3 ) );
	    INVDA WWLW4Buf0 ( .A ( WWL[4] ), .Z ( WWLW4Wire0 ) );
	    INVDA WWLW4Buf1 ( .A ( WWL[4] ), .Z ( WWLW4Wire1 ) );
	    INVDA WWLW4Buf2 ( .A ( WWL[4] ), .Z ( WWLW4Wire2 ) );
	    INVDA WWLW4Buf3 ( .A ( WWL[4] ), .Z ( WWLW4Wire3 ) );
	    INVDA WWLW5Buf0 ( .A ( WWL[5] ), .Z ( WWLW5Wire0 ) );
	    INVDA WWLW5Buf1 ( .A ( WWL[5] ), .Z ( WWLW5Wire1 ) );
	    INVDA WWLW5Buf2 ( .A ( WWL[5] ), .Z ( WWLW5Wire2 ) );
	    INVDA WWLW5Buf3 ( .A ( WWL[5] ), .Z ( WWLW5Wire3 ) );
	    INVDA WWLW6Buf0 ( .A ( WWL[6] ), .Z ( WWLW6Wire0 ) );
	    INVDA WWLW6Buf1 ( .A ( WWL[6] ), .Z ( WWLW6Wire1 ) );
	    INVDA WWLW6Buf2 ( .A ( WWL[6] ), .Z ( WWLW6Wire2 ) );
	    INVDA WWLW6Buf3 ( .A ( WWL[6] ), .Z ( WWLW6Wire3 ) );
	    INVDA WWLW7Buf0 ( .A ( WWL[7] ), .Z ( WWLW7Wire0 ) );
	    INVDA WWLW7Buf1 ( .A ( WWL[7] ), .Z ( WWLW7Wire1 ) );
	    INVDA WWLW7Buf2 ( .A ( WWL[7] ), .Z ( WWLW7Wire2 ) );
	    INVDA WWLW7Buf3 ( .A ( WWL[7] ), .Z ( WWLW7Wire3 ) );
	    INVDA WWLW8Buf0 ( .A ( WWL[8] ), .Z ( WWLW8Wire0 ) );
	    INVDA WWLW8Buf1 ( .A ( WWL[8] ), .Z ( WWLW8Wire1 ) );
	    INVDA WWLW8Buf2 ( .A ( WWL[8] ), .Z ( WWLW8Wire2 ) );
	    INVDA WWLW8Buf3 ( .A ( WWL[8] ), .Z ( WWLW8Wire3 ) );
	    INVDA WWLW9Buf0 ( .A ( WWL[9] ), .Z ( WWLW9Wire0 ) );
	    INVDA WWLW9Buf1 ( .A ( WWL[9] ), .Z ( WWLW9Wire1 ) );
	    INVDA WWLW9Buf2 ( .A ( WWL[9] ), .Z ( WWLW9Wire2 ) );
	    INVDA WWLW9Buf3 ( .A ( WWL[9] ), .Z ( WWLW9Wire3 ) );
	    INVDA WWLW10Buf0 ( .A ( WWL[10] ), .Z ( WWLW10Wire0 ) );
	    INVDA WWLW10Buf1 ( .A ( WWL[10] ), .Z ( WWLW10Wire1 ) );
	    INVDA WWLW10Buf2 ( .A ( WWL[10] ), .Z ( WWLW10Wire2 ) );
	    INVDA WWLW10Buf3 ( .A ( WWL[10] ), .Z ( WWLW10Wire3 ) );
	    INVDA WWLW11Buf0 ( .A ( WWL[11] ), .Z ( WWLW11Wire0 ) );
	    INVDA WWLW11Buf1 ( .A ( WWL[11] ), .Z ( WWLW11Wire1 ) );
	    INVDA WWLW11Buf2 ( .A ( WWL[11] ), .Z ( WWLW11Wire2 ) );
	    INVDA WWLW11Buf3 ( .A ( WWL[11] ), .Z ( WWLW11Wire3 ) );
	    INVDA WWLW12Buf0 ( .A ( WWL[12] ), .Z ( WWLW12Wire0 ) );
	    INVDA WWLW12Buf1 ( .A ( WWL[12] ), .Z ( WWLW12Wire1 ) );
	    INVDA WWLW12Buf2 ( .A ( WWL[12] ), .Z ( WWLW12Wire2 ) );
	    INVDA WWLW12Buf3 ( .A ( WWL[12] ), .Z ( WWLW12Wire3 ) );
	    INVDA WWLW13Buf0 ( .A ( WWL[13] ), .Z ( WWLW13Wire0 ) );
	    INVDA WWLW13Buf1 ( .A ( WWL[13] ), .Z ( WWLW13Wire1 ) );
	    INVDA WWLW13Buf2 ( .A ( WWL[13] ), .Z ( WWLW13Wire2 ) );
	    INVDA WWLW13Buf3 ( .A ( WWL[13] ), .Z ( WWLW13Wire3 ) );
	    INVDA WWLW14Buf0 ( .A ( WWL[14] ), .Z ( WWLW14Wire0 ) );
	    INVDA WWLW14Buf1 ( .A ( WWL[14] ), .Z ( WWLW14Wire1 ) );
	    INVDA WWLW14Buf2 ( .A ( WWL[14] ), .Z ( WWLW14Wire2 ) );
	    INVDA WWLW14Buf3 ( .A ( WWL[14] ), .Z ( WWLW14Wire3 ) );
	    INVDA WWLW15Buf0 ( .A ( WWL[15] ), .Z ( WWLW15Wire0 ) );
	    INVDA WWLW15Buf1 ( .A ( WWL[15] ), .Z ( WWLW15Wire1 ) );
	    INVDA WWLW15Buf2 ( .A ( WWL[15] ), .Z ( WWLW15Wire2 ) );
	    INVDA WWLW15Buf3 ( .A ( WWL[15] ), .Z ( WWLW15Wire3 ) );

//
//
// this is the array of latches itself.
     
            //
            // Row 0
	    //
	      
 	    LATNT1 LatR0C0B0( .G(WWLW0Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR0C1B0( .G(WWLW1Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR0Wire4), 
					  .Z(RdCol1Line[0]) );
 	    LATNT1 LatR0C0B1( .G(WWLW0Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR0C1B1( .G(WWLW1Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR0Wire4), 
					  .Z(RdCol1Line[1]) );
 	    LATNT1 LatR0C0B2( .G(WWLW0Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR0C1B2( .G(WWLW1Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR0Wire4), 
					  .Z(RdCol1Line[2]) );
 	    LATNT1 LatR0C0B3( .G(WWLW0Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR0C1B3( .G(WWLW1Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR0Wire4), 
					  .Z(RdCol1Line[3]) );
 	    LATNT1 LatR0C0B4( .G(WWLW0Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[4]) );
 	    LATNT1 LatR0C1B4( .G(WWLW1Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR0Wire4), 
					  .Z(RdCol1Line[4]) );
 	    LATNT1 LatR0C0B5( .G(WWLW0Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[5]) );
 	    LATNT1 LatR0C1B5( .G(WWLW1Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR0Wire4), 
					  .Z(RdCol1Line[5]) );
 	    LATNT1 LatR0C0B6( .G(WWLW0Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[6]) );
 	    LATNT1 LatR0C1B6( .G(WWLW1Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR0Wire4), 
					  .Z(RdCol1Line[6]) );
 	    LATNT1 LatR0C0B7( .G(WWLW0Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[7]) );
 	    LATNT1 LatR0C1B7( .G(WWLW1Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR0Wire4), 
					  .Z(RdCol1Line[7]) );
 	    LATNT1 LatR0C0B8( .G(WWLW0Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[8]) );
 	    LATNT1 LatR0C1B8( .G(WWLW1Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR0Wire4), 
					  .Z(RdCol1Line[8]) );
 	    LATNT1 LatR0C0B9( .G(WWLW0Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[9]) );
 	    LATNT1 LatR0C1B9( .G(WWLW1Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR0Wire4), 
					  .Z(RdCol1Line[9]) );
 	    LATNT1 LatR0C0B10( .G(WWLW0Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[10]) );
 	    LATNT1 LatR0C1B10( .G(WWLW1Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR0Wire4), 
					  .Z(RdCol1Line[10]) );
 	    LATNT1 LatR0C0B11( .G(WWLW0Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[11]) );
 	    LATNT1 LatR0C1B11( .G(WWLW1Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR0Wire4), 
					  .Z(RdCol1Line[11]) );
 	    LATNT1 LatR0C0B12( .G(WWLW0Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[12]) );
 	    LATNT1 LatR0C1B12( .G(WWLW1Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR0Wire4), 
					  .Z(RdCol1Line[12]) );
 	    LATNT1 LatR0C0B13( .G(WWLW0Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[13]) );
 	    LATNT1 LatR0C1B13( .G(WWLW1Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR0Wire4), 
					  .Z(RdCol1Line[13]) );
 	    LATNT1 LatR0C0B14( .G(WWLW0Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[14]) );
 	    LATNT1 LatR0C1B14( .G(WWLW1Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR0Wire4), 
					  .Z(RdCol1Line[14]) );
 	    LATNT1 LatR0C0B15( .G(WWLW0Wire1), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[15]) );
 	    LATNT1 LatR0C1B15( .G(WWLW1Wire1), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR0Wire5), 
					  .Z(RdCol1Line[15]) );
 	    LATNT1 LatR0C0B16( .G(WWLW0Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[16]) );
 	    LATNT1 LatR0C1B16( .G(WWLW1Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR0Wire5), 
					  .Z(RdCol1Line[16]) );
 	    LATNT1 LatR0C0B17( .G(WWLW0Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[17]) );
 	    LATNT1 LatR0C1B17( .G(WWLW1Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR0Wire5), 
					  .Z(RdCol1Line[17]) );
 	    LATNT1 LatR0C0B18( .G(WWLW0Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[18]) );
 	    LATNT1 LatR0C1B18( .G(WWLW1Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR0Wire5), 
					  .Z(RdCol1Line[18]) );
 	    LATNT1 LatR0C0B19( .G(WWLW0Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[19]) );
 	    LATNT1 LatR0C1B19( .G(WWLW1Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR0Wire5), 
					  .Z(RdCol1Line[19]) );
 	    LATNT1 LatR0C0B20( .G(WWLW0Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[20]) );
 	    LATNT1 LatR0C1B20( .G(WWLW1Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR0Wire5), 
					  .Z(RdCol1Line[20]) );
 	    LATNT1 LatR0C0B21( .G(WWLW0Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[21]) );
 	    LATNT1 LatR0C1B21( .G(WWLW1Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR0Wire5), 
					  .Z(RdCol1Line[21]) );
 	    LATNT1 LatR0C0B22( .G(WWLW0Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[22]) );
 	    LATNT1 LatR0C1B22( .G(WWLW1Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR0Wire5), 
					  .Z(RdCol1Line[22]) );
 	    LATNT1 LatR0C0B23( .G(WWLW0Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[23]) );
 	    LATNT1 LatR0C1B23( .G(WWLW1Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR0Wire5), 
					  .Z(RdCol1Line[23]) );
 	    LATNT1 LatR0C0B24( .G(WWLW0Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[24]) );
 	    LATNT1 LatR0C1B24( .G(WWLW1Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR0Wire5), 
					  .Z(RdCol1Line[24]) );
 	    LATNT1 LatR0C0B25( .G(WWLW0Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[25]) );
 	    LATNT1 LatR0C1B25( .G(WWLW1Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR0Wire5), 
					  .Z(RdCol1Line[25]) );
 	    LATNT1 LatR0C0B26( .G(WWLW0Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[26]) );
 	    LATNT1 LatR0C1B26( .G(WWLW1Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR0Wire5), 
					  .Z(RdCol1Line[26]) );
 	    LATNT1 LatR0C0B27( .G(WWLW0Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[27]) );
 	    LATNT1 LatR0C1B27( .G(WWLW1Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR0Wire5), 
					  .Z(RdCol1Line[27]) );
 	    LATNT1 LatR0C0B28( .G(WWLW0Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[28]) );
 	    LATNT1 LatR0C1B28( .G(WWLW1Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR0Wire5), 
					  .Z(RdCol1Line[28]) );
 	    LATNT1 LatR0C0B29( .G(WWLW0Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[29]) );
 	    LATNT1 LatR0C1B29( .G(WWLW1Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR0Wire5), 
					  .Z(RdCol1Line[29]) );
 	    LATNT1 LatR0C0B30( .G(WWLW0Wire2), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol0Line[30]) );
 	    LATNT1 LatR0C1B30( .G(WWLW1Wire2), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR0Wire6), 
					  .Z(RdCol1Line[30]) );
 	    LATNT1 LatR0C0B31( .G(WWLW0Wire2), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol0Line[31]) );
 	    LATNT1 LatR0C1B31( .G(WWLW1Wire2), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR0Wire6), 
					  .Z(RdCol1Line[31]) );
 	    LATNT1 LatR0C0B32( .G(WWLW0Wire2), 
					  .D(WrDat[32]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol0Line[32]) );
 	    LATNT1 LatR0C1B32( .G(WWLW1Wire2), 
					  .D(WrDat[32]), 
                           		  .EN(RWLR0Wire6), 
					  .Z(RdCol1Line[32]) );
 	    LATNT1 LatR0C0B33( .G(WWLW0Wire2), 
					  .D(WrDat[33]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol0Line[33]) );
 	    LATNT1 LatR0C1B33( .G(WWLW1Wire2), 
					  .D(WrDat[33]), 
                           		  .EN(RWLR0Wire6), 
					  .Z(RdCol1Line[33]) );
 	    LATNT1 LatR0C0B34( .G(WWLW0Wire2), 
					  .D(WrDat[34]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol0Line[34]) );
 	    LATNT1 LatR0C1B34( .G(WWLW1Wire2), 
					  .D(WrDat[34]), 
                           		  .EN(RWLR0Wire6), 
					  .Z(RdCol1Line[34]) );
 	    LATNT1 LatR0C0B35( .G(WWLW0Wire2), 
					  .D(WrDat[35]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol0Line[35]) );
 	    LATNT1 LatR0C1B35( .G(WWLW1Wire2), 
					  .D(WrDat[35]), 
                           		  .EN(RWLR0Wire6), 
					  .Z(RdCol1Line[35]) );
 	    LATNT1 LatR0C0B36( .G(WWLW0Wire2), 
					  .D(WrDat[36]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol0Line[36]) );
 	    LATNT1 LatR0C1B36( .G(WWLW1Wire2), 
					  .D(WrDat[36]), 
                           		  .EN(RWLR0Wire6), 
					  .Z(RdCol1Line[36]) );
 	    LATNT1 LatR0C0B37( .G(WWLW0Wire2), 
					  .D(WrDat[37]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol0Line[37]) );
 	    LATNT1 LatR0C1B37( .G(WWLW1Wire2), 
					  .D(WrDat[37]), 
                           		  .EN(RWLR0Wire6), 
					  .Z(RdCol1Line[37]) );
 	    LATNT1 LatR0C0B38( .G(WWLW0Wire2), 
					  .D(WrDat[38]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol0Line[38]) );
 	    LATNT1 LatR0C1B38( .G(WWLW1Wire2), 
					  .D(WrDat[38]), 
                           		  .EN(RWLR0Wire6), 
					  .Z(RdCol1Line[38]) );
 	    LATNT1 LatR0C0B39( .G(WWLW0Wire2), 
					  .D(WrDat[39]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol0Line[39]) );
 	    LATNT1 LatR0C1B39( .G(WWLW1Wire2), 
					  .D(WrDat[39]), 
                           		  .EN(RWLR0Wire6), 
					  .Z(RdCol1Line[39]) );
 	    LATNT1 LatR0C0B40( .G(WWLW0Wire2), 
					  .D(WrDat[40]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol0Line[40]) );
 	    LATNT1 LatR0C1B40( .G(WWLW1Wire2), 
					  .D(WrDat[40]), 
                           		  .EN(RWLR0Wire6), 
					  .Z(RdCol1Line[40]) );
 	    LATNT1 LatR0C0B41( .G(WWLW0Wire2), 
					  .D(WrDat[41]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol0Line[41]) );
 	    LATNT1 LatR0C1B41( .G(WWLW1Wire2), 
					  .D(WrDat[41]), 
                           		  .EN(RWLR0Wire6), 
					  .Z(RdCol1Line[41]) );
 	    LATNT1 LatR0C0B42( .G(WWLW0Wire2), 
					  .D(WrDat[42]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol0Line[42]) );
 	    LATNT1 LatR0C1B42( .G(WWLW1Wire2), 
					  .D(WrDat[42]), 
                           		  .EN(RWLR0Wire6), 
					  .Z(RdCol1Line[42]) );
 	    LATNT1 LatR0C0B43( .G(WWLW0Wire2), 
					  .D(WrDat[43]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol0Line[43]) );
 	    LATNT1 LatR0C1B43( .G(WWLW1Wire2), 
					  .D(WrDat[43]), 
                           		  .EN(RWLR0Wire6), 
					  .Z(RdCol1Line[43]) );
 	    LATNT1 LatR0C0B44( .G(WWLW0Wire2), 
					  .D(WrDat[44]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol0Line[44]) );
 	    LATNT1 LatR0C1B44( .G(WWLW1Wire2), 
					  .D(WrDat[44]), 
                           		  .EN(RWLR0Wire6), 
					  .Z(RdCol1Line[44]) );
 	    LATNT1 LatR0C0B45( .G(WWLW0Wire3), 
					  .D(WrDat[45]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol0Line[45]) );
 	    LATNT1 LatR0C1B45( .G(WWLW1Wire3), 
					  .D(WrDat[45]), 
                           		  .EN(RWLR0Wire7), 
					  .Z(RdCol1Line[45]) );
 	    LATNT1 LatR0C0B46( .G(WWLW0Wire3), 
					  .D(WrDat[46]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol0Line[46]) );
 	    LATNT1 LatR0C1B46( .G(WWLW1Wire3), 
					  .D(WrDat[46]), 
                           		  .EN(RWLR0Wire7), 
					  .Z(RdCol1Line[46]) );
 	    LATNT1 LatR0C0B47( .G(WWLW0Wire3), 
					  .D(WrDat[47]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol0Line[47]) );
 	    LATNT1 LatR0C1B47( .G(WWLW1Wire3), 
					  .D(WrDat[47]), 
                           		  .EN(RWLR0Wire7), 
					  .Z(RdCol1Line[47]) );
 	    LATNT1 LatR0C0B48( .G(WWLW0Wire3), 
					  .D(WrDat[48]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol0Line[48]) );
 	    LATNT1 LatR0C1B48( .G(WWLW1Wire3), 
					  .D(WrDat[48]), 
                           		  .EN(RWLR0Wire7), 
					  .Z(RdCol1Line[48]) );
 	    LATNT1 LatR0C0B49( .G(WWLW0Wire3), 
					  .D(WrDat[49]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol0Line[49]) );
 	    LATNT1 LatR0C1B49( .G(WWLW1Wire3), 
					  .D(WrDat[49]), 
                           		  .EN(RWLR0Wire7), 
					  .Z(RdCol1Line[49]) );
 	    LATNT1 LatR0C0B50( .G(WWLW0Wire3), 
					  .D(WrDat[50]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol0Line[50]) );
 	    LATNT1 LatR0C1B50( .G(WWLW1Wire3), 
					  .D(WrDat[50]), 
                           		  .EN(RWLR0Wire7), 
					  .Z(RdCol1Line[50]) );
 	    LATNT1 LatR0C0B51( .G(WWLW0Wire3), 
					  .D(WrDat[51]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol0Line[51]) );
 	    LATNT1 LatR0C1B51( .G(WWLW1Wire3), 
					  .D(WrDat[51]), 
                           		  .EN(RWLR0Wire7), 
					  .Z(RdCol1Line[51]) );
 	    LATNT1 LatR0C0B52( .G(WWLW0Wire3), 
					  .D(WrDat[52]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol0Line[52]) );
 	    LATNT1 LatR0C1B52( .G(WWLW1Wire3), 
					  .D(WrDat[52]), 
                           		  .EN(RWLR0Wire7), 
					  .Z(RdCol1Line[52]) );
 	    LATNT1 LatR0C0B53( .G(WWLW0Wire3), 
					  .D(WrDat[53]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol0Line[53]) );
 	    LATNT1 LatR0C1B53( .G(WWLW1Wire3), 
					  .D(WrDat[53]), 
                           		  .EN(RWLR0Wire7), 
					  .Z(RdCol1Line[53]) );
 	    LATNT1 LatR0C0B54( .G(WWLW0Wire3), 
					  .D(WrDat[54]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol0Line[54]) );
 	    LATNT1 LatR0C1B54( .G(WWLW1Wire3), 
					  .D(WrDat[54]), 
                           		  .EN(RWLR0Wire7), 
					  .Z(RdCol1Line[54]) );
 	    LATNT1 LatR0C0B55( .G(WWLW0Wire3), 
					  .D(WrDat[55]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol0Line[55]) );
 	    LATNT1 LatR0C1B55( .G(WWLW1Wire3), 
					  .D(WrDat[55]), 
                           		  .EN(RWLR0Wire7), 
					  .Z(RdCol1Line[55]) );
 	    LATNT1 LatR0C0B56( .G(WWLW0Wire3), 
					  .D(WrDat[56]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol0Line[56]) );
 	    LATNT1 LatR0C1B56( .G(WWLW1Wire3), 
					  .D(WrDat[56]), 
                           		  .EN(RWLR0Wire7), 
					  .Z(RdCol1Line[56]) );
 	    LATNT1 LatR0C0B57( .G(WWLW0Wire3), 
					  .D(WrDat[57]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol0Line[57]) );
 	    LATNT1 LatR0C1B57( .G(WWLW1Wire3), 
					  .D(WrDat[57]), 
                           		  .EN(RWLR0Wire7), 
					  .Z(RdCol1Line[57]) );
 	    LATNT1 LatR0C0B58( .G(WWLW0Wire3), 
					  .D(WrDat[58]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol0Line[58]) );
 	    LATNT1 LatR0C1B58( .G(WWLW1Wire3), 
					  .D(WrDat[58]), 
                           		  .EN(RWLR0Wire7), 
					  .Z(RdCol1Line[58]) );
 	    LATNT1 LatR0C0B59( .G(WWLW0Wire3), 
					  .D(WrDat[59]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol0Line[59]) );
 	    LATNT1 LatR0C1B59( .G(WWLW1Wire3), 
					  .D(WrDat[59]), 
                           		  .EN(RWLR0Wire7), 
					  .Z(RdCol1Line[59]) );
 	    LATNT1 LatR0C0B60( .G(WWLW0Wire3), 
					  .D(WrDat[60]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol0Line[60]) );
 	    LATNT1 LatR0C1B60( .G(WWLW1Wire3), 
					  .D(WrDat[60]), 
                           		  .EN(RWLR0Wire7), 
					  .Z(RdCol1Line[60]) );
     
            //
            // Row 1
	    //
	      
 	    LATNT1 LatR1C0B0( .G(WWLW2Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR1C1B0( .G(WWLW3Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR1Wire4), 
					  .Z(RdCol1Line[0]) );
 	    LATNT1 LatR1C0B1( .G(WWLW2Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR1C1B1( .G(WWLW3Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR1Wire4), 
					  .Z(RdCol1Line[1]) );
 	    LATNT1 LatR1C0B2( .G(WWLW2Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR1C1B2( .G(WWLW3Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR1Wire4), 
					  .Z(RdCol1Line[2]) );
 	    LATNT1 LatR1C0B3( .G(WWLW2Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR1C1B3( .G(WWLW3Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR1Wire4), 
					  .Z(RdCol1Line[3]) );
 	    LATNT1 LatR1C0B4( .G(WWLW2Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[4]) );
 	    LATNT1 LatR1C1B4( .G(WWLW3Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR1Wire4), 
					  .Z(RdCol1Line[4]) );
 	    LATNT1 LatR1C0B5( .G(WWLW2Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[5]) );
 	    LATNT1 LatR1C1B5( .G(WWLW3Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR1Wire4), 
					  .Z(RdCol1Line[5]) );
 	    LATNT1 LatR1C0B6( .G(WWLW2Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[6]) );
 	    LATNT1 LatR1C1B6( .G(WWLW3Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR1Wire4), 
					  .Z(RdCol1Line[6]) );
 	    LATNT1 LatR1C0B7( .G(WWLW2Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[7]) );
 	    LATNT1 LatR1C1B7( .G(WWLW3Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR1Wire4), 
					  .Z(RdCol1Line[7]) );
 	    LATNT1 LatR1C0B8( .G(WWLW2Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[8]) );
 	    LATNT1 LatR1C1B8( .G(WWLW3Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR1Wire4), 
					  .Z(RdCol1Line[8]) );
 	    LATNT1 LatR1C0B9( .G(WWLW2Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[9]) );
 	    LATNT1 LatR1C1B9( .G(WWLW3Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR1Wire4), 
					  .Z(RdCol1Line[9]) );
 	    LATNT1 LatR1C0B10( .G(WWLW2Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[10]) );
 	    LATNT1 LatR1C1B10( .G(WWLW3Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR1Wire4), 
					  .Z(RdCol1Line[10]) );
 	    LATNT1 LatR1C0B11( .G(WWLW2Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[11]) );
 	    LATNT1 LatR1C1B11( .G(WWLW3Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR1Wire4), 
					  .Z(RdCol1Line[11]) );
 	    LATNT1 LatR1C0B12( .G(WWLW2Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[12]) );
 	    LATNT1 LatR1C1B12( .G(WWLW3Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR1Wire4), 
					  .Z(RdCol1Line[12]) );
 	    LATNT1 LatR1C0B13( .G(WWLW2Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[13]) );
 	    LATNT1 LatR1C1B13( .G(WWLW3Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR1Wire4), 
					  .Z(RdCol1Line[13]) );
 	    LATNT1 LatR1C0B14( .G(WWLW2Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[14]) );
 	    LATNT1 LatR1C1B14( .G(WWLW3Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR1Wire4), 
					  .Z(RdCol1Line[14]) );
 	    LATNT1 LatR1C0B15( .G(WWLW2Wire1), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[15]) );
 	    LATNT1 LatR1C1B15( .G(WWLW3Wire1), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR1Wire5), 
					  .Z(RdCol1Line[15]) );
 	    LATNT1 LatR1C0B16( .G(WWLW2Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[16]) );
 	    LATNT1 LatR1C1B16( .G(WWLW3Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR1Wire5), 
					  .Z(RdCol1Line[16]) );
 	    LATNT1 LatR1C0B17( .G(WWLW2Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[17]) );
 	    LATNT1 LatR1C1B17( .G(WWLW3Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR1Wire5), 
					  .Z(RdCol1Line[17]) );
 	    LATNT1 LatR1C0B18( .G(WWLW2Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[18]) );
 	    LATNT1 LatR1C1B18( .G(WWLW3Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR1Wire5), 
					  .Z(RdCol1Line[18]) );
 	    LATNT1 LatR1C0B19( .G(WWLW2Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[19]) );
 	    LATNT1 LatR1C1B19( .G(WWLW3Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR1Wire5), 
					  .Z(RdCol1Line[19]) );
 	    LATNT1 LatR1C0B20( .G(WWLW2Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[20]) );
 	    LATNT1 LatR1C1B20( .G(WWLW3Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR1Wire5), 
					  .Z(RdCol1Line[20]) );
 	    LATNT1 LatR1C0B21( .G(WWLW2Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[21]) );
 	    LATNT1 LatR1C1B21( .G(WWLW3Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR1Wire5), 
					  .Z(RdCol1Line[21]) );
 	    LATNT1 LatR1C0B22( .G(WWLW2Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[22]) );
 	    LATNT1 LatR1C1B22( .G(WWLW3Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR1Wire5), 
					  .Z(RdCol1Line[22]) );
 	    LATNT1 LatR1C0B23( .G(WWLW2Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[23]) );
 	    LATNT1 LatR1C1B23( .G(WWLW3Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR1Wire5), 
					  .Z(RdCol1Line[23]) );
 	    LATNT1 LatR1C0B24( .G(WWLW2Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[24]) );
 	    LATNT1 LatR1C1B24( .G(WWLW3Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR1Wire5), 
					  .Z(RdCol1Line[24]) );
 	    LATNT1 LatR1C0B25( .G(WWLW2Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[25]) );
 	    LATNT1 LatR1C1B25( .G(WWLW3Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR1Wire5), 
					  .Z(RdCol1Line[25]) );
 	    LATNT1 LatR1C0B26( .G(WWLW2Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[26]) );
 	    LATNT1 LatR1C1B26( .G(WWLW3Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR1Wire5), 
					  .Z(RdCol1Line[26]) );
 	    LATNT1 LatR1C0B27( .G(WWLW2Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[27]) );
 	    LATNT1 LatR1C1B27( .G(WWLW3Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR1Wire5), 
					  .Z(RdCol1Line[27]) );
 	    LATNT1 LatR1C0B28( .G(WWLW2Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[28]) );
 	    LATNT1 LatR1C1B28( .G(WWLW3Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR1Wire5), 
					  .Z(RdCol1Line[28]) );
 	    LATNT1 LatR1C0B29( .G(WWLW2Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[29]) );
 	    LATNT1 LatR1C1B29( .G(WWLW3Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR1Wire5), 
					  .Z(RdCol1Line[29]) );
 	    LATNT1 LatR1C0B30( .G(WWLW2Wire2), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol0Line[30]) );
 	    LATNT1 LatR1C1B30( .G(WWLW3Wire2), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR1Wire6), 
					  .Z(RdCol1Line[30]) );
 	    LATNT1 LatR1C0B31( .G(WWLW2Wire2), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol0Line[31]) );
 	    LATNT1 LatR1C1B31( .G(WWLW3Wire2), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR1Wire6), 
					  .Z(RdCol1Line[31]) );
 	    LATNT1 LatR1C0B32( .G(WWLW2Wire2), 
					  .D(WrDat[32]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol0Line[32]) );
 	    LATNT1 LatR1C1B32( .G(WWLW3Wire2), 
					  .D(WrDat[32]), 
                           		  .EN(RWLR1Wire6), 
					  .Z(RdCol1Line[32]) );
 	    LATNT1 LatR1C0B33( .G(WWLW2Wire2), 
					  .D(WrDat[33]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol0Line[33]) );
 	    LATNT1 LatR1C1B33( .G(WWLW3Wire2), 
					  .D(WrDat[33]), 
                           		  .EN(RWLR1Wire6), 
					  .Z(RdCol1Line[33]) );
 	    LATNT1 LatR1C0B34( .G(WWLW2Wire2), 
					  .D(WrDat[34]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol0Line[34]) );
 	    LATNT1 LatR1C1B34( .G(WWLW3Wire2), 
					  .D(WrDat[34]), 
                           		  .EN(RWLR1Wire6), 
					  .Z(RdCol1Line[34]) );
 	    LATNT1 LatR1C0B35( .G(WWLW2Wire2), 
					  .D(WrDat[35]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol0Line[35]) );
 	    LATNT1 LatR1C1B35( .G(WWLW3Wire2), 
					  .D(WrDat[35]), 
                           		  .EN(RWLR1Wire6), 
					  .Z(RdCol1Line[35]) );
 	    LATNT1 LatR1C0B36( .G(WWLW2Wire2), 
					  .D(WrDat[36]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol0Line[36]) );
 	    LATNT1 LatR1C1B36( .G(WWLW3Wire2), 
					  .D(WrDat[36]), 
                           		  .EN(RWLR1Wire6), 
					  .Z(RdCol1Line[36]) );
 	    LATNT1 LatR1C0B37( .G(WWLW2Wire2), 
					  .D(WrDat[37]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol0Line[37]) );
 	    LATNT1 LatR1C1B37( .G(WWLW3Wire2), 
					  .D(WrDat[37]), 
                           		  .EN(RWLR1Wire6), 
					  .Z(RdCol1Line[37]) );
 	    LATNT1 LatR1C0B38( .G(WWLW2Wire2), 
					  .D(WrDat[38]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol0Line[38]) );
 	    LATNT1 LatR1C1B38( .G(WWLW3Wire2), 
					  .D(WrDat[38]), 
                           		  .EN(RWLR1Wire6), 
					  .Z(RdCol1Line[38]) );
 	    LATNT1 LatR1C0B39( .G(WWLW2Wire2), 
					  .D(WrDat[39]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol0Line[39]) );
 	    LATNT1 LatR1C1B39( .G(WWLW3Wire2), 
					  .D(WrDat[39]), 
                           		  .EN(RWLR1Wire6), 
					  .Z(RdCol1Line[39]) );
 	    LATNT1 LatR1C0B40( .G(WWLW2Wire2), 
					  .D(WrDat[40]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol0Line[40]) );
 	    LATNT1 LatR1C1B40( .G(WWLW3Wire2), 
					  .D(WrDat[40]), 
                           		  .EN(RWLR1Wire6), 
					  .Z(RdCol1Line[40]) );
 	    LATNT1 LatR1C0B41( .G(WWLW2Wire2), 
					  .D(WrDat[41]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol0Line[41]) );
 	    LATNT1 LatR1C1B41( .G(WWLW3Wire2), 
					  .D(WrDat[41]), 
                           		  .EN(RWLR1Wire6), 
					  .Z(RdCol1Line[41]) );
 	    LATNT1 LatR1C0B42( .G(WWLW2Wire2), 
					  .D(WrDat[42]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol0Line[42]) );
 	    LATNT1 LatR1C1B42( .G(WWLW3Wire2), 
					  .D(WrDat[42]), 
                           		  .EN(RWLR1Wire6), 
					  .Z(RdCol1Line[42]) );
 	    LATNT1 LatR1C0B43( .G(WWLW2Wire2), 
					  .D(WrDat[43]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol0Line[43]) );
 	    LATNT1 LatR1C1B43( .G(WWLW3Wire2), 
					  .D(WrDat[43]), 
                           		  .EN(RWLR1Wire6), 
					  .Z(RdCol1Line[43]) );
 	    LATNT1 LatR1C0B44( .G(WWLW2Wire2), 
					  .D(WrDat[44]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol0Line[44]) );
 	    LATNT1 LatR1C1B44( .G(WWLW3Wire2), 
					  .D(WrDat[44]), 
                           		  .EN(RWLR1Wire6), 
					  .Z(RdCol1Line[44]) );
 	    LATNT1 LatR1C0B45( .G(WWLW2Wire3), 
					  .D(WrDat[45]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol0Line[45]) );
 	    LATNT1 LatR1C1B45( .G(WWLW3Wire3), 
					  .D(WrDat[45]), 
                           		  .EN(RWLR1Wire7), 
					  .Z(RdCol1Line[45]) );
 	    LATNT1 LatR1C0B46( .G(WWLW2Wire3), 
					  .D(WrDat[46]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol0Line[46]) );
 	    LATNT1 LatR1C1B46( .G(WWLW3Wire3), 
					  .D(WrDat[46]), 
                           		  .EN(RWLR1Wire7), 
					  .Z(RdCol1Line[46]) );
 	    LATNT1 LatR1C0B47( .G(WWLW2Wire3), 
					  .D(WrDat[47]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol0Line[47]) );
 	    LATNT1 LatR1C1B47( .G(WWLW3Wire3), 
					  .D(WrDat[47]), 
                           		  .EN(RWLR1Wire7), 
					  .Z(RdCol1Line[47]) );
 	    LATNT1 LatR1C0B48( .G(WWLW2Wire3), 
					  .D(WrDat[48]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol0Line[48]) );
 	    LATNT1 LatR1C1B48( .G(WWLW3Wire3), 
					  .D(WrDat[48]), 
                           		  .EN(RWLR1Wire7), 
					  .Z(RdCol1Line[48]) );
 	    LATNT1 LatR1C0B49( .G(WWLW2Wire3), 
					  .D(WrDat[49]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol0Line[49]) );
 	    LATNT1 LatR1C1B49( .G(WWLW3Wire3), 
					  .D(WrDat[49]), 
                           		  .EN(RWLR1Wire7), 
					  .Z(RdCol1Line[49]) );
 	    LATNT1 LatR1C0B50( .G(WWLW2Wire3), 
					  .D(WrDat[50]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol0Line[50]) );
 	    LATNT1 LatR1C1B50( .G(WWLW3Wire3), 
					  .D(WrDat[50]), 
                           		  .EN(RWLR1Wire7), 
					  .Z(RdCol1Line[50]) );
 	    LATNT1 LatR1C0B51( .G(WWLW2Wire3), 
					  .D(WrDat[51]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol0Line[51]) );
 	    LATNT1 LatR1C1B51( .G(WWLW3Wire3), 
					  .D(WrDat[51]), 
                           		  .EN(RWLR1Wire7), 
					  .Z(RdCol1Line[51]) );
 	    LATNT1 LatR1C0B52( .G(WWLW2Wire3), 
					  .D(WrDat[52]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol0Line[52]) );
 	    LATNT1 LatR1C1B52( .G(WWLW3Wire3), 
					  .D(WrDat[52]), 
                           		  .EN(RWLR1Wire7), 
					  .Z(RdCol1Line[52]) );
 	    LATNT1 LatR1C0B53( .G(WWLW2Wire3), 
					  .D(WrDat[53]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol0Line[53]) );
 	    LATNT1 LatR1C1B53( .G(WWLW3Wire3), 
					  .D(WrDat[53]), 
                           		  .EN(RWLR1Wire7), 
					  .Z(RdCol1Line[53]) );
 	    LATNT1 LatR1C0B54( .G(WWLW2Wire3), 
					  .D(WrDat[54]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol0Line[54]) );
 	    LATNT1 LatR1C1B54( .G(WWLW3Wire3), 
					  .D(WrDat[54]), 
                           		  .EN(RWLR1Wire7), 
					  .Z(RdCol1Line[54]) );
 	    LATNT1 LatR1C0B55( .G(WWLW2Wire3), 
					  .D(WrDat[55]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol0Line[55]) );
 	    LATNT1 LatR1C1B55( .G(WWLW3Wire3), 
					  .D(WrDat[55]), 
                           		  .EN(RWLR1Wire7), 
					  .Z(RdCol1Line[55]) );
 	    LATNT1 LatR1C0B56( .G(WWLW2Wire3), 
					  .D(WrDat[56]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol0Line[56]) );
 	    LATNT1 LatR1C1B56( .G(WWLW3Wire3), 
					  .D(WrDat[56]), 
                           		  .EN(RWLR1Wire7), 
					  .Z(RdCol1Line[56]) );
 	    LATNT1 LatR1C0B57( .G(WWLW2Wire3), 
					  .D(WrDat[57]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol0Line[57]) );
 	    LATNT1 LatR1C1B57( .G(WWLW3Wire3), 
					  .D(WrDat[57]), 
                           		  .EN(RWLR1Wire7), 
					  .Z(RdCol1Line[57]) );
 	    LATNT1 LatR1C0B58( .G(WWLW2Wire3), 
					  .D(WrDat[58]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol0Line[58]) );
 	    LATNT1 LatR1C1B58( .G(WWLW3Wire3), 
					  .D(WrDat[58]), 
                           		  .EN(RWLR1Wire7), 
					  .Z(RdCol1Line[58]) );
 	    LATNT1 LatR1C0B59( .G(WWLW2Wire3), 
					  .D(WrDat[59]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol0Line[59]) );
 	    LATNT1 LatR1C1B59( .G(WWLW3Wire3), 
					  .D(WrDat[59]), 
                           		  .EN(RWLR1Wire7), 
					  .Z(RdCol1Line[59]) );
 	    LATNT1 LatR1C0B60( .G(WWLW2Wire3), 
					  .D(WrDat[60]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol0Line[60]) );
 	    LATNT1 LatR1C1B60( .G(WWLW3Wire3), 
					  .D(WrDat[60]), 
                           		  .EN(RWLR1Wire7), 
					  .Z(RdCol1Line[60]) );
     
            //
            // Row 2
	    //
	      
 	    LATNT1 LatR2C0B0( .G(WWLW4Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR2C1B0( .G(WWLW5Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR2Wire4), 
					  .Z(RdCol1Line[0]) );
 	    LATNT1 LatR2C0B1( .G(WWLW4Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR2C1B1( .G(WWLW5Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR2Wire4), 
					  .Z(RdCol1Line[1]) );
 	    LATNT1 LatR2C0B2( .G(WWLW4Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR2C1B2( .G(WWLW5Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR2Wire4), 
					  .Z(RdCol1Line[2]) );
 	    LATNT1 LatR2C0B3( .G(WWLW4Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR2C1B3( .G(WWLW5Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR2Wire4), 
					  .Z(RdCol1Line[3]) );
 	    LATNT1 LatR2C0B4( .G(WWLW4Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[4]) );
 	    LATNT1 LatR2C1B4( .G(WWLW5Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR2Wire4), 
					  .Z(RdCol1Line[4]) );
 	    LATNT1 LatR2C0B5( .G(WWLW4Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[5]) );
 	    LATNT1 LatR2C1B5( .G(WWLW5Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR2Wire4), 
					  .Z(RdCol1Line[5]) );
 	    LATNT1 LatR2C0B6( .G(WWLW4Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[6]) );
 	    LATNT1 LatR2C1B6( .G(WWLW5Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR2Wire4), 
					  .Z(RdCol1Line[6]) );
 	    LATNT1 LatR2C0B7( .G(WWLW4Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[7]) );
 	    LATNT1 LatR2C1B7( .G(WWLW5Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR2Wire4), 
					  .Z(RdCol1Line[7]) );
 	    LATNT1 LatR2C0B8( .G(WWLW4Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[8]) );
 	    LATNT1 LatR2C1B8( .G(WWLW5Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR2Wire4), 
					  .Z(RdCol1Line[8]) );
 	    LATNT1 LatR2C0B9( .G(WWLW4Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[9]) );
 	    LATNT1 LatR2C1B9( .G(WWLW5Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR2Wire4), 
					  .Z(RdCol1Line[9]) );
 	    LATNT1 LatR2C0B10( .G(WWLW4Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[10]) );
 	    LATNT1 LatR2C1B10( .G(WWLW5Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR2Wire4), 
					  .Z(RdCol1Line[10]) );
 	    LATNT1 LatR2C0B11( .G(WWLW4Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[11]) );
 	    LATNT1 LatR2C1B11( .G(WWLW5Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR2Wire4), 
					  .Z(RdCol1Line[11]) );
 	    LATNT1 LatR2C0B12( .G(WWLW4Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[12]) );
 	    LATNT1 LatR2C1B12( .G(WWLW5Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR2Wire4), 
					  .Z(RdCol1Line[12]) );
 	    LATNT1 LatR2C0B13( .G(WWLW4Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[13]) );
 	    LATNT1 LatR2C1B13( .G(WWLW5Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR2Wire4), 
					  .Z(RdCol1Line[13]) );
 	    LATNT1 LatR2C0B14( .G(WWLW4Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[14]) );
 	    LATNT1 LatR2C1B14( .G(WWLW5Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR2Wire4), 
					  .Z(RdCol1Line[14]) );
 	    LATNT1 LatR2C0B15( .G(WWLW4Wire1), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[15]) );
 	    LATNT1 LatR2C1B15( .G(WWLW5Wire1), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR2Wire5), 
					  .Z(RdCol1Line[15]) );
 	    LATNT1 LatR2C0B16( .G(WWLW4Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[16]) );
 	    LATNT1 LatR2C1B16( .G(WWLW5Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR2Wire5), 
					  .Z(RdCol1Line[16]) );
 	    LATNT1 LatR2C0B17( .G(WWLW4Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[17]) );
 	    LATNT1 LatR2C1B17( .G(WWLW5Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR2Wire5), 
					  .Z(RdCol1Line[17]) );
 	    LATNT1 LatR2C0B18( .G(WWLW4Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[18]) );
 	    LATNT1 LatR2C1B18( .G(WWLW5Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR2Wire5), 
					  .Z(RdCol1Line[18]) );
 	    LATNT1 LatR2C0B19( .G(WWLW4Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[19]) );
 	    LATNT1 LatR2C1B19( .G(WWLW5Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR2Wire5), 
					  .Z(RdCol1Line[19]) );
 	    LATNT1 LatR2C0B20( .G(WWLW4Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[20]) );
 	    LATNT1 LatR2C1B20( .G(WWLW5Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR2Wire5), 
					  .Z(RdCol1Line[20]) );
 	    LATNT1 LatR2C0B21( .G(WWLW4Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[21]) );
 	    LATNT1 LatR2C1B21( .G(WWLW5Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR2Wire5), 
					  .Z(RdCol1Line[21]) );
 	    LATNT1 LatR2C0B22( .G(WWLW4Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[22]) );
 	    LATNT1 LatR2C1B22( .G(WWLW5Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR2Wire5), 
					  .Z(RdCol1Line[22]) );
 	    LATNT1 LatR2C0B23( .G(WWLW4Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[23]) );
 	    LATNT1 LatR2C1B23( .G(WWLW5Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR2Wire5), 
					  .Z(RdCol1Line[23]) );
 	    LATNT1 LatR2C0B24( .G(WWLW4Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[24]) );
 	    LATNT1 LatR2C1B24( .G(WWLW5Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR2Wire5), 
					  .Z(RdCol1Line[24]) );
 	    LATNT1 LatR2C0B25( .G(WWLW4Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[25]) );
 	    LATNT1 LatR2C1B25( .G(WWLW5Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR2Wire5), 
					  .Z(RdCol1Line[25]) );
 	    LATNT1 LatR2C0B26( .G(WWLW4Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[26]) );
 	    LATNT1 LatR2C1B26( .G(WWLW5Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR2Wire5), 
					  .Z(RdCol1Line[26]) );
 	    LATNT1 LatR2C0B27( .G(WWLW4Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[27]) );
 	    LATNT1 LatR2C1B27( .G(WWLW5Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR2Wire5), 
					  .Z(RdCol1Line[27]) );
 	    LATNT1 LatR2C0B28( .G(WWLW4Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[28]) );
 	    LATNT1 LatR2C1B28( .G(WWLW5Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR2Wire5), 
					  .Z(RdCol1Line[28]) );
 	    LATNT1 LatR2C0B29( .G(WWLW4Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[29]) );
 	    LATNT1 LatR2C1B29( .G(WWLW5Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR2Wire5), 
					  .Z(RdCol1Line[29]) );
 	    LATNT1 LatR2C0B30( .G(WWLW4Wire2), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol0Line[30]) );
 	    LATNT1 LatR2C1B30( .G(WWLW5Wire2), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR2Wire6), 
					  .Z(RdCol1Line[30]) );
 	    LATNT1 LatR2C0B31( .G(WWLW4Wire2), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol0Line[31]) );
 	    LATNT1 LatR2C1B31( .G(WWLW5Wire2), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR2Wire6), 
					  .Z(RdCol1Line[31]) );
 	    LATNT1 LatR2C0B32( .G(WWLW4Wire2), 
					  .D(WrDat[32]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol0Line[32]) );
 	    LATNT1 LatR2C1B32( .G(WWLW5Wire2), 
					  .D(WrDat[32]), 
                           		  .EN(RWLR2Wire6), 
					  .Z(RdCol1Line[32]) );
 	    LATNT1 LatR2C0B33( .G(WWLW4Wire2), 
					  .D(WrDat[33]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol0Line[33]) );
 	    LATNT1 LatR2C1B33( .G(WWLW5Wire2), 
					  .D(WrDat[33]), 
                           		  .EN(RWLR2Wire6), 
					  .Z(RdCol1Line[33]) );
 	    LATNT1 LatR2C0B34( .G(WWLW4Wire2), 
					  .D(WrDat[34]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol0Line[34]) );
 	    LATNT1 LatR2C1B34( .G(WWLW5Wire2), 
					  .D(WrDat[34]), 
                           		  .EN(RWLR2Wire6), 
					  .Z(RdCol1Line[34]) );
 	    LATNT1 LatR2C0B35( .G(WWLW4Wire2), 
					  .D(WrDat[35]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol0Line[35]) );
 	    LATNT1 LatR2C1B35( .G(WWLW5Wire2), 
					  .D(WrDat[35]), 
                           		  .EN(RWLR2Wire6), 
					  .Z(RdCol1Line[35]) );
 	    LATNT1 LatR2C0B36( .G(WWLW4Wire2), 
					  .D(WrDat[36]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol0Line[36]) );
 	    LATNT1 LatR2C1B36( .G(WWLW5Wire2), 
					  .D(WrDat[36]), 
                           		  .EN(RWLR2Wire6), 
					  .Z(RdCol1Line[36]) );
 	    LATNT1 LatR2C0B37( .G(WWLW4Wire2), 
					  .D(WrDat[37]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol0Line[37]) );
 	    LATNT1 LatR2C1B37( .G(WWLW5Wire2), 
					  .D(WrDat[37]), 
                           		  .EN(RWLR2Wire6), 
					  .Z(RdCol1Line[37]) );
 	    LATNT1 LatR2C0B38( .G(WWLW4Wire2), 
					  .D(WrDat[38]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol0Line[38]) );
 	    LATNT1 LatR2C1B38( .G(WWLW5Wire2), 
					  .D(WrDat[38]), 
                           		  .EN(RWLR2Wire6), 
					  .Z(RdCol1Line[38]) );
 	    LATNT1 LatR2C0B39( .G(WWLW4Wire2), 
					  .D(WrDat[39]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol0Line[39]) );
 	    LATNT1 LatR2C1B39( .G(WWLW5Wire2), 
					  .D(WrDat[39]), 
                           		  .EN(RWLR2Wire6), 
					  .Z(RdCol1Line[39]) );
 	    LATNT1 LatR2C0B40( .G(WWLW4Wire2), 
					  .D(WrDat[40]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol0Line[40]) );
 	    LATNT1 LatR2C1B40( .G(WWLW5Wire2), 
					  .D(WrDat[40]), 
                           		  .EN(RWLR2Wire6), 
					  .Z(RdCol1Line[40]) );
 	    LATNT1 LatR2C0B41( .G(WWLW4Wire2), 
					  .D(WrDat[41]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol0Line[41]) );
 	    LATNT1 LatR2C1B41( .G(WWLW5Wire2), 
					  .D(WrDat[41]), 
                           		  .EN(RWLR2Wire6), 
					  .Z(RdCol1Line[41]) );
 	    LATNT1 LatR2C0B42( .G(WWLW4Wire2), 
					  .D(WrDat[42]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol0Line[42]) );
 	    LATNT1 LatR2C1B42( .G(WWLW5Wire2), 
					  .D(WrDat[42]), 
                           		  .EN(RWLR2Wire6), 
					  .Z(RdCol1Line[42]) );
 	    LATNT1 LatR2C0B43( .G(WWLW4Wire2), 
					  .D(WrDat[43]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol0Line[43]) );
 	    LATNT1 LatR2C1B43( .G(WWLW5Wire2), 
					  .D(WrDat[43]), 
                           		  .EN(RWLR2Wire6), 
					  .Z(RdCol1Line[43]) );
 	    LATNT1 LatR2C0B44( .G(WWLW4Wire2), 
					  .D(WrDat[44]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol0Line[44]) );
 	    LATNT1 LatR2C1B44( .G(WWLW5Wire2), 
					  .D(WrDat[44]), 
                           		  .EN(RWLR2Wire6), 
					  .Z(RdCol1Line[44]) );
 	    LATNT1 LatR2C0B45( .G(WWLW4Wire3), 
					  .D(WrDat[45]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol0Line[45]) );
 	    LATNT1 LatR2C1B45( .G(WWLW5Wire3), 
					  .D(WrDat[45]), 
                           		  .EN(RWLR2Wire7), 
					  .Z(RdCol1Line[45]) );
 	    LATNT1 LatR2C0B46( .G(WWLW4Wire3), 
					  .D(WrDat[46]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol0Line[46]) );
 	    LATNT1 LatR2C1B46( .G(WWLW5Wire3), 
					  .D(WrDat[46]), 
                           		  .EN(RWLR2Wire7), 
					  .Z(RdCol1Line[46]) );
 	    LATNT1 LatR2C0B47( .G(WWLW4Wire3), 
					  .D(WrDat[47]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol0Line[47]) );
 	    LATNT1 LatR2C1B47( .G(WWLW5Wire3), 
					  .D(WrDat[47]), 
                           		  .EN(RWLR2Wire7), 
					  .Z(RdCol1Line[47]) );
 	    LATNT1 LatR2C0B48( .G(WWLW4Wire3), 
					  .D(WrDat[48]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol0Line[48]) );
 	    LATNT1 LatR2C1B48( .G(WWLW5Wire3), 
					  .D(WrDat[48]), 
                           		  .EN(RWLR2Wire7), 
					  .Z(RdCol1Line[48]) );
 	    LATNT1 LatR2C0B49( .G(WWLW4Wire3), 
					  .D(WrDat[49]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol0Line[49]) );
 	    LATNT1 LatR2C1B49( .G(WWLW5Wire3), 
					  .D(WrDat[49]), 
                           		  .EN(RWLR2Wire7), 
					  .Z(RdCol1Line[49]) );
 	    LATNT1 LatR2C0B50( .G(WWLW4Wire3), 
					  .D(WrDat[50]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol0Line[50]) );
 	    LATNT1 LatR2C1B50( .G(WWLW5Wire3), 
					  .D(WrDat[50]), 
                           		  .EN(RWLR2Wire7), 
					  .Z(RdCol1Line[50]) );
 	    LATNT1 LatR2C0B51( .G(WWLW4Wire3), 
					  .D(WrDat[51]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol0Line[51]) );
 	    LATNT1 LatR2C1B51( .G(WWLW5Wire3), 
					  .D(WrDat[51]), 
                           		  .EN(RWLR2Wire7), 
					  .Z(RdCol1Line[51]) );
 	    LATNT1 LatR2C0B52( .G(WWLW4Wire3), 
					  .D(WrDat[52]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol0Line[52]) );
 	    LATNT1 LatR2C1B52( .G(WWLW5Wire3), 
					  .D(WrDat[52]), 
                           		  .EN(RWLR2Wire7), 
					  .Z(RdCol1Line[52]) );
 	    LATNT1 LatR2C0B53( .G(WWLW4Wire3), 
					  .D(WrDat[53]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol0Line[53]) );
 	    LATNT1 LatR2C1B53( .G(WWLW5Wire3), 
					  .D(WrDat[53]), 
                           		  .EN(RWLR2Wire7), 
					  .Z(RdCol1Line[53]) );
 	    LATNT1 LatR2C0B54( .G(WWLW4Wire3), 
					  .D(WrDat[54]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol0Line[54]) );
 	    LATNT1 LatR2C1B54( .G(WWLW5Wire3), 
					  .D(WrDat[54]), 
                           		  .EN(RWLR2Wire7), 
					  .Z(RdCol1Line[54]) );
 	    LATNT1 LatR2C0B55( .G(WWLW4Wire3), 
					  .D(WrDat[55]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol0Line[55]) );
 	    LATNT1 LatR2C1B55( .G(WWLW5Wire3), 
					  .D(WrDat[55]), 
                           		  .EN(RWLR2Wire7), 
					  .Z(RdCol1Line[55]) );
 	    LATNT1 LatR2C0B56( .G(WWLW4Wire3), 
					  .D(WrDat[56]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol0Line[56]) );
 	    LATNT1 LatR2C1B56( .G(WWLW5Wire3), 
					  .D(WrDat[56]), 
                           		  .EN(RWLR2Wire7), 
					  .Z(RdCol1Line[56]) );
 	    LATNT1 LatR2C0B57( .G(WWLW4Wire3), 
					  .D(WrDat[57]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol0Line[57]) );
 	    LATNT1 LatR2C1B57( .G(WWLW5Wire3), 
					  .D(WrDat[57]), 
                           		  .EN(RWLR2Wire7), 
					  .Z(RdCol1Line[57]) );
 	    LATNT1 LatR2C0B58( .G(WWLW4Wire3), 
					  .D(WrDat[58]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol0Line[58]) );
 	    LATNT1 LatR2C1B58( .G(WWLW5Wire3), 
					  .D(WrDat[58]), 
                           		  .EN(RWLR2Wire7), 
					  .Z(RdCol1Line[58]) );
 	    LATNT1 LatR2C0B59( .G(WWLW4Wire3), 
					  .D(WrDat[59]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol0Line[59]) );
 	    LATNT1 LatR2C1B59( .G(WWLW5Wire3), 
					  .D(WrDat[59]), 
                           		  .EN(RWLR2Wire7), 
					  .Z(RdCol1Line[59]) );
 	    LATNT1 LatR2C0B60( .G(WWLW4Wire3), 
					  .D(WrDat[60]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol0Line[60]) );
 	    LATNT1 LatR2C1B60( .G(WWLW5Wire3), 
					  .D(WrDat[60]), 
                           		  .EN(RWLR2Wire7), 
					  .Z(RdCol1Line[60]) );
     
            //
            // Row 3
	    //
	      
 	    LATNT1 LatR3C0B0( .G(WWLW6Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR3C1B0( .G(WWLW7Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR3Wire4), 
					  .Z(RdCol1Line[0]) );
 	    LATNT1 LatR3C0B1( .G(WWLW6Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR3C1B1( .G(WWLW7Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR3Wire4), 
					  .Z(RdCol1Line[1]) );
 	    LATNT1 LatR3C0B2( .G(WWLW6Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR3C1B2( .G(WWLW7Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR3Wire4), 
					  .Z(RdCol1Line[2]) );
 	    LATNT1 LatR3C0B3( .G(WWLW6Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR3C1B3( .G(WWLW7Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR3Wire4), 
					  .Z(RdCol1Line[3]) );
 	    LATNT1 LatR3C0B4( .G(WWLW6Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[4]) );
 	    LATNT1 LatR3C1B4( .G(WWLW7Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR3Wire4), 
					  .Z(RdCol1Line[4]) );
 	    LATNT1 LatR3C0B5( .G(WWLW6Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[5]) );
 	    LATNT1 LatR3C1B5( .G(WWLW7Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR3Wire4), 
					  .Z(RdCol1Line[5]) );
 	    LATNT1 LatR3C0B6( .G(WWLW6Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[6]) );
 	    LATNT1 LatR3C1B6( .G(WWLW7Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR3Wire4), 
					  .Z(RdCol1Line[6]) );
 	    LATNT1 LatR3C0B7( .G(WWLW6Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[7]) );
 	    LATNT1 LatR3C1B7( .G(WWLW7Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR3Wire4), 
					  .Z(RdCol1Line[7]) );
 	    LATNT1 LatR3C0B8( .G(WWLW6Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[8]) );
 	    LATNT1 LatR3C1B8( .G(WWLW7Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR3Wire4), 
					  .Z(RdCol1Line[8]) );
 	    LATNT1 LatR3C0B9( .G(WWLW6Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[9]) );
 	    LATNT1 LatR3C1B9( .G(WWLW7Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR3Wire4), 
					  .Z(RdCol1Line[9]) );
 	    LATNT1 LatR3C0B10( .G(WWLW6Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[10]) );
 	    LATNT1 LatR3C1B10( .G(WWLW7Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR3Wire4), 
					  .Z(RdCol1Line[10]) );
 	    LATNT1 LatR3C0B11( .G(WWLW6Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[11]) );
 	    LATNT1 LatR3C1B11( .G(WWLW7Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR3Wire4), 
					  .Z(RdCol1Line[11]) );
 	    LATNT1 LatR3C0B12( .G(WWLW6Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[12]) );
 	    LATNT1 LatR3C1B12( .G(WWLW7Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR3Wire4), 
					  .Z(RdCol1Line[12]) );
 	    LATNT1 LatR3C0B13( .G(WWLW6Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[13]) );
 	    LATNT1 LatR3C1B13( .G(WWLW7Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR3Wire4), 
					  .Z(RdCol1Line[13]) );
 	    LATNT1 LatR3C0B14( .G(WWLW6Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[14]) );
 	    LATNT1 LatR3C1B14( .G(WWLW7Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR3Wire4), 
					  .Z(RdCol1Line[14]) );
 	    LATNT1 LatR3C0B15( .G(WWLW6Wire1), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[15]) );
 	    LATNT1 LatR3C1B15( .G(WWLW7Wire1), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR3Wire5), 
					  .Z(RdCol1Line[15]) );
 	    LATNT1 LatR3C0B16( .G(WWLW6Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[16]) );
 	    LATNT1 LatR3C1B16( .G(WWLW7Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR3Wire5), 
					  .Z(RdCol1Line[16]) );
 	    LATNT1 LatR3C0B17( .G(WWLW6Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[17]) );
 	    LATNT1 LatR3C1B17( .G(WWLW7Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR3Wire5), 
					  .Z(RdCol1Line[17]) );
 	    LATNT1 LatR3C0B18( .G(WWLW6Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[18]) );
 	    LATNT1 LatR3C1B18( .G(WWLW7Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR3Wire5), 
					  .Z(RdCol1Line[18]) );
 	    LATNT1 LatR3C0B19( .G(WWLW6Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[19]) );
 	    LATNT1 LatR3C1B19( .G(WWLW7Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR3Wire5), 
					  .Z(RdCol1Line[19]) );
 	    LATNT1 LatR3C0B20( .G(WWLW6Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[20]) );
 	    LATNT1 LatR3C1B20( .G(WWLW7Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR3Wire5), 
					  .Z(RdCol1Line[20]) );
 	    LATNT1 LatR3C0B21( .G(WWLW6Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[21]) );
 	    LATNT1 LatR3C1B21( .G(WWLW7Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR3Wire5), 
					  .Z(RdCol1Line[21]) );
 	    LATNT1 LatR3C0B22( .G(WWLW6Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[22]) );
 	    LATNT1 LatR3C1B22( .G(WWLW7Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR3Wire5), 
					  .Z(RdCol1Line[22]) );
 	    LATNT1 LatR3C0B23( .G(WWLW6Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[23]) );
 	    LATNT1 LatR3C1B23( .G(WWLW7Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR3Wire5), 
					  .Z(RdCol1Line[23]) );
 	    LATNT1 LatR3C0B24( .G(WWLW6Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[24]) );
 	    LATNT1 LatR3C1B24( .G(WWLW7Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR3Wire5), 
					  .Z(RdCol1Line[24]) );
 	    LATNT1 LatR3C0B25( .G(WWLW6Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[25]) );
 	    LATNT1 LatR3C1B25( .G(WWLW7Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR3Wire5), 
					  .Z(RdCol1Line[25]) );
 	    LATNT1 LatR3C0B26( .G(WWLW6Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[26]) );
 	    LATNT1 LatR3C1B26( .G(WWLW7Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR3Wire5), 
					  .Z(RdCol1Line[26]) );
 	    LATNT1 LatR3C0B27( .G(WWLW6Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[27]) );
 	    LATNT1 LatR3C1B27( .G(WWLW7Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR3Wire5), 
					  .Z(RdCol1Line[27]) );
 	    LATNT1 LatR3C0B28( .G(WWLW6Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[28]) );
 	    LATNT1 LatR3C1B28( .G(WWLW7Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR3Wire5), 
					  .Z(RdCol1Line[28]) );
 	    LATNT1 LatR3C0B29( .G(WWLW6Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[29]) );
 	    LATNT1 LatR3C1B29( .G(WWLW7Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR3Wire5), 
					  .Z(RdCol1Line[29]) );
 	    LATNT1 LatR3C0B30( .G(WWLW6Wire2), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol0Line[30]) );
 	    LATNT1 LatR3C1B30( .G(WWLW7Wire2), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR3Wire6), 
					  .Z(RdCol1Line[30]) );
 	    LATNT1 LatR3C0B31( .G(WWLW6Wire2), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol0Line[31]) );
 	    LATNT1 LatR3C1B31( .G(WWLW7Wire2), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR3Wire6), 
					  .Z(RdCol1Line[31]) );
 	    LATNT1 LatR3C0B32( .G(WWLW6Wire2), 
					  .D(WrDat[32]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol0Line[32]) );
 	    LATNT1 LatR3C1B32( .G(WWLW7Wire2), 
					  .D(WrDat[32]), 
                           		  .EN(RWLR3Wire6), 
					  .Z(RdCol1Line[32]) );
 	    LATNT1 LatR3C0B33( .G(WWLW6Wire2), 
					  .D(WrDat[33]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol0Line[33]) );
 	    LATNT1 LatR3C1B33( .G(WWLW7Wire2), 
					  .D(WrDat[33]), 
                           		  .EN(RWLR3Wire6), 
					  .Z(RdCol1Line[33]) );
 	    LATNT1 LatR3C0B34( .G(WWLW6Wire2), 
					  .D(WrDat[34]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol0Line[34]) );
 	    LATNT1 LatR3C1B34( .G(WWLW7Wire2), 
					  .D(WrDat[34]), 
                           		  .EN(RWLR3Wire6), 
					  .Z(RdCol1Line[34]) );
 	    LATNT1 LatR3C0B35( .G(WWLW6Wire2), 
					  .D(WrDat[35]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol0Line[35]) );
 	    LATNT1 LatR3C1B35( .G(WWLW7Wire2), 
					  .D(WrDat[35]), 
                           		  .EN(RWLR3Wire6), 
					  .Z(RdCol1Line[35]) );
 	    LATNT1 LatR3C0B36( .G(WWLW6Wire2), 
					  .D(WrDat[36]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol0Line[36]) );
 	    LATNT1 LatR3C1B36( .G(WWLW7Wire2), 
					  .D(WrDat[36]), 
                           		  .EN(RWLR3Wire6), 
					  .Z(RdCol1Line[36]) );
 	    LATNT1 LatR3C0B37( .G(WWLW6Wire2), 
					  .D(WrDat[37]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol0Line[37]) );
 	    LATNT1 LatR3C1B37( .G(WWLW7Wire2), 
					  .D(WrDat[37]), 
                           		  .EN(RWLR3Wire6), 
					  .Z(RdCol1Line[37]) );
 	    LATNT1 LatR3C0B38( .G(WWLW6Wire2), 
					  .D(WrDat[38]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol0Line[38]) );
 	    LATNT1 LatR3C1B38( .G(WWLW7Wire2), 
					  .D(WrDat[38]), 
                           		  .EN(RWLR3Wire6), 
					  .Z(RdCol1Line[38]) );
 	    LATNT1 LatR3C0B39( .G(WWLW6Wire2), 
					  .D(WrDat[39]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol0Line[39]) );
 	    LATNT1 LatR3C1B39( .G(WWLW7Wire2), 
					  .D(WrDat[39]), 
                           		  .EN(RWLR3Wire6), 
					  .Z(RdCol1Line[39]) );
 	    LATNT1 LatR3C0B40( .G(WWLW6Wire2), 
					  .D(WrDat[40]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol0Line[40]) );
 	    LATNT1 LatR3C1B40( .G(WWLW7Wire2), 
					  .D(WrDat[40]), 
                           		  .EN(RWLR3Wire6), 
					  .Z(RdCol1Line[40]) );
 	    LATNT1 LatR3C0B41( .G(WWLW6Wire2), 
					  .D(WrDat[41]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol0Line[41]) );
 	    LATNT1 LatR3C1B41( .G(WWLW7Wire2), 
					  .D(WrDat[41]), 
                           		  .EN(RWLR3Wire6), 
					  .Z(RdCol1Line[41]) );
 	    LATNT1 LatR3C0B42( .G(WWLW6Wire2), 
					  .D(WrDat[42]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol0Line[42]) );
 	    LATNT1 LatR3C1B42( .G(WWLW7Wire2), 
					  .D(WrDat[42]), 
                           		  .EN(RWLR3Wire6), 
					  .Z(RdCol1Line[42]) );
 	    LATNT1 LatR3C0B43( .G(WWLW6Wire2), 
					  .D(WrDat[43]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol0Line[43]) );
 	    LATNT1 LatR3C1B43( .G(WWLW7Wire2), 
					  .D(WrDat[43]), 
                           		  .EN(RWLR3Wire6), 
					  .Z(RdCol1Line[43]) );
 	    LATNT1 LatR3C0B44( .G(WWLW6Wire2), 
					  .D(WrDat[44]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol0Line[44]) );
 	    LATNT1 LatR3C1B44( .G(WWLW7Wire2), 
					  .D(WrDat[44]), 
                           		  .EN(RWLR3Wire6), 
					  .Z(RdCol1Line[44]) );
 	    LATNT1 LatR3C0B45( .G(WWLW6Wire3), 
					  .D(WrDat[45]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol0Line[45]) );
 	    LATNT1 LatR3C1B45( .G(WWLW7Wire3), 
					  .D(WrDat[45]), 
                           		  .EN(RWLR3Wire7), 
					  .Z(RdCol1Line[45]) );
 	    LATNT1 LatR3C0B46( .G(WWLW6Wire3), 
					  .D(WrDat[46]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol0Line[46]) );
 	    LATNT1 LatR3C1B46( .G(WWLW7Wire3), 
					  .D(WrDat[46]), 
                           		  .EN(RWLR3Wire7), 
					  .Z(RdCol1Line[46]) );
 	    LATNT1 LatR3C0B47( .G(WWLW6Wire3), 
					  .D(WrDat[47]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol0Line[47]) );
 	    LATNT1 LatR3C1B47( .G(WWLW7Wire3), 
					  .D(WrDat[47]), 
                           		  .EN(RWLR3Wire7), 
					  .Z(RdCol1Line[47]) );
 	    LATNT1 LatR3C0B48( .G(WWLW6Wire3), 
					  .D(WrDat[48]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol0Line[48]) );
 	    LATNT1 LatR3C1B48( .G(WWLW7Wire3), 
					  .D(WrDat[48]), 
                           		  .EN(RWLR3Wire7), 
					  .Z(RdCol1Line[48]) );
 	    LATNT1 LatR3C0B49( .G(WWLW6Wire3), 
					  .D(WrDat[49]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol0Line[49]) );
 	    LATNT1 LatR3C1B49( .G(WWLW7Wire3), 
					  .D(WrDat[49]), 
                           		  .EN(RWLR3Wire7), 
					  .Z(RdCol1Line[49]) );
 	    LATNT1 LatR3C0B50( .G(WWLW6Wire3), 
					  .D(WrDat[50]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol0Line[50]) );
 	    LATNT1 LatR3C1B50( .G(WWLW7Wire3), 
					  .D(WrDat[50]), 
                           		  .EN(RWLR3Wire7), 
					  .Z(RdCol1Line[50]) );
 	    LATNT1 LatR3C0B51( .G(WWLW6Wire3), 
					  .D(WrDat[51]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol0Line[51]) );
 	    LATNT1 LatR3C1B51( .G(WWLW7Wire3), 
					  .D(WrDat[51]), 
                           		  .EN(RWLR3Wire7), 
					  .Z(RdCol1Line[51]) );
 	    LATNT1 LatR3C0B52( .G(WWLW6Wire3), 
					  .D(WrDat[52]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol0Line[52]) );
 	    LATNT1 LatR3C1B52( .G(WWLW7Wire3), 
					  .D(WrDat[52]), 
                           		  .EN(RWLR3Wire7), 
					  .Z(RdCol1Line[52]) );
 	    LATNT1 LatR3C0B53( .G(WWLW6Wire3), 
					  .D(WrDat[53]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol0Line[53]) );
 	    LATNT1 LatR3C1B53( .G(WWLW7Wire3), 
					  .D(WrDat[53]), 
                           		  .EN(RWLR3Wire7), 
					  .Z(RdCol1Line[53]) );
 	    LATNT1 LatR3C0B54( .G(WWLW6Wire3), 
					  .D(WrDat[54]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol0Line[54]) );
 	    LATNT1 LatR3C1B54( .G(WWLW7Wire3), 
					  .D(WrDat[54]), 
                           		  .EN(RWLR3Wire7), 
					  .Z(RdCol1Line[54]) );
 	    LATNT1 LatR3C0B55( .G(WWLW6Wire3), 
					  .D(WrDat[55]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol0Line[55]) );
 	    LATNT1 LatR3C1B55( .G(WWLW7Wire3), 
					  .D(WrDat[55]), 
                           		  .EN(RWLR3Wire7), 
					  .Z(RdCol1Line[55]) );
 	    LATNT1 LatR3C0B56( .G(WWLW6Wire3), 
					  .D(WrDat[56]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol0Line[56]) );
 	    LATNT1 LatR3C1B56( .G(WWLW7Wire3), 
					  .D(WrDat[56]), 
                           		  .EN(RWLR3Wire7), 
					  .Z(RdCol1Line[56]) );
 	    LATNT1 LatR3C0B57( .G(WWLW6Wire3), 
					  .D(WrDat[57]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol0Line[57]) );
 	    LATNT1 LatR3C1B57( .G(WWLW7Wire3), 
					  .D(WrDat[57]), 
                           		  .EN(RWLR3Wire7), 
					  .Z(RdCol1Line[57]) );
 	    LATNT1 LatR3C0B58( .G(WWLW6Wire3), 
					  .D(WrDat[58]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol0Line[58]) );
 	    LATNT1 LatR3C1B58( .G(WWLW7Wire3), 
					  .D(WrDat[58]), 
                           		  .EN(RWLR3Wire7), 
					  .Z(RdCol1Line[58]) );
 	    LATNT1 LatR3C0B59( .G(WWLW6Wire3), 
					  .D(WrDat[59]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol0Line[59]) );
 	    LATNT1 LatR3C1B59( .G(WWLW7Wire3), 
					  .D(WrDat[59]), 
                           		  .EN(RWLR3Wire7), 
					  .Z(RdCol1Line[59]) );
 	    LATNT1 LatR3C0B60( .G(WWLW6Wire3), 
					  .D(WrDat[60]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol0Line[60]) );
 	    LATNT1 LatR3C1B60( .G(WWLW7Wire3), 
					  .D(WrDat[60]), 
                           		  .EN(RWLR3Wire7), 
					  .Z(RdCol1Line[60]) );
     
            //
            // Row 4
	    //
	      
 	    LATNT1 LatR4C0B0( .G(WWLW8Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR4C1B0( .G(WWLW9Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR4Wire4), 
					  .Z(RdCol1Line[0]) );
 	    LATNT1 LatR4C0B1( .G(WWLW8Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR4C1B1( .G(WWLW9Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR4Wire4), 
					  .Z(RdCol1Line[1]) );
 	    LATNT1 LatR4C0B2( .G(WWLW8Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR4C1B2( .G(WWLW9Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR4Wire4), 
					  .Z(RdCol1Line[2]) );
 	    LATNT1 LatR4C0B3( .G(WWLW8Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR4C1B3( .G(WWLW9Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR4Wire4), 
					  .Z(RdCol1Line[3]) );
 	    LATNT1 LatR4C0B4( .G(WWLW8Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[4]) );
 	    LATNT1 LatR4C1B4( .G(WWLW9Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR4Wire4), 
					  .Z(RdCol1Line[4]) );
 	    LATNT1 LatR4C0B5( .G(WWLW8Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[5]) );
 	    LATNT1 LatR4C1B5( .G(WWLW9Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR4Wire4), 
					  .Z(RdCol1Line[5]) );
 	    LATNT1 LatR4C0B6( .G(WWLW8Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[6]) );
 	    LATNT1 LatR4C1B6( .G(WWLW9Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR4Wire4), 
					  .Z(RdCol1Line[6]) );
 	    LATNT1 LatR4C0B7( .G(WWLW8Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[7]) );
 	    LATNT1 LatR4C1B7( .G(WWLW9Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR4Wire4), 
					  .Z(RdCol1Line[7]) );
 	    LATNT1 LatR4C0B8( .G(WWLW8Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[8]) );
 	    LATNT1 LatR4C1B8( .G(WWLW9Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR4Wire4), 
					  .Z(RdCol1Line[8]) );
 	    LATNT1 LatR4C0B9( .G(WWLW8Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[9]) );
 	    LATNT1 LatR4C1B9( .G(WWLW9Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR4Wire4), 
					  .Z(RdCol1Line[9]) );
 	    LATNT1 LatR4C0B10( .G(WWLW8Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[10]) );
 	    LATNT1 LatR4C1B10( .G(WWLW9Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR4Wire4), 
					  .Z(RdCol1Line[10]) );
 	    LATNT1 LatR4C0B11( .G(WWLW8Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[11]) );
 	    LATNT1 LatR4C1B11( .G(WWLW9Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR4Wire4), 
					  .Z(RdCol1Line[11]) );
 	    LATNT1 LatR4C0B12( .G(WWLW8Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[12]) );
 	    LATNT1 LatR4C1B12( .G(WWLW9Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR4Wire4), 
					  .Z(RdCol1Line[12]) );
 	    LATNT1 LatR4C0B13( .G(WWLW8Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[13]) );
 	    LATNT1 LatR4C1B13( .G(WWLW9Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR4Wire4), 
					  .Z(RdCol1Line[13]) );
 	    LATNT1 LatR4C0B14( .G(WWLW8Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[14]) );
 	    LATNT1 LatR4C1B14( .G(WWLW9Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR4Wire4), 
					  .Z(RdCol1Line[14]) );
 	    LATNT1 LatR4C0B15( .G(WWLW8Wire1), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[15]) );
 	    LATNT1 LatR4C1B15( .G(WWLW9Wire1), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR4Wire5), 
					  .Z(RdCol1Line[15]) );
 	    LATNT1 LatR4C0B16( .G(WWLW8Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[16]) );
 	    LATNT1 LatR4C1B16( .G(WWLW9Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR4Wire5), 
					  .Z(RdCol1Line[16]) );
 	    LATNT1 LatR4C0B17( .G(WWLW8Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[17]) );
 	    LATNT1 LatR4C1B17( .G(WWLW9Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR4Wire5), 
					  .Z(RdCol1Line[17]) );
 	    LATNT1 LatR4C0B18( .G(WWLW8Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[18]) );
 	    LATNT1 LatR4C1B18( .G(WWLW9Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR4Wire5), 
					  .Z(RdCol1Line[18]) );
 	    LATNT1 LatR4C0B19( .G(WWLW8Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[19]) );
 	    LATNT1 LatR4C1B19( .G(WWLW9Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR4Wire5), 
					  .Z(RdCol1Line[19]) );
 	    LATNT1 LatR4C0B20( .G(WWLW8Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[20]) );
 	    LATNT1 LatR4C1B20( .G(WWLW9Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR4Wire5), 
					  .Z(RdCol1Line[20]) );
 	    LATNT1 LatR4C0B21( .G(WWLW8Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[21]) );
 	    LATNT1 LatR4C1B21( .G(WWLW9Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR4Wire5), 
					  .Z(RdCol1Line[21]) );
 	    LATNT1 LatR4C0B22( .G(WWLW8Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[22]) );
 	    LATNT1 LatR4C1B22( .G(WWLW9Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR4Wire5), 
					  .Z(RdCol1Line[22]) );
 	    LATNT1 LatR4C0B23( .G(WWLW8Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[23]) );
 	    LATNT1 LatR4C1B23( .G(WWLW9Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR4Wire5), 
					  .Z(RdCol1Line[23]) );
 	    LATNT1 LatR4C0B24( .G(WWLW8Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[24]) );
 	    LATNT1 LatR4C1B24( .G(WWLW9Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR4Wire5), 
					  .Z(RdCol1Line[24]) );
 	    LATNT1 LatR4C0B25( .G(WWLW8Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[25]) );
 	    LATNT1 LatR4C1B25( .G(WWLW9Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR4Wire5), 
					  .Z(RdCol1Line[25]) );
 	    LATNT1 LatR4C0B26( .G(WWLW8Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[26]) );
 	    LATNT1 LatR4C1B26( .G(WWLW9Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR4Wire5), 
					  .Z(RdCol1Line[26]) );
 	    LATNT1 LatR4C0B27( .G(WWLW8Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[27]) );
 	    LATNT1 LatR4C1B27( .G(WWLW9Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR4Wire5), 
					  .Z(RdCol1Line[27]) );
 	    LATNT1 LatR4C0B28( .G(WWLW8Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[28]) );
 	    LATNT1 LatR4C1B28( .G(WWLW9Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR4Wire5), 
					  .Z(RdCol1Line[28]) );
 	    LATNT1 LatR4C0B29( .G(WWLW8Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[29]) );
 	    LATNT1 LatR4C1B29( .G(WWLW9Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR4Wire5), 
					  .Z(RdCol1Line[29]) );
 	    LATNT1 LatR4C0B30( .G(WWLW8Wire2), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol0Line[30]) );
 	    LATNT1 LatR4C1B30( .G(WWLW9Wire2), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR4Wire6), 
					  .Z(RdCol1Line[30]) );
 	    LATNT1 LatR4C0B31( .G(WWLW8Wire2), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol0Line[31]) );
 	    LATNT1 LatR4C1B31( .G(WWLW9Wire2), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR4Wire6), 
					  .Z(RdCol1Line[31]) );
 	    LATNT1 LatR4C0B32( .G(WWLW8Wire2), 
					  .D(WrDat[32]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol0Line[32]) );
 	    LATNT1 LatR4C1B32( .G(WWLW9Wire2), 
					  .D(WrDat[32]), 
                           		  .EN(RWLR4Wire6), 
					  .Z(RdCol1Line[32]) );
 	    LATNT1 LatR4C0B33( .G(WWLW8Wire2), 
					  .D(WrDat[33]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol0Line[33]) );
 	    LATNT1 LatR4C1B33( .G(WWLW9Wire2), 
					  .D(WrDat[33]), 
                           		  .EN(RWLR4Wire6), 
					  .Z(RdCol1Line[33]) );
 	    LATNT1 LatR4C0B34( .G(WWLW8Wire2), 
					  .D(WrDat[34]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol0Line[34]) );
 	    LATNT1 LatR4C1B34( .G(WWLW9Wire2), 
					  .D(WrDat[34]), 
                           		  .EN(RWLR4Wire6), 
					  .Z(RdCol1Line[34]) );
 	    LATNT1 LatR4C0B35( .G(WWLW8Wire2), 
					  .D(WrDat[35]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol0Line[35]) );
 	    LATNT1 LatR4C1B35( .G(WWLW9Wire2), 
					  .D(WrDat[35]), 
                           		  .EN(RWLR4Wire6), 
					  .Z(RdCol1Line[35]) );
 	    LATNT1 LatR4C0B36( .G(WWLW8Wire2), 
					  .D(WrDat[36]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol0Line[36]) );
 	    LATNT1 LatR4C1B36( .G(WWLW9Wire2), 
					  .D(WrDat[36]), 
                           		  .EN(RWLR4Wire6), 
					  .Z(RdCol1Line[36]) );
 	    LATNT1 LatR4C0B37( .G(WWLW8Wire2), 
					  .D(WrDat[37]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol0Line[37]) );
 	    LATNT1 LatR4C1B37( .G(WWLW9Wire2), 
					  .D(WrDat[37]), 
                           		  .EN(RWLR4Wire6), 
					  .Z(RdCol1Line[37]) );
 	    LATNT1 LatR4C0B38( .G(WWLW8Wire2), 
					  .D(WrDat[38]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol0Line[38]) );
 	    LATNT1 LatR4C1B38( .G(WWLW9Wire2), 
					  .D(WrDat[38]), 
                           		  .EN(RWLR4Wire6), 
					  .Z(RdCol1Line[38]) );
 	    LATNT1 LatR4C0B39( .G(WWLW8Wire2), 
					  .D(WrDat[39]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol0Line[39]) );
 	    LATNT1 LatR4C1B39( .G(WWLW9Wire2), 
					  .D(WrDat[39]), 
                           		  .EN(RWLR4Wire6), 
					  .Z(RdCol1Line[39]) );
 	    LATNT1 LatR4C0B40( .G(WWLW8Wire2), 
					  .D(WrDat[40]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol0Line[40]) );
 	    LATNT1 LatR4C1B40( .G(WWLW9Wire2), 
					  .D(WrDat[40]), 
                           		  .EN(RWLR4Wire6), 
					  .Z(RdCol1Line[40]) );
 	    LATNT1 LatR4C0B41( .G(WWLW8Wire2), 
					  .D(WrDat[41]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol0Line[41]) );
 	    LATNT1 LatR4C1B41( .G(WWLW9Wire2), 
					  .D(WrDat[41]), 
                           		  .EN(RWLR4Wire6), 
					  .Z(RdCol1Line[41]) );
 	    LATNT1 LatR4C0B42( .G(WWLW8Wire2), 
					  .D(WrDat[42]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol0Line[42]) );
 	    LATNT1 LatR4C1B42( .G(WWLW9Wire2), 
					  .D(WrDat[42]), 
                           		  .EN(RWLR4Wire6), 
					  .Z(RdCol1Line[42]) );
 	    LATNT1 LatR4C0B43( .G(WWLW8Wire2), 
					  .D(WrDat[43]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol0Line[43]) );
 	    LATNT1 LatR4C1B43( .G(WWLW9Wire2), 
					  .D(WrDat[43]), 
                           		  .EN(RWLR4Wire6), 
					  .Z(RdCol1Line[43]) );
 	    LATNT1 LatR4C0B44( .G(WWLW8Wire2), 
					  .D(WrDat[44]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol0Line[44]) );
 	    LATNT1 LatR4C1B44( .G(WWLW9Wire2), 
					  .D(WrDat[44]), 
                           		  .EN(RWLR4Wire6), 
					  .Z(RdCol1Line[44]) );
 	    LATNT1 LatR4C0B45( .G(WWLW8Wire3), 
					  .D(WrDat[45]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol0Line[45]) );
 	    LATNT1 LatR4C1B45( .G(WWLW9Wire3), 
					  .D(WrDat[45]), 
                           		  .EN(RWLR4Wire7), 
					  .Z(RdCol1Line[45]) );
 	    LATNT1 LatR4C0B46( .G(WWLW8Wire3), 
					  .D(WrDat[46]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol0Line[46]) );
 	    LATNT1 LatR4C1B46( .G(WWLW9Wire3), 
					  .D(WrDat[46]), 
                           		  .EN(RWLR4Wire7), 
					  .Z(RdCol1Line[46]) );
 	    LATNT1 LatR4C0B47( .G(WWLW8Wire3), 
					  .D(WrDat[47]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol0Line[47]) );
 	    LATNT1 LatR4C1B47( .G(WWLW9Wire3), 
					  .D(WrDat[47]), 
                           		  .EN(RWLR4Wire7), 
					  .Z(RdCol1Line[47]) );
 	    LATNT1 LatR4C0B48( .G(WWLW8Wire3), 
					  .D(WrDat[48]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol0Line[48]) );
 	    LATNT1 LatR4C1B48( .G(WWLW9Wire3), 
					  .D(WrDat[48]), 
                           		  .EN(RWLR4Wire7), 
					  .Z(RdCol1Line[48]) );
 	    LATNT1 LatR4C0B49( .G(WWLW8Wire3), 
					  .D(WrDat[49]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol0Line[49]) );
 	    LATNT1 LatR4C1B49( .G(WWLW9Wire3), 
					  .D(WrDat[49]), 
                           		  .EN(RWLR4Wire7), 
					  .Z(RdCol1Line[49]) );
 	    LATNT1 LatR4C0B50( .G(WWLW8Wire3), 
					  .D(WrDat[50]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol0Line[50]) );
 	    LATNT1 LatR4C1B50( .G(WWLW9Wire3), 
					  .D(WrDat[50]), 
                           		  .EN(RWLR4Wire7), 
					  .Z(RdCol1Line[50]) );
 	    LATNT1 LatR4C0B51( .G(WWLW8Wire3), 
					  .D(WrDat[51]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol0Line[51]) );
 	    LATNT1 LatR4C1B51( .G(WWLW9Wire3), 
					  .D(WrDat[51]), 
                           		  .EN(RWLR4Wire7), 
					  .Z(RdCol1Line[51]) );
 	    LATNT1 LatR4C0B52( .G(WWLW8Wire3), 
					  .D(WrDat[52]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol0Line[52]) );
 	    LATNT1 LatR4C1B52( .G(WWLW9Wire3), 
					  .D(WrDat[52]), 
                           		  .EN(RWLR4Wire7), 
					  .Z(RdCol1Line[52]) );
 	    LATNT1 LatR4C0B53( .G(WWLW8Wire3), 
					  .D(WrDat[53]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol0Line[53]) );
 	    LATNT1 LatR4C1B53( .G(WWLW9Wire3), 
					  .D(WrDat[53]), 
                           		  .EN(RWLR4Wire7), 
					  .Z(RdCol1Line[53]) );
 	    LATNT1 LatR4C0B54( .G(WWLW8Wire3), 
					  .D(WrDat[54]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol0Line[54]) );
 	    LATNT1 LatR4C1B54( .G(WWLW9Wire3), 
					  .D(WrDat[54]), 
                           		  .EN(RWLR4Wire7), 
					  .Z(RdCol1Line[54]) );
 	    LATNT1 LatR4C0B55( .G(WWLW8Wire3), 
					  .D(WrDat[55]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol0Line[55]) );
 	    LATNT1 LatR4C1B55( .G(WWLW9Wire3), 
					  .D(WrDat[55]), 
                           		  .EN(RWLR4Wire7), 
					  .Z(RdCol1Line[55]) );
 	    LATNT1 LatR4C0B56( .G(WWLW8Wire3), 
					  .D(WrDat[56]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol0Line[56]) );
 	    LATNT1 LatR4C1B56( .G(WWLW9Wire3), 
					  .D(WrDat[56]), 
                           		  .EN(RWLR4Wire7), 
					  .Z(RdCol1Line[56]) );
 	    LATNT1 LatR4C0B57( .G(WWLW8Wire3), 
					  .D(WrDat[57]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol0Line[57]) );
 	    LATNT1 LatR4C1B57( .G(WWLW9Wire3), 
					  .D(WrDat[57]), 
                           		  .EN(RWLR4Wire7), 
					  .Z(RdCol1Line[57]) );
 	    LATNT1 LatR4C0B58( .G(WWLW8Wire3), 
					  .D(WrDat[58]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol0Line[58]) );
 	    LATNT1 LatR4C1B58( .G(WWLW9Wire3), 
					  .D(WrDat[58]), 
                           		  .EN(RWLR4Wire7), 
					  .Z(RdCol1Line[58]) );
 	    LATNT1 LatR4C0B59( .G(WWLW8Wire3), 
					  .D(WrDat[59]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol0Line[59]) );
 	    LATNT1 LatR4C1B59( .G(WWLW9Wire3), 
					  .D(WrDat[59]), 
                           		  .EN(RWLR4Wire7), 
					  .Z(RdCol1Line[59]) );
 	    LATNT1 LatR4C0B60( .G(WWLW8Wire3), 
					  .D(WrDat[60]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol0Line[60]) );
 	    LATNT1 LatR4C1B60( .G(WWLW9Wire3), 
					  .D(WrDat[60]), 
                           		  .EN(RWLR4Wire7), 
					  .Z(RdCol1Line[60]) );
     
            //
            // Row 5
	    //
	      
 	    LATNT1 LatR5C0B0( .G(WWLW10Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR5C1B0( .G(WWLW11Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR5Wire4), 
					  .Z(RdCol1Line[0]) );
 	    LATNT1 LatR5C0B1( .G(WWLW10Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR5C1B1( .G(WWLW11Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR5Wire4), 
					  .Z(RdCol1Line[1]) );
 	    LATNT1 LatR5C0B2( .G(WWLW10Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR5C1B2( .G(WWLW11Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR5Wire4), 
					  .Z(RdCol1Line[2]) );
 	    LATNT1 LatR5C0B3( .G(WWLW10Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR5C1B3( .G(WWLW11Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR5Wire4), 
					  .Z(RdCol1Line[3]) );
 	    LATNT1 LatR5C0B4( .G(WWLW10Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[4]) );
 	    LATNT1 LatR5C1B4( .G(WWLW11Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR5Wire4), 
					  .Z(RdCol1Line[4]) );
 	    LATNT1 LatR5C0B5( .G(WWLW10Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[5]) );
 	    LATNT1 LatR5C1B5( .G(WWLW11Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR5Wire4), 
					  .Z(RdCol1Line[5]) );
 	    LATNT1 LatR5C0B6( .G(WWLW10Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[6]) );
 	    LATNT1 LatR5C1B6( .G(WWLW11Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR5Wire4), 
					  .Z(RdCol1Line[6]) );
 	    LATNT1 LatR5C0B7( .G(WWLW10Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[7]) );
 	    LATNT1 LatR5C1B7( .G(WWLW11Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR5Wire4), 
					  .Z(RdCol1Line[7]) );
 	    LATNT1 LatR5C0B8( .G(WWLW10Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[8]) );
 	    LATNT1 LatR5C1B8( .G(WWLW11Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR5Wire4), 
					  .Z(RdCol1Line[8]) );
 	    LATNT1 LatR5C0B9( .G(WWLW10Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[9]) );
 	    LATNT1 LatR5C1B9( .G(WWLW11Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR5Wire4), 
					  .Z(RdCol1Line[9]) );
 	    LATNT1 LatR5C0B10( .G(WWLW10Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[10]) );
 	    LATNT1 LatR5C1B10( .G(WWLW11Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR5Wire4), 
					  .Z(RdCol1Line[10]) );
 	    LATNT1 LatR5C0B11( .G(WWLW10Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[11]) );
 	    LATNT1 LatR5C1B11( .G(WWLW11Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR5Wire4), 
					  .Z(RdCol1Line[11]) );
 	    LATNT1 LatR5C0B12( .G(WWLW10Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[12]) );
 	    LATNT1 LatR5C1B12( .G(WWLW11Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR5Wire4), 
					  .Z(RdCol1Line[12]) );
 	    LATNT1 LatR5C0B13( .G(WWLW10Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[13]) );
 	    LATNT1 LatR5C1B13( .G(WWLW11Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR5Wire4), 
					  .Z(RdCol1Line[13]) );
 	    LATNT1 LatR5C0B14( .G(WWLW10Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[14]) );
 	    LATNT1 LatR5C1B14( .G(WWLW11Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR5Wire4), 
					  .Z(RdCol1Line[14]) );
 	    LATNT1 LatR5C0B15( .G(WWLW10Wire1), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol0Line[15]) );
 	    LATNT1 LatR5C1B15( .G(WWLW11Wire1), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR5Wire5), 
					  .Z(RdCol1Line[15]) );
 	    LATNT1 LatR5C0B16( .G(WWLW10Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol0Line[16]) );
 	    LATNT1 LatR5C1B16( .G(WWLW11Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR5Wire5), 
					  .Z(RdCol1Line[16]) );
 	    LATNT1 LatR5C0B17( .G(WWLW10Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol0Line[17]) );
 	    LATNT1 LatR5C1B17( .G(WWLW11Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR5Wire5), 
					  .Z(RdCol1Line[17]) );
 	    LATNT1 LatR5C0B18( .G(WWLW10Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol0Line[18]) );
 	    LATNT1 LatR5C1B18( .G(WWLW11Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR5Wire5), 
					  .Z(RdCol1Line[18]) );
 	    LATNT1 LatR5C0B19( .G(WWLW10Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol0Line[19]) );
 	    LATNT1 LatR5C1B19( .G(WWLW11Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR5Wire5), 
					  .Z(RdCol1Line[19]) );
 	    LATNT1 LatR5C0B20( .G(WWLW10Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol0Line[20]) );
 	    LATNT1 LatR5C1B20( .G(WWLW11Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR5Wire5), 
					  .Z(RdCol1Line[20]) );
 	    LATNT1 LatR5C0B21( .G(WWLW10Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol0Line[21]) );
 	    LATNT1 LatR5C1B21( .G(WWLW11Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR5Wire5), 
					  .Z(RdCol1Line[21]) );
 	    LATNT1 LatR5C0B22( .G(WWLW10Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol0Line[22]) );
 	    LATNT1 LatR5C1B22( .G(WWLW11Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR5Wire5), 
					  .Z(RdCol1Line[22]) );
 	    LATNT1 LatR5C0B23( .G(WWLW10Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol0Line[23]) );
 	    LATNT1 LatR5C1B23( .G(WWLW11Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR5Wire5), 
					  .Z(RdCol1Line[23]) );
 	    LATNT1 LatR5C0B24( .G(WWLW10Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol0Line[24]) );
 	    LATNT1 LatR5C1B24( .G(WWLW11Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR5Wire5), 
					  .Z(RdCol1Line[24]) );
 	    LATNT1 LatR5C0B25( .G(WWLW10Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol0Line[25]) );
 	    LATNT1 LatR5C1B25( .G(WWLW11Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR5Wire5), 
					  .Z(RdCol1Line[25]) );
 	    LATNT1 LatR5C0B26( .G(WWLW10Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol0Line[26]) );
 	    LATNT1 LatR5C1B26( .G(WWLW11Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR5Wire5), 
					  .Z(RdCol1Line[26]) );
 	    LATNT1 LatR5C0B27( .G(WWLW10Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol0Line[27]) );
 	    LATNT1 LatR5C1B27( .G(WWLW11Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR5Wire5), 
					  .Z(RdCol1Line[27]) );
 	    LATNT1 LatR5C0B28( .G(WWLW10Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol0Line[28]) );
 	    LATNT1 LatR5C1B28( .G(WWLW11Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR5Wire5), 
					  .Z(RdCol1Line[28]) );
 	    LATNT1 LatR5C0B29( .G(WWLW10Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol0Line[29]) );
 	    LATNT1 LatR5C1B29( .G(WWLW11Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR5Wire5), 
					  .Z(RdCol1Line[29]) );
 	    LATNT1 LatR5C0B30( .G(WWLW10Wire2), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol0Line[30]) );
 	    LATNT1 LatR5C1B30( .G(WWLW11Wire2), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR5Wire6), 
					  .Z(RdCol1Line[30]) );
 	    LATNT1 LatR5C0B31( .G(WWLW10Wire2), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol0Line[31]) );
 	    LATNT1 LatR5C1B31( .G(WWLW11Wire2), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR5Wire6), 
					  .Z(RdCol1Line[31]) );
 	    LATNT1 LatR5C0B32( .G(WWLW10Wire2), 
					  .D(WrDat[32]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol0Line[32]) );
 	    LATNT1 LatR5C1B32( .G(WWLW11Wire2), 
					  .D(WrDat[32]), 
                           		  .EN(RWLR5Wire6), 
					  .Z(RdCol1Line[32]) );
 	    LATNT1 LatR5C0B33( .G(WWLW10Wire2), 
					  .D(WrDat[33]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol0Line[33]) );
 	    LATNT1 LatR5C1B33( .G(WWLW11Wire2), 
					  .D(WrDat[33]), 
                           		  .EN(RWLR5Wire6), 
					  .Z(RdCol1Line[33]) );
 	    LATNT1 LatR5C0B34( .G(WWLW10Wire2), 
					  .D(WrDat[34]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol0Line[34]) );
 	    LATNT1 LatR5C1B34( .G(WWLW11Wire2), 
					  .D(WrDat[34]), 
                           		  .EN(RWLR5Wire6), 
					  .Z(RdCol1Line[34]) );
 	    LATNT1 LatR5C0B35( .G(WWLW10Wire2), 
					  .D(WrDat[35]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol0Line[35]) );
 	    LATNT1 LatR5C1B35( .G(WWLW11Wire2), 
					  .D(WrDat[35]), 
                           		  .EN(RWLR5Wire6), 
					  .Z(RdCol1Line[35]) );
 	    LATNT1 LatR5C0B36( .G(WWLW10Wire2), 
					  .D(WrDat[36]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol0Line[36]) );
 	    LATNT1 LatR5C1B36( .G(WWLW11Wire2), 
					  .D(WrDat[36]), 
                           		  .EN(RWLR5Wire6), 
					  .Z(RdCol1Line[36]) );
 	    LATNT1 LatR5C0B37( .G(WWLW10Wire2), 
					  .D(WrDat[37]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol0Line[37]) );
 	    LATNT1 LatR5C1B37( .G(WWLW11Wire2), 
					  .D(WrDat[37]), 
                           		  .EN(RWLR5Wire6), 
					  .Z(RdCol1Line[37]) );
 	    LATNT1 LatR5C0B38( .G(WWLW10Wire2), 
					  .D(WrDat[38]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol0Line[38]) );
 	    LATNT1 LatR5C1B38( .G(WWLW11Wire2), 
					  .D(WrDat[38]), 
                           		  .EN(RWLR5Wire6), 
					  .Z(RdCol1Line[38]) );
 	    LATNT1 LatR5C0B39( .G(WWLW10Wire2), 
					  .D(WrDat[39]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol0Line[39]) );
 	    LATNT1 LatR5C1B39( .G(WWLW11Wire2), 
					  .D(WrDat[39]), 
                           		  .EN(RWLR5Wire6), 
					  .Z(RdCol1Line[39]) );
 	    LATNT1 LatR5C0B40( .G(WWLW10Wire2), 
					  .D(WrDat[40]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol0Line[40]) );
 	    LATNT1 LatR5C1B40( .G(WWLW11Wire2), 
					  .D(WrDat[40]), 
                           		  .EN(RWLR5Wire6), 
					  .Z(RdCol1Line[40]) );
 	    LATNT1 LatR5C0B41( .G(WWLW10Wire2), 
					  .D(WrDat[41]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol0Line[41]) );
 	    LATNT1 LatR5C1B41( .G(WWLW11Wire2), 
					  .D(WrDat[41]), 
                           		  .EN(RWLR5Wire6), 
					  .Z(RdCol1Line[41]) );
 	    LATNT1 LatR5C0B42( .G(WWLW10Wire2), 
					  .D(WrDat[42]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol0Line[42]) );
 	    LATNT1 LatR5C1B42( .G(WWLW11Wire2), 
					  .D(WrDat[42]), 
                           		  .EN(RWLR5Wire6), 
					  .Z(RdCol1Line[42]) );
 	    LATNT1 LatR5C0B43( .G(WWLW10Wire2), 
					  .D(WrDat[43]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol0Line[43]) );
 	    LATNT1 LatR5C1B43( .G(WWLW11Wire2), 
					  .D(WrDat[43]), 
                           		  .EN(RWLR5Wire6), 
					  .Z(RdCol1Line[43]) );
 	    LATNT1 LatR5C0B44( .G(WWLW10Wire2), 
					  .D(WrDat[44]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol0Line[44]) );
 	    LATNT1 LatR5C1B44( .G(WWLW11Wire2), 
					  .D(WrDat[44]), 
                           		  .EN(RWLR5Wire6), 
					  .Z(RdCol1Line[44]) );
 	    LATNT1 LatR5C0B45( .G(WWLW10Wire3), 
					  .D(WrDat[45]), 
                           		  .EN(RWLR5Wire3), 
					  .Z(RdCol0Line[45]) );
 	    LATNT1 LatR5C1B45( .G(WWLW11Wire3), 
					  .D(WrDat[45]), 
                           		  .EN(RWLR5Wire7), 
					  .Z(RdCol1Line[45]) );
 	    LATNT1 LatR5C0B46( .G(WWLW10Wire3), 
					  .D(WrDat[46]), 
                           		  .EN(RWLR5Wire3), 
					  .Z(RdCol0Line[46]) );
 	    LATNT1 LatR5C1B46( .G(WWLW11Wire3), 
					  .D(WrDat[46]), 
                           		  .EN(RWLR5Wire7), 
					  .Z(RdCol1Line[46]) );
 	    LATNT1 LatR5C0B47( .G(WWLW10Wire3), 
					  .D(WrDat[47]), 
                           		  .EN(RWLR5Wire3), 
					  .Z(RdCol0Line[47]) );
 	    LATNT1 LatR5C1B47( .G(WWLW11Wire3), 
					  .D(WrDat[47]), 
                           		  .EN(RWLR5Wire7), 
					  .Z(RdCol1Line[47]) );
 	    LATNT1 LatR5C0B48( .G(WWLW10Wire3), 
					  .D(WrDat[48]), 
                           		  .EN(RWLR5Wire3), 
					  .Z(RdCol0Line[48]) );
 	    LATNT1 LatR5C1B48( .G(WWLW11Wire3), 
					  .D(WrDat[48]), 
                           		  .EN(RWLR5Wire7), 
					  .Z(RdCol1Line[48]) );
 	    LATNT1 LatR5C0B49( .G(WWLW10Wire3), 
					  .D(WrDat[49]), 
                           		  .EN(RWLR5Wire3), 
					  .Z(RdCol0Line[49]) );
 	    LATNT1 LatR5C1B49( .G(WWLW11Wire3), 
					  .D(WrDat[49]), 
                           		  .EN(RWLR5Wire7), 
					  .Z(RdCol1Line[49]) );
 	    LATNT1 LatR5C0B50( .G(WWLW10Wire3), 
					  .D(WrDat[50]), 
                           		  .EN(RWLR5Wire3), 
					  .Z(RdCol0Line[50]) );
 	    LATNT1 LatR5C1B50( .G(WWLW11Wire3), 
					  .D(WrDat[50]), 
                           		  .EN(RWLR5Wire7), 
					  .Z(RdCol1Line[50]) );
 	    LATNT1 LatR5C0B51( .G(WWLW10Wire3), 
					  .D(WrDat[51]), 
                           		  .EN(RWLR5Wire3), 
					  .Z(RdCol0Line[51]) );
 	    LATNT1 LatR5C1B51( .G(WWLW11Wire3), 
					  .D(WrDat[51]), 
                           		  .EN(RWLR5Wire7), 
					  .Z(RdCol1Line[51]) );
 	    LATNT1 LatR5C0B52( .G(WWLW10Wire3), 
					  .D(WrDat[52]), 
                           		  .EN(RWLR5Wire3), 
					  .Z(RdCol0Line[52]) );
 	    LATNT1 LatR5C1B52( .G(WWLW11Wire3), 
					  .D(WrDat[52]), 
                           		  .EN(RWLR5Wire7), 
					  .Z(RdCol1Line[52]) );
 	    LATNT1 LatR5C0B53( .G(WWLW10Wire3), 
					  .D(WrDat[53]), 
                           		  .EN(RWLR5Wire3), 
					  .Z(RdCol0Line[53]) );
 	    LATNT1 LatR5C1B53( .G(WWLW11Wire3), 
					  .D(WrDat[53]), 
                           		  .EN(RWLR5Wire7), 
					  .Z(RdCol1Line[53]) );
 	    LATNT1 LatR5C0B54( .G(WWLW10Wire3), 
					  .D(WrDat[54]), 
                           		  .EN(RWLR5Wire3), 
					  .Z(RdCol0Line[54]) );
 	    LATNT1 LatR5C1B54( .G(WWLW11Wire3), 
					  .D(WrDat[54]), 
                           		  .EN(RWLR5Wire7), 
					  .Z(RdCol1Line[54]) );
 	    LATNT1 LatR5C0B55( .G(WWLW10Wire3), 
					  .D(WrDat[55]), 
                           		  .EN(RWLR5Wire3), 
					  .Z(RdCol0Line[55]) );
 	    LATNT1 LatR5C1B55( .G(WWLW11Wire3), 
					  .D(WrDat[55]), 
                           		  .EN(RWLR5Wire7), 
					  .Z(RdCol1Line[55]) );
 	    LATNT1 LatR5C0B56( .G(WWLW10Wire3), 
					  .D(WrDat[56]), 
                           		  .EN(RWLR5Wire3), 
					  .Z(RdCol0Line[56]) );
 	    LATNT1 LatR5C1B56( .G(WWLW11Wire3), 
					  .D(WrDat[56]), 
                           		  .EN(RWLR5Wire7), 
					  .Z(RdCol1Line[56]) );
 	    LATNT1 LatR5C0B57( .G(WWLW10Wire3), 
					  .D(WrDat[57]), 
                           		  .EN(RWLR5Wire3), 
					  .Z(RdCol0Line[57]) );
 	    LATNT1 LatR5C1B57( .G(WWLW11Wire3), 
					  .D(WrDat[57]), 
                           		  .EN(RWLR5Wire7), 
					  .Z(RdCol1Line[57]) );
 	    LATNT1 LatR5C0B58( .G(WWLW10Wire3), 
					  .D(WrDat[58]), 
                           		  .EN(RWLR5Wire3), 
					  .Z(RdCol0Line[58]) );
 	    LATNT1 LatR5C1B58( .G(WWLW11Wire3), 
					  .D(WrDat[58]), 
                           		  .EN(RWLR5Wire7), 
					  .Z(RdCol1Line[58]) );
 	    LATNT1 LatR5C0B59( .G(WWLW10Wire3), 
					  .D(WrDat[59]), 
                           		  .EN(RWLR5Wire3), 
					  .Z(RdCol0Line[59]) );
 	    LATNT1 LatR5C1B59( .G(WWLW11Wire3), 
					  .D(WrDat[59]), 
                           		  .EN(RWLR5Wire7), 
					  .Z(RdCol1Line[59]) );
 	    LATNT1 LatR5C0B60( .G(WWLW10Wire3), 
					  .D(WrDat[60]), 
                           		  .EN(RWLR5Wire3), 
					  .Z(RdCol0Line[60]) );
 	    LATNT1 LatR5C1B60( .G(WWLW11Wire3), 
					  .D(WrDat[60]), 
                           		  .EN(RWLR5Wire7), 
					  .Z(RdCol1Line[60]) );
     
            //
            // Row 6
	    //
	      
 	    LATNT1 LatR6C0B0( .G(WWLW12Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR6C1B0( .G(WWLW13Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR6Wire4), 
					  .Z(RdCol1Line[0]) );
 	    LATNT1 LatR6C0B1( .G(WWLW12Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR6C1B1( .G(WWLW13Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR6Wire4), 
					  .Z(RdCol1Line[1]) );
 	    LATNT1 LatR6C0B2( .G(WWLW12Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR6C1B2( .G(WWLW13Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR6Wire4), 
					  .Z(RdCol1Line[2]) );
 	    LATNT1 LatR6C0B3( .G(WWLW12Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR6C1B3( .G(WWLW13Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR6Wire4), 
					  .Z(RdCol1Line[3]) );
 	    LATNT1 LatR6C0B4( .G(WWLW12Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[4]) );
 	    LATNT1 LatR6C1B4( .G(WWLW13Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR6Wire4), 
					  .Z(RdCol1Line[4]) );
 	    LATNT1 LatR6C0B5( .G(WWLW12Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[5]) );
 	    LATNT1 LatR6C1B5( .G(WWLW13Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR6Wire4), 
					  .Z(RdCol1Line[5]) );
 	    LATNT1 LatR6C0B6( .G(WWLW12Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[6]) );
 	    LATNT1 LatR6C1B6( .G(WWLW13Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR6Wire4), 
					  .Z(RdCol1Line[6]) );
 	    LATNT1 LatR6C0B7( .G(WWLW12Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[7]) );
 	    LATNT1 LatR6C1B7( .G(WWLW13Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR6Wire4), 
					  .Z(RdCol1Line[7]) );
 	    LATNT1 LatR6C0B8( .G(WWLW12Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[8]) );
 	    LATNT1 LatR6C1B8( .G(WWLW13Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR6Wire4), 
					  .Z(RdCol1Line[8]) );
 	    LATNT1 LatR6C0B9( .G(WWLW12Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[9]) );
 	    LATNT1 LatR6C1B9( .G(WWLW13Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR6Wire4), 
					  .Z(RdCol1Line[9]) );
 	    LATNT1 LatR6C0B10( .G(WWLW12Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[10]) );
 	    LATNT1 LatR6C1B10( .G(WWLW13Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR6Wire4), 
					  .Z(RdCol1Line[10]) );
 	    LATNT1 LatR6C0B11( .G(WWLW12Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[11]) );
 	    LATNT1 LatR6C1B11( .G(WWLW13Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR6Wire4), 
					  .Z(RdCol1Line[11]) );
 	    LATNT1 LatR6C0B12( .G(WWLW12Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[12]) );
 	    LATNT1 LatR6C1B12( .G(WWLW13Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR6Wire4), 
					  .Z(RdCol1Line[12]) );
 	    LATNT1 LatR6C0B13( .G(WWLW12Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[13]) );
 	    LATNT1 LatR6C1B13( .G(WWLW13Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR6Wire4), 
					  .Z(RdCol1Line[13]) );
 	    LATNT1 LatR6C0B14( .G(WWLW12Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[14]) );
 	    LATNT1 LatR6C1B14( .G(WWLW13Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR6Wire4), 
					  .Z(RdCol1Line[14]) );
 	    LATNT1 LatR6C0B15( .G(WWLW12Wire1), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR6Wire1), 
					  .Z(RdCol0Line[15]) );
 	    LATNT1 LatR6C1B15( .G(WWLW13Wire1), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR6Wire5), 
					  .Z(RdCol1Line[15]) );
 	    LATNT1 LatR6C0B16( .G(WWLW12Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR6Wire1), 
					  .Z(RdCol0Line[16]) );
 	    LATNT1 LatR6C1B16( .G(WWLW13Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR6Wire5), 
					  .Z(RdCol1Line[16]) );
 	    LATNT1 LatR6C0B17( .G(WWLW12Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR6Wire1), 
					  .Z(RdCol0Line[17]) );
 	    LATNT1 LatR6C1B17( .G(WWLW13Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR6Wire5), 
					  .Z(RdCol1Line[17]) );
 	    LATNT1 LatR6C0B18( .G(WWLW12Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR6Wire1), 
					  .Z(RdCol0Line[18]) );
 	    LATNT1 LatR6C1B18( .G(WWLW13Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR6Wire5), 
					  .Z(RdCol1Line[18]) );
 	    LATNT1 LatR6C0B19( .G(WWLW12Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR6Wire1), 
					  .Z(RdCol0Line[19]) );
 	    LATNT1 LatR6C1B19( .G(WWLW13Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR6Wire5), 
					  .Z(RdCol1Line[19]) );
 	    LATNT1 LatR6C0B20( .G(WWLW12Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR6Wire1), 
					  .Z(RdCol0Line[20]) );
 	    LATNT1 LatR6C1B20( .G(WWLW13Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR6Wire5), 
					  .Z(RdCol1Line[20]) );
 	    LATNT1 LatR6C0B21( .G(WWLW12Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR6Wire1), 
					  .Z(RdCol0Line[21]) );
 	    LATNT1 LatR6C1B21( .G(WWLW13Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR6Wire5), 
					  .Z(RdCol1Line[21]) );
 	    LATNT1 LatR6C0B22( .G(WWLW12Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR6Wire1), 
					  .Z(RdCol0Line[22]) );
 	    LATNT1 LatR6C1B22( .G(WWLW13Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR6Wire5), 
					  .Z(RdCol1Line[22]) );
 	    LATNT1 LatR6C0B23( .G(WWLW12Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR6Wire1), 
					  .Z(RdCol0Line[23]) );
 	    LATNT1 LatR6C1B23( .G(WWLW13Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR6Wire5), 
					  .Z(RdCol1Line[23]) );
 	    LATNT1 LatR6C0B24( .G(WWLW12Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR6Wire1), 
					  .Z(RdCol0Line[24]) );
 	    LATNT1 LatR6C1B24( .G(WWLW13Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR6Wire5), 
					  .Z(RdCol1Line[24]) );
 	    LATNT1 LatR6C0B25( .G(WWLW12Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR6Wire1), 
					  .Z(RdCol0Line[25]) );
 	    LATNT1 LatR6C1B25( .G(WWLW13Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR6Wire5), 
					  .Z(RdCol1Line[25]) );
 	    LATNT1 LatR6C0B26( .G(WWLW12Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR6Wire1), 
					  .Z(RdCol0Line[26]) );
 	    LATNT1 LatR6C1B26( .G(WWLW13Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR6Wire5), 
					  .Z(RdCol1Line[26]) );
 	    LATNT1 LatR6C0B27( .G(WWLW12Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR6Wire1), 
					  .Z(RdCol0Line[27]) );
 	    LATNT1 LatR6C1B27( .G(WWLW13Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR6Wire5), 
					  .Z(RdCol1Line[27]) );
 	    LATNT1 LatR6C0B28( .G(WWLW12Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR6Wire1), 
					  .Z(RdCol0Line[28]) );
 	    LATNT1 LatR6C1B28( .G(WWLW13Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR6Wire5), 
					  .Z(RdCol1Line[28]) );
 	    LATNT1 LatR6C0B29( .G(WWLW12Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR6Wire1), 
					  .Z(RdCol0Line[29]) );
 	    LATNT1 LatR6C1B29( .G(WWLW13Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR6Wire5), 
					  .Z(RdCol1Line[29]) );
 	    LATNT1 LatR6C0B30( .G(WWLW12Wire2), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR6Wire2), 
					  .Z(RdCol0Line[30]) );
 	    LATNT1 LatR6C1B30( .G(WWLW13Wire2), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR6Wire6), 
					  .Z(RdCol1Line[30]) );
 	    LATNT1 LatR6C0B31( .G(WWLW12Wire2), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR6Wire2), 
					  .Z(RdCol0Line[31]) );
 	    LATNT1 LatR6C1B31( .G(WWLW13Wire2), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR6Wire6), 
					  .Z(RdCol1Line[31]) );
 	    LATNT1 LatR6C0B32( .G(WWLW12Wire2), 
					  .D(WrDat[32]), 
                           		  .EN(RWLR6Wire2), 
					  .Z(RdCol0Line[32]) );
 	    LATNT1 LatR6C1B32( .G(WWLW13Wire2), 
					  .D(WrDat[32]), 
                           		  .EN(RWLR6Wire6), 
					  .Z(RdCol1Line[32]) );
 	    LATNT1 LatR6C0B33( .G(WWLW12Wire2), 
					  .D(WrDat[33]), 
                           		  .EN(RWLR6Wire2), 
					  .Z(RdCol0Line[33]) );
 	    LATNT1 LatR6C1B33( .G(WWLW13Wire2), 
					  .D(WrDat[33]), 
                           		  .EN(RWLR6Wire6), 
					  .Z(RdCol1Line[33]) );
 	    LATNT1 LatR6C0B34( .G(WWLW12Wire2), 
					  .D(WrDat[34]), 
                           		  .EN(RWLR6Wire2), 
					  .Z(RdCol0Line[34]) );
 	    LATNT1 LatR6C1B34( .G(WWLW13Wire2), 
					  .D(WrDat[34]), 
                           		  .EN(RWLR6Wire6), 
					  .Z(RdCol1Line[34]) );
 	    LATNT1 LatR6C0B35( .G(WWLW12Wire2), 
					  .D(WrDat[35]), 
                           		  .EN(RWLR6Wire2), 
					  .Z(RdCol0Line[35]) );
 	    LATNT1 LatR6C1B35( .G(WWLW13Wire2), 
					  .D(WrDat[35]), 
                           		  .EN(RWLR6Wire6), 
					  .Z(RdCol1Line[35]) );
 	    LATNT1 LatR6C0B36( .G(WWLW12Wire2), 
					  .D(WrDat[36]), 
                           		  .EN(RWLR6Wire2), 
					  .Z(RdCol0Line[36]) );
 	    LATNT1 LatR6C1B36( .G(WWLW13Wire2), 
					  .D(WrDat[36]), 
                           		  .EN(RWLR6Wire6), 
					  .Z(RdCol1Line[36]) );
 	    LATNT1 LatR6C0B37( .G(WWLW12Wire2), 
					  .D(WrDat[37]), 
                           		  .EN(RWLR6Wire2), 
					  .Z(RdCol0Line[37]) );
 	    LATNT1 LatR6C1B37( .G(WWLW13Wire2), 
					  .D(WrDat[37]), 
                           		  .EN(RWLR6Wire6), 
					  .Z(RdCol1Line[37]) );
 	    LATNT1 LatR6C0B38( .G(WWLW12Wire2), 
					  .D(WrDat[38]), 
                           		  .EN(RWLR6Wire2), 
					  .Z(RdCol0Line[38]) );
 	    LATNT1 LatR6C1B38( .G(WWLW13Wire2), 
					  .D(WrDat[38]), 
                           		  .EN(RWLR6Wire6), 
					  .Z(RdCol1Line[38]) );
 	    LATNT1 LatR6C0B39( .G(WWLW12Wire2), 
					  .D(WrDat[39]), 
                           		  .EN(RWLR6Wire2), 
					  .Z(RdCol0Line[39]) );
 	    LATNT1 LatR6C1B39( .G(WWLW13Wire2), 
					  .D(WrDat[39]), 
                           		  .EN(RWLR6Wire6), 
					  .Z(RdCol1Line[39]) );
 	    LATNT1 LatR6C0B40( .G(WWLW12Wire2), 
					  .D(WrDat[40]), 
                           		  .EN(RWLR6Wire2), 
					  .Z(RdCol0Line[40]) );
 	    LATNT1 LatR6C1B40( .G(WWLW13Wire2), 
					  .D(WrDat[40]), 
                           		  .EN(RWLR6Wire6), 
					  .Z(RdCol1Line[40]) );
 	    LATNT1 LatR6C0B41( .G(WWLW12Wire2), 
					  .D(WrDat[41]), 
                           		  .EN(RWLR6Wire2), 
					  .Z(RdCol0Line[41]) );
 	    LATNT1 LatR6C1B41( .G(WWLW13Wire2), 
					  .D(WrDat[41]), 
                           		  .EN(RWLR6Wire6), 
					  .Z(RdCol1Line[41]) );
 	    LATNT1 LatR6C0B42( .G(WWLW12Wire2), 
					  .D(WrDat[42]), 
                           		  .EN(RWLR6Wire2), 
					  .Z(RdCol0Line[42]) );
 	    LATNT1 LatR6C1B42( .G(WWLW13Wire2), 
					  .D(WrDat[42]), 
                           		  .EN(RWLR6Wire6), 
					  .Z(RdCol1Line[42]) );
 	    LATNT1 LatR6C0B43( .G(WWLW12Wire2), 
					  .D(WrDat[43]), 
                           		  .EN(RWLR6Wire2), 
					  .Z(RdCol0Line[43]) );
 	    LATNT1 LatR6C1B43( .G(WWLW13Wire2), 
					  .D(WrDat[43]), 
                           		  .EN(RWLR6Wire6), 
					  .Z(RdCol1Line[43]) );
 	    LATNT1 LatR6C0B44( .G(WWLW12Wire2), 
					  .D(WrDat[44]), 
                           		  .EN(RWLR6Wire2), 
					  .Z(RdCol0Line[44]) );
 	    LATNT1 LatR6C1B44( .G(WWLW13Wire2), 
					  .D(WrDat[44]), 
                           		  .EN(RWLR6Wire6), 
					  .Z(RdCol1Line[44]) );
 	    LATNT1 LatR6C0B45( .G(WWLW12Wire3), 
					  .D(WrDat[45]), 
                           		  .EN(RWLR6Wire3), 
					  .Z(RdCol0Line[45]) );
 	    LATNT1 LatR6C1B45( .G(WWLW13Wire3), 
					  .D(WrDat[45]), 
                           		  .EN(RWLR6Wire7), 
					  .Z(RdCol1Line[45]) );
 	    LATNT1 LatR6C0B46( .G(WWLW12Wire3), 
					  .D(WrDat[46]), 
                           		  .EN(RWLR6Wire3), 
					  .Z(RdCol0Line[46]) );
 	    LATNT1 LatR6C1B46( .G(WWLW13Wire3), 
					  .D(WrDat[46]), 
                           		  .EN(RWLR6Wire7), 
					  .Z(RdCol1Line[46]) );
 	    LATNT1 LatR6C0B47( .G(WWLW12Wire3), 
					  .D(WrDat[47]), 
                           		  .EN(RWLR6Wire3), 
					  .Z(RdCol0Line[47]) );
 	    LATNT1 LatR6C1B47( .G(WWLW13Wire3), 
					  .D(WrDat[47]), 
                           		  .EN(RWLR6Wire7), 
					  .Z(RdCol1Line[47]) );
 	    LATNT1 LatR6C0B48( .G(WWLW12Wire3), 
					  .D(WrDat[48]), 
                           		  .EN(RWLR6Wire3), 
					  .Z(RdCol0Line[48]) );
 	    LATNT1 LatR6C1B48( .G(WWLW13Wire3), 
					  .D(WrDat[48]), 
                           		  .EN(RWLR6Wire7), 
					  .Z(RdCol1Line[48]) );
 	    LATNT1 LatR6C0B49( .G(WWLW12Wire3), 
					  .D(WrDat[49]), 
                           		  .EN(RWLR6Wire3), 
					  .Z(RdCol0Line[49]) );
 	    LATNT1 LatR6C1B49( .G(WWLW13Wire3), 
					  .D(WrDat[49]), 
                           		  .EN(RWLR6Wire7), 
					  .Z(RdCol1Line[49]) );
 	    LATNT1 LatR6C0B50( .G(WWLW12Wire3), 
					  .D(WrDat[50]), 
                           		  .EN(RWLR6Wire3), 
					  .Z(RdCol0Line[50]) );
 	    LATNT1 LatR6C1B50( .G(WWLW13Wire3), 
					  .D(WrDat[50]), 
                           		  .EN(RWLR6Wire7), 
					  .Z(RdCol1Line[50]) );
 	    LATNT1 LatR6C0B51( .G(WWLW12Wire3), 
					  .D(WrDat[51]), 
                           		  .EN(RWLR6Wire3), 
					  .Z(RdCol0Line[51]) );
 	    LATNT1 LatR6C1B51( .G(WWLW13Wire3), 
					  .D(WrDat[51]), 
                           		  .EN(RWLR6Wire7), 
					  .Z(RdCol1Line[51]) );
 	    LATNT1 LatR6C0B52( .G(WWLW12Wire3), 
					  .D(WrDat[52]), 
                           		  .EN(RWLR6Wire3), 
					  .Z(RdCol0Line[52]) );
 	    LATNT1 LatR6C1B52( .G(WWLW13Wire3), 
					  .D(WrDat[52]), 
                           		  .EN(RWLR6Wire7), 
					  .Z(RdCol1Line[52]) );
 	    LATNT1 LatR6C0B53( .G(WWLW12Wire3), 
					  .D(WrDat[53]), 
                           		  .EN(RWLR6Wire3), 
					  .Z(RdCol0Line[53]) );
 	    LATNT1 LatR6C1B53( .G(WWLW13Wire3), 
					  .D(WrDat[53]), 
                           		  .EN(RWLR6Wire7), 
					  .Z(RdCol1Line[53]) );
 	    LATNT1 LatR6C0B54( .G(WWLW12Wire3), 
					  .D(WrDat[54]), 
                           		  .EN(RWLR6Wire3), 
					  .Z(RdCol0Line[54]) );
 	    LATNT1 LatR6C1B54( .G(WWLW13Wire3), 
					  .D(WrDat[54]), 
                           		  .EN(RWLR6Wire7), 
					  .Z(RdCol1Line[54]) );
 	    LATNT1 LatR6C0B55( .G(WWLW12Wire3), 
					  .D(WrDat[55]), 
                           		  .EN(RWLR6Wire3), 
					  .Z(RdCol0Line[55]) );
 	    LATNT1 LatR6C1B55( .G(WWLW13Wire3), 
					  .D(WrDat[55]), 
                           		  .EN(RWLR6Wire7), 
					  .Z(RdCol1Line[55]) );
 	    LATNT1 LatR6C0B56( .G(WWLW12Wire3), 
					  .D(WrDat[56]), 
                           		  .EN(RWLR6Wire3), 
					  .Z(RdCol0Line[56]) );
 	    LATNT1 LatR6C1B56( .G(WWLW13Wire3), 
					  .D(WrDat[56]), 
                           		  .EN(RWLR6Wire7), 
					  .Z(RdCol1Line[56]) );
 	    LATNT1 LatR6C0B57( .G(WWLW12Wire3), 
					  .D(WrDat[57]), 
                           		  .EN(RWLR6Wire3), 
					  .Z(RdCol0Line[57]) );
 	    LATNT1 LatR6C1B57( .G(WWLW13Wire3), 
					  .D(WrDat[57]), 
                           		  .EN(RWLR6Wire7), 
					  .Z(RdCol1Line[57]) );
 	    LATNT1 LatR6C0B58( .G(WWLW12Wire3), 
					  .D(WrDat[58]), 
                           		  .EN(RWLR6Wire3), 
					  .Z(RdCol0Line[58]) );
 	    LATNT1 LatR6C1B58( .G(WWLW13Wire3), 
					  .D(WrDat[58]), 
                           		  .EN(RWLR6Wire7), 
					  .Z(RdCol1Line[58]) );
 	    LATNT1 LatR6C0B59( .G(WWLW12Wire3), 
					  .D(WrDat[59]), 
                           		  .EN(RWLR6Wire3), 
					  .Z(RdCol0Line[59]) );
 	    LATNT1 LatR6C1B59( .G(WWLW13Wire3), 
					  .D(WrDat[59]), 
                           		  .EN(RWLR6Wire7), 
					  .Z(RdCol1Line[59]) );
 	    LATNT1 LatR6C0B60( .G(WWLW12Wire3), 
					  .D(WrDat[60]), 
                           		  .EN(RWLR6Wire3), 
					  .Z(RdCol0Line[60]) );
 	    LATNT1 LatR6C1B60( .G(WWLW13Wire3), 
					  .D(WrDat[60]), 
                           		  .EN(RWLR6Wire7), 
					  .Z(RdCol1Line[60]) );
     
            //
            // Row 7
	    //
	      
 	    LATNT1 LatR7C0B0( .G(WWLW14Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR7C1B0( .G(WWLW15Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR7Wire4), 
					  .Z(RdCol1Line[0]) );
 	    LATNT1 LatR7C0B1( .G(WWLW14Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR7C1B1( .G(WWLW15Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR7Wire4), 
					  .Z(RdCol1Line[1]) );
 	    LATNT1 LatR7C0B2( .G(WWLW14Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR7C1B2( .G(WWLW15Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR7Wire4), 
					  .Z(RdCol1Line[2]) );
 	    LATNT1 LatR7C0B3( .G(WWLW14Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR7C1B3( .G(WWLW15Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR7Wire4), 
					  .Z(RdCol1Line[3]) );
 	    LATNT1 LatR7C0B4( .G(WWLW14Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[4]) );
 	    LATNT1 LatR7C1B4( .G(WWLW15Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR7Wire4), 
					  .Z(RdCol1Line[4]) );
 	    LATNT1 LatR7C0B5( .G(WWLW14Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[5]) );
 	    LATNT1 LatR7C1B5( .G(WWLW15Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR7Wire4), 
					  .Z(RdCol1Line[5]) );
 	    LATNT1 LatR7C0B6( .G(WWLW14Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[6]) );
 	    LATNT1 LatR7C1B6( .G(WWLW15Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR7Wire4), 
					  .Z(RdCol1Line[6]) );
 	    LATNT1 LatR7C0B7( .G(WWLW14Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[7]) );
 	    LATNT1 LatR7C1B7( .G(WWLW15Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR7Wire4), 
					  .Z(RdCol1Line[7]) );
 	    LATNT1 LatR7C0B8( .G(WWLW14Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[8]) );
 	    LATNT1 LatR7C1B8( .G(WWLW15Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR7Wire4), 
					  .Z(RdCol1Line[8]) );
 	    LATNT1 LatR7C0B9( .G(WWLW14Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[9]) );
 	    LATNT1 LatR7C1B9( .G(WWLW15Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR7Wire4), 
					  .Z(RdCol1Line[9]) );
 	    LATNT1 LatR7C0B10( .G(WWLW14Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[10]) );
 	    LATNT1 LatR7C1B10( .G(WWLW15Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR7Wire4), 
					  .Z(RdCol1Line[10]) );
 	    LATNT1 LatR7C0B11( .G(WWLW14Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[11]) );
 	    LATNT1 LatR7C1B11( .G(WWLW15Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR7Wire4), 
					  .Z(RdCol1Line[11]) );
 	    LATNT1 LatR7C0B12( .G(WWLW14Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[12]) );
 	    LATNT1 LatR7C1B12( .G(WWLW15Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR7Wire4), 
					  .Z(RdCol1Line[12]) );
 	    LATNT1 LatR7C0B13( .G(WWLW14Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[13]) );
 	    LATNT1 LatR7C1B13( .G(WWLW15Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR7Wire4), 
					  .Z(RdCol1Line[13]) );
 	    LATNT1 LatR7C0B14( .G(WWLW14Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[14]) );
 	    LATNT1 LatR7C1B14( .G(WWLW15Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR7Wire4), 
					  .Z(RdCol1Line[14]) );
 	    LATNT1 LatR7C0B15( .G(WWLW14Wire1), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR7Wire1), 
					  .Z(RdCol0Line[15]) );
 	    LATNT1 LatR7C1B15( .G(WWLW15Wire1), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR7Wire5), 
					  .Z(RdCol1Line[15]) );
 	    LATNT1 LatR7C0B16( .G(WWLW14Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR7Wire1), 
					  .Z(RdCol0Line[16]) );
 	    LATNT1 LatR7C1B16( .G(WWLW15Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR7Wire5), 
					  .Z(RdCol1Line[16]) );
 	    LATNT1 LatR7C0B17( .G(WWLW14Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR7Wire1), 
					  .Z(RdCol0Line[17]) );
 	    LATNT1 LatR7C1B17( .G(WWLW15Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR7Wire5), 
					  .Z(RdCol1Line[17]) );
 	    LATNT1 LatR7C0B18( .G(WWLW14Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR7Wire1), 
					  .Z(RdCol0Line[18]) );
 	    LATNT1 LatR7C1B18( .G(WWLW15Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR7Wire5), 
					  .Z(RdCol1Line[18]) );
 	    LATNT1 LatR7C0B19( .G(WWLW14Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR7Wire1), 
					  .Z(RdCol0Line[19]) );
 	    LATNT1 LatR7C1B19( .G(WWLW15Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR7Wire5), 
					  .Z(RdCol1Line[19]) );
 	    LATNT1 LatR7C0B20( .G(WWLW14Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR7Wire1), 
					  .Z(RdCol0Line[20]) );
 	    LATNT1 LatR7C1B20( .G(WWLW15Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR7Wire5), 
					  .Z(RdCol1Line[20]) );
 	    LATNT1 LatR7C0B21( .G(WWLW14Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR7Wire1), 
					  .Z(RdCol0Line[21]) );
 	    LATNT1 LatR7C1B21( .G(WWLW15Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR7Wire5), 
					  .Z(RdCol1Line[21]) );
 	    LATNT1 LatR7C0B22( .G(WWLW14Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR7Wire1), 
					  .Z(RdCol0Line[22]) );
 	    LATNT1 LatR7C1B22( .G(WWLW15Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR7Wire5), 
					  .Z(RdCol1Line[22]) );
 	    LATNT1 LatR7C0B23( .G(WWLW14Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR7Wire1), 
					  .Z(RdCol0Line[23]) );
 	    LATNT1 LatR7C1B23( .G(WWLW15Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR7Wire5), 
					  .Z(RdCol1Line[23]) );
 	    LATNT1 LatR7C0B24( .G(WWLW14Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR7Wire1), 
					  .Z(RdCol0Line[24]) );
 	    LATNT1 LatR7C1B24( .G(WWLW15Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR7Wire5), 
					  .Z(RdCol1Line[24]) );
 	    LATNT1 LatR7C0B25( .G(WWLW14Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR7Wire1), 
					  .Z(RdCol0Line[25]) );
 	    LATNT1 LatR7C1B25( .G(WWLW15Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR7Wire5), 
					  .Z(RdCol1Line[25]) );
 	    LATNT1 LatR7C0B26( .G(WWLW14Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR7Wire1), 
					  .Z(RdCol0Line[26]) );
 	    LATNT1 LatR7C1B26( .G(WWLW15Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR7Wire5), 
					  .Z(RdCol1Line[26]) );
 	    LATNT1 LatR7C0B27( .G(WWLW14Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR7Wire1), 
					  .Z(RdCol0Line[27]) );
 	    LATNT1 LatR7C1B27( .G(WWLW15Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR7Wire5), 
					  .Z(RdCol1Line[27]) );
 	    LATNT1 LatR7C0B28( .G(WWLW14Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR7Wire1), 
					  .Z(RdCol0Line[28]) );
 	    LATNT1 LatR7C1B28( .G(WWLW15Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR7Wire5), 
					  .Z(RdCol1Line[28]) );
 	    LATNT1 LatR7C0B29( .G(WWLW14Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR7Wire1), 
					  .Z(RdCol0Line[29]) );
 	    LATNT1 LatR7C1B29( .G(WWLW15Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR7Wire5), 
					  .Z(RdCol1Line[29]) );
 	    LATNT1 LatR7C0B30( .G(WWLW14Wire2), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR7Wire2), 
					  .Z(RdCol0Line[30]) );
 	    LATNT1 LatR7C1B30( .G(WWLW15Wire2), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR7Wire6), 
					  .Z(RdCol1Line[30]) );
 	    LATNT1 LatR7C0B31( .G(WWLW14Wire2), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR7Wire2), 
					  .Z(RdCol0Line[31]) );
 	    LATNT1 LatR7C1B31( .G(WWLW15Wire2), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR7Wire6), 
					  .Z(RdCol1Line[31]) );
 	    LATNT1 LatR7C0B32( .G(WWLW14Wire2), 
					  .D(WrDat[32]), 
                           		  .EN(RWLR7Wire2), 
					  .Z(RdCol0Line[32]) );
 	    LATNT1 LatR7C1B32( .G(WWLW15Wire2), 
					  .D(WrDat[32]), 
                           		  .EN(RWLR7Wire6), 
					  .Z(RdCol1Line[32]) );
 	    LATNT1 LatR7C0B33( .G(WWLW14Wire2), 
					  .D(WrDat[33]), 
                           		  .EN(RWLR7Wire2), 
					  .Z(RdCol0Line[33]) );
 	    LATNT1 LatR7C1B33( .G(WWLW15Wire2), 
					  .D(WrDat[33]), 
                           		  .EN(RWLR7Wire6), 
					  .Z(RdCol1Line[33]) );
 	    LATNT1 LatR7C0B34( .G(WWLW14Wire2), 
					  .D(WrDat[34]), 
                           		  .EN(RWLR7Wire2), 
					  .Z(RdCol0Line[34]) );
 	    LATNT1 LatR7C1B34( .G(WWLW15Wire2), 
					  .D(WrDat[34]), 
                           		  .EN(RWLR7Wire6), 
					  .Z(RdCol1Line[34]) );
 	    LATNT1 LatR7C0B35( .G(WWLW14Wire2), 
					  .D(WrDat[35]), 
                           		  .EN(RWLR7Wire2), 
					  .Z(RdCol0Line[35]) );
 	    LATNT1 LatR7C1B35( .G(WWLW15Wire2), 
					  .D(WrDat[35]), 
                           		  .EN(RWLR7Wire6), 
					  .Z(RdCol1Line[35]) );
 	    LATNT1 LatR7C0B36( .G(WWLW14Wire2), 
					  .D(WrDat[36]), 
                           		  .EN(RWLR7Wire2), 
					  .Z(RdCol0Line[36]) );
 	    LATNT1 LatR7C1B36( .G(WWLW15Wire2), 
					  .D(WrDat[36]), 
                           		  .EN(RWLR7Wire6), 
					  .Z(RdCol1Line[36]) );
 	    LATNT1 LatR7C0B37( .G(WWLW14Wire2), 
					  .D(WrDat[37]), 
                           		  .EN(RWLR7Wire2), 
					  .Z(RdCol0Line[37]) );
 	    LATNT1 LatR7C1B37( .G(WWLW15Wire2), 
					  .D(WrDat[37]), 
                           		  .EN(RWLR7Wire6), 
					  .Z(RdCol1Line[37]) );
 	    LATNT1 LatR7C0B38( .G(WWLW14Wire2), 
					  .D(WrDat[38]), 
                           		  .EN(RWLR7Wire2), 
					  .Z(RdCol0Line[38]) );
 	    LATNT1 LatR7C1B38( .G(WWLW15Wire2), 
					  .D(WrDat[38]), 
                           		  .EN(RWLR7Wire6), 
					  .Z(RdCol1Line[38]) );
 	    LATNT1 LatR7C0B39( .G(WWLW14Wire2), 
					  .D(WrDat[39]), 
                           		  .EN(RWLR7Wire2), 
					  .Z(RdCol0Line[39]) );
 	    LATNT1 LatR7C1B39( .G(WWLW15Wire2), 
					  .D(WrDat[39]), 
                           		  .EN(RWLR7Wire6), 
					  .Z(RdCol1Line[39]) );
 	    LATNT1 LatR7C0B40( .G(WWLW14Wire2), 
					  .D(WrDat[40]), 
                           		  .EN(RWLR7Wire2), 
					  .Z(RdCol0Line[40]) );
 	    LATNT1 LatR7C1B40( .G(WWLW15Wire2), 
					  .D(WrDat[40]), 
                           		  .EN(RWLR7Wire6), 
					  .Z(RdCol1Line[40]) );
 	    LATNT1 LatR7C0B41( .G(WWLW14Wire2), 
					  .D(WrDat[41]), 
                           		  .EN(RWLR7Wire2), 
					  .Z(RdCol0Line[41]) );
 	    LATNT1 LatR7C1B41( .G(WWLW15Wire2), 
					  .D(WrDat[41]), 
                           		  .EN(RWLR7Wire6), 
					  .Z(RdCol1Line[41]) );
 	    LATNT1 LatR7C0B42( .G(WWLW14Wire2), 
					  .D(WrDat[42]), 
                           		  .EN(RWLR7Wire2), 
					  .Z(RdCol0Line[42]) );
 	    LATNT1 LatR7C1B42( .G(WWLW15Wire2), 
					  .D(WrDat[42]), 
                           		  .EN(RWLR7Wire6), 
					  .Z(RdCol1Line[42]) );
 	    LATNT1 LatR7C0B43( .G(WWLW14Wire2), 
					  .D(WrDat[43]), 
                           		  .EN(RWLR7Wire2), 
					  .Z(RdCol0Line[43]) );
 	    LATNT1 LatR7C1B43( .G(WWLW15Wire2), 
					  .D(WrDat[43]), 
                           		  .EN(RWLR7Wire6), 
					  .Z(RdCol1Line[43]) );
 	    LATNT1 LatR7C0B44( .G(WWLW14Wire2), 
					  .D(WrDat[44]), 
                           		  .EN(RWLR7Wire2), 
					  .Z(RdCol0Line[44]) );
 	    LATNT1 LatR7C1B44( .G(WWLW15Wire2), 
					  .D(WrDat[44]), 
                           		  .EN(RWLR7Wire6), 
					  .Z(RdCol1Line[44]) );
 	    LATNT1 LatR7C0B45( .G(WWLW14Wire3), 
					  .D(WrDat[45]), 
                           		  .EN(RWLR7Wire3), 
					  .Z(RdCol0Line[45]) );
 	    LATNT1 LatR7C1B45( .G(WWLW15Wire3), 
					  .D(WrDat[45]), 
                           		  .EN(RWLR7Wire7), 
					  .Z(RdCol1Line[45]) );
 	    LATNT1 LatR7C0B46( .G(WWLW14Wire3), 
					  .D(WrDat[46]), 
                           		  .EN(RWLR7Wire3), 
					  .Z(RdCol0Line[46]) );
 	    LATNT1 LatR7C1B46( .G(WWLW15Wire3), 
					  .D(WrDat[46]), 
                           		  .EN(RWLR7Wire7), 
					  .Z(RdCol1Line[46]) );
 	    LATNT1 LatR7C0B47( .G(WWLW14Wire3), 
					  .D(WrDat[47]), 
                           		  .EN(RWLR7Wire3), 
					  .Z(RdCol0Line[47]) );
 	    LATNT1 LatR7C1B47( .G(WWLW15Wire3), 
					  .D(WrDat[47]), 
                           		  .EN(RWLR7Wire7), 
					  .Z(RdCol1Line[47]) );
 	    LATNT1 LatR7C0B48( .G(WWLW14Wire3), 
					  .D(WrDat[48]), 
                           		  .EN(RWLR7Wire3), 
					  .Z(RdCol0Line[48]) );
 	    LATNT1 LatR7C1B48( .G(WWLW15Wire3), 
					  .D(WrDat[48]), 
                           		  .EN(RWLR7Wire7), 
					  .Z(RdCol1Line[48]) );
 	    LATNT1 LatR7C0B49( .G(WWLW14Wire3), 
					  .D(WrDat[49]), 
                           		  .EN(RWLR7Wire3), 
					  .Z(RdCol0Line[49]) );
 	    LATNT1 LatR7C1B49( .G(WWLW15Wire3), 
					  .D(WrDat[49]), 
                           		  .EN(RWLR7Wire7), 
					  .Z(RdCol1Line[49]) );
 	    LATNT1 LatR7C0B50( .G(WWLW14Wire3), 
					  .D(WrDat[50]), 
                           		  .EN(RWLR7Wire3), 
					  .Z(RdCol0Line[50]) );
 	    LATNT1 LatR7C1B50( .G(WWLW15Wire3), 
					  .D(WrDat[50]), 
                           		  .EN(RWLR7Wire7), 
					  .Z(RdCol1Line[50]) );
 	    LATNT1 LatR7C0B51( .G(WWLW14Wire3), 
					  .D(WrDat[51]), 
                           		  .EN(RWLR7Wire3), 
					  .Z(RdCol0Line[51]) );
 	    LATNT1 LatR7C1B51( .G(WWLW15Wire3), 
					  .D(WrDat[51]), 
                           		  .EN(RWLR7Wire7), 
					  .Z(RdCol1Line[51]) );
 	    LATNT1 LatR7C0B52( .G(WWLW14Wire3), 
					  .D(WrDat[52]), 
                           		  .EN(RWLR7Wire3), 
					  .Z(RdCol0Line[52]) );
 	    LATNT1 LatR7C1B52( .G(WWLW15Wire3), 
					  .D(WrDat[52]), 
                           		  .EN(RWLR7Wire7), 
					  .Z(RdCol1Line[52]) );
 	    LATNT1 LatR7C0B53( .G(WWLW14Wire3), 
					  .D(WrDat[53]), 
                           		  .EN(RWLR7Wire3), 
					  .Z(RdCol0Line[53]) );
 	    LATNT1 LatR7C1B53( .G(WWLW15Wire3), 
					  .D(WrDat[53]), 
                           		  .EN(RWLR7Wire7), 
					  .Z(RdCol1Line[53]) );
 	    LATNT1 LatR7C0B54( .G(WWLW14Wire3), 
					  .D(WrDat[54]), 
                           		  .EN(RWLR7Wire3), 
					  .Z(RdCol0Line[54]) );
 	    LATNT1 LatR7C1B54( .G(WWLW15Wire3), 
					  .D(WrDat[54]), 
                           		  .EN(RWLR7Wire7), 
					  .Z(RdCol1Line[54]) );
 	    LATNT1 LatR7C0B55( .G(WWLW14Wire3), 
					  .D(WrDat[55]), 
                           		  .EN(RWLR7Wire3), 
					  .Z(RdCol0Line[55]) );
 	    LATNT1 LatR7C1B55( .G(WWLW15Wire3), 
					  .D(WrDat[55]), 
                           		  .EN(RWLR7Wire7), 
					  .Z(RdCol1Line[55]) );
 	    LATNT1 LatR7C0B56( .G(WWLW14Wire3), 
					  .D(WrDat[56]), 
                           		  .EN(RWLR7Wire3), 
					  .Z(RdCol0Line[56]) );
 	    LATNT1 LatR7C1B56( .G(WWLW15Wire3), 
					  .D(WrDat[56]), 
                           		  .EN(RWLR7Wire7), 
					  .Z(RdCol1Line[56]) );
 	    LATNT1 LatR7C0B57( .G(WWLW14Wire3), 
					  .D(WrDat[57]), 
                           		  .EN(RWLR7Wire3), 
					  .Z(RdCol0Line[57]) );
 	    LATNT1 LatR7C1B57( .G(WWLW15Wire3), 
					  .D(WrDat[57]), 
                           		  .EN(RWLR7Wire7), 
					  .Z(RdCol1Line[57]) );
 	    LATNT1 LatR7C0B58( .G(WWLW14Wire3), 
					  .D(WrDat[58]), 
                           		  .EN(RWLR7Wire3), 
					  .Z(RdCol0Line[58]) );
 	    LATNT1 LatR7C1B58( .G(WWLW15Wire3), 
					  .D(WrDat[58]), 
                           		  .EN(RWLR7Wire7), 
					  .Z(RdCol1Line[58]) );
 	    LATNT1 LatR7C0B59( .G(WWLW14Wire3), 
					  .D(WrDat[59]), 
                           		  .EN(RWLR7Wire3), 
					  .Z(RdCol0Line[59]) );
 	    LATNT1 LatR7C1B59( .G(WWLW15Wire3), 
					  .D(WrDat[59]), 
                           		  .EN(RWLR7Wire7), 
					  .Z(RdCol1Line[59]) );
 	    LATNT1 LatR7C0B60( .G(WWLW14Wire3), 
					  .D(WrDat[60]), 
                           		  .EN(RWLR7Wire3), 
					  .Z(RdCol0Line[60]) );
 	    LATNT1 LatR7C1B60( .G(WWLW15Wire3), 
					  .D(WrDat[60]), 
                           		  .EN(RWLR7Wire7), 
					  .Z(RdCol1Line[60]) );

//
//
// the psuedo sense amp (and column muxing if necessary)
         wire [3:0] ColSelectBuf;
         BUFD7 mxbuf0(.A(ColSelect), .Z(ColSelectBuf[0]) );
         BUFD7 mxbuf1(.A(ColSelect), .Z(ColSelectBuf[1]) );
         BUFD7 mxbuf2(.A(ColSelect), .Z(ColSelectBuf[2]) );
         BUFD7 mxbuf3(.A(ColSelect), .Z(ColSelectBuf[3]) );
	 MUX2D2 rd0mx(.A0(RdCol0Line[0]), .A1(RdCol1Line[0]),
           .SL(ColSelectBuf[0]), .Z(RdDat[0]) );
	 MUX2D2 rd1mx(.A0(RdCol0Line[1]), .A1(RdCol1Line[1]),
           .SL(ColSelectBuf[1]), .Z(RdDat[1]) );
	 MUX2D2 rd2mx(.A0(RdCol0Line[2]), .A1(RdCol1Line[2]),
           .SL(ColSelectBuf[2]), .Z(RdDat[2]) );
	 MUX2D2 rd3mx(.A0(RdCol0Line[3]), .A1(RdCol1Line[3]),
           .SL(ColSelectBuf[3]), .Z(RdDat[3]) );
	 MUX2D2 rd4mx(.A0(RdCol0Line[4]), .A1(RdCol1Line[4]),
           .SL(ColSelectBuf[0]), .Z(RdDat[4]) );
	 MUX2D2 rd5mx(.A0(RdCol0Line[5]), .A1(RdCol1Line[5]),
           .SL(ColSelectBuf[1]), .Z(RdDat[5]) );
	 MUX2D2 rd6mx(.A0(RdCol0Line[6]), .A1(RdCol1Line[6]),
           .SL(ColSelectBuf[2]), .Z(RdDat[6]) );
	 MUX2D2 rd7mx(.A0(RdCol0Line[7]), .A1(RdCol1Line[7]),
           .SL(ColSelectBuf[3]), .Z(RdDat[7]) );
	 MUX2D2 rd8mx(.A0(RdCol0Line[8]), .A1(RdCol1Line[8]),
           .SL(ColSelectBuf[0]), .Z(RdDat[8]) );
	 MUX2D2 rd9mx(.A0(RdCol0Line[9]), .A1(RdCol1Line[9]),
           .SL(ColSelectBuf[1]), .Z(RdDat[9]) );
	 MUX2D2 rd10mx(.A0(RdCol0Line[10]), .A1(RdCol1Line[10]),
           .SL(ColSelectBuf[2]), .Z(RdDat[10]) );
	 MUX2D2 rd11mx(.A0(RdCol0Line[11]), .A1(RdCol1Line[11]),
           .SL(ColSelectBuf[3]), .Z(RdDat[11]) );
	 MUX2D2 rd12mx(.A0(RdCol0Line[12]), .A1(RdCol1Line[12]),
           .SL(ColSelectBuf[0]), .Z(RdDat[12]) );
	 MUX2D2 rd13mx(.A0(RdCol0Line[13]), .A1(RdCol1Line[13]),
           .SL(ColSelectBuf[1]), .Z(RdDat[13]) );
	 MUX2D2 rd14mx(.A0(RdCol0Line[14]), .A1(RdCol1Line[14]),
           .SL(ColSelectBuf[2]), .Z(RdDat[14]) );
	 MUX2D2 rd15mx(.A0(RdCol0Line[15]), .A1(RdCol1Line[15]),
           .SL(ColSelectBuf[3]), .Z(RdDat[15]) );
	 MUX2D2 rd16mx(.A0(RdCol0Line[16]), .A1(RdCol1Line[16]),
           .SL(ColSelectBuf[0]), .Z(RdDat[16]) );
	 MUX2D2 rd17mx(.A0(RdCol0Line[17]), .A1(RdCol1Line[17]),
           .SL(ColSelectBuf[1]), .Z(RdDat[17]) );
	 MUX2D2 rd18mx(.A0(RdCol0Line[18]), .A1(RdCol1Line[18]),
           .SL(ColSelectBuf[2]), .Z(RdDat[18]) );
	 MUX2D2 rd19mx(.A0(RdCol0Line[19]), .A1(RdCol1Line[19]),
           .SL(ColSelectBuf[3]), .Z(RdDat[19]) );
	 MUX2D2 rd20mx(.A0(RdCol0Line[20]), .A1(RdCol1Line[20]),
           .SL(ColSelectBuf[0]), .Z(RdDat[20]) );
	 MUX2D2 rd21mx(.A0(RdCol0Line[21]), .A1(RdCol1Line[21]),
           .SL(ColSelectBuf[1]), .Z(RdDat[21]) );
	 MUX2D2 rd22mx(.A0(RdCol0Line[22]), .A1(RdCol1Line[22]),
           .SL(ColSelectBuf[2]), .Z(RdDat[22]) );
	 MUX2D2 rd23mx(.A0(RdCol0Line[23]), .A1(RdCol1Line[23]),
           .SL(ColSelectBuf[3]), .Z(RdDat[23]) );
	 MUX2D2 rd24mx(.A0(RdCol0Line[24]), .A1(RdCol1Line[24]),
           .SL(ColSelectBuf[0]), .Z(RdDat[24]) );
	 MUX2D2 rd25mx(.A0(RdCol0Line[25]), .A1(RdCol1Line[25]),
           .SL(ColSelectBuf[1]), .Z(RdDat[25]) );
	 MUX2D2 rd26mx(.A0(RdCol0Line[26]), .A1(RdCol1Line[26]),
           .SL(ColSelectBuf[2]), .Z(RdDat[26]) );
	 MUX2D2 rd27mx(.A0(RdCol0Line[27]), .A1(RdCol1Line[27]),
           .SL(ColSelectBuf[3]), .Z(RdDat[27]) );
	 MUX2D2 rd28mx(.A0(RdCol0Line[28]), .A1(RdCol1Line[28]),
           .SL(ColSelectBuf[0]), .Z(RdDat[28]) );
	 MUX2D2 rd29mx(.A0(RdCol0Line[29]), .A1(RdCol1Line[29]),
           .SL(ColSelectBuf[1]), .Z(RdDat[29]) );
	 MUX2D2 rd30mx(.A0(RdCol0Line[30]), .A1(RdCol1Line[30]),
           .SL(ColSelectBuf[2]), .Z(RdDat[30]) );
	 MUX2D2 rd31mx(.A0(RdCol0Line[31]), .A1(RdCol1Line[31]),
           .SL(ColSelectBuf[3]), .Z(RdDat[31]) );
	 MUX2D2 rd32mx(.A0(RdCol0Line[32]), .A1(RdCol1Line[32]),
           .SL(ColSelectBuf[0]), .Z(RdDat[32]) );
	 MUX2D2 rd33mx(.A0(RdCol0Line[33]), .A1(RdCol1Line[33]),
           .SL(ColSelectBuf[1]), .Z(RdDat[33]) );
	 MUX2D2 rd34mx(.A0(RdCol0Line[34]), .A1(RdCol1Line[34]),
           .SL(ColSelectBuf[2]), .Z(RdDat[34]) );
	 MUX2D2 rd35mx(.A0(RdCol0Line[35]), .A1(RdCol1Line[35]),
           .SL(ColSelectBuf[3]), .Z(RdDat[35]) );
	 MUX2D2 rd36mx(.A0(RdCol0Line[36]), .A1(RdCol1Line[36]),
           .SL(ColSelectBuf[0]), .Z(RdDat[36]) );
	 MUX2D2 rd37mx(.A0(RdCol0Line[37]), .A1(RdCol1Line[37]),
           .SL(ColSelectBuf[1]), .Z(RdDat[37]) );
	 MUX2D2 rd38mx(.A0(RdCol0Line[38]), .A1(RdCol1Line[38]),
           .SL(ColSelectBuf[2]), .Z(RdDat[38]) );
	 MUX2D2 rd39mx(.A0(RdCol0Line[39]), .A1(RdCol1Line[39]),
           .SL(ColSelectBuf[3]), .Z(RdDat[39]) );
	 MUX2D2 rd40mx(.A0(RdCol0Line[40]), .A1(RdCol1Line[40]),
           .SL(ColSelectBuf[0]), .Z(RdDat[40]) );
	 MUX2D2 rd41mx(.A0(RdCol0Line[41]), .A1(RdCol1Line[41]),
           .SL(ColSelectBuf[1]), .Z(RdDat[41]) );
	 MUX2D2 rd42mx(.A0(RdCol0Line[42]), .A1(RdCol1Line[42]),
           .SL(ColSelectBuf[2]), .Z(RdDat[42]) );
	 MUX2D2 rd43mx(.A0(RdCol0Line[43]), .A1(RdCol1Line[43]),
           .SL(ColSelectBuf[3]), .Z(RdDat[43]) );
	 MUX2D2 rd44mx(.A0(RdCol0Line[44]), .A1(RdCol1Line[44]),
           .SL(ColSelectBuf[0]), .Z(RdDat[44]) );
	 MUX2D2 rd45mx(.A0(RdCol0Line[45]), .A1(RdCol1Line[45]),
           .SL(ColSelectBuf[1]), .Z(RdDat[45]) );
	 MUX2D2 rd46mx(.A0(RdCol0Line[46]), .A1(RdCol1Line[46]),
           .SL(ColSelectBuf[2]), .Z(RdDat[46]) );
	 MUX2D2 rd47mx(.A0(RdCol0Line[47]), .A1(RdCol1Line[47]),
           .SL(ColSelectBuf[3]), .Z(RdDat[47]) );
	 MUX2D2 rd48mx(.A0(RdCol0Line[48]), .A1(RdCol1Line[48]),
           .SL(ColSelectBuf[0]), .Z(RdDat[48]) );
	 MUX2D2 rd49mx(.A0(RdCol0Line[49]), .A1(RdCol1Line[49]),
           .SL(ColSelectBuf[1]), .Z(RdDat[49]) );
	 MUX2D2 rd50mx(.A0(RdCol0Line[50]), .A1(RdCol1Line[50]),
           .SL(ColSelectBuf[2]), .Z(RdDat[50]) );
	 MUX2D2 rd51mx(.A0(RdCol0Line[51]), .A1(RdCol1Line[51]),
           .SL(ColSelectBuf[3]), .Z(RdDat[51]) );
	 MUX2D2 rd52mx(.A0(RdCol0Line[52]), .A1(RdCol1Line[52]),
           .SL(ColSelectBuf[0]), .Z(RdDat[52]) );
	 MUX2D2 rd53mx(.A0(RdCol0Line[53]), .A1(RdCol1Line[53]),
           .SL(ColSelectBuf[1]), .Z(RdDat[53]) );
	 MUX2D2 rd54mx(.A0(RdCol0Line[54]), .A1(RdCol1Line[54]),
           .SL(ColSelectBuf[2]), .Z(RdDat[54]) );
	 MUX2D2 rd55mx(.A0(RdCol0Line[55]), .A1(RdCol1Line[55]),
           .SL(ColSelectBuf[3]), .Z(RdDat[55]) );
	 MUX2D2 rd56mx(.A0(RdCol0Line[56]), .A1(RdCol1Line[56]),
           .SL(ColSelectBuf[0]), .Z(RdDat[56]) );
	 MUX2D2 rd57mx(.A0(RdCol0Line[57]), .A1(RdCol1Line[57]),
           .SL(ColSelectBuf[1]), .Z(RdDat[57]) );
	 MUX2D2 rd58mx(.A0(RdCol0Line[58]), .A1(RdCol1Line[58]),
           .SL(ColSelectBuf[2]), .Z(RdDat[58]) );
	 MUX2D2 rd59mx(.A0(RdCol0Line[59]), .A1(RdCol1Line[59]),
           .SL(ColSelectBuf[3]), .Z(RdDat[59]) );
	 MUX2D2 rd60mx(.A0(RdCol0Line[60]), .A1(RdCol1Line[60]),
           .SL(ColSelectBuf[0]), .Z(RdDat[60]) );

   endmodule

