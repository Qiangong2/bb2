// This file was automatically derived from "FIFO4x64.vpp"
// using vpp on Mon May 29 03:08:32 2000.


    // this is the FIFO4x64 module's declaration.
    // model the ram to be capable of punching the write data to the 
    // read port, when writing and reading to same address.
// syn myclk = PushClk


module FIFO4x64 (
	PushClk,
	PopClk,
	ScanMode,
	PushRst,
	PopRst,
	e1Push,
	Pop,
	e1DatIn,
	DatOut);

       input PushClk;
       input PopClk;
       input ScanMode;
       input PushRst, PopRst, e1Push, Pop;   
       input [63:0] e1DatIn;
       output [63:0] DatOut;

// Begin vpp generated register declarations...
	reg [63:0] DatIn;
	reg Push;
	reg [1:0] RdAdr;
// ...end vpp generated register declarations.


//
// Read Functionality...
//
// input address registers
   
   wire [1:0] gRdAdr =  ~{ 2{PopRst} } & RdAdr[1:0] ;
   wire [1:0] incRdAdr = gRdAdr[1:0] + 2'h1 ;
always @(posedge PopClk) RdAdr[1:0] <= ( Pop ? incRdAdr[1:0] : gRdAdr[1:0] ) ;

// 
   wire [1:0] RowAdr;
   assign RowAdr[1:0] = RdAdr[1:0];

   wire [3:0] RWL, RWLB;
   assign RWL[3:0] = 4'h1 << RowAdr;

  macro_invda RWLB_0 (.Y(RWLB[0]), .A(RWL[0]));
  macro_invda RWLB_1 (.Y(RWLB[1]), .A(RWL[1]));
  macro_invda RWLB_2 (.Y(RWLB[2]), .A(RWL[2]));
  macro_invda RWLB_3 (.Y(RWLB[3]), .A(RWL[3]));

   wire ScanMode_buf;

   artx_buffer1 ScanModeBuf ( .A(ScanMode), .Y(ScanMode_buf) );
//
// Write Functionality...
//
//  write address decoder happens in previous cycle
//
always @(posedge PushClk) Push <= e1Push;

   reg [3:0]  e1DecWrAdr;
   wire [3:0] DecWrAdr;

   always @(DecWrAdr or PushRst or Push) begin
      casex ({PushRst,Push})
         2'b00 : e1DecWrAdr[3:0] = DecWrAdr[3:0];
         2'b01 : e1DecWrAdr[3:0] = {DecWrAdr[2:0], DecWrAdr[3]} ;      
         2'b1x : e1DecWrAdr = 4'h1;
      default:
	 e1DecWrAdr = 4'hx;
      endcase
   end

// write enable

   wire [3:0]WWL;

   macro_latwen latwen_0 (.wen(WE_0), .WrEn(WrEn_0), .e1WrEn(e1Push), .clk(PushClk));
   macro_latwl latwl_0 (.wl(WWL[0]), .DecWrAdr(DecWrAdr[0]), .e1DecWrAdr(e1DecWrAdr[0]), .wen(WE_0), .clk(PushClk), .ScanMode(ScanMode_buf));
   macro_latwl latwl_1 (.wl(WWL[1]), .DecWrAdr(DecWrAdr[1]), .e1DecWrAdr(e1DecWrAdr[1]), .wen(WE_0), .clk(PushClk), .ScanMode(ScanMode_buf));
   macro_latwl latwl_2 (.wl(WWL[2]), .DecWrAdr(DecWrAdr[2]), .e1DecWrAdr(e1DecWrAdr[2]), .wen(WE_0), .clk(PushClk), .ScanMode(ScanMode_buf));
   macro_latwl latwl_3 (.wl(WWL[3]), .DecWrAdr(DecWrAdr[3]), .e1DecWrAdr(e1DecWrAdr[3]), .wen(WE_0), .clk(PushClk), .ScanMode(ScanMode_buf));

// keep it backward compatible
wire WrEn = WrEn_0;

// input data register
always @(posedge PushClk) DatIn[63:0] <= e1DatIn;

//
// this instantiates a Latch Array Submodule
// which includes any column muxing
//


   LArr4x64 LArr4x64 (
	.NullClk(),
	.WWL(WWL[3:0]),
	.WrDat(DatIn[63:0]),
	.RWL(RWLB[3:0]),
	.RdDat(DatOut[63:0])
   );
   endmodule

