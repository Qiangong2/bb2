// This file was automatically derived from "LArr4x64.vpp"
// using vpp on Thu May 18 23:07:02 2000.



// syn myclk = NullClk


module LArr4x64 (
	NullClk,
	WWL,
	WrDat,
	RWL,
	RdDat);

	input NullClk;
	input [3:0]WWL;
	input [63:0] WrDat;
	input [3:0]RWL;
	output [63:0] RdDat;


       wire [63:0] RdCol0Line;

  
            //
            // ReadLineBuffers
            //
	      
	    INVDA RWLR0Buf0 ( .A ( RWL[0] ), .Z ( RWLR0Wire0 ) );
	    INVDA RWLR0Buf1 ( .A ( RWL[0] ), .Z ( RWLR0Wire1 ) );
	    INVDA RWLR0Buf2 ( .A ( RWL[0] ), .Z ( RWLR0Wire2 ) );
	    INVDA RWLR0Buf3 ( .A ( RWL[0] ), .Z ( RWLR0Wire3 ) );
	    INVDA RWLR1Buf0 ( .A ( RWL[1] ), .Z ( RWLR1Wire0 ) );
	    INVDA RWLR1Buf1 ( .A ( RWL[1] ), .Z ( RWLR1Wire1 ) );
	    INVDA RWLR1Buf2 ( .A ( RWL[1] ), .Z ( RWLR1Wire2 ) );
	    INVDA RWLR1Buf3 ( .A ( RWL[1] ), .Z ( RWLR1Wire3 ) );
	    INVDA RWLR2Buf0 ( .A ( RWL[2] ), .Z ( RWLR2Wire0 ) );
	    INVDA RWLR2Buf1 ( .A ( RWL[2] ), .Z ( RWLR2Wire1 ) );
	    INVDA RWLR2Buf2 ( .A ( RWL[2] ), .Z ( RWLR2Wire2 ) );
	    INVDA RWLR2Buf3 ( .A ( RWL[2] ), .Z ( RWLR2Wire3 ) );
	    INVDA RWLR3Buf0 ( .A ( RWL[3] ), .Z ( RWLR3Wire0 ) );
	    INVDA RWLR3Buf1 ( .A ( RWL[3] ), .Z ( RWLR3Wire1 ) );
	    INVDA RWLR3Buf2 ( .A ( RWL[3] ), .Z ( RWLR3Wire2 ) );
	    INVDA RWLR3Buf3 ( .A ( RWL[3] ), .Z ( RWLR3Wire3 ) );
  
            //
            // WriteLineBuffers
            //
	      
	    INVDA WWLW0Buf0 ( .A ( WWL[0] ), .Z ( WWLW0Wire0 ) );
	    INVDA WWLW0Buf1 ( .A ( WWL[0] ), .Z ( WWLW0Wire1 ) );
	    INVDA WWLW0Buf2 ( .A ( WWL[0] ), .Z ( WWLW0Wire2 ) );
	    INVDA WWLW0Buf3 ( .A ( WWL[0] ), .Z ( WWLW0Wire3 ) );
	    INVDA WWLW1Buf0 ( .A ( WWL[1] ), .Z ( WWLW1Wire0 ) );
	    INVDA WWLW1Buf1 ( .A ( WWL[1] ), .Z ( WWLW1Wire1 ) );
	    INVDA WWLW1Buf2 ( .A ( WWL[1] ), .Z ( WWLW1Wire2 ) );
	    INVDA WWLW1Buf3 ( .A ( WWL[1] ), .Z ( WWLW1Wire3 ) );
	    INVDA WWLW2Buf0 ( .A ( WWL[2] ), .Z ( WWLW2Wire0 ) );
	    INVDA WWLW2Buf1 ( .A ( WWL[2] ), .Z ( WWLW2Wire1 ) );
	    INVDA WWLW2Buf2 ( .A ( WWL[2] ), .Z ( WWLW2Wire2 ) );
	    INVDA WWLW2Buf3 ( .A ( WWL[2] ), .Z ( WWLW2Wire3 ) );
	    INVDA WWLW3Buf0 ( .A ( WWL[3] ), .Z ( WWLW3Wire0 ) );
	    INVDA WWLW3Buf1 ( .A ( WWL[3] ), .Z ( WWLW3Wire1 ) );
	    INVDA WWLW3Buf2 ( .A ( WWL[3] ), .Z ( WWLW3Wire2 ) );
	    INVDA WWLW3Buf3 ( .A ( WWL[3] ), .Z ( WWLW3Wire3 ) );

//
//
// this is the array of latches itself.
     
            //
            // Row 0
	    //
	      
 	    LATNT1 LatR0C0B0( .G(WWLW0Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR0C0B1( .G(WWLW0Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR0C0B2( .G(WWLW0Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR0C0B3( .G(WWLW0Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR0C0B4( .G(WWLW0Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[4]) );
 	    LATNT1 LatR0C0B5( .G(WWLW0Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[5]) );
 	    LATNT1 LatR0C0B6( .G(WWLW0Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[6]) );
 	    LATNT1 LatR0C0B7( .G(WWLW0Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[7]) );
 	    LATNT1 LatR0C0B8( .G(WWLW0Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[8]) );
 	    LATNT1 LatR0C0B9( .G(WWLW0Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[9]) );
 	    LATNT1 LatR0C0B10( .G(WWLW0Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[10]) );
 	    LATNT1 LatR0C0B11( .G(WWLW0Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[11]) );
 	    LATNT1 LatR0C0B12( .G(WWLW0Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[12]) );
 	    LATNT1 LatR0C0B13( .G(WWLW0Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[13]) );
 	    LATNT1 LatR0C0B14( .G(WWLW0Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[14]) );
 	    LATNT1 LatR0C0B15( .G(WWLW0Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[15]) );
 	    LATNT1 LatR0C0B16( .G(WWLW0Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[16]) );
 	    LATNT1 LatR0C0B17( .G(WWLW0Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[17]) );
 	    LATNT1 LatR0C0B18( .G(WWLW0Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[18]) );
 	    LATNT1 LatR0C0B19( .G(WWLW0Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[19]) );
 	    LATNT1 LatR0C0B20( .G(WWLW0Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[20]) );
 	    LATNT1 LatR0C0B21( .G(WWLW0Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[21]) );
 	    LATNT1 LatR0C0B22( .G(WWLW0Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[22]) );
 	    LATNT1 LatR0C0B23( .G(WWLW0Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[23]) );
 	    LATNT1 LatR0C0B24( .G(WWLW0Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[24]) );
 	    LATNT1 LatR0C0B25( .G(WWLW0Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[25]) );
 	    LATNT1 LatR0C0B26( .G(WWLW0Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[26]) );
 	    LATNT1 LatR0C0B27( .G(WWLW0Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[27]) );
 	    LATNT1 LatR0C0B28( .G(WWLW0Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[28]) );
 	    LATNT1 LatR0C0B29( .G(WWLW0Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[29]) );
 	    LATNT1 LatR0C0B30( .G(WWLW0Wire1), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[30]) );
 	    LATNT1 LatR0C0B31( .G(WWLW0Wire1), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[31]) );
 	    LATNT1 LatR0C0B32( .G(WWLW0Wire2), 
					  .D(WrDat[32]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol0Line[32]) );
 	    LATNT1 LatR0C0B33( .G(WWLW0Wire2), 
					  .D(WrDat[33]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol0Line[33]) );
 	    LATNT1 LatR0C0B34( .G(WWLW0Wire2), 
					  .D(WrDat[34]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol0Line[34]) );
 	    LATNT1 LatR0C0B35( .G(WWLW0Wire2), 
					  .D(WrDat[35]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol0Line[35]) );
 	    LATNT1 LatR0C0B36( .G(WWLW0Wire2), 
					  .D(WrDat[36]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol0Line[36]) );
 	    LATNT1 LatR0C0B37( .G(WWLW0Wire2), 
					  .D(WrDat[37]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol0Line[37]) );
 	    LATNT1 LatR0C0B38( .G(WWLW0Wire2), 
					  .D(WrDat[38]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol0Line[38]) );
 	    LATNT1 LatR0C0B39( .G(WWLW0Wire2), 
					  .D(WrDat[39]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol0Line[39]) );
 	    LATNT1 LatR0C0B40( .G(WWLW0Wire2), 
					  .D(WrDat[40]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol0Line[40]) );
 	    LATNT1 LatR0C0B41( .G(WWLW0Wire2), 
					  .D(WrDat[41]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol0Line[41]) );
 	    LATNT1 LatR0C0B42( .G(WWLW0Wire2), 
					  .D(WrDat[42]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol0Line[42]) );
 	    LATNT1 LatR0C0B43( .G(WWLW0Wire2), 
					  .D(WrDat[43]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol0Line[43]) );
 	    LATNT1 LatR0C0B44( .G(WWLW0Wire2), 
					  .D(WrDat[44]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol0Line[44]) );
 	    LATNT1 LatR0C0B45( .G(WWLW0Wire2), 
					  .D(WrDat[45]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol0Line[45]) );
 	    LATNT1 LatR0C0B46( .G(WWLW0Wire2), 
					  .D(WrDat[46]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol0Line[46]) );
 	    LATNT1 LatR0C0B47( .G(WWLW0Wire2), 
					  .D(WrDat[47]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol0Line[47]) );
 	    LATNT1 LatR0C0B48( .G(WWLW0Wire3), 
					  .D(WrDat[48]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol0Line[48]) );
 	    LATNT1 LatR0C0B49( .G(WWLW0Wire3), 
					  .D(WrDat[49]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol0Line[49]) );
 	    LATNT1 LatR0C0B50( .G(WWLW0Wire3), 
					  .D(WrDat[50]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol0Line[50]) );
 	    LATNT1 LatR0C0B51( .G(WWLW0Wire3), 
					  .D(WrDat[51]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol0Line[51]) );
 	    LATNT1 LatR0C0B52( .G(WWLW0Wire3), 
					  .D(WrDat[52]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol0Line[52]) );
 	    LATNT1 LatR0C0B53( .G(WWLW0Wire3), 
					  .D(WrDat[53]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol0Line[53]) );
 	    LATNT1 LatR0C0B54( .G(WWLW0Wire3), 
					  .D(WrDat[54]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol0Line[54]) );
 	    LATNT1 LatR0C0B55( .G(WWLW0Wire3), 
					  .D(WrDat[55]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol0Line[55]) );
 	    LATNT1 LatR0C0B56( .G(WWLW0Wire3), 
					  .D(WrDat[56]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol0Line[56]) );
 	    LATNT1 LatR0C0B57( .G(WWLW0Wire3), 
					  .D(WrDat[57]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol0Line[57]) );
 	    LATNT1 LatR0C0B58( .G(WWLW0Wire3), 
					  .D(WrDat[58]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol0Line[58]) );
 	    LATNT1 LatR0C0B59( .G(WWLW0Wire3), 
					  .D(WrDat[59]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol0Line[59]) );
 	    LATNT1 LatR0C0B60( .G(WWLW0Wire3), 
					  .D(WrDat[60]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol0Line[60]) );
 	    LATNT1 LatR0C0B61( .G(WWLW0Wire3), 
					  .D(WrDat[61]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol0Line[61]) );
 	    LATNT1 LatR0C0B62( .G(WWLW0Wire3), 
					  .D(WrDat[62]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol0Line[62]) );
 	    LATNT1 LatR0C0B63( .G(WWLW0Wire3), 
					  .D(WrDat[63]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol0Line[63]) );
     
            //
            // Row 1
	    //
	      
 	    LATNT1 LatR1C0B0( .G(WWLW1Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR1C0B1( .G(WWLW1Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR1C0B2( .G(WWLW1Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR1C0B3( .G(WWLW1Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR1C0B4( .G(WWLW1Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[4]) );
 	    LATNT1 LatR1C0B5( .G(WWLW1Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[5]) );
 	    LATNT1 LatR1C0B6( .G(WWLW1Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[6]) );
 	    LATNT1 LatR1C0B7( .G(WWLW1Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[7]) );
 	    LATNT1 LatR1C0B8( .G(WWLW1Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[8]) );
 	    LATNT1 LatR1C0B9( .G(WWLW1Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[9]) );
 	    LATNT1 LatR1C0B10( .G(WWLW1Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[10]) );
 	    LATNT1 LatR1C0B11( .G(WWLW1Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[11]) );
 	    LATNT1 LatR1C0B12( .G(WWLW1Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[12]) );
 	    LATNT1 LatR1C0B13( .G(WWLW1Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[13]) );
 	    LATNT1 LatR1C0B14( .G(WWLW1Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[14]) );
 	    LATNT1 LatR1C0B15( .G(WWLW1Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[15]) );
 	    LATNT1 LatR1C0B16( .G(WWLW1Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[16]) );
 	    LATNT1 LatR1C0B17( .G(WWLW1Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[17]) );
 	    LATNT1 LatR1C0B18( .G(WWLW1Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[18]) );
 	    LATNT1 LatR1C0B19( .G(WWLW1Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[19]) );
 	    LATNT1 LatR1C0B20( .G(WWLW1Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[20]) );
 	    LATNT1 LatR1C0B21( .G(WWLW1Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[21]) );
 	    LATNT1 LatR1C0B22( .G(WWLW1Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[22]) );
 	    LATNT1 LatR1C0B23( .G(WWLW1Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[23]) );
 	    LATNT1 LatR1C0B24( .G(WWLW1Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[24]) );
 	    LATNT1 LatR1C0B25( .G(WWLW1Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[25]) );
 	    LATNT1 LatR1C0B26( .G(WWLW1Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[26]) );
 	    LATNT1 LatR1C0B27( .G(WWLW1Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[27]) );
 	    LATNT1 LatR1C0B28( .G(WWLW1Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[28]) );
 	    LATNT1 LatR1C0B29( .G(WWLW1Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[29]) );
 	    LATNT1 LatR1C0B30( .G(WWLW1Wire1), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[30]) );
 	    LATNT1 LatR1C0B31( .G(WWLW1Wire1), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[31]) );
 	    LATNT1 LatR1C0B32( .G(WWLW1Wire2), 
					  .D(WrDat[32]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol0Line[32]) );
 	    LATNT1 LatR1C0B33( .G(WWLW1Wire2), 
					  .D(WrDat[33]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol0Line[33]) );
 	    LATNT1 LatR1C0B34( .G(WWLW1Wire2), 
					  .D(WrDat[34]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol0Line[34]) );
 	    LATNT1 LatR1C0B35( .G(WWLW1Wire2), 
					  .D(WrDat[35]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol0Line[35]) );
 	    LATNT1 LatR1C0B36( .G(WWLW1Wire2), 
					  .D(WrDat[36]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol0Line[36]) );
 	    LATNT1 LatR1C0B37( .G(WWLW1Wire2), 
					  .D(WrDat[37]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol0Line[37]) );
 	    LATNT1 LatR1C0B38( .G(WWLW1Wire2), 
					  .D(WrDat[38]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol0Line[38]) );
 	    LATNT1 LatR1C0B39( .G(WWLW1Wire2), 
					  .D(WrDat[39]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol0Line[39]) );
 	    LATNT1 LatR1C0B40( .G(WWLW1Wire2), 
					  .D(WrDat[40]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol0Line[40]) );
 	    LATNT1 LatR1C0B41( .G(WWLW1Wire2), 
					  .D(WrDat[41]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol0Line[41]) );
 	    LATNT1 LatR1C0B42( .G(WWLW1Wire2), 
					  .D(WrDat[42]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol0Line[42]) );
 	    LATNT1 LatR1C0B43( .G(WWLW1Wire2), 
					  .D(WrDat[43]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol0Line[43]) );
 	    LATNT1 LatR1C0B44( .G(WWLW1Wire2), 
					  .D(WrDat[44]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol0Line[44]) );
 	    LATNT1 LatR1C0B45( .G(WWLW1Wire2), 
					  .D(WrDat[45]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol0Line[45]) );
 	    LATNT1 LatR1C0B46( .G(WWLW1Wire2), 
					  .D(WrDat[46]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol0Line[46]) );
 	    LATNT1 LatR1C0B47( .G(WWLW1Wire2), 
					  .D(WrDat[47]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol0Line[47]) );
 	    LATNT1 LatR1C0B48( .G(WWLW1Wire3), 
					  .D(WrDat[48]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol0Line[48]) );
 	    LATNT1 LatR1C0B49( .G(WWLW1Wire3), 
					  .D(WrDat[49]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol0Line[49]) );
 	    LATNT1 LatR1C0B50( .G(WWLW1Wire3), 
					  .D(WrDat[50]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol0Line[50]) );
 	    LATNT1 LatR1C0B51( .G(WWLW1Wire3), 
					  .D(WrDat[51]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol0Line[51]) );
 	    LATNT1 LatR1C0B52( .G(WWLW1Wire3), 
					  .D(WrDat[52]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol0Line[52]) );
 	    LATNT1 LatR1C0B53( .G(WWLW1Wire3), 
					  .D(WrDat[53]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol0Line[53]) );
 	    LATNT1 LatR1C0B54( .G(WWLW1Wire3), 
					  .D(WrDat[54]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol0Line[54]) );
 	    LATNT1 LatR1C0B55( .G(WWLW1Wire3), 
					  .D(WrDat[55]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol0Line[55]) );
 	    LATNT1 LatR1C0B56( .G(WWLW1Wire3), 
					  .D(WrDat[56]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol0Line[56]) );
 	    LATNT1 LatR1C0B57( .G(WWLW1Wire3), 
					  .D(WrDat[57]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol0Line[57]) );
 	    LATNT1 LatR1C0B58( .G(WWLW1Wire3), 
					  .D(WrDat[58]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol0Line[58]) );
 	    LATNT1 LatR1C0B59( .G(WWLW1Wire3), 
					  .D(WrDat[59]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol0Line[59]) );
 	    LATNT1 LatR1C0B60( .G(WWLW1Wire3), 
					  .D(WrDat[60]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol0Line[60]) );
 	    LATNT1 LatR1C0B61( .G(WWLW1Wire3), 
					  .D(WrDat[61]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol0Line[61]) );
 	    LATNT1 LatR1C0B62( .G(WWLW1Wire3), 
					  .D(WrDat[62]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol0Line[62]) );
 	    LATNT1 LatR1C0B63( .G(WWLW1Wire3), 
					  .D(WrDat[63]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol0Line[63]) );
     
            //
            // Row 2
	    //
	      
 	    LATNT1 LatR2C0B0( .G(WWLW2Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR2C0B1( .G(WWLW2Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR2C0B2( .G(WWLW2Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR2C0B3( .G(WWLW2Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR2C0B4( .G(WWLW2Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[4]) );
 	    LATNT1 LatR2C0B5( .G(WWLW2Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[5]) );
 	    LATNT1 LatR2C0B6( .G(WWLW2Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[6]) );
 	    LATNT1 LatR2C0B7( .G(WWLW2Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[7]) );
 	    LATNT1 LatR2C0B8( .G(WWLW2Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[8]) );
 	    LATNT1 LatR2C0B9( .G(WWLW2Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[9]) );
 	    LATNT1 LatR2C0B10( .G(WWLW2Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[10]) );
 	    LATNT1 LatR2C0B11( .G(WWLW2Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[11]) );
 	    LATNT1 LatR2C0B12( .G(WWLW2Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[12]) );
 	    LATNT1 LatR2C0B13( .G(WWLW2Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[13]) );
 	    LATNT1 LatR2C0B14( .G(WWLW2Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[14]) );
 	    LATNT1 LatR2C0B15( .G(WWLW2Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[15]) );
 	    LATNT1 LatR2C0B16( .G(WWLW2Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[16]) );
 	    LATNT1 LatR2C0B17( .G(WWLW2Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[17]) );
 	    LATNT1 LatR2C0B18( .G(WWLW2Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[18]) );
 	    LATNT1 LatR2C0B19( .G(WWLW2Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[19]) );
 	    LATNT1 LatR2C0B20( .G(WWLW2Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[20]) );
 	    LATNT1 LatR2C0B21( .G(WWLW2Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[21]) );
 	    LATNT1 LatR2C0B22( .G(WWLW2Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[22]) );
 	    LATNT1 LatR2C0B23( .G(WWLW2Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[23]) );
 	    LATNT1 LatR2C0B24( .G(WWLW2Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[24]) );
 	    LATNT1 LatR2C0B25( .G(WWLW2Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[25]) );
 	    LATNT1 LatR2C0B26( .G(WWLW2Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[26]) );
 	    LATNT1 LatR2C0B27( .G(WWLW2Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[27]) );
 	    LATNT1 LatR2C0B28( .G(WWLW2Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[28]) );
 	    LATNT1 LatR2C0B29( .G(WWLW2Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[29]) );
 	    LATNT1 LatR2C0B30( .G(WWLW2Wire1), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[30]) );
 	    LATNT1 LatR2C0B31( .G(WWLW2Wire1), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[31]) );
 	    LATNT1 LatR2C0B32( .G(WWLW2Wire2), 
					  .D(WrDat[32]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol0Line[32]) );
 	    LATNT1 LatR2C0B33( .G(WWLW2Wire2), 
					  .D(WrDat[33]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol0Line[33]) );
 	    LATNT1 LatR2C0B34( .G(WWLW2Wire2), 
					  .D(WrDat[34]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol0Line[34]) );
 	    LATNT1 LatR2C0B35( .G(WWLW2Wire2), 
					  .D(WrDat[35]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol0Line[35]) );
 	    LATNT1 LatR2C0B36( .G(WWLW2Wire2), 
					  .D(WrDat[36]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol0Line[36]) );
 	    LATNT1 LatR2C0B37( .G(WWLW2Wire2), 
					  .D(WrDat[37]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol0Line[37]) );
 	    LATNT1 LatR2C0B38( .G(WWLW2Wire2), 
					  .D(WrDat[38]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol0Line[38]) );
 	    LATNT1 LatR2C0B39( .G(WWLW2Wire2), 
					  .D(WrDat[39]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol0Line[39]) );
 	    LATNT1 LatR2C0B40( .G(WWLW2Wire2), 
					  .D(WrDat[40]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol0Line[40]) );
 	    LATNT1 LatR2C0B41( .G(WWLW2Wire2), 
					  .D(WrDat[41]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol0Line[41]) );
 	    LATNT1 LatR2C0B42( .G(WWLW2Wire2), 
					  .D(WrDat[42]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol0Line[42]) );
 	    LATNT1 LatR2C0B43( .G(WWLW2Wire2), 
					  .D(WrDat[43]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol0Line[43]) );
 	    LATNT1 LatR2C0B44( .G(WWLW2Wire2), 
					  .D(WrDat[44]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol0Line[44]) );
 	    LATNT1 LatR2C0B45( .G(WWLW2Wire2), 
					  .D(WrDat[45]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol0Line[45]) );
 	    LATNT1 LatR2C0B46( .G(WWLW2Wire2), 
					  .D(WrDat[46]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol0Line[46]) );
 	    LATNT1 LatR2C0B47( .G(WWLW2Wire2), 
					  .D(WrDat[47]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol0Line[47]) );
 	    LATNT1 LatR2C0B48( .G(WWLW2Wire3), 
					  .D(WrDat[48]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol0Line[48]) );
 	    LATNT1 LatR2C0B49( .G(WWLW2Wire3), 
					  .D(WrDat[49]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol0Line[49]) );
 	    LATNT1 LatR2C0B50( .G(WWLW2Wire3), 
					  .D(WrDat[50]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol0Line[50]) );
 	    LATNT1 LatR2C0B51( .G(WWLW2Wire3), 
					  .D(WrDat[51]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol0Line[51]) );
 	    LATNT1 LatR2C0B52( .G(WWLW2Wire3), 
					  .D(WrDat[52]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol0Line[52]) );
 	    LATNT1 LatR2C0B53( .G(WWLW2Wire3), 
					  .D(WrDat[53]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol0Line[53]) );
 	    LATNT1 LatR2C0B54( .G(WWLW2Wire3), 
					  .D(WrDat[54]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol0Line[54]) );
 	    LATNT1 LatR2C0B55( .G(WWLW2Wire3), 
					  .D(WrDat[55]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol0Line[55]) );
 	    LATNT1 LatR2C0B56( .G(WWLW2Wire3), 
					  .D(WrDat[56]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol0Line[56]) );
 	    LATNT1 LatR2C0B57( .G(WWLW2Wire3), 
					  .D(WrDat[57]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol0Line[57]) );
 	    LATNT1 LatR2C0B58( .G(WWLW2Wire3), 
					  .D(WrDat[58]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol0Line[58]) );
 	    LATNT1 LatR2C0B59( .G(WWLW2Wire3), 
					  .D(WrDat[59]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol0Line[59]) );
 	    LATNT1 LatR2C0B60( .G(WWLW2Wire3), 
					  .D(WrDat[60]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol0Line[60]) );
 	    LATNT1 LatR2C0B61( .G(WWLW2Wire3), 
					  .D(WrDat[61]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol0Line[61]) );
 	    LATNT1 LatR2C0B62( .G(WWLW2Wire3), 
					  .D(WrDat[62]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol0Line[62]) );
 	    LATNT1 LatR2C0B63( .G(WWLW2Wire3), 
					  .D(WrDat[63]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol0Line[63]) );
     
            //
            // Row 3
	    //
	      
 	    LATNT1 LatR3C0B0( .G(WWLW3Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR3C0B1( .G(WWLW3Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR3C0B2( .G(WWLW3Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR3C0B3( .G(WWLW3Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR3C0B4( .G(WWLW3Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[4]) );
 	    LATNT1 LatR3C0B5( .G(WWLW3Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[5]) );
 	    LATNT1 LatR3C0B6( .G(WWLW3Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[6]) );
 	    LATNT1 LatR3C0B7( .G(WWLW3Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[7]) );
 	    LATNT1 LatR3C0B8( .G(WWLW3Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[8]) );
 	    LATNT1 LatR3C0B9( .G(WWLW3Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[9]) );
 	    LATNT1 LatR3C0B10( .G(WWLW3Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[10]) );
 	    LATNT1 LatR3C0B11( .G(WWLW3Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[11]) );
 	    LATNT1 LatR3C0B12( .G(WWLW3Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[12]) );
 	    LATNT1 LatR3C0B13( .G(WWLW3Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[13]) );
 	    LATNT1 LatR3C0B14( .G(WWLW3Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[14]) );
 	    LATNT1 LatR3C0B15( .G(WWLW3Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[15]) );
 	    LATNT1 LatR3C0B16( .G(WWLW3Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[16]) );
 	    LATNT1 LatR3C0B17( .G(WWLW3Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[17]) );
 	    LATNT1 LatR3C0B18( .G(WWLW3Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[18]) );
 	    LATNT1 LatR3C0B19( .G(WWLW3Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[19]) );
 	    LATNT1 LatR3C0B20( .G(WWLW3Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[20]) );
 	    LATNT1 LatR3C0B21( .G(WWLW3Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[21]) );
 	    LATNT1 LatR3C0B22( .G(WWLW3Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[22]) );
 	    LATNT1 LatR3C0B23( .G(WWLW3Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[23]) );
 	    LATNT1 LatR3C0B24( .G(WWLW3Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[24]) );
 	    LATNT1 LatR3C0B25( .G(WWLW3Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[25]) );
 	    LATNT1 LatR3C0B26( .G(WWLW3Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[26]) );
 	    LATNT1 LatR3C0B27( .G(WWLW3Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[27]) );
 	    LATNT1 LatR3C0B28( .G(WWLW3Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[28]) );
 	    LATNT1 LatR3C0B29( .G(WWLW3Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[29]) );
 	    LATNT1 LatR3C0B30( .G(WWLW3Wire1), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[30]) );
 	    LATNT1 LatR3C0B31( .G(WWLW3Wire1), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[31]) );
 	    LATNT1 LatR3C0B32( .G(WWLW3Wire2), 
					  .D(WrDat[32]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol0Line[32]) );
 	    LATNT1 LatR3C0B33( .G(WWLW3Wire2), 
					  .D(WrDat[33]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol0Line[33]) );
 	    LATNT1 LatR3C0B34( .G(WWLW3Wire2), 
					  .D(WrDat[34]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol0Line[34]) );
 	    LATNT1 LatR3C0B35( .G(WWLW3Wire2), 
					  .D(WrDat[35]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol0Line[35]) );
 	    LATNT1 LatR3C0B36( .G(WWLW3Wire2), 
					  .D(WrDat[36]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol0Line[36]) );
 	    LATNT1 LatR3C0B37( .G(WWLW3Wire2), 
					  .D(WrDat[37]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol0Line[37]) );
 	    LATNT1 LatR3C0B38( .G(WWLW3Wire2), 
					  .D(WrDat[38]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol0Line[38]) );
 	    LATNT1 LatR3C0B39( .G(WWLW3Wire2), 
					  .D(WrDat[39]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol0Line[39]) );
 	    LATNT1 LatR3C0B40( .G(WWLW3Wire2), 
					  .D(WrDat[40]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol0Line[40]) );
 	    LATNT1 LatR3C0B41( .G(WWLW3Wire2), 
					  .D(WrDat[41]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol0Line[41]) );
 	    LATNT1 LatR3C0B42( .G(WWLW3Wire2), 
					  .D(WrDat[42]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol0Line[42]) );
 	    LATNT1 LatR3C0B43( .G(WWLW3Wire2), 
					  .D(WrDat[43]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol0Line[43]) );
 	    LATNT1 LatR3C0B44( .G(WWLW3Wire2), 
					  .D(WrDat[44]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol0Line[44]) );
 	    LATNT1 LatR3C0B45( .G(WWLW3Wire2), 
					  .D(WrDat[45]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol0Line[45]) );
 	    LATNT1 LatR3C0B46( .G(WWLW3Wire2), 
					  .D(WrDat[46]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol0Line[46]) );
 	    LATNT1 LatR3C0B47( .G(WWLW3Wire2), 
					  .D(WrDat[47]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol0Line[47]) );
 	    LATNT1 LatR3C0B48( .G(WWLW3Wire3), 
					  .D(WrDat[48]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol0Line[48]) );
 	    LATNT1 LatR3C0B49( .G(WWLW3Wire3), 
					  .D(WrDat[49]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol0Line[49]) );
 	    LATNT1 LatR3C0B50( .G(WWLW3Wire3), 
					  .D(WrDat[50]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol0Line[50]) );
 	    LATNT1 LatR3C0B51( .G(WWLW3Wire3), 
					  .D(WrDat[51]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol0Line[51]) );
 	    LATNT1 LatR3C0B52( .G(WWLW3Wire3), 
					  .D(WrDat[52]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol0Line[52]) );
 	    LATNT1 LatR3C0B53( .G(WWLW3Wire3), 
					  .D(WrDat[53]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol0Line[53]) );
 	    LATNT1 LatR3C0B54( .G(WWLW3Wire3), 
					  .D(WrDat[54]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol0Line[54]) );
 	    LATNT1 LatR3C0B55( .G(WWLW3Wire3), 
					  .D(WrDat[55]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol0Line[55]) );
 	    LATNT1 LatR3C0B56( .G(WWLW3Wire3), 
					  .D(WrDat[56]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol0Line[56]) );
 	    LATNT1 LatR3C0B57( .G(WWLW3Wire3), 
					  .D(WrDat[57]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol0Line[57]) );
 	    LATNT1 LatR3C0B58( .G(WWLW3Wire3), 
					  .D(WrDat[58]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol0Line[58]) );
 	    LATNT1 LatR3C0B59( .G(WWLW3Wire3), 
					  .D(WrDat[59]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol0Line[59]) );
 	    LATNT1 LatR3C0B60( .G(WWLW3Wire3), 
					  .D(WrDat[60]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol0Line[60]) );
 	    LATNT1 LatR3C0B61( .G(WWLW3Wire3), 
					  .D(WrDat[61]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol0Line[61]) );
 	    LATNT1 LatR3C0B62( .G(WWLW3Wire3), 
					  .D(WrDat[62]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol0Line[62]) );
 	    LATNT1 LatR3C0B63( .G(WWLW3Wire3), 
					  .D(WrDat[63]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol0Line[63]) );

//
//
// the psuedo sense amp (and column muxing if necessary)
         BUFD3 rd0buf(.A(RdCol0Line[0]), .Z(RdDat[0]) );
         BUFD3 rd1buf(.A(RdCol0Line[1]), .Z(RdDat[1]) );
         BUFD3 rd2buf(.A(RdCol0Line[2]), .Z(RdDat[2]) );
         BUFD3 rd3buf(.A(RdCol0Line[3]), .Z(RdDat[3]) );
         BUFD3 rd4buf(.A(RdCol0Line[4]), .Z(RdDat[4]) );
         BUFD3 rd5buf(.A(RdCol0Line[5]), .Z(RdDat[5]) );
         BUFD3 rd6buf(.A(RdCol0Line[6]), .Z(RdDat[6]) );
         BUFD3 rd7buf(.A(RdCol0Line[7]), .Z(RdDat[7]) );
         BUFD3 rd8buf(.A(RdCol0Line[8]), .Z(RdDat[8]) );
         BUFD3 rd9buf(.A(RdCol0Line[9]), .Z(RdDat[9]) );
         BUFD3 rd10buf(.A(RdCol0Line[10]), .Z(RdDat[10]) );
         BUFD3 rd11buf(.A(RdCol0Line[11]), .Z(RdDat[11]) );
         BUFD3 rd12buf(.A(RdCol0Line[12]), .Z(RdDat[12]) );
         BUFD3 rd13buf(.A(RdCol0Line[13]), .Z(RdDat[13]) );
         BUFD3 rd14buf(.A(RdCol0Line[14]), .Z(RdDat[14]) );
         BUFD3 rd15buf(.A(RdCol0Line[15]), .Z(RdDat[15]) );
         BUFD3 rd16buf(.A(RdCol0Line[16]), .Z(RdDat[16]) );
         BUFD3 rd17buf(.A(RdCol0Line[17]), .Z(RdDat[17]) );
         BUFD3 rd18buf(.A(RdCol0Line[18]), .Z(RdDat[18]) );
         BUFD3 rd19buf(.A(RdCol0Line[19]), .Z(RdDat[19]) );
         BUFD3 rd20buf(.A(RdCol0Line[20]), .Z(RdDat[20]) );
         BUFD3 rd21buf(.A(RdCol0Line[21]), .Z(RdDat[21]) );
         BUFD3 rd22buf(.A(RdCol0Line[22]), .Z(RdDat[22]) );
         BUFD3 rd23buf(.A(RdCol0Line[23]), .Z(RdDat[23]) );
         BUFD3 rd24buf(.A(RdCol0Line[24]), .Z(RdDat[24]) );
         BUFD3 rd25buf(.A(RdCol0Line[25]), .Z(RdDat[25]) );
         BUFD3 rd26buf(.A(RdCol0Line[26]), .Z(RdDat[26]) );
         BUFD3 rd27buf(.A(RdCol0Line[27]), .Z(RdDat[27]) );
         BUFD3 rd28buf(.A(RdCol0Line[28]), .Z(RdDat[28]) );
         BUFD3 rd29buf(.A(RdCol0Line[29]), .Z(RdDat[29]) );
         BUFD3 rd30buf(.A(RdCol0Line[30]), .Z(RdDat[30]) );
         BUFD3 rd31buf(.A(RdCol0Line[31]), .Z(RdDat[31]) );
         BUFD3 rd32buf(.A(RdCol0Line[32]), .Z(RdDat[32]) );
         BUFD3 rd33buf(.A(RdCol0Line[33]), .Z(RdDat[33]) );
         BUFD3 rd34buf(.A(RdCol0Line[34]), .Z(RdDat[34]) );
         BUFD3 rd35buf(.A(RdCol0Line[35]), .Z(RdDat[35]) );
         BUFD3 rd36buf(.A(RdCol0Line[36]), .Z(RdDat[36]) );
         BUFD3 rd37buf(.A(RdCol0Line[37]), .Z(RdDat[37]) );
         BUFD3 rd38buf(.A(RdCol0Line[38]), .Z(RdDat[38]) );
         BUFD3 rd39buf(.A(RdCol0Line[39]), .Z(RdDat[39]) );
         BUFD3 rd40buf(.A(RdCol0Line[40]), .Z(RdDat[40]) );
         BUFD3 rd41buf(.A(RdCol0Line[41]), .Z(RdDat[41]) );
         BUFD3 rd42buf(.A(RdCol0Line[42]), .Z(RdDat[42]) );
         BUFD3 rd43buf(.A(RdCol0Line[43]), .Z(RdDat[43]) );
         BUFD3 rd44buf(.A(RdCol0Line[44]), .Z(RdDat[44]) );
         BUFD3 rd45buf(.A(RdCol0Line[45]), .Z(RdDat[45]) );
         BUFD3 rd46buf(.A(RdCol0Line[46]), .Z(RdDat[46]) );
         BUFD3 rd47buf(.A(RdCol0Line[47]), .Z(RdDat[47]) );
         BUFD3 rd48buf(.A(RdCol0Line[48]), .Z(RdDat[48]) );
         BUFD3 rd49buf(.A(RdCol0Line[49]), .Z(RdDat[49]) );
         BUFD3 rd50buf(.A(RdCol0Line[50]), .Z(RdDat[50]) );
         BUFD3 rd51buf(.A(RdCol0Line[51]), .Z(RdDat[51]) );
         BUFD3 rd52buf(.A(RdCol0Line[52]), .Z(RdDat[52]) );
         BUFD3 rd53buf(.A(RdCol0Line[53]), .Z(RdDat[53]) );
         BUFD3 rd54buf(.A(RdCol0Line[54]), .Z(RdDat[54]) );
         BUFD3 rd55buf(.A(RdCol0Line[55]), .Z(RdDat[55]) );
         BUFD3 rd56buf(.A(RdCol0Line[56]), .Z(RdDat[56]) );
         BUFD3 rd57buf(.A(RdCol0Line[57]), .Z(RdDat[57]) );
         BUFD3 rd58buf(.A(RdCol0Line[58]), .Z(RdDat[58]) );
         BUFD3 rd59buf(.A(RdCol0Line[59]), .Z(RdDat[59]) );
         BUFD3 rd60buf(.A(RdCol0Line[60]), .Z(RdDat[60]) );
         BUFD3 rd61buf(.A(RdCol0Line[61]), .Z(RdDat[61]) );
         BUFD3 rd62buf(.A(RdCol0Line[62]), .Z(RdDat[62]) );
         BUFD3 rd63buf(.A(RdCol0Line[63]), .Z(RdDat[63]) );

   endmodule

