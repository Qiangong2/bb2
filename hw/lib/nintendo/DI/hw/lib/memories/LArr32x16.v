// This file was automatically derived from "LArr32x16.vpp"
// using vpp on Thu May 18 23:07:31 2000.



// syn myclk = NullClk


module LArr32x16 (
	NullClk,
	WWL,
	WrDat,
	RWL,
	ColSelect,
	RdDat);

	input NullClk;
	input [31:0]WWL;
	input [15:0] WrDat;
	input [7:0]RWL;
	   input [1:0] ColSelect;
	output [15:0] RdDat;


       wire [15:0] RdCol0Line;
       wire [15:0] RdCol1Line;
       wire [15:0] RdCol2Line;
       wire [15:0] RdCol3Line;

  
            //
            // ReadLineBuffers
            //
	      
	    INVDA RWLR0Buf0 ( .A ( RWL[0] ), .Z ( RWLR0Wire0 ) );
	    INVDA RWLR0Buf1 ( .A ( RWL[0] ), .Z ( RWLR0Wire1 ) );
	    INVDA RWLR0Buf2 ( .A ( RWL[0] ), .Z ( RWLR0Wire2 ) );
	    INVDA RWLR0Buf3 ( .A ( RWL[0] ), .Z ( RWLR0Wire3 ) );
	    INVDA RWLR1Buf0 ( .A ( RWL[1] ), .Z ( RWLR1Wire0 ) );
	    INVDA RWLR1Buf1 ( .A ( RWL[1] ), .Z ( RWLR1Wire1 ) );
	    INVDA RWLR1Buf2 ( .A ( RWL[1] ), .Z ( RWLR1Wire2 ) );
	    INVDA RWLR1Buf3 ( .A ( RWL[1] ), .Z ( RWLR1Wire3 ) );
	    INVDA RWLR2Buf0 ( .A ( RWL[2] ), .Z ( RWLR2Wire0 ) );
	    INVDA RWLR2Buf1 ( .A ( RWL[2] ), .Z ( RWLR2Wire1 ) );
	    INVDA RWLR2Buf2 ( .A ( RWL[2] ), .Z ( RWLR2Wire2 ) );
	    INVDA RWLR2Buf3 ( .A ( RWL[2] ), .Z ( RWLR2Wire3 ) );
	    INVDA RWLR3Buf0 ( .A ( RWL[3] ), .Z ( RWLR3Wire0 ) );
	    INVDA RWLR3Buf1 ( .A ( RWL[3] ), .Z ( RWLR3Wire1 ) );
	    INVDA RWLR3Buf2 ( .A ( RWL[3] ), .Z ( RWLR3Wire2 ) );
	    INVDA RWLR3Buf3 ( .A ( RWL[3] ), .Z ( RWLR3Wire3 ) );
	    INVDA RWLR4Buf0 ( .A ( RWL[4] ), .Z ( RWLR4Wire0 ) );
	    INVDA RWLR4Buf1 ( .A ( RWL[4] ), .Z ( RWLR4Wire1 ) );
	    INVDA RWLR4Buf2 ( .A ( RWL[4] ), .Z ( RWLR4Wire2 ) );
	    INVDA RWLR4Buf3 ( .A ( RWL[4] ), .Z ( RWLR4Wire3 ) );
	    INVDA RWLR5Buf0 ( .A ( RWL[5] ), .Z ( RWLR5Wire0 ) );
	    INVDA RWLR5Buf1 ( .A ( RWL[5] ), .Z ( RWLR5Wire1 ) );
	    INVDA RWLR5Buf2 ( .A ( RWL[5] ), .Z ( RWLR5Wire2 ) );
	    INVDA RWLR5Buf3 ( .A ( RWL[5] ), .Z ( RWLR5Wire3 ) );
	    INVDA RWLR6Buf0 ( .A ( RWL[6] ), .Z ( RWLR6Wire0 ) );
	    INVDA RWLR6Buf1 ( .A ( RWL[6] ), .Z ( RWLR6Wire1 ) );
	    INVDA RWLR6Buf2 ( .A ( RWL[6] ), .Z ( RWLR6Wire2 ) );
	    INVDA RWLR6Buf3 ( .A ( RWL[6] ), .Z ( RWLR6Wire3 ) );
	    INVDA RWLR7Buf0 ( .A ( RWL[7] ), .Z ( RWLR7Wire0 ) );
	    INVDA RWLR7Buf1 ( .A ( RWL[7] ), .Z ( RWLR7Wire1 ) );
	    INVDA RWLR7Buf2 ( .A ( RWL[7] ), .Z ( RWLR7Wire2 ) );
	    INVDA RWLR7Buf3 ( .A ( RWL[7] ), .Z ( RWLR7Wire3 ) );
  
            //
            // WriteLineBuffers
            //
	      
	    INVDA WWLW0Buf0 ( .A ( WWL[0] ), .Z ( WWLW0Wire0 ) );
	    INVDA WWLW1Buf0 ( .A ( WWL[1] ), .Z ( WWLW1Wire0 ) );
	    INVDA WWLW2Buf0 ( .A ( WWL[2] ), .Z ( WWLW2Wire0 ) );
	    INVDA WWLW3Buf0 ( .A ( WWL[3] ), .Z ( WWLW3Wire0 ) );
	    INVDA WWLW4Buf0 ( .A ( WWL[4] ), .Z ( WWLW4Wire0 ) );
	    INVDA WWLW5Buf0 ( .A ( WWL[5] ), .Z ( WWLW5Wire0 ) );
	    INVDA WWLW6Buf0 ( .A ( WWL[6] ), .Z ( WWLW6Wire0 ) );
	    INVDA WWLW7Buf0 ( .A ( WWL[7] ), .Z ( WWLW7Wire0 ) );
	    INVDA WWLW8Buf0 ( .A ( WWL[8] ), .Z ( WWLW8Wire0 ) );
	    INVDA WWLW9Buf0 ( .A ( WWL[9] ), .Z ( WWLW9Wire0 ) );
	    INVDA WWLW10Buf0 ( .A ( WWL[10] ), .Z ( WWLW10Wire0 ) );
	    INVDA WWLW11Buf0 ( .A ( WWL[11] ), .Z ( WWLW11Wire0 ) );
	    INVDA WWLW12Buf0 ( .A ( WWL[12] ), .Z ( WWLW12Wire0 ) );
	    INVDA WWLW13Buf0 ( .A ( WWL[13] ), .Z ( WWLW13Wire0 ) );
	    INVDA WWLW14Buf0 ( .A ( WWL[14] ), .Z ( WWLW14Wire0 ) );
	    INVDA WWLW15Buf0 ( .A ( WWL[15] ), .Z ( WWLW15Wire0 ) );
	    INVDA WWLW16Buf0 ( .A ( WWL[16] ), .Z ( WWLW16Wire0 ) );
	    INVDA WWLW17Buf0 ( .A ( WWL[17] ), .Z ( WWLW17Wire0 ) );
	    INVDA WWLW18Buf0 ( .A ( WWL[18] ), .Z ( WWLW18Wire0 ) );
	    INVDA WWLW19Buf0 ( .A ( WWL[19] ), .Z ( WWLW19Wire0 ) );
	    INVDA WWLW20Buf0 ( .A ( WWL[20] ), .Z ( WWLW20Wire0 ) );
	    INVDA WWLW21Buf0 ( .A ( WWL[21] ), .Z ( WWLW21Wire0 ) );
	    INVDA WWLW22Buf0 ( .A ( WWL[22] ), .Z ( WWLW22Wire0 ) );
	    INVDA WWLW23Buf0 ( .A ( WWL[23] ), .Z ( WWLW23Wire0 ) );
	    INVDA WWLW24Buf0 ( .A ( WWL[24] ), .Z ( WWLW24Wire0 ) );
	    INVDA WWLW25Buf0 ( .A ( WWL[25] ), .Z ( WWLW25Wire0 ) );
	    INVDA WWLW26Buf0 ( .A ( WWL[26] ), .Z ( WWLW26Wire0 ) );
	    INVDA WWLW27Buf0 ( .A ( WWL[27] ), .Z ( WWLW27Wire0 ) );
	    INVDA WWLW28Buf0 ( .A ( WWL[28] ), .Z ( WWLW28Wire0 ) );
	    INVDA WWLW29Buf0 ( .A ( WWL[29] ), .Z ( WWLW29Wire0 ) );
	    INVDA WWLW30Buf0 ( .A ( WWL[30] ), .Z ( WWLW30Wire0 ) );
	    INVDA WWLW31Buf0 ( .A ( WWL[31] ), .Z ( WWLW31Wire0 ) );

//
//
// this is the array of latches itself.
     
            //
            // Row 0
	    //
	      
 	    LATNT1 LatR0C0B0( .G(WWLW0Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR0C1B0( .G(WWLW1Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol1Line[0]) );
 	    LATNT1 LatR0C2B0( .G(WWLW2Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol2Line[0]) );
 	    LATNT1 LatR0C3B0( .G(WWLW3Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol3Line[0]) );
 	    LATNT1 LatR0C0B1( .G(WWLW0Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR0C1B1( .G(WWLW1Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol1Line[1]) );
 	    LATNT1 LatR0C2B1( .G(WWLW2Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol2Line[1]) );
 	    LATNT1 LatR0C3B1( .G(WWLW3Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol3Line[1]) );
 	    LATNT1 LatR0C0B2( .G(WWLW0Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR0C1B2( .G(WWLW1Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol1Line[2]) );
 	    LATNT1 LatR0C2B2( .G(WWLW2Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol2Line[2]) );
 	    LATNT1 LatR0C3B2( .G(WWLW3Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol3Line[2]) );
 	    LATNT1 LatR0C0B3( .G(WWLW0Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR0C1B3( .G(WWLW1Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol1Line[3]) );
 	    LATNT1 LatR0C2B3( .G(WWLW2Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol2Line[3]) );
 	    LATNT1 LatR0C3B3( .G(WWLW3Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol3Line[3]) );
 	    LATNT1 LatR0C0B4( .G(WWLW0Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[4]) );
 	    LATNT1 LatR0C1B4( .G(WWLW1Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol1Line[4]) );
 	    LATNT1 LatR0C2B4( .G(WWLW2Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol2Line[4]) );
 	    LATNT1 LatR0C3B4( .G(WWLW3Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol3Line[4]) );
 	    LATNT1 LatR0C0B5( .G(WWLW0Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[5]) );
 	    LATNT1 LatR0C1B5( .G(WWLW1Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol1Line[5]) );
 	    LATNT1 LatR0C2B5( .G(WWLW2Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol2Line[5]) );
 	    LATNT1 LatR0C3B5( .G(WWLW3Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol3Line[5]) );
 	    LATNT1 LatR0C0B6( .G(WWLW0Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[6]) );
 	    LATNT1 LatR0C1B6( .G(WWLW1Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol1Line[6]) );
 	    LATNT1 LatR0C2B6( .G(WWLW2Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol2Line[6]) );
 	    LATNT1 LatR0C3B6( .G(WWLW3Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol3Line[6]) );
 	    LATNT1 LatR0C0B7( .G(WWLW0Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[7]) );
 	    LATNT1 LatR0C1B7( .G(WWLW1Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol1Line[7]) );
 	    LATNT1 LatR0C2B7( .G(WWLW2Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol2Line[7]) );
 	    LATNT1 LatR0C3B7( .G(WWLW3Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol3Line[7]) );
 	    LATNT1 LatR0C0B8( .G(WWLW0Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[8]) );
 	    LATNT1 LatR0C1B8( .G(WWLW1Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol1Line[8]) );
 	    LATNT1 LatR0C2B8( .G(WWLW2Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol2Line[8]) );
 	    LATNT1 LatR0C3B8( .G(WWLW3Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol3Line[8]) );
 	    LATNT1 LatR0C0B9( .G(WWLW0Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[9]) );
 	    LATNT1 LatR0C1B9( .G(WWLW1Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol1Line[9]) );
 	    LATNT1 LatR0C2B9( .G(WWLW2Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol2Line[9]) );
 	    LATNT1 LatR0C3B9( .G(WWLW3Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol3Line[9]) );
 	    LATNT1 LatR0C0B10( .G(WWLW0Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[10]) );
 	    LATNT1 LatR0C1B10( .G(WWLW1Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol1Line[10]) );
 	    LATNT1 LatR0C2B10( .G(WWLW2Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol2Line[10]) );
 	    LATNT1 LatR0C3B10( .G(WWLW3Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol3Line[10]) );
 	    LATNT1 LatR0C0B11( .G(WWLW0Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[11]) );
 	    LATNT1 LatR0C1B11( .G(WWLW1Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol1Line[11]) );
 	    LATNT1 LatR0C2B11( .G(WWLW2Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol2Line[11]) );
 	    LATNT1 LatR0C3B11( .G(WWLW3Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol3Line[11]) );
 	    LATNT1 LatR0C0B12( .G(WWLW0Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[12]) );
 	    LATNT1 LatR0C1B12( .G(WWLW1Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol1Line[12]) );
 	    LATNT1 LatR0C2B12( .G(WWLW2Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol2Line[12]) );
 	    LATNT1 LatR0C3B12( .G(WWLW3Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol3Line[12]) );
 	    LATNT1 LatR0C0B13( .G(WWLW0Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[13]) );
 	    LATNT1 LatR0C1B13( .G(WWLW1Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol1Line[13]) );
 	    LATNT1 LatR0C2B13( .G(WWLW2Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol2Line[13]) );
 	    LATNT1 LatR0C3B13( .G(WWLW3Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol3Line[13]) );
 	    LATNT1 LatR0C0B14( .G(WWLW0Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[14]) );
 	    LATNT1 LatR0C1B14( .G(WWLW1Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol1Line[14]) );
 	    LATNT1 LatR0C2B14( .G(WWLW2Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol2Line[14]) );
 	    LATNT1 LatR0C3B14( .G(WWLW3Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol3Line[14]) );
 	    LATNT1 LatR0C0B15( .G(WWLW0Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[15]) );
 	    LATNT1 LatR0C1B15( .G(WWLW1Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol1Line[15]) );
 	    LATNT1 LatR0C2B15( .G(WWLW2Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol2Line[15]) );
 	    LATNT1 LatR0C3B15( .G(WWLW3Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol3Line[15]) );
     
            //
            // Row 1
	    //
	      
 	    LATNT1 LatR1C0B0( .G(WWLW4Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR1C1B0( .G(WWLW5Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol1Line[0]) );
 	    LATNT1 LatR1C2B0( .G(WWLW6Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol2Line[0]) );
 	    LATNT1 LatR1C3B0( .G(WWLW7Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol3Line[0]) );
 	    LATNT1 LatR1C0B1( .G(WWLW4Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR1C1B1( .G(WWLW5Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol1Line[1]) );
 	    LATNT1 LatR1C2B1( .G(WWLW6Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol2Line[1]) );
 	    LATNT1 LatR1C3B1( .G(WWLW7Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol3Line[1]) );
 	    LATNT1 LatR1C0B2( .G(WWLW4Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR1C1B2( .G(WWLW5Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol1Line[2]) );
 	    LATNT1 LatR1C2B2( .G(WWLW6Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol2Line[2]) );
 	    LATNT1 LatR1C3B2( .G(WWLW7Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol3Line[2]) );
 	    LATNT1 LatR1C0B3( .G(WWLW4Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR1C1B3( .G(WWLW5Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol1Line[3]) );
 	    LATNT1 LatR1C2B3( .G(WWLW6Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol2Line[3]) );
 	    LATNT1 LatR1C3B3( .G(WWLW7Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol3Line[3]) );
 	    LATNT1 LatR1C0B4( .G(WWLW4Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[4]) );
 	    LATNT1 LatR1C1B4( .G(WWLW5Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol1Line[4]) );
 	    LATNT1 LatR1C2B4( .G(WWLW6Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol2Line[4]) );
 	    LATNT1 LatR1C3B4( .G(WWLW7Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol3Line[4]) );
 	    LATNT1 LatR1C0B5( .G(WWLW4Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[5]) );
 	    LATNT1 LatR1C1B5( .G(WWLW5Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol1Line[5]) );
 	    LATNT1 LatR1C2B5( .G(WWLW6Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol2Line[5]) );
 	    LATNT1 LatR1C3B5( .G(WWLW7Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol3Line[5]) );
 	    LATNT1 LatR1C0B6( .G(WWLW4Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[6]) );
 	    LATNT1 LatR1C1B6( .G(WWLW5Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol1Line[6]) );
 	    LATNT1 LatR1C2B6( .G(WWLW6Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol2Line[6]) );
 	    LATNT1 LatR1C3B6( .G(WWLW7Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol3Line[6]) );
 	    LATNT1 LatR1C0B7( .G(WWLW4Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[7]) );
 	    LATNT1 LatR1C1B7( .G(WWLW5Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol1Line[7]) );
 	    LATNT1 LatR1C2B7( .G(WWLW6Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol2Line[7]) );
 	    LATNT1 LatR1C3B7( .G(WWLW7Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol3Line[7]) );
 	    LATNT1 LatR1C0B8( .G(WWLW4Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[8]) );
 	    LATNT1 LatR1C1B8( .G(WWLW5Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol1Line[8]) );
 	    LATNT1 LatR1C2B8( .G(WWLW6Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol2Line[8]) );
 	    LATNT1 LatR1C3B8( .G(WWLW7Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol3Line[8]) );
 	    LATNT1 LatR1C0B9( .G(WWLW4Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[9]) );
 	    LATNT1 LatR1C1B9( .G(WWLW5Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol1Line[9]) );
 	    LATNT1 LatR1C2B9( .G(WWLW6Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol2Line[9]) );
 	    LATNT1 LatR1C3B9( .G(WWLW7Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol3Line[9]) );
 	    LATNT1 LatR1C0B10( .G(WWLW4Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[10]) );
 	    LATNT1 LatR1C1B10( .G(WWLW5Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol1Line[10]) );
 	    LATNT1 LatR1C2B10( .G(WWLW6Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol2Line[10]) );
 	    LATNT1 LatR1C3B10( .G(WWLW7Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol3Line[10]) );
 	    LATNT1 LatR1C0B11( .G(WWLW4Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[11]) );
 	    LATNT1 LatR1C1B11( .G(WWLW5Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol1Line[11]) );
 	    LATNT1 LatR1C2B11( .G(WWLW6Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol2Line[11]) );
 	    LATNT1 LatR1C3B11( .G(WWLW7Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol3Line[11]) );
 	    LATNT1 LatR1C0B12( .G(WWLW4Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[12]) );
 	    LATNT1 LatR1C1B12( .G(WWLW5Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol1Line[12]) );
 	    LATNT1 LatR1C2B12( .G(WWLW6Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol2Line[12]) );
 	    LATNT1 LatR1C3B12( .G(WWLW7Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol3Line[12]) );
 	    LATNT1 LatR1C0B13( .G(WWLW4Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[13]) );
 	    LATNT1 LatR1C1B13( .G(WWLW5Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol1Line[13]) );
 	    LATNT1 LatR1C2B13( .G(WWLW6Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol2Line[13]) );
 	    LATNT1 LatR1C3B13( .G(WWLW7Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol3Line[13]) );
 	    LATNT1 LatR1C0B14( .G(WWLW4Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[14]) );
 	    LATNT1 LatR1C1B14( .G(WWLW5Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol1Line[14]) );
 	    LATNT1 LatR1C2B14( .G(WWLW6Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol2Line[14]) );
 	    LATNT1 LatR1C3B14( .G(WWLW7Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol3Line[14]) );
 	    LATNT1 LatR1C0B15( .G(WWLW4Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[15]) );
 	    LATNT1 LatR1C1B15( .G(WWLW5Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol1Line[15]) );
 	    LATNT1 LatR1C2B15( .G(WWLW6Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol2Line[15]) );
 	    LATNT1 LatR1C3B15( .G(WWLW7Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol3Line[15]) );
     
            //
            // Row 2
	    //
	      
 	    LATNT1 LatR2C0B0( .G(WWLW8Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR2C1B0( .G(WWLW9Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol1Line[0]) );
 	    LATNT1 LatR2C2B0( .G(WWLW10Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol2Line[0]) );
 	    LATNT1 LatR2C3B0( .G(WWLW11Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol3Line[0]) );
 	    LATNT1 LatR2C0B1( .G(WWLW8Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR2C1B1( .G(WWLW9Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol1Line[1]) );
 	    LATNT1 LatR2C2B1( .G(WWLW10Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol2Line[1]) );
 	    LATNT1 LatR2C3B1( .G(WWLW11Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol3Line[1]) );
 	    LATNT1 LatR2C0B2( .G(WWLW8Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR2C1B2( .G(WWLW9Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol1Line[2]) );
 	    LATNT1 LatR2C2B2( .G(WWLW10Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol2Line[2]) );
 	    LATNT1 LatR2C3B2( .G(WWLW11Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol3Line[2]) );
 	    LATNT1 LatR2C0B3( .G(WWLW8Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR2C1B3( .G(WWLW9Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol1Line[3]) );
 	    LATNT1 LatR2C2B3( .G(WWLW10Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol2Line[3]) );
 	    LATNT1 LatR2C3B3( .G(WWLW11Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol3Line[3]) );
 	    LATNT1 LatR2C0B4( .G(WWLW8Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[4]) );
 	    LATNT1 LatR2C1B4( .G(WWLW9Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol1Line[4]) );
 	    LATNT1 LatR2C2B4( .G(WWLW10Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol2Line[4]) );
 	    LATNT1 LatR2C3B4( .G(WWLW11Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol3Line[4]) );
 	    LATNT1 LatR2C0B5( .G(WWLW8Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[5]) );
 	    LATNT1 LatR2C1B5( .G(WWLW9Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol1Line[5]) );
 	    LATNT1 LatR2C2B5( .G(WWLW10Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol2Line[5]) );
 	    LATNT1 LatR2C3B5( .G(WWLW11Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol3Line[5]) );
 	    LATNT1 LatR2C0B6( .G(WWLW8Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[6]) );
 	    LATNT1 LatR2C1B6( .G(WWLW9Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol1Line[6]) );
 	    LATNT1 LatR2C2B6( .G(WWLW10Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol2Line[6]) );
 	    LATNT1 LatR2C3B6( .G(WWLW11Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol3Line[6]) );
 	    LATNT1 LatR2C0B7( .G(WWLW8Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[7]) );
 	    LATNT1 LatR2C1B7( .G(WWLW9Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol1Line[7]) );
 	    LATNT1 LatR2C2B7( .G(WWLW10Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol2Line[7]) );
 	    LATNT1 LatR2C3B7( .G(WWLW11Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol3Line[7]) );
 	    LATNT1 LatR2C0B8( .G(WWLW8Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[8]) );
 	    LATNT1 LatR2C1B8( .G(WWLW9Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol1Line[8]) );
 	    LATNT1 LatR2C2B8( .G(WWLW10Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol2Line[8]) );
 	    LATNT1 LatR2C3B8( .G(WWLW11Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol3Line[8]) );
 	    LATNT1 LatR2C0B9( .G(WWLW8Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[9]) );
 	    LATNT1 LatR2C1B9( .G(WWLW9Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol1Line[9]) );
 	    LATNT1 LatR2C2B9( .G(WWLW10Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol2Line[9]) );
 	    LATNT1 LatR2C3B9( .G(WWLW11Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol3Line[9]) );
 	    LATNT1 LatR2C0B10( .G(WWLW8Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[10]) );
 	    LATNT1 LatR2C1B10( .G(WWLW9Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol1Line[10]) );
 	    LATNT1 LatR2C2B10( .G(WWLW10Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol2Line[10]) );
 	    LATNT1 LatR2C3B10( .G(WWLW11Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol3Line[10]) );
 	    LATNT1 LatR2C0B11( .G(WWLW8Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[11]) );
 	    LATNT1 LatR2C1B11( .G(WWLW9Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol1Line[11]) );
 	    LATNT1 LatR2C2B11( .G(WWLW10Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol2Line[11]) );
 	    LATNT1 LatR2C3B11( .G(WWLW11Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol3Line[11]) );
 	    LATNT1 LatR2C0B12( .G(WWLW8Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[12]) );
 	    LATNT1 LatR2C1B12( .G(WWLW9Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol1Line[12]) );
 	    LATNT1 LatR2C2B12( .G(WWLW10Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol2Line[12]) );
 	    LATNT1 LatR2C3B12( .G(WWLW11Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol3Line[12]) );
 	    LATNT1 LatR2C0B13( .G(WWLW8Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[13]) );
 	    LATNT1 LatR2C1B13( .G(WWLW9Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol1Line[13]) );
 	    LATNT1 LatR2C2B13( .G(WWLW10Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol2Line[13]) );
 	    LATNT1 LatR2C3B13( .G(WWLW11Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol3Line[13]) );
 	    LATNT1 LatR2C0B14( .G(WWLW8Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[14]) );
 	    LATNT1 LatR2C1B14( .G(WWLW9Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol1Line[14]) );
 	    LATNT1 LatR2C2B14( .G(WWLW10Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol2Line[14]) );
 	    LATNT1 LatR2C3B14( .G(WWLW11Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol3Line[14]) );
 	    LATNT1 LatR2C0B15( .G(WWLW8Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[15]) );
 	    LATNT1 LatR2C1B15( .G(WWLW9Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol1Line[15]) );
 	    LATNT1 LatR2C2B15( .G(WWLW10Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol2Line[15]) );
 	    LATNT1 LatR2C3B15( .G(WWLW11Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol3Line[15]) );
     
            //
            // Row 3
	    //
	      
 	    LATNT1 LatR3C0B0( .G(WWLW12Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR3C1B0( .G(WWLW13Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol1Line[0]) );
 	    LATNT1 LatR3C2B0( .G(WWLW14Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol2Line[0]) );
 	    LATNT1 LatR3C3B0( .G(WWLW15Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol3Line[0]) );
 	    LATNT1 LatR3C0B1( .G(WWLW12Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR3C1B1( .G(WWLW13Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol1Line[1]) );
 	    LATNT1 LatR3C2B1( .G(WWLW14Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol2Line[1]) );
 	    LATNT1 LatR3C3B1( .G(WWLW15Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol3Line[1]) );
 	    LATNT1 LatR3C0B2( .G(WWLW12Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR3C1B2( .G(WWLW13Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol1Line[2]) );
 	    LATNT1 LatR3C2B2( .G(WWLW14Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol2Line[2]) );
 	    LATNT1 LatR3C3B2( .G(WWLW15Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol3Line[2]) );
 	    LATNT1 LatR3C0B3( .G(WWLW12Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR3C1B3( .G(WWLW13Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol1Line[3]) );
 	    LATNT1 LatR3C2B3( .G(WWLW14Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol2Line[3]) );
 	    LATNT1 LatR3C3B3( .G(WWLW15Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol3Line[3]) );
 	    LATNT1 LatR3C0B4( .G(WWLW12Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[4]) );
 	    LATNT1 LatR3C1B4( .G(WWLW13Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol1Line[4]) );
 	    LATNT1 LatR3C2B4( .G(WWLW14Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol2Line[4]) );
 	    LATNT1 LatR3C3B4( .G(WWLW15Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol3Line[4]) );
 	    LATNT1 LatR3C0B5( .G(WWLW12Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[5]) );
 	    LATNT1 LatR3C1B5( .G(WWLW13Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol1Line[5]) );
 	    LATNT1 LatR3C2B5( .G(WWLW14Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol2Line[5]) );
 	    LATNT1 LatR3C3B5( .G(WWLW15Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol3Line[5]) );
 	    LATNT1 LatR3C0B6( .G(WWLW12Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[6]) );
 	    LATNT1 LatR3C1B6( .G(WWLW13Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol1Line[6]) );
 	    LATNT1 LatR3C2B6( .G(WWLW14Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol2Line[6]) );
 	    LATNT1 LatR3C3B6( .G(WWLW15Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol3Line[6]) );
 	    LATNT1 LatR3C0B7( .G(WWLW12Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[7]) );
 	    LATNT1 LatR3C1B7( .G(WWLW13Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol1Line[7]) );
 	    LATNT1 LatR3C2B7( .G(WWLW14Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol2Line[7]) );
 	    LATNT1 LatR3C3B7( .G(WWLW15Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol3Line[7]) );
 	    LATNT1 LatR3C0B8( .G(WWLW12Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[8]) );
 	    LATNT1 LatR3C1B8( .G(WWLW13Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol1Line[8]) );
 	    LATNT1 LatR3C2B8( .G(WWLW14Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol2Line[8]) );
 	    LATNT1 LatR3C3B8( .G(WWLW15Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol3Line[8]) );
 	    LATNT1 LatR3C0B9( .G(WWLW12Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[9]) );
 	    LATNT1 LatR3C1B9( .G(WWLW13Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol1Line[9]) );
 	    LATNT1 LatR3C2B9( .G(WWLW14Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol2Line[9]) );
 	    LATNT1 LatR3C3B9( .G(WWLW15Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol3Line[9]) );
 	    LATNT1 LatR3C0B10( .G(WWLW12Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[10]) );
 	    LATNT1 LatR3C1B10( .G(WWLW13Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol1Line[10]) );
 	    LATNT1 LatR3C2B10( .G(WWLW14Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol2Line[10]) );
 	    LATNT1 LatR3C3B10( .G(WWLW15Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol3Line[10]) );
 	    LATNT1 LatR3C0B11( .G(WWLW12Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[11]) );
 	    LATNT1 LatR3C1B11( .G(WWLW13Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol1Line[11]) );
 	    LATNT1 LatR3C2B11( .G(WWLW14Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol2Line[11]) );
 	    LATNT1 LatR3C3B11( .G(WWLW15Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol3Line[11]) );
 	    LATNT1 LatR3C0B12( .G(WWLW12Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[12]) );
 	    LATNT1 LatR3C1B12( .G(WWLW13Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol1Line[12]) );
 	    LATNT1 LatR3C2B12( .G(WWLW14Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol2Line[12]) );
 	    LATNT1 LatR3C3B12( .G(WWLW15Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol3Line[12]) );
 	    LATNT1 LatR3C0B13( .G(WWLW12Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[13]) );
 	    LATNT1 LatR3C1B13( .G(WWLW13Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol1Line[13]) );
 	    LATNT1 LatR3C2B13( .G(WWLW14Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol2Line[13]) );
 	    LATNT1 LatR3C3B13( .G(WWLW15Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol3Line[13]) );
 	    LATNT1 LatR3C0B14( .G(WWLW12Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[14]) );
 	    LATNT1 LatR3C1B14( .G(WWLW13Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol1Line[14]) );
 	    LATNT1 LatR3C2B14( .G(WWLW14Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol2Line[14]) );
 	    LATNT1 LatR3C3B14( .G(WWLW15Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol3Line[14]) );
 	    LATNT1 LatR3C0B15( .G(WWLW12Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[15]) );
 	    LATNT1 LatR3C1B15( .G(WWLW13Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol1Line[15]) );
 	    LATNT1 LatR3C2B15( .G(WWLW14Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol2Line[15]) );
 	    LATNT1 LatR3C3B15( .G(WWLW15Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol3Line[15]) );
     
            //
            // Row 4
	    //
	      
 	    LATNT1 LatR4C0B0( .G(WWLW16Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR4C1B0( .G(WWLW17Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol1Line[0]) );
 	    LATNT1 LatR4C2B0( .G(WWLW18Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol2Line[0]) );
 	    LATNT1 LatR4C3B0( .G(WWLW19Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol3Line[0]) );
 	    LATNT1 LatR4C0B1( .G(WWLW16Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR4C1B1( .G(WWLW17Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol1Line[1]) );
 	    LATNT1 LatR4C2B1( .G(WWLW18Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol2Line[1]) );
 	    LATNT1 LatR4C3B1( .G(WWLW19Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol3Line[1]) );
 	    LATNT1 LatR4C0B2( .G(WWLW16Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR4C1B2( .G(WWLW17Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol1Line[2]) );
 	    LATNT1 LatR4C2B2( .G(WWLW18Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol2Line[2]) );
 	    LATNT1 LatR4C3B2( .G(WWLW19Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol3Line[2]) );
 	    LATNT1 LatR4C0B3( .G(WWLW16Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR4C1B3( .G(WWLW17Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol1Line[3]) );
 	    LATNT1 LatR4C2B3( .G(WWLW18Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol2Line[3]) );
 	    LATNT1 LatR4C3B3( .G(WWLW19Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol3Line[3]) );
 	    LATNT1 LatR4C0B4( .G(WWLW16Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[4]) );
 	    LATNT1 LatR4C1B4( .G(WWLW17Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol1Line[4]) );
 	    LATNT1 LatR4C2B4( .G(WWLW18Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol2Line[4]) );
 	    LATNT1 LatR4C3B4( .G(WWLW19Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol3Line[4]) );
 	    LATNT1 LatR4C0B5( .G(WWLW16Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[5]) );
 	    LATNT1 LatR4C1B5( .G(WWLW17Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol1Line[5]) );
 	    LATNT1 LatR4C2B5( .G(WWLW18Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol2Line[5]) );
 	    LATNT1 LatR4C3B5( .G(WWLW19Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol3Line[5]) );
 	    LATNT1 LatR4C0B6( .G(WWLW16Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[6]) );
 	    LATNT1 LatR4C1B6( .G(WWLW17Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol1Line[6]) );
 	    LATNT1 LatR4C2B6( .G(WWLW18Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol2Line[6]) );
 	    LATNT1 LatR4C3B6( .G(WWLW19Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol3Line[6]) );
 	    LATNT1 LatR4C0B7( .G(WWLW16Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[7]) );
 	    LATNT1 LatR4C1B7( .G(WWLW17Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol1Line[7]) );
 	    LATNT1 LatR4C2B7( .G(WWLW18Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol2Line[7]) );
 	    LATNT1 LatR4C3B7( .G(WWLW19Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol3Line[7]) );
 	    LATNT1 LatR4C0B8( .G(WWLW16Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[8]) );
 	    LATNT1 LatR4C1B8( .G(WWLW17Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol1Line[8]) );
 	    LATNT1 LatR4C2B8( .G(WWLW18Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol2Line[8]) );
 	    LATNT1 LatR4C3B8( .G(WWLW19Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol3Line[8]) );
 	    LATNT1 LatR4C0B9( .G(WWLW16Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[9]) );
 	    LATNT1 LatR4C1B9( .G(WWLW17Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol1Line[9]) );
 	    LATNT1 LatR4C2B9( .G(WWLW18Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol2Line[9]) );
 	    LATNT1 LatR4C3B9( .G(WWLW19Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol3Line[9]) );
 	    LATNT1 LatR4C0B10( .G(WWLW16Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[10]) );
 	    LATNT1 LatR4C1B10( .G(WWLW17Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol1Line[10]) );
 	    LATNT1 LatR4C2B10( .G(WWLW18Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol2Line[10]) );
 	    LATNT1 LatR4C3B10( .G(WWLW19Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol3Line[10]) );
 	    LATNT1 LatR4C0B11( .G(WWLW16Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[11]) );
 	    LATNT1 LatR4C1B11( .G(WWLW17Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol1Line[11]) );
 	    LATNT1 LatR4C2B11( .G(WWLW18Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol2Line[11]) );
 	    LATNT1 LatR4C3B11( .G(WWLW19Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol3Line[11]) );
 	    LATNT1 LatR4C0B12( .G(WWLW16Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[12]) );
 	    LATNT1 LatR4C1B12( .G(WWLW17Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol1Line[12]) );
 	    LATNT1 LatR4C2B12( .G(WWLW18Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol2Line[12]) );
 	    LATNT1 LatR4C3B12( .G(WWLW19Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol3Line[12]) );
 	    LATNT1 LatR4C0B13( .G(WWLW16Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[13]) );
 	    LATNT1 LatR4C1B13( .G(WWLW17Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol1Line[13]) );
 	    LATNT1 LatR4C2B13( .G(WWLW18Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol2Line[13]) );
 	    LATNT1 LatR4C3B13( .G(WWLW19Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol3Line[13]) );
 	    LATNT1 LatR4C0B14( .G(WWLW16Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[14]) );
 	    LATNT1 LatR4C1B14( .G(WWLW17Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol1Line[14]) );
 	    LATNT1 LatR4C2B14( .G(WWLW18Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol2Line[14]) );
 	    LATNT1 LatR4C3B14( .G(WWLW19Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol3Line[14]) );
 	    LATNT1 LatR4C0B15( .G(WWLW16Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[15]) );
 	    LATNT1 LatR4C1B15( .G(WWLW17Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol1Line[15]) );
 	    LATNT1 LatR4C2B15( .G(WWLW18Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol2Line[15]) );
 	    LATNT1 LatR4C3B15( .G(WWLW19Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol3Line[15]) );
     
            //
            // Row 5
	    //
	      
 	    LATNT1 LatR5C0B0( .G(WWLW20Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR5C1B0( .G(WWLW21Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol1Line[0]) );
 	    LATNT1 LatR5C2B0( .G(WWLW22Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol2Line[0]) );
 	    LATNT1 LatR5C3B0( .G(WWLW23Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR5Wire3), 
					  .Z(RdCol3Line[0]) );
 	    LATNT1 LatR5C0B1( .G(WWLW20Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR5C1B1( .G(WWLW21Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol1Line[1]) );
 	    LATNT1 LatR5C2B1( .G(WWLW22Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol2Line[1]) );
 	    LATNT1 LatR5C3B1( .G(WWLW23Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR5Wire3), 
					  .Z(RdCol3Line[1]) );
 	    LATNT1 LatR5C0B2( .G(WWLW20Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR5C1B2( .G(WWLW21Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol1Line[2]) );
 	    LATNT1 LatR5C2B2( .G(WWLW22Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol2Line[2]) );
 	    LATNT1 LatR5C3B2( .G(WWLW23Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR5Wire3), 
					  .Z(RdCol3Line[2]) );
 	    LATNT1 LatR5C0B3( .G(WWLW20Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR5C1B3( .G(WWLW21Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol1Line[3]) );
 	    LATNT1 LatR5C2B3( .G(WWLW22Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol2Line[3]) );
 	    LATNT1 LatR5C3B3( .G(WWLW23Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR5Wire3), 
					  .Z(RdCol3Line[3]) );
 	    LATNT1 LatR5C0B4( .G(WWLW20Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[4]) );
 	    LATNT1 LatR5C1B4( .G(WWLW21Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol1Line[4]) );
 	    LATNT1 LatR5C2B4( .G(WWLW22Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol2Line[4]) );
 	    LATNT1 LatR5C3B4( .G(WWLW23Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR5Wire3), 
					  .Z(RdCol3Line[4]) );
 	    LATNT1 LatR5C0B5( .G(WWLW20Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[5]) );
 	    LATNT1 LatR5C1B5( .G(WWLW21Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol1Line[5]) );
 	    LATNT1 LatR5C2B5( .G(WWLW22Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol2Line[5]) );
 	    LATNT1 LatR5C3B5( .G(WWLW23Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR5Wire3), 
					  .Z(RdCol3Line[5]) );
 	    LATNT1 LatR5C0B6( .G(WWLW20Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[6]) );
 	    LATNT1 LatR5C1B6( .G(WWLW21Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol1Line[6]) );
 	    LATNT1 LatR5C2B6( .G(WWLW22Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol2Line[6]) );
 	    LATNT1 LatR5C3B6( .G(WWLW23Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR5Wire3), 
					  .Z(RdCol3Line[6]) );
 	    LATNT1 LatR5C0B7( .G(WWLW20Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[7]) );
 	    LATNT1 LatR5C1B7( .G(WWLW21Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol1Line[7]) );
 	    LATNT1 LatR5C2B7( .G(WWLW22Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol2Line[7]) );
 	    LATNT1 LatR5C3B7( .G(WWLW23Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR5Wire3), 
					  .Z(RdCol3Line[7]) );
 	    LATNT1 LatR5C0B8( .G(WWLW20Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[8]) );
 	    LATNT1 LatR5C1B8( .G(WWLW21Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol1Line[8]) );
 	    LATNT1 LatR5C2B8( .G(WWLW22Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol2Line[8]) );
 	    LATNT1 LatR5C3B8( .G(WWLW23Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR5Wire3), 
					  .Z(RdCol3Line[8]) );
 	    LATNT1 LatR5C0B9( .G(WWLW20Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[9]) );
 	    LATNT1 LatR5C1B9( .G(WWLW21Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol1Line[9]) );
 	    LATNT1 LatR5C2B9( .G(WWLW22Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol2Line[9]) );
 	    LATNT1 LatR5C3B9( .G(WWLW23Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR5Wire3), 
					  .Z(RdCol3Line[9]) );
 	    LATNT1 LatR5C0B10( .G(WWLW20Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[10]) );
 	    LATNT1 LatR5C1B10( .G(WWLW21Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol1Line[10]) );
 	    LATNT1 LatR5C2B10( .G(WWLW22Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol2Line[10]) );
 	    LATNT1 LatR5C3B10( .G(WWLW23Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR5Wire3), 
					  .Z(RdCol3Line[10]) );
 	    LATNT1 LatR5C0B11( .G(WWLW20Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[11]) );
 	    LATNT1 LatR5C1B11( .G(WWLW21Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol1Line[11]) );
 	    LATNT1 LatR5C2B11( .G(WWLW22Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol2Line[11]) );
 	    LATNT1 LatR5C3B11( .G(WWLW23Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR5Wire3), 
					  .Z(RdCol3Line[11]) );
 	    LATNT1 LatR5C0B12( .G(WWLW20Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[12]) );
 	    LATNT1 LatR5C1B12( .G(WWLW21Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol1Line[12]) );
 	    LATNT1 LatR5C2B12( .G(WWLW22Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol2Line[12]) );
 	    LATNT1 LatR5C3B12( .G(WWLW23Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR5Wire3), 
					  .Z(RdCol3Line[12]) );
 	    LATNT1 LatR5C0B13( .G(WWLW20Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[13]) );
 	    LATNT1 LatR5C1B13( .G(WWLW21Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol1Line[13]) );
 	    LATNT1 LatR5C2B13( .G(WWLW22Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol2Line[13]) );
 	    LATNT1 LatR5C3B13( .G(WWLW23Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR5Wire3), 
					  .Z(RdCol3Line[13]) );
 	    LATNT1 LatR5C0B14( .G(WWLW20Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[14]) );
 	    LATNT1 LatR5C1B14( .G(WWLW21Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol1Line[14]) );
 	    LATNT1 LatR5C2B14( .G(WWLW22Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol2Line[14]) );
 	    LATNT1 LatR5C3B14( .G(WWLW23Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR5Wire3), 
					  .Z(RdCol3Line[14]) );
 	    LATNT1 LatR5C0B15( .G(WWLW20Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[15]) );
 	    LATNT1 LatR5C1B15( .G(WWLW21Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol1Line[15]) );
 	    LATNT1 LatR5C2B15( .G(WWLW22Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol2Line[15]) );
 	    LATNT1 LatR5C3B15( .G(WWLW23Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR5Wire3), 
					  .Z(RdCol3Line[15]) );
     
            //
            // Row 6
	    //
	      
 	    LATNT1 LatR6C0B0( .G(WWLW24Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR6C1B0( .G(WWLW25Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR6Wire1), 
					  .Z(RdCol1Line[0]) );
 	    LATNT1 LatR6C2B0( .G(WWLW26Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR6Wire2), 
					  .Z(RdCol2Line[0]) );
 	    LATNT1 LatR6C3B0( .G(WWLW27Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR6Wire3), 
					  .Z(RdCol3Line[0]) );
 	    LATNT1 LatR6C0B1( .G(WWLW24Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR6C1B1( .G(WWLW25Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR6Wire1), 
					  .Z(RdCol1Line[1]) );
 	    LATNT1 LatR6C2B1( .G(WWLW26Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR6Wire2), 
					  .Z(RdCol2Line[1]) );
 	    LATNT1 LatR6C3B1( .G(WWLW27Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR6Wire3), 
					  .Z(RdCol3Line[1]) );
 	    LATNT1 LatR6C0B2( .G(WWLW24Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR6C1B2( .G(WWLW25Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR6Wire1), 
					  .Z(RdCol1Line[2]) );
 	    LATNT1 LatR6C2B2( .G(WWLW26Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR6Wire2), 
					  .Z(RdCol2Line[2]) );
 	    LATNT1 LatR6C3B2( .G(WWLW27Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR6Wire3), 
					  .Z(RdCol3Line[2]) );
 	    LATNT1 LatR6C0B3( .G(WWLW24Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR6C1B3( .G(WWLW25Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR6Wire1), 
					  .Z(RdCol1Line[3]) );
 	    LATNT1 LatR6C2B3( .G(WWLW26Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR6Wire2), 
					  .Z(RdCol2Line[3]) );
 	    LATNT1 LatR6C3B3( .G(WWLW27Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR6Wire3), 
					  .Z(RdCol3Line[3]) );
 	    LATNT1 LatR6C0B4( .G(WWLW24Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[4]) );
 	    LATNT1 LatR6C1B4( .G(WWLW25Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR6Wire1), 
					  .Z(RdCol1Line[4]) );
 	    LATNT1 LatR6C2B4( .G(WWLW26Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR6Wire2), 
					  .Z(RdCol2Line[4]) );
 	    LATNT1 LatR6C3B4( .G(WWLW27Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR6Wire3), 
					  .Z(RdCol3Line[4]) );
 	    LATNT1 LatR6C0B5( .G(WWLW24Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[5]) );
 	    LATNT1 LatR6C1B5( .G(WWLW25Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR6Wire1), 
					  .Z(RdCol1Line[5]) );
 	    LATNT1 LatR6C2B5( .G(WWLW26Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR6Wire2), 
					  .Z(RdCol2Line[5]) );
 	    LATNT1 LatR6C3B5( .G(WWLW27Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR6Wire3), 
					  .Z(RdCol3Line[5]) );
 	    LATNT1 LatR6C0B6( .G(WWLW24Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[6]) );
 	    LATNT1 LatR6C1B6( .G(WWLW25Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR6Wire1), 
					  .Z(RdCol1Line[6]) );
 	    LATNT1 LatR6C2B6( .G(WWLW26Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR6Wire2), 
					  .Z(RdCol2Line[6]) );
 	    LATNT1 LatR6C3B6( .G(WWLW27Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR6Wire3), 
					  .Z(RdCol3Line[6]) );
 	    LATNT1 LatR6C0B7( .G(WWLW24Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[7]) );
 	    LATNT1 LatR6C1B7( .G(WWLW25Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR6Wire1), 
					  .Z(RdCol1Line[7]) );
 	    LATNT1 LatR6C2B7( .G(WWLW26Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR6Wire2), 
					  .Z(RdCol2Line[7]) );
 	    LATNT1 LatR6C3B7( .G(WWLW27Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR6Wire3), 
					  .Z(RdCol3Line[7]) );
 	    LATNT1 LatR6C0B8( .G(WWLW24Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[8]) );
 	    LATNT1 LatR6C1B8( .G(WWLW25Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR6Wire1), 
					  .Z(RdCol1Line[8]) );
 	    LATNT1 LatR6C2B8( .G(WWLW26Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR6Wire2), 
					  .Z(RdCol2Line[8]) );
 	    LATNT1 LatR6C3B8( .G(WWLW27Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR6Wire3), 
					  .Z(RdCol3Line[8]) );
 	    LATNT1 LatR6C0B9( .G(WWLW24Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[9]) );
 	    LATNT1 LatR6C1B9( .G(WWLW25Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR6Wire1), 
					  .Z(RdCol1Line[9]) );
 	    LATNT1 LatR6C2B9( .G(WWLW26Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR6Wire2), 
					  .Z(RdCol2Line[9]) );
 	    LATNT1 LatR6C3B9( .G(WWLW27Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR6Wire3), 
					  .Z(RdCol3Line[9]) );
 	    LATNT1 LatR6C0B10( .G(WWLW24Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[10]) );
 	    LATNT1 LatR6C1B10( .G(WWLW25Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR6Wire1), 
					  .Z(RdCol1Line[10]) );
 	    LATNT1 LatR6C2B10( .G(WWLW26Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR6Wire2), 
					  .Z(RdCol2Line[10]) );
 	    LATNT1 LatR6C3B10( .G(WWLW27Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR6Wire3), 
					  .Z(RdCol3Line[10]) );
 	    LATNT1 LatR6C0B11( .G(WWLW24Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[11]) );
 	    LATNT1 LatR6C1B11( .G(WWLW25Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR6Wire1), 
					  .Z(RdCol1Line[11]) );
 	    LATNT1 LatR6C2B11( .G(WWLW26Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR6Wire2), 
					  .Z(RdCol2Line[11]) );
 	    LATNT1 LatR6C3B11( .G(WWLW27Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR6Wire3), 
					  .Z(RdCol3Line[11]) );
 	    LATNT1 LatR6C0B12( .G(WWLW24Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[12]) );
 	    LATNT1 LatR6C1B12( .G(WWLW25Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR6Wire1), 
					  .Z(RdCol1Line[12]) );
 	    LATNT1 LatR6C2B12( .G(WWLW26Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR6Wire2), 
					  .Z(RdCol2Line[12]) );
 	    LATNT1 LatR6C3B12( .G(WWLW27Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR6Wire3), 
					  .Z(RdCol3Line[12]) );
 	    LATNT1 LatR6C0B13( .G(WWLW24Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[13]) );
 	    LATNT1 LatR6C1B13( .G(WWLW25Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR6Wire1), 
					  .Z(RdCol1Line[13]) );
 	    LATNT1 LatR6C2B13( .G(WWLW26Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR6Wire2), 
					  .Z(RdCol2Line[13]) );
 	    LATNT1 LatR6C3B13( .G(WWLW27Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR6Wire3), 
					  .Z(RdCol3Line[13]) );
 	    LATNT1 LatR6C0B14( .G(WWLW24Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[14]) );
 	    LATNT1 LatR6C1B14( .G(WWLW25Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR6Wire1), 
					  .Z(RdCol1Line[14]) );
 	    LATNT1 LatR6C2B14( .G(WWLW26Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR6Wire2), 
					  .Z(RdCol2Line[14]) );
 	    LATNT1 LatR6C3B14( .G(WWLW27Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR6Wire3), 
					  .Z(RdCol3Line[14]) );
 	    LATNT1 LatR6C0B15( .G(WWLW24Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[15]) );
 	    LATNT1 LatR6C1B15( .G(WWLW25Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR6Wire1), 
					  .Z(RdCol1Line[15]) );
 	    LATNT1 LatR6C2B15( .G(WWLW26Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR6Wire2), 
					  .Z(RdCol2Line[15]) );
 	    LATNT1 LatR6C3B15( .G(WWLW27Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR6Wire3), 
					  .Z(RdCol3Line[15]) );
     
            //
            // Row 7
	    //
	      
 	    LATNT1 LatR7C0B0( .G(WWLW28Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR7C1B0( .G(WWLW29Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR7Wire1), 
					  .Z(RdCol1Line[0]) );
 	    LATNT1 LatR7C2B0( .G(WWLW30Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR7Wire2), 
					  .Z(RdCol2Line[0]) );
 	    LATNT1 LatR7C3B0( .G(WWLW31Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR7Wire3), 
					  .Z(RdCol3Line[0]) );
 	    LATNT1 LatR7C0B1( .G(WWLW28Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR7C1B1( .G(WWLW29Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR7Wire1), 
					  .Z(RdCol1Line[1]) );
 	    LATNT1 LatR7C2B1( .G(WWLW30Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR7Wire2), 
					  .Z(RdCol2Line[1]) );
 	    LATNT1 LatR7C3B1( .G(WWLW31Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR7Wire3), 
					  .Z(RdCol3Line[1]) );
 	    LATNT1 LatR7C0B2( .G(WWLW28Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR7C1B2( .G(WWLW29Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR7Wire1), 
					  .Z(RdCol1Line[2]) );
 	    LATNT1 LatR7C2B2( .G(WWLW30Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR7Wire2), 
					  .Z(RdCol2Line[2]) );
 	    LATNT1 LatR7C3B2( .G(WWLW31Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR7Wire3), 
					  .Z(RdCol3Line[2]) );
 	    LATNT1 LatR7C0B3( .G(WWLW28Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR7C1B3( .G(WWLW29Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR7Wire1), 
					  .Z(RdCol1Line[3]) );
 	    LATNT1 LatR7C2B3( .G(WWLW30Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR7Wire2), 
					  .Z(RdCol2Line[3]) );
 	    LATNT1 LatR7C3B3( .G(WWLW31Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR7Wire3), 
					  .Z(RdCol3Line[3]) );
 	    LATNT1 LatR7C0B4( .G(WWLW28Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[4]) );
 	    LATNT1 LatR7C1B4( .G(WWLW29Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR7Wire1), 
					  .Z(RdCol1Line[4]) );
 	    LATNT1 LatR7C2B4( .G(WWLW30Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR7Wire2), 
					  .Z(RdCol2Line[4]) );
 	    LATNT1 LatR7C3B4( .G(WWLW31Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR7Wire3), 
					  .Z(RdCol3Line[4]) );
 	    LATNT1 LatR7C0B5( .G(WWLW28Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[5]) );
 	    LATNT1 LatR7C1B5( .G(WWLW29Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR7Wire1), 
					  .Z(RdCol1Line[5]) );
 	    LATNT1 LatR7C2B5( .G(WWLW30Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR7Wire2), 
					  .Z(RdCol2Line[5]) );
 	    LATNT1 LatR7C3B5( .G(WWLW31Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR7Wire3), 
					  .Z(RdCol3Line[5]) );
 	    LATNT1 LatR7C0B6( .G(WWLW28Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[6]) );
 	    LATNT1 LatR7C1B6( .G(WWLW29Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR7Wire1), 
					  .Z(RdCol1Line[6]) );
 	    LATNT1 LatR7C2B6( .G(WWLW30Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR7Wire2), 
					  .Z(RdCol2Line[6]) );
 	    LATNT1 LatR7C3B6( .G(WWLW31Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR7Wire3), 
					  .Z(RdCol3Line[6]) );
 	    LATNT1 LatR7C0B7( .G(WWLW28Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[7]) );
 	    LATNT1 LatR7C1B7( .G(WWLW29Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR7Wire1), 
					  .Z(RdCol1Line[7]) );
 	    LATNT1 LatR7C2B7( .G(WWLW30Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR7Wire2), 
					  .Z(RdCol2Line[7]) );
 	    LATNT1 LatR7C3B7( .G(WWLW31Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR7Wire3), 
					  .Z(RdCol3Line[7]) );
 	    LATNT1 LatR7C0B8( .G(WWLW28Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[8]) );
 	    LATNT1 LatR7C1B8( .G(WWLW29Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR7Wire1), 
					  .Z(RdCol1Line[8]) );
 	    LATNT1 LatR7C2B8( .G(WWLW30Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR7Wire2), 
					  .Z(RdCol2Line[8]) );
 	    LATNT1 LatR7C3B8( .G(WWLW31Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR7Wire3), 
					  .Z(RdCol3Line[8]) );
 	    LATNT1 LatR7C0B9( .G(WWLW28Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[9]) );
 	    LATNT1 LatR7C1B9( .G(WWLW29Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR7Wire1), 
					  .Z(RdCol1Line[9]) );
 	    LATNT1 LatR7C2B9( .G(WWLW30Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR7Wire2), 
					  .Z(RdCol2Line[9]) );
 	    LATNT1 LatR7C3B9( .G(WWLW31Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR7Wire3), 
					  .Z(RdCol3Line[9]) );
 	    LATNT1 LatR7C0B10( .G(WWLW28Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[10]) );
 	    LATNT1 LatR7C1B10( .G(WWLW29Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR7Wire1), 
					  .Z(RdCol1Line[10]) );
 	    LATNT1 LatR7C2B10( .G(WWLW30Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR7Wire2), 
					  .Z(RdCol2Line[10]) );
 	    LATNT1 LatR7C3B10( .G(WWLW31Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR7Wire3), 
					  .Z(RdCol3Line[10]) );
 	    LATNT1 LatR7C0B11( .G(WWLW28Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[11]) );
 	    LATNT1 LatR7C1B11( .G(WWLW29Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR7Wire1), 
					  .Z(RdCol1Line[11]) );
 	    LATNT1 LatR7C2B11( .G(WWLW30Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR7Wire2), 
					  .Z(RdCol2Line[11]) );
 	    LATNT1 LatR7C3B11( .G(WWLW31Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR7Wire3), 
					  .Z(RdCol3Line[11]) );
 	    LATNT1 LatR7C0B12( .G(WWLW28Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[12]) );
 	    LATNT1 LatR7C1B12( .G(WWLW29Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR7Wire1), 
					  .Z(RdCol1Line[12]) );
 	    LATNT1 LatR7C2B12( .G(WWLW30Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR7Wire2), 
					  .Z(RdCol2Line[12]) );
 	    LATNT1 LatR7C3B12( .G(WWLW31Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR7Wire3), 
					  .Z(RdCol3Line[12]) );
 	    LATNT1 LatR7C0B13( .G(WWLW28Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[13]) );
 	    LATNT1 LatR7C1B13( .G(WWLW29Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR7Wire1), 
					  .Z(RdCol1Line[13]) );
 	    LATNT1 LatR7C2B13( .G(WWLW30Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR7Wire2), 
					  .Z(RdCol2Line[13]) );
 	    LATNT1 LatR7C3B13( .G(WWLW31Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR7Wire3), 
					  .Z(RdCol3Line[13]) );
 	    LATNT1 LatR7C0B14( .G(WWLW28Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[14]) );
 	    LATNT1 LatR7C1B14( .G(WWLW29Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR7Wire1), 
					  .Z(RdCol1Line[14]) );
 	    LATNT1 LatR7C2B14( .G(WWLW30Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR7Wire2), 
					  .Z(RdCol2Line[14]) );
 	    LATNT1 LatR7C3B14( .G(WWLW31Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR7Wire3), 
					  .Z(RdCol3Line[14]) );
 	    LATNT1 LatR7C0B15( .G(WWLW28Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[15]) );
 	    LATNT1 LatR7C1B15( .G(WWLW29Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR7Wire1), 
					  .Z(RdCol1Line[15]) );
 	    LATNT1 LatR7C2B15( .G(WWLW30Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR7Wire2), 
					  .Z(RdCol2Line[15]) );
 	    LATNT1 LatR7C3B15( .G(WWLW31Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR7Wire3), 
					  .Z(RdCol3Line[15]) );

//
//
// the psuedo sense amp (and column muxing if necessary)
	MUX4D2 rd0mx(.A0(RdCol0Line[0]), .A1(RdCol1Line[0]),
           .A2(RdCol2Line[0]), .A3(RdCol3Line[0]),
           .SL1(ColSelect[1]), .SL0(ColSelect[0]), .Z(RdDat[0]) );
	MUX4D2 rd1mx(.A0(RdCol0Line[1]), .A1(RdCol1Line[1]),
           .A2(RdCol2Line[1]), .A3(RdCol3Line[1]),
           .SL1(ColSelect[1]), .SL0(ColSelect[0]), .Z(RdDat[1]) );
	MUX4D2 rd2mx(.A0(RdCol0Line[2]), .A1(RdCol1Line[2]),
           .A2(RdCol2Line[2]), .A3(RdCol3Line[2]),
           .SL1(ColSelect[1]), .SL0(ColSelect[0]), .Z(RdDat[2]) );
	MUX4D2 rd3mx(.A0(RdCol0Line[3]), .A1(RdCol1Line[3]),
           .A2(RdCol2Line[3]), .A3(RdCol3Line[3]),
           .SL1(ColSelect[1]), .SL0(ColSelect[0]), .Z(RdDat[3]) );
	MUX4D2 rd4mx(.A0(RdCol0Line[4]), .A1(RdCol1Line[4]),
           .A2(RdCol2Line[4]), .A3(RdCol3Line[4]),
           .SL1(ColSelect[1]), .SL0(ColSelect[0]), .Z(RdDat[4]) );
	MUX4D2 rd5mx(.A0(RdCol0Line[5]), .A1(RdCol1Line[5]),
           .A2(RdCol2Line[5]), .A3(RdCol3Line[5]),
           .SL1(ColSelect[1]), .SL0(ColSelect[0]), .Z(RdDat[5]) );
	MUX4D2 rd6mx(.A0(RdCol0Line[6]), .A1(RdCol1Line[6]),
           .A2(RdCol2Line[6]), .A3(RdCol3Line[6]),
           .SL1(ColSelect[1]), .SL0(ColSelect[0]), .Z(RdDat[6]) );
	MUX4D2 rd7mx(.A0(RdCol0Line[7]), .A1(RdCol1Line[7]),
           .A2(RdCol2Line[7]), .A3(RdCol3Line[7]),
           .SL1(ColSelect[1]), .SL0(ColSelect[0]), .Z(RdDat[7]) );
	MUX4D2 rd8mx(.A0(RdCol0Line[8]), .A1(RdCol1Line[8]),
           .A2(RdCol2Line[8]), .A3(RdCol3Line[8]),
           .SL1(ColSelect[1]), .SL0(ColSelect[0]), .Z(RdDat[8]) );
	MUX4D2 rd9mx(.A0(RdCol0Line[9]), .A1(RdCol1Line[9]),
           .A2(RdCol2Line[9]), .A3(RdCol3Line[9]),
           .SL1(ColSelect[1]), .SL0(ColSelect[0]), .Z(RdDat[9]) );
	MUX4D2 rd10mx(.A0(RdCol0Line[10]), .A1(RdCol1Line[10]),
           .A2(RdCol2Line[10]), .A3(RdCol3Line[10]),
           .SL1(ColSelect[1]), .SL0(ColSelect[0]), .Z(RdDat[10]) );
	MUX4D2 rd11mx(.A0(RdCol0Line[11]), .A1(RdCol1Line[11]),
           .A2(RdCol2Line[11]), .A3(RdCol3Line[11]),
           .SL1(ColSelect[1]), .SL0(ColSelect[0]), .Z(RdDat[11]) );
	MUX4D2 rd12mx(.A0(RdCol0Line[12]), .A1(RdCol1Line[12]),
           .A2(RdCol2Line[12]), .A3(RdCol3Line[12]),
           .SL1(ColSelect[1]), .SL0(ColSelect[0]), .Z(RdDat[12]) );
	MUX4D2 rd13mx(.A0(RdCol0Line[13]), .A1(RdCol1Line[13]),
           .A2(RdCol2Line[13]), .A3(RdCol3Line[13]),
           .SL1(ColSelect[1]), .SL0(ColSelect[0]), .Z(RdDat[13]) );
	MUX4D2 rd14mx(.A0(RdCol0Line[14]), .A1(RdCol1Line[14]),
           .A2(RdCol2Line[14]), .A3(RdCol3Line[14]),
           .SL1(ColSelect[1]), .SL0(ColSelect[0]), .Z(RdDat[14]) );
	MUX4D2 rd15mx(.A0(RdCol0Line[15]), .A1(RdCol1Line[15]),
           .A2(RdCol2Line[15]), .A3(RdCol3Line[15]),
           .SL1(ColSelect[1]), .SL0(ColSelect[0]), .Z(RdDat[15]) );

   endmodule

