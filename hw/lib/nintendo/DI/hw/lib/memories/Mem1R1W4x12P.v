// This file was automatically derived from "Mem1R1W4x12P.vpp"
// using vpp on Mon May 29 03:08:26 2000.


// syn myclk = RdClk


module Mem1R1W4x12P (
	RdClk,
	WrClk,
	ScanMode,
	e1WrEn,
	e1RdAdr,
	e1WrAdr,
	e1WrDat,
	RdDat);

	input RdClk;
	input WrClk;
	input ScanMode;
	input e1WrEn;
	input [1:0] e1RdAdr, e1WrAdr;
	input [11:0] e1WrDat;
	output [11:0] RdDat;

// Begin vpp generated register declarations...
	reg [1:0] WrAdrP;
	reg [11:0] WrDat;
	reg WrEnP;
	reg [1:0] RdAdrP;
	reg [1:0] RdAdr;
// ...end vpp generated register declarations.


//
// Read Functionality...
//
// input address registers
always @(posedge RdClk) RdAdr[1:0] <= e1RdAdr;

// 
   wire [1:0] RowAdr;
   assign RowAdr[1:0] = RdAdr[1:0];

   wire [3:0] RWL, RWLB;
   assign RWL[3:0] = 4'h1 << RowAdr;

  macro_invda RWLB_0 (.Y(RWLB[0]), .A(RWL[0]));
  macro_invda RWLB_1 (.Y(RWLB[1]), .A(RWL[1]));
  macro_invda RWLB_2 (.Y(RWLB[2]), .A(RWL[2]));
  macro_invda RWLB_3 (.Y(RWLB[3]), .A(RWL[3]));

   wire ScanMode_buf;

   artx_buffer1 ScanModeBuf ( .A(ScanMode), .Y(ScanMode_buf) );

//
// Write Functionality...
//
// write address decoder happens in previous cycle
//
   wire [3:0] e1DecWrAdr, DecWrAdr;
   assign e1DecWrAdr[3:0] = (4'h1 << e1WrAdr);

   wire [3:0]WWL;

   macro_latwen latwen_0 (.wen(WE_0), .WrEn(WrEn_0), .e1WrEn(e1WrEn), .clk(WrClk));
   macro_latwl latwl_0 (.wl(WWL[0]), .DecWrAdr(DecWrAdr[0]), .e1DecWrAdr(e1DecWrAdr[0]), .wen(WE_0), .clk(WrClk), .ScanMode(ScanMode_buf));
   macro_latwl latwl_1 (.wl(WWL[1]), .DecWrAdr(DecWrAdr[1]), .e1DecWrAdr(e1DecWrAdr[1]), .wen(WE_0), .clk(WrClk), .ScanMode(ScanMode_buf));
   macro_latwl latwl_2 (.wl(WWL[2]), .DecWrAdr(DecWrAdr[2]), .e1DecWrAdr(e1DecWrAdr[2]), .wen(WE_0), .clk(WrClk), .ScanMode(ScanMode_buf));
   macro_latwl latwl_3 (.wl(WWL[3]), .DecWrAdr(DecWrAdr[3]), .e1DecWrAdr(e1DecWrAdr[3]), .wen(WE_0), .clk(WrClk), .ScanMode(ScanMode_buf));

// keep it backward compatible
wire WrEn = WrEn_0;

// input data register
always @(posedge WrClk) WrDat[11:0] <= e1WrDat;

// bypass mux
// re-register input address
always @(posedge RdClk) RdAdrP[1:0] <= e1RdAdr;
always @(posedge WrClk) WrAdrP[1:0] <= e1WrAdr;
always @(posedge WrClk) WrEnP <= e1WrEn;

   wire [11:0] RdDat_tmp;
   assign RdDat = (WrEnP & ~|(WrAdrP ^ RdAdrP))? WrDat : RdDat_tmp;

//
// this instantiates a Latch Array Submodule
// which includes any column muxing
//


   LArr4x12 LArr4x12 (
	.NullClk(),
	.WWL(WWL[3:0]),
	.WrDat(WrDat[11:0]),
	.RWL(RWLB[3:0]),
	.RdDat(RdDat_tmp[11:0])
   );
   endmodule

