// This file was automatically derived from "LArr32x4.vpp"
// using vpp on Thu May 18 23:07:10 2000.



// syn myclk = NullClk


module LArr32x4 (
	NullClk,
	WWL,
	WrDat,
	RWL,
	ColSelect,
	RdDat);

	input NullClk;
	input [31:0]WWL;
	input [3:0] WrDat;
	input [7:0]RWL;
	   input [1:0] ColSelect;
	output [3:0] RdDat;


       wire [3:0] RdCol0Line;
       wire [3:0] RdCol1Line;
       wire [3:0] RdCol2Line;
       wire [3:0] RdCol3Line;

  
            //
            // ReadLineBuffers
            //
	      
	    INVDA RWLR0Buf0 ( .A ( RWL[0] ), .Z ( RWLR0Wire0 ) );
	    INVDA RWLR1Buf0 ( .A ( RWL[1] ), .Z ( RWLR1Wire0 ) );
	    INVDA RWLR2Buf0 ( .A ( RWL[2] ), .Z ( RWLR2Wire0 ) );
	    INVDA RWLR3Buf0 ( .A ( RWL[3] ), .Z ( RWLR3Wire0 ) );
	    INVDA RWLR4Buf0 ( .A ( RWL[4] ), .Z ( RWLR4Wire0 ) );
	    INVDA RWLR5Buf0 ( .A ( RWL[5] ), .Z ( RWLR5Wire0 ) );
	    INVDA RWLR6Buf0 ( .A ( RWL[6] ), .Z ( RWLR6Wire0 ) );
	    INVDA RWLR7Buf0 ( .A ( RWL[7] ), .Z ( RWLR7Wire0 ) );
  
            //
            // WriteLineBuffers
            //
	      
	    INVD7 WWLW0Buf0 ( .A ( WWL[0] ), .Z ( WWLW0Wire0 ) );
	    INVD7 WWLW1Buf0 ( .A ( WWL[1] ), .Z ( WWLW1Wire0 ) );
	    INVD7 WWLW2Buf0 ( .A ( WWL[2] ), .Z ( WWLW2Wire0 ) );
	    INVD7 WWLW3Buf0 ( .A ( WWL[3] ), .Z ( WWLW3Wire0 ) );
	    INVD7 WWLW4Buf0 ( .A ( WWL[4] ), .Z ( WWLW4Wire0 ) );
	    INVD7 WWLW5Buf0 ( .A ( WWL[5] ), .Z ( WWLW5Wire0 ) );
	    INVD7 WWLW6Buf0 ( .A ( WWL[6] ), .Z ( WWLW6Wire0 ) );
	    INVD7 WWLW7Buf0 ( .A ( WWL[7] ), .Z ( WWLW7Wire0 ) );
	    INVD7 WWLW8Buf0 ( .A ( WWL[8] ), .Z ( WWLW8Wire0 ) );
	    INVD7 WWLW9Buf0 ( .A ( WWL[9] ), .Z ( WWLW9Wire0 ) );
	    INVD7 WWLW10Buf0 ( .A ( WWL[10] ), .Z ( WWLW10Wire0 ) );
	    INVD7 WWLW11Buf0 ( .A ( WWL[11] ), .Z ( WWLW11Wire0 ) );
	    INVD7 WWLW12Buf0 ( .A ( WWL[12] ), .Z ( WWLW12Wire0 ) );
	    INVD7 WWLW13Buf0 ( .A ( WWL[13] ), .Z ( WWLW13Wire0 ) );
	    INVD7 WWLW14Buf0 ( .A ( WWL[14] ), .Z ( WWLW14Wire0 ) );
	    INVD7 WWLW15Buf0 ( .A ( WWL[15] ), .Z ( WWLW15Wire0 ) );
	    INVD7 WWLW16Buf0 ( .A ( WWL[16] ), .Z ( WWLW16Wire0 ) );
	    INVD7 WWLW17Buf0 ( .A ( WWL[17] ), .Z ( WWLW17Wire0 ) );
	    INVD7 WWLW18Buf0 ( .A ( WWL[18] ), .Z ( WWLW18Wire0 ) );
	    INVD7 WWLW19Buf0 ( .A ( WWL[19] ), .Z ( WWLW19Wire0 ) );
	    INVD7 WWLW20Buf0 ( .A ( WWL[20] ), .Z ( WWLW20Wire0 ) );
	    INVD7 WWLW21Buf0 ( .A ( WWL[21] ), .Z ( WWLW21Wire0 ) );
	    INVD7 WWLW22Buf0 ( .A ( WWL[22] ), .Z ( WWLW22Wire0 ) );
	    INVD7 WWLW23Buf0 ( .A ( WWL[23] ), .Z ( WWLW23Wire0 ) );
	    INVD7 WWLW24Buf0 ( .A ( WWL[24] ), .Z ( WWLW24Wire0 ) );
	    INVD7 WWLW25Buf0 ( .A ( WWL[25] ), .Z ( WWLW25Wire0 ) );
	    INVD7 WWLW26Buf0 ( .A ( WWL[26] ), .Z ( WWLW26Wire0 ) );
	    INVD7 WWLW27Buf0 ( .A ( WWL[27] ), .Z ( WWLW27Wire0 ) );
	    INVD7 WWLW28Buf0 ( .A ( WWL[28] ), .Z ( WWLW28Wire0 ) );
	    INVD7 WWLW29Buf0 ( .A ( WWL[29] ), .Z ( WWLW29Wire0 ) );
	    INVD7 WWLW30Buf0 ( .A ( WWL[30] ), .Z ( WWLW30Wire0 ) );
	    INVD7 WWLW31Buf0 ( .A ( WWL[31] ), .Z ( WWLW31Wire0 ) );

//
//
// this is the array of latches itself.
     
            //
            // Row 0
	    //
	      
 	    LATNT1 LatR0C0B0( .G(WWLW0Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR0C1B0( .G(WWLW1Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol1Line[0]) );
 	    LATNT1 LatR0C2B0( .G(WWLW2Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol2Line[0]) );
 	    LATNT1 LatR0C3B0( .G(WWLW3Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol3Line[0]) );
 	    LATNT1 LatR0C0B1( .G(WWLW0Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR0C1B1( .G(WWLW1Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol1Line[1]) );
 	    LATNT1 LatR0C2B1( .G(WWLW2Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol2Line[1]) );
 	    LATNT1 LatR0C3B1( .G(WWLW3Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol3Line[1]) );
 	    LATNT1 LatR0C0B2( .G(WWLW0Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR0C1B2( .G(WWLW1Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol1Line[2]) );
 	    LATNT1 LatR0C2B2( .G(WWLW2Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol2Line[2]) );
 	    LATNT1 LatR0C3B2( .G(WWLW3Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol3Line[2]) );
 	    LATNT1 LatR0C0B3( .G(WWLW0Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR0C1B3( .G(WWLW1Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol1Line[3]) );
 	    LATNT1 LatR0C2B3( .G(WWLW2Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol2Line[3]) );
 	    LATNT1 LatR0C3B3( .G(WWLW3Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol3Line[3]) );
     
            //
            // Row 1
	    //
	      
 	    LATNT1 LatR1C0B0( .G(WWLW4Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR1C1B0( .G(WWLW5Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol1Line[0]) );
 	    LATNT1 LatR1C2B0( .G(WWLW6Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol2Line[0]) );
 	    LATNT1 LatR1C3B0( .G(WWLW7Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol3Line[0]) );
 	    LATNT1 LatR1C0B1( .G(WWLW4Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR1C1B1( .G(WWLW5Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol1Line[1]) );
 	    LATNT1 LatR1C2B1( .G(WWLW6Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol2Line[1]) );
 	    LATNT1 LatR1C3B1( .G(WWLW7Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol3Line[1]) );
 	    LATNT1 LatR1C0B2( .G(WWLW4Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR1C1B2( .G(WWLW5Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol1Line[2]) );
 	    LATNT1 LatR1C2B2( .G(WWLW6Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol2Line[2]) );
 	    LATNT1 LatR1C3B2( .G(WWLW7Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol3Line[2]) );
 	    LATNT1 LatR1C0B3( .G(WWLW4Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR1C1B3( .G(WWLW5Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol1Line[3]) );
 	    LATNT1 LatR1C2B3( .G(WWLW6Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol2Line[3]) );
 	    LATNT1 LatR1C3B3( .G(WWLW7Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol3Line[3]) );
     
            //
            // Row 2
	    //
	      
 	    LATNT1 LatR2C0B0( .G(WWLW8Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR2C1B0( .G(WWLW9Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol1Line[0]) );
 	    LATNT1 LatR2C2B0( .G(WWLW10Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol2Line[0]) );
 	    LATNT1 LatR2C3B0( .G(WWLW11Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol3Line[0]) );
 	    LATNT1 LatR2C0B1( .G(WWLW8Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR2C1B1( .G(WWLW9Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol1Line[1]) );
 	    LATNT1 LatR2C2B1( .G(WWLW10Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol2Line[1]) );
 	    LATNT1 LatR2C3B1( .G(WWLW11Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol3Line[1]) );
 	    LATNT1 LatR2C0B2( .G(WWLW8Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR2C1B2( .G(WWLW9Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol1Line[2]) );
 	    LATNT1 LatR2C2B2( .G(WWLW10Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol2Line[2]) );
 	    LATNT1 LatR2C3B2( .G(WWLW11Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol3Line[2]) );
 	    LATNT1 LatR2C0B3( .G(WWLW8Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR2C1B3( .G(WWLW9Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol1Line[3]) );
 	    LATNT1 LatR2C2B3( .G(WWLW10Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol2Line[3]) );
 	    LATNT1 LatR2C3B3( .G(WWLW11Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol3Line[3]) );
     
            //
            // Row 3
	    //
	      
 	    LATNT1 LatR3C0B0( .G(WWLW12Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR3C1B0( .G(WWLW13Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol1Line[0]) );
 	    LATNT1 LatR3C2B0( .G(WWLW14Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol2Line[0]) );
 	    LATNT1 LatR3C3B0( .G(WWLW15Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol3Line[0]) );
 	    LATNT1 LatR3C0B1( .G(WWLW12Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR3C1B1( .G(WWLW13Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol1Line[1]) );
 	    LATNT1 LatR3C2B1( .G(WWLW14Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol2Line[1]) );
 	    LATNT1 LatR3C3B1( .G(WWLW15Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol3Line[1]) );
 	    LATNT1 LatR3C0B2( .G(WWLW12Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR3C1B2( .G(WWLW13Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol1Line[2]) );
 	    LATNT1 LatR3C2B2( .G(WWLW14Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol2Line[2]) );
 	    LATNT1 LatR3C3B2( .G(WWLW15Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol3Line[2]) );
 	    LATNT1 LatR3C0B3( .G(WWLW12Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR3C1B3( .G(WWLW13Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol1Line[3]) );
 	    LATNT1 LatR3C2B3( .G(WWLW14Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol2Line[3]) );
 	    LATNT1 LatR3C3B3( .G(WWLW15Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol3Line[3]) );
     
            //
            // Row 4
	    //
	      
 	    LATNT1 LatR4C0B0( .G(WWLW16Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR4C1B0( .G(WWLW17Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol1Line[0]) );
 	    LATNT1 LatR4C2B0( .G(WWLW18Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol2Line[0]) );
 	    LATNT1 LatR4C3B0( .G(WWLW19Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol3Line[0]) );
 	    LATNT1 LatR4C0B1( .G(WWLW16Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR4C1B1( .G(WWLW17Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol1Line[1]) );
 	    LATNT1 LatR4C2B1( .G(WWLW18Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol2Line[1]) );
 	    LATNT1 LatR4C3B1( .G(WWLW19Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol3Line[1]) );
 	    LATNT1 LatR4C0B2( .G(WWLW16Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR4C1B2( .G(WWLW17Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol1Line[2]) );
 	    LATNT1 LatR4C2B2( .G(WWLW18Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol2Line[2]) );
 	    LATNT1 LatR4C3B2( .G(WWLW19Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol3Line[2]) );
 	    LATNT1 LatR4C0B3( .G(WWLW16Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR4C1B3( .G(WWLW17Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol1Line[3]) );
 	    LATNT1 LatR4C2B3( .G(WWLW18Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol2Line[3]) );
 	    LATNT1 LatR4C3B3( .G(WWLW19Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol3Line[3]) );
     
            //
            // Row 5
	    //
	      
 	    LATNT1 LatR5C0B0( .G(WWLW20Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR5C1B0( .G(WWLW21Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol1Line[0]) );
 	    LATNT1 LatR5C2B0( .G(WWLW22Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol2Line[0]) );
 	    LATNT1 LatR5C3B0( .G(WWLW23Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol3Line[0]) );
 	    LATNT1 LatR5C0B1( .G(WWLW20Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR5C1B1( .G(WWLW21Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol1Line[1]) );
 	    LATNT1 LatR5C2B1( .G(WWLW22Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol2Line[1]) );
 	    LATNT1 LatR5C3B1( .G(WWLW23Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol3Line[1]) );
 	    LATNT1 LatR5C0B2( .G(WWLW20Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR5C1B2( .G(WWLW21Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol1Line[2]) );
 	    LATNT1 LatR5C2B2( .G(WWLW22Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol2Line[2]) );
 	    LATNT1 LatR5C3B2( .G(WWLW23Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol3Line[2]) );
 	    LATNT1 LatR5C0B3( .G(WWLW20Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR5C1B3( .G(WWLW21Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol1Line[3]) );
 	    LATNT1 LatR5C2B3( .G(WWLW22Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol2Line[3]) );
 	    LATNT1 LatR5C3B3( .G(WWLW23Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol3Line[3]) );
     
            //
            // Row 6
	    //
	      
 	    LATNT1 LatR6C0B0( .G(WWLW24Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR6C1B0( .G(WWLW25Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol1Line[0]) );
 	    LATNT1 LatR6C2B0( .G(WWLW26Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol2Line[0]) );
 	    LATNT1 LatR6C3B0( .G(WWLW27Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol3Line[0]) );
 	    LATNT1 LatR6C0B1( .G(WWLW24Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR6C1B1( .G(WWLW25Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol1Line[1]) );
 	    LATNT1 LatR6C2B1( .G(WWLW26Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol2Line[1]) );
 	    LATNT1 LatR6C3B1( .G(WWLW27Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol3Line[1]) );
 	    LATNT1 LatR6C0B2( .G(WWLW24Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR6C1B2( .G(WWLW25Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol1Line[2]) );
 	    LATNT1 LatR6C2B2( .G(WWLW26Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol2Line[2]) );
 	    LATNT1 LatR6C3B2( .G(WWLW27Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol3Line[2]) );
 	    LATNT1 LatR6C0B3( .G(WWLW24Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR6C1B3( .G(WWLW25Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol1Line[3]) );
 	    LATNT1 LatR6C2B3( .G(WWLW26Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol2Line[3]) );
 	    LATNT1 LatR6C3B3( .G(WWLW27Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol3Line[3]) );
     
            //
            // Row 7
	    //
	      
 	    LATNT1 LatR7C0B0( .G(WWLW28Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR7C1B0( .G(WWLW29Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol1Line[0]) );
 	    LATNT1 LatR7C2B0( .G(WWLW30Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol2Line[0]) );
 	    LATNT1 LatR7C3B0( .G(WWLW31Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol3Line[0]) );
 	    LATNT1 LatR7C0B1( .G(WWLW28Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR7C1B1( .G(WWLW29Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol1Line[1]) );
 	    LATNT1 LatR7C2B1( .G(WWLW30Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol2Line[1]) );
 	    LATNT1 LatR7C3B1( .G(WWLW31Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol3Line[1]) );
 	    LATNT1 LatR7C0B2( .G(WWLW28Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR7C1B2( .G(WWLW29Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol1Line[2]) );
 	    LATNT1 LatR7C2B2( .G(WWLW30Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol2Line[2]) );
 	    LATNT1 LatR7C3B2( .G(WWLW31Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol3Line[2]) );
 	    LATNT1 LatR7C0B3( .G(WWLW28Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR7C1B3( .G(WWLW29Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol1Line[3]) );
 	    LATNT1 LatR7C2B3( .G(WWLW30Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol2Line[3]) );
 	    LATNT1 LatR7C3B3( .G(WWLW31Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol3Line[3]) );

//
//
// the psuedo sense amp (and column muxing if necessary)
	MUX4D2 rd0mx(.A0(RdCol0Line[0]), .A1(RdCol1Line[0]),
           .A2(RdCol2Line[0]), .A3(RdCol3Line[0]),
           .SL1(ColSelect[1]), .SL0(ColSelect[0]), .Z(RdDat[0]) );
	MUX4D2 rd1mx(.A0(RdCol0Line[1]), .A1(RdCol1Line[1]),
           .A2(RdCol2Line[1]), .A3(RdCol3Line[1]),
           .SL1(ColSelect[1]), .SL0(ColSelect[0]), .Z(RdDat[1]) );
	MUX4D2 rd2mx(.A0(RdCol0Line[2]), .A1(RdCol1Line[2]),
           .A2(RdCol2Line[2]), .A3(RdCol3Line[2]),
           .SL1(ColSelect[1]), .SL0(ColSelect[0]), .Z(RdDat[2]) );
	MUX4D2 rd3mx(.A0(RdCol0Line[3]), .A1(RdCol1Line[3]),
           .A2(RdCol2Line[3]), .A3(RdCol3Line[3]),
           .SL1(ColSelect[1]), .SL0(ColSelect[0]), .Z(RdDat[3]) );

   endmodule

