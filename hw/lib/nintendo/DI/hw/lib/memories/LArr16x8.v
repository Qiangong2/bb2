// This file was automatically derived from "LArr16x8.vpp"
// using vpp on Thu May 18 23:06:51 2000.



// syn myclk = NullClk


module LArr16x8 (
	NullClk,
	WWL,
	WrDat,
	RWL,
	ColSelect,
	RdDat);

	input NullClk;
	input [15:0]WWL;
	input [7:0] WrDat;
	input [7:0]RWL;
	   input ColSelect;
	output [7:0] RdDat;


       wire [7:0] RdCol0Line;
       wire [7:0] RdCol1Line;

  
            //
            // ReadLineBuffers
            //
	      
	    INVDA RWLR0Buf0 ( .A ( RWL[0] ), .Z ( RWLR0Wire0 ) );
	    INVDA RWLR1Buf0 ( .A ( RWL[1] ), .Z ( RWLR1Wire0 ) );
	    INVDA RWLR2Buf0 ( .A ( RWL[2] ), .Z ( RWLR2Wire0 ) );
	    INVDA RWLR3Buf0 ( .A ( RWL[3] ), .Z ( RWLR3Wire0 ) );
	    INVDA RWLR4Buf0 ( .A ( RWL[4] ), .Z ( RWLR4Wire0 ) );
	    INVDA RWLR5Buf0 ( .A ( RWL[5] ), .Z ( RWLR5Wire0 ) );
	    INVDA RWLR6Buf0 ( .A ( RWL[6] ), .Z ( RWLR6Wire0 ) );
	    INVDA RWLR7Buf0 ( .A ( RWL[7] ), .Z ( RWLR7Wire0 ) );
  
            //
            // WriteLineBuffers
            //
	      
	    INVDA WWLW0Buf0 ( .A ( WWL[0] ), .Z ( WWLW0Wire0 ) );
	    INVDA WWLW1Buf0 ( .A ( WWL[1] ), .Z ( WWLW1Wire0 ) );
	    INVDA WWLW2Buf0 ( .A ( WWL[2] ), .Z ( WWLW2Wire0 ) );
	    INVDA WWLW3Buf0 ( .A ( WWL[3] ), .Z ( WWLW3Wire0 ) );
	    INVDA WWLW4Buf0 ( .A ( WWL[4] ), .Z ( WWLW4Wire0 ) );
	    INVDA WWLW5Buf0 ( .A ( WWL[5] ), .Z ( WWLW5Wire0 ) );
	    INVDA WWLW6Buf0 ( .A ( WWL[6] ), .Z ( WWLW6Wire0 ) );
	    INVDA WWLW7Buf0 ( .A ( WWL[7] ), .Z ( WWLW7Wire0 ) );
	    INVDA WWLW8Buf0 ( .A ( WWL[8] ), .Z ( WWLW8Wire0 ) );
	    INVDA WWLW9Buf0 ( .A ( WWL[9] ), .Z ( WWLW9Wire0 ) );
	    INVDA WWLW10Buf0 ( .A ( WWL[10] ), .Z ( WWLW10Wire0 ) );
	    INVDA WWLW11Buf0 ( .A ( WWL[11] ), .Z ( WWLW11Wire0 ) );
	    INVDA WWLW12Buf0 ( .A ( WWL[12] ), .Z ( WWLW12Wire0 ) );
	    INVDA WWLW13Buf0 ( .A ( WWL[13] ), .Z ( WWLW13Wire0 ) );
	    INVDA WWLW14Buf0 ( .A ( WWL[14] ), .Z ( WWLW14Wire0 ) );
	    INVDA WWLW15Buf0 ( .A ( WWL[15] ), .Z ( WWLW15Wire0 ) );

//
//
// this is the array of latches itself.
     
            //
            // Row 0
	    //
	      
 	    LATNT1 LatR0C0B0( .G(WWLW0Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR0C1B0( .G(WWLW1Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol1Line[0]) );
 	    LATNT1 LatR0C0B1( .G(WWLW0Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR0C1B1( .G(WWLW1Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol1Line[1]) );
 	    LATNT1 LatR0C0B2( .G(WWLW0Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR0C1B2( .G(WWLW1Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol1Line[2]) );
 	    LATNT1 LatR0C0B3( .G(WWLW0Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR0C1B3( .G(WWLW1Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol1Line[3]) );
 	    LATNT1 LatR0C0B4( .G(WWLW0Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[4]) );
 	    LATNT1 LatR0C1B4( .G(WWLW1Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol1Line[4]) );
 	    LATNT1 LatR0C0B5( .G(WWLW0Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[5]) );
 	    LATNT1 LatR0C1B5( .G(WWLW1Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol1Line[5]) );
 	    LATNT1 LatR0C0B6( .G(WWLW0Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[6]) );
 	    LATNT1 LatR0C1B6( .G(WWLW1Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol1Line[6]) );
 	    LATNT1 LatR0C0B7( .G(WWLW0Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[7]) );
 	    LATNT1 LatR0C1B7( .G(WWLW1Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol1Line[7]) );
     
            //
            // Row 1
	    //
	      
 	    LATNT1 LatR1C0B0( .G(WWLW2Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR1C1B0( .G(WWLW3Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol1Line[0]) );
 	    LATNT1 LatR1C0B1( .G(WWLW2Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR1C1B1( .G(WWLW3Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol1Line[1]) );
 	    LATNT1 LatR1C0B2( .G(WWLW2Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR1C1B2( .G(WWLW3Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol1Line[2]) );
 	    LATNT1 LatR1C0B3( .G(WWLW2Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR1C1B3( .G(WWLW3Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol1Line[3]) );
 	    LATNT1 LatR1C0B4( .G(WWLW2Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[4]) );
 	    LATNT1 LatR1C1B4( .G(WWLW3Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol1Line[4]) );
 	    LATNT1 LatR1C0B5( .G(WWLW2Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[5]) );
 	    LATNT1 LatR1C1B5( .G(WWLW3Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol1Line[5]) );
 	    LATNT1 LatR1C0B6( .G(WWLW2Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[6]) );
 	    LATNT1 LatR1C1B6( .G(WWLW3Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol1Line[6]) );
 	    LATNT1 LatR1C0B7( .G(WWLW2Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[7]) );
 	    LATNT1 LatR1C1B7( .G(WWLW3Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol1Line[7]) );
     
            //
            // Row 2
	    //
	      
 	    LATNT1 LatR2C0B0( .G(WWLW4Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR2C1B0( .G(WWLW5Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol1Line[0]) );
 	    LATNT1 LatR2C0B1( .G(WWLW4Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR2C1B1( .G(WWLW5Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol1Line[1]) );
 	    LATNT1 LatR2C0B2( .G(WWLW4Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR2C1B2( .G(WWLW5Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol1Line[2]) );
 	    LATNT1 LatR2C0B3( .G(WWLW4Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR2C1B3( .G(WWLW5Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol1Line[3]) );
 	    LATNT1 LatR2C0B4( .G(WWLW4Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[4]) );
 	    LATNT1 LatR2C1B4( .G(WWLW5Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol1Line[4]) );
 	    LATNT1 LatR2C0B5( .G(WWLW4Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[5]) );
 	    LATNT1 LatR2C1B5( .G(WWLW5Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol1Line[5]) );
 	    LATNT1 LatR2C0B6( .G(WWLW4Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[6]) );
 	    LATNT1 LatR2C1B6( .G(WWLW5Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol1Line[6]) );
 	    LATNT1 LatR2C0B7( .G(WWLW4Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[7]) );
 	    LATNT1 LatR2C1B7( .G(WWLW5Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol1Line[7]) );
     
            //
            // Row 3
	    //
	      
 	    LATNT1 LatR3C0B0( .G(WWLW6Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR3C1B0( .G(WWLW7Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol1Line[0]) );
 	    LATNT1 LatR3C0B1( .G(WWLW6Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR3C1B1( .G(WWLW7Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol1Line[1]) );
 	    LATNT1 LatR3C0B2( .G(WWLW6Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR3C1B2( .G(WWLW7Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol1Line[2]) );
 	    LATNT1 LatR3C0B3( .G(WWLW6Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR3C1B3( .G(WWLW7Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol1Line[3]) );
 	    LATNT1 LatR3C0B4( .G(WWLW6Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[4]) );
 	    LATNT1 LatR3C1B4( .G(WWLW7Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol1Line[4]) );
 	    LATNT1 LatR3C0B5( .G(WWLW6Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[5]) );
 	    LATNT1 LatR3C1B5( .G(WWLW7Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol1Line[5]) );
 	    LATNT1 LatR3C0B6( .G(WWLW6Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[6]) );
 	    LATNT1 LatR3C1B6( .G(WWLW7Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol1Line[6]) );
 	    LATNT1 LatR3C0B7( .G(WWLW6Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[7]) );
 	    LATNT1 LatR3C1B7( .G(WWLW7Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol1Line[7]) );
     
            //
            // Row 4
	    //
	      
 	    LATNT1 LatR4C0B0( .G(WWLW8Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR4C1B0( .G(WWLW9Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol1Line[0]) );
 	    LATNT1 LatR4C0B1( .G(WWLW8Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR4C1B1( .G(WWLW9Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol1Line[1]) );
 	    LATNT1 LatR4C0B2( .G(WWLW8Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR4C1B2( .G(WWLW9Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol1Line[2]) );
 	    LATNT1 LatR4C0B3( .G(WWLW8Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR4C1B3( .G(WWLW9Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol1Line[3]) );
 	    LATNT1 LatR4C0B4( .G(WWLW8Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[4]) );
 	    LATNT1 LatR4C1B4( .G(WWLW9Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol1Line[4]) );
 	    LATNT1 LatR4C0B5( .G(WWLW8Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[5]) );
 	    LATNT1 LatR4C1B5( .G(WWLW9Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol1Line[5]) );
 	    LATNT1 LatR4C0B6( .G(WWLW8Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[6]) );
 	    LATNT1 LatR4C1B6( .G(WWLW9Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol1Line[6]) );
 	    LATNT1 LatR4C0B7( .G(WWLW8Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[7]) );
 	    LATNT1 LatR4C1B7( .G(WWLW9Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol1Line[7]) );
     
            //
            // Row 5
	    //
	      
 	    LATNT1 LatR5C0B0( .G(WWLW10Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR5C1B0( .G(WWLW11Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol1Line[0]) );
 	    LATNT1 LatR5C0B1( .G(WWLW10Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR5C1B1( .G(WWLW11Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol1Line[1]) );
 	    LATNT1 LatR5C0B2( .G(WWLW10Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR5C1B2( .G(WWLW11Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol1Line[2]) );
 	    LATNT1 LatR5C0B3( .G(WWLW10Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR5C1B3( .G(WWLW11Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol1Line[3]) );
 	    LATNT1 LatR5C0B4( .G(WWLW10Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[4]) );
 	    LATNT1 LatR5C1B4( .G(WWLW11Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol1Line[4]) );
 	    LATNT1 LatR5C0B5( .G(WWLW10Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[5]) );
 	    LATNT1 LatR5C1B5( .G(WWLW11Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol1Line[5]) );
 	    LATNT1 LatR5C0B6( .G(WWLW10Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[6]) );
 	    LATNT1 LatR5C1B6( .G(WWLW11Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol1Line[6]) );
 	    LATNT1 LatR5C0B7( .G(WWLW10Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[7]) );
 	    LATNT1 LatR5C1B7( .G(WWLW11Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol1Line[7]) );
     
            //
            // Row 6
	    //
	      
 	    LATNT1 LatR6C0B0( .G(WWLW12Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR6C1B0( .G(WWLW13Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol1Line[0]) );
 	    LATNT1 LatR6C0B1( .G(WWLW12Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR6C1B1( .G(WWLW13Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol1Line[1]) );
 	    LATNT1 LatR6C0B2( .G(WWLW12Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR6C1B2( .G(WWLW13Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol1Line[2]) );
 	    LATNT1 LatR6C0B3( .G(WWLW12Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR6C1B3( .G(WWLW13Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol1Line[3]) );
 	    LATNT1 LatR6C0B4( .G(WWLW12Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[4]) );
 	    LATNT1 LatR6C1B4( .G(WWLW13Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol1Line[4]) );
 	    LATNT1 LatR6C0B5( .G(WWLW12Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[5]) );
 	    LATNT1 LatR6C1B5( .G(WWLW13Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol1Line[5]) );
 	    LATNT1 LatR6C0B6( .G(WWLW12Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[6]) );
 	    LATNT1 LatR6C1B6( .G(WWLW13Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol1Line[6]) );
 	    LATNT1 LatR6C0B7( .G(WWLW12Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[7]) );
 	    LATNT1 LatR6C1B7( .G(WWLW13Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol1Line[7]) );
     
            //
            // Row 7
	    //
	      
 	    LATNT1 LatR7C0B0( .G(WWLW14Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR7C1B0( .G(WWLW15Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol1Line[0]) );
 	    LATNT1 LatR7C0B1( .G(WWLW14Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR7C1B1( .G(WWLW15Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol1Line[1]) );
 	    LATNT1 LatR7C0B2( .G(WWLW14Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR7C1B2( .G(WWLW15Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol1Line[2]) );
 	    LATNT1 LatR7C0B3( .G(WWLW14Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR7C1B3( .G(WWLW15Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol1Line[3]) );
 	    LATNT1 LatR7C0B4( .G(WWLW14Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[4]) );
 	    LATNT1 LatR7C1B4( .G(WWLW15Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol1Line[4]) );
 	    LATNT1 LatR7C0B5( .G(WWLW14Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[5]) );
 	    LATNT1 LatR7C1B5( .G(WWLW15Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol1Line[5]) );
 	    LATNT1 LatR7C0B6( .G(WWLW14Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[6]) );
 	    LATNT1 LatR7C1B6( .G(WWLW15Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol1Line[6]) );
 	    LATNT1 LatR7C0B7( .G(WWLW14Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[7]) );
 	    LATNT1 LatR7C1B7( .G(WWLW15Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol1Line[7]) );

//
//
// the psuedo sense amp (and column muxing if necessary)
	 MUX2D2 rd0mx(.A0(RdCol0Line[0]), .A1(RdCol1Line[0]),
           .SL(ColSelect), .Z(RdDat[0]) );
	 MUX2D2 rd1mx(.A0(RdCol0Line[1]), .A1(RdCol1Line[1]),
           .SL(ColSelect), .Z(RdDat[1]) );
	 MUX2D2 rd2mx(.A0(RdCol0Line[2]), .A1(RdCol1Line[2]),
           .SL(ColSelect), .Z(RdDat[2]) );
	 MUX2D2 rd3mx(.A0(RdCol0Line[3]), .A1(RdCol1Line[3]),
           .SL(ColSelect), .Z(RdDat[3]) );
	 MUX2D2 rd4mx(.A0(RdCol0Line[4]), .A1(RdCol1Line[4]),
           .SL(ColSelect), .Z(RdDat[4]) );
	 MUX2D2 rd5mx(.A0(RdCol0Line[5]), .A1(RdCol1Line[5]),
           .SL(ColSelect), .Z(RdDat[5]) );
	 MUX2D2 rd6mx(.A0(RdCol0Line[6]), .A1(RdCol1Line[6]),
           .SL(ColSelect), .Z(RdDat[6]) );
	 MUX2D2 rd7mx(.A0(RdCol0Line[7]), .A1(RdCol1Line[7]),
           .SL(ColSelect), .Z(RdDat[7]) );

   endmodule

