// This file was automatically derived from "Mem1R1W5x32P.vpp"
// using vpp on Mon May 29 03:08:27 2000.


// syn myclk = RdClk


module Mem1R1W5x32P (
	RdClk,
	WrClk,
	ScanMode,
	e1WrEn,
	e1RdAdr,
	e1WrAdr,
	e1WrDat,
	RdDat);

	input RdClk;
	input WrClk;
	input ScanMode;
	input e1WrEn;
	input [2:0] e1RdAdr, e1WrAdr;
	input [31:0] e1WrDat;
	output [31:0] RdDat;

// Begin vpp generated register declarations...
	reg [2:0] WrAdrP;
	reg [31:0] WrDat;
	reg WrEnP;
	reg [2:0] RdAdrP;
	reg [2:0] RdAdr;
// ...end vpp generated register declarations.


//
// Read Functionality...
//
// input address registers
always @(posedge RdClk) RdAdr[2:0] <= e1RdAdr;

// 
   wire [2:0] RowAdr;
   assign RowAdr[2:0] = RdAdr[2:0];

   wire [4:0] RWL, RWLB;
   assign RWL[3:0] = 4'h1 << RowAdr;
   assign RWL[4] = (RowAdr >= 4);

  macro_invda RWLB_0 (.Y(RWLB[0]), .A(RWL[0]));
  macro_invda RWLB_1 (.Y(RWLB[1]), .A(RWL[1]));
  macro_invda RWLB_2 (.Y(RWLB[2]), .A(RWL[2]));
  macro_invda RWLB_3 (.Y(RWLB[3]), .A(RWL[3]));
  macro_invda RWLB_4 (.Y(RWLB[4]), .A(RWL[4]));

   wire ScanMode_buf;

   artx_buffer1 ScanModeBuf ( .A(ScanMode), .Y(ScanMode_buf) );

//
// Write Functionality...
//
// write address decoder happens in previous cycle
//
   wire [4:0] e1DecWrAdr, DecWrAdr;
   assign e1DecWrAdr[4:0] = (5'h1 << e1WrAdr);

   wire [4:0]WWL;

   macro_latwen latwen_0 (.wen(WE_0), .WrEn(WrEn_0), .e1WrEn(e1WrEn), .clk(WrClk));
   macro_latwl latwl_0 (.wl(WWL[0]), .DecWrAdr(DecWrAdr[0]), .e1DecWrAdr(e1DecWrAdr[0]), .wen(WE_0), .clk(WrClk), .ScanMode(ScanMode_buf));
   macro_latwl latwl_1 (.wl(WWL[1]), .DecWrAdr(DecWrAdr[1]), .e1DecWrAdr(e1DecWrAdr[1]), .wen(WE_0), .clk(WrClk), .ScanMode(ScanMode_buf));
   macro_latwl latwl_2 (.wl(WWL[2]), .DecWrAdr(DecWrAdr[2]), .e1DecWrAdr(e1DecWrAdr[2]), .wen(WE_0), .clk(WrClk), .ScanMode(ScanMode_buf));
   macro_latwl latwl_3 (.wl(WWL[3]), .DecWrAdr(DecWrAdr[3]), .e1DecWrAdr(e1DecWrAdr[3]), .wen(WE_0), .clk(WrClk), .ScanMode(ScanMode_buf));
   macro_latwl latwl_4 (.wl(WWL[4]), .DecWrAdr(DecWrAdr[4]), .e1DecWrAdr(e1DecWrAdr[4]), .wen(WE_0), .clk(WrClk), .ScanMode(ScanMode_buf));

// keep it backward compatible
wire WrEn = WrEn_0;

// input data register
always @(posedge WrClk) WrDat[31:0] <= e1WrDat;

// bypass mux
// re-register input address
always @(posedge RdClk) RdAdrP[2:0] <= e1RdAdr;
always @(posedge WrClk) WrAdrP[2:0] <= e1WrAdr;
always @(posedge WrClk) WrEnP <= e1WrEn;

   wire [31:0] RdDat_tmp;
   assign RdDat = (WrEnP & ~|(WrAdrP ^ RdAdrP))? WrDat : RdDat_tmp;

//
// this instantiates a Latch Array Submodule
// which includes any column muxing
//


   LArr5x32 LArr5x32 (
	.NullClk(),
	.WWL(WWL[4:0]),
	.WrDat(WrDat[31:0]),
	.RWL(RWLB[4:0]),
	.RdDat(RdDat_tmp[31:0])
   );
   endmodule

