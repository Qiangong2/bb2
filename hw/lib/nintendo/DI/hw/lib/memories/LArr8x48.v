// This file was automatically derived from "LArr8x48.vpp"
// using vpp on Thu May 18 23:07:08 2000.



// syn myclk = NullClk


module LArr8x48 (
	NullClk,
	WWL,
	WrDat,
	RWL,
	RdDat);

	input NullClk;
	input [7:0]WWL;
	input [47:0] WrDat;
	input [7:0]RWL;
	output [47:0] RdDat;


       wire [47:0] RdCol0Line;

  
            //
            // ReadLineBuffers
            //
	      
	    INVDA RWLR0Buf0 ( .A ( RWL[0] ), .Z ( RWLR0Wire0 ) );
	    INVDA RWLR0Buf1 ( .A ( RWL[0] ), .Z ( RWLR0Wire1 ) );
	    INVDA RWLR0Buf2 ( .A ( RWL[0] ), .Z ( RWLR0Wire2 ) );
	    INVDA RWLR1Buf0 ( .A ( RWL[1] ), .Z ( RWLR1Wire0 ) );
	    INVDA RWLR1Buf1 ( .A ( RWL[1] ), .Z ( RWLR1Wire1 ) );
	    INVDA RWLR1Buf2 ( .A ( RWL[1] ), .Z ( RWLR1Wire2 ) );
	    INVDA RWLR2Buf0 ( .A ( RWL[2] ), .Z ( RWLR2Wire0 ) );
	    INVDA RWLR2Buf1 ( .A ( RWL[2] ), .Z ( RWLR2Wire1 ) );
	    INVDA RWLR2Buf2 ( .A ( RWL[2] ), .Z ( RWLR2Wire2 ) );
	    INVDA RWLR3Buf0 ( .A ( RWL[3] ), .Z ( RWLR3Wire0 ) );
	    INVDA RWLR3Buf1 ( .A ( RWL[3] ), .Z ( RWLR3Wire1 ) );
	    INVDA RWLR3Buf2 ( .A ( RWL[3] ), .Z ( RWLR3Wire2 ) );
	    INVDA RWLR4Buf0 ( .A ( RWL[4] ), .Z ( RWLR4Wire0 ) );
	    INVDA RWLR4Buf1 ( .A ( RWL[4] ), .Z ( RWLR4Wire1 ) );
	    INVDA RWLR4Buf2 ( .A ( RWL[4] ), .Z ( RWLR4Wire2 ) );
	    INVDA RWLR5Buf0 ( .A ( RWL[5] ), .Z ( RWLR5Wire0 ) );
	    INVDA RWLR5Buf1 ( .A ( RWL[5] ), .Z ( RWLR5Wire1 ) );
	    INVDA RWLR5Buf2 ( .A ( RWL[5] ), .Z ( RWLR5Wire2 ) );
	    INVDA RWLR6Buf0 ( .A ( RWL[6] ), .Z ( RWLR6Wire0 ) );
	    INVDA RWLR6Buf1 ( .A ( RWL[6] ), .Z ( RWLR6Wire1 ) );
	    INVDA RWLR6Buf2 ( .A ( RWL[6] ), .Z ( RWLR6Wire2 ) );
	    INVDA RWLR7Buf0 ( .A ( RWL[7] ), .Z ( RWLR7Wire0 ) );
	    INVDA RWLR7Buf1 ( .A ( RWL[7] ), .Z ( RWLR7Wire1 ) );
	    INVDA RWLR7Buf2 ( .A ( RWL[7] ), .Z ( RWLR7Wire2 ) );
  
            //
            // WriteLineBuffers
            //
	      
	    INVDA WWLW0Buf0 ( .A ( WWL[0] ), .Z ( WWLW0Wire0 ) );
	    INVDA WWLW0Buf1 ( .A ( WWL[0] ), .Z ( WWLW0Wire1 ) );
	    INVDA WWLW0Buf2 ( .A ( WWL[0] ), .Z ( WWLW0Wire2 ) );
	    INVDA WWLW1Buf0 ( .A ( WWL[1] ), .Z ( WWLW1Wire0 ) );
	    INVDA WWLW1Buf1 ( .A ( WWL[1] ), .Z ( WWLW1Wire1 ) );
	    INVDA WWLW1Buf2 ( .A ( WWL[1] ), .Z ( WWLW1Wire2 ) );
	    INVDA WWLW2Buf0 ( .A ( WWL[2] ), .Z ( WWLW2Wire0 ) );
	    INVDA WWLW2Buf1 ( .A ( WWL[2] ), .Z ( WWLW2Wire1 ) );
	    INVDA WWLW2Buf2 ( .A ( WWL[2] ), .Z ( WWLW2Wire2 ) );
	    INVDA WWLW3Buf0 ( .A ( WWL[3] ), .Z ( WWLW3Wire0 ) );
	    INVDA WWLW3Buf1 ( .A ( WWL[3] ), .Z ( WWLW3Wire1 ) );
	    INVDA WWLW3Buf2 ( .A ( WWL[3] ), .Z ( WWLW3Wire2 ) );
	    INVDA WWLW4Buf0 ( .A ( WWL[4] ), .Z ( WWLW4Wire0 ) );
	    INVDA WWLW4Buf1 ( .A ( WWL[4] ), .Z ( WWLW4Wire1 ) );
	    INVDA WWLW4Buf2 ( .A ( WWL[4] ), .Z ( WWLW4Wire2 ) );
	    INVDA WWLW5Buf0 ( .A ( WWL[5] ), .Z ( WWLW5Wire0 ) );
	    INVDA WWLW5Buf1 ( .A ( WWL[5] ), .Z ( WWLW5Wire1 ) );
	    INVDA WWLW5Buf2 ( .A ( WWL[5] ), .Z ( WWLW5Wire2 ) );
	    INVDA WWLW6Buf0 ( .A ( WWL[6] ), .Z ( WWLW6Wire0 ) );
	    INVDA WWLW6Buf1 ( .A ( WWL[6] ), .Z ( WWLW6Wire1 ) );
	    INVDA WWLW6Buf2 ( .A ( WWL[6] ), .Z ( WWLW6Wire2 ) );
	    INVDA WWLW7Buf0 ( .A ( WWL[7] ), .Z ( WWLW7Wire0 ) );
	    INVDA WWLW7Buf1 ( .A ( WWL[7] ), .Z ( WWLW7Wire1 ) );
	    INVDA WWLW7Buf2 ( .A ( WWL[7] ), .Z ( WWLW7Wire2 ) );

//
//
// this is the array of latches itself.
     
            //
            // Row 0
	    //
	      
 	    LATNT1 LatR0C0B0( .G(WWLW0Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR0C0B1( .G(WWLW0Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR0C0B2( .G(WWLW0Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR0C0B3( .G(WWLW0Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR0C0B4( .G(WWLW0Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[4]) );
 	    LATNT1 LatR0C0B5( .G(WWLW0Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[5]) );
 	    LATNT1 LatR0C0B6( .G(WWLW0Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[6]) );
 	    LATNT1 LatR0C0B7( .G(WWLW0Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[7]) );
 	    LATNT1 LatR0C0B8( .G(WWLW0Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[8]) );
 	    LATNT1 LatR0C0B9( .G(WWLW0Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[9]) );
 	    LATNT1 LatR0C0B10( .G(WWLW0Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[10]) );
 	    LATNT1 LatR0C0B11( .G(WWLW0Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[11]) );
 	    LATNT1 LatR0C0B12( .G(WWLW0Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[12]) );
 	    LATNT1 LatR0C0B13( .G(WWLW0Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[13]) );
 	    LATNT1 LatR0C0B14( .G(WWLW0Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[14]) );
 	    LATNT1 LatR0C0B15( .G(WWLW0Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[15]) );
 	    LATNT1 LatR0C0B16( .G(WWLW0Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[16]) );
 	    LATNT1 LatR0C0B17( .G(WWLW0Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[17]) );
 	    LATNT1 LatR0C0B18( .G(WWLW0Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[18]) );
 	    LATNT1 LatR0C0B19( .G(WWLW0Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[19]) );
 	    LATNT1 LatR0C0B20( .G(WWLW0Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[20]) );
 	    LATNT1 LatR0C0B21( .G(WWLW0Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[21]) );
 	    LATNT1 LatR0C0B22( .G(WWLW0Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[22]) );
 	    LATNT1 LatR0C0B23( .G(WWLW0Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[23]) );
 	    LATNT1 LatR0C0B24( .G(WWLW0Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[24]) );
 	    LATNT1 LatR0C0B25( .G(WWLW0Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[25]) );
 	    LATNT1 LatR0C0B26( .G(WWLW0Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[26]) );
 	    LATNT1 LatR0C0B27( .G(WWLW0Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[27]) );
 	    LATNT1 LatR0C0B28( .G(WWLW0Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[28]) );
 	    LATNT1 LatR0C0B29( .G(WWLW0Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[29]) );
 	    LATNT1 LatR0C0B30( .G(WWLW0Wire1), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[30]) );
 	    LATNT1 LatR0C0B31( .G(WWLW0Wire1), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[31]) );
 	    LATNT1 LatR0C0B32( .G(WWLW0Wire2), 
					  .D(WrDat[32]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol0Line[32]) );
 	    LATNT1 LatR0C0B33( .G(WWLW0Wire2), 
					  .D(WrDat[33]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol0Line[33]) );
 	    LATNT1 LatR0C0B34( .G(WWLW0Wire2), 
					  .D(WrDat[34]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol0Line[34]) );
 	    LATNT1 LatR0C0B35( .G(WWLW0Wire2), 
					  .D(WrDat[35]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol0Line[35]) );
 	    LATNT1 LatR0C0B36( .G(WWLW0Wire2), 
					  .D(WrDat[36]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol0Line[36]) );
 	    LATNT1 LatR0C0B37( .G(WWLW0Wire2), 
					  .D(WrDat[37]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol0Line[37]) );
 	    LATNT1 LatR0C0B38( .G(WWLW0Wire2), 
					  .D(WrDat[38]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol0Line[38]) );
 	    LATNT1 LatR0C0B39( .G(WWLW0Wire2), 
					  .D(WrDat[39]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol0Line[39]) );
 	    LATNT1 LatR0C0B40( .G(WWLW0Wire2), 
					  .D(WrDat[40]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol0Line[40]) );
 	    LATNT1 LatR0C0B41( .G(WWLW0Wire2), 
					  .D(WrDat[41]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol0Line[41]) );
 	    LATNT1 LatR0C0B42( .G(WWLW0Wire2), 
					  .D(WrDat[42]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol0Line[42]) );
 	    LATNT1 LatR0C0B43( .G(WWLW0Wire2), 
					  .D(WrDat[43]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol0Line[43]) );
 	    LATNT1 LatR0C0B44( .G(WWLW0Wire2), 
					  .D(WrDat[44]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol0Line[44]) );
 	    LATNT1 LatR0C0B45( .G(WWLW0Wire2), 
					  .D(WrDat[45]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol0Line[45]) );
 	    LATNT1 LatR0C0B46( .G(WWLW0Wire2), 
					  .D(WrDat[46]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol0Line[46]) );
 	    LATNT1 LatR0C0B47( .G(WWLW0Wire2), 
					  .D(WrDat[47]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol0Line[47]) );
     
            //
            // Row 1
	    //
	      
 	    LATNT1 LatR1C0B0( .G(WWLW1Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR1C0B1( .G(WWLW1Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR1C0B2( .G(WWLW1Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR1C0B3( .G(WWLW1Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR1C0B4( .G(WWLW1Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[4]) );
 	    LATNT1 LatR1C0B5( .G(WWLW1Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[5]) );
 	    LATNT1 LatR1C0B6( .G(WWLW1Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[6]) );
 	    LATNT1 LatR1C0B7( .G(WWLW1Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[7]) );
 	    LATNT1 LatR1C0B8( .G(WWLW1Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[8]) );
 	    LATNT1 LatR1C0B9( .G(WWLW1Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[9]) );
 	    LATNT1 LatR1C0B10( .G(WWLW1Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[10]) );
 	    LATNT1 LatR1C0B11( .G(WWLW1Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[11]) );
 	    LATNT1 LatR1C0B12( .G(WWLW1Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[12]) );
 	    LATNT1 LatR1C0B13( .G(WWLW1Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[13]) );
 	    LATNT1 LatR1C0B14( .G(WWLW1Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[14]) );
 	    LATNT1 LatR1C0B15( .G(WWLW1Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[15]) );
 	    LATNT1 LatR1C0B16( .G(WWLW1Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[16]) );
 	    LATNT1 LatR1C0B17( .G(WWLW1Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[17]) );
 	    LATNT1 LatR1C0B18( .G(WWLW1Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[18]) );
 	    LATNT1 LatR1C0B19( .G(WWLW1Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[19]) );
 	    LATNT1 LatR1C0B20( .G(WWLW1Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[20]) );
 	    LATNT1 LatR1C0B21( .G(WWLW1Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[21]) );
 	    LATNT1 LatR1C0B22( .G(WWLW1Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[22]) );
 	    LATNT1 LatR1C0B23( .G(WWLW1Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[23]) );
 	    LATNT1 LatR1C0B24( .G(WWLW1Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[24]) );
 	    LATNT1 LatR1C0B25( .G(WWLW1Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[25]) );
 	    LATNT1 LatR1C0B26( .G(WWLW1Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[26]) );
 	    LATNT1 LatR1C0B27( .G(WWLW1Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[27]) );
 	    LATNT1 LatR1C0B28( .G(WWLW1Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[28]) );
 	    LATNT1 LatR1C0B29( .G(WWLW1Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[29]) );
 	    LATNT1 LatR1C0B30( .G(WWLW1Wire1), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[30]) );
 	    LATNT1 LatR1C0B31( .G(WWLW1Wire1), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[31]) );
 	    LATNT1 LatR1C0B32( .G(WWLW1Wire2), 
					  .D(WrDat[32]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol0Line[32]) );
 	    LATNT1 LatR1C0B33( .G(WWLW1Wire2), 
					  .D(WrDat[33]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol0Line[33]) );
 	    LATNT1 LatR1C0B34( .G(WWLW1Wire2), 
					  .D(WrDat[34]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol0Line[34]) );
 	    LATNT1 LatR1C0B35( .G(WWLW1Wire2), 
					  .D(WrDat[35]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol0Line[35]) );
 	    LATNT1 LatR1C0B36( .G(WWLW1Wire2), 
					  .D(WrDat[36]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol0Line[36]) );
 	    LATNT1 LatR1C0B37( .G(WWLW1Wire2), 
					  .D(WrDat[37]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol0Line[37]) );
 	    LATNT1 LatR1C0B38( .G(WWLW1Wire2), 
					  .D(WrDat[38]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol0Line[38]) );
 	    LATNT1 LatR1C0B39( .G(WWLW1Wire2), 
					  .D(WrDat[39]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol0Line[39]) );
 	    LATNT1 LatR1C0B40( .G(WWLW1Wire2), 
					  .D(WrDat[40]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol0Line[40]) );
 	    LATNT1 LatR1C0B41( .G(WWLW1Wire2), 
					  .D(WrDat[41]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol0Line[41]) );
 	    LATNT1 LatR1C0B42( .G(WWLW1Wire2), 
					  .D(WrDat[42]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol0Line[42]) );
 	    LATNT1 LatR1C0B43( .G(WWLW1Wire2), 
					  .D(WrDat[43]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol0Line[43]) );
 	    LATNT1 LatR1C0B44( .G(WWLW1Wire2), 
					  .D(WrDat[44]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol0Line[44]) );
 	    LATNT1 LatR1C0B45( .G(WWLW1Wire2), 
					  .D(WrDat[45]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol0Line[45]) );
 	    LATNT1 LatR1C0B46( .G(WWLW1Wire2), 
					  .D(WrDat[46]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol0Line[46]) );
 	    LATNT1 LatR1C0B47( .G(WWLW1Wire2), 
					  .D(WrDat[47]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol0Line[47]) );
     
            //
            // Row 2
	    //
	      
 	    LATNT1 LatR2C0B0( .G(WWLW2Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR2C0B1( .G(WWLW2Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR2C0B2( .G(WWLW2Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR2C0B3( .G(WWLW2Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR2C0B4( .G(WWLW2Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[4]) );
 	    LATNT1 LatR2C0B5( .G(WWLW2Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[5]) );
 	    LATNT1 LatR2C0B6( .G(WWLW2Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[6]) );
 	    LATNT1 LatR2C0B7( .G(WWLW2Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[7]) );
 	    LATNT1 LatR2C0B8( .G(WWLW2Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[8]) );
 	    LATNT1 LatR2C0B9( .G(WWLW2Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[9]) );
 	    LATNT1 LatR2C0B10( .G(WWLW2Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[10]) );
 	    LATNT1 LatR2C0B11( .G(WWLW2Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[11]) );
 	    LATNT1 LatR2C0B12( .G(WWLW2Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[12]) );
 	    LATNT1 LatR2C0B13( .G(WWLW2Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[13]) );
 	    LATNT1 LatR2C0B14( .G(WWLW2Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[14]) );
 	    LATNT1 LatR2C0B15( .G(WWLW2Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[15]) );
 	    LATNT1 LatR2C0B16( .G(WWLW2Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[16]) );
 	    LATNT1 LatR2C0B17( .G(WWLW2Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[17]) );
 	    LATNT1 LatR2C0B18( .G(WWLW2Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[18]) );
 	    LATNT1 LatR2C0B19( .G(WWLW2Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[19]) );
 	    LATNT1 LatR2C0B20( .G(WWLW2Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[20]) );
 	    LATNT1 LatR2C0B21( .G(WWLW2Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[21]) );
 	    LATNT1 LatR2C0B22( .G(WWLW2Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[22]) );
 	    LATNT1 LatR2C0B23( .G(WWLW2Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[23]) );
 	    LATNT1 LatR2C0B24( .G(WWLW2Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[24]) );
 	    LATNT1 LatR2C0B25( .G(WWLW2Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[25]) );
 	    LATNT1 LatR2C0B26( .G(WWLW2Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[26]) );
 	    LATNT1 LatR2C0B27( .G(WWLW2Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[27]) );
 	    LATNT1 LatR2C0B28( .G(WWLW2Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[28]) );
 	    LATNT1 LatR2C0B29( .G(WWLW2Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[29]) );
 	    LATNT1 LatR2C0B30( .G(WWLW2Wire1), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[30]) );
 	    LATNT1 LatR2C0B31( .G(WWLW2Wire1), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[31]) );
 	    LATNT1 LatR2C0B32( .G(WWLW2Wire2), 
					  .D(WrDat[32]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol0Line[32]) );
 	    LATNT1 LatR2C0B33( .G(WWLW2Wire2), 
					  .D(WrDat[33]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol0Line[33]) );
 	    LATNT1 LatR2C0B34( .G(WWLW2Wire2), 
					  .D(WrDat[34]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol0Line[34]) );
 	    LATNT1 LatR2C0B35( .G(WWLW2Wire2), 
					  .D(WrDat[35]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol0Line[35]) );
 	    LATNT1 LatR2C0B36( .G(WWLW2Wire2), 
					  .D(WrDat[36]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol0Line[36]) );
 	    LATNT1 LatR2C0B37( .G(WWLW2Wire2), 
					  .D(WrDat[37]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol0Line[37]) );
 	    LATNT1 LatR2C0B38( .G(WWLW2Wire2), 
					  .D(WrDat[38]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol0Line[38]) );
 	    LATNT1 LatR2C0B39( .G(WWLW2Wire2), 
					  .D(WrDat[39]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol0Line[39]) );
 	    LATNT1 LatR2C0B40( .G(WWLW2Wire2), 
					  .D(WrDat[40]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol0Line[40]) );
 	    LATNT1 LatR2C0B41( .G(WWLW2Wire2), 
					  .D(WrDat[41]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol0Line[41]) );
 	    LATNT1 LatR2C0B42( .G(WWLW2Wire2), 
					  .D(WrDat[42]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol0Line[42]) );
 	    LATNT1 LatR2C0B43( .G(WWLW2Wire2), 
					  .D(WrDat[43]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol0Line[43]) );
 	    LATNT1 LatR2C0B44( .G(WWLW2Wire2), 
					  .D(WrDat[44]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol0Line[44]) );
 	    LATNT1 LatR2C0B45( .G(WWLW2Wire2), 
					  .D(WrDat[45]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol0Line[45]) );
 	    LATNT1 LatR2C0B46( .G(WWLW2Wire2), 
					  .D(WrDat[46]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol0Line[46]) );
 	    LATNT1 LatR2C0B47( .G(WWLW2Wire2), 
					  .D(WrDat[47]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol0Line[47]) );
     
            //
            // Row 3
	    //
	      
 	    LATNT1 LatR3C0B0( .G(WWLW3Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR3C0B1( .G(WWLW3Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR3C0B2( .G(WWLW3Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR3C0B3( .G(WWLW3Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR3C0B4( .G(WWLW3Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[4]) );
 	    LATNT1 LatR3C0B5( .G(WWLW3Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[5]) );
 	    LATNT1 LatR3C0B6( .G(WWLW3Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[6]) );
 	    LATNT1 LatR3C0B7( .G(WWLW3Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[7]) );
 	    LATNT1 LatR3C0B8( .G(WWLW3Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[8]) );
 	    LATNT1 LatR3C0B9( .G(WWLW3Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[9]) );
 	    LATNT1 LatR3C0B10( .G(WWLW3Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[10]) );
 	    LATNT1 LatR3C0B11( .G(WWLW3Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[11]) );
 	    LATNT1 LatR3C0B12( .G(WWLW3Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[12]) );
 	    LATNT1 LatR3C0B13( .G(WWLW3Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[13]) );
 	    LATNT1 LatR3C0B14( .G(WWLW3Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[14]) );
 	    LATNT1 LatR3C0B15( .G(WWLW3Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[15]) );
 	    LATNT1 LatR3C0B16( .G(WWLW3Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[16]) );
 	    LATNT1 LatR3C0B17( .G(WWLW3Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[17]) );
 	    LATNT1 LatR3C0B18( .G(WWLW3Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[18]) );
 	    LATNT1 LatR3C0B19( .G(WWLW3Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[19]) );
 	    LATNT1 LatR3C0B20( .G(WWLW3Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[20]) );
 	    LATNT1 LatR3C0B21( .G(WWLW3Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[21]) );
 	    LATNT1 LatR3C0B22( .G(WWLW3Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[22]) );
 	    LATNT1 LatR3C0B23( .G(WWLW3Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[23]) );
 	    LATNT1 LatR3C0B24( .G(WWLW3Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[24]) );
 	    LATNT1 LatR3C0B25( .G(WWLW3Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[25]) );
 	    LATNT1 LatR3C0B26( .G(WWLW3Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[26]) );
 	    LATNT1 LatR3C0B27( .G(WWLW3Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[27]) );
 	    LATNT1 LatR3C0B28( .G(WWLW3Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[28]) );
 	    LATNT1 LatR3C0B29( .G(WWLW3Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[29]) );
 	    LATNT1 LatR3C0B30( .G(WWLW3Wire1), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[30]) );
 	    LATNT1 LatR3C0B31( .G(WWLW3Wire1), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[31]) );
 	    LATNT1 LatR3C0B32( .G(WWLW3Wire2), 
					  .D(WrDat[32]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol0Line[32]) );
 	    LATNT1 LatR3C0B33( .G(WWLW3Wire2), 
					  .D(WrDat[33]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol0Line[33]) );
 	    LATNT1 LatR3C0B34( .G(WWLW3Wire2), 
					  .D(WrDat[34]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol0Line[34]) );
 	    LATNT1 LatR3C0B35( .G(WWLW3Wire2), 
					  .D(WrDat[35]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol0Line[35]) );
 	    LATNT1 LatR3C0B36( .G(WWLW3Wire2), 
					  .D(WrDat[36]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol0Line[36]) );
 	    LATNT1 LatR3C0B37( .G(WWLW3Wire2), 
					  .D(WrDat[37]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol0Line[37]) );
 	    LATNT1 LatR3C0B38( .G(WWLW3Wire2), 
					  .D(WrDat[38]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol0Line[38]) );
 	    LATNT1 LatR3C0B39( .G(WWLW3Wire2), 
					  .D(WrDat[39]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol0Line[39]) );
 	    LATNT1 LatR3C0B40( .G(WWLW3Wire2), 
					  .D(WrDat[40]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol0Line[40]) );
 	    LATNT1 LatR3C0B41( .G(WWLW3Wire2), 
					  .D(WrDat[41]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol0Line[41]) );
 	    LATNT1 LatR3C0B42( .G(WWLW3Wire2), 
					  .D(WrDat[42]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol0Line[42]) );
 	    LATNT1 LatR3C0B43( .G(WWLW3Wire2), 
					  .D(WrDat[43]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol0Line[43]) );
 	    LATNT1 LatR3C0B44( .G(WWLW3Wire2), 
					  .D(WrDat[44]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol0Line[44]) );
 	    LATNT1 LatR3C0B45( .G(WWLW3Wire2), 
					  .D(WrDat[45]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol0Line[45]) );
 	    LATNT1 LatR3C0B46( .G(WWLW3Wire2), 
					  .D(WrDat[46]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol0Line[46]) );
 	    LATNT1 LatR3C0B47( .G(WWLW3Wire2), 
					  .D(WrDat[47]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol0Line[47]) );
     
            //
            // Row 4
	    //
	      
 	    LATNT1 LatR4C0B0( .G(WWLW4Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR4C0B1( .G(WWLW4Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR4C0B2( .G(WWLW4Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR4C0B3( .G(WWLW4Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR4C0B4( .G(WWLW4Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[4]) );
 	    LATNT1 LatR4C0B5( .G(WWLW4Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[5]) );
 	    LATNT1 LatR4C0B6( .G(WWLW4Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[6]) );
 	    LATNT1 LatR4C0B7( .G(WWLW4Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[7]) );
 	    LATNT1 LatR4C0B8( .G(WWLW4Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[8]) );
 	    LATNT1 LatR4C0B9( .G(WWLW4Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[9]) );
 	    LATNT1 LatR4C0B10( .G(WWLW4Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[10]) );
 	    LATNT1 LatR4C0B11( .G(WWLW4Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[11]) );
 	    LATNT1 LatR4C0B12( .G(WWLW4Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[12]) );
 	    LATNT1 LatR4C0B13( .G(WWLW4Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[13]) );
 	    LATNT1 LatR4C0B14( .G(WWLW4Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[14]) );
 	    LATNT1 LatR4C0B15( .G(WWLW4Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[15]) );
 	    LATNT1 LatR4C0B16( .G(WWLW4Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[16]) );
 	    LATNT1 LatR4C0B17( .G(WWLW4Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[17]) );
 	    LATNT1 LatR4C0B18( .G(WWLW4Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[18]) );
 	    LATNT1 LatR4C0B19( .G(WWLW4Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[19]) );
 	    LATNT1 LatR4C0B20( .G(WWLW4Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[20]) );
 	    LATNT1 LatR4C0B21( .G(WWLW4Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[21]) );
 	    LATNT1 LatR4C0B22( .G(WWLW4Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[22]) );
 	    LATNT1 LatR4C0B23( .G(WWLW4Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[23]) );
 	    LATNT1 LatR4C0B24( .G(WWLW4Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[24]) );
 	    LATNT1 LatR4C0B25( .G(WWLW4Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[25]) );
 	    LATNT1 LatR4C0B26( .G(WWLW4Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[26]) );
 	    LATNT1 LatR4C0B27( .G(WWLW4Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[27]) );
 	    LATNT1 LatR4C0B28( .G(WWLW4Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[28]) );
 	    LATNT1 LatR4C0B29( .G(WWLW4Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[29]) );
 	    LATNT1 LatR4C0B30( .G(WWLW4Wire1), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[30]) );
 	    LATNT1 LatR4C0B31( .G(WWLW4Wire1), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[31]) );
 	    LATNT1 LatR4C0B32( .G(WWLW4Wire2), 
					  .D(WrDat[32]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol0Line[32]) );
 	    LATNT1 LatR4C0B33( .G(WWLW4Wire2), 
					  .D(WrDat[33]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol0Line[33]) );
 	    LATNT1 LatR4C0B34( .G(WWLW4Wire2), 
					  .D(WrDat[34]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol0Line[34]) );
 	    LATNT1 LatR4C0B35( .G(WWLW4Wire2), 
					  .D(WrDat[35]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol0Line[35]) );
 	    LATNT1 LatR4C0B36( .G(WWLW4Wire2), 
					  .D(WrDat[36]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol0Line[36]) );
 	    LATNT1 LatR4C0B37( .G(WWLW4Wire2), 
					  .D(WrDat[37]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol0Line[37]) );
 	    LATNT1 LatR4C0B38( .G(WWLW4Wire2), 
					  .D(WrDat[38]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol0Line[38]) );
 	    LATNT1 LatR4C0B39( .G(WWLW4Wire2), 
					  .D(WrDat[39]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol0Line[39]) );
 	    LATNT1 LatR4C0B40( .G(WWLW4Wire2), 
					  .D(WrDat[40]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol0Line[40]) );
 	    LATNT1 LatR4C0B41( .G(WWLW4Wire2), 
					  .D(WrDat[41]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol0Line[41]) );
 	    LATNT1 LatR4C0B42( .G(WWLW4Wire2), 
					  .D(WrDat[42]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol0Line[42]) );
 	    LATNT1 LatR4C0B43( .G(WWLW4Wire2), 
					  .D(WrDat[43]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol0Line[43]) );
 	    LATNT1 LatR4C0B44( .G(WWLW4Wire2), 
					  .D(WrDat[44]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol0Line[44]) );
 	    LATNT1 LatR4C0B45( .G(WWLW4Wire2), 
					  .D(WrDat[45]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol0Line[45]) );
 	    LATNT1 LatR4C0B46( .G(WWLW4Wire2), 
					  .D(WrDat[46]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol0Line[46]) );
 	    LATNT1 LatR4C0B47( .G(WWLW4Wire2), 
					  .D(WrDat[47]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol0Line[47]) );
     
            //
            // Row 5
	    //
	      
 	    LATNT1 LatR5C0B0( .G(WWLW5Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR5C0B1( .G(WWLW5Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR5C0B2( .G(WWLW5Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR5C0B3( .G(WWLW5Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR5C0B4( .G(WWLW5Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[4]) );
 	    LATNT1 LatR5C0B5( .G(WWLW5Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[5]) );
 	    LATNT1 LatR5C0B6( .G(WWLW5Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[6]) );
 	    LATNT1 LatR5C0B7( .G(WWLW5Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[7]) );
 	    LATNT1 LatR5C0B8( .G(WWLW5Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[8]) );
 	    LATNT1 LatR5C0B9( .G(WWLW5Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[9]) );
 	    LATNT1 LatR5C0B10( .G(WWLW5Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[10]) );
 	    LATNT1 LatR5C0B11( .G(WWLW5Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[11]) );
 	    LATNT1 LatR5C0B12( .G(WWLW5Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[12]) );
 	    LATNT1 LatR5C0B13( .G(WWLW5Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[13]) );
 	    LATNT1 LatR5C0B14( .G(WWLW5Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[14]) );
 	    LATNT1 LatR5C0B15( .G(WWLW5Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[15]) );
 	    LATNT1 LatR5C0B16( .G(WWLW5Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol0Line[16]) );
 	    LATNT1 LatR5C0B17( .G(WWLW5Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol0Line[17]) );
 	    LATNT1 LatR5C0B18( .G(WWLW5Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol0Line[18]) );
 	    LATNT1 LatR5C0B19( .G(WWLW5Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol0Line[19]) );
 	    LATNT1 LatR5C0B20( .G(WWLW5Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol0Line[20]) );
 	    LATNT1 LatR5C0B21( .G(WWLW5Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol0Line[21]) );
 	    LATNT1 LatR5C0B22( .G(WWLW5Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol0Line[22]) );
 	    LATNT1 LatR5C0B23( .G(WWLW5Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol0Line[23]) );
 	    LATNT1 LatR5C0B24( .G(WWLW5Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol0Line[24]) );
 	    LATNT1 LatR5C0B25( .G(WWLW5Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol0Line[25]) );
 	    LATNT1 LatR5C0B26( .G(WWLW5Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol0Line[26]) );
 	    LATNT1 LatR5C0B27( .G(WWLW5Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol0Line[27]) );
 	    LATNT1 LatR5C0B28( .G(WWLW5Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol0Line[28]) );
 	    LATNT1 LatR5C0B29( .G(WWLW5Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol0Line[29]) );
 	    LATNT1 LatR5C0B30( .G(WWLW5Wire1), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol0Line[30]) );
 	    LATNT1 LatR5C0B31( .G(WWLW5Wire1), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol0Line[31]) );
 	    LATNT1 LatR5C0B32( .G(WWLW5Wire2), 
					  .D(WrDat[32]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol0Line[32]) );
 	    LATNT1 LatR5C0B33( .G(WWLW5Wire2), 
					  .D(WrDat[33]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol0Line[33]) );
 	    LATNT1 LatR5C0B34( .G(WWLW5Wire2), 
					  .D(WrDat[34]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol0Line[34]) );
 	    LATNT1 LatR5C0B35( .G(WWLW5Wire2), 
					  .D(WrDat[35]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol0Line[35]) );
 	    LATNT1 LatR5C0B36( .G(WWLW5Wire2), 
					  .D(WrDat[36]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol0Line[36]) );
 	    LATNT1 LatR5C0B37( .G(WWLW5Wire2), 
					  .D(WrDat[37]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol0Line[37]) );
 	    LATNT1 LatR5C0B38( .G(WWLW5Wire2), 
					  .D(WrDat[38]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol0Line[38]) );
 	    LATNT1 LatR5C0B39( .G(WWLW5Wire2), 
					  .D(WrDat[39]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol0Line[39]) );
 	    LATNT1 LatR5C0B40( .G(WWLW5Wire2), 
					  .D(WrDat[40]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol0Line[40]) );
 	    LATNT1 LatR5C0B41( .G(WWLW5Wire2), 
					  .D(WrDat[41]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol0Line[41]) );
 	    LATNT1 LatR5C0B42( .G(WWLW5Wire2), 
					  .D(WrDat[42]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol0Line[42]) );
 	    LATNT1 LatR5C0B43( .G(WWLW5Wire2), 
					  .D(WrDat[43]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol0Line[43]) );
 	    LATNT1 LatR5C0B44( .G(WWLW5Wire2), 
					  .D(WrDat[44]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol0Line[44]) );
 	    LATNT1 LatR5C0B45( .G(WWLW5Wire2), 
					  .D(WrDat[45]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol0Line[45]) );
 	    LATNT1 LatR5C0B46( .G(WWLW5Wire2), 
					  .D(WrDat[46]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol0Line[46]) );
 	    LATNT1 LatR5C0B47( .G(WWLW5Wire2), 
					  .D(WrDat[47]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol0Line[47]) );
     
            //
            // Row 6
	    //
	      
 	    LATNT1 LatR6C0B0( .G(WWLW6Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR6C0B1( .G(WWLW6Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR6C0B2( .G(WWLW6Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR6C0B3( .G(WWLW6Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR6C0B4( .G(WWLW6Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[4]) );
 	    LATNT1 LatR6C0B5( .G(WWLW6Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[5]) );
 	    LATNT1 LatR6C0B6( .G(WWLW6Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[6]) );
 	    LATNT1 LatR6C0B7( .G(WWLW6Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[7]) );
 	    LATNT1 LatR6C0B8( .G(WWLW6Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[8]) );
 	    LATNT1 LatR6C0B9( .G(WWLW6Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[9]) );
 	    LATNT1 LatR6C0B10( .G(WWLW6Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[10]) );
 	    LATNT1 LatR6C0B11( .G(WWLW6Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[11]) );
 	    LATNT1 LatR6C0B12( .G(WWLW6Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[12]) );
 	    LATNT1 LatR6C0B13( .G(WWLW6Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[13]) );
 	    LATNT1 LatR6C0B14( .G(WWLW6Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[14]) );
 	    LATNT1 LatR6C0B15( .G(WWLW6Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR6Wire0), 
					  .Z(RdCol0Line[15]) );
 	    LATNT1 LatR6C0B16( .G(WWLW6Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR6Wire1), 
					  .Z(RdCol0Line[16]) );
 	    LATNT1 LatR6C0B17( .G(WWLW6Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR6Wire1), 
					  .Z(RdCol0Line[17]) );
 	    LATNT1 LatR6C0B18( .G(WWLW6Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR6Wire1), 
					  .Z(RdCol0Line[18]) );
 	    LATNT1 LatR6C0B19( .G(WWLW6Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR6Wire1), 
					  .Z(RdCol0Line[19]) );
 	    LATNT1 LatR6C0B20( .G(WWLW6Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR6Wire1), 
					  .Z(RdCol0Line[20]) );
 	    LATNT1 LatR6C0B21( .G(WWLW6Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR6Wire1), 
					  .Z(RdCol0Line[21]) );
 	    LATNT1 LatR6C0B22( .G(WWLW6Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR6Wire1), 
					  .Z(RdCol0Line[22]) );
 	    LATNT1 LatR6C0B23( .G(WWLW6Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR6Wire1), 
					  .Z(RdCol0Line[23]) );
 	    LATNT1 LatR6C0B24( .G(WWLW6Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR6Wire1), 
					  .Z(RdCol0Line[24]) );
 	    LATNT1 LatR6C0B25( .G(WWLW6Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR6Wire1), 
					  .Z(RdCol0Line[25]) );
 	    LATNT1 LatR6C0B26( .G(WWLW6Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR6Wire1), 
					  .Z(RdCol0Line[26]) );
 	    LATNT1 LatR6C0B27( .G(WWLW6Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR6Wire1), 
					  .Z(RdCol0Line[27]) );
 	    LATNT1 LatR6C0B28( .G(WWLW6Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR6Wire1), 
					  .Z(RdCol0Line[28]) );
 	    LATNT1 LatR6C0B29( .G(WWLW6Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR6Wire1), 
					  .Z(RdCol0Line[29]) );
 	    LATNT1 LatR6C0B30( .G(WWLW6Wire1), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR6Wire1), 
					  .Z(RdCol0Line[30]) );
 	    LATNT1 LatR6C0B31( .G(WWLW6Wire1), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR6Wire1), 
					  .Z(RdCol0Line[31]) );
 	    LATNT1 LatR6C0B32( .G(WWLW6Wire2), 
					  .D(WrDat[32]), 
                           		  .EN(RWLR6Wire2), 
					  .Z(RdCol0Line[32]) );
 	    LATNT1 LatR6C0B33( .G(WWLW6Wire2), 
					  .D(WrDat[33]), 
                           		  .EN(RWLR6Wire2), 
					  .Z(RdCol0Line[33]) );
 	    LATNT1 LatR6C0B34( .G(WWLW6Wire2), 
					  .D(WrDat[34]), 
                           		  .EN(RWLR6Wire2), 
					  .Z(RdCol0Line[34]) );
 	    LATNT1 LatR6C0B35( .G(WWLW6Wire2), 
					  .D(WrDat[35]), 
                           		  .EN(RWLR6Wire2), 
					  .Z(RdCol0Line[35]) );
 	    LATNT1 LatR6C0B36( .G(WWLW6Wire2), 
					  .D(WrDat[36]), 
                           		  .EN(RWLR6Wire2), 
					  .Z(RdCol0Line[36]) );
 	    LATNT1 LatR6C0B37( .G(WWLW6Wire2), 
					  .D(WrDat[37]), 
                           		  .EN(RWLR6Wire2), 
					  .Z(RdCol0Line[37]) );
 	    LATNT1 LatR6C0B38( .G(WWLW6Wire2), 
					  .D(WrDat[38]), 
                           		  .EN(RWLR6Wire2), 
					  .Z(RdCol0Line[38]) );
 	    LATNT1 LatR6C0B39( .G(WWLW6Wire2), 
					  .D(WrDat[39]), 
                           		  .EN(RWLR6Wire2), 
					  .Z(RdCol0Line[39]) );
 	    LATNT1 LatR6C0B40( .G(WWLW6Wire2), 
					  .D(WrDat[40]), 
                           		  .EN(RWLR6Wire2), 
					  .Z(RdCol0Line[40]) );
 	    LATNT1 LatR6C0B41( .G(WWLW6Wire2), 
					  .D(WrDat[41]), 
                           		  .EN(RWLR6Wire2), 
					  .Z(RdCol0Line[41]) );
 	    LATNT1 LatR6C0B42( .G(WWLW6Wire2), 
					  .D(WrDat[42]), 
                           		  .EN(RWLR6Wire2), 
					  .Z(RdCol0Line[42]) );
 	    LATNT1 LatR6C0B43( .G(WWLW6Wire2), 
					  .D(WrDat[43]), 
                           		  .EN(RWLR6Wire2), 
					  .Z(RdCol0Line[43]) );
 	    LATNT1 LatR6C0B44( .G(WWLW6Wire2), 
					  .D(WrDat[44]), 
                           		  .EN(RWLR6Wire2), 
					  .Z(RdCol0Line[44]) );
 	    LATNT1 LatR6C0B45( .G(WWLW6Wire2), 
					  .D(WrDat[45]), 
                           		  .EN(RWLR6Wire2), 
					  .Z(RdCol0Line[45]) );
 	    LATNT1 LatR6C0B46( .G(WWLW6Wire2), 
					  .D(WrDat[46]), 
                           		  .EN(RWLR6Wire2), 
					  .Z(RdCol0Line[46]) );
 	    LATNT1 LatR6C0B47( .G(WWLW6Wire2), 
					  .D(WrDat[47]), 
                           		  .EN(RWLR6Wire2), 
					  .Z(RdCol0Line[47]) );
     
            //
            // Row 7
	    //
	      
 	    LATNT1 LatR7C0B0( .G(WWLW7Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR7C0B1( .G(WWLW7Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR7C0B2( .G(WWLW7Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR7C0B3( .G(WWLW7Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR7C0B4( .G(WWLW7Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[4]) );
 	    LATNT1 LatR7C0B5( .G(WWLW7Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[5]) );
 	    LATNT1 LatR7C0B6( .G(WWLW7Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[6]) );
 	    LATNT1 LatR7C0B7( .G(WWLW7Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[7]) );
 	    LATNT1 LatR7C0B8( .G(WWLW7Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[8]) );
 	    LATNT1 LatR7C0B9( .G(WWLW7Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[9]) );
 	    LATNT1 LatR7C0B10( .G(WWLW7Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[10]) );
 	    LATNT1 LatR7C0B11( .G(WWLW7Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[11]) );
 	    LATNT1 LatR7C0B12( .G(WWLW7Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[12]) );
 	    LATNT1 LatR7C0B13( .G(WWLW7Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[13]) );
 	    LATNT1 LatR7C0B14( .G(WWLW7Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[14]) );
 	    LATNT1 LatR7C0B15( .G(WWLW7Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR7Wire0), 
					  .Z(RdCol0Line[15]) );
 	    LATNT1 LatR7C0B16( .G(WWLW7Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR7Wire1), 
					  .Z(RdCol0Line[16]) );
 	    LATNT1 LatR7C0B17( .G(WWLW7Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR7Wire1), 
					  .Z(RdCol0Line[17]) );
 	    LATNT1 LatR7C0B18( .G(WWLW7Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR7Wire1), 
					  .Z(RdCol0Line[18]) );
 	    LATNT1 LatR7C0B19( .G(WWLW7Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR7Wire1), 
					  .Z(RdCol0Line[19]) );
 	    LATNT1 LatR7C0B20( .G(WWLW7Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR7Wire1), 
					  .Z(RdCol0Line[20]) );
 	    LATNT1 LatR7C0B21( .G(WWLW7Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR7Wire1), 
					  .Z(RdCol0Line[21]) );
 	    LATNT1 LatR7C0B22( .G(WWLW7Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR7Wire1), 
					  .Z(RdCol0Line[22]) );
 	    LATNT1 LatR7C0B23( .G(WWLW7Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR7Wire1), 
					  .Z(RdCol0Line[23]) );
 	    LATNT1 LatR7C0B24( .G(WWLW7Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR7Wire1), 
					  .Z(RdCol0Line[24]) );
 	    LATNT1 LatR7C0B25( .G(WWLW7Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR7Wire1), 
					  .Z(RdCol0Line[25]) );
 	    LATNT1 LatR7C0B26( .G(WWLW7Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR7Wire1), 
					  .Z(RdCol0Line[26]) );
 	    LATNT1 LatR7C0B27( .G(WWLW7Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR7Wire1), 
					  .Z(RdCol0Line[27]) );
 	    LATNT1 LatR7C0B28( .G(WWLW7Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR7Wire1), 
					  .Z(RdCol0Line[28]) );
 	    LATNT1 LatR7C0B29( .G(WWLW7Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR7Wire1), 
					  .Z(RdCol0Line[29]) );
 	    LATNT1 LatR7C0B30( .G(WWLW7Wire1), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR7Wire1), 
					  .Z(RdCol0Line[30]) );
 	    LATNT1 LatR7C0B31( .G(WWLW7Wire1), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR7Wire1), 
					  .Z(RdCol0Line[31]) );
 	    LATNT1 LatR7C0B32( .G(WWLW7Wire2), 
					  .D(WrDat[32]), 
                           		  .EN(RWLR7Wire2), 
					  .Z(RdCol0Line[32]) );
 	    LATNT1 LatR7C0B33( .G(WWLW7Wire2), 
					  .D(WrDat[33]), 
                           		  .EN(RWLR7Wire2), 
					  .Z(RdCol0Line[33]) );
 	    LATNT1 LatR7C0B34( .G(WWLW7Wire2), 
					  .D(WrDat[34]), 
                           		  .EN(RWLR7Wire2), 
					  .Z(RdCol0Line[34]) );
 	    LATNT1 LatR7C0B35( .G(WWLW7Wire2), 
					  .D(WrDat[35]), 
                           		  .EN(RWLR7Wire2), 
					  .Z(RdCol0Line[35]) );
 	    LATNT1 LatR7C0B36( .G(WWLW7Wire2), 
					  .D(WrDat[36]), 
                           		  .EN(RWLR7Wire2), 
					  .Z(RdCol0Line[36]) );
 	    LATNT1 LatR7C0B37( .G(WWLW7Wire2), 
					  .D(WrDat[37]), 
                           		  .EN(RWLR7Wire2), 
					  .Z(RdCol0Line[37]) );
 	    LATNT1 LatR7C0B38( .G(WWLW7Wire2), 
					  .D(WrDat[38]), 
                           		  .EN(RWLR7Wire2), 
					  .Z(RdCol0Line[38]) );
 	    LATNT1 LatR7C0B39( .G(WWLW7Wire2), 
					  .D(WrDat[39]), 
                           		  .EN(RWLR7Wire2), 
					  .Z(RdCol0Line[39]) );
 	    LATNT1 LatR7C0B40( .G(WWLW7Wire2), 
					  .D(WrDat[40]), 
                           		  .EN(RWLR7Wire2), 
					  .Z(RdCol0Line[40]) );
 	    LATNT1 LatR7C0B41( .G(WWLW7Wire2), 
					  .D(WrDat[41]), 
                           		  .EN(RWLR7Wire2), 
					  .Z(RdCol0Line[41]) );
 	    LATNT1 LatR7C0B42( .G(WWLW7Wire2), 
					  .D(WrDat[42]), 
                           		  .EN(RWLR7Wire2), 
					  .Z(RdCol0Line[42]) );
 	    LATNT1 LatR7C0B43( .G(WWLW7Wire2), 
					  .D(WrDat[43]), 
                           		  .EN(RWLR7Wire2), 
					  .Z(RdCol0Line[43]) );
 	    LATNT1 LatR7C0B44( .G(WWLW7Wire2), 
					  .D(WrDat[44]), 
                           		  .EN(RWLR7Wire2), 
					  .Z(RdCol0Line[44]) );
 	    LATNT1 LatR7C0B45( .G(WWLW7Wire2), 
					  .D(WrDat[45]), 
                           		  .EN(RWLR7Wire2), 
					  .Z(RdCol0Line[45]) );
 	    LATNT1 LatR7C0B46( .G(WWLW7Wire2), 
					  .D(WrDat[46]), 
                           		  .EN(RWLR7Wire2), 
					  .Z(RdCol0Line[46]) );
 	    LATNT1 LatR7C0B47( .G(WWLW7Wire2), 
					  .D(WrDat[47]), 
                           		  .EN(RWLR7Wire2), 
					  .Z(RdCol0Line[47]) );

//
//
// the psuedo sense amp (and column muxing if necessary)
         BUFD3 rd0buf(.A(RdCol0Line[0]), .Z(RdDat[0]) );
         BUFD3 rd1buf(.A(RdCol0Line[1]), .Z(RdDat[1]) );
         BUFD3 rd2buf(.A(RdCol0Line[2]), .Z(RdDat[2]) );
         BUFD3 rd3buf(.A(RdCol0Line[3]), .Z(RdDat[3]) );
         BUFD3 rd4buf(.A(RdCol0Line[4]), .Z(RdDat[4]) );
         BUFD3 rd5buf(.A(RdCol0Line[5]), .Z(RdDat[5]) );
         BUFD3 rd6buf(.A(RdCol0Line[6]), .Z(RdDat[6]) );
         BUFD3 rd7buf(.A(RdCol0Line[7]), .Z(RdDat[7]) );
         BUFD3 rd8buf(.A(RdCol0Line[8]), .Z(RdDat[8]) );
         BUFD3 rd9buf(.A(RdCol0Line[9]), .Z(RdDat[9]) );
         BUFD3 rd10buf(.A(RdCol0Line[10]), .Z(RdDat[10]) );
         BUFD3 rd11buf(.A(RdCol0Line[11]), .Z(RdDat[11]) );
         BUFD3 rd12buf(.A(RdCol0Line[12]), .Z(RdDat[12]) );
         BUFD3 rd13buf(.A(RdCol0Line[13]), .Z(RdDat[13]) );
         BUFD3 rd14buf(.A(RdCol0Line[14]), .Z(RdDat[14]) );
         BUFD3 rd15buf(.A(RdCol0Line[15]), .Z(RdDat[15]) );
         BUFD3 rd16buf(.A(RdCol0Line[16]), .Z(RdDat[16]) );
         BUFD3 rd17buf(.A(RdCol0Line[17]), .Z(RdDat[17]) );
         BUFD3 rd18buf(.A(RdCol0Line[18]), .Z(RdDat[18]) );
         BUFD3 rd19buf(.A(RdCol0Line[19]), .Z(RdDat[19]) );
         BUFD3 rd20buf(.A(RdCol0Line[20]), .Z(RdDat[20]) );
         BUFD3 rd21buf(.A(RdCol0Line[21]), .Z(RdDat[21]) );
         BUFD3 rd22buf(.A(RdCol0Line[22]), .Z(RdDat[22]) );
         BUFD3 rd23buf(.A(RdCol0Line[23]), .Z(RdDat[23]) );
         BUFD3 rd24buf(.A(RdCol0Line[24]), .Z(RdDat[24]) );
         BUFD3 rd25buf(.A(RdCol0Line[25]), .Z(RdDat[25]) );
         BUFD3 rd26buf(.A(RdCol0Line[26]), .Z(RdDat[26]) );
         BUFD3 rd27buf(.A(RdCol0Line[27]), .Z(RdDat[27]) );
         BUFD3 rd28buf(.A(RdCol0Line[28]), .Z(RdDat[28]) );
         BUFD3 rd29buf(.A(RdCol0Line[29]), .Z(RdDat[29]) );
         BUFD3 rd30buf(.A(RdCol0Line[30]), .Z(RdDat[30]) );
         BUFD3 rd31buf(.A(RdCol0Line[31]), .Z(RdDat[31]) );
         BUFD3 rd32buf(.A(RdCol0Line[32]), .Z(RdDat[32]) );
         BUFD3 rd33buf(.A(RdCol0Line[33]), .Z(RdDat[33]) );
         BUFD3 rd34buf(.A(RdCol0Line[34]), .Z(RdDat[34]) );
         BUFD3 rd35buf(.A(RdCol0Line[35]), .Z(RdDat[35]) );
         BUFD3 rd36buf(.A(RdCol0Line[36]), .Z(RdDat[36]) );
         BUFD3 rd37buf(.A(RdCol0Line[37]), .Z(RdDat[37]) );
         BUFD3 rd38buf(.A(RdCol0Line[38]), .Z(RdDat[38]) );
         BUFD3 rd39buf(.A(RdCol0Line[39]), .Z(RdDat[39]) );
         BUFD3 rd40buf(.A(RdCol0Line[40]), .Z(RdDat[40]) );
         BUFD3 rd41buf(.A(RdCol0Line[41]), .Z(RdDat[41]) );
         BUFD3 rd42buf(.A(RdCol0Line[42]), .Z(RdDat[42]) );
         BUFD3 rd43buf(.A(RdCol0Line[43]), .Z(RdDat[43]) );
         BUFD3 rd44buf(.A(RdCol0Line[44]), .Z(RdDat[44]) );
         BUFD3 rd45buf(.A(RdCol0Line[45]), .Z(RdDat[45]) );
         BUFD3 rd46buf(.A(RdCol0Line[46]), .Z(RdDat[46]) );
         BUFD3 rd47buf(.A(RdCol0Line[47]), .Z(RdDat[47]) );

   endmodule

