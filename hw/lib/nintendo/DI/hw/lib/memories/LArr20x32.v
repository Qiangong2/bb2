// This file was automatically derived from "LArr20x32.vpp"
// using vpp on Thu May 18 23:07:25 2000.



// syn myclk = NullClk


module LArr20x32 (
	NullClk,
	WWL,
	WrDat,
	RWL,
	ColSelect,
	RdDat);

	input NullClk;
	input [19:0]WWL;
	input [31:0] WrDat;
	input [4:0]RWL;
	   input [1:0] ColSelect;
	output [31:0] RdDat;


       wire [31:0] RdCol0Line;
       wire [31:0] RdCol1Line;
       wire [31:0] RdCol2Line;
       wire [31:0] RdCol3Line;

  
            //
            // ReadLineBuffers
            //
	      
	    INVDA RWLR0Buf0 ( .A ( RWL[0] ), .Z ( RWLR0Wire0 ) );
	    INVDA RWLR0Buf1 ( .A ( RWL[0] ), .Z ( RWLR0Wire1 ) );
	    INVDA RWLR0Buf2 ( .A ( RWL[0] ), .Z ( RWLR0Wire2 ) );
	    INVDA RWLR0Buf3 ( .A ( RWL[0] ), .Z ( RWLR0Wire3 ) );
	    INVDA RWLR0Buf4 ( .A ( RWL[0] ), .Z ( RWLR0Wire4 ) );
	    INVDA RWLR0Buf5 ( .A ( RWL[0] ), .Z ( RWLR0Wire5 ) );
	    INVDA RWLR0Buf6 ( .A ( RWL[0] ), .Z ( RWLR0Wire6 ) );
	    INVDA RWLR0Buf7 ( .A ( RWL[0] ), .Z ( RWLR0Wire7 ) );
	    INVDA RWLR1Buf0 ( .A ( RWL[1] ), .Z ( RWLR1Wire0 ) );
	    INVDA RWLR1Buf1 ( .A ( RWL[1] ), .Z ( RWLR1Wire1 ) );
	    INVDA RWLR1Buf2 ( .A ( RWL[1] ), .Z ( RWLR1Wire2 ) );
	    INVDA RWLR1Buf3 ( .A ( RWL[1] ), .Z ( RWLR1Wire3 ) );
	    INVDA RWLR1Buf4 ( .A ( RWL[1] ), .Z ( RWLR1Wire4 ) );
	    INVDA RWLR1Buf5 ( .A ( RWL[1] ), .Z ( RWLR1Wire5 ) );
	    INVDA RWLR1Buf6 ( .A ( RWL[1] ), .Z ( RWLR1Wire6 ) );
	    INVDA RWLR1Buf7 ( .A ( RWL[1] ), .Z ( RWLR1Wire7 ) );
	    INVDA RWLR2Buf0 ( .A ( RWL[2] ), .Z ( RWLR2Wire0 ) );
	    INVDA RWLR2Buf1 ( .A ( RWL[2] ), .Z ( RWLR2Wire1 ) );
	    INVDA RWLR2Buf2 ( .A ( RWL[2] ), .Z ( RWLR2Wire2 ) );
	    INVDA RWLR2Buf3 ( .A ( RWL[2] ), .Z ( RWLR2Wire3 ) );
	    INVDA RWLR2Buf4 ( .A ( RWL[2] ), .Z ( RWLR2Wire4 ) );
	    INVDA RWLR2Buf5 ( .A ( RWL[2] ), .Z ( RWLR2Wire5 ) );
	    INVDA RWLR2Buf6 ( .A ( RWL[2] ), .Z ( RWLR2Wire6 ) );
	    INVDA RWLR2Buf7 ( .A ( RWL[2] ), .Z ( RWLR2Wire7 ) );
	    INVDA RWLR3Buf0 ( .A ( RWL[3] ), .Z ( RWLR3Wire0 ) );
	    INVDA RWLR3Buf1 ( .A ( RWL[3] ), .Z ( RWLR3Wire1 ) );
	    INVDA RWLR3Buf2 ( .A ( RWL[3] ), .Z ( RWLR3Wire2 ) );
	    INVDA RWLR3Buf3 ( .A ( RWL[3] ), .Z ( RWLR3Wire3 ) );
	    INVDA RWLR3Buf4 ( .A ( RWL[3] ), .Z ( RWLR3Wire4 ) );
	    INVDA RWLR3Buf5 ( .A ( RWL[3] ), .Z ( RWLR3Wire5 ) );
	    INVDA RWLR3Buf6 ( .A ( RWL[3] ), .Z ( RWLR3Wire6 ) );
	    INVDA RWLR3Buf7 ( .A ( RWL[3] ), .Z ( RWLR3Wire7 ) );
	    INVDA RWLR4Buf0 ( .A ( RWL[4] ), .Z ( RWLR4Wire0 ) );
	    INVDA RWLR4Buf1 ( .A ( RWL[4] ), .Z ( RWLR4Wire1 ) );
	    INVDA RWLR4Buf2 ( .A ( RWL[4] ), .Z ( RWLR4Wire2 ) );
	    INVDA RWLR4Buf3 ( .A ( RWL[4] ), .Z ( RWLR4Wire3 ) );
	    INVDA RWLR4Buf4 ( .A ( RWL[4] ), .Z ( RWLR4Wire4 ) );
	    INVDA RWLR4Buf5 ( .A ( RWL[4] ), .Z ( RWLR4Wire5 ) );
	    INVDA RWLR4Buf6 ( .A ( RWL[4] ), .Z ( RWLR4Wire6 ) );
	    INVDA RWLR4Buf7 ( .A ( RWL[4] ), .Z ( RWLR4Wire7 ) );
  
            //
            // WriteLineBuffers
            //
	      
	    INVDA WWLW0Buf0 ( .A ( WWL[0] ), .Z ( WWLW0Wire0 ) );
	    INVDA WWLW0Buf1 ( .A ( WWL[0] ), .Z ( WWLW0Wire1 ) );
	    INVDA WWLW1Buf0 ( .A ( WWL[1] ), .Z ( WWLW1Wire0 ) );
	    INVDA WWLW1Buf1 ( .A ( WWL[1] ), .Z ( WWLW1Wire1 ) );
	    INVDA WWLW2Buf0 ( .A ( WWL[2] ), .Z ( WWLW2Wire0 ) );
	    INVDA WWLW2Buf1 ( .A ( WWL[2] ), .Z ( WWLW2Wire1 ) );
	    INVDA WWLW3Buf0 ( .A ( WWL[3] ), .Z ( WWLW3Wire0 ) );
	    INVDA WWLW3Buf1 ( .A ( WWL[3] ), .Z ( WWLW3Wire1 ) );
	    INVDA WWLW4Buf0 ( .A ( WWL[4] ), .Z ( WWLW4Wire0 ) );
	    INVDA WWLW4Buf1 ( .A ( WWL[4] ), .Z ( WWLW4Wire1 ) );
	    INVDA WWLW5Buf0 ( .A ( WWL[5] ), .Z ( WWLW5Wire0 ) );
	    INVDA WWLW5Buf1 ( .A ( WWL[5] ), .Z ( WWLW5Wire1 ) );
	    INVDA WWLW6Buf0 ( .A ( WWL[6] ), .Z ( WWLW6Wire0 ) );
	    INVDA WWLW6Buf1 ( .A ( WWL[6] ), .Z ( WWLW6Wire1 ) );
	    INVDA WWLW7Buf0 ( .A ( WWL[7] ), .Z ( WWLW7Wire0 ) );
	    INVDA WWLW7Buf1 ( .A ( WWL[7] ), .Z ( WWLW7Wire1 ) );
	    INVDA WWLW8Buf0 ( .A ( WWL[8] ), .Z ( WWLW8Wire0 ) );
	    INVDA WWLW8Buf1 ( .A ( WWL[8] ), .Z ( WWLW8Wire1 ) );
	    INVDA WWLW9Buf0 ( .A ( WWL[9] ), .Z ( WWLW9Wire0 ) );
	    INVDA WWLW9Buf1 ( .A ( WWL[9] ), .Z ( WWLW9Wire1 ) );
	    INVDA WWLW10Buf0 ( .A ( WWL[10] ), .Z ( WWLW10Wire0 ) );
	    INVDA WWLW10Buf1 ( .A ( WWL[10] ), .Z ( WWLW10Wire1 ) );
	    INVDA WWLW11Buf0 ( .A ( WWL[11] ), .Z ( WWLW11Wire0 ) );
	    INVDA WWLW11Buf1 ( .A ( WWL[11] ), .Z ( WWLW11Wire1 ) );
	    INVDA WWLW12Buf0 ( .A ( WWL[12] ), .Z ( WWLW12Wire0 ) );
	    INVDA WWLW12Buf1 ( .A ( WWL[12] ), .Z ( WWLW12Wire1 ) );
	    INVDA WWLW13Buf0 ( .A ( WWL[13] ), .Z ( WWLW13Wire0 ) );
	    INVDA WWLW13Buf1 ( .A ( WWL[13] ), .Z ( WWLW13Wire1 ) );
	    INVDA WWLW14Buf0 ( .A ( WWL[14] ), .Z ( WWLW14Wire0 ) );
	    INVDA WWLW14Buf1 ( .A ( WWL[14] ), .Z ( WWLW14Wire1 ) );
	    INVDA WWLW15Buf0 ( .A ( WWL[15] ), .Z ( WWLW15Wire0 ) );
	    INVDA WWLW15Buf1 ( .A ( WWL[15] ), .Z ( WWLW15Wire1 ) );
	    INVDA WWLW16Buf0 ( .A ( WWL[16] ), .Z ( WWLW16Wire0 ) );
	    INVDA WWLW16Buf1 ( .A ( WWL[16] ), .Z ( WWLW16Wire1 ) );
	    INVDA WWLW17Buf0 ( .A ( WWL[17] ), .Z ( WWLW17Wire0 ) );
	    INVDA WWLW17Buf1 ( .A ( WWL[17] ), .Z ( WWLW17Wire1 ) );
	    INVDA WWLW18Buf0 ( .A ( WWL[18] ), .Z ( WWLW18Wire0 ) );
	    INVDA WWLW18Buf1 ( .A ( WWL[18] ), .Z ( WWLW18Wire1 ) );
	    INVDA WWLW19Buf0 ( .A ( WWL[19] ), .Z ( WWLW19Wire0 ) );
	    INVDA WWLW19Buf1 ( .A ( WWL[19] ), .Z ( WWLW19Wire1 ) );

//
//
// this is the array of latches itself.
     
            //
            // Row 0
	    //
	      
 	    LATNT1 LatR0C0B0( .G(WWLW0Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR0C1B0( .G(WWLW1Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol1Line[0]) );
 	    LATNT1 LatR0C2B0( .G(WWLW2Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR0Wire4), 
					  .Z(RdCol2Line[0]) );
 	    LATNT1 LatR0C3B0( .G(WWLW3Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR0Wire6), 
					  .Z(RdCol3Line[0]) );
 	    LATNT1 LatR0C0B1( .G(WWLW0Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR0C1B1( .G(WWLW1Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol1Line[1]) );
 	    LATNT1 LatR0C2B1( .G(WWLW2Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR0Wire4), 
					  .Z(RdCol2Line[1]) );
 	    LATNT1 LatR0C3B1( .G(WWLW3Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR0Wire6), 
					  .Z(RdCol3Line[1]) );
 	    LATNT1 LatR0C0B2( .G(WWLW0Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR0C1B2( .G(WWLW1Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol1Line[2]) );
 	    LATNT1 LatR0C2B2( .G(WWLW2Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR0Wire4), 
					  .Z(RdCol2Line[2]) );
 	    LATNT1 LatR0C3B2( .G(WWLW3Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR0Wire6), 
					  .Z(RdCol3Line[2]) );
 	    LATNT1 LatR0C0B3( .G(WWLW0Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR0C1B3( .G(WWLW1Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol1Line[3]) );
 	    LATNT1 LatR0C2B3( .G(WWLW2Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR0Wire4), 
					  .Z(RdCol2Line[3]) );
 	    LATNT1 LatR0C3B3( .G(WWLW3Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR0Wire6), 
					  .Z(RdCol3Line[3]) );
 	    LATNT1 LatR0C0B4( .G(WWLW0Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[4]) );
 	    LATNT1 LatR0C1B4( .G(WWLW1Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol1Line[4]) );
 	    LATNT1 LatR0C2B4( .G(WWLW2Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR0Wire4), 
					  .Z(RdCol2Line[4]) );
 	    LATNT1 LatR0C3B4( .G(WWLW3Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR0Wire6), 
					  .Z(RdCol3Line[4]) );
 	    LATNT1 LatR0C0B5( .G(WWLW0Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[5]) );
 	    LATNT1 LatR0C1B5( .G(WWLW1Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol1Line[5]) );
 	    LATNT1 LatR0C2B5( .G(WWLW2Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR0Wire4), 
					  .Z(RdCol2Line[5]) );
 	    LATNT1 LatR0C3B5( .G(WWLW3Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR0Wire6), 
					  .Z(RdCol3Line[5]) );
 	    LATNT1 LatR0C0B6( .G(WWLW0Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[6]) );
 	    LATNT1 LatR0C1B6( .G(WWLW1Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol1Line[6]) );
 	    LATNT1 LatR0C2B6( .G(WWLW2Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR0Wire4), 
					  .Z(RdCol2Line[6]) );
 	    LATNT1 LatR0C3B6( .G(WWLW3Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR0Wire6), 
					  .Z(RdCol3Line[6]) );
 	    LATNT1 LatR0C0B7( .G(WWLW0Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[7]) );
 	    LATNT1 LatR0C1B7( .G(WWLW1Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol1Line[7]) );
 	    LATNT1 LatR0C2B7( .G(WWLW2Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR0Wire4), 
					  .Z(RdCol2Line[7]) );
 	    LATNT1 LatR0C3B7( .G(WWLW3Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR0Wire6), 
					  .Z(RdCol3Line[7]) );
 	    LATNT1 LatR0C0B8( .G(WWLW0Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[8]) );
 	    LATNT1 LatR0C1B8( .G(WWLW1Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol1Line[8]) );
 	    LATNT1 LatR0C2B8( .G(WWLW2Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR0Wire4), 
					  .Z(RdCol2Line[8]) );
 	    LATNT1 LatR0C3B8( .G(WWLW3Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR0Wire6), 
					  .Z(RdCol3Line[8]) );
 	    LATNT1 LatR0C0B9( .G(WWLW0Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[9]) );
 	    LATNT1 LatR0C1B9( .G(WWLW1Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol1Line[9]) );
 	    LATNT1 LatR0C2B9( .G(WWLW2Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR0Wire4), 
					  .Z(RdCol2Line[9]) );
 	    LATNT1 LatR0C3B9( .G(WWLW3Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR0Wire6), 
					  .Z(RdCol3Line[9]) );
 	    LATNT1 LatR0C0B10( .G(WWLW0Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[10]) );
 	    LATNT1 LatR0C1B10( .G(WWLW1Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol1Line[10]) );
 	    LATNT1 LatR0C2B10( .G(WWLW2Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR0Wire4), 
					  .Z(RdCol2Line[10]) );
 	    LATNT1 LatR0C3B10( .G(WWLW3Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR0Wire6), 
					  .Z(RdCol3Line[10]) );
 	    LATNT1 LatR0C0B11( .G(WWLW0Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[11]) );
 	    LATNT1 LatR0C1B11( .G(WWLW1Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol1Line[11]) );
 	    LATNT1 LatR0C2B11( .G(WWLW2Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR0Wire4), 
					  .Z(RdCol2Line[11]) );
 	    LATNT1 LatR0C3B11( .G(WWLW3Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR0Wire6), 
					  .Z(RdCol3Line[11]) );
 	    LATNT1 LatR0C0B12( .G(WWLW0Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[12]) );
 	    LATNT1 LatR0C1B12( .G(WWLW1Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol1Line[12]) );
 	    LATNT1 LatR0C2B12( .G(WWLW2Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR0Wire4), 
					  .Z(RdCol2Line[12]) );
 	    LATNT1 LatR0C3B12( .G(WWLW3Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR0Wire6), 
					  .Z(RdCol3Line[12]) );
 	    LATNT1 LatR0C0B13( .G(WWLW0Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[13]) );
 	    LATNT1 LatR0C1B13( .G(WWLW1Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol1Line[13]) );
 	    LATNT1 LatR0C2B13( .G(WWLW2Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR0Wire4), 
					  .Z(RdCol2Line[13]) );
 	    LATNT1 LatR0C3B13( .G(WWLW3Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR0Wire6), 
					  .Z(RdCol3Line[13]) );
 	    LATNT1 LatR0C0B14( .G(WWLW0Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[14]) );
 	    LATNT1 LatR0C1B14( .G(WWLW1Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol1Line[14]) );
 	    LATNT1 LatR0C2B14( .G(WWLW2Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR0Wire4), 
					  .Z(RdCol2Line[14]) );
 	    LATNT1 LatR0C3B14( .G(WWLW3Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR0Wire6), 
					  .Z(RdCol3Line[14]) );
 	    LATNT1 LatR0C0B15( .G(WWLW0Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[15]) );
 	    LATNT1 LatR0C1B15( .G(WWLW1Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol1Line[15]) );
 	    LATNT1 LatR0C2B15( .G(WWLW2Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR0Wire4), 
					  .Z(RdCol2Line[15]) );
 	    LATNT1 LatR0C3B15( .G(WWLW3Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR0Wire6), 
					  .Z(RdCol3Line[15]) );
 	    LATNT1 LatR0C0B16( .G(WWLW0Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[16]) );
 	    LATNT1 LatR0C1B16( .G(WWLW1Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol1Line[16]) );
 	    LATNT1 LatR0C2B16( .G(WWLW2Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR0Wire5), 
					  .Z(RdCol2Line[16]) );
 	    LATNT1 LatR0C3B16( .G(WWLW3Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR0Wire7), 
					  .Z(RdCol3Line[16]) );
 	    LATNT1 LatR0C0B17( .G(WWLW0Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[17]) );
 	    LATNT1 LatR0C1B17( .G(WWLW1Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol1Line[17]) );
 	    LATNT1 LatR0C2B17( .G(WWLW2Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR0Wire5), 
					  .Z(RdCol2Line[17]) );
 	    LATNT1 LatR0C3B17( .G(WWLW3Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR0Wire7), 
					  .Z(RdCol3Line[17]) );
 	    LATNT1 LatR0C0B18( .G(WWLW0Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[18]) );
 	    LATNT1 LatR0C1B18( .G(WWLW1Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol1Line[18]) );
 	    LATNT1 LatR0C2B18( .G(WWLW2Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR0Wire5), 
					  .Z(RdCol2Line[18]) );
 	    LATNT1 LatR0C3B18( .G(WWLW3Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR0Wire7), 
					  .Z(RdCol3Line[18]) );
 	    LATNT1 LatR0C0B19( .G(WWLW0Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[19]) );
 	    LATNT1 LatR0C1B19( .G(WWLW1Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol1Line[19]) );
 	    LATNT1 LatR0C2B19( .G(WWLW2Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR0Wire5), 
					  .Z(RdCol2Line[19]) );
 	    LATNT1 LatR0C3B19( .G(WWLW3Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR0Wire7), 
					  .Z(RdCol3Line[19]) );
 	    LATNT1 LatR0C0B20( .G(WWLW0Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[20]) );
 	    LATNT1 LatR0C1B20( .G(WWLW1Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol1Line[20]) );
 	    LATNT1 LatR0C2B20( .G(WWLW2Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR0Wire5), 
					  .Z(RdCol2Line[20]) );
 	    LATNT1 LatR0C3B20( .G(WWLW3Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR0Wire7), 
					  .Z(RdCol3Line[20]) );
 	    LATNT1 LatR0C0B21( .G(WWLW0Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[21]) );
 	    LATNT1 LatR0C1B21( .G(WWLW1Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol1Line[21]) );
 	    LATNT1 LatR0C2B21( .G(WWLW2Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR0Wire5), 
					  .Z(RdCol2Line[21]) );
 	    LATNT1 LatR0C3B21( .G(WWLW3Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR0Wire7), 
					  .Z(RdCol3Line[21]) );
 	    LATNT1 LatR0C0B22( .G(WWLW0Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[22]) );
 	    LATNT1 LatR0C1B22( .G(WWLW1Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol1Line[22]) );
 	    LATNT1 LatR0C2B22( .G(WWLW2Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR0Wire5), 
					  .Z(RdCol2Line[22]) );
 	    LATNT1 LatR0C3B22( .G(WWLW3Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR0Wire7), 
					  .Z(RdCol3Line[22]) );
 	    LATNT1 LatR0C0B23( .G(WWLW0Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[23]) );
 	    LATNT1 LatR0C1B23( .G(WWLW1Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol1Line[23]) );
 	    LATNT1 LatR0C2B23( .G(WWLW2Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR0Wire5), 
					  .Z(RdCol2Line[23]) );
 	    LATNT1 LatR0C3B23( .G(WWLW3Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR0Wire7), 
					  .Z(RdCol3Line[23]) );
 	    LATNT1 LatR0C0B24( .G(WWLW0Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[24]) );
 	    LATNT1 LatR0C1B24( .G(WWLW1Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol1Line[24]) );
 	    LATNT1 LatR0C2B24( .G(WWLW2Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR0Wire5), 
					  .Z(RdCol2Line[24]) );
 	    LATNT1 LatR0C3B24( .G(WWLW3Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR0Wire7), 
					  .Z(RdCol3Line[24]) );
 	    LATNT1 LatR0C0B25( .G(WWLW0Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[25]) );
 	    LATNT1 LatR0C1B25( .G(WWLW1Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol1Line[25]) );
 	    LATNT1 LatR0C2B25( .G(WWLW2Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR0Wire5), 
					  .Z(RdCol2Line[25]) );
 	    LATNT1 LatR0C3B25( .G(WWLW3Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR0Wire7), 
					  .Z(RdCol3Line[25]) );
 	    LATNT1 LatR0C0B26( .G(WWLW0Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[26]) );
 	    LATNT1 LatR0C1B26( .G(WWLW1Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol1Line[26]) );
 	    LATNT1 LatR0C2B26( .G(WWLW2Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR0Wire5), 
					  .Z(RdCol2Line[26]) );
 	    LATNT1 LatR0C3B26( .G(WWLW3Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR0Wire7), 
					  .Z(RdCol3Line[26]) );
 	    LATNT1 LatR0C0B27( .G(WWLW0Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[27]) );
 	    LATNT1 LatR0C1B27( .G(WWLW1Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol1Line[27]) );
 	    LATNT1 LatR0C2B27( .G(WWLW2Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR0Wire5), 
					  .Z(RdCol2Line[27]) );
 	    LATNT1 LatR0C3B27( .G(WWLW3Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR0Wire7), 
					  .Z(RdCol3Line[27]) );
 	    LATNT1 LatR0C0B28( .G(WWLW0Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[28]) );
 	    LATNT1 LatR0C1B28( .G(WWLW1Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol1Line[28]) );
 	    LATNT1 LatR0C2B28( .G(WWLW2Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR0Wire5), 
					  .Z(RdCol2Line[28]) );
 	    LATNT1 LatR0C3B28( .G(WWLW3Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR0Wire7), 
					  .Z(RdCol3Line[28]) );
 	    LATNT1 LatR0C0B29( .G(WWLW0Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[29]) );
 	    LATNT1 LatR0C1B29( .G(WWLW1Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol1Line[29]) );
 	    LATNT1 LatR0C2B29( .G(WWLW2Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR0Wire5), 
					  .Z(RdCol2Line[29]) );
 	    LATNT1 LatR0C3B29( .G(WWLW3Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR0Wire7), 
					  .Z(RdCol3Line[29]) );
 	    LATNT1 LatR0C0B30( .G(WWLW0Wire1), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[30]) );
 	    LATNT1 LatR0C1B30( .G(WWLW1Wire1), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol1Line[30]) );
 	    LATNT1 LatR0C2B30( .G(WWLW2Wire1), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR0Wire5), 
					  .Z(RdCol2Line[30]) );
 	    LATNT1 LatR0C3B30( .G(WWLW3Wire1), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR0Wire7), 
					  .Z(RdCol3Line[30]) );
 	    LATNT1 LatR0C0B31( .G(WWLW0Wire1), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[31]) );
 	    LATNT1 LatR0C1B31( .G(WWLW1Wire1), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol1Line[31]) );
 	    LATNT1 LatR0C2B31( .G(WWLW2Wire1), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR0Wire5), 
					  .Z(RdCol2Line[31]) );
 	    LATNT1 LatR0C3B31( .G(WWLW3Wire1), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR0Wire7), 
					  .Z(RdCol3Line[31]) );
     
            //
            // Row 1
	    //
	      
 	    LATNT1 LatR1C0B0( .G(WWLW4Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR1C1B0( .G(WWLW5Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol1Line[0]) );
 	    LATNT1 LatR1C2B0( .G(WWLW6Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR1Wire4), 
					  .Z(RdCol2Line[0]) );
 	    LATNT1 LatR1C3B0( .G(WWLW7Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR1Wire6), 
					  .Z(RdCol3Line[0]) );
 	    LATNT1 LatR1C0B1( .G(WWLW4Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR1C1B1( .G(WWLW5Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol1Line[1]) );
 	    LATNT1 LatR1C2B1( .G(WWLW6Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR1Wire4), 
					  .Z(RdCol2Line[1]) );
 	    LATNT1 LatR1C3B1( .G(WWLW7Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR1Wire6), 
					  .Z(RdCol3Line[1]) );
 	    LATNT1 LatR1C0B2( .G(WWLW4Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR1C1B2( .G(WWLW5Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol1Line[2]) );
 	    LATNT1 LatR1C2B2( .G(WWLW6Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR1Wire4), 
					  .Z(RdCol2Line[2]) );
 	    LATNT1 LatR1C3B2( .G(WWLW7Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR1Wire6), 
					  .Z(RdCol3Line[2]) );
 	    LATNT1 LatR1C0B3( .G(WWLW4Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR1C1B3( .G(WWLW5Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol1Line[3]) );
 	    LATNT1 LatR1C2B3( .G(WWLW6Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR1Wire4), 
					  .Z(RdCol2Line[3]) );
 	    LATNT1 LatR1C3B3( .G(WWLW7Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR1Wire6), 
					  .Z(RdCol3Line[3]) );
 	    LATNT1 LatR1C0B4( .G(WWLW4Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[4]) );
 	    LATNT1 LatR1C1B4( .G(WWLW5Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol1Line[4]) );
 	    LATNT1 LatR1C2B4( .G(WWLW6Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR1Wire4), 
					  .Z(RdCol2Line[4]) );
 	    LATNT1 LatR1C3B4( .G(WWLW7Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR1Wire6), 
					  .Z(RdCol3Line[4]) );
 	    LATNT1 LatR1C0B5( .G(WWLW4Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[5]) );
 	    LATNT1 LatR1C1B5( .G(WWLW5Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol1Line[5]) );
 	    LATNT1 LatR1C2B5( .G(WWLW6Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR1Wire4), 
					  .Z(RdCol2Line[5]) );
 	    LATNT1 LatR1C3B5( .G(WWLW7Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR1Wire6), 
					  .Z(RdCol3Line[5]) );
 	    LATNT1 LatR1C0B6( .G(WWLW4Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[6]) );
 	    LATNT1 LatR1C1B6( .G(WWLW5Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol1Line[6]) );
 	    LATNT1 LatR1C2B6( .G(WWLW6Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR1Wire4), 
					  .Z(RdCol2Line[6]) );
 	    LATNT1 LatR1C3B6( .G(WWLW7Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR1Wire6), 
					  .Z(RdCol3Line[6]) );
 	    LATNT1 LatR1C0B7( .G(WWLW4Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[7]) );
 	    LATNT1 LatR1C1B7( .G(WWLW5Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol1Line[7]) );
 	    LATNT1 LatR1C2B7( .G(WWLW6Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR1Wire4), 
					  .Z(RdCol2Line[7]) );
 	    LATNT1 LatR1C3B7( .G(WWLW7Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR1Wire6), 
					  .Z(RdCol3Line[7]) );
 	    LATNT1 LatR1C0B8( .G(WWLW4Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[8]) );
 	    LATNT1 LatR1C1B8( .G(WWLW5Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol1Line[8]) );
 	    LATNT1 LatR1C2B8( .G(WWLW6Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR1Wire4), 
					  .Z(RdCol2Line[8]) );
 	    LATNT1 LatR1C3B8( .G(WWLW7Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR1Wire6), 
					  .Z(RdCol3Line[8]) );
 	    LATNT1 LatR1C0B9( .G(WWLW4Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[9]) );
 	    LATNT1 LatR1C1B9( .G(WWLW5Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol1Line[9]) );
 	    LATNT1 LatR1C2B9( .G(WWLW6Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR1Wire4), 
					  .Z(RdCol2Line[9]) );
 	    LATNT1 LatR1C3B9( .G(WWLW7Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR1Wire6), 
					  .Z(RdCol3Line[9]) );
 	    LATNT1 LatR1C0B10( .G(WWLW4Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[10]) );
 	    LATNT1 LatR1C1B10( .G(WWLW5Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol1Line[10]) );
 	    LATNT1 LatR1C2B10( .G(WWLW6Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR1Wire4), 
					  .Z(RdCol2Line[10]) );
 	    LATNT1 LatR1C3B10( .G(WWLW7Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR1Wire6), 
					  .Z(RdCol3Line[10]) );
 	    LATNT1 LatR1C0B11( .G(WWLW4Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[11]) );
 	    LATNT1 LatR1C1B11( .G(WWLW5Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol1Line[11]) );
 	    LATNT1 LatR1C2B11( .G(WWLW6Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR1Wire4), 
					  .Z(RdCol2Line[11]) );
 	    LATNT1 LatR1C3B11( .G(WWLW7Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR1Wire6), 
					  .Z(RdCol3Line[11]) );
 	    LATNT1 LatR1C0B12( .G(WWLW4Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[12]) );
 	    LATNT1 LatR1C1B12( .G(WWLW5Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol1Line[12]) );
 	    LATNT1 LatR1C2B12( .G(WWLW6Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR1Wire4), 
					  .Z(RdCol2Line[12]) );
 	    LATNT1 LatR1C3B12( .G(WWLW7Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR1Wire6), 
					  .Z(RdCol3Line[12]) );
 	    LATNT1 LatR1C0B13( .G(WWLW4Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[13]) );
 	    LATNT1 LatR1C1B13( .G(WWLW5Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol1Line[13]) );
 	    LATNT1 LatR1C2B13( .G(WWLW6Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR1Wire4), 
					  .Z(RdCol2Line[13]) );
 	    LATNT1 LatR1C3B13( .G(WWLW7Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR1Wire6), 
					  .Z(RdCol3Line[13]) );
 	    LATNT1 LatR1C0B14( .G(WWLW4Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[14]) );
 	    LATNT1 LatR1C1B14( .G(WWLW5Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol1Line[14]) );
 	    LATNT1 LatR1C2B14( .G(WWLW6Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR1Wire4), 
					  .Z(RdCol2Line[14]) );
 	    LATNT1 LatR1C3B14( .G(WWLW7Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR1Wire6), 
					  .Z(RdCol3Line[14]) );
 	    LATNT1 LatR1C0B15( .G(WWLW4Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[15]) );
 	    LATNT1 LatR1C1B15( .G(WWLW5Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol1Line[15]) );
 	    LATNT1 LatR1C2B15( .G(WWLW6Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR1Wire4), 
					  .Z(RdCol2Line[15]) );
 	    LATNT1 LatR1C3B15( .G(WWLW7Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR1Wire6), 
					  .Z(RdCol3Line[15]) );
 	    LATNT1 LatR1C0B16( .G(WWLW4Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[16]) );
 	    LATNT1 LatR1C1B16( .G(WWLW5Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol1Line[16]) );
 	    LATNT1 LatR1C2B16( .G(WWLW6Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR1Wire5), 
					  .Z(RdCol2Line[16]) );
 	    LATNT1 LatR1C3B16( .G(WWLW7Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR1Wire7), 
					  .Z(RdCol3Line[16]) );
 	    LATNT1 LatR1C0B17( .G(WWLW4Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[17]) );
 	    LATNT1 LatR1C1B17( .G(WWLW5Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol1Line[17]) );
 	    LATNT1 LatR1C2B17( .G(WWLW6Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR1Wire5), 
					  .Z(RdCol2Line[17]) );
 	    LATNT1 LatR1C3B17( .G(WWLW7Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR1Wire7), 
					  .Z(RdCol3Line[17]) );
 	    LATNT1 LatR1C0B18( .G(WWLW4Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[18]) );
 	    LATNT1 LatR1C1B18( .G(WWLW5Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol1Line[18]) );
 	    LATNT1 LatR1C2B18( .G(WWLW6Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR1Wire5), 
					  .Z(RdCol2Line[18]) );
 	    LATNT1 LatR1C3B18( .G(WWLW7Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR1Wire7), 
					  .Z(RdCol3Line[18]) );
 	    LATNT1 LatR1C0B19( .G(WWLW4Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[19]) );
 	    LATNT1 LatR1C1B19( .G(WWLW5Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol1Line[19]) );
 	    LATNT1 LatR1C2B19( .G(WWLW6Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR1Wire5), 
					  .Z(RdCol2Line[19]) );
 	    LATNT1 LatR1C3B19( .G(WWLW7Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR1Wire7), 
					  .Z(RdCol3Line[19]) );
 	    LATNT1 LatR1C0B20( .G(WWLW4Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[20]) );
 	    LATNT1 LatR1C1B20( .G(WWLW5Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol1Line[20]) );
 	    LATNT1 LatR1C2B20( .G(WWLW6Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR1Wire5), 
					  .Z(RdCol2Line[20]) );
 	    LATNT1 LatR1C3B20( .G(WWLW7Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR1Wire7), 
					  .Z(RdCol3Line[20]) );
 	    LATNT1 LatR1C0B21( .G(WWLW4Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[21]) );
 	    LATNT1 LatR1C1B21( .G(WWLW5Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol1Line[21]) );
 	    LATNT1 LatR1C2B21( .G(WWLW6Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR1Wire5), 
					  .Z(RdCol2Line[21]) );
 	    LATNT1 LatR1C3B21( .G(WWLW7Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR1Wire7), 
					  .Z(RdCol3Line[21]) );
 	    LATNT1 LatR1C0B22( .G(WWLW4Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[22]) );
 	    LATNT1 LatR1C1B22( .G(WWLW5Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol1Line[22]) );
 	    LATNT1 LatR1C2B22( .G(WWLW6Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR1Wire5), 
					  .Z(RdCol2Line[22]) );
 	    LATNT1 LatR1C3B22( .G(WWLW7Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR1Wire7), 
					  .Z(RdCol3Line[22]) );
 	    LATNT1 LatR1C0B23( .G(WWLW4Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[23]) );
 	    LATNT1 LatR1C1B23( .G(WWLW5Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol1Line[23]) );
 	    LATNT1 LatR1C2B23( .G(WWLW6Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR1Wire5), 
					  .Z(RdCol2Line[23]) );
 	    LATNT1 LatR1C3B23( .G(WWLW7Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR1Wire7), 
					  .Z(RdCol3Line[23]) );
 	    LATNT1 LatR1C0B24( .G(WWLW4Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[24]) );
 	    LATNT1 LatR1C1B24( .G(WWLW5Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol1Line[24]) );
 	    LATNT1 LatR1C2B24( .G(WWLW6Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR1Wire5), 
					  .Z(RdCol2Line[24]) );
 	    LATNT1 LatR1C3B24( .G(WWLW7Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR1Wire7), 
					  .Z(RdCol3Line[24]) );
 	    LATNT1 LatR1C0B25( .G(WWLW4Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[25]) );
 	    LATNT1 LatR1C1B25( .G(WWLW5Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol1Line[25]) );
 	    LATNT1 LatR1C2B25( .G(WWLW6Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR1Wire5), 
					  .Z(RdCol2Line[25]) );
 	    LATNT1 LatR1C3B25( .G(WWLW7Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR1Wire7), 
					  .Z(RdCol3Line[25]) );
 	    LATNT1 LatR1C0B26( .G(WWLW4Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[26]) );
 	    LATNT1 LatR1C1B26( .G(WWLW5Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol1Line[26]) );
 	    LATNT1 LatR1C2B26( .G(WWLW6Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR1Wire5), 
					  .Z(RdCol2Line[26]) );
 	    LATNT1 LatR1C3B26( .G(WWLW7Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR1Wire7), 
					  .Z(RdCol3Line[26]) );
 	    LATNT1 LatR1C0B27( .G(WWLW4Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[27]) );
 	    LATNT1 LatR1C1B27( .G(WWLW5Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol1Line[27]) );
 	    LATNT1 LatR1C2B27( .G(WWLW6Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR1Wire5), 
					  .Z(RdCol2Line[27]) );
 	    LATNT1 LatR1C3B27( .G(WWLW7Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR1Wire7), 
					  .Z(RdCol3Line[27]) );
 	    LATNT1 LatR1C0B28( .G(WWLW4Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[28]) );
 	    LATNT1 LatR1C1B28( .G(WWLW5Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol1Line[28]) );
 	    LATNT1 LatR1C2B28( .G(WWLW6Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR1Wire5), 
					  .Z(RdCol2Line[28]) );
 	    LATNT1 LatR1C3B28( .G(WWLW7Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR1Wire7), 
					  .Z(RdCol3Line[28]) );
 	    LATNT1 LatR1C0B29( .G(WWLW4Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[29]) );
 	    LATNT1 LatR1C1B29( .G(WWLW5Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol1Line[29]) );
 	    LATNT1 LatR1C2B29( .G(WWLW6Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR1Wire5), 
					  .Z(RdCol2Line[29]) );
 	    LATNT1 LatR1C3B29( .G(WWLW7Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR1Wire7), 
					  .Z(RdCol3Line[29]) );
 	    LATNT1 LatR1C0B30( .G(WWLW4Wire1), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[30]) );
 	    LATNT1 LatR1C1B30( .G(WWLW5Wire1), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol1Line[30]) );
 	    LATNT1 LatR1C2B30( .G(WWLW6Wire1), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR1Wire5), 
					  .Z(RdCol2Line[30]) );
 	    LATNT1 LatR1C3B30( .G(WWLW7Wire1), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR1Wire7), 
					  .Z(RdCol3Line[30]) );
 	    LATNT1 LatR1C0B31( .G(WWLW4Wire1), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[31]) );
 	    LATNT1 LatR1C1B31( .G(WWLW5Wire1), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol1Line[31]) );
 	    LATNT1 LatR1C2B31( .G(WWLW6Wire1), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR1Wire5), 
					  .Z(RdCol2Line[31]) );
 	    LATNT1 LatR1C3B31( .G(WWLW7Wire1), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR1Wire7), 
					  .Z(RdCol3Line[31]) );
     
            //
            // Row 2
	    //
	      
 	    LATNT1 LatR2C0B0( .G(WWLW8Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR2C1B0( .G(WWLW9Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol1Line[0]) );
 	    LATNT1 LatR2C2B0( .G(WWLW10Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR2Wire4), 
					  .Z(RdCol2Line[0]) );
 	    LATNT1 LatR2C3B0( .G(WWLW11Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR2Wire6), 
					  .Z(RdCol3Line[0]) );
 	    LATNT1 LatR2C0B1( .G(WWLW8Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR2C1B1( .G(WWLW9Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol1Line[1]) );
 	    LATNT1 LatR2C2B1( .G(WWLW10Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR2Wire4), 
					  .Z(RdCol2Line[1]) );
 	    LATNT1 LatR2C3B1( .G(WWLW11Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR2Wire6), 
					  .Z(RdCol3Line[1]) );
 	    LATNT1 LatR2C0B2( .G(WWLW8Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR2C1B2( .G(WWLW9Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol1Line[2]) );
 	    LATNT1 LatR2C2B2( .G(WWLW10Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR2Wire4), 
					  .Z(RdCol2Line[2]) );
 	    LATNT1 LatR2C3B2( .G(WWLW11Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR2Wire6), 
					  .Z(RdCol3Line[2]) );
 	    LATNT1 LatR2C0B3( .G(WWLW8Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR2C1B3( .G(WWLW9Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol1Line[3]) );
 	    LATNT1 LatR2C2B3( .G(WWLW10Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR2Wire4), 
					  .Z(RdCol2Line[3]) );
 	    LATNT1 LatR2C3B3( .G(WWLW11Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR2Wire6), 
					  .Z(RdCol3Line[3]) );
 	    LATNT1 LatR2C0B4( .G(WWLW8Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[4]) );
 	    LATNT1 LatR2C1B4( .G(WWLW9Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol1Line[4]) );
 	    LATNT1 LatR2C2B4( .G(WWLW10Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR2Wire4), 
					  .Z(RdCol2Line[4]) );
 	    LATNT1 LatR2C3B4( .G(WWLW11Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR2Wire6), 
					  .Z(RdCol3Line[4]) );
 	    LATNT1 LatR2C0B5( .G(WWLW8Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[5]) );
 	    LATNT1 LatR2C1B5( .G(WWLW9Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol1Line[5]) );
 	    LATNT1 LatR2C2B5( .G(WWLW10Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR2Wire4), 
					  .Z(RdCol2Line[5]) );
 	    LATNT1 LatR2C3B5( .G(WWLW11Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR2Wire6), 
					  .Z(RdCol3Line[5]) );
 	    LATNT1 LatR2C0B6( .G(WWLW8Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[6]) );
 	    LATNT1 LatR2C1B6( .G(WWLW9Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol1Line[6]) );
 	    LATNT1 LatR2C2B6( .G(WWLW10Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR2Wire4), 
					  .Z(RdCol2Line[6]) );
 	    LATNT1 LatR2C3B6( .G(WWLW11Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR2Wire6), 
					  .Z(RdCol3Line[6]) );
 	    LATNT1 LatR2C0B7( .G(WWLW8Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[7]) );
 	    LATNT1 LatR2C1B7( .G(WWLW9Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol1Line[7]) );
 	    LATNT1 LatR2C2B7( .G(WWLW10Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR2Wire4), 
					  .Z(RdCol2Line[7]) );
 	    LATNT1 LatR2C3B7( .G(WWLW11Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR2Wire6), 
					  .Z(RdCol3Line[7]) );
 	    LATNT1 LatR2C0B8( .G(WWLW8Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[8]) );
 	    LATNT1 LatR2C1B8( .G(WWLW9Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol1Line[8]) );
 	    LATNT1 LatR2C2B8( .G(WWLW10Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR2Wire4), 
					  .Z(RdCol2Line[8]) );
 	    LATNT1 LatR2C3B8( .G(WWLW11Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR2Wire6), 
					  .Z(RdCol3Line[8]) );
 	    LATNT1 LatR2C0B9( .G(WWLW8Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[9]) );
 	    LATNT1 LatR2C1B9( .G(WWLW9Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol1Line[9]) );
 	    LATNT1 LatR2C2B9( .G(WWLW10Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR2Wire4), 
					  .Z(RdCol2Line[9]) );
 	    LATNT1 LatR2C3B9( .G(WWLW11Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR2Wire6), 
					  .Z(RdCol3Line[9]) );
 	    LATNT1 LatR2C0B10( .G(WWLW8Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[10]) );
 	    LATNT1 LatR2C1B10( .G(WWLW9Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol1Line[10]) );
 	    LATNT1 LatR2C2B10( .G(WWLW10Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR2Wire4), 
					  .Z(RdCol2Line[10]) );
 	    LATNT1 LatR2C3B10( .G(WWLW11Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR2Wire6), 
					  .Z(RdCol3Line[10]) );
 	    LATNT1 LatR2C0B11( .G(WWLW8Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[11]) );
 	    LATNT1 LatR2C1B11( .G(WWLW9Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol1Line[11]) );
 	    LATNT1 LatR2C2B11( .G(WWLW10Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR2Wire4), 
					  .Z(RdCol2Line[11]) );
 	    LATNT1 LatR2C3B11( .G(WWLW11Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR2Wire6), 
					  .Z(RdCol3Line[11]) );
 	    LATNT1 LatR2C0B12( .G(WWLW8Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[12]) );
 	    LATNT1 LatR2C1B12( .G(WWLW9Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol1Line[12]) );
 	    LATNT1 LatR2C2B12( .G(WWLW10Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR2Wire4), 
					  .Z(RdCol2Line[12]) );
 	    LATNT1 LatR2C3B12( .G(WWLW11Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR2Wire6), 
					  .Z(RdCol3Line[12]) );
 	    LATNT1 LatR2C0B13( .G(WWLW8Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[13]) );
 	    LATNT1 LatR2C1B13( .G(WWLW9Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol1Line[13]) );
 	    LATNT1 LatR2C2B13( .G(WWLW10Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR2Wire4), 
					  .Z(RdCol2Line[13]) );
 	    LATNT1 LatR2C3B13( .G(WWLW11Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR2Wire6), 
					  .Z(RdCol3Line[13]) );
 	    LATNT1 LatR2C0B14( .G(WWLW8Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[14]) );
 	    LATNT1 LatR2C1B14( .G(WWLW9Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol1Line[14]) );
 	    LATNT1 LatR2C2B14( .G(WWLW10Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR2Wire4), 
					  .Z(RdCol2Line[14]) );
 	    LATNT1 LatR2C3B14( .G(WWLW11Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR2Wire6), 
					  .Z(RdCol3Line[14]) );
 	    LATNT1 LatR2C0B15( .G(WWLW8Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[15]) );
 	    LATNT1 LatR2C1B15( .G(WWLW9Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol1Line[15]) );
 	    LATNT1 LatR2C2B15( .G(WWLW10Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR2Wire4), 
					  .Z(RdCol2Line[15]) );
 	    LATNT1 LatR2C3B15( .G(WWLW11Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR2Wire6), 
					  .Z(RdCol3Line[15]) );
 	    LATNT1 LatR2C0B16( .G(WWLW8Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[16]) );
 	    LATNT1 LatR2C1B16( .G(WWLW9Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol1Line[16]) );
 	    LATNT1 LatR2C2B16( .G(WWLW10Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR2Wire5), 
					  .Z(RdCol2Line[16]) );
 	    LATNT1 LatR2C3B16( .G(WWLW11Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR2Wire7), 
					  .Z(RdCol3Line[16]) );
 	    LATNT1 LatR2C0B17( .G(WWLW8Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[17]) );
 	    LATNT1 LatR2C1B17( .G(WWLW9Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol1Line[17]) );
 	    LATNT1 LatR2C2B17( .G(WWLW10Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR2Wire5), 
					  .Z(RdCol2Line[17]) );
 	    LATNT1 LatR2C3B17( .G(WWLW11Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR2Wire7), 
					  .Z(RdCol3Line[17]) );
 	    LATNT1 LatR2C0B18( .G(WWLW8Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[18]) );
 	    LATNT1 LatR2C1B18( .G(WWLW9Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol1Line[18]) );
 	    LATNT1 LatR2C2B18( .G(WWLW10Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR2Wire5), 
					  .Z(RdCol2Line[18]) );
 	    LATNT1 LatR2C3B18( .G(WWLW11Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR2Wire7), 
					  .Z(RdCol3Line[18]) );
 	    LATNT1 LatR2C0B19( .G(WWLW8Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[19]) );
 	    LATNT1 LatR2C1B19( .G(WWLW9Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol1Line[19]) );
 	    LATNT1 LatR2C2B19( .G(WWLW10Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR2Wire5), 
					  .Z(RdCol2Line[19]) );
 	    LATNT1 LatR2C3B19( .G(WWLW11Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR2Wire7), 
					  .Z(RdCol3Line[19]) );
 	    LATNT1 LatR2C0B20( .G(WWLW8Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[20]) );
 	    LATNT1 LatR2C1B20( .G(WWLW9Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol1Line[20]) );
 	    LATNT1 LatR2C2B20( .G(WWLW10Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR2Wire5), 
					  .Z(RdCol2Line[20]) );
 	    LATNT1 LatR2C3B20( .G(WWLW11Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR2Wire7), 
					  .Z(RdCol3Line[20]) );
 	    LATNT1 LatR2C0B21( .G(WWLW8Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[21]) );
 	    LATNT1 LatR2C1B21( .G(WWLW9Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol1Line[21]) );
 	    LATNT1 LatR2C2B21( .G(WWLW10Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR2Wire5), 
					  .Z(RdCol2Line[21]) );
 	    LATNT1 LatR2C3B21( .G(WWLW11Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR2Wire7), 
					  .Z(RdCol3Line[21]) );
 	    LATNT1 LatR2C0B22( .G(WWLW8Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[22]) );
 	    LATNT1 LatR2C1B22( .G(WWLW9Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol1Line[22]) );
 	    LATNT1 LatR2C2B22( .G(WWLW10Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR2Wire5), 
					  .Z(RdCol2Line[22]) );
 	    LATNT1 LatR2C3B22( .G(WWLW11Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR2Wire7), 
					  .Z(RdCol3Line[22]) );
 	    LATNT1 LatR2C0B23( .G(WWLW8Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[23]) );
 	    LATNT1 LatR2C1B23( .G(WWLW9Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol1Line[23]) );
 	    LATNT1 LatR2C2B23( .G(WWLW10Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR2Wire5), 
					  .Z(RdCol2Line[23]) );
 	    LATNT1 LatR2C3B23( .G(WWLW11Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR2Wire7), 
					  .Z(RdCol3Line[23]) );
 	    LATNT1 LatR2C0B24( .G(WWLW8Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[24]) );
 	    LATNT1 LatR2C1B24( .G(WWLW9Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol1Line[24]) );
 	    LATNT1 LatR2C2B24( .G(WWLW10Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR2Wire5), 
					  .Z(RdCol2Line[24]) );
 	    LATNT1 LatR2C3B24( .G(WWLW11Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR2Wire7), 
					  .Z(RdCol3Line[24]) );
 	    LATNT1 LatR2C0B25( .G(WWLW8Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[25]) );
 	    LATNT1 LatR2C1B25( .G(WWLW9Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol1Line[25]) );
 	    LATNT1 LatR2C2B25( .G(WWLW10Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR2Wire5), 
					  .Z(RdCol2Line[25]) );
 	    LATNT1 LatR2C3B25( .G(WWLW11Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR2Wire7), 
					  .Z(RdCol3Line[25]) );
 	    LATNT1 LatR2C0B26( .G(WWLW8Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[26]) );
 	    LATNT1 LatR2C1B26( .G(WWLW9Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol1Line[26]) );
 	    LATNT1 LatR2C2B26( .G(WWLW10Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR2Wire5), 
					  .Z(RdCol2Line[26]) );
 	    LATNT1 LatR2C3B26( .G(WWLW11Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR2Wire7), 
					  .Z(RdCol3Line[26]) );
 	    LATNT1 LatR2C0B27( .G(WWLW8Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[27]) );
 	    LATNT1 LatR2C1B27( .G(WWLW9Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol1Line[27]) );
 	    LATNT1 LatR2C2B27( .G(WWLW10Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR2Wire5), 
					  .Z(RdCol2Line[27]) );
 	    LATNT1 LatR2C3B27( .G(WWLW11Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR2Wire7), 
					  .Z(RdCol3Line[27]) );
 	    LATNT1 LatR2C0B28( .G(WWLW8Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[28]) );
 	    LATNT1 LatR2C1B28( .G(WWLW9Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol1Line[28]) );
 	    LATNT1 LatR2C2B28( .G(WWLW10Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR2Wire5), 
					  .Z(RdCol2Line[28]) );
 	    LATNT1 LatR2C3B28( .G(WWLW11Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR2Wire7), 
					  .Z(RdCol3Line[28]) );
 	    LATNT1 LatR2C0B29( .G(WWLW8Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[29]) );
 	    LATNT1 LatR2C1B29( .G(WWLW9Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol1Line[29]) );
 	    LATNT1 LatR2C2B29( .G(WWLW10Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR2Wire5), 
					  .Z(RdCol2Line[29]) );
 	    LATNT1 LatR2C3B29( .G(WWLW11Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR2Wire7), 
					  .Z(RdCol3Line[29]) );
 	    LATNT1 LatR2C0B30( .G(WWLW8Wire1), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[30]) );
 	    LATNT1 LatR2C1B30( .G(WWLW9Wire1), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol1Line[30]) );
 	    LATNT1 LatR2C2B30( .G(WWLW10Wire1), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR2Wire5), 
					  .Z(RdCol2Line[30]) );
 	    LATNT1 LatR2C3B30( .G(WWLW11Wire1), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR2Wire7), 
					  .Z(RdCol3Line[30]) );
 	    LATNT1 LatR2C0B31( .G(WWLW8Wire1), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[31]) );
 	    LATNT1 LatR2C1B31( .G(WWLW9Wire1), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol1Line[31]) );
 	    LATNT1 LatR2C2B31( .G(WWLW10Wire1), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR2Wire5), 
					  .Z(RdCol2Line[31]) );
 	    LATNT1 LatR2C3B31( .G(WWLW11Wire1), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR2Wire7), 
					  .Z(RdCol3Line[31]) );
     
            //
            // Row 3
	    //
	      
 	    LATNT1 LatR3C0B0( .G(WWLW12Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR3C1B0( .G(WWLW13Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol1Line[0]) );
 	    LATNT1 LatR3C2B0( .G(WWLW14Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR3Wire4), 
					  .Z(RdCol2Line[0]) );
 	    LATNT1 LatR3C3B0( .G(WWLW15Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR3Wire6), 
					  .Z(RdCol3Line[0]) );
 	    LATNT1 LatR3C0B1( .G(WWLW12Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR3C1B1( .G(WWLW13Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol1Line[1]) );
 	    LATNT1 LatR3C2B1( .G(WWLW14Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR3Wire4), 
					  .Z(RdCol2Line[1]) );
 	    LATNT1 LatR3C3B1( .G(WWLW15Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR3Wire6), 
					  .Z(RdCol3Line[1]) );
 	    LATNT1 LatR3C0B2( .G(WWLW12Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR3C1B2( .G(WWLW13Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol1Line[2]) );
 	    LATNT1 LatR3C2B2( .G(WWLW14Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR3Wire4), 
					  .Z(RdCol2Line[2]) );
 	    LATNT1 LatR3C3B2( .G(WWLW15Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR3Wire6), 
					  .Z(RdCol3Line[2]) );
 	    LATNT1 LatR3C0B3( .G(WWLW12Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR3C1B3( .G(WWLW13Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol1Line[3]) );
 	    LATNT1 LatR3C2B3( .G(WWLW14Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR3Wire4), 
					  .Z(RdCol2Line[3]) );
 	    LATNT1 LatR3C3B3( .G(WWLW15Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR3Wire6), 
					  .Z(RdCol3Line[3]) );
 	    LATNT1 LatR3C0B4( .G(WWLW12Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[4]) );
 	    LATNT1 LatR3C1B4( .G(WWLW13Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol1Line[4]) );
 	    LATNT1 LatR3C2B4( .G(WWLW14Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR3Wire4), 
					  .Z(RdCol2Line[4]) );
 	    LATNT1 LatR3C3B4( .G(WWLW15Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR3Wire6), 
					  .Z(RdCol3Line[4]) );
 	    LATNT1 LatR3C0B5( .G(WWLW12Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[5]) );
 	    LATNT1 LatR3C1B5( .G(WWLW13Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol1Line[5]) );
 	    LATNT1 LatR3C2B5( .G(WWLW14Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR3Wire4), 
					  .Z(RdCol2Line[5]) );
 	    LATNT1 LatR3C3B5( .G(WWLW15Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR3Wire6), 
					  .Z(RdCol3Line[5]) );
 	    LATNT1 LatR3C0B6( .G(WWLW12Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[6]) );
 	    LATNT1 LatR3C1B6( .G(WWLW13Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol1Line[6]) );
 	    LATNT1 LatR3C2B6( .G(WWLW14Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR3Wire4), 
					  .Z(RdCol2Line[6]) );
 	    LATNT1 LatR3C3B6( .G(WWLW15Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR3Wire6), 
					  .Z(RdCol3Line[6]) );
 	    LATNT1 LatR3C0B7( .G(WWLW12Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[7]) );
 	    LATNT1 LatR3C1B7( .G(WWLW13Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol1Line[7]) );
 	    LATNT1 LatR3C2B7( .G(WWLW14Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR3Wire4), 
					  .Z(RdCol2Line[7]) );
 	    LATNT1 LatR3C3B7( .G(WWLW15Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR3Wire6), 
					  .Z(RdCol3Line[7]) );
 	    LATNT1 LatR3C0B8( .G(WWLW12Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[8]) );
 	    LATNT1 LatR3C1B8( .G(WWLW13Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol1Line[8]) );
 	    LATNT1 LatR3C2B8( .G(WWLW14Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR3Wire4), 
					  .Z(RdCol2Line[8]) );
 	    LATNT1 LatR3C3B8( .G(WWLW15Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR3Wire6), 
					  .Z(RdCol3Line[8]) );
 	    LATNT1 LatR3C0B9( .G(WWLW12Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[9]) );
 	    LATNT1 LatR3C1B9( .G(WWLW13Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol1Line[9]) );
 	    LATNT1 LatR3C2B9( .G(WWLW14Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR3Wire4), 
					  .Z(RdCol2Line[9]) );
 	    LATNT1 LatR3C3B9( .G(WWLW15Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR3Wire6), 
					  .Z(RdCol3Line[9]) );
 	    LATNT1 LatR3C0B10( .G(WWLW12Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[10]) );
 	    LATNT1 LatR3C1B10( .G(WWLW13Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol1Line[10]) );
 	    LATNT1 LatR3C2B10( .G(WWLW14Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR3Wire4), 
					  .Z(RdCol2Line[10]) );
 	    LATNT1 LatR3C3B10( .G(WWLW15Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR3Wire6), 
					  .Z(RdCol3Line[10]) );
 	    LATNT1 LatR3C0B11( .G(WWLW12Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[11]) );
 	    LATNT1 LatR3C1B11( .G(WWLW13Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol1Line[11]) );
 	    LATNT1 LatR3C2B11( .G(WWLW14Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR3Wire4), 
					  .Z(RdCol2Line[11]) );
 	    LATNT1 LatR3C3B11( .G(WWLW15Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR3Wire6), 
					  .Z(RdCol3Line[11]) );
 	    LATNT1 LatR3C0B12( .G(WWLW12Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[12]) );
 	    LATNT1 LatR3C1B12( .G(WWLW13Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol1Line[12]) );
 	    LATNT1 LatR3C2B12( .G(WWLW14Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR3Wire4), 
					  .Z(RdCol2Line[12]) );
 	    LATNT1 LatR3C3B12( .G(WWLW15Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR3Wire6), 
					  .Z(RdCol3Line[12]) );
 	    LATNT1 LatR3C0B13( .G(WWLW12Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[13]) );
 	    LATNT1 LatR3C1B13( .G(WWLW13Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol1Line[13]) );
 	    LATNT1 LatR3C2B13( .G(WWLW14Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR3Wire4), 
					  .Z(RdCol2Line[13]) );
 	    LATNT1 LatR3C3B13( .G(WWLW15Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR3Wire6), 
					  .Z(RdCol3Line[13]) );
 	    LATNT1 LatR3C0B14( .G(WWLW12Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[14]) );
 	    LATNT1 LatR3C1B14( .G(WWLW13Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol1Line[14]) );
 	    LATNT1 LatR3C2B14( .G(WWLW14Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR3Wire4), 
					  .Z(RdCol2Line[14]) );
 	    LATNT1 LatR3C3B14( .G(WWLW15Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR3Wire6), 
					  .Z(RdCol3Line[14]) );
 	    LATNT1 LatR3C0B15( .G(WWLW12Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[15]) );
 	    LATNT1 LatR3C1B15( .G(WWLW13Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol1Line[15]) );
 	    LATNT1 LatR3C2B15( .G(WWLW14Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR3Wire4), 
					  .Z(RdCol2Line[15]) );
 	    LATNT1 LatR3C3B15( .G(WWLW15Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR3Wire6), 
					  .Z(RdCol3Line[15]) );
 	    LATNT1 LatR3C0B16( .G(WWLW12Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[16]) );
 	    LATNT1 LatR3C1B16( .G(WWLW13Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol1Line[16]) );
 	    LATNT1 LatR3C2B16( .G(WWLW14Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR3Wire5), 
					  .Z(RdCol2Line[16]) );
 	    LATNT1 LatR3C3B16( .G(WWLW15Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR3Wire7), 
					  .Z(RdCol3Line[16]) );
 	    LATNT1 LatR3C0B17( .G(WWLW12Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[17]) );
 	    LATNT1 LatR3C1B17( .G(WWLW13Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol1Line[17]) );
 	    LATNT1 LatR3C2B17( .G(WWLW14Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR3Wire5), 
					  .Z(RdCol2Line[17]) );
 	    LATNT1 LatR3C3B17( .G(WWLW15Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR3Wire7), 
					  .Z(RdCol3Line[17]) );
 	    LATNT1 LatR3C0B18( .G(WWLW12Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[18]) );
 	    LATNT1 LatR3C1B18( .G(WWLW13Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol1Line[18]) );
 	    LATNT1 LatR3C2B18( .G(WWLW14Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR3Wire5), 
					  .Z(RdCol2Line[18]) );
 	    LATNT1 LatR3C3B18( .G(WWLW15Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR3Wire7), 
					  .Z(RdCol3Line[18]) );
 	    LATNT1 LatR3C0B19( .G(WWLW12Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[19]) );
 	    LATNT1 LatR3C1B19( .G(WWLW13Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol1Line[19]) );
 	    LATNT1 LatR3C2B19( .G(WWLW14Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR3Wire5), 
					  .Z(RdCol2Line[19]) );
 	    LATNT1 LatR3C3B19( .G(WWLW15Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR3Wire7), 
					  .Z(RdCol3Line[19]) );
 	    LATNT1 LatR3C0B20( .G(WWLW12Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[20]) );
 	    LATNT1 LatR3C1B20( .G(WWLW13Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol1Line[20]) );
 	    LATNT1 LatR3C2B20( .G(WWLW14Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR3Wire5), 
					  .Z(RdCol2Line[20]) );
 	    LATNT1 LatR3C3B20( .G(WWLW15Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR3Wire7), 
					  .Z(RdCol3Line[20]) );
 	    LATNT1 LatR3C0B21( .G(WWLW12Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[21]) );
 	    LATNT1 LatR3C1B21( .G(WWLW13Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol1Line[21]) );
 	    LATNT1 LatR3C2B21( .G(WWLW14Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR3Wire5), 
					  .Z(RdCol2Line[21]) );
 	    LATNT1 LatR3C3B21( .G(WWLW15Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR3Wire7), 
					  .Z(RdCol3Line[21]) );
 	    LATNT1 LatR3C0B22( .G(WWLW12Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[22]) );
 	    LATNT1 LatR3C1B22( .G(WWLW13Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol1Line[22]) );
 	    LATNT1 LatR3C2B22( .G(WWLW14Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR3Wire5), 
					  .Z(RdCol2Line[22]) );
 	    LATNT1 LatR3C3B22( .G(WWLW15Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR3Wire7), 
					  .Z(RdCol3Line[22]) );
 	    LATNT1 LatR3C0B23( .G(WWLW12Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[23]) );
 	    LATNT1 LatR3C1B23( .G(WWLW13Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol1Line[23]) );
 	    LATNT1 LatR3C2B23( .G(WWLW14Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR3Wire5), 
					  .Z(RdCol2Line[23]) );
 	    LATNT1 LatR3C3B23( .G(WWLW15Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR3Wire7), 
					  .Z(RdCol3Line[23]) );
 	    LATNT1 LatR3C0B24( .G(WWLW12Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[24]) );
 	    LATNT1 LatR3C1B24( .G(WWLW13Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol1Line[24]) );
 	    LATNT1 LatR3C2B24( .G(WWLW14Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR3Wire5), 
					  .Z(RdCol2Line[24]) );
 	    LATNT1 LatR3C3B24( .G(WWLW15Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR3Wire7), 
					  .Z(RdCol3Line[24]) );
 	    LATNT1 LatR3C0B25( .G(WWLW12Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[25]) );
 	    LATNT1 LatR3C1B25( .G(WWLW13Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol1Line[25]) );
 	    LATNT1 LatR3C2B25( .G(WWLW14Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR3Wire5), 
					  .Z(RdCol2Line[25]) );
 	    LATNT1 LatR3C3B25( .G(WWLW15Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR3Wire7), 
					  .Z(RdCol3Line[25]) );
 	    LATNT1 LatR3C0B26( .G(WWLW12Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[26]) );
 	    LATNT1 LatR3C1B26( .G(WWLW13Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol1Line[26]) );
 	    LATNT1 LatR3C2B26( .G(WWLW14Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR3Wire5), 
					  .Z(RdCol2Line[26]) );
 	    LATNT1 LatR3C3B26( .G(WWLW15Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR3Wire7), 
					  .Z(RdCol3Line[26]) );
 	    LATNT1 LatR3C0B27( .G(WWLW12Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[27]) );
 	    LATNT1 LatR3C1B27( .G(WWLW13Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol1Line[27]) );
 	    LATNT1 LatR3C2B27( .G(WWLW14Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR3Wire5), 
					  .Z(RdCol2Line[27]) );
 	    LATNT1 LatR3C3B27( .G(WWLW15Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR3Wire7), 
					  .Z(RdCol3Line[27]) );
 	    LATNT1 LatR3C0B28( .G(WWLW12Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[28]) );
 	    LATNT1 LatR3C1B28( .G(WWLW13Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol1Line[28]) );
 	    LATNT1 LatR3C2B28( .G(WWLW14Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR3Wire5), 
					  .Z(RdCol2Line[28]) );
 	    LATNT1 LatR3C3B28( .G(WWLW15Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR3Wire7), 
					  .Z(RdCol3Line[28]) );
 	    LATNT1 LatR3C0B29( .G(WWLW12Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[29]) );
 	    LATNT1 LatR3C1B29( .G(WWLW13Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol1Line[29]) );
 	    LATNT1 LatR3C2B29( .G(WWLW14Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR3Wire5), 
					  .Z(RdCol2Line[29]) );
 	    LATNT1 LatR3C3B29( .G(WWLW15Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR3Wire7), 
					  .Z(RdCol3Line[29]) );
 	    LATNT1 LatR3C0B30( .G(WWLW12Wire1), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[30]) );
 	    LATNT1 LatR3C1B30( .G(WWLW13Wire1), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol1Line[30]) );
 	    LATNT1 LatR3C2B30( .G(WWLW14Wire1), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR3Wire5), 
					  .Z(RdCol2Line[30]) );
 	    LATNT1 LatR3C3B30( .G(WWLW15Wire1), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR3Wire7), 
					  .Z(RdCol3Line[30]) );
 	    LATNT1 LatR3C0B31( .G(WWLW12Wire1), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[31]) );
 	    LATNT1 LatR3C1B31( .G(WWLW13Wire1), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol1Line[31]) );
 	    LATNT1 LatR3C2B31( .G(WWLW14Wire1), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR3Wire5), 
					  .Z(RdCol2Line[31]) );
 	    LATNT1 LatR3C3B31( .G(WWLW15Wire1), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR3Wire7), 
					  .Z(RdCol3Line[31]) );
     
            //
            // Row 4
	    //
	      
 	    LATNT1 LatR4C0B0( .G(WWLW16Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR4C1B0( .G(WWLW17Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol1Line[0]) );
 	    LATNT1 LatR4C2B0( .G(WWLW18Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR4Wire4), 
					  .Z(RdCol2Line[0]) );
 	    LATNT1 LatR4C3B0( .G(WWLW19Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR4Wire6), 
					  .Z(RdCol3Line[0]) );
 	    LATNT1 LatR4C0B1( .G(WWLW16Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR4C1B1( .G(WWLW17Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol1Line[1]) );
 	    LATNT1 LatR4C2B1( .G(WWLW18Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR4Wire4), 
					  .Z(RdCol2Line[1]) );
 	    LATNT1 LatR4C3B1( .G(WWLW19Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR4Wire6), 
					  .Z(RdCol3Line[1]) );
 	    LATNT1 LatR4C0B2( .G(WWLW16Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR4C1B2( .G(WWLW17Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol1Line[2]) );
 	    LATNT1 LatR4C2B2( .G(WWLW18Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR4Wire4), 
					  .Z(RdCol2Line[2]) );
 	    LATNT1 LatR4C3B2( .G(WWLW19Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR4Wire6), 
					  .Z(RdCol3Line[2]) );
 	    LATNT1 LatR4C0B3( .G(WWLW16Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR4C1B3( .G(WWLW17Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol1Line[3]) );
 	    LATNT1 LatR4C2B3( .G(WWLW18Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR4Wire4), 
					  .Z(RdCol2Line[3]) );
 	    LATNT1 LatR4C3B3( .G(WWLW19Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR4Wire6), 
					  .Z(RdCol3Line[3]) );
 	    LATNT1 LatR4C0B4( .G(WWLW16Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[4]) );
 	    LATNT1 LatR4C1B4( .G(WWLW17Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol1Line[4]) );
 	    LATNT1 LatR4C2B4( .G(WWLW18Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR4Wire4), 
					  .Z(RdCol2Line[4]) );
 	    LATNT1 LatR4C3B4( .G(WWLW19Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR4Wire6), 
					  .Z(RdCol3Line[4]) );
 	    LATNT1 LatR4C0B5( .G(WWLW16Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[5]) );
 	    LATNT1 LatR4C1B5( .G(WWLW17Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol1Line[5]) );
 	    LATNT1 LatR4C2B5( .G(WWLW18Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR4Wire4), 
					  .Z(RdCol2Line[5]) );
 	    LATNT1 LatR4C3B5( .G(WWLW19Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR4Wire6), 
					  .Z(RdCol3Line[5]) );
 	    LATNT1 LatR4C0B6( .G(WWLW16Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[6]) );
 	    LATNT1 LatR4C1B6( .G(WWLW17Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol1Line[6]) );
 	    LATNT1 LatR4C2B6( .G(WWLW18Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR4Wire4), 
					  .Z(RdCol2Line[6]) );
 	    LATNT1 LatR4C3B6( .G(WWLW19Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR4Wire6), 
					  .Z(RdCol3Line[6]) );
 	    LATNT1 LatR4C0B7( .G(WWLW16Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[7]) );
 	    LATNT1 LatR4C1B7( .G(WWLW17Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol1Line[7]) );
 	    LATNT1 LatR4C2B7( .G(WWLW18Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR4Wire4), 
					  .Z(RdCol2Line[7]) );
 	    LATNT1 LatR4C3B7( .G(WWLW19Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR4Wire6), 
					  .Z(RdCol3Line[7]) );
 	    LATNT1 LatR4C0B8( .G(WWLW16Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[8]) );
 	    LATNT1 LatR4C1B8( .G(WWLW17Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol1Line[8]) );
 	    LATNT1 LatR4C2B8( .G(WWLW18Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR4Wire4), 
					  .Z(RdCol2Line[8]) );
 	    LATNT1 LatR4C3B8( .G(WWLW19Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR4Wire6), 
					  .Z(RdCol3Line[8]) );
 	    LATNT1 LatR4C0B9( .G(WWLW16Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[9]) );
 	    LATNT1 LatR4C1B9( .G(WWLW17Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol1Line[9]) );
 	    LATNT1 LatR4C2B9( .G(WWLW18Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR4Wire4), 
					  .Z(RdCol2Line[9]) );
 	    LATNT1 LatR4C3B9( .G(WWLW19Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR4Wire6), 
					  .Z(RdCol3Line[9]) );
 	    LATNT1 LatR4C0B10( .G(WWLW16Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[10]) );
 	    LATNT1 LatR4C1B10( .G(WWLW17Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol1Line[10]) );
 	    LATNT1 LatR4C2B10( .G(WWLW18Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR4Wire4), 
					  .Z(RdCol2Line[10]) );
 	    LATNT1 LatR4C3B10( .G(WWLW19Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR4Wire6), 
					  .Z(RdCol3Line[10]) );
 	    LATNT1 LatR4C0B11( .G(WWLW16Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[11]) );
 	    LATNT1 LatR4C1B11( .G(WWLW17Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol1Line[11]) );
 	    LATNT1 LatR4C2B11( .G(WWLW18Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR4Wire4), 
					  .Z(RdCol2Line[11]) );
 	    LATNT1 LatR4C3B11( .G(WWLW19Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR4Wire6), 
					  .Z(RdCol3Line[11]) );
 	    LATNT1 LatR4C0B12( .G(WWLW16Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[12]) );
 	    LATNT1 LatR4C1B12( .G(WWLW17Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol1Line[12]) );
 	    LATNT1 LatR4C2B12( .G(WWLW18Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR4Wire4), 
					  .Z(RdCol2Line[12]) );
 	    LATNT1 LatR4C3B12( .G(WWLW19Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR4Wire6), 
					  .Z(RdCol3Line[12]) );
 	    LATNT1 LatR4C0B13( .G(WWLW16Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[13]) );
 	    LATNT1 LatR4C1B13( .G(WWLW17Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol1Line[13]) );
 	    LATNT1 LatR4C2B13( .G(WWLW18Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR4Wire4), 
					  .Z(RdCol2Line[13]) );
 	    LATNT1 LatR4C3B13( .G(WWLW19Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR4Wire6), 
					  .Z(RdCol3Line[13]) );
 	    LATNT1 LatR4C0B14( .G(WWLW16Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[14]) );
 	    LATNT1 LatR4C1B14( .G(WWLW17Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol1Line[14]) );
 	    LATNT1 LatR4C2B14( .G(WWLW18Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR4Wire4), 
					  .Z(RdCol2Line[14]) );
 	    LATNT1 LatR4C3B14( .G(WWLW19Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR4Wire6), 
					  .Z(RdCol3Line[14]) );
 	    LATNT1 LatR4C0B15( .G(WWLW16Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[15]) );
 	    LATNT1 LatR4C1B15( .G(WWLW17Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol1Line[15]) );
 	    LATNT1 LatR4C2B15( .G(WWLW18Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR4Wire4), 
					  .Z(RdCol2Line[15]) );
 	    LATNT1 LatR4C3B15( .G(WWLW19Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR4Wire6), 
					  .Z(RdCol3Line[15]) );
 	    LATNT1 LatR4C0B16( .G(WWLW16Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[16]) );
 	    LATNT1 LatR4C1B16( .G(WWLW17Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol1Line[16]) );
 	    LATNT1 LatR4C2B16( .G(WWLW18Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR4Wire5), 
					  .Z(RdCol2Line[16]) );
 	    LATNT1 LatR4C3B16( .G(WWLW19Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR4Wire7), 
					  .Z(RdCol3Line[16]) );
 	    LATNT1 LatR4C0B17( .G(WWLW16Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[17]) );
 	    LATNT1 LatR4C1B17( .G(WWLW17Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol1Line[17]) );
 	    LATNT1 LatR4C2B17( .G(WWLW18Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR4Wire5), 
					  .Z(RdCol2Line[17]) );
 	    LATNT1 LatR4C3B17( .G(WWLW19Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR4Wire7), 
					  .Z(RdCol3Line[17]) );
 	    LATNT1 LatR4C0B18( .G(WWLW16Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[18]) );
 	    LATNT1 LatR4C1B18( .G(WWLW17Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol1Line[18]) );
 	    LATNT1 LatR4C2B18( .G(WWLW18Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR4Wire5), 
					  .Z(RdCol2Line[18]) );
 	    LATNT1 LatR4C3B18( .G(WWLW19Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR4Wire7), 
					  .Z(RdCol3Line[18]) );
 	    LATNT1 LatR4C0B19( .G(WWLW16Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[19]) );
 	    LATNT1 LatR4C1B19( .G(WWLW17Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol1Line[19]) );
 	    LATNT1 LatR4C2B19( .G(WWLW18Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR4Wire5), 
					  .Z(RdCol2Line[19]) );
 	    LATNT1 LatR4C3B19( .G(WWLW19Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR4Wire7), 
					  .Z(RdCol3Line[19]) );
 	    LATNT1 LatR4C0B20( .G(WWLW16Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[20]) );
 	    LATNT1 LatR4C1B20( .G(WWLW17Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol1Line[20]) );
 	    LATNT1 LatR4C2B20( .G(WWLW18Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR4Wire5), 
					  .Z(RdCol2Line[20]) );
 	    LATNT1 LatR4C3B20( .G(WWLW19Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR4Wire7), 
					  .Z(RdCol3Line[20]) );
 	    LATNT1 LatR4C0B21( .G(WWLW16Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[21]) );
 	    LATNT1 LatR4C1B21( .G(WWLW17Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol1Line[21]) );
 	    LATNT1 LatR4C2B21( .G(WWLW18Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR4Wire5), 
					  .Z(RdCol2Line[21]) );
 	    LATNT1 LatR4C3B21( .G(WWLW19Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR4Wire7), 
					  .Z(RdCol3Line[21]) );
 	    LATNT1 LatR4C0B22( .G(WWLW16Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[22]) );
 	    LATNT1 LatR4C1B22( .G(WWLW17Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol1Line[22]) );
 	    LATNT1 LatR4C2B22( .G(WWLW18Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR4Wire5), 
					  .Z(RdCol2Line[22]) );
 	    LATNT1 LatR4C3B22( .G(WWLW19Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR4Wire7), 
					  .Z(RdCol3Line[22]) );
 	    LATNT1 LatR4C0B23( .G(WWLW16Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[23]) );
 	    LATNT1 LatR4C1B23( .G(WWLW17Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol1Line[23]) );
 	    LATNT1 LatR4C2B23( .G(WWLW18Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR4Wire5), 
					  .Z(RdCol2Line[23]) );
 	    LATNT1 LatR4C3B23( .G(WWLW19Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR4Wire7), 
					  .Z(RdCol3Line[23]) );
 	    LATNT1 LatR4C0B24( .G(WWLW16Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[24]) );
 	    LATNT1 LatR4C1B24( .G(WWLW17Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol1Line[24]) );
 	    LATNT1 LatR4C2B24( .G(WWLW18Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR4Wire5), 
					  .Z(RdCol2Line[24]) );
 	    LATNT1 LatR4C3B24( .G(WWLW19Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR4Wire7), 
					  .Z(RdCol3Line[24]) );
 	    LATNT1 LatR4C0B25( .G(WWLW16Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[25]) );
 	    LATNT1 LatR4C1B25( .G(WWLW17Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol1Line[25]) );
 	    LATNT1 LatR4C2B25( .G(WWLW18Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR4Wire5), 
					  .Z(RdCol2Line[25]) );
 	    LATNT1 LatR4C3B25( .G(WWLW19Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR4Wire7), 
					  .Z(RdCol3Line[25]) );
 	    LATNT1 LatR4C0B26( .G(WWLW16Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[26]) );
 	    LATNT1 LatR4C1B26( .G(WWLW17Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol1Line[26]) );
 	    LATNT1 LatR4C2B26( .G(WWLW18Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR4Wire5), 
					  .Z(RdCol2Line[26]) );
 	    LATNT1 LatR4C3B26( .G(WWLW19Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR4Wire7), 
					  .Z(RdCol3Line[26]) );
 	    LATNT1 LatR4C0B27( .G(WWLW16Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[27]) );
 	    LATNT1 LatR4C1B27( .G(WWLW17Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol1Line[27]) );
 	    LATNT1 LatR4C2B27( .G(WWLW18Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR4Wire5), 
					  .Z(RdCol2Line[27]) );
 	    LATNT1 LatR4C3B27( .G(WWLW19Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR4Wire7), 
					  .Z(RdCol3Line[27]) );
 	    LATNT1 LatR4C0B28( .G(WWLW16Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[28]) );
 	    LATNT1 LatR4C1B28( .G(WWLW17Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol1Line[28]) );
 	    LATNT1 LatR4C2B28( .G(WWLW18Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR4Wire5), 
					  .Z(RdCol2Line[28]) );
 	    LATNT1 LatR4C3B28( .G(WWLW19Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR4Wire7), 
					  .Z(RdCol3Line[28]) );
 	    LATNT1 LatR4C0B29( .G(WWLW16Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[29]) );
 	    LATNT1 LatR4C1B29( .G(WWLW17Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol1Line[29]) );
 	    LATNT1 LatR4C2B29( .G(WWLW18Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR4Wire5), 
					  .Z(RdCol2Line[29]) );
 	    LATNT1 LatR4C3B29( .G(WWLW19Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR4Wire7), 
					  .Z(RdCol3Line[29]) );
 	    LATNT1 LatR4C0B30( .G(WWLW16Wire1), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[30]) );
 	    LATNT1 LatR4C1B30( .G(WWLW17Wire1), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol1Line[30]) );
 	    LATNT1 LatR4C2B30( .G(WWLW18Wire1), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR4Wire5), 
					  .Z(RdCol2Line[30]) );
 	    LATNT1 LatR4C3B30( .G(WWLW19Wire1), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR4Wire7), 
					  .Z(RdCol3Line[30]) );
 	    LATNT1 LatR4C0B31( .G(WWLW16Wire1), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[31]) );
 	    LATNT1 LatR4C1B31( .G(WWLW17Wire1), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol1Line[31]) );
 	    LATNT1 LatR4C2B31( .G(WWLW18Wire1), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR4Wire5), 
					  .Z(RdCol2Line[31]) );
 	    LATNT1 LatR4C3B31( .G(WWLW19Wire1), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR4Wire7), 
					  .Z(RdCol3Line[31]) );

//
//
// the psuedo sense amp (and column muxing if necessary)
         wire [1:0] ColSelectBuf0;
         wire [1:0] ColSelectBuf1;
         BUFD7 mx0buf0(.A(ColSelect[0]), .Z(ColSelectBuf0[0]) );
         BUFD7 mx1buf0(.A(ColSelect[1]), .Z(ColSelectBuf1[0]) );
         BUFD7 mx0buf1(.A(ColSelect[0]), .Z(ColSelectBuf0[1]) );
         BUFD7 mx1buf1(.A(ColSelect[1]), .Z(ColSelectBuf1[1]) );
	MUX4D2 rd0mx(.A0(RdCol0Line[0]), .A1(RdCol1Line[0]),
           .A2(RdCol2Line[0]), .A3(RdCol3Line[0]),
           .SL1(ColSelectBuf1[0]), .SL0(ColSelectBuf0[0]), .Z(RdDat[0]) );
	MUX4D2 rd1mx(.A0(RdCol0Line[1]), .A1(RdCol1Line[1]),
           .A2(RdCol2Line[1]), .A3(RdCol3Line[1]),
           .SL1(ColSelectBuf1[1]), .SL0(ColSelectBuf0[1]), .Z(RdDat[1]) );
	MUX4D2 rd2mx(.A0(RdCol0Line[2]), .A1(RdCol1Line[2]),
           .A2(RdCol2Line[2]), .A3(RdCol3Line[2]),
           .SL1(ColSelectBuf1[0]), .SL0(ColSelectBuf0[0]), .Z(RdDat[2]) );
	MUX4D2 rd3mx(.A0(RdCol0Line[3]), .A1(RdCol1Line[3]),
           .A2(RdCol2Line[3]), .A3(RdCol3Line[3]),
           .SL1(ColSelectBuf1[1]), .SL0(ColSelectBuf0[1]), .Z(RdDat[3]) );
	MUX4D2 rd4mx(.A0(RdCol0Line[4]), .A1(RdCol1Line[4]),
           .A2(RdCol2Line[4]), .A3(RdCol3Line[4]),
           .SL1(ColSelectBuf1[0]), .SL0(ColSelectBuf0[0]), .Z(RdDat[4]) );
	MUX4D2 rd5mx(.A0(RdCol0Line[5]), .A1(RdCol1Line[5]),
           .A2(RdCol2Line[5]), .A3(RdCol3Line[5]),
           .SL1(ColSelectBuf1[1]), .SL0(ColSelectBuf0[1]), .Z(RdDat[5]) );
	MUX4D2 rd6mx(.A0(RdCol0Line[6]), .A1(RdCol1Line[6]),
           .A2(RdCol2Line[6]), .A3(RdCol3Line[6]),
           .SL1(ColSelectBuf1[0]), .SL0(ColSelectBuf0[0]), .Z(RdDat[6]) );
	MUX4D2 rd7mx(.A0(RdCol0Line[7]), .A1(RdCol1Line[7]),
           .A2(RdCol2Line[7]), .A3(RdCol3Line[7]),
           .SL1(ColSelectBuf1[1]), .SL0(ColSelectBuf0[1]), .Z(RdDat[7]) );
	MUX4D2 rd8mx(.A0(RdCol0Line[8]), .A1(RdCol1Line[8]),
           .A2(RdCol2Line[8]), .A3(RdCol3Line[8]),
           .SL1(ColSelectBuf1[0]), .SL0(ColSelectBuf0[0]), .Z(RdDat[8]) );
	MUX4D2 rd9mx(.A0(RdCol0Line[9]), .A1(RdCol1Line[9]),
           .A2(RdCol2Line[9]), .A3(RdCol3Line[9]),
           .SL1(ColSelectBuf1[1]), .SL0(ColSelectBuf0[1]), .Z(RdDat[9]) );
	MUX4D2 rd10mx(.A0(RdCol0Line[10]), .A1(RdCol1Line[10]),
           .A2(RdCol2Line[10]), .A3(RdCol3Line[10]),
           .SL1(ColSelectBuf1[0]), .SL0(ColSelectBuf0[0]), .Z(RdDat[10]) );
	MUX4D2 rd11mx(.A0(RdCol0Line[11]), .A1(RdCol1Line[11]),
           .A2(RdCol2Line[11]), .A3(RdCol3Line[11]),
           .SL1(ColSelectBuf1[1]), .SL0(ColSelectBuf0[1]), .Z(RdDat[11]) );
	MUX4D2 rd12mx(.A0(RdCol0Line[12]), .A1(RdCol1Line[12]),
           .A2(RdCol2Line[12]), .A3(RdCol3Line[12]),
           .SL1(ColSelectBuf1[0]), .SL0(ColSelectBuf0[0]), .Z(RdDat[12]) );
	MUX4D2 rd13mx(.A0(RdCol0Line[13]), .A1(RdCol1Line[13]),
           .A2(RdCol2Line[13]), .A3(RdCol3Line[13]),
           .SL1(ColSelectBuf1[1]), .SL0(ColSelectBuf0[1]), .Z(RdDat[13]) );
	MUX4D2 rd14mx(.A0(RdCol0Line[14]), .A1(RdCol1Line[14]),
           .A2(RdCol2Line[14]), .A3(RdCol3Line[14]),
           .SL1(ColSelectBuf1[0]), .SL0(ColSelectBuf0[0]), .Z(RdDat[14]) );
	MUX4D2 rd15mx(.A0(RdCol0Line[15]), .A1(RdCol1Line[15]),
           .A2(RdCol2Line[15]), .A3(RdCol3Line[15]),
           .SL1(ColSelectBuf1[1]), .SL0(ColSelectBuf0[1]), .Z(RdDat[15]) );
	MUX4D2 rd16mx(.A0(RdCol0Line[16]), .A1(RdCol1Line[16]),
           .A2(RdCol2Line[16]), .A3(RdCol3Line[16]),
           .SL1(ColSelectBuf1[0]), .SL0(ColSelectBuf0[0]), .Z(RdDat[16]) );
	MUX4D2 rd17mx(.A0(RdCol0Line[17]), .A1(RdCol1Line[17]),
           .A2(RdCol2Line[17]), .A3(RdCol3Line[17]),
           .SL1(ColSelectBuf1[1]), .SL0(ColSelectBuf0[1]), .Z(RdDat[17]) );
	MUX4D2 rd18mx(.A0(RdCol0Line[18]), .A1(RdCol1Line[18]),
           .A2(RdCol2Line[18]), .A3(RdCol3Line[18]),
           .SL1(ColSelectBuf1[0]), .SL0(ColSelectBuf0[0]), .Z(RdDat[18]) );
	MUX4D2 rd19mx(.A0(RdCol0Line[19]), .A1(RdCol1Line[19]),
           .A2(RdCol2Line[19]), .A3(RdCol3Line[19]),
           .SL1(ColSelectBuf1[1]), .SL0(ColSelectBuf0[1]), .Z(RdDat[19]) );
	MUX4D2 rd20mx(.A0(RdCol0Line[20]), .A1(RdCol1Line[20]),
           .A2(RdCol2Line[20]), .A3(RdCol3Line[20]),
           .SL1(ColSelectBuf1[0]), .SL0(ColSelectBuf0[0]), .Z(RdDat[20]) );
	MUX4D2 rd21mx(.A0(RdCol0Line[21]), .A1(RdCol1Line[21]),
           .A2(RdCol2Line[21]), .A3(RdCol3Line[21]),
           .SL1(ColSelectBuf1[1]), .SL0(ColSelectBuf0[1]), .Z(RdDat[21]) );
	MUX4D2 rd22mx(.A0(RdCol0Line[22]), .A1(RdCol1Line[22]),
           .A2(RdCol2Line[22]), .A3(RdCol3Line[22]),
           .SL1(ColSelectBuf1[0]), .SL0(ColSelectBuf0[0]), .Z(RdDat[22]) );
	MUX4D2 rd23mx(.A0(RdCol0Line[23]), .A1(RdCol1Line[23]),
           .A2(RdCol2Line[23]), .A3(RdCol3Line[23]),
           .SL1(ColSelectBuf1[1]), .SL0(ColSelectBuf0[1]), .Z(RdDat[23]) );
	MUX4D2 rd24mx(.A0(RdCol0Line[24]), .A1(RdCol1Line[24]),
           .A2(RdCol2Line[24]), .A3(RdCol3Line[24]),
           .SL1(ColSelectBuf1[0]), .SL0(ColSelectBuf0[0]), .Z(RdDat[24]) );
	MUX4D2 rd25mx(.A0(RdCol0Line[25]), .A1(RdCol1Line[25]),
           .A2(RdCol2Line[25]), .A3(RdCol3Line[25]),
           .SL1(ColSelectBuf1[1]), .SL0(ColSelectBuf0[1]), .Z(RdDat[25]) );
	MUX4D2 rd26mx(.A0(RdCol0Line[26]), .A1(RdCol1Line[26]),
           .A2(RdCol2Line[26]), .A3(RdCol3Line[26]),
           .SL1(ColSelectBuf1[0]), .SL0(ColSelectBuf0[0]), .Z(RdDat[26]) );
	MUX4D2 rd27mx(.A0(RdCol0Line[27]), .A1(RdCol1Line[27]),
           .A2(RdCol2Line[27]), .A3(RdCol3Line[27]),
           .SL1(ColSelectBuf1[1]), .SL0(ColSelectBuf0[1]), .Z(RdDat[27]) );
	MUX4D2 rd28mx(.A0(RdCol0Line[28]), .A1(RdCol1Line[28]),
           .A2(RdCol2Line[28]), .A3(RdCol3Line[28]),
           .SL1(ColSelectBuf1[0]), .SL0(ColSelectBuf0[0]), .Z(RdDat[28]) );
	MUX4D2 rd29mx(.A0(RdCol0Line[29]), .A1(RdCol1Line[29]),
           .A2(RdCol2Line[29]), .A3(RdCol3Line[29]),
           .SL1(ColSelectBuf1[1]), .SL0(ColSelectBuf0[1]), .Z(RdDat[29]) );
	MUX4D2 rd30mx(.A0(RdCol0Line[30]), .A1(RdCol1Line[30]),
           .A2(RdCol2Line[30]), .A3(RdCol3Line[30]),
           .SL1(ColSelectBuf1[0]), .SL0(ColSelectBuf0[0]), .Z(RdDat[30]) );
	MUX4D2 rd31mx(.A0(RdCol0Line[31]), .A1(RdCol1Line[31]),
           .A2(RdCol2Line[31]), .A3(RdCol3Line[31]),
           .SL1(ColSelectBuf1[1]), .SL0(ColSelectBuf0[1]), .Z(RdDat[31]) );

   endmodule

