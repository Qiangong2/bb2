// This file was automatically derived from "LArr4x12.vpp"
// using vpp on Thu May 18 23:06:52 2000.



// syn myclk = NullClk


module LArr4x12 (
	NullClk,
	WWL,
	WrDat,
	RWL,
	RdDat);

	input NullClk;
	input [3:0]WWL;
	input [11:0] WrDat;
	input [3:0]RWL;
	output [11:0] RdDat;


       wire [11:0] RdCol0Line;

  
            //
            // ReadLineBuffers
            //
	      
	    INVDA RWLR0Buf0 ( .A ( RWL[0] ), .Z ( RWLR0Wire0 ) );
	    INVDA RWLR1Buf0 ( .A ( RWL[1] ), .Z ( RWLR1Wire0 ) );
	    INVDA RWLR2Buf0 ( .A ( RWL[2] ), .Z ( RWLR2Wire0 ) );
	    INVDA RWLR3Buf0 ( .A ( RWL[3] ), .Z ( RWLR3Wire0 ) );
  
            //
            // WriteLineBuffers
            //
	      
	    INVDA WWLW0Buf0 ( .A ( WWL[0] ), .Z ( WWLW0Wire0 ) );
	    INVDA WWLW1Buf0 ( .A ( WWL[1] ), .Z ( WWLW1Wire0 ) );
	    INVDA WWLW2Buf0 ( .A ( WWL[2] ), .Z ( WWLW2Wire0 ) );
	    INVDA WWLW3Buf0 ( .A ( WWL[3] ), .Z ( WWLW3Wire0 ) );

//
//
// this is the array of latches itself.
     
            //
            // Row 0
	    //
	      
 	    LATNT1 LatR0C0B0( .G(WWLW0Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR0C0B1( .G(WWLW0Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR0C0B2( .G(WWLW0Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR0C0B3( .G(WWLW0Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR0C0B4( .G(WWLW0Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[4]) );
 	    LATNT1 LatR0C0B5( .G(WWLW0Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[5]) );
 	    LATNT1 LatR0C0B6( .G(WWLW0Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[6]) );
 	    LATNT1 LatR0C0B7( .G(WWLW0Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[7]) );
 	    LATNT1 LatR0C0B8( .G(WWLW0Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[8]) );
 	    LATNT1 LatR0C0B9( .G(WWLW0Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[9]) );
 	    LATNT1 LatR0C0B10( .G(WWLW0Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[10]) );
 	    LATNT1 LatR0C0B11( .G(WWLW0Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[11]) );
     
            //
            // Row 1
	    //
	      
 	    LATNT1 LatR1C0B0( .G(WWLW1Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR1C0B1( .G(WWLW1Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR1C0B2( .G(WWLW1Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR1C0B3( .G(WWLW1Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR1C0B4( .G(WWLW1Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[4]) );
 	    LATNT1 LatR1C0B5( .G(WWLW1Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[5]) );
 	    LATNT1 LatR1C0B6( .G(WWLW1Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[6]) );
 	    LATNT1 LatR1C0B7( .G(WWLW1Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[7]) );
 	    LATNT1 LatR1C0B8( .G(WWLW1Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[8]) );
 	    LATNT1 LatR1C0B9( .G(WWLW1Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[9]) );
 	    LATNT1 LatR1C0B10( .G(WWLW1Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[10]) );
 	    LATNT1 LatR1C0B11( .G(WWLW1Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[11]) );
     
            //
            // Row 2
	    //
	      
 	    LATNT1 LatR2C0B0( .G(WWLW2Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR2C0B1( .G(WWLW2Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR2C0B2( .G(WWLW2Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR2C0B3( .G(WWLW2Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR2C0B4( .G(WWLW2Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[4]) );
 	    LATNT1 LatR2C0B5( .G(WWLW2Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[5]) );
 	    LATNT1 LatR2C0B6( .G(WWLW2Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[6]) );
 	    LATNT1 LatR2C0B7( .G(WWLW2Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[7]) );
 	    LATNT1 LatR2C0B8( .G(WWLW2Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[8]) );
 	    LATNT1 LatR2C0B9( .G(WWLW2Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[9]) );
 	    LATNT1 LatR2C0B10( .G(WWLW2Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[10]) );
 	    LATNT1 LatR2C0B11( .G(WWLW2Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[11]) );
     
            //
            // Row 3
	    //
	      
 	    LATNT1 LatR3C0B0( .G(WWLW3Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR3C0B1( .G(WWLW3Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR3C0B2( .G(WWLW3Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR3C0B3( .G(WWLW3Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR3C0B4( .G(WWLW3Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[4]) );
 	    LATNT1 LatR3C0B5( .G(WWLW3Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[5]) );
 	    LATNT1 LatR3C0B6( .G(WWLW3Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[6]) );
 	    LATNT1 LatR3C0B7( .G(WWLW3Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[7]) );
 	    LATNT1 LatR3C0B8( .G(WWLW3Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[8]) );
 	    LATNT1 LatR3C0B9( .G(WWLW3Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[9]) );
 	    LATNT1 LatR3C0B10( .G(WWLW3Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[10]) );
 	    LATNT1 LatR3C0B11( .G(WWLW3Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[11]) );

//
//
// the psuedo sense amp (and column muxing if necessary)
         BUFD3 rd0buf(.A(RdCol0Line[0]), .Z(RdDat[0]) );
         BUFD3 rd1buf(.A(RdCol0Line[1]), .Z(RdDat[1]) );
         BUFD3 rd2buf(.A(RdCol0Line[2]), .Z(RdDat[2]) );
         BUFD3 rd3buf(.A(RdCol0Line[3]), .Z(RdDat[3]) );
         BUFD3 rd4buf(.A(RdCol0Line[4]), .Z(RdDat[4]) );
         BUFD3 rd5buf(.A(RdCol0Line[5]), .Z(RdDat[5]) );
         BUFD3 rd6buf(.A(RdCol0Line[6]), .Z(RdDat[6]) );
         BUFD3 rd7buf(.A(RdCol0Line[7]), .Z(RdDat[7]) );
         BUFD3 rd8buf(.A(RdCol0Line[8]), .Z(RdDat[8]) );
         BUFD3 rd9buf(.A(RdCol0Line[9]), .Z(RdDat[9]) );
         BUFD3 rd10buf(.A(RdCol0Line[10]), .Z(RdDat[10]) );
         BUFD3 rd11buf(.A(RdCol0Line[11]), .Z(RdDat[11]) );

   endmodule

