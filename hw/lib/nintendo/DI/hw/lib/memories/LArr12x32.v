// This file was automatically derived from "LArr12x32.vpp"
// using vpp on Thu May 18 23:07:00 2000.



// syn myclk = NullClk


module LArr12x32 (
	NullClk,
	WWL,
	WrDat,
	RWL,
	ColSelect,
	RdDat);

	input NullClk;
	input [11:0]WWL;
	input [31:0] WrDat;
	input [5:0]RWL;
	   input ColSelect;
	output [31:0] RdDat;


       wire [31:0] RdCol0Line;
       wire [31:0] RdCol1Line;

  
            //
            // ReadLineBuffers
            //
	      
	    INVDA RWLR0Buf0 ( .A ( RWL[0] ), .Z ( RWLR0Wire0 ) );
	    INVDA RWLR0Buf1 ( .A ( RWL[0] ), .Z ( RWLR0Wire1 ) );
	    INVDA RWLR0Buf2 ( .A ( RWL[0] ), .Z ( RWLR0Wire2 ) );
	    INVDA RWLR0Buf3 ( .A ( RWL[0] ), .Z ( RWLR0Wire3 ) );
	    INVDA RWLR1Buf0 ( .A ( RWL[1] ), .Z ( RWLR1Wire0 ) );
	    INVDA RWLR1Buf1 ( .A ( RWL[1] ), .Z ( RWLR1Wire1 ) );
	    INVDA RWLR1Buf2 ( .A ( RWL[1] ), .Z ( RWLR1Wire2 ) );
	    INVDA RWLR1Buf3 ( .A ( RWL[1] ), .Z ( RWLR1Wire3 ) );
	    INVDA RWLR2Buf0 ( .A ( RWL[2] ), .Z ( RWLR2Wire0 ) );
	    INVDA RWLR2Buf1 ( .A ( RWL[2] ), .Z ( RWLR2Wire1 ) );
	    INVDA RWLR2Buf2 ( .A ( RWL[2] ), .Z ( RWLR2Wire2 ) );
	    INVDA RWLR2Buf3 ( .A ( RWL[2] ), .Z ( RWLR2Wire3 ) );
	    INVDA RWLR3Buf0 ( .A ( RWL[3] ), .Z ( RWLR3Wire0 ) );
	    INVDA RWLR3Buf1 ( .A ( RWL[3] ), .Z ( RWLR3Wire1 ) );
	    INVDA RWLR3Buf2 ( .A ( RWL[3] ), .Z ( RWLR3Wire2 ) );
	    INVDA RWLR3Buf3 ( .A ( RWL[3] ), .Z ( RWLR3Wire3 ) );
	    INVDA RWLR4Buf0 ( .A ( RWL[4] ), .Z ( RWLR4Wire0 ) );
	    INVDA RWLR4Buf1 ( .A ( RWL[4] ), .Z ( RWLR4Wire1 ) );
	    INVDA RWLR4Buf2 ( .A ( RWL[4] ), .Z ( RWLR4Wire2 ) );
	    INVDA RWLR4Buf3 ( .A ( RWL[4] ), .Z ( RWLR4Wire3 ) );
	    INVDA RWLR5Buf0 ( .A ( RWL[5] ), .Z ( RWLR5Wire0 ) );
	    INVDA RWLR5Buf1 ( .A ( RWL[5] ), .Z ( RWLR5Wire1 ) );
	    INVDA RWLR5Buf2 ( .A ( RWL[5] ), .Z ( RWLR5Wire2 ) );
	    INVDA RWLR5Buf3 ( .A ( RWL[5] ), .Z ( RWLR5Wire3 ) );
  
            //
            // WriteLineBuffers
            //
	      
	    INVDA WWLW0Buf0 ( .A ( WWL[0] ), .Z ( WWLW0Wire0 ) );
	    INVDA WWLW0Buf1 ( .A ( WWL[0] ), .Z ( WWLW0Wire1 ) );
	    INVDA WWLW1Buf0 ( .A ( WWL[1] ), .Z ( WWLW1Wire0 ) );
	    INVDA WWLW1Buf1 ( .A ( WWL[1] ), .Z ( WWLW1Wire1 ) );
	    INVDA WWLW2Buf0 ( .A ( WWL[2] ), .Z ( WWLW2Wire0 ) );
	    INVDA WWLW2Buf1 ( .A ( WWL[2] ), .Z ( WWLW2Wire1 ) );
	    INVDA WWLW3Buf0 ( .A ( WWL[3] ), .Z ( WWLW3Wire0 ) );
	    INVDA WWLW3Buf1 ( .A ( WWL[3] ), .Z ( WWLW3Wire1 ) );
	    INVDA WWLW4Buf0 ( .A ( WWL[4] ), .Z ( WWLW4Wire0 ) );
	    INVDA WWLW4Buf1 ( .A ( WWL[4] ), .Z ( WWLW4Wire1 ) );
	    INVDA WWLW5Buf0 ( .A ( WWL[5] ), .Z ( WWLW5Wire0 ) );
	    INVDA WWLW5Buf1 ( .A ( WWL[5] ), .Z ( WWLW5Wire1 ) );
	    INVDA WWLW6Buf0 ( .A ( WWL[6] ), .Z ( WWLW6Wire0 ) );
	    INVDA WWLW6Buf1 ( .A ( WWL[6] ), .Z ( WWLW6Wire1 ) );
	    INVDA WWLW7Buf0 ( .A ( WWL[7] ), .Z ( WWLW7Wire0 ) );
	    INVDA WWLW7Buf1 ( .A ( WWL[7] ), .Z ( WWLW7Wire1 ) );
	    INVDA WWLW8Buf0 ( .A ( WWL[8] ), .Z ( WWLW8Wire0 ) );
	    INVDA WWLW8Buf1 ( .A ( WWL[8] ), .Z ( WWLW8Wire1 ) );
	    INVDA WWLW9Buf0 ( .A ( WWL[9] ), .Z ( WWLW9Wire0 ) );
	    INVDA WWLW9Buf1 ( .A ( WWL[9] ), .Z ( WWLW9Wire1 ) );
	    INVDA WWLW10Buf0 ( .A ( WWL[10] ), .Z ( WWLW10Wire0 ) );
	    INVDA WWLW10Buf1 ( .A ( WWL[10] ), .Z ( WWLW10Wire1 ) );
	    INVDA WWLW11Buf0 ( .A ( WWL[11] ), .Z ( WWLW11Wire0 ) );
	    INVDA WWLW11Buf1 ( .A ( WWL[11] ), .Z ( WWLW11Wire1 ) );

//
//
// this is the array of latches itself.
     
            //
            // Row 0
	    //
	      
 	    LATNT1 LatR0C0B0( .G(WWLW0Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR0C1B0( .G(WWLW1Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol1Line[0]) );
 	    LATNT1 LatR0C0B1( .G(WWLW0Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR0C1B1( .G(WWLW1Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol1Line[1]) );
 	    LATNT1 LatR0C0B2( .G(WWLW0Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR0C1B2( .G(WWLW1Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol1Line[2]) );
 	    LATNT1 LatR0C0B3( .G(WWLW0Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR0C1B3( .G(WWLW1Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol1Line[3]) );
 	    LATNT1 LatR0C0B4( .G(WWLW0Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[4]) );
 	    LATNT1 LatR0C1B4( .G(WWLW1Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol1Line[4]) );
 	    LATNT1 LatR0C0B5( .G(WWLW0Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[5]) );
 	    LATNT1 LatR0C1B5( .G(WWLW1Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol1Line[5]) );
 	    LATNT1 LatR0C0B6( .G(WWLW0Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[6]) );
 	    LATNT1 LatR0C1B6( .G(WWLW1Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol1Line[6]) );
 	    LATNT1 LatR0C0B7( .G(WWLW0Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[7]) );
 	    LATNT1 LatR0C1B7( .G(WWLW1Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol1Line[7]) );
 	    LATNT1 LatR0C0B8( .G(WWLW0Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[8]) );
 	    LATNT1 LatR0C1B8( .G(WWLW1Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol1Line[8]) );
 	    LATNT1 LatR0C0B9( .G(WWLW0Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[9]) );
 	    LATNT1 LatR0C1B9( .G(WWLW1Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol1Line[9]) );
 	    LATNT1 LatR0C0B10( .G(WWLW0Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[10]) );
 	    LATNT1 LatR0C1B10( .G(WWLW1Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol1Line[10]) );
 	    LATNT1 LatR0C0B11( .G(WWLW0Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[11]) );
 	    LATNT1 LatR0C1B11( .G(WWLW1Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol1Line[11]) );
 	    LATNT1 LatR0C0B12( .G(WWLW0Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[12]) );
 	    LATNT1 LatR0C1B12( .G(WWLW1Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol1Line[12]) );
 	    LATNT1 LatR0C0B13( .G(WWLW0Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[13]) );
 	    LATNT1 LatR0C1B13( .G(WWLW1Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol1Line[13]) );
 	    LATNT1 LatR0C0B14( .G(WWLW0Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[14]) );
 	    LATNT1 LatR0C1B14( .G(WWLW1Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol1Line[14]) );
 	    LATNT1 LatR0C0B15( .G(WWLW0Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR0Wire0), 
					  .Z(RdCol0Line[15]) );
 	    LATNT1 LatR0C1B15( .G(WWLW1Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR0Wire2), 
					  .Z(RdCol1Line[15]) );
 	    LATNT1 LatR0C0B16( .G(WWLW0Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[16]) );
 	    LATNT1 LatR0C1B16( .G(WWLW1Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol1Line[16]) );
 	    LATNT1 LatR0C0B17( .G(WWLW0Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[17]) );
 	    LATNT1 LatR0C1B17( .G(WWLW1Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol1Line[17]) );
 	    LATNT1 LatR0C0B18( .G(WWLW0Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[18]) );
 	    LATNT1 LatR0C1B18( .G(WWLW1Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol1Line[18]) );
 	    LATNT1 LatR0C0B19( .G(WWLW0Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[19]) );
 	    LATNT1 LatR0C1B19( .G(WWLW1Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol1Line[19]) );
 	    LATNT1 LatR0C0B20( .G(WWLW0Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[20]) );
 	    LATNT1 LatR0C1B20( .G(WWLW1Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol1Line[20]) );
 	    LATNT1 LatR0C0B21( .G(WWLW0Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[21]) );
 	    LATNT1 LatR0C1B21( .G(WWLW1Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol1Line[21]) );
 	    LATNT1 LatR0C0B22( .G(WWLW0Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[22]) );
 	    LATNT1 LatR0C1B22( .G(WWLW1Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol1Line[22]) );
 	    LATNT1 LatR0C0B23( .G(WWLW0Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[23]) );
 	    LATNT1 LatR0C1B23( .G(WWLW1Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol1Line[23]) );
 	    LATNT1 LatR0C0B24( .G(WWLW0Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[24]) );
 	    LATNT1 LatR0C1B24( .G(WWLW1Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol1Line[24]) );
 	    LATNT1 LatR0C0B25( .G(WWLW0Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[25]) );
 	    LATNT1 LatR0C1B25( .G(WWLW1Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol1Line[25]) );
 	    LATNT1 LatR0C0B26( .G(WWLW0Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[26]) );
 	    LATNT1 LatR0C1B26( .G(WWLW1Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol1Line[26]) );
 	    LATNT1 LatR0C0B27( .G(WWLW0Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[27]) );
 	    LATNT1 LatR0C1B27( .G(WWLW1Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol1Line[27]) );
 	    LATNT1 LatR0C0B28( .G(WWLW0Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[28]) );
 	    LATNT1 LatR0C1B28( .G(WWLW1Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol1Line[28]) );
 	    LATNT1 LatR0C0B29( .G(WWLW0Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[29]) );
 	    LATNT1 LatR0C1B29( .G(WWLW1Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol1Line[29]) );
 	    LATNT1 LatR0C0B30( .G(WWLW0Wire1), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[30]) );
 	    LATNT1 LatR0C1B30( .G(WWLW1Wire1), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol1Line[30]) );
 	    LATNT1 LatR0C0B31( .G(WWLW0Wire1), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR0Wire1), 
					  .Z(RdCol0Line[31]) );
 	    LATNT1 LatR0C1B31( .G(WWLW1Wire1), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR0Wire3), 
					  .Z(RdCol1Line[31]) );
     
            //
            // Row 1
	    //
	      
 	    LATNT1 LatR1C0B0( .G(WWLW2Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR1C1B0( .G(WWLW3Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol1Line[0]) );
 	    LATNT1 LatR1C0B1( .G(WWLW2Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR1C1B1( .G(WWLW3Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol1Line[1]) );
 	    LATNT1 LatR1C0B2( .G(WWLW2Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR1C1B2( .G(WWLW3Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol1Line[2]) );
 	    LATNT1 LatR1C0B3( .G(WWLW2Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR1C1B3( .G(WWLW3Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol1Line[3]) );
 	    LATNT1 LatR1C0B4( .G(WWLW2Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[4]) );
 	    LATNT1 LatR1C1B4( .G(WWLW3Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol1Line[4]) );
 	    LATNT1 LatR1C0B5( .G(WWLW2Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[5]) );
 	    LATNT1 LatR1C1B5( .G(WWLW3Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol1Line[5]) );
 	    LATNT1 LatR1C0B6( .G(WWLW2Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[6]) );
 	    LATNT1 LatR1C1B6( .G(WWLW3Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol1Line[6]) );
 	    LATNT1 LatR1C0B7( .G(WWLW2Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[7]) );
 	    LATNT1 LatR1C1B7( .G(WWLW3Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol1Line[7]) );
 	    LATNT1 LatR1C0B8( .G(WWLW2Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[8]) );
 	    LATNT1 LatR1C1B8( .G(WWLW3Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol1Line[8]) );
 	    LATNT1 LatR1C0B9( .G(WWLW2Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[9]) );
 	    LATNT1 LatR1C1B9( .G(WWLW3Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol1Line[9]) );
 	    LATNT1 LatR1C0B10( .G(WWLW2Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[10]) );
 	    LATNT1 LatR1C1B10( .G(WWLW3Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol1Line[10]) );
 	    LATNT1 LatR1C0B11( .G(WWLW2Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[11]) );
 	    LATNT1 LatR1C1B11( .G(WWLW3Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol1Line[11]) );
 	    LATNT1 LatR1C0B12( .G(WWLW2Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[12]) );
 	    LATNT1 LatR1C1B12( .G(WWLW3Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol1Line[12]) );
 	    LATNT1 LatR1C0B13( .G(WWLW2Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[13]) );
 	    LATNT1 LatR1C1B13( .G(WWLW3Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol1Line[13]) );
 	    LATNT1 LatR1C0B14( .G(WWLW2Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[14]) );
 	    LATNT1 LatR1C1B14( .G(WWLW3Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol1Line[14]) );
 	    LATNT1 LatR1C0B15( .G(WWLW2Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR1Wire0), 
					  .Z(RdCol0Line[15]) );
 	    LATNT1 LatR1C1B15( .G(WWLW3Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR1Wire2), 
					  .Z(RdCol1Line[15]) );
 	    LATNT1 LatR1C0B16( .G(WWLW2Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[16]) );
 	    LATNT1 LatR1C1B16( .G(WWLW3Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol1Line[16]) );
 	    LATNT1 LatR1C0B17( .G(WWLW2Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[17]) );
 	    LATNT1 LatR1C1B17( .G(WWLW3Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol1Line[17]) );
 	    LATNT1 LatR1C0B18( .G(WWLW2Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[18]) );
 	    LATNT1 LatR1C1B18( .G(WWLW3Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol1Line[18]) );
 	    LATNT1 LatR1C0B19( .G(WWLW2Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[19]) );
 	    LATNT1 LatR1C1B19( .G(WWLW3Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol1Line[19]) );
 	    LATNT1 LatR1C0B20( .G(WWLW2Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[20]) );
 	    LATNT1 LatR1C1B20( .G(WWLW3Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol1Line[20]) );
 	    LATNT1 LatR1C0B21( .G(WWLW2Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[21]) );
 	    LATNT1 LatR1C1B21( .G(WWLW3Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol1Line[21]) );
 	    LATNT1 LatR1C0B22( .G(WWLW2Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[22]) );
 	    LATNT1 LatR1C1B22( .G(WWLW3Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol1Line[22]) );
 	    LATNT1 LatR1C0B23( .G(WWLW2Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[23]) );
 	    LATNT1 LatR1C1B23( .G(WWLW3Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol1Line[23]) );
 	    LATNT1 LatR1C0B24( .G(WWLW2Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[24]) );
 	    LATNT1 LatR1C1B24( .G(WWLW3Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol1Line[24]) );
 	    LATNT1 LatR1C0B25( .G(WWLW2Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[25]) );
 	    LATNT1 LatR1C1B25( .G(WWLW3Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol1Line[25]) );
 	    LATNT1 LatR1C0B26( .G(WWLW2Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[26]) );
 	    LATNT1 LatR1C1B26( .G(WWLW3Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol1Line[26]) );
 	    LATNT1 LatR1C0B27( .G(WWLW2Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[27]) );
 	    LATNT1 LatR1C1B27( .G(WWLW3Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol1Line[27]) );
 	    LATNT1 LatR1C0B28( .G(WWLW2Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[28]) );
 	    LATNT1 LatR1C1B28( .G(WWLW3Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol1Line[28]) );
 	    LATNT1 LatR1C0B29( .G(WWLW2Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[29]) );
 	    LATNT1 LatR1C1B29( .G(WWLW3Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol1Line[29]) );
 	    LATNT1 LatR1C0B30( .G(WWLW2Wire1), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[30]) );
 	    LATNT1 LatR1C1B30( .G(WWLW3Wire1), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol1Line[30]) );
 	    LATNT1 LatR1C0B31( .G(WWLW2Wire1), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR1Wire1), 
					  .Z(RdCol0Line[31]) );
 	    LATNT1 LatR1C1B31( .G(WWLW3Wire1), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR1Wire3), 
					  .Z(RdCol1Line[31]) );
     
            //
            // Row 2
	    //
	      
 	    LATNT1 LatR2C0B0( .G(WWLW4Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR2C1B0( .G(WWLW5Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol1Line[0]) );
 	    LATNT1 LatR2C0B1( .G(WWLW4Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR2C1B1( .G(WWLW5Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol1Line[1]) );
 	    LATNT1 LatR2C0B2( .G(WWLW4Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR2C1B2( .G(WWLW5Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol1Line[2]) );
 	    LATNT1 LatR2C0B3( .G(WWLW4Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR2C1B3( .G(WWLW5Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol1Line[3]) );
 	    LATNT1 LatR2C0B4( .G(WWLW4Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[4]) );
 	    LATNT1 LatR2C1B4( .G(WWLW5Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol1Line[4]) );
 	    LATNT1 LatR2C0B5( .G(WWLW4Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[5]) );
 	    LATNT1 LatR2C1B5( .G(WWLW5Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol1Line[5]) );
 	    LATNT1 LatR2C0B6( .G(WWLW4Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[6]) );
 	    LATNT1 LatR2C1B6( .G(WWLW5Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol1Line[6]) );
 	    LATNT1 LatR2C0B7( .G(WWLW4Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[7]) );
 	    LATNT1 LatR2C1B7( .G(WWLW5Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol1Line[7]) );
 	    LATNT1 LatR2C0B8( .G(WWLW4Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[8]) );
 	    LATNT1 LatR2C1B8( .G(WWLW5Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol1Line[8]) );
 	    LATNT1 LatR2C0B9( .G(WWLW4Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[9]) );
 	    LATNT1 LatR2C1B9( .G(WWLW5Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol1Line[9]) );
 	    LATNT1 LatR2C0B10( .G(WWLW4Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[10]) );
 	    LATNT1 LatR2C1B10( .G(WWLW5Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol1Line[10]) );
 	    LATNT1 LatR2C0B11( .G(WWLW4Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[11]) );
 	    LATNT1 LatR2C1B11( .G(WWLW5Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol1Line[11]) );
 	    LATNT1 LatR2C0B12( .G(WWLW4Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[12]) );
 	    LATNT1 LatR2C1B12( .G(WWLW5Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol1Line[12]) );
 	    LATNT1 LatR2C0B13( .G(WWLW4Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[13]) );
 	    LATNT1 LatR2C1B13( .G(WWLW5Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol1Line[13]) );
 	    LATNT1 LatR2C0B14( .G(WWLW4Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[14]) );
 	    LATNT1 LatR2C1B14( .G(WWLW5Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol1Line[14]) );
 	    LATNT1 LatR2C0B15( .G(WWLW4Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR2Wire0), 
					  .Z(RdCol0Line[15]) );
 	    LATNT1 LatR2C1B15( .G(WWLW5Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR2Wire2), 
					  .Z(RdCol1Line[15]) );
 	    LATNT1 LatR2C0B16( .G(WWLW4Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[16]) );
 	    LATNT1 LatR2C1B16( .G(WWLW5Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol1Line[16]) );
 	    LATNT1 LatR2C0B17( .G(WWLW4Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[17]) );
 	    LATNT1 LatR2C1B17( .G(WWLW5Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol1Line[17]) );
 	    LATNT1 LatR2C0B18( .G(WWLW4Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[18]) );
 	    LATNT1 LatR2C1B18( .G(WWLW5Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol1Line[18]) );
 	    LATNT1 LatR2C0B19( .G(WWLW4Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[19]) );
 	    LATNT1 LatR2C1B19( .G(WWLW5Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol1Line[19]) );
 	    LATNT1 LatR2C0B20( .G(WWLW4Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[20]) );
 	    LATNT1 LatR2C1B20( .G(WWLW5Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol1Line[20]) );
 	    LATNT1 LatR2C0B21( .G(WWLW4Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[21]) );
 	    LATNT1 LatR2C1B21( .G(WWLW5Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol1Line[21]) );
 	    LATNT1 LatR2C0B22( .G(WWLW4Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[22]) );
 	    LATNT1 LatR2C1B22( .G(WWLW5Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol1Line[22]) );
 	    LATNT1 LatR2C0B23( .G(WWLW4Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[23]) );
 	    LATNT1 LatR2C1B23( .G(WWLW5Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol1Line[23]) );
 	    LATNT1 LatR2C0B24( .G(WWLW4Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[24]) );
 	    LATNT1 LatR2C1B24( .G(WWLW5Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol1Line[24]) );
 	    LATNT1 LatR2C0B25( .G(WWLW4Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[25]) );
 	    LATNT1 LatR2C1B25( .G(WWLW5Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol1Line[25]) );
 	    LATNT1 LatR2C0B26( .G(WWLW4Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[26]) );
 	    LATNT1 LatR2C1B26( .G(WWLW5Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol1Line[26]) );
 	    LATNT1 LatR2C0B27( .G(WWLW4Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[27]) );
 	    LATNT1 LatR2C1B27( .G(WWLW5Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol1Line[27]) );
 	    LATNT1 LatR2C0B28( .G(WWLW4Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[28]) );
 	    LATNT1 LatR2C1B28( .G(WWLW5Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol1Line[28]) );
 	    LATNT1 LatR2C0B29( .G(WWLW4Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[29]) );
 	    LATNT1 LatR2C1B29( .G(WWLW5Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol1Line[29]) );
 	    LATNT1 LatR2C0B30( .G(WWLW4Wire1), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[30]) );
 	    LATNT1 LatR2C1B30( .G(WWLW5Wire1), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol1Line[30]) );
 	    LATNT1 LatR2C0B31( .G(WWLW4Wire1), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR2Wire1), 
					  .Z(RdCol0Line[31]) );
 	    LATNT1 LatR2C1B31( .G(WWLW5Wire1), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR2Wire3), 
					  .Z(RdCol1Line[31]) );
     
            //
            // Row 3
	    //
	      
 	    LATNT1 LatR3C0B0( .G(WWLW6Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR3C1B0( .G(WWLW7Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol1Line[0]) );
 	    LATNT1 LatR3C0B1( .G(WWLW6Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR3C1B1( .G(WWLW7Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol1Line[1]) );
 	    LATNT1 LatR3C0B2( .G(WWLW6Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR3C1B2( .G(WWLW7Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol1Line[2]) );
 	    LATNT1 LatR3C0B3( .G(WWLW6Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR3C1B3( .G(WWLW7Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol1Line[3]) );
 	    LATNT1 LatR3C0B4( .G(WWLW6Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[4]) );
 	    LATNT1 LatR3C1B4( .G(WWLW7Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol1Line[4]) );
 	    LATNT1 LatR3C0B5( .G(WWLW6Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[5]) );
 	    LATNT1 LatR3C1B5( .G(WWLW7Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol1Line[5]) );
 	    LATNT1 LatR3C0B6( .G(WWLW6Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[6]) );
 	    LATNT1 LatR3C1B6( .G(WWLW7Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol1Line[6]) );
 	    LATNT1 LatR3C0B7( .G(WWLW6Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[7]) );
 	    LATNT1 LatR3C1B7( .G(WWLW7Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol1Line[7]) );
 	    LATNT1 LatR3C0B8( .G(WWLW6Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[8]) );
 	    LATNT1 LatR3C1B8( .G(WWLW7Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol1Line[8]) );
 	    LATNT1 LatR3C0B9( .G(WWLW6Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[9]) );
 	    LATNT1 LatR3C1B9( .G(WWLW7Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol1Line[9]) );
 	    LATNT1 LatR3C0B10( .G(WWLW6Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[10]) );
 	    LATNT1 LatR3C1B10( .G(WWLW7Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol1Line[10]) );
 	    LATNT1 LatR3C0B11( .G(WWLW6Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[11]) );
 	    LATNT1 LatR3C1B11( .G(WWLW7Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol1Line[11]) );
 	    LATNT1 LatR3C0B12( .G(WWLW6Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[12]) );
 	    LATNT1 LatR3C1B12( .G(WWLW7Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol1Line[12]) );
 	    LATNT1 LatR3C0B13( .G(WWLW6Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[13]) );
 	    LATNT1 LatR3C1B13( .G(WWLW7Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol1Line[13]) );
 	    LATNT1 LatR3C0B14( .G(WWLW6Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[14]) );
 	    LATNT1 LatR3C1B14( .G(WWLW7Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol1Line[14]) );
 	    LATNT1 LatR3C0B15( .G(WWLW6Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR3Wire0), 
					  .Z(RdCol0Line[15]) );
 	    LATNT1 LatR3C1B15( .G(WWLW7Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR3Wire2), 
					  .Z(RdCol1Line[15]) );
 	    LATNT1 LatR3C0B16( .G(WWLW6Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[16]) );
 	    LATNT1 LatR3C1B16( .G(WWLW7Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol1Line[16]) );
 	    LATNT1 LatR3C0B17( .G(WWLW6Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[17]) );
 	    LATNT1 LatR3C1B17( .G(WWLW7Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol1Line[17]) );
 	    LATNT1 LatR3C0B18( .G(WWLW6Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[18]) );
 	    LATNT1 LatR3C1B18( .G(WWLW7Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol1Line[18]) );
 	    LATNT1 LatR3C0B19( .G(WWLW6Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[19]) );
 	    LATNT1 LatR3C1B19( .G(WWLW7Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol1Line[19]) );
 	    LATNT1 LatR3C0B20( .G(WWLW6Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[20]) );
 	    LATNT1 LatR3C1B20( .G(WWLW7Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol1Line[20]) );
 	    LATNT1 LatR3C0B21( .G(WWLW6Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[21]) );
 	    LATNT1 LatR3C1B21( .G(WWLW7Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol1Line[21]) );
 	    LATNT1 LatR3C0B22( .G(WWLW6Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[22]) );
 	    LATNT1 LatR3C1B22( .G(WWLW7Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol1Line[22]) );
 	    LATNT1 LatR3C0B23( .G(WWLW6Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[23]) );
 	    LATNT1 LatR3C1B23( .G(WWLW7Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol1Line[23]) );
 	    LATNT1 LatR3C0B24( .G(WWLW6Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[24]) );
 	    LATNT1 LatR3C1B24( .G(WWLW7Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol1Line[24]) );
 	    LATNT1 LatR3C0B25( .G(WWLW6Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[25]) );
 	    LATNT1 LatR3C1B25( .G(WWLW7Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol1Line[25]) );
 	    LATNT1 LatR3C0B26( .G(WWLW6Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[26]) );
 	    LATNT1 LatR3C1B26( .G(WWLW7Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol1Line[26]) );
 	    LATNT1 LatR3C0B27( .G(WWLW6Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[27]) );
 	    LATNT1 LatR3C1B27( .G(WWLW7Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol1Line[27]) );
 	    LATNT1 LatR3C0B28( .G(WWLW6Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[28]) );
 	    LATNT1 LatR3C1B28( .G(WWLW7Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol1Line[28]) );
 	    LATNT1 LatR3C0B29( .G(WWLW6Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[29]) );
 	    LATNT1 LatR3C1B29( .G(WWLW7Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol1Line[29]) );
 	    LATNT1 LatR3C0B30( .G(WWLW6Wire1), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[30]) );
 	    LATNT1 LatR3C1B30( .G(WWLW7Wire1), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol1Line[30]) );
 	    LATNT1 LatR3C0B31( .G(WWLW6Wire1), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR3Wire1), 
					  .Z(RdCol0Line[31]) );
 	    LATNT1 LatR3C1B31( .G(WWLW7Wire1), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR3Wire3), 
					  .Z(RdCol1Line[31]) );
     
            //
            // Row 4
	    //
	      
 	    LATNT1 LatR4C0B0( .G(WWLW8Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR4C1B0( .G(WWLW9Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol1Line[0]) );
 	    LATNT1 LatR4C0B1( .G(WWLW8Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR4C1B1( .G(WWLW9Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol1Line[1]) );
 	    LATNT1 LatR4C0B2( .G(WWLW8Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR4C1B2( .G(WWLW9Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol1Line[2]) );
 	    LATNT1 LatR4C0B3( .G(WWLW8Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR4C1B3( .G(WWLW9Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol1Line[3]) );
 	    LATNT1 LatR4C0B4( .G(WWLW8Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[4]) );
 	    LATNT1 LatR4C1B4( .G(WWLW9Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol1Line[4]) );
 	    LATNT1 LatR4C0B5( .G(WWLW8Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[5]) );
 	    LATNT1 LatR4C1B5( .G(WWLW9Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol1Line[5]) );
 	    LATNT1 LatR4C0B6( .G(WWLW8Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[6]) );
 	    LATNT1 LatR4C1B6( .G(WWLW9Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol1Line[6]) );
 	    LATNT1 LatR4C0B7( .G(WWLW8Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[7]) );
 	    LATNT1 LatR4C1B7( .G(WWLW9Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol1Line[7]) );
 	    LATNT1 LatR4C0B8( .G(WWLW8Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[8]) );
 	    LATNT1 LatR4C1B8( .G(WWLW9Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol1Line[8]) );
 	    LATNT1 LatR4C0B9( .G(WWLW8Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[9]) );
 	    LATNT1 LatR4C1B9( .G(WWLW9Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol1Line[9]) );
 	    LATNT1 LatR4C0B10( .G(WWLW8Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[10]) );
 	    LATNT1 LatR4C1B10( .G(WWLW9Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol1Line[10]) );
 	    LATNT1 LatR4C0B11( .G(WWLW8Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[11]) );
 	    LATNT1 LatR4C1B11( .G(WWLW9Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol1Line[11]) );
 	    LATNT1 LatR4C0B12( .G(WWLW8Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[12]) );
 	    LATNT1 LatR4C1B12( .G(WWLW9Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol1Line[12]) );
 	    LATNT1 LatR4C0B13( .G(WWLW8Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[13]) );
 	    LATNT1 LatR4C1B13( .G(WWLW9Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol1Line[13]) );
 	    LATNT1 LatR4C0B14( .G(WWLW8Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[14]) );
 	    LATNT1 LatR4C1B14( .G(WWLW9Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol1Line[14]) );
 	    LATNT1 LatR4C0B15( .G(WWLW8Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR4Wire0), 
					  .Z(RdCol0Line[15]) );
 	    LATNT1 LatR4C1B15( .G(WWLW9Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR4Wire2), 
					  .Z(RdCol1Line[15]) );
 	    LATNT1 LatR4C0B16( .G(WWLW8Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[16]) );
 	    LATNT1 LatR4C1B16( .G(WWLW9Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol1Line[16]) );
 	    LATNT1 LatR4C0B17( .G(WWLW8Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[17]) );
 	    LATNT1 LatR4C1B17( .G(WWLW9Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol1Line[17]) );
 	    LATNT1 LatR4C0B18( .G(WWLW8Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[18]) );
 	    LATNT1 LatR4C1B18( .G(WWLW9Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol1Line[18]) );
 	    LATNT1 LatR4C0B19( .G(WWLW8Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[19]) );
 	    LATNT1 LatR4C1B19( .G(WWLW9Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol1Line[19]) );
 	    LATNT1 LatR4C0B20( .G(WWLW8Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[20]) );
 	    LATNT1 LatR4C1B20( .G(WWLW9Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol1Line[20]) );
 	    LATNT1 LatR4C0B21( .G(WWLW8Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[21]) );
 	    LATNT1 LatR4C1B21( .G(WWLW9Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol1Line[21]) );
 	    LATNT1 LatR4C0B22( .G(WWLW8Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[22]) );
 	    LATNT1 LatR4C1B22( .G(WWLW9Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol1Line[22]) );
 	    LATNT1 LatR4C0B23( .G(WWLW8Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[23]) );
 	    LATNT1 LatR4C1B23( .G(WWLW9Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol1Line[23]) );
 	    LATNT1 LatR4C0B24( .G(WWLW8Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[24]) );
 	    LATNT1 LatR4C1B24( .G(WWLW9Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol1Line[24]) );
 	    LATNT1 LatR4C0B25( .G(WWLW8Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[25]) );
 	    LATNT1 LatR4C1B25( .G(WWLW9Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol1Line[25]) );
 	    LATNT1 LatR4C0B26( .G(WWLW8Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[26]) );
 	    LATNT1 LatR4C1B26( .G(WWLW9Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol1Line[26]) );
 	    LATNT1 LatR4C0B27( .G(WWLW8Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[27]) );
 	    LATNT1 LatR4C1B27( .G(WWLW9Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol1Line[27]) );
 	    LATNT1 LatR4C0B28( .G(WWLW8Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[28]) );
 	    LATNT1 LatR4C1B28( .G(WWLW9Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol1Line[28]) );
 	    LATNT1 LatR4C0B29( .G(WWLW8Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[29]) );
 	    LATNT1 LatR4C1B29( .G(WWLW9Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol1Line[29]) );
 	    LATNT1 LatR4C0B30( .G(WWLW8Wire1), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[30]) );
 	    LATNT1 LatR4C1B30( .G(WWLW9Wire1), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol1Line[30]) );
 	    LATNT1 LatR4C0B31( .G(WWLW8Wire1), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR4Wire1), 
					  .Z(RdCol0Line[31]) );
 	    LATNT1 LatR4C1B31( .G(WWLW9Wire1), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR4Wire3), 
					  .Z(RdCol1Line[31]) );
     
            //
            // Row 5
	    //
	      
 	    LATNT1 LatR5C0B0( .G(WWLW10Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[0]) );
 	    LATNT1 LatR5C1B0( .G(WWLW11Wire0), 
					  .D(WrDat[0]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol1Line[0]) );
 	    LATNT1 LatR5C0B1( .G(WWLW10Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[1]) );
 	    LATNT1 LatR5C1B1( .G(WWLW11Wire0), 
					  .D(WrDat[1]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol1Line[1]) );
 	    LATNT1 LatR5C0B2( .G(WWLW10Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[2]) );
 	    LATNT1 LatR5C1B2( .G(WWLW11Wire0), 
					  .D(WrDat[2]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol1Line[2]) );
 	    LATNT1 LatR5C0B3( .G(WWLW10Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[3]) );
 	    LATNT1 LatR5C1B3( .G(WWLW11Wire0), 
					  .D(WrDat[3]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol1Line[3]) );
 	    LATNT1 LatR5C0B4( .G(WWLW10Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[4]) );
 	    LATNT1 LatR5C1B4( .G(WWLW11Wire0), 
					  .D(WrDat[4]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol1Line[4]) );
 	    LATNT1 LatR5C0B5( .G(WWLW10Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[5]) );
 	    LATNT1 LatR5C1B5( .G(WWLW11Wire0), 
					  .D(WrDat[5]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol1Line[5]) );
 	    LATNT1 LatR5C0B6( .G(WWLW10Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[6]) );
 	    LATNT1 LatR5C1B6( .G(WWLW11Wire0), 
					  .D(WrDat[6]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol1Line[6]) );
 	    LATNT1 LatR5C0B7( .G(WWLW10Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[7]) );
 	    LATNT1 LatR5C1B7( .G(WWLW11Wire0), 
					  .D(WrDat[7]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol1Line[7]) );
 	    LATNT1 LatR5C0B8( .G(WWLW10Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[8]) );
 	    LATNT1 LatR5C1B8( .G(WWLW11Wire0), 
					  .D(WrDat[8]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol1Line[8]) );
 	    LATNT1 LatR5C0B9( .G(WWLW10Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[9]) );
 	    LATNT1 LatR5C1B9( .G(WWLW11Wire0), 
					  .D(WrDat[9]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol1Line[9]) );
 	    LATNT1 LatR5C0B10( .G(WWLW10Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[10]) );
 	    LATNT1 LatR5C1B10( .G(WWLW11Wire0), 
					  .D(WrDat[10]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol1Line[10]) );
 	    LATNT1 LatR5C0B11( .G(WWLW10Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[11]) );
 	    LATNT1 LatR5C1B11( .G(WWLW11Wire0), 
					  .D(WrDat[11]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol1Line[11]) );
 	    LATNT1 LatR5C0B12( .G(WWLW10Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[12]) );
 	    LATNT1 LatR5C1B12( .G(WWLW11Wire0), 
					  .D(WrDat[12]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol1Line[12]) );
 	    LATNT1 LatR5C0B13( .G(WWLW10Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[13]) );
 	    LATNT1 LatR5C1B13( .G(WWLW11Wire0), 
					  .D(WrDat[13]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol1Line[13]) );
 	    LATNT1 LatR5C0B14( .G(WWLW10Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[14]) );
 	    LATNT1 LatR5C1B14( .G(WWLW11Wire0), 
					  .D(WrDat[14]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol1Line[14]) );
 	    LATNT1 LatR5C0B15( .G(WWLW10Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR5Wire0), 
					  .Z(RdCol0Line[15]) );
 	    LATNT1 LatR5C1B15( .G(WWLW11Wire0), 
					  .D(WrDat[15]), 
                           		  .EN(RWLR5Wire2), 
					  .Z(RdCol1Line[15]) );
 	    LATNT1 LatR5C0B16( .G(WWLW10Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol0Line[16]) );
 	    LATNT1 LatR5C1B16( .G(WWLW11Wire1), 
					  .D(WrDat[16]), 
                           		  .EN(RWLR5Wire3), 
					  .Z(RdCol1Line[16]) );
 	    LATNT1 LatR5C0B17( .G(WWLW10Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol0Line[17]) );
 	    LATNT1 LatR5C1B17( .G(WWLW11Wire1), 
					  .D(WrDat[17]), 
                           		  .EN(RWLR5Wire3), 
					  .Z(RdCol1Line[17]) );
 	    LATNT1 LatR5C0B18( .G(WWLW10Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol0Line[18]) );
 	    LATNT1 LatR5C1B18( .G(WWLW11Wire1), 
					  .D(WrDat[18]), 
                           		  .EN(RWLR5Wire3), 
					  .Z(RdCol1Line[18]) );
 	    LATNT1 LatR5C0B19( .G(WWLW10Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol0Line[19]) );
 	    LATNT1 LatR5C1B19( .G(WWLW11Wire1), 
					  .D(WrDat[19]), 
                           		  .EN(RWLR5Wire3), 
					  .Z(RdCol1Line[19]) );
 	    LATNT1 LatR5C0B20( .G(WWLW10Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol0Line[20]) );
 	    LATNT1 LatR5C1B20( .G(WWLW11Wire1), 
					  .D(WrDat[20]), 
                           		  .EN(RWLR5Wire3), 
					  .Z(RdCol1Line[20]) );
 	    LATNT1 LatR5C0B21( .G(WWLW10Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol0Line[21]) );
 	    LATNT1 LatR5C1B21( .G(WWLW11Wire1), 
					  .D(WrDat[21]), 
                           		  .EN(RWLR5Wire3), 
					  .Z(RdCol1Line[21]) );
 	    LATNT1 LatR5C0B22( .G(WWLW10Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol0Line[22]) );
 	    LATNT1 LatR5C1B22( .G(WWLW11Wire1), 
					  .D(WrDat[22]), 
                           		  .EN(RWLR5Wire3), 
					  .Z(RdCol1Line[22]) );
 	    LATNT1 LatR5C0B23( .G(WWLW10Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol0Line[23]) );
 	    LATNT1 LatR5C1B23( .G(WWLW11Wire1), 
					  .D(WrDat[23]), 
                           		  .EN(RWLR5Wire3), 
					  .Z(RdCol1Line[23]) );
 	    LATNT1 LatR5C0B24( .G(WWLW10Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol0Line[24]) );
 	    LATNT1 LatR5C1B24( .G(WWLW11Wire1), 
					  .D(WrDat[24]), 
                           		  .EN(RWLR5Wire3), 
					  .Z(RdCol1Line[24]) );
 	    LATNT1 LatR5C0B25( .G(WWLW10Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol0Line[25]) );
 	    LATNT1 LatR5C1B25( .G(WWLW11Wire1), 
					  .D(WrDat[25]), 
                           		  .EN(RWLR5Wire3), 
					  .Z(RdCol1Line[25]) );
 	    LATNT1 LatR5C0B26( .G(WWLW10Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol0Line[26]) );
 	    LATNT1 LatR5C1B26( .G(WWLW11Wire1), 
					  .D(WrDat[26]), 
                           		  .EN(RWLR5Wire3), 
					  .Z(RdCol1Line[26]) );
 	    LATNT1 LatR5C0B27( .G(WWLW10Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol0Line[27]) );
 	    LATNT1 LatR5C1B27( .G(WWLW11Wire1), 
					  .D(WrDat[27]), 
                           		  .EN(RWLR5Wire3), 
					  .Z(RdCol1Line[27]) );
 	    LATNT1 LatR5C0B28( .G(WWLW10Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol0Line[28]) );
 	    LATNT1 LatR5C1B28( .G(WWLW11Wire1), 
					  .D(WrDat[28]), 
                           		  .EN(RWLR5Wire3), 
					  .Z(RdCol1Line[28]) );
 	    LATNT1 LatR5C0B29( .G(WWLW10Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol0Line[29]) );
 	    LATNT1 LatR5C1B29( .G(WWLW11Wire1), 
					  .D(WrDat[29]), 
                           		  .EN(RWLR5Wire3), 
					  .Z(RdCol1Line[29]) );
 	    LATNT1 LatR5C0B30( .G(WWLW10Wire1), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol0Line[30]) );
 	    LATNT1 LatR5C1B30( .G(WWLW11Wire1), 
					  .D(WrDat[30]), 
                           		  .EN(RWLR5Wire3), 
					  .Z(RdCol1Line[30]) );
 	    LATNT1 LatR5C0B31( .G(WWLW10Wire1), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR5Wire1), 
					  .Z(RdCol0Line[31]) );
 	    LATNT1 LatR5C1B31( .G(WWLW11Wire1), 
					  .D(WrDat[31]), 
                           		  .EN(RWLR5Wire3), 
					  .Z(RdCol1Line[31]) );

//
//
// the psuedo sense amp (and column muxing if necessary)
         wire [1:0] ColSelectBuf;
         BUFD7 mxbuf0(.A(ColSelect), .Z(ColSelectBuf[0]) );
         BUFD7 mxbuf1(.A(ColSelect), .Z(ColSelectBuf[1]) );
	 MUX2D2 rd0mx(.A0(RdCol0Line[0]), .A1(RdCol1Line[0]),
           .SL(ColSelectBuf[0]), .Z(RdDat[0]) );
	 MUX2D2 rd1mx(.A0(RdCol0Line[1]), .A1(RdCol1Line[1]),
           .SL(ColSelectBuf[1]), .Z(RdDat[1]) );
	 MUX2D2 rd2mx(.A0(RdCol0Line[2]), .A1(RdCol1Line[2]),
           .SL(ColSelectBuf[0]), .Z(RdDat[2]) );
	 MUX2D2 rd3mx(.A0(RdCol0Line[3]), .A1(RdCol1Line[3]),
           .SL(ColSelectBuf[1]), .Z(RdDat[3]) );
	 MUX2D2 rd4mx(.A0(RdCol0Line[4]), .A1(RdCol1Line[4]),
           .SL(ColSelectBuf[0]), .Z(RdDat[4]) );
	 MUX2D2 rd5mx(.A0(RdCol0Line[5]), .A1(RdCol1Line[5]),
           .SL(ColSelectBuf[1]), .Z(RdDat[5]) );
	 MUX2D2 rd6mx(.A0(RdCol0Line[6]), .A1(RdCol1Line[6]),
           .SL(ColSelectBuf[0]), .Z(RdDat[6]) );
	 MUX2D2 rd7mx(.A0(RdCol0Line[7]), .A1(RdCol1Line[7]),
           .SL(ColSelectBuf[1]), .Z(RdDat[7]) );
	 MUX2D2 rd8mx(.A0(RdCol0Line[8]), .A1(RdCol1Line[8]),
           .SL(ColSelectBuf[0]), .Z(RdDat[8]) );
	 MUX2D2 rd9mx(.A0(RdCol0Line[9]), .A1(RdCol1Line[9]),
           .SL(ColSelectBuf[1]), .Z(RdDat[9]) );
	 MUX2D2 rd10mx(.A0(RdCol0Line[10]), .A1(RdCol1Line[10]),
           .SL(ColSelectBuf[0]), .Z(RdDat[10]) );
	 MUX2D2 rd11mx(.A0(RdCol0Line[11]), .A1(RdCol1Line[11]),
           .SL(ColSelectBuf[1]), .Z(RdDat[11]) );
	 MUX2D2 rd12mx(.A0(RdCol0Line[12]), .A1(RdCol1Line[12]),
           .SL(ColSelectBuf[0]), .Z(RdDat[12]) );
	 MUX2D2 rd13mx(.A0(RdCol0Line[13]), .A1(RdCol1Line[13]),
           .SL(ColSelectBuf[1]), .Z(RdDat[13]) );
	 MUX2D2 rd14mx(.A0(RdCol0Line[14]), .A1(RdCol1Line[14]),
           .SL(ColSelectBuf[0]), .Z(RdDat[14]) );
	 MUX2D2 rd15mx(.A0(RdCol0Line[15]), .A1(RdCol1Line[15]),
           .SL(ColSelectBuf[1]), .Z(RdDat[15]) );
	 MUX2D2 rd16mx(.A0(RdCol0Line[16]), .A1(RdCol1Line[16]),
           .SL(ColSelectBuf[0]), .Z(RdDat[16]) );
	 MUX2D2 rd17mx(.A0(RdCol0Line[17]), .A1(RdCol1Line[17]),
           .SL(ColSelectBuf[1]), .Z(RdDat[17]) );
	 MUX2D2 rd18mx(.A0(RdCol0Line[18]), .A1(RdCol1Line[18]),
           .SL(ColSelectBuf[0]), .Z(RdDat[18]) );
	 MUX2D2 rd19mx(.A0(RdCol0Line[19]), .A1(RdCol1Line[19]),
           .SL(ColSelectBuf[1]), .Z(RdDat[19]) );
	 MUX2D2 rd20mx(.A0(RdCol0Line[20]), .A1(RdCol1Line[20]),
           .SL(ColSelectBuf[0]), .Z(RdDat[20]) );
	 MUX2D2 rd21mx(.A0(RdCol0Line[21]), .A1(RdCol1Line[21]),
           .SL(ColSelectBuf[1]), .Z(RdDat[21]) );
	 MUX2D2 rd22mx(.A0(RdCol0Line[22]), .A1(RdCol1Line[22]),
           .SL(ColSelectBuf[0]), .Z(RdDat[22]) );
	 MUX2D2 rd23mx(.A0(RdCol0Line[23]), .A1(RdCol1Line[23]),
           .SL(ColSelectBuf[1]), .Z(RdDat[23]) );
	 MUX2D2 rd24mx(.A0(RdCol0Line[24]), .A1(RdCol1Line[24]),
           .SL(ColSelectBuf[0]), .Z(RdDat[24]) );
	 MUX2D2 rd25mx(.A0(RdCol0Line[25]), .A1(RdCol1Line[25]),
           .SL(ColSelectBuf[1]), .Z(RdDat[25]) );
	 MUX2D2 rd26mx(.A0(RdCol0Line[26]), .A1(RdCol1Line[26]),
           .SL(ColSelectBuf[0]), .Z(RdDat[26]) );
	 MUX2D2 rd27mx(.A0(RdCol0Line[27]), .A1(RdCol1Line[27]),
           .SL(ColSelectBuf[1]), .Z(RdDat[27]) );
	 MUX2D2 rd28mx(.A0(RdCol0Line[28]), .A1(RdCol1Line[28]),
           .SL(ColSelectBuf[0]), .Z(RdDat[28]) );
	 MUX2D2 rd29mx(.A0(RdCol0Line[29]), .A1(RdCol1Line[29]),
           .SL(ColSelectBuf[1]), .Z(RdDat[29]) );
	 MUX2D2 rd30mx(.A0(RdCol0Line[30]), .A1(RdCol1Line[30]),
           .SL(ColSelectBuf[0]), .Z(RdDat[30]) );
	 MUX2D2 rd31mx(.A0(RdCol0Line[31]), .A1(RdCol1Line[31]),
           .SL(ColSelectBuf[1]), .Z(RdDat[31]) );

   endmodule

