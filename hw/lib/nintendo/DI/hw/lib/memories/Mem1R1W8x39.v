// This file was automatically derived from "Mem1R1W8x39.vpp"
// using vpp on Mon May 29 03:08:21 2000.


// syn myclk = RdClk


module Mem1R1W8x39 (
	RdClk,
	WrClk,
	ScanMode,
	e1WrEn,
	e1RdAdr,
	e1WrAdr,
	e1WrDat,
	RdDat);

	input RdClk;
	input WrClk;
	input ScanMode;
	input e1WrEn;
	input [2:0] e1RdAdr, e1WrAdr;
	input [38:0] e1WrDat;
	output [38:0] RdDat;

// Begin vpp generated register declarations...
	reg [38:0] WrDat;
	reg [2:0] RdAdr;
// ...end vpp generated register declarations.


//
// Read Functionality...
//
// input address registers
always @(posedge RdClk) RdAdr[2:0] <= e1RdAdr;

// 
   wire [2:0] RowAdr;
   assign RowAdr[2:0] = RdAdr[2:0];

   wire [7:0] RWL, RWLB;
   assign RWL[7:0] = 8'h1 << RowAdr;

  macro_invda RWLB_0 (.Y(RWLB[0]), .A(RWL[0]));
  macro_invda RWLB_1 (.Y(RWLB[1]), .A(RWL[1]));
  macro_invda RWLB_2 (.Y(RWLB[2]), .A(RWL[2]));
  macro_invda RWLB_3 (.Y(RWLB[3]), .A(RWL[3]));
  macro_invda RWLB_4 (.Y(RWLB[4]), .A(RWL[4]));
  macro_invda RWLB_5 (.Y(RWLB[5]), .A(RWL[5]));
  macro_invda RWLB_6 (.Y(RWLB[6]), .A(RWL[6]));
  macro_invda RWLB_7 (.Y(RWLB[7]), .A(RWL[7]));

   wire ScanMode_buf;

   artx_buffer1 ScanModeBuf ( .A(ScanMode), .Y(ScanMode_buf) );

//
// Write Functionality...
//
// write address decoder happens in previous cycle
//
   wire [7:0] e1DecWrAdr, DecWrAdr;
   assign e1DecWrAdr[7:0] = (8'h1 << e1WrAdr);

   wire [7:0]WWL;

   macro_latwen latwen_0 (.wen(WE_0), .WrEn(WrEn_0), .e1WrEn(e1WrEn), .clk(WrClk));
   macro_latwl latwl_0 (.wl(WWL[0]), .DecWrAdr(DecWrAdr[0]), .e1DecWrAdr(e1DecWrAdr[0]), .wen(WE_0), .clk(WrClk), .ScanMode(ScanMode_buf));
   macro_latwl latwl_1 (.wl(WWL[1]), .DecWrAdr(DecWrAdr[1]), .e1DecWrAdr(e1DecWrAdr[1]), .wen(WE_0), .clk(WrClk), .ScanMode(ScanMode_buf));
   macro_latwl latwl_2 (.wl(WWL[2]), .DecWrAdr(DecWrAdr[2]), .e1DecWrAdr(e1DecWrAdr[2]), .wen(WE_0), .clk(WrClk), .ScanMode(ScanMode_buf));
   macro_latwl latwl_3 (.wl(WWL[3]), .DecWrAdr(DecWrAdr[3]), .e1DecWrAdr(e1DecWrAdr[3]), .wen(WE_0), .clk(WrClk), .ScanMode(ScanMode_buf));
   macro_latwl latwl_4 (.wl(WWL[4]), .DecWrAdr(DecWrAdr[4]), .e1DecWrAdr(e1DecWrAdr[4]), .wen(WE_0), .clk(WrClk), .ScanMode(ScanMode_buf));
   macro_latwl latwl_5 (.wl(WWL[5]), .DecWrAdr(DecWrAdr[5]), .e1DecWrAdr(e1DecWrAdr[5]), .wen(WE_0), .clk(WrClk), .ScanMode(ScanMode_buf));
   macro_latwl latwl_6 (.wl(WWL[6]), .DecWrAdr(DecWrAdr[6]), .e1DecWrAdr(e1DecWrAdr[6]), .wen(WE_0), .clk(WrClk), .ScanMode(ScanMode_buf));
   macro_latwl latwl_7 (.wl(WWL[7]), .DecWrAdr(DecWrAdr[7]), .e1DecWrAdr(e1DecWrAdr[7]), .wen(WE_0), .clk(WrClk), .ScanMode(ScanMode_buf));

// keep it backward compatible
wire WrEn = WrEn_0;

// input data register
always @(posedge WrClk) WrDat[38:0] <= e1WrDat;

//
// this instantiates a Latch Array Submodule
// which includes any column muxing
//


   LArr8x39 LArr8x39 (
	.NullClk(),
	.WWL(WWL[7:0]),
	.WrDat(WrDat[38:0]),
	.RWL(RWLB[7:0]),
	.RdDat(RdDat[38:0])
   );
   endmodule

