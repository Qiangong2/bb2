/*
 *  io_reg.v
 *
 *  NOTE: This is a generated file.  DO NOT HAND EDIT.
 *
 *  Generated from ../../../../bw/fdl/io_reg.fdl
 */


/*
 *  io_base_address value
 */
`define IO_BASE_DI	32'h0c006000
`define IO_BASE_SI	32'h0c006400
`define IO_BASE_EXI	32'h0c006800
`define IO_BASE_AI	32'h0c006c00

/*
 *  io_mod enum
 */
`define IO_ADDR_DI	2'b00
`define IO_ADDR_SI	2'b01
`define IO_ADDR_EXI	2'b10
`define IO_ADDR_AI	2'b11

/*
 *  exi_reg enum
 */
`define EXI_0CPR_IDX	4'b0000
`define EXI_0MAR_IDX	4'b0001
`define EXI_0LENGTH_IDX	4'b0010
`define EXI_0CR_IDX	4'b0011
`define EXI_0DATA_IDX	4'b0100
`define EXI_1CPR_IDX	4'b0101
`define EXI_1MAR_IDX	4'b0110
`define EXI_1LENGTH_IDX	4'b0111
`define EXI_1CR_IDX	4'b1000
`define EXI_1DATA_IDX	4'b1001
`define EXI_2CPR_IDX	4'b1010
`define EXI_2MAR_IDX	4'b1011
`define EXI_2LENGTH_IDX	4'b1100
`define EXI_2CR_IDX	4'b1101
`define EXI_2DATA_IDX	4'b1110
`define EXI_REG_UNUSED_15	4'b1111

/*
 *  exi_romdis enum
 */
`define EXI_ROMDIS_ENABLED	1'b0
`define EXI_ROMDIS_DISABLED	1'b1

/*
 *  exi_ext enum
 */
`define EXI_EXT_NOT_PRESENT	1'b0
`define EXI_EXT_PRESENT	1'b1

/*
 *  io_int enum
 */
`define IO_INT_NO_REQUEST	1'b0
`define IO_INT_REQUEST	1'b1

/*
 *  io_intmsk enum
 */
`define IO_INTMSK_MASKED	1'b0
`define IO_INTMSK_ENABLED	1'b1

/*
 *  exi_csb enum
 */
`define EXI_CSB_DESELECT	1'b0
`define EXI_CSB_SELECT	1'b1

/*
 *  exi_clk enum
 */
`define EXI_CLK_1MHZ	3'b000
`define EXI_CLK_2MHZ	3'b001
`define EXI_CLK_4MHZ	3'b010
`define EXI_CLK_8MHZ	3'b011
`define EXI_CLK_16MHZ	3'b100
`define EXI_CLK_32MHZ	3'b101
`define EXI_CLK_UNUSED_6	3'b110
`define EXI_CLK_UNUSED_7	3'b111

/*
 *  exi_0cpr struct
 */
`define EXI_0CPR_EXIINTMSK	0:0
`define EXI_0CPR_EXIINT	1:1
`define EXI_0CPR_TCINTMSK	2:2
`define EXI_0CPR_TCINT	3:3
`define EXI_0CPR_CLK	6:4
`define EXI_0CPR_CS0B	7:7
`define EXI_0CPR_CS1B	8:8
`define EXI_0CPR_CS2B	9:9
`define EXI_0CPR_EXTINTMSK	10:10
`define EXI_0CPR_EXTINT	11:11
`define EXI_0CPR_EXT	12:12
`define EXI_0CPR_ROMDIS	13:13
`define EXI_0CPR_TOTAL_SIZE	32'd14
`define EXI_0CPR_TOTAL_RANGE	13:0

/*
 *  exi_0mar struct
 */
`define EXI_0MAR_PAD0	4:0
`define EXI_0MAR_EXIMAR	25:5
`define EXI_0MAR_PAD1	31:26
`define EXI_0MAR_TOTAL_SIZE	32'd32
`define EXI_0MAR_TOTAL_RANGE	31:0

/*
 *  exi_0length struct
 */
`define EXI_0LENGTH_PAD0	4:0
`define EXI_0LENGTH_EXILENGTH	25:5
`define EXI_0LENGTH_PAD1	31:26
`define EXI_0LENGTH_TOTAL_SIZE	32'd32
`define EXI_0LENGTH_TOTAL_RANGE	31:0

/*
 *  exi_tlen enum
 */
`define EXI_TLEN_1B	2'b00
`define EXI_TLEN_2B	2'b01
`define EXI_TLEN_3B	2'b10
`define EXI_TLEN_4B	2'b11

/*
 *  exi_rw enum
 */
`define EXI_RW_RD	2'b00
`define EXI_RW_WR	2'b01
`define EXI_RW_RDWR	2'b10
`define EXI_RW_UNUSED_3	2'b11

/*
 *  io_dma enum
 */
`define IO_DMA_IMM	1'b0
`define IO_DMA_DMA	1'b1

/*
 *  di_rw enum
 */
`define DI_RW_RD	1'b0
`define DI_RW_WR	1'b1

/*
 *  io_tstart enum
 */
`define IO_TSTART_DONE	1'b0
`define IO_TSTART_START	1'b1

/*
 *  exi_0cr struct
 */
`define EXI_0CR_TSTART	0:0
`define EXI_0CR_DMA	1:1
`define EXI_0CR_RW	3:2
`define EXI_0CR_TLEN	5:4
`define EXI_0CR_TOTAL_SIZE	32'd6
`define EXI_0CR_TOTAL_RANGE	5:0

/*
 *  exi_0data struct
 */
`define EXI_0DATA_DATA3	7:0
`define EXI_0DATA_DATA2	15:8
`define EXI_0DATA_DATA1	23:16
`define EXI_0DATA_DATA0	31:24
`define EXI_0DATA_TOTAL_SIZE	32'd32
`define EXI_0DATA_TOTAL_RANGE	31:0

/*
 *  exi_1cpr struct
 */
`define EXI_1CPR_EXIINTMSK	0:0
`define EXI_1CPR_EXIINT	1:1
`define EXI_1CPR_TCINTMSK	2:2
`define EXI_1CPR_TCINT	3:3
`define EXI_1CPR_CLK	6:4
`define EXI_1CPR_CS0B	7:7
`define EXI_1CPR_PAD0	9:8
`define EXI_1CPR_EXTINTMSK	10:10
`define EXI_1CPR_EXTINT	11:11
`define EXI_1CPR_EXT	12:12
`define EXI_1CPR_PAD1	13:13
`define EXI_1CPR_TOTAL_SIZE	32'd14
`define EXI_1CPR_TOTAL_RANGE	13:0

/*
 *  exi_1mar struct
 */
`define EXI_1MAR_PAD0	4:0
`define EXI_1MAR_EXIMAR	25:5
`define EXI_1MAR_PAD1	31:26
`define EXI_1MAR_TOTAL_SIZE	32'd32
`define EXI_1MAR_TOTAL_RANGE	31:0

/*
 *  exi_1length struct
 */
`define EXI_1LENGTH_PAD0	4:0
`define EXI_1LENGTH_EXILENGTH	25:5
`define EXI_1LENGTH_PAD1	31:26
`define EXI_1LENGTH_TOTAL_SIZE	32'd32
`define EXI_1LENGTH_TOTAL_RANGE	31:0

/*
 *  exi_1cr struct
 */
`define EXI_1CR_TSTART	0:0
`define EXI_1CR_DMA	1:1
`define EXI_1CR_RW	3:2
`define EXI_1CR_TLEN	5:4
`define EXI_1CR_TOTAL_SIZE	32'd6
`define EXI_1CR_TOTAL_RANGE	5:0

/*
 *  exi_1data struct
 */
`define EXI_1DATA_DATA3	7:0
`define EXI_1DATA_DATA2	15:8
`define EXI_1DATA_DATA1	23:16
`define EXI_1DATA_DATA0	31:24
`define EXI_1DATA_TOTAL_SIZE	32'd32
`define EXI_1DATA_TOTAL_RANGE	31:0

/*
 *  exi_2cpr struct
 */
`define EXI_2CPR_EXIINTMSK	0:0
`define EXI_2CPR_EXIINT	1:1
`define EXI_2CPR_TCINTMSK	2:2
`define EXI_2CPR_TCINT	3:3
`define EXI_2CPR_CLK	6:4
`define EXI_2CPR_CS0B	7:7
`define EXI_2CPR_PAD0	13:8
`define EXI_2CPR_TOTAL_SIZE	32'd14
`define EXI_2CPR_TOTAL_RANGE	13:0

/*
 *  exi_2mar struct
 */
`define EXI_2MAR_PAD0	4:0
`define EXI_2MAR_EXIMAR	25:5
`define EXI_2MAR_PAD1	31:26
`define EXI_2MAR_TOTAL_SIZE	32'd32
`define EXI_2MAR_TOTAL_RANGE	31:0

/*
 *  exi_2length struct
 */
`define EXI_2LENGTH_PAD0	4:0
`define EXI_2LENGTH_EXILENGTH	25:5
`define EXI_2LENGTH_PAD1	31:26
`define EXI_2LENGTH_TOTAL_SIZE	32'd32
`define EXI_2LENGTH_TOTAL_RANGE	31:0

/*
 *  exi_2cr struct
 */
`define EXI_2CR_TSTART	0:0
`define EXI_2CR_DMA	1:1
`define EXI_2CR_RW	3:2
`define EXI_2CR_TLEN	5:4
`define EXI_2CR_TOTAL_SIZE	32'd6
`define EXI_2CR_TOTAL_RANGE	5:0

/*
 *  exi_2data struct
 */
`define EXI_2DATA_DATA3	7:0
`define EXI_2DATA_DATA2	15:8
`define EXI_2DATA_DATA1	23:16
`define EXI_2DATA_DATA0	31:24
`define EXI_2DATA_TOTAL_SIZE	32'd32
`define EXI_2DATA_TOTAL_RANGE	31:0

/*
 *  di_reg enum
 */
`define DI_SR_IDX	4'b0000
`define DI_CVR_IDX	4'b0001
`define DI_CMDBUF0_IDX	4'b0010
`define DI_CMDBUF1_IDX	4'b0011
`define DI_CMDBUF2_IDX	4'b0100
`define DI_MAR_IDX	4'b0101
`define DI_LENGTH_IDX	4'b0110
`define DI_CR_IDX	4'b0111
`define DI_IMMBUF_IDX	4'b1000
`define DI_CONFIG_IDX	4'b1001
`define DI_REG_UNUSED_10	4'b1010
`define DI_REG_UNUSED_11	4'b1011
`define DI_REG_UNUSED_12	4'b1100
`define DI_REG_UNUSED_13	4'b1101
`define DI_REG_UNUSED_14	4'b1110
`define DI_REG_UNUSED_15	4'b1111

/*
 *  di_brk enum
 */
`define DI_BRK_DONE	1'b0
`define DI_BRK_REQUEST	1'b1

/*
 *  di_sr struct
 */
`define DI_SR_BRK	0:0
`define DI_SR_DEINTMSK	1:1
`define DI_SR_DEINT	2:2
`define DI_SR_TCINTMSK	3:3
`define DI_SR_TCINT	4:4
`define DI_SR_BRKINTMSK	5:5
`define DI_SR_BRKINT	6:6
`define DI_SR_TOTAL_SIZE	32'd7
`define DI_SR_TOTAL_RANGE	6:0

/*
 *  di_cvr enum
 */
`define DI_CVR_CLOSED	1'b0
`define DI_CVR_OPEN	1'b1

/*
 *  di_cvr struct
 */
`define DI_CVR_CVR	0:0
`define DI_CVR_CVRINTMSK	1:1
`define DI_CVR_CVRINT	2:2
`define DI_CVR_TOTAL_SIZE	32'd3
`define DI_CVR_TOTAL_RANGE	2:0

/*
 *  di_cmdbuf0 struct
 */
`define DI_CMDBUF0_CMDBYTE0	7:0
`define DI_CMDBUF0_CMDBYTE1	15:8
`define DI_CMDBUF0_CMDBYTE2	23:16
`define DI_CMDBUF0_CMDBYTE3	31:24
`define DI_CMDBUF0_TOTAL_SIZE	32'd32
`define DI_CMDBUF0_TOTAL_RANGE	31:0

/*
 *  di_cmdbuf1 struct
 */
`define DI_CMDBUF1_CMDBYTE4	7:0
`define DI_CMDBUF1_CMDBYTE5	15:8
`define DI_CMDBUF1_CMDBYTE6	23:16
`define DI_CMDBUF1_CMDBYTE7	31:24
`define DI_CMDBUF1_TOTAL_SIZE	32'd32
`define DI_CMDBUF1_TOTAL_RANGE	31:0

/*
 *  di_cmdbuf2 struct
 */
`define DI_CMDBUF2_CMDBYTE8	7:0
`define DI_CMDBUF2_CMDBYTE9	15:8
`define DI_CMDBUF2_CMDBYTE10	23:16
`define DI_CMDBUF2_CMDBYTE11	31:24
`define DI_CMDBUF2_TOTAL_SIZE	32'd32
`define DI_CMDBUF2_TOTAL_RANGE	31:0

/*
 *  di_mar struct
 */
`define DI_MAR_PAD0	4:0
`define DI_MAR_DIMAR	25:5
`define DI_MAR_PAD1	31:26
`define DI_MAR_TOTAL_SIZE	32'd32
`define DI_MAR_TOTAL_RANGE	31:0

/*
 *  di_length struct
 */
`define DI_LENGTH_PAD0	4:0
`define DI_LENGTH_DILENGTH	25:5
`define DI_LENGTH_PAD1	31:26
`define DI_LENGTH_TOTAL_SIZE	32'd32
`define DI_LENGTH_TOTAL_RANGE	31:0

/*
 *  di_cr struct
 */
`define DI_CR_TSTART	0:0
`define DI_CR_DMA	1:1
`define DI_CR_RW	2:2
`define DI_CR_PAD0	31:3
`define DI_CR_TOTAL_SIZE	32'd32
`define DI_CR_TOTAL_RANGE	31:0

/*
 *  di_immbuf struct
 */
`define DI_IMMBUF_REGVAL3	7:0
`define DI_IMMBUF_REGVAL2	15:8
`define DI_IMMBUF_REGVAL1	23:16
`define DI_IMMBUF_REGVAL0	31:24
`define DI_IMMBUF_TOTAL_SIZE	32'd32
`define DI_IMMBUF_TOTAL_RANGE	31:0

/*
 *  di_config struct
 */
`define DI_CONFIG_CONFIG	7:0
`define DI_CONFIG_PAD0	31:8
`define DI_CONFIG_TOTAL_SIZE	32'd32
`define DI_CONFIG_TOTAL_RANGE	31:0

/*
 *  si_reg enum
 */
`define SI_0OUTBUF_IDX	6'b000000
`define SI_0INBUFH_IDX	6'b000001
`define SI_0INBUFL_IDX	6'b000010
`define SI_1OUTBUF_IDX	6'b000011
`define SI_1INBUFH_IDX	6'b000100
`define SI_1INBUFL_IDX	6'b000101
`define SI_2OUTBUF_IDX	6'b000110
`define SI_2INBUFH_IDX	6'b000111
`define SI_2INBUFL_IDX	6'b001000
`define SI_3OUTBUF_IDX	6'b001001
`define SI_3INBUFH_IDX	6'b001010
`define SI_3INBUFL_IDX	6'b001011
`define SI_POLL_IDX	6'b001100
`define SI_COMCSR_IDX	6'b001101
`define SI_SR_IDX	6'b001110
`define SI_EXILK_IDX	6'b001111
`define SI_REG_UNUSED_16	6'b010000
`define SI_REG_UNUSED_17	6'b010001
`define SI_REG_UNUSED_18	6'b010010
`define SI_REG_UNUSED_19	6'b010011
`define SI_REG_UNUSED_20	6'b010100
`define SI_REG_UNUSED_21	6'b010101
`define SI_REG_UNUSED_22	6'b010110
`define SI_REG_UNUSED_23	6'b010111
`define SI_REG_UNUSED_24	6'b011000
`define SI_REG_UNUSED_25	6'b011001
`define SI_REG_UNUSED_26	6'b011010
`define SI_REG_UNUSED_27	6'b011011
`define SI_REG_UNUSED_28	6'b011100
`define SI_REG_UNUSED_29	6'b011101
`define SI_REG_UNUSED_30	6'b011110
`define SI_REG_UNUSED_31	6'b011111
`define SI_RAM_IDX	6'b100000
`define SI_REG_UNUSED_33	6'b100001
`define SI_REG_UNUSED_34	6'b100010
`define SI_REG_UNUSED_35	6'b100011
`define SI_REG_UNUSED_36	6'b100100
`define SI_REG_UNUSED_37	6'b100101
`define SI_REG_UNUSED_38	6'b100110
`define SI_REG_UNUSED_39	6'b100111
`define SI_REG_UNUSED_40	6'b101000
`define SI_REG_UNUSED_41	6'b101001
`define SI_REG_UNUSED_42	6'b101010
`define SI_REG_UNUSED_43	6'b101011
`define SI_REG_UNUSED_44	6'b101100
`define SI_REG_UNUSED_45	6'b101101
`define SI_REG_UNUSED_46	6'b101110
`define SI_REG_UNUSED_47	6'b101111
`define SI_REG_UNUSED_48	6'b110000
`define SI_REG_UNUSED_49	6'b110001
`define SI_REG_UNUSED_50	6'b110010
`define SI_REG_UNUSED_51	6'b110011
`define SI_REG_UNUSED_52	6'b110100
`define SI_REG_UNUSED_53	6'b110101
`define SI_REG_UNUSED_54	6'b110110
`define SI_REG_UNUSED_55	6'b110111
`define SI_REG_UNUSED_56	6'b111000
`define SI_REG_UNUSED_57	6'b111001
`define SI_REG_UNUSED_58	6'b111010
`define SI_REG_UNUSED_59	6'b111011
`define SI_REG_UNUSED_60	6'b111100
`define SI_REG_UNUSED_61	6'b111101
`define SI_REG_UNUSED_62	6'b111110
`define SI_REG_UNUSED_63	6'b111111

/*
 *  si_0outbuf struct
 */
`define SI_0OUTBUF_OUTPUT1	7:0
`define SI_0OUTBUF_OUTPUT0	15:8
`define SI_0OUTBUF_CMD	23:16
`define SI_0OUTBUF_PAD0	31:24
`define SI_0OUTBUF_TOTAL_SIZE	32'd32
`define SI_0OUTBUF_TOTAL_RANGE	31:0

/*
 *  si_errlatch enum
 */
`define SI_ERRLATCH_NONE	1'b0
`define SI_ERRLATCH_ERROR	1'b1

/*
 *  si_errstat enum
 */
`define SI_ERRSTAT_NONE	1'b0
`define SI_ERRSTAT_ERROR	1'b1

/*
 *  si_0inbufh struct
 */
`define SI_0INBUFH_INPUT3	7:0
`define SI_0INBUFH_INPUT2	15:8
`define SI_0INBUFH_INPUT1	23:16
`define SI_0INBUFH_INPUT0	29:24
`define SI_0INBUFH_ERRLATCH	30:30
`define SI_0INBUFH_ERRSTAT	31:31
`define SI_0INBUFH_TOTAL_SIZE	32'd32
`define SI_0INBUFH_TOTAL_RANGE	31:0

/*
 *  si_0inbufl struct
 */
`define SI_0INBUFL_INPUT7	7:0
`define SI_0INBUFL_INPUT6	15:8
`define SI_0INBUFL_INPUT5	23:16
`define SI_0INBUFL_INPUT4	31:24
`define SI_0INBUFL_TOTAL_SIZE	32'd32
`define SI_0INBUFL_TOTAL_RANGE	31:0

/*
 *  si_1outbuf struct
 */
`define SI_1OUTBUF_OUTPUT1	7:0
`define SI_1OUTBUF_OUTPUT0	15:8
`define SI_1OUTBUF_CMD	23:16
`define SI_1OUTBUF_PAD0	31:24
`define SI_1OUTBUF_TOTAL_SIZE	32'd32
`define SI_1OUTBUF_TOTAL_RANGE	31:0

/*
 *  si_1inbufh struct
 */
`define SI_1INBUFH_INPUT3	7:0
`define SI_1INBUFH_INPUT2	15:8
`define SI_1INBUFH_INPUT1	23:16
`define SI_1INBUFH_INPUT0	29:24
`define SI_1INBUFH_ERRLATCH	30:30
`define SI_1INBUFH_ERRSTAT	31:31
`define SI_1INBUFH_TOTAL_SIZE	32'd32
`define SI_1INBUFH_TOTAL_RANGE	31:0

/*
 *  si_1inbufl struct
 */
`define SI_1INBUFL_INPUT7	7:0
`define SI_1INBUFL_INPUT6	15:8
`define SI_1INBUFL_INPUT5	23:16
`define SI_1INBUFL_INPUT4	31:24
`define SI_1INBUFL_TOTAL_SIZE	32'd32
`define SI_1INBUFL_TOTAL_RANGE	31:0

/*
 *  si_2outbuf struct
 */
`define SI_2OUTBUF_OUTPUT1	7:0
`define SI_2OUTBUF_OUTPUT0	15:8
`define SI_2OUTBUF_CMD	23:16
`define SI_2OUTBUF_PAD0	31:24
`define SI_2OUTBUF_TOTAL_SIZE	32'd32
`define SI_2OUTBUF_TOTAL_RANGE	31:0

/*
 *  si_2inbufh struct
 */
`define SI_2INBUFH_INPUT3	7:0
`define SI_2INBUFH_INPUT2	15:8
`define SI_2INBUFH_INPUT1	23:16
`define SI_2INBUFH_INPUT0	29:24
`define SI_2INBUFH_ERRLATCH	30:30
`define SI_2INBUFH_ERRSTAT	31:31
`define SI_2INBUFH_TOTAL_SIZE	32'd32
`define SI_2INBUFH_TOTAL_RANGE	31:0

/*
 *  si_2inbufl struct
 */
`define SI_2INBUFL_INPUT7	7:0
`define SI_2INBUFL_INPUT6	15:8
`define SI_2INBUFL_INPUT5	23:16
`define SI_2INBUFL_INPUT4	31:24
`define SI_2INBUFL_TOTAL_SIZE	32'd32
`define SI_2INBUFL_TOTAL_RANGE	31:0

/*
 *  si_3outbuf struct
 */
`define SI_3OUTBUF_OUTPUT1	7:0
`define SI_3OUTBUF_OUTPUT0	15:8
`define SI_3OUTBUF_CMD	23:16
`define SI_3OUTBUF_PAD0	31:24
`define SI_3OUTBUF_TOTAL_SIZE	32'd32
`define SI_3OUTBUF_TOTAL_RANGE	31:0

/*
 *  si_3inbufh struct
 */
`define SI_3INBUFH_INPUT3	7:0
`define SI_3INBUFH_INPUT2	15:8
`define SI_3INBUFH_INPUT1	23:16
`define SI_3INBUFH_INPUT0	29:24
`define SI_3INBUFH_ERRLATCH	30:30
`define SI_3INBUFH_ERRSTAT	31:31
`define SI_3INBUFH_TOTAL_SIZE	32'd32
`define SI_3INBUFH_TOTAL_RANGE	31:0

/*
 *  si_3inbufl struct
 */
`define SI_3INBUFL_INPUT7	7:0
`define SI_3INBUFL_INPUT6	15:8
`define SI_3INBUFL_INPUT5	23:16
`define SI_3INBUFL_INPUT4	31:24
`define SI_3INBUFL_TOTAL_SIZE	32'd32
`define SI_3INBUFL_TOTAL_RANGE	31:0

/*
 *  si_vbcpy enum
 */
`define SI_VBCPY_DISABLE	1'b0
`define SI_VBCPY_ENABLE	1'b1

/*
 *  si_en enum
 */
`define SI_EN_DISABLE	1'b0
`define SI_EN_ENABLE	1'b1

/*
 *  si_poll struct
 */
`define SI_POLL_VBCPY3	0:0
`define SI_POLL_VBCPY2	1:1
`define SI_POLL_VBCPY1	2:2
`define SI_POLL_VBCPY0	3:3
`define SI_POLL_EN3	4:4
`define SI_POLL_EN2	5:5
`define SI_POLL_EN1	6:6
`define SI_POLL_EN0	7:7
`define SI_POLL_Y	15:8
`define SI_POLL_X	25:16
`define SI_POLL_PAD0	31:26
`define SI_POLL_TOTAL_SIZE	32'd32
`define SI_POLL_TOTAL_RANGE	31:0

/*
 *  si_channel enum
 */
`define SI_CHANNEL_0	2'b00
`define SI_CHANNEL_1	2'b01
`define SI_CHANNEL_2	2'b10
`define SI_CHANNEL_3	2'b11

/*
 *  si_comcsr struct
 */
`define SI_COMCSR_TSTART	0:0
`define SI_COMCSR_CHANNEL	2:1
`define SI_COMCSR_PAD0	7:3
`define SI_COMCSR_INLNGTH	14:8
`define SI_COMCSR_PAD1	15:15
`define SI_COMCSR_OUTLNGTH	22:16
`define SI_COMCSR_PAD2	26:23
`define SI_COMCSR_RDSTINTMSK	27:27
`define SI_COMCSR_RDSTINT	28:28
`define SI_COMCSR_COMERR	29:29
`define SI_COMCSR_TCINTMSK	30:30
`define SI_COMCSR_TCINT	31:31
`define SI_COMCSR_TOTAL_SIZE	32'd32
`define SI_COMCSR_TOTAL_RANGE	31:0

/*
 *  si_error enum
 */
`define SI_ERROR_NONE	1'b0
`define SI_ERROR_ERROR	1'b1

/*
 *  si_wr enum
 */
`define SI_WR_DONE	1'b0
`define SI_WR_WRITE	1'b1

/*
 *  si_rd enum
 */
`define SI_RD_DONE	1'b0
`define SI_RD_VALID	1'b1

/*
 *  si_sr struct
 */
`define SI_SR_UNRUN3	0:0
`define SI_SR_OVRUN3	1:1
`define SI_SR_COLL3	2:2
`define SI_SR_NOREP3	3:3
`define SI_SR_WRST3	4:4
`define SI_SR_RDST3	5:5
`define SI_SR_PAD0	7:6
`define SI_SR_UNRUN2	8:8
`define SI_SR_OVRUN2	9:9
`define SI_SR_COLL2	10:10
`define SI_SR_NOREP2	11:11
`define SI_SR_WRST2	12:12
`define SI_SR_RDST2	13:13
`define SI_SR_PAD1	15:14
`define SI_SR_UNRUN1	16:16
`define SI_SR_OVRUN1	17:17
`define SI_SR_COLL1	18:18
`define SI_SR_NOREP1	19:19
`define SI_SR_WRST1	20:20
`define SI_SR_RDST1	21:21
`define SI_SR_PAD2	23:22
`define SI_SR_UNRUN0	24:24
`define SI_SR_OVRUN0	25:25
`define SI_SR_COLL0	26:26
`define SI_SR_NOREP0	27:27
`define SI_SR_WRST0	28:28
`define SI_SR_RDST0	29:29
`define SI_SR_PAD3	30:30
`define SI_SR_WR	31:31
`define SI_SR_TOTAL_SIZE	32'd32
`define SI_SR_TOTAL_RANGE	31:0

/*
 *  si_exilock enum
 */
`define SI_EXILK_UNLOCKED	1'b0
`define SI_EXILK_LOCKED	1'b1

/*
 *  si_exilk struct
 */
`define SI_EXILK_PAD0	30:0
`define SI_EXILK_LOCK	31:31
`define SI_EXILK_TOTAL_SIZE	32'd32
`define SI_EXILK_TOTAL_RANGE	31:0

/*
 *  ai_reg enum
 */
`define AI_CR_IDX	3'b000
`define AI_VR_IDX	3'b001
`define AI_SCNT_IDX	3'b010
`define AI_IT_IDX	3'b011
`define AI_REG_UNUSED_4	3'b100
`define AI_REG_UNUSED_5	3'b101
`define AI_REG_UNUSED_6	3'b110
`define AI_REG_UNUSED_7	3'b111

/*
 *  ai_pstat enum
 */
`define AI_PSTAT_STOP	1'b0
`define AI_PSTAT_PLAY	1'b1

/*
 *  ai_afr enum
 */
`define AI_AFR_32K	1'b0
`define AI_AFR_48K	1'b1

/*
 *  ai_scrst enum
 */
`define AI_SCRST_NONE	1'b0
`define AI_SCRST_RESET	1'b1

/*
 *  ai_intvld enum
 */
`define AI_INTVLD_VALID	1'b0
`define AI_INTVLD_HOLD	1'b1

/*
 *  ai_dsp32 enum
 */
`define AI_DSP32_OFF	1'b0
`define AI_DSP32_ON	1'b1

/*
 *  ai_dvdaudio enum
 */
`define AI_DVDAUDIO_OFF	1'b0
`define AI_DVDAUDIO_ON	1'b1

/*
 *  ai_cr struct
 */
`define AI_CR_PSTAT	0:0
`define AI_CR_AFR	1:1
`define AI_CR_AIINTMSK	2:2
`define AI_CR_AIINT	3:3
`define AI_CR_AIINTVLD	4:4
`define AI_CR_SCRST	5:5
`define AI_CR_DSP32	6:6
`define AI_CR_TOTAL_SIZE	32'd7
`define AI_CR_TOTAL_RANGE	6:0

/*
 *  ai_vr struct
 */
`define AI_VR_AVRL	7:0
`define AI_VR_AVRR	15:8
`define AI_VR_TOTAL_SIZE	32'd16
`define AI_VR_TOTAL_RANGE	15:0

/*
 *  ai_scnt struct
 */
`define AI_SCNT_AISCNT	30:0
`define AI_SCNT_TOTAL_SIZE	32'd31
`define AI_SCNT_TOTAL_RANGE	30:0

/*
 *  ai_it struct
 */
`define AI_IT_AIIT	30:0
`define AI_IT_DVDAUDIO	31:31
`define AI_IT_TOTAL_SIZE	32'd32
`define AI_IT_TOTAL_RANGE	31:0

/*
 *  io_reg struct
 */
`define IO_REG_LO	15:0
`define IO_REG_HI	31:16
`define IO_REG_TOTAL_SIZE	32'd32
`define IO_REG_TOTAL_RANGE	31:0

