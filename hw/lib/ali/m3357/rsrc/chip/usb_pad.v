
module usb_pad ( A, ANT, DM, DP, H_LB, OEB, RCVD, SE0, SE0D,  
    YNT );
input  A, ANT, H_LB, OEB, SE0 ;
output RCVD, SE0D, YNT;
inout  DM, DP;
    wire n231, n232, n233, n234, n235, n236;
    TBUFX8 DM_tri ( .A(n234), .OE(n236), .Y(DM) );
    BUFX4 C31 ( .A(DP), .Y(RCVD) );
    TBUFX4 DP_tri ( .A(n235), .OE(n236), .Y(DP) );
    OR2X4 C32 ( .A(n231), .B(n232), .Y(YNT) );
    NOR2X4 C33 ( .A(DP), .B(DM), .Y(SE0D) );
    INVX4 C34 ( .A(OEB), .Y(n236) );
    AND2X4 C35 ( .A(A), .B(n233), .Y(n235) );
    NOR2X4 C36 ( .A(SE0), .B(A), .Y(n234) );
    AND2X4 C37 ( .A(RCVD), .B(ANT), .Y(n231) );
    INVX4 C38 ( .A(DM), .Y(n232) );
    INVX4 C39 ( .A(SE0), .Y(n233) );
endmodule

