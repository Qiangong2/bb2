/*********************************************************************
File:           scan_mux.v
Descript:       control the scan chain input/output mux
				The scan chain number in M6304 is limited by avaliable
				I/O.
Author:         Norman
Histroy:        2002-10-15      initial
				2003-09-18		Change the scan chain number to 94
*********************************************************************/
module  scan_mux(
		scan_din_out2core,
		scan_dout_fromcore,
		scan_din_frompadmisc,
		scan_dout_out2padmisc,

		test_mode,
		test_sel
		);


output	[112:0]	scan_din_out2core;			// In put data out to core
input	[112:0]	scan_dout_fromcore;			// Output data from core

input	[59:0]	scan_din_frompadmisc;		// Input data from padmisc
output	[59:0]	scan_dout_out2padmisc;		// Output data to padmisc

input	test_mode;							// chip scan test mode
input	test_sel;							// scan chain select

	//=========================================================================
	//	In test mode, when the test_sel = 1, transfer the scan din data to high
	//	part of scan_din of core, otherwise transfer the scan din data to low
	//	part of scan_din of core.
	//	If test mode not enable, set the scan din to core to 0
	//=========================================================================
assign  scan_din_out2core[6:0] = scan_din_frompadmisc[6:0];	//nb, mem

assign	scan_din_out2core[46:7]
			= test_mode ? (!test_sel ? scan_din_frompadmisc[46:7] : 'h0) : 'h0;

assign	scan_din_out2core[86:47]
			= test_mode ? (test_sel ? scan_din_frompadmisc[46:7] : 'h0) : 'h0;

assign	scan_din_out2core[99:87]
			= test_mode ? (!test_sel ? scan_din_frompadmisc[59:47] : 'h0) : 'h0;

assign	scan_din_out2core[112:100]
			= test_mode ? (test_sel ? scan_din_frompadmisc[59:47] : 'h0) : 'h0;

	//=========================================================================
	//	In test mode, when the test_sel = 1, select the high part of scan_dout
	//	to PAD, otherwise transfer the low part of scan_dout to PAD
	//	If test mode not enabled, set the scan dout to PAD to 0
	//=========================================================================
assign  scan_dout_out2padmisc[6:0] = scan_dout_fromcore[6:0];	//nb, mem

assign	scan_dout_out2padmisc[46:7]
			= test_mode ? (test_sel ? scan_dout_fromcore[86:47] : scan_dout_fromcore[46:7]) : 'h0;

assign	scan_dout_out2padmisc[59:47]
			= test_mode ? (test_sel ? scan_dout_fromcore[112:100] : scan_dout_fromcore[99:87]) : 'h0;

endmodule
