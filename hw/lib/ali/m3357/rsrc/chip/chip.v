/**********************************************************************************
// Description: M3357 top level module
// File:        chip.v
// Author:      Jeffrey Xie
//
// History:     2003-06-07      copy from m6304 chip.v and modify it to m3357 chip.v
//                              But the strap pins and mode config still not confirm.
//                              So I just modify the pins list,add servo mode as main
//								work mode, IDE mode will be used as debug mode.
//                              There are following module, core, PLL behavior model,
//                              pad, pad_misc, clock generator, pad_rst_strap.
//				2003-06-10		add SERVO(70), ADC(5), ruduce gpio form 14 to 8
//				2003-06-28		removed ac97, gsi, up, cd, spf, ps
				2003-08-28		Modify the PLL ports
				2003-09-01		Add VIDEO_CLK_GEN. The F54MHz output from CLOCK_GEN is
								only for IDE. The VIDEO clock could come from
								VIDEO_CLOCK_GEN
				2003-09-18		Modify the scan chain mux part.
				2003-11-28		modify the xsflag[1:0] logic
				2003-12-02		Connect the IDE_PROBE_EN signal to pad_misc for the
								EJTAG/GPIO share pin logic
				2003-12-05		Add the SVGA mode enable and the tv encoder clock setting
				2003-12-09		Change some port direction from output/input to inout
				2003-12-15		Remove the servo digital power port on the chip.v
								Following power port be removed: vd33d, vd18d, azec_vdd, azec_gnd
								dvdd, dvss, fad_gnd, fad_vdd, gndd
***********************************************************************************/

module chip (
// SDRAM Interface 16 bit sdram(38) (56 for m6304)
        p_d_clk,         // 1,   SDRAM master clock input
        p_d_addr,       // 12,  SDRAM address
        p_d_baddr,      // 2,   SDRAM bank address
        p_d_dq,         // 32,  SDRAM 32 bits output data pins
        p_d_dqm,        // 4,   SDRAM data mask signal
        p_d_cs_,        // 2,   SDRAM chip select
        p_d_cas_,       // 1,   SDRAM cas#
        p_d_we_,        // 1,   SDRAM we#
        p_d_ras_,       // 1,   SDRAM ras#
// FLASH ROM Interface(32, do not share with IDE bus as m6304)
        p_rom_ce_,      // 1,   Flash rom chip select
        p_rom_we_,      // 1,   Flash rom write enable
        p_rom_oe_,      // 1,   Flash rom output enable
        p_rom_data,		// 8 	Flash rom data
        p_rom_addr,		// 21	Flash rom address
// I2S Audio Interface (8)
        i2so_data0,     // 1,   Serial data for I2S output 0
        i2so_data1,     // 1,   Serial data for I2S output 1
        i2so_data2,     // 1,   Serial data for I2S output 2
        i2so_data3,     // 1,   Serial data for I2S output 3
        i2so_bck,       // 1,   Bit clock for I2S output
        i2so_lrck,      // 1,   Channel clock for I2S output
        i2so_mclk,      // 1,   over sample clock for i2s DAC
//        i2si_data,      // 1,   serial data for I2S input


//TV encoder Interface (13)
		vd33a_tvdac,	// 1,	Analog Power, input
		gnda_tvdac,		// 1,	Analog GND, input
		gnda1_tvdac,	// 1,	Analog GND, input
		vsud_tvdac,		// 1,	Analog GND, input
		iout1,			// 1,   Analog video output 1
		iout2,			// 1,   Analog video output 2
		iout3,			// 1,   Analog video output 3
		iout4,			// 1,   Analog video output 4
		iout5,			// 1,   Analog video output 5
		iout6,			// 1,   Analog video output 6
		xiref1,			// 1,	connect external via capacitance toward VDDA
		xiref2,			// 1,	connect external via capacitance toward VDDA, not valid in 2003-12-05
		xiext,			// 1,	470 Ohms resistor connect pin, I/O
		xiext1,			// 1,	470 Ohms resistor connect pin, I/O
		xidump,			// 1,	For performance and chip temperature, output

// SPDIF (1)
        spdif,          // 1, output

// PCI (17)
//		pci_ad,			// 3, pci address/data [29:31]
//		pci_cbe_,		// 4, pci control/byte enable [0:3]
//		pci_clk, 		// 1,
//		pci_frame_,		// 1,
//		pci_irdy_,      // 1,
//		pci_trdy_,      // 1,
//		pci_stop_,      // 1,
//		pci_devsel_,    // 1,
//		pci_inta_,      // 1,
//		pci_req_,       // 1,
//		pci_par,        // 1,
//		pci_gnta_,       // 1,

//UART  (1)
		uart_tx,		// 1

// USB port (6)
   //     usb4dp,         // 1,   D+ for USB port4
   //     usb4dn,         // 1,   D- for USB port4
   //     usb3dp,         // 1,   D+ for USB port3
   //     usb3dn,         // 1,   D- for USB port3
        usb2dp,         // 1,   D+ for USB port2
        usb2dn,         // 1,   D- for USB port2
        usb1dp,         // 1,   D+ for USB port1
        usb1dn,         // 1,   D- for USB port1
        usbpon,         // 1,   USB port power control
        usbovc,         // 1,   USB port over current
//SERVO port (70) JFR030611
   		XADCIN		,
   		XADCIP		,
   		XATTON		,
   		XATTOP		,
   		XBIASR		,
   		XCDLD		,
   		XCDPD		,
   		XCDRF		,
   		XCD_A		,
   		XCD_B		,
   		XCD_C		,
   		XCD_D		,
   		XCD_E		,
   		XCD_F		,
   		XCELPFO		,
   		XDPD_A		,
   		XDPD_B		,
   		XDPD_C		,
   		XDPD_D		,
   		XDVDLD		,
   		XDVDPD		,
   		XDVDRFN		,
   		XDVDRFP		,
   		XDVD_A		,
   		XDVD_B		,
   		XDVD_C		,
   		XDVD_D		,
   		XFELPFO		,
   		XFOCUS		,
   		XGMBIASR	,
   		XLPFON		,
   		XLPFOP		,
   		XPDAUX1		,
   		XPDAUX2		,
   		XSBLPFO		,
   		XSFGN		,
   		XSFGP		,
   		XSFLAG		,		//	carcy add 2003/11/17 18:52
   		XSLEGN		,
   		XSLEGP		,
   		XSPINDLE	,
   		XTELP		,
   		XTELPFO		,
   		XTESTDA		,
   		XTEXO		,
   		XTRACK		,
   		XTRAY		,
   		XVGAIN		,
   		XVGAIP		,
   		XVREF15		,
   		XVREF21		,

//  		VD33D		,
//  		VD18D		,
  		VDD3MIX1	,
  		VDD3MIX2	,
  		AVDD_AD		,
  		AVDD_ATT	,
  		AVDD_DA		,
  		AVDD_DPD	,
  		AVDD_LPF	,
  		AVDD_RAD	,
  		AVDD_REF	,
  		AVDD_SVO	,
  		AVDD_VGA	,
  		AVSS_AD		,
  		AVSS_ATT	,
  		AVSS_DA		,
  		AVSS_DPD	,
  		AVSS_GA		,
  		AVSS_GD		,
  		AVSS_LPF	,
  		AVSS_RAD	,
  		AVSS_REF	,
  		AVSS_SVO	,
  		AVSS_VGA	,
//  		AZEC_GND	,
//  		AZEC_VDD	,
  		DV18_RCKA	,
  		DV18_RCKD	,
//  		DVDD	    ,
//  		DVSS	    ,
  		DVSS_RCKA	,
  		DVSS_RCKD	,
//  		FAD_GND		,
//  		FAD_VDD		,
//  		GNDD		,


// Consumer IR Interface (1)
        irrx,           // 1,   consumer infra-red remote controller/wireless keyboard

// General Purpose IO (14,GPIO)
        gpio,           // 14,  General purpose io[7:0]

// EJTAG Interface (5)
        p_j_tdo,        // 1,   serial test data output
        p_j_tdi,        // 1,   serial test data input
        p_j_tms,        // 1,   test mode select
        p_j_tclk,       // 1,   test clock
        p_j_rst_,       // 1,   test reset

// power and ground
		vddcore,		// 		core vdd
//		vsscore,		//		core vss
		vddio,			//		io vdd
		gnd,			//		io vss
		cpu_pllvdd,			//		pll vdd for cpu pll
		cpu_pllvss,			//		pll vss for cpu pll
		f27_pllvdd,			//		pll vdd for f27 pll
		f27_pllvss,			//		pll vss for f27 pll

// system (4)
        p_crst_,        	// 1,   cold reset
        test_mode,      	// 1,   chip test mode input, (for scan chain)
        p_x27in,        	// 1,   crystal input (27 MHz for video output)
        p_x27out   ,     	// 1,   crystal output
//        test_sel,			// 1,   select the scan chain port
//        p_x48in,        	// 1,   crystal input (48 MHz for USB)
//        p_x48out       	// 1,   crystal output
//ADC (5)
		vd33a_adc	,		// 1,	3.3V Analog VDD for ADC.
		xmic1in     ,        // 1,   ADC microphone input 1.
		xadc_vref   ,        // 1,   ADC reference voltage (refered to the reference circuit).
		xmic2in     ,        // 1,   ADC microphone input 2.
		gnda_adc            // 1,   Analog GND for ADC.

        );

parameter       udly = 1;
parameter       ram_dly = 1,
                pci_dly = 1,
                oth_dly = 1;
//============define the input/output/inout signals===============
//--------------SDRAM Interface (56)
output          p_d_clk;                 // 1,   SDRAM master clock input
inout   [11:0]  p_d_addr;               // 12,  SDRAM address
inout   [1:0]   p_d_baddr;      		// 2,   SDRAM bank address
inout   [31:0]  p_d_dq;         		// 32,  SDRAM 32 bits output data pins
inout   [3:0]   p_d_dqm;        		// 4,   SDRAM data mask signal
inout   		p_d_cs_;        		// 1,   SDRAM chip select
inout           p_d_cas_;               // 1,   SDRAM cas#
inout           p_d_we_;                // 1,   SDRAM we#
inout           p_d_ras_;               // 1,   SDRAM ras#
//--------------FLASH ROM Interface(3, others share with IDE bus)
inout           p_rom_ce_;              // 1,   Flash rom chip select
inout           p_rom_we_;              // 1,   Flash rom write enable
inout           p_rom_oe_;              // 1,   Flash rom output enable
inout   [7:0]   p_rom_data;             // 8,   Flash rom data
inout	[20:0]	p_rom_addr;             // 21,   Flash rom address
//--------------I2S Audio Interface (8)
inout           i2so_data0;             // 1,   Serial data for I2S output
inout 			i2so_data1;     		// 1,   Serial data for I2S output 1
inout   	    i2so_data2;     		// 1,   Serial data for I2S output 2
inout       	i2so_data3;		     	// 1,   Serial data for I2S output 3
inout           i2so_bck;               // 1,   Bit clock for I2S output
inout           i2so_lrck;              // 1,   Channel clock for I2S output
inout           i2so_mclk;              // 1,   over sample clock for i2s DAC
//inout           i2si_data;              // 1,   serial data for I2S input

//TV encoder Interface (13)
input			vd33a_tvdac;	// 1,	Analog Power, input
input			gnda_tvdac;		// 1,	Analog GND, input
input			gnda1_tvdac;	// 1,	Analog GND, input
input			vsud_tvdac;		// 1,	Analog GND, input
output			iout1;			// 1,   Analog video output 1
output			iout2;			// 1,   Analog video output 2
output			iout3;			// 1,   Analog video output 3
output			iout4;			// 1,   Analog video output 4
output			iout5;			// 1,   Analog video output 5
output			iout6;			// 1,   Analog video output 6
output			xiref1;			// 1,	connect external via capacitance toward VDDA
output			xiref2;			// 1,	connect external via capacitance toward VDDA
output			xiext;			// 1,	470 Ohms resistor connect pin, I/O
output			xiext1;			// 1,	470 Ohms resistor connect pin, I/O
output			xidump;			// 1,	For performance and chip temperature, output

//--------------SPDIF (1)
inout			spdif;                  // 1, output

//UART
inout			uart_tx;		// 1

//--------------USB port (6)
//inout           usb4dp;                 // 1,   D+ for USB port4
//inout           usb4dn;                 // 1,   D- for USB port4
//inout           usb3dp;                 // 1,   D+ for USB port3
//inout           usb3dn;                 // 1,   D- for USB port3
inout           usb2dp;                 // 1,   D+ for USB port2
inout           usb2dn;                 // 1,   D- for USB port2
inout           usb1dp;                 // 1,   D+ for USB port1
inout           usb1dn;                 // 1,   D- for USB port1
output          usbpon;                 // 1,   USB port power control
input           usbovc;                 // 1,   USB port over current
//JFR030610
//--------------SEVO port (70)
   input  XADCIN;
   input  XADCIP;
   output XATTON;
   output XATTOP;
   input  XBIASR;
   output XCDLD;
   input  XCDPD;
   input  XCDRF;
   input  XCD_A;
   input  XCD_B;
   input  XCD_C;
   input  XCD_D;
   input  XCD_E;
   input  XCD_F;
   output XCELPFO;
   input  XDPD_A;
   input  XDPD_B;
   input  XDPD_C;
   input  XDPD_D;
   output XDVDLD;
   input  XDVDPD;
   input  XDVDRFN;
   input  XDVDRFP;
   input  XDVD_A;
   input  XDVD_B;
   input  XDVD_C;
   input  XDVD_D;
   output XFELPFO;
   output XFOCUS;
   input  XGMBIASR;
   output XLPFON;
   output XLPFOP;
   input  XPDAUX1;
   input  XPDAUX2;
   output XSBLPFO;
   input  XSFGN;
   input  XSFGP;
   inout [1:0] XSFLAG;		//	carcy add 2003/11/17 18:52
   output XSLEGN;
   output XSLEGP;
   output XSPINDLE;
   input  XTELP;
   output XTELPFO;
   output XTESTDA;
   output XTEXO;
   output XTRACK;
   output XTRAY;
   input  XVGAIN;
   input  XVGAIP;
   output XVREF15;
   output XVREF21;
   // power
//   input  VD33D;
//   input  VD18D;
   input  VDD3MIX1;
   input  VDD3MIX2;
   input  AVDD_AD;
   input  AVDD_ATT;
   input  AVDD_DA;
   input  AVDD_DPD;
   input  AVDD_LPF;
   input  AVDD_RAD;
   input  AVDD_REF;
   input  AVDD_SVO;
   input  AVDD_VGA;
   input  AVSS_AD;
   input  AVSS_ATT;
   input  AVSS_DA;
   input  AVSS_DPD;
   input  AVSS_GA;
   input  AVSS_GD;
   input  AVSS_LPF;
   input  AVSS_RAD;
   input  AVSS_REF;
   input  AVSS_SVO;
   input  AVSS_VGA;
//   input  AZEC_GND;
//   input  AZEC_VDD;
   input  DV18_RCKA;
   input  DV18_RCKD;
//   input  DVDD;
//   input  DVSS;
   input  DVSS_RCKA;
   input  DVSS_RCKD;
//   input  FAD_GND;
//   input  FAD_VDD;
//   input  GNDD;


//--------------Consumer IR Interface (1)
inout			irrx;                   // 1,   consumer infra-red remote controller/wireless keyboard


//--------------General Purpose IO (13:0, 28~24,GPIO)
inout   [28:0]  gpio;                   // 19,  General purpose io


//--------------EJTAG Interface (5)
inout          p_j_tdo;                // 1,   serial test data output
inout           p_j_tdi;                // 1,   serial test data input
inout           p_j_tms;                // 1,   test mode select
inout           p_j_tclk;               // 1,   test clock
inout           p_j_rst_;               // 1,   test reset
// PCI clock	//yuky, 2002-10-07
//output          xpciclk;
// power and ground
input			vddcore;			// 		core vdd
//input			vsscore;			//		core vss
input			vddio;				//		io vdd
input			gnd;				//		io vss
input			cpu_pllvdd;			//		pll vdd
input			cpu_pllvss;			//		pll vss
input			f27_pllvdd;			//		pll vdd for f27 pll
input			f27_pllvss;			//		pll vss for f27 pll

//--------------system (5)
input           p_crst_;            // 1,   cold reset
input           test_mode;          // 1,   chip test mode input for scan
//input			test_sel;			// 1,   select the scan chain port
//input           p_x48in;          // 1,   crystal input (48 MHz for USB)
//output          p_x48out;         // 1,   crystal output
input           p_x27in;            // 1,   crystal input (27 MHz for video output)
output          p_x27out;           // 1,   crystal output

//--------------ADC (5)
input			vd33a_adc	;		// 1,	3.3V Analog VDD for ADC.
input			xmic1in     ;       // 1,   ADC microphone input 1.
output			xadc_vref   ;       // 1,   ADC reference voltage (refered to the reference circuit).
input			xmic2in     ;       // 1,   ADC microphone input 2.
input			gnda_adc    ;       // 1,   Analog GND for ADC.
//================================================================

//===================define the signal types======================
//------------ clock generater
wire            usb_clk_gen2core,       // USB clock 48M
                sb_clk_gen2core ,       // SouthBridge Clock 12.288M
                pixel_clk_gen2core   ,  // Display Engine clock,27MHz
                cpu_clk_gen2core,       // CPU clock (with JTAG test mode mask)
                cpu_clk_src_gen2core,   // CPU clock (without JTAG test mode mask)
                mbus_clk_gen2core,      // MIPS clock
                inter_mem_clk_gen,      // internal SDRAM, NorthBridge clock
                ext_mem_clk_gen,    	// external SDRAM clock,output to pad
                dsp_clk_gen2core,       // SDP Clock
                pci_clk_gen2core,       // internal PCI bus clock
                ve_clk_gen2core,    	// Video clock
                spdif_clk_gen2core, 	// SPDIF clock
                pci_mbus_clk_gen;       // output to PCI/MIPS clock pad


//================================================================
wire    [11:0]  ram_addr_core2padmisc;
wire	[11:0]	ram_addr_padmisc2pad;
wire	[11:0]	ram_addr_oe_padmisc2pad;
//wire	[11:0]	ram_addr_frompad;	JFR030927

wire    [1:0]   ram_ba_core2padmisc;
wire	[1:0]	ram_ba_padmisc2pad;
wire	[1:0]	ram_ba_oe_padmisc2pad_;
//wire	[1:0]	ram_ba_frompad;	JFR030927

wire    [31:0]  ram_dq_core2padmisc, ram_dq_padmisc2core;
wire    [31:0]  ram_dq_pad2padmisc;
wire	[31:0]	ram_dq_padmisc2pad;
wire	[31:0]	ram_dq_oe_padmisc2pad_;
wire    [3:0]   ram_dqm_core2padmisc;
wire	[3:0]	ram_dqm_padmisc2pad,	ram_dqm_oe_padmisc2pad_;
wire    [1:0]   ram_cs_core2padmisc;
wire			ram_cs_padmisc2pad_, ram_cs_pad2padmisc_;//bit width change to 1 JFR030704
//wire	[1:0]	ram_cs_oe_padmisc2pad_,	ram_cs_oe_pad2padmisc_;//bit width change to 1 JFR030704
wire			ram_cs_oe_padmisc2pad_;	//ram_cs_oe_pad2padmisc_;//bit width change to 1 JFR030704

wire	[31:0]	ram_dq_oe_core2padmisc_;

wire    [31:0]  pci_ad_core2padmisc, pci_ad_padmisc2core;
wire    [3:0]   pci_cbe_core2padmisc_, pci_cbe_padmisc2core_;

//wire	[31:29]	pci_ad_pad2padmisc;
//wire	[31:29]	pci_ad_padmisc2pad;
//wire	[31:29]	pci_ad_oe_padmisc2pad_;

//wire	[3:0]	pci_cbe_pad2padmisc_;
//wire	[3:0]	pci_cbe_padmisc2pad_;
//wire	[3:0]	pci_cbe_oe_padmisc2pad_;

wire    [20:0]  rom_addr_core2padmisc;
wire    [7:0]   rom_rdata_padmisc2core,   rom_wdata_core2padmisc    ;
wire	[7:0]	rom_data_pad2padmisc;
wire	[7:0]	rom_data_padmisc2pad;
wire	[7:0]	rom_data_oe_padmisc2pad_;
wire	[20:0]	rom_addr_pad2padmisc;
wire	[20:0]	rom_addr_padmisc2pad;
wire	[20:0]	rom_addr_oe_padmisc2pad_;


//wire  [7:0]   ledpin;
wire    [1:0]   pci_gnt_core2padmisc_,   pci_req_padmisc2core_     ;
//wire    [1:0]   ext_int_in;				JFR030927
//wire    [31:0]  gpio_out,       gpio_oe_;	JFR030927
//wire    [31:0]  gpio_in;					JFR030927
wire	[28:0]	gpio_padmisc2pad;
wire	[28:0]	gpio_pad2padmisc;
wire	[28:0]	gpio_oe_padmisc2pad_;
wire	[31:0]	gpio_core2padmisc;
wire	[31:0]	gpio_oe_core2padmisc_;
wire	[31:0]	gpio_padmisc2core;

wire	[24:23]	iadc_smtsen_padmisc2core;		// ADC SRAM Test Enable
wire	[2:0]	iadc_smtsmode_padmisc2core;		// ADC SRAM Test Mode
wire	[24:23]	iadc_smts_err_core2padmisc;		// ADC SRAM Test Error Flag
wire	[24:23]	iadc_smts_end_core2padmisc;		// ADC SRAM Self Test End
wire	[3:0]	iadc_tst_mod_padmisc2core;		// ADC Internal SDM Test Selection

wire    [31:0]  pad_boot_config;
//MIPS Interface
wire    [31:0]  sysad_core2padmisc, sysad_padmisc2core;
wire    [4:0]   syscmd_core2padmisc, syscmd_padmisc2core;
wire    [4:0]   sys_int_padmisc2core_;
wire            sys_nmi_padmisc2core_;
wire            sysout_oe_core2padmisc;
wire            pvalid_core2padmisc_;
wire            preq_core2padmisc_;
wire            pmaster_core2padmisc_;
//wire            j_tdo_core2padmisc;
wire            eok_padmisc2core_;
wire            evalid_padmisc2core_;
wire            ereq_padmisc2core_;
wire            j_tdi_pad2padmisc, j_tms_pad2padmisc;// j_tclk, j_rst_;

wire			cpu_bist_mode_out2core;
wire			cpu_bist_finish_core2padmisc;
wire	[5:0]	cpu_bist_error_vec_core2padmisc;//JFR030806

wire    [5:0]   cpu_clk_pll_m;
wire    [4:0]   mem_dly_sel;
wire	[4:0]	mem_rd_dly_sel;
wire	[5:0]	test_dly_chain_sel;		// TEST_DLY_CHAIN delay select
wire			test_dly_chain_flag;	// TEST_DLY_CHAIN flag output

wire    [1:0]   pci_mips_fs;
wire	[5:0]	mem_clk_pll_m;


wire    [3:0]   dsp_fs_core2gen;
wire	[1:0]	ve_fs_core2gen;
wire    [1:0]   pci_clk_out_sel;
wire	[2:0]	work_mode_value_core2str;
wire    [1:0]   pci_fs_value_core2str;
wire	[1:0]	pci_clk_out_sel_core2gen;
wire	[11:8]	tv_enc_clk_sel_core2gen;
wire    [5:0]   mem_clk_pll_m_value_core2str;
wire    [5:0]   cpu_clk_pll_m_value_core2str;

wire    [7:0]   vdata_core2padmisc;
wire	[7:0]	vdata_out2pad,
				vdata_oe_out2pad_,
				vdata_frompad;

wire	[1:0]	i2s_mclk_sel_core2gen,
				spdif_clk_sel_core2gen;
wire	[11:0]	tvenc_vdi4vdac_padmisc2core;
wire	[11:0]	tvenc_vdo4vdac_core2padmisc;

wire    [11:0]  ram_addr_pad2padmisc;
wire    [1:0]   ram_ba_pad2padmisc;
wire    [3:0]   ram_dqm_pad2padmisc;//JFR030704

wire    [15:0]  atadd_core2padmisc, atadd_padmisc2core,
                atadd_padmisc2pad, atadd_oe_padmisc2pad_,
                atadd_pad2padmisc;
wire	[3:0]	atadd_oe_core2padmisc_;
wire    [2:0]   atada_core2padmisc,
                atada_padmisc2pad, atada_oe_padmisc2pad_,
                atada_pad2padmisc;

//	SERVO signals
//wire	[23:0]	sv_pmata_somd_padmisc2core;
//wire	[2:0]	sv_tst_svoio_i_padmisc2core;
//wire	[2:0]	sv_svo_svoio_o_core2padmisc	 ;
//wire	[2:0]	sv_svo_svoio_oe_core2padmisc_;
wire	[5:0]	sv_tst_addata_padmisc2core;
wire	[44:0]	sv_tst_pin_core2padmisc;
wire	[1:0]	sv_pmsvd_c2ftstin_padmisc2core;
wire	[23:0]	sv_sycg_somd_padmisc2core;
wire	[15:0]	sv_sv_hi_hd_o_core2padmisc;
wire	[2:0]	sv_pmata_ha_padmisc2core;
wire	[15:0]	sv_pmata_hd_padmisc2core;
//wire	[1:0]	sv_sv_svo_sflag_core2padmisc;
wire	[12:0]	sv_sv_svo_soma_core2padmisc;
//wire	[23:0]	sv_sv_svo_somd_core2padmisc;
wire	[3:0]	sv_sv_reg_gpioten;
//wire	[1:0]	sv_pata_sflag_core2padmisc		;
//wire	[1:0]	sv_pmata_sflag_padmisc2core	    ;
//wire	[1:0]	sv_pmata_sflag_oe_core2padmisc_ ;
wire	[1:0]	sv_xsflag_padmisc2core;
wire	[1:0]	sv_xsflag_core2padmisc;
wire	[1:0]	sv_xsflag_oe_core2padmisc_;

wire	[1:0]	sv_xsflag_padmisc2pad		;
wire	[1:0]	sv_xsflag_oe_padmisc2pad_	;
wire	[1:0]	sv_xsflag_pad2padmisc		;

wire	[2:0]	sv_gpio_core2padmisc;
wire	[2:0]	sv_gpio_oe_core2padmisc_;
wire	[2:0]	sv_gpio_padmisc2core;

wire	[15:0]	sv_atadd_core2padmisc;
wire			sv_atadd_oe_core2padmisc_1bit_;		// servo atadd oe signal
wire	[15:0]	sv_atadd_oe_core2padmisc_		= {16{sv_atadd_oe_core2padmisc_1bit_}};

wire	[15:0]	sv_atadd_padmisc2core;
wire	[2:0]	sv_atada_padmisc2core;

wire	[36:0]	sram_ifx_core2padmisc;
wire	[36:0]	sram_ifx_oe_core2padmisc_;		// could be replaced
assign			sram_ifx_oe_core2padmisc_[36:24]	=	13'h0;		// could be replaced
wire	[36:0]	sram_ifx_padmisc2core;

//	------------------ Servo External uP interface -------------------
wire	[22:0]	ext_up_padmisc2core;
wire	[22:0]	ext_up_core2padmisc;
wire	[22:0]	ext_up_oe_core2padmisc_;
wire			sv_mpg_pcs1j		;
wire	[7:0]	sv_mpg_pdatao		;
wire			sv_mpg_prdj			;
wire			sv_mpg_pwrj			;
wire	[9:0]	sv_mpg_paddr		;
wire			sv_sv_pwaitj		;
wire	[7:0]	sv_sv_pdatai		;
wire			sv_sv_mpi_intj		;

//	------------------ Servo External uP interface end-------------------

//wire	[3:0]	sv_cpu_gpioin;
//wire	[1:0]	sv_sycg_burnin;


//	TEST_MODE signals
wire	[2:0]	dsp_bist_vec_core2padmisc;
wire	[10:0]	video_bist_vec_core2padmisc_10_0;//video_bist_vec from core [10:0]JFR
wire	[11:0]	video_bist_vec_core2padmisc	=	{1'b0,video_bist_vec_core2padmisc_10_0};

wire	[112:0]	scan_dout;					// scan chain dout from core
wire	[112:0]	scan_din;					// scan chain din to core
wire	[59:0]	scan_dout_mux2padmisc;		// scan chain dout to pad_misc
wire	[59:0]	scan_din_padmisc2mux;		// scan chain din from pad_misc

wire	[7:0]	video_in_pixel_data_padmisc2core;

//=================invoke the core ===============================
`ifdef INC_CORE
core    CORE    (
//------chipset bus
        // SDRAM & FLASH
        .ram_addr               (ram_addr_core2padmisc          ),
        .ram_ba                 (ram_ba_core2padmisc            ),
        .ram_dq_out             (ram_dq_core2padmisc            ),
        .ram_dq_in              (ram_dq_padmisc2core   			),
        .ram_dq_oe_             (ram_dq_oe_core2padmisc_        ),
        .ram_cas_               (ram_cas_core2padmisc           ),
        .ram_we_                (ram_we_core2padmisc            ),
        .ram_ras_               (ram_ras_core2padmisc           ),
        .ram_cke                (ram_cke_out            		),//no use by now. yuky, 2002-10-09
        .ram_dqm                (ram_dqm_core2padmisc           ),
        .ram_cs_                (ram_cs_core2padmisc            ),
        // EEPROM
        .eeprom_addr            (rom_addr_core2padmisc       ),
        .eeprom_rdata           (rom_rdata_padmisc2core      ),
        .eeprom_ce_             (rom_ce_core2padmisc         ),
        .eeprom_oe_             (rom_oe_core2padmisc         ),
        .eeprom_we_             (rom_we_core2padmisc         ),
        .rom_data_oe_           (rom_data_oe_core2padmisc_      ),
        .rom_addr_oe            (rom_addr_oe_core2padmisc    ),
        .eeprom_wdata           (rom_wdata_core2padmisc      ),
        .rom_en_                (rom_en_core2padmisc_           ),
        // LED
//      .ledpin                 (ledpin                 ),
        // PCI
        .oe_ad_                 (pci_ad_oe_core2padmisc_        ),
        .out_ad                 (pci_ad_core2padmisc            ),
        .oe_cbe_                (pci_cbe_oe_core2padmisc_       ),
        .out_cbe_               (pci_cbe_core2padmisc_          ),
        .oe_frame_              (pci_frame_oe_core2padmisc_     ),
        .out_frame_             (pci_frame_core2padmisc_        ),
        .oe_irdy_               (pci_irdy_oe_core2padmisc_      ),
        .out_irdy_              (pci_irdy_core2padmisc_         ),
        .oe_devsel_             (pci_devsel_oe_core2padmisc_    ),
        .out_devsel_            (pci_devsel_core2padmisc_       ),
        .oe_trdy_               (pci_trdy_oe_core2padmisc_      ),
        .out_trdy_              (pci_trdy_core2padmisc_         ),
        .oe_stop_               (pci_stop_oe_core2padmisc_      ),
        .out_stop_              (pci_stop_core2padmisc_         ),
        .oe_par_                (pci_par_oe_core2padmisc_       ),
        .out_par                (pci_par_core2padmisc           ),
        .out_gnt_               (pci_gnt_core2padmisc_          ),
        .in_req_                (pci_req_padmisc2core_          ),
        .in_ad                  (pci_ad_padmisc2core            ),
        .in_cbe_                (pci_cbe_padmisc2core_          ),
        .in_frame_              (pci_frame_padmisc2core_        ),
        .in_irdy_               (pci_irdy_padmisc2core_         ),
        .in_devsel_             (pci_devsel_padmisc2core_       ),
        .in_trdy_               (pci_trdy_padmisc2core_         ),
        .in_stop_               (pci_stop_padmisc2core_         ),
        .in_par                 (pci_par_padmisc2core           ),
        .ext_int                (pci_inta_padmisc2core_         ),
        .gpio_out               (gpio_core2padmisc    			),
        .gpio_oe_               (gpio_oe_core2padmisc_          ),
        .gpio_in                (gpio_padmisc2core      		),
        // Audio

        .out_i2so_data0         (i2so_data0_core2padmisc        ),
		.out_i2so_data1			(i2so_data1_core2padmisc		),
		.out_i2so_data2			(i2so_data2_core2padmisc		),
		.out_i2so_data3			(i2so_data3_core2padmisc		),
	//	.ac97_i2s_sel			(ac97_i2s_sel					),	//no use by now, yuky, 2002-10-09
        .out_i2so_lrck          (i2so_lrck_core2padmisc         ),
        .out_i2so_bick          (i2so_bick_core2padmisc         ),
        .in_i2si_data           (i2si_data_padmisc2core         ),
        .in_i2so_mclk           (i2so_mclk_gen2core				),
        .iadc_mclk    			(iadc_mclk_gen2core				),
		.iadc_smtsen  			(iadc_smtsen_padmisc2core		),
		.iadc_smtsmode			(iadc_smtsmode_padmisc2core		),
		.iadc_smts_err			(iadc_smts_err_core2padmisc		),
		.iadc_smts_end			(iadc_smts_end_core2padmisc		),
		.iadc_adcclkp1			(iadc_clkp1_core2padmisc		),
		.iadc_adcclkp2			(iadc_clkp2_core2padmisc		),
		.iadc_tst_mod 			(iadc_tst_mod_padmisc2core		),

        //spdif
        .out_spdif              (spdif_core2padmisc             ),
        // South Bridge
        .ir_rx                  (irrx_padmisc2core  			),

        .scb_scl_out            (scbscl_core2padmisc    		),
        .scb_scl_oe             (scbscl_oe_core2padmisc 		),
        .scb_sda_out            (scbsda_core2padmisc    		),
        .scb_sda_oe             (scbsda_oe_core2padmisc 		),
        .scb_scl_in             (scbscl_padmisc2core    		),
        .scb_sda_in             (scbsda_padmisc2core    		),

//UAAT
		.uart_tx				(uart_tx_core2padmisc				),
		.uart_rx				(uart_rx_padmisc2core				),
        // USB
        .p1_ls_mode             (p1_ls_mode_core2padmisc        ),
        .p1_txd                 (p1_txd_core2padmisc            ),
        .p1_txd_oej             (p1_txd_oej_core2padmisc        ),
        .p1_txd_se0             (p1_txd_se0_core2padmisc        ),
        .p2_ls_mode             (p2_ls_mode_core2padmisc        ),
        .p2_txd                 (p2_txd_core2padmisc            ),
        .p2_txd_oej             (p2_txd_oej_core2padmisc        ),
        .p2_txd_se0             (p2_txd_se0_core2padmisc        ),
        .usb_pon_               (usbpon_core2padmisc_    		),	//Dont care the polarity (low or high)

        .i_p1_rxd               (i_p1_rxd_padmisc2core          ),
        .i_p1_rxd_se0           (i_p1_rxd_se0_padmisc2core      ),
        .i_p2_rxd               (i_p2_rxd_padmisc2core          ),
        .i_p2_rxd_se0           (i_p2_rxd_se0_padmisc2core      ),
        .usb_overcur_           (usbovc_padmisc2core   			),	//Dont care the polarity (low or high)

        .vdata_oe_              (vdata_oe_core2padmisc_         ),
        .vdata_out              (vdata_core2padmisc             ),
        .h_sync_out_            (de_h_sync_core2padmisc_   		),		// DE H_SYNC#
        .h_sync_oe_             (h_sync_oe_core2padmisc_        ),
        .in_h_sync_             (h_sync_padmisc2core_           ),
        .v_sync_out_            (de_v_sync_core2padmisc_		),		// DE_V_SYNC#
        .v_sync_oe_             (v_sync_oe_core2padmisc_        ),
        .in_v_sync_             (v_sync_padmisc2core_           ),

		// TV Encoder intf
		.tvenc_vdi4vdac			(tvenc_vdi4vdac_padmisc2core	),
		.tvenc_vdo4vdac			(tvenc_vdo4vdac_core2padmisc	),

		.tvenc_idump			(tvenc_idump_core2pad			),
		.tvenc_iext				(tvenc_iext_core2pad			),	// IEXT changed to output
		.tvenc_iext1			(tvenc_iext1_core2pad			),	// IEXT1 is output
		.tvenc_iout1			(tvenc_iout1_core2pad			),
		.tvenc_iout2			(tvenc_iout2_core2pad			),
		.tvenc_iout3			(tvenc_iout3_core2pad			),
		.tvenc_iout4			(tvenc_iout4_core2pad			),
		.tvenc_iout5			(tvenc_iout5_core2pad			),
		.tvenc_iout6			(tvenc_iout6_core2pad			),
		.tvenc_iref1			(tvenc_iref1_core2pad			),
		.tvenc_iref2			(tvenc_iref2_core2pad			),
		.tvenc_gnda				(gnda_tvdac						),	//TVENC GND JFR 031112
		.tvenc_gnda1			(gnda1_tvdac					),	// Ground for DAC, Norman 2003-12-05
		.tvenc_vsud				(vsud_tvdac						),	// Ground for DAC, Norman 2003-12-05
		.tvenc_vd33a			(vd33a_tvdac					),	//TVENC PWR JFR 031112

		.tvenc_h_sync_			(tvenc_h_sync_core2padmisc_		),	// TV Encoder H_SYNC#
		.tvenc_v_sync_      	(tvenc_v_sync_core2padmisc_     ),	// TV Encoder V_SYNC#
		.tvenc_vdactest			(tvenc_test_mode				),
//VIDEO IN
		.VIN_PIXEL_DATA	       	(video_in_pixel_data_padmisc2core	),
		.VIN_PIXEL_CLK	        (video_in_pixel_clk_padmisc2core	),
		.VIN_H_SYNCJ	        (video_in_hsync_padmisc2core_		),
		.VIN_V_SYNCJ	        (video_in_vsync_padmisc2core_		),

        // IDE Interface
        .atadiow_out_           (atadiow_core2padmisc_          ),
        .atadior_out_           (atadior_core2padmisc_          ),
        .atacs0_out_            (atacs0_core2padmisc_           ),
        .atacs1_out_            (atacs1_core2padmisc_           ),
        .atadd_out              (atadd_core2padmisc             ),
        .atadd_oe_              (atadd_oe_core2padmisc_ 		),
        .atada_out              (atada_core2padmisc             ),
        .atareset_out_          (atareset_core2padmisc_         ),
        .atareset_oe_			(atareset_oe_core2padmisc_		),
        .atadmack_out_          (atadmack_core2padmisc_         ),

        .ataiordy_in            (ataiordy_padmisc2core          ),
        .ataintrq_in            (ataintrq_padmisc2core          ),
        .atadd_in               (atadd_padmisc2core             ),
        .atadmarq_in            (atadmarq_padmisc2core          ),
// System Interrupt, output from Hostbridge
        .x_err_int              (x_err_int_core2padmisc         ),
        .x_ext_int              (x_ext_int_core2padmisc         ),

//CPU Interface
        // MIPS bus interface
        .sysad_out              (sysad_core2padmisc             ),
        .sysad_in               (sysad_padmisc2core             ),
        .syscmd_out             (syscmd_core2padmisc            ),
        .syscmd_in              (syscmd_padmisc2core            ),
        .sysout_oe              (sysout_oe_core2padmisc         ),
        .sys_int_               (sys_int_padmisc2core_          ),
        .sys_nmi_               (sys_nmi_padmisc2core_          ),
        .pvalid_                (pvalid_core2padmisc_           ),
        .preq_                  (preq_core2padmisc_             ),
        .pmaster_               (pmaster_core2padmisc_          ),
        .eok_                   (eok_padmisc2core_              ),
        .evalid_                (evalid_padmisc2core_           ),
        .ereq_                  (ereq_padmisc2core_             ),

        // JTAG interface
        .j_tdi                  (j_tdi_padmisc2core				),
        .j_tms                  (j_tms_padmisc2core				),
        .j_tclk                 (j_tclk_pad2padmisc       		),		// j_tclk from pad not from padmisc
        .j_rst_                 (j_trst_padmisc2core_      		),
        .j_tdo                  (j_tdo_core2padmisc             ),

		// CPU BIST intf
		.cpu_bist_mode			(cpu_bist_mode_padmisc2core		),
		.cpu_bist_error_vec		(cpu_bist_error_vec_core2padmisc),
		.cpu_bist_finish		(cpu_bist_finish_core2padmisc	),

// Clock signals
        .sb_clk                 (sb_clk_gen2core        		),
        .usb_clk                (usb_clk_gen2core       		),
        .cpu_clk                (cpu_clk_gen2core       		), // pipeline clock   (CPU core)
        .mem_clk                (inter_mem_clk_gen      		), // transfer clock   (Memory Controller, IO, ROM)
        .pci_clk                (pci_clk_gen2core       		),
        .cpu_clk_src            (cpu_clk_gen2core   			), // same as the pipeline clock (did not mask by JTAG mode
        .dsp_clk                (dsp_clk_gen2core       		),
        .spdif_clk              (spdif_clk_gen2core     		),
        .mbus_clk               (mbus_clk_gen2core      		),
		.ata_clk				(ata_clk_gen2core				),
		.servo_clk				(servo_clk_gen2core				),
        .pad_boot_config        (pad_boot_config        		),

		.inter_only				(inter_only_core2gen			),		// Interlace output
		.dvi_rwbuf_sel			(dvi_rwbuf_sel_core2gen			),		// DVI buffer read/write

		.cvbs2x_clk				(cvbs2x_clk_gen2core			),		// 54MHZ clock
		.cvbs_clk				(cvbs_clk_gen2core				),		// 27MHz clock
		.cav_clk				(cav_clk_gen2core				),		// 54 or 27 MHZ clock
		.dvi_buf0_clk			(dvi_buf0_clk_gen2core			),		// CAV_CLK or VIDEO_CLK for buffer 0
		.dvi_buf1_clk			(dvi_buf1_clk_gen2core			),		// CAV_CLK or VIDEO_CLK for buffer 1
		.video_clk				(video_clk_gen2core				),		// VIDEO clock output

		.tv_f108m_clk			(tv_f108m_clk_gen2core			),		// tv_encoder 108MHz clock
		.tv_cvbs2x_clk			(tv_cvbs2x_clk_gen2core			),		// tv_encoder 54MHZ clock
		.tv_cvbs_clk			(tv_cvbs_clk_gen2core			),		// tv_encoder 27MHz clock
		.tv_cav2x_clk			(tv_cav2x_clk_gen2core			),		// tv_encoder 108 or 54 MHZ clock
		.tv_cav_clk				(tv_cav_clk_gen2core			),		// tv_encoder 54 or 27 MHZ clock

//	PLL control
		.cpu_clk_pll_m_value	(cpu_clk_pll_m_value_core2str	),	// new cpu_clk_pll m parameter
		.cpu_clk_pll_m_tri		(cpu_clk_pll_m_tri_core2str		),  // cpu_clk_pll m modification trigger
		.cpu_clk_pll_m_ack		(cpu_clk_pll_m_ack_str2core		),  // cpu_clk_pll m modification ack

		.mem_clk_pll_m_value	(mem_clk_pll_m_value_core2str	),  // new mem_clk_pll m parameter
		.mem_clk_pll_m_tri		(mem_clk_pll_m_tri_core2str		),  // mem_clk_pll m modification trigger
		.mem_clk_pll_m_ack		(mem_clk_pll_m_ack_str2core		),  // mem_clk_pll m modification ack

		.pci_fs_value			(pci_fs_value_core2str			),	// new pci frequency select parameter
		.pci_fs_tri				(pci_fs_tri_core2str			),  // pci frequency select modification trigger
		.pci_fs_ack				(pci_fs_ack_str2core			),  // pci frequency select modification ack

		.work_mode_value		(work_mode_value_core2str		),	// new work_mode
		.work_mode_tri  		(work_mode_tri_core2str			),  // work_mode modification trigger
		.work_mode_ack  		(work_mode_ack_str2core			),  // work_mode modification ack

//	clock frequency select, clock misc control
		.dsp_fs					(dsp_fs_core2gen				),	// dsp clock frequency select
		.dsp_div_src_sel		(dsp_div_src_sel_core2gen		),	// dsp source clock select
		.ve_fs					(ve_fs_core2gen					),	// ve clock frequency select
		.pci_clk_out_sel		(pci_clk_out_sel_core2gen		),	//
		.svga_mode_en			(svga_mode_en_core2gen			),	// SVGA mode output enable
		.tv_enc_clk_sel			(tv_enc_clk_sel_core2gen		),	// TV encoder clock setting

        .mem_dly_sel            (mem_dly_sel            		),
        .mem_rd_dly_sel			(mem_rd_dly_sel					),
		.test_dly_chain_sel		(test_dly_chain_sel				),	// TEST_DLY_CHAIN delay select
		.test_dly_chain_flag	(test_dly_chain_flag			),	// TEST_DLY_CHAIN flag output

//	external interface control
		.audio_pll_fout_sel		(audio_pll_fout_sel_core2gen	),	// Audio PLL FOUT frequency select
		.i2s_mclk_sel			(i2s_mclk_sel_core2gen			),	// I2S MCLK source select
		.i2s_mclk_out_en_		(i2so_mclk_oe_core2padmisc_		),	// I2S MCLK output enable
		.spdif_clk_sel			(spdif_clk_sel_core2gen			),	// S/PDIF clock select
		.hv_sync_en				(hv_sync_en_core2padmisc		),	// h_sync_, v_sync_ enable on the xsflag[1:0] pins
		.i2si_data_en			(i2si_data_en_core2padmisc		),	// I2S input data enable on the gpio[7] pin
		.scb_en					(scb_en_core2padmisc			),	// SCB interface enable on the gpio[6:5] pins
		.servo_gpio_en			(sv_gpio_en_core2padmisc		),	// servo_gpio[2:0] enable on the gpio[2:0] pins
		.ext_ide_device_en		(ext_ide_device_en_core2padmisc	),	// external IDE device enable
		.pci_gntb_en			(pci_gntb_en_core2padmisc		),	// PCI GNTB# enable
		.vdata_en				(vdata_en_core2padmisc			),	// video data enable
		.sync_source_sel		(sync_source_sel_core2padmisc	),	// sync signal source select, 0 -> TVENC, 1 -> DE.
		.video_in_intf_en		(video_in_intf_en_core2padmisc	),	// video in interface enable
		.pci_mode_ide_en		(pci_mode_ide_en_core2padmisc	),	// pci_mode ide interface enable
		.ide_active				(ide_active_core2padmisc		),	// ide interface is active in the pci mode

		//SERVO INTERFACE
//		dmckg_mpgclk		,		// MEM_CLK
//		sv_top_clkdiv		,
		.sv_pmata_somd			(sram_ifx_padmisc2core[23:0]		), // ok
		.sv_pmsvd_trcclk		(sv_pmsvd_trcclk_padmisc2core		),	// ok
		.sv_pmsvd_trfclk		(sv_pmsvd_trfclk_padmisc2core		),	// ok
		.sv_sv_tslrf			(sv_sv_tslrf_core2padmisc			),	// ok
		.sv_sv_tplck			(sv_sv_tplck_core2padmisc			),	// ok
//		.sv_sv_dev_true  		(sv_sv_dev_true_core2padmisc     	), // hardwire 1 in sv_servo
		.sv_tst_svoio_i			(sv_gpio_padmisc2core				), // ok
		.sv_svo_svoio_o			(sv_gpio_core2padmisc				), // ok
		.sv_svo_svoio_oej		(sv_gpio_oe_core2padmisc_			), // ok
		.sv_tst_addata			(sv_tst_addata_padmisc2core			),	// ok
		.sv_reg_rctst_2  		(sv_reg_rctst_2_core2padmisc       	),	// ok
//		sv_top_sv_mppdn		,
//		.sv_dmckg_sysclk 		(sv_dmckg_sysclk					),	//Need modify
		.sv_mckg_dspclk  		(ata_clk_gen2core					),	// use IDE 54MHz clock
//		.sv_sycg_svoata 		(sv_sycg_svoata_padmisc2core 		),	// Useless
//		sv_sv_reg_c2ftst       ,
//		sv_pmdm_ideslave      ,
		.sv_pmsvd_tplck      	(sv_pmsvd_tplck_padmisc2core        ),	// Ok
		.sv_pmsvd_tslrf      	(sv_pmsvd_tslrf_padmisc2core        ),	// Ok
		.sv_sv_tst_pin			(sv_tst_pin_core2padmisc			),	// Ok
//		.sv_sv_reg_tstoen    	(sv_reg_tstoen       				), // Dangling
		.sv_pmsvd_c2ftstin		(sv_pmsvd_c2ftstin_padmisc2core		),	// Ok
		.sv_sycg_somd			(sram_ifx_padmisc2core[23:0]		),	// Ok
		// Servo analog
//		.sv_sycg_updbgout 		(sv_sycg_updbgout					), // NB controller output
//		.sv_cpu_gpioin			(sv_cpu_gpioin						), // Dangling
//		.sv_sycg_burnin			(sv_sycg_burnin						), //Need modify [1:0]
		//	External uP interface
		.sv_ex_up_in			(ext_up_padmisc2core				),		// Ok
		.sv_ex_up_out			(ext_up_core2padmisc				),		// Ok
		.sv_ex_up_oe_			(ext_up_oe_core2padmisc_			),		// Ok
		// Servo Atapi inf
		.sv_sv_hi_hdrq_o     	(sv_atadmarq_core2padmisc			),		// Ok
		.sv_sv_hi_hdrq_oej   	(sv_sv_hi_hdrq_oe_core2padmisc_     ),		// Useless
		.sv_sv_hi_hcs16j_o   	(sv_ataiocs16_core2padmisc_			),		// Ok
		.sv_sv_hi_hcs16j_oej 	(sv_ataiocs16_oe_core2padmisc_		),		// Ok
		.sv_sv_hi_hint_o     	(sv_ataintrq_core2padmisc			),		// Ok
		.sv_sv_hi_hint_oej   	(sv_sv_hi_hint_oe_core2padmisc_     ),		// Useless
		.sv_sv_hi_hpdiagj_o  	(sv_atapdiag_core2padmisc_			),		// Ok
		.sv_sv_hi_hpdiagj_oej	(sv_atapdiag_oe_core2padmisc_		),		// Ok
		.sv_sv_hi_hdaspj_o   	(sv_atadasp_core2padmisc_			),		// Ok
		.sv_sv_hi_hdaspj_oej 	(sv_atadasp_oe_core2padmisc_		),		// Ok
		.sv_sv_hi_hiordy_o   	(sv_ataiordy_core2padmisc			),		// Ok
		.sv_sv_hi_hiordy_oej 	(sv_sv_hi_hiordy_oe_core2padmisc_   ),		// Useless
		.sv_sv_hi_hd_o			(sv_atadd_core2padmisc				),		// Ok
		.sv_sv_hi_hd_oej     	(sv_atadd_oe_core2padmisc_1bit_     ),		// Ok
		.sv_pmata_hrstj      	(sv_atareset_padmisc2core_			),		// Ok
		.sv_pmata_hcs1j      	(sv_atacs0_padmisc2core_        	),		// Ok
		.sv_pmata_hcs3j      	(sv_atacs1_padmisc2core_	        ),		// Ok
		.sv_pmata_hiorj      	(sv_atadior_padmisc2core_			),		// Ok
		.sv_pmata_hiowj      	(sv_atadiow_padmisc2core_			),		// Ok
		.sv_pmata_hdackj     	(sv_atadmack_padmisc2core_			),		// Ok
		.sv_pmata_hpdiagj    	(sv_atapdiag_padmisc2core_			),		// Ok
		.sv_pmata_hdaspj     	(sv_atadasp_padmisc2core_			),		// Ok
		.sv_pmata_ha			(sv_atada_padmisc2core				),		// Ok
		.sv_pmata_hd			(sv_atadd_padmisc2core				),		// Ok
		// Servo Test
//		.sv_sv_svo_sflag		(sv_sv_svo_sflag_core2padmisc		),
		.sv_sv_svo_sflag		(sv_xsflag_core2padmisc[1:0]		), // to PAD_MISC, output only
		.sv_sv_svo_soma			(sram_ifx_core2padmisc[36:24]		), // ok
		.sv_sv_svo_somd			(sram_ifx_core2padmisc[23:0]		), // ok
		.sv_sv_svo_somd_oej  	(sram_ifx_oe_core2padmisc_[21:0]    ), // ok
		.sv_sv_svo_somdh_oej 	(sram_ifx_oe_core2padmisc_[23:22]   ), // ok
//		.sv_sv_reg_dvdram_toe	(sv_sv_reg_dvdram_toe   			), // Dangling
//		.sv_sv_reg_dfctsel   	(sv_sv_reg_dfctsel      			), // Dangling
//		.sv_sv_reg_trfsel 		(sv_sv_reg_trfsel 					), // Dangling
//		.sv_sv_reg_flgtoen 		(sv_sv_reg_flgtoen 					), // Dandling
//		.sv_sv_reg_gpioten		(sv_sv_reg_gpioten					), // Dandling
//		.sv_sv_reg_13e_7		(sv_sv_reg_13e_7					), // Dandling
//		.sv_sv_reg_13e_6		(sv_sv_reg_13e_6					), // Dandling
//		.sv_reg_trg_plcken		(									),
//		.sv_reg_sv_tplck_oej	(									), // useless in 3357
		//	Servo analog ANA_DTSV
		.sv_pad_clkref			(f27m_clk			  ),				// 27 MHz from the crystall
		.sv_xadcin       		(XADCIN       	      ),
		.sv_xadcip       		(XADCIP       	      ),
		.sv_xatton       		(XATTON       	      ),
		.sv_xattop       		(XATTOP       	      ),
		.sv_xbiasr       		(XBIASR       	      ),
		.sv_xcdld        		(XCDLD        	      ),
		.sv_xcdpd        		(XCDPD        	      ),
		.sv_xcdrf        		(XCDRF        	      ),
		.sv_xcd_a        		(XCD_A        	      ),
		.sv_xcd_b        		(XCD_B        	      ),
		.sv_xcd_c        		(XCD_C        	      ),
		.sv_xcd_d        		(XCD_D        	      ),
		.sv_xcd_e        		(XCD_E        	      ),
		.sv_xcd_f        		(XCD_F        	      ),
		.sv_xcelpfo      		(XCELPFO      	      ),
		.sv_xdpd_a       		(XDPD_A       	      ),
		.sv_xdpd_b       		(XDPD_B       	      ),
		.sv_xdpd_c       		(XDPD_C       	      ),
		.sv_xdpd_d       		(XDPD_D       	      ),
		.sv_xdvdld       		(XDVDLD       	      ),
		.sv_xdvdpd       		(XDVDPD       	      ),
		.sv_xdvdrfn      		(XDVDRFN      	      ),
		.sv_xdvdrfp      		(XDVDRFP      	      ),
		.sv_xdvd_a       		(XDVD_A       	      ),
		.sv_xdvd_b       		(XDVD_B       	      ),
		.sv_xdvd_c       		(XDVD_C       	      ),
		.sv_xdvd_d       		(XDVD_D       	      ),
		.sv_xfelpfo      		(XFELPFO      	      ),
		.sv_xfocus       		(XFOCUS       	      ),
		.sv_xgmbiasr     		(XGMBIASR     	      ),
		.sv_xlpfon       		(XLPFON       	      ),
		.sv_xlpfop       		(XLPFOP       	      ),
		.sv_xpdaux1      		(XPDAUX1      	      ),
		.sv_xpdaux2      		(XPDAUX2      	      ),
		.sv_xsblpfo      		(XSBLPFO      	      ),
		.sv_xsfgn        		(XSFGN        	      ),
		.sv_xsfgp        		(XSFGP        	      ),

		.sv_pata_sflag			(sv_xsflag_pad2padmisc[1:0]),	// signal from PAD to padmisc
		.sv_pmata_sflag			(sv_xsflag_padmisc2pad[1:0]),	// ok	carcy
		.sv_pmata_sflag_oej		(sv_xsflag_oe_padmisc2pad_[1:0]),	// ok	carcy

		.sv_xsflag				(XSFLAG				),	//	carcy add 2003/11/17 18:52 				),	// need check
		.sv_xslegn       		(XSLEGN             ),
		.sv_xslegp       		(XSLEGP             ),
		.sv_xspindle     		(XSPINDLE           ),
		.sv_xtelp        		(XTELP              ),
		.sv_xtelpfo      		(XTELPFO            ),
		.sv_xtestda      		(XTESTDA            ),
		.sv_xtexo        		(XTEXO              ),
		.sv_xtrack       		(XTRACK             ),
		.sv_xtray        		(XTRAY              ),
		.sv_xvgain       		(XVGAIN             ),
		.sv_xvgaip       		(XVGAIP             ),
		.sv_xvref15      		(XVREF15            ),
		.sv_xvref21      		(XVREF21            ),
		//Servo power
		.sv_vd33d        		(vddio            	),	// Use the IO power
		.sv_vd18d        		(vddcore           	),	// Use the core power
		.sv_vdd3mix1			(VDD3MIX1			),
		.sv_vdd3mix2			(VDD3MIX2			),
		.sv_avdd_ad      		(AVDD_AD         	),
		.sv_avdd_att     		(AVDD_ATT         	),
		.sv_avdd_da      		(AVDD_DA          	),
		.sv_avdd_dpd     		(AVDD_DPD        	),
		.sv_avdd_lpf     		(AVDD_LPF        	),
		.sv_avdd_rad     		(AVDD_RAD        	),
		.sv_avdd_ref     		(AVDD_REF        	),
		.sv_avdd_svo     		(AVDD_SVO        	),
		.sv_avdd_vga     		(AVDD_VGA         	),
		.sv_avss_ad      		(AVSS_AD         	),
		.sv_avss_att     		(AVSS_ATT         	),
		.sv_avss_da      		(AVSS_DA         	),
		.sv_avss_dpd     		(AVSS_DPD         	),
		.sv_avss_ga      		(AVSS_GA         	),
		.sv_avss_gd      		(AVSS_GD        	),
		.sv_avss_lpf     		(AVSS_LPF         	),
		.sv_avss_rad     		(AVSS_RAD         	),
		.sv_avss_ref     		(AVSS_REF         	),
		.sv_avss_svo     		(AVSS_SVO         	),
		.sv_avss_vga     		(AVSS_VGA         	),
		.sv_azec_gnd     		(gnd	         	),	//	digital ground
		.sv_azec_vdd     		(vddcore         	),	//	vdd core 1.8v
		.sv_dv18_rcka    		(DV18_RCKA        	),
		.sv_dv18_rckd    		(DV18_RCKD        	),
		.sv_dvdd         		(vddcore		  	),	//	vdd core 1.8v
		.sv_dvss         		(gnd             	),	//	digital ground
		.sv_dvss_rcka    		(DVSS_RCKA        	),
		.sv_dvss_rckd    		(DVSS_RCKD        	),
		.sv_fad_gnd      		(gnd	          	),	//	digital ground
		.sv_fad_vdd      		(vddcore         	),	//  vdd core 1.8v
		.sv_gndd         		(gnd             	),	//	digital ground

//	SERVO control
		.sv_tst_pin_en			(sv_tst_pin_en_core2padmisc		),	// SERVO test pin enable
		.sv_ide_mode_part0_en	(sv_ide_mode_part0_en_core2padmisc),// ide mode SERVO test pin part 0 enable
		.sv_ide_mode_part1_en	(sv_ide_mode_part1_en_core2padmisc),// ide mode SERVO test pin part 1 enable
		.sv_ide_mode_part2_en	(sv_ide_mode_part2_en_core2padmisc),// ide mode SERVO test pin part 2 enable
		.sv_ide_mode_part3_en	(sv_ide_mode_part3_en_core2padmisc),// ide mode SERVO test pin part 3 enable
		.sv_c2ftstin_en			(sv_c2ftstin_en_core2padmisc	),	// SERVO c2 flag enable
		.sv_trg_plcken			(sv_trg_plcken_core2padmisc		),	// SERVO tslrf and tplck enable
		.sv_pmsvd_trcclk_trfclk_en (sv_pmsvd_trcclk_trfclk_en_core2padmisc),	// SERVO pmsvd_trcclk, pmsvd_trfclk enable

		.iadc_v1p2u 			(xadc_vref_core2pad	),
		.iadc_vs                (gnda_adc			),
		.iadc_vd                (vd33a_adc			),
		.iadc_vin_l             (xmic1in_pad2core	),
		.iadc_vin_r             (xmic2in_pad2core	),


        // working mode select
        .pci_mode               (pci_mode               		),
        .ide_mode            	(ide_mode            			),
        .tvenc_test_mode        (tvenc_test_mode        		),
        .servo_only_mode		(servo_only_mode				),
        .servo_test_mode        (servo_test_mode        		),
        .servo_sram_mode		(servo_sram_mode				),
        .test_mode              (scan_test_mode_padmisc2core	),
        .bist_test_mode			(bist_test_mode_padmisc2core	),

        .dsp_bist_mode			(dsp_bist_tri_padmisc2core		),

        .dsp_bist_finish		(dsp_bist_finish_core2padmisc	),
        .dsp_bist_err_vec		(dsp_bist_vec_core2padmisc		),

		.video_bist_tri			(video_bist_tri_padmisc2core	),
		.video_bist_finish		(video_bist_finish_core2padmisc	),
		.video_bist_vec			(video_bist_vec_core2padmisc_10_0),//[10:0]JFR031106


        //to cpu
        .pci_mips_fs            (pci_mips_fs            		),
        .cpu_endian_mode        (cpu_endian_mode        		),
        .core_warmrst_          (core_warmrst_          		),
        .core_coldrst_          (core_coldrst_          		),
		.cpu_probe_en			(cpu_probe_en					),
	//DFT test port
`ifdef  SCAN_EN
	.DFT_TEST_SE		(scan_en_padmisc2core),
	.DFT_TEST_SI		({scan_din[99:0]}),     // Reserve {scan chain [112:100]}
	.DFT_TEST_SO		({scan_dout[99:0]}),
`endif
        //connect to chipset
        .sw_rst_pulse           (sw_rst_pulse       ),
        .chipset_rst_           (chipset_rst_   	)
        );

`else
//include core_bh, delete by yuky, 2002-10-09
`endif
//========================End of core invoking==========================

//======================== Clock Generater =============================

wire    [31:0]  strap_pin_in;

F27_PLL F27_PLL(
                .FIN            (f27m_clk       	),      // 27MHz input
                .FOUT_48		(f48m_clk_pll2gen	),		// 48MHz output
                .FOUT_54        (f54m_clk_pll2gen   ),		// 54MHz output only for IDE
                .FOUT_80		(f80m_clk_pll2gen	),		// 80MHz output
                .FOUT_108		(f108m_clk_pll2gen	),		// 108MHz output
                .PD				(1'b0				),
                .TFCK			(f27_pll_fck		),
                .TLOCK			(f27_pll_lock		),
                .LOCKEN			(1'b0				),
                .PLLAVSS		(f27_pllvss			),
                .PLLAVDD		(f27_pllvdd			),
				.PLLDVDD		(					),
				.PLLDVSS		(					)
                );

CPU_PLL CPU_CLK_PLL(
                .FIN            (f27m_clk   		),	// 27 input
                .PLL_M      	(cpu_clk_pll_m      ),	//cpu_pll_m,from strap pins
                .PD				(1'b0				),
                .FOUT           (fout_pll2gen       ),
                .TFCK			(cpu_clk_pll_fck	),
                .TLOCK			(cpu_clk_pll_lock	),
                .LOCKEN			(gpio_core2padmisc[30]	),
                .PLLAVSS		(cpu_pllvss			),
                .PLLAVDD		(cpu_pllvdd			),
				.PLLDVDD		(					),
				.PLLDVSS		(					)
                );

CPU_PLL	MEM_CLK_PLL(
                .FIN            (f27m_clk   		),	// 27 input
                .PLL_M      	(mem_clk_pll_m		),	//cpu_pll_m,from strap pins
                .PD				(1'b0				),
                .FOUT           (mem_clk_fout2gen	),
                .TFCK			(mem_clk_pll_fck	),
                .TLOCK			(mem_clk_pll_lock	),
                .LOCKEN			(gpio_core2padmisc[29]),
                .PLLAVSS		(cpu_pllvss			),
                .PLLAVDD		(cpu_pllvdd			),
				.PLLDVDD		(					),
				.PLLDVSS		(					)
                );

AUDIO_PLL AUDIO_PLL	(
				.FIN			(f27m_clk  			),
				.LOCKEN			(1'b0	 			),
				.PD				(1'b0     			),
				.FOUT_SEL		(audio_pll_fout_sel_core2gen	),

				.FOUT			(audio_clk_pll2gen	),
				.TFCK			(audio_pll_fck		),
				.TLOCK      	(audio_pll_lock		),
				.PLLAVSS		(f27_pllvss			),
				.PLLAVDD		(f27_pllvdd			),
				.PLLDVDD		(					),
				.PLLDVSS		(					)
				);

CLOCK_GEN CLOCK_GEN(
// input
				.SRC_CLK		(f27m_clk				),	// from clock input pad
                .F48M           (f48m_clk_pll2gen		),  // from F27_PLL 48M output
                .F54M           (f54m_clk_pll2gen		),	// from F27_PLL output
                .PCI_MIPS_FS    (pci_mips_fs			),  // select the divisor of PCI/MIPS bus clock,from strap pins
                .DSP_FS         (dsp_fs_core2gen		),  // DSP frequency select
				.DSP_DIV_SRC_SEL(dsp_div_src_sel_core2gen),	// DSP source clock select
                .VE_FS          (ve_fs_core2gen			),  // VIDEO ENGINE frequency select
                .J_TCLK         (j_tclk_pad2padmisc		),  // JTAG test mode clock
                .TEST_EN        (scan_test_mode_padmisc2core),  // Test Mode Enable
                .CPU_MODE       (1'b0					),  // CPU standalone mode
                .MEM_DLY_SEL    (mem_dly_sel			),  // SDRAM clock pad delay chain control
                .PLL_FVCO       (fout_pll2gen			), 	// Clock output from PLL
                .MEM_CLK_FOUT	(mem_clk_fout2gen		),	// memory clock output from CPU_PLL
                .BYPASS_PLL     (pll_bypass				),  // BY PASS CPU_PLL
                .PCI_MBUS_SEL   (pci_clk_out_sel_core2gen),	// PCI_MBUS clock output select
                .TEST_MODE      (scan_test_mode_padmisc2core),	// CHIP TEST MODE
                .PSCAN_EN		(1'b0					),	// Progressive scan enable

// output
                .USB_CLK        (usb_clk_gen2core		),  // USB clock 48M
                .SB_CLK         (sb_clk_gen2core		),  // SouthBridge Clock 12M
                .CPU_CLK        (cpu_clk_gen2core		),  // CPU clock (with JTAG test mode mask)
                .MBUS_CLK       (mbus_clk_gen2core		),  // MIPS clock
                .MEM_CLK        (inter_mem_clk_gen		),  // internal SDRAM, NorthBridge clock
                .DSP_CLK        (dsp_clk_gen2core		),  // DSP clock
                .VE_CLK         (ve_clk_gen2gen			),  // VIDEO ENGINE CLOCK to VIDEO_CLOCK_GEN
                .F54M_CLK		(ata_clk_gen2core		),	// 54 MHz clock for IDE
                .PCI_CLK        (pci_clk_gen2core		),  // internal PCI bus clock
                .OUT_MEM_CLK    (ext_mem_clk_gen		),  // output to SDRAM clock pad
                .PCI_MBUS_CLK   (pci_mbus_clk_gen		),	// output to PCI/MIPS clock pad
				.SERVO_CLK		(servo_clk_gen2core		),	// SERVO clock, it is 1/2 mem_clk

                .COLD_RST_      (boot_config_finish_),
                .RST_           (clk_gen_rst_)
                );

AUDIO_CLOCK_GEN AUDIO_CLOCK_GEN (

			// input
				.SRC_CLK		(f27m_clk				),		// from clock input pad
				.AUDIO_PLL_FOUT	(audio_clk_pll2gen		),		// 24.576 or 16.9344 MHz from audio PLL
				.JTCLK			(j_tclk_pad2padmisc		),		// jtclk from pad
				.I2S_MCLK_IN	(i2so_mclk_padmisc2core	),		// I2S master clock from PAD input
				.AC97BCK		(1'b0),							// no use in m3357
				.I2S_MCLK_SEL	(i2s_mclk_sel_core2gen	),		// I2S Master clock frequency select
				.SPDIF_CLK_SEL	(spdif_clk_sel_core2gen	),		// SPDIF clock frequency selectDSP_FS

				.BYPASS_PLL		(pll_bypass				),		// By pass audio PLL
				.TEST_MODE		(scan_test_mode_padmisc2core),		// Chip in test mode
			// output
				.I2S_MCLK		(i2so_mclk_gen2core		),		// I2S master clock to audio core and PAD
				.SPDIF_CLK		(spdif_clk_gen2core		),		// spdif clock to audio core
				.AC97BCK_OUT	(		),						// no use in m3357 JFR 030627
				.IADC_MCLK		(iadc_mclk_gen2core		),		// IADC master clock

				.RST_			(clk_gen_rst_			)

				);

VIDEO_CLOCK_GEN VIDEO_CLOCK_GEN (

			// input
				.SRC_CLK			(f27m_clk				),		// from clock input pad
				.F108M_IN			(f108m_clk_pll2gen		),		// 108MHz clock input from PLL
				.F80M_IN			(f80m_clk_pll2gen		),		// 80MHz clock input from PLL
				.JTCLK				(j_tclk_pad2padmisc		),		// jtclk from pad
				.VIDEO_CLK_IN		(ve_clk_gen2gen			),		// VIDEO clock from clock gen

				.BYPASS_PLL			(pll_bypass				),		// By pass audio PLL
				.TEST_MODE			(scan_test_mode_padmisc2core),	// Chip in scan test mode
				.INTER_ONLY			(inter_only_core2gen	),		// Interlace output
				.DVI_RWBUF_SEL		(dvi_rwbuf_sel_core2gen	),		// DVI buffer read/write
				.SVGA_MODE_EN		(svga_mode_en_core2gen	),		// SVGA mode enable, TV encoder output the
			// output
				.CVBS2X_CLK			(cvbs2x_clk_gen2core	),		// 54MHZ clock
				.CVBS_CLK			(cvbs_clk_gen2core		),		// 27MHz clock
				.CAV_CLK			(cav_clk_gen2core		),		// 54 or 27 MHZ clock, or 80MHz in SVGA mode
				.DVI_BUF0_CLK		(dvi_buf0_clk_gen2core	),		// CAV_CLK or VIDEO_CLK for buffer 0
				.DVI_BUF1_CLK		(dvi_buf1_clk_gen2core	),		// CAV_CLK or VIDEO_CLK for buffer 1
				.VIDEO_CLK			(video_clk_gen2core		),		// VIDEO clock output

				.TV_ENC_CLK_SEL		(tv_enc_clk_sel_core2gen),		// video clock delay select
				.F108M_CLK_TV		(tv_f108m_clk_gen2core	),		// 108MHz output for TV encoder
				.CVBS2X_CLK_TV		(tv_cvbs2x_clk_gen2core	),		// 54MHZ clock for TV encoder
				.CVBS_CLK_TV		(tv_cvbs_clk_gen2core	),		// 27MHz clock for TV encoder
				.CAV2X_CLK_TV		(tv_cav2x_clk_gen2core	),		// 108 or 54 MHZ clock for TV encoder
				.CAV_CLK_TV			(tv_cav_clk_gen2core	),		// 54 or 27 MHZ clock for TV encoder

				.RST_				(clk_gen_rst_			)
				);

//====================================================================
// Strap Pin and Reset Logic
//there is not warm reset input in m3357
wire    warm_rst_in_ = 1'b1; //unactive
pad_rst_strap PAD_RST_STRAP(
//<<output>>
//                .warm_rst_oe_           (warm_rst_oe_           ), //no use in m3357, to pad.v
// Strap pin
                .cpu_pll_m              (cpu_clk_pll_m              ),
                .mem_pll_m              (mem_clk_pll_m              ),
                .pci_t2risc_fs          (pci_mips_fs            ),
				.combo_mode				(combo_mode				),//add for m3357 by JFR 030526
                .ide_mode            	(ide_mode            	),
                .pci_mode               (pci_mode               ),
  				.tvenc_test_mode		(tvenc_test_mode		),//analogt_mode in m6304
				.servo_only_mode		(servo_only_mode	    ),//added for m3357	JFR030616
				.servo_test_mode		(servo_test_mode	    ),//added for m3357	JFR030616
				.servo_sram_mode		(servo_sram_mode	    ),//added for m3357	JFR030616
                .pll_bypass             (pll_bypass             ),
                .cpu_probe_en			(cpu_probe_en			),
                //fixed setting, only little endian
                .cpu_endian_mode        (cpu_endian_mode        ),

// Strap pin program
                .cpu_pll_m_ack          (cpu_clk_pll_m_ack_str2core ),
                .mem_pll_m_ack          (mem_clk_pll_m_ack_str2core	),
                .pci_t2risc_fs_ack      (pci_fs_ack_str2core     ),
                .work_mode_ack			(work_mode_ack_str2core	),
// roger
                .clk_gen_rst_           (clk_gen_rst_           ),
                .core_coldrst_          (core_coldrst_          ),
                .core_warmrst_          (core_warmrst_          ),
                .chipset_rst_           (chipset_rst_           ),
                .boot_config_finish_    (boot_config_finish_    ),
                .pad_boot_config        (pad_boot_config        ),
//<<input>>
// strap pin program
                .cpu_pll_m_tri          (cpu_clk_pll_m_tri_core2str    ),
                .cpu_pll_m_ctrldata     (cpu_clk_pll_m_value_core2str  ),
                .mem_pll_m_tri          (mem_clk_pll_m_tri_core2str    ),
                .mem_pll_m_ctrldata     (mem_clk_pll_m_value_core2str  ),
                .pci_t2risc_fs_tri      (pci_fs_tri_core2str     		),
                .pci_t2risc_fs_ctrldata (pci_fs_value_core2str 	 		),
                .work_mode_tri			(work_mode_tri_core2str			),
                .work_mode_ctrldata		(work_mode_value_core2str		),

				.test_mode				(scan_test_mode_padmisc2core	),
                .strap_pin_in           (strap_pin_in           		),
                .sw_rst_pulse           (sw_rst_pulse           		),
                .clk_src_in             (f27m_clk               		),
                .warm_rst_in_           (warm_rst_in_           		),
                .cold_rst_in_           (cold_rst_in_           		),
                .j_pr_rst_              (1'b1                   		),      // from JTAG reset
                .cpu_bus_clk            (mbus_clk_gen2core      		)
                );

//=================== invoke the pad_misc =============================
PAD_MISC PAD_MISC(
//--------------strap pins control
                .strap_pin_out          (strap_pin_in           	),

//------SDRAM
                //connect to pad
                .ram_dq_out2pad			(ram_dq_padmisc2pad         ),
                .ram_dq_oe_out2pad_		(ram_dq_oe_padmisc2pad_		),
                .ram_dq_frompad         (ram_dq_pad2padmisc         ),
                //connect to core
                .ram_dq_fromcore        (ram_dq_core2padmisc        ),
                .ram_dq_oe_fromcore_	(ram_dq_oe_core2padmisc_	),
                .ram_dq_out2core        (ram_dq_padmisc2core        ),

//------SDRAM DQM, Command Line
                .ram_dqm_out2pad		(ram_dqm_padmisc2pad		),
                .ram_dqm_oe_out2pad_	(ram_dqm_oe_padmisc2pad_	),
                .ram_dqm_fromcore		(ram_dqm_core2padmisc		),
                .ram_dqm_frompad        (ram_dqm_pad2padmisc    	),
//------SDRAM bank address, address line
                .ram_ba_out2pad			(ram_ba_padmisc2pad			),
                .ram_ba_oe_out2pad_		(ram_ba_oe_padmisc2pad_		),
                .ram_ba_fromcore		(ram_ba_core2padmisc		),
                .ram_ba_frompad         (ram_ba_pad2padmisc     	),

                .ram_addr_out2pad		(ram_addr_padmisc2pad		),
                .ram_addr_oe_out2pad_	(ram_addr_oe_padmisc2pad	),
                .ram_addr_fromcore		(ram_addr_core2padmisc		),
                .ram_addr_frompad       (ram_addr_pad2padmisc   	),

//-----	SDRAM Command Line
                .ram_cs_out2pad_		(ram_cs_padmisc2pad_		),
                .ram_cs_fromcore_		(ram_cs_core2padmisc		),
                .ram_we_out2pad_		(ram_we_padmisc2pad_		),
                .ram_we_fromcore_		(ram_we_core2padmisc		),
//                .ram_we_frompad_		(ram_we_pad2padmisc_		),//JFR030627

                .ram_cas_out2pad_		(ram_cas_padmisc2pad_		),
                .ram_cas_fromcore_		(ram_cas_core2padmisc		),
                .ram_ras_out2pad_		(ram_ras_padmisc2pad_		),
                .ram_ras_fromcore_		(ram_ras_core2padmisc		),
//                .ram_ras_frompad_		(ram_ras_pad2padmisc_		),//JFR030627

                .ram_cs_oe_out2pad_		(ram_cs_oe_padmisc2pad_		),
                .ram_we_oe_out2pad_		(ram_we_oe_padmisc2pad_		),
                .ram_cas_oe_out2pad_	(ram_cas_oe_padmisc2pad_	),
                .ram_ras_oe_out2pad_	(ram_ras_oe_padmisc2pad_	),

                .ram_cs_frompad_		(ram_cs_pad2padmisc_		),
                .ram_cas_frompad_		(ram_cas_pad2padmisc_		),

//------FLASH
                .rom_rdata_out2core  		(rom_rdata_padmisc2core		),
                .rom_wdata_fromcore  		(rom_wdata_core2padmisc      ),
                .rom_data_oe_fromcore_		({8{rom_data_oe_core2padmisc_}}),
                .rom_data_out2pad			(rom_data_padmisc2pad		),
                .rom_data_oe_out2pad_		(rom_data_oe_padmisc2pad_	),
                .rom_data_frompad			(rom_data_pad2padmisc		),

                .rom_addr_fromcore   		(rom_addr_core2padmisc[20:0] ),
                .rom_addr_oe_fromcore		(rom_addr_oe_core2padmisc    ),
                .rom_addr_out2pad			(rom_addr_padmisc2pad		),
                .rom_addr_oe_out2pad_		(rom_addr_oe_padmisc2pad_	),
                .rom_addr_frompad			(rom_addr_pad2padmisc		),

		        .rom_oe_fromcore_			(rom_oe_core2padmisc			),
		        .rom_oe_out2pad_			(rom_oe_padmisc2pad_			),
		        .rom_oe_oe_out2pad_			(rom_oe_oe_padmisc2pad_		),
		        .rom_oe_frompad_			(rom_oe_pad2padmisc_			),

		        .rom_ce_fromcore_			(rom_ce_core2padmisc			),
		        .rom_ce_out2pad_			(rom_ce_padmisc2pad_			),
		        .rom_ce_oe_out2pad_			(rom_ce_oe_padmisc2pad_		),
		        .rom_ce_frompad_			(rom_ce_pad2padmisc_			),

		        .rom_we_fromcore_			(rom_we_core2padmisc			),
		        .rom_we_out2pad_			(rom_we_padmisc2pad_			),
		        .rom_we_oe_out2pad_			(rom_we_oe_padmisc2pad_		),
		        .rom_we_frompad_			(rom_we_pad2padmisc_			),

//--------------CPU Bus
                .cpu_sysint_out2core_   	(sys_int_padmisc2core_       		),
                .cpu_sysnmi_out2core_   	(sys_nmi_padmisc2core_          	),

                .cpu_errint_fromcore    	(x_err_int_core2padmisc         	),
                .cpu_extint_fromcore    	(x_ext_int_core2padmisc         	),

				.cpu_bist_tri_out2core		(cpu_bist_mode_padmisc2core			),
				.cpu_bist_finish_fromcore 	(cpu_bist_finish_core2padmisc		),
				.cpu_bist_vec_fromcore		(cpu_bist_error_vec_core2padmisc	),

//------EJTAG
				.j_tdi_out2pad				(j_tdi_padmisc2pad				),
				.j_tdo_out2pad				(j_tdo_padmisc2pad				),
				.j_tms_out2pad				(j_tms_padmisc2pad				),
				.j_trst_out2pad_			(j_trst_padmisc2pad_			),
				.j_tclk_out2pad				(j_tclk_padmisc2pad				),

				.j_tdi_oe_out2pad_			(j_tdi_oe_padmisc2pad_			),
				.j_tdo_oe_out2pad_			(j_tdo_oe_padmisc2pad_			),
				.j_tms_oe_out2pad_			(j_tms_oe_padmisc2pad_			),
				.j_trst_oe_out2pad_			(j_trst_oe_padmisc2pad_			),
				.j_tclk_oe_out2pad_			(j_tclk_oe_padmisc2pad_			),

				.j_tdo_fromcore				(j_tdo_core2padmisc				),

				.j_tdi_frompad				(j_tdi_pad2padmisc				),
				.j_tdo_frompad				(j_tdo_pad2padmisc				),
				.j_tms_frompad				(j_tms_pad2padmisc				),
				.j_trst_frompad_			(j_trst_pad2padmisc_			),
				.j_tclk_frompad				(j_tclk_pad2padmisc				),

				.j_tdi_out2core				(j_tdi_padmisc2core				),
				.j_tms_out2core				(j_tms_padmisc2core				),
				.j_trst_out2core_			(j_trst_padmisc2core_			),
				.j_tclk_out2core			(j_tclk_padmisc2core			),

//------IDE
                //connect to core
                .atadiow_fromcore_      (atadiow_core2padmisc_   		),
                .atadior_fromcore_      (atadior_core2padmisc_   		),
                .atacs0_fromcore_       (atacs0_core2padmisc_    		),
                .atacs1_fromcore_       (atacs1_core2padmisc_    		),
                .atadd_fromcore         (atadd_core2padmisc      		),
                .atadd_oe_fromcore_     ({16{atadd_oe_core2padmisc_[0]}}),
                .atada_fromcore         (atada_core2padmisc      		),
                .atareset_fromcore_     (atareset_core2padmisc_  		),
                .atadmack_fromcore_     (atadmack_core2padmisc_  		),

                .ataiordy_out2core      (ataiordy_padmisc2core   		),
                .ataintrq_out2core      (ataintrq_padmisc2core   		),
                .atadd_out2core         (atadd_padmisc2core      		),
                .atadmarq_out2core      (atadmarq_padmisc2core   		),

                .atareset_oe_fromcore_	(atareset_oe_core2padmisc_		),
//--------------PCI Bus
                //connect to core
                .pci_clk_fromcore		(pci_mbus_clk_gen				),

                .pci_ad_out2core        (pci_ad_padmisc2core            ),
                .pci_cbe_out2core_      (pci_cbe_padmisc2core_          ),
                .pci_par_out2core       (pci_par_padmisc2core           ),
                .pci_frame_out2core_    (pci_frame_padmisc2core_        ),
                .pci_irdy_out2core_     (pci_irdy_padmisc2core_         ),
                .pci_trdy_out2core_     (pci_trdy_padmisc2core_         ),
                .pci_stop_out2core_     (pci_stop_padmisc2core_         ),
                .pci_devsel_out2core_   (pci_devsel_padmisc2core_       ),
                .pci_req_out2core_      (pci_req_padmisc2core_          ),//2 bits width
				.pci_inta_out2core_		(pci_inta_padmisc2core_			),

                .pci_ad_fromcore        (pci_ad_core2padmisc            ),
                .pci_cbe_fromcore_      (pci_cbe_core2padmisc_          ),
                .pci_par_fromcore       (pci_par_core2padmisc           ),
                .pci_frame_fromcore_    (pci_frame_core2padmisc_        ),
                .pci_irdy_fromcore_     (pci_irdy_core2padmisc_         ),
                .pci_gnta_fromcore_     (pci_gnt_core2padmisc_[0]       ),
                .pci_gntb_fromcore_		(pci_gnt_core2padmisc_[1]		),
                .pci_trdy_fromcore_     (pci_trdy_core2padmisc_         ),
                .pci_stop_fromcore_     (pci_stop_core2padmisc_         ),
                .pci_devsel_fromcore_   (pci_devsel_core2padmisc_       ),

                .pci_ad_oe_fromcore_    ({32{pci_ad_oe_core2padmisc_}}  ),
                .pci_cbe_oe_fromcore_   ({4{pci_cbe_oe_core2padmisc_}}  ),
                .pci_par_oe_fromcore_   (pci_par_oe_core2padmisc_       ),
                .pci_frame_oe_fromcore_ (pci_frame_oe_core2padmisc_     ),
                .pci_irdy_oe_fromcore_  (pci_irdy_oe_core2padmisc_      ),
                .pci_trdy_oe_fromcore_  (pci_trdy_oe_core2padmisc_      ),
                .pci_stop_oe_fromcore_  (pci_stop_oe_core2padmisc_      ),
                .pci_devsel_oe_fromcore_(pci_devsel_oe_core2padmisc_    ),

//--------------GPIO(0~13, 28~24)
                //connect to pad
                .gpio_out2pad           (gpio_padmisc2pad[28:0]        	),
                .gpio_oe_out2pad_       (gpio_oe_padmisc2pad_[28:0]   	),
                .gpio_frompad           (gpio_pad2padmisc[28:0]        	),
                //connect to core
                .gpio_fromcore          (gpio_core2padmisc[31:0]		),
                .gpio_out2core          (gpio_padmisc2core[31:0]   		),
                .gpio_oe_fromcore_      (gpio_oe_core2padmisc_[31:0]	),

//--------------I2C Intf
                //connect to core
                .scbsda_out2core        (scbsda_padmisc2core            ),
                .scbscl_out2core        (scbscl_padmisc2core            ),

                .scbsda_fromcore        (scbsda_core2padmisc            ),
                .scbscl_fromcore        (scbscl_core2padmisc            ),
                .scbsda_oe_fromcore     (scbsda_oe_core2padmisc         ),
                .scbscl_oe_fromcore     (scbscl_oe_core2padmisc         ),

//------IR
				.irrx_frompad			(irrx_pad2padmisc				),
				.irrx_out2pad			(irrx_padmisc2pad				),
				.irrx_oe_out2pad_		(irrx_oe_padmisc2pad_			),
				.irrx_out2core			(irrx_padmisc2core				),

//-----UART
				.uart_tx_fromcore		(uart_tx_core2padmisc			),
				.uart_tx_out2pad		(uart_tx_padmisc2pad			),
				.uart_tx_oe_out2pad_	(uart_tx_oe_padmisc2pad_		),
				.uart_tx_frompad		(uart_tx_pad2padmisc			),
				.uart_rx_out2core		(uart_rx_padmisc2core			),

//--------------USB
                //connect to pad
				.usbpon_out2pad			(usbpon_padmisc2pad				),
				.usbpon_oe_out2pad_		(usbpon_oe_padmisc2pad_			),
				.usbpon_fromcore		(usbpon_core2padmisc_			),

				.usbovc_frompad			(usbovc_pad2padmisc				),
				.usbovc_out2core		(usbovc_padmisc2core			),

//------I2S
        		.i2so_data0_out2pad		(i2so_data0_padmisc2pad			),
				.i2so_data1_out2pad		(i2so_data1_padmisc2pad			),
				.i2so_data2_out2pad		(i2so_data2_padmisc2pad			),
				.i2so_data3_out2pad		(i2so_data3_padmisc2pad			),
				.i2so_data0_oe_out2pad_	(i2so_data0_oe_padmisc2pad_		),
				.i2so_data1_oe_out2pad_	(i2so_data1_oe_padmisc2pad_		),
				.i2so_data2_oe_out2pad_	(i2so_data2_oe_padmisc2pad_		),
				.i2so_data3_oe_out2pad_	(i2so_data3_oe_padmisc2pad_		),
				.i2so_data0_frompad		(i2so_data0_pad2padmisc			),
				.i2so_data1_frompad		(i2so_data1_pad2padmisc			),
				.i2so_data2_frompad		(i2so_data2_pad2padmisc			),
				.i2so_data3_frompad		(i2so_data3_pad2padmisc			),

                .i2si_data_out2core     (i2si_data_padmisc2core         ),
				.i2so_data0_fromcore	(i2so_data0_core2padmisc		),
				.i2so_data1_fromcore	(i2so_data1_core2padmisc		),
				.i2so_data2_fromcore	(i2so_data2_core2padmisc		),
				.i2so_data3_fromcore	(i2so_data3_core2padmisc		),

				.i2so_mclk_fromcore		(i2so_mclk_gen2core				),
				.i2so_mclk_oe_fromcore_	(i2so_mclk_oe_core2padmisc_		),
				.i2so_mclk_out2pad		(i2so_mclk_padmisc2pad			),
				.i2so_mclk_oe_out2pad_	(i2so_mclk_oe_padmisc2pad_		),
				.i2so_mclk_out2core		(i2so_mclk_padmisc2core			),
				.i2so_mclk_frompad		(i2so_mclk_pad2padmisc			),

				.i2so_bck_out2pad		(i2so_bck_padmisc2pad			),
				.i2so_bck_oe_out2pad_	(i2so_bck_oe_padmisc2pad_		),
				.i2so_bck_fromcore		(i2so_bick_core2padmisc			),
				.i2so_bck_frompad		(i2so_bck_pad2padmisc			),

				.i2so_lrck_out2pad		(i2so_lrck_padmisc2pad			),
				.i2so_lrck_oe_out2pad_	(i2so_lrck_oe_padmisc2pad_		),
				.i2so_lrck_fromcore		(i2so_lrck_core2padmisc			),
				.i2so_lrck_frompad		(i2so_lrck_pad2padmisc			),

				.iadc_smtsen_out2core	(iadc_smtsen_padmisc2core		),		// ADC SRAM Test Enable
				.iadc_smtsmode_out2core	(iadc_smtsmode_padmisc2core		),		// ADC SRAM Test Mode
				.iadc_smts_err_fromcore	(iadc_smts_err_core2padmisc		),		// ADC SRAM Test Error Flag
				.iadc_smts_end_fromcore	(iadc_smts_end_core2padmisc		),		// ADC SRAM Self Test End
				.iadc_clkp1_fromcore	(iadc_clkp1_core2padmisc		),		// ADC digital test pin 1
				.iadc_clkp2_fromcore	(iadc_clkp2_core2padmisc		),		// ADC digital test pin 2
				.iadc_tst_mod_out2core	(iadc_tst_mod_padmisc2core		),		// ADC Internal SDM Test Selection

//--------------spdif output enable
                //connect to pad
				.spdif_out2pad			(spdif_padmisc2pad				),
				.spdif_oe_out2pad_		(spdif_oe_padmisc2pad_			),
				.spdif_fromcore			(spdif_core2padmisc				),
				.spdif_frompad			(spdif_pad2padmisc				),
//----- video reset and its enable,share with gpio[27]
                //connect to pad
				.tvenc_h_sync_fromcore_		(tvenc_h_sync_core2padmisc_		),
				.tvenc_v_sync_fromcore_		(tvenc_v_sync_core2padmisc_		),

				.vdata_fromcore				(vdata_core2padmisc				),
				.de_h_sync_fromcore_		(de_h_sync_core2padmisc_		),
				.de_v_sync_fromcore_		(de_v_sync_core2padmisc_		),

				.vdi4vdac_out2core			(tvenc_vdi4vdac_padmisc2core	),
				.vdo4vdac_fromcore			(tvenc_vdo4vdac_core2padmisc	),
				.pix_clk_fromcore			(cvbs_clk_gen2core				),	//from clock generator directly
// VIDEO_IN
				.video_in_pixel_clk_out2core	(video_in_pixel_clk_padmisc2core	),
				.video_in_pixel_data_out2core	(video_in_pixel_data_padmisc2core	),
				.video_in_hsync_out2core_		(video_in_hsync_padmisc2core_		),
				.video_in_vsync_out2core_		(video_in_vsync_padmisc2core_		),

//------SERVO
				.sv_gpio_fromcore			(sv_gpio_core2padmisc			),
				.sv_gpio_oe_fromcore_		(sv_gpio_oe_core2padmisc_		),
				.sv_gpio_out2core			(sv_gpio_padmisc2core			),

				.sv_atadd_fromcore			(sv_atadd_core2padmisc			),
				.sv_ataiordy_fromcore		(sv_ataiordy_core2padmisc		),
				.sv_ataintrq_fromcore		(sv_ataintrq_core2padmisc		),
				.sv_atadmarq_fromcore		(sv_atadmarq_core2padmisc		),
				.sv_ataiocs16_fromcore_		(sv_ataiocs16_core2padmisc_		),
				.sv_atapdiag_fromcore_		(sv_atapdiag_core2padmisc_		),
				.sv_atadasp_fromcore_		(sv_atadasp_core2padmisc_		),

				.sv_tst_pin_fromcore		(sv_tst_pin_core2padmisc    	),
				.sram_ifx_fromcore			(sram_ifx_core2padmisc			),

				.sv_atadd_oe_fromcore_		(sv_atadd_oe_core2padmisc_		),
				.sv_ataiocs16_oe_fromcore_	(sv_ataiocs16_oe_core2padmisc_	),
				.sv_atapdiag_oe_fromcore_	(sv_atapdiag_oe_core2padmisc_	),
				.sv_atadasp_oe_fromcore_	(sv_atadasp_oe_core2padmisc_	),

				.sram_ifx_oe_fromcore_		(sram_ifx_oe_core2padmisc_		),

				.sram_ifx_out2core			(sram_ifx_padmisc2core			),

				.sv_atadd_out2core			(sv_atadd_padmisc2core			),
				.sv_atada_out2core			(sv_atada_padmisc2core			),
				.sv_atacs0_out2core_		(sv_atacs0_padmisc2core_		),
				.sv_atacs1_out2core_		(sv_atacs1_padmisc2core_		),
				.sv_atadior_out2core_		(sv_atadior_padmisc2core_		),
				.sv_atadiow_out2core_		(sv_atadiow_padmisc2core_		),
				.sv_atadmack_out2core_		(sv_atadmack_padmisc2core_		),
				.sv_atareset_out2core_		(sv_atareset_padmisc2core_		),
				.sv_atapdiag_out2core_		(sv_atapdiag_padmisc2core_		),
				.sv_atadasp_out2core_		(sv_atadasp_padmisc2core_		),

				.ext_up_out2core			(ext_up_padmisc2core			),
				.ext_up_fromcore			(ext_up_core2padmisc			),
				.ext_up_oe_fromcore_		(ext_up_oe_core2padmisc_		),

				.sv_tplck_fromcore			(sv_sv_tplck_core2padmisc			),
				.sv_tslrf_fromcore			(sv_sv_tslrf_core2padmisc			),

				.pmsvd_trcclk_out2core		(sv_pmsvd_trcclk_padmisc2core		),
				.pmsvd_trfclk_out2core		(sv_pmsvd_trfclk_padmisc2core		),
				.pmsvd_tplck_out2core		(sv_pmsvd_tplck_padmisc2core		),
				.pmsvd_tslrf_out2core		(sv_pmsvd_tslrf_padmisc2core		),
				.pmsvd_c2ftstin_0_out2core	(sv_pmsvd_c2ftstin_padmisc2core[0]),
				.pmsvd_c2ftstin_1_out2core	(sv_pmsvd_c2ftstin_padmisc2core[1]),

				.sv_tst_addata_out2core		(sv_tst_addata_padmisc2core		),		// Servo External A/D data input
				.sv_reg_rctst_2_fromcore	(sv_reg_rctst_2_core2padmisc	),		// Servo RC test mode enable

				.xsflag_0_out2pad			(sv_xsflag_padmisc2pad[0]		),
				.xsflag_1_out2pad			(sv_xsflag_padmisc2pad[1]		),
				.xsflag_0_oe_out2pad_		(sv_xsflag_oe_padmisc2pad_[0]	),
				.xsflag_1_oe_out2pad_		(sv_xsflag_oe_padmisc2pad_[1]	),
				.xsflag_0_frompad			(sv_xsflag_pad2padmisc[0]		),
				.xsflag_1_frompad			(sv_xsflag_pad2padmisc[1]		),
//				.xsflag_0_out2core			(sv_xsflag_padmisc2core[0]		),
//				.xsflag_1_out2core			(sv_xsflag_padmisc2core[1]		),
				.xsflag_0_fromcore			(sv_xsflag_core2padmisc[0]		),
				.xsflag_1_fromcore			(sv_xsflag_core2padmisc[1]		),
				.xsflag_1_oe_fromcore_		(1'b0							),		//	servo flag is output only
				.xsflag_0_oe_fromcore_		(1'b0							),		//	servo flag is output only

//	SERVO control
				.sv_tst_pin_en_fromcore				(sv_tst_pin_en_core2padmisc				),	// SERVO test pin enable
				.sv_ide_mode_part0_en_fromcore		(sv_ide_mode_part0_en_core2padmisc		),	// ide mode SERVO test pin part 0 enable
				.sv_ide_mode_part1_en_fromcore		(sv_ide_mode_part1_en_core2padmisc		),	// ide mode SERVO test pin part 1 enable
				.sv_ide_mode_part2_en_fromcore		(sv_ide_mode_part2_en_core2padmisc		),	// ide mode SERVO test pin part 2 enable
				.sv_ide_mode_part3_en_fromcore		(sv_ide_mode_part3_en_core2padmisc		),	// ide mode SERVO test pin part 3 enable
				.sv_c2ftstin_en_fromcore			(sv_c2ftstin_en_core2padmisc			),	// SERVO c2 flag enable
				.sv_trg_plcken_fromcore				(sv_trg_plcken_core2padmisc				),	// SERVO tslrf and tplck enable
				.sv_pmsvd_trcclk_trfclk_en_fromcore	(sv_pmsvd_trcclk_trfclk_en_core2padmisc	),	// SERVO pmsvd_trcclk, pmsvd_trfclk enable

//	Control signals
				.hv_sync_en_fromcore		(hv_sync_en_core2padmisc		),		// h_sync_, v_sync_ enable on the xsflag[1:0] pins
				.i2si_data_en_fromcore		(i2si_data_en_core2padmisc		),		// I2S input data enable on the gpio[7] pin
				.scb_en_fromcore			(scb_en_core2padmisc			),		// SCB interface enable on the gpio[6:5] pins
				.sv_gpio_en_fromcore		(sv_gpio_en_core2padmisc		),		// servo_gpio[2:0] enable on the gpio[2:0] pins
				.ext_ide_device_en_fromcore	(ext_ide_device_en_core2padmisc	),		// external IDE device enable
				.vdata_en_fromcore			(vdata_en_core2padmisc			),		// video data enable on sdram [23:16]
				.sync_source_sel_fromcore	(sync_source_sel_core2padmisc	),		// sync signal source select, 0 -> TVENC, 1 -> DE.
				.pci_gntb_en_fromcore		(pci_gntb_en_core2padmisc		),		// pci GNTB enable
				.video_in_intf_en_fromcore	(video_in_intf_en_core2padmisc	),		// video in interface enable
				.pci_mode_ide_en_fromcore	(pci_mode_ide_en_core2padmisc	),		// pci_mode ide interface enable

				.ide_active_fromcore		(ide_active_core2padmisc		),		// ide interface is active in the pci mode
				.ice_probe_en_fromcore		(cpu_probe_en					),		// ICE probe enable. When disabled, the EJTAG signals will
        																			// be used as the GPIO[20:16]

//------TEST_MODE
				.dsp_bist_tri_out2core		(dsp_bist_tri_padmisc2core		),
				.dsp_bist_finish_fromcore	(dsp_bist_finish_core2padmisc	),
				.dsp_bist_vec_fromcore		(dsp_bist_vec_core2padmisc		),

				.video_bist_tri_out2core	(video_bist_tri_padmisc2core	),
				.video_bist_finish_fromcore	(video_bist_finish_core2padmisc	),
				.video_bist_vec_fromcore	(video_bist_vec_core2padmisc	),

                .rom_en_fromcore_       	(rom_en_core2padmisc_   		),

                .combo_mode					(combo_mode						),
                .ide_mode            		(ide_mode            			),      //select ide mode, 1 means ide mode
                .pci_mode               	(pci_mode               		),      //select pci_mode, 1 means pci
                .tvenc_test_mode			(tvenc_test_mode				),
                .servo_only_mode			(servo_only_mode				),
                .servo_test_mode			(servo_test_mode				),
                .servo_sram_mode			(servo_sram_mode				),

                .mem_clk_in             	(inter_mem_clk_gen      		),
        		.cpu_pll_fck_fromcore		(cpu_clk_pll_fck				),
        		.cpu_pll_lock_fromcore		(cpu_clk_pll_lock				),
        		.mem_pll_fck_fromcore		(mem_clk_pll_fck				),
        		.mem_pll_lock_fromcore		(mem_clk_pll_lock				),
        		.audio_pll_fck_fromcore		(audio_pll_fck					),
        		.audio_pll_lock_fromcore	(audio_pll_lock					),
        		.f27_pll_fck_fromcore		(f27_pll_fck					),
        		.f27_pll_lock_fromcore		(f27_pll_lock					),

                .test_mode_frompad      	(test_mode_frompad      		),
                .scan_en_frompad			(j_tms_pad2padmisc				),
                .scan_en_out2core			(scan_en_padmisc2core			),
                .scan_chain_sel_out2core	(test_sel_padmisc2core			),
                .scan_dout_fromcore			(scan_dout_mux2padmisc			),
                .scan_din_out2core			(scan_din_padmisc2mux			),
                .mem_rd_dly_sel				(mem_rd_dly_sel					),
				.test_dly_chain_sel			(test_dly_chain_sel				),	// TEST_DLY_CHAIN delay select
				.test_dly_chain_flag		(test_dly_chain_flag			),	// TEST_DLY_CHAIN flag output

                .bond_option_frompad		(bond_option_pad2padmisc		),

                .bist_test_mode				(bist_test_mode_padmisc2core	),
                .scan_test_mode				(scan_test_mode_padmisc2core	),
                .rst_                   	(chipset_rst_           		)

                );
//======================End of invoke the pad_misc================================
scan_mux  SCAN_MUX(
				.scan_din_out2core		(scan_din						),
				.scan_dout_fromcore		(scan_dout						),
				.scan_din_frompadmisc	(scan_din_padmisc2mux			),
				.scan_dout_out2padmisc	(scan_dout_mux2padmisc			),

				.test_mode				(scan_test_mode_padmisc2core	),
				.test_sel				(test_sel_padmisc2core			)
				);
//=================== invoke the pad =================================

iopad   IOPAD   (
//========================== pad ===========================================
// SDRAM Interface (55)
                .p_d_clk                (p_d_clk                ),      // 1,   SDRAM master clock input
                .p_d_addr               (p_d_addr               ),      // 12,  SDRAM address
                .p_d_baddr              (p_d_baddr              ),      // 2,   SDRAM bank address
                .p_d_dq                 (p_d_dq                 ),      // 32,  SDRAM 32 bits output data pins
                .p_d_dqm                (p_d_dqm                ),      // 4,   SDRAM data mask signal
                .p_d_cs_                (p_d_cs_                ),      // 2,   SDRAM chip select
                .p_d_cas_               (p_d_cas_               ),      // 1,   SDRAM cas#
                .p_d_we_                (p_d_we_                ),      // 1,   SDRAM we#
                .p_d_ras_               (p_d_ras_               ),      // 1,   SDRAM ras#
// FLASH ROM Interface(3, others share with SDRAM bus)
                .p_rom_ce_              (p_rom_ce_              ),      // 1,   Flash rom chip select
                .p_rom_we_              (p_rom_we_              ),      // 1,   Flash rom write enable
                .p_rom_oe_              (p_rom_oe_              ),      // 1,   Flash rom output enable
				.p_rom_data				(p_rom_data				),		// 8 	Flash rom data
        		.p_rom_addr				(p_rom_addr				),		// 21	Flash rom address
// I2S Audio Interface (5)
                .i2so_data0             (i2so_data0             ),      // 1,   Serial data0 for I2S output
                .i2so_data1				(i2so_data1				),		// 1,	Serial data1 for I2S output
                .i2so_data2				(i2so_data2				),		// 1,	Serial data2 for I2S output
                .i2so_data3				(i2so_data3				),		// 1,	Serial data3 for I2S output
                .i2so_bck               (i2so_bck               ),      // 1,   Bit clock for I2S output
                .i2so_lrck              (i2so_lrck              ),      // 1,   Channel clock for I2S output
                .i2so_mclk              (i2so_mclk              ),      // 1,   over sample clock for i2s DAC
//                .i2si_data              (i2si_data              ),      // 1,   serial data for I2S input

//TV encoder Interface (10)
				.gnda_tvdac				(gnda_tvdac				),		// 1,	Analog GND, input
				.gnda1_tvdac			(gnda1_tvdac			),		// 1,	Analog GND, input
				.vsud_tvdac				(vsud_tvdac				),		// 1,	Analog GND, input
				.vd33a_tvdac			(vd33a_tvdac 			),		// 3,	Analog Power, input
				.iout1					(iout1					),		// 1,   Analog video output 1
				.iout2					(iout2					),		// 1,   Analog video output 2
				.iout3					(iout3					),		// 1,   Analog video output 3
				.iout4					(iout4					),		// 1,   Analog video output 4
				.iout5					(iout5					),		// 1,   Analog video output 5
				.iout6					(iout6					),		// 1,   Analog video output 6

//added for m3357 JFR030613
				.xiref1					(xiref1					),		// 1,	connect external via capacitance toward VDDA
				.xiref2					(xiref2					),		// 1,	connect external via capacitance toward VDDA
				.xiext					(xiext					),		// 1,	470 Ohms resistor connect pin, I/O
				.xiext1					(xiext1					),		// 1,	470 Ohms resistor connect pin, I/O
				.xidump					(xidump					),		// 1,	For performance and chip temperature, output

// SPDIF (1)
                .spdif                  (spdif                  ),      // 1, 	output only

//UART port (2)JFR030730
				.uart_tx				(uart_tx				),		// 1
// USB port (6)
                .usb2dp                 (usb2dp                 ),      // 1,   D+ for USB port2
                .usb2dn                 (usb2dn                 ),      // 1,   D- for USB port2
                .usb1dp                 (usb1dp                 ),      // 1,   D+ for USB port1
                .usb1dn                 (usb1dn                 ),      // 1,   D- for USB port1
                .usbpon                 (usbpon                 ),      // 1,   USB port power control
                .usbovc                 (usbovc                 ),      // 1,   USB port over current

// Consumer IR Interface (1)
                .irrx                   (irrx                   ),      // 1,   consumer infra-red remote controller/wireless keyboard
// General Purpose IO (8,GPIO)
                .gpio                   (gpio                   ),      // 14,   General purpose io[7:0]
// EJTAG Interface (5)
                .p_j_tdo                (p_j_tdo                ),      // 1,   serial test data output
                .p_j_tdi                (p_j_tdi                ),      // 1,   serial test data input
                .p_j_tms                (p_j_tms    			),      // 1,   test mode select
                .p_j_tclk               (p_j_tclk               ),      // 1,   test clock
                .p_j_rst_               (p_j_rst_               ),      // 1,   test reset
// system (4)
                .p_crst_                (p_crst_                ),      // 1,   cold reset
                .test_mode              (test_mode              ),
				.p_x27in				(p_x27in				),      // 1,   crystal input (27 MHz for video output)
        		.p_x27out        		(p_x27out				),		// 1,   crystal output
// power and ground
				.vddcore				(vddcore				),		// 		core vdd
//				.vsscore				(vsscore				),		//		core vss
				.vddio					(vddio					),		//		io vdd
				.gnd					(gnd					),		//		io vss
				.cpu_pllvdd				(cpu_pllvdd				),		//		cpu pll vdd
				.cpu_pllvss				(cpu_pllvss				),		//		cpu pll vss
				.f27_pllvdd				(f27_pllvdd				),		//		f27 pll vdd
				.f27_pllvss             (f27_pllvss             ),      //		f27 pll vss
//ADC (5)
				.vd33a_adc				(vd33a_adc	 			),		// 1,	3.3V Analog VDD for ADC.
				.xmic1in     			(xmic1in     			),      // 1,   ADC microphone input 1.
				.xadc_vref   			(xadc_vref   			),      // 1,   ADC reference voltage (refered to the reference circuit).
				.xmic2in     			(xmic2in     			),      // 1,   ADC microphone input 2.
				.gnda_adc    			(gnda_adc    			),   	// 1,   Analog GND for ADC.
//=========================== internal net =======================================
                // SDRAM & FLASH
                .ram_addr_frompadmisc   	(ram_addr_padmisc2pad		),
                .ram_addr_oe_frompadmisc_	(ram_addr_oe_padmisc2pad	),
                .ram_ba_frompadmisc     	(ram_ba_padmisc2pad     	),
                .ram_ba_oe_frompadmisc_		(ram_ba_oe_padmisc2pad_		),
                .ram_dq_frompadmisc     	(ram_dq_padmisc2pad			),//p_ram_dq_out, change the dq bus from core directly,yuky,2002-07-09
                .ram_dq_oe_frompadmisc_ 	(ram_dq_oe_padmisc2pad_     ),
                .ram_cas_frompadmisc_   	(ram_cas_padmisc2pad_       ),
                .ram_cas_oe_frompadmisc_	(ram_cas_oe_padmisc2pad_	),
                .ram_we_frompadmisc_    	(ram_we_padmisc2pad_        ),
                .ram_we_oe_frompadmisc_		(ram_we_oe_padmisc2pad_		),
                .ram_ras_frompadmisc_   	(ram_ras_padmisc2pad_       ),
                .ram_ras_oe_frompadmisc_	(ram_ras_oe_padmisc2pad_	),

                .ram_dqm_frompadmisc    	(ram_dqm_padmisc2pad        ),
                .ram_dqm_oe_frompadmisc_	(ram_dqm_oe_padmisc2pad_	),
                .ram_cs_frompadmisc_    	(ram_cs_padmisc2pad_        ),
                .ram_cs_oe_frompadmisc_		(ram_cs_oe_padmisc2pad_		),

                .ram_dq_out2padmisc     	(ram_dq_pad2padmisc         ),		//strap pins
                .ram_addr_out2padmisc   	(ram_addr_pad2padmisc   	),      //strap pins
                .ram_ba_out2padmisc     	(ram_ba_pad2padmisc     	),      //strap pins
                .ram_dqm_out2padmisc    	(ram_dqm_pad2padmisc    	),      //strap pins
                .ram_cs_out2padmisc_		(ram_cs_pad2padmisc_		),
                .ram_cas_out2padmisc_		(ram_cas_pad2padmisc_		),

                // EEPROM
                .rom_ce_frompadmisc_		(rom_ce_padmisc2pad_	),
                .rom_oe_frompadmisc_		(rom_oe_padmisc2pad_	),
                .rom_we_frompadmisc_		(rom_we_padmisc2pad_	),
//added for m3357 JFR030613
         		.rom_data_frompadmisc    	(rom_data_padmisc2pad	),
        		.rom_addr_frompadmisc		(rom_addr_padmisc2pad	),

                .rom_ce_oe_frompadmisc_		(rom_ce_oe_padmisc2pad_	),
                .rom_oe_oe_frompadmisc_		(rom_oe_oe_padmisc2pad_	),
                .rom_we_oe_frompadmisc_		(rom_we_oe_padmisc2pad_	),
//added for m3357 JFR030613
				.rom_data_oe_frompadmisc_ 	(rom_data_oe_padmisc2pad_),
				.rom_addr_oe_frompadmisc_ 	(rom_addr_oe_padmisc2pad_),

                .rom_ce_out2padmisc_		(rom_ce_pad2padmisc_	),
                .rom_oe_out2padmisc_		(rom_oe_pad2padmisc_	),
                .rom_we_out2padmisc_		(rom_we_pad2padmisc_	),
//added for m3357 JFR030613
				.rom_data_out2padmisc		(rom_data_pad2padmisc	),
        		.rom_addr_out2padmisc    	(rom_addr_pad2padmisc	),

//------GPIO
                .gpio_frompadmisc       	(gpio_padmisc2pad           ),
                .gpio_oe_frompadmisc_   	(gpio_oe_padmisc2pad_       ),
                .gpio_out2padmisc       	(gpio_pad2padmisc           ),

//				.i2si_data_out2padmisc    	(i2si_data_pad2padmisc   	),
				.i2so_data0_out2padmisc    	(i2so_data0_pad2padmisc  	),
				.i2so_data1_out2padmisc    	(i2so_data1_pad2padmisc  	),
				.i2so_data2_out2padmisc    	(i2so_data2_pad2padmisc  	),
				.i2so_data3_out2padmisc    	(i2so_data3_pad2padmisc  	),
				.i2so_mclk_out2padmisc      (i2so_mclk_pad2padmisc   	),
				.i2so_bck_out2padmisc		(i2so_bck_pad2padmisc		),
				.i2so_lrck_out2padmisc		(i2so_lrck_pad2padmisc		),

//        		.i2si_data_frompadmisc		(i2si_data_padmisc2pad		),
//				.i2si_data_oe_frompadmisc_	(i2si_data_oe_padmisc2pad_	),
				.i2so_data0_frompadmisc     (i2so_data0_padmisc2pad		),
				.i2so_data1_frompadmisc     (i2so_data1_padmisc2pad		),
				.i2so_data2_frompadmisc     (i2so_data2_padmisc2pad		),
				.i2so_data3_frompadmisc     (i2so_data3_padmisc2pad		),
				.i2so_data0_oe_frompadmisc_	(i2so_data0_oe_padmisc2pad_	),
				.i2so_data1_oe_frompadmisc_	(i2so_data1_oe_padmisc2pad_	),
				.i2so_data2_oe_frompadmisc_	(i2so_data2_oe_padmisc2pad_	),
				.i2so_data3_oe_frompadmisc_	(i2so_data3_oe_padmisc2pad_	),
				.i2so_mclk_frompadmisc		(i2so_mclk_padmisc2pad		),
				.i2so_mclk_oe_frompadmisc_	(i2so_mclk_oe_padmisc2pad_	),
				.i2so_lrck_frompadmisc      (i2so_lrck_padmisc2pad		),
				.i2so_lrck_oe_frompadmisc_	(i2so_lrck_oe_padmisc2pad_	),
				.i2so_bck_frompadmisc       (i2so_bck_padmisc2pad		),
				.i2so_bck_oe_frompadmisc_	(i2so_bck_oe_padmisc2pad_	),

				.j_tdi_frompadmisc			(j_tdi_padmisc2pad			),
				.j_tdo_frompadmisc			(j_tdo_padmisc2pad			),
				.j_tms_frompadmisc			(j_tms_padmisc2pad			),
				.j_rst_frompadmisc_			(j_trst_padmisc2pad_		),
				.j_tclk_frompadmisc			(j_tclk_padmisc2pad			),

                .j_tdi_out2padmisc      	(j_tdi_pad2padmisc          ),
                .j_tms_out2padmisc      	(j_tms_pad2padmisc          ),
                .j_tclk_out2padmisc        	(j_tclk_pad2padmisc      	),
                .j_rst_out2padmisc_        	(j_trst_pad2padmisc_        ),
                .j_tdo_out2padmisc      	(j_tdo_pad2padmisc          ),

				.j_tdi_oe_frompadmisc_		(j_tdi_oe_padmisc2pad_		),
				.j_tdo_oe_frompadmisc_		(j_tdo_oe_padmisc2pad_		),
				.j_tms_oe_frompadmisc_		(j_tms_oe_padmisc2pad_		),
				.j_rst_oe_frompadmisc_		(j_trst_oe_padmisc2pad_		),
				.j_tclk_oe_frompadmisc_		(j_tclk_oe_padmisc2pad_		),

                //spdif
                .spdif_frompadmisc      (spdif_padmisc2pad			),
                .spdif_oe_frompadmisc_  (spdif_oe_padmisc2pad_  	),
				.spdif_out2padmisc		(spdif_pad2padmisc		),

                .p1_ls_mode_fromcore    (p1_ls_mode_core2padmisc             ),
                .p1_txd_fromcore        (p1_txd_core2padmisc                 ),
                .p1_txd_oej_fromcore    (p1_txd_oej_core2padmisc             ),
                .p1_txd_se0_fromcore    (p1_txd_se0_core2padmisc             ),
                .p2_ls_mode_fromcore    (p2_ls_mode_core2padmisc             ),
                .p2_txd_fromcore        (p2_txd_core2padmisc                 ),
                .p2_txd_oej_fromcore    (p2_txd_oej_core2padmisc             ),
                .p2_txd_se0_fromcore    (p2_txd_se0_core2padmisc             ),
     //         .p3_ls_mode_fromcore    (p3_ls_mode  ),
     //         .p3_txd_fromcore        (p3_txd      ),
     //         .p3_txd_oej_fromcore    (p3_txd_oej  ),
     //         .p3_txd_se0_fromcore    (p3_txd_se0  ),
     //         .p4_ls_mode_fromcore    (p4_ls_mode  ),
     //         .p4_txd_fromcore        (p4_txd      ),
     //         .p4_txd_oej_fromcore    (p4_txd_oej  ),
     //         .p4_txd_se0_fromcore    (p4_txd_se0  ),
                .i_p1_rxd_out2core      (i_p1_rxd_padmisc2core               ),
                .i_p1_rxd_se0_out2core  (i_p1_rxd_se0_padmisc2core           ),
                .i_p2_rxd_out2core      (i_p2_rxd_padmisc2core               ),
                .i_p2_rxd_se0_out2core  (i_p2_rxd_se0_padmisc2core           ),
    //          .i_p3_rxd_out2core      (i_p3_rxd     ),
    //          .i_p3_rxd_se0_out2core  (i_p3_rxd_se0 ),
    //          .i_p4_rxd_out2core      (i_p4_rxd     ),
    //          .i_p4_rxd_se0_out2core  (i_p4_rxd_se0 ),
                .usbpon_frompadmisc     (usbpon_padmisc2pad         ),
                .usbpon_oe_frompadmisc_	(usbpon_oe_padmisc2pad_		),
                .usbovc_out2padmisc     (usbovc_pad2padmisc     ),

				.irrx_out2padmisc		(irrx_pad2padmisc		),
				.irrx_frompadmisc		(irrx_padmisc2pad		),
				.irrx_oe_frompadmisc_	(irrx_oe_padmisc2pad_	),

//UART JFR030730
				.uart_tx_frompadmisc	(uart_tx_padmisc2pad	),
				.uart_tx_oe_frompadmisc_(uart_tx_oe_padmisc2pad_),
				.uart_tx_out2padmisc	(uart_tx_pad2padmisc	),

//TV encoder
				.tvenc_iout1_fromcore	(tvenc_iout1_core2pad	),
				.tvenc_iout2_fromcore	(tvenc_iout2_core2pad	),
				.tvenc_iout3_fromcore	(tvenc_iout3_core2pad	),
				.tvenc_iout4_fromcore	(tvenc_iout4_core2pad	),
				.tvenc_iout5_fromcore	(tvenc_iout5_core2pad	),
				.tvenc_iout6_fromcore	(tvenc_iout6_core2pad	),

//added for m3357 JFR030613
				.tvenc_xiref2_fromcore	(tvenc_iref2_core2pad	),
				.tvenc_xiref1_fromcore	(tvenc_iref1_core2pad	),
				.tvenc_xidump_fromcore	(tvenc_idump_core2pad	),	// output from tv encoder
				.tvenc_xiext_fromcore	(tvenc_iext_core2pad	),	// output from tv encoder
				.tvenc_xiext1_fromcore	(tvenc_iext1_core2pad	),	// output from tv encoder

//ADC start
				.xmic1in_out2core		(xmic1in_pad2core		),
				.xadc_vref_fromcore     (xadc_vref_core2pad		),
				.xmic2in_out2core	    (xmic2in_pad2core	    ),
//ADC end

        		// One Crystal Clock ouput
                .f27m_clk_out2pll               (f27m_clk               	),

                //clock
                .ext_mem_dclk_fromclkgen        (ext_mem_clk_gen        	),

                .test_mode_out2padmisc          (test_mode_frompad          ),
//        		.test_sel_out2padmisc			(test_sel_pad2padmisc		),
//        		.warm_rst_frompadmisc			(chipset_rst_padmisc2pad	),       //used to enable(OEN) the only output signals
  		        .bond_option_out2padmisc		(bond_option_pad2padmisc	),
        		.cold_rst_out2padrststrap_		(cold_rst_in_				)
                );
//===========================End of invoke the pad==========================

endmodule
