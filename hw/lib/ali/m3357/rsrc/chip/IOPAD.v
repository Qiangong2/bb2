/*=============================================================================
// TSMC pad select for m3357
// File:        iopad.v
// Author:      Jeffrey Xie
// History:     2003-06-12      initial version, copy from m6304
//				2003-08-15		modified p_d_cs_ to 1 bit width.
//
//				2003-11-26		modified based on spec v1.1
//				2003-12-16		change the spdif to inout type
=============================================================================*/
module  iopad   (
//==================== pad ========================================
// SDRAM Interface (56)
        p_d_clk,         // 1,   SDRAM master clock input
        p_d_addr,       // 12,  SDRAM address
        p_d_baddr,      // 2,   SDRAM bank address
        p_d_dq,         // 32,  SDRAM 32 bits output data pins
        p_d_dqm,        // 4,   SDRAM data mask signal
        p_d_cs_,        // 1,   SDRAM chip select
        p_d_cas_,       // 1,   SDRAM cas#
        p_d_we_,        // 1,   SDRAM we#
        p_d_ras_,       // 1,   SDRAM ras#
// FLASH ROM Interface(3, others share with IDE bus)
        p_rom_ce_,      // 1,   Flash rom chip select
        p_rom_we_,      // 1,   Flash rom write enable
        p_rom_oe_,      // 1,   Flash rom output enable
//added for m3357 JFR030612
        p_rom_data,		// 8 	Flash rom data
//added for m3357 JFR030612
        p_rom_addr,		// 21	Flash rom address

// I2S Audio Interface (7)
        i2so_data0,     // 1,   Serial data for I2S output 0
        i2so_data1,     // 1,   Serial data for I2S output 1
        i2so_data2,     // 1,   Serial data for I2S output 2
        i2so_data3,     // 1,   Serial data for I2S output 3
        i2so_bck,       // 1,   Bit clock for I2S output
        i2so_lrck,      // 1,   Channel clock for I2S output
        i2so_mclk,      // 1,   over sample clock for i2s DAC

//TV encoder Interface (13)
		vd33a_tvdac,	// 1,	Analog Power, input
		gnda_tvdac,		// 1,	Analog GND, input
		gnda1_tvdac,	// 1,	Analog GND, input
		vsud_tvdac,		// 1,	Analog GND, input
		iout1,			// 1,   Analog video output 1
		iout2,			// 1,   Analog video output 2
		iout3,			// 1,   Analog video output 3
		iout4,			// 1,   Analog video output 4
		iout5,			// 1,   Analog video output 5
		iout6,			// 1,   Analog video output 6
		xiref1,			// 1,	connect external via capacitance toward VDDA
		xiref2,			// 1,	connect external via capacitance toward VDDA
		xiext,			// 1,	470 Ohms resistor connect pin, I/O
		xiext1,			// 1,	470 Ohms resistor connect pin, I/O
		xidump,			// 1,	For performance and chip temperature, output

// SPDIF (1)
        spdif,          // 1, output only

//UART port (2)JFR030730
		uart_tx,		// 1

// USB port (6)
        usb2dp,         // 1,   D+ for USB port2
        usb2dn,         // 1,   D- for USB port2
        usb1dp,         // 1,   D+ for USB port1
        usb1dn,         // 1,   D- for USB port1
        usbpon,         // 1,   USB port power control
        usbovc,         // 1,   USB port over current

// Consumer IR Interface (1)
        irrx,           // 1,   consumer infra-red remote controller/wireless keyboard

// General Purpose IO (15~0, 28~24,GPIO)
        gpio,           // 19,  General purpose io

// EJTAG Interface (5)
        p_j_tdo,        // 1,   serial test data output
        p_j_tdi,        // 1,   serial test data input
        p_j_tms,        // 1,   test mode select
        p_j_tclk,       // 1,   test clock
        p_j_rst_,       // 1,   test reset

// system (4)
        p_crst_,        // 1,   cold reset
        test_mode,      // 1,   chip test mode input, (for scan chain)
        p_x27in,        // 1,   crystal input (27 MHz for video output)
        p_x27out,       // 1,   crystal output

// power and ground
		vddcore,		// 		core vdd
		vddio,			//		io vdd
		gnd,			//		ground
		cpu_pllvdd,		//	pll vdd
		cpu_pllvss,		//	pll vss
		f27_pllvdd,		//  pll vdd
		f27_pllvss,		//  pll vss

//ADC (5)
		vd33a_adc	,	// 1,	3.3V Analog VDD for ADC.
		xmic1in     ,   // 1,   ADC microphone input 1.
		xadc_vref   ,   // 1,   ADC reference voltage (refered to the reference circuit).
		xmic2in     ,   // 1,   ADC microphone input 2.
		gnda_adc    ,   // 1,   Analog GND for ADC.
//========================== internal net ===========================
        // SDRAM & FLASH
        ram_addr_frompadmisc,
        ram_addr_oe_frompadmisc_,
        ram_ba_frompadmisc ,
        ram_ba_oe_frompadmisc_,
        ram_dq_frompadmisc ,
        ram_dq_oe_frompadmisc_,
        ram_cas_frompadmisc_,
        ram_cas_oe_frompadmisc_,
        ram_we_frompadmisc_,
        ram_we_oe_frompadmisc_,
        ram_ras_frompadmisc_,
        ram_ras_oe_frompadmisc_,

        ram_dqm_frompadmisc,
        ram_dqm_oe_frompadmisc_,
        ram_cs_frompadmisc_,
        ram_cs_oe_frompadmisc_,

		ram_dq_out2padmisc,
        ram_addr_out2padmisc,   //strap pins
        ram_ba_out2padmisc, 	//strap pins
        ram_dqm_out2padmisc,    //strap pins
        ram_cs_out2padmisc_,	// scan chain
        ram_cas_out2padmisc_,	// scan chain
        // EEPROM
        rom_ce_frompadmisc_,
        rom_oe_frompadmisc_,
        rom_we_frompadmisc_,
//added for m3357 JFR030613
        rom_data_frompadmisc,
        rom_addr_frompadmisc,

        rom_ce_oe_frompadmisc_,
        rom_oe_oe_frompadmisc_,
        rom_we_oe_frompadmisc_,
//added for m3357 JFR030613
        rom_data_oe_frompadmisc_,
        rom_addr_oe_frompadmisc_,

        rom_ce_out2padmisc_,
        rom_oe_out2padmisc_,
        rom_we_out2padmisc_,
//added for m3357 JFR030613
        rom_data_out2padmisc,
        rom_addr_out2padmisc,

//------GPIO
        gpio_frompadmisc        ,
        gpio_oe_frompadmisc_    ,
        gpio_out2padmisc        ,

//       	i2si_data_out2padmisc    	,
		i2so_data0_out2padmisc    	,
		i2so_data1_out2padmisc    	,
		i2so_data2_out2padmisc    	,
		i2so_data3_out2padmisc    	,
		i2so_mclk_out2padmisc      	,
		i2so_bck_out2padmisc		,
		i2so_lrck_out2padmisc		,


//        i2si_data_frompadmisc		,
//		i2si_data_oe_frompadmisc_	,
		i2so_data0_frompadmisc      ,
		i2so_data1_frompadmisc      ,
		i2so_data2_frompadmisc      ,
		i2so_data3_frompadmisc      ,
		i2so_data0_oe_frompadmisc_	,
		i2so_data1_oe_frompadmisc_	,
		i2so_data2_oe_frompadmisc_	,
		i2so_data3_oe_frompadmisc_	,
		i2so_mclk_frompadmisc		,
		i2so_mclk_oe_frompadmisc_	,
		i2so_lrck_frompadmisc      	,
		i2so_lrck_oe_frompadmisc_	,
		i2so_bck_frompadmisc       	,
		i2so_bck_oe_frompadmisc_	,


		j_tdi_frompadmisc		,
		j_tdo_frompadmisc		,
		j_tms_frompadmisc		,
		j_rst_frompadmisc_	    ,
		j_tclk_frompadmisc		,

		j_tdi_out2padmisc       ,
		j_tms_out2padmisc       ,
		j_tclk_out2padmisc      ,
		j_rst_out2padmisc_      ,
		j_tdo_out2padmisc       ,

		j_tdi_oe_frompadmisc_	,
		j_tdo_oe_frompadmisc_	,
		j_tms_oe_frompadmisc_	,
		j_rst_oe_frompadmisc_	,
		j_tclk_oe_frompadmisc_	,


        //spdif
        spdif_frompadmisc,
        spdif_oe_frompadmisc_,
        spdif_out2padmisc,

        p1_ls_mode_fromcore,
        p1_txd_fromcore    ,
        p1_txd_oej_fromcore,
        p1_txd_se0_fromcore,
        p2_ls_mode_fromcore,
        p2_txd_fromcore    ,
        p2_txd_oej_fromcore,
        p2_txd_se0_fromcore,
     //   p3_ls_mode_fromcore,
     //   p3_txd_fromcore    ,
     //   p3_txd_oej_fromcore,
     //   p3_txd_se0_fromcore,
     //   p4_ls_mode_fromcore,
     //   p4_txd_fromcore    ,
     //   p4_txd_oej_fromcore,
     //   p4_txd_se0_fromcore,
        i_p1_rxd_out2core    ,
        i_p1_rxd_se0_out2core,
        i_p2_rxd_out2core    ,
        i_p2_rxd_se0_out2core,
     //   i_p3_rxd_out2core    ,
     //   i_p3_rxd_se0_out2core,
     //   i_p4_rxd_out2core    ,
     //   i_p4_rxd_se0_out2core,
        usbpon_frompadmisc,
        usbpon_oe_frompadmisc_,
        usbovc_out2padmisc,

        irrx_out2padmisc,
        irrx_frompadmisc,
        irrx_oe_frompadmisc_,
//UART JFR030730
		uart_tx_frompadmisc		,
		uart_tx_oe_frompadmisc_ ,
		uart_tx_out2padmisc,

		//TV encoder
		tvenc_iout1_fromcore,
		tvenc_iout2_fromcore,
		tvenc_iout3_fromcore,
		tvenc_iout4_fromcore,
		tvenc_iout5_fromcore,
		tvenc_iout6_fromcore,
		tvenc_xiref1_fromcore,
		tvenc_xiref2_fromcore,		//	The second reference power
		tvenc_xidump_fromcore,
		tvenc_xiext_fromcore,
		tvenc_xiext1_fromcore,		//	The IEXT1 from tv encoder

//ADC start
		xmic1in_out2core	,
		xadc_vref_fromcore,
		xmic2in_out2core	,
//ADC end

        //--crystal
        f27m_clk_out2pll,

        //clock
        ext_mem_dclk_fromclkgen,

        test_mode_out2padmisc,
        bond_option_out2padmisc,
        cold_rst_out2padrststrap_
                );
parameter udly  = 1;
//============define the input/output/inout signals===============
//--------------pad-----------------
//--------------SDRAM Interface (55)
output          p_d_clk;                 // 1,   SDRAM master clock input
inout   [11:0]  p_d_addr;               // 12,  SDRAM address
inout   [1:0]   p_d_baddr;      // 2,   SDRAM bank address
inout   [31:0]  p_d_dq;         // 32,  SDRAM 32 bits output data pins
inout   [3:0]   p_d_dqm;        // 4,   SDRAM data mask signal
//inout   [1:0]   p_d_cs_;        // 2,   SDRAM chip select
inout      		p_d_cs_;        // 1,   SDRAM chip select
inout           p_d_cas_;               // 1,   SDRAM cas#
inout           p_d_we_;                // 1,   SDRAM we#
inout           p_d_ras_;               // 1,   SDRAM ras#
//--------------FLASH ROM Interface(3, others share with IDE bus)
inout           p_rom_ce_;              // 1,   Flash rom chip select
inout           p_rom_we_;              // 1,   Flash rom write enable
inout           p_rom_oe_;              // 1,   Flash rom output enable
inout  [7:0]    p_rom_data;             // 8,   Flash rom data
inout  [20:0]   p_rom_addr;             // 21,   Flash rom address
//--------------I2S Audio Interface (8)
inout           i2so_data0;             // 1,   Serial data for I2S output
inout 			i2so_data1;     		// 1,   Serial data for I2S output 1
inout   	    i2so_data2;     		// 1,   Serial data for I2S output 2
inout       	i2so_data3;		     	// 1,   Serial data for I2S output 3
inout           i2so_bck;               // 1,   Bit clock for I2S output
inout           i2so_lrck;              // 1,   Channel clock for I2S output
inout           i2so_mclk;              // 1,   over sample clock for i2s DAC
//input           i2si_data;              // 1,   serial data for I2S input

//TV encoder Interface (13)
input			vd33a_tvdac;	// 1,	Analog Power, input
input			gnda_tvdac;		// 1,	Analog GND, input
input			gnda1_tvdac;	// 1,	Analog GND, input
input			vsud_tvdac;		// 1,	Analog GND, input
output			iout1;			// 1,   Analog video output 1
output			iout2;			// 1,   Analog video output 2
output			iout3;			// 1,   Analog video output 3
output			iout4;			// 1,   Analog video output 4
output			iout5;			// 1,   Analog video output 5
output			iout6;			// 1,   Analog video output 6
output			xiref1;			// 1,	connect external via capacitance toward VDDA
output			xiref2;			// 1,	connect external via capacitance toward VDDA
output			xiext;			// 1,	470 Ohms resistor connect pin, I/O
output			xiext1;			// 1,	470 Ohms resistor connect pin, I/O
output			xidump;			// 1,	For performance and chip temperature, output

//--------------SPDIF (1)
inout			spdif;                  // 1, output

//-------------- UART (1)
output			uart_tx;		// 1

//--------------USB port (6)
//inout           usb4dp;                 // 1,   D+ for USB port4
//inout           usb4dn;                 // 1,   D- for USB port4
//inout           usb3dp;                 // 1,   D+ for USB port3
//inout           usb3dn;                 // 1,   D- for USB port3
inout           usb2dp;                 // 1,   D+ for USB port2
inout           usb2dn;                 // 1,   D- for USB port2
inout           usb1dp;                 // 1,   D+ for USB port1
inout           usb1dn;                 // 1,   D- for USB port1
output          usbpon;                 // 1,   USB port power control
input           usbovc;                 // 1,   USB port over current

//--------------Consumer IR Interface (1)
input           irrx;                   // 1,   consumer infra-red remote controller/wireless keyboard

//--------------General Purpose IO (13~0, 28~24,GPIO)
inout   [28:0]  gpio;                   // 19,  General purpose io

//--------------EJTAG Interface (5)
output          p_j_tdo;                // 1,   serial test data output
input           p_j_tdi;                // 1,   serial test data input
input           p_j_tms;                // 1,   test mode select
input           p_j_tclk;               // 1,   test clock
input           p_j_rst_;               // 1,   test reset

//--------------system (5)
input           p_crst_;            // 1,   cold reset
input           test_mode;          // 1,   chip test mode input for scan
//input			test_sel;			// 1,	select the scan chain port
//input           p_x48in;          // 1,   crystal input (48 MHz for USB)
//output          p_x48out;         // 1,   crystal output
input           p_x27in;            // 1,   crystal input (27 MHz for video output)
output          p_x27out;           // 1,   crystal output
// power and ground
input			vddcore;			//	core vdd
//input			vsscore;			//	core vss
input			vddio;				//	io vdd
input			gnd;				//	ground
input			cpu_pllvdd;			//	pll vdd
input			cpu_pllvss;			//	pll vss
input			f27_pllvdd;			//	pll vdd for f27 pll
input			f27_pllvss;			// l vss for f27 pll

//--------------ADC (5)
input			vd33a_adc	;		// 1,	3.3V Analog VDD for ADC.
input			xmic1in     ;       // 1,   ADC microphone input 1.
output			xadc_vref   ;       // 1,   ADC reference voltage (refered to the reference circuit).
input			xmic2in     ;       // 1,   ADC microphone input 2.
input			gnda_adc    ;		// 1,   Analog GND for ADC.
//--------------- internal net --------------------------------
// SDRAM & FLASH
input   [11:0]  ram_addr_frompadmisc;
input	[11:0]	ram_addr_oe_frompadmisc_;
input   [1:0]   ram_ba_frompadmisc;
input	[1:0]	ram_ba_oe_frompadmisc_;
input   [31:0]  ram_dq_frompadmisc;
input	[31:0]	ram_dq_oe_frompadmisc_;
input           ram_cas_frompadmisc_,
				ram_cas_oe_frompadmisc_,
                ram_we_frompadmisc_,
                ram_we_oe_frompadmisc_,
                ram_ras_frompadmisc_,
                ram_ras_oe_frompadmisc_;
input			ram_cs_frompadmisc_;//JFR 030704
input			ram_cs_oe_frompadmisc_;//JFR 030704
input   [3:0]   ram_dqm_frompadmisc;
input	[3:0]	ram_dqm_oe_frompadmisc_;

output  [31:0]  ram_dq_out2padmisc;
output  [11:0]  ram_addr_out2padmisc;   //strap pins
output  [1:0]   ram_ba_out2padmisc; 	//strap pins
output  [3:0]   ram_dqm_out2padmisc;    //strap pins
output			ram_cs_out2padmisc_;	// scan chain JFR 030704
output			ram_cas_out2padmisc_;	// scan chain

// EEPROM
input           rom_ce_frompadmisc_;
input           rom_oe_frompadmisc_;
input           rom_we_frompadmisc_;
//added for m3357 JFR030613
input	[7:0]   rom_data_frompadmisc;
input	[20:0]  rom_addr_frompadmisc;
input	        rom_ce_oe_frompadmisc_;
input	        rom_oe_oe_frompadmisc_;
input	        rom_we_oe_frompadmisc_;
//added for m3357 JFR030613
input   [7:0] 	rom_data_oe_frompadmisc_;
input   [20:0]	rom_addr_oe_frompadmisc_;
output			rom_ce_out2padmisc_;
output  	    rom_oe_out2padmisc_;
output	        rom_we_out2padmisc_;
//added for m3357 JFR030613
output  [7:0]   rom_data_out2padmisc;
output  [20:0]  rom_addr_out2padmisc;

//------GPIO
input   [28:0]  gpio_frompadmisc,
                gpio_oe_frompadmisc_;
output  [28:0]  gpio_out2padmisc;

input			j_tdi_frompadmisc		;
input			j_tdo_frompadmisc		;
input			j_tms_frompadmisc		;
input			j_rst_frompadmisc_	    ;
input			j_tclk_frompadmisc		;

output			j_tdi_out2padmisc       ;
output			j_tms_out2padmisc       ;
output			j_tclk_out2padmisc      ;
output			j_rst_out2padmisc_      ;
output			j_tdo_out2padmisc       ;

input			j_tdi_oe_frompadmisc_	;
input			j_tdo_oe_frompadmisc_	;
input			j_tms_oe_frompadmisc_	;
input			j_rst_oe_frompadmisc_	;
input			j_tclk_oe_frompadmisc_	;


//output          i2si_data_out2padmisc    	;
output          i2so_data0_out2padmisc    	;
output          i2so_data1_out2padmisc    	;
output          i2so_data2_out2padmisc    	;
output          i2so_data3_out2padmisc    	;
output          i2so_mclk_out2padmisc      	;
output	        i2so_bck_out2padmisc		;
output	        i2so_lrck_out2padmisc		;

//input			i2si_data_frompadmisc		;
//input			i2si_data_oe_frompadmisc_	;
input           i2so_data0_frompadmisc      ;
input           i2so_data1_frompadmisc      ;
input           i2so_data2_frompadmisc      ;
input           i2so_data3_frompadmisc      ;
input			i2so_data0_oe_frompadmisc_	;
input			i2so_data1_oe_frompadmisc_	;
input			i2so_data2_oe_frompadmisc_	;
input			i2so_data3_oe_frompadmisc_	;
input			i2so_mclk_frompadmisc		;
input			i2so_mclk_oe_frompadmisc_	;
input           i2so_lrck_frompadmisc      	;
input			i2so_lrck_oe_frompadmisc_	;
input           i2so_bck_frompadmisc       	;
input	        i2so_bck_oe_frompadmisc_	;

//spdif
input       	spdif_frompadmisc;
input       	spdif_oe_frompadmisc_;
output      	spdif_out2padmisc;

input           p1_ls_mode_fromcore     ,
                p1_txd_fromcore         ,
                p1_txd_oej_fromcore     ,
                p1_txd_se0_fromcore     ,
                p2_ls_mode_fromcore     ,
                p2_txd_fromcore         ,
                p2_txd_oej_fromcore     ,
                p2_txd_se0_fromcore     ;
      //          p3_ls_mode_fromcore     ,
      //          p3_txd_fromcore         ,
      //          p3_txd_oej_fromcore     ,
      //          p3_txd_se0_fromcore     ,
      //          p4_ls_mode_fromcore     ,
      //          p4_txd_fromcore         ,
      //          p4_txd_oej_fromcore     ,
      //          p4_txd_se0_fromcore     ;
output          i_p1_rxd_out2core       ,
                i_p1_rxd_se0_out2core   ,
                i_p2_rxd_out2core       ,
                i_p2_rxd_se0_out2core   ;
      //          i_p3_rxd_out2core       ,
      //          i_p3_rxd_se0_out2core   ,
      //          i_p4_rxd_out2core       ,
      //          i_p4_rxd_se0_out2core   ;
input           usbpon_frompadmisc;
input			usbpon_oe_frompadmisc_;
output          usbovc_out2padmisc;

output          irrx_out2padmisc;
input			irrx_frompadmisc,
        		irrx_oe_frompadmisc_;

//UART JFR030730
input			uart_tx_frompadmisc		;
input			uart_tx_oe_frompadmisc_ ;
output			uart_tx_out2padmisc;
input           ext_mem_dclk_fromclkgen    ;
//input           pci_mbus_clk    ;

output          cold_rst_out2padrststrap_    ;

//TV encoder
//input			tvenc_iout0_fromcore;
input			tvenc_iout1_fromcore;
input			tvenc_iout2_fromcore;
input			tvenc_iout3_fromcore;
input			tvenc_iout4_fromcore;
input			tvenc_iout5_fromcore;
input			tvenc_iout6_fromcore;
//aeede for m3357 JFR 030613
input			tvenc_xiref1_fromcore;
input			tvenc_xiref2_fromcore;		//	The second reference power
input			tvenc_xidump_fromcore;
input			tvenc_xiext_fromcore;
input			tvenc_xiext1_fromcore;		//	The IEXT1 from tv encoder

output		xmic1in_out2core	;
input		xadc_vref_fromcore;
output		xmic2in_out2core	;

output          f27m_clk_out2pll;
//input           pci_clk_oe_frompadmisc_;
output          test_mode_out2padmisc;
//output			test_sel_out2padmisc;
output			bond_option_out2padmisc;
//input           warm_rst_frompadmisc;
//================================================================

wire N0001, N0002;
/*================================================================
	PAD list
================================================================*/
    PVSS3DGZ	pin1	(.VSS(gnd),	.VD33(vddio)  );	//	 ground
    PVSS3DGZ	pin1_1	(.VSS(gnd),	.VD33(vddio)  );	//	 ground
    PVSS3DGZ	pin1_2	(.VSS(gnd),	.VD33(vddio)  );	//	 ground

	PDB04DGZ	pin2	(.I(spdif_frompadmisc),		.OEN(spdif_oe_frompadmisc_), 		.PAD(spdif),		.C(spdif_out2padmisc),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PDB04DGZ	pin3	(.I(i2so_data3_frompadmisc),	.OEN(i2so_data3_oe_frompadmisc_), 	.PAD(i2so_data3), 	.C(i2so_data3_out2padmisc),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PDB04DGZ	pin4	(.I(i2so_data2_frompadmisc),	.OEN(i2so_data2_oe_frompadmisc_), 	.PAD(i2so_data2), 	.C(i2so_data2_out2padmisc),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PDB04DGZ	pin5	(.I(i2so_data1_frompadmisc),	.OEN(i2so_data1_oe_frompadmisc_), 	.PAD(i2so_data1), 	.C(i2so_data1_out2padmisc),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PDB04DGZ	pin6	(.I(i2so_data0_frompadmisc),	.OEN(i2so_data0_oe_frompadmisc_), 	.PAD(i2so_data0), .C(i2so_data0_out2padmisc),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PDB08DGZ	pin7	(.I(i2so_mclk_frompadmisc),		.OEN(i2so_mclk_oe_frompadmisc_),	.PAD(i2so_mclk),	.C(i2so_mclk_out2padmisc),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PVDD2DGZ	pin8	(.VD33	(vddio),	.VSSPST	(gnd));//	IO power
	PVDD2DGZ	pin8_1	(.VD33	(vddio),	.VSSPST	(gnd));//	IO power
	PVDD2DGZ	pin8_2	(.VD33	(vddio),	.VSSPST	(gnd));//	IO power

	PDB04DGZ	pin9	(.I(i2so_bck_frompadmisc),		.OEN(i2so_bck_oe_frompadmisc_), 	.PAD(i2so_bck), 	.C(i2so_bck_out2padmisc),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PDB04DGZ	pin10	(.I(i2so_lrck_frompadmisc),		.OEN(i2so_lrck_oe_frompadmisc_),	.PAD(i2so_lrck), 	.C(i2so_lrck_out2padmisc),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	TIEHI U_PAD_0001 	( .Y(N0001) );
	TIELO U_PAD_0002 	( .Y(N0002) );

	PVDD1DGZ	pin11	(.VDD(vddcore),	.VD33(vddio), .VSSPST(gnd));//	Core power
    PVDD1DGZ	pin11_1	(.VDD(vddcore),	.VD33(vddio), .VSSPST(gnd));//	Core power
    PVDD1DGZ	pin11_2	(.VDD(vddcore),	.VD33(vddio), .VSSPST(gnd));//	Core power

	usb_pad 	pin12_pin13
					( .A(p1_txd_fromcore), .ANT(N0001), .DM(usb1dn), .DP(usb1dp),
                      .H_LB(p1_ls_mode_fromcore), .OEB(p1_txd_oej_fromcore),
                      .RCVD(i_p1_rxd_out2core), .SE0(p1_txd_se0_fromcore),
                      .SE0D(i_p1_rxd_se0_out2core)
                	);

	PDUDGZ		pin14	(.PAD(usbovc),	.C(usbovc_out2padmisc), .VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PDB04DGZ	pin15	(.I(usbpon_frompadmisc),	.OEN(N0002),	.PAD(usbpon),	.C(),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	usb_pad 	pin16_pin17
					( .A(p2_txd_fromcore), .ANT(N0001), .DM(usb2dn), .DP(usb2dp),
                      .H_LB(p2_ls_mode_fromcore), .OEB(p2_txd_oej_fromcore),
                      .RCVD(i_p2_rxd_out2core), .SE0(p2_txd_se0_fromcore),
                      .SE0D(i_p2_rxd_se0_out2core)
                	);

    PVSS3DGZ	pin18		(.VSS(gnd),	.VD33(vddio)  );	//	 ground
    PVSS3DGZ	pin18_1		(.VSS(gnd),	.VD33(vddio)  );	//	 ground
    PVSS3DGZ	pin18_2		(.VSS(gnd),	.VD33(vddio)  );	//	 ground

	PRB08DGZ	pin19		(.I(gpio_frompadmisc[7]),	.OEN(gpio_oe_frompadmisc_[7]), .PAD(gpio[7]), .C(gpio_out2padmisc[7]),
								.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PRB12DGZ	pin20		(.I(gpio_frompadmisc[8]),	.OEN(gpio_oe_frompadmisc_[8]), 	.PAD(gpio[8]), .C(gpio_out2padmisc[8]),
								.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PRB12DGZ	pin21		(.I(gpio_frompadmisc[9]),	.OEN(gpio_oe_frompadmisc_[9]), 	.PAD(gpio[9]), .C(gpio_out2padmisc[9]),
								.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PRB12DGZ	pin22		(.I(gpio_frompadmisc[10]),	.OEN(gpio_oe_frompadmisc_[10]), .PAD(gpio[10]), .C(gpio_out2padmisc[10]),
								.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PRB12DGZ	pin23		(.I(gpio_frompadmisc[11]),	.OEN(gpio_oe_frompadmisc_[11]), .PAD(gpio[11]), .C(gpio_out2padmisc[11]),
								.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PRB12DGZ	pin24		(.I(gpio_frompadmisc[12]),	.OEN(gpio_oe_frompadmisc_[12]), .PAD(gpio[12]), .C(gpio_out2padmisc[12]),
								.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PVDD1DGZ	pin25		(.VDD(vddcore),	.VD33(vddio), .VSSPST(gnd));//	Core power
    PVDD1DGZ	pin25_1		(.VDD(vddcore),	.VD33(vddio), .VSSPST(gnd));//	Core power
    PVDD1DGZ	pin25_2		(.VDD(vddcore),	.VD33(vddio), .VSSPST(gnd));//	Core power

	PRB12DGZ	pin26		(.I(gpio_frompadmisc[13]),	.OEN(gpio_oe_frompadmisc_[13]), .PAD(gpio[13]), .C(gpio_out2padmisc[13]),
								.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PRB12DGZ	pin27		(.I(gpio_frompadmisc[6]),	.OEN(gpio_oe_frompadmisc_[6]), .PAD(gpio[6]), .C(gpio_out2padmisc[6]),
								.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PRB12DGZ	pin28		(.I(gpio_frompadmisc[5]),	.OEN(gpio_oe_frompadmisc_[5]), .PAD(gpio[5]), .C(gpio_out2padmisc[5]),
								.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PDB24DGZ	pin29		(.I(gpio_frompadmisc[4]),	.OEN(gpio_oe_frompadmisc_[4]), .PAD(gpio[4]), .C(gpio_out2padmisc[4]),
								.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

    PVSS3DGZ	pin30		(.VSS(gnd),	.VD33(vddio)  );	//	 ground
    PVSS3DGZ	pin30_1		(.VSS(gnd),	.VD33(vddio)  );	//	 ground
    PVSS3DGZ	pin30_2		(.VSS(gnd),	.VD33(vddio)  );	//	 ground

	PVDD1DGZ	pin101		(.VDD(vddcore),	.VD33(vddio), .VSSPST(gnd));//	Core power
    PVDD1DGZ	pin101_1	(.VDD(vddcore),	.VD33(vddio), .VSSPST(gnd));//	Core power
    PVDD1DGZ	pin101_2	(.VDD(vddcore),	.VD33(vddio), .VSSPST(gnd));//	Core power

	PRB12DGZ	pin102		(.I(rom_data_frompadmisc[0]),		.OEN(rom_data_oe_frompadmisc_[0]),	.PAD(p_rom_data[0]),	.C(rom_data_out2padmisc[0]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PRB12DGZ	pin103		(.I(rom_data_frompadmisc[1]),		.OEN(rom_data_oe_frompadmisc_[1]),	.PAD(p_rom_data[1]),	.C(rom_data_out2padmisc[1]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PRB12DGZ	pin104		(.I(rom_data_frompadmisc[2]),		.OEN(rom_data_oe_frompadmisc_[2]),	.PAD(p_rom_data[2]),	.C(rom_data_out2padmisc[2]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PRB12DGZ	pin105		(.I(rom_data_frompadmisc[3]),		.OEN(rom_data_oe_frompadmisc_[3]),	.PAD(p_rom_data[3]),	.C(rom_data_out2padmisc[3]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PRB12DGZ	pin106		(.I(rom_data_frompadmisc[4]),		.OEN(rom_data_oe_frompadmisc_[4]),	.PAD(p_rom_data[4]),	.C(rom_data_out2padmisc[4]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PRB12DGZ	pin107		(.I(rom_data_frompadmisc[5]),		.OEN(rom_data_oe_frompadmisc_[5]),	.PAD(p_rom_data[5]),	.C(rom_data_out2padmisc[5]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PRB12DGZ	pin108		(.I(rom_data_frompadmisc[6]),		.OEN(rom_data_oe_frompadmisc_[6]),	.PAD(p_rom_data[6]),	.C(rom_data_out2padmisc[6]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PRB12DGZ	pin109		(.I(rom_data_frompadmisc[7]),		.OEN(rom_data_oe_frompadmisc_[7]),	.PAD(p_rom_data[7]),	.C(rom_data_out2padmisc[7]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

    PVSS3DGZ	pin110		(.VSS(gnd),	.VD33(vddio)  );	//	 ground
    PVSS3DGZ	pin110_1	(.VSS(gnd),	.VD33(vddio)  );	//	 ground
    PVSS3DGZ	pin110_2	(.VSS(gnd),	.VD33(vddio)  );	//	 ground

	PRB08DGZ	pin111		(.I(rom_oe_frompadmisc_),	.OEN(rom_oe_oe_frompadmisc_),	.PAD(p_rom_oe_),	.C(rom_oe_out2padmisc_),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PRB08DGZ	pin112		(.I(rom_we_frompadmisc_),	.OEN(rom_we_oe_frompadmisc_),	.PAD(p_rom_we_),	.C(rom_we_out2padmisc_),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PRB08DGZ	pin113		(.I(rom_ce_frompadmisc_),	.OEN(rom_ce_oe_frompadmisc_),	.PAD(p_rom_ce_),	.C(rom_ce_out2padmisc_),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PRB12DGZ	pin114		(.I(rom_addr_frompadmisc[20]),		.OEN(rom_addr_oe_frompadmisc_[20]),  	.PAD(p_rom_addr[20]),  	.C(rom_addr_out2padmisc[20]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PRB12DGZ	pin115		(.I(rom_addr_frompadmisc[19]),		.OEN(rom_addr_oe_frompadmisc_[19]),  	.PAD(p_rom_addr[19]),  	.C(rom_addr_out2padmisc[19]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PRB12DGZ	pin116		(.I(rom_addr_frompadmisc[18]),		.OEN(rom_addr_oe_frompadmisc_[18]),  	.PAD(p_rom_addr[18]),  	.C(rom_addr_out2padmisc[18]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PRB12DGZ	pin117		(.I(rom_addr_frompadmisc[17]),		.OEN(rom_addr_oe_frompadmisc_[17]),  	.PAD(p_rom_addr[17]),  	.C(rom_addr_out2padmisc[17]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PVDD1DGZ	pin118		(.VDD(vddcore),	.VD33(vddio), .VSSPST(gnd));//	Core power
    PVDD1DGZ	pin118_1	(.VDD(vddcore),	.VD33(vddio), .VSSPST(gnd));//	Core power
    PVDD1DGZ	pin118_2	(.VDD(vddcore),	.VD33(vddio), .VSSPST(gnd));//	Core power

	PRB12DGZ	pin119		(.I(rom_addr_frompadmisc[16]),		.OEN(rom_addr_oe_frompadmisc_[16]),  	.PAD(p_rom_addr[16]),  	.C(rom_addr_out2padmisc[16]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PRB12DGZ	pin120		(.I(rom_addr_frompadmisc[15]),		.OEN(rom_addr_oe_frompadmisc_[15]),  	.PAD(p_rom_addr[15]),  	.C(rom_addr_out2padmisc[15]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PRB12DGZ	pin121		(.I(rom_addr_frompadmisc[14]),		.OEN(rom_addr_oe_frompadmisc_[14]),  	.PAD(p_rom_addr[14]),  	.C(rom_addr_out2padmisc[14]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PRB12DGZ	pin122		(.I(rom_addr_frompadmisc[13]),		.OEN(rom_addr_oe_frompadmisc_[13]),  	.PAD(p_rom_addr[13]),  	.C(rom_addr_out2padmisc[13]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PVDD2DGZ	pin123		(.VD33	(vddio),	.VSSPST	(gnd));//	IO power
	PVDD2DGZ	pin123_1	(.VD33	(vddio),	.VSSPST	(gnd));//	IO power

	PDDDGZ		pin123_2	(.PAD(vddio),	.C(bond_option_out2padmisc), .VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PRB12DGZ	pin124		(.I(rom_addr_frompadmisc[12]),		.OEN(rom_addr_oe_frompadmisc_[12]),		.PAD(p_rom_addr[12]),	.C(rom_addr_out2padmisc[12]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PRB12DGZ	pin125		(.I(rom_addr_frompadmisc[11]),		.OEN(rom_addr_oe_frompadmisc_[11]),		.PAD(p_rom_addr[11]),	.C(rom_addr_out2padmisc[11]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PRB12DGZ	pin126		(.I(rom_addr_frompadmisc[10]),		.OEN(rom_addr_oe_frompadmisc_[10]),		.PAD(p_rom_addr[10]),	.C(rom_addr_out2padmisc[10]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PRB12DGZ	pin127		(.I(rom_addr_frompadmisc[9]),		.OEN(rom_addr_oe_frompadmisc_[9]),		.PAD(p_rom_addr[9]),	.C(rom_addr_out2padmisc[9]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PRB12DGZ	pin128		(.I(rom_addr_frompadmisc[8]),		.OEN(rom_addr_oe_frompadmisc_[8]),		.PAD(p_rom_addr[8]),	.C(rom_addr_out2padmisc[8]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PRB12DGZ	pin129		(.I(rom_addr_frompadmisc[7]),		.OEN(rom_addr_oe_frompadmisc_[7]),		.PAD(p_rom_addr[7]),	.C(rom_addr_out2padmisc[7]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PRB12DGZ	pin130		(.I(rom_addr_frompadmisc[6]),		.OEN(rom_addr_oe_frompadmisc_[6]),		.PAD(p_rom_addr[6]),	.C(rom_addr_out2padmisc[6]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PRB12DGZ	pin131		(.I(rom_addr_frompadmisc[5]),		.OEN(rom_addr_oe_frompadmisc_[5]),		.PAD(p_rom_addr[5]),	.C(rom_addr_out2padmisc[5]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

    PVSS3DGZ	pin132		(.VSS(gnd),	.VD33(vddio)  );	//	 ground
    PVSS3DGZ	pin132_1	(.VSS(gnd),	.VD33(vddio)  );	//	 ground
    PVSS3DGZ	pin132_2	(.VSS(gnd),	.VD33(vddio)  );	//	 ground

	PRB12DGZ	pin133		(.I(rom_addr_frompadmisc[4]),		.OEN(rom_addr_oe_frompadmisc_[4]),		.PAD(p_rom_addr[4]),	.C(rom_addr_out2padmisc[4]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PRB12DGZ	pin134		(.I(rom_addr_frompadmisc[3]),		.OEN(rom_addr_oe_frompadmisc_[3]),		.PAD(p_rom_addr[3]),	.C(rom_addr_out2padmisc[3]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PRB12DGZ	pin135		(.I(rom_addr_frompadmisc[2]),		.OEN(rom_addr_oe_frompadmisc_[2]),		.PAD(p_rom_addr[2]),	.C(rom_addr_out2padmisc[2]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PVDD1DGZ	pin136		(.VDD(vddcore),	.VD33(vddio), .VSSPST(gnd));//	Core power
    PVDD1DGZ	pin136_1	(.VDD(vddcore),	.VD33(vddio), .VSSPST(gnd));//	Core power
    PVDD1DGZ	pin136_2	(.VDD(vddcore),	.VD33(vddio), .VSSPST(gnd));//	Core power

	PRB12DGZ	pin137		(.I(rom_addr_frompadmisc[1]),		.OEN(rom_addr_oe_frompadmisc_[1]),		.PAD(p_rom_addr[1]),	.C(rom_addr_out2padmisc[1]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PRB12DGZ	pin138		(.I(rom_addr_frompadmisc[0]),		.OEN(rom_addr_oe_frompadmisc_[0]),		.PAD(p_rom_addr[0]),	.C(rom_addr_out2padmisc[0]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

 	PRB12DGZ	pin139		(.I(uart_tx_frompadmisc), 	.OEN(uart_tx_oe_frompadmisc_), 	.PAD(uart_tx),	.C(uart_tx_out2padmisc),
 							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PRB08DGZ	pin140		(.I(irrx_frompadmisc), 		.OEN(irrx_oe_frompadmisc_), 	.PAD(irrx),		.C(irrx_out2padmisc),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PDDDGZ		pin141		(.PAD(test_mode),	.C(test_mode_out2padmisc), .VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));
	PDUDGZ		pin142		(.PAD(p_crst_),		.C(cold_rst_out2padrststrap_), .VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

    PVSS3DGZ	pin143		(.VSS(gnd),	.VD33(vddio)  );	//	 ground
    PVSS3DGZ	pin143_1	(.VSS(gnd),	.VD33(vddio)  );	//	 ground
    PVSS3DGZ	pin143_2	(.VSS(gnd),	.VD33(vddio)  );	//	 ground

	PDXO03DG	pin144_pin145	(.XIN(p_x27in),	.XOUT(p_x27out),	.XC(f27m_clk_out2pll), .VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	//	Power cut between digital power and PLL power
	PRDIODE		PCUT_F27_PLL_1	(.VDD1(f27_pllvdd), .VDD2(vddio), .VSS1(f27_pllvss), .VSS2(gnd));

	PVDD3P		pin146		(.TAVDD(f27_pllvdd), .TAVSS(f27_pllvss) );//F27M_PLL analog power
	PVDD3P		pin146_1	(.TAVDD(f27_pllvdd), .TAVSS(f27_pllvss) );//F27M_PLL analog power

	PVSS3P		pin147		(.TAVDD(f27_pllvdd), .TAVSS(f27_pllvss) );//F27M_PLL analog ground
	PVSS3P		pin147_1	(.TAVDD(f27_pllvdd), .TAVSS(f27_pllvss) );//F27M_PLL analog ground

	//	Power cut between F27_PLL and the CPU_PLL power
	PRDIODE		PCUT_F27_PLL_2	(.VDD1(f27_pllvdd), .VDD2(cpu_pllvdd),	.VSS1(f27_pllvss), .VSS2(cpu_pllvss));

	//	Power cut between F27_PLL and the CPU_PLL power
	PRDIODE		PCUT_CPU_PLL_1	(.VDD1(f27_pllvdd), .VDD2(cpu_pllvdd),	.VSS1(f27_pllvss), .VSS2(cpu_pllvss));

	PVDD3P		pin148		(.TAVDD(cpu_pllvdd), .TAVSS(cpu_pllvss) );//CPU_PLL analog power
	PVDD3P		pin148_1	(.TAVDD(cpu_pllvdd), .TAVSS(cpu_pllvss) );//CPU_PLL analog power

	PVSS3P		pin149		(.TAVDD(cpu_pllvdd), .TAVSS(cpu_pllvss) );//CPU_PLL analog ground
	PVSS3P		pin149_1	(.TAVDD(cpu_pllvdd), .TAVSS(cpu_pllvss) );//CPU_PLL analog ground

	//	Power cut between the digital power and CPU_PLL power
	PRDIODE		PCUT_CPU_PLL_2	(.VDD1(cpu_pllvdd), .VDD2(vddio), .VSS1(cpu_pllvss), .VSS2(gnd));

	PRB12DGZ	pin150		(.I(gpio_frompadmisc[14]),	.OEN(gpio_oe_frompadmisc_[14]),	.PAD(gpio[14]),	.C(gpio_out2padmisc[14]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PRB12DGZ	pin151		(.I(gpio_frompadmisc[15]),	.OEN(gpio_oe_frompadmisc_[15]),	.PAD(gpio[15]),	.C(gpio_out2padmisc[15]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PRB12DGZ	pin152		(.I(gpio_frompadmisc[24]),	.OEN(gpio_oe_frompadmisc_[24]),	.PAD(gpio[24]),	.C(gpio_out2padmisc[24]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PVDD2DGZ	pin153		(.VD33	(vddio),	.VSSPST	(gnd));//	IO power
	PVDD2DGZ	pin153_1	(.VD33	(vddio),	.VSSPST	(gnd));//	IO power
	PVDD2DGZ	pin153_2	(.VD33	(vddio),	.VSSPST	(gnd));//	IO power

	PRB12DGZ	pin154		(.I(gpio_frompadmisc[25]),	.OEN(gpio_oe_frompadmisc_[25]),	.PAD(gpio[25]),	.C(gpio_out2padmisc[25]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PRB12DGZ	pin155		(.I(gpio_frompadmisc[26]),	.OEN(gpio_oe_frompadmisc_[26]),	.PAD(gpio[26]),	.C(gpio_out2padmisc[26]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PRB12DGZ	pin156		(.I(gpio_frompadmisc[27]),	.OEN(gpio_oe_frompadmisc_[27]),	.PAD(gpio[27]),	.C(gpio_out2padmisc[27]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PRB12DGZ	pin157		(.I(gpio_frompadmisc[28]),	.OEN(gpio_oe_frompadmisc_[28]),	.PAD(gpio[28]),	.C(gpio_out2padmisc[28]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PVDD1DGZ	pin158		(.VDD(vddcore),	.VD33(vddio), .VSSPST(gnd));//	Core power
    PVDD1DGZ	pin158_1	(.VDD(vddcore),	.VD33(vddio), .VSSPST(gnd));//	Core power
    PVDD1DGZ	pin158_2	(.VDD(vddcore),	.VD33(vddio), .VSSPST(gnd));//	Core power

	PRB12DGZ	pin159		(.I(gpio_frompadmisc[3]),	.OEN(gpio_oe_frompadmisc_[3]), .PAD(gpio[3]), .C(gpio_out2padmisc[3]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PRB08DGZ	pin160		(.I(gpio_frompadmisc[2]),	.OEN(gpio_oe_frompadmisc_[2]), .PAD(gpio[2]), .C(gpio_out2padmisc[2]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PRB08DGZ	pin161		(.I(gpio_frompadmisc[1]),	.OEN(gpio_oe_frompadmisc_[1]), .PAD(gpio[1]), .C(gpio_out2padmisc[1]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));


	PRB08DGZ	pin162		(.I(gpio_frompadmisc[0]),	.OEN(gpio_oe_frompadmisc_[0]), .PAD(gpio[0]), .C(gpio_out2padmisc[0]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PVDD2DGZ	pin163		(.VD33	(vddio),	.VSSPST	(gnd));//	IO power
	PVDD2DGZ	pin163_1	(.VD33	(vddio),	.VSSPST	(gnd));//	IO power
	PVDD2DGZ	pin163_2	(.VD33	(vddio),	.VSSPST	(gnd));//	IO power

	PDD12DGZ	pin164		(.I(ram_dq_frompadmisc[7]),		.OEN(ram_dq_oe_frompadmisc_[7]),	.PAD(p_d_dq[7]),	.C(ram_dq_out2padmisc[7]),
								.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PDD12DGZ	pin165		(.I(ram_dq_frompadmisc[6]),		.OEN(ram_dq_oe_frompadmisc_[6]),	.PAD(p_d_dq[6]),	.C(ram_dq_out2padmisc[6]),
								.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PDD12DGZ	pin166		(.I(ram_dq_frompadmisc[5]),		.OEN(ram_dq_oe_frompadmisc_[5]),	.PAD(p_d_dq[5]),	.C(ram_dq_out2padmisc[5]),
								.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PDD12DGZ	pin167		(.I(ram_dq_frompadmisc[4]),		.OEN(ram_dq_oe_frompadmisc_[4]),	.PAD(p_d_dq[4]),	.C(ram_dq_out2padmisc[4]),
								.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

    PVSS3DGZ	pin168		(.VSS(gnd),	.VD33(vddio)  );	//	 ground
    PVSS3DGZ	pin168_1	(.VSS(gnd),	.VD33(vddio)  );	//	 ground
    PVSS3DGZ	pin168_2	(.VSS(gnd),	.VD33(vddio)  );	//	 ground

	PDD12DGZ	pin169		(.I(ram_dq_frompadmisc[3]),		.OEN(ram_dq_oe_frompadmisc_[3]),	.PAD(p_d_dq[3]),	.C(ram_dq_out2padmisc[3]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PDD12DGZ	pin170		(.I(ram_dq_frompadmisc[2]),		.OEN(ram_dq_oe_frompadmisc_[2]),	.PAD(p_d_dq[2]),	.C(ram_dq_out2padmisc[2]),
								.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PDD12DGZ	pin171		(.I(ram_dq_frompadmisc[1]),		.OEN(ram_dq_oe_frompadmisc_[1]),	.PAD(p_d_dq[1]),	.C(ram_dq_out2padmisc[1]),
								.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PDD12DGZ	pin172		(.I(ram_dq_frompadmisc[0]),		.OEN(ram_dq_oe_frompadmisc_[0]),	.PAD(p_d_dq[0]),	.C(ram_dq_out2padmisc[0]),
								.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PDD12DGZ	pin173		(.I(ram_dqm_frompadmisc[0]),	.OEN(ram_dqm_oe_frompadmisc_[0]),	.PAD(p_d_dqm[0]),	.C(ram_dqm_out2padmisc[0]),
								.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PVDD2DGZ	pin174		(.VD33	(vddio),	.VSSPST	(gnd));//	IO power
	PVDD2DGZ	pin174_1	(.VD33	(vddio),	.VSSPST	(gnd));//	IO power
	PVDD2DGZ	pin174_2	(.VD33	(vddio),	.VSSPST	(gnd));//	IO power

	PDB12DGZ	pin175		(.I(ram_we_frompadmisc_),		.OEN(ram_we_oe_frompadmisc_),		.PAD(p_d_we_),		.C(),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PDB12DGZ	pin176		(.I(ram_cas_frompadmisc_),		.OEN(ram_cas_oe_frompadmisc_),		.PAD(p_d_cas_),		.C(ram_cas_out2padmisc_),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PDB12DGZ	pin177		(.I(ram_ras_frompadmisc_),		.OEN(ram_ras_oe_frompadmisc_),		.PAD(p_d_ras_),		.C(),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PDB12DGZ	pin178		(.I(ram_cs_frompadmisc_),		.OEN(ram_cs_oe_frompadmisc_),		.PAD(p_d_cs_ ),		.C(ram_cs_out2padmisc_),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PVDD1DGZ	pin179		(.VDD(vddcore),	.VD33(vddio), .VSSPST(gnd));//	Core power
    PVDD1DGZ	pin179_1	(.VDD(vddcore),	.VD33(vddio), .VSSPST(gnd));//	Core power
    PVDD1DGZ	pin179_2	(.VDD(vddcore),	.VD33(vddio), .VSSPST(gnd));//	Core power

	PDD12DGZ	pin180		(.I(ram_ba_frompadmisc[0]),		.OEN(ram_ba_oe_frompadmisc_[0]),	.PAD(p_d_baddr[0]),	.C(ram_ba_out2padmisc[0]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PDD12DGZ	pin181		(.I(ram_ba_frompadmisc[1]),		.OEN(ram_ba_oe_frompadmisc_[1]),	.PAD(p_d_baddr[1]),	.C(ram_ba_out2padmisc[1]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PDD12DGZ	pin182		(.I(ram_addr_frompadmisc[10]),	.OEN(ram_addr_oe_frompadmisc_[10]),	.PAD(p_d_addr[10]),	.C(ram_addr_out2padmisc[10]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PDD12DGZ	pin183		(.I(ram_addr_frompadmisc[0]),	.OEN(ram_addr_oe_frompadmisc_[0]),	.PAD(p_d_addr[0]),	.C(ram_addr_out2padmisc[0]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PDD12DGZ	pin184		(.I(ram_addr_frompadmisc[1]),	.OEN(ram_addr_oe_frompadmisc_[1]),	.PAD(p_d_addr[1]),	.C(ram_addr_out2padmisc[1]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

    PVSS3DGZ	pin185		(.VSS(gnd),	.VD33(vddio)  );	//	 ground
    PVSS3DGZ	pin185_1	(.VSS(gnd),	.VD33(vddio)  );	//	 ground
    PVSS3DGZ	pin185_2	(.VSS(gnd),	.VD33(vddio)  );	//	 ground

	PDD12DGZ	pin186		(.I(ram_addr_frompadmisc[2]),	.OEN(ram_addr_oe_frompadmisc_[2]),	.PAD(p_d_addr[2]),	.C(ram_addr_out2padmisc[2]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PDD12DGZ	pin187		(.I(ram_addr_frompadmisc[3]),	.OEN(ram_addr_oe_frompadmisc_[3]),	.PAD(p_d_addr[3]),	.C(ram_addr_out2padmisc[3]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PDD12DGZ	pin188		(.I(ram_addr_frompadmisc[4]),	.OEN(ram_addr_oe_frompadmisc_[4]),	.PAD(p_d_addr[4]),	.C(ram_addr_out2padmisc[4]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PDD12DGZ	pin189		(.I(ram_addr_frompadmisc[5]),	.OEN(ram_addr_oe_frompadmisc_[5]),	.PAD(p_d_addr[5]),	.C(ram_addr_out2padmisc[5]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PDD12DGZ	pin190		(.I(ram_addr_frompadmisc[6]),	.OEN(ram_addr_oe_frompadmisc_[6]),	.PAD(p_d_addr[6]),	.C(ram_addr_out2padmisc[6]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PDD12DGZ	pin191		(.I(ram_addr_frompadmisc[7]),	.OEN(ram_addr_oe_frompadmisc_[7]),	.PAD(p_d_addr[7]),	.C(ram_addr_out2padmisc[7]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PVDD2DGZ	pin192		(.VD33	(vddio),	.VSSPST	(gnd));//	IO power
	PVDD2DGZ	pin192_1	(.VD33	(vddio),	.VSSPST	(gnd));//	IO power
	PVDD2DGZ	pin192_2	(.VD33	(vddio),	.VSSPST	(gnd));//	IO power

	PVDD1DGZ	pin193		(.VDD(vddcore),	.VD33(vddio), .VSSPST(gnd));//	Core power
    PVDD1DGZ	pin193_1	(.VDD(vddcore),	.VD33(vddio), .VSSPST(gnd));//	Core power
    PVDD1DGZ	pin193_2	(.VDD(vddcore),	.VD33(vddio), .VSSPST(gnd));//	Core power

	PDD12DGZ	pin194		(.I(ram_addr_frompadmisc[8]),	.OEN(ram_addr_oe_frompadmisc_[8]),	.PAD(p_d_addr[8]),	.C(ram_addr_out2padmisc[8]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PDD12DGZ	pin195		(.I(ram_addr_frompadmisc[9]),	.OEN(ram_addr_oe_frompadmisc_[9]),	.PAD(p_d_addr[9]),	.C(ram_addr_out2padmisc[9]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PDD12DGZ	pin196		(.I(ram_addr_frompadmisc[11]),	.OEN(ram_addr_oe_frompadmisc_[11]),	.PAD(p_d_addr[11]),	.C(ram_addr_out2padmisc[11]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PDD12DGZ	pin197		(.I(ram_dq_frompadmisc[15]),	.OEN(ram_dq_oe_frompadmisc_[15]),	.PAD(p_d_dq[15]),	.C(ram_dq_out2padmisc[15]),
								.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PDD12DGZ	pin198		(.I(ram_dq_frompadmisc[14]),	.OEN(ram_dq_oe_frompadmisc_[14]),	.PAD(p_d_dq[14]),	.C(ram_dq_out2padmisc[14]),
								.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PDD12DGZ	pin199		(.I(ram_dq_frompadmisc[13]),	.OEN(ram_dq_oe_frompadmisc_[13]),	.PAD(p_d_dq[13]),	.C(ram_dq_out2padmisc[13]),
								.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PDD12DGZ	pin200		(.I(ram_dq_frompadmisc[12]),	.OEN(ram_dq_oe_frompadmisc_[12]),	.PAD(p_d_dq[12]),	.C(ram_dq_out2padmisc[12]),
								.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PVDD2DGZ	pin201		(.VD33	(vddio),	.VSSPST	(gnd));//	IO power
	PVDD2DGZ	pin201_1	(.VD33	(vddio),	.VSSPST	(gnd));//	IO power
	PVDD2DGZ	pin201_2	(.VD33	(vddio),	.VSSPST	(gnd));//	IO power

	PDD12DGZ	pin202		(.I(ram_dq_frompadmisc[11]),	.OEN(ram_dq_oe_frompadmisc_[11]),	.PAD(p_d_dq[11]),	.C(ram_dq_out2padmisc[11]),
								.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PDD12DGZ	pin203		(.I(ram_dq_frompadmisc[10]),	.OEN(ram_dq_oe_frompadmisc_[10]),	.PAD(p_d_dq[10]),	.C(ram_dq_out2padmisc[10]),
								.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PDD12DGZ	pin204		(.I(ram_dq_frompadmisc[9]),		.OEN(ram_dq_oe_frompadmisc_[9]),	.PAD(p_d_dq[9]),	.C(ram_dq_out2padmisc[9]),
								.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PDD12DGZ	pin205		(.I(ram_dq_frompadmisc[8]),		.OEN(ram_dq_oe_frompadmisc_[8]),	.PAD(p_d_dq[8]),	.C(ram_dq_out2padmisc[8]),
								.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PDD12DGZ	pin206		(.I(ram_dqm_frompadmisc[1]),	.OEN(ram_dqm_oe_frompadmisc_[1]),	.PAD(p_d_dqm[1]),	.C(ram_dqm_out2padmisc[1]),
								.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

    PVSS3DGZ	pin207		(.VSS(gnd),	.VD33(vddio)  );	//	 ground
    PVSS3DGZ	pin207_1	(.VSS(gnd),	.VD33(vddio)  );	//	 ground
    PVSS3DGZ	pin207_2	(.VSS(gnd),	.VD33(vddio)  );	//	 ground

	PDO24CDG	pin208		(.I(ext_mem_dclk_fromclkgen),	.PAD(p_d_clk),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));	//JFR 030829

	PVDD2DGZ	pin209		(.VD33	(vddio),	.VSSPST	(gnd));//	IO power
	PVDD2DGZ	pin209_1	(.VD33	(vddio),	.VSSPST	(gnd));//	IO power
	PVDD2DGZ	pin209_2	(.VD33	(vddio),	.VSSPST	(gnd));//	IO power

	PDB12DGZ	pin210		(.I(ram_dq_frompadmisc[16]),	.OEN(ram_dq_oe_frompadmisc_[16]),	.PAD(p_d_dq[16]),	.C(ram_dq_out2padmisc[16]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PDB12DGZ	pin211		(.I(ram_dq_frompadmisc[17]),	.OEN(ram_dq_oe_frompadmisc_[17]),	.PAD(p_d_dq[17]),	.C(ram_dq_out2padmisc[17]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PDB12DGZ	pin212		(.I(ram_dq_frompadmisc[18]),	.OEN(ram_dq_oe_frompadmisc_[18]),	.PAD(p_d_dq[18]),	.C(ram_dq_out2padmisc[18]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PDB12DGZ	pin213		(.I(ram_dq_frompadmisc[19]),	.OEN(ram_dq_oe_frompadmisc_[19]),	.PAD(p_d_dq[19]),	.C(ram_dq_out2padmisc[19]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

    PVSS3DGZ	pin214		(.VSS(gnd),	.VD33(vddio)  );	//	 ground
    PVSS3DGZ	pin214_1	(.VSS(gnd),	.VD33(vddio)  );	//	 ground
    PVSS3DGZ	pin214_2	(.VSS(gnd),	.VD33(vddio)  );	//	 ground

	PDB12DGZ	pin215		(.I(ram_dq_frompadmisc[20]),	.OEN(ram_dq_oe_frompadmisc_[20]),	.PAD(p_d_dq[20]),	.C(ram_dq_out2padmisc[20]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PDB12DGZ	pin216		(.I(ram_dq_frompadmisc[21]),	.OEN(ram_dq_oe_frompadmisc_[21]),	.PAD(p_d_dq[21]),	.C(ram_dq_out2padmisc[21]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PDB12DGZ	pin217		(.I(ram_dq_frompadmisc[22]),	.OEN(ram_dq_oe_frompadmisc_[22]),	.PAD(p_d_dq[22]),	.C(ram_dq_out2padmisc[22]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PDB12DGZ	pin218		(.I(ram_dq_frompadmisc[23]),	.OEN(ram_dq_oe_frompadmisc_[23]),	.PAD(p_d_dq[23]),	.C(ram_dq_out2padmisc[23]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PDB12DGZ	pin219		(.I(ram_dqm_frompadmisc[2]),	.OEN(ram_dqm_oe_frompadmisc_[2]), .PAD(p_d_dqm[2]),	.C(ram_dqm_out2padmisc[2]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PVDD1DGZ	pin220		(.VDD(vddcore),	.VD33(vddio), .VSSPST(gnd));//	Core power
    PVDD1DGZ	pin220_1	(.VDD(vddcore),	.VD33(vddio), .VSSPST(gnd));//	Core power
    PVDD1DGZ	pin220_2	(.VDD(vddcore),	.VD33(vddio), .VSSPST(gnd));//	Core power

	PDB12DGZ	pin221		(.I(ram_dq_frompadmisc[31]),	.OEN(ram_dq_oe_frompadmisc_[31]),	.PAD(p_d_dq[31]),	.C(ram_dq_out2padmisc[31]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PDB12DGZ	pin222		(.I(ram_dq_frompadmisc[30]),	.OEN(ram_dq_oe_frompadmisc_[30]),	.PAD(p_d_dq[30]),	.C(ram_dq_out2padmisc[30]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PDB12DGZ	pin223		(.I(ram_dq_frompadmisc[29]),	.OEN(ram_dq_oe_frompadmisc_[29]),	.PAD(p_d_dq[29]),	.C(ram_dq_out2padmisc[29]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

    PVSS3DGZ	pin224		(.VSS(gnd),	.VD33(vddio)  );	//	 ground
    PVSS3DGZ	pin224_1	(.VSS(gnd),	.VD33(vddio)  );	//	 ground
    PVSS3DGZ	pin224_2	(.VSS(gnd),	.VD33(vddio)  );	//	 ground

	PDB12DGZ	pin225		(.I(ram_dq_frompadmisc[28]),	.OEN(ram_dq_oe_frompadmisc_[28]),	.PAD(p_d_dq[28]),	.C(ram_dq_out2padmisc[28]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PDB12DGZ	pin226		(.I(ram_dq_frompadmisc[27]),	.OEN(ram_dq_oe_frompadmisc_[27]),	.PAD(p_d_dq[27]),	.C(ram_dq_out2padmisc[27]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PDB12DGZ	pin227		(.I(ram_dq_frompadmisc[26]),	.OEN(ram_dq_oe_frompadmisc_[26]),	.PAD(p_d_dq[26]),	.C(ram_dq_out2padmisc[26]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PVDD2DGZ	pin228		(.VD33	(vddio),	.VSSPST	(gnd));//	IO power
	PVDD2DGZ	pin228_1	(.VD33	(vddio),	.VSSPST	(gnd));//	IO power
	PVDD2DGZ	pin228_2	(.VD33	(vddio),	.VSSPST	(gnd));//	IO power

	PDB12DGZ	pin229		(.I(ram_dq_frompadmisc[25]),	.OEN(ram_dq_oe_frompadmisc_[25]),	.PAD(p_d_dq[25]),	.C(ram_dq_out2padmisc[25]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PDB12DGZ	pin230		(.I(ram_dq_frompadmisc[24]),	.OEN(ram_dq_oe_frompadmisc_[24]),	.PAD(p_d_dq[24]),	.C(ram_dq_out2padmisc[24]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PDB12DGZ	pin231		(.I(ram_dqm_frompadmisc[3]),	.OEN(ram_dqm_oe_frompadmisc_[3]), 	.PAD(p_d_dqm[3]),	.C(ram_dqm_out2padmisc[3]),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

    PVSS3DGZ	pin232		(.VSS(gnd),	.VD33(vddio)  );	//	 ground
    PVSS3DGZ	pin232_1	(.VSS(gnd),	.VD33(vddio)  );	//	 ground
    PVSS3DGZ	pin232_2	(.VSS(gnd),	.VD33(vddio)  );	//	 ground

	PDD04DGZ	pin233		(.I(j_rst_frompadmisc_), 	.OEN(j_rst_oe_frompadmisc_),	.PAD(p_j_rst_),		.C(j_rst_out2padmisc_),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PDB04DGZ	pin234		(.I(j_tdo_frompadmisc),		.OEN(j_tdo_oe_frompadmisc_),	.PAD(p_j_tdo),		.C(j_tdo_out2padmisc),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PDB04DGZ	pin235		(.I(j_tclk_frompadmisc),	.OEN(j_tclk_oe_frompadmisc_),	.PAD(p_j_tclk),		.C(j_tclk_out2padmisc),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PDB04DGZ	pin236		(.I(j_tms_frompadmisc),		.OEN(j_tms_oe_frompadmisc_),	.PAD(p_j_tms),		.C(j_tms_out2padmisc),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PDB04DGZ	pin237		(.I(j_tdi_frompadmisc),		.OEN(j_tdi_oe_frompadmisc_),	.PAD(p_j_tdi),		.C(j_tdi_out2padmisc),
							.VD33(vddio),	.VDD(vddcore),	.VSSPST(gnd),	.VSS(gnd));

	PVDD1DGZ	pin238		(.VDD(vddcore),	.VD33(vddio), .VSSPST(gnd));//	Core power
    PVDD1DGZ	pin238_1	(.VDD(vddcore),	.VD33(vddio), .VSSPST(gnd));//	Core power
    PVDD1DGZ	pin238_2	(.VDD(vddcore),	.VD33(vddio), .VSSPST(gnd));//	Core power

	//	Power cut between digital power and TV_ENC
	PRDIODE		PCUT_TVDAC	(.VDD1(vddio),	.VDD2(vd33a_tvdac),	.VSS1(gnd),	.VSS2(vsud_tvdac));

    PDIANA2P	pin239		(.C(tvenc_iout1_fromcore), 	.PAD(iout1),  .TAVDD(vd33a_tvdac), .TAVSS(vsud_tvdac));

	PDIANA2P	pin240		(.C(tvenc_iout2_fromcore), 	.PAD(iout2),  .TAVDD(vd33a_tvdac), .TAVSS(vsud_tvdac));

	PVDD3P		DAC_DUMMY_PWR_1		(.TAVDD(vd33a_tvdac), .TAVSS(vsud_tvdac));		//	TV Encoder dummy power

    PDIANA2P	pin241		(.C(tvenc_iout3_fromcore), 	.PAD(iout3),  .TAVDD(vd33a_tvdac), .TAVSS(vsud_tvdac));

	PDIANA2P	pin242		(.C(tvenc_xidump_fromcore), .PAD(xidump),  .TAVDD(vd33a_tvdac), .TAVSS(vsud_tvdac));
	PDIANA2P	pin242_1	(.C(tvenc_xidump_fromcore), .PAD(xidump),  .TAVDD(vd33a_tvdac), .TAVSS(vsud_tvdac));
	PDIANA2P	pin242_2	(.C(tvenc_xidump_fromcore), .PAD(xidump),  .TAVDD(vd33a_tvdac), .TAVSS(vsud_tvdac));

	PVDD3P		pin243		(.TAVDD(vd33a_tvdac), .TAVSS(gnda_tvdac));		//	TV Encoder power
	PVDD3P		pin243_1	(.TAVDD(vd33a_tvdac), .TAVSS(gnda_tvdac));		//	TV Encoder power
	PVDD3P		pin243_2	(.TAVDD(vd33a_tvdac), .TAVSS(gnda_tvdac));		//	TV Encoder power

    PDIANA2P	pin244		(.C(tvenc_iout4_fromcore), 	.PAD(iout4),  .TAVDD(vd33a_tvdac), .TAVSS(vsud_tvdac));

    PDIANA2P	pin245		(.C(tvenc_xiref2_fromcore),	.PAD(xiref2),	.TAVDD(vd33a_tvdac),	.TAVSS(vsud_tvdac));

    PDIANA2P	pin246		(.C(tvenc_xiref1_fromcore),	.PAD(xiref1),	.TAVDD(vd33a_tvdac),	.TAVSS(vsud_tvdac));

	PDIANA2P	pin247		(.C(tvenc_xiext_fromcore),	.PAD(xiext),	.TAVDD(vd33a_tvdac),	.TAVSS(vsud_tvdac));
	PDIANA2P	pin247_1	(.C(tvenc_xiext1_fromcore),	.PAD(xiext1),	.TAVDD(vd33a_tvdac),	.TAVSS(vsud_tvdac));

	PVSS1P		pin248		(.TVSS1P(gnda1_tvdac),	.TAVDD(vd33a_tvdac), .TAVSS(vsud_tvdac));		//	TV Encoder GND

	PVDD3P		DAC_DUMMY_PWR_2		(.TAVDD(vd33a_tvdac), .TAVSS(vsud_tvdac));		//	TV Encoder dummy power

    PDIANA2P	pin249		(.C(tvenc_iout5_fromcore), 	.PAD(iout5),  .TAVDD(vd33a_tvdac), .TAVSS(vsud_tvdac));

	PDIANA2P	pin250		(.C(tvenc_iout6_fromcore), 	.PAD(iout6),  .TAVDD(vd33a_tvdac), .TAVSS(vsud_tvdac));


	PVSS3P		pin251		(.TAVDD(vd33a_tvdac), .TAVSS(vsud_tvdac));								//	TV Encoder power
	PVSS1P		pin251_1	(.TVSS1P(gnda_tvdac),	.TAVDD(vd33a_tvdac), .TAVSS(vsud_tvdac));		//	TV Encoder power
	PVSS1P		pin251_2	(.TVSS1P(gnda_tvdac),	.TAVDD(vd33a_tvdac), .TAVSS(vsud_tvdac));		//	TV Encoder power

	//	Power cut between	TV_ENC and ADC
	PRDIODE		PCUT_ADC_1	(.VDD1(vd33a_adc), .VDD2(vd33a_tvdac), .VSS1(gnda_adc), .VSS2(vsud_tvdac));	//(.PCUT_TVDAC		(adc));  guard adc

	PVDD3P		pin252		(.TAVDD(vd33a_adc), .TAVSS(gnda_adc));	//	ADC power
	PVDD3P		pin252_1	(.TAVDD(vd33a_adc), .TAVSS(gnda_adc));	//	ADC power
	PVDD3P		pin252_2	(.TAVDD(vd33a_adc), .TAVSS(gnda_adc));	//	ADC power

	PDIANA1P	pin253		(.C(xmic1in_out2core), 	.PAD(xmic1in  ), .TAVDD(vd33a_adc), .TAVSS(gnda_adc));

	PDIANA1P	pin254		(.C(xadc_vref_fromcore), 	.PAD(xadc_vref), .TAVDD(vd33a_adc), .TAVSS(gnda_adc));
	PDIANA1P	pin254_1	(.C(xadc_vref_fromcore), 	.PAD(xadc_vref), .TAVDD(vd33a_adc), .TAVSS(gnda_adc));

	PDIANA1P	pin255		(.C(xmic2in_out2core), 	.PAD(xmic2in  ), .TAVDD(vd33a_adc), .TAVSS(gnda_adc));

	PVSS3P		pin256		(.TAVDD(vd33a_adc), .TAVSS(gnda_adc));	//	ADC GND
	PVSS3P		pin256_1	(.TAVDD(vd33a_adc), .TAVSS(gnda_adc));	//	ADC GND
	PVSS3P		pin256_2	(.TAVDD(vd33a_adc), .TAVSS(gnda_adc));	//	ADC GND

	//	Power cut between ADC and Digital power
	PRDIODE		PCUT_ADC_2	(.VDD1(vd33a_adc),	.VDD2(vddio),	.VSS1(gnda_adc),	.VSS2(gnd));

//================================================================
/*    PCORNERDG_L top_left 		(.VDD(),		.VSS() );
    PCORNERDG_L top_right 		(.VDD(),		.VSS() );
    PCORNERDG_L bottom_left 	(.VDD(),		.VSS() );
    PCORNERDG_L bottom_right	(.VDD(),		.VSS() );
*/
endmodule
