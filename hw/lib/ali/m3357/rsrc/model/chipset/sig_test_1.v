//=====================================================================
//file:				sig_terminal_1.v
//description:		use to test the system level line connection bwteen
//					core and the chip.
//					This model will be take place the internal signals
//					of the core: output, input or inout signals, its
//					default status is input status
//					It will automatically to generate the output data
//					sequence. And automatically to check the input
//					sequence data.
//					It must used with another module sig_terminal_2
//authod:			yuky
//date:				2002-07-10 initial version
//=====================================================================

module sig_terminal_1(
	sig_in,			//input signal
	sig_out,		//output signal
	sig_oe_,		//output signal enable

	err,			//input signals not match the expected value
	test_tri,		//start test triger
	sig_type,		//0->in, 1->out, 2->inout, 3->reserved
	clk				//test clock, the input or output signal data sequence clock
	);

parameter SEQ_NO = 0; 	//default to select the sequence zero, its value should be same
						// in the two termination of one pin
parameter udly =1;
parameter width = 6;
parameter SIG_SEQUENCE  = 64'ha5a5_1111_5a5a_2222; //test data sequence, must as same as another module sig_terminal_1



input 	sig_in;
output	sig_out;
output	sig_oe_;

output	err;
input	test_tri;
input[1:0] sig_type;
input	clk;

reg	[ (1 << width) - 1 :0] sig_mem;
initial begin
	sig_mem = 64'h0;
	sig_mem = SIG_SEQUENCE + (SEQ_NO << 32) + SEQ_NO; //set the signal sequence value
end

reg	[width - 1 : 0] bit_addr;
reg	sig_oe_;
initial begin
	bit_addr	= 63;
	sig_oe_		= 1'b1;//default is input
end

wire	input_type  = sig_type == 2'b00;//input pin of the chip
wire	output_type = sig_type == 2'b01;//output pin of the chip
wire	inout_type  = sig_type == 2'b10;//inout pin of the chip

wire sig_tmp1	= sig_oe_ ? 1'bz : sig_mem[bit_addr];
wire sig_tmp2	= sig_mem[bit_addr];
wire sig_out_tmp = inout_type ? sig_tmp2 : ((input_type | output_type) ? sig_tmp1 : 1'bz);
wire sig_out	= sig_out_tmp;

//delay the input test_tri with the test clock
reg	inter_tri;
initial inter_tri = 0;
always @(posedge clk)
	inter_tri <= #udly test_tri;

//generate one tri pulse to enable the sig_oe first
wire tri_pulse = test_tri & ~ inter_tri;
always @(posedge clk or negedge test_tri)
	if (!test_tri)
		sig_oe_	<= #udly 1'b1;
	else if (tri_pulse)
		sig_oe_	<= #udly 1'b0;

//use the internal tri(be delay one clock) to control the output or input status
wire test_end 	= bit_addr == 0;
always @(posedge clk or negedge test_tri)
	if (!test_tri)
		bit_addr <= #udly 63;
	else if (inter_tri) begin
		bit_addr 	<= #udly bit_addr - 1;
		if (test_end)
			sig_oe_ 	<= #udly ~sig_oe_;
	end

//compare the result during input status of input/output signal,
wire	err;
reg		input_type_err;
reg		output_type_err;
reg		inout_type_err;
initial input_type_err = 0;
initial	output_type_err = 0;
initial	inout_type_err = 0;
assign	err = input_type_err | output_type_err | inout_type_err;

//detect the test signal changes
wire detc_chanel1 = sig_in & sig_tmp2;
wire detc_chanel2 = sig_in | sig_tmp2;
reg	detc_channel_sel;
initial detc_channel_sel = 0;
always @(posedge clk or negedge test_tri)
	if (!test_tri)
		detc_channel_sel = 1'b0;
	else if (inter_tri && sig_oe_ && sig_in) //sig_in =1
		detc_channel_sel = 1'b1;
	else
		detc_channel_sel = 1'b0;
wire check_data = detc_channel_sel ? detc_chanel2:detc_chanel1;

//generate the check period for output_type
reg	sig_oe_high;
initial sig_oe_high = 0;
always @(posedge clk or negedge test_tri)
	if (!test_tri)
		sig_oe_high <= #udly 1'b0;
	else if (inter_tri && sig_oe_)
		sig_oe_high <= #udly 1'b1;
	else
		sig_oe_high <= #udly 1'b0;

//report error
//input type error
always @(posedge clk or negedge test_tri)
	if (!test_tri)
		input_type_err <= #udly 0;
	else if (inter_tri) begin
		if (input_type && sig_oe_ && (sig_in !== sig_tmp2))
			input_type_err <= #udly 1;	//the data must be same
	end

//output type error
reg	not_match;
initial not_match = 0;
always @(posedge clk or negedge test_tri)
	if (!test_tri)
		not_match <= #udly 0;
	else if (inter_tri) begin
		if (output_type && sig_oe_ && (check_data !== sig_tmp2))
			not_match <= #udly 1;	//the data must be different because it can't input data from pad
		else if (output_type && ~sig_oe_)
			not_match <= #udly 0;
	end

//check_point
wire check_point = sig_oe_high & ~sig_oe_;
always @(posedge clk or negedge test_tri)
	if (!test_tri)
		output_type_err <= #udly 0;
	else if (inter_tri && output_type && check_point && ~not_match)
		output_type_err <= #udly 1;

//inout type error
always @(posedge clk or negedge test_tri)
	if (!test_tri)
		inout_type_err <= #udly 0;
	else if (inter_tri) begin
		if (inout_type && (sig_in !== sig_tmp2))
			inout_type_err <= #udly 1;	//the data must be same
	end
endmodule
