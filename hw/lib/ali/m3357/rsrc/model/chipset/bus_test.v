/****************************************************************
*	File name   : bus_test.v									*
*	Author	    : Snow Yi										*
*	Revision    : 15/12/98, initial creation					*
*				  15/08/2002, add the INT_ and NMI_, yuky liu	*
*	Discription : 												*
*					for CPU core testing MIPS bus				*
****************************************************************/
`timescale 1ns/100ps

/*
`include "/fremont/home1/risc/t2chipset/wince/bus_dec.v"
`include "/fremont/home1/risc/t2chipset/wince/sdram.v"
`include "/fremont/home1/risc/t2chipset/wince/bootrom.v"
`include "/fremont/home1/risc/t2chipset/wince/local_io.v"
*/

module bus_test (
		sysad,
		syscmd,
		pvalid_,
		preq_,
		pmaster_,
		eok_,
		evalid_,
		ereq_,
		int_,
		nmi_,
		tclk,
		rst_
		);

inout [31:0] sysad;
inout [4:0]  syscmd;

output       eok_;
output       evalid_;
output       ereq_;

input        pvalid_;
input        pmaster_;
input        preq_;
input        tclk;
input        rst_;
//interruput bus
output [4:0] int_;
output		 nmi_;

parameter	udly	= 1;
integer		dump_file;

/*
initial begin
	dump_file = $fopen ("mips_bus.file");
	$fdisplay (dump_file, "MIPS Bus Cycle Dump File !!!");
end
*/

wire [20:2]  eeprom_addr;
wire [31:0]  eeprom_data;
wire [5:2]   io_addr;
wire [31:0]  io_data;
wire [27:2]  sdram_addr;
wire [31:0]  sdram_data;
wire [3:0]   sdram_dqm;

bootrom bootrom (.eeprom_data	(eeprom_data	),
		 .eeprom_addr	(eeprom_addr	),
		 .eeprom_oe_	(1'b0		),
		 .eeprom_cs_	(eeprom_cs_	)
		);

local_io local_io	(.io_addr	(io_addr	),
			 .sel_io	(sdram_addr[24] ), // roger
			 .io_data	(io_data	),
			 .io_cs_	(io_cs_		),
			 .io_oe_	(io_oe_		),
			 .io_we_	(io_we_		),
			 .int_		(int_		),
			 .nmi_		(nmi_		),
			 .io_clk	(tclk		)
			);

sdram	sdram	(.sdram_addr	(sdram_addr	),
		 .sdram_data	(sdram_data	),
		 .sdram_dqm	(sdram_dqm	),
		 .sdram_cs_	(sdram_cs_	),
		 .sdram_oe_	(sdram_oe_	),
		 .sdram_we_	(sdram_we_	),
		 .last_end	(last_end	),
		 .sdram_clk	(tclk		)
		);

bus_dec	bus_dec	   (
		    .sysad			(sysad		),
            .syscmd         (syscmd     ),
		    .evalid_		(evalid_	),
		    .pvalid_		(pvalid_	),
		    .eok_			(eok_		),
		    .preq_			(preq_		),
		    .ereq_			(ereq_		),
		    .pmaster_		(pmaster_	),

		    .eeprom_data	(eeprom_data	),
            .eeprom_addr	(eeprom_addr	),
//          .eeprom_oe_		(eeprom_oe_	),
            .eeprom_cs_		(eeprom_cs_	),

		    .io_addr		(io_addr	),
		    .io_data		(io_data	),
		    .io_cs_			(io_cs_		),
 		    .io_oe_			(io_oe_		),
		    .io_we_			(io_we_		),

		    .sdram_addr		(sdram_addr	),
		    .sdram_data		(sdram_data	),
		    .sdram_dqm		(sdram_dqm	),
		    .sdram_cs_		(sdram_cs_	),
		    .sdram_oe_		(sdram_oe_	),
		    .sdram_we_		(sdram_we_	),
		    .last_end	    (last_end	),

		    .tclk			(tclk		),
            .rst_			(rst_  		)   // TOP
		);
/*
always @(posedge tclk)
	if (~pvalid_) begin
		$fdisplay (dump_file, "");
		$fdisplay (dump_file, "\tM_A_D=%h,\tM_CMD=%h",sysad,syscmd);
	end
	else if (~evalid_) begin
		$fdisplay (dump_file, "\tS_A_D=%h,\tS_CMD=%h",sysad,syscmd);
	end
	else if (~pvalid_ & ~evalid_) begin
		$fdisplay (dump_file, "MIPS Error !!!");
	end
*/
endmodule
