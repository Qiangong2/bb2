/**********************************************************************************
// Description: M6304 core behavior module; it is just used to test the system level
//				logic connection.(chip,pad,padmisc)
// File:        core_bh.v
// Author:      yuky
//
// History:     2002-07-10 initial version
***********************************************************************************/
module core_bh (
//------chipset bus
        // SDRAM & FLASH
        ram_addr,
        ram_ba,
        ram_dq_out,
        ram_dq_in,
        ram_dq_oe_,
        ram_cas_,
        ram_we_,
        ram_ras_,
        ram_cke,
        ram_dqm,
        ram_cs_,
        // EEPROM
        eeprom_addr,
        eeprom_rdata,
        eeprom_cs_,
        eeprom_oe_,
        eeprom_we_,
        rom_data_oe_,
        rom_addr_oe,
        eeprom_wdata,
        rom_en_,                 //indicates start rom operation
        // LED
//      ledpin,
        // PCI
        oe_ad_,
        out_ad,
        oe_cbe_,
        out_cbe_,
        oe_frame_,
        out_frame_,
        oe_irdy_,
        out_irdy_,
        oe_devsel_,
        out_devsel_,
        oe_trdy_,
        out_trdy_,
        oe_stop_,
        out_stop_,
        oe_par_,
        out_par,
        out_gnt_,
        in_req_,
        in_ad,
        in_cbe_,
        in_frame_,
        in_irdy_,
        in_devsel_,
        in_trdy_,
        in_stop_,
        in_par,
        ext_int,
        gpio_out,
        gpio_oe_,
        gpio_in,
        // Audio
        out_ac97_rst_,
        out_ac97_sdo,
        out_ac97_sync,
        in_ac97_sdi0,
        in_ac97_sdi1,
        ac97_clk,

        out_i2so_data,
        out_i2so_lrck,
        out_i2so_bick,
        in_i2si_data,
   //     in_i2si_lrck,
   //     in_i2si_bick,
        in_i2so_mclk,
        i2s_share_ctrl,

        //spdif
        out_spdif,
        oe_spdif,
        in_spdif,
        // South Bridge
        cd_bck,
        cd_lrck,
        cd_data,
        cd_c2po,
        subq_en,
        subq_sqck,              // Clock output to read SubQ data from CD-DSP, share pin with GPIO
        subq_sqso,              // SubQ data from CD-DSP, share pin with GPIO
        subq_scor,              // SubQ ready from CD-DSP, share pin with GPINT
        ir_rx,
        gsi_en,
        gsi_sdo,
        gsi_sck,
        gsi_sdi,
        scb_en,
        scb_scl_out,
        scb_scl_oe,
        scb_sda_out,
        scb_sda_oe,
        scb_scl_in,
        scb_sda_in,
        //m3321 interface
        m_updreq_,          // M3321 up request input
        m_upwr_ ,          // M3321 uP Write strobe (Intel mode).
        m_upcs_ ,          // M3321 uP Host Chip Select.
        m_upwait_,         // M3321 uP Waitj for data transfer not complete
        m_upda_in,         // Data [7:0] from M3321.
        m_upda_oe_,         // Data output enable.
        m_upda_out,        // Data [7:0] to M3321.
        m_upa   ,          // M3321 uP Address Bus[2:0]
        m_uprd_ ,          // M3321 uP Read strobe (Intel mode).
        m_upint_,          // M3321 uP Host Interrupt Request.
        m_uprst_,          // M3321 uP Host Reset.
        // USB
        p1_ls_mode,
        p1_txd  ,
        p1_txd_oej,
        p1_txd_se0,
        p2_ls_mode,
        p2_txd  ,
        p2_txd_oej,
        p2_txd_se0,

        usb_pon_,
        i_p1_rxd,
        i_p1_rxd_se0,
        i_p2_rxd,
        i_p2_rxd_se0,

        usb_overcur_,

        // Display Engine
        pclk_oe_,
        vdata_oe_,
        vdata_out,
        h_sync_out_,
        h_sync_oe_,
        in_h_sync_,
        v_sync_out_,
        v_sync_oe_,
        in_v_sync_,

         // IDE Interface
        atadiow_out_,
        atadior_out_,
        atacs0_out_,
        atacs1_out_,
        atadd_out,
        atadd_oe_,
        atada_out,
  //     atareset_out_,
        atadmack_out_,

        ataiordy_in,
        ataintrq_in,
        atadd_in,
        atadmarq_in,

// System Interrupt
        x_err_int,
        x_ext_int,

// CPU Interface
        // MIPS bus interface
        sysad_out,
        sysad_in,
        syscmd_out,
        syscmd_in,
        sysout_oe,
        sys_int_,
        sys_nmi_,
        pvalid_,
        preq_,
        pmaster_,
        eok_,
        evalid_,
        ereq_,

        // JTAG interface
        j_tdi,
        j_tms,
        j_tclk,
        j_rst_,
        j_tdo,

// Clock signals
        sb_clk,
        usb_clk,
        disp_clk,
        gpu_clk,
        cpu_clk,                        // pipeline clock   (CPU core)
        mem_clk,                        // transfer clock   (Memory Controller, IO, ROM)
        pci_clk,
        cpu_clk_src,
        dsp_clk,
        video_clk,
        spdif_clk,
        mbus_clk,
        ide100m_clk,
		ide66m_clk,

        //the clock frequency select
        dsp_clk_fs,
        gpu_clk_fs,
        ve_clk_fs,
        pci_clk_out_sel,
        pci_clk_oe_,
        pci_clk_cfg,
        //pll program control
        pci_t2risc_fs_tri,
        pci_t2risc_fs_ctrldata,
        mem_fs_tri,
        mem_fs_ctrldata,
        pll_m_tri,
        pll_m_ctrldata,

        pci_t2risc_fs_ack,
        mem_fs_ack,
        pll_m_ack,

        // working mode select
        cpu_mode,
        cpu_mode_,
        pci_mode,
        ide_cd_mode,
        up_mode,
        test_mode,

        //to cpu
        pci_mips_fs,
        cpu_endian_mode,
        core_warmrst_,
        core_coldrst_,
        cpu_probe_en,

        //connect to chipset
        sw_rst_pulse,
        pad_boot_config,
        mem_dly_sel,
        chipset_rst_
        );

parameter       udly = 1;
parameter       ram_dly = 1,
                pci_dly = 1,
                oth_dly = 1;
//============define the input/output/inout signals=============
//---------------chip set bus
// SDRAM & FLASH
output  [11:0]  ram_addr;
output  [1:0]   ram_ba;
output  [31:0]  ram_dq_out;
input   [31:0]  ram_dq_in;
output          ram_dq_oe_;
output      ram_cas_;
output      ram_we_;
output      ram_ras_;
output      ram_cke;
output  [3:0]   ram_dqm;
output  [1:0]   ram_cs_;

// EEPROM
output  [23:0]  eeprom_addr;
input   [7:0]   eeprom_rdata;
output          eeprom_cs_;
output          eeprom_oe_;
output          eeprom_we_;
output          rom_data_oe_, rom_addr_oe;
output  [7:0]   eeprom_wdata;
output          rom_en_;
// LED
//output        [7:0]   ledpin;

// PCI
output          oe_ad_;
output  [31:0]  out_ad;
output          oe_cbe_;
output  [3:0]   out_cbe_;
output          oe_frame_;
output          out_frame_;
output          oe_irdy_;
output          out_irdy_;
output          oe_devsel_;
output          out_devsel_;
output          oe_trdy_;
output          out_trdy_;
output          oe_stop_;
output          out_stop_;
output          oe_par_;
output          out_par;

output  [1:0]   out_gnt_;
input   [1:0]   in_req_;

input   [31:0]  in_ad;
input   [3:0]   in_cbe_;

input           in_frame_;
input           in_irdy_;
input           in_devsel_;
input           in_trdy_;
input           in_stop_;
input           in_par;

input           ext_int;
output  [31:0]  gpio_out;
output  [31:0]  gpio_oe_;
input   [31:0]  gpio_in;

// Audio
output          out_ac97_rst_;
output          out_ac97_sdo;
output          out_ac97_sync;
input           in_ac97_sdi0;
input           in_ac97_sdi1;
input           ac97_clk;

output          out_i2so_data;
output          out_i2so_lrck;
output          out_i2so_bick;
input           in_i2si_data;
//input           in_i2si_lrck;
//input           in_i2si_bick;
input           in_i2so_mclk;
output          i2s_share_ctrl;
//spdif
output          out_spdif;
output          oe_spdif;
input           in_spdif;
// South Bridge
input           cd_bck;
input           cd_lrck;
input           cd_data;
input           cd_c2po;

output          subq_en;
output          subq_sqck;              // output
input           subq_sqso;              // input
input           subq_scor;              // input

input           ir_rx;

output          gsi_en;
output          gsi_sdo;
output          gsi_sck;
input           gsi_sdi;

output          scb_en;
output          scb_scl_out;
output          scb_scl_oe;
output          scb_sda_out;
output          scb_sda_oe;
input           scb_scl_in;
input           scb_sda_in;

// uP interface
input   [7:0]   m_upda_in;
output  [7:0]   m_upda_out;
output          m_upda_oe_;
output  [9:0]   m_upa;
output          m_upcs_;        //uP Host Chip Select.
output          m_upwr_;        //uP Write strobe (Intel mode).
output          m_uprd_;        //uP Read strobe (Intel mode).
input           m_upwait_;
input   [1:0]   m_updreq_;      //uP DMA Request[1:0].
input           m_upint_;               //uP Host Interrupt Request.
output          m_uprst_;               //uP Host Reset.

input           sb_clk;

// USB
output          p1_ls_mode      ;
output          p1_txd          ;
output          p1_txd_oej      ;
output          p1_txd_se0      ;
output          p2_ls_mode      ;
output          p2_txd          ;
output          p2_txd_oej      ;
output          p2_txd_se0      ;

output          usb_pon_;
input           i_p1_rxd        ;
input           i_p1_rxd_se0;
input           i_p2_rxd        ;
input           i_p2_rxd_se0;

input           usb_clk;
input           usb_overcur_;

// Display Engine
output  pclk_oe_;
output  vdata_oe_;
output  h_sync_out_;
output  h_sync_oe_;
output  v_sync_out_;
output  v_sync_oe_;
input   in_h_sync_;
input   in_v_sync_;
output  [7:0]   vdata_out;

input   disp_clk;

output  x_err_int;
output  x_ext_int;
// MIPS bus interface
output  [31:0]  sysad_out;
input   [31:0]  sysad_in;
output  [4:0]   syscmd_out;
input   [4:0]   syscmd_in;
output          sysout_oe;
input   [4:0]   sys_int_;
input           sys_nmi_;
output          pvalid_;
output  preq_;
output  pmaster_;
input   eok_;
input   evalid_;
input   ereq_;

        // JTAG interface
input   j_tdi;
input   j_tms;
input   j_tclk;
input   j_rst_;
output  j_tdo;

input           gpu_clk;
input           cpu_clk;                        // pipeline clock   (CPU core)
input           mem_clk;                        // transfer clock   (Memory Controller, IO, ROM)
input           pci_clk;
input           mbus_clk;
input           cpu_clk_src;
input           dsp_clk;
input           video_clk;
input           spdif_clk;
input			ide100m_clk,
				ide66m_clk;

input   [31:0]  pad_boot_config;

output          sw_rst_pulse;
input           core_warmrst_;
input           chipset_rst_;

// IDE Interface
output  atadiow_out_;
output  atadior_out_;
output  atacs0_out_;
output  atacs1_out_;
output[15:0]atadd_out;
output[3:0]atadd_oe_;
output[2:0] atada_out;
//output  atareset_out_;
output  atadmack_out_;

input   ataiordy_in;
input   ataintrq_in;
input[15:0]     atadd_in;
input   atadmarq_in;

//the clock frequency select
output  [4:0]   dsp_clk_fs;
output  [1:0]   gpu_clk_fs;
output  [1:0]   ve_clk_fs;
output  [2:0]   pci_clk_out_sel;
output          pci_clk_oe_;
output          pci_clk_cfg;

output  [3:0]   mem_dly_sel;

//pll program control
output          pci_t2risc_fs_tri;
output  [1:0]   pci_t2risc_fs_ctrldata;
output          mem_fs_tri;
output  [1:0]   mem_fs_ctrldata;
output          pll_m_tri;
output  [7:0]   pll_m_ctrldata;

input           pci_t2risc_fs_ack;
input           mem_fs_ack;
input           pll_m_ack;

input           cpu_mode;
input           cpu_mode_;
input           pci_mode;
input           ide_cd_mode;
input           up_mode;
input           cpu_endian_mode;
input           test_mode;
input   [1:0]   pci_mips_fs;

//------------------CPU bus
input           core_coldrst_;
input           cpu_probe_en;
//==============================================================



//invoke the signal test module to connect all the core ports signals
wire	test_clk = top.sig_tclk;
wire [1:0] input_type  = 2'b00;
wire [1:0] output_type = 2'b01;
wire [1:0] inout_type  = 2'b10;

wire            CPU_mode        =       top.chip.cpu_mode;
wire            PCI_mode        =       top.chip.pci_mode;
wire            IDE_CD_mode     =       top.chip.ide_cd_mode;
wire            UP_mode         =       top.chip.up_mode;
wire            Rom_en          =       ~top.chip.rom_en_;

reg	test_tri;
reg	rom_tri;
reg i2s_ac97_tri;


initial	test_tri		= 1'b0;
initial	rom_tri			= 1'b0;
initial i2s_ac97_tri	= 1'b0;

wire cpu_tri 			= CPU_mode & test_tri;
wire sdram_tri 			= ~CPU_mode & test_tri;
wire eeprom_tri 		= ~CPU_mode & test_tri;
wire eeprom_share_tri 	= eeprom_tri & rom_tri;
wire pci_tri 			= PCI_mode & ~rom_tri;
wire ide_cd_tri 		= IDE_CD_mode & ~rom_tri;
wire up_tri 			= UP_mode & ~rom_tri;
wire i2s_tri			= ~CPU_mode & i2s_ac97_tri;

task pin_scan_start;
begin
	test_tri = 1;
	if (CPU_mode)
		$display("CPU_mode pins scan start......\n");
	else if (PCI_mode)
		$display("PCI_mode pins scan start......\n");
	else if (IDE_CD_mode)
		$display("IDE_CD_mode pins scan start......\n");
	else if (UP_mode)
		$display("UP_mode pins scan start......\n");
	else
		$display("Mode select don't confirm\n");
	rom_tri = 1;
	i2s_ac97_tri = 1;
	repeat(150) @(posedge test_clk);
	rom_tri = 0;
	i2s_ac97_tri = 0;
	repeat(150) @(posedge test_clk);
	rom_tri = 1;
	i2s_ac97_tri = 1;
	repeat(150) @(posedge test_clk);
	rom_tri = 0;
	i2s_ac97_tri = 0;
	repeat(150) @(posedge test_clk);
end
endtask

//========================= SDRAM
//output  [11:0]  ram_addr;
wire [11:0]	core_ram_addr_err;
sig_terminal_1 #(100)	core_ram_addr0 (.sig_in(ram_addr[0]) ,.sig_out(ram_addr[0]) ,.sig_oe_(),.err(core_ram_addr_err[0]) ,.test_tri(sdram_tri),.sig_type(output_type),.clk(test_clk)	);
sig_terminal_1 #(101)	core_ram_addr1 (.sig_in(ram_addr[1]) ,.sig_out(ram_addr[1]) ,.sig_oe_(),.err(core_ram_addr_err[1]) ,.test_tri(sdram_tri),.sig_type(output_type),.clk(test_clk)	);
sig_terminal_1 #(102)	core_ram_addr2 (.sig_in(ram_addr[2]) ,.sig_out(ram_addr[2]) ,.sig_oe_(),.err(core_ram_addr_err[2]) ,.test_tri(sdram_tri),.sig_type(output_type),.clk(test_clk)	);
sig_terminal_1 #(103)	core_ram_addr3 (.sig_in(ram_addr[3]) ,.sig_out(ram_addr[3]) ,.sig_oe_(),.err(core_ram_addr_err[3]) ,.test_tri(sdram_tri),.sig_type(output_type),.clk(test_clk)	);
sig_terminal_1 #(104)	core_ram_addr4 (.sig_in(ram_addr[4]) ,.sig_out(ram_addr[4]) ,.sig_oe_(),.err(core_ram_addr_err[4]) ,.test_tri(sdram_tri),.sig_type(output_type),.clk(test_clk)	);
sig_terminal_1 #(105)	core_ram_addr5 (.sig_in(ram_addr[5]) ,.sig_out(ram_addr[5]) ,.sig_oe_(),.err(core_ram_addr_err[5]) ,.test_tri(sdram_tri),.sig_type(output_type),.clk(test_clk)	);
sig_terminal_1 #(106)	core_ram_addr6 (.sig_in(ram_addr[6]) ,.sig_out(ram_addr[6]) ,.sig_oe_(),.err(core_ram_addr_err[6]) ,.test_tri(sdram_tri),.sig_type(output_type),.clk(test_clk)	);
sig_terminal_1 #(107)	core_ram_addr7 (.sig_in(ram_addr[7]) ,.sig_out(ram_addr[7]) ,.sig_oe_(),.err(core_ram_addr_err[7]) ,.test_tri(sdram_tri),.sig_type(output_type),.clk(test_clk)	);
sig_terminal_1 #(108)	core_ram_addr8 (.sig_in(ram_addr[8]) ,.sig_out(ram_addr[8]) ,.sig_oe_(),.err(core_ram_addr_err[8]) ,.test_tri(sdram_tri),.sig_type(output_type),.clk(test_clk)	);
sig_terminal_1 #(109)	core_ram_addr9 (.sig_in(ram_addr[9]) ,.sig_out(ram_addr[9]) ,.sig_oe_(),.err(core_ram_addr_err[9]) ,.test_tri(sdram_tri),.sig_type(output_type),.clk(test_clk)	);
sig_terminal_1 #(110)	core_ram_addr10(.sig_in(ram_addr[10]),.sig_out(ram_addr[10]),.sig_oe_(),.err(core_ram_addr_err[10]),.test_tri(sdram_tri),.sig_type(output_type),.clk(test_clk)	);
sig_terminal_1 #(111)	core_ram_addr11(.sig_in(ram_addr[11]),.sig_out(ram_addr[11]),.sig_oe_(),.err(core_ram_addr_err[11]),.test_tri(sdram_tri),.sig_type(output_type),.clk(test_clk)	);
//error monitor
always @(posedge (|core_ram_addr_err))
	$display ("internal signals of <ram_addr> scan error:%h at %0t",core_ram_addr_err, $realtime);

//output  [1:0]   ram_ba;
wire [1:0] core_ram_ba_err;
sig_terminal_1 #(112)	core_ram_ba0 (.sig_in(ram_ba[0]) ,.sig_out(ram_ba[0]) ,.sig_oe_(),.err(core_ram_ba_err[0]) ,.test_tri(sdram_tri),.sig_type(output_type),.clk(test_clk)	);
sig_terminal_1 #(113)	core_ram_ba1 (.sig_in(ram_ba[1]) ,.sig_out(ram_ba[1]) ,.sig_oe_(),.err(core_ram_ba_err[1]) ,.test_tri(sdram_tri),.sig_type(output_type),.clk(test_clk)	);
//error monitor
always @(posedge (|core_ram_ba_err))
	$display ("internal signals of <ram_ba> scan error:%h at %0t",core_ram_ba_err, $realtime);

//output  [31:0]  ram_dq_out;
//input   [31:0]  ram_dq_in;
//output          ram_dq_oe_;

//output      ram_cas_;
wire core_ram_cas_err;
sig_terminal_1 #(114)	core_ram_cas_(.sig_in(ram_cas_),.sig_out(ram_cas_),.sig_oe_(),.err(core_ram_cas_err),.test_tri(sdram_tri),.sig_type(output_type),.clk(test_clk)	);
always @(posedge core_ram_cas_err)
	$display ("internal signals of <ram_cas_> scan error:%h at %0t",core_ram_cas_err, $realtime);

//output      ram_we_;
wire core_ram_we_err;
sig_terminal_1 #(115)	core_ram_we_(.sig_in(ram_we_),.sig_out(ram_we_),.sig_oe_(),.err(core_ram_we_err),.test_tri(sdram_tri),.sig_type(output_type),.clk(test_clk)	);
always @(posedge core_ram_we_err)
	$display ("internal signals of <ram_we_> scan error:%h at %0t",core_ram_we_err, $realtime);

//output      ram_ras_;
wire core_ram_ras_err;
sig_terminal_1 #(116)	core_ram_ras_(.sig_in(ram_ras_),.sig_out(ram_ras_),.sig_oe_(),.err(core_ram_ras_err),.test_tri(sdram_tri),.sig_type(output_type),.clk(test_clk)	);
always @(posedge core_ram_ras_err)
	$display ("internal signals of <ram_ras_> scan error:%h at %0t",core_ram_ras_err, $realtime);

//output      ram_cke;//did not connect to pins at m6304

//output  [3:0]   ram_dqm;
wire [3:0] core_ram_dqm_err;
sig_terminal_1 #(117)	core_ram_dqm0(.sig_in(ram_dqm[0]),.sig_out(ram_dqm[0]),.sig_oe_(),.err(core_ram_dqm_err[0]),.test_tri(sdram_tri),.sig_type(output_type),.clk(test_clk)	);
sig_terminal_1 #(118)	core_ram_dqm1(.sig_in(ram_dqm[1]),.sig_out(ram_dqm[1]),.sig_oe_(),.err(core_ram_dqm_err[1]),.test_tri(sdram_tri),.sig_type(output_type),.clk(test_clk)	);
sig_terminal_1 #(119)	core_ram_dqm2(.sig_in(ram_dqm[2]),.sig_out(ram_dqm[2]),.sig_oe_(),.err(core_ram_dqm_err[2]),.test_tri(sdram_tri),.sig_type(output_type),.clk(test_clk)	);
sig_terminal_1 #(120)	core_ram_dqm3(.sig_in(ram_dqm[3]),.sig_out(ram_dqm[3]),.sig_oe_(),.err(core_ram_dqm_err[3]),.test_tri(sdram_tri),.sig_type(output_type),.clk(test_clk)	);
always @(posedge |core_ram_dqm_err)
	$display ("internal signals of <ram_dqm> scan error:%h at %0t",core_ram_dqm_err, $realtime);

//output  [1:0]   ram_cs_;//only one cs_[0] will be used
wire core_ram_cs_err;
sig_terminal_1 #(121)	core_ram_cs_0(.sig_in(ram_cs_[0]),.sig_out(ram_cs_[0]),.sig_oe_(),.err(core_ram_cs_err),.test_tri(sdram_tri),.sig_type(output_type),.clk(test_clk)	);
always @(posedge |core_ram_cs_err)
	$display ("internal signals of <ram_cs_> scan error:%h at %0t",core_ram_cs_err, $realtime);

//=====================EEPROM
//output          rom_en_;
wire	rom_en_ = ~rom_tri;
//output  [23:0]  eeprom_addr;//only connect [21:0] to the pin
//output		  rom_addr_oe;

//input   [7:0]   eeprom_rdata;
//output  [7:0]   eeprom_wdata;
//output          rom_data_oe_;


//output          eeprom_cs_;
wire core_eeprom_cs_err;
sig_terminal_1 core_eeprom_cs_(.sig_in(eeprom_cs_),.sig_out(eeprom_cs_),.sig_oe_(),.err(core_eeprom_cs_err),.test_tri(eeprom_tri),.sig_type(output_type),.clk(test_clk)	);
always @(posedge |core_eeprom_cs_err)
	$display ("internal signals of <eeprom_cs_> scan error:%h at %0t",core_eeprom_cs_err, $realtime);

//output          eeprom_oe_;
wire core_eeprom_oe_err;
sig_terminal_1 core_eeprom_oe_(.sig_in(eeprom_oe_),.sig_out(eeprom_oe_),.sig_oe_(),.err(core_eeprom_oe_err),.test_tri(eeprom_tri),.sig_type(output_type),.clk(test_clk)	);
always @(posedge |core_eeprom_oe_err)
	$display ("internal signals of <eeprom_oe_> scan error:%h at %0t",core_eeprom_oe_err, $realtime);

//output          eeprom_we_;
wire core_eeprom_we_err;
sig_terminal_1 core_eeprom_we_(.sig_in(eeprom_we_),.sig_out(eeprom_we_),.sig_oe_(),.err(core_eeprom_we_err),.test_tri(eeprom_tri),.sig_type(output_type),.clk(test_clk)	);
always @(posedge |core_eeprom_we_err)
	$display ("internal signals of <eeprom_we_> scan error:%h at %0t",core_eeprom_we_err, $realtime);


//// PCI
//output          oe_ad_;
//output  [31:0]  out_ad;
//output          oe_cbe_;
//output  [3:0]   out_cbe_;
//output          oe_frame_;
//output          out_frame_;
//output          oe_irdy_;
//output          out_irdy_;
//output          oe_devsel_;
//output          out_devsel_;
//output          oe_trdy_;
//output          out_trdy_;
//output          oe_stop_;
//output          out_stop_;
//output          oe_par_;
//output          out_par;
//
//output  [1:0]   out_gnt_;
//input   [1:0]   in_req_;
//
//input   [31:0]  in_ad;
//input   [3:0]   in_cbe_;
//
//input           in_frame_;
//input           in_irdy_;
//input           in_devsel_;
//input           in_trdy_;
//input           in_stop_;
//input           in_par;
//
//input           ext_int;
//output  [31:0]  gpio_out;
//output  [31:0]  gpio_oe_;
//input   [31:0]  gpio_in;
//
////===============Audio

//output          out_ac97_rst_; //no use to pad

//output          out_ac97_sdo;
wire core_out_ac97_sdo_err;
sig_terminal_1 core_out_ac97_sdo(.sig_in(out_ac97_sdo),.sig_out(out_ac97_sdo),.sig_oe_(),.err(core_out_ac97_sdo_err),.test_tri(test_tri),.sig_type(output_type),.clk(test_clk)	);
always @(posedge |core_out_ac97_sdo_err)
	$display ("internal signals of <out_ac97_sdo> scan error:%h at %0t",core_out_ac97_sdo_err, $realtime);

//output          out_ac97_sync;
wire core_out_ac97_sync_err;
sig_terminal_1 core_out_ac97_sync(.sig_in(out_ac97_sync),.sig_out(out_ac97_sync),.sig_oe_(),.err(core_out_ac97_sync_err),.test_tri(test_tri),.sig_type(output_type),.clk(test_clk)	);
always @(posedge |core_out_ac97_sync_err)
	$display ("internal signals of <out_ac97_sync> scan error:%h at %0t",core_out_ac97_sync_err, $realtime);

//input           in_ac97_sdi0;
wire core_in_ac97_sdi0_err;
sig_terminal_1 core_in_ac97_sdi0(.sig_in(in_ac97_sdi0),.sig_out(in_ac97_sdi0),.sig_oe_(),.err(core_in_ac97_sdi0_err),.test_tri(test_tri),.sig_type(input_type),.clk(test_clk)	);
always @(posedge |core_in_ac97_sdi0_err)
	$display ("internal signals of <in_ac97_sdi0> scan error:%h at %0t",core_in_ac97_sdi0_err, $realtime);

//input           in_ac97_sdi1;
wire core_in_ac97_sdi1_err;
sig_terminal_1 core_in_ac97_sdi1(.sig_in(in_ac97_sdi1),.sig_out(in_ac97_sdi1),.sig_oe_(),.err(core_in_ac97_sdi1_err),.test_tri(test_tri),.sig_type(input_type),.clk(test_clk)	);
always @(posedge |core_in_ac97_sdi1_err)
	$display ("internal signals of <in_ac97_sdi1> scan error:%h at %0t",core_in_ac97_sdi1_err, $realtime);

//input           ac97_clk;
wire core_ac97_clk_err;
sig_terminal_1 core_ac97_clk(.sig_in(ac97_clk),.sig_out(ac97_clk),.sig_oe_(),.err(core_ac97_clk_err),.test_tri(test_tri),.sig_type(input_type),.clk(test_clk)	);
always @(posedge |core_ac97_clk_err)
	$display ("internal signals of <ac97_clk> scan error:%h at %0t",core_ac97_clk_err, $realtime);

//
//================= I2S
//output          out_i2so_data;
wire core_out_i2so_data_err;
sig_terminal_1 core_out_i2so_data(.sig_in(out_i2so_data),.sig_out(out_i2so_data),.sig_oe_(),.err(core_out_i2so_data_err),.test_tri(test_tri),.sig_type(output_type),.clk(test_clk)	);
always @(posedge |core_out_i2so_data_err)
	$display ("internal signals of <out_i2so_data> scan error:%h at %0t",core_out_i2so_data_err, $realtime);

//output          out_i2so_lrck;
wire core_out_i2so_lrck_err;
sig_terminal_1 core_out_i2so_lrck(.sig_in(out_i2so_lrck),.sig_out(out_i2so_lrck),.sig_oe_(),.err(core_out_i2so_lrck_err),.test_tri(test_tri),.sig_type(output_type),.clk(test_clk)	);
always @(posedge |core_out_i2so_lrck_err)
	$display ("internal signals of <out_i2so_lrck> scan error:%h at %0t",core_out_i2so_lrck_err, $realtime);

//output          out_i2so_bick;
wire core_out_i2so_bick_err;
sig_terminal_1 core_out_i2so_bick(.sig_in(out_i2so_bick),.sig_out(out_i2so_bick),.sig_oe_(),.err(core_out_i2so_bick_err),.test_tri(test_tri),.sig_type(output_type),.clk(test_clk)	);
always @(posedge |core_out_i2so_bick_err)
	$display ("internal signals of <out_i2so_bick> scan error:%h at %0t",core_out_i2so_bick_err, $realtime);

//output          i2s_share_ctrl;//control i2si_data as i2s data or ac97 din1
wire i2s_share_ctrl = i2s_tri;
//input           in_i2si_data;
wire core_in_i2si_data_err;
sig_terminal_1 core_in_i2si_data(.sig_in(in_i2si_data),.sig_out(in_i2si_data),.sig_oe_(),.err(core_in_i2si_data_err),.test_tri(test_tri),.sig_type(input_type),.clk(test_clk)	);
always @(posedge |core_in_i2si_data_err)
	$display ("internal signals of <in_i2si_data> scan error:%h at %0t",core_in_i2si_data_err, $realtime);

//input           in_i2so_mclk;

////spdif
//output          out_spdif;
//output          oe_spdif;
//input           in_spdif;
wire oe_spdif_;
assign oe_spdif = ~oe_spdif_;
wire core_spdif_err;
sig_terminal_1 core_spdif(.sig_in(in_spdif),.sig_out(out_spdif),.sig_oe_(oe_spdif_),.err(core_spdif_err),.test_tri(test_tri),.sig_type(input_type),.clk(test_clk)	);
always @(posedge |core_spdif_err)
	$display ("internal signals of <spdif> scan error:%h at %0t",core_spdif_err, $realtime);


//// South Bridge
//input           cd_bck;
//input           cd_lrck;
//input           cd_data;
//input           cd_c2po;
//
//output          subq_en;
//output          subq_sqck;              // output
//input           subq_sqso;              // input
//input           subq_scor;              // input
//
//input           ir_rx;
//
//output          gsi_en;
//output          gsi_sdo;
//output          gsi_sck;
//input           gsi_sdi;
//
//output          scb_en;
//output          scb_scl_out;
//output          scb_scl_oe;
//output          scb_sda_out;
//output          scb_sda_oe;
//input           scb_scl_in;
//input           scb_sda_in;
//
//// uP interface
//input   [7:0]   m_upda_in;
//output  [7:0]   m_upda_out;
//output          m_upda_oe_;
//output  [9:0]   m_upa;
//output          m_upcs_;        //uP Host Chip Select.
//output          m_upwr_;        //uP Write strobe (Intel mode).
//output          m_uprd_;        //uP Read strobe (Intel mode).
//input           m_upwait_;
//input   [1:0]   m_updreq_;      //uP DMA Request[1:0].
//input           m_upint_;               //uP Host Interrupt Request.
//output          m_uprst_;               //uP Host Reset.
//
//input           sb_clk;
//
//// USB
//output          p1_ls_mode      ;
//output          p1_txd          ;
//output          p1_txd_oej      ;
//output          p1_txd_se0      ;
//output          p2_ls_mode      ;
//output          p2_txd          ;
//output          p2_txd_oej      ;
//output          p2_txd_se0      ;

//output          usb_pon_;
//input           i_p1_rxd        ;
//input           i_p1_rxd_se0;
//input           i_p2_rxd        ;
//input           i_p2_rxd_se0;

//input           usb_clk;
//input           usb_overcur_;
//

////======== Display Engine
//output  pclk_oe_; //control the pixclk out from clock gen
wire pclk_oe_ = 0;
//output  vdata_oe_;
//output  [7:0]   vdata_out;
wire [7:0] core_vdata_out_err;
sig_terminal_1 core_vdata_out0(.sig_in(vdata_out[0]),.sig_out(vdata_out[0]),.sig_oe_(vdata_oe_),.err(core_vdata_out_err[0]),.test_tri(test_tri),.sig_type(output_type),.clk(test_clk)	);
sig_terminal_1 core_vdata_out1(.sig_in(vdata_out[1]),.sig_out(vdata_out[1]),.sig_oe_(vdata_oe_),.err(core_vdata_out_err[1]),.test_tri(test_tri),.sig_type(output_type),.clk(test_clk)	);
sig_terminal_1 core_vdata_out2(.sig_in(vdata_out[2]),.sig_out(vdata_out[2]),.sig_oe_(vdata_oe_),.err(core_vdata_out_err[2]),.test_tri(test_tri),.sig_type(output_type),.clk(test_clk)	);
sig_terminal_1 core_vdata_out3(.sig_in(vdata_out[3]),.sig_out(vdata_out[3]),.sig_oe_(vdata_oe_),.err(core_vdata_out_err[3]),.test_tri(test_tri),.sig_type(output_type),.clk(test_clk)	);
sig_terminal_1 core_vdata_out4(.sig_in(vdata_out[4]),.sig_out(vdata_out[4]),.sig_oe_(vdata_oe_),.err(core_vdata_out_err[4]),.test_tri(test_tri),.sig_type(output_type),.clk(test_clk)	);
sig_terminal_1 core_vdata_out5(.sig_in(vdata_out[5]),.sig_out(vdata_out[5]),.sig_oe_(vdata_oe_),.err(core_vdata_out_err[5]),.test_tri(test_tri),.sig_type(output_type),.clk(test_clk)	);
sig_terminal_1 core_vdata_out6(.sig_in(vdata_out[6]),.sig_out(vdata_out[6]),.sig_oe_(vdata_oe_),.err(core_vdata_out_err[6]),.test_tri(test_tri),.sig_type(output_type),.clk(test_clk)	);
sig_terminal_1 core_vdata_out7(.sig_in(vdata_out[7]),.sig_out(vdata_out[7]),.sig_oe_(vdata_oe_),.err(core_vdata_out_err[7]),.test_tri(test_tri),.sig_type(output_type),.clk(test_clk)	);
always @(posedge |core_vdata_out_err)
	$display ("internal signals of <vdata_out> scan error:%h at %0t",core_vdata_out_err, $realtime);

//output  h_sync_out_;
//output  h_sync_oe_;
//input   in_h_sync_;
wire core_h_sync_err;
sig_terminal_1 core_h_sync(.sig_in(in_h_sync_),.sig_out(h_sync_out_),.sig_oe_(h_sync_oe_),.err(core_h_sync_err),.test_tri(test_tri),.sig_type(inout_type),.clk(test_clk)	);
always @(posedge |core_h_sync_err)
	$display ("internal signals of <h_sync> scan error:%h at %0t",core_h_sync_err, $realtime);

//output  v_sync_oe_;
//output  v_sync_out_;
//input   in_v_sync_;
wire core_v_sync_err;
sig_terminal_1 core_v_sync(.sig_in(in_v_sync_),.sig_out(v_sync_out_),.sig_oe_(v_sync_oe_),.err(core_v_sync_err),.test_tri(test_tri),.sig_type(inout_type),.clk(test_clk)	);
always @(posedge |core_v_sync_err)
	$display ("internal signals of <v_sync> scan error:%h at %0t",core_v_sync_err, $realtime);


//
//input   disp_clk;
//
//output  x_err_int;
//output  x_ext_int;
//// MIPS bus interface
//output  [31:0]  sysad_out;
//input   [31:0]  sysad_in;
//output  [4:0]   syscmd_out;
//input   [4:0]   syscmd_in;
//output          sysout_oe;
//input   [4:0]   sys_int_;
//input           sys_nmi_;
//output          pvalid_;
//output  preq_;
//output  pmaster_;
//input   eok_;
//input   evalid_;
//input   ereq_;
//
//        // JTAG interface
//input   j_tdi;
//input   j_tms;
//input   j_tclk;
//input   j_rst_;
//output  j_tdo;
//
//input           gpu_clk;
//input           cpu_clk;                        // pipeline clock   (CPU core)
//input           mem_clk;                        // transfer clock   (Memory Controller, IO, ROM)
//input           pci_clk;
//input           mbus_clk;
//input           cpu_clk_src;
//input           dsp_clk;
//input           video_clk;
//input           spdif_clk;
//input			ide100m_clk,
//				ide66m_clk;
//
//input   [31:0]  pad_boot_config;
//
//output          sw_rst_pulse;
//input           core_warmrst_;
//input           chipset_rst_;
//
//// IDE Interface
//output  atadiow_out_;
//output  atadior_out_;
//output  atacs0_out_;
//output  atacs1_out_;
//output[15:0]atadd_out;
//output[3:0]atadd_oe_;
//output[2:0] atada_out;
////output  atareset_out_;
//output  atadmack_out_;
//
//input   ataiordy_in;
//input   ataintrq_in;
//input[15:0]     atadd_in;
//input   atadmarq_in;
//
////the clock frequency select
//output  [4:0]   dsp_clk_fs;
//output  [1:0]   gpu_clk_fs;
//output  [1:0]   ve_clk_fs;
//output  [2:0]   pci_clk_out_sel;
//output          pci_clk_oe_;
//output          pci_clk_cfg;
//
//output  [3:0]   mem_dly_sel;
//
////pll program control
//output          pci_t2risc_fs_tri;
//output  [1:0]   pci_t2risc_fs_ctrldata;
//output          mem_fs_tri;
//output  [1:0]   mem_fs_ctrldata;
//output          pll_m_tri;
//output  [7:0]   pll_m_ctrldata;
//
//input           pci_t2risc_fs_ack;
//input           mem_fs_ack;
//input           pll_m_ack;
//
//input           cpu_mode;
//input           cpu_mode_;
//input           pci_mode;
//input           ide_cd_mode;
//input           up_mode;
//input           cpu_endian_mode;
//input           test_mode;
//input   [1:0]   pci_mips_fs;
//
////------------------CPU bus
//input           core_coldrst_;
//input           cpu_probe_en;

endmodule
