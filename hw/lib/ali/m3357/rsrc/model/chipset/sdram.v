// snow 1999-11-03
//
// 1M bytes SDRAM for WinCE Emulation
//
// Version 0.1 ---	1> 8-bit byte r/w mode
//			2> 1 clock r/w mode
//			3> Physical Address: 0000_0000 ~ 000F_FFFF

module	sdram	(sdram_addr,
		 sdram_dqm,
		 sdram_data,
		 sdram_cs_,
		 sdram_oe_,
		 sdram_we_,
		 last_end,
		 sdram_clk
		);

inout	[31:0]	sdram_data;
input	[27:2]	sdram_addr;
input	[3:0]	sdram_dqm;
input		sdram_cs_;
input		sdram_oe_;
input		sdram_we_;
input		last_end;
input		sdram_clk;

parameter	udly	= 1;
parameter	size	= 8*256*1024;	// 8M bytes, 2M words

reg	[7:0]	ram0 [0:size -1];	// 256K x 8
reg	[7:0]	ram1 [0:size -1];	// 256K x 8
reg	[7:0]	ram2 [0:size -1];	// 256K x 8
reg	[7:0]	ram3 [0:size -1];	// 256K x 8

//// snow 1999-11-04 ////
//reg	[31:0]	data_reg [0:size - 1];	// 256K x 32

// wire	[19:2]	ram_addr	= sdram_addr[19:2];

//// for sub_block mode
reg	[1:0]	word_cnt;	// 1, 2, 4 words
wire	[22:2]	ram_addr;
wire	[3:2]	ram_addrl;

integer ps_file;

initial
begin
//        ps_file=$fopen("ps_io.file");
	word_cnt = 0;
end

always @(posedge sdram_clk)
	if (last_end)
		word_cnt <= #udly 2'b00;
	else if (sdram_cs_ == 1'b0)
		word_cnt <= #udly word_cnt + 1;

assign		ram_addrl = sdram_addr[3:2] ^ word_cnt[1:0];
assign		ram_addr  = {sdram_addr[22:4], ram_addrl[3:2]};

wire	[7:0]	wr_byte0	= sdram_data[7:0];
wire	[15:8]	wr_byte1	= sdram_data[15:8];
wire	[23:16]	wr_byte2	= sdram_data[23:16];
wire	[31:24]	wr_byte3	= sdram_data[31:24];

/* for psemu test*/
wire    special = ram_addr == 26'h2082;
wire	[31:0]	ram_data	= special ? 32'b0 : {ram3[ram_addr],ram2[ram_addr],
				   ram1[ram_addr],ram0[ram_addr]};

assign		sdram_data	= (sdram_cs_ | sdram_oe_) ? 32'hz : ram_data[31:0];

always @(posedge sdram_clk)
	if (sdram_cs_ == 1'b0 & sdram_we_ == 1'b0) begin
		if (sdram_dqm[0] == 1'b1) begin
                        if(ram_addr == 26'h2081)
                        begin
                          $fdisplay(ps_file, "%h",wr_byte0[7:0]);
                          $display("%h",wr_byte0[7:0]);
                        end
			ram0[ram_addr] <= #udly	wr_byte0[7:0];
                end
		if (sdram_dqm[1] == 1'b1)
			ram1[ram_addr] <= #udly wr_byte1[15:8];
		if (sdram_dqm[2] == 1'b1)
			ram2[ram_addr] <= #udly wr_byte2[23:16];
		if (sdram_dqm[3] == 1'b1)
			ram3[ram_addr] <= #udly	wr_byte3[31:24];
	end
/*	else begin
		ram0[ram_addr]	<= #udly	ram0[ram_addr];
		ram1[ram_addr]	<= #udly	ram1[ram_addr];
		ram2[ram_addr]	<= #udly	ram2[ram_addr];
		ram3[ram_addr]	<= #udly	ram3[ram_addr];
	end
*///modified by yuky,2002-08-16
integer	i;

initial begin

	for (i=0;i<256*1024;i=i+1) begin
		ram0[i] = 0;
		ram1[i] = 0;
		ram2[i] = 0;
		ram3[i] = 0;
	end
end

endmodule
