/**********************************************************************************
// Description: External device behavior module for m6304;
//				It is just used to test the system level
//				logic connection.(chip,pad,padmisc)
// File:        ext_dev_bh.v
// Author:      yuky
//
// History:     2002-07-11 initial version
***********************************************************************************/
module ext_dev_bh(
// SDRAM Interface (55)
        p_dclk,         // 1,   SDRAM master clock input
        p_d_addr,       // 12,  SDRAM address
        p_d_baddr,      // 2,   SDRAM bank address
        p_d_dq,         // 32,  SDRAM 32 bits output data pins
        p_d_dqm,        // 4,   SDRAM data mask signal
        p_d_cs_,        // 1,   SDRAM chip select
        p_d_cas_,       // 1,   SDRAM cas#
        p_d_we_,        // 1,   SDRAM we#
        p_d_ras_,       // 1,   SDRAM ras#
// FLASH ROM Interface(3, others share with IDE bus)
        p_rom_ce_,      // 1,   Flash rom chip select
        p_rom_we_,      // 1,   Flash rom write enable
        p_rom_oe_,      // 1,   Flash rom output enable
// I2S Audio Interface (5)
        i2so_data,      // 1,   Serial data for I2S output
        i2so_bck,       // 1,   Bit clock for I2S output
        i2so_lrck,      // 1,   Channel clock for I2S output
        i2so_mclk,      // 1,   over sample clock for i2s DAC
        i2si_data,      // 1,   serial data for I2S input
// Videl Output Interface (12)
        pixclk,         // 1,   pixel clock
        hsync,          // 1,   horizontal sync
        vsync,          // 1,   vertical sync
        vdata,          // 8,   digital videl output data bus
        tv_env_rst_,    // 1,   TV encoder reset
// AC97/MC97 Codec Interface (4, ac97din1 share pin with i2si_data)
        ac97bclk,       // 1,   ac97 bit clock
        ac97sync,       // 1,   ac97 sync
        ac97dout,       // 1,   ac97 serial data to ac97/mc97 codec
        ac97din0,       // 1,   ac97 serial data from primary ac97/mc97 codec
// SPDIF (1)
        spdif,          // 1, inout
// CD & SUBQ port(7)
        cd_bck,         // 1, bit clock for the data from cd-dsp
        cd_lrck,        // 1, channel clock for the data from cd-dsp
        cd_data,        // 1, data from cd-dsp
        cd_c2po,        // 1, error signal from cd-dsp

        subq_scor,      // 1, scor of subq
        subq_sqso,      // 1, subq data input
        subq_sqck,      // 1, shift clock for subq
// USB port (10)
   //     usb4dp,         // 1,   D+ for USB port4
   //     usb4dn,         // 1,   D- for USB port4
   //     usb3dp,         // 1,   D+ for USB port3
   //     usb3dn,         // 1,   D- for USB port3
        usb2dp,         // 1,   D+ for USB port2
        usb2dn,         // 1,   D- for USB port2
        usb1dp,         // 1,   D+ for USB port1
        usb1dn,         // 1,   D- for USB port1
        usbpon,         // 1,   USB port power control
        usbovc,         // 1,   USB port over current
// Serial Communication Bus Interface(2)
        scbsda,         // 1,   serial data
        scbscl,         // 1,   serial clock
// General Serial Interface (3)
        gsisck,         // 1,   General Serial Interface serial clock output
        gsisdo,         // 1,   General Serial Interface serial output
        gsisdi,         // 1,   General Serial Interface serial input
// Consumer IR Interface (1)
        irrx,           // 1,   consumer infra-red remote controller/wireless keyboard
// PS Device Interface (3)
        pscs0_,         // 1,   PS device chip select for port0
        pscs1_,         // 1,   PS device chip select for port1
        psack_,         // 1,   PS device acknowledge
// General Purpose IO (16,GPIO)
        gpio,           // 16,  General purpose io[15:0]
// IDE Interface (28)
        atadiow_,       // 1,   IDE device I/O write
        atadior_,       // 1,   IDE device I/O read
        ataiordy,       // 1,   IDE I/O channel ready
        ataintrq,       // 1,   IDE device interrupt request
        atacs0_,         // 1,   IDE chip select 0
        atacs1_,         // 1,   IDE chip select 1
        atadd,          // 16,  IDE device data
        atada,          // 3,   IDE device address
        atareset_,      // 1,   IDE reset
        atadmarq,       // 1,   IDE DMA request
        atadmack_,      // 1,   IDE acknowledge
// EJTAG Interface (5)
        p_j_tdo,        // 1,   serial test data output
        p_j_tdi,        // 1,   serial test data input
        p_j_tms,        // 1,   test mode select
        p_j_tclk,       // 1,   test clock
        p_j_rst_,       // 1,   test reset
// PCI clock
        xpciclk,
// system (6)
        p_crst_,        // 1,   cold reset
        test_mode,      // 1,   chip test mode input, (for scan chain)
        p_x48in,        // 1,   crystal input (48 MHz for USB)
        p_x48out       // 1,   crystal output
//        p_x27in,        // 1,   crystal input (27 MHz for video output)
//        p_x27out        // 1,   crystal output
        );

parameter       udly = 1;
//============define the input/output/inout signals===============
//--------------SDRAM Interface (55)
input           p_dclk;                 // 1,   SDRAM master clock input
input   [11:0]  p_d_addr;               // 12,  SDRAM address
input   [1:0]   p_d_baddr;      // 2,   SDRAM bank address
inout   [31:0]  p_d_dq;         // 32,  SDRAM 32 bits output data pins
input   [3:0]   p_d_dqm;        // 4,   SDRAM data mask signal
input           p_d_cs_;        // 1,   SDRAM chip select
input           p_d_cas_;               // 1,   SDRAM cas#
input           p_d_we_;                // 1,   SDRAM we#
input           p_d_ras_;               // 1,   SDRAM ras#
//--------------FLASH ROM Interface(3, others share with IDE bus)
input           p_rom_ce_;              // 1,   Flash rom chip select
input           p_rom_we_;              // 1,   Flash rom write enable
input           p_rom_oe_;              // 1,   Flash rom output enable
//--------------I2S Audio Interface (5)
input           i2so_data;              // 1,   Serial data for I2S output
input           i2so_bck;               // 1,   Bit clock for I2S output
input           i2so_lrck;              // 1,   Channel clock for I2S output
output          i2so_mclk;              // 1,   over sample clock for i2s DAC
output          i2si_data;              // 1,   serial data for I2S input
//--------------Videl Output Interface (12)
inout           pixclk;                 // 1,   pixel clock
inout           hsync;                  // 1,   horizontal sync
inout           vsync;                  // 1,   vertical sync
input   [7:0]   vdata;          // 8,   digital videl output data bus
input           tv_env_rst_;    // 1,   TV encoder reset
//--------------AC97/MC97 Codec Interface (4, ac97din2 share pin with i2si_data)
input           ac97sync;               // 1,   ac97 sync
input           ac97dout;               // 1,   ac97 serial data to ac97/mc97 codec
output          ac97din0;               // 1,   ac97 serial data from primary ac97/mc97 codec
output          ac97bclk;               // 1,   ac97 bit clock
//--------------SPDIF (1)
inout           spdif;                  // 1, inout
//--------------cd & subq intf(7)
output          cd_bck;         // 1, bit clock for the data from cd-dsp
output          cd_lrck;        // 1, channel clock for the data from cd-dsp
output          cd_data;        // 1, data from cd-dsp
output          cd_c2po;        // 1, error signal from cd-dsp

output          subq_scor;      // 1, scor of subq
output          subq_sqso;      // 1, subq data input
input           subq_sqck;      // 1, shift clock for subq
//--------------USB port (10)
//inout           usb4dp;                 // 1,   D+ for USB port4
//inout           usb4dn;                 // 1,   D- for USB port4
//inout           usb3dp;                 // 1,   D+ for USB port3
//inout           usb3dn;                 // 1,   D- for USB port3
inout           usb2dp;                 // 1,   D+ for USB port2
inout           usb2dn;                 // 1,   D- for USB port2
inout           usb1dp;                 // 1,   D+ for USB port1
inout           usb1dn;                 // 1,   D- for USB port1
input           usbpon;                 // 1,   USB port power control
output          usbovc;                 // 1,   USB port over current
//--------------I2C Interface (2)
inout           scbsda;                 // 1,   I2C data
inout           scbscl;                 // 1,   scb clock
//--------------SPI Interface (3)
input           gsisck;                 // 1,   serial peripheral Interface serial clock output
input           gsisdo;                 // 1,   serial peripheral interface serial output
output          gsisdi;                 // 1,   serial peripheral interface serial input
//--------------Consumer IR Interface (1)
output          irrx;                   // 1,   consumer infra-red remote controller/wireless keyboard
//--------------PS Device Interface (3)
input           pscs0_;                 // 1,   PS device chip select for port0
input           pscs1_;                 // 1,   PS device chip select for port1
output          psack_;                 // 1,   PS device acknowledge
//--------------General Purpose IO (16,GPIO)
inout   [15:0]  gpio;                   // 16,  General purpose io


// IDE Interface (28)
input           atadiow_;               // 1,   IDE device I/O write
input           atadior_;               // 1,   IDE device I/O read
output          ataiordy;               // 1,   IDE I/O channel ready
output          ataintrq;               // 1,   IDE device interrupt request
input           atacs0_;                 // 1,   IDE chip select 0
input           atacs1_;                 // 1,   IDE chip select 1
inout[15:0]     atadd;                  // 16,  IDE device data
input[2:0]      atada;                  // 3,   IDE device address
input           atareset_;              // 1,   IDE reset
output          atadmarq;               // 1,   IDE DMA request
input           atadmack_;              // 1,   IDE acknowledge
//--------------EJTAG Interface (5)
input           p_j_tdo;                // 1,   serial test data output
output          p_j_tdi;                // 1,   serial test data input
output          p_j_tms;                // 1,   test mode select
output          p_j_tclk;               // 1,   test clock
output          p_j_rst_;               // 1,   test reset
// PCI clock
input           xpciclk;
//--------------system (6)
output          p_crst_;                // 1,   cold reset
output          test_mode;              // 1,   chip test mode input for scan
output          p_x48in;                // 1,   crystal input (48 MHz for USB)
input           p_x48out;               // 1,   crystal output

//invoke the signal test module to connect all the core ports signals
wire	test_clk = top.sig_tclk;
wire	test_tri = top.chip.core_bh.test_tri;
wire cpu_tri 			= top.chip.core_bh.cpu_tri 			;
wire sdram_tri 			= top.chip.core_bh.sdram_tri 		;
wire eeprom_tri 		= top.chip.core_bh.eeprom_tri 		;
wire eeprom_share_tri 	= top.chip.core_bh.eeprom_share_tri ;
wire pci_tri 			= top.chip.core_bh.pci_tri 			;
wire ide_cd_tri 		= top.chip.core_bh.ide_cd_tri 		;
wire up_tri 			= top.chip.core_bh.up_tri 			;

wire [1:0] input_type  = 2'b00;
wire [1:0] output_type = 2'b01;
wire [1:0] inout_type  = 2'b10;

wire            CPU_mode        =       top.chip.cpu_mode;
wire            PCI_mode        =       top.chip.pci_mode;
wire            IDE_CD_mode     =       top.chip.ide_cd_mode;
wire            UP_mode         =       top.chip.up_mode;
wire            Rom_en          =       ~top.chip.rom_en_;

//--------------SDRAM Interface (55)
//input           p_dclk;                 // 1,   SDRAM master clock input

//input   [11:0]  p_d_addr;               // 12,  SDRAM address
wire	[11:0] pin_p_d_addr_err;
sig_terminal_2 #(100)	pin_p_d_addr0 (.sig_inout(p_d_addr[0]) ,.err(pin_p_d_addr_err[0]) ,.test_tri(test_tri),.sig_type(input_type),.clk(test_clk));
sig_terminal_2 #(101)	pin_p_d_addr1 (.sig_inout(p_d_addr[1]) ,.err(pin_p_d_addr_err[1]) ,.test_tri(test_tri),.sig_type(input_type),.clk(test_clk));
sig_terminal_2 #(102)	pin_p_d_addr2 (.sig_inout(p_d_addr[2]) ,.err(pin_p_d_addr_err[2]) ,.test_tri(test_tri),.sig_type(input_type),.clk(test_clk));
sig_terminal_2 #(103)	pin_p_d_addr3 (.sig_inout(p_d_addr[3]) ,.err(pin_p_d_addr_err[3]) ,.test_tri(test_tri),.sig_type(input_type),.clk(test_clk));
sig_terminal_2 #(104)	pin_p_d_addr4 (.sig_inout(p_d_addr[4]) ,.err(pin_p_d_addr_err[4]) ,.test_tri(test_tri),.sig_type(input_type),.clk(test_clk));
sig_terminal_2 #(105)	pin_p_d_addr5 (.sig_inout(p_d_addr[5]) ,.err(pin_p_d_addr_err[5]) ,.test_tri(test_tri),.sig_type(input_type),.clk(test_clk));
sig_terminal_2 #(106)	pin_p_d_addr6 (.sig_inout(p_d_addr[6]) ,.err(pin_p_d_addr_err[6]) ,.test_tri(test_tri),.sig_type(input_type),.clk(test_clk));
sig_terminal_2 #(107)	pin_p_d_addr7 (.sig_inout(p_d_addr[7]) ,.err(pin_p_d_addr_err[7]) ,.test_tri(test_tri),.sig_type(input_type),.clk(test_clk));
sig_terminal_2 #(108)	pin_p_d_addr8 (.sig_inout(p_d_addr[8]) ,.err(pin_p_d_addr_err[8]) ,.test_tri(test_tri),.sig_type(input_type),.clk(test_clk));
sig_terminal_2 #(109)	pin_p_d_addr9 (.sig_inout(p_d_addr[9]) ,.err(pin_p_d_addr_err[9]) ,.test_tri(test_tri),.sig_type(input_type),.clk(test_clk));
sig_terminal_2 #(110)	pin_p_d_addr10(.sig_inout(p_d_addr[10]),.err(pin_p_d_addr_err[10]),.test_tri(test_tri),.sig_type(input_type),.clk(test_clk));
sig_terminal_2 #(111)	pin_p_d_addr11(.sig_inout(p_d_addr[11]),.err(pin_p_d_addr_err[11]),.test_tri(test_tri),.sig_type(input_type),.clk(test_clk));
//error monitor
always @(posedge (|pin_p_d_addr_err))
	$display ("Pin signals of <p_d_addr> scan error:%h at %0t",pin_p_d_addr_err, $realtime);

//input   [1:0]   p_d_baddr;      // 2,   SDRAM bank address
wire [1:0] pin_p_d_baddr_err;
sig_terminal_2 #(112) 	pin_p_d_baddr0 (.sig_inout(p_d_baddr[0]) ,.err(pin_p_d_baddr_err[0]) ,.test_tri(test_tri),.sig_type(input_type),.clk(test_clk));
sig_terminal_2 #(113) 	pin_p_d_baddr1 (.sig_inout(p_d_baddr[1]) ,.err(pin_p_d_baddr_err[1]) ,.test_tri(test_tri),.sig_type(input_type),.clk(test_clk));
//error monitor
always @(posedge (|pin_p_d_baddr_err))
	$display ("Pin signals of <p_d_baddr> scan error:%h at %0t",pin_p_d_baddr_err, $realtime);

//inout   [31:0]  p_d_dq;         // 32,  SDRAM 32 bits output data pins

//input   [3:0]   p_d_dqm;        // 4,   SDRAM data mask signal
wire [3:0] pin_p_d_dqm_err;
sig_terminal_2 #(117)	pin_p_d_dqm0 (.sig_inout(p_d_dqm[0]) ,.err(pin_p_d_dqm_err[0]) ,.test_tri(test_tri),.sig_type(input_type),.clk(test_clk));
sig_terminal_2 #(118)	pin_p_d_dqm1 (.sig_inout(p_d_dqm[1]) ,.err(pin_p_d_dqm_err[1]) ,.test_tri(test_tri),.sig_type(input_type),.clk(test_clk));
sig_terminal_2 #(119)	pin_p_d_dqm2 (.sig_inout(p_d_dqm[2]) ,.err(pin_p_d_dqm_err[2]) ,.test_tri(test_tri),.sig_type(input_type),.clk(test_clk));
sig_terminal_2 #(120)	pin_p_d_dqm3 (.sig_inout(p_d_dqm[3]) ,.err(pin_p_d_dqm_err[3]) ,.test_tri(test_tri),.sig_type(input_type),.clk(test_clk));
//error monitor
always @(posedge (|pin_p_d_dqm_err))
	$display ("Pin signals of <p_d_dqm> scan error:%h at %0t",pin_p_d_dqm_err, $realtime);

//input           p_d_cs_;        // 1,   SDRAM chip select
wire	pin_p_d_cs_err;
sig_terminal_2 #(121)	pin_p_d_cs_ (.sig_inout(p_d_cs_) ,.err(pin_p_d_cs_err) ,.test_tri(test_tri),.sig_type(input_type),.clk(test_clk));
//error monitor
always @(posedge (|pin_p_d_cs_err))
	$display ("Pin signals of <p_d_cs_> scan error:%h at %0t",pin_p_d_cs_err, $realtime);

//input           p_d_cas_;               // 1,   SDRAM cas#
wire	pin_p_d_cas_err;
sig_terminal_2 #(114)	pin_p_d_cas_ (.sig_inout(p_d_cas_) ,.err(pin_p_d_cas_err) ,.test_tri(test_tri),.sig_type(input_type),.clk(test_clk));
//error monitor
always @(posedge (|pin_p_d_cas_err))
	$display ("Pin signals of <p_d_cas_> scan error:%h at %0t",pin_p_d_cas_err, $realtime);

//input           p_d_we_;                // 1,   SDRAM we#
wire	pin_p_d_we_err;
sig_terminal_2 #(115)	pin_p_d_we_ (.sig_inout(p_d_we_) ,.err(pin_p_d_we_err) ,.test_tri(test_tri),.sig_type(input_type),.clk(test_clk));
//error monitor
always @(posedge (|pin_p_d_we_err))
	$display ("Pin signals of <p_d_we_> scan error:%h at %0t",pin_p_d_we_err, $realtime);

//input           p_d_ras_;               // 1,   SDRAM ras#
wire	pin_p_d_ras_err;
sig_terminal_2 #(116)	pin_p_d_ras_ (.sig_inout(p_d_ras_) ,.err(pin_p_d_ras_err) ,.test_tri(test_tri),.sig_type(input_type),.clk(test_clk));
//error monitor
always @(posedge (|pin_p_d_ras_err))
	$display ("Pin signals of <p_d_ras_> scan error:%h at %0t",pin_p_d_ras_err, $realtime);

//--------------FLASH ROM Interface(3, others share with IDE bus)
//input           p_rom_ce_;              // 1,   Flash rom chip select
wire	pin_p_rom_ce_err;
sig_terminal_2 pin_p_rom_ce_ (.sig_inout(p_rom_ce_) ,.err(pin_p_rom_ce_err) ,.test_tri(test_tri),.sig_type(input_type),.clk(test_clk));
//error monitor
always @(posedge (|pin_p_rom_ce_err))
	$display ("Pin signals of <p_rom_ce_> scan error:%h at %0t",pin_p_rom_ce_err, $realtime);

//input           p_rom_we_;              // 1,   Flash rom write enable
wire	pin_p_rom_we_err;
sig_terminal_2 pin_p_rom_we_ (.sig_inout(p_rom_we_) ,.err(pin_p_rom_we_err) ,.test_tri(test_tri),.sig_type(input_type),.clk(test_clk));
//error monitor
always @(posedge (|pin_p_rom_we_err))
	$display ("Pin signals of <p_rom_we_> scan error:%h at %0t",pin_p_rom_we_err, $realtime);

//input           p_rom_oe_;              // 1,   Flash rom output enable
wire	pin_p_rom_oe_err;
sig_terminal_2 pin_p_rom_oe_ (.sig_inout(p_rom_oe_) ,.err(pin_p_rom_oe_err) ,.test_tri(test_tri),.sig_type(input_type),.clk(test_clk));
//error monitor
always @(posedge (|pin_p_rom_oe_err))
	$display ("Pin signals of <p_rom_oe_> scan error:%h at %0t",pin_p_rom_oe_err, $realtime);

//--------------I2S Audio Interface (5)
//input           i2so_data;              // 1,   Serial data for I2S output
wire	pin_i2so_data_err;
sig_terminal_2 pin_i2so_data (.sig_inout(i2so_data) ,.err(pin_i2so_data_err) ,.test_tri(test_tri),.sig_type(input_type),.clk(test_clk));
//error monitor
always @(posedge (|pin_i2so_data_err))
	$display ("Pin signals of <i2so_data> scan error:%h at %0t",pin_i2so_data_err, $realtime);

//input           i2so_bck;               // 1,   Bit clock for I2S output
wire	pin_i2so_bck_err;
sig_terminal_2 pin_i2so_bck (.sig_inout(i2so_bck) ,.err(pin_i2so_bck_err) ,.test_tri(test_tri),.sig_type(input_type),.clk(test_clk));
//error monitor
always @(posedge (|pin_i2so_bck_err))
	$display ("Pin signals of <i2so_bck> scan error:%h at %0t",pin_i2so_bck_err, $realtime);

//input           i2so_lrck;              // 1,   Channel clock for I2S output
wire	pin_i2so_lrck_err;
sig_terminal_2 pin_i2so_lrck (.sig_inout(i2so_lrck) ,.err(pin_i2so_lrck_err) ,.test_tri(test_tri),.sig_type(input_type),.clk(test_clk));
//error monitor
always @(posedge (|pin_i2so_lrck_err))
	$display ("Pin signals of <i2so_lrck> scan error:%h at %0t",pin_i2so_lrck_err, $realtime);

//output          i2so_mclk;              // 1,   over sample clock for i2s DAC
//clock signals, don't test here

//output          i2si_data;              // 1,   serial data for I2S input
wire pin_i2si_data_err;
sig_terminal_2 pin_i2si_data (.sig_inout(i2si_data) ,.err(pin_i2si_data_err) ,.test_tri(test_tri),.sig_type(output_type),.clk(test_clk));
//error monitor
always @(posedge (|pin_i2si_data_err))
	$display ("Pin signals of <i2si_data> scan error:%h at %0t",pin_i2si_data_err, $realtime);

//--------------Videl Output Interface (12)
//input           pixclk;                 // 1,   pixel clock

//inout           hsync;                  // 1,   horizontal sync
wire pin_hsync_err;
sig_terminal_2 pin_hsync (.sig_inout(hsync) ,.err(pin_hsync_err) ,.test_tri(test_tri),.sig_type(inout_type),.clk(test_clk));
//error monitor
always @(posedge (|pin_hsync_err))
	$display ("Pin signals of <hsync> scan error:%h at %0t",pin_hsync_err, $realtime);


//inout           vsync;                  // 1,   vertical sync
wire pin_vsync_err;
sig_terminal_2 pin_vsync (.sig_inout(vsync) ,.err(pin_vsync_err) ,.test_tri(test_tri),.sig_type(inout_type),.clk(test_clk));
//error monitor
always @(posedge (|pin_vsync_err))
	$display ("Pin signals of <vsync> scan error:%h at %0t",pin_vsync_err, $realtime);

//input   [7:0]   vdata;          // 8,   digital videl output data bus
wire [7:0] pin_vdata_err;
sig_terminal_2 pin_vdata0 (.sig_inout(vdata[0]) ,.err(pin_vdata_err[0]) ,.test_tri(test_tri),.sig_type(input_type),.clk(test_clk));
sig_terminal_2 pin_vdata1 (.sig_inout(vdata[1]) ,.err(pin_vdata_err[1]) ,.test_tri(test_tri),.sig_type(input_type),.clk(test_clk));
sig_terminal_2 pin_vdata2 (.sig_inout(vdata[2]) ,.err(pin_vdata_err[2]) ,.test_tri(test_tri),.sig_type(input_type),.clk(test_clk));
sig_terminal_2 pin_vdata3 (.sig_inout(vdata[3]) ,.err(pin_vdata_err[3]) ,.test_tri(test_tri),.sig_type(input_type),.clk(test_clk));
sig_terminal_2 pin_vdata4 (.sig_inout(vdata[4]) ,.err(pin_vdata_err[4]) ,.test_tri(test_tri),.sig_type(input_type),.clk(test_clk));
sig_terminal_2 pin_vdata5 (.sig_inout(vdata[5]) ,.err(pin_vdata_err[5]) ,.test_tri(test_tri),.sig_type(input_type),.clk(test_clk));
sig_terminal_2 pin_vdata6 (.sig_inout(vdata[6]) ,.err(pin_vdata_err[6]) ,.test_tri(test_tri),.sig_type(input_type),.clk(test_clk));
sig_terminal_2 pin_vdata7 (.sig_inout(vdata[7]) ,.err(pin_vdata_err[7]) ,.test_tri(test_tri),.sig_type(input_type),.clk(test_clk));
//error monitor
always @(posedge (|pin_vdata_err))
	$display ("Pin signals of <vdata> scan error:%h at %0t",pin_vdata_err, $realtime);

//input           tv_env_rst_;    // 1,   TV encoder reset
wire pin_tv_env_rst_err;
sig_terminal_2 pin_tv_env_rst_ (.sig_inout(tv_env_rst_) ,.err(pin_tv_env_rst_err) ,.test_tri(test_tri),.sig_type(input_type),.clk(test_clk));
//error monitor
always @(posedge (|pin_tv_env_rst_err))
	$display ("Pin signals of <tv_env_rst_> scan error:%h at %0t",pin_tv_env_rst_err, $realtime);

//--------------AC97/MC97 Codec Interface (4, ac97din2 share pin with i2si_data)
//input           ac97sync;               // 1,   ac97 sync
wire pin_ac97sync_err;
sig_terminal_2 pin_ac97sync (.sig_inout(ac97sync) ,.err(pin_ac97sync_err) ,.test_tri(test_tri),.sig_type(input_type),.clk(test_clk));
//error monitor
always @(posedge (|pin_ac97sync_err))
	$display ("Pin signals of <ac97sync> scan error:%h at %0t",pin_ac97sync_err, $realtime);

//input           ac97dout;               // 1,   ac97 serial data to ac97/mc97 codec
wire pin_ac97dout_err;
sig_terminal_2 pin_ac97dout (.sig_inout(ac97dout) ,.err(pin_ac97dout_err) ,.test_tri(test_tri),.sig_type(input_type),.clk(test_clk));
//error monitor
always @(posedge (|pin_ac97dout_err))
	$display ("Pin signals of <ac97dout> scan error:%h at %0t",pin_ac97dout_err, $realtime);

//output          ac97din0;               // 1,   ac97 serial data from primary ac97/mc97 codec
wire pin_ac97din0_err;
sig_terminal_2 pin_ac97din0 (.sig_inout(ac97din0) ,.err(pin_ac97din0_err) ,.test_tri(test_tri),.sig_type(output_type),.clk(test_clk));
//error monitor
always @(posedge (|pin_ac97din0_err))
	$display ("Pin signals of <ac97din0> scan error:%h at %0t",pin_ac97din0_err, $realtime);

//output          ac97bclk;               // 1,   ac97 bit clock
wire pin_ac97bclk_err;
sig_terminal_2 pin_ac97bclk (.sig_inout(ac97bclk) ,.err(pin_ac97bclk_err) ,.test_tri(test_tri),.sig_type(output_type),.clk(test_clk));
//error monitor
always @(posedge (|pin_ac97bclk_err))
	$display ("Pin signals of <ac97bclk> scan error:%h at %0t",pin_ac97bclk_err, $realtime);

//--------------SPDIF (1)
//inout           spdif;                  // 1, inout
wire pin_spdif_err;
sig_terminal_2 pin_spdif (.sig_inout(spdif) ,.err(pin_spdif_err) ,.test_tri(test_tri),.sig_type(inout_type),.clk(test_clk));
//error monitor
always @(posedge (|pin_spdif_err))
	$display ("Pin signals of <spdif> scan error:%h at %0t",pin_spdif_err, $realtime);

//--------------cd & subq intf(7)
//output          cd_bck;         // 1, bit clock for the data from cd-dsp
//output          cd_lrck;        // 1, channel clock for the data from cd-dsp
//output          cd_data;        // 1, data from cd-dsp
//output          cd_c2po;        // 1, error signal from cd-dsp

//output          subq_scor;      // 1, scor of subq
//output          subq_sqso;      // 1, subq data input
//input           subq_sqck;      // 1, shift clock for subq
//--------------USB port (10)
//inout           usb4dp;                 // 1,   D+ for USB port4
//inout           usb4dn;                 // 1,   D- for USB port4
//inout           usb3dp;                 // 1,   D+ for USB port3
//inout           usb3dn;                 // 1,   D- for USB port3
//inout           usb2dp;                 // 1,   D+ for USB port2
//inout           usb2dn;                 // 1,   D- for USB port2
//inout           usb1dp;                 // 1,   D+ for USB port1
//inout           usb1dn;                 // 1,   D- for USB port1
//input           usbpon;                 // 1,   USB port power control
//output          usbovc;                 // 1,   USB port over current
//--------------I2C Interface (2)
//inout           scbsda;                 // 1,   I2C data
//inout           scbscl;                 // 1,   scb clock
//--------------SPI Interface (3)
//input           gsisck;                 // 1,   serial peripheral Interface serial clock output
//input           gsisdo;                 // 1,   serial peripheral interface serial output
//output          gsisdi;                 // 1,   serial peripheral interface serial input
//--------------Consumer IR Interface (1)
//output          irrx;                   // 1,   consumer infra-red remote controller/wireless keyboard
//--------------PS Device Interface (3)
//input           pscs0_;                 // 1,   PS device chip select for port0
//input           pscs1_;                 // 1,   PS device chip select for port1
//output          psack_;                 // 1,   PS device acknowledge
//--------------General Purpose IO (16,GPIO)
//inout   [15:0]  gpio;                   // 16,  General purpose io


// IDE Interface (28)
//input           atadiow_;               // 1,   IDE device I/O write
//input           atadior_;               // 1,   IDE device I/O read
//output          ataiordy;               // 1,   IDE I/O channel ready
//output          ataintrq;               // 1,   IDE device interrupt request
//input           atacs0_;                 // 1,   IDE chip select 0
//input           atacs1_;                 // 1,   IDE chip select 1
//inout[15:0]     atadd;                  // 16,  IDE device data
//input[2:0]      atada;                  // 3,   IDE device address
//input           atareset_;              // 1,   IDE reset
//output          atadmarq;               // 1,   IDE DMA request
//input           atadmack_;              // 1,   IDE acknowledge
//--------------EJTAG Interface (5)
//input           p_j_tdo;                // 1,   serial test data output
//output          p_j_tdi;                // 1,   serial test data input
//output          p_j_tms;                // 1,   test mode select
//output          p_j_tclk;               // 1,   test clock
//output          p_j_rst_;               // 1,   test reset
// PCI clock
//input           xpciclk;
//--------------system (6)
//output          p_crst_;                // 1,   cold reset
//output          test_mode;              // 1,   chip test mode input for scan
//output          p_x48in;                // 1,   crystal input (48 MHz for USB)
//input           p_x48out;               // 1,   crystal output

endmodule