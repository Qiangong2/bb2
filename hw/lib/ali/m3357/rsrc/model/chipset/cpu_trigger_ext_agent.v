/***********************************************************
File:		cpu_trigger_ext_agent.v
Description:Control cpu whether enter trigger test mode.
			In the test mode, check the test whether finish
			and report the test whether correct.
			all the method is come from SH.
Author:		yuky
History:	2002-10-17	Initial version
************************************************************/
module cpu_trigger_ext_agent(
	//input
	i_cpu_testout,
	i_cpu_trig_signal,
	//output
	o_cpu_testin,	//tri-state signal
	o_cpu_trigcond,	//tri-state signal
	o_cpu_trig_en,	//tri-state signal
	//sys
	clk,			//cpu pipeline clock
	rst_			//cpu core warm reset
	);
input	[39:0]	i_cpu_testout;
input			i_cpu_trig_signal;
output	[4:0]	o_cpu_testin;
output	[9:0]	o_cpu_trigcond;
output			o_cpu_trig_en;
input			clk, rst_;

`define trig_top_path top.CHIP.CORE.T2_CPU.t2_risc_top	//M6304 hierarchy
`define jtlb_path 	`trig_top_path.t2_mmu.jtlb_hb
`define dtlb_path 	`trig_top_path.t2_mmu.dtlb_hb
`define itlb_path 	`trig_top_path.t2_mmu.itlb_hb
`define dtag_path 	`trig_top_path.t2_d_cache.d_cachefile_1
`define itag_path 	`trig_top_path.t2_ifetch.i_cachefile_1.i_tag
`define ddata_path 	`trig_top_path.t2_d_cache.d_cachefile_1
`define idata_path 	`trig_top_path.t2_ifetch.i_cachefile_1
`define rf_path 	`trig_top_path.t2_reg_file.gpr
`define vrfh_path 	`trig_top_path.t2_vrf.ERFH
`define vrfl_path 	`trig_top_path.t2_vrf.ERFL

`ifdef	CHIP_GSIM
`define trig_top_path top.chip.core.T2_CPU.T2_RISC_TOP	//M6304 hierarchy
`define jtlb_path 	`trig_top_path.T2_MMU.JTLB_HB
`define dtlb_path 	`trig_top_path.T2_MMU.DTLB_HB
`define itlb_path 	`trig_top_path.T2_MMU.ITLB_HB
`define dtag_path 	`trig_top_path.T2_D_CACHE.D_CACHEFILE_1
`define itag_path 	`trig_top_path.T2_IFETCH.I_CACHEFILE_1.I_TAG
`define ddata_path 	`trig_top_path.T2_D_CACHE.D_CACHEFILE_1
`define idata_path 	`trig_top_path.T2_IFETCH.I_CACHEFILE_1
`define rf_path 	`trig_top_path.T2_REG_FILE.GPR
`define vrfh_path 	`trig_top_path.T2_VRF.ERFH
`define vrfl_path 	`trig_top_path.T2_VRF.ERFL
`endif

parameter udly = 1;
reg				cpu_trig_en;
reg [9:0]		cpu_trigcond;
reg	[4:0]		cpu_testin;

initial begin
		cpu_trig_en		=	1'b0;
		cpu_trigcond	=	10'b0;
		cpu_testin		=	5'b0;
		end

		//new version
wire    [39:0]  jtlb_testout = {18'b0,
                                ^`jtlb_path.CORE_VPNIN,
                                 `jtlb_path.CORE_TLBRD,
                                 `jtlb_path.CORE_TLBWR,
                                 `jtlb_path.CORE_RW_INDEX,
                                ^`jtlb_path.CORE_WR_DATA,
                              	 `jtlb_path.CORE_TLBP,
                                 `jtlb_path.CORE_TLBID,
                                 `jtlb_path.CS,
                                 `jtlb_path.ENTRY_OUT[0],
                                ^`jtlb_path.ENTRY_DATA,
                                 `jtlb_path.SEL_ODD,
                                 `jtlb_path.CORE_HIT_INDEX,
                                 `jtlb_path.CORE_HIT,
                                 `jtlb_path.CORE_MULTIHIT
                               };

wire    [39:0]  dtlb_testout = {17'b0,
                                ^`dtlb_path.VPN_IN,
                                 `dtlb_path.COMP_EN,
                                 `dtlb_path.FLUSH_VEC,
                                ^`dtlb_path.UPDATE_DATA,
                                 `dtlb_path.UPDATE_EN,
                                 `dtlb_path.UPDATE_VEC,
                                ^`dtlb_path.CP0_ASID,
                                ^`dtlb_path.BYPASS_MODE_IN,
                                ^`dtlb_path.CONFIG_K0_IN,
                                 `dtlb_path.HIT_IN_DTLB,
                                 `dtlb_path.HIT_VEC,
                                ^`dtlb_path.ENTRY_PFN,
                                ^`dtlb_path.ENTRY_CACHE,
                                 `dtlb_path.ENTRY_DIRTY
                                };

wire    [39:0]  itlb_testout = {17'b0,
                                ^`itlb_path.VPN_IN,
                                 `itlb_path.COMP_EN,
                                 `itlb_path.FLUSH_VEC,
                                ^`itlb_path.UPDATE_DATA,
                                 `itlb_path.UPDATE_EN,
                                 `itlb_path.UPDATE_VEC,
                                ^`itlb_path.CP0_ASID,
                                ^`itlb_path.BYPASS_MODE_IN,
                                ^`itlb_path.CONFIG_K0_IN,
                                 `itlb_path.HIT_IN_DTLB,
                                 `itlb_path.HIT_VEC,
                                ^`itlb_path.ENTRY_PFN,
                                ^`itlb_path.ENTRY_CACHE,
                                 `itlb_path.ENTRY_DIRTY
                                };
`ifdef	CHIP_GSIM
wire	[39:0]	itag_testout = 0;
wire	[39:0]	dtag_testout = 0;
wire	[39:0]	idata_testout = 0;
wire	[39:0]	ddata_testout = 0;
`else
wire    [39:0]  itag_testout = {3'b0,
                                ^`itag_path.ic_sram_tag_in_b,
                                 `itag_path.ic_sram_tag_addr_b,
                                ^`itag_path.ic_sram_lru_in_b,
                                 `itag_path.ic_sram_tag_1_cs_b_,
                                 `itag_path.ic_sram_tag_0_cs_b_,
                                 `itag_path.ic_sram_wr_b_,
                                 `itag_path.ic_sram_invld_b_,
                                 `itag_path.ic_sram_indexop_b_,
                                 `itag_path.ic_sram_way_sel_b,
                                ^`itag_path.ic_sram_paddr_b,
                                 `itag_path.ic_sram_way_option_b,
                                 `itag_path.ic_sram_way_addr_b,
                                ^`itag_path.ic_sram_1_tag3_out,
                                ^`itag_path.ic_sram_1_tag2_out,
                                ^`itag_path.ic_sram_1_tag1_out,
                                ^`itag_path.ic_sram_1_tag0_out,
                                ^`itag_path.ic_sram_0_tag3_out,
                                ^`itag_path.ic_sram_0_tag2_out,
                                ^`itag_path.ic_sram_0_tag1_out,
                                ^`itag_path.ic_sram_0_tag0_out,
                                 `itag_path.ic_sram_1_hit_vec,
                                 `itag_path.ic_sram_0_hit_vec,
                                ^`itag_path.ic_sram_1_lru_out,
                                ^`itag_path.ic_sram_0_lru_out
                                };

wire    [39:0]  dtag_testout = {3'b0,
                                ^`dtag_path.ram_tag_in,
                                 `dtag_path.ram_tag_a_in[6:0],
                                ^`dtag_path.ram_lru_in,
                                 `dtag_path.tag1_cs0_,
                                 `dtag_path.tag0_cs0_,
                                 `dtag_path.wr_,
                                 `dtag_path.dc_sram_invalid_,
                                 `dtag_path.dc_sram_indexop_,
                                 `dtag_path.way_sel,
                                ^`dtag_path.dc_tag_paddr,
                                 `dtag_path.dc_way_option0,
                                 `dtag_path.ram_tag_a_in[7],
                                ^`dtag_path.ram_tag_1_3_out,
                                ^`dtag_path.ram_tag_1_2_out,
                                ^`dtag_path.ram_tag_1_1_out,
                                ^`dtag_path.ram_tag_1_0_out,
                                ^`dtag_path.ram_tag_0_3_out,
                                ^`dtag_path.ram_tag_0_2_out,
                                ^`dtag_path.ram_tag_0_1_out,
                                ^`dtag_path.ram_tag_0_0_out,
                                 `dtag_path.d_hit_1_3,
                                 `dtag_path.d_hit_1_2,
                                 `dtag_path.d_hit_1_1,
                                 `dtag_path.d_hit_1_0,
                                 `dtag_path.d_hit_0_3,
                                 `dtag_path.d_hit_0_2,
                                 `dtag_path.d_hit_0_1,
                                 `dtag_path.d_hit_0_0,
                                ^`dtag_path.ram_lru_1_out,
                                ^`dtag_path.ram_lru_0_out
                                };

wire    [39:0]  idata_testout = {18'b0,
                                 `idata_path.ram_data_1_1_cen_b_,
                                 `idata_path.ram_data_1_0_cen_b_,
                                 `idata_path.ram_data_0_1_cen_b_,
                                 `idata_path.ram_data_0_0_cen_b_,
                                 `idata_path.ram_data_wen_b_,
                                 `idata_path.ram_data_addr_b,
                                ^`idata_path.ram_data_1_1_in,
                                ^`idata_path.ram_data_1_0_in,
                                ^`idata_path.ram_data_0_1_in,
                                ^`idata_path.ram_data_0_0_in,
                                ^`idata_path.ram_data_1_1_out,
                                ^`idata_path.ram_data_1_0_out,
                                ^`idata_path.ram_data_0_1_out,
                                ^`idata_path.ram_data_0_0_out
                                };

wire    [39:0]  ddata_testout = {18'b0,
                                 `ddata_path.data_0_0_cs0_,
                                 `ddata_path.data_0_1_cs0_,
                                 `ddata_path.data_1_0_cs0_,
                                 `ddata_path.data_1_1_cs0_,
                                 `ddata_path.wen_,
                                 `ddata_path.ram_data_a_in,
                                ^`ddata_path.ram_data_1_1_in0,
                                ^`ddata_path.ram_data_1_0_in0,
                                ^`ddata_path.ram_data_0_1_in0,
                                ^`ddata_path.ram_data_0_0_in0,
                                ^`ddata_path.ram_data_1_1_out,
                                ^`ddata_path.ram_data_1_0_out,
                                ^`ddata_path.ram_data_0_1_out,
                                ^`ddata_path.ram_data_0_0_out
                                };
`endif

wire    [9:0]   rf_addr_rarb  = `rf_path.ID_ADDR_SEL0 ? `rf_path.ID_RSRTRD_REG0[14:5] :
                                `rf_path.ID_ADDR_SEL1 ? `rf_path.ID_RSRTRD_REG1[14:5] :
                                                        `rf_path.ID_RSRTRD_IN0[14:5];
wire    [9:0]   rf_addr_rcrd  = `rf_path.ID_ADDR_SEL0 ? `rf_path.ID_RSRTRD_REG1[14:5] :
                                `rf_path.ID_ADDR_SEL1 ? `rf_path.ID_RSRTRD_IN0[14:5]  :
                                                        `rf_path.ID_RSRTRD_IN1[14:5];
wire    [9:0]   rf_addr_cacb  = `rf_path.ID_ADDR_SEL0 ? {`rf_path.ID_RSRTRD_REG0[4:0],`rf_path.ID_RSRTRD_REG1[4:0]} :
                                `rf_path.ID_ADDR_SEL1 ? {`rf_path.ID_RSRTRD_REG1[4:0],`rf_path.ID_RSRTRD_IN0[4:0]}  :
                                                        {`rf_path.ID_RSRTRD_IN0[4:0], `rf_path.ID_RSRTRD_IN1[4:0]};

wire    [39:0]  rf_testout_0  = {1'b0,
                                 `rf_path.RF_AWA,
                                 `rf_path.RF_AWB,
                                ^`rf_path.RF_DWA,
                                ^`rf_path.RF_DWB,
                                 `rf_path.RF_CENWA_,
                                 `rf_path.RF_CENWB_,
                                 `rf_path.RF_CENSTWA_,
                                 `rf_path.RF_CENSTWB_,
                                 `rf_path.RF_CENSTWC_,
                                 `rf_path.RF_CENSTWD_,
                                 `rf_path.RF_CENRA_,
                                 `rf_path.RF_CENRB_,
                                  rf_addr_rarb,
                                 `rf_path.ID_ADDR_SEL0,
                                 `rf_path.ID_ADDR_SEL1,
                                 `rf_path.RST_,
                                 `rf_path.RF_RAREDO_EN,
                                 `rf_path.RF_RBREDO_EN,
                                ^`rf_path.RF_QRA,
                                ^`rf_path.RF_QRB,
                                 `rf_path.RF_STRA[0],
                                 `rf_path.RF_STRB[0]
                                };
wire    [39:0]  rf_testout_1  = {1'b0,
                                 `rf_path.RF_AWC,
                                 `rf_path.RF_AWD,
                                ^`rf_path.RF_DWC,
                                ^`rf_path.RF_DWD,
                                 `rf_path.RF_CENWC_,
                                 `rf_path.RF_CENWD_,
                                 `rf_path.RF_CENSTWC_,
                                 `rf_path.RF_CENSTWD_,
                                 `rf_path.RF_CENSTWA_,
                                 `rf_path.RF_CENSTWB_,
                                 `rf_path.RF_CENRC_,
                                 `rf_path.RF_CENRD_,
                                  rf_addr_rcrd,
                                 `rf_path.ID_ADDR_SEL0,
                                 `rf_path.ID_ADDR_SEL1,
                                 `rf_path.RST_,
                                 `rf_path.RF_RCREDO_EN,
                                 `rf_path.RF_RDREDO_EN,
                                ^`rf_path.RF_QRC,
                                ^`rf_path.RF_QRD,
                                 `rf_path.RF_STRC[0],
                                 `rf_path.RF_STRD[0]
                                };
wire    [39:0]  rf_testout_2  = {1'b0,
                                 `rf_path.RF_AWA,
                                 `rf_path.RF_AWB,
                                ^`rf_path.RF_DWA,
                                ^`rf_path.RF_DWB,
                                 `rf_path.RF_CENWA_,
                                 `rf_path.RF_CENWB_,
                                 `rf_path.RF_CENSTWA_,
                                 `rf_path.RF_CENSTWB_,
                                 `rf_path.RF_CENSTWC_,
                                 `rf_path.RF_CENSTWD_,
                                 `rf_path.RF_CENCA_,
                                 `rf_path.RF_CENCB_,
                                 `rf_path.DISP_CA_S,
                                 `rf_path.DISP_CB_S,
                                 `rf_path.RD_ENCA,
                                 `rf_path.RD_ENCB,
                                  rf_addr_cacb,
                                 `rf_path.RST_,
                                 `rf_path.RF_CAREDO_EN,
                                 `rf_path.RF_CBREDO_EN,
                                 `rf_path.RF_STCA[0],
                                 `rf_path.RF_STCB[0]
                                };

wire    [9:0]   vrfh_addr_rarb  = `vrfh_path.ID_ADDR_SEL0 ? `vrfh_path.ID_RSRTRD_REG0[14:5] :
                                  `vrfh_path.ID_ADDR_SEL1 ? `vrfh_path.ID_RSRTRD_REG1[14:5] :
                                                            `vrfh_path.ID_RSRTRD_IN0[14:5];
wire    [9:0]   vrfh_addr_rcrd  = `vrfh_path.ID_ADDR_SEL0 ? `vrfh_path.ID_RSRTRD_REG1[14:5] :
                                  `vrfh_path.ID_ADDR_SEL1 ? `vrfh_path.ID_RSRTRD_IN0[14:5]  :
                                                            `vrfh_path.ID_RSRTRD_IN1[14:5];
wire    [9:0]   vrfh_addr_cacb  = `vrfh_path.ID_ADDR_SEL0 ? {`vrfh_path.ID_RSRTRD_REG0[4:0],`vrfh_path.ID_RSRTRD_REG1[4:0]} :
                                  `vrfh_path.ID_ADDR_SEL1 ? {`vrfh_path.ID_RSRTRD_REG1[4:0],`vrfh_path.ID_RSRTRD_IN0[4:0]}  :
                                                            {`vrfh_path.ID_RSRTRD_IN0[4:0], `vrfh_path.ID_RSRTRD_IN1[4:0]};

wire    [39:0]  vrfh_testout_0  = {1'b0,
                                 `vrfh_path.RF_AWA,
                                 `vrfh_path.RF_AWB,
                                ^`vrfh_path.RF_DWA,
                                ^`vrfh_path.RF_DWB,
                                 `vrfh_path.RF_CENWA_,
                                 `vrfh_path.RF_CENWB_,
                                 `vrfh_path.RF_CENSTWA_,
                                 `vrfh_path.RF_CENSTWB_,
                                 `vrfh_path.RF_CENSTWC_,
                                 `vrfh_path.RF_CENSTWD_,
                                 `vrfh_path.RF_CENRA_,
                                 `vrfh_path.RF_CENRB_,
                                  vrfh_addr_rarb,
                                 `vrfh_path.ID_ADDR_SEL0,
                                 `vrfh_path.ID_ADDR_SEL1,
                                 `vrfh_path.RST_,
                                 `vrfh_path.RF_RAREDO_EN,
                                 `vrfh_path.RF_RBREDO_EN,
                                ^`vrfh_path.RF_QRA,
                                ^`vrfh_path.RF_QRB,
                                 `vrfh_path.RF_STRA[0],
                                 `vrfh_path.RF_STRB[0]
                                };
wire    [39:0]  vrfh_testout_1  = {1'b0,
                                 `vrfh_path.RF_AWC,
                                 `vrfh_path.RF_AWD,
                                ^`vrfh_path.RF_DWC,
                                ^`vrfh_path.RF_DWD,
                                 `vrfh_path.RF_CENWC_,
                                 `vrfh_path.RF_CENWD_,
                                 `vrfh_path.RF_CENSTWC_,
                                 `vrfh_path.RF_CENSTWD_,
                                 `vrfh_path.RF_CENSTWA_,
                                 `vrfh_path.RF_CENSTWB_,
                                 `vrfh_path.RF_CENRC_,
                                 `vrfh_path.RF_CENRD_,
                                  vrfh_addr_rcrd,
                                 `vrfh_path.ID_ADDR_SEL0,
                                 `vrfh_path.ID_ADDR_SEL1,
                                 `vrfh_path.RST_,
                                 `vrfh_path.RF_RCREDO_EN,
                                 `vrfh_path.RF_RDREDO_EN,
                                ^`vrfh_path.RF_QRC,
                                ^`vrfh_path.RF_QRD,
                                 `vrfh_path.RF_STRC[0],
                                 `vrfh_path.RF_STRD[0]
                                };
wire    [39:0]  vrfh_testout_2  = {1'b0,
                                 `vrfh_path.RF_AWA,
                                 `vrfh_path.RF_AWB,
                                ^`vrfh_path.RF_DWA,
                                ^`vrfh_path.RF_DWB,
                                 `vrfh_path.RF_CENWA_,
                                 `vrfh_path.RF_CENWB_,
                                 `vrfh_path.RF_CENSTWA_,
                                 `vrfh_path.RF_CENSTWB_,
                                 `vrfh_path.RF_CENSTWC_,
                                 `vrfh_path.RF_CENSTWD_,
                                 `vrfh_path.RF_CENCA_,
                                 `vrfh_path.RF_CENCB_,
                                 `vrfh_path.DISP_CA_S,
                                 `vrfh_path.DISP_CB_S,
                                 `vrfh_path.RD_ENCA,
                                 `vrfh_path.RD_ENCB,
                                  vrfh_addr_cacb,
                                 `vrfh_path.RST_,
                                 `vrfh_path.RF_CAREDO_EN,
                                 `vrfh_path.RF_CBREDO_EN,
                                 `vrfh_path.RF_STCA[0],
                                 `vrfh_path.RF_STCB[0]
                                };

wire    [9:0]   vrfl_addr_rarb  = `vrfl_path.ID_ADDR_SEL0 ? `vrfl_path.ID_RSRTRD_REG0[14:5] :
                                  `vrfl_path.ID_ADDR_SEL1 ? `vrfl_path.ID_RSRTRD_REG1[14:5] :
                                                            `vrfl_path.ID_RSRTRD_IN0[14:5];
wire    [9:0]   vrfl_addr_rcrd  = `vrfl_path.ID_ADDR_SEL0 ? `vrfl_path.ID_RSRTRD_REG1[14:5] :
                                  `vrfl_path.ID_ADDR_SEL1 ? `vrfl_path.ID_RSRTRD_IN0[14:5]  :
                                                            `vrfl_path.ID_RSRTRD_IN1[14:5];
wire    [9:0]   vrfl_addr_cacb  = `vrfl_path.ID_ADDR_SEL0 ? {`vrfl_path.ID_RSRTRD_REG0[4:0],`vrfl_path.ID_RSRTRD_REG1[4:0]} :
                                  `vrfl_path.ID_ADDR_SEL1 ? {`vrfl_path.ID_RSRTRD_REG1[4:0],`vrfl_path.ID_RSRTRD_IN0[4:0]}  :
                                                            {`vrfl_path.ID_RSRTRD_IN0[4:0], `vrfl_path.ID_RSRTRD_IN1[4:0]};

wire    [39:0]  vrfl_testout_0  = {1'b0,
                                 `vrfl_path.RF_AWA,
                                 `vrfl_path.RF_AWB,
                                ^`vrfl_path.RF_DWA,
                                ^`vrfl_path.RF_DWB,
                                 `vrfl_path.RF_CENWA_,
                                 `vrfl_path.RF_CENWB_,
                                 `vrfl_path.RF_CENSTWA_,
                                 `vrfl_path.RF_CENSTWB_,
                                 `vrfl_path.RF_CENSTWC_,
                                 `vrfl_path.RF_CENSTWD_,
                                 `vrfl_path.RF_CENRA_,
                                 `vrfl_path.RF_CENRB_,
                                  vrfl_addr_rarb,
                                 `vrfl_path.ID_ADDR_SEL0,
                                 `vrfl_path.ID_ADDR_SEL1,
                                 `vrfl_path.RST_,
                                 `vrfl_path.RF_RAREDO_EN,
                                 `vrfl_path.RF_RBREDO_EN,
                                ^`vrfl_path.RF_QRA,
                                ^`vrfl_path.RF_QRB,
                                 `vrfl_path.RF_STRA[0],
                                 `vrfl_path.RF_STRB[0]
                                };
wire    [39:0]  vrfl_testout_1  = {1'b0,
                                 `vrfl_path.RF_AWC,
                                 `vrfl_path.RF_AWD,
                                ^`vrfl_path.RF_DWC,
                                ^`vrfl_path.RF_DWD,
                                 `vrfl_path.RF_CENWC_,
                                 `vrfl_path.RF_CENWD_,
                                 `vrfl_path.RF_CENSTWC_,
                                 `vrfl_path.RF_CENSTWD_,
                                 `vrfl_path.RF_CENSTWA_,
                                 `vrfl_path.RF_CENSTWB_,
                                 `vrfl_path.RF_CENRC_,
                                 `vrfl_path.RF_CENRD_,
                                  vrfl_addr_rcrd,
                                 `vrfl_path.ID_ADDR_SEL0,
                                 `vrfl_path.ID_ADDR_SEL1,
                                 `vrfl_path.RST_,
                                 `vrfl_path.RF_RCREDO_EN,
                                 `vrfl_path.RF_RDREDO_EN,
                                ^`vrfl_path.RF_QRC,
                                ^`vrfl_path.RF_QRD,
                                 `vrfl_path.RF_STRC[0],
                                 `vrfl_path.RF_STRD[0]
                                };
wire    [39:0]  vrfl_testout_2  = {1'b0,
                                 `vrfl_path.RF_AWA,
                                 `vrfl_path.RF_AWB,
                                ^`vrfl_path.RF_DWA,
                                ^`vrfl_path.RF_DWB,
                                 `vrfl_path.RF_CENWA_,
                                 `vrfl_path.RF_CENWB_,
                                 `vrfl_path.RF_CENSTWA_,
                                 `vrfl_path.RF_CENSTWB_,
                                 `vrfl_path.RF_CENSTWC_,
                                 `vrfl_path.RF_CENSTWD_,
                                 `vrfl_path.RF_CENCA_,
                                 `vrfl_path.RF_CENCB_,
                                 `vrfl_path.DISP_CA_S,
                                 `vrfl_path.DISP_CB_S,
                                 `vrfl_path.RD_ENCA,
                                 `vrfl_path.RD_ENCB,
                                  vrfl_addr_cacb,
                                 `vrfl_path.RST_,
                                 `vrfl_path.RF_CAREDO_EN,
                                 `vrfl_path.RF_CBREDO_EN,
                                 `vrfl_path.RF_STCA[0],
                                 `vrfl_path.RF_STCB[0]
                                };

/*
//old version
wire    [39:0]  jtlb_testout = {18'b0,
                                ^`jtlb_path.CORE_VPNIN,
                                 `jtlb_path.CORE_TLBRD,
                                 `jtlb_path.CORE_TLBWR,
                                 `jtlb_path.CORE_RW_INDEX,
                                ^`jtlb_path.CORE_WR_DATA,
                              	 `jtlb_path.CORE_TLBP,
                                 `jtlb_path.CORE_TLBID,
                                 `jtlb_path.CS,
                                 `jtlb_path.ENTRY_OUT[0],
                                ^`jtlb_path.ENTRY_DATA,
                                 `jtlb_path.SEL_ODD,
                                 `jtlb_path.CORE_HIT_INDEX,
                                 `jtlb_path.CORE_HIT,
                                 `jtlb_path.CORE_MULTIHIT
                               };

wire    [39:0]  dtlb_testout = {17'b0,
                                ^`dtlb_path.VPN_IN,
                                 `dtlb_path.COMP_EN,
                                 `dtlb_path.FLUSH_VEC,
                                ^`dtlb_path.UPDATE_DATA,
                                 `dtlb_path.UPDATE_EN,
                                 `dtlb_path.UPDATE_VEC,
                                ^`dtlb_path.CP0_ASID,
                                ^`dtlb_path.BYPASS_MODE_IN,
                                ^`dtlb_path.CONFIG_K0_IN,
                                 `dtlb_path.HIT_IN_DTLB,
                                 `dtlb_path.HIT_VEC,
                                ^`dtlb_path.ENTRY_PFN,
                                ^`dtlb_path.ENTRY_CACHE,
                                 `dtlb_path.ENTRY_DIRTY
                                };

wire    [39:0]  itlb_testout = {17'b0,
                                ^`itlb_path.VPN_IN,
                                 `itlb_path.COMP_EN,
                                 `itlb_path.FLUSH_VEC,
                                ^`itlb_path.UPDATE_DATA,
                                 `itlb_path.UPDATE_EN,
                                 `itlb_path.UPDATE_VEC,
                                ^`itlb_path.CP0_ASID,
                                ^`itlb_path.BYPASS_MODE_IN,
                                ^`itlb_path.CONFIG_K0_IN,
                                 `itlb_path.HIT_IN_DTLB,
                                 `itlb_path.HIT_VEC,
                                ^`itlb_path.ENTRY_PFN,
                                ^`itlb_path.ENTRY_CACHE,
                                 `itlb_path.ENTRY_DIRTY
                                };

wire    [39:0]  itag_testout = {3'b0,
                                ^`itag_path.ic_sram_tag_in_b,
                                 `itag_path.ic_sram_tag_addr_b,
                                ^`itag_path.ic_sram_lru_in_b,
                                 `itag_path.ic_sram_tag_1_cs_b_,
                                 `itag_path.ic_sram_tag_0_cs_b_,
                                 `itag_path.ic_sram_wr_b_,
                                 `itag_path.ic_sram_invld_b_,
                                 `itag_path.ic_sram_indexop_b_,
                                 `itag_path.ic_sram_way_sel_b,
                                ^`itag_path.ic_sram_paddr_b,
                                 `itag_path.ic_sram_way_option_b,
                                 `itag_path.ic_sram_way_addr_b,
                                ^`itag_path.ic_sram_1_tag3_out,
                                ^`itag_path.ic_sram_1_tag2_out,
                                ^`itag_path.ic_sram_1_tag1_out,
                                ^`itag_path.ic_sram_1_tag0_out,
                                ^`itag_path.ic_sram_0_tag3_out,
                                ^`itag_path.ic_sram_0_tag2_out,
                                ^`itag_path.ic_sram_0_tag1_out,
                                ^`itag_path.ic_sram_0_tag0_out,
                                 `itag_path.ic_sram_1_hit_vec,
                                 `itag_path.ic_sram_0_hit_vec,
                                ^`itag_path.ic_sram_1_lru_out,
                                ^`itag_path.ic_sram_0_lru_out
                                };

wire    [39:0]  dtag_testout = {3'b0,
                                ^`dtag_path.ram_tag_in,
                                 `dtag_path.ram_tag_a_in[6:0],
                                ^`dtag_path.ram_lru_in,
                                 `dtag_path.tag1_cs_,
                                 `dtag_path.tag0_cs_,
                                 `dtag_path.wr_,
                                 `dtag_path.dc_sram_invalid_,
                                 `dtag_path.dc_sram_indexop_,
                                 `dtag_path.way_sel,
                                ^`dtag_path.dc_tag_paddr,
                                 `dtag_path.dc_way_option,
                                 `dtag_path.ram_tag_a_in[7],
                                ^`dtag_path.ram_tag_1_3_out,
                                ^`dtag_path.ram_tag_1_2_out,
                                ^`dtag_path.ram_tag_1_1_out,
                                ^`dtag_path.ram_tag_1_0_out,
                                ^`dtag_path.ram_tag_0_3_out,
                                ^`dtag_path.ram_tag_0_2_out,
                                ^`dtag_path.ram_tag_0_1_out,
                                ^`dtag_path.ram_tag_0_0_out,
                                 `dtag_path.d_hit_1_3,
                                 `dtag_path.d_hit_1_2,
                                 `dtag_path.d_hit_1_1,
                                 `dtag_path.d_hit_1_0,
                                 `dtag_path.d_hit_0_3,
                                 `dtag_path.d_hit_0_2,
                                 `dtag_path.d_hit_0_1,
                                 `dtag_path.d_hit_0_0,
                                ^`dtag_path.ram_lru_1_out,
                                ^`dtag_path.ram_lru_0_out
                                };

wire    [39:0]  idata_testout = {18'b0,
                                 `idata_path.ram_data_1_1_cen_b_,
                                 `idata_path.ram_data_1_0_cen_b_,
                                 `idata_path.ram_data_0_1_cen_b_,
                                 `idata_path.ram_data_0_0_cen_b_,
                                 `idata_path.ram_data_wen_b_,
                                 `idata_path.ram_data_addr_b,
                                ^`idata_path.ram_data_1_1_in,
                                ^`idata_path.ram_data_1_0_in,
                                ^`idata_path.ram_data_0_1_in,
                                ^`idata_path.ram_data_0_0_in,
                                ^`idata_path.ram_data_1_1_out,
                                ^`idata_path.ram_data_1_0_out,
                                ^`idata_path.ram_data_0_1_out,
                                ^`idata_path.ram_data_0_0_out
                                };

wire    [39:0]  ddata_testout = {18'b0,
                                 `ddata_path.data_0_0_cs_,
                                 `ddata_path.data_0_1_cs_,
                                 `ddata_path.data_1_0_cs_,
                                 `ddata_path.data_1_1_cs_,
                                 `ddata_path.wen_,
                                 `ddata_path.ram_data_a_in,
                                ^`ddata_path.ram_data_1_1_in,
                                ^`ddata_path.ram_data_1_0_in,
                                ^`ddata_path.ram_data_0_1_in,
                                ^`ddata_path.ram_data_0_0_in,
                                ^`ddata_path.ram_data_1_1_out,
                                ^`ddata_path.ram_data_1_0_out,
                                ^`ddata_path.ram_data_0_1_out,
                                ^`ddata_path.ram_data_0_0_out
                                };

wire    [9:0]   rf_addr_rarb  = `rf_path.ID_ADDR_SEL0 ? `rf_path.ID_RSRTRD_REG0[14:5] :
                                `rf_path.ID_ADDR_SEL1 ? `rf_path.ID_RSRTRD_REG1[14:5] :
                                                        `rf_path.ID_RSRTRD_IN0[14:5];
wire    [9:0]   rf_addr_rcrd  = `rf_path.ID_ADDR_SEL0 ? `rf_path.ID_RSRTRD_REG1[14:5] :
                                `rf_path.ID_ADDR_SEL1 ? `rf_path.ID_RSRTRD_IN0[14:5]  :
                                                        `rf_path.ID_RSRTRD_IN1[14:5];
wire    [9:0]   rf_addr_cacb  = `rf_path.ID_ADDR_SEL0 ? {`rf_path.ID_RSRTRD_REG0[4:0],`rf_path.ID_RSRTRD_REG1[4:0]} :
                                `rf_path.ID_ADDR_SEL1 ? {`rf_path.ID_RSRTRD_REG1[4:0],`rf_path.ID_RSRTRD_IN0[4:0]}  :
                                                        {`rf_path.ID_RSRTRD_IN0[4:0], `rf_path.ID_RSRTRD_IN1[4:0]};

wire    [39:0]  rf_testout_0  = {1'b0,
                                 `rf_path.RF_AWA,
                                 `rf_path.RF_AWB,
                                ^`rf_path.RF_DWA,
                                ^`rf_path.RF_DWB,
                                 `rf_path.RF_CENWA_,
                                 `rf_path.RF_CENWB_,
                                 `rf_path.RF_CENSTWA_,
                                 `rf_path.RF_CENSTWB_,
                                 `rf_path.RF_CENSTWC_,
                                 `rf_path.RF_CENSTWD_,
                                 `rf_path.RF_CENRA_,
                                 `rf_path.RF_CENRB_,
                                  rf_addr_rarb,
                                 `rf_path.ID_ADDR_SEL0,
                                 `rf_path.ID_ADDR_SEL1,
                                 `rf_path.RST_,
                                 `rf_path.RF_RAREDO_EN,
                                 `rf_path.RF_RBREDO_EN,
                                ^`rf_path.RF_QRA,
                                ^`rf_path.RF_QRB,
                                 `rf_path.RF_STRA[0],
                                 `rf_path.RF_STRB[0]
                                };
wire    [39:0]  rf_testout_1  = {1'b0,
                                 `rf_path.RF_AWC,
                                 `rf_path.RF_AWD,
                                ^`rf_path.RF_DWC,
                                ^`rf_path.RF_DWD,
                                 `rf_path.RF_CENWC_,
                                 `rf_path.RF_CENWD_,
                                 `rf_path.RF_CENSTWC_,
                                 `rf_path.RF_CENSTWD_,
                                 `rf_path.RF_CENSTWA_,
                                 `rf_path.RF_CENSTWB_,
                                 `rf_path.RF_CENRC_,
                                 `rf_path.RF_CENRD_,
                                  rf_addr_rcrd,
                                 `rf_path.ID_ADDR_SEL0,
                                 `rf_path.ID_ADDR_SEL1,
                                 `rf_path.RST_,
                                 `rf_path.RF_RCREDO_EN,
                                 `rf_path.RF_RDREDO_EN,
                                ^`rf_path.RF_QRC,
                                ^`rf_path.RF_QRD,
                                 `rf_path.RF_STRC[0],
                                 `rf_path.RF_STRD[0]
                                };
wire    [39:0]  rf_testout_2  = {1'b0,
                                 `rf_path.RF_AWA,
                                 `rf_path.RF_AWB,
                                ^`rf_path.RF_DWA,
                                ^`rf_path.RF_DWB,
                                 `rf_path.RF_CENWA_,
                                 `rf_path.RF_CENWB_,
                                 `rf_path.RF_CENSTWA_,
                                 `rf_path.RF_CENSTWB_,
                                 `rf_path.RF_CENSTWC_,
                                 `rf_path.RF_CENSTWD_,
                                 `rf_path.RF_CENCA_,
                                 `rf_path.RF_CENCB_,
                                 `rf_path.DISP_CA_S,
                                 `rf_path.DISP_CB_S,
                                 `rf_path.RD_ENCA,
                                 `rf_path.RD_ENCB,
                                  rf_addr_cacb,
                                 `rf_path.RST_,
                                 `rf_path.RF_CAREDO_EN,
                                 `rf_path.RF_CBREDO_EN,
                                 `rf_path.RF_STCA[0],
                                 `rf_path.RF_STCB[0]
                                };

wire    [9:0]   vrfh_addr_rarb  = `vrfh_path.ID_ADDR_SEL0 ? `vrfh_path.ID_RSRTRD_REG0[14:5] :
                                  `vrfh_path.ID_ADDR_SEL1 ? `vrfh_path.ID_RSRTRD_REG1[14:5] :
                                                            `vrfh_path.ID_RSRTRD_IN0[14:5];
wire    [9:0]   vrfh_addr_rcrd  = `vrfh_path.ID_ADDR_SEL0 ? `vrfh_path.ID_RSRTRD_REG1[14:5] :
                                  `vrfh_path.ID_ADDR_SEL1 ? `vrfh_path.ID_RSRTRD_IN0[14:5]  :
                                                            `vrfh_path.ID_RSRTRD_IN1[14:5];
wire    [9:0]   vrfh_addr_cacb  = `vrfh_path.ID_ADDR_SEL0 ? {`vrfh_path.ID_RSRTRD_REG0[4:0],`vrfh_path.ID_RSRTRD_REG1[4:0]} :
                                  `vrfh_path.ID_ADDR_SEL1 ? {`vrfh_path.ID_RSRTRD_REG1[4:0],`vrfh_path.ID_RSRTRD_IN0[4:0]}  :
                                                            {`vrfh_path.ID_RSRTRD_IN0[4:0], `vrfh_path.ID_RSRTRD_IN1[4:0]};

wire    [39:0]  vrfh_testout_0  = {1'b0,
                                 `vrfh_path.RF_AWA,
                                 `vrfh_path.RF_AWB,
                                ^`vrfh_path.RF_DWA,
                                ^`vrfh_path.RF_DWB,
                                 `vrfh_path.RF_CENWA_,
                                 `vrfh_path.RF_CENWB_,
                                 `vrfh_path.RF_CENSTWA_,
                                 `vrfh_path.RF_CENSTWB_,
                                 `vrfh_path.RF_CENSTWC_,
                                 `vrfh_path.RF_CENSTWD_,
                                 `vrfh_path.RF_CENRA_,
                                 `vrfh_path.RF_CENRB_,
                                  vrfh_addr_rarb,
                                 `vrfh_path.ID_ADDR_SEL0,
                                 `vrfh_path.ID_ADDR_SEL1,
                                 `vrfh_path.RST_,
                                 `vrfh_path.RF_RAREDO_EN,
                                 `vrfh_path.RF_RBREDO_EN,
                                ^`vrfh_path.RF_QRA,
                                ^`vrfh_path.RF_QRB,
                                 `vrfh_path.RF_STRA[0],
                                 `vrfh_path.RF_STRB[0]
                                };
wire    [39:0]  vrfh_testout_1  = {1'b0,
                                 `vrfh_path.RF_AWC,
                                 `vrfh_path.RF_AWD,
                                ^`vrfh_path.RF_DWC,
                                ^`vrfh_path.RF_DWD,
                                 `vrfh_path.RF_CENWC_,
                                 `vrfh_path.RF_CENWD_,
                                 `vrfh_path.RF_CENSTWC_,
                                 `vrfh_path.RF_CENSTWD_,
                                 `vrfh_path.RF_CENSTWA_,
                                 `vrfh_path.RF_CENSTWB_,
                                 `vrfh_path.RF_CENRC_,
                                 `vrfh_path.RF_CENRD_,
                                  vrfh_addr_rcrd,
                                 `vrfh_path.ID_ADDR_SEL0,
                                 `vrfh_path.ID_ADDR_SEL1,
                                 `vrfh_path.RST_,
                                 `vrfh_path.RF_RCREDO_EN,
                                 `vrfh_path.RF_RDREDO_EN,
                                ^`vrfh_path.RF_QRC,
                                ^`vrfh_path.RF_QRD,
                                 `vrfh_path.RF_STRC[0],
                                 `vrfh_path.RF_STRD[0]
                                };
wire    [39:0]  vrfh_testout_2  = {1'b0,
                                 `vrfh_path.RF_AWA,
                                 `vrfh_path.RF_AWB,
                                ^`vrfh_path.RF_DWA,
                                ^`vrfh_path.RF_DWB,
                                 `vrfh_path.RF_CENWA_,
                                 `vrfh_path.RF_CENWB_,
                                 `vrfh_path.RF_CENSTWA_,
                                 `vrfh_path.RF_CENSTWB_,
                                 `vrfh_path.RF_CENSTWC_,
                                 `vrfh_path.RF_CENSTWD_,
                                 `vrfh_path.RF_CENCA_,
                                 `vrfh_path.RF_CENCB_,
                                 `vrfh_path.DISP_CA_S,
                                 `vrfh_path.DISP_CB_S,
                                 `vrfh_path.RD_ENCA,
                                 `vrfh_path.RD_ENCB,
                                  vrfh_addr_cacb,
                                 `vrfh_path.RST_,
                                 `vrfh_path.RF_CAREDO_EN,
                                 `vrfh_path.RF_CBREDO_EN,
                                 `vrfh_path.RF_STCA[0],
                                 `vrfh_path.RF_STCB[0]
                                };

wire    [9:0]   vrfl_addr_rarb  = `vrfl_path.ID_ADDR_SEL0 ? `vrfl_path.ID_RSRTRD_REG0[14:5] :
                                  `vrfl_path.ID_ADDR_SEL1 ? `vrfl_path.ID_RSRTRD_REG1[14:5] :
                                                            `vrfl_path.ID_RSRTRD_IN0[14:5];
wire    [9:0]   vrfl_addr_rcrd  = `vrfl_path.ID_ADDR_SEL0 ? `vrfl_path.ID_RSRTRD_REG1[14:5] :
                                  `vrfl_path.ID_ADDR_SEL1 ? `vrfl_path.ID_RSRTRD_IN0[14:5]  :
                                                            `vrfl_path.ID_RSRTRD_IN1[14:5];
wire    [9:0]   vrfl_addr_cacb  = `vrfl_path.ID_ADDR_SEL0 ? {`vrfl_path.ID_RSRTRD_REG0[4:0],`vrfl_path.ID_RSRTRD_REG1[4:0]} :
                                  `vrfl_path.ID_ADDR_SEL1 ? {`vrfl_path.ID_RSRTRD_REG1[4:0],`vrfl_path.ID_RSRTRD_IN0[4:0]}  :
                                                            {`vrfl_path.ID_RSRTRD_IN0[4:0], `vrfl_path.ID_RSRTRD_IN1[4:0]};

wire    [39:0]  vrfl_testout_0  = {1'b0,
                                 `vrfl_path.RF_AWA,
                                 `vrfl_path.RF_AWB,
                                ^`vrfl_path.RF_DWA,
                                ^`vrfl_path.RF_DWB,
                                 `vrfl_path.RF_CENWA_,
                                 `vrfl_path.RF_CENWB_,
                                 `vrfl_path.RF_CENSTWA_,
                                 `vrfl_path.RF_CENSTWB_,
                                 `vrfl_path.RF_CENSTWC_,
                                 `vrfl_path.RF_CENSTWD_,
                                 `vrfl_path.RF_CENRA_,
                                 `vrfl_path.RF_CENRB_,
                                  vrfl_addr_rarb,
                                 `vrfl_path.ID_ADDR_SEL0,
                                 `vrfl_path.ID_ADDR_SEL1,
                                 `vrfl_path.RST_,
                                 `vrfl_path.RF_RAREDO_EN,
                                 `vrfl_path.RF_RBREDO_EN,
                                ^`vrfl_path.RF_QRA,
                                ^`vrfl_path.RF_QRB,
                                 `vrfl_path.RF_STRA[0],
                                 `vrfl_path.RF_STRB[0]
                                };
wire    [39:0]  vrfl_testout_1  = {1'b0,
                                 `vrfl_path.RF_AWC,
                                 `vrfl_path.RF_AWD,
                                ^`vrfl_path.RF_DWC,
                                ^`vrfl_path.RF_DWD,
                                 `vrfl_path.RF_CENWC_,
                                 `vrfl_path.RF_CENWD_,
                                 `vrfl_path.RF_CENSTWC_,
                                 `vrfl_path.RF_CENSTWD_,
                                 `vrfl_path.RF_CENSTWA_,
                                 `vrfl_path.RF_CENSTWB_,
                                 `vrfl_path.RF_CENRC_,
                                 `vrfl_path.RF_CENRD_,
                                  vrfl_addr_rcrd,
                                 `vrfl_path.ID_ADDR_SEL0,
                                 `vrfl_path.ID_ADDR_SEL1,
                                 `vrfl_path.RST_,
                                 `vrfl_path.RF_RCREDO_EN,
                                 `vrfl_path.RF_RDREDO_EN,
                                ^`vrfl_path.RF_QRC,
                                ^`vrfl_path.RF_QRD,
                                 `vrfl_path.RF_STRC[0],
                                 `vrfl_path.RF_STRD[0]
                                };
wire    [39:0]  vrfl_testout_2  = {1'b0,
                                 `vrfl_path.RF_AWA,
                                 `vrfl_path.RF_AWB,
                                ^`vrfl_path.RF_DWA,
                                ^`vrfl_path.RF_DWB,
                                 `vrfl_path.RF_CENWA_,
                                 `vrfl_path.RF_CENWB_,
                                 `vrfl_path.RF_CENSTWA_,
                                 `vrfl_path.RF_CENSTWB_,
                                 `vrfl_path.RF_CENSTWC_,
                                 `vrfl_path.RF_CENSTWD_,
                                 `vrfl_path.RF_CENCA_,
                                 `vrfl_path.RF_CENCB_,
                                 `vrfl_path.DISP_CA_S,
                                 `vrfl_path.DISP_CB_S,
                                 `vrfl_path.RD_ENCA,
                                 `vrfl_path.RD_ENCB,
                                  vrfl_addr_cacb,
                                 `vrfl_path.RST_,
                                 `vrfl_path.RF_CAREDO_EN,
                                 `vrfl_path.RF_CBREDO_EN,
                                 `vrfl_path.RF_STCA[0],
                                 `vrfl_path.RF_STCB[0]
                                };
*/
//old version end

reg     [39:0]   cpu_testout_comp1;
always@ (posedge clk)
    cpu_testout_comp1  <= #1
                {40{cpu_testin[3:0] == 4'b0000}} & jtlb_testout |
                {40{cpu_testin[3:0] == 4'b0001}} & dtlb_testout |
                {40{cpu_testin[3:0] == 4'b0010}} & itlb_testout |
                {40{cpu_testin[3:0] == 4'b0011}} & itag_testout |
                {40{cpu_testin[3:0] == 4'b0100}} & dtag_testout |
                {40{cpu_testin[3:0] == 4'b0101}} & idata_testout |
                {40{cpu_testin[3:0] == 4'b0110}} & ddata_testout |
                {40{cpu_testin[3:0] == 4'b0111}} & rf_testout_0 |
                {40{cpu_testin[3:0] == 4'b1000}} & rf_testout_1 |
                {40{cpu_testin[3:0] == 4'b1001}} & rf_testout_2 |
                {40{cpu_testin[3:0] == 4'b1010}} & vrfh_testout_0 |
                {40{cpu_testin[3:0] == 4'b1011}} & vrfh_testout_1 |
                {40{cpu_testin[3:0] == 4'b1100}} & vrfh_testout_2 |
                {40{cpu_testin[3:0] == 4'b1101}} & vrfl_testout_0 |
                {40{cpu_testin[3:0] == 4'b1110}} & vrfl_testout_1 |
                {40{cpu_testin[3:0] == 4'b1111}} & vrfl_testout_2;

//wire    [39:0]  #PCLK_PERIOD    cpu_testout_comp = cpu_testout_comp1;
reg		[39:0]	cpu_testout_comp;
always	@(posedge clk or negedge rst_)
	if (!rst_)
		cpu_testout_comp	<= #udly 0;
	else
		cpu_testout_comp 	<= #udly cpu_testout_comp1;

//wire    #(PCLK_PERIOD*2)  cpu_trig_signal_dly = i_cpu_trig_signal;
reg		cpu_trig_signal_dly1;
reg		cpu_trig_signal_dly2;
always	@(posedge clk or negedge rst_)
	if (!rst_)	begin
		cpu_trig_signal_dly1	<= #udly 0;
		cpu_trig_signal_dly2	<= #udly 0;
	end	else 	begin
		cpu_trig_signal_dly1	<= #udly i_cpu_trig_signal;
		cpu_trig_signal_dly2	<= #udly cpu_trig_signal_dly1;
	end

wire    mismatch = cpu_testout_comp !== i_cpu_testout;
always	@(posedge clk)
    if  (cpu_trig_signal_dly2 & rst_) begin
        if  (mismatch) begin
            $display ("Error_T2_rsim:Trig Sad!!!");
            case (cpu_testin[3:0])
            4'b0000: $display ("cpu_testout mismatch with jtlb block!!!\n");
            4'b0001: $display ("cpu_testout mismatch with dtlb block!!!\n");
            4'b0010: $display ("cpu_testout mismatch with itlb block!!!\n");
            4'b0011: $display ("cpu_testout mismatch with itag block!!!\n");
            4'b0100: $display ("cpu_testout mismatch with dtag block!!!\n");
            4'b0101: $display ("cpu_testout mismatch with idata block!!!\n");
            4'b0110: $display ("cpu_testout mismatch with ddata block!!!\n");
            4'b0111: $display ("cpu_testout mismatch with rf_0 block!!!\n");
            4'b1000: $display ("cpu_testout mismatch with rf_1 block!!!\n");
            4'b1001: $display ("cpu_testout mismatch with rf_2 block!!!\n");
            4'b1010: $display ("cpu_testout mismatch with vrfh_0 block!!!\n");
            4'b1011: $display ("cpu_testout mismatch with vrfh_1 block!!!\n");
            4'b1100: $display ("cpu_testout mismatch with vrfh_2 block!!!\n");
            4'b1101: $display ("cpu_testout mismatch with vrfl_0 block!!!\n");
            4'b1110: $display ("cpu_testout mismatch with vrfl_1 block!!!\n");
            4'b1111: $display ("cpu_testout mismatch with vrfl_2 block!!!\n");
            endcase
        end //else
        	//$display ("Trig Happy!!!");
   end

//make the output signals is tri-state
reg		oe;
initial oe = 0;
always @(posedge clk or negedge rst_)
	if (!rst_)
		oe <= #udly 0;
	else
		oe <= #udly 1;

wire	[4:0]	o_cpu_testin	= oe ? cpu_testin	    : 5'hz;
wire	[9:0]	o_cpu_trigcond	= oe ? cpu_trigcond	: 10'hz;
wire			o_cpu_trig_en	= oe ? cpu_trig_en	: 1'hz;

/* By now the trigcond will always zero
   Following is the cpu_testin's value and its test target
   cpu_testin  = 5'b00000; //TRIG_JTLB
   cpu_testin  = 5'b00001; //TRIG_DTLB
   cpu_testin  = 5'b00010; //TRIG_ITLB
   cpu_testin  = 5'b00011; //TRIG_ITAG
   cpu_testin  = 5'b00100; //TRIG_DTAG
   cpu_testin  = 5'b00101; //TRIG_IDATA
   cpu_testin  = 5'b00110; //TRIG_DDATA
   cpu_testin  = 5'b00111; //TRIG_RF_0
   cpu_testin  = 5'b01000; //TRIG_RF_1
   cpu_testin  = 5'b01001; //TRIG_RF_2
   cpu_testin  = 5'b01010; //TRIG_VRFH_0
   cpu_testin  = 5'b01011; //TRIG_VRFH_1
   cpu_testin  = 5'b01100; //TRIG_VRFH_2
   cpu_testin  = 5'b01101; //TRIG_VRFL_0
   cpu_testin  = 5'b01110; //TRIG_VRFL_1
   cpu_testin  = 5'b01111; //TRIG_VRFL_2
*/
task	cpu_trigger_test;
input	[9:0]	tricond;
input	[4:0]	testin;
begin
	cpu_trig_en		= 1'b1;
	cpu_trigcond 	= tricond;
	cpu_testin		= testin;
	wait (cpu_trig_signal_dly2);
	@(negedge clk);
	wait (~cpu_trig_signal_dly2);	//wait test end
	@(negedge clk);
	cpu_trig_en		= 1'b0;
end
endtask

task	cpu_trigger_test_onetime;
input	[9:0]	tricond;
input	[4:0]	testin;
begin
	cpu_trig_en		= 1'b1;
	cpu_trigcond 	= tricond;
	cpu_testin		= testin;
	wait (cpu_trig_signal_dly2);
	@(negedge clk);
	cpu_trig_en		= 1'b0;
end
endtask

//Check the share pins and PAD connection whether correct
`ifdef	CPU_SHAREPIN_CHECK

	`define	t2cpu_path top.chip.core.T2_CPU
	wire [39:0]	CPU_TESTOUT			= `t2cpu_path.CPU_TESTOUT;
	wire		CPU_TRIG_SIGNAL		= `t2cpu_path.CPU_TRIG_SIGNAL;


	wire	pins1_mismatch	=	CPU_TESTOUT !== i_cpu_testout;
	wire	pins2_mismatch	=	CPU_TRIG_SIGNAL !== i_cpu_trig_signal;
	always @(posedge clk)
		if (rst_) begin
			if (pins1_mismatch) begin
				$display("CPU_TESTOUT pin connection error at time %0t", $realtime);
			end
			if (pins2_mismatch) begin
				$display("CPU_TRIG_SIGNAL pin connection error at time %0t", $realtime);
			end
		end

`endif

endmodule	//cpu_trigger_ext_agent
