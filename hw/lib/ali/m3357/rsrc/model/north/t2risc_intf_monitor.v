/******************************************************************************
          (c) copyrights 1997-2000. All rights reserved

           T-Square Design, Inc.

          Company confidential and Properietary information.
          This information may not be disclosed to unauthorized
          individual.
******************************************************************************/
/*********************************************************************
//File 	: 	t2risc_intf_monitor.v -- used the interface protocol
//Author: 	Yuky
//Date	: 	2002-05-28	Initial version
//			2002-07-05  x_ready is allowed to insert wait state.
//			2002-09-23	u_request is changed to detect with sync method
//						fix the bug of check the burst length
//						fix the bug of test bytemask during burst operation
//Macro	:	CPU_MONITOR 	if define, will write the error or warning
//							message to the file of "cpu_monitor.rpt"
//			CLOSE_CPU_ERROR if define, will close all error protocol
//							report
//			CLOSE_CPU_WARNING if define, will close all Warning protocol
//							report
*********************************************************************/

`timescale 1ns/100ps
module t2risc_intf_monitor(
//input from t2chipset signals
	x_ack,			// last data tranfer
	x_ready,		// In read, means data is on the bus x_data_in
					// In write, means data on u_data_to_send be accepted
					// This signal should assert one sclk for every word
	x_bus_error,	// Asserted one clock when bus data error
	x_data_in,		// The recieved data
	x_err_int,		// The cpu bus error interrupt
	x_ext_int,		// The external interrupt

//input from t2risc signals
	u_request,		// Request reading or writing,asserted until the x_ack
					// for last word being accepted
	u_write,		// 1: write, 0: read
	u_wordsize,		// 0 -> 1words, 1 -> 2words,  2 -> 4words,  3 -> 8words
	u_startpaddr,	// Request address
	u_data_to_send,	// Write data
	u_bytemask,		// four bits bytemask signals for u_data_to_send,
					// 1 means valid, 0 means invalid

//global signals
	g_sclk,	        // memory interface clock, it is strickly half of
					// pipeline clk
	g_reset_		// global reset
	);
parameter udly = 1;
parameter filter_dly = 0.1;
input			u_request;
input			u_write;
input	[1:0]	u_wordsize;
input	[31:0]	u_startpaddr;
input	[31:0]	u_data_to_send;
input	[3:0]	u_bytemask;

input			x_ack;
input			x_ready;
input			x_bus_error;
input	[31:0]	x_data_in;
input			x_err_int,
				x_ext_int;

input			g_sclk;
input			g_reset_;

`ifdef CPU_MONITOR
integer	c_moni_msg;
initial c_moni_msg = $fopen("cpu_monitor.rpt");
`endif

//========== Error message ==============
`ifdef CLOSE_CPU_ERROR
`else
//"x_ack" must be active for only one clock
reg	x_ack_dly_1;
always @(posedge g_sclk or negedge g_reset_)//delay x_ack for one clock
	if (!g_reset_)
		x_ack_dly_1 <= #udly 1'b0;
	else
		x_ack_dly_1 <= #udly x_ack;

wire x_ack_error_1 = (x_ack === 1'b1) && (x_ack_dly_1 === 1'b1);

always @(posedge g_sclk)
	if (x_ack_error_1) begin
		$display("Error_T2_rsim --> T2RISC interface protocol error:");
		$display("x_ack active for more than one clock at %0t",$realtime);
		`ifdef CPU_MONITOR
		$fdisplay(c_moni_msg,"Error_T2_rsim --> T2RISC interface protocol error:");
		$fdisplay(c_moni_msg,"x_ack active for more than one clock at %0t",$realtime);
		`endif
	end

//"x_ack" only can be active during u_request and x_ready active
wire x_ack_error_2 = (x_ack === 1'b1) && ((u_request === 1'b0) | (x_ready === 1'b0));
always @(posedge g_sclk)
	if (g_reset_ & x_ack_error_2) begin
		$display("Error_T2_rsim --> T2RISC interface protocol error:");
		$display("x_ack active during u_request or x_ready unactive at %0t",$realtime);
		`ifdef CPU_MONITOR
		$fdisplay(c_moni_msg,"Error_T2_rsim --> T2RISC interface protocol error:");
		$fdisplay(c_moni_msg,"x_ack active during u_request or x_ready unactive at %0t",$realtime);
		`endif
	end

/*
//u_request, after x_ack active, it must be deasserted
wire u_req_error_1 = (x_ack_dly_1 === 1'b1) && (u_request === 1'b1);
always @(posedge g_sclk)
	if (u_req_error_1) begin
		$display("Error_T2_rsim --> T2RISC interface protocol error:");
		$display("u_request active immediately after x_ack active at %0t",$realtime);
		`ifdef CPU_MONITOR
		$fdisplay(c_moni_msg,"Error_T2_rsim --> T2RISC interface protocol error:");
		$fdisplay(c_moni_msg,"u_request active immediately after x_ack active at %0t",$realtime);
		`endif
	end
*/

//u_request, after it active, it can't change it state until x_ack active
reg	u_req_dly_1;
always @(posedge g_sclk or negedge g_reset_)
	if (!g_reset_)
		u_req_dly_1 <= #udly 1'b0;
	else
		u_req_dly_1 <= #udly u_request;

wire req_1change0 = (u_request == 0 && u_req_dly_1 == 1);
//always @(negedge u_request_dly)
//	if (g_reset_ && ((x_ack | x_ack_dly_1) === 1'b0)) begin
always @(posedge g_sclk or negedge g_reset_)
	if (g_reset_ && (x_ack_dly_1 === 1'b0) && req_1change0) begin
		$display("Error_T2_rsim --> T2RISC interface protocol error:");
		$display("u_request become unactive before x_ack active at %0t",$realtime);
		`ifdef CPU_MONITOR
		$fdisplay(c_moni_msg,"Error_T2_rsim --> T2RISC interface protocol error:");
		$fdisplay(c_moni_msg,"u_request become unactive before x_ack active at %0t",$realtime);
		`endif
	end

//u_write, (1-write 0-read), can't change its value during u_request active
reg u_req_check_period;
always @(posedge g_sclk or negedge g_reset_)
	if (!g_reset_)
		u_req_check_period <= #udly 1'b0;
	else if (x_ack)
		u_req_check_period <= #udly 1'b0;
	else if (u_request)
		u_req_check_period <= #udly 1'b1;


reg	u_write_dly_1;
always @(posedge g_sclk or negedge g_reset_)
	if (!g_reset_)
		u_write_dly_1 <= #udly 1'b0;
	else
		u_write_dly_1 <= #udly u_write;

wire u_write_change = (u_write_dly_1 !== u_write);
always @(posedge g_sclk)
	if (u_req_check_period & u_write_change) begin
		$display("Error_T2_rsim --> T2RISC interface protocol error:");
		$display("u_write change value during u_request active at %0t",$realtime);
		`ifdef CPU_MONITOR
		$fdisplay(c_moni_msg,"Error_T2_rsim --> T2RISC interface protocol error:");
		$fdisplay(c_moni_msg,"u_write change value during u_request active at %0t",$realtime);
		`endif
	end

//u_wordsize, can't change its value during u_request active
reg	[1:0] u_wordsize_dly_1;
always @(posedge g_sclk or negedge g_reset_)
	if (!g_reset_)
		u_wordsize_dly_1 <= #udly 2'b00;
	else
		u_wordsize_dly_1 <= #udly u_wordsize;

wire u_wordsize_change = (u_wordsize_dly_1 !== u_wordsize);
always @(posedge g_sclk)
	if (u_req_check_period & u_wordsize_change) begin
		$display("Error_T2_rsim --> T2RISC interface protocol error:");
		$display("u_wordsize change value during u_request active at %0t",$realtime);
		`ifdef CPU_MONITOR
		$fdisplay(c_moni_msg,"Error_T2_rsim --> T2RISC interface protocol error:");
		$fdisplay(c_moni_msg,"u_wordsize change value during u_request active at %0t",$realtime);
		`endif
	end

//u_startpaddr, can't change its value during u_request active
reg	[31:0] u_startpaddr_dly_1;
always @(posedge g_sclk or negedge g_reset_)
	if (!g_reset_)
		u_startpaddr_dly_1 <= #udly 32'h0;
	else
		u_startpaddr_dly_1 <= #udly u_startpaddr;

wire u_startpaddr_change = (u_startpaddr_dly_1 !== u_startpaddr);
always @(posedge g_sclk)
	if (u_req_check_period & u_startpaddr_change) begin
		$display("Error_T2_rsim --> T2RISC interface protocol error:");
		$display("u_startpaddr change value during u_request active at %0t",$realtime);
		`ifdef CPU_MONITOR
		$fdisplay(c_moni_msg,"Error_T2_rsim --> T2RISC interface protocol error:");
		$fdisplay(c_moni_msg,"u_startpaddr change value during u_request active at %0t",$realtime);
		`endif
	end

//u_data_to_send, can't change its value until the x_ready active
reg	x_ready_dly_1;
always @(posedge g_sclk or negedge g_reset_)
	if (!g_reset_)
		x_ready_dly_1 <= #udly 1'b0;
	else
		x_ready_dly_1 <= #udly x_ready;

wire first_data_pulse = (u_req_check_period & ~x_ready_dly_1);

reg	[31:0] u_data_to_send_dly_1;
always @(posedge g_sclk or negedge g_reset_)
	if (!g_reset_)
		u_data_to_send_dly_1 <= #udly 32'h0;
	else
		u_data_to_send_dly_1 <= #udly u_data_to_send;

wire u_data_to_send_change = (u_data_to_send !== u_data_to_send_dly_1);
always @(posedge g_sclk)
	if (first_data_pulse & u_data_to_send_change & u_write) begin
		$display("Error_T2_rsim --> T2RISC interface protocol error:");
		$display("u_data_to_send change value before x_ready active at %0t",$realtime);
		`ifdef CPU_MONITOR
		$fdisplay(c_moni_msg,"Error_T2_rsim --> T2RISC interface protocol error:");
		$fdisplay(c_moni_msg,"u_data_to_send change value before x_ready active at %0t",$realtime);
		`endif
	end

//u_bytemask, can't change its value until the x_ready active
reg	[3:0] u_bytemask_dly_1;
always @(posedge g_sclk or negedge g_reset_)
	if (!g_reset_)
		u_bytemask_dly_1 <= #udly 4'h0;
	else
		u_bytemask_dly_1 <= #udly u_bytemask;

wire u_bytemask_change = (u_bytemask !== u_bytemask_dly_1);
always @(posedge g_sclk)
	if (first_data_pulse & u_bytemask_change & u_write) begin
		$display("Error_T2_rsim --> T2RISC interface protocol error:");
		$display("u_bytemask change value before x_ready active at %0t",$realtime);
		`ifdef CPU_MONITOR
		$fdisplay(c_moni_msg,"Error_T2_rsim --> T2RISC interface protocol error:");
		$fdisplay(c_moni_msg,"u_bytemask change value before x_ready active at %0t",$realtime);
		`endif
	end

//x_ready, can't active during u_request untive
wire x_ready_error_1 = ((x_ready === 1'b1) && (u_request === 1'b0));
always @(posedge g_sclk)
	if (g_reset_ & x_ready_error_1) begin
		$display("Error_T2_rsim --> T2RISC interface protocol error:");
		$display("x_ready active during u_request unactive at %0t",$realtime);
		`ifdef CPU_MONITOR
		$fdisplay(c_moni_msg,"Error_T2_rsim --> T2RISC interface protocol error:");
		$fdisplay(c_moni_msg,"x_ready active during u_request unactive at %0t",$realtime);
		`endif
	end

//x_ready, can't be active at the first clock of u_request active
wire u_req_first_pulse = (u_request && ~u_req_dly_1);
wire x_ready_error_2 = ((x_ready === 1'b1) && u_req_first_pulse);
always @(posedge g_sclk)
	if (g_reset_ & x_ready_error_2) begin
		$display("Error_T2_rsim --> T2RISC interface protocol error:");
		$display("x_ready active at the first clock of u_request active at %0t",$realtime);
		`ifdef CPU_MONITOR
		$fdisplay(c_moni_msg,"Error_T2_rsim --> T2RISC interface protocol error:");
		$fdisplay(c_moni_msg,"x_ready active at the first clock of u_request active at %0t",$realtime);
		`endif
	end

//x_ready, its valid time doesn't match u_wordsize
wire[2:0]u_len	= 	(u_wordsize == 0) ? 3'b000	:
					(u_wordsize == 1) ? 3'b001	:
					(u_wordsize == 2) ? 3'b011	:
										3'b111	;

reg	[3:0] len_cnt;
wire	overflow_error	=	len_cnt[3];
always @(posedge g_sclk or negedge g_reset_)
	if (!g_reset_)
		len_cnt <= #udly 4'h0;
	else if (x_ack) begin
		len_cnt <= #udly 4'h0;
		if (len_cnt !== u_len) begin
		$display("Error_T2_rsim --> T2RISC interface protocol error:");
		$display("x_ready valid time doesn't match u_wordsize at %0t",$realtime);
		`ifdef CPU_MONITOR
		$fdisplay(c_moni_msg,"Error_T2_rsim --> T2RISC interface protocol error:");
		$fdisplay(c_moni_msg,"x_ready valid time doesn't match u_wordsize at %0t",$realtime);
		`endif
		end
	end else if (x_ready && !overflow_error)
		len_cnt <= #udly len_cnt + 1;
/*
//x_ready, it does not support inserting the wait state
wire x_ready_change = (x_ready !== x_ready_dly_1);
reg	x_ready_valid_flag;
always @(posedge g_sclk or negedge g_reset_)
	if (!g_reset_)
		x_ready_valid_flag <= #udly 1'b0;
	else if (x_ack)
		x_ready_valid_flag <= #udly 1'b0;
	else if (x_ready)
		x_ready_valid_flag <= #udly 1'b1;
	else if (x_ready_valid_flag & x_ready_change) begin
		$display("Error_T2_rsim --> T2RISC interface protocol error:");
		$display("x_ready inserts wait state during data transaction at %0t",$realtime);
		`ifdef CPU_MONITOR
		$fdisplay(c_moni_msg,"Error_T2_rsim --> T2RISC interface protocol error:");
		$fdisplay(c_moni_msg,"x_ready inserts wait state during data transaction at %0t",$realtime);
		`endif
	end
*/
//"x_bus_error" must be active for only one clock
reg	x_bus_error_dly_1;
always @(posedge g_sclk or negedge g_reset_)//delay x_bus_error for one clock
	if (!g_reset_)
		x_bus_error_dly_1 <= #udly 1'b0;
	else
		x_bus_error_dly_1 <= #udly x_bus_error;

wire x_bus_error_err_1 = (x_bus_error === 1'b1) && (x_bus_error_dly_1 === 1'b1);

always @(posedge g_sclk)
	if (x_bus_error_err_1) begin
		$display("Error_T2_rsim --> T2RISC interface protocol error:");
		$display("x_bus_error active for more than one clock at %0t",$realtime);
		`ifdef CPU_MONITOR
		$fdisplay(c_moni_msg,"Error_T2_rsim --> T2RISC interface protocol error:");
		$fdisplay(c_moni_msg,"x_bus_error active for more than one clock at %0t",$realtime);
		`endif
	end

//"x_bus_error" only can be active together with x_ack active
wire x_bus_error_err_2 = ((x_bus_error === 1'b1) && (x_ack === 1'b0));
always @(posedge g_sclk)
	if (g_reset_ & x_bus_error_err_2) begin
		$display("Error_T2_rsim --> T2RISC interface protocol error:");
		$display("x_bus_error active doesn't together with x_ack active at %0t",$realtime);
		`ifdef CPU_MONITOR
		$fdisplay(c_moni_msg,"Error_T2_rsim --> T2RISC interface protocol error:");
		$fdisplay(c_moni_msg,"x_bus_error active doesn't together with x_ack active at %0t",$realtime);
		`endif
	end

//As to the relationship of lowest two bits of u_startpaddr and u_bytemssk. the specification does not make clear yet.

`endif

//========== Warning message ==============
`ifdef CLOSE_CPU_WARNING
`else

//cpu address error via x_bus_error indicating
always @(posedge g_sclk)
	if (x_bus_error & x_ack) begin
		$display("Warning_T2_rsim --> T2RISC interface protocol Warning:");
		$display("x_bus_error active at %0t",$realtime);
		`ifdef CPU_MONITOR
		$fdisplay(c_moni_msg,"Warning_T2_rsim --> T2RISC interface protocol Warning:");
		$fdisplay(c_moni_msg,"x_bus_error active at %0t",$realtime);
		`endif
	end

//the x_ready latency is too long after u_request active
reg	[10:0] lat_cnt; //2048
wire lat_cnt_full = (lat_cnt === 11'h7ff);
always @(posedge g_sclk or negedge g_reset_)
	if (!g_reset_)
		lat_cnt	<= #udly 11'h0;
	else if (x_ready)
		lat_cnt <= #udly 11'h0;
	else if (u_request && !lat_cnt_full)
		lat_cnt <= #udly lat_cnt + 1;

reg	lat_cnt_full_dly_1;
always @(posedge g_sclk or negedge g_reset_)
	if (!g_reset_)
		lat_cnt_full_dly_1 <= #udly 1'b0;
	else
		lat_cnt_full_dly_1 <= #udly lat_cnt_full;

wire lat_cnt_full_pulse = (lat_cnt_full & ~lat_cnt_full_dly_1);
always @(posedge g_sclk)
	if (lat_cnt_full_pulse) begin
		$display("Warning_T2_rsim --> T2RISC interface protocol Warning:");
		$display("x_ready latency is too long,more than 2048 cycles, at %0t",$realtime);
		`ifdef CPU_MONITOR
		$fdisplay(c_moni_msg,"Warning_T2_rsim --> T2RISC interface protocol Warning:");
		$fdisplay(c_moni_msg,"x_ready latency is too long,more than 2048 cycles, at %0t",$realtime);
		`endif
	end

//the u_request is in uncertain status
always @(posedge g_sclk)
	if (g_reset_ & (u_request === 1'bx)) begin
		$display("Warning_T2_rsim --> T2RISC interface protocol Warning:");
		$display("u_request is in uncertain status at %0t",$realtime);
		`ifdef CPU_MONITOR
		$fdisplay(c_moni_msg,"Warning_T2_rsim --> T2RISC interface protocol Warning:");
		$fdisplay(c_moni_msg,"u_request is in uncertain status at %0t",$realtime);
		`endif
	end

//the u_write is in uncertain status
always @(posedge g_sclk)
	if (g_reset_ & (u_write === 1'bx)) begin
		$display("Warning_T2_rsim --> T2RISC interface protocol Warning:");
		$display("u_write is in uncertain status at %0t",$realtime);
		`ifdef CPU_MONITOR
		$fdisplay(c_moni_msg,"Warning_T2_rsim --> T2RISC interface protocol Warning:");
		$fdisplay(c_moni_msg,"u_write is in uncertain status at %0t",$realtime);
		`endif
	end

//the u_wordsize is in uncertain status
always @(posedge g_sclk)
	if (g_reset_ & (^u_wordsize === 1'bx)) begin
		$display("Warning_T2_rsim --> T2RISC interface protocol Warning:");
		$display("u_wordsize is in uncertain status at %0t",$realtime);
		`ifdef CPU_MONITOR
		$fdisplay(c_moni_msg,"Warning_T2_rsim --> T2RISC interface protocol Warning:");
		$fdisplay(c_moni_msg,"u_wordsize is in uncertain status at %0t",$realtime);
		`endif
	end

//the u_startpaddr is in uncertain status
always @(posedge g_sclk)
	if (g_reset_ & (^u_startpaddr === 1'bx)) begin
		$display("Warning_T2_rsim --> T2RISC interface protocol Warning:");
		$display("u_startpaddr is in uncertain status at %0t",$realtime);
		`ifdef CPU_MONITOR
		$fdisplay(c_moni_msg,"Warning_T2_rsim --> T2RISC interface protocol Warning:");
		$fdisplay(c_moni_msg,"u_startpaddr is in uncertain status at %0t",$realtime);
		`endif
	end

//the u_bytemask is in uncertain status
always @(posedge g_sclk)
	if (g_reset_ & (^u_bytemask === 1'bx)) begin
		$display("Warning_T2_rsim --> T2RISC interface protocol Warning:");
		$display("u_bytemask is in uncertain status at %0t",$realtime);
		`ifdef CPU_MONITOR
		$fdisplay(c_moni_msg,"Warning_T2_rsim --> T2RISC interface protocol Warning:");
		$fdisplay(c_moni_msg,"u_bytemask is in uncertain status at %0t",$realtime);
		`endif
	end

//the u_data_to_send includes some uncertain data
wire [7:0] wr_byte0 = u_bytemask[0] ? u_data_to_send[7:0] 	: 8'h0;
wire [7:0] wr_byte1 = u_bytemask[1] ? u_data_to_send[15:8] 	: 8'h0;
wire [7:0] wr_byte2 = u_bytemask[2] ? u_data_to_send[23:16] : 8'h0;
wire [7:0] wr_byte3 = u_bytemask[3] ? u_data_to_send[31:24] : 8'h0;
always @(posedge g_sclk)
	if (u_write && x_ready && ((^wr_byte0 === 1'bx)|(^wr_byte1 === 1'bx)|(^wr_byte2 === 1'bx)|(^wr_byte3 === 1'bx)))
	begin
		$display("Warning_T2_rsim --> T2RISC interface protocol Warning:");
		$display("u_data_to_send inclues uncertain data byte at %0t, addr=%h,u_data=%h",$realtime,u_startpaddr,u_data_to_send);
		`ifdef CPU_MONITOR
		$fdisplay(c_moni_msg,"Warning_T2_rsim --> T2RISC interface protocol Warning:");
		$fdisplay(c_moni_msg,"u_data_to_send inclues uncertain data byte at %0t, addr=%h,u_data=%h",$realtime,u_startpaddr,u_data_to_send);
		`endif
	end

//x_data_in includes some uncertain data
wire [7:0] rd_byte0 = u_bytemask[0] ? x_data_in[7:0] 	: 8'h0;
wire [7:0] rd_byte1 = u_bytemask[1] ? x_data_in[15:8] 	: 8'h0;
wire [7:0] rd_byte2 = u_bytemask[2] ? x_data_in[23:16] 	: 8'h0;
wire [7:0] rd_byte3 = u_bytemask[3] ? x_data_in[31:24] 	: 8'h0;
always @(posedge g_sclk)
	if (!u_write && x_ready && ((^rd_byte0 === 1'bx)|(^rd_byte1 === 1'bx)|(^rd_byte2 === 1'bx)|(^rd_byte3 === 1'bx)))
	begin
		$display("Warning_T2_rsim --> T2RISC interface protocol Warning:");
		$display("x_data_in inclues uncertain data byte at %0t, addr=%h,x_data=%h",$realtime,u_startpaddr,x_data_in);
		`ifdef CPU_MONITOR
		$fdisplay(c_moni_msg,"Warning_T2_rsim --> T2RISC interface protocol Warning:");
		$fdisplay(c_moni_msg,"x_data_in inclues uncertain data byte at %0t, addr=%h,x_data=%h",$realtime,u_startpaddr,x_data_in);
		`endif
	end

//u_bytemask's value is set to no use data, 4'b0000
always @(posedge g_sclk)
	if (u_write && x_ready && (u_bytemask === 4'b0000))
	begin
		$display("Warning_T2_rsim --> T2RISC interface protocol Warning:");
		$display("u_bytemask are set to no use value at %0t, addr=%h,u_bytemask=%h",$realtime,u_startpaddr,u_bytemask);
		`ifdef CPU_MONITOR
		$fdisplay(c_moni_msg,"Warning_T2_rsim --> T2RISC interface protocol Warning:");
		$fdisplay(c_moni_msg,"u_bytemask are set to no use value at %0t, addr=%h,u_bytemask=%h",$realtime,u_startpaddr,u_bytemask);
		`endif
	end

//u_bytemask's value is set to no use data during burst operation
//all byte must be valid
always @(posedge g_sclk)
	if (u_write && x_ready && (u_len !== 3'b000) && (u_bytemask !== 4'b1111))
	begin
		$display("Warning_T2_rsim --> T2RISC interface protocol Warning:");
		$display("u_bytemask are set to no use value at %0t, addr=%h,u_bytemask=%h",$realtime,u_startpaddr,u_bytemask);
		`ifdef CPU_MONITOR
		$fdisplay(c_moni_msg,"Warning_T2_rsim --> T2RISC interface protocol Warning:");
		$fdisplay(c_moni_msg,"u_bytemask are set to no use value at %0t, addr=%h,u_bytemask=%h",$realtime,u_startpaddr,u_bytemask);
		`endif
	end


`endif
endmodule
