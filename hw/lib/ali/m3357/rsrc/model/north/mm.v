/*******************************************************************
          (c) copyrights 1997-1998. All rights reserved

           T-Square Design, Inc.

          Company confidential and Properietary information.
          This information may not be disclosed to unauthorized
          individual.
*******************************************************************/

/*
        The PCI Bus master is modified to hanlde
        PCI to PCI Bridge simulations.

        1. Normally, this bus master generates Type 0 Configuration
        cycles. However, when the configuration cycles are
        addressed to a secondary PCI bus device, the configuration
        cycle must be Type 1. The PCI to PCI bridge then converts
        type 1 Configuration cycle to Type 0.

        Type 0 configuration format is as follows :-

        31           24  23         16     15    11 10  8  7   2  1  0
        ---------------------------------------------------------
        |    ONLY ONE '1'           | RES |    func |register| 0|0 |
        |                           | RVD |    no   |number  ||    |
        ---------------------------------------------------------

        This bus master will generate Configuration type 1
        cycles.

        The format of type 1 configuration cycle is :-

        31           24  23         16     15    11 10  8  7          2  1  0
        ---------------------------------------------------------
        |  Reserved      |    Bus            | dev |    func |register| 0|1 |
        |                |    Number         | no  |    no   |number  |     |    |
        ---------------------------------------------------------

*/

//`timescale      1ns/1ps

//	2002-10-10	Yuky,	Make the "req_" signal as tri-state signal.
//						Control by the "doIt" temporary
module master (
              gnt_,         req_,          ad,            cbe_,
              par,          frame_,        irdy_,         trdy_,
              stop_,        lock_,         perr_,         serr_,
              devsel_,      //idsel,
              clock,         rst_ ,
              own_bus,      enable, back2back  );


inout [31:0]  ad;
inout [ 3:0]  cbe_;//,         idsel;
inout         par,          frame_,        irdy_,         perr_,
              serr_,        lock_,         devsel_,       trdy_,
              stop_;
input         gnt_,         rst_,          clock;
input	      enable;
output        req_;
output        own_bus;
output        back2back ;

parameter
    clk_width      =15,
    omi_ad_delay   =2,
    omi_cbe_delay  =2,
    omi_ctrl_delay =5;

parameter
    u_ad_off_delay   =2,
    u_cbe_off_delay  =2,
    u_ctl_off_delay  =2,
    u_irdy_off_delay =2,
    u_frame_off_delay=2,
    u_frame_on_delay =2;

parameter bus_sel=0;  //default
parameter sel_rate=1;  //default
parameter trdyWait=60;//default

wire	req_;

integer  f_name;
integer  pb_name;
/*
initial begin

//    pb_name = $fopen("playback.txt");          //98-06-12, Yi // 98-11-27 KD
    case (bus_sel)
    0:   f_name = $fopen("p_master.rpt");
    1:   f_name = $fopen("a_master.rpt");
    2:   f_name = $fopen("b_master.rpt");
    default: begin
               $display("Error_T2_biu: Wrong bus number for master model");
               $stop;
             end
    endcase

    $fdisplay(f_name, "START HERE");
//    $fdisplay(pb_name, "PB START HERE");
end
*/
//////////////// burst buffer //////////////////////////
integer    burst_cnt;
integer    burst_adr;
reg [31:0] burst_buffer [0:1791];
initial begin

/*
  if (sel_rate)
     $readmemh("../rsim/spdif_dat.txt",burst_buffer,0,1791);
  else
     $readmemh("../rsim/rec_all_24k.txt",burst_buffer,0,1791);
*/

  burst_cnt = 0;
  burst_adr = 0;
end
//////////////////////////////////////////////////////// 98-06-12, Yi

`define default_ado     32'h1111_2222

`define INTERRUPT_ACKNOWLEDGE   4'b0000
`define SPECIAL_CYCLE           4'b0001
`define IO_READ                 4'b0010
`define IO_WRITE                4'b0011
`define PCI_RESERVED1           4'b0100
`define PCI_RESERVED2           4'b0101
`define MEMORY_READ             4'b0110
`define MEMORY_WRITE            4'b0111
`define PCI_RESERVED3           4'b1000
`define PCI_RESERVED4           4'b1001
`define CONFIGURATION_READ      4'b1010
`define CONFIGURATION_WRITE     4'b1011
`define MEMORY_READ_MULTIPLE    4'b1100
`define DUAL_ADDRESS_CYCLE      4'b1101
`define MEMORY_READ_LINE        4'b1110
`define MEMORY_WRITE_INVALIDATE 4'b1111




parameter
  sizeOfMemory = 512;


reg   [43:0]  tmpMemReg,    writeBuffer[sizeOfMemory:0],
                            readBuffer[sizeOfMemory:0];

reg   [31:0]  ado,          adi,           pciData,       address,
              address2;
reg   [ 3:0]  cbeo_,        cbei_,         curPciCmd,     cbe_b[3:0],
              cbe_w[3:0],   cbe_dw[3:0],   curCbe_;
//reg	[15:0]	curIdSel;
reg   [ 7:0]  irdyCntP;
wire          parEnable,    busIdle;

reg              cfg_type0 ;
reg           mas_adEnable, mas_cbeEnable, mas_ctlEnable,     shadAdEnable,
              perrEnable,   lockEnable,    paro,          frameo_,
              irdyo_,       perro_,        serro_,        reqo_,
              locko_,       serrEnable,    prevAdEnable,  framei_,
              trdyi_,       pari,          irdyi_,        doIt,
              dualAccess,   contTrx,       stopi_,        devseli_,
              gnti_,        cmdIsWrite,    cfgCycle,      idEnable;
reg          park_adEnable,     park_cbeEnable,        park_parEnable ;
reg          park_ctlEnable ;
reg           mas_chkPerr ;
reg           bus_master1 ;
reg           gen_data_parity ;
reg          genBadCmdParity ;
reg          genBadParity ;
reg              got_lock ;
reg              req_lock ;
reg              release_lock ;
reg              lock_obtained ;
reg        cmdCycle ;
reg        cmdCycle1 ;
reg          busop_fail ;
reg          Master_abort ;
reg          Disc_With_Data ;
reg          Target_abort ;
reg          Target_retry ;
reg        retry_busop ;
reg        Loopit ;
reg        prevCmdRead ;
reg        rdCmd_perr ;
reg        curRdCmd ;
reg        mas_rd_data ;

reg        back2back ;

reg [3:0]       curTargetCmd ;
reg [31:0]      pci_data ;
reg [31:0]      target_ado ;
reg             serri_ ,    oldframei_ ;
reg             newCmd ;
reg             readCmd,    op_valid,       target_burst,   firstTime ;

integer         i;
integer         driveDelay, abortCnt ;
integer         validCbeDelay ;
integer         trdy_count ;

integer        ad_off_delay ;
integer        cbe_off_delay ;
integer        ctl_off_delay ;
integer        irdy_off_delay ;
integer        frame_off_delay ;
integer        frame_on_delay ;

reg             perr_detected ;
reg             signalled_serr ;
reg             rcvd_masterAbort ;
reg             rcvd_targetAbort ;
reg             signaled_targetAbort ;
reg             badParity_detected ;

reg             trdyo_ ,        devselo_,      tstopo_ ;
reg             rcvd_parity ;
reg             tranfer_Ok ;
reg             validOp ;
reg             newParity ;

reg  [15:0]     command_reg ;
reg  [31:0]     target_address ;

//reg        idsel_in ;
reg        fast_timing ;


//3-5-96 add
reg grant_bus;


integer       //adDelay,
              //cbeDelay,
              //ctlDelay,
              checkParity,
              cacheSize,    devselWait,    irdyDriveDelay,
              adDriveDelay, cbeDriveDelay, trxCount,      trxLen,
              startIdx,     irdyCnt,       idTurnOnDelay, idTurnOffDelay;
integer       reqDelay;
integer          lock_timer ;
integer          perr_count ;
//integer          idsel_Num  ;

integer       ad_delay,     cbe_delay,     ctl_delay,     idelay;
integer       irdy_delay;

wire           adEnable,        cbeEnable ;


//*******************  END MONITOR PAD SIGNAL **************************
//added to make it compatible with the old master model
// 11/11/93
/*
always  @(ad_delay) begin
        adDelay = ad_delay;
end
always  @(cbe_delay) begin
        cbeDelay = cbe_delay;
end
always  @(ctl_delay) begin
        ctlDelay = ctl_delay;
end
*/

always  @(idelay) begin
        irdyCntP = idelay;
end

wire    ctlEnable ;
reg     tar_ctl_enable ;
reg     tar_ad_enable ;
reg     tar_perr_enable ;
reg     tar_serr_enable ;

wire            dev_sel_ ;
wire            stop_ ;
wire [15:0]     status_reg ;
reg  [31:0]     configData ;

wire    [31:0]  ad_out ;

`protect

assign  ad_out  = tar_ad_enable ? target_ado : ado ;

assign   adEnable  = mas_adEnable  || (park_adEnable) ||
                     tar_ad_enable ;
assign   cbeEnable = mas_cbeEnable || (park_cbeEnable );
assign   ctlEnable = mas_ctlEnable || tar_ctl_enable ;

`endprotect

parameter tapeout=1;
integer t_clock, t_req, t_ad, t_cbe, t_idsel, t_frame, t_irdy, t_devsel, t_trdy, t_stop, t_par, t_perr, t_serr, t_lock;


/////////////////////////////////////////////////////////////////
// assign  #2 ad        = (adEnable)   ? ad_out  : 32'bz;
wire [31:0]  ad_tmp;
reg  [31:0]  ad_reg;
wire [3:0]  cbe__tmp;
reg  [3:0]  cbe__reg;
wire [15:0] idsel_tmp;
//reg  [3:0] idsel_reg;
wire own_bus_tmp,req_tmp_,par_tmp,frame__tmp,irdy__tmp,perr__tmp,serr__tmp,lock__tmp;
reg own_bus_reg,req_reg_,par_reg,frame__reg,irdy__reg,perr__reg,serr__reg,lock__reg;

assign  ad_tmp    = (adEnable & enable)   ? ad_out  : 32'bz;
assign  ad         = ad_reg;

always @(negedge rst_ or posedge clock)
	if (!rst_) ad_reg= 32'hz;
	else begin
		#omi_ad_delay;
		ad_reg= ad_tmp;
	end
/////////////////////////////////////////////////////////////////
//assign  #2 cbe_      = (cbeEnable)  ? cbeo_   : 4'bz;
assign  cbe__tmp      = (cbeEnable & enable)  ? cbeo_   : 4'bz;
assign  cbe_= cbe__reg;

always @(negedge rst_ or posedge clock)
	if (!rst_) cbe__reg = 4'hz;
	else begin
		#omi_ad_delay;
		cbe__reg= cbe__tmp;
	end


//assign  #2  idsel     = (idEnable) ? curIdSel : 4'bz;
//assign  #2 par       = (parEnable)  ? paro    : 1'bz;
//assign  #2 frame_    = (mas_ctlEnable)  ? frameo_ : 1'bz;
//assign  #2 irdy_     = (mas_ctlEnable)  ? irdyo_  : 1'bz;
//assign  #2 perr_     = (perrEnable) ? perro_  : 1'bz;
//assign  #2 serr_     = (serrEnable) ? serro_  : 1'bz;
//assign  #2 lock_     = (lockEnable) ? locko_  : 1'bz;
//assign  req_         = reqo_;
//assign            own_bus   = ctlEnable ;
assign              parEnable = prevAdEnable;
assign              busIdle   = frame_ & irdy_;


/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
//assign  idsel_tmp     = (idEnable & enable) ? curIdSel : 4'bz;
//assign  idsel         = idsel_reg;
assign  par_tmp       = (parEnable & enable)  ? paro    : 1'bz;
assign  par           = par_reg;
assign  frame__tmp = (mas_ctlEnable & enable)  ? frameo_ : 1'bz;
assign  frame_        = frame__reg;
assign  irdy__tmp     = (mas_ctlEnable & enable)  ? irdyo_  : 1'bz;
assign  irdy_         = irdy__reg;
assign  perr__tmp     = (perrEnable & enable) ? perro_  : 1'bz;
assign  perr_         = perr__reg;
assign  serr__tmp     = (serrEnable & enable) ? serro_  : 1'bz;
assign  serr_         = serr__reg;
assign  lock__tmp     = (lockEnable & enable) ? locko_  : 1'bz;
assign  lock_         = lock__reg;
//assign  req_tmp_         = reqo_;
//assign  req_         = enable ? req_reg_ : 1'bz;
//modified by yuky,2002-10-10.
assign	req_tmp_	= (doIt & enable)?  reqo_ : 1'bz;
assign  req_        = req_reg_;

always @(negedge rst_ or posedge clock)
	if (!rst_) begin
		//idsel_reg  = 1'bz;
  		par_reg    = 1'bz;
		frame__reg = 1'bz;
		irdy__reg  = 1'bz;
		perr__reg  = 1'bz;
		serr__reg  = 1'bz;
		lock__reg  = 1'bz;
		req_reg_   = 1'bz;
	end else begin
	 	#omi_ctrl_delay;
		//idsel_reg  = idsel_tmp;
  		par_reg    = par_tmp;
		frame__reg = frame__tmp;
		irdy__reg  = irdy__tmp;
		perr__reg  = perr__tmp;
		serr__reg  = serr__tmp;
		lock__reg  = lock__tmp;
		req_reg_   = req_tmp_;
	end


/////////////////////////////////////////////////////////////
assign  #2  own_bus   = ctlEnable & enable;
/////////////////////////////////////////////////////////////
assign  #2 trdy_     = (tar_ctl_enable) ? trdyo_   : 1'bz ;
/////////////////////////////////////////////////////////////////
assign  #2 stop_     = (tar_ctl_enable) ? tstopo_  : 1'bz ;
////////////////////////////////////////////////////////////
assign  #2 devsel_   = (tar_ctl_enable) ? devselo_ : 1'bz ;

task  fast ;
begin

/*
  cbe_delay      = 10;
  ctl_delay      = 10;
  reqDelay       = 10;
  irdy_delay     = 10;
  //04-29-96 ad_delay       = 10;
  //03-12-96: ad_off_delay   =  1 ;
  //03-12-96: cbe_off_delay  =  1 ;
*/

  reqDelay        = 10;

  ad_delay        = u_ad_off_delay;
  ad_off_delay    = u_ad_off_delay;

  cbe_delay       = u_cbe_off_delay;
  cbe_off_delay   = u_cbe_off_delay;

  ctl_delay       = u_ctl_off_delay;
  ctl_off_delay   = u_ctl_off_delay;

  irdy_delay      = u_irdy_off_delay;
  irdy_off_delay  = u_irdy_off_delay;

  frame_on_delay  = u_frame_off_delay;
  frame_off_delay = u_frame_off_delay;

end
endtask     // setting timing parameters


task  slow ;
begin

/*
  ad_delay       =  20;
  cbe_delay      =  20;
  ctl_delay      =  20;
*/
  ad_delay       =  10;
  cbe_delay      =  10;
  ctl_delay      =  10;

  reqDelay       = 10;

  ad_off_delay   = 1 ;
  cbe_off_delay  = 1 ;
  ctl_off_delay  = 1 ;

  irdy_off_delay  = 1 ;
  frame_off_delay = 1 ;

end
endtask         // setting timing parameters

`protect
/*
always @(idsel or idsel_Num) begin

  case (idsel_Num)

     0 :  idsel_in = idsel [0] ;
     1 :  idsel_in = idsel [1] ;
     2 :  idsel_in = idsel [2] ;
     3 :  idsel_in = idsel [3] ;

     default : begin
    $display ("Error_T2_biu: %0t, ID_SELECT for the Master is wrong idsel_Num = ", $realtime, idsel_Num);
    $fdisplay (f_name, "Error_T2_biu: MASTER: ID_SELECT for the Master is wrong idsel_Num = ", idsel_Num);
     end
  endcase     // end of ID Select Decode.

end         // selecting the proper Id for Idsel
*/
always @(devsel_) begin
     devseli_ = devsel_;
end




always @(posedge clock) begin
  prevAdEnable = mas_adEnable || tar_ad_enable ;

  mas_chkPerr  = bus_master1 ;

  mas_rd_data  = mas_ctlEnable && ~irdy_ && ~trdy_ && ~curPciCmd[0] ;
  bus_master1  = mas_adEnable || mas_rd_data ;

  adi          = ad;
  framei_      = frame_;

  if (rdCmd_perr)
    perrEnable =  1 ;
  else
    perrEnable =  0 ;

  curRdCmd     = ~curPciCmd[0]  || ~curTargetCmd[0] ;
  rdCmd_perr   = (~irdy_ && ~trdy_ && curRdCmd && ctlEnable);

  trdyi_       = trdy_;
  irdyi_       = irdy_;
  pari         = par;
  cbei_        = cbe_;
  stopi_       = stop_;
  gnti_        = gnt_;

  cmdCycle1 = cmdCycle;

  perro_ =  ~(gen_data_parity ^ pari );

  if (perrEnable && ~perro_)  begin
    $display ("Error_T2_biu: %0t: Master Detected Parity Error during Read Operation ", $realtime );
    $fdisplay (f_name, "Error_T2_biu: Master: Detected Parity Error during Read Operation ");
  end

  gen_data_parity = ^( {adi[31:0], cbei_[3:0]} );
  if (cmdCycle)
      paro         = (genBadCmdParity) ? ~(^({cbe_,ad})) : ^({cbe_,ad});
  else if (genBadParity && (perr_count == trxCount) )
      paro         = ~(^({cbe_,ad})) ;
  else
      paro         = ^({cbe_,ad});

end


/****
 **** If the arbiter parks on us we must drive the
 **** ad and cbe_ lines ONCE the bus becomes idle.
 ****
 ****/
// ED: 08/14/96
always @(posedge clock or negedge rst_) begin:gotGntWait4BusIdle
  cmdCycle1 = cmdCycle;
  if(!rst_) begin
      park_adEnable = 0;
      park_cbeEnable = 0;
      park_ctlEnable = 0;
  end
  else if (~gnt_ && busIdle) begin
      park_adEnable  = 1;
      park_cbeEnable = 1;
      park_ctlEnable = 1;
  end else begin
      park_adEnable  = 0;
      park_cbeEnable = 0;
      park_ctlEnable = 0;
  end
end

`endprotect

/****
 **** Initialize all variables
 ****
 ****/
initial begin
  $display("******** %m: PCI Master Model Version 1.K ********");

  fast_timing = 1 ;

  if (fast_timing) begin
     $display("******** %m: executing Fast Timing Model ********");
     fast ;
  end else begin
     $display("******** %m: executing SLOW Timing Model ********");
     slow ;
  end

// added to make it compatible with old master model
  checkParity    = 0;
  genBadParity   = 0;
  genBadCmdParity= 0;
  back2back     = 0;
  cacheSize      = 0;
  devselWait     = 6;
//  trdyWait       = 120;
//  trdyWait       = 20;

//    added cfg_type0 flag to generate either type 0 or
//  type 1 configuration cycles.
//        by default it generates type 0 config cycles.
//  added 1/05/94

  cfg_type0      = 1 ;
  Loopit   	= 0 ;
  mas_adEnable       = 0;
  mas_cbeEnable      = 0;
  mas_ctlEnable      = 0;
  perrEnable     = 0;
  lockEnable     = 0;
  serrEnable     = 0;
  prevAdEnable   = 0;
  rdCmd_perr     = 0;
  curRdCmd       = 0;
  ado            = 32'b0;
  pciData        = 32'b0;
  paro           = 1'b0;
  frameo_        = 1'b1;
  irdyo_         = 1'b1;
  perro_         = 1'b1;
  serro_         = 1'b1;
  reqo_          = 1'b1;
  locko_         = 1'b1;

  devselo_       = 1'b1 ;
  tstopo_        = 1'b1 ;
  trdyo_         = 1'b1 ;

  framei_        = 1'b1;
  trdyi_         = 1'b1;
  pari           = 1'b1;
  stopi_         = 1'b1;
  devseli_       = 1'b1;
  doIt           = 0;
  dualAccess     = 0;
  curPciCmd      = `IO_READ;
  cbeo_          = `IO_READ;
  curCbe_        = `IO_READ;
  contTrx        = 0;
  adDriveDelay   = 0;
  irdyDriveDelay = 0;
  cbeDriveDelay  = 0;
  trxCount       = 0;
  trxLen         = 0;
  irdyCnt        = 0;
  irdyCntP       = 0;
  cbe_b[0]       = 4'b1110;
  cbe_b[1]       = 4'b1101;
  cbe_b[2]       = 4'b1011;
  cbe_b[3]       = 4'b0111;
  cbe_w[0]       = 4'b1100;
  cbe_w[1]       = 4'b1001;
  cbe_w[2]       = 4'b0011;
  cbe_w[3]       = 4'b0111;
  cbe_dw[0]      = 4'b0000;
  cbe_dw[1]      = 4'b0001;
  cbe_dw[2]      = 4'b0011;
  cbe_dw[3]      = 4'b0111;
  cfgCycle       = 0;
//  curIdSel       = 16'b0;
  idTurnOnDelay  = 10;
  idTurnOffDelay = 10;
  idEnable       = 0;
  lock_timer      = 1000 ;
  got_lock      = 0 ;
  req_lock      = 0 ;
  release_lock     = 0 ;
  lock_obtained  = 0 ;
  cmdCycle          = 0 ;
  busop_fail     = 0 ;
//  retry_busop     = 0 ;
  retry_busop     = 1 ;
  prevCmdRead     = 1  ;
  perr_count      = 0 ;

  command_reg           = 16'h0 ;
  target_ado         = 16'h0 ;

  perr_detected         = 0 ;
  signalled_serr        = 0 ;
  rcvd_masterAbort      = 0 ;
  rcvd_targetAbort      = 0 ;
  signaled_targetAbort  = 0 ;
  badParity_detected    = 0 ;

  address2      = 32'b0 ;    // Used for Dual Access

  driveDelay     = 0 ;
  abortCnt       = 0 ;
  validCbeDelay  = 15 ;
  trdy_count     = 1 ;

//  idsel_Num     = 1 ;

  tar_ctl_enable = 0 ;
  tar_ad_enable  = 0;
  tar_perr_enable = 0 ;
  tar_serr_enable = 0;

  for (i=0;i<sizeOfMemory; i = i + 1)	begin
  	writeBuffer[i] = 44'h0;
	readBuffer [i] = 44'h0;
  end

end

task getTheBus;
begin

`protect

/************************************************************************

    If we are trying to obtain lock and lock_ is already asserted
    then we must wait for lock_ to become inactive before requesting
    bus.

************************************************************************/
  if (req_lock && locko_ && ~lock_)
    begin

    fork

      begin: lock_timeout

        repeat (lock_timer) @(posedge clock);

        $fdisplay (f_name, "Error_T2_biu: %0t %m Lock Timeout after waiting ", $realtime, lock_timer,"clocks");
        $fdisplay (f_name, "waiting for lock_ to become inactive");
        $display ("Error_T2_biu: %0t %m Lock Timeout after waiting ", $realtime, lock_timer,"clocks");
        $display ("waiting for lock_ to become inactive");
        disable lock_timeout;

      end         // end of lock_timeout

      begin: lock_wait
        wait (lock_ && busIdle);
        while (~lock_) @(posedge clock);
        disable lock_timeout ;
      end         // waiting for lock_ to become inactive

    join

    end        // wait for lock_ to become inactive.

  reqo_ = 1'b0;

  if (back2back && ~prevCmdRead) begin
    mas_adEnable  = 1;
    mas_cbeEnable = 1;
    mas_ctlEnable = 1;
  end
  else begin
    mas_adEnable  = 0;
    mas_cbeEnable = 0;
    mas_ctlEnable = 0;
    grant_bus = 0;
    while (~grant_bus) begin
      wait (~gnt_);
      @(posedge clock); // at this clock edge grant is valid
      //wait (frame_ & irdy_);
      while (!frame_ | !irdy_ | gnt_) @(posedge clock); // bus idle at this clock edge
      grant_bus=1;
    end
  end

end
endtask

`endprotect
/*
task assertID ;

    input    [3:0]    id_select ;
 begin
    case (id_select)
        2'b00    : curIdSel = 4'b0001 ;
        2'b01    : curIdSel = 4'b0010 ;
        2'b10    : curIdSel = 4'b0100 ;
        2'b11    : curIdSel = 4'b1000 ;
    endcase
 end
endtask
*/
function [15:0] assertID;
input	[4:0]	id_select;
  begin
	case(id_select)
	5'b00000:	assertID  = 16'h1;
	5'b00001:	assertID  = 16'h2;
	5'b00010:	assertID  = 16'h4;
	5'b00011:	assertID  = 16'h8;
	5'b00100:	assertID  = 16'h10;
	5'b00101:	assertID  = 16'h20;
	5'b00110:	assertID  = 16'h40;
	5'b0111:	assertID  = 16'h80;
	5'b01000:	assertID  = 16'h100;
	5'b01001:	assertID  = 16'h200;
	5'b01010:	assertID  = 16'h400;
	5'b01011:	assertID  = 16'h800;
	5'b01100:	assertID  = 16'h1000;
	5'b01101:	assertID  = 16'h2000;
	5'b01110:	assertID  = 16'h4000;
	5'b01111:	assertID  = 16'h8000;
	default:	begin
		$fdisplay (f_name, "Error_T2_biu: Incorrected Device Number %b", id_select);
	        $display ("Error_T2_biu: Incorrected Device Number %b", id_select);
		assertID  = 16'h0;
	end
	endcase
  end
endfunction

task PCI_cmd ;
  input  [ 3:0]     command ;
  input  [ 3:0]     byteEn ;
  input  [ 7:0]     irdyDelay ;
  input  [31:0]  addr;
  input  [31:0]  data;
  output [31:0]  datard;
begin
`protect
  cfgCycle             = 0;
  startIdx             = sizeOfMemory;
  trxLen               = 1;
  curPciCmd            = command;

  address = addr ;

  if ((curPciCmd == `CONFIGURATION_READ) ||
      (curPciCmd == `CONFIGURATION_WRITE) ) begin

    $fdisplay (f_name, "Error_T2_biu: %0t: WARNING!! Warning!! PCI_cmd does not support Config CMDs", $realtime);
    $fdisplay (f_name, "Error_T2_biu:  IDSEL is not set. Operation will fail. with Master Abort ");
    $display ("Error_T2_biu: %0t: WARNING!! Warning!! PCI_cmd does not support Config CMDs", $realtime);
    $display ("Error_T2_biu:  IDSEL is not set. Operation will fail. with Master Abort ");
  end         // Checking for Configuration Command.

  if (curPciCmd [0]) begin
    writeBuffer[startIdx] = {byteEn,irdyDelay,data};
      cmdIsWrite           = 1 ;
  end else begin
    readBuffer [startIdx] = {byteEn,irdyDelay,32'hxxxx_xxxx};
      cmdIsWrite           = 0;
  end

  doIt                 = 1;
  wait(~doIt);

  if (~curPciCmd [0]) begin
     tmpMemReg = readBuffer[startIdx];
     datard = tmpMemReg [31:0];
  end

`endprotect
end
endtask        // End of Generic PCI Command.


task PCI_confrd_b;
  input  [4:0]  id_select;
  input  [2:0]	func_num;
  input  [7:0]  addr;
  output [7:0]  data;
begin


`protect
  cfgCycle             = 1;
  cmdIsWrite           = 0;
  startIdx             = sizeOfMemory;
  trxLen               = 1;
  curPciCmd            = `CONFIGURATION_READ;
  readBuffer[startIdx] = {cbe_b[addr[1:0]],irdyCntP,32'h0};

  if (cfg_type0) begin
      address            = {assertID(id_select), id_select,func_num, addr[7:2], 2'b00};
  end else begin
      address            = {16'h0, id_select, func_num, addr[7:2],2'b01};
  end

  doIt                 = 1;
  wait(~doIt);
  tmpMemReg = readBuffer[startIdx];
  case (addr[1:0])
    2'b00: data = tmpMemReg[7:0];
    2'b01: data = tmpMemReg[15:8];
    2'b10: data = tmpMemReg[23:16];
    2'b11: data = tmpMemReg[31:24];
  endcase
  cfgCycle             = 0;
  $fdisplay(f_name ,"PCI_CONFRD_B: IDSEL=%h, ADDR=%h, data=%h",id_select, addr, data);

`endprotect
end
endtask



task PCI_special;
  input [31:0] addr, data;
  integer savedDevselWait;
begin
`protect
  savedDevselWait       = devselWait;
  devselWait            = 6;
  cfgCycle              = 0;
  cmdIsWrite            = 1;
  startIdx              = sizeOfMemory;
  trxLen                = 1;
  curPciCmd             = `SPECIAL_CYCLE;
  writeBuffer[startIdx] = {4'b0,irdyCntP,data};
  address               =  addr;
  doIt                  = 1;
  wait(~doIt);
  devselWait            = savedDevselWait;
`endprotect
end
endtask



task PCI_intAck;
  input  [31:0] addr;
  input  [ 3:0] enables;
  output [31:0] vector;
`protect
begin
  cfgCycle             = 0;
  cmdIsWrite           = 0;
  startIdx             = sizeOfMemory;
  trxLen               = 1;
  curPciCmd            = `INTERRUPT_ACKNOWLEDGE;
  readBuffer[startIdx] = {enables,irdyCntP,32'h0};
  address              = addr;
  doIt                 = 1;
  wait(~doIt);
  tmpMemReg = readBuffer[startIdx];
  vector = 32'hx;
  if (enables[0] === 1'b0) vector[ 7:0]  = tmpMemReg[7:0];
  if (enables[1] === 1'b0) vector[15:8]  = tmpMemReg[15:8];
  if (enables[2] === 1'b0) vector[23:16] = tmpMemReg[23:16];
  if (enables[3] === 1'b0) vector[31:24] = tmpMemReg[31:24];
`endprotect
end
endtask


task PCI_confrd_w;
  input  [4:0] id_select;
  input	 [2:0] func_num;
  input  [7:0] addr;
  output [15:0] data;
`protect
begin


  cfgCycle             = 1;
  cmdIsWrite           = 0;
  startIdx             = sizeOfMemory;
  trxLen               = 1;
  curPciCmd            = `CONFIGURATION_READ;
  readBuffer[startIdx] = {cbe_w[addr[1:0]],irdyCntP,32'h0};

  if (cfg_type0) begin
      address            = {assertID(id_select), id_select,func_num, addr[7:2], 2'b00};
  end else begin
      address            = {16'h0, id_select, func_num, addr[7:2],2'b01};
  end
  doIt                 = 1;
  wait(~doIt);
  tmpMemReg = readBuffer[startIdx];
  case (addr[1:0])
    2'b00: data = tmpMemReg[15:0];
    2'b01: data = tmpMemReg[23:8];
    2'b10: data = tmpMemReg[31:16];
    2'b11: data = tmpMemReg[31:16];
  endcase
  cfgCycle             = 0;
  $fdisplay(f_name ,"PCI_CONFRD_W: IDSEL=%h, ADDR=%h, data=%h",id_select, addr, data);
`endprotect
end
endtask


task PCI_confrd_dw;
  input  [4:0] id_select;
  input  [2:0] func_num;
  input  [7:0] addr;
  output [31:0] data;
`protect
begin

  cfgCycle             = 1;
  cmdIsWrite           = 0;
  startIdx             = sizeOfMemory;
  trxLen               = 1;
  curPciCmd            = `CONFIGURATION_READ;
  readBuffer[startIdx] = {cbe_dw[addr[1:0]],irdyCntP,32'h0};

  if (addr[1:0] != 2'b00) begin
    $fdisplay (f_name, "Error_T2_biu: %0t: %m: WARNING Address Not on DW Boundary. May be Error", $realtime );
    $display ("Error_T2_biu: %0t: %m: WARNING Address Not on DW Boundary. May be Error", $realtime );
  end

  if (cfg_type0) begin
      address            = {assertID(id_select), id_select,func_num, addr[7:2], 2'b00};
  end else begin
      address            = {16'h0, id_select, func_num, addr[7:2],2'b01};
  end

  doIt                 = 1;
  wait(~doIt);
  tmpMemReg = readBuffer[startIdx];
  data      = tmpMemReg[31:0];
  cfgCycle             = 0;
  $fdisplay(f_name ,"PCI_CONFRD_DW: IDSEL=%h, ADDR=%h, data=%h",id_select, addr, data);
`endprotect
end
endtask


task PCI_confwr_b;
  input [4:0] id_select;
  input [2:0] func_num;
  input [7:0] addr;
  input [ 7:0] data;
`protect
begin

$fdisplay(f_name ,"PCI_CONFWR_B: IDSEL=%h, ADDR=%h, data=%h",id_select, addr, data);

  cfgCycle              = 1;
  cmdIsWrite            = 1;
  startIdx              = sizeOfMemory;
  trxLen                = 1;
  curPciCmd             = `CONFIGURATION_WRITE;
  writeBuffer[startIdx] = {cbe_b[addr[1:0]],irdyCntP,{data,data,data,data}};

  if (cfg_type0) begin
      address            = {assertID(id_select), id_select,func_num, addr[7:2], 2'b00};
  end else begin
      address            = {16'h0, id_select, func_num, addr[7:2],2'b01};
  end

  doIt                  = 1;
  wait(~doIt);
  cfgCycle              = 0;
`endprotect
end
endtask


task PCI_confwr_w;
  input [4:0] id_select;
  input [2:0] func_num;
  input [7:0] addr;
  input [15:0] data;
`protect
begin

$fdisplay(f_name ,"PCI_CONFWR_W: IDSEL=%h, ADDR=%h, data=%h",id_select, addr, data);

  cfgCycle              = 1;
  cmdIsWrite            = 1;
  startIdx              = sizeOfMemory;
  trxLen                = 1;
  curPciCmd             = `CONFIGURATION_WRITE;
  case ({addr[1], addr[0]})
    2'b00 : writeBuffer[startIdx] = {cbe_w[addr[1:0]],irdyCntP,
                          {16'h5a5a,data}};
    2'b01 : writeBuffer[startIdx] = {cbe_w[addr[1:0]],irdyCntP,
                        {8'h5a,data,8'h5a}};
    2'b10 : writeBuffer[startIdx] = {cbe_w[addr[1:0]],irdyCntP,
                        {data, 16'h5a5a }};
    2'b11: begin
        writeBuffer[startIdx] = {cbe_w[addr[1:0]],irdyCntP,
                        {data, 16'h5a5a }};
        $fdisplay (f_name, "Error_T2_biu: %0t WARNING PCI_CONFGWR_W TASK ........", $realtime );
        $fdisplay (f_name, "Error_T2_biu: Inavlid Alignment for Word address " );
        $fdisplay (f_name, "Error_T2_biu: ADDR  = %h ", addr, " data = %h ", data );
        $display ("Error_T2_biu: %0t WARNING PCI_CONFGWR_W TASK ........", $realtime );
        $display ("Error_T2_biu: Inavlid Alignment for Word address " );
        $display ("Error_T2_biu: ADDR  = %h ", addr, " data = %h ", data );
       end
   endcase

  if (cfg_type0) begin
      address            = {assertID(id_select), id_select,func_num, addr[7:2], 2'b00};
  end else begin
      address            = {16'h0, id_select, func_num, addr[7:2],2'b01};
  end

  doIt                 = 1;
  wait(~doIt);
  cfgCycle             = 0;
`endprotect
end
endtask


task PCI_confwr_dw;
  input [4:0]  id_select;
  input [2:0] func_num;
  input [7:0] addr;
  input [31:0] data;
`protect
begin

$fdisplay(f_name ,"PCI_CONFWR_DW: IDSEL=%h, ADDR=%h, data=%h",id_select, addr, data);

  cfgCycle              = 1;
  cmdIsWrite            = 1;
  startIdx              = sizeOfMemory;
  trxLen                = 1;
  curPciCmd             = `CONFIGURATION_WRITE;
  writeBuffer[startIdx] = {cbe_dw[addr[1:0]],irdyCntP,data};

  if (addr[1:0] != 2'b00) begin
    $fdisplay (f_name, "Error_T2_biu: %0t: %m: WARNING Address Not on DW Boundary. May be Error", $realtime );
    $display ("Error_T2_biu: %0t: %m: WARNING Address Not on DW Boundary. May be Error", $realtime );
  end
  if (cfg_type0) begin
      address            = {assertID(id_select), id_select,func_num, addr[7:2], 2'b00};
  end else begin
      address            = {16'h0, id_select, func_num, addr[7:2],2'b01};
  end

  doIt                 = 1;
  wait(~doIt);
  cfgCycle             = 0;
`endprotect
end
endtask


task PCI_memrd_b;
  input  [31:0] addr;
  output [7:0]  data;
`protect
begin

  cfgCycle             = 0;
  cmdIsWrite           = 0;
  startIdx             = sizeOfMemory;
  trxLen               = 1;
  curPciCmd            = `MEMORY_READ;
  readBuffer[startIdx] = {cbe_b[addr[1:0]],irdyCntP,32'h0};
  address              = {addr[31:2],2'b0};
  doIt                 = 1;
  wait(~doIt);
  tmpMemReg = readBuffer[startIdx];
  case (addr[1:0])
    2'b00: data = tmpMemReg[7:0];
    2'b01: data = tmpMemReg[15:8];
    2'b10: data = tmpMemReg[23:16];
    2'b11: data = tmpMemReg[31:24];
  endcase

  $fdisplay(f_name ,"PCI_MEMRD_B: ADDR=%h, data=%h",addr, data);

`endprotect
end
endtask



task PCI_memrd_w;
  input  [31:0] addr;
  output [15:0]  data;
`protect
begin

  cfgCycle             = 0;
  cmdIsWrite           = 0;
  startIdx             = sizeOfMemory;
  trxLen               = 1;
  curPciCmd            = `MEMORY_READ;
  readBuffer[startIdx] = {cbe_w[addr[1:0]],irdyCntP,32'h0};
  address              = {addr[31:2],2'b0};
  doIt                 = 1;
  wait(~doIt);
  tmpMemReg = readBuffer[startIdx];
  case (addr[1:0])
    2'b00: data = tmpMemReg[15:0];
    2'b01: data = tmpMemReg[23:8];
    2'b10: data = tmpMemReg[31:16];
    2'b11: data = tmpMemReg[31:16];
  endcase

  $fdisplay(f_name ,"PCI_MEMRD_W: ADDR=%h, data=%h",addr, data);
`endprotect
end
endtask


task PCI_memrd_dw;
  input  [31:0] addr;
  output [31:0]  data;
`protect
begin

  cfgCycle             = 0;
  cmdIsWrite           = 0;
  startIdx             = sizeOfMemory;
  trxLen               = 1;
  curPciCmd            = `MEMORY_READ;

  if (addr[1:0] != 2'b00) begin
    $fdisplay (f_name, "Error_T2_biu: %0t: %m: WARNING Address Not on DW Boundary. May be Error", $realtime );
    $display ("Error_T2_biu: %0t: %m: WARNING Address Not on DW Boundary. May be Error", $realtime );
  end
  readBuffer[startIdx] = {cbe_dw[addr[1:0]],irdyCntP,32'h0};
  address              = {addr[31:2],2'b0};
  doIt                 = 1;
  wait(~doIt);
  tmpMemReg = readBuffer[startIdx];
  data      = tmpMemReg[31:0];

  $fdisplay(f_name ,"PCI_MEMRD_DW: ADDR=%h, data=%h",addr, data);
`endprotect
end
endtask



task PCI_memrd_burst;
  input [31:0] addr;
  input        length;
  integer      length;
  integer      i;
`protect
begin

$fdisplay(f_name ,"PCI_MEMRD_BURST: ADDR=%h, length=%h",addr, length);

  cfgCycle              = 0;
  cmdIsWrite            = 0;
  startIdx              = 0;
  trxLen                = length;
  curPciCmd             = `MEMORY_READ ;
  address               = {addr[31:2],2'b0};
  $display("PCI_MEMRD_BURST: ADDR=%h, length=%h",addr, length);
  for (i=0; i<length; i=i+1)
        readBuffer[startIdx + i] = {cbe_dw[addr[1:0]],irdyCntP,32'h0};
  doIt                  = 1;
  wait(~doIt);
  for (i=0; i<length; i=i+1) begin
    tmpMemReg = readBuffer[startIdx + i];
    $fdisplay(f_name ,"PCI_MEMRD_BURST: ADDR=%h, data=%h",addr+i*4, tmpMemReg[31:0]);
  end
`endprotect
end
endtask



task PCI_confrd_burst;
  input [4:0] id_select;
  input [2:0] func_num;
  input [7:0] addr;
  input        length;
  integer      length;
`protect
begin

  cfgCycle              = 1;
  cmdIsWrite            = 0;
  startIdx              = 0;
  trxLen                = length;
  curPciCmd             = `CONFIGURATION_READ ;
  //address               = {addr[31:2],2'b0};

  $display("PCI_CONFRD_BURST: IDSEL=%h, FUNC=%h, ADDR=%h, length=%h",id_select, func_num, addr, length);
//   startIdx             = sizeOfMemory;
  for (i=0; i<length; i=i+1)
        readBuffer[startIdx + i] = {cbe_w[addr[1:0]],irdyCntP,32'h0};

  if (cfg_type0) begin
      address            = {assertID(id_select), id_select,func_num, addr[7:2], 2'b00};
  end else begin
      address            = {16'h0, id_select, func_num, addr[7:2],2'b01};
  end

  doIt                  = 1;
  wait(~doIt);
  for (i=0; i<length; i=i+1) begin
    tmpMemReg = readBuffer[startIdx + i];
    $fdisplay(f_name ,"PCI_CONFRD_BURST: ADDR=%h, data=%h",addr+i*4, tmpMemReg[31:0]);
  end
`endprotect
end
endtask




task PCI_memwr_b;
  input [31:0] addr;
  input [ 7:0] data;
`protect
begin

$fdisplay(f_name ,"PCI_MEMWR_B: ADDR=%h, DATA=%h",addr, data);

  cfgCycle             = 0;
  cmdIsWrite            = 1;
  startIdx              = sizeOfMemory;
  trxLen                = 1;
  curPciCmd             = `MEMORY_WRITE;
  writeBuffer[startIdx] = {cbe_b[addr[1:0]],irdyCntP,{data,data,data,data}};
  address               = {addr[31:2],2'b0};
  doIt                  = 1;
  wait(~doIt);
`endprotect
end
endtask



task PCI_memwr_b64;
  input [63:0] addr;
  input [ 7:0] data;
`protect
begin
  cfgCycle             = 0;
  cmdIsWrite            = 1;
  startIdx              = sizeOfMemory;
  trxLen                = 1;
  curPciCmd             = `MEMORY_WRITE;
  writeBuffer[startIdx] = {cbe_b[addr[1:0]],irdyCntP,{data,data,data,data}};
  address               = {addr[31:2],2'b0};
  doIt                  = 1;
  wait(~doIt);
`endprotect
end
endtask



task PCI_memwr_w;
  input [31:0] addr;
  input [15:0] data;
`protect
begin

$fdisplay(f_name ,"PCI_MEMWR_W: ADDR=%h, DATA=%h",addr, data);

  cfgCycle             = 0;
  cmdIsWrite            = 1;
  startIdx              = sizeOfMemory;
  trxLen                = 1;
  curPciCmd             = `MEMORY_WRITE;
  case ({addr[1], addr[0]})
    2'b00 : writeBuffer[startIdx] = {cbe_w[addr[1:0]],irdyCntP,
                                                  {16'h5a5a,data}};
    2'b01 : writeBuffer[startIdx] = {cbe_w[addr[1:0]],irdyCntP,
                                                {8'h5a,data,8'h5a}};
    2'b10 : writeBuffer[startIdx] = {cbe_w[addr[1:0]],irdyCntP,
                                                {data, 16'h5a5a }};
    2'b11: begin
            writeBuffer[startIdx] = {cbe_w[addr[1:0]],irdyCntP,
                                                {data, 16'h5a5a }};
            $fdisplay (f_name, "Error_T2_biu: %0t WARNING PCI_MEMWR_W TASK ........", $realtime );
            $fdisplay (f_name, "Error_T2_biu: Inavlid Alignment for Word address " );
            $fdisplay (f_name, "Error_T2_biu: ADDR  = %h ", addr, " data = %h ", data );
            $display ("Error_T2_biu: %0t WARNING PCI_MEMWR_W TASK ........", $realtime );
            $display ("Error_T2_biu: Inavlid Alignment for Word address " );
            $display ("Error_T2_biu: ADDR  = %h ", addr, " data = %h ", data );
           end
   endcase
  address               = {addr[31:2],2'b0};
  doIt                  = 1;
  wait(~doIt);
`endprotect
end
endtask


task PCI_memwr_dw;
  input [31:0] addr;
  input [31:0] data;
`protect
begin

$fdisplay(f_name ,"PCI_MEMWR_DW: ADDR=%h, DATA=%h",addr, data);

  cfgCycle             = 0;
  cmdIsWrite            = 1;
  startIdx              = sizeOfMemory;
  trxLen                = 1;
  curPciCmd             = `MEMORY_WRITE;

  if (addr[1:0] != 2'b00) begin
    $fdisplay (f_name, "Error_T2_biu: %0t: %m: WARNING Address Not on DW Boundary. May be Error", $realtime );
    $display ("Error_T2_biu: %0t: %m: WARNING Address Not on DW Boundary. May be Error", $realtime );
  end
  writeBuffer[startIdx] = {cbe_dw[addr[1:0]],irdyCntP,data};
  address               = {addr[31:2],2'b0};
  doIt                  = 1;
  wait(~doIt);
`endprotect
end
endtask


task PCI_memrd_mul;
  input [31:0] addr;
  input        sAddr, length;
  integer      sAddr, length;
`protect
begin

$fdisplay(f_name ,"PCI_MEMRD_MUL: ADDR=%h, sAddr=%h, length=%h",addr, sAddr, length);

  cfgCycle             = 0;
  cmdIsWrite            = 0;
  startIdx              = sAddr;
  trxLen                = length;
  curPciCmd             = `MEMORY_READ_MULTIPLE;
  address               = {addr[31:2],2'b0};
  doIt                  = 1;
  wait(~doIt);
  for (i=0; i<length; i=i+1) begin
    tmpMemReg = readBuffer[startIdx + i];
    $fdisplay(f_name ,"PCI_MEMRD_MUL: ADDR=%h, data=%h",addr+i*4, tmpMemReg[31:0]);
  end
`endprotect
end
endtask


task PCI_memrd_line;
  input [31:0] addr;
  input        sAddr, length;
  integer      sAddr, length;
`protect
begin

$fdisplay(f_name ,"PCI_MEMRD_LINE: ADDR=%h, sAddr=%h, length=%h",addr, sAddr, length);

  cfgCycle             = 0;
  cmdIsWrite            = 0;
  startIdx              = sAddr;
  trxLen                = length;
  curPciCmd             = `MEMORY_READ_LINE;
  address               = {addr[31:2],2'b0};
  doIt                  = 1;
  wait(~doIt);
  for (i=0; i<length; i=i+1) begin
    tmpMemReg = readBuffer[startIdx + i];
    $fdisplay(f_name ,"PCI_MEMRD_LINE: ADDR=%h, data=%h",addr+i*4, tmpMemReg[31:0]);
  end
`endprotect
end
endtask



task PCI_memwr_burst;
  input [31:0] addr;
  input        length;
  integer      length;
`protect
begin

$fdisplay(f_name ,"PCI_MEMWR_BURST: ADDR=%h, length=%h",addr, length);

  cfgCycle              = 0;
  cmdIsWrite            = 1;
  startIdx              = 0;
  trxLen                = length;
  curPciCmd             = `MEMORY_WRITE ;
  address               = {addr[31:2],2'b0};
  $display("PCI_MEMWR_BURST: ADDR=%h, length=%h",addr, length);
  doIt                  = 1;
  wait(~doIt);
`endprotect
end
endtask


task PCI_confwr_burst;
  input [4:0] id_select;
  input [2:0] func_num;
  input [7:0] addr;
  input        length;
  integer      length;
`protect
begin

$fdisplay(f_name ,"PCI_CONFWR_BURST: IDSEL=%h, FUNC=%h, ADDR=%h, LENGTH=%h",id_select, func_num, addr, length);
$display("PCI_CONFWR_BURST: IDSEL=%h, FUNC=%h, ADDR=%h, LENGTH=%h",id_select, func_num, addr, length);

  cfgCycle              = 1;
  cmdIsWrite            = 1;
  startIdx              = 0;
  trxLen                = length;
  curPciCmd             = `CONFIGURATION_WRITE ;
  //address               = {addr[31:2],2'b0};

  if (cfg_type0) begin
      address            = {assertID(id_select), id_select,func_num, addr[7:2], 2'b00};
  end else begin
      address            = {16'h0, id_select, func_num, addr[7:2],2'b01};
  end

  doIt                  = 1;
  wait(~doIt);
`endprotect
end
endtask



task PCI_memwr_inv;
  input [31:0] addr;
  input        sAddr, length;
  integer      sAddr, length;
`protect
begin

$fdisplay(f_name ,"PCI_MEMWR_INV: addr=%h, sAddr=%h, length=%h", addr, sAddr, length);

  cfgCycle             = 0;
  cmdIsWrite            = 1;
  startIdx              = sAddr;
  trxLen                = length;
  curPciCmd             = `MEMORY_WRITE_INVALIDATE;
  address               = {addr[31:2],2'b0};
  doIt                  = 1;
  wait(~doIt);
`endprotect
end
endtask


task PCI_iord_b;
  input  [31:0] addr;
  output [7:0]  data;
`protect
begin

  cfgCycle             = 0;
  cmdIsWrite           = 0;
  startIdx             = sizeOfMemory;
  trxLen               = 1;
  curPciCmd            = `IO_READ;
  readBuffer[startIdx] = {cbe_b[addr[1:0]],irdyCntP,32'h0};
  address              = addr;
  doIt                 = 1;
  wait(~doIt);
  tmpMemReg = readBuffer[startIdx];
  case (addr[1:0])
    2'b00: data = tmpMemReg[7:0];
    2'b01: data = tmpMemReg[15:8];
    2'b10: data = tmpMemReg[23:16];
    2'b11: data = tmpMemReg[31:24];
  endcase
  $fdisplay(f_name ,"PCI_IORD_B: addr=%h, data=%h", addr, data);

`endprotect
end
endtask


task PCI_iord_w;
  input  [31:0] addr;
  output [15:0] data;
`protect
begin

  cfgCycle             = 0;
  cmdIsWrite           = 0;
  startIdx             = sizeOfMemory;
  trxLen               = 1;
  curPciCmd            = `IO_READ;
  readBuffer[startIdx] = {cbe_w[addr[1:0]],irdyCntP,32'h0};
  address              = addr;

  if (address[1:0] == 2'b11)  begin
     $fdisplay (f_name, "Error_T2_biu: %0t %m WARNING Address Specified is 3, which crosses a DW Boundary", $realtime );
     $display ("Error_T2_biu: %0t %m WARNING Address Specified is 3, which crosses a DW Boundary", $realtime );
  end

  doIt                 = 1;
  wait(~doIt);
  tmpMemReg = readBuffer[startIdx];
  case (addr[1:0])
    2'b00: data = tmpMemReg[15:0];
    2'b01: data = tmpMemReg[23:8];
    2'b10: data = tmpMemReg[31:16];
    2'b11: data = tmpMemReg[31:16];
  endcase
  $fdisplay(f_name ,"PCI_IORD_W: addr=%h, data=%h", addr, data);

`endprotect
end
endtask


task PCI_iord_dw;
  input  [31:0] addr;
  output [31:0]  data;
`protect
begin

  cfgCycle             = 0;
  cmdIsWrite           = 0;
  startIdx             = sizeOfMemory;
  trxLen               = 1;
  curPciCmd            = `IO_READ;
  readBuffer[startIdx] = {cbe_dw[addr[1:0]],irdyCntP,32'h0};
  address              = addr;

  if (address[1:0] != 2'b00) begin
   $fdisplay (f_name, "Error_T2_biu: %0t %m WARNING Starting Address is not 0, which crosses a DW Boundary", $realtime);
   $display ("Error_T2_biu: %0t %m WARNING Starting Address is not 0, which crosses a DW Boundary", $realtime);
  end
  doIt                 = 1;
  wait(~doIt);
  tmpMemReg = readBuffer[startIdx];
  data      = tmpMemReg[31:0];
  $fdisplay(f_name ,"PCI_IORD_DW: addr=%h, data=%h", addr, data);

`endprotect
end
endtask



task PCI_iord_burst;
  input [31:0] addr;
  input        length;
  integer      length;
`protect
begin

$fdisplay(f_name ,"PCI_IORD_BURST: addr=%h, length=%h", addr, length);

  cfgCycle              = 0;
  cmdIsWrite            = 0;
  startIdx              = 0;
  trxLen                = length;
  curPciCmd             = `IO_READ;
  address               = addr ;
  doIt                  = 1;
  wait(~doIt);
  for (i=0; i<length; i=i+1) begin
    tmpMemReg = readBuffer[startIdx + i];
    $fdisplay(f_name ,"PCI_IORD_BURST: ADDR=%h, data=%h",addr+i*4, tmpMemReg[31:0]);
  end
`endprotect
end
endtask



task PCI_iowr_b;
  input [31:0] addr;
  input [ 7:0] data;
`protect
begin

$fdisplay(f_name ,"PCI_IOWR_B: addr=%h, data=%h", addr, data);

  cfgCycle              = 0;
  cmdIsWrite            = 1;
  startIdx              = sizeOfMemory;
  trxLen                = 1;
  curPciCmd             = `IO_WRITE;
  writeBuffer[startIdx] = {cbe_b[addr[1:0]],irdyCntP,{data,data,data,data}};
  address               = addr;
  doIt                  = 1;
  wait(~doIt);
`endprotect
end
endtask


task PCI_iowr_w;
  input [31:0] addr;
  input [15:0] data;
`protect
begin

$fdisplay(f_name ,"PCI_IOWR_W: addr=%h, data=%h", addr, data);

  cfgCycle              = 0;
  cmdIsWrite            = 1;
  startIdx              = sizeOfMemory;
  trxLen                = 1;
  curPciCmd             = `IO_WRITE;
  case ({addr[1], addr[0]})
    2'b00 : writeBuffer[startIdx] = {cbe_w[addr[1:0]],irdyCntP,
                                                  {16'h5a5a,data}};
    2'b01 : writeBuffer[startIdx] = {cbe_w[addr[1:0]],irdyCntP,
                                                {8'h5a,data,8'h5a}};
    2'b10 : writeBuffer[startIdx] = {cbe_w[addr[1:0]],irdyCntP,
                                                {data, 16'h5a5a }};
    2'b11: begin
            writeBuffer[startIdx] = {cbe_w[addr[1:0]],irdyCntP,
                                                {data, 16'h5a5a }};
            $fdisplay (f_name, "Error_T2_biu: %0t WARNING PCI_IOWR_W TASK ........", $realtime );
            $fdisplay (f_name, "Error_T2_biu: Inavlid Alignment for Word address " );
            $fdisplay (f_name, "Error_T2_biu: ADDR  = %h ", addr, " data = %h ", data );
            $display ("Error_T2_biu: %0t WARNING PCI_IOWR_W TASK ........", $realtime );
            $display ("Error_T2_biu: Inavlid Alignment for Word address " );
            $display ("Error_T2_biu: ADDR  = %h ", addr, " data = %h ", data );
           end
   endcase
  address               = addr;
  doIt                  = 1;
  wait(~doIt);
`endprotect
end
endtask


task PCI_iowr_dw;
  input [31:0] addr;
  input [31:0] data;
`protect
begin

$fdisplay(f_name ,"PCI_IOWR_DW: addr=%h, data=%h", addr, data);

  cfgCycle              = 0;
  cmdIsWrite            = 1;
  startIdx              = sizeOfMemory;
  trxLen                = 1;
  curPciCmd             = `IO_WRITE;
  writeBuffer[startIdx] = {cbe_dw[addr[1:0]],irdyCntP,data};
  address               = addr;

  if (address[1:0] != 2'b00) begin
   $fdisplay (f_name, "Error_T2_biu: %0t %m WARNING Starting Address is not 0, which crosses a DW Boundary", $realtime);
   $fdisplay (f_name, "Error_T2_biu: Address is = %h ", address );

   $display ("Error_T2_biu: %0t %m WARNING Starting Address is not 0, which crosses a DW Boundary", $realtime);
   $display ("Error_T2_biu: Address is = %h ", address );
  end

  doIt                  = 1;
  wait(~doIt);
`endprotect
end
endtask



task PCI_iowr_burst;
  input [31:0] addr;
  input        length;
  integer      length;
`protect
begin

$fdisplay(f_name ,"PCI_IOWR_BURST: addr=%h, length=%h", addr, length);

  cfgCycle              = 0;
  cmdIsWrite            = 1;
  startIdx              = 0;
  trxLen                = length;
  curPciCmd             = `IO_WRITE;
  address               = addr ;
  doIt                  = 1;
  wait(~doIt);
`endprotect
end
endtask            // end of task PCI_iowr_burst;

task check_lock ;

begin
`protect

   if (req_lock && ~locko_)
     begin
    if (got_lock)
      begin
               lock_obtained = 1 ;
            req_lock   = 0 ;
    end else
          begin
            $fdisplay (f_name, "Error_T2_biu: %0t %m Lock_ Request failed with Target Abort ", $realtime );
            $display ("Error_T2_biu: %0t %m Lock_ Request failed with Target Abort ", $realtime );
            req_lock   = 0 ;
            lockEnable = 0 ;
        lock_obtained = 0 ;
    end
   end

  got_lock = 0 ;
`endprotect
end
endtask        // end of check_lock ;

`protect

/****
    When Lock Release is requested we Immediately release the lock
*****/

always @(posedge release_lock) begin

   locko_ = 1'b1 ;
   lock_obtained = 0 ;
   lockEnable    = 0 ;

end        // release lock

`endprotect

wire     mem_op = (curPciCmd == `MEMORY_READ) ||
         (curPciCmd == `MEMORY_WRITE) ||
               (curPciCmd == `MEMORY_READ_MULTIPLE) ||
               (curPciCmd == `MEMORY_READ_LINE)     ||
               (curPciCmd == `MEMORY_WRITE_INVALIDATE) ;

`protect

always begin: Dispatch
  wait (doIt);
  Loopit   = 1 ;
  trxCount = 0;
  busop_fail = 0 ;
  Master_abort = 0 ;
  Disc_With_Data = 0 ;
  Target_abort = 0 ;
  Target_retry = 0 ;

  rcvd_masterAbort = 0 ;
  rcvd_targetAbort = 0 ;

  while (Loopit) begin

    busInterface ;    // Cal the bus interface task.

    if (busop_fail) begin
      if (Master_abort ) begin
        $fdisplay (f_name, "Error_T2_biu: MASTER: WARNING Master ABORT condition encountered ");
        $fdisplay (f_name, "Error_T2_biu:  The operation will not be Re_Tried by the Bus Master ...");
        $display ("Error_T2_biu: %0t %m: WARNING Master ABORT condition encountered ", $realtime );
        $display ("Error_T2_biu:  The operation will not be Re_Tried by the Bus Master ...");
        Loopit = 0 ;
      end // Operation cannot be retried.
      else if (Target_abort) begin
        $fdisplay (f_name, "Error_T2_biu: MASTER: WARNING Master ABORT or Target ABort condition encountered ");
        $fdisplay (f_name, "Error_T2_biu:  The operation will not be Re_Tried by the Bus Master ...");
        $display ("Error_T2_biu: %0t %m: WARNING Master ABORT or Target ABort condition encountered ", $realtime );
        $display ("Error_T2_biu:  The operation will not be Re_Tried by the Bus Master ...");
        Loopit = 0 ;
      end // Operation cannot be retried.

      if (~retry_busop) begin
        repeat (2) @(posedge clock);    // wait for 3 cycles
        Loopit = 0 ;
      end         // no retry is necessary


      if ( Target_retry && retry_busop) begin

         $display ("MASTER: WARNING Target Retry condition encountered ");
         $fdisplay (f_name, "MASTER: WARNING Target Retry condition encountered ");

         if (curPciCmd == `MEMORY_WRITE_INVALIDATE)
            curPciCmd = `MEMORY_WRITE ;

         if ( (curPciCmd == `MEMORY_READ_LINE) ||
            (curPciCmd == `MEMORY_READ_MULTIPLE) )
            curPciCmd = `MEMORY_READ ;

         if (trxCount == trxLen)
            Loopit = 0 ;

         repeat (2) @(posedge clock);    // wait for 3 cycles
      end         // Target Retry, Busop will be re_issued.
      else
      if ( Disc_With_Data && retry_busop) begin

         $display ("MASTER: WARNING Target Disc_with_data condition encountered ");
         $fdisplay (f_name, "MASTER: WARNING Target Disc_with_data condition encountered ");

         if (curPciCmd == `MEMORY_WRITE_INVALIDATE)
            curPciCmd = `MEMORY_WRITE ;

         if ( (curPciCmd == `MEMORY_READ_LINE) ||
            (curPciCmd == `MEMORY_READ_MULTIPLE) )
            curPciCmd = `MEMORY_READ ;

         if (trxCount == trxLen) Loopit = 0 ;

         repeat (2) @(posedge clock);    // wait for 3 cycles
      end         // Target Retry, Busop will be re_issued.

    end else    // An error was encountered by the model.
    Loopit = 0 ;

  end         // Loop on a task, in case retry or disconnect is forced.

  doIt = 0 ;

end     // The main loop

`endprotect
/****
 **** Main Loop
 ****
 ****/



task busInterface ;

begin        // Start PCI Bus interface Task.
`protect
  busop_fail = 0 ;
  getTheBus;
  if (curPciCmd [0])
    prevCmdRead = 0 ;
  else
    prevCmdRead = 1 ;

/////////////////////////////////////////
  fork
    begin
      #0.01 frameo_ = 1'b0;
      if ( (req_lock || lock_obtained) && mem_op) begin
        locko_ = 1'b1 ;
        lockEnable = 1 ;
      end
      ado     = address;
      irdyo_    = 1'b1;
      mas_adEnable  = 1;
      mas_cbeEnable = 1;
      mas_ctlEnable     = 1;
    end

    begin
      #0.01 cbeo_   = curPciCmd;
      cmdCycle = 1 ;
    end

  join
/////////////////////////////////////////

  if (cfgCycle) begin
    idEnable = 1;
  end

  @(posedge clock);
  #0.01 cmdCycle = 0 ;

/////////////////////////////////////////////
  fork
    #0.01 ado = `default_ado ;
    #0.01 cbeo_ = 4'b1010 ;
  join
/////////////////////////////////////////////

  if ( (req_lock  || lock_obtained ) && mem_op )
    locko_ = 1'b0 ;
  else begin
    if (req_lock) begin
       lockEnable = 0 ;
       locko_     = 1'b1 ;
    end
  end

  if (cfgCycle) begin
    idEnable = 0;
  end

  if (~cmdIsWrite) begin
    mas_adEnable = 0;
  end

  contTrx  = 1;

  while (contTrx) begin
    if (cmdIsWrite) begin
      tmpMemReg = writeBuffer[startIdx + trxCount];
    end else begin
      tmpMemReg = readBuffer[startIdx + trxCount];
    end

    curCbe_   = tmpMemReg[43:40];
    irdyCnt   = tmpMemReg[39:32];
	// for detect some bug // KD990622
    pciData   = tmpMemReg[31:0] | {{8{curCbe_[3]}},{8{curCbe_[2]}},{8{curCbe_[1]}},{8{curCbe_[0]}}};

    if (irdyCnt > 0) #0.01 irdyo_ = 1'b1;
    if ((trxCount + 1) == trxLen) begin
      reqo_   = 1'b1;
    end

    fork

/////////////////////////////////////////////////
      begin:genCbe
        #0.01 cbeo_ = curCbe_;
      end

/////////////////////////////////////////////////
      begin:targetAbortCheck
        #0.01;
        wait(~stopi_);

          disable genCbe;

          $fdisplay(f_name, "MASTER: WARNING!! TARGET termination!! STOP_ signal is active");
          $display("%0t %m: WARNING!! TARGET termination!! STOP_ signal is active", $realtime);

          if (~trdyi_) begin  //Transfer with data
            wait(~irdy_ && ~trdy_);

            while ( irdy_ ||  trdy_) @(posedge clock);
            if (devsel_) begin
              $fdisplay(f_name, "Error_T2_biu: MASTER: ERROR!!  TARGET Asserted TRDY_ without DEVSEL_ active");
              $display("Error_T2_biu: %m: ERROR!!  TARGET Asserted TRDY_ without DEVSEL_ active");
            end

            disable dataTrx;
            disable wait4Trdy;
            disable genIrdy;
            disable wait4Devsel;

            if (!frameo_) #0.01 frameo_ = 1'b1;
            else #0.01 irdyo_ = 1'b1;
            @(posedge clock);
            if (~cmdIsWrite) begin
              tmpMemReg[31:0] = adi;
              readBuffer[startIdx + trxCount] = tmpMemReg;
            end

            trxCount = trxCount + 1;
            address = address + 4;

//          checking if Lock was requested
            got_lock = 0 ;
            busop_fail = 1 ;
            Disc_With_Data = 1 ;
            check_lock ;
            wait(trdy_);
            contTrx = 0;
          end else begin  // disconnect without transfer data
//          checking if Lock was requested

//if (curPciCmd==`IO_WRITE)
//$display("In targetAbortCheck transfer without data, %m: %0t, trxCount = %h, ado=%h", $realtime, trxCount, ado);
            got_lock = 0 ;
            if (devsel_) begin
              rcvd_targetAbort = 1 ;
              Target_abort = 1 ;
            end else
              Target_retry = 1 ;

            busop_fail = 1 ;
            check_lock ;
            disable wait4Trdy;
            disable genIrdy;
            disable dataTrx;
            disable wait4Devsel;
            if (~frameo_) begin

              fork
                #0.01 frameo_ = 1'b1;
                #0.01      irdyo_ = 1'b0;
              join

              @(posedge clock);
            end
            #0.01 irdyo_ = 1'b1;
            contTrx = 0;
          end

          reqo_    = 1'b1 ;
          mas_adEnable = 0 ;
          mas_ctlEnable = 0 ;
          perrEnable = 0 ;
          lockEnable = 0 ;
          idEnable = 0 ;
      end //targetAbortCheck

/////////////////////////////////////////////////
      begin:wait4Trdy
    #0.01;
        repeat (trdyWait) begin
          @(posedge clock);
          if (~trdy_) begin
            disable wait4Trdy;
          end
        end

        $display("Error_T2_biu: %m: WARNING!! no TRDY_ is received after %h",trdyWait);
        $fdisplay(f_name, "Error_T2_biu: MASTER: WARNING!! no TRDY_ is received after 4%h",trdyWait);

        disable genCbe;
        disable targetAbortCheck;
        disable dataTrx;
        disable wait4Devsel;

//        checking if Lock was requested

        reqo_ = 1'b1 ;
        got_lock = 0 ;
        busop_fail = 1 ;
        check_lock ;

        if (~frameo_) begin
          #0.01 frameo_ = 1'b1;
          @(posedge clock);
        end

        if (~irdyi_) begin
          disable genIrdy;
        end

        @(posedge clock);
        contTrx = 0;
      end //wait4Trdy

      begin:wait4Devsel
        #0.01;
        repeat (devselWait) begin
          @(posedge clock);
          if (~devseli_) disable wait4Devsel;
        end

        $fdisplay(f_name, "Error_T2_biu: MASTER: WARNING! no DEVSEL_ is received after %h clocks",devselWait + 2);
        $display("Error_T2_biu: %0t %m: WARNING! no DEVSEL_ is received after %h clocks",$realtime,devselWait + 2);
        $display("Error_T2_biu: Command = %h ", curPciCmd, " Address = %h", address );

        rcvd_masterAbort = 1 ;
        Master_abort = 1 ;
        disable wait4Trdy;
        disable genCbe;
        disable targetAbortCheck;
        disable dataTrx;
        fork
          reqo_ = 1'b1 ;
            if (irdyi_) begin
                 disable genIrdy;
                 #0.01 irdyo_ = 1'b0;
                 @(posedge clock);
            end
            #0.01 frameo_ = 1'b1;
        join

//        checking if Lock was requested

        got_lock = 0 ;
        busop_fail = 1 ;
        check_lock ;
        reqo_ = 1'b1 ;
        @(posedge clock);
        mas_adEnable = 0 ;
        mas_ctlEnable = 0 ;
        perrEnable = 0 ;
        lockEnable = 0 ;
        idEnable = 0 ;
        contTrx = 0;
      end //wait4Devsel

      begin:dataTrx
        #0.01;
        if (cmdIsWrite) begin
          #0.01 ado = pciData;
          @(posedge clock);
          #0.01 wait (~irdyi_ && ~trdyi_ && stop_ );
          while (irdyi_ && trdyi_ && stop_) @(posedge clock);

          fork
            #0.01  ado = `default_ado  ;
            #0.01 cbeo_ = 4'b1010 ;
            trxCount = trxCount + 1;
          join

          if (trxCount == trxLen) contTrx = 0;
        end else begin
          @(posedge clock);
          #0.01 wait (~irdyi_ && ~trdyi_ && stop_);
          while (irdyi_ && trdyi_ && stop_) @(posedge clock);
          tmpMemReg[31:0] = adi;
//$display("get one data, read buffer = %h ", readBuffer[startIdx + trxCount], " pci data = %h", tmpMemReg, " trxCount = %d", trxCount );
          readBuffer[startIdx + trxCount] = tmpMemReg;
          trxCount = trxCount + 1;
          #0.01 cbeo_ = 4'b1010 ;
          if (trxCount == trxLen) contTrx = 0;
        end

        address = address + 4 ;
//        wait (stop_);

//        checking if Lock was requested

        got_lock = 1 ;
        check_lock ;
        disable targetAbortCheck;
        disable wait4Trdy;
        disable wait4Devsel;
      end //dataTrx

///////////////////////
      begin:genIrdy
        #0.01;
        repeat (irdyCnt) @(posedge clock);
        fork
            #0.01 irdyo_ = 1'b0;
            if ((trxCount + 1) == trxLen) #0.01 frameo_ = 1'b1;
        join
      end
    join
  end  // while transfer

  #0.01 irdyo_ = 1'b1;

  reqo_    = 1'b1 ;
  mas_adEnable = 0 ;
  mas_cbeEnable = 0 ;
  idEnable = 0 ;
  mas_ctlEnable = 0 ;

`endprotect

end
endtask

//`include "mast_conf.v"

endmodule
