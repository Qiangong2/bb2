//===============================================================

module	dimm_bh(dq,
		addr,
		ba,
		cs_,
		d1_cke,
		d2_cke,
		dqm,
		ras_,
		cas_,
		we_,
		sd_clk0,
		sd_clk1
		);

inout	[31:0]	dq;
input	[11:0]	addr;
input	[1:0]	ba;
input	[1:0]	cs_;
input	d1_cke, d2_cke;
input	ras_, cas_, we_, sd_clk0, sd_clk1;
input	[3:0]	dqm;

integer	dimm_rpt;

`ifdef OPEN_DIMM_RPT
	initial	dimm_rpt = $fopen("dimm_bh.rpt");
`else
`endif

// check SDRAM command
wire	[3:0]	sdram_cmd = {ras_, cas_, we_, addr[10]};

    parameter          Read         = 4'b1010;               // Command_state
    parameter          Read_A       = 4'b1011;
    parameter          Write        = 4'b1000;
    parameter          Write_A      = 4'b1001;
    parameter          Nop          = 4'b111x;
    parameter          Precharge    = 4'b0100;
    parameter          Pre_all      = 4'b0101;
    parameter          Active       = 4'b011x;
    parameter          Mode_Reg_Set = 4'b0000;
    parameter          Auto_Ref     = 4'b001x;
//    parameter          Self_Ref     = 4'b001x;

task	sdram_cmd_msg;
input	[1:0]	index;
begin

`ifdef OPEN_DIMM_RPT
casex	(sdram_cmd)
	Read:		$fdisplay(dimm_rpt, "SDRAM %d received Read	######	bank %b, \tcol= %h", index, ba, addr[9:0]);
	Read_A:		$fdisplay(dimm_rpt, "SDRAM %d received Read_AP	######	bank %b, \tcol= %h", index, ba, addr[9:0]);
	Write:		$fdisplay(dimm_rpt, "SDRAM %d received Write	######	bank %b, \tcol= %h", index, ba, addr[9:0]);
	Write_A:	$fdisplay(dimm_rpt, "SDRAM %d received Write_AP	######	bank %b, \tcol= %h", index, ba, addr[9:0]);
	Nop:		$fdisplay(dimm_rpt, "SDRAM %d received NOP	######	bank %b", index, ba);
	Precharge:	$fdisplay(dimm_rpt, "SDRAM %d received Prech	######	bank %b", index, ba);
	Active:		$fdisplay(dimm_rpt, "SDRAM %d received RowAct	######	bank %b, row= %h", index, ba, addr);
	Auto_Ref:	$fdisplay(dimm_rpt, "SDRAM %d received CBR	######	", index);
endcase
`else
`endif

end
endtask

always	@(posedge sd_clk0)
	if (!cs_[0])	begin
		sdram_cmd_msg(0);
	end

always	@(posedge sd_clk1)
	if (!cs_[1])	begin
		sdram_cmd_msg(1);
	end
/*
always	@(posedge sd_clk2)
	if (!cs_[2])	begin
		sdram_cmd_msg(2);
	end

always	@(posedge sd_clk1)
	if (!cs_[3])	begin
		sdram_cmd_msg(3);
	end
*/

//	---------------------------------------------------------------------------------
reg	pall_en,mrs_en;
reg	pall_mrs_en;
reg	bk0_pre,bk1_pre,bk2_pre,bk3_pre;
reg	bk0_ras,bk1_ras,bk2_ras,bk3_ras;
reg	bk0_cas,bk1_cas,bk2_cas,bk3_cas;

wire	bk0_sel,bk1_sel,bk2_sel,bk3_sel;

initial	begin
	pall_en	=	0;	mrs_en	=	0;	pall_mrs_en	=	0;
	bk0_pre	=	0;	bk1_pre	=	0; bk2_pre	=	0; bk3_pre	=	0;
	bk0_ras	=	0;	bk1_ras	=	0; bk2_ras	=	0; bk3_ras	=	0;
	bk0_cas	=	0;	bk1_cas	=	0; bk2_cas	=	0; bk3_cas	=	0;
end

always	@(posedge sd_clk0)
	if(sdram_cmd == Pre_all)begin
		pall_en	=	1;
	end

always	@(posedge sd_clk0)
	if(pall_en && (sdram_cmd == Mode_Reg_Set))begin
		mrs_en	=	1;
	end

always	@(pall_en or mrs_en)begin
	pall_mrs_en	=	pall_en && mrs_en;
end

//	----------------------------------------------------
always	@(posedge sd_clk0)
	if(!pall_mrs_en)begin
		bk0_pre	=	0;
		bk1_pre	=	0;
		bk2_pre	=	0;
		bk3_pre	=	0;
	end
	else	begin
		casex(sdram_cmd)
			Precharge:	begin
				bk0_pre	=	bk0_sel;
				bk1_pre	=	bk1_sel;
				bk2_pre	=	bk2_sel;
				bk3_pre	=	bk3_sel;
			end
			Pre_all:	begin
				bk0_pre	=	1;
				bk1_pre	=	1;
				bk2_pre	=	1;
				bk3_pre	=	1;
			end
			default:	begin
				bk0_pre	=	0;
				bk1_pre	=	0;
				bk2_pre	=	0;
				bk3_pre	=	0;
			end
		endcase
	end

//	-----------------------------------------------------------------------------
always	@(posedge sd_clk0)
	if(!pall_mrs_en)begin
		bk0_ras	=	0;
		bk1_ras	=	0;
		bk2_ras	=	0;
		bk3_ras	=	0;
	end
	else	begin
		casex(sdram_cmd)
			Active:	begin
				bk0_ras	=	bk0_sel;
				bk1_ras	=	bk1_sel;
				bk2_ras	=	bk2_sel;
				bk3_ras	=	bk3_sel;
			end
			default:	begin
				bk0_ras	=	0;
				bk1_ras	=	0;
				bk2_ras	=	0;
				bk3_ras	=	0;
			end
		endcase
	end

//	-----------------------------------------
always	@(posedge sd_clk0)
	if(!pall_mrs_en)begin
		bk0_cas	=	0;
		bk1_cas	=	0;
		bk2_cas	=	0;
		bk3_cas	=	0;
	end
	else	begin
		casex(sdram_cmd)
			Read:	begin
				bk0_cas	=	bk0_sel;
				bk1_cas	=	bk1_sel;
				bk2_cas	=	bk2_sel;
				bk3_cas	=	bk3_sel;
			end
			Read_A:	begin
				bk0_cas	=	bk0_sel;
				bk1_cas	=	bk1_sel;
				bk2_cas	=	bk2_sel;
				bk3_cas	=	bk3_sel;
			end
			Write:	begin
				bk0_cas	=	bk0_sel;
				bk1_cas	=	bk1_sel;
				bk2_cas	=	bk2_sel;
				bk3_cas	=	bk3_sel;
			end
			Write_A:	begin
				bk0_cas	=	bk0_sel;
				bk1_cas	=	bk1_sel;
				bk2_cas	=	bk2_sel;
				bk3_cas	=	bk3_sel;
			end
			default:	begin
				bk0_cas	=	0;
				bk1_cas	=	0;
				bk2_cas	=	0;
				bk3_cas	=	0;
			end
		endcase
	end

//	-----------------------------------------------------------------------------	
assign	bk0_sel	=	ba == 2'b00;
assign	bk1_sel	=	ba == 2'b01;
assign	bk2_sel	=	ba == 2'b10;
assign	bk3_sel	=	ba == 2'b11;

//	---------------------------------------------------------------------------------


//	---------	define DIMM0	-------------
`ifdef	sdram_d0_16m_4b
dimm_16m_4b	dimm0(
		.dq		(dq			),			
		.addr	(addr		),
		.ba		(ba			),
		.cs_	(cs_[0]		),
		.dqm	(dqm		),
		.cke	(d1_cke		),
		.ras_	(ras_		),
		.cas_	(cas_		),
		.we_	(we_		),
		.sd_clk	(sd_clk0	)
	);
	
`else
`ifdef	sdram_d0_8m_4b
dimm_8m_4b	dimm0(
		.dq		(dq			),			
		.addr	(addr		),
		.ba		(ba			),
		.cs_	(cs_[0]		),
		.dqm	(dqm		),
		.cke	(d1_cke		),
		.ras_	(ras_		),
		.cas_	(cas_		),
		.we_	(we_		),
		.sd_clk	(sd_clk0	)
	);

`else
`ifdef	sdram_d0_4m_4b

dimm_4m_4b dimm0(
		.dq	(dq		),
		.addr	(addr		),
		.dqm	(dqm		),
		.cke	(d1_cke		),
		.ras_	(ras_		),
		.cas_	(cas_		),
		.we_	(we_		),
		.ba	(ba		),
		.cs_	(cs_[0]		),
		.sd_clk	(sd_clk0	)
		);
`else
// `ifdef	sdram_d0_2m_2b // default

dimm_2m_2b dimm0(
		.dq	(dq		),
		.addr	(addr[10:0]	),
		.dqm	(dqm		),
		.cke	(d1_cke		),
		.ras_	(ras_		),
		.cas_	(cas_		),
		.we_	(we_		),
		.ba	(ba		),
		.cs_	(cs_[0]		),
		.sd_clk	(sd_clk0	)
		);

`endif
`endif
`endif

//	---------	define DIMM1	-------------
`ifdef	sdram_d1_16m_4b
dimm_16m_4b	dimm1(
		.dq		(dq			),			
		.addr	(addr		),
		.ba		(ba			),
		.cs_	(cs_[1]		),
		.dqm	(dqm		),
		.cke	(d2_cke		),
		.ras_	(ras_		),
		.cas_	(cas_		),
		.we_	(we_		),
		.sd_clk	(sd_clk0	)
	);
`else
`ifdef	sdram_d1_8m_4b
dimm_8m_4b	dimm1(
		.dq		(dq			),			
		.addr	(addr		),
		.ba		(ba			),
		.cs_	(cs_[1]		),
		.dqm	(dqm		),
		.cke	(d2_cke		),
		.ras_	(ras_		),
		.cas_	(cas_		),
		.we_	(we_		),
		.sd_clk	(sd_clk0	)
	);

`else
`ifdef	sdram_d1_4m_4b
dimm_4m_4b dimm1(
		.dq	(dq		),
		.addr	(addr		),
		.dqm	(dqm		),
		.cke	(d2_cke		),
		.ras_	(ras_		),
		.cas_	(cas_		),
		.we_	(we_		),
		.ba	(ba		),
		.cs_	(cs_[1]		),
		.sd_clk	(sd_clk1	)
		);
`else
`ifdef	sdram_d1_2m_2b

dimm_2m_2b dimm1(
		.dq	(dq		),
		.addr	(addr[10:0]	),
		.dqm	(dqm		),
		.cke	(d2_cke		),
		.ras_	(ras_		),
		.cas_	(cas_		),
		.we_	(we_		),
		.ba	(ba		),
		.cs_	(cs_[1]		),
		.sd_clk	(sd_clk1	)
		);

`endif
`endif
`endif
`endif
endmodule

/******************************************
* File Name:	dimm_4m.v

* Description:	SDRAM DIMM non-parity, caparity 4 MB
		Address Width: 11 bits
*
********************************************/

module dimm_4m_4b(
	// inout
	dq,			// data in/out
	//input
	addr,
	ba,
	cs_,
	dqm,
	cke,
	ras_,
	cas_,
	we_,
	// SPD pins
/*	scl,			// SPD clock
	sda,			// SPD data in/out
	saddr,			// slave address
*/
	sd_clk			// SDRAM clock
		);

inout	[31:0]	dq;
input	[11:0]	addr;
input	[3:0]	dqm;
input	[1:0]	ba;
input		cke, cs_, ras_, cas_, we_, sd_clk;
/*
inout		sda;
input		scl;
input	[2:0]	saddr;
*/
wire	[31:0]	dq;

mt48lc4m16a2 sdram0(
	.Addr		(addr		),
	.We_n		(we_		),
	.Cas_n		(cas_		),
	.Ras_n		(ras_		),
	.Clk		(sd_clk		),
	.Cke		(cke		),
	.Cs_n		(cs_		),
	.Dqm		(dqm[1:0]	),
	.Ba		(ba		),
	// inout
	.Dq		(dq[15:0]	)
	);

mt48lc4m16a2 sdram1(
	.Addr		(addr		),
	.We_n		(we_		),
	.Cas_n		(cas_		),
	.Ras_n		(ras_		),
	.Clk		(sd_clk		),
	.Cke		(cke		),
	.Cs_n		(cs_		),
	.Dqm		(dqm[3:2]	),
	.Ba		(ba		),
	// inout
	.Dq		(dq[31:16]	)
	);

endmodule

/********************************************
* File Name:	dimm_2m.v

* Description:	SDRAM DIMM non-parity, caparity 4 MB
		Address Width: 11 bits
*
********************************************/

module dimm_2m_2b(
	// inout
	dq,			// data in/out
	//input
	addr,
	ba,
	cs_,
	dqm,
	cke,
	ras_,
	cas_,
	we_,
	// SPD pins
/*	scl,			// SPD clock
	sda,			// SPD data in/out
	saddr,			// slave address
*/
	sd_clk			// SDRAM clock
		);

inout	[31:0]	dq;
input	[10:0]	addr;
input	[3:0]	dqm;
input	[1:0]	ba;
input		cke, cs_, ras_, cas_, we_, sd_clk;
/*
inout		sda;
input		scl;
input	[2:0]	saddr;
*/
wire	[31:0]	dq;

mt48lc1m16a1 sdram0(
	.Add		(addr		),
	.We_n		(we_		),
	.Cas_n		(cas_		),
	.Ras_n		(ras_		),
	.Clk		(sd_clk		),
	.Cke		(cke		),
	.Cs_n		(cs_		),
	.Dqm		(dqm[1:0]	),
	.Ba		(ba[0]		),
	// inout
	.Dq		(dq[15:0]	)
	);

mt48lc1m16a1 sdram1(
	.Add		(addr		),
	.We_n		(we_		),
	.Cas_n		(cas_		),
	.Ras_n		(ras_		),
	.Clk		(sd_clk		),
	.Cke		(cke		),
	.Cs_n		(cs_		),
	.Dqm		(dqm[3:2]	),
	.Ba		(ba[0]		),
	// inout
	.Dq		(dq[31:16]	)
	);

endmodule

//	----------------
/******************************************
* File Name:	dimm_8m.v

* Description:	SDRAM DIMM non-parity, caparity 8x4 MB
		Address Width: 12 bits
*
********************************************/

module dimm_8m_4b(
	dq,			
	addr,
	ba,
	cs_,
	dqm,
	cke,
	ras_,
	cas_,
	we_,
	sd_clk
	);

inout	[31:0]	dq;
input	[11:0]	addr;
input	[3:0]	dqm;
input	[1:0]	ba;
input		cke, cs_, ras_, cas_, we_, sd_clk;
wire	[31:0]	dq;

mt48lc8m8a2	sdram0(
	.Dq			(dq[7:0]	),
	.Addr		(addr		),
	.Ba			(ba			),
	.Clk		(sd_clk		),
	.Cke		(cke		),
	.Cs_n		(cs_		),
	.Ras_n		(ras_		),
	.Cas_n		(cas_		),
	.We_n		(we_		),
	.Dqm		(dqm[0]		)
	);

mt48lc8m8a2	sdram1(
	.Dq			(dq[15:8]	),
	.Addr		(addr		),
	.Ba			(ba			),
	.Clk		(sd_clk		),
	.Cke		(cke		),
	.Cs_n		(cs_		),
	.Ras_n		(ras_		),
	.Cas_n		(cas_		),
	.We_n		(we_		),
	.Dqm		(dqm[1]		)
	);

mt48lc8m8a2	sdram2(
	.Dq			(dq[23:16]	),
	.Addr		(addr		),
	.Ba			(ba			),
	.Clk		(sd_clk		),
	.Cke		(cke		),
	.Cs_n		(cs_		),
	.Ras_n		(ras_		),
	.Cas_n		(cas_		),
	.We_n		(we_		),
	.Dqm		(dqm[2]		)
	);

mt48lc8m8a2	sdram3(
	.Dq			(dq[31:24]	),
	.Addr		(addr		),
	.Ba			(ba			),
	.Clk		(sd_clk		),
	.Cke		(cke		),
	.Cs_n		(cs_		),
	.Ras_n		(ras_		),
	.Cas_n		(cas_		),
	.We_n		(we_		),
	.Dqm		(dqm[3]		)
	);

endmodule

//	------------------------------------

//	----------------
/******************************************
* File Name:	dimm_8m.v

* Description:	SDRAM DIMM non-parity, caparity 8x4 MB
		Address Width: 12 bits, 10bits
*
********************************************/

module dimm_16m_4b(
	dq,			
	addr,
	ba,
	cs_,
	dqm,
	cke,
	ras_,
	cas_,
	we_,
	sd_clk
	);

inout	[31:0]	dq;
input	[11:0]	addr;
input	[3:0]	dqm;
input	[1:0]	ba;
input		cke, cs_, ras_, cas_, we_, sd_clk;
wire	[31:0]	dq;

mt48lc16m8a2	sdram0(
	.Dq			(dq[7:0]	),
	.Addr		(addr		),
	.Ba			(ba			),
	.Clk		(sd_clk		),
	.Cke		(cke		),
	.Cs_n		(cs_		),
	.Ras_n		(ras_		),
	.Cas_n		(cas_		),
	.We_n		(we_		),
	.Dqm		(dqm[0]		)
	);

mt48lc16m8a2	sdram1(
	.Dq			(dq[15:8]	),
	.Addr		(addr		),
	.Ba			(ba			),
	.Clk		(sd_clk		),
	.Cke		(cke		),
	.Cs_n		(cs_		),
	.Ras_n		(ras_		),
	.Cas_n		(cas_		),
	.We_n		(we_		),
	.Dqm		(dqm[1]		)
	);

mt48lc16m8a2	sdram2(
	.Dq			(dq[23:16]	),
	.Addr		(addr		),
	.Ba			(ba			),
	.Clk		(sd_clk		),
	.Cke		(cke		),
	.Cs_n		(cs_		),
	.Ras_n		(ras_		),
	.Cas_n		(cas_		),
	.We_n		(we_		),
	.Dqm		(dqm[2]		)
	);

mt48lc16m8a2	sdram3(
	.Dq			(dq[31:24]	),
	.Addr		(addr		),
	.Ba			(ba			),
	.Clk		(sd_clk		),
	.Cke		(cke		),
	.Cs_n		(cs_		),
	.Ras_n		(ras_		),
	.Cas_n		(cas_		),
	.We_n		(we_		),
	.Dqm		(dqm[3]		)
	);

endmodule

//	------------------------------------

