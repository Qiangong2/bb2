/*********************************************************************
//File 	: 	internal_mem_bus_monitor.v -- used the interface protocol
//			to check the bus automatically
//Author: 	Yuky
//Date	: 	2002-06-03	Initial version
//			2002-07-12	disable the wdata and wbe monitor
*********************************************************************/
module internal_mem_bus_monitor(
	xxx_ram_req,
	xxx_ram_rw,
	xxx_ram_addr,
	xxx_ram_rbl,
	xxx_ram_ack,

	xxx_ram_wlast,
	xxx_ram_wrdy,
	xxx_ram_wbe,
	xxx_ram_wdata,
	xxx_ram_rlast,
	xxx_ram_rrdy,
	xxx_ram_rdata,

	clk,
	rst_
	);
//The monitor object name ASCII number
//default:XXX
parameter BYTE3 = 0;
parameter BYTE2 = 88;
parameter BYTE1 = 88;
parameter BYTE0 = 88;

parameter udly = 1;
input		xxx_ram_req;
input		xxx_ram_rw;
input[27:0]	xxx_ram_addr;
input[2:0]	xxx_ram_rbl;
input		xxx_ram_ack;

input		xxx_ram_wlast;
input		xxx_ram_wrdy;
input[3:0]	xxx_ram_wbe;
input[31:0]	xxx_ram_wdata;
input		xxx_ram_rlast;
input		xxx_ram_rrdy;
input[31:0]	xxx_ram_rdata;

input	clk;
input	rst_;

`ifdef INTERNAL_MEM_MONITOR
integer	inter_mem_moni_msg;
initial inter_mem_moni_msg = $fopen("internal_mem_monitor.rpt");
`endif

//make a counter to calculate the rst_ what time to active
reg	rst_tri;
initial begin
	rst_tri = 1'b0;
	wait (rst_ == 1'b0);
	#20 rst_tri = 1'b1;
end

//Make the 4 character to one ID
reg	[31:0] ID;
initial ID = {BYTE3[7:0],BYTE2[7:0],BYTE1[7:0],BYTE0[7:0]};
//================== Error Message =========================
`ifdef CLOSE_INTERNAL_MEM_ERROR
`else

//req, become unactive before ack active during request.
//it must keep its active status till ack active. but it can keep
//active after ack active for next request.
reg	req_dly_1;
always @(posedge clk or negedge rst_)
	if (!rst_)
		req_dly_1 <= #udly 1'b0;
	else
		req_dly_1 <= #udly xxx_ram_req;

reg	ack_dly_1;
always @(posedge clk or negedge rst_)
	if (!rst_)
		ack_dly_1 <= #udly 1'b0;
	else
		ack_dly_1 <= #udly xxx_ram_ack;
wire req_1change0 = (xxx_ram_req == 0 && req_dly_1 == 1);
//always @(negedge xxx_ram_req)
always @(posedge clk or negedge rst_)
	if (rst_&&(ack_dly_1 === 1'b0)&& req_1change0) begin
		$display("Error_T2_rsim --> Internal Mem bus interface protocol error:");
		$display("%s_ram_req become unactive before %s_ram_ack active at %0t",ID,ID,$realtime);
		$display("");
		`ifdef INTERNAL_MEM_MONITOR
		$fdisplay(inter_mem_moni_msg,"Error_T2_rsim --> Internal Mem bus interface protocol error:");
		$fdisplay(inter_mem_moni_msg,"%s_ram_req become unactive before %s_ram_ack active at %0t",ID,ID,$realtime);
		$fdisplay(inter_mem_moni_msg,"");
		`endif
	end

//rw, change its value in the course of request, before ack active
//it should be keep it the init value till the ack active during request
reg	req_check_period;
always @(posedge clk or negedge rst_)
	if (!rst_)
		req_check_period <= #udly 1'b0;
	else if (xxx_ram_ack)
		req_check_period <= #udly 1'b0;
	else if (xxx_ram_req)
		req_check_period <= #udly 1'b1;
	else
		req_check_period <= #udly 1'b0;

reg	rw_dly_1;
always @(posedge clk or negedge rst_)
	if (!rst_)
		rw_dly_1 <= #udly 1'b0;
	else
		rw_dly_1 <= #udly xxx_ram_rw;

wire rw_change = (xxx_ram_rw !== rw_dly_1);

always @(posedge clk)
	if (rst_ && req_check_period && rw_change) begin
		$display("Error_T2_rsim --> Internal Mem bus interface protocol error:");
		$display("%s_ram_rw changes its value during request at %0t",ID,$realtime);
		$display("");
		`ifdef INTERNAL_MEM_MONITOR
		$fdisplay(inter_mem_moni_msg,"Error_T2_rsim --> Internal Mem bus interface protocol error:");
		$fdisplay(inter_mem_moni_msg,"%s_ram_rw changes its value during request at %0t",ID,$realtime);
		$fdisplay(inter_mem_moni_msg,"");
		`endif
	end

//addr, change its value in the course of request, before ack active
//it should be keep it the init value till the ack active during request
reg [27:0] addr_dly_1;
always @(posedge clk or negedge rst_)
	if (!rst_)
		addr_dly_1 <= #udly 28'h0;
	else
		addr_dly_1 <= #udly xxx_ram_addr;

wire addr_change = (xxx_ram_addr !== addr_dly_1);

always @(posedge clk)
	if (rst_ && req_check_period && addr_change) begin
		$display("Error_T2_rsim --> Internal Mem bus interface protocol error:");
		$display("%s_ram_addr changes its value during request at %0t",ID,$realtime);
		$display("");
		`ifdef INTERNAL_MEM_MONITOR
		$fdisplay(inter_mem_moni_msg,"Error_T2_rsim --> Internal Mem bus interface protocol error:");
		$fdisplay(inter_mem_moni_msg,"%s_ram_addr changes its value during request at %0t",ID,$realtime);
		$fdisplay(inter_mem_moni_msg,"");
		`endif
	end

//rbl, change its value in the course of request, before ack active
//it should be keep it the init value till the ack active during request
reg [2:0] rbl_dly_1;
always @(posedge clk or negedge rst_)
	if (!rst_)
		rbl_dly_1 <= #udly 3'h0;
	else
		rbl_dly_1 <= #udly xxx_ram_rbl;

wire rbl_change = (xxx_ram_rbl !== rbl_dly_1);

always @(posedge clk)
	if (rst_ && req_check_period && rbl_change && !xxx_ram_rw ) begin
		$display("Error_T2_rsim --> Internal Mem bus interface protocol error:");
		$display("%s_ram_rbl changes its value during request at %0t",ID,$realtime);
		$display("");
		`ifdef INTERNAL_MEM_MONITOR
		$fdisplay(inter_mem_moni_msg,"Error_T2_rsim --> Internal Mem bus interface protocol error:");
		$fdisplay(inter_mem_moni_msg,"%s_ram_rbl changes its value during request at %0t",ID,$realtime);
		$fdisplay(inter_mem_moni_msg,"");
		`endif
	end

//rlast, keep active continuously more than one clock cycle
//it just is only one clock pulse.
reg	rlast_dly_1;
always @(posedge clk or negedge rst_)
	if (!rst_)
		rlast_dly_1 <= #udly 1'b0;
	else
		rlast_dly_1 <= #udly xxx_ram_rlast;

wire rlast_error_1 = (xxx_ram_rlast === 1'b1) && (rlast_dly_1 === 1'b1);
`ifdef	NEW_MEM
`else
always @(posedge clk)
	if (rst_ && rlast_error_1) begin
		$display("Error_T2_rsim --> Internal Mem bus interface protocol error:");
		$display("%s_ram_rlast keep active continuously more than one clock at %0t",ID,$realtime);
		$display("");
		`ifdef INTERNAL_MEM_MONITOR
		$fdisplay(inter_mem_moni_msg,"Error_T2_rsim --> Internal Mem bus interface protocol error:");
		$fdisplay(inter_mem_moni_msg,"%s_ram_rlast keep active continuously more than one clock at %0t",ID,$realtime);
		$fdisplay(inter_mem_moni_msg,"");
		`endif
	end
`endif

//rlast, become active when rrdy unactive.
//it just indicates the data is the last data during data transaction--rrdy is active
wire rlast_error_2 = (xxx_ram_rlast === 1'b1) && (xxx_ram_rrdy === 1'b0);
always @(posedge clk)
	if (rst_ && rlast_error_2) begin
		$display("Error_T2_rsim --> Internal Mem bus interface protocol error:");
		$display("%s_ram_rlast become active when rrdy is unactive at %0t",ID,$realtime);
		$display("");
		`ifdef INTERNAL_MEM_MONITOR
		$fdisplay(inter_mem_moni_msg,"Error_T2_rsim --> Internal Mem bus interface protocol error:");
		$fdisplay(inter_mem_moni_msg,"%s_ram_rlast become active when rrdy is unactive at %0t",ID,$realtime);
		$fdisplay(inter_mem_moni_msg,"");
		`endif
	end

//wlast, become unactive before wrdy active during the one write request.
//it does not need to be a clock pulse when the wrdy is not ready at
//the last write data. wlast can be high active till the wrdy ready at
//the last data transaction.
//when the write operation is one data, then the wlast can be active with
//req signal and wait the wrdy become active
reg wr_req_flag;
always @(posedge clk or negedge rst_)
	if (!rst_)
		wr_req_flag <= #udly 1'b0;
	else if (xxx_ram_wlast & xxx_ram_wrdy)//write request transaction end
		wr_req_flag <= #udly 1'b0;
	else if (xxx_ram_req & xxx_ram_rw)//write request come
		wr_req_flag <= #udly 1'b1;

reg	wrdy_dly_1;
always @(posedge clk or negedge rst_)
	if (!rst_)
		wrdy_dly_1 <= #udly 1'b0;
	else
		wrdy_dly_1 <= #udly xxx_ram_wrdy;

wire	wrdy_width = xxx_ram_wrdy | wrdy_dly_1;
always @(negedge xxx_ram_wlast)
	if (rst_ && wr_req_flag && !wrdy_width) begin
		$display("Error_T2_rsim --> Internal Mem bus interface protocol error:");
		$display("%s_ram_wlast become unactive before wrdy active during one write request at %0t",ID,$realtime);
		$display("");
		`ifdef INTERNAL_MEM_MONITOR
		$fdisplay(inter_mem_moni_msg,"Error_T2_rsim --> Internal Mem bus interface protocol error:");
		$fdisplay(inter_mem_moni_msg,"%s_ram_wlast become unactive before wrdy active during one write request at %0t",ID,$realtime);
		$fdisplay(inter_mem_moni_msg,"");
		`endif
	end

//rrdy, its valid total cycle is more than the rbl indication.
//the valid total cycle must equals to rbl indication.
//rbl indication: 	000b --> 1 	001b --> 2
//					010b --> 3  011b --> 4
//					100b --> 5 	101b --> 6
//					110b --> 7  111b --> 8
//use the 8 level fifo to keep the rbl request message
wire rbl_ren = xxx_ram_rlast;
wire rbl_wen = xxx_ram_req && !xxx_ram_rw && xxx_ram_ack;
wire [2:0] rbl_exp_msg;
rbl_fifo rbl_fifo(
       .in_rbl		(xxx_ram_rbl),
       .out_rbl		(rbl_exp_msg),
       .read_en		(rbl_ren),
       .write_en	(rbl_wen),
       .fifo_full	(fifo_full),
       .fifo_empty	(fifo_empty),
       .clk			(clk),
       .rst_		(rst_)
       );

reg [3:0] len_msg;
always @(posedge clk or negedge rst_)
	if (!rst_)
		len_msg <= #udly 4'h0;
	else if (xxx_ram_rlast) begin
		len_msg <= #udly 4'h0;
		if (len_msg !== rbl_exp_msg) begin
			$display("Error_T2_rsim --> Internal Mem bus interface protocol error:");
			$display("%s_ram_rrdy total valid cycles are not matched the %s_ram_rbl at %0t",ID,ID,$realtime);
			$display("");
			`ifdef INTERNAL_MEM_MONITOR
			$fdisplay(inter_mem_moni_msg,"Error_T2_rsim --> Internal Mem bus interface protocol error:");
			$fdisplay(inter_mem_moni_msg,"%s_ram_rrdy total valid cycles are not matched the %s_ram_rbl at %0t",ID,ID,$realtime);
			$fdisplay(inter_mem_moni_msg,"");
			`endif
		end
	end
	else if (xxx_ram_rrdy)
		len_msg <= #udly len_msg + 1;

//pipeline read requests outstanding total is more than 8, which is not be finished read transaction
always @(posedge clk)
	if (rst_ && fifo_full && xxx_ram_req && !xxx_ram_rw) begin
		$display("Error_T2_rsim --> Internal Mem bus interface protocol error:");
		$display("%s pipeline read requests' total is more than 8 level at %0t",ID,$realtime);
		$display("");
		`ifdef INTERNAL_MEM_MONITOR
		$fdisplay(inter_mem_moni_msg,"Error_T2_rsim --> Internal Mem bus interface protocol error:");
		$fdisplay(inter_mem_moni_msg,"%s pipeline read requests' total is more than 8 level at %0t",ID,$realtime);
		$fdisplay(inter_mem_moni_msg,"");
		`endif
	end

//pipelines read requests' acknowledge total is more than the requests total.
always @(posedge clk)
	if (rst_ && fifo_empty && xxx_ram_rrdy) begin
		$display("Error_T2_rsim --> Internal Mem bus interface protocol error:");
		$display("%s pipeline read requests' acknowledge total is more than the requests total at %0t",ID,$realtime);
		$display("");
		`ifdef INTERNAL_MEM_MONITOR
		$fdisplay(inter_mem_moni_msg,"Error_T2_rsim --> Internal Mem bus interface protocol error:");
		$fdisplay(inter_mem_moni_msg,"%s pipeline read requests' acknowledge total is more than the requests total at %0t",ID,$realtime);
		$fdisplay(inter_mem_moni_msg,"");
		`endif
	end

//wrdy, its valid cycle total is more than 8.
//although there is not wbl to indicate the write request data length.
//but the total cycle can't greater than 8
reg	[3:0] wr_len;
always @(posedge clk or negedge rst_)
	if (!rst_)
		wr_len <= #udly 4'h0;
	else if (xxx_ram_wlast & xxx_ram_wrdy) begin
		wr_len <= #udly 4'h0;
		if (wr_len > 4'b0111) begin
			$display("Error_T2_rsim --> Internal Mem bus interface protocol error:");
			$display("%s_ram_wrdy total valid cycles are more than 8 Dwords at %0t",ID,$realtime);
			$display("");
			`ifdef INTERNAL_MEM_MONITOR
			$fdisplay(inter_mem_moni_msg,"Error_T2_rsim --> Internal Mem bus interface protocol error:");
			$fdisplay(inter_mem_moni_msg,"%s_ram_wrdy total valid cycles are more than 8 Dwords at %0t",ID,$realtime);
			$fdisplay(inter_mem_moni_msg,"");
			`endif
		end
	end
	else if (xxx_ram_wrdy)
		wr_len <= #udly wr_len + 1;

//wrdy, when is active with wlast acitve at the same time,
//then next clock, wrdy must become unactive.
reg wlast_flag;
always @(posedge clk or negedge rst_)
	if (!rst_)
		wlast_flag <= #udly 1'b0;
	else if (xxx_ram_wlast & xxx_ram_wrdy)
		wlast_flag <= #udly 1'b1;
	else
		wlast_flag <= #udly 1'b0;

wire wrdy_error_1 = (wlast_flag === 1'b1) && (xxx_ram_wrdy === 1'b1);
`ifdef  NEW_MEM
`else
always @(posedge clk)
	if (rst_ && wrdy_error_1) begin
		$display("Error_T2_rsim --> Internal Mem bus interface protocol error:");
		$display("%s_ram_wrdy still keep active when wlast is active for the last transaction at %0t",ID,$realtime);
		$display("");
		`ifdef INTERNAL_MEM_MONITOR
		$fdisplay(inter_mem_moni_msg,"Error_T2_rsim --> Internal Mem bus interface protocol error:");
		$fdisplay(inter_mem_moni_msg,"%s_ram_wrdy still keep active when wlast is active for the last transaction at %0t",ID,$realtime);
		$fdisplay(inter_mem_moni_msg,"");
		`endif
	end
`endif
//==================================================================
//Now we suppose that the write request not support pipeline
//then the wdata and wbe should not be changed after request untill the wrdy active
//==================================================================
//wdata change its value before the wrdy active
reg wdata_check_period;
always @(posedge clk or negedge rst_)
	if (!rst_)
		wdata_check_period <= #udly 1'b0;
	else if (xxx_ram_wrdy)
		wdata_check_period <= #udly 1'b0;
	else if (xxx_ram_req & xxx_ram_rw)
		wdata_check_period <= #udly 1'b1;

reg	[31:0] wdata_dly_1;
always @(posedge clk or negedge rst_)
	if (!rst_)
		wdata_dly_1 <= #udly 32'h0;
	else
		wdata_dly_1 <= #udly xxx_ram_wdata;

wire wdata_change = (xxx_ram_wdata !== wdata_dly_1);
always @(posedge clk)
	if (rst_ && wdata_check_period && wdata_change) begin
//		$display("Error_T2_rsim --> Internal Mem bus interface protocol error:");
//		$display("%s_ram_wdata changes its value before %s_ram_wrdy active at %0t",ID,ID,$realtime);
//		$display("");
		`ifdef INTERNAL_MEM_MONITOR
		$fdisplay(inter_mem_moni_msg,"Error_T2_rsim --> Internal Mem bus interface protocol error:");
		$fdisplay(inter_mem_moni_msg,"%s_ram_wdata changes its value before %s_ram_wrdy active at %0t",ID,ID,$realtime);
		$fdisplay(inter_mem_moni_msg,"");
		`endif
	end

//wbe change its value before the wrdy active
reg [3:0] wbe_dly_1;
always @(posedge clk or negedge rst_)
	if (!rst_)
		wbe_dly_1 <= #udly 4'h0;
	else
		wbe_dly_1 <= #udly xxx_ram_wbe;

wire wbe_change = (xxx_ram_wbe !== wbe_dly_1);
always @(posedge clk)
	if (rst_ && wdata_check_period && wbe_change) begin
//		$display("Error_T2_rsim --> Internal Mem bus interface protocol error:");
//		$display("%s_ram_wbe changes its value before %s_ram_wrdy active at %0t",ID,ID,$realtime);
//		$display("");
		`ifdef INTERNAL_MEM_MONITOR
		$fdisplay(inter_mem_moni_msg,"Error_T2_rsim --> Internal Mem bus interface protocol error:");
		$fdisplay(inter_mem_moni_msg,"%s_ram_wbe changes its value before %s_ram_wrdy active at %0t",ID,ID,$realtime);
		$fdisplay(inter_mem_moni_msg,"");
		`endif
	end

//ack, keep active more than one clock,
//it just is a one clock pulse.
wire ack_error_1 = (xxx_ram_ack === 1'b1) && (ack_dly_1 === 1'b1);
always @(posedge clk)
	if (rst_ && rst_tri && ack_error_1) begin
		$display("Error_T2_rsim --> Internal Mem bus interface protocol error:");
		$display("%s_ram_ack keep active status more than one clock at %0t",ID,$realtime);
		$display("");
		`ifdef INTERNAL_MEM_MONITOR
		$fdisplay(inter_mem_moni_msg,"Error_T2_rsim --> Internal Mem bus interface protocol error:");
		$fdisplay(inter_mem_moni_msg,"%s_ram_ack keep active status more than one clock at %0t",ID,$realtime);
		$fdisplay(inter_mem_moni_msg,"");
		`endif
	end

//ack, it become active immediately with the req active
//it should be delay one clock after req become active
wire req_one_pulse = xxx_ram_req && !req_dly_1;
wire ack_error_2 = req_one_pulse && xxx_ram_ack;
always @(posedge clk)
	if (rst_ && rst_tri && ack_error_2) begin
		$display("Error_T2_rsim --> Internal Mem bus interface protocol error:");
		$display("%s_ram_ack become active immediate with %s_ram_req at %0t",ID,ID,$realtime);
		$display("");
		`ifdef INTERNAL_MEM_MONITOR
		$fdisplay(inter_mem_moni_msg,"Error_T2_rsim --> Internal Mem bus interface protocol error:");
		$fdisplay(inter_mem_moni_msg,"%s_ram_ack become active immediate with %s_ram_req at %0t",ID,ID,$realtime);
		$fdisplay(inter_mem_moni_msg,"");
		`endif
	end

//ack, become active during req unactive
wire ack_error_3 = (xxx_ram_ack === 1'b1) && (xxx_ram_req === 1'b0);
always @(posedge clk)
	if (rst_ && rst_tri && ack_error_3) begin
		$display("Error_T2_rsim --> Internal Mem bus interface protocol error:");
		$display("%s_ram_ack become active during %s_ram_req unactive at %0t",ID,ID,$realtime);
		$display("");
		`ifdef INTERNAL_MEM_MONITOR
		$fdisplay(inter_mem_moni_msg,"Error_T2_rsim --> Internal Mem bus interface protocol error:");
		$fdisplay(inter_mem_moni_msg,"%s_ram_ack become active during %s_ram_req unactive at %0t",ID,ID,$realtime);
		$fdisplay(inter_mem_moni_msg,"");
		`endif
	end

`endif

//=============== Warning Message ==================
`ifdef CLOSE_INTERNAL_MEM_WARNING
`else

//ack, the latency is too long after req active
reg	[10:0] lat_cnt; //2048
wire lat_cnt_full = (lat_cnt === 11'h7ff);
always @(posedge clk or negedge rst_)
	if (!rst_)
		lat_cnt	<= #udly 11'h0;
	else if (xxx_ram_ack)
		lat_cnt <= #udly 11'h0;
	else if (xxx_ram_req && !lat_cnt_full)
		lat_cnt <= #udly lat_cnt + 1;

reg	lat_cnt_full_dly_1;
always @(posedge clk or negedge rst_)
	if (!rst_)
		lat_cnt_full_dly_1 <= #udly 1'b0;
	else
		lat_cnt_full_dly_1 <= #udly lat_cnt_full;

wire lat_cnt_full_pulse = (lat_cnt_full & ~lat_cnt_full_dly_1);
always @(posedge clk)
	if (lat_cnt_full_pulse) begin
		$display("Warning_T2_rsim --> Internal Mem bus interface protocol Warning:");
		$display("%s_ram_ack latency is too long after %s_ram_req active at %0t",ID,ID,$realtime);
		$display("");
		`ifdef INTERNAL_MEM_MONITOR
		$fdisplay(inter_mem_moni_msg,"Warning_T2_rsim --> Internal Mem bus interface protocol Warning:");
		$fdisplay(inter_mem_moni_msg,"%s_ram_ack latency is too long after %s_ram_req active at %0t",ID,ID,$realtime);
		$fdisplay(inter_mem_moni_msg,"");
		`endif
	end

//be, its value equals to Zero, 4'h0
always @(posedge clk)
	if (rst_ && xxx_ram_req && xxx_ram_rw && (xxx_ram_wbe === 4'b0000) ) begin
//		$display("Warning_T2_rsim --> Internal Mem bus interface protocol Warning:");
//		$display("%s_ram_wbe value is zero(4'b0000) during write request at %0t",ID,$realtime);
//		$display("");
		`ifdef INTERNAL_MEM_MONITOR
		$fdisplay(inter_mem_moni_msg,"Warning_T2_rsim --> Internal Mem bus interface protocol Warning:");
		$fdisplay(inter_mem_moni_msg,"%s_ram_wbe value is zero(4'b0000) during write request at %0t",ID,$realtime);
		$fdisplay(inter_mem_moni_msg,"");
		`endif
	end

//the request is in uncertain status
always @(posedge clk)
	if (rst_ && rst_tri && (xxx_ram_req === 1'bx)) begin
		$display("Warning_T2_rsim --> Internal Mem bus interface protocol Warning:");
		$display("%s_ram_req value is in uncertain status at %0t",ID,$realtime);
		$display("");
		`ifdef INTERNAL_MEM_MONITOR
		$fdisplay(inter_mem_moni_msg,"Warning_T2_rsim --> Internal Mem bus interface protocol Warning:");
		$fdisplay(inter_mem_moni_msg,"%s_ram_req value is in uncertain status at %0t",ID,$realtime);
		$fdisplay(inter_mem_moni_msg,"");
		`endif
	end

//rw is in uncertain status
always @(posedge clk)
	if (rst_ && rst_tri && (xxx_ram_rw === 1'bx)) begin
		$display("Warning_T2_rsim --> Internal Mem bus interface protocol Warning:");
		$display("%s_ram_rw value is in uncertain status at %0t",ID,$realtime);
		$display("");
		`ifdef INTERNAL_MEM_MONITOR
		$fdisplay(inter_mem_moni_msg,"Warning_T2_rsim --> Internal Mem bus interface protocol Warning:");
		$fdisplay(inter_mem_moni_msg,"%s_ram_rw value is in uncertain status at %0t",ID,$realtime);
		$fdisplay(inter_mem_moni_msg,"");
		`endif
	end

//addr is in uncertain status
always @(posedge clk)
	if (rst_ && rst_tri && (^xxx_ram_addr === 1'bx)) begin
		$display("Warning_T2_rsim --> Internal Mem bus interface protocol Warning:");
		$display("%s_ram_addr value is in uncertain status at %0t",ID,$realtime);
		$display("");
		`ifdef INTERNAL_MEM_MONITOR
		$fdisplay(inter_mem_moni_msg,"Warning_T2_rsim --> Internal Mem bus interface protocol Warning:");
		$fdisplay(inter_mem_moni_msg,"%s_ram_addr value is in uncertain status at %0t",ID,$realtime);
		$fdisplay(inter_mem_moni_msg,"");
		`endif
	end

//ack is in uncertain status
always @(posedge clk)
	if (rst_ && rst_tri && (xxx_ram_ack === 1'bx)) begin
		$display("Warning_T2_rsim --> Internal Mem bus interface protocol Warning:");
		$display("%s_ram_ack value is in uncertain status at %0t",ID,$realtime);
		$display("");
		`ifdef INTERNAL_MEM_MONITOR
		$fdisplay(inter_mem_moni_msg,"Warning_T2_rsim --> Internal Mem bus interface protocol Warning:");
		$fdisplay(inter_mem_moni_msg,"%s_ram_ack value is in uncertain status at %0t",ID,$realtime);
		$fdisplay(inter_mem_moni_msg,"");
		`endif
	end

//rbl is in uncertain status
always @(posedge clk)
	if (rst_ && rst_tri && (^xxx_ram_rbl === 1'bx)) begin
		$display("Warning_T2_rsim --> Internal Mem bus interface protocol Warning:");
		$display("%s_ram_rbl value is in uncertain status at %0t",ID,$realtime);
		$display("");
		`ifdef INTERNAL_MEM_MONITOR
		$fdisplay(inter_mem_moni_msg,"Warning_T2_rsim --> Internal Mem bus interface protocol Warning:");
		$fdisplay(inter_mem_moni_msg,"%s_ram_rbl value is in uncertain status at %0t",ID,$realtime);
		$fdisplay(inter_mem_moni_msg,"");
		`endif
	end

//rlast is in uncertain status
always @(posedge clk)
	if (rst_ && rst_tri && (xxx_ram_rlast === 1'bx)) begin
		$display("Warning_T2_rsim --> Internal Mem bus interface protocol Warning:");
		$display("%s_ram_rlast value is in uncertain status at %0t",ID,$realtime);
		$display("");
		`ifdef INTERNAL_MEM_MONITOR
		$fdisplay(inter_mem_moni_msg,"Warning_T2_rsim --> Internal Mem bus interface protocol Warning:");
		$fdisplay(inter_mem_moni_msg,"%s_ram_rlast value is in uncertain status at %0t",ID,$realtime);
		$fdisplay(inter_mem_moni_msg,"");
		`endif
	end

//rrdy is in uncertain status
always @(posedge clk)
	if (rst_ && rst_tri && (xxx_ram_rrdy === 1'bx)) begin
		$display("Warning_T2_rsim --> Internal Mem bus interface protocol Warning:");
		$display("%s_ram_rrdy value is in uncertain status at %0t",ID,$realtime);
		$display("");
		`ifdef INTERNAL_MEM_MONITOR
		$fdisplay(inter_mem_moni_msg,"Warning_T2_rsim --> Internal Mem bus interface protocol Warning:");
		$fdisplay(inter_mem_moni_msg,"%s_ram_rrdy value is in uncertain status at %0t",ID,$realtime);
		$fdisplay(inter_mem_moni_msg,"");
		`endif
	end

//wlast is in uncertain status
always @(posedge clk)
	if (rst_ && rst_tri && (xxx_ram_wlast === 1'bx)) begin
		$display("Warning_T2_rsim --> Internal Mem bus interface protocol Warning:");
		$display("%s_ram_wlast value is in uncertain status at %0t",ID,$realtime);
		$display("");
		`ifdef INTERNAL_MEM_MONITOR
		$fdisplay(inter_mem_moni_msg,"Warning_T2_rsim --> Internal Mem bus interface protocol Warning:");
		$fdisplay(inter_mem_moni_msg,"%s_ram_wlast value is in uncertain status at %0t",ID,$realtime);
		$fdisplay(inter_mem_moni_msg,"");
		`endif
	end

//wbe is in uncertain status
always @(posedge clk)
	if (rst_ && rst_tri && (^xxx_ram_wbe === 1'bx)) begin
		$display("Warning_T2_rsim --> Internal Mem bus interface protocol Warning:");
		$display("%s_ram_wbe value is in uncertain status at %0t",ID,$realtime);
		$display("");
		`ifdef INTERNAL_MEM_MONITOR
		$fdisplay(inter_mem_moni_msg,"Warning_T2_rsim --> Internal Mem bus interface protocol Warning:");
		$fdisplay(inter_mem_moni_msg,"%s_ram_wbe value is in uncertain status at %0t",ID,$realtime);
		$fdisplay(inter_mem_moni_msg,"");
		`endif
	end

//rdata include uncertain status
always @(posedge clk)
	if (xxx_ram_rrdy && (^xxx_ram_rdata === 1'bx)) begin
		$display("Warning_T2_rsim --> Internal Mem bus interface protocol Warning:");
		$display("%s_ram_rdata value is in uncertain status at %0t, rdata=%h",ID,$realtime, xxx_ram_rdata);
		$display("");
		`ifdef INTERNAL_MEM_MONITOR
		$fdisplay(inter_mem_moni_msg,"Warning_T2_rsim --> Internal Mem bus interface protocol Warning:");
		$fdisplay(inter_mem_moni_msg,"%s_ram_rdata value is in uncertain status at %0t, rdata=%h",ID,$realtime, xxx_ram_rdata);
		$fdisplay(inter_mem_moni_msg,"");
		`endif
	end

//wdata include uncertain status
wire [7:0] wr_byte0 = xxx_ram_wbe[0] ? xxx_ram_wdata[7:0]	: 8'h0;
wire [7:0] wr_byte1 = xxx_ram_wbe[1] ? xxx_ram_wdata[15:8]	: 8'h0;
wire [7:0] wr_byte2 = xxx_ram_wbe[2] ? xxx_ram_wdata[23:16]	: 8'h0;
wire [7:0] wr_byte3 = xxx_ram_wbe[3] ? xxx_ram_wdata[31:24]	: 8'h0;

always @(posedge clk)
	if (xxx_ram_wrdy & ((^wr_byte0 === 1'bx)|(^wr_byte1 === 1'bx)|(^wr_byte2 === 1'bx)|(^wr_byte3 === 1'bx))) begin
		$display("Warning_T2_rsim --> Internal Mem bus interface protocol Warning:");
		$display("%s_ram_wdata value is in uncertain status at %0t, wdata=%h",ID,$realtime, xxx_ram_wdata);
		$display("");
		`ifdef INTERNAL_MEM_MONITOR
		$fdisplay(inter_mem_moni_msg,"Warning_T2_rsim --> Internal Mem bus interface protocol Warning:");
		$fdisplay(inter_mem_moni_msg,"%s_ram_wdata value is in uncertain status at %0t, wdata=%h",ID,$realtime, xxx_ram_wdata);
		$fdisplay(inter_mem_moni_msg,"");
		`endif
	end

`endif
endmodule






module rbl_fifo(
       in_rbl,          // output data,3 bits
       out_rbl,         // output data,3 bits

       read_en,           // read enable, get out data from the fifo
       write_en,          	// write enable,write data to the fifo

       fifo_full,     	// indicates the fifo is full, can't save new data to it
       fifo_empty,  	// indicates the fifo is empty, can't read new data out from it

       clk,			  	// clock signal
       rst_			  	// reset signal
       );
parameter DEPTH_INDEX = 3;		// 2^3 = 8 level
parameter udly = 1;				// delay unit: 1ns

input [2:0] in_rbl;
input       clk,    rst_,
            read_en,  write_en;

output [2:0] out_rbl;
output       fifo_full;
output       fifo_empty;
//---------------- define the FIFO
// total 2 registers, one memory
reg      [DEPTH_INDEX : 0]        wr_imag_point,
                                  rd_imag_point;
reg 	 [2:0] 	rbl_msg_mem [2<<DEPTH_INDEX - 1:0];
wire     [DEPTH_INDEX - 1 : 0]    wr_real_point,
                                  rd_real_point;
wire     fifo_full,
         fifo_empty;
wire	 fifo_send_rdy,
		 fifo_receive_rdy;
wire     same_band,
         diff_band,
         equal_point;

assign  wr_real_point = wr_imag_point[DEPTH_INDEX - 1 : 0];
assign  rd_real_point = rd_imag_point[DEPTH_INDEX - 1 : 0];
assign  same_band   = (wr_imag_point[DEPTH_INDEX] == rd_imag_point[DEPTH_INDEX]);
assign  diff_band   = (wr_imag_point[DEPTH_INDEX] != rd_imag_point[DEPTH_INDEX]);
assign  equal_point = (wr_real_point == rd_real_point );
assign  fifo_full   = (diff_band && equal_point );
assign  fifo_empty  = (same_band && equal_point);
assign  fifo_send_rdy    = !fifo_empty;
assign  fifo_receive_rdy = !fifo_full;

wire	wr_en		= fifo_receive_rdy & write_en;
wire	rd_en		= fifo_send_rdy & read_en;

//-----------------registers
//write data to FIFO when fifo and data source ready
always  @(posedge clk or negedge rst_)
        if (!rst_)
           wr_imag_point <= #udly 4'h0;
        else if (wr_en) begin
           wr_imag_point <= #udly wr_imag_point + 1;
		   rbl_msg_mem[wr_real_point] <= #udly in_rbl;
		end
//read data from FIFO when fifo and data sink ready
always  @(posedge clk or negedge rst_)
        if (!rst_)
           rd_imag_point <= #udly 4'h0;
        else if (rd_en)//rd_en means the receiver is ready to receive data
           rd_imag_point <= #udly rd_imag_point + 1;

wire [2:0] out_rbl = rbl_msg_mem[rd_real_point];
endmodule
