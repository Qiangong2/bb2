/*********************************************************************
//File 	: 	internal_local_bus_monitor.v -- used the interface protocol
//			to check the bus automatically
//Author: 	Yuky
//Date	: 	2002-05-31	Initial version
*********************************************************************/
module internal_local_bus_monitor (
	cpu_xxx_req,
	cpu_xxx_rw,
	cpu_xxx_addr,
	cpu_xxx_be,
	cpu_xxx_wdata,
	cpu_xxx_rdata,
	cpu_xxx_ack,

	clk,
	rst_
	);
//The monitor object name ASCII number
//default:XXX
parameter BYTE3 = 0;
parameter BYTE2 = 88;
parameter BYTE1 = 88;
parameter BYTE0 = 88;

parameter	udly = 1;
input 			clk, rst_;
input			cpu_xxx_req;
input			cpu_xxx_rw;
input	[15:2]	cpu_xxx_addr; //14width, word(4bytes)address
input	[3:0]	cpu_xxx_be;
input	[31:0]	cpu_xxx_wdata;
input	[31:0]	cpu_xxx_rdata;
input			cpu_xxx_ack;


`ifdef LOCAL_BUS_MONITOR
integer	local_moni_msg;
initial local_moni_msg = $fopen("local_bus_monitor.rpt");
`endif

//Make the 4 character to one ID
reg	[31:0] ID;
initial ID = {BYTE3[7:0],BYTE2[7:0],BYTE1[7:0],BYTE0[7:0]};
/*
initial begin
	#100 $display ("ID=%h",ID);
	$display ("ID=%s",ID);
end
*/
reg	req_dly_1;
//================== Error Message =========================
`ifdef CLOSE_LOCAL_ERROR
`else
//req, it can't change its value after it assert until the ack active.
reg	ack_dly_1;
always @(posedge clk or negedge rst_)
	if (!rst_)
		ack_dly_1 <= #udly 1'b0;
	else
		ack_dly_1 <= #udly cpu_xxx_ack;

wire req_1change0 = (cpu_xxx_req == 0 && req_dly_1 == 1);
//always @(negedge cpu_xxx_req)
always @(posedge clk or negedge rst_)
	if (rst_&&(ack_dly_1 === 1'b0) && req_1change0) begin
		$display("Error_T2_rsim --> Local bus interface protocol error:");
		$display("cpu_%s_req become unactive before cpu_%s_ack active at %0t",ID,ID,$realtime);
		$display("");
		`ifdef LOCAL_BUS_MONITOR
		$fdisplay(local_moni_msg,"Error_T2_rsim --> Local bus interface protocol error:");
		$fdisplay(local_moni_msg,"cpu_%s_req become unactive before cpu_%s_ack active at %0t",ID,ID,$realtime);
		$fdisplay(local_moni_msg,"");
		`endif
	end

//req, it still keep active after ack active
wire req_error_1 = (ack_dly_1 === 1'b1) && (cpu_xxx_req === 1'b1);
always @(posedge clk)
	if (rst_&&req_error_1) begin
		$display("Error_T2_rsim --> Local bus interface protocol error:");
		$display("cpu_%s_req still keep active after cpu_%s_ack active at %0t",ID,ID,$realtime);
		$display("");
		`ifdef LOCAL_BUS_MONITOR
		$fdisplay(local_moni_msg,"Error_T2_rsim --> Local bus interface protocol error:");
		$fdisplay(local_moni_msg,"cpu_%s_req still keep active after cpu_%s_ack active at %0t",ID,ID,$realtime);
		$fdisplay(local_moni_msg,"");
		`endif
	end

//rw, change its value in the course of request

always @(posedge clk or negedge rst_)
	if (!rst_)
		req_dly_1 <= #udly 1'b0;
	else
		req_dly_1 <= #udly cpu_xxx_req;

wire req_check_period = cpu_xxx_req & req_dly_1;

reg	rw_dly_1;
always @(posedge clk or negedge rst_)
	if (!rst_)
		rw_dly_1 <= #udly 1'b0;
	else
		rw_dly_1 <= #udly cpu_xxx_rw;

wire rw_change = (cpu_xxx_rw !== rw_dly_1);
always @(posedge clk)
	if (req_check_period & rw_change) begin
		$display("Error_T2_rsim --> Local bus interface protocol error:");
		$display("cpu_%s_rw change it value during request at %0t",ID,$realtime);
		$display("");
		`ifdef LOCAL_BUS_MONITOR
		$fdisplay(local_moni_msg,"Error_T2_rsim --> Local bus interface protocol error:");
		$fdisplay(local_moni_msg,"cpu_%s_rw change it value during request at %0t",ID,$realtime);
		$fdisplay(local_moni_msg,"");
		`endif
	end

//addr,change its value in the course of request
reg	[15:2] addr_dly_1;
always @(posedge clk or negedge rst_)
	if (!rst_)
		addr_dly_1 <= #udly 14'h0;
	else
		addr_dly_1 <= #udly cpu_xxx_addr;

wire addr_change = (cpu_xxx_addr !== addr_dly_1);
always @(posedge clk)
	if (req_check_period & addr_change) begin
		$display("Error_T2_rsim --> Local bus interface protocol error:");
		$display("cpu_%s_addr change it value during request at %0t",ID,$realtime);
		$display("");
		`ifdef LOCAL_BUS_MONITOR
		$fdisplay(local_moni_msg,"Error_T2_rsim --> Local bus interface protocol error:");
		$fdisplay(local_moni_msg,"cpu_%s_addr change it value during request at %0t",ID,$realtime);
		$fdisplay(local_moni_msg,"");
		`endif
	end

//be, change its value in the course of request
reg	[3:0] be_dly_1;
always @(posedge clk or negedge rst_)
	if (!rst_)
		be_dly_1 <= #udly 4'h0;
	else
		be_dly_1 <= #udly cpu_xxx_be;

wire be_change = (cpu_xxx_be !== be_dly_1);
always @(posedge clk)
	if (req_check_period & be_change) begin
		$display("Error_T2_rsim --> Local bus interface protocol error:");
		$display("cpu_%s_be change it value during request at %0t",ID,$realtime);
		$display("");
		`ifdef LOCAL_BUS_MONITOR
		$fdisplay(local_moni_msg,"Error_T2_rsim --> Local bus interface protocol error:");
		$fdisplay(local_moni_msg,"cpu_%s_be change it value during request at %0t",ID,$realtime);
		$fdisplay(local_moni_msg,"");
		`endif
	end

//wdata, change its value in the course of request
reg [31:0] wdata_dly_1;
always @(posedge clk or negedge rst_)
	if (!rst_)
		wdata_dly_1 <= #udly 32'h0;
	else
		wdata_dly_1 <= #udly cpu_xxx_wdata;

wire wdata_change = (cpu_xxx_wdata !== wdata_dly_1);
always @(posedge clk)
	if (req_check_period & wdata_change) begin
		$display("Error_T2_rsim --> Local bus interface protocol error:");
		$display("cpu_%s_wdata change it value during request at %0t",ID,$realtime);
		$display("");
		`ifdef LOCAL_BUS_MONITOR
		$fdisplay(local_moni_msg,"Error_T2_rsim --> Local bus interface protocol error:");
		$fdisplay(local_moni_msg,"cpu_%s_wdata change it value during request at %0t",ID,$realtime);
		$fdisplay(local_moni_msg,"");
		`endif
	end

//ack, become active during req unactive
wire ack_error_1 = (cpu_xxx_req === 1'b0) && (cpu_xxx_ack === 1'b1);
always @(posedge clk)
	if (rst_&&ack_error_1) begin
		$display("Error_T2_rsim --> Local bus interface protocol error:");
		$display("cpu_%s_ack become active during request unactive at %0t",ID,$realtime);
		$display("");
		`ifdef LOCAL_BUS_MONITOR
		$fdisplay(local_moni_msg,"Error_T2_rsim --> Local bus interface protocol error:");
		$fdisplay(local_moni_msg,"cpu_%s_ack become active during request unactive at %0t",ID,$realtime);
		$fdisplay(local_moni_msg,"");
		`endif
	end

//ack, active continuously more than one clock
wire ack_error_2 = (cpu_xxx_ack === 1'b1) && (ack_dly_1 === 1'b1);
always @(posedge clk)
	if (rst_&&ack_error_2) begin
		$display("Error_T2_rsim --> Local bus interface protocol error:");
		$display("cpu_%s_ack active more than one clock at %0t",ID,$realtime);
		$display("");
		`ifdef LOCAL_BUS_MONITOR
		$fdisplay(local_moni_msg,"Error_T2_rsim --> Local bus interface protocol error:");
		$fdisplay(local_moni_msg,"cpu_%s_ack active more than one clock at %0t",ID,$realtime);
		$fdisplay(local_moni_msg,"");
		`endif
	end

//ack, active immediately at the first clock of req active
wire req_first_pulse = (cpu_xxx_req && !req_dly_1);
wire ack_error_3 = (req_first_pulse === 1'b1) && (cpu_xxx_ack === 1'b1);
always @(posedge clk)
	if (rst_&&ack_error_3) begin
		$display("Error_T2_rsim --> Local bus interface protocol error:");
		$display("cpu_%s_ack active at the first clock of request active at %0t",ID,$realtime);
		$display("");
		`ifdef LOCAL_BUS_MONITOR
		$fdisplay(local_moni_msg,"Error_T2_rsim --> Local bus interface protocol error:");
		$fdisplay(local_moni_msg,"cpu_%s_ack active at the first clock of request active at %0t",ID,$realtime);
		$fdisplay(local_moni_msg,"");
		`endif
	end

`endif




//=============== Warning Message ==================
`ifdef CLOSE_LOCAL_WARNING
`else
//ack, the latency is too long after req active
reg	[10:0] lat_cnt; //2048
wire lat_cnt_full = (lat_cnt === 11'h7ff);
always @(posedge clk or negedge rst_)
	if (!rst_)
		lat_cnt	<= #udly 11'h0;
	else if (cpu_xxx_ack)
		lat_cnt <= #udly 11'h0;
	else if (cpu_xxx_req && !lat_cnt_full)
		lat_cnt <= #udly lat_cnt + 1;

reg	lat_cnt_full_dly_1;
always @(posedge clk or negedge rst_)
	if (!rst_)
		lat_cnt_full_dly_1 <= #udly 1'b0;
	else
		lat_cnt_full_dly_1 <= #udly lat_cnt_full;

wire lat_cnt_full_pulse = (lat_cnt_full & ~lat_cnt_full_dly_1);
always @(posedge clk)
	if (lat_cnt_full_pulse) begin
		$display("Warning_T2_rsim --> Local bus interface protocol Warning:");
		$display("cpu_%s_ack latency is too long after cpu_%s_req active at %0t",ID,ID,$realtime);
		$display("");
		`ifdef LOCAL_BUS_MONITOR
		$fdisplay(local_moni_msg,"Warning_T2_rsim --> Local bus interface protocol Warning:");
		$fdisplay(local_moni_msg,"cpu_%s_ack latency is too long after cpu_%s_req active at %0t",ID,ID,$realtime);
		$fdisplay(local_moni_msg,"");
		`endif
	end

//be, its value equals to Zero, 4'h0
always @(posedge clk)
	if (rst_ && cpu_xxx_req && (cpu_xxx_be === 4'b0000) ) begin
		$display("Warning_T2_rsim --> Local bus interface protocol Warning:");
		$display("cpu_%s_be value is zero(4'b0000) during request at %0t",ID,$realtime);
		$display("");
		`ifdef LOCAL_BUS_MONITOR
		$fdisplay(local_moni_msg,"Warning_T2_rsim --> Local bus interface protocol Warning:");
		$fdisplay(local_moni_msg,"cpu_%s_be value is zero(4'b0000) during request at %0t",ID,$realtime);
		$fdisplay(local_moni_msg,"");
		`endif
	end

//the request is in uncertain status
always @(posedge clk)
	if (rst_ & (cpu_xxx_req === 1'bx)) begin
		$display("Warning_T2_rsim --> Local bus interface protocol Warning:");
		$display("cpu_%s_req is in uncertain status at %0t",ID,$realtime);
		$display("");
		`ifdef LOCAL_BUS_MONITOR
		$fdisplay(local_moni_msg,"Warning_T2_rsim --> Local bus interface protocol Warning:");
		$fdisplay(local_moni_msg,"cpu_%s_req is in uncertain status at %0t",ID,$realtime);
		$fdisplay(local_moni_msg,"");
		`endif
	end

//the rw is in uncertain status
always @(posedge clk)
	if (rst_ & (cpu_xxx_rw === 1'bx)) begin
		$display("Warning_T2_rsim --> Local bus interface protocol Warning:");
		$display("cpu_%s_rw is in uncertain status at %0t",ID,$realtime);
		$display("");
		`ifdef LOCAL_BUS_MONITOR
		$fdisplay(local_moni_msg,"Warning_T2_rsim --> Local bus interface protocol Warning:");
		$fdisplay(local_moni_msg,"cpu_%s_rw is in uncertain status at %0t",ID,$realtime);
		$fdisplay(local_moni_msg,"");
		`endif
	end

//the addr is in uncertain status
always @(posedge clk)
	if (rst_ & (^cpu_xxx_addr === 1'bx)) begin
		$display("Warning_T2_rsim --> Local bus interface protocol Warning:");
		$display("cpu_%s_addr is in uncertain status at %0t",ID,$realtime);
		$display("");
		`ifdef LOCAL_BUS_MONITOR
		$fdisplay(local_moni_msg,"Warning_T2_rsim --> Local bus interface protocol Warning:");
		$fdisplay(local_moni_msg,"cpu_%s_addr is in uncertain status at %0t",ID,$realtime);
		$fdisplay(local_moni_msg,"");
		`endif
	end

//the ack is in uncertain status
always @(posedge clk)
	if (rst_ & (cpu_xxx_ack === 1'bx)) begin
		$display("Warning_T2_rsim --> Local bus interface protocol Warning:");
		$display("cpu_%s_ack is in uncertain status at %0t",ID,$realtime);
		$display("");
		`ifdef LOCAL_BUS_MONITOR
		$fdisplay(local_moni_msg,"Warning_T2_rsim --> Local bus interface protocol Warning:");
		$fdisplay(local_moni_msg,"cpu_%s_ack is in uncertain status at %0t",ID,$realtime);
		$fdisplay(local_moni_msg,"");
		`endif
	end

//the be is in uncertain status
always @(posedge clk)
	if (rst_ & (^cpu_xxx_be === 1'bx)) begin
		$display("Warning_T2_rsim --> Local bus interface protocol Warning:");
		$display("cpu_%s_be is in uncertain status at %0t",ID,$realtime);
		$display("");
		`ifdef LOCAL_BUS_MONITOR
		$fdisplay(local_moni_msg,"Warning_T2_rsim --> Local bus interface protocol Warning:");
		$fdisplay(local_moni_msg,"cpu_%s_be is in uncertain status at %0t",ID,$realtime);
		$fdisplay(local_moni_msg,"");
		`endif
	end

//the rdata is in uncertain status
wire [7:0] rd_byte0 = cpu_xxx_be[0] ? cpu_xxx_rdata[7:0]	: 8'h0;
wire [7:0] rd_byte1 = cpu_xxx_be[1] ? cpu_xxx_rdata[15:8]	: 8'h0;
wire [7:0] rd_byte2 = cpu_xxx_be[2] ? cpu_xxx_rdata[23:16]	: 8'h0;
wire [7:0] rd_byte3 = cpu_xxx_be[3] ? cpu_xxx_rdata[31:24]	: 8'h0;

always @(posedge clk)
	if (cpu_xxx_ack & ~cpu_xxx_rw &((^rd_byte0 === 1'bx)|(^rd_byte1 === 1'bx)|(^rd_byte2 === 1'bx)|(^rd_byte3 === 1'bx))) begin
		$display("Warning_T2_rsim --> Local bus interface protocol Warning:");
		$display("cpu_%s_rdata is in uncertain status at %0t,addr=%h, rdata=%h",ID,$realtime,cpu_xxx_addr,cpu_xxx_rdata);
		$display("");
		`ifdef LOCAL_BUS_MONITOR
		$fdisplay(local_moni_msg,"Warning_T2_rsim --> Local bus interface protocol Warning:");
		$fdisplay(local_moni_msg,"cpu_%s_rdata is in uncertain status at %0t,addr=%h, rdata=%h",ID,$realtime,cpu_xxx_addr,cpu_xxx_rdata);
		$fdisplay(local_moni_msg,"");
		`endif
	end

//the wdata is in uncertain status
wire [7:0] wr_byte0 = cpu_xxx_be[0] ? cpu_xxx_wdata[7:0]	: 8'h0;
wire [7:0] wr_byte1 = cpu_xxx_be[1] ? cpu_xxx_wdata[15:8]	: 8'h0;
wire [7:0] wr_byte2 = cpu_xxx_be[2] ? cpu_xxx_wdata[23:16]	: 8'h0;
wire [7:0] wr_byte3 = cpu_xxx_be[3] ? cpu_xxx_wdata[31:24]	: 8'h0;

always @(posedge clk)
	if (cpu_xxx_req & cpu_xxx_rw & ((^wr_byte0 === 1'bx)|(^wr_byte1 === 1'bx)|(^wr_byte2 === 1'bx)|(^wr_byte3 === 1'bx))) begin
		$display("Warning_T2_rsim --> Local bus interface protocol Warning:");
		$display("cpu_%s_wdata is in uncertain status at %0t,addr=%h, wdata=%h",ID,$realtime,cpu_xxx_addr,cpu_xxx_wdata);
		$display("");
		`ifdef LOCAL_BUS_MONITOR
		$fdisplay(local_moni_msg,"Warning_T2_rsim --> Local bus interface protocol Warning:");
		$fdisplay(local_moni_msg,"cpu_%s_wdata is in uncertain status at %0t,addr=%h, wdata=%h",ID,$realtime,cpu_xxx_addr,cpu_xxx_wdata);
		$fdisplay(local_moni_msg,"");
		`endif
	end

`endif

endmodule