/******************************************************************************
	  (c) copyrights 2000-. All rights reserved

	   T-Square Design, Inc.

	  Company confidential and Properietary	information.
	  This information may not be disclosed	to unauthorized individual.
*******************************************************************************/

/*******************************************************************************
 *
 *       PROJECT:	M6304 monitor
 *
 *       FILE NAME: performance.v
 *
 *       DESCRIPTION: Dimm Interface
 *
 *       AUTHOR: Joyous Wang
 *
 *       HISTORY:
 *
 *********************************************************************************/
module performance(
           //SDR SDRAM memory interface:
           			dq	,
					addr,
					ba,
					cs_,
					d1_cke,
					d2_cke,
					dqm,
					ras_,
					cas_,
					we_,
					sd_clk0,
					sd_clk1
					);

//SDR SDRAM memory interface:
input	[31:0]	dq;
input	[11:0]	addr;
input	[1:0]	ba;
input	[1:0]	cs_;
input	d1_cke, d2_cke;
input	ras_, cas_, we_, sd_clk0, sd_clk1;
input	[3:0]	dqm;

parameter udly		=	1'b1	;
integer perform_file	;

`ifdef CLOSE_PERFORM
`else

`ifdef OPEN_RPT
initial begin
perform_file = $fopen("perform.rpt");
end
`else
`endif

`ifdef	NEW_MEM
	`ifdef 	SD_GSIM
wire	wdata_avail	=	top.CHIP.CORE.T2_CHIPSET.MEM_INTF.DRAM_CTRL.DRAM_CMD_GEN.ACCE_WR_CYCLE;		
wire	rdata_avail	=	top.CHIP.CORE.T2_CHIPSET.MEM_INTF.DRAM_CTRL.DRAM_CMD_GEN.ACCE_RD_CYCLE;		
	`else
wire	wdata_avail	=	top.CHIP.CORE.T2_CHIPSET.MEM_INTF.dram_ctrl.dram_cmd_gen.acce_wr_cycle;		
wire	rdata_avail	=	top.CHIP.CORE.T2_CHIPSET.MEM_INTF.dram_ctrl.dram_cmd_gen.acce_rd_cycle;		
	`endif
`else
`endif
wire	CPU_req		=	top.CHIP.CORE.T2_CHIPSET.MEM_INTF.CPU_RAM_REQ	;
wire	DSP_req		=	top.CHIP.CORE.T2_CHIPSET.MEM_INTF.DSP_RAM_REQ	;
`ifdef	NEW_MEM
wire	SB_req		=	top.CHIP.CORE.T2_CHIPSET.MEM_INTF.AUD_RAM_REQ	;
wire	SB_ack		=	top.CHIP.CORE.T2_CHIPSET.MEM_INTF.AUD_RAM_ACK	;
wire	DISP_req	=	top.CHIP.CORE.T2_CHIPSET.MEM_INTF.VIDEO_RAM_REQ	;
wire	DISP_ack	=	top.CHIP.CORE.T2_CHIPSET.MEM_INTF.VIDEO_RAM_ACK	;
wire	SERVO_req	=	top.CHIP.CORE.T2_CHIPSET.MEM_INTF.SER_RAM_REQ	; 
wire	SERVO_ack	=	top.CHIP.CORE.T2_CHIPSET.MEM_INTF.SER_RAM_ACK	;  
`else
wire	SB_req		=	top.CHIP.CORE.T2_CHIPSET.MEM_INTF.SB_RAM_REQ	;
wire	SB_ack		=	top.CHIP.CORE.T2_CHIPSET.MEM_INTF.SB_RAM_ACK	;
wire	DISP_req	=	top.CHIP.CORE.T2_CHIPSET.MEM_INTF.DISP_RAM_REQ	;
wire	DISP_ack	=	top.CHIP.CORE.T2_CHIPSET.MEM_INTF.DISP_RAM_ACK	; 
`endif
wire	VSB_req		=	top.CHIP.CORE.T2_CHIPSET.MEM_INTF.VSB_RAM_REQ	;
wire	IDE_req		=	top.CHIP.CORE.T2_CHIPSET.MEM_INTF.IDE_RAM_REQ	;
wire	CPU_ack		=	top.CHIP.CORE.T2_CHIPSET.MEM_INTF.CPU_RAM_ACK	;
wire	DSP_ack		=	top.CHIP.CORE.T2_CHIPSET.MEM_INTF.DSP_RAM_ACK	;
wire	VSB_ack		=	top.CHIP.CORE.T2_CHIPSET.MEM_INTF.VSB_RAM_ACK	;
wire	IDE_ack		=	top.CHIP.CORE.T2_CHIPSET.MEM_INTF.IDE_RAM_ACK	;
wire	PCI_wreq	=	top.CHIP.CORE.T2_CHIPSET.MEM_INTF.PCI_RAM_WREQ	;
wire	PCI_wack	=	top.CHIP.CORE.T2_CHIPSET.MEM_INTF.PCI_RAM_WACK	;
wire	PCI_rreq	=	top.CHIP.CORE.T2_CHIPSET.MEM_INTF.PCI_RAM_RREQ	;
wire	PCI_rack	=	top.CHIP.CORE.T2_CHIPSET.MEM_INTF.PCI_RAM_RACK	;
wire	rst_		=	top.CHIP.CORE.T2_CHIPSET.MEM_INTF.RST_			;

wire	cpu_req 		=	CPU_req &CPU_ack	;	
wire	disp_req        =	DISP_req&DISP_ack	;	
wire	dsp_req         =	DSP_req &DSP_ack	;	
wire	sb_req          =	SB_req	&SB_ack		;	
wire	vsb_req         =	VSB_req &VSB_ack	;	
wire	ide_req	        =	IDE_req &IDE_ack	;
wire	pci_wreq	    =	PCI_wreq &PCI_wack	;	
wire	pci_rreq	    =	PCI_rreq &PCI_rack	;
`ifdef NEW_MEM
wire	svo_req			=	SERVO_req&SERVO_ack	;
`else
`endif

reg	[31:0]	cpu_req_reg		;
reg	[31:0]	disp_req_reg    ;
reg	[31:0]	dsp_req_reg	    ;
reg	[31:0]	sb_req_reg	    ;
reg	[31:0]	vsb_req_reg     ;
reg	[31:0]	ide_req_reg	    ;
reg [31:0]	pci_wreq_reg	;
reg [31:0]	pci_rreq_reg	;
reg	[31:0]	svo_req_reg	    ; 

reg	[31:0]	cpu_req_reg_one		;
reg	[31:0]	disp_req_reg_one    ;
reg	[31:0]	dsp_req_reg_one	    ;
reg	[31:0]	sb_req_reg_one	    ;
reg	[31:0]	vsb_req_reg_one     ;
reg	[31:0]	ide_req_reg_one	    ;
reg [31:0]	pci_wreq_reg_one	;
reg [31:0]	pci_rreq_reg_one	;
reg	[31:0]	svo_req_reg_one	    ; 

reg	[31:0]	cpu_req_max		;
reg	[31:0]	disp_req_max    ;
reg	[31:0]	dsp_req_max	    ;
reg	[31:0]	sb_req_max	    ;
reg	[31:0]	vsb_req_max     ;
reg	[31:0]	ide_req_max	    ;
reg [31:0]	pci_wreq_max	;
reg [31:0]	pci_rreq_max	;
reg	[31:0]	svo_req_max	    ; 

reg	[31:0]	cpu_req_max_cnt		;
reg	[31:0]	disp_req_max_cnt    ;
reg	[31:0]	dsp_req_max_cnt	    ;
reg	[31:0]	sb_req_max_cnt	    ;
reg	[31:0]	vsb_req_max_cnt     ;
reg	[31:0]	ide_req_max_cnt	    ;
reg [31:0]	pci_wreq_max_cnt	;
reg [31:0]	pci_rreq_max_cnt	;
reg	[31:0]	svo_req_max_cnt	    ; 

reg	[31:0]	cpu_num		;
reg	[31:0]	disp_num	;
reg	[31:0]	dsp_num		;
reg	[31:0]	sb_num		;
reg	[31:0]	vsb_num		;
reg	[31:0]	ide_num		;
reg	[31:0]	pci_w_num	;
reg	[31:0]	pci_r_num	;
reg	[31:0]	svo_num	    ; 

reg	[31:0]	clk_num1	;
reg	[31:0]	clk_num2	;

reg	[31:0]	data_avail	;
reg			perfor_avail;
reg			perfor_reg	;

always	@(posedge sd_clk0 or negedge rst_)
	if(!rst_)begin
		cpu_req_reg   	<=	#udly   32'h0	;
		disp_req_reg    <=	#udly   32'h0	;
		dsp_req_reg	    <=	#udly   32'h0	;
		sb_req_reg	    <=	#udly   32'h0	;
		vsb_req_reg    	<=	#udly   32'h0	;
		ide_req_reg	    <=	#udly   32'h0	;
		pci_wreq_reg   	<=	#udly   32'h0	;
		pci_rreq_reg    <=	#udly   32'h0	;
		svo_req_reg	    <=	#udly   32'h0	;
		
		cpu_req_reg_one   	<=	#udly   32'h0	;
		disp_req_reg_one    <=	#udly   32'h0	;
		dsp_req_reg_one    	<=	#udly   32'h0	;
		sb_req_reg_one    	<=	#udly   32'h0	;
		vsb_req_reg_one   	<=	#udly   32'h0	;
		ide_req_reg_one    	<=	#udly   32'h0	;
		pci_wreq_reg_one   	<=	#udly   32'h0	;
		pci_rreq_reg_one    <=	#udly   32'h0	;
		svo_req_reg_one	    <=	#udly   32'h0	;
	end
	else	begin
		if(cpu_req)begin
		cpu_req_reg_one   	<=	#udly   32'h0	;
		cpu_req_reg			<=	#udly	cpu_req_reg  	+ 1'b1	;
		end
		else if(CPU_req)begin
		cpu_req_reg			<=	#udly	cpu_req_reg  	+ 1'b1	;
		cpu_req_reg_one		<=	#udly	cpu_req_reg_one  + 1'b1	;
		end			
		
		if(disp_req)begin
		disp_req_reg_one    <=	#udly   32'h0	;
		disp_req_reg	<=	#udly	disp_req_reg + 1'b1	;
		end	
		else if(DISP_req)begin
		disp_req_reg		<=	#udly	disp_req_reg 	+ 1'b1	;
		disp_req_reg_one	<=	#udly	disp_req_reg_one + 1'b1	;
		end		
		
		if(dsp_req)begin
		dsp_req_reg_one    <=	#udly   32'h0	;
		dsp_req_reg		<=	#udly	dsp_req_reg  + 1'b1	;
		end
		else if(DSP_req)begin
		dsp_req_reg			<=	#udly	dsp_req_reg 	 + 1'b1	;
		dsp_req_reg_one		<=	#udly	dsp_req_reg_one  + 1'b1	;
		end
		
		if(sb_req)begin
		sb_req_reg_one    <=	#udly   32'h0	;
		sb_req_reg			<=	#udly	sb_req_reg   	+ 1'b1	;
		end
		else if(SB_req)begin
		sb_req_reg			<=	#udly	sb_req_reg   	+ 1'b1	;
		sb_req_reg_one		<=	#udly	sb_req_reg_one   + 1'b1	;
		end
		
		if(vsb_req)begin
		vsb_req_reg_one   	<=	#udly   32'h0	;
		vsb_req_reg			<=	#udly	vsb_req_reg  	+ 1'b1	;
		end
		else if(VSB_req)begin
		vsb_req_reg			<=	#udly	vsb_req_reg  	+ 1'b1	;
		vsb_req_reg_one		<=	#udly	vsb_req_reg_one  + 1'b1	;
		end
		
		if(ide_req)begin
		ide_req_reg_one    <=	#udly   32'h0	;
		ide_req_reg			<=	#udly	ide_req_reg  	+ 1'b1	;
		end
		else if(IDE_req)begin
		ide_req_reg			<=	#udly	ide_req_reg  	+ 1'b1	;
		ide_req_reg_one		<=	#udly	ide_req_reg_one  + 1'b1	;
		end
		
		if(pci_wreq)begin
		pci_wreq_reg_one    <=	#udly   32'h0	;
		pci_wreq_reg		<=	#udly	pci_wreq_reg  	+ 1'b1	;
		end
		else if(PCI_wreq)begin
		pci_wreq_reg			<=	#udly	pci_wreq_reg  	+ 1'b1	;
		pci_wreq_reg_one		<=	#udly	pci_wreq_reg_one  + 1'b1	;
		end
		
		if(pci_rreq)begin
		pci_rreq_reg_one    <=	#udly   32'h0	;
		pci_rreq_reg		<=	#udly	pci_rreq_reg  	+ 1'b1	;
		end
		else if(PCI_rreq)begin
		pci_rreq_reg			<=	#udly	pci_rreq_reg  	+ 1'b1	;
		pci_rreq_reg_one		<=	#udly	pci_rreq_reg_one  + 1'b1	;
		end
		
		if(svo_req)begin
		svo_req_reg_one   	<=	#udly   32'h0	;
		svo_req_reg			<=	#udly	svo_req_reg  	+ 1'b1	;
		end
		else if(SERVO_req)begin
		svo_req_reg			<=	#udly	svo_req_reg  	+ 1'b1	;
		svo_req_reg_one		<=	#udly	svo_req_reg_one  + 1'b1	;
		end
	end


always	@(posedge sd_clk0 or negedge rst_)
	if(!rst_)begin
		cpu_num		<=	#udly 	32'h0	;
		disp_num    <=	#udly   32'h0	;
		dsp_num	    <=	#udly   32'h0	;
		sb_num	    <=	#udly   32'h0	;
		vsb_num	    <=	#udly   32'h0	;
		ide_num	    <=	#udly   32'h0	;
		pci_w_num   <=	#udly   32'h0	;
		pci_r_num   <=	#udly   32'h0	;
		svo_num   	<=	#udly   32'h0	;
		
		clk_num1    <=	#udly   32'h0	;
		clk_num2    <=	#udly   32'h0	;
		data_avail	<=	#udly	32'h0	;
		
		cpu_req_max   	<=	#udly   32'h0	;
		disp_req_max    <=	#udly   32'h0	;
		dsp_req_max	    <=	#udly   32'h0	;
		sb_req_max	    <=	#udly   32'h0	;
		vsb_req_max    	<=	#udly   32'h0	;
		ide_req_max	    <=	#udly   32'h0	;
		pci_wreq_max    <=	#udly   32'h0	;
		pci_rreq_max	<=	#udly   32'h0	;
		svo_req_max    	<=	#udly   32'h0	;
	end
	else	begin
		clk_num1	<=	#udly	clk_num1 + 1'b1	;
		`ifdef	NEW_MEM
		if(wdata_avail|rdata_avail)//(dq[15:0]	!==16'h4)
		data_avail	<=	#udly	data_avail + 1'b1	;
		`else
		if(^dq[15:0]!==1'bx)
		data_avail	<=	#udly	data_avail + 1'b1	;
		`endif
		if(cpu_req)begin
		cpu_num		<=	#udly	cpu_num  + 1'b1	;
			if(cpu_req_max<cpu_req_reg_one)begin
			cpu_req_max		<=	#udly	cpu_req_reg_one	;
			cpu_req_max_cnt	<=	#udly	cpu_num			;
			end
		end
		if(disp_req)begin
		disp_num	<=	#udly	disp_num + 1'b1	;
			if(disp_req_max<disp_req_reg_one)begin
			disp_req_max	<=	#udly	disp_req_reg_one;
			disp_req_max_cnt<=	#udly	disp_num		;
			end
		end
		if(dsp_req)begin
		dsp_num		<=	#udly	dsp_num  + 1'b1	;
			if(dsp_req_max<dsp_req_reg_one)begin
			dsp_req_max		<=	#udly	dsp_req_reg_one	;
			dsp_req_max_cnt	<=	#udly	dsp_num			;
			end
		end
		if(sb_req)begin
		sb_num		<=	#udly	sb_num   + 1'b1	;
			if(sb_req_max<sb_req_reg_one)begin
			sb_req_max		<=	#udly	sb_req_reg_one	;
			sb_req_max_cnt	<=	#udly	sb_num			;
			end
		end
		if(vsb_req)begin
		vsb_num		<=	#udly	vsb_num  + 1'b1	;
			if(vsb_req_max<vsb_req_reg_one)begin
			vsb_req_max		<=	#udly	vsb_req_reg_one	;
			vsb_req_max_cnt	<=	#udly	vsb_num			;
			end
		end
		if(ide_req)begin
			ide_num		<=	#udly	ide_num  + 1'b1	;
			if(ide_req_max<ide_req_reg_one)begin
			ide_req_max		<=	#udly	ide_req_reg_one	;
			ide_req_max_cnt	<=	#udly	ide_num			;
			end
		end
		if(pci_wreq)begin
			pci_w_num		<=	#udly	pci_w_num  + 1'b1	;
			if(pci_wreq_max<pci_wreq_reg_one)begin
			pci_wreq_max		<=	#udly	pci_wreq_reg_one	;
			pci_wreq_max_cnt	<=	#udly	pci_w_num			;
			end
		end
		if(pci_rreq)begin
			pci_r_num		<=	#udly	pci_r_num  + 1'b1	;
			if(pci_rreq_max<pci_rreq_reg_one)begin
			pci_rreq_max		<=	#udly	pci_rreq_reg_one	;
			pci_rreq_max_cnt	<=	#udly	pci_r_num			;
			end
		end
		if(svo_req)begin
		svo_num		<=	#udly	svo_num  + 1'b1	;
			if(svo_req_max<svo_req_reg_one)begin
			svo_req_max		<=	#udly	svo_req_reg_one	;
			svo_req_max_cnt	<=	#udly	svo_num			;
			end
		end		
		if(&clk_num1)begin
		clk_num2	<=	#udly	clk_num2 + 1'b1	;
		end
	end

wire	[31:0]	start_num	;
wire	[31:0]	finish_num	;

`ifdef	CPU_REQ_EN
assign	start_num	=	top.CPU_START		;
assign	finish_num	=	top.CPU_FINISH		;
	always	@(posedge sd_clk0 or negedge rst_)
	if(!rst_)begin
		perfor_avail	<=	#udly	1'b0	;
	end
	else if(start_num==cpu_num)begin
		perfor_avail	<=	#udly	1'b1	;
	end
	else if(finish_num==cpu_num)begin
		perfor_avail	<=	#udly	1'b0	;
	end

`else
`endif

`ifdef	DISP_REQ_EN
assign	start_num	=	top.DISP_START	;
assign	finish_num	=	top.DISP_FINISH	;
	always	@(posedge sd_clk0 or negedge rst_)
	if(!rst_)begin
		perfor_avail	<=	#udly	1'b0	;
	end
	else if(start_num==disp_num)begin
		perfor_avail	<=	#udly	1'b1	;
	end
	else if(finish_num==disp_num)begin
		perfor_avail	<=	#udly	1'b0	;
	end

`else
`endif

`ifdef	DSP_REQ_EN
assign	start_num	=	top.DSP_START	;
assign	finish_num	=	top.DSP_FINISH	;
	always	@(posedge sd_clk0 or negedge rst_)
	if(!rst_)begin
		perfor_avail	<=	#udly	1'b0	;
	end
	else if(start_num==dsp_num)begin
		perfor_avail	<=	#udly	1'b1	;
	end
	else if(finish_num==dsp_num)begin
		perfor_avail	<=	#udly	1'b0	;
	end

`else
`endif

`ifdef	SB_REQ_EN
assign	start_num	=	top.SB_START	;
assign	finish_num	=	top.SB_FINISH	;
	always	@(posedge sd_clk0 or negedge rst_)
	if(!rst_)begin
		perfor_avail	<=	#udly	1'b0	;
	end
	else if(start_num==sb_num)begin
		perfor_avail	<=	#udly	1'b1	;
	end
	else if(finish_num==sb_num)begin
		perfor_avail	<=	#udly	1'b0	;
	end

`else
`endif

`ifdef	VSB_REQ_EN
assign	start_num	=	top.VSB_START	;
assign	finish_num	=	top.VSB_FINISH	;
	always	@(posedge sd_clk0 or negedge rst_)
	if(!rst_)begin
		perfor_avail	<=	#udly	1'b0	;
	end
	else if(start_num==vsb_num)begin
		perfor_avail	<=	#udly	1'b1	;
	end
	else if(finish_num==vsb_num)begin
		perfor_avail	<=	#udly	1'b0	;
	end

`else
`endif

`ifdef	IDE_REQ_EN
assign	start_num	=	top.IDE_START	;
assign	finish_num	=	top.IDE_FINISH	;
	always	@(posedge sd_clk0 or negedge rst_)
	if(!rst_)begin
		perfor_avail	<=	#udly	1'b0	;
	end
	else if(start_num==ide_num)begin
		perfor_avail	<=	#udly	1'b1	;
	end
	else if(finish_num==ide_num)begin
		perfor_avail	<=	#udly	1'b0	;
	end
`else
`endif

`ifdef	MEM_CLK_EN
assign	start_num	=	top.MEM_START	;
assign	finish_num	=	top.MEM_FINISH	;
always	@(posedge sd_clk0 or negedge rst_)
	if(!rst_)begin
		perfor_avail	<=	#udly	1'b0	;
	end
	else if(start_num==clk_num1)begin
		perfor_avail	<=	#udly	1'b1	;
	end
	else if(finish_num==clk_num1)begin
		perfor_avail	<=	#udly	1'b0	;
	end
`else
`endif

`ifdef	CONTINUOUS_RPT
reg	[31:0]	contin_num		;
reg	[31:0]	num_dimmy		;
reg			perfor_avail_dimmy	;
always	@(posedge sd_clk0 or negedge rst_)
	if(!rst_)begin
		contin_num	<=	#udly	32'b0			;
		end
	else if(perfor_avail)begin
		contin_num	<=	#udly	contin_num + 1'b1;
	end

always	@(posedge sd_clk0 or negedge rst_)
	if(!rst_)begin
		perfor_avail_dimmy	<=	#udly	1'b0;
		end
	else if(perfor_avail)begin
		perfor_avail_dimmy	<=	#udly	1'b0;
	end
	else if(num_dimmy==contin_num)begin
		perfor_avail_dimmy	<=	#udly	1'b0;
	end
	else if(!perfor_avail&(num_dimmy!==contin_num))begin
		perfor_avail_dimmy	<=	#udly	1'b1;
	end

always	@(posedge sd_clk0 or negedge rst_)
	if(!rst_)begin
		num_dimmy	<=	#udly	32'b0			;
		end
	else if(num_dimmy==contin_num)begin
		num_dimmy	<=	#udly	32'b0;
	end
	else if(perfor_avail_dimmy)begin
		num_dimmy	<=	#udly	num_dimmy + 1'b1;
	end

always	@(posedge sd_clk0 or negedge rst_)
	if(!rst_)begin
		perfor_reg	<=	#udly	1'b0			;
		end
	else
		perfor_reg	<=	#udly	(perfor_avail|perfor_avail_dimmy);
		
wire	cnt_start	=	(perfor_avail|perfor_avail_dimmy)&&!perfor_reg	;
wire	cnt_over	=	!(perfor_avail|perfor_avail_dimmy)&&perfor_reg	;
	
`else

always	@(posedge sd_clk0 or negedge rst_)
	if(!rst_)begin
		perfor_reg	<=	#udly	1'b0			;
		end
	else
		perfor_reg	<=	#udly	perfor_avail	;
wire	cnt_start	=	perfor_avail&&!perfor_reg	;
wire	cnt_over	=	!perfor_avail&&perfor_reg	;		
`endif	


reg	[31:0]	clk_cnt1	;
reg	[31:0]	clk_cnt2	;
reg	[31:0]	data_cnt1	;
reg	[31:0]	data_cnt2	;
always	@(posedge sd_clk0 or negedge rst_)
	    if(!rst_) begin
			clk_cnt1	<=	32'h0		;
			data_cnt1	<=	32'h0		;
		end
		else if	(cnt_start)begin
			clk_cnt1	<=	clk_num1	;
			data_cnt1	<=	data_avail	;
		end
always	@(posedge sd_clk0 or negedge rst_)
	    if(!rst_) begin
			clk_cnt2	<=	32'h0		;
			data_cnt2	<=	32'h0		;
		end
		else if(cnt_over)begin
			clk_cnt2	<=	clk_num1	;
			data_cnt2	<=	data_avail	;
		end


reg	[31:0]	cpu_req_cnt1	;
reg	[31:0]	cpu_req_cnt2	;
reg	[31:0]	cpu_ack_cnt1	;
reg	[31:0]	cpu_ack_cnt2	;

reg	[31:0]	disp_req_cnt1	;
reg	[31:0]	disp_req_cnt2	;
reg	[31:0]	disp_ack_cnt1	;
reg	[31:0]	disp_ack_cnt2	;

reg	[31:0]	dsp_req_cnt1	;
reg	[31:0]	dsp_req_cnt2	;
reg	[31:0]	dsp_ack_cnt1	;
reg	[31:0]	dsp_ack_cnt2	;

reg	[31:0]	vsb_req_cnt1	;
reg	[31:0]	vsb_req_cnt2	;
reg	[31:0]	vsb_ack_cnt1	;
reg	[31:0]	vsb_ack_cnt2	;

reg	[31:0]	sb_req_cnt1	;
reg	[31:0]	sb_req_cnt2	;
reg	[31:0]	sb_ack_cnt1	;
reg	[31:0]	sb_ack_cnt2	;

reg	[31:0]	ide_req_cnt1	;
reg	[31:0]	ide_req_cnt2	;
reg	[31:0]	ide_ack_cnt1	;
reg	[31:0]	ide_ack_cnt2	;

reg	[31:0]	pci_wreq_cnt1	;
reg	[31:0]	pci_wreq_cnt2	;
reg	[31:0]	pci_wack_cnt1	;
reg	[31:0]	pci_wack_cnt2	;

reg	[31:0]	pci_rreq_cnt1	;
reg	[31:0]	pci_rreq_cnt2	;
reg	[31:0]	pci_rack_cnt1	;
reg	[31:0]	pci_rack_cnt2	;

reg	[31:0]	svo_req_cnt1	;
reg	[31:0]	svo_req_cnt2	;
reg	[31:0]	svo_ack_cnt1	;
reg	[31:0]	svo_ack_cnt2	;

always	@(posedge sd_clk0 or negedge rst_)
	    if(!rst_) begin
			cpu_req_cnt1	<=	32'h0		;
			cpu_ack_cnt1	<=	32'h0		;
			disp_req_cnt1	<=	32'h0		;
			disp_ack_cnt1	<=	32'h0		;
			dsp_req_cnt1	<=	32'h0		;
			dsp_ack_cnt1	<=	32'h0		;
			vsb_req_cnt1	<=	32'h0		;
			vsb_ack_cnt1	<=	32'h0		;
			sb_req_cnt1		<=	32'h0		;
			sb_ack_cnt1		<=	32'h0		;
			ide_req_cnt1	<=	32'h0		;
			ide_ack_cnt1	<=	32'h0		;
			pci_wreq_cnt1	<=	32'h0		;
			pci_wack_cnt1	<=	32'h0		;
			pci_rreq_cnt1	<=	32'h0		;
			pci_rack_cnt1	<=	32'h0		;
			svo_req_cnt1	<=	32'h0		;
			svo_ack_cnt1	<=	32'h0		;
		end
		else if	(cnt_start)begin
			cpu_req_cnt1	<=	cpu_req_reg	;
			cpu_ack_cnt1	<=	cpu_num		;
			disp_req_cnt1	<=	disp_req_reg;	
			disp_ack_cnt1	<=	disp_num	;	
			dsp_req_cnt1	<=	dsp_req_reg	;
			dsp_ack_cnt1	<=	dsp_num		;
			vsb_req_cnt1	<=	vsb_req_reg	;
			vsb_ack_cnt1	<=	vsb_num		;
			sb_req_cnt1		<=	sb_req_reg	;
			sb_ack_cnt1		<=	sb_num		;
			ide_req_cnt1	<=	ide_req_reg	;
			ide_ack_cnt1	<=	ide_num		;
			pci_wreq_cnt1	<=	pci_wreq_reg;
			pci_wack_cnt1	<=	pci_w_num	;
			pci_rreq_cnt1	<=	pci_rreq_reg;
			pci_rack_cnt1	<=	pci_r_num	;
			svo_req_cnt1	<=	svo_req_reg	;
			svo_ack_cnt1	<=	svo_num		;
		end
always	@(posedge sd_clk0 or negedge rst_)
	    if(!rst_) begin
			cpu_req_cnt2	<=	32'h0		;
			cpu_ack_cnt2	<=	32'h0		;
			disp_req_cnt2	<=	32'h0		;
			disp_ack_cnt2	<=	32'h0		;
			dsp_req_cnt2	<=	32'h0		;
			dsp_ack_cnt2	<=	32'h0		;
			vsb_req_cnt2	<=	32'h0		;
			vsb_ack_cnt2	<=	32'h0		;
			sb_req_cnt2		<=	32'h0		;
			sb_ack_cnt2		<=	32'h0		;
			ide_req_cnt2	<=	32'h0		;
			ide_ack_cnt2	<=	32'h0		;
			pci_wreq_cnt2	<=	32'h0		;
			pci_wack_cnt2	<=	32'h0		;
			pci_rreq_cnt2	<=	32'h0		;
			pci_rack_cnt2	<=	32'h0		;
			svo_req_cnt2	<=	32'h0		;
			svo_ack_cnt2	<=	32'h0		;
		end
		else if(cnt_over)begin
			cpu_req_cnt2	<=	cpu_req_reg	;
			cpu_ack_cnt2	<=	cpu_num		;
			disp_req_cnt2	<=	disp_req_reg;	
			disp_ack_cnt2	<=	disp_num	;	
			dsp_req_cnt2	<=	dsp_req_reg	;
			dsp_ack_cnt2	<=	dsp_num		;
			vsb_req_cnt2	<=	vsb_req_reg	;
			vsb_ack_cnt2	<=	vsb_num		;
			sb_req_cnt2		<=	sb_req_reg	;
			sb_ack_cnt2		<=	sb_num		;
			ide_req_cnt2	<=	ide_req_reg	;
			ide_ack_cnt2	<=	ide_num		;
			pci_wreq_cnt2	<=	pci_wreq_reg;
			pci_wack_cnt2	<=	pci_w_num	;
			pci_rreq_cnt2	<=	pci_rreq_reg;
			pci_rack_cnt2	<=	pci_r_num	;
			svo_req_cnt2	<=	svo_req_reg	;
			svo_ack_cnt2	<=	svo_num		;
		end

//-----------------------------precharge and col_address total----------------//
wire	precharge_cmd 	= 	!cs_[0] &!ras_ &cas_ &!we_	;
wire	col_addr_cmd	=	!cs_[0] &ras_ &!cas_		;
reg	[31:0]		pre_cmd_cnt	;
reg	[31:0] 		col_cmd_cnt	;
always	@(posedge sd_clk0 or negedge rst_)
	if(!rst_)begin
		pre_cmd_cnt		<=	#udly	32'b0	;
		col_cmd_cnt		<=	#udly	32'b0	;
	end
	else if(cnt_start)begin
		pre_cmd_cnt		<=	#udly	32'b0	;
		col_cmd_cnt		<=	#udly	32'b0	;
	end
	else begin
		if(precharge_cmd)begin
		pre_cmd_cnt	<=	#udly	pre_cmd_cnt	+	1'b1	;
		end
		if(col_addr_cmd)begin
		col_cmd_cnt	<=	#udly	col_cmd_cnt	+	1'b1	;
		end
	end
	
`ifdef	DISP_ADDR	
reg		precharge_cmd_0		;  
wire	active_cmd_0	=	col_addr_cmd	&   (ba==2'b00)	;
reg		precharge_cmd_1	;
wire	active_cmd_1	=	col_addr_cmd	&   (ba==2'b01)	;
reg		precharge_cmd_2	;
wire	active_cmd_2	=	col_addr_cmd	&   (ba==2'b10)	;
reg		precharge_cmd_3	;
wire	active_cmd_3	=	col_addr_cmd	&   (ba==2'b11)	;
always	@(posedge sd_clk0 or negedge rst_)
	if(!rst_)begin
		precharge_cmd_0		<=	#udly	1'b0	;
		precharge_cmd_1		<=	#udly	1'b0	;
		precharge_cmd_2		<=	#udly	1'b0	;
		precharge_cmd_3		<=	#udly	1'b0	;
	end
	else	begin 
	if(precharge_cmd 	&	(ba==2'b00))begin
		precharge_cmd_0		<=	#udly	1'b1	;
	end
	else if(active_cmd_0)begin
		precharge_cmd_0		<=	#udly	1'b0	;
	end
	if(precharge_cmd 	&	(ba==2'b01))begin
		precharge_cmd_1		<=	#udly	1'b1	;
	end
	else if(active_cmd_1)begin
		precharge_cmd_1		<=	#udly	1'b0	;
	end
	if(precharge_cmd 	&	(ba==2'b10))begin
		precharge_cmd_2		<=	#udly	1'b1	;
	end
	else if(active_cmd_2)begin
		precharge_cmd_2		<=	#udly	1'b0	;
	end
	if(precharge_cmd 	&	(ba==2'b11))begin
		precharge_cmd_3		<=	#udly	1'b1	;
	end
	else if(active_cmd_3)begin
		precharge_cmd_3		<=	#udly	1'b0	;
	end
end
wire	pre_act	=	(precharge_cmd_0	&active_cmd_0)	|	
                    (precharge_cmd_1	&active_cmd_1)	|
                    (precharge_cmd_2	&active_cmd_2)	|
     				(precharge_cmd_3	&active_cmd_3)	;
                    
always	@(negedge pre_act) 
		if	(rst_==1'b1)begin
		$display("From ba_addr %h (H) row_addr %h (H)  \n",ba,addr);
		$fdisplay(perform_file,"From ba_addr %h (H) row_addr %h (H)  \n",ba,addr);
		end	
`else
`endif

`ifdef	NO_PERFORMANCE_DISP
`else
`ifdef	SDRAM_DQ_Mode
always	@(negedge cnt_over)
		if	(rst_==1'b1)begin
		$display("From mem_clk %h (H) to %h (H) performance is %d  percent \n",clk_cnt1,clk_cnt2,{(data_cnt2-data_cnt1)*100/(clk_cnt2-clk_cnt1)});
		$fdisplay(perform_file,"From mem_clk %h (H) to %h (H) performance is %d  percent \n",clk_cnt1,clk_cnt2,{(data_cnt2-data_cnt1)*100/(clk_cnt2-clk_cnt1)});
		end
`else
always	@(negedge cnt_over)
		if	(rst_==1'b1)begin
		$display("From mem_clk %h (H) to %h (H) performance is %d  percent \n",clk_cnt1,clk_cnt2,{(data_cnt2-data_cnt1)*100/(clk_cnt2-clk_cnt1)});
		$fdisplay(perform_file,"From mem_clk %h (H) to %h (H) performance is %d  percent \n",clk_cnt1,clk_cnt2,{(data_cnt2-data_cnt1)*100/(clk_cnt2-clk_cnt1)});
		end
`endif
always	@(negedge cnt_over)
		if	(rst_==1'b1)begin
		$display("From mem_clk %h (H) to %h (H) pre_cmd_cnt is %h (H) re_wr_cnt is %h (H) , pre_percent is %d (D) \n \n",
		clk_cnt1,clk_cnt2,pre_cmd_cnt,col_cmd_cnt,{pre_cmd_cnt*100/col_cmd_cnt});
		$fdisplay(perform_file,"From mem_clk %h (H) to %h (H) pre_cmd_cnt is %h (H) re_wr_cnt is %h (H) , pre_percent is %d (D) \n \n",
		clk_cnt1,clk_cnt2,pre_cmd_cnt,col_cmd_cnt,{pre_cmd_cnt*100/col_cmd_cnt});
		end
`endif

`ifdef	NO_LATENCY_DISP
`else
always	@(negedge cnt_over)
		if	(rst_==1'b1)begin
		$display("From mem_clk %h to %h cpu_req  average latency: %h cycle(H), max_latency: %h cycle(H), cpu_num=%h(H) \n",
		clk_cnt1,clk_cnt2,{(cpu_req_cnt2-cpu_req_cnt1)/(cpu_ack_cnt2-cpu_ack_cnt1)},cpu_req_max,cpu_req_max_cnt);
		$fdisplay(perform_file,"From mem_clk %h to %h cpu_req  average latency: %h cycle(H),max_latency: %h cycle(H), cpu_num=%h(H) \n",
		clk_cnt1,clk_cnt2,{(cpu_req_cnt2-cpu_req_cnt1)/(cpu_ack_cnt2-cpu_ack_cnt1)},cpu_req_max,cpu_req_max_cnt);
		end
always	@(negedge cnt_over)
		if	(rst_==1'b1)begin
		$display("From mem_clk %h to %h dsp_req  average latency: %h cycle(H), max_latency: %h cycle(H), dsp_num=%h(H) \n",
		clk_cnt1,clk_cnt2,{(dsp_req_cnt2-dsp_req_cnt1)/(dsp_ack_cnt2-dsp_ack_cnt1)},dsp_req_max,dsp_req_max_cnt);
		$fdisplay(perform_file,"From mem_clk %h to %h dsp_req  average latency: %h cycle(H),max_latency: %h cycle(H), dsp_num=%h(H) \n",
		clk_cnt1,clk_cnt2,{(dsp_req_cnt2-dsp_req_cnt1)/(dsp_ack_cnt2-dsp_ack_cnt1)},dsp_req_max,dsp_req_max_cnt);
		end
always	@(negedge cnt_over)
		if	(rst_==1'b1)begin
		$display("From mem_clk %h to %h vsb_req  average latency: %h cycle(H), max_latency: %h cycle(H), vsb_num=%h(H) \n",
		clk_cnt1,clk_cnt2, {(vsb_req_cnt2-vsb_req_cnt1)/(vsb_ack_cnt2-vsb_ack_cnt1)},vsb_req_max,vsb_req_max_cnt);
		$fdisplay(perform_file,"From mem_clk %h to %h vsb_req  average latency: %h cycle(H),max_latency: %h cycle(H), vsb_num=%h(H) \n",
		clk_cnt1,clk_cnt2, {(vsb_req_cnt2-vsb_req_cnt1)/(vsb_ack_cnt2-vsb_ack_cnt1)},vsb_req_max,vsb_req_max_cnt);
		
		end
always	@(negedge cnt_over)
		if	(rst_==1'b1)begin
		$display("From mem_clk %h to %h ide_req  average latency: %h cycle(H), max_latency: %h cycle(H), ide_num=%h(H) \n",
		clk_cnt1,clk_cnt2,{(ide_req_cnt2-ide_req_cnt1)/(ide_ack_cnt2-ide_ack_cnt1)},ide_req_max,ide_req_max_cnt);
		$fdisplay(perform_file,"From mem_clk %h to %h ide_req  average latency: %h cycle(H),max_latency: %h cycle(H), ide_num=%h(H) \n",
		clk_cnt1,clk_cnt2,{(ide_req_cnt2-ide_req_cnt1)/(ide_ack_cnt2-ide_ack_cnt1)},ide_req_max,ide_req_max_cnt);
		end
always	@(negedge cnt_over)
		if	(rst_==1'b1)begin
		$display("From mem_clk %h to %h pci_wreq average latency: %h cycle(H), max_latency: %h cycle(H), pci_w_num=%h(H) \n",
		clk_cnt1,clk_cnt2,{(pci_wreq_cnt2-pci_wreq_cnt1)/(pci_wack_cnt2-pci_wack_cnt1)},pci_wreq_max,pci_wreq_max_cnt);
        $fdisplay(perform_file,"From mem_clk %h to %h pci_wreq average latency: %h cycle(H),max_latency: %h cycle(H), pci_w_num=%h(H) \n",
		clk_cnt1,clk_cnt2,{(pci_wreq_cnt2-pci_wreq_cnt1)/(pci_wack_cnt2-pci_wack_cnt1)},pci_wreq_max,pci_wreq_max_cnt);
                                                                                                                      
        end                                                                                                           
always	@(negedge cnt_over)                                                                                           
		if	(rst_==1'b1)begin                                                                                         
		$display("From mem_clk %h to %h pci_rreq average latency: %h cycle(H), max_latency: %h cycle(H), pci_r_num=%h(H) \n",
		clk_cnt1,clk_cnt2,{(pci_rreq_cnt2-pci_rreq_cnt1)/(pci_rack_cnt2-pci_rack_cnt1)},pci_rreq_max,pci_rreq_max_cnt);
		$fdisplay(perform_file,"From mem_clk %h to %h pci_rreq average latency: %h cycle(H),max_latency: %h cycle(H), pci_r_num=%h(H) \n",
		clk_cnt1,clk_cnt2,{(pci_rreq_cnt2-pci_rreq_cnt1)/(pci_rack_cnt2-pci_rack_cnt1)},pci_rreq_max,pci_rreq_max_cnt);
	                                                                                                                  
		end                                                                                                           
	`ifdef	NEW_MEM                                                                                                   
always	@(negedge cnt_over)                                                                                           
		if	(rst_==1'b1)begin                                                                                         
		$display("From mem_clk %h to %h aud_req  average latency: %h cycle(H), max_latency: %h cycle(H), aud_num=%h(H) \n",
		clk_cnt1,clk_cnt2,{(sb_req_cnt2-sb_req_cnt1)/(sb_ack_cnt2-cpu_ack_cnt1)},sb_req_max,sb_req_max_cnt);          
		$fdisplay(perform_file,"From mem_clk %h to %h aud_req  average latency: %h cycle(H),max_latency: %h cycle(H), aud_num=%h(H) \n",
		clk_cnt1,clk_cnt2,{(sb_req_cnt2-sb_req_cnt1)/(sb_ack_cnt2-cpu_ack_cnt1)},sb_req_max,sb_req_max_cnt);          
	                                                                                                                  
		end                                                                                                           
always	@(negedge cnt_over)                                                                                           
		if	(rst_==1'b1)begin                                                                                         
		$display("From mem_clk %h to %h vido_req average latency: %h cycle(H), max_latency: %h cycle(H), video_num=%h (H)\n",
		clk_cnt1,clk_cnt2,{(disp_req_cnt2-disp_req_cnt1)/(disp_ack_cnt2-disp_ack_cnt1)},disp_req_max,disp_req_max_cnt);
		$fdisplay(perform_file,"From mem_clk %h to %h vido_req average latency: %h cycle(H),max_latency: %h cycle(H), video_num=%h (H)\n",
		clk_cnt1,clk_cnt2,{(disp_req_cnt2-disp_req_cnt1)/(disp_ack_cnt2-disp_ack_cnt1)},disp_req_max,disp_req_max_cnt);
	                                                                                                                  
		end                                                                                                           
always	@(negedge cnt_over)                                                                                           
		if	(rst_==1'b1)begin                                                                                         
		$display("From mem_clk %h to %h sevo_req average latency: %h cycle(H), max_latency: %h cycle(H), servo_num=%h (H)\n",
		clk_cnt1,clk_cnt2,{(svo_req_cnt2-svo_req_cnt1)/(svo_ack_cnt2-svo_ack_cnt1)},svo_req_max,svo_req_max_cnt);     
		$fdisplay(perform_file,"From mem_clk %h to %h sevo_req average latency: %h cycle(H),max_latency: %h cycle(H), servo_num=%h (H)\n",
		clk_cnt1,clk_cnt2,{(svo_req_cnt2-svo_req_cnt1)/(svo_ack_cnt2-svo_ack_cnt1)},svo_req_max,svo_req_max_cnt);
	
		end
`else
`endif		
`endif

`endif 
endmodule

