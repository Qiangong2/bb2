// for test EEPROM controller
// 98-07-31
// snow, ver 0.1

module	boot_rom       (
			eeprom_addr,
			eeprom_data,
			eeprom_cs_,
			eeprom_oe_,
			eeprom_we_
			);
parameter	rom_addr_width = 21,
		oe_dly = 1,
		we_dly = 1;

inout	[7:0]	eeprom_data;

input	[rom_addr_width - 1 :0]	eeprom_addr;
input		eeprom_cs_;
input		eeprom_oe_;
input		eeprom_we_;


reg	[7:0]	rom_reg	[('h1 << rom_addr_width) - 1:0];	// for temparory test
//initial thr rom_reg to ZERO for run the program
//yuky,2001-12-14
integer	i_tmp;
initial begin
for (i_tmp = 32'h0_0000; i_tmp < ('h1 << boot_rom.rom_addr_width); i_tmp = i_tmp + 1)
	rom_reg [i_tmp] = 8'h0;
end
wire	[7:0]	rom_data_temp = rom_reg[eeprom_addr];
wire	[7:0]	dly_data;
assign	#1 dly_data = eeprom_data;
assign  #oe_dly eeprom_data = (eeprom_cs_ | eeprom_oe_ ) ? 8'hz : rom_data_temp;

//reg	[7:0]	test;
wire	valid_we_ = eeprom_cs_ | eeprom_we_;
always	@(posedge valid_we_)
	//if (!eeprom_cs_)	begin
		rom_reg[eeprom_addr] <= #we_dly dly_data;//eeprom_data;
//		test <= #we_dly eeprom_data;
	//end

/****
integer i;
integer j;

initial begin


for (i=0;i<='hff;i=i+1) begin
	for (j=i;j<='h3f_ffff;j=j+'h100) begin
   		rom_reg[j] = i;
 	end
  end
end
*******/
integer			cpu_rdata;
initial         cpu_rdata = $fopen("cpu_rdata.rpt");
//==================== Error Report For test =========================
parameter	Exp_data_addr	=	24'hff_ffec;
parameter	Err_data_addr	=	24'hff_ffe8;
parameter	Err_addr_addr	=	24'hff_ffe4;
parameter	Err_type_addr	=	24'hff_ffe0;
wire		err_rpt_hit	=	eeprom_addr == Err_type_addr;
wire	[31:0]	exp_data_info	=	{rom_reg[Exp_data_addr+3],rom_reg[Exp_data_addr+2],
					rom_reg[Exp_data_addr+1],rom_reg[Exp_data_addr]};

wire	[31:0]	err_data_info	=	{rom_reg[Err_data_addr+3],rom_reg[Err_data_addr+2],
					rom_reg[Err_data_addr+1],rom_reg[Err_data_addr]};

wire	[31:0]	err_addr_info	=	{rom_reg[Err_addr_addr+3],rom_reg[Err_addr_addr+2],
					rom_reg[Err_addr_addr+1],rom_reg[Err_addr_addr]};

wire	[31:0]	err_type_info	=	{rom_reg[Err_type_addr+3],rom_reg[Err_type_addr+2],
					rom_reg[Err_type_addr+1],dly_data};

always	@(posedge valid_we_)
	if (err_rpt_hit)	begin
		case (err_type_info)
		32'h100:	$display("Error_T2_rsim: SDRAM Read ERROR  \tWrong Address=%h,ReadData=%h,Expected=%h",
				err_addr_info,err_data_info,exp_data_info);
		32'h101:	$display("Error_T2_rsim: PCI33IO Read ERROR\tWrong Address=%h,ReadData=%h,Expected=%h",
				err_addr_info,err_data_info,exp_data_info);
		32'h102:	$display("Error_T2_rsim: PCI33MEM Read ERROR\tWrong Address=%h,ReadData=%h,Expected=%h",
				err_addr_info,err_data_info,exp_data_info);
		32'h105:	$display("Error_T2_rsim: IO Read ERROR      \tWrong Address=%h,ReadData=%h,Expected=%h",
				err_addr_info,err_data_info,exp_data_info);
		32'h108:	$display("Error_T2_rsim: Local IO Read Error\tWrong Address=%h, ReadData=%h,Expected=%h",
				err_addr_info,err_data_info,exp_data_info);
		32'h109:	$display("Error_T2_rsim: Config Read ERROR  \tWrong Address=%h, ReadData=%h,Expected=%h",
				err_addr_info,err_data_info,exp_data_info);
		32'h110:	$display("Error_T2_rsim: EEPROM Read ERROR  \tWrong Address=%h, ReadData=%h,Expected=%h",
				err_addr_info,err_data_info,exp_data_info);
		32'h111:	$display("Error_T2_rsim: GPU IO ERROR  \tWrong Address=%h, ReadData=%h,Expected=%h",
				err_addr_info,err_data_info,exp_data_info);
		32'h112:	$display("Error_T2_rsim: IDCT IO ERROR  \tWrong Address=%h, ReadData=%h,Expected=%h",
				err_addr_info,err_data_info,exp_data_info);
		32'h113:	$display("Error_T2_rsim: SB IO ERROR  \tWrong Address=%h, ReadData=%h,Expected=%h",
				err_addr_info,err_data_info,exp_data_info);
		32'h8000_0001:	$display("External Control trigger 1");
		32'h8000_0002:	$display("External Control trigger 2");
		32'h8000_0003:	$display("External Control trigger 3");
		32'h8000_0004:	$display("External Control trigger 4");
		32'h8000_0005:	$display("External Control trigger 5");
		32'h8000_0006:	$display("External Control trigger 6");
		32'h8000_0007:	$display("External Control trigger 7");
		32'h8000_0008:	$display("External Control trigger 8");
		32'h8000_0009:	$display("External Control trigger 9");
		32'h8000_000a:	$display("External Control trigger 10");
		32'h8000_000b:	$display("External Control trigger 11");
		32'h8000_000c:	$display("External Control trigger 12");
		32'h8000_000d:	$display("External Control trigger 13");
		32'h8000_000e:	$display("External Control trigger 14");
		32'h8000_000f:	$display("External Control trigger 15");
		32'h8000_0011:	$display("External Control trigger 16");
		32'h8000_0012:	$display("External Control trigger 17");
		32'h8000_0013:	$display("External Control trigger 18");
		32'h8000_0014:	$display("External Control trigger 19");
		32'h8000_0015:	$display("External Control trigger 20");
		32'h8000_0016:	$display("External Control trigger 21");
		32'h8000_0017:	$display("External Control trigger 22");
		32'h8000_0018:	$display("External Control trigger 23");
		32'h8000_0019:	$display("External Control trigger 24");
		32'h8000_001a:	$display("External Control trigger 25");
		32'h8000_001b:	$display("External Control trigger 26");
		32'h8000_001c:	$display("External Control trigger 27");
		32'h8000_001d:	$display("External Control trigger 28");
		32'h8000_001e:	$display("External Control trigger 29");
		32'h8000_001f:	$display("External Control trigger 30");
		32'h4352_5354:	$display("External Cold Reset Trigger!!!"); //CRST--ascii:0x43525354
		32'hffff_ffff:	$display("Trigger the rsim pattern");
		default:	begin
					$display("Error_T2_rsim: an unsupported error type %h issued!Wrong Address=%h,ReadData=%h,Expected=%h",
					err_type_info,err_addr_info,err_data_info,exp_data_info);
					$fdisplay(cpu_rdata,"Addr= %h , Data0= %h , Data1= %h, Data2= %h",
							err_type_info,err_addr_info,err_data_info,exp_data_info);
					end
		endcase
	end
//====================================================================

//============= Report the data flow when use CPU netlist ============
wire	read_en	=	eeprom_cs_ | eeprom_oe_	;
`ifdef CPU_IR
`ifdef NO_CPU
`else
	`ifdef	GSIM
	always @(posedge read_en)
		$display("PC_counter = %h, ReadData=%h",
				{8'h1f,eeprom_addr},{rom_reg[eeprom_addr + 3],rom_reg[eeprom_addr + 2],
				rom_reg[eeprom_addr + 1],rom_reg[eeprom_addr]});
	`endif
`endif
`endif
//====================================================================

endmodule
