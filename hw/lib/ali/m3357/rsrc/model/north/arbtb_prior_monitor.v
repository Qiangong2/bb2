/******************************************************************************
	  (c) copyrights 2000-. All rights reserved
 
	   T-Square Design, Inc.
 
	  Company confidential and Properietary	information.
	  This information may not be disclosed	to unauthorized individual.	  
*******************************************************************************/

/*******************************************************************************
 *
 *       PROJECT:M6304
 *
 *       FILE NAME: arbtb_prior_monitor.v
 *
 *       DESCRIPTION: SDRAM Memory Bus SUB-B Arbitor Priority Monitor
 *						Three source devices:	IDE, SB, VSB
 *
 *					Priority Mode:
 *								00: Rotation:
 *								IDE -> SB -> VSB
 *								01: Fixed_1st:
 *								IDE >> SB >> VSB
 *								10: Fixed_2nd:
 *								VSB >> IDE >> SB
 *								11: Fixed_3rd:
 *								IDE >> VSB >> SB
 *
 *       AUTHOR: Figo OU
 *
 *       HISTORY:   10/31/02         initial version
 *      
 *********************************************************************************/

module	arbtb_prior_monitor(
	//Configure Ports:
	arbtb_prior_mode,
	//Devices Ports:
	//IDE:
	ide_req,
	ide_ack,
	
	//SB:
	sb_req,
	sb_ack,
	//VSB:
	vsb_req,
	vsb_ack,
	
	//For arbtm_prior_monitor:
	arbtb_req,
	arbtb_ack,
	//Others:
	rst_,
	mem_clk
	);

parameter	udly	=	1'b1;

//	-------------------------------------------------------------------------------
//Configure Ports:
input[1:0]		arbtb_prior_mode;
//Devices Ports:
//IDE:
input			ide_req;
input			ide_ack;
//SB:
input			sb_req;
input			sb_ack;
//VSB:
input			vsb_req;
input			vsb_ack;

//For arbtm_prior_monitor:
output			arbtb_req;
output			arbtb_ack;
//Others:
input		rst_,
			mem_clk;
`ifdef	NEW_MEM

`else
//	-------------------------------------------------------------------------------
parameter		IDLE_ST_ENTER	=	3'b000,
				IDE_ST_ENTER	=	3'b001,
				SB_ST_ENTER		=	3'b010,
				VSB_ST_ENTER	=	3'b100;
				
//	-------------------------------------------------------------------------------
wire	rotation_sel;
wire	fixed_1st_sel;
wire	fixed_2nd_sel;
wire	fixed_3rd_sel;

wire	ide_trigger;
wire	sb_trigger;
wire	vsb_trigger;

wire	ide_st_err_ack;
wire	sb_st_err_ack;
wire	vsb_st_err_ack;

wire	arbtb_gnts;
wire	arbtb_set_busy;
reg		arbtb_is_busy;


wire[2:0]	nextst_in_idle;
wire[2:0]	nextst_in_ide;
wire[2:0]	nextst_in_sb;
wire[2:0]	nextst_in_vsb;
wire[2:0]	arbtb_next_st;
wire		hold_st;

wire[2:0]	enc_idle_rot;
wire[2:0]	enc_idle_1st;
wire[2:0]	enc_idle_2nd;
wire[2:0]	enc_idle_3rd;

wire[2:0]	enc_ide_rot;
wire[2:0]	enc_ide_1st;
wire[2:0]	enc_ide_2nd;
wire[2:0]	enc_ide_3rd;

wire[2:0]	enc_sb_rot;
wire[2:0]	enc_sb_1st;
wire[2:0]	enc_sb_2nd;
wire[2:0]	enc_sb_3rd;

wire[2:0]	enc_vsb_rot;
wire[2:0]	enc_vsb_1st;
wire[2:0]	enc_vsb_2nd;
wire[2:0]	enc_vsb_3rd;


reg[2:0]	arbtb_curr_st;
wire		arbtb_idle_st;
wire		arbtb_ide_st;
wire		arbtb_sb_st;
wire		arbtb_vsb_st;

//	-------------------------------------------------------------------------------
always	@(posedge mem_clk or negedge rst_)
	if(!rst_)begin
		arbtb_is_busy	<=	#udly	1'b0;
	end
	else if(arbtb_set_busy)begin
		arbtb_is_busy	<=	#udly	1'b1;
	end
	else if(arbtb_gnts)begin
		arbtb_is_busy	<=	#udly	1'b0;
	end

always	@(posedge mem_clk or negedge rst_)
	if(!rst_)begin
		arbtb_curr_st	<=	#udly	IDLE_ST_ENTER;
	end
	else if(!hold_st)begin
		arbtb_curr_st	<=	#udly	arbtb_next_st;
	end

//	-------------------------------------------------------------------------------
//Report the Arbited-Priority Error in IDLE_ST:
always	@(posedge mem_clk)
	if(arbtb_idle_st && (arbtb_gnts))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!\nIn ARBTB_IDLE_ST, Any GNT should not be found",$realtime);
		if(ide_ack)begin
			$display("Error_T2_rsim:\tIn ARBTB_IDLE_ST, IDE GNT is found");
		end
		if(sb_ack)begin
			$display("Error_T2_rsim:\tIn ARBTB_IDLE_ST, SB GNT is found");
		end
		if(vsb_ack)begin
			$display("Error_T2_rsim:\tIn ARBTB_IDLE_ST, VSB GNT is found");
		end
	end


//	-------------------------------------------------------------------------------
//Report the Arbited-Priority Error in IDE_ST:
always	@(posedge mem_clk)
	if(arbtb_ide_st && (ide_st_err_ack))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!\nIn ARBTB_IDE_ST, Any Other GNT should not be found",$realtime);
		if(sb_ack)begin
			$display("Error_T2_rsim:\tIn ARBTB_IDE_ST, SB GNT is found");
		end
		if(vsb_ack)begin
			$display("Error_T2_rsim:\tIn ARBTB_IDE_ST, VSB GNT is found");
		end
	end
	
//	-------------------------------------------------------------------------------
//Report the Arbited-Priority Error in SB_ST:
always	@(posedge mem_clk)
	if(arbtb_sb_st && (sb_st_err_ack))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!\nIn ARBTB_SB_ST, Any Other GNT should not be found",$realtime);
		if(ide_ack)begin
			$display("Error_T2_rsim:\tIn ARBTB_SB_ST, IDE GNT is found");
		end
		if(vsb_ack)begin
			$display("Error_T2_rsim:\tIn ARBTB_SB_ST, VSB GNT is found");
		end
	end

//	-------------------------------------------------------------------------------
//Report the Arbited-Priority Error in VSB_ST:
always	@(posedge mem_clk)
	if(arbtb_vsb_st && (vsb_st_err_ack))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!\nIn ARBTB_VSB_ST, Any Other GNT should not be found",$realtime);
		if(ide_ack)begin
			$display("Error_T2_rsim:\tIn ARBTB_VSB_ST, IDE GNT is found");
		end
		if(sb_ack)begin
			$display("Error_T2_rsim:\tIn ARBTB_VSB_ST, SB GNT is found");
		end
	end


//	-------------------------------------------------------------------------------
assign	rotation_sel	=	arbtb_prior_mode == 2'b00;
assign	fixed_1st_sel	=	arbtb_prior_mode == 2'b01;
assign	fixed_2nd_sel	=	arbtb_prior_mode == 2'b10;
assign	fixed_3rd_sel	=	arbtb_prior_mode == 2'b11;

assign	ide_trigger		=	ide_req && !ide_ack;
assign	sb_trigger		=	sb_req && !sb_ack;
assign	vsb_trigger		=	vsb_req && !vsb_ack;

assign	arbtb_set_busy	=	ide_trigger || sb_trigger || vsb_trigger;

assign	ide_st_err_ack	=	sb_ack || vsb_ack;
assign	sb_st_err_ack	=	ide_ack || vsb_ack;
assign	vsb_st_err_ack	=	ide_ack || sb_ack;

assign	arbtb_gnts		=	ide_ack || sb_ack || vsb_ack;

//	-------------------------------------------------------------------------------
assign	hold_st			=	arbtb_is_busy && !arbtb_gnts;

//	-------------------------------------------------------------------------------
assign	arbtb_idle_st	=	!(arbtb_curr_st[2] || arbtb_curr_st[1] || arbtb_curr_st[0]);

assign	arbtb_ide_st	=	arbtb_curr_st[0];
assign	arbtb_sb_st		=	arbtb_curr_st[1];
assign	arbtb_vsb_st	=	arbtb_curr_st[2];

assign	arbtb_next_st	=	{5{arbtb_idle_st}}	&	nextst_in_idle	|
							{5{arbtb_ide_st}}	&	nextst_in_ide	|
							{5{arbtb_sb_st}}	&	nextst_in_sb	|
							{5{arbtb_vsb_st}}	&	nextst_in_vsb;

//	-------------------------------------------------------------------------------
//Arbiter stays in IDLE_ST:
assign	nextst_in_idle	=	{5{rotation_sel}}	&	enc_idle_rot	|
							{5{fixed_1st_sel}}	&	enc_idle_1st	|
							{5{fixed_2nd_sel}}	&	enc_idle_2nd	|
							{5{fixed_3rd_sel}}	&	enc_idle_3rd;


assign	enc_idle_rot	=	ide_req 	?	IDE_ST_ENTER	:
							sb_req		?	SB_ST_ENTER		:
							vsb_req		?	VSB_ST_ENTER	:
											IDLE_ST_ENTER	;

assign	enc_idle_1st	=	ide_req 	?	IDE_ST_ENTER	:
							sb_req		?	SB_ST_ENTER		:
							vsb_req		?	VSB_ST_ENTER	:
											IDLE_ST_ENTER	;

assign	enc_idle_2nd	=	vsb_req		?	VSB_ST_ENTER	:
							ide_req 	?	IDE_ST_ENTER	:
							sb_req		?	SB_ST_ENTER		:
											IDLE_ST_ENTER	;

assign	enc_idle_3rd	=	ide_req 	?	IDE_ST_ENTER	:
							vsb_req		?	VSB_ST_ENTER	:
							sb_req		?	SB_ST_ENTER		:
											IDLE_ST_ENTER	;

//	----------------------------------------------------------------------------------
//Arbiter stays in IDE_ST:
assign	nextst_in_ide	=	{5{rotation_sel}}	&	enc_ide_rot	|
							{5{fixed_1st_sel}}	&	enc_ide_1st	|
							{5{fixed_2nd_sel}}	&	enc_ide_2nd	|
							{5{fixed_3rd_sel}}	&	enc_ide_3rd;


assign	enc_ide_rot		=	sb_req		?	SB_ST_ENTER	:
							vsb_req		?	VSB_ST_ENTER	:
											IDLE_ST_ENTER	;

assign	enc_ide_1st		=	sb_req		?	SB_ST_ENTER	:
							vsb_req		?	VSB_ST_ENTER	:
											IDLE_ST_ENTER	;

assign	enc_ide_2nd		=	vsb_req		?	VSB_ST_ENTER	:
							sb_req		?	SB_ST_ENTER		:
											IDLE_ST_ENTER	;

assign	enc_ide_3rd		=	vsb_req		?	VSB_ST_ENTER	:
							sb_req		?	SB_ST_ENTER		:
											IDLE_ST_ENTER	;

//	----------------------------------------------------------------------------------
//Arbiter stays in SB_ST:
assign	nextst_in_sb	=	{5{rotation_sel}}	&	enc_sb_rot	|
							{5{fixed_1st_sel}}	&	enc_sb_1st	|
							{5{fixed_2nd_sel}}	&	enc_sb_2nd	|
							{5{fixed_3rd_sel}}	&	enc_sb_3rd;

assign	enc_sb_rot	=		vsb_req		?	VSB_ST_ENTER	:
							ide_req		?	IDE_ST_ENTER	:
											IDLE_ST_ENTER	;

assign	enc_sb_1st	=		ide_req		?	IDE_ST_ENTER	:
							vsb_req		?	VSB_ST_ENTER	:
											IDLE_ST_ENTER	;

assign	enc_sb_2nd	=		vsb_req		?	VSB_ST_ENTER	:
							ide_req		?	IDE_ST_ENTER	:
											IDLE_ST_ENTER	;

assign	enc_sb_3rd	=		ide_req		?	IDE_ST_ENTER	:
							vsb_req		?	VSB_ST_ENTER	:
											IDLE_ST_ENTER	;

//	----------------------------------------------------------------------------------
//Arbiter stays in VSB_ST:
assign	nextst_in_vsb	=	{5{rotation_sel}}	&	enc_vsb_rot	|
							{5{fixed_1st_sel}}	&	enc_vsb_1st	|
							{5{fixed_2nd_sel}}	&	enc_vsb_2nd	|
							{5{fixed_3rd_sel}}	&	enc_vsb_3rd;

assign	enc_vsb_rot		=	ide_req		?	IDE_ST_ENTER	:
							sb_req		?	SB_ST_ENTER		:
											IDLE_ST_ENTER	;

assign	enc_vsb_1st		=	ide_req		?	IDE_ST_ENTER	:
							sb_req		?	SB_ST_ENTER		:
											IDLE_ST_ENTER	;

assign	enc_vsb_2nd		=	ide_req		?	IDE_ST_ENTER	:
							sb_req		?	SB_ST_ENTER		:
											IDLE_ST_ENTER	;

assign	enc_vsb_3rd		=	ide_req		?	IDE_ST_ENTER	:
							sb_req		?	SB_ST_ENTER		:
											IDLE_ST_ENTER	;


//	--------------------------------------------------------------------------------------
wire	arbtb_req		=	arbtb_is_busy;
wire	arbtb_ack		=	arbtb_gnts;

//	--------------------------------------------------------------------------------------
`endif
endmodule


