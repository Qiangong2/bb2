/******************************************************************************
	  (c) copyrights 2000-. All rights reserved
 
	   T-Square Design, Inc.
 
	  Company confidential and Properietary	information.
	  This information may not be disclosed	to unauthorized individual.	  
*******************************************************************************/

/*******************************************************************************
 *
 *       PROJECT:M6304
 *
 *       FILE NAME: imb_status_monitor.v
 *
 *       DESCRIPTION: Internal Memory Bus Status Monitor
 *						1. For checking the states of arbiters
 *						2. For checking the write dead lock
 *						3. For checking the read sequencer
 *						4. Foc checking the write sequencer
 *
 *
 *       AUTHOR: Figo OU
 *
 *       HISTORY:   11/04/02         initial version
 *      
 *********************************************************************************/

module	imb_status_monitor(
	//the memory bus arbiter states:
	suba_arbt_st,		//store the latest four states of the Sub-A Arbiter
	subb_arbt_st,		//store the latest four states of the Sub-B Arbiter
	main_arbt_st,		//store the latest four states of the Main Arbiter
	//for write dead lock:
	ram_wrlock_timer,	//used to enable the write dead lock timer
	ram_wrlock_resume,	//used to enable exiting from write dead lock state 
	ram_wrlock_tag,		//report the write dead lock error
	ram_wrlock_id,		//report the write dead lock device id
	//bus arbiting:
	//Display:
//	disp_ack,
//	disp_addr,
	//CPU:
	cpu_ack,
	cpu_rw,
	cpu_addr,
	//GPU:
	gpu_ack,
	gpu_rw,
	gpu_addr,
	//Video:
	video_ack,
	video_rw,
	video_addr,
	//DSP:
	dsp_ack,
	dsp_rw,
	dsp_addr,
	//PCI:
	pci_rack,
	pci_raddr,
	pci_wack,
	pci_waddr,
	//IDE:
	ide_ack,
	ide_rw,
	ide_addr,
	//SB:
	sb_ack,
	sb_rw,
	sb_addr,
	//VSB:
	vsb_ack,
	vsb_rw,
	vsb_addr,
	//Read Data-Path:
//	disp_rrdy,
//	disp_rlast,
//	disp_rdata,
	cpu_rrdy,
	cpu_rlast,
	cpu_rdata,
	gpu_rrdy,
	gpu_rlast,
	gpu_rdata,
	video_rrdy,
	video_rlast,
	video_rdata,
	dsp_rrdy,
	dsp_rlast,
	dsp_rdata,
	pci_rrdy,
	pci_rlast,
	pci_rdata,
	ide_rrdy,
	ide_rlast,
	ide_rdata,
	sb_rrdy,
	sb_rlast,
	sb_rdata,
	vsb_rrdy,
	vsb_rlast,
	vsb_rdata,
	//Write Data-Path:
	cpu_wrdy,
	cpu_wlast,
	cpu_wbe,
	cpu_wdata,
	gpu_wrdy,
	gpu_wlast,
	gpu_wbe,
	gpu_wdata,
	video_wrdy,
	video_wlast,
	video_wbe,
	video_wdata,
	dsp_wrdy,
	dsp_wlast,
	dsp_wbe,
	dsp_wdata,
	pci_wrdy,
	pci_wlast,
	pci_wbe,
	pci_wdata,
	ide_wrdy,
	ide_wlast,
	ide_wbe,
	ide_wdata,
	sb_wrdy,
	sb_wlast,
	sb_wbe,
	sb_wdata,
	vsb_wrdy,
	vsb_wlast,
	vsb_wbe,
	vsb_wdata,

	//Others:
	rst_,
	mem_clk
	);

parameter	udly	=	1'b1;

//	-------------------------------------------------------------------------------
//the memory bus arbiter states:
input[31:0]	suba_arbt_st;		//store the latest four states of the Sub-A Arbiter
//{PCI,DSP,VIDEO,GPU}
input[31:0]	subb_arbt_st;		//store the latest four states of the Sub-B Arbiter
//{VSB,SB,IDE}
input[31:0]	main_arbt_st;		//store the latest four states of the Main Arbiter
//{SUBB,SUBA,CPU,DISP,REF}
//for write dead lock:
input		ram_wrlock_timer;	//used to enable the write dead lock timer
input		ram_wrlock_resume;	//used to enable exiting from write dead lock state 
input		ram_wrlock_tag;		//report the write dead lock error
input[7:0]	ram_wrlock_id;		//report the write dead lock device id
/*
{ide_ram_wmux,sb_ram_wmux,dsp_ram_wmux,vsb_ram_wmux,
video_ram_wmux,gpu_ram_wmux,pci_ram_wmux,cpu_ram_wmux};
*/

//bus arbiting:
//Display:
//input		disp_ack;
//input[27:0]	disp_addr; 
wire			disp_ack;     
wire	[27:0]	disp_addr;   //for 3357  joyous 
//CPU:
input		cpu_ack;
input		cpu_rw;
input[27:0]	cpu_addr;
//GPU:
input		gpu_ack;
input		gpu_rw;
input[27:0]	gpu_addr;
//Video:
input		video_ack;
input		video_rw;
input[27:0]	video_addr;
//DSP:
input		dsp_ack;
input		dsp_rw;
input[27:0]	dsp_addr;
//PCI:
input		pci_rack;
input[27:0]	pci_raddr;
input		pci_wack;
input[27:0]	pci_waddr;
//IDE:
input		ide_ack;
input		ide_rw;
input[27:0]	ide_addr;
//SB:
input		sb_ack;
input		sb_rw;
input[27:0]	sb_addr;

//VSB:
input		vsb_ack;
input		vsb_rw;
input[27:0]	vsb_addr;
	
//Read Data-Path:
//input		disp_rrdy;
//input		disp_rlast;
//input[31:0]	disp_rdata;
input		cpu_rrdy;
input		cpu_rlast;
input[31:0]	cpu_rdata;
input		gpu_rrdy;
input		gpu_rlast;
input[31:0]	gpu_rdata;
input		video_rrdy;
input		video_rlast;
input[31:0]	video_rdata;
input		dsp_rrdy;
input		dsp_rlast;
input[31:0]	dsp_rdata;
input		pci_rrdy;
input		pci_rlast;
input[31:0]	pci_rdata;
input		ide_rrdy;
input		ide_rlast;
input[31:0]	ide_rdata;
input		sb_rrdy;
input		sb_rlast;
input[31:0]	sb_rdata;
input		vsb_rrdy;
input		vsb_rlast;
input[31:0]	vsb_rdata;
//Write Data-Path:
input		cpu_wrdy;
input		cpu_wlast;
input[3:0]	cpu_wbe;
input[31:0]	cpu_wdata;
input		gpu_wrdy;
input		gpu_wlast;
input[3:0]	gpu_wbe;
input[31:0]	gpu_wdata;
input		video_wrdy;
input		video_wlast;
input[3:0]	video_wbe;
input[31:0]	video_wdata;
input		dsp_wrdy;
input		dsp_wlast;
input[3:0]	dsp_wbe;
input[31:0]	dsp_wdata;
input		pci_wrdy;
input		pci_wlast;
input[3:0]	pci_wbe;
input[31:0]	pci_wdata;
input		ide_wrdy;
input		ide_wlast;
input[3:0]	ide_wbe;
input[31:0]	ide_wdata;
input		sb_wrdy;
input		sb_wlast;
input[3:0]	sb_wbe;  
input[31:0]	sb_wdata;
input		vsb_wrdy;
input		vsb_wlast;
input[3:0]	vsb_wbe;  
input[31:0]	vsb_wdata;

//Others:
input		rst_,
			mem_clk;  
			
wire		disp_rrdy;     
wire		disp_rlast;    
wire[31:0]	disp_rdata;  //for 3357 Joyous    
`ifdef  NEW_MEM

`else				
//	--------------------------------------------------------------------------------------
wire[7:0]	suba_st0,suba_st1,suba_st2,suba_st3;
wire		suba_st0_pci,suba_st0_dsp,suba_st0_video,suba_st0_gpu;
wire		suba_st1_pci,suba_st1_dsp,suba_st1_video,suba_st1_gpu;
wire		suba_st2_pci,suba_st2_dsp,suba_st2_video,suba_st2_gpu;
wire		suba_st3_pci,suba_st3_dsp,suba_st3_video,suba_st3_gpu;
wire		suba_st0_err,suba_st1_err,suba_st2_err,suba_st3_err;

wire[7:0]	subb_st0,subb_st1,subb_st2,subb_st3;
wire		subb_st0_vsb,subb_st0_sb,subb_st0_ide;
wire		subb_st1_vsb,subb_st1_sb,subb_st1_ide;
wire		subb_st2_vsb,subb_st2_sb,subb_st2_ide;
wire		subb_st3_vsb,subb_st3_sb,subb_st3_ide;
wire		subb_st0_err,subb_st1_err,subb_st2_err,subb_st3_err;

wire[7:0]	main_st0,main_st1,main_st2,main_st3;
wire		main_st0_ref,main_st0_disp,main_st0_cpu,main_st0_suba,main_st0_subb;
wire		main_st1_ref,main_st1_disp,main_st1_cpu,main_st1_suba,main_st1_subb;
wire		main_st2_ref,main_st2_disp,main_st2_cpu,main_st2_suba,main_st2_subb;
wire		main_st3_ref,main_st3_disp,main_st3_cpu,main_st3_suba,main_st3_subb;
wire		main_st0_err,main_st1_err,main_st2_err,main_st3_err;

//	--------------------------------------------------------------------------------------
reg			wrlock_timer_en,wrlock_timer_en_dly;
wire		wrlock_timer_enable, wrlock_timer_disable;

reg			wrlock_resume_en,wrlock_resume_en_dly;
wire		wrlock_resume_enable,wrlock_resume_disable;

reg			wrlock_tag, wrlock_tag_dly;
wire		wrlock_occur;
wire		wrlock_ide,wrlock_sb,wrlock_dsp,wrlock_vsb,
			wrlock_video,wrlock_gpu,wrlock_pci,wrlock_cpu;
			
//	--------------------------------------------------------------------------------------
wire		disp_rgnt;
wire		cpu_rgnt;
wire		cpu_wgnt;
wire		gpu_rgnt;
wire		gpu_wgnt;
wire		video_rgnt;
wire		video_wgnt;
wire		dsp_rgnt;
wire		dsp_wgnt;
wire		pci_rgnt;
wire		pci_wgnt;
wire		ide_rgnt;
wire		ide_wgnt;
wire		sb_rgnt;
wire		sb_wgnt;
wire		vsb_rgnt;
wire		vsb_wgnt;
wire		rd_gnts;
wire		wr_gnts;
wire[7:0]	wr_mux;

//Read-Data-Path-Checking:
reg[8:0]	rd_pipeline[7:0];
wire		rd_pipeline_in,rd_pipeline_out;

reg[2:0]	rd_pipeline_waddr, rd_pipeline_raddr;
wire[8:0]	rd_pipeline_rdata;
wire		disp_rd_en,cpu_rd_en,
			gpu_rd_en,video_rd_en,dsp_rd_en,pci_rd_en,
			ide_rd_en,sb_rd_en,vsb_rd_en;

wire		disp_rd_err,cpu_rd_err,
			gpu_rd_err,video_rd_err,dsp_rd_err,pci_rd_err,
			ide_rd_err,sb_rd_err,vsb_rd_err;

//Write-Data-Path-Checking:
reg[7:0]	wr_pipeline;
reg			cpu_wrdy_reg,
			gpu_wrdy_reg,video_wrdy_reg,dsp_wrdy_reg,pci_wrdy_reg,
			ide_wrdy_reg,sb_wrdy_reg,vsb_wrdy_reg;

wire		cpu_wr_en,
			gpu_wr_en,video_wr_en,dsp_wr_en,pci_wr_en,
			ide_wr_en,sb_wr_en,vsb_wr_en;

wire		cpu_wr_err,
			gpu_wr_err,video_wr_err,dsp_wr_err,pci_wr_err,
			ide_wr_err,sb_wr_err,vsb_wr_err;

//	--------------------------------------------------------------------------------------
//dump write data operation:
reg			devs_wrdy_dly;
reg[3:0]	devs_wbe_dly;
reg[31:0]	devs_wdata_dly;
reg[27:0]	devs_waddr_bus;
integer		sdram_wr_fhandle;

`ifdef	SDRAM_WRPATH_DEBUG
initial	begin
	sdram_wr_fhandle	=	$fopen("sdram_wrpath.rpt");
end
`endif

//	--------------------------------------------------------------------------------------
//dump read data operation:
reg[27:0]	devs_raddr_buf[7:0];
reg[2:0]	devs_raddr_buf_waddr,devs_raddr_buf_raddr;
wire[27:0]	devs_start_raddr;
reg[1:0]	devs_rbl_cnt;
wire[1:0]	devs_dw_addr;
wire		devs_rrdy,devs_rlast;
integer		devs_raddr_buf_i;
integer		sdram_rd_fhandle;

`ifdef	SDRAM_RDPATH_DEBUG
initial	begin
	sdram_rd_fhandle	=	$fopen("sdram_rdpath.rpt");
end
`endif

initial	begin
	for(devs_raddr_buf_i=0;devs_raddr_buf_i<8;devs_raddr_buf_i=devs_raddr_buf_i+1)begin
		devs_raddr_buf[devs_raddr_buf_i]	=	0;
	end
	devs_raddr_buf_waddr	=	0;
	devs_raddr_buf_raddr	=	0;
	devs_rbl_cnt			=	0;
end

//	--------------------------------------------------------------------------------------
//ARBTA_ST_ERROR:
always	@(posedge mem_clk)
	if(suba_st0_err)begin
		$display("\n%m\tAt Time: %t\tError_T2_rsim is found!\nIn ARBTA_ST_0, Multiple States Contest",$realtime);
		if(suba_st0_pci)begin
			$display("ARBTA_ST0_PCI is activated!");
		end
		if(suba_st0_dsp)begin
			$display("ARBTA_ST0_DSP is activated!");
		end
		if(suba_st0_video)begin
			$display("ARBTA_ST0_VIDEO is activated!");
		end
		if(suba_st0_gpu)begin
			$display("ARBTA_ST0_GPU is activated!");
		end
	end

always	@(posedge mem_clk)
	if(suba_st1_err)begin
		$display("\n%m\tAt Time: %t\tError_T2_rsim is found!\nIn ARBTA_ST_1, Multiple States Contest",$realtime);
		if(suba_st1_pci)begin
			$display("ARBTA_ST1_PCI is activated!");
		end
		if(suba_st1_dsp)begin
			$display("ARBTA_ST1_DSP is activated!");
		end
		if(suba_st1_video)begin
			$display("ARBTA_ST1_VIDEO is activated!");
		end
		if(suba_st1_gpu)begin
			$display("ARBTA_ST1_GPU is activated!");
		end
	end

always	@(posedge mem_clk)
	if(suba_st2_err)begin
		$display("\n%m\tAt Time: %t\tError_T2_rsim is found!\nIn ARBTA_ST_2, Multiple States Contest",$realtime);
		if(suba_st2_pci)begin
			$display("ARBTA_ST2_PCI is activated!");
		end
		if(suba_st2_dsp)begin
			$display("ARBTA_ST2_DSP is activated!");
		end
		if(suba_st2_video)begin
			$display("ARBTA_ST2_VIDEO is activated!");
		end
		if(suba_st2_gpu)begin
			$display("ARBTA_ST2_GPU is activated!");
		end
	end

always	@(posedge mem_clk)
	if(suba_st3_err)begin
		$display("\n%m\tAt Time: %t\tError_T2_rsim is found!\nIn ARBTA_ST_3, Multiple States Contest",$realtime);
		if(suba_st3_pci)begin
			$display("ARBTA_ST3_PCI is activated!");
		end
		if(suba_st3_dsp)begin
			$display("ARBTA_ST3_DSP is activated!");
		end
		if(suba_st3_video)begin
			$display("ARBTA_ST3_VIDEO is activated!");
		end
		if(suba_st3_gpu)begin
			$display("ARBTA_ST3_GPU is activated!");
		end
	end

//	--------------------------------------------------------------------------------------
//ARBTB_ST_ERROR:
always	@(posedge mem_clk)
	if(subb_st0_err)begin
		$display("\n%m\tAt Time: %t\tError_T2_rsim is found!\nIn ARBTB_ST_0, Multiple States Contest",$realtime);
		if(subb_st0_vsb)begin
			$display("ARBTB_ST0_VSB is activated!");
		end
		if(subb_st0_sb)begin
			$display("ARBTB_ST0_SB is activated!");
		end
		if(subb_st0_ide)begin
			$display("ARBTB_ST0_IDE is activated!");
		end
	end

always	@(posedge mem_clk)
	if(subb_st1_err)begin
		$display("\n%m\tAt Time: %t\tError_T2_rsim is found!\nIn ARBTB_ST_1, Multiple States Contest",$realtime);
		if(subb_st1_vsb)begin
			$display("ARBTB_ST1_VSB is activated!");
		end
		if(subb_st1_sb)begin
			$display("ARBTB_ST1_SB is activated!");
		end
		if(subb_st1_ide)begin
			$display("ARBTB_ST1_IDE is activated!");
		end
	end

always	@(posedge mem_clk)
	if(subb_st2_err)begin
		$display("\n%m\tAt Time: %t\tError_T2_rsim is found!\nIn ARBTB_ST_2, Multiple States Contest",$realtime);
		if(subb_st2_vsb)begin
			$display("ARBTB_ST2_VSB is activated!");
		end
		if(subb_st2_sb)begin
			$display("ARBTB_ST2_SB is activated!");
		end
		if(subb_st2_ide)begin
			$display("ARBTB_ST2_IDE is activated!");
		end
	end

always	@(posedge mem_clk)
	if(subb_st3_err)begin
		$display("\n%m\tAt Time: %t\tError_T2_rsim is found!\nIn ARBTB_ST_3, Multiple States Contest",$realtime);
		if(subb_st3_vsb)begin
			$display("ARBTB_ST3_VSB is activated!");
		end
		if(subb_st3_sb)begin
			$display("ARBTB_ST3_SB is activated!");
		end
		if(subb_st3_ide)begin
			$display("ARBTB_ST3_IDE is activated!");
		end
	end

//	--------------------------------------------------------------------------------------
//ARBTM_ST_ERROR:
always	@(posedge mem_clk)
	if(main_st0_err)begin
		$display("\n%m\tAt Time: %t\tError_T2_rsim is found!\nIn ARBTM_ST_0, Multiple States Contest",$realtime);
		if(main_st0_subb)begin
			$display("ARBTM_ST0_SUBB is activated!");
		end
		if(main_st0_suba)begin
			$display("ARBTM_ST0_SUBA is activated!");
		end
		if(main_st0_cpu)begin
			$display("ARBTM_ST0_CPU is activated!");
		end
		if(main_st0_disp)begin
			$display("ARBTM_ST0_DISP is activated!");
		end
		if(main_st0_ref)begin
			$display("ARBTM_ST0_REF is activated!");
		end
	end

always	@(posedge mem_clk)
	if(main_st1_err)begin
		$display("\n%m\tAt Time: %t\tError_T2_rsim is found!\nIn ARBTM_ST_1, Multiple States Contest",$realtime);
		if(main_st1_subb)begin
			$display("ARBTM_ST1_SUBB is activated!");
		end
		if(main_st1_suba)begin
			$display("ARBTM_ST1_SUBA is activated!");
		end
		if(main_st1_cpu)begin
			$display("ARBTM_ST1_CPU is activated!");
		end
		if(main_st1_disp)begin
			$display("ARBTM_ST1_DISP is activated!");
		end
		if(main_st1_ref)begin
			$display("ARBTM_ST1_REF is activated!");
		end
	end

always	@(posedge mem_clk)
	if(main_st2_err)begin
		$display("\n%m\tAt Time: %t\tError_T2_rsim is found!\nIn ARBTM_ST_2, Multiple States Contest",$realtime);
		if(main_st2_subb)begin
			$display("ARBTM_ST2_SUBB is activated!");
		end
		if(main_st2_suba)begin
			$display("ARBTM_ST2_SUBA is activated!");
		end
		if(main_st2_cpu)begin
			$display("ARBTM_ST2_CPU is activated!");
		end
		if(main_st2_disp)begin
			$display("ARBTM_ST2_DISP is activated!");
		end
		if(main_st2_ref)begin
			$display("ARBTM_ST2_REF is activated!");
		end
	end

always	@(posedge mem_clk)
	if(main_st3_err)begin
		$display("\n%m\tAt Time: %t\tError_T2_rsim is found!\nIn ARBTM_ST_3, Multiple States Contest",$realtime);
		if(main_st3_subb)begin
			$display("ARBTM_ST3_SUBB is activated!");
		end
		if(main_st3_suba)begin
			$display("ARBTM_ST3_SUBA is activated!");
		end
		if(main_st3_cpu)begin
			$display("ARBTM_ST3_CPU is activated!");
		end
		if(main_st3_disp)begin
			$display("ARBTM_ST3_DISP is activated!");
		end
		if(main_st3_ref)begin
			$display("ARBTM_ST3_REF is activated!");
		end
	end

//	--------------------------------------------------------------------------------------
always	@(posedge mem_clk or negedge rst_)
	if(!rst_)begin
		wrlock_timer_en		<=	#udly	1'b0;
		wrlock_timer_en_dly	<=	#udly	1'b0;
	end
	else	begin
		wrlock_timer_en		<=	#udly	ram_wrlock_timer;
		wrlock_timer_en_dly	<=	#udly	wrlock_timer_en;
	end

always	@(posedge mem_clk or negedge rst_)
	if(!rst_)begin
		wrlock_resume_en		<=	#udly	1'b0;
		wrlock_resume_en_dly	<=	#udly	1'b0;
	end
	else	begin
		wrlock_resume_en		<=	#udly	ram_wrlock_resume;
		wrlock_resume_en_dly	<=	#udly	wrlock_resume_en;
	end

always	@(posedge mem_clk)
	if(wrlock_timer_enable)begin
		$display("\n%m\tAt Time: %t\tSystem IO Control Messages:\nWrite Dead-Lock Timer is activated!\n",$realtime);
	end

always	@(posedge mem_clk)
	if(wrlock_timer_disable)begin
		$display("\n%m\tAt Time: %t\tSystem IO Control Messages:\nWrite Dead-Lock Timer is unactivated!\n",$realtime);
	end

always	@(posedge mem_clk)
	if(wrlock_resume_enable)begin
		$display("\n%m\tAt Time: %t\tSystem IO Control Messages:\nWrite Dead-Lock Resuming is activated!\n",$realtime);
	end

always	@(posedge mem_clk)
	if(wrlock_resume_disable)begin
		$display("\n%m\tAt Time: %t\tSystem IO Control Messages:\nWrite Dead-Lock Resuming is unactivated!\n",$realtime);
	end

always	@(posedge mem_clk or negedge rst_)
	if(!rst_)begin
		wrlock_tag		<=	#udly	1'b0;
		wrlock_tag_dly	<=	#udly	1'b0;
	end
	else	begin
		wrlock_tag		<=	#udly	ram_wrlock_tag;
		wrlock_tag_dly	<=	#udly	wrlock_tag;
	end

always	@(posedge mem_clk)
	if(wrlock_occur)begin
		$display("\n%m\tAt Time: %t\tError_T2_rsim is found!\nWrite Dead-Lock Occurs",$realtime);
		if(wrlock_ide)begin
			$display("Error_T2_rsim:\tIDE Results In Write Dead-Lock!");
		end
		if(wrlock_sb)begin
			$display("Error_T2_rsim:\tSB Results In Write Dead-Lock!");
		end
		if(wrlock_dsp)begin
			$display("Error_T2_rsim:\tDSP Results In Write Dead-Lock!");
		end
		if(wrlock_vsb)begin
			$display("Error_T2_rsim:\tVSB Results In Write Dead-Lock!");
		end
		if(wrlock_video)begin
			$display("Error_T2_rsim:\tVIDEO Results In Write Dead-Lock!");
		end
		if(wrlock_gpu)begin
			$display("Error_T2_rsim:\tGPU Results In Write Dead-Lock!");
		end
		if(wrlock_pci)begin
			$display("Error_T2_rsim:\tPCI Results In Write Dead-Lock!");
		end
		if(wrlock_cpu)begin
			$display("Error_T2_rsim:\tCPU Results In Write Dead-Lock!");
		end
	end

//	--------------------------------------------------------------------------------------
//Read Sequencer Checking:
always	@(posedge mem_clk or negedge rst_)
	if(!rst_)begin
		rd_pipeline_waddr	<=	#udly	3'h0;
		rd_pipeline[0]	<=	#udly	9'h0;rd_pipeline[1]	<=	#udly	9'h0;
		rd_pipeline[2]	<=	#udly	9'h0;rd_pipeline[3]	<=	#udly	9'h0;
		rd_pipeline[4]	<=	#udly	9'h0;rd_pipeline[5]	<=	#udly	9'h0;
		rd_pipeline[6]	<=	#udly	9'h0;rd_pipeline[7]	<=	#udly	9'h0;
	end
	else if(rd_pipeline_in)begin
		rd_pipeline_waddr	<=	#udly	rd_pipeline_waddr + 1'b1;
		rd_pipeline[rd_pipeline_waddr]	<=	#udly	{disp_rgnt,cpu_rgnt,
													gpu_rgnt,video_rgnt,dsp_rgnt,pci_rgnt,
													ide_rgnt,sb_rgnt,vsb_rgnt};
	end

always	@(posedge mem_clk or negedge rst_)
	if(!rst_)begin
		rd_pipeline_raddr	<=	#udly	3'h0;
	end
	else if(rd_pipeline_out)begin
		rd_pipeline_raddr	<=	#udly	rd_pipeline_raddr + 1'b1;
	end
	
always	@(posedge mem_clk)
	if(disp_rd_err)begin
		$display("\n%m\tAt Time: %t\tError_T2_rsim is found!\nIn DISP_RD, Read Sequencing is wrong",$realtime);
		if(cpu_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for CPU!");
		end
		if(gpu_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for GPU!");
		end
		if(video_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for VIDEO!");
		end
		if(dsp_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for DSP!");
		end
		if(pci_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for PCI!");
		end
		if(ide_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for IDE!");
		end
		if(sb_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for SB!");
		end
		if(vsb_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for VSB!");
		end
	end

always	@(posedge mem_clk)
	if(cpu_rd_err)begin
		$display("\n%m\tAt Time: %t\tError_T2_rsim is found!\nIn CPU_RD, Read Sequencing is wrong",$realtime);
		if(disp_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for DISP!");
		end
		if(gpu_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for GPU!");
		end
		if(video_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for VIDEO!");
		end
		if(dsp_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for DSP!");
		end
		if(pci_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for PCI!");
		end
		if(ide_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for IDE!");
		end
		if(sb_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for SB!");
		end
		if(vsb_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for VSB!");
		end
	end

always	@(posedge mem_clk)
	if(gpu_rd_err)begin
		$display("\n%m\tAt Time: %t\tError_T2_rsim is found!\nIn GPU_RD, Read Sequencing is wrong",$realtime);
		if(disp_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for DISP!");
		end
		if(cpu_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for CPU!");
		end
		if(video_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for VIDEO!");
		end
		if(dsp_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for DSP!");
		end
		if(pci_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for PCI!");
		end
		if(ide_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for IDE!");
		end
		if(sb_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for SB!");
		end
		if(vsb_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for VSB!");
		end
	end

always	@(posedge mem_clk)
	if(video_rd_err)begin
		$display("\n%m\tAt Time: %t\tError_T2_rsim is found!\nIn VIDEO_RD, Read Sequencing is wrong",$realtime);
		if(disp_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for DISP!");
		end
		if(cpu_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for CPU!");
		end
		if(gpu_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for GPU!");
		end
		if(dsp_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for DSP!");
		end
		if(pci_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for PCI!");
		end
		if(ide_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for IDE!");
		end
		if(sb_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for SB!");
		end
		if(vsb_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for VSB!");
		end
	end

always	@(posedge mem_clk)
	if(dsp_rd_err)begin
		$display("\n%m\tAt Time: %t\tError_T2_rsim is found!\nIn DSP_RD, Read Sequencing is wrong",$realtime);
		if(disp_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for DISP!");
		end
		if(cpu_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for CPU!");
		end
		if(gpu_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for GPU!");
		end
		if(video_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for VIDEO!");
		end
		if(pci_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for PCI!");
		end
		if(ide_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for IDE!");
		end
		if(sb_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for SB!");
		end
		if(vsb_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for VSB!");
		end
	end

always	@(posedge mem_clk)
	if(pci_rd_err)begin
		$display("\n%m\tAt Time: %t\tError_T2_rsim is found!\nIn PCI_RD, Read Sequencing is wrong",$realtime);
		if(disp_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for DISP!");
		end
		if(cpu_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for CPU!");
		end
		if(gpu_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for GPU!");
		end
		if(video_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for VIDEO!");
		end
		if(dsp_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for DSP!");
		end
		if(ide_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for IDE!");
		end
		if(sb_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for SB!");
		end
		if(vsb_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for VSB!");
		end
	end

always	@(posedge mem_clk)
	if(ide_rd_err)begin
		$display("\n%m\tAt Time: %t\tError_T2_rsim is found!\nIn IDE_RD, Read Sequencing is wrong",$realtime);
		if(disp_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for DISP!");
		end
		if(cpu_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for CPU!");
		end
		if(gpu_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for GPU!");
		end
		if(video_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for VIDEO!");
		end
		if(dsp_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for DSP!");
		end
		if(pci_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for PCI!");
		end
		if(sb_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for SB!");
		end
		if(vsb_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for VSB!");
		end
	end

always	@(posedge mem_clk)
	if(sb_rd_err)begin
		$display("\n%m\tAt Time: %t\tError_T2_rsim is found!\nIn SB_RD, Read Sequencing is wrong",$realtime);
		if(disp_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for DISP!");
		end
		if(cpu_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for CPU!");
		end
		if(gpu_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for GPU!");
		end
		if(video_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for VIDEO!");
		end
		if(dsp_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for DSP!");
		end
		if(pci_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for PCI!");
		end
		if(ide_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for IDE!");
		end
		if(vsb_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for VSB!");
		end
	end

always	@(posedge mem_clk)
	if(vsb_rd_err)begin
		$display("\n%m\tAt Time: %t\tError_T2_rsim is found!\nIn VSB_RD, Read Sequencing is wrong",$realtime);
		if(disp_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for DISP!");
		end
		if(cpu_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for CPU!");
		end
		if(gpu_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for GPU!");
		end
		if(video_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for VIDEO!");
		end
		if(dsp_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for DSP!");
		end
		if(pci_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for PCI!");
		end
		if(ide_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for IDE!");
		end
		if(sb_rrdy)begin
			$display("Error_T2_rsim:\tCurrent Read-Data is not for SB!");
		end
	end

//	--------------------------------------------------------------------------------------
//Write-Sequence Checking:
always	@(posedge mem_clk or negedge rst_)
	if(!rst_)begin
		wr_pipeline	<=	#udly	8'h0;
	end
	else if(wr_gnts)begin
		wr_pipeline	<=	#udly	wr_mux;
	end

always	@(posedge mem_clk or negedge rst_)
	if(!rst_)begin
		cpu_wrdy_reg	<=	#udly	1'b0;
	end
	else	begin
		cpu_wrdy_reg	<=	#udly	cpu_wrdy;
	end

always	@(posedge mem_clk or negedge rst_)
	if(!rst_)begin
		gpu_wrdy_reg	<=	#udly	1'b0;
		video_wrdy_reg	<=	#udly	1'b0;
		dsp_wrdy_reg	<=	#udly	1'b0;
		pci_wrdy_reg	<=	#udly	1'b0;
	end
	else	begin
		gpu_wrdy_reg	<=	#udly	gpu_wrdy;
		video_wrdy_reg	<=	#udly	video_wrdy;
		dsp_wrdy_reg	<=	#udly	dsp_wrdy;
		pci_wrdy_reg	<=	#udly	pci_wrdy;
	end

always	@(posedge mem_clk or negedge rst_)
	if(!rst_)begin
		ide_wrdy_reg	<=	#udly	1'b0;
		sb_wrdy_reg		<=	#udly	1'b0;
		vsb_wrdy_reg	<=	#udly	1'b0;
	end
	else	begin
		ide_wrdy_reg	<=	#udly	ide_wrdy;
		sb_wrdy_reg		<=	#udly	sb_wrdy;
		vsb_wrdy_reg	<=	#udly	vsb_wrdy;
	end

always	@(posedge mem_clk)
	if(cpu_wr_err)begin
		$display("\n%m\tAt Time: %t\tError_T2_rsim is found!\nIn CPU_WR, Write Sequencing is wrong",$realtime);
		if(gpu_wrdy_reg)begin
			$display("Error_T2_rsim:\tCurrent Write-Data is not for GPU!");
		end
		if(video_wrdy_reg)begin
			$display("Error_T2_rsim:\tCurrent Write-Data is not for VIDEO!");
		end
		if(dsp_wrdy_reg)begin
			$display("Error_T2_rsim:\tCurrent Write-Data is not for DSP!");
		end
		if(pci_wrdy_reg)begin
			$display("Error_T2_rsim:\tCurrent Write-Data is not for PCI!");
		end
		if(ide_wrdy_reg)begin
			$display("Error_T2_rsim:\tCurrent Write-Data is not for IDE!");
		end
		if(sb_wrdy_reg)begin
			$display("Error_T2_rsim:\tCurrent Write-Data is not for SB!");
		end
		if(vsb_wrdy_reg)begin
			$display("Error_T2_rsim:\tCurrent Write-Data is not for VSB!");
		end
	end

always	@(posedge mem_clk)
	if(gpu_wr_err)begin
		$display("\n%m\tAt Time: %t\tError_T2_rsim is found!\nIn GPU_WR, Write Sequencing is wrong",$realtime);
		if(cpu_wrdy_reg)begin
			$display("Error_T2_rsim:\tCurrent Write-Data is not for CPU!");
		end
		if(video_wrdy_reg)begin
			$display("Error_T2_rsim:\tCurrent Write-Data is not for VIDEO!");
		end
		if(dsp_wrdy_reg)begin
			$display("Error_T2_rsim:\tCurrent Write-Data is not for DSP!");
		end
		if(pci_wrdy_reg)begin
			$display("Error_T2_rsim:\tCurrent Write-Data is not for PCI!");
		end
		if(ide_wrdy_reg)begin
			$display("Error_T2_rsim:\tCurrent Write-Data is not for IDE!");
		end
		if(sb_wrdy_reg)begin
			$display("Error_T2_rsim:\tCurrent Write-Data is not for SB!");
		end
		if(vsb_wrdy_reg)begin
			$display("Error_T2_rsim:\tCurrent Write-Data is not for VSB!");
		end
	end

always	@(posedge mem_clk)
	if(video_wr_err)begin
		$display("\n%m\tAt Time: %t\tError_T2_rsim is found!\nIn VIDEO_WR, Write Sequencing is wrong",$realtime);
		if(cpu_wrdy_reg)begin
			$display("Error_T2_rsim:\tCurrent Write-Data is not for CPU!");
		end
		if(gpu_wrdy_reg)begin
			$display("Error_T2_rsim:\tCurrent Write-Data is not for GPU!");
		end
		if(dsp_wrdy_reg)begin
			$display("Error_T2_rsim:\tCurrent Write-Data is not for DSP!");
		end
		if(pci_wrdy_reg)begin
			$display("Error_T2_rsim:\tCurrent Write-Data is not for PCI!");
		end
		if(ide_wrdy_reg)begin
			$display("Error_T2_rsim:\tCurrent Write-Data is not for IDE!");
		end
		if(sb_wrdy_reg)begin
			$display("Error_T2_rsim:\tCurrent Write-Data is not for SB!");
		end
		if(vsb_wrdy_reg)begin
			$display("Error_T2_rsim:\tCurrent Write-Data is not for VSB!");
		end
	end

always	@(posedge mem_clk)
	if(dsp_wr_err)begin
		$display("\n%m\tAt Time: %t\tError_T2_rsim is found!\nIn DSP_WR, Write Sequencing is wrong",$realtime);
		if(cpu_wrdy_reg)begin
			$display("Error_T2_rsim:\tCurrent Write-Data is not for CPU!");
		end
		if(gpu_wrdy_reg)begin
			$display("Error_T2_rsim:\tCurrent Write-Data is not for GPU!");
		end
		if(video_wrdy_reg)begin
			$display("Error_T2_rsim:\tCurrent Write-Data is not for VIDEO!");
		end
		if(pci_wrdy_reg)begin
			$display("Error_T2_rsim:\tCurrent Write-Data is not for PCI!");
		end
		if(ide_wrdy_reg)begin
			$display("Error_T2_rsim:\tCurrent Write-Data is not for IDE!");
		end
		if(sb_wrdy_reg)begin
			$display("Error_T2_rsim:\tCurrent Write-Data is not for SB!");
		end
		if(vsb_wrdy_reg)begin
			$display("Error_T2_rsim:\tCurrent Write-Data is not for VSB!");
		end
	end

always	@(posedge mem_clk)
	if(pci_wr_err)begin
		$display("\n%m\tAt Time: %t\tError_T2_rsim is found!\nIn PCI_WR, Write Sequencing is wrong",$realtime);
		if(cpu_wrdy_reg)begin
			$display("Error_T2_rsim:\tCurrent Write-Data is not for CPU!");
		end
		if(gpu_wrdy_reg)begin
			$display("Error_T2_rsim:\tCurrent Write-Data is not for GPU!");
		end
		if(video_wrdy_reg)begin
			$display("Error_T2_rsim:\tCurrent Write-Data is not for VIDEO!");
		end
		if(dsp_wrdy_reg)begin
			$display("Error_T2_rsim:\tCurrent Write-Data is not for DSP!");
		end
		if(ide_wrdy_reg)begin
			$display("Error_T2_rsim:\tCurrent Write-Data is not for IDE!");
		end
		if(sb_wrdy_reg)begin
			$display("Error_T2_rsim:\tCurrent Write-Data is not for SB!");
		end
		if(vsb_wrdy_reg)begin
			$display("Error_T2_rsim:\tCurrent Write-Data is not for VSB!");
		end
	end

always	@(posedge mem_clk)
	if(ide_wr_err)begin
		$display("\n%m\tAt Time: %t\tError_T2_rsim is found!\nIn IDE_WR, Write Sequencing is wrong",$realtime);
		if(cpu_wrdy_reg)begin
			$display("Error_T2_rsim:\tCurrent Write-Data is not for CPU!");
		end
		if(gpu_wrdy_reg)begin
			$display("Error_T2_rsim:\tCurrent Write-Data is not for GPU!");
		end
		if(video_wrdy_reg)begin
			$display("Error_T2_rsim:\tCurrent Write-Data is not for VIDEO!");
		end
		if(dsp_wrdy_reg)begin
			$display("Error_T2_rsim:\tCurrent Write-Data is not for DSP!");
		end
		if(pci_wrdy_reg)begin
			$display("Error_T2_rsim:\tCurrent Write-Data is not for PCI!");
		end
		if(sb_wrdy_reg)begin
			$display("Error_T2_rsim:\tCurrent Write-Data is not for SB!");
		end
		if(vsb_wrdy_reg)begin
			$display("Error_T2_rsim:\tCurrent Write-Data is not for VSB!");
		end
	end

always	@(posedge mem_clk)
	if(sb_wr_err)begin
		$display("\n%m\tAt Time: %t\tError_T2_rsim is found!\nIn SB_WR, Write Sequencing is wrong",$realtime);
		if(cpu_wrdy_reg)begin
			$display("Error_T2_rsim:\tCurrent Write-Data is not for CPU!");
		end
		if(gpu_wrdy_reg)begin
			$display("Error_T2_rsim:\tCurrent Write-Data is not for GPU!");
		end
		if(video_wrdy_reg)begin
			$display("Error_T2_rsim:\tCurrent Write-Data is not for VIDEO!");
		end
		if(dsp_wrdy_reg)begin
			$display("Error_T2_rsim:\tCurrent Write-Data is not for DSP!");
		end
		if(pci_wrdy_reg)begin
			$display("Error_T2_rsim:\tCurrent Write-Data is not for PCI!");
		end
		if(ide_wrdy_reg)begin
			$display("Error_T2_rsim:\tCurrent Write-Data is not for IDE!");
		end
		if(vsb_wrdy_reg)begin
			$display("Error_T2_rsim:\tCurrent Write-Data is not for VSB!");
		end
	end

always	@(posedge mem_clk)
	if(vsb_wr_err)begin
		$display("\n%m\tAt Time: %t\tError_T2_rsim is found!\nIn VSB_WR, Write Sequencing is wrong",$realtime);
		if(cpu_wrdy_reg)begin
			$display("Error_T2_rsim:\tCurrent Write-Data is not for CPU!");
		end
		if(gpu_wrdy_reg)begin
			$display("Error_T2_rsim:\tCurrent Write-Data is not for GPU!");
		end
		if(video_wrdy_reg)begin
			$display("Error_T2_rsim:\tCurrent Write-Data is not for VIDEO!");
		end
		if(dsp_wrdy_reg)begin
			$display("Error_T2_rsim:\tCurrent Write-Data is not for DSP!");
		end
		if(pci_wrdy_reg)begin
			$display("Error_T2_rsim:\tCurrent Write-Data is not for PCI!");
		end
		if(ide_wrdy_reg)begin
			$display("Error_T2_rsim:\tCurrent Write-Data is not for IDE!");
		end
		if(sb_wrdy_reg)begin
			$display("Error_T2_rsim:\tCurrent Write-Data is not for SB!");
		end
	end

//	--------------------------------------------------------------------------------------
//write operation control:
always	@(posedge mem_clk)
	if(rst_)begin
		case({cpu_wrdy,gpu_wrdy,video_wrdy,dsp_wrdy,pci_wrdy,ide_wrdy,sb_wrdy,vsb_wrdy})
		8'b1000_0000:	begin
			devs_wrdy_dly	<=	#udly	cpu_wrdy;
			devs_wbe_dly	<=	#udly	cpu_wbe;
			devs_wdata_dly	<=	#udly	cpu_wdata;
		end
		8'b0100_0000:	begin
			devs_wrdy_dly	<=	#udly	gpu_wrdy;
			devs_wbe_dly	<=	#udly	gpu_wbe;
			devs_wdata_dly	<=	#udly	gpu_wdata;
		end
		8'b0010_0000:	begin
			devs_wrdy_dly	<=	#udly	video_wrdy;
			devs_wbe_dly	<=	#udly	video_wbe;
			devs_wdata_dly	<=	#udly	video_wdata;
		end
		8'b0001_0000:	begin
			devs_wrdy_dly	<=	#udly	dsp_wrdy;
			devs_wbe_dly	<=	#udly	dsp_wbe;
			devs_wdata_dly	<=	#udly	dsp_wdata;
		end
		8'b0000_1000:	begin
			devs_wrdy_dly	<=	#udly	pci_wrdy;
			devs_wbe_dly	<=	#udly	pci_wbe;
			devs_wdata_dly	<=	#udly	pci_wdata;
		end
		8'b0000_0100:	begin
			devs_wrdy_dly	<=	#udly	ide_wrdy;
			devs_wbe_dly	<=	#udly	ide_wbe;
			devs_wdata_dly	<=	#udly	ide_wdata;
		end
		8'b0000_0010:	begin
			devs_wrdy_dly	<=	#udly	sb_wrdy;
			devs_wbe_dly	<=	#udly	sb_wbe;
			devs_wdata_dly	<=	#udly	sb_wdata;
		end
		8'b0000_0001:	begin
			devs_wrdy_dly	<=	#udly	vsb_wrdy;
			devs_wbe_dly	<=	#udly	vsb_wbe;
			devs_wdata_dly	<=	#udly	vsb_wdata;
		end
		8'b0000_0000:	begin
			devs_wrdy_dly	<=	#udly	0;
			devs_wbe_dly	<=	#udly	0;
			devs_wdata_dly	<=	#udly	0;
		end
		default:	begin
			$display ("\n%m : at time %t Error_T2_rsim is found!\nSDRAM Memory Interface Write Bus Contest!\n",$time);
			devs_wrdy_dly	<=	#udly	0;
			devs_wbe_dly	<=	#udly	0;
			devs_wdata_dly	<=	#udly	0;
		end
		endcase
	end
//dump write data into file:
always	@(posedge mem_clk)
	if(rst_)begin
		if(wr_gnts)begin
			case({cpu_wgnt,gpu_wgnt,video_wgnt,dsp_wgnt,pci_wgnt,ide_wgnt,sb_wgnt,vsb_wgnt})
			8'b1000_0000:	begin
				`ifdef	SDRAM_WRPATH_DEBUG
					$fdisplay(sdram_wr_fhandle,"\nat time %t CPU Write SDRAM Memory:", $realtime);
				`endif
				devs_waddr_bus	<=	#udly	cpu_addr;
			end
			8'b0100_0000:	begin
				`ifdef	SDRAM_WRPATH_DEBUG
					$fdisplay(sdram_wr_fhandle,"\nat time %t GPU Write SDRAM Memory:", $realtime);
				`endif
				devs_waddr_bus	<=	#udly	gpu_addr;
			end
			8'b0010_0000:	begin
				`ifdef	SDRAM_WRPATH_DEBUG
					$fdisplay(sdram_wr_fhandle,"\nat time %t VIDEO Write SDRAM Memory:", $realtime);
				`endif
				devs_waddr_bus	<=	#udly	video_addr;
			end
			8'b0001_0000:	begin
				`ifdef	SDRAM_WRPATH_DEBUG
					$fdisplay(sdram_wr_fhandle,"\nat time %t DSP Write SDRAM Memory:", $realtime);
				`endif
				devs_waddr_bus	<=	#udly	dsp_addr;
			end
			8'b0000_1000:	begin
				`ifdef	SDRAM_WRPATH_DEBUG
					$fdisplay(sdram_wr_fhandle,"\nat time %t PCI Write SDRAM Memory:", $realtime);
				`endif
				devs_waddr_bus	<=	#udly	pci_waddr;
			end
			8'b0000_0100:	begin
				`ifdef	SDRAM_WRPATH_DEBUG
					$fdisplay(sdram_wr_fhandle,"\nat time %t IDE Write SDRAM Memory:", $realtime);
				`endif
				devs_waddr_bus	<=	#udly	ide_addr;
			end
			8'b0000_0010:	begin
				`ifdef	SDRAM_WRPATH_DEBUG
					$fdisplay(sdram_wr_fhandle,"\nat time %t SB Write SDRAM Memory:", $realtime);
				`endif
				devs_waddr_bus	<=	#udly	sb_addr;
			end
			8'b0000_0001:	begin
				`ifdef	SDRAM_WRPATH_DEBUG
					$fdisplay(sdram_wr_fhandle,"\nat time %t VSB Write SDRAM Memory:", $realtime);
				`endif
				devs_waddr_bus	<=	#udly	vsb_addr;
			end
			default:	begin
				$display ("\n%m : at time %t Error_T2_rsim is found!\nSDRAM Memory Interface Write Bus Contest!\n",$time);
			end
			endcase
		end
		else if(devs_wrdy_dly)begin
			`ifdef	SDRAM_WRPATH_DEBUG
				$fdisplay(sdram_wr_fhandle,"%t waddr=32'h%h, wbe=4'b%b, wdata=32'h%h,",$realtime,
							{2'b00,devs_waddr_bus,2'b00},devs_wbe_dly,devs_wdata_dly);
			`endif
			devs_waddr_bus	<=	#udly	{devs_waddr_bus[27:2],devs_waddr_bus[1:0]+2'b1};
		end
	end

//	--------------------------------------------------------------------------------------
//read path control:
always	@(posedge mem_clk)
	if(rst_)begin
		if(rd_gnts)begin
			case({disp_rgnt,cpu_rgnt,gpu_rgnt,video_rgnt,dsp_rgnt,pci_rgnt,ide_rgnt,sb_rgnt,vsb_rgnt})
			9'b1_0000_0000:	begin
				devs_raddr_buf[devs_raddr_buf_waddr]	<=	#udly	disp_addr;
				devs_raddr_buf_waddr	<=	#udly	devs_raddr_buf_waddr + 1'b1;
			end
			9'b0_1000_0000:	begin
				devs_raddr_buf[devs_raddr_buf_waddr]	<=	#udly	cpu_addr;
				devs_raddr_buf_waddr	<=	#udly	devs_raddr_buf_waddr + 1'b1;
			end
			9'b0_0100_0000:	begin
				devs_raddr_buf[devs_raddr_buf_waddr]	<=	#udly	gpu_addr;
				devs_raddr_buf_waddr	<=	#udly	devs_raddr_buf_waddr + 1'b1;
			end
			9'b0_0010_0000:	begin
				devs_raddr_buf[devs_raddr_buf_waddr]	<=	#udly	video_addr;
				devs_raddr_buf_waddr	<=	#udly	devs_raddr_buf_waddr + 1'b1;
			end
			9'b0_0001_0000:	begin
				devs_raddr_buf[devs_raddr_buf_waddr]	<=	#udly	dsp_addr;
				devs_raddr_buf_waddr	<=	#udly	devs_raddr_buf_waddr + 1'b1;
			end
			9'b0_0000_1000:	begin
				devs_raddr_buf[devs_raddr_buf_waddr]	<=	#udly	pci_raddr;
				devs_raddr_buf_waddr	<=	#udly	devs_raddr_buf_waddr + 1'b1;
			end
			9'b0_0000_0100:	begin
				devs_raddr_buf[devs_raddr_buf_waddr]	<=	#udly	ide_addr;
				devs_raddr_buf_waddr	<=	#udly	devs_raddr_buf_waddr + 1'b1;
			end
			9'b0_0000_0010:	begin
				devs_raddr_buf[devs_raddr_buf_waddr]	<=	#udly	sb_addr;
				devs_raddr_buf_waddr	<=	#udly	devs_raddr_buf_waddr + 1'b1;
			end
			9'b0_0000_0001:	begin
				devs_raddr_buf[devs_raddr_buf_waddr]	<=	#udly	vsb_addr;
				devs_raddr_buf_waddr	<=	#udly	devs_raddr_buf_waddr + 1'b1;
			end
			default:	begin
				$display ("\n%m : at time %t Error_T2_rsim is found!\nSDRAM Memory Interface Read Bus Contest!\n",$time);
			end
			endcase
		end
	end


always	@(posedge mem_clk)
	if(rst_)begin
		if(devs_rrdy)begin
			case({disp_rrdy,cpu_rrdy,gpu_rrdy,video_rrdy,dsp_rrdy,pci_rrdy,ide_rrdy,sb_rrdy,vsb_rrdy})
			9'b1_0000_0000:	begin
				`ifdef	SDRAM_RDPATH_DEBUG
					$fdisplay(sdram_rd_fhandle,"%t Display raddr=32'h%h, rbe=4'b1111, rdata=32'h%h,",$realtime,
						{2'b00,devs_start_raddr[27:2],devs_dw_addr[1:0],2'b00},disp_rdata);
					if(devs_rlast)begin
						$fdisplay(sdram_rd_fhandle,"\nThe next read access:");
					end
				`endif
			end
			9'b0_1000_0000:	begin
				`ifdef	SDRAM_RDPATH_DEBUG
					$fdisplay(sdram_rd_fhandle,"%t CPU raddr=32'h%h, rbe=4'b1111, rdata=32'h%h,",$realtime,
						{2'b00,devs_start_raddr[27:2],devs_dw_addr[1:0],2'b00},cpu_rdata);
					if(devs_rlast)begin
						$fdisplay(sdram_rd_fhandle,"\nThe next read access:");
					end
				`endif
			end
			9'b0_0100_0000:	begin
				`ifdef	SDRAM_RDPATH_DEBUG
					$fdisplay(sdram_rd_fhandle,"%t GPU raddr=32'h%h, rbe=4'b1111, rdata=32'h%h,",$realtime,
						{2'b00,devs_start_raddr[27:2],devs_dw_addr[1:0],2'b00},gpu_rdata);
					if(devs_rlast)begin
						$fdisplay(sdram_rd_fhandle,"\nThe next read access:");
					end
				`endif
			end
			9'b0_0010_0000:	begin
				`ifdef	SDRAM_RDPATH_DEBUG
					$fdisplay(sdram_rd_fhandle,"%t Video raddr=32'h%h, rbe=4'b1111, rdata=32'h%h,",$realtime,
						{2'b00,devs_start_raddr[27:2],devs_dw_addr[1:0],2'b00},video_rdata);
					if(devs_rlast)begin
						$fdisplay(sdram_rd_fhandle,"\nThe next read access:");
					end
				`endif
			end
			9'b0_0001_0000:	begin
				`ifdef	SDRAM_RDPATH_DEBUG
					$fdisplay(sdram_rd_fhandle,"%t DSP raddr=32'h%h, rbe=4'b1111, rdata=32'h%h,",$realtime,
						{2'b00,devs_start_raddr[27:2],devs_dw_addr[1:0],2'b00},dsp_rdata);
					if(devs_rlast)begin
						$fdisplay(sdram_rd_fhandle,"\nThe next read access:");
					end
				`endif
			end
			9'b0_0000_1000:	begin
				`ifdef	SDRAM_RDPATH_DEBUG
					$fdisplay(sdram_rd_fhandle,"%t PCI raddr=32'h%h, rbe=4'b1111, rdata=32'h%h,",$realtime,
						{2'b00,devs_start_raddr[27:2],devs_dw_addr[1:0],2'b00},pci_rdata);
					if(devs_rlast)begin
						$fdisplay(sdram_rd_fhandle,"\nThe next read access:");
					end
				`endif
			end
			9'b0_0000_0100:	begin
				`ifdef	SDRAM_RDPATH_DEBUG
					$fdisplay(sdram_rd_fhandle,"%t IDE raddr=32'h%h, rbe=4'b1111, rdata=32'h%h,",$realtime,
						{2'b00,devs_start_raddr[27:2],devs_dw_addr[1:0],2'b00},ide_rdata);
					if(devs_rlast)begin
						$fdisplay(sdram_rd_fhandle,"\nThe next read access:");
					end
				`endif
			end
			9'b0_0000_0010:	begin
				`ifdef	SDRAM_RDPATH_DEBUG
					$fdisplay(sdram_rd_fhandle,"%t SB raddr=32'h%h, rbe=4'b1111, rdata=32'h%h,",$realtime,
						{2'b00,devs_start_raddr[27:2],devs_dw_addr[1:0],2'b00},sb_rdata);
					if(devs_rlast)begin
						$fdisplay(sdram_rd_fhandle,"\nThe next read access:");
					end
				`endif
			end
			9'b0_0000_0001:	begin
				`ifdef	SDRAM_RDPATH_DEBUG
					$fdisplay(sdram_rd_fhandle,"%t VSB raddr=32'h%h, rbe=4'b1111, rdata=32'h%h,",$realtime,
						{2'b00,devs_start_raddr[27:2],devs_dw_addr[1:0],2'b00},vsb_rdata);
					if(devs_rlast)begin
						$fdisplay(sdram_rd_fhandle,"\nThe next read access:");
					end
				`endif
			end
			default:	begin
				$display ("\n%m : at time %t Error_T2_rsim is found!\nSDRAM Memory Interface Read Bus Contest!\n",$time);
			end
			endcase
		end
	end
	
always	@(posedge mem_clk)
	if(rst_)begin
		if(devs_rlast)begin
			devs_rbl_cnt	<=	#udly	2'b00;
		end
		else if(devs_rrdy)begin
			devs_rbl_cnt	<=	#udly	devs_rbl_cnt + 1'b1;
		end
	end

always	@(posedge mem_clk)
	if(rst_)begin
		if(devs_rlast)begin
			devs_raddr_buf_raddr	<=	#udly	devs_raddr_buf_raddr + 1'b1;
		end
	end


//	--------------------------------------------------------------------------------------
assign	suba_st0		=	suba_arbt_st[7:0];
assign	suba_st1		=	suba_arbt_st[15:8];
assign	suba_st2		=	suba_arbt_st[23:16];
assign	suba_st3		=	suba_arbt_st[31:24];

assign	suba_st0_pci	=	suba_st0[3];
assign	suba_st0_dsp	=	suba_st0[2];
assign	suba_st0_video	=	suba_st0[1];
assign	suba_st0_gpu	=	suba_st0[0];

assign	suba_st1_pci	=	suba_st1[3];
assign	suba_st1_dsp	=	suba_st1[2];
assign	suba_st1_video	=	suba_st1[1];
assign	suba_st1_gpu	=	suba_st1[0];

assign	suba_st2_pci	=	suba_st2[3];
assign	suba_st2_dsp	=	suba_st2[2];
assign	suba_st2_video	=	suba_st2[1];
assign	suba_st2_gpu	=	suba_st2[0];

assign	suba_st3_pci	=	suba_st3[3];
assign	suba_st3_dsp	=	suba_st3[2];
assign	suba_st3_video	=	suba_st3[1];
assign	suba_st3_gpu	=	suba_st3[0];

assign	suba_st0_err	=	suba_st0_pci 	& (suba_st0_dsp	| suba_st0_video | suba_st0_gpu)	|
							suba_st0_dsp 	& (suba_st0_pci | suba_st0_video | suba_st0_gpu)	|
							suba_st0_video	& (suba_st0_pci | suba_st0_dsp | suba_st0_gpu)		|
							suba_st0_gpu 	& (suba_st0_pci | suba_st0_dsp | suba_st0_video);

assign	suba_st1_err	=	suba_st1_pci 	& (suba_st1_dsp	| suba_st1_video | suba_st1_gpu)	|
							suba_st1_dsp 	& (suba_st1_pci | suba_st1_video | suba_st1_gpu)	|
							suba_st1_video	& (suba_st1_pci | suba_st1_dsp | suba_st1_gpu)		|
							suba_st1_gpu 	& (suba_st1_pci | suba_st1_dsp | suba_st1_video);

assign	suba_st2_err	=	suba_st2_pci 	& (suba_st2_dsp	| suba_st2_video | suba_st2_gpu)	|
							suba_st2_dsp 	& (suba_st2_pci | suba_st2_video | suba_st2_gpu)	|
							suba_st2_video	& (suba_st2_pci | suba_st2_dsp | suba_st2_gpu)		|
							suba_st2_gpu 	& (suba_st2_pci | suba_st2_dsp | suba_st2_video);

assign	suba_st3_err	=	suba_st3_pci 	& (suba_st3_dsp	| suba_st3_video | suba_st3_gpu)	|
							suba_st3_dsp 	& (suba_st3_pci | suba_st3_video | suba_st3_gpu)	|
							suba_st3_video	& (suba_st3_pci | suba_st3_dsp | suba_st3_gpu)		|
							suba_st3_gpu 	& (suba_st3_pci | suba_st3_dsp | suba_st3_video);

//	--------------------------------------------------------------------------------------
assign	subb_st0		=	subb_arbt_st[7:0];
assign	subb_st1		=	subb_arbt_st[15:8];
assign	subb_st2		=	subb_arbt_st[23:16];
assign	subb_st3		=	subb_arbt_st[31:24];

assign	subb_st0_vsb	=	subb_st0[2];
assign	subb_st0_sb		=	subb_st0[1];
assign	subb_st0_ide	=	subb_st0[0];

assign	subb_st1_vsb	=	subb_st1[2];
assign	subb_st1_sb		=	subb_st1[1];
assign	subb_st1_ide	=	subb_st1[0];

assign	subb_st2_vsb	=	subb_st2[2];
assign	subb_st2_sb		=	subb_st2[1];
assign	subb_st2_ide	=	subb_st2[0];

assign	subb_st3_vsb	=	subb_st3[2];
assign	subb_st3_sb		=	subb_st3[1];
assign	subb_st3_ide	=	subb_st3[0];

assign	subb_st0_err	=	subb_st0_vsb 	& (subb_st0_sb | subb_st0_ide)	|
							subb_st0_sb 	& (subb_st0_vsb | subb_st0_ide)	|
							subb_st0_ide 	& (subb_st0_vsb | subb_st0_sb);

assign	subb_st1_err	=	subb_st1_vsb 	& (subb_st1_sb | subb_st1_ide)	|
							subb_st1_sb 	& (subb_st1_vsb | subb_st1_ide)	|
							subb_st1_ide 	& (subb_st1_vsb | subb_st1_sb);

assign	subb_st2_err	=	subb_st2_vsb 	& (subb_st2_sb | subb_st2_ide)	|
							subb_st2_sb 	& (subb_st2_vsb | subb_st2_ide)	|
							subb_st2_ide 	& (subb_st2_vsb | subb_st2_sb);

assign	subb_st3_err	=	subb_st3_vsb 	& (subb_st3_sb | subb_st3_ide)	|
							subb_st3_sb 	& (subb_st3_vsb | subb_st3_ide)	|
							subb_st3_ide 	& (subb_st3_vsb | subb_st3_sb);

//	--------------------------------------------------------------------------------------
assign	main_st0		=	main_arbt_st[7:0];
assign	main_st1		=	main_arbt_st[15:8];
assign	main_st2		=	main_arbt_st[23:16];
assign	main_st3		=	main_arbt_st[31:24];

assign	main_st0_subb	=	main_st0[4];
assign	main_st0_suba	=	main_st0[3];
assign	main_st0_cpu	=	main_st0[2];
assign	main_st0_disp	=	main_st0[1];
assign	main_st0_ref	=	main_st0[0];

assign	main_st1_subb	=	main_st1[4];
assign	main_st1_suba	=	main_st1[3];
assign	main_st1_cpu	=	main_st1[2];
assign	main_st1_disp	=	main_st1[1];
assign	main_st1_ref	=	main_st1[0];

assign	main_st2_subb	=	main_st2[4];
assign	main_st2_suba	=	main_st2[3];
assign	main_st2_cpu	=	main_st2[2];
assign	main_st2_disp	=	main_st2[1];
assign	main_st2_ref	=	main_st2[0];

assign	main_st3_subb	=	main_st3[4];
assign	main_st3_suba	=	main_st3[3];
assign	main_st3_cpu	=	main_st3[2];
assign	main_st3_disp	=	main_st3[1];
assign	main_st3_ref	=	main_st3[0];

assign	main_st0_err	=	main_st0_subb 	& (main_st0_suba | main_st0_cpu | main_st0_disp | main_st0_ref)	|
							main_st0_suba 	& (main_st0_subb | main_st0_cpu | main_st0_disp | main_st0_ref)	|
							main_st0_cpu 	& (main_st0_subb | main_st0_suba | main_st0_disp | main_st0_ref)	|
							main_st0_disp 	& (main_st0_subb | main_st0_suba | main_st0_cpu | main_st0_ref)	|
							main_st0_ref 	& (main_st0_subb | main_st0_suba | main_st0_cpu | main_st0_disp);

assign	main_st1_err	=	main_st1_subb 	& (main_st1_suba | main_st1_cpu | main_st1_disp | main_st1_ref)	|
							main_st1_suba 	& (main_st1_subb | main_st1_cpu | main_st1_disp | main_st1_ref)	|
							main_st1_cpu 	& (main_st1_subb | main_st1_suba | main_st1_disp | main_st1_ref)	|
							main_st1_disp 	& (main_st1_subb | main_st1_suba | main_st1_cpu | main_st1_ref)	|
							main_st1_ref 	& (main_st1_subb | main_st1_suba | main_st1_cpu | main_st1_disp);

assign	main_st2_err	=	main_st2_subb 	& (main_st2_suba | main_st2_cpu | main_st2_disp | main_st2_ref)	|
							main_st2_suba 	& (main_st2_subb | main_st2_cpu | main_st2_disp | main_st2_ref)	|
							main_st2_cpu 	& (main_st2_subb | main_st2_suba | main_st2_disp | main_st2_ref)	|
							main_st2_disp 	& (main_st2_subb | main_st2_suba | main_st2_cpu | main_st2_ref)	|
							main_st2_ref 	& (main_st2_subb | main_st2_suba | main_st2_cpu | main_st2_disp);

assign	main_st3_err	=	main_st3_subb 	& (main_st3_suba | main_st3_cpu | main_st3_disp | main_st3_ref)	|
							main_st3_suba 	& (main_st3_subb | main_st3_cpu | main_st3_disp | main_st3_ref)	|
							main_st3_cpu 	& (main_st3_subb | main_st3_suba | main_st3_disp | main_st3_ref)	|
							main_st3_disp 	& (main_st3_subb | main_st3_suba | main_st3_cpu | main_st3_ref)	|
							main_st3_ref 	& (main_st3_subb | main_st3_suba | main_st3_cpu | main_st3_disp);

//	--------------------------------------------------------------------------------------
assign	wrlock_timer_enable		=	wrlock_timer_en && !wrlock_timer_en_dly;
assign	wrlock_timer_disable	=	!wrlock_timer_en && wrlock_timer_en_dly;

assign	wrlock_resume_enable	=	wrlock_resume_en && !wrlock_resume_en_dly;
assign	wrlock_resume_disable	=	!wrlock_resume_en && wrlock_resume_en_dly;

assign	wrlock_occur			=	wrlock_tag && !wrlock_tag_dly;

assign	wrlock_ide				=	ram_wrlock_id[7];
assign	wrlock_sb				=	ram_wrlock_id[6];
assign	wrlock_dsp				=	ram_wrlock_id[5];
assign	wrlock_vsb				=	ram_wrlock_id[4];
assign	wrlock_video			=	ram_wrlock_id[3];
assign	wrlock_gpu				=	ram_wrlock_id[2];
assign	wrlock_pci				=	ram_wrlock_id[1];
assign	wrlock_cpu				=	ram_wrlock_id[0];

//	--------------------------------------------------------------------------------------
assign	disp_rgnt	=	disp_ack;
assign	cpu_rgnt	=	!cpu_rw && cpu_ack;
assign	cpu_wgnt	=	cpu_rw && cpu_ack;
assign	gpu_rgnt	=	!gpu_rw && gpu_ack;
assign	gpu_wgnt	=	gpu_rw && gpu_ack;
assign	video_rgnt	=	!video_rw && video_ack;
assign	video_wgnt	=	video_rw && video_ack;
assign	dsp_rgnt	=	!dsp_rw && dsp_ack;
assign	dsp_wgnt	=	dsp_rw && dsp_ack;
assign	pci_rgnt	=	pci_rack;
assign	pci_wgnt	=	pci_wack;
assign	ide_rgnt	=	!ide_rw && ide_ack;
assign	ide_wgnt	=	ide_rw && ide_ack;
assign	sb_rgnt		=	!sb_rw && sb_ack;
assign	sb_wgnt		=	sb_rw && sb_ack;
assign	vsb_rgnt	=	!vsb_rw && vsb_ack;
assign	vsb_wgnt	=	vsb_rw && vsb_ack;

assign	wr_gnts		=	cpu_wgnt ||
						gpu_wgnt || video_wgnt || dsp_wgnt || pci_wgnt ||
						ide_wgnt || sb_wgnt || vsb_wgnt;

assign	wr_mux		=	{cpu_wgnt,
						gpu_wgnt,video_wgnt,dsp_wgnt,pci_wgnt,
						ide_wgnt,sb_wgnt,vsb_wgnt};

assign	rd_gnts		=	disp_rgnt || cpu_rgnt ||
						gpu_rgnt || video_rgnt || dsp_rgnt || pci_rgnt ||
						ide_rgnt || sb_rgnt || vsb_rgnt;

assign	rd_pipeline_in	=	rd_gnts;

assign	rd_pipeline_out	=	disp_rlast || cpu_rlast ||
							gpu_rlast || video_rlast || dsp_rlast || pci_rlast ||
							ide_rlast || sb_rlast || vsb_rlast;
							
assign	rd_pipeline_rdata	=	rd_pipeline[rd_pipeline_raddr];

//	--------------------------------------------------------------------------------------
assign	disp_rd_en		=	rd_pipeline_rdata[8];
assign	cpu_rd_en		=	rd_pipeline_rdata[7];
assign	gpu_rd_en		=	rd_pipeline_rdata[6];
assign	video_rd_en		=	rd_pipeline_rdata[5];
assign	dsp_rd_en		=	rd_pipeline_rdata[4];
assign	pci_rd_en		=	rd_pipeline_rdata[3];
assign	ide_rd_en		=	rd_pipeline_rdata[2];
assign	sb_rd_en		=	rd_pipeline_rdata[1];
assign	vsb_rd_en		=	rd_pipeline_rdata[0];

assign	disp_rd_err		=	disp_rd_en && (cpu_rrdy ||
									gpu_rrdy || video_rrdy || dsp_rrdy || pci_rrdy ||
									ide_rrdy || sb_rrdy || vsb_rrdy);

assign	cpu_rd_err		=	cpu_rd_en && (disp_rrdy ||
									gpu_rrdy || video_rrdy || dsp_rrdy || pci_rrdy ||
									ide_rrdy || sb_rrdy || vsb_rrdy);

assign	gpu_rd_err		=	gpu_rd_en && (disp_rrdy || cpu_rrdy ||
									video_rrdy || dsp_rrdy || pci_rrdy ||
									ide_rrdy || sb_rrdy || vsb_rrdy);

assign	video_rd_err	=	video_rd_en && (disp_rrdy || cpu_rrdy ||
									gpu_rrdy || dsp_rrdy || pci_rrdy ||
									ide_rrdy || sb_rrdy || vsb_rrdy);

assign	dsp_rd_err		=	dsp_rd_en && (disp_rrdy || cpu_rrdy ||
									gpu_rrdy || video_rrdy || pci_rrdy ||
									ide_rrdy || sb_rrdy || vsb_rrdy);

assign	pci_rd_err		=	pci_rd_en && (disp_rrdy || cpu_rrdy ||
									gpu_rrdy || video_rrdy || dsp_rrdy ||
									ide_rrdy || sb_rrdy || vsb_rrdy);

assign	ide_rd_err		=	ide_rd_en && (disp_rrdy || cpu_rrdy ||
									gpu_rrdy || video_rrdy || dsp_rrdy || pci_rrdy ||
									sb_rrdy || vsb_rrdy);

assign	sb_rd_err		=	sb_rd_en && (disp_rrdy || cpu_rrdy ||
									gpu_rrdy || video_rrdy || dsp_rrdy || pci_rrdy ||
									ide_rrdy || vsb_rrdy);

assign	vsb_rd_err		=	vsb_rd_en && (disp_rrdy || cpu_rrdy ||
									gpu_rrdy || video_rrdy || dsp_rrdy || pci_rrdy ||
									ide_rrdy || sb_rrdy);

//	--------------------------------------------------------------------------------------
assign	cpu_wr_en		=	wr_pipeline[7];
assign	gpu_wr_en		=	wr_pipeline[6];
assign	video_wr_en		=	wr_pipeline[5];
assign	dsp_wr_en		=	wr_pipeline[4];
assign	pci_wr_en		=	wr_pipeline[3];
assign	ide_wr_en		=	wr_pipeline[2];
assign	sb_wr_en		=	wr_pipeline[1];
assign	vsb_wr_en		=	wr_pipeline[0];

assign	cpu_wr_err		=	cpu_wr_en && (
										gpu_wrdy_reg || video_wrdy_reg || dsp_wrdy_reg || pci_wrdy_reg ||
										ide_wrdy_reg || sb_wrdy_reg || vsb_wrdy_reg);

assign	gpu_wr_err		=	gpu_wr_en && (cpu_wrdy_reg ||
										video_wrdy_reg || dsp_wrdy_reg || pci_wrdy_reg ||
										ide_wrdy_reg || sb_wrdy_reg || vsb_wrdy_reg);

assign	video_wr_err	=	video_wr_en && (cpu_wrdy_reg ||
										gpu_wrdy_reg || dsp_wrdy_reg || pci_wrdy_reg ||
										ide_wrdy_reg || sb_wrdy_reg || vsb_wrdy_reg);

assign	dsp_wr_err		=	dsp_wr_en && (cpu_wrdy_reg ||
										gpu_wrdy_reg || video_wrdy_reg || pci_wrdy_reg ||
										ide_wrdy_reg || sb_wrdy_reg || vsb_wrdy_reg);

assign	pci_wr_err		=	pci_wr_en && (cpu_wrdy_reg ||
										gpu_wrdy_reg || video_wrdy_reg || dsp_wrdy_reg ||
										ide_wrdy_reg || sb_wrdy_reg || vsb_wrdy_reg);

assign	ide_wr_err		=	ide_wr_en && (cpu_wrdy_reg ||
										gpu_wrdy_reg || video_wrdy_reg || dsp_wrdy_reg || pci_wrdy_reg ||
										sb_wrdy_reg || vsb_wrdy_reg);

assign	sb_wr_err		=	sb_wr_en && (cpu_wrdy_reg ||
										gpu_wrdy_reg || video_wrdy_reg || dsp_wrdy_reg || pci_wrdy_reg ||
										ide_wrdy_reg || vsb_wrdy_reg);

assign	vsb_wr_err		=	vsb_wr_en && (cpu_wrdy_reg ||
										gpu_wrdy_reg || video_wrdy_reg || dsp_wrdy_reg || pci_wrdy_reg ||
										ide_wrdy_reg || sb_wrdy_reg);


//	--------------------------------------------------------------------------------------
//write operation control:

//	--------------------------------------------------------------------------------------
//read operation control:
assign	devs_rrdy	=	disp_rrdy || cpu_rrdy || gpu_rrdy || video_rrdy ||
						dsp_rrdy || pci_rrdy || ide_rrdy || sb_rrdy || vsb_rrdy;

assign	devs_rlast	=	disp_rlast || cpu_rlast || gpu_rlast || video_rlast ||
						dsp_rlast || pci_rlast || ide_rlast || sb_rlast || vsb_rlast;

assign	devs_start_raddr	=	devs_raddr_buf[devs_raddr_buf_raddr];

assign	devs_dw_addr		=	devs_start_raddr[1:0] + devs_rbl_cnt;

`endif   //joyous
endmodule


