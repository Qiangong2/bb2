/******************************************************************************
	  (c) copyrights 2000-. All rights reserved

	   T-Square Design, Inc.

	  Company confidential and Properietary	information.
	  This information may not be disclosed	to unauthorized individual.
*******************************************************************************/

/*******************************************************************************
 *
 *       PROJECT:M3357
 *
 *       FILE NAME: sdram_dimm_monitor.v
 *
 *       DESCRIPTION: External SDRAM Memory Bus Monitor
 *						For checking the external SDRAM memory bus operations
 *
 *
 *       AUTHOR: Figo OU
 *
 *       HISTORY:   11/04/02         initial version
 *
 *********************************************************************************/

module	sdram_dimm_monitor(
	//System IO:
	ram_timeparam,
	ram_mode_set,
	ram_16bits_mode,
//	ram_pre_oe_en,
	ram_post_oe_en,
	ram_r2w_ctrl,
	//SDRAM Memory Bus:
	ram_cke,
	ram_cs_,
	ram_ras_,
	ram_cas_,
	ram_we_,
	ram_dqm,
	ram_ba,
	ram_addr,
	ram_wdata,
	ram_rdata,
	ram_dq_oe_,
	//dimm state:
	dimm_is_idle,
	ext_dimm_st,
	//Others:
	rst_,
	mem_clk
	);

parameter	udly	=	1'b1;

//	-----------------------------------------------------------------------------------
//System IO:
input[15:0]	ram_timeparam;
input[11:0]	ram_mode_set;
input		ram_16bits_mode;
//input		ram_pre_oe_en;
input		ram_post_oe_en;
input		ram_r2w_ctrl;

//SDRAM Memory Bus:
input		ram_cke;
input		ram_cs_;
input		ram_ras_;
input		ram_cas_;
input		ram_we_;
input[3:0]	ram_dqm;
input[1:0]	ram_ba;
input[11:0]	ram_addr;
input[31:0]	ram_wdata;
input[31:0]	ram_rdata;
input[31:0]	ram_dq_oe_;

//dimm state:
output		dimm_is_idle;
input		ext_dimm_st;
//Others:
input		rst_,
			mem_clk;

//	-----------------------------------------------------------------------------------
wire	sdram_clk	=	ram_cke && mem_clk;

reg	sdram_pall_en, sdram_mrs_en, sdram_cbr_en, sdram_init_en;
reg[2:0]	sdram_bl;
reg[3:0]	trfc_cnt;
wire		trfc_timeout;

reg[3:0]	trp_cnt;
wire		trp_cnt_timeout;

reg[3:0]	trrd_cnt;
wire		trrd_cnt_timeout;

wire	addr_bit10;
wire	nop_cmd,pall_cmd,mrs_cmd,cbr_cmd,
		pre_cmd,act_cmd,rd_cmd,rdap_cmd,wr_cmd,wrap_cmd;
wire	nop_cmd_do,pall_cmd_do,mrs_cmd_do,cbr_cmd_do,
		pre_cmd_do,act_cmd_do,rd_cmd_do,rdap_cmd_do,wr_cmd_do,wrap_cmd_do;

wire	bk0_sel,bk1_sel,bk2_sel,bk3_sel;

wire	bk0_pre_cmd,
		bk0_act_cmd,
		bk0_rd_cmd,
		bk0_rdap_cmd,
		bk0_wr_cmd,
		bk0_wrap_cmd;

wire	bk1_pre_cmd,
		bk1_act_cmd,
		bk1_rd_cmd,
		bk1_rdap_cmd,
		bk1_wr_cmd,
		bk1_wrap_cmd;

wire	bk2_pre_cmd,
		bk2_act_cmd,
		bk2_rd_cmd,
		bk2_rdap_cmd,
		bk2_wr_cmd,
		bk2_wrap_cmd;


wire	bk3_pre_cmd,
		bk3_act_cmd,
		bk3_rd_cmd,
		bk3_rdap_cmd,
		bk3_wr_cmd,
		bk3_wrap_cmd;

wire	bk0_is_idle,
		bk1_is_idle,
		bk2_is_idle,
		bk3_is_idle,
		bks_is_idle;

wire		Tcmd_setup;
wire[3:0]	Tcl,Trcd,Trp,Trrd,Trc,Tras;

wire[16:0]	sdram_input_bus;
reg[16:0]	sdram_input_bus_dly;
wire		sdram_2tcmd_err;

reg			dimm_is_idle;

initial	begin
	sdram_pall_en	=	0;
	sdram_mrs_en	=	0;
	sdram_cbr_en	=	0;
	sdram_init_en	=	0;
	sdram_bl		=	3'bxxx;
	trfc_cnt		=	4'h0;
	trp_cnt			=	4'h0;
	trrd_cnt		=	4'h0;
end

initial	begin
	sdram_input_bus_dly	=	17'hx;
	dimm_is_idle		=	1'b0;
end

always	@(posedge sdram_clk)
	if(!ram_cs_)begin
		if(pall_cmd)begin
			dimm_is_idle		=	1'b1;
		end
		else if(act_cmd)begin
			dimm_is_idle		=	1'b0;
		end
	end


always	@(posedge sdram_clk)
	if(!ext_dimm_st)begin
		if(!ram_cs_)begin
			if(pre_cmd || act_cmd || rd_cmd || rdap_cmd || wr_cmd || wrap_cmd)begin
				$display("\n%m\tAt Time: %t\nError_T2_rsim: 3357 SDRAM DIMM Switching Design Desire Violation, External SDRAM DIMM is not closed!\n",$realtime);
			end
		end
	end


always	@(posedge sdram_clk)
	if(pall_cmd_do)begin
		sdram_pall_en	<=	#udly	1'b1;
	end

always	@(posedge sdram_clk)
	if(mrs_cmd_do)begin
		if(sdram_pall_en)begin
			sdram_mrs_en	<=	#udly	1'b1;
			sdram_bl		<=	#udly	ram_addr[2:0];
		end
		else	begin
			$display("\n%m\tAt Time: %t\nError_T2_rsim: PALL is needed before MRS\n",$realtime);
		end
	end

always	@(posedge sdram_clk)
	if(mrs_cmd_do && sdram_pall_en)begin
		trfc_cnt	<=	#udly	Trc;
	end
	else if(!trfc_timeout)begin
		trfc_cnt	<=	#udly	trfc_cnt - 1'b1;
	end

always	@(posedge sdram_clk)
	if(pall_cmd_do)begin
		trp_cnt	<=	#udly	Trp + 1'b1;
	end
	else if(!trp_cnt_timeout)begin
		trp_cnt	<=	#udly	trp_cnt - 1'b1;
	end

always	@(posedge sdram_clk)
	if(cbr_cmd_do)begin
		if(sdram_pall_en)begin
			sdram_cbr_en	<=	#udly	1'b1;
		end
		else	begin
			$display("\n%m\tAt Time: %t\nError_T2_rsim: PALL is needed before CBR\n",$realtime);
		end
	end

always	@(posedge sdram_clk)
	if(sdram_pall_en && sdram_mrs_en && sdram_cbr_en)begin
		sdram_init_en	<=	#udly	1'b1;
	end
	else	begin
		sdram_init_en	<=	#udly	1'b0;
	end

always	@(posedge sdram_clk)
	if(!sdram_init_en)begin
			if(act_cmd_do)begin
				$display("\n%m\tAt Time: %t\nError_T2_rsim: SDRAM Initialization is needed before ACT\n",$realtime);
			end
			if(rd_cmd_do)begin
				$display("\n%m\tAt Time: %t\nError_T2_rsim: SDRAM Initialization is needed before READ\n",$realtime);
			end
			if(rdap_cmd_do)begin
				$display("\n%m\tAt Time: %t\nError_T2_rsim: SDRAM Initialization is needed before READAP\n",$realtime);
			end
			if(wr_cmd_do)begin
				$display("\n%m\tAt Time: %t\nError_T2_rsim: SDRAM Initialization is needed before WRITE\n",$realtime);
			end
			if(wrap_cmd_do)begin
				$display("\n%m\tAt Time: %t\nError_T2_rsim: SDRAM Initialization is needed before WRITEAP\n",$realtime);
			end
	end

always	@(posedge sdram_clk)
	if(!trfc_timeout)begin
			if(mrs_cmd_do)begin
				$display("\n%m\tAt Time: %t\nError_T2_rsim: Trfc Timing Violation During Refresh Before MRS\n",$realtime);
			end
			if(cbr_cmd_do)begin
				$display("\n%m\tAt Time: %t\nError_T2_rsim: Trfc Timing Violation During Refresh Before Refresh\n",$realtime);
			end
			if(pall_cmd_do || pre_cmd_do)begin
				$display("\n%m\tAt Time: %t\nError_T2_rsim: Trfc Timing Violation During Refresh Before PRE or PALL\n",$realtime);
			end
			if(act_cmd_do)begin
				$display("\n%m\tAt Time: %t\nError_T2_rsim: Trfc Timing Violation During Refresh Before ACT\n",$realtime);
			end
			if(rd_cmd_do)begin
				$display("\n%m\tAt Time: %t\nError_T2_rsim: Trfc Timing Violation During Refresh Before READ\n",$realtime);
			end
			if(rdap_cmd_do)begin
				$display("\n%m\tAt Time: %t\nError_T2_rsim: Trfc Timing Violation During Refresh Before READAP\n",$realtime);
			end
			if(wr_cmd_do)begin
				$display("\n%m\tAt Time: %t\nError_T2_rsim: Trfc Timing Violation During Refresh Before WRITE\n",$realtime);
			end
			if(wrap_cmd_do)begin
				$display("\n%m\tAt Time: %t\nError_T2_rsim: Trfc Timing Violation During Refresh Before WRITEAP\n",$realtime);
			end
	end

always	@(posedge sdram_clk)
	if(!trp_cnt_timeout)begin
			if(mrs_cmd_do)begin
				$display("\n%m\tAt Time: %t\nError_T2_rsim: Trp Timing Violation During Precharging Before MRS\n",$realtime);
			end
			if(cbr_cmd_do)begin
				$display("\n%m\tAt Time: %t\nError_T2_rsim: Trp Timing Violation During Precharging Before Refresh\n",$realtime);
			end
			if(pall_cmd_do || pre_cmd_do)begin
				$display("\n%m\tAt Time: %t\nError_T2_rsim: Trp Timing Violation During Precharging Before PRE or PALL\n",$realtime);
			end
			if(act_cmd_do)begin
				$display("\n%m\tAt Time: %t\nError_T2_rsim: Trp Timing Violation During Precharging Before ACT\n",$realtime);
			end
			if(rd_cmd_do)begin
				$display("\n%m\tAt Time: %t\nError_T2_rsim: Trp Timing Violation During Precharging Before READ\n",$realtime);
			end
			if(rdap_cmd_do)begin
				$display("\n%m\tAt Time: %t\nError_T2_rsim: Trp Timing Violation During Precharging Before READAP\n",$realtime);
			end
			if(wr_cmd_do)begin
				$display("\n%m\tAt Time: %t\nError_T2_rsim: Trp Timing Violation During Precharging Before WRITE\n",$realtime);
			end
			if(wrap_cmd_do)begin
				$display("\n%m\tAt Time: %t\nError_T2_rsim: Trp Timing Violation During Precharging Before WRITEAP\n",$realtime);
			end
	end

always	@(posedge sdram_clk)
	if(!bks_is_idle)begin
		if(cbr_cmd_do)begin
			$display("\n%m\tAt Time: %t\nError_T2_rsim: Illegal SDRAM Command, All Banks Should Stay In IDLE State Before Refresh\n",$realtime);
		end
		if(mrs_cmd_do)begin
			$display("\n%m\tAt Time: %t\nError_T2_rsim: Illegal SDRAM Command, All Banks Should Stay In IDLE State Before MRS\n",$realtime);
		end
	end

always	@(posedge sdram_clk)
	sdram_input_bus_dly	<=	#udly	sdram_input_bus;

always	@(posedge sdram_clk)
	if(sdram_2tcmd_err)begin
		$display("\n%m\tAt Time: %t\nError_T2_rsim: SDRAM 2T Command Cycles Contol Violation\n",$realtime);
	end

always	@(posedge sdram_clk)
	if(act_cmd_do)begin
		trrd_cnt	<=	#udly	Trrd;
	end
	else if(!trrd_cnt_timeout)begin
		trrd_cnt	<=	#udly	trrd_cnt - 1'b1;
	end

always	@(posedge sdram_clk)
	if(!trrd_cnt_timeout)begin
		if(act_cmd_do)begin
			$display("\n%m\tAt Time: %t\nError_T2_rsim: SDRAM Trrd Timing Violation\n",$realtime);
		end
	end

//	-----------------------------------------------------------------------------------
//SDRAM Commands Decode:
assign	addr_bit10	=	ram_addr[10];
assign	nop_cmd		=	ram_ras_ && ram_cas_ && ram_we_;
assign	pall_cmd	=	!ram_ras_ && ram_cas_ && !ram_we_ && addr_bit10;
assign	mrs_cmd		=	!ram_ras_ && !ram_cas_ && !ram_we_;
assign	cbr_cmd		=	!ram_ras_ && !ram_cas_ && ram_we_;
assign	pre_cmd		=	!ram_ras_ && ram_cas_ && !ram_we_ && !addr_bit10;
assign	act_cmd		=	!ram_ras_ && ram_cas_ && ram_we_;
assign	rd_cmd		=	ram_ras_ && !ram_cas_ && ram_we_ && !addr_bit10;
assign	rdap_cmd	=	ram_ras_ && !ram_cas_ && ram_we_ && addr_bit10;
assign	wr_cmd		=	ram_ras_ && !ram_cas_ && !ram_we_ && !addr_bit10;
assign	wrap_cmd	=	ram_ras_ && !ram_cas_ && !ram_we_ && addr_bit10;

assign	nop_cmd_do	=	!ram_cs_ && nop_cmd;
assign	pall_cmd_do	=	!ram_cs_ && pall_cmd;
assign	mrs_cmd_do	=	!ram_cs_ && mrs_cmd;
assign	cbr_cmd_do	=	!ram_cs_ && cbr_cmd;
assign	pre_cmd_do	=	!ram_cs_ && pre_cmd;
assign	act_cmd_do	=	!ram_cs_ && act_cmd;
assign	rd_cmd_do	=	!ram_cs_ && rd_cmd;
assign	rdap_cmd_do	=	!ram_cs_ && rdap_cmd;
assign	wr_cmd_do	=	!ram_cs_ && wr_cmd;
assign	wrap_cmd_do	=	!ram_cs_ && wrap_cmd;

assign	bk0_sel		=	ram_ba == 2'b00;
assign	bk1_sel		=	ram_ba == 2'b01;
assign	bk2_sel		=	ram_ba == 2'b10;
assign	bk3_sel		=	ram_ba == 2'b11;

assign	bk0_pre_cmd		=	(bk0_sel && pre_cmd_do) || pall_cmd_do;
assign	bk0_act_cmd		=	bk0_sel && act_cmd_do;
assign	bk0_rd_cmd		=	bk0_sel && rd_cmd_do;
assign	bk0_rdap_cmd	=	bk0_sel && rdap_cmd_do;
assign	bk0_wr_cmd		=	bk0_sel && wr_cmd_do;
assign	bk0_wrap_cmd	=	bk0_sel && wrap_cmd_do;

assign	bk1_pre_cmd		=	(bk1_sel && pre_cmd_do) || pall_cmd_do;
assign	bk1_act_cmd		=	bk1_sel && act_cmd_do;
assign	bk1_rd_cmd		=	bk1_sel && rd_cmd_do;
assign	bk1_rdap_cmd	=	bk1_sel && rdap_cmd_do;
assign	bk1_wr_cmd		=	bk1_sel && wr_cmd_do;
assign	bk1_wrap_cmd	=	bk1_sel && wrap_cmd_do;

assign	bk2_pre_cmd		=	(bk2_sel && pre_cmd_do) || pall_cmd_do;
assign	bk2_act_cmd		=	bk2_sel && act_cmd_do;
assign	bk2_rd_cmd		=	bk2_sel && rd_cmd_do;
assign	bk2_rdap_cmd	=	bk2_sel && rdap_cmd_do;
assign	bk2_wr_cmd		=	bk2_sel && wr_cmd_do;
assign	bk2_wrap_cmd	=	bk2_sel && wrap_cmd_do;

assign	bk3_pre_cmd		=	(bk3_sel && pre_cmd_do) || pall_cmd_do;
assign	bk3_act_cmd		=	bk3_sel && act_cmd_do;
assign	bk3_rd_cmd		=	bk3_sel && rd_cmd_do;
assign	bk3_rdap_cmd	=	bk3_sel && rdap_cmd_do;
assign	bk3_wr_cmd		=	bk3_sel && wr_cmd_do;
assign	bk3_wrap_cmd	=	bk3_sel && wrap_cmd_do;


//	-----------------------------------------------------------------------------------
assign	Tcl			=	{2'b00,ram_timeparam[1:0]};
assign	Trcd		=	{2'b00,ram_timeparam[3:2]};
assign	Trp			=	{2'b00,ram_timeparam[5:4]};
assign	Trrd		=	{3'b000,ram_timeparam[6]} + 1'b1;
assign	Trc			=	ram_timeparam[11:8];
assign	Tras		=	Trc - Trp - 4'h3;
assign	Tcmd_setup	=	ram_timeparam[7];

assign	trfc_timeout	=	trfc_cnt == 4'h0;
assign	trp_cnt_timeout	=	trp_cnt == 4'h0;

assign	trrd_cnt_timeout	=	trrd_cnt == 4'h0;

assign	bks_is_idle	=	bk0_is_idle && bk1_is_idle && bk2_is_idle && bk3_is_idle;

assign	sdram_input_bus	=	{ram_ras_,ram_cas_,ram_we_,ram_ba[1:0],ram_addr[11:0]};
assign	sdram_2tcmd_err	=	Tcmd_setup && !ram_cs_ &&
							!(sdram_input_bus == sdram_input_bus_dly);

//	-----------------------------------------------------------------------------------
sdram_bank_monitor	sdram_bank0_monitor(
	.bank_pre_cmd	(bk0_pre_cmd		),
	.bank_act_cmd	(bk0_act_cmd		),
	.bank_rd_cmd	(bk0_rd_cmd			),
	.bank_rdap_cmd	(bk0_rdap_cmd		),
	.bank_wr_cmd	(bk0_wr_cmd			),
	.bank_wrap_cmd	(bk0_wrap_cmd		),
	.time_trcd		(Trcd				),
	.time_trp		(Trp				),
	.time_trc		(Trc				),
	.time_tras		(Tras				),
	.bank_is_idle	(bk0_is_idle		),
	.sdram_clk		(sdram_clk			)
	);

sdram_bank_monitor	sdram_bank1_monitor(
	.bank_pre_cmd	(bk1_pre_cmd		),
	.bank_act_cmd	(bk1_act_cmd		),
	.bank_rd_cmd	(bk1_rd_cmd			),
	.bank_rdap_cmd	(bk1_rdap_cmd		),
	.bank_wr_cmd	(bk1_wr_cmd			),
	.bank_wrap_cmd	(bk1_wrap_cmd		),
	.time_trcd		(Trcd				),
	.time_trp		(Trp				),
	.time_trc		(Trc				),
	.time_tras		(Tras				),
	.bank_is_idle	(bk1_is_idle		),
	.sdram_clk		(sdram_clk			)
	);

sdram_bank_monitor	sdram_bank2_monitor(
	.bank_pre_cmd	(bk2_pre_cmd		),
	.bank_act_cmd	(bk2_act_cmd		),
	.bank_rd_cmd	(bk2_rd_cmd			),
	.bank_rdap_cmd	(bk2_rdap_cmd		),
	.bank_wr_cmd	(bk2_wr_cmd			),
	.bank_wrap_cmd	(bk2_wrap_cmd		),
	.time_trcd		(Trcd				),
	.time_trp		(Trp				),
	.time_trc		(Trc				),
	.time_tras		(Tras				),
	.bank_is_idle	(bk2_is_idle		),
	.sdram_clk		(sdram_clk			)
	);

sdram_bank_monitor	sdram_bank3_monitor(
	.bank_pre_cmd	(bk3_pre_cmd		),
	.bank_act_cmd	(bk3_act_cmd		),
	.bank_rd_cmd	(bk3_rd_cmd			),
	.bank_rdap_cmd	(bk3_rdap_cmd		),
	.bank_wr_cmd	(bk3_wr_cmd			),
	.bank_wrap_cmd	(bk3_wrap_cmd		),
	.time_trcd		(Trcd				),
	.time_trp		(Trp				),
	.time_trc		(Trc				),
	.time_tras		(Tras				),
	.bank_is_idle	(bk3_is_idle		),
	.sdram_clk		(sdram_clk			)
	);


endmodule


