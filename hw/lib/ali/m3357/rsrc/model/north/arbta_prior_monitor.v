/******************************************************************************
	  (c) copyrights 2000-. All rights reserved
 
	   T-Square Design, Inc.
 
	  Company confidential and Properietary	information.
	  This information may not be disclosed	to unauthorized individual.	  
*******************************************************************************/

/*******************************************************************************
 *
 *       PROJECT:M6304
 *
 *       FILE NAME: arbta_prior_monitor.v
 *
 *       DESCRIPTION: SDRAM Memory Bus SUB-A Arbitor Priority Monitor
 *						Five source devices:	GPU Engine, Video Engine, DSP,
 *												PCI Read Buffer, PCI Write Buffer
 *					Priority Mode:
 *								00: Rotation:
 *								GPU -> Video -> DSP -> PCI_RD -> PCI_WR
 *								01: Fixed_1st:
 *								GPU >> Video >> DSP >> PCI_RD >> PCI_WR
 *								10: Fixed_2nd:
 *								DSP >> GPU >> Video >> PCI_RD >> PCI_WR
 *								11: Fixed_3rd:
 *								PCI_RD >> PCI_WR >> GPU >> Video >> DSP
 *
 *       AUTHOR: Figo OU
 *
 *       HISTORY:   10/31/02         initial version
 *      
 *********************************************************************************/

module	arbta_prior_monitor(
	//Configure Ports:
	arbta_prior_mode,
	//Devices Ports:
	//GPU:
	gpu_req,
	gpu_ack,
	//Video:
	video_req,
	video_ack,
	//DSP:
	dsp_req,
	dsp_ack,
	//PCI_RD:
	pci_rreq,
	pci_rack,
	//PCI_WR:
	pci_wreq,
	pci_wack,
	//For arbtm_prior_monitor:
	arbta_req,
	arbta_ack,
	//Others:
	rst_,
	mem_clk
	);

parameter	udly	=	1'b1;

//	-------------------------------------------------------------------------------
//Configure Ports:
input[1:0]	arbta_prior_mode;
//Devices Ports:
//GPU:
input		gpu_req;
input		gpu_ack;
//Video:
input		video_req;
input		video_ack;
//DSP:
input		dsp_req;
input		dsp_ack;
//PCI_RD:
input		pci_rreq;
input		pci_rack;
//PCI_WR:
input		pci_wreq;
input		pci_wack;
//For arbtm_prior_monitor:
output		arbta_req;
output		arbta_ack;
//Others:
input		rst_,
			mem_clk;

`ifdef NEW_MEM

`else

//	-------------------------------------------------------------------------------
parameter		IDLE_ST_ENTER	=	5'b0_0000,
				GPU_ST_ENTER	=	5'b0_0001,
				VIDEO_ST_ENTER	=	5'b0_0010,
				DSP_ST_ENTER	=	5'b0_0100,
				PCIRD_ST_ENTER	=	5'b0_1000,
				PCIWR_ST_ENTER	=	5'b1_0000;
				
//	-------------------------------------------------------------------------------
wire	rotation_sel;
wire	fixed_1st_sel;
wire	fixed_2nd_sel;
wire	fixed_3rd_sel;

wire	gpu_trigger;
wire	video_trigger;
wire	dsp_trigger;
wire	pcird_trigger;
wire	pciwr_trigger;

wire	gpu_st_err_ack;
wire	video_st_err_ack;
wire	dsp_st_err_ack;
wire	pcird_st_err_ack;
wire	pciwr_st_err_ack;

wire	arbta_gnts;
wire	arbta_set_busy;
reg		arbta_is_busy;


wire[4:0]	nextst_in_idle;
wire[4:0]	nextst_in_gpu;
wire[4:0]	nextst_in_video;
wire[4:0]	nextst_in_dsp;
wire[4:0]	nextst_in_pcird;
wire[4:0]	nextst_in_pciwr;
wire[4:0]	arbta_next_st;
wire		hold_st;

wire[4:0]	enc_idle_rot;
wire[4:0]	enc_idle_1st;
wire[4:0]	enc_idle_2nd;
wire[4:0]	enc_idle_3rd;

wire[4:0]	enc_gpu_rot;
wire[4:0]	enc_gpu_1st;
wire[4:0]	enc_gpu_2nd;
wire[4:0]	enc_gpu_3rd;

wire[4:0]	enc_video_rot;
wire[4:0]	enc_video_1st;
wire[4:0]	enc_video_2nd;
wire[4:0]	enc_video_3rd;

wire[4:0]	enc_dsp_rot;
wire[4:0]	enc_dsp_1st;
wire[4:0]	enc_dsp_2nd;
wire[4:0]	enc_dsp_3rd;

wire[4:0]	enc_pcird_rot;
wire[4:0]	enc_pcird_1st;
wire[4:0]	enc_pcird_2nd;
wire[4:0]	enc_pcird_3rd;

wire[4:0]	enc_pciwr_rot;
wire[4:0]	enc_pciwr_1st;
wire[4:0]	enc_pciwr_2nd;
wire[4:0]	enc_pciwr_3rd;

reg[4:0]	arbta_curr_st;
wire		arbta_idle_st;
wire		arbta_gpu_st;
wire		arbta_video_st;
wire		arbta_dsp_st;
wire		arbta_pcird_st;
wire		arbta_pciwr_st;

//	-------------------------------------------------------------------------------
always	@(posedge mem_clk or negedge rst_)
	if(!rst_)begin
		arbta_is_busy	<=	#udly	1'b0;
	end
	else if(arbta_set_busy)begin
		arbta_is_busy	<=	#udly	1'b1;
	end
	else if(arbta_gnts)begin
		arbta_is_busy	<=	#udly	1'b0;
	end

always	@(posedge mem_clk or negedge rst_)
	if(!rst_)begin
		arbta_curr_st	<=	#udly	IDLE_ST_ENTER;
	end
	else if(!hold_st)begin
		arbta_curr_st	<=	#udly	arbta_next_st;
	end

//	-------------------------------------------------------------------------------
//Report the Arbited-Priority Error in IDLE_ST:
always	@(posedge mem_clk)
	if(arbta_idle_st && (arbta_gnts))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!\nIn ARBTA_IDLE_ST, Any GNT should not be found",$realtime);
		if(gpu_ack)begin
			$display("Error_T2_rsim:\tIn ARBTA_IDLE_ST, GPU GNT is found");
		end
		if(video_ack)begin
			$display("Error_T2_rsim:\tIn ARBTA_IDLE_ST, VIDEO GNT is found");
		end
		if(dsp_ack)begin
			$display("Error_T2_rsim:\tIn ARBTA_IDLE_ST, DSP GNT is found");
		end
		if(pci_rack)begin
			$display("Error_T2_rsim:\tIn ARBTA_IDLE_ST, PCI_RD GNT is found");
		end
		if(pci_wack)begin
			$display("Error_T2_rsim:\tIn ARBTA_IDLE_ST, PCI_WR GNT is found");
		end
	end


//	-------------------------------------------------------------------------------
//Report the Arbited-Priority Error in GPU_ST:
always	@(posedge mem_clk)
	if(arbta_gpu_st && (gpu_st_err_ack))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!\nIn ARBTA_GPU_ST, Any Other GNT should not be found",$realtime);
		if(video_ack)begin
			$display("Error_T2_rsim:\tIn ARBTA_GPU_ST, VIDEO GNT is found");
		end
		if(dsp_ack)begin
			$display("Error_T2_rsim:\tIn ARBTA_GPU_ST, DSP GNT is found");
		end
		if(pci_rack)begin
			$display("Error_T2_rsim:\tIn ARBTA_GPU_ST, PCI_RD GNT is found");
		end
		if(pci_wack)begin
			$display("Error_T2_rsim:\tIn ARBTA_GPU_ST, PCI_WR GNT is found");
		end
	end
	
//	-------------------------------------------------------------------------------
//Report the Arbited-Priority Error in VIDEO_ST:
always	@(posedge mem_clk)
	if(arbta_video_st && (video_st_err_ack))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!\nIn ARBTA_VIDEO_ST, Any Other GNT should not be found",$realtime);
		if(gpu_ack)begin
			$display("Error_T2_rsim:\tIn ARBTA_VIDEO_ST, GPU GNT is found");
		end
		if(dsp_ack)begin
			$display("Error_T2_rsim:\tIn ARBTA_VIDEO_ST, DSP GNT is found");
		end
		if(pci_rack)begin
			$display("Error_T2_rsim:\tIn ARBTA_VIDEO_ST, PCI_RD GNT is found");
		end
		if(pci_wack)begin
			$display("Error_T2_rsim:\tIn ARBTA_VIDEO_ST, PCI_WR GNT is found");
		end
	end

//	-------------------------------------------------------------------------------
//Report the Arbited-Priority Error in DSP_ST:
always	@(posedge mem_clk)
	if(arbta_dsp_st && (dsp_st_err_ack))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!\nIn ARBTA_DSP_ST, Any Other GNT should not be found",$realtime);
		if(gpu_ack)begin
			$display("Error_T2_rsim:\tIn ARBTA_DSP_ST, GPU GNT is found");
		end
		if(video_ack)begin
			$display("Error_T2_rsim:\tIn ARBTA_DSP_ST, VIDEO GNT is found");
		end
		if(pci_rack)begin
			$display("Error_T2_rsim:\tIn ARBTA_DSP_ST, PCI_RD GNT is found");
		end
		if(pci_wack)begin
			$display("Error_T2_rsim:\tIn ARBTA_DSP_ST, PCI_WR GNT is found");
		end
	end

//	-------------------------------------------------------------------------------
//Report the Arbited-Priority Error in PCIRD_ST:
always	@(posedge mem_clk)
	if(arbta_pcird_st && (pcird_st_err_ack))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!\nIn ARBTA_PCIRD_ST, Any Other GNT should not be found",$realtime);
		if(gpu_ack)begin
			$display("Error_T2_rsim:\tIn ARBTA_PCIRD_ST, GPU GNT is found");
		end
		if(video_ack)begin
			$display("Error_T2_rsim:\tIn ARBTA_PCIRD_ST, VIDEO GNT is found");
		end
		if(dsp_ack)begin
			$display("Error_T2_rsim:\tIn ARBTA_PCIRD_ST, DSP_RD GNT is found");
		end
		if(pci_wack)begin
			$display("Error_T2_rsim:\tIn ARBTA_PCIRD_ST, PCI_WR GNT is found");
		end
	end

//	-------------------------------------------------------------------------------
//Report the Arbited-Priority Error in PCIWR_ST:
always	@(posedge mem_clk)
	if(arbta_pciwr_st && (pciwr_st_err_ack))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!\nIn ARBTA_PCIWR_ST, Any Other GNT should not be found",$realtime);
		if(gpu_ack)begin
			$display("Error_T2_rsim:\tIn ARBTA_PCIWR_ST, GPU GNT is found");
		end
		if(video_ack)begin
			$display("Error_T2_rsim:\tIn ARBTA_PCIWR_ST, VIDEO GNT is found");
		end
		if(dsp_ack)begin
			$display("Error_T2_rsim:\tIn ARBTA_PCIWR_ST, DSP_RD GNT is found");
		end
		if(pci_rack)begin
			$display("Error_T2_rsim:\tIn ARBTA_PCIWR_ST, PCI_RD GNT is found");
		end
	end

//	-------------------------------------------------------------------------------
assign	rotation_sel	=	arbta_prior_mode == 2'b00;
assign	fixed_1st_sel	=	arbta_prior_mode == 2'b01;
assign	fixed_2nd_sel	=	arbta_prior_mode == 2'b10;
assign	fixed_3rd_sel	=	arbta_prior_mode == 2'b11;

assign	gpu_trigger		=	gpu_req && !gpu_ack;
assign	video_trigger	=	video_req && !video_ack;
assign	dsp_trigger		=	dsp_req && !dsp_ack;
assign	pcird_trigger	=	pci_rreq && !pci_rack;
assign	pciwr_trigger	=	pci_wreq && !pci_wack;

assign	arbta_set_busy	=	gpu_trigger || video_trigger || dsp_trigger ||
							pcird_trigger || pciwr_trigger;

assign	gpu_st_err_ack		=	video_ack || dsp_ack || pci_rack || pci_wack;
assign	video_st_err_ack	=	gpu_ack || dsp_ack || pci_rack || pci_wack;
assign	dsp_st_err_ack		=	gpu_ack || video_ack || pci_rack || pci_wack;
assign	pcird_st_err_ack	=	gpu_ack || video_ack || dsp_ack || pci_wack;
assign	pciwr_st_err_ack	=	gpu_ack || video_ack || dsp_ack || pci_rack;

assign	arbta_gnts		=	gpu_ack || video_ack || dsp_ack || pci_rack || pci_wack;

//	-------------------------------------------------------------------------------
assign	hold_st			=	arbta_is_busy && !arbta_gnts;

//	-------------------------------------------------------------------------------
assign	arbta_idle_st	=	!(arbta_curr_st[4] || arbta_curr_st[3] || arbta_curr_st[2] ||
								arbta_curr_st[1] || arbta_curr_st[0]);

assign	arbta_gpu_st	=	arbta_curr_st[0];
assign	arbta_video_st	=	arbta_curr_st[1];
assign	arbta_dsp_st	=	arbta_curr_st[2];
assign	arbta_pcird_st	=	arbta_curr_st[3];
assign	arbta_pciwr_st	=	arbta_curr_st[4];

assign	arbta_next_st	=	{5{arbta_idle_st}}	&	nextst_in_idle	|
							{5{arbta_gpu_st}}	&	nextst_in_gpu	|
							{5{arbta_video_st}}	&	nextst_in_video	|
							{5{arbta_dsp_st}}	&	nextst_in_dsp	|
							{5{arbta_pcird_st}}	&	nextst_in_pcird	|
							{5{arbta_pciwr_st}}	&	nextst_in_pciwr;

//	-------------------------------------------------------------------------------
//Arbiter stays in IDLE_ST:
assign	nextst_in_idle	=	{5{rotation_sel}}	&	enc_idle_rot	|
							{5{fixed_1st_sel}}	&	enc_idle_1st	|
							{5{fixed_2nd_sel}}	&	enc_idle_2nd	|
							{5{fixed_3rd_sel}}	&	enc_idle_3rd;


assign	enc_idle_rot	=	gpu_req 	?	GPU_ST_ENTER	:
							video_req	?	VIDEO_ST_ENTER	:
							dsp_req		?	DSP_ST_ENTER	:
							pci_rreq	?	PCIRD_ST_ENTER	:
							pci_wreq	?	PCIWR_ST_ENTER	:
											IDLE_ST_ENTER	;

assign	enc_idle_1st	=	gpu_req 	?	GPU_ST_ENTER	:
							video_req	?	VIDEO_ST_ENTER	:
							dsp_req		?	DSP_ST_ENTER	:
							pci_rreq	?	PCIRD_ST_ENTER	:
							pci_wreq	?	PCIWR_ST_ENTER	:
											IDLE_ST_ENTER	;

assign	enc_idle_2nd	=	dsp_req		?	DSP_ST_ENTER	:
							gpu_req 	?	GPU_ST_ENTER	:
							video_req	?	VIDEO_ST_ENTER	:
							pci_rreq	?	PCIRD_ST_ENTER	:
							pci_wreq	?	PCIWR_ST_ENTER	:
											IDLE_ST_ENTER	;

assign	enc_idle_3rd	=	pci_rreq	?	PCIRD_ST_ENTER	:
							pci_wreq	?	PCIWR_ST_ENTER	:
							gpu_req 	?	GPU_ST_ENTER	:
							video_req	?	VIDEO_ST_ENTER	:
							dsp_req		?	DSP_ST_ENTER	:
											IDLE_ST_ENTER	;

//	----------------------------------------------------------------------------------
//Arbiter stays in GPU_ST:
assign	nextst_in_gpu	=	{5{rotation_sel}}	&	enc_gpu_rot	|
							{5{fixed_1st_sel}}	&	enc_gpu_1st	|
							{5{fixed_2nd_sel}}	&	enc_gpu_2nd	|
							{5{fixed_3rd_sel}}	&	enc_gpu_3rd;


assign	enc_gpu_rot		=	video_req	?	VIDEO_ST_ENTER	:
							dsp_req		?	DSP_ST_ENTER	:
							pci_rreq	?	PCIRD_ST_ENTER	:
							pci_wreq	?	PCIWR_ST_ENTER	:
											IDLE_ST_ENTER	;

assign	enc_gpu_1st		=	video_req	?	VIDEO_ST_ENTER	:
							dsp_req		?	DSP_ST_ENTER	:
							pci_rreq	?	PCIRD_ST_ENTER	:
							pci_wreq	?	PCIWR_ST_ENTER	:
											IDLE_ST_ENTER	;

assign	enc_gpu_2nd		=	dsp_req		?	DSP_ST_ENTER	:
							video_req	?	VIDEO_ST_ENTER	:
							pci_rreq	?	PCIRD_ST_ENTER	:
							pci_wreq	?	PCIWR_ST_ENTER	:
											IDLE_ST_ENTER	;

assign	enc_gpu_3rd		=	pci_rreq	?	PCIRD_ST_ENTER	:
							pci_wreq	?	PCIWR_ST_ENTER	:
							video_req	?	VIDEO_ST_ENTER	:
							dsp_req		?	DSP_ST_ENTER	:
											IDLE_ST_ENTER	;

//	----------------------------------------------------------------------------------
//Arbiter stays in VIDEO_ST:
assign	nextst_in_video	=	{5{rotation_sel}}	&	enc_video_rot	|
							{5{fixed_1st_sel}}	&	enc_video_1st	|
							{5{fixed_2nd_sel}}	&	enc_video_2nd	|
							{5{fixed_3rd_sel}}	&	enc_video_3rd;

assign	enc_video_rot	=	dsp_req		?	DSP_ST_ENTER	:
							pci_rreq	?	PCIRD_ST_ENTER	:
							pci_wreq	?	PCIWR_ST_ENTER	:
							gpu_req		?	GPU_ST_ENTER	:
											IDLE_ST_ENTER	;

assign	enc_video_1st	=	gpu_req		?	GPU_ST_ENTER	:
							dsp_req		?	DSP_ST_ENTER	:
							pci_rreq	?	PCIRD_ST_ENTER	:
							pci_wreq	?	PCIWR_ST_ENTER	:
											IDLE_ST_ENTER	;

assign	enc_video_2nd	=	dsp_req		?	DSP_ST_ENTER	:
							gpu_req		?	GPU_ST_ENTER	:
							pci_rreq	?	PCIRD_ST_ENTER	:
							pci_wreq	?	PCIWR_ST_ENTER	:
											IDLE_ST_ENTER	;

assign	enc_video_3rd	=	pci_rreq	?	PCIRD_ST_ENTER	:
							pci_wreq	?	PCIWR_ST_ENTER	:
							gpu_req		?	GPU_ST_ENTER	:
							dsp_req		?	DSP_ST_ENTER	:
											IDLE_ST_ENTER	;

//	----------------------------------------------------------------------------------
//Arbiter stays in DSP_ST:
assign	nextst_in_dsp	=	{5{rotation_sel}}	&	enc_dsp_rot	|
							{5{fixed_1st_sel}}	&	enc_dsp_1st	|
							{5{fixed_2nd_sel}}	&	enc_dsp_2nd	|
							{5{fixed_3rd_sel}}	&	enc_dsp_3rd;

assign	enc_dsp_rot		=	pci_rreq	?	PCIRD_ST_ENTER	:
							pci_wreq	?	PCIWR_ST_ENTER	:
							gpu_req		?	GPU_ST_ENTER	:
							video_req	?	VIDEO_ST_ENTER	:
											IDLE_ST_ENTER	;

assign	enc_dsp_1st		=	gpu_req		?	GPU_ST_ENTER	:
							video_req	?	VIDEO_ST_ENTER	:
							pci_rreq	?	PCIRD_ST_ENTER	:
							pci_wreq	?	PCIWR_ST_ENTER	:
											IDLE_ST_ENTER	;

assign	enc_dsp_2nd		=	gpu_req		?	GPU_ST_ENTER	:
							video_req	?	VIDEO_ST_ENTER	:
							pci_rreq	?	PCIRD_ST_ENTER	:
							pci_wreq	?	PCIWR_ST_ENTER	:
											IDLE_ST_ENTER	;

assign	enc_dsp_3rd		=	pci_rreq	?	PCIRD_ST_ENTER	:
							pci_wreq	?	PCIWR_ST_ENTER	:
							gpu_req		?	GPU_ST_ENTER	:
							video_req	?	VIDEO_ST_ENTER	:
											IDLE_ST_ENTER	;

//	----------------------------------------------------------------------------------
//Arbiter stays in PCIRD_ST:
assign	nextst_in_pcird	=	{5{rotation_sel}}	&	enc_pcird_rot	|
							{5{fixed_1st_sel}}	&	enc_pcird_1st	|
							{5{fixed_2nd_sel}}	&	enc_pcird_2nd	|
							{5{fixed_3rd_sel}}	&	enc_pcird_3rd;

assign	enc_pcird_rot	=	pci_wreq	?	PCIWR_ST_ENTER	:
							gpu_req		?	GPU_ST_ENTER	:
							video_req	?	VIDEO_ST_ENTER	:
							dsp_req		?	DSP_ST_ENTER	:
											IDLE_ST_ENTER	;

assign	enc_pcird_1st	=	pci_wreq	?	PCIWR_ST_ENTER	:
							gpu_req		?	GPU_ST_ENTER	:
							video_req	?	VIDEO_ST_ENTER	:
							dsp_req		?	DSP_ST_ENTER	:
											IDLE_ST_ENTER	;

assign	enc_pcird_2nd	=	pci_wreq	?	PCIWR_ST_ENTER	:
							dsp_req		?	DSP_ST_ENTER	:
							gpu_req		?	GPU_ST_ENTER	:
							video_req	?	VIDEO_ST_ENTER	:
											IDLE_ST_ENTER	;

assign	enc_pcird_3rd	=	pci_wreq	?	PCIWR_ST_ENTER	:
							gpu_req		?	GPU_ST_ENTER	:
							video_req	?	VIDEO_ST_ENTER	:
							dsp_req		?	DSP_ST_ENTER	:
											IDLE_ST_ENTER	;

//	----------------------------------------------------------------------------------
//Arbiter stays in PCIWR_ST:
assign	nextst_in_pciwr	=	{5{rotation_sel}}	&	enc_pciwr_rot	|
							{5{fixed_1st_sel}}	&	enc_pciwr_1st	|
							{5{fixed_2nd_sel}}	&	enc_pciwr_2nd	|
							{5{fixed_3rd_sel}}	&	enc_pciwr_3rd;

assign	enc_pciwr_rot	=	gpu_req		?	GPU_ST_ENTER	:
							video_req	?	VIDEO_ST_ENTER	:
							dsp_req		?	DSP_ST_ENTER	:
							pci_rreq	?	PCIRD_ST_ENTER	:
											IDLE_ST_ENTER	;

assign	enc_pciwr_1st	=	gpu_req		?	GPU_ST_ENTER	:
							video_req	?	VIDEO_ST_ENTER	:
							dsp_req		?	DSP_ST_ENTER	:
							pci_rreq	?	PCIRD_ST_ENTER	:
											IDLE_ST_ENTER	;

assign	enc_pciwr_2nd	=	dsp_req		?	DSP_ST_ENTER	:
							gpu_req		?	GPU_ST_ENTER	:
							video_req	?	VIDEO_ST_ENTER	:
							pci_rreq	?	PCIRD_ST_ENTER	:
											IDLE_ST_ENTER	;

assign	enc_pciwr_3rd	=	gpu_req		?	GPU_ST_ENTER	:
							video_req	?	VIDEO_ST_ENTER	:
							dsp_req		?	DSP_ST_ENTER	:
							pci_rreq	?	PCIRD_ST_ENTER	:
											IDLE_ST_ENTER	;

//	--------------------------------------------------------------------------------------
wire	arbta_req		=	arbta_is_busy;
wire	arbta_ack		=	arbta_gnts;

//	--------------------------------------------------------------------------------------

`endif

endmodule


