/******************************************************************************
	  (c) copyrights 2000-. All rights reserved
 
	   T-Square Design, Inc.
 
	  Company confidential and Properietary	information.
	  This information may not be disclosed	to unauthorized individual.	  
*******************************************************************************/

/*******************************************************************************
 *
 *       PROJECT:M6304
 *
 *       FILE NAME: arbtm_prior_monitor.v
 *
 *       DESCRIPTION: SDRAM Memory Bus MAIN Arbitor Priority Monitor
 *						Five source devices:	REF, DISP, CPU,
 *												ARBTA, ARBTB
 *					Priority Mode:
 *								00: Rotation:
 *								REF -> DISP -> CPU -> ARBTA -> ARBTB
 *								01: Fixed_1st:
 *								REF >> DISP >> CPU >> ARBTA >> ARBTB
 *								10: Fixed_2nd:
 *								REF >> DISP >> ARBTA >> ARBTB >> CPU
 *								11: Fixed_3rd:
 *								REF >> DISP >> ARBTB >> CPU >> ARBTA
 *
 *       AUTHOR: Figo OU
 *
 *       HISTORY:   10/31/02         initial version
 *      
 *********************************************************************************/

module	arbtm_prior_monitor(
`ifdef NEW_MEM
	//Configure Ports:
	arbtm_prior_mode,
	//Refresh:
	ref_req,
	ref_ack,
	//Display:
	ser_req,
	ser_ack,
	//CPU:
	cpu_req,
	cpu_ack,
	//Arbiter-A:
	arbta_req,
	arbta_ack,
	//Arbiter-B:
	arbtb_req,
	arbtb_ack,
	//Others:
	rst_,
	mem_clk
	);

parameter	udly	=	1'b1;

//	-------------------------------------------------------------------------------
//Configure Ports:
input[1:0]		arbtm_prior_mode;
//Refresh:
input			ref_req;
input			ref_ack;
//Display:
input			ser_req;
input			ser_ack;
//CPU:
input			cpu_req;
input			cpu_ack;
//Arbiter-A:
input			arbta_req;
input			arbta_ack;
//Arbiter-B:
input			arbtb_req;
input			arbtb_ack;

//Others:
input		rst_,
			mem_clk;

//	-------------------------------------------------------------------------------
parameter		IDLE_ST_ENTER	=	5'b0_0000,
				REF_ST_ENTER	=	5'b0_0001,
				SER_ST_ENTER	=	5'b0_0010,
				CPU_ST_ENTER	=	5'b0_0100,
				ARBTA_ST_ENTER	=	5'b0_1000,
				ARBTB_ST_ENTER	=	5'b1_0000;
/*				
//	-------------------------------------------------------------------------------
wire	rotation_sel;
wire	fixed_1st_sel;
wire	fixed_2nd_sel;
wire	fixed_3rd_sel;

wire	ref_trigger;
wire	ser_trigger;
wire	cpu_trigger;
wire	arbta_trigger;
wire	arbtb_trigger;

wire	ref_st_err_ack;
wire	ser_st_err_ack;
wire	cpu_st_err_ack;
wire	arbta_st_err_ack;
wire	arbtb_st_err_ack;

wire	arbtm_gnts;
wire	arbtm_set_busy;
reg		arbtm_is_busy;


wire[4:0]	nextst_in_idle;
wire[4:0]	nextst_in_ref;
wire[4:0]	nextst_in_ser;
wire[4:0]	nextst_in_cpu;
wire[4:0]	nextst_in_arbta;
wire[4:0]	nextst_in_arbtb;
wire[4:0]	arbtm_next_st;
wire		hold_st;

wire[4:0]	enc_idle_rot;
wire[4:0]	enc_idle_1st;
wire[4:0]	enc_idle_2nd;
wire[4:0]	enc_idle_3rd;

wire[4:0]	enc_ref_rot;
wire[4:0]	enc_ref_1st;
wire[4:0]	enc_ref_2nd;
wire[4:0]	enc_ref_3rd;

wire[4:0]	enc_ser_rot;
wire[4:0]	enc_ser_1st;
wire[4:0]	enc_ser_2nd;
wire[4:0]	enc_ser_3rd;

wire[4:0]	enc_cpu_rot;
wire[4:0]	enc_cpu_1st;
wire[4:0]	enc_cpu_2nd;
wire[4:0]	enc_cpu_3rd;

wire[4:0]	enc_arbta_rot;
wire[4:0]	enc_arbta_1st;
wire[4:0]	enc_arbta_2nd;
wire[4:0]	enc_arbta_3rd;

wire[4:0]	enc_arbtb_rot;
wire[4:0]	enc_arbtb_1st;
wire[4:0]	enc_arbtb_2nd;
wire[4:0]	enc_arbtb_3rd;

reg[4:0]	arbtm_curr_st;
wire		arbtm_idle_st;
wire		arbtm_ref_st;
wire		arbtm_ser_st;
wire		arbtm_cpu_st;
wire		arbtm_arbta_st;
wire		arbtm_arbtb_st;

//	-------------------------------------------------------------------------------
always	@(posedge mem_clk or negedge rst_)
	if(!rst_)begin
		arbtm_is_busy	<=	#udly	1'b0;
	end
	else if(arbtm_set_busy)begin
		arbtm_is_busy	<=	#udly	1'b1;
	end
	else if(arbtm_gnts)begin
		arbtm_is_busy	<=	#udly	1'b0;
	end

always	@(posedge mem_clk or negedge rst_)
	if(!rst_)begin
		arbtm_curr_st	<=	#udly	IDLE_ST_ENTER;
	end
	else if(!hold_st)begin
		arbtm_curr_st	<=	#udly	arbtm_next_st;
	end

//	-------------------------------------------------------------------------------
//Report the Arbited-Priority Error in IDLE_ST:
always	@(posedge mem_clk)
	if(arbtm_idle_st && (arbtm_gnts))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!\nIn ARBTA_IDLE_ST, Any GNT should not be found",$realtime);
		if(ref_ack)begin
			$display("Error_T2_rsim:\tIn ARBTA_IDLE_ST, REF GNT is found");
		end
		if(ser_ack)begin
			$display("Error_T2_rsim:\tIn ARBTA_IDLE_ST, SER GNT is found");
		end
		if(cpu_ack)begin
			$display("Error_T2_rsim:\tIn ARBTA_IDLE_ST, CPU GNT is found");
		end
		if(arbta_ack)begin
			$display("Error_T2_rsim:\tIn ARBTA_IDLE_ST, ARBTA GNT is found");
		end
		if(arbtb_ack)begin
			$display("Error_T2_rsim:\tIn ARBTA_IDLE_ST, ARBTB GNT is found");
		end
	end


//	-------------------------------------------------------------------------------
//Report the Arbited-Priority Error in REF_ST:
always	@(posedge mem_clk)
	if(arbtm_ref_st && (ref_st_err_ack))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!\nIn ARBTM_REF_ST, Any Other GNT should not be found",$realtime);
		if(ser_ack)begin
			$display("Error_T2_rsim:\tIn ARBTM_REF_ST, SER GNT is found");
		end
		if(cpu_ack)begin
			$display("Error_T2_rsim:\tIn ARBTM_REF_ST, CPU GNT is found");
		end
		if(arbta_ack)begin
			$display("Error_T2_rsim:\tIn ARBTM_REF_ST, ARBTA GNT is found");
		end
		if(arbtb_ack)begin
			$display("Error_T2_rsim:\tIn ARBTM_REF_ST, ARBTB GNT is found");
		end
	end
	
//	-------------------------------------------------------------------------------
//Report the Arbited-Priority Error in SER_ST:
always	@(posedge mem_clk)
	if(arbtm_ser_st && (ser_st_err_ack))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!\nIn ARBTM_SER_ST, Any Other GNT should not be found",$realtime);
		if(ref_ack)begin
			$display("Error_T2_rsim:\tIn ARBTM_SER_ST, REF GNT is found");
		end
		if(cpu_ack)begin
			$display("Error_T2_rsim:\tIn ARBTM_SER_ST, CPU GNT is found");
		end
		if(arbta_ack)begin
			$display("Error_T2_rsim:\tIn ARBTM_SER_ST, ARBTA GNT is found");
		end
		if(arbtb_ack)begin
			$display("Error_T2_rsim:\tIn ARBTM_SER_ST, ARBTB GNT is found");
		end
	end

//	-------------------------------------------------------------------------------
//Report the Arbited-Priority Error in CPU_ST:
always	@(posedge mem_clk)
	if(arbtm_cpu_st && (cpu_st_err_ack))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!\nIn ARBTM_CPU_ST, Any Other GNT should not be found",$realtime);
		if(ref_ack)begin
			$display("Error_T2_rsim:\tIn ARBTM_CPU_ST, REF GNT is found");
		end
		if(ser_ack)begin
			$display("Error_T2_rsim:\tIn ARBTM_CPU_ST, SER GNT is found");
		end
		if(arbta_ack)begin
			$display("Error_T2_rsim:\tIn ARBTM_CPU_ST, ARBTA GNT is found");
		end
		if(arbtb_ack)begin
			$display("Error_T2_rsim:\tIn ARBTM_CPU_ST, ARBTB GNT is found");
		end
	end

//	-------------------------------------------------------------------------------
//Report the Arbited-Priority Error in ARBTA_ST:
always	@(posedge mem_clk)
	if(arbtm_arbta_st && (arbta_st_err_ack))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!\nIn ARBTA_ARBTA_ST, Any Other GNT should not be found",$realtime);
		if(ref_ack)begin
			$display("Error_T2_rsim:\tIn ARBTA_ARBTA_ST, REF GNT is found");
		end
		if(ser_ack)begin
			$display("Error_T2_rsim:\tIn ARBTA_ARBTA_ST, SER GNT is found");
		end
		if(cpu_ack)begin
			$display("Error_T2_rsim:\tIn ARBTA_ARBTA_ST, CPU_RD GNT is found");
		end
		if(arbtb_ack)begin
			$display("Error_T2_rsim:\tIn ARBTA_ARBTA_ST, ARBTB GNT is found");
		end
	end

//	-------------------------------------------------------------------------------
//Report the Arbited-Priority Error in ARBTB_ST:
always	@(posedge mem_clk)
	if(arbtm_arbtb_st && (arbtb_st_err_ack))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!\nIn ARBTA_ARBTB_ST, Any Other GNT should not be found",$realtime);
		if(ref_ack)begin
			$display("Error_T2_rsim:\tIn ARBTA_ARBTB_ST, REF GNT is found");
		end
		if(ser_ack)begin
			$display("Error_T2_rsim:\tIn ARBTA_ARBTB_ST, SER GNT is found");
		end
		if(cpu_ack)begin
			$display("Error_T2_rsim:\tIn ARBTA_ARBTB_ST, CPU_RD GNT is found");
		end
		if(arbta_ack)begin
			$display("Error_T2_rsim:\tIn ARBTA_ARBTB_ST, ARBTA GNT is found");
		end
	end

//	-------------------------------------------------------------------------------
assign	rotation_sel	=	arbtm_prior_mode == 2'b00;
assign	fixed_1st_sel	=	arbtm_prior_mode == 2'b01;
assign	fixed_2nd_sel	=	arbtm_prior_mode == 2'b10;
assign	fixed_3rd_sel	=	arbtm_prior_mode == 2'b11;

assign	ref_trigger		=	ref_req && !ref_ack;
assign	ser_trigger		=	ser_req && !ser_ack;
assign	cpu_trigger		=	cpu_req && !cpu_ack;
assign	arbta_trigger	=	arbta_req && !arbta_ack;
assign	arbtb_trigger	=	arbtb_req && !arbtb_ack;

assign	arbtm_set_busy	=	ref_trigger || ser_trigger || cpu_trigger ||
							arbta_trigger || arbtb_trigger;

assign	ref_st_err_ack		=	ser_ack || cpu_ack || arbta_ack || arbtb_ack;
assign	ser_st_err_ack		=	ref_ack || cpu_ack || arbta_ack || arbtb_ack;
assign	cpu_st_err_ack		=	ref_ack || ser_ack || arbta_ack || arbtb_ack;
assign	arbta_st_err_ack	=	ref_ack || ser_ack || cpu_ack || arbtb_ack;
assign	arbtb_st_err_ack	=	ref_ack || ser_ack || cpu_ack || arbta_ack;

assign	arbtm_gnts		=	ref_ack || ser_ack || cpu_ack || arbta_ack || arbtb_ack;

//	-------------------------------------------------------------------------------
assign	hold_st			=	arbtm_is_busy && !arbtm_gnts;

//	-------------------------------------------------------------------------------
assign	arbtm_idle_st	=	!(arbtm_curr_st[4] || arbtm_curr_st[3] || arbtm_curr_st[2] ||
								arbtm_curr_st[1] || arbtm_curr_st[0]);

assign	arbtm_ref_st	=	arbtm_curr_st[0];
assign	arbtm_ser_st	=	arbtm_curr_st[1];
assign	arbtm_cpu_st	=	arbtm_curr_st[2];
assign	arbtm_arbta_st	=	arbtm_curr_st[3];
assign	arbtm_arbtb_st	=	arbtm_curr_st[4];

assign	arbtm_next_st	=	{5{arbtm_idle_st}}	&	nextst_in_idle	|
							{5{arbtm_ref_st}}	&	nextst_in_ref	|
							{5{arbtm_ser_st}}	&	nextst_in_ser	|
							{5{arbtm_cpu_st}}	&	nextst_in_cpu	|
							{5{arbtm_arbta_st}}	&	nextst_in_arbta	|
							{5{arbtm_arbtb_st}}	&	nextst_in_arbtb;

//	-------------------------------------------------------------------------------
//Arbiter stays in IDLE_ST:
assign	nextst_in_idle	=	{5{rotation_sel}}	&	enc_idle_rot	|
							{5{fixed_1st_sel}}	&	enc_idle_1st	|
							{5{fixed_2nd_sel}}	&	enc_idle_2nd	|
							{5{fixed_3rd_sel}}	&	enc_idle_3rd;


assign	enc_idle_rot	=	ref_req 	?	REF_ST_ENTER	:
							ser_req		?	SER_ST_ENTER	:
							cpu_req		?	CPU_ST_ENTER	:
							arbta_req	?	ARBTA_ST_ENTER	:
							arbtb_req	?	ARBTB_ST_ENTER	:
											IDLE_ST_ENTER	;

assign	enc_idle_1st	=	ref_req 	?	REF_ST_ENTER	:
							ser_req		?	SER_ST_ENTER	:
							cpu_req		?	CPU_ST_ENTER	:
							arbta_req	?	ARBTA_ST_ENTER	:
							arbtb_req	?	ARBTB_ST_ENTER	:
											IDLE_ST_ENTER	;


assign	enc_idle_2nd	=	ref_req 	?	REF_ST_ENTER	:
							ser_req		?	SER_ST_ENTER	:
							arbta_req	?	ARBTA_ST_ENTER	:
							arbtb_req	?	ARBTB_ST_ENTER	:
							cpu_req		?	CPU_ST_ENTER	:
											IDLE_ST_ENTER	;

assign	enc_idle_3rd	=	ref_req 	?	REF_ST_ENTER	:
							ser_req		?	SER_ST_ENTER	:
							arbtb_req	?	ARBTB_ST_ENTER	:
							cpu_req		?	CPU_ST_ENTER	:
							arbta_req	?	ARBTA_ST_ENTER	:
											IDLE_ST_ENTER	;

//	----------------------------------------------------------------------------------
//Arbiter stays in REF_ST:
assign	enc_ref_rot		=	ser_req		?	SER_ST_ENTER	:
							cpu_req		?	CPU_ST_ENTER	:
							arbta_req	?	ARBTA_ST_ENTER	:
							arbtb_req	?	ARBTB_ST_ENTER	:
											IDLE_ST_ENTER	;

assign	enc_ref_1st		=	ser_req		?	SER_ST_ENTER	:
							cpu_req		?	CPU_ST_ENTER	:
							arbta_req	?	ARBTA_ST_ENTER	:
							arbtb_req	?	ARBTB_ST_ENTER	:
											IDLE_ST_ENTER	;


assign	enc_ref_2nd		=	ser_req		?	SER_ST_ENTER	:
							arbta_req	?	ARBTA_ST_ENTER	:
							arbtb_req	?	ARBTB_ST_ENTER	:
							cpu_req		?	CPU_ST_ENTER	:
											IDLE_ST_ENTER	;

assign	enc_ref_3rd		=	ser_req		?	SER_ST_ENTER	:
							arbtb_req	?	ARBTB_ST_ENTER	:
							cpu_req		?	CPU_ST_ENTER	:
							arbta_req	?	ARBTA_ST_ENTER	:
											IDLE_ST_ENTER	;

//	----------------------------------------------------------------------------------
//Arbiter stays in SER_ST:
assign	nextst_in_ser	=	{5{rotation_sel}}	&	enc_ser_rot	|
							{5{fixed_1st_sel}}	&	enc_ser_1st	|
							{5{fixed_2nd_sel}}	&	enc_ser_2nd	|
							{5{fixed_3rd_sel}}	&	enc_ser_3rd;

assign	enc_ser_rot	=		cpu_req		?	CPU_ST_ENTER	:
							arbta_req	?	ARBTA_ST_ENTER	:
							arbtb_req	?	ARBTB_ST_ENTER	:
							ref_req		?	REF_ST_ENTER	:
											IDLE_ST_ENTER	;

assign	enc_ser_1st	=		ref_req		?	REF_ST_ENTER	:
							cpu_req		?	CPU_ST_ENTER	:
							arbta_req	?	ARBTA_ST_ENTER	:
							arbtb_req	?	ARBTB_ST_ENTER	:
											IDLE_ST_ENTER	;

assign	enc_ser_2nd	=		ref_req		?	REF_ST_ENTER	:
							arbta_req	?	ARBTA_ST_ENTER	:
							arbtb_req	?	ARBTB_ST_ENTER	:
							cpu_req		?	CPU_ST_ENTER	:
											IDLE_ST_ENTER	;

assign	enc_ser_3rd	=		ref_req		?	REF_ST_ENTER	:
							arbtb_req	?	ARBTB_ST_ENTER	:
							cpu_req		?	CPU_ST_ENTER	:
							arbta_req	?	ARBTA_ST_ENTER	:
											IDLE_ST_ENTER	;

//	----------------------------------------------------------------------------------
//Arbiter stays in CPU_ST:
assign	nextst_in_cpu	=	{5{rotation_sel}}	&	enc_cpu_rot	|
							{5{fixed_1st_sel}}	&	enc_cpu_1st	|
							{5{fixed_2nd_sel}}	&	enc_cpu_2nd	|
							{5{fixed_3rd_sel}}	&	enc_cpu_3rd;

assign	enc_cpu_rot		=	arbta_req	?	ARBTA_ST_ENTER	:
							arbtb_req	?	ARBTB_ST_ENTER	:
							ref_req		?	REF_ST_ENTER	:
							ser_req		?	SER_ST_ENTER	:
											IDLE_ST_ENTER	;

assign	enc_cpu_1st		=	ref_req		?	REF_ST_ENTER	:
							ser_req		?	SER_ST_ENTER	:
							arbta_req	?	ARBTA_ST_ENTER	:
							arbtb_req	?	ARBTB_ST_ENTER	:
											IDLE_ST_ENTER	;

assign	enc_cpu_2nd		=	ref_req		?	REF_ST_ENTER	:
							ser_req		?	SER_ST_ENTER	:
							arbta_req	?	ARBTA_ST_ENTER	:
							arbtb_req	?	ARBTB_ST_ENTER	:
											IDLE_ST_ENTER	;

assign	enc_cpu_3rd		=	ref_req		?	REF_ST_ENTER	:
							ser_req		?	SER_ST_ENTER	:
							arbtb_req	?	ARBTB_ST_ENTER	:
							arbta_req	?	ARBTA_ST_ENTER	:
											IDLE_ST_ENTER	;

//	----------------------------------------------------------------------------------
//Arbiter stays in ARBTA_ST:
assign	nextst_in_arbta	=	{5{rotation_sel}}	&	enc_arbta_rot	|
							{5{fixed_1st_sel}}	&	enc_arbta_1st	|
							{5{fixed_2nd_sel}}	&	enc_arbta_2nd	|
							{5{fixed_3rd_sel}}	&	enc_arbta_3rd;

assign	enc_arbta_rot	=	arbtb_req	?	ARBTB_ST_ENTER	:
							ref_req		?	REF_ST_ENTER	:
							ser_req		?	SER_ST_ENTER	:
							cpu_req		?	CPU_ST_ENTER	:
											IDLE_ST_ENTER	;

assign	enc_arbta_1st	=	ref_req		?	REF_ST_ENTER	:
							ser_req		?	SER_ST_ENTER	:
							cpu_req		?	CPU_ST_ENTER	:
							arbtb_req	?	ARBTB_ST_ENTER	:
											IDLE_ST_ENTER	;

assign	enc_arbta_2nd	=	ref_req		?	REF_ST_ENTER	:
							ser_req		?	SER_ST_ENTER	:
							arbtb_req	?	ARBTB_ST_ENTER	:
							cpu_req		?	CPU_ST_ENTER	:
											IDLE_ST_ENTER	;

assign	enc_arbta_3rd	=	ref_req		?	REF_ST_ENTER	:
							ser_req		?	SER_ST_ENTER	:
							arbtb_req	?	ARBTB_ST_ENTER	:
							cpu_req		?	CPU_ST_ENTER	:
											IDLE_ST_ENTER	;

//	----------------------------------------------------------------------------------
//Arbiter stays in ARBTB_ST:
assign	nextst_in_arbtb	=	{5{rotation_sel}}	&	enc_arbtb_rot	|
							{5{fixed_1st_sel}}	&	enc_arbtb_1st	|
							{5{fixed_2nd_sel}}	&	enc_arbtb_2nd	|
							{5{fixed_3rd_sel}}	&	enc_arbtb_3rd;

assign	enc_arbtb_rot	=	ref_req		?	REF_ST_ENTER	:
							ser_req		?	SER_ST_ENTER	:
							cpu_req		?	CPU_ST_ENTER	:
							arbta_req	?	ARBTA_ST_ENTER	:
											IDLE_ST_ENTER	;

assign	enc_arbtb_1st	=	ref_req		?	REF_ST_ENTER	:
							ser_req		?	SER_ST_ENTER	:
							cpu_req		?	CPU_ST_ENTER	:
							arbta_req	?	ARBTA_ST_ENTER	:
											IDLE_ST_ENTER	;

assign	enc_arbtb_2nd	=	ref_req		?	REF_ST_ENTER	:
							ser_req		?	SER_ST_ENTER	:
							arbta_req	?	ARBTA_ST_ENTER	:
							cpu_req		?	CPU_ST_ENTER	:
											IDLE_ST_ENTER	;


assign	enc_arbtb_3rd	=	ref_req		?	REF_ST_ENTER	:
							ser_req		?	SER_ST_ENTER	:
							cpu_req		?	CPU_ST_ENTER	:
							arbta_req	?	ARBTA_ST_ENTER	:
											IDLE_ST_ENTER	;

//	----------------------------------------------------------------------------
*/

`else
	//Configure Ports:
	arbtm_prior_mode,
	//Refresh:
	ref_req,
	ref_ack,
	//Display:
	disp_req,
	disp_ack,
	//CPU:
	cpu_req,
	cpu_ack,
	//Arbiter-A:
	arbta_req,
	arbta_ack,
	//Arbiter-B:
	arbtb_req,
	arbtb_ack,
	//Others:
	rst_,
	mem_clk
	);

parameter	udly	=	1'b1;

//	-------------------------------------------------------------------------------
//Configure Ports:
input[1:0]		arbtm_prior_mode;
//Refresh:
input			ref_req;
input			ref_ack;
//Display:
input			disp_req;
input			disp_ack;
//CPU:
input			cpu_req;
input			cpu_ack;
//Arbiter-A:
input			arbta_req;
input			arbta_ack;
//Arbiter-B:
input			arbtb_req;
input			arbtb_ack;

//Others:
input		rst_,
			mem_clk;

//	-------------------------------------------------------------------------------
parameter		IDLE_ST_ENTER	=	5'b0_0000,
				REF_ST_ENTER	=	5'b0_0001,
				DISP_ST_ENTER	=	5'b0_0010,
				CPU_ST_ENTER	=	5'b0_0100,
				ARBTA_ST_ENTER	=	5'b0_1000,
				ARBTB_ST_ENTER	=	5'b1_0000;
				
//	-------------------------------------------------------------------------------
wire	rotation_sel;
wire	fixed_1st_sel;
wire	fixed_2nd_sel;
wire	fixed_3rd_sel;

wire	ref_trigger;
wire	disp_trigger;
wire	cpu_trigger;
wire	arbta_trigger;
wire	arbtb_trigger;

wire	ref_st_err_ack;
wire	disp_st_err_ack;
wire	cpu_st_err_ack;
wire	arbta_st_err_ack;
wire	arbtb_st_err_ack;

wire	arbtm_gnts;
wire	arbtm_set_busy;
reg		arbtm_is_busy;


wire[4:0]	nextst_in_idle;
wire[4:0]	nextst_in_ref;
wire[4:0]	nextst_in_disp;
wire[4:0]	nextst_in_cpu;
wire[4:0]	nextst_in_arbta;
wire[4:0]	nextst_in_arbtb;
wire[4:0]	arbtm_next_st;
wire		hold_st;

wire[4:0]	enc_idle_rot;
wire[4:0]	enc_idle_1st;
wire[4:0]	enc_idle_2nd;
wire[4:0]	enc_idle_3rd;

wire[4:0]	enc_ref_rot;
wire[4:0]	enc_ref_1st;
wire[4:0]	enc_ref_2nd;
wire[4:0]	enc_ref_3rd;

wire[4:0]	enc_disp_rot;
wire[4:0]	enc_disp_1st;
wire[4:0]	enc_disp_2nd;
wire[4:0]	enc_disp_3rd;

wire[4:0]	enc_cpu_rot;
wire[4:0]	enc_cpu_1st;
wire[4:0]	enc_cpu_2nd;
wire[4:0]	enc_cpu_3rd;

wire[4:0]	enc_arbta_rot;
wire[4:0]	enc_arbta_1st;
wire[4:0]	enc_arbta_2nd;
wire[4:0]	enc_arbta_3rd;

wire[4:0]	enc_arbtb_rot;
wire[4:0]	enc_arbtb_1st;
wire[4:0]	enc_arbtb_2nd;
wire[4:0]	enc_arbtb_3rd;

reg[4:0]	arbtm_curr_st;
wire		arbtm_idle_st;
wire		arbtm_ref_st;
wire		arbtm_disp_st;
wire		arbtm_cpu_st;
wire		arbtm_arbta_st;
wire		arbtm_arbtb_st;

//	-------------------------------------------------------------------------------
always	@(posedge mem_clk or negedge rst_)
	if(!rst_)begin
		arbtm_is_busy	<=	#udly	1'b0;
	end
	else if(arbtm_set_busy)begin
		arbtm_is_busy	<=	#udly	1'b1;
	end
	else if(arbtm_gnts)begin
		arbtm_is_busy	<=	#udly	1'b0;
	end

always	@(posedge mem_clk or negedge rst_)
	if(!rst_)begin
		arbtm_curr_st	<=	#udly	IDLE_ST_ENTER;
	end
	else if(!hold_st)begin
		arbtm_curr_st	<=	#udly	arbtm_next_st;
	end

//	-------------------------------------------------------------------------------
//Report the Arbited-Priority Error in IDLE_ST:
always	@(posedge mem_clk)
	if(arbtm_idle_st && (arbtm_gnts))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!\nIn ARBTA_IDLE_ST, Any GNT should not be found",$realtime);
		if(ref_ack)begin
			$display("Error_T2_rsim:\tIn ARBTA_IDLE_ST, REF GNT is found");
		end
		if(disp_ack)begin
			$display("Error_T2_rsim:\tIn ARBTA_IDLE_ST, DISP GNT is found");
		end
		if(cpu_ack)begin
			$display("Error_T2_rsim:\tIn ARBTA_IDLE_ST, CPU GNT is found");
		end
		if(arbta_ack)begin
			$display("Error_T2_rsim:\tIn ARBTA_IDLE_ST, ARBTA GNT is found");
		end
		if(arbtb_ack)begin
			$display("Error_T2_rsim:\tIn ARBTA_IDLE_ST, ARBTB GNT is found");
		end
	end


//	-------------------------------------------------------------------------------
//Report the Arbited-Priority Error in REF_ST:
always	@(posedge mem_clk)
	if(arbtm_ref_st && (ref_st_err_ack))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!\nIn ARBTM_REF_ST, Any Other GNT should not be found",$realtime);
		if(disp_ack)begin
			$display("Error_T2_rsim:\tIn ARBTM_REF_ST, DISP GNT is found");
		end
		if(cpu_ack)begin
			$display("Error_T2_rsim:\tIn ARBTM_REF_ST, CPU GNT is found");
		end
		if(arbta_ack)begin
			$display("Error_T2_rsim:\tIn ARBTM_REF_ST, ARBTA GNT is found");
		end
		if(arbtb_ack)begin
			$display("Error_T2_rsim:\tIn ARBTM_REF_ST, ARBTB GNT is found");
		end
	end
	
//	-------------------------------------------------------------------------------
//Report the Arbited-Priority Error in DISP_ST:
always	@(posedge mem_clk)
	if(arbtm_disp_st && (disp_st_err_ack))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!\nIn ARBTM_DISP_ST, Any Other GNT should not be found",$realtime);
		if(ref_ack)begin
			$display("Error_T2_rsim:\tIn ARBTM_DISP_ST, REF GNT is found");
		end
		if(cpu_ack)begin
			$display("Error_T2_rsim:\tIn ARBTM_DISP_ST, CPU GNT is found");
		end
		if(arbta_ack)begin
			$display("Error_T2_rsim:\tIn ARBTM_DISP_ST, ARBTA GNT is found");
		end
		if(arbtb_ack)begin
			$display("Error_T2_rsim:\tIn ARBTM_DISP_ST, ARBTB GNT is found");
		end
	end

//	-------------------------------------------------------------------------------
//Report the Arbited-Priority Error in CPU_ST:
always	@(posedge mem_clk)
	if(arbtm_cpu_st && (cpu_st_err_ack))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!\nIn ARBTM_CPU_ST, Any Other GNT should not be found",$realtime);
		if(ref_ack)begin
			$display("Error_T2_rsim:\tIn ARBTM_CPU_ST, REF GNT is found");
		end
		if(disp_ack)begin
			$display("Error_T2_rsim:\tIn ARBTM_CPU_ST, DISP GNT is found");
		end
		if(arbta_ack)begin
			$display("Error_T2_rsim:\tIn ARBTM_CPU_ST, ARBTA GNT is found");
		end
		if(arbtb_ack)begin
			$display("Error_T2_rsim:\tIn ARBTM_CPU_ST, ARBTB GNT is found");
		end
	end

//	-------------------------------------------------------------------------------
//Report the Arbited-Priority Error in ARBTA_ST:
always	@(posedge mem_clk)
	if(arbtm_arbta_st && (arbta_st_err_ack))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!\nIn ARBTA_ARBTA_ST, Any Other GNT should not be found",$realtime);
		if(ref_ack)begin
			$display("Error_T2_rsim:\tIn ARBTA_ARBTA_ST, REF GNT is found");
		end
		if(disp_ack)begin
			$display("Error_T2_rsim:\tIn ARBTA_ARBTA_ST, DISP GNT is found");
		end
		if(cpu_ack)begin
			$display("Error_T2_rsim:\tIn ARBTA_ARBTA_ST, CPU_RD GNT is found");
		end
		if(arbtb_ack)begin
			$display("Error_T2_rsim:\tIn ARBTA_ARBTA_ST, ARBTB GNT is found");
		end
	end

//	-------------------------------------------------------------------------------
//Report the Arbited-Priority Error in ARBTB_ST:
always	@(posedge mem_clk)
	if(arbtm_arbtb_st && (arbtb_st_err_ack))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!\nIn ARBTA_ARBTB_ST, Any Other GNT should not be found",$realtime);
		if(ref_ack)begin
			$display("Error_T2_rsim:\tIn ARBTA_ARBTB_ST, REF GNT is found");
		end
		if(disp_ack)begin
			$display("Error_T2_rsim:\tIn ARBTA_ARBTB_ST, DISP GNT is found");
		end
		if(cpu_ack)begin
			$display("Error_T2_rsim:\tIn ARBTA_ARBTB_ST, CPU_RD GNT is found");
		end
		if(arbta_ack)begin
			$display("Error_T2_rsim:\tIn ARBTA_ARBTB_ST, ARBTA GNT is found");
		end
	end

//	-------------------------------------------------------------------------------
assign	rotation_sel	=	arbtm_prior_mode == 2'b00;
assign	fixed_1st_sel	=	arbtm_prior_mode == 2'b01;
assign	fixed_2nd_sel	=	arbtm_prior_mode == 2'b10;
assign	fixed_3rd_sel	=	arbtm_prior_mode == 2'b11;

assign	ref_trigger		=	ref_req && !ref_ack;
assign	disp_trigger	=	disp_req && !disp_ack;
assign	cpu_trigger		=	cpu_req && !cpu_ack;
assign	arbta_trigger	=	arbta_req && !arbta_ack;
assign	arbtb_trigger	=	arbtb_req && !arbtb_ack;

assign	arbtm_set_busy	=	ref_trigger || disp_trigger || cpu_trigger ||
							arbta_trigger || arbtb_trigger;

assign	ref_st_err_ack		=	disp_ack || cpu_ack || arbta_ack || arbtb_ack;
assign	disp_st_err_ack		=	ref_ack || cpu_ack || arbta_ack || arbtb_ack;
assign	cpu_st_err_ack		=	ref_ack || disp_ack || arbta_ack || arbtb_ack;
assign	arbta_st_err_ack	=	ref_ack || disp_ack || cpu_ack || arbtb_ack;
assign	arbtb_st_err_ack	=	ref_ack || disp_ack || cpu_ack || arbta_ack;

assign	arbtm_gnts		=	ref_ack || disp_ack || cpu_ack || arbta_ack || arbtb_ack;

//	-------------------------------------------------------------------------------
assign	hold_st			=	arbtm_is_busy && !arbtm_gnts;

//	-------------------------------------------------------------------------------
assign	arbtm_idle_st	=	!(arbtm_curr_st[4] || arbtm_curr_st[3] || arbtm_curr_st[2] ||
								arbtm_curr_st[1] || arbtm_curr_st[0]);

assign	arbtm_ref_st	=	arbtm_curr_st[0];
assign	arbtm_disp_st	=	arbtm_curr_st[1];
assign	arbtm_cpu_st	=	arbtm_curr_st[2];
assign	arbtm_arbta_st	=	arbtm_curr_st[3];
assign	arbtm_arbtb_st	=	arbtm_curr_st[4];

assign	arbtm_next_st	=	{5{arbtm_idle_st}}	&	nextst_in_idle	|
							{5{arbtm_ref_st}}	&	nextst_in_ref	|
							{5{arbtm_disp_st}}	&	nextst_in_disp	|
							{5{arbtm_cpu_st}}	&	nextst_in_cpu	|
							{5{arbtm_arbta_st}}	&	nextst_in_arbta	|
							{5{arbtm_arbtb_st}}	&	nextst_in_arbtb;

//	-------------------------------------------------------------------------------
//Arbiter stays in IDLE_ST:
assign	nextst_in_idle	=	{5{rotation_sel}}	&	enc_idle_rot	|
							{5{fixed_1st_sel}}	&	enc_idle_1st	|
							{5{fixed_2nd_sel}}	&	enc_idle_2nd	|
							{5{fixed_3rd_sel}}	&	enc_idle_3rd;


assign	enc_idle_rot	=	ref_req 	?	REF_ST_ENTER	:
							disp_req	?	DISP_ST_ENTER	:
							cpu_req		?	CPU_ST_ENTER	:
							arbta_req	?	ARBTA_ST_ENTER	:
							arbtb_req	?	ARBTB_ST_ENTER	:
											IDLE_ST_ENTER	;

assign	enc_idle_1st	=	ref_req 	?	REF_ST_ENTER	:
							disp_req	?	DISP_ST_ENTER	:
							cpu_req		?	CPU_ST_ENTER	:
							arbta_req	?	ARBTA_ST_ENTER	:
							arbtb_req	?	ARBTB_ST_ENTER	:
											IDLE_ST_ENTER	;


assign	enc_idle_2nd	=	ref_req 	?	REF_ST_ENTER	:
							disp_req	?	DISP_ST_ENTER	:
							arbta_req	?	ARBTA_ST_ENTER	:
							arbtb_req	?	ARBTB_ST_ENTER	:
							cpu_req		?	CPU_ST_ENTER	:
											IDLE_ST_ENTER	;

assign	enc_idle_3rd	=	ref_req 	?	REF_ST_ENTER	:
							disp_req	?	DISP_ST_ENTER	:
							arbtb_req	?	ARBTB_ST_ENTER	:
							cpu_req		?	CPU_ST_ENTER	:
							arbta_req	?	ARBTA_ST_ENTER	:
											IDLE_ST_ENTER	;

//	----------------------------------------------------------------------------------
//Arbiter stays in REF_ST:
assign	enc_ref_rot		=	disp_req	?	DISP_ST_ENTER	:
							cpu_req		?	CPU_ST_ENTER	:
							arbta_req	?	ARBTA_ST_ENTER	:
							arbtb_req	?	ARBTB_ST_ENTER	:
											IDLE_ST_ENTER	;

assign	enc_ref_1st		=	disp_req	?	DISP_ST_ENTER	:
							cpu_req		?	CPU_ST_ENTER	:
							arbta_req	?	ARBTA_ST_ENTER	:
							arbtb_req	?	ARBTB_ST_ENTER	:
											IDLE_ST_ENTER	;


assign	enc_ref_2nd		=	disp_req	?	DISP_ST_ENTER	:
							arbta_req	?	ARBTA_ST_ENTER	:
							arbtb_req	?	ARBTB_ST_ENTER	:
							cpu_req		?	CPU_ST_ENTER	:
											IDLE_ST_ENTER	;

assign	enc_ref_3rd		=	disp_req	?	DISP_ST_ENTER	:
							arbtb_req	?	ARBTB_ST_ENTER	:
							cpu_req		?	CPU_ST_ENTER	:
							arbta_req	?	ARBTA_ST_ENTER	:
											IDLE_ST_ENTER	;

//	----------------------------------------------------------------------------------
//Arbiter stays in DISP_ST:
assign	nextst_in_disp	=	{5{rotation_sel}}	&	enc_disp_rot	|
							{5{fixed_1st_sel}}	&	enc_disp_1st	|
							{5{fixed_2nd_sel}}	&	enc_disp_2nd	|
							{5{fixed_3rd_sel}}	&	enc_disp_3rd;

assign	enc_disp_rot	=	cpu_req		?	CPU_ST_ENTER	:
							arbta_req	?	ARBTA_ST_ENTER	:
							arbtb_req	?	ARBTB_ST_ENTER	:
							ref_req		?	REF_ST_ENTER	:
											IDLE_ST_ENTER	;

assign	enc_disp_1st	=	ref_req		?	REF_ST_ENTER	:
							cpu_req		?	CPU_ST_ENTER	:
							arbta_req	?	ARBTA_ST_ENTER	:
							arbtb_req	?	ARBTB_ST_ENTER	:
											IDLE_ST_ENTER	;

assign	enc_disp_2nd	=	ref_req		?	REF_ST_ENTER	:
							arbta_req	?	ARBTA_ST_ENTER	:
							arbtb_req	?	ARBTB_ST_ENTER	:
							cpu_req		?	CPU_ST_ENTER	:
											IDLE_ST_ENTER	;

assign	enc_disp_3rd	=	ref_req		?	REF_ST_ENTER	:
							arbtb_req	?	ARBTB_ST_ENTER	:
							cpu_req		?	CPU_ST_ENTER	:
							arbta_req	?	ARBTA_ST_ENTER	:
											IDLE_ST_ENTER	;

//	----------------------------------------------------------------------------------
//Arbiter stays in CPU_ST:
assign	nextst_in_cpu	=	{5{rotation_sel}}	&	enc_cpu_rot	|
							{5{fixed_1st_sel}}	&	enc_cpu_1st	|
							{5{fixed_2nd_sel}}	&	enc_cpu_2nd	|
							{5{fixed_3rd_sel}}	&	enc_cpu_3rd;

assign	enc_cpu_rot		=	arbta_req	?	ARBTA_ST_ENTER	:
							arbtb_req	?	ARBTB_ST_ENTER	:
							ref_req		?	REF_ST_ENTER	:
							disp_req	?	DISP_ST_ENTER	:
											IDLE_ST_ENTER	;

assign	enc_cpu_1st		=	ref_req		?	REF_ST_ENTER	:
							disp_req	?	DISP_ST_ENTER	:
							arbta_req	?	ARBTA_ST_ENTER	:
							arbtb_req	?	ARBTB_ST_ENTER	:
											IDLE_ST_ENTER	;

assign	enc_cpu_2nd		=	ref_req		?	REF_ST_ENTER	:
							disp_req	?	DISP_ST_ENTER	:
							arbta_req	?	ARBTA_ST_ENTER	:
							arbtb_req	?	ARBTB_ST_ENTER	:
											IDLE_ST_ENTER	;

assign	enc_cpu_3rd		=	ref_req		?	REF_ST_ENTER	:
							disp_req	?	DISP_ST_ENTER	:
							arbtb_req	?	ARBTB_ST_ENTER	:
							arbta_req	?	ARBTA_ST_ENTER	:
											IDLE_ST_ENTER	;

//	----------------------------------------------------------------------------------
//Arbiter stays in ARBTA_ST:
assign	nextst_in_arbta	=	{5{rotation_sel}}	&	enc_arbta_rot	|
							{5{fixed_1st_sel}}	&	enc_arbta_1st	|
							{5{fixed_2nd_sel}}	&	enc_arbta_2nd	|
							{5{fixed_3rd_sel}}	&	enc_arbta_3rd;

assign	enc_arbta_rot	=	arbtb_req	?	ARBTB_ST_ENTER	:
							ref_req		?	REF_ST_ENTER	:
							disp_req	?	DISP_ST_ENTER	:
							cpu_req		?	CPU_ST_ENTER	:
											IDLE_ST_ENTER	;

assign	enc_arbta_1st	=	ref_req		?	REF_ST_ENTER	:
							disp_req	?	DISP_ST_ENTER	:
							cpu_req		?	CPU_ST_ENTER	:
							arbtb_req	?	ARBTB_ST_ENTER	:
											IDLE_ST_ENTER	;

assign	enc_arbta_2nd	=	ref_req		?	REF_ST_ENTER	:
							disp_req	?	DISP_ST_ENTER	:
							arbtb_req	?	ARBTB_ST_ENTER	:
							cpu_req		?	CPU_ST_ENTER	:
											IDLE_ST_ENTER	;

assign	enc_arbta_3rd	=	ref_req		?	REF_ST_ENTER	:
							disp_req	?	DISP_ST_ENTER	:
							arbtb_req	?	ARBTB_ST_ENTER	:
							cpu_req		?	CPU_ST_ENTER	:
											IDLE_ST_ENTER	;

//	----------------------------------------------------------------------------------
//Arbiter stays in ARBTB_ST:
assign	nextst_in_arbtb	=	{5{rotation_sel}}	&	enc_arbtb_rot	|
							{5{fixed_1st_sel}}	&	enc_arbtb_1st	|
							{5{fixed_2nd_sel}}	&	enc_arbtb_2nd	|
							{5{fixed_3rd_sel}}	&	enc_arbtb_3rd;

assign	enc_arbtb_rot	=	ref_req		?	REF_ST_ENTER	:
							disp_req	?	DISP_ST_ENTER	:
							cpu_req		?	CPU_ST_ENTER	:
							arbta_req	?	ARBTA_ST_ENTER	:
											IDLE_ST_ENTER	;

assign	enc_arbtb_1st	=	ref_req		?	REF_ST_ENTER	:
							disp_req	?	DISP_ST_ENTER	:
							cpu_req		?	CPU_ST_ENTER	:
							arbta_req	?	ARBTA_ST_ENTER	:
											IDLE_ST_ENTER	;

assign	enc_arbtb_2nd	=	ref_req		?	REF_ST_ENTER	:
							disp_req	?	DISP_ST_ENTER	:
							arbta_req	?	ARBTA_ST_ENTER	:
							cpu_req		?	CPU_ST_ENTER	:
											IDLE_ST_ENTER	;


assign	enc_arbtb_3rd	=	ref_req		?	REF_ST_ENTER	:
							disp_req	?	DISP_ST_ENTER	:
							cpu_req		?	CPU_ST_ENTER	:
							arbta_req	?	ARBTA_ST_ENTER	:
											IDLE_ST_ENTER	;


//	--------------------------------------------------------------------------------------
`endif

endmodule


