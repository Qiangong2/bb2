/*******************************************************************
          (c) copyrights 1997-1998. All rights reserved

           T-Square Design, Inc.

          Company confidential and Properietary information.
          This information may not be disclosed to unauthorized
          individual.
*******************************************************************/
/******************************************************************
	Description	:	PCI target bahaviral model
					define the parameter after line 76 as you need
					io byte write to ioByteAddress will generate
					pci interrupt
					see detail in line 359

********************************************************************/
/*******************************************************************
	Snow : June 23, 1999	-- Change "tt.v" start address to RISC CPU
	snow : June 30, 1999	-- changed to be a 33MHz PCI target
	Kandy: April 10, 2001	-- Add PCI interrupt and Byte/Word IO
	yuky:  Nov. 13, 2001	-- Add PCI IO 0x62f0 ~ 0x62ff as report IO
				   as same as the 0x63f0 ~ 0x63ff.
	yuky:  Nov. 30, 2001	-- Make the IO(0x62f0) as the Audio status report IO Base SH
	yuky:  Dec. 07, 2001	-- Initial the PCI mem with MIPS program,
				   Use PCI mem to run program.
********************************************************************/

module target (
	ad,
	cbe_,
	par,
	frame_,
	irdy_,
	trdy_,
	stop_,
	lock_,
	perr_,
	serr_,
	devsel_,
	idsel,
	enable,     //// hardwired to "1" ____ Snow, 1.8/98
	pci_int_,

	clock,
	rst_
	);

inout	[31:0]	ad;
input	[3:0]	cbe_;

inout	par,
		devsel_ ,
		trdy_ ,
		stop_;

output	serr_,
		perr_;

input	idsel,
		frame_,
		irdy_,
		lock_,
		clock,
		rst_;
/////////////////////////////////

input	enable;

output	[3:0]	pci_int_;

//integer pb_file;

parameter
  ASTART	  = 32'h0800_0000,//JFR030703
  mStartAddress	  = ASTART,
  mEndAddress     = ASTART + 'hffff,	//64k memory
  ioStartAddress  = 32'h0000_6200,
  ioEndAddress    = 32'h0000_62ff,
  ioByteAddress	  = 32'h0000_6300,	// 00: byte IO and Interrupt Clear IO
  ioWordAddress	  = 32'h0000_6304,	// 04: word IO
  cStartAddress   = 0,  cEndAddress = 256;
//  cStartAddress   = 8'h00,  cEndAddress = 8'hff;	// -- Runner

//  ASTART	  = 32'h3000_0000,
//  mStartAddress	  = ASTART,
//  mEndAddress     = ASTART + 32'h0fff_ffff,	//256M memory
//  ioStartAddress  = 32'h1000_0000,
//  ioEndAddress    = 32'h103f_ffff,
//  ioByteAddress	  = 32'h1000_6300,	// 00: byte IO and Interrupt Clear IO
//  ioWordAddress	  = 32'h1000_6304,	// 04: word IO
//  cStartAddress   = 8'h00,  cEndAddress = 8'hff;	// -- Runner

parameter
	omi_delay =2;
//	adDelay =1,
//	ctlDelay=8;

parameter       // 1997-05-08 Raymond 	add WAIT state & dec_mode
    wait_state  = 4'h0,
	dec_mode    = 2'h1,	// 0: fast
						// 1: medium
						// 2: slow
						// 3: sub
	enable_retry = 1'b0,	// add RETRY case 	06/03/97
	enable_disconnect = 1'b0;	// add DISCONNECT case 	06/03/97

reg	[3:0]	wait_state_counter;
reg	set_retry, set_disconnect;
reg	[3:0]	stop_counter;

reg [31:0] t_address;
wire [31:0] io_out_display;
/////////////////////////////////////////////////////

always @ ( negedge rst_ or posedge clock)
if (!rst_)
	stop_counter <= #omi_delay 4'h0;
//else
//	stop_counter <= #omi_delay stop_counter + 3'h1;
else if (stop_counter == 4'h6)
	stop_counter <= #omi_delay 4'h0;
else
	stop_counter <= #omi_delay stop_counter + 4'h1;		// snow 1999.07.05


always @ ( negedge rst_ or posedge clock)
if (!rst_)
	set_retry <= #omi_delay 1'h0;
else if(enable_retry)
	set_retry <= #omi_delay (stop_counter==4'h3 | stop_counter == 4'h5);

always @ ( negedge rst_ or posedge clock)
if (!rst_)
	set_disconnect <= #omi_delay 1'h0;
else if(enable_disconnect)
	set_disconnect <= #omi_delay (stop_counter==4'h6  | stop_counter==4'h1);

parameter bus_sel = 0;	//default for PCI,
			//1 for cb A, 2 for cb B, setup by 194.m

reg [7:0] memReg[mEndAddress:mStartAddress];
reg [7:0] ioReg[ioEndAddress:ioStartAddress];
reg [7:0] configReg[cEndAddress:cStartAddress];
integer   f_name;
integer i;

/*
initial begin

  case (bus_sel)
  0:	begin
		f_name =$fopen("p_target0.rpt");
		$fdisplay(f_name, "\nTARGET P START HERE");
	end
  1:	begin
		f_name =$fopen("p_target1.rpt");
		$fdisplay(f_name, "\nTARGET A START HERE");
	end
  2:	begin
		f_name =$fopen("p_target2.rpt");
		$fdisplay(f_name, "\nTARGET B START HERE");
	end
  default: begin
           $display("Error_T2_biu: WRONG bus number for target model");
           $stop;
         end
  endcase
end
*/
reg	[31:0]	byte_io;
reg	[31:0]	word_io;
reg	[3:0]	pci_int_;

/**
`define INTERRUPT_ACKNOWLEDGE   4'b0000
`define SPECIAL_CYCLE           4'b0001
`define IO_READ                 4'b0010
`define IO_WRITE                4'b0011
`define PCI_RESERVED1           4'b0100
`define PCI_RESERVED2           4'b0101
`define MEMORY_READ             4'b0110
`define MEMORY_WRITE            4'b0111
`define PCI_RESERVED3           4'b1000
`define PCI_RESERVED4           4'b1001
`define CONFIGURATION_READ      4'b1010
`define CONFIGURATION_WRITE     4'b1011
`define MEMORY_READ_MULTIPLE    4'b1100
`define DUAL_ADDRESS_CYCLE      4'b1101
`define MEMORY_READ_LINE        4'b1110
`define MEMORY_WRITE_INVALIDATE 4'b1111
`define NO_ABORT                0
`define DISCONNECT_WITH_DATA    1
`define DISCONNECT_WITHOUT_DATA 2
`define TARGET_ABORT            3
***/

/////////////////////
wire [31:0] out_ad;
reg oe_ad;
reg out_devsel, oe_devsel;
reg out_trdy,   oe_trdy;
reg out_stop,   oe_stop;
reg oe_par;
reg out_perr,   oe_perr;
wire out_par;

reg t_frame;
reg [ 3:0] t_cmd;
reg [ 3:0] t_cbe_;
reg [31:0] t_data;
reg t_idsel;
reg oe_serr, out_serr;

/////////////////////
wire  in_frame;
wire  in_irdy;
wire  in_devsel;
wire  in_trdy;
wire  in_stop;
wire  in_par;
wire  in_perr;
wire  [31:0] in_ad;
wire  [ 3:0] in_cbe_;
wire  in_idsel;

wire  hit;

wire  io_en = 1'b1;  // 03-12-97 IK Response to IO Cycles
wire mem_en = 1'b1;

wire	latch_data = in_irdy && in_trdy;

// ED:0423
assign # omi_delay ad        = (oe_ad & enable)     ? out_ad      : 32'hz;
assign # omi_delay devsel_   = (oe_devsel & enable) ? !out_devsel : 1'bz;
assign # omi_delay trdy_     = (oe_trdy & enable)   ? !out_trdy   : 1'bz;
assign # omi_delay stop_     = (oe_stop & enable)   ? !out_stop   : 1'bz;
assign # omi_delay par       = (oe_par & enable)    ? out_par     : 1'bz;
assign # omi_delay perr_     = (oe_perr & enable)   ? !out_perr   : 1'bz;
assign # omi_delay serr_     = (oe_serr & enable)   ? !out_serr   : 1'bz;

assign in_ad     = ad;
assign in_cbe_   = cbe_ ;
assign in_idsel  = idsel;
assign # omi_delay in_frame  = !frame_;
assign # omi_delay in_irdy   = !irdy_;
assign # omi_delay in_devsel = !devsel_;
assign # omi_delay in_trdy   = !trdy_;
assign # omi_delay in_stop   = !stop_;
assign in_par    = par;
assign # omi_delay in_perr   = !perr_;

//wire	# omi_delay config_hit= ( (bus_sel == 0) ? t_idsel : 1'b1) & (t_cmd[3:1]  == 3'b101)
//		& ({t_address[15:2],2'b00} >= cStartAddress)
//		& ({t_address[15:2],2'b00} <= cEndAddress);

wire	# omi_delay config_hit= ( (bus_sel == 0) ? t_idsel : 1'b1) & (t_cmd[3:1]  == 3'b101)
			& (t_address[10:8] == 3'b000) & ( t_address[1:0] == 2'b00 );	// function 0 and type 0

wire	# omi_delay io_hit  = io_en  & (t_cmd[3:1] == 3'b001) &
		(({t_address[31:2],2'b00} >= ioStartAddress)
		& ({t_address[31:2],2'b00} <= ioEndAddress));

wire	# omi_delay mem_hit = mem_en & (t_cmd[3:1] == 3'b011)
		& ({t_address[31:2],2'b00} >= mStartAddress)
		& ({t_address[31:2],2'b00} <= mEndAddress);

wire	# omi_delay byte_io_hit = io_en & (t_cmd[3:1] == 3'b001)
		& ({t_address[31:2],2'b00} == ioByteAddress);

wire	# omi_delay word_io_hit = io_en & (t_cmd[3:1] == 3'b001)
		& ({t_address[31:2],2'b00} == ioWordAddress);

always @ ( posedge clock)
if (latch_data & !in_cbe_[0] & t_cmd[0]) begin
   if (config_hit)
   	configReg[{t_address[7:2], 2'b00}] <= # omi_delay in_ad[7:0];
   else
   if (io_hit)
	ioReg    [{t_address[31:2], 2'b00}] <= # omi_delay in_ad[7:0];
   else
   if (mem_hit)
	memReg   [{t_address[31:2], 2'b00}] <= # omi_delay in_ad[7:0];
end

always @ ( posedge clock)
if (latch_data & !in_cbe_[1] & t_cmd[0]) begin
   if (config_hit)
	configReg[{t_address[7:2], 2'b01}] <= # omi_delay in_ad[15:8];
   else
   if (io_hit)
	ioReg    [{t_address[31:2], 2'b01}] <= # omi_delay in_ad[15:8];
   else
   if (mem_hit)
	memReg   [{t_address[31:2], 2'b01}] <= # omi_delay in_ad[15:8];
end

always @ ( posedge clock)
if (latch_data & !in_cbe_[2] & t_cmd[0]) begin
   if (config_hit)
	configReg[{t_address[7:2], 2'b10}] <= # omi_delay in_ad[23:16];
   else
   if (io_hit)
	ioReg    [{t_address[31:2], 2'b10}] <= # omi_delay in_ad[23:16];
   else
   if (mem_hit)
	memReg   [{t_address[31:2], 2'b10}] <= # omi_delay in_ad[23:16];
end

always @ ( posedge clock)
if (latch_data & !in_cbe_[3] & t_cmd[0]) begin
   if (config_hit)
	configReg[{t_address[7:2], 2'b11}] <= # omi_delay in_ad[31:24];
   else
   if (io_hit)
	ioReg    [{t_address[31:2], 2'b11}] <= # omi_delay in_ad[31:24];
   else
   if (mem_hit)
	memReg   [{t_address[31:2], 2'b11}] <= # omi_delay in_ad[31:24];
end

wire	byte_match = (in_cbe_ == 4'b1110 ) | (in_cbe_ == 4'b1101 ) | (in_cbe_ == 4'b1011 ) | (in_cbe_ == 4'b0111 );
always @ (posedge clock or negedge rst_)
if (!rst_)
	byte_io <= 0;
else if (latch_data & byte_io_hit & byte_match & t_cmd[0]) begin
	byte_io[ 7: 0] <= # omi_delay in_ad[ 7: 0];
	byte_io[15: 8] <= # omi_delay in_ad[15: 8];
	byte_io[23:16] <= # omi_delay in_ad[23:16];
	byte_io[31:24] <= # omi_delay in_ad[31:24];
end else if (latch_data & byte_io_hit & ~byte_match ) begin
	if (t_cmd[0])
		$display ("Error_T2_rsim:PCI IO Write Byte Operation--ADDRESS =%h, BE# =%b",t_address,in_cbe_);
	else
		$display ("Error_T2_rsim:PCI IO Read Byte Operation--ADDRESS =%h, BE# =%b",t_address,in_cbe_);
end

wire	word_match = (in_cbe_ == 4'b1100 ) |  (in_cbe_ == 4'b0011 );
always @ (posedge clock or negedge rst_)
if (!rst_)
	word_io <= 0;
else if (latch_data & word_io_hit & word_match & t_cmd[0])	begin
	word_io[ 7: 0] <= # omi_delay in_ad[ 7: 0];
	word_io[15: 8] <= # omi_delay in_ad[15: 8];
	word_io[23:16] <= # omi_delay in_ad[23:16];
	word_io[31:24] <= # omi_delay in_ad[31:24];
end else if (latch_data & word_io_hit & ~word_match) 	begin
	if (t_cmd[0])
		$display ("Error_T2_rsim:PCI IO Write Word Operation--ADDRESS =%h, BE# =%b",t_address,in_cbe_);
	else
		$display ("Error_T2_rsim:PCI IO Write Word Operation--ADDRESS =%h, BE# =%b",t_address,in_cbe_);
end
//===============================================================================
// PCI Interrupt

always @ (posedge clock or negedge rst_)
if (!rst_)
	pci_int_ <= 4'hf;
else if (latch_data & t_cmd[0] & byte_io_hit)	begin
	if (!in_cbe_[0])	pci_int_[0] <= 1'b1;
	if (!in_cbe_[1])	pci_int_[1] <= 1'b1;
	if (!in_cbe_[2])	pci_int_[2] <= 1'b1;
	if (!in_cbe_[3])	pci_int_[3] <= 1'b1;
end
else if (latch_data & ~t_cmd[0] & byte_io_hit)	begin
	if (!in_cbe_[0])	pci_int_[3] <= 1'b0;
	if (!in_cbe_[1])	pci_int_[2] <= 1'b0;
	if (!in_cbe_[2])	pci_int_[1] <= 1'b0;
	if (!in_cbe_[3])	pci_int_[0] <= 1'b0;
end

task	set_int_level;
input	int_idx;
input	int_level;
integer	int_idx;
begin
	pci_int_[int_idx] = int_level;
	@(posedge clock);
end
endtask


/////////////////////////////////////////////////////
/* snow 1999.12.27 */
wire [31:0] config_out;
assign # omi_delay config_out [31:24] = configReg[{t_address[7:2], 2'b11}];
assign # omi_delay config_out [23:16] = configReg[{t_address[7:2], 2'b10}];
assign # omi_delay config_out [15: 8] = configReg[{t_address[7:2], 2'b01}];
assign # omi_delay config_out [ 7: 0] = configReg[{t_address[7:2], 2'b00}];
/*
wire [31:0] io_out     = { ioReg[{t_address[31:2], 2'b11}],
			   ioReg[{t_address[31:2], 2'b10}],
			   ioReg[{t_address[31:2], 2'b01}],
			   ioReg[{t_address[31:2], 2'b00}]};
*/
/*
//copy from SH,2001-11-21
// frank 2001.07.18
`ifdef NO_AUDIO
	wire [31:0] io_out = { ioReg[{t_address[31:2], 2'b11}],
			   ioReg[{t_address[31:2], 2'b10}],
			   ioReg[{t_address[31:2], 2'b01}],
			   ioReg[{t_address[31:2], 2'b00}]};
`else
	wire    [31:0]  loop_value;
	assign loop_value = top.loop;
	wire [31:0] io_out = (t_address == 32'h0000_62f0) ? (loop_value) :
						{ ioReg[{t_address[31:2], 2'b11}],
					   ioReg[{t_address[31:2], 2'b10}],
					   ioReg[{t_address[31:2], 2'b01}],
					   ioReg[{t_address[31:2], 2'b00}]};
`endif
*/

wire [31:0] io_out = { ioReg[{t_address[31:2], 2'b11}],
			   ioReg[{t_address[31:2], 2'b10}],
			   ioReg[{t_address[31:2], 2'b01}],
			   ioReg[{t_address[31:2], 2'b00}]};
/*
// frank 2001.07.27
wire    [31:0]  disp_value = top.encoder.count2;
`ifdef NO_AUDIO
	wire [31:0] io_out = (t_address == 32'h0000_62f4) ? (disp_value) :
				{ ioReg[{t_address[31:2], 2'b11}],
			   ioReg[{t_address[31:2], 2'b10}],
			   ioReg[{t_address[31:2], 2'b01}],
			   ioReg[{t_address[31:2], 2'b00}]};
`else
	wire    [31:0]  loop_value;
	assign loop_value = top.loop;
	wire [31:0] io_out = (t_address == 32'h0000_62f0) ? (loop_value) :
						(t_address == 32'h0000_62f4) ? (disp_value) :
						{ ioReg[{t_address[31:2], 2'b11}],
					   ioReg[{t_address[31:2], 2'b10}],
					   ioReg[{t_address[31:2], 2'b01}],
					   ioReg[{t_address[31:2], 2'b00}]};
`endif
*/
wire [31:0] mem_out    =   { memReg[{t_address[31:2], 2'b11}],
			     memReg[{t_address[31:2], 2'b10}],
			     memReg[{t_address[31:2], 2'b01}],
			     memReg[{t_address[31:2], 2'b00}]};

assign	io_out_display =	io_hit 		? io_out 	:
							byte_io_hit ? byte_io 	:
							word_io_hit ? word_io 	: 32'h0;

assign # omi_delay out_ad = config_hit 	? config_out 	:
							io_hit		? io_out     	:
							mem_hit		? mem_out    	:
							byte_io_hit	? byte_io 		:
							word_io_hit	? word_io 		: 32'h0;

//2000-03-27 bob,from joe's idea
//assign # omi_delay hit = config_hit | io_hit | mem_hit;
/* joe000313 add error reporting for audio device */
//	parameter	[31:0]	SoC_Err_Type_Addr	= 32'h0000_63f0;
//	parameter	[31:0]	SoC_Err_Value0_Addr	= 32'h0000_63f4;
//	parameter	[31:0]	SoC_Err_Value1_Addr	= 32'h0000_63f8;
//	parameter	[31:0]	SoC_Err_Value2_Addr	= 32'h0000_63fc;

parameter	SoC_Err_Type_Addr	= 32'h0000_63f0;
parameter	SoC_Err_Value0_Addr	= 32'h0000_63f4;
parameter	SoC_Err_Value1_Addr	= 32'h0000_63f8;
parameter	SoC_Err_Value2_Addr	= 32'h0000_63fc;

//2001-11-13 yuky, set for audio wafer test, set same as SH
//	parameter	[31:0]	SoC_Err_Type_Addr_1		= 32'h0000_62f0;
//	parameter	[31:0]	SoC_Err_Value0_Addr_1	= 32'h0000_62f4;
//	parameter	[31:0]	SoC_Err_Value1_Addr_1	= 32'h0000_62f8;
//	parameter	[31:0]	SoC_Err_Value2_Addr_1	= 32'h0000_62fc;

parameter	SoC_Err_Type_Addr_1		= 32'h0000_62f0;
parameter	SoC_Err_Value0_Addr_1	= 32'h0000_62f4;
parameter	SoC_Err_Value1_Addr_1	= 32'h0000_62f8;
parameter	SoC_Err_Value2_Addr_1	= 32'h0000_62fc;

parameter	err_rpt_en	= 1'b1;

wire	# omi_delay	soc_err_type_hit	= (err_rpt_en & (t_cmd[3:1] == 3'b001)
		& (({t_address[31:2],2'b00} == SoC_Err_Type_Addr)
			|({t_address[31:2],2'b00} == SoC_Err_Type_Addr_1)));//add by yuky,2001-11-13

wire	# omi_delay	soc_err_value0_hit	= (err_rpt_en & (t_cmd[3:1] == 3'b001)
		& (({t_address[31:2],2'b00} == SoC_Err_Value0_Addr)
			|({t_address[31:2],2'b00} == SoC_Err_Value0_Addr_1)));//add by yuky,2001-11-13

wire	# omi_delay	soc_err_value1_hit	= (err_rpt_en & (t_cmd[3:1] == 3'b001)
		& (({t_address[31:2],2'b00} == SoC_Err_Value1_Addr)
			|({t_address[31:2],2'b00} == SoC_Err_Value1_Addr_1)));//add by yuky,2001-11-13

wire	# omi_delay	soc_err_value2_hit	= (err_rpt_en & (t_cmd[3:1] == 3'b001)
		& (({t_address[31:2],2'b00} == SoC_Err_Value2_Addr)
			|({t_address[31:2],2'b00} == SoC_Err_Value2_Addr_1)));//add by yuky,2001-11-13

wire	# omi_delay soc_err_hit	= soc_err_type_hit
				| soc_err_value0_hit
				| soc_err_value1_hit
				| soc_err_value2_hit;

reg	[31:0]	soc_err_type;
reg	[31:0]	soc_err_value0;
reg	[31:0]	soc_err_value1;
reg	[31:0]	soc_err_value2;

reg	receive_err_msg;
initial	receive_err_msg = 0;
always @(posedge clock or negedge rst_)
	if (!rst_)
		receive_err_msg <= #omi_delay	1'b0;
	else
		receive_err_msg <= #omi_delay latch_data & t_cmd[0] & soc_err_type_hit;

always @(posedge clock) begin
	if (latch_data & t_cmd[0]) begin
		if (!in_cbe_[0]) begin
			if (soc_err_type_hit)
				soc_err_type[7:0]	<= # omi_delay in_ad[7:0];
			else if (soc_err_value0_hit)
				soc_err_value0[7:0]	<= # omi_delay in_ad[7:0];
			else if (soc_err_value1_hit)
				soc_err_value1[7:0]	<= # omi_delay in_ad[7:0];
			else if (soc_err_value2_hit)
				soc_err_value2[7:0]	<= # omi_delay in_ad[7:0];
		end
		if (!in_cbe_[1]) begin
			if (soc_err_type_hit)
				soc_err_type[15:8]	<= # omi_delay in_ad[15:8];
			else if (soc_err_value0_hit)
				soc_err_value0[15:8]	<= # omi_delay in_ad[15:8];
			else if (soc_err_value1_hit)
				soc_err_value1[15:8]	<= # omi_delay in_ad[15:8];
			else if (soc_err_value2_hit)
				soc_err_value2[15:8]	<= # omi_delay in_ad[15:8];
		end
		if (!in_cbe_[2]) begin
			if (soc_err_type_hit)
				soc_err_type[23:16]	<= # omi_delay in_ad[23:16];
			else if (soc_err_value0_hit)
				soc_err_value0[23:16]	<= # omi_delay in_ad[23:16];
			else if (soc_err_value1_hit)
				soc_err_value1[23:16]	<= # omi_delay in_ad[23:16];
			else if (soc_err_value2_hit)
				soc_err_value2[23:16]	<= # omi_delay in_ad[23:16];
		end
		if (!in_cbe_[3]) begin
			if (soc_err_type_hit)
				soc_err_type[31:24]	<= # omi_delay in_ad[31:24];
			else if (soc_err_value0_hit)
				soc_err_value0[31:24]	<= # omi_delay in_ad[31:24];
			else if (soc_err_value1_hit)
				soc_err_value1[31:24]	<= # omi_delay in_ad[31:24];
			else if (soc_err_value2_hit)
				soc_err_value2[31:24]	<= # omi_delay in_ad[31:24];
		end
	end
end

// joe000327
assign # omi_delay hit = config_hit | io_hit | mem_hit |
			byte_io_hit | word_io_hit | soc_err_hit;


//the Finite State Machine
///////////////////////////////
parameter

//  FIFO_EMPTY	=2'b00,
//  FIFO_LAST	=2'b01,
//  FIFO_FULL	=2'b11,
//  AR_WAIT	=2'b00,
//  AR_RETRY	=2'b01,
//  AR_TERM	=2'b10,
//  AR_DONE	=2'b11,

  D_SUB		=1,
  D_SLOW	=0,
  D_MEDIUM	=1,
  ST_IDLE	=0,
  ST_BUSY_M	=1,
  ST_BUSY_S	=2,
  ST_BUSY_SUB	=14,  // 04-03-97 IK Add Subtractive Mode
  ST_DATA	=3,
  ST_BACKOFF	=4,
  ST_TURN_AR	=5,
  ST_DATA_FIFO	=6,
  ST_DATA_EXT	=7,
  ST_BACKOFF_DLY=8,
  ST_TURN_AR_DLY=9,
  ST_IDLE_DLY	=10,	// 0ah
  ST_BUSY_M_DLY	=11,	// 0bh
  ST_BUSY_S_DLY	=12,	// 0ch
  ST_EXT_BURST  =13,	// 0dh
  ST_WAIT       =15;	// 0fh


/////////////////////

reg [3:0] st;
reg start_cycle;
reg oe_perr_tmp;
reg perr;

///////////////////////////
wire dpen;
wire par, out_tmp_perr;

/////////////////////////////////////////
// 07-03-96 IK Latch
always @(negedge rst_ or posedge clock)
	if (!rst_) begin
		t_data <= # omi_delay 32'h0;
		t_cbe_ <= # omi_delay 4'hf;
	end
	else
	if (in_irdy) begin
		t_data [31:0] <= # omi_delay in_ad;
		t_cbe_ [3:0] <=  # omi_delay in_cbe_;
	end

assign # omi_delay dpen=in_irdy & out_trdy;
assign # omi_delay out_par=(^t_data)^(^t_cbe_);

wire #1	w_oe_par      = oe_ad;
wire #1	w_oe_perr     = oe_perr_tmp;
wire #1	w_oe_perr_tmp = dpen & t_cmd[0];
wire #1	w_out_perr    = out_par^in_par;

always @(negedge rst_ or posedge clock)
	if (!rst_) begin
		oe_par      <= # omi_delay 0;
		oe_perr     <= # omi_delay 0;
		oe_perr_tmp <= # omi_delay 0;
		out_perr    <= # omi_delay 0;
	end else begin
		oe_par      <= # omi_delay w_oe_par;
		oe_perr     <= # omi_delay w_oe_perr;
		oe_perr_tmp <= # omi_delay w_oe_perr_tmp;
		out_perr    <= # omi_delay w_out_perr;
	end

wire #1 w_start_cycle= start_cycle;
always @(negedge rst_ or posedge clock)
	if (!rst_)
		oe_serr <= # omi_delay 1'b0;
	else
		oe_serr <= # omi_delay w_start_cycle;

wire #1 w_out_serr= ^{t_cmd,t_address,in_par};
always @(negedge rst_ or posedge clock)
	if (!rst_)
		out_serr <= # omi_delay 1'b0;
	else
		out_serr <= # omi_delay w_out_serr;

always @(negedge rst_ or posedge clock)
	if(!rst_) t_frame <=# omi_delay 0;
	else      t_frame <= #1 in_frame;

/////////////////////////////////////////////////////////
always @(posedge clock or negedge rst_)
if (!rst_) begin
   oe_ad <= # omi_delay 0;
   oe_devsel <= # omi_delay 0;
   oe_trdy <= # omi_delay 0;
   oe_stop <= # omi_delay 0;

   st<= # omi_delay 0;

   t_address <= # omi_delay 0;
   t_cmd <= # omi_delay 4'b0000;
   t_idsel <= # omi_delay 0;

	start_cycle <= # omi_delay 0;

   out_stop <= # omi_delay 0;
   out_trdy <= # omi_delay 0;
   out_devsel <= # omi_delay 0;

   wait_state_counter <= # omi_delay wait_state;
   //request_master=0;
end else
begin
  case (st)
  ST_IDLE:
     begin
       wait_state_counter <= # omi_delay wait_state;
       if (in_frame & !t_frame) begin
	  start_cycle <= # omi_delay 1;
          if (!in_devsel) begin
            	if(dec_mode==0) begin	// fast
	  	  st <= # omi_delay ST_BUSY_M;
		  $display("\nError_T2_biu: Don't support fast decode mode, use medium instead.");
		  $fdisplay(f_name, "\nError_T2_biu: Don't support fast decode mode, use medium instead.");
		end
		else if(dec_mode==1)	// medium
	  	  st <= # omi_delay ST_BUSY_M;
		else if(dec_mode==2)	// slow
	  	  st <= # omi_delay ST_BUSY_S;
		else if(dec_mode==3)	// substractive
	  	  st <= # omi_delay ST_BUSY_SUB;
	    t_cmd     <= # omi_delay in_cbe_;
            t_address <= # omi_delay in_ad;
            t_idsel   <= # omi_delay in_idsel;
          end
       end
     end
  ST_BUSY_M:
     begin

	start_cycle <= # omi_delay 0;
       if (!in_irdy | in_devsel)
	  st <= # omi_delay ST_IDLE;
       else
       if (hit) begin

	case (t_cmd)
	4'b0010: begin
	  $display ("\n******TARGET %0d: Detected IO READ, ADDRESS= %h, BE#= %b, DATA= %h******", bus_sel, t_address, in_cbe_, io_out_display);
	  $fdisplay (f_name, "******TARGET %0d: Detected a IO READ, ADDRESS= %h, BE#= %b, DATA= %h******", bus_sel, t_address, in_cbe_, io_out_display);
	end
	4'b0011: begin
	  $display ("\n******TARGET %0d: Detected IO WRITE, ADDRESS= %h******", bus_sel, t_address);
	  $fdisplay (f_name, "******TARGET %0d: Detected a IO WRITE, ADDRESS= %h******", bus_sel, t_address);
	end
	4'b1100,
	4'b1110,
	4'b0110: begin
	  $display ("\n******TARGET %0d: Detected a MEM READ, ADDRESS= %h, BE#= %b, DATA= %h******", bus_sel, t_address, in_cbe_,mem_out);
	  $fdisplay (f_name, "******TARGET %0d: Detected a MEM READ, ADDRESS= %h, BE#= %b, DATA= %h******", bus_sel, t_address, in_cbe_, mem_out);
	end
	4'b1111,
	4'b0111: begin
	  $display ("\n******TARGET %0d: Detected a MEM WRITE, ADDRESS= %h******", bus_sel, t_address);
	  $fdisplay (f_name, "******TARGET %0d: Detected a MEM WRITE, ADDRESS= %h******", bus_sel, t_address);
	end
	4'b1010: begin
	  $display ("\nTARGET %0d: Detected a CONF_RD, ADDRESS= %h, BE#= %b, DATA= %h", bus_sel, t_address, in_cbe_, config_out);
	  $fdisplay (f_name, "TARGET %0d: Detected a CONF_RD, ADDRESS= %h, BE#= %b, DATA= %h", bus_sel, t_address, in_cbe_, config_out);
	end
	4'b1011: begin
	  $display ("\nTARGET %0d: Detected a CONF_WR, ADDRESS= %h", bus_sel, t_address);
	  $fdisplay (f_name, "TARGET %0d: Detected a CONF_WR, ADDRESS= %h", bus_sel, t_address);
	end
	default: begin
	  $display ("\nTARGET %0d: Detected an unsupported command ",bus_sel);
	  $fdisplay (f_name, "TARGET %0d: Detected an unsupported command", bus_sel);
	end
	endcase


          oe_devsel <= # omi_delay 1; out_devsel <= # omi_delay 1;
          oe_stop <= # omi_delay 1;
          if (!t_cmd[0]) oe_ad  <=  # omi_delay 1;  // cmd == read
          oe_trdy <= # omi_delay 1;

          if (in_frame & set_retry)
	    begin
	     $display ("\n******TARGET %0d: Issued RETRY,     ADDRESS= %h          ******", bus_sel, t_address);
             st <=  # omi_delay ST_BACKOFF;
             out_stop <=  # omi_delay 1;
	     out_trdy <=  # omi_delay 0;
	    end
          else
          if (in_frame & set_disconnect)
	    begin
	     $display ("\n******TARGET %0d: Issued DISCONNECT,ADDRESS= %h, DATA= %h******", bus_sel, t_address, io_out);
             st <=  # omi_delay ST_BACKOFF;
             out_stop <=  # omi_delay 1;
	     out_trdy <=  # omi_delay 1;
	    end
          else
	  if(wait_state_counter==4'h0) begin
	     out_trdy <= # omi_delay 1;
             st <= # omi_delay ST_DATA;
	  end
	  else begin
	     out_trdy <= # omi_delay 0;
             st <= # omi_delay ST_WAIT;
	  end
       end
	else  begin
	 st <= # omi_delay ST_IDLE;
	 oe_ad  <= # omi_delay 0;
      end
     end
  ST_BUSY_S:
     begin
	start_cycle <= # omi_delay 0; // 06-20-97 Ben
       if (!in_irdy | in_devsel)
	  st <= # omi_delay ST_IDLE;
       else
	  st <= # omi_delay ST_BUSY_M;
     end

// 04-03-97 IK
// Subtractive Decoding
  ST_BUSY_SUB:
     begin
	start_cycle <= # omi_delay 0; // 06-20-97 Ben
       if (!in_irdy | in_devsel)
          begin
          st <= # omi_delay ST_IDLE;
          end
        else
          begin
          st <= # omi_delay ST_BUSY_M;
          end
     end

  ST_DATA:  // Agent has accepted request and will respond
     begin

       if ( in_irdy & in_trdy) begin
          //t_cbe_  = in_cbe_;
		if (in_frame) t_address[31:2]  <= # omi_delay  t_address[31:2]+1;
       end

/*
       if (in_frame & set_retry)
	 begin
          st <=  # omi_delay ST_BACKOFF;
          out_stop <=  # omi_delay 1;
	  out_trdy <=  # omi_delay 0;
	 end
       else
*/
       if (in_frame & set_disconnect)
	 begin
	  $display ("\n******TARGET %0d: Issued DISCONNECT,ADDRESS= %h, DATA= %h******", bus_sel, t_address, io_out);
          st <=  # omi_delay ST_BACKOFF;
          out_stop <=  # omi_delay 1;
	  out_trdy <=  # omi_delay 1;
	 end
       else
       if ( in_frame & in_stop & in_trdy & !in_irdy	// TBD
          | in_frame & !in_stop
          | !in_frame &  !in_trdy & !in_stop)
          st <=  # omi_delay ST_DATA;
       else
       if (!in_frame & (in_trdy | in_stop)) begin
          st <=  # omi_delay ST_TURN_AR;
          oe_ad <=  # omi_delay 0;
          out_trdy <=  # omi_delay 0;
          out_devsel <=  # omi_delay 0;
          out_stop <=  # omi_delay 0;		// 07-12-96 IK
       end
     end

  ST_BACKOFF:
     begin
       out_trdy <=  # omi_delay 0;
       if (!in_frame) begin
          st <=   # omi_delay ST_TURN_AR;
          oe_ad <=  # omi_delay 0;
          out_devsel <=  # omi_delay 0;
          out_stop <=  # omi_delay 0;
       end
       else
	 st <=  # omi_delay ST_BACKOFF;
     end

  ST_TURN_AR:
     begin
        oe_devsel <=  # omi_delay 0;
        oe_trdy <=  # omi_delay 0;
        oe_stop <=  # omi_delay 0;

        if (!in_frame)
	  begin
           st <=  # omi_delay ST_IDLE;
	   oe_ad  <=  # omi_delay 0;		// 06-26-96 IK
	  end
        else

	// 07-09-96 IK:  Start Next Cycle
          begin
            t_address <=  # omi_delay in_ad;
            t_cmd     <=  # omi_delay in_cbe_;
            t_idsel   <=  # omi_delay in_idsel;
// 06-20-97  Ben
	    start_cycle <= # omi_delay 1;     // 06-20-97  Ben
	    if(dec_mode==0) begin	// fast
	  	  st <= # omi_delay ST_BUSY_M;
		  $display("\nError_T2_biu: Don't support fast decode mode, use medium instead.");
		  $fdisplay(f_name, "\nError_T2_biu: Don't support fast decode mode, use medium instead.");
		end
	    else if(dec_mode==1)	// medium
	  	  st <= # omi_delay ST_BUSY_M;
	    else if(dec_mode==2)	// slow
	  	  st <= # omi_delay ST_BUSY_S;
	    else if(dec_mode==3)	// substractive
	  	  st <= # omi_delay ST_BUSY_SUB;
          end
     end

  ST_WAIT:
     begin
	  if(enable_retry & set_retry) begin
	     $display ("\n******TARGET %0d: Issued RETRY,     ADDRESS= %h          ******", bus_sel, t_address);
             st <=  # omi_delay ST_BACKOFF;
             out_stop <=  # omi_delay 1;
	     out_trdy <=  # omi_delay 0;
	  end
	  else if(enable_disconnect & set_disconnect) begin
	     $display ("\n******TARGET %0d: Issued DISCONNECT,ADDRESS= %h, DATA= %h******", bus_sel, t_address, io_out);
             st <=  # omi_delay ST_BACKOFF;
             out_stop <=  # omi_delay 1;
	     out_trdy <=  # omi_delay 1;
	  end
	  else if(wait_state_counter==4'h0) begin
	     out_trdy <= # omi_delay 1;
             st <= # omi_delay ST_DATA;
	  end
	  else begin
	     out_trdy <= # omi_delay 0;
             wait_state_counter <= # omi_delay wait_state_counter - 4'h1;
             st <= # omi_delay ST_WAIT;
	  end
     end

  default: begin
	  st <=  # omi_delay ST_IDLE;
	  oe_ad <=  # omi_delay 0;
	end
  endcase
end
////////////////////////////////////////////////

always @(posedge clock) begin
	if (receive_err_msg) begin
		$display("SOC Monitor receive message on %0T",$realtime);
		case(soc_err_type)

		32'h0000_0000: begin
			$display("Aud_Err_Mon => This is audio error monitor.");
			$display("Aud_Err_Mon => Input value 1st %h 2nd %h 3rd %h",
					soc_err_value0, soc_err_value1, soc_err_value2);
		end

		32'h0000_0001: begin
			$display("Error_T2_sintf: Codec97_%b not ready to write",
					soc_err_value0[1:0]);
		end

		32'h0000_0002: begin
			$display("Error_T2 : Set_ADPCM_Channel doesn't support index = %d",
					soc_err_value0[31:0]);
		end

		32'h0000_0003: begin
			$display("Error_T2 : Set_DLS_Channel doesn't support index = %d",
					soc_err_value0[31:0]);
		end

		32'h0000_0004: begin
			$display("Error_T2 : Set_PCM_In_Channel_31 doesn't support index = %d",
					soc_err_value0[31:0]);
		end

		32'h0000_0005: begin
			$display("Error_T2 : Set_MMC_In_Channel_30 doesn't support index = %d",
					soc_err_value0[31:0]);
		end

		32'h0000_0006: begin
			$display("Error_T2 : Set_I2S_In_Channel_29 doesn't support index = %d",
					soc_err_value0[31:0]);
		end

		32'h0000_0007: begin
			$display("Error_T2 : Set_MIC_In_Channel_22 doesn't support index = %d",
					soc_err_value0[31:0]);
		end

		32'h0000_0008: begin
			$display("Error_T2 : Set_LINE1_In_Channel_21 doesn't support index = %d",
					soc_err_value0[31:0]);
		end

		32'h0000_0009: begin
			$display("Error_T2 : Set_LINE2_In_Channel_19 doesn't support index = %d",
					soc_err_value0[31:0]);
		end

		32'h0000_000a: begin
			$display("Error_T2 : Set_HSET_In_Channel_17 doesn't support index = %d",
					soc_err_value0[31:0]);
		end

		32'h0000_000b: begin
			$display("Error_T2 : Set_PCM_Out_Channel_28 doesn't support index = %d",
					soc_err_value0[31:0]);
		end

		32'h0000_000c: begin
			$display("Error_T2 : Set_PCM_Out_Channel_27 doesn't support index = %d",
					soc_err_value0[31:0]);
		end

		32'h0000_000d: begin
			$display("Error_T2 : Set_SURR_Out_Channel_26 doesn't support index = %d",
					soc_err_value0[31:0]);
		end

		32'h0000_000e: begin
			$display("Error_T2 : Set_SURR_Out_Channel_25 doesn't support index = %d",
					soc_err_value0[31:0]);
		end

		32'h0000_000f: begin
			$display("Error_T2 : Set_CENLFE_Out_Channel_24 doesn't support index = %d",
					soc_err_value0[31:0]);
		end

		32'h0000_0010: begin
			$display("Error_T2 : Set_CENLFE_Out_Channel_23 doesn't support index = %d",
					soc_err_value0[31:0]);
		end

		32'h0000_0011: begin
			$display("Error_T2 : Set_LINE1_Out_Channel_20 doesn't support index = %d",
					soc_err_value0[31:0]);
		end

		32'h0000_0012: begin
			$display("Error_T2 : Set_LINE2_Out_Channel_18 doesn't support index = %d",
					soc_err_value0[31:0]);
		end

		32'h0000_0013: begin
			$display("Error_T2 : Set_HSET_Out_Channel_16 doesn't support index = %d",
					soc_err_value0[31:0]);
		end

		32'h0000_0014: begin
			$display("Error_T2 : Set_Test_FW_Channel doesn't support index = %d",
					soc_err_value0[31:0]);
		end

		32'h0000_0015: begin
			$display("Error_T2_rsim: ERR in MEMRD");
		end

		32'h0000_0016: begin
			$display("Error_T2_wave : Playback Underrun at %f!",$time);
		end

		32'h0000_0017: begin
			$display("Error_T2_wave : Recording Overrun %f!",$time);
		end

		32'h0000_0018: begin
			$display("Error_T2_wave : Reg98 is not cleared %f!",$time);
		end

		32'h0000_0019: begin
			$display("Error_T2_wave : Reg9c is not cleared %f!",$time);
		end

		32'h0000_0020: begin
            $display("Error_T2_sintf: Read Codec97_%b Register %hh: error response",
						soc_err_value0[1:0],
						soc_err_value1[6:0]);
        end

		32'h0000_0021: begin
            $display("Error_T2_sintf:Read Codec97_%b Register %hh: no response",
						soc_err_value0[1:0],
						soc_err_value1[6:0]);
        end

		32'h0000_0022: begin
            $display("Error_T2_sintf: Read Codec97_%b Register %hh data= 16'h%h\texpect:16'h%h",
						soc_err_value0[1:0],
						soc_err_value0[22:16],
						soc_err_value1[15:0],
						soc_err_value2[15:0]);
        end

		32'h0000_0023: begin
            $display("Err_T2_aclink: Reg_4C bit15 dont go low");
        end

//display information

		32'h8000_0000: begin
			$display("Write Codec97_%b Register %hh data= 16'h%h",
					soc_err_value0[1:0],
					soc_err_value0[31:16],
					soc_err_value1[15:0]);
		end

		32'h8000_0001: begin
			$display("Set Up Channel:CIR= %d",
					soc_err_value0[4:0]);
		end

		32'h8000_0002: begin
			$display("CSO_SIGN=%b\tCSO=%h\tFMS=%h\tALPHA=%h",
					soc_err_value0[1:0],
					soc_err_value0[31:16],
					soc_err_value1[3:0],
					soc_err_value1[15:4]);
		end

		32'h8000_0003: begin
			$display("LBA=%h\tESO=%h\tDELTA=%h\tCTRL=%h",
					soc_err_value0[31:0],
					soc_err_value1[15:0],
					soc_err_value1[31:16],
					soc_err_value2[15:0]);
		end

		32'h8000_0004: begin
			$display("LBA=%h\tESO=%h\tDELTA=%h",
					soc_err_value0[31:0],
                    soc_err_value1[15:0],
    			    soc_err_value1[31:16]);
		end

		32'h8000_0005: begin
			$display("GVSEL=%b\tPAN=%h\tVOL=%h\tCTRL=%h\tEC=%h",
					soc_err_value0[1:0],
					soc_err_value0[31:16],
					soc_err_value1[15:0],
					soc_err_value1[31:16],
					soc_err_value2[15:0]);
		end

		32'h8000_0006: begin
			$display("CEBC=%b\tAMS=%h\tEBUF1=%h\tEBUF2=%h",
					soc_err_value0[1:0],
					soc_err_value0[31:16],
					soc_err_value1[31:0],
					soc_err_value2[31:0]);
		end

		32'h8000_0007: begin
			$display("SB IRQ Recieved.");
		end

		32'h8000_0008: begin
			$display("MPU401 IRQ Recieved.");
		end

		32'h8000_0009: begin
			$display("OPL3 IRQ Recieved.");
		end

		32'h8000_000a: begin
			$display("Address Engine IRQ Recieved.");
		end

		32'h8000_000b: begin
			$display("INT issued by Address Engine channel %d",
					soc_err_value0[4:0]);
		end

		32'h8000_000c: begin
			$display("Envelope Engine IRQ Recieved.");
		end

		32'h8000_000d: begin
			$display("INT issued by Envelope Engine channel %d",
					soc_err_value0[4:0]);
		end

		32'h8000_000e: begin
			$display("Current Reg80h: %h",
   					soc_err_value0[31:0]);
		end

		32'h8000_000f: begin
			$display("****** Self Loop Detected, Rsim Will Stop ******");
		end

		32'h8000_0010: begin
			$display("LOOP STOP BECAUSE OF COUNTER>3000");
		end

		32'h8000_0011: begin
			$display("vec:aud.init.v> Audio initial start!");
		end

		32'h8000_0012: begin
			$display("Wait ac97_ready...");
		end

		32'h8000_0013: begin
		 	$display("audio_mem_write:offset=%h data=%h be=%b",
					soc_err_value0[31:0],
					soc_err_value1[31:0],
					soc_err_value2[3:0]);
		end

		32'h8000_0014: begin
			$display("audio_mem_readbk:offset=%h be=%b data=%h",
					soc_err_value0[15:0],
                    soc_err_value0[19:16],
					soc_err_value1[31:0]);
		end

		32'h8000_0015: begin
			$display("audio_mem_read:offset=%h be=%b data=%h compdata=%h",
   					soc_err_value0[15:0],
					soc_err_value0[19:16],
					soc_err_value1[31:0],
					soc_err_value2[31:0]);
		end

              32'h8000_0016: begin
                       $display("SUCCESS!");
         end

		32'h8000_0017: begin
	        $display("********IRQ detected*********");
		end

		32'h8000_0018: begin
		    $display("WAIT Playback Samples to AC97 ...");
		end

	    32'h8000_0019: begin
            $display("phase 1 : Wave Engine Global Register programming");
        end

        32'h8000_001a: begin
            $display("phase 2 : Wave Engine Channel Configuration");
        end

		32'h8000_001b: begin
            $display("phase 3 : Wave Engine Channel Start");
        end

		32'h8000_001c: begin
            $display("phase 4 :  INT Reg Polling till All Channels are Stopped");
        end

		32'h8000_001d: begin
			$display("---------------- IRQ Acknowledged.-----------------");
		end

		32'h8000_001e: begin
            $display("----------- IRQ Reg Polling ----------");
        end

		32'h8000_001f: begin
            $display("Current Loop:%h",
					soc_err_value0[31:0]);
        end

		32'h8000_0020: begin
            $display("$$$$$  Loop :  6  $$$$$ ");
        end

		32'h8000_0021: begin
            $display("$$$$$  Loop :  11  $$$$$ ");
        end

		32'h8000_0022: begin
            $display("$$$$$  Loop :  15  $$$$$ ");
        end

		32'h8000_0023: begin
            $display("$$$$$  Loop :  20  $$$$$ ");
        end

		32'h8000_0024: begin
            $display("$$$$$  Loop :  24  $$$$$ ");
        end

		32'h8000_0025: begin
            $display("$$$$$  Loop :  29  $$$$$ ");
        end

		32'h8000_0026: begin
             $display("Phase 23: Recording + 8-ch Wave Engine Playback + Global pause ");
        end

		32'h8000_0027: begin
 		     $display("Phase 2.1 : Wave Engine Global Register programming ");
        end

		32'h8000_0028: begin
             $display("Phase 2 : Special Channel Control Setup ");
        end

		32'h8000_0029: begin
            $display("Phase 2.2 : Recording setup : 16-bit Stereo signed ");
        end

		32'h8000_002a: begin
            $display("Phase 2.3 : 8 WE Channel setup ");
        end

		32'h8000_002b: begin
            $display("Phase 2.4 : PHASE 2 start ");
        end

		32'h8000_002c: begin
            $display("Phase 2.5 Wait till All Channels are Stopped ");
        end

		32'h8000_002d: begin
            $display("Phase 23 done!");
        end

		32'h8000_002e: begin
            $display(" WAIT FIFO EMPTY! ");
        end

		32'h8000_002f: begin
            $display("    Phase 50 done!  ");
        end

		32'h8000_0030: begin
			$display("********* ARAM Test Channel: %d **********",
					soc_err_value0[5:0]);
		end

		32'h8000_0031: begin
            $display("********* ERAM Test Channel: %d **********",
                    soc_err_value0[5:0]);
        end

		32'h8000_0032: begin
			$display("phase 1 : Test DLSRAM  ");
        end

		32'h8000_0033: begin
            $display("phase 2 : Test ARAM  ");
        end

		32'h8000_0034: begin
            $display("phase 3 : Test ERAM  ");
        end

		32'h8000_0035: begin
            $display("phase 4 : Test CACHERAM  ");
        end

		32'h8000_0036: begin
            $display("phase 5 : Test CODECRAM1  ");
        end

		32'h8000_0037: begin
            $display("phase 6 : Test CODECRAM2  ");
        end

		32'h8000_0038: begin
            $display("Current Pattern: ap_01 ");
        end

		32'h8000_0039: begin
            $display("Current Pattern: ap_02 ");
        end

		32'h8000_003a: begin
            $display("Current Pattern: ap_03 ");
        end

		32'h8000_003b: begin
            $display("Current Pattern: ap_04 ");
        end

		32'h8000_003c: begin
            $display("Current Pattern: ap_05 ");
        end

		32'h8000_003d: begin
            $display("Current Pattern: dls_01 ");
        end

		32'h8000_003e: begin
            $display("Current Pattern: dls_02 ");
        end

		32'h8000_003f: begin
            $display("Current Pattern: dls_03 ");
        end

		32'h8000_0040: begin
            $display("Current Pattern: dls_04 ");
        end

		32'h8000_0041: begin
            $display("Current Pattern: dls_05 ");
        end

		32'h8000_0042: begin
            $display("Current Pattern: dls_06 ");
        end

		32'h8000_0043: begin
            $display("Current Pattern: dls_07 ");
        end

		32'h8000_0044: begin
            $display("Current Pattern: dls_08 ");
        end

		32'h8000_0045: begin
            $display("Current Pattern: dls_09 ");
        end

		32'h8000_0046: begin
            $display("Current Pattern: dls_10 ");
        end

		32'h8000_0047: begin
            $display("Current Pattern: dls_11 ");
        end

		32'h8000_0048: begin
            $display("Current Pattern: fw_01 ");
        end

		32'h8000_0049: begin
            $display("Current Pattern: fw_02 ");
        end

		32'h8000_004a: begin
            $display("Current Pattern: fw_03 ");
        end

		32'h8000_004b: begin
            $display("Current Pattern: fw_04 ");
        end

		32'h8000_004c: begin
			$display("Current Pattern: rec_01 ");
		end

		32'h8000_004d: begin
            $display("Current Pattern: rec_02 ");
        end

		32'h8000_004e: begin
            $display("Current Pattern: rec_03 ");
        end

		32'h8000_004f: begin
            $display("Current Pattern: rec_04 ");
        end

		32'h8000_0050: begin
            $display("Current Pattern: rec_05 ");
        end

		32'h8000_0051: begin
            $display("Current Pattern: rec_06 ");
        end

		32'h8000_0052: begin
            $display("Current Pattern: rec_07 ");
        end

		32'h8000_0053: begin
            $display("Current Pattern: rec_08 ");
        end

		32'h8000_0054: begin
            $display("Current Pattern: rec_09 ");
        end

		32'h8000_0055: begin
            $display("Current Pattern: rec_10 ");
        end

		32'h8000_0056: begin
            $display("Current Pattern: rec_11 ");
        end

		32'h8000_0057: begin
            $display("Current Pattern: rec_12 ");
        end

		32'h8000_0058: begin
            $display("Current Pattern: rec_13 ");
        end

		32'h8000_0059: begin
            $display("Current Pattern: rec_14 ");
        end

		32'h8000_005a: begin
            $display("Current Pattern: rec_15 ");
        end

		32'h8000_005b: begin
            $display("Current Pattern: rec_16 ");
        end

		32'h8000_005c: begin
            $display("Current Pattern: rec_17 ");
        end

		32'h8000_005d: begin
            $display("Current Pattern: rec_18 ");
        end

		32'h8000_005e: begin
            $display("Current Pattern: rec_19 ");
        end

		32'h8000_005f: begin
            $display("Current Pattern: rec_20 ");
        end

		32'h8000_0060: begin
            $display("Current Pattern: rec_21 ");
        end

		32'h8000_0061: begin
            $display("Current Pattern: rec_22 ");
        end

		32'h8000_0062: begin
			$display("Current Pattern: spb_01 ");
		end

		32'h8000_0063: begin
            $display("Current Pattern: spb_02 ");
        end

		32'h8000_0064: begin
            $display("Current Pattern: spb_03 ");
        end

		32'h8000_0065: begin
            $display("Current Pattern: we_p31 ");
        end

		32'h8000_0066: begin
            $display("Current Pattern: we_p32 ");
        end

		32'h8000_0067: begin
            $display("Current Pattern: we_p33 ");
        end

		32'h8000_0068: begin
            $display("Current Pattern: we_p34 ");
        end

		32'h8000_0069: begin
            $display("Current Pattern: we_p35");
        end

		32'h8000_006a: begin
            $display("Current Pattern: we_p48 ");
        end

		32'h8000_006b: begin
            $display("Current Pattern: we_p50 ");
        end

		32'h8000_006c: begin
            $display("Current Pattern: we_p51 ");
        end

		32'h8000_006d: begin
            $display("Current Pattern: we_p52 ");
        end

		32'h8000_006e: begin
            $display("Current Pattern: we_p53 ");
        end

		32'h8000_006f: begin
            $display("Current Pattern: wt_regmem ");
        end

		32'h8000_0070: begin
			$display("Phase tsaclink : Tsaclink Global Register Functional Testing");
		end

		32'h8000_0071: begin
            $display("Read Codec97_%b Register %hh data= 16'h%h",
						soc_err_value0[1:0],
						soc_err_value1[6:0],
						soc_err_value2[15:0]);
        end

		32'h8000_0072: begin
            $display("Phase tsaclink.1 : Reg40h 44h 48h 4ch 70h 7ch read write test ");
        end

		32'h8000_0073: begin
            $display("Phase tsaclink.2 : GPIO register testing ");
        end

		32'h8000_0074: begin
            $display("Phase tsaclink.3 : Reg40h 44h function test ");
        end

		32'h8000_0075: begin
            $display("Phase tsaclink done!  ");
        end

		32'h8000_0076: begin
            $display("Phase s_all: Recording + 1-ch Wave Engine Playback  ");
        end

		32'h8000_0077: begin
            $display("Phase s_all.1 : Wave Engine Global Register programming  ");
        end

		32'h8000_0078: begin
            $display("Phase s_all.3 :  WE Channel   ");
        end

		32'h8000_0079: begin
            $display("Current Patten: s_all  ");
        end

		32'h8000_007a: begin
            $display("Phase s_all done!  ");
        end

		32'h8000_007b: begin
            $display("Phase s_mixer_reg: Recording + 1-ch Wave Engine Playback ");
        end

		32'h8000_007c: begin
            $display("Phase s_mixer_reg.1 : Wave Engine Global Register programming  ");
        end

		32'h8000_007d: begin
            $display("Phase s_mixer_reg.2 : Recording setup : 16-bit Stereo signed  ");
        end


		32'h8000_007e: begin
            $display("Phase s_mixer_reg.3 :  WE Channel   ");
        end

		32'h8000_007f: begin
            $display("Phase s_mixer_reg.4 :  read Codec97 registers default value  ");
        end

		32'h8000_0080: begin
            $display("Phase s_mixer_reg.5 :  test Codec97 variable sample rate  ");
        end

		32'h8000_0081: begin
            $display("Phase s_mixer_reg.6 :  test Codec97  channel volume and level  ");
        end

		32'h8000_0082: begin
            $display("Current Patten: s_mixer_reg ");
        end

		32'h8000_0083: begin
            $display("Phase s_mixer_reg done!   ");
        end






// from 'h100 are used by ZH

		32'h0000_0100: begin
			$display("Error_T2_rsim: SDRAM Read ERROR, Wrong Address=%h,ReadData=%h,Expected=%h",
					soc_err_value0[31:0], soc_err_value1[31:0],soc_err_value2[31:0]);
		end

		32'h0000_0101: begin
			$display("Error_T2_rsim: PCI33IO Read ERROR: Wrong Address=%h,ReadData=%h,Expected=%h",
					soc_err_value0[31:0], soc_err_value1[31:0],soc_err_value2[31:0]);

		end

		32'h0000_0102: begin
			$display("Error_T2_rsim: PCI33MEM Read ERROR, Wrong Address=%h,ReadData=%h,Expected=%h",
					soc_err_value0[31:0], soc_err_value1[31:0],soc_err_value2[31:0]);

		end

		32'h0000_0103: begin
			$display("Error_T2_rsim: PCI66IO Read ERROR, Wrong Address=%h,ReadData=%h,Expected=%h",
					soc_err_value0[31:0], soc_err_value1[31:0],soc_err_value2[31:0]);

		end

		32'h0000_0104: begin
			$display("Error_T2_rsim: PCI66MEM Read ERROR: Wrong Address=%h,ReadData=%h,Expected=%h",
					soc_err_value0[31:0], soc_err_value1[31:0],soc_err_value2[31:0]);

		end

		32'h0000_0105: begin
			$display("Error_T2_rsim: IO Read ERROR, Wrong Address=%h,ReadData=%h,Expected=%h",
					soc_err_value0[31:0], soc_err_value1[31:0],soc_err_value2[31:0]);

		end

		32'h0000_0106: begin
			$display("Error_T2_rsim: MEM Read ERROR (USB), Wrong Address=%h, ReadData=%h,Expected=%h",
					soc_err_value0[31:0], soc_err_value1[31:0],soc_err_value2[31:0]);

		end

		32'h0000_0107: begin
			$display("Error_T2_rsim: Config Read ERROR (USB), Wrong OffSet=%h, ReadData=%h,Expected=%h",
					soc_err_value0[31:0], soc_err_value1[31:0],soc_err_value2[31:0]);

		end

		32'h0000_0108: begin
			$display("Error_T2_rsim: Local IO Read Error, Wrong Address=%h, ReadData=%h,Expected=%h",
					soc_err_value0[31:0], soc_err_value1[31:0],soc_err_value2[31:0]);

		end

		32'h0000_0109: begin
			$display("Error_T2_rsim: Config Read ERROR, Wrong Address=%h, ReadData=%h,Expected=%h",
					soc_err_value0[31:0], soc_err_value1[31:0],soc_err_value2[31:0]);

		end

		default: begin
			if (soc_err_type !== 32'hxxxx_xxxx)
			$display("Aud_Err_Mon => an unsupported error type%h issued!",
					soc_err_type);
		end
		endcase
	end
end

//============== Task area ======================
//initial the pci memory for wafer test, yuky,2002-03-05
integer	init_tmp;
initial begin
for (init_tmp = mStartAddress; init_tmp <= mEndAddress; init_tmp = init_tmp + 1)
	memReg[init_tmp] = init_tmp;
end

// initial ioReg and configReg to aviod 'x' state when read -- Runner, 2002-07-09
initial begin
for (init_tmp = ioStartAddress; init_tmp <= ioEndAddress; init_tmp = init_tmp + 1)
	ioReg[init_tmp] = 0;
end

initial begin
for (init_tmp = cStartAddress; init_tmp <= cEndAddress; init_tmp = init_tmp + 1)
	configReg[init_tmp] = 0;
end


//add by yuky, 2001-12-07
task	PCI_MEM_MIPS_Initial;
begin
	begin
	$display("\nInitial Test Program Data in PCI Memory");
        $readmemh("pci_mips.txt", memReg,	mStartAddress, mEndAddress);
	end
end
endtask

endmodule
