/**********************************************************************************
// Description:	The clock monitor, can snoop the clock status dynamic.
//				clock dynamic parameter:
//				1). whether in "z" or "x" status.
//				2). whether always in "1" or "0" status.
//				3). clock frequency or period, duty cycle, initial status
// File:	clock_monitor.v
// Author:	yuky
//
// History:	2002-09-09, initial version,following the new coding style
//			2002-10-17, modify the report method with $fdisplay.
//						and this module only can be invoded in the "top" module
//						And the top module handle name must be "clk_monitor_handle"
//						If you want to invode this module in other project, you must
//						re-define the macro at the head of this file
***********************************************************************************/
`ifdef  	NO_CLK_MONITOR
module	clock_monitor (
		IN_CLK,				//The clock need to be monitor.
		START_TRIGGER_POINT,			//what time start to monitor the clock. 1--> active
		REGULAR,			//the clock whether is a regular clock. 1--> regular, 0--> unregular
							//if unregular clock, report the average frequency.
		SENSITIVE_EDGE,		//1--> rise edge sensitive, 0--> fall edge sensitive
							//if rise edge sensitive, use the rise edge to calculate the period
							//if fall edge sensitive, use the fall edge to calculate the period
		SENSITIVE_DUTY,		//1--> Duty cycle sensitive, 0--> Don't care the duty cycle change
		OBJECT_CLK_SCALE	//point out the object clock's(be measured) frquency scale.
							//0 --> scale : 100MHz ~ 10GHz, 1 --> scale:   2MHz ~ 200MHz
							//2 --> scale :  40KHz ~  4MHz, 3 --> scale: 0.8KHz ~  80KHz
		);
//port define
input	IN_CLK;
input	START_TRIGGER_POINT;
input	REGULAR;
input	SENSITIVE_EDGE;
input	SENSITIVE_DUTY;
input	[1:0] OBJECT_CLK_SCALE;

`else  
`define		U_HANDLE	top.clk_monitor_handle

`timescale	1ns/1ps
module	clock_monitor (
		IN_CLK,				//The clock need to be monitor.
		START_TRIGGER_POINT,			//what time start to monitor the clock. 1--> active
		REGULAR,			//the clock whether is a regular clock. 1--> regular, 0--> unregular
							//if unregular clock, report the average frequency.
		SENSITIVE_EDGE,		//1--> rise edge sensitive, 0--> fall edge sensitive
							//if rise edge sensitive, use the rise edge to calculate the period
							//if fall edge sensitive, use the fall edge to calculate the period
		SENSITIVE_DUTY,		//1--> Duty cycle sensitive, 0--> Don't care the duty cycle change
		OBJECT_CLK_SCALE	//point out the object clock's(be measured) frquency scale.
							//0 --> scale : 100MHz ~ 10GHz, 1 --> scale:   2MHz ~ 200MHz
							//2 --> scale :  40KHz ~  4MHz, 3 --> scale: 0.8KHz ~  80KHz
		);
//port define
input	IN_CLK;
input	START_TRIGGER_POINT;
input	REGULAR;
input	SENSITIVE_EDGE;
input	SENSITIVE_DUTY;
input	[1:0] OBJECT_CLK_SCALE;
//The monitor object name ASCII number
//default:XXXXXX
parameter BYTE5 = 88;
parameter BYTE4 = 88;
parameter BYTE3 = 88;
parameter BYTE2 = 88;
parameter BYTE1 = 88;
parameter BYTE0 = 88;

//parameter define
parameter	f_tolerance = 0.015; //Define the clock frequency tolerance value. less 100%. If the clock
								//frequency changing is greater than tolerance, then report the clock changing.
parameter	d_tolerance = 0.01; //Define the clock duty cycle tolerance value. less 100%. If the clock
								//duty cycle changing is greater than tolerance, then report the clock changing.
parameter	ref_clk_period = 5;	//200M
parameter	udly  = 0.001;
// port connection part
wire	in_clk = IN_CLK;
wire	start_trigger_point  = START_TRIGGER_POINT;
wire	regular        = REGULAR        ;
wire	sensitive_edge = SENSITIVE_EDGE ;
wire	sensitive_duty = SENSITIVE_DUTY ;
wire	[1:0]object_clk_scale = OBJECT_CLK_SCALE;
//Make the 4 character to one ID
reg	[47:0] ID;
initial ID = {BYTE5[7:0],BYTE4[7:0],BYTE3[7:0],BYTE2[7:0],BYTE1[7:0],BYTE0[7:0]};

// define the real type datas
real		clk_rise_edge_time1, clk_rise_edge_time2,
			clk_fall_edge_time1, clk_fall_edge_time2;
real  		in_clk_period_1, in_clk_period_2,
			in_clk_high_period, in_clk_low_period;
real		unreg_clk_frequency;
reg	[63:0]	clk_period_int_1, clk_period_int_2, clk_period_int_latch;
reg	[63:0]	clk_duty_cycle_int, clk_duty_cycle_int_latch;
reg	[63:0]	unreg_frequency_int, unreg_frequency_int_latch;
// generate a reference clock
real		ref_clk_period_real_1;
real		ref_clk_period_real_2;
real		ref_clk_period_real_3;
real		ref_clk_period_real_4;
//real		ref_clk_frequency_1;
//real		ref_clk_frequency_2;
//real		ref_clk_frequency_3;
//real		ref_clk_frequency_4;
real		ref_clk_frequency;
reg		ref_clk_1, ref_clk_2, ref_clk_3, ref_clk_4;
//decode
wire	scale_1_hit = (object_clk_scale == 2'b00);
wire	scale_2_hit = (object_clk_scale == 2'b01);
wire	scale_3_hit = (object_clk_scale == 2'b10);
wire	scale_4_hit = (object_clk_scale == 2'b11);

initial begin
	ref_clk_1 = 0;
	ref_clk_2 = 0;
	ref_clk_3 = 0;
	ref_clk_4 = 0;
end
initial	begin
	ref_clk_period_real_1 = ref_clk_period;				//200M
	ref_clk_period_real_2 = ref_clk_period * 50;		//4M
	ref_clk_period_real_3 = ref_clk_period_real_2 * 50;	//80k
	ref_clk_period_real_4 = ref_clk_period_real_3 * 50; //1.6k
	#udly;
	if (scale_1_hit)
		ref_clk_frequency	= 1000 / ref_clk_period_real_1;
	else if (scale_2_hit)
		ref_clk_frequency	= 1000 / ref_clk_period_real_2;
	else if (scale_3_hit)
		ref_clk_frequency	= 1000 / ref_clk_period_real_3;
	else if (scale_4_hit)
		ref_clk_frequency	= 1000 / ref_clk_period_real_4;

end
always #(ref_clk_period_real_1/2)	ref_clk_1 = ~ref_clk_1;
always #(ref_clk_period_real_2/2)	ref_clk_2 = ~ref_clk_2;
always #(ref_clk_period_real_3/2)	ref_clk_3 = ~ref_clk_3;
always #(ref_clk_period_real_4/2)	ref_clk_4 = ~ref_clk_4;
wire	ref_clk = 	scale_1_hit && ref_clk_1 ||
					scale_2_hit && ref_clk_2 ||
					scale_3_hit && ref_clk_3 ||
					scale_4_hit && ref_clk_4 ;

// check the input clock status whether is "x" or "z"
//1:initial status
initial	begin
	#0.001;
	//$display("\tThe %s_clk initial value is %h at %0.3fns", ID, in_clk, $realtime);
	$fdisplay(`U_HANDLE,"\tThe %s_clk initial value is %h at %0.3fns", ID, in_clk, $realtime);
end

//2:automatic to check it status
always	@(in_clk)	//include posedge and negedge
	if (start_trigger_point) begin
		if (in_clk === 1'bz) begin
		/*
			$display("\tWarning_T2_rsim: %s_clk is in high resistance value: %h after %0.3fns\n",
					ID, in_clk, $realtime);
		*/
		$fdisplay(`U_HANDLE,"\tWarning_T2_rsim: %s_clk is in high resistance value: %h after %0.3fns\n",
					ID, in_clk, $realtime);
		end
		if (in_clk === 1'bx) begin
		/*
			$display("\tWarning_T2_rsim: %s_clk is in uncertain value: %h after %0.3fns\n",
					ID, in_clk, $realtime);
		*/
		$fdisplay(`U_HANDLE,"\tWarning_T2_rsim: %s_clk is in uncertain value: %h after %0.3fns\n",
					ID, in_clk, $realtime);
		end
	end

//calculate the input clock period/frequency, and duty
initial begin
	clk_rise_edge_time1	= 0;
	clk_rise_edge_time2	= 0;
	clk_fall_edge_time1	= 0;
	clk_fall_edge_time2	= 0;
	in_clk_period_1		= 0;
	in_clk_period_2		= 0;
	in_clk_high_period	= 0;
	in_clk_low_period	= 0;
	clk_period_int_1	= 0;
	clk_period_int_2	= 0;
	clk_period_int_latch= 0;
	clk_duty_cycle_int	= 0;
	clk_duty_cycle_int_latch = 0;
	unreg_clk_frequency = 1;
	unreg_frequency_int = 0;
	unreg_frequency_int_latch = 0;
end
//latch the rise edge real time
always @(posedge in_clk)
	if (start_trigger_point)
		begin
   		clk_rise_edge_time1 <= #udly $realtime;
   		clk_rise_edge_time2 <= #udly clk_rise_edge_time1;
  		end
    else
    	begin
   		clk_rise_edge_time1 <= #udly 0;
   		clk_rise_edge_time2 <= #udly 0;
  		end
//latch the fall edge real time
always @(negedge in_clk)
	if (start_trigger_point)
		begin
   		clk_fall_edge_time1 <= #udly $realtime;
   		clk_fall_edge_time2 <= #udly clk_fall_edge_time1;
  		end
	else
		begin
   		clk_fall_edge_time1 <= #udly 0;
   		clk_fall_edge_time2 <= #udly 0;
  		end
//calculate the clock period with rising edge
always @(negedge in_clk)
	if (start_trigger_point)
		if (clk_rise_edge_time2 != 0)	begin
			in_clk_period_1		<= #udly clk_rise_edge_time1 - clk_rise_edge_time2;
			clk_period_int_1  	<= #udly (clk_rise_edge_time1 - clk_rise_edge_time2) * 1000; //make ps to ns
		end
	else
		begin
		in_clk_period_1		<= #udly	0;
		clk_period_int_1  	<= #udly	0;
		end

//calculate the clock period with falling edge
always @(posedge in_clk)
	if (start_trigger_point)
		if (clk_fall_edge_time2 != 0)	begin
			in_clk_period_2		<= #udly clk_fall_edge_time1 - clk_fall_edge_time2;
			clk_period_int_2	<= #udly (clk_fall_edge_time1 - clk_fall_edge_time2) * 1000; //make ps to ns
		end
	else
		begin
		in_clk_period_2		<= #udly 0;
		clk_period_int_2	<= #udly 0;
		end
//calculate the clock high level period
always @(posedge in_clk)
	if (start_trigger_point)
		if (clk_fall_edge_time1 != 0) begin
			in_clk_high_period <= #udly clk_fall_edge_time1 - clk_rise_edge_time1;
			clk_duty_cycle_int <= #udly (clk_fall_edge_time1 - clk_rise_edge_time1) * 1000;
		end
	else
		begin
		in_clk_high_period <= #udly	0;
		clk_duty_cycle_int <= #udly	0;
		end
//calculate the clock low level period
/*
always @(negedge in_clk)
	if (clk_rise_edge_time1 != 0) begin
		in_clk_low_period <= #udly clk_rise_edge_time1 - clk_fall_edge_time1;
	end
*/
//calculate the clock frequency and its duty and display
//wire clk_pe_change_1 = (clk_period_int_latch != clk_period_int_1);
//wire clk_pe_change_2 = (clk_period_int_latch != clk_period_int_2);
//wire clk_du_change_1 = (clk_duty_cycle_int_latch != clk_duty_cycle_int);
//determine whether clk change 1
wire	[63:0]	clk_period_sub_result_1 = clk_period_int_latch - clk_period_int_1;
wire	[63:0]	clk_period_cmp_1 = (clk_period_sub_result_1 == 0) ? 64'h0 :
				(	clk_period_sub_result_1[63]?
 					(~clk_period_sub_result_1 + 1 )	:
					clk_period_sub_result_1
				);
wire	[63:0]	error_clk_period_1 = clk_period_int_latch * f_tolerance;
wire 	clk_pe_change_1 =	(clk_period_cmp_1 > error_clk_period_1);
//determine whether clk change 2
wire	[63:0]	clk_period_sub_result_2 = clk_period_int_latch - clk_period_int_2;
wire	[63:0]	clk_period_cmp_2 = (clk_period_sub_result_2 == 0) ? 64'h0 :
				(	clk_period_sub_result_2[63]?
 					(~clk_period_sub_result_2 + 1 )	:
					clk_period_sub_result_2
				);
wire	[63:0]	error_clk_period_2 = clk_period_int_latch * f_tolerance;
wire 	clk_pe_change_2 =	(clk_period_cmp_2 > error_clk_period_2);

//determine whether duty cycle change
wire	[63:0]	clk_duty_sub_result = clk_duty_cycle_int_latch - clk_duty_cycle_int;
wire	[63:0]	clk_duty_cmp = (clk_duty_sub_result == 0) ? 64'h0 :
				(	clk_duty_sub_result[63]?
 					(~clk_duty_sub_result + 1 )	:
					clk_duty_sub_result
				);
wire	[63:0]	error_clk_duty = clk_period_int_1 * d_tolerance;
wire 	clk_du_change_1 =	(clk_duty_cmp > error_clk_duty);

always @(in_clk)
	if ((in_clk === 1'bz) || (in_clk === 1'bx))
		;
	else begin
		clk_duty_cycle_int_latch <= #udly clk_duty_cycle_int;
		if (start_trigger_point & sensitive_edge & regular) begin
			clk_period_int_latch <= #udly clk_period_int_1;
		end
		else if (start_trigger_point & ~sensitive_edge & regular) begin
			clk_period_int_latch <= #udly clk_period_int_2;
		end
	end

wire rpt_duty_start = |clk_duty_cycle_int_latch;
wire rpt_pe_start_1 = |clk_period_int_1;
wire rpt_pe_start_2 = |clk_period_int_2;
wire rpt_pe_start = (sensitive_edge & rpt_pe_start_1 | ~sensitive_edge & rpt_pe_start_2);
wire rpt_start = rpt_pe_start & (sensitive_duty  & rpt_duty_start | ~sensitive_duty);

always @(posedge clk_pe_change_1 or posedge clk_pe_change_2 or posedge clk_du_change_1) begin
	if (start_trigger_point & sensitive_edge & regular & rpt_start) begin
		if (clk_pe_change_1 | clk_du_change_1 & sensitive_duty) begin
		/*
			$display("\n\tAfter %0.3fns:---", ($realtime - in_clk_period_1));
			$display("\t%s_clk period: %0.3fns /Frequency: %0.3fMHz \n\tDuty cycle: %0.3f\%",
					 ID, in_clk_period_1, 1000/in_clk_period_1, in_clk_high_period*100/in_clk_period_1);
			$display("\tFrequency tolerance: %0.6fMHz \n\tDuty cycle tolerance: %0.6f\%",
					 1000/in_clk_period_1 * f_tolerance, 100*d_tolerance*in_clk_high_period/in_clk_period_1);
			$display("");
		*/
		$fdisplay(`U_HANDLE,"\n\tAfter %0.3fns:---", ($realtime - in_clk_period_1));
		$fdisplay(`U_HANDLE,"\t%s_clk period: %0.3fns /Frequency: %0.3fMHz \n\tDuty cycle: %0.3f\%",
					 ID, in_clk_period_1, 1000/in_clk_period_1, in_clk_high_period*100/in_clk_period_1);
		$fdisplay(`U_HANDLE,"\tFrequency tolerance: %0.6fMHz \n\tDuty cycle tolerance: %0.6f\%",
					 1000/in_clk_period_1 * f_tolerance, 100*d_tolerance*in_clk_high_period/in_clk_period_1);
		$fdisplay(`U_HANDLE,"");
		end
	end
	if (start_trigger_point & ~sensitive_edge & regular & rpt_start) begin
		if (clk_pe_change_2 | clk_du_change_1 & sensitive_duty) begin
		/*
			$display("\n\tAfter %0.3fns:---", ($realtime - in_clk_period_2));
			$display("\t%s_clk period: %0.3fns /Frequency: %0.3fMHz \n\tDuty cycle: %0.3f\%",
					 ID, in_clk_period_2, 1000/in_clk_period_2, in_clk_high_period*100/in_clk_period_2);
			$display("\tFrequency tolerance: %0.6fMHz \n\tDuty cycle tolerance: %0.6f\%",
					 1000/in_clk_period_2 * f_tolerance, 100*d_tolerance*in_clk_high_period/in_clk_period_2);
			$display("");
		*/
		$fdisplay(`U_HANDLE,"\n\tAfter %0.3fns:---", ($realtime - in_clk_period_2));
		$fdisplay(`U_HANDLE,"\t%s_clk period: %0.3fns /Frequency: %0.3fMHz \n\tDuty cycle: %0.3f\%",
					 ID, in_clk_period_2, 1000/in_clk_period_2, in_clk_high_period*100/in_clk_period_2);
		$fdisplay(`U_HANDLE,"\tFrequency tolerance: %0.6fMHz \n\tDuty cycle tolerance: %0.6f\%",
					 1000/in_clk_period_2 * f_tolerance, 100*d_tolerance*in_clk_high_period/in_clk_period_2);
		$fdisplay(`U_HANDLE,"");
		end
	end
end
//==========================================================
//calculate the unregular clock
reg	[8:0]	ref_clk_cnt;	//9bits
reg	[15:0]	in_clk_cnt;		//16bits
initial	begin
	ref_clk_cnt	= 0;
	in_clk_cnt	= 0;
end
wire ref_clk_cnt_full	= ref_clk_cnt == 9'hff;

always @(posedge ref_clk) begin
	if (start_trigger_point) begin
		if ((in_clk === 1'bz) || (in_clk === 1'bx) || ref_clk_cnt_full)
			ref_clk_cnt <= #udly 0;
		else
			ref_clk_cnt <= #udly ref_clk_cnt + 1;
	end else
		ref_clk_cnt <= #udly 0;
end

always @(in_clk or posedge ref_clk_cnt_full) begin
	if (start_trigger_point) begin
		if ((in_clk === 1'bz) || (in_clk === 1'bx) || ref_clk_cnt_full)
			in_clk_cnt <= #udly 0;
		else if (sensitive_edge & in_clk)   //add at posedge
			in_clk_cnt <= #udly in_clk_cnt + 1;
		else if (~sensitive_edge & ~in_clk) //add at negedge
			in_clk_cnt <= #udly in_clk_cnt + 1;
		else if (in_clk_cnt == 16'hffff)
		//	$display ("\tWarning_T2_rsim: %s_clk's frequency is higher than test scale",ID);
		$fdisplay(`U_HANDLE,"\tWarning_T2_rsim: %s_clk's frequency is higher than test scale",ID);
	end	else
		in_clk_cnt <= #udly 0;
end

//wire unreg_frequency_change_1 = (unreg_frequency_int_latch != unreg_frequency_int);
wire	[63:0]	unreg_frequency_sub_result = unreg_frequency_int_latch - unreg_frequency_int;
wire	[63:0]	unreg_frequency_cmp = (unreg_frequency_sub_result == 0) ? 64'h0 :
				(	unreg_frequency_sub_result[63]?
 					(~unreg_frequency_sub_result + 1 )	:
					unreg_frequency_sub_result
				);
wire	[63:0]	error_unreg_frequency = unreg_frequency_int_latch * f_tolerance;
wire unreg_frequency_change_1 =	(unreg_frequency_cmp > error_unreg_frequency);

always @(ref_clk_cnt_full)
	if (start_trigger_point & ~regular) begin
		if (ref_clk_cnt_full) begin
			unreg_clk_frequency <= (ref_clk_frequency * in_clk_cnt)/ ref_clk_cnt;
			unreg_frequency_int <= ((ref_clk_frequency * in_clk_cnt)/ ref_clk_cnt) * 1000;
		end else begin
			unreg_frequency_int_latch <= unreg_frequency_int;
			if (unreg_frequency_change_1)	begin
			/*
				$display("\n\tBefore %0.3fns:---", $realtime);
				$display("\t%s_clk average period: %0.3fns /Average frequency: %0.3fMHz \n\tFrequency tolerance: %0.6fMHz\n",
					     ID, 1000/unreg_clk_frequency, unreg_clk_frequency, unreg_clk_frequency * f_tolerance);
			*/
			$fdisplay(`U_HANDLE,"\n\tBefore %0.3fns:---", $realtime);
			$fdisplay(`U_HANDLE,"\t%s_clk average period: %0.3fns /Average frequency: %0.3fMHz \n\tFrequency tolerance: %0.6fMHz\n",
					     ID, 1000/unreg_clk_frequency, unreg_clk_frequency, unreg_clk_frequency * f_tolerance);
			end
		end
	end

//Check the clock whether in "1" or "0" for a long time.
reg		zero_flag_1, zero_flag_2;
wire	zero_flag = zero_flag_1 && !zero_flag_2;
initial begin
	zero_flag_1 = 0;
	zero_flag_2 = 0;
end
always @(posedge ref_clk_cnt_full)
	if (in_clk_cnt == 0) begin
		zero_flag_1 <= #udly 1;
		zero_flag_2 <= #udly zero_flag_1;
		if (zero_flag)	begin
			if (scale_1_hit) begin
			/*
				$display ("\n\tWarning_T2_rsim: %s_clk's frequency is lower than 782KHz!",ID);
				$display ("\t                 %s_clk's is in %b status for a long time!\n", ID,in_clk);
			*/
			$fdisplay(`U_HANDLE,"\n\tWarning_T2_rsim: %s_clk's frequency is lower than 782KHz!",ID);
			$fdisplay(`U_HANDLE,"\t                 %s_clk's is in %b status for a long time!\n", ID,in_clk);
			end
			if (scale_2_hit) begin
			/*
				$display ("\n\tWarning_T2_rsim: %s_clk's frequency is lower than 16KHz!",ID);
				$display ("\t                 %s_clk's is in %b status for a long time!\n", ID,in_clk);
			*/
			$fdisplay(`U_HANDLE,"\n\tWarning_T2_rsim: %s_clk's frequency is lower than 16KHz!",ID);
			$fdisplay(`U_HANDLE,"\t                 %s_clk's is in %b status for a long time!\n", ID,in_clk);
			end
			if (scale_3_hit) begin
			/*
				$display ("\n\tWarning_T2_rsim: %s_clk's frequency is lower than 0.33KHz!", ID);
				$display ("\t                 %s_clk's is in %b status for a long time!\n", ID, in_clk);
			*/
			$fdisplay(`U_HANDLE,"\n\tWarning_T2_rsim: %s_clk's frequency is lower than 0.33KHz!", ID);
			$fdisplay(`U_HANDLE,"\t                 %s_clk's is in %b status for a long time!\n", ID, in_clk);
			end
			if (scale_4_hit) begin
			/*
				$display ("\n\tWarning_T2_rsim: %s_clk's frequency is lower than 6KHz!", ID);
				$display ("\t                 %s_clk's is in %b status for a long time!\n", ID, in_clk);
			*/
			$fdisplay(`U_HANDLE,"\n\tWarning_T2_rsim: %s_clk's frequency is lower than 6KHz!", ID);
			$fdisplay(`U_HANDLE,"\t                 %s_clk's is in %b status for a long time!\n", ID, in_clk);
			end
		end
	end
	else	begin
		zero_flag_1 <= #udly 0;
		zero_flag_2 <= #udly 0;
	end

`endif	
endmodule //end of clock monitor module

