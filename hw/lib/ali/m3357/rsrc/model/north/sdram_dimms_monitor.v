/******************************************************************************
	  (c) copyrights 2000-. All rights reserved

	   T-Square Design, Inc.

	  Company confidential and Properietary	information.
	  This information may not be disclosed	to unauthorized individual.
*******************************************************************************/

/*******************************************************************************
 *
 *       PROJECT:M6304
 *
 *       FILE NAME: sdram_dimms_monitor.v
 *
 *       DESCRIPTION: External SDRAM Memory Bus Monitor
 *						For checking the external SDRAM memory bus operations
 *
 *
 *       AUTHOR: Figo OU
 *
 *       HISTORY:   11/04/02         initial version
 *
 *********************************************************************************/

module	sdram_dimms_monitor(
	//System IO:
	ram_timeparam,
	ram_mode_set,
	ram_16bits_mode,
//	ram_pre_oe_en,
	ram_post_oe_en,
	ram_r2w_ctrl,
	//SDRAM Memory Bus:
	ram_cke,
	ram_cs_,
	ram_ras_,
	ram_cas_,
	ram_we_,
	ram_dqm,
	ram_ba,
	ram_addr,
	ram_wdata,
	ram_rdata,
	ram_dq_oe_,

	//Others:
	rst_,
	mem_clk
	);

parameter	udly	=	1'b1;

//	-----------------------------------------------------------------------------------
//System IO:
input[15:0]	ram_timeparam;
input[11:0]	ram_mode_set;
input		ram_16bits_mode;
//input		ram_pre_oe_en;
input		ram_post_oe_en;
input		ram_r2w_ctrl;

//SDRAM Memory Bus:
input		ram_cke;
input[1:0]	ram_cs_;
input		ram_ras_;
input		ram_cas_;
input		ram_we_;
input[3:0]	ram_dqm;
input[1:0]	ram_ba;
input[11:0]	ram_addr;
input[31:0]	ram_wdata;
input[31:0]	ram_rdata;
input[31:0]	ram_dq_oe_;
//Others:
input		rst_,
			mem_clk;



wire	dimm0_is_idle,dimm1_is_idle;
`ifdef	NEW_MEM

`else
sdram_dimm_monitor	sdram_dimm0_monitor(
	//System IO:
	.ram_timeparam			(ram_timeparam			),
	.ram_mode_set			(ram_mode_set			),
	.ram_16bits_mode		(ram_16bits_mode		),
//	.ram_pre_oe_en			(ram_pre_oe_en			),
	.ram_post_oe_en			(ram_post_oe_en			),
	.ram_r2w_ctrl			(ram_r2w_ctrl			),
	//SDRAM Memory Bus:
	.ram_cke				(ram_cke			    ),
	.ram_cs_				(ram_cs_[0]			    ),
	.ram_ras_				(ram_ras_				),
	.ram_cas_				(ram_cas_				),
	.ram_we_				(ram_we_			    ),
	.ram_dqm				(ram_dqm			    ),
	.ram_ba					(ram_ba					),
	.ram_addr				(ram_addr				),
	.ram_wdata				(ram_wdata				),
	.ram_rdata				(ram_rdata				),
	.ram_dq_oe_				(ram_dq_oe_				),
	//dimm state:
	.dimm_is_idle			(dimm0_is_idle			),
//	.ext_dimm_st			(dimm1_is_idle			),
	.ext_dimm_st			(1			),
	//Others:
	.rst_					(rst_					),
	.mem_clk				(mem_clk				)
	);


sdram_dimm_monitor	sdram_dimm1_monitor(
	//System IO:
	.ram_timeparam			(ram_timeparam			),
	.ram_mode_set			(ram_mode_set			),
	.ram_16bits_mode		(ram_16bits_mode		),
//	.ram_pre_oe_en			(ram_pre_oe_en			),
	.ram_post_oe_en			(ram_post_oe_en			),
	.ram_r2w_ctrl			(ram_r2w_ctrl			),
	//SDRAM Memory Bus:
	.ram_cke				(ram_cke			    ),
	.ram_cs_				(ram_cs_[1]			    ),
//	.ram_cs_				(			    ),		//JFR030630
	.ram_ras_				(ram_ras_				),
	.ram_cas_				(ram_cas_				),
	.ram_we_				(ram_we_			    ),
	.ram_dqm				(ram_dqm			    ),
	.ram_ba					(ram_ba					),
	.ram_addr				(ram_addr				),
	.ram_wdata				(ram_wdata				),
	.ram_rdata				(ram_rdata				),
	.ram_dq_oe_				(ram_dq_oe_				),
	//dimm state:
	.dimm_is_idle			(dimm1_is_idle			),
	.ext_dimm_st			(dimm0_is_idle			),
	//Others:
	.rst_					(rst_					),
	.mem_clk				(mem_clk				)
	);
`endif

endmodule


