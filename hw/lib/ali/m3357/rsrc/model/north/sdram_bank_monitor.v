/******************************************************************************
	  (c) copyrights 2000-. All rights reserved
 
	   T-Square Design, Inc.
 
	  Company confidential and Properietary	information.
	  This information may not be disclosed	to unauthorized individual.	  
*******************************************************************************/

/*******************************************************************************
 *
 *       PROJECT:M6304
 *
 *       FILE NAME: sdram_bank_monitor.v
 *
 *       DESCRIPTION: External SDRAM Memory Bank Operation Monitor
 *						For checking the external SDRAM memory Bank operations
 *
 *
 *       AUTHOR: Figo OU
 *
 *       HISTORY:   11/04/02         initial version
 *      
 *********************************************************************************/
module	sdram_bank_monitor(
	bank_pre_cmd,
	bank_act_cmd,
	bank_rd_cmd,
	bank_rdap_cmd,
	bank_wr_cmd,
	bank_wrap_cmd,
	time_trcd,
	time_trp,
	time_trc,
	time_tras,
	bank_is_idle,
	sdram_clk
	);

parameter	udly	=	1'b1;
//	-------------------------------------------------------------------------------
input		bank_pre_cmd;
input		bank_act_cmd;
input		bank_rd_cmd;
input		bank_rdap_cmd;
input		bank_wr_cmd;
input		bank_wrap_cmd;
input[3:0]	time_trcd;
input[3:0]	time_trp;
input[3:0]	time_trc;
input[3:0]	time_tras;

output		bank_is_idle;
input		sdram_clk;
	
//	-------------------------------------------------------------------------------
parameter	UNKNOW		=	3'b000,
			IDLE		=	3'b001,
			PRECHARGING	=	3'b010,
			ACTIVATING	=	3'b011,
			ACTIVATE	=	3'b100,
			READAP		=	3'b101,
			WRITEAP		=	3'b110;

reg[2:0]	bank_st;
reg[3:0]	time_cnt;
reg[3:0]	tras_cnt;

wire		time_out;
wire		tras_cnt_out;
wire[5:0]	bank_cmd_bus;

wire		bank_is_idle;

initial	begin
	bank_st		=	UNKNOW;
	time_cnt	=	4'h0;
	tras_cnt	=	4'h0;
end

always	@(posedge sdram_clk)
	case(bank_st)
		UNKNOW:	begin
			case(bank_cmd_bus)
				6'b00_0000:	begin
					bank_st		<=	#udly	UNKNOW;
					time_cnt	<=	#udly	time_cnt;
					tras_cnt	<=	#udly	tras_cnt;
				end
				6'b00_0001:	begin
					bank_st		<=	#udly	PRECHARGING;
					time_cnt	<=	#udly	time_trp;
					tras_cnt	<=	#udly	tras_cnt;
				end
				6'b00_0010:	begin
					$display("\n%m\tAt Time: %t\nError_T2_rsim: SDRAM Initialization is needed before ACT\n",$realtime);
					bank_st		<=	#udly	UNKNOW;
					time_cnt	<=	#udly	time_cnt;
					tras_cnt	<=	#udly	tras_cnt;
				end
				6'b00_0100:	begin
					$display("\n%m\tAt Time: %t\nError_T2_rsim: SDRAM Initialization is needed before READ\n",$realtime);
					bank_st		<=	#udly	UNKNOW;
					time_cnt	<=	#udly	time_cnt;
					tras_cnt	<=	#udly	tras_cnt;
				end
				6'b00_1000:	begin
					$display("\n%m\tAt Time: %t\nError_T2_rsim: SDRAM Initialization is needed before READAP\n",$realtime);
					$display("\n%m\tAt Time: %t\nError_T2_rsim: T2-M6305-SOC SDRAM COMMAND DECODE ERROR\n",$realtime);
					bank_st		<=	#udly	UNKNOW;
					time_cnt	<=	#udly	time_cnt;
					tras_cnt	<=	#udly	tras_cnt;
				end
				6'b01_0000:	begin
					$display("\n%m\tAt Time: %t\nError_T2_rsim: SDRAM Initialization is needed before WRITE\n",$realtime);
					bank_st		<=	#udly	UNKNOW;
					time_cnt	<=	#udly	time_cnt;
					tras_cnt	<=	#udly	tras_cnt;
				end
				6'b10_0000:	begin
					$display("\n%m\tAt Time: %t\nError_T2_rsim: SDRAM Initialization is needed before WRITEAP\n",$realtime);
					$display("\n%m\tAt Time: %t\nError_T2_rsim: T2-M6305-SOC SDRAM COMMAND DECODE ERROR\n",$realtime);
					bank_st		<=	#udly	UNKNOW;
					time_cnt	<=	#udly	time_cnt;
					tras_cnt	<=	#udly	tras_cnt;
				end
				default:	begin
					$display("\n%m\tAt Time: %t\nError_T2_rsim: T2-M6305-SOC SDRAM COMMAND DECODE ERROR\n",$realtime);
					bank_st		<=	#udly	UNKNOW;
					time_cnt	<=	#udly	time_cnt;
					tras_cnt	<=	#udly	tras_cnt;
				end
			endcase
		end
		IDLE:	begin
			case(bank_cmd_bus)
				6'b00_0000:	begin
					bank_st		<=	#udly	IDLE;
					time_cnt	<=	#udly	time_cnt;
					tras_cnt	<=	#udly	tras_cnt;
				end
				6'b00_0001:	begin
					bank_st		<=	#udly	PRECHARGING;
					time_cnt	<=	#udly	time_trp;
					tras_cnt	<=	#udly	tras_cnt;
				end
				6'b00_0010:	begin
					bank_st		<=	#udly	ACTIVATING;
					time_cnt	<=	#udly	time_trcd;
					tras_cnt	<=	#udly	time_tras;
				end
				6'b00_0100:	begin
					$display("\n%m\tAt Time: %t\nError_T2_rsim: SDRAM ACTIVATING is needed before READ\n",$realtime);
					bank_st		<=	#udly	IDLE;
					time_cnt	<=	#udly	time_cnt;
					tras_cnt	<=	#udly	tras_cnt;
				end
				6'b00_1000:	begin
					$display("\n%m\tAt Time: %t\nError_T2_rsim: SDRAM ACTIVATING is needed before READAP\n",$realtime);
					$display("\n%m\tAt Time: %t\nError_T2_rsim: T2-M6305-SOC SDRAM COMMAND DECODE ERROR\n",$realtime);
					bank_st		<=	#udly	IDLE;
					time_cnt	<=	#udly	time_cnt;
					tras_cnt	<=	#udly	tras_cnt;
				end
				6'b01_0000:	begin
					$display("\n%m\tAt Time: %t\nError_T2_rsim: SDRAM ACTIVATING is needed before WRITE\n",$realtime);
					bank_st		<=	#udly	IDLE;
					time_cnt	<=	#udly	time_cnt;
					tras_cnt	<=	#udly	tras_cnt;
				end
				6'b10_0000:	begin
					$display("\n%m\tAt Time: %t\nError_T2_rsim: SDRAM ACTIVATING is needed before WRITEAP\n",$realtime);
					$display("\n%m\tAt Time: %t\nError_T2_rsim: T2-M6305-SOC SDRAM COMMAND DECODE ERROR\n",$realtime);
					bank_st		<=	#udly	IDLE;
					time_cnt	<=	#udly	time_cnt;
					tras_cnt	<=	#udly	tras_cnt;
				end
				default:	begin
					$display("\n%m\tAt Time: %t\nError_T2_rsim: T2-M6305-SOC SDRAM COMMAND DECODE ERROR\n",$realtime);
					bank_st		<=	#udly	IDLE;
					time_cnt	<=	#udly	time_cnt;
					tras_cnt	<=	#udly	tras_cnt;
				end
			endcase
		end
		PRECHARGING:	begin
			if(time_out)begin
				bank_st		<=	#udly	IDLE;
				time_cnt	<=	#udly	time_cnt;
				tras_cnt	<=	#udly	tras_cnt;
			end
			else	begin
				bank_st		<=	#udly	PRECHARGING;
				time_cnt	<=	#udly	time_cnt - 1'b1;
				tras_cnt	<=	#udly	tras_cnt;
			end
			case(bank_cmd_bus)
				6'b00_0000:	begin
				end
				6'b00_0001:	begin
					$display("\n%m\tAt Time: %t\nError_T2_rsim: SDRAM Trp Timing Violation before PRE\n",$realtime);
				end
				6'b00_0010:	begin
					$display("\n%m\tAt Time: %t\nError_T2_rsim: SDRAM Trp Timing Violation before ACT\n",$realtime);
				end
				6'b00_0100:	begin
					$display("\n%m\tAt Time: %t\nError_T2_rsim: SDRAM Trp Timing Violation before READ\n",$realtime);
					$display("\n%m\tAt Time: %t\nError_T2_rsim: SDRAM ACTIVATING is needed before READ\n",$realtime);
				end
				6'b00_1000:	begin
					$display("\n%m\tAt Time: %t\nError_T2_rsim: SDRAM Trp Timing Violation before READAP\n",$realtime);
					$display("\n%m\tAt Time: %t\nError_T2_rsim: SDRAM ACTIVATING is needed before READAP\n",$realtime);
					$display("\n%m\tAt Time: %t\nError_T2_rsim: T2-M6305-SOC SDRAM COMMAND DECODE ERROR\n",$realtime);
				end
				6'b01_0000:	begin
					$display("\n%m\tAt Time: %t\nError_T2_rsim: SDRAM Trp Timing Violation before WRITE\n",$realtime);
					$display("\n%m\tAt Time: %t\nError_T2_rsim: SDRAM ACTIVATING is needed before WRITE\n",$realtime);
				end
				6'b10_0000:	begin
					$display("\n%m\tAt Time: %t\nError_T2_rsim: SDRAM Trp Timing Violation before WRITEAP\n",$realtime);
					$display("\n%m\tAt Time: %t\nError_T2_rsim: SDRAM ACTIVATING is needed before WRITEAP\n",$realtime);
					$display("\n%m\tAt Time: %t\nError_T2_rsim: T2-M6305-SOC SDRAM COMMAND DECODE ERROR\n",$realtime);
				end
				default:	begin
					$display("\n%m\tAt Time: %t\nError_T2_rsim: T2-M6305-SOC SDRAM COMMAND DECODE ERROR\n",$realtime);
				end
			endcase
		end
		ACTIVATING:	begin
			if(tras_cnt_out)begin
				tras_cnt	<=	#udly	tras_cnt;
			end
			else	begin
				tras_cnt	<=	#udly	tras_cnt - 1'b1;
			end
			if(time_out)begin
				bank_st		<=	#udly	ACTIVATE;
				time_cnt	<=	#udly	time_cnt;
			end
			else	begin
				bank_st		<=	#udly	ACTIVATING;
				time_cnt	<=	#udly	time_cnt - 1'b1;
			end
			case(bank_cmd_bus)
				6'b00_0000:	begin
				end
				6'b00_0001:	begin
					$display("\n%m\tAt Time: %t\nError_T2_rsim: SDRAM Trcd Timing Violation before PRE\n",$realtime);
					if(!tras_cnt_out)begin
						$display("\n%m\tAt Time: %t\nError_T2_rsim: SDRAM Tras Timing Violation before PRE\n",$realtime);
					end
				end
				6'b00_0010:	begin
					$display("\n%m\tAt Time: %t\nError_T2_rsim: SDRAM Trcd Timing Violation before ACT\n",$realtime);
					$display("\n%m\tAt Time: %t\nError_T2_rsim: Illegal SDRAM Command, SDRAM Bank has been activated already!\n",$realtime);
				end
				6'b00_0100:	begin
					$display("\n%m\tAt Time: %t\nError_T2_rsim: SDRAM Trcd Timing Violation before READ\n",$realtime);
				end
				6'b00_1000:	begin
					$display("\n%m\tAt Time: %t\nError_T2_rsim: SDRAM Trcd Timing Violation before READAP\n",$realtime);
					$display("\n%m\tAt Time: %t\nError_T2_rsim: T2-M6305-SOC SDRAM COMMAND DECODE ERROR\n",$realtime);
				end
				6'b01_0000:	begin
					$display("\n%m\tAt Time: %t\nError_T2_rsim: SDRAM Trcd Timing Violation before WRITE\n",$realtime);
				end
				6'b10_0000:	begin
					$display("\n%m\tAt Time: %t\nError_T2_rsim: SDRAM Trcd Timing Violation before WRITEAP\n",$realtime);
					$display("\n%m\tAt Time: %t\nError_T2_rsim: T2-M6305-SOC SDRAM COMMAND DECODE ERROR\n",$realtime);
				end
				default:	begin
					$display("\n%m\tAt Time: %t\nError_T2_rsim: T2-M6305-SOC SDRAM COMMAND DECODE ERROR\n",$realtime);
				end
			endcase
		end
		ACTIVATE:	begin
			if(tras_cnt_out)begin
				tras_cnt	<=	#udly	tras_cnt;
			end
			else	begin
				tras_cnt	<=	#udly	tras_cnt - 1'b1;
			end
			case(bank_cmd_bus)
				6'b00_0000:	begin
				end
				6'b00_0001:	begin
					if(tras_cnt_out)begin
						bank_st		<=	#udly	PRECHARGING;
						time_cnt	<=	#udly	time_trp;
					end
					else	begin
						$display("\n%m\tAt Time: %t\nError_T2_rsim: SDRAM Tras Timing Violation before PRE\n",$realtime);
						bank_st		<=	#udly	ACTIVATE;
						time_cnt	<=	#udly	time_cnt;
					end
				end
				6'b00_0010:	begin
					$display("\n%m\tAt Time: %t\nError_T2_rsim: Illegal SDRAM Command, SDRAM Bank has been activated already!\n",$realtime);
					bank_st		<=	#udly	ACTIVATE;
					time_cnt	<=	#udly	time_cnt;
				end
				6'b00_0100:	begin
					bank_st		<=	#udly	ACTIVATE;
					time_cnt	<=	#udly	time_cnt;
				end
				6'b00_1000:	begin
					bank_st		<=	#udly	READAP;
					time_cnt	<=	#udly	time_cnt;
					$display("\n%m\tAt Time: %t\nError_T2_rsim: T2-M6305-SOC SDRAM COMMAND DECODE ERROR\n",$realtime);
				end
				6'b01_0000:	begin
					bank_st		<=	#udly	ACTIVATE;
					time_cnt	<=	#udly	time_cnt;
				end
				6'b10_0000:	begin
					bank_st		<=	#udly	WRITEAP;
					time_cnt	<=	#udly	time_cnt;
					$display("\n%m\tAt Time: %t\nError_T2_rsim: T2-M6305-SOC SDRAM COMMAND DECODE ERROR\n",$realtime);
				end
				default:	begin
					$display("\n%m\tAt Time: %t\nError_T2_rsim: T2-M6305-SOC SDRAM COMMAND DECODE ERROR\n",$realtime);
				end
			endcase
		end
		READAP:	begin
			$display("\n%m\tAt Time: %t\nError_T2_rsim: T2-M6305-SOC SDRAM COMMAND DECODE ERROR\n",$realtime);
			$display("\n%m\tAt Time: %t\nError_T2_rsim: It is impossible to issue READAP command!\n",$realtime);
			bank_st		<=	#udly	IDLE;
			time_cnt	<=	#udly	time_cnt;
			tras_cnt	<=	#udly	tras_cnt;
		end
		WRITEAP:	begin
			$display("\n%m\tAt Time: %t\nError_T2_rsim: T2-M6305-SOC SDRAM COMMAND DECODE ERROR\n",$realtime);
			$display("\n%m\tAt Time: %t\nError_T2_rsim: It is impossible to issue WRITEAP command!\n",$realtime);
			bank_st		<=	#udly	IDLE;
			time_cnt	<=	#udly	time_cnt;
			tras_cnt	<=	#udly	tras_cnt;
		end
		default:	begin
			$display("\n%m\tAt Time: %t\nError_T2_rsim: T2-M6305-SOC SDRAM COMMAND DECODE ERROR\n",$realtime);
			bank_st		<=	#udly	IDLE;
			time_cnt	<=	#udly	time_cnt;
			tras_cnt	<=	#udly	tras_cnt;
		end
	endcase


//---------------------------------------------
assign	time_out		=	time_cnt == 4'h0;
assign	tras_cnt_out	=	tras_cnt == 4'h0;

assign	bank_cmd_bus	=	{bank_wrap_cmd,bank_wr_cmd,
							bank_rdap_cmd,bank_rd_cmd,bank_act_cmd,bank_pre_cmd};

assign	bank_is_idle	=	bank_st == IDLE;

endmodule

