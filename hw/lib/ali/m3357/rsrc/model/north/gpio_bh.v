module	gpio_bh ( gpio );
inout 	[31:0]	gpio;

//check gpio[31:16], connect gpio[31:24] to gpio[23:16]
assign	gpio[31:24] = 	gpio[3] ? 	gpio[23:16] 	: 8'hz;
assign	gpio[23:16] = 	~gpio[3] ? 	gpio[31:24] 	: 8'hz;
//check gpio[15:8], connect gpio[15:12] to gpio[11:8]
assign	gpio[15:12] = 	gpio[3]	?	gpio[11:8]	: 4'hz;
assign	gpio[11:8] = 	~gpio[3] ?	gpio[15:12]	: 4'hz;
//check gpio[7:3], connect gpio[7:6] to gpio[5:4]
assign	gpio[7:6]   = 	gpio[3] ?	gpio[5:4]	: 2'hz;
assign	gpio[5:4]   = 	~gpio[3] ?	gpio[7:6]	: 2'hz;
endmodule