/******************************************************************************
          (c) copyrights 1997-2000. All rights reserved

           T-Square Design, Inc.

          Company confidential and Properietary information.
          This information may not be disclosed to unauthorized
          individual.
******************************************************************************/
/*********************************************************************
//File 	: 	pro_basic_tasks.v
//Description:
//			used send out the one thread processing
//			commands: write 1word, 2words, 4words, 8words
//					  read 1word, 2words, 4words, 8words
//
//Author: 	Yuky
//Date	: 	2002-05-13	initial version
//			2002-05-17  because the xxx_pro_ack has been latched in
//						the pro_mux module, this module only use this
//						signal's posedge triger
*********************************************************************/
module	pro_basic_tasks(
	pro_xxx_req,		// request arbitration
	pro_xxx_wr_rd_,		// 1 means write, 0 means read
	pro_xxx_addr,		// request address
	pro_xxx_byte,		// byte enable, high level active
	pro_xxx_wordsize,	// word number
	pro_xxx_data0,		// first data to send
	pro_xxx_data1,		// second data to send
	pro_xxx_data2,		// third data to send
	pro_xxx_data3,		// fourth data to send
	pro_xxx_data4,		// fifth data to send
	pro_xxx_data5,		// sixth data to send
	pro_xxx_data6,		// seventh data to send
	pro_xxx_data7,		// eighth data to send
	xxx_pro_ack,		// accept the request data, high level active
	xxx_pro_data0,		// first data for receive
	xxx_pro_data1,		// second data for receive
	xxx_pro_data2,		// third data for receive
	xxx_pro_data3,		// fourth data for receive
	xxx_pro_data4,		// fifth data for receive
	xxx_pro_data5,		// sixth data for receive
	xxx_pro_data6,		// seventh data for receive
	xxx_pro_data7,		// eighth data for receive

	g_sclk,				// pipe line clock
	pro_req_lock		// ask arbitration to lock its request for continuous accessing
	);

parameter	clk_dly		=	1.3;
parameter 	task_dly	=	0.3;
output			pro_xxx_req;
output			pro_xxx_wr_rd_;
output	[31:0]	pro_xxx_addr;
output	[3:0]	pro_xxx_byte;
output	[1:0]	pro_xxx_wordsize;
output	[31:0]	pro_xxx_data0;
output	[31:0]	pro_xxx_data1;
output	[31:0]	pro_xxx_data2;
output	[31:0]	pro_xxx_data3;
output	[31:0]	pro_xxx_data4;
output	[31:0]	pro_xxx_data5;
output	[31:0]	pro_xxx_data6;
output	[31:0]	pro_xxx_data7;
output			pro_req_lock;
input			xxx_pro_ack;
input	[31:0]	xxx_pro_data0;
input	[31:0]	xxx_pro_data1;
input	[31:0]	xxx_pro_data2;
input	[31:0]	xxx_pro_data3;
input	[31:0]	xxx_pro_data4;
input	[31:0]	xxx_pro_data5;
input	[31:0]	xxx_pro_data6;
input	[31:0]	xxx_pro_data7;

input			g_sclk;

reg			pro_xxx_req_r;
reg			pro_xxx_wr_rd_;
reg	[31:0]	pro_xxx_addr;
reg	[3:0]	pro_xxx_byte;
reg	[1:0]	pro_xxx_wordsize;
reg	[31:0]	pro_xxx_data0;
reg	[31:0]	pro_xxx_data1;
reg	[31:0]	pro_xxx_data2;
reg	[31:0]	pro_xxx_data3;
reg	[31:0]	pro_xxx_data4;
reg	[31:0]	pro_xxx_data5;
reg	[31:0]	pro_xxx_data6;
reg	[31:0]	pro_xxx_data7;

reg			pro_xxx_req_dly_1;

reg			pro_req_lock_set;
reg	[7:0]	lock_counter;
reg [7:0]	MAX_LOCK_COUNT;
wire		pro_req_lock,
			cnt_zero,
			lock_count;
//=================================================================
initial begin
	pro_xxx_req_r		= 0;
	pro_xxx_wr_rd_	= 0;
	pro_xxx_addr		= 32'h0;
	pro_xxx_byte		= 4'h0;
	pro_xxx_wordsize	= 2'h0;
	pro_xxx_data0		= 32'h0;
	pro_xxx_data1		= 32'h0;
	pro_xxx_data2		= 32'h0;
	pro_xxx_data3		= 32'h0;
	pro_xxx_data4		= 32'h0;
	pro_xxx_data5		= 32'h0;
	pro_xxx_data6		= 32'h0;
	pro_xxx_data7		= 32'h0;
	pro_req_lock_set	= 0;
	MAX_LOCK_COUNT		= 15; //16 continuous requests
	lock_counter		= 15;
	pro_xxx_req_dly_1	= 0;
end
//delay	the pro_xxx_req
wire	#task_dly	pro_xxx_req = pro_xxx_req_r;
//delay the clock
wire	#clk_dly	intl_clk	=	g_sclk;
//latch the request with input clock
always @(posedge g_sclk)
	pro_xxx_req_dly_1 <= pro_xxx_req_r;
//calculate the last requset
wire	last_req_act = pro_xxx_req_dly_1 & (g_sclk & ~ intl_clk);

//auto check the pro_req_lock_set whether greater than the max lock counter
always @(posedge xxx_pro_ack or negedge pro_req_lock_set)
	if (cnt_zero)
		lock_counter <= MAX_LOCK_COUNT;
	else if(pro_req_lock_set)
		lock_counter <= lock_counter - 1;
	else
		lock_counter <= MAX_LOCK_COUNT;

assign	#1 cnt_zero	= (lock_counter == 0);
assign	lock_count 	= !cnt_zero;


assign	pro_req_lock = lock_count & pro_req_lock_set;
//=================================================================
task write_1w;
input	[31:0] addr;
input	[3:0]  byte;
input	[31:0] data;
begin
	if (~last_req_act) begin
		@(posedge	g_sclk);
	end
	pro_xxx_wr_rd_	= 1;
	pro_xxx_addr	= addr;
	pro_xxx_byte	= byte;
	pro_xxx_data0	= data;
	pro_xxx_wordsize	= 0;
	pro_xxx_req_r	= 1;
	@(posedge xxx_pro_ack)
		pro_xxx_req_r	= 0;
end
endtask

task write_2w;
input	[31:0]	addr;
input	[31:0]	data0, data1;
begin
	if (~last_req_act) begin
		@(posedge	g_sclk);
	end
	pro_xxx_wr_rd_	= 1;
	pro_xxx_addr	= addr;
	pro_xxx_byte	= 4'hf;
	pro_xxx_data0	= data0;
	pro_xxx_data1	= data1;
	pro_xxx_wordsize	= 1;
	pro_xxx_req_r	= 1;
	@(posedge xxx_pro_ack)
		pro_xxx_req_r	= 0;
end
endtask

task write_4w;
input	[31:0]	addr;
input	[31:0]	data0, data1, data2, data3;
begin
	if (~last_req_act) begin
		@(posedge	g_sclk);
	end
	pro_xxx_wr_rd_	= 1;
	pro_xxx_addr	= addr;
	pro_xxx_byte	= 4'hf;
	pro_xxx_data0	= data0;
	pro_xxx_data1	= data1;
	pro_xxx_data2	= data2;
	pro_xxx_data3	= data3;
	pro_xxx_wordsize	= 2;
	pro_xxx_req_r	= 1;
	@(posedge xxx_pro_ack)
		pro_xxx_req_r	= 0;
end
endtask

task write_8w;
input	[31:0]	addr;
input	[31:0]	data0, data1, data2, data3;
input	[31:0]	data4, data5, data6, data7;
begin
	if (~last_req_act) begin
		@(posedge	g_sclk);
	end
	pro_xxx_wr_rd_	= 1;
	pro_xxx_addr	= addr;
	pro_xxx_byte	= 4'hf;
	pro_xxx_data0	= data0;
	pro_xxx_data1	= data1;
	pro_xxx_data2	= data2;
	pro_xxx_data3	= data3;
	pro_xxx_data4	= data4;
	pro_xxx_data5	= data5;
	pro_xxx_data6	= data6;
	pro_xxx_data7	= data7;
	pro_xxx_wordsize	= 3;
	pro_xxx_req_r	= 1;
	@(posedge xxx_pro_ack)
		pro_xxx_req_r	= 0;
end
endtask

task read_1w;
input	[31:0]	addr;
input	[3:0]	byte;
output	[31:0]	rdbk_data;
begin
	if (~last_req_act)	begin
		@(posedge	g_sclk);
	end
	pro_xxx_wr_rd_	= 0;
	pro_xxx_addr	= addr;
	pro_xxx_byte	= byte;
	pro_xxx_wordsize	= 0;
	pro_xxx_req_r	= 1;
	@(posedge xxx_pro_ack)
		begin
		pro_xxx_req_r	= 0;
		rdbk_data	= xxx_pro_data0;
		end
end
endtask

task read_2w;
input	[31:0]	addr;
output	[31:0]	rdbk_data0, rdbk_data1;
begin
	if (~last_req_act) begin
		@(posedge	g_sclk);
	end
	pro_xxx_wr_rd_	= 0;
	pro_xxx_addr	= addr;
	pro_xxx_byte	= 4'hf;
	pro_xxx_wordsize	= 1;
	pro_xxx_req_r	= 1;
	@(posedge xxx_pro_ack)
		begin
		pro_xxx_req_r	= 0;
		rdbk_data0	= xxx_pro_data0;
		rdbk_data1	= xxx_pro_data1;
		end
end
endtask

task read_4w;
input	[31:0]	addr;
output	[31:0]	rdbk_data0, rdbk_data1, rdbk_data2, rdbk_data3;
begin
	if (~last_req_act) begin
		@(posedge	g_sclk);
	end
	pro_xxx_wr_rd_	= 0;
	pro_xxx_addr	= addr;
	pro_xxx_byte	= 4'hf;
	pro_xxx_wordsize	= 2;
	pro_xxx_req_r	= 1;
	@(posedge xxx_pro_ack)
		begin
		pro_xxx_req_r	= 0;
		rdbk_data0	= xxx_pro_data0;
		rdbk_data1	= xxx_pro_data1;
		rdbk_data2	= xxx_pro_data2;
		rdbk_data3	= xxx_pro_data3;
		end
end
endtask

task read_8w;
input	[31:0]	addr;
output	[31:0]	rdbk_data0, rdbk_data1, rdbk_data2, rdbk_data3;
output	[31:0]	rdbk_data4, rdbk_data5, rdbk_data6, rdbk_data7;
begin
	if (~last_req_act) begin
		@(posedge	g_sclk);
	end
	pro_xxx_wr_rd_	= 0;
	pro_xxx_addr	= addr;
	pro_xxx_byte	= 4'hf;
	pro_xxx_wordsize	= 3;
	pro_xxx_req_r	= 1;
	@(posedge xxx_pro_ack)
		begin
		pro_xxx_req_r	= 0;
		rdbk_data0	= xxx_pro_data0;
		rdbk_data1	= xxx_pro_data1;
		rdbk_data2	= xxx_pro_data2;
		rdbk_data3	= xxx_pro_data3;
		rdbk_data4	= xxx_pro_data4;
		rdbk_data5	= xxx_pro_data5;
		rdbk_data6	= xxx_pro_data6;
		rdbk_data7	= xxx_pro_data7;
		end
end
endtask

task request_lock;
begin
	pro_req_lock_set	= 1;
end
endtask

task request_unlock;
begin
	pro_req_lock_set	= 0;
end
endtask

task request_lock_num_set;
input [7:0] max_lock_num;
begin
	MAX_LOCK_COUNT	= max_lock_num;
end
endtask

endmodule