/******************************************************************************
          (c) copyrights 1997-2000. All rights reserved

           T-Square Design, Inc.

          Company confidential and Properietary information.
          This information may not be disclosed to unauthorized
          individual.
******************************************************************************/
/*********************************************************************
//File 	: 	pro_mux.v
//Description:
//			used to select the accessing target/current processing thread
//			basing on the arbitrator's result.
//Author: 	Yuky
//Date	: 	2002-05-13	initial version
//			2002-07-25	extend to 32 channels
*********************************************************************/
module	pro_mux(
//out to transfering interface
	task_req,		// request arbitration
	task_wr_rd_,	// 1 means write, 0 means read
	task_addr,		// request address
	task_byte,		// byte enable, high level active
	task_wordsize,	// word number
	task_data0,		// first data to send
	task_data1,		// second data to send
	task_data2,		// third data to send
	task_data3,		// fourth data to send
	task_data4,		// fifth data to send
	task_data5,		// sixth data to send
	task_data6,		// seventh data to send
	task_data7,		// eighth data to send
	task_ack,		// accept the request data, high level active
	back_data0,		// first data for read
	back_data1,		// second data for read
	back_data2,		// third data for read
	back_data3,		// fourth data for read
	back_data4,		// fifth data for read
	back_data5,		// sixth data for read
	back_data6,		// seventh data for read
	back_data7,		// eighth data for read

//cpu process pro00 request
	u_pro00_req,
	u_pro00_wr_rd_,
	u_pro00_addr,
	u_pro00_byte,
	u_pro00_wordsize,
	u_pro00_data0,
	u_pro00_data1,
	u_pro00_data2,
	u_pro00_data3,
	u_pro00_data4,
	u_pro00_data5,
	u_pro00_data6,
	u_pro00_data7,
	x_pro00_ack,
	x_pro00_data0,
	x_pro00_data1,
	x_pro00_data2,
	x_pro00_data3,
	x_pro00_data4,
	x_pro00_data5,
	x_pro00_data6,
	x_pro00_data7,

//cpu process pro01 request
	u_pro01_req,
	u_pro01_wr_rd_,
	u_pro01_addr,
	u_pro01_byte,
	u_pro01_wordsize,
	u_pro01_data0,
	u_pro01_data1,
	u_pro01_data2,
	u_pro01_data3,
	u_pro01_data4,
	u_pro01_data5,
	u_pro01_data6,
	u_pro01_data7,
	x_pro01_ack,
	x_pro01_data0,
	x_pro01_data1,
	x_pro01_data2,
	x_pro01_data3,
	x_pro01_data4,
	x_pro01_data5,
	x_pro01_data6,
	x_pro01_data7,

//cpu process pro02 request
	u_pro02_req,
	u_pro02_wr_rd_,
	u_pro02_addr,
	u_pro02_byte,
	u_pro02_wordsize,
	u_pro02_data0,
	u_pro02_data1,
	u_pro02_data2,
	u_pro02_data3,
	u_pro02_data4,
	u_pro02_data5,
	u_pro02_data6,
	u_pro02_data7,
	x_pro02_ack,
	x_pro02_data0,
	x_pro02_data1,
	x_pro02_data2,
	x_pro02_data3,
	x_pro02_data4,
	x_pro02_data5,
	x_pro02_data6,
	x_pro02_data7,

//cpu process pro03 request
	u_pro03_req,
	u_pro03_wr_rd_,
	u_pro03_addr,
	u_pro03_byte,
	u_pro03_wordsize,
	u_pro03_data0,
	u_pro03_data1,
	u_pro03_data2,
	u_pro03_data3,
	u_pro03_data4,
	u_pro03_data5,
	u_pro03_data6,
	u_pro03_data7,
	x_pro03_ack,
	x_pro03_data0,
	x_pro03_data1,
	x_pro03_data2,
	x_pro03_data3,
	x_pro03_data4,
	x_pro03_data5,
	x_pro03_data6,
	x_pro03_data7,

//cpu process pro04 request
	u_pro04_req,
	u_pro04_wr_rd_,
	u_pro04_addr,
	u_pro04_byte,
	u_pro04_wordsize,
	u_pro04_data0,
	u_pro04_data1,
	u_pro04_data2,
	u_pro04_data3,
	u_pro04_data4,
	u_pro04_data5,
	u_pro04_data6,
	u_pro04_data7,
	x_pro04_ack,
	x_pro04_data0,
	x_pro04_data1,
	x_pro04_data2,
	x_pro04_data3,
	x_pro04_data4,
	x_pro04_data5,
	x_pro04_data6,
	x_pro04_data7,

//cpu process pro05 request
	u_pro05_req,
	u_pro05_wr_rd_,
	u_pro05_addr,
	u_pro05_byte,
	u_pro05_wordsize,
	u_pro05_data0,
	u_pro05_data1,
	u_pro05_data2,
	u_pro05_data3,
	u_pro05_data4,
	u_pro05_data5,
	u_pro05_data6,
	u_pro05_data7,
	x_pro05_ack,
	x_pro05_data0,
	x_pro05_data1,
	x_pro05_data2,
	x_pro05_data3,
	x_pro05_data4,
	x_pro05_data5,
	x_pro05_data6,
	x_pro05_data7,

//cpu process pro06 request
	u_pro06_req,
	u_pro06_wr_rd_,
	u_pro06_addr,
	u_pro06_byte,
	u_pro06_wordsize,
	u_pro06_data0,
	u_pro06_data1,
	u_pro06_data2,
	u_pro06_data3,
	u_pro06_data4,
	u_pro06_data5,
	u_pro06_data6,
	u_pro06_data7,
	x_pro06_ack,
	x_pro06_data0,
	x_pro06_data1,
	x_pro06_data2,
	x_pro06_data3,
	x_pro06_data4,
	x_pro06_data5,
	x_pro06_data6,
	x_pro06_data7,

//cpu process pro07 request
	u_pro07_req,
	u_pro07_wr_rd_,
	u_pro07_addr,
	u_pro07_byte,
	u_pro07_wordsize,
	u_pro07_data0,
	u_pro07_data1,
	u_pro07_data2,
	u_pro07_data3,
	u_pro07_data4,
	u_pro07_data5,
	u_pro07_data6,
	u_pro07_data7,
	x_pro07_ack,
	x_pro07_data0,
	x_pro07_data1,
	x_pro07_data2,
	x_pro07_data3,
	x_pro07_data4,
	x_pro07_data5,
	x_pro07_data6,
	x_pro07_data7,

//cpu process pro08 request
	u_pro08_req,
	u_pro08_wr_rd_,
	u_pro08_addr,
	u_pro08_byte,
	u_pro08_wordsize,
	u_pro08_data0,
	u_pro08_data1,
	u_pro08_data2,
	u_pro08_data3,
	u_pro08_data4,
	u_pro08_data5,
	u_pro08_data6,
	u_pro08_data7,
	x_pro08_ack,
	x_pro08_data0,
	x_pro08_data1,
	x_pro08_data2,
	x_pro08_data3,
	x_pro08_data4,
	x_pro08_data5,
	x_pro08_data6,
	x_pro08_data7,

//cpu process pro09 request
	u_pro09_req,
	u_pro09_wr_rd_,
	u_pro09_addr,
	u_pro09_byte,
	u_pro09_wordsize,
	u_pro09_data0,
	u_pro09_data1,
	u_pro09_data2,
	u_pro09_data3,
	u_pro09_data4,
	u_pro09_data5,
	u_pro09_data6,
	u_pro09_data7,
	x_pro09_ack,
	x_pro09_data0,
	x_pro09_data1,
	x_pro09_data2,
	x_pro09_data3,
	x_pro09_data4,
	x_pro09_data5,
	x_pro09_data6,
	x_pro09_data7,

//cpu process pro10 request
	u_pro10_req,
	u_pro10_wr_rd_,
	u_pro10_addr,
	u_pro10_byte,
	u_pro10_wordsize,
	u_pro10_data0,
	u_pro10_data1,
	u_pro10_data2,
	u_pro10_data3,
	u_pro10_data4,
	u_pro10_data5,
	u_pro10_data6,
	u_pro10_data7,
	x_pro10_ack,
	x_pro10_data0,
	x_pro10_data1,
	x_pro10_data2,
	x_pro10_data3,
	x_pro10_data4,
	x_pro10_data5,
	x_pro10_data6,
	x_pro10_data7,

//cpu process pro11 request
	u_pro11_req,
	u_pro11_wr_rd_,
	u_pro11_addr,
	u_pro11_byte,
	u_pro11_wordsize,
	u_pro11_data0,
	u_pro11_data1,
	u_pro11_data2,
	u_pro11_data3,
	u_pro11_data4,
	u_pro11_data5,
	u_pro11_data6,
	u_pro11_data7,
	x_pro11_ack,
	x_pro11_data0,
	x_pro11_data1,
	x_pro11_data2,
	x_pro11_data3,
	x_pro11_data4,
	x_pro11_data5,
	x_pro11_data6,
	x_pro11_data7,

//cpu process pro12 request
	u_pro12_req,
	u_pro12_wr_rd_,
	u_pro12_addr,
	u_pro12_byte,
	u_pro12_wordsize,
	u_pro12_data0,
	u_pro12_data1,
	u_pro12_data2,
	u_pro12_data3,
	u_pro12_data4,
	u_pro12_data5,
	u_pro12_data6,
	u_pro12_data7,
	x_pro12_ack,
	x_pro12_data0,
	x_pro12_data1,
	x_pro12_data2,
	x_pro12_data3,
	x_pro12_data4,
	x_pro12_data5,
	x_pro12_data6,
	x_pro12_data7,

//cpu process pro13 request
	u_pro13_req,
	u_pro13_wr_rd_,
	u_pro13_addr,
	u_pro13_byte,
	u_pro13_wordsize,
	u_pro13_data0,
	u_pro13_data1,
	u_pro13_data2,
	u_pro13_data3,
	u_pro13_data4,
	u_pro13_data5,
	u_pro13_data6,
	u_pro13_data7,
	x_pro13_ack,
	x_pro13_data0,
	x_pro13_data1,
	x_pro13_data2,
	x_pro13_data3,
	x_pro13_data4,
	x_pro13_data5,
	x_pro13_data6,
	x_pro13_data7,

//cpu process pro14 request
	u_pro14_req,
	u_pro14_wr_rd_,
	u_pro14_addr,
	u_pro14_byte,
	u_pro14_wordsize,
	u_pro14_data0,
	u_pro14_data1,
	u_pro14_data2,
	u_pro14_data3,
	u_pro14_data4,
	u_pro14_data5,
	u_pro14_data6,
	u_pro14_data7,
	x_pro14_ack,
	x_pro14_data0,
	x_pro14_data1,
	x_pro14_data2,
	x_pro14_data3,
	x_pro14_data4,
	x_pro14_data5,
	x_pro14_data6,
	x_pro14_data7,

//cpu process pro15 request
	u_pro15_req,
	u_pro15_wr_rd_,
	u_pro15_addr,
	u_pro15_byte,
	u_pro15_wordsize,
	u_pro15_data0,
	u_pro15_data1,
	u_pro15_data2,
	u_pro15_data3,
	u_pro15_data4,
	u_pro15_data5,
	u_pro15_data6,
	u_pro15_data7,
	x_pro15_ack,
	x_pro15_data0,
	x_pro15_data1,
	x_pro15_data2,
	x_pro15_data3,
	x_pro15_data4,
	x_pro15_data5,
	x_pro15_data6,
	x_pro15_data7,

//cpu process pro16 request
	u_pro16_req,
	u_pro16_wr_rd_,
	u_pro16_addr,
	u_pro16_byte,
	u_pro16_wordsize,
	u_pro16_data0,
	u_pro16_data1,
	u_pro16_data2,
	u_pro16_data3,
	u_pro16_data4,
	u_pro16_data5,
	u_pro16_data6,
	u_pro16_data7,
	x_pro16_ack,
	x_pro16_data0,
	x_pro16_data1,
	x_pro16_data2,
	x_pro16_data3,
	x_pro16_data4,
	x_pro16_data5,
	x_pro16_data6,
	x_pro16_data7,

//cpu process pro17 request
	u_pro17_req,
	u_pro17_wr_rd_,
	u_pro17_addr,
	u_pro17_byte,
	u_pro17_wordsize,
	u_pro17_data0,
	u_pro17_data1,
	u_pro17_data2,
	u_pro17_data3,
	u_pro17_data4,
	u_pro17_data5,
	u_pro17_data6,
	u_pro17_data7,
	x_pro17_ack,
	x_pro17_data0,
	x_pro17_data1,
	x_pro17_data2,
	x_pro17_data3,
	x_pro17_data4,
	x_pro17_data5,
	x_pro17_data6,
	x_pro17_data7,

//cpu process pro18 request
	u_pro18_req,
	u_pro18_wr_rd_,
	u_pro18_addr,
	u_pro18_byte,
	u_pro18_wordsize,
	u_pro18_data0,
	u_pro18_data1,
	u_pro18_data2,
	u_pro18_data3,
	u_pro18_data4,
	u_pro18_data5,
	u_pro18_data6,
	u_pro18_data7,
	x_pro18_ack,
	x_pro18_data0,
	x_pro18_data1,
	x_pro18_data2,
	x_pro18_data3,
	x_pro18_data4,
	x_pro18_data5,
	x_pro18_data6,
	x_pro18_data7,

//cpu process pro19 request
	u_pro19_req,
	u_pro19_wr_rd_,
	u_pro19_addr,
	u_pro19_byte,
	u_pro19_wordsize,
	u_pro19_data0,
	u_pro19_data1,
	u_pro19_data2,
	u_pro19_data3,
	u_pro19_data4,
	u_pro19_data5,
	u_pro19_data6,
	u_pro19_data7,
	x_pro19_ack,
	x_pro19_data0,
	x_pro19_data1,
	x_pro19_data2,
	x_pro19_data3,
	x_pro19_data4,
	x_pro19_data5,
	x_pro19_data6,
	x_pro19_data7,

//cpu process pro20 request
	u_pro20_req,
	u_pro20_wr_rd_,
	u_pro20_addr,
	u_pro20_byte,
	u_pro20_wordsize,
	u_pro20_data0,
	u_pro20_data1,
	u_pro20_data2,
	u_pro20_data3,
	u_pro20_data4,
	u_pro20_data5,
	u_pro20_data6,
	u_pro20_data7,
	x_pro20_ack,
	x_pro20_data0,
	x_pro20_data1,
	x_pro20_data2,
	x_pro20_data3,
	x_pro20_data4,
	x_pro20_data5,
	x_pro20_data6,
	x_pro20_data7,

//cpu process pro21 request
	u_pro21_req,
	u_pro21_wr_rd_,
	u_pro21_addr,
	u_pro21_byte,
	u_pro21_wordsize,
	u_pro21_data0,
	u_pro21_data1,
	u_pro21_data2,
	u_pro21_data3,
	u_pro21_data4,
	u_pro21_data5,
	u_pro21_data6,
	u_pro21_data7,
	x_pro21_ack,
	x_pro21_data0,
	x_pro21_data1,
	x_pro21_data2,
	x_pro21_data3,
	x_pro21_data4,
	x_pro21_data5,
	x_pro21_data6,
	x_pro21_data7,

//cpu process pro22 request
	u_pro22_req,
	u_pro22_wr_rd_,
	u_pro22_addr,
	u_pro22_byte,
	u_pro22_wordsize,
	u_pro22_data0,
	u_pro22_data1,
	u_pro22_data2,
	u_pro22_data3,
	u_pro22_data4,
	u_pro22_data5,
	u_pro22_data6,
	u_pro22_data7,
	x_pro22_ack,
	x_pro22_data0,
	x_pro22_data1,
	x_pro22_data2,
	x_pro22_data3,
	x_pro22_data4,
	x_pro22_data5,
	x_pro22_data6,
	x_pro22_data7,

//cpu process pro23 request
	u_pro23_req,
	u_pro23_wr_rd_,
	u_pro23_addr,
	u_pro23_byte,
	u_pro23_wordsize,
	u_pro23_data0,
	u_pro23_data1,
	u_pro23_data2,
	u_pro23_data3,
	u_pro23_data4,
	u_pro23_data5,
	u_pro23_data6,
	u_pro23_data7,
	x_pro23_ack,
	x_pro23_data0,
	x_pro23_data1,
	x_pro23_data2,
	x_pro23_data3,
	x_pro23_data4,
	x_pro23_data5,
	x_pro23_data6,
	x_pro23_data7,

//cpu process pro24 request
	u_pro24_req,
	u_pro24_wr_rd_,
	u_pro24_addr,
	u_pro24_byte,
	u_pro24_wordsize,
	u_pro24_data0,
	u_pro24_data1,
	u_pro24_data2,
	u_pro24_data3,
	u_pro24_data4,
	u_pro24_data5,
	u_pro24_data6,
	u_pro24_data7,
	x_pro24_ack,
	x_pro24_data0,
	x_pro24_data1,
	x_pro24_data2,
	x_pro24_data3,
	x_pro24_data4,
	x_pro24_data5,
	x_pro24_data6,
	x_pro24_data7,

//cpu process pro25 request
	u_pro25_req,
	u_pro25_wr_rd_,
	u_pro25_addr,
	u_pro25_byte,
	u_pro25_wordsize,
	u_pro25_data0,
	u_pro25_data1,
	u_pro25_data2,
	u_pro25_data3,
	u_pro25_data4,
	u_pro25_data5,
	u_pro25_data6,
	u_pro25_data7,
	x_pro25_ack,
	x_pro25_data0,
	x_pro25_data1,
	x_pro25_data2,
	x_pro25_data3,
	x_pro25_data4,
	x_pro25_data5,
	x_pro25_data6,
	x_pro25_data7,

//cpu process pro26 request
	u_pro26_req,
	u_pro26_wr_rd_,
	u_pro26_addr,
	u_pro26_byte,
	u_pro26_wordsize,
	u_pro26_data0,
	u_pro26_data1,
	u_pro26_data2,
	u_pro26_data3,
	u_pro26_data4,
	u_pro26_data5,
	u_pro26_data6,
	u_pro26_data7,
	x_pro26_ack,
	x_pro26_data0,
	x_pro26_data1,
	x_pro26_data2,
	x_pro26_data3,
	x_pro26_data4,
	x_pro26_data5,
	x_pro26_data6,
	x_pro26_data7,

//cpu process pro27 request
	u_pro27_req,
	u_pro27_wr_rd_,
	u_pro27_addr,
	u_pro27_byte,
	u_pro27_wordsize,
	u_pro27_data0,
	u_pro27_data1,
	u_pro27_data2,
	u_pro27_data3,
	u_pro27_data4,
	u_pro27_data5,
	u_pro27_data6,
	u_pro27_data7,
	x_pro27_ack,
	x_pro27_data0,
	x_pro27_data1,
	x_pro27_data2,
	x_pro27_data3,
	x_pro27_data4,
	x_pro27_data5,
	x_pro27_data6,
	x_pro27_data7,

//cpu process pro28 request
	u_pro28_req,
	u_pro28_wr_rd_,
	u_pro28_addr,
	u_pro28_byte,
	u_pro28_wordsize,
	u_pro28_data0,
	u_pro28_data1,
	u_pro28_data2,
	u_pro28_data3,
	u_pro28_data4,
	u_pro28_data5,
	u_pro28_data6,
	u_pro28_data7,
	x_pro28_ack,
	x_pro28_data0,
	x_pro28_data1,
	x_pro28_data2,
	x_pro28_data3,
	x_pro28_data4,
	x_pro28_data5,
	x_pro28_data6,
	x_pro28_data7,

//cpu process pro29 request
	u_pro29_req,
	u_pro29_wr_rd_,
	u_pro29_addr,
	u_pro29_byte,
	u_pro29_wordsize,
	u_pro29_data0,
	u_pro29_data1,
	u_pro29_data2,
	u_pro29_data3,
	u_pro29_data4,
	u_pro29_data5,
	u_pro29_data6,
	u_pro29_data7,
	x_pro29_ack,
	x_pro29_data0,
	x_pro29_data1,
	x_pro29_data2,
	x_pro29_data3,
	x_pro29_data4,
	x_pro29_data5,
	x_pro29_data6,
	x_pro29_data7,

//cpu process pro30 request
	u_pro30_req,
	u_pro30_wr_rd_,
	u_pro30_addr,
	u_pro30_byte,
	u_pro30_wordsize,
	u_pro30_data0,
	u_pro30_data1,
	u_pro30_data2,
	u_pro30_data3,
	u_pro30_data4,
	u_pro30_data5,
	u_pro30_data6,
	u_pro30_data7,
	x_pro30_ack,
	x_pro30_data0,
	x_pro30_data1,
	x_pro30_data2,
	x_pro30_data3,
	x_pro30_data4,
	x_pro30_data5,
	x_pro30_data6,
	x_pro30_data7,

//cpu process pro31 request
	u_pro31_req,
	u_pro31_wr_rd_,
	u_pro31_addr,
	u_pro31_byte,
	u_pro31_wordsize,
	u_pro31_data0,
	u_pro31_data1,
	u_pro31_data2,
	u_pro31_data3,
	u_pro31_data4,
	u_pro31_data5,
	u_pro31_data6,
	u_pro31_data7,
	x_pro31_ack,
	x_pro31_data0,
	x_pro31_data1,
	x_pro31_data2,
	x_pro31_data3,
	x_pro31_data4,
	x_pro31_data5,
	x_pro31_data6,
	x_pro31_data7,

//cpu process interrupt request
	u_int_req,
	u_int_wr_rd_,
	u_int_addr,
	u_int_byte,
	u_int_wordsize,
	u_int_data0,
	u_int_data1,
	u_int_data2,
	u_int_data3,
	u_int_data4,
	u_int_data5,
	u_int_data6,
	u_int_data7,
	x_int_ack,
	x_int_data0,
	x_int_data1,
	x_int_data2,
	x_int_data3,
	x_int_data4,
	x_int_data5,
	x_int_data6,
	x_int_data7,

//select signal from arbitrator
	u_pro31_sel	,
	u_pro30_sel	,
	u_pro29_sel	,
	u_pro28_sel	,
	u_pro27_sel	,
	u_pro26_sel	,
	u_pro25_sel	,
 	u_pro24_sel	,
 	u_pro23_sel	,
 	u_pro22_sel	,
 	u_pro21_sel	,
	u_pro20_sel	,
	u_pro19_sel	,
	u_pro18_sel	,
	u_pro17_sel	,
	u_pro16_sel	,
	u_pro15_sel	,
	u_pro14_sel	,
	u_pro13_sel	,
	u_pro12_sel	,
	u_pro11_sel	,
	u_pro10_sel	,
	u_pro09_sel	,
 	u_pro08_sel	,
 	u_pro07_sel	,
 	u_pro06_sel	,
 	u_pro05_sel	,
	u_pro04_sel	,
	u_pro03_sel	,
	u_pro02_sel	,
	u_pro01_sel	,
	u_pro00_sel	,
	u_int_sel	,

	g_sclk,
	g_rst_
	);
parameter udly = 0.5 ;
output			task_req;
output			task_wr_rd_;
output	[31:0]	task_addr;
output	[3:0]	task_byte;
output	[1:0]	task_wordsize;
output	[31:0]	task_data0;
output	[31:0]	task_data1;
output	[31:0]	task_data2;
output	[31:0]	task_data3;
output	[31:0]	task_data4;
output	[31:0]	task_data5;
output	[31:0]	task_data6;
output	[31:0]	task_data7;
input			task_ack;
input	[31:0]	back_data0;
input	[31:0]	back_data1;
input	[31:0]	back_data2;
input	[31:0]	back_data3;
input	[31:0]	back_data4;
input	[31:0]	back_data5;
input	[31:0]	back_data6;
input	[31:0]	back_data7;

input			u_pro00_req;
input			u_pro00_wr_rd_;
input	[31:0]	u_pro00_addr;
input	[3:0]	u_pro00_byte;
input	[1:0]	u_pro00_wordsize;
input	[31:0]	u_pro00_data0;
input	[31:0]	u_pro00_data1;
input	[31:0]	u_pro00_data2;
input	[31:0]	u_pro00_data3;
input	[31:0]	u_pro00_data4;
input	[31:0]	u_pro00_data5;
input	[31:0]	u_pro00_data6;
input	[31:0]	u_pro00_data7;
output			x_pro00_ack;
output	[31:0]	x_pro00_data0;
output	[31:0]	x_pro00_data1;
output	[31:0]	x_pro00_data2;
output	[31:0]	x_pro00_data3;
output	[31:0]	x_pro00_data4;
output	[31:0]	x_pro00_data5;
output	[31:0]	x_pro00_data6;
output	[31:0]	x_pro00_data7;

input			u_pro01_req;
input			u_pro01_wr_rd_;
input	[31:0]	u_pro01_addr;
input	[3:0]	u_pro01_byte;
input	[1:0]	u_pro01_wordsize;
input	[31:0]	u_pro01_data0;
input	[31:0]	u_pro01_data1;
input	[31:0]	u_pro01_data2;
input	[31:0]	u_pro01_data3;
input	[31:0]	u_pro01_data4;
input	[31:0]	u_pro01_data5;
input	[31:0]	u_pro01_data6;
input	[31:0]	u_pro01_data7;
output			x_pro01_ack;
output	[31:0]	x_pro01_data0;
output	[31:0]	x_pro01_data1;
output	[31:0]	x_pro01_data2;
output	[31:0]	x_pro01_data3;
output	[31:0]	x_pro01_data4;
output	[31:0]	x_pro01_data5;
output	[31:0]	x_pro01_data6;
output	[31:0]	x_pro01_data7;

input			u_pro02_req;
input			u_pro02_wr_rd_;
input	[31:0]	u_pro02_addr;
input	[3:0]	u_pro02_byte;
input	[1:0]	u_pro02_wordsize;
input	[31:0]	u_pro02_data0;
input	[31:0]	u_pro02_data1;
input	[31:0]	u_pro02_data2;
input	[31:0]	u_pro02_data3;
input	[31:0]	u_pro02_data4;
input	[31:0]	u_pro02_data5;
input	[31:0]	u_pro02_data6;
input	[31:0]	u_pro02_data7;
output			x_pro02_ack;
output	[31:0]	x_pro02_data0;
output	[31:0]	x_pro02_data1;
output	[31:0]	x_pro02_data2;
output	[31:0]	x_pro02_data3;
output	[31:0]	x_pro02_data4;
output	[31:0]	x_pro02_data5;
output	[31:0]	x_pro02_data6;
output	[31:0]	x_pro02_data7;

input			u_pro03_req;
input			u_pro03_wr_rd_;
input	[31:0]	u_pro03_addr;
input	[3:0]	u_pro03_byte;
input	[1:0]	u_pro03_wordsize;
input	[31:0]	u_pro03_data0;
input	[31:0]	u_pro03_data1;
input	[31:0]	u_pro03_data2;
input	[31:0]	u_pro03_data3;
input	[31:0]	u_pro03_data4;
input	[31:0]	u_pro03_data5;
input	[31:0]	u_pro03_data6;
input	[31:0]	u_pro03_data7;
output			x_pro03_ack;
output	[31:0]	x_pro03_data0;
output	[31:0]	x_pro03_data1;
output	[31:0]	x_pro03_data2;
output	[31:0]	x_pro03_data3;
output	[31:0]	x_pro03_data4;
output	[31:0]	x_pro03_data5;
output	[31:0]	x_pro03_data6;
output	[31:0]	x_pro03_data7;

input			u_pro04_req;
input			u_pro04_wr_rd_;
input	[31:0]	u_pro04_addr;
input	[3:0]	u_pro04_byte;
input	[1:0]	u_pro04_wordsize;
input	[31:0]	u_pro04_data0;
input	[31:0]	u_pro04_data1;
input	[31:0]	u_pro04_data2;
input	[31:0]	u_pro04_data3;
input	[31:0]	u_pro04_data4;
input	[31:0]	u_pro04_data5;
input	[31:0]	u_pro04_data6;
input	[31:0]	u_pro04_data7;
output			x_pro04_ack;
output	[31:0]	x_pro04_data0;
output	[31:0]	x_pro04_data1;
output	[31:0]	x_pro04_data2;
output	[31:0]	x_pro04_data3;
output	[31:0]	x_pro04_data4;
output	[31:0]	x_pro04_data5;
output	[31:0]	x_pro04_data6;
output	[31:0]	x_pro04_data7;

input			u_pro05_req;
input			u_pro05_wr_rd_;
input	[31:0]	u_pro05_addr;
input	[3:0]	u_pro05_byte;
input	[1:0]	u_pro05_wordsize;
input	[31:0]	u_pro05_data0;
input	[31:0]	u_pro05_data1;
input	[31:0]	u_pro05_data2;
input	[31:0]	u_pro05_data3;
input	[31:0]	u_pro05_data4;
input	[31:0]	u_pro05_data5;
input	[31:0]	u_pro05_data6;
input	[31:0]	u_pro05_data7;
output			x_pro05_ack;
output	[31:0]	x_pro05_data0;
output	[31:0]	x_pro05_data1;
output	[31:0]	x_pro05_data2;
output	[31:0]	x_pro05_data3;
output	[31:0]	x_pro05_data4;
output	[31:0]	x_pro05_data5;
output	[31:0]	x_pro05_data6;
output	[31:0]	x_pro05_data7;

input			u_pro06_req;
input			u_pro06_wr_rd_;
input	[31:0]	u_pro06_addr;
input	[3:0]	u_pro06_byte;
input	[1:0]	u_pro06_wordsize;
input	[31:0]	u_pro06_data0;
input	[31:0]	u_pro06_data1;
input	[31:0]	u_pro06_data2;
input	[31:0]	u_pro06_data3;
input	[31:0]	u_pro06_data4;
input	[31:0]	u_pro06_data5;
input	[31:0]	u_pro06_data6;
input	[31:0]	u_pro06_data7;
output			x_pro06_ack;
output	[31:0]	x_pro06_data0;
output	[31:0]	x_pro06_data1;
output	[31:0]	x_pro06_data2;
output	[31:0]	x_pro06_data3;
output	[31:0]	x_pro06_data4;
output	[31:0]	x_pro06_data5;
output	[31:0]	x_pro06_data6;
output	[31:0]	x_pro06_data7;

input			u_pro07_req;
input			u_pro07_wr_rd_;
input	[31:0]	u_pro07_addr;
input	[3:0]	u_pro07_byte;
input	[1:0]	u_pro07_wordsize;
input	[31:0]	u_pro07_data0;
input	[31:0]	u_pro07_data1;
input	[31:0]	u_pro07_data2;
input	[31:0]	u_pro07_data3;
input	[31:0]	u_pro07_data4;
input	[31:0]	u_pro07_data5;
input	[31:0]	u_pro07_data6;
input	[31:0]	u_pro07_data7;
output			x_pro07_ack;
output	[31:0]	x_pro07_data0;
output	[31:0]	x_pro07_data1;
output	[31:0]	x_pro07_data2;
output	[31:0]	x_pro07_data3;
output	[31:0]	x_pro07_data4;
output	[31:0]	x_pro07_data5;
output	[31:0]	x_pro07_data6;
output	[31:0]	x_pro07_data7;

input			u_pro08_req;
input			u_pro08_wr_rd_;
input	[31:0]	u_pro08_addr;
input	[3:0]	u_pro08_byte;
input	[1:0]	u_pro08_wordsize;
input	[31:0]	u_pro08_data0;
input	[31:0]	u_pro08_data1;
input	[31:0]	u_pro08_data2;
input	[31:0]	u_pro08_data3;
input	[31:0]	u_pro08_data4;
input	[31:0]	u_pro08_data5;
input	[31:0]	u_pro08_data6;
input	[31:0]	u_pro08_data7;
output			x_pro08_ack;
output	[31:0]	x_pro08_data0;
output	[31:0]	x_pro08_data1;
output	[31:0]	x_pro08_data2;
output	[31:0]	x_pro08_data3;
output	[31:0]	x_pro08_data4;
output	[31:0]	x_pro08_data5;
output	[31:0]	x_pro08_data6;
output	[31:0]	x_pro08_data7;

input			u_pro09_req;
input			u_pro09_wr_rd_;
input	[31:0]	u_pro09_addr;
input	[3:0]	u_pro09_byte;
input	[1:0]	u_pro09_wordsize;
input	[31:0]	u_pro09_data0;
input	[31:0]	u_pro09_data1;
input	[31:0]	u_pro09_data2;
input	[31:0]	u_pro09_data3;
input	[31:0]	u_pro09_data4;
input	[31:0]	u_pro09_data5;
input	[31:0]	u_pro09_data6;
input	[31:0]	u_pro09_data7;
output			x_pro09_ack;
output	[31:0]	x_pro09_data0;
output	[31:0]	x_pro09_data1;
output	[31:0]	x_pro09_data2;
output	[31:0]	x_pro09_data3;
output	[31:0]	x_pro09_data4;
output	[31:0]	x_pro09_data5;
output	[31:0]	x_pro09_data6;
output	[31:0]	x_pro09_data7;

input			u_pro10_req;
input			u_pro10_wr_rd_;
input	[31:0]	u_pro10_addr;
input	[3:0]	u_pro10_byte;
input	[1:0]	u_pro10_wordsize;
input	[31:0]	u_pro10_data0;
input	[31:0]	u_pro10_data1;
input	[31:0]	u_pro10_data2;
input	[31:0]	u_pro10_data3;
input	[31:0]	u_pro10_data4;
input	[31:0]	u_pro10_data5;
input	[31:0]	u_pro10_data6;
input	[31:0]	u_pro10_data7;
output			x_pro10_ack;
output	[31:0]	x_pro10_data0;
output	[31:0]	x_pro10_data1;
output	[31:0]	x_pro10_data2;
output	[31:0]	x_pro10_data3;
output	[31:0]	x_pro10_data4;
output	[31:0]	x_pro10_data5;
output	[31:0]	x_pro10_data6;
output	[31:0]	x_pro10_data7;

input			u_pro11_req;
input			u_pro11_wr_rd_;
input	[31:0]	u_pro11_addr;
input	[3:0]	u_pro11_byte;
input	[1:0]	u_pro11_wordsize;
input	[31:0]	u_pro11_data0;
input	[31:0]	u_pro11_data1;
input	[31:0]	u_pro11_data2;
input	[31:0]	u_pro11_data3;
input	[31:0]	u_pro11_data4;
input	[31:0]	u_pro11_data5;
input	[31:0]	u_pro11_data6;
input	[31:0]	u_pro11_data7;
output			x_pro11_ack;
output	[31:0]	x_pro11_data0;
output	[31:0]	x_pro11_data1;
output	[31:0]	x_pro11_data2;
output	[31:0]	x_pro11_data3;
output	[31:0]	x_pro11_data4;
output	[31:0]	x_pro11_data5;
output	[31:0]	x_pro11_data6;
output	[31:0]	x_pro11_data7;

input			u_pro12_req;
input			u_pro12_wr_rd_;
input	[31:0]	u_pro12_addr;
input	[3:0]	u_pro12_byte;
input	[1:0]	u_pro12_wordsize;
input	[31:0]	u_pro12_data0;
input	[31:0]	u_pro12_data1;
input	[31:0]	u_pro12_data2;
input	[31:0]	u_pro12_data3;
input	[31:0]	u_pro12_data4;
input	[31:0]	u_pro12_data5;
input	[31:0]	u_pro12_data6;
input	[31:0]	u_pro12_data7;
output			x_pro12_ack;
output	[31:0]	x_pro12_data0;
output	[31:0]	x_pro12_data1;
output	[31:0]	x_pro12_data2;
output	[31:0]	x_pro12_data3;
output	[31:0]	x_pro12_data4;
output	[31:0]	x_pro12_data5;
output	[31:0]	x_pro12_data6;
output	[31:0]	x_pro12_data7;

input			u_pro13_req;
input			u_pro13_wr_rd_;
input	[31:0]	u_pro13_addr;
input	[3:0]	u_pro13_byte;
input	[1:0]	u_pro13_wordsize;
input	[31:0]	u_pro13_data0;
input	[31:0]	u_pro13_data1;
input	[31:0]	u_pro13_data2;
input	[31:0]	u_pro13_data3;
input	[31:0]	u_pro13_data4;
input	[31:0]	u_pro13_data5;
input	[31:0]	u_pro13_data6;
input	[31:0]	u_pro13_data7;
output			x_pro13_ack;
output	[31:0]	x_pro13_data0;
output	[31:0]	x_pro13_data1;
output	[31:0]	x_pro13_data2;
output	[31:0]	x_pro13_data3;
output	[31:0]	x_pro13_data4;
output	[31:0]	x_pro13_data5;
output	[31:0]	x_pro13_data6;
output	[31:0]	x_pro13_data7;

input			u_pro14_req;
input			u_pro14_wr_rd_;
input	[31:0]	u_pro14_addr;
input	[3:0]	u_pro14_byte;
input	[1:0]	u_pro14_wordsize;
input	[31:0]	u_pro14_data0;
input	[31:0]	u_pro14_data1;
input	[31:0]	u_pro14_data2;
input	[31:0]	u_pro14_data3;
input	[31:0]	u_pro14_data4;
input	[31:0]	u_pro14_data5;
input	[31:0]	u_pro14_data6;
input	[31:0]	u_pro14_data7;
output			x_pro14_ack;
output	[31:0]	x_pro14_data0;
output	[31:0]	x_pro14_data1;
output	[31:0]	x_pro14_data2;
output	[31:0]	x_pro14_data3;
output	[31:0]	x_pro14_data4;
output	[31:0]	x_pro14_data5;
output	[31:0]	x_pro14_data6;
output	[31:0]	x_pro14_data7;

input			u_pro15_req;
input			u_pro15_wr_rd_;
input	[31:0]	u_pro15_addr;
input	[3:0]	u_pro15_byte;
input	[1:0]	u_pro15_wordsize;
input	[31:0]	u_pro15_data0;
input	[31:0]	u_pro15_data1;
input	[31:0]	u_pro15_data2;
input	[31:0]	u_pro15_data3;
input	[31:0]	u_pro15_data4;
input	[31:0]	u_pro15_data5;
input	[31:0]	u_pro15_data6;
input	[31:0]	u_pro15_data7;
output			x_pro15_ack;
output	[31:0]	x_pro15_data0;
output	[31:0]	x_pro15_data1;
output	[31:0]	x_pro15_data2;
output	[31:0]	x_pro15_data3;
output	[31:0]	x_pro15_data4;
output	[31:0]	x_pro15_data5;
output	[31:0]	x_pro15_data6;
output	[31:0]	x_pro15_data7;

input			u_pro16_req;
input			u_pro16_wr_rd_;
input	[31:0]	u_pro16_addr;
input	[3:0]	u_pro16_byte;
input	[1:0]	u_pro16_wordsize;
input	[31:0]	u_pro16_data0;
input	[31:0]	u_pro16_data1;
input	[31:0]	u_pro16_data2;
input	[31:0]	u_pro16_data3;
input	[31:0]	u_pro16_data4;
input	[31:0]	u_pro16_data5;
input	[31:0]	u_pro16_data6;
input	[31:0]	u_pro16_data7;
output			x_pro16_ack;
output	[31:0]	x_pro16_data0;
output	[31:0]	x_pro16_data1;
output	[31:0]	x_pro16_data2;
output	[31:0]	x_pro16_data3;
output	[31:0]	x_pro16_data4;
output	[31:0]	x_pro16_data5;
output	[31:0]	x_pro16_data6;
output	[31:0]	x_pro16_data7;

input			u_pro17_req;
input			u_pro17_wr_rd_;
input	[31:0]	u_pro17_addr;
input	[3:0]	u_pro17_byte;
input	[1:0]	u_pro17_wordsize;
input	[31:0]	u_pro17_data0;
input	[31:0]	u_pro17_data1;
input	[31:0]	u_pro17_data2;
input	[31:0]	u_pro17_data3;
input	[31:0]	u_pro17_data4;
input	[31:0]	u_pro17_data5;
input	[31:0]	u_pro17_data6;
input	[31:0]	u_pro17_data7;
output			x_pro17_ack;
output	[31:0]	x_pro17_data0;
output	[31:0]	x_pro17_data1;
output	[31:0]	x_pro17_data2;
output	[31:0]	x_pro17_data3;
output	[31:0]	x_pro17_data4;
output	[31:0]	x_pro17_data5;
output	[31:0]	x_pro17_data6;
output	[31:0]	x_pro17_data7;

input			u_pro18_req;
input			u_pro18_wr_rd_;
input	[31:0]	u_pro18_addr;
input	[3:0]	u_pro18_byte;
input	[1:0]	u_pro18_wordsize;
input	[31:0]	u_pro18_data0;
input	[31:0]	u_pro18_data1;
input	[31:0]	u_pro18_data2;
input	[31:0]	u_pro18_data3;
input	[31:0]	u_pro18_data4;
input	[31:0]	u_pro18_data5;
input	[31:0]	u_pro18_data6;
input	[31:0]	u_pro18_data7;
output			x_pro18_ack;
output	[31:0]	x_pro18_data0;
output	[31:0]	x_pro18_data1;
output	[31:0]	x_pro18_data2;
output	[31:0]	x_pro18_data3;
output	[31:0]	x_pro18_data4;
output	[31:0]	x_pro18_data5;
output	[31:0]	x_pro18_data6;
output	[31:0]	x_pro18_data7;

input			u_pro19_req;
input			u_pro19_wr_rd_;
input	[31:0]	u_pro19_addr;
input	[3:0]	u_pro19_byte;
input	[1:0]	u_pro19_wordsize;
input	[31:0]	u_pro19_data0;
input	[31:0]	u_pro19_data1;
input	[31:0]	u_pro19_data2;
input	[31:0]	u_pro19_data3;
input	[31:0]	u_pro19_data4;
input	[31:0]	u_pro19_data5;
input	[31:0]	u_pro19_data6;
input	[31:0]	u_pro19_data7;
output			x_pro19_ack;
output	[31:0]	x_pro19_data0;
output	[31:0]	x_pro19_data1;
output	[31:0]	x_pro19_data2;
output	[31:0]	x_pro19_data3;
output	[31:0]	x_pro19_data4;
output	[31:0]	x_pro19_data5;
output	[31:0]	x_pro19_data6;
output	[31:0]	x_pro19_data7;

input			u_pro20_req;
input			u_pro20_wr_rd_;
input	[31:0]	u_pro20_addr;
input	[3:0]	u_pro20_byte;
input	[1:0]	u_pro20_wordsize;
input	[31:0]	u_pro20_data0;
input	[31:0]	u_pro20_data1;
input	[31:0]	u_pro20_data2;
input	[31:0]	u_pro20_data3;
input	[31:0]	u_pro20_data4;
input	[31:0]	u_pro20_data5;
input	[31:0]	u_pro20_data6;
input	[31:0]	u_pro20_data7;
output			x_pro20_ack;
output	[31:0]	x_pro20_data0;
output	[31:0]	x_pro20_data1;
output	[31:0]	x_pro20_data2;
output	[31:0]	x_pro20_data3;
output	[31:0]	x_pro20_data4;
output	[31:0]	x_pro20_data5;
output	[31:0]	x_pro20_data6;
output	[31:0]	x_pro20_data7;

input			u_pro21_req;
input			u_pro21_wr_rd_;
input	[31:0]	u_pro21_addr;
input	[3:0]	u_pro21_byte;
input	[1:0]	u_pro21_wordsize;
input	[31:0]	u_pro21_data0;
input	[31:0]	u_pro21_data1;
input	[31:0]	u_pro21_data2;
input	[31:0]	u_pro21_data3;
input	[31:0]	u_pro21_data4;
input	[31:0]	u_pro21_data5;
input	[31:0]	u_pro21_data6;
input	[31:0]	u_pro21_data7;
output			x_pro21_ack;
output	[31:0]	x_pro21_data0;
output	[31:0]	x_pro21_data1;
output	[31:0]	x_pro21_data2;
output	[31:0]	x_pro21_data3;
output	[31:0]	x_pro21_data4;
output	[31:0]	x_pro21_data5;
output	[31:0]	x_pro21_data6;
output	[31:0]	x_pro21_data7;

input			u_pro22_req;
input			u_pro22_wr_rd_;
input	[31:0]	u_pro22_addr;
input	[3:0]	u_pro22_byte;
input	[1:0]	u_pro22_wordsize;
input	[31:0]	u_pro22_data0;
input	[31:0]	u_pro22_data1;
input	[31:0]	u_pro22_data2;
input	[31:0]	u_pro22_data3;
input	[31:0]	u_pro22_data4;
input	[31:0]	u_pro22_data5;
input	[31:0]	u_pro22_data6;
input	[31:0]	u_pro22_data7;
output			x_pro22_ack;
output	[31:0]	x_pro22_data0;
output	[31:0]	x_pro22_data1;
output	[31:0]	x_pro22_data2;
output	[31:0]	x_pro22_data3;
output	[31:0]	x_pro22_data4;
output	[31:0]	x_pro22_data5;
output	[31:0]	x_pro22_data6;
output	[31:0]	x_pro22_data7;

input			u_pro23_req;
input			u_pro23_wr_rd_;
input	[31:0]	u_pro23_addr;
input	[3:0]	u_pro23_byte;
input	[1:0]	u_pro23_wordsize;
input	[31:0]	u_pro23_data0;
input	[31:0]	u_pro23_data1;
input	[31:0]	u_pro23_data2;
input	[31:0]	u_pro23_data3;
input	[31:0]	u_pro23_data4;
input	[31:0]	u_pro23_data5;
input	[31:0]	u_pro23_data6;
input	[31:0]	u_pro23_data7;
output			x_pro23_ack;
output	[31:0]	x_pro23_data0;
output	[31:0]	x_pro23_data1;
output	[31:0]	x_pro23_data2;
output	[31:0]	x_pro23_data3;
output	[31:0]	x_pro23_data4;
output	[31:0]	x_pro23_data5;
output	[31:0]	x_pro23_data6;
output	[31:0]	x_pro23_data7;

input			u_pro24_req;
input			u_pro24_wr_rd_;
input	[31:0]	u_pro24_addr;
input	[3:0]	u_pro24_byte;
input	[1:0]	u_pro24_wordsize;
input	[31:0]	u_pro24_data0;
input	[31:0]	u_pro24_data1;
input	[31:0]	u_pro24_data2;
input	[31:0]	u_pro24_data3;
input	[31:0]	u_pro24_data4;
input	[31:0]	u_pro24_data5;
input	[31:0]	u_pro24_data6;
input	[31:0]	u_pro24_data7;
output			x_pro24_ack;
output	[31:0]	x_pro24_data0;
output	[31:0]	x_pro24_data1;
output	[31:0]	x_pro24_data2;
output	[31:0]	x_pro24_data3;
output	[31:0]	x_pro24_data4;
output	[31:0]	x_pro24_data5;
output	[31:0]	x_pro24_data6;
output	[31:0]	x_pro24_data7;

input			u_pro25_req;
input			u_pro25_wr_rd_;
input	[31:0]	u_pro25_addr;
input	[3:0]	u_pro25_byte;
input	[1:0]	u_pro25_wordsize;
input	[31:0]	u_pro25_data0;
input	[31:0]	u_pro25_data1;
input	[31:0]	u_pro25_data2;
input	[31:0]	u_pro25_data3;
input	[31:0]	u_pro25_data4;
input	[31:0]	u_pro25_data5;
input	[31:0]	u_pro25_data6;
input	[31:0]	u_pro25_data7;
output			x_pro25_ack;
output	[31:0]	x_pro25_data0;
output	[31:0]	x_pro25_data1;
output	[31:0]	x_pro25_data2;
output	[31:0]	x_pro25_data3;
output	[31:0]	x_pro25_data4;
output	[31:0]	x_pro25_data5;
output	[31:0]	x_pro25_data6;
output	[31:0]	x_pro25_data7;

input			u_pro26_req;
input			u_pro26_wr_rd_;
input	[31:0]	u_pro26_addr;
input	[3:0]	u_pro26_byte;
input	[1:0]	u_pro26_wordsize;
input	[31:0]	u_pro26_data0;
input	[31:0]	u_pro26_data1;
input	[31:0]	u_pro26_data2;
input	[31:0]	u_pro26_data3;
input	[31:0]	u_pro26_data4;
input	[31:0]	u_pro26_data5;
input	[31:0]	u_pro26_data6;
input	[31:0]	u_pro26_data7;
output			x_pro26_ack;
output	[31:0]	x_pro26_data0;
output	[31:0]	x_pro26_data1;
output	[31:0]	x_pro26_data2;
output	[31:0]	x_pro26_data3;
output	[31:0]	x_pro26_data4;
output	[31:0]	x_pro26_data5;
output	[31:0]	x_pro26_data6;
output	[31:0]	x_pro26_data7;

input			u_pro27_req;
input			u_pro27_wr_rd_;
input	[31:0]	u_pro27_addr;
input	[3:0]	u_pro27_byte;
input	[1:0]	u_pro27_wordsize;
input	[31:0]	u_pro27_data0;
input	[31:0]	u_pro27_data1;
input	[31:0]	u_pro27_data2;
input	[31:0]	u_pro27_data3;
input	[31:0]	u_pro27_data4;
input	[31:0]	u_pro27_data5;
input	[31:0]	u_pro27_data6;
input	[31:0]	u_pro27_data7;
output			x_pro27_ack;
output	[31:0]	x_pro27_data0;
output	[31:0]	x_pro27_data1;
output	[31:0]	x_pro27_data2;
output	[31:0]	x_pro27_data3;
output	[31:0]	x_pro27_data4;
output	[31:0]	x_pro27_data5;
output	[31:0]	x_pro27_data6;
output	[31:0]	x_pro27_data7;

input			u_pro28_req;
input			u_pro28_wr_rd_;
input	[31:0]	u_pro28_addr;
input	[3:0]	u_pro28_byte;
input	[1:0]	u_pro28_wordsize;
input	[31:0]	u_pro28_data0;
input	[31:0]	u_pro28_data1;
input	[31:0]	u_pro28_data2;
input	[31:0]	u_pro28_data3;
input	[31:0]	u_pro28_data4;
input	[31:0]	u_pro28_data5;
input	[31:0]	u_pro28_data6;
input	[31:0]	u_pro28_data7;
output			x_pro28_ack;
output	[31:0]	x_pro28_data0;
output	[31:0]	x_pro28_data1;
output	[31:0]	x_pro28_data2;
output	[31:0]	x_pro28_data3;
output	[31:0]	x_pro28_data4;
output	[31:0]	x_pro28_data5;
output	[31:0]	x_pro28_data6;
output	[31:0]	x_pro28_data7;

input			u_pro29_req;
input			u_pro29_wr_rd_;
input	[31:0]	u_pro29_addr;
input	[3:0]	u_pro29_byte;
input	[1:0]	u_pro29_wordsize;
input	[31:0]	u_pro29_data0;
input	[31:0]	u_pro29_data1;
input	[31:0]	u_pro29_data2;
input	[31:0]	u_pro29_data3;
input	[31:0]	u_pro29_data4;
input	[31:0]	u_pro29_data5;
input	[31:0]	u_pro29_data6;
input	[31:0]	u_pro29_data7;
output			x_pro29_ack;
output	[31:0]	x_pro29_data0;
output	[31:0]	x_pro29_data1;
output	[31:0]	x_pro29_data2;
output	[31:0]	x_pro29_data3;
output	[31:0]	x_pro29_data4;
output	[31:0]	x_pro29_data5;
output	[31:0]	x_pro29_data6;
output	[31:0]	x_pro29_data7;

input			u_pro30_req;
input			u_pro30_wr_rd_;
input	[31:0]	u_pro30_addr;
input	[3:0]	u_pro30_byte;
input	[1:0]	u_pro30_wordsize;
input	[31:0]	u_pro30_data0;
input	[31:0]	u_pro30_data1;
input	[31:0]	u_pro30_data2;
input	[31:0]	u_pro30_data3;
input	[31:0]	u_pro30_data4;
input	[31:0]	u_pro30_data5;
input	[31:0]	u_pro30_data6;
input	[31:0]	u_pro30_data7;
output			x_pro30_ack;
output	[31:0]	x_pro30_data0;
output	[31:0]	x_pro30_data1;
output	[31:0]	x_pro30_data2;
output	[31:0]	x_pro30_data3;
output	[31:0]	x_pro30_data4;
output	[31:0]	x_pro30_data5;
output	[31:0]	x_pro30_data6;
output	[31:0]	x_pro30_data7;

input			u_pro31_req;
input			u_pro31_wr_rd_;
input	[31:0]	u_pro31_addr;
input	[3:0]	u_pro31_byte;
input	[1:0]	u_pro31_wordsize;
input	[31:0]	u_pro31_data0;
input	[31:0]	u_pro31_data1;
input	[31:0]	u_pro31_data2;
input	[31:0]	u_pro31_data3;
input	[31:0]	u_pro31_data4;
input	[31:0]	u_pro31_data5;
input	[31:0]	u_pro31_data6;
input	[31:0]	u_pro31_data7;
output			x_pro31_ack;
output	[31:0]	x_pro31_data0;
output	[31:0]	x_pro31_data1;
output	[31:0]	x_pro31_data2;
output	[31:0]	x_pro31_data3;
output	[31:0]	x_pro31_data4;
output	[31:0]	x_pro31_data5;
output	[31:0]	x_pro31_data6;
output	[31:0]	x_pro31_data7;

input			u_int_req;
input			u_int_wr_rd_;
input	[31:0]	u_int_addr;
input	[3:0]	u_int_byte;
input	[1:0]	u_int_wordsize;
input	[31:0]	u_int_data0;
input	[31:0]	u_int_data1;
input	[31:0]	u_int_data2;
input	[31:0]	u_int_data3;
input	[31:0]	u_int_data4;
input	[31:0]	u_int_data5;
input	[31:0]	u_int_data6;
input	[31:0]	u_int_data7;
output			x_int_ack;
output	[31:0]	x_int_data0;
output	[31:0]	x_int_data1;
output	[31:0]	x_int_data2;
output	[31:0]	x_int_data3;
output	[31:0]	x_int_data4;
output	[31:0]	x_int_data5;
output	[31:0]	x_int_data6;
output	[31:0]	x_int_data7;

input		u_pro31_sel	    ;
input		u_pro30_sel		;
input		u_pro29_sel		;
input		u_pro28_sel		;
input		u_pro27_sel		;
input		u_pro26_sel		;
input		u_pro25_sel		;
input		u_pro24_sel		;
input		u_pro23_sel		;
input		u_pro22_sel		;
input		u_pro21_sel	    ;
input		u_pro20_sel		;
input		u_pro19_sel		;
input		u_pro18_sel		;
input		u_pro17_sel		;
input		u_pro16_sel	    ;
input		u_pro15_sel	    ;
input		u_pro14_sel		;
input		u_pro13_sel		;
input		u_pro12_sel		;
input		u_pro11_sel		;
input		u_pro10_sel		;
input		u_pro09_sel		;
input		u_pro08_sel		;
input		u_pro07_sel		;
input		u_pro06_sel		;
input		u_pro05_sel	    ;
input		u_pro04_sel		;
input		u_pro03_sel		;
input		u_pro02_sel		;
input		u_pro01_sel		;
input		u_pro00_sel	    ;
input		u_int_sel		;

input			g_sclk;
input			g_rst_;

wire		x_pro00_ack_tmp	;
wire		x_pro01_ack_tmp ;
wire		x_pro02_ack_tmp ;
wire		x_pro03_ack_tmp ;
wire		x_pro04_ack_tmp ;
wire		x_pro05_ack_tmp ;
wire		x_pro06_ack_tmp ;
wire		x_pro07_ack_tmp ;
wire		x_pro08_ack_tmp ;
wire		x_pro09_ack_tmp ;
wire		x_pro10_ack_tmp ;
wire		x_pro11_ack_tmp ;
wire		x_pro12_ack_tmp ;
wire		x_pro13_ack_tmp ;
wire		x_pro14_ack_tmp ;
wire		x_pro15_ack_tmp ;
wire		x_pro16_ack_tmp	;
wire		x_pro17_ack_tmp ;
wire		x_pro18_ack_tmp ;
wire		x_pro19_ack_tmp ;
wire		x_pro20_ack_tmp ;
wire		x_pro21_ack_tmp ;
wire		x_pro22_ack_tmp ;
wire		x_pro23_ack_tmp ;
wire		x_pro24_ack_tmp ;
wire		x_pro25_ack_tmp ;
wire		x_pro26_ack_tmp ;
wire		x_pro27_ack_tmp ;
wire		x_pro28_ack_tmp ;
wire		x_pro29_ack_tmp ;
wire		x_pro30_ack_tmp ;
wire		x_pro31_ack_tmp ;
wire		x_int_ack_tmp   ;

reg			x_pro00_ack		;
reg			x_pro01_ack		;
reg			x_pro02_ack		;
reg			x_pro03_ack		;
reg			x_pro04_ack		;
reg			x_pro05_ack		;
reg			x_pro06_ack		;
reg			x_pro07_ack		;
reg			x_pro08_ack		;
reg			x_pro09_ack		;
reg			x_pro10_ack		;
reg			x_pro11_ack		;
reg			x_pro12_ack		;
reg			x_pro13_ack		;
reg			x_pro14_ack		;
reg			x_pro15_ack		;
reg			x_pro16_ack		;
reg			x_pro17_ack		;
reg			x_pro18_ack		;
reg			x_pro19_ack		;
reg			x_pro20_ack		;
reg			x_pro21_ack		;
reg			x_pro22_ack		;
reg			x_pro23_ack		;
reg			x_pro24_ack		;
reg			x_pro25_ack		;
reg			x_pro26_ack		;
reg			x_pro27_ack		;
reg			x_pro28_ack		;
reg			x_pro29_ack		;
reg			x_pro30_ack		;
reg			x_pro31_ack		;
reg			x_int_ack		;

reg			task_ack_latch;
always	@(posedge g_sclk or negedge g_rst_)
		if (~g_rst_)	begin
			task_ack_latch	<= #udly	1'b0;
			x_pro00_ack		<= #udly	1'b0;
			x_pro01_ack		<= #udly	1'b0;
			x_pro02_ack		<= #udly	1'b0;
			x_pro03_ack		<= #udly	1'b0;
			x_pro04_ack		<= #udly	1'b0;
			x_pro05_ack		<= #udly	1'b0;
			x_pro06_ack		<= #udly	1'b0;
			x_pro07_ack		<= #udly	1'b0;
			x_pro08_ack		<= #udly	1'b0;
			x_pro09_ack		<= #udly	1'b0;
			x_pro10_ack		<= #udly	1'b0;
			x_pro11_ack		<= #udly	1'b0;
			x_pro12_ack		<= #udly	1'b0;
			x_pro13_ack		<= #udly	1'b0;
			x_pro14_ack		<= #udly	1'b0;
			x_pro15_ack		<= #udly	1'b0;
			x_pro16_ack		<= #udly	1'b0;
			x_pro17_ack		<= #udly	1'b0;
			x_pro18_ack		<= #udly	1'b0;
			x_pro19_ack		<= #udly	1'b0;
			x_pro20_ack		<= #udly	1'b0;
			x_pro21_ack		<= #udly	1'b0;
			x_pro22_ack		<= #udly	1'b0;
			x_pro23_ack		<= #udly	1'b0;
			x_pro24_ack		<= #udly	1'b0;
			x_pro25_ack		<= #udly	1'b0;
			x_pro26_ack		<= #udly	1'b0;
			x_pro27_ack		<= #udly	1'b0;
			x_pro28_ack		<= #udly	1'b0;
			x_pro29_ack		<= #udly	1'b0;
			x_pro30_ack		<= #udly	1'b0;
			x_pro31_ack		<= #udly	1'b0;
			x_int_ack		<= #udly	1'b0;
		end
		else	begin
			task_ack_latch	<= #udly	task_ack;
			x_pro00_ack		<= #udly	x_pro00_ack_tmp;
			x_pro01_ack     <= #udly    x_pro01_ack_tmp;
			x_pro02_ack		<= #udly    x_pro02_ack_tmp;
			x_pro03_ack		<= #udly    x_pro03_ack_tmp;
			x_pro04_ack		<= #udly    x_pro04_ack_tmp;
			x_pro05_ack		<= #udly    x_pro05_ack_tmp;
			x_pro06_ack		<= #udly    x_pro06_ack_tmp;
			x_pro07_ack		<= #udly    x_pro07_ack_tmp;
			x_pro08_ack		<= #udly    x_pro08_ack_tmp;
			x_pro09_ack		<= #udly    x_pro09_ack_tmp;
			x_pro10_ack		<= #udly    x_pro10_ack_tmp;
			x_pro11_ack     <= #udly    x_pro11_ack_tmp;
			x_pro12_ack		<= #udly    x_pro12_ack_tmp;
			x_pro13_ack		<= #udly    x_pro13_ack_tmp;
			x_pro14_ack		<= #udly    x_pro14_ack_tmp;
			x_pro15_ack		<= #udly    x_pro15_ack_tmp;
			x_pro16_ack		<= #udly	x_pro16_ack_tmp;
			x_pro17_ack     <= #udly    x_pro17_ack_tmp;
			x_pro18_ack		<= #udly    x_pro18_ack_tmp;
			x_pro19_ack		<= #udly    x_pro19_ack_tmp;
			x_pro20_ack		<= #udly    x_pro20_ack_tmp;
			x_pro21_ack		<= #udly    x_pro21_ack_tmp;
			x_pro22_ack		<= #udly    x_pro22_ack_tmp;
			x_pro23_ack		<= #udly    x_pro23_ack_tmp;
			x_pro24_ack		<= #udly    x_pro24_ack_tmp;
			x_pro25_ack		<= #udly    x_pro25_ack_tmp;
			x_pro26_ack		<= #udly    x_pro26_ack_tmp;
			x_pro27_ack     <= #udly    x_pro27_ack_tmp;
			x_pro28_ack		<= #udly    x_pro28_ack_tmp;
			x_pro29_ack		<= #udly    x_pro29_ack_tmp;
			x_pro30_ack		<= #udly    x_pro30_ack_tmp;
			x_pro31_ack		<= #udly    x_pro31_ack_tmp;
			x_int_ack		<= #udly    x_int_ack_tmp;
		end

assign	task_req	= 	u_pro00_sel		& 	u_pro00_req		|
						u_pro01_sel     & 	u_pro01_req  	|
						u_pro02_sel     & 	u_pro02_req  	|
						u_pro03_sel     & 	u_pro03_req  	|
						u_pro04_sel     & 	u_pro04_req  	|
						u_pro05_sel     & 	u_pro05_req  	|
						u_pro06_sel     & 	u_pro06_req  	|
						u_pro07_sel    	& 	u_pro07_req  	|
						u_pro08_sel     & 	u_pro08_req  	|
						u_pro09_sel     & 	u_pro09_req  	|
					  	u_pro10_sel  	& 	u_pro10_req  	|
					  	u_pro11_sel     & 	u_pro11_req  	|
						u_pro12_sel     & 	u_pro12_req  	|
						u_pro13_sel     & 	u_pro13_req  	|
						u_pro14_sel     & 	u_pro14_req  	|
						u_pro15_sel     & 	u_pro15_req  	|
						u_pro16_sel		& 	u_pro16_req		|
						u_pro17_sel     & 	u_pro17_req  	|
						u_pro18_sel     & 	u_pro18_req  	|
						u_pro19_sel     & 	u_pro19_req  	|
						u_pro20_sel     & 	u_pro20_req  	|
						u_pro21_sel     & 	u_pro21_req  	|
						u_pro22_sel     & 	u_pro22_req  	|
						u_pro23_sel    	& 	u_pro23_req  	|
						u_pro24_sel     & 	u_pro24_req  	|
						u_pro25_sel     & 	u_pro25_req  	|
					  	u_pro26_sel  	& 	u_pro26_req  	|
					  	u_pro27_sel     & 	u_pro27_req  	|
						u_pro28_sel     & 	u_pro28_req  	|
						u_pro29_sel     & 	u_pro29_req  	|
						u_pro30_sel     & 	u_pro30_req  	|
						u_pro31_sel     & 	u_pro31_req  	|
	                    u_int_sel       & 	u_int_req       ;

assign	task_wr_rd_	=	u_pro00_sel   	&	u_pro00_wr_rd_		|
                        u_pro01_sel     &   u_pro01_wr_rd_      |
                        u_pro02_sel     &   u_pro02_wr_rd_      |
                        u_pro03_sel     &   u_pro03_wr_rd_      |
                        u_pro04_sel     &   u_pro04_wr_rd_      |
                        u_pro05_sel     &   u_pro05_wr_rd_     	|
                        u_pro06_sel     &   u_pro06_wr_rd_      |
                        u_pro07_sel    	&   u_pro07_wr_rd_    	|
                        u_pro08_sel     &   u_pro08_wr_rd_      |
                        u_pro09_sel     &   u_pro09_wr_rd_      |
                        u_pro10_sel  	&   u_pro10_wr_rd_  	|
                        u_pro11_sel     &   u_pro11_wr_rd_      |
                        u_pro12_sel     &   u_pro12_wr_rd_      |
                        u_pro13_sel     &   u_pro13_wr_rd_      |
                        u_pro14_sel     &   u_pro14_wr_rd_      |
                        u_pro15_sel     &   u_pro15_wr_rd_     	|
                        u_pro16_sel   	&	u_pro16_wr_rd_		|
                        u_pro17_sel     &   u_pro17_wr_rd_      |
                        u_pro18_sel     &   u_pro18_wr_rd_      |
                        u_pro19_sel     &   u_pro19_wr_rd_      |
                        u_pro20_sel     &   u_pro20_wr_rd_      |
                        u_pro21_sel     &   u_pro21_wr_rd_     	|
                        u_pro22_sel     &   u_pro22_wr_rd_      |
                        u_pro23_sel    	&   u_pro23_wr_rd_    	|
                        u_pro24_sel     &   u_pro24_wr_rd_      |
                        u_pro25_sel     &   u_pro25_wr_rd_      |
                        u_pro26_sel  	&   u_pro26_wr_rd_  	|
                        u_pro27_sel     &   u_pro27_wr_rd_      |
                        u_pro28_sel     &   u_pro28_wr_rd_      |
                        u_pro29_sel     &   u_pro29_wr_rd_      |
                        u_pro30_sel     &   u_pro30_wr_rd_      |
                        u_pro31_sel     &   u_pro31_wr_rd_     	|
                        u_int_sel       &   u_int_wr_rd_       	;

assign	task_addr	=	{32{u_pro00_sel   	}}&	u_pro00_addr    |
                        {32{u_pro01_sel   	}}&	u_pro01_addr    |
                        {32{u_pro02_sel   	}}&	u_pro02_addr    |
                        {32{u_pro03_sel   	}}&	u_pro03_addr    |
                        {32{u_pro04_sel   	}}&	u_pro04_addr    |
                        {32{u_pro05_sel   	}}&	u_pro05_addr    |
                        {32{u_pro06_sel   	}}&	u_pro06_addr    |
                        {32{u_pro07_sel  	}}&	u_pro07_addr   	|
                        {32{u_pro08_sel    	}}&	u_pro08_addr    |
                        {32{u_pro09_sel    	}}&	u_pro09_addr    |
                        {32{u_pro10_sel		}}&	u_pro10_addr	|
                        {32{u_pro11_sel   	}}&	u_pro11_addr    |
                        {32{u_pro12_sel   	}}&	u_pro12_addr    |
                        {32{u_pro13_sel   	}}&	u_pro13_addr    |
                        {32{u_pro14_sel   	}}&	u_pro14_addr    |
                        {32{u_pro15_sel   	}}&	u_pro15_addr    |
                        {32{u_pro16_sel   	}}&	u_pro16_addr    |
                        {32{u_pro17_sel   	}}&	u_pro17_addr    |
                        {32{u_pro18_sel   	}}&	u_pro18_addr    |
                        {32{u_pro19_sel   	}}&	u_pro19_addr    |
                        {32{u_pro20_sel   	}}&	u_pro20_addr    |
                        {32{u_pro21_sel   	}}&	u_pro21_addr    |
                        {32{u_pro22_sel   	}}&	u_pro22_addr    |
                        {32{u_pro23_sel  	}}&	u_pro23_addr   	|
                        {32{u_pro24_sel    	}}&	u_pro24_addr    |
                        {32{u_pro25_sel    	}}&	u_pro25_addr    |
                        {32{u_pro26_sel		}}&	u_pro26_addr	|
                        {32{u_pro27_sel   	}}&	u_pro27_addr    |
                        {32{u_pro28_sel   	}}&	u_pro28_addr    |
                        {32{u_pro29_sel   	}}&	u_pro29_addr    |
                        {32{u_pro30_sel   	}}&	u_pro30_addr    |
                        {32{u_pro31_sel   	}}&	u_pro31_addr    |
                        {32{u_int_sel     	}}&	u_int_addr     	;


assign	task_byte	=	{4{u_pro00_sel   	}}&	u_pro00_byte    |
                        {4{u_pro01_sel     	}}&	u_pro01_byte    |
                        {4{u_pro02_sel     	}}&	u_pro02_byte    |
                        {4{u_pro03_sel      }}&	u_pro03_byte    |
                        {4{u_pro04_sel     	}}&	u_pro04_byte    |
                        {4{u_pro05_sel   	}}&	u_pro05_byte    |
                        {4{u_pro06_sel     	}}&	u_pro06_byte    |
                        {4{u_pro07_sel  	}}&	u_pro07_byte   	|
                        {4{u_pro08_sel    	}}&	u_pro08_byte    |
                        {4{u_pro09_sel    	}}&	u_pro09_byte    |
                        {4{u_pro10_sel		}}&	u_pro10_byte	|
                        {4{u_pro11_sel     	}}&	u_pro11_byte    |
                        {4{u_pro12_sel     	}}&	u_pro12_byte    |
                        {4{u_pro13_sel      }}&	u_pro13_byte    |
                        {4{u_pro14_sel     	}}&	u_pro14_byte    |
                        {4{u_pro15_sel   	}}&	u_pro15_byte    |
                        {4{u_pro16_sel   	}}&	u_pro16_byte    |
                        {4{u_pro17_sel     	}}&	u_pro17_byte    |
                        {4{u_pro18_sel     	}}&	u_pro18_byte    |
                        {4{u_pro19_sel      }}&	u_pro19_byte    |
                        {4{u_pro20_sel     	}}&	u_pro20_byte    |
                        {4{u_pro21_sel   	}}&	u_pro21_byte    |
                        {4{u_pro22_sel     	}}&	u_pro22_byte    |
                        {4{u_pro23_sel  	}}&	u_pro23_byte   	|
                        {4{u_pro24_sel    	}}&	u_pro24_byte    |
                        {4{u_pro25_sel    	}}&	u_pro25_byte    |
                        {4{u_pro26_sel		}}&	u_pro26_byte	|
                        {4{u_pro27_sel     	}}&	u_pro27_byte    |
                        {4{u_pro28_sel     	}}&	u_pro28_byte    |
                        {4{u_pro29_sel      }}&	u_pro29_byte    |
                        {4{u_pro30_sel     	}}&	u_pro30_byte    |
                        {4{u_pro31_sel   	}}&	u_pro31_byte    |
                        {4{u_int_sel     	}}&	u_int_byte     	;

assign	task_wordsize =	{2{u_pro00_sel   	}}&	u_pro00_wordsize    |
                        {2{u_pro01_sel     	}}&	u_pro01_wordsize    |
                        {2{u_pro02_sel     	}}&	u_pro02_wordsize    |
                        {2{u_pro03_sel      }}&	u_pro03_wordsize    |
                        {2{u_pro04_sel     	}}&	u_pro04_wordsize    |
                        {2{u_pro05_sel   	}}&	u_pro05_wordsize    |
                        {2{u_pro06_sel     	}}&	u_pro06_wordsize    |
                        {2{u_pro07_sel  	}}&	u_pro07_wordsize   	|
                        {2{u_pro08_sel    	}}&	u_pro08_wordsize    |
                        {2{u_pro09_sel    	}}&	u_pro09_wordsize    |
                        {2{u_pro10_sel		}}&	u_pro10_wordsize	|
                        {2{u_pro11_sel     	}}&	u_pro11_wordsize    |
                        {2{u_pro12_sel     	}}&	u_pro12_wordsize    |
                        {2{u_pro13_sel      }}&	u_pro13_wordsize    |
                        {2{u_pro14_sel     	}}&	u_pro14_wordsize    |
                        {2{u_pro15_sel   	}}&	u_pro15_wordsize    |
                        {2{u_pro16_sel   	}}&	u_pro16_wordsize    |
                        {2{u_pro17_sel     	}}&	u_pro17_wordsize    |
                        {2{u_pro18_sel     	}}&	u_pro18_wordsize    |
                        {2{u_pro19_sel      }}&	u_pro19_wordsize    |
                        {2{u_pro20_sel     	}}&	u_pro20_wordsize    |
                        {2{u_pro21_sel   	}}&	u_pro21_wordsize    |
                        {2{u_pro22_sel     	}}&	u_pro22_wordsize    |
                        {2{u_pro23_sel  	}}&	u_pro23_wordsize   	|
                        {2{u_pro24_sel    	}}&	u_pro24_wordsize    |
                        {2{u_pro25_sel    	}}&	u_pro25_wordsize    |
                        {2{u_pro26_sel		}}&	u_pro26_wordsize	|
                        {2{u_pro27_sel     	}}&	u_pro27_wordsize    |
                        {2{u_pro28_sel     	}}&	u_pro28_wordsize    |
                        {2{u_pro29_sel      }}&	u_pro29_wordsize    |
                        {2{u_pro30_sel     	}}&	u_pro30_wordsize    |
                        {2{u_pro31_sel   	}}&	u_pro31_wordsize    |
                        {2{u_int_sel     	}}&	u_int_wordsize     	;

assign	task_data0	=	{32{u_pro00_sel   	}}&	u_pro00_data0    	|
                        {32{u_pro01_sel     }}&	u_pro01_data0      	|
                        {32{u_pro02_sel     }}&	u_pro02_data0      	|
                        {32{u_pro03_sel     }}&	u_pro03_data0       |
                        {32{u_pro04_sel     }}&	u_pro04_data0      	|
                        {32{u_pro05_sel   	}}&	u_pro05_data0    	|
                        {32{u_pro06_sel     }}&	u_pro06_data0      	|
                        {32{u_pro07_sel  	}}&	u_pro07_data0   	|
                        {32{u_pro08_sel    	}}&	u_pro08_data0     	|
                        {32{u_pro09_sel    	}}&	u_pro09_data0     	|
                        {32{u_pro10_sel		}}&	u_pro10_data0		|
                        {32{u_pro11_sel     }}&	u_pro11_data0      	|
                        {32{u_pro12_sel     }}&	u_pro12_data0      	|
                        {32{u_pro13_sel     }}&	u_pro13_data0       |
                        {32{u_pro14_sel     }}&	u_pro14_data0      	|
                        {32{u_pro15_sel   	}}&	u_pro15_data0    	|
                        {32{u_pro16_sel   	}}&	u_pro16_data0    	|
                        {32{u_pro17_sel     }}&	u_pro17_data0      	|
                        {32{u_pro18_sel     }}&	u_pro18_data0      	|
                        {32{u_pro19_sel     }}&	u_pro19_data0       |
                        {32{u_pro20_sel     }}&	u_pro20_data0      	|
                        {32{u_pro21_sel   	}}&	u_pro21_data0    	|
                        {32{u_pro22_sel     }}&	u_pro22_data0      	|
                        {32{u_pro23_sel  	}}&	u_pro23_data0   	|
                        {32{u_pro24_sel    	}}&	u_pro24_data0     	|
                        {32{u_pro25_sel    	}}&	u_pro25_data0     	|
                        {32{u_pro26_sel		}}&	u_pro26_data0		|
                        {32{u_pro27_sel     }}&	u_pro27_data0      	|
                        {32{u_pro28_sel     }}&	u_pro28_data0      	|
                        {32{u_pro29_sel     }}&	u_pro29_data0       |
                        {32{u_pro30_sel     }}&	u_pro30_data0      	|
                        {32{u_pro31_sel   	}}&	u_pro31_data0    	|
                        {32{u_int_sel     	}}&	u_int_data0     	;

assign	task_data1	=	{32{u_pro00_sel   	}}&	u_pro00_data1    	|
                        {32{u_pro01_sel     }}&	u_pro01_data1      	|
                        {32{u_pro02_sel     }}&	u_pro02_data1      	|
                        {32{u_pro03_sel     }}&	u_pro03_data1       |
                        {32{u_pro04_sel     }}&	u_pro04_data1      	|
                        {32{u_pro05_sel   	}}&	u_pro05_data1    	|
                        {32{u_pro06_sel     }}&	u_pro06_data1      	|
                        {32{u_pro07_sel  	}}&	u_pro07_data1   	|
                        {32{u_pro08_sel    	}}&	u_pro08_data1     	|
                        {32{u_pro09_sel    	}}&	u_pro09_data1     	|
                        {32{u_pro10_sel		}}&	u_pro10_data1		|
                        {32{u_pro11_sel     }}&	u_pro11_data1      	|
                        {32{u_pro12_sel     }}&	u_pro12_data1      	|
                        {32{u_pro13_sel     }}&	u_pro13_data1       |
                        {32{u_pro14_sel     }}&	u_pro14_data1      	|
                        {32{u_pro15_sel   	}}&	u_pro15_data1    	|
                        {32{u_pro16_sel   	}}&	u_pro16_data1    	|
                        {32{u_pro17_sel     }}&	u_pro17_data1      	|
                        {32{u_pro18_sel     }}&	u_pro18_data1      	|
                        {32{u_pro19_sel     }}&	u_pro19_data1       |
                        {32{u_pro20_sel     }}&	u_pro20_data1      	|
                        {32{u_pro21_sel   	}}&	u_pro21_data1    	|
                        {32{u_pro22_sel     }}&	u_pro22_data1      	|
                        {32{u_pro23_sel  	}}&	u_pro23_data1   	|
                        {32{u_pro24_sel    	}}&	u_pro24_data1     	|
                        {32{u_pro25_sel    	}}&	u_pro25_data1     	|
                        {32{u_pro26_sel		}}&	u_pro26_data1		|
                        {32{u_pro27_sel     }}&	u_pro27_data1      	|
                        {32{u_pro28_sel     }}&	u_pro28_data1      	|
                        {32{u_pro29_sel     }}&	u_pro29_data1       |
                        {32{u_pro30_sel     }}&	u_pro30_data1      	|
                        {32{u_pro31_sel   	}}&	u_pro31_data1    	|
                        {32{u_int_sel     	}}&	u_int_data1     	;

assign	task_data2	=	{32{u_pro00_sel   	}}&	u_pro00_data2    	|
                        {32{u_pro01_sel     }}&	u_pro01_data2      	|
                        {32{u_pro02_sel     }}&	u_pro02_data2      	|
                        {32{u_pro03_sel     }}&	u_pro03_data2   	|
                        {32{u_pro04_sel     }}&	u_pro04_data2      	|
                        {32{u_pro05_sel   	}}&	u_pro05_data2    	|
                        {32{u_pro06_sel     }}&	u_pro06_data2      	|
                        {32{u_pro07_sel  	}}&	u_pro07_data2   	|
                        {32{u_pro08_sel    	}}&	u_pro08_data2     	|
                        {32{u_pro09_sel    	}}&	u_pro09_data2     	|
                        {32{u_pro10_sel		}}&	u_pro10_data2		|
                        {32{u_pro11_sel     }}&	u_pro11_data2      	|
                        {32{u_pro12_sel     }}&	u_pro12_data2      	|
                        {32{u_pro13_sel     }}&	u_pro13_data2   	|
                        {32{u_pro14_sel     }}&	u_pro14_data2      	|
                        {32{u_pro15_sel   	}}&	u_pro15_data2    	|
                        {32{u_pro16_sel   	}}&	u_pro16_data2    	|
                        {32{u_pro17_sel     }}&	u_pro17_data2      	|
                        {32{u_pro18_sel     }}&	u_pro18_data2      	|
                        {32{u_pro19_sel     }}&	u_pro19_data2   	|
                        {32{u_pro20_sel     }}&	u_pro20_data2      	|
                        {32{u_pro21_sel   	}}&	u_pro21_data2    	|
                        {32{u_pro22_sel     }}&	u_pro22_data2      	|
                        {32{u_pro23_sel  	}}&	u_pro23_data2   	|
                        {32{u_pro24_sel    	}}&	u_pro24_data2     	|
                        {32{u_pro25_sel    	}}&	u_pro25_data2     	|
                        {32{u_pro26_sel		}}&	u_pro26_data2		|
                        {32{u_pro27_sel     }}&	u_pro27_data2      	|
                        {32{u_pro28_sel     }}&	u_pro28_data2      	|
                        {32{u_pro29_sel     }}&	u_pro29_data2   	|
                        {32{u_pro30_sel     }}&	u_pro30_data2      	|
                        {32{u_pro31_sel   	}}&	u_pro31_data2    	|
                        {32{u_int_sel     	}}&	u_int_data2     	;

assign	task_data3	=	{32{u_pro00_sel   	}}&	u_pro00_data3    	|
                        {32{u_pro01_sel     }}&	u_pro01_data3      	|
                        {32{u_pro02_sel     }}&	u_pro02_data3      	|
                        {32{u_pro03_sel     }}&	u_pro03_data3      	|
                        {32{u_pro04_sel     }}&	u_pro04_data3      	|
                        {32{u_pro05_sel   	}}&	u_pro05_data3    	|
                        {32{u_pro06_sel     }}&	u_pro06_data3      	|
                        {32{u_pro07_sel  	}}&	u_pro07_data3   	|
                        {32{u_pro08_sel    	}}&	u_pro08_data3     	|
                        {32{u_pro09_sel    	}}&	u_pro09_data3     	|
                        {32{u_pro10_sel		}}&	u_pro10_data3		|
                        {32{u_pro11_sel     }}&	u_pro11_data3      	|
                        {32{u_pro12_sel     }}&	u_pro12_data3      	|
                        {32{u_pro13_sel     }}&	u_pro13_data3      	|
                        {32{u_pro14_sel     }}&	u_pro14_data3      	|
                        {32{u_pro15_sel   	}}&	u_pro15_data3    	|
                        {32{u_pro16_sel   	}}&	u_pro16_data3    	|
                        {32{u_pro17_sel     }}&	u_pro17_data3      	|
                        {32{u_pro18_sel     }}&	u_pro18_data3      	|
                        {32{u_pro19_sel     }}&	u_pro19_data3   	|
                        {32{u_pro20_sel     }}&	u_pro20_data3      	|
                        {32{u_pro21_sel   	}}&	u_pro21_data3    	|
                        {32{u_pro22_sel     }}&	u_pro22_data3      	|
                        {32{u_pro23_sel  	}}&	u_pro23_data3   	|
                        {32{u_pro24_sel    	}}&	u_pro24_data3     	|
                        {32{u_pro25_sel    	}}&	u_pro25_data3     	|
                        {32{u_pro26_sel		}}&	u_pro26_data3		|
                        {32{u_pro27_sel     }}&	u_pro27_data3      	|
                        {32{u_pro28_sel     }}&	u_pro28_data3      	|
                        {32{u_pro29_sel     }}&	u_pro29_data3   	|
                        {32{u_pro30_sel     }}&	u_pro30_data3      	|
                        {32{u_pro31_sel   	}}&	u_pro31_data3    	|
                        {32{u_int_sel     	}}&	u_int_data3     	;

assign	task_data4	=	{32{u_pro00_sel   	}}&	u_pro00_data4    	|
                        {32{u_pro01_sel     }}&	u_pro01_data4      	|
                        {32{u_pro02_sel     }}&	u_pro02_data4      	|
                        {32{u_pro03_sel     }}&	u_pro03_data4       |
                        {32{u_pro04_sel     }}&	u_pro04_data4      	|
                        {32{u_pro05_sel   	}}&	u_pro05_data4    	|
                        {32{u_pro06_sel     }}&	u_pro06_data4      	|
                        {32{u_pro07_sel  	}}&	u_pro07_data4   	|
                        {32{u_pro08_sel    	}}&	u_pro08_data4     	|
                        {32{u_pro09_sel    	}}&	u_pro09_data4     	|
                        {32{u_pro10_sel		}}&	u_pro10_data4		|
                        {32{u_pro11_sel     }}&	u_pro11_data4      	|
                        {32{u_pro12_sel     }}&	u_pro12_data4      	|
                        {32{u_pro13_sel     }}&	u_pro13_data4       |
                        {32{u_pro14_sel     }}&	u_pro14_data4      	|
                        {32{u_pro15_sel   	}}&	u_pro15_data4    	|
                        {32{u_pro16_sel   	}}&	u_pro16_data4    	|
                        {32{u_pro17_sel     }}&	u_pro17_data4      	|
                        {32{u_pro18_sel     }}&	u_pro18_data4      	|
                        {32{u_pro19_sel     }}&	u_pro19_data4   	|
                        {32{u_pro20_sel     }}&	u_pro20_data4      	|
                        {32{u_pro21_sel   	}}&	u_pro21_data4    	|
                        {32{u_pro22_sel     }}&	u_pro22_data4      	|
                        {32{u_pro23_sel  	}}&	u_pro23_data4   	|
                        {32{u_pro24_sel    	}}&	u_pro24_data4     	|
                        {32{u_pro25_sel    	}}&	u_pro25_data4     	|
                        {32{u_pro26_sel		}}&	u_pro26_data4		|
                        {32{u_pro27_sel     }}&	u_pro27_data4      	|
                        {32{u_pro28_sel     }}&	u_pro28_data4      	|
                        {32{u_pro29_sel     }}&	u_pro29_data4   	|
                        {32{u_pro30_sel     }}&	u_pro30_data4      	|
                        {32{u_pro31_sel   	}}&	u_pro31_data4    	|
                        {32{u_int_sel     	}}&	u_int_data4     	;

assign	task_data5	=	{32{u_pro00_sel   	}}&	u_pro00_data5    	|
                        {32{u_pro01_sel     }}&	u_pro01_data5      	|
                        {32{u_pro02_sel     }}&	u_pro02_data5      	|
                        {32{u_pro03_sel     }}&	u_pro03_data5       |
                        {32{u_pro04_sel     }}&	u_pro04_data5      	|
                        {32{u_pro05_sel   	}}&	u_pro05_data5    	|
                        {32{u_pro06_sel     }}&	u_pro06_data5      	|
                        {32{u_pro07_sel  	}}&	u_pro07_data5   	|
                        {32{u_pro08_sel    	}}&	u_pro08_data5     	|
                        {32{u_pro09_sel    	}}&	u_pro09_data5     	|
                        {32{u_pro10_sel		}}&	u_pro10_data5		|
                        {32{u_pro11_sel     }}&	u_pro11_data5      	|
                        {32{u_pro12_sel     }}&	u_pro12_data5      	|
                        {32{u_pro13_sel     }}&	u_pro13_data5       |
                        {32{u_pro14_sel     }}&	u_pro14_data5      	|
                        {32{u_pro15_sel   	}}&	u_pro15_data5    	|
                        {32{u_pro16_sel   	}}&	u_pro16_data5    	|
                        {32{u_pro17_sel     }}&	u_pro17_data5      	|
                        {32{u_pro18_sel     }}&	u_pro18_data5      	|
                        {32{u_pro19_sel     }}&	u_pro19_data5   	|
                        {32{u_pro20_sel     }}&	u_pro20_data5      	|
                        {32{u_pro21_sel   	}}&	u_pro21_data5    	|
                        {32{u_pro22_sel     }}&	u_pro22_data5      	|
                        {32{u_pro23_sel  	}}&	u_pro23_data5   	|
                        {32{u_pro24_sel    	}}&	u_pro24_data5     	|
                        {32{u_pro25_sel    	}}&	u_pro25_data5     	|
                        {32{u_pro26_sel		}}&	u_pro26_data5		|
                        {32{u_pro27_sel     }}&	u_pro27_data5      	|
                        {32{u_pro28_sel     }}&	u_pro28_data5      	|
                        {32{u_pro29_sel     }}&	u_pro29_data5   	|
                        {32{u_pro30_sel     }}&	u_pro30_data5      	|
                        {32{u_pro31_sel   	}}&	u_pro31_data5    	|
                        {32{u_int_sel     	}}&	u_int_data5     	;

assign	task_data6	=	{32{u_pro00_sel   	}}&	u_pro00_data6    	|
                        {32{u_pro01_sel     }}&	u_pro01_data6      	|
                        {32{u_pro02_sel     }}&	u_pro02_data6      	|
                        {32{u_pro03_sel     }}&	u_pro03_data6   	|
                        {32{u_pro04_sel     }}&	u_pro04_data6      	|
                        {32{u_pro05_sel   	}}&	u_pro05_data6    	|
                        {32{u_pro06_sel     }}&	u_pro06_data6      	|
                        {32{u_pro07_sel  	}}&	u_pro07_data6   	|
                        {32{u_pro08_sel    	}}&	u_pro08_data6     	|
                        {32{u_pro09_sel    	}}&	u_pro09_data6     	|
                        {32{u_pro10_sel		}}&	u_pro10_data6		|
                        {32{u_pro11_sel     }}&	u_pro11_data6      	|
                        {32{u_pro12_sel     }}&	u_pro12_data6      	|
                        {32{u_pro13_sel     }}&	u_pro13_data6   	|
                        {32{u_pro14_sel     }}&	u_pro14_data6      	|
                        {32{u_pro15_sel   	}}&	u_pro15_data6    	|
                        {32{u_pro16_sel   	}}&	u_pro16_data6    	|
                        {32{u_pro17_sel     }}&	u_pro17_data6      	|
                        {32{u_pro18_sel     }}&	u_pro18_data6      	|
                        {32{u_pro19_sel     }}&	u_pro19_data6   	|
                        {32{u_pro20_sel     }}&	u_pro20_data6      	|
                        {32{u_pro21_sel   	}}&	u_pro21_data6    	|
                        {32{u_pro22_sel     }}&	u_pro22_data6      	|
                        {32{u_pro23_sel  	}}&	u_pro23_data6   	|
                        {32{u_pro24_sel    	}}&	u_pro24_data6     	|
                        {32{u_pro25_sel    	}}&	u_pro25_data6     	|
                        {32{u_pro26_sel		}}&	u_pro26_data6		|
                        {32{u_pro27_sel     }}&	u_pro27_data6      	|
                        {32{u_pro28_sel     }}&	u_pro28_data6      	|
                        {32{u_pro29_sel     }}&	u_pro29_data6   	|
                        {32{u_pro30_sel     }}&	u_pro30_data6      	|
                        {32{u_pro31_sel   	}}&	u_pro31_data6    	|
                        {32{u_int_sel     	}}&	u_int_data6     	;

assign	task_data7	=	{32{u_pro00_sel   	}}&	u_pro00_data7    	|
                        {32{u_pro01_sel     }}&	u_pro01_data7      	|
                        {32{u_pro02_sel     }}&	u_pro02_data7      	|
                        {32{u_pro03_sel     }}&	u_pro03_data7      	|
                        {32{u_pro04_sel     }}&	u_pro04_data7      	|
                        {32{u_pro05_sel   	}}&	u_pro05_data7    	|
                        {32{u_pro06_sel     }}&	u_pro06_data7      	|
                        {32{u_pro07_sel  	}}&	u_pro07_data7   	|
                        {32{u_pro08_sel    	}}&	u_pro08_data7     	|
                        {32{u_pro09_sel    	}}&	u_pro09_data7     	|
                        {32{u_pro10_sel		}}&	u_pro10_data7		|
                        {32{u_pro11_sel     }}&	u_pro11_data7      	|
                        {32{u_pro12_sel     }}&	u_pro12_data7      	|
                        {32{u_pro13_sel     }}&	u_pro13_data7      	|
                        {32{u_pro14_sel     }}&	u_pro14_data7      	|
                        {32{u_pro15_sel   	}}&	u_pro15_data7    	|
                        {32{u_pro16_sel   	}}&	u_pro16_data7    	|
                        {32{u_pro17_sel     }}&	u_pro17_data7      	|
                        {32{u_pro18_sel     }}&	u_pro18_data7      	|
                        {32{u_pro19_sel     }}&	u_pro19_data7   	|
                        {32{u_pro20_sel     }}&	u_pro20_data7      	|
                        {32{u_pro21_sel   	}}&	u_pro21_data7    	|
                        {32{u_pro22_sel     }}&	u_pro22_data7      	|
                        {32{u_pro23_sel  	}}&	u_pro23_data7   	|
                        {32{u_pro24_sel    	}}&	u_pro24_data7     	|
                        {32{u_pro25_sel    	}}&	u_pro25_data7     	|
                        {32{u_pro26_sel		}}&	u_pro26_data7		|
                        {32{u_pro27_sel     }}&	u_pro27_data7      	|
                        {32{u_pro28_sel     }}&	u_pro28_data7      	|
                        {32{u_pro29_sel     }}&	u_pro29_data7   	|
                        {32{u_pro30_sel     }}&	u_pro30_data7      	|
                        {32{u_pro31_sel   	}}&	u_pro31_data7    	|
                        {32{u_int_sel     	}}&	u_int_data7     	;

assign	x_pro00_ack_tmp		=	u_pro00_sel		&	task_ack;//_latch;
assign	x_pro01_ack_tmp		=	u_pro01_sel		&	task_ack;//_latch;
assign	x_pro02_ack_tmp		=	u_pro02_sel		&	task_ack;//_latch;
assign	x_pro03_ack_tmp		=	u_pro03_sel		&	task_ack;//_latch;
assign	x_pro04_ack_tmp		=	u_pro04_sel		&	task_ack;//_latch;
assign	x_pro05_ack_tmp		=	u_pro05_sel		&	task_ack;//_latch;
assign	x_pro06_ack_tmp		=	u_pro06_sel		&	task_ack;//_latch;
assign	x_pro07_ack_tmp		=	u_pro07_sel		&	task_ack;//_latch;
assign	x_pro08_ack_tmp		=	u_pro08_sel		& 	task_ack;//_latch;
assign	x_pro09_ack_tmp		=	u_pro09_sel		&	task_ack;//_latch;
assign	x_pro10_ack_tmp		=	u_pro10_sel		&	task_ack;//_latch;
assign	x_pro11_ack_tmp		=	u_pro11_sel		&	task_ack;//_latch;
assign	x_pro12_ack_tmp		=	u_pro12_sel		&	task_ack;//_latch;
assign	x_pro13_ack_tmp		=	u_pro13_sel		&	task_ack;//_latch;
assign	x_pro14_ack_tmp		=	u_pro14_sel		&	task_ack;//_latch;
assign	x_pro15_ack_tmp		=	u_pro15_sel		&	task_ack;//_latch;
assign	x_pro16_ack_tmp		=	u_pro16_sel		&	task_ack;//_latch;
assign	x_pro17_ack_tmp		=	u_pro17_sel		&	task_ack;//_latch;
assign	x_pro18_ack_tmp		=	u_pro18_sel		&	task_ack;//_latch;
assign	x_pro19_ack_tmp		=	u_pro19_sel		&	task_ack;//_latch;
assign	x_pro20_ack_tmp		=	u_pro20_sel		&	task_ack;//_latch;
assign	x_pro21_ack_tmp		=	u_pro21_sel		&	task_ack;//_latch;
assign	x_pro22_ack_tmp		=	u_pro22_sel		&	task_ack;//_latch;
assign	x_pro23_ack_tmp		=	u_pro23_sel		&	task_ack;//_latch;
assign	x_pro24_ack_tmp		=	u_pro24_sel		& 	task_ack;//_latch;
assign	x_pro25_ack_tmp		=	u_pro25_sel		&	task_ack;//_latch;
assign	x_pro26_ack_tmp		=	u_pro26_sel		&	task_ack;//_latch;
assign	x_pro27_ack_tmp		=	u_pro27_sel		&	task_ack;//_latch;
assign	x_pro28_ack_tmp		=	u_pro28_sel		&	task_ack;//_latch;
assign	x_pro29_ack_tmp		=	u_pro29_sel		&	task_ack;//_latch;
assign	x_pro30_ack_tmp		=	u_pro30_sel		&	task_ack;//_latch;
assign	x_pro31_ack_tmp		=	u_pro31_sel		&	task_ack;//_latch;
assign	x_int_ack_tmp		=	u_int_sel		&	task_ack;//_latch;

assign	x_pro00_data0		=	back_data0;
assign	x_pro01_data0		=	back_data0;
assign	x_pro02_data0		=	back_data0;
assign	x_pro03_data0		=	back_data0;
assign	x_pro04_data0		=	back_data0;
assign	x_pro05_data0		=	back_data0;
assign	x_pro06_data0		=	back_data0;
assign	x_pro07_data0		=	back_data0;
assign	x_pro08_data0		=	back_data0;
assign	x_pro09_data0		=	back_data0;
assign	x_pro10_data0		=	back_data0;
assign	x_pro11_data0		=	back_data0;
assign	x_pro12_data0		=	back_data0;
assign	x_pro13_data0		=	back_data0;
assign	x_pro14_data0		=	back_data0;
assign	x_pro15_data0		=	back_data0;
assign	x_pro16_data0		=	back_data0;
assign	x_pro17_data0		=	back_data0;
assign	x_pro18_data0		=	back_data0;
assign	x_pro19_data0		=	back_data0;
assign	x_pro20_data0		=	back_data0;
assign	x_pro21_data0		=	back_data0;
assign	x_pro22_data0		=	back_data0;
assign	x_pro23_data0		=	back_data0;
assign	x_pro24_data0		=	back_data0;
assign	x_pro25_data0		=	back_data0;
assign	x_pro26_data0		=	back_data0;
assign	x_pro27_data0		=	back_data0;
assign	x_pro28_data0		=	back_data0;
assign	x_pro29_data0		=	back_data0;
assign	x_pro30_data0		=	back_data0;
assign	x_pro31_data0		=	back_data0;
assign	x_int_data0			=	back_data0;

assign	x_pro00_data1		=	back_data1;
assign	x_pro01_data1		=	back_data1;
assign	x_pro02_data1		=	back_data1;
assign	x_pro03_data1		=	back_data1;
assign	x_pro04_data1 		=	back_data1;
assign	x_pro05_data1		=	back_data1;
assign	x_pro06_data1		=	back_data1;
assign	x_pro07_data1		=	back_data1;
assign	x_pro08_data1		=	back_data1;
assign	x_pro09_data1		=	back_data1;
assign	x_pro10_data1		=	back_data1;
assign	x_pro11_data1		=	back_data1;
assign	x_pro12_data1		=	back_data1;
assign	x_pro13_data1		=	back_data1;
assign	x_pro14_data1 		=	back_data1;
assign	x_pro15_data1		=	back_data1;
assign	x_pro16_data1		=	back_data1;
assign	x_pro17_data1		=	back_data1;
assign	x_pro18_data1		=	back_data1;
assign	x_pro19_data1		=	back_data1;
assign	x_pro20_data1		=	back_data1;
assign	x_pro21_data1		=	back_data1;
assign	x_pro22_data1		=	back_data1;
assign	x_pro23_data1		=	back_data1;
assign	x_pro24_data1		=	back_data1;
assign	x_pro25_data1		=	back_data1;
assign	x_pro26_data1		=	back_data1;
assign	x_pro27_data1		=	back_data1;
assign	x_pro28_data1		=	back_data1;
assign	x_pro29_data1		=	back_data1;
assign	x_pro30_data1		=	back_data1;
assign	x_pro31_data1		=	back_data1;
assign	x_int_data1			=	back_data1;

assign	x_pro00_data2		=	back_data2;
assign	x_pro01_data2		=	back_data2;
assign	x_pro02_data2		=	back_data2;
assign	x_pro03_data2		=	back_data2;
assign	x_pro04_data2		=	back_data2;
assign	x_pro05_data2		=	back_data2;
assign	x_pro06_data2		=	back_data2;
assign	x_pro07_data2		=	back_data2;
assign	x_pro08_data2		=	back_data2;
assign	x_pro09_data2		=	back_data2;
assign	x_pro10_data2		=	back_data2;
assign	x_pro11_data2		=	back_data2;
assign	x_pro12_data2		=	back_data2;
assign	x_pro13_data2		=	back_data2;
assign	x_pro14_data2		=	back_data2;
assign	x_pro15_data2		=	back_data2;
assign	x_pro16_data2		=	back_data2;
assign	x_pro17_data2		=	back_data2;
assign	x_pro18_data2		=	back_data2;
assign	x_pro19_data2		=	back_data2;
assign	x_pro20_data2		=	back_data2;
assign	x_pro21_data2		=	back_data2;
assign	x_pro22_data2		=	back_data2;
assign	x_pro23_data2		=	back_data2;
assign	x_pro24_data2		=	back_data2;
assign	x_pro25_data2		=	back_data2;
assign	x_pro26_data2		=	back_data2;
assign	x_pro27_data2		=	back_data2;
assign	x_pro28_data2		=	back_data2;
assign	x_pro29_data2		=	back_data2;
assign	x_pro30_data2		=	back_data2;
assign	x_pro31_data2		=	back_data2;
assign	x_int_data2			=	back_data2;

assign	x_pro00_data3		=	back_data3;
assign	x_pro01_data3 		=	back_data3;
assign	x_pro02_data3		=	back_data3;
assign	x_pro03_data3		=	back_data3;
assign	x_pro04_data3		=	back_data3;
assign	x_pro05_data3		=	back_data3;
assign	x_pro06_data3		=	back_data3;
assign	x_pro07_data3		=	back_data3;
assign	x_pro08_data3		=	back_data3;
assign	x_pro09_data3		=	back_data3;
assign	x_pro10_data3		=	back_data3;
assign	x_pro11_data3 		=	back_data3;
assign	x_pro12_data3		=	back_data3;
assign	x_pro13_data3		=	back_data3;
assign	x_pro14_data3		=	back_data3;
assign	x_pro15_data3		=	back_data3;
assign	x_pro16_data3		=	back_data3;
assign	x_pro17_data3		=	back_data3;
assign	x_pro18_data3		=	back_data3;
assign	x_pro19_data3		=	back_data3;
assign	x_pro20_data3		=	back_data3;
assign	x_pro21_data3		=	back_data3;
assign	x_pro22_data3		=	back_data3;
assign	x_pro23_data3		=	back_data3;
assign	x_pro24_data3		=	back_data3;
assign	x_pro25_data3		=	back_data3;
assign	x_pro26_data3		=	back_data3;
assign	x_pro27_data3		=	back_data3;
assign	x_pro28_data3		=	back_data3;
assign	x_pro29_data3		=	back_data3;
assign	x_pro30_data3		=	back_data3;
assign	x_pro31_data3		=	back_data3;
assign	x_int_data3			=	back_data3;

assign	x_pro00_data4		=	back_data4;
assign	x_pro01_data4		=	back_data4;
assign	x_pro02_data4		=	back_data4;
assign	x_pro03_data4		=	back_data4;
assign	x_pro04_data4		=	back_data4;
assign	x_pro05_data4		=	back_data4;
assign	x_pro06_data4		=	back_data4;
assign	x_pro07_data4		=	back_data4;
assign	x_pro08_data4		=	back_data4;
assign	x_pro09_data4		=	back_data4;
assign	x_pro10_data4		=	back_data4;
assign	x_pro11_data4		=	back_data4;
assign	x_pro12_data4		=	back_data4;
assign	x_pro13_data4		=	back_data4;
assign	x_pro14_data4		=	back_data4;
assign	x_pro15_data4		=	back_data4;
assign	x_pro16_data4		=	back_data4;
assign	x_pro17_data4		=	back_data4;
assign	x_pro18_data4		=	back_data4;
assign	x_pro19_data4		=	back_data4;
assign	x_pro20_data4		=	back_data4;
assign	x_pro21_data4		=	back_data4;
assign	x_pro22_data4		=	back_data4;
assign	x_pro23_data4		=	back_data4;
assign	x_pro24_data4		=	back_data4;
assign	x_pro25_data4		=	back_data4;
assign	x_pro26_data4		=	back_data4;
assign	x_pro27_data4		=	back_data4;
assign	x_pro28_data4		=	back_data4;
assign	x_pro29_data4		=	back_data4;
assign	x_pro30_data4		=	back_data4;
assign	x_pro31_data4		=	back_data4;
assign	x_int_data4			=	back_data4;

assign	x_pro00_data5		=	back_data5;
assign	x_pro01_data5		=	back_data5;
assign	x_pro02_data5		=	back_data5;
assign	x_pro03_data5		=	back_data5;
assign	x_pro04_data5 		=	back_data5;
assign	x_pro05_data5		=	back_data5;
assign	x_pro06_data5		=	back_data5;
assign	x_pro07_data5		=	back_data5;
assign	x_pro08_data5		=	back_data5;
assign	x_pro09_data5		=	back_data5;
assign	x_pro10_data5		=	back_data5;
assign	x_pro11_data5		=	back_data5;
assign	x_pro12_data5		=	back_data5;
assign	x_pro13_data5		=	back_data5;
assign	x_pro14_data5 		=	back_data5;
assign	x_pro15_data5		=	back_data5;
assign	x_pro16_data5		=	back_data5;
assign	x_pro17_data5		=	back_data5;
assign	x_pro18_data5		=	back_data5;
assign	x_pro19_data5		=	back_data5;
assign	x_pro20_data5		=	back_data5;
assign	x_pro21_data5		=	back_data5;
assign	x_pro22_data5		=	back_data5;
assign	x_pro23_data5		=	back_data5;
assign	x_pro24_data5		=	back_data5;
assign	x_pro25_data5		=	back_data5;
assign	x_pro26_data5		=	back_data5;
assign	x_pro27_data5		=	back_data5;
assign	x_pro28_data5		=	back_data5;
assign	x_pro29_data5		=	back_data5;
assign	x_pro30_data5		=	back_data5;
assign	x_pro31_data5		=	back_data5;
assign	x_int_data5			=	back_data5;

assign	x_pro00_data6		=	back_data6;
assign	x_pro01_data6		=	back_data6;
assign	x_pro02_data6		=	back_data6;
assign	x_pro03_data6		=	back_data6;
assign	x_pro04_data6		=	back_data6;
assign	x_pro05_data6		=	back_data6;
assign	x_pro06_data6		=	back_data6;
assign	x_pro07_data6		=	back_data6;
assign	x_pro08_data6		=	back_data6;
assign	x_pro09_data6		=	back_data6;
assign	x_pro10_data6		=	back_data6;
assign	x_pro11_data6		=	back_data6;
assign	x_pro12_data6		=	back_data6;
assign	x_pro13_data6		=	back_data6;
assign	x_pro14_data6		=	back_data6;
assign	x_pro15_data6		=	back_data6;
assign	x_pro16_data6		=	back_data6;
assign	x_pro17_data6		=	back_data6;
assign	x_pro18_data6		=	back_data6;
assign	x_pro19_data6		=	back_data6;
assign	x_pro20_data6		=	back_data6;
assign	x_pro21_data6		=	back_data6;
assign	x_pro22_data6		=	back_data6;
assign	x_pro23_data6		=	back_data6;
assign	x_pro24_data6		=	back_data6;
assign	x_pro25_data6		=	back_data6;
assign	x_pro26_data6		=	back_data6;
assign	x_pro27_data6		=	back_data6;
assign	x_pro28_data6		=	back_data6;
assign	x_pro29_data6		=	back_data6;
assign	x_pro30_data6		=	back_data6;
assign	x_pro31_data6		=	back_data6;
assign	x_int_data6			=	back_data6;

assign	x_pro00_data7		=	back_data7;
assign	x_pro01_data7 		=	back_data7;
assign	x_pro02_data7		=	back_data7;
assign	x_pro03_data7		=	back_data7;
assign	x_pro04_data7		=	back_data7;
assign	x_pro05_data7		=	back_data7;
assign	x_pro06_data7		=	back_data7;
assign	x_pro07_data7		=	back_data7;
assign	x_pro08_data7		=	back_data7;
assign	x_pro09_data7		=	back_data7;
assign	x_pro10_data7		=	back_data7;
assign	x_pro11_data7 		=	back_data7;
assign	x_pro12_data7		=	back_data7;
assign	x_pro13_data7		=	back_data7;
assign	x_pro14_data7		=	back_data7;
assign	x_pro15_data7		=	back_data7;
assign	x_pro16_data7		=	back_data7;
assign	x_pro17_data7		=	back_data7;
assign	x_pro18_data7		=	back_data7;
assign	x_pro19_data7		=	back_data7;
assign	x_pro20_data7		=	back_data7;
assign	x_pro21_data7		=	back_data7;
assign	x_pro22_data7		=	back_data7;
assign	x_pro23_data7		=	back_data7;
assign	x_pro24_data7		=	back_data7;
assign	x_pro25_data7		=	back_data7;
assign	x_pro26_data7		=	back_data7;
assign	x_pro27_data7		=	back_data7;
assign	x_pro28_data7		=	back_data7;
assign	x_pro29_data7		=	back_data7;
assign	x_pro30_data7		=	back_data7;
assign	x_pro31_data7		=	back_data7;
assign	x_int_data7			=	back_data7;

endmodule