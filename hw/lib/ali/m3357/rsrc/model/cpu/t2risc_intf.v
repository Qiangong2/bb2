/******************************************************************************
          (c) copyrights 1997-2000. All rights reserved

           T-Square Design, Inc.

          Company confidential and Properietary information.
          This information may not be disclosed to unauthorized
          individual.
******************************************************************************/
/*********************************************************************
//File 	: 	t2risc_intf.v -- used for test chipset module
//Author: 	Yuky
//Date	: 	2001-04-11	Initial version
//			2001-06-11	update the tasks
//			2002-05-14	start to design the time-share system/multi-threading:
//						1).rename the filename and mudule name from "risc_core" to
//						t2risc_intf;
//						2).move the old operation tasks(read/write) to the pro_basic_tasks
//						module, invoke the pro_task_core module here.
//						3).del some signals of the interface, which will not be used as
//						the T2 IBUS standard of CPU interface. the deleted signas:
//						x_bus_idle, u_busy_to_mem, u_abort_trans, g_config_ep, g_clk_rate
//						4).support 8words sequence data transaction
//			2002-05-17  cut the old FSM to reduce the latency between each request
//			2002-05-22	programable latency between two operation, support
//						best case: 	one clock
//						worst case:	256 clocks
*********************************************************************/
`timescale	1ns/100ps
module	t2risc_intf  (
//external agent signals
	x_ack,		// last data tranfer
	x_ready,	// In read, means data is on the bus x_data_in
				// In write, means data on u_data_to_send be accepted
				// This signal should assert one sclk for every word
	x_bus_error,// Asserted one clock when bus data error
	x_data_in,	// The recieved data
	x_err_int,	// The cpu bus error interrupt
	x_ext_int,	// The external interrupt

//processor signals
	u_request,	// Request reading or writing,asserted until the x_ack
				// for last word being accepted
	u_write,	// 1: write, 0: read
	u_wordsize,	// 0 -> 1words, 1 -> 2words,  2 -> 4words,  3 -> 8words
	u_startpaddr,	// Request address
	u_data_to_send,	// Write data
	u_bytemask,		// four bits bytemask signals for u_data_to_send,
					// 1 means valid, 0 means invalid

	g_sclk,	        // memory interface clock, it is strickly half of
					// pipeline clk
	g_reset_		// global reset
	);
output			u_request;
output			u_write;
output	[1:0]	u_wordsize;
output	[31:0]	u_startpaddr;
output	[31:0]	u_data_to_send; // if memory interface only support 32 bits mode
output	[3:0]	u_bytemask;	// if memory interface only support 32 bits mode

input			x_ack;
input			x_ready;
input			x_bus_error;
input	[31:0]	x_data_in;
input			x_err_int,
				x_ext_int;

input			g_sclk;
input			g_reset_;

parameter 		udly = 0.9;
parameter		latch_dly = 0.1;
parameter		filter_dly = 0.1;
//reg			u_request,u_write;
//reg 	[1:0] 	u_wordsize;
//reg	[31:0] 	u_startpaddr;
//reg	[3:0] 	u_bytemask;
//reg	[31:0]	u_data_to_send;

wire			u_request,u_write;
wire 	[1:0] 	u_wordsize;
wire	[31:0] 	u_startpaddr;
wire	[3:0] 	u_bytemask;
wire	[31:0]	u_data_to_send;

wire			int_flag;
reg				err_int_flag,
				ext_int_flag;
//parameter		IDLE	=	3'b001;
//parameter		REQ		=	3'b010;
//parameter		DATA	=	3'b100;
//reg		[3:0]	st;
reg		[31:0]	data_mem [7:0]	;
reg		[2:0]	mem_pointer;

reg		[7:0]	u_req_latency; //0 --> 1 clock, 1--> 2 clocks, 2--> 3 clocks, ......
reg		[7:0]	latency_set;

wire			task_req;
wire			task_wr_rd_;
wire	[31:0]	task_addr;
wire	[1:0]	task_wordsize;
wire	[3:0]	task_byte;
wire	[31:0]	task_data0;
wire	[31:0]	task_data1;
wire	[31:0]	task_data2;
wire	[31:0]	task_data3;
wire	[31:0]	task_data4;
wire	[31:0]	task_data5;
wire	[31:0]	task_data6;
wire	[31:0]	task_data7;
wire	[31:0]	back_data0;
wire	[31:0]	back_data1;
wire	[31:0]	back_data2;
wire	[31:0]	back_data3;
wire	[31:0]	back_data4;
wire	[31:0]	back_data5;
wire	[31:0]	back_data6;
wire	[31:0]	back_data7;

wire			u_req_mask;

assign		u_data_to_send	= u_req_mask ?(
							  {32{(mem_pointer == 3'b000)}} & task_data0 |
							  {32{(mem_pointer == 3'b001)}} & task_data1 |
							  {32{(mem_pointer == 3'b010)}} & task_data2 |
							  {32{(mem_pointer == 3'b011)}} & task_data3 |
							  {32{(mem_pointer == 3'b100)}} & task_data4 |
							  {32{(mem_pointer == 3'b101)}} & task_data5 |
							  {32{(mem_pointer == 3'b110)}} & task_data6 |
							  {32{(mem_pointer == 3'b111)}} & task_data7 )
							  : 32'h0;

assign		back_data0	=	data_mem[0];
assign		back_data1	=	data_mem[1];
assign		back_data2	=	data_mem[2];
assign		back_data3	=	data_mem[3];
assign		back_data4	=	data_mem[4];
assign		back_data5	=	data_mem[5];
assign		back_data6	=	data_mem[6];
assign		back_data7	=	data_mem[7];


assign	#filter_dly	u_request 		= task_req 		& u_req_mask;
assign	#filter_dly	u_write   		= task_wr_rd_	& u_req_mask;
assign	#filter_dly	u_wordsize		= task_wordsize	& {2{u_req_mask}};
assign	#filter_dly	u_startpaddr	= task_addr		& {32{u_req_mask}};
assign	#filter_dly	u_bytemask		= task_byte		& {4{u_req_mask}};
//control the mem pointer
always @(posedge g_sclk or negedge g_reset_)
	if (!g_reset_)
		mem_pointer				<= #udly 3'b000;
	else if (x_ack)
		mem_pointer				<= #udly 3'b000;
	else if (x_ready)
		mem_pointer				<= #udly mem_pointer + 1;

//save the data during read operation
always @(posedge g_sclk)
	if (x_ready && ~task_wr_rd_)
		data_mem[mem_pointer]	<= #latch_dly x_data_in[31:0];

//calculate the u_request latency
assign	u_req_mask = u_req_latency == 0;
always @(posedge g_sclk or negedge g_reset_)
	if (~g_reset_)
		u_req_latency <= #udly latency_set;
	else if (x_ack)
		u_req_latency <= #udly latency_set;
	else if (task_req & ~u_req_mask)
		u_req_latency <= #udly u_req_latency - 1;
/*
//=================== System Interface FSM ========================
always	@(posedge g_sclk or negedge g_reset_)
	if (~g_reset_)
		begin
		st				<= #udly IDLE;
		u_request 		<= #udly 1'b0;
		u_write   		<= #udly 1'b0;
		u_wordsize		<= #udly 1'b0;
		u_startpaddr	<= #udly 32'h0;
		u_bytemask		<= #udly 4'h0;
		mem_pointer		<= #udly 3'b000;
		end
	else 	begin
		case (st)
		IDLE:	begin
			if (task_req)
				st				<= #udly REQ;
			else	begin
				st				<= #udly IDLE;
				u_request 		<= #udly 1'b0;
				u_write   		<= #udly 1'b0;
				u_wordsize		<= #udly 1'b0;
				u_startpaddr	<= #udly 32'h0;
				u_bytemask		<= #udly 4'h0;
				mem_pointer		<= #udly 3'b000;
			end
		end

		REQ:	begin
				st				<= #udly DATA;
				u_request 		<= #udly 1'b1;
				u_write   		<= #udly task_wr_rd_;
				u_wordsize		<= #udly task_wordsize;
				u_startpaddr	<= #udly task_addr;
				u_bytemask		<= #udly task_byte;
		end

		DATA:	begin
			if (x_ready) begin
				mem_pointer				<= #udly mem_pointer + 1;
				if (~task_wr_rd_)
					data_mem[mem_pointer]	<= #udly x_data_in[31:0];
				if (x_ack) begin
					st	<= #udly	IDLE;
					u_request 			<= #udly 1'b0;
					u_write   			<= #udly 1'b0;
					u_wordsize			<= #udly 1'b0;
					u_startpaddr		<= #udly 32'h0;
					u_bytemask			<= #udly 4'h0;
					mem_pointer			<= #udly 3'b000;
				end
			end
		end

		default:begin
			st				<= #udly IDLE;
			u_request 		<= #udly 1'b0;
			u_write   		<= #udly 1'b0;
			u_wordsize		<= #udly 1'b0;
			u_startpaddr	<= #udly 32'h0;
			u_bytemask		<= #udly 4'h0;
			mem_pointer		<= #udly 3'b000;
		end

		endcase
	end
*/
//=================================================================
pro_task_core	pro_task_core(
	.task_req		(task_req		),
	.task_wr_rd_	(task_wr_rd_	),
	.task_addr		(task_addr		),
	.task_byte		(task_byte		),
	.task_wordsize	(task_wordsize	),
	.task_data0		(task_data0		),
	.task_data1		(task_data1		),
	.task_data2		(task_data2		),
	.task_data3		(task_data3		),
	.task_data4		(task_data4		),
	.task_data5		(task_data5		),
	.task_data6		(task_data6		),
	.task_data7		(task_data7		),
	.task_ack		(x_ack			),
	.back_data0		(back_data0		),
	.back_data1		(back_data1		),
	.back_data2		(back_data2		),
	.back_data3		(back_data3		),
	.back_data4		(back_data4		),
	.back_data5		(back_data5		),
	.back_data6		(back_data6		),
	.back_data7		(back_data7		),
	.int_flag		(int_flag		),

	.g_sclk			(g_sclk			),
	.g_rst_         (g_reset_       )
	);
//=================Interrupt process ============================

assign	int_flag = x_ext_int | x_err_int;

task	int_check;
output	err_int_flag_out;
output	ext_int_flag_out;
begin
	err_int_flag_out = x_err_int;
	ext_int_flag_out = x_ext_int;
end
endtask
//====================================================================

initial	latency_set = 0; //default value
task	u_latency_set;
input [7:0]	lat_set;
begin
	latency_set = lat_set;
end
endtask

endmodule
