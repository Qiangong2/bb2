/**********************************************************
*  File:        t2_cpu.v
*  Description: verilog shell module for T2-CPU ISA
*               model
*  Author:      Farmer Wang
*  Revision:    2002.1.05
*               The interface signal is compatible
*               with T2-RISCLP core, but the Ejtag,
*               Power management, MIPS interface is NULL
***********************************************************/

module  T2_CPU(
            //output ports
			// jtag interface
            J_TDO,

			// MIPS bus interface
            SYSAD_OUT,
            SYSCMD_OUT,
            SYSOUT_OE,
            PVALIDJ,
            PMASTERJ,
            PREQJ,

			// internal bus interface
            U_REQUEST,
            U_WRITE,
            U_WORDSIZE,
            U_BYTEMASK,
            U_STARTPADDR,
            U_DATA_TO_SEND,

			// DFT interface
            BIST_ERROR_VEC,
            BIST_FINISH,
	        TEST_SO,

			// other signals
            STATUS_RP,
            STATUS_SUSPEND,
            STATUS_EXL,
            STATUS_ERL,

            //input  ports
            // JTAG interface
            J_TDI,
            J_TMS,
            J_TCLK,
			J_TRSTJ,
			// DFT interface
            BIST_MODE,
	        SCAN_MODE,
	        TEST_SE,
	        TEST_SI,
			// internal bus interface
            O_READY,
            O_ACK,
            O_BUS_ERROR,
            O_DATA_IN,
            // MIPS interface
            SYSAD_IN,
            SYSCMD_IN,
            EOKJ,
            EVALIDJ,
            EREQJ,

			// other signals
            NMIJ,
            INTJ,
            PD_CPU_MDIV,
            PD_CPU_MBUS,
            PROBE_ENABLE,
            CLK,
            G_SCLK,
            RESETJ,
            RSTJ
            );

output          J_TDO;

output  [31:0]  SYSAD_OUT;
output  [4:0]   SYSCMD_OUT;
output          SYSOUT_OE;
output          PVALIDJ;
output          PMASTERJ;
output          PREQJ;

output          U_REQUEST;
output          U_WRITE;
output  [1:0]   U_WORDSIZE;
output  [3:0]   U_BYTEMASK;
output  [31:0]  U_STARTPADDR;
output  [31:0]  U_DATA_TO_SEND;

output  [5:0]   BIST_ERROR_VEC;
output          BIST_FINISH;
output	[7:0]	TEST_SO;

output          STATUS_RP;
output          STATUS_SUSPEND;
output          STATUS_EXL;
output          STATUS_ERL;

input           J_TDI;
input           J_TMS;
input			J_TCLK;
input			J_TRSTJ;

input           BIST_MODE;
input		    SCAN_MODE;
input		    TEST_SE;
input	[7:0]	TEST_SI;

input           O_READY;
input           O_ACK;
input           O_BUS_ERROR;
input   [31:0]  O_DATA_IN;

input   [31:0]  SYSAD_IN;
input   [4:0]   SYSCMD_IN;
input           EOKJ;
input           EVALIDJ;
input           EREQJ;

input           NMIJ;
input   [4:0]   INTJ;
input   [1:0]   PD_CPU_MDIV;
input   	    PD_CPU_MBUS;
input           PROBE_ENABLE;
input           CLK;
input           G_SCLK;
input           RESETJ;
input           RSTJ;

reg          J_TDO;
reg          STATUS_RP;
reg          STATUS_SUSPEND;
reg          STATUS_EXL;
reg          STATUS_ERL;
reg  [31:0]  SYSAD_OUT;
reg  [4:0]   SYSCMD_OUT;
reg          SYSOUT_OE;
reg          PVALIDJ;
reg          PMASTERJ;
reg          PREQJ;
reg  [5:0]   BIST_ERROR_VEC;
reg          BIST_FINISH;
reg	 [7:0]	 TEST_SO;

initial begin
  J_TDO=1'b0;
  STATUS_RP=1'b0;
  STATUS_SUSPEND=1'b0;
  STATUS_EXL=1'b0;
  STATUS_ERL=1'b0;
  SYSAD_OUT=32'b0;
  SYSCMD_OUT=5'b0;
  SYSOUT_OE=1'b0;
  PVALIDJ=1'b1;
  PMASTERJ=1'b1;
  PREQJ=1'b1;
  BIST_ERROR_VEC=6'b0;
  BIST_FINISH=1'b0;
  TEST_SO=8'b0;
end

reg		   U_REQUEST;
reg        U_WRITE;
reg [31:0] U_STARTPADDR;
reg [1:0]  U_WORDSIZE;
reg [3:0]  U_BYTEMASK;
reg [31:0] U_DATA_TO_SEND;

reg [31:0] zr, at, v0, v1, a0, a1, a2, a3;
reg [31:0] t0, t1, t2, t3, t4, t5, t6, t7;
reg [31:0] s0, s1, s2, s3, s4, s5, s6, s7;
reg [31:0] t8, t9, k0, k1, gp, sp, s8, ra;
reg [31:0] pc, inst_cnt, ir;

/* exit reason
0 normal
1 require bus service
2 encounter break point
3 single step
4 terminate
5,6,7 for future extension
*/
reg [2:0] reason;
reg [3:0] dynamic;

reg [31:0] paddr;
reg [31:0] cycle_cnt;
reg [15:0] wordsize;
reg [255:0] wr_buf;
reg write_flag;
reg [319:0] inst_p;
reg [255:0] lfb;

event req_bus;
event data_ready;

initial begin
  pc <= 32'b0;
  ir <= 32'b0;
  dynamic=4'b1010;
  @(negedge RSTJ);
  @(posedge RSTJ)
  $risc_initial(RSTJ, "../../../rsim/stimu.t/cpu/t2risc1.cfg", dynamic);
end

always @(posedge CLK)
  if(RSTJ) begin
    reason=$risc_exec_inst(top.chip.core.T2_CPU, {2'b0,~NMIJ,~INTJ});
    cycle_cnt=0;
    if(reason==3'b1)begin
      ->req_bus;
      @(data_ready);
      $risc_feed_data(O_BUS_ERROR, lfb);
    end
/*
    else begin
      inst_p=$deassembler(pc, ir);
      $display("INST_CNT=%d PC=%x IR=%x Code=%s", inst_cnt, pc, ir, inst_p);
    end
*/
    if(reason==0 )
      $display("INST_CNT=%d PC=%x IR=%x", inst_cnt, pc, ir);
    if(reason == 3'b100)
      #1 $finish;
  end

/*
initial begin
 $fsdbDumpfile("isa.fsdb");
 $fsdbDumpvars(0, top);
end
*/


always @(posedge CLK)begin
  cycle_cnt=cycle_cnt+1;
  if(cycle_cnt > 50000)begin
    $display("Pattern sad because of DEAD LOCK!");
  end
end

reg	[3:0]	bytemask;
always @(req_bus) begin
  //add bus transaction below
/*********************************************************************************
jim begin, 11/16/01
**********************************************************************************/
    if(write_flag && (wordsize == 2) && (paddr == 32'h123000)) begin
      if(wr_buf[31:0]==32'h65616063)begin
        $display("************************************");
        $display("           Pattern Sad              ");
        $display("************************************");
        $finish;
      end
      if(wr_buf[31:0]==32'h88888888)begin
        $display("************************************");
        $display("         Pattern Happy              ");
        $display("************************************");
        $finish;
      end
    end
	case (wordsize[3:0])
	4'b0000: begin	// byte operation
	           case (paddr[1:0])
	           2'b00: bytemask = 4'b0001;
	           2'b01: bytemask = 4'b0010;
	           2'b10: bytemask = 4'b0100;
	           2'b11: bytemask = 4'b1000;
	           endcase
	           if (write_flag)
	              write_w(paddr, wr_buf, bytemask);
	           else
	              read_w(paddr, bytemask);
	        end
	4'b0001: begin	// halfword operation
	           bytemask = paddr[1] ? 4'b1100 : 4'b0011;
	           if (write_flag)
	              write_w(paddr, wr_buf, bytemask);
	           else
	              read_w(paddr, bytemask);
	        end
	4'b0010: begin	// word operation
 	         if (write_flag)
	            write_w(paddr, wr_buf, 4'b1111);
	         else
	            read_w(paddr, 4'b1111);
	        end
	4'b0011: begin	// doubleword operation
		 if (write_flag)
		    write_dw(paddr, wr_buf);
		 else
		    read_dw(paddr);
	        end
	4'b0100: begin	// quadword operation
		 if (write_flag)
		    write_4w(paddr, wr_buf);
		 else
		    read_4w(paddr);
	        end
    4'b1000: begin // eightword operation
         if (write_flag)
            write_8w(paddr, wr_buf);
         else
            read_8w(paddr);
         end
	endcase
/*****************************************************************************************/
  ->data_ready;
end

/*****************************************************************************************/
task	write_w;	// write a word to memory
input	[31:0]	address;
input	[255:0]	wr_buf;
input	[3:0]	bytemask;
reg		sending;
reg	[31:0]	wr_data;
begin
	case (address[4:2])
	3'b000:	wr_data = wr_buf[31:0];
	3'b001:	wr_data = wr_buf[63:32];
	3'b010:	wr_data = wr_buf[95:64];
	3'b011:	wr_data = wr_buf[127:96];
    3'b100: wr_data = wr_buf[159:128];
    3'b101: wr_data = wr_buf[191:160];
    3'b110: wr_data = wr_buf[223:192];
    3'b111: wr_data = wr_buf[255:224];
	endcase
	@(posedge CLK) begin
		U_REQUEST	= 1'b1;
		U_WRITE		= 1'b1;
		U_BYTEMASK	= bytemask;
		U_WORDSIZE	= 2'b00;
		U_STARTPADDR	= address;
		U_DATA_TO_SEND	= wr_data;
	end
	sending = 1'b1;
	while (sending) begin
		@(posedge CLK) begin
			if (O_READY) begin
				U_WRITE = 1'b0;
			end
			if (O_ACK) begin
				U_REQUEST = 1'b0;
				sending = 1'b0;
			end
		end
	end
end
endtask

task	write_dw;	// write a doubleworld to memory
input	[31:0]	address;
input	[255:0]	wr_buf;
reg	[31:0]	wr_data1;
reg	[31:0]	wr_data2;
reg		sending;
reg	[1:0]	send_cnt;
begin
	case (address[4:3])
	2'b00:	begin
		   wr_data1 = wr_buf[31:0];
		   wr_data2 = wr_buf[63:32];
		end
	2'b01:	begin
		   wr_data1 = wr_buf[95:64];
		   wr_data2 = wr_buf[127:96];
		end
    2'b10:  begin
		   wr_data1 = wr_buf[159:128];
		   wr_data2 = wr_buf[191:160];
		end
    2'b11:  begin
		   wr_data1 = wr_buf[223:192];
		   wr_data2 = wr_buf[255:224];
		end
	endcase
	@(posedge CLK) begin
		U_REQUEST	= 1'b1;
		U_WRITE		= 1'b1;
		U_BYTEMASK	= 4'b1111;
		U_WORDSIZE	= 2'b01;
		U_STARTPADDR	= address;
		U_DATA_TO_SEND	= wr_data1;
	end
	sending = 1'b1;
	send_cnt = 2'b0;
	while (sending) begin
		@(posedge CLK) begin
			if (O_READY) begin
				if (send_cnt == 2'b00) begin
					U_DATA_TO_SEND = wr_data2;
					send_cnt = send_cnt + 1;
				end
				else if (send_cnt == 2'b01) begin
					U_WRITE = 1'b0;
				end
			end
			if (O_ACK) begin
				U_REQUEST = 1'b0;
				sending = 1'b0;
			end
		end
	end
end
endtask

task	write_4w;	// write four words to memory
input	[31:0]	address;
input	[255:0]	wr_buf;
reg	[31:0]	wr_data1;
reg	[31:0]	wr_data2;
reg	[31:0]	wr_data3;
reg	[31:0]	wr_data4;
reg		sending;
reg	[1:0]	send_cnt;
begin
    if(address[4]) begin
	  wr_data1 = wr_buf[31:0];
	  wr_data2 = wr_buf[63:32];
	  wr_data3 = wr_buf[95:64];
	  wr_data4 = wr_buf[127:96];
    end
    else begin
	  wr_data1 = wr_buf[159:128];
	  wr_data2 = wr_buf[191:160];
	  wr_data3 = wr_buf[223:192];
	  wr_data4 = wr_buf[255:224];
    end
	@(posedge CLK) begin
		U_REQUEST	= 1'b1;
	    U_WRITE		= 1'b1;
		U_BYTEMASK	= 4'b1111;
		U_WORDSIZE	= 2'b10;
		U_STARTPADDR	= address;
		U_DATA_TO_SEND	= wr_data1;
	end
	sending = 1'b1;
	send_cnt = 2'b00;
	while (sending) begin
		@(posedge CLK) begin
			if (O_READY) begin
				if (send_cnt == 2'b00) begin
					U_DATA_TO_SEND = wr_data2;
					send_cnt = send_cnt + 1;
				end
				else if (send_cnt == 2'b01) begin
					U_DATA_TO_SEND = wr_data3;
					send_cnt = send_cnt + 1;
				end
				else if (send_cnt == 2'b10) begin
					U_DATA_TO_SEND = wr_data4;
					send_cnt = send_cnt + 1;
				end
				else if (send_cnt == 2'b11) begin
					U_WRITE = 1'b0;
				end
			end
			if (O_ACK) begin
				U_REQUEST = 1'b0;
				sending = 1'b0;
			end
		end
	end
end
endtask

task	write_8w;	// write four words to memory
input	[31:0]	address;
input	[255:0]	wr_buf;
reg	[31:0]	wr_data1;
reg	[31:0]	wr_data2;
reg	[31:0]	wr_data3;
reg	[31:0]	wr_data4;
reg	[31:0]	wr_data5;
reg	[31:0]	wr_data6;
reg	[31:0]	wr_data7;
reg	[31:0]	wr_data8;
reg		sending;
reg	[2:0]	send_cnt;
begin
	wr_data1 = wr_buf[31:0];
	wr_data2 = wr_buf[63:32];
	wr_data3 = wr_buf[95:64];
	wr_data4 = wr_buf[127:96];
	wr_data5 = wr_buf[159:128];
	wr_data6 = wr_buf[191:160];
	wr_data7 = wr_buf[223:192];
	wr_data8 = wr_buf[255:224];
	@(posedge CLK) begin
		U_REQUEST	= 1'b1;
		U_WRITE		= 1'b1;
		U_BYTEMASK	= 4'b1111;
		U_WORDSIZE	= 2'b11;
		U_STARTPADDR	= address;
		U_DATA_TO_SEND	= wr_data1;
	end
	sending = 1'b1;
	send_cnt = 3'b000;
	while (sending) begin
		@(posedge CLK) begin
			if (O_READY) begin
				if (send_cnt == 3'b000) begin
					U_DATA_TO_SEND = wr_data2;
					send_cnt = send_cnt + 1;
				end
				else if (send_cnt == 3'b001) begin
					U_DATA_TO_SEND = wr_data3;
					send_cnt = send_cnt + 1;
				end
				else if (send_cnt == 3'b010) begin
					U_DATA_TO_SEND = wr_data4;
					send_cnt = send_cnt + 1;
				end
				else if (send_cnt == 3'b011) begin
				  U_DATA_TO_SEND = wr_data5;
				  send_cnt = send_cnt + 1;
				end
				else if (send_cnt == 3'b100) begin
				  U_DATA_TO_SEND = wr_data6;
				  send_cnt = send_cnt + 1;
				end
				else if (send_cnt == 3'b101) begin
				  U_DATA_TO_SEND = wr_data7;
				  send_cnt = send_cnt + 1;
				end
				else if (send_cnt == 3'b110) begin
				  U_DATA_TO_SEND = wr_data8;
				  send_cnt = send_cnt + 1;
                end
				else if (send_cnt == 3'b111) begin
					U_WRITE = 1'b0;
				end
				else begin
				  $display("error in write_8w task, sent_cnt greater than 7!");
				  $finish;
				end
			end
			if (O_ACK) begin
				U_REQUEST = 1'b0;
				sending = 1'b0;
			end
		end
	end
end
endtask

task	read_w;	// read a word from memory
input	[31:0]	address;
input	[3:0]	bytemask;
reg		receiving;
begin
	@(posedge CLK) begin
		U_REQUEST	= 1'b1;
		U_WRITE		= 1'b0;
		U_BYTEMASK	= bytemask;
		U_WORDSIZE	= 2'b00;
		U_STARTPADDR	= address;
	end
	receiving = 1'b1;
	while (receiving) begin
		@(posedge CLK) begin
			if (O_READY) begin
				case (address[4:2])
				3'b000: lfb[31:0]   = O_DATA_IN;
				3'b001: lfb[63:32]  = O_DATA_IN;
				3'b010: lfb[95:64]  = O_DATA_IN;
				3'b011: lfb[127:96] = O_DATA_IN;
				3'b100: begin lfb[159:128] = O_DATA_IN; lfb[31:0]   = O_DATA_IN; end
				3'b101: begin lfb[191:160] = O_DATA_IN; lfb[63:32]  = O_DATA_IN; end
				3'b110: begin lfb[223:192] = O_DATA_IN; lfb[95:64]  = O_DATA_IN; end
				3'b111: begin lfb[255:224] = O_DATA_IN; lfb[127:96] = O_DATA_IN; end
				endcase
			end
			if (O_ACK) begin
				U_REQUEST = 1'b0;
				receiving = 1'b0;
			end
		end
	end
end
endtask

task	read_dw;	// read a doubleword from memory
input	[31:0]	address;
reg		receiving;
reg	[1:0]	receive_cnt;
begin
	@(posedge CLK) begin
		U_REQUEST	= 1'b1;
		U_WRITE		= 1'b0;
		U_BYTEMASK	= 4'b1111;
		U_WORDSIZE	= 2'b01;
		U_STARTPADDR	= address;
	end
	receiving = 1'b1;
	receive_cnt = 2'b00;
	while (receiving) begin
		@(posedge CLK) begin
			if (O_READY) begin
				if (receive_cnt == 2'b00) begin
					case (address[4:3])
					2'b00: lfb[31:0] = O_DATA_IN;
					2'b01: lfb[95:64] = O_DATA_IN;
                    2'b10: begin lfb[159:128] = O_DATA_IN; lfb[31:0] = O_DATA_IN; end
                    2'b11: begin lfb[223:192] = O_DATA_IN; lfb[95:64] = O_DATA_IN; end
					endcase
					receive_cnt = receive_cnt + 1;
				end
				else if (receive_cnt == 2'b01) begin
					case (address[4:3])
					2'b00: lfb[63:32] = O_DATA_IN;
					2'b01: lfb[127:96] = O_DATA_IN;
					2'b01: begin lfb[191:160] = O_DATA_IN; lfb[63:32] = O_DATA_IN; end
					2'b01: begin lfb[255:224] = O_DATA_IN; lfb[127:96] = O_DATA_IN; end
					endcase
				end
			end
			if (O_ACK) begin
				U_REQUEST = 1'b0;
				receiving = 1'b0;
			end
		end
	end
end
endtask

task	read_4w;	// read four words from memory
input	[31:0]	address;
reg		receiving;
reg	[1:0]	receive_cnt;
begin
	@(posedge CLK) begin
		U_REQUEST	= 1'b1;
		U_WRITE		= 1'b0;
		U_BYTEMASK	= 4'b1111;
		U_WORDSIZE	= 2'b10;
		U_STARTPADDR	= address;
	end
	receiving = 1'b1;
	receive_cnt = 2'b00;
	while (receiving) begin
		@(posedge CLK) begin
			if (O_READY) begin
				if (receive_cnt == 2'b00) begin
					lfb[31:0] = O_DATA_IN;
                    lfb[159:128] = O_DATA_IN;
					receive_cnt = receive_cnt + 1;
				end
				else if (receive_cnt == 2'b01) begin
					lfb[63:32] = O_DATA_IN;
					lfb[191:160] = O_DATA_IN;
					receive_cnt = receive_cnt + 1;
				end
				else if (receive_cnt == 2'b10) begin
					lfb[95:64] = O_DATA_IN;
					lfb[223:192] = O_DATA_IN;
					receive_cnt = receive_cnt + 1;
				end
				else if (receive_cnt == 2'b11) begin
					lfb[127:96] = O_DATA_IN;
					lfb[255:224] = O_DATA_IN;
				end
			end
			if (O_ACK) begin
				U_REQUEST = 1'b0;
				receiving = 1'b0;
			end
		end
	end
end
endtask

task	read_8w;	// read eight words from memory
input	[31:0]	address;
reg		receiving;
reg	[2:0]	receive_cnt;
begin
	@(posedge CLK) begin
		U_REQUEST	= 1'b1;
		U_WRITE		= 1'b0;
		U_BYTEMASK	= 4'b1111;
		U_WORDSIZE	= 2'b11;
		U_STARTPADDR = address;
	end
	receiving = 1'b1;
	receive_cnt = 3'b000;
	while (receiving) begin
		@(posedge CLK) begin
			if (O_READY) begin
				if (receive_cnt == 3'b000) begin
					lfb[31:0] = O_DATA_IN;
					receive_cnt = receive_cnt + 1;
				end
				else if (receive_cnt == 3'b001) begin
					lfb[63:32] = O_DATA_IN;
					receive_cnt = receive_cnt + 1;
				end
				else if (receive_cnt == 3'b010) begin
					lfb[95:64] = O_DATA_IN;
					receive_cnt = receive_cnt + 1;
				end
				else if (receive_cnt == 3'b011) begin
					lfb[127:96] = O_DATA_IN;
					receive_cnt = receive_cnt + 1;
				end
				else if (receive_cnt == 3'b100) begin
				  lfb[159:128] = O_DATA_IN;
				  receive_cnt = receive_cnt + 1;
				end
				else if (receive_cnt == 3'b101) begin
				  lfb[191:160] = O_DATA_IN;
				  receive_cnt = receive_cnt + 1;
				end
				else if (receive_cnt == 3'b110) begin
				  lfb[223:192] = O_DATA_IN;
				  receive_cnt = receive_cnt + 1;
				end
				else if (receive_cnt == 3'b111) begin
				  lfb[255:224] = O_DATA_IN;
				end
			end
			if (O_ACK) begin
				U_REQUEST = 1'b0;
				receiving = 1'b0;
			end
		end
	end
end
endtask
/*********************************************************************************
jim end
**********************************************************************************/

endmodule
