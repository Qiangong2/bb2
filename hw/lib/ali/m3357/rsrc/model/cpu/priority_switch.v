/******************************************************************************
          (c) copyrights 1997-2000. All rights reserved

           T-Square Design, Inc.

          Company confidential and Properietary information.
          This information may not be disclosed to unauthorized
          individual.
******************************************************************************/
/*********************************************************************
//File 	: 	priority_switch.v
//Description:
//			used to switch the requests priority for enhancing the
//			flexibility.
//Algorithm:
//			In the arbitrator, the priority ring order is: pro00 -> pro01
//			-> pro02 -> pro03 -> pro04 -> pro05 -> pro06 -> pro07 -> pro08
//			-> pro09-> pro10 -> pro11 -> pro12 -> pro13 -> pro14 -> pro15
//			But in fact, 16 general requests order has 16 permutation.
//			We just use to switch the connection between the external request
//			and its select and the arbitrator internal request and its select
//
//			not including switch the interrupt request. because it always in
//			the top priority
//Author: 	Yuky
//Date	: 	2002-05-25	initial version
//			2002-07-25	extend to 32 channels
*********************************************************************/
module priority_switch(
	switch_in31_req,	//all the requests input here are come from the
	switch_in30_req,	//general tasks set mudule.
	switch_in29_req,
	switch_in28_req,
	switch_in27_req,
	switch_in26_req,
	switch_in25_req,
 	switch_in24_req,
 	switch_in23_req,
 	switch_in22_req,
 	switch_in21_req,
	switch_in20_req,
	switch_in19_req,
	switch_in18_req,
	switch_in17_req,
	switch_in16_req,
	switch_in15_req,	//all the requests input here are come from the
	switch_in14_req,	//general tasks set mudule.
	switch_in13_req,
	switch_in12_req,
	switch_in11_req,
	switch_in10_req,
	switch_in09_req,
 	switch_in08_req,
 	switch_in07_req,
 	switch_in06_req,
 	switch_in05_req,
	switch_in04_req,
	switch_in03_req,
	switch_in02_req,
	switch_in01_req,
	switch_in00_req,

	switch_out31_req,	//all the requests are output to
	switch_out30_req,   //the arbitrator mudule.
	switch_out29_req,
	switch_out28_req,
	switch_out27_req,
	switch_out26_req,
	switch_out25_req,
 	switch_out24_req,
 	switch_out23_req,
 	switch_out22_req,
 	switch_out21_req,
	switch_out20_req,
	switch_out19_req,
	switch_out18_req,
	switch_out17_req,
	switch_out16_req,
	switch_out15_req,	//all the requests are output to
	switch_out14_req,   //the arbitrator mudule.
	switch_out13_req,
	switch_out12_req,
	switch_out11_req,
	switch_out10_req,
	switch_out09_req,
 	switch_out08_req,
 	switch_out07_req,
 	switch_out06_req,
 	switch_out05_req,
	switch_out04_req,
	switch_out03_req,
	switch_out02_req,
	switch_out01_req,
	switch_out00_req,

	switch_in31_lock,	//all the lock input here are come from the
	switch_in30_lock,   //general tasks set mudule.
	switch_in29_lock,
	switch_in28_lock,
	switch_in27_lock,
	switch_in26_lock,
	switch_in25_lock,
 	switch_in24_lock,
 	switch_in23_lock,
 	switch_in22_lock,
 	switch_in21_lock,
	switch_in20_lock,
	switch_in19_lock,
	switch_in18_lock,
	switch_in17_lock,
	switch_in16_lock,
	switch_in15_lock,	//all the lock input here are come from the
	switch_in14_lock,   //general tasks set mudule.
	switch_in13_lock,
	switch_in12_lock,
	switch_in11_lock,
	switch_in10_lock,
	switch_in09_lock,
 	switch_in08_lock,
 	switch_in07_lock,
 	switch_in06_lock,
 	switch_in05_lock,
	switch_in04_lock,
	switch_in03_lock,
	switch_in02_lock,
	switch_in01_lock,
	switch_in00_lock,

	switch_out31_lock,	//all the lock are output to
	switch_out30_lock,  //the arbitrator mudule.
	switch_out29_lock,
	switch_out28_lock,
	switch_out27_lock,
	switch_out26_lock,
	switch_out25_lock,
 	switch_out24_lock,
 	switch_out23_lock,
 	switch_out22_lock,
 	switch_out21_lock,
	switch_out20_lock,
	switch_out19_lock,
	switch_out18_lock,
	switch_out17_lock,
	switch_out16_lock,
	switch_out15_lock,	//all the lock are output to
	switch_out14_lock,  //the arbitrator mudule.
	switch_out13_lock,
	switch_out12_lock,
	switch_out11_lock,
	switch_out10_lock,
	switch_out09_lock,
 	switch_out08_lock,
 	switch_out07_lock,
 	switch_out06_lock,
 	switch_out05_lock,
	switch_out04_lock,
	switch_out03_lock,
	switch_out02_lock,
	switch_out01_lock,
	switch_out00_lock,

	switch_in31_sel,	//all the select input here are come from
	switch_in30_sel,    //the arbitrator mudule.
	switch_in29_sel,
	switch_in28_sel,
	switch_in27_sel,
	switch_in26_sel,
	switch_in25_sel,
 	switch_in24_sel,
 	switch_in23_sel,
 	switch_in22_sel,
 	switch_in21_sel,
	switch_in20_sel,
	switch_in19_sel,
	switch_in18_sel,
	switch_in17_sel,
	switch_in16_sel,
	switch_in15_sel,	//all the select input here are come from
	switch_in14_sel,    //the arbitrator mudule.
	switch_in13_sel,
	switch_in12_sel,
	switch_in11_sel,
	switch_in10_sel,
	switch_in09_sel,
 	switch_in08_sel,
 	switch_in07_sel,
 	switch_in06_sel,
 	switch_in05_sel,
	switch_in04_sel,
	switch_in03_sel,
	switch_in02_sel,
	switch_in01_sel,
	switch_in00_sel,

	switch_out31_sel,	//all the select are output to this module father module
	switch_out30_sel,	//to select the patterns operation information
	switch_out29_sel,
	switch_out28_sel,
	switch_out27_sel,
	switch_out26_sel,
	switch_out25_sel,
 	switch_out24_sel,
 	switch_out23_sel,
 	switch_out22_sel,
 	switch_out21_sel,
	switch_out20_sel,
	switch_out19_sel,
	switch_out18_sel,
	switch_out17_sel,
	switch_out16_sel,
	switch_out15_sel,	//all the select are output to this module father module
	switch_out14_sel,	//to select the patterns operation information
	switch_out13_sel,
	switch_out12_sel,
	switch_out11_sel,
	switch_out10_sel,
	switch_out09_sel,
 	switch_out08_sel,
 	switch_out07_sel,
 	switch_out06_sel,
 	switch_out05_sel,
	switch_out04_sel,
	switch_out03_sel,
	switch_out02_sel,
	switch_out01_sel,
	switch_out00_sel,

	task_ack,
	g_sclk
	);
parameter udly = 0.9;
input	switch_in31_req;	//all the requests input here are come from the
input	switch_in30_req;	//general tasks set mudule.
input	switch_in29_req;
input	switch_in28_req;
input	switch_in27_req;
input	switch_in26_req;
input	switch_in25_req;
input 	switch_in24_req;
input 	switch_in23_req;
input 	switch_in22_req;
input 	switch_in21_req;
input	switch_in20_req;
input	switch_in19_req;
input	switch_in18_req;
input	switch_in17_req;
input	switch_in16_req;
input	switch_in15_req;
input	switch_in14_req;
input	switch_in13_req;
input	switch_in12_req;
input	switch_in11_req;
input	switch_in10_req;
input	switch_in09_req;
input	switch_in08_req;
input	switch_in07_req;
input	switch_in06_req;
input	switch_in05_req;
input	switch_in04_req;
input	switch_in03_req;
input	switch_in02_req;
input	switch_in01_req;
input	switch_in00_req;

output	switch_out31_req;	//all the requests are output to
output	switch_out30_req;   //the arbitrator mudule.
output	switch_out29_req;
output	switch_out28_req;
output	switch_out27_req;
output	switch_out26_req;
output	switch_out25_req;
output 	switch_out24_req;
output 	switch_out23_req;
output 	switch_out22_req;
output 	switch_out21_req;
output	switch_out20_req;
output	switch_out19_req;
output	switch_out18_req;
output	switch_out17_req;
output	switch_out16_req;
output	switch_out15_req;
output	switch_out14_req;
output	switch_out13_req;
output	switch_out12_req;
output	switch_out11_req;
output	switch_out10_req;
output	switch_out09_req;
output 	switch_out08_req;
output 	switch_out07_req;
output 	switch_out06_req;
output 	switch_out05_req;
output	switch_out04_req;
output	switch_out03_req;
output	switch_out02_req;
output	switch_out01_req;
output	switch_out00_req;

input	switch_in31_lock;	//all the lock input here are come from the
input	switch_in30_lock;   //general tasks set mudule.
input	switch_in29_lock;
input	switch_in28_lock;
input	switch_in27_lock;
input	switch_in26_lock;
input	switch_in25_lock;
input 	switch_in24_lock;
input 	switch_in23_lock;
input 	switch_in22_lock;
input 	switch_in21_lock;
input	switch_in20_lock;
input	switch_in19_lock;
input	switch_in18_lock;
input	switch_in17_lock;
input	switch_in16_lock;
input	switch_in15_lock;
input	switch_in14_lock;
input	switch_in13_lock;
input	switch_in12_lock;
input	switch_in11_lock;
input	switch_in10_lock;
input	switch_in09_lock;
input 	switch_in08_lock;
input 	switch_in07_lock;
input 	switch_in06_lock;
input 	switch_in05_lock;
input	switch_in04_lock;
input	switch_in03_lock;
input	switch_in02_lock;
input	switch_in01_lock;
input	switch_in00_lock;

output	switch_out31_lock;	//all the lock are output to
output	switch_out30_lock;  //the arbitrator mudule.
output	switch_out29_lock;
output	switch_out28_lock;
output	switch_out27_lock;
output	switch_out26_lock;
output	switch_out25_lock;
output 	switch_out24_lock;
output 	switch_out23_lock;
output 	switch_out22_lock;
output 	switch_out21_lock;
output	switch_out20_lock;
output	switch_out19_lock;
output	switch_out18_lock;
output	switch_out17_lock;
output	switch_out16_lock;
output	switch_out15_lock;
output	switch_out14_lock;
output	switch_out13_lock;
output	switch_out12_lock;
output	switch_out11_lock;
output	switch_out10_lock;
output	switch_out09_lock;
output 	switch_out08_lock;
output 	switch_out07_lock;
output 	switch_out06_lock;
output 	switch_out05_lock;
output	switch_out04_lock;
output	switch_out03_lock;
output	switch_out02_lock;
output	switch_out01_lock;
output	switch_out00_lock;

input	switch_in31_sel;	//all the select input here are come from
input	switch_in30_sel;    //the arbitrator mudule.
input	switch_in29_sel;
input	switch_in28_sel;
input	switch_in27_sel;
input	switch_in26_sel;
input	switch_in25_sel;
input 	switch_in24_sel;
input 	switch_in23_sel;
input 	switch_in22_sel;
input 	switch_in21_sel;
input	switch_in20_sel;
input	switch_in19_sel;
input	switch_in18_sel;
input	switch_in17_sel;
input	switch_in16_sel;
input	switch_in15_sel;
input	switch_in14_sel;
input	switch_in13_sel;
input	switch_in12_sel;
input	switch_in11_sel;
input	switch_in10_sel;
input	switch_in09_sel;
input 	switch_in08_sel;
input 	switch_in07_sel;
input 	switch_in06_sel;
input 	switch_in05_sel;
input	switch_in04_sel;
input	switch_in03_sel;
input	switch_in02_sel;
input	switch_in01_sel;
input	switch_in00_sel;

output	switch_out31_sel;	//all the select are output to this module father module
output	switch_out30_sel;	//to select the patterns operation information
output	switch_out29_sel;
output	switch_out28_sel;
output	switch_out27_sel;
output	switch_out26_sel;
output	switch_out25_sel;
output 	switch_out24_sel;
output 	switch_out23_sel;
output 	switch_out22_sel;
output 	switch_out21_sel;
output	switch_out20_sel;
output	switch_out19_sel;
output	switch_out18_sel;
output	switch_out17_sel;
output	switch_out16_sel;
output	switch_out15_sel;
output	switch_out14_sel;
output	switch_out13_sel;
output	switch_out12_sel;
output	switch_out11_sel;
output	switch_out10_sel;
output	switch_out09_sel;
output 	switch_out08_sel;
output 	switch_out07_sel;
output 	switch_out06_sel;
output 	switch_out05_sel;
output	switch_out04_sel;
output	switch_out03_sel;
output	switch_out02_sel;
output	switch_out01_sel;
output	switch_out00_sel;

input	task_ack;
input	g_sclk;

`protect
wire	[31:0]	switch_in_req_bus;
wire	[31:0]	switch_in_lock_bus;
wire	[31:0]	switch_in_sel_bus;
wire			general_sel_idle;
reg	[4:0]	id00,id01,id02,id03,id04,id05,id06,id07,
			id08,id09,id10,id11,id12,id13,id14,id15,
			id16,id17,id18,id19,id20,id21,id22,id23,
			id24,id25,id26,id27,id28,id29,id30,id31;
//combine the signals to bus
assign	switch_in_req_bus	= {switch_in31_req,
                               switch_in30_req,
                               switch_in29_req,
                               switch_in28_req,
                               switch_in27_req,
                               switch_in26_req,
                               switch_in25_req,
                               switch_in24_req,
                               switch_in23_req,
                               switch_in22_req,
                               switch_in21_req,
                               switch_in20_req,
                               switch_in19_req,
                               switch_in18_req,
                               switch_in17_req,
                               switch_in16_req,
							   switch_in15_req,
                               switch_in14_req,
                               switch_in13_req,
                               switch_in12_req,
                               switch_in11_req,
                               switch_in10_req,
                               switch_in09_req,
                               switch_in08_req,
                               switch_in07_req,
                               switch_in06_req,
                               switch_in05_req,
                               switch_in04_req,
                               switch_in03_req,
                               switch_in02_req,
                               switch_in01_req,
                               switch_in00_req};

assign	switch_in_lock_bus	= {switch_in31_lock,
                               switch_in30_lock,
                               switch_in29_lock,
                               switch_in28_lock,
                               switch_in27_lock,
                               switch_in26_lock,
                               switch_in25_lock,
                               switch_in24_lock,
                               switch_in23_lock,
                               switch_in22_lock,
                               switch_in21_lock,
                               switch_in20_lock,
                               switch_in19_lock,
                               switch_in18_lock,
                               switch_in17_lock,
                               switch_in16_lock,
							   switch_in15_lock,
                               switch_in14_lock,
                               switch_in13_lock,
                               switch_in12_lock,
                               switch_in11_lock,
                               switch_in10_lock,
                               switch_in09_lock,
                               switch_in08_lock,
                               switch_in07_lock,
                               switch_in06_lock,
                               switch_in05_lock,
                               switch_in04_lock,
                               switch_in03_lock,
                               switch_in02_lock,
                               switch_in01_lock,
                               switch_in00_lock};

assign	switch_in_sel_bus	= {switch_in31_sel,
                               switch_in30_sel,
                               switch_in29_sel,
                               switch_in28_sel,
                               switch_in27_sel,
                               switch_in26_sel,
                               switch_in25_sel,
                               switch_in24_sel,
                               switch_in23_sel,
                               switch_in22_sel,
                               switch_in21_sel,
                               switch_in20_sel,
                               switch_in19_sel,
                               switch_in18_sel,
                               switch_in17_sel,
                               switch_in16_sel,
							   switch_in15_sel,
                               switch_in14_sel,
                               switch_in13_sel,
                               switch_in12_sel,
                               switch_in11_sel,
                               switch_in10_sel,
                               switch_in09_sel,
                               switch_in08_sel,
                               switch_in07_sel,
                               switch_in06_sel,
                               switch_in05_sel,
                               switch_in04_sel,
                               switch_in03_sel,
                               switch_in02_sel,
                               switch_in01_sel,
                               switch_in00_sel};

assign	switch_out31_req	=	switch_in_req_bus[id31];
assign	switch_out30_req	=	switch_in_req_bus[id30];
assign	switch_out29_req	=	switch_in_req_bus[id29];
assign	switch_out28_req	=	switch_in_req_bus[id28];
assign	switch_out27_req	=	switch_in_req_bus[id27];
assign	switch_out26_req	=	switch_in_req_bus[id26];
assign	switch_out25_req	=	switch_in_req_bus[id25];
assign	switch_out24_req	=	switch_in_req_bus[id24];
assign	switch_out23_req	=	switch_in_req_bus[id23];
assign	switch_out22_req	=	switch_in_req_bus[id22];
assign	switch_out21_req	=	switch_in_req_bus[id21];
assign	switch_out20_req	=	switch_in_req_bus[id20];
assign	switch_out19_req	=	switch_in_req_bus[id19];
assign	switch_out18_req	=	switch_in_req_bus[id18];
assign	switch_out17_req	=	switch_in_req_bus[id17];
assign	switch_out16_req	=	switch_in_req_bus[id16];
assign	switch_out15_req	=	switch_in_req_bus[id15];
assign	switch_out14_req	=	switch_in_req_bus[id14];
assign	switch_out13_req	=	switch_in_req_bus[id13];
assign	switch_out12_req	=	switch_in_req_bus[id12];
assign	switch_out11_req	=	switch_in_req_bus[id11];
assign	switch_out10_req	=	switch_in_req_bus[id10];
assign	switch_out09_req	=	switch_in_req_bus[id09];
assign	switch_out08_req	=	switch_in_req_bus[id08];
assign	switch_out07_req	=	switch_in_req_bus[id07];
assign	switch_out06_req	=	switch_in_req_bus[id06];
assign	switch_out05_req	=	switch_in_req_bus[id05];
assign	switch_out04_req	=	switch_in_req_bus[id04];
assign	switch_out03_req	=	switch_in_req_bus[id03];
assign	switch_out02_req	=	switch_in_req_bus[id02];
assign	switch_out01_req	=	switch_in_req_bus[id01];
assign	switch_out00_req	=	switch_in_req_bus[id00];

assign	switch_out31_lock	=	switch_in_lock_bus[id31];
assign	switch_out30_lock   =	switch_in_lock_bus[id30];
assign	switch_out29_lock   =	switch_in_lock_bus[id29];
assign	switch_out28_lock   =	switch_in_lock_bus[id28];
assign	switch_out27_lock   =	switch_in_lock_bus[id27];
assign	switch_out26_lock   =	switch_in_lock_bus[id26];
assign	switch_out25_lock   =	switch_in_lock_bus[id25];
assign	switch_out24_lock   =	switch_in_lock_bus[id24];
assign	switch_out23_lock   =	switch_in_lock_bus[id23];
assign	switch_out22_lock   =	switch_in_lock_bus[id22];
assign	switch_out21_lock   =	switch_in_lock_bus[id21];
assign	switch_out20_lock   =	switch_in_lock_bus[id20];
assign	switch_out19_lock   =	switch_in_lock_bus[id19];
assign	switch_out18_lock   =	switch_in_lock_bus[id18];
assign	switch_out17_lock   =	switch_in_lock_bus[id17];
assign	switch_out16_lock   =	switch_in_lock_bus[id16];
assign	switch_out15_lock	=	switch_in_lock_bus[id15];
assign	switch_out14_lock   =	switch_in_lock_bus[id14];
assign	switch_out13_lock   =	switch_in_lock_bus[id13];
assign	switch_out12_lock   =	switch_in_lock_bus[id12];
assign	switch_out11_lock   =	switch_in_lock_bus[id11];
assign	switch_out10_lock   =	switch_in_lock_bus[id10];
assign	switch_out09_lock   =	switch_in_lock_bus[id09];
assign	switch_out08_lock   =	switch_in_lock_bus[id08];
assign	switch_out07_lock   =	switch_in_lock_bus[id07];
assign	switch_out06_lock   =	switch_in_lock_bus[id06];
assign	switch_out05_lock   =	switch_in_lock_bus[id05];
assign	switch_out04_lock   =	switch_in_lock_bus[id04];
assign	switch_out03_lock   =	switch_in_lock_bus[id03];
assign	switch_out02_lock   =	switch_in_lock_bus[id02];
assign	switch_out01_lock   =	switch_in_lock_bus[id01];
assign	switch_out00_lock   =	switch_in_lock_bus[id00];

assign	switch_out31_sel	=	switch_in_sel_bus[id31];
assign	switch_out30_sel	=	switch_in_sel_bus[id30];
assign	switch_out29_sel	=	switch_in_sel_bus[id29];
assign	switch_out28_sel	=	switch_in_sel_bus[id28];
assign	switch_out27_sel	=	switch_in_sel_bus[id27];
assign	switch_out26_sel	=	switch_in_sel_bus[id26];
assign	switch_out25_sel	=	switch_in_sel_bus[id25];
assign	switch_out24_sel	=	switch_in_sel_bus[id24];
assign	switch_out23_sel	=	switch_in_sel_bus[id23];
assign	switch_out22_sel	=	switch_in_sel_bus[id22];
assign	switch_out21_sel	=	switch_in_sel_bus[id21];
assign	switch_out20_sel	=	switch_in_sel_bus[id20];
assign	switch_out19_sel	=	switch_in_sel_bus[id19];
assign	switch_out18_sel	=	switch_in_sel_bus[id18];
assign	switch_out17_sel	=	switch_in_sel_bus[id17];
assign	switch_out16_sel	=	switch_in_sel_bus[id16];
assign	switch_out15_sel	=	switch_in_sel_bus[id15];
assign	switch_out14_sel	=	switch_in_sel_bus[id14];
assign	switch_out13_sel	=	switch_in_sel_bus[id13];
assign	switch_out12_sel	=	switch_in_sel_bus[id12];
assign	switch_out11_sel	=	switch_in_sel_bus[id11];
assign	switch_out10_sel	=	switch_in_sel_bus[id10];
assign	switch_out09_sel	=	switch_in_sel_bus[id09];
assign	switch_out08_sel	=	switch_in_sel_bus[id08];
assign	switch_out07_sel	=	switch_in_sel_bus[id07];
assign	switch_out06_sel	=	switch_in_sel_bus[id06];
assign	switch_out05_sel	=	switch_in_sel_bus[id05];
assign	switch_out04_sel	=	switch_in_sel_bus[id04];
assign	switch_out03_sel	=	switch_in_sel_bus[id03];
assign	switch_out02_sel	=	switch_in_sel_bus[id02];
assign	switch_out01_sel	=	switch_in_sel_bus[id01];
assign	switch_out00_sel	=	switch_in_sel_bus[id00];

assign	general_sel_idle	=	~(|switch_in_sel_bus);
reg		ack_back;
initial ack_back = 0;
always @(posedge g_sclk)
	if (task_ack)
		ack_back <= #udly 1;
	else
		ack_back <= #udly 0;
//set the idxx registers value
initial	begin
	id00	=	5'h00;	//0
	id01	=	5'h01;   //1
	id02	=	5'h02;   //2
	id03	=	5'h03;   //3
	id04	=	5'h04;   //4
	id05	=	5'h05;   //5
	id06	=	5'h06;   //6
	id07	=	5'h07;   //7
	id08	=	5'h08;   //8
	id09	=	5'h09;   //9
	id10	=	5'h0a;   //10
	id11	=	5'h0b;   //11
	id12	=	5'h0c;   //12
	id13	=	5'h0d;   //13
	id14	=	5'h0e;   //14
	id15	=	5'h0f;   //15
	id16	=	5'h10;	//16
	id17	=	5'h11;   //17
	id18	=	5'h12;   //18
	id19	=	5'h13;   //19
	id20	=	5'h14;   //20
	id21	=	5'h15;   //21
	id22	=	5'h16;   //22
	id23	=	5'h17;   //23
	id24	=	5'h18;   //24
	id25	=	5'h19;   //25
	id26	=	5'h1a;   //26
	id27	=	5'h1b;   //27
	id28	=	5'h1c;   //28
	id29	=	5'h1d;   //29
	id30	=	5'h1e;   //30
	id31	=	5'h1f;   //31
	end

`endprotect

task	priority_set;
input	[4:0]	set_id00;
input	[4:0]	set_id01;
input	[4:0]	set_id02;
input	[4:0]	set_id03;
input	[4:0]	set_id04;
input	[4:0]	set_id05;
input	[4:0]	set_id06;
input	[4:0]	set_id07;
input	[4:0]	set_id08;
input	[4:0]	set_id09;
input	[4:0]	set_id10;
input	[4:0]	set_id11;
input	[4:0]	set_id12;
input	[4:0]	set_id13;
input	[4:0]	set_id14;
input	[4:0]	set_id15;
input	[4:0]	set_id16;
input	[4:0]	set_id17;
input	[4:0]	set_id18;
input	[4:0]	set_id19;
input	[4:0]	set_id20;
input	[4:0]	set_id21;
input	[4:0]	set_id22;
input	[4:0]	set_id23;
input	[4:0]	set_id24;
input	[4:0]	set_id25;
input	[4:0]	set_id26;
input	[4:0]	set_id27;
input	[4:0]	set_id28;
input	[4:0]	set_id29;
input	[4:0]	set_id30;
input	[4:0]	set_id31;
begin
	if (general_sel_idle) begin
		id00	= set_id00;
		id01	= set_id01;
		id02	= set_id02;
		id03	= set_id03;
		id04	= set_id04;
		id05	= set_id05;
		id06	= set_id06;
		id07	= set_id07;
		id08	= set_id08;
		id09	= set_id09;
		id10	= set_id10;
		id11	= set_id11;
		id12	= set_id12;
		id13	= set_id13;
		id14	= set_id14;
		id15	= set_id15;
		id16	= set_id16;
		id17	= set_id17;
		id18	= set_id18;
		id19	= set_id19;
		id20	= set_id20;
		id21	= set_id21;
		id22	= set_id22;
		id23	= set_id23;
		id24	= set_id24;
		id25	= set_id25;
		id26	= set_id26;
		id27	= set_id27;
		id28	= set_id28;
		id29	= set_id29;
		id30	= set_id30;
		id31	= set_id31;
	end
	else begin
		@(posedge ack_back);
		id00	= set_id00;
		id01	= set_id01;
		id02	= set_id02;
		id03	= set_id03;
		id04	= set_id04;
		id05	= set_id05;
		id06	= set_id06;
		id07	= set_id07;
		id08	= set_id08;
		id09	= set_id09;
		id10	= set_id10;
		id11	= set_id11;
		id12	= set_id12;
		id13	= set_id13;
		id14	= set_id14;
		id15	= set_id15;
		id16	= set_id16;
		id17	= set_id17;
		id18	= set_id18;
		id19	= set_id19;
		id20	= set_id20;
		id21	= set_id21;
		id22	= set_id22;
		id23	= set_id23;
		id24	= set_id24;
		id25	= set_id25;
		id26	= set_id26;
		id27	= set_id27;
		id28	= set_id28;
		id29	= set_id29;
		id30	= set_id30;
		id31	= set_id31;
	end
end
endtask

endmodule

