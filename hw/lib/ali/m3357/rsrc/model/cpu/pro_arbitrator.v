/******************************************************************************
          (c) copyrights 1997-2000. All rights reserved

           T-Square Design, Inc.

          Company confidential and Properietary information.
          This information may not be disclosed to unauthorized
          individual.
******************************************************************************/
/*********************************************************************
//File 	: 	pro_arbitrator.v
//Description:
//			used to arbitrate the request one interrupt processing thread
//			and 16 processing threads.
//Algorithm:
//			1). the interrupt request has the highest priority. when the
//			interrupt occurs, the arbitrator will give the right to interrupt
//			request in no case. after interrupt request disactive, the arbitrator
//			will return the previous status before processing the interrupt
//			2). else 16 threads' request have the same priority,
//			3). after reset, the default ring order is: pro00 -> pro01 -> pro02
//			-> pro03 -> pro04 -> pro05 -> pro06 -> pro07 -> pro08 -> pro09
//			-> pro10 -> pro11 -> pro12 -> pro13 -> pro14 -> pro15
//			4). support the thread lock. it means make the ring not change for
//			some operation cycle.
//Author: 	Yuky
//Date	: 	2002-05-13	initial version
			2002-07-25	extend to 32 general channels
*********************************************************************/
module pro_arbitrator(
	u_pro31_req,
	u_pro30_req,
	u_pro29_req,
	u_pro28_req,
	u_pro27_req,
	u_pro26_req,
	u_pro25_req,
 	u_pro24_req,
 	u_pro23_req,
 	u_pro22_req,
 	u_pro21_req,
	u_pro20_req,
	u_pro19_req,
	u_pro18_req,
	u_pro17_req,
	u_pro16_req,

	u_pro15_req,
	u_pro14_req,
	u_pro13_req,
	u_pro12_req,
	u_pro11_req,
	u_pro10_req,
	u_pro09_req,
 	u_pro08_req,
 	u_pro07_req,
 	u_pro06_req,
 	u_pro05_req,
	u_pro04_req,
	u_pro03_req,
	u_pro02_req,
	u_pro01_req,
	u_pro00_req,
	u_int_req,

	u_int_lock,
	u_pro31_lock,
	u_pro30_lock,
	u_pro29_lock,
	u_pro28_lock,
	u_pro27_lock,
	u_pro26_lock,
	u_pro25_lock,
 	u_pro24_lock,
 	u_pro23_lock,
 	u_pro22_lock,
 	u_pro21_lock,
	u_pro20_lock,
	u_pro19_lock,
	u_pro18_lock,
	u_pro17_lock,
	u_pro16_lock,

	u_pro15_lock,
	u_pro14_lock,
	u_pro13_lock,
	u_pro12_lock,
	u_pro11_lock,
	u_pro10_lock,
	u_pro09_lock,
 	u_pro08_lock,
 	u_pro07_lock,
 	u_pro06_lock,
 	u_pro05_lock,
	u_pro04_lock,
	u_pro03_lock,
	u_pro02_lock,
	u_pro01_lock,
	u_pro00_lock,

	task_ack,
	int_flag,

	u_pro31_sel,
	u_pro30_sel,
	u_pro29_sel,
	u_pro28_sel,
	u_pro27_sel,
	u_pro26_sel,
	u_pro25_sel,
 	u_pro24_sel,
 	u_pro23_sel,
 	u_pro22_sel,
 	u_pro21_sel,
	u_pro20_sel,
	u_pro19_sel,
	u_pro18_sel,
	u_pro17_sel,
	u_pro16_sel,

	u_pro15_sel,
	u_pro14_sel,
	u_pro13_sel,
	u_pro12_sel,
	u_pro11_sel,
	u_pro10_sel,
	u_pro09_sel,
 	u_pro08_sel,
 	u_pro07_sel,
 	u_pro06_sel,
 	u_pro05_sel,
	u_pro04_sel,
	u_pro03_sel,
	u_pro02_sel,
	u_pro01_sel,
	u_pro00_sel,
	u_int_sel,

	g_sclk,
	g_rst_
	);
parameter udly = 0.9;

input	 	u_pro31_req;
input		u_pro30_req;
input		u_pro29_req;
input		u_pro28_req;
input		u_pro27_req;
input		u_pro26_req;
input		u_pro25_req;
input	 	u_pro24_req;
input	 	u_pro23_req;
input	 	u_pro22_req;
input	 	u_pro21_req;
input		u_pro20_req;
input		u_pro19_req;
input		u_pro18_req;
input		u_pro17_req;
input		u_pro16_req;

input	 	u_pro15_req;
input		u_pro14_req;
input		u_pro13_req;
input		u_pro12_req;
input		u_pro11_req;
input		u_pro10_req;
input		u_pro09_req;
input	 	u_pro08_req;
input	 	u_pro07_req;
input	 	u_pro06_req;
input	 	u_pro05_req;
input		u_pro04_req;
input		u_pro03_req;
input		u_pro02_req;
input		u_pro01_req;
input		u_pro00_req;

input		u_int_req;
input		task_ack;
input		int_flag;
input		u_int_lock;

input	 	u_pro31_lock;
input		u_pro30_lock;
input		u_pro29_lock;
input		u_pro28_lock;
input		u_pro27_lock;
input		u_pro26_lock;
input		u_pro25_lock;
input	 	u_pro24_lock;
input	 	u_pro23_lock;
input	 	u_pro22_lock;
input	 	u_pro21_lock;
input		u_pro20_lock;
input		u_pro19_lock;
input		u_pro18_lock;
input		u_pro17_lock;
input		u_pro16_lock;

input	 	u_pro15_lock;
input		u_pro14_lock;
input		u_pro13_lock;
input		u_pro12_lock;
input		u_pro11_lock;
input		u_pro10_lock;
input		u_pro09_lock;
input	 	u_pro08_lock;
input	 	u_pro07_lock;
input	 	u_pro06_lock;
input	 	u_pro05_lock;
input		u_pro04_lock;
input		u_pro03_lock;
input		u_pro02_lock;
input		u_pro01_lock;
input		u_pro00_lock;

output	 	u_pro31_sel;
output		u_pro30_sel;
output		u_pro29_sel;
output		u_pro28_sel;
output		u_pro27_sel;
output		u_pro26_sel;
output		u_pro25_sel;
output	 	u_pro24_sel;
output	 	u_pro23_sel;
output	 	u_pro22_sel;
output	 	u_pro21_sel;
output		u_pro20_sel;
output		u_pro19_sel;
output		u_pro18_sel;
output		u_pro17_sel;
output		u_pro16_sel;

output	 	u_pro15_sel;
output		u_pro14_sel;
output		u_pro13_sel;
output		u_pro12_sel;
output		u_pro11_sel;
output		u_pro10_sel;
output		u_pro09_sel;
output	 	u_pro08_sel;
output	 	u_pro07_sel;
output	 	u_pro06_sel;
output	 	u_pro05_sel;
output		u_pro04_sel;
output		u_pro03_sel;
output		u_pro02_sel;
output		u_pro01_sel;
output		u_pro00_sel;
output		u_int_sel;

input		g_sclk;
input		g_rst_;

`protect
reg	[31:0]	pass_ring;
reg	[31:0]	pass_ring_latch;
reg	[31:0]	sel_bus_latch;

reg		 	u_pro31_sel;
reg			u_pro30_sel;
reg			u_pro29_sel;
reg			u_pro28_sel;
reg			u_pro27_sel;
reg			u_pro26_sel;
reg			u_pro25_sel;
reg		 	u_pro24_sel;
reg		 	u_pro23_sel;
reg		 	u_pro22_sel;
reg		 	u_pro21_sel;
reg			u_pro20_sel;
reg			u_pro19_sel;
reg			u_pro18_sel;
reg			u_pro17_sel;
reg			u_pro16_sel;

reg		 	u_pro15_sel;
reg			u_pro14_sel;
reg			u_pro13_sel;
reg			u_pro12_sel;
reg			u_pro11_sel;
reg			u_pro10_sel;
reg			u_pro09_sel;
reg		 	u_pro08_sel;
reg		 	u_pro07_sel;
reg		 	u_pro06_sel;
reg		 	u_pro05_sel;
reg			u_pro04_sel;
reg			u_pro03_sel;
reg			u_pro02_sel;
reg			u_pro01_sel;
reg			u_pro00_sel;
reg			u_int_sel;

wire	[31:0]	req_bus;
assign	req_bus	=	{u_pro31_req,	u_pro30_req,	u_pro29_req, 	u_pro28_req,
					 u_pro27_req,	u_pro26_req,	u_pro25_req, 	u_pro24_req,
					 u_pro23_req,	u_pro22_req,	u_pro21_req,	u_pro20_req,
					 u_pro19_req,	u_pro18_req,	u_pro17_req,	u_pro16_req,
					 u_pro15_req,	u_pro14_req,	u_pro13_req, 	u_pro12_req,
					 u_pro11_req,	u_pro10_req,	u_pro09_req, 	u_pro08_req,
					 u_pro07_req,	u_pro06_req,	u_pro05_req,	u_pro04_req,
					 u_pro03_req,	u_pro02_req,	u_pro01_req,	u_pro00_req
					 };
wire	[31:0] 	sel_bus;
assign	sel_bus =	{u_pro31_sel,	u_pro30_sel,	u_pro29_sel, 	u_pro28_sel,
					 u_pro27_sel,	u_pro26_sel,	u_pro25_sel, 	u_pro24_sel,
					 u_pro23_sel,	u_pro22_sel,	u_pro21_sel,	u_pro20_sel,
					 u_pro19_sel,	u_pro18_sel,	u_pro17_sel,	u_pro16_sel,
					 u_pro15_sel,	u_pro14_sel,	u_pro13_sel, 	u_pro12_sel,
					 u_pro11_sel,	u_pro10_sel,	u_pro09_sel, 	u_pro08_sel,
					 u_pro07_sel,	u_pro06_sel,	u_pro05_sel,	u_pro04_sel,
					 u_pro03_sel,	u_pro02_sel,	u_pro01_sel,	u_pro00_sel
					 };
wire	common_req = |req_bus;
wire	common_sel = |sel_bus;
wire	[31:0] active_req = (req_bus & sel_bus);
wire	common_idle = ~(|active_req);
wire	pass_ring_nouse = ~(|pass_ring);
//save the pass_ring status during interrupt occur
reg		u_int_lock_latch;
always @(posedge g_sclk or negedge g_rst_)
	if (~g_rst_)
		u_int_lock_latch <= #udly 1'b0;
	else
		u_int_lock_latch <= #udly u_int_lock;

always @(posedge int_flag or negedge g_rst_)
	if (~g_rst_) begin
		pass_ring_latch <= #udly 31'h0001;
		sel_bus_latch	<= #udly 31'h0001;
	end else if (~u_int_lock_latch) begin
		pass_ring_latch <= #udly pass_ring;//save the current pass_ring status
		sel_bus_latch	<= #udly sel_bus;//save the current sel_bus
	end

//arbitrator FSM
always @(posedge g_sclk or negedge g_rst_)
	if (~g_rst_)
		judge_rst;
	else if (u_int_req) begin //judge int request
			if (task_ack & u_int_sel)	begin	//return to int previous status
				if (u_int_lock)
					u_int_sel		<= #udly 1'b1;
				else
					int_pro_return;
			end
			else if (task_ack & common_sel)	begin //wait the last common request end
				int_pro_set;
			end
			else if ( common_idle & ~u_int_sel) //start the int select
				int_pro_set;
	end //end judge int request
	else if (~u_int_lock) begin//u_int_lock make the common processing can be start
		if (pass_ring_nouse)
			int_pro_return;	//interrupt return
		else
		case (pass_ring)
		32'h0001:begin
				if (req_bus[0]) begin
					if (task_ack)	begin
						if (u_pro00_lock)
							u_pro00_sel		<= #udly 1'b1;
						else
							judge_00;
					end else
						u_pro00_sel		<= #udly 1'b1;
				end else
					judge_00;

		end

		32'h0002:begin
				if (req_bus[1]) begin
					if (task_ack)	begin
						if (u_pro01_lock)
							u_pro01_sel		<= #udly 1'b1;
						else begin
							judge_01;
						end
					end else
						u_pro01_sel		<= #udly 1'b1;
				end else
					judge_01;
		end

		32'h0004:begin
				if (req_bus[2]) begin
					if (task_ack)	begin
						if (u_pro02_lock)
							u_pro02_sel		<= #udly 1'b1;
						else begin
							judge_02;
						end
					end	else
						u_pro02_sel		<= #udly 1'b1;
				end else
					judge_02;
		end

		32'h0008:begin
				if (req_bus[3]) begin
					if (task_ack)	begin
						if (u_pro03_lock)
							u_pro03_sel		<= #udly 1'b1;
						else begin
							judge_03;
						end
					end	else
						u_pro03_sel		<= #udly 1'b1;
				end else
					judge_03;
		end

		32'h0010:begin
				if (req_bus[4]) begin
					if (task_ack)	begin
						if (u_pro04_lock)
							u_pro04_sel		<= #udly 1'b1;
						else begin
							judge_04;
						end
					end	else
						u_pro04_sel		<= #udly 1'b1;
				end else
					judge_04;
		end

		32'h0020:begin
				if (req_bus[5]) begin
					if (task_ack)	begin
						if (u_pro05_lock)
							u_pro05_sel		<= #udly 1'b1;
						else begin
							judge_05;
						end
					end	else
						u_pro05_sel		<= #udly 1'b1;
				end else
					judge_05;
		end

		32'h0040:	begin
				if (req_bus[6]) begin
					if (task_ack)	begin
						if (u_pro06_lock)
							u_pro06_sel		<= #udly 1'b1;
						else begin
							judge_06;
						end
					end	else
						u_pro06_sel		<= #udly 1'b1;
				end else
					judge_06;
		end

		32'h0080:begin
				if (req_bus[7]) begin
					if (task_ack)	begin
						if (u_pro07_lock)
							u_pro07_sel		<= #udly 1'b1;
						else begin
							judge_07;
						end
					end	else
						u_pro07_sel		<= #udly 1'b1;
				end else
					judge_07;
		end


		32'h0100:	begin
				if (req_bus[8]) begin
					if (task_ack)	begin
						if (u_pro08_lock)
							u_pro08_sel		<= #udly 1'b1;
						else begin
							judge_08;
						end
					end	else
						u_pro08_sel		<= #udly 1'b1;
				end else
					judge_08;
		end

		32'h0200:	begin
				if (req_bus[9]) begin
					if (task_ack)	begin
						if (u_pro09_lock)
							u_pro09_sel		<= #udly 1'b1;
						else begin
							judge_09;
						end
					end	else
						u_pro09_sel		<= #udly 1'b1;
				end else
					judge_09;
		end

		32'h0400:	begin
				if (req_bus[10]) begin
					if (task_ack)	begin
						if (u_pro10_lock)
							u_pro10_sel		<= #udly 1'b1;
						else begin
							judge_10;
						end
					end	else
						u_pro10_sel		<= #udly 1'b1;
				end else
					judge_10;
		end

		32'h0800:	begin
				if (req_bus[11]) begin
					if (task_ack)	begin
						if (u_pro11_lock)
							u_pro11_sel		<= #udly 1'b1;
						else begin
							judge_11;
						end
					end	else
						u_pro11_sel		<= #udly 1'b1;
				end else
					judge_11;
		end

		32'h1000:	begin
				if (req_bus[12]) begin
					if (task_ack)	begin
						if (u_pro12_lock)
							u_pro12_sel		<= #udly 1'b1;
						else begin
							judge_12;
						end
					end	else
						u_pro12_sel		<= #udly 1'b1;
				end else
					judge_12;
		end

		32'h2000:	begin
				if (req_bus[13]) begin
					if (task_ack)	begin
						if (u_pro13_lock)
							u_pro13_sel		<= #udly 1'b1;
						else begin
							judge_13;
						end
					end	else
						u_pro13_sel		<= #udly 1'b1;
				end else
					judge_13;
		end

		32'h4000:	begin
				if (req_bus[14]) begin
					if (task_ack)	begin
						if (u_pro14_lock)
							u_pro14_sel		<= #udly 1'b1;
						else begin
							judge_14;
						end
					end	else
						u_pro14_sel		<= #udly 1'b1;
				end else
					judge_14;
		end

		32'h8000:	begin
				if (req_bus[15]) begin
					if (task_ack)	begin
						if (u_pro15_lock)
							u_pro15_sel		<= #udly 1'b1;
						else begin
							judge_15;
						end
					end	else
						u_pro15_sel		<= #udly 1'b1;
				end else
					judge_15;
		end

		32'h0001_0000:	begin
				if (req_bus[16]) begin
					if (task_ack)	begin
						if (u_pro16_lock)
							u_pro16_sel		<= #udly 1'b1;
						else begin
							judge_16;
						end
					end	else
						u_pro16_sel		<= #udly 1'b1;
				end else
					judge_16;
		end

		32'h0002_0000:	begin
				if (req_bus[17]) begin
					if (task_ack)	begin
						if (u_pro17_lock)
							u_pro17_sel		<= #udly 1'b1;
						else begin
							judge_17;
						end
					end	else
						u_pro17_sel		<= #udly 1'b1;
				end else
					judge_17;
		end

		32'h0004_0000:	begin
				if (req_bus[18]) begin
					if (task_ack)	begin
						if (u_pro18_lock)
							u_pro18_sel		<= #udly 1'b1;
						else begin
							judge_18;
						end
					end	else
						u_pro18_sel		<= #udly 1'b1;
				end else
					judge_18;
		end

		32'h0008_0000:	begin
				if (req_bus[19]) begin
					if (task_ack)	begin
						if (u_pro19_lock)
							u_pro19_sel		<= #udly 1'b1;
						else begin
							judge_19;
						end
					end	else
						u_pro19_sel		<= #udly 1'b1;
				end else
					judge_19;
		end

		32'h0010_0000:	begin
				if (req_bus[20]) begin
					if (task_ack)	begin
						if (u_pro20_lock)
							u_pro20_sel		<= #udly 1'b1;
						else begin
							judge_20;
						end
					end	else
						u_pro20_sel		<= #udly 1'b1;
				end else
					judge_20;
		end

		32'h0020_0000:	begin
				if (req_bus[21]) begin
					if (task_ack)	begin
						if (u_pro21_lock)
							u_pro21_sel		<= #udly 1'b1;
						else begin
							judge_21;
						end
					end	else
						u_pro21_sel		<= #udly 1'b1;
				end else
					judge_21;
		end

		32'h0040_0000:	begin
				if (req_bus[22]) begin
					if (task_ack)	begin
						if (u_pro22_lock)
							u_pro22_sel		<= #udly 1'b1;
						else begin
							judge_22;
						end
					end	else
						u_pro22_sel		<= #udly 1'b1;
				end else
					judge_22;
		end

		32'h0080_0000:	begin
				if (req_bus[23]) begin
					if (task_ack)	begin
						if (u_pro23_lock)
							u_pro23_sel		<= #udly 1'b1;
						else begin
							judge_23;
						end
					end	else
						u_pro23_sel		<= #udly 1'b1;
				end else
					judge_23;
		end

		32'h0100_0000:	begin
				if (req_bus[24]) begin
					if (task_ack)	begin
						if (u_pro24_lock)
							u_pro24_sel		<= #udly 1'b1;
						else begin
							judge_24;
						end
					end	else
						u_pro24_sel		<= #udly 1'b1;
				end else
					judge_24;
		end

		32'h0200_0000:	begin
				if (req_bus[25]) begin
					if (task_ack)	begin
						if (u_pro25_lock)
							u_pro25_sel		<= #udly 1'b1;
						else begin
							judge_25;
						end
					end	else
						u_pro25_sel		<= #udly 1'b1;
				end else
					judge_25;
		end

		32'h0400_0000:	begin
				if (req_bus[26]) begin
					if (task_ack)	begin
						if (u_pro26_lock)
							u_pro26_sel		<= #udly 1'b1;
						else begin
							judge_26;
						end
					end	else
						u_pro26_sel		<= #udly 1'b1;
				end else
					judge_26;
		end

		32'h0800_0000:	begin
				if (req_bus[27]) begin
					if (task_ack)	begin
						if (u_pro27_lock)
							u_pro27_sel		<= #udly 1'b1;
						else begin
							judge_27;
						end
					end	else
						u_pro27_sel		<= #udly 1'b1;
				end else
					judge_27;
		end

		32'h1000_0000:	begin
				if (req_bus[28]) begin
					if (task_ack)	begin
						if (u_pro28_lock)
							u_pro28_sel		<= #udly 1'b1;
						else begin
							judge_28;
						end
					end	else
						u_pro28_sel		<= #udly 1'b1;
				end else
					judge_28;
		end

		32'h2000_0000:	begin
				if (req_bus[29]) begin
					if (task_ack)	begin
						if (u_pro29_lock)
							u_pro29_sel		<= #udly 1'b1;
						else begin
							judge_29;
						end
					end	else
						u_pro29_sel		<= #udly 1'b1;
				end else
					judge_29;
		end

		32'h4000_0000:	begin
				if (req_bus[30]) begin
					if (task_ack)	begin
						if (u_pro30_lock)
							u_pro30_sel		<= #udly 1'b1;
						else begin
							judge_30;
						end
					end	else
						u_pro30_sel		<= #udly 1'b1;
				end else
					judge_30;
		end

		32'h8000_0000:	begin
				if (req_bus[31]) begin
					if (task_ack)	begin
						if (u_pro31_lock)
							u_pro31_sel		<= #udly 1'b1;
						else begin
							judge_31;
						end
					end	else
						u_pro31_sel		<= #udly 1'b1;
				end else
					judge_31;
		end
		endcase

	end //end to judge u_int_lock
//########## the FSM End ###########

//######################################################
//#### the tasks area for the FSM invoking #############
//######################################################
task judge_rst;
begin
		pass_ring 		<= #udly 31'h0001;
		u_pro31_sel		<= #udly 1'b0;
		u_pro30_sel		<= #udly 1'b0;
		u_pro29_sel		<= #udly 1'b0;
		u_pro28_sel		<= #udly 1'b0;
		u_pro27_sel		<= #udly 1'b0;
		u_pro26_sel		<= #udly 1'b0;
		u_pro25_sel		<= #udly 1'b0;
		u_pro24_sel		<= #udly 1'b0;
		u_pro23_sel		<= #udly 1'b0;
		u_pro22_sel		<= #udly 1'b0;
		u_pro21_sel		<= #udly 1'b0;
		u_pro20_sel		<= #udly 1'b0;
		u_pro19_sel		<= #udly 1'b0;
		u_pro18_sel		<= #udly 1'b0;
		u_pro17_sel		<= #udly 1'b0;
		u_pro16_sel		<= #udly 1'b0;
		u_pro15_sel		<= #udly 1'b0;
		u_pro14_sel		<= #udly 1'b0;
		u_pro13_sel		<= #udly 1'b0;
		u_pro12_sel		<= #udly 1'b0;
		u_pro11_sel		<= #udly 1'b0;
		u_pro10_sel		<= #udly 1'b0;
		u_pro09_sel		<= #udly 1'b0;
		u_pro08_sel		<= #udly 1'b0;
		u_pro07_sel		<= #udly 1'b0;
		u_pro06_sel		<= #udly 1'b0;
		u_pro05_sel		<= #udly 1'b0;
		u_pro04_sel		<= #udly 1'b0;
		u_pro03_sel		<= #udly 1'b0;
		u_pro02_sel		<= #udly 1'b0;
		u_pro01_sel		<= #udly 1'b0;
		u_pro00_sel		<= #udly 1'b0;
		u_int_sel		<= #udly 1'b0;
end
endtask

task int_pro_set;
begin
		u_int_sel		<= #udly 1'b1;
		u_pro31_sel		<= #udly 1'b0;
		u_pro30_sel		<= #udly 1'b0;
		u_pro29_sel		<= #udly 1'b0;
		u_pro28_sel		<= #udly 1'b0;
		u_pro27_sel		<= #udly 1'b0;
		u_pro26_sel		<= #udly 1'b0;
		u_pro25_sel		<= #udly 1'b0;
		u_pro24_sel		<= #udly 1'b0;
		u_pro23_sel		<= #udly 1'b0;
		u_pro22_sel		<= #udly 1'b0;
		u_pro21_sel		<= #udly 1'b0;
		u_pro20_sel		<= #udly 1'b0;
		u_pro19_sel		<= #udly 1'b0;
		u_pro18_sel		<= #udly 1'b0;
		u_pro17_sel		<= #udly 1'b0;
		u_pro16_sel		<= #udly 1'b0;
		u_pro15_sel		<= #udly 1'b0;
		u_pro14_sel		<= #udly 1'b0;
		u_pro13_sel		<= #udly 1'b0;
		u_pro12_sel		<= #udly 1'b0;
		u_pro11_sel		<= #udly 1'b0;
		u_pro10_sel		<= #udly 1'b0;
		u_pro09_sel		<= #udly 1'b0;
 		u_pro08_sel		<= #udly 1'b0;
 		u_pro07_sel		<= #udly 1'b0;
 		u_pro06_sel		<= #udly 1'b0;
 		u_pro05_sel		<= #udly 1'b0;
		u_pro04_sel		<= #udly 1'b0;
		u_pro03_sel		<= #udly 1'b0;
		u_pro02_sel		<= #udly 1'b0;
		u_pro01_sel		<= #udly 1'b0;
		u_pro00_sel		<= #udly 1'b0;
		pass_ring 		<= #udly 32'h0000;//no use pass_ring value
end
endtask

task int_pro_return;
begin
		u_int_sel		<= #udly 	1'b0;
		u_pro31_sel		<= #udly	sel_bus_latch[31];
		u_pro30_sel		<= #udly	sel_bus_latch[30];
		u_pro29_sel		<= #udly    sel_bus_latch[29];
		u_pro28_sel		<= #udly    sel_bus_latch[28];
		u_pro27_sel		<= #udly    sel_bus_latch[27];
		u_pro26_sel		<= #udly    sel_bus_latch[26];
		u_pro25_sel		<= #udly    sel_bus_latch[25];
		u_pro24_sel		<= #udly    sel_bus_latch[24];
		u_pro23_sel		<= #udly    sel_bus_latch[23];
		u_pro22_sel		<= #udly    sel_bus_latch[22];
		u_pro21_sel		<= #udly    sel_bus_latch[21];
		u_pro20_sel		<= #udly    sel_bus_latch[20];
		u_pro19_sel		<= #udly    sel_bus_latch[19];
		u_pro18_sel		<= #udly    sel_bus_latch[18];
		u_pro17_sel		<= #udly    sel_bus_latch[17];
		u_pro16_sel		<= #udly    sel_bus_latch[16];
		u_pro15_sel		<= #udly    sel_bus_latch[15];
		u_pro14_sel		<= #udly    sel_bus_latch[14];
		u_pro13_sel		<= #udly    sel_bus_latch[13];
		u_pro12_sel		<= #udly    sel_bus_latch[12];
		u_pro11_sel		<= #udly    sel_bus_latch[11];
		u_pro10_sel		<= #udly    sel_bus_latch[10];
		u_pro09_sel		<= #udly    sel_bus_latch[09];
		u_pro08_sel		<= #udly    sel_bus_latch[08];
		u_pro07_sel		<= #udly    sel_bus_latch[07];
		u_pro06_sel		<= #udly    sel_bus_latch[06];
		u_pro05_sel		<= #udly    sel_bus_latch[05];
		u_pro04_sel		<= #udly    sel_bus_latch[04];
		u_pro03_sel		<= #udly    sel_bus_latch[03];
		u_pro02_sel		<= #udly    sel_bus_latch[02];
		u_pro01_sel		<= #udly    sel_bus_latch[01];
		u_pro00_sel		<= #udly    sel_bus_latch[00];
		pass_ring 		<= #udly	pass_ring_latch;
end
endtask

task judge_00;
begin
	casex (req_bus)
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x:
				begin
					u_pro01_sel		<= #udly 1'b1;
					u_pro00_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx:
				begin
					u_pro02_sel		<= #udly 1'b1;
					u_pro00_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx:
				begin
					u_pro03_sel		<= #udly 1'b1;
					u_pro00_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx:
				begin
					u_pro04_sel		<= #udly 1'b1;
					u_pro00_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx:
				begin
					u_pro05_sel		<= #udly 1'b1;
					u_pro00_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx:
				begin
					u_pro06_sel		<= #udly 1'b1;
					u_pro00_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx:
				begin
					u_pro07_sel		<= #udly 1'b1;
					u_pro00_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx:
				begin
					u_pro08_sel		<= #udly 1'b1;
					u_pro00_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx:
				begin
					u_pro09_sel		<= #udly 1'b1;
					u_pro00_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx:
				begin
					u_pro10_sel		<= #udly 1'b1;
					u_pro00_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx:
				begin
					u_pro11_sel		<= #udly 1'b1;
					u_pro00_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx:
				begin
					u_pro12_sel		<= #udly 1'b1;
					u_pro00_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx:
				begin
					u_pro13_sel		<= #udly 1'b1;
					u_pro00_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx:
				begin
					u_pro14_sel		<= #udly 1'b1;
					u_pro00_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx:
				begin
					u_pro15_sel		<= #udly 1'b1;
					u_pro00_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h8000;
				end

			//extend to 32
			32'bxxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro16_sel		<= #udly 1'b1;
					u_pro00_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001_0000;
				end
			32'bxxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro17_sel		<= #udly 1'b1;
					u_pro00_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002_0000;
				end
			32'bxxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro18_sel		<= #udly 1'b1;
					u_pro00_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004_0000;
				end
			32'bxxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro19_sel		<= #udly 1'b1;
					u_pro00_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008_0000;
				end
			32'bxxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro20_sel		<= #udly 1'b1;
					u_pro00_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010_0000;
				end
			32'bxxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro21_sel		<= #udly 1'b1;
					u_pro00_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020_0000;
				end
			32'bxxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro22_sel		<= #udly 1'b1;
					u_pro00_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040_0000;
				end
			32'bxxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro23_sel		<= #udly 1'b1;
					u_pro00_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080_0000;
				end
			32'bxxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro24_sel		<= #udly 1'b1;
					u_pro00_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100_0000;
				end
			32'bxxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro25_sel		<= #udly 1'b1;
					u_pro00_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200_0000;
				end
			32'bxxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro26_sel		<= #udly 1'b1;
					u_pro00_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400_0000;
				end
			32'bxxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro27_sel		<= #udly 1'b1;
					u_pro00_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800_0000;
				end
			32'bxxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro28_sel		<= #udly 1'b1;
					u_pro00_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000_0000;
				end
			32'bxx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro29_sel		<= #udly 1'b1;
					u_pro00_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000_0000;
				end
			32'bx1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro30_sel		<= #udly 1'b1;
					u_pro00_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000_0000;
				end
			32'b1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro31_sel		<= #udly 1'b1;
					u_pro00_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h8000_0000;
				end
			endcase
end
endtask

task judge_01;
begin
	casex (req_bus)
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx:
				begin
					u_pro02_sel		<= #udly 1'b1;
					u_pro01_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx:
				begin
					u_pro03_sel		<= #udly 1'b1;
					u_pro01_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx:
				begin
					u_pro04_sel		<= #udly 1'b1;
					u_pro01_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx:
				begin
					u_pro05_sel		<= #udly 1'b1;
					u_pro01_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx:
				begin
					u_pro06_sel		<= #udly 1'b1;
					u_pro01_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx:
				begin
					u_pro07_sel		<= #udly 1'b1;
					u_pro01_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx:
				begin
					u_pro08_sel		<= #udly 1'b1;
					u_pro01_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx:
				begin
					u_pro09_sel		<= #udly 1'b1;
					u_pro01_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx:
				begin
					u_pro10_sel		<= #udly 1'b1;
					u_pro01_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx:
				begin
					u_pro11_sel		<= #udly 1'b1;
					u_pro01_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx:
				begin
					u_pro12_sel		<= #udly 1'b1;
					u_pro01_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx:
				begin
					u_pro13_sel		<= #udly 1'b1;
					u_pro01_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx:
				begin
					u_pro14_sel		<= #udly 1'b1;
					u_pro01_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx:
				begin
					u_pro15_sel		<= #udly 1'b1;
					u_pro01_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h8000;
				end

			//extend to 32
			32'bxxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro16_sel		<= #udly 1'b1;
					u_pro01_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001_0000;
				end
			32'bxxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro17_sel		<= #udly 1'b1;
					u_pro01_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002_0000;
				end
			32'bxxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro18_sel		<= #udly 1'b1;
					u_pro01_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004_0000;
				end
			32'bxxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro19_sel		<= #udly 1'b1;
					u_pro01_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008_0000;
				end
			32'bxxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro20_sel		<= #udly 1'b1;
					u_pro01_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010_0000;
				end
			32'bxxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro21_sel		<= #udly 1'b1;
					u_pro01_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020_0000;
				end
			32'bxxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro22_sel		<= #udly 1'b1;
					u_pro01_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040_0000;
				end
			32'bxxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro23_sel		<= #udly 1'b1;
					u_pro01_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080_0000;
				end
			32'bxxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro24_sel		<= #udly 1'b1;
					u_pro01_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100_0000;
				end
			32'bxxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro25_sel		<= #udly 1'b1;
					u_pro01_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200_0000;
				end
			32'bxxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro26_sel		<= #udly 1'b1;
					u_pro01_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400_0000;
				end
			32'bxxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro27_sel		<= #udly 1'b1;
					u_pro01_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800_0000;
				end
			32'bxxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro28_sel		<= #udly 1'b1;
					u_pro01_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000_0000;
				end
			32'bxx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro29_sel		<= #udly 1'b1;
					u_pro01_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000_0000;
				end
			32'bx1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro30_sel		<= #udly 1'b1;
					u_pro01_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000_0000;
				end
			32'b1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro31_sel		<= #udly 1'b1;
					u_pro01_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h8000_0000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1:
				begin
					u_pro00_sel		<= #udly 1'b1;
					u_pro01_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001;
				end
			endcase
end
endtask

task judge_02;
begin
	casex (req_bus)
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx:
				begin
					u_pro03_sel		<= #udly 1'b1;
					u_pro02_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx:
				begin
					u_pro04_sel		<= #udly 1'b1;
					u_pro02_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx:
				begin
					u_pro05_sel		<= #udly 1'b1;
					u_pro02_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx:
				begin
					u_pro06_sel		<= #udly 1'b1;
					u_pro02_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx:
				begin
					u_pro07_sel		<= #udly 1'b1;
					u_pro02_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx:
				begin
					u_pro08_sel		<= #udly 1'b1;
					u_pro02_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx:
				begin
					u_pro09_sel		<= #udly 1'b1;
					u_pro02_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx:
				begin
					u_pro10_sel		<= #udly 1'b1;
					u_pro02_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx:
				begin
					u_pro11_sel		<= #udly 1'b1;
					u_pro02_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx:
				begin
					u_pro12_sel		<= #udly 1'b1;
					u_pro02_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx:
				begin
					u_pro13_sel		<= #udly 1'b1;
					u_pro02_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx:
				begin
					u_pro14_sel		<= #udly 1'b1;
					u_pro02_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx:
				begin
					u_pro15_sel		<= #udly 1'b1;
					u_pro02_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h8000;
				end

			//extend to 32
			32'bxxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro16_sel		<= #udly 1'b1;
					u_pro02_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001_0000;
				end
			32'bxxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro17_sel		<= #udly 1'b1;
					u_pro02_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002_0000;
				end
			32'bxxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro18_sel		<= #udly 1'b1;
					u_pro02_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004_0000;
				end
			32'bxxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro19_sel		<= #udly 1'b1;
					u_pro02_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008_0000;
				end
			32'bxxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro20_sel		<= #udly 1'b1;
					u_pro02_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010_0000;
				end
			32'bxxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro21_sel		<= #udly 1'b1;
					u_pro02_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020_0000;
				end
			32'bxxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro22_sel		<= #udly 1'b1;
					u_pro02_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040_0000;
				end
			32'bxxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro23_sel		<= #udly 1'b1;
					u_pro02_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080_0000;
				end
			32'bxxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro24_sel		<= #udly 1'b1;
					u_pro02_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100_0000;
				end
			32'bxxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro25_sel		<= #udly 1'b1;
					u_pro02_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200_0000;
				end
			32'bxxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro26_sel		<= #udly 1'b1;
					u_pro02_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400_0000;
				end
			32'bxxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro27_sel		<= #udly 1'b1;
					u_pro02_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800_0000;
				end
			32'bxxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro28_sel		<= #udly 1'b1;
					u_pro02_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000_0000;
				end
			32'bxx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro29_sel		<= #udly 1'b1;
					u_pro02_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000_0000;
				end
			32'bx1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro30_sel		<= #udly 1'b1;
					u_pro02_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000_0000;
				end
			32'b1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro31_sel		<= #udly 1'b1;
					u_pro02_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h8000_0000;
				end

			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1:
				begin
					u_pro00_sel		<= #udly 1'b1;
					u_pro02_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x:
				begin
					u_pro01_sel		<= #udly 1'b1;
					u_pro02_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002;
				end
			endcase
end
endtask

task judge_03;
begin
	casex (req_bus)
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx:
				begin
					u_pro04_sel		<= #udly 1'b1;
					u_pro03_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx:
				begin
					u_pro05_sel		<= #udly 1'b1;
					u_pro03_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx:
				begin
					u_pro06_sel		<= #udly 1'b1;
					u_pro03_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx:
				begin
					u_pro07_sel		<= #udly 1'b1;
					u_pro03_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx:
				begin
					u_pro08_sel		<= #udly 1'b1;
					u_pro03_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx:
				begin
					u_pro09_sel		<= #udly 1'b1;
					u_pro03_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx:
				begin
					u_pro10_sel		<= #udly 1'b1;
					u_pro03_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx:
				begin
					u_pro11_sel		<= #udly 1'b1;
					u_pro03_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx:
				begin
					u_pro12_sel		<= #udly 1'b1;
					u_pro03_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx:
				begin
					u_pro13_sel		<= #udly 1'b1;
					u_pro03_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx:
				begin
					u_pro14_sel		<= #udly 1'b1;
					u_pro03_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx:
				begin
					u_pro15_sel		<= #udly 1'b1;
					u_pro03_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h8000;
				end

			//extend to 32
			32'bxxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro16_sel		<= #udly 1'b1;
					u_pro03_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001_0000;
				end
			32'bxxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro17_sel		<= #udly 1'b1;
					u_pro03_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002_0000;
				end
			32'bxxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro18_sel		<= #udly 1'b1;
					u_pro03_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004_0000;
				end
			32'bxxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro19_sel		<= #udly 1'b1;
					u_pro03_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008_0000;
				end
			32'bxxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro20_sel		<= #udly 1'b1;
					u_pro03_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010_0000;
				end
			32'bxxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro21_sel		<= #udly 1'b1;
					u_pro03_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020_0000;
				end
			32'bxxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro22_sel		<= #udly 1'b1;
					u_pro03_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040_0000;
				end
			32'bxxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro23_sel		<= #udly 1'b1;
					u_pro03_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080_0000;
				end
			32'bxxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro24_sel		<= #udly 1'b1;
					u_pro03_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100_0000;
				end
			32'bxxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro25_sel		<= #udly 1'b1;
					u_pro03_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200_0000;
				end
			32'bxxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro26_sel		<= #udly 1'b1;
					u_pro03_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400_0000;
				end
			32'bxxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro27_sel		<= #udly 1'b1;
					u_pro03_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800_0000;
				end
			32'bxxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro28_sel		<= #udly 1'b1;
					u_pro03_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000_0000;
				end
			32'bxx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro29_sel		<= #udly 1'b1;
					u_pro03_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000_0000;
				end
			32'bx1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro30_sel		<= #udly 1'b1;
					u_pro03_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000_0000;
				end
			32'b1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro31_sel		<= #udly 1'b1;
					u_pro03_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h8000_0000;
				end

			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1:
				begin
					u_pro00_sel		<= #udly 1'b1;
					u_pro03_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x:
				begin
					u_pro01_sel		<= #udly 1'b1;
					u_pro03_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx:
				begin
					u_pro02_sel		<= #udly 1'b1;
					u_pro03_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004;
				end
			endcase
end
endtask

task judge_04;
begin
	casex (req_bus)
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx:
				begin
					u_pro05_sel		<= #udly 1'b1;
					u_pro04_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx:
				begin
					u_pro06_sel		<= #udly 1'b1;
					u_pro04_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx:
				begin
					u_pro07_sel		<= #udly 1'b1;
					u_pro04_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx:
				begin
					u_pro08_sel		<= #udly 1'b1;
					u_pro04_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx:
				begin
					u_pro09_sel		<= #udly 1'b1;
					u_pro04_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx:
				begin
					u_pro10_sel		<= #udly 1'b1;
					u_pro04_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx:
				begin
					u_pro11_sel		<= #udly 1'b1;
					u_pro04_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx:
				begin
					u_pro12_sel		<= #udly 1'b1;
					u_pro04_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx:
				begin
					u_pro13_sel		<= #udly 1'b1;
					u_pro04_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx:
				begin
					u_pro14_sel		<= #udly 1'b1;
					u_pro04_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx:
				begin
					u_pro15_sel		<= #udly 1'b1;
					u_pro04_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h8000;
				end

			//extend to 32
			32'bxxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro16_sel		<= #udly 1'b1;
					u_pro04_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001_0000;
				end
			32'bxxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro17_sel		<= #udly 1'b1;
					u_pro04_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002_0000;
				end
			32'bxxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro18_sel		<= #udly 1'b1;
					u_pro04_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004_0000;
				end
			32'bxxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro19_sel		<= #udly 1'b1;
					u_pro04_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008_0000;
				end
			32'bxxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro20_sel		<= #udly 1'b1;
					u_pro04_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010_0000;
				end
			32'bxxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro21_sel		<= #udly 1'b1;
					u_pro04_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020_0000;
				end
			32'bxxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro22_sel		<= #udly 1'b1;
					u_pro04_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040_0000;
				end
			32'bxxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro23_sel		<= #udly 1'b1;
					u_pro04_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080_0000;
				end
			32'bxxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro24_sel		<= #udly 1'b1;
					u_pro04_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100_0000;
				end
			32'bxxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro25_sel		<= #udly 1'b1;
					u_pro04_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200_0000;
				end
			32'bxxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro26_sel		<= #udly 1'b1;
					u_pro04_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400_0000;
				end
			32'bxxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro27_sel		<= #udly 1'b1;
					u_pro04_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800_0000;
				end
			32'bxxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro28_sel		<= #udly 1'b1;
					u_pro04_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000_0000;
				end
			32'bxx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro29_sel		<= #udly 1'b1;
					u_pro04_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000_0000;
				end
			32'bx1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro30_sel		<= #udly 1'b1;
					u_pro04_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000_0000;
				end
			32'b1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro31_sel		<= #udly 1'b1;
					u_pro04_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h8000_0000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1:
				begin
					u_pro00_sel		<= #udly 1'b1;
					u_pro04_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x:
				begin
					u_pro01_sel		<= #udly 1'b1;
					u_pro04_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx:
				begin
					u_pro02_sel		<= #udly 1'b1;
					u_pro04_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx:
				begin
					u_pro03_sel		<= #udly 1'b1;
					u_pro04_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008;
				end
			endcase
end
endtask

task judge_05;
begin
	casex (req_bus)
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx:
				begin
					u_pro06_sel		<= #udly 1'b1;
					u_pro05_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx:
				begin
					u_pro07_sel		<= #udly 1'b1;
					u_pro05_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx:
				begin
					u_pro08_sel		<= #udly 1'b1;
					u_pro05_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx:
				begin
					u_pro09_sel		<= #udly 1'b1;
					u_pro05_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx:
				begin
					u_pro10_sel		<= #udly 1'b1;
					u_pro05_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx:
				begin
					u_pro11_sel		<= #udly 1'b1;
					u_pro05_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx:
				begin
					u_pro12_sel		<= #udly 1'b1;
					u_pro05_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx:
				begin
					u_pro13_sel		<= #udly 1'b1;
					u_pro05_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx:
				begin
					u_pro14_sel		<= #udly 1'b1;
					u_pro05_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx:
				begin
					u_pro15_sel		<= #udly 1'b1;
					u_pro05_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h8000;
				end
			//extend to 32
			32'bxxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro16_sel		<= #udly 1'b1;
					u_pro05_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001_0000;
				end
			32'bxxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro17_sel		<= #udly 1'b1;
					u_pro05_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002_0000;
				end
			32'bxxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro18_sel		<= #udly 1'b1;
					u_pro05_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004_0000;
				end
			32'bxxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro19_sel		<= #udly 1'b1;
					u_pro05_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008_0000;
				end
			32'bxxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro20_sel		<= #udly 1'b1;
					u_pro05_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010_0000;
				end
			32'bxxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro21_sel		<= #udly 1'b1;
					u_pro05_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020_0000;
				end
			32'bxxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro22_sel		<= #udly 1'b1;
					u_pro05_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040_0000;
				end
			32'bxxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro23_sel		<= #udly 1'b1;
					u_pro05_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080_0000;
				end
			32'bxxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro24_sel		<= #udly 1'b1;
					u_pro05_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100_0000;
				end
			32'bxxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro25_sel		<= #udly 1'b1;
					u_pro05_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200_0000;
				end
			32'bxxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro26_sel		<= #udly 1'b1;
					u_pro05_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400_0000;
				end
			32'bxxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro27_sel		<= #udly 1'b1;
					u_pro05_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800_0000;
				end
			32'bxxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro28_sel		<= #udly 1'b1;
					u_pro05_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000_0000;
				end
			32'bxx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro29_sel		<= #udly 1'b1;
					u_pro05_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000_0000;
				end
			32'bx1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro30_sel		<= #udly 1'b1;
					u_pro05_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000_0000;
				end
			32'b1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro31_sel		<= #udly 1'b1;
					u_pro05_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h8000_0000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1:
				begin
					u_pro00_sel		<= #udly 1'b1;
					u_pro05_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x:
				begin
					u_pro01_sel		<= #udly 1'b1;
					u_pro05_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx:
				begin
					u_pro02_sel		<= #udly 1'b1;
					u_pro05_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx:
				begin
					u_pro03_sel		<= #udly 1'b1;
					u_pro05_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx:
				begin
					u_pro04_sel		<= #udly 1'b1;
					u_pro05_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010;
				end
			endcase
end
endtask

task judge_06;
begin
	casex (req_bus)
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx:
				begin
					u_pro07_sel		<= #udly 1'b1;
					u_pro06_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx:
				begin
					u_pro08_sel		<= #udly 1'b1;
					u_pro06_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx:
				begin
					u_pro09_sel		<= #udly 1'b1;
					u_pro06_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx:
				begin
					u_pro10_sel		<= #udly 1'b1;
					u_pro06_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx:
				begin
					u_pro11_sel		<= #udly 1'b1;
					u_pro06_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx:
				begin
					u_pro12_sel		<= #udly 1'b1;
					u_pro06_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx:
				begin
					u_pro13_sel		<= #udly 1'b1;
					u_pro06_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx:
				begin
					u_pro14_sel		<= #udly 1'b1;
					u_pro06_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx:
				begin
					u_pro15_sel		<= #udly 1'b1;
					u_pro06_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h8000;
				end

			//extend to 32
			32'bxxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro16_sel		<= #udly 1'b1;
					u_pro06_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001_0000;
				end
			32'bxxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro17_sel		<= #udly 1'b1;
					u_pro06_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002_0000;
				end
			32'bxxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro18_sel		<= #udly 1'b1;
					u_pro06_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004_0000;
				end
			32'bxxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro19_sel		<= #udly 1'b1;
					u_pro06_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008_0000;
				end
			32'bxxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro20_sel		<= #udly 1'b1;
					u_pro06_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010_0000;
				end
			32'bxxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro21_sel		<= #udly 1'b1;
					u_pro06_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020_0000;
				end
			32'bxxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro22_sel		<= #udly 1'b1;
					u_pro06_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040_0000;
				end
			32'bxxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro23_sel		<= #udly 1'b1;
					u_pro06_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080_0000;
				end
			32'bxxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro24_sel		<= #udly 1'b1;
					u_pro06_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100_0000;
				end
			32'bxxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro25_sel		<= #udly 1'b1;
					u_pro06_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200_0000;
				end
			32'bxxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro26_sel		<= #udly 1'b1;
					u_pro06_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400_0000;
				end
			32'bxxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro27_sel		<= #udly 1'b1;
					u_pro06_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800_0000;
				end
			32'bxxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro28_sel		<= #udly 1'b1;
					u_pro06_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000_0000;
				end
			32'bxx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro29_sel		<= #udly 1'b1;
					u_pro06_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000_0000;
				end
			32'bx1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro30_sel		<= #udly 1'b1;
					u_pro06_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000_0000;
				end
			32'b1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro31_sel		<= #udly 1'b1;
					u_pro06_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h8000_0000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1:
				begin
					u_pro00_sel		<= #udly 1'b1;
					u_pro06_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x:
				begin
					u_pro01_sel		<= #udly 1'b1;
					u_pro06_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx:
				begin
					u_pro02_sel		<= #udly 1'b1;
					u_pro06_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx:
				begin
					u_pro03_sel		<= #udly 1'b1;
					u_pro06_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx:
				begin
					u_pro04_sel		<= #udly 1'b1;
					u_pro06_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx:
				begin
					u_pro05_sel		<= #udly 1'b1;
					u_pro06_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020;
				end
			endcase
end
endtask

task judge_07;
begin
	casex (req_bus)
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx:
				begin
					u_pro08_sel		<= #udly 1'b1;
					u_pro07_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx:
				begin
					u_pro09_sel		<= #udly 1'b1;
					u_pro07_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx:
				begin
					u_pro10_sel		<= #udly 1'b1;
					u_pro07_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx:
				begin
					u_pro11_sel		<= #udly 1'b1;
					u_pro07_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx:
				begin
					u_pro12_sel		<= #udly 1'b1;
					u_pro07_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx:
				begin
					u_pro13_sel		<= #udly 1'b1;
					u_pro07_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx:
				begin
					u_pro14_sel		<= #udly 1'b1;
					u_pro07_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx:
				begin
					u_pro15_sel		<= #udly 1'b1;
					u_pro07_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h8000;
				end
			//extend to 32
			32'bxxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro16_sel		<= #udly 1'b1;
					u_pro07_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001_0000;
				end
			32'bxxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro17_sel		<= #udly 1'b1;
					u_pro07_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002_0000;
				end
			32'bxxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro18_sel		<= #udly 1'b1;
					u_pro07_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004_0000;
				end
			32'bxxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro19_sel		<= #udly 1'b1;
					u_pro07_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008_0000;
				end
			32'bxxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro20_sel		<= #udly 1'b1;
					u_pro07_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010_0000;
				end
			32'bxxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro21_sel		<= #udly 1'b1;
					u_pro07_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020_0000;
				end
			32'bxxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro22_sel		<= #udly 1'b1;
					u_pro07_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040_0000;
				end
			32'bxxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro23_sel		<= #udly 1'b1;
					u_pro07_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080_0000;
				end
			32'bxxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro24_sel		<= #udly 1'b1;
					u_pro07_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100_0000;
				end
			32'bxxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro25_sel		<= #udly 1'b1;
					u_pro07_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200_0000;
				end
			32'bxxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro26_sel		<= #udly 1'b1;
					u_pro07_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400_0000;
				end
			32'bxxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro27_sel		<= #udly 1'b1;
					u_pro07_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800_0000;
				end
			32'bxxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro28_sel		<= #udly 1'b1;
					u_pro07_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000_0000;
				end
			32'bxx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro29_sel		<= #udly 1'b1;
					u_pro07_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000_0000;
				end
			32'bx1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro30_sel		<= #udly 1'b1;
					u_pro07_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000_0000;
				end
			32'b1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro31_sel		<= #udly 1'b1;
					u_pro07_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h8000_0000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1:
				begin
					u_pro00_sel		<= #udly 1'b1;
					u_pro07_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x:
				begin
					u_pro01_sel		<= #udly 1'b1;
					u_pro07_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx:
				begin
					u_pro02_sel		<= #udly 1'b1;
					u_pro07_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx:
				begin
					u_pro03_sel		<= #udly 1'b1;
					u_pro07_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx:
				begin
					u_pro04_sel		<= #udly 1'b1;
					u_pro07_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx:
				begin
					u_pro05_sel		<= #udly 1'b1;
					u_pro07_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx:
				begin
					u_pro06_sel		<= #udly 1'b1;
					u_pro07_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040;
				end
			endcase
end
endtask

task judge_08;
begin
	casex (req_bus)
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx:
				begin
					u_pro09_sel		<= #udly 1'b1;
					u_pro08_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx:
				begin
					u_pro10_sel		<= #udly 1'b1;
					u_pro08_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx:
				begin
					u_pro11_sel		<= #udly 1'b1;
					u_pro08_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx:
				begin
					u_pro12_sel		<= #udly 1'b1;
					u_pro08_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx:
				begin
					u_pro13_sel		<= #udly 1'b1;
					u_pro08_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx:
				begin
					u_pro14_sel		<= #udly 1'b1;
					u_pro08_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx:
				begin
					u_pro15_sel		<= #udly 1'b1;
					u_pro08_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h8000;
				end
			//extend to 32
			32'bxxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro16_sel		<= #udly 1'b1;
					u_pro08_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001_0000;
				end
			32'bxxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro17_sel		<= #udly 1'b1;
					u_pro08_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002_0000;
				end
			32'bxxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro18_sel		<= #udly 1'b1;
					u_pro08_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004_0000;
				end
			32'bxxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro19_sel		<= #udly 1'b1;
					u_pro08_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008_0000;
				end
			32'bxxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro20_sel		<= #udly 1'b1;
					u_pro08_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010_0000;
				end
			32'bxxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro21_sel		<= #udly 1'b1;
					u_pro08_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020_0000;
				end
			32'bxxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro22_sel		<= #udly 1'b1;
					u_pro08_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040_0000;
				end
			32'bxxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro23_sel		<= #udly 1'b1;
					u_pro08_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080_0000;
				end
			32'bxxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro24_sel		<= #udly 1'b1;
					u_pro08_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100_0000;
				end
			32'bxxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro25_sel		<= #udly 1'b1;
					u_pro08_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200_0000;
				end
			32'bxxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro26_sel		<= #udly 1'b1;
					u_pro08_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400_0000;
				end
			32'bxxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro27_sel		<= #udly 1'b1;
					u_pro08_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800_0000;
				end
			32'bxxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro28_sel		<= #udly 1'b1;
					u_pro08_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000_0000;
				end
			32'bxx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro29_sel		<= #udly 1'b1;
					u_pro08_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000_0000;
				end
			32'bx1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro30_sel		<= #udly 1'b1;
					u_pro08_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000_0000;
				end
			32'b1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro31_sel		<= #udly 1'b1;
					u_pro08_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h8000_0000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1:
				begin
					u_pro00_sel		<= #udly 1'b1;
					u_pro08_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x:
				begin
					u_pro01_sel		<= #udly 1'b1;
					u_pro08_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx:
				begin
					u_pro02_sel		<= #udly 1'b1;
					u_pro08_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx:
				begin
					u_pro03_sel		<= #udly 1'b1;
					u_pro08_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx:
				begin
					u_pro04_sel		<= #udly 1'b1;
					u_pro08_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx:
				begin
					u_pro05_sel		<= #udly 1'b1;
					u_pro08_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx:
				begin
					u_pro06_sel		<= #udly 1'b1;
					u_pro08_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx:
				begin
					u_pro07_sel		<= #udly 1'b1;
					u_pro08_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080;
				end
			endcase
end
endtask

task judge_09;
begin
	casex (req_bus)
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx:
				begin
					u_pro10_sel		<= #udly 1'b1;
					u_pro09_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx:
				begin
					u_pro11_sel		<= #udly 1'b1;
					u_pro09_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx:
				begin
					u_pro12_sel		<= #udly 1'b1;
					u_pro09_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx:
				begin
					u_pro13_sel		<= #udly 1'b1;
					u_pro09_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx:
				begin
					u_pro14_sel		<= #udly 1'b1;
					u_pro09_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx:
				begin
					u_pro15_sel		<= #udly 1'b1;
					u_pro09_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h8000;
				end
			//extend to 32
			32'bxxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro16_sel		<= #udly 1'b1;
					u_pro09_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001_0000;
				end
			32'bxxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro17_sel		<= #udly 1'b1;
					u_pro09_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002_0000;
				end
			32'bxxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro18_sel		<= #udly 1'b1;
					u_pro09_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004_0000;
				end
			32'bxxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro19_sel		<= #udly 1'b1;
					u_pro09_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008_0000;
				end
			32'bxxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro20_sel		<= #udly 1'b1;
					u_pro09_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010_0000;
				end
			32'bxxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro21_sel		<= #udly 1'b1;
					u_pro09_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020_0000;
				end
			32'bxxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro22_sel		<= #udly 1'b1;
					u_pro09_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040_0000;
				end
			32'bxxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro23_sel		<= #udly 1'b1;
					u_pro09_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080_0000;
				end
			32'bxxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro24_sel		<= #udly 1'b1;
					u_pro09_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100_0000;
				end
			32'bxxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro25_sel		<= #udly 1'b1;
					u_pro09_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200_0000;
				end
			32'bxxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro26_sel		<= #udly 1'b1;
					u_pro09_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400_0000;
				end
			32'bxxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro27_sel		<= #udly 1'b1;
					u_pro09_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800_0000;
				end
			32'bxxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro28_sel		<= #udly 1'b1;
					u_pro09_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000_0000;
				end
			32'bxx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro29_sel		<= #udly 1'b1;
					u_pro09_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000_0000;
				end
			32'bx1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro30_sel		<= #udly 1'b1;
					u_pro09_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000_0000;
				end
			32'b1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro31_sel		<= #udly 1'b1;
					u_pro09_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h8000_0000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1:
				begin
					u_pro00_sel		<= #udly 1'b1;
					u_pro09_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x:
				begin
					u_pro01_sel		<= #udly 1'b1;
					u_pro09_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx:
				begin
					u_pro02_sel		<= #udly 1'b1;
					u_pro09_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx:
				begin
					u_pro03_sel		<= #udly 1'b1;
					u_pro09_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx:
				begin
					u_pro04_sel		<= #udly 1'b1;
					u_pro09_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx:
				begin
					u_pro05_sel		<= #udly 1'b1;
					u_pro09_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx:
				begin
					u_pro06_sel		<= #udly 1'b1;
					u_pro09_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx:
				begin
					u_pro07_sel		<= #udly 1'b1;
					u_pro09_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx:
				begin
					u_pro08_sel		<= #udly 1'b1;
					u_pro09_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100;
				end
			endcase
end
endtask

task judge_10;
begin
	casex (req_bus)
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx:
				begin
					u_pro11_sel		<= #udly 1'b1;
					u_pro10_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx:
				begin
					u_pro12_sel		<= #udly 1'b1;
					u_pro10_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx:
				begin
					u_pro13_sel		<= #udly 1'b1;
					u_pro10_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx:
				begin
					u_pro14_sel		<= #udly 1'b1;
					u_pro10_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx:
				begin
					u_pro15_sel		<= #udly 1'b1;
					u_pro10_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h8000;
				end
			//extend to 32
			32'bxxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro16_sel		<= #udly 1'b1;
					u_pro10_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001_0000;
				end
			32'bxxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro17_sel		<= #udly 1'b1;
					u_pro10_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002_0000;
				end
			32'bxxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro18_sel		<= #udly 1'b1;
					u_pro10_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004_0000;
				end
			32'bxxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro19_sel		<= #udly 1'b1;
					u_pro10_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008_0000;
				end
			32'bxxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro20_sel		<= #udly 1'b1;
					u_pro10_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010_0000;
				end
			32'bxxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro21_sel		<= #udly 1'b1;
					u_pro10_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020_0000;
				end
			32'bxxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro22_sel		<= #udly 1'b1;
					u_pro10_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040_0000;
				end
			32'bxxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro23_sel		<= #udly 1'b1;
					u_pro10_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080_0000;
				end
			32'bxxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro24_sel		<= #udly 1'b1;
					u_pro10_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100_0000;
				end
			32'bxxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro25_sel		<= #udly 1'b1;
					u_pro10_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200_0000;
				end
			32'bxxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro26_sel		<= #udly 1'b1;
					u_pro10_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400_0000;
				end
			32'bxxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro27_sel		<= #udly 1'b1;
					u_pro10_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800_0000;
				end
			32'bxxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro28_sel		<= #udly 1'b1;
					u_pro10_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000_0000;
				end
			32'bxx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro29_sel		<= #udly 1'b1;
					u_pro10_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000_0000;
				end
			32'bx1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro30_sel		<= #udly 1'b1;
					u_pro10_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000_0000;
				end
			32'b1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro31_sel		<= #udly 1'b1;
					u_pro10_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h8000_0000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1:
				begin
					u_pro00_sel		<= #udly 1'b1;
					u_pro10_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x:
				begin
					u_pro01_sel		<= #udly 1'b1;
					u_pro10_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx:
				begin
					u_pro02_sel		<= #udly 1'b1;
					u_pro10_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx:
				begin
					u_pro03_sel		<= #udly 1'b1;
					u_pro10_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx:
				begin
					u_pro04_sel		<= #udly 1'b1;
					u_pro10_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx:
				begin
					u_pro05_sel		<= #udly 1'b1;
					u_pro10_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx:
				begin
					u_pro06_sel		<= #udly 1'b1;
					u_pro10_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx:
				begin
					u_pro07_sel		<= #udly 1'b1;
					u_pro10_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx:
				begin
					u_pro08_sel		<= #udly 1'b1;
					u_pro10_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx:
				begin
					u_pro09_sel		<= #udly 1'b1;
					u_pro10_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200;
				end
			endcase
end
endtask

task judge_11;
begin
	casex (req_bus)
			32'bxxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx:
				begin
					u_pro12_sel		<= #udly 1'b1;
					u_pro11_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx:
				begin
					u_pro13_sel		<= #udly 1'b1;
					u_pro11_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx:
				begin
					u_pro14_sel		<= #udly 1'b1;
					u_pro11_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx:
				begin
					u_pro15_sel		<= #udly 1'b1;
					u_pro11_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h8000;
				end
			//extend to 32
			32'bxxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro16_sel		<= #udly 1'b1;
					u_pro11_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001_0000;
				end
			32'bxxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro17_sel		<= #udly 1'b1;
					u_pro11_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002_0000;
				end
			32'bxxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro18_sel		<= #udly 1'b1;
					u_pro11_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004_0000;
				end
			32'bxxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro19_sel		<= #udly 1'b1;
					u_pro11_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008_0000;
				end
			32'bxxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro20_sel		<= #udly 1'b1;
					u_pro11_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010_0000;
				end
			32'bxxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro21_sel		<= #udly 1'b1;
					u_pro11_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020_0000;
				end
			32'bxxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro22_sel		<= #udly 1'b1;
					u_pro11_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040_0000;
				end
			32'bxxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro23_sel		<= #udly 1'b1;
					u_pro11_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080_0000;
				end
			32'bxxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro24_sel		<= #udly 1'b1;
					u_pro11_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100_0000;
				end
			32'bxxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro25_sel		<= #udly 1'b1;
					u_pro11_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200_0000;
				end
			32'bxxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro26_sel		<= #udly 1'b1;
					u_pro11_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400_0000;
				end
			32'bxxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro27_sel		<= #udly 1'b1;
					u_pro11_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800_0000;
				end
			32'bxxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro28_sel		<= #udly 1'b1;
					u_pro11_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000_0000;
				end
			32'bxx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro29_sel		<= #udly 1'b1;
					u_pro11_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000_0000;
				end
			32'bx1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro30_sel		<= #udly 1'b1;
					u_pro11_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000_0000;
				end
			32'b1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro31_sel		<= #udly 1'b1;
					u_pro11_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h8000_0000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1:
				begin
					u_pro00_sel		<= #udly 1'b1;
					u_pro11_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x:
				begin
					u_pro01_sel		<= #udly 1'b1;
					u_pro11_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx:
				begin
					u_pro02_sel		<= #udly 1'b1;
					u_pro11_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx:
				begin
					u_pro03_sel		<= #udly 1'b1;
					u_pro11_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx:
				begin
					u_pro04_sel		<= #udly 1'b1;
					u_pro11_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx:
				begin
					u_pro05_sel		<= #udly 1'b1;
					u_pro11_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx:
				begin
					u_pro06_sel		<= #udly 1'b1;
					u_pro11_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx:
				begin
					u_pro07_sel		<= #udly 1'b1;
					u_pro11_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx:
				begin
					u_pro08_sel		<= #udly 1'b1;
					u_pro11_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx:
				begin
					u_pro09_sel		<= #udly 1'b1;
					u_pro11_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx:
				begin
					u_pro10_sel		<= #udly 1'b1;
					u_pro11_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400;
				end
			endcase
end
endtask

task judge_12;
begin
	casex (req_bus)
			32'bxxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx:
				begin
					u_pro13_sel		<= #udly 1'b1;
					u_pro12_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx:
				begin
					u_pro14_sel		<= #udly 1'b1;
					u_pro12_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx:
				begin
					u_pro15_sel		<= #udly 1'b1;
					u_pro12_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h8000;
				end
			//extend to 32
			32'bxxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro16_sel		<= #udly 1'b1;
					u_pro12_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001_0000;
				end
			32'bxxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro17_sel		<= #udly 1'b1;
					u_pro12_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002_0000;
				end
			32'bxxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro18_sel		<= #udly 1'b1;
					u_pro12_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004_0000;
				end
			32'bxxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro19_sel		<= #udly 1'b1;
					u_pro12_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008_0000;
				end
			32'bxxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro20_sel		<= #udly 1'b1;
					u_pro12_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010_0000;
				end
			32'bxxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro21_sel		<= #udly 1'b1;
					u_pro12_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020_0000;
				end
			32'bxxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro22_sel		<= #udly 1'b1;
					u_pro12_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040_0000;
				end
			32'bxxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro23_sel		<= #udly 1'b1;
					u_pro12_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080_0000;
				end
			32'bxxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro24_sel		<= #udly 1'b1;
					u_pro12_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100_0000;
				end
			32'bxxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro25_sel		<= #udly 1'b1;
					u_pro12_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200_0000;
				end
			32'bxxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro26_sel		<= #udly 1'b1;
					u_pro12_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400_0000;
				end
			32'bxxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro27_sel		<= #udly 1'b1;
					u_pro12_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800_0000;
				end
			32'bxxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro28_sel		<= #udly 1'b1;
					u_pro12_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000_0000;
				end
			32'bxx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro29_sel		<= #udly 1'b1;
					u_pro12_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000_0000;
				end
			32'bx1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro30_sel		<= #udly 1'b1;
					u_pro12_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000_0000;
				end
			32'b1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro31_sel		<= #udly 1'b1;
					u_pro12_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h8000_0000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1:
				begin
					u_pro00_sel		<= #udly 1'b1;
					u_pro12_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x:
				begin
					u_pro01_sel		<= #udly 1'b1;
					u_pro12_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx:
				begin
					u_pro02_sel		<= #udly 1'b1;
					u_pro12_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx:
				begin
					u_pro03_sel		<= #udly 1'b1;
					u_pro12_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx:
				begin
					u_pro04_sel		<= #udly 1'b1;
					u_pro12_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx:
				begin
					u_pro05_sel		<= #udly 1'b1;
					u_pro12_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx:
				begin
					u_pro06_sel		<= #udly 1'b1;
					u_pro12_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx:
				begin
					u_pro07_sel		<= #udly 1'b1;
					u_pro12_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx:
				begin
					u_pro08_sel		<= #udly 1'b1;
					u_pro12_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx:
				begin
					u_pro09_sel		<= #udly 1'b1;
					u_pro12_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx:
				begin
					u_pro10_sel		<= #udly 1'b1;
					u_pro12_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx:
				begin
					u_pro11_sel		<= #udly 1'b1;
					u_pro12_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800;
				end
			endcase
end
endtask

task judge_13;
begin
	casex (req_bus)
			32'bxxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx:
				begin
					u_pro14_sel		<= #udly 1'b1;
					u_pro13_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx:
				begin
					u_pro15_sel		<= #udly 1'b1;
					u_pro13_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h8000;
				end
			//extend to 32
			32'bxxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro16_sel		<= #udly 1'b1;
					u_pro13_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001_0000;
				end
			32'bxxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro17_sel		<= #udly 1'b1;
					u_pro13_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002_0000;
				end
			32'bxxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro18_sel		<= #udly 1'b1;
					u_pro13_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004_0000;
				end
			32'bxxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro19_sel		<= #udly 1'b1;
					u_pro13_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008_0000;
				end
			32'bxxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro20_sel		<= #udly 1'b1;
					u_pro13_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010_0000;
				end
			32'bxxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro21_sel		<= #udly 1'b1;
					u_pro13_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020_0000;
				end
			32'bxxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro22_sel		<= #udly 1'b1;
					u_pro13_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040_0000;
				end
			32'bxxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro23_sel		<= #udly 1'b1;
					u_pro13_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080_0000;
				end
			32'bxxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro24_sel		<= #udly 1'b1;
					u_pro13_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100_0000;
				end
			32'bxxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro25_sel		<= #udly 1'b1;
					u_pro13_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200_0000;
				end
			32'bxxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro26_sel		<= #udly 1'b1;
					u_pro13_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400_0000;
				end
			32'bxxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro27_sel		<= #udly 1'b1;
					u_pro13_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800_0000;
				end
			32'bxxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro28_sel		<= #udly 1'b1;
					u_pro13_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000_0000;
				end
			32'bxx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro29_sel		<= #udly 1'b1;
					u_pro13_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000_0000;
				end
			32'bx1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro30_sel		<= #udly 1'b1;
					u_pro13_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000_0000;
				end
			32'b1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro31_sel		<= #udly 1'b1;
					u_pro13_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h8000_0000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1:
				begin
					u_pro00_sel		<= #udly 1'b1;
					u_pro13_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x:
				begin
					u_pro01_sel		<= #udly 1'b1;
					u_pro13_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx:
				begin
					u_pro02_sel		<= #udly 1'b1;
					u_pro13_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx:
				begin
					u_pro03_sel		<= #udly 1'b1;
					u_pro13_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx:
				begin
					u_pro04_sel		<= #udly 1'b1;
					u_pro13_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx:
				begin
					u_pro05_sel		<= #udly 1'b1;
					u_pro13_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx:
				begin
					u_pro06_sel		<= #udly 1'b1;
					u_pro13_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx:
				begin
					u_pro07_sel		<= #udly 1'b1;
					u_pro13_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx:
				begin
					u_pro08_sel		<= #udly 1'b1;
					u_pro13_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx:
				begin
					u_pro09_sel		<= #udly 1'b1;
					u_pro13_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx:
				begin
					u_pro10_sel		<= #udly 1'b1;
					u_pro13_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx:
				begin
					u_pro11_sel		<= #udly 1'b1;
					u_pro13_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx:
				begin
					u_pro12_sel		<= #udly 1'b1;
					u_pro13_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000;
				end
			endcase
end
endtask

task judge_14;
begin
	casex (req_bus)
			32'bxxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx:
				begin
					u_pro15_sel		<= #udly 1'b1;
					u_pro14_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h8000;
				end
			//extend to 32
			32'bxxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro16_sel		<= #udly 1'b1;
					u_pro14_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001_0000;
				end
			32'bxxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro17_sel		<= #udly 1'b1;
					u_pro14_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002_0000;
				end
			32'bxxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro18_sel		<= #udly 1'b1;
					u_pro14_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004_0000;
				end
			32'bxxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro19_sel		<= #udly 1'b1;
					u_pro14_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008_0000;
				end
			32'bxxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro20_sel		<= #udly 1'b1;
					u_pro14_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010_0000;
				end
			32'bxxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro21_sel		<= #udly 1'b1;
					u_pro14_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020_0000;
				end
			32'bxxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro22_sel		<= #udly 1'b1;
					u_pro14_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040_0000;
				end
			32'bxxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro23_sel		<= #udly 1'b1;
					u_pro14_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080_0000;
				end
			32'bxxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro24_sel		<= #udly 1'b1;
					u_pro14_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100_0000;
				end
			32'bxxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro25_sel		<= #udly 1'b1;
					u_pro14_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200_0000;
				end
			32'bxxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro26_sel		<= #udly 1'b1;
					u_pro14_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400_0000;
				end
			32'bxxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro27_sel		<= #udly 1'b1;
					u_pro14_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800_0000;
				end
			32'bxxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro28_sel		<= #udly 1'b1;
					u_pro14_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000_0000;
				end
			32'bxx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro29_sel		<= #udly 1'b1;
					u_pro14_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000_0000;
				end
			32'bx1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro30_sel		<= #udly 1'b1;
					u_pro14_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000_0000;
				end
			32'b1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro31_sel		<= #udly 1'b1;
					u_pro14_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h8000_0000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1:
				begin
					u_pro00_sel		<= #udly 1'b1;
					u_pro14_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x:
				begin
					u_pro01_sel		<= #udly 1'b1;
					u_pro14_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx:
				begin
					u_pro02_sel		<= #udly 1'b1;
					u_pro14_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx:
				begin
					u_pro03_sel		<= #udly 1'b1;
					u_pro14_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx:
				begin
					u_pro04_sel		<= #udly 1'b1;
					u_pro14_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx:
				begin
					u_pro05_sel		<= #udly 1'b1;
					u_pro14_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx:
				begin
					u_pro06_sel		<= #udly 1'b1;
					u_pro14_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx:
				begin
					u_pro07_sel		<= #udly 1'b1;
					u_pro14_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx:
				begin
					u_pro08_sel		<= #udly 1'b1;
					u_pro14_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx:
				begin
					u_pro09_sel		<= #udly 1'b1;
					u_pro14_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx:
				begin
					u_pro10_sel		<= #udly 1'b1;
					u_pro14_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx:
				begin
					u_pro11_sel		<= #udly 1'b1;
					u_pro14_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx:
				begin
					u_pro12_sel		<= #udly 1'b1;
					u_pro14_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx:
				begin
					u_pro13_sel		<= #udly 1'b1;
					u_pro14_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000;
				end
			endcase
end
endtask

task judge_15;
begin
	casex (req_bus)
			//extend to 32
			32'bxxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro16_sel		<= #udly 1'b1;
					u_pro15_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001_0000;
				end
			32'bxxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro17_sel		<= #udly 1'b1;
					u_pro15_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002_0000;
				end
			32'bxxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro18_sel		<= #udly 1'b1;
					u_pro15_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004_0000;
				end
			32'bxxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro19_sel		<= #udly 1'b1;
					u_pro15_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008_0000;
				end
			32'bxxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro20_sel		<= #udly 1'b1;
					u_pro15_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010_0000;
				end
			32'bxxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro21_sel		<= #udly 1'b1;
					u_pro15_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020_0000;
				end
			32'bxxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro22_sel		<= #udly 1'b1;
					u_pro15_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040_0000;
				end
			32'bxxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro23_sel		<= #udly 1'b1;
					u_pro15_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080_0000;
				end
			32'bxxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro24_sel		<= #udly 1'b1;
					u_pro15_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100_0000;
				end
			32'bxxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro25_sel		<= #udly 1'b1;
					u_pro15_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200_0000;
				end
			32'bxxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro26_sel		<= #udly 1'b1;
					u_pro15_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400_0000;
				end
			32'bxxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro27_sel		<= #udly 1'b1;
					u_pro15_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800_0000;
				end
			32'bxxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro28_sel		<= #udly 1'b1;
					u_pro15_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000_0000;
				end
			32'bxx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro29_sel		<= #udly 1'b1;
					u_pro15_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000_0000;
				end
			32'bx1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro30_sel		<= #udly 1'b1;
					u_pro15_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000_0000;
				end
			32'b1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro31_sel		<= #udly 1'b1;
					u_pro15_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h8000_0000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1:
				begin
					u_pro00_sel		<= #udly 1'b1;
					u_pro15_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x:
				begin
					u_pro01_sel		<= #udly 1'b1;
					u_pro15_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx:
				begin
					u_pro02_sel		<= #udly 1'b1;
					u_pro15_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx:
				begin
					u_pro03_sel		<= #udly 1'b1;
					u_pro15_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx:
				begin
					u_pro04_sel		<= #udly 1'b1;
					u_pro15_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx:
				begin
					u_pro05_sel		<= #udly 1'b1;
					u_pro15_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx:
				begin
					u_pro06_sel		<= #udly 1'b1;
					u_pro15_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx:
				begin
					u_pro07_sel		<= #udly 1'b1;
					u_pro15_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx:
				begin
					u_pro08_sel		<= #udly 1'b1;
					u_pro15_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx:
				begin
					u_pro09_sel		<= #udly 1'b1;
					u_pro15_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx:
				begin
					u_pro10_sel		<= #udly 1'b1;
					u_pro15_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx:
				begin
					u_pro11_sel		<= #udly 1'b1;
					u_pro15_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx:
				begin
					u_pro12_sel		<= #udly 1'b1;
					u_pro15_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx:
				begin
					u_pro13_sel		<= #udly 1'b1;
					u_pro15_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx:
				begin
					u_pro14_sel		<= #udly 1'b1;
					u_pro15_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000;
				end
			endcase
end
endtask

task judge_16;
begin
	casex (req_bus)
			32'bxxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro17_sel		<= #udly 1'b1;
					u_pro16_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002_0000;
				end
			32'bxxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro18_sel		<= #udly 1'b1;
					u_pro16_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004_0000;
				end
			32'bxxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro19_sel		<= #udly 1'b1;
					u_pro16_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008_0000;
				end
			32'bxxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro20_sel		<= #udly 1'b1;
					u_pro16_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010_0000;
				end
			32'bxxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro21_sel		<= #udly 1'b1;
					u_pro16_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020_0000;
				end
			32'bxxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro22_sel		<= #udly 1'b1;
					u_pro16_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040_0000;
				end
			32'bxxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro23_sel		<= #udly 1'b1;
					u_pro16_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080_0000;
				end
			32'bxxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro24_sel		<= #udly 1'b1;
					u_pro16_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100_0000;
				end
			32'bxxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro25_sel		<= #udly 1'b1;
					u_pro16_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200_0000;
				end
			32'bxxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro26_sel		<= #udly 1'b1;
					u_pro16_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400_0000;
				end
			32'bxxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro27_sel		<= #udly 1'b1;
					u_pro16_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800_0000;
				end
			32'bxxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro28_sel		<= #udly 1'b1;
					u_pro16_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000_0000;
				end
			32'bxx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro29_sel		<= #udly 1'b1;
					u_pro16_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000_0000;
				end
			32'bx1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro30_sel		<= #udly 1'b1;
					u_pro16_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000_0000;
				end
			32'b1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro31_sel		<= #udly 1'b1;
					u_pro16_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h8000_0000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1:
				begin
					u_pro00_sel		<= #udly 1'b1;
					u_pro16_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x:
				begin
					u_pro01_sel		<= #udly 1'b1;
					u_pro16_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx:
				begin
					u_pro02_sel		<= #udly 1'b1;
					u_pro16_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx:
				begin
					u_pro03_sel		<= #udly 1'b1;
					u_pro16_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx:
				begin
					u_pro04_sel		<= #udly 1'b1;
					u_pro16_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx:
				begin
					u_pro05_sel		<= #udly 1'b1;
					u_pro16_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx:
				begin
					u_pro06_sel		<= #udly 1'b1;
					u_pro16_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx:
				begin
					u_pro07_sel		<= #udly 1'b1;
					u_pro16_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx:
				begin
					u_pro08_sel		<= #udly 1'b1;
					u_pro16_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx:
				begin
					u_pro09_sel		<= #udly 1'b1;
					u_pro16_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx:
				begin
					u_pro10_sel		<= #udly 1'b1;
					u_pro16_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx:
				begin
					u_pro11_sel		<= #udly 1'b1;
					u_pro16_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx:
				begin
					u_pro12_sel		<= #udly 1'b1;
					u_pro16_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx:
				begin
					u_pro13_sel		<= #udly 1'b1;
					u_pro16_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx:
				begin
					u_pro14_sel		<= #udly 1'b1;
					u_pro16_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx:
				begin
					u_pro15_sel		<= #udly 1'b1;
					u_pro16_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0000_8000;
				end
			endcase
end
endtask

task judge_17;
begin
	casex (req_bus)
			32'bxxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro18_sel		<= #udly 1'b1;
					u_pro17_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004_0000;
				end
			32'bxxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro19_sel		<= #udly 1'b1;
					u_pro17_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008_0000;
				end
			32'bxxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro20_sel		<= #udly 1'b1;
					u_pro17_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010_0000;
				end
			32'bxxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro21_sel		<= #udly 1'b1;
					u_pro17_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020_0000;
				end
			32'bxxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro22_sel		<= #udly 1'b1;
					u_pro17_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040_0000;
				end
			32'bxxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro23_sel		<= #udly 1'b1;
					u_pro17_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080_0000;
				end
			32'bxxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro24_sel		<= #udly 1'b1;
					u_pro17_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100_0000;
				end
			32'bxxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro25_sel		<= #udly 1'b1;
					u_pro17_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200_0000;
				end
			32'bxxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro26_sel		<= #udly 1'b1;
					u_pro17_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400_0000;
				end
			32'bxxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro27_sel		<= #udly 1'b1;
					u_pro17_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800_0000;
				end
			32'bxxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro28_sel		<= #udly 1'b1;
					u_pro17_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000_0000;
				end
			32'bxx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro29_sel		<= #udly 1'b1;
					u_pro17_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000_0000;
				end
			32'bx1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro30_sel		<= #udly 1'b1;
					u_pro17_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000_0000;
				end
			32'b1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro31_sel		<= #udly 1'b1;
					u_pro17_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h8000_0000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1:
				begin
					u_pro00_sel		<= #udly 1'b1;
					u_pro17_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x:
				begin
					u_pro01_sel		<= #udly 1'b1;
					u_pro17_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx:
				begin
					u_pro02_sel		<= #udly 1'b1;
					u_pro17_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx:
				begin
					u_pro03_sel		<= #udly 1'b1;
					u_pro17_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx:
				begin
					u_pro04_sel		<= #udly 1'b1;
					u_pro17_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx:
				begin
					u_pro05_sel		<= #udly 1'b1;
					u_pro17_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx:
				begin
					u_pro06_sel		<= #udly 1'b1;
					u_pro17_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx:
				begin
					u_pro07_sel		<= #udly 1'b1;
					u_pro17_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx:
				begin
					u_pro08_sel		<= #udly 1'b1;
					u_pro17_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx:
				begin
					u_pro09_sel		<= #udly 1'b1;
					u_pro17_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx:
				begin
					u_pro10_sel		<= #udly 1'b1;
					u_pro17_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx:
				begin
					u_pro11_sel		<= #udly 1'b1;
					u_pro17_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx:
				begin
					u_pro12_sel		<= #udly 1'b1;
					u_pro17_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx:
				begin
					u_pro13_sel		<= #udly 1'b1;
					u_pro17_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx:
				begin
					u_pro14_sel		<= #udly 1'b1;
					u_pro17_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx:
				begin
					u_pro15_sel		<= #udly 1'b1;
					u_pro17_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0000_8000;
				end
			32'bxxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro16_sel		<= #udly 1'b1;
					u_pro17_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001_0000;
				end
			endcase
end
endtask

task judge_18;
begin
	casex (req_bus)
			32'bxxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro19_sel		<= #udly 1'b1;
					u_pro18_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008_0000;
				end
			32'bxxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro20_sel		<= #udly 1'b1;
					u_pro18_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010_0000;
				end
			32'bxxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro21_sel		<= #udly 1'b1;
					u_pro18_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020_0000;
				end
			32'bxxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro22_sel		<= #udly 1'b1;
					u_pro18_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040_0000;
				end
			32'bxxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro23_sel		<= #udly 1'b1;
					u_pro18_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080_0000;
				end
			32'bxxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro24_sel		<= #udly 1'b1;
					u_pro18_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100_0000;
				end
			32'bxxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro25_sel		<= #udly 1'b1;
					u_pro18_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200_0000;
				end
			32'bxxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro26_sel		<= #udly 1'b1;
					u_pro18_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400_0000;
				end
			32'bxxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro27_sel		<= #udly 1'b1;
					u_pro18_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800_0000;
				end
			32'bxxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro28_sel		<= #udly 1'b1;
					u_pro18_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000_0000;
				end
			32'bxx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro29_sel		<= #udly 1'b1;
					u_pro18_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000_0000;
				end
			32'bx1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro30_sel		<= #udly 1'b1;
					u_pro18_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000_0000;
				end
			32'b1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro31_sel		<= #udly 1'b1;
					u_pro18_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h8000_0000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1:
				begin
					u_pro00_sel		<= #udly 1'b1;
					u_pro18_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x:
				begin
					u_pro01_sel		<= #udly 1'b1;
					u_pro18_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx:
				begin
					u_pro02_sel		<= #udly 1'b1;
					u_pro18_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx:
				begin
					u_pro03_sel		<= #udly 1'b1;
					u_pro18_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx:
				begin
					u_pro04_sel		<= #udly 1'b1;
					u_pro18_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx:
				begin
					u_pro05_sel		<= #udly 1'b1;
					u_pro18_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx:
				begin
					u_pro06_sel		<= #udly 1'b1;
					u_pro18_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx:
				begin
					u_pro07_sel		<= #udly 1'b1;
					u_pro18_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx:
				begin
					u_pro08_sel		<= #udly 1'b1;
					u_pro18_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx:
				begin
					u_pro09_sel		<= #udly 1'b1;
					u_pro18_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx:
				begin
					u_pro10_sel		<= #udly 1'b1;
					u_pro18_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx:
				begin
					u_pro11_sel		<= #udly 1'b1;
					u_pro18_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx:
				begin
					u_pro12_sel		<= #udly 1'b1;
					u_pro18_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx:
				begin
					u_pro13_sel		<= #udly 1'b1;
					u_pro18_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx:
				begin
					u_pro14_sel		<= #udly 1'b1;
					u_pro18_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx:
				begin
					u_pro15_sel		<= #udly 1'b1;
					u_pro18_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0000_8000;
				end
			32'bxxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro16_sel		<= #udly 1'b1;
					u_pro18_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001_0000;
				end
			32'bxxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro17_sel		<= #udly 1'b1;
					u_pro18_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002_0000;
				end
			endcase
end
endtask

task judge_19;
begin
	casex (req_bus)
			32'bxxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro20_sel		<= #udly 1'b1;
					u_pro19_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010_0000;
				end
			32'bxxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro21_sel		<= #udly 1'b1;
					u_pro19_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020_0000;
				end
			32'bxxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro22_sel		<= #udly 1'b1;
					u_pro19_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040_0000;
				end
			32'bxxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro23_sel		<= #udly 1'b1;
					u_pro19_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080_0000;
				end
			32'bxxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro24_sel		<= #udly 1'b1;
					u_pro19_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100_0000;
				end
			32'bxxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro25_sel		<= #udly 1'b1;
					u_pro19_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200_0000;
				end
			32'bxxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro26_sel		<= #udly 1'b1;
					u_pro19_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400_0000;
				end
			32'bxxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro27_sel		<= #udly 1'b1;
					u_pro19_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800_0000;
				end
			32'bxxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro28_sel		<= #udly 1'b1;
					u_pro19_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000_0000;
				end
			32'bxx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro29_sel		<= #udly 1'b1;
					u_pro19_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000_0000;
				end
			32'bx1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro30_sel		<= #udly 1'b1;
					u_pro19_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000_0000;
				end
			32'b1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro31_sel		<= #udly 1'b1;
					u_pro19_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h8000_0000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1:
				begin
					u_pro00_sel		<= #udly 1'b1;
					u_pro19_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x:
				begin
					u_pro01_sel		<= #udly 1'b1;
					u_pro19_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx:
				begin
					u_pro02_sel		<= #udly 1'b1;
					u_pro19_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx:
				begin
					u_pro03_sel		<= #udly 1'b1;
					u_pro19_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx:
				begin
					u_pro04_sel		<= #udly 1'b1;
					u_pro19_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx:
				begin
					u_pro05_sel		<= #udly 1'b1;
					u_pro19_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx:
				begin
					u_pro06_sel		<= #udly 1'b1;
					u_pro19_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx:
				begin
					u_pro07_sel		<= #udly 1'b1;
					u_pro19_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx:
				begin
					u_pro08_sel		<= #udly 1'b1;
					u_pro19_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx:
				begin
					u_pro09_sel		<= #udly 1'b1;
					u_pro19_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx:
				begin
					u_pro10_sel		<= #udly 1'b1;
					u_pro19_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx:
				begin
					u_pro11_sel		<= #udly 1'b1;
					u_pro19_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx:
				begin
					u_pro12_sel		<= #udly 1'b1;
					u_pro19_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx:
				begin
					u_pro13_sel		<= #udly 1'b1;
					u_pro19_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx:
				begin
					u_pro14_sel		<= #udly 1'b1;
					u_pro19_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx:
				begin
					u_pro15_sel		<= #udly 1'b1;
					u_pro19_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0000_8000;
				end
			32'bxxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro16_sel		<= #udly 1'b1;
					u_pro19_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001_0000;
				end
			32'bxxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro17_sel		<= #udly 1'b1;
					u_pro19_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002_0000;
				end
			32'bxxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro18_sel		<= #udly 1'b1;
					u_pro19_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004_0000;
				end
			endcase
end
endtask

task judge_20;
begin
	casex (req_bus)
			32'bxxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro21_sel		<= #udly 1'b1;
					u_pro20_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020_0000;
				end
			32'bxxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro22_sel		<= #udly 1'b1;
					u_pro20_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040_0000;
				end
			32'bxxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro23_sel		<= #udly 1'b1;
					u_pro20_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080_0000;
				end
			32'bxxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro24_sel		<= #udly 1'b1;
					u_pro20_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100_0000;
				end
			32'bxxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro25_sel		<= #udly 1'b1;
					u_pro20_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200_0000;
				end
			32'bxxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro26_sel		<= #udly 1'b1;
					u_pro20_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400_0000;
				end
			32'bxxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro27_sel		<= #udly 1'b1;
					u_pro20_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800_0000;
				end
			32'bxxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro28_sel		<= #udly 1'b1;
					u_pro20_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000_0000;
				end
			32'bxx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro29_sel		<= #udly 1'b1;
					u_pro20_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000_0000;
				end
			32'bx1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro30_sel		<= #udly 1'b1;
					u_pro20_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000_0000;
				end
			32'b1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro31_sel		<= #udly 1'b1;
					u_pro20_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h8000_0000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1:
				begin
					u_pro00_sel		<= #udly 1'b1;
					u_pro20_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x:
				begin
					u_pro01_sel		<= #udly 1'b1;
					u_pro20_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx:
				begin
					u_pro02_sel		<= #udly 1'b1;
					u_pro20_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx:
				begin
					u_pro03_sel		<= #udly 1'b1;
					u_pro20_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx:
				begin
					u_pro04_sel		<= #udly 1'b1;
					u_pro20_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx:
				begin
					u_pro05_sel		<= #udly 1'b1;
					u_pro20_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx:
				begin
					u_pro06_sel		<= #udly 1'b1;
					u_pro20_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx:
				begin
					u_pro07_sel		<= #udly 1'b1;
					u_pro20_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx:
				begin
					u_pro08_sel		<= #udly 1'b1;
					u_pro20_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx:
				begin
					u_pro09_sel		<= #udly 1'b1;
					u_pro20_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx:
				begin
					u_pro10_sel		<= #udly 1'b1;
					u_pro20_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx:
				begin
					u_pro11_sel		<= #udly 1'b1;
					u_pro20_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx:
				begin
					u_pro12_sel		<= #udly 1'b1;
					u_pro20_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx:
				begin
					u_pro13_sel		<= #udly 1'b1;
					u_pro20_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx:
				begin
					u_pro14_sel		<= #udly 1'b1;
					u_pro20_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx:
				begin
					u_pro15_sel		<= #udly 1'b1;
					u_pro20_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0000_8000;
				end
			32'bxxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro16_sel		<= #udly 1'b1;
					u_pro20_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001_0000;
				end
			32'bxxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro17_sel		<= #udly 1'b1;
					u_pro20_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002_0000;
				end
			32'bxxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro18_sel		<= #udly 1'b1;
					u_pro20_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004_0000;
				end
			32'bxxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro19_sel		<= #udly 1'b1;
					u_pro20_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008_0000;
				end
			endcase
end
endtask

task judge_21;
begin
	casex (req_bus)
			32'bxxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro22_sel		<= #udly 1'b1;
					u_pro21_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040_0000;
				end
			32'bxxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro23_sel		<= #udly 1'b1;
					u_pro21_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080_0000;
				end
			32'bxxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro24_sel		<= #udly 1'b1;
					u_pro21_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100_0000;
				end
			32'bxxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro25_sel		<= #udly 1'b1;
					u_pro21_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200_0000;
				end
			32'bxxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro26_sel		<= #udly 1'b1;
					u_pro21_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400_0000;
				end
			32'bxxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro27_sel		<= #udly 1'b1;
					u_pro21_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800_0000;
				end
			32'bxxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro28_sel		<= #udly 1'b1;
					u_pro21_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000_0000;
				end
			32'bxx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro29_sel		<= #udly 1'b1;
					u_pro21_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000_0000;
				end
			32'bx1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro30_sel		<= #udly 1'b1;
					u_pro21_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000_0000;
				end
			32'b1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro31_sel		<= #udly 1'b1;
					u_pro21_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h8000_0000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1:
				begin
					u_pro00_sel		<= #udly 1'b1;
					u_pro21_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x:
				begin
					u_pro01_sel		<= #udly 1'b1;
					u_pro21_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx:
				begin
					u_pro02_sel		<= #udly 1'b1;
					u_pro21_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx:
				begin
					u_pro03_sel		<= #udly 1'b1;
					u_pro21_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx:
				begin
					u_pro04_sel		<= #udly 1'b1;
					u_pro21_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx:
				begin
					u_pro05_sel		<= #udly 1'b1;
					u_pro21_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx:
				begin
					u_pro06_sel		<= #udly 1'b1;
					u_pro21_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx:
				begin
					u_pro07_sel		<= #udly 1'b1;
					u_pro21_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx:
				begin
					u_pro08_sel		<= #udly 1'b1;
					u_pro21_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx:
				begin
					u_pro09_sel		<= #udly 1'b1;
					u_pro21_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx:
				begin
					u_pro10_sel		<= #udly 1'b1;
					u_pro21_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx:
				begin
					u_pro11_sel		<= #udly 1'b1;
					u_pro21_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx:
				begin
					u_pro12_sel		<= #udly 1'b1;
					u_pro21_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx:
				begin
					u_pro13_sel		<= #udly 1'b1;
					u_pro21_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx:
				begin
					u_pro14_sel		<= #udly 1'b1;
					u_pro21_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx:
				begin
					u_pro15_sel		<= #udly 1'b1;
					u_pro21_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0000_8000;
				end
			32'bxxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro16_sel		<= #udly 1'b1;
					u_pro21_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001_0000;
				end
			32'bxxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro17_sel		<= #udly 1'b1;
					u_pro21_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002_0000;
				end
			32'bxxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro18_sel		<= #udly 1'b1;
					u_pro21_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004_0000;
				end
			32'bxxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro19_sel		<= #udly 1'b1;
					u_pro21_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008_0000;
				end
			32'bxxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro20_sel		<= #udly 1'b1;
					u_pro21_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010_0000;
				end
			endcase
end
endtask

task judge_22;
begin
	casex (req_bus)
			32'bxxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro23_sel		<= #udly 1'b1;
					u_pro22_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080_0000;
				end
			32'bxxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro24_sel		<= #udly 1'b1;
					u_pro22_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100_0000;
				end
			32'bxxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro25_sel		<= #udly 1'b1;
					u_pro22_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200_0000;
				end
			32'bxxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro26_sel		<= #udly 1'b1;
					u_pro22_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400_0000;
				end
			32'bxxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro27_sel		<= #udly 1'b1;
					u_pro22_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800_0000;
				end
			32'bxxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro28_sel		<= #udly 1'b1;
					u_pro22_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000_0000;
				end
			32'bxx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro29_sel		<= #udly 1'b1;
					u_pro22_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000_0000;
				end
			32'bx1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro30_sel		<= #udly 1'b1;
					u_pro22_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000_0000;
				end
			32'b1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro31_sel		<= #udly 1'b1;
					u_pro22_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h8000_0000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1:
				begin
					u_pro00_sel		<= #udly 1'b1;
					u_pro22_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x:
				begin
					u_pro01_sel		<= #udly 1'b1;
					u_pro22_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx:
				begin
					u_pro02_sel		<= #udly 1'b1;
					u_pro22_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx:
				begin
					u_pro03_sel		<= #udly 1'b1;
					u_pro22_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx:
				begin
					u_pro04_sel		<= #udly 1'b1;
					u_pro22_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx:
				begin
					u_pro05_sel		<= #udly 1'b1;
					u_pro22_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx:
				begin
					u_pro06_sel		<= #udly 1'b1;
					u_pro22_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx:
				begin
					u_pro07_sel		<= #udly 1'b1;
					u_pro22_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx:
				begin
					u_pro08_sel		<= #udly 1'b1;
					u_pro22_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx:
				begin
					u_pro09_sel		<= #udly 1'b1;
					u_pro22_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx:
				begin
					u_pro10_sel		<= #udly 1'b1;
					u_pro22_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx:
				begin
					u_pro11_sel		<= #udly 1'b1;
					u_pro22_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx:
				begin
					u_pro12_sel		<= #udly 1'b1;
					u_pro22_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx:
				begin
					u_pro13_sel		<= #udly 1'b1;
					u_pro22_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx:
				begin
					u_pro14_sel		<= #udly 1'b1;
					u_pro22_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx:
				begin
					u_pro15_sel		<= #udly 1'b1;
					u_pro22_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0000_8000;
				end
			32'bxxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro16_sel		<= #udly 1'b1;
					u_pro22_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001_0000;
				end
			32'bxxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro17_sel		<= #udly 1'b1;
					u_pro22_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002_0000;
				end
			32'bxxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro18_sel		<= #udly 1'b1;
					u_pro22_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004_0000;
				end
			32'bxxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro19_sel		<= #udly 1'b1;
					u_pro22_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008_0000;
				end
			32'bxxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro20_sel		<= #udly 1'b1;
					u_pro22_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010_0000;
				end
			32'bxxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro21_sel		<= #udly 1'b1;
					u_pro22_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020_0000;
				end
			endcase
end
endtask

task judge_23;
begin
	casex (req_bus)
			32'bxxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro24_sel		<= #udly 1'b1;
					u_pro23_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100_0000;
				end
			32'bxxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro25_sel		<= #udly 1'b1;
					u_pro23_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200_0000;
				end
			32'bxxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro26_sel		<= #udly 1'b1;
					u_pro23_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400_0000;
				end
			32'bxxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro27_sel		<= #udly 1'b1;
					u_pro23_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800_0000;
				end
			32'bxxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro28_sel		<= #udly 1'b1;
					u_pro23_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000_0000;
				end
			32'bxx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro29_sel		<= #udly 1'b1;
					u_pro23_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000_0000;
				end
			32'bx1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro30_sel		<= #udly 1'b1;
					u_pro23_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000_0000;
				end
			32'b1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro31_sel		<= #udly 1'b1;
					u_pro23_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h8000_0000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1:
				begin
					u_pro00_sel		<= #udly 1'b1;
					u_pro23_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x:
				begin
					u_pro01_sel		<= #udly 1'b1;
					u_pro23_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx:
				begin
					u_pro02_sel		<= #udly 1'b1;
					u_pro23_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx:
				begin
					u_pro03_sel		<= #udly 1'b1;
					u_pro23_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx:
				begin
					u_pro04_sel		<= #udly 1'b1;
					u_pro23_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx:
				begin
					u_pro05_sel		<= #udly 1'b1;
					u_pro23_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx:
				begin
					u_pro06_sel		<= #udly 1'b1;
					u_pro23_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx:
				begin
					u_pro07_sel		<= #udly 1'b1;
					u_pro23_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx:
				begin
					u_pro08_sel		<= #udly 1'b1;
					u_pro23_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx:
				begin
					u_pro09_sel		<= #udly 1'b1;
					u_pro23_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx:
				begin
					u_pro10_sel		<= #udly 1'b1;
					u_pro23_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx:
				begin
					u_pro11_sel		<= #udly 1'b1;
					u_pro23_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx:
				begin
					u_pro12_sel		<= #udly 1'b1;
					u_pro23_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx:
				begin
					u_pro13_sel		<= #udly 1'b1;
					u_pro23_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx:
				begin
					u_pro14_sel		<= #udly 1'b1;
					u_pro23_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx:
				begin
					u_pro15_sel		<= #udly 1'b1;
					u_pro23_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0000_8000;
				end
			32'bxxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro16_sel		<= #udly 1'b1;
					u_pro23_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001_0000;
				end
			32'bxxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro17_sel		<= #udly 1'b1;
					u_pro23_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002_0000;
				end
			32'bxxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro18_sel		<= #udly 1'b1;
					u_pro23_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004_0000;
				end
			32'bxxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro19_sel		<= #udly 1'b1;
					u_pro23_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008_0000;
				end
			32'bxxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro20_sel		<= #udly 1'b1;
					u_pro23_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010_0000;
				end
			32'bxxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro21_sel		<= #udly 1'b1;
					u_pro23_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020_0000;
				end
			32'bxxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro22_sel		<= #udly 1'b1;
					u_pro23_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040_0000;
				end
			endcase
end
endtask

task judge_24;
begin
		casex (req_bus)
			32'bxxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro25_sel		<= #udly 1'b1;
					u_pro24_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200_0000;
				end
			32'bxxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro26_sel		<= #udly 1'b1;
					u_pro24_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400_0000;
				end
			32'bxxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro27_sel		<= #udly 1'b1;
					u_pro24_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800_0000;
				end
			32'bxxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro28_sel		<= #udly 1'b1;
					u_pro24_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000_0000;
				end
			32'bxx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro29_sel		<= #udly 1'b1;
					u_pro24_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000_0000;
				end
			32'bx1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro30_sel		<= #udly 1'b1;
					u_pro24_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000_0000;
				end
			32'b1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro31_sel		<= #udly 1'b1;
					u_pro24_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h8000_0000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1:
				begin
					u_pro00_sel		<= #udly 1'b1;
					u_pro24_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x:
				begin
					u_pro01_sel		<= #udly 1'b1;
					u_pro24_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx:
				begin
					u_pro02_sel		<= #udly 1'b1;
					u_pro24_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx:
				begin
					u_pro03_sel		<= #udly 1'b1;
					u_pro24_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx:
				begin
					u_pro04_sel		<= #udly 1'b1;
					u_pro24_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx:
				begin
					u_pro05_sel		<= #udly 1'b1;
					u_pro24_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx:
				begin
					u_pro06_sel		<= #udly 1'b1;
					u_pro24_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx:
				begin
					u_pro07_sel		<= #udly 1'b1;
					u_pro24_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx:
				begin
					u_pro08_sel		<= #udly 1'b1;
					u_pro24_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx:
				begin
					u_pro09_sel		<= #udly 1'b1;
					u_pro24_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx:
				begin
					u_pro10_sel		<= #udly 1'b1;
					u_pro24_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx:
				begin
					u_pro11_sel		<= #udly 1'b1;
					u_pro24_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx:
				begin
					u_pro12_sel		<= #udly 1'b1;
					u_pro24_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx:
				begin
					u_pro13_sel		<= #udly 1'b1;
					u_pro24_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx:
				begin
					u_pro14_sel		<= #udly 1'b1;
					u_pro24_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx:
				begin
					u_pro15_sel		<= #udly 1'b1;
					u_pro24_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0000_8000;
				end
			32'bxxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro16_sel		<= #udly 1'b1;
					u_pro24_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001_0000;
				end
			32'bxxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro17_sel		<= #udly 1'b1;
					u_pro24_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002_0000;
				end
			32'bxxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro18_sel		<= #udly 1'b1;
					u_pro24_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004_0000;
				end
			32'bxxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro19_sel		<= #udly 1'b1;
					u_pro24_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008_0000;
				end
			32'bxxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro20_sel		<= #udly 1'b1;
					u_pro24_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010_0000;
				end
			32'bxxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro21_sel		<= #udly 1'b1;
					u_pro24_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020_0000;
				end
			32'bxxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro22_sel		<= #udly 1'b1;
					u_pro24_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040_0000;
				end
			32'bxxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro23_sel		<= #udly 1'b1;
					u_pro24_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080_0000;
				end
			endcase
end
endtask

task judge_25;
begin
	casex (req_bus)
			32'bxxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro26_sel		<= #udly 1'b1;
					u_pro25_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400_0000;
				end
			32'bxxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro27_sel		<= #udly 1'b1;
					u_pro25_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800_0000;
				end
			32'bxxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro28_sel		<= #udly 1'b1;
					u_pro25_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000_0000;
				end
			32'bxx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro29_sel		<= #udly 1'b1;
					u_pro25_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000_0000;
				end
			32'bx1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro30_sel		<= #udly 1'b1;
					u_pro25_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000_0000;
				end
			32'b1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro31_sel		<= #udly 1'b1;
					u_pro25_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h8000_0000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1:
				begin
					u_pro00_sel		<= #udly 1'b1;
					u_pro25_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x:
				begin
					u_pro01_sel		<= #udly 1'b1;
					u_pro25_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx:
				begin
					u_pro02_sel		<= #udly 1'b1;
					u_pro25_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx:
				begin
					u_pro03_sel		<= #udly 1'b1;
					u_pro25_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx:
				begin
					u_pro04_sel		<= #udly 1'b1;
					u_pro25_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx:
				begin
					u_pro05_sel		<= #udly 1'b1;
					u_pro25_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx:
				begin
					u_pro06_sel		<= #udly 1'b1;
					u_pro25_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx:
				begin
					u_pro07_sel		<= #udly 1'b1;
					u_pro25_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx:
				begin
					u_pro08_sel		<= #udly 1'b1;
					u_pro25_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx:
				begin
					u_pro09_sel		<= #udly 1'b1;
					u_pro25_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx:
				begin
					u_pro10_sel		<= #udly 1'b1;
					u_pro25_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx:
				begin
					u_pro11_sel		<= #udly 1'b1;
					u_pro25_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx:
				begin
					u_pro12_sel		<= #udly 1'b1;
					u_pro25_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx:
				begin
					u_pro13_sel		<= #udly 1'b1;
					u_pro25_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx:
				begin
					u_pro14_sel		<= #udly 1'b1;
					u_pro25_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx:
				begin
					u_pro15_sel		<= #udly 1'b1;
					u_pro25_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0000_8000;
				end
			32'bxxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro16_sel		<= #udly 1'b1;
					u_pro25_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001_0000;
				end
			32'bxxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro17_sel		<= #udly 1'b1;
					u_pro25_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002_0000;
				end
			32'bxxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro18_sel		<= #udly 1'b1;
					u_pro25_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004_0000;
				end
			32'bxxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro19_sel		<= #udly 1'b1;
					u_pro25_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008_0000;
				end
			32'bxxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro20_sel		<= #udly 1'b1;
					u_pro25_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010_0000;
				end
			32'bxxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro21_sel		<= #udly 1'b1;
					u_pro25_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020_0000;
				end
			32'bxxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro22_sel		<= #udly 1'b1;
					u_pro25_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040_0000;
				end
			32'bxxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro23_sel		<= #udly 1'b1;
					u_pro25_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080_0000;
				end
			32'bxxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro24_sel		<= #udly 1'b1;
					u_pro25_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100_0000;
				end
			endcase
end
endtask

task judge_26;
begin
	casex (req_bus)
			32'bxxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro27_sel		<= #udly 1'b1;
					u_pro26_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800_0000;
				end
			32'bxxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro28_sel		<= #udly 1'b1;
					u_pro26_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000_0000;
				end
			32'bxx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro29_sel		<= #udly 1'b1;
					u_pro26_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000_0000;
				end
			32'bx1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro30_sel		<= #udly 1'b1;
					u_pro26_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000_0000;
				end
			32'b1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro31_sel		<= #udly 1'b1;
					u_pro26_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h8000_0000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1:
				begin
					u_pro00_sel		<= #udly 1'b1;
					u_pro26_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x:
				begin
					u_pro01_sel		<= #udly 1'b1;
					u_pro26_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx:
				begin
					u_pro02_sel		<= #udly 1'b1;
					u_pro26_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx:
				begin
					u_pro03_sel		<= #udly 1'b1;
					u_pro26_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx:
				begin
					u_pro04_sel		<= #udly 1'b1;
					u_pro26_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx:
				begin
					u_pro05_sel		<= #udly 1'b1;
					u_pro26_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx:
				begin
					u_pro06_sel		<= #udly 1'b1;
					u_pro26_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx:
				begin
					u_pro07_sel		<= #udly 1'b1;
					u_pro26_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx:
				begin
					u_pro08_sel		<= #udly 1'b1;
					u_pro26_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx:
				begin
					u_pro09_sel		<= #udly 1'b1;
					u_pro26_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx:
				begin
					u_pro10_sel		<= #udly 1'b1;
					u_pro26_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx:
				begin
					u_pro11_sel		<= #udly 1'b1;
					u_pro26_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx:
				begin
					u_pro12_sel		<= #udly 1'b1;
					u_pro26_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx:
				begin
					u_pro13_sel		<= #udly 1'b1;
					u_pro26_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx:
				begin
					u_pro14_sel		<= #udly 1'b1;
					u_pro26_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx:
				begin
					u_pro15_sel		<= #udly 1'b1;
					u_pro26_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0000_8000;
				end
			32'bxxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro16_sel		<= #udly 1'b1;
					u_pro26_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001_0000;
				end
			32'bxxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro17_sel		<= #udly 1'b1;
					u_pro26_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002_0000;
				end
			32'bxxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro18_sel		<= #udly 1'b1;
					u_pro26_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004_0000;
				end
			32'bxxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro19_sel		<= #udly 1'b1;
					u_pro26_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008_0000;
				end
			32'bxxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro20_sel		<= #udly 1'b1;
					u_pro26_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010_0000;
				end
			32'bxxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro21_sel		<= #udly 1'b1;
					u_pro26_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020_0000;
				end
			32'bxxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro22_sel		<= #udly 1'b1;
					u_pro26_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040_0000;
				end
			32'bxxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro23_sel		<= #udly 1'b1;
					u_pro26_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080_0000;
				end
			32'bxxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro24_sel		<= #udly 1'b1;
					u_pro26_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100_0000;
				end
			32'bxxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro25_sel		<= #udly 1'b1;
					u_pro26_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200_0000;
				end
			endcase
end
endtask

task judge_27;
begin
	casex (req_bus)
			32'bxxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro28_sel		<= #udly 1'b1;
					u_pro27_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000_0000;
				end
			32'bxx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro29_sel		<= #udly 1'b1;
					u_pro27_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000_0000;
				end
			32'bx1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro30_sel		<= #udly 1'b1;
					u_pro27_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000_0000;
				end
			32'b1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro31_sel		<= #udly 1'b1;
					u_pro27_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h8000_0000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1:
				begin
					u_pro00_sel		<= #udly 1'b1;
					u_pro27_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x:
				begin
					u_pro01_sel		<= #udly 1'b1;
					u_pro27_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx:
				begin
					u_pro02_sel		<= #udly 1'b1;
					u_pro27_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx:
				begin
					u_pro03_sel		<= #udly 1'b1;
					u_pro27_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx:
				begin
					u_pro04_sel		<= #udly 1'b1;
					u_pro27_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx:
				begin
					u_pro05_sel		<= #udly 1'b1;
					u_pro27_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx:
				begin
					u_pro06_sel		<= #udly 1'b1;
					u_pro27_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx:
				begin
					u_pro07_sel		<= #udly 1'b1;
					u_pro27_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx:
				begin
					u_pro08_sel		<= #udly 1'b1;
					u_pro27_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx:
				begin
					u_pro09_sel		<= #udly 1'b1;
					u_pro27_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx:
				begin
					u_pro10_sel		<= #udly 1'b1;
					u_pro27_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx:
				begin
					u_pro11_sel		<= #udly 1'b1;
					u_pro27_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx:
				begin
					u_pro12_sel		<= #udly 1'b1;
					u_pro27_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx:
				begin
					u_pro13_sel		<= #udly 1'b1;
					u_pro27_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx:
				begin
					u_pro14_sel		<= #udly 1'b1;
					u_pro27_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx:
				begin
					u_pro15_sel		<= #udly 1'b1;
					u_pro27_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0000_8000;
				end
			32'bxxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro16_sel		<= #udly 1'b1;
					u_pro27_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001_0000;
				end
			32'bxxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro17_sel		<= #udly 1'b1;
					u_pro27_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002_0000;
				end
			32'bxxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro18_sel		<= #udly 1'b1;
					u_pro27_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004_0000;
				end
			32'bxxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro19_sel		<= #udly 1'b1;
					u_pro27_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008_0000;
				end
			32'bxxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro20_sel		<= #udly 1'b1;
					u_pro27_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010_0000;
				end
			32'bxxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro21_sel		<= #udly 1'b1;
					u_pro27_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020_0000;
				end
			32'bxxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro22_sel		<= #udly 1'b1;
					u_pro27_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040_0000;
				end
			32'bxxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro23_sel		<= #udly 1'b1;
					u_pro27_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080_0000;
				end
			32'bxxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro24_sel		<= #udly 1'b1;
					u_pro27_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100_0000;
				end
			32'bxxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro25_sel		<= #udly 1'b1;
					u_pro27_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200_0000;
				end
			32'bxxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro26_sel		<= #udly 1'b1;
					u_pro27_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400_0000;
				end
			endcase
end
endtask

task judge_28;
begin
	casex (req_bus)
			32'bxx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro29_sel		<= #udly 1'b1;
					u_pro28_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000_0000;
				end
			32'bx1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro30_sel		<= #udly 1'b1;
					u_pro28_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000_0000;
				end
			32'b1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro31_sel		<= #udly 1'b1;
					u_pro28_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h8000_0000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1:
				begin
					u_pro00_sel		<= #udly 1'b1;
					u_pro28_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x:
				begin
					u_pro01_sel		<= #udly 1'b1;
					u_pro28_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx:
				begin
					u_pro02_sel		<= #udly 1'b1;
					u_pro28_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx:
				begin
					u_pro03_sel		<= #udly 1'b1;
					u_pro28_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx:
				begin
					u_pro04_sel		<= #udly 1'b1;
					u_pro28_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx:
				begin
					u_pro05_sel		<= #udly 1'b1;
					u_pro28_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx:
				begin
					u_pro06_sel		<= #udly 1'b1;
					u_pro28_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx:
				begin
					u_pro07_sel		<= #udly 1'b1;
					u_pro28_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx:
				begin
					u_pro08_sel		<= #udly 1'b1;
					u_pro28_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx:
				begin
					u_pro09_sel		<= #udly 1'b1;
					u_pro28_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx:
				begin
					u_pro10_sel		<= #udly 1'b1;
					u_pro28_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx:
				begin
					u_pro11_sel		<= #udly 1'b1;
					u_pro28_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx:
				begin
					u_pro12_sel		<= #udly 1'b1;
					u_pro28_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx:
				begin
					u_pro13_sel		<= #udly 1'b1;
					u_pro28_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx:
				begin
					u_pro14_sel		<= #udly 1'b1;
					u_pro28_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx:
				begin
					u_pro15_sel		<= #udly 1'b1;
					u_pro28_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0000_8000;
				end
			32'bxxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro16_sel		<= #udly 1'b1;
					u_pro28_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001_0000;
				end
			32'bxxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro17_sel		<= #udly 1'b1;
					u_pro28_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002_0000;
				end
			32'bxxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro18_sel		<= #udly 1'b1;
					u_pro28_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004_0000;
				end
			32'bxxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro19_sel		<= #udly 1'b1;
					u_pro28_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008_0000;
				end
			32'bxxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro20_sel		<= #udly 1'b1;
					u_pro28_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010_0000;
				end
			32'bxxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro21_sel		<= #udly 1'b1;
					u_pro28_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020_0000;
				end
			32'bxxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro22_sel		<= #udly 1'b1;
					u_pro28_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040_0000;
				end
			32'bxxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro23_sel		<= #udly 1'b1;
					u_pro28_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080_0000;
				end
			32'bxxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro24_sel		<= #udly 1'b1;
					u_pro28_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100_0000;
				end
			32'bxxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro25_sel		<= #udly 1'b1;
					u_pro28_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200_0000;
				end
			32'bxxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro26_sel		<= #udly 1'b1;
					u_pro28_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400_0000;
				end
			32'bxxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro27_sel		<= #udly 1'b1;
					u_pro28_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800_0000;
				end
			endcase
end
endtask

task judge_29;
begin
	casex (req_bus)
			32'bx1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro30_sel		<= #udly 1'b1;
					u_pro29_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000_0000;
				end
			32'b1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro31_sel		<= #udly 1'b1;
					u_pro29_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h8000_0000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1:
				begin
					u_pro00_sel		<= #udly 1'b1;
					u_pro29_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x:
				begin
					u_pro01_sel		<= #udly 1'b1;
					u_pro29_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx:
				begin
					u_pro02_sel		<= #udly 1'b1;
					u_pro29_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx:
				begin
					u_pro03_sel		<= #udly 1'b1;
					u_pro29_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx:
				begin
					u_pro04_sel		<= #udly 1'b1;
					u_pro29_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx:
				begin
					u_pro05_sel		<= #udly 1'b1;
					u_pro29_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx:
				begin
					u_pro06_sel		<= #udly 1'b1;
					u_pro29_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx:
				begin
					u_pro07_sel		<= #udly 1'b1;
					u_pro29_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx:
				begin
					u_pro08_sel		<= #udly 1'b1;
					u_pro29_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx:
				begin
					u_pro09_sel		<= #udly 1'b1;
					u_pro29_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx:
				begin
					u_pro10_sel		<= #udly 1'b1;
					u_pro29_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx:
				begin
					u_pro11_sel		<= #udly 1'b1;
					u_pro29_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx:
				begin
					u_pro12_sel		<= #udly 1'b1;
					u_pro29_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx:
				begin
					u_pro13_sel		<= #udly 1'b1;
					u_pro29_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx:
				begin
					u_pro14_sel		<= #udly 1'b1;
					u_pro29_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx:
				begin
					u_pro15_sel		<= #udly 1'b1;
					u_pro29_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0000_8000;
				end
			32'bxxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro16_sel		<= #udly 1'b1;
					u_pro29_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001_0000;
				end
			32'bxxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro17_sel		<= #udly 1'b1;
					u_pro29_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002_0000;
				end
			32'bxxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro18_sel		<= #udly 1'b1;
					u_pro29_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004_0000;
				end
			32'bxxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro19_sel		<= #udly 1'b1;
					u_pro29_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008_0000;
				end
			32'bxxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro20_sel		<= #udly 1'b1;
					u_pro29_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010_0000;
				end
			32'bxxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro21_sel		<= #udly 1'b1;
					u_pro29_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020_0000;
				end
			32'bxxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro22_sel		<= #udly 1'b1;
					u_pro29_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040_0000;
				end
			32'bxxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro23_sel		<= #udly 1'b1;
					u_pro29_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080_0000;
				end
			32'bxxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro24_sel		<= #udly 1'b1;
					u_pro29_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100_0000;
				end
			32'bxxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro25_sel		<= #udly 1'b1;
					u_pro29_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200_0000;
				end
			32'bxxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro26_sel		<= #udly 1'b1;
					u_pro29_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400_0000;
				end
			32'bxxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro27_sel		<= #udly 1'b1;
					u_pro29_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800_0000;
				end
			32'bxxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro28_sel		<= #udly 1'b1;
					u_pro29_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000_0000;
				end
			endcase
end
endtask

task judge_30;
begin
	casex (req_bus)
			32'b1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro31_sel		<= #udly 1'b1;
					u_pro30_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h8000_0000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1:
				begin
					u_pro00_sel		<= #udly 1'b1;
					u_pro30_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x:
				begin
					u_pro01_sel		<= #udly 1'b1;
					u_pro30_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx:
				begin
					u_pro02_sel		<= #udly 1'b1;
					u_pro30_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx:
				begin
					u_pro03_sel		<= #udly 1'b1;
					u_pro30_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx:
				begin
					u_pro04_sel		<= #udly 1'b1;
					u_pro30_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx:
				begin
					u_pro05_sel		<= #udly 1'b1;
					u_pro30_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx:
				begin
					u_pro06_sel		<= #udly 1'b1;
					u_pro30_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx:
				begin
					u_pro07_sel		<= #udly 1'b1;
					u_pro30_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx:
				begin
					u_pro08_sel		<= #udly 1'b1;
					u_pro30_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx:
				begin
					u_pro09_sel		<= #udly 1'b1;
					u_pro30_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx:
				begin
					u_pro10_sel		<= #udly 1'b1;
					u_pro30_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx:
				begin
					u_pro11_sel		<= #udly 1'b1;
					u_pro30_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx:
				begin
					u_pro12_sel		<= #udly 1'b1;
					u_pro30_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx:
				begin
					u_pro13_sel		<= #udly 1'b1;
					u_pro30_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx:
				begin
					u_pro14_sel		<= #udly 1'b1;
					u_pro30_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx:
				begin
					u_pro15_sel		<= #udly 1'b1;
					u_pro30_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0000_8000;
				end
			32'bxxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro16_sel		<= #udly 1'b1;
					u_pro30_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001_0000;
				end
			32'bxxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro17_sel		<= #udly 1'b1;
					u_pro30_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002_0000;
				end
			32'bxxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro18_sel		<= #udly 1'b1;
					u_pro30_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004_0000;
				end
			32'bxxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro19_sel		<= #udly 1'b1;
					u_pro30_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008_0000;
				end
			32'bxxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro20_sel		<= #udly 1'b1;
					u_pro30_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010_0000;
				end
			32'bxxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro21_sel		<= #udly 1'b1;
					u_pro30_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020_0000;
				end
			32'bxxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro22_sel		<= #udly 1'b1;
					u_pro30_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040_0000;
				end
			32'bxxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro23_sel		<= #udly 1'b1;
					u_pro30_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080_0000;
				end
			32'bxxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro24_sel		<= #udly 1'b1;
					u_pro30_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100_0000;
				end
			32'bxxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro25_sel		<= #udly 1'b1;
					u_pro30_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200_0000;
				end
			32'bxxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro26_sel		<= #udly 1'b1;
					u_pro30_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400_0000;
				end
			32'bxxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro27_sel		<= #udly 1'b1;
					u_pro30_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800_0000;
				end
			32'bxxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro28_sel		<= #udly 1'b1;
					u_pro30_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000_0000;
				end
			32'bxx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro29_sel		<= #udly 1'b1;
					u_pro30_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000_0000;
				end
			endcase
end
endtask

task judge_31;
begin
	casex (req_bus)
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1:
				begin
					u_pro00_sel		<= #udly 1'b1;
					u_pro31_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x:
				begin
					u_pro01_sel		<= #udly 1'b1;
					u_pro31_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx:
				begin
					u_pro02_sel		<= #udly 1'b1;
					u_pro31_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx:
				begin
					u_pro03_sel		<= #udly 1'b1;
					u_pro31_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx:
				begin
					u_pro04_sel		<= #udly 1'b1;
					u_pro31_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx:
				begin
					u_pro05_sel		<= #udly 1'b1;
					u_pro31_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx:
				begin
					u_pro06_sel		<= #udly 1'b1;
					u_pro31_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx:
				begin
					u_pro07_sel		<= #udly 1'b1;
					u_pro31_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx:
				begin
					u_pro08_sel		<= #udly 1'b1;
					u_pro31_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx:
				begin
					u_pro09_sel		<= #udly 1'b1;
					u_pro31_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx:
				begin
					u_pro10_sel		<= #udly 1'b1;
					u_pro31_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx:
				begin
					u_pro11_sel		<= #udly 1'b1;
					u_pro31_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx:
				begin
					u_pro12_sel		<= #udly 1'b1;
					u_pro31_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx:
				begin
					u_pro13_sel		<= #udly 1'b1;
					u_pro31_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx:
				begin
					u_pro14_sel		<= #udly 1'b1;
					u_pro31_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000;
				end
			32'bxxxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx:
				begin
					u_pro15_sel		<= #udly 1'b1;
					u_pro31_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0000_8000;
				end
			32'bxxxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro16_sel		<= #udly 1'b1;
					u_pro31_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0001_0000;
				end
			32'bxxxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro17_sel		<= #udly 1'b1;
					u_pro31_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0002_0000;
				end
			32'bxxxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro18_sel		<= #udly 1'b1;
					u_pro31_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0004_0000;
				end
			32'bxxxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro19_sel		<= #udly 1'b1;
					u_pro31_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0008_0000;
				end
			32'bxxxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro20_sel		<= #udly 1'b1;
					u_pro31_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0010_0000;
				end
			32'bxxxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro21_sel		<= #udly 1'b1;
					u_pro31_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0020_0000;
				end
			32'bxxxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro22_sel		<= #udly 1'b1;
					u_pro31_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0040_0000;
				end
			32'bxxxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro23_sel		<= #udly 1'b1;
					u_pro31_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0080_0000;
				end
			32'bxxxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro24_sel		<= #udly 1'b1;
					u_pro31_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0100_0000;
				end
			32'bxxxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro25_sel		<= #udly 1'b1;
					u_pro31_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0200_0000;
				end
			32'bxxxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro26_sel		<= #udly 1'b1;
					u_pro31_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0400_0000;
				end
			32'bxxxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro27_sel		<= #udly 1'b1;
					u_pro31_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h0800_0000;
				end
			32'bxxx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro28_sel		<= #udly 1'b1;
					u_pro31_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h1000_0000;
				end
			32'bxx1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro29_sel		<= #udly 1'b1;
					u_pro31_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h2000_0000;
				end
			32'bx1xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx:
				begin
					u_pro30_sel		<= #udly 1'b1;
					u_pro31_sel		<= #udly 1'b0;
					pass_ring 		<= #udly 32'h4000_0000;
				end
			endcase
end
endtask

`endprotect

endmodule
