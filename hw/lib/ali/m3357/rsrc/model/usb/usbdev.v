/*****************************************************************************
          (c) copyrights 2000-. All rights reserved

           T-Square Design, Inc.

          Company confidential and Properietary information.
          This information may not be disclosed to unauthorized
          individual.
 *****************************************************************************/

/*****************************************************************************
 *
 *    FILE NAME:  	usbdev.v
 *
 *    DESCRIPTION: 	usb device behavior module
 *
 *
 *    AUTHOR:  		Daniel Liu
 *
 *****************************************************************************/
//`timescale 1ps/100fs
`define		USB_HANDLE	top.usb_fn

module usbdev(
/*	tx_se0,
	tx_oe,
	dp_out,
	rx_se0,
	rx_d,*/
	dpls,
	dmns
);

/*output	tx_se0,
		tx_oe,
		dp_out;
input	rx_se0,
		rx_d;*/
inout   dpls;
inout   dmns;

parameter	udly = 1.000;
parameter	period_12m = 83.336;
parameter	half_period_12m = period_12m/2;

parameter	st_idle = 0;
//parameter	st_sync = 1;
parameter	st_reset = 2;
parameter	st_pid = 3;
parameter	st_data = 4;
parameter	st_addr = 5;
parameter	st_endp = 6;
parameter	st_frame_num = 7;
parameter	st_crc = 8;
parameter	st_se0 = 9;

parameter	pid_in	= 4'b1001;
parameter	pid_out	= 4'b0001;
parameter	pid_sof	= 4'b0101;
parameter	pid_setup	= 4'b1101;
parameter	pid_data0	= 4'b0011;
parameter	pid_data1	= 4'b1011;
parameter	pid_ack		= 4'b0010;
parameter	pid_nak		= 4'b1010;
parameter	pid_stall	= 4'b1110;

integer		i, j;// fn;

wire			dp_in;
wire			dm_in;
reg				dp_out;
reg				tx_oe, tx_se0;
wire			se0;
reg		[4:0]	state;
reg				clk_12m_tmp;
wire			clk_12m;
reg		[15:0]	shift_in_data;
reg		[3:0]	shift_count;
reg				dp_out_nrzi;
reg				tx_oe_nrzi, tx_se0_nrzi;
reg				tmp_var;
reg		[7:0]	rx_buffer[1025:0];
reg		[12:0]	rx_buf_ptr;
reg		[7:0]	tx_buffer[1023:0];
reg				endp_iso;

tri1			dp_idle;

reg		usbdev_connect;
tranif1(dp_idle, dpls, usbdev_connect);

`ifdef LOW_SPEED
tri1			dm_idle;

reg		usbdev_connect_ls;
tranif1(dm_idle, dmns, usbdev_connect_ls);
`endif

reg		slect_port;


assign		se0 = ~(dpls | dmns);
assign		dp_in = dpls;
assign		dpls = tx_oe ? (tx_se0 ? 1'b0 : dp_out): 1'bz;
assign		dmns = tx_oe ? (tx_se0 ? 1'b0 : ~dp_out) : 1'bz;
/*usb_pad usb_pad(
		.se0	(tx_se0	),
		.oeb	(~tx_oe	),
		.a		(dp_out		),
		.h_lb	(1'b1	),
		.dp		(dpls		),
		.dm		(dmns		),
		.se0d	(se0	),
		.rcvd	(dp_in		),
		.ant	(1'b1			),
		.ynt	(				)
);*/

reg			tx_se0_d;
always	@(posedge clk_12m)
begin
	if (tx_oe_nrzi)
	begin
		if (~dp_out_nrzi)
			dp_out <= ~dp_out;
	end
	else
			dp_out <= 1;
//	tx_se0 <= tx_se0_d;
end

always	@(posedge clk_12m)
begin
	tx_oe <= tx_oe_nrzi;
	tx_se0 <= tx_se0_nrzi;
end
reg		tmp1;

initial
begin
	endp_iso = 1'b0;
	tmp1=0;
	rx_buf_ptr = 0;
	usbdev_connect = 0;
	clk_12m_tmp = 0;
	state = st_idle;
	shift_count = 0;
	shift_in_data = 0;
	tx_oe = 0;
	tx_se0 = 0;
	tx_oe_nrzi = 0;
	tx_se0_nrzi = 0;
	dp_out = 1;
	tmp_var = 0;
	slect_port = 1;
//	fn = $fopen("usbdev.rpt");
end

// 12M clock
always
	#half_period_12m clk_12m_tmp = ~clk_12m_tmp;

assign	#udly clk_12m = clk_12m_tmp;

// detect reset signal
always
begin
	wait (se0)
	for (j=0; j<30; j=j+1)
		#period_12m;
	if (se0)
		$fdisplay(`USB_HANDLE, "detect reset on bus");
	wait (~se0);
end

reg		dp_in_d;
reg		se0_d;
wire	nrzi_data_tmp = ~(dp_in_d ^ dp_in);
reg		nrzi_data;
reg		[3:0]	pid;
reg		[10:0]	frame_num;
reg		[6:0]	addr;
reg		[3:0]	endp;
reg		[7:0]	data[1023:0];

always	@(posedge clk_12m)
begin
	dp_in_d = #udly dp_in;
	se0_d = #udly ~se0;
end

reg		[5:0]	bit_stuff;

always @(posedge clk_12m)
	nrzi_data = #udly nrzi_data_tmp;

always @(posedge clk_12m)
begin
	if (nrzi_data && (state != st_idle))
	begin
		bit_stuff <= #udly bit_stuff + 1;
	end else
		bit_stuff <= #udly 0;
end

// if device transmitting, don't shift data in, and if bit stuffing occur, hold
// the shift in data
always @(posedge clk_12m)
	if (tx_oe)
		shift_in_data <= #udly 16'hffff;
	else if (bit_stuff == 6)
	begin
		shift_in_data <= #udly shift_in_data;
		$fdisplay(`USB_HANDLE, "bit stuffing occur");
		if (nrzi_data == 0)
			bit_stuff <= #udly 0;
	end	else
		shift_in_data[15:0] = #udly {nrzi_data, shift_in_data[15:1]};

wire	bit_stuff_err = ((bit_stuff > 6) || (bit_stuff == 6) && (nrzi_data == 1)) &&
		(state != st_idle) && (state != st_se0);
always @(posedge clk_12m)
	if (bit_stuff_err)
		$fdisplay(`USB_HANDLE, "bit stuffing error occur!");

// while got 16 consecutive '1', restore from error.
always @(posedge clk_12m)
	if (bit_stuff == 16)
	begin
		bit_stuff = 0;
	end

// state machine
always @(posedge clk_12m)
	if (bit_stuff != 6)
		case (state)
			st_idle:
					if (shift_in_data[15:8] == 8'h80)
					begin
						state = #udly st_pid;
						shift_count = #udly 0;
					end
			st_pid: get_pid(state);
			st_addr: get_addr(state);
			st_endp: get_endp(state);
			st_frame_num: get_frame_num(state);
			st_crc: get_crc(state);
			st_se0:
				begin
					get_se0(state);
					tmp1 = (!endp_iso) && ( (pid == pid_data0) || (pid == pid_data1) );
					if (tmp1 && slect_port)
						send_ack;

					if (pid == pid_in)
					begin
						tx_buffer[0] = 8'h55;
						send_data(1);
					end
					rx_buf_ptr = 0;



				end
			st_data: get_data(state);
		endcase

task get_pid;
inout	[4:0]	state;
integer	i;
begin
	for (i=0; i<7; i=i+1)
		#period_12m;
	pid = shift_in_data[11:8];
	$fwrite(`USB_HANDLE, "pid=%b (%h), ", pid, pid);
	case (pid)
		pid_sof: $fdisplay(`USB_HANDLE, "SOF packet");
		pid_setup: $fdisplay(`USB_HANDLE, "SETUP packet");
		pid_out: $fdisplay(`USB_HANDLE, "OUT packet");
		pid_in: $fdisplay(`USB_HANDLE, "IN packet");
		pid_data0: $fdisplay(`USB_HANDLE, "DATA0 packet");
		pid_data1: $fdisplay(`USB_HANDLE, "DATA1 packet");
		pid_ack: $fdisplay(`USB_HANDLE, "ACK packet");
		pid_nak: $fdisplay(`USB_HANDLE, "NAK packet");
		pid_stall: $fdisplay(`USB_HANDLE, "STALL packet");
		default:
			begin
				$fdisplay(`USB_HANDLE, "??? packet");
				state = #udly st_addr;
			end
	endcase
	case (pid)
		pid_sof: state = #udly st_frame_num;
		pid_setup: state = #udly st_addr;
		pid_data0: state = #udly st_data;
		pid_data1: state = #udly st_data;
		pid_ack: state = #udly st_se0;
		pid_nak: state = #udly st_se0;
		pid_stall: state = #udly st_se0;
		default:
			state = #udly st_addr;
	endcase
end
endtask

task delay_bits;
input	[4:0]	bits;
integer	i;
begin
	for (i=0; i<bits; i=i+1)
	begin
//		tmp_var = ~tmp_var;
		#period_12m;
		if (bit_stuff == 6)
			#period_12m;
	end
end
endtask

task get_addr;
inout	[4:0]	state;
begin
	delay_bits(6);
	addr = shift_in_data[15:9];
	$fdisplay(`USB_HANDLE, "addr=%b (%h)", addr, addr);
	state = #udly st_endp;
end
endtask

task get_endp;
inout	[4:0]	state;
integer	i;
begin
	delay_bits(3);
	endp = shift_in_data[15:12];
	$fdisplay(`USB_HANDLE, "endp=%b (%h)", endp, endp);
	state = #udly st_crc;
end
endtask

task get_frame_num;
inout	[4:0]	state;
integer	i;
begin
	delay_bits(10);
	frame_num = shift_in_data[15:5];
	$fdisplay(`USB_HANDLE, "frame_num=%b (%h)", frame_num, frame_num);
	state = #udly st_crc;
end
endtask

task get_crc;
inout	[4:0]	state;
integer	i;
begin
	delay_bits(4);
	$fdisplay(`USB_HANDLE, "crc=%b (%h)", shift_in_data[15:11], shift_in_data[15:11]);
	state = #udly st_se0;
	check_crc5(shift_in_data);
end
endtask

task get_se0;
inout	[4:0]	state;
begin
	#period_12m;
	$fdisplay(`USB_HANDLE, "-------------");
	state = #udly st_idle;
end
endtask

task get_data;
inout	[4:0]	state;
integer	i;
reg		[7:0]	data;
begin
	delay_bits(7);
	if (bit_stuff_err)
		state = st_idle;
	else
	begin
		data = shift_in_data[15:8];
		rx_buffer[rx_buf_ptr] = data;
		rx_buf_ptr = rx_buf_ptr + 1;
		$fdisplay(`USB_HANDLE, "data=%b (%h), rx_buf_ptr=%d", data, data, rx_buf_ptr);
		if (se0_d)
			state = #udly st_data;
		else
		begin
			check_crc16(rx_buf_ptr);
			state = #udly st_se0;
		end
	end
end
endtask

task send_ack;
begin
	#period_12m;
	#period_12m;
	tx_oe_nrzi = 1;
	send_sync;
	send_pid(pid_ack);
	send_eop;
	tx_oe_nrzi = 0;
end
endtask

task send_sync;
reg		[7:0]	data;
integer			i;
begin
	data = 8'h80;
	for (i=0; i<8; i=i+1)
	begin
		dp_out_nrzi = data[i];
		#period_12m;
	end
end
endtask

task send_pid;
input	[3:0]	pid;
reg		[7:0]	data;
integer			i;
begin
	data = {~pid, pid};
	$fdisplay(`USB_HANDLE, "send pid=%b (%h)", pid, pid);
	for (i=0; i<8; i=i+1)
	begin
		tmp_var = ~tmp_var;
//		$fdisplay(`USB_HANDLE, $realtime, " send pid=%b, %h", pid, tmp_var);
		dp_out_nrzi = data[i];
		#period_12m;
	end
end
endtask

task send_eop;
begin
	tx_se0_nrzi = 1;
	#period_12m;
	#period_12m;
	tx_se0_nrzi = 0;
	$fdisplay(`USB_HANDLE, "-------------");
end
endtask

task check_crc5;
input	[15:0]	crc_data;
reg		[4:0]	shift_reg;
reg		[4:0]	gen_poly;
reg				tmp;
integer			i;
begin
	gen_poly = 5'b00101;
	shift_reg = 5'b11111;
	for (i=0; i<16; i=i+1)
	begin
		tmp = shift_reg[4] ^ crc_data[i];
		shift_reg = {shift_reg[3:0], 1'b0};
		if (tmp)
			shift_reg = shift_reg ^ gen_poly;
	end
	if (shift_reg == 5'b01100)
		$fdisplay(`USB_HANDLE, "CRC ok!");
	else
		$fdisplay(`USB_HANDLE, "CRC Fail!");
end
endtask

task check_crc16;
input	[12:0]		data_num;
reg		[8207:0]	crc_data;
reg		[15:0]	shift_reg;
reg		[15:0]	gen_poly;
reg				tmp;
integer			i, j;
begin
	gen_poly = 16'b1000_0000_0000_0101;
	shift_reg = 16'b1111_1111_1111_1111;
	for (i=data_num-1; i>=0; i=i-1)
	begin
		crc_data = {crc_data[8199:0], 8'b0};
		crc_data[7:0] = rx_buffer[i];
	end
	for (i=0; i<data_num*8; i=i+1)
	begin
		tmp = shift_reg[15] ^ crc_data[i];
		shift_reg = {shift_reg[14:0], 1'b0};
		if (tmp)
			shift_reg = shift_reg ^ gen_poly;
	end
	if (shift_reg == 16'b1000_0000_0000_1101)
		$fdisplay(`USB_HANDLE, "CRC ok!");
	else
		$fdisplay(`USB_HANDLE, "CRC Fail!");
end
endtask

function [15:0] calc_crc16;
input	[12:0]		data_num;
reg		[8207:0]	crc_data;
reg		[15:0]	shift_reg;
reg		[15:0]	gen_poly;
reg				tmp;
integer			i, j;
begin
	gen_poly = 16'b1000_0000_0000_0101;
	shift_reg = 16'b1111_1111_1111_1111;
	for (i=data_num-1; i>=0; i=i-1)
	begin
		crc_data = {crc_data[8199:0], 8'b0};
		crc_data[7:0] = tx_buffer[i];
	end
	for (i=0; i<data_num*8; i=i+1)
	begin
		tmp = shift_reg[15] ^ crc_data[i];
		shift_reg = {shift_reg[14:0], 1'b0};
		if (tmp)
			shift_reg = shift_reg ^ gen_poly;
//$fdisplay(`USB_HANDLE, "shift_reg=%h", shift_reg);
	end
	shift_reg = ~shift_reg;
	calc_crc16 = {shift_reg[0], shift_reg[1], shift_reg[2], shift_reg[3],
				shift_reg[4], shift_reg[5], shift_reg[6], shift_reg[7],
				shift_reg[8], shift_reg[9], shift_reg[10], shift_reg[11],
				shift_reg[12], shift_reg[13], shift_reg[14], shift_reg[15]};
end
endfunction

task send_byte;
input	[7:0]	byte;
integer			i;
begin
	$fdisplay(`USB_HANDLE, "send %h", byte);
	for (i=0; i<8; i=i+1)
	begin
		dp_out_nrzi = byte[i];
		#period_12m;
	end
end
endtask

task send_data;
input	[11:0] data_num;
integer			i;
reg		[15:0]	crc;
begin
	$display(`USB_HANDLE, "send_data");
	#period_12m;
	#period_12m;
	tx_oe_nrzi = 1;
	send_sync;
	send_pid(pid_data0);
	for (i=0; i<data_num; i=i+1)
		send_byte(tx_buffer[i]);
	crc = calc_crc16(data_num);
	send_byte(crc[7:0]);
	send_byte(crc[15:8]);
	send_eop;
	tx_oe_nrzi = 0;
end
endtask

endmodule

 /**********************history****************************

 2001-09-26		first create

 *********************************************************/
