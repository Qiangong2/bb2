
//disp
`define QDWIDTH 44
`define QCWIDTH 13
`define EXCPINFO_WIDTH 41
`define PERFINFO_WIDTH 13
`define QPERFINFOW 9
`define QEXCPINFOW 35

//dc_sram
`define addr_width  12
`define dcd_size    64
`define dct_size    27
`define line_depth  128
`define u_dly       1
`define lru_wide    2

//decode
`define REG_WIDTH 202
`define INST_WIDTH 84

//ic_sram
//`define addr_width      12  //  address width
`define icd_size        72  //  i cache data size
`define ict_size        22  //  i cache tag size
`define wide            2   //  i cache lru sram wide
`define hit_vec_dly     20  //  i cache hit_vec delay for rsim

//reg_file
`define RFD_WIDTH 32
`define RFA_WIDTH 5
`define RFAD_WIDTH 32
`define RFST_WIDTH 3
