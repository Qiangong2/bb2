/*******************************************************************
          (c) copyrights 1997-1998. All rights reserved

           T-Square Design, Inc.

          Company confidential and Properietary information.
          This information may not be disclosed to unauthorized
          individual.
*******************************************************************/
/*******************************************************************
 *
 *    DESCRIPTION: PCI device integeration, include pci host, usb, ide
 *				   and pci arbiter
 *    AUTHOR:      Runner
 *
 *    HISTORY:	06/14/02	edit for m6304  -- Runner
 *				06/15/02	sdram parameter is no more in pci host config
 *				10/09/02	Delete the IDE IP and corresponding signals/intf
 				2003-08-27	Remove the GATX20 for USB.
 				2003-08-28	Connect the second external PCI_MASTER request to
 							always active. Which is the default and lowest
 							request
 *******************************************************************/

`timescale 1ps/1ps

module	p_biu(
// external pci bus	output
	OE_AD_		,
	OUT_AD		,
	OE_CBE_		,
	OUT_CBE_	,
	OE_PAR_		,
	OUT_PAR		,
	OE_FRAME_	,
	OUT_FRAME_	,
	OE_IRDY_	,
	OUT_IRDY_	,
	OE_DEVSEL_	,
	OUT_DEVSEL_	,
	OE_TRDY_	,
	OUT_TRDY_	,
	OE_STOP_	,
	OUT_STOP_	,
	OUT_GNT_	, // only provide one request on external pci bus in m6304 -- runner

// Input from External pci Bus
	IN_AD		,
	IN_CBE_		,
	IN_PAR		,
	IN_FRAME_	,
	IN_IRDY_	,
	IN_DEVSEL_	,
	IN_TRDY_	,
	IN_STOP_	,
	IN_REQ_		, // only provide one request on external pci bus in m6304 -- runner

// North Bridge to pci host interface
	PBIU_REQ	,
	PBIU_GNT	,
	PBIU_RW		,
	PBIU_BL		,
	PBIU_BE		,
	PBIU_WR_RDY	,
	PBIU_RD_RDY	,
	PBIU_LAST	,
	PBIU_ERR	,
	PBIU_ADDR	,
	PBIU_WR_DATA,
	PBIU_RD_DATA,

	P_CFG_ADDR	,
	P_CFG_BE	,
	P_CFG_DIN	,
	P_CFG_DOUT	,
	P_CFG_RW	,
	P_CFG_CS	,
	P_CFG_GNT	,

// PCI to sdram controller read cycle interface
	PCI_RAM_RREQ	,
	PCI_RAM_RACK	,
	PCI_RAM_RBL		,
	PCI_RAM_RRDY	,
	PCI_RAM_RLAST	,
	PCI_RAM_RERR	,
	PCI_RAM_RADDR	,
	PCI_RAM_RDATA	,

// PCI Posted Write FIFO interface
	PCI_RAM_WREQ	,
	PCI_RAM_WACK	,
	PCI_RAM_WBL		,
	PCI_RAM_WBE		,
	PCI_RAM_WRDY	,
	PCI_RAM_WLAST	,
	PCI_RAM_WERR	,
	PCI_RAM_WADDR	,
	PCI_RAM_WDATA	,


	PCI_PAR_ERR	,


// USB interface
	P1_LS_MODE	,
	P1_TXD		,
	P1_TXD_OEJ	,
	P1_TXD_SE0	,
	P2_LS_MODE	,
	P2_TXD		,
	P2_TXD_OEJ	,
	P2_TXD_SE0	,
/*
	p3_ls_mode	,
	p3_txd		,
	p3_txd_oej	,
	p3_txd_se0	,
	p4_ls_mode	,
	p4_txd		,
	p4_txd_oej	,
	p4_txd_se0	,
*/
	I_P1_RXD	,
	I_P1_RXD_SE0,
	I_P2_RXD	,
	I_P2_RXD_SE0,
/*
	i_p3_rxd	,
	i_p3_rxd_se0,
	i_p4_rxd	,
	i_p4_rxd_se0,
*/
	USB_PON_	,	// POWER CONTROL
	USB_CLK		,	// 48m CLOCK
	USB_OVERCUR_,	// OVER CURRENT
	USB_CLK_EN	,
	USB_INT_	,	// USB interrupt output

// IDE interface
	PCI_MODE	,
	PCI_MODE_IDE_EN	,
	PINS_STOP	,
	PINS_IDLE	,
/*
	ch2_detect		, // 40 lines or 80 line cable select
	atadiow_out_	,
	atadior_out_	,
	atacs0_out_		,
	atacs1_out_		,
	atadd_out		,
	atadd_oe_		,
	atada_out		,
//	atareset_out_	,
	atadmack_out_	,
	ataiordy_in		,
	ataintrq_in		,
	atadd_in		,
	atadmarq_in		,
	ide_int_		,
*/
// system input
	TESTMD		,

	PCI_CLK		,		// pci cLOCK	--	33mhZ
//	IDE100M_CLK	,
//	IDE66M_CLK	,
	MEM_CLK		,		// cHIPSET cLOCK
	RST_
	);


//////////////////////////////////////////////////////////////////////
// PCI ousput Signals
output			OE_AD_	;
output	[31:0]	OUT_AD	;
output			OE_CBE_	;
output	[3:0]	OUT_CBE_;
output			OE_PAR_,    OUT_PAR,
				OE_FRAME_,  OUT_FRAME_,
				OE_IRDY_, 	OUT_IRDY_,
				OE_DEVSEL_, OUT_DEVSEL_,
				OE_TRDY_,   OUT_TRDY_,
				OE_STOP_,   OUT_STOP_;

// external PCI bus	input
input  [31:0]	IN_AD		;
input  [ 3:0]	IN_CBE_		;
input			IN_PAR		,
				IN_FRAME_	,
				IN_IRDY_	,
				IN_DEVSEL_	,
				IN_TRDY_	,
				IN_STOP_	;

output	[1:0]	OUT_GNT_	;
input	[1:0]	IN_REQ_		;

// North Bridge to pci host interface
input			PBIU_REQ	;
output			PBIU_GNT	;
input			PBIU_RW		;
input	[1:0]	PBIU_BL		;
input	[3:0]	PBIU_BE		;
output			PBIU_WR_RDY	,
				PBIU_RD_RDY	,
				PBIU_LAST	,
				PBIU_ERR	;
input	[31:0]	PBIU_WR_DATA;
output	[31:0]	PBIU_RD_DATA;
input	[29:2]	PBIU_ADDR	;

input			P_CFG_CS	;
output			P_CFG_GNT	;
input			P_CFG_RW	;
input	[3:0]	P_CFG_BE	;
output	[31:0]	P_CFG_DIN	;
input	[31:0]	P_CFG_DOUT	;
input	[31:0]	P_CFG_ADDR	;

//*******************************************************************
	// PCI read cycle interface
output	 		PCI_RAM_RREQ	;
input			PCI_RAM_RACK	;
output	[1:0]	PCI_RAM_RBL		;
input			PCI_RAM_RRDY	,
				PCI_RAM_RLAST	,
				PCI_RAM_RERR	;
input	[31:0]	PCI_RAM_RDATA	;
output	[29:2]	PCI_RAM_RADDR	;

	// PCI Posted Write FIFO interface
output			PCI_RAM_WREQ	;
input			PCI_RAM_WACK	;
output	[1:0]	PCI_RAM_WBL		;
output	[3:0]	PCI_RAM_WBE		;
input			PCI_RAM_WRDY	,
				PCI_RAM_WERR	;
output			PCI_RAM_WLAST	;
output	[31:0]	PCI_RAM_WDATA	;
output	[29:2]	PCI_RAM_WADDR	;

//*******************************************************************

output			PCI_PAR_ERR;

// USB interface
output	P1_LS_MODE	,
		P1_TXD		,
		P1_TXD_OEJ	,
		P1_TXD_SE0	,
		P2_LS_MODE	,
		P2_TXD		,
		P2_TXD_OEJ	,
		P2_TXD_SE0	;
input	I_P1_RXD	,
		I_P1_RXD_SE0,
		I_P2_RXD	,
		I_P2_RXD_SE0;

/*
output	p3_ls_mode	,
		p3_txd		,
		p3_txd_oej	,
		p3_txd_se0	,
		p4_ls_mode	,
		p4_txd		,
		p4_txd_oej	,
		p4_txd_se0	;

input	i_p3_rxd	,
		i_p3_rxd_se0,
		i_p4_rxd	,
		i_p4_rxd_se0;
*/
input	USB_CLK		;
output	USB_PON_	;
input	USB_OVERCUR_;
input	USB_CLK_EN	;	//Kandy
output	USB_INT_	;	// USB interrupt output

// IDE interface
input	PCI_MODE	;
input	PCI_MODE_IDE_EN	; 
output	PINS_STOP	;
input	PINS_IDLE	;

/*
input	ch2_detect	;
output  atadiow_out_;
output  atadior_out_;
output  atacs0_out_;
output  atacs1_out_;
output	[15:0]	atadd_out;
output  [3:0]  	atadd_oe_ ;
output	[2:0]   atada_out;
//output  atareset_out_;
output  atadmack_out_;

input   ataiordy_in;
input   ataintrq_in;
input	[15:0]	atadd_in;
input   atadmarq_in;
output	ide_int_;
*/
// system input
input		PCI_CLK,
			MEM_CLK,
//			ide100m_clk,
//			ide66m_clk,
			TESTMD,
			RST_;

wire			oe_ad_	;
wire	[31:0]	out_ad	;
wire			oe_cbe_	;
wire	[3:0]	out_cbe_;
wire			oe_par_,    out_par,
				oe_frame_,  out_frame_,
				oe_irdy_, 	out_irdy_,
				oe_devsel_, out_devsel_,
				oe_trdy_,   out_trdy_,
				oe_stop_,   out_stop_;
				
assign		OE_AD_		=	oe_ad_		;				
assign		OUT_AD	    =	out_ad	    ;
assign		OE_CBE_	    =	oe_cbe_	    ;
assign		OUT_CBE_    =	out_cbe_    ;
assign		OE_PAR_     =	oe_par_     ;
assign		OE_FRAME_	=	oe_frame_	;	  
assign		OE_IRDY_	=	oe_irdy_	;	 	
assign		OE_DEVSEL_	=	oe_devsel_	;	 
assign		OE_TRDY_	=	oe_trdy_	;	   
assign		OE_STOP_	=	oe_stop_	;	   
assign		OUT_PAR		=	out_par		;   
assign		OUT_FRAME_	=	out_frame_	;
assign		OUT_IRDY_	=	out_irdy_	;  
assign		OUT_DEVSEL_ =	out_devsel_ ;
assign		OUT_TRDY_	=	out_trdy_	; 
assign		OUT_STOP_	=	out_stop_	; 

// external PCI bus	input
wire	[1:0]	out_gnt_	; 
assign    		OUT_GNT_	=	out_gnt_	;

wire  [31:0]	in_ad		=	IN_AD		;
wire  [ 3:0]	in_cbe_		=	IN_CBE_		;
wire			in_par		=	IN_PAR		;
wire			in_frame_	=	IN_FRAME_	;
wire			in_irdy_	=	IN_IRDY_	;
wire			in_devsel_	=	IN_DEVSEL_	;
wire			in_trdy_	=	IN_TRDY_	;
wire			in_stop_	=	IN_STOP_	;
//XXX wire	[1:0]	in_req_		=	IN_REQ_		;
wire	[1:0]	in_req_;
assign in_req_[0] = IN_REQ_[0];

// North Bridge to pci host interface
wire			pbiu_req	=	PBIU_REQ	;
wire	[29:2]	pbiu_addr	=	PBIU_ADDR	;    
wire			pbiu_rw		=	PBIU_RW		;
wire	[1:0]	pbiu_bl		=	PBIU_BL		;
wire	[3:0]	pbiu_be		=	PBIU_BE		;
wire	[31:0]	pbiu_wr_data=	PBIU_WR_DATA; 

wire			pbiu_wr_rdy	;
wire			pbiu_rd_rdy	;
wire			pbiu_last	;
wire			pbiu_err	;
wire	[31:0]	pbiu_rd_data;
wire			pbiu_gnt	;

assign			PBIU_WR_RDY		=	pbiu_wr_rdy		;
assign			PBIU_RD_RDY	    =	pbiu_rd_rdy	    ;
assign			PBIU_LAST	    =	pbiu_last	    ;
assign			PBIU_ERR	    =	pbiu_err	    ;
assign			PBIU_RD_DATA    =	pbiu_rd_data    ;
assign			PBIU_GNT	    =	pbiu_gnt	    ;

wire			p_cfg_cs	=P_CFG_CS	;
wire			p_cfg_rw	=P_CFG_RW	;
wire	[3:0]	p_cfg_be	=P_CFG_BE	;
wire	[31:0]	p_cfg_dout	=P_CFG_DOUT	;
wire	[31:0]	p_cfg_addr	=P_CFG_ADDR	;

wire	[31:0]	p_cfg_din	; 
wire			p_cfg_gnt	; 
assign			P_CFG_DIN	=	p_cfg_din	;
assign			P_CFG_GNT   =	p_cfg_gnt   ;
	// PCI read cycle interface

wire	      	pci_ram_rack	=PCI_RAM_RACK	;               
wire	      	pci_ram_rrdy	=PCI_RAM_RRDY	;
wire       		pci_ram_rlast	=PCI_RAM_RLAST	;
wire       		pci_ram_rerr	=PCI_RAM_RERR	;
wire	[31:0]	pci_ram_rdata	=PCI_RAM_RDATA	;

wire         [29:2]	pci_ram_raddr	;
wire         [1:0]	pci_ram_rbl		; 
wire          		pci_ram_rreq	;
assign		   	PCI_RAM_RADDR		=	pci_ram_raddr		;
assign		    PCI_RAM_RBL		    =	pci_ram_rbl		    ;
assign		    PCI_RAM_RREQ	    =	pci_ram_rreq	    ;
	// PCI Posted Write FIFO interface
wire			pci_ram_wreq	;
wire			pci_ram_wlast	; 
wire	[31:0]	pci_ram_wdata	; 
wire	[29:2]	pci_ram_waddr	; 
wire	[1:0]	pci_ram_wbl		;
wire	[3:0]	pci_ram_wbe		;  
assign			PCI_RAM_WREQ	=	pci_ram_wreq	;		
assign			PCI_RAM_WLAST	=	pci_ram_wlast	;
assign			PCI_RAM_WDATA	=	pci_ram_wdata	;
assign			PCI_RAM_WADDR	=	pci_ram_waddr	;
assign			PCI_RAM_WBL		=	pci_ram_wbl		;
assign			PCI_RAM_WBE		=	pci_ram_wbe		;

wire			pci_ram_wrdy=PCI_RAM_WRDY	;
wire			pci_ram_werr=PCI_RAM_WERR	;
wire			pci_ram_wack=PCI_RAM_WACK	;
// USB interface
wire	p1_ls_mode	,
		p1_txd		,
		p1_txd_oej	,
		p1_txd_se0	,
		p2_ls_mode	,
		p2_txd		,
		p2_txd_oej	,
		p2_txd_se0	;
assign	P1_LS_MODE	=	p1_ls_mode	;		
assign	P1_TXD		=	p1_txd		;		
assign	P1_TXD_OEJ	=	p1_txd_oej	;		
assign	P1_TXD_SE0	=	p1_txd_se0	;		
assign	P2_LS_MODE	=	p2_ls_mode	;		
assign	P2_TXD		=	p2_txd		;		
assign	P2_TXD_OEJ	=	p2_txd_oej	;		
assign	P2_TXD_SE0	=	p2_txd_se0	;		
		
wire	i_p1_rxd	=I_P1_RXD		;
wire	i_p1_rxd_se0=I_P1_RXD_SE0	;
wire	i_p2_rxd	=I_P2_RXD		;
wire	i_p2_rxd_se0=I_P2_RXD_SE0	;

wire	usb_clk		=USB_CLK		;
wire	usb_overcur_=USB_OVERCUR_	;
wire	usb_clk_en	=USB_CLK_EN		;	//Kandy 

wire	usb_int_	;	// USB interrupt output
wire	usb_pon_	;
assign	USB_INT_	=usb_int_	;
assign	USB_PON_    =usb_pon_   ;
// system input
wire	pci_clk	=	PCI_CLK	;
wire	mem_clk	=	MEM_CLK	;
wire	testmd	=	TESTMD	;
wire	rst_	=	RST_	;


assign PCI_PAR_ERR = 1'b0;
parameter	udly = 1;

// Norman 2003-08-28
// assign out_gnt_[1] = 1'b1;	// runner 020816
// ======= module inter-connect wire
wire	[3:0]	curr_owner;

wire	host_oe_ad		,
		host_oe_cbe	    ,
		host_oe_par	    ,
		host_oe_frame	,
		host_oe_irdy	,
		host_oe_devsel  ,
		host_oe_trdy	,
		host_oe_stop	;

wire	[31:0]	host_out_ad		;
wire	[ 3:0]	host_out_cbe_	;
wire	host_out_par	,
		host_out_frame  ,
		host_out_irdy_  ,
		host_out_devse  ,
		host_out_trdy_  ,
		host_out_stop_  ,
		host_req_		;

// usb interface wire
wire			usb_req_		;
wire	[31:0]	usb_out_ad		;
wire	[3:0]	usb_out_cbe_	;
wire			usb_out_par		,
				usb_out_frame_	,
				usb_out_irdy_	,
				usb_out_devsel_	,
				usb_out_trdy_	,
				usb_out_stop_	,
				usb_out_perr_	,
				usb_out_serr_	;

wire	usb_oe_ad		,
		usb_oe_cbe		,
		usb_oe_par		,
		usb_oe_frame	,
		usb_oe_irdy		,
		usb_tdsoe		;
/*
wire	atadiow_out_	;
wire	atadior_out_	;
wire	atacs0_out_		;
wire	atacs1_out_		;
wire	[15:0]	atadd_out		;

wire	[3:0]	atadd_oe		;
wire	[3:0]	atadd_oe_		;

wire	[2:0]	atada_out		;
//assign	atareset_out_	= 1'b1;
wire	atadmack_out_	;

//wire	ide_int_ 	;
*/
// wire	ide_req_	;
/*
wire	[31:0]	ide_out_ad		;
wire	[3:0]	ide_out_cbe_ 	;
wire	ide_out_par 	;
wire	ide_out_frame_ 	;
wire	ide_out_irdy_ 	;
wire	ide_out_devsel_ ;
wire	ide_out_trdy_ 	;
wire	ide_out_perr_ 	;
wire	ide_out_serr_ 	;
wire	ide_out_stop_ 	;

wire	ide_oe_ad 		;
wire	ide_oe_cbe 		;
wire	ide_oe_frame 	;
wire	ide_oe_par 		;
wire	ide_oe_irdy 	;

//wire	ide_oe_devsel	;
//wire	ide_oe_trdy		;
//wire	ide_oe_stop		;
wire	ide_oe_dts		;
*/
// ====================== spg cell ================================
SEQ_DUMMYJ	PBIU_SEQ_DUMMYJ_0(
	.SDMY_OUT	(		),
	.DMY_IN		(1'b0	),
	.CLK		(mem_clk),
	.RSTJ		(rst_	)
	);
COMB_DUMMY	PBIU_COMB_DUMMY_0(
	.DMY_IN		(1'b0	),
	.DMY_OUT	(		)
	);
SEQ_DUMMYJ	PBIU_SEQ_DUMMYJ_1(
	.SDMY_OUT	(		),
	.DMY_IN		(1'b0	),
	.CLK		(mem_clk),
	.RSTJ		(rst_	)
	);
COMB_DUMMY	PBIU_COMB_DUMMY_1(
	.DMY_IN		(1'b0	),
	.DMY_OUT	(		)
	);

SEQ_DUMMYJ	PBIU_SEQ_DUMMYJ_2(
	.SDMY_OUT	(		),
	.DMY_IN		(1'b0	),
	.CLK		(pci_clk),
	.RSTJ		(rst_	)
	);
COMB_DUMMY	PBIU_COMB_DUMMY_2(
	.DMY_IN		(1'b0	),
	.DMY_OUT	(		)
	);


// ========= PCI Bus arbiter ===============
// assign	ide_req_	= 1'b1;	//no IDE now
wire		PCI_IDE_STOP	;	
wire		IDE_PCI_IDLE    ;

p_arbiter p_arbiter (
	.curr_owner	( curr_owner	),
	.p_req0_	( host_req_		),	// PCI request	-- PCI host master request
	.p_req1_	( usb_req_		),	// PCI request	-- USB Device request
	.p_req2_	( in_req_[0]	),	// PCI request	-- External PCI master request
	.p_req3_	( in_req_[1]	),	// PCI request 	-- The lowest request always connect to active

	.p_gnt0_	( host_gnt_		),	// PCI grant	-- PCI host master grant
	.p_gnt1_	( usb_gnt_		),	// PCI grant	-- USB grant
	.p_gnt2_	( out_gnt_[0]	),	// PCI grant	-- External PCI master request
	.p_gnt3_	( out_gnt_[1]	),	// PCI grant	-- The lowest request grant

	.pci_mode	(PCI_MODE		),	//PCI_MODE		
	.ata_pins_en(PCI_MODE_IDE_EN),	//ATA_PINS_EN		
	.pins_stop	(PCI_IDE_STOP	),
	.pins_idle	(IDE_PCI_IDLE	),

	.p_frame_	( in_frame_	),
	.p_irdy_	( in_irdy_	),
	.pci_clk	( pci_clk	),
	.rst_		( rst_		)

		);

p_host	p_host(
// ===== external pci bus output
	.oe_ad		( host_oe_ad		),
	.oe_cbe		( host_oe_cbe		),
	.oe_par		( host_oe_par		),
	.oe_frame	( host_oe_frame		),
	.oe_irdy	( host_oe_irdy		),
	.oe_devsel	( host_oe_devsel	),
	.oe_trdy	( host_oe_trdy		),
	.oe_stop	( host_oe_stop		),

	.out_ad		( host_out_ad		),
	.out_cbe_	( host_out_cbe_		),
	.out_par	( host_out_par	    ),
	.out_frame_	( host_out_frame_	),
	.out_irdy_	( host_out_irdy_	),
	.out_devsel_( host_out_devsel_ 	),
	.out_trdy_	( host_out_trdy_	),
	.out_stop_	( host_out_stop_	),
	.out_req_	( host_req_		),

// ==== Input from External pci Bus
	.in_ad		( in_ad			),
	.in_cbe_	( in_cbe_	    ),
	.in_frame_	( in_frame_		),
	.in_irdy_	( in_irdy_		),
	.in_devsel_	( in_devsel_	),
	.in_trdy_	( in_trdy_		),
	.in_stop_	( in_stop_		),
	.in_par		( in_par		),
	.in_gnt_	( host_gnt_	),

// ===== North Bridge to PCI host interface
////// ----- PCI IO and memory access path
	.pbiu_req		( pbiu_req		),
	.pbiu_gnt		( pbiu_gnt		),
	.pbiu_rw		( pbiu_rw		),
	.pbiu_bl		( pbiu_bl		),
	.pbiu_be		( pbiu_be		),
	.pbiu_wr_rdy	( pbiu_wr_rdy	),
	.pbiu_rd_rdy	( pbiu_rd_rdy	),
	.pbiu_last		( pbiu_last		),
	.pbiu_err		( pbiu_err		),
	.pbiu_addr		( pbiu_addr		),
	.pbiu_wr_data	( pbiu_wr_data	),
	.pbiu_rd_data	( pbiu_rd_data	),

///// ----- PCI configuration access path
	.p_cfg_cs		( p_cfg_cs		),
	.p_cfg_gnt		( p_cfg_gnt		),
	.p_cfg_rw		( p_cfg_rw		),
	.p_cfg_be		( p_cfg_be		),
	.p_cfg_din		( p_cfg_din		),
	.p_cfg_dout		( p_cfg_dout	),
	.p_cfg_addr		( p_cfg_addr	),

// ===== PCI to sdram controller read cycle interface
	.pci_ram_rreq	( pci_ram_rreq	),
	.pci_ram_rack	( pci_ram_rack	),
	.pci_ram_rbl	( pci_ram_rbl	),
	.pci_ram_rrdy	( pci_ram_rrdy	),
	.pci_ram_rlast	( pci_ram_rlast	),
	.pci_ram_rerr	( pci_ram_rerr	),
	.pci_ram_raddr	( pci_ram_raddr	),
	.pci_ram_rdata	( pci_ram_rdata	),

// ===== PCI Posted Write FIFO interface
	.pci_ram_wreq	( pci_ram_wreq	),
	.pci_ram_wack	( pci_ram_wack	),
	.pci_ram_wbl	( pci_ram_wbl	),
	.pci_ram_wbe	( pci_ram_wbe	),
	.pci_ram_wrdy	( pci_ram_wrdy	),
	.pci_ram_wlast	( pci_ram_wlast	),
	.pci_ram_werr	( pci_ram_werr	),
	.pci_ram_waddr	( pci_ram_waddr	),
	.pci_ram_wdata	( pci_ram_wdata	),


// =====  system signal
	.pci_clk	( pci_clk	),		// PCI Clock	--	33MHz
	.mem_clk	( mem_clk	),		// Chipset Clock
	.TESTMD		(TESTMD		),
	.rst_		( rst_		)
	);
//wire	PINS_STOP_TMP	;
ctrl_sync	ctrl_sync(
	.PCI_IDE_STOP		(PCI_IDE_STOP	),
	.IDE_PCI_IDLE	    (IDE_PCI_IDLE	),
	.SHARE_PINS_STOP	(PINS_STOP		),
	.SHARE_PINS_IDLE	(PINS_IDLE		),
	.PCI_CLK			(pci_clk		),
	.MEM_CLK			(mem_clk		),
	.RSTJ               (rst_			)
	);
////reg	[31:0]	STOP_CNT		;	
////always 	@(posedge mem_clk or negedge rst_)
////	if (!rst_)begin
////		STOP_CNT	<=	#1	32'b0				;
////		end
////	else if(PINS_IDLE)	begin
////		STOP_CNT	<=	#1	STOP_CNT +	1'b1	;
////		end
////	else	begin
////		STOP_CNT	<=	#1	32'b0				;
////		end
////		
////reg    PINS_STOP_TMP_DLY1	;
////
////
////reg    PINS_STOP_DLY1	;
////reg    PINS_STOP_DLY2	;
////always 	@(posedge mem_clk or negedge rst_)
////	if (!rst_)
////		PINS_STOP_TMP_DLY1	<=	#1	1'b0	;
////	else if(STOP_CNT	==	32'h100)
////		PINS_STOP_TMP_DLY1	<=	#1	1'b0	;
////	else
////		PINS_STOP_TMP_DLY1	<=	#1	1'b1	;	
////assign	PINS_STOP	=	PINS_STOP_TMP_DLY1;
////
////always 	@(posedge mem_clk or negedge rst_)
////	if (!rst_)begin
////		PINS_STOP_DLY1	<=	#1	1'b0	;
////		PINS_STOP_DLY2	<=	#1	1'b0	;
////		end
////	else	begin
////		PINS_STOP_DLY1	<=	#1	PINS_STOP	;
////		PINS_STOP_DLY2	<=	#1	PINS_STOP_DLY1	;
////	end
////wire stop_one_cycle =	PINS_STOP&!PINS_STOP_DLY1&PINS_STOP_DLY2;
////always	@(posedge	stop_one_cycle)
////	$display ("**Found :IDE_STOP falling one cycle at time %d ns",$time );		
//for debug  produce one cycle falling stop
		
// ================== USB core instance =====================
`ifdef	INC_USB

//wire	usb_pon_;			// power control
//wire	usb_overcur_;
wire	usb_gate_clk;
wire	i_p1_ovrcurj, i_p2_ovrcurj ;	// over current
//wire	i_p3_ovrcurj, i_p4_ovrcurj, ;	// over current

wire	p3_ls_mode 	= 1'b0,
		p3_txd 		= 1'b0,
		p3_txd_oej 	= 1'b1,
		p3_txd_se0 	= 1'b0,
		p4_ls_mode 	= 1'b0,
		p4_txd 		= 1'b0,
		p4_txd_oej 	= 1'b1,
		p4_txd_se0 	= 1'b0;

assign	usb_gate_clk	= usb_clk;

// instance USB cocr
hydra_core HYDRA_CORE (

// UBS interface
	// output
	.PWREN1J	( pwren1j		),
	.PWREN2J	( pwren2j		),
//	.PWREN3J	( pwren3j		),
//	.PWREN4J	( pwren4j		),

	.P1_LS_MODE	( p1_ls_mode	),
	.P1_TXD		( p1_txd		),
	.P1_TXD_OEJ	( p1_txd_oej	),
	.P1_TXD_SE0	( p1_txd_se0	),

	.P2_LS_MODE	( p2_ls_mode	),
	.P2_TXD		( p2_txd		),
	.P2_TXD_OEJ	( p2_txd_oej	),
	.P2_TXD_SE0	( p2_txd_se0	),

//	.P3_LS_MODE	( p3_ls_mode	),
//	.P3_TXD		( p3_txd		),
//	.P3_TXD_OEJ	( p3_txd_oej	),
//	.P3_TXD_SE0	( p3_txd_se0	),

//	.P4_LS_MODE	( p4_ls_mode	),
//	.P4_TXD		( p4_txd		),
//	.P4_TXD_OEJ	( p4_txd_oej	),
//	.P4_TXD_SE0	( p4_txd_se0	),
// input
	.I_P1_OVRCURJ	( i_p1_ovrcurj	),
	.I_P2_OVRCURJ	( i_p2_ovrcurj	),
//	.I_P3_OVRCURJ	( i_p3_ovrcurj	),
//	.I_P4_OVRCURJ	( i_p4_ovrcurj	),
	.I_P1_RXD		( i_p1_rxd		),
	.I_P1_RXD_SE0	( i_p1_rxd_se0	),
	.I_P2_RXD		( i_p2_rxd		),
	.I_P2_RXD_SE0	( i_p2_rxd_se0	),
//	.I_P3_RXD		( i_p3_rxd		),
//	.I_P3_RXD_SE0	( i_p3_rxd_se0	),
//	.I_P4_RXD		( i_p4_rxd		),
//	.I_P4_RXD_SE0	( i_p4_rxd_se0	),

// PCI bus output
	.O_DEVSELJ	( usb_out_devsel_	),
	.O_AD		( usb_out_ad		),
	.O_CBEJ		( usb_out_cbe_		),
	.O_PAR		( usb_out_par		),
	.O_FRAMEJ	( usb_out_frame_	),
	.O_IRDYJ	( usb_out_irdy_		),
	.O_TRDYJ	( usb_out_trdy_		),
	.O_PERRJ	( usb_out_perr_		),
	.O_SERRJ	( usb_out_serr_		),
	.O_STOPJ	( usb_out_stop_		),

	.ADOE		( usb_oe_ad		),
	.CBEJOE		( usb_oe_cbe	),
	.PAROE		( usb_oe_par	),
	.FRAMEJOE	( usb_oe_frame	),
	.IRDYJOE	( usb_oe_irdy	),
	.TDSOE		( usb_tdsoe			),	// trdy_, devsel_, stop_ output enable
	.O_USBREQJ	( usb_req_		),
	.O_PCIINTJ	( usb_int_		),

// PCI bus input
//T2-eric000425	.I_IDSEL	(PCI_AD[31]	),
	.I_IDSEL	( in_ad[18]		),	// Slow PCI 33 Device	-- Snow 2000-04-30
	.I_AD		( in_ad			),
	.I_CBEJ		( in_cbe_		),
	.I_PAR		( in_par		),
	.I_FRAMEJ	( in_frame_		),
	.I_IRDYJ	( in_irdy_		),
	.I_DEVSELJ	( in_devsel_	),
	.I_TRDYJ	( in_trdy_		),
	.I_STOPJ	( in_stop_		),

	.I_USBGNTJ	( usb_gnt_		),

// others
	.I_ACPI_CLK3( pci_clk		),
	.RSM_RSTJ	( rst_			),

	.I_CLK48	( usb_gate_clk	),
	.I_PCICLK	( pci_clk		),	// PCI clock
	.TESTMD		( TESTMD		),
	.I_PCIRSTJ	( rst_			)

	);

assign	usb_pon_ 	= pwren1j && pwren2j;
assign	i_p1_ovrcurj	= usb_overcur_;
assign	i_p2_ovrcurj	= usb_overcur_;
//assign	i_p3_ovrcurj	= usb_overcur_;
//assign	i_p4_ovrcurj	= usb_overcur_;

`else
// =============== No USB ============================
assign	usb_pon_ = 1'b1;		// power control

assign	p1_ls_mode 	= 1'b0,
		p1_txd 		= 1'b0,
		p1_txd_oej 	= 1'b1,
		p1_txd_se0 	= 1'b0,
		p2_ls_mode 	= 1'b0,
		p2_txd 		= 1'b0,
		p2_txd_oej 	= 1'b1,
		p2_txd_se0 	= 1'b0;
wire	p3_ls_mode 	= 1'b0,
		p3_txd 		= 1'b0,
		p3_txd_oej 	= 1'b1,
		p3_txd_se0 	= 1'b0,
		p4_ls_mode 	= 1'b0,
		p4_txd 		= 1'b0,
		p4_txd_oej 	= 1'b1,
		p4_txd_se0 	= 1'b0;

assign	usb_req_ = 1'b1;
assign	usb_int_ = 1'b1;

assign	usb_out_ad	= 32'h0000_0000;
assign	usb_out_cbe_ = 4'hf;
assign	usb_out_par = 1'b0;
assign	usb_out_frame_ = 1'b1;
assign	usb_out_irdy_ = 1'b1;
assign	usb_out_devsel_ = 1'b1;
assign	usb_out_trdy_ = 1'b1;
assign	usb_out_perr_ = 1'b1;
assign	usb_out_serr_ = 1'b1;
assign	usb_out_stop_ = 1'b1;

assign	usb_oe_ad = 1'b0;
assign	usb_oe_cbe = 1'b0;
assign	usb_oe_frame = 1'b0;
assign	usb_oe_par = 1'b0;
assign	usb_oe_irdy = 1'b0;
assign	usb_tdsoe = 1'b0;

`endif
// ============== Above No USB =====================

//============== Invoke IDE IP======================
/*
`ifdef INC_IDE
CORE5229 CORE5229 (
	// input port
     	.BCLK_66M 		(ide66m_clk  ),
        .BCLK_100M 		(ide100m_clk ),
        .CH2_DETECT 	( ch2_detect ),
        .IDSEL 			( in_ad[19]			),	// device 3
        .PAD_AD 		( in_ad				),
        .PAD_CBEJ 		( in_cbe_			),
        .PAD_CLK 		( pci_clk			),
        .PAD_DEVSELJ 	( in_devsel_		),
        .PAD_FRAMEJ 	( in_frame_			),
        .PAD_GNTJ 		( ide_gnt_			),
        .PAD_IRDYJ 		( in_irdy_			),
        .PAD_PAR 		( in_par			),
        .PAD_RSTJ 		( rst_				),
        .PAD_SHD 		( atadd_in		),
        .PAD_SHDRQ 		( atadmarq_in	),
        .PAD_SHINT 		( ataintrq_in	),
        .PAD_SNDIORDY 	( ataiordy_in	),
        .PAD_STOPJ 		( in_stop_	),
        .PAD_TRDYJ 		( in_trdy_	),

	// output port
        .IDE_INTB 		( ide_int_	)	,
        .PI_AD_O 		( ide_out_ad		)	,
        .PI_AD_OE 		( ide_oe_ad			)	,
        .PI_CBEJ_O 		( ide_out_cbe_		)	,
        .PI_CBEJ_OE 	( ide_oe_cbe		)	,
        .PI_DEVSELJ_O 	( ide_out_devsel_	)	,
        .PI_DTS_OE 		( ide_oe_dts		)	,	// pci devsel_, trdy_, stop_ output eable
        .PI_FRAMEJ_O 	( ide_out_frame_	)	,
        .PI_FRAMEJ_OE 	( ide_oe_frame		)	,
        .PI_IRDYJ_O 	( ide_out_irdy_		)	,
        .PI_IRDYJ_OE 	( ide_oe_irdy		)	,
        .PI_PAR_O 		( ide_out_par		)	,
        .PI_PAR_OE 		( ide_oe_par		)	,
        .PI_PERRJ_O 	( ide_out_perr_		)	,
        .PI_REQJ_O 		( ide_req_			)	,
        .PI_STOPJ_O 	( ide_out_stop_		)	,
        .PI_TRDYJ_O 	( ide_out_trdy_		)	,
        .SLV_SHA 		( atada_out		)	,
        .SLV_SHCS1J 	( atacs0_out_	)	,
        .SLV_SHCS3J 	( atacs1_out_	)	,
        .SLV_SHD_OE 	( atadd_oe		)	,	// 4bit width for 16 bit data oe
        .SLV_SHDACKJ 	( atadmack_out_	)	,
        .SLV_SHIORJ 	( atadior_out_	)	,
        .SLV_SHIOWJ 	( atadiow_out_	)	,
        .SLV_SND_HD_O   ( atadd_out		)
);

assign 	atadd_oe_	= ~atadd_oe;

`else
// IDE interface
assign	atadiow_out_	= 1'b1;
assign	atadior_out_	= 1'b1;
assign	atacs0_out_		= 1'b1;
assign	atacs1_out_		= 1'b1;
assign	atadd_out		= 16'h0;
assign	atadd_oe_		= 4'hf;
assign	atada_out		= 3'h0;
//assign	atareset_out_	= 1'b1;
assign	atadmack_out_	= 1'b1;

assign	ide_int_ 	= 1'b1;

assign	ide_out_ad		= 32'h0000_0000;
assign	ide_out_cbe_ 	= 4'hf;
assign	ide_out_par 	= 1'b0;
assign	ide_out_frame_ 	= 1'b1;
assign	ide_out_irdy_ 	= 1'b1;
assign	ide_out_devsel_ = 1'b1;
assign	ide_out_trdy_ 	= 1'b1;
assign	ide_out_perr_ 	= 1'b1;
assign	ide_out_serr_ 	= 1'b1;
assign	ide_out_stop_ 	= 1'b1;

assign	ide_oe_ad 		= 1'b0;
assign	ide_oe_cbe 		= 1'b0;
assign	ide_oe_frame 	= 1'b0;
assign	ide_oe_par 		= 1'b0;
assign	ide_oe_irdy 	= 1'b0;
assign	ide_oe_dts		= 1'b0;
//assign	ide_oe_devsel	= 1'b0;
//assign	ide_oe_trdy		= 1'b0;
//assign	ide_oe_stop		= 1'b0;

`endif
*/

/* ============ GI ============ */

wire [31:0] gi_out_ad;
wire [3:0] gi_out_cbe_;
wire gi_out_par;
wire gi_out_frame_;
wire gi_out_devsel_;
wire gi_out_irdy_;
wire gi_out_trdy_;
wire gi_out_stop_;

wire gi_oe_ad;
wire gi_oe_cbe;
wire gi_oe_par;
wire gi_oe_frame;
wire gi_oe_devsel;
wire gi_oe_irdy;
wire gi_oe_trdy;
wire gi_oe_stop;

// for now, put all the gi environment here;
// wait for ALI to change top-level, add pins and improve hierarchy;

`ifdef	ALI_GI

`include "gi.vh"
`include "pci.vh"

	wire gi_clk;			// gi clock;
	wire clk_lock;			// clk is stable;
	reg pin_rst;			// pin reset;
	wire pci_rst;			// pci reset;
	wire reset;				// chip-wide reset from gi;
	wire test_ena;			// enable test/debug logic;
	wire sec_intr;			// secure intr to cpu, NMI;
	wire sec_ack;			// secure exception fetch;

	assign pci_rst = ~rst_;
	assign clk_lock = 1'b1; //XXX
	assign sec_ack = 1'b1; //XXX

	// gi clock generator;
	// XXX hack until ALI supplies clock;

	reg gclk;				// gi clock derived from pci_clk;
	integer pci_period;		// measured pci period;
	integer pci_t0;

	initial
	begin
		pci_period = 0;		// force measurement of pci period;
		pci_t0 = -1;
		gclk = 1'b0;
	end

	// measure pci period;

	always @(posedge pci_clk)
	begin
		if(pci_period == 0) begin
			if(pci_t0 < 0)
				pci_t0 = $time;
			else
				pci_period = $time - pci_t0;
		end
	end

	// generate gi clock;

	always @(posedge pci_clk)
	begin
		if(pci_period == 0)
			gclk <= 1'b0;
		else begin
			gclk <= 1'b1;
			repeat(5) begin
				#(pci_period / 6);
				gclk <= ~gclk;
			end
		end
	end

	assign gi_clk = gclk;

	// power-on reset;

	reg go;					// when to start the tests;

	initial
	begin
		go = 1'b0;
		pin_rst = 1'b1;
		repeat(10) @(posedge gi_clk);
		pin_rst = 1'b0;
		@(negedge reset);
		@(posedge gi_clk);
		go = 1;
	end

	// monitor pin_rst;

	always @(pin_rst)
		$display("%t: %M: pin_rst=%b", $time, pin_rst);

	// monitor chip reset;

	always @(reset)
		$display("%t: %M: reset=%b", $time, reset);

	// alternative boot flag;
	// only useable if debug is enabled;
	// forces reading the rom backwards;
	// jump instruction there can go anywhere;

	reg alt_boot;

	initial
		alt_boot = 0;

	always @(alt_boot)
		$display("%t: %M: alt_boot=%b", $time, alt_boot);

	// monitor test mode;

	always @(test_ena)
		$display("%t: %M: test_ena=%b", $time, test_ena);

	// di interface;

	wire di_reset;			// interface reset;
	wire di_dir;			// xfer direction;
	wire [7:0] di_dd;		// data bus;
	wire [7:0] di_in;		// data from input cell;
	wire [7:0] di_out;		// data to output cell;
	wire di_oe;				// dd output enable;
	wire di_hstrb;			// host strobe/ready;
	wire di_dstrb;			// device strobe/ready;
	wire di_err;			// device error;
	wire di_brk;			// break;
	wire di_brk_in;			// break from input cell;
	wire di_brk_out;		// break to output cell;
	wire di_brk_oe;			// break output enable;
	wire di_cover;			// cover open;

	// ais interface;

	wire ais_clk;			// audio clock;
	wire ais_lr;			// left/right signal;
	wire ais_d;				// bit stream;

	// chip top-level io cells;
	// XXX should move to pad layer;

	assign di_in = di_dd;
	assign di_dd = di_oe? di_out : 8'bz;
	assign di_brk_in = di_brk;
	assign di_brk = di_brk_oe? di_brk_out : 1'bz;

	// XXX should move to board layer;

	pullup ( di_brk );

	// instantiate gi pci module;

	gi_pci gi_pci (
		.clk(gi_clk),
		.clk_lock(clk_lock),
		.pin_rst(pin_rst),
		.reset(reset),
		.alt_boot(alt_boot),
		.test_ena(test_ena),
		.gc_rst(), //XXX
		.sec_intr(sec_intr),
		.sec_ack(sec_ack),
		.pci_clk(pci_clk),
		.pci_rst(pci_rst),
		.ad_in(in_ad),
		.ad_out(gi_out_ad),
		.ad_oe(gi_oe_ad),
		.cbe_in(in_cbe_),
		.cbe_out(gi_out_cbe_),
		.cbe_oe(gi_oe_cbe),
		.par_in(in_par),
		.par_out(gi_out_par),
		.par_oe(gi_oe_par),
		.frame_in(in_frame_),
		.frame_out(gi_out_frame_),
		.frame_oe(gi_oe_frame),
		.devsel_in(in_devsel_),
		.devsel_out(gi_out_devsel_),
		.devsel_oe(gi_oe_devsel),
		.irdy_in(in_irdy_),
		.irdy_out(gi_out_irdy_),
		.irdy_oe(gi_oe_irdy),
		.trdy_in(in_trdy_),
		.trdy_out(gi_out_trdy_),
		.trdy_oe(gi_oe_trdy),
		.stop_in(in_stop_),
		.stop_out(gi_out_stop_),
		.stop_oe(gi_oe_stop),
		.pci_req(in_req_[1]), //XXX
		.pci_gnt(out_gnt_[1]), //XXX
		.pci_ognt(1'b0), //XXX
		.pci_intr(), //XXX
		.di_in(di_in),
		.di_out(di_out),
		.di_oe(di_oe),
		.di_brk_in(di_brk_in),
		.di_brk_out(di_brk_out),
		.di_brk_oe(di_brk_oe),
		.di_dir(di_dir),
		.di_hstrb(di_hstrb),
		.di_dstrb(di_dstrb),
		.di_err(di_err),
		.di_cover(di_cover),
		.di_reset(di_reset),
		.ais_clk(ais_clk),
		.ais_lr(ais_lr),
		.ais_d(ais_d)
	);

	// instantiate pci bus monitor;
	// monitors internal bus traffic;

	pci_mon pci_mon (
		.bus(1'b0),
		.clk(pci_clk),
		.rst(~pci_rst),
		.ad(in_ad),
		.cbe(in_cbe_),
		.par(in_par),
		.frame(in_frame_),
		.devsel(in_devsel_),
		.irdy(in_irdy_),
		.trdy(in_trdy_),
		.stop(in_stop_),
		.perr(1'b1), //XXX
		.serr(1'b1), //XXX
		.intr(4'b1111), //XXX
		.req(4'b1111), //XXX
		.gnt(4'b1111) //XXX
	);

	// XXX instantiate pci bus monitor;
	// XXX monitors external bus traffic;
	// XXX should move to above top level;

	// instantiate di host model;
	// XXX should move to above top level;

	di_host di_host (
		.reset(di_reset),
		.dir(di_dir),
		.dd(di_dd),
		.hstrb(di_hstrb),
		.dstrb(di_dstrb),
		.err(di_err),
		.brk(di_brk),
		.cover(di_cover)
	);

	// instantiate di monitor;
	// XXX should move to above top level;

	di_mon di_mon (
		.reset(di_reset),
		.dir(di_dir),
		.dd(di_dd),
		.hstrb(di_hstrb),
		.dstrb(di_dstrb),
		.err(di_err),
		.brk(di_brk),
		.cover(di_cover)
	);

	// instantiate ais host model;
	// XXX should move to above top level;

	ais_host ais_host (
		.reset(di_reset),
		.ais_clk(ais_clk),
		.ais_lr(ais_lr),
		.ais_d(ais_d)
	);

`else	// ALI_GI

	assign gi_out_ad = 32'd0;
	assign gi_out_cbe_ = 4'b1111;
	assign gi_out_par = 1'b0;
	assign gi_out_frame_ = 1'b1;
	assign gi_out_devsel_ = 1'b1;
	assign gi_out_irdy_ = 1'b1;
	assign gi_out_trdy_ = 1'b1;
	assign gi_out_stop_ = 1'b1;

	assign gi_oe_ad = 1'b0;
	assign gi_oe_cbe = 1'b0;
	assign gi_oe_par = 1'b0;
	assign gi_oe_frame = 1'b0;
	assign gi_oe_devsel = 1'b0;
	assign gi_oe_irdy = 1'b0;
	assign gi_oe_trdy = 1'b0;
	assign gi_oe_stop = 1'b0;

`endif	// ALI_GI

assign	out_ad 	=		({32{host_oe_ad}} 	& host_out_ad  	) |
						({32{gi_oe_ad}}     & gi_out_ad     ) |
					 	({32{usb_oe_ad}} 	& usb_out_ad 	) ;//|
					 	//({32{ide_oe_ad}} 	& ide_out_ad 	) ;

assign 	out_cbe_= 		({4{host_oe_cbe}} 	& host_out_cbe_	) 	|
						({4{gi_oe_cbe}}     & gi_out_cbe_   )   |
						({4{usb_oe_cbe}} 	& usb_out_cbe_	)	;//|
						//({4{ide_oe_cbe}} 	& ide_out_cbe_	)	;

assign	out_par	=		( host_oe_par 	& host_out_par 	)|
						( gi_oe_par 	& gi_out_par 	)|
						( usb_oe_par 	& usb_out_par 	);//|
						//( ide_oe_par 	& ide_out_par 	);

assign  out_frame_ 	=  	host_out_frame_	& usb_out_frame_ & gi_out_frame_ ;//& ide_out_frame_	;
assign  out_irdy_	= 	host_out_irdy_  & usb_out_irdy_	 & gi_out_irdy_  ;//& ide_out_irdy_	;
assign	out_devsel_	=	host_out_devsel_& usb_out_devsel_ & gi_out_devsel_ ;//& ide_out_devsel_	;
assign	out_trdy_	= 	host_out_trdy_	& usb_out_trdy_	& gi_out_trdy_  ;//& ide_out_trdy_	;
assign	out_stop_	=	host_out_stop_	& usb_out_stop_	& gi_out_stop_  ;//& ide_out_stop_	;

assign	oe_ad_ 		= ~( host_oe_ad		| usb_oe_ad		| gi_oe_ad     );//| ide_oe_ad		);
assign	oe_cbe_		= ~( host_oe_cbe	| usb_oe_cbe	| gi_oe_cbe    );//| ide_oe_cbe	    );
assign	oe_par_		= ~( host_oe_par	| usb_oe_par	| gi_oe_par    );//| ide_oe_par	    );
assign	oe_frame_	= ~( host_oe_frame	| usb_oe_frame	| gi_oe_frame  );//| ide_oe_frame		);
assign	oe_irdy_	= ~( host_oe_irdy	| usb_oe_irdy	| gi_oe_irdy   );//| ide_oe_irdy		);
assign	oe_devsel_	= ~( host_oe_devsel	| usb_tdsoe		| gi_oe_devsel );//| ide_oe_dts		);
assign	oe_trdy_	= ~( host_oe_trdy	| usb_tdsoe     | gi_oe_trdy   );//| ide_oe_dts		);
assign	oe_stop_	= ~( host_oe_stop	| usb_tdsoe     | gi_oe_stop   );//| ide_oe_dts		);

endmodule // p_biu
