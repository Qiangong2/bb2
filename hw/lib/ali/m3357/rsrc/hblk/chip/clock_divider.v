/******************************************************************************
	  (c) copyrights 2000-. All rights reserved

	   T-Square Design, Inc.

	  Company confidential and Properietary	information.
	  This information may not be disclosed	to unauthorized individual.
*******************************************************************************/

/******************************************************************************
 *
 *    DESCRIPTION:	clock divider in clock generator
 *
 *    AUTHOR:		norman
 *
 *    HISTORY: 		2002.03.10 initial version
 					2002-07-15	change div_src_23 from 33% duty cycle to 66% duty
 								cycle by invert.
 					2002-07-16	remove input mem_fs, output div_mem_clk
 								The 1 : 1.5 divider is still kept
 					2002-09-05	Add one option 1:1.5 option to GPU_CLK
 								Add DSP source clock selection between 2/3 or 1/2
 								cpu clock.
 								DSP clock programmable step changed from 1/32 to
 								1/16 DSP source clock.
 					2002-10-08	Remove PC trace clock
 					2003-11-10	Update the video clock when configuration set to '11'


 *****************************************************************************/

module clock_divider(

	// input
	pci_mips_fs,	// select the divisor of PCI/MIPS bus clock
	dsp_fs,			// frequency select of dsp clock
	dsp_div_src_sel,// dsp source clock selection
	ve_fs,			// frequency select of video engine clock
	div_clk_src,	// source clock
	mem_clk_src,	// memory clock from the mem_clk_pll

	//output
	div_cpu_clk,	// output CPU clock
	div_dsp_clk,	// output dsp clock
	div_ve_clk,		// output video engine clock
	div_pci_m_clk,	// output PCI/MIPS bus clock
	div_servo_clk,	// output SERVO clock
	rst_
	);

input	[1:0]	pci_mips_fs;
input	[3:0]	dsp_fs;
input			dsp_div_src_sel;// dsp source clock selection
input	[1:0]	ve_fs;

input			div_clk_src;
input			mem_clk_src;	// memory clock from the mem_clk_pll


output		div_cpu_clk,
			div_pci_m_clk,
			div_dsp_clk,
			div_ve_clk;
output		div_servo_clk;		// output SERVO clock

input		rst_;

parameter	udly = 0.3;
reg			div_servo_clk;

	/*=========================================================================
		Divide input clock to get 1:1, 1:1.5, 1:2, 1:3, 1:4, 1:8
		clocks
		clk_src_14 is generated by clk_src_12,
		clk_src_18 is generated by clk_src_14
		other clock is generated by div_clk_src_
	=========================================================================*/
	wire	div_clk_src_	= !div_clk_src;		// inverted input clock

	wire	clk_src_11	= div_clk_src;			// 1:1 sub clock
	wire	clk_src_23;							// 2:3 sub clock
	reg		clk_src23_flag;

	reg		clk_src_12;							// 1:2 sub clock
	reg		clk_src_13;							// 1:3 sub clock
	reg		clk_src_14;							// 1:4 sub clock
	reg		clk_src_15;							// 1:5 sub clock
	reg		clk_src_16;							// 1:6 sub clock
	reg		clk_src_18;							// 1:8 sub clock

	reg	[1:0]	clk_src3_cnt;					// counter 3 of clk_src
	reg	[2:0]	clk_src5_cnt;					// counter 5 of clk_src
	reg	[2:0]	clk_src6_cnt;					// counter 6 of clk_src

	// clk_src3_cnt
	always @ (posedge div_clk_src or negedge rst_)
	   if (!rst_)
	      clk_src3_cnt <= #udly 0;
	   else if (clk_src3_cnt == 2'b10 | clk_src3_cnt == 2'b11)
	      clk_src3_cnt <= #udly 0;
	   else
	      clk_src3_cnt <= #udly clk_src3_cnt + 2'b1;

	//clk_src5_cnt
	always @ (posedge div_clk_src or negedge rst_)
	   if (!rst_)
	      clk_src5_cnt <= #udly 0;
	   else if (clk_src5_cnt == 3'b100 | clk_src5_cnt == 3'b101
	   		 | clk_src5_cnt == 3'b110 | clk_src5_cnt == 3'b111)
	      clk_src5_cnt <= #udly 0;
	   else
	      clk_src5_cnt <= #udly clk_src5_cnt + 3'b1;

	//clk_src6_cnt
	always @ (posedge div_clk_src or negedge rst_)
	   if (!rst_)
	      clk_src6_cnt <= #udly 0;
	   else if (clk_src6_cnt == 3'b101 | clk_src6_cnt == 3'b110 | clk_src6_cnt == 3'b111)
	      clk_src6_cnt <= #udly 0;
	   else
	      clk_src6_cnt <= #udly clk_src6_cnt + 3'b1;

	always @ (posedge div_clk_src_ or negedge rst_)
	   if (!rst_)
	      clk_src23_flag <= #udly 0;
	   else if (clk_src3_cnt == 2'b10)
	      clk_src23_flag <= #udly 1;
	   else if (clk_src3_cnt == 2'b00)
	      clk_src23_flag <= #udly 0;

	assign	clk_src_23	= (clk_src3_cnt == 2'b00) & clk_src23_flag & div_clk_src |
						  (clk_src3_cnt == 2'b01) & !clk_src23_flag & div_clk_src_;

	// clk_src_12
	always @ (posedge div_clk_src or negedge rst_)
	   if (!rst_)
	      clk_src_12 <= #udly 0;
	   else
	      clk_src_12 <= #udly !clk_src_12;

	// clk_src_13
	always @ (posedge div_clk_src or negedge rst_)
	   if (!rst_)
	      clk_src_13 <= #udly 0;
	   else if (clk_src3_cnt == 2'b10)			// clk_src_13 will be high when cnt3 is 0
	      clk_src_13 <= #udly 1;
	   else
	      clk_src_13 <= #udly 0;

	// clk_src_14
	always @ (posedge clk_src_12 or negedge rst_)
	   if (!rst_)
	      clk_src_14 <= #udly 0;
	   else
	      clk_src_14 <= #udly !clk_src_14;

	// clk_src_15
	always @ (posedge div_clk_src or negedge rst_)
	   if (!rst_)
	      clk_src_15 <= #udly 0;
	   else if (clk_src5_cnt == 3'b000 |
	   			clk_src5_cnt == 3'b001)			// clk_src_15 will be high after cnt5 is 0, 1
	      clk_src_15 <= #udly 1;
	   else
	      clk_src_15 <= #udly 0;

	// clk_src_16
	always @ (posedge div_clk_src or negedge rst_)
	   if (!rst_)
	      clk_src_16 <= #udly 0;
	   else if (clk_src6_cnt == 3'b000 |
	   			clk_src6_cnt == 3'b001 |
	   			clk_src6_cnt == 3'b010)			// clk_src_16 will be high after cnt6 is 0, 1, 2
	      clk_src_16 <= #udly 1;
	   else
	      clk_src_16 <= #udly 0;

	// clk_src_18
	always @ (posedge clk_src_14 or negedge rst_)
	   if (!rst_)
	      clk_src_18 <= #udly 0;
	   else
	      clk_src_18 <= #udly !clk_src_18;

	/*=========================================================================
		cpu_clk
		CPU clock is the output from CPU_PLL output and no divider is added
		for cpu clock in this module.
	=========================================================================*/
	assign	div_cpu_clk = clk_src_11;

	/*=========================================================================
		video engine clock
		video_fs	ve_clk : cpu_clk
		2'b00		1:2
		2'b01		2:3
		2'b10		1:3
		2'b11		1:4
	=========================================================================*/
	assign	div_ve_clk = (ve_fs == 2'b00) & clk_src_12 |
						 (ve_fs == 2'b01) & clk_src_23 |
						 (ve_fs == 2'b10) & clk_src_13 |
						 (ve_fs == 2'b11) & mem_clk_src;

	/*=========================================================================
		pci_mips_clk
		pci_mips_fs		pci_mips_clk : cpu_clk
		00				1 : 4
		01				1 : 5
		10				1 : 6
		11				1 : 8
	=========================================================================*/
	assign	div_pci_m_clk	= (pci_mips_fs == 2'b00) & clk_src_14 |
							  (pci_mips_fs == 2'b01) & clk_src_15 |
							  (pci_mips_fs == 2'b10) & clk_src_16 |
							  (pci_mips_fs == 2'b11) & clk_src_18;

	/*=========================================================================
		servo clock
	=========================================================================*/
	always @ (posedge mem_clk_src or negedge rst_)
	   if (!rst_)
	      div_servo_clk	<= 0;
	   else
	      div_servo_clk <= !div_servo_clk;

	/*=========================================================================
		dsp_clk
		DSP clock is requested to have a step about 5MHz. The FS input for DSP
		clock are 5 bits and the DSP will get the clock from 1/64 to 32/64 of
		CPU clock.

		The DSP divider source clock could be selected between 2/3 CPU clock or
		1/2 CPU clock. The programmable step is changed from 1/32 source clock
		to 1/16 source clock. While the DSP source clock is 2/3 CPU clock, DSP
		clock range is from 2/3 CPU clock to

		dsp_fs		dsp_clk : cpu_clk
		5'b00000		1 : 64
		5'b00001		2 : 64
		5'b00010		3 : 64
		...				...
		5'b11110		31 : 64
		5'b11111		32 : 64

		The clock output from 32/64 to 22/64 is one group with the same minimum
		clock rising edge delay: 2 cpu clock
		The clock output from 21/64 to 17/64 is one group with the same minimum
		clock rising edge delay: 3 cpu clock
		The clock output from 16/64 to 1/64 is one group with the same minimum
		clock rising edge delay: 4 cpu clock
	=========================================================================*/
	//	dsp clock divider source select
	wire	dsp_clk_src_sel	= dsp_div_src_sel;	// 1-> 2/3 CPU clock, 0-> 1/2 CPU clock
	wire	dsp_clk_src		= dsp_clk_src_sel ? mem_clk_src		: clk_src_12;
	wire	dsp_clk_src_12	= dsp_clk_src_sel ? div_servo_clk	: clk_src_14;	//	the div_servo_clk is 1/2 mem_clk

	wire	dsp_clk_src_	= !dsp_clk_src;
	wire	dsp_clk_src_12_	= !dsp_clk_src_12;

	//	dsp clock mask part
	wire	dsp_grp1_en		= dsp_fs[3];
	wire	dsp_grp2_en		= !dsp_fs[3];

	reg		dsp_grp1_mask, dsp_grp2_mask;

	reg	[3:0]	dsp_grp1_cnt;		// count from 0 to 15
	reg	[3:0]	dsp_grp1_cnt2;		// count from 15 down to dsp_fs

	reg	[2:0]	dsp_grp2_cnt;		// count from 0 to 7
	reg	[2:0]	dsp_grp2_cnt2;		// count from 7 to dsp_fs

	/*=========================================================================
		DSP clock group one 100 ~ 50 MHz
		The source is 1/2 cpu clock and useless clock pulse will be masked when
		the counter exceed the DSP fs setting.
	=========================================================================*/
	always @ (negedge rst_ or posedge dsp_clk_src)
	   if (!rst_)
	      dsp_grp1_cnt <= #udly 0;
	   else if (dsp_grp1_cnt == 4'b1111)
	      dsp_grp1_cnt <= #udly 0;
	   else
	      dsp_grp1_cnt <= #udly dsp_grp1_cnt + 1;

	// How many clock cycle has been masked.
	// count down from 4'b1111 to dsp_fs
	always @ (negedge rst_ or posedge dsp_clk_src)
	   if (!rst_)
	      dsp_grp1_cnt2 <= #udly 4'b1111;
	   else if (dsp_grp1_cnt == 4'b1111)		// set cnt2 to 4'hf when cnt1 grow to 1111
	      dsp_grp1_cnt2 <= #udly 4'b1111;
	   else if (dsp_grp1_mask & (dsp_grp1_cnt2 != dsp_fs[3:0]))
	      dsp_grp1_cnt2 <= #udly dsp_grp1_cnt2 - 1;

	// using clk_src_12 to generate the mask signal
	always @ (negedge rst_ or posedge dsp_clk_src_)
	   if (!rst_)
	      dsp_grp1_mask <= #udly 0;
	   else if ((dsp_grp1_cnt[0] == 1'b1) & (dsp_grp1_cnt2 != dsp_fs[3:0]))
	      dsp_grp1_mask <= #udly 1;
	   else
	      dsp_grp1_mask <= #udly 0;

	wire	dsp_grp1_clk	= dsp_clk_src & !dsp_grp1_mask;

	/*=========================================================================
		DSP clock group two 50 ~ 25 MHz
		The source is 1/4 cpu clock and useless clock pulse will be masked when
		the counter exceed the DSP fs setting.
	=========================================================================*/

	always @ (negedge rst_ or posedge dsp_clk_src_12)
	   if (!rst_)
	      dsp_grp2_cnt <= #udly 0;
	   else if (dsp_grp2_cnt == 3'b111)
	      dsp_grp2_cnt <= #udly 0;
	   else
	      dsp_grp2_cnt <= #udly dsp_grp2_cnt + 1;

	// How many clock cycle has been masked.
	// count down from 4'b1111 to dsp_fs
	always @ (negedge rst_ or posedge dsp_clk_src_12)
	   if (!rst_)
	      dsp_grp2_cnt2 <= #udly 3'b111;
	   else if (dsp_grp2_cnt == 3'b111)		// set cnt2 to 5'h1f when cnt1 grow to 11111
	      dsp_grp2_cnt2 <= #udly 3'b111;
	   else if (dsp_grp2_mask & (dsp_grp2_cnt2 != dsp_fs[2:0]))
	      dsp_grp2_cnt2 <= #udly dsp_grp2_cnt2 - 1;

	// using clk_src_14_ to generate mask signal
	always @ (negedge rst_ or posedge dsp_clk_src_12_)
	   if (!rst_)
	      dsp_grp2_mask <= #udly 0;
	   else if ((dsp_grp2_cnt[0] == 1'b1) & (dsp_grp2_cnt2 != dsp_fs[2:0]))
	      dsp_grp2_mask <= #udly 1;
	   else
	      dsp_grp2_mask <= #udly 0;

	wire	dsp_grp2_clk	= dsp_clk_src_12 & !dsp_grp2_mask;

	// DSP clock could be selected from group 1 or 2 clock output.
	assign	div_dsp_clk	= dsp_grp1_clk & dsp_grp1_en |
						  dsp_grp2_clk & dsp_grp2_en;
endmodule
