/******************************************************************************
	  (c) copyrights 2000-. All rights reserved

	   T-Square Design, Inc.

	  Company confidential and Properietary	information.
	  This information may not be disclosed	to unauthorized individual.
*******************************************************************************/

/*******************************************************************************
 *
 *
 *       AUTHOR: Kandy Kong
 *
 *       HISTORY:   29/03/01         initial version
 *
 *********************************************************************************/
module	DLYCELL02	(
	A,		// input source
	Y		// fixed delay output
	);

parameter	fix_dly_05ns = 0.5;	// 0.5 ns delay of whole chain, support delay <1GHz clock

input		A;
output		Y;

//total delay 2ns
wire	#fix_dly_05ns DLY_FIX_TMP0 = A;
wire	#fix_dly_05ns DLY_FIX_TMP1 = DLY_FIX_TMP0;
wire	#fix_dly_05ns DLY_FIX_TMP2 = DLY_FIX_TMP1;
assign	#fix_dly_05ns Y			   = DLY_FIX_TMP2;

endmodule

