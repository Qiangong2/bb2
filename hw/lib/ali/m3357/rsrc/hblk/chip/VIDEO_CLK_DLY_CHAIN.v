/******************************************************************************
	  (c) copyrights 2000-. All rights reserved

	   T-Square Design, Inc.

	  Company confidential and Properietary	information.
	  This information may not be disclosed	to unauthorized individual.
*******************************************************************************/

/*******************************************************************************
 *
 *
 *       AUTHOR:		Norman
 *		 Description:	Generate clock and inverted clock and the delay 1, 2, 3 ns
 						clocks of the origin clocks and inverted clocks.
 *
 *       HISTORY:		2003-11-06	initial version
 						2003-12-06	The output type changed to
 										Origianl, 2ns Delay, Inverted, Inverted with 2ns delay

 *********************************************************************************/
module	VIDEO_CLK_DLY_CHAIN(
		CLK_IN,		// input clock source
		CLK_OUT,	// Delay chain output clock
		DLY_SEL		// delay select
		);

input			CLK_IN;		// input clock source
output			CLK_OUT;	// Delay chain output clock
input	[1:0]	DLY_SEL;	// delay select

INVX4	U_INV_1		(.A(CLK_IN),	.Y(CLK_IN_INV));

//	Every DLY4X1 cell could delay 0.6ns in typical process
DLY4X1	CLK_DELAY_1		(.A(CLK_IN		),		.Y(CLK_IN_DLY_1 ));
DLY4X1	CLK_DELAY_2		(.A(CLK_IN_DLY_1),		.Y(CLK_IN_DLY_2 ));
DLY4X1	CLK_DELAY_3		(.A(CLK_IN_DLY_2),		.Y(CLK_IN_DLY_3 ));
DLY4X1	CLK_DELAY_4		(.A(CLK_IN_DLY_3),		.Y(CLK_IN_DLY_4 ));

DLY4X1	CLK_INV_DELAY_1	(.A(CLK_IN_INV		),	.Y(CLK_IN_INV_DLY_1 ));
DLY4X1	CLK_INV_DELAY_2	(.A(CLK_IN_INV_DLY_1),	.Y(CLK_IN_INV_DLY_2 ));
DLY4X1	CLK_INV_DELAY_3	(.A(CLK_IN_INV_DLY_2),	.Y(CLK_IN_INV_DLY_3 ));
DLY4X1	CLK_INV_DELAY_4	(.A(CLK_IN_INV_DLY_3),	.Y(CLK_IN_INV_DLY_4 ));

MX4X4	U_MUX_1	(.S0(DLY_SEL[0]	),	.S1	(DLY_SEL[1]	 		),
				 .A	(CLK_IN		),	.B	(CLK_IN_DLY_4		),
				 .C	(CLK_IN_INV	),	.D	(CLK_IN_INV_DLY_4	),
				 .Y	(CLK_OUT)
				 );

endmodule
