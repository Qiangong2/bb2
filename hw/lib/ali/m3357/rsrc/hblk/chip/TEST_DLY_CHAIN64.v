/******************************************************************************
	  (c) copyrights 2000-. All rights reserved

	   T-Square Design, Inc.

	  Company confidential and Properietary	information.
	  This information may not be disclosed	to unauthorized individual.
*******************************************************************************/

/*******************************************************************************
 *
 *
 *       AUTHOR:	Norman
 *		 Description:	Add the delay select value, the FLAG will change from 0 -> 1
 *
 *       HISTORY:	2003-09-25	initial version
 					2003-09-29	change cell MXI4X4 to MX4X4, using MXI4X4 is wrong

 *********************************************************************************/
module	TEST_DLY_CHAIN64(
		CK,		// input clock source
		FLAG,	// Delay chain output flag
		DLY_SEL	// delay select
		);

input			CK;			// input clock source
output			FLAG;		// delay chain output flag
input	[5:0]	DLY_SEL;	// delay select

DLY4X1	TEST_DELAY_1	(.A(CK		),	.Y(CK_tmp	));
DLY4X1	TEST_DELAY_1_1	(.A(CK_tmp	),	.Y(CK1		));

DLY4X1	TEST_DELAY_2	(.A(CK1		),	.Y(CK1_tmp	));
DLY4X1	TEST_DELAY_2_1	(.A(CK1_tmp	),	.Y(CK2		));

DLY4X1	TEST_DELAY_3	(.A(CK2		),	.Y(CK2_tmp	));
DLY4X1	TEST_DELAY_3_1	(.A(CK2_tmp	),	.Y(CK3		));

DLY4X1	TEST_DELAY_4	(.A(CK3		),	.Y(CK3_tmp	));
DLY4X1	TEST_DELAY_4_1	(.A(CK3_tmp	),	.Y(CK4		));

DLY4X1	TEST_DELAY_5	(.A(CK4		),	.Y(CK4_tmp	));
DLY4X1	TEST_DELAY_5_1	(.A(CK4_tmp	),	.Y(CK5		));

DLY4X1	TEST_DELAY_6	(.A(CK5		),	.Y(CK5_tmp	));
DLY4X1	TEST_DELAY_6_1	(.A(CK5_tmp	),	.Y(CK6		));

DLY4X1	TEST_DELAY_7	(.A(CK6		),	.Y(CK6_tmp	));
DLY4X1	TEST_DELAY_7_1	(.A(CK6_tmp	),	.Y(CK7		));

DLY4X1	TEST_DELAY_8	(.A(CK7		),	.Y(CK7_tmp	));
DLY4X1	TEST_DELAY_8_1	(.A(CK7_tmp	),	.Y(CK8		));

DLY4X1	TEST_DELAY_9	(.A(CK8		),	.Y(CK8_tmp	));
DLY4X1	TEST_DELAY_9_1	(.A(CK8_tmp	),	.Y(CK9		));

DLY4X1	TEST_DELAY_10	(.A(CK9		),	.Y(CK9_tmp	));
DLY4X1	TEST_DELAY_10_1	(.A(CK9_tmp	),	.Y(CK10		));

DLY4X1	TEST_DELAY_11	(.A(CK10	),	.Y(CK10_tmp	));
DLY4X1	TEST_DELAY_11_1	(.A(CK10_tmp),	.Y(CK11		));

DLY4X1	TEST_DELAY_12	(.A(CK11	),	.Y(CK11_tmp	));
DLY4X1	TEST_DELAY_12_1	(.A(CK11_tmp),	.Y(CK12		));

DLY4X1	TEST_DELAY_13	(.A(CK12	),	.Y(CK12_tmp	));
DLY4X1	TEST_DELAY_13_1	(.A(CK12_tmp),	.Y(CK13		));

DLY4X1	TEST_DELAY_14	(.A(CK13	),	.Y(CK13_tmp));
DLY4X1	TEST_DELAY_14_1	(.A(CK13_tmp),	.Y(CK14		));

DLY4X1	TEST_DELAY_15	(.A(CK14	),	.Y(CK14_tmp	));
DLY4X1	TEST_DELAY_15_1	(.A(CK14_tmp),	.Y(CK15		));

DLY4X1	TEST_DELAY_16	(.A(CK15	),	.Y(CK15_tmp	));
DLY4X1	TEST_DELAY_16_1	(.A(CK15_tmp),	.Y(CK16		));

DLY4X1	TEST_DELAY_17	(.A(CK16	),	.Y(CK16_tmp	));
DLY4X1	TEST_DELAY_17_1	(.A(CK16_tmp),	.Y(CK17		));

DLY4X1	TEST_DELAY_18	(.A(CK17	),	.Y(CK17_tmp	));
DLY4X1	TEST_DELAY_18_1	(.A(CK17_tmp),	.Y(CK18		));

DLY4X1	TEST_DELAY_19	(.A(CK18	),	.Y(CK18_tmp	));
DLY4X1	TEST_DELAY_19_1	(.A(CK18_tmp),	.Y(CK19		));

DLY4X1	TEST_DELAY_20	(.A(CK19	),	.Y(CK19_tmp	));
DLY4X1	TEST_DELAY_20_1	(.A(CK19_tmp),	.Y(CK20		));

DLY4X1	TEST_DELAY_21	(.A(CK20	),	.Y(CK20_tmp	));
DLY4X1	TEST_DELAY_21_1	(.A(CK20_tmp),	.Y(CK21		));

DLY4X1	TEST_DELAY_22	(.A(CK21	),	.Y(CK21_tmp	));
DLY4X1	TEST_DELAY_22_1	(.A(CK21_tmp),	.Y(CK22		));

DLY4X1	TEST_DELAY_23	(.A(CK22	),	.Y(CK22_tmp	));
DLY4X1	TEST_DELAY_23_1	(.A(CK22_tmp),	.Y(CK23		));

DLY4X1	TEST_DELAY_24	(.A(CK23	),	.Y(CK23_tmp	));
DLY4X1	TEST_DELAY_24_1	(.A(CK23_tmp),	.Y(CK24		));

DLY4X1	TEST_DELAY_25	(.A(CK24	),	.Y(CK24_tmp	));
DLY4X1	TEST_DELAY_25_1	(.A(CK24_tmp),	.Y(CK25		));

DLY4X1	TEST_DELAY_26	(.A(CK25	),	.Y(CK25_tmp	));
DLY4X1	TEST_DELAY_26_1	(.A(CK25_tmp),	.Y(CK26		));

DLY4X1	TEST_DELAY_27	(.A(CK26	),	.Y(CK26_tmp	));
DLY4X1	TEST_DELAY_27_1	(.A(CK26_tmp),	.Y(CK27		));

DLY4X1	TEST_DELAY_28	(.A(CK27	),	.Y(CK27_tmp));
DLY4X1	TEST_DELAY_28_1	(.A(CK27_tmp),	.Y(CK28		));

DLY4X1	TEST_DELAY_29	(.A(CK28	),	.Y(CK28_tmp	));
DLY4X1	TEST_DELAY_29_1	(.A(CK28_tmp),	.Y(CK29		));

DLY4X1	TEST_DELAY_30	(.A(CK29	),	.Y(CK29_tmp	));
DLY4X1	TEST_DELAY_30_1	(.A(CK29_tmp),	.Y(CK30		));

DLY4X1	TEST_DELAY_31	(.A(CK30	),	.Y(CK30_tmp	));
DLY4X1	TEST_DELAY_31_1	(.A(CK30_tmp),	.Y(CK31		));

DLY4X1	TEST_DELAY_32	(.A(CK31	),	.Y(CK31_tmp	));
DLY4X1	TEST_DELAY_32_1	(.A(CK31_tmp),	.Y(CK32		));

DLY4X1	TEST_DELAY_33	(.A(CK32	),	.Y(CK32_tmp	));
DLY4X1	TEST_DELAY_33_1	(.A(CK32_tmp),	.Y(CK33		));

DLY4X1	TEST_DELAY_34	(.A(CK33	),	.Y(CK33_tmp	));
DLY4X1	TEST_DELAY_34_1	(.A(CK33_tmp),	.Y(CK34		));

DLY4X1	TEST_DELAY_35	(.A(CK34	),	.Y(CK34_tmp	));
DLY4X1	TEST_DELAY_35_1	(.A(CK34_tmp),	.Y(CK35		));

DLY4X1	TEST_DELAY_36	(.A(CK35	),	.Y(CK35_tmp	));
DLY4X1	TEST_DELAY_36_1	(.A(CK35_tmp),	.Y(CK36		));

DLY4X1	TEST_DELAY_37	(.A(CK36	),	.Y(CK36_tmp	));
DLY4X1	TEST_DELAY_37_1	(.A(CK36_tmp),	.Y(CK37		));

DLY4X1	TEST_DELAY_38	(.A(CK37	),	.Y(CK37_tmp	));
DLY4X1	TEST_DELAY_38_1	(.A(CK37_tmp),	.Y(CK38		));

DLY4X1	TEST_DELAY_39	(.A(CK38	),	.Y(CK38_tmp	));
DLY4X1	TEST_DELAY_39_1	(.A(CK38_tmp),	.Y(CK39		));

DLY4X1	TEST_DELAY_40	(.A(CK39	),	.Y(CK39_tmp	));
DLY4X1	TEST_DELAY_40_1	(.A(CK39_tmp),	.Y(CK40		));

DLY4X1	TEST_DELAY_41	(.A(CK40	),	.Y(CK40_tmp	));
DLY4X1	TEST_DELAY_41_1	(.A(CK40_tmp),	.Y(CK41		));

DLY4X1	TEST_DELAY_42	(.A(CK41	),	.Y(CK41_tmp	));
DLY4X1	TEST_DELAY_42_1	(.A(CK41_tmp),	.Y(CK42		));

DLY4X1	TEST_DELAY_43	(.A(CK42	),	.Y(CK42_tmp	));
DLY4X1	TEST_DELAY_43_1	(.A(CK42_tmp),	.Y(CK43		));

DLY4X1	TEST_DELAY_44	(.A(CK43	),	.Y(CK43_tmp	));
DLY4X1	TEST_DELAY_44_1	(.A(CK43_tmp),	.Y(CK44		));

DLY4X1	TEST_DELAY_45	(.A(CK44	),	.Y(CK44_tmp	));
DLY4X1	TEST_DELAY_45_1	(.A(CK44_tmp),	.Y(CK45		));

DLY4X1	TEST_DELAY_46	(.A(CK45	),	.Y(CK45_tmp	));
DLY4X1	TEST_DELAY_46_1	(.A(CK45_tmp),	.Y(CK46		));

DLY4X1	TEST_DELAY_47	(.A(CK46	),	.Y(CK46_tmp	));
DLY4X1	TEST_DELAY_47_1	(.A(CK46_tmp),	.Y(CK47		));

DLY4X1	TEST_DELAY_48	(.A(CK47	),	.Y(CK47_tmp	));
DLY4X1	TEST_DELAY_48_1	(.A(CK47_tmp),	.Y(CK48		));

DLY4X1	TEST_DELAY_49	(.A(CK48	),	.Y(CK48_tmp	));
DLY4X1	TEST_DELAY_49_1	(.A(CK48_tmp),	.Y(CK49		));

DLY4X1	TEST_DELAY_50	(.A(CK49	),	.Y(CK49_tmp	));
DLY4X1	TEST_DELAY_50_1	(.A(CK49_tmp),	.Y(CK50		));

DLY4X1	TEST_DELAY_51	(.A(CK50	),	.Y(CK50_tmp	));
DLY4X1	TEST_DELAY_51_1	(.A(CK50_tmp),	.Y(CK51		));

DLY4X1	TEST_DELAY_52	(.A(CK51	),	.Y(CK51_tmp	));
DLY4X1	TEST_DELAY_52_1	(.A(CK51_tmp),	.Y(CK52		));

DLY4X1	TEST_DELAY_53	(.A(CK52	),	.Y(CK52_tmp	));
DLY4X1	TEST_DELAY_53_1	(.A(CK52_tmp),	.Y(CK53		));

DLY4X1	TEST_DELAY_54	(.A(CK53	),	.Y(CK53_tmp	));
DLY4X1	TEST_DELAY_54_1	(.A(CK53_tmp),	.Y(CK54		));

DLY4X1	TEST_DELAY_55	(.A(CK54	),	.Y(CK54_tmp	));
DLY4X1	TEST_DELAY_55_1	(.A(CK54_tmp),	.Y(CK55		));

DLY4X1	TEST_DELAY_56	(.A(CK55	),	.Y(CK55_tmp	));
DLY4X1	TEST_DELAY_56_1	(.A(CK55_tmp),	.Y(CK56		));

DLY4X1	TEST_DELAY_57	(.A(CK56	),	.Y(CK56_tmp	));
DLY4X1	TEST_DELAY_57_1	(.A(CK56_tmp),	.Y(CK57		));

DLY4X1	TEST_DELAY_58	(.A(CK57	),	.Y(CK57_tmp	));
DLY4X1	TEST_DELAY_58_1	(.A(CK57_tmp),	.Y(CK58		));

DLY4X1	TEST_DELAY_59	(.A(CK58	),	.Y(CK58_tmp	));
DLY4X1	TEST_DELAY_59_1	(.A(CK58_tmp),	.Y(CK59		));

DLY4X1	TEST_DELAY_60	(.A(CK59	),	.Y(CK59_tmp	));
DLY4X1	TEST_DELAY_60_1	(.A(CK59_tmp),	.Y(CK60		));

DLY4X1	TEST_DELAY_61	(.A(CK60	),	.Y(CK60_tmp	));
DLY4X1	TEST_DELAY_61_1	(.A(CK60_tmp),	.Y(CK61		));

DLY4X1	TEST_DELAY_62	(.A(CK61	),	.Y(CK61_tmp	));
DLY4X1	TEST_DELAY_62_1	(.A(CK61_tmp),	.Y(CK62		));

DLY4X1	TEST_DELAY_63	(.A(CK62	),	.Y(CK62_tmp	));
DLY4X1	TEST_DELAY_63_1	(.A(CK62_tmp),	.Y(CK63		));

MX4X4	TEST_MX4_0_3	(.S0(DLY_SEL[0]),	.S1(DLY_SEL[1]),	.A(CK  ),	.B(CK1 ),	.C(CK2 ),	.D(CK3 ),	.Y(CK_TMP_0));
MX4X4	TEST_MX4_4_7	(.S0(DLY_SEL[0]),	.S1(DLY_SEL[1]),	.A(CK4 ),	.B(CK5 ),	.C(CK6 ),	.D(CK7 ),	.Y(CK_TMP_1));
MX4X4	TEST_MX4_8_11	(.S0(DLY_SEL[0]),	.S1(DLY_SEL[1]),	.A(CK8 ),	.B(CK9 ),	.C(CK10),	.D(CK11),	.Y(CK_TMP_2));
MX4X4	TEST_MX4_12_15	(.S0(DLY_SEL[0]),	.S1(DLY_SEL[1]),	.A(CK12),	.B(CK13),	.C(CK14),	.D(CK15),	.Y(CK_TMP_3));
MX4X4	TEST_MX4_16_19	(.S0(DLY_SEL[0]),	.S1(DLY_SEL[1]),	.A(CK16),	.B(CK17),	.C(CK18),	.D(CK19),	.Y(CK_TMP_4));
MX4X4	TEST_MX4_20_23	(.S0(DLY_SEL[0]),	.S1(DLY_SEL[1]),	.A(CK20),	.B(CK21),	.C(CK22),	.D(CK23),	.Y(CK_TMP_5));
MX4X4	TEST_MX4_24_27	(.S0(DLY_SEL[0]),	.S1(DLY_SEL[1]),	.A(CK24),	.B(CK25),	.C(CK26),	.D(CK27),	.Y(CK_TMP_6));
MX4X4	TEST_MX4_28_31	(.S0(DLY_SEL[0]),	.S1(DLY_SEL[1]),	.A(CK28),	.B(CK29),	.C(CK30),	.D(CK31),	.Y(CK_TMP_7));
MX4X4	TEST_MX4_32_35	(.S0(DLY_SEL[0]),	.S1(DLY_SEL[1]),	.A(CK32),	.B(CK33),	.C(CK34),	.D(CK35),	.Y(CK_TMP_8));
MX4X4	TEST_MX4_36_39	(.S0(DLY_SEL[0]),	.S1(DLY_SEL[1]),	.A(CK36),	.B(CK37),	.C(CK38),	.D(CK39),	.Y(CK_TMP_9));
MX4X4	TEST_MX4_40_43	(.S0(DLY_SEL[0]),	.S1(DLY_SEL[1]),	.A(CK40),	.B(CK41),	.C(CK42),	.D(CK43),	.Y(CK_TMP_10));
MX4X4	TEST_MX4_44_47	(.S0(DLY_SEL[0]),	.S1(DLY_SEL[1]),	.A(CK44),	.B(CK45),	.C(CK46),	.D(CK47),	.Y(CK_TMP_11));
MX4X4	TEST_MX4_48_51	(.S0(DLY_SEL[0]),	.S1(DLY_SEL[1]),	.A(CK48),	.B(CK49),	.C(CK50),	.D(CK51),	.Y(CK_TMP_12));
MX4X4	TEST_MX4_52_55	(.S0(DLY_SEL[0]),	.S1(DLY_SEL[1]),	.A(CK52),	.B(CK53),	.C(CK54),	.D(CK55),	.Y(CK_TMP_13));
MX4X4	TEST_MX4_56_59	(.S0(DLY_SEL[0]),	.S1(DLY_SEL[1]),	.A(CK56),	.B(CK57),	.C(CK58),	.D(CK59),	.Y(CK_TMP_14));
MX4X4	TEST_MX4_60_63	(.S0(DLY_SEL[0]),	.S1(DLY_SEL[1]),	.A(CK60),	.B(CK61),	.C(CK62),	.D(CK63),	.Y(CK_TMP_15));

MX4X4	TEST_MX4_2_0	(.S0(DLY_SEL[2]),	.S1(DLY_SEL[3]),	.A(CK_TMP_0 ),	.B(CK_TMP_1 ),	.C(CK_TMP_2 ),	.D(CK_TMP_3 ),	.Y(CK_TMP_2_0));
MX4X4	TEST_MX4_2_1	(.S0(DLY_SEL[2]),	.S1(DLY_SEL[3]),	.A(CK_TMP_4 ),	.B(CK_TMP_5 ),	.C(CK_TMP_6 ),	.D(CK_TMP_7 ),	.Y(CK_TMP_2_1));
MX4X4	TEST_MX4_2_2	(.S0(DLY_SEL[2]),	.S1(DLY_SEL[3]),	.A(CK_TMP_8 ),	.B(CK_TMP_9 ),	.C(CK_TMP_10),	.D(CK_TMP_11),	.Y(CK_TMP_2_2));
MX4X4	TEST_MX4_2_3	(.S0(DLY_SEL[2]),	.S1(DLY_SEL[3]),	.A(CK_TMP_12),	.B(CK_TMP_13),	.C(CK_TMP_14),	.D(CK_TMP_15),	.Y(CK_TMP_2_3));

MX4X4	TEST_MX4_3_0	(.S0(DLY_SEL[4]),	.S1(DLY_SEL[5]),	.A(CK_TMP_2_0),	.B(CK_TMP_2_1),	.C(CK_TMP_2_2),	.D(CK_TMP_2_3),	.Y(CK_TMP_3_0));

DFFHQX4	FLAG_reg		(.CK(CK),	.D(CK_TMP_3_0),	.Q(FLAG));

endmodule
