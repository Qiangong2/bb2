/******************************************************************************
	  (c) copyrights 2000-. All rights reserved

	   T-Square Design, Inc.

	  Company confidential and Properietary	information.
	  This information may not be disclosed	to unauthorized individual.
*******************************************************************************/

/*******************************************************************************
 *	DESCRIPTION:	The clock generator in M6303E.
 *					Generate clock output in PCI/CD/uP/CPU mode
 *
 *	AUTHOR: 		Norman
 *
 *	HISTORY:   		2002.03.11	initial version
 					2002-06-18	change the SDRAM clock delay select from [2:0] to
 								[3:0]
 					2002-07-16	Remove clock source to PLL output. The PLL source is
 								from the first 27_48MHz crystal.
 								Remove the MEM_FS input.
 								Add MEM_CLK_FOUT input
 					2002-08-12	Add externl_pixel_clk_in and pixel_clk_src_sel
 								input.
 					2002-10-08	Remove the F27M, F100M input. The source clock is
 								fixed at 27MHz now and the F100M for IDE clock is not
 								needed.
 								Add F54M input, PSCAN_EN
 								Remove the PCI_CLK_CFG, PCI clock is generate from CPU clock
 								Remove EXT_PIXEL_CLK_IN, PIXEL_CLK_SRC_SEL input.
 								Remove CPU_SRC_CLK, JDCLK
 								Remove SPDIF_CLK, ATA100_CLK, ATA66_CLK
 					2003-07-01	M3357 version.
 								Add SERVO_CLK
 *
 *********************************************************************************/
module CLOCK_GEN (

	// input
		SRC_CLK,		// from clock input pad
		F48M,			// from F27_PLL 48M output
		F54M,			// from F27_PLL output
		PCI_MIPS_FS,	// select the divisor of PCI/MIPS bus clock
		DSP_FS,			// DSP frequency select
		DSP_DIV_SRC_SEL,// DSP source clock select
		VE_FS,			// VIDEO ENGINE frequency select
		J_TCLK,			// JTAG test mode clock
		TEST_EN,		// Test Mode Enable
		CPU_MODE,		// CPU standalone mode
		MEM_DLY_SEL,	// SDRAM clock pad delay chain control
		PLL_FVCO,		// Clock output from PLL
		MEM_CLK_FOUT,	// MEM Clock output from CPU PLL
		BYPASS_PLL,		// BY PASS CPU_PLL
		PCI_MBUS_SEL,	// PCI_MBUS clock output select
		TEST_MODE,		// CHIP TEST MODE
		PSCAN_EN,		// Progressive scan enable

	// output
		USB_CLK,		// USB clock 48M
		SB_CLK,			// SouthBridge Clock 12M
		CPU_CLK,		// CPU clock (with JTAG test mode mask)
		MBUS_CLK,		// MIPS clock
		MEM_CLK,		// internal SDRAM, NorthBridge clock
		DSP_CLK,		// DSP clock
		VE_CLK,			// VIDEO ENGINE CLOCK
//		PIXEL_CLK,		// PIXEL CLOCK, 27/54MHz
		F54M_CLK,		// 54MHz clock for IDE and TV encoder
		PCI_CLK,		// internal PCI bus clock
		OUT_MEM_CLK,	// output to SDRAM clock pad
		PCI_MBUS_CLK,	// output to PCI/MIPS clock pad
		SERVO_CLK,		// SERVO clock, it is 1/2 mem_clk

		COLD_RST_,
		RST_
		);

	// input
	input			SRC_CLK,		// clock pad input
					F48M,			// from 48M crystal
					J_TCLK,			// JTAG clock
					TEST_EN,		// Test Mode Enable
					CPU_MODE,		// CPU standalone mode
					PLL_FVCO;		// PLL clock input from cpu_pll
	input			MEM_CLK_FOUT;	// Memory clock output from CPU PLL
	input			F54M;			// 54 MHz clock input

	input	[1:0]	PCI_MIPS_FS;	// select the divisor of PCI/MIPS bus clock
	input	[3:0]	DSP_FS;			// DSP clock frequency select
	input			DSP_DIV_SRC_SEL;// DSP source clock select
	input	[1:0]	VE_FS;			// VIDEO ENGINE clock frequency select

	input	[4:0]	MEM_DLY_SEL;	// SDRAM clock pad delay chain control
	input			BYPASS_PLL;		// by pass cpu_pll
	input	[1:0]	PCI_MBUS_SEL;	// PCI_MUBS output select
	input			TEST_MODE;		// CHIP IN TEST MODE
	input			PSCAN_EN;		// Progressive Scan Enable

	// output
	output			USB_CLK,		// USB clock 48M
					SB_CLK,			// SouthBridge Clock 12M
					CPU_CLK,		// CPU clock (with JTAG test mode mask)
					MBUS_CLK,		// MIPS clock
					MEM_CLK,		// internal SDRAM, NorthBridge clock
					DSP_CLK,		// DSP clock
					VE_CLK,			// VIDEO ENGINE CLOCK
//					PIXEL_CLK,		// PIXEL_CLK
					PCI_CLK,		// internal PCI bus clock
					OUT_MEM_CLK,	// output to SDRAM clock pad
					PCI_MBUS_CLK;	// output to PCI/MIPS clock pad
	output			F54M_CLK;		// 54MHz clock for TV and IDE
	output			SERVO_CLK;		// SERVO clock, it is 1/2 mem_clk

	input			COLD_RST_;
	input			RST_;


	/*=========================================================================
		The clock input to CPU_PLL is 12MHz.
		The clock input to F27_PLL is 12MHz.
		The clock for SPDIF is 24MHz.
		Those clocks could be got by divide the F48M in by 2.
	=========================================================================*/
	wire	f48m_bypass_tmp = BYPASS_PLL ? SRC_CLK : F48M;

	reg		f24m;		// 24MHz clock to SPDIF and to 12MHz divider
	reg		f12m;		// 12Mhz clock to CPU_PLL, F27_PLL and SB

	always @ (negedge COLD_RST_ or posedge f48m_bypass_tmp)
	   if (!COLD_RST_)
	      f24m <= 0;
	   else
	      f24m <= ~f24m;

	always @ (negedge COLD_RST_ or posedge f24m)
	   if (!COLD_RST_)
	      f12m <= 0;
	   else
	      f12m <= ~f12m;

	/*=========================================================================
		The CPU_PLL could be by passed and the CPU_CLK would be 27MHz in
		In bypass mode
		CPU_CLK = input clock
		MEM_CLK = input clock
		F27M	= 1/4 input clock
		F48M	= 1/4 input clock
		F54M	= 1/4 input clock

	=========================================================================*/
	wire	div_clk_src;	// source clock send to clock_divider
	assign	div_clk_src 	= BYPASS_PLL ? SRC_CLK : PLL_FVCO;
	wire	mem_clk_src		= BYPASS_PLL ? SRC_CLK : MEM_CLK_FOUT;
	wire	f54m_clk_src	= BYPASS_PLL ? f12m : F54M;
	wire	f27m_clk_src	= BYPASS_PLL ? f12m : SRC_CLK;
	wire	f48m_clk_src 	= BYPASS_PLL ? f12m : F48M;

	clock_divider clock_divider (
			.pci_mips_fs	(PCI_MIPS_FS	),
			.dsp_fs			(DSP_FS			),
			.dsp_div_src_sel(DSP_DIV_SRC_SEL),	// dsp source clock selection
			.ve_fs			(VE_FS			),
			.div_clk_src	(div_clk_src	),
			.mem_clk_src	(MEM_CLK_FOUT	),	// memory clock from the mem_clk_pll

			.div_cpu_clk	(DIV_CPU_CLK	),
			.div_dsp_clk	(DIV_DSP_CLK	),
			.div_ve_clk		(DIV_VE_CLK		),
			.div_pci_m_clk	(DIV_PCI_M_CLK	),
			.div_servo_clk	(DIV_SERVO_CLK	),	// output SERVO clock to clock gateway

			.rst_			(RST_			)
			);


	clock_gateway clock_gateway (
			.f48m			(f48m_clk_src	),
			.f54m_in		(f54m_clk_src	),
			.f12m			(f12m			),
			.f27m			(f27m_clk_src	),

			.div_cpu_clk	(DIV_CPU_CLK	),
			.div_mem_clk	(mem_clk_src	),
			.div_ve_clk		(DIV_VE_CLK		),
			.div_dsp_clk	(DIV_DSP_CLK	),
			.div_pci_m_clk	(DIV_PCI_M_CLK	),
			.div_servo_clk	(DIV_SERVO_CLK	),
			.j_tclk			(J_TCLK			),
			.test_en		(TEST_EN		),
			.cpu_mode		(CPU_MODE		),
			.mem_dly_sel	(MEM_DLY_SEL	),
			.pci_mbus_sel	(PCI_MBUS_SEL	),
			.test_mode		(TEST_MODE		),
			.pscan_en		(PSCAN_EN		),

			.cpu_clk		(CPU_CLK		),
			.mbus_clk		(MBUS_CLK		),
			.mem_clk		(MEM_CLK		),
			.pci_clk		(PCI_CLK		),
			.out_mem_clk	(OUT_MEM_CLK	),
			.pci_mbus_clk	(PCI_MBUS_CLK	),
			.sb_clk			(SB_CLK			),
			.dsp_clk		(DSP_CLK		),
			.gpu_clk		(		),
			.usb_clk		(USB_CLK		),
			.pixel_clk		(PIXEL_CLK		),
			.f54m_clk		(F54M_CLK		),
			.servo_clk		(SERVO_CLK		),
			.ve_clk			(VE_CLK			)
			);

endmodule
