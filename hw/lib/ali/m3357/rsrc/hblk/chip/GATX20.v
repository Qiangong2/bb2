module GATX20 (CLK, EN, SE, YGCLK);

input CLK, EN, SE;
output YGCLK;

reg A1;

assign YGCLK = A1 & CLK;

always @ (CLK or SE or EN) begin
   if(!CLK) A1= SE | EN;
end

endmodule
