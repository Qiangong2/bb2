/******************************************************************************
	  (c) copyrights 2000-. All rights reserved

	   T-Square Design, Inc.

	  Company confidential and Properietary	information.
	  This information may not be disclosed	to unauthorized individual.
*******************************************************************************/

/*******************************************************************************
 *	DESCRIPTION:	The CPU_PLL behavior module in M6304.
 *
 *	AUTHOR: 		Norman
 *
 *	HISTORY:   		2002.06.15	initial version
 *					2002.07.16	Add MEM_CLK_FOUT output from CPU_PLL to get 50% duty cycle
 					2002.07.24	Add input port FIN_SEL
 					2002.08.07	Add output port FCK, LOCK
 					2002.08.21  Add input port PD
 					2002.10.07	Delete the FIN_SEL, because FIN will be fixed to 27MHz
 					2002.11.25	Add PLL power/ground input
 					2003.06.14	Modified for m3357,pll_m[5:0],mem_clk_fs[5:0]
 					2003.07.01	Remove the MEM_CLK_FS and MEM_CLK_FOUT
 					2003-08-28	Change analog power name
 *********************************************************************************/
`timescale	1ns/1ps

module	CPU_PLL	(
		FIN,
		PLL_M,
		PD,
		FOUT,
//		MEM_CLK_FS,
//		MEM_CLK_FOUT,
		TFCK,
		TLOCK,
		LOCKEN,
		PLLAVDD,
		PLLAVSS,
		PLLDVDD,
		PLLDVSS
		);

input			FIN;			// 48MHz input
input	[5:0]	PLL_M;			// CPU PLL M factor
input           PD;             // power down control
output			FOUT;			// CPU_PLL output
//input	[5:0]	MEM_CLK_FS;		// memory PLL M factor
//output			MEM_CLK_FOUT;	// memory clock output
output			TFCK;			// PLL TEST FCK
output			TLOCK;			// PLL TEST LOCK
input			LOCKEN;			// PLL lock gate off clock enable.
input			PLLAVSS;		// PLL analog ground
input			PLLAVDD;		// PLL analog power
input			PLLDVDD;		// PLL digital power
input			PLLDVSS;		// PLL digital ground

tri0	TFCK, TLOCK;	//temporary use, for kill no driver warning
/*=============================================================================
	48MHz input monitor
=============================================================================*/
reg		[9:0]	fin_clk_cnt;
real			fin_clk_1000times;
real			fin_clk_start;

initial	begin
	fin_clk_cnt = 0;
	fin_clk_1000times = 0;
	fin_clk_start = 0;
	wait	(fin_clk_cnt == 10'd1001);
    $display ("CPU PLL FIN frequency is %f MHz", (1000000/fin_clk_1000times));
//    if (fin_clk_1000times != 1000000/48) begin
//    	$display ("CPU_PLL FIN clock frequency Error! It must be fixed at 48MHz");
//    	$display ("CPU_PLL FIN_SEL is %b", FIN_SEL);
//    end
//    else begin
//    	$display ("FIN clock frequency Correct!");
//    end
end

// fin clock monitor
always @ (posedge FIN)
	if (fin_clk_cnt == 10'd0) begin
	   fin_clk_start	= $realtime;
	   fin_clk_cnt		= fin_clk_cnt + 1;
	end
	else if (fin_clk_cnt == 10'd1000) begin
	   fin_clk_1000times	= $realtime - fin_clk_start;
	   fin_clk_cnt			= fin_clk_cnt + 1;
	end
	else if (fin_clk_cnt != 10'h3ff) begin
	   fin_clk_cnt		= fin_clk_cnt + 1;
	end

/*=============================================================================
	output clock generator
	No swith time now
	No relation to the input frequency
=============================================================================*/
wire	FOUT;
real	fout_half_cycle;
reg	[3:0]	fout_clk_cnt;
real		fout_clk_start;
real		fout_clk_10times;

reg		PLL_M_FLAG;

reg		fout_temp;

real	mem_fout_half_cycle;
reg	[3:0]	mem_fout_clk_cnt;
real		mem_fout_clk_start;
real		mem_fout_clk_10times;

reg		MEM_CLK_FS_FLAG;

reg		mem_fout_temp;

initial	begin
	fout_half_cycle = 3.76;//133MHz
	fout_temp = 0;
	mem_fout_half_cycle = 3.76;//133MHz
	mem_fout_temp = 0;
	MEM_CLK_FS_FLAG = 1;
	PLL_M_FLAG = 1;
	#10 MEM_CLK_FS_FLAG = 0;
	#10 PLL_M_FLAG = 0;

end

always @ (PLL_M)
	begin
	PLL_M_FLAG = 1;
	if (PLL_M !== 6'hx & PLL_M > 0)
		fout_half_cycle = 1000.000/(9*PLL_M);//JFR 030616
	else
		fout_half_cycle = 3.76;//133MHz
	end


always #fout_half_cycle fout_temp = !fout_temp;

assign	FOUT = (PLL_M !== 6'hx) ? ((PLL_M >= 8) ? fout_temp : 0) : 0;//JFR 030616

// fout clock monitor
always @ (posedge FOUT or negedge PLL_M_FLAG)
	if (!PLL_M_FLAG) begin
	   fout_clk_cnt		= 0;
	end
	else if (fout_clk_cnt == 4'd2) begin
	   fout_clk_start	= $realtime;
	   fout_clk_cnt		= fout_clk_cnt + 1;
	end
	else if (fout_clk_cnt == 4'd12) begin
	   fout_clk_10times	= $realtime - fout_clk_start;
	   fout_clk_cnt		= fout_clk_cnt + 1;
	   $display("FOUT frequency is %f MHz at %m", 10000/fout_clk_10times);
	end
	else if (fout_clk_cnt != 4'hf) begin
	   fout_clk_cnt		= fout_clk_cnt + 1;
	end
	else if (fout_clk_cnt == 4'hf) begin
	   PLL_M_FLAG <= #1 0;
	end

endmodule

