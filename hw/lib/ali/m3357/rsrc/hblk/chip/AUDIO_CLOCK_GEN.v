/******************************************************************************
	  (c) copyrights 2000-. All rights reserved

	   T-Square Design, Inc.

	  Company confidential and Properietary	information.
	  This information may not be disclosed	to unauthorized individual.
*******************************************************************************/

/*******************************************************************************
 *	DESCRIPTION:	The clock generator in M6304 for Audio.
 *
 *	AUTHOR: 		Norman
 *
 *	HISTORY:   		2002.10.08	initial version
 					2003-09-08	Add IADC_MCLK output
 *
 *********************************************************************************/
module AUDIO_CLOCK_GEN (

	// input
		SRC_CLK,		// from clock input pad
		AUDIO_PLL_FOUT,	// Audio PLL output clock
//		F24M,			// 24.576MHz clock input from PLL
//		F16M,			// 16.9344MHz clock input from PLL
		JTCLK,			// jtclk from pad
		I2S_MCLK_IN,	// I2S master clock from PAD input
		AC97BCK,		// AC97 bck from PAD
		I2S_MCLK_SEL,	// I2S Master clock frequency select
		SPDIF_CLK_SEL,	// SPDIF clock frequency selectDSP_FS

		BYPASS_PLL,		// By pass audio PLL
		TEST_MODE,		// Chip in test mode
	// output
		I2S_MCLK,		// I2S master clock to audio core and PAD
		SPDIF_CLK,		// spdif clock to audio core
		AC97BCK_OUT,	// AC97 BCK output to audio core
		IADC_MCLK,		// IADC master clock

		RST_

		);

	// input
input			SRC_CLK;		// from clock input pad
input			AUDIO_PLL_FOUT;	// Audio PLL output clock
//input			F24M;			// 24.576MHz clock input from PLL
//input			F16M;			// 16.9344MHz clock input from PLL
input			JTCLK;			// jtclk from pad
input			I2S_MCLK_IN;	// I2S master clock from PAD input
input			AC97BCK;		// AC97 bck from PAD
input	[1:0]	I2S_MCLK_SEL;	// I2S Master clock frequency select
input	[1:0]	SPDIF_CLK_SEL;	// SPDIF clock frequency selectDSP_FS

input			BYPASS_PLL;		// By pass audio PLL
input			TEST_MODE;		// Chip in test mode
	// output
output			I2S_MCLK;		// I2S master clock to audio core and PAD
output			SPDIF_CLK;		// spdif clock to audio core
output			AC97BCK_OUT;	// AC97 BCK output to audio core
output			IADC_MCLK;		// IADC master clock

input			RST_;

	//=========================================================================
	// Get 1/4 input source clock
	//=========================================================================
	reg		src_12;		// 1/2 source clock
	reg		src_14;		// 1/4 source clock

	always @ (negedge RST_ or posedge SRC_CLK)
	   if (!RST_)
	      src_12 <= 0;
  	   else
	      src_12 <= ~src_12;

	always @ (negedge RST_ or posedge src_12)
	   if (!RST_)
	      src_14 <= 0;
	   else
	      src_14 <= ~src_14;

	//=========================================================================
	// Get 1/2 audio pll fout
	//=========================================================================
	reg		audio_pll_fout_12;
	always @ (negedge RST_ or posedge AUDIO_PLL_FOUT)
	   if (!RST_)
	      audio_pll_fout_12 <= 0;
	   else
	      audio_pll_fout_12 <= ~audio_pll_fout_12;

	/*=========================================================================
		I2S MCLK Select
		00 -> AUDIO_PLL_FOUT
		01 -> 1/2 AUDIO_PLL_FOUT
		10 -> external input
		11 -> external input

		In bypass mode, i2s_mclk = 1/4 source clock from pad
	=========================================================================*/
	wire	I2S_MCLK_tmp = BYPASS_PLL ? src_14 :
						  ((I2S_MCLK_SEL == 2'b00) & AUDIO_PLL_FOUT |
						   (I2S_MCLK_SEL == 2'b01) & audio_pll_fout_12 |
						   (I2S_MCLK_SEL == 2'b10) & I2S_MCLK_IN |
						   (I2S_MCLK_SEL == 2'b11) & I2S_MCLK_IN);

	assign	I2S_MCLK	= TEST_MODE ? JTCLK : I2S_MCLK_tmp;

	/*=========================================================================
		SPDIF Clock Select
		00 -> AUDIO_PLL_FOUT
		01 -> 1/2 AUDIO_PLL_FOUT
		10 -> I2S MCLK in
		11 -> I2S MCLK in
		In bypass mode, spdif_clk = 1/4 source clock from pad
	=========================================================================*/
	wire	SPDIF_CLK_tmp = BYPASS_PLL ? src_14 :
						   ((SPDIF_CLK_SEL == 2'b00) & AUDIO_PLL_FOUT |
						    (SPDIF_CLK_SEL == 2'b01) & audio_pll_fout_12 |
						    (SPDIF_CLK_SEL == 2'b10) & I2S_MCLK_IN |
						    (SPDIF_CLK_SEL == 2'b11) & I2S_MCLK_IN);

	assign	SPDIF_CLK	= TEST_MODE ? JTCLK : SPDIF_CLK_tmp;

	assign	AC97BCK_OUT	= TEST_MODE ? JTCLK : AC97BCK;

	wire	IADC_MCLK_tmp = BYPASS_PLL ? src_14 : AUDIO_PLL_FOUT;
	assign	IADC_MCLK	= TEST_MODE ? JTCLK : IADC_MCLK_tmp;

endmodule
