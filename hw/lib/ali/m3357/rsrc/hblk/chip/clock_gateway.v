/******************************************************************************
	  (c) copyrights 2000-. All rights reserved

	   T-Square Design, Inc.

	  Company confidential and Properietary	information.
	  This information may not be disclosed	to unauthorized individual.
*******************************************************************************/

/*******************************************************************************
 *
 *	DESCRIPTION:	The clock gateway in M6303E.
 *					Generate clock output in PCI/CD/uP/CPU mode
 *
 *	AUTHOR:			Norman
 *
 *	HISTORY:		2002.03.11	initial version
 					2002.06.12	add the selection to PCI_CLK output pad
 								add f27m, f24m input, add ve_clk, pixel_clk, spdif_clk
 					2002-06-18	change the port width mem_dly_sel from [2:0] to [3:0]
 					2002-07-04	add input div_100_clk, div_66_clk, div_33_clk
 								The PCI clock output pad have 8 source to select now.
 								The pci_mbus_sel changed from [1:0] to [2:0].
 								Add pci_clk_cfg to control internal PCI clock source.
 					2002-07-12	Add two delay cell for ATA66 and ATA100 clock
 					2002-09-05	Add CPU_CLK in test mode should be switch to jtag clock
 					2002-10-08	Remove f24m, div_100_clk, div_66_clk, div_33_clk, div_trace_clk,
 								pci_clk_cfg, cpu_clk_src,
 								ata100_clk, ata66_clk, spdif_clk
 					2003-12-09	Switch the MEM_CLK to inverted jtag clock in scan test mode

 *
 *********************************************************************************/
module clock_gateway(
		f48m,
		f12m,
		f27m,
		f54m_in,

		div_cpu_clk,
		div_mem_clk,
		div_dsp_clk,
		div_ve_clk,
		div_pci_m_clk,
		div_servo_clk,
		j_tclk,
		test_en,
		cpu_mode,
		mem_dly_sel,
		pci_mbus_sel,
//		pci_clk_cfg,
		test_mode,
		pscan_en,

		cpu_clk,
//		cpu_clk_src,
		mbus_clk,
		mem_clk,
		pci_clk,
		out_mem_clk,
		pci_mbus_clk,
		sb_clk,
		usb_clk,
		dsp_clk,
		gpu_clk,
		ve_clk,
		f54m_clk,
		servo_clk,
		pixel_clk
		);

	input			f48m,				// crystal clock input
					f54m_in,			// 54MHz clock input
					f12m,				// 1/4 crystal clock
					f27m,				// 27MHz for video application
					div_cpu_clk,		// cpu clock from clock_divider
					div_mem_clk,		// mem clock from clock_divider
					div_dsp_clk,		// dsp clock from clock divider
					div_ve_clk,			// video engine clock from clock divider
					div_pci_m_clk,		// pci_m_clk form clock divider
					div_servo_clk,		// servo clock from clock divider
					j_tclk,				// EJTAG clock from ICE
					test_en,			// CPU test mode enable
					cpu_mode;			// CPU mode of M6303E
	input	[4:0]	mem_dly_sel;		// memory clock delay select JFR030718
	input	[1:0]	pci_mbus_sel;		// PCI_CLK output pad source selection
	input			test_mode;			// Chip in Test Mode
	input			pscan_en;			// Progressive scan enable

	output			cpu_clk,			// cpu clock
					mbus_clk,			// mbus clock
					mem_clk,			// memory clock
					pci_clk,			// pci clock
					out_mem_clk,		// memory write clock
					pci_mbus_clk,		// pci_mbus_clk
					sb_clk,				// south bridge clock
					usb_clk,			// USB clock
					dsp_clk,			// DSP clock
					gpu_clk,			// GPU clock
					ve_clk,				// video engine clock
					f54m_clk,			// 54 MHz clock for IDE and TV encoder
					servo_clk,			// servo clock
					pixel_clk;			// pixel_clk


	/*=========================================================================
		Memory clock
		Insert fixed delay in internal memory clock
		Insert programmable delay in external memory clock
	=========================================================================*/
	wire	mem_dly_fix_out, mem_dly_prg_out;

	//	MEM clock be the PLL output clock or the BYPASSED clock
	wire	mem_clk_tmp = div_mem_clk;
	P_DLY_CHAIN32 mem_clk_out_chain(
						.A			(mem_clk_tmp		),
						.Y			(mem_dly_prg_out	),
						.YN			(),
						.DLY_SEL	(mem_dly_sel		)
						);

//	DLYCELL02	mem_clk_in_chain(
//						.A	(mem_clk_tmp		),
//						.Y	(mem_dly_fix_out	)
//						);

	assign	mem_clk		= test_mode ? ~j_tclk : mem_clk_tmp;
	assign	out_mem_clk = test_mode ? ~j_tclk : mem_dly_prg_out;

	/*=========================================================================
		CPU clock
	=========================================================================*/
	assign	cpu_clk 	= (test_en | test_mode) ? j_tclk : div_cpu_clk;

	/*=========================================================================
		pci_mbus clock, output to PCI_CLK PAD
		pci clock, internal PCI_CLK
		mbus clock
		When the M6304 in CPU mode, internal pci clock is mbus_clk. When the
		PCI_CLK_CFG is
	=========================================================================*/
	assign	pci_mbus_clk= (pci_mbus_sel[1:0] == 2'b00) & pci_clk		|
						  (pci_mbus_sel[1:0] == 2'b01) & div_dsp_clk	|
						  (pci_mbus_sel[1:0] == 2'b10) & div_servo_clk	|
						  (pci_mbus_sel[1:0] == 2'b11) & div_ve_clk;

	assign	pci_clk		= test_mode ? j_tclk : div_pci_m_clk;

	assign	mbus_clk 	= test_en ? j_tclk : div_pci_m_clk;

	/*=========================================================================
		dsp_clk
	=========================================================================*/
	assign	dsp_clk		= test_mode ? j_tclk : div_dsp_clk;

	/*=========================================================================
		servo_clk
	=========================================================================*/
	assign	servo_clk	= test_mode ? j_tclk : div_servo_clk;

	/*=========================================================================
		ve_clk
	=========================================================================*/
	assign	ve_clk		= test_mode ? j_tclk : div_ve_clk;

	/*=========================================================================
		South bridge clock
		USB clock
	=========================================================================*/
	assign	sb_clk		= test_mode ? j_tclk : f12m;
	assign	usb_clk 	= test_mode ? j_tclk : f48m;

	/*=========================================================================
		Pixel Clock
	=========================================================================*/
	assign	pixel_clk	= test_mode ? j_tclk :
						  pscan_en	? f54m_in : f27m;

	//=========================================================================
	//	TV/IDE clock 54MHz
	assign	f54m_clk	= test_mode ? j_tclk : f54m_in;

endmodule


