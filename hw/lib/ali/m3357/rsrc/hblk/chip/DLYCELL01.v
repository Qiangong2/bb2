/******************************************************************************
	  (c) copyrights 2000-. All rights reserved

	   T-Square Design, Inc.

	  Company confidential and Properietary	information.
	  This information may not be disclosed	to unauthorized individual.
*******************************************************************************/

/*******************************************************************************
 *
 *
 *       AUTHOR:	Norman Yang
 *
 *       HISTORY:	2002-07-12	initial version
 *
 *********************************************************************************/
module	DLYCELL01	(
	A,			// input source
	Y			// fixed delay output
	);

parameter	fix_dly = 1.0;	// 1.0 ns delay

input		A;
output		Y;

assign	#fix_dly Y = A;

endmodule

