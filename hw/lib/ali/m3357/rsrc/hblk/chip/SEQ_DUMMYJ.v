
/* Verilog Model Created from ECS Schematic seq_dummyj.sch -- Mar 22, 2001 09:25 */

module SEQ_DUMMYJ( CLK, DMY_IN, RSTJ, SDMY_OUT );
 input CLK, DMY_IN, RSTJ;
output SDMY_OUT;

wire N_12;
wire N_13;
wire N_14;
wire N_15;
wire N_16;
wire LOW;
wire N_6;
wire N_10;
wire N_11;
wire N_1;
wire N_2;
wire N_4;

TIELO I_8 ( .Y(LOW) );
SDFFRX4 I_6 ( .CK(CLK), .D(N_4), .Q(SDMY_OUT), .QN(N_6), .RN(RSTJ), .SE(LOW),
           .SI(SDMY_OUT) );
SDFFRX4 I_5 ( .CK(CLK), .D(N_15), .Q(N_4), .QN(N_12), .RN(RSTJ), .SE(LOW), .SI(N_4) );
SDFFRX4 I_4 ( .CK(CLK), .D(N_2), .Q(N_15), .QN(N_13), .RN(RSTJ), .SE(LOW),
           .SI(N_15) );
SDFFRX4 I_3 ( .CK(CLK), .D(N_1), .Q(N_2), .QN(N_14), .RN(RSTJ), .SE(LOW), .SI(N_2) );
SDFFRX4 I_2 ( .CK(CLK), .D(N_16), .Q(N_1), .QN(N_10), .RN(RSTJ), .SE(LOW), .SI(N_1) );
SDFFRX4 I_1 ( .CK(CLK), .D(DMY_IN), .Q(N_16), .QN(N_11), .RN(RSTJ), .SE(LOW),
           .SI(N_16) );

endmodule // seq_dummyj

