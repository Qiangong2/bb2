module spg( Z, A, CLK );
input  A, CLK;
output Z;
    wire FF2_ON, IV1_O, IV2_O, MUX1_O, ND1_O, FF2_O, IV3_O, NR1_O, MUX2_O, 
        NR2_O, FF1_ON, ND2_O, IV4_O, FF1_O, XOR1_O;
    INVX2 IV1 ( .A(A), .Y(IV1_O) );
    INVX2 IV2 ( .A(IV1_O), .Y(IV2_O) );
    NOR3X2 NR1 ( .A(MUX1_O), .B(ND2_O), .C(IV2_O), .Y(NR1_O) );
    DFFRX2 FF1 ( .D(MUX2_O), .CK(CLK), .RN(1'b0), .Q(FF1_O), .QN(FF1_ON)
         );
    NAND3X2 ND1 ( .A(FF2_ON), .B(NR2_O), .C(NR1_O), .Y(ND1_O) );
    MX2X2 MUX2 ( .S0(1'b0), .B(NR1_O), .A(IV4_O), .Y(MUX2_O) );
    INVX2 IV3 ( .A(FF1_ON), .Y(IV3_O) );
    INVX2 IV4 ( .A(ND1_O), .Y(IV4_O) );
    XOR2X2 XOR2 ( .A(1'b0), .B(XOR1_O), .Y(Z) );
    DFFRX2 FF2 ( .D(MUX1_O), .CK(CLK), .RN(1'b0), .Q(FF2_O), .QN(FF2_ON)
         );
    NOR3X2 NR2 ( .A(1'b0), .B(1'b0), .C(FF2_O), .Y(NR2_O) );
    MX2X2 MUX1 ( .S0(1'b0), .B(1'b0), .A(IV3_O), .Y(MUX1_O) );
    NAND3X2 ND2 ( .A(1'b0), .B(FF1_O), .C(NR2_O), .Y(ND2_O) );
    XOR2X2 XOR1 ( .A(1'b0), .B(ND2_O), .Y(XOR1_O) );
endmodule