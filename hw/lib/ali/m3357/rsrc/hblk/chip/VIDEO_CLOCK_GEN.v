/******************************************************************************
	  (c) copyrights 2000-. All rights reserved

	   T-Square Design, Inc.

	  Company confidential and Properietary	information.
	  This information may not be disclosed	to unauthorized individual.
*******************************************************************************/

/*******************************************************************************
 *	DESCRIPTION:	The clock generator in M3357 for Video.
 *
 *	AUTHOR: 		Norman
 *
 *	HISTORY:   		2003.08.29	initial version
 					2003-12-06	Add the 80 MHz clock input.
 								Add SVGA mode enable.
 								Add the clock delay and polarity adjustment
 *
 *********************************************************************************/
module VIDEO_CLOCK_GEN (

	// input
		SRC_CLK,			// from clock input pad
		F108M_IN,			// 108MHz clock input from PLL
		F80M_IN,			// 80MHz clock input from PLL
		JTCLK,				// jtclk from pad
		VIDEO_CLK_IN,		// VIDEO clock from clock gen

		BYPASS_PLL,			// By pass audio PLL
		TEST_MODE,			// Chip in scan test mode
		INTER_ONLY,			// Interlace output
		DVI_RWBUF_SEL,		// DVI buffer read/write
		SVGA_MODE_EN,		// SVGA mode enable, TV encoder output the
	// output
//		F108M_CLK,			// 108MHz output
		CVBS2X_CLK,			// 54MHZ clock
		CVBS_CLK,			// 27MHz clock
//		CAV2X_CLK,			// 108 or 54 MHZ clock
		CAV_CLK,			// 54 or 27 MHZ clock
		DVI_BUF0_CLK,		// CAV_CLK or VIDEO_CLK for buffer 0
		DVI_BUF1_CLK,		// CAV_CLK or VIDEO_CLK for buffer 1
		VIDEO_CLK,			// VIDEO clock output

		TV_ENC_CLK_SEL,	// video clock delay select
		F108M_CLK_TV,		// 108MHz output for TV encoder
		CVBS2X_CLK_TV,		// 54MHZ clock for TV encoder
		CVBS_CLK_TV,		// 27MHz clock for TV encoder
		CAV2X_CLK_TV,		// 108 or 54 MHZ clock for TV encoder
		CAV_CLK_TV,			// 54 or 27 MHZ clock for TV encoder

		RST_
		);

	// input
input			SRC_CLK;			// from clock input pad
input			F108M_IN;			// 108MHz clock input from PLL
input			F80M_IN;			// 80MHz clock input from PLL
input			JTCLK;				// jtclk from pad
input			VIDEO_CLK_IN;		// VIDEO clock from clock gen

input			BYPASS_PLL;			// By pass audio PLL
input			TEST_MODE;			// Chip in test mode
input			INTER_ONLY;			// Interlace output
input			DVI_RWBUF_SEL;		// DVI buffer read/write
input			SVGA_MODE_EN;		// SVGA mode enable, TV encoder output the
	// output
//output			F108M_CLK;			// 108MHz output
output			CVBS2X_CLK;			// 54MHZ clock
output			CVBS_CLK;			// 27MHz clock
//output			CAV2X_CLK;			// 108 or 54 MHZ clock
output			CAV_CLK;			// 54 or 27 MHZ clock
output			DVI_BUF0_CLK;		// CAV_CLK or VIDEO_CLK for buffer 0
output			DVI_BUF1_CLK;		// CAV_CLK or VIDEO_CLK for buffer 1
output			VIDEO_CLK;			// VIDEO clock output

input	[11:8]	TV_ENC_CLK_SEL;	// video clock delay select
output			F108M_CLK_TV;		// 108MHz output for TV encoder
output			CVBS2X_CLK_TV;		// 54MHZ clock for TV encoder
output			CVBS_CLK_TV;		// 27MHz clock for TV encoder
output			CAV2X_CLK_TV;		// 108 or 54 MHZ clock for TV encoder
output			CAV_CLK_TV;			// 54 or 27 MHZ clock for TV encoder

input			RST_;

	//=========================================================================
	// Get 1/2 input source clock
	//=========================================================================
	reg		src_12;		// 1/2 source clock

	always @ (negedge RST_ or posedge SRC_CLK)
	   if (!RST_)
	      src_12 <= 0;
  	   else
	      src_12 <= ~src_12;

	//=========================================================================
	// Get 108, 80, 54, 27 MHz clocks
	//=========================================================================
	wire	f108m_clk_tmp	= BYPASS_PLL ? src_12 : F108M_IN;
	reg		f54m_clk_tmp;
	reg		f27m_clk_tmp;
	wire	f80m_clk_tmp	= BYPASS_PLL ? f54m_clk_tmp : F80M_IN;

	always @ (negedge RST_ or posedge f108m_clk_tmp)
		if (!RST_)
		   f54m_clk_tmp <= 0;
		else
		   f54m_clk_tmp <= ~f54m_clk_tmp;

	always @ (negedge RST_ or posedge f54m_clk_tmp)
		if (!RST_)
		   f27m_clk_tmp <= 0;
		else
		   f27m_clk_tmp <= ~f27m_clk_tmp;


	//=========================================================================
	// outputs
	//=========================================================================
	wire	F108M_CLK	= TEST_MODE ? JTCLK : f108m_clk_tmp;
	assign	CVBS2X_CLK	= TEST_MODE ? JTCLK : f54m_clk_tmp;
	assign	CVBS_CLK	= TEST_MODE ? JTCLK : f27m_clk_tmp;
	wire	CAV2X_CLK	= TEST_MODE ? JTCLK :
							(SVGA_MODE_EN ? f80m_clk_tmp :
							 (INTER_ONLY ? f54m_clk_tmp : f108m_clk_tmp));
	assign	CAV_CLK		= TEST_MODE ? JTCLK :
							(SVGA_MODE_EN ? f80m_clk_tmp :
							 (INTER_ONLY ? f27m_clk_tmp : f54m_clk_tmp));

	assign	DVI_BUF0_CLK	= TEST_MODE ? JTCLK : (DVI_RWBUF_SEL ? VIDEO_CLK_IN : CAV_CLK);
	assign	DVI_BUF1_CLK	= TEST_MODE ? JTCLK : (DVI_RWBUF_SEL ? CAV_CLK : VIDEO_CLK_IN);
	assign	VIDEO_CLK		= TEST_MODE ? JTCLK : VIDEO_CLK_IN;

	wire	[11:8]	tv_enc_clk_sel_tmp	= TEST_MODE ? 4'b0 : TV_ENC_CLK_SEL;

	//	F108 MHz delay
	assign	F108M_CLK_TV	= F108M_CLK;

	//	CVBS2X_CLK  delay
	VIDEO_CLK_DLY_CHAIN		DLY_CVBS2X	(
		.CLK_IN		(CVBS2X_CLK					),	// input clock source
		.CLK_OUT	(CVBS2X_CLK_TV				),	// Delay chain output clock
		.DLY_SEL	(tv_enc_clk_sel_tmp[9:8]	)	// delay select
		);

	//	CVBS_CLK  delay
	VIDEO_CLK_DLY_CHAIN		DLY_CVBS	(
		.CLK_IN		(CVBS_CLK					),	// input clock source
		.CLK_OUT	(CVBS_CLK_TV				),	// Delay chain output clock
		.DLY_SEL	(tv_enc_clk_sel_tmp[9:8]	)	// delay select
		);

	//	CAV2X_CLK  delay
	VIDEO_CLK_DLY_CHAIN		DLY_CAV2X	(
		.CLK_IN		(CAV2X_CLK					),	// input clock source
		.CLK_OUT	(CAV2X_CLK_TV				),	// Delay chain output clock
		.DLY_SEL	(tv_enc_clk_sel_tmp[11:10]	)	// delay select
		);

	//	CAV_CLK  delay
	VIDEO_CLK_DLY_CHAIN		DLY_CAV	(
		.CLK_IN		(CAV_CLK					),	// input clock source
		.CLK_OUT	(CAV_CLK_TV					),	// Delay chain output clock
		.DLY_SEL	(tv_enc_clk_sel_tmp[11:10]	)	// delay select
		);

endmodule
