/******************************************************************************
	  (c) copyrights 2000-. All rights reserved

	   T-Square Design, Inc.

	  Company confidential and Properietary	information.
	  This information may not be disclosed	to unauthorized individual.
*******************************************************************************/

/*******************************************************************************
 *	DESCRIPTION:	The F27_PLL behavior module in M6304.
 *
 *	AUTHOR: 		Norman
 *
 *	HISTORY:   		2002.06.15	initial version
 *					2002.07.16	add input FIN, add output FOUT_27, FOUT_48.
 					2002.08.07	Add output port FCK, LOCK
 					2002.08.21	Add input port PD
 					2002.10.07	Delete the FIN_SEL, because FIN will be fixed to 27MHz
 								Modify the FOUT_27 to FOUT_54
 					2002.11.25	Add PLL power/ground
 					2003-08-28	Change analog power name
 								Add 108 MHz output
 					2003-12-06	Add 80 MHz output
 *********************************************************************************/
`timescale	1ns/1ps

module	F27_PLL	(
		FIN,
		PD,
		FOUT_48,
		FOUT_54,
		FOUT_80,
		FOUT_108,
		TFCK,
		TLOCK,
		LOCKEN,
		PLLAVSS,
		PLLAVDD,
		PLLDVDD,
		PLLDVSS

		);

input			FIN;		// 48MHz input
input           PD;			// power down control
output			FOUT_48;	// 48MHz output
output			FOUT_54;	// 54MHz output
output			FOUT_80;	// 80MHz output
output			FOUT_108;	// 108MHz output
output			TFCK;		// PLL TEST FCK
output			TLOCK;		// PLL TEST LOCK
input			LOCKEN;		// PLL clock gate off enable
input			PLLAVSS;	// PLL analog ground
input			PLLAVDD;	// PLL analog power
input			PLLDVDD;	// PLL digital power
input			PLLDVSS;	// PLL digital ground

tri0	TFCK, TLOCK;	//temporary use, for kill no driver warning
/*=============================================================================
	input monitor
=============================================================================*/
reg		[9:0]	fin_clk_cnt;
real			fin_clk_1000times;
real			fin_clk_start;

initial	begin
	fin_clk_cnt = 0;
	fin_clk_1000times = 0;
	fin_clk_start = 0;
	wait	(fin_clk_cnt == 10'd1001);
    $display ("F27 PLL FIN frequency is %f MHz", (1000000/fin_clk_1000times));
//    if (fin_clk_1000times != 1000000/48) begin
//    	$display ("FIN clock frequency Error! It must be fixed at 48MHz");
//    end
//    else begin
//    	$display ("FIN clock frequency Correct!");
//    end
end

// fin clock monitor
always @ (posedge FIN)
	if (fin_clk_cnt == 10'd0) begin
	   fin_clk_start	= $realtime;
	   fin_clk_cnt		= fin_clk_cnt + 1;
	end
	else if (fin_clk_cnt == 10'd1000) begin
	   fin_clk_1000times	= $realtime - fin_clk_start;
	   fin_clk_cnt			= fin_clk_cnt + 1;
	end
	else if (fin_clk_cnt != 10'h3ff) begin
	   fin_clk_cnt		= fin_clk_cnt + 1;
	end

/*=============================================================================
	output clock generator
	No swith time now
	No relation to the input frequency
=============================================================================*/

reg			FOUT_48_tmp;
reg	[3:0]	fout_48_clk_cnt;
real		fout_48_clk_start;
real		fout_48_clk_10times;

initial begin
	FOUT_48_tmp = 10;
	fout_48_clk_cnt = 0;
	fout_48_clk_start = 1;
	fout_48_clk_10times = 1;
end

always #10.4167 FOUT_48_tmp = !FOUT_48_tmp;

// fout clock monitor
always @ (posedge FOUT_48)
	if (fout_48_clk_cnt == 4'd1) begin
	   fout_48_clk_start	= $realtime;
	   fout_48_clk_cnt		= fout_48_clk_cnt + 1;
	end
	else if (fout_48_clk_cnt == 4'd11) begin
	   fout_48_clk_10times	= $realtime - fout_48_clk_start;
	   fout_48_clk_cnt		= fout_48_clk_cnt + 1;
	   $display("FOUT_48 frequency is %fMHz at %m", 10000/fout_48_clk_10times);
	end
	else if (fout_48_clk_cnt != 4'hf) begin
	   fout_48_clk_cnt		= fout_48_clk_cnt + 1;
	end

/*=============================================================================
	output clock generator for 54MHz
	No swith time now
	No relationship with the input frequency
=============================================================================*/
reg			FOUT_54_tmp;
reg	[3:0]	fout_54_clk_cnt;
real		fout_54_clk_start;
real		fout_54_clk_10times;

initial begin
	FOUT_54_tmp = 10;
	fout_54_clk_cnt = 0;
	fout_54_clk_start = 1;
	fout_54_clk_10times = 1;
end

always #9.259259 FOUT_54_tmp = !FOUT_54_tmp;

// fout clock monitor
always @ (posedge FOUT_54)
	if (fout_54_clk_cnt == 4'd1) begin
	   fout_54_clk_start	= $realtime;
	   fout_54_clk_cnt		= fout_54_clk_cnt + 1;
	end
	else if (fout_54_clk_cnt == 4'd11) begin
	   fout_54_clk_10times	= $realtime - fout_54_clk_start;
	   fout_54_clk_cnt		= fout_54_clk_cnt + 1;
	   $display("FOUT_54 frequency is %fMHz at %m", 10000/fout_54_clk_10times);
	end
	else if (fout_54_clk_cnt != 4'hf) begin
	   fout_54_clk_cnt		= fout_54_clk_cnt + 1;
	end

/*=============================================================================
	output clock generator for 80MHz
	No swith time now
	No relationship with the input frequency
=============================================================================*/
reg			FOUT_80_tmp;
reg	[3:0]	fout_80_clk_cnt;
real		fout_80_clk_start;
real		fout_80_clk_10times;

initial begin
	FOUT_80_tmp = 10;
	fout_80_clk_cnt = 0;
	fout_80_clk_start = 1;
	fout_80_clk_10times = 1;
end

always #6.369426751 FOUT_80_tmp = !FOUT_80_tmp;

// fout clock monitor
always @ (posedge FOUT_80)
	if (fout_80_clk_cnt == 4'd1) begin
	   fout_80_clk_start	= $realtime;
	   fout_80_clk_cnt		= fout_80_clk_cnt + 1;
	end
	else if (fout_80_clk_cnt == 4'd11) begin
	   fout_80_clk_10times	= $realtime - fout_80_clk_start;
	   fout_80_clk_cnt		= fout_80_clk_cnt + 1;
	   $display("FOUT_80 frequency is %fMHz at %m", 10000/fout_80_clk_10times);
	end
	else if (fout_80_clk_cnt != 4'hf) begin
	   fout_80_clk_cnt		= fout_80_clk_cnt + 1;
	end

/*=============================================================================
	output clock generator for 108MHz
	No swith time now
	No relationship with the input frequency
=============================================================================*/
reg			FOUT_108_tmp;
reg	[3:0]	fout_108_clk_cnt;
real		fout_108_clk_start;
real		fout_108_clk_10times;

initial begin
	FOUT_108_tmp = 10;
	fout_108_clk_cnt = 0;
	fout_108_clk_start = 1;
	fout_108_clk_10times = 1;
end

always #4.629629 FOUT_108_tmp = !FOUT_108_tmp;

// fout clock monitor
always @ (posedge FOUT_108)
	if (fout_108_clk_cnt == 4'd1) begin
	   fout_108_clk_start	= $realtime;
	   fout_108_clk_cnt		= fout_108_clk_cnt + 1;
	end
	else if (fout_108_clk_cnt == 4'd11) begin
	   fout_108_clk_10times	= $realtime - fout_108_clk_start;
	   fout_108_clk_cnt		= fout_108_clk_cnt + 1;
	   $display("FOUT_108 frequency is %fMHz at %m", 10000/fout_108_clk_10times);
	end
	else if (fout_108_clk_cnt != 4'hf) begin
	   fout_108_clk_cnt		= fout_108_clk_cnt + 1;
	end

	/*=========================================================================
		Output Part
	=========================================================================*/
	assign	FOUT_48 = FOUT_48_tmp;
	assign	FOUT_54 = FOUT_54_tmp;
	assign	FOUT_80 = FOUT_80_tmp;
	assign	FOUT_108= FOUT_108_tmp;

endmodule

