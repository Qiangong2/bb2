module ram4x32d (DOUT, DIN, RADDR, WADDR, WE, CLK,RST_);

input  [31:0] DIN;
input  [1:0]  RADDR, WADDR;
input         WE, CLK, RST_;
output [31:0] DOUT;

parameter	rd_dly = 1,
		wr_dly = 1;

reg	[31:0]	mem_0,
		mem_1,
		mem_2,
		mem_3;
		
assign		DOUT	= {32{RADDR == 2'b00}} & mem_0 |
			  {32{RADDR == 2'b01}} & mem_1 |
			  {32{RADDR == 2'b10}} & mem_2 |	
			  {32{RADDR == 2'b11}} & mem_3;
			  
always @(posedge CLK)
	if (WE == 1'b1 & WADDR == 2'b00)
		mem_0 <= #wr_dly DIN;
		
always @(posedge CLK)
	if (WE == 1'b1 & WADDR == 2'b01)
		mem_1 <= #wr_dly DIN;
		
always @(posedge CLK)
	if (WE == 1'b1 & WADDR == 2'b10)
		mem_2 <= #wr_dly DIN;
		
always @(posedge CLK)
	if (WE == 1'b1 & WADDR == 2'b11)
		mem_3 <= #wr_dly DIN;

// synopsys translate_off
initial begin
	mem_0 = 0;
	mem_1 = 0;
	mem_2 = 0;
	mem_3 = 0;
end
// synopsys translate_on

endmodule
