//`include "defines.h"
module RF_ARRAY(
		// output ports
		array_qra	,
		array_qrb	,
		array_qrc	,
		array_qrd	,

    	array_stra  ,
    	array_strb  ,
    	array_strc  ,
    	array_strd  ,
    	array_stca  ,
    	array_stcb  ,


		test_so0	,
		test_so1    ,

		// input ports
		array_ara	,
		array_cenra	,
		array_arb	,
		array_cenrb	,
		array_arc 	,
		array_cenrc	,
		array_ard	,
		array_cenrd	,
		array_awa	,
		array_cenwa ,
		array_dwa	,
		array_awb	,
		array_cenwb ,
		array_dwb	,
		array_awc	,
		array_cenwc ,
		array_dwc	,
		array_awd	,
		array_cenwd ,
		array_dwd	,

		array_censtwcal ,
		array_censtwcbl ,
		array_censtwcah ,
		array_censtwcbh ,
		array_censtwa  ,
		array_censtwb  ,
		array_censtwc  ,
		array_censtwd  ,

        array_dwstca_2   ,
        array_dwstcb_2   ,
        array_dwstca_3   ,
        array_dwstcb_3   ,

        array_cenca ,
        array_aca   ,
        array_cencb ,
        array_acb   ,
		scan_clk	,
		test_si0    ,
		test_si1    ,
    	st_rst
		);

	output	[`RFD_WIDTH-1:0]	array_qra;
	output	[`RFD_WIDTH-1:0]	array_qrb;
	output	[`RFD_WIDTH-1:0]	array_qrc;
	output	[`RFD_WIDTH-1:0]	array_qrd;
    output  [4:0]   			array_stra;
    output  [4:0]   			array_strb;
    output  [4:0]   			array_strc;
    output  [4:0]   			array_strd;
    output  [4:0]   			array_stca;
    output  [4:0]   			array_stcb;


	output						test_so0;
    output                      test_so1;

	input						array_censtwcal ;
	input						array_censtwcbl ;
	input                       array_censtwcah ;
	input                       array_censtwcbh ;
	input						array_censtwa   ;
	input						array_censtwb   ;
	input						array_censtwc   ;
	input						array_censtwd   ;

	input	                    array_dwstca_2  ;
	input	                    array_dwstcb_2  ;
	input                       array_dwstca_3  ;
	input                       array_dwstcb_3  ;

	input	[`RFAD_WIDTH-1:0]	array_ara;
	input						array_cenra;
	input	[`RFAD_WIDTH-1:0]	array_arb;
	input						array_cenrb;
	input	[`RFAD_WIDTH-1:0]	array_arc;
	input						array_cenrc;
	input	[`RFAD_WIDTH-1:0]	array_ard;
	input						array_cenrd;
	input	[`RFAD_WIDTH-1:0]	array_awa;
	input						array_cenwa;
	input	[`RFD_WIDTH-1:0]	array_dwa;
	input	[`RFAD_WIDTH-1:0]	array_awb;
	input						array_cenwb;
	input	[`RFD_WIDTH-1:0]	array_dwb;
	input	[`RFAD_WIDTH-1:0]	array_awc;
	input						array_cenwc;
	input	[`RFD_WIDTH-1:0]	array_dwc;
	input	[`RFAD_WIDTH-1:0]	array_awd;
	input						array_cenwd;
	input	[`RFD_WIDTH-1:0]	array_dwd;
    input           			array_cenca;
    input   [`RFAD_WIDTH-1:0] 	array_aca;
    input           			array_cencb;
    input   [`RFAD_WIDTH-1:0] 	array_acb;
	input						scan_clk;
	input						test_si0;
	input                       test_si1;
  	input           			st_rst ;

	parameter		U_DLY = 0.2;

	reg	    [`RFD_WIDTH-1:0]	rf_array [`RFAD_WIDTH-1:0];
//	wire	[`RFD_WIDTH-1:0]	rf_arrayl = rf_array[`RFAD_WIDTH-1];


	// read operation, when cen is invalid, the output should hold as
	// the before's value

	reg	  [`RFD_WIDTH-1:0]	 array_qra;
	reg	  [`RFD_WIDTH-1:0]	 array_qrb;
	reg	  [`RFD_WIDTH-1:0]	 array_qrc;
	reg	  [`RFD_WIDTH-1:0]	 array_qrd;
    // for state information begin
    reg   [`RFST_WIDTH-1:0]  rf_arraystl [`RFAD_WIDTH-1:0];
    reg   [1:0]              rf_arraysth [`RFAD_WIDTH-1:0];
    reg   [4:0]  			 array_stra;
    reg   [4:0]  			 array_strb;
    reg   [4:0]  			 array_strc;
    reg   [4:0]  			 array_strd;
    reg   [4:0]  			 array_stca;
    reg   [4:0]  			 array_stcb;

    wire[2:0]   rf_arraystl0 = rf_arraystl[0];
    wire[31:0]  rf_array16   = rf_array[16];
	assign	    test_so0	 = rf_arraystl0[0];

assign      test_so1     = rf_array16[0];

    wire  [4:0]  		rf_arraystra =
					   				{5{array_ara[0]}}  & {rf_arraysth[0], rf_arraystl[0]}			|
                       				{5{array_ara[1]}}  & {rf_arraysth[1], rf_arraystl[1]}			|
                       				{5{array_ara[2]}}  & {rf_arraysth[2], rf_arraystl[2]}			|
                       				{5{array_ara[3]}}  & {rf_arraysth[3], rf_arraystl[3]}			|
                       				{5{array_ara[4]}}  & {rf_arraysth[4], rf_arraystl[4]}			|
                       				{5{array_ara[5]}}  & {rf_arraysth[5], rf_arraystl[5]}			|
                       				{5{array_ara[6]}}  & {rf_arraysth[6], rf_arraystl[6]}			|
                       				{5{array_ara[7]}}  & {rf_arraysth[7], rf_arraystl[7]}			|
                       				{5{array_ara[8]}}  & {rf_arraysth[8], rf_arraystl[8]}			|
                       				{5{array_ara[9]}}  & {rf_arraysth[9], rf_arraystl[9]}			|
                       				{5{array_ara[10]}} & {rf_arraysth[10], rf_arraystl[10]}			|
                       				{5{array_ara[11]}} & {rf_arraysth[11], rf_arraystl[11]}			|
                       				{5{array_ara[12]}} & {rf_arraysth[12], rf_arraystl[12]}			|
                       				{5{array_ara[13]}} & {rf_arraysth[13], rf_arraystl[13]}			|
                       				{5{array_ara[14]}} & {rf_arraysth[14], rf_arraystl[14]}			|
                       				{5{array_ara[15]}} & {rf_arraysth[15], rf_arraystl[15]}			|
                       				{5{array_ara[16]}} & {rf_arraysth[16], rf_arraystl[16]}			|
                       				{5{array_ara[17]}} & {rf_arraysth[17], rf_arraystl[17]}			|
                       				{5{array_ara[18]}} & {rf_arraysth[18], rf_arraystl[18]}			|
                       				{5{array_ara[19]}} & {rf_arraysth[19], rf_arraystl[19]}			|
                       				{5{array_ara[20]}} & {rf_arraysth[20], rf_arraystl[20]}			|
                       				{5{array_ara[21]}} & {rf_arraysth[21], rf_arraystl[21]}			|
                       				{5{array_ara[22]}} & {rf_arraysth[22], rf_arraystl[22]}			|
                       				{5{array_ara[23]}} & {rf_arraysth[23], rf_arraystl[23]}			|
                       				{5{array_ara[24]}} & {rf_arraysth[24], rf_arraystl[24]}			|
                       				{5{array_ara[25]}} & {rf_arraysth[25], rf_arraystl[25]}			|
                       				{5{array_ara[26]}} & {rf_arraysth[26], rf_arraystl[26]}			|
                       				{5{array_ara[27]}} & {rf_arraysth[27], rf_arraystl[27]}			|
                       				{5{array_ara[28]}} & {rf_arraysth[28], rf_arraystl[28]}			|
                       				{5{array_ara[29]}} & {rf_arraysth[29], rf_arraystl[29]}			|
                       				{5{array_ara[30]}} & {rf_arraysth[30], rf_arraystl[30]}			|
                       				{5{array_ara[31]}} & {rf_arraysth[31], rf_arraystl[31]}	 ;

    wire  [4:0] 		rf_arraystrb =
					   				{5{array_arb[0]}}  & {rf_arraysth[0], rf_arraystl[0]}			|
                       				{5{array_arb[1]}}  & {rf_arraysth[1], rf_arraystl[1]}			|
                       				{5{array_arb[2]}}  & {rf_arraysth[2], rf_arraystl[2]}			|
                       				{5{array_arb[3]}}  & {rf_arraysth[3], rf_arraystl[3]}			|
                       				{5{array_arb[4]}}  & {rf_arraysth[4], rf_arraystl[4]}			|
                       				{5{array_arb[5]}}  & {rf_arraysth[5], rf_arraystl[5]}			|
                       				{5{array_arb[6]}}  & {rf_arraysth[6], rf_arraystl[6]}			|
                       				{5{array_arb[7]}}  & {rf_arraysth[7], rf_arraystl[7]}			|
                       				{5{array_arb[8]}}  & {rf_arraysth[8], rf_arraystl[8]}			|
                       				{5{array_arb[9]}}  & {rf_arraysth[9], rf_arraystl[9]}			|
                       				{5{array_arb[10]}} & {rf_arraysth[10], rf_arraystl[10]}  		|
                       				{5{array_arb[11]}} & {rf_arraysth[11], rf_arraystl[11]}  		|
                       				{5{array_arb[12]}} & {rf_arraysth[12], rf_arraystl[12]}  		|
                       				{5{array_arb[13]}} & {rf_arraysth[13], rf_arraystl[13]}  		|
                       				{5{array_arb[14]}} & {rf_arraysth[14], rf_arraystl[14]}  		|
                       				{5{array_arb[15]}} & {rf_arraysth[15], rf_arraystl[15]}  		|
                       				{5{array_arb[16]}} & {rf_arraysth[16], rf_arraystl[16]}  		|
                       				{5{array_arb[17]}} & {rf_arraysth[17], rf_arraystl[17]}  		|
                       				{5{array_arb[18]}} & {rf_arraysth[18], rf_arraystl[18]}  		|
                       				{5{array_arb[19]}} & {rf_arraysth[19], rf_arraystl[19]}  		|
                       				{5{array_arb[20]}} & {rf_arraysth[20], rf_arraystl[20]}  		|
                       				{5{array_arb[21]}} & {rf_arraysth[21], rf_arraystl[21]}  		|
                       				{5{array_arb[22]}} & {rf_arraysth[22], rf_arraystl[22]}  		|
                       				{5{array_arb[23]}} & {rf_arraysth[23], rf_arraystl[23]}  		|
                       				{5{array_arb[24]}} & {rf_arraysth[24], rf_arraystl[24]}  		|
                       				{5{array_arb[25]}} & {rf_arraysth[25], rf_arraystl[25]}  		|
                       				{5{array_arb[26]}} & {rf_arraysth[26], rf_arraystl[26]}  		|
                       				{5{array_arb[27]}} & {rf_arraysth[27], rf_arraystl[27]}  		|
                       				{5{array_arb[28]}} & {rf_arraysth[28], rf_arraystl[28]}  		|
                       				{5{array_arb[29]}} & {rf_arraysth[29], rf_arraystl[29]}  		|
                       				{5{array_arb[30]}} & {rf_arraysth[30], rf_arraystl[30]}  		|
                       				{5{array_arb[31]}} & {rf_arraysth[31], rf_arraystl[31]}  	 ;

        wire  [4:0] 		rf_arraystrc =
					   				{5{array_arc[0]}}  & {rf_arraysth[0], rf_arraystl[0]}			|
                       				{5{array_arc[1]}}  & {rf_arraysth[1], rf_arraystl[1]}			|
                       				{5{array_arc[2]}}  & {rf_arraysth[2], rf_arraystl[2]}			|
                       				{5{array_arc[3]}}  & {rf_arraysth[3], rf_arraystl[3]}			|
                       				{5{array_arc[4]}}  & {rf_arraysth[4], rf_arraystl[4]}			|
                       				{5{array_arc[5]}}  & {rf_arraysth[5], rf_arraystl[5]}			|
                       				{5{array_arc[6]}}  & {rf_arraysth[6], rf_arraystl[6]}			|
                       				{5{array_arc[7]}}  & {rf_arraysth[7], rf_arraystl[7]}			|
                       				{5{array_arc[8]}}  & {rf_arraysth[8], rf_arraystl[8]}			|
                       				{5{array_arc[9]}}  & {rf_arraysth[9], rf_arraystl[9]}			|
                       				{5{array_arc[10]}} & {rf_arraysth[10], rf_arraystl[10]}  		|
                       				{5{array_arc[11]}} & {rf_arraysth[11], rf_arraystl[11]}  		|
                       				{5{array_arc[12]}} & {rf_arraysth[12], rf_arraystl[12]}  		|
                       				{5{array_arc[13]}} & {rf_arraysth[13], rf_arraystl[13]}  		|
                       				{5{array_arc[14]}} & {rf_arraysth[14], rf_arraystl[14]}  		|
                       				{5{array_arc[15]}} & {rf_arraysth[15], rf_arraystl[15]}  		|
                       				{5{array_arc[16]}} & {rf_arraysth[16], rf_arraystl[16]}  		|
                       				{5{array_arc[17]}} & {rf_arraysth[17], rf_arraystl[17]}  		|
                       				{5{array_arc[18]}} & {rf_arraysth[18], rf_arraystl[18]}  		|
                       				{5{array_arc[19]}} & {rf_arraysth[19], rf_arraystl[19]}  		|
                       				{5{array_arc[20]}} & {rf_arraysth[20], rf_arraystl[20]}  		|
                       				{5{array_arc[21]}} & {rf_arraysth[21], rf_arraystl[21]}  		|
                       				{5{array_arc[22]}} & {rf_arraysth[22], rf_arraystl[22]}  		|
                       				{5{array_arc[23]}} & {rf_arraysth[23], rf_arraystl[23]}  		|
                       				{5{array_arc[24]}} & {rf_arraysth[24], rf_arraystl[24]}  		|
                       				{5{array_arc[25]}} & {rf_arraysth[25], rf_arraystl[25]}  		|
                       				{5{array_arc[26]}} & {rf_arraysth[26], rf_arraystl[26]}  		|
                       				{5{array_arc[27]}} & {rf_arraysth[27], rf_arraystl[27]}  		|
                       				{5{array_arc[28]}} & {rf_arraysth[28], rf_arraystl[28]}  		|
                       				{5{array_arc[29]}} & {rf_arraysth[29], rf_arraystl[29]}  		|
                       				{5{array_arc[30]}} & {rf_arraysth[30], rf_arraystl[30]}  		|
                       				{5{array_arc[31]}} & {rf_arraysth[31], rf_arraystl[31]}  	 ;

    wire  [4:0] 		rf_arraystrd =
					   				{5{array_ard[0]}}  & {rf_arraysth[0], rf_arraystl[0]}			|
                       				{5{array_ard[1]}}  & {rf_arraysth[1], rf_arraystl[1]}			|
                       				{5{array_ard[2]}}  & {rf_arraysth[2], rf_arraystl[2]}			|
                       				{5{array_ard[3]}}  & {rf_arraysth[3], rf_arraystl[3]}			|
                       				{5{array_ard[4]}}  & {rf_arraysth[4], rf_arraystl[4]}			|
                       				{5{array_ard[5]}}  & {rf_arraysth[5], rf_arraystl[5]}			|
                       				{5{array_ard[6]}}  & {rf_arraysth[6], rf_arraystl[6]}			|
                       				{5{array_ard[7]}}  & {rf_arraysth[7], rf_arraystl[7]}			|
                       				{5{array_ard[8]}}  & {rf_arraysth[8], rf_arraystl[8]}			|
                       				{5{array_ard[9]}}  & {rf_arraysth[9], rf_arraystl[9]}			|
                       				{5{array_ard[10]}} & {rf_arraysth[10], rf_arraystl[10]}  		|
                       				{5{array_ard[11]}} & {rf_arraysth[11], rf_arraystl[11]}  		|
                       				{5{array_ard[12]}} & {rf_arraysth[12], rf_arraystl[12]}  		|
                       				{5{array_ard[13]}} & {rf_arraysth[13], rf_arraystl[13]}  		|
                       				{5{array_ard[14]}} & {rf_arraysth[14], rf_arraystl[14]}  		|
                       				{5{array_ard[15]}} & {rf_arraysth[15], rf_arraystl[15]}  		|
                       				{5{array_ard[16]}} & {rf_arraysth[16], rf_arraystl[16]}  		|
                       				{5{array_ard[17]}} & {rf_arraysth[17], rf_arraystl[17]}  		|
                       				{5{array_ard[18]}} & {rf_arraysth[18], rf_arraystl[18]}  		|
                       				{5{array_ard[19]}} & {rf_arraysth[19], rf_arraystl[19]}  		|
                       				{5{array_ard[20]}} & {rf_arraysth[20], rf_arraystl[20]}  		|
                       				{5{array_ard[21]}} & {rf_arraysth[21], rf_arraystl[21]}  		|
                       				{5{array_ard[22]}} & {rf_arraysth[22], rf_arraystl[22]}  		|
                       				{5{array_ard[23]}} & {rf_arraysth[23], rf_arraystl[23]}  		|
                       				{5{array_ard[24]}} & {rf_arraysth[24], rf_arraystl[24]}  		|
                       				{5{array_ard[25]}} & {rf_arraysth[25], rf_arraystl[25]}  		|
                       				{5{array_ard[26]}} & {rf_arraysth[26], rf_arraystl[26]}  		|
                       				{5{array_ard[27]}} & {rf_arraysth[27], rf_arraystl[27]}  		|
                       				{5{array_ard[28]}} & {rf_arraysth[28], rf_arraystl[28]}  		|
                       				{5{array_ard[29]}} & {rf_arraysth[29], rf_arraystl[29]}  		|
                       				{5{array_ard[30]}} & {rf_arraysth[30], rf_arraystl[30]}  		|
                       				{5{array_ard[31]}} & {rf_arraysth[31], rf_arraystl[31]}  	 ;

    wire  [4:0] 		rf_arraystca =
					   				{5{array_aca[0]}}  & {rf_arraysth[0], rf_arraystl[0]}			|
                       				{5{array_aca[1]}}  & {rf_arraysth[1], rf_arraystl[1]}			|
                       				{5{array_aca[2]}}  & {rf_arraysth[2], rf_arraystl[2]}			|
                       				{5{array_aca[3]}}  & {rf_arraysth[3], rf_arraystl[3]}			|
                       				{5{array_aca[4]}}  & {rf_arraysth[4], rf_arraystl[4]}			|
                       				{5{array_aca[5]}}  & {rf_arraysth[5], rf_arraystl[5]}			|
                       				{5{array_aca[6]}}  & {rf_arraysth[6], rf_arraystl[6]}			|
                       				{5{array_aca[7]}}  & {rf_arraysth[7], rf_arraystl[7]}			|
                       				{5{array_aca[8]}}  & {rf_arraysth[8], rf_arraystl[8]}			|
                       				{5{array_aca[9]}}  & {rf_arraysth[9], rf_arraystl[9]}			|
                       				{5{array_aca[10]}} & {rf_arraysth[10], rf_arraystl[10]}  		|
                       				{5{array_aca[11]}} & {rf_arraysth[11], rf_arraystl[11]}  		|
                       				{5{array_aca[12]}} & {rf_arraysth[12], rf_arraystl[12]}  		|
                       				{5{array_aca[13]}} & {rf_arraysth[13], rf_arraystl[13]}  		|
                       				{5{array_aca[14]}} & {rf_arraysth[14], rf_arraystl[14]}  		|
                       				{5{array_aca[15]}} & {rf_arraysth[15], rf_arraystl[15]}  		|
                       				{5{array_aca[16]}} & {rf_arraysth[16], rf_arraystl[16]}  		|
                       				{5{array_aca[17]}} & {rf_arraysth[17], rf_arraystl[17]}  		|
                       				{5{array_aca[18]}} & {rf_arraysth[18], rf_arraystl[18]}  		|
                       				{5{array_aca[19]}} & {rf_arraysth[19], rf_arraystl[19]}  		|
                       				{5{array_aca[20]}} & {rf_arraysth[20], rf_arraystl[20]}  		|
                       				{5{array_aca[21]}} & {rf_arraysth[21], rf_arraystl[21]}  		|
                       				{5{array_aca[22]}} & {rf_arraysth[22], rf_arraystl[22]}  		|
                       				{5{array_aca[23]}} & {rf_arraysth[23], rf_arraystl[23]}  		|
                       				{5{array_aca[24]}} & {rf_arraysth[24], rf_arraystl[24]}  		|
                       				{5{array_aca[25]}} & {rf_arraysth[25], rf_arraystl[25]}  		|
                       				{5{array_aca[26]}} & {rf_arraysth[26], rf_arraystl[26]}  		|
                       				{5{array_aca[27]}} & {rf_arraysth[27], rf_arraystl[27]}  		|
                       				{5{array_aca[28]}} & {rf_arraysth[28], rf_arraystl[28]}  		|
                       				{5{array_aca[29]}} & {rf_arraysth[29], rf_arraystl[29]}  		|
                       				{5{array_aca[30]}} & {rf_arraysth[30], rf_arraystl[30]}  		|
                       				{5{array_aca[31]}} & {rf_arraysth[31], rf_arraystl[31]}  	 ;

    wire  [4:0] 		rf_arraystcb =
					   				{5{array_acb[0]}}  & {rf_arraysth[0], rf_arraystl[0]}			|
                       				{5{array_acb[1]}}  & {rf_arraysth[1], rf_arraystl[1]}			|
                       				{5{array_acb[2]}}  & {rf_arraysth[2], rf_arraystl[2]}			|
                       				{5{array_acb[3]}}  & {rf_arraysth[3], rf_arraystl[3]}			|
                       				{5{array_acb[4]}}  & {rf_arraysth[4], rf_arraystl[4]}			|
                       				{5{array_acb[5]}}  & {rf_arraysth[5], rf_arraystl[5]}			|
                       				{5{array_acb[6]}}  & {rf_arraysth[6], rf_arraystl[6]}			|
                       				{5{array_acb[7]}}  & {rf_arraysth[7], rf_arraystl[7]}			|
                       				{5{array_acb[8]}}  & {rf_arraysth[8], rf_arraystl[8]}			|
                       				{5{array_acb[9]}}  & {rf_arraysth[9], rf_arraystl[9]}			|
                       				{5{array_acb[10]}} & {rf_arraysth[10], rf_arraystl[10]}  		|
                       				{5{array_acb[11]}} & {rf_arraysth[11], rf_arraystl[11]}  		|
                       				{5{array_acb[12]}} & {rf_arraysth[12], rf_arraystl[12]}  		|
                       				{5{array_acb[13]}} & {rf_arraysth[13], rf_arraystl[13]}  		|
                       				{5{array_acb[14]}} & {rf_arraysth[14], rf_arraystl[14]}  		|
                       				{5{array_acb[15]}} & {rf_arraysth[15], rf_arraystl[15]}  		|
                       				{5{array_acb[16]}} & {rf_arraysth[16], rf_arraystl[16]}  		|
                       				{5{array_acb[17]}} & {rf_arraysth[17], rf_arraystl[17]}  		|
                       				{5{array_acb[18]}} & {rf_arraysth[18], rf_arraystl[18]}  		|
                       				{5{array_acb[19]}} & {rf_arraysth[19], rf_arraystl[19]}  		|
                       				{5{array_acb[20]}} & {rf_arraysth[20], rf_arraystl[20]}  		|
                       				{5{array_acb[21]}} & {rf_arraysth[21], rf_arraystl[21]}  		|
                       				{5{array_acb[22]}} & {rf_arraysth[22], rf_arraystl[22]}  		|
                       				{5{array_acb[23]}} & {rf_arraysth[23], rf_arraystl[23]}  		|
                       				{5{array_acb[24]}} & {rf_arraysth[24], rf_arraystl[24]}  		|
                       				{5{array_acb[25]}} & {rf_arraysth[25], rf_arraystl[25]}  		|
                       				{5{array_acb[26]}} & {rf_arraysth[26], rf_arraystl[26]}  		|
                       				{5{array_acb[27]}} & {rf_arraysth[27], rf_arraystl[27]}  		|
                       				{5{array_acb[28]}} & {rf_arraysth[28], rf_arraystl[28]}  		|
                       				{5{array_acb[29]}} & {rf_arraysth[29], rf_arraystl[29]}  		|
                       				{5{array_acb[30]}} & {rf_arraysth[30], rf_arraystl[30]}  		|
                       				{5{array_acb[31]}} & {rf_arraysth[31], rf_arraystl[31]}  	 ;
    // for state information end

	wire  [`RFD_WIDTH-1:0]   rf_arrayra =
					   				{32{array_ara[0]}}  & rf_array[0]			|
                       				{32{array_ara[1]}}  & rf_array[1]			|
                       				{32{array_ara[2]}}  & rf_array[2]			|
                       				{32{array_ara[3]}}  & rf_array[3]			|
                       				{32{array_ara[4]}}  & rf_array[4]			|
                       				{32{array_ara[5]}}  & rf_array[5]			|
                       				{32{array_ara[6]}}  & rf_array[6]			|
                       				{32{array_ara[7]}}  & rf_array[7]			|
                       				{32{array_ara[8]}}  & rf_array[8]			|
                       				{32{array_ara[9]}}  & rf_array[9]			|
                       				{32{array_ara[10]}} & rf_array[10]			|
                       				{32{array_ara[11]}} & rf_array[11]			|
                       				{32{array_ara[12]}} & rf_array[12]			|
                       				{32{array_ara[13]}} & rf_array[13]			|
                       				{32{array_ara[14]}} & rf_array[14]			|
                       				{32{array_ara[15]}} & rf_array[15]			|
                       				{32{array_ara[16]}} & rf_array[16]			|
                       				{32{array_ara[17]}} & rf_array[17]			|
                       				{32{array_ara[18]}} & rf_array[18]			|
                       				{32{array_ara[19]}} & rf_array[19]			|
                       				{32{array_ara[20]}} & rf_array[20]			|
                       				{32{array_ara[21]}} & rf_array[21]			|
                       				{32{array_ara[22]}} & rf_array[22]			|
                       				{32{array_ara[23]}} & rf_array[23]			|
                       				{32{array_ara[24]}} & rf_array[24]			|
                       				{32{array_ara[25]}} & rf_array[25]			|
                       				{32{array_ara[26]}} & rf_array[26]			|
                       				{32{array_ara[27]}} & rf_array[27]			|
                       				{32{array_ara[28]}} & rf_array[28]			|
                       				{32{array_ara[29]}} & rf_array[29]			|
                       				{32{array_ara[30]}} & rf_array[30]			|
                       				{32{array_ara[31]}} & rf_array[31]	 ;

	wire  [`RFD_WIDTH-1:0] rf_arrayrb =
					   				{32{array_arb[0]}}  & rf_array[0]			|
                       				{32{array_arb[1]}}  & rf_array[1]			|
                       				{32{array_arb[2]}}  & rf_array[2]			|
                       				{32{array_arb[3]}}  & rf_array[3]			|
                       				{32{array_arb[4]}}  & rf_array[4]			|
                       				{32{array_arb[5]}}  & rf_array[5]			|
                       				{32{array_arb[6]}}  & rf_array[6]			|
                       				{32{array_arb[7]}}  & rf_array[7]			|
                       				{32{array_arb[8]}}  & rf_array[8]			|
                       				{32{array_arb[9]}}  & rf_array[9]			|
                       				{32{array_arb[10]}} & rf_array[10]			|
                       				{32{array_arb[11]}} & rf_array[11]			|
                       				{32{array_arb[12]}} & rf_array[12]			|
                       				{32{array_arb[13]}} & rf_array[13]			|
                       				{32{array_arb[14]}} & rf_array[14]			|
                       				{32{array_arb[15]}} & rf_array[15]			|
                       				{32{array_arb[16]}} & rf_array[16]			|
                       				{32{array_arb[17]}} & rf_array[17]			|
                       				{32{array_arb[18]}} & rf_array[18]			|
                       				{32{array_arb[19]}} & rf_array[19]			|
                       				{32{array_arb[20]}} & rf_array[20]			|
                       				{32{array_arb[21]}} & rf_array[21]			|
                       				{32{array_arb[22]}} & rf_array[22]			|
                       				{32{array_arb[23]}} & rf_array[23]			|
                       				{32{array_arb[24]}} & rf_array[24]			|
                       				{32{array_arb[25]}} & rf_array[25]			|
                       				{32{array_arb[26]}} & rf_array[26]			|
                       				{32{array_arb[27]}} & rf_array[27]			|
                       				{32{array_arb[28]}} & rf_array[28]			|
                       				{32{array_arb[29]}} & rf_array[29]			|
                       				{32{array_arb[30]}} & rf_array[30]			|
                       				{32{array_arb[31]}} & rf_array[31]	 ;

	wire  [`RFD_WIDTH-1:0] rf_arrayrc =
					   				{32{array_arc[0]}}  & rf_array[0]			|
                       				{32{array_arc[1]}}  & rf_array[1]			|
                       				{32{array_arc[2]}}  & rf_array[2]			|
                       				{32{array_arc[3]}}  & rf_array[3]			|
                       				{32{array_arc[4]}}  & rf_array[4]			|
                       				{32{array_arc[5]}}  & rf_array[5]			|
                       				{32{array_arc[6]}}  & rf_array[6]			|
                       				{32{array_arc[7]}}  & rf_array[7]			|
                       				{32{array_arc[8]}}  & rf_array[8]			|
                       				{32{array_arc[9]}}  & rf_array[9]			|
                       				{32{array_arc[10]}} & rf_array[10]			|
                       				{32{array_arc[11]}} & rf_array[11]			|
                       				{32{array_arc[12]}} & rf_array[12]			|
                       				{32{array_arc[13]}} & rf_array[13]			|
                       				{32{array_arc[14]}} & rf_array[14]			|
                       				{32{array_arc[15]}} & rf_array[15]			|
                       				{32{array_arc[16]}} & rf_array[16]			|
                       				{32{array_arc[17]}} & rf_array[17]			|
                       				{32{array_arc[18]}} & rf_array[18]			|
                       				{32{array_arc[19]}} & rf_array[19]			|
                       				{32{array_arc[20]}} & rf_array[20]			|
                       				{32{array_arc[21]}} & rf_array[21]			|
                       				{32{array_arc[22]}} & rf_array[22]			|
                       				{32{array_arc[23]}} & rf_array[23]			|
                       				{32{array_arc[24]}} & rf_array[24]			|
                       				{32{array_arc[25]}} & rf_array[25]			|
                       				{32{array_arc[26]}} & rf_array[26]			|
                       				{32{array_arc[27]}} & rf_array[27]			|
                       				{32{array_arc[28]}} & rf_array[28]			|
                       				{32{array_arc[29]}} & rf_array[29]			|
                       				{32{array_arc[30]}} & rf_array[30]			|
                       				{32{array_arc[31]}} & rf_array[31]	 ;

wire  [`RFD_WIDTH-1:0] rf_arrayrd =
					   				{32{array_ard[0]}}  & rf_array[0]			|
                       				{32{array_ard[1]}}  & rf_array[1]			|
                       				{32{array_ard[2]}}  & rf_array[2]			|
                       				{32{array_ard[3]}}  & rf_array[3]			|
                       				{32{array_ard[4]}}  & rf_array[4]			|
                       				{32{array_ard[5]}}  & rf_array[5]			|
                       				{32{array_ard[6]}}  & rf_array[6]			|
                       				{32{array_ard[7]}}  & rf_array[7]			|
                       				{32{array_ard[8]}}  & rf_array[8]			|
                       				{32{array_ard[9]}}  & rf_array[9]			|
                       				{32{array_ard[10]}} & rf_array[10]			|
                       				{32{array_ard[11]}} & rf_array[11]			|
                       				{32{array_ard[12]}} & rf_array[12]			|
                       				{32{array_ard[13]}} & rf_array[13]			|
                       				{32{array_ard[14]}} & rf_array[14]			|
                       				{32{array_ard[15]}} & rf_array[15]			|
                       				{32{array_ard[16]}} & rf_array[16]			|
                       				{32{array_ard[17]}} & rf_array[17]			|
                       				{32{array_ard[18]}} & rf_array[18]			|
                       				{32{array_ard[19]}} & rf_array[19]			|
                       				{32{array_ard[20]}} & rf_array[20]			|
                       				{32{array_ard[21]}} & rf_array[21]			|
                       				{32{array_ard[22]}} & rf_array[22]			|
                       				{32{array_ard[23]}} & rf_array[23]			|
                       				{32{array_ard[24]}} & rf_array[24]			|
                       				{32{array_ard[25]}} & rf_array[25]			|
                       				{32{array_ard[26]}} & rf_array[26]			|
                       				{32{array_ard[27]}} & rf_array[27]			|
                       				{32{array_ard[28]}} & rf_array[28]			|
                       				{32{array_ard[29]}} & rf_array[29]			|
                       				{32{array_ard[30]}} & rf_array[30]			|
                       				{32{array_ard[31]}} & rf_array[31]	 ;

	always @(array_cenra or rf_arrayra or rf_arraystra)
	begin
		if (array_cenra)
		begin
			array_qra  <= #U_DLY rf_arrayra;
      		array_stra <= #U_DLY rf_arraystra;
    	end
	end

	always @(array_cenrb or rf_arrayrb or rf_arraystrb)
	begin
		if (array_cenrb)
		begin
			array_qrb  <= #U_DLY rf_arrayrb;
      		array_strb <= #U_DLY rf_arraystrb;
    	end
	end

	always @(array_cenrc or rf_arrayrc or rf_arraystrc)
	begin
		if (array_cenrc)
		begin
			array_qrc  <= #U_DLY rf_arrayrc;
      		array_strc <= #U_DLY rf_arraystrc;
    	end
	end

	always @(array_cenrd or rf_arrayrd or rf_arraystrd)
	begin
		if (array_cenrd)
		begin
			array_qrd  <= #U_DLY rf_arrayrd;
      		array_strd <= #U_DLY rf_arraystrd;
    	end
	end

    always @(array_cenca or rf_arraystca)
    begin
      	if (array_cenca)
        	array_stca <= #U_DLY rf_arraystca;
    end

    always @(array_cencb or rf_arraystcb)
    begin
      	if (array_cencb)
        	array_stcb <= #U_DLY rf_arraystcb;
    end

	// write operation
	// write port a
	always @(posedge array_cenwa)
	begin
	    case(1'b1)
	    array_awa[0]  :
			rf_array[0] <= #U_DLY  array_dwa;
	    array_awa[1]  :
			rf_array[1] <= #U_DLY  array_dwa;
	    array_awa[2]  :
			rf_array[2] <= #U_DLY  array_dwa;
		array_awa[3]  :
			rf_array[3] <= #U_DLY  array_dwa;
	    array_awa[4]  :
			rf_array[4] <= #U_DLY  array_dwa;
	    array_awa[5]  :
			rf_array[5] <= #U_DLY  array_dwa;
		array_awa[6]  :
			rf_array[6] <= #U_DLY  array_dwa;
	    array_awa[7]  :
			rf_array[7] <= #U_DLY  array_dwa;
	    array_awa[8]  :
			rf_array[8] <= #U_DLY  array_dwa;
		array_awa[9]  :
			rf_array[9] <= #U_DLY  array_dwa;
		array_awa[10]  :
			rf_array[10] <= #U_DLY array_dwa;
	    array_awa[11]  :
			rf_array[11] <= #U_DLY array_dwa;
	    array_awa[12]  :
			rf_array[12] <= #U_DLY array_dwa;
		array_awa[13]  :
			rf_array[13] <= #U_DLY array_dwa;
	    array_awa[14]  :
			rf_array[14] <= #U_DLY array_dwa;
	    array_awa[15]  :
			rf_array[15] <= #U_DLY array_dwa;
		array_awa[16]  :
			rf_array[16] <= #U_DLY array_dwa;
	    array_awa[17]  :
			rf_array[17] <= #U_DLY array_dwa;
	    array_awa[18]  :
			rf_array[18] <= #U_DLY array_dwa;
		array_awa[19]  :
			rf_array[19] <= #U_DLY array_dwa;
	    array_awa[20]  :
			rf_array[20] <= #U_DLY array_dwa;
	    array_awa[21]  :
			rf_array[21] <= #U_DLY array_dwa;
	    array_awa[22]  :
			rf_array[22] <= #U_DLY array_dwa;
		array_awa[23]  :
			rf_array[23] <= #U_DLY array_dwa;
	    array_awa[24]  :
			rf_array[24] <= #U_DLY array_dwa;
	    array_awa[25]  :
			rf_array[25] <= #U_DLY array_dwa;
		array_awa[26]  :
			rf_array[26] <= #U_DLY array_dwa;
	    array_awa[27]  :
			rf_array[27] <= #U_DLY array_dwa;
	    array_awa[28]  :
			rf_array[28] <= #U_DLY array_dwa;
		array_awa[29]  :
			rf_array[29] <= #U_DLY array_dwa;
		array_awa[30]  :
			rf_array[30] <= #U_DLY array_dwa;
	    array_awa[31]  :
			rf_array[31] <= #U_DLY array_dwa;
		default        :
		    rf_array[0]  <= #U_DLY 32'h0000 ;
		endcase
	end

	// write port b
	always @(posedge array_cenwb)
	begin
		case(1'b1)
	    array_awb[0]  :
			rf_array[0] <= #U_DLY  array_dwb;
	    array_awb[1]  :
			rf_array[1] <= #U_DLY  array_dwb;
	    array_awb[2]  :
			rf_array[2] <= #U_DLY  array_dwb;
		array_awb[3]  :
			rf_array[3] <= #U_DLY  array_dwb;
	    array_awb[4]  :
			rf_array[4] <= #U_DLY  array_dwb;
	    array_awb[5]  :
			rf_array[5] <= #U_DLY  array_dwb;
		array_awb[6]  :
			rf_array[6] <= #U_DLY  array_dwb;
	    array_awb[7]  :
			rf_array[7] <= #U_DLY  array_dwb;
	    array_awb[8]  :
			rf_array[8] <= #U_DLY  array_dwb;
		array_awb[9]  :
			rf_array[9] <= #U_DLY  array_dwb;
		array_awb[10]  :
			rf_array[10] <= #U_DLY array_dwb;
	    array_awb[11]  :
			rf_array[11] <= #U_DLY array_dwb;
	    array_awb[12]  :
			rf_array[12] <= #U_DLY array_dwb;
		array_awb[13]  :
			rf_array[13] <= #U_DLY array_dwb;
	    array_awb[14]  :
			rf_array[14] <= #U_DLY array_dwb;
	    array_awb[15]  :
			rf_array[15] <= #U_DLY array_dwb;
		array_awb[16]  :
			rf_array[16] <= #U_DLY array_dwb;
	    array_awb[17]  :
			rf_array[17] <= #U_DLY array_dwb;
	    array_awb[18]  :
			rf_array[18] <= #U_DLY array_dwb;
		array_awb[19]  :
			rf_array[19] <= #U_DLY array_dwb;
	    array_awb[20]  :
			rf_array[20] <= #U_DLY array_dwb;
	    array_awb[21]  :
			rf_array[21] <= #U_DLY array_dwb;
	    array_awb[22]  :
			rf_array[22] <= #U_DLY array_dwb;
		array_awb[23]  :
			rf_array[23] <= #U_DLY array_dwb;
	    array_awb[24]  :
			rf_array[24] <= #U_DLY array_dwb;
	    array_awb[25]  :
			rf_array[25] <= #U_DLY array_dwb;
		array_awb[26]  :
			rf_array[26] <= #U_DLY array_dwb;
	    array_awb[27]  :
			rf_array[27] <= #U_DLY array_dwb;
	    array_awb[28]  :
			rf_array[28] <= #U_DLY array_dwb;
		array_awb[29]  :
			rf_array[29] <= #U_DLY array_dwb;
		array_awb[30]  :
			rf_array[30] <= #U_DLY array_dwb;
	    array_awb[31]  :
			rf_array[31] <= #U_DLY array_dwb;
		default   :
		    rf_array[0] <= #U_DLY 32'h0000;
		endcase
	end

	// write port c
	always @(posedge array_cenwc)
	begin
		case(1'b1)
	    array_awc[0]  :
			rf_array[0] <= #U_DLY  array_dwc;
	    array_awc[1]  :
			rf_array[1] <= #U_DLY  array_dwc;
	    array_awc[2]  :
			rf_array[2] <= #U_DLY  array_dwc;
		array_awc[3]  :
			rf_array[3] <= #U_DLY  array_dwc;
	    array_awc[4]  :
			rf_array[4] <= #U_DLY  array_dwc;
	    array_awc[5]  :
			rf_array[5] <= #U_DLY  array_dwc;
		array_awc[6]  :
			rf_array[6] <= #U_DLY  array_dwc;
	    array_awc[7]  :
			rf_array[7] <= #U_DLY  array_dwc;
	    array_awc[8]  :
			rf_array[8] <= #U_DLY  array_dwc;
		array_awc[9]  :
			rf_array[9] <= #U_DLY  array_dwc;
		array_awc[10]  :
			rf_array[10] <= #U_DLY array_dwc;
	    array_awc[11]  :
			rf_array[11] <= #U_DLY array_dwc;
	    array_awc[12]  :
			rf_array[12] <= #U_DLY array_dwc;
		array_awc[13]  :
			rf_array[13] <= #U_DLY array_dwc;
	    array_awc[14]  :
			rf_array[14] <= #U_DLY array_dwc;
	    array_awc[15]  :
			rf_array[15] <= #U_DLY array_dwc;
		array_awc[16]  :
			rf_array[16] <= #U_DLY array_dwc;
	    array_awc[17]  :
			rf_array[17] <= #U_DLY array_dwc;
	    array_awc[18]  :
			rf_array[18] <= #U_DLY array_dwc;
		array_awc[19]  :
			rf_array[19] <= #U_DLY array_dwc;
	    array_awc[20]  :
			rf_array[20] <= #U_DLY array_dwc;
	    array_awc[21]  :
			rf_array[21] <= #U_DLY array_dwc;
	    array_awc[22]  :
			rf_array[22] <= #U_DLY array_dwc;
		array_awc[23]  :
			rf_array[23] <= #U_DLY array_dwc;
	    array_awc[24]  :
			rf_array[24] <= #U_DLY array_dwc;
	    array_awc[25]  :
			rf_array[25] <= #U_DLY array_dwc;
		array_awc[26]  :
			rf_array[26] <= #U_DLY array_dwc;
	    array_awc[27]  :
			rf_array[27] <= #U_DLY array_dwc;
	    array_awc[28]  :
			rf_array[28] <= #U_DLY array_dwc;
		array_awc[29]  :
			rf_array[29] <= #U_DLY array_dwc;
		array_awc[30]  :
			rf_array[30] <= #U_DLY array_dwc;
	    array_awc[31]  :
			rf_array[31] <= #U_DLY array_dwc;
		default   :
		    rf_array[0] <= #U_DLY 32'h0000;
		endcase
	end

	// write port d
	always @(posedge array_cenwd)
	begin
		case(1'b1)
	    array_awd[0]  :
			rf_array[0] <= #U_DLY  array_dwd;
	    array_awd[1]  :
			rf_array[1] <= #U_DLY  array_dwd;
	    array_awd[2]  :
			rf_array[2] <= #U_DLY  array_dwd;
		array_awd[3]  :
			rf_array[3] <= #U_DLY  array_dwd;
	    array_awd[4]  :
			rf_array[4] <= #U_DLY  array_dwd;
	    array_awd[5]  :
			rf_array[5] <= #U_DLY  array_dwd;
		array_awd[6]  :
			rf_array[6] <= #U_DLY  array_dwd;
	    array_awd[7]  :
			rf_array[7] <= #U_DLY  array_dwd;
	    array_awd[8]  :
			rf_array[8] <= #U_DLY  array_dwd;
		array_awd[9]  :
			rf_array[9] <= #U_DLY  array_dwd;
		array_awd[10]  :
			rf_array[10] <= #U_DLY array_dwd;
	    array_awd[11]  :
			rf_array[11] <= #U_DLY array_dwd;
	    array_awd[12]  :
			rf_array[12] <= #U_DLY array_dwd;
		array_awd[13]  :
			rf_array[13] <= #U_DLY array_dwd;
	    array_awd[14]  :
			rf_array[14] <= #U_DLY array_dwd;
	    array_awd[15]  :
			rf_array[15] <= #U_DLY array_dwd;
		array_awd[16]  :
			rf_array[16] <= #U_DLY array_dwd;
	    array_awd[17]  :
			rf_array[17] <= #U_DLY array_dwd;
	    array_awd[18]  :
			rf_array[18] <= #U_DLY array_dwd;
		array_awd[19]  :
			rf_array[19] <= #U_DLY array_dwd;
	    array_awd[20]  :
			rf_array[20] <= #U_DLY array_dwd;
	    array_awd[21]  :
			rf_array[21] <= #U_DLY array_dwd;
	    array_awd[22]  :
			rf_array[22] <= #U_DLY array_dwd;
		array_awd[23]  :
			rf_array[23] <= #U_DLY array_dwd;
	    array_awd[24]  :
			rf_array[24] <= #U_DLY array_dwd;
	    array_awd[25]  :
			rf_array[25] <= #U_DLY array_dwd;
		array_awd[26]  :
			rf_array[26] <= #U_DLY array_dwd;
	    array_awd[27]  :
			rf_array[27] <= #U_DLY array_dwd;
	    array_awd[28]  :
			rf_array[28] <= #U_DLY array_dwd;
		array_awd[29]  :
			rf_array[29] <= #U_DLY array_dwd;
		array_awd[30]  :
			rf_array[30] <= #U_DLY array_dwd;
	    array_awd[31]  :
			rf_array[31] <= #U_DLY array_dwd;
		default   :
		    rf_array[0] <= #U_DLY 32'h0000;
		endcase
	end

    // scan port
    always @(posedge scan_clk )
    begin
      	shf_array1;
      	shf_array0;
      	shf_arrayst;
    end

    // state bits write operation
    // check a set
    wire [2:0]  array_dwstcal = {array_dwstca_2, 1'b0, 1'b1};
    always @(posedge array_censtwcal)
    begin
		case(1'b1)
	    array_aca[0]  :
			rf_arraystl[0] <= #U_DLY array_dwstcal;
	    array_aca[1]  :
			rf_arraystl[1] <= #U_DLY array_dwstcal;
	    array_aca[2]  :
			rf_arraystl[2] <= #U_DLY array_dwstcal;
		array_aca[3]  :
			rf_arraystl[3] <= #U_DLY array_dwstcal;
	    array_aca[4]  :
			rf_arraystl[4] <= #U_DLY array_dwstcal;
	    array_aca[5]  :
			rf_arraystl[5] <= #U_DLY array_dwstcal;
		array_aca[6]  :
			rf_arraystl[6] <= #U_DLY array_dwstcal;
	    array_aca[7]  :
			rf_arraystl[7] <= #U_DLY array_dwstcal;
	    array_aca[8]  :
			rf_arraystl[8] <= #U_DLY array_dwstcal;
		array_aca[9]  :
			rf_arraystl[9] <= #U_DLY array_dwstcal;
		array_aca[10]  :
			rf_arraystl[10] <= #U_DLY array_dwstcal;
	    array_aca[11]  :
			rf_arraystl[11] <= #U_DLY array_dwstcal;
	    array_aca[12]  :
			rf_arraystl[12] <= #U_DLY array_dwstcal;
		array_aca[13]  :
			rf_arraystl[13] <= #U_DLY array_dwstcal;
	    array_aca[14]  :
			rf_arraystl[14] <= #U_DLY array_dwstcal;
	    array_aca[15]  :
			rf_arraystl[15] <= #U_DLY array_dwstcal;
		array_aca[16]  :
			rf_arraystl[16] <= #U_DLY array_dwstcal;
	    array_aca[17]  :
			rf_arraystl[17] <= #U_DLY array_dwstcal;
	    array_aca[18]  :
			rf_arraystl[18] <= #U_DLY array_dwstcal;
		array_aca[19]  :
			rf_arraystl[19] <= #U_DLY array_dwstcal;
	    array_aca[20]  :
			rf_arraystl[20] <= #U_DLY array_dwstcal;
	    array_aca[21]  :
			rf_arraystl[21] <= #U_DLY array_dwstcal;
	    array_aca[22]  :
			rf_arraystl[22] <= #U_DLY array_dwstcal;
		array_aca[23]  :
			rf_arraystl[23] <= #U_DLY array_dwstcal;
	    array_aca[24]  :
			rf_arraystl[24] <= #U_DLY array_dwstcal;
	    array_aca[25]  :
			rf_arraystl[25] <= #U_DLY array_dwstcal;
		array_aca[26]  :
			rf_arraystl[26] <= #U_DLY array_dwstcal;
	    array_aca[27]  :
			rf_arraystl[27] <= #U_DLY array_dwstcal;
	    array_aca[28]  :
			rf_arraystl[28] <= #U_DLY array_dwstcal;
		array_aca[29]  :
			rf_arraystl[29] <= #U_DLY array_dwstcal;
		array_aca[30]  :
			rf_arraystl[30] <= #U_DLY array_dwstcal;
	    array_aca[31]  :
			rf_arraystl[31] <= #U_DLY array_dwstcal;
		default  :
		    rf_arraystl[0]  <= #U_DLY {`RFST_WIDTH{1'b0}};
		endcase
    end


    wire [1:0]  array_dwstcah = {1'b0, array_dwstca_3};
    always @(posedge array_censtwcah)
    begin
		case(1'b1)
	    array_aca[0]  :
			rf_arraysth[0] <= #U_DLY array_dwstcah;
	    array_aca[1]  :
			rf_arraysth[1] <= #U_DLY array_dwstcah;
	    array_aca[2]  :
			rf_arraysth[2] <= #U_DLY array_dwstcah;
		array_aca[3]  :
			rf_arraysth[3] <= #U_DLY array_dwstcah;
	    array_aca[4]  :
			rf_arraysth[4] <= #U_DLY array_dwstcah;
	    array_aca[5]  :
			rf_arraysth[5] <= #U_DLY array_dwstcah;
		array_aca[6]  :
			rf_arraysth[6] <= #U_DLY array_dwstcah;
	    array_aca[7]  :
			rf_arraysth[7] <= #U_DLY array_dwstcah;
	    array_aca[8]  :
			rf_arraysth[8] <= #U_DLY array_dwstcah;
		array_aca[9]  :
			rf_arraysth[9] <= #U_DLY array_dwstcah;
		array_aca[10]  :
			rf_arraysth[10] <= #U_DLY array_dwstcah;
	    array_aca[11]  :
			rf_arraysth[11] <= #U_DLY array_dwstcah;
	    array_aca[12]  :
			rf_arraysth[12] <= #U_DLY array_dwstcah;
		array_aca[13]  :
			rf_arraysth[13] <= #U_DLY array_dwstcah;
	    array_aca[14]  :
			rf_arraysth[14] <= #U_DLY array_dwstcah;
	    array_aca[15]  :
			rf_arraysth[15] <= #U_DLY array_dwstcah;
		array_aca[16]  :
			rf_arraysth[16] <= #U_DLY array_dwstcah;
	    array_aca[17]  :
			rf_arraysth[17] <= #U_DLY array_dwstcah;
	    array_aca[18]  :
			rf_arraysth[18] <= #U_DLY array_dwstcah;
		array_aca[19]  :
			rf_arraysth[19] <= #U_DLY array_dwstcah;
	    array_aca[20]  :
			rf_arraysth[20] <= #U_DLY array_dwstcah;
	    array_aca[21]  :
			rf_arraysth[21] <= #U_DLY array_dwstcah;
	    array_aca[22]  :
			rf_arraysth[22] <= #U_DLY array_dwstcah;
		array_aca[23]  :
			rf_arraysth[23] <= #U_DLY array_dwstcah;
	    array_aca[24]  :
			rf_arraysth[24] <= #U_DLY array_dwstcah;
	    array_aca[25]  :
			rf_arraysth[25] <= #U_DLY array_dwstcah;
		array_aca[26]  :
			rf_arraysth[26] <= #U_DLY array_dwstcah;
	    array_aca[27]  :
			rf_arraysth[27] <= #U_DLY array_dwstcah;
	    array_aca[28]  :
			rf_arraysth[28] <= #U_DLY array_dwstcah;
		array_aca[29]  :
			rf_arraysth[29] <= #U_DLY array_dwstcah;
		array_aca[30]  :
			rf_arraysth[30] <= #U_DLY array_dwstcah;
	    array_aca[31]  :
			rf_arraysth[31] <= #U_DLY array_dwstcah;
		default  :
		    rf_arraysth[0]  <= #U_DLY 'b0;
		endcase
    end


    // check b set
    wire [2:0]  array_dwstcbl = {array_dwstcb_2, 1'b0, 1'b1 };
    always @(posedge array_censtwcbl)
    begin
		case(1'b1)
	    array_acb[0]  :
			rf_arraystl[0] <= #U_DLY array_dwstcbl;
	    array_acb[1]  :
			rf_arraystl[1] <= #U_DLY array_dwstcbl;
	    array_acb[2]  :
			rf_arraystl[2] <= #U_DLY array_dwstcbl;
		array_acb[3]  :
			rf_arraystl[3] <= #U_DLY array_dwstcbl;
	    array_acb[4]  :
			rf_arraystl[4] <= #U_DLY array_dwstcbl;
	    array_acb[5]  :
			rf_arraystl[5] <= #U_DLY array_dwstcbl;
		array_acb[6]  :
			rf_arraystl[6] <= #U_DLY array_dwstcbl;
	    array_acb[7]  :
			rf_arraystl[7] <= #U_DLY array_dwstcbl;
	    array_acb[8]  :
			rf_arraystl[8] <= #U_DLY array_dwstcbl;
		array_acb[9]  :
			rf_arraystl[9] <= #U_DLY array_dwstcbl;
		array_acb[10]  :
			rf_arraystl[10] <= #U_DLY array_dwstcbl;
	    array_acb[11]  :
			rf_arraystl[11] <= #U_DLY array_dwstcbl;
	    array_acb[12]  :
			rf_arraystl[12] <= #U_DLY array_dwstcbl;
		array_acb[13]  :
			rf_arraystl[13] <= #U_DLY array_dwstcbl;
	    array_acb[14]  :
			rf_arraystl[14] <= #U_DLY array_dwstcbl;
	    array_acb[15]  :
			rf_arraystl[15] <= #U_DLY array_dwstcbl;
		array_acb[16]  :
			rf_arraystl[16] <= #U_DLY array_dwstcbl;
	    array_acb[17]  :
			rf_arraystl[17] <= #U_DLY array_dwstcbl;
	    array_acb[18]  :
			rf_arraystl[18] <= #U_DLY array_dwstcbl;
		array_acb[19]  :
			rf_arraystl[19] <= #U_DLY array_dwstcbl;
	    array_acb[20]  :
			rf_arraystl[20] <= #U_DLY array_dwstcbl;
	    array_acb[21]  :
			rf_arraystl[21] <= #U_DLY array_dwstcbl;
	    array_acb[22]  :
			rf_arraystl[22] <= #U_DLY array_dwstcbl;
		array_acb[23]  :
			rf_arraystl[23] <= #U_DLY array_dwstcbl;
	    array_acb[24]  :
			rf_arraystl[24] <= #U_DLY array_dwstcbl;
	    array_acb[25]  :
			rf_arraystl[25] <= #U_DLY array_dwstcbl;
		array_acb[26]  :
			rf_arraystl[26] <= #U_DLY array_dwstcbl;
	    array_acb[27]  :
			rf_arraystl[27] <= #U_DLY array_dwstcbl;
	    array_acb[28]  :
			rf_arraystl[28] <= #U_DLY array_dwstcbl;
		array_acb[29]  :
			rf_arraystl[29] <= #U_DLY array_dwstcbl;
		array_acb[30]  :
			rf_arraystl[30] <= #U_DLY array_dwstcbl;
	    array_acb[31]  :
			rf_arraystl[31] <= #U_DLY array_dwstcbl;
		default  :
		    rf_arraystl[0]  <= #U_DLY {`RFST_WIDTH{1'b0}};
		endcase
    end

    wire [1:0]  array_dwstcbh = {1'b0 , array_dwstcb_3 } ;
    always @(posedge array_censtwcbh)
    begin
		case(1'b1)
	    array_acb[0]  :
			rf_arraysth[0] <= #U_DLY array_dwstcbh;
	    array_acb[1]  :
			rf_arraysth[1] <= #U_DLY array_dwstcbh;
	    array_acb[2]  :
			rf_arraysth[2] <= #U_DLY array_dwstcbh;
		array_acb[3]  :
			rf_arraysth[3] <= #U_DLY array_dwstcbh;
	    array_acb[4]  :
			rf_arraysth[4] <= #U_DLY array_dwstcbh;
	    array_acb[5]  :
			rf_arraysth[5] <= #U_DLY array_dwstcbh;
		array_acb[6]  :
			rf_arraysth[6] <= #U_DLY array_dwstcbh;
	    array_acb[7]  :
			rf_arraysth[7] <= #U_DLY array_dwstcbh;
	    array_acb[8]  :
			rf_arraysth[8] <= #U_DLY array_dwstcbh;
		array_acb[9]  :
			rf_arraysth[9] <= #U_DLY array_dwstcbh;
		array_acb[10]  :
			rf_arraysth[10] <= #U_DLY array_dwstcbh;
	    array_acb[11]  :
			rf_arraysth[11] <= #U_DLY array_dwstcbh;
	    array_acb[12]  :
			rf_arraysth[12] <= #U_DLY array_dwstcbh;
		array_acb[13]  :
			rf_arraysth[13] <= #U_DLY array_dwstcbh;
	    array_acb[14]  :
			rf_arraysth[14] <= #U_DLY array_dwstcbh;
	    array_acb[15]  :
			rf_arraysth[15] <= #U_DLY array_dwstcbh;
		array_acb[16]  :
			rf_arraysth[16] <= #U_DLY array_dwstcbh;
	    array_acb[17]  :
			rf_arraysth[17] <= #U_DLY array_dwstcbh;
	    array_acb[18]  :
			rf_arraysth[18] <= #U_DLY array_dwstcbh;
		array_acb[19]  :
			rf_arraysth[19] <= #U_DLY array_dwstcbh;
	    array_acb[20]  :
			rf_arraysth[20] <= #U_DLY array_dwstcbh;
	    array_acb[21]  :
			rf_arraysth[21] <= #U_DLY array_dwstcbh;
	    array_acb[22]  :
			rf_arraysth[22] <= #U_DLY array_dwstcbh;
		array_acb[23]  :
			rf_arraysth[23] <= #U_DLY array_dwstcbh;
	    array_acb[24]  :
			rf_arraysth[24] <= #U_DLY array_dwstcbh;
	    array_acb[25]  :
			rf_arraysth[25] <= #U_DLY array_dwstcbh;
		array_acb[26]  :
			rf_arraysth[26] <= #U_DLY array_dwstcbh;
	    array_acb[27]  :
			rf_arraysth[27] <= #U_DLY array_dwstcbh;
	    array_acb[28]  :
			rf_arraysth[28] <= #U_DLY array_dwstcbh;
		array_acb[29]  :
			rf_arraysth[29] <= #U_DLY array_dwstcbh;
		array_acb[30]  :
			rf_arraysth[30] <= #U_DLY array_dwstcbh;
	    array_acb[31]  :
			rf_arraysth[31] <= #U_DLY array_dwstcbh;
		default  :
		    rf_arraysth[0]  <= #U_DLY 'b0 ;
		endcase
    end

    // write a clear
    wire  [`RFST_WIDTH-1:0]  rf_arraystlwa =
					   				{3{array_awa[0]}} & rf_arraystl[0]			|
                       				{3{array_awa[1]}} & rf_arraystl[1]			|
                       				{3{array_awa[2]}} & rf_arraystl[2]			|
                       				{3{array_awa[3]}} & rf_arraystl[3]			|
                       				{3{array_awa[4]}} & rf_arraystl[4]			|
                       				{3{array_awa[5]}} & rf_arraystl[5]			|
                       				{3{array_awa[6]}} & rf_arraystl[6]			|
                       				{3{array_awa[7]}} & rf_arraystl[7]			|
                       				{3{array_awa[8]}} & rf_arraystl[8]			|
                       				{3{array_awa[9]}} & rf_arraystl[9]			|
                       				{3{array_awa[10]}} & rf_arraystl[10]			|
                       				{3{array_awa[11]}} & rf_arraystl[11]			|
                       				{3{array_awa[12]}} & rf_arraystl[12]			|
                       				{3{array_awa[13]}} & rf_arraystl[13]			|
                       				{3{array_awa[14]}} & rf_arraystl[14]			|
                       				{3{array_awa[15]}} & rf_arraystl[15]			|
                       				{3{array_awa[16]}} & rf_arraystl[16]			|
                       				{3{array_awa[17]}} & rf_arraystl[17]			|
                       				{3{array_awa[18]}} & rf_arraystl[18]			|
                       				{3{array_awa[19]}} & rf_arraystl[19]			|
                       				{3{array_awa[20]}} & rf_arraystl[20]			|
                       				{3{array_awa[21]}} & rf_arraystl[21]			|
                       				{3{array_awa[22]}} & rf_arraystl[22]			|
                       				{3{array_awa[23]}} & rf_arraystl[23]			|
                       				{3{array_awa[24]}} & rf_arraystl[24]			|
                       				{3{array_awa[25]}} & rf_arraystl[25]			|
                       				{3{array_awa[26]}} & rf_arraystl[26]			|
                       				{3{array_awa[27]}} & rf_arraystl[27]			|
                       				{3{array_awa[28]}} & rf_arraystl[28]			|
                       				{3{array_awa[29]}} & rf_arraystl[29]			|
                       				{3{array_awa[30]}} & rf_arraystl[30]			|
                       				{3{array_awa[31]}} & rf_arraystl[31]	 ;

    wire [2:0]  array_dstlwa = {1'b0, 1'b0, 1'b0};
    always @(posedge array_censtwa)
    begin
		case(1'b1)
	    array_awa[0]  :
			rf_arraystl[0] <= #U_DLY array_dstlwa;
	    array_awa[1]  :
			rf_arraystl[1] <= #U_DLY array_dstlwa;
	    array_awa[2]  :
			rf_arraystl[2] <= #U_DLY array_dstlwa;
		array_awa[3]  :
			rf_arraystl[3] <= #U_DLY array_dstlwa;
	    array_awa[4]  :
			rf_arraystl[4] <= #U_DLY array_dstlwa;
	    array_awa[5]  :
			rf_arraystl[5] <= #U_DLY array_dstlwa;
		array_awa[6]  :
			rf_arraystl[6] <= #U_DLY array_dstlwa;
	    array_awa[7]  :
			rf_arraystl[7] <= #U_DLY array_dstlwa;
	    array_awa[8]  :
			rf_arraystl[8] <= #U_DLY array_dstlwa;
		array_awa[9]  :
			rf_arraystl[9] <= #U_DLY array_dstlwa;
		array_awa[10]  :
			rf_arraystl[10] <= #U_DLY array_dstlwa;
	    array_awa[11]  :
			rf_arraystl[11] <= #U_DLY array_dstlwa;
	    array_awa[12]  :
			rf_arraystl[12] <= #U_DLY array_dstlwa;
		array_awa[13]  :
			rf_arraystl[13] <= #U_DLY array_dstlwa;
	    array_awa[14]  :
			rf_arraystl[14] <= #U_DLY array_dstlwa;
	    array_awa[15]  :
			rf_arraystl[15] <= #U_DLY array_dstlwa;
		array_awa[16]  :
			rf_arraystl[16] <= #U_DLY array_dstlwa;
	    array_awa[17]  :
			rf_arraystl[17] <= #U_DLY array_dstlwa;
	    array_awa[18]  :
			rf_arraystl[18] <= #U_DLY array_dstlwa;
		array_awa[19]  :
			rf_arraystl[19] <= #U_DLY array_dstlwa;
	    array_awa[20]  :
			rf_arraystl[20] <= #U_DLY array_dstlwa;
	    array_awa[21]  :
			rf_arraystl[21] <= #U_DLY array_dstlwa;
	    array_awa[22]  :
			rf_arraystl[22] <= #U_DLY array_dstlwa;
		array_awa[23]  :
			rf_arraystl[23] <= #U_DLY array_dstlwa;
	    array_awa[24]  :
			rf_arraystl[24] <= #U_DLY array_dstlwa;
	    array_awa[25]  :
			rf_arraystl[25] <= #U_DLY array_dstlwa;
		array_awa[26]  :
			rf_arraystl[26] <= #U_DLY array_dstlwa;
	    array_awa[27]  :
			rf_arraystl[27] <= #U_DLY array_dstlwa;
	    array_awa[28]  :
			rf_arraystl[28] <= #U_DLY array_dstlwa;
		array_awa[29]  :
			rf_arraystl[29] <= #U_DLY array_dstlwa;
		array_awa[30]  :
			rf_arraystl[30] <= #U_DLY array_dstlwa;
	    array_awa[31]  :
			rf_arraystl[31] <= #U_DLY array_dstlwa;
		default  :
		    rf_arraystl[0]  <= #U_DLY {`RFST_WIDTH{1'b0}};
		endcase
    end

    // write b clear
    wire  [`RFST_WIDTH-1:0]  rf_arraystlwb =
					   				{3{array_awb[0]}} & rf_arraystl[0]			|
                       				{3{array_awb[1]}} & rf_arraystl[1]			|
                       				{3{array_awb[2]}} & rf_arraystl[2]			|
                       				{3{array_awb[3]}} & rf_arraystl[3]			|
                       				{3{array_awb[4]}} & rf_arraystl[4]			|
                       				{3{array_awb[5]}} & rf_arraystl[5]			|
                       				{3{array_awb[6]}} & rf_arraystl[6]			|
                       				{3{array_awb[7]}} & rf_arraystl[7]			|
                       				{3{array_awb[8]}} & rf_arraystl[8]			|
                       				{3{array_awb[9]}} & rf_arraystl[9]			|
                       				{3{array_awb[10]}} & rf_arraystl[10]			|
                       				{3{array_awb[11]}} & rf_arraystl[11]			|
                       				{3{array_awb[12]}} & rf_arraystl[12]			|
                       				{3{array_awb[13]}} & rf_arraystl[13]			|
                       				{3{array_awb[14]}} & rf_arraystl[14]			|
                       				{3{array_awb[15]}} & rf_arraystl[15]			|
                       				{3{array_awb[16]}} & rf_arraystl[16]			|
                       				{3{array_awb[17]}} & rf_arraystl[17]			|
                       				{3{array_awb[18]}} & rf_arraystl[18]			|
                       				{3{array_awb[19]}} & rf_arraystl[19]			|
                       				{3{array_awb[20]}} & rf_arraystl[20]			|
                       				{3{array_awb[21]}} & rf_arraystl[21]			|
                       				{3{array_awb[22]}} & rf_arraystl[22]			|
                       				{3{array_awb[23]}} & rf_arraystl[23]			|
                       				{3{array_awb[24]}} & rf_arraystl[24]			|
                       				{3{array_awb[25]}} & rf_arraystl[25]			|
                       				{3{array_awb[26]}} & rf_arraystl[26]			|
                       				{3{array_awb[27]}} & rf_arraystl[27]			|
                       				{3{array_awb[28]}} & rf_arraystl[28]			|
                       				{3{array_awb[29]}} & rf_arraystl[29]			|
                       				{3{array_awb[30]}} & rf_arraystl[30]			|
                       				{3{array_awb[31]}} & rf_arraystl[31]	 ;

    wire [2:0]  array_dstlwb = {1'b0, 1'b0, 1'b0};
    always @(posedge array_censtwb)
    begin
		case(1'b1)
	    array_awb[0]  :
			rf_arraystl[0] <= #U_DLY array_dstlwb;
	    array_awb[1]  :
			rf_arraystl[1] <= #U_DLY array_dstlwb;
	    array_awb[2]  :
			rf_arraystl[2] <= #U_DLY array_dstlwb;
		array_awb[3]  :
			rf_arraystl[3] <= #U_DLY array_dstlwb;
	    array_awb[4]  :
			rf_arraystl[4] <= #U_DLY array_dstlwb;
	    array_awb[5]  :
			rf_arraystl[5] <= #U_DLY array_dstlwb;
		array_awb[6]  :
			rf_arraystl[6] <= #U_DLY array_dstlwb;
	    array_awb[7]  :
			rf_arraystl[7] <= #U_DLY array_dstlwb;
	    array_awb[8]  :
			rf_arraystl[8] <= #U_DLY array_dstlwb;
		array_awb[9]  :
			rf_arraystl[9] <= #U_DLY array_dstlwb;
		array_awb[10]  :
			rf_arraystl[10] <= #U_DLY array_dstlwb;
	    array_awb[11]  :
			rf_arraystl[11] <= #U_DLY array_dstlwb;
	    array_awb[12]  :
			rf_arraystl[12] <= #U_DLY array_dstlwb;
		array_awb[13]  :
			rf_arraystl[13] <= #U_DLY array_dstlwb;
	    array_awb[14]  :
			rf_arraystl[14] <= #U_DLY array_dstlwb;
	    array_awb[15]  :
			rf_arraystl[15] <= #U_DLY array_dstlwb;
		array_awb[16]  :
			rf_arraystl[16] <= #U_DLY array_dstlwb;
	    array_awb[17]  :
			rf_arraystl[17] <= #U_DLY array_dstlwb;
	    array_awb[18]  :
			rf_arraystl[18] <= #U_DLY array_dstlwb;
		array_awb[19]  :
			rf_arraystl[19] <= #U_DLY array_dstlwb;
	    array_awb[20]  :
			rf_arraystl[20] <= #U_DLY array_dstlwb;
	    array_awb[21]  :
			rf_arraystl[21] <= #U_DLY array_dstlwb;
	    array_awb[22]  :
			rf_arraystl[22] <= #U_DLY array_dstlwb;
		array_awb[23]  :
			rf_arraystl[23] <= #U_DLY array_dstlwb;
	    array_awb[24]  :
			rf_arraystl[24] <= #U_DLY array_dstlwb;
	    array_awb[25]  :
			rf_arraystl[25] <= #U_DLY array_dstlwb;
		array_awb[26]  :
			rf_arraystl[26] <= #U_DLY array_dstlwb;
	    array_awb[27]  :
			rf_arraystl[27] <= #U_DLY array_dstlwb;
	    array_awb[28]  :
			rf_arraystl[28] <= #U_DLY array_dstlwb;
		array_awb[29]  :
			rf_arraystl[29] <= #U_DLY array_dstlwb;
		array_awb[30]  :
			rf_arraystl[30] <= #U_DLY array_dstlwb;
	    array_awb[31]  :
			rf_arraystl[31] <= #U_DLY array_dstlwb;
		default  :
		    rf_arraystl[0]  <= #U_DLY {`RFST_WIDTH{1'b0}};
		endcase
    end

    // write c clear
    always @(posedge array_censtwc)
    begin
		case(1'b1)
	    array_awc[0]  :
			rf_arraystl[0] <= #U_DLY {`RFST_WIDTH{1'b0}};
	    array_awc[1]  :
			rf_arraystl[1] <= #U_DLY {`RFST_WIDTH{1'b0}};
	    array_awc[2]  :
			rf_arraystl[2] <= #U_DLY {`RFST_WIDTH{1'b0}};
		array_awc[3]  :
			rf_arraystl[3] <= #U_DLY {`RFST_WIDTH{1'b0}};
	    array_awc[4]  :
			rf_arraystl[4] <= #U_DLY {`RFST_WIDTH{1'b0}};
	    array_awc[5]  :
			rf_arraystl[5] <= #U_DLY {`RFST_WIDTH{1'b0}};
		array_awc[6]  :
			rf_arraystl[6] <= #U_DLY {`RFST_WIDTH{1'b0}};
	    array_awc[7]  :
			rf_arraystl[7] <= #U_DLY {`RFST_WIDTH{1'b0}};
	    array_awc[8]  :
			rf_arraystl[8] <= #U_DLY {`RFST_WIDTH{1'b0}};
		array_awc[9]  :
			rf_arraystl[9] <= #U_DLY {`RFST_WIDTH{1'b0}};
		array_awc[10]  :
			rf_arraystl[10] <= #U_DLY {`RFST_WIDTH{1'b0}};
	    array_awc[11]  :
			rf_arraystl[11] <= #U_DLY {`RFST_WIDTH{1'b0}};
	    array_awc[12]  :
			rf_arraystl[12] <= #U_DLY {`RFST_WIDTH{1'b0}};
		array_awc[13]  :
			rf_arraystl[13] <= #U_DLY {`RFST_WIDTH{1'b0}};
	    array_awc[14]  :
			rf_arraystl[14] <= #U_DLY {`RFST_WIDTH{1'b0}};
	    array_awc[15]  :
			rf_arraystl[15] <= #U_DLY {`RFST_WIDTH{1'b0}};
		array_awc[16]  :
			rf_arraystl[16] <= #U_DLY {`RFST_WIDTH{1'b0}};
	    array_awc[17]  :
			rf_arraystl[17] <= #U_DLY {`RFST_WIDTH{1'b0}};
	    array_awc[18]  :
			rf_arraystl[18] <= #U_DLY {`RFST_WIDTH{1'b0}};
		array_awc[19]  :
			rf_arraystl[19] <= #U_DLY {`RFST_WIDTH{1'b0}};
	    array_awc[20]  :
			rf_arraystl[20] <= #U_DLY {`RFST_WIDTH{1'b0}};
	    array_awc[21]  :
			rf_arraystl[21] <= #U_DLY {`RFST_WIDTH{1'b0}};
	    array_awc[22]  :
			rf_arraystl[22] <= #U_DLY {`RFST_WIDTH{1'b0}};
		array_awc[23]  :
			rf_arraystl[23] <= #U_DLY {`RFST_WIDTH{1'b0}};
	    array_awc[24]  :
			rf_arraystl[24] <= #U_DLY {`RFST_WIDTH{1'b0}};
	    array_awc[25]  :
			rf_arraystl[25] <= #U_DLY {`RFST_WIDTH{1'b0}};
		array_awc[26]  :
			rf_arraystl[26] <= #U_DLY {`RFST_WIDTH{1'b0}};
	    array_awc[27]  :
			rf_arraystl[27] <= #U_DLY {`RFST_WIDTH{1'b0}};
	    array_awc[28]  :
			rf_arraystl[28] <= #U_DLY {`RFST_WIDTH{1'b0}};
		array_awc[29]  :
			rf_arraystl[29] <= #U_DLY {`RFST_WIDTH{1'b0}};
		array_awc[30]  :
			rf_arraystl[30] <= #U_DLY {`RFST_WIDTH{1'b0}};
	    array_awc[31]  :
			rf_arraystl[31] <= #U_DLY {`RFST_WIDTH{1'b0}};
		default  :
		    rf_arraystl[0]  <= #U_DLY {`RFST_WIDTH{1'b0}};
		endcase
    end

    // write d clear
    always @(posedge array_censtwd)
    begin
		case(1'b1)
	    array_awd[0]  :
			rf_arraystl[0] <= #U_DLY {`RFST_WIDTH{1'b0}};
	    array_awd[1]  :
			rf_arraystl[1] <= #U_DLY {`RFST_WIDTH{1'b0}};
	    array_awd[2]  :
			rf_arraystl[2] <= #U_DLY {`RFST_WIDTH{1'b0}};
		array_awd[3]  :
			rf_arraystl[3] <= #U_DLY {`RFST_WIDTH{1'b0}};
	    array_awd[4]  :
			rf_arraystl[4] <= #U_DLY {`RFST_WIDTH{1'b0}};
	    array_awd[5]  :
			rf_arraystl[5] <= #U_DLY {`RFST_WIDTH{1'b0}};
		array_awd[6]  :
			rf_arraystl[6] <= #U_DLY {`RFST_WIDTH{1'b0}};
	    array_awd[7]  :
			rf_arraystl[7] <= #U_DLY {`RFST_WIDTH{1'b0}};
	    array_awd[8]  :
			rf_arraystl[8] <= #U_DLY {`RFST_WIDTH{1'b0}};
		array_awd[9]  :
			rf_arraystl[9] <= #U_DLY {`RFST_WIDTH{1'b0}};
		array_awd[10]  :
			rf_arraystl[10] <= #U_DLY {`RFST_WIDTH{1'b0}};
	    array_awd[11]  :
			rf_arraystl[11] <= #U_DLY {`RFST_WIDTH{1'b0}};
	    array_awd[12]  :
			rf_arraystl[12] <= #U_DLY {`RFST_WIDTH{1'b0}};
		array_awd[13]  :
			rf_arraystl[13] <= #U_DLY {`RFST_WIDTH{1'b0}};
	    array_awd[14]  :
			rf_arraystl[14] <= #U_DLY {`RFST_WIDTH{1'b0}};
	    array_awd[15]  :
			rf_arraystl[15] <= #U_DLY {`RFST_WIDTH{1'b0}};
		array_awd[16]  :
			rf_arraystl[16] <= #U_DLY {`RFST_WIDTH{1'b0}};
	    array_awd[17]  :
			rf_arraystl[17] <= #U_DLY {`RFST_WIDTH{1'b0}};
	    array_awd[18]  :
			rf_arraystl[18] <= #U_DLY {`RFST_WIDTH{1'b0}};
		array_awd[19]  :
			rf_arraystl[19] <= #U_DLY {`RFST_WIDTH{1'b0}};
	    array_awd[20]  :
			rf_arraystl[20] <= #U_DLY {`RFST_WIDTH{1'b0}};
	    array_awd[21]  :
			rf_arraystl[21] <= #U_DLY {`RFST_WIDTH{1'b0}};
	    array_awd[22]  :
			rf_arraystl[22] <= #U_DLY {`RFST_WIDTH{1'b0}};
		array_awd[23]  :
			rf_arraystl[23] <= #U_DLY {`RFST_WIDTH{1'b0}};
	    array_awd[24]  :
			rf_arraystl[24] <= #U_DLY {`RFST_WIDTH{1'b0}};
	    array_awd[25]  :
			rf_arraystl[25] <= #U_DLY {`RFST_WIDTH{1'b0}};
		array_awd[26]  :
			rf_arraystl[26] <= #U_DLY {`RFST_WIDTH{1'b0}};
	    array_awd[27]  :
			rf_arraystl[27] <= #U_DLY {`RFST_WIDTH{1'b0}};
	    array_awd[28]  :
			rf_arraystl[28] <= #U_DLY {`RFST_WIDTH{1'b0}};
		array_awd[29]  :
			rf_arraystl[29] <= #U_DLY {`RFST_WIDTH{1'b0}};
		array_awd[30]  :
			rf_arraystl[30] <= #U_DLY {`RFST_WIDTH{1'b0}};
	    array_awd[31]  :
			rf_arraystl[31] <= #U_DLY {`RFST_WIDTH{1'b0}};
		default  :
		    rf_arraystl[0]  <= #U_DLY {`RFST_WIDTH{1'b0}};
		endcase
    end

    // for rtl simulation
    // synopsys off
    wire [4:0]  gpr_00_state  = {rf_arraysth[0] , rf_arraystl[0] } ;
    wire [4:0]  gpr_01_state  = {rf_arraysth[1] , rf_arraystl[1] };
    wire [4:0]  gpr_02_state  = {rf_arraysth[2] , rf_arraystl[2] };
    wire [4:0]  gpr_03_state  = {rf_arraysth[3] , rf_arraystl[3] };
    wire [4:0]  gpr_04_state  = {rf_arraysth[4] , rf_arraystl[4] };
    wire [4:0]  gpr_05_state  = {rf_arraysth[5] , rf_arraystl[5] };
    wire [4:0]  gpr_06_state  = {rf_arraysth[6] , rf_arraystl[6] };
    wire [4:0]  gpr_07_state  = {rf_arraysth[7] , rf_arraystl[7] };
    wire [4:0]  gpr_08_state  = {rf_arraysth[8] , rf_arraystl[8] };
    wire [4:0]  gpr_09_state  = {rf_arraysth[9] , rf_arraystl[9] };
    wire [4:0]  gpr_10_state  = {rf_arraysth[10], rf_arraystl[10]};
    wire [4:0]  gpr_11_state  = {rf_arraysth[11], rf_arraystl[11]};
    wire [4:0]  gpr_12_state  = {rf_arraysth[12], rf_arraystl[12]};
    wire [4:0]  gpr_13_state  = {rf_arraysth[13], rf_arraystl[13]};
    wire [4:0]  gpr_14_state  = {rf_arraysth[14], rf_arraystl[14]};
    wire [4:0]  gpr_15_state  = {rf_arraysth[15], rf_arraystl[15]};
    wire [4:0]  gpr_16_state  = {rf_arraysth[16], rf_arraystl[16]};
    wire [4:0]  gpr_17_state  = {rf_arraysth[17], rf_arraystl[17]};
    wire [4:0]  gpr_18_state  = {rf_arraysth[18], rf_arraystl[18]};
    wire [4:0]  gpr_19_state  = {rf_arraysth[19], rf_arraystl[19]};
    wire [4:0]  gpr_20_state  = {rf_arraysth[20], rf_arraystl[20]};
    wire [4:0]  gpr_21_state  = {rf_arraysth[21], rf_arraystl[21]};
    wire [4:0]  gpr_22_state  = {rf_arraysth[22], rf_arraystl[22]};
    wire [4:0]  gpr_23_state  = {rf_arraysth[23], rf_arraystl[23]};
    wire [4:0]  gpr_24_state  = {rf_arraysth[24], rf_arraystl[24]};
    wire [4:0]  gpr_25_state  = {rf_arraysth[25], rf_arraystl[25]};
    wire [4:0]  gpr_26_state  = {rf_arraysth[26], rf_arraystl[26]};
    wire [4:0]  gpr_27_state  = {rf_arraysth[27], rf_arraystl[27]};
    wire [4:0]  gpr_28_state  = {rf_arraysth[28], rf_arraystl[28]};
    wire [4:0]  gpr_29_state  = {rf_arraysth[29], rf_arraystl[29]};
    wire [4:0]  gpr_30_state  = {rf_arraysth[30], rf_arraystl[30]};
    wire [4:0]  gpr_31_state  = {rf_arraysth[31], rf_arraystl[31]};

    wire [31:0]  gpr_00_data  = rf_array[0] ;
    wire [31:0]  gpr_01_data  = rf_array[1] ;
    wire [31:0]  gpr_02_data  = rf_array[2] ;
    wire [31:0]  gpr_03_data  = rf_array[3] ;
    wire [31:0]  gpr_04_data  = rf_array[4] ;
    wire [31:0]  gpr_05_data  = rf_array[5] ;
    wire [31:0]  gpr_06_data  = rf_array[6] ;
    wire [31:0]  gpr_07_data  = rf_array[7] ;
    wire [31:0]  gpr_08_data  = rf_array[8] ;
    wire [31:0]  gpr_09_data  = rf_array[9] ;
    wire [31:0]  gpr_10_data  = rf_array[10] ;
    wire [31:0]  gpr_11_data  = rf_array[11] ;
    wire [31:0]  gpr_12_data  = rf_array[12] ;
    wire [31:0]  gpr_13_data  = rf_array[13] ;
    wire [31:0]  gpr_14_data  = rf_array[14] ;
    wire [31:0]  gpr_15_data  = rf_array[15] ;
    wire [31:0]  gpr_16_data  = rf_array[16] ;
    wire [31:0]  gpr_17_data  = rf_array[17] ;
    wire [31:0]  gpr_18_data  = rf_array[18] ;
    wire [31:0]  gpr_19_data  = rf_array[19] ;
    wire [31:0]  gpr_20_data  = rf_array[20] ;
    wire [31:0]  gpr_21_data  = rf_array[21] ;
    wire [31:0]  gpr_22_data  = rf_array[22] ;
    wire [31:0]  gpr_23_data  = rf_array[23] ;
    wire [31:0]  gpr_24_data  = rf_array[24] ;
    wire [31:0]  gpr_25_data  = rf_array[25] ;
    wire [31:0]  gpr_26_data  = rf_array[26] ;
    wire [31:0]  gpr_27_data  = rf_array[27] ;
    wire [31:0]  gpr_28_data  = rf_array[28] ;
    wire [31:0]  gpr_29_data  = rf_array[29] ;
    wire [31:0]  gpr_30_data  = rf_array[30] ;
    wire [31:0]  gpr_31_data  = rf_array[31] ;
    // synopsys on

    wire [31:0]  rf_array0 = rf_array[0];

    // rst port
    always @(posedge st_rst)
    begin
      	reset_arrayst;
    end

	task	shf_array1;
	reg	[31:0]	rf_array31;
	reg	[31:0]	rf_arrayi;
	reg	[31:0]	rf_arrayi1;
	integer     i;
	begin
			rf_array31   = rf_array[31];
			rf_array[31] = {test_si1, rf_array31[31:1]};
			for (i=1; i<16; i=i+1)
			begin
				rf_arrayi1  = rf_array[31-i+1];
				rf_arrayi   = rf_array[31-i];
				rf_array[31-i] = {rf_arrayi1[0],rf_arrayi[31:1]};
			end
	end
	endtask

	task	shf_array0;
	reg	[31:0]	rf_array15;
	reg	[31:0]	rf_arrayi;
	reg	[31:0]	rf_arrayi1;
	integer     i;
	begin
			rf_array15   = rf_array[15];
			rf_array[15] = {test_si0, rf_array15[31:1]};
			for (i=17; i<32; i=i+1)
			begin
				rf_arrayi1  = rf_array[31-i+1];
				rf_arrayi   = rf_array[31-i];
				rf_array[31-i] = {rf_arrayi1[0],rf_arrayi[31:1]};
			end
	end
	endtask

    task 	shf_arrayst;
    reg	[4:0]	rf_arrayst31;
	reg	[4:0]	rf_arraysti;
	reg	[4:0]	rf_arraysti1;
	integer     i;
	begin
			rf_arrayst31   = {rf_arraysth[31], rf_arraystl[31]};
			{rf_arraysth[31], rf_arraystl[31]} = {rf_array0[0], rf_arrayst31[4:1]};
			for (i=1; i<32; i=i+1)
			begin
				rf_arraysti1  = {rf_arraysth[31-i+1], rf_arraystl[31-i+1]};
				rf_arraysti   = {rf_arraysth[31-i], rf_arraystl[31-i]};
				{rf_arraysth[31-i], rf_arraystl[31-i]} = {rf_arraysti1[0],rf_arraysti[4:1]};
			end
	end
	endtask

    task reset_arrayst;
    integer  i;
    begin
     	for (i=0; i<32; i=i+1)
     	   begin
     	   	rf_arraystl[i] = 3'b0 ;
            rf_arraysth[i] = 2'b0 ;
           end
    end
    endtask

/////////////////////////////////////////////////////////////////////////
 endmodule
