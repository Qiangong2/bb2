/*************************************************************
*
*   Module      :   jtlb_core.v
*   Function    :   jtlb core logic block (with RAM and CAM).
*   Rev History :
*   		2001/06/08    :   Created by zacky
*
******************************************************************/
`celldefine
module	JTLB6304 (
          // output
          ENTRY_OUT,
          ENTRY_DATA,
          SEL_ODD,
          CORE_HIT_INDEX,
          CORE_HIT,
          CORE_MULTIHIT,
          // input
          CORE_VPNIN,
          CORE_TLBRD,
          CORE_TLBWR,
          CORE_RW_INDEX,
          CORE_WR_DATA,
          CORE_TLBP,
          CORE_TLBID,
          BIST_HOLD,
          BIST_DIAG,
          SCAN_MODE,
          CLK,
          CS,
          //FRE,
          // for scan
          SO,
          SE,
          BIST_SO
		);
 output	[24:0]	ENTRY_OUT;
 output	[89:0]	ENTRY_DATA;
 output		SEL_ODD;
 output	[4:0]	CORE_HIT_INDEX;
 output		CORE_HIT;
 output		CORE_MULTIHIT;

 input	[19:0]  CORE_VPNIN;
 input		CORE_TLBRD;
 input		CORE_TLBWR;
 input	[4:0]   CORE_RW_INDEX;
 input	[89:0]  CORE_WR_DATA;
 input		CORE_TLBP;
 input		CORE_TLBID;
 input      BIST_HOLD;
 input      CS;
 input		BIST_DIAG;
 input		CLK;
 input		SCAN_MODE;
 //input          FRE;

 output		SO;
 input		SE;
 input		BIST_SO;

 parameter    udly = 1;

 wire		jtlb_hit;
 wire	[4:0]	jtlb_hit_index;
 wire		jtlb_multihit;
 wire	[31:0]	tlbr_vec;
 wire   [31:0]	tlbw_vec;



 wire	[89:0]	jtlb_data_nml;
 wire	[24:0]	jtlb_out_nml;
 wire   [31:0]  odd_vec;


 wire		core_sel;
 wire		tlb_rd_nml;
 wire		tlb_wr_nml;
 wire		tlb_p_nml;
 wire		tlb_id_nml;
 wire	[4:0]	tlb_rw_index_nml;
 wire	[19:0]	vpn_in_nml;
 wire	[7:0]	cp0_asid_nml;
 wire	[89:0]	data_wr_nml;

 wire		tlb_rd_test;
 wire		tlb_wr_test;
 wire		tlb_p_test;
 wire		tlb_id_test;
 wire	[4:0]	tlb_rw_index_test;
 wire	[19:0]	vpn_in_test;
 wire	[7:0]	cp0_asid_test;
 wire	[89:0]	data_wr_test;
 wire	[24:0]	entry_out_test;
 wire	[89:0]	entry_data_test;
 wire		sel_odd_test;
 wire		hit_test;
 wire		multihit_test;
 wire	[4:0]	hit_index_test;
 wire   [89:0]  ENTRY_DATA;
 wire   [24:0]  ENTRY_OUT;
 wire           SEL_ODD;
 wire           CORE_HIT;
 wire   [4:0]   CORE_HIT_INDEX;
 wire           CORE_MULTIHIT;
 wire           SO;
 wire	[24:0]	jtlb_out;
 wire	[89:0]	jtlb_data;
 wire	[4:0]	hit_index_nml;
 wire	[31:0]	hit_vec_nml;
 wire		hit_nml = | hit_vec_nml;
 wire	[31:0]	odd_vec_nml;
 wire		sel_odd_nml = | odd_vec_nml;
 wire		multihit_nml;
 wire	[4:0]	address;
 wire		addr_en;
 wire	[31:0]	addr_vec;
 wire           jtlb_odd;

 wire 	[19:0]  CORE_VPNINi;
 wire 		CORE_TLBRDi;
 wire 		CORE_TLBWRi;
 wire 	[4:0]   CORE_RW_INDEXi;
 wire 	[89:0]  CORE_WR_DATAi;
 wire 		CORE_TLBPi;
 wire 		CORE_TLBIDi;
 wire       BIST_HOLDi;
 wire       CSi;
 wire 		BIST_DIAGi;
 wire 		CLK;
 wire 		SCAN_MODEi;
 //wire           FREi;

 wire 		SEi;
 wire 		BIST_SOi;

 reg		tlb_rd;
 reg		tlb_wr;
 reg		tlb_p;
 reg		tlb_id;
 reg	[4:0]	tlb_rw_index;
 reg        bist_hold_lch;

   always@ (posedge CLK)
    bist_hold_lch   <= #udly BIST_HOLDi;

   always@ (posedge CLK)
   if (~BIST_HOLDi & ~SEi)	begin		// control signal latch
      if (~CSi) begin
   		tlb_rd		<= #udly CORE_TLBRDi;
   		tlb_wr		<= #udly CORE_TLBWRi;
   		tlb_p		<= #udly CORE_TLBPi;
   		tlb_id		<= #udly CORE_TLBIDi;
   		tlb_rw_index<= #udly CORE_RW_INDEXi;
      end
   	end	// ~bist_hold & ~SE
   else	if (BIST_DIAGi | SEi)	begin
		tlb_rd		<= #udly BIST_SOi;
		tlb_wr		<= #udly tlb_rd;
		tlb_p		<= #udly tlb_wr;
		tlb_id		<= #udly tlb_p;
		tlb_rw_index<= #udly {tlb_id, tlb_rw_index[4:1]};
		end

   // enable signals
   wire		vpn_en			= CORE_TLBPi | CORE_TLBIDi;
   wire		data_wr_en		= CORE_TLBWRi;
   wire		entry_out_en	= tlb_id & jtlb_hit & ~jtlb_multihit;
   wire		entry_data_en	= tlb_id & jtlb_hit & ~jtlb_multihit | tlb_rd;
   wire		odd_en			= tlb_id & jtlb_hit & ~jtlb_multihit;
   wire		hit_en			= tlb_p | tlb_id;
   wire		hit_index_en	= tlb_p | tlb_id;

   // input latch
   reg	[19:0]	vpn_in;
   reg	[7:0]	cp0_asid;
   always@ (posedge CLK)
	if (~BIST_HOLDi & ~SEi)	begin
      if (~CSi)   begin
	 	if (vpn_en)		begin
   			vpn_in	<= #udly CORE_VPNINi;
   			cp0_asid<= #udly CORE_WR_DATAi[58:51];
   		end
      end
   	end	// ~bist_hold & ~SE
	else if (BIST_DIAGi | SEi)	begin
		vpn_in	<= #udly {tlb_rw_index[0], vpn_in[19:1]};
		cp0_asid<= #udly {vpn_in[0],cp0_asid[7:1]};
		end


   reg		[89:0]	data_wr;
   always@ (posedge CLK)
	if (~BIST_HOLDi & ~SEi)	begin
      if (~CSi) begin
   		if (data_wr_en)
   			data_wr		<= #udly CORE_WR_DATAi;
      end
   	end	// ~bist_hold & ~SE
	else if (BIST_DIAGi | SEi)
		data_wr		<= #udly {cp0_asid[0], data_wr[89:1]};


   // output latch
   reg		[24:0]	ENTRY_OUTi;
   always@ (posedge CLK)
	if (~BIST_HOLDi & ~SEi)	begin
      if (~CSi) begin
   		if (entry_out_en)
   			ENTRY_OUTi	<= #udly jtlb_out;
      end
   	end	// ~bist_hold & ~SE
	else if (BIST_DIAGi | SEi)
		ENTRY_OUTi	<= #udly {data_wr[0], ENTRY_OUTi[24:1]};


   reg		[89:0]	ENTRY_DATAi;
   always@ (posedge CLK)
	if (~BIST_HOLDi & ~SEi)	begin
      if (~CSi) begin
   		if (entry_data_en)
   			ENTRY_DATAi	<= #udly jtlb_data;
      end
   	end	// ~bist_hold & ~SE
	else if (BIST_DIAGi | SEi)
		ENTRY_DATAi	<= #udly {ENTRY_OUTi[0], ENTRY_DATAi[89:1]};


   reg				SEL_ODDi;
   always@ (posedge CLK)
	if (~BIST_HOLDi & ~SEi)	begin
      if (~CSi) begin
   		if (odd_en)
   			SEL_ODDi		<= #udly jtlb_odd;
      end
   	end	// ~bist_hold & ~SE
	else if (BIST_DIAGi | SEi)
		SEL_ODDi		<= #udly ENTRY_DATAi[0];


   reg				CORE_HITi;
   always@ (posedge CLK)
	if (~BIST_HOLDi & ~SEi)	begin
      if (~CSi) begin
   		if (hit_en)
   			CORE_HITi	<= #udly jtlb_hit;
      end
   	end	// ~bist_hold & ~SE
	else if (BIST_DIAGi | SEi)
		CORE_HITi	<= #udly SEL_ODDi;


   reg				CORE_MULTIHITi;
   always@ (posedge CLK)
	if (~BIST_HOLDi & ~SEi)	begin
      if (~CSi) begin
   		if (hit_en)
   			CORE_MULTIHITi	<= #udly jtlb_multihit;
      end
   	end	// ~bist_hold & ~SE
	else if (BIST_DIAGi | SEi)
		CORE_MULTIHITi	<= #udly CORE_HITi;


   reg		[4:0]	CORE_HIT_INDEXi;
   always@ (posedge CLK)
	if (~BIST_HOLDi & ~SEi)	begin
      if (~CSi) begin
   		if (hit_index_en)
   			CORE_HIT_INDEXi	<= #udly jtlb_hit_index;
      end
   	end	// ~bist_hold & ~SE
	else if (BIST_DIAGi | SEi)
		CORE_HIT_INDEXi	<= #udly {CORE_MULTIHITi, CORE_HIT_INDEXi[4:1]};
   wire		SOi = CORE_HIT_INDEXi[0];


   assign	core_sel	= ~bist_hold_lch & ~SCAN_MODEi;
   assign	tlb_rd_nml	= tlb_rd & core_sel;
   assign	tlb_wr_nml	= tlb_wr & core_sel;
   assign	tlb_p_nml	= tlb_p & core_sel;
   assign	tlb_id_nml	= tlb_id & core_sel;
   assign	tlb_rw_index_nml    = tlb_rw_index & {5{core_sel}};
   assign	vpn_in_nml	= vpn_in & {20{core_sel}};
   assign	cp0_asid_nml	= cp0_asid & {8{core_sel}};
   assign	data_wr_nml	= data_wr & {90{core_sel}};

   assign	tlb_rd_test	= tlb_rd & SCAN_MODEi;
   assign	tlb_wr_test	= tlb_wr & SCAN_MODEi;
   assign	tlb_p_test	= tlb_p & SCAN_MODEi;
   assign	tlb_id_test	= tlb_id & SCAN_MODEi;
   assign	tlb_rw_index_test	= tlb_rw_index & {5{SCAN_MODEi}};
   assign	vpn_in_test	= vpn_in & {20{SCAN_MODEi}};
   assign	cp0_asid_test	= cp0_asid & {8{SCAN_MODEi}};
   assign	data_wr_test	= data_wr & {90{SCAN_MODEi}};

   assign	entry_out_test	= {vpn_in_test,tlb_rd_test,tlb_wr_test,tlb_p_test,tlb_id_test,cp0_asid_test[0]^cp0_asid_test[1]};
   assign	entry_data_test	= data_wr_test;
   assign	sel_odd_test	= cp0_asid_test[2] ^ cp0_asid_test[3];
   assign	hit_test	= cp0_asid_test[4] ^ cp0_asid_test[5];
   assign	multihit_test	= cp0_asid_test[6] ^ cp0_asid_test[7];
   assign	hit_index_test	= tlb_rw_index_test;

   assign	jtlb_out	= SCAN_MODEi ? entry_out_test : jtlb_out_nml;
   assign	jtlb_data	= SCAN_MODEi ? entry_data_test : jtlb_data_nml;
   assign	jtlb_odd	= SCAN_MODEi ? sel_odd_test : sel_odd_nml;
   assign	jtlb_hit	= SCAN_MODEi ? hit_test : hit_nml;
   assign	jtlb_multihit	= SCAN_MODEi ? multihit_test : multihit_nml;
   assign	jtlb_hit_index	= SCAN_MODEi ? hit_index_test : hit_index_nml;

   assign	address = tlb_rw_index_nml;
   assign	addr_en = tlb_wr | tlb_rd;

cpu_encoder		cpu_encoder			(
		.index_out			( hit_index_nml			),
		.vec_in				( hit_vec_nml			));
decoder		decoder_wr		(
		.vec_out			( addr_vec			),
		.index_in			( address			),
		.en				( addr_en			));

tlb_sd		tlb_sd			(
		.multi_hit			( multihit_nml			),
		.hit_vec			( hit_vec_nml			));


jtlb_cam	jtlb_cam	(
	.cam_data_out				( jtlb_data_nml[89:50]		),
	.hit_vec				( hit_vec_nml			),
	.odd_vec				( odd_vec_nml			),
	.cam_data_in				( data_wr_nml[89:50]		),
	.vpn					( vpn_in_nml			),
	.cp0_asid				( cp0_asid_nml			),
	.tlbwr					( tlb_wr_nml			),
	.tlbrd					( tlb_rd_nml			),
	.tlbid					( tlb_id_nml			),
	.tlbp					( tlb_p_nml			),
	.addr_vec				( addr_vec			),
	.clk					( CLK				));

jtlb_ram	jtlb_ram	(
	.ram_data				( jtlb_data_nml[49:0]		),
	.jtlb_out				( jtlb_out_nml			),
	.ram_data_in				( data_wr_nml[49:0]		),
	.pagemask				( data_wr_nml[89:78]		),
	.tlbwr					( tlb_wr_nml			),
	.tlbrd					( tlb_rd_nml			),
	.tlbid					( tlb_id_nml			),
	.vpn					( vpn_in_nml			),
	.odd_vec				( odd_vec_nml			),
	.addr_vec				( addr_vec			),
	.hit_vec				( hit_vec_nml			),
	.clk					( CLK				));


// declare timing violation notifiers
reg             NOT_CLKH;
reg             NOT_CLKL;
reg             NOT_CS;
reg             NOT_CORE_VPNIN_0,  NOT_CORE_VPNIN_1,  NOT_CORE_VPNIN_2,  NOT_CORE_VPNIN_3,  NOT_CORE_VPNIN_4,
                NOT_CORE_VPNIN_5,  NOT_CORE_VPNIN_6,  NOT_CORE_VPNIN_7,  NOT_CORE_VPNIN_8,  NOT_CORE_VPNIN_9,
                NOT_CORE_VPNIN_10, NOT_CORE_VPNIN_11, NOT_CORE_VPNIN_12, NOT_CORE_VPNIN_13, NOT_CORE_VPNIN_14,
                NOT_CORE_VPNIN_15, NOT_CORE_VPNIN_16, NOT_CORE_VPNIN_17, NOT_CORE_VPNIN_18, NOT_CORE_VPNIN_19;
reg             NOT_CORE_TLBRD;
reg             NOT_CORE_TLBWR;
reg             NOT_CORE_RW_INDEX_0, NOT_CORE_RW_INDEX_1, NOT_CORE_RW_INDEX_2, NOT_CORE_RW_INDEX_3, NOT_CORE_RW_INDEX_4;
reg             NOT_CORE_WR_DATA_0,  NOT_CORE_WR_DATA_1,  NOT_CORE_WR_DATA_2,  NOT_CORE_WR_DATA_3,  NOT_CORE_WR_DATA_4,
                NOT_CORE_WR_DATA_5,  NOT_CORE_WR_DATA_6,  NOT_CORE_WR_DATA_7,  NOT_CORE_WR_DATA_8,  NOT_CORE_WR_DATA_9,
                NOT_CORE_WR_DATA_10, NOT_CORE_WR_DATA_11, NOT_CORE_WR_DATA_12, NOT_CORE_WR_DATA_13, NOT_CORE_WR_DATA_14,
                NOT_CORE_WR_DATA_15, NOT_CORE_WR_DATA_16, NOT_CORE_WR_DATA_17, NOT_CORE_WR_DATA_18, NOT_CORE_WR_DATA_19,
                NOT_CORE_WR_DATA_20, NOT_CORE_WR_DATA_21, NOT_CORE_WR_DATA_22, NOT_CORE_WR_DATA_23, NOT_CORE_WR_DATA_24,
                NOT_CORE_WR_DATA_25, NOT_CORE_WR_DATA_26, NOT_CORE_WR_DATA_27, NOT_CORE_WR_DATA_28, NOT_CORE_WR_DATA_29,
                NOT_CORE_WR_DATA_30, NOT_CORE_WR_DATA_31, NOT_CORE_WR_DATA_32, NOT_CORE_WR_DATA_33, NOT_CORE_WR_DATA_34,
                NOT_CORE_WR_DATA_35, NOT_CORE_WR_DATA_36, NOT_CORE_WR_DATA_37, NOT_CORE_WR_DATA_38, NOT_CORE_WR_DATA_39,
                NOT_CORE_WR_DATA_40, NOT_CORE_WR_DATA_41, NOT_CORE_WR_DATA_42, NOT_CORE_WR_DATA_43, NOT_CORE_WR_DATA_44,
                NOT_CORE_WR_DATA_45, NOT_CORE_WR_DATA_46, NOT_CORE_WR_DATA_47, NOT_CORE_WR_DATA_48, NOT_CORE_WR_DATA_49,
                NOT_CORE_WR_DATA_50, NOT_CORE_WR_DATA_51, NOT_CORE_WR_DATA_52, NOT_CORE_WR_DATA_53, NOT_CORE_WR_DATA_54,
                NOT_CORE_WR_DATA_55, NOT_CORE_WR_DATA_56, NOT_CORE_WR_DATA_57, NOT_CORE_WR_DATA_58, NOT_CORE_WR_DATA_59,
                NOT_CORE_WR_DATA_60, NOT_CORE_WR_DATA_61, NOT_CORE_WR_DATA_62, NOT_CORE_WR_DATA_63, NOT_CORE_WR_DATA_64,
                NOT_CORE_WR_DATA_65, NOT_CORE_WR_DATA_66, NOT_CORE_WR_DATA_67, NOT_CORE_WR_DATA_68, NOT_CORE_WR_DATA_69,
                NOT_CORE_WR_DATA_70, NOT_CORE_WR_DATA_71, NOT_CORE_WR_DATA_72, NOT_CORE_WR_DATA_73, NOT_CORE_WR_DATA_74,
                NOT_CORE_WR_DATA_75, NOT_CORE_WR_DATA_76, NOT_CORE_WR_DATA_77, NOT_CORE_WR_DATA_78, NOT_CORE_WR_DATA_79,
                NOT_CORE_WR_DATA_80, NOT_CORE_WR_DATA_81, NOT_CORE_WR_DATA_82, NOT_CORE_WR_DATA_83, NOT_CORE_WR_DATA_84,
                NOT_CORE_WR_DATA_85, NOT_CORE_WR_DATA_86, NOT_CORE_WR_DATA_87, NOT_CORE_WR_DATA_88, NOT_CORE_WR_DATA_89;
reg             NOT_CORE_TLBP;
reg             NOT_CORE_TLBID;
reg             NOT_BIST_HOLD;
reg             NOT_BIST_DIAG;
reg             NOT_SCAN_MODE;
//reg             NOT_FRE;
reg             NOT_BIST_SO;
reg             NOT_SE;

reg             cs_vio, core_vpnin_vio, core_tlbrd_vio, core_tlbwr_vio, core_rw_index_vio, core_wr_data_vio,
                core_tlbp_vio, core_tlbid_vio, bist_hold_vio, bist_diag_vio, scan_mode_vio, bist_so_vio, se_vio;

wire    [24:0]  ENTRY_OUTiw     = ENTRY_OUTi;
wire    [89:0]  ENTRY_DATAiw    = ENTRY_DATAi;
wire            SEL_ODDiw       = SEL_ODDi;
wire    [4:0]   CORE_HIT_INDEXiw= CORE_HIT_INDEXi;
wire            CORE_HITiw      = CORE_HITi;
wire            CORE_MULTIHITiw = CORE_MULTIHITi;

buf             (ENTRY_OUT[0],  ENTRY_OUTiw[0]);
buf             (ENTRY_OUT[1],  ENTRY_OUTiw[1]);
buf             (ENTRY_OUT[2],  ENTRY_OUTiw[2]);
buf             (ENTRY_OUT[3],  ENTRY_OUTiw[3]);
buf             (ENTRY_OUT[4],  ENTRY_OUTiw[4]);
buf             (ENTRY_OUT[5],  ENTRY_OUTiw[5]);
buf             (ENTRY_OUT[6],  ENTRY_OUTiw[6]);
buf             (ENTRY_OUT[7],  ENTRY_OUTiw[7]);
buf             (ENTRY_OUT[8],  ENTRY_OUTiw[8]);
buf             (ENTRY_OUT[9],  ENTRY_OUTiw[9]);
buf             (ENTRY_OUT[10], ENTRY_OUTiw[10]);
buf             (ENTRY_OUT[11], ENTRY_OUTiw[11]);
buf             (ENTRY_OUT[12], ENTRY_OUTiw[12]);
buf             (ENTRY_OUT[13], ENTRY_OUTiw[13]);
buf             (ENTRY_OUT[14], ENTRY_OUTiw[14]);
buf             (ENTRY_OUT[15], ENTRY_OUTiw[15]);
buf             (ENTRY_OUT[16], ENTRY_OUTiw[16]);
buf             (ENTRY_OUT[17], ENTRY_OUTiw[17]);
buf             (ENTRY_OUT[18], ENTRY_OUTiw[18]);
buf             (ENTRY_OUT[19], ENTRY_OUTiw[19]);
buf             (ENTRY_OUT[20], ENTRY_OUTiw[20]);
buf             (ENTRY_OUT[21], ENTRY_OUTiw[21]);
buf             (ENTRY_OUT[22], ENTRY_OUTiw[22]);
buf             (ENTRY_OUT[23], ENTRY_OUTiw[23]);
buf             (ENTRY_OUT[24], ENTRY_OUTiw[24]);

buf             (ENTRY_DATA[0],  ENTRY_DATAiw[0]);
buf             (ENTRY_DATA[1],  ENTRY_DATAiw[1]);
buf             (ENTRY_DATA[2],  ENTRY_DATAiw[2]);
buf             (ENTRY_DATA[3],  ENTRY_DATAiw[3]);
buf             (ENTRY_DATA[4],  ENTRY_DATAiw[4]);
buf             (ENTRY_DATA[5],  ENTRY_DATAiw[5]);
buf             (ENTRY_DATA[6],  ENTRY_DATAiw[6]);
buf             (ENTRY_DATA[7],  ENTRY_DATAiw[7]);
buf             (ENTRY_DATA[8],  ENTRY_DATAiw[8]);
buf             (ENTRY_DATA[9],  ENTRY_DATAiw[9]);
buf             (ENTRY_DATA[10], ENTRY_DATAiw[10]);
buf             (ENTRY_DATA[11], ENTRY_DATAiw[11]);
buf             (ENTRY_DATA[12], ENTRY_DATAiw[12]);
buf             (ENTRY_DATA[13], ENTRY_DATAiw[13]);
buf             (ENTRY_DATA[14], ENTRY_DATAiw[14]);
buf             (ENTRY_DATA[15], ENTRY_DATAiw[15]);
buf             (ENTRY_DATA[16], ENTRY_DATAiw[16]);
buf             (ENTRY_DATA[17], ENTRY_DATAiw[17]);
buf             (ENTRY_DATA[18], ENTRY_DATAiw[18]);
buf             (ENTRY_DATA[19], ENTRY_DATAiw[19]);
buf             (ENTRY_DATA[20], ENTRY_DATAiw[20]);
buf             (ENTRY_DATA[21], ENTRY_DATAiw[21]);
buf             (ENTRY_DATA[22], ENTRY_DATAiw[22]);
buf             (ENTRY_DATA[23], ENTRY_DATAiw[23]);
buf             (ENTRY_DATA[24], ENTRY_DATAiw[24]);
buf             (ENTRY_DATA[25], ENTRY_DATAiw[25]);
buf             (ENTRY_DATA[26], ENTRY_DATAiw[26]);
buf             (ENTRY_DATA[27], ENTRY_DATAiw[27]);
buf             (ENTRY_DATA[28], ENTRY_DATAiw[28]);
buf             (ENTRY_DATA[29], ENTRY_DATAiw[29]);
buf             (ENTRY_DATA[30], ENTRY_DATAiw[30]);
buf             (ENTRY_DATA[31], ENTRY_DATAiw[31]);
buf             (ENTRY_DATA[32], ENTRY_DATAiw[32]);
buf             (ENTRY_DATA[33], ENTRY_DATAiw[33]);
buf             (ENTRY_DATA[34], ENTRY_DATAiw[34]);
buf             (ENTRY_DATA[35], ENTRY_DATAiw[35]);
buf             (ENTRY_DATA[36], ENTRY_DATAiw[36]);
buf             (ENTRY_DATA[37], ENTRY_DATAiw[37]);
buf             (ENTRY_DATA[38], ENTRY_DATAiw[38]);
buf             (ENTRY_DATA[39], ENTRY_DATAiw[39]);
buf             (ENTRY_DATA[40], ENTRY_DATAiw[40]);
buf             (ENTRY_DATA[41], ENTRY_DATAiw[41]);
buf             (ENTRY_DATA[42], ENTRY_DATAiw[42]);
buf             (ENTRY_DATA[43], ENTRY_DATAiw[43]);
buf             (ENTRY_DATA[44], ENTRY_DATAiw[44]);
buf             (ENTRY_DATA[45], ENTRY_DATAiw[45]);
buf             (ENTRY_DATA[46], ENTRY_DATAiw[46]);
buf             (ENTRY_DATA[47], ENTRY_DATAiw[47]);
buf             (ENTRY_DATA[48], ENTRY_DATAiw[48]);
buf             (ENTRY_DATA[49], ENTRY_DATAiw[49]);
buf             (ENTRY_DATA[50], ENTRY_DATAiw[50]);
buf             (ENTRY_DATA[51], ENTRY_DATAiw[51]);
buf             (ENTRY_DATA[52], ENTRY_DATAiw[52]);
buf             (ENTRY_DATA[53], ENTRY_DATAiw[53]);
buf             (ENTRY_DATA[54], ENTRY_DATAiw[54]);
buf             (ENTRY_DATA[55], ENTRY_DATAiw[55]);
buf             (ENTRY_DATA[56], ENTRY_DATAiw[56]);
buf             (ENTRY_DATA[57], ENTRY_DATAiw[57]);
buf             (ENTRY_DATA[58], ENTRY_DATAiw[58]);
buf             (ENTRY_DATA[59], ENTRY_DATAiw[59]);
buf             (ENTRY_DATA[60], ENTRY_DATAiw[60]);
buf             (ENTRY_DATA[61], ENTRY_DATAiw[61]);
buf             (ENTRY_DATA[62], ENTRY_DATAiw[62]);
buf             (ENTRY_DATA[63], ENTRY_DATAiw[63]);
buf             (ENTRY_DATA[64], ENTRY_DATAiw[64]);
buf             (ENTRY_DATA[65], ENTRY_DATAiw[65]);
buf             (ENTRY_DATA[66], ENTRY_DATAiw[66]);
buf             (ENTRY_DATA[67], ENTRY_DATAiw[67]);
buf             (ENTRY_DATA[68], ENTRY_DATAiw[68]);
buf             (ENTRY_DATA[69], ENTRY_DATAiw[69]);
buf             (ENTRY_DATA[70], ENTRY_DATAiw[70]);
buf             (ENTRY_DATA[71], ENTRY_DATAiw[71]);
buf             (ENTRY_DATA[72], ENTRY_DATAiw[72]);
buf             (ENTRY_DATA[73], ENTRY_DATAiw[73]);
buf             (ENTRY_DATA[74], ENTRY_DATAiw[74]);
buf             (ENTRY_DATA[75], ENTRY_DATAiw[75]);
buf             (ENTRY_DATA[76], ENTRY_DATAiw[76]);
buf             (ENTRY_DATA[77], ENTRY_DATAiw[77]);
buf             (ENTRY_DATA[78], ENTRY_DATAiw[78]);
buf             (ENTRY_DATA[79], ENTRY_DATAiw[79]);
buf             (ENTRY_DATA[80], ENTRY_DATAiw[80]);
buf             (ENTRY_DATA[81], ENTRY_DATAiw[81]);
buf             (ENTRY_DATA[82], ENTRY_DATAiw[82]);
buf             (ENTRY_DATA[83], ENTRY_DATAiw[83]);
buf             (ENTRY_DATA[84], ENTRY_DATAiw[84]);
buf             (ENTRY_DATA[85], ENTRY_DATAiw[85]);
buf             (ENTRY_DATA[86], ENTRY_DATAiw[86]);
buf             (ENTRY_DATA[87], ENTRY_DATAiw[87]);
buf             (ENTRY_DATA[88], ENTRY_DATAiw[88]);
buf             (ENTRY_DATA[89], ENTRY_DATAiw[89]);

buf             (SEL_ODD,        SEL_ODDiw);
buf             (CORE_HIT,       CORE_HITiw);
buf             (CORE_HIT_INDEX[0], CORE_HIT_INDEXiw[0]);
buf             (CORE_HIT_INDEX[1], CORE_HIT_INDEXiw[1]);
buf             (CORE_HIT_INDEX[2], CORE_HIT_INDEXiw[2]);
buf             (CORE_HIT_INDEX[3], CORE_HIT_INDEXiw[3]);
buf             (CORE_HIT_INDEX[4], CORE_HIT_INDEXiw[4]);
buf             (CORE_MULTIHIT,     CORE_MULTIHITiw);

buf             (SO,                SOi);
// input buffers
buf             (CORE_VPNINi[0], CORE_VPNIN[0]);
buf             (CORE_VPNINi[1], CORE_VPNIN[1]);
buf             (CORE_VPNINi[2], CORE_VPNIN[2]);
buf             (CORE_VPNINi[3], CORE_VPNIN[3]);
buf             (CORE_VPNINi[4], CORE_VPNIN[4]);
buf             (CORE_VPNINi[5], CORE_VPNIN[5]);
buf             (CORE_VPNINi[6], CORE_VPNIN[6]);
buf             (CORE_VPNINi[7], CORE_VPNIN[7]);
buf             (CORE_VPNINi[8], CORE_VPNIN[8]);
buf             (CORE_VPNINi[9], CORE_VPNIN[9]);
buf             (CORE_VPNINi[10], CORE_VPNIN[10]);
buf             (CORE_VPNINi[11], CORE_VPNIN[11]);
buf             (CORE_VPNINi[12], CORE_VPNIN[12]);
buf             (CORE_VPNINi[13], CORE_VPNIN[13]);
buf             (CORE_VPNINi[14], CORE_VPNIN[14]);
buf             (CORE_VPNINi[15], CORE_VPNIN[15]);
buf             (CORE_VPNINi[16], CORE_VPNIN[16]);
buf             (CORE_VPNINi[17], CORE_VPNIN[17]);
buf             (CORE_VPNINi[18], CORE_VPNIN[18]);
buf             (CORE_VPNINi[19], CORE_VPNIN[19]);

buf             (CORE_TLBRDi,   CORE_TLBRD);
buf             (CORE_TLBWRi,   CORE_TLBWR);

buf             (CORE_RW_INDEXi[0], CORE_RW_INDEX[0]);
buf             (CORE_RW_INDEXi[1], CORE_RW_INDEX[1]);
buf             (CORE_RW_INDEXi[2], CORE_RW_INDEX[2]);
buf             (CORE_RW_INDEXi[3], CORE_RW_INDEX[3]);
buf             (CORE_RW_INDEXi[4], CORE_RW_INDEX[4]);

buf             (CORE_WR_DATAi[0],  CORE_WR_DATA[0]);
buf             (CORE_WR_DATAi[1],  CORE_WR_DATA[1]);
buf             (CORE_WR_DATAi[2],  CORE_WR_DATA[2]);
buf             (CORE_WR_DATAi[3],  CORE_WR_DATA[3]);
buf             (CORE_WR_DATAi[4],  CORE_WR_DATA[4]);
buf             (CORE_WR_DATAi[5],  CORE_WR_DATA[5]);
buf             (CORE_WR_DATAi[6],  CORE_WR_DATA[6]);
buf             (CORE_WR_DATAi[7],  CORE_WR_DATA[7]);
buf             (CORE_WR_DATAi[8],  CORE_WR_DATA[8]);
buf             (CORE_WR_DATAi[9],  CORE_WR_DATA[9]);
buf             (CORE_WR_DATAi[10], CORE_WR_DATA[10]);
buf             (CORE_WR_DATAi[11], CORE_WR_DATA[11]);
buf             (CORE_WR_DATAi[12], CORE_WR_DATA[12]);
buf             (CORE_WR_DATAi[13], CORE_WR_DATA[13]);
buf             (CORE_WR_DATAi[14], CORE_WR_DATA[14]);
buf             (CORE_WR_DATAi[15], CORE_WR_DATA[15]);
buf             (CORE_WR_DATAi[16], CORE_WR_DATA[16]);
buf             (CORE_WR_DATAi[17], CORE_WR_DATA[17]);
buf             (CORE_WR_DATAi[18], CORE_WR_DATA[18]);
buf             (CORE_WR_DATAi[19], CORE_WR_DATA[19]);
buf             (CORE_WR_DATAi[20], CORE_WR_DATA[20]);
buf             (CORE_WR_DATAi[21], CORE_WR_DATA[21]);
buf             (CORE_WR_DATAi[22], CORE_WR_DATA[22]);
buf             (CORE_WR_DATAi[23], CORE_WR_DATA[23]);
buf             (CORE_WR_DATAi[24], CORE_WR_DATA[24]);
buf             (CORE_WR_DATAi[25], CORE_WR_DATA[25]);
buf             (CORE_WR_DATAi[26], CORE_WR_DATA[26]);
buf             (CORE_WR_DATAi[27], CORE_WR_DATA[27]);
buf             (CORE_WR_DATAi[28], CORE_WR_DATA[28]);
buf             (CORE_WR_DATAi[29], CORE_WR_DATA[29]);
buf             (CORE_WR_DATAi[30], CORE_WR_DATA[30]);
buf             (CORE_WR_DATAi[31], CORE_WR_DATA[31]);
buf             (CORE_WR_DATAi[32], CORE_WR_DATA[32]);
buf             (CORE_WR_DATAi[33], CORE_WR_DATA[33]);
buf             (CORE_WR_DATAi[34], CORE_WR_DATA[34]);
buf             (CORE_WR_DATAi[35], CORE_WR_DATA[35]);
buf             (CORE_WR_DATAi[36], CORE_WR_DATA[36]);
buf             (CORE_WR_DATAi[37], CORE_WR_DATA[37]);
buf             (CORE_WR_DATAi[38], CORE_WR_DATA[38]);
buf             (CORE_WR_DATAi[39], CORE_WR_DATA[39]);
buf             (CORE_WR_DATAi[40], CORE_WR_DATA[40]);
buf             (CORE_WR_DATAi[41], CORE_WR_DATA[41]);
buf             (CORE_WR_DATAi[42], CORE_WR_DATA[42]);
buf             (CORE_WR_DATAi[43], CORE_WR_DATA[43]);
buf             (CORE_WR_DATAi[44], CORE_WR_DATA[44]);
buf             (CORE_WR_DATAi[45], CORE_WR_DATA[45]);
buf             (CORE_WR_DATAi[46], CORE_WR_DATA[46]);
buf             (CORE_WR_DATAi[47], CORE_WR_DATA[47]);
buf             (CORE_WR_DATAi[48], CORE_WR_DATA[48]);
buf             (CORE_WR_DATAi[49], CORE_WR_DATA[49]);
buf             (CORE_WR_DATAi[50], CORE_WR_DATA[50]);
buf             (CORE_WR_DATAi[51], CORE_WR_DATA[51]);
buf             (CORE_WR_DATAi[52], CORE_WR_DATA[52]);
buf             (CORE_WR_DATAi[53], CORE_WR_DATA[53]);
buf             (CORE_WR_DATAi[54], CORE_WR_DATA[54]);
buf             (CORE_WR_DATAi[55], CORE_WR_DATA[55]);
buf             (CORE_WR_DATAi[56], CORE_WR_DATA[56]);
buf             (CORE_WR_DATAi[57], CORE_WR_DATA[57]);
buf             (CORE_WR_DATAi[58], CORE_WR_DATA[58]);
buf             (CORE_WR_DATAi[59], CORE_WR_DATA[59]);
buf             (CORE_WR_DATAi[60], CORE_WR_DATA[60]);
buf             (CORE_WR_DATAi[61], CORE_WR_DATA[61]);
buf             (CORE_WR_DATAi[62], CORE_WR_DATA[62]);
buf             (CORE_WR_DATAi[63], CORE_WR_DATA[63]);
buf             (CORE_WR_DATAi[64], CORE_WR_DATA[64]);
buf             (CORE_WR_DATAi[65], CORE_WR_DATA[65]);
buf             (CORE_WR_DATAi[66], CORE_WR_DATA[66]);
buf             (CORE_WR_DATAi[67], CORE_WR_DATA[67]);
buf             (CORE_WR_DATAi[68], CORE_WR_DATA[68]);
buf             (CORE_WR_DATAi[69], CORE_WR_DATA[69]);
buf             (CORE_WR_DATAi[70], CORE_WR_DATA[70]);
buf             (CORE_WR_DATAi[71], CORE_WR_DATA[71]);
buf             (CORE_WR_DATAi[72], CORE_WR_DATA[72]);
buf             (CORE_WR_DATAi[73], CORE_WR_DATA[73]);
buf             (CORE_WR_DATAi[74], CORE_WR_DATA[74]);
buf             (CORE_WR_DATAi[75], CORE_WR_DATA[75]);
buf             (CORE_WR_DATAi[76], CORE_WR_DATA[76]);
buf             (CORE_WR_DATAi[77], CORE_WR_DATA[77]);
buf             (CORE_WR_DATAi[78], CORE_WR_DATA[78]);
buf             (CORE_WR_DATAi[79], CORE_WR_DATA[79]);
buf             (CORE_WR_DATAi[80], CORE_WR_DATA[80]);
buf             (CORE_WR_DATAi[81], CORE_WR_DATA[81]);
buf             (CORE_WR_DATAi[82], CORE_WR_DATA[82]);
buf             (CORE_WR_DATAi[83], CORE_WR_DATA[83]);
buf             (CORE_WR_DATAi[84], CORE_WR_DATA[84]);
buf             (CORE_WR_DATAi[85], CORE_WR_DATA[85]);
buf             (CORE_WR_DATAi[86], CORE_WR_DATA[86]);
buf             (CORE_WR_DATAi[87], CORE_WR_DATA[87]);
buf             (CORE_WR_DATAi[88], CORE_WR_DATA[88]);
buf             (CORE_WR_DATAi[89], CORE_WR_DATA[89]);

buf             (CORE_TLBPi,    CORE_TLBP);
buf             (CORE_TLBIDi,   CORE_TLBID);

buf             (BIST_HOLDi,    BIST_HOLD);
buf             (BIST_DIAGi,    BIST_DIAG);
buf             (SCAN_MODEi,    SCAN_MODE);
//buf             (FREi,          FRE);
buf             (CSi,           CS);
buf             (SEi,           SE);
buf             (BIST_SOi,      BIST_SO);

wire            cs_flag     = ~CSi;
wire            wr_flag     = CORE_TLBWRi & ~CSi;
wire            rw_flag     = (CORE_TLBWRi | CORE_TLBRDi) & ~CSi;
wire            cm_flag     = (CORE_TLBRDi | CORE_TLBIDi) & ~CSi;
wire            asid_flag   = (CORE_TLBIDi | CORE_TLBPi) & ~CSi;
specify
        $width(posedge CLK, 1.000,0, NOT_CLKH);
        $width(negedge CLK, 1.000,0, NOT_CLKL);
        $setuphold(posedge CLK, posedge CS, 1.000, 1.000, NOT_CS);
        $setuphold(posedge CLK, negedge CS, 1.000, 1.000, NOT_CS);
        $setuphold(posedge CLK, CS, 1.000, 1.000, NOT_CS);
        $setuphold(posedge CLK &&& cs_flag, posedge CORE_TLBWR, 1.000, 1.000, NOT_CORE_TLBWR);
        $setuphold(posedge CLK &&& cs_flag, negedge CORE_TLBWR, 1.000, 1.000, NOT_CORE_TLBWR);
        $setuphold(posedge CLK &&& cs_flag, posedge CORE_TLBRD, 1.000, 1.000, NOT_CORE_TLBRD);
        $setuphold(posedge CLK &&& cs_flag, negedge CORE_TLBRD, 1.000, 1.000, NOT_CORE_TLBRD);
        $setuphold(posedge CLK &&& cs_flag, posedge CORE_TLBID, 1.000, 1.000, NOT_CORE_TLBID);
        $setuphold(posedge CLK &&& cs_flag, negedge CORE_TLBID, 1.000, 1.000, NOT_CORE_TLBID);
        $setuphold(posedge CLK &&& cs_flag, posedge CORE_TLBP,  1.000, 1.000, NOT_CORE_TLBP);
        $setuphold(posedge CLK &&& cs_flag, negedge CORE_TLBP,  1.000, 1.000, NOT_CORE_TLBP);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[0],   1.000, 1.000, NOT_CORE_WR_DATA_0);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[0],   1.000, 1.000, NOT_CORE_WR_DATA_0);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[1],   1.000, 1.000, NOT_CORE_WR_DATA_1);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[1],   1.000, 1.000, NOT_CORE_WR_DATA_1);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[2],   1.000, 1.000, NOT_CORE_WR_DATA_2);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[2],   1.000, 1.000, NOT_CORE_WR_DATA_2);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[3],   1.000, 1.000, NOT_CORE_WR_DATA_3);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[3],   1.000, 1.000, NOT_CORE_WR_DATA_3);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[4],   1.000, 1.000, NOT_CORE_WR_DATA_4);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[4],   1.000, 1.000, NOT_CORE_WR_DATA_4);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[5],   1.000, 1.000, NOT_CORE_WR_DATA_5);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[5],   1.000, 1.000, NOT_CORE_WR_DATA_5);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[6],   1.000, 1.000, NOT_CORE_WR_DATA_6);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[6],   1.000, 1.000, NOT_CORE_WR_DATA_6);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[7],   1.000, 1.000, NOT_CORE_WR_DATA_7);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[7],   1.000, 1.000, NOT_CORE_WR_DATA_7);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[8],   1.000, 1.000, NOT_CORE_WR_DATA_8);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[8],   1.000, 1.000, NOT_CORE_WR_DATA_8);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[9],   1.000, 1.000, NOT_CORE_WR_DATA_9);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[9],   1.000, 1.000, NOT_CORE_WR_DATA_9);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[10],  1.000, 1.000, NOT_CORE_WR_DATA_10);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[10],  1.000, 1.000, NOT_CORE_WR_DATA_10);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[11],  1.000, 1.000, NOT_CORE_WR_DATA_11);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[11],  1.000, 1.000, NOT_CORE_WR_DATA_11);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[12],  1.000, 1.000, NOT_CORE_WR_DATA_12);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[12],  1.000, 1.000, NOT_CORE_WR_DATA_12);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[13],  1.000, 1.000, NOT_CORE_WR_DATA_13);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[13],  1.000, 1.000, NOT_CORE_WR_DATA_13);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[14],  1.000, 1.000, NOT_CORE_WR_DATA_14);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[14],  1.000, 1.000, NOT_CORE_WR_DATA_14);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[15],  1.000, 1.000, NOT_CORE_WR_DATA_15);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[15],  1.000, 1.000, NOT_CORE_WR_DATA_15);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[16],  1.000, 1.000, NOT_CORE_WR_DATA_16);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[16],  1.000, 1.000, NOT_CORE_WR_DATA_16);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[17],  1.000, 1.000, NOT_CORE_WR_DATA_17);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[17],  1.000, 1.000, NOT_CORE_WR_DATA_17);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[18],  1.000, 1.000, NOT_CORE_WR_DATA_18);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[18],  1.000, 1.000, NOT_CORE_WR_DATA_18);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[19],  1.000, 1.000, NOT_CORE_WR_DATA_19);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[19],  1.000, 1.000, NOT_CORE_WR_DATA_19);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[20],  1.000, 1.000, NOT_CORE_WR_DATA_20);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[20],  1.000, 1.000, NOT_CORE_WR_DATA_20);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[21],  1.000, 1.000, NOT_CORE_WR_DATA_21);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[21],  1.000, 1.000, NOT_CORE_WR_DATA_21);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[22],  1.000, 1.000, NOT_CORE_WR_DATA_22);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[22],  1.000, 1.000, NOT_CORE_WR_DATA_22);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[23],  1.000, 1.000, NOT_CORE_WR_DATA_23);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[23],  1.000, 1.000, NOT_CORE_WR_DATA_23);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[24],  1.000, 1.000, NOT_CORE_WR_DATA_24);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[24],  1.000, 1.000, NOT_CORE_WR_DATA_24);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[25],  1.000, 1.000, NOT_CORE_WR_DATA_25);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[25],  1.000, 1.000, NOT_CORE_WR_DATA_25);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[26],  1.000, 1.000, NOT_CORE_WR_DATA_26);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[26],  1.000, 1.000, NOT_CORE_WR_DATA_26);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[27],  1.000, 1.000, NOT_CORE_WR_DATA_27);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[27],  1.000, 1.000, NOT_CORE_WR_DATA_27);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[28],  1.000, 1.000, NOT_CORE_WR_DATA_28);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[28],  1.000, 1.000, NOT_CORE_WR_DATA_28);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[29],  1.000, 1.000, NOT_CORE_WR_DATA_29);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[29],  1.000, 1.000, NOT_CORE_WR_DATA_29);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[30],  1.000, 1.000, NOT_CORE_WR_DATA_30);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[30],  1.000, 1.000, NOT_CORE_WR_DATA_30);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[31],  1.000, 1.000, NOT_CORE_WR_DATA_31);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[31],  1.000, 1.000, NOT_CORE_WR_DATA_31);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[32],  1.000, 1.000, NOT_CORE_WR_DATA_32);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[32],  1.000, 1.000, NOT_CORE_WR_DATA_32);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[33],  1.000, 1.000, NOT_CORE_WR_DATA_33);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[33],  1.000, 1.000, NOT_CORE_WR_DATA_33);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[34],  1.000, 1.000, NOT_CORE_WR_DATA_34);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[34],  1.000, 1.000, NOT_CORE_WR_DATA_34);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[35],  1.000, 1.000, NOT_CORE_WR_DATA_35);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[35],  1.000, 1.000, NOT_CORE_WR_DATA_35);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[36],  1.000, 1.000, NOT_CORE_WR_DATA_36);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[36],  1.000, 1.000, NOT_CORE_WR_DATA_36);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[37],  1.000, 1.000, NOT_CORE_WR_DATA_37);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[37],  1.000, 1.000, NOT_CORE_WR_DATA_37);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[38],  1.000, 1.000, NOT_CORE_WR_DATA_38);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[38],  1.000, 1.000, NOT_CORE_WR_DATA_38);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[39],  1.000, 1.000, NOT_CORE_WR_DATA_39);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[39],  1.000, 1.000, NOT_CORE_WR_DATA_39);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[40],  1.000, 1.000, NOT_CORE_WR_DATA_40);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[40],  1.000, 1.000, NOT_CORE_WR_DATA_40);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[41],  1.000, 1.000, NOT_CORE_WR_DATA_41);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[41],  1.000, 1.000, NOT_CORE_WR_DATA_41);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[42],  1.000, 1.000, NOT_CORE_WR_DATA_42);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[42],  1.000, 1.000, NOT_CORE_WR_DATA_42);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[43],  1.000, 1.000, NOT_CORE_WR_DATA_43);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[43],  1.000, 1.000, NOT_CORE_WR_DATA_43);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[44],  1.000, 1.000, NOT_CORE_WR_DATA_44);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[44],  1.000, 1.000, NOT_CORE_WR_DATA_44);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[45],  1.000, 1.000, NOT_CORE_WR_DATA_45);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[45],  1.000, 1.000, NOT_CORE_WR_DATA_45);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[46],  1.000, 1.000, NOT_CORE_WR_DATA_46);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[46],  1.000, 1.000, NOT_CORE_WR_DATA_46);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[47],  1.000, 1.000, NOT_CORE_WR_DATA_47);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[47],  1.000, 1.000, NOT_CORE_WR_DATA_47);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[48],  1.000, 1.000, NOT_CORE_WR_DATA_48);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[48],  1.000, 1.000, NOT_CORE_WR_DATA_48);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[49],  1.000, 1.000, NOT_CORE_WR_DATA_49);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[49],  1.000, 1.000, NOT_CORE_WR_DATA_49);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[50],  1.000, 1.000, NOT_CORE_WR_DATA_50);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[50],  1.000, 1.000, NOT_CORE_WR_DATA_50);
        $setuphold(posedge CLK &&& asid_flag, posedge CORE_WR_DATA[51],  1.000, 1.000, NOT_CORE_WR_DATA_51);
        $setuphold(posedge CLK &&& asid_flag, negedge CORE_WR_DATA[51],  1.000, 1.000, NOT_CORE_WR_DATA_51);
        $setuphold(posedge CLK &&& asid_flag, posedge CORE_WR_DATA[52],  1.000, 1.000, NOT_CORE_WR_DATA_52);
        $setuphold(posedge CLK &&& asid_flag, negedge CORE_WR_DATA[52],  1.000, 1.000, NOT_CORE_WR_DATA_52);
        $setuphold(posedge CLK &&& asid_flag, posedge CORE_WR_DATA[53],  1.000, 1.000, NOT_CORE_WR_DATA_53);
        $setuphold(posedge CLK &&& asid_flag, negedge CORE_WR_DATA[53],  1.000, 1.000, NOT_CORE_WR_DATA_53);
        $setuphold(posedge CLK &&& asid_flag, posedge CORE_WR_DATA[54],  1.000, 1.000, NOT_CORE_WR_DATA_54);
        $setuphold(posedge CLK &&& asid_flag, negedge CORE_WR_DATA[54],  1.000, 1.000, NOT_CORE_WR_DATA_54);
        $setuphold(posedge CLK &&& asid_flag, posedge CORE_WR_DATA[55],  1.000, 1.000, NOT_CORE_WR_DATA_55);
        $setuphold(posedge CLK &&& asid_flag, negedge CORE_WR_DATA[55],  1.000, 1.000, NOT_CORE_WR_DATA_55);
        $setuphold(posedge CLK &&& asid_flag, posedge CORE_WR_DATA[56],  1.000, 1.000, NOT_CORE_WR_DATA_56);
        $setuphold(posedge CLK &&& asid_flag, negedge CORE_WR_DATA[56],  1.000, 1.000, NOT_CORE_WR_DATA_56);
        $setuphold(posedge CLK &&& asid_flag, posedge CORE_WR_DATA[57],  1.000, 1.000, NOT_CORE_WR_DATA_57);
        $setuphold(posedge CLK &&& asid_flag, negedge CORE_WR_DATA[57],  1.000, 1.000, NOT_CORE_WR_DATA_57);
        $setuphold(posedge CLK &&& asid_flag, posedge CORE_WR_DATA[58],  1.000, 1.000, NOT_CORE_WR_DATA_58);
        $setuphold(posedge CLK &&& asid_flag, negedge CORE_WR_DATA[58],  1.000, 1.000, NOT_CORE_WR_DATA_58);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[59],  1.000, 1.000, NOT_CORE_WR_DATA_59);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[59],  1.000, 1.000, NOT_CORE_WR_DATA_59);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[60],  1.000, 1.000, NOT_CORE_WR_DATA_60);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[60],  1.000, 1.000, NOT_CORE_WR_DATA_60);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[61],  1.000, 1.000, NOT_CORE_WR_DATA_61);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[61],  1.000, 1.000, NOT_CORE_WR_DATA_61);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[62],  1.000, 1.000, NOT_CORE_WR_DATA_62);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[62],  1.000, 1.000, NOT_CORE_WR_DATA_62);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[63],  1.000, 1.000, NOT_CORE_WR_DATA_63);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[63],  1.000, 1.000, NOT_CORE_WR_DATA_63);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[64],  1.000, 1.000, NOT_CORE_WR_DATA_64);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[64],  1.000, 1.000, NOT_CORE_WR_DATA_64);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[65],  1.000, 1.000, NOT_CORE_WR_DATA_65);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[65],  1.000, 1.000, NOT_CORE_WR_DATA_65);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[66],  1.000, 1.000, NOT_CORE_WR_DATA_66);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[66],  1.000, 1.000, NOT_CORE_WR_DATA_66);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[67],  1.000, 1.000, NOT_CORE_WR_DATA_67);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[67],  1.000, 1.000, NOT_CORE_WR_DATA_67);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[68],  1.000, 1.000, NOT_CORE_WR_DATA_68);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[68],  1.000, 1.000, NOT_CORE_WR_DATA_68);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[69],  1.000, 1.000, NOT_CORE_WR_DATA_69);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[69],  1.000, 1.000, NOT_CORE_WR_DATA_69);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[70],  1.000, 1.000, NOT_CORE_WR_DATA_70);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[70],  1.000, 1.000, NOT_CORE_WR_DATA_70);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[71],  1.000, 1.000, NOT_CORE_WR_DATA_71);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[71],  1.000, 1.000, NOT_CORE_WR_DATA_71);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[72],  1.000, 1.000, NOT_CORE_WR_DATA_72);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[72],  1.000, 1.000, NOT_CORE_WR_DATA_72);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[73],  1.000, 1.000, NOT_CORE_WR_DATA_73);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[73],  1.000, 1.000, NOT_CORE_WR_DATA_73);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[74],  1.000, 1.000, NOT_CORE_WR_DATA_74);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[74],  1.000, 1.000, NOT_CORE_WR_DATA_74);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[75],  1.000, 1.000, NOT_CORE_WR_DATA_75);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[75],  1.000, 1.000, NOT_CORE_WR_DATA_75);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[76],  1.000, 1.000, NOT_CORE_WR_DATA_76);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[76],  1.000, 1.000, NOT_CORE_WR_DATA_76);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[77],  1.000, 1.000, NOT_CORE_WR_DATA_77);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[77],  1.000, 1.000, NOT_CORE_WR_DATA_77);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[78],  1.000, 1.000, NOT_CORE_WR_DATA_78);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[78],  1.000, 1.000, NOT_CORE_WR_DATA_78);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[79],  1.000, 1.000, NOT_CORE_WR_DATA_79);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[79],  1.000, 1.000, NOT_CORE_WR_DATA_79);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[80],  1.000, 1.000, NOT_CORE_WR_DATA_80);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[80],  1.000, 1.000, NOT_CORE_WR_DATA_80);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[81],  1.000, 1.000, NOT_CORE_WR_DATA_81);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[81],  1.000, 1.000, NOT_CORE_WR_DATA_81);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[82],  1.000, 1.000, NOT_CORE_WR_DATA_82);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[82],  1.000, 1.000, NOT_CORE_WR_DATA_82);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[83],  1.000, 1.000, NOT_CORE_WR_DATA_83);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[83],  1.000, 1.000, NOT_CORE_WR_DATA_83);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[84],  1.000, 1.000, NOT_CORE_WR_DATA_84);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[84],  1.000, 1.000, NOT_CORE_WR_DATA_84);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[85],  1.000, 1.000, NOT_CORE_WR_DATA_85);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[85],  1.000, 1.000, NOT_CORE_WR_DATA_85);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[86],  1.000, 1.000, NOT_CORE_WR_DATA_86);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[86],  1.000, 1.000, NOT_CORE_WR_DATA_86);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[87],  1.000, 1.000, NOT_CORE_WR_DATA_87);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[87],  1.000, 1.000, NOT_CORE_WR_DATA_87);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[88],  1.000, 1.000, NOT_CORE_WR_DATA_88);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[88],  1.000, 1.000, NOT_CORE_WR_DATA_88);
        $setuphold(posedge CLK &&& wr_flag, posedge CORE_WR_DATA[89],  1.000, 1.000, NOT_CORE_WR_DATA_89);
        $setuphold(posedge CLK &&& wr_flag, negedge CORE_WR_DATA[89],  1.000, 1.000, NOT_CORE_WR_DATA_89);
        $setuphold(posedge CLK &&& cm_flag, posedge CORE_VPNIN[0],     1.000, 1.000, NOT_CORE_VPNIN_0);
        $setuphold(posedge CLK &&& cm_flag, negedge CORE_VPNIN[0],     1.000, 1.000, NOT_CORE_VPNIN_0);
        $setuphold(posedge CLK &&& cm_flag, posedge CORE_VPNIN[1],     1.000, 1.000, NOT_CORE_VPNIN_1);
        $setuphold(posedge CLK &&& cm_flag, negedge CORE_VPNIN[1],     1.000, 1.000, NOT_CORE_VPNIN_1);
        $setuphold(posedge CLK &&& cm_flag, posedge CORE_VPNIN[2],     1.000, 1.000, NOT_CORE_VPNIN_2);
        $setuphold(posedge CLK &&& cm_flag, negedge CORE_VPNIN[2],     1.000, 1.000, NOT_CORE_VPNIN_2);
        $setuphold(posedge CLK &&& cm_flag, posedge CORE_VPNIN[3],     1.000, 1.000, NOT_CORE_VPNIN_3);
        $setuphold(posedge CLK &&& cm_flag, negedge CORE_VPNIN[3],     1.000, 1.000, NOT_CORE_VPNIN_3);
        $setuphold(posedge CLK &&& cm_flag, posedge CORE_VPNIN[4],     1.000, 1.000, NOT_CORE_VPNIN_4);
        $setuphold(posedge CLK &&& cm_flag, negedge CORE_VPNIN[4],     1.000, 1.000, NOT_CORE_VPNIN_4);
        $setuphold(posedge CLK &&& cm_flag, posedge CORE_VPNIN[5],     1.000, 1.000, NOT_CORE_VPNIN_5);
        $setuphold(posedge CLK &&& cm_flag, negedge CORE_VPNIN[5],     1.000, 1.000, NOT_CORE_VPNIN_5);
        $setuphold(posedge CLK &&& cm_flag, posedge CORE_VPNIN[6],     1.000, 1.000, NOT_CORE_VPNIN_6);
        $setuphold(posedge CLK &&& cm_flag, negedge CORE_VPNIN[6],     1.000, 1.000, NOT_CORE_VPNIN_6);
        $setuphold(posedge CLK &&& cm_flag, posedge CORE_VPNIN[7],     1.000, 1.000, NOT_CORE_VPNIN_7);
        $setuphold(posedge CLK &&& cm_flag, negedge CORE_VPNIN[7],     1.000, 1.000, NOT_CORE_VPNIN_7);
        $setuphold(posedge CLK &&& cm_flag, posedge CORE_VPNIN[8],     1.000, 1.000, NOT_CORE_VPNIN_8);
        $setuphold(posedge CLK &&& cm_flag, negedge CORE_VPNIN[8],     1.000, 1.000, NOT_CORE_VPNIN_8);
        $setuphold(posedge CLK &&& cm_flag, posedge CORE_VPNIN[9],     1.000, 1.000, NOT_CORE_VPNIN_9);
        $setuphold(posedge CLK &&& cm_flag, negedge CORE_VPNIN[9],     1.000, 1.000, NOT_CORE_VPNIN_9);
        $setuphold(posedge CLK &&& cm_flag, posedge CORE_VPNIN[10],    1.000, 1.000, NOT_CORE_VPNIN_10);
        $setuphold(posedge CLK &&& cm_flag, negedge CORE_VPNIN[10],    1.000, 1.000, NOT_CORE_VPNIN_10);
        $setuphold(posedge CLK &&& cm_flag, posedge CORE_VPNIN[11],    1.000, 1.000, NOT_CORE_VPNIN_11);
        $setuphold(posedge CLK &&& cm_flag, negedge CORE_VPNIN[11],    1.000, 1.000, NOT_CORE_VPNIN_11);
        $setuphold(posedge CLK &&& cm_flag, posedge CORE_VPNIN[12],    1.000, 1.000, NOT_CORE_VPNIN_12);
        $setuphold(posedge CLK &&& cm_flag, negedge CORE_VPNIN[12],    1.000, 1.000, NOT_CORE_VPNIN_12);
        $setuphold(posedge CLK &&& cm_flag, posedge CORE_VPNIN[13],    1.000, 1.000, NOT_CORE_VPNIN_13);
        $setuphold(posedge CLK &&& cm_flag, negedge CORE_VPNIN[13],    1.000, 1.000, NOT_CORE_VPNIN_13);
        $setuphold(posedge CLK &&& cm_flag, posedge CORE_VPNIN[14],    1.000, 1.000, NOT_CORE_VPNIN_14);
        $setuphold(posedge CLK &&& cm_flag, negedge CORE_VPNIN[14],    1.000, 1.000, NOT_CORE_VPNIN_14);
        $setuphold(posedge CLK &&& cm_flag, posedge CORE_VPNIN[15],    1.000, 1.000, NOT_CORE_VPNIN_15);
        $setuphold(posedge CLK &&& cm_flag, negedge CORE_VPNIN[15],    1.000, 1.000, NOT_CORE_VPNIN_15);
        $setuphold(posedge CLK &&& cm_flag, posedge CORE_VPNIN[16],    1.000, 1.000, NOT_CORE_VPNIN_16);
        $setuphold(posedge CLK &&& cm_flag, negedge CORE_VPNIN[16],    1.000, 1.000, NOT_CORE_VPNIN_16);
        $setuphold(posedge CLK &&& cm_flag, posedge CORE_VPNIN[17],    1.000, 1.000, NOT_CORE_VPNIN_17);
        $setuphold(posedge CLK &&& cm_flag, negedge CORE_VPNIN[17],    1.000, 1.000, NOT_CORE_VPNIN_17);
        $setuphold(posedge CLK &&& cm_flag, posedge CORE_VPNIN[18],    1.000, 1.000, NOT_CORE_VPNIN_18);
        $setuphold(posedge CLK &&& cm_flag, negedge CORE_VPNIN[18],    1.000, 1.000, NOT_CORE_VPNIN_18);
        $setuphold(posedge CLK &&& cm_flag, posedge CORE_VPNIN[19],    1.000, 1.000, NOT_CORE_VPNIN_19);
        $setuphold(posedge CLK &&& cm_flag, negedge CORE_VPNIN[19],    1.000, 1.000, NOT_CORE_VPNIN_19);
        $setuphold(posedge CLK &&& rw_flag, posedge CORE_RW_INDEX[0],  1.000, 1.000, NOT_CORE_RW_INDEX_0);
        $setuphold(posedge CLK &&& rw_flag, negedge CORE_RW_INDEX[0],  1.000, 1.000, NOT_CORE_RW_INDEX_0);
        $setuphold(posedge CLK &&& rw_flag, posedge CORE_RW_INDEX[1],  1.000, 1.000, NOT_CORE_RW_INDEX_1);
        $setuphold(posedge CLK &&& rw_flag, negedge CORE_RW_INDEX[1],  1.000, 1.000, NOT_CORE_RW_INDEX_1);
        $setuphold(posedge CLK &&& rw_flag, posedge CORE_RW_INDEX[2],  1.000, 1.000, NOT_CORE_RW_INDEX_2);
        $setuphold(posedge CLK &&& rw_flag, negedge CORE_RW_INDEX[2],  1.000, 1.000, NOT_CORE_RW_INDEX_2);
        $setuphold(posedge CLK &&& rw_flag, posedge CORE_RW_INDEX[3],  1.000, 1.000, NOT_CORE_RW_INDEX_3);
        $setuphold(posedge CLK &&& rw_flag, negedge CORE_RW_INDEX[3],  1.000, 1.000, NOT_CORE_RW_INDEX_3);
        $setuphold(posedge CLK &&& rw_flag, posedge CORE_RW_INDEX[4],  1.000, 1.000, NOT_CORE_RW_INDEX_4);
        $setuphold(posedge CLK &&& rw_flag, negedge CORE_RW_INDEX[4],  1.000, 1.000, NOT_CORE_RW_INDEX_4);
        //$setuphold(posedge CLK,             FRE,               1.000, 1.000, NOT_FRE);
        $setuphold(posedge CLK,           posedge SE,      1.000, 1.000, NOT_SE);
        $setuphold(posedge CLK,           negedge SE,      1.000, 1.000, NOT_SE);
        $setuphold(posedge CLK,           posedge BIST_DIAG,      1.000, 1.000, NOT_SE);
        $setuphold(posedge CLK,           negedge BIST_DIAG,      1.000, 1.000, NOT_SE);
        $setuphold(posedge CLK,           posedge BIST_HOLD,      1.000, 1.000, NOT_SE);
        $setuphold(posedge CLK,           negedge BIST_HOLD,      1.000, 1.000, NOT_SE);
        $setuphold(posedge CLK,           posedge SCAN_MODE,      1.000, 1.000, NOT_SE);
        $setuphold(posedge CLK,           negedge SCAN_MODE,      1.000, 1.000, NOT_SE);
        $setuphold(posedge CLK,           posedge BIST_SO,      1.000, 1.000, NOT_SE);
        $setuphold(posedge CLK,           negedge BIST_SO,      1.000, 1.000, NOT_SE);
        $setuphold(posedge CLK &&& SEi,     BIST_SO,           1.000, 1.000, NOT_BIST_SO);

        (CLK => ENTRY_OUT[0])   = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_OUT[1])   = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_OUT[2])   = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_OUT[3])   = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_OUT[4])   = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_OUT[5])   = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_OUT[6])   = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_OUT[7])   = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_OUT[8])   = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_OUT[9])   = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_OUT[10])  = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_OUT[11])  = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_OUT[12])  = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_OUT[13])  = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_OUT[14])  = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_OUT[15])  = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_OUT[16])  = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_OUT[17])  = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_OUT[18])  = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_OUT[19])  = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_OUT[20])  = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_OUT[21])  = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_OUT[22])  = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_OUT[23])  = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_OUT[24])  = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[0])  = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[1])  = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[2])  = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[3])  = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[4])  = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[5])  = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[6])  = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[7])  = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[8])  = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[9])  = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[10]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[11]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[12]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[13]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[14]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[15]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[16]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[17]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[18]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[19]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[20]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[21]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[22]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[23]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[24]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[25]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[26]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[27]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[28]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[29]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[30]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[31]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[32]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[33]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[34]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[35]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[36]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[37]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[38]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[39]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[40]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[41]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[42]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[43]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[44]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[45]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[46]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[47]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[48]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[49]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[50]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[51]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[52]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[53]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[54]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[55]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[56]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[57]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[58]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[59]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[60]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[61]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[62]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[63]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[64]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[65]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[66]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[67]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[68]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[69]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[70]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[71]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[72]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[73]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[74]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[75]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[76]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[77]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[78]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[79]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[80]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[81]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[82]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[83]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[84]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[85]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[86]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[87]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[88]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DATA[89]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => SEL_ODD) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => CORE_HIT)           = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => CORE_HIT_INDEX[0])  = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => CORE_HIT_INDEX[1])  = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => CORE_HIT_INDEX[2])  = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => CORE_HIT_INDEX[3])  = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => CORE_HIT_INDEX[4])  = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => CORE_MULTIHIT)      = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => SO)      = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
endspecify
endmodule
`endcelldefine

module decoder (vec_out,en,index_in);

output	[31:0]	vec_out;
input			en;
input	[4:0]	index_in;

wire	[31:0]	vec_wire;
assign	vec_wire[0] = index_in[4:0] ==      5'b00000;
assign	vec_wire[1] = index_in[4:0] ==      5'b00001;
assign	vec_wire[2] = index_in[4:0] ==      5'b00010;
assign	vec_wire[3] = index_in[4:0] ==      5'b00011;
assign	vec_wire[4] = index_in[4:0] ==      5'b00100;
assign	vec_wire[5] = index_in[4:0] ==      5'b00101;
assign	vec_wire[6] = index_in[4:0] ==      5'b00110;
assign	vec_wire[7] = index_in[4:0] ==      5'b00111;
assign	vec_wire[8] = index_in[4:0] ==      5'b01000;
assign	vec_wire[9] = index_in[4:0] ==      5'b01001;
assign	vec_wire[10] = index_in[4:0] ==      5'b01010;
assign	vec_wire[11] = index_in[4:0] ==      5'b01011;
assign	vec_wire[12] = index_in[4:0] ==      5'b01100;
assign	vec_wire[13] = index_in[4:0] ==      5'b01101;
assign	vec_wire[14] = index_in[4:0] ==      5'b01110;
assign	vec_wire[15] = index_in[4:0] ==      5'b01111;
assign	vec_wire[16] = index_in[4:0] ==      5'b10000;
assign	vec_wire[17] = index_in[4:0] ==      5'b10001;
assign	vec_wire[18] = index_in[4:0] ==      5'b10010;
assign	vec_wire[19] = index_in[4:0] ==      5'b10011;
assign	vec_wire[20] = index_in[4:0] ==      5'b10100;
assign	vec_wire[21] = index_in[4:0] ==      5'b10101;
assign	vec_wire[22] = index_in[4:0] ==      5'b10110;
assign	vec_wire[23] = index_in[4:0] ==      5'b10111;
assign	vec_wire[24] = index_in[4:0] ==      5'b11000;
assign	vec_wire[25] = index_in[4:0] ==      5'b11001;
assign	vec_wire[26] = index_in[4:0] ==      5'b11010;
assign	vec_wire[27] = index_in[4:0] ==      5'b11011;
assign	vec_wire[28] = index_in[4:0] ==      5'b11100;
assign	vec_wire[29] = index_in[4:0] ==      5'b11101;
assign	vec_wire[30] = index_in[4:0] ==      5'b11110;
assign	vec_wire[31] = index_in[4:0] ==      5'b11111;

assign	vec_out	=	en ? vec_wire : 32'h00000000;

endmodule

module		cpu_encoder(
			index_out,
			vec_in
			);
output		[4:0]	index_out;
input		[31:0]	vec_in;

function [4:0]	d;
input	[31:0]	vec_in;
	case(1)// synopsys parallel_case full_case
		vec_in[0]:	d = 5'b00000;
		vec_in[1]:	d = 5'b00001;
		vec_in[2]:  d = 5'b00010;
		vec_in[3]:  d = 5'b00011;
		vec_in[4]:  d = 5'b00100;
		vec_in[5]:  d = 5'b00101;
		vec_in[6]:  d = 5'b00110;
		vec_in[7]:  d = 5'b00111;
		vec_in[8]:  d = 5'b01000;
		vec_in[9]:  d = 5'b01001;
		vec_in[10]:  d = 5'b01010;
		vec_in[11]:  d = 5'b01011;
		vec_in[12]:  d = 5'b01100;
		vec_in[13]:  d = 5'b01101;
		vec_in[14]:  d = 5'b01110;
		vec_in[15]:  d = 5'b01111;
		vec_in[16]:  d = 5'b10000;
		vec_in[17]:  d = 5'b10001;
		vec_in[18]:  d = 5'b10010;
		vec_in[19]:  d = 5'b10011;
		vec_in[20]:  d = 5'b10100;
		vec_in[21]:  d = 5'b10101;
		vec_in[22]:  d = 5'b10110;
		vec_in[23]:  d = 5'b10111;
		vec_in[24]:  d = 5'b11000;
		vec_in[25]:  d = 5'b11001;
		vec_in[26]:  d = 5'b11010;
		vec_in[27]:  d = 5'b11011;
		vec_in[28]:  d = 5'b11100;
		vec_in[29]:  d = 5'b11101;
		vec_in[30]:  d = 5'b11110;
		vec_in[31]:  d = 5'b11111;
		default	  :	 d = 5'b00000;
	endcase
endfunction
assign index_out = d(vec_in);

endmodule	//end of p_encoder

module tlb_sd (
			multi_hit,
			hit_vec
				);

output		multi_hit;

input	[31:0]	hit_vec;

parameter	u_dly	=	1;

wire	[30:0]	sel;
assign	sel[0] 		=	 (~(hit_vec[31:16] == 16'b0)) &	(~(hit_vec[15:0]  == 16'b0));
assign	sel[1]		=	 (~(hit_vec[31:24] == 8'b0))  &	(~(hit_vec[23:16] == 8'b0));
assign	sel[2]		=	 (~(hit_vec[15:8]  == 8'b0))  &	(~(hit_vec[7:0]   == 8'b0));
assign	sel[3]		=	 (~(hit_vec[31:28] == 4'b0))  &	(~(hit_vec[27:24] == 4'b0));
assign	sel[4]		=	 (~(hit_vec[23:20] == 4'b0))  &	(~(hit_vec[19:16] == 4'b0));
assign	sel[5]		=	 (~(hit_vec[15:12] == 4'b0))  &	(~(hit_vec[11:8]  == 4'b0));
assign	sel[6]		=	 (~(hit_vec[7:4]   == 4'b0))  &	(~(hit_vec[3:0]   == 4'b0));
assign	sel[7]		=	 (~(hit_vec[31:30] == 2'b0))  &	(~(hit_vec[29:28] == 2'b0));
assign	sel[8]		=	 (~(hit_vec[27:26] == 2'b0))  &	(~(hit_vec[25:24] == 2'b0));
assign	sel[9]		=	 (~(hit_vec[23:22] == 2'b0))  &	(~(hit_vec[21:20] == 2'b0));
assign	sel[10]		=	 (~(hit_vec[19:18] == 2'b0))  &	(~(hit_vec[17:16] == 2'b0));
assign	sel[11]		=	 (~(hit_vec[15:14] == 2'b0))  &	(~(hit_vec[13:12] == 2'b0));
assign	sel[12]		=	 (~(hit_vec[11:10] == 2'b0))  &	(~(hit_vec[9:8]   == 2'b0));
assign	sel[13]		=	 (~(hit_vec[7:6]   == 2'b0))  &	(~(hit_vec[5:4]   == 2'b0));
assign	sel[14]		=	 (~(hit_vec[3:2]   == 2'b0))  &	(~(hit_vec[1:0]   == 2'b0));
assign	sel[15]		=	 (hit_vec[31]      == 1'b1)   &	(hit_vec[30] 	  == 1'b1);
assign	sel[16]		=	 (hit_vec[29]      == 1'b1)   &	(hit_vec[28] 	  == 1'b1);
assign	sel[17]		=	 (hit_vec[27]      == 1'b1)   &	(hit_vec[26] 	  == 1'b1);
assign	sel[18]		=	 (hit_vec[25]      == 1'b1)   &	(hit_vec[24] 	  == 1'b1);
assign	sel[19]		=	 (hit_vec[23]      == 1'b1)   &	(hit_vec[22] 	  == 1'b1);
assign	sel[20]		=	 (hit_vec[21]      == 1'b1)   &	(hit_vec[20] 	  == 1'b1);
assign	sel[21]		=	 (hit_vec[19]      == 1'b1)   &	(hit_vec[18] 	  == 1'b1);
assign	sel[22]		=	 (hit_vec[17]      == 1'b1)   &	(hit_vec[16] 	  == 1'b1);
assign	sel[23]		=	 (hit_vec[15]      == 1'b1)   &	(hit_vec[14] 	  == 1'b1);
assign	sel[24]		=	 (hit_vec[13]      == 1'b1)   &	(hit_vec[12] 	  == 1'b1);
assign	sel[25]		=	 (hit_vec[11]      == 1'b1)   &	(hit_vec[10] 	  == 1'b1);
assign	sel[26]		=	 (hit_vec[9]       == 1'b1)   &	(hit_vec[8]  	  == 1'b1);
assign	sel[27]		=	 (hit_vec[7]       == 1'b1)   &	(hit_vec[6]  	  == 1'b1);
assign	sel[28]		=	 (hit_vec[5]       == 1'b1)   &	(hit_vec[4]  	  == 1'b1);
assign	sel[29]		=	 (hit_vec[3]       == 1'b1)   &	(hit_vec[2]  	  == 1'b1);
assign	sel[30]		=	 (hit_vec[1]       == 1'b1)   &	(hit_vec[0]  	  == 1'b1);

function multihit_wire;
input	[30:0]	sel;
case (1) // synopsys parallel_case full_case
	sel[0]	:	multihit_wire	=	1'b1;
	sel[1]	:	multihit_wire	=	1'b1;
	sel[2]	:	multihit_wire	=	1'b1;
	sel[3]	:	multihit_wire	=	1'b1;
	sel[4]	:	multihit_wire	=	1'b1;
	sel[5]	:	multihit_wire	=	1'b1;
	sel[6]	:	multihit_wire	=	1'b1;
	sel[7]	:	multihit_wire	=	1'b1;
	sel[8]	:	multihit_wire	=	1'b1;
	sel[9]	:	multihit_wire	=	1'b1;
	sel[10]	:	multihit_wire	=	1'b1;
	sel[11]	:	multihit_wire	=	1'b1;
	sel[12]	:	multihit_wire	=	1'b1;
	sel[13]	:	multihit_wire	=	1'b1;
	sel[14]	:	multihit_wire	=	1'b1;
	sel[15]	:	multihit_wire	=	1'b1;
	sel[16]	:	multihit_wire	=	1'b1;
	sel[17]	:	multihit_wire	=	1'b1;
	sel[18]	:	multihit_wire	=	1'b1;
	sel[19]	:	multihit_wire	=	1'b1;
	sel[20]	:	multihit_wire	=	1'b1;
	sel[21]	:	multihit_wire	=	1'b1;
	sel[22]	:	multihit_wire	=	1'b1;
	sel[23]	:	multihit_wire	=	1'b1;
	sel[24]	:	multihit_wire	=	1'b1;
	sel[25]	:	multihit_wire	=	1'b1;
	sel[26]	:	multihit_wire	=	1'b1;
	sel[27]	:	multihit_wire	=	1'b1;
	sel[28]	:	multihit_wire	=	1'b1;
	sel[29]	:	multihit_wire	=	1'b1;
	sel[30]	:	multihit_wire	=	1'b1;
	default	:	multihit_wire	=	1'b0;
endcase
endfunction
assign multi_hit	=	multihit_wire(sel);

endmodule

module	jtlb_cam	(
	// output
	cam_data_out,
	hit_vec,
	odd_vec,
	// input
	cam_data_in,
	vpn,
	cp0_asid,
	tlbwr,
	tlbrd,
	tlbid,
	tlbp,
	addr_vec,
	clk		);


output	[39:0]	cam_data_out;
output	[31:0]	hit_vec;
output	[31:0]	odd_vec;
input	[39:0]	cam_data_in;
input	[19:0]	vpn;
input	[7:0]	cp0_asid;
input		tlbwr;
input		tlbrd;
input		tlbid;
input		tlbp;
input	[31:0]	addr_vec;
input		clk;

wire		camp_en		= tlbid | tlbp;
wire		data_en		= tlbrd | tlbid;
wire	[31:0]	hit_vec_out;
wire	[31:0]	odd_vec_out;
wire	[31:0]	hit_vec		= hit_vec_out & {32{camp_en}};
wire	[31:0]	odd_vec		= odd_vec_out & {32{camp_en}};
wire	[31:0]	rd_en_vec	= tlbrd ? addr_vec : (hit_vec & {32{tlbid}});
wire	[31:0]	wr_vec		= addr_vec & {32{tlbwr}};

//entry cam data
wire	[39:0]	cd0_out, cd1_out, cd2_out, cd3_out, cd4_out, cd5_out, cd6_out, cd7_out,
		cd8_out, cd9_out, cd10_out,cd11_out,cd12_out,cd13_out,cd14_out,cd15_out,
		cd16_out,cd17_out,cd18_out,cd19_out,cd20_out,cd21_out,cd22_out,cd23_out,
		cd24_out,cd25_out,cd26_out,cd27_out,cd28_out,cd29_out,cd30_out,cd31_out;

wire	[39:0]	data_out;
assign	data_out = cd0_out |cd1_out |cd2_out |cd3_out |cd4_out |cd5_out |cd6_out |cd7_out |
		   cd8_out |cd9_out |cd10_out|cd11_out|cd12_out|cd13_out|cd14_out|cd15_out|
		   cd16_out|cd17_out|cd18_out|cd19_out|cd20_out|cd21_out|cd22_out|cd23_out|
		   cd24_out|cd25_out|cd26_out|cd27_out|cd28_out|cd29_out|cd30_out|cd31_out;
wire	[39:0]	cam_data_out = data_out & {40{data_en}};

entry_cam	entry_cam0	(
	.data_out				( cd0_out			),
	.hit					( hit_vec_out[0]		),
	.odd					( odd_vec_out[0]		),

	.cam_data_in				( cam_data_in			),
	.cp0_asid				( cp0_asid			),
	.vpn					( vpn				),
	.tlbwr					( wr_vec[0]			),
	.rd_en					( rd_en_vec[0]			),
	.clk					( clk				));

entry_cam	entry_cam1	(
	.data_out				( cd1_out			),
	.hit					( hit_vec_out[1]		),
	.odd					( odd_vec_out[1]		),

	.cam_data_in				( cam_data_in			),
	.cp0_asid				( cp0_asid			),
	.vpn					( vpn				),
	.tlbwr					( wr_vec[1]			),
	.rd_en					( rd_en_vec[1]			),
	.clk					( clk				));


entry_cam	entry_cam2	(
	.data_out				( cd2_out			),
	.hit					( hit_vec_out[2]		),
	.odd					( odd_vec_out[2]		),

	.cam_data_in				( cam_data_in			),
	.cp0_asid				( cp0_asid			),
	.vpn					( vpn				),
	.tlbwr					( wr_vec[2]				),
	.rd_en					( rd_en_vec[2]			),
	.clk					( clk				));


entry_cam	entry_cam3	(
	.data_out				( cd3_out			),
	.hit					( hit_vec_out[3]		),
	.odd					( odd_vec_out[3]		),

	.cam_data_in				( cam_data_in			),
	.cp0_asid				( cp0_asid			),
	.vpn					( vpn				),
	.tlbwr					( wr_vec[3]				),
	.rd_en					( rd_en_vec[3]			),
	.clk					( clk				));


entry_cam	entry_cam4	(
	.data_out				( cd4_out			),
	.hit					( hit_vec_out[4]		),
	.odd					( odd_vec_out[4]		),

	.cam_data_in				( cam_data_in			),
	.cp0_asid				( cp0_asid			),
	.vpn					( vpn				),
	.tlbwr					( wr_vec[4]				),
	.rd_en					( rd_en_vec[4]			),
	.clk					( clk				));


entry_cam	entry_cam5	(
	.data_out				( cd5_out			),
	.hit					( hit_vec_out[5]		),
	.odd					( odd_vec_out[5]		),

	.cam_data_in				( cam_data_in			),
	.cp0_asid				( cp0_asid			),
	.vpn					( vpn				),
	.tlbwr					( wr_vec[5]				),
	.rd_en					( rd_en_vec[5]			),
	.clk					( clk				));


entry_cam	entry_cam6	(
	.data_out				( cd6_out			),
	.hit					( hit_vec_out[6]		),
	.odd					( odd_vec_out[6]		),

	.cam_data_in				( cam_data_in			),
	.cp0_asid				( cp0_asid			),
	.vpn					( vpn				),
	.tlbwr					( wr_vec[6]				),
	.rd_en					( rd_en_vec[6]			),
	.clk					( clk				));


entry_cam	entry_cam7	(
	.data_out				( cd7_out			),
	.hit					( hit_vec_out[7]		),
	.odd					( odd_vec_out[7]		),

	.cam_data_in				( cam_data_in			),
	.cp0_asid				( cp0_asid			),
	.vpn					( vpn				),
	.tlbwr					( wr_vec[7]				),
	.rd_en					( rd_en_vec[7]			),
	.clk					( clk				));


entry_cam	entry_cam8	(
	.data_out				( cd8_out			),
	.hit					( hit_vec_out[8]		),
	.odd					( odd_vec_out[8]		),

	.cam_data_in				( cam_data_in			),
	.cp0_asid				( cp0_asid			),
	.vpn					( vpn				),
	.tlbwr					( wr_vec[8]				),
	.rd_en					( rd_en_vec[8]			),
	.clk					( clk				));


entry_cam	entry_cam9	(
	.data_out				( cd9_out			),
	.hit					( hit_vec_out[9]		),
	.odd					( odd_vec_out[9]		),

	.cam_data_in				( cam_data_in			),
	.cp0_asid				( cp0_asid			),
	.vpn					( vpn				),
	.tlbwr					( wr_vec[9]				),
	.rd_en					( rd_en_vec[9]			),
	.clk					( clk				));


entry_cam	entry_cam10	(
	.data_out				( cd10_out			),
	.hit					( hit_vec_out[10]		),
	.odd					( odd_vec_out[10]		),

	.cam_data_in				( cam_data_in			),
	.cp0_asid				( cp0_asid			),
	.vpn					( vpn				),
	.tlbwr					( wr_vec[10]				),
	.rd_en					( rd_en_vec[10]			),
	.clk					( clk				));


entry_cam	entry_cam11	(
	.data_out				( cd11_out			),
	.hit					( hit_vec_out[11]		),
	.odd					( odd_vec_out[11]		),

	.cam_data_in				( cam_data_in			),
	.cp0_asid				( cp0_asid			),
	.vpn					( vpn				),
	.tlbwr					( wr_vec[11]				),
	.rd_en					( rd_en_vec[11]			),
	.clk					( clk				));


entry_cam	entry_cam12	(
	.data_out				( cd12_out			),
	.hit					( hit_vec_out[12]		),
	.odd					( odd_vec_out[12]		),

	.cam_data_in				( cam_data_in			),
	.cp0_asid				( cp0_asid			),
	.vpn					( vpn				),
	.tlbwr					( wr_vec[12]				),
	.rd_en					( rd_en_vec[12]			),
	.clk					( clk				));


entry_cam	entry_cam13	(
	.data_out				( cd13_out			),
	.hit					( hit_vec_out[13]		),
	.odd					( odd_vec_out[13]		),

	.cam_data_in				( cam_data_in			),
	.cp0_asid				( cp0_asid			),
	.vpn					( vpn				),
	.tlbwr					( wr_vec[13]				),
	.rd_en					( rd_en_vec[13]			),
	.clk					( clk				));


entry_cam	entry_cam14	(
	.data_out				( cd14_out			),
	.hit					( hit_vec_out[14]		),
	.odd					( odd_vec_out[14]		),

	.cam_data_in				( cam_data_in			),
	.cp0_asid				( cp0_asid			),
	.vpn					( vpn				),
	.tlbwr					( wr_vec[14]				),
	.rd_en					( rd_en_vec[14]			),
	.clk					( clk				));


entry_cam	entry_cam15	(
	.data_out				( cd15_out			),
	.hit					( hit_vec_out[15]		),
	.odd					( odd_vec_out[15]		),

	.cam_data_in				( cam_data_in			),
	.cp0_asid				( cp0_asid			),
	.vpn					( vpn				),
	.tlbwr					( wr_vec[15]				),
	.rd_en					( rd_en_vec[15]			),
	.clk					( clk				));


entry_cam	entry_cam16	(
	.data_out				( cd16_out			),
	.hit					( hit_vec_out[16]		),
	.odd					( odd_vec_out[16]		),

	.cam_data_in				( cam_data_in			),
	.cp0_asid				( cp0_asid			),
	.vpn					( vpn				),
	.tlbwr					( wr_vec[16]				),
	.rd_en					( rd_en_vec[16]			),
	.clk					( clk				));


entry_cam	entry_cam17	(
	.data_out				( cd17_out			),
	.hit					( hit_vec_out[17]		),
	.odd					( odd_vec_out[17]		),

	.cam_data_in				( cam_data_in			),
	.cp0_asid				( cp0_asid			),
	.vpn					( vpn				),
	.tlbwr					( wr_vec[17]				),
	.rd_en					( rd_en_vec[17]			),
	.clk					( clk				));


entry_cam	entry_cam18	(
	.data_out				( cd18_out			),
	.hit					( hit_vec_out[18]		),
	.odd					( odd_vec_out[18]		),

	.cam_data_in				( cam_data_in			),
	.cp0_asid				( cp0_asid			),
	.vpn					( vpn				),
	.tlbwr					( wr_vec[18]				),
	.rd_en					( rd_en_vec[18]			),
	.clk					( clk				));


entry_cam	entry_cam19	(
	.data_out				( cd19_out			),
	.hit					( hit_vec_out[19]		),
	.odd					( odd_vec_out[19]		),

	.cam_data_in				( cam_data_in			),
	.cp0_asid				( cp0_asid			),
	.vpn					( vpn				),
	.tlbwr					( wr_vec[19]				),
	.rd_en					( rd_en_vec[19]			),
	.clk					( clk				));


entry_cam	entry_cam20	(
	.data_out				( cd20_out			),
	.hit					( hit_vec_out[20]		),
	.odd					( odd_vec_out[20]		),

	.cam_data_in				( cam_data_in			),
	.cp0_asid				( cp0_asid			),
	.vpn					( vpn				),
	.tlbwr					( wr_vec[20]				),
	.rd_en					( rd_en_vec[20]			),
	.clk					( clk				));


entry_cam	entry_cam21	(
	.data_out				( cd21_out			),
	.hit					( hit_vec_out[21]		),
	.odd					( odd_vec_out[21]		),

	.cam_data_in				( cam_data_in			),
	.cp0_asid				( cp0_asid			),
	.vpn					( vpn				),
	.tlbwr					( wr_vec[21]				),
	.rd_en					( rd_en_vec[21]			),
	.clk					( clk				));


entry_cam	entry_cam22	(
	.data_out				( cd22_out			),
	.hit					( hit_vec_out[22]		),
	.odd					( odd_vec_out[22]		),

	.cam_data_in				( cam_data_in			),
	.cp0_asid				( cp0_asid			),
	.vpn					( vpn				),
	.tlbwr					( wr_vec[22]				),
	.rd_en					( rd_en_vec[22]			),
	.clk					( clk				));


entry_cam	entry_cam23	(
	.data_out				( cd23_out			),
	.hit					( hit_vec_out[23]		),
	.odd					( odd_vec_out[23]		),

	.cam_data_in				( cam_data_in			),
	.cp0_asid				( cp0_asid			),
	.vpn					( vpn				),
	.tlbwr					( wr_vec[23]				),
	.rd_en					( rd_en_vec[23]			),
	.clk					( clk				));


entry_cam	entry_cam24	(
	.data_out				( cd24_out			),
	.hit					( hit_vec_out[24]		),
	.odd					( odd_vec_out[24]		),

	.cam_data_in				( cam_data_in			),
	.cp0_asid				( cp0_asid			),
	.vpn					( vpn				),
	.tlbwr					( wr_vec[24]				),
	.rd_en					( rd_en_vec[24]			),
	.clk					( clk				));


entry_cam	entry_cam25	(
	.data_out				( cd25_out			),
	.hit					( hit_vec_out[25]		),
	.odd					( odd_vec_out[25]		),

	.cam_data_in				( cam_data_in			),
	.cp0_asid				( cp0_asid			),
	.vpn					( vpn				),
	.tlbwr					( wr_vec[25]				),
	.rd_en					( rd_en_vec[25]			),
	.clk					( clk				));


entry_cam	entry_cam26	(
	.data_out				( cd26_out			),
	.hit					( hit_vec_out[26]		),
	.odd					( odd_vec_out[26]		),

	.cam_data_in				( cam_data_in			),
	.cp0_asid				( cp0_asid			),
	.vpn					( vpn				),
	.tlbwr					( wr_vec[26]				),
	.rd_en					( rd_en_vec[26]			),
	.clk					( clk				));


entry_cam	entry_cam27	(
	.data_out				( cd27_out			),
	.hit					( hit_vec_out[27]		),
	.odd					( odd_vec_out[27]		),

	.cam_data_in				( cam_data_in			),
	.cp0_asid				( cp0_asid			),
	.vpn					( vpn				),
	.tlbwr					( wr_vec[27]				),
	.rd_en					( rd_en_vec[27]			),
	.clk					( clk				));


entry_cam	entry_cam28	(
	.data_out				( cd28_out			),
	.hit					( hit_vec_out[28]		),
	.odd					( odd_vec_out[28]		),

	.cam_data_in				( cam_data_in			),
	.cp0_asid				( cp0_asid			),
	.vpn					( vpn				),
	.tlbwr					( wr_vec[28]				),
	.rd_en					( rd_en_vec[28]			),
	.clk					( clk				));


entry_cam	entry_cam29	(
	.data_out				( cd29_out			),
	.hit					( hit_vec_out[29]		),
	.odd					( odd_vec_out[29]		),

	.cam_data_in				( cam_data_in			),
	.cp0_asid				( cp0_asid			),
	.vpn					( vpn				),
	.tlbwr					( wr_vec[29]				),
	.rd_en					( rd_en_vec[29]			),
	.clk					( clk				));


entry_cam	entry_cam30	(
	.data_out				( cd30_out			),
	.hit					( hit_vec_out[30]		),
	.odd					( odd_vec_out[30]		),

	.cam_data_in				( cam_data_in			),
	.cp0_asid				( cp0_asid			),
	.vpn					( vpn				),
	.tlbwr					( wr_vec[30]				),
	.rd_en					( rd_en_vec[30]			),
	.clk					( clk				));

entry_cam	entry_cam31	(
	.data_out				( cd31_out			),
	.hit					( hit_vec_out[31]		),
	.odd					( odd_vec_out[31]		),

	.cam_data_in				( cam_data_in			),
	.cp0_asid				( cp0_asid			),
	.vpn					( vpn				),
	.tlbwr					( wr_vec[31]				),
	.rd_en					( rd_en_vec[31]			),
	.clk					( clk				));

endmodule	// jtlb_cam

module	jtlb_ram	(
	// output
	ram_data,
	jtlb_out,
	// input
	ram_data_in,
	pagemask,
	tlbwr,
	tlbrd,
	tlbid,
	vpn,
	odd_vec,
	addr_vec,
	hit_vec,
	clk
			);

output	[49:0]	ram_data;
output	[24:0]	jtlb_out;

input	[49:0]	ram_data_in;
input	[11:0]	pagemask;
input		tlbwr;
input		tlbrd;
input		tlbid;
input	[19:0]	vpn;
input	[31:0]	odd_vec;
input	[31:0]	addr_vec;
input	[31:0]	hit_vec;
input		clk;

wire	[31:0]	rd_en_vec	= tlbrd ? addr_vec : (hit_vec & {32{tlbid}});
wire		data_en		= tlbrd | tlbid;
wire	[31:0]	wr_vec		= addr_vec & {32{tlbwr}};

wire	[49:0]	r0_data, r1_data, r2_data, r3_data, r4_data, r5_data, r6_data, r7_data,
		r8_data, r9_data, r10_data,r11_data,r12_data,r13_data,r14_data,r15_data,
		r16_data,r17_data,r18_data,r19_data,r20_data,r21_data,r22_data,r23_data,
		r24_data,r25_data,r26_data,r27_data,r28_data,r29_data,r30_data,r31_data;

wire	[49:0]	data_out;
assign	data_out = r0_data |r1_data |r2_data |r3_data |r4_data |r5_data |r6_data |r7_data |
		   r8_data |r9_data |r10_data|r11_data|r12_data|r13_data|r14_data|r15_data|
		   r16_data|r17_data|r18_data|r19_data|r20_data|r21_data|r22_data|r23_data|
		   r24_data|r25_data|r26_data|r27_data|r28_data|r29_data|r30_data|r31_data;
wire	[49:0]	ram_data = data_out & {50{data_en}};

wire	[24:0]	r0_out, r1_out, r2_out, r3_out, r4_out, r5_out, r6_out, r7_out,
		r8_out, r9_out, r10_out,r11_out,r12_out,r13_out,r14_out,r15_out,
		r16_out,r17_out,r18_out,r19_out,r20_out,r21_out,r22_out,r23_out,
		r24_out,r25_out,r26_out,r27_out,r28_out,r29_out,r30_out,r31_out;

wire	[24:0]	entry_out;
assign	jtlb_out = r0_out |r1_out |r2_out |r3_out |r4_out |r5_out |r6_out |r7_out |
		    r8_out |r9_out |r10_out|r11_out|r12_out|r13_out|r14_out|r15_out|
		    r16_out|r17_out|r18_out|r19_out|r20_out|r21_out|r22_out|r23_out|
		    r24_out|r25_out|r26_out|r27_out|r28_out|r29_out|r30_out|r31_out;

entry_ram	entry_ram0	(
	.ram_data			( r0_data			),
	.ram_out			( r0_out			),

	.ram_data_in			( ram_data_in			),
	.pagemask			( pagemask			),
	.vpn				( vpn				),
	.odd				( odd_vec[0]			),
	.tlbwr				( wr_vec[0]			),
	.rd_en				( rd_en_vec[0]			),
	.clk				( clk				));


entry_ram	entry_ram1	(
	.ram_data			( r1_data			),
	.ram_out			( r1_out			),

	.ram_data_in			( ram_data_in			),
	.pagemask			( pagemask			),
	.vpn				( vpn				),
	.odd				( odd_vec[1]			),
	.tlbwr				( wr_vec[1]			),
	.rd_en				( rd_en_vec[1]			),
	.clk				( clk				));


entry_ram	entry_ram2	(
	.ram_data			( r2_data			),
	.ram_out			( r2_out			),

	.ram_data_in			( ram_data_in			),
	.pagemask			( pagemask			),
	.vpn				( vpn				),
	.odd				( odd_vec[2]			),
	.tlbwr				( wr_vec[2]			),
	.rd_en				( rd_en_vec[2]			),
	.clk				( clk				));


entry_ram	entry_ram3	(
	.ram_data			( r3_data			),
	.ram_out			( r3_out			),

	.ram_data_in			( ram_data_in			),
	.pagemask			( pagemask			),
	.vpn				( vpn				),
	.odd				( odd_vec[3]			),
	.tlbwr				( wr_vec[3]			),
	.rd_en				( rd_en_vec[3]			),
	.clk				( clk				));


entry_ram	entry_ram4	(
	.ram_data			( r4_data			),
	.ram_out			( r4_out			),

	.ram_data_in			( ram_data_in			),
	.pagemask			( pagemask			),
	.vpn				( vpn				),
	.odd				( odd_vec[4]			),
	.tlbwr				( wr_vec[4]			),
	.rd_en				( rd_en_vec[4]			),
	.clk				( clk				));


entry_ram	entry_ram5	(
	.ram_data			( r5_data			),
	.ram_out			( r5_out			),

	.ram_data_in			( ram_data_in			),
	.pagemask			( pagemask			),
	.vpn				( vpn				),
	.odd				( odd_vec[5]			),
	.tlbwr				( wr_vec[5]			),
	.rd_en				( rd_en_vec[5]			),
	.clk				( clk				));


entry_ram	entry_ram6	(
	.ram_data			( r6_data			),
	.ram_out			( r6_out			),

	.ram_data_in			( ram_data_in			),
	.pagemask			( pagemask			),
	.vpn				( vpn				),
	.odd				( odd_vec[6]			),
	.tlbwr				( wr_vec[6]			),
	.rd_en				( rd_en_vec[6]			),
	.clk				( clk				));


entry_ram	entry_ram7	(
	.ram_data			( r7_data			),
	.ram_out			( r7_out			),

	.ram_data_in			( ram_data_in			),
	.pagemask			( pagemask			),
	.vpn				( vpn				),
	.odd				( odd_vec[7]			),
	.tlbwr				( wr_vec[7]			),
	.rd_en				( rd_en_vec[7]			),
	.clk				( clk				));


entry_ram	entry_ram8	(
	.ram_data			( r8_data			),
	.ram_out			( r8_out			),

	.ram_data_in			( ram_data_in			),
	.pagemask			( pagemask			),
	.vpn				( vpn				),
	.odd				( odd_vec[8]			),
	.tlbwr				( wr_vec[8]			),
	.rd_en				( rd_en_vec[8]			),
	.clk				( clk				));


entry_ram	entry_ram9	(
	.ram_data			( r9_data			),
	.ram_out			( r9_out			),

	.ram_data_in			( ram_data_in			),
	.pagemask			( pagemask			),
	.vpn				( vpn				),
	.odd				( odd_vec[9]			),
	.tlbwr				( wr_vec[9]			),
	.rd_en				( rd_en_vec[9]			),
	.clk				( clk				));


entry_ram	entry_ram10	(
	.ram_data			( r10_data			),
	.ram_out			( r10_out			),

	.ram_data_in			( ram_data_in			),
	.pagemask			( pagemask			),
	.vpn				( vpn				),
	.odd				( odd_vec[10]			),
	.tlbwr				( wr_vec[10]			),
	.rd_en				( rd_en_vec[10]			),
	.clk				( clk				));


entry_ram	entry_ram11	(
	.ram_data			( r11_data			),
	.ram_out			( r11_out			),

	.ram_data_in			( ram_data_in			),
	.pagemask			( pagemask			),
	.vpn				( vpn				),
	.odd				( odd_vec[11]			),
	.tlbwr				( wr_vec[11]			),
	.rd_en				( rd_en_vec[11]			),
	.clk				( clk				));


entry_ram	entry_ram12	(
	.ram_data			( r12_data			),
	.ram_out			( r12_out			),

	.ram_data_in			( ram_data_in			),
	.pagemask			( pagemask			),
	.vpn				( vpn				),
	.odd				( odd_vec[12]			),
	.tlbwr				( wr_vec[12]			),
	.rd_en				( rd_en_vec[12]			),
	.clk				( clk				));


entry_ram	entry_ram13	(
	.ram_data			( r13_data			),
	.ram_out			( r13_out			),

	.ram_data_in			( ram_data_in			),
	.pagemask			( pagemask			),
	.vpn				( vpn				),
	.odd				( odd_vec[13]			),
	.tlbwr				( wr_vec[13]			),
	.rd_en				( rd_en_vec[13]			),
	.clk				( clk				));


entry_ram	entry_ram14	(
	.ram_data			( r14_data			),
	.ram_out			( r14_out			),

	.ram_data_in			( ram_data_in			),
	.pagemask			( pagemask			),
	.vpn				( vpn				),
	.odd				( odd_vec[14]			),
	.tlbwr				( wr_vec[14]			),
	.rd_en				( rd_en_vec[14]			),
	.clk				( clk				));


entry_ram	entry_ram15	(
	.ram_data			( r15_data			),
	.ram_out			( r15_out			),

	.ram_data_in			( ram_data_in			),
	.pagemask			( pagemask			),
	.vpn				( vpn				),
	.odd				( odd_vec[15]			),
	.tlbwr				( wr_vec[15]			),
	.rd_en				( rd_en_vec[15]			),
	.clk				( clk				));


entry_ram	entry_ram16	(
	.ram_data			( r16_data			),
	.ram_out			( r16_out			),

	.ram_data_in			( ram_data_in			),
	.pagemask			( pagemask			),
	.vpn				( vpn				),
	.odd				( odd_vec[16]			),
	.tlbwr				( wr_vec[16]			),
	.rd_en				( rd_en_vec[16]			),
	.clk				( clk				));


entry_ram	entry_ram17	(
	.ram_data			( r17_data			),
	.ram_out			( r17_out			),

	.ram_data_in			( ram_data_in			),
	.pagemask			( pagemask			),
	.vpn				( vpn				),
	.odd				( odd_vec[17]			),
	.tlbwr				( wr_vec[17]			),
	.rd_en				( rd_en_vec[17]			),
	.clk				( clk				));


entry_ram	entry_ram18	(
	.ram_data			( r18_data			),
	.ram_out			( r18_out			),

	.ram_data_in			( ram_data_in			),
	.pagemask			( pagemask			),
	.vpn				( vpn				),
	.odd				( odd_vec[18]			),
	.tlbwr				( wr_vec[18]			),
	.rd_en				( rd_en_vec[18]			),
	.clk				( clk				));


entry_ram	entry_ram19	(
	.ram_data			( r19_data			),
	.ram_out			( r19_out			),

	.ram_data_in			( ram_data_in			),
	.pagemask			( pagemask			),
	.vpn				( vpn				),
	.odd				( odd_vec[19]			),
	.tlbwr				( wr_vec[19]			),
	.rd_en				( rd_en_vec[19]			),
	.clk				( clk				));


entry_ram	entry_ram20	(
	.ram_data			( r20_data			),
	.ram_out			( r20_out			),

	.ram_data_in			( ram_data_in			),
	.pagemask			( pagemask			),
	.vpn				( vpn				),
	.odd				( odd_vec[20]			),
	.tlbwr				( wr_vec[20]			),
	.rd_en				( rd_en_vec[20]			),
	.clk				( clk				));


entry_ram	entry_ram21	(
	.ram_data			( r21_data			),
	.ram_out			( r21_out			),

	.ram_data_in			( ram_data_in			),
	.pagemask			( pagemask			),
	.vpn				( vpn				),
	.odd				( odd_vec[21]			),
	.tlbwr				( wr_vec[21]			),
	.rd_en				( rd_en_vec[21]			),
	.clk				( clk				));


entry_ram	entry_ram22	(
	.ram_data			( r22_data			),
	.ram_out			( r22_out			),

	.ram_data_in			( ram_data_in			),
	.pagemask			( pagemask			),
	.vpn				( vpn				),
	.odd				( odd_vec[22]			),
	.tlbwr				( wr_vec[22]			),
	.rd_en				( rd_en_vec[22]			),
	.clk				( clk				));


entry_ram	entry_ram23	(
	.ram_data			( r23_data			),
	.ram_out			( r23_out			),

	.ram_data_in			( ram_data_in			),
	.pagemask			( pagemask			),
	.vpn				( vpn				),
	.odd				( odd_vec[23]			),
	.tlbwr				( wr_vec[23]			),
	.rd_en				( rd_en_vec[23]			),
	.clk				( clk				));


entry_ram	entry_ram24	(
	.ram_data			( r24_data			),
	.ram_out			( r24_out			),

	.ram_data_in			( ram_data_in			),
	.pagemask			( pagemask			),
	.vpn				( vpn				),
	.odd				( odd_vec[24]			),
	.tlbwr				( wr_vec[24]			),
	.rd_en				( rd_en_vec[24]			),
	.clk				( clk				));


entry_ram	entry_ram25	(
	.ram_data			( r25_data			),
	.ram_out			( r25_out			),

	.ram_data_in			( ram_data_in			),
	.pagemask			( pagemask			),
	.vpn				( vpn				),
	.odd				( odd_vec[25]			),
	.tlbwr				( wr_vec[25]			),
	.rd_en				( rd_en_vec[25]			),
	.clk				( clk				));


entry_ram	entry_ram26	(
	.ram_data			( r26_data			),
	.ram_out			( r26_out			),

	.ram_data_in			( ram_data_in			),
	.pagemask			( pagemask			),
	.vpn				( vpn				),
	.odd				( odd_vec[26]			),
	.tlbwr				( wr_vec[26]			),
	.rd_en				( rd_en_vec[26]			),
	.clk				( clk				));


entry_ram	entry_ram27	(
	.ram_data			( r27_data			),
	.ram_out			( r27_out			),

	.ram_data_in			( ram_data_in			),
	.pagemask			( pagemask			),
	.vpn				( vpn				),
	.odd				( odd_vec[27]			),
	.tlbwr				( wr_vec[27]			),
	.rd_en				( rd_en_vec[27]			),
	.clk				( clk				));


entry_ram	entry_ram28	(
	.ram_data			( r28_data			),
	.ram_out			( r28_out			),

	.ram_data_in			( ram_data_in			),
	.pagemask			( pagemask			),
	.vpn				( vpn				),
	.odd				( odd_vec[28]			),
	.tlbwr				( wr_vec[28]			),
	.rd_en				( rd_en_vec[28]			),
	.clk				( clk				));


entry_ram	entry_ram29	(
	.ram_data			( r29_data			),
	.ram_out			( r29_out			),

	.ram_data_in			( ram_data_in			),
	.pagemask			( pagemask			),
	.vpn				( vpn				),
	.odd				( odd_vec[29]			),
	.tlbwr				( wr_vec[29]			),
	.rd_en				( rd_en_vec[29]			),
	.clk				( clk				));


entry_ram	entry_ram30	(
	.ram_data			( r30_data			),
	.ram_out			( r30_out			),

	.ram_data_in			( ram_data_in			),
	.pagemask			( pagemask			),
	.vpn				( vpn				),
	.odd				( odd_vec[30]			),
	.tlbwr				( wr_vec[30]			),
	.rd_en				( rd_en_vec[30]			),
	.clk				( clk				));


entry_ram	entry_ram31	(
	.ram_data			( r31_data			),
	.ram_out			( r31_out			),

	.ram_data_in			( ram_data_in			),
	.pagemask			( pagemask			),
	.vpn				( vpn				),
	.odd				( odd_vec[31]			),
	.tlbwr				( wr_vec[31]			),
	.rd_en				( rd_en_vec[31]			),
	.clk				( clk				));

endmodule	// jtlb_ram

module	entry_cam	(
	data_out,
	hit,
	odd,

	cam_data_in,
	cp0_asid,
	vpn,
	tlbwr,
	rd_en,
	clk		);

output	[39:0]	data_out;
output		hit;
output		odd;

input	[39:0]	cam_data_in;
input	[7:0]	cp0_asid;
input	[19:0]	vpn;
input		tlbwr;
input		rd_en;
input		clk;

parameter	udly = 1;

reg		entry_g;
reg	[7:0]	entry_asid;
reg	[11:0]	entry_pagemask;
reg	[18:0]	entry_vpn2;


always@ (posedge clk)
    if (tlbwr)	begin
        entry_g		<= #udly cam_data_in[0];
	entry_asid	<= #udly cam_data_in[8:1];
	entry_vpn2	<= #udly cam_data_in[27:9];
	entry_pagemask	<= #udly cam_data_in[39:28];
	end


wire	odd_0 = ~entry_pagemask[0] & vpn [0];
wire	odd_1 = ~entry_pagemask[2] & entry_pagemask[1] & vpn[2];
wire	odd_2 = ~entry_pagemask[4] & entry_pagemask[3] & vpn[4];
wire	odd_3 = ~entry_pagemask[6] & entry_pagemask[5] & vpn[6];
wire	odd_4 = ~entry_pagemask[8] & entry_pagemask[7] & vpn[8];
wire	odd_5 = ~entry_pagemask[10] & entry_pagemask[9] & vpn[10];
wire	odd_6 = entry_pagemask[11] & vpn[12];

wire	odd = ( odd_0 | odd_1 | odd_2 | odd_3 | odd_4 | odd_5 | odd_6 ) & hit;

wire	asid_hit = (cp0_asid == entry_asid ) | entry_g;
wire	vpn_hi_hit = vpn[19:13] == entry_vpn2[18:12];
wire	vpn_lo_hit = & (~(entry_vpn2[11:0]^vpn[12:1]) | entry_pagemask[11:0]);
wire	hit = asid_hit & vpn_hi_hit & vpn_lo_hit;

wire	[39:0]	data_out = {40{rd_en}} & {entry_pagemask, entry_vpn2, entry_asid, entry_g};

endmodule	// entry_cam

module	entry_ram	(
	ram_data,
	ram_out,

	ram_data_in,
	pagemask,
	vpn,
	odd,
	tlbwr,
	rd_en,
	clk		);

output	[49:0]	ram_data;
output	[24:0]	ram_out;

input	[49:0]	ram_data_in;
input	[11:0]	pagemask;
input	[19:0]	vpn;
input		odd;
input		tlbwr;
input		rd_en;
input		clk;

parameter	udly = 1;

reg		entry_v1;
reg		entry_v0;
reg		entry_d1;
reg		entry_d0;
reg	[2:0]	entry_c1;
reg	[2:0]	entry_c0;
reg	[19:0]	entry_pfn1;
reg	[19:0]	entry_pfn0;
reg	[11:0]	entry_pagemask;


always@ (posedge clk)
    if (tlbwr)	begin
	entry_pfn0	<= #udly ram_data_in[49:30];
	entry_pfn1	<= #udly ram_data_in[29:10];
	entry_c0	<= #udly ram_data_in[9:7];
	entry_c1	<= #udly ram_data_in[6:4];
	entry_d0	<= #udly ram_data_in[3];
	entry_d1	<= #udly ram_data_in[2];
	entry_v0	<= #udly ram_data_in[1];
	entry_v1	<= #udly ram_data_in[0];
	entry_pagemask	<= #udly pagemask;
	end

wire	[2:0]	entry_c = {3{rd_en}} & (odd ? entry_c1 : entry_c0);
wire		entry_d = rd_en & (odd ? entry_d1 : entry_d0);
wire		entry_v = rd_en & (odd ? entry_v1 : entry_v0);
wire	[19:0]	entry_pfn0_m = {entry_pfn0[19:12], entry_pfn0[11:0] & ~entry_pagemask[11:0] |
			       vpn [11:0] & entry_pagemask[11:0]};
wire	[19:0]	entry_pfn1_m = {entry_pfn1[19:12], entry_pfn1[11:0] & ~entry_pagemask[11:0] |
			       vpn [11:0] & entry_pagemask[11:0]};

wire	[19:0]	entry_pfn = {20{rd_en}} & (odd ? entry_pfn1_m : entry_pfn0_m);

wire	[24:0]	ram_out = {entry_pfn, entry_c, entry_d, entry_v};

wire	[49:0]	ram_data = {50{rd_en}} & {entry_pfn0, entry_pfn1, entry_c0, entry_c1,
			entry_d0, entry_d1, entry_v0, entry_v1};

endmodule	// entry_ram
