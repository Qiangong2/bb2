`define	addr_width      12  //  address width
`define	icd_size        72  //  i cache data size
`define	ict_size        22  //  i cache tag size
`define	line_depth      128 //  i cache line depth
`define	u_dly           1	  //  clock to q delay time
`define wide            2   //  i cache lru sram wide
`define hit_vec_dly     2   //  clock period
