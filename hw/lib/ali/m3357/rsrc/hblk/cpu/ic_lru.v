/*******************************************************************************
             Project Name : T2risc1H
             File Name : ic_lru.v
             Create Date : 2001/05/30
             Author : Charliema
             Description :behavior module for i_cache lru
             Revision History :
                 05/31/2001   charlie       initial creation
                 09/07/2001   charlie       add write_through
*******************************************************************************/
//`include  "defines.h"
module ic_lru  (
    Q,    // read port output
    A,    // addr in
    D,    // data in
    WEN,  // write enable
    CEN,  // chip select
    CLK
    );

	parameter ict_size = 22;
        parameter wide = 2;
        parameter addr_width = 12;
        parameter u_dly = 1;
        parameter icd_size = 72;
        parameter line_depth = 128;
        parameter hit_vec_dly = 2;	

     output [wide-1:0]       Q;
     input  [addr_width-1:5] A;
     input                    CEN;
     input                    WEN;
     input  [wide-1:0]       D;
     input                    CLK;

    reg [wide-1:0]   lru_mem[line_depth-1:0];
    reg [wide-1:0]   lru_out;
    wire    CLK_ = !CLK;
    
// the one line code is for verilog simulation, circuit just use tag_out derect to output: port Q;
// this is for write_through,circuit just use old design
    wire [wide-1:0]    Q = lru_out & {(wide){WEN}} | D & {(wide){~WEN}};
    
// read port
always @(CEN or WEN or A or lru_mem[A])
  begin
    if(!CEN & WEN)
      lru_out = lru_mem[A];
  end

//write port  
always @(posedge CLK_)
  begin
    if(!CEN & !WEN)
      lru_mem[A] <= D;
  end 
  
endmodule
