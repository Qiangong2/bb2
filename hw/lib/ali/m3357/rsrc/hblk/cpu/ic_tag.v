/*******************************************************************************
              Project Name : T2risc1H
              File Name : ic_tag.v
              Create Date : 2002/02/18
              Author : Charliema
              Description :behavior module for i_cache tag
              Revision History :
              02/18/2002   charlie       initial creation
*******************************************************************************/
//`include	"defines.h"
//`celldefine
module ITAG6304  (
    // Output ports
    IC_SRAM_TAG_OUT,
    IC_SRAM_LRU_OUT,
    IC_SRAM_HIT_0,
    IC_SRAM_HIT_1,
    IC_SRAM_HIT_2,
    IC_SRAM_HIT_3,
    IC_SRAM_TAG0_ST_OUT,		// instruction cache tag0 valid & lock bit out
    IC_SRAM_TAG1_ST_OUT,
    IC_SRAM_TAG2_ST_OUT,
    IC_SRAM_TAG3_ST_OUT,
    IC_SRAM_TAG0_OUT,       // tag0 output before output reg latch, for m6303
    IC_SRAM_TAG1_OUT,
    IC_SRAM_TAG2_OUT,
    IC_SRAM_TAG3_OUT,
    IC_HIT_VEC,            // hit vec for compare, rsim not use it.
    IC_LRU_OUT,            // lru out before output reg latch, for m6303.

    // Input ports
    IC_SRAM_TAG_IN,
    IC_TAG_ADDR_IN,
    IC_SRAM_LRU_IN,
    IC_SRAM_TAG_CS_,
    IC_SRAM_WR_,
    IC_SRAM_INVLD_,
    IC_SRAM_INDEXOP_,
    IC_SRAM_WAY_SEL,
    IC_TAG_PADDR,
    IC_OUTPUT_ENABLE,
    IC_WAY_OPTION,
    IC_WAY_ADDR,
    BIST_HOLD,            // hold signal from bist-control.
                          // if valid, hold the input & output registers
    BIST_DIAG,            // diagnostic signal.
                          // if valid when BIST_HOLD active, scan out the input & output registers.
    SCAN_MODE,            // bypass icache sram enable.
                          // 0 --> normal mode,     1 --> bypass mode.
    SE,                   // scan enable signal.
                          // if valid, boundary scan start.
    SO,                   // scan output.
    SI,                   // scan input.
    CLK
    );

    // Output ports
    output [`ict_size-1:0]   IC_SRAM_TAG_OUT;
    output [`wide-1:0]       IC_SRAM_LRU_OUT;
    output                   IC_SRAM_HIT_0;
    output                   IC_SRAM_HIT_1;
    output                   IC_SRAM_HIT_2;
    output                   IC_SRAM_HIT_3;
    output [1:0]             IC_SRAM_TAG0_ST_OUT;
    output [1:0]             IC_SRAM_TAG1_ST_OUT;
    output [1:0]             IC_SRAM_TAG2_ST_OUT;
    output [1:0]             IC_SRAM_TAG3_ST_OUT;
    output [`ict_size-1:0]   IC_SRAM_TAG0_OUT;
    output [`ict_size-1:0]   IC_SRAM_TAG1_OUT;
    output [`ict_size-1:0]   IC_SRAM_TAG2_OUT;
    output [`ict_size-1:0]   IC_SRAM_TAG3_OUT;
    output [`wide-1:0]       IC_LRU_OUT;
    output [3:0]             IC_HIT_VEC;

    // Input ports
    input [`ict_size-1:0]    IC_SRAM_TAG_IN;
    input [`addr_width-6:0]  IC_TAG_ADDR_IN;
    input [`wide-1:0]        IC_SRAM_LRU_IN;
    input                    IC_SRAM_TAG_CS_;
    input                    IC_SRAM_WR_;
    input                    IC_SRAM_INVLD_;
    input                    IC_SRAM_INDEXOP_;
    input [1:0]              IC_SRAM_WAY_SEL;
    input [`ict_size-3:0]    IC_TAG_PADDR;
    input                    IC_OUTPUT_ENABLE;
    input                    IC_WAY_OPTION;
    input                    IC_WAY_ADDR;
    input                    BIST_HOLD;
    input                    BIST_DIAG;
    input                    SE;
    input                    SI;
    output                   SO;
    input                    SCAN_MODE;
    input                    CLK;

	  parameter               latch_delay = 0.5;

/***********************************
  declare input/output wire variable
***********************************/
    wire [`ict_size-1:0]   IC_SRAM_TAG_OUT;
    wire [`wide-1:0]       IC_SRAM_LRU_OUT;
    wire                   IC_SRAM_HIT_0;
    wire                   IC_SRAM_HIT_1;
    wire                   IC_SRAM_HIT_2;
    wire                   IC_SRAM_HIT_3;
    wire [1:0]             IC_SRAM_TAG0_ST_OUT;
    wire [1:0]             IC_SRAM_TAG1_ST_OUT;
    wire [1:0]             IC_SRAM_TAG2_ST_OUT;
    wire [1:0]             IC_SRAM_TAG3_ST_OUT;
    wire [`ict_size-1:0]   IC_SRAM_TAG0_OUT;
    wire [`ict_size-1:0]   IC_SRAM_TAG1_OUT;
    wire [`ict_size-1:0]   IC_SRAM_TAG2_OUT;
    wire [`ict_size-1:0]   IC_SRAM_TAG3_OUT;
    wire [`wide-1:0]       IC_LRU_OUT;
    wire [3:0]             IC_HIT_VEC;
    wire                    SO;

    // Input ports
    wire [`ict_size-1:0]    IC_SRAM_TAG_IN;
    wire [`addr_width-6:0]  IC_TAG_ADDR_IN;
    wire [`wide-1:0]        IC_SRAM_LRU_IN;
    wire                    IC_SRAM_TAG_CS_;
    wire                    IC_SRAM_WR_;
    wire                    IC_SRAM_INVLD_;
    wire                    IC_SRAM_INDEXOP_;
    wire [1:0]              IC_SRAM_WAY_SEL;
    wire [`ict_size-3:0]    IC_TAG_PADDR;
    wire                    IC_OUTPUT_ENABLE;
    wire                    IC_WAY_OPTION;
    wire                    IC_WAY_ADDR;
    wire                    BIST_HOLD;
    wire                    BIST_DIAG;
    wire                    SE;
    wire                    SI;
    wire                    SCAN_MODE;
    wire                    CLK;

// internal output wire(i)
    wire [`ict_size-1:0]   IC_SRAM_TAG_OUTi;
    wire [`ict_size-1:0]   IC_SRAM_TAG0_OUTi;
    wire [`ict_size-1:0]   IC_SRAM_TAG1_OUTi;
    wire [`ict_size-1:0]   IC_SRAM_TAG2_OUTi;
    wire [`ict_size-1:0]   IC_SRAM_TAG3_OUTi;
    wire [3:0]             IC_HIT_VECi;
    wire                    SOi;

// internal input wire(i)
    wire [`ict_size-1:0]    IC_SRAM_TAG_INi;
    wire [`addr_width-6:0]  IC_TAG_ADDR_INi;
    wire [`wide-1:0]        IC_SRAM_LRU_INi;
    wire                    IC_SRAM_TAG_CSi_;
    wire                    IC_SRAM_WRi_;
    wire                    IC_SRAM_INVLDi_;
    wire                    IC_SRAM_INDEXOPi_;
    wire [1:0]              IC_SRAM_WAY_SELi;
    wire [`ict_size-3:0]    IC_TAG_PADDRi;
    wire                    IC_OUTPUT_ENABLEi;
    wire                    IC_WAY_OPTIONi;
    wire                    IC_WAY_ADDRi;
    wire                    BIST_HOLDi;
    wire                    BIST_DIAGi;
    wire                    SEi;
    wire                    SIi;
    wire                    SCAN_MODEi;
    wire                    CLKi;

    reg                     IC_SRAM_HIT_0ii;
    reg                     IC_SRAM_HIT_1ii;
    reg                     IC_SRAM_HIT_2ii;
    reg                     IC_SRAM_HIT_3ii;
    reg [`wide-1:0]         IC_SRAM_LRU_OUTii;
    reg [1:0]               IC_SRAM_TAG0_ST_OUTii;
    reg [1:0]               IC_SRAM_TAG1_ST_OUTii;
    reg [1:0]               IC_SRAM_TAG2_ST_OUTii;
    reg [1:0]               IC_SRAM_TAG3_ST_OUTii;
    reg [`wide-1:0]         IC_LRU_OUTii;

    wire                     IC_SRAM_HIT_0i = IC_SRAM_HIT_0ii;
    wire                     IC_SRAM_HIT_1i = IC_SRAM_HIT_1ii;
    wire                     IC_SRAM_HIT_2i = IC_SRAM_HIT_2ii;
    wire                     IC_SRAM_HIT_3i = IC_SRAM_HIT_3ii;
    wire [`wide-1:0]         IC_SRAM_LRU_OUTi = IC_SRAM_LRU_OUTii;
    wire [1:0]               IC_SRAM_TAG0_ST_OUTi = IC_SRAM_TAG0_ST_OUTii;
    wire [1:0]               IC_SRAM_TAG1_ST_OUTi = IC_SRAM_TAG1_ST_OUTii;
    wire [1:0]               IC_SRAM_TAG2_ST_OUTi = IC_SRAM_TAG2_ST_OUTii;
    wire [1:0]               IC_SRAM_TAG3_ST_OUTi = IC_SRAM_TAG3_ST_OUTii;
    wire [`wide-1:0]         IC_LRU_OUTi = IC_LRU_OUTii;

// buf declare
	  buf(IC_SRAM_TAG_OUT[21],IC_SRAM_TAG_OUTi[21]);
	  buf(IC_SRAM_TAG_OUT[20],IC_SRAM_TAG_OUTi[20]);
	  buf(IC_SRAM_TAG_OUT[19],IC_SRAM_TAG_OUTi[19]);
	  buf(IC_SRAM_TAG_OUT[18],IC_SRAM_TAG_OUTi[18]);
	  buf(IC_SRAM_TAG_OUT[17],IC_SRAM_TAG_OUTi[17]);
	  buf(IC_SRAM_TAG_OUT[16],IC_SRAM_TAG_OUTi[16]);
	  buf(IC_SRAM_TAG_OUT[15],IC_SRAM_TAG_OUTi[15]);
	  buf(IC_SRAM_TAG_OUT[14],IC_SRAM_TAG_OUTi[14]);
	  buf(IC_SRAM_TAG_OUT[13],IC_SRAM_TAG_OUTi[13]);
	  buf(IC_SRAM_TAG_OUT[12],IC_SRAM_TAG_OUTi[12]);
	  buf(IC_SRAM_TAG_OUT[11],IC_SRAM_TAG_OUTi[11]);
	  buf(IC_SRAM_TAG_OUT[10],IC_SRAM_TAG_OUTi[10]);
	  buf(IC_SRAM_TAG_OUT[9 ],IC_SRAM_TAG_OUTi[9 ]);
	  buf(IC_SRAM_TAG_OUT[8 ],IC_SRAM_TAG_OUTi[8 ]);
	  buf(IC_SRAM_TAG_OUT[7 ],IC_SRAM_TAG_OUTi[7 ]);
	  buf(IC_SRAM_TAG_OUT[6 ],IC_SRAM_TAG_OUTi[6 ]);
	  buf(IC_SRAM_TAG_OUT[5 ],IC_SRAM_TAG_OUTi[5 ]);
	  buf(IC_SRAM_TAG_OUT[4 ],IC_SRAM_TAG_OUTi[4 ]);
	  buf(IC_SRAM_TAG_OUT[3 ],IC_SRAM_TAG_OUTi[3 ]);
	  buf(IC_SRAM_TAG_OUT[2 ],IC_SRAM_TAG_OUTi[2 ]);
	  buf(IC_SRAM_TAG_OUT[1 ],IC_SRAM_TAG_OUTi[1 ]);
	  buf(IC_SRAM_TAG_OUT[0 ],IC_SRAM_TAG_OUTi[0 ]);
    buf(IC_SRAM_LRU_OUT[1],IC_SRAM_LRU_OUTi[1]);
    buf(IC_SRAM_LRU_OUT[0],IC_SRAM_LRU_OUTi[0]);
    buf(IC_SRAM_HIT_0,IC_SRAM_HIT_0i);
    buf(IC_SRAM_HIT_1,IC_SRAM_HIT_1i);
    buf(IC_SRAM_HIT_2,IC_SRAM_HIT_2i);
    buf(IC_SRAM_HIT_3,IC_SRAM_HIT_3i);
    buf(IC_SRAM_TAG0_ST_OUT[1],IC_SRAM_TAG0_ST_OUTi[1]);
    buf(IC_SRAM_TAG0_ST_OUT[0],IC_SRAM_TAG0_ST_OUTi[0]);
    buf(IC_SRAM_TAG1_ST_OUT[1],IC_SRAM_TAG1_ST_OUTi[1]);
    buf(IC_SRAM_TAG1_ST_OUT[0],IC_SRAM_TAG1_ST_OUTi[0]);
    buf(IC_SRAM_TAG2_ST_OUT[1],IC_SRAM_TAG2_ST_OUTi[1]);
    buf(IC_SRAM_TAG2_ST_OUT[0],IC_SRAM_TAG2_ST_OUTi[0]);
    buf(IC_SRAM_TAG3_ST_OUT[1],IC_SRAM_TAG3_ST_OUTi[1]);
    buf(IC_SRAM_TAG3_ST_OUT[0],IC_SRAM_TAG3_ST_OUTi[0]);
	  buf(IC_SRAM_TAG0_OUT[21],IC_SRAM_TAG0_OUTi[21]);
	  buf(IC_SRAM_TAG0_OUT[20],IC_SRAM_TAG0_OUTi[20]);
	  buf(IC_SRAM_TAG0_OUT[19],IC_SRAM_TAG0_OUTi[19]);
	  buf(IC_SRAM_TAG0_OUT[18],IC_SRAM_TAG0_OUTi[18]);
	  buf(IC_SRAM_TAG0_OUT[17],IC_SRAM_TAG0_OUTi[17]);
	  buf(IC_SRAM_TAG0_OUT[16],IC_SRAM_TAG0_OUTi[16]);
	  buf(IC_SRAM_TAG0_OUT[15],IC_SRAM_TAG0_OUTi[15]);
	  buf(IC_SRAM_TAG0_OUT[14],IC_SRAM_TAG0_OUTi[14]);
	  buf(IC_SRAM_TAG0_OUT[13],IC_SRAM_TAG0_OUTi[13]);
	  buf(IC_SRAM_TAG0_OUT[12],IC_SRAM_TAG0_OUTi[12]);
	  buf(IC_SRAM_TAG0_OUT[11],IC_SRAM_TAG0_OUTi[11]);
	  buf(IC_SRAM_TAG0_OUT[10],IC_SRAM_TAG0_OUTi[10]);
	  buf(IC_SRAM_TAG0_OUT[9 ],IC_SRAM_TAG0_OUTi[9 ]);
	  buf(IC_SRAM_TAG0_OUT[8 ],IC_SRAM_TAG0_OUTi[8 ]);
	  buf(IC_SRAM_TAG0_OUT[7 ],IC_SRAM_TAG0_OUTi[7 ]);
	  buf(IC_SRAM_TAG0_OUT[6 ],IC_SRAM_TAG0_OUTi[6 ]);
	  buf(IC_SRAM_TAG0_OUT[5 ],IC_SRAM_TAG0_OUTi[5 ]);
	  buf(IC_SRAM_TAG0_OUT[4 ],IC_SRAM_TAG0_OUTi[4 ]);
	  buf(IC_SRAM_TAG0_OUT[3 ],IC_SRAM_TAG0_OUTi[3 ]);
	  buf(IC_SRAM_TAG0_OUT[2 ],IC_SRAM_TAG0_OUTi[2 ]);
	  buf(IC_SRAM_TAG0_OUT[1 ],IC_SRAM_TAG0_OUTi[1 ]);
	  buf(IC_SRAM_TAG0_OUT[0 ],IC_SRAM_TAG0_OUTi[0 ]);
	  buf(IC_SRAM_TAG1_OUT[21],IC_SRAM_TAG1_OUTi[21]);
	  buf(IC_SRAM_TAG1_OUT[20],IC_SRAM_TAG1_OUTi[20]);
	  buf(IC_SRAM_TAG1_OUT[19],IC_SRAM_TAG1_OUTi[19]);
	  buf(IC_SRAM_TAG1_OUT[18],IC_SRAM_TAG1_OUTi[18]);
	  buf(IC_SRAM_TAG1_OUT[17],IC_SRAM_TAG1_OUTi[17]);
	  buf(IC_SRAM_TAG1_OUT[16],IC_SRAM_TAG1_OUTi[16]);
	  buf(IC_SRAM_TAG1_OUT[15],IC_SRAM_TAG1_OUTi[15]);
	  buf(IC_SRAM_TAG1_OUT[14],IC_SRAM_TAG1_OUTi[14]);
	  buf(IC_SRAM_TAG1_OUT[13],IC_SRAM_TAG1_OUTi[13]);
	  buf(IC_SRAM_TAG1_OUT[12],IC_SRAM_TAG1_OUTi[12]);
	  buf(IC_SRAM_TAG1_OUT[11],IC_SRAM_TAG1_OUTi[11]);
	  buf(IC_SRAM_TAG1_OUT[10],IC_SRAM_TAG1_OUTi[10]);
	  buf(IC_SRAM_TAG1_OUT[9 ],IC_SRAM_TAG1_OUTi[9 ]);
	  buf(IC_SRAM_TAG1_OUT[8 ],IC_SRAM_TAG1_OUTi[8 ]);
	  buf(IC_SRAM_TAG1_OUT[7 ],IC_SRAM_TAG1_OUTi[7 ]);
	  buf(IC_SRAM_TAG1_OUT[6 ],IC_SRAM_TAG1_OUTi[6 ]);
	  buf(IC_SRAM_TAG1_OUT[5 ],IC_SRAM_TAG1_OUTi[5 ]);
	  buf(IC_SRAM_TAG1_OUT[4 ],IC_SRAM_TAG1_OUTi[4 ]);
	  buf(IC_SRAM_TAG1_OUT[3 ],IC_SRAM_TAG1_OUTi[3 ]);
	  buf(IC_SRAM_TAG1_OUT[2 ],IC_SRAM_TAG1_OUTi[2 ]);
	  buf(IC_SRAM_TAG1_OUT[1 ],IC_SRAM_TAG1_OUTi[1 ]);
	  buf(IC_SRAM_TAG1_OUT[0 ],IC_SRAM_TAG1_OUTi[0 ]);
	  buf(IC_SRAM_TAG2_OUT[21],IC_SRAM_TAG2_OUTi[21]);
	  buf(IC_SRAM_TAG2_OUT[20],IC_SRAM_TAG2_OUTi[20]);
	  buf(IC_SRAM_TAG2_OUT[19],IC_SRAM_TAG2_OUTi[19]);
	  buf(IC_SRAM_TAG2_OUT[18],IC_SRAM_TAG2_OUTi[18]);
	  buf(IC_SRAM_TAG2_OUT[17],IC_SRAM_TAG2_OUTi[17]);
	  buf(IC_SRAM_TAG2_OUT[16],IC_SRAM_TAG2_OUTi[16]);
	  buf(IC_SRAM_TAG2_OUT[15],IC_SRAM_TAG2_OUTi[15]);
	  buf(IC_SRAM_TAG2_OUT[14],IC_SRAM_TAG2_OUTi[14]);
	  buf(IC_SRAM_TAG2_OUT[13],IC_SRAM_TAG2_OUTi[13]);
	  buf(IC_SRAM_TAG2_OUT[12],IC_SRAM_TAG2_OUTi[12]);
	  buf(IC_SRAM_TAG2_OUT[11],IC_SRAM_TAG2_OUTi[11]);
	  buf(IC_SRAM_TAG2_OUT[10],IC_SRAM_TAG2_OUTi[10]);
	  buf(IC_SRAM_TAG2_OUT[9 ],IC_SRAM_TAG2_OUTi[9 ]);
	  buf(IC_SRAM_TAG2_OUT[8 ],IC_SRAM_TAG2_OUTi[8 ]);
	  buf(IC_SRAM_TAG2_OUT[7 ],IC_SRAM_TAG2_OUTi[7 ]);
	  buf(IC_SRAM_TAG2_OUT[6 ],IC_SRAM_TAG2_OUTi[6 ]);
	  buf(IC_SRAM_TAG2_OUT[5 ],IC_SRAM_TAG2_OUTi[5 ]);
	  buf(IC_SRAM_TAG2_OUT[4 ],IC_SRAM_TAG2_OUTi[4 ]);
	  buf(IC_SRAM_TAG2_OUT[3 ],IC_SRAM_TAG2_OUTi[3 ]);
	  buf(IC_SRAM_TAG2_OUT[2 ],IC_SRAM_TAG2_OUTi[2 ]);
	  buf(IC_SRAM_TAG2_OUT[1 ],IC_SRAM_TAG2_OUTi[1 ]);
	  buf(IC_SRAM_TAG2_OUT[0 ],IC_SRAM_TAG2_OUTi[0 ]);
	  buf(IC_SRAM_TAG3_OUT[21],IC_SRAM_TAG3_OUTi[21]);
	  buf(IC_SRAM_TAG3_OUT[20],IC_SRAM_TAG3_OUTi[20]);
	  buf(IC_SRAM_TAG3_OUT[19],IC_SRAM_TAG3_OUTi[19]);
	  buf(IC_SRAM_TAG3_OUT[18],IC_SRAM_TAG3_OUTi[18]);
	  buf(IC_SRAM_TAG3_OUT[17],IC_SRAM_TAG3_OUTi[17]);
	  buf(IC_SRAM_TAG3_OUT[16],IC_SRAM_TAG3_OUTi[16]);
	  buf(IC_SRAM_TAG3_OUT[15],IC_SRAM_TAG3_OUTi[15]);
	  buf(IC_SRAM_TAG3_OUT[14],IC_SRAM_TAG3_OUTi[14]);
	  buf(IC_SRAM_TAG3_OUT[13],IC_SRAM_TAG3_OUTi[13]);
	  buf(IC_SRAM_TAG3_OUT[12],IC_SRAM_TAG3_OUTi[12]);
	  buf(IC_SRAM_TAG3_OUT[11],IC_SRAM_TAG3_OUTi[11]);
	  buf(IC_SRAM_TAG3_OUT[10],IC_SRAM_TAG3_OUTi[10]);
	  buf(IC_SRAM_TAG3_OUT[9 ],IC_SRAM_TAG3_OUTi[9 ]);
	  buf(IC_SRAM_TAG3_OUT[8 ],IC_SRAM_TAG3_OUTi[8 ]);
	  buf(IC_SRAM_TAG3_OUT[7 ],IC_SRAM_TAG3_OUTi[7 ]);
	  buf(IC_SRAM_TAG3_OUT[6 ],IC_SRAM_TAG3_OUTi[6 ]);
	  buf(IC_SRAM_TAG3_OUT[5 ],IC_SRAM_TAG3_OUTi[5 ]);
	  buf(IC_SRAM_TAG3_OUT[4 ],IC_SRAM_TAG3_OUTi[4 ]);
	  buf(IC_SRAM_TAG3_OUT[3 ],IC_SRAM_TAG3_OUTi[3 ]);
	  buf(IC_SRAM_TAG3_OUT[2 ],IC_SRAM_TAG3_OUTi[2 ]);
	  buf(IC_SRAM_TAG3_OUT[1 ],IC_SRAM_TAG3_OUTi[1 ]);
	  buf(IC_SRAM_TAG3_OUT[0 ],IC_SRAM_TAG3_OUTi[0 ]);
    buf(IC_LRU_OUT[1],IC_LRU_OUTi[1]);
    buf(IC_LRU_OUT[0],IC_LRU_OUTi[0]);
    buf(IC_HIT_VEC[3],IC_HIT_VECi[3]);
    buf(IC_HIT_VEC[2],IC_HIT_VECi[2]);
    buf(IC_HIT_VEC[1],IC_HIT_VECi[1]);
    buf(IC_HIT_VEC[0],IC_HIT_VECi[0]);
    buf(SO,SOi);
	  buf(IC_SRAM_TAG_INi[21],IC_SRAM_TAG_IN[21]);
	  buf(IC_SRAM_TAG_INi[20],IC_SRAM_TAG_IN[20]);
	  buf(IC_SRAM_TAG_INi[19],IC_SRAM_TAG_IN[19]);
	  buf(IC_SRAM_TAG_INi[18],IC_SRAM_TAG_IN[18]);
	  buf(IC_SRAM_TAG_INi[17],IC_SRAM_TAG_IN[17]);
	  buf(IC_SRAM_TAG_INi[16],IC_SRAM_TAG_IN[16]);
	  buf(IC_SRAM_TAG_INi[15],IC_SRAM_TAG_IN[15]);
	  buf(IC_SRAM_TAG_INi[14],IC_SRAM_TAG_IN[14]);
	  buf(IC_SRAM_TAG_INi[13],IC_SRAM_TAG_IN[13]);
	  buf(IC_SRAM_TAG_INi[12],IC_SRAM_TAG_IN[12]);
	  buf(IC_SRAM_TAG_INi[11],IC_SRAM_TAG_IN[11]);
	  buf(IC_SRAM_TAG_INi[10],IC_SRAM_TAG_IN[10]);
	  buf(IC_SRAM_TAG_INi[9 ],IC_SRAM_TAG_IN[9 ]);
	  buf(IC_SRAM_TAG_INi[8 ],IC_SRAM_TAG_IN[8 ]);
	  buf(IC_SRAM_TAG_INi[7 ],IC_SRAM_TAG_IN[7 ]);
	  buf(IC_SRAM_TAG_INi[6 ],IC_SRAM_TAG_IN[6 ]);
	  buf(IC_SRAM_TAG_INi[5 ],IC_SRAM_TAG_IN[5 ]);
	  buf(IC_SRAM_TAG_INi[4 ],IC_SRAM_TAG_IN[4 ]);
	  buf(IC_SRAM_TAG_INi[3 ],IC_SRAM_TAG_IN[3 ]);
	  buf(IC_SRAM_TAG_INi[2 ],IC_SRAM_TAG_IN[2 ]);
	  buf(IC_SRAM_TAG_INi[1 ],IC_SRAM_TAG_IN[1 ]);
	  buf(IC_SRAM_TAG_INi[0 ],IC_SRAM_TAG_IN[0 ]);
	  buf(IC_TAG_ADDR_INi[6 ],IC_TAG_ADDR_IN[6 ]);
	  buf(IC_TAG_ADDR_INi[5 ],IC_TAG_ADDR_IN[5 ]);
	  buf(IC_TAG_ADDR_INi[4 ],IC_TAG_ADDR_IN[4 ]);
	  buf(IC_TAG_ADDR_INi[3 ],IC_TAG_ADDR_IN[3 ]);
	  buf(IC_TAG_ADDR_INi[2 ],IC_TAG_ADDR_IN[2 ]);
	  buf(IC_TAG_ADDR_INi[1 ],IC_TAG_ADDR_IN[1 ]);
	  buf(IC_TAG_ADDR_INi[0 ],IC_TAG_ADDR_IN[0 ]);
 	  buf(IC_SRAM_LRU_INi[1 ],IC_SRAM_LRU_IN[1 ]);
	  buf(IC_SRAM_LRU_INi[0 ],IC_SRAM_LRU_IN[0 ]);
	  buf(IC_SRAM_TAG_CSi_,IC_SRAM_TAG_CS_);
	  buf(IC_SRAM_INVLDi_,IC_SRAM_INVLD_);
    buf(IC_SRAM_WRi_,IC_SRAM_WR_);
    buf(IC_SRAM_INDEXOPi_,IC_SRAM_INDEXOP_);
    buf(IC_SRAM_WAY_SELi[1],IC_SRAM_WAY_SEL[1]);
    buf(IC_SRAM_WAY_SELi[0],IC_SRAM_WAY_SEL[0]);
    buf(IC_TAG_PADDRi[19],IC_TAG_PADDR[19]);
    buf(IC_TAG_PADDRi[18],IC_TAG_PADDR[18]);
    buf(IC_TAG_PADDRi[17],IC_TAG_PADDR[17]);
    buf(IC_TAG_PADDRi[16],IC_TAG_PADDR[16]);
    buf(IC_TAG_PADDRi[15],IC_TAG_PADDR[15]);
    buf(IC_TAG_PADDRi[14],IC_TAG_PADDR[14]);
    buf(IC_TAG_PADDRi[13],IC_TAG_PADDR[13]);
    buf(IC_TAG_PADDRi[12],IC_TAG_PADDR[12]);
    buf(IC_TAG_PADDRi[11],IC_TAG_PADDR[11]);
    buf(IC_TAG_PADDRi[10],IC_TAG_PADDR[10]);
    buf(IC_TAG_PADDRi[9 ],IC_TAG_PADDR[9 ]);
    buf(IC_TAG_PADDRi[8 ],IC_TAG_PADDR[8 ]);
    buf(IC_TAG_PADDRi[7 ],IC_TAG_PADDR[7 ]);
    buf(IC_TAG_PADDRi[6 ],IC_TAG_PADDR[6 ]);
    buf(IC_TAG_PADDRi[5 ],IC_TAG_PADDR[5 ]);
    buf(IC_TAG_PADDRi[4 ],IC_TAG_PADDR[4 ]);
    buf(IC_TAG_PADDRi[3 ],IC_TAG_PADDR[3 ]);
    buf(IC_TAG_PADDRi[2 ],IC_TAG_PADDR[2 ]);
    buf(IC_TAG_PADDRi[1 ],IC_TAG_PADDR[1 ]);
    buf(IC_TAG_PADDRi[0 ],IC_TAG_PADDR[0 ]);
    buf(IC_OUTPUT_ENABLEi,IC_OUTPUT_ENABLE);
    buf(IC_WAY_OPTIONi,IC_WAY_OPTION);
    buf(IC_WAY_ADDRi,IC_WAY_ADDR);
    buf(BIST_HOLDi,BIST_HOLD);
    buf(BIST_DIAGi,BIST_DIAG);
    buf(SEi,SE);
    buf(SIi,SI);
    buf(SCAN_MODEi,SCAN_MODE);
    buf(CLKi,CLK);

    /*************************************
      define notify violation register
    *************************************/


    reg     NOT_TAG_IN00;
    reg     NOT_TAG_IN01;
    reg     NOT_TAG_IN02;
    reg     NOT_TAG_IN03;
    reg     NOT_TAG_IN04;
    reg     NOT_TAG_IN05;
    reg     NOT_TAG_IN06;
    reg     NOT_TAG_IN07;
    reg     NOT_TAG_IN08;
    reg     NOT_TAG_IN09;
    reg     NOT_TAG_IN10;
    reg     NOT_TAG_IN11;
    reg     NOT_TAG_IN12;
    reg     NOT_TAG_IN13;
    reg     NOT_TAG_IN14;
    reg     NOT_TAG_IN15;
    reg     NOT_TAG_IN16;
    reg     NOT_TAG_IN17;
    reg     NOT_TAG_IN18;
    reg     NOT_TAG_IN19;
    reg     NOT_TAG_IN20;
    reg     NOT_TAG_IN21;
    reg     NOT_ADDR_IN00;
    reg     NOT_ADDR_IN01;
    reg     NOT_ADDR_IN02;
    reg     NOT_ADDR_IN03;
    reg     NOT_ADDR_IN04;
    reg     NOT_ADDR_IN05;
    reg     NOT_ADDR_IN06;
    reg     NOT_LRU_IN1;
    reg     NOT_LRU_IN0;

    reg     NOT_TAG_CS;
    reg     NOT_SRAM_WR;
    reg     NOT_INVLD;
    reg     NOT_INDEXOP;
    reg     NOT_WAY_SEL0;
    reg     NOT_WAY_SEL1;
    reg     NOT_WAY_OPTION;
    reg     NOT_WAY_ADDR;

    reg    NOT_TAG_PADDR00;
    reg    NOT_TAG_PADDR01;
    reg    NOT_TAG_PADDR02;
    reg    NOT_TAG_PADDR03;
    reg    NOT_TAG_PADDR04;
    reg    NOT_TAG_PADDR05;
    reg    NOT_TAG_PADDR06;
    reg    NOT_TAG_PADDR07;
    reg    NOT_TAG_PADDR08;
    reg    NOT_TAG_PADDR09;
    reg    NOT_TAG_PADDR10;
    reg    NOT_TAG_PADDR11;
    reg    NOT_TAG_PADDR12;
    reg    NOT_TAG_PADDR13;
    reg    NOT_TAG_PADDR14;
    reg    NOT_TAG_PADDR15;
    reg    NOT_TAG_PADDR16;
    reg    NOT_TAG_PADDR17;
    reg    NOT_TAG_PADDR18;
    reg    NOT_TAG_PADDR19;
    reg    NOT_BIST_HOLD;
    reg    NOT_BIST_DIAG;
    reg    NOT_SE;
    reg    NOT_SI;
    reg    NOT_SCAN_MODE;
    reg    NOT_OUTPUT_ENABLE;
    reg    NOT_CLK_PER;
    reg    NOT_CLK_MINH;
    reg    NOT_CLK_MINL;

    wire    cs_flag = ~IC_SRAM_TAG_CSi_;
    /**************************************
                specify block
    **************************************/

  	specify
    /**************************************
            setup hold timing check
    **************************************/

    $setuphold(posedge CLK &&& cs_flag,posedge IC_SRAM_TAG_IN[00],1.000,0.500,NOT_TAG_IN00);
    $setuphold(posedge CLK &&& cs_flag,posedge IC_SRAM_TAG_IN[01],1.000,0.500,NOT_TAG_IN01);
    $setuphold(posedge CLK &&& cs_flag,posedge IC_SRAM_TAG_IN[02],1.000,0.500,NOT_TAG_IN02);
    $setuphold(posedge CLK &&& cs_flag,posedge IC_SRAM_TAG_IN[03],1.000,0.500,NOT_TAG_IN03);
    $setuphold(posedge CLK &&& cs_flag,posedge IC_SRAM_TAG_IN[04],1.000,0.500,NOT_TAG_IN04);
    $setuphold(posedge CLK &&& cs_flag,posedge IC_SRAM_TAG_IN[05],1.000,0.500,NOT_TAG_IN05);
    $setuphold(posedge CLK &&& cs_flag,posedge IC_SRAM_TAG_IN[06],1.000,0.500,NOT_TAG_IN06);
    $setuphold(posedge CLK &&& cs_flag,posedge IC_SRAM_TAG_IN[07],1.000,0.500,NOT_TAG_IN07);
    $setuphold(posedge CLK &&& cs_flag,posedge IC_SRAM_TAG_IN[08],1.000,0.500,NOT_TAG_IN08);
    $setuphold(posedge CLK &&& cs_flag,posedge IC_SRAM_TAG_IN[09],1.000,0.500,NOT_TAG_IN09);
    $setuphold(posedge CLK &&& cs_flag,posedge IC_SRAM_TAG_IN[10],1.000,0.500,NOT_TAG_IN10);
    $setuphold(posedge CLK &&& cs_flag,posedge IC_SRAM_TAG_IN[11],1.000,0.500,NOT_TAG_IN11);
    $setuphold(posedge CLK &&& cs_flag,posedge IC_SRAM_TAG_IN[12],1.000,0.500,NOT_TAG_IN12);
    $setuphold(posedge CLK &&& cs_flag,posedge IC_SRAM_TAG_IN[13],1.000,0.500,NOT_TAG_IN13);
    $setuphold(posedge CLK &&& cs_flag,posedge IC_SRAM_TAG_IN[14],1.000,0.500,NOT_TAG_IN14);
    $setuphold(posedge CLK &&& cs_flag,posedge IC_SRAM_TAG_IN[15],1.000,0.500,NOT_TAG_IN15);
    $setuphold(posedge CLK &&& cs_flag,posedge IC_SRAM_TAG_IN[16],1.000,0.500,NOT_TAG_IN16);
    $setuphold(posedge CLK &&& cs_flag,posedge IC_SRAM_TAG_IN[17],1.000,0.500,NOT_TAG_IN17);
    $setuphold(posedge CLK &&& cs_flag,posedge IC_SRAM_TAG_IN[18],1.000,0.500,NOT_TAG_IN18);
    $setuphold(posedge CLK &&& cs_flag,posedge IC_SRAM_TAG_IN[19],1.000,0.500,NOT_TAG_IN19);
    $setuphold(posedge CLK &&& cs_flag,posedge IC_SRAM_TAG_IN[20],1.000,0.500,NOT_TAG_IN20);
    $setuphold(posedge CLK &&& cs_flag,posedge IC_SRAM_TAG_IN[21],1.000,0.500,NOT_TAG_IN21);

    $setuphold(posedge CLK &&& cs_flag,negedge IC_SRAM_TAG_IN[00],1.000,0.500,NOT_TAG_IN00);
    $setuphold(posedge CLK &&& cs_flag,negedge IC_SRAM_TAG_IN[01],1.000,0.500,NOT_TAG_IN01);
    $setuphold(posedge CLK &&& cs_flag,negedge IC_SRAM_TAG_IN[02],1.000,0.500,NOT_TAG_IN02);
    $setuphold(posedge CLK &&& cs_flag,negedge IC_SRAM_TAG_IN[03],1.000,0.500,NOT_TAG_IN03);
    $setuphold(posedge CLK &&& cs_flag,negedge IC_SRAM_TAG_IN[04],1.000,0.500,NOT_TAG_IN04);
    $setuphold(posedge CLK &&& cs_flag,negedge IC_SRAM_TAG_IN[05],1.000,0.500,NOT_TAG_IN05);
    $setuphold(posedge CLK &&& cs_flag,negedge IC_SRAM_TAG_IN[06],1.000,0.500,NOT_TAG_IN06);
    $setuphold(posedge CLK &&& cs_flag,negedge IC_SRAM_TAG_IN[07],1.000,0.500,NOT_TAG_IN07);
    $setuphold(posedge CLK &&& cs_flag,negedge IC_SRAM_TAG_IN[08],1.000,0.500,NOT_TAG_IN08);
    $setuphold(posedge CLK &&& cs_flag,negedge IC_SRAM_TAG_IN[09],1.000,0.500,NOT_TAG_IN09);
    $setuphold(posedge CLK &&& cs_flag,negedge IC_SRAM_TAG_IN[10],1.000,0.500,NOT_TAG_IN10);
    $setuphold(posedge CLK &&& cs_flag,negedge IC_SRAM_TAG_IN[11],1.000,0.500,NOT_TAG_IN11);
    $setuphold(posedge CLK &&& cs_flag,negedge IC_SRAM_TAG_IN[12],1.000,0.500,NOT_TAG_IN12);
    $setuphold(posedge CLK &&& cs_flag,negedge IC_SRAM_TAG_IN[13],1.000,0.500,NOT_TAG_IN13);
    $setuphold(posedge CLK &&& cs_flag,negedge IC_SRAM_TAG_IN[14],1.000,0.500,NOT_TAG_IN14);
    $setuphold(posedge CLK &&& cs_flag,negedge IC_SRAM_TAG_IN[15],1.000,0.500,NOT_TAG_IN15);
    $setuphold(posedge CLK &&& cs_flag,negedge IC_SRAM_TAG_IN[16],1.000,0.500,NOT_TAG_IN16);
    $setuphold(posedge CLK &&& cs_flag,negedge IC_SRAM_TAG_IN[17],1.000,0.500,NOT_TAG_IN17);
    $setuphold(posedge CLK &&& cs_flag,negedge IC_SRAM_TAG_IN[18],1.000,0.500,NOT_TAG_IN18);
    $setuphold(posedge CLK &&& cs_flag,negedge IC_SRAM_TAG_IN[19],1.000,0.500,NOT_TAG_IN19);
    $setuphold(posedge CLK &&& cs_flag,negedge IC_SRAM_TAG_IN[20],1.000,0.500,NOT_TAG_IN20);
    $setuphold(posedge CLK &&& cs_flag,negedge IC_SRAM_TAG_IN[21],1.000,0.500,NOT_TAG_IN21);

    $setuphold(posedge CLK &&& cs_flag,posedge IC_TAG_ADDR_IN[0],1.000,0.500,NOT_ADDR_IN00);
    $setuphold(posedge CLK &&& cs_flag,posedge IC_TAG_ADDR_IN[1],1.000,0.500,NOT_ADDR_IN01);
    $setuphold(posedge CLK &&& cs_flag,posedge IC_TAG_ADDR_IN[2],1.000,0.500,NOT_ADDR_IN02);
    $setuphold(posedge CLK &&& cs_flag,posedge IC_TAG_ADDR_IN[3],1.000,0.500,NOT_ADDR_IN03);
    $setuphold(posedge CLK &&& cs_flag,posedge IC_TAG_ADDR_IN[4],1.000,0.500,NOT_ADDR_IN04);
    $setuphold(posedge CLK &&& cs_flag,posedge IC_TAG_ADDR_IN[5],1.000,0.500,NOT_ADDR_IN05);
    $setuphold(posedge CLK &&& cs_flag,posedge IC_TAG_ADDR_IN[6],1.000,0.500,NOT_ADDR_IN06);

    $setuphold(posedge CLK &&& cs_flag,negedge IC_TAG_ADDR_IN[0],1.000,0.500,NOT_ADDR_IN00);
    $setuphold(posedge CLK &&& cs_flag,negedge IC_TAG_ADDR_IN[1],1.000,0.500,NOT_ADDR_IN01);
    $setuphold(posedge CLK &&& cs_flag,negedge IC_TAG_ADDR_IN[2],1.000,0.500,NOT_ADDR_IN02);
    $setuphold(posedge CLK &&& cs_flag,negedge IC_TAG_ADDR_IN[3],1.000,0.500,NOT_ADDR_IN03);
    $setuphold(posedge CLK &&& cs_flag,negedge IC_TAG_ADDR_IN[4],1.000,0.500,NOT_ADDR_IN04);
    $setuphold(posedge CLK &&& cs_flag,negedge IC_TAG_ADDR_IN[5],1.000,0.500,NOT_ADDR_IN05);
    $setuphold(posedge CLK &&& cs_flag,negedge IC_TAG_ADDR_IN[6],1.000,0.500,NOT_ADDR_IN06);

    $setuphold(posedge CLK &&& cs_flag,posedge IC_SRAM_LRU_IN[1],1.000,0.500,NOT_LRU_IN1);
    $setuphold(posedge CLK &&& cs_flag,posedge IC_SRAM_LRU_IN[0],1.000,0.500,NOT_LRU_IN0);
    $setuphold(posedge CLK &&& cs_flag,negedge IC_SRAM_LRU_IN[1],1.000,0.500,NOT_LRU_IN1);
    $setuphold(posedge CLK &&& cs_flag,negedge IC_SRAM_LRU_IN[0],1.000,0.500,NOT_LRU_IN0);

    $setuphold(posedge CLK &&& cs_flag,posedge IC_SRAM_INVLD_,1.000,0.500,NOT_INVLD);
    $setuphold(posedge CLK &&& cs_flag,negedge IC_SRAM_INVLD_,1.000,0.500,NOT_INVLD);
    $setuphold(posedge CLK &&& cs_flag,posedge IC_SRAM_INDEXOP_,1.000,0.500,NOT_INDEXOP);
    $setuphold(posedge CLK &&& cs_flag,negedge IC_SRAM_INDEXOP_,1.000,0.500,NOT_INDEXOP);
    $setuphold(posedge CLK &&& cs_flag,posedge IC_SRAM_WAY_SEL[0],1.000,0.500,NOT_WAY_SEL0);
    $setuphold(posedge CLK &&& cs_flag,negedge IC_SRAM_WAY_SEL[0],1.000,0.500,NOT_WAY_SEL0);
    $setuphold(posedge CLK &&& cs_flag,posedge IC_SRAM_WAY_SEL[1],1.000,0.500,NOT_WAY_SEL1);
    $setuphold(posedge CLK &&& cs_flag,negedge IC_SRAM_WAY_SEL[1],1.000,0.500,NOT_WAY_SEL1);
    $setuphold(posedge CLK &&& cs_flag,posedge IC_WAY_OPTION,1.000,0.500,NOT_WAY_OPTION);
    $setuphold(posedge CLK &&& cs_flag,negedge IC_WAY_OPTION,1.000,0.500,NOT_WAY_OPTION);
    $setuphold(posedge CLK &&& cs_flag,posedge IC_WAY_ADDR,1.000,0.500,NOT_WAY_ADDR);
    $setuphold(posedge CLK &&& cs_flag,negedge IC_WAY_ADDR,1.000,0.500,NOT_WAY_ADDR);
    $setuphold(negedge CLK &&& cs_flag,posedge IC_SRAM_INVLD_,1.000,0.500,NOT_INVLD);
    $setuphold(negedge CLK &&& cs_flag,posedge IC_SRAM_INDEXOP_,1.000,0.500,NOT_INDEXOP);
    $setuphold(negedge CLK &&& cs_flag,posedge IC_SRAM_WAY_SEL[0],1.000,0.500,NOT_WAY_SEL0);
    $setuphold(negedge CLK &&& cs_flag,posedge IC_SRAM_WAY_SEL[1],1.000,0.500,NOT_WAY_SEL1);
    $setuphold(negedge CLK &&& cs_flag,posedge IC_WAY_OPTION,1.000,0.500,NOT_WAY_OPTION);
    $setuphold(negedge CLK &&& cs_flag,posedge IC_WAY_ADDR,1.000,0.500,NOT_WAY_ADDR);

    $setuphold(posedge CLK,posedge IC_SRAM_TAG_CS_,1.000,0.500,NOT_TAG_CS);
    $setuphold(posedge CLK,negedge IC_SRAM_TAG_CS_,1.000,0.500,NOT_TAG_CS);
    $setuphold(posedge CLK,negedge IC_SRAM_WR_,1.000,0.500,NOT_SRAM_WR);
    $setuphold(posedge CLK,posedge IC_SRAM_WR_,1.000,0.500,NOT_SRAM_WR);
    $setuphold(posedge CLK,negedge IC_OUTPUT_ENABLE,1.000,0.500,NOT_OUTPUT_ENABLE);
    $setuphold(posedge CLK,posedge IC_OUTPUT_ENABLE,1.000,0.500,NOT_OUTPUT_ENABLE);

    $setuphold(posedge CLK,posedge IC_TAG_PADDR[0 ],1.000,0.500,NOT_TAG_PADDR00);
    $setuphold(posedge CLK,posedge IC_TAG_PADDR[1 ],1.000,0.500,NOT_TAG_PADDR01);
    $setuphold(posedge CLK,posedge IC_TAG_PADDR[2 ],1.000,0.500,NOT_TAG_PADDR02);
    $setuphold(posedge CLK,posedge IC_TAG_PADDR[3 ],1.000,0.500,NOT_TAG_PADDR03);
    $setuphold(posedge CLK,posedge IC_TAG_PADDR[4 ],1.000,0.500,NOT_TAG_PADDR04);
    $setuphold(posedge CLK,posedge IC_TAG_PADDR[5 ],1.000,0.500,NOT_TAG_PADDR05);
    $setuphold(posedge CLK,posedge IC_TAG_PADDR[6 ],1.000,0.500,NOT_TAG_PADDR06);
    $setuphold(posedge CLK,posedge IC_TAG_PADDR[7 ],1.000,0.500,NOT_TAG_PADDR07);
    $setuphold(posedge CLK,posedge IC_TAG_PADDR[8 ],1.000,0.500,NOT_TAG_PADDR08);
    $setuphold(posedge CLK,posedge IC_TAG_PADDR[9 ],1.000,0.500,NOT_TAG_PADDR09);
    $setuphold(posedge CLK,posedge IC_TAG_PADDR[10],1.000,0.500,NOT_TAG_PADDR10);
    $setuphold(posedge CLK,posedge IC_TAG_PADDR[11],1.000,0.500,NOT_TAG_PADDR11);
    $setuphold(posedge CLK,posedge IC_TAG_PADDR[12],1.000,0.500,NOT_TAG_PADDR12);
    $setuphold(posedge CLK,posedge IC_TAG_PADDR[13],1.000,0.500,NOT_TAG_PADDR13);
    $setuphold(posedge CLK,posedge IC_TAG_PADDR[14],1.000,0.500,NOT_TAG_PADDR14);
    $setuphold(posedge CLK,posedge IC_TAG_PADDR[15],1.000,0.500,NOT_TAG_PADDR15);
    $setuphold(posedge CLK,posedge IC_TAG_PADDR[16],1.000,0.500,NOT_TAG_PADDR16);
    $setuphold(posedge CLK,posedge IC_TAG_PADDR[17],1.000,0.500,NOT_TAG_PADDR17);
    $setuphold(posedge CLK,posedge IC_TAG_PADDR[18],1.000,0.500,NOT_TAG_PADDR18);
    $setuphold(posedge CLK,posedge IC_TAG_PADDR[19],1.000,0.500,NOT_TAG_PADDR19);
    $setuphold(negedge CLK,posedge IC_TAG_PADDR[0 ],1.000,0.500,NOT_TAG_PADDR00);
    $setuphold(negedge CLK,posedge IC_TAG_PADDR[1 ],1.000,0.500,NOT_TAG_PADDR01);
    $setuphold(negedge CLK,posedge IC_TAG_PADDR[2 ],1.000,0.500,NOT_TAG_PADDR02);
    $setuphold(negedge CLK,posedge IC_TAG_PADDR[3 ],1.000,0.500,NOT_TAG_PADDR03);
    $setuphold(negedge CLK,posedge IC_TAG_PADDR[4 ],1.000,0.500,NOT_TAG_PADDR04);
    $setuphold(negedge CLK,posedge IC_TAG_PADDR[5 ],1.000,0.500,NOT_TAG_PADDR05);
    $setuphold(negedge CLK,posedge IC_TAG_PADDR[6 ],1.000,0.500,NOT_TAG_PADDR06);
    $setuphold(negedge CLK,posedge IC_TAG_PADDR[7 ],1.000,0.500,NOT_TAG_PADDR07);
    $setuphold(negedge CLK,posedge IC_TAG_PADDR[8 ],1.000,0.500,NOT_TAG_PADDR08);
    $setuphold(negedge CLK,posedge IC_TAG_PADDR[9 ],1.000,0.500,NOT_TAG_PADDR09);
    $setuphold(negedge CLK,posedge IC_TAG_PADDR[10],1.000,0.500,NOT_TAG_PADDR10);
    $setuphold(negedge CLK,posedge IC_TAG_PADDR[11],1.000,0.500,NOT_TAG_PADDR11);
    $setuphold(negedge CLK,posedge IC_TAG_PADDR[12],1.000,0.500,NOT_TAG_PADDR12);
    $setuphold(negedge CLK,posedge IC_TAG_PADDR[13],1.000,0.500,NOT_TAG_PADDR13);
    $setuphold(negedge CLK,posedge IC_TAG_PADDR[14],1.000,0.500,NOT_TAG_PADDR14);
    $setuphold(negedge CLK,posedge IC_TAG_PADDR[15],1.000,0.500,NOT_TAG_PADDR15);
    $setuphold(negedge CLK,posedge IC_TAG_PADDR[16],1.000,0.500,NOT_TAG_PADDR16);
    $setuphold(negedge CLK,posedge IC_TAG_PADDR[17],1.000,0.500,NOT_TAG_PADDR17);
    $setuphold(negedge CLK,posedge IC_TAG_PADDR[18],1.000,0.500,NOT_TAG_PADDR18);
    $setuphold(negedge CLK,posedge IC_TAG_PADDR[19],1.000,0.500,NOT_TAG_PADDR19);

    $setuphold(posedge CLK,negedge IC_TAG_PADDR[0 ],1.000,0.500,NOT_TAG_PADDR00);
    $setuphold(posedge CLK,negedge IC_TAG_PADDR[1 ],1.000,0.500,NOT_TAG_PADDR01);
    $setuphold(posedge CLK,negedge IC_TAG_PADDR[2 ],1.000,0.500,NOT_TAG_PADDR02);
    $setuphold(posedge CLK,negedge IC_TAG_PADDR[3 ],1.000,0.500,NOT_TAG_PADDR03);
    $setuphold(posedge CLK,negedge IC_TAG_PADDR[4 ],1.000,0.500,NOT_TAG_PADDR04);
    $setuphold(posedge CLK,negedge IC_TAG_PADDR[5 ],1.000,0.500,NOT_TAG_PADDR05);
    $setuphold(posedge CLK,negedge IC_TAG_PADDR[6 ],1.000,0.500,NOT_TAG_PADDR06);
    $setuphold(posedge CLK,negedge IC_TAG_PADDR[7 ],1.000,0.500,NOT_TAG_PADDR07);
    $setuphold(posedge CLK,negedge IC_TAG_PADDR[8 ],1.000,0.500,NOT_TAG_PADDR08);
    $setuphold(posedge CLK,negedge IC_TAG_PADDR[9 ],1.000,0.500,NOT_TAG_PADDR09);
    $setuphold(posedge CLK,negedge IC_TAG_PADDR[10],1.000,0.500,NOT_TAG_PADDR10);
    $setuphold(posedge CLK,negedge IC_TAG_PADDR[11],1.000,0.500,NOT_TAG_PADDR11);
    $setuphold(posedge CLK,negedge IC_TAG_PADDR[12],1.000,0.500,NOT_TAG_PADDR12);
    $setuphold(posedge CLK,negedge IC_TAG_PADDR[13],1.000,0.500,NOT_TAG_PADDR13);
    $setuphold(posedge CLK,negedge IC_TAG_PADDR[14],1.000,0.500,NOT_TAG_PADDR14);
    $setuphold(posedge CLK,negedge IC_TAG_PADDR[15],1.000,0.500,NOT_TAG_PADDR15);
    $setuphold(posedge CLK,negedge IC_TAG_PADDR[16],1.000,0.500,NOT_TAG_PADDR16);
    $setuphold(posedge CLK,negedge IC_TAG_PADDR[17],1.000,0.500,NOT_TAG_PADDR17);
    $setuphold(posedge CLK,negedge IC_TAG_PADDR[18],1.000,0.500,NOT_TAG_PADDR18);
    $setuphold(posedge CLK,negedge IC_TAG_PADDR[19],1.000,0.500,NOT_TAG_PADDR19);
    $setuphold(negedge CLK,negedge IC_TAG_PADDR[0 ],1.000,0.500,NOT_TAG_PADDR00);
    $setuphold(negedge CLK,negedge IC_TAG_PADDR[1 ],1.000,0.500,NOT_TAG_PADDR01);
    $setuphold(negedge CLK,negedge IC_TAG_PADDR[2 ],1.000,0.500,NOT_TAG_PADDR02);
    $setuphold(negedge CLK,negedge IC_TAG_PADDR[3 ],1.000,0.500,NOT_TAG_PADDR03);
    $setuphold(negedge CLK,negedge IC_TAG_PADDR[4 ],1.000,0.500,NOT_TAG_PADDR04);
    $setuphold(negedge CLK,negedge IC_TAG_PADDR[5 ],1.000,0.500,NOT_TAG_PADDR05);
    $setuphold(negedge CLK,negedge IC_TAG_PADDR[6 ],1.000,0.500,NOT_TAG_PADDR06);
    $setuphold(negedge CLK,negedge IC_TAG_PADDR[7 ],1.000,0.500,NOT_TAG_PADDR07);
    $setuphold(negedge CLK,negedge IC_TAG_PADDR[8 ],1.000,0.500,NOT_TAG_PADDR08);
    $setuphold(negedge CLK,negedge IC_TAG_PADDR[9 ],1.000,0.500,NOT_TAG_PADDR09);
    $setuphold(negedge CLK,negedge IC_TAG_PADDR[10],1.000,0.500,NOT_TAG_PADDR10);
    $setuphold(negedge CLK,negedge IC_TAG_PADDR[11],1.000,0.500,NOT_TAG_PADDR11);
    $setuphold(negedge CLK,negedge IC_TAG_PADDR[12],1.000,0.500,NOT_TAG_PADDR12);
    $setuphold(negedge CLK,negedge IC_TAG_PADDR[13],1.000,0.500,NOT_TAG_PADDR13);
    $setuphold(negedge CLK,negedge IC_TAG_PADDR[14],1.000,0.500,NOT_TAG_PADDR14);
    $setuphold(negedge CLK,negedge IC_TAG_PADDR[15],1.000,0.500,NOT_TAG_PADDR15);
    $setuphold(negedge CLK,negedge IC_TAG_PADDR[16],1.000,0.500,NOT_TAG_PADDR16);
    $setuphold(negedge CLK,negedge IC_TAG_PADDR[17],1.000,0.500,NOT_TAG_PADDR17);
    $setuphold(negedge CLK,negedge IC_TAG_PADDR[18],1.000,0.500,NOT_TAG_PADDR18);
    $setuphold(negedge CLK,negedge IC_TAG_PADDR[19],1.000,0.500,NOT_TAG_PADDR19);

    $setuphold(posedge CLK,posedge BIST_HOLD ,1.000,0.500,NOT_BIST_HOLD );
    $setuphold(negedge CLK,posedge BIST_HOLD ,1.000,0.500,NOT_BIST_HOLD );
    $setuphold(posedge CLK,negedge BIST_HOLD ,1.000,0.500,NOT_BIST_HOLD );
    $setuphold(negedge CLK,negedge BIST_HOLD ,1.000,0.500,NOT_BIST_HOLD );

    $setuphold(posedge CLK,posedge BIST_DIAG ,1.000,0.500,NOT_BIST_DIAG );
    $setuphold(negedge CLK,posedge BIST_DIAG ,1.000,0.500,NOT_BIST_DIAG );
    $setuphold(posedge CLK,negedge BIST_DIAG ,1.000,0.500,NOT_BIST_DIAG );
    $setuphold(negedge CLK,negedge BIST_DIAG ,1.000,0.500,NOT_BIST_DIAG );

    $setuphold(posedge CLK,posedge SE        ,1.000,0.500,NOT_SE        );
    $setuphold(posedge CLK,negedge SE        ,1.000,0.500,NOT_SE        );
    $setuphold(posedge CLK,posedge SI        ,1.000,0.500,NOT_SI        );
    $setuphold(posedge CLK,negedge SI        ,1.000,0.500,NOT_SI        );
    $setuphold(posedge CLK,posedge SCAN_MODE ,1.000,0.500,NOT_SCAN_MODE );
    $setuphold(posedge CLK,negedge SCAN_MODE ,1.000,0.500,NOT_SCAN_MODE );

        /*===========================
            clock period & width
        ============================*/

        $period(posedge CLK ,3.000,NOT_CLK_PER);
        $width(posedge CLK,1.000,0,NOT_CLK_MINH);
        $width(posedge CLK,1.000,0,NOT_CLK_MINL);
        $period(negedge CLK ,3.000,NOT_CLK_PER);
        $width(negedge CLK,1.000,0,NOT_CLK_MINH);
        $width(negedge CLK,1.000,0,NOT_CLK_MINL);

        /*===========================
            define IOPATH
        ============================*/

        (CLK => IC_SRAM_TAG_OUT[00])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG_OUT[01])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG_OUT[02])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG_OUT[03])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG_OUT[04])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG_OUT[05])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG_OUT[06])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG_OUT[07])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG_OUT[08])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG_OUT[09])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG_OUT[10])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG_OUT[11])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG_OUT[12])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG_OUT[13])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG_OUT[14])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG_OUT[15])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG_OUT[16])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG_OUT[17])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG_OUT[18])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG_OUT[19])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG_OUT[20])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG_OUT[21])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);

        (CLK => IC_SRAM_LRU_OUT[0])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_LRU_OUT[1])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);

        (CLK => IC_SRAM_TAG0_ST_OUT[0])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG0_ST_OUT[1])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG1_ST_OUT[0])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG1_ST_OUT[1])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG2_ST_OUT[0])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG2_ST_OUT[1])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG3_ST_OUT[0])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG3_ST_OUT[1])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);

        (CLK => IC_SRAM_HIT_0)=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_HIT_1)=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_HIT_2)=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_HIT_3)=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);

        (CLK => IC_SRAM_TAG0_OUT[00])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG0_OUT[01])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG0_OUT[02])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG0_OUT[03])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG0_OUT[04])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG0_OUT[05])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG0_OUT[06])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG0_OUT[07])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG0_OUT[08])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG0_OUT[09])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG0_OUT[10])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG0_OUT[11])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG0_OUT[12])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG0_OUT[13])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG0_OUT[14])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG0_OUT[15])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG0_OUT[16])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG0_OUT[17])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG0_OUT[18])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG0_OUT[19])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG0_OUT[20])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG0_OUT[21])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);

        (CLK => IC_SRAM_TAG1_OUT[00])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG1_OUT[01])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG1_OUT[02])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG1_OUT[03])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG1_OUT[04])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG1_OUT[05])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG1_OUT[06])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG1_OUT[07])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG1_OUT[08])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG1_OUT[09])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG1_OUT[10])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG1_OUT[11])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG1_OUT[12])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG1_OUT[13])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG1_OUT[14])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG1_OUT[15])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG1_OUT[16])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG1_OUT[17])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG1_OUT[18])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG1_OUT[19])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG1_OUT[20])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG1_OUT[21])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);

        (CLK => IC_SRAM_TAG2_OUT[00])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG2_OUT[01])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG2_OUT[02])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG2_OUT[03])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG2_OUT[04])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG2_OUT[05])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG2_OUT[06])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG2_OUT[07])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG2_OUT[08])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG2_OUT[09])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG2_OUT[10])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG2_OUT[11])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG2_OUT[12])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG2_OUT[13])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG2_OUT[14])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG2_OUT[15])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG2_OUT[16])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG2_OUT[17])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG2_OUT[18])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG2_OUT[19])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG2_OUT[20])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG2_OUT[21])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);

        (CLK => IC_SRAM_TAG3_OUT[00])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG3_OUT[01])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG3_OUT[02])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG3_OUT[03])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG3_OUT[04])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG3_OUT[05])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG3_OUT[06])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG3_OUT[07])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG3_OUT[08])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG3_OUT[09])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG3_OUT[10])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG3_OUT[11])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG3_OUT[12])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG3_OUT[13])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG3_OUT[14])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG3_OUT[15])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG3_OUT[16])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG3_OUT[17])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG3_OUT[18])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG3_OUT[19])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG3_OUT[20])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_SRAM_TAG3_OUT[21])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);

        (CLK => IC_LRU_OUT[1])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_LRU_OUT[0])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_HIT_VEC[3])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_HIT_VEC[2])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_HIT_VEC[1])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => IC_HIT_VEC[0])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => SO)=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);

	endspecify

    assign SOi = IC_SRAM_LRU_OUTii[0];
// the follow regs are the latch of all inputs
    reg [`ict_size-1:0]     sram_tag_in;
    reg [`addr_width-6:0]   sram_addr_in;
    reg                     sram_tag_cs_;
    reg                     sram_wr_en_;
    reg [`wide-1:0]         sram_lru_in;
    reg                     sram_invld_;
    reg                     sram_indexop_;
    reg [1:0]               sram_way_sel;
    reg                     way_option;
    reg                     way_address;

// latch the input signal,these four latchs is new this version 0907

// the follow five lines code is for verilog simulation, circuit don't care it, just use TAG_PADDR.
//    wire    CLK_LATCH = (~IC_SRAM_TAG_CSi_ | IC_OUTPUT_ENABLEi | BIST_HOLD & BIST_DIAG | SCAN_MODE) & CLKi;
   wire latch_en = ~IC_SRAM_TAG_CSi_ | IC_OUTPUT_ENABLEi | BIST_HOLD & BIST_DIAG | SCAN_MODE;
    reg clk_en;

always @(latch_en or CLK)
    begin
      if(~CLK)
         clk_en = latch_en;
    end

    wire   CLK_LATCH = clk_en & CLK;

    reg [`ict_size-3:0]   tag_paddr;
always @(sram_tag_cs_ or IC_TAG_PADDRi)
  begin
    if (~sram_tag_cs_)
      tag_paddr = IC_TAG_PADDRi;
  end

always @(posedge CLKi)
  begin
    if (~SE & ~BIST_HOLD)
      sram_tag_cs_  <= #`u_dly IC_SRAM_TAG_CSi_;
    else if (SE | BIST_HOLD & BIST_DIAG)
      sram_tag_cs_  <= #`u_dly way_address;
  end

always @(posedge CLK_LATCH)
  begin
    if (~SE & ~BIST_HOLD)
    begin
      if (~IC_SRAM_TAG_CSi_)
      begin
        sram_tag_in   <= #`u_dly IC_SRAM_TAG_INi;
        sram_lru_in   <= #`u_dly IC_SRAM_LRU_INi;
        sram_invld_   <= #`u_dly IC_SRAM_INVLDi_;
        sram_addr_in  <= #`u_dly IC_TAG_ADDR_INi;
        sram_wr_en_   <= #`u_dly IC_SRAM_WRi_;
        sram_indexop_ <= #`u_dly IC_SRAM_INDEXOPi_;
        sram_way_sel  <= #`u_dly IC_SRAM_WAY_SELi;
        way_option    <= #`u_dly IC_WAY_OPTIONi;
        way_address   <= #`u_dly IC_WAY_ADDRi;
      end
    end
    else if (SE | BIST_HOLD & BIST_DIAG)
    begin
      sram_tag_in   <= #`u_dly {IC_SRAM_HIT_0ii, sram_tag_in[21:1]};
      sram_lru_in   <= #`u_dly {sram_tag_in[0], sram_lru_in[1]};
      sram_invld_   <= #`u_dly sram_wr_en_;
      sram_addr_in  <= #`u_dly {sram_way_sel[0], sram_addr_in[6:1]};
      sram_wr_en_   <= #`u_dly sram_tag_cs_;
      sram_indexop_ <= #`u_dly sram_invld_;
      sram_way_sel  <= #`u_dly {sram_indexop_, sram_way_sel[1]};
      way_option    <= #`u_dly SI;
      way_address   <= #`u_dly way_option;
    end
  end

    // internal wire definitions
    wire [`ict_size-1:0]    ic_tag_out;
    reg [1:0]               ic_tag0_st_out;
    reg [1:0]               ic_tag1_st_out;
    reg [1:0]               ic_tag2_st_out;
    reg [1:0]               ic_tag3_st_out;
    reg                     i_hit_0;
    reg                     i_hit_1;
    reg                     i_hit_2;
    reg                     i_hit_3;
    wire [`ict_size-1:0]   ic_tag0_out;
    wire [`ict_size-1:0]   ic_tag1_out;
    wire [`ict_size-1:0]   ic_tag2_out;
    wire [`ict_size-1:0]   ic_tag3_out;
    wire [`wide-1:0]       lru_out;
    reg [`ict_size-1:0]    i_tag0_out;
    reg [`ict_size-1:0]    i_tag1_out;
    reg [`ict_size-1:0]    i_tag2_out;
    reg [`ict_size-1:0]    i_tag3_out;
    assign  IC_SRAM_TAG0_OUTi = i_tag0_out;
    assign  IC_SRAM_TAG1_OUTi = i_tag1_out;
    assign  IC_SRAM_TAG2_OUTi = i_tag2_out;
    assign  IC_SRAM_TAG3_OUTi = i_tag3_out;
    wire  inv_wr_ = sram_invld_;
    reg ic_hit_0;
    reg ic_hit_1;
    reg ic_hit_2;
    reg ic_hit_3;

always @(tag_paddr or ic_tag0_out or ic_tag1_out or ic_tag2_out or ic_tag3_out or sram_tag_cs_)
  begin
    if (~sram_tag_cs_)
      begin
        ic_hit_0 = ({tag_paddr,1'b1} == ic_tag0_out[`ict_size-1:1]);
        ic_hit_1 = ({tag_paddr,1'b1} == ic_tag1_out[`ict_size-1:1]);
        ic_hit_2 = ({tag_paddr,1'b1} == ic_tag2_out[`ict_size-1:1]);
        ic_hit_3 = ({tag_paddr,1'b1} == ic_tag3_out[`ict_size-1:1]);
      end
  end

    wire [1:0]    ic_tag_0_st_out = ic_tag0_out[1:0];
    wire [1:0]    ic_tag_1_st_out = ic_tag1_out[1:0];
    wire [1:0]    ic_tag_2_st_out = ic_tag2_out[1:0];
    wire [1:0]    ic_tag_3_st_out = ic_tag3_out[1:0];

    wire [3:0]    ic_way_select = { sram_way_sel[0] &  sram_way_sel[1],
                                   ~sram_way_sel[0] &  sram_way_sel[1],
                                    sram_way_sel[0] & ~sram_way_sel[1],
                                   ~sram_way_sel[0] & ~sram_way_sel[1]};
    wire [3:0]    ic_way_cs = way_option ? 4'b1111 : (way_address ? 4'b1010 : 4'b0101);
    wire [3:0]    ic_tag_cs_ = ~(((~sram_wr_en_ | ~sram_invld_) ?
                               ic_way_select : ic_way_cs) & {4{~sram_tag_cs_}});
    wire [`ict_size-1:0]    ic_tag_in = {sram_tag_in[`ict_size-1:2],sram_tag_in[1:0] & {2{sram_invld_}}};
    wire    lru_wr_en_ = sram_invld_ & sram_wr_en_;
    assign  IC_HIT_VECi = {i_hit_3,i_hit_2,i_hit_1,i_hit_0};
    reg [3:0]    ic_hit_vec_sel;

always @(posedge CLK_LATCH)
  begin
    if(IC_OUTPUT_ENABLEi | SCAN_MODE)
      begin
        ic_hit_vec_sel <= #`u_dly IC_HIT_VECi;
      end
  end

// these three latch is new for this version: 0907
always @(ic_tag_cs_[0] or ic_tag0_out or ic_tag_0_st_out)
  begin
    if(~ic_tag_cs_[0])
      begin
        i_tag0_out = ic_tag0_out;
        ic_tag0_st_out = ic_tag_0_st_out;
      end
  end

always @(ic_tag_cs_[1] or ic_tag1_out or ic_tag_1_st_out)
  begin
    if(~ic_tag_cs_[1])
      begin
        i_tag1_out = ic_tag1_out;
        ic_tag1_st_out = ic_tag_1_st_out;
      end
  end

always @(ic_tag_cs_[2] or ic_tag2_out or ic_tag_2_st_out)
  begin
    if(~ic_tag_cs_[2])
      begin
        i_tag2_out = ic_tag2_out;
        ic_tag2_st_out = ic_tag_2_st_out;
      end
  end

always @(ic_tag_cs_[3] or ic_tag3_out or ic_tag_3_st_out)
  begin
    if(~ic_tag_cs_[3])
      begin
        i_tag3_out = ic_tag3_out;
        ic_tag3_st_out = ic_tag_3_st_out;
      end
  end

always @(sram_tag_cs_ or lru_out)
  begin
    if(~sram_tag_cs_)
      begin
#latch_delay        IC_LRU_OUTii = lru_out;
      end
  end

always @(sram_indexop_ or sram_tag_cs_ or ic_hit_0 or ic_hit_1 or ic_hit_2 or ic_hit_3)
  begin
    if(~sram_tag_cs_ & sram_indexop_)
      begin
        i_hit_0 = ic_hit_0;
        i_hit_1 = ic_hit_1;
        i_hit_2 = ic_hit_2;
        i_hit_3 = ic_hit_3;
      end
  end

// inv the output reg tag_in;
    reg [`ict_size-1:0]    i_tag0_inv_out;
    reg [`ict_size-1:0]    i_tag1_inv_out;
    reg [`ict_size-1:0]    i_tag2_inv_out;
    reg [`ict_size-1:0]    i_tag3_inv_out;

// inv the output reg tag_out
    wire [`ict_size-1:0]    ic_sram_tag0_lch = ~i_tag0_inv_out;
    wire [`ict_size-1:0]    ic_sram_tag1_lch = ~i_tag1_inv_out;
    wire [`ict_size-1:0]    ic_sram_tag2_lch = ~i_tag2_inv_out;
    wire [`ict_size-1:0]    ic_sram_tag3_lch = ~i_tag3_inv_out;

    reg [3:0]    ic_way_select_lch;
    reg    ic_indexop_lch;
    reg    ic_indexop_inv_lch;
    wire    ic_indexop_inv = ~sram_indexop_;

always @(posedge CLK_LATCH)
  if (~SCAN_MODE & ~SE & ~BIST_HOLD)
  begin
    if (IC_OUTPUT_ENABLEi)
      begin
        ic_way_select_lch   <= #`u_dly ic_way_select;
        ic_indexop_inv_lch  <= #`u_dly ic_indexop_inv;
        ic_indexop_lch      <= #`u_dly sram_indexop_;
      end
  end
  else if (SE | BIST_HOLD & BIST_DIAG)
  begin
        ic_way_select_lch   <= #`u_dly {sram_addr_in[0], ic_way_select_lch[3:1]};
        ic_indexop_inv_lch  <= #`u_dly ic_way_select_lch[0];
        ic_indexop_lch      <= #`u_dly ic_indexop_inv_lch;
  end

    wire [3:0]    ic_tag_sel = {4{ic_indexop_lch}} & ic_hit_vec_sel |
                               {4{ic_indexop_inv_lch}} & ic_way_select_lch;
    assign    IC_SRAM_TAG_OUTi = {(`ict_size){ic_tag_sel[0]}} & ic_sram_tag0_lch |
                                 {(`ict_size){ic_tag_sel[1]}} & ic_sram_tag1_lch |
                                 {(`ict_size){ic_tag_sel[2]}} & ic_sram_tag2_lch |
                                 {(`ict_size){ic_tag_sel[3]}} & ic_sram_tag3_lch;

// the follow regs are the latch of all outputs

    wire [`ict_size-1:0]    i_tag0_inv_in = ~i_tag0_out;
    wire [`ict_size-1:0]    i_tag1_inv_in = ~i_tag1_out;
    wire [`ict_size-1:0]    i_tag2_inv_in = ~i_tag2_out;
    wire [`ict_size-1:0]    i_tag3_inv_in = ~i_tag3_out;

always @(posedge CLK_LATCH)
  if (~SCAN_MODE & ~SE & ~BIST_HOLD)
  begin
    if (IC_OUTPUT_ENABLEi)
      begin
        IC_SRAM_LRU_OUTii     <= #`u_dly IC_LRU_OUTii;
        i_tag0_inv_out        <= #`u_dly i_tag0_inv_in;
        i_tag1_inv_out        <= #`u_dly i_tag1_inv_in;
        i_tag2_inv_out        <= #`u_dly i_tag2_inv_in;
        i_tag3_inv_out        <= #`u_dly i_tag3_inv_in;
        IC_SRAM_TAG0_ST_OUTii <= #`u_dly ic_tag0_st_out;
        IC_SRAM_TAG1_ST_OUTii <= #`u_dly ic_tag1_st_out;
        IC_SRAM_TAG2_ST_OUTii <= #`u_dly ic_tag2_st_out;
        IC_SRAM_TAG3_ST_OUTii <= #`u_dly ic_tag3_st_out;
        IC_SRAM_HIT_0ii       <= #`u_dly i_hit_0;
        IC_SRAM_HIT_1ii       <= #`u_dly i_hit_1;
        IC_SRAM_HIT_2ii       <= #`u_dly i_hit_2;
        IC_SRAM_HIT_3ii       <= #`u_dly i_hit_3;
      end
  end
  else if (SE | BIST_HOLD & BIST_DIAG)
  begin
        IC_SRAM_LRU_OUTii     <= #`u_dly {IC_SRAM_TAG3_ST_OUTii[0], IC_SRAM_LRU_OUTii[1]};
        i_tag0_inv_out        <= #`u_dly i_tag1_inv_out;
        i_tag1_inv_out        <= #`u_dly i_tag2_inv_out;
        i_tag2_inv_out        <= #`u_dly i_tag3_inv_out;
        i_tag3_inv_out        <= #`u_dly {sram_lru_in[0], i_tag0_inv_out[21:1]};
        IC_SRAM_TAG0_ST_OUTii <= #`u_dly {i_tag0_inv_out[0], IC_SRAM_TAG0_ST_OUTii[1]};
        IC_SRAM_TAG1_ST_OUTii <= #`u_dly {IC_SRAM_TAG0_ST_OUTii[0], IC_SRAM_TAG1_ST_OUTii[1]};
        IC_SRAM_TAG2_ST_OUTii <= #`u_dly {IC_SRAM_TAG1_ST_OUTii[0], IC_SRAM_TAG2_ST_OUTii[1]};
        IC_SRAM_TAG3_ST_OUTii <= #`u_dly {IC_SRAM_TAG2_ST_OUTii[0], IC_SRAM_TAG3_ST_OUTii[1]};
        IC_SRAM_HIT_0ii       <= #`u_dly IC_SRAM_HIT_1ii;
        IC_SRAM_HIT_1ii       <= #`u_dly IC_SRAM_HIT_2ii;
        IC_SRAM_HIT_2ii       <= #`u_dly IC_SRAM_HIT_3ii;
        IC_SRAM_HIT_3ii       <= #`u_dly ic_indexop_lch;
  end


// generate core select signal.
  wire          core_sel_           = SCAN_MODE | BIST_HOLD;
  wire  [3:0]   ic_tag_cs_core_     = ic_tag_cs_ | {4{core_sel_}};
  wire          sram_tag_cs_core_   = sram_tag_cs_ | core_sel_;
// sub four ways tag module

      ic_oneway_tag ic_tag0 (
              .Q        (ic_tag0_out                  ),
              .A        (sram_addr_in                 ),
              .D        (ic_tag_in                    ),
              .WEN      ({sram_wr_en_,inv_wr_}        ),
              .CEN      (ic_tag_cs_core_[0]           ),
              .CLK      (CLK_LATCH                    )
                            );

      ic_oneway_tag ic_tag1 (
              .Q        (ic_tag1_out                  ),
              .A        (sram_addr_in                 ),
              .D        (ic_tag_in                    ),
              .WEN      ({sram_wr_en_,inv_wr_}        ),
              .CEN      (ic_tag_cs_core_[1]           ),
              .CLK      (CLK_LATCH                    )
                            );

      ic_oneway_tag ic_tag2 (
              .Q        (ic_tag2_out                  ),
              .A        (sram_addr_in                 ),
              .D        (ic_tag_in                    ),
              .WEN      ({sram_wr_en_,inv_wr_}        ),
              .CEN      (ic_tag_cs_core_[2]           ),
              .CLK      (CLK_LATCH                    )
                            );

      ic_oneway_tag ic_tag3 (
              .Q        (ic_tag3_out                  ),
              .A        (sram_addr_in                 ),
              .D        (ic_tag_in                    ),
              .WEN      ({sram_wr_en_,inv_wr_}        ),
              .CEN      (ic_tag_cs_core_[3]           ),
              .CLK      (CLK_LATCH                    )
                            );

      ic_lru       ic_lru_0 (
              .WEN      (lru_wr_en_                   ),
              .D        (sram_lru_in                  ),
              .Q        (lru_out                      ),
              .CEN      (sram_tag_cs_core_            ),
              .A        (sram_addr_in                 ),
              .CLK      (CLK_LATCH                    )
                            );

endmodule
//`endcelldefine
