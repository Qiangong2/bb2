/*******************************************************************************
             Project Name : T2Risc1H
             File Name : dc_tag_ctrl.v
             Create Date : 2002/01/11
             Author : Charliema
             Description :behavior module for dc_sram tag control logic
             Revision History :
                      01/11/2002   charlie       initial creation
*******************************************************************************/
//`include  "defines.h"
module dc_tag_ctrl  (
    // output ports
    DC_SRAM_TAG_OUT,
    DC_SRAM_LRU_OUT,
    DC_SRAM_HIT,
    DC_SRAM_TAG0_ST_OUT,
    DC_SRAM_TAG1_ST_OUT,
    DC_SRAM_TAG2_ST_OUT,
    DC_SRAM_TAG3_ST_OUT,
    DC_TAG_WR_,
    LRU_WR_EN_,
    DC_TAG_CS_,
    SRAM_TAG_CS_,
    SRAM_INVLD_,
    FORWARD_TAG_SEL,
    HIT_VEC_FOR_SEL,
    LRU_OUT_BEFORE_LCH,
    CLK_LATCH,

    // input ports
    DC_TAG0_OUT,
    DC_TAG1_OUT,
    DC_TAG2_OUT,
    DC_TAG3_OUT,
    DC_LRU_OUT,
    HIT_VEC_OUT,
    DC_SRAM_ADDR_IN,
    DC_SRAM_LRU_IN,
    DC_SRAM_TAG_CS_,
    DC_TAG_WR_EN_,
    DC_SRAM_INVLD_,
    DC_SRAM_INDEXOP_,
    DC_SRAM_WAY_SEL,
    FORWARD_INFO,
    DC_SRAM_BYPASS,
    DC_OUTPUT_ENABLE,
    DC_WAY_OPTION,
    DC_WAY_ADDR,
    BIST_HOLD,
    BIST_DIAG,
    SCAN_MODE,
    SE,
    SO,
    SI,
    CLK
    );

    // Output ports
    // dcd: data cache data,  dct: data cache tag
    output [`dct_size-1:0]    DC_SRAM_TAG_OUT;
    output [3:0]              DC_SRAM_LRU_OUT;
    output [6:0]              DC_SRAM_HIT;
    output [6:0]              DC_SRAM_TAG0_ST_OUT;
    output [6:0]              DC_SRAM_TAG1_ST_OUT;
    output [6:0]              DC_SRAM_TAG2_ST_OUT;
    output [6:0]              DC_SRAM_TAG3_ST_OUT;
    output [1:0]              DC_TAG_WR_;
    output                    LRU_WR_EN_;
    output [3:0]              DC_TAG_CS_;
    output                    SRAM_TAG_CS_;
    output                    SRAM_INVLD_;
    output [3:0]              FORWARD_TAG_SEL;
    output [3:0]              HIT_VEC_FOR_SEL;
    output [1:0]              LRU_OUT_BEFORE_LCH;
    output                    CLK_LATCH;
    output                    SO;

    // Input ports
    input [`dct_size-1:0]    DC_TAG0_OUT;
    input [`dct_size-1:0]    DC_TAG1_OUT;
    input [`dct_size-1:0]    DC_TAG2_OUT;
    input [`dct_size-1:0]    DC_TAG3_OUT;
    input [1:0]              DC_LRU_OUT;
    input [3:0]              HIT_VEC_OUT;
    input [`addr_width-6:0]  DC_SRAM_ADDR_IN;
    input [1:0]              DC_SRAM_LRU_IN;
    input                    DC_SRAM_TAG_CS_;
    input                    DC_TAG_WR_EN_;
    input                    DC_SRAM_INVLD_;
    input                    DC_SRAM_INDEXOP_;
    input [1:0]              DC_SRAM_WAY_SEL;
    input [8:0]              FORWARD_INFO;
    input                    DC_SRAM_BYPASS;
    input                    DC_OUTPUT_ENABLE;
    input                    DC_WAY_OPTION;
    input                    DC_WAY_ADDR;
    input                    BIST_HOLD;
    input                    BIST_DIAG;
    input                    SE;
    input                    SI;
    input                    SCAN_MODE;
    input                    CLK;

// the follow regs are the latch of inputs,the fwd data,index,valid,tag_st and me_paddr
// should not be latched
    reg [`addr_width-6:0]    sram_addr_in;
    reg                      SRAM_TAG_CS_;
    reg                      tag_wr_en_;
    reg                      DATA_WR_EN_;
    reg                      dc_invld_;
    reg                      DC_INDEX_OP_;
    reg [1:0]                dc_way_sel;
    reg                      way_option;
    reg                      way_address;
    reg [3:0]                dc_hit_vec_sel;

// the follow regs are the output regsiters
    wire [`dct_size-1:0]     DC_SRAM_TAG_OUT;
    reg [3:0]                DC_SRAM_LRU_OUT;
    reg [`dct_size-1:0]      dc_sram_tag0_out;
    reg [`dct_size-1:0]      dc_sram_tag1_out;
    reg [`dct_size-1:0]      dc_sram_tag2_out;
    reg [`dct_size-1:0]      dc_sram_tag3_out;
    reg [6:0]                DC_SRAM_HIT;

// the follow regs are the sram sub_block outputs
    wire  [`lru_wide-1:0]    DC_LRU_OUT;
    wire                     SO = dc_sram_tag0_out[26];

//    wire    CLK_LATCH = (~DC_SRAM_TAG_CS_ | DC_OUTPUT_ENABLE | BIST_HOLD & BIST_DIAG | SCAN_MODE) & CLK;
    wire latch_en = ~DC_SRAM_TAG_CS_ | DC_OUTPUT_ENABLE | BIST_HOLD & BIST_DIAG | SCAN_MODE;
    reg clk_en;

  always @(latch_en or CLK)
    begin
      if(~CLK)
        clk_en = latch_en;
    end

    wire   CLK_LATCH = clk_en & CLK;

// latch the input signal,generate some local cs to latch input signal
always @(posedge CLK)
  begin
    if (~SE & ~BIST_HOLD)
      SRAM_TAG_CS_      <= #`u_dly DC_SRAM_TAG_CS_;
    else if (SE | BIST_HOLD & BIST_DIAG)
      SRAM_TAG_CS_      <= #`u_dly way_address;
  end

always @(posedge CLK_LATCH)
  begin
    if (~SE & ~BIST_HOLD)
      begin
      if (~DC_SRAM_TAG_CS_)
        begin
          tag_wr_en_      <= #`u_dly DC_TAG_WR_EN_;
          dc_invld_       <= #`u_dly DC_SRAM_INVLD_;
          sram_addr_in    <= #`u_dly DC_SRAM_ADDR_IN;
          DC_INDEX_OP_    <= #`u_dly DC_SRAM_INDEXOP_;
          dc_way_sel      <= #`u_dly DC_SRAM_WAY_SEL;
          way_option      <= #`u_dly DC_WAY_OPTION;
          way_address     <= #`u_dly DC_WAY_ADDR;
        end
      end
    else if (SE | BIST_HOLD & BIST_DIAG)
      begin
        tag_wr_en_        <= #`u_dly SRAM_TAG_CS_;
        dc_invld_         <= #`u_dly tag_wr_en_;
        sram_addr_in      <= #`u_dly {DC_INDEX_OP_,sram_addr_in[`addr_width-6:1]};
        DC_INDEX_OP_      <= #`u_dly dc_way_sel[0];
        dc_way_sel        <= #`u_dly {dc_invld_,dc_way_sel[1]};
        way_option        <= #`u_dly SI;
        way_address       <= #`u_dly way_option;
      end
  end

    wire [3:0]    dc_way_select = { dc_way_sel[0] &  dc_way_sel[1],
                                   ~dc_way_sel[0] &  dc_way_sel[1],
                                    dc_way_sel[0] & ~dc_way_sel[1],
                                   ~dc_way_sel[0] & ~dc_way_sel[1]};
    wire [3:0]    dc_wayop_cs = way_option ? 4'b1111 : (way_address ? 4'b1010 : 4'b0101);
    wire [3:0]    DC_TAG_CS_ = ~(((tag_wr_en_ & dc_invld_) ? dc_wayop_cs : dc_way_select) & {4{~SRAM_TAG_CS_}});
    wire [1:0]    DC_TAG_WR_ = {2{tag_wr_en_}} & {1'b1,dc_invld_};
    wire    SRAM_INVLD_ = dc_invld_;
    wire    LRU_WR_EN_ = dc_invld_ & tag_wr_en_;

//merge data
    wire [1:0]             fwd_way   = FORWARD_INFO[8:7];
    wire [`addr_width-6:0] fwd_index = FORWARD_INFO[6:0];
    wire    tag_fwd_en = (fwd_index[`addr_width-6:0] == sram_addr_in[`addr_width-6:0]) & DC_SRAM_BYPASS;
    wire    lru_fwd_en = (fwd_index[`addr_width-6:0] == sram_addr_in[`addr_width-6:0]) & DC_SRAM_BYPASS;
    wire [3:0]    FORWARD_TAG_SEL = {fwd_way[1] &  fwd_way[0],
                                     fwd_way[1] & ~fwd_way[0],
                                    ~fwd_way[1] &  fwd_way[0],
                                    ~fwd_way[1] & ~fwd_way[0]} & {4{tag_fwd_en}};

    wire    d_hit_0 = HIT_VEC_OUT[0] & (DC_TAG0_OUT[2:1] == 2'b11);
    wire    d_hit_1 = HIT_VEC_OUT[1] & (DC_TAG1_OUT[2:1] == 2'b11);
    wire    d_hit_2 = HIT_VEC_OUT[2] & (DC_TAG2_OUT[2:1] == 2'b11);
    wire    d_hit_3 = HIT_VEC_OUT[3] & (DC_TAG3_OUT[2:1] == 2'b11);
    wire    hit_in_dcache = d_hit_0 | d_hit_1 | d_hit_2 | d_hit_3;
    wire [3:0]    HIT_VEC_FOR_SEL = {d_hit_3,d_hit_2,d_hit_1,d_hit_0};
    wire    hit_nbl_0 = HIT_VEC_OUT[0] & (DC_TAG0_OUT[2:1] == 2'b01) |
                        HIT_VEC_OUT[1] & (DC_TAG1_OUT[2:1] == 2'b01) |
                        HIT_VEC_OUT[2] & (DC_TAG2_OUT[2:1] == 2'b01) |
                        HIT_VEC_OUT[3] & (DC_TAG3_OUT[2:1] == 2'b01);
    wire    hit_nbl_1 = HIT_VEC_OUT[0] & (DC_TAG0_OUT[2:1] == 2'b10) |
                        HIT_VEC_OUT[1] & (DC_TAG1_OUT[2:1] == 2'b10) |
                        HIT_VEC_OUT[2] & (DC_TAG2_OUT[2:1] == 2'b10) |
                        HIT_VEC_OUT[3] & (DC_TAG3_OUT[2:1] == 2'b10);

    reg [3:0]    dc_way_select_lch;
    reg    dc_index_op_lch;
    reg    dc_index_op_inv_lch;
    wire    dc_index_op_inv = ~DC_INDEX_OP_;

always @(posedge CLK_LATCH)
  if (~SCAN_MODE & ~SE & ~BIST_HOLD)
    begin
      if (DC_OUTPUT_ENABLE)
        begin
          dc_way_select_lch   <= #`u_dly dc_way_select;
          dc_index_op_inv_lch <= #`u_dly dc_index_op_inv;
          dc_index_op_lch     <= #`u_dly DC_INDEX_OP_;
        end
    end
  else if (SE | BIST_HOLD & BIST_DIAG)
    begin
      dc_way_select_lch   <= #`u_dly {sram_addr_in[0],dc_way_select_lch[3:1]};
      dc_index_op_inv_lch <= #`u_dly dc_way_select_lch[0];
      dc_index_op_lch     <= #`u_dly dc_index_op_inv_lch;
    end

always @(posedge CLK_LATCH)
    if (DC_OUTPUT_ENABLE | SCAN_MODE)
      begin
        dc_hit_vec_sel      <= #`u_dly HIT_VEC_FOR_SEL;
      end

     wire [3:0]    dc_tag_sel = {4{dc_index_op_lch}} & dc_hit_vec_sel |
                                {4{dc_index_op_inv_lch}} & dc_way_select_lch;
    assign    DC_SRAM_TAG_OUT = dc_sram_tag0_out & {(`dct_size){dc_tag_sel[0]}} |
                                dc_sram_tag1_out & {(`dct_size){dc_tag_sel[1]}} |
                                dc_sram_tag2_out & {(`dct_size){dc_tag_sel[2]}} |
                                dc_sram_tag3_out & {(`dct_size){dc_tag_sel[3]}};
    wire [1:0]    lru_after_fwd = lru_fwd_en ? DC_SRAM_LRU_IN : DC_LRU_OUT;
    wire [1:0]    LRU_OUT_BEFORE_LCH = lru_after_fwd;
    wire [3:0]    dc_lru_decode = { lru_after_fwd[1] &  lru_after_fwd[0],
                                    lru_after_fwd[1] & ~lru_after_fwd[0],
                                   ~lru_after_fwd[1] &  lru_after_fwd[0],
                                   ~lru_after_fwd[1] & ~lru_after_fwd[0]};
    wire [6:0]    dc_sram_hit_in = {hit_in_dcache,hit_nbl_1,hit_nbl_0,d_hit_3,d_hit_2,d_hit_1,d_hit_0};
    wire [6:0]    DC_SRAM_TAG0_ST_OUT = dc_sram_tag0_out[6:0];
    wire [6:0]    DC_SRAM_TAG1_ST_OUT = dc_sram_tag1_out[6:0];
    wire [6:0]    DC_SRAM_TAG2_ST_OUT = dc_sram_tag2_out[6:0];
    wire [6:0]    DC_SRAM_TAG3_ST_OUT = dc_sram_tag3_out[6:0];

// latch the output signal
always @(posedge CLK_LATCH)
  if (~SCAN_MODE & ~SE & ~BIST_HOLD)
    begin
      if (DC_OUTPUT_ENABLE)
        begin
          dc_sram_tag0_out <= #`u_dly DC_TAG0_OUT;
          dc_sram_tag1_out <= #`u_dly DC_TAG1_OUT;
          dc_sram_tag2_out <= #`u_dly DC_TAG2_OUT;
          dc_sram_tag3_out <= #`u_dly DC_TAG3_OUT;
          DC_SRAM_LRU_OUT  <= #`u_dly dc_lru_decode;
          DC_SRAM_HIT      <= #`u_dly dc_sram_hit_in;
        end
    end
  else if (SE | BIST_HOLD & BIST_DIAG)
    begin
      dc_sram_tag0_out <= #`u_dly dc_sram_tag1_out;
      dc_sram_tag1_out <= #`u_dly dc_sram_tag2_out;
      dc_sram_tag2_out <= #`u_dly dc_sram_tag3_out;
      dc_sram_tag3_out <= #`u_dly {dc_sram_tag0_out[`dct_size-2:0],DC_SRAM_LRU_OUT[0]};
      DC_SRAM_LRU_OUT  <= #`u_dly {DC_SRAM_HIT[0],DC_SRAM_LRU_OUT[3:1]};
      DC_SRAM_HIT      <= #`u_dly {dc_index_op_lch,DC_SRAM_HIT[6:1]};
    end

endmodule

