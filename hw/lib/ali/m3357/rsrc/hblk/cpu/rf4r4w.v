/******************************************************************

	note :
		a: read port use clock posedge, write port use
			clock negedge ----> need make some attention
			to the clock plan, is better to leave more
			time for phase one
		b: scan-chain path not connect yet, the connection
			can be done after floor plan.
		c: more faster the read access time more better for
			later's logic, the intial time-budge only as reference
		d: more surrounding logic is consided to add in this
			hard-block later
Ranod modify 05/17/2002
Kevin Gao added the timing check 07/15/2002
Roger Fu simplify the control logic 09/09/2002
*******************************************************************/
//`include "defines.h"
/*
`define RFD_WIDTH  32
`define RFA_WIDTH  5
`define RFAD_WIDTH 32
`define RFST_WIDTH 3
*/
`timescale 1ns/10ps
`celldefine
module RF6304	(
		// write port a
		RF_AWA			,
		RF_DWA			,
		RF_CENWA_		,
        	RF_CENSTWA_     ,

		// write port b
		RF_AWB			,
		RF_DWB			,
		RF_CENWB_		,
        	RF_CENSTWB_ 	,

		// write port c
		RF_AWC			,
		RF_DWC			,
		RF_CENWC_		,
    		RF_CENSTWC_ 	,

		// write port d
		RF_AWD			,
		RF_DWD			,
		RF_CENWD_		,
    		RF_CENSTWD_ 	,

		// read port a
		RF_CENRA_		,
		RF_QRA			,
    		RF_STRA     	,
       	 	RF_RAREDO_EN    ,

		// read port b
		RF_CENRB_		,
		RF_QRB			,
    		RF_STRB     	,
		RF_RBREDO_EN    ,

		// read port c
		RF_CENRC_		,
		RF_QRC			,
    		RF_STRC     	,
		RF_RCREDO_EN    ,

		// read port d
		RF_CENRD_		,
		RF_QRD			,
    		RF_STRD     	,
    		RF_RDREDO_EN    ,

    		// waw check port a
    		RF_CENCA_   	,
    		RF_STCA     	,
    		RF_CAREDO_EN    ,

    		// waw check port b
    		RF_CENCB_   	,
    		RF_STCB     	,
       		RF_CBREDO_EN    ,

    		RD_ENCA     	,
    		DISP_CA_S   	,
    		RD_ENCB     	,
    		DISP_CB_S   	,
    		DISWAW_CA   	,
    		DISWAW_CB   	,

        	DESALUA_CA    	,
        	DESALUA_CB    	,

		ID_RSRTRD_REG0  ,
		ID_RSRTRD_REG1  ,
		ID_RSRTRD_IN0   ,
		ID_RSRTRD_IN1   ,

		ID_ADDR_SEL0    ,
		ID_ADDR_SEL1    ,


		TEST_SE			,
		TEST_SO0		,
        	TEST_SO1		,
		TEST_SI0		,
		TEST_SI1		,
		CLKR			,
		CLKW			,
    		RST_
		);


	output	[`RFD_WIDTH-1:0]	RF_QRA;
	output	[`RFD_WIDTH-1:0]	RF_QRB;
	output	[`RFD_WIDTH-1:0]	RF_QRC;
	output	[`RFD_WIDTH-1:0]	RF_QRD;
    	output  [4:0]   		RF_STRA;
    	output  [4:0]   		RF_STRB;
    	output  [4:0]   		RF_STRC;
    	output  [4:0]   		RF_STRD;
    	output  [4:0]   		RF_STCA;
    	output  [4:0]   		RF_STCB;
	output				TEST_SO0;
    	output                      	TEST_SO1;

	input	[`RFA_WIDTH-1:0]	RF_AWA;
	input	[`RFD_WIDTH-1:0]	RF_DWA;
	input				RF_CENWA_;
    	input                      	RF_CENSTWA_;
	input	[`RFA_WIDTH-1:0]	RF_AWB;
	input	[`RFD_WIDTH-1:0]	RF_DWB;
	input				RF_CENWB_;
    	input                       	RF_CENSTWB_;
	input	[`RFA_WIDTH-1:0]	RF_AWC;
	input	[`RFD_WIDTH-1:0]	RF_DWC;
	input				RF_CENWC_;
    	input           		RF_CENSTWC_;
	input	[`RFA_WIDTH-1:0]	RF_AWD;
	input	[`RFD_WIDTH-1:0]	RF_DWD;
	input				RF_CENWD_;
    	input           		RF_CENSTWD_;
	input				RF_CENRA_;
	input				RF_CENRB_;
	input				RF_CENRC_;
    	input                       	RF_CENRD_;
    	input           		RF_CENCA_;
  	input           		RF_CENCB_;
  	input           		RD_ENCA;
  	input           		DISP_CA_S;
  	input           		RD_ENCB;
  	input           		DISP_CB_S;
  	input           		DISWAW_CA;
  	input           		DISWAW_CB;
    	input                 		DESALUA_CA;
    	input                       	RF_RAREDO_EN;
    	input                       	RF_RBREDO_EN;
    	input                       	RF_RCREDO_EN;
    	input                       	RF_RDREDO_EN;
    	input                       	RF_CAREDO_EN;
    	input                       	RF_CBREDO_EN;

    	input                 		DESALUA_CB;
  	input  [14:0]               	ID_RSRTRD_REG0;
	input  [14:0]               	ID_RSRTRD_REG1;
	input  [14:0]               	ID_RSRTRD_IN0;
	input  [14:0]               	ID_RSRTRD_IN1;
	input                     	ID_ADDR_SEL0;
	input                     	ID_ADDR_SEL1  ;

	input				TEST_SI0;
	input                       	TEST_SI1;
	input				TEST_SE;
	input				CLKR;
	input				CLKW;
    	input         			RST_;
/////////////////////////////////////////////////////////////////////////////
	parameter	U_DLY   = 0.1 ;
    parameter   rphase_dly = 0.5 ;
    parameter   wphase_dly = 0.5 ;

	/*=====================================
	 declare input/output wire variable
	=====================================*/

	wire	[`RFD_WIDTH-1:0]	RF_QRA;
	wire	[`RFD_WIDTH-1:0]	RF_QRB;
	wire	[`RFD_WIDTH-1:0]	RF_QRC;
	wire	[`RFD_WIDTH-1:0]	RF_QRD;
    	wire  	[4:0]   		RF_STRA;
    	wire  	[4:0]   		RF_STRB;
    	wire	[4:0]   		RF_STRC;
    	wire  	[4:0]   		RF_STRD;
    	wire  	[4:0]   		RF_STCA;
    	wire	[4:0]   		RF_STCB;
	wire				TEST_SO0;
    	wire                      	TEST_SO1;

	wire	[`RFA_WIDTH-1:0]	RF_AWA;
	wire	[`RFD_WIDTH-1:0]	RF_DWA;
	wire				RF_CENWA_;
    	wire                      	RF_CENSTWA_;
	wire	[`RFA_WIDTH-1:0]	RF_AWB;
	wire	[`RFD_WIDTH-1:0]	RF_DWB;
	wire				RF_CENWB_;
    	wire                       	RF_CENSTWB_;
	wire	[`RFA_WIDTH-1:0]	RF_AWC;
	wire	[`RFD_WIDTH-1:0]	RF_DWC;
	wire				RF_CENWC_;
    	wire	           		RF_CENSTWC_;
	wire	[`RFA_WIDTH-1:0]	RF_AWD;
	wire	[`RFD_WIDTH-1:0]	RF_DWD;
	wire				RF_CENWD_;
    	wire 	          		RF_CENSTWD_;
	wire				RF_CENRA_;
	wire				RF_CENRB_;
	wire				RF_CENRC_;
    	wire                       	RF_CENRD_;
    	wire           			RF_CENCA_;
  	wire           			RF_CENCB_;
  	wire           			RD_ENCA;
  	wire           			DISP_CA_S;
  	wire           			RD_ENCB;
  	wire           			DISP_CB_S;
  	wire           			DISWAW_CA;
  	wire           			DISWAW_CB;
    	wire                 		DESALUA_CA;
    	wire                       	RF_RAREDO_EN;
    	wire                       	RF_RBREDO_EN;
    	wire                       	RF_RCREDO_EN;
    	wire                       	RF_RDREDO_EN;
    	wire                       	RF_CAREDO_EN;
    	wire                       	RF_CBREDO_EN;

    	wire                 		DESALUA_CB;
  	wire  [14:0]               	ID_RSRTRD_REG0;
	wire  [14:0]               	ID_RSRTRD_REG1;
	wire  [14:0]               	ID_RSRTRD_IN0;
	wire  [14:0]               	ID_RSRTRD_IN1;
	wire                     	ID_ADDR_SEL0;
	wire                     	ID_ADDR_SEL1  ;

	wire				TEST_SI0;
	wire                       	TEST_SI1;
	wire				TEST_SE;
	wire				CLKR;
	wire				CLKW;
    	wire         			RST_;

	/*=====================================
	 declare temp wire variable
	=====================================*/

	wire	[`RFD_WIDTH-1:0]	RF_QRAi;
	wire	[`RFD_WIDTH-1:0]	RF_QRBi;
	wire	[`RFD_WIDTH-1:0]	RF_QRCi;
	wire	[`RFD_WIDTH-1:0]	RF_QRDi;
    	wire  	[4:0]   		RF_STRAi;
    	wire  	[4:0]   		RF_STRBi;
    	wire	[4:0]   		RF_STRCi;
    	wire  	[4:0]   		RF_STRDi;
    	wire  	[4:0]   		RF_STCAi;
    	wire	[4:0]   		RF_STCBi;
	wire				TEST_SO0i;
    	wire                      	TEST_SO1i;

	wire	[`RFA_WIDTH-1:0]	RF_AWAi;
	wire	[`RFD_WIDTH-1:0]	RF_DWAi;
	wire				RF_CENWAi_;
    	wire                      	RF_CENSTWAi_;
	wire	[`RFA_WIDTH-1:0]	RF_AWBi;
	wire	[`RFD_WIDTH-1:0]	RF_DWBi;
	wire				RF_CENWBi_;
    	wire                       	RF_CENSTWBi_;
	wire	[`RFA_WIDTH-1:0]	RF_AWCi;
	wire	[`RFD_WIDTH-1:0]	RF_DWCi;
	wire				RF_CENWCi_;
    	wire	           		RF_CENSTWCi_;
	wire	[`RFA_WIDTH-1:0]	RF_AWDi;
	wire	[`RFD_WIDTH-1:0]	RF_DWDi;
	wire				RF_CENWDi_;
    	wire 	          		RF_CENSTWDi_;
	wire				RF_CENRAi_;
	wire				RF_CENRBi_;
	wire				RF_CENRCi_;
    	wire                       	RF_CENRDi_;
    	wire           			RF_CENCAi_;
  	wire           			RF_CENCBi_;
  	wire           			RD_ENCAi;
  	wire           			DISP_CA_Si;
  	wire           			RD_ENCBi;
  	wire           			DISP_CB_Si;
  	wire           			DISWAW_CAi;
  	wire           			DISWAW_CBi;
    	wire                 		DESALUA_CAi;
    	wire                       	RF_RAREDO_ENi;
    	wire                       	RF_RBREDO_ENi;
    	wire                       	RF_RCREDO_ENi;
    	wire                       	RF_RDREDO_ENi;
    	wire                       	RF_CAREDO_ENi;
    	wire                       	RF_CBREDO_ENi;

    	wire                 		DESALUA_CBi;
  	wire  [14:0]               	ID_RSRTRD_REG0i;
	wire  [14:0]               	ID_RSRTRD_REG1i;
	wire  [14:0]               	ID_RSRTRD_IN0i;
	wire  [14:0]               	ID_RSRTRD_IN1i;
	wire                     	ID_ADDR_SEL0i;
	wire                     	ID_ADDR_SEL1i  ;

	wire				TEST_SI0i;
	wire                       	TEST_SI1i;
	wire				TEST_SEi;
	wire				CLKRi;
    wire				CLKWi;
    	wire         			RSTi_;

	/*=====================================
		buffer define
	=====================================*/

        buf(RF_QRA[31],RF_QRAi[31]);
        buf(RF_QRA[30],RF_QRAi[30]);
        buf(RF_QRA[29],RF_QRAi[29]);
        buf(RF_QRA[28],RF_QRAi[28]);
        buf(RF_QRA[27],RF_QRAi[27]);
        buf(RF_QRA[26],RF_QRAi[26]);
        buf(RF_QRA[25],RF_QRAi[25]);
        buf(RF_QRA[24],RF_QRAi[24]);
        buf(RF_QRA[23],RF_QRAi[23]);
        buf(RF_QRA[22],RF_QRAi[22]);
        buf(RF_QRA[21],RF_QRAi[21]);
        buf(RF_QRA[20],RF_QRAi[20]);
        buf(RF_QRA[19],RF_QRAi[19]);
        buf(RF_QRA[18],RF_QRAi[18]);
        buf(RF_QRA[17],RF_QRAi[17]);
        buf(RF_QRA[16],RF_QRAi[16]);
        buf(RF_QRA[15],RF_QRAi[15]);
        buf(RF_QRA[14],RF_QRAi[14]);
        buf(RF_QRA[13],RF_QRAi[13]);
        buf(RF_QRA[12],RF_QRAi[12]);
        buf(RF_QRA[11],RF_QRAi[11]);
        buf(RF_QRA[10],RF_QRAi[10]);
        buf(RF_QRA[09],RF_QRAi[09]);
        buf(RF_QRA[08],RF_QRAi[08]);
        buf(RF_QRA[07],RF_QRAi[07]);
        buf(RF_QRA[06],RF_QRAi[06]);
        buf(RF_QRA[05],RF_QRAi[05]);
        buf(RF_QRA[04],RF_QRAi[04]);
        buf(RF_QRA[03],RF_QRAi[03]);
        buf(RF_QRA[02],RF_QRAi[02]);
        buf(RF_QRA[01],RF_QRAi[01]);
        buf(RF_QRA[00],RF_QRAi[00]);

        buf(RF_QRB[31],RF_QRBi[31]);
        buf(RF_QRB[30],RF_QRBi[30]);
        buf(RF_QRB[29],RF_QRBi[29]);
        buf(RF_QRB[28],RF_QRBi[28]);
        buf(RF_QRB[27],RF_QRBi[27]);
        buf(RF_QRB[26],RF_QRBi[26]);
        buf(RF_QRB[25],RF_QRBi[25]);
        buf(RF_QRB[24],RF_QRBi[24]);
        buf(RF_QRB[23],RF_QRBi[23]);
        buf(RF_QRB[22],RF_QRBi[22]);
        buf(RF_QRB[21],RF_QRBi[21]);
        buf(RF_QRB[20],RF_QRBi[20]);
        buf(RF_QRB[19],RF_QRBi[19]);
        buf(RF_QRB[18],RF_QRBi[18]);
        buf(RF_QRB[17],RF_QRBi[17]);
        buf(RF_QRB[16],RF_QRBi[16]);
        buf(RF_QRB[15],RF_QRBi[15]);
        buf(RF_QRB[14],RF_QRBi[14]);
        buf(RF_QRB[13],RF_QRBi[13]);
        buf(RF_QRB[12],RF_QRBi[12]);
        buf(RF_QRB[11],RF_QRBi[11]);
        buf(RF_QRB[10],RF_QRBi[10]);
        buf(RF_QRB[09],RF_QRBi[09]);
        buf(RF_QRB[08],RF_QRBi[08]);
        buf(RF_QRB[07],RF_QRBi[07]);
        buf(RF_QRB[06],RF_QRBi[06]);
        buf(RF_QRB[05],RF_QRBi[05]);
        buf(RF_QRB[04],RF_QRBi[04]);
        buf(RF_QRB[03],RF_QRBi[03]);
        buf(RF_QRB[02],RF_QRBi[02]);
        buf(RF_QRB[01],RF_QRBi[01]);
        buf(RF_QRB[00],RF_QRBi[00]);

        buf(RF_QRC[31],RF_QRCi[31]);
        buf(RF_QRC[30],RF_QRCi[30]);
        buf(RF_QRC[29],RF_QRCi[29]);
        buf(RF_QRC[28],RF_QRCi[28]);
        buf(RF_QRC[27],RF_QRCi[27]);
        buf(RF_QRC[26],RF_QRCi[26]);
        buf(RF_QRC[25],RF_QRCi[25]);
        buf(RF_QRC[24],RF_QRCi[24]);
        buf(RF_QRC[23],RF_QRCi[23]);
        buf(RF_QRC[22],RF_QRCi[22]);
        buf(RF_QRC[21],RF_QRCi[21]);
        buf(RF_QRC[20],RF_QRCi[20]);
        buf(RF_QRC[19],RF_QRCi[19]);
        buf(RF_QRC[18],RF_QRCi[18]);
        buf(RF_QRC[17],RF_QRCi[17]);
        buf(RF_QRC[16],RF_QRCi[16]);
        buf(RF_QRC[15],RF_QRCi[15]);
        buf(RF_QRC[14],RF_QRCi[14]);
        buf(RF_QRC[13],RF_QRCi[13]);
        buf(RF_QRC[12],RF_QRCi[12]);
        buf(RF_QRC[11],RF_QRCi[11]);
        buf(RF_QRC[10],RF_QRCi[10]);
        buf(RF_QRC[09],RF_QRCi[09]);
        buf(RF_QRC[08],RF_QRCi[08]);
        buf(RF_QRC[07],RF_QRCi[07]);
        buf(RF_QRC[06],RF_QRCi[06]);
        buf(RF_QRC[05],RF_QRCi[05]);
        buf(RF_QRC[04],RF_QRCi[04]);
        buf(RF_QRC[03],RF_QRCi[03]);
        buf(RF_QRC[02],RF_QRCi[02]);
        buf(RF_QRC[01],RF_QRCi[01]);
        buf(RF_QRC[00],RF_QRCi[00]);

        buf(RF_QRD[31],RF_QRDi[31]);
        buf(RF_QRD[30],RF_QRDi[30]);
        buf(RF_QRD[29],RF_QRDi[29]);
        buf(RF_QRD[28],RF_QRDi[28]);
        buf(RF_QRD[27],RF_QRDi[27]);
        buf(RF_QRD[26],RF_QRDi[26]);
        buf(RF_QRD[25],RF_QRDi[25]);
        buf(RF_QRD[24],RF_QRDi[24]);
        buf(RF_QRD[23],RF_QRDi[23]);
        buf(RF_QRD[22],RF_QRDi[22]);
        buf(RF_QRD[21],RF_QRDi[21]);
        buf(RF_QRD[20],RF_QRDi[20]);
        buf(RF_QRD[19],RF_QRDi[19]);
        buf(RF_QRD[18],RF_QRDi[18]);
        buf(RF_QRD[17],RF_QRDi[17]);
        buf(RF_QRD[16],RF_QRDi[16]);
        buf(RF_QRD[15],RF_QRDi[15]);
        buf(RF_QRD[14],RF_QRDi[14]);
        buf(RF_QRD[13],RF_QRDi[13]);
        buf(RF_QRD[12],RF_QRDi[12]);
        buf(RF_QRD[11],RF_QRDi[11]);
        buf(RF_QRD[10],RF_QRDi[10]);
        buf(RF_QRD[09],RF_QRDi[09]);
        buf(RF_QRD[08],RF_QRDi[08]);
        buf(RF_QRD[07],RF_QRDi[07]);
        buf(RF_QRD[06],RF_QRDi[06]);
        buf(RF_QRD[05],RF_QRDi[05]);
        buf(RF_QRD[04],RF_QRDi[04]);
        buf(RF_QRD[03],RF_QRDi[03]);
        buf(RF_QRD[02],RF_QRDi[02]);
        buf(RF_QRD[01],RF_QRDi[01]);
        buf(RF_QRD[00],RF_QRDi[00]);

        buf(RF_STRA[04],RF_STRAi[04]);
        buf(RF_STRA[03],RF_STRAi[03]);
        buf(RF_STRA[02],RF_STRAi[02]);
        buf(RF_STRA[01],RF_STRAi[01]);
        buf(RF_STRA[00],RF_STRAi[00]);

        buf(RF_STRB[04],RF_STRBi[04]);
        buf(RF_STRB[03],RF_STRBi[03]);
        buf(RF_STRB[02],RF_STRBi[02]);
        buf(RF_STRB[01],RF_STRBi[01]);
        buf(RF_STRB[00],RF_STRBi[00]);

        buf(RF_STRC[04],RF_STRCi[04]);
        buf(RF_STRC[03],RF_STRCi[03]);
        buf(RF_STRC[02],RF_STRCi[02]);
        buf(RF_STRC[01],RF_STRCi[01]);
        buf(RF_STRC[00],RF_STRCi[00]);

        buf(RF_STRD[04],RF_STRDi[04]);
        buf(RF_STRD[03],RF_STRDi[03]);
        buf(RF_STRD[02],RF_STRDi[02]);
        buf(RF_STRD[01],RF_STRDi[01]);
        buf(RF_STRD[00],RF_STRDi[00]);

        buf(RF_STCA[04],RF_STCAi[04]);
        buf(RF_STCA[03],RF_STCAi[03]);
        buf(RF_STCA[02],RF_STCAi[02]);
        buf(RF_STCA[01],RF_STCAi[01]);
        buf(RF_STCA[00],RF_STCAi[00]);

        buf(RF_STCB[04],RF_STCBi[04]);
        buf(RF_STCB[03],RF_STCBi[03]);
        buf(RF_STCB[02],RF_STCBi[02]);
        buf(RF_STCB[01],RF_STCBi[01]);
        buf(RF_STCB[00],RF_STCBi[00]);

        buf(TEST_SO0,TEST_SO0i);
        buf(TEST_SO1,TEST_SO1i);

        buf(RF_AWAi[04],RF_AWA[04]);
        buf(RF_AWAi[03],RF_AWA[03]);
        buf(RF_AWAi[02],RF_AWA[02]);
        buf(RF_AWAi[01],RF_AWA[01]);
        buf(RF_AWAi[00],RF_AWA[00]);

        buf(RF_DWAi[31],RF_DWA[31]);
        buf(RF_DWAi[30],RF_DWA[30]);
        buf(RF_DWAi[29],RF_DWA[29]);
        buf(RF_DWAi[28],RF_DWA[28]);
        buf(RF_DWAi[27],RF_DWA[27]);
        buf(RF_DWAi[26],RF_DWA[26]);
        buf(RF_DWAi[25],RF_DWA[25]);
        buf(RF_DWAi[24],RF_DWA[24]);
        buf(RF_DWAi[23],RF_DWA[23]);
        buf(RF_DWAi[22],RF_DWA[22]);
        buf(RF_DWAi[21],RF_DWA[21]);
        buf(RF_DWAi[20],RF_DWA[20]);
        buf(RF_DWAi[19],RF_DWA[19]);
        buf(RF_DWAi[18],RF_DWA[18]);
        buf(RF_DWAi[17],RF_DWA[17]);
        buf(RF_DWAi[16],RF_DWA[16]);
        buf(RF_DWAi[15],RF_DWA[15]);
        buf(RF_DWAi[14],RF_DWA[14]);
        buf(RF_DWAi[13],RF_DWA[13]);
        buf(RF_DWAi[12],RF_DWA[12]);
        buf(RF_DWAi[11],RF_DWA[11]);
        buf(RF_DWAi[10],RF_DWA[10]);
        buf(RF_DWAi[09],RF_DWA[09]);
        buf(RF_DWAi[08],RF_DWA[08]);
        buf(RF_DWAi[07],RF_DWA[07]);
        buf(RF_DWAi[06],RF_DWA[06]);
        buf(RF_DWAi[05],RF_DWA[05]);
        buf(RF_DWAi[04],RF_DWA[04]);
        buf(RF_DWAi[03],RF_DWA[03]);
        buf(RF_DWAi[02],RF_DWA[02]);
        buf(RF_DWAi[01],RF_DWA[01]);
        buf(RF_DWAi[00],RF_DWA[00]);

        buf(RF_CENWAi_,RF_CENWA_);
        buf(RF_CENSTWAi_,RF_CENSTWA_);

        buf(RF_AWBi[04],RF_AWB[04]);
        buf(RF_AWBi[03],RF_AWB[03]);
        buf(RF_AWBi[02],RF_AWB[02]);
        buf(RF_AWBi[01],RF_AWB[01]);
        buf(RF_AWBi[00],RF_AWB[00]);

    buf(RF_DWBi[31],RF_DWB[31]);
    buf(RF_DWBi[30],RF_DWB[30]);
    buf(RF_DWBi[29],RF_DWB[29]);
    buf(RF_DWBi[28],RF_DWB[28]);
    buf(RF_DWBi[27],RF_DWB[27]);
    buf(RF_DWBi[26],RF_DWB[26]);
    buf(RF_DWBi[25],RF_DWB[25]);
    buf(RF_DWBi[24],RF_DWB[24]);
    buf(RF_DWBi[23],RF_DWB[23]);
    buf(RF_DWBi[22],RF_DWB[22]);
    buf(RF_DWBi[21],RF_DWB[21]);
    buf(RF_DWBi[20],RF_DWB[20]);
    buf(RF_DWBi[19],RF_DWB[19]);
    buf(RF_DWBi[18],RF_DWB[18]);
    buf(RF_DWBi[17],RF_DWB[17]);
    buf(RF_DWBi[16],RF_DWB[16]);
    buf(RF_DWBi[15],RF_DWB[15]);
    buf(RF_DWBi[14],RF_DWB[14]);
    buf(RF_DWBi[13],RF_DWB[13]);
    buf(RF_DWBi[12],RF_DWB[12]);
    buf(RF_DWBi[11],RF_DWB[11]);
    buf(RF_DWBi[10],RF_DWB[10]);
    buf(RF_DWBi[09],RF_DWB[09]);
    buf(RF_DWBi[08],RF_DWB[08]);
    buf(RF_DWBi[07],RF_DWB[07]);
    buf(RF_DWBi[06],RF_DWB[06]);
    buf(RF_DWBi[05],RF_DWB[05]);
    buf(RF_DWBi[04],RF_DWB[04]);
    buf(RF_DWBi[03],RF_DWB[03]);
    buf(RF_DWBi[02],RF_DWB[02]);
    buf(RF_DWBi[01],RF_DWB[01]);
    buf(RF_DWBi[00],RF_DWB[00]);

    buf(RF_CENWBi_,RF_CENWB_);
    buf(RF_CENSTWBi_,RF_CENSTWB_);

    buf(RF_AWCi[04],RF_AWC[04]);
    buf(RF_AWCi[03],RF_AWC[03]);
    buf(RF_AWCi[02],RF_AWC[02]);
    buf(RF_AWCi[01],RF_AWC[01]);
    buf(RF_AWCi[00],RF_AWC[00]);

    buf(RF_DWCi[31],RF_DWC[31]);
    buf(RF_DWCi[30],RF_DWC[30]);
    buf(RF_DWCi[29],RF_DWC[29]);
    buf(RF_DWCi[28],RF_DWC[28]);
    buf(RF_DWCi[27],RF_DWC[27]);
    buf(RF_DWCi[26],RF_DWC[26]);
    buf(RF_DWCi[25],RF_DWC[25]);
    buf(RF_DWCi[24],RF_DWC[24]);
    buf(RF_DWCi[23],RF_DWC[23]);
    buf(RF_DWCi[22],RF_DWC[22]);
    buf(RF_DWCi[21],RF_DWC[21]);
    buf(RF_DWCi[20],RF_DWC[20]);
    buf(RF_DWCi[19],RF_DWC[19]);
    buf(RF_DWCi[18],RF_DWC[18]);
    buf(RF_DWCi[17],RF_DWC[17]);
    buf(RF_DWCi[16],RF_DWC[16]);
    buf(RF_DWCi[15],RF_DWC[15]);
    buf(RF_DWCi[14],RF_DWC[14]);
    buf(RF_DWCi[13],RF_DWC[13]);
    buf(RF_DWCi[12],RF_DWC[12]);
    buf(RF_DWCi[11],RF_DWC[11]);
    buf(RF_DWCi[10],RF_DWC[10]);
    buf(RF_DWCi[09],RF_DWC[09]);
    buf(RF_DWCi[08],RF_DWC[08]);
    buf(RF_DWCi[07],RF_DWC[07]);
    buf(RF_DWCi[06],RF_DWC[06]);
    buf(RF_DWCi[05],RF_DWC[05]);
    buf(RF_DWCi[04],RF_DWC[04]);
    buf(RF_DWCi[03],RF_DWC[03]);
    buf(RF_DWCi[02],RF_DWC[02]);
    buf(RF_DWCi[01],RF_DWC[01]);
    buf(RF_DWCi[00],RF_DWC[00]);

    buf(RF_CENWCi_,RF_CENWC_);
    buf(RF_CENSTWCi_,RF_CENSTWC_);

    buf(RF_AWDi[04],RF_AWD[04]);
    buf(RF_AWDi[03],RF_AWD[03]);
    buf(RF_AWDi[02],RF_AWD[02]);
    buf(RF_AWDi[01],RF_AWD[01]);
    buf(RF_AWDi[00],RF_AWD[00]);

    buf(RF_DWDi[31],RF_DWD[31]);
    buf(RF_DWDi[30],RF_DWD[30]);
    buf(RF_DWDi[29],RF_DWD[29]);
    buf(RF_DWDi[28],RF_DWD[28]);
    buf(RF_DWDi[27],RF_DWD[27]);
    buf(RF_DWDi[26],RF_DWD[26]);
    buf(RF_DWDi[25],RF_DWD[25]);
    buf(RF_DWDi[24],RF_DWD[24]);
    buf(RF_DWDi[23],RF_DWD[23]);
    buf(RF_DWDi[22],RF_DWD[22]);
    buf(RF_DWDi[21],RF_DWD[21]);
    buf(RF_DWDi[20],RF_DWD[20]);
    buf(RF_DWDi[19],RF_DWD[19]);
    buf(RF_DWDi[18],RF_DWD[18]);
    buf(RF_DWDi[17],RF_DWD[17]);
    buf(RF_DWDi[16],RF_DWD[16]);
    buf(RF_DWDi[15],RF_DWD[15]);
    buf(RF_DWDi[14],RF_DWD[14]);
    buf(RF_DWDi[13],RF_DWD[13]);
    buf(RF_DWDi[12],RF_DWD[12]);
    buf(RF_DWDi[11],RF_DWD[11]);
    buf(RF_DWDi[10],RF_DWD[10]);
    buf(RF_DWDi[09],RF_DWD[09]);
    buf(RF_DWDi[08],RF_DWD[08]);
    buf(RF_DWDi[07],RF_DWD[07]);
    buf(RF_DWDi[06],RF_DWD[06]);
    buf(RF_DWDi[05],RF_DWD[05]);
    buf(RF_DWDi[04],RF_DWD[04]);
    buf(RF_DWDi[03],RF_DWD[03]);
    buf(RF_DWDi[02],RF_DWD[02]);
    buf(RF_DWDi[01],RF_DWD[01]);
    buf(RF_DWDi[00],RF_DWD[00]);

    buf(RF_CENWDi_,RF_CENWD_);
    buf(RF_CENSTWDi_,RF_CENSTWD_);

    buf(RF_CENRAi_,RF_CENRA_);
    buf(RF_CENRBi_,RF_CENRB_);
    buf(RF_CENRCi_,RF_CENRC_);
    buf(RF_CENRDi_,RF_CENRD_);

    buf(RF_CENCAi_,RF_CENCA_);
    buf(RF_CENCBi_,RF_CENCB_);

    buf(RD_ENCAi,RD_ENCA);
    buf(DISP_CA_Si,DISP_CA_S);

    buf(RD_ENCBi,RD_ENCB);
    buf(DISP_CB_Si,DISP_CB_S);

    buf(DISWAW_CAi,DISWAW_CA);
    buf(DISWAW_CBi,DISWAW_CB);

    buf(DESALUA_CAi,DESALUA_CA);

    buf(RF_RAREDO_ENi,RF_RAREDO_EN);
    buf(RF_RBREDO_ENi,RF_RBREDO_EN);
    buf(RF_RCREDO_ENi,RF_RCREDO_EN);
    buf(RF_RDREDO_ENi,RF_RDREDO_EN);
    buf(RF_CAREDO_ENi,RF_CAREDO_EN);
    buf(RF_CBREDO_ENi,RF_CBREDO_EN);

    buf(DESALUA_CBi,DESALUA_CB);

    buf(ID_RSRTRD_REG0i[14],ID_RSRTRD_REG0[14]);
    buf(ID_RSRTRD_REG0i[13],ID_RSRTRD_REG0[13]);
    buf(ID_RSRTRD_REG0i[12],ID_RSRTRD_REG0[12]);
    buf(ID_RSRTRD_REG0i[11],ID_RSRTRD_REG0[11]);
    buf(ID_RSRTRD_REG0i[10],ID_RSRTRD_REG0[10]);
    buf(ID_RSRTRD_REG0i[09],ID_RSRTRD_REG0[09]);
    buf(ID_RSRTRD_REG0i[08],ID_RSRTRD_REG0[08]);
    buf(ID_RSRTRD_REG0i[07],ID_RSRTRD_REG0[07]);
    buf(ID_RSRTRD_REG0i[06],ID_RSRTRD_REG0[06]);
    buf(ID_RSRTRD_REG0i[05],ID_RSRTRD_REG0[05]);
    buf(ID_RSRTRD_REG0i[04],ID_RSRTRD_REG0[04]);
    buf(ID_RSRTRD_REG0i[03],ID_RSRTRD_REG0[03]);
    buf(ID_RSRTRD_REG0i[02],ID_RSRTRD_REG0[02]);
    buf(ID_RSRTRD_REG0i[01],ID_RSRTRD_REG0[01]);
    buf(ID_RSRTRD_REG0i[00],ID_RSRTRD_REG0[00]);

    buf(ID_RSRTRD_REG1i[14],ID_RSRTRD_REG1[14]);
    buf(ID_RSRTRD_REG1i[13],ID_RSRTRD_REG1[13]);
    buf(ID_RSRTRD_REG1i[12],ID_RSRTRD_REG1[12]);
    buf(ID_RSRTRD_REG1i[11],ID_RSRTRD_REG1[11]);
    buf(ID_RSRTRD_REG1i[10],ID_RSRTRD_REG1[10]);
    buf(ID_RSRTRD_REG1i[09],ID_RSRTRD_REG1[09]);
    buf(ID_RSRTRD_REG1i[08],ID_RSRTRD_REG1[08]);
    buf(ID_RSRTRD_REG1i[07],ID_RSRTRD_REG1[07]);
    buf(ID_RSRTRD_REG1i[06],ID_RSRTRD_REG1[06]);
    buf(ID_RSRTRD_REG1i[05],ID_RSRTRD_REG1[05]);
    buf(ID_RSRTRD_REG1i[04],ID_RSRTRD_REG1[04]);
    buf(ID_RSRTRD_REG1i[03],ID_RSRTRD_REG1[03]);
    buf(ID_RSRTRD_REG1i[02],ID_RSRTRD_REG1[02]);
    buf(ID_RSRTRD_REG1i[01],ID_RSRTRD_REG1[01]);
    buf(ID_RSRTRD_REG1i[00],ID_RSRTRD_REG1[00]);

    buf(ID_RSRTRD_IN0i[14],ID_RSRTRD_IN0[14]);
    buf(ID_RSRTRD_IN0i[13],ID_RSRTRD_IN0[13]);
    buf(ID_RSRTRD_IN0i[12],ID_RSRTRD_IN0[12]);
    buf(ID_RSRTRD_IN0i[11],ID_RSRTRD_IN0[11]);
    buf(ID_RSRTRD_IN0i[10],ID_RSRTRD_IN0[10]);
    buf(ID_RSRTRD_IN0i[09],ID_RSRTRD_IN0[09]);
    buf(ID_RSRTRD_IN0i[08],ID_RSRTRD_IN0[08]);
    buf(ID_RSRTRD_IN0i[07],ID_RSRTRD_IN0[07]);
    buf(ID_RSRTRD_IN0i[06],ID_RSRTRD_IN0[06]);
    buf(ID_RSRTRD_IN0i[05],ID_RSRTRD_IN0[05]);
    buf(ID_RSRTRD_IN0i[04],ID_RSRTRD_IN0[04]);
    buf(ID_RSRTRD_IN0i[03],ID_RSRTRD_IN0[03]);
    buf(ID_RSRTRD_IN0i[02],ID_RSRTRD_IN0[02]);
    buf(ID_RSRTRD_IN0i[01],ID_RSRTRD_IN0[01]);
    buf(ID_RSRTRD_IN0i[00],ID_RSRTRD_IN0[00]);

    buf(ID_RSRTRD_IN1i[14],ID_RSRTRD_IN1[14]);
    buf(ID_RSRTRD_IN1i[13],ID_RSRTRD_IN1[13]);
    buf(ID_RSRTRD_IN1i[12],ID_RSRTRD_IN1[12]);
    buf(ID_RSRTRD_IN1i[11],ID_RSRTRD_IN1[11]);
    buf(ID_RSRTRD_IN1i[10],ID_RSRTRD_IN1[10]);
    buf(ID_RSRTRD_IN1i[09],ID_RSRTRD_IN1[09]);
    buf(ID_RSRTRD_IN1i[08],ID_RSRTRD_IN1[08]);
    buf(ID_RSRTRD_IN1i[07],ID_RSRTRD_IN1[07]);
    buf(ID_RSRTRD_IN1i[06],ID_RSRTRD_IN1[06]);
    buf(ID_RSRTRD_IN1i[05],ID_RSRTRD_IN1[05]);
    buf(ID_RSRTRD_IN1i[04],ID_RSRTRD_IN1[04]);
    buf(ID_RSRTRD_IN1i[03],ID_RSRTRD_IN1[03]);
    buf(ID_RSRTRD_IN1i[02],ID_RSRTRD_IN1[02]);
    buf(ID_RSRTRD_IN1i[01],ID_RSRTRD_IN1[01]);
    buf(ID_RSRTRD_IN1i[00],ID_RSRTRD_IN1[00]);

    buf(ID_ADDR_SEL0i,ID_ADDR_SEL0);
    buf(ID_ADDR_SEL1i,ID_ADDR_SEL1);


    buf(TEST_SI0i,TEST_SI0);
    buf(TEST_SI1i,TEST_SI1);
    buf(TEST_SEi,TEST_SE);

    buf(CLKRi,CLKR);
	buf(CLKWi,CLKW);
    buf(RSTi_,RST_);

     /*===================================
      define notify violation register
     ====================================*/

    reg     NOT_CENWA,NOT_CENWB,NOT_CENWC,NOT_CENWD;
    reg     NOT_CENRA,NOT_CENRB,NOT_CENRC,NOT_CENRD;

    reg	    NOT_CENSTWA,NOT_CENSTWB,NOT_CENSTWC,NOT_CENSTWD;
    reg     NOT_CENCA,NOT_CENCB;

    reg     NOT_AWA0,
            NOT_AWA1,
            NOT_AWA2,
            NOT_AWA3,
            NOT_AWA4;

    reg     NOT_AWB0,
            NOT_AWB1,
            NOT_AWB2,
            NOT_AWB3,
            NOT_AWB4;

    reg     NOT_AWC0,
            NOT_AWC1,
            NOT_AWC2,
            NOT_AWC3,
            NOT_AWC4;

    reg     NOT_AWD0,
            NOT_AWD1,
            NOT_AWD2,
            NOT_AWD3,
            NOT_AWD4;


    reg     NOT_DWA0,
            NOT_DWA1,
            NOT_DWA2,
            NOT_DWA3,
            NOT_DWA4,
            NOT_DWA5,
            NOT_DWA6,
            NOT_DWA7,
            NOT_DWA8,
            NOT_DWA9,
            NOT_DWA10,
            NOT_DWA11,
            NOT_DWA12,
            NOT_DWA13,
            NOT_DWA14,
            NOT_DWA15,
            NOT_DWA16,
            NOT_DWA17,
            NOT_DWA18,
            NOT_DWA19,
            NOT_DWA20,
            NOT_DWA21,
            NOT_DWA22,
            NOT_DWA23,
            NOT_DWA24,
            NOT_DWA25,
            NOT_DWA26,
            NOT_DWA27,
            NOT_DWA28,
            NOT_DWA29,
            NOT_DWA30,
            NOT_DWA31;

    reg     NOT_DWB0,
            NOT_DWB1,
            NOT_DWB2,
            NOT_DWB3,
            NOT_DWB4,
            NOT_DWB5,
            NOT_DWB6,
            NOT_DWB7,
            NOT_DWB8,
            NOT_DWB9,
            NOT_DWB10,
            NOT_DWB11,
            NOT_DWB12,
            NOT_DWB13,
            NOT_DWB14,
            NOT_DWB15,
            NOT_DWB16,
            NOT_DWB17,
            NOT_DWB18,
            NOT_DWB19,
            NOT_DWB20,
            NOT_DWB21,
            NOT_DWB22,
            NOT_DWB23,
            NOT_DWB24,
            NOT_DWB25,
            NOT_DWB26,
            NOT_DWB27,
            NOT_DWB28,
            NOT_DWB29,
            NOT_DWB30,
            NOT_DWB31;

    reg     NOT_DWC0,
            NOT_DWC1,
            NOT_DWC2,
            NOT_DWC3,
            NOT_DWC4,
            NOT_DWC5,
            NOT_DWC6,
            NOT_DWC7,
            NOT_DWC8,
            NOT_DWC9,
            NOT_DWC10,
            NOT_DWC11,
            NOT_DWC12,
            NOT_DWC13,
            NOT_DWC14,
            NOT_DWC15,
            NOT_DWC16,
            NOT_DWC17,
            NOT_DWC18,
            NOT_DWC19,
            NOT_DWC20,
            NOT_DWC21,
            NOT_DWC22,
            NOT_DWC23,
            NOT_DWC24,
            NOT_DWC25,
            NOT_DWC26,
            NOT_DWC27,
            NOT_DWC28,
            NOT_DWC29,
            NOT_DWC30,
            NOT_DWC31;

    reg     NOT_DWD0,
            NOT_DWD1,
            NOT_DWD2,
            NOT_DWD3,
            NOT_DWD4,
            NOT_DWD5,
            NOT_DWD6,
            NOT_DWD7,
            NOT_DWD8,
            NOT_DWD9,
            NOT_DWD10,
            NOT_DWD11,
            NOT_DWD12,
            NOT_DWD13,
            NOT_DWD14,
            NOT_DWD15,
            NOT_DWD16,
            NOT_DWD17,
            NOT_DWD18,
            NOT_DWD19,
            NOT_DWD20,
            NOT_DWD21,
            NOT_DWD22,
            NOT_DWD23,
            NOT_DWD24,
            NOT_DWD25,
            NOT_DWD26,
            NOT_DWD27,
            NOT_DWD28,
            NOT_DWD29,
            NOT_DWD30,
            NOT_DWD31;

    reg     NOT_REG00,
            NOT_REG01,
            NOT_REG02,
            NOT_REG03,
            NOT_REG04,
            NOT_REG05,
            NOT_REG06,
            NOT_REG07,
            NOT_REG08,
            NOT_REG09,
            NOT_REG010,
            NOT_REG011,
            NOT_REG012,
            NOT_REG013,
            NOT_REG014;

    reg     NOT_REG10,
            NOT_REG11,
            NOT_REG12,
            NOT_REG13,
            NOT_REG14,
            NOT_REG15,
            NOT_REG16,
            NOT_REG17,
            NOT_REG18,
            NOT_REG19,
            NOT_REG110,
            NOT_REG111,
            NOT_REG112,
            NOT_REG113,
            NOT_REG114;

    reg     NOT_IN00,
            NOT_IN01,
            NOT_IN02,
            NOT_IN03,
            NOT_IN04,
            NOT_IN05,
            NOT_IN06,
            NOT_IN07,
            NOT_IN08,
            NOT_IN09,
            NOT_IN010,
            NOT_IN011,
            NOT_IN012,
            NOT_IN013,
            NOT_IN014;

    reg     NOT_IN10,
            NOT_IN11,
            NOT_IN12,
            NOT_IN13,
            NOT_IN14,
            NOT_IN15,
            NOT_IN16,
            NOT_IN17,
            NOT_IN18,
            NOT_IN19,
            NOT_IN110,
            NOT_IN111,
            NOT_IN112,
            NOT_IN113,
            NOT_IN114;

      reg   NOT_SEL0,
            NOT_SEL1;

      reg   NOT_CLKR_PER;
      reg   NOT_CLKR_MINH;
      reg   NOT_CLKR_MINL;

      reg   NOT_CLKW_PER;
      reg   NOT_CLKW_MINH;
      reg   NOT_CLKW_MINL;

      reg   NOT_ENCA,NOT_CAS,NOT_ENCB,NOT_CBS,NOT_WAWCA,NOT_WAWCB,
            NOT_ALUACA,NOT_ALUACB,NOT_SI0,NOT_SI1,NOT_SE,NOT_RST;

      reg   NOT_RAEN,NOT_RBEN,NOT_RCEN,NOT_RDEN,NOT_CAEN,NOT_CBEN,NOT_FRE;

     /*===================================
           Read/Write timing check flag
     ====================================*/

    wire    re_flagA =   !RF_CENRAi_;
    wire    re_flagB =   !RF_CENRBi_;
    wire    re_flagC =   !RF_CENRCi_;
    wire    re_flagD =   !RF_CENRDi_;

    wire    re_flag  =    re_flagA | re_flagB | re_flagC | re_flagD;

    wire    wr_flagA =   !RF_CENSTWAi_;
    wire    wr_flagB =   !RF_CENSTWBi_;
    wire    wr_flagC =   !RF_CENSTWCi_;
    wire    wr_flagD =   !RF_CENSTWDi_;

 	/*===================================
            specify block
        ====================================*/

        specify

                /*===========================
                 setup hold timing check
                ============================*/

                /*===========================
                    all posedge check
                ============================*/

                /*===========================
                  read port posedge check
                ============================*/

	$setuphold(posedge CLKR, posedge RF_CENRA_,1.000,0.500,NOT_CENRA);
        $setuphold(posedge CLKR, posedge RF_CENRB_,1.000,0.500,NOT_CENRB);
        $setuphold(posedge CLKR, posedge RF_CENRC_,1.000,0.500,NOT_CENRC);
        $setuphold(posedge CLKR, posedge RF_CENRD_,1.000,0.500,NOT_CENRD);


                /*===========================
                  write port negedge check
                ============================*/

        $setuphold(posedge CLKW, posedge RF_CENWA_,1.000,0.500,NOT_CENWA);
        $setuphold(posedge CLKW, posedge RF_CENWB_,1.000,0.500,NOT_CENWB);
        $setuphold(posedge CLKW, posedge RF_CENWC_,1.000,0.500,NOT_CENWC);
        $setuphold(posedge CLKW, posedge RF_CENWD_,1.000,0.500,NOT_CENWD);

	$setuphold(posedge CLKW &&& wr_flagA,posedge RF_AWA[00],1.000,0.500,NOT_AWA0);
        $setuphold(posedge CLKW &&& wr_flagA,posedge RF_AWA[01],1.000,0.500,NOT_AWA1);
        $setuphold(posedge CLKW &&& wr_flagA,posedge RF_AWA[02],1.000,0.500,NOT_AWA2);
        $setuphold(posedge CLKW &&& wr_flagA,posedge RF_AWA[03],1.000,0.500,NOT_AWA3);
        $setuphold(posedge CLKW &&& wr_flagA,posedge RF_AWA[04],1.000,0.500,NOT_AWA4);

	$setuphold(posedge CLKW &&& wr_flagA,posedge RF_DWA[00],1.000,0.500,NOT_DWA0);
        $setuphold(posedge CLKW &&& wr_flagA,posedge RF_DWA[01],1.000,0.500,NOT_DWA1);
        $setuphold(posedge CLKW &&& wr_flagA,posedge RF_DWA[02],1.000,0.500,NOT_DWA2);
        $setuphold(posedge CLKW &&& wr_flagA,posedge RF_DWA[03],1.000,0.500,NOT_DWA3);
        $setuphold(posedge CLKW &&& wr_flagA,posedge RF_DWA[04],1.000,0.500,NOT_DWA4);
        $setuphold(posedge CLKW &&& wr_flagA,posedge RF_DWA[05],1.000,0.500,NOT_DWA5);
        $setuphold(posedge CLKW &&& wr_flagA,posedge RF_DWA[06],1.000,0.500,NOT_DWA6);
        $setuphold(posedge CLKW &&& wr_flagA,posedge RF_DWA[07],1.000,0.500,NOT_DWA7);
        $setuphold(posedge CLKW &&& wr_flagA,posedge RF_DWA[08],1.000,0.500,NOT_DWA8);
        $setuphold(posedge CLKW &&& wr_flagA,posedge RF_DWA[09],1.000,0.500,NOT_DWA9);
        $setuphold(posedge CLKW &&& wr_flagA,posedge RF_DWA[10],1.000,0.500,NOT_DWA10);
        $setuphold(posedge CLKW &&& wr_flagA,posedge RF_DWA[11],1.000,0.500,NOT_DWA11);
        $setuphold(posedge CLKW &&& wr_flagA,posedge RF_DWA[12],1.000,0.500,NOT_DWA12);
        $setuphold(posedge CLKW &&& wr_flagA,posedge RF_DWA[13],1.000,0.500,NOT_DWA13);
        $setuphold(posedge CLKW &&& wr_flagA,posedge RF_DWA[14],1.000,0.500,NOT_DWA14);
        $setuphold(posedge CLKW &&& wr_flagA,posedge RF_DWA[15],1.000,0.500,NOT_DWA15);
        $setuphold(posedge CLKW &&& wr_flagA,posedge RF_DWA[16],1.000,0.500,NOT_DWA16);
        $setuphold(posedge CLKW &&& wr_flagA,posedge RF_DWA[17],1.000,0.500,NOT_DWA17);
        $setuphold(posedge CLKW &&& wr_flagA,posedge RF_DWA[18],1.000,0.500,NOT_DWA18);
        $setuphold(posedge CLKW &&& wr_flagA,posedge RF_DWA[19],1.000,0.500,NOT_DWA19);
        $setuphold(posedge CLKW &&& wr_flagA,posedge RF_DWA[20],1.000,0.500,NOT_DWA20);
        $setuphold(posedge CLKW &&& wr_flagA,posedge RF_DWA[21],1.000,0.500,NOT_DWA21);
        $setuphold(posedge CLKW &&& wr_flagA,posedge RF_DWA[22],1.000,0.500,NOT_DWA22);
        $setuphold(posedge CLKW &&& wr_flagA,posedge RF_DWA[23],1.000,0.500,NOT_DWA23);
        $setuphold(posedge CLKW &&& wr_flagA,posedge RF_DWA[24],1.000,0.500,NOT_DWA24);
        $setuphold(posedge CLKW &&& wr_flagA,posedge RF_DWA[25],1.000,0.500,NOT_DWA25);
        $setuphold(posedge CLKW &&& wr_flagA,posedge RF_DWA[26],1.000,0.500,NOT_DWA26);
        $setuphold(posedge CLKW &&& wr_flagA,posedge RF_DWA[27],1.000,0.500,NOT_DWA27);
        $setuphold(posedge CLKW &&& wr_flagA,posedge RF_DWA[28],1.000,0.500,NOT_DWA28);
        $setuphold(posedge CLKW &&& wr_flagA,posedge RF_DWA[29],1.000,0.500,NOT_DWA29);
        $setuphold(posedge CLKW &&& wr_flagA,posedge RF_DWA[30],1.000,0.500,NOT_DWA30);
        $setuphold(posedge CLKW &&& wr_flagA,posedge RF_DWA[31],1.000,0.500,NOT_DWA31);

        $setuphold(posedge CLKW, posedge RF_CENSTWA_,1.000,0.500,NOT_CENSTWA);

	$setuphold(posedge CLKW &&& wr_flagB,posedge RF_AWB[00],1.000,0.500,NOT_AWB0);
        $setuphold(posedge CLKW &&& wr_flagB,posedge RF_AWB[01],1.000,0.500,NOT_AWB1);
        $setuphold(posedge CLKW &&& wr_flagB,posedge RF_AWB[02],1.000,0.500,NOT_AWB2);
        $setuphold(posedge CLKW &&& wr_flagB,posedge RF_AWB[03],1.000,0.500,NOT_AWB3);
        $setuphold(posedge CLKW &&& wr_flagB,posedge RF_AWB[04],1.000,0.500,NOT_AWB4);

        $setuphold(posedge CLKW &&& wr_flagB,posedge RF_DWB[00],1.000,0.500,NOT_DWB0);
        $setuphold(posedge CLKW &&& wr_flagB,posedge RF_DWB[01],1.000,0.500,NOT_DWB1);
        $setuphold(posedge CLKW &&& wr_flagB,posedge RF_DWB[02],1.000,0.500,NOT_DWB2);
        $setuphold(posedge CLKW &&& wr_flagB,posedge RF_DWB[03],1.000,0.500,NOT_DWB3);
        $setuphold(posedge CLKW &&& wr_flagB,posedge RF_DWB[04],1.000,0.500,NOT_DWB4);
        $setuphold(posedge CLKW &&& wr_flagB,posedge RF_DWB[05],1.000,0.500,NOT_DWB5);
        $setuphold(posedge CLKW &&& wr_flagB,posedge RF_DWB[06],1.000,0.500,NOT_DWB6);
        $setuphold(posedge CLKW &&& wr_flagB,posedge RF_DWB[07],1.000,0.500,NOT_DWB7);
        $setuphold(posedge CLKW &&& wr_flagB,posedge RF_DWB[08],1.000,0.500,NOT_DWB8);
        $setuphold(posedge CLKW &&& wr_flagB,posedge RF_DWB[09],1.000,0.500,NOT_DWB9);
        $setuphold(posedge CLKW &&& wr_flagB,posedge RF_DWB[10],1.000,0.500,NOT_DWB10);
        $setuphold(posedge CLKW &&& wr_flagB,posedge RF_DWB[11],1.000,0.500,NOT_DWB11);
        $setuphold(posedge CLKW &&& wr_flagB,posedge RF_DWB[12],1.000,0.500,NOT_DWB12);
        $setuphold(posedge CLKW &&& wr_flagB,posedge RF_DWB[13],1.000,0.500,NOT_DWB13);
        $setuphold(posedge CLKW &&& wr_flagB,posedge RF_DWB[14],1.000,0.500,NOT_DWB14);
        $setuphold(posedge CLKW &&& wr_flagB,posedge RF_DWB[15],1.000,0.500,NOT_DWB15);
        $setuphold(posedge CLKW &&& wr_flagB,posedge RF_DWB[16],1.000,0.500,NOT_DWB16);
        $setuphold(posedge CLKW &&& wr_flagB,posedge RF_DWB[17],1.000,0.500,NOT_DWB17);
        $setuphold(posedge CLKW &&& wr_flagB,posedge RF_DWB[18],1.000,0.500,NOT_DWB18);
        $setuphold(posedge CLKW &&& wr_flagB,posedge RF_DWB[19],1.000,0.500,NOT_DWB19);
        $setuphold(posedge CLKW &&& wr_flagB,posedge RF_DWB[20],1.000,0.500,NOT_DWB20);
        $setuphold(posedge CLKW &&& wr_flagB,posedge RF_DWB[21],1.000,0.500,NOT_DWB21);
        $setuphold(posedge CLKW &&& wr_flagB,posedge RF_DWB[22],1.000,0.500,NOT_DWB22);
        $setuphold(posedge CLKW &&& wr_flagB,posedge RF_DWB[23],1.000,0.500,NOT_DWB23);
        $setuphold(posedge CLKW &&& wr_flagB,posedge RF_DWB[24],1.000,0.500,NOT_DWB24);
        $setuphold(posedge CLKW &&& wr_flagB,posedge RF_DWB[25],1.000,0.500,NOT_DWB25);
        $setuphold(posedge CLKW &&& wr_flagB,posedge RF_DWB[26],1.000,0.500,NOT_DWB26);
        $setuphold(posedge CLKW &&& wr_flagB,posedge RF_DWB[27],1.000,0.500,NOT_DWB27);
        $setuphold(posedge CLKW &&& wr_flagB,posedge RF_DWB[28],1.000,0.500,NOT_DWB28);
        $setuphold(posedge CLKW &&& wr_flagB,posedge RF_DWB[29],1.000,0.500,NOT_DWB29);
        $setuphold(posedge CLKW &&& wr_flagB,posedge RF_DWB[30],1.000,0.500,NOT_DWB30);
        $setuphold(posedge CLKW &&& wr_flagB,posedge RF_DWB[31],1.000,0.500,NOT_DWB31);

        $setuphold(posedge CLKW, posedge RF_CENSTWB_,1.000,0.500,NOT_CENSTWB);

        $setuphold(posedge CLKW &&& wr_flagC,posedge RF_AWC[00],1.000,0.500,NOT_AWC0);
        $setuphold(posedge CLKW &&& wr_flagC,posedge RF_AWC[01],1.000,0.500,NOT_AWC1);
        $setuphold(posedge CLKW &&& wr_flagC,posedge RF_AWC[02],1.000,0.500,NOT_AWC2);
        $setuphold(posedge CLKW &&& wr_flagC,posedge RF_AWC[03],1.000,0.500,NOT_AWC3);
        $setuphold(posedge CLKW &&& wr_flagC,posedge RF_AWC[04],1.000,0.500,NOT_AWC4);

        $setuphold(posedge CLKW &&& wr_flagC,posedge RF_DWC[00],1.000,0.500,NOT_DWC0);
        $setuphold(posedge CLKW &&& wr_flagC,posedge RF_DWC[01],1.000,0.500,NOT_DWC1);
        $setuphold(posedge CLKW &&& wr_flagC,posedge RF_DWC[02],1.000,0.500,NOT_DWC2);
        $setuphold(posedge CLKW &&& wr_flagC,posedge RF_DWC[03],1.000,0.500,NOT_DWC3);
        $setuphold(posedge CLKW &&& wr_flagC,posedge RF_DWC[04],1.000,0.500,NOT_DWC4);
        $setuphold(posedge CLKW &&& wr_flagC,posedge RF_DWC[05],1.000,0.500,NOT_DWC5);
        $setuphold(posedge CLKW &&& wr_flagC,posedge RF_DWC[06],1.000,0.500,NOT_DWC6);
        $setuphold(posedge CLKW &&& wr_flagC,posedge RF_DWC[07],1.000,0.500,NOT_DWC7);
        $setuphold(posedge CLKW &&& wr_flagC,posedge RF_DWC[08],1.000,0.500,NOT_DWC8);
        $setuphold(posedge CLKW &&& wr_flagC,posedge RF_DWC[09],1.000,0.500,NOT_DWC9);
        $setuphold(posedge CLKW &&& wr_flagC,posedge RF_DWC[10],1.000,0.500,NOT_DWC10);
        $setuphold(posedge CLKW &&& wr_flagC,posedge RF_DWC[11],1.000,0.500,NOT_DWC11);
        $setuphold(posedge CLKW &&& wr_flagC,posedge RF_DWC[12],1.000,0.500,NOT_DWC12);
        $setuphold(posedge CLKW &&& wr_flagC,posedge RF_DWC[13],1.000,0.500,NOT_DWC13);
        $setuphold(posedge CLKW &&& wr_flagC,posedge RF_DWC[14],1.000,0.500,NOT_DWC14);
        $setuphold(posedge CLKW &&& wr_flagC,posedge RF_DWC[15],1.000,0.500,NOT_DWC15);
        $setuphold(posedge CLKW &&& wr_flagC,posedge RF_DWC[16],1.000,0.500,NOT_DWC16);
        $setuphold(posedge CLKW &&& wr_flagC,posedge RF_DWC[17],1.000,0.500,NOT_DWC17);
        $setuphold(posedge CLKW &&& wr_flagC,posedge RF_DWC[18],1.000,0.500,NOT_DWC18);
        $setuphold(posedge CLKW &&& wr_flagC,posedge RF_DWC[19],1.000,0.500,NOT_DWC19);
        $setuphold(posedge CLKW &&& wr_flagC,posedge RF_DWC[20],1.000,0.500,NOT_DWC20);
        $setuphold(posedge CLKW &&& wr_flagC,posedge RF_DWC[21],1.000,0.500,NOT_DWC21);
        $setuphold(posedge CLKW &&& wr_flagC,posedge RF_DWC[22],1.000,0.500,NOT_DWC22);
        $setuphold(posedge CLKW &&& wr_flagC,posedge RF_DWC[23],1.000,0.500,NOT_DWC23);
        $setuphold(posedge CLKW &&& wr_flagC,posedge RF_DWC[24],1.000,0.500,NOT_DWC24);
        $setuphold(posedge CLKW &&& wr_flagC,posedge RF_DWC[25],1.000,0.500,NOT_DWC25);
        $setuphold(posedge CLKW &&& wr_flagC,posedge RF_DWC[26],1.000,0.500,NOT_DWC26);
        $setuphold(posedge CLKW &&& wr_flagC,posedge RF_DWC[27],1.000,0.500,NOT_DWC27);
        $setuphold(posedge CLKW &&& wr_flagC,posedge RF_DWC[28],1.000,0.500,NOT_DWC28);
        $setuphold(posedge CLKW &&& wr_flagC,posedge RF_DWC[29],1.000,0.500,NOT_DWC29);
        $setuphold(posedge CLKW &&& wr_flagC,posedge RF_DWC[30],1.000,0.500,NOT_DWC30);
        $setuphold(posedge CLKW &&& wr_flagC,posedge RF_DWC[31],1.000,0.500,NOT_DWC31);

        $setuphold(posedge CLKW, posedge RF_CENSTWC_,1.000,0.500,NOT_CENSTWC);

        $setuphold(posedge CLKW &&& wr_flagD,posedge RF_AWD[00],1.000,0.500,NOT_AWD0);
        $setuphold(posedge CLKW &&& wr_flagD,posedge RF_AWD[01],1.000,0.500,NOT_AWD1);
        $setuphold(posedge CLKW &&& wr_flagD,posedge RF_AWD[02],1.000,0.500,NOT_AWD2);
        $setuphold(posedge CLKW &&& wr_flagD,posedge RF_AWD[03],1.000,0.500,NOT_AWD3);
        $setuphold(posedge CLKW &&& wr_flagD,posedge RF_AWD[04],1.000,0.500,NOT_AWD4);

        $setuphold(posedge CLKW &&& wr_flagD,posedge RF_DWD[00],1.000,0.500,NOT_DWD0);
        $setuphold(posedge CLKW &&& wr_flagD,posedge RF_DWD[01],1.000,0.500,NOT_DWD1);
        $setuphold(posedge CLKW &&& wr_flagD,posedge RF_DWD[02],1.000,0.500,NOT_DWD2);
        $setuphold(posedge CLKW &&& wr_flagD,posedge RF_DWD[03],1.000,0.500,NOT_DWD3);
        $setuphold(posedge CLKW &&& wr_flagD,posedge RF_DWD[04],1.000,0.500,NOT_DWD4);
        $setuphold(posedge CLKW &&& wr_flagD,posedge RF_DWD[05],1.000,0.500,NOT_DWD5);
        $setuphold(posedge CLKW &&& wr_flagD,posedge RF_DWD[06],1.000,0.500,NOT_DWD6);
        $setuphold(posedge CLKW &&& wr_flagD,posedge RF_DWD[07],1.000,0.500,NOT_DWD7);
        $setuphold(posedge CLKW &&& wr_flagD,posedge RF_DWD[08],1.000,0.500,NOT_DWD8);
        $setuphold(posedge CLKW &&& wr_flagD,posedge RF_DWD[09],1.000,0.500,NOT_DWD9);
        $setuphold(posedge CLKW &&& wr_flagD,posedge RF_DWD[10],1.000,0.500,NOT_DWD10);
        $setuphold(posedge CLKW &&& wr_flagD,posedge RF_DWD[11],1.000,0.500,NOT_DWD11);
        $setuphold(posedge CLKW &&& wr_flagD,posedge RF_DWD[12],1.000,0.500,NOT_DWD12);
        $setuphold(posedge CLKW &&& wr_flagD,posedge RF_DWD[13],1.000,0.500,NOT_DWD13);
        $setuphold(posedge CLKW &&& wr_flagD,posedge RF_DWD[14],1.000,0.500,NOT_DWD14);
        $setuphold(posedge CLKW &&& wr_flagD,posedge RF_DWD[15],1.000,0.500,NOT_DWD15);
        $setuphold(posedge CLKW &&& wr_flagD,posedge RF_DWD[16],1.000,0.500,NOT_DWD16);
        $setuphold(posedge CLKW &&& wr_flagD,posedge RF_DWD[17],1.000,0.500,NOT_DWD17);
        $setuphold(posedge CLKW &&& wr_flagD,posedge RF_DWD[18],1.000,0.500,NOT_DWD18);
        $setuphold(posedge CLKW &&& wr_flagD,posedge RF_DWD[19],1.000,0.500,NOT_DWD19);
        $setuphold(posedge CLKW &&& wr_flagD,posedge RF_DWD[20],1.000,0.500,NOT_DWD20);
        $setuphold(posedge CLKW &&& wr_flagD,posedge RF_DWD[21],1.000,0.500,NOT_DWD21);
        $setuphold(posedge CLKW &&& wr_flagD,posedge RF_DWD[22],1.000,0.500,NOT_DWD22);
        $setuphold(posedge CLKW &&& wr_flagD,posedge RF_DWD[23],1.000,0.500,NOT_DWD23);
        $setuphold(posedge CLKW &&& wr_flagD,posedge RF_DWD[24],1.000,0.500,NOT_DWD24);
        $setuphold(posedge CLKW &&& wr_flagD,posedge RF_DWD[25],1.000,0.500,NOT_DWD25);
        $setuphold(posedge CLKW &&& wr_flagD,posedge RF_DWD[26],1.000,0.500,NOT_DWD26);
        $setuphold(posedge CLKW &&& wr_flagD,posedge RF_DWD[27],1.000,0.500,NOT_DWD27);
        $setuphold(posedge CLKW &&& wr_flagD,posedge RF_DWD[28],1.000,0.500,NOT_DWD28);
        $setuphold(posedge CLKW &&& wr_flagD,posedge RF_DWD[29],1.000,0.500,NOT_DWD29);
        $setuphold(posedge CLKW &&& wr_flagD,posedge RF_DWD[30],1.000,0.500,NOT_DWD30);
        $setuphold(posedge CLKW &&& wr_flagD,posedge RF_DWD[31],1.000,0.500,NOT_DWD31);

        $setuphold(posedge CLKW, posedge RF_CENSTWD_,1.000,0.500,NOT_CENSTWD);

        /*===============================
            misc signal timing check
        ================================*/

        $setuphold(posedge CLKR ,posedge RF_CENCA_,1.000,0.500,NOT_CENCA);
        $setuphold(posedge CLKR ,posedge RF_CENCB_,1.000,0.500,NOT_CENCB);


        $setuphold(posedge CLKW ,posedge RD_ENCA,1.000,0.500,NOT_ENCA);
        $setuphold(posedge CLKW ,posedge DISP_CA_S,1.000,0.500,NOT_CAS);
        $setuphold(posedge CLKW ,posedge RD_ENCB,1.000,0.500,NOT_ENCB);
        $setuphold(posedge CLKW ,posedge DISP_CB_S,1.000,0.500,NOT_CBS);
        $setuphold(posedge CLKW ,posedge DISWAW_CA,1.000,0.500,NOT_WAWCA);
        $setuphold(posedge CLKW ,posedge DISWAW_CB,1.000,0.500,NOT_WAWCB);
        $setuphold(posedge CLKW ,posedge DESALUA_CA,1.000,0.500,NOT_ALUACA);
        $setuphold(posedge CLKW ,posedge DESALUA_CB,1.000,0.500,NOT_ALUACB);
        $setuphold(posedge CLKW ,posedge RST_,1.000,0.500,NOT_RST);

	$setuphold(posedge CLKR ,posedge RD_ENCA,1.000,0.500,NOT_ENCA);

        $setuphold(posedge CLKR ,posedge TEST_SE,1.000,0.500,NOT_SE);
        $setuphold(posedge CLKR ,posedge TEST_SI0,1.000,0.500,NOT_SI0);
        $setuphold(posedge CLKR ,posedge TEST_SI1,1.000,0.500,NOT_SI1);

        $setuphold(posedge CLKR ,posedge RF_RAREDO_EN,1.000,0.500,NOT_RAEN);
        $setuphold(posedge CLKR ,posedge RF_RBREDO_EN,1.000,0.500,NOT_RBEN);
        $setuphold(posedge CLKR ,posedge RF_RCREDO_EN,1.000,0.500,NOT_RCEN);
        $setuphold(posedge CLKR ,posedge RF_RDREDO_EN,1.000,0.500,NOT_RDEN);

        $setuphold(posedge CLKR ,posedge RF_CAREDO_EN,1.000,0.500,NOT_CAEN);
        $setuphold(posedge CLKR ,posedge RF_CBREDO_EN,1.000,0.500,NOT_CBEN);

        //===================================

        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_REG0[00],1.000,0.500,NOT_REG00);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_REG0[01],1.000,0.500,NOT_REG01);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_REG0[02],1.000,0.500,NOT_REG02);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_REG0[03],1.000,0.500,NOT_REG03);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_REG0[04],1.000,0.500,NOT_REG04);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_REG0[05],1.000,0.500,NOT_REG05);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_REG0[06],1.000,0.500,NOT_REG06);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_REG0[07],1.000,0.500,NOT_REG07);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_REG0[08],1.000,0.500,NOT_REG08);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_REG0[09],1.000,0.500,NOT_REG09);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_REG0[10],1.000,0.500,NOT_REG010);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_REG0[11],1.000,0.500,NOT_REG011);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_REG0[12],1.000,0.500,NOT_REG012);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_REG0[13],1.000,0.500,NOT_REG013);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_REG0[14],1.000,0.500,NOT_REG014);

        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_REG1[00],1.000,0.500,NOT_REG10);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_REG1[01],1.000,0.500,NOT_REG11);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_REG1[02],1.000,0.500,NOT_REG12);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_REG1[03],1.000,0.500,NOT_REG13);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_REG1[04],1.000,0.500,NOT_REG14);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_REG1[05],1.000,0.500,NOT_REG15);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_REG1[06],1.000,0.500,NOT_REG16);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_REG1[07],1.000,0.500,NOT_REG17);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_REG1[08],1.000,0.500,NOT_REG18);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_REG1[09],1.000,0.500,NOT_REG19);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_REG1[10],1.000,0.500,NOT_REG110);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_REG1[11],1.000,0.500,NOT_REG111);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_REG1[12],1.000,0.500,NOT_REG112);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_REG1[13],1.000,0.500,NOT_REG113);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_REG1[14],1.000,0.500,NOT_REG114);

        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_IN0[00],1.000,0.500,NOT_IN00);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_IN0[01],1.000,0.500,NOT_IN01);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_IN0[02],1.000,0.500,NOT_IN02);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_IN0[03],1.000,0.500,NOT_IN03);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_IN0[04],1.000,0.500,NOT_IN04);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_IN0[05],1.000,0.500,NOT_IN05);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_IN0[06],1.000,0.500,NOT_IN06);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_IN0[07],1.000,0.500,NOT_IN07);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_IN0[08],1.000,0.500,NOT_IN08);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_IN0[09],1.000,0.500,NOT_IN09);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_IN0[10],1.000,0.500,NOT_IN010);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_IN0[11],1.000,0.500,NOT_IN011);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_IN0[12],1.000,0.500,NOT_IN012);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_IN0[13],1.000,0.500,NOT_IN013);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_IN0[14],1.000,0.500,NOT_IN014);

        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_IN1[00],1.000,0.500,NOT_IN10);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_IN1[01],1.000,0.500,NOT_IN11);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_IN1[02],1.000,0.500,NOT_IN12);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_IN1[03],1.000,0.500,NOT_IN13);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_IN1[04],1.000,0.500,NOT_IN14);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_IN1[05],1.000,0.500,NOT_IN15);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_IN1[06],1.000,0.500,NOT_IN16);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_IN1[07],1.000,0.500,NOT_IN17);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_IN1[08],1.000,0.500,NOT_IN18);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_IN1[09],1.000,0.500,NOT_IN19);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_IN1[10],1.000,0.500,NOT_IN110);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_IN1[11],1.000,0.500,NOT_IN111);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_IN1[12],1.000,0.500,NOT_IN112);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_IN1[13],1.000,0.500,NOT_IN113);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_RSRTRD_IN1[14],1.000,0.500,NOT_IN114);

        $setuphold(posedge CLKR &&& re_flag,posedge ID_ADDR_SEL0,1.000,0.500,NOT_SEL0);
        $setuphold(posedge CLKR &&& re_flag,posedge ID_ADDR_SEL1,1.000,0.500,NOT_SEL1);

              //=============================
              //  all negedge check
              //=============================

        $setuphold(posedge CLKR, negedge RF_CENRA_,1.000,0.500,NOT_CENRA);
        $setuphold(posedge CLKR, negedge RF_CENRB_,1.000,0.500,NOT_CENRB);
        $setuphold(posedge CLKR, negedge RF_CENRC_,1.000,0.500,NOT_CENRC);
        $setuphold(posedge CLKR, negedge RF_CENRD_,1.000,0.500,NOT_CENRD);


                /*===========================
                  write port negedge check
                ============================*/

        $setuphold(posedge CLKW, negedge RF_CENWA_,1.000,0.500,NOT_CENWA);
        $setuphold(posedge CLKW, negedge RF_CENWB_,1.000,0.500,NOT_CENWB);
        $setuphold(posedge CLKW, negedge RF_CENWC_,1.000,0.500,NOT_CENWC);
        $setuphold(posedge CLKW, negedge RF_CENWD_,1.000,0.500,NOT_CENWD);

	$setuphold(posedge CLKW &&& wr_flagA,negedge RF_AWA[00],1.000,0.500,NOT_AWA0);
        $setuphold(posedge CLKW &&& wr_flagA,negedge RF_AWA[01],1.000,0.500,NOT_AWA1);
        $setuphold(posedge CLKW &&& wr_flagA,negedge RF_AWA[02],1.000,0.500,NOT_AWA2);
        $setuphold(posedge CLKW &&& wr_flagA,negedge RF_AWA[03],1.000,0.500,NOT_AWA3);
        $setuphold(posedge CLKW &&& wr_flagA,negedge RF_AWA[04],1.000,0.500,NOT_AWA4);

	$setuphold(posedge CLKW &&& wr_flagA,negedge RF_DWA[00],1.000,0.500,NOT_DWA0);
        $setuphold(posedge CLKW &&& wr_flagA,negedge RF_DWA[01],1.000,0.500,NOT_DWA1);
        $setuphold(posedge CLKW &&& wr_flagA,negedge RF_DWA[02],1.000,0.500,NOT_DWA2);
        $setuphold(posedge CLKW &&& wr_flagA,negedge RF_DWA[03],1.000,0.500,NOT_DWA3);
        $setuphold(posedge CLKW &&& wr_flagA,negedge RF_DWA[04],1.000,0.500,NOT_DWA4);
        $setuphold(posedge CLKW &&& wr_flagA,negedge RF_DWA[05],1.000,0.500,NOT_DWA5);
        $setuphold(posedge CLKW &&& wr_flagA,negedge RF_DWA[06],1.000,0.500,NOT_DWA6);
        $setuphold(posedge CLKW &&& wr_flagA,negedge RF_DWA[07],1.000,0.500,NOT_DWA7);
        $setuphold(posedge CLKW &&& wr_flagA,negedge RF_DWA[08],1.000,0.500,NOT_DWA8);
        $setuphold(posedge CLKW &&& wr_flagA,negedge RF_DWA[09],1.000,0.500,NOT_DWA9);
        $setuphold(posedge CLKW &&& wr_flagA,negedge RF_DWA[10],1.000,0.500,NOT_DWA10);
        $setuphold(posedge CLKW &&& wr_flagA,negedge RF_DWA[11],1.000,0.500,NOT_DWA11);
        $setuphold(posedge CLKW &&& wr_flagA,negedge RF_DWA[12],1.000,0.500,NOT_DWA12);
        $setuphold(posedge CLKW &&& wr_flagA,negedge RF_DWA[13],1.000,0.500,NOT_DWA13);
        $setuphold(posedge CLKW &&& wr_flagA,negedge RF_DWA[14],1.000,0.500,NOT_DWA14);
        $setuphold(posedge CLKW &&& wr_flagA,negedge RF_DWA[15],1.000,0.500,NOT_DWA15);
        $setuphold(posedge CLKW &&& wr_flagA,negedge RF_DWA[16],1.000,0.500,NOT_DWA16);
        $setuphold(posedge CLKW &&& wr_flagA,negedge RF_DWA[17],1.000,0.500,NOT_DWA17);
        $setuphold(posedge CLKW &&& wr_flagA,negedge RF_DWA[18],1.000,0.500,NOT_DWA18);
        $setuphold(posedge CLKW &&& wr_flagA,negedge RF_DWA[19],1.000,0.500,NOT_DWA19);
        $setuphold(posedge CLKW &&& wr_flagA,negedge RF_DWA[20],1.000,0.500,NOT_DWA20);
        $setuphold(posedge CLKW &&& wr_flagA,negedge RF_DWA[21],1.000,0.500,NOT_DWA21);
        $setuphold(posedge CLKW &&& wr_flagA,negedge RF_DWA[22],1.000,0.500,NOT_DWA22);
        $setuphold(posedge CLKW &&& wr_flagA,negedge RF_DWA[23],1.000,0.500,NOT_DWA23);
        $setuphold(posedge CLKW &&& wr_flagA,negedge RF_DWA[24],1.000,0.500,NOT_DWA24);
        $setuphold(posedge CLKW &&& wr_flagA,negedge RF_DWA[25],1.000,0.500,NOT_DWA25);
        $setuphold(posedge CLKW &&& wr_flagA,negedge RF_DWA[26],1.000,0.500,NOT_DWA26);
        $setuphold(posedge CLKW &&& wr_flagA,negedge RF_DWA[27],1.000,0.500,NOT_DWA27);
        $setuphold(posedge CLKW &&& wr_flagA,negedge RF_DWA[28],1.000,0.500,NOT_DWA28);
        $setuphold(posedge CLKW &&& wr_flagA,negedge RF_DWA[29],1.000,0.500,NOT_DWA29);
        $setuphold(posedge CLKW &&& wr_flagA,negedge RF_DWA[30],1.000,0.500,NOT_DWA30);
        $setuphold(posedge CLKW &&& wr_flagA,negedge RF_DWA[31],1.000,0.500,NOT_DWA31);

        $setuphold(posedge CLKW, negedge RF_CENSTWA_,1.000,0.500,NOT_CENSTWA);

	$setuphold(posedge CLKW &&& wr_flagB,negedge RF_AWB[00],1.000,0.500,NOT_AWB0);
        $setuphold(posedge CLKW &&& wr_flagB,negedge RF_AWB[01],1.000,0.500,NOT_AWB1);
        $setuphold(posedge CLKW &&& wr_flagB,negedge RF_AWB[02],1.000,0.500,NOT_AWB2);
        $setuphold(posedge CLKW &&& wr_flagB,negedge RF_AWB[03],1.000,0.500,NOT_AWB3);
        $setuphold(posedge CLKW &&& wr_flagB,negedge RF_AWB[04],1.000,0.500,NOT_AWB4);

        $setuphold(posedge CLKW &&& wr_flagB,negedge RF_DWB[00],1.000,0.500,NOT_DWB0);
        $setuphold(posedge CLKW &&& wr_flagB,negedge RF_DWB[01],1.000,0.500,NOT_DWB1);
        $setuphold(posedge CLKW &&& wr_flagB,negedge RF_DWB[02],1.000,0.500,NOT_DWB2);
        $setuphold(posedge CLKW &&& wr_flagB,negedge RF_DWB[03],1.000,0.500,NOT_DWB3);
        $setuphold(posedge CLKW &&& wr_flagB,negedge RF_DWB[04],1.000,0.500,NOT_DWB4);
        $setuphold(posedge CLKW &&& wr_flagB,negedge RF_DWB[05],1.000,0.500,NOT_DWB5);
        $setuphold(posedge CLKW &&& wr_flagB,negedge RF_DWB[06],1.000,0.500,NOT_DWB6);
        $setuphold(posedge CLKW &&& wr_flagB,negedge RF_DWB[07],1.000,0.500,NOT_DWB7);
        $setuphold(posedge CLKW &&& wr_flagB,negedge RF_DWB[08],1.000,0.500,NOT_DWB8);
        $setuphold(posedge CLKW &&& wr_flagB,negedge RF_DWB[09],1.000,0.500,NOT_DWB9);
        $setuphold(posedge CLKW &&& wr_flagB,negedge RF_DWB[10],1.000,0.500,NOT_DWB10);
        $setuphold(posedge CLKW &&& wr_flagB,negedge RF_DWB[11],1.000,0.500,NOT_DWB11);
        $setuphold(posedge CLKW &&& wr_flagB,negedge RF_DWB[12],1.000,0.500,NOT_DWB12);
        $setuphold(posedge CLKW &&& wr_flagB,negedge RF_DWB[13],1.000,0.500,NOT_DWB13);
        $setuphold(posedge CLKW &&& wr_flagB,negedge RF_DWB[14],1.000,0.500,NOT_DWB14);
        $setuphold(posedge CLKW &&& wr_flagB,negedge RF_DWB[15],1.000,0.500,NOT_DWB15);
        $setuphold(posedge CLKW &&& wr_flagB,negedge RF_DWB[16],1.000,0.500,NOT_DWB16);
        $setuphold(posedge CLKW &&& wr_flagB,negedge RF_DWB[17],1.000,0.500,NOT_DWB17);
        $setuphold(posedge CLKW &&& wr_flagB,negedge RF_DWB[18],1.000,0.500,NOT_DWB18);
        $setuphold(posedge CLKW &&& wr_flagB,negedge RF_DWB[19],1.000,0.500,NOT_DWB19);
        $setuphold(posedge CLKW &&& wr_flagB,negedge RF_DWB[20],1.000,0.500,NOT_DWB20);
        $setuphold(posedge CLKW &&& wr_flagB,negedge RF_DWB[21],1.000,0.500,NOT_DWB21);
        $setuphold(posedge CLKW &&& wr_flagB,negedge RF_DWB[22],1.000,0.500,NOT_DWB22);
        $setuphold(posedge CLKW &&& wr_flagB,negedge RF_DWB[23],1.000,0.500,NOT_DWB23);
        $setuphold(posedge CLKW &&& wr_flagB,negedge RF_DWB[24],1.000,0.500,NOT_DWB24);
        $setuphold(posedge CLKW &&& wr_flagB,negedge RF_DWB[25],1.000,0.500,NOT_DWB25);
        $setuphold(posedge CLKW &&& wr_flagB,negedge RF_DWB[26],1.000,0.500,NOT_DWB26);
        $setuphold(posedge CLKW &&& wr_flagB,negedge RF_DWB[27],1.000,0.500,NOT_DWB27);
        $setuphold(posedge CLKW &&& wr_flagB,negedge RF_DWB[28],1.000,0.500,NOT_DWB28);
        $setuphold(posedge CLKW &&& wr_flagB,negedge RF_DWB[29],1.000,0.500,NOT_DWB29);
        $setuphold(posedge CLKW &&& wr_flagB,negedge RF_DWB[30],1.000,0.500,NOT_DWB30);
        $setuphold(posedge CLKW &&& wr_flagB,negedge RF_DWB[31],1.000,0.500,NOT_DWB31);

        $setuphold(posedge CLKW, negedge RF_CENSTWB_,1.000,0.500,NOT_CENSTWB);

        $setuphold(posedge CLKW &&& wr_flagC,negedge RF_AWC[00],1.000,0.500,NOT_AWC0);
        $setuphold(posedge CLKW &&& wr_flagC,negedge RF_AWC[01],1.000,0.500,NOT_AWC1);
        $setuphold(posedge CLKW &&& wr_flagC,negedge RF_AWC[02],1.000,0.500,NOT_AWC2);
        $setuphold(posedge CLKW &&& wr_flagC,negedge RF_AWC[03],1.000,0.500,NOT_AWC3);
        $setuphold(posedge CLKW &&& wr_flagC,negedge RF_AWC[04],1.000,0.500,NOT_AWC4);

        $setuphold(posedge CLKW &&& wr_flagC,negedge RF_DWC[00],1.000,0.500,NOT_DWC0);
        $setuphold(posedge CLKW &&& wr_flagC,negedge RF_DWC[01],1.000,0.500,NOT_DWC1);
        $setuphold(posedge CLKW &&& wr_flagC,negedge RF_DWC[02],1.000,0.500,NOT_DWC2);
        $setuphold(posedge CLKW &&& wr_flagC,negedge RF_DWC[03],1.000,0.500,NOT_DWC3);
        $setuphold(posedge CLKW &&& wr_flagC,negedge RF_DWC[04],1.000,0.500,NOT_DWC4);
        $setuphold(posedge CLKW &&& wr_flagC,negedge RF_DWC[05],1.000,0.500,NOT_DWC5);
        $setuphold(posedge CLKW &&& wr_flagC,negedge RF_DWC[06],1.000,0.500,NOT_DWC6);
        $setuphold(posedge CLKW &&& wr_flagC,negedge RF_DWC[07],1.000,0.500,NOT_DWC7);
        $setuphold(posedge CLKW &&& wr_flagC,negedge RF_DWC[08],1.000,0.500,NOT_DWC8);
        $setuphold(posedge CLKW &&& wr_flagC,negedge RF_DWC[09],1.000,0.500,NOT_DWC9);
        $setuphold(posedge CLKW &&& wr_flagC,negedge RF_DWC[10],1.000,0.500,NOT_DWC10);
        $setuphold(posedge CLKW &&& wr_flagC,negedge RF_DWC[11],1.000,0.500,NOT_DWC11);
        $setuphold(posedge CLKW &&& wr_flagC,negedge RF_DWC[12],1.000,0.500,NOT_DWC12);
        $setuphold(posedge CLKW &&& wr_flagC,negedge RF_DWC[13],1.000,0.500,NOT_DWC13);
        $setuphold(posedge CLKW &&& wr_flagC,negedge RF_DWC[14],1.000,0.500,NOT_DWC14);
        $setuphold(posedge CLKW &&& wr_flagC,negedge RF_DWC[15],1.000,0.500,NOT_DWC15);
        $setuphold(posedge CLKW &&& wr_flagC,negedge RF_DWC[16],1.000,0.500,NOT_DWC16);
        $setuphold(posedge CLKW &&& wr_flagC,negedge RF_DWC[17],1.000,0.500,NOT_DWC17);
        $setuphold(posedge CLKW &&& wr_flagC,negedge RF_DWC[18],1.000,0.500,NOT_DWC18);
        $setuphold(posedge CLKW &&& wr_flagC,negedge RF_DWC[19],1.000,0.500,NOT_DWC19);
        $setuphold(posedge CLKW &&& wr_flagC,negedge RF_DWC[20],1.000,0.500,NOT_DWC20);
        $setuphold(posedge CLKW &&& wr_flagC,negedge RF_DWC[21],1.000,0.500,NOT_DWC21);
        $setuphold(posedge CLKW &&& wr_flagC,negedge RF_DWC[22],1.000,0.500,NOT_DWC22);
        $setuphold(posedge CLKW &&& wr_flagC,negedge RF_DWC[23],1.000,0.500,NOT_DWC23);
        $setuphold(posedge CLKW &&& wr_flagC,negedge RF_DWC[24],1.000,0.500,NOT_DWC24);
        $setuphold(posedge CLKW &&& wr_flagC,negedge RF_DWC[25],1.000,0.500,NOT_DWC25);
        $setuphold(posedge CLKW &&& wr_flagC,negedge RF_DWC[26],1.000,0.500,NOT_DWC26);
        $setuphold(posedge CLKW &&& wr_flagC,negedge RF_DWC[27],1.000,0.500,NOT_DWC27);
        $setuphold(posedge CLKW &&& wr_flagC,negedge RF_DWC[28],1.000,0.500,NOT_DWC28);
        $setuphold(posedge CLKW &&& wr_flagC,negedge RF_DWC[29],1.000,0.500,NOT_DWC29);
        $setuphold(posedge CLKW &&& wr_flagC,negedge RF_DWC[30],1.000,0.500,NOT_DWC30);
        $setuphold(posedge CLKW &&& wr_flagC,negedge RF_DWC[31],1.000,0.500,NOT_DWC31);

        $setuphold(posedge CLKW, negedge RF_CENSTWC_,1.000,0.500,NOT_CENSTWC);

        $setuphold(posedge CLKW &&& wr_flagD,negedge RF_AWD[00],1.000,0.500,NOT_AWD0);
        $setuphold(posedge CLKW &&& wr_flagD,negedge RF_AWD[01],1.000,0.500,NOT_AWD1);
        $setuphold(posedge CLKW &&& wr_flagD,negedge RF_AWD[02],1.000,0.500,NOT_AWD2);
        $setuphold(posedge CLKW &&& wr_flagD,negedge RF_AWD[03],1.000,0.500,NOT_AWD3);
        $setuphold(posedge CLKW &&& wr_flagD,negedge RF_AWD[04],1.000,0.500,NOT_AWD4);

        $setuphold(posedge CLKW &&& wr_flagD,negedge RF_DWD[00],1.000,0.500,NOT_DWD0);
        $setuphold(posedge CLKW &&& wr_flagD,negedge RF_DWD[01],1.000,0.500,NOT_DWD1);
        $setuphold(posedge CLKW &&& wr_flagD,negedge RF_DWD[02],1.000,0.500,NOT_DWD2);
        $setuphold(posedge CLKW &&& wr_flagD,negedge RF_DWD[03],1.000,0.500,NOT_DWD3);
        $setuphold(posedge CLKW &&& wr_flagD,negedge RF_DWD[04],1.000,0.500,NOT_DWD4);
        $setuphold(posedge CLKW &&& wr_flagD,negedge RF_DWD[05],1.000,0.500,NOT_DWD5);
        $setuphold(posedge CLKW &&& wr_flagD,negedge RF_DWD[06],1.000,0.500,NOT_DWD6);
        $setuphold(posedge CLKW &&& wr_flagD,negedge RF_DWD[07],1.000,0.500,NOT_DWD7);
        $setuphold(posedge CLKW &&& wr_flagD,negedge RF_DWD[08],1.000,0.500,NOT_DWD8);
        $setuphold(posedge CLKW &&& wr_flagD,negedge RF_DWD[09],1.000,0.500,NOT_DWD9);
        $setuphold(posedge CLKW &&& wr_flagD,negedge RF_DWD[10],1.000,0.500,NOT_DWD10);
        $setuphold(posedge CLKW &&& wr_flagD,negedge RF_DWD[11],1.000,0.500,NOT_DWD11);
        $setuphold(posedge CLKW &&& wr_flagD,negedge RF_DWD[12],1.000,0.500,NOT_DWD12);
        $setuphold(posedge CLKW &&& wr_flagD,negedge RF_DWD[13],1.000,0.500,NOT_DWD13);
        $setuphold(posedge CLKW &&& wr_flagD,negedge RF_DWD[14],1.000,0.500,NOT_DWD14);
        $setuphold(posedge CLKW &&& wr_flagD,negedge RF_DWD[15],1.000,0.500,NOT_DWD15);
        $setuphold(posedge CLKW &&& wr_flagD,negedge RF_DWD[16],1.000,0.500,NOT_DWD16);
        $setuphold(posedge CLKW &&& wr_flagD,negedge RF_DWD[17],1.000,0.500,NOT_DWD17);
        $setuphold(posedge CLKW &&& wr_flagD,negedge RF_DWD[18],1.000,0.500,NOT_DWD18);
        $setuphold(posedge CLKW &&& wr_flagD,negedge RF_DWD[19],1.000,0.500,NOT_DWD19);
        $setuphold(posedge CLKW &&& wr_flagD,negedge RF_DWD[20],1.000,0.500,NOT_DWD20);
        $setuphold(posedge CLKW &&& wr_flagD,negedge RF_DWD[21],1.000,0.500,NOT_DWD21);
        $setuphold(posedge CLKW &&& wr_flagD,negedge RF_DWD[22],1.000,0.500,NOT_DWD22);
        $setuphold(posedge CLKW &&& wr_flagD,negedge RF_DWD[23],1.000,0.500,NOT_DWD23);
        $setuphold(posedge CLKW &&& wr_flagD,negedge RF_DWD[24],1.000,0.500,NOT_DWD24);
        $setuphold(posedge CLKW &&& wr_flagD,negedge RF_DWD[25],1.000,0.500,NOT_DWD25);
        $setuphold(posedge CLKW &&& wr_flagD,negedge RF_DWD[26],1.000,0.500,NOT_DWD26);
        $setuphold(posedge CLKW &&& wr_flagD,negedge RF_DWD[27],1.000,0.500,NOT_DWD27);
        $setuphold(posedge CLKW &&& wr_flagD,negedge RF_DWD[28],1.000,0.500,NOT_DWD28);
        $setuphold(posedge CLKW &&& wr_flagD,negedge RF_DWD[29],1.000,0.500,NOT_DWD29);
        $setuphold(posedge CLKW &&& wr_flagD,negedge RF_DWD[30],1.000,0.500,NOT_DWD30);
        $setuphold(posedge CLKW &&& wr_flagD,negedge RF_DWD[31],1.000,0.500,NOT_DWD31);

        $setuphold(posedge CLKW ,negedge RF_CENSTWD_,1.000,0.500,NOT_CENSTWD);

        /*===============================
            misc signal timing check
        ================================*/

        $setuphold(posedge CLKR ,negedge RF_CENCA_,1.000,0.500,NOT_CENCA);
        $setuphold(posedge CLKR ,negedge RF_CENCB_,1.000,0.500,NOT_CENCB);


        $setuphold(posedge CLKW ,negedge RD_ENCA,1.000,0.500,NOT_ENCA);
        $setuphold(posedge CLKW ,negedge DISP_CA_S,1.000,0.500,NOT_CAS);
        $setuphold(posedge CLKW ,negedge RD_ENCB,1.000,0.500,NOT_ENCB);
        $setuphold(posedge CLKW ,negedge DISP_CB_S,1.000,0.500,NOT_CBS);
        $setuphold(posedge CLKW ,negedge DISWAW_CA,1.000,0.500,NOT_WAWCA);
        $setuphold(posedge CLKW ,negedge DISWAW_CB,1.000,0.500,NOT_WAWCB);
        $setuphold(posedge CLKW ,negedge DESALUA_CA,1.000,0.500,NOT_ALUACA);
        $setuphold(posedge CLKW ,negedge DESALUA_CB,1.000,0.500,NOT_ALUACB);
        $setuphold(posedge CLKW ,negedge RST_,1.000,0.500,NOT_RST);

        $setuphold(posedge CLKR ,negedge RD_ENCA,1.000,0.500,NOT_ENCA);

        $setuphold(posedge CLKR ,negedge TEST_SE,1.000,0.500,NOT_SE);
        $setuphold(posedge CLKR ,negedge TEST_SI0,1.000,0.500,NOT_SI0);
        $setuphold(posedge CLKR ,negedge TEST_SI1,1.000,0.500,NOT_SI1);

        $setuphold(posedge CLKR ,negedge RF_RAREDO_EN,1.000,0.500,NOT_RAEN);
        $setuphold(posedge CLKR ,negedge RF_RBREDO_EN,1.000,0.500,NOT_RBEN);
        $setuphold(posedge CLKR ,negedge RF_RCREDO_EN,1.000,0.500,NOT_RCEN);
        $setuphold(posedge CLKR ,negedge RF_RDREDO_EN,1.000,0.500,NOT_RDEN);

        $setuphold(posedge CLKR ,negedge RF_CAREDO_EN,1.000,0.500,NOT_CAEN);
        $setuphold(posedge CLKR ,negedge RF_CBREDO_EN,1.000,0.500,NOT_CBEN);


        //===================================

        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_REG0[00],1.000,0.500,NOT_REG00);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_REG0[01],1.000,0.500,NOT_REG01);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_REG0[02],1.000,0.500,NOT_REG02);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_REG0[03],1.000,0.500,NOT_REG03);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_REG0[04],1.000,0.500,NOT_REG04);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_REG0[05],1.000,0.500,NOT_REG05);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_REG0[06],1.000,0.500,NOT_REG06);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_REG0[07],1.000,0.500,NOT_REG07);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_REG0[08],1.000,0.500,NOT_REG08);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_REG0[09],1.000,0.500,NOT_REG09);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_REG0[10],1.000,0.500,NOT_REG010);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_REG0[11],1.000,0.500,NOT_REG011);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_REG0[12],1.000,0.500,NOT_REG012);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_REG0[13],1.000,0.500,NOT_REG013);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_REG0[14],1.000,0.500,NOT_REG014);

        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_REG1[00],1.000,0.500,NOT_REG10);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_REG1[01],1.000,0.500,NOT_REG11);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_REG1[02],1.000,0.500,NOT_REG12);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_REG1[03],1.000,0.500,NOT_REG13);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_REG1[04],1.000,0.500,NOT_REG14);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_REG1[05],1.000,0.500,NOT_REG15);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_REG1[06],1.000,0.500,NOT_REG16);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_REG1[07],1.000,0.500,NOT_REG17);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_REG1[08],1.000,0.500,NOT_REG18);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_REG1[09],1.000,0.500,NOT_REG19);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_REG1[10],1.000,0.500,NOT_REG110);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_REG1[11],1.000,0.500,NOT_REG111);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_REG1[12],1.000,0.500,NOT_REG112);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_REG1[13],1.000,0.500,NOT_REG113);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_REG1[14],1.000,0.500,NOT_REG114);

        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_IN0[00],1.000,0.500,NOT_IN00);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_IN0[01],1.000,0.500,NOT_IN01);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_IN0[02],1.000,0.500,NOT_IN02);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_IN0[03],1.000,0.500,NOT_IN03);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_IN0[04],1.000,0.500,NOT_IN04);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_IN0[05],1.000,0.500,NOT_IN05);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_IN0[06],1.000,0.500,NOT_IN06);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_IN0[07],1.000,0.500,NOT_IN07);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_IN0[08],1.000,0.500,NOT_IN08);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_IN0[09],1.000,0.500,NOT_IN09);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_IN0[10],1.000,0.500,NOT_IN010);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_IN0[11],1.000,0.500,NOT_IN011);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_IN0[12],1.000,0.500,NOT_IN012);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_IN0[13],1.000,0.500,NOT_IN013);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_IN0[14],1.000,0.500,NOT_IN014);

        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_IN1[00],1.000,0.500,NOT_IN10);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_IN1[01],1.000,0.500,NOT_IN11);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_IN1[02],1.000,0.500,NOT_IN12);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_IN1[03],1.000,0.500,NOT_IN13);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_IN1[04],1.000,0.500,NOT_IN14);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_IN1[05],1.000,0.500,NOT_IN15);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_IN1[06],1.000,0.500,NOT_IN16);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_IN1[07],1.000,0.500,NOT_IN17);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_IN1[08],1.000,0.500,NOT_IN18);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_IN1[09],1.000,0.500,NOT_IN19);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_IN1[10],1.000,0.500,NOT_IN110);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_IN1[11],1.000,0.500,NOT_IN111);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_IN1[12],1.000,0.500,NOT_IN112);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_IN1[13],1.000,0.500,NOT_IN113);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_RSRTRD_IN1[14],1.000,0.500,NOT_IN114);

        $setuphold(posedge CLKR &&& re_flag,negedge ID_ADDR_SEL0,1.000,0.500,NOT_SEL0);
        $setuphold(posedge CLKR &&& re_flag,negedge ID_ADDR_SEL1,1.000,0.500,NOT_SEL1);

        /*===========================
            clock period & width
        ============================*/

        $period(posedge CLKR ,3.000,NOT_CLKR_PER);
        $width(posedge CLKR,1.000,0,NOT_CLKR_MINH);
        $width(negedge CLKR,1.000,0,NOT_CLKR_MINH);
        $width(posedge CLKR,1.000,0,NOT_CLKR_MINL);
        $width(negedge CLKR,1.000,0,NOT_CLKR_MINL);

        $period(posedge CLKW ,3.000,NOT_CLKW_PER);
        $width(posedge CLKW,1.000,0,NOT_CLKW_MINH);
        $width(negedge CLKW,1.000,0,NOT_CLKW_MINH);
        $width(posedge CLKW,1.000,0,NOT_CLKW_MINL);
        $width(negedge CLKW,1.000,0,NOT_CLKW_MINL);


        /*===========================
            define IOPATH
        ============================*/

        (CLKR => RF_QRA[00])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRA[01])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRA[02])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRA[03])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRA[04])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRA[05])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRA[06])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRA[07])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRA[08])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRA[09])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRA[10])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRA[11])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRA[12])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRA[13])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRA[14])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRA[15])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRA[16])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRA[17])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRA[18])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRA[19])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRA[20])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRA[21])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRA[22])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRA[23])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRA[24])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRA[25])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRA[26])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRA[27])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRA[28])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRA[29])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRA[30])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRA[31])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);

        (CLKR => RF_QRB[00])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRB[01])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRB[02])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRB[03])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRB[04])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRB[05])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRB[06])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRB[07])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRB[08])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRB[09])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRB[10])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRB[11])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRB[12])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRB[13])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRB[14])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRB[15])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRB[16])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRB[17])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRB[18])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRB[19])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRB[20])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRB[21])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRB[22])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRB[23])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRB[24])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRB[25])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRB[26])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRB[27])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRB[28])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRB[29])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRB[30])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRB[31])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);

        (CLKR => RF_QRC[00])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRC[01])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRC[02])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRC[03])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRC[04])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRC[05])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRC[06])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRC[07])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRC[08])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRC[09])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRC[10])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRC[11])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRC[12])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRC[13])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRC[14])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRC[15])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRC[16])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRC[17])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRC[18])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRC[19])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRC[20])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRC[21])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRC[22])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRC[23])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRC[24])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRC[25])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRC[26])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRC[27])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRC[28])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRC[29])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRC[30])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRC[31])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);

        (CLKR => RF_QRD[00])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRD[01])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRD[02])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRD[03])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRD[04])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRD[05])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRD[06])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRD[07])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRD[08])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRD[09])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRD[10])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRD[11])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRD[12])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRD[13])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRD[14])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRD[15])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRD[16])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRD[17])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRD[18])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRD[19])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRD[20])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRD[21])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRD[22])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRD[23])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRD[24])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRD[25])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRD[26])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRD[27])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRD[28])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRD[29])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRD[30])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_QRD[31])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);

        (CLKR => TEST_SO1)=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => TEST_SO0)=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);

        (CLKR => RF_STRA[4])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_STRA[3])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_STRA[2])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_STRA[1])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_STRA[0])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);

        (CLKR => RF_STRB[4])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_STRB[3])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_STRB[2])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_STRB[1])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_STRB[0])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);

        (CLKR => RF_STRC[4])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_STRC[3])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_STRC[2])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_STRC[1])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_STRC[0])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);

        (CLKR => RF_STRD[4])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_STRD[3])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_STRD[2])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_STRD[1])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_STRD[0])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);

        (CLKR => RF_STCA[4])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_STCA[3])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_STCA[2])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_STCA[1])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_STCA[0])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);

        (CLKR => RF_STCB[4])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_STCB[3])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_STCB[2])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_STCB[1])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLKR => RF_STCB[0])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);

    endspecify


///////////////////////////////////////////////////////////////////////////////////////
// decode and mux portion //

	wire [`RFA_WIDTH-1:0]   RF_ACA ;
	wire [`RFA_WIDTH-1:0]   RF_ACB ;
	wire [`RFA_WIDTH-1:0]   RF_ARA ;
	wire [`RFA_WIDTH-1:0]   RF_ARB ;
	wire [`RFA_WIDTH-1:0]   RF_ARC ;
	wire [`RFA_WIDTH-1:0]   RF_ARD ;

	wire [`RFAD_WIDTH-1:0]	awa_d  ;
	wire [`RFAD_WIDTH-1:0]	awb_d  ;
	wire [`RFAD_WIDTH-1:0]	awc_d  ;
	wire [`RFAD_WIDTH-1:0]	awd_d  ;


wire  sel_case0 = {ID_ADDR_SEL0i, ID_ADDR_SEL1i} == 2'b00 ;
wire  sel_case1 = {ID_ADDR_SEL0i, ID_ADDR_SEL1i} == 2'b01 ;
wire  sel_case2 = ID_ADDR_SEL0i == 1'b1 ;

assign       RF_ARA = {5{sel_case0}} & ID_RSRTRD_IN0i[14:10]  |
                      {5{sel_case1}} & ID_RSRTRD_REG1i[14:10] |
                      {5{sel_case2}} & ID_RSRTRD_REG0i[14:10] ;

assign       RF_ARB = {5{sel_case0}} & ID_RSRTRD_IN0i[9:5]    |
                      {5{sel_case1}} & ID_RSRTRD_REG1i[9:5]   |
                      {5{sel_case2}} & ID_RSRTRD_REG0i[9:5]   ;

assign       RF_ARC = {5{sel_case0}} & ID_RSRTRD_IN1i[14:10]  |
                      {5{sel_case1}} & ID_RSRTRD_IN0i[14:10]  |
                      {5{sel_case2}} & ID_RSRTRD_REG1i[14:10] ;

assign       RF_ARD = {5{sel_case0}} & ID_RSRTRD_IN1i[9:5]    |
                      {5{sel_case1}} & ID_RSRTRD_IN0i[9:5]    |
                      {5{sel_case2}} & ID_RSRTRD_REG1i[9:5]   ;

assign       RF_ACA = {5{sel_case0}} & ID_RSRTRD_IN0i[4:0]    |
                      {5{sel_case1}} & ID_RSRTRD_REG1i[4:0]   |
                      {5{sel_case2}} & ID_RSRTRD_REG0i[4:0]   ;

assign       RF_ACB = {5{sel_case0}} & ID_RSRTRD_IN1i[4:0]    |
                      {5{sel_case1}} & ID_RSRTRD_IN0i[4:0]    |
                      {5{sel_case2}} & ID_RSRTRD_REG1i[4:0]   ;

wire [3:0]   ara_d_xa = 1'b1 << RF_ARA[4:3] ;
wire [7:0]   ara_d_xb = 1'b1 << RF_ARA[2:0] ;
wire [3:0]   arb_d_xa = 1'b1 << RF_ARB[4:3] ;
wire [7:0]   arb_d_xb = 1'b1 << RF_ARB[2:0] ;
wire [3:0]   arc_d_xa = 1'b1 << RF_ARC[4:3] ;
wire [7:0]   arc_d_xb = 1'b1 << RF_ARC[2:0] ;
wire [3:0]   ard_d_xa = 1'b1 << RF_ARD[4:3] ;
wire [7:0]   ard_d_xb = 1'b1 << RF_ARD[2:0] ;

wire [3:0]   aca_d_xa = 1'b1 << RF_ACA[4:3] ;
wire [7:0]   aca_d_xb = 1'b1 << RF_ACA[2:0] ;
wire [3:0]   acb_d_xa = 1'b1 << RF_ACB[4:3] ;
wire [7:0]   acb_d_xb = 1'b1 << RF_ACB[2:0] ;

wire [3:0]   awa_d_xa = 1'b1 << RF_AWAi[4:3] ;
wire [7:0]   awa_d_xb = 1'b1 << RF_AWAi[2:0] ;
wire [3:0]   awb_d_xa = 1'b1 << RF_AWBi[4:3] ;
wire [7:0]   awb_d_xb = 1'b1 << RF_AWBi[2:0] ;
wire [3:0]   awc_d_xa = 1'b1 << RF_AWCi[4:3] ;
wire [7:0]   awc_d_xb = 1'b1 << RF_AWCi[2:0] ;
wire [3:0]   awd_d_xa = 1'b1 << RF_AWDi[4:3] ;
wire [7:0]   awd_d_xb = 1'b1 << RF_AWDi[2:0] ;

/////////////////////////////////////////////////////////////////////////////////

	reg	 [`RFD_WIDTH-1:0]	dwa_lch;
	reg	 [`RFD_WIDTH-1:0]	dwb_lch;
	reg	 [`RFD_WIDTH-1:0]	dwc_lch;
	reg	 [`RFD_WIDTH-1:0]	dwd_lch;
  	reg [`RFA_WIDTH-1:0]    acb_lch;
 	reg [`RFA_WIDTH-1:0]    aca_lch;
  	reg 	                cenca_lch_;
  	reg                     cencb_lch_	;
//////////////////////////////////////////////////////////////////////
// to generate the internal clk //

    wire  #rphase_dly lcd_clkr = CLKRi;
	wire  #wphase_dly lcd_clkw = CLKWi;
    wire  #(2*U_DLY) lcd_dly_clkr = lcd_clkr;
    wire  #(2*U_DLY) lcd_dly_clkw = lcd_clkw;


/////////////////////////////////////////////////////////////////////////
    reg    lcd_rf_censtwa_ ;
    reg    lcd_rf_censtwb_ ;
    reg    lcd_rf_censtwc_ ;
    reg    lcd_rf_censtwd_ ;
	reg	   disp_ca_s_lch ;
    reg	   disp_cb_s_lch ;
    reg    lcd_rf_cenra_ ;
    reg    lcd_rf_cenrb_ ;
    reg    lcd_rf_cenrc_ ;
    reg    lcd_rf_cenrd_ ;
    reg    lcd_rf_cenca_ ;
    reg    lcd_rf_cencb_ ;

    always @(posedge CLKWi)
      begin
       lcd_rf_censtwa_ <= #U_DLY RF_CENSTWAi_ ;
       lcd_rf_censtwb_ <= #U_DLY RF_CENSTWBi_ ;
       lcd_rf_censtwc_ <= #U_DLY RF_CENSTWCi_ ;
       lcd_rf_censtwd_ <= #U_DLY RF_CENSTWDi_ ;
	   disp_ca_s_lch   <= #U_DLY DISP_CA_Si ;
       disp_cb_s_lch   <= #U_DLY DISP_CB_Si ;
      end

    always @(posedge CLKRi)
      begin
        lcd_rf_cenra_ <= #U_DLY RF_CENRAi_ ;
        lcd_rf_cenrb_ <= #U_DLY RF_CENRBi_ ;
        lcd_rf_cenrc_ <= #U_DLY RF_CENRCi_ ;
        lcd_rf_cenrd_ <= #U_DLY RF_CENRDi_ ;
        lcd_rf_cenca_ <= #U_DLY RF_CENCAi_ ;
        lcd_rf_cencb_ <= #U_DLY RF_CENCBi_ ;
      end

/////////////////////////////////////////////////////////////////////////
	// write port a
   	reg	[3:0]	awa_d_xa_lch ;
   	reg [7:0]   awa_d_xb_lch ;
    reg         cenwa_lch ;
    wire    cenwa       = ~RF_CENWAi_ ;
	wire	wa_clk      = (TEST_SEi | ~lcd_rf_censtwa_) & lcd_clkw;
    wire #(2*U_DLY) wa_dly_clk = wa_clk;
	wire	array_cenwa = ~TEST_SEi & cenwa_lch & wa_dly_clk ;



    always @(posedge wa_clk)
	begin
	     awa_d_xa_lch <= #U_DLY awa_d_xa ;
	     awa_d_xb_lch <= #U_DLY awa_d_xb ;
	end

    wire [31:0]  awa_dlch = {({8{awa_d_xa_lch[3]}} & awa_d_xb_lch),
                             ({8{awa_d_xa_lch[2]}} & awa_d_xb_lch),
                             ({8{awa_d_xa_lch[1]}} & awa_d_xb_lch),
                             ({8{awa_d_xa_lch[0]}} & awa_d_xb_lch) };

	always @(posedge wa_clk)
	begin
			cenwa_lch <= #U_DLY cenwa ;
	end

	always @(posedge wa_clk)
	begin
			dwa_lch <= #U_DLY RF_DWAi;
	end

	// write port b
	reg [3:0]  awb_d_xa_lch ;
	reg [7:0]  awb_d_xb_lch ;
	reg        cenwb_lch ;
    wire    cenwb = ~RF_CENWBi_ ;
	wire	wb_clk = (TEST_SEi | ~lcd_rf_censtwb_ ) & lcd_clkw;
    wire #(2*U_DLY) wb_dly_clk = wb_clk;
	wire	array_cenwb = ~TEST_SEi & cenwb_lch & wb_dly_clk;

    always @(posedge wb_clk)
	begin
	      awb_d_xa_lch <= #U_DLY awb_d_xa ;
	      awb_d_xb_lch <= #U_DLY awb_d_xb ;
	end

    wire [31:0]  awb_dlch =   {({8{awb_d_xa_lch[3]}} & awb_d_xb_lch),
                               ({8{awb_d_xa_lch[2]}} & awb_d_xb_lch),
                               ({8{awb_d_xa_lch[1]}} & awb_d_xb_lch),
                               ({8{awb_d_xa_lch[0]}} & awb_d_xb_lch) };


	always @(posedge wb_clk)
	begin
			dwb_lch <= #U_DLY RF_DWBi;
	end

	always @(posedge wb_clk)
	begin
			cenwb_lch  <= #U_DLY cenwb ;
	end


	// write port c
   	reg	[3:0]	awc_d_xa_lch;
	reg [7:0]   awc_d_xb_lch;
	reg         cenwc_lch ;
   	wire    cenwc  = ~RF_CENWCi_ ;
	wire	wc_clk = ( TEST_SEi | ~lcd_rf_censtwc_) & lcd_clkw;
    wire #(2*U_DLY) wc_dly_clk = wc_clk;
	wire	array_cenwc = ~TEST_SEi & cenwc_lch & wc_dly_clk;

    always @(posedge wc_clk)
	begin
	       awc_d_xa_lch <= #U_DLY awc_d_xa ;
	       awc_d_xb_lch <= #U_DLY awc_d_xb ;
	end

    wire [31:0]  awc_dlch =   {({8{awc_d_xa_lch[3]}} & awc_d_xb_lch),
                               ({8{awc_d_xa_lch[2]}} & awc_d_xb_lch),
                               ({8{awc_d_xa_lch[1]}} & awc_d_xb_lch),
                               ({8{awc_d_xa_lch[0]}} & awc_d_xb_lch) };


	always @(posedge wc_clk)
	begin
			dwc_lch <= #U_DLY RF_DWCi;
	end

	always @(posedge wc_clk)
	begin
			cenwc_lch <= #U_DLY cenwc ;
	end


	// write port d
   	reg	[3:0]	awd_d_xa_lch ;
	reg [7:0]   awd_d_xb_lch ;
	reg         cenwd_lch ;
    wire    cenwd = ~RF_CENWDi_ ;
	wire	wd_clk = ( TEST_SEi | ~lcd_rf_censtwd_) & lcd_clkw;
    wire #(2*U_DLY) wd_dly_clk = wd_clk;
	wire	array_cenwd = ~TEST_SEi & cenwd_lch & wd_dly_clk;

    always @(posedge wd_clk)
	begin
	      awd_d_xa_lch <= #U_DLY awd_d_xa ;
	      awd_d_xb_lch <= #U_DLY awd_d_xb ;
	end

    wire [31:0]  awd_dlch =   {({8{awd_d_xa_lch[3]}} & awd_d_xb_lch),
                               ({8{awd_d_xa_lch[2]}} & awd_d_xb_lch),
                               ({8{awd_d_xa_lch[1]}} & awd_d_xb_lch),
                               ({8{awd_d_xa_lch[0]}} & awd_d_xb_lch) };


	always @(posedge wd_clk)
	begin
			dwd_lch <= #U_DLY RF_DWDi;
	end

	always @(posedge wd_clk)
	begin
			cenwd_lch <= #U_DLY ~RF_CENWDi_ ;
	end


	// read port a
	reg	[3:0]	ara_d_xa_lch ;
	reg [7:0]   ara_d_xb_lch ;

	reg         cenra_lch_ ;
    wire	cenra_ = ~RF_RAREDO_ENi;
	wire	ra_clk      = (TEST_SEi | ~lcd_rf_cenra_) & lcd_clkr;
	wire	array_cenra = ~cenra_lch_ & lcd_dly_clkr;   // gating

    always @(posedge ra_clk)
	begin
		if (TEST_SEi)
		  begin
			ara_d_xa_lch <= #U_DLY {acb_lch[0],ara_d_xa_lch[3:1]};
		    ara_d_xb_lch <= #U_DLY {ara_d_xa_lch[0],ara_d_xb_lch[7:1]};
		  end
		else
		  begin
			ara_d_xa_lch <= #U_DLY ara_d_xa ;
            ara_d_xb_lch <= #U_DLY ara_d_xb ;
          end
	end

    wire [31:0]  ara_dlch =   {({8{ara_d_xa_lch[3]}} & ara_d_xb_lch),
                               ({8{ara_d_xa_lch[2]}} & ara_d_xb_lch),
                               ({8{ara_d_xa_lch[1]}} & ara_d_xb_lch),
                               ({8{ara_d_xa_lch[0]}} & ara_d_xb_lch) };

	always @(posedge lcd_clkr)
	begin
		if (TEST_SEi)
			cenra_lch_ <= #U_DLY TEST_SI1i;
		else
			cenra_lch_ <= #U_DLY cenra_;
	end

	// read port b

	reg	[3:0]	arb_d_xa_lch  ;
	reg [7:0]   arb_d_xb_lch  ;
	reg         cenrb_lch_    ;
	wire	cenrb_ = ~RF_RBREDO_ENi;
	wire	rb_clk   = (TEST_SEi | ~lcd_rf_cenrb_) & lcd_clkr;
	wire	array_cenrb = ~cenrb_lch_  & lcd_dly_clkr;

	always @(posedge rb_clk)
	begin
       if (TEST_SEi)
         begin
           arb_d_xa_lch <= #U_DLY {ara_d_xb_lch[0], arb_d_xa_lch[3:1]} ;
		   arb_d_xb_lch <= #U_DLY {arb_d_xa_lch[0], arb_d_xb_lch[7:1]} ;
	     end
	   else
         begin
           arb_d_xa_lch <= #U_DLY arb_d_xa ;
		   arb_d_xb_lch <= #U_DLY arb_d_xb ;
	     end
	end

    wire [31:0]  arb_dlch = {({8{arb_d_xa_lch[3]}} & arb_d_xb_lch),
                             ({8{arb_d_xa_lch[2]}} & arb_d_xb_lch),
                             ({8{arb_d_xa_lch[1]}} & arb_d_xb_lch),
                             ({8{arb_d_xa_lch[0]}} & arb_d_xb_lch) };

	always @(posedge lcd_clkr)
	begin
		if (TEST_SEi)
			cenrb_lch_ <= #U_DLY cenra_lch_;
		else
			cenrb_lch_ <= #U_DLY cenrb_;
	end

	// read port c
	reg	[3:0]	arc_d_xa_lch	;
	reg [7:0]    arc_d_xb_lch    ;
	reg						cenrc_lch_	;
    wire	cenrc_ = ~RF_RCREDO_ENi;
	wire	rc_clk      = (TEST_SEi | ~lcd_rf_cenrc_) & lcd_clkr;
	wire	array_cenrc = ~cenrc_lch_ & lcd_dly_clkr;

	always @(posedge rc_clk)
	begin
		if (TEST_SEi)
		   begin
			arc_d_xa_lch <= #U_DLY {arb_d_xb_lch[0], arc_d_xa_lch[3:1]};
		    arc_d_xb_lch <= #U_DLY {arc_d_xa_lch[0], arc_d_xb_lch[7:1]};
		   end
		else
		   begin
			arc_d_xa_lch <= #U_DLY arc_d_xa ;
		    arc_d_xb_lch <= #U_DLY arc_d_xb ;
	       end
	end

    wire [31:0]  arc_dlch =   {({8{arc_d_xa_lch[3]}} & arc_d_xb_lch),
                               ({8{arc_d_xa_lch[2]}} & arc_d_xb_lch),
                               ({8{arc_d_xa_lch[1]}} & arc_d_xb_lch),
                               ({8{arc_d_xa_lch[0]}} & arc_d_xb_lch) };

	always @(posedge lcd_clkr)
	begin
		if (TEST_SEi)
			cenrc_lch_ <= #U_DLY cenrb_lch_;
		else
			cenrc_lch_ <= #U_DLY cenrc_;
	end

	// read port d
	reg	[3:0]	ard_d_xa_lch ;
	reg [7:0]   ard_d_xb_lch ;
	reg                     cenrd_lch_	;
    wire    cenrd_   = ~RF_RDREDO_ENi ;
	wire	rd_clk   = (TEST_SEi | ~lcd_rf_cenrd_) & lcd_clkr;
	wire	array_cenrd = ~cenrd_lch_ & lcd_dly_clkr;
	always @(posedge rd_clk)
	begin
		if (TEST_SEi)
		  begin
			ard_d_xa_lch <= #U_DLY {arc_d_xb_lch[0], ard_d_xa_lch[3:1]};
		    ard_d_xb_lch <= #U_DLY {ard_d_xa_lch[0], ard_d_xb_lch[7:1]};
		  end
		else
		  begin
			ard_d_xa_lch <= #U_DLY ard_d_xa ;
	        ard_d_xb_lch <= #U_DLY ard_d_xb ;
	      end
	end

    wire [31:0]  ard_dlch = {({8{ard_d_xa_lch[3]}} & ard_d_xb_lch),
                             ({8{ard_d_xa_lch[2]}} & ard_d_xb_lch),
                             ({8{ard_d_xa_lch[1]}} & ard_d_xb_lch),
                             ({8{ard_d_xa_lch[0]}} & ard_d_xb_lch) };

	always @(posedge lcd_clkr)
	begin
		if (TEST_SEi)
			cenrd_lch_ <= #U_DLY cenrc_lch_;
		else
			cenrd_lch_ <= #U_DLY cenrd_;
	end

  	// check port a

  	reg [3:0]   aca_d_xa_lch ;
  	reg [7:0]   aca_d_xb_lch ;
  	wire  cenca_  =  ~RF_CAREDO_ENi ;
  	wire  ca_clk      = (TEST_SEi | ~lcd_rf_cenca_) & lcd_clkr ;
  	wire  array_cenca = ~cenca_lch_ & lcd_dly_clkr;

  	always @(posedge ca_clk)
  	begin
  	  	if (TEST_SEi)
  	    	aca_lch <= #U_DLY {cencb_lch_, aca_lch[`RFA_WIDTH-1:1]};
  	  	else
  	    	aca_lch <= #U_DLY RF_ACA;
  	end

  	always @(posedge ca_clk)
  	begin
  	  	if (TEST_SEi)
  	      begin
  	    	aca_d_xa_lch <= #U_DLY {ard_d_xb_lch[0], aca_d_xa_lch[3:1]};
  	  	    aca_d_xb_lch <= #U_DLY {aca_d_xa_lch[0], aca_d_xb_lch[7:1]};
  	  	  end
  	  	else
  	  	  begin
  	    	aca_d_xa_lch <= #U_DLY aca_d_xa ;
  	        aca_d_xb_lch <= #U_DLY aca_d_xb ;
  	      end
  	end

    wire [31:0]  aca_dlch = {({8{aca_d_xa_lch[3]}} & aca_d_xb_lch),
                             ({8{aca_d_xa_lch[2]}} & aca_d_xb_lch),
                             ({8{aca_d_xa_lch[1]}} & aca_d_xb_lch),
                             ({8{aca_d_xa_lch[0]}} & aca_d_xb_lch) };

  	always @(posedge lcd_clkr )
  	begin
  	  	if (TEST_SEi)
  	    	cenca_lch_ <= #U_DLY cenrd_lch_;
  	  	else
  	    	cenca_lch_ <= #U_DLY cenca_;
  	end

  	// check port b
  	reg [3:0]   acb_d_xa_lch ;
  	reg [7:0]   acb_d_xb_lch ;
  	wire  cencb_   = ~RF_CBREDO_ENi ;
  	wire  cb_clk   = (TEST_SEi | ~lcd_rf_cencb_) & lcd_clkr ;
  	wire  array_cencb = ~cencb_lch_ & lcd_dly_clkr;

  	always @(posedge cb_clk)
  	begin
  	  	if (TEST_SEi)
  	    	acb_lch <= #U_DLY {aca_lch[0],acb_lch[`RFA_WIDTH-1:1]};
  	  	else
  	    	acb_lch <= #U_DLY RF_ACB;
  	end

  	always @(posedge cb_clk)
  	begin
  	  	if (TEST_SEi)
  	  	  begin
  	  	    acb_d_xa_lch <= #U_DLY {aca_d_xb_lch[0], acb_d_xa_lch[3:1]};
  	    	acb_d_xb_lch <= #U_DLY {acb_d_xa_lch[0], acb_d_xb_lch[7:1]};
  	  	  end
  	  	else
  	  	  begin
  	    	acb_d_xa_lch <= #U_DLY acb_d_xa;
  	        acb_d_xb_lch <= #U_DLY acb_d_xb;
  	      end
  	end

  	wire [31:0] acb_dlch = {({8{acb_d_xa_lch[3]}} & acb_d_xb_lch),
                            ({8{acb_d_xa_lch[2]}} & acb_d_xb_lch),
                            ({8{acb_d_xa_lch[1]}} & acb_d_xb_lch),
                            ({8{acb_d_xa_lch[0]}} & acb_d_xb_lch) };

  	always @(posedge lcd_clkr)
  	begin
  	  	if (TEST_SEi)
  	    	cencb_lch_ <= #U_DLY cenca_lch_;
  	  	else
  	    	cencb_lch_ <= #U_DLY cencb_;
  	end

  	// test port

    reg     rst_lch_        ;
  	reg 	cenwstcal_lch   ;
  	reg     cenwstcbl_lch   ;

  	reg 	diswaw_ca_lch   ;
  	reg 	diswaw_cb_lch   ;
    reg   	desalua_ca_lch  ;
    reg   	desalua_cb_lch  ;

    wire    cw_clk = (TEST_SEi | disp_ca_s_lch | disp_cb_s_lch) & lcd_clkw;
    wire #(2*U_DLY) cw_dly_clk = cw_clk;
  	wire    cenwstcal  = RD_ENCAi ;
    wire    cenwstcbl  = RD_ENCBi ;

    wire    array_cenwstcal = ~TEST_SEi & cenwstcal_lch & disp_ca_s_lch & cw_dly_clk ;
    wire    array_cenwstcbl = ~TEST_SEi & cenwstcbl_lch & disp_cb_s_lch & cw_dly_clk ;
    wire    array_cenwstcah = ~TEST_SEi & cenwstcal_lch & disp_ca_s_lch & cw_dly_clk ;
    wire    array_cenwstcbh = ~TEST_SEi & cenwstcbl_lch & disp_cb_s_lch & cw_dly_clk ;
    wire    array_censtwa   = ~TEST_SEi & ~lcd_rf_censtwa_ & wa_dly_clk ;
    wire    array_censtwb   = ~TEST_SEi & ~lcd_rf_censtwb_ & wb_dly_clk ;
    wire    array_censtwc   = ~TEST_SEi & ~lcd_rf_censtwc_ & wc_dly_clk ;
    wire    array_censtwd   = ~TEST_SEi & ~lcd_rf_censtwd_ & wd_dly_clk ;

    wire    array_dwstca_2   = diswaw_ca_lch  ;
    wire    array_dwstca_3   = desalua_ca_lch ;
    wire    array_dwstcb_2   = diswaw_cb_lch  ;
    wire    array_dwstcb_3   = desalua_cb_lch ;
  	wire  	st_rst 		     = ~TEST_SEi & ~rst_lch_  ;

   	always @(posedge cw_clk)
  	begin
  	    cenwstcal_lch <= #U_DLY cenwstcal ;
  	end


   	always @(posedge cw_clk)
  	begin
  	    cenwstcbl_lch <= #U_DLY cenwstcbl ;
  	end



  	always @(posedge cw_clk)
  	begin
  	    diswaw_ca_lch <= #U_DLY DISWAW_CAi;
  	end

  	always @(posedge cw_clk)
  	begin
  	    diswaw_cb_lch <= #U_DLY DISWAW_CBi;
  	end

    always @(posedge cw_clk)
    begin
        desalua_ca_lch <= #U_DLY DESALUA_CAi;
    end

    always @(posedge cw_clk)
    begin
        desalua_cb_lch <= #U_DLY DESALUA_CBi;
    end

    always @(posedge lcd_clkw)
  	begin
  	    rst_lch_ <= #U_DLY RSTi_;
  	end

  	RF_ARRAY	rf_array(
				// output ports
				.array_qra		  (RF_QRAi		  ),
				.array_qrb		  (RF_QRBi		  ),
				.array_qrc		  (RF_QRCi		  ),
				.array_qrd		  (RF_QRDi		  ),
        		.array_stra       (RF_STRAi    	  ),
        		.array_strb       (RF_STRBi    	  ),
        		.array_strc       (RF_STRCi    	  ),
        		.array_strd       (RF_STRDi    	  ),
        		.array_stca       (RF_STCAi    	  ),
        		.array_stcb       (RF_STCBi    	  ),
				.test_so0		  (TEST_SO0i		  ),
				.test_so1         (TEST_SO1i       ),
				// input ports
				.array_ara		  (ara_dlch		  ),
				.array_cenra	  (array_cenra	  ),
				.array_arb		  (arb_dlch		  ),
				.array_cenrb	  (array_cenrb	  ),
				.array_arc 		  (arc_dlch		  ),
				.array_cenrc	  (array_cenrc	  ),
				.array_ard		  (ard_dlch		  ),
				.array_cenrd	  (array_cenrd	  ),
	            // write ports
				.array_awa		  (awa_dlch		  ),
				.array_cenwa	  (array_cenwa	  ),
				.array_dwa		  (dwa_lch		  ),
				.array_awb		  (awb_dlch		  ),
				.array_cenwb	  (array_cenwb	  ),
				.array_dwb		  (dwb_lch		  ),
				.array_awc		  (awc_dlch		  ),
				.array_cenwc	  (array_cenwc	  ),
				.array_dwc		  (dwc_lch		  ),
				.array_awd		  (awd_dlch		  ),
				.array_cenwd	  (array_cenwd	  ),
				.array_dwd		  (dwd_lch		  ),

				.array_censtwcal  (array_cenwstcal ),
				.array_censtwcbl  (array_cenwstcbl ),
				.array_censtwcah  (array_cenwstcah ),
				.array_censtwcbh  (array_cenwstcbh ),

				.array_censtwa    (array_censtwa   ),
				.array_censtwb    (array_censtwb   ),
				.array_censtwc    (array_censtwc   ),
				.array_censtwd    (array_censtwd   ),

				.array_dwstca_2   (array_dwstca_2   ),
				.array_dwstcb_2   (array_dwstcb_2   ),
				.array_dwstca_3   (array_dwstca_3   ),
				.array_dwstcb_3   (array_dwstcb_3   ),

        		.array_cenca  	  (array_cenca	    ),
        		.array_aca    	  (aca_dlch 		),
        		.array_cencb  	  (array_cencb	    ),
        		.array_acb    	  (acb_dlch 		),

				.scan_clk		  (1'b0		        ),
        		.st_rst     	  (st_rst   		),
				.test_si1		  (acb_d_xb_lch[0]  ),
				.test_si0         (TEST_SI0i         )
				);
///////////////////////////////////////////////////////////////////////
endmodule
`endcelldefine
