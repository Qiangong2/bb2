`timescale 1ns/10ps
`celldefine
module GATX20( CLK, EN, SE, YGCLK );
 input CLK, EN, SE;
output YGCLK;
wire N_1;
wire N_2;
wire N_4;
reg NOTIFIER;
supply1 R, S;

udp_tlat I0 (N_4, N_1, CLK, R, S, NOTIFIER);
not      I2 (YGCLK, N_2);
nand     I3 (N_2, N_4, CLK);
or       I4 (N_1, SE, EN);

 specify
  specparam FanoutLoad$SE=0.003023;
  specparam FanoutLoad$VD25=0.117268;
  specparam FanoutLoad$YGCLK=0.0899834;
  specparam FanoutLoad$CLK=0.0320678;
  specparam FanoutLoad$EN=0.00310441;
  specparam FanoutLoad$VSS=0.0956355;
    specparam
  //timing parameters
      tplh$CLK$YGCLK  = 1.0,
      tphl$CLK$YGCLK  = 1.0,

      tminpwl$GN  = 1.0;

  // path delays
    ( CLK *> YGCLK ) = (tplh$CLK$YGCLK, tphl$CLK$YGCLK );
  // error checking time

    $setup(posedge EN,posedge CLK,(0.0:0.0:0.0), NOTIFIER);
    $setup(negedge EN,posedge CLK,(0.0:0.0:0.0), NOTIFIER);
    $setup(posedge SE,posedge CLK,(0.0:0.0:0.0), NOTIFIER);
    $setup(negedge SE,posedge CLK,(0.0:0.0:0.0), NOTIFIER);
    $hold(posedge CLK,posedge EN,0.0, NOTIFIER);
    $hold(posedge CLK,negedge EN,0.0, NOTIFIER);
    $hold(posedge CLK,posedge SE,0.0, NOTIFIER);
    $hold(posedge CLK,negedge SE,0.0, NOTIFIER);
    $width(negedge CLK,0.0, 0, NOTIFIER);

 endspecify

endmodule // gatx20
`endcelldefine
