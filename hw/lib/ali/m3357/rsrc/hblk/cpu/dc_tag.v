/*******************************************************************************
             Project Name :         T2Risc1H
             File Name :            dc_tag.v
             Create Date :          2001/06/06
             Author :               Charliema
             Description :          behavior module for d_cache tag
             Revision History :
                 06/06/2001   charlie       initial creation
*******************************************************************************/

//`include  "defines.h"
`celldefine

module DTAG6304  (
    // Output ports
    DC_SRAM_TAG_OUT,
    DC_SRAM_LRU_OUT,
    DC_SRAM_TAG0_ST_OUT,
    DC_SRAM_TAG1_ST_OUT,
    DC_SRAM_TAG2_ST_OUT,
    DC_SRAM_TAG3_ST_OUT,
    DC_SRAM_HIT,
    HIT_VEC_FOR_SEL,
    DC_TAG0_OUT,          // the five ports is before output latch, for 6303.
    DC_TAG1_OUT,
    DC_TAG2_OUT,
    DC_TAG3_OUT,
    LRU_OUT_BEFORE_LCH,
    HIT_VEC_OUT,

    // Input ports
    DC_SRAM_TAG_IN,
    DC_SRAM_LRU_IN,
    DC_SRAM_ADDR_IN,
    DC_SRAM_TAG_CS_,
    DC_TAG_WR_EN_,
    DC_TAG_PADDR,
    DC_SRAM_INVLD_,
    DC_SRAM_INDEXOP_,
    DC_SRAM_WAY_SEL,
    FORWARD_INFO,
    DC_SRAM_BYPASS,
    DC_OUTPUT_ENABLE,
    DC_WAY_OPTION,
    DC_WAY_ADDR,
    BIST_HOLD,
    BIST_DIAG,
    SCAN_MODE,
    SE,
    SO,
    SI,
    CLK
               );

    output [`dct_size-1:0]   DC_SRAM_TAG_OUT;
    output [3:0]             DC_SRAM_LRU_OUT;
    output [6:0]             DC_SRAM_TAG0_ST_OUT;
    output [6:0]             DC_SRAM_TAG1_ST_OUT;
    output [6:0]             DC_SRAM_TAG2_ST_OUT;
    output [6:0]             DC_SRAM_TAG3_ST_OUT;
    output [6:0]             DC_SRAM_HIT;
    output [3:0]             HIT_VEC_FOR_SEL;
    output [`dct_size-1:0]   DC_TAG0_OUT;
    output [`dct_size-1:0]   DC_TAG1_OUT;
    output [`dct_size-1:0]   DC_TAG2_OUT;
    output [`dct_size-1:0]   DC_TAG3_OUT;
    output [1:0]             LRU_OUT_BEFORE_LCH;
    output [3:0]             HIT_VEC_OUT;

    // Input ports
    input [`dct_size-1:0]    DC_SRAM_TAG_IN;
    input [`lru_wide-1:0]    DC_SRAM_LRU_IN;
    input [`addr_width-6:0]  DC_SRAM_ADDR_IN;
    input                    DC_SRAM_TAG_CS_;
    input                    DC_TAG_WR_EN_;
    input [`dct_size-8:0]    DC_TAG_PADDR;
    input                    DC_SRAM_INVLD_;
    input                    DC_SRAM_INDEXOP_;
    input [1:0]              DC_SRAM_WAY_SEL;
    input [8:0]              FORWARD_INFO;
    input                    DC_SRAM_BYPASS;
    input                    DC_OUTPUT_ENABLE;
    input                    DC_WAY_OPTION;
    input                    DC_WAY_ADDR;
    input                    BIST_HOLD;
    input                    BIST_DIAG;
    input                    SE;
    input                    SI;
    output                   SO;
    input                    SCAN_MODE;
    input                    CLK;

    wire [`lru_wide-1:0]    DC_LRU_OUT;
    wire [1:0]              DC_TAG_WR_;
    wire                    LRU_WR_EN_;
    wire [3:0]              DC_TAG_CS_;
    wire                    SRAM_TAG_CS_;
    wire                    SRAM_INVLD_;
    wire [3:0]              FORWARD_TAG_SEL;
    wire                    CLK_LATCH;

/***********************************
  declare input/output wire variable
***********************************/
    wire [`dct_size-1:0]   DC_SRAM_TAG_OUT;
    wire [3:0]             DC_SRAM_LRU_OUT;
    wire [6:0]             DC_SRAM_TAG0_ST_OUT;
    wire [6:0]             DC_SRAM_TAG1_ST_OUT;
    wire [6:0]             DC_SRAM_TAG2_ST_OUT;
    wire [6:0]             DC_SRAM_TAG3_ST_OUT;
    wire [6:0]             DC_SRAM_HIT;
    wire [3:0]             HIT_VEC_FOR_SEL;
    wire [`dct_size-1:0]   DC_TAG0_OUT;
    wire [`dct_size-1:0]   DC_TAG1_OUT;
    wire [`dct_size-1:0]   DC_TAG2_OUT;
    wire [`dct_size-1:0]   DC_TAG3_OUT;
    wire [1:0]             LRU_OUT_BEFORE_LCH;
    wire [3:0]             HIT_VEC_OUT;
    wire                   SO;

    wire [`dct_size-1:0]    DC_SRAM_TAG_IN;
    wire [`lru_wide-1:0]    DC_SRAM_LRU_IN;
    wire [`addr_width-6:0]  DC_SRAM_ADDR_IN;
    wire                    DC_SRAM_TAG_CS_;
    wire                    DC_TAG_WR_EN_;
    wire [`dct_size-8:0]    DC_TAG_PADDR;
    wire                    DC_SRAM_INVLD_;
    wire                    DC_SRAM_INDEXOP_;
    wire [1:0]              DC_SRAM_WAY_SEL;
    wire [8:0]              FORWARD_INFO;
    wire                    DC_SRAM_BYPASS;
    wire                    DC_OUTPUT_ENABLE;
    wire                    DC_WAY_OPTION;
    wire                    DC_WAY_ADDR;
    wire                    BIST_HOLD;
    wire                    BIST_DIAG;
    wire                    SE;
    wire                    SI;
    wire                    SCAN_MODE;
    wire                    CLK;

// internal output wire(i)
    wire [`dct_size-1:0]   DC_SRAM_TAG_OUTi;
    wire [3:0]             DC_SRAM_LRU_OUTi;
    wire [6:0]             DC_SRAM_TAG0_ST_OUTi;
    wire [6:0]             DC_SRAM_TAG1_ST_OUTi;
    wire [6:0]             DC_SRAM_TAG2_ST_OUTi;
    wire [6:0]             DC_SRAM_TAG3_ST_OUTi;
    wire [6:0]             DC_SRAM_HITi;
    wire [3:0]             HIT_VEC_FOR_SELi;
    wire [`dct_size-1:0]   DC_TAG0_OUTi;
    wire [`dct_size-1:0]   DC_TAG1_OUTi;
    wire [`dct_size-1:0]   DC_TAG2_OUTi;
    wire [`dct_size-1:0]   DC_TAG3_OUTi;
    wire [1:0]             LRU_OUT_BEFORE_LCHi;
    wire [3:0]             HIT_VEC_OUTi;
    wire                   SOi;

// internal input wire(i)
    wire [`dct_size-1:0]    DC_SRAM_TAG_INi;
    wire [`lru_wide-1:0]    DC_SRAM_LRU_INi;
    wire [`addr_width-6:0]  DC_SRAM_ADDR_INi;
    wire                    DC_SRAM_TAG_CSi_;
    wire                    DC_TAG_WR_ENi_;
    wire [`dct_size-8:0]    DC_TAG_PADDRi;
    wire                    DC_SRAM_INVLDi_;
    wire                    DC_SRAM_INDEXOPi_;
    wire [1:0]              DC_SRAM_WAY_SELi;
    wire [8:0]              FORWARD_INFOi;
    wire                    DC_SRAM_BYPASSi;
    wire                    DC_OUTPUT_ENABLEi;
    wire                    DC_WAY_OPTIONi;
    wire                    DC_WAY_ADDRi;
    wire                    BIST_HOLDi;
    wire                    BIST_DIAGi;
    wire                    SEi;
    wire                    SIi;
    wire                    SCAN_MODEi;
    wire                    CLKi;

// buf declare
	  buf(DC_SRAM_TAG_OUT[26],DC_SRAM_TAG_OUTi[26]);
	  buf(DC_SRAM_TAG_OUT[25],DC_SRAM_TAG_OUTi[25]);
	  buf(DC_SRAM_TAG_OUT[24],DC_SRAM_TAG_OUTi[24]);
	  buf(DC_SRAM_TAG_OUT[23],DC_SRAM_TAG_OUTi[23]);
	  buf(DC_SRAM_TAG_OUT[22],DC_SRAM_TAG_OUTi[22]);
	  buf(DC_SRAM_TAG_OUT[21],DC_SRAM_TAG_OUTi[21]);
	  buf(DC_SRAM_TAG_OUT[20],DC_SRAM_TAG_OUTi[20]);
	  buf(DC_SRAM_TAG_OUT[19],DC_SRAM_TAG_OUTi[19]);
	  buf(DC_SRAM_TAG_OUT[18],DC_SRAM_TAG_OUTi[18]);
	  buf(DC_SRAM_TAG_OUT[17],DC_SRAM_TAG_OUTi[17]);
	  buf(DC_SRAM_TAG_OUT[16],DC_SRAM_TAG_OUTi[16]);
	  buf(DC_SRAM_TAG_OUT[15],DC_SRAM_TAG_OUTi[15]);
	  buf(DC_SRAM_TAG_OUT[14],DC_SRAM_TAG_OUTi[14]);
	  buf(DC_SRAM_TAG_OUT[13],DC_SRAM_TAG_OUTi[13]);
	  buf(DC_SRAM_TAG_OUT[12],DC_SRAM_TAG_OUTi[12]);
	  buf(DC_SRAM_TAG_OUT[11],DC_SRAM_TAG_OUTi[11]);
	  buf(DC_SRAM_TAG_OUT[10],DC_SRAM_TAG_OUTi[10]);
	  buf(DC_SRAM_TAG_OUT[9 ],DC_SRAM_TAG_OUTi[9 ]);
	  buf(DC_SRAM_TAG_OUT[8 ],DC_SRAM_TAG_OUTi[8 ]);
	  buf(DC_SRAM_TAG_OUT[7 ],DC_SRAM_TAG_OUTi[7 ]);
	  buf(DC_SRAM_TAG_OUT[6 ],DC_SRAM_TAG_OUTi[6 ]);
	  buf(DC_SRAM_TAG_OUT[5 ],DC_SRAM_TAG_OUTi[5 ]);
	  buf(DC_SRAM_TAG_OUT[4 ],DC_SRAM_TAG_OUTi[4 ]);
	  buf(DC_SRAM_TAG_OUT[3 ],DC_SRAM_TAG_OUTi[3 ]);
	  buf(DC_SRAM_TAG_OUT[2 ],DC_SRAM_TAG_OUTi[2 ]);
	  buf(DC_SRAM_TAG_OUT[1 ],DC_SRAM_TAG_OUTi[1 ]);
	  buf(DC_SRAM_TAG_OUT[0 ],DC_SRAM_TAG_OUTi[0 ]);

	  buf(DC_SRAM_LRU_OUT[3],DC_SRAM_LRU_OUTi[3]);
	  buf(DC_SRAM_LRU_OUT[2],DC_SRAM_LRU_OUTi[2]);
	  buf(DC_SRAM_LRU_OUT[1],DC_SRAM_LRU_OUTi[1]);
	  buf(DC_SRAM_LRU_OUT[0],DC_SRAM_LRU_OUTi[0]);

	  buf(DC_SRAM_TAG0_ST_OUT[6],DC_SRAM_TAG0_ST_OUTi[6]);
	  buf(DC_SRAM_TAG0_ST_OUT[5],DC_SRAM_TAG0_ST_OUTi[5]);
	  buf(DC_SRAM_TAG0_ST_OUT[4],DC_SRAM_TAG0_ST_OUTi[4]);
	  buf(DC_SRAM_TAG0_ST_OUT[3],DC_SRAM_TAG0_ST_OUTi[3]);
	  buf(DC_SRAM_TAG0_ST_OUT[2],DC_SRAM_TAG0_ST_OUTi[2]);
	  buf(DC_SRAM_TAG0_ST_OUT[1],DC_SRAM_TAG0_ST_OUTi[1]);
	  buf(DC_SRAM_TAG0_ST_OUT[0],DC_SRAM_TAG0_ST_OUTi[0]);
	  buf(DC_SRAM_TAG1_ST_OUT[6],DC_SRAM_TAG1_ST_OUTi[6]);
	  buf(DC_SRAM_TAG1_ST_OUT[5],DC_SRAM_TAG1_ST_OUTi[5]);
	  buf(DC_SRAM_TAG1_ST_OUT[4],DC_SRAM_TAG1_ST_OUTi[4]);
	  buf(DC_SRAM_TAG1_ST_OUT[3],DC_SRAM_TAG1_ST_OUTi[3]);
	  buf(DC_SRAM_TAG1_ST_OUT[2],DC_SRAM_TAG1_ST_OUTi[2]);
	  buf(DC_SRAM_TAG1_ST_OUT[1],DC_SRAM_TAG1_ST_OUTi[1]);
	  buf(DC_SRAM_TAG1_ST_OUT[0],DC_SRAM_TAG1_ST_OUTi[0]);
	  buf(DC_SRAM_TAG2_ST_OUT[6],DC_SRAM_TAG2_ST_OUTi[6]);
	  buf(DC_SRAM_TAG2_ST_OUT[5],DC_SRAM_TAG2_ST_OUTi[5]);
	  buf(DC_SRAM_TAG2_ST_OUT[4],DC_SRAM_TAG2_ST_OUTi[4]);
	  buf(DC_SRAM_TAG2_ST_OUT[3],DC_SRAM_TAG2_ST_OUTi[3]);
	  buf(DC_SRAM_TAG2_ST_OUT[2],DC_SRAM_TAG2_ST_OUTi[2]);
	  buf(DC_SRAM_TAG2_ST_OUT[1],DC_SRAM_TAG2_ST_OUTi[1]);
	  buf(DC_SRAM_TAG2_ST_OUT[0],DC_SRAM_TAG2_ST_OUTi[0]);
	  buf(DC_SRAM_TAG3_ST_OUT[6],DC_SRAM_TAG3_ST_OUTi[6]);
	  buf(DC_SRAM_TAG3_ST_OUT[5],DC_SRAM_TAG3_ST_OUTi[5]);
	  buf(DC_SRAM_TAG3_ST_OUT[4],DC_SRAM_TAG3_ST_OUTi[4]);
	  buf(DC_SRAM_TAG3_ST_OUT[3],DC_SRAM_TAG3_ST_OUTi[3]);
	  buf(DC_SRAM_TAG3_ST_OUT[2],DC_SRAM_TAG3_ST_OUTi[2]);
	  buf(DC_SRAM_TAG3_ST_OUT[1],DC_SRAM_TAG3_ST_OUTi[1]);
	  buf(DC_SRAM_TAG3_ST_OUT[0],DC_SRAM_TAG3_ST_OUTi[0]);

	  buf(DC_SRAM_HIT[6],DC_SRAM_HITi[6]);
	  buf(DC_SRAM_HIT[5],DC_SRAM_HITi[5]);
	  buf(DC_SRAM_HIT[4],DC_SRAM_HITi[4]);
	  buf(DC_SRAM_HIT[3],DC_SRAM_HITi[3]);
	  buf(DC_SRAM_HIT[2],DC_SRAM_HITi[2]);
	  buf(DC_SRAM_HIT[1],DC_SRAM_HITi[1]);
	  buf(DC_SRAM_HIT[0],DC_SRAM_HITi[0]);

	  buf(HIT_VEC_FOR_SEL[3],HIT_VEC_FOR_SELi[3]);
	  buf(HIT_VEC_FOR_SEL[2],HIT_VEC_FOR_SELi[2]);
	  buf(HIT_VEC_FOR_SEL[1],HIT_VEC_FOR_SELi[1]);
	  buf(HIT_VEC_FOR_SEL[0],HIT_VEC_FOR_SELi[0]);

	  buf(DC_TAG3_OUT[26],DC_TAG3_OUTi[26]);
	  buf(DC_TAG3_OUT[25],DC_TAG3_OUTi[25]);
	  buf(DC_TAG3_OUT[24],DC_TAG3_OUTi[24]);
	  buf(DC_TAG3_OUT[23],DC_TAG3_OUTi[23]);
	  buf(DC_TAG3_OUT[22],DC_TAG3_OUTi[22]);
	  buf(DC_TAG3_OUT[21],DC_TAG3_OUTi[21]);
	  buf(DC_TAG3_OUT[20],DC_TAG3_OUTi[20]);
	  buf(DC_TAG3_OUT[19],DC_TAG3_OUTi[19]);
	  buf(DC_TAG3_OUT[18],DC_TAG3_OUTi[18]);
	  buf(DC_TAG3_OUT[17],DC_TAG3_OUTi[17]);
	  buf(DC_TAG3_OUT[16],DC_TAG3_OUTi[16]);
	  buf(DC_TAG3_OUT[15],DC_TAG3_OUTi[15]);
	  buf(DC_TAG3_OUT[14],DC_TAG3_OUTi[14]);
	  buf(DC_TAG3_OUT[13],DC_TAG3_OUTi[13]);
	  buf(DC_TAG3_OUT[12],DC_TAG3_OUTi[12]);
	  buf(DC_TAG3_OUT[11],DC_TAG3_OUTi[11]);
	  buf(DC_TAG3_OUT[10],DC_TAG3_OUTi[10]);
	  buf(DC_TAG3_OUT[9 ],DC_TAG3_OUTi[9 ]);
	  buf(DC_TAG3_OUT[8 ],DC_TAG3_OUTi[8 ]);
	  buf(DC_TAG3_OUT[7 ],DC_TAG3_OUTi[7 ]);
	  buf(DC_TAG3_OUT[6 ],DC_TAG3_OUTi[6 ]);
	  buf(DC_TAG3_OUT[5 ],DC_TAG3_OUTi[5 ]);
	  buf(DC_TAG3_OUT[4 ],DC_TAG3_OUTi[4 ]);
	  buf(DC_TAG3_OUT[3 ],DC_TAG3_OUTi[3 ]);
	  buf(DC_TAG3_OUT[2 ],DC_TAG3_OUTi[2 ]);
	  buf(DC_TAG3_OUT[1 ],DC_TAG3_OUTi[1 ]);
	  buf(DC_TAG3_OUT[0 ],DC_TAG3_OUTi[0 ]);
	  buf(DC_TAG2_OUT[26],DC_TAG2_OUTi[26]);
	  buf(DC_TAG2_OUT[25],DC_TAG2_OUTi[25]);
	  buf(DC_TAG2_OUT[24],DC_TAG2_OUTi[24]);
	  buf(DC_TAG2_OUT[23],DC_TAG2_OUTi[23]);
	  buf(DC_TAG2_OUT[22],DC_TAG2_OUTi[22]);
	  buf(DC_TAG2_OUT[21],DC_TAG2_OUTi[21]);
	  buf(DC_TAG2_OUT[20],DC_TAG2_OUTi[20]);
	  buf(DC_TAG2_OUT[19],DC_TAG2_OUTi[19]);
	  buf(DC_TAG2_OUT[18],DC_TAG2_OUTi[18]);
	  buf(DC_TAG2_OUT[17],DC_TAG2_OUTi[17]);
	  buf(DC_TAG2_OUT[16],DC_TAG2_OUTi[16]);
	  buf(DC_TAG2_OUT[15],DC_TAG2_OUTi[15]);
	  buf(DC_TAG2_OUT[14],DC_TAG2_OUTi[14]);
	  buf(DC_TAG2_OUT[13],DC_TAG2_OUTi[13]);
	  buf(DC_TAG2_OUT[12],DC_TAG2_OUTi[12]);
	  buf(DC_TAG2_OUT[11],DC_TAG2_OUTi[11]);
	  buf(DC_TAG2_OUT[10],DC_TAG2_OUTi[10]);
	  buf(DC_TAG2_OUT[9 ],DC_TAG2_OUTi[9 ]);
	  buf(DC_TAG2_OUT[8 ],DC_TAG2_OUTi[8 ]);
	  buf(DC_TAG2_OUT[7 ],DC_TAG2_OUTi[7 ]);
	  buf(DC_TAG2_OUT[6 ],DC_TAG2_OUTi[6 ]);
	  buf(DC_TAG2_OUT[5 ],DC_TAG2_OUTi[5 ]);
	  buf(DC_TAG2_OUT[4 ],DC_TAG2_OUTi[4 ]);
	  buf(DC_TAG2_OUT[3 ],DC_TAG2_OUTi[3 ]);
	  buf(DC_TAG2_OUT[2 ],DC_TAG2_OUTi[2 ]);
	  buf(DC_TAG2_OUT[1 ],DC_TAG2_OUTi[1 ]);
	  buf(DC_TAG2_OUT[0 ],DC_TAG2_OUTi[0 ]);
	  buf(DC_TAG1_OUT[26],DC_TAG1_OUTi[26]);
	  buf(DC_TAG1_OUT[25],DC_TAG1_OUTi[25]);
	  buf(DC_TAG1_OUT[24],DC_TAG1_OUTi[24]);
	  buf(DC_TAG1_OUT[23],DC_TAG1_OUTi[23]);
	  buf(DC_TAG1_OUT[22],DC_TAG1_OUTi[22]);
	  buf(DC_TAG1_OUT[21],DC_TAG1_OUTi[21]);
	  buf(DC_TAG1_OUT[20],DC_TAG1_OUTi[20]);
	  buf(DC_TAG1_OUT[19],DC_TAG1_OUTi[19]);
	  buf(DC_TAG1_OUT[18],DC_TAG1_OUTi[18]);
	  buf(DC_TAG1_OUT[17],DC_TAG1_OUTi[17]);
	  buf(DC_TAG1_OUT[16],DC_TAG1_OUTi[16]);
	  buf(DC_TAG1_OUT[15],DC_TAG1_OUTi[15]);
	  buf(DC_TAG1_OUT[14],DC_TAG1_OUTi[14]);
	  buf(DC_TAG1_OUT[13],DC_TAG1_OUTi[13]);
	  buf(DC_TAG1_OUT[12],DC_TAG1_OUTi[12]);
	  buf(DC_TAG1_OUT[11],DC_TAG1_OUTi[11]);
	  buf(DC_TAG1_OUT[10],DC_TAG1_OUTi[10]);
	  buf(DC_TAG1_OUT[9 ],DC_TAG1_OUTi[9 ]);
	  buf(DC_TAG1_OUT[8 ],DC_TAG1_OUTi[8 ]);
	  buf(DC_TAG1_OUT[7 ],DC_TAG1_OUTi[7 ]);
	  buf(DC_TAG1_OUT[6 ],DC_TAG1_OUTi[6 ]);
	  buf(DC_TAG1_OUT[5 ],DC_TAG1_OUTi[5 ]);
	  buf(DC_TAG1_OUT[4 ],DC_TAG1_OUTi[4 ]);
	  buf(DC_TAG1_OUT[3 ],DC_TAG1_OUTi[3 ]);
	  buf(DC_TAG1_OUT[2 ],DC_TAG1_OUTi[2 ]);
	  buf(DC_TAG1_OUT[1 ],DC_TAG1_OUTi[1 ]);
	  buf(DC_TAG1_OUT[0 ],DC_TAG1_OUTi[0 ]);
	  buf(DC_TAG0_OUT[26],DC_TAG0_OUTi[26]);
	  buf(DC_TAG0_OUT[25],DC_TAG0_OUTi[25]);
	  buf(DC_TAG0_OUT[24],DC_TAG0_OUTi[24]);
	  buf(DC_TAG0_OUT[23],DC_TAG0_OUTi[23]);
	  buf(DC_TAG0_OUT[22],DC_TAG0_OUTi[22]);
	  buf(DC_TAG0_OUT[21],DC_TAG0_OUTi[21]);
	  buf(DC_TAG0_OUT[20],DC_TAG0_OUTi[20]);
	  buf(DC_TAG0_OUT[19],DC_TAG0_OUTi[19]);
	  buf(DC_TAG0_OUT[18],DC_TAG0_OUTi[18]);
	  buf(DC_TAG0_OUT[17],DC_TAG0_OUTi[17]);
	  buf(DC_TAG0_OUT[16],DC_TAG0_OUTi[16]);
	  buf(DC_TAG0_OUT[15],DC_TAG0_OUTi[15]);
	  buf(DC_TAG0_OUT[14],DC_TAG0_OUTi[14]);
	  buf(DC_TAG0_OUT[13],DC_TAG0_OUTi[13]);
	  buf(DC_TAG0_OUT[12],DC_TAG0_OUTi[12]);
	  buf(DC_TAG0_OUT[11],DC_TAG0_OUTi[11]);
	  buf(DC_TAG0_OUT[10],DC_TAG0_OUTi[10]);
	  buf(DC_TAG0_OUT[9 ],DC_TAG0_OUTi[9 ]);
	  buf(DC_TAG0_OUT[8 ],DC_TAG0_OUTi[8 ]);
	  buf(DC_TAG0_OUT[7 ],DC_TAG0_OUTi[7 ]);
	  buf(DC_TAG0_OUT[6 ],DC_TAG0_OUTi[6 ]);
	  buf(DC_TAG0_OUT[5 ],DC_TAG0_OUTi[5 ]);
	  buf(DC_TAG0_OUT[4 ],DC_TAG0_OUTi[4 ]);
	  buf(DC_TAG0_OUT[3 ],DC_TAG0_OUTi[3 ]);
	  buf(DC_TAG0_OUT[2 ],DC_TAG0_OUTi[2 ]);
	  buf(DC_TAG0_OUT[1 ],DC_TAG0_OUTi[1 ]);
    buf(DC_TAG0_OUT[0 ],DC_TAG0_OUTi[0 ]);

    buf(LRU_OUT_BEFORE_LCH[1],LRU_OUT_BEFORE_LCHi[1]);
    buf(LRU_OUT_BEFORE_LCH[0],LRU_OUT_BEFORE_LCHi[0]);
    buf(HIT_VEC_OUT[3],HIT_VEC_OUTi[3]);
    buf(HIT_VEC_OUT[2],HIT_VEC_OUTi[2]);
    buf(HIT_VEC_OUT[1],HIT_VEC_OUTi[1]);
    buf(HIT_VEC_OUT[0],HIT_VEC_OUTi[0]);
    buf(SO,SOi);

// internal input wire(i)
    buf(DC_SRAM_TAG_INi[26],DC_SRAM_TAG_IN[26]);
    buf(DC_SRAM_TAG_INi[25],DC_SRAM_TAG_IN[25]);
    buf(DC_SRAM_TAG_INi[24],DC_SRAM_TAG_IN[24]);
    buf(DC_SRAM_TAG_INi[23],DC_SRAM_TAG_IN[23]);
    buf(DC_SRAM_TAG_INi[22],DC_SRAM_TAG_IN[22]);
    buf(DC_SRAM_TAG_INi[21],DC_SRAM_TAG_IN[21]);
    buf(DC_SRAM_TAG_INi[20],DC_SRAM_TAG_IN[20]);
    buf(DC_SRAM_TAG_INi[19],DC_SRAM_TAG_IN[19]);
    buf(DC_SRAM_TAG_INi[18],DC_SRAM_TAG_IN[18]);
    buf(DC_SRAM_TAG_INi[17],DC_SRAM_TAG_IN[17]);
    buf(DC_SRAM_TAG_INi[16],DC_SRAM_TAG_IN[16]);
    buf(DC_SRAM_TAG_INi[15],DC_SRAM_TAG_IN[15]);
    buf(DC_SRAM_TAG_INi[14],DC_SRAM_TAG_IN[14]);
    buf(DC_SRAM_TAG_INi[13],DC_SRAM_TAG_IN[13]);
    buf(DC_SRAM_TAG_INi[12],DC_SRAM_TAG_IN[12]);
    buf(DC_SRAM_TAG_INi[11],DC_SRAM_TAG_IN[11]);
    buf(DC_SRAM_TAG_INi[10],DC_SRAM_TAG_IN[10]);
    buf(DC_SRAM_TAG_INi[9 ],DC_SRAM_TAG_IN[9 ]);
    buf(DC_SRAM_TAG_INi[8 ],DC_SRAM_TAG_IN[8 ]);
    buf(DC_SRAM_TAG_INi[7 ],DC_SRAM_TAG_IN[7 ]);
    buf(DC_SRAM_TAG_INi[6 ],DC_SRAM_TAG_IN[6 ]);
    buf(DC_SRAM_TAG_INi[5 ],DC_SRAM_TAG_IN[5 ]);
    buf(DC_SRAM_TAG_INi[4 ],DC_SRAM_TAG_IN[4 ]);
    buf(DC_SRAM_TAG_INi[3 ],DC_SRAM_TAG_IN[3 ]);
    buf(DC_SRAM_TAG_INi[2 ],DC_SRAM_TAG_IN[2 ]);
    buf(DC_SRAM_TAG_INi[1 ],DC_SRAM_TAG_IN[1 ]);
    buf(DC_SRAM_TAG_INi[0 ],DC_SRAM_TAG_IN[0 ]);

    buf(DC_SRAM_LRU_INi[1],DC_SRAM_LRU_IN[1]);
    buf(DC_SRAM_LRU_INi[0],DC_SRAM_LRU_IN[0]);

    buf(DC_SRAM_ADDR_INi[6],DC_SRAM_ADDR_IN[6]);
    buf(DC_SRAM_ADDR_INi[5],DC_SRAM_ADDR_IN[5]);
    buf(DC_SRAM_ADDR_INi[4],DC_SRAM_ADDR_IN[4]);
    buf(DC_SRAM_ADDR_INi[3],DC_SRAM_ADDR_IN[3]);
    buf(DC_SRAM_ADDR_INi[2],DC_SRAM_ADDR_IN[2]);
    buf(DC_SRAM_ADDR_INi[1],DC_SRAM_ADDR_IN[1]);
    buf(DC_SRAM_ADDR_INi[0],DC_SRAM_ADDR_IN[0]);

    buf(DC_SRAM_TAG_CSi_,DC_SRAM_TAG_CS_);
    buf(DC_TAG_WR_ENi_,DC_TAG_WR_EN_);
    buf(DC_TAG_PADDRi[19],DC_TAG_PADDR[19]);
    buf(DC_TAG_PADDRi[18],DC_TAG_PADDR[18]);
    buf(DC_TAG_PADDRi[17],DC_TAG_PADDR[17]);
    buf(DC_TAG_PADDRi[16],DC_TAG_PADDR[16]);
    buf(DC_TAG_PADDRi[15],DC_TAG_PADDR[15]);
    buf(DC_TAG_PADDRi[14],DC_TAG_PADDR[14]);
    buf(DC_TAG_PADDRi[13],DC_TAG_PADDR[13]);
    buf(DC_TAG_PADDRi[12],DC_TAG_PADDR[12]);
    buf(DC_TAG_PADDRi[11],DC_TAG_PADDR[11]);
    buf(DC_TAG_PADDRi[10],DC_TAG_PADDR[10]);
    buf(DC_TAG_PADDRi[9 ],DC_TAG_PADDR[9 ]);
    buf(DC_TAG_PADDRi[8 ],DC_TAG_PADDR[8 ]);
    buf(DC_TAG_PADDRi[7 ],DC_TAG_PADDR[7 ]);
    buf(DC_TAG_PADDRi[6 ],DC_TAG_PADDR[6 ]);
    buf(DC_TAG_PADDRi[5 ],DC_TAG_PADDR[5 ]);
    buf(DC_TAG_PADDRi[4 ],DC_TAG_PADDR[4 ]);
    buf(DC_TAG_PADDRi[3 ],DC_TAG_PADDR[3 ]);
    buf(DC_TAG_PADDRi[2 ],DC_TAG_PADDR[2 ]);
    buf(DC_TAG_PADDRi[1 ],DC_TAG_PADDR[1 ]);
    buf(DC_TAG_PADDRi[0 ],DC_TAG_PADDR[0 ]);

    buf(DC_SRAM_INVLDi_,DC_SRAM_INVLD_);
    buf(DC_SRAM_INDEXOPi_,DC_SRAM_INDEXOP_);
    buf(DC_SRAM_WAY_SELi[1],DC_SRAM_WAY_SEL[1]);
    buf(DC_SRAM_WAY_SELi[0],DC_SRAM_WAY_SEL[0]);

    buf(FORWARD_INFOi[8],FORWARD_INFO[8]);
    buf(FORWARD_INFOi[7],FORWARD_INFO[7]);
    buf(FORWARD_INFOi[6],FORWARD_INFO[6]);
    buf(FORWARD_INFOi[5],FORWARD_INFO[5]);
    buf(FORWARD_INFOi[4],FORWARD_INFO[4]);
    buf(FORWARD_INFOi[3],FORWARD_INFO[3]);
    buf(FORWARD_INFOi[2],FORWARD_INFO[2]);
    buf(FORWARD_INFOi[1],FORWARD_INFO[1]);
    buf(FORWARD_INFOi[0],FORWARD_INFO[0]);

    buf(DC_SRAM_BYPASSi,DC_SRAM_BYPASS);
    buf(DC_OUTPUT_ENABLEi,DC_OUTPUT_ENABLE);
    buf(DC_WAY_OPTIONi,DC_WAY_OPTION);
    buf(DC_WAY_ADDRi,DC_WAY_ADDR);
    buf(BIST_HOLDi,BIST_HOLD);
    buf(BIST_DIAGi,BIST_DIAG);
    buf(SEi,SE);
    buf(SIi,SI);
    buf(SCAN_MODEi,SCAN_MODE);
    buf(CLKi,CLK);

    /*===================================
      define notify violation register
     ====================================*/

    reg     NOT_TAG_IN00;
    reg     NOT_TAG_IN01;
    reg     NOT_TAG_IN02;
    reg     NOT_TAG_IN03;
    reg     NOT_TAG_IN04;
    reg     NOT_TAG_IN05;
    reg     NOT_TAG_IN06;
    reg     NOT_TAG_IN07;
    reg     NOT_TAG_IN08;
    reg     NOT_TAG_IN09;
    reg     NOT_TAG_IN10;
    reg     NOT_TAG_IN11;
    reg     NOT_TAG_IN12;
    reg     NOT_TAG_IN13;
    reg     NOT_TAG_IN14;
    reg     NOT_TAG_IN15;
    reg     NOT_TAG_IN16;
    reg     NOT_TAG_IN17;
    reg     NOT_TAG_IN18;
    reg     NOT_TAG_IN19;
    reg     NOT_TAG_IN20;
    reg     NOT_TAG_IN21;
    reg     NOT_TAG_IN22;
    reg     NOT_TAG_IN23;
    reg     NOT_TAG_IN24;
    reg     NOT_TAG_IN25;
    reg     NOT_TAG_IN26;
    reg     NOT_ADDR_IN00;
    reg     NOT_ADDR_IN01;
    reg     NOT_ADDR_IN02;
    reg     NOT_ADDR_IN03;
    reg     NOT_ADDR_IN04;
    reg     NOT_ADDR_IN05;
    reg     NOT_ADDR_IN06;
    reg     NOT_LRU_IN0;
    reg     NOT_LRU_IN1;

    reg     NOT_TAG_CS;
    reg     NOT_TAG_WR;
    reg     NOT_INVLD;
    reg     NOT_INDEXOP;
    reg     NOT_WAY_SEL0;
    reg     NOT_WAY_SEL1;
    reg     NOT_WAY_OPTION;
    reg     NOT_WAY_ADDR;

    reg    NOT_TAG_PADDR00;
    reg    NOT_TAG_PADDR01;
    reg    NOT_TAG_PADDR02;
    reg    NOT_TAG_PADDR03;
    reg    NOT_TAG_PADDR04;
    reg    NOT_TAG_PADDR05;
    reg    NOT_TAG_PADDR06;
    reg    NOT_TAG_PADDR07;
    reg    NOT_TAG_PADDR08;
    reg    NOT_TAG_PADDR09;
    reg    NOT_TAG_PADDR10;
    reg    NOT_TAG_PADDR11;
    reg    NOT_TAG_PADDR12;
    reg    NOT_TAG_PADDR13;
    reg    NOT_TAG_PADDR14;
    reg    NOT_TAG_PADDR15;
    reg    NOT_TAG_PADDR16;
    reg    NOT_TAG_PADDR17;
    reg    NOT_TAG_PADDR18;
    reg    NOT_TAG_PADDR19;
    reg    NOT_FRE_OPTION;
    reg    NOT_BIST_HOLD;
    reg    NOT_BIST_DIAG;
    reg    NOT_SE;
    reg    NOT_SI;
    reg    NOT_SCAN_MODE;
    reg    NOT_OUTPUT_ENABLE;
    reg    NOT_CLK_PER;
    reg    NOT_CLK_MINH;
    reg    NOT_CLK_MINL;
    reg    NOT_FORWARD_INFO0;
    reg    NOT_FORWARD_INFO1;
    reg    NOT_FORWARD_INFO2;
    reg    NOT_FORWARD_INFO3;
    reg    NOT_FORWARD_INFO4;
    reg    NOT_FORWARD_INFO5;
    reg    NOT_FORWARD_INFO6;
    reg    NOT_FORWARD_INFO7;
    reg    NOT_FORWARD_INFO8;
    reg    NOT_SRAM_BYPASS;

    wire    cs_flag = ~DC_SRAM_TAG_CSi_;

    /**************************************
                specify block
    **************************************/

  	specify
    /**************************************
            setup hold timing check
    **************************************/

    $setuphold(posedge CLK &&& cs_flag,posedge DC_SRAM_TAG_IN[00],1.000,0.500,NOT_TAG_IN00);
    $setuphold(posedge CLK &&& cs_flag,posedge DC_SRAM_TAG_IN[01],1.000,0.500,NOT_TAG_IN01);
    $setuphold(posedge CLK &&& cs_flag,posedge DC_SRAM_TAG_IN[02],1.000,0.500,NOT_TAG_IN02);
    $setuphold(posedge CLK &&& cs_flag,posedge DC_SRAM_TAG_IN[03],1.000,0.500,NOT_TAG_IN03);
    $setuphold(posedge CLK &&& cs_flag,posedge DC_SRAM_TAG_IN[04],1.000,0.500,NOT_TAG_IN04);
    $setuphold(posedge CLK &&& cs_flag,posedge DC_SRAM_TAG_IN[05],1.000,0.500,NOT_TAG_IN05);
    $setuphold(posedge CLK &&& cs_flag,posedge DC_SRAM_TAG_IN[06],1.000,0.500,NOT_TAG_IN06);
    $setuphold(posedge CLK &&& cs_flag,posedge DC_SRAM_TAG_IN[07],1.000,0.500,NOT_TAG_IN07);
    $setuphold(posedge CLK &&& cs_flag,posedge DC_SRAM_TAG_IN[08],1.000,0.500,NOT_TAG_IN08);
    $setuphold(posedge CLK &&& cs_flag,posedge DC_SRAM_TAG_IN[09],1.000,0.500,NOT_TAG_IN09);
    $setuphold(posedge CLK &&& cs_flag,posedge DC_SRAM_TAG_IN[10],1.000,0.500,NOT_TAG_IN10);
    $setuphold(posedge CLK &&& cs_flag,posedge DC_SRAM_TAG_IN[11],1.000,0.500,NOT_TAG_IN11);
    $setuphold(posedge CLK &&& cs_flag,posedge DC_SRAM_TAG_IN[12],1.000,0.500,NOT_TAG_IN12);
    $setuphold(posedge CLK &&& cs_flag,posedge DC_SRAM_TAG_IN[13],1.000,0.500,NOT_TAG_IN13);
    $setuphold(posedge CLK &&& cs_flag,posedge DC_SRAM_TAG_IN[14],1.000,0.500,NOT_TAG_IN14);
    $setuphold(posedge CLK &&& cs_flag,posedge DC_SRAM_TAG_IN[15],1.000,0.500,NOT_TAG_IN15);
    $setuphold(posedge CLK &&& cs_flag,posedge DC_SRAM_TAG_IN[16],1.000,0.500,NOT_TAG_IN16);
    $setuphold(posedge CLK &&& cs_flag,posedge DC_SRAM_TAG_IN[17],1.000,0.500,NOT_TAG_IN17);
    $setuphold(posedge CLK &&& cs_flag,posedge DC_SRAM_TAG_IN[18],1.000,0.500,NOT_TAG_IN18);
    $setuphold(posedge CLK &&& cs_flag,posedge DC_SRAM_TAG_IN[19],1.000,0.500,NOT_TAG_IN19);
    $setuphold(posedge CLK &&& cs_flag,posedge DC_SRAM_TAG_IN[20],1.000,0.500,NOT_TAG_IN20);
    $setuphold(posedge CLK &&& cs_flag,posedge DC_SRAM_TAG_IN[21],1.000,0.500,NOT_TAG_IN21);
    $setuphold(posedge CLK &&& cs_flag,posedge DC_SRAM_TAG_IN[22],1.000,0.500,NOT_TAG_IN22);
    $setuphold(posedge CLK &&& cs_flag,posedge DC_SRAM_TAG_IN[23],1.000,0.500,NOT_TAG_IN23);
    $setuphold(posedge CLK &&& cs_flag,posedge DC_SRAM_TAG_IN[24],1.000,0.500,NOT_TAG_IN24);
    $setuphold(posedge CLK &&& cs_flag,posedge DC_SRAM_TAG_IN[25],1.000,0.500,NOT_TAG_IN25);
    $setuphold(posedge CLK &&& cs_flag,posedge DC_SRAM_TAG_IN[26],1.000,0.500,NOT_TAG_IN26);
    $setuphold(posedge CLK &&& cs_flag,posedge DC_SRAM_ADDR_IN[0],1.000,0.500,NOT_ADDR_IN00);
    $setuphold(posedge CLK &&& cs_flag,posedge DC_SRAM_ADDR_IN[1],1.000,0.500,NOT_ADDR_IN01);
    $setuphold(posedge CLK &&& cs_flag,posedge DC_SRAM_ADDR_IN[2],1.000,0.500,NOT_ADDR_IN02);
    $setuphold(posedge CLK &&& cs_flag,posedge DC_SRAM_ADDR_IN[3],1.000,0.500,NOT_ADDR_IN03);
    $setuphold(posedge CLK &&& cs_flag,posedge DC_SRAM_ADDR_IN[4],1.000,0.500,NOT_ADDR_IN04);
    $setuphold(posedge CLK &&& cs_flag,posedge DC_SRAM_ADDR_IN[5],1.000,0.500,NOT_ADDR_IN05);
    $setuphold(posedge CLK &&& cs_flag,posedge DC_SRAM_ADDR_IN[6],1.000,0.500,NOT_ADDR_IN06);

    $setuphold(posedge CLK &&& cs_flag,negedge DC_SRAM_TAG_IN[00],1.000,0.500,NOT_TAG_IN00);
    $setuphold(posedge CLK &&& cs_flag,negedge DC_SRAM_TAG_IN[01],1.000,0.500,NOT_TAG_IN01);
    $setuphold(posedge CLK &&& cs_flag,negedge DC_SRAM_TAG_IN[02],1.000,0.500,NOT_TAG_IN02);
    $setuphold(posedge CLK &&& cs_flag,negedge DC_SRAM_TAG_IN[03],1.000,0.500,NOT_TAG_IN03);
    $setuphold(posedge CLK &&& cs_flag,negedge DC_SRAM_TAG_IN[04],1.000,0.500,NOT_TAG_IN04);
    $setuphold(posedge CLK &&& cs_flag,negedge DC_SRAM_TAG_IN[05],1.000,0.500,NOT_TAG_IN05);
    $setuphold(posedge CLK &&& cs_flag,negedge DC_SRAM_TAG_IN[06],1.000,0.500,NOT_TAG_IN06);
    $setuphold(posedge CLK &&& cs_flag,negedge DC_SRAM_TAG_IN[07],1.000,0.500,NOT_TAG_IN07);
    $setuphold(posedge CLK &&& cs_flag,negedge DC_SRAM_TAG_IN[08],1.000,0.500,NOT_TAG_IN08);
    $setuphold(posedge CLK &&& cs_flag,negedge DC_SRAM_TAG_IN[09],1.000,0.500,NOT_TAG_IN09);
    $setuphold(posedge CLK &&& cs_flag,negedge DC_SRAM_TAG_IN[10],1.000,0.500,NOT_TAG_IN10);
    $setuphold(posedge CLK &&& cs_flag,negedge DC_SRAM_TAG_IN[11],1.000,0.500,NOT_TAG_IN11);
    $setuphold(posedge CLK &&& cs_flag,negedge DC_SRAM_TAG_IN[12],1.000,0.500,NOT_TAG_IN12);
    $setuphold(posedge CLK &&& cs_flag,negedge DC_SRAM_TAG_IN[13],1.000,0.500,NOT_TAG_IN13);
    $setuphold(posedge CLK &&& cs_flag,negedge DC_SRAM_TAG_IN[14],1.000,0.500,NOT_TAG_IN14);
    $setuphold(posedge CLK &&& cs_flag,negedge DC_SRAM_TAG_IN[15],1.000,0.500,NOT_TAG_IN15);
    $setuphold(posedge CLK &&& cs_flag,negedge DC_SRAM_TAG_IN[16],1.000,0.500,NOT_TAG_IN16);
    $setuphold(posedge CLK &&& cs_flag,negedge DC_SRAM_TAG_IN[17],1.000,0.500,NOT_TAG_IN17);
    $setuphold(posedge CLK &&& cs_flag,negedge DC_SRAM_TAG_IN[18],1.000,0.500,NOT_TAG_IN18);
    $setuphold(posedge CLK &&& cs_flag,negedge DC_SRAM_TAG_IN[19],1.000,0.500,NOT_TAG_IN19);
    $setuphold(posedge CLK &&& cs_flag,negedge DC_SRAM_TAG_IN[20],1.000,0.500,NOT_TAG_IN20);
    $setuphold(posedge CLK &&& cs_flag,negedge DC_SRAM_TAG_IN[21],1.000,0.500,NOT_TAG_IN21);
    $setuphold(posedge CLK &&& cs_flag,negedge DC_SRAM_TAG_IN[22],1.000,0.500,NOT_TAG_IN22);
    $setuphold(posedge CLK &&& cs_flag,negedge DC_SRAM_TAG_IN[23],1.000,0.500,NOT_TAG_IN23);
    $setuphold(posedge CLK &&& cs_flag,negedge DC_SRAM_TAG_IN[24],1.000,0.500,NOT_TAG_IN24);
    $setuphold(posedge CLK &&& cs_flag,negedge DC_SRAM_TAG_IN[25],1.000,0.500,NOT_TAG_IN25);
    $setuphold(posedge CLK &&& cs_flag,negedge DC_SRAM_TAG_IN[26],1.000,0.500,NOT_TAG_IN26);
    $setuphold(posedge CLK &&& cs_flag,negedge DC_SRAM_ADDR_IN[0],1.000,0.500,NOT_ADDR_IN00);
    $setuphold(posedge CLK &&& cs_flag,negedge DC_SRAM_ADDR_IN[1],1.000,0.500,NOT_ADDR_IN01);
    $setuphold(posedge CLK &&& cs_flag,negedge DC_SRAM_ADDR_IN[2],1.000,0.500,NOT_ADDR_IN02);
    $setuphold(posedge CLK &&& cs_flag,negedge DC_SRAM_ADDR_IN[3],1.000,0.500,NOT_ADDR_IN03);
    $setuphold(posedge CLK &&& cs_flag,negedge DC_SRAM_ADDR_IN[4],1.000,0.500,NOT_ADDR_IN04);
    $setuphold(posedge CLK &&& cs_flag,negedge DC_SRAM_ADDR_IN[5],1.000,0.500,NOT_ADDR_IN05);
    $setuphold(posedge CLK &&& cs_flag,negedge DC_SRAM_ADDR_IN[6],1.000,0.500,NOT_ADDR_IN06);

    $setuphold(posedge CLK &&& cs_flag,posedge DC_SRAM_LRU_IN[0],1.000,0.500,NOT_ADDR_IN00);
    $setuphold(posedge CLK &&& cs_flag,posedge DC_SRAM_LRU_IN[1],1.000,0.500,NOT_ADDR_IN01);
    $setuphold(posedge CLK &&& cs_flag,negedge DC_SRAM_LRU_IN[0],1.000,0.500,NOT_ADDR_IN00);
    $setuphold(posedge CLK &&& cs_flag,negedge DC_SRAM_LRU_IN[1],1.000,0.500,NOT_ADDR_IN01);

    $setuphold(posedge CLK &&& cs_flag,posedge DC_SRAM_INVLD_,1.000,0.500,NOT_INVLD);
    $setuphold(posedge CLK &&& cs_flag,negedge DC_SRAM_INVLD_,1.000,0.500,NOT_INVLD);
    $setuphold(posedge CLK &&& cs_flag,posedge DC_SRAM_INDEXOP_,1.000,0.500,NOT_INDEXOP);
    $setuphold(posedge CLK &&& cs_flag,negedge DC_SRAM_INDEXOP_,1.000,0.500,NOT_INDEXOP);
    $setuphold(posedge CLK &&& cs_flag,negedge DC_SRAM_WAY_SEL[0],1.000,0.500,NOT_WAY_SEL0);
    $setuphold(posedge CLK &&& cs_flag,negedge DC_SRAM_WAY_SEL[1],1.000,0.500,NOT_WAY_SEL1);
    $setuphold(posedge CLK &&& cs_flag,posedge DC_SRAM_WAY_SEL[0],1.000,0.500,NOT_WAY_SEL0);
    $setuphold(posedge CLK &&& cs_flag,posedge DC_SRAM_WAY_SEL[1],1.000,0.500,NOT_WAY_SEL1);
    $setuphold(posedge CLK &&& cs_flag,posedge DC_WAY_OPTION,1.000,0.500,NOT_WAY_OPTION);
    $setuphold(posedge CLK &&& cs_flag,negedge DC_WAY_OPTION,1.000,0.500,NOT_WAY_OPTION);
    $setuphold(posedge CLK &&& cs_flag,posedge DC_WAY_ADDR,1.000,0.500,NOT_WAY_ADDR);
    $setuphold(posedge CLK &&& cs_flag,negedge DC_WAY_ADDR,1.000,0.500,NOT_WAY_ADDR);

    $setuphold(posedge CLK,posedge DC_SRAM_TAG_CS_,1.000,0.500,NOT_TAG_CS);
    $setuphold(posedge CLK,negedge DC_SRAM_TAG_CS_,1.000,0.500,NOT_TAG_CS);
    $setuphold(posedge CLK &&& cs_flag,posedge DC_TAG_WR_EN_,1.000,0.500,NOT_TAG_WR);
    $setuphold(posedge CLK &&& cs_flag,negedge DC_TAG_WR_EN_,1.000,0.500,NOT_TAG_WR);
    $setuphold(posedge CLK,posedge DC_OUTPUT_ENABLE,1.000,0.500,NOT_OUTPUT_ENABLE);
    $setuphold(posedge CLK,negedge DC_OUTPUT_ENABLE,1.000,0.500,NOT_OUTPUT_ENABLE);

    $setuphold(posedge CLK,posedge DC_TAG_PADDR[0 ],1.000,0.500,NOT_TAG_PADDR00);
    $setuphold(posedge CLK,posedge DC_TAG_PADDR[1 ],1.000,0.500,NOT_TAG_PADDR01);
    $setuphold(posedge CLK,posedge DC_TAG_PADDR[2 ],1.000,0.500,NOT_TAG_PADDR02);
    $setuphold(posedge CLK,posedge DC_TAG_PADDR[3 ],1.000,0.500,NOT_TAG_PADDR03);
    $setuphold(posedge CLK,posedge DC_TAG_PADDR[4 ],1.000,0.500,NOT_TAG_PADDR04);
    $setuphold(posedge CLK,posedge DC_TAG_PADDR[5 ],1.000,0.500,NOT_TAG_PADDR05);
    $setuphold(posedge CLK,posedge DC_TAG_PADDR[6 ],1.000,0.500,NOT_TAG_PADDR06);
    $setuphold(posedge CLK,posedge DC_TAG_PADDR[7 ],1.000,0.500,NOT_TAG_PADDR07);
    $setuphold(posedge CLK,posedge DC_TAG_PADDR[8 ],1.000,0.500,NOT_TAG_PADDR08);
    $setuphold(posedge CLK,posedge DC_TAG_PADDR[9 ],1.000,0.500,NOT_TAG_PADDR09);
    $setuphold(posedge CLK,posedge DC_TAG_PADDR[10],1.000,0.500,NOT_TAG_PADDR10);
    $setuphold(posedge CLK,posedge DC_TAG_PADDR[11],1.000,0.500,NOT_TAG_PADDR11);
    $setuphold(posedge CLK,posedge DC_TAG_PADDR[12],1.000,0.500,NOT_TAG_PADDR12);
    $setuphold(posedge CLK,posedge DC_TAG_PADDR[13],1.000,0.500,NOT_TAG_PADDR13);
    $setuphold(posedge CLK,posedge DC_TAG_PADDR[14],1.000,0.500,NOT_TAG_PADDR14);
    $setuphold(posedge CLK,posedge DC_TAG_PADDR[15],1.000,0.500,NOT_TAG_PADDR15);
    $setuphold(posedge CLK,posedge DC_TAG_PADDR[16],1.000,0.500,NOT_TAG_PADDR16);
    $setuphold(posedge CLK,posedge DC_TAG_PADDR[17],1.000,0.500,NOT_TAG_PADDR17);
    $setuphold(posedge CLK,posedge DC_TAG_PADDR[18],1.000,0.500,NOT_TAG_PADDR18);
    $setuphold(posedge CLK,posedge DC_TAG_PADDR[19],1.000,0.500,NOT_TAG_PADDR19);
    $setuphold(posedge CLK,negedge DC_TAG_PADDR[0 ],1.000,0.500,NOT_TAG_PADDR00);
    $setuphold(posedge CLK,negedge DC_TAG_PADDR[1 ],1.000,0.500,NOT_TAG_PADDR01);
    $setuphold(posedge CLK,negedge DC_TAG_PADDR[2 ],1.000,0.500,NOT_TAG_PADDR02);
    $setuphold(posedge CLK,negedge DC_TAG_PADDR[3 ],1.000,0.500,NOT_TAG_PADDR03);
    $setuphold(posedge CLK,negedge DC_TAG_PADDR[4 ],1.000,0.500,NOT_TAG_PADDR04);
    $setuphold(posedge CLK,negedge DC_TAG_PADDR[5 ],1.000,0.500,NOT_TAG_PADDR05);
    $setuphold(posedge CLK,negedge DC_TAG_PADDR[6 ],1.000,0.500,NOT_TAG_PADDR06);
    $setuphold(posedge CLK,negedge DC_TAG_PADDR[7 ],1.000,0.500,NOT_TAG_PADDR07);
    $setuphold(posedge CLK,negedge DC_TAG_PADDR[8 ],1.000,0.500,NOT_TAG_PADDR08);
    $setuphold(posedge CLK,negedge DC_TAG_PADDR[9 ],1.000,0.500,NOT_TAG_PADDR09);
    $setuphold(posedge CLK,negedge DC_TAG_PADDR[10],1.000,0.500,NOT_TAG_PADDR10);
    $setuphold(posedge CLK,negedge DC_TAG_PADDR[11],1.000,0.500,NOT_TAG_PADDR11);
    $setuphold(posedge CLK,negedge DC_TAG_PADDR[12],1.000,0.500,NOT_TAG_PADDR12);
    $setuphold(posedge CLK,negedge DC_TAG_PADDR[13],1.000,0.500,NOT_TAG_PADDR13);
    $setuphold(posedge CLK,negedge DC_TAG_PADDR[14],1.000,0.500,NOT_TAG_PADDR14);
    $setuphold(posedge CLK,negedge DC_TAG_PADDR[15],1.000,0.500,NOT_TAG_PADDR15);
    $setuphold(posedge CLK,negedge DC_TAG_PADDR[16],1.000,0.500,NOT_TAG_PADDR16);
    $setuphold(posedge CLK,negedge DC_TAG_PADDR[17],1.000,0.500,NOT_TAG_PADDR17);
    $setuphold(posedge CLK,negedge DC_TAG_PADDR[18],1.000,0.500,NOT_TAG_PADDR18);
    $setuphold(posedge CLK,negedge DC_TAG_PADDR[19],1.000,0.500,NOT_TAG_PADDR19);

    $setuphold(posedge CLK,posedge BIST_HOLD ,1.000,0.500,NOT_BIST_HOLD );
    $setuphold(posedge CLK,negedge BIST_HOLD ,1.000,0.500,NOT_BIST_HOLD );
    $setuphold(negedge CLK,posedge BIST_HOLD ,1.000,0.500,NOT_BIST_HOLD );
    $setuphold(negedge CLK,negedge BIST_HOLD ,1.000,0.500,NOT_BIST_HOLD );

    $setuphold(posedge CLK,posedge BIST_DIAG ,1.000,0.500,NOT_BIST_DIAG );
    $setuphold(posedge CLK,negedge BIST_DIAG ,1.000,0.500,NOT_BIST_DIAG );
    $setuphold(negedge CLK,posedge BIST_DIAG ,1.000,0.500,NOT_BIST_DIAG );
    $setuphold(negedge CLK,negedge BIST_DIAG ,1.000,0.500,NOT_BIST_DIAG );

    $setuphold(posedge CLK,posedge SE        ,1.000,0.500,NOT_SE        );
    $setuphold(posedge CLK,negedge SE        ,1.000,0.500,NOT_SE        );
    $setuphold(negedge CLK,posedge SE        ,1.000,0.500,NOT_SE        );
    $setuphold(negedge CLK,negedge SE        ,1.000,0.500,NOT_SE        );

    $setuphold(posedge CLK,posedge SI        ,1.000,0.500,NOT_SI        );
    $setuphold(posedge CLK,negedge SI        ,1.000,0.500,NOT_SI        );
    $setuphold(posedge CLK,posedge SCAN_MODE ,1.000,0.500,NOT_SCAN_MODE );
    $setuphold(posedge CLK,negedge SCAN_MODE ,1.000,0.500,NOT_SCAN_MODE );

    $setuphold(posedge CLK,posedge FORWARD_INFO[8],1.000,0.500,NOT_FORWARD_INFO8);
    $setuphold(posedge CLK,posedge FORWARD_INFO[7],1.000,0.500,NOT_FORWARD_INFO7);
    $setuphold(posedge CLK,posedge FORWARD_INFO[6],1.000,0.500,NOT_FORWARD_INFO6);
    $setuphold(posedge CLK,posedge FORWARD_INFO[5],1.000,0.500,NOT_FORWARD_INFO5);
    $setuphold(posedge CLK,posedge FORWARD_INFO[4],1.000,0.500,NOT_FORWARD_INFO4);
    $setuphold(posedge CLK,posedge FORWARD_INFO[3],1.000,0.500,NOT_FORWARD_INFO3);
    $setuphold(posedge CLK,posedge FORWARD_INFO[2],1.000,0.500,NOT_FORWARD_INFO2);
    $setuphold(posedge CLK,posedge FORWARD_INFO[1],1.000,0.500,NOT_FORWARD_INFO1);
    $setuphold(posedge CLK,posedge FORWARD_INFO[0],1.000,0.500,NOT_FORWARD_INFO0);
    $setuphold(posedge CLK,negedge FORWARD_INFO[8],1.000,0.500,NOT_FORWARD_INFO8);
    $setuphold(posedge CLK,negedge FORWARD_INFO[7],1.000,0.500,NOT_FORWARD_INFO7);
    $setuphold(posedge CLK,negedge FORWARD_INFO[6],1.000,0.500,NOT_FORWARD_INFO6);
    $setuphold(posedge CLK,negedge FORWARD_INFO[5],1.000,0.500,NOT_FORWARD_INFO5);
    $setuphold(posedge CLK,negedge FORWARD_INFO[4],1.000,0.500,NOT_FORWARD_INFO4);
    $setuphold(posedge CLK,negedge FORWARD_INFO[3],1.000,0.500,NOT_FORWARD_INFO3);
    $setuphold(posedge CLK,negedge FORWARD_INFO[2],1.000,0.500,NOT_FORWARD_INFO2);
    $setuphold(posedge CLK,negedge FORWARD_INFO[1],1.000,0.500,NOT_FORWARD_INFO1);
    $setuphold(posedge CLK,negedge FORWARD_INFO[0],1.000,0.500,NOT_FORWARD_INFO0);
    $setuphold(posedge CLK,posedge DC_SRAM_BYPASS,1.000,0.500,NOT_SRAM_BYPASS);
    $setuphold(posedge CLK,negedge DC_SRAM_BYPASS,1.000,0.500,NOT_SRAM_BYPASS);

        /*===========================
            clock period & width
        ============================*/

        $period(posedge CLK ,3.000,NOT_CLK_PER);
        $width(posedge CLK,1.000,0,NOT_CLK_MINH);
        $width(posedge CLK,1.000,0,NOT_CLK_MINL);
        $period(negedge CLK ,3.000,NOT_CLK_PER);
        $width(negedge CLK,1.000,0,NOT_CLK_MINH);
        $width(negedge CLK,1.000,0,NOT_CLK_MINL);

        /*===========================
            define IOPATH
        ============================*/

        (CLK => DC_SRAM_TAG_OUT[00])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_TAG_OUT[01])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_TAG_OUT[02])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_TAG_OUT[03])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_TAG_OUT[04])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_TAG_OUT[05])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_TAG_OUT[06])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_TAG_OUT[07])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_TAG_OUT[08])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_TAG_OUT[09])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_TAG_OUT[10])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_TAG_OUT[11])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_TAG_OUT[12])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_TAG_OUT[13])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_TAG_OUT[14])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_TAG_OUT[15])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_TAG_OUT[16])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_TAG_OUT[17])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_TAG_OUT[18])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_TAG_OUT[19])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_TAG_OUT[20])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_TAG_OUT[21])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_TAG_OUT[22])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_TAG_OUT[23])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_TAG_OUT[24])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_TAG_OUT[25])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_TAG_OUT[26])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);

        (CLK => DC_SRAM_LRU_OUT[0])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_LRU_OUT[1])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_LRU_OUT[2])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_LRU_OUT[3])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);

        (CLK => DC_SRAM_TAG0_ST_OUT[0])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_TAG0_ST_OUT[1])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_TAG0_ST_OUT[2])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_TAG0_ST_OUT[3])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_TAG0_ST_OUT[4])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_TAG0_ST_OUT[5])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_TAG0_ST_OUT[6])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_TAG1_ST_OUT[0])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_TAG1_ST_OUT[1])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_TAG1_ST_OUT[2])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_TAG1_ST_OUT[3])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_TAG1_ST_OUT[4])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_TAG1_ST_OUT[5])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_TAG1_ST_OUT[6])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_TAG2_ST_OUT[0])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_TAG2_ST_OUT[1])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_TAG2_ST_OUT[2])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_TAG2_ST_OUT[3])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_TAG2_ST_OUT[4])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_TAG2_ST_OUT[5])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_TAG2_ST_OUT[6])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);

        (CLK => DC_SRAM_TAG3_ST_OUT[0])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_TAG3_ST_OUT[1])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_TAG3_ST_OUT[2])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_TAG3_ST_OUT[3])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_TAG3_ST_OUT[4])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_TAG3_ST_OUT[5])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_TAG3_ST_OUT[6])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);

        (CLK => DC_SRAM_HIT[0])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_HIT[1])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_HIT[2])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_HIT[3])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_HIT[4])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_HIT[5])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_SRAM_HIT[6])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);

        (CLK => DC_TAG0_OUT[00])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG0_OUT[01])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG0_OUT[02])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG0_OUT[03])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG0_OUT[04])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG0_OUT[05])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG0_OUT[06])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG0_OUT[07])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG0_OUT[08])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG0_OUT[09])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG0_OUT[10])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG0_OUT[11])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG0_OUT[12])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG0_OUT[13])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG0_OUT[14])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG0_OUT[15])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG0_OUT[16])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG0_OUT[17])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG0_OUT[18])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG0_OUT[19])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG0_OUT[20])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG0_OUT[21])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG0_OUT[22])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG0_OUT[23])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG0_OUT[24])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG0_OUT[25])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG0_OUT[26])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);

        (CLK => DC_TAG1_OUT[00])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG1_OUT[01])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG1_OUT[02])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG1_OUT[03])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG1_OUT[04])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG1_OUT[05])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG1_OUT[06])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG1_OUT[07])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG1_OUT[08])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG1_OUT[09])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG1_OUT[10])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG1_OUT[11])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG1_OUT[12])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG1_OUT[13])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG1_OUT[14])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG1_OUT[15])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG1_OUT[16])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG1_OUT[17])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG1_OUT[18])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG1_OUT[19])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG1_OUT[20])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG1_OUT[21])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG1_OUT[22])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG1_OUT[23])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG1_OUT[24])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG1_OUT[25])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG1_OUT[26])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);

        (CLK => DC_TAG2_OUT[00])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG2_OUT[01])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG2_OUT[02])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG2_OUT[03])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG2_OUT[04])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG2_OUT[05])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG2_OUT[06])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG2_OUT[07])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG2_OUT[08])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG2_OUT[09])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG2_OUT[10])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG2_OUT[11])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG2_OUT[12])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG2_OUT[13])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG2_OUT[14])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG2_OUT[15])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG2_OUT[16])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG2_OUT[17])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG2_OUT[18])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG2_OUT[19])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG2_OUT[20])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG2_OUT[21])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG2_OUT[22])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG2_OUT[23])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG2_OUT[24])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG2_OUT[25])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG2_OUT[26])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);

        (CLK => DC_TAG3_OUT[00])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG3_OUT[01])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG3_OUT[02])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG3_OUT[03])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG3_OUT[04])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG3_OUT[05])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG3_OUT[06])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG3_OUT[07])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG3_OUT[08])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG3_OUT[09])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG3_OUT[10])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG3_OUT[11])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG3_OUT[12])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG3_OUT[13])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG3_OUT[14])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG3_OUT[15])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG3_OUT[16])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG3_OUT[17])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG3_OUT[18])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG3_OUT[19])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG3_OUT[20])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG3_OUT[21])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG3_OUT[22])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG3_OUT[23])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG3_OUT[24])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG3_OUT[25])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => DC_TAG3_OUT[26])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);

        (CLK => LRU_OUT_BEFORE_LCH[1])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => LRU_OUT_BEFORE_LCH[0])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => HIT_VEC_OUT[3])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => HIT_VEC_OUT[2])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => HIT_VEC_OUT[1])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => HIT_VEC_OUT[0])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => HIT_VEC_FOR_SEL[3])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => HIT_VEC_FOR_SEL[2])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => HIT_VEC_FOR_SEL[1])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => HIT_VEC_FOR_SEL[0])=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);
        (CLK => SO)=(1.000,1.000, 0.500, 1.000, 0.500, 1.000);

	endspecify

    dc_tag_array    dc_tag_array (
            .DC_TAG0_OUT            (DC_TAG0_OUTi            ),
            .DC_TAG1_OUT            (DC_TAG1_OUTi            ),
            .DC_TAG2_OUT            (DC_TAG2_OUTi            ),
            .DC_TAG3_OUT            (DC_TAG3_OUTi            ),
            .DC_LRU_OUT             (DC_LRU_OUT              ),
            .HIT_VEC_OUT            (HIT_VEC_OUTi            ),
            .DC_SRAM_ADDR_IN        (DC_SRAM_ADDR_INi        ),
            .DC_SRAM_TAG_IN         (DC_SRAM_TAG_INi         ),
            .FORWARD_TAG_SEL        (FORWARD_TAG_SEL         ),
            .DC_SRAM_LRU_IN         (DC_SRAM_LRU_INi         ),
            .DC_SRAM_TAG_CS_        (DC_SRAM_TAG_CSi_        ),
            .DC_TAG_CS_             (DC_TAG_CS_              ),
            .SRAM_TAG_CS_           (SRAM_TAG_CS_            ),
            .DC_TAG_WR_             (DC_TAG_WR_              ),
            .LRU_WR_EN_             (LRU_WR_EN_              ),
            .SRAM_INVLD_            (SRAM_INVLD_             ),
            .DC_TAG_PADDR           (DC_TAG_PADDRi           ),
            .CLK_LATCH              (CLK_LATCH               ),
            .BIST_HOLD              (BIST_HOLDi              ),
            .BIST_DIAG              (BIST_DIAGi              ),
            .SCAN_MODE              (SCAN_MODEi              ),
            .SE                     (SEi                     ),
            .SO                     (SOi                     ),
            .SI                     (dtag_array_si_in        ),
            .CLK                    (CLKi                    )
                                 );

    dc_tag_ctrl     dc_tag_ctrl (
            .DC_SRAM_TAG_OUT        (DC_SRAM_TAG_OUTi        ),
            .DC_SRAM_LRU_OUT        (DC_SRAM_LRU_OUTi        ),
            .DC_SRAM_HIT            (DC_SRAM_HITi            ),
            .DC_SRAM_TAG0_ST_OUT    (DC_SRAM_TAG0_ST_OUTi    ),
            .DC_SRAM_TAG1_ST_OUT    (DC_SRAM_TAG1_ST_OUTi    ),
            .DC_SRAM_TAG2_ST_OUT    (DC_SRAM_TAG2_ST_OUTi    ),
            .DC_SRAM_TAG3_ST_OUT    (DC_SRAM_TAG3_ST_OUTi    ),
            .LRU_OUT_BEFORE_LCH     (LRU_OUT_BEFORE_LCHi     ),
            .DC_TAG_WR_             (DC_TAG_WR_              ),
            .LRU_WR_EN_             (LRU_WR_EN_              ),
            .DC_TAG_CS_             (DC_TAG_CS_              ),
            .SRAM_TAG_CS_           (SRAM_TAG_CS_            ),
            .SRAM_INVLD_            (SRAM_INVLD_             ),
            .FORWARD_TAG_SEL        (FORWARD_TAG_SEL         ),
            .HIT_VEC_FOR_SEL        (HIT_VEC_FOR_SELi        ),
            .CLK_LATCH              (CLK_LATCH               ),

            .DC_TAG0_OUT            (DC_TAG0_OUTi            ),
            .DC_TAG1_OUT            (DC_TAG1_OUTi            ),
            .DC_TAG2_OUT            (DC_TAG2_OUTi            ),
            .DC_TAG3_OUT            (DC_TAG3_OUTi            ),
            .DC_LRU_OUT             (DC_LRU_OUT              ),
            .HIT_VEC_OUT            (HIT_VEC_OUTi            ),
            .DC_SRAM_ADDR_IN        (DC_SRAM_ADDR_INi        ),
            .DC_SRAM_LRU_IN         (DC_SRAM_LRU_INi         ),
            .DC_SRAM_TAG_CS_        (DC_SRAM_TAG_CSi_        ),
            .DC_TAG_WR_EN_          (DC_TAG_WR_ENi_          ),
            .DC_SRAM_INVLD_         (DC_SRAM_INVLDi_         ),
            .DC_SRAM_INDEXOP_       (DC_SRAM_INDEXOPi_       ),
            .DC_SRAM_WAY_SEL        (DC_SRAM_WAY_SELi        ),
            .FORWARD_INFO           (FORWARD_INFOi           ),
            .DC_SRAM_BYPASS         (DC_SRAM_BYPASSi         ),
            .DC_OUTPUT_ENABLE       (DC_OUTPUT_ENABLEi       ),
            .DC_WAY_OPTION          (DC_WAY_OPTIONi          ),
            .DC_WAY_ADDR            (DC_WAY_ADDRi            ),
            .BIST_HOLD              (BIST_HOLDi              ),
            .BIST_DIAG              (BIST_DIAGi              ),
            .SCAN_MODE              (SCAN_MODEi              ),
            .SE                     (SEi                     ),
            .SO                     (dtag_array_si_in        ),
            .SI                     (SIi                     ),
            .CLK                    (CLKi                    )
                                );

endmodule
`endcelldefine

