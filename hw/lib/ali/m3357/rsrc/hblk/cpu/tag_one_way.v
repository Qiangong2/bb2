/*******************************************************************************
             Project Name :         T2Risc1H
             File Name :            tag_one_way.v
             Create Date :          2001/06/06
             Author :               Charliema
             Description :          behavior module for d_cache one way tag
             Revision History :
                 06/06/2001   charlie       initial creation
*******************************************************************************/
//`include  "defines.h"
module tag_one_way  (
    D_ONEWAY_TAG_OUT,
    D_ONEWAY_TAG_IN,
    D_TAG_ADDR_IN,
    D_ONEWAY_TAG_CS_,
    D_ONEWAY_TAG_WR_EN_,
    CLK
    );

    output [`dct_size-1:0]      D_ONEWAY_TAG_OUT;
    input  [`addr_width-1:5]    D_TAG_ADDR_IN;
    input  [`dct_size-1:0]      D_ONEWAY_TAG_IN;
    input                       D_ONEWAY_TAG_CS_;
    input  [1:0]                D_ONEWAY_TAG_WR_EN_;
    input                       CLK;

    reg [`dct_size-8:0]    tag_content[`line_depth-1:0];  //tag content define
    reg [6:0]              tag_state[`line_depth-1:0];  //tag state define
    reg [`dct_size-8:0]    d_tag_content;
    reg [6:0]              d_tag_state;

    wire    CLK_ =~CLK;
// the mux_logic below is for write_thorough, circuit use
// "D_ONEWAY_TAG_OUT = {d_tag_content,d_tag_state}" here.
    wire [`dct_size-1:0]    D_ONEWAY_TAG_OUT;
    assign D_ONEWAY_TAG_OUT[`dct_size-1:7] = (~D_ONEWAY_TAG_WR_EN_[1] & ~D_ONEWAY_TAG_CS_) ?
                                               D_ONEWAY_TAG_IN[`dct_size-1:7] : d_tag_content;
    assign D_ONEWAY_TAG_OUT[6:0] = (~D_ONEWAY_TAG_WR_EN_[0] & ~D_ONEWAY_TAG_CS_) ?
                                     D_ONEWAY_TAG_IN[6:0] : d_tag_state;
    wire    d_tag_rd_en_ = ~(~D_ONEWAY_TAG_CS_ & (D_ONEWAY_TAG_WR_EN_[1] == 1'b1));
    wire    tag_content_wr_en_ = D_ONEWAY_TAG_CS_ | D_ONEWAY_TAG_WR_EN_[1];
    wire    tag_state_wr_en_ = D_ONEWAY_TAG_CS_ | D_ONEWAY_TAG_WR_EN_[0];
    wire [`addr_width-1:5]    d_tag_addr_in  = D_TAG_ADDR_IN;
    wire [`dct_size-1:0]     d_oneway_tag_in = D_ONEWAY_TAG_IN;

// read
always @(d_tag_rd_en_ or d_tag_addr_in or tag_content[d_tag_addr_in])
  begin
    if (~d_tag_rd_en_)
      d_tag_content = tag_content[d_tag_addr_in];
  end

always @(d_tag_rd_en_ or d_tag_addr_in or tag_state[d_tag_addr_in])
  begin
    if (~d_tag_rd_en_)
      d_tag_state = tag_state[d_tag_addr_in];
  end

// write
always @(posedge CLK_)
  begin
    if (~tag_content_wr_en_)
      begin
          tag_content[d_tag_addr_in] <= d_oneway_tag_in[`dct_size-1:7];
      end
  end

always @(posedge CLK_)
  begin
    if (~tag_state_wr_en_)
      begin
          tag_state[d_tag_addr_in]   <= d_oneway_tag_in[6:0];
      end
  end

endmodule