/*******************************************************************************
             Project Name : T2risc1H
             File Name : ic_oneway_tag.v
             Create Date : 2001/05/30
             Author : Charliema
             Description :behavior module for i_cache tag one way bank
             Revision History :
                 05/31/2001   charlie       initial creation
                 09/07/2001   charlie       add write_through
*******************************************************************************/
//`include  "defines.h"
module ic_oneway_tag  (
    Q,      //tag content output for i_cache inst
    A,      //addr in
    D,      //tag content in
    WEN,    //tag write enable
    CEN,    //tag read enable
    CLK
                      );
	parameter ict_size = 22;
        parameter wide = 2;
        parameter addr_width = 12;
        parameter u_dly = 1;
        parameter icd_size = 72;
        parameter line_depth = 128;
        parameter hit_vec_dly = 2;
 
    output [ict_size-1:0]   Q;
    input  [addr_width-1:5] A;
    input  [ict_size-1:0]   D;
    input  [1:0]             WEN;
    input                    CEN;
    input                    CLK;
  
    reg  [ict_size-1:2]     tag_content[line_depth-1:0];  //tag content define   
    reg  [1:0]               tag_state[line_depth-1:0];    //tag state define
    reg  [ict_size-1:0]     tag_out;
    wire [addr_width-1:5]   addr = A[addr_width-1:5];
    wire                     CLK_ = !CLK;
    wire                     wr_en = WEN[0] & WEN[1];
    
// the one line code is for verilog simulation, circuit just use tag_out derect to output: port Q;
// this modify is for verion 0907, just for write_throuth
    wire [ict_size-1:0]    Q = tag_out & {(ict_size){(WEN[1] | CEN)}} | 
                                D & {(ict_size){~WEN[1] & ~CEN}};
    
always @(CEN or WEN or addr or tag_content[addr] or tag_state[addr])
  begin
    if(~CEN & WEN[1])
      begin
        tag_out = {tag_content[addr],tag_state[addr]};
      end
  end
     
always @(posedge CLK_)
  begin
    if(~CEN & ~WEN[1])
      tag_content[addr] <= D [ict_size-1:2];
  end
  
always @(posedge CLK_)
  begin
    if (~CEN & ~wr_en)
      tag_state[addr]   <= D[1:0];
  end 

endmodule
