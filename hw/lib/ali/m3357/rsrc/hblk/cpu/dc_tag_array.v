/*******************************************************************************
             Project Name :         T2Risc1H
             File Name :            dc_tag_array.v
             Create Date :          2001/06/06
             Author :               Charliema
             Description :          behavior module for d_cache tag array
             Revision History :
                 06/06/2001   charlie       initial creation
*******************************************************************************/

//`include  "defines.h"

module dc_tag_array  (
    // Output ports
    DC_TAG0_OUT,
    DC_TAG1_OUT,
    DC_TAG2_OUT,
    DC_TAG3_OUT,
    DC_LRU_OUT,
    HIT_VEC_OUT,

    // Input ports
    DC_SRAM_ADDR_IN,
    DC_SRAM_TAG_IN,
    FORWARD_TAG_SEL,
    DC_SRAM_LRU_IN,
    DC_SRAM_TAG_CS_,
    DC_TAG_CS_,
    SRAM_TAG_CS_,
    DC_TAG_WR_,
    LRU_WR_EN_,
    SRAM_INVLD_,
    DC_TAG_PADDR,
    CLK_LATCH,
    BIST_HOLD,
    BIST_DIAG,
    SCAN_MODE,
    SE,
    SO,
    SI,
    CLK
    );

    output [`dct_size-1:0]    DC_TAG0_OUT;
    output [`dct_size-1:0]    DC_TAG1_OUT;
    output [`dct_size-1:0]    DC_TAG2_OUT;
    output [`dct_size-1:0]    DC_TAG3_OUT;
    output [`lru_wide-1:0]    DC_LRU_OUT;
    output [3:0]              HIT_VEC_OUT;

    input [`addr_width-6:0]   DC_SRAM_ADDR_IN;
    input [`lru_wide-1:0]     DC_SRAM_LRU_IN;
    input [`dct_size-1:0]     DC_SRAM_TAG_IN;
    input [3:0]               FORWARD_TAG_SEL;
    input                     DC_SRAM_TAG_CS_;
    input [3:0]               DC_TAG_CS_;
    input                     SRAM_TAG_CS_;
    input [1:0]               DC_TAG_WR_;
    input                     LRU_WR_EN_;
    input                     SRAM_INVLD_;
    input [`dct_size-8:0]     DC_TAG_PADDR;
    input                     CLK_LATCH;
    input                     BIST_HOLD;
    input                     BIST_DIAG;
    input                     SE;
    input                     SI;
    output                    SO;
    input                     SCAN_MODE;
    input                     CLK;

    wire    d_tag0_cs_ = DC_TAG_CS_[0];
    wire    d_tag1_cs_ = DC_TAG_CS_[1];
    wire    d_tag2_cs_ = DC_TAG_CS_[2];
    wire    d_tag3_cs_ = DC_TAG_CS_[3];
    wire [1:0]    d_tag_wr_en_ = DC_TAG_WR_;
    wire [`dct_size-1:0]    dc_tag_0_out;
    wire [`dct_size-1:0]    dc_tag_1_out;
    wire [`dct_size-1:0]    dc_tag_2_out;
    wire [`dct_size-1:0]    dc_tag_3_out;
    wire [`lru_wide-1:0]    LRU_OUT;
    reg    DC_HIT_0;
    reg    DC_HIT_1;
    reg    DC_HIT_2;
    reg    DC_HIT_3;
    reg    HIT_NBL_0;
    reg    HIT_NBL_1;
    reg [3:0]    HIT_VEC_OUT;
    reg [`dct_size-1:0]    TAG0_OUT;
    reg [`dct_size-1:0]    TAG1_OUT;
    reg [`dct_size-1:0]    TAG2_OUT;
    reg [`dct_size-1:0]    TAG3_OUT;
    reg [`lru_wide-1:0]    DC_LRU_OUT;
    reg [`dct_size-1:0]    sram_tag_in;
    reg [`addr_width-6:0]  tag_addr_in;
    reg [`lru_wide-1:0]    sram_lru_in;
    wire                   SO = sram_lru_in[0];

    // the follow five lines code is for verilog simulation, circuit don't care it, just use DC_TAG_PADDR.
    wire [`dct_size-8:0]  #1 tag_paddr = DC_TAG_PADDR;



always @(posedge CLK_LATCH)
  begin
    if (~SE & ~BIST_HOLD)
      begin
        if (~DC_SRAM_TAG_CS_)
          begin
            sram_tag_in       <= #`u_dly DC_SRAM_TAG_IN;
            sram_lru_in       <= #`u_dly DC_SRAM_LRU_IN;
            tag_addr_in       <= #`u_dly DC_SRAM_ADDR_IN[`addr_width-6:0];
          end
      end
  else if (SE | BIST_HOLD & BIST_DIAG)
    begin
      sram_tag_in       <= #`u_dly {tag_addr_in[0],sram_tag_in[`dct_size-1:1]};
      sram_lru_in       <= #`u_dly {sram_tag_in[0],sram_lru_in[1]};
      tag_addr_in       <= #`u_dly {SI,tag_addr_in[`addr_width-6:1]};
    end
  end

    wire [`dct_size-1:0]  dc_tag_in = {sram_tag_in[`dct_size-1:7],
                                       SRAM_INVLD_ ? sram_tag_in[6:0] : {7{SRAM_INVLD_}}};

// hit compare for tag, latch hit, tag_out and lru_out , mux logic for tag
    wire [`dct_size-1:0]    tag0_to_compare = FORWARD_TAG_SEL[0] ? DC_SRAM_TAG_IN : TAG0_OUT;
    wire [`dct_size-1:0]    tag1_to_compare = FORWARD_TAG_SEL[1] ? DC_SRAM_TAG_IN : TAG1_OUT;
    wire [`dct_size-1:0]    tag2_to_compare = FORWARD_TAG_SEL[2] ? DC_SRAM_TAG_IN : TAG2_OUT;
    wire [`dct_size-1:0]    tag3_to_compare = FORWARD_TAG_SEL[3] ? DC_SRAM_TAG_IN : TAG3_OUT;
    reg [3:0]   HIT_VEC;

always @(d_tag0_cs_ or d_tag1_cs_ or d_tag2_cs_ or d_tag3_cs_ or tag_paddr or tag0_to_compare or
         tag1_to_compare or tag2_to_compare or tag3_to_compare)
  begin
    if (~(d_tag0_cs_ & d_tag1_cs_ & d_tag2_cs_ & d_tag3_cs_))
      begin
        HIT_VEC[0] = (tag_paddr == tag0_to_compare[`dct_size-1:7]);
        HIT_VEC[1] = (tag_paddr == tag1_to_compare[`dct_size-1:7]);
        HIT_VEC[2] = (tag_paddr == tag2_to_compare[`dct_size-1:7]);
        HIT_VEC[3] = (tag_paddr == tag3_to_compare[`dct_size-1:7]);
      end
  end

    wire [`dct_size-1:0]    DC_TAG0_OUT = tag0_to_compare;
    wire [`dct_size-1:0]    DC_TAG1_OUT = tag1_to_compare;
    wire [`dct_size-1:0]    DC_TAG2_OUT = tag2_to_compare;
    wire [`dct_size-1:0]    DC_TAG3_OUT = tag3_to_compare;

always @ (d_tag0_cs_ or dc_tag_0_out)
  begin
    if (~d_tag0_cs_)
      TAG0_OUT = dc_tag_0_out;
  end

always @ (d_tag1_cs_ or dc_tag_1_out)
  begin
    if (~d_tag1_cs_)
      TAG1_OUT = dc_tag_1_out;
  end

always @ (d_tag2_cs_ or dc_tag_2_out)
  begin
    if (~d_tag2_cs_)
      TAG2_OUT = dc_tag_2_out;
  end

always @ (d_tag3_cs_ or dc_tag_3_out)
  begin
    if (~d_tag3_cs_)
      TAG3_OUT = dc_tag_3_out;
  end

always @ (SRAM_TAG_CS_ or LRU_OUT or HIT_VEC)
  begin
    if (~SRAM_TAG_CS_)
      begin
        DC_LRU_OUT = LRU_OUT;
        HIT_VEC_OUT = HIT_VEC;
      end
  end

// generate core select signal.
  wire          core_sel_           = SCAN_MODE | BIST_HOLD;
  wire  [3:0]   dc_tag_cs_core_     = DC_TAG_CS_ | {4{core_sel_}};
  wire          sram_tag_cs_core_   = SRAM_TAG_CS_ | core_sel_;

    tag_one_way  tag_way0   (
            .D_ONEWAY_TAG_OUT      (dc_tag_0_out          ),
            .D_ONEWAY_TAG_IN       (dc_tag_in             ),
            .D_TAG_ADDR_IN         (tag_addr_in           ),
            .D_ONEWAY_TAG_CS_      (dc_tag_cs_core_[0]    ),
            .D_ONEWAY_TAG_WR_EN_   (d_tag_wr_en_          ),
            .CLK                   (CLK                   )
                            );

    tag_one_way  tag_way1   (
            .D_ONEWAY_TAG_OUT      (dc_tag_1_out          ),
            .D_ONEWAY_TAG_IN       (dc_tag_in             ),
            .D_TAG_ADDR_IN         (tag_addr_in           ),
            .D_ONEWAY_TAG_CS_      (dc_tag_cs_core_[1]    ),
            .D_ONEWAY_TAG_WR_EN_   (d_tag_wr_en_          ),
            .CLK                   (CLK                   )
                            );

    tag_one_way  tag_way2   (
            .D_ONEWAY_TAG_OUT      (dc_tag_2_out          ),
            .D_ONEWAY_TAG_IN       (dc_tag_in             ),
            .D_TAG_ADDR_IN         (tag_addr_in           ),
            .D_ONEWAY_TAG_CS_      (dc_tag_cs_core_[2]    ),
            .D_ONEWAY_TAG_WR_EN_   (d_tag_wr_en_          ),
            .CLK                   (CLK                   )
                            );

    tag_one_way  tag_way3   (
            .D_ONEWAY_TAG_OUT      (dc_tag_3_out          ),
            .D_ONEWAY_TAG_IN       (dc_tag_in             ),
            .D_TAG_ADDR_IN         (tag_addr_in           ),
            .D_ONEWAY_TAG_CS_      (dc_tag_cs_core_[3]    ),
            .D_ONEWAY_TAG_WR_EN_   (d_tag_wr_en_          ),
            .CLK                   (CLK                   )
                            );

      dc_lru    dc_lru_1    (
            .DC_SRAM_LRU_OUT       (LRU_OUT               ),
            .DC_SRAM_LRU_IN        (sram_lru_in           ),
            .DC_SRAM_LRU_ADDR_IN   (tag_addr_in           ),
            .DC_SRAM_LRU_CS_       (sram_tag_cs_core_     ),
            .DC_SRAM_LRU_WR_EN_    (LRU_WR_EN_            ),
            .CLK                   (CLK                   )
                            );

endmodule
