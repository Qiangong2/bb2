/*******************************************************************************
             Project Name : T2Risc1H
             File Name : dc_lru.v
             Create Date : 2001/06/06
             Author : Charliema
             Description :behavior module for d_cache lru
             Revision History :
                 06/06/2001   charlie       initial creation
*******************************************************************************/
//`include  "defines.h"
module dc_lru  (
    DC_SRAM_LRU_OUT,
    DC_SRAM_LRU_IN,
    DC_SRAM_LRU_ADDR_IN,
    DC_SRAM_LRU_CS_,
    DC_SRAM_LRU_WR_EN_,
    CLK
    );

    output [`lru_wide-1:0]   DC_SRAM_LRU_OUT;
    input  [`addr_width-6:0] DC_SRAM_LRU_ADDR_IN;
    input  [`lru_wide-1:0]   DC_SRAM_LRU_IN;
    input                    DC_SRAM_LRU_CS_;
    input                    DC_SRAM_LRU_WR_EN_;
    input                    CLK;

    reg [`lru_wide-1:0]    lru[`line_depth-1:0];  //lru sram define
    reg [`lru_wide-1:0]    dc_lru_out;
// the mux_logic below is for write_thorough, circuit use "DC_SRAM_LRU_OUT = dc_lru_out" here.
    assign    DC_SRAM_LRU_OUT = (DC_SRAM_LRU_WR_EN_ | DC_SRAM_LRU_CS_) ? dc_lru_out : DC_SRAM_LRU_IN;
    wire    CLK_ =~CLK;

// read
always @(DC_SRAM_LRU_CS_ or DC_SRAM_LRU_WR_EN_ or DC_SRAM_LRU_ADDR_IN or lru[DC_SRAM_LRU_ADDR_IN])
  begin
    if (~DC_SRAM_LRU_CS_ & DC_SRAM_LRU_WR_EN_)
      dc_lru_out = lru[DC_SRAM_LRU_ADDR_IN];
  end

// write
always @(posedge CLK_)
  begin
    if (~DC_SRAM_LRU_CS_ & ~DC_SRAM_LRU_WR_EN_)
      begin
        lru[DC_SRAM_LRU_ADDR_IN] <= DC_SRAM_LRU_IN;
      end
  end
endmodule