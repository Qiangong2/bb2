`celldefine
module  DTLB6304 (
        // output 
        HIT_IN_DTLB,        // timing require: 1.0ns(arrive time)
        HIT_VEC,            // timing require: 1.0ns(arrive time)
        ENTRY_CACHE,        // timing require: 1.0ns(arrive time)
        ENTRY_PFN,          // timing require: 1.0ns(arrive time)
        ENTRY_DIRTY,        // timing require: 1.0ns(arrive time)

        VPN_IN,             // timing require: 1.5ns(arrive time)
        COMP_EN,            // timing require: 1.5ns(arrive time)
        FLUSH_VEC,          // timing require: 1.5ns(arrive time)
                            // the vector is 4 bit-wide, it will indicate which entry(entries) should be flushed.
                            // It will be multi-valid sometimes.
        UPDATE_DATA,        // timing require: 1.5ns(arrive time)
                            // update_vpn2      [77:59]
                            // update_asid      [58:51]
                            // update_g         [50]
                            // update_v0        [49]
                            // update_v1        [48]
                            // update_pfn0      [47:28]
                            // update_pfn1      [27:8]
                            // update_c0        [7:5]
                            // update_c1        [4:2]
                            // update_d0        [1]
                            // update_d1        [0]
        UPDATE_EN,          // timing require: 1.5ns(arrive time)
        UPDATE_VEC,         // timing require: 1.5ns(arrive time)
                            // after one-hot encode, only 1 bit valid.
        CP0_ASID,           // timing require: 1.5ns(arrive time)
        BYPASS_MODE_IN,     // timing require: 1.5ns(arrive time)
        CONFIG_K0_IN,       // timing require: 1.5ns(arrive time)

        SCAN_MODE,          // scan mode. open sense-amplify's.
        //FRE,                // frequency option
        SO,                 // scan chain output
        SE,                 // scan enable signal
        SI,                 // scan chain input

        CLK                 // clock
                );


output          HIT_IN_DTLB;
output  [3:0]   HIT_VEC;
output  [2:0]   ENTRY_CACHE;
output  [19:0]  ENTRY_PFN;
output          ENTRY_DIRTY;


input   [19:0]  VPN_IN;
input           COMP_EN;
input   [3:0]   FLUSH_VEC;
input   [77:0]  UPDATE_DATA;
input           UPDATE_EN;
input   [3:0]   UPDATE_VEC;
input   [7:0]   CP0_ASID;
input   [1:0]   BYPASS_MODE_IN;
input   [2:0]   CONFIG_K0_IN;
input           CLK;

input           SCAN_MODE;
//input           FRE;
output          SO;
input           SE;
input           SI;

parameter       U_DLY   = 1;

reg     [19:0]  vpn;
reg     [1:0]   bypass_mode;
reg     [2:0]   config_k0;
reg     [7:0]   cp0_asid;
reg             comp_cs;
reg             flush_cs;
reg             update_cs;
reg     [77:0]  update_data;
reg     [3:0]   update_vec;
reg     [3:0]   flush_vec;

wire            HIT_IN_DTLBi;
wire    [3:0]   HIT_VECi;
wire    [2:0]   ENTRY_CACHEi;
wire    [19:0]  ENTRY_PFNi;
wire            ENTRY_DIRTYi;
wire            SOi;

wire    [19:0]  VPN_INi;
wire            COMP_ENi;
wire    [3:0]   FLUSH_VECi;
wire    [77:0]  UPDATE_DATAi;
wire            UPDATE_ENi;
wire    [3:0]   UPDATE_VECi;
wire    [7:0]   CP0_ASIDi;
wire    [1:0]   BYPASS_MODE_INi;
wire    [2:0]   CONFIG_K0_INi;

wire            SCAN_MODEi;
//wire            FREi;
wire            SEi;
wire            SIi;

wire    [19:0]  entry_pfn;
wire    [2:0]   entry_cache;
wire            entry_dirty;
wire    [3:0]   HIT_VEC;
wire    [3:0]   hit_vec,        nml_hit_vec;
wire            HIT_IN_DTLB;
wire    [2:0]   ENTRY_CACHE,    bypass_cache;
wire    [19:0]  ENTRY_PFN,      bypass_pfn;
wire            ENTRY_DIRTY;
wire    [3:0]   UPDATE_VEC;
wire    [3:0]   FLUSH_VEC;
wire            bypass_hit;
wire            read_cs;
wire            latch_en;
wire    [3:0]   update_vec_v;
wire            dtlb_ram_SO;

assign          update_vec_v    = {4{update_cs}} & update_vec;

// compare latch
always@ (posedge CLK)
    if  (SEi)    begin
        vpn         <= #U_DLY   {vpn[18:0], SIi};
        bypass_mode <= #U_DLY   {bypass_mode[0], vpn[19]};
        config_k0   <= #U_DLY   {config_k0[1:0], bypass_mode[1]};
        cp0_asid    <= #U_DLY   {cp0_asid[6:0],config_k0[2]};
        end        
    else if (COMP_ENi)   begin
        vpn         <= #U_DLY   VPN_INi;
        bypass_mode <= #U_DLY   BYPASS_MODE_INi;
        config_k0   <= #U_DLY   CONFIG_K0_INi;
        cp0_asid    <= #U_DLY   CP0_ASIDi;
        end

// update_latch
always@ (posedge CLK)
    if  (SEi)    begin
        update_vec  <= #U_DLY   {update_vec[2:0], cp0_asid[7]};
        update_data <= #U_DLY   {update_data[76:0], update_vec[3]};
        end
    else if (UPDATE_ENi) begin
        update_vec  <= #U_DLY   UPDATE_VECi;
        update_data <= #U_DLY   UPDATE_DATAi;
        end
        
// flush_latch
always@ (posedge CLK)
    if  (SEi)
        flush_vec   <= #U_DLY   {flush_vec[2:0], update_data[77]};
    else
        flush_vec   <= #U_DLY   FLUSH_VECi;

always@ (posedge CLK)
    if  (SEi)    begin
        comp_cs     <= #U_DLY   flush_vec[3];
        flush_cs    <= #U_DLY   comp_cs;
        update_cs   <= #U_DLY   flush_cs;
        end
    else    begin
        comp_cs     <= #U_DLY   COMP_ENi;
        flush_cs    <= #U_DLY   FLUSH_VECi[0] | FLUSH_VECi[1] | FLUSH_VECi[2] | FLUSH_VECi[3];
        update_cs   <= #U_DLY   UPDATE_ENi;
        end

// the read-out data for sense amplify.
assign          read_cs         = comp_cs | update_cs;
assign          latch_en        = comp_cs | update_cs | flush_cs | SCAN_MODEi;

// bypass logic
assign          bypass_hit      = bypass_mode[1] & ( vpn[19:18]==2'b10 ) | bypass_mode[0] & ( vpn[19:9]==11'h7f9 );
assign          bypass_pfn      = {({3{vpn[18]}} & vpn[19:17]), vpn[16:0]};
assign          bypass_cache    = ( vpn[19:17]==3'b100 ) ? config_k0 : 3'b010;

// output internal data
assign          ENTRY_PFNi       = bypass_hit ? bypass_pfn   : entry_pfn;
assign          ENTRY_CACHEi     = bypass_hit ? bypass_cache : entry_cache;
assign          ENTRY_DIRTYi     = bypass_hit | entry_dirty;
assign          HIT_VECi         = hit_vec;
assign          HIT_IN_DTLBi     = HIT_VEC[0]    | HIT_VEC[1]    | HIT_VEC[2]    | HIT_VEC[3]    | bypass_hit;

assign          SOi              = dtlb_ram_SO;

// output data
buf             (ENTRY_PFN[0],  ENTRY_PFNi[0]);
buf             (ENTRY_PFN[1],  ENTRY_PFNi[1]);
buf             (ENTRY_PFN[2],  ENTRY_PFNi[2]);
buf             (ENTRY_PFN[3],  ENTRY_PFNi[3]);
buf             (ENTRY_PFN[4],  ENTRY_PFNi[4]);
buf             (ENTRY_PFN[5],  ENTRY_PFNi[5]);
buf             (ENTRY_PFN[6],  ENTRY_PFNi[6]);
buf             (ENTRY_PFN[7],  ENTRY_PFNi[7]);
buf             (ENTRY_PFN[8],  ENTRY_PFNi[8]);
buf             (ENTRY_PFN[9],  ENTRY_PFNi[9]);
buf             (ENTRY_PFN[10],  ENTRY_PFNi[10]);
buf             (ENTRY_PFN[11],  ENTRY_PFNi[11]);
buf             (ENTRY_PFN[12],  ENTRY_PFNi[12]);
buf             (ENTRY_PFN[13],  ENTRY_PFNi[13]);
buf             (ENTRY_PFN[14],  ENTRY_PFNi[14]);
buf             (ENTRY_PFN[15],  ENTRY_PFNi[15]);
buf             (ENTRY_PFN[16],  ENTRY_PFNi[16]);
buf             (ENTRY_PFN[17],  ENTRY_PFNi[17]);
buf             (ENTRY_PFN[18],  ENTRY_PFNi[18]);
buf             (ENTRY_PFN[19],  ENTRY_PFNi[19]);

buf             (ENTRY_CACHE[0],ENTRY_CACHEi[0]);
buf             (ENTRY_CACHE[1],ENTRY_CACHEi[1]);
buf             (ENTRY_CACHE[2],ENTRY_CACHEi[2]);

buf             (ENTRY_DIRTY,   ENTRY_DIRTYi);
buf             (HIT_IN_DTLB,   HIT_IN_DTLBi);
buf             (HIT_VEC[0],    HIT_VECi[0]);
buf             (HIT_VEC[1],    HIT_VECi[1]);
buf             (HIT_VEC[2],    HIT_VECi[2]);
buf             (HIT_VEC[3],    HIT_VECi[3]);
buf             (SO,            SOi);

buf             (VPN_INi[0],    VPN_IN[0]);
buf             (VPN_INi[1],    VPN_IN[1]);
buf             (VPN_INi[2],    VPN_IN[2]);
buf             (VPN_INi[3],    VPN_IN[3]);
buf             (VPN_INi[4],    VPN_IN[4]);
buf             (VPN_INi[5],    VPN_IN[5]);
buf             (VPN_INi[6],    VPN_IN[6]);
buf             (VPN_INi[7],    VPN_IN[7]);
buf             (VPN_INi[8],    VPN_IN[8]);
buf             (VPN_INi[9],    VPN_IN[9]);
buf             (VPN_INi[10],   VPN_IN[10]);
buf             (VPN_INi[11],   VPN_IN[11]);
buf             (VPN_INi[12],   VPN_IN[12]);
buf             (VPN_INi[13],   VPN_IN[13]);
buf             (VPN_INi[14],   VPN_IN[14]);
buf             (VPN_INi[15],   VPN_IN[15]);
buf             (VPN_INi[16],   VPN_IN[16]);
buf             (VPN_INi[17],   VPN_IN[17]);
buf             (VPN_INi[18],   VPN_IN[18]);
buf             (VPN_INi[19],   VPN_IN[19]);

buf             (COMP_ENi,      COMP_EN);

buf             (FLUSH_VECi[0], FLUSH_VEC[0]);
buf             (FLUSH_VECi[1], FLUSH_VEC[1]);
buf             (FLUSH_VECi[2], FLUSH_VEC[2]);
buf             (FLUSH_VECi[3], FLUSH_VEC[3]);

buf             (UPDATE_DATAi[0],   UPDATE_DATA[0]);
buf             (UPDATE_DATAi[1],   UPDATE_DATA[1]);
buf             (UPDATE_DATAi[2],   UPDATE_DATA[2]);
buf             (UPDATE_DATAi[3],   UPDATE_DATA[3]);
buf             (UPDATE_DATAi[4],   UPDATE_DATA[4]);
buf             (UPDATE_DATAi[5],   UPDATE_DATA[5]);
buf             (UPDATE_DATAi[6],   UPDATE_DATA[6]);
buf             (UPDATE_DATAi[7],   UPDATE_DATA[7]);
buf             (UPDATE_DATAi[8],   UPDATE_DATA[8]);
buf             (UPDATE_DATAi[9],   UPDATE_DATA[9]);
buf             (UPDATE_DATAi[10],  UPDATE_DATA[10]);
buf             (UPDATE_DATAi[11],  UPDATE_DATA[11]);
buf             (UPDATE_DATAi[12],  UPDATE_DATA[12]);
buf             (UPDATE_DATAi[13],  UPDATE_DATA[13]);
buf             (UPDATE_DATAi[14],  UPDATE_DATA[14]);
buf             (UPDATE_DATAi[15],  UPDATE_DATA[15]);
buf             (UPDATE_DATAi[16],  UPDATE_DATA[16]);
buf             (UPDATE_DATAi[17],  UPDATE_DATA[17]);
buf             (UPDATE_DATAi[18],  UPDATE_DATA[18]);
buf             (UPDATE_DATAi[19],  UPDATE_DATA[19]);
buf             (UPDATE_DATAi[20],  UPDATE_DATA[20]);
buf             (UPDATE_DATAi[21],  UPDATE_DATA[21]);
buf             (UPDATE_DATAi[22],  UPDATE_DATA[22]);
buf             (UPDATE_DATAi[23],  UPDATE_DATA[23]);
buf             (UPDATE_DATAi[24],  UPDATE_DATA[24]);
buf             (UPDATE_DATAi[25],  UPDATE_DATA[25]);
buf             (UPDATE_DATAi[26],  UPDATE_DATA[26]);
buf             (UPDATE_DATAi[27],  UPDATE_DATA[27]);
buf             (UPDATE_DATAi[28],  UPDATE_DATA[28]);
buf             (UPDATE_DATAi[29],  UPDATE_DATA[29]);
buf             (UPDATE_DATAi[30],  UPDATE_DATA[30]);
buf             (UPDATE_DATAi[31],  UPDATE_DATA[31]);
buf             (UPDATE_DATAi[32],  UPDATE_DATA[32]);
buf             (UPDATE_DATAi[33],  UPDATE_DATA[33]);
buf             (UPDATE_DATAi[34],  UPDATE_DATA[34]);
buf             (UPDATE_DATAi[35],  UPDATE_DATA[35]);
buf             (UPDATE_DATAi[36],  UPDATE_DATA[36]);
buf             (UPDATE_DATAi[37],  UPDATE_DATA[37]);
buf             (UPDATE_DATAi[38],  UPDATE_DATA[38]);
buf             (UPDATE_DATAi[39],  UPDATE_DATA[39]);
buf             (UPDATE_DATAi[40],  UPDATE_DATA[40]);
buf             (UPDATE_DATAi[41],  UPDATE_DATA[41]);
buf             (UPDATE_DATAi[42],  UPDATE_DATA[42]);
buf             (UPDATE_DATAi[43],  UPDATE_DATA[43]);
buf             (UPDATE_DATAi[44],  UPDATE_DATA[44]);
buf             (UPDATE_DATAi[45],  UPDATE_DATA[45]);
buf             (UPDATE_DATAi[46],  UPDATE_DATA[46]);
buf             (UPDATE_DATAi[47],  UPDATE_DATA[47]);
buf             (UPDATE_DATAi[48],  UPDATE_DATA[48]);
buf             (UPDATE_DATAi[49],  UPDATE_DATA[49]);
buf             (UPDATE_DATAi[50],  UPDATE_DATA[50]);
buf             (UPDATE_DATAi[51],  UPDATE_DATA[51]);
buf             (UPDATE_DATAi[52],  UPDATE_DATA[52]);
buf             (UPDATE_DATAi[53],  UPDATE_DATA[53]);
buf             (UPDATE_DATAi[54],  UPDATE_DATA[54]);
buf             (UPDATE_DATAi[55],  UPDATE_DATA[55]);
buf             (UPDATE_DATAi[56],  UPDATE_DATA[56]);
buf             (UPDATE_DATAi[57],  UPDATE_DATA[57]);
buf             (UPDATE_DATAi[58],  UPDATE_DATA[58]);
buf             (UPDATE_DATAi[59],  UPDATE_DATA[59]);
buf             (UPDATE_DATAi[60],  UPDATE_DATA[60]);
buf             (UPDATE_DATAi[61],  UPDATE_DATA[61]);
buf             (UPDATE_DATAi[62],  UPDATE_DATA[62]);
buf             (UPDATE_DATAi[63],  UPDATE_DATA[63]);
buf             (UPDATE_DATAi[64],  UPDATE_DATA[64]);
buf             (UPDATE_DATAi[65],  UPDATE_DATA[65]);
buf             (UPDATE_DATAi[66],  UPDATE_DATA[66]);
buf             (UPDATE_DATAi[67],  UPDATE_DATA[67]);
buf             (UPDATE_DATAi[68],  UPDATE_DATA[68]);
buf             (UPDATE_DATAi[69],  UPDATE_DATA[69]);
buf             (UPDATE_DATAi[70],  UPDATE_DATA[70]);
buf             (UPDATE_DATAi[71],  UPDATE_DATA[71]);
buf             (UPDATE_DATAi[72],  UPDATE_DATA[72]);
buf             (UPDATE_DATAi[73],  UPDATE_DATA[73]);
buf             (UPDATE_DATAi[74],  UPDATE_DATA[74]);
buf             (UPDATE_DATAi[75],  UPDATE_DATA[75]);
buf             (UPDATE_DATAi[76],  UPDATE_DATA[76]);
buf             (UPDATE_DATAi[77],  UPDATE_DATA[77]);

buf             (UPDATE_ENi,    UPDATE_EN);

buf             (UPDATE_VECi[0],    UPDATE_VEC[0]);
buf             (UPDATE_VECi[1],    UPDATE_VEC[1]);
buf             (UPDATE_VECi[2],    UPDATE_VEC[2]);
buf             (UPDATE_VECi[3],    UPDATE_VEC[3]);

buf             (CP0_ASIDi[0],  CP0_ASID[0]);
buf             (CP0_ASIDi[1],  CP0_ASID[1]);
buf             (CP0_ASIDi[2],  CP0_ASID[2]);
buf             (CP0_ASIDi[3],  CP0_ASID[3]);
buf             (CP0_ASIDi[4],  CP0_ASID[4]);
buf             (CP0_ASIDi[5],  CP0_ASID[5]);
buf             (CP0_ASIDi[6],  CP0_ASID[6]);
buf             (CP0_ASIDi[7],  CP0_ASID[7]);

buf             (BYPASS_MODE_INi[0],    BYPASS_MODE_IN[0]);
buf             (BYPASS_MODE_INi[1],    BYPASS_MODE_IN[1]);

buf             (CONFIG_K0_INi[0],  CONFIG_K0_IN[0]);
buf             (CONFIG_K0_INi[1],  CONFIG_K0_IN[1]);
buf             (CONFIG_K0_INi[2],  CONFIG_K0_IN[2]);

buf             (SCAN_MODEi,    SCAN_MODE);
//buf             (FREi,          FRE);

buf             (SEi,           SE);
buf             (SIi,           SI);

// input timing checks
wire            cp_flag = COMP_ENi;
wire            up_flag = UPDATE_ENi;

reg             NOT_CLKH;
reg             NOT_CLKL;
reg             NOT_COMP_EN;
reg             NOT_UPDATE_EN;
reg             NOT_VPN_IN_0,  NOT_VPN_IN_1,  NOT_VPN_IN_2,  NOT_VPN_IN_3,  NOT_VPN_IN_4,
                NOT_VPN_IN_5,  NOT_VPN_IN_6,  NOT_VPN_IN_7,  NOT_VPN_IN_8,  NOT_VPN_IN_9,
                NOT_VPN_IN_10, NOT_VPN_IN_11, NOT_VPN_IN_12, NOT_VPN_IN_13, NOT_VPN_IN_14,
                NOT_VPN_IN_15, NOT_VPN_IN_16, NOT_VPN_IN_17, NOT_VPN_IN_18, NOT_VPN_IN_19;
reg             NOT_FLUSH_VEC_0,    NOT_FLUSH_VEC_1,    NOT_FLUSH_VEC_2,    NOT_FLUSH_VEC_3;
reg             NOT_UPDATE_DATA_0,  NOT_UPDATE_DATA_1,  NOT_UPDATE_DATA_2,  NOT_UPDATE_DATA_3,
                NOT_UPDATE_DATA_4,  NOT_UPDATE_DATA_5,  NOT_UPDATE_DATA_6,  NOT_UPDATE_DATA_7,
                NOT_UPDATE_DATA_8,  NOT_UPDATE_DATA_9,  NOT_UPDATE_DATA_10, NOT_UPDATE_DATA_11,
                NOT_UPDATE_DATA_12, NOT_UPDATE_DATA_13, NOT_UPDATE_DATA_14, NOT_UPDATE_DATA_15,
                NOT_UPDATE_DATA_16, NOT_UPDATE_DATA_17, NOT_UPDATE_DATA_18, NOT_UPDATE_DATA_19,
                NOT_UPDATE_DATA_20, NOT_UPDATE_DATA_21, NOT_UPDATE_DATA_22, NOT_UPDATE_DATA_23,
                NOT_UPDATE_DATA_24, NOT_UPDATE_DATA_25, NOT_UPDATE_DATA_26, NOT_UPDATE_DATA_27,
                NOT_UPDATE_DATA_28, NOT_UPDATE_DATA_29, NOT_UPDATE_DATA_30, NOT_UPDATE_DATA_31,
                NOT_UPDATE_DATA_32, NOT_UPDATE_DATA_33, NOT_UPDATE_DATA_34, NOT_UPDATE_DATA_35,
                NOT_UPDATE_DATA_36, NOT_UPDATE_DATA_37, NOT_UPDATE_DATA_38, NOT_UPDATE_DATA_39,
                NOT_UPDATE_DATA_40, NOT_UPDATE_DATA_41, NOT_UPDATE_DATA_42, NOT_UPDATE_DATA_43,
                NOT_UPDATE_DATA_44, NOT_UPDATE_DATA_45, NOT_UPDATE_DATA_46, NOT_UPDATE_DATA_47,
                NOT_UPDATE_DATA_48, NOT_UPDATE_DATA_49, NOT_UPDATE_DATA_50, NOT_UPDATE_DATA_51,
                NOT_UPDATE_DATA_52, NOT_UPDATE_DATA_53, NOT_UPDATE_DATA_54, NOT_UPDATE_DATA_55,
                NOT_UPDATE_DATA_56, NOT_UPDATE_DATA_57, NOT_UPDATE_DATA_58, NOT_UPDATE_DATA_59,
                NOT_UPDATE_DATA_60, NOT_UPDATE_DATA_61, NOT_UPDATE_DATA_62, NOT_UPDATE_DATA_63,
                NOT_UPDATE_DATA_64, NOT_UPDATE_DATA_65, NOT_UPDATE_DATA_66, NOT_UPDATE_DATA_67,
                NOT_UPDATE_DATA_68, NOT_UPDATE_DATA_69, NOT_UPDATE_DATA_70, NOT_UPDATE_DATA_71,
                NOT_UPDATE_DATA_72, NOT_UPDATE_DATA_73, NOT_UPDATE_DATA_74, NOT_UPDATE_DATA_75,
                NOT_UPDATE_DATA_76, NOT_UPDATE_DATA_77;
reg             NOT_UPDATE_VEC_0,   NOT_UPDATE_VEC_1,   NOT_UPDATE_VEC_2,   NOT_UPDATE_VEC_3;
reg             NOT_CP0_ASID_0,     NOT_CP0_ASID_1,     NOT_CP0_ASID_2,     NOT_CP0_ASID_3,
                NOT_CP0_ASID_4,     NOT_CP0_ASID_5,     NOT_CP0_ASID_6,     NOT_CP0_ASID_7;
reg             NOT_BYPASS_MODE_IN_0,   NOT_BYPASS_MODE_IN_1;
reg             NOT_CONFIG_K0_IN_0,     NOT_CONFIG_K0_IN_1,     NOT_CONFIG_K0_IN_2;
reg             NOT_SE;
reg             NOT_SI;
reg             NOT_SCAN_MODE;
//reg             NOT_FRE;
specify
        $width(posedge CLK, 1.000, 0, NOT_CLKH);
        $width(negedge CLK, 1.000, 0, NOT_CLKL);
        $setuphold(posedge CLK,             posedge COMP_EN,        1.000, 1.000, NOT_COMP_EN);
        $setuphold(posedge CLK,             negedge COMP_EN,        1.000, 1.000, NOT_COMP_EN);
        $setuphold(posedge CLK,             posedge UPDATE_EN,      1.000, 1.000, NOT_UPDATE_EN);
        $setuphold(posedge CLK,             negedge UPDATE_EN,      1.000, 1.000, NOT_UPDATE_EN);
        $setuphold(posedge CLK &&& cp_flag, posedge VPN_IN[0],      1.000, 1.000, NOT_VPN_IN_0);
        $setuphold(posedge CLK &&& cp_flag, negedge VPN_IN[0],      1.000, 1.000, NOT_VPN_IN_0);
        $setuphold(posedge CLK &&& cp_flag, posedge VPN_IN[1],      1.000, 1.000, NOT_VPN_IN_1);
        $setuphold(posedge CLK &&& cp_flag, negedge VPN_IN[1],      1.000, 1.000, NOT_VPN_IN_1);
        $setuphold(posedge CLK &&& cp_flag, posedge VPN_IN[2],      1.000, 1.000, NOT_VPN_IN_2);
        $setuphold(posedge CLK &&& cp_flag, negedge VPN_IN[2],      1.000, 1.000, NOT_VPN_IN_2);
        $setuphold(posedge CLK &&& cp_flag, posedge VPN_IN[3],      1.000, 1.000, NOT_VPN_IN_3);
        $setuphold(posedge CLK &&& cp_flag, negedge VPN_IN[3],      1.000, 1.000, NOT_VPN_IN_3);
        $setuphold(posedge CLK &&& cp_flag, posedge VPN_IN[4],      1.000, 1.000, NOT_VPN_IN_4);
        $setuphold(posedge CLK &&& cp_flag, negedge VPN_IN[4],      1.000, 1.000, NOT_VPN_IN_4);
        $setuphold(posedge CLK &&& cp_flag, posedge VPN_IN[5],      1.000, 1.000, NOT_VPN_IN_5);
        $setuphold(posedge CLK &&& cp_flag, negedge VPN_IN[5],      1.000, 1.000, NOT_VPN_IN_5);
        $setuphold(posedge CLK &&& cp_flag, posedge VPN_IN[6],      1.000, 1.000, NOT_VPN_IN_6);
        $setuphold(posedge CLK &&& cp_flag, negedge VPN_IN[6],      1.000, 1.000, NOT_VPN_IN_6);
        $setuphold(posedge CLK &&& cp_flag, posedge VPN_IN[7],      1.000, 1.000, NOT_VPN_IN_7);
        $setuphold(posedge CLK &&& cp_flag, negedge VPN_IN[7],      1.000, 1.000, NOT_VPN_IN_7);
        $setuphold(posedge CLK &&& cp_flag, posedge VPN_IN[8],      1.000, 1.000, NOT_VPN_IN_8);
        $setuphold(posedge CLK &&& cp_flag, negedge VPN_IN[8],      1.000, 1.000, NOT_VPN_IN_8);
        $setuphold(posedge CLK &&& cp_flag, posedge VPN_IN[9],      1.000, 1.000, NOT_VPN_IN_9);
        $setuphold(posedge CLK &&& cp_flag, negedge VPN_IN[9],      1.000, 1.000, NOT_VPN_IN_9);
        $setuphold(posedge CLK &&& cp_flag, posedge VPN_IN[10],     1.000, 1.000, NOT_VPN_IN_10);
        $setuphold(posedge CLK &&& cp_flag, negedge VPN_IN[10],     1.000, 1.000, NOT_VPN_IN_10);
        $setuphold(posedge CLK &&& cp_flag, posedge VPN_IN[11],     1.000, 1.000, NOT_VPN_IN_11);
        $setuphold(posedge CLK &&& cp_flag, negedge VPN_IN[11],     1.000, 1.000, NOT_VPN_IN_11);
        $setuphold(posedge CLK &&& cp_flag, posedge VPN_IN[12],     1.000, 1.000, NOT_VPN_IN_12);
        $setuphold(posedge CLK &&& cp_flag, negedge VPN_IN[12],     1.000, 1.000, NOT_VPN_IN_12);
        $setuphold(posedge CLK &&& cp_flag, posedge VPN_IN[13],     1.000, 1.000, NOT_VPN_IN_13);
        $setuphold(posedge CLK &&& cp_flag, negedge VPN_IN[13],     1.000, 1.000, NOT_VPN_IN_13);
        $setuphold(posedge CLK &&& cp_flag, posedge VPN_IN[14],     1.000, 1.000, NOT_VPN_IN_14);
        $setuphold(posedge CLK &&& cp_flag, negedge VPN_IN[14],     1.000, 1.000, NOT_VPN_IN_14);
        $setuphold(posedge CLK &&& cp_flag, posedge VPN_IN[15],     1.000, 1.000, NOT_VPN_IN_15);
        $setuphold(posedge CLK &&& cp_flag, negedge VPN_IN[15],     1.000, 1.000, NOT_VPN_IN_15);
        $setuphold(posedge CLK &&& cp_flag, posedge VPN_IN[16],     1.000, 1.000, NOT_VPN_IN_16);
        $setuphold(posedge CLK &&& cp_flag, negedge VPN_IN[16],     1.000, 1.000, NOT_VPN_IN_16);
        $setuphold(posedge CLK &&& cp_flag, posedge VPN_IN[17],     1.000, 1.000, NOT_VPN_IN_17);
        $setuphold(posedge CLK &&& cp_flag, negedge VPN_IN[17],     1.000, 1.000, NOT_VPN_IN_17);
        $setuphold(posedge CLK &&& cp_flag, posedge VPN_IN[18],     1.000, 1.000, NOT_VPN_IN_18);
        $setuphold(posedge CLK &&& cp_flag, negedge VPN_IN[18],     1.000, 1.000, NOT_VPN_IN_18);
        $setuphold(posedge CLK &&& cp_flag, posedge VPN_IN[19],     1.000, 1.000, NOT_VPN_IN_19);
        $setuphold(posedge CLK &&& cp_flag, negedge VPN_IN[19],     1.000, 1.000, NOT_VPN_IN_19);
        $setuphold(posedge CLK,             posedge FLUSH_VEC[0],   1.000, 1.000, NOT_FLUSH_VEC_0);
        $setuphold(posedge CLK,             negedge FLUSH_VEC[0],   1.000, 1.000, NOT_FLUSH_VEC_0);
        $setuphold(posedge CLK,             posedge FLUSH_VEC[1],   1.000, 1.000, NOT_FLUSH_VEC_1);
        $setuphold(posedge CLK,             negedge FLUSH_VEC[1],   1.000, 1.000, NOT_FLUSH_VEC_1);
        $setuphold(posedge CLK,             posedge FLUSH_VEC[2],   1.000, 1.000, NOT_FLUSH_VEC_2);
        $setuphold(posedge CLK,             negedge FLUSH_VEC[2],   1.000, 1.000, NOT_FLUSH_VEC_2);
        $setuphold(posedge CLK,             posedge FLUSH_VEC[3],   1.000, 1.000, NOT_FLUSH_VEC_3);
        $setuphold(posedge CLK,             negedge FLUSH_VEC[3],   1.000, 1.000, NOT_FLUSH_VEC_3);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[0], 1.000, 1.000, NOT_UPDATE_DATA_0);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[0], 1.000, 1.000, NOT_UPDATE_DATA_0);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[1], 1.000, 1.000, NOT_UPDATE_DATA_1);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[1], 1.000, 1.000, NOT_UPDATE_DATA_1);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[2], 1.000, 1.000, NOT_UPDATE_DATA_2);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[2], 1.000, 1.000, NOT_UPDATE_DATA_2);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[3], 1.000, 1.000, NOT_UPDATE_DATA_3);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[3], 1.000, 1.000, NOT_UPDATE_DATA_3);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[4], 1.000, 1.000, NOT_UPDATE_DATA_4);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[4], 1.000, 1.000, NOT_UPDATE_DATA_4);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[5], 1.000, 1.000, NOT_UPDATE_DATA_5);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[5], 1.000, 1.000, NOT_UPDATE_DATA_5);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[6], 1.000, 1.000, NOT_UPDATE_DATA_6);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[6], 1.000, 1.000, NOT_UPDATE_DATA_6);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[7], 1.000, 1.000, NOT_UPDATE_DATA_7);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[7], 1.000, 1.000, NOT_UPDATE_DATA_7);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[8], 1.000, 1.000, NOT_UPDATE_DATA_8);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[8], 1.000, 1.000, NOT_UPDATE_DATA_8);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[9], 1.000, 1.000, NOT_UPDATE_DATA_9);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[9], 1.000, 1.000, NOT_UPDATE_DATA_9);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[10],1.000, 1.000, NOT_UPDATE_DATA_10);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[10],1.000, 1.000, NOT_UPDATE_DATA_10);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[11],1.000, 1.000, NOT_UPDATE_DATA_11);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[11],1.000, 1.000, NOT_UPDATE_DATA_11);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[12],1.000, 1.000, NOT_UPDATE_DATA_12);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[12],1.000, 1.000, NOT_UPDATE_DATA_12);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[13],1.000, 1.000, NOT_UPDATE_DATA_13);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[13],1.000, 1.000, NOT_UPDATE_DATA_13);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[14],1.000, 1.000, NOT_UPDATE_DATA_14);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[14],1.000, 1.000, NOT_UPDATE_DATA_14);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[15],1.000, 1.000, NOT_UPDATE_DATA_15);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[15],1.000, 1.000, NOT_UPDATE_DATA_15);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[16],1.000, 1.000, NOT_UPDATE_DATA_16);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[16],1.000, 1.000, NOT_UPDATE_DATA_16);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[17],1.000, 1.000, NOT_UPDATE_DATA_17);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[17],1.000, 1.000, NOT_UPDATE_DATA_17);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[18],1.000, 1.000, NOT_UPDATE_DATA_18);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[18],1.000, 1.000, NOT_UPDATE_DATA_18);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[19],1.000, 1.000, NOT_UPDATE_DATA_19);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[19],1.000, 1.000, NOT_UPDATE_DATA_19);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[20],1.000, 1.000, NOT_UPDATE_DATA_20);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[20],1.000, 1.000, NOT_UPDATE_DATA_20);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[21],1.000, 1.000, NOT_UPDATE_DATA_21);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[21],1.000, 1.000, NOT_UPDATE_DATA_21);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[22],1.000, 1.000, NOT_UPDATE_DATA_22);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[22],1.000, 1.000, NOT_UPDATE_DATA_22);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[23],1.000, 1.000, NOT_UPDATE_DATA_23);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[23],1.000, 1.000, NOT_UPDATE_DATA_23);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[24],1.000, 1.000, NOT_UPDATE_DATA_24);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[24],1.000, 1.000, NOT_UPDATE_DATA_24);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[25],1.000, 1.000, NOT_UPDATE_DATA_25);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[25],1.000, 1.000, NOT_UPDATE_DATA_25);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[26],1.000, 1.000, NOT_UPDATE_DATA_26);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[26],1.000, 1.000, NOT_UPDATE_DATA_26);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[27],1.000, 1.000, NOT_UPDATE_DATA_27);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[27],1.000, 1.000, NOT_UPDATE_DATA_27);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[28],1.000, 1.000, NOT_UPDATE_DATA_28);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[28],1.000, 1.000, NOT_UPDATE_DATA_28);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[29],1.000, 1.000, NOT_UPDATE_DATA_29);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[29],1.000, 1.000, NOT_UPDATE_DATA_29);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[30],1.000, 1.000, NOT_UPDATE_DATA_30);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[30],1.000, 1.000, NOT_UPDATE_DATA_30);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[31],1.000, 1.000, NOT_UPDATE_DATA_31);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[31],1.000, 1.000, NOT_UPDATE_DATA_31);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[32],1.000, 1.000, NOT_UPDATE_DATA_32);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[32],1.000, 1.000, NOT_UPDATE_DATA_32);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[33],1.000, 1.000, NOT_UPDATE_DATA_33);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[33],1.000, 1.000, NOT_UPDATE_DATA_33);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[34],1.000, 1.000, NOT_UPDATE_DATA_34);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[34],1.000, 1.000, NOT_UPDATE_DATA_34);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[35],1.000, 1.000, NOT_UPDATE_DATA_35);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[35],1.000, 1.000, NOT_UPDATE_DATA_35);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[36],1.000, 1.000, NOT_UPDATE_DATA_36);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[36],1.000, 1.000, NOT_UPDATE_DATA_36);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[37],1.000, 1.000, NOT_UPDATE_DATA_37);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[37],1.000, 1.000, NOT_UPDATE_DATA_37);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[38],1.000, 1.000, NOT_UPDATE_DATA_38);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[38],1.000, 1.000, NOT_UPDATE_DATA_38);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[39],1.000, 1.000, NOT_UPDATE_DATA_39);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[39],1.000, 1.000, NOT_UPDATE_DATA_39);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[40],1.000, 1.000, NOT_UPDATE_DATA_40);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[40],1.000, 1.000, NOT_UPDATE_DATA_40);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[41],1.000, 1.000, NOT_UPDATE_DATA_41);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[41],1.000, 1.000, NOT_UPDATE_DATA_41);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[42],1.000, 1.000, NOT_UPDATE_DATA_42);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[42],1.000, 1.000, NOT_UPDATE_DATA_42);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[43],1.000, 1.000, NOT_UPDATE_DATA_43);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[43],1.000, 1.000, NOT_UPDATE_DATA_43);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[44],1.000, 1.000, NOT_UPDATE_DATA_44);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[44],1.000, 1.000, NOT_UPDATE_DATA_44);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[45],1.000, 1.000, NOT_UPDATE_DATA_45);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[45],1.000, 1.000, NOT_UPDATE_DATA_45);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[46],1.000, 1.000, NOT_UPDATE_DATA_46);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[46],1.000, 1.000, NOT_UPDATE_DATA_46);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[47],1.000, 1.000, NOT_UPDATE_DATA_47);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[47],1.000, 1.000, NOT_UPDATE_DATA_47);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[48],1.000, 1.000, NOT_UPDATE_DATA_48);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[48],1.000, 1.000, NOT_UPDATE_DATA_48);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[49],1.000, 1.000, NOT_UPDATE_DATA_49);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[49],1.000, 1.000, NOT_UPDATE_DATA_49);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[50],1.000, 1.000, NOT_UPDATE_DATA_50);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[50],1.000, 1.000, NOT_UPDATE_DATA_50);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[51],1.000, 1.000, NOT_UPDATE_DATA_51);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[51],1.000, 1.000, NOT_UPDATE_DATA_51);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[52],1.000, 1.000, NOT_UPDATE_DATA_52);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[52],1.000, 1.000, NOT_UPDATE_DATA_52);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[53],1.000, 1.000, NOT_UPDATE_DATA_53);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[53],1.000, 1.000, NOT_UPDATE_DATA_53);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[54],1.000, 1.000, NOT_UPDATE_DATA_54);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[54],1.000, 1.000, NOT_UPDATE_DATA_54);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[55],1.000, 1.000, NOT_UPDATE_DATA_55);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[55],1.000, 1.000, NOT_UPDATE_DATA_55);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[56],1.000, 1.000, NOT_UPDATE_DATA_56);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[56],1.000, 1.000, NOT_UPDATE_DATA_56);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[57],1.000, 1.000, NOT_UPDATE_DATA_57);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[57],1.000, 1.000, NOT_UPDATE_DATA_57);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[58],1.000, 1.000, NOT_UPDATE_DATA_58);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[58],1.000, 1.000, NOT_UPDATE_DATA_58);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[59],1.000, 1.000, NOT_UPDATE_DATA_59);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[59],1.000, 1.000, NOT_UPDATE_DATA_59);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[60],1.000, 1.000, NOT_UPDATE_DATA_60);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[60],1.000, 1.000, NOT_UPDATE_DATA_60);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[61],1.000, 1.000, NOT_UPDATE_DATA_61);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[61],1.000, 1.000, NOT_UPDATE_DATA_61);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[62],1.000, 1.000, NOT_UPDATE_DATA_62);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[62],1.000, 1.000, NOT_UPDATE_DATA_62);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[63],1.000, 1.000, NOT_UPDATE_DATA_63);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[63],1.000, 1.000, NOT_UPDATE_DATA_63);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[64],1.000, 1.000, NOT_UPDATE_DATA_64);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[64],1.000, 1.000, NOT_UPDATE_DATA_64);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[65],1.000, 1.000, NOT_UPDATE_DATA_65);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[65],1.000, 1.000, NOT_UPDATE_DATA_65);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[66],1.000, 1.000, NOT_UPDATE_DATA_66);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[66],1.000, 1.000, NOT_UPDATE_DATA_66);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[67],1.000, 1.000, NOT_UPDATE_DATA_67);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[67],1.000, 1.000, NOT_UPDATE_DATA_67);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[68],1.000, 1.000, NOT_UPDATE_DATA_68);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[68],1.000, 1.000, NOT_UPDATE_DATA_68);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[69],1.000, 1.000, NOT_UPDATE_DATA_69);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[69],1.000, 1.000, NOT_UPDATE_DATA_69);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[70],1.000, 1.000, NOT_UPDATE_DATA_70);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[70],1.000, 1.000, NOT_UPDATE_DATA_70);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[71],1.000, 1.000, NOT_UPDATE_DATA_71);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[71],1.000, 1.000, NOT_UPDATE_DATA_71);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[72],1.000, 1.000, NOT_UPDATE_DATA_72);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[72],1.000, 1.000, NOT_UPDATE_DATA_72);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[73],1.000, 1.000, NOT_UPDATE_DATA_73);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[73],1.000, 1.000, NOT_UPDATE_DATA_73);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[74],1.000, 1.000, NOT_UPDATE_DATA_74);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[74],1.000, 1.000, NOT_UPDATE_DATA_74);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[75],1.000, 1.000, NOT_UPDATE_DATA_75);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[75],1.000, 1.000, NOT_UPDATE_DATA_75);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[76],1.000, 1.000, NOT_UPDATE_DATA_76);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[76],1.000, 1.000, NOT_UPDATE_DATA_76);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_DATA[77],1.000, 1.000, NOT_UPDATE_DATA_77);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_DATA[77],1.000, 1.000, NOT_UPDATE_DATA_77);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_VEC[0],  1.000, 1.000, NOT_UPDATE_VEC_0);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_VEC[0],  1.000, 1.000, NOT_UPDATE_VEC_0);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_VEC[1],  1.000, 1.000, NOT_UPDATE_VEC_1);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_VEC[1],  1.000, 1.000, NOT_UPDATE_VEC_1);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_VEC[2],  1.000, 1.000, NOT_UPDATE_VEC_2);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_VEC[2],  1.000, 1.000, NOT_UPDATE_VEC_2);
        $setuphold(posedge CLK &&& up_flag, posedge UPDATE_VEC[3],  1.000, 1.000, NOT_UPDATE_VEC_3);
        $setuphold(posedge CLK &&& up_flag, negedge UPDATE_VEC[3],  1.000, 1.000, NOT_UPDATE_VEC_3);
        $setuphold(posedge CLK &&& cp_flag, posedge CP0_ASID[0],    1.000, 1.000, NOT_CP0_ASID_0);
        $setuphold(posedge CLK &&& cp_flag, negedge CP0_ASID[0],    1.000, 1.000, NOT_CP0_ASID_0);
        $setuphold(posedge CLK &&& cp_flag, posedge CP0_ASID[1],    1.000, 1.000, NOT_CP0_ASID_1);
        $setuphold(posedge CLK &&& cp_flag, negedge CP0_ASID[1],    1.000, 1.000, NOT_CP0_ASID_1);
        $setuphold(posedge CLK &&& cp_flag, posedge CP0_ASID[2],    1.000, 1.000, NOT_CP0_ASID_2);
        $setuphold(posedge CLK &&& cp_flag, negedge CP0_ASID[2],    1.000, 1.000, NOT_CP0_ASID_2);
        $setuphold(posedge CLK &&& cp_flag, posedge CP0_ASID[3],    1.000, 1.000, NOT_CP0_ASID_3);
        $setuphold(posedge CLK &&& cp_flag, negedge CP0_ASID[3],    1.000, 1.000, NOT_CP0_ASID_3);
        $setuphold(posedge CLK &&& cp_flag, posedge CP0_ASID[4],    1.000, 1.000, NOT_CP0_ASID_4);
        $setuphold(posedge CLK &&& cp_flag, negedge CP0_ASID[4],    1.000, 1.000, NOT_CP0_ASID_4);
        $setuphold(posedge CLK &&& cp_flag, posedge CP0_ASID[5],    1.000, 1.000, NOT_CP0_ASID_5);
        $setuphold(posedge CLK &&& cp_flag, negedge CP0_ASID[5],    1.000, 1.000, NOT_CP0_ASID_5);
        $setuphold(posedge CLK &&& cp_flag, posedge CP0_ASID[6],    1.000, 1.000, NOT_CP0_ASID_6);
        $setuphold(posedge CLK &&& cp_flag, negedge CP0_ASID[6],    1.000, 1.000, NOT_CP0_ASID_6);
        $setuphold(posedge CLK &&& cp_flag, posedge CP0_ASID[7],    1.000, 1.000, NOT_CP0_ASID_7);
        $setuphold(posedge CLK &&& cp_flag, negedge CP0_ASID[7],    1.000, 1.000, NOT_CP0_ASID_7);
        $setuphold(posedge CLK &&& cp_flag, posedge BYPASS_MODE_IN[0],  1.000, 1.000, NOT_BYPASS_MODE_IN_0);
        $setuphold(posedge CLK &&& cp_flag, negedge BYPASS_MODE_IN[0],  1.000, 1.000, NOT_BYPASS_MODE_IN_0);
        $setuphold(posedge CLK &&& cp_flag, posedge BYPASS_MODE_IN[1],  1.000, 1.000, NOT_BYPASS_MODE_IN_1);
        $setuphold(posedge CLK &&& cp_flag, negedge BYPASS_MODE_IN[1],  1.000, 1.000, NOT_BYPASS_MODE_IN_1);
        $setuphold(posedge CLK &&& cp_flag, posedge CONFIG_K0_IN[0],1.000, 1.000, NOT_CONFIG_K0_IN_0);
        $setuphold(posedge CLK &&& cp_flag, negedge CONFIG_K0_IN[0],1.000, 1.000, NOT_CONFIG_K0_IN_0);
        $setuphold(posedge CLK &&& cp_flag, posedge CONFIG_K0_IN[1],1.000, 1.000, NOT_CONFIG_K0_IN_1);
        $setuphold(posedge CLK &&& cp_flag, negedge CONFIG_K0_IN[1],1.000, 1.000, NOT_CONFIG_K0_IN_1);
        $setuphold(posedge CLK &&& cp_flag, posedge CONFIG_K0_IN[2],1.000, 1.000, NOT_CONFIG_K0_IN_2);
        $setuphold(posedge CLK &&& cp_flag, negedge CONFIG_K0_IN[2],1.000, 1.000, NOT_CONFIG_K0_IN_2);
        $setuphold(posedge CLK,             posedge SE,             1.000, 1.000, NOT_SE);
        $setuphold(posedge CLK,             negedge SE,             1.000, 1.000, NOT_SE);
        $setuphold(posedge CLK,             posedge SI,             1.000, 1.000, NOT_SI);
        $setuphold(posedge CLK,             negedge SI,             1.000, 1.000, NOT_SI);
        $setuphold(posedge CLK,             SCAN_MODE,      1.000, 1.000, NOT_SCAN_MODE);
        //$setuphold(posedge CLK,             FRE,            1.000, 1.000, NOT_FRE);

        (CLK => ENTRY_PFN[0])   = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_PFN[1])   = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_PFN[2])   = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_PFN[3])   = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_PFN[4])   = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_PFN[5])   = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_PFN[6])   = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_PFN[7])   = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_PFN[8])   = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_PFN[9])   = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_PFN[10])  = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_PFN[11])  = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_PFN[12])  = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_PFN[13])  = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_PFN[14])  = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_PFN[15])  = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_PFN[16])  = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_PFN[17])  = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_PFN[18])  = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_PFN[19])  = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_CACHE[0]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_CACHE[1]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_CACHE[2]) = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => ENTRY_DIRTY)    = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => HIT_IN_DTLB)    = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => HIT_VEC[0])     = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => HIT_VEC[1])     = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => HIT_VEC[2])     = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => HIT_VEC[3])     = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
        (CLK => SO)             = (1.000, 1.000, 1.000, 1.000, 1.000, 1.000);
endspecify

dtlb_cam    dtlb_cam    (
    .hit_vec                    ( hit_vec                   ),

    .vpn                        ( vpn                       ),
    .cam_update                 ( update_data[77:48]        ),
    .update_vec                 ( update_vec_v              ),
    .flush_vec                  ( flush_vec                 ),
    .cp0_asid                   ( cp0_asid                  ),
    .latch_en                   ( latch_en                  ),

    .SE                         ( SEi                       ),
    .SI                         ( update_cs                 ),
    .SO                         ( dtlb_cam_SO               ),

    .clk                        ( CLK                       ));

dtlb_ram    dtlb_ram    (
    .entry_pfn                  ( entry_pfn                 ),
    .entry_cache                ( entry_cache               ),
    .entry_dirty                ( entry_dirty               ),

    .ram_update                 ( update_data[47:0]         ),
    .update_vec                 ( update_vec_v              ),
    .hit_vec                    ( hit_vec                   ),
    .vpn_0                      ( vpn[0]                    ),
    .read_cs                    ( read_cs                   ),
    .latch_en                   ( latch_en                  ),
    
    .SE                         ( SEi                       ),
    .SI                         ( dtlb_cam_SO               ),
    .SO                         ( dtlb_ram_SO               ),

    .clk                        ( CLK                       ));

endmodule
`endcelldefine
module  dtlb_cam    (
    // output
    hit_vec,

    // input
    vpn,
    cam_update,
    update_vec,
    flush_vec,
    cp0_asid,
    latch_en,

    SE,
    SI,
    SO,

    clk
                    );

output  [3:0]   hit_vec;
input   [19:0]  vpn;
input   [29:0]  cam_update;
input   [3:0]   update_vec;
input   [3:0]   flush_vec;
input   [7:0]   cp0_asid;
input           latch_en;

input           SE;
input           SI;
output          SO;

input           clk;

wire    [3:0]   hit_vec_tmp;

reg     [3:0]   hit_vec;

`ifdef  FPGA
always@ (hit_vec_tmp or update_vec)
        hit_vec     = hit_vec_tmp | update_vec;
`else
always@ (hit_vec_tmp or update_vec or latch_en)
    if  (latch_en)
        hit_vec     = hit_vec_tmp | update_vec;
`endif

dtlb_cam_entry  dtlb_cam_entry0 (
    .entry_hit                  ( hit_vec_tmp[0]            ),
    .vpn                        ( vpn                       ),
    .cam_update                 ( cam_update                ),
    .update_en                  ( update_vec[0]             ),
    .flush_entry                ( flush_vec[0]              ),
    .cp0_asid                   ( cp0_asid                  ),
    .SE                         ( SE                        ),
    .SI                         ( SI                        ),
    .SO                         ( entry0_SO                 ),
    .clk                        ( clk                       ));

dtlb_cam_entry  dtlb_cam_entry1 (
    .entry_hit                  ( hit_vec_tmp[1]            ),
    .vpn                        ( vpn                       ),
    .cam_update                 ( cam_update                ),
    .update_en                  ( update_vec[1]             ),
    .flush_entry                ( flush_vec[1]              ),
    .cp0_asid                   ( cp0_asid                  ),
    .SE                         ( SE                        ),
    .SI                         ( entry0_SO                 ),
    .SO                         ( entry1_SO                 ),
    .clk                        ( clk                       ));

dtlb_cam_entry  dtlb_cam_entry2 (
    .entry_hit                  ( hit_vec_tmp[2]            ),
    .vpn                        ( vpn                       ),
    .cam_update                 ( cam_update                ),
    .update_en                  ( update_vec[2]             ),
    .flush_entry                ( flush_vec[2]              ),
    .cp0_asid                   ( cp0_asid                  ),
    .SE                         ( SE                        ),
    .SI                         ( entry1_SO                 ),
    .SO                         ( entry2_SO                 ),
    .clk                        ( clk                       ));

dtlb_cam_entry  dtlb_cam_entry3 (
    .entry_hit                  ( hit_vec_tmp[3]            ),
    .vpn                        ( vpn                       ),
    .cam_update                 ( cam_update                ),
    .update_en                  ( update_vec[3]             ),
    .flush_entry                ( flush_vec[3]              ),
    .cp0_asid                   ( cp0_asid                  ),
    .SE                         ( SE                        ),
    .SI                         ( entry2_SO                 ),
    .SO                         ( SO                        ),
    .clk                        ( clk                       ));

endmodule


    module  dtlb_cam_entry  (
    entry_hit,
    vpn,
    cam_update,
    update_en,
    flush_entry,
    cp0_asid,
    SE,
    SI,
    SO,
    clk
                        );

output          entry_hit;
input   [19:0]  vpn;
input   [29:0]  cam_update;
input           update_en;
input           flush_entry;
input   [7:0]   cp0_asid;

input           SE;
input           SI;
output          SO;

input           clk;

parameter       U_DLY   = 1;
reg     [18:0]  entry_vpn2;
reg     [7:0]   entry_asid;
reg             entry_g;
reg             entry_v0;
reg             entry_v1;

wire            asid_match;
wire            vpn_match;
wire            entry_valid;
wire            entry_hit;
wire            SO = entry_v1;

always@ (posedge clk)
    if  (SE)
        entry_vpn2  <= #U_DLY   {entry_vpn2[17:0], SI};
    else if (update_en)
        entry_vpn2  <= #U_DLY   cam_update[29:11];

always@ (posedge clk)
    if  (SE)
        entry_asid  <= #U_DLY   {entry_asid[6:0], entry_vpn2[18]};
    else if (update_en)
        entry_asid  <= #U_DLY   cam_update[10:3];

always@ (posedge clk)
    if  (SE)
        entry_g     <= #U_DLY   entry_asid[7];
    else if (update_en)
        entry_g     <= #U_DLY   cam_update[2];

always@ (posedge clk)
    if  (SE)
        entry_v0    <= #U_DLY   entry_g;
    else if (update_en | flush_entry)
        entry_v0    <= #U_DLY   cam_update[1] & ~flush_entry;

always@ (posedge clk)
    if  (SE)
        entry_v1    <= #U_DLY   entry_v0;
    else if (update_en | flush_entry)
        entry_v1    <= #U_DLY   cam_update[0] & ~flush_entry;

assign  asid_match  = entry_asid == cp0_asid | entry_g;
assign  vpn_match   = entry_vpn2 == vpn[19:1];
assign  entry_valid = vpn[0] ? entry_v1 : entry_v0;

assign  entry_hit   = asid_match & vpn_match & entry_valid;

endmodule

module  dtlb_ram    (
    entry_pfn,
    entry_cache,
    entry_dirty,

    ram_update,
    update_vec,
    hit_vec,
    vpn_0,
    read_cs,
    latch_en,

    SE,
    SI,
    SO,

    clk
                    );

output  [19:0]  entry_pfn;
output  [2:0]   entry_cache;
output          entry_dirty;

input   [47:0]  ram_update;
input   [3:0]   update_vec;
input   [3:0]   hit_vec;
input           vpn_0;
input           read_cs;
input           latch_en;

input           SE;
input           SI;
output          SO;
input           clk;

reg     [47:0]  ram_data_latch;
wire    [19:0]  entry_pfn;
wire    [2:0]   entry_cache;
wire            entry_dirty;
wire    [47:0]  ram_data_0, ram_data_1, ram_data_2, ram_data_3;
wire    [47:0]  ram_data;

`ifdef  FPGA
// sense amplifier with latch
always@ (ram_data_0 or ram_data_1 or ram_data_2 or ram_data_3)
        ram_data_latch  = ram_data_0 | ram_data_1 | ram_data_2 | ram_data_3;
`else
// sense amplifier with latch
always@ (ram_data_0 or ram_data_1 or ram_data_2 or ram_data_3 or latch_en)
    if  (latch_en)
        ram_data_latch  = ram_data_0 | ram_data_1 | ram_data_2 | ram_data_3;
`endif

assign  entry_pfn   = vpn_0 ? ram_data_latch[27:8] : ram_data_latch[47:28];
assign  entry_cache = vpn_0 ? ram_data_latch[4:2]  : ram_data_latch[7:5];
assign  entry_dirty = vpn_0 ? ram_data_latch[0]    : ram_data_latch[1];

dtlb_ram_entry  dtlb_ram_entry0 (
    .ram_data                   ( ram_data_0                ),

    .ram_update                 ( ram_update                ),
    .update_en                  ( update_vec[0]             ),
    .hit                        ( hit_vec[0]                ),
    .read_cs                    ( read_cs                   ),
    .SE                         ( SE                        ),
    .SI                         ( SI                        ),
    .SO                         ( entry0_SO                 ),
    .clk                        ( clk                       ));

dtlb_ram_entry  dtlb_ram_entry1 (
    .ram_data                   ( ram_data_1                ),

    .ram_update                 ( ram_update                ),
    .update_en                  ( update_vec[1]             ),
    .hit                        ( hit_vec[1]                ),
    .read_cs                    ( read_cs                   ),
    .SE                         ( SE                        ),
    .SI                         ( entry0_SO                 ),
    .SO                         ( entry1_SO                 ),
    .clk                        ( clk                       ));

dtlb_ram_entry  dtlb_ram_entry2 (
    .ram_data                   ( ram_data_2                ),

    .ram_update                 ( ram_update                ),
    .update_en                  ( update_vec[2]             ),
    .hit                        ( hit_vec[2]                ),
    .read_cs                    ( read_cs                   ),
    .SE                         ( SE                        ),
    .SI                         ( entry1_SO                 ),
    .SO                         ( entry2_SO                 ),
    .clk                        ( clk                       ));

dtlb_ram_entry  dtlb_ram_entry3 (
    .ram_data                   ( ram_data_3                ),

    .ram_update                 ( ram_update                ),
    .update_en                  ( update_vec[3]             ),
    .read_cs                    ( read_cs                   ),
    .hit                        ( hit_vec[3]                ),
    .SE                         ( SE                        ),
    .SI                         ( entry2_SO                 ),
    .SO                         ( SO                        ),
    .clk                        ( clk                       ));

endmodule

module  dtlb_ram_entry  (
    ram_data,

    ram_update,
    update_en,
    hit,
    read_cs,
    SE,
    SI,
    SO,
    clk
                        );

output  [47:0]  ram_data;

input   [47:0]  ram_update;
input           update_en;
input           hit;
input           read_cs;
input           SE;
input           SI;
output          SO;
input           clk;

parameter       U_DLY   = 1;

reg     [47:0]  entry_data;
wire    [47:0]  ram_data;
wire            SO;

always@ (posedge clk)
    if  (SE)
        entry_data    <= #U_DLY {entry_data[46:0], SI};
    else if (update_en)
        entry_data    <= #U_DLY ram_update;

`ifdef  FPGA
assign  ram_data    = update_en ? ram_update : (entry_data & {48{hit}});
`else
assign  ram_data    = update_en ? ram_update : ((read_cs & hit) ? entry_data : 48'b0);
`endif
assign  SO          = entry_data[47];

endmodule
