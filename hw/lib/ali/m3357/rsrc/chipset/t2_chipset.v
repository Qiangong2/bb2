/************************************************************************
//      Description:    t2_chipset of m3357,integrate all the Application IP
//                      core here:
//                      Host brdg, South brdg, p_biu(pci host,USB,IDE), audio
//                      DSP, Video, Display, GPU, SDRAM controler, eeprom,
//                      chipset_mux
//      Author:         many
*************************************************************************/
module t2_chipset (

/////////////////////////////
//      x_rd_last,
//      x_wr_last,
        x_ready,
        x_data_in,
//      x_bus_idle,
        x_bus_error,    // To t2core
        x_ack,          // to t2core clear u_request

        u_request,
        u_write,
        u_wordsize,
        u_startpaddr,
        u_data_to_send,
        u_bytemask,

//      g_config_ep,
//        g_reset_,       // From t2core

        x_err_int,
        x_ext_int,

//        cpu_testout,

// external controller

// SDRAM & FLASH
        ram_addr,       // SDRAM address pins & ROM partial address pins
        ram_ba,         // SDRAM bank address
        ram_dq_out,     // SDRAM 32 bits output data pins
        ram_dq_in,      // SDRAM 32 bit input data bus(latch-up),
        ram_dq_oe_,     // Data Port Output Enable
        ram_dqm,        // SDRAM data mask signal
        ram_cs_,        // SDRAM chip select
        ram_cas_,       // SDRAM cas#
        ram_we_,        // SDRAM we#
        ram_ras_,       // SDRAM ras#
        ram_cke,        // SDRAM clock enable

// EEPROM
        eeprom_addr,
        eeprom_rdata,
        eeprom_ce_,
        eeprom_oe_,
        eeprom_we_,
        eeprom_wdata,
        rom_data_oe_,
        rom_addr_oe,
        rom_en_,                 //add by yuky, 20020613, for rom operation
// LED
//      ledpin,

//PCI
        oe_ad_,
        out_ad,
        oe_cbe_,
        out_cbe_,
        oe_frame_,
        out_frame_,
        oe_irdy_,
        out_irdy_,
        oe_devsel_,
        out_devsel_,
        oe_trdy_,
        out_trdy_,
        oe_stop_,
        out_stop_,
        oe_par_,
        out_par,
        out_gnt_,       //only one gnt_ output in m6304
        in_req_,        //only one requst input in m6304
        in_ad,
        in_cbe_,
        in_frame_,
        in_irdy_,
        in_devsel_,
        in_trdy_,
        in_stop_,
        in_par,

        ext_int,        // external int, only one int input in m6304
        gpio_out,       // only use 23 gpio in m6304
        gpio_in,
        gpio_oe_,

// -------------------------------------------------------------------------- //
// Audio pads
        //i2s
        out_i2so_data0,
        out_i2so_data1,
        out_i2so_data2,
        out_i2so_data3,
        out_i2so_lrck,
        out_i2so_bick,
        in_i2si_data,
 //       in_i2si_lrck,   //no use in m6304
 //       in_i2si_bick,   //no use in m6304
        in_i2so_mclk,

//ADC
		iadc_v1p2u ,
		iadc_vs    ,
		iadc_vd    ,
		iadc_vin_l ,
		iadc_vin_r ,

        iadc_mclk,
		iadc_smtsen,
		iadc_smtsmode,
		iadc_smts_err,
		iadc_smts_end,
		iadc_adcclkp1,
		iadc_adcclkp2,
		iadc_tst_mod,

        //spdif
        out_spdif,
// -------------------------------------------------------------------------- //
// South Bridge

        ir_rx,

        scb_scl_out,
        scb_scl_oe,
        scb_sda_out,
        scb_sda_oe,
        scb_scl_in,
        scb_sda_in,
// UART interface
		uart_tx,
		uart_rx,

// eric000430: add USB below,
// add more 2 ports in m6304
        p1_ls_mode      ,
        p1_txd          ,
        p1_txd_oej      ,
        p1_txd_se0      ,
        p2_ls_mode      ,
        p2_txd          ,
        p2_txd_oej      ,
        p2_txd_se0      ,
        //add two ports p3,p4 yuky, 2002-06-13
 //       p3_ls_mode      ,
 //       p3_txd          ,
 //       p3_txd_oej      ,
 //       p3_txd_se0      ,
 //       p4_ls_mode      ,
 //       p4_txd          ,
 //       p4_txd_oej      ,
 //       p4_txd_se0      ,
        usb_pon_,
        i_p1_rxd        ,
        i_p1_rxd_se0,
        i_p2_rxd,
        i_p2_rxd_se0,
        //add two ports p3,p4 yuky, 2002-06-13
 //       i_p3_rxd        ,
 //       i_p3_rxd_se0,
 //       i_p4_rxd,
 //       i_p4_rxd_se0,
        usb_clk,
        usb_overcur_,
// eric000430: add USB above

// Display Engine
//        pclk_oe_,
        vdata_oe_,
        vdata_out,
        h_sync_out_,
        h_sync_oe_,
        in_h_sync_,
        v_sync_out_,
        v_sync_oe_,
        in_v_sync_,

// TV Encoder intf
		tvenc_vdi4vdac,
		tvenc_vdo4vdac,
		tvenc_idump,
		tvenc_iext,
		tvenc_iext1,		// 470 Ohm connects for the full scale amplitudes
		tvenc_iout1,
		tvenc_iout2,
		tvenc_iout3,
		tvenc_iout4,
		tvenc_iout5,
		tvenc_iout6,

		tvenc_iref1,
		tvenc_iref2,
		tvenc_gnda,
		tvenc_gnda1,		// ground for DAC
		tvenc_vsud,			// ground for DAC
		tvenc_vd33a,

//added for m3357 JFR030626
		tvenc_h_sync_,	//output to mux with ext TV encoder for DE slaver sync
		tvenc_v_sync_,
		tvenc_vdactest,

//VIDEO IN JFR 031127
		VIN_PIXEL_DATA	,
		VIN_PIXEL_CLK	,
		VIN_H_SYNCJ	    ,
		VIN_V_SYNCJ	    ,

//----------------- SERVO Interface begin -------------------------
		//SERVO INTERFACE
//		dmckg_mpgclk		,		// MEM_CLK
//		sv_top_clkdiv		,
		sv_pmata_somd		,
		sv_pmsvd_trcclk		,
		sv_pmsvd_trfclk		,
		sv_sv_tslrf			,
		sv_sv_tplck			,
//		sv_sv_dev_true     	,
		sv_tst_svoio_i			,
		sv_svo_svoio_o			,
		sv_svo_svoio_oej		,
		sv_tst_addata			,
		sv_reg_rctst_2       	,
//		sv_top_sv_mppdn		,
//		sv_dmckg_sysclk        ,
		sv_mckg_dspclk         ,
//		sv_sycg_svoata 		,
//		sv_sv_reg_c2ftst       ,
//		sv_pmdm_ideslave      ,
		sv_pmsvd_tplck         ,
		sv_pmsvd_tslrf         ,
		sv_sv_tst_pin			,
//		sv_sv_reg_tstoen       ,
		sv_pmsvd_c2ftstin		,
		sv_sycg_somd			,
		// Servo analog
//		sv_cpu_gpioin			,
//		sv_sycg_burnin			,
//		External uP interface
		sv_ex_up_in				,
		sv_ex_up_out			,
		sv_ex_up_oe_			,
		// Servo Atapi inf
		sv_sv_hi_hdrq_o        ,
		sv_sv_hi_hdrq_oej      ,
		sv_sv_hi_hcs16j_o      ,
		sv_sv_hi_hcs16j_oej    ,
		sv_sv_hi_hint_o        ,
		sv_sv_hi_hint_oej      ,
		sv_sv_hi_hpdiagj_o     ,
		sv_sv_hi_hpdiagj_oej   ,
		sv_sv_hi_hdaspj_o      ,
		sv_sv_hi_hdaspj_oej    ,
		sv_sv_hi_hiordy_o      ,
		sv_sv_hi_hiordy_oej    ,
		sv_sv_hi_hd_o			,
		sv_sv_hi_hd_oej        ,
		sv_pmata_hrstj         ,
		sv_pmata_hcs1j         ,
		sv_pmata_hcs3j         ,
		sv_pmata_hiorj         ,
		sv_pmata_hiowj         ,
		sv_pmata_hdackj        ,
		sv_pmata_hpdiagj       ,
		sv_pmata_hdaspj        ,
		sv_pmata_ha			,
		sv_pmata_hd			,
		// Servo Test
		sv_sv_svo_sflag		,
		sv_sv_svo_soma			,
		sv_sv_svo_somd			,
		sv_sv_svo_somd_oej     ,
		sv_sv_svo_somdh_oej    ,
//		sv_sv_reg_dvdram_toe   ,
//		sv_sv_reg_dfctsel      ,
//		sv_sv_reg_trfsel 		,
//		sv_sv_reg_flgtoen 		,
//		sv_sv_reg_gpioten		,
//		sv_sv_reg_13e_7		,
//		sv_sv_reg_13e_6		,
//		sv_reg_trg_plcken		,
//		sv_reg_sv_tplck_oej	,
		//	Servo analog ANA_DTSV

		sv_pad_clkref			,
		sv_xadcin              ,
		sv_xadcip              ,
		sv_xatton              ,
		sv_xattop              ,
		sv_xbiasr              ,
		sv_xcdld               ,
		sv_xcdpd               ,
		sv_xcdrf               ,
		sv_xcd_a               ,
		sv_xcd_b               ,
		sv_xcd_c               ,
		sv_xcd_d               ,
		sv_xcd_e               ,
		sv_xcd_f               ,
		sv_xcelpfo             ,
		sv_xdpd_a              ,
		sv_xdpd_b              ,
		sv_xdpd_c              ,
		sv_xdpd_d              ,
		sv_xdvdld              ,
		sv_xdvdpd              ,
		sv_xdvdrfn             ,
		sv_xdvdrfp             ,
		sv_xdvd_a              ,
		sv_xdvd_b              ,
		sv_xdvd_c              ,
		sv_xdvd_d              ,
		sv_xfelpfo             ,
		sv_xfocus              ,
		sv_xgmbiasr            ,
		sv_xlpfon              ,
		sv_xlpfop              ,
		sv_xpdaux1             ,
		sv_xpdaux2             ,
		sv_xsblpfo             ,
		sv_xsfgn               ,
		sv_xsfgp               ,

		sv_pata_sflag			,
		sv_pmata_sflag			,
		sv_pmata_sflag_oej		,

		sv_xsflag              ,
		sv_xslegn              ,
		sv_xslegp              ,
		sv_xspindle            ,
		sv_xtelp               ,
		sv_xtelpfo             ,
		sv_xtestda             ,
		sv_xtexo               ,
		sv_xtrack              ,
		sv_xtray               ,
		sv_xvgain              ,
		sv_xvgaip              ,
		sv_xvref15             ,
		sv_xvref21             ,
		//Servo power
		sv_vd33d            	,
		sv_vd18d            	,
		sv_vdd3mix1				,
		sv_vdd3mix2				,
		sv_avdd_ad          	,
		sv_avdd_att         	,
		sv_avdd_da          	,
		sv_avdd_dpd         	,
		sv_avdd_lpf         	,
		sv_avdd_rad         	,
		sv_avdd_ref         	,
		sv_avdd_svo         	,
		sv_avdd_vga         	,
		sv_avss_ad          	,
		sv_avss_att         	,
		sv_avss_da          	,
		sv_avss_dpd         	,
		sv_avss_ga          	,
		sv_avss_gd          	,
		sv_avss_lpf         	,
		sv_avss_rad         	,
		sv_avss_ref         	,
		sv_avss_svo         	,
		sv_avss_vga         	,
		sv_azec_gnd         	,
		sv_azec_vdd         	,
		sv_dv18_rcka        	,
		sv_dv18_rckd        	,
		sv_dvdd             	,
		sv_dvss             	,
		sv_dvss_rcka        	,
		sv_dvss_rckd        	,
		sv_fad_gnd          	,
		sv_fad_vdd          	,
		sv_gndd             	,

//------------------- Servo interface end-------------------

// IDE Interface
        atadiow_out_,
        atadior_out_,
        atacs0_out_,
        atacs1_out_,
        atadd_out,
        atadd_oe_,
        atada_out,
        atareset_out_,
        atareset_oe_,
        atadmack_out_,

        ataiordy_in,
        ataintrq_in,
        atadd_in,
        atadmarq_in,



// -------------------------------------------------------------------------- //
//        disp_clk,
//        gpu_clk,
        cpu_clk,                        // pipeline clock   (CPU core)
        mem_clk,                        // transfer clock   (Memory Controller, IO, ROM)
        pci_clk,
        sb_clk,
        dsp_clk,
//        video_clk,
        spdif_clk,
//        ide100m_clk,
//		ide66m_clk,
//		f54m_clk,
		ata_clk,
		servo_clk,				// servo clock
        pad_boot_config,

		inter_only,				// Interlace output
		dvi_rwbuf_sel,			// DVI buffer read/write

//		f108m_clk,				// 108MHz output
		cvbs2x_clk,				// 54MHZ clock
		cvbs_clk,				// 27MHz clock
//		cav2x_clk,				// 108 or 54 MHZ clock
		cav_clk,				// 54 or 27 MHZ clock
		dvi_buf0_clk,			// CAV_CLK or VIDEO_CLK for buffer 0
		dvi_buf1_clk,			// CAV_CLK or VIDEO_CLK for buffer 1
		video_clk,				// VIDEO clock output

		tv_f108m_clk,			// tv_encoder 108MHz clock
		tv_cvbs2x_clk,			// tv_encoder 54MHZ clock
		tv_cvbs_clk,			// tv_encoder 27MHz clock
		tv_cav2x_clk,			// tv_encoder 108 or 54 MHZ clock
		tv_cav_clk,				// tv_encoder 54 or 27 MHZ clock


//	PLL control
		cpu_clk_pll_m_value,	// new cpu_clk_pll m parameter
		cpu_clk_pll_m_tri,      // cpu_clk_pll m modification trigger
		cpu_clk_pll_m_ack,      // cpu_clk_pll m modification ack

		mem_clk_pll_m_value,    // new mem_clk_pll m parameter
		mem_clk_pll_m_tri,      // mem_clk_pll m modification trigger
		mem_clk_pll_m_ack,      // mem_clk_pll m modification ack

		pci_fs_value,			// new pci frequency select parameter
		pci_fs_tri,             // pci frequency select modification trigger
		pci_fs_ack,             // pci frequency select modification ack

		work_mode_value,		// new work_mode
		work_mode_tri,          // work_mode modification trigger
		work_mode_ack,          // work_mode modification ack

//	clock frequency select, clock misc control
		dsp_fs,					// dsp clock frequency select
		dsp_div_src_sel,		// dsp source clock select
		ve_fs,					// ve clock frequency select
		pci_clk_out_sel,
		svga_mode_en,			// SVGA mode output enable
		tv_enc_clk_sel,			// TV encoder clock setting

        //sdram delay select
        mem_dly_sel,
        mem_rd_dly_sel,
		test_dly_chain_sel,		// TEST_DLY_CHAIN delay select
		test_dly_chain_flag,	// TEST_DLY_CHAIN flag output

//	external interface control
		audio_pll_fout_sel,		// Audio PLL FOUT frequency select
		i2s_mclk_sel,			// I2S MCLK source select
		i2s_mclk_out_en_,		// I2S MCLK output enable
		spdif_clk_sel,			// S/PDIF clock select
		hv_sync_en,				// h_sync_, v_sync_ enable on the xsflag[1:0] pins
		i2si_data_en,			// I2S input data enable on the gpio[7] pin
		scb_en,					// SCB interface enable on the gpio[6:5] pins
		servo_gpio_en,			// servo_gpio[2:0] enable on the gpio[2:0] pins
		ext_ide_device_en,		// external IDE device enable
		pci_gntb_en,			// PCI GNTB# enable
		vdata_en,				// video data enable
		sync_source_sel,		// sync signal source select, 0 -> TVENC, 1 -> DE.
		video_in_intf_en,		// video in interface enable
		pci_mode_ide_en,		// pci_mode ide interface enable
		ide_active,				// ide interface is active in the pci mode

//	SERVO control
		sv_tst_pin_en,			// SERVO test pin enable
		sv_ide_mode_part0_en,	// ide mode SERVO test pin part 0 enable
		sv_ide_mode_part1_en,	// ide mode SERVO test pin part 1 enable
		sv_ide_mode_part2_en,	// ide mode SERVO test pin part 2 enable
		sv_ide_mode_part3_en,	// ide mode SERVO test pin part 3 enable
		sv_c2ftstin_en,			// SERVO c2 flag enable
//		sv_sycg_updbgout,		// SERVO cpu interface debug enable
		sv_trg_plcken,			// SERVO tslrf and tplck enable
		sv_pmsvd_trcclk_trfclk_en,	// SERVO pmsvd_trcclk, pmsvd_trfclk enable



        pci_mode,
        ide_mode,
		tvenc_test_mode	,
		servo_only_mode ,
		servo_test_mode ,
		servo_sram_mode ,
		bist_test_mode	,

		dsp_bist_mode	,

//DSP bist test interface
		dsp_bist_finish,
		dsp_bist_err_vec,

//VIDEO bist interface
		video_bist_tri		,
		video_bist_finish	,
		video_bist_vec		,

        test_mode,
`ifdef  SCAN_EN
	TEST_SE,
	TEST_SI,
	TEST_SO,
`endif
        sw_rst_pulse,
        rst_                    // all chipset reset pin
        );
//////////////////////
//output                x_rd_last;
//output                x_wr_last;
output          x_ready;
output  [31:0]  x_data_in;
//output                x_bus_idle;
output          x_bus_error;    // To t2core
output          x_ack;

input           u_request;
input           u_write;
input   [1:0]   u_wordsize;
input   [31:0]  u_startpaddr;
input   [31:0]  u_data_to_send;
input   [3:0]   u_bytemask;

//input         g_config_ep;
//input           g_reset_;       // From t2core

output          x_err_int;
output          x_ext_int;

//input	[39:0]	cpu_testout;	// Add for test

// chipset controller
// SDRAM & FLASH
output  [11:0]  ram_addr;       // SDRAM address pins & ROM partial address pins
output  [1:0]   ram_ba;         // SDRAM bank address
output  [31:0]  ram_dq_out;     // SDRAM & Flash ROM 32 bits output data pins
input   [31:0]  ram_dq_in;      // SDRAM and Flash 32 bit input data bus
output  [31:0]  ram_dq_oe_;     // Data Port Output Enable
output          ram_cas_;       // SDRAM cas# and flash we#
output          ram_we_;        // SDRAM we#
output          ram_ras_;       // SDRAM ras# and Flash address
output          ram_cke;        // SDRAM clock enable
output  [3:0]   ram_dqm;        // SDRAM data mask signal
output  [1:0]   ram_cs_;        // SDRAM chip select

// EEPROM
output  [20:0]  eeprom_addr;
input   [7:0]   eeprom_rdata;
output          eeprom_ce_;
output          eeprom_oe_;
output          eeprom_we_;
output          rom_data_oe_, rom_addr_oe;
output  [7:0]   eeprom_wdata;
output          rom_en_; //means rom operation
// LED
//output        [7:0]   ledpin;

// AGP,PCI
output          oe_ad_;
output  [31:0]  out_ad;
output          oe_cbe_;
output  [3:0]   out_cbe_;
output          oe_frame_;
output          out_frame_;
output          oe_irdy_;
output          out_irdy_;
output          oe_devsel_;
output          out_devsel_;
output          oe_trdy_;
output          out_trdy_;
output          oe_stop_;
output          out_stop_;
output          oe_par_;
output          out_par;

output  [1:0]   out_gnt_;
input   [1:0]   in_req_;

input   [31:0]  in_ad;
input   [3:0]   in_cbe_;

input           in_frame_;
input           in_irdy_;
input           in_devsel_;
input           in_trdy_;
input           in_stop_;
input           in_par;

input           ext_int;
output  [31:0]  gpio_out;
output  [31:0]  gpio_oe_;
input   [31:0]  gpio_in;

output          out_i2so_data0,
	        	out_i2so_data1,
        		out_i2so_data2,
        		out_i2so_data3;
output          out_i2so_lrck;
output          out_i2so_bick;
input           in_i2si_data;
//input   in_i2si_lrck;
//input   in_i2si_bick;
input           in_i2so_mclk;
input	        iadc_mclk;					// IADC master clock
input	[24:23]	iadc_smtsen;				// ADC SRAM Test Enable
input	[2:0]	iadc_smtsmode;				// ADC SRAM Test Mode
output	[24:23]	iadc_smts_err;              // ADC SRAM Test Error Flag
output	[24:23]	iadc_smts_end;              // ADC SRAM Self Test End
output			iadc_adcclkp1;				// ADC digital test pin 1
output			iadc_adcclkp2;              // ADC digital test pin 2
input	[3:0]	iadc_tst_mod;               // ADC Internal SDM Test Selection

output			iadc_v1p2u 	;
input			iadc_vs    	;
input			iadc_vd    	;
input			iadc_vin_l 	;
input			iadc_vin_r 	;

//spdif
output          out_spdif;

// South Bridge
input           ir_rx;

//output          scb_en;
output          scb_scl_out;
output          scb_scl_oe;
output          scb_sda_out;
output          scb_sda_oe;
input           scb_scl_in;
input           scb_sda_in;

output			uart_tx;

input			uart_rx;


// eric000430: add USB below
output  p1_ls_mode;
output  p1_txd          ;
output  p1_txd_oej      ;
output  p1_txd_se0      ;
output  p2_ls_mode      ;
output  p2_txd          ;
output  p2_txd_oej      ;
output  p2_txd_se0      ;
//output  p3_ls_mode;
//output  p3_txd          ;
//output  p3_txd_oej      ;
//output  p3_txd_se0      ;
//output  p4_ls_mode      ;
//output  p4_txd          ;
//output  p4_txd_oej      ;
//output  p4_txd_se0      ;
output  usb_pon_;

input   i_p1_rxd        ;
input   i_p1_rxd_se0;
input   i_p2_rxd        ;
input   i_p2_rxd_se0;
//input   i_p3_rxd        ;
//input   i_p3_rxd_se0;
//input   i_p4_rxd        ;
//input   i_p4_rxd_se0;
input   usb_clk;
input   usb_overcur_;
// eric000430: add USB above

// Display Engine
//output  pclk_oe_;
output  vdata_oe_;
output  h_sync_out_;
output  h_sync_oe_;
output  v_sync_out_;
output  v_sync_oe_;
input   in_h_sync_;
input   in_v_sync_;
output  [7:0]   vdata_out;

//TV encoder intf
input	[11:0]	tvenc_vdi4vdac;	// input, for debug
output	[11:0]	tvenc_vdo4vdac;	// output, for debug
output		tvenc_idump;		// output, for external load resistance
output		tvenc_iext;			// inout, for external load resistance	!!! bi-direction
output		tvenc_iext1;		// 470 Ohm connects for the full scale amplitudes
output		tvenc_iout1;		// output, analog
output		tvenc_iout2;		// output, analog
output		tvenc_iout3;		// output, analog
output		tvenc_iout4;		// output, analog
output		tvenc_iout5;		// output, analog
output		tvenc_iout6;		// output, analog

output		tvenc_iref1;		// added for m3357JFR 030626
output		tvenc_iref2;		// tv encoder IREF2 output

output		tvenc_h_sync_;
output		tvenc_v_sync_;
input		tvenc_vdactest;
input		tvenc_gnda;
input		tvenc_gnda1;		// ground for DAC
input		tvenc_vsud;			// ground for DAC
input		tvenc_vd33a;

input	[7:0]	VIN_PIXEL_DATA	;
input		VIN_PIXEL_CLK	;
input		VIN_H_SYNCJ	    ;
input		VIN_V_SYNCJ	    ;



// Servo interface
//input			sv_top_clkdiv			;	//// sERVO iNTERNAL CLOCK DIVISE
input	[23:0]	sv_pmata_somd  		;	////
input			sv_pmsvd_trcclk		;
input			sv_pmsvd_trfclk		;
output			sv_sv_tslrf			;
output			sv_sv_tplck			;
//output			sv_sv_dev_true     	;
output	[2:0]	sv_svo_svoio_o 		;
output	[2:0]	sv_svo_svoio_oej	;
output			sv_reg_rctst_2       	;
input	[2:0]	sv_tst_svoio_i			;
input	[5:0]	sv_tst_addata 			;
//input			sv_top_sv_mppdn		;
//input			sv_dmckg_sysclk        ;	//// Servo System clock decided by TOP_CLKDIV
input			sv_mckg_dspclk         ;
//input			sv_sycg_svoata 		;
//output			sv_sv_reg_c2ftst       ;
//input			sv_pmdm_ideslave       ;
input			sv_pmsvd_tplck         ;
input			sv_pmsvd_tslrf         ;
output	[44:0]	sv_sv_tst_pin    		;
//output			sv_sv_reg_tstoen       ;
input	[1:0]	sv_pmsvd_c2ftstin 		;
input	[23:0]	sv_sycg_somd     		;
//				analog
//input	[3:0]	sv_cpu_gpioin			;
//input	[1:0]	sv_sycg_burnin			;
input	[22:0]	sv_ex_up_in				;
output	[22:0]	sv_ex_up_out			;
output	[22:0]	sv_ex_up_oe_			;

//			Atapi inf
output			sv_sv_hi_hdrq_o        ;
output			sv_sv_hi_hdrq_oej      ;
output			sv_sv_hi_hcs16j_o      ;
output			sv_sv_hi_hcs16j_oej    ;
output			sv_sv_hi_hint_o        ;
output			sv_sv_hi_hint_oej      ;
output			sv_sv_hi_hpdiagj_o     ;
output			sv_sv_hi_hpdiagj_oej   ;
output			sv_sv_hi_hdaspj_o      ;
output			sv_sv_hi_hdaspj_oej    ;
output			sv_sv_hi_hiordy_o      ;
output			sv_sv_hi_hiordy_oej    ;
output	[15:0] 	sv_sv_hi_hd_o   		;
output			sv_sv_hi_hd_oej        ;
input			sv_pmata_hrstj         ;
input			sv_pmata_hcs1j         ;
input			sv_pmata_hcs3j         ;
input			sv_pmata_hiorj         ;
input			sv_pmata_hiowj         ;
input			sv_pmata_hdackj        ;
input			sv_pmata_hpdiagj       ;
input			sv_pmata_hdaspj        ;
input	[2:0]	sv_pmata_ha           	;
input	[15:0]	sv_pmata_hd          	;

////// SERVO INTERFACE
output	[1:0]	sv_sv_svo_sflag   		;
output	[12:0]	sv_sv_svo_soma   		;
output	[23:0]	sv_sv_svo_somd   		;
output	[21:0]	sv_sv_svo_somd_oej     ;
output	[23:22]	sv_sv_svo_somdh_oej    ;
//output			sv_sv_reg_dvdram_toe   ;
//output			sv_sv_reg_dfctsel      ;
//output			sv_sv_reg_trfsel 		;
//output			sv_sv_reg_flgtoen 		;
//output	[3:0]	sv_sv_reg_gpioten		;
//output			sv_sv_reg_13e_7		;
//output			sv_sv_reg_13e_6		;
//output			sv_reg_trg_plcken		;
//output			sv_reg_sv_tplck_oej	;
//				ANA_DTSV

input			sv_pad_clkref			;
input			sv_xadcin              ;	// Differential input signal to ADC
input			sv_xadcip              ;	// Differential input signal to ADC
output			sv_xatton              ;	// Differential output from attenuator
output			sv_xattop              ;	// Differential output from attenuator
input			sv_xbiasr              ;	// Bi-direction pin for internal bias current external R=12K
output			sv_xcdld               ;	// CD laser power control output
input			sv_xcdpd               ;	// CD laser power control input from the monitor photo diode
input			sv_xcdrf               ;	// CD RF input from pick up head
input			sv_xcd_a               ;	// Photo detect signal input from pickup head for CD sub beam
input			sv_xcd_b               ;	// Photo detect signal input from pickup head for CD sub beam
input			sv_xcd_c               ;	// Photo detect signal input from pickup head for CD sub beam
input			sv_xcd_d               ;	// Photo detect signal input from pickup head for CD sub beam
input			sv_xcd_e               ;	// Photo detect signal input from pickup head for CD sub beam
input			sv_xcd_f               ;	// Photo detect signal input from pickup head for CD sub beam
output			sv_xcelpfo             ;	// Output for outside low pass filter of central error signal
input			sv_xdpd_a              ;	// Photo detect signal input from pickup head for DPD function
input			sv_xdpd_b              ;	// Photo detect signal input from pickup head for DPD function
input			sv_xdpd_c              ;	// Photo detect signal input from pickup head for DPD function
input			sv_xdpd_d              ;	// Photo detect signal input from pickup head for DPD function
output			sv_xdvdld              ;	// DVD laser power control output
input			sv_xdvdpd              ;	// DVD laser power control input from the monitor photo diode
input			sv_xdvdrfn             ;	// DVD RF differential input from pickup head
input			sv_xdvdrfp             ;	// DVD RF differential input from pickup head
input			sv_xdvd_a              ;	// Photo detect signal input from pickup head for DVD servo signal
input			sv_xdvd_b              ;	// Photo detect signal input from pickup head for DVD servo signal
input			sv_xdvd_c              ;	// Photo detect signal input from pickup head for DVD servo signal
input			sv_xdvd_d              ;	// Photo detect signal input from pickup head for DVD servo signal
output			sv_xfelpfo             ;	// Output for outside low pass filter of focus error signal
output			sv_xfocus              ;	// Focus actuator control signal output
input			sv_xgmbiasr            ;	// Bi-derection pin for Gm cell bias, external R=43K
output			sv_xlpfon              ;	// Differential output from low pass filter
output			sv_xlpfop              ;	// Differential output from low pass filter
input			sv_xpdaux1             ;	// Analog test pin 1
input			sv_xpdaux2             ;	// Analog test pin 2
output			sv_xsblpfo             ;	// Output for outside low pass filter of SBAD signal
input			sv_xsfgn               ;	// Spindle rotarion speed detection input pin
input			sv_xsfgp               ;	// Spindle rotarion speed detection input pin

output	[1:0]	sv_pata_sflag     		;
input	[1:0]	sv_pmata_sflag    		;
input	[1:0]	sv_pmata_sflag_oej		;

inout	[1:0]	sv_xsflag				;
output			sv_xslegn              ;	// Control signal output for sledge motor(-)
output			sv_xslegp              ;	// Control signal output for sledge motor(+)
output			sv_xspindle            ;	// PWM output for spindle motor control
input			sv_xtelp               ;	// Low pass filter of tracking center level for TEZC
output			sv_xtelpfo             ;	// Output for outside low passfilter of tracking error signal
output			sv_xtestda             ;	// Reserved DA output pin for testing
output			sv_xtexo               ;	// Tracking error output
output			sv_xtrack              ;	// Track actuator control signal output
output			sv_xtray               ;	// PWM output for tray motor control
input			sv_xvgain              ;	// Differential input signal to VGA
input			sv_xvgaip              ;	// Differential input signal to VGA
output			sv_xvref15             ;	// 1.5V reference voltage output for servo control output signals
output			sv_xvref21             ;	// 2.1V reference voltage output for input I/F from pick_up head

//				power
input			sv_vd33d               ;
input			sv_vd18d               ;
input			sv_vdd3mix1				;	// Digital Power +3.3V inside analog block
input			sv_vdd3mix2				;	// Digital Power +3.3V inside analog block
input			sv_avdd_ad             ;	// Analog Power +3.3V for Servo ADC part
input			sv_avdd_att            ;
input			sv_avdd_da             ;	// Analog Power +3.3V for servo DAC part
input			sv_avdd_dpd            ;	// Analog Power +3.3V for DPD part
input			sv_avdd_lpf            ;
input			sv_avdd_rad            ;	// Analog Power +3.3V for Read Channel ADC part
input			sv_avdd_ref            ;
input			sv_avdd_svo            ;	// Analog Power +3.3V for Servo Signal generation part
input			sv_avdd_vga            ;
input			sv_avss_ad             ;	// Analog Ground for Servo ADC part
input			sv_avss_att            ;
input			sv_avss_da             ;	// Analog Ground for servo DAC part
input			sv_avss_dpd            ;	// Analog Ground for DPD part
input			sv_avss_ga             ;	// Guard Ring Ground for Analog part
input			sv_avss_gd             ;	// Guard Ring Ground for Digital part
input			sv_avss_lpf            ;
input			sv_avss_rad            ;	// Analog Ground for Read Channel ADC part
input			sv_avss_ref            ;
input			sv_avss_svo            ;	// Analog Ground for Servo Signal generation part
input			sv_avss_vga            ;
input			sv_azec_gnd            ;
input			sv_azec_vdd            ;
input			sv_dv18_rcka           ;	// Digital Power +1.8V for Read Channel clock?
input			sv_dv18_rckd           ;	// Digital Power +1.8V for Read Channel clock?
input			sv_dvdd                ;
input			sv_dvss                ;
input			sv_dvss_rcka           ;	// Digital Ground for Read Channel clock?
input			sv_dvss_rckd           ;	// Digital Ground for Read Channel clock?
input			sv_fad_gnd             ;
input			sv_fad_vdd             ;
input			sv_gndd                ;

// IDE Interface
output  	atadiow_out_;
output  	atadior_out_;
output  	atacs0_out_;
output  	atacs1_out_;
output[15:0]atadd_out;
output[3:0] atadd_oe_;
output[2:0] atada_out;
output  	atareset_out_;
output  	atareset_oe_;
output  	atadmack_out_;

input   	ataiordy_in;
input   	ataintrq_in;
input[15:0] atadd_in;
input   	atadmarq_in;

//clk
input      	cpu_clk;                        // pipeline clock   (CPU core)
input      	mem_clk;                        // transfer clock   (Memory Controller, IO, ROM)
input      	pci_clk;                        // AGP clock        (66)
input      	sb_clk;
input      	dsp_clk;
input      	spdif_clk;
input		ata_clk;
input		servo_clk;					// servo clock
input   [31:0]  pad_boot_config;

output			inter_only;				// Interlace output
output			dvi_rwbuf_sel;			// DVI buffer read/write

//input			f108m_clk;				// 108MHz output
input			cvbs2x_clk;				// 54MHZ clock
input			cvbs_clk;				// 27MHz clock
//input			cav2x_clk;				// 108 or 54 MHZ clock
input			cav_clk;				// 54 or 27 MHZ clock
input			dvi_buf0_clk;			// CAV_CLK or VIDEO_CLK for buffer 0
input			dvi_buf1_clk;			// CAV_CLK or VIDEO_CLK for buffer 1
input			video_clk;				// VIDEO clock output

input			tv_f108m_clk;			// tv_encoder 108MHz clock
input			tv_cvbs2x_clk;			// tv_encoder 54MHZ clock
input			tv_cvbs_clk;			// tv_encoder 27MHz clock
input			tv_cav2x_clk;			// tv_encoder 108 or 54 MHZ clock
input			tv_cav_clk;				// tv_encoder 54 or 27 MHZ clock

//	PLL control
output	[5:0]	cpu_clk_pll_m_value;// new cpu_clk_pll m parameter
output			cpu_clk_pll_m_tri;	// cpu_clk_pll m modification trigger
input			cpu_clk_pll_m_ack;	// cpu_clk_pll m modification ack

output	[5:0]	mem_clk_pll_m_value;// new mem_clk_pll m parameter
output			mem_clk_pll_m_tri;	// mem_clk_pll m modification trigger
input			mem_clk_pll_m_ack;	// mem_clk_pll m modification ack

output	[1:0]	pci_fs_value;		// new pci_fs parameter
output			pci_fs_tri;			// pci_fs modification trigger
input			pci_fs_ack;			// pci_fs modification ack

output	[2:0]	work_mode_value;	// new work_mode
output			work_mode_tri;      // work_mode modification trigger
input			work_mode_ack;      // work_mode modification ack

//	clock frequency select, clock misc control
output	[3:0]	dsp_fs;				// dsp clock frequency select
output			dsp_div_src_sel;	// dsp source clock select
output	[1:0]	ve_fs;				// ve clock frequency select
output	[1:0]	pci_clk_out_sel;	// JFR030830
output			svga_mode_en;		// SVGA mode output enable
output	[11:8]	tv_enc_clk_sel;		// TV encoder clock setting

output  [4:0]   mem_dly_sel;
output	[4:0]	mem_rd_dly_sel;		//	SDRAM read clock delay control
output	[5:0]	test_dly_chain_sel;	// TEST_DLY_CHAIN delay select
input			test_dly_chain_flag;// TEST_DLY_CHAIN flag output

//	external interface control
output			audio_pll_fout_sel;	// Audio PLL FOUT frequency select
output	[1:0]	i2s_mclk_sel;		// I2S MCLK source select
output			i2s_mclk_out_en_;	// I2S MCLK output enable
output	[1:0]	spdif_clk_sel;		// S/PDIF clock select
output			hv_sync_en;			// h_sync_, v_sync_ enable on the xsflag[1:0] pins
output			i2si_data_en;		// I2S input data enable on the gpio[7] pin
output			scb_en;				// SCB interface enable on the gpio[6:5] pins
output			servo_gpio_en;		// servo_gpio[2:0] enable on the gpio[2:0] pins
output			ext_ide_device_en;	// external IDE device enable
output			pci_gntb_en;		// PCI GNTB# enable
output			vdata_en;			// video data enable
output			sync_source_sel;	// sync signal source select, 0 -> TVENC, 1 -> DE.
output			video_in_intf_en;	// video in interface enable
output			pci_mode_ide_en;	// pci_mode ide interface enable
output			ide_active;			// ide interface is active in the pci mode

//	SERVO control
output			sv_tst_pin_en;			// SERVO test pin enable
output			sv_ide_mode_part0_en;	// ide mode SERVO test pin part 0 enable
output			sv_ide_mode_part1_en;	// ide mode SERVO test pin part 1 enable
output			sv_ide_mode_part2_en;	// ide mode SERVO test pin part 2 enable
output			sv_ide_mode_part3_en;	// ide mode SERVO test pin part 3 enable
output			sv_c2ftstin_en;			// SERVO c2 flag enable
wire			sv_sycg_updbgout;		// SERVO cpu interface debug enable
output			sv_trg_plcken;			// SERVO tslrf and tplck enable
output			sv_pmsvd_trcclk_trfclk_en;	// SERVO pmsvd_trcclk, pmsvd_trfclk enable

input           pci_mode,
                ide_mode,
                tvenc_test_mode,
				servo_only_mode,
				servo_test_mode,
				servo_sram_mode,
				bist_test_mode	;

input			dsp_bist_mode	;

output			dsp_bist_finish;
output	[2:0]	dsp_bist_err_vec;

input			video_bist_tri		;

output			video_bist_finish	;
output	[10:0]	video_bist_vec		;

`ifdef  SCAN_EN
input		TEST_SE;
input	[90:0]	TEST_SI;
output	[90:0]	TEST_SO;
wire	[90:0]	TEST_SO;
`endif

output          sw_rst_pulse;
input           rst_;
input           test_mode;

parameter       udly            = 1;

//// Decode Bus ////
//wire    test_mode = 1'b0;    // test mode for GATX20, should be from PAD!

wire    [31:0]  x_data_in;
wire    [1:0]   u_wordsize;
wire    [31:0]  u_startpaddr;
wire    [31:0]  u_data_to_send;
wire    [3:0]   u_bytemask;

wire    [31:0]  ioreg_rd_data,ioreg_wr_data,
                rom_rd_data, rom_wr_data,
                pbiu_rd_data,pbiu_wr_data;

wire    [29:2]  ioreg_addr,rom_addr,
                pbiu_addr;

wire    [1:0]   pbiu_bl;
wire    [3:0]   pbiu_be;
wire    [3:0]   ioreg_be, rom_rw_be;

//FOR PCI AND IDE SWITCH SHARE PINS
wire			IDE_STOP	;
wire			IDE_IDLE    ;
// CPU slave interface
wire    [31:0]  cpu_ram_wdata;
wire    [31:0]  cpu_ram_rdata;
wire    [3:0]   cpu_ram_wbe;
wire    [29:2]  cpu_ram_addr;
wire    [1:0]   cpu_ram_rbl;              // burst length from 1 to 8
wire            cpu_ram_req;
wire            cpu_ram_rw;
wire            cpu_ram_wlast;
wire            cpu_ram_rlast;
wire            cpu_ram_rrdy;
wire            cpu_ram_wrdy;
wire            cpu_ram_err;
wire            cpu_ram_ack;

//// IO Register ////
//wire    [7:0]   ledpin;

wire    [31:0]  p_cfg_addr,
                p_cfg_din,
                p_cfg_dout;
wire    [3:0]   p_cfg_be;

wire    [7:0]   ledpin;
// local device io read/write signal
// all the signals are in memory clock domain
// all read/write control, address, byte enable, wdata are connect to all device.
wire            cpu_slave_rw;           // cpu read/write command, 0-> read; 1-> write
wire    [15:2]  cpu_slave_addr;         // cpu read/write address
wire    [3:0]   cpu_slave_be;           // cpu read/write byte enable
wire    [31:0]  cpu_slave_wdata;        // cpu write data

wire            cpu_gpu_req;            // cpu to gpu read/write request
wire            cpu_dsp_req;            // cpu to dsp read/write request
wire            cpu_sb_req;             // cpu to sb read/write request
wire            cpu_ve_req;             // cpu to ve read/write request
wire            cpu_disp_req;           // cpu to display read/write request
wire			cpu_vsb_req;			// cpu to vsb read/write request
wire			cpu_ide_req;			// cpu to ide read/write request
wire			cpu_tvenc_req;			// cpu to tv encoder read/write request
wire			cpu_video_in_req;		// cpu to VIDEO_IN read/write request

wire            cpu_gpu_ack;            // gpu to cpu ack
wire            cpu_dsp_ack;            // dsp to cpu ack
wire            cpu_sb_ack;             // sb to cpu ack
wire            cpu_ve_ack;             // ve to cpu ack
wire            cpu_disp_ack;           // display to cpu ack
wire			cpu_vsb_ack;			// vsb to cpu ack
wire			cpu_ide_ack;			// ide to cpu ack
wire			cpu_tvenc_ack;			// tv encoder to cpu ack
wire			cpu_video_in_ack;		// cpu to VIDEO_IN ack

wire    [31:0]  cpu_gpu_rdata;          // gpu to cpu read data
wire    [31:0]  cpu_dsp_rdata;          // dsp to cpu read data
wire    [31:0]  cpu_sb_rdata;           // sb to cpu read data
//wire    [31:0]  cpu_ve_rdata;           // ve to cpu read data
wire    [31:0]  cpu_disp_rdata;         // display to cpu read data
wire	[31:0]	cpu_vsb_rdata;			// vsb to cpu read data
wire	[31:0]	cpu_ide_rdata;			// ide to cpu read data
wire	[31:0]	cpu_tvenc_rdata;		// TV encoder to cpu read data
wire	[31:0]	cpu_audio_rdata;		// audio to cpu read data
wire	[31:0]	cpu_video_rdata	;		//for video 1060
wire	[31:0]	cpu_servo_rdata	;		//for servo 1061   joyous
wire	[31:0]	cpu_video_in_rdata;		// VIDEO_IN to cpu read data

// PCI read cycle interface
wire            pci_ram_rreq;
wire    [29:2]  pci_ram_raddr;
wire    [1:0]   pci_ram_rbl;
wire            pci_ram_rack;
wire            pci_ram_rerr;
wire            pci_ram_rrdy;
wire            pci_ram_rlast;
wire    [31:0]  pci_ram_rdata;
// PCI Posted Write FIFO interface
wire            pci_ram_wreq;
wire    [29:2]  pci_ram_waddr;
wire            pci_ram_wack;
wire            pci_ram_werr;
wire    [31:0]  pci_ram_wdata;
wire    [1:0]   pci_ram_wbl;
wire    [3:0]   pci_ram_wbe;
wire            pci_ram_wlast;
wire            pci_ram_wrdy;

// Video interface
wire            video_ram_req;
wire            video_ram_rw;
wire    [29:2]  video_ram_addr;
wire    [3:0]   video_ram_wbe;
wire    [1:0]   video_ram_rbl;
wire            video_ram_ack;
wire            video_ram_err;
wire            video_ram_wrdy, video_ram_rrdy;
wire            video_ram_wlast, video_ram_rlast;
wire    [31:0]  video_ram_wdata, video_ram_rdata;

// Snow Yi, 2003.12.16 	// begin
wire 			out_mode_etv_sel;
wire			tv_mode;
wire			vga_800x600_mode;
// Snow Yi, 2003.12.16	// end

// Video_In interface
wire            vin_ram_req;
wire            vin_ram_rw;
wire    [29:2]  vin_ram_addr;
wire    [3:0]   vin_ram_wbe;
wire    [1:0]   vin_ram_rbl;
wire            vin_ram_ack;
wire            vin_ram_err;
wire            vin_ram_wrdy, vin_ram_rrdy;
wire            vin_ram_wlast, vin_ram_rlast;
wire    [31:0]  vin_ram_wdata, vin_ram_rdata;

// Display Engine interface
wire            disp_ram_req;
wire    [1:0]   disp_ram_rbl;
wire    [29:2]  disp_ram_addr;
wire            disp_ram_ack;
wire            disp_ram_err;
wire            disp_ram_rrdy;
wire            disp_ram_rlast;
wire    [31:0]  disp_ram_rdata;
// South Bridge Device
wire            sb_ram_req, sb_ram_ack, sb_ram_wrdy, sb_ram_wlast, sb_ram_err;
wire            sb_ram_rrdy, sb_ram_rw, sb_ram_rlast;
wire    [3:0]   sb_ram_wbe;
wire    [31:0]  sb_ram_wdata;
wire    [1:0]   sb_ram_rbl;
wire    [27:0]  sb_ram_addr;
wire    [31:0]  sb_ram_rdata;
// ---------------------------------------dsp------------------------------ //
// Local device slave interface
wire            dsp_ram_req;
wire            dsp_ram_rw;
wire    [27:0]  dsp_ram_addr;   // DWORD
wire    [1:0]   dsp_ram_rbl;    // 2'b0 means 1 DW.
wire            dsp_ram_ack;
wire            dsp_ram_err;
wire    [3:0]   dsp_ram_wbe;
wire            dsp_ram_wrdy, dsp_ram_rrdy;
wire            dsp_ram_wlast, dsp_ram_rlast;
wire    [31:0]  dsp_ram_wdata, dsp_ram_rdata;

//--------------------------------audio------------------------------------//
// Local device slave interface
wire            aud_ram_req;
wire            aud_ram_rw;
wire    [27:0]  aud_ram_addr;   // DWORD
wire    [1:0]   aud_ram_rbl;    // 2'b0 means 1 DW.
wire            aud_ram_ack;
wire            aud_ram_err;
wire    [3:0]   aud_ram_wbe;
wire            aud_ram_wrdy, aud_ram_rrdy;
wire            aud_ram_wlast, aud_ram_rlast;
wire    [31:0]  aud_ram_wdata, aud_ram_rdata;

wire			IADC_EXTINR   ;
wire			IADC_EXTINL   ;

//	-----------------------------------------------------------------------------
//Video Support Block Interface:
wire			vsb_ram_req;
wire			vsb_ram_rw;
wire[27:0]		vsb_ram_addr;
wire[1:0]		vsb_ram_rbl;
wire			vsb_ram_wlast;
wire[3:0]		vsb_ram_wbe;
wire[31:0]		vsb_ram_wdata;
wire			vsb_ram_ack;
wire			vsb_ram_err;
wire			vsb_ram_rrdy;
wire			vsb_ram_rlast;
wire[31:0]		vsb_ram_rdata;
wire			vsb_ram_wrdy;

//      --------------------------
//IDE Interface:
wire			ide_ram_req;
wire			ide_ram_rw;
wire[29:2]		ide_ram_addr;
wire[1:0]		ide_ram_rbl;
wire			ide_ram_wlast;
wire[3:0]		ide_ram_wbe;
wire[31:0]		ide_ram_wdata;
wire			ide_ram_ack;
wire			ide_ram_err;
wire			ide_ram_rrdy;
wire			ide_ram_rlast;
wire[31:0]		ide_ram_rdata;
wire			ide_ram_wrdy;

//--------------------------------
//SERVO Interface
///////////// MBUS INTERFACE
//From:
wire			sv_ram_req		;
wire			sv_ram_rw		;
wire	[29:2]	sv_ram_addr		;
wire	[1:0]	sv_ram_rbl		;
//wire	[1:0]	sv_ram_wbl		;
wire			sv_ram_wlast	;
wire	[3:0]	sv_ram_wbe		;
wire	[31:0]  sv_ram_wdata	;
//To:
wire			sv_ram_ack		;
wire			sv_ram_err		;
wire			sv_ram_rrdy		;
wire			sv_ram_rlast	;
wire	[31:0]	sv_ram_rdata	;
wire			sv_ram_wrdy		;

wire    [1:0]   mdm_pmcsr;
wire	[7:0]	sv_ecc_latency_ctrl	;
wire	[1:0]	sv_sycg_burnin		;

wire	[22:0]	sv_ex_up_in				;
wire	[22:0]	sv_ex_up_out			;
wire	[22:0]	sv_ex_up_oe_			;
// -------------------------------------------------------------------------- //
//      -------------------
//for memory interface:
wire            ctrl_ram_req;
wire            ctrl_ram_rst;
wire[1:0]       ctrl_ram_cmd;
wire[7:0]       ram_ref_init;
wire            ram_16bits_mode;
//wire			ram_pre_oe_en;
wire			ram_post_oe_en;
wire			ram_r2w_ctrl;
wire            ram_ba_width;
wire[1:0]       ram_ra_width;
wire[1:0]       ram_ca_width;
wire[1:0]		ram_row1_ra_width;
wire[1:0]		ram_row1_ca_width;
wire            ram_row0_active;
wire            ram_row1_active;
wire[15:0]      ram_timeparam;
wire[11:0]      ram_mode_set;

//      SDRAM arbiter control
wire	[21:0]	devs_reqs_num;

//wire    [1:0]   suba_arbt_ctrl;  // SDRAM sub arbiter A control
//wire    [1:0]   subb_arbt_ctrl;  // SDRAM sub arbiter B control
//wire    [1:0]   main_arbt_ctrl; // SDRAM main arbiter control

//the memory bus arbiter states:
//wire[31:0]		suba_arbt_st;		//store the latest four states of the Sub-A Arbiter
//wire[31:0]		subb_arbt_st;		//store the latest four states of the Sub-B Arbiter
//wire[31:0]		main_arbt_st;		//store the latest four states of the Main Arbiter
//for write dead lock:
wire			ram_wrlock_timer;	//1->enable the write dead lock timer
wire			ram_wrlock_resume;	//1->enable the wr-lock auto resume
wire			ram_wrlock_tag;		//1->write dead lock occur
wire[7:0]		ram_wrlock_id;		//indicate the device id that make dead lock

//	--------------------------------------------------------------------------
wire    [15:0]  intnl_int,
                int_pol_sel,    // polarity
                int_mask_sel;

wire    [19:0]  eeprom_waddr;
wire    [4:0]   fsh_cyl_width;
wire    [5:0]   eeprom_rw_cnt;

wire	rom_addr_oe_tmp_;
assign 	rom_addr_oe = ~rom_addr_oe_tmp_;

wire    sw_rst_pulse = 1'b0; //not support software reset whold chip

// Use gpio signals to control directly
// Includes: ATA reset JFR030717
wire	atareset_out_		= gpio_out[31];
wire    atareset_oe_		= gpio_oe_[31];
/*
wire	ps_cs0_			= gpio_out[31];
wire    ps_cs1_			= gpio_out[30];
wire    ps_cs0_oe_ 		= gpio_oe_[31];
wire    ps_cs1_oe_	 	= gpio_oe_[30];
*/
nb_cpu_biu NB_CPU_BIU(
        .x_rd_last              (x_rd_last              ),
        .x_wr_last              (x_wr_last              ),
        .x_ready                (x_ready                ),
        .x_data_in              (x_data_in              ),
        .x_bus_idle             (x_bus_idle             ),
        .x_bus_error            (x_bus_error            ),
        .x_ack                  (x_ack                  ),

        .u_request              (u_request              ),
        .u_write                (u_write                ),
        .u_wordsize             (u_wordsize             ),
        .u_startpaddr           (u_startpaddr           ),
        .u_data_to_send         (u_data_to_send         ),
        .u_bytemask             (u_bytemask             ),

        .endianmode             (endianmode             ),
//		.cpu_testout			(cpu_testout			),
//*******************************************************************
        .mem_wdata              (cpu_ram_wdata          ),
        .mem_rdata              (cpu_ram_rdata          ),
        .mem_wbe                (cpu_ram_wbe            ),
        .mem_addr               (cpu_ram_addr           ),
        .mem_req                (cpu_ram_req            ),
        .mem_rw                 (cpu_ram_rw             ),
        .mem_bl                 (cpu_ram_rbl[1:0]       ),
        .mem_wlast              (cpu_ram_wlast          ),
        .mem_rlast              (cpu_ram_rlast          ),
        .mem_rrdy               (cpu_ram_rrdy           ),
        .mem_wrdy               (cpu_ram_wrdy           ),
        .mem_err                (cpu_ram_err            ),
        .mem_ack                (cpu_ram_ack            ),

        .pbiu_req               (pbiu_req               ),
        .pbiu_rw                (pbiu_rw                ),
        .pbiu_rd_data           (pbiu_rd_data           ),
        .pbiu_addr              (pbiu_addr              ),
        .pbiu_bl                (pbiu_bl                ),
        .pbiu_gnt               (pbiu_gnt               ),
        .pbiu_rd_rdy            (pbiu_rd_rdy            ),
        .pbiu_last              (pbiu_last              ),
        .pbiu_err               (pbiu_err               ),
        .pbiu_wr_data           (pbiu_wr_data           ),
        .pbiu_be                (pbiu_be                ),
        .pbiu_wr_rdy            (pbiu_wr_rdy            ),

        .rom_req                (rom_req                ),
        .rom_addr               (rom_addr               ),
        .rom_gnt                (rom_gnt                ),
        .rom_rd_data            (rom_rd_data            ),
        .rom_wr_data            (rom_wr_data            ),
        .rom_rw_be              (rom_rw_be              ),
        .rom_rw                 (bootrom_rw             ),
        .rom_err                (1'b0                   ),

//        .ledpin                 (ledpin),//no use now

        .p_cfg_addr             (p_cfg_addr             ),
        .p_cfg_be               (p_cfg_be               ),
        .p_cfg_din              (p_cfg_din              ),
        .p_cfg_dout             (p_cfg_dout             ),
        .p_cfg_rw               (p_cfg_rw               ),
        .p_cfg_cs               (p_cfg_cs               ),
        .p_cfg_gnt              (p_cfg_gnt              ),

// interrupt
// South Bridge Interrupt
		.servo_int				(servo_int				),	//	SERVO interrupt
		.audio_int				(audio_int				),	//	AUDIO interrupt
		.video_int				(video_int				),	//	VIDEO interrupt

		.irc_int				(irc_int				),	//	IR interrupt
		.dsp_int				(dsp_int				),  //	DSP interrupt
		.i2c_int				(scb_int				),  //	SCB interrupt
		.uart_int				(uart_int				),	//  UART interrupt
		.ide_int_				(ide_int_				),  //	IDE interrupt
		.usb_int_				(usb_int_				),  //	USB interrupt
		.ext_int				(ext_int				),  //	PCI inta# interrupt
		.video_in_int			(video_in_int			),	//	VIDEO_IN interrupt


        .x_err_int              (x_err_int              ),
        .x_ext_int              (x_ext_int              ),

//== CPU Slave Bus
        .cpu_slave_rw           (cpu_slave_rw           ),
        .cpu_slave_addr         (cpu_slave_addr         ),
        .cpu_slave_be           (cpu_slave_be           ),
        .cpu_slave_wdata        (cpu_slave_wdata        ),

// chip select for slave device
		.cpu_audio_req			(cpu_audio_req			),	// cpu to audio io request
		.cpu_video_req			(cpu_video_req			),	// cpu to video io request
		.cpu_servo_req			(cpu_servo_req			),	// cpu to servo io request
		.cpu_sb_req				(cpu_sb_req				),	// cpu to south bridge io request
		.cpu_dsp_req			(cpu_dsp_req			),	// cpu to dsp io request
		.cpu_vsb_req			(cpu_vsb_req			),	// cpu to vsb io request
		.cpu_ide_req			(cpu_ide_req			),	// cpu to ide io request
		.cpu_tvenc_req			(cpu_tvenc_req			),	// cpu to tv encoder io request
		.cpu_video_in_req		(cpu_video_in_req		),	// cpu to video in io request

		.cpu_audio_ack			(cpu_audio_ack			),	// audio to cpu ack
		.cpu_video_ack			(cpu_video_ack			),	// video to cpu ack
		.cpu_servo_ack			(cpu_servo_ack			),	// servo to cpu ack
		.cpu_sb_ack				(cpu_sb_ack				),  // south bridge to cpu ack
		.cpu_dsp_ack			(cpu_dsp_ack			),  // DSP to CPU ack
		.cpu_vsb_ack			(cpu_vsb_ack			),  // VSB to CPU ack
		.cpu_ide_ack			(cpu_ide_ack			),  // IDE to CPU ack
		.cpu_tvenc_ack			(cpu_tvenc_ack			),  // TV encoder to CPU ack
		.cpu_video_in_ack		(cpu_video_in_ack		),	// VIDEO_IN to cpu ack

        .cpu_audio_rdata        (cpu_audio_rdata        ),
        .cpu_video_rdata        (cpu_video_rdata        ),
        .cpu_servo_rdata        (cpu_servo_rdata        ),
        .cpu_sb_rdata           (cpu_sb_rdata           ),
        .cpu_dsp_rdata			(cpu_dsp_rdata			),
        .cpu_vsb_rdata			(cpu_vsb_rdata			),
        .cpu_ide_rdata			(cpu_ide_rdata			),
        .cpu_tvenc_rdata		(cpu_tvenc_rdata		),
        .cpu_video_in_rdata		(cpu_video_in_rdata		),	// VIDEO_IN to cpu read data

// GPIO
        .gpio_out               (gpio_out               ),
        .gpio_in                (gpio_in                ),
        .gpio_oe_               (gpio_oe_               ),

        .pci_par_err            (pci_par_err            ),

//      Local Device Control
		.de_rst_				(video_rst_				), 	// Snow: 2003.11.20 RESET to VIDEO_CORE
		.dsp_rst_				(dsp_rst_				),
		.ve_rst_				(vdec_rst_				),	// Snow: 2003.11.20 RESET to VDEC, VSB and VIN
		.audio_rst_				(audio_rst_				),
		.tvenc_rst_				(tvenc_rst_				),
		.servo_rst_				(servo_rst_				),
		.vin_rst_				(vin_rst_				),

// Power Manager, Gated clock
		.dsp_clk_en				(dsp_clk_en				),
		.ve_clk_en				(ve_clk_en				),
		.servo_clk_en			(servo_clk_en			),	// SERVO clock enable
		.tvenc_clk_en			(tvenc_clk_en			),	// TV encoder
		.video_in_clk_en		(video_in_clk_en		),	// VIDEO_IN clock enable

//      PLL control
		.cpu_clk_pll_m_value	(cpu_clk_pll_m_value	),	// new cpu_clk_pll m parameter
		.cpu_clk_pll_m_tri      (cpu_clk_pll_m_tri		),  // cpu_clk_pll m modification trigger
		.cpu_clk_pll_m_ack      (cpu_clk_pll_m_ack		),  // cpu_clk_pll m modification ack

		.mem_clk_pll_m_value    (mem_clk_pll_m_value	),  // new mem_clk_pll m parameter
		.mem_clk_pll_m_tri      (mem_clk_pll_m_tri		),  // mem_clk_pll m modification trigger
		.mem_clk_pll_m_ack      (mem_clk_pll_m_ack		),  // mem_clk_pll m modification ack

		.pci_fs_value 			(pci_fs_value			),	// new pci frequency select parameter
		.pci_fs_tri             (pci_fs_tri				),	// pci frequency select modification trigger
		.pci_fs_ack             (pci_fs_ack				),	// pci frequency select modification ack

		.work_mode_value		(work_mode_value		),	// new work_mode
		.work_mode_tri  		(work_mode_tri			),  // work_mode modification trigger
		.work_mode_ack  		(work_mode_ack			),  // work_mode modification ack
        //      clock frequency control, clock misc control
		.dsp_fs					(dsp_fs					),	// dsp clock frequency select
		.dsp_div_src_sel		(dsp_div_src_sel		),	// dsp source clock select
		.ve_fs					(ve_fs					),	// ve clock frequency select
		.pci_clk_out_sel		(pci_clk_out_sel		),
		.svga_mode_en			(svga_mode_en			),	// SVGA mode output enable
		.tv_enc_clk_sel			(tv_enc_clk_sel			),	// TV encoder clock setting
        //      -----------------------------------------------------------
        // SDRAM cmd & parameter
        .ctrl_ram_req           (ctrl_ram_req       	),  // SDRAM CMD request
        .ack_ram_cmd            (ctrl_ram_rst       	),  // SDRAM CMD ack
        .ctrl_ram_cmd           (ctrl_ram_cmd       	),  // SDRAM CMD
        .ram_ref_init           (ram_ref_init       	),  // Refresh rate register
        .ram_16bits_mode        (ram_16bits_mode    	),  // 1 -> RAM in 16 bits mode, 0 -> RAM in 32 bits mode
		.ram_post_oe_en			(ram_post_oe_en			),	// ram data oe post drive enable
        .ram_ba_width           (ram_ba_width       	),  // SDRAM bank address width
        .ram_ra_width           (ram_ra_width       	),  // SDRAM row address width
        .ram_ca_width           (ram_ca_width       	),  // SDRAM col address width
        .ram_row0_active        (ram_row0_active    	),  // SDRAM row0 active, hardware to 1
        .ram_row1_active        (ram_row1_active    	),  // SDRAM row1 active, hardware to 0 in M6304
        .ram_timeparam          (ram_timeparam      	),  // SDRAM timing parameter
        .ram_mode_set           (ram_mode_set       	),  // SDRAM mode set value
		.ram_r2w_ctrl			(ram_r2w_ctrl			),	// sdram read to write ture around 0-> 2T, 1 -> 1T
        //Flash:
        .fsh_acc_en_            (rom_en_            	),  // out to pad_misc and to eeprom
        .fsh_wr_en              (eeprom_wr_en       	),  // Flash write enable
        .fsh_cyl_width          (eeprom_rw_cnt      	),  // Flash read/write cycle width counter

        //      arbiter control
        .devs_reqs_num			(devs_reqs_num			),
		.ram_wrlock_timer_en	(ram_wrlock_timer		),	//
		.ram_wrlock_resume_en	(ram_wrlock_resume		),	//
		.ram_wrlock_tag			(ram_wrlock_tag			),	//
		.ram_wrlock_id			(ram_wrlock_id			),	// SDRAM write deadlock device

//	external interface control
		.audio_pll_fout_sel		(audio_pll_fout_sel		),	// Audio PLL FOUT frequency select
		.i2s_mclk_sel			(i2s_mclk_sel			),	// I2S MCLK source select
		.i2s_mclk_out_en_		(i2s_mclk_out_en_		),	// I2S MCLK output enable
		.spdif_clk_sel			(spdif_clk_sel			),	// S/PDIF clock select
		.hv_sync_en				(hv_sync_en				),	// h_sync_, v_sync_ enable on the xsflag[1:0] pins
		.i2si_data_en			(i2si_data_en			),	// I2S input data enable on the gpio[7] pin
		.scb_en					(scb_en					),	// SCB interface enable on the gpio[6:5] pins
		.servo_gpio_en			(servo_gpio_en			),	// servo_gpio[2:0] enable on the gpio[2:0] pins
		.ext_ide_device_en		(ext_ide_device_en		),	// external IDE device enable
		.pci_gntb_en			(pci_gntb_en			),	// PCI GNTB# enable
		.vdata_en				(vdata_en				),	// video data enable
		.sync_source_sel		(sync_source_sel		),	// sync signal source select, 0 -> TVENC, 1 -> DE.
		.video_in_intf_en		(video_in_intf_en		),	// video in interface enable
		.pci_mode_ide_en		(pci_mode_ide_en		),	// pci_mode ide interface enable
		.dvb_spi_en				(dvb_spi_en				),	// DVB SPI interface enable

//	SERVO control
		.sv_tst_pin_en				(sv_tst_pin_en			),	// SERVO test pin enable
		.sv_ide_mode_part0_en		(sv_ide_mode_part0_en	),	// ide mode SERVO test pin part 0 enable
		.sv_ide_mode_part1_en		(sv_ide_mode_part1_en	),	// ide mode SERVO test pin part 1 enable
		.sv_ide_mode_part2_en		(sv_ide_mode_part2_en	),	// ide mode SERVO test pin part 2 enable
		.sv_ide_mode_part3_en		(sv_ide_mode_part3_en	),	// ide mode SERVO test pin part 3 enable
		.sv_c2ftstin_en				(sv_c2ftstin_en			),	// SERVO c2 flag enable
		.sv_sycg_updbgout			(sv_sycg_updbgout		),	// SERVO cpu interface debug enable
		.sv_trg_plcken				(sv_trg_plcken			),	// SERVO tslrf and tplck enable
		.sv_pmsvd_trcclk_trfclk_en	(sv_pmsvd_trcclk_trfclk_en),	// SERVO pmsvd_trcclk, pmsvd_trfclk enable
		.sv_ecc_latency_ctrl		(sv_ecc_latency_ctrl	),
		.sv_sycg_burnin				(sv_sycg_burnin			),	// SERVO burn in test control

    //------------------------------------------------------------
        .mem_dly_sel            (mem_dly_sel        	),
        .mem_rd_dly_sel			(mem_rd_dly_sel			),
		.test_dly_chain_sel		(test_dly_chain_sel		),	// TEST_DLY_CHAIN delay select
		.test_dly_chain_flag	(test_dly_chain_flag	),	// TEST_DLY_CHAIN flag output
        .strap_pin_in           (pad_boot_config    	),

        .cpu_clk                (cpu_clk            	),  // CPU clock
        .pci_clk                (pci_clk            	),  // PCI Clock
        .mem_clk                (mem_clk            	),  // Chipset (Mem) clock
        .test_mode              (test_mode          	),

        `ifdef	INSERT_SCAN_NB
        .test_se				(1'b0					),
        `endif

`ifdef  SCAN_EN
		.TEST_SE				(TEST_SE				),
		.TEST_SI1				(TEST_SI[0]		),
		.TEST_SI2				(TEST_SI[1]		),
		.TEST_SI3				(TEST_SI[2]		),
		.TEST_SO1				(TEST_SO[0]		),
		.TEST_SO2				(TEST_SO[1]		),
		.TEST_SO3				(TEST_SO[2]		),
`endif

        .rst_                   (rst_               )
                );


//// Boot Rom ////
wire [21:0]	eeprom_addr_tmp		;
assign		eeprom_addr		=	eeprom_addr_tmp[20:0];
eeprom EEPROM           (
        .EEPROM_ADDR            (eeprom_addr_tmp	),
        .EEPROM_RDATA           (eeprom_rdata      	),
        .EEPROM_WDATA           (eeprom_wdata      	),
        .EEPROM_DATA_OEJ        (rom_data_oe_      	),
        .EEPROM_ADDR_OEJ        (rom_addr_oe_tmp_   ),
        .EEPROM_OEJ             (eeprom_oe_         ),
        .EEPROM_CSJ             (eeprom_ce_         ),
        .EEPROM_WEJ             (eeprom_we_         ),
        .XXX_EEPROM_ACK         (rom_gnt            ),
        .XXX_EEPROM_RDATA       (rom_rd_data        ),
        .XXX_EEPROM_WDATA       (rom_wr_data        ),
        .XXX_EEPROM_ADDR        (rom_addr[21:2]     ),
        .XXX_EEPROM_BE          (rom_rw_be          ),
//      .bootrom_cs             (bootrom_ce         ),
        .XXX_EEPROM_REQ         (rom_req            ),
        .XXX_EEPROM_RW          (bootrom_rw         ),
//        .mips_boot_sel          (1'b0             ),
        .EEPROM_WR_EN           (eeprom_wr_en       ),
        .EEPROM_ACCESS_ENJ		(rom_en_			),
        .EEPROM_RW_CNT          (eeprom_rw_cnt      ),
        .CLK	                (mem_clk            ),
`ifdef  SCAN_EN
		.TEST_SE				(TEST_SE			),
		.TEST_SI				(TEST_SI[24]		),
		.TEST_SO				(TEST_SO[24]		),
`endif
        .RSTJ                   (rst_               )
         );

//======================== invoke Memory Interface===============
mem_intf MEM_INTF(
        //CPU interface:
       	//From:
        .CPU_RAM_REQ            (cpu_ram_req            ),
        .CPU_RAM_RW             (cpu_ram_rw             ),
        .CPU_RAM_ADDR           (cpu_ram_addr[29:2]     ),
        .CPU_RAM_RBL            (cpu_ram_rbl			),
        .CPU_RAM_WLAST          (cpu_ram_wlast          ),
        .CPU_RAM_WBE            (cpu_ram_wbe            ),
        .CPU_RAM_WDATA          (cpu_ram_wdata          ),
        //To:
        .CPU_RAM_ACK            (cpu_ram_ack            ),
        .CPU_RAM_ERR            (cpu_ram_err            ),
        .CPU_RAM_RRDY           (cpu_ram_rrdy           ),
        .CPU_RAM_RLAST          (cpu_ram_rlast          ),
        .CPU_RAM_RDATA          (cpu_ram_rdata          ),
        .CPU_RAM_WRDY           (cpu_ram_wrdy           ),
        //      ---------------------------------------
        //PCI Read Buffer Interface:
        //From:
        .PCI_RAM_RREQ           (pci_ram_rreq           ),
        .PCI_RAM_RADDR          (pci_ram_raddr[29:2]    ),
        .PCI_RAM_RBL            (pci_ram_rbl[1:0]		),
        //To:
        .PCI_RAM_RACK           (pci_ram_rack           ),
        .PCI_RAM_RERR           (pci_ram_rerr           ),
        .PCI_RAM_RRDY           (pci_ram_rrdy           ),
        .PCI_RAM_RLAST          (pci_ram_rlast          ),
        .PCI_RAM_RDATA          (pci_ram_rdata          ),
        //      ---------------------------------------
        //PCI Write Buffer Interface:
        //From:
        .PCI_RAM_WREQ           (pci_ram_wreq           ),
        .PCI_RAM_WADDR          (pci_ram_waddr[29:2]    ),
        .PCI_RAM_WLAST          (pci_ram_wlast          ),
        .PCI_RAM_WBE            (pci_ram_wbe            ),
        .PCI_RAM_WDATA          (pci_ram_wdata          ),
        //To:
        .PCI_RAM_WACK           (pci_ram_wack           ),
        .PCI_RAM_WERR           (pci_ram_werr           ),
        .PCI_RAM_WRDY           (pci_ram_wrdy           ),
        //      ---------------------------------------
//      --------------------------------------------
        //config interface:
        .CTRL_RAM_REQ           (ctrl_ram_req           ),
        .CTRL_RAM_CMD           (ctrl_ram_cmd           ),
        .CTRL_RAM_RST           (ctrl_ram_rst           ),
        //Auto-Refresh Rate:
        .RAM_REF_INIT           (ram_ref_init           ),
		//Mapping:
		.RAM_ROW1_ACTIVE		(ram_row1_active        ),
		.RAM_ROW0_ACTIVE		(ram_row0_active        ),
		.RAM_ROW0_BA_WIDTH		(ram_ba_width           ),
		.RAM_ROW0_RA_WIDTH		(ram_ra_width           ),
		.RAM_ROW0_CA_WIDTH		(ram_ca_width           ),
		.RAM_ROW1_BA_WIDTH		(1'b0					),
		.RAM_ROW1_RA_WIDTH		(2'b0					),
		.RAM_ROW1_CA_WIDTH		(2'b0					),
		//Timing Parameters:
        .RAM_TIMEPARAM          (ram_timeparam          ),
        .RAM_MODE_SET           (ram_mode_set           ),
        .RAM_16BITS_MODE        (ram_16bits_mode        ),
//		.RAM_PRE_OE_EN			(ram_pre_oe_en			),
		.RAM_POST_OE_EN			(ram_post_oe_en			),
       	.RAM_R2W_CTRL			(ram_r2w_ctrl			),
       	.DEVS_REQS_NUM			(devs_reqs_num			),//(22'h3f_f2ff	),
       	//for arbiter
//		.SUBA_ARBT_CTRL			(2'b0			),
//		.SUBB_ARBT_CTRL			(2'b0			),
//		.MAIN_ARBT_CTRL			(2'b0			),
		//the memory bus arbiter states:
//		.SUBA_ARBT_ST			(suba_arbt_st			),	//store the latest four states of the Sub-A Arbiter
//		.SUBB_ARBT_ST			(subb_arbt_st			),	//store the latest four states of the Sub-B Arbiter
//		.MAIN_ARBT_ST			(main_arbt_st			),	//store the latest four states of the Main Arbiter
		//for write dead lock:
		.RAM_WRLOCK_TIMER		(1'b0					),	//used to enable the write dead lock timer
		.RAM_WRLOCK_RESUME		(ram_wrlock_resume		),	//used to enable exiting from write dead lock state
//		.RAM_WRLOCK_TAG			(ram_wrlock_tag			),	//report the write dead lock error
//		.RAM_WRLOCK_ID			(ram_wrlock_id			),	//report the write dead lock device id
        //      ---------------------------------------
        //SDR SDRAM memory interface:
        .RAM_CKE                (ram_cke                ),
        .RAM_CS_                (ram_cs_                ),
        .RAM_RAS_               (ram_ras_               ),
        .RAM_CAS_               (ram_cas_               ),
        .RAM_WE_                (ram_we_                ),
        .RAM_DQM                (ram_dqm                ),
        .RAM_BA                 (ram_ba                 ),
        .RAM_ADDR               (ram_addr               ),
        .RAM_WDATA              (ram_dq_out             ),
        .RAM_RDATA              (ram_dq_in              ),
        .RAM_DQ_OE_             (ram_dq_oe_             ),
        //      ---------------------------------------
        //others:
        `ifdef	INSERT_SCAN_MEM
        .TEST_SE				(1'b0					),
        `endif
`ifdef  SCAN_EN
		.TEST_SE				(TEST_SE				),
		.TEST_SI1				(TEST_SI[3]				),
		.TEST_SI2				(TEST_SI[4]				),
		.TEST_SI3				(TEST_SI[5]				),
		.TEST_SI4				(TEST_SI[6]				),
		.TEST_SO1				(TEST_SO[3]				),
		.TEST_SO2				(TEST_SO[4]				),
		.TEST_SO3				(TEST_SO[5]				),
		.TEST_SO4				(TEST_SO[6]				),
`endif
        .RST_                   (rst_                   ),
        .MEM_CLK                (mem_clk                )
        );


//=================================================================

//===================invoke PCI Bus Unit===========================

p_biu   P_BIU(
        .OE_AD_                 (oe_ad_                 ),
        .OUT_AD                 (out_ad                 ),
        .OE_CBE_                (oe_cbe_                ),
        .OUT_CBE_               (out_cbe_               ),
        .OE_FRAME_              (oe_frame_              ),
        .OUT_FRAME_             (out_frame_             ),
        .OE_IRDY_               (oe_irdy_               ),
        .OUT_IRDY_              (out_irdy_              ),
        .OE_DEVSEL_             (oe_devsel_             ),
        .OUT_DEVSEL_            (out_devsel_            ),
        .OE_TRDY_               (oe_trdy_               ),
        .OUT_TRDY_              (out_trdy_              ),
        .OE_STOP_               (oe_stop_               ),
        .OUT_STOP_              (out_stop_              ),
        .OE_PAR_                (oe_par_                ),
        .OUT_PAR                (out_par                ),
        .OUT_GNT_               (out_gnt_               ),

        .USB_INT_               (usb_int_               ),      // Snow 2000-04-30

// Input from External Bus
        .IN_AD                  (in_ad                  ),
        .IN_CBE_                (in_cbe_                ),
        .IN_FRAME_              (in_frame_              ),
        .IN_IRDY_               (in_irdy_               ),
        .IN_DEVSEL_             (in_devsel_             ),
        .IN_TRDY_               (in_trdy_               ),
        .IN_STOP_               (in_stop_               ),
        .IN_PAR                 (in_par                 ),
        .IN_REQ_                (in_req_                ),

// Bus Decode
        .PBIU_REQ               (pbiu_req               ),
        .PBIU_RW                (pbiu_rw                ),
        .PBIU_RD_DATA           (pbiu_rd_data           ),
        .PBIU_ADDR              (pbiu_addr              ),
        .PBIU_BL                (pbiu_bl                ),
        .PBIU_GNT               (pbiu_gnt               ),
        .PBIU_RD_RDY            (pbiu_rd_rdy            ),
        .PBIU_LAST              (pbiu_last              ),
        .PBIU_ERR               (pbiu_err               ),
        .PBIU_WR_DATA           (pbiu_wr_data           ),
        .PBIU_BE                (pbiu_be                ),
        .PBIU_WR_RDY            (pbiu_wr_rdy            ),

// Memory Interface

// PCI slave
        .PCI_RAM_RREQ           (pci_ram_rreq           ),
        .PCI_RAM_RDATA          (pci_ram_rdata          ),
        .PCI_RAM_RADDR          (pci_ram_raddr          ),
        .PCI_RAM_RBL            (pci_ram_rbl[1:0]       ),
        .PCI_RAM_RACK           (pci_ram_rack           ),
        .PCI_RAM_RRDY           (pci_ram_rrdy           ),
        .PCI_RAM_RLAST          (pci_ram_rlast          ),
        .PCI_RAM_RERR           (pci_ram_rerr           ),
// PCI Posted Write FIFO interface
        .PCI_RAM_WREQ           (pci_ram_wreq           ),
        .PCI_RAM_WDATA          (pci_ram_wdata          ),
        .PCI_RAM_WADDR          (pci_ram_waddr          ),
        .PCI_RAM_WBL            (pci_ram_wbl            ),
        .PCI_RAM_WBE            (pci_ram_wbe            ),
        .PCI_RAM_WACK           (pci_ram_wack           ),
        .PCI_RAM_WRDY           (pci_ram_wrdy           ),
        .PCI_RAM_WLAST          (pci_ram_wlast          ),
        .PCI_RAM_WERR           (pci_ram_werr           ),

        .P_CFG_ADDR             (p_cfg_addr             ),
        .P_CFG_BE               (p_cfg_be               ),
        .P_CFG_DIN              (p_cfg_din              ),
        .P_CFG_DOUT             (p_cfg_dout             ),
        .P_CFG_RW               (p_cfg_rw               ),
        .P_CFG_CS               (p_cfg_cs               ),
        .P_CFG_GNT              (p_cfg_gnt              ),

// eric000430: USB added below
        .P1_LS_MODE             (p1_ls_mode             ),      // to USB pad
        .P1_TXD                 (p1_txd                 ),
        .P1_TXD_OEJ             (p1_txd_oej             ),
        .P1_TXD_SE0             (p1_txd_se0             ),
        .P2_LS_MODE             (p2_ls_mode             ),
        .P2_TXD                 (p2_txd                 ),
        .P2_TXD_OEJ             (p2_txd_oej             ),
        .P2_TXD_SE0             (p2_txd_se0             ),
 //       .p3_ls_mode             (p3_ls_mode             ),      // to USB pad
 //       .p3_txd                 (p3_txd                 ),
 //       .p3_txd_oej             (p3_txd_oej             ),
 //       .p3_txd_se0             (p3_txd_se0             ),
 //       .p4_ls_mode             (p4_ls_mode             ),
 //       .p4_txd                 (p4_txd                 ),
 //       .p4_txd_oej             (p4_txd_oej             ),
 //       .p4_txd_se0             (p4_txd_se0             ),
        .USB_PON_               (usb_pon_               ),      // power control
        .I_P1_RXD               (i_p1_rxd               ),      // from USB pad
        .I_P1_RXD_SE0           (i_p1_rxd_se0           ),
        .I_P2_RXD               (i_p2_rxd               ),
        .I_P2_RXD_SE0           (i_p2_rxd_se0           ),
 //       .i_p3_rxd               (i_p3_rxd               ),      // from USB pad
 //       .i_p3_rxd_se0           (i_p3_rxd_se0           ),
 //       .i_p4_rxd               (i_p4_rxd               ),
 //       .i_p4_rxd_se0           (i_p4_rxd_se0           ),
        .USB_CLK                (usb_clk                ),      // 48M clock for USB, from PLL
        .USB_OVERCUR_           (usb_overcur_           ),      // over current input

//        .USB_CLK_EN             (usb_clk_en             ),      // controll USB clock
// eric000430: USB added above

// Others
		.PCI_MODE				(pci_mode				),
		.PCI_MODE_IDE_EN		(pci_mode_ide_en		),
        .PINS_STOP	            (IDE_STOP				),
        .PINS_IDLE	            (IDE_IDLE				),
//        .pad_boot_config        (pad_boot_config        ),
       	.PCI_PAR_ERR            (pci_par_err            ),
        .PCI_CLK                (pci_clk        		),
        .MEM_CLK                (mem_clk        		),
        .TESTMD                 (test_mode      		),
`ifdef	INSERT_SCAN_PBIU
		.TEST_SE				(1'b0					),
`endif
`ifdef  SCAN_EN
		.TEST_SE				(TEST_SE				),
		.TEST_SI1				(TEST_SI[7]				),
		.TEST_SI2				(TEST_SI[8]				),
		.TEST_SI3				(TEST_SI[9]				),
		.TEST_SI4				(TEST_SI[10]			),
		.TEST_SI5				(TEST_SI[11]			),
		.TEST_SI6				(TEST_SI[12]			),

		.TEST_SO1				(TEST_SO[7]				),
		.TEST_SO2				(TEST_SO[8]				),
		.TEST_SO3				(TEST_SO[9]				),
		.TEST_SO4				(TEST_SO[10]			),
		.TEST_SO5				(TEST_SO[11]			),
		.TEST_SO6				(TEST_SO[12]			),
`endif
        .RST_                   (rst_           )
        );
//===================================================================


endmodule
