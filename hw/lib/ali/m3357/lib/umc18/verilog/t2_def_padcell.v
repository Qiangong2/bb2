//t2_define_padcell
//Note:	All the padcell in this file is not standard cell of TSMC.18 ASIC LIB.
//2002-10-11, initial version, Yuky
//2003-09-27, add  ,Jeffrey

// type: PCORNERDG
`celldefine
module PCORNERDG (VDD,	VSS );
    inout   VSS;
    supply0 VSS;
    inout	VDD;
    supply1	VDD;
endmodule
`endcelldefine

// type: PCORNERDG_L
`celldefine
module PCORNERDG_L (VDD,	VSS );
    inout   VSS;
    supply0 VSS;
    inout	VDD;
    supply1	VDD;
endmodule
`endcelldefine

// type: PVDDx
// Note: This PAD did not exist, Just for PLL analog power temporary
`celldefine
module PVDD0DGZ (VDD);
    inout   VDD;
    supply1 VDD;
endmodule
`endcelldefine

// type: PVSSx
// Note: This PAD did not exist, Just for PLL analog power temporary
`celldefine
module PVSS0DGZ (VSS);
    inout   VSS;
    supply0 VSS;
endmodule
`endcelldefine

//----
// The following PAD is for TV Encoder analog pin temporary
// type: PRDIODE
`celldefine
module PRDIODE ();
//power cut cell
endmodule
`endcelldefine

// type: PVDD1A
`celldefine
module PVDD1A  (TVDD1A,VD33, VSSPST );
    input VD33;
    buf I1 (VD33_buf, VD33);
    input VSSPST;
    buf I3 (VSSPST_buf, VSSPST);
    input   TVDD1A;
    supply1 TVDD1A;
endmodule
`endcelldefine
/*$Id: t2_def_padcell.v,v 1.1.1.1 2004/01/14 20:10:31 psmith Exp $*/
/*$Name:  $*/


// type: PVDD1P
`celldefine
module PVDD1P  (TVDD1P,TAVDD, TAVSS );
    input TAVDD;
    buf I1 (TAVDD_buf, TAVDD);
    input TAVSS;
    buf I3 (TAVSS_buf, TAVSS);
    input   TVDD1P;
    supply1 TVDD1P;
endmodule
`endcelldefine
/*$Id: t2_def_padcell.v,v 1.1.1.1 2004/01/14 20:10:31 psmith Exp $*/
/*$Name:  $*/

// type: PVDD1P1
`celldefine
module PVDD1P1  (TVDD1P1,TAVD33, TAVSSPST );
    input TAVD33;
    buf I1 (TAVD33_buf, TAVD33);
    input TAVSSPST;
    buf I3 (TAVSSPST_buf, TAVSSPST);
    input   TVDD1P1;
    supply1 TVDD1P1;
endmodule
`endcelldefine
/*$Id: t2_def_padcell.v,v 1.1.1.1 2004/01/14 20:10:31 psmith Exp $*/
/*$Name:  $*/


// type: PNOPIN
`celldefine
module PVDD2P  (TAVD33, TAVSSPST );
    input TAVD33;
    buf I1 (TAVD33_buf, TAVD33);
    input TAVSSPST;
    buf I2 (TAVSSPST_buf, TAVSSPST);
endmodule
`endcelldefine
/*$Id: t2_def_padcell.v,v 1.1.1.1 2004/01/14 20:10:31 psmith Exp $*/
/*$Name:  $*/


// type: PVDD3P
`celldefine
module PVDD3P  (TAVDD,TAVSS );
    input TAVSS;
    buf I2 (TAVSS_buf, TAVSS);
    input   TAVDD;
    supply1 TAVDD;
endmodule
`endcelldefine
/*$Id: t2_def_padcell.v,v 1.1.1.1 2004/01/14 20:10:31 psmith Exp $*/
/*$Name:  $*/


// type: PNOPIN
`celldefine
module PVDD4P  (TAVD33, TAVDD, TAVSSPST );
    input TAVD33;
    buf I1 (TAVD33_buf, TAVD33);
    input TAVDD;
    buf I2 (TAVDD_buf, TAVDD);
    input TAVSSPST;
    buf I3 (TAVSSPST_buf, TAVSSPST);
endmodule
`endcelldefine
/*$Id: t2_def_padcell.v,v 1.1.1.1 2004/01/14 20:10:31 psmith Exp $*/
/*$Name:  $*/


// type: PNOPIN
`celldefine
module PVDD5P  (TAVDD, TAVSS );
    input TAVDD;
    buf I1 (TAVDD_buf, TAVDD);
    input TAVSS;
    buf I2 (TAVSS_buf, TAVSS);
endmodule
`endcelldefine

// type: PVDD1A
`celldefine
module PVDD1A  (TVDD1A,VD33, VSSPST );
    input VD33;
    buf I1 (VD33_buf, VD33);
    input VSSPST;
    buf I3 (VSSPST_buf, VSSPST);
    input   TVDD1A;
    supply1 TVDD1A;
endmodule

// type: PVSS1A
`celldefine
module PVSS1A  (TVSS1A,VD33, VSSPST, );
    input VD33;
    buf I1 (VD33_buf, VD33);
    input VSSPST;
    buf I2 (VSSPST_buf, VSSPST);
    input   TVSS1A;
    supply0 TVSS1A;
endmodule
`endcelldefine
/*$Id: t2_def_padcell.v,v 1.1.1.1 2004/01/14 20:10:31 psmith Exp $*/
/*$Name:  $*/


// type: PVSS1P
`celldefine
module PVSS1P  (TVSS1P,TAVDD, TAVSS, );
    input TAVDD;
    buf I1 (TAVDD_buf, TAVDD);
    input TAVSS;
    buf I2 (TAVSS_buf, TAVSS);
    input   TVSS1P;
    supply0 TVSS1P;
endmodule
`endcelldefine
/*$Id: t2_def_padcell.v,v 1.1.1.1 2004/01/14 20:10:31 psmith Exp $*/
/*$Name:  $*/


// type: PVSS1P1
`celldefine
module PVSS1P1  (TVSS1P1,TAVD33, TAVSSPST, );
    input TAVD33;
    buf I1 (TAVD33_buf, TAVD33);
    input TAVSSPST;
    buf I2 (TAVSSPST_buf, TAVSSPST);
    input   TVSS1P1;
    supply0 TVSS1P1;
endmodule
`endcelldefine
/*$Id: t2_def_padcell.v,v 1.1.1.1 2004/01/14 20:10:31 psmith Exp $*/
/*$Name:  $*/


// type: PNOPIN
`celldefine
module PVSS2P  (TAVD33, TAVSSPST );
    input TAVD33;
    buf I1 (TAVD33_buf, TAVD33);
    input TAVSSPST;
    buf I2 (TAVSSPST_buf, TAVSSPST);
endmodule
`endcelldefine
/*$Id: t2_def_padcell.v,v 1.1.1.1 2004/01/14 20:10:31 psmith Exp $*/
/*$Name:  $*/


// type: PVSS3P
`celldefine
module PVSS3P  (TAVSS,TAVDD, );
    input TAVDD;
    buf I1 (TAVDD_buf, TAVDD);
    input   TAVSS;
    supply0 TAVSS;
endmodule
`endcelldefine
/*$Id: t2_def_padcell.v,v 1.1.1.1 2004/01/14 20:10:31 psmith Exp $*/
/*$Name:  $*/


// type: PNOPIN
`celldefine
module PVSS4P  (TAVD33, TAVSSPST, TAVSS );
    input TAVD33;
    buf I1 (TAVD33_buf, TAVD33);
    input TAVSSPST;
    buf I2 (TAVSSPST_buf, TAVSSPST);
    input TAVSS;
    buf I3 (TAVSS_buf, TAVSS);
endmodule
`endcelldefine
/*$Id: t2_def_padcell.v,v 1.1.1.1 2004/01/14 20:10:31 psmith Exp $*/
/*$Name:  $*/


// type: PNOPIN
`celldefine
module PVSS5P  (TAVDD, TAVSS );
    input TAVDD;
    buf I1 (TAVDD_buf, TAVDD);
    input TAVSS;
    buf I2 (TAVSS_buf, TAVSS);
endmodule
`endcelldefine


// type: PDIANA1P1
`celldefine
module PDIANA1P1 ( PAD , C , TAVD33 , TAVSSPST , TAVSS );
    inout PAD ;
    output C ;
    input TAVD33 ;
    input TAVSSPST ;
    input TAVSS ;
// synopsys translate_off
wire	PAD = C;
// synopsys translate_on
endmodule
`endcelldefine

// type: PDIANA1P
`celldefine
module PDIANA1P ( PAD , C , TAVD33 , TAVSSPST , TAVSS );
    inout PAD ;
    output C ;
    input TAVD33 ;
    input TAVSSPST ;
    input TAVSS ;
// synopsys translate_off
wire	PAD = C;
// synopsys translate_on
endmodule
`endcelldefine

// type: PDIANA2P
`celldefine
module PDIANA2P ( PAD , C , TAVD33 , TAVSSPST , TAVSS );
    inout PAD ;
    output C ;
    input TAVD33 ;
    input TAVSSPST ;
    input TAVSS ;
// synopsys translate_off
wire	PAD = C;
// synopsys translate_on
endmodule
`endcelldefine
// The above PAD is for TV Encoder analog pin temporary
//----

// type: USB_PAD
`celldefine
module USB_PAD ( A, ANT, DM, DP, H_LB, OEB, RCVD, SE0, SE0D,
    YNT );
input  A, ANT, H_LB, OEB, SE0 ;
output RCVD, SE0D, YNT;
inout  DM, DP;
    wire n231, n232, n233, n234, n235, n236;
    TBUFX8 DM_tri ( .A(n234), .OE(n236), .Y(DM) );
    BUFX4 C31 ( .A(DP), .Y(RCVD) );
    TBUFX4 DP_tri ( .A(n235), .OE(n236), .Y(DP) );
    OR2X4 C32 ( .A(n231), .B(n232), .Y(YNT) );
    NOR2X4 C33 ( .A(DP), .B(DM), .Y(SE0D) );
    INVX4 C34 ( .A(OEB), .Y(n236) );
    AND2X4 C35 ( .A(A), .B(n233), .Y(n235) );
    NOR2X4 C36 ( .A(SE0), .B(A), .Y(n234) );
    AND2X4 C37 ( .A(RCVD), .B(ANT), .Y(n231) );
    INVX4 C38 ( .A(DM), .Y(n232) );
    INVX4 C39 ( .A(SE0), .Y(n233) );
endmodule
`endcelldefine

// type: ANTENNA
// Description: No function use, just for electromagnetic problem
`celldefine
module	ANTENNA ( A );
input	A;

endmodule
`endcelldefine

//only for M6304 postlayout version netlist
module LAYERDEF();
	//empty module
endmodule

module LOGO();
	//empty module
endmodule

module USB_BOND_SUB();
	//empty module
endmodule

