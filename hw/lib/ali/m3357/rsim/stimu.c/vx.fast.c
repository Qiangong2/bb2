	ncxlmode  				\
	-l verilog.log			\
	+assert					\
	+noassert_synth_pragma	\
	+ieee +pathpulse 		\
	+define+verbose_2 		\
	+define+nobanner		\
	+libext+.udp+.v+.vmd 	\
	+pulse_r/100 +pulse_e/100 +no_pulse_msg \
	+nolibcell	\
	+notimingchecks		\
	+access+r \
	+mixedlang				\
	+loadpli1=fileio:fileio_register \
	+ncvlogargs+-NOMEMPACK \
	top_all.v +bus_conflict_off\
	-y ${SYNOPSYS}/dw/sim_ver \
	+incdir+${SYNOPSYS}/dw/sim_ver \
