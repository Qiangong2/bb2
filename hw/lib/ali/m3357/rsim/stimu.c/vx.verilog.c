verilog \
+incdir+${XILINX}/verilog/src \
-y ${XILINX}/verilog/src/unisims +libext+.v \
-y ${XILINX}/verilog/src/simprims +libext+.v \
-y ${XILINX}/verilog/src/XilinxCoreLib		\
-y ${SYNOPSYS}/dw/sim_ver +libext+.v \
+incdir+${SYNOPSYS}/dw/sim_ver \
+pulse_r/0 +pulse_e/0 	\
+typdelays +logpsterr +logpllerr +hier 	\
+nowarnTFNPC \
+nowarnLFSNN \
+nowarnTMREN \
+nosdfwarn	\
top_all.v	\
