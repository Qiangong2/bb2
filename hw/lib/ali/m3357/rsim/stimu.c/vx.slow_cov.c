	ncxlmode \
	-l verilog.log	\
	+delay_mode_unit \
	+noassert_synth_pragma	\
	+linedebug \
	+tcl+cov.tcl \
	+ieee +pathpulse \
	+define+verbose_2 \
	+define+nobanner	\
	+libext+.udp+.v+.vmd \
	+pulse_r/0 +pulse_e/100 +no_pulse_msg \
	+nolibcell	\
	+notimingchecks		\
	+access+r+w+c \
	+nosdfwarn	\
	+debug \
	+ncredmem	\
	+mixedlang				\
	top_all.v +bus_conflict_off\
	+ncvlogargs+-NOMEMPACK \
