#========================================	\
#========= History ======================	\
#	remove pll_clk_src.v in PLL Behavior	\
#########  cpu behavior model	#########	\
 $rsrc_dir/model/cpu/t2risc_intf.v		\
 $rsrc_dir/model/cpu/pro_task_core.v	\
 $rsrc_dir/model/cpu/pro_mux.v			\
 $rsrc_dir/model/cpu/pro_basic_tasks.v	\
 $rsrc_dir/model/cpu/pro_arbitrator.v	\
 $rsrc_dir/model/cpu/priority_switch.v	\
#### Strap Pins behavior model #######	\
 $rsrc_dir/model/north/strap_pin_bh.v	\
#### North Bridge behavior module ####	\
 $rsrc_dir/model/north/mm.v				\
 $rsrc_dir/model/north/tt.v				\
 $rsrc_dir/model/north/int_bh.v			\
 $rsrc_dir/model/north/monitor.v		\
 $rsrc_dir/model/north/boot_rom.v		\
 $rsrc_dir/model/north/dimm_bh.v  	\
 $rsrc_dir/model/north/performance.v	\
 $rsrc_dir/model/north/mt48lc1m16a1.v  	\
 $rsrc_dir/model/north/mt48lc4m16a2.v  	\
 $rsrc_dir/model/north/mt48lc8m8a2.v  	\
# $rsrc_dir/model/north/mt48lc16m8a2.v  	\
 $rsrc_dir/model/north/mt48lc16m8a2_144.v  	\
 $rsrc_dir/model/north/gpio_bh.v  		\
#### The internal bus protocol monitor ####\
 $rsrc_dir/model/north/t2risc_intf_monitor.v\
 $rsrc_dir/model/north/internal_mem_bus_monitor.v\
 $rsrc_dir/model/north/internal_local_bus_monitor.v\
#### The memory bus arbiter priority monitor ####\
 $rsrc_dir/model/north/arbta_prior_monitor.v\
 $rsrc_dir/model/north/arbtb_prior_monitor.v\
 $rsrc_dir/model/north/arbtm_prior_monitor.v\
#### The memory interface monitor ####\
 $rsrc_dir/model/north/imb_status_monitor.v\
 $rsrc_dir/model/north/sdram_dimms_monitor.v\
 $rsrc_dir/model/north/sdram_dimm_monitor.v\
 $rsrc_dir/model/north/sdram_bank_monitor.v\
#### The clock monitor	####################\
 $rsrc_dir/model/north/clock_monitor.v\
#### The pin signals test behavior module ####\
 #$rsrc_dir/model/chipset/sig_test_1.v		\
 #$rsrc_dir/model/chipset/sig_test_2.v     \
 #$rsrc_dir/model/chipset/core_bh.v        \
 #$rsrc_dir/model/chipset/ext_dev_bh.v     \
#### The device behavior model for testing SDRAM memory interface ###\
 $rsrc_dir/model/chipset/device_bh.v     \
 $rsrc_dir/model/chipset/disp_device_bh.v     \
#### North_Bridge synthesizable code start ##################	\
################# Memory controller	###############	\
 $rsrc_dir/north/sdram/mem_intf.vp                    	\
###### PCI bus BIU and Host	######		\
 $rsrc_dir/north/pbiu/p_biu.v			\
 $rsrc_dir/north/pbiu/pci_host.vp		\
####  Local bus BIU and other/top module #########\
 $rsrc_dir/north/mbiu/mbiu.vp		\
#### T2 chipset level	#########		\
 $rsrc_dir/chipset/t2_chipset.v		\
#### Hard block used by North or Chip #	\
 $rsrc_dir/hblk/chip/GATX20.v 		\
 $rsrc_dir/hblk/north/ram4x32d.v	\
 $rsrc_dir/hblk/north/ram8x32d1.v		\
 $rsrc_dir/hblk/north/ram8x32d4.v		\
###	Hard block used by servo and video	\
#### clock generator #############				\
 $rsrc_dir/hblk/chip/CPU_PLL.v 					\
 $rsrc_dir/hblk/chip/F27_PLL.v 					\
 $rsrc_dir/hblk/chip/AUDIO_PLL.v				\
 $rsrc_dir/hblk/chip/DLYCELL02.v				\
 $rsrc_dir/hblk/chip/P_DLY_CHAIN32.v			\
 $rsrc_dir/hblk/chip/TEST_DLY_CHAIN64.v			\
 $rsrc_dir/hblk/chip/clock_divider.v			\
 $rsrc_dir/hblk/chip/clock_gateway.v			\
 $rsrc_dir/hblk/chip/clock_gen.v				\
 $rsrc_dir/hblk/chip/AUDIO_CLOCK_GEN.v			\
 $rsrc_dir/hblk/chip/VIDEO_CLOCK_GEN.v			\
 $rsrc_dir/hblk/chip/VIDEO_CLK_DLY_CHAIN.v		\
####  Top moudle for smulation					\
 $rsrc_dir/chip/usb_pad.v						\
 $rsrc_dir/chip/IOPAD.v							\
 $rsrc_dir/chip/PAD_RST_STRAP.v					\
 $rsrc_dir/chip/PAD_MISC.v						\
 $rsrc_dir/chip/core.v                 			\
 $rsrc_dir/chip/chip.v							\
 $rsrc_dir/chip/scan_mux.v						\
#### temp use for IDE							\
# $rsrc_dir/ide/ide_core.v						\
#-v $lib_dir/umc18/verilog/tpz973g.v			\
#-v $lib_dir/umc18/verilog/t2_def_padcell.v		\
-v $lib_dir/umc18/verilog/UMC018AG_ABSG_005.v	\
-v $lib_dir/umc18/verilog/UMC018AG_AGSG_005.v	\
-v $rsrc_dir/hblk/chip/spg.v					\
-v $rsrc_dir/hblk/chip/SEQ_DUMMYJ.v				\
-v $rsrc_dir/hblk/chip/COMB_DUMMY.v				\
-v $lib_dir/umc18/verilog/UMC018AG_AASG_019.v

