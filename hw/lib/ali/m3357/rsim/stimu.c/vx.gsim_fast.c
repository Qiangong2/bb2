ncxlmode	\
-l verilog.log	\
+no_notifier \
+libext+.v \
+notchk \
+nosdfwarn \
+bus_conflict_off \
+sdf_verbose	\
+define+nobanner	\
+access+r\
+mixedlang				\
+ieee +defparam +pulse_x/0 +pulse_r/0 +pathpulse +define+verbose_0	\
+loadpli1=fileio:fileio_register \
+ncvlogargs+-NOMEMPACK \
-y ${SYNOPSYS}/dw/sim_ver \
+incdir+${SYNOPSYS}/dw/sim_ver \
top_all.v	\
