#									\
# Behavior module       			\
#									\
$rsim_dir/model/tvencoder.v            	\
$rsim_dir/model/dimm_bh.v            	\
$rsim_dir/model/mt48lc16m8a2.v            	\
$rsim_dir/model/mt48lc1m16a1.v            	\
$rsim_dir/model/mt48lc4m16a2.v            	\
$rsim_dir/model/mt48lc8m8a2.v            	\
$rsim_dir/model/mm.v                 	\
$rsim_dir/model/monitor.v            	\
$rsim_dir/model/tt.v						\
$rsim_dir/model/bb.v						\
$rsrc_dir/VIDEO_CORE/VIDEO_POST/vpost2dvi_bh.v	\
######################## 3rd IP ########	\
#									\
# Hard Block module    				\
$rsrc_dir/hblk/DVI_BRAM1920X8S.v    \
$rsrc_dir/hblk/DVI_BRAM960X8S.v    \
$rsrc_dir/hblk/FPGA/DVI_BRAM1920X8S_FPGA.v    \
$rsrc_dir/hblk/FPGA/DVI_BRAM960X8S_FPGA.v    \
#									\
# I2C models            			\
#									\
$rsrc_dir/i2c/ram16x8s.v       			\
$rsrc_dir/i2c/scb_reg.v         			\
$rsrc_dir/i2c/scb_master.v      			\
#$rsrc_dir/i2c/scb_sync.v        			\
$rsrc_dir/i2c/scb_fifoctrl.v      			\
$rsrc_dir/i2c/scb_glue.v				\
$rsrc_dir/i2c/scb_intf.v						\
$rsrc_dir/i2c/CPU2DVI.v						\
#	 								\
# MEM interface models 	  			\
#	  								\
$rsrc_dir/mem_intf/ram_intf/mem_intf.v                	\
$rsrc_dir/mem_intf/ram_intf/auto_ref_timer.v				\
$rsrc_dir/mem_intf/ram_intf/mbus_arbt.v		    		\
$rsrc_dir/mem_intf/ram_intf/mem_wrbus_mux.v	    		\
$rsrc_dir/mem_intf/ram_intf/ram_addrs_trans.v			\
$rsrc_dir/mem_intf/ram_intf/ram_param_dly.v	    		\
$rsrc_dir/mem_intf/ram_intf/ram_rdata_capture.v 		\
$rsrc_dir/mem_intf/sdr_ctrl/data_fifo.v			\
$rsrc_dir/mem_intf/sdr_ctrl/sdr_acce_ctrl.v			\
$rsrc_dir/mem_intf/sdr_ctrl/sdr_bus_path.v			\
$rsrc_dir/mem_intf/sdr_ctrl/sdr_ctrl_fsm.v			\
$rsrc_dir/mem_intf/sdr_ctrl/sdr_init_fsm.v			\
$rsrc_dir/mem_intf/sdr_ctrl/sdr_pg_info.v			\
$rsrc_dir/mem_intf/sdr_ctrl/sdr_pgs_info.v			\
$rsrc_dir/mem_intf/sdr_ctrl/sdr_pgs_mux.v			\
$rsrc_dir/mem_intf/sdr_ctrl/sdr_ref_fsm.v			\
$rsrc_dir/mem_intf/sdr_ctrl/sdr_sdram_ctrl.v		\
#									\
# P_BIU models              		\
#									\
$rsrc_dir/pbiu/p_biu.v                 	\
$rsrc_dir/pbiu/p_decode.v               	\
$rsrc_dir/pbiu/p_master.v               	\
$rsrc_dir/pbiu/pm_arbt.v               		\
$rsrc_dir/pbiu/p_target.v               	\
$rsrc_dir/pbiu/pci_reg.v                	\
#$rsrc_dir/pbiu/pt_add30_1.v             	\
$rsrc_dir/pbiu/pt_fifo_ctrl.v           	\
$rsrc_dir/pbiu/pt_rd_buf.v              	\
$rsrc_dir/pbiu/pt_wr_buf.v              	\
$rsrc_dir/pbiu/ram8x32d1.v              	\
$rsrc_dir/pbiu/ram8x32d4.v              	\
#					  				\
# VDEC			  		\
$rsrc_dir/VIDEO_CORE/VIDEO_DECODE/VIDEO_DECODE.v	\
# VPOST			  		\
$rsrc_dir/VIDEO_CORE/VIDEO_POST/VIDEO_POST.v	\
# ADDERSS ENGINE		\
$rsrc_dir/VIDEO_CORE/VIDEO_POST/VPOST_ADDRESS/VPOST_ADDRESS.V	\
$rsrc_dir/VIDEO_CORE/VIDEO_POST/VPOST_ADDRESS/ADDRESS_ADDER.V	\
# CMD DECODE			\
$rsrc_dir/VIDEO_CORE/VIDEO_POST/VPOST_CMD/VPOST_CMD_DECODE.V	\
# MCTRL					\
$rsrc_dir/VIDEO_CORE/VIDEO_POST/VPOST_MCTRL/VPOST_MCTRL.V	\
$rsrc_dir/VIDEO_CORE/VIDEO_POST/VPOST_MCTRL/MCTRL_ALPHA.V	\
$rsrc_dir/VIDEO_CORE/VIDEO_POST/VPOST_MCTRL/MCTRL_LINE_ACTIVE.V	\
$rsrc_dir/VIDEO_CORE/VIDEO_POST/VPOST_MCTRL/ALPHA_ADD_MUL.V	\
# MEM_REQ			\
$rsrc_dir/VIDEO_CORE/VIDEO_POST/VPOST_MEMREQ/VPOST_MEMREQ.V	\
$rsrc_dir/VIDEO_CORE/VIDEO_POST/VPOST_MEMREQ/MEMREQ_MP_REQ.V	\
$rsrc_dir/VIDEO_CORE/VIDEO_POST/VPOST_MEMREQ/REQ_ADDRESS_CHANGE.V	\
$rsrc_dir/VIDEO_CORE/VIDEO_POST/VPOST_MEMREQ/REQ_CTRL.V	\
$rsrc_dir/VIDEO_CORE/VIDEO_POST/VPOST_MEMREQ/FIFO.V	\
$rsrc_dir/VIDEO_CORE/VIDEO_POST/VPOST_MEMREQ/REQ_ARBITER.V	\
$rsrc_dir/VIDEO_CORE/VIDEO_POST/VPOST_MEMREQ/ADDRESS_ADD_MUL.V	\
$rsrc_dir/VIDEO_CORE/VIDEO_POST/VPOST_MEMREQ/VPOST_PIXEL_FIFO.V	\
$rsrc_dir/VIDEO_CORE/VIDEO_POST/VPOST_MEMREQ/VPOST_RX_CTRL.V	\
#PARAMETER CTRL		\
$rsrc_dir/VIDEO_CORE/VIDEO_POST/VPOST_PAR_CTRL/VPOST_PAR_CTRL.V	\
#SP		\
#$rsrc_dir/VIDEO_CORE/VIDEO_POST/VPOST_SP/VPOST_SP.v	\
#$rsrc_dir/VIDEO_CORE/VIDEO_POST/VPOST_SP/SP_2TAP_FILTER.v	\
#$rsrc_dir/VIDEO_CORE/VIDEO_POST/VPOST_SP/SP_COLCON_DECODER.v	\
#$rsrc_dir/VIDEO_CORE/VIDEO_POST/VPOST_SP/SP_FILTER_OPERATION.v	\
#$rsrc_dir/VIDEO_CORE/VIDEO_POST/VPOST_SP/SP_PALETTE.v	\
#$rsrc_dir/VIDEO_CORE/VIDEO_POST/VPOST_SP/SP_RAM16X24D.v	\
#$rsrc_dir/VIDEO_CORE/VIDEO_POST/VPOST_SP/SP_REG_RAM.v	\
#$rsrc_dir/VIDEO_CORE/VIDEO_POST/VPOST_SP/SP_RLC_DECODER.v	\
#OSD		\
#$rsrc_dir/VIDEO_CORE/VIDEO_POST/VPOST_OSD/OSD_ENGINE.v	\
#$rsrc_dir/VIDEO_CORE/VIDEO_POST/VPOST_OSD/OSD_RAM8X32D.v	\
#$rsrc_dir/VIDEO_CORE/VIDEO_POST/VPOST_OSD/OSD_BLEND.v	\
#$rsrc_dir/VIDEO_CORE/VIDEO_POST/VPOST_OSD/COLOR_TABLE256X24.v	\
#$rsrc_dir/VIDEO_CORE/VIDEO_POST/VPOST_OSD/RLE_DE.v	\
#FILTER		\
#$rsrc_dir/VIDEO_CORE/VIDEO_POST/VPOST_FILTER/VPOST_FILTER.V	\
#$rsrc_dir/VIDEO_CORE/VIDEO_POST/VPOST_FILTER/FILTER_H_ADDR_ENGINE.V	\
#$rsrc_dir/VIDEO_CORE/VIDEO_POST/VPOST_FILTER/FILTER_COEFF.V	\
#$rsrc_dir/VIDEO_CORE/VIDEO_POST/VPOST_FILTER/FILTER_ALU.V	\
#$rsrc_dir/VIDEO_CORE/VIDEO_POST/VPOST_FILTER/FILTER_ADDR_ADDER.V	\
#$rsrc_dir/VIDEO_CORE/VIDEO_POST/VPOST_FILTER/ALU_ADD.V	\
#$rsrc_dir/VIDEO_CORE/VIDEO_POST/VPOST_FILTER/ALU_MULT.V	\
#ADDMUL		\
$rsrc_dir/hblk/FPGA/us18add18c.v	\
$rsrc_dir/hblk/FPGA/us10add10c.v	\
$rsrc_dir/hblk/FPGA/us6add6c.v	\
$rsrc_dir/hblk/FPGA/us20add20c.v	\
$rsrc_dir/hblk/FPGA/us28add28c.v	\
$rsrc_dir/hblk/FPGA/us13add13c.v	\
$rsrc_dir/hblk/FPGA/multi6x6.v	\
$rsrc_dir/hblk/FPGA/multi8x4.v	\
#$rsrc_dir/hblk/ASIC/DW01_add.v	\
#$rsrc_dir/hblk/ASIC/DW02_mult.v	\
#$rsrc_dir/VIDEO_CORE/VIDEO_POST/vpost2dvi_bh.v	\
# VCTRL			  		\
$rsrc_dir/VIDEO_CORE/VIDEO_CTRL/VIDEO_CTRL.v	\
# DVI			  		\
$rsrc_dir/VIDEO_CORE/VIDEO_DVI/DVI_BUF_CTRL.v	\
$rsrc_dir/VIDEO_CORE/VIDEO_DVI/DVI_CMD_DEC.v	\
$rsrc_dir/VIDEO_CORE/VIDEO_DVI/DVI_OUTPUT_CTRL.v	\
$rsrc_dir/VIDEO_CORE/VIDEO_DVI/DVI_SEQUENCE.v	\
$rsrc_dir/VIDEO_CORE/VIDEO_DVI/DVI_SLAVE_SYNC.v	\
$rsrc_dir/VIDEO_CORE/VIDEO_DVI/VIDEO_DVI.v	\
# SYNC			  		\
$rsrc_dir/VIDEO_CORE/VIDEO_IOSYNC/VIDEO_IOSYNCF.v	\
$rsrc_dir/VIDEO_CORE/VIDEO_IOSYNC/VIDEO_IOSYNCS.v	\
$rsrc_dir/VIDEO_CORE/VIDEO_MEMSYNC/VDEC_REG4x33DP.v	\
$rsrc_dir/VIDEO_CORE/VIDEO_MEMSYNC/VDEC_REG4x37DP.v	\
$rsrc_dir/VIDEO_CORE/VIDEO_MEMSYNC/VMEM_GRAY.v	\
$rsrc_dir/VIDEO_CORE/VIDEO_MEMSYNC/VMEM_RD_SYNC.v	\
$rsrc_dir/VIDEO_CORE/VIDEO_MEMSYNC/VMEM_WR_SYNC.v	\
$rsrc_dir/VIDEO_CORE/VIDEO_MEMSYNC/VIDEO_MEMSYNC.v	\
#					  				\
$rsrc_dir/VIDEO_CORE/VIDEO_CORE.v	\
$rsrc_dir/chip/pci_video_sync.v		\
$rsrc_dir/chip/video_chip.v

