ncxlmode	\
-l verilog.log	\
+loadpli1=ttlib:tt_bootstrap\
+loadpli1=tclib:tc_bootstrap\
+no_notifier \
+libext+.v \
+notchk \
+nosdfwarn \
+bus_conflict_off \
+debug \
+sdf_verbose	\
+define+nobanner	\
+access+r+w\
+ieee +defparam +pulse_x/0 +pulse_r/0 +pathpulse +define+verbose_0	\
+loadpli1=fileio:fileio_register \
+ncvlogargs+-NOMEMPACK \
top_all.v	\
