
//==========================================================
// Used to define the parameters at the top.v
// Author:	yuky
// history:	2002-06-07 	first version
//			2002-10-09 	modify the strap pins
//						Add IDE local io base and TV encoder local io base
//==========================================================
//for pci host bridge config, yuky,2002-06-19
parameter   CHIPSET_FUNC = 3'h0;       	// Chipset PCI CFG function No.
parameter   CHIPSET_IDSEL = 5'h01;     	// Chipset PCI CFG Addr offset

//for sdram initial
defparam	SDRAM_DQ_Mode		= 1'b1;	//0->32bits mode, 1->16bits mode, define in top.v first
`ifdef NEW_MEM
	//	use M3357 parameter
parameter	SDRAM_Cas_Latency	=	3'h3,	// Tcl	time	default is 3clk 2 -> 2clk
			SDRAM_Ras_ActTime	=	4'h3,	// Tras time	default is 7 clks
			SDRAM_Ras_RefCyc	=	4'h5,	// Trfc time    default is 10 clks
			SDRAM_CtoC_dly		=	1'b0,	// Tc2c time    default is 0 0-> 1Tclk, 1-> 2Tclk
			SDRAM_RtoR_dly		=	1'b0,	// Trrd	delay	0-> 2cycles, 1->3cycles
			SDRAM_RtoC_dly		=	2'h1,	// Trcd	delay	0 -> 2clk 3 -> 5 clks
			SDRAM_Ras_PreTime	=	2'h1;	// Trp	delay	0 -> 2clk 3 -> 5 clks


parameter	SDRAM_TurnAround_Mode	=	1'h0;	//SDRAM Controller Turn around 0 -> 2 cycle 1 -> 1 cycle
parameter	SDRAM_OE_Post_En		=	1'h0;	//SDRAM OE post	1-> enable 0->disable(default)
parameter	SDRAM_OE_Pre_En			=	1'h0;	//SDRAM OE pre	1-> enable 0->disable(default)

parameter	SDRAM_Burst_Length	=	{2'h1,SDRAM_DQ_Mode}; 	//3'b010->4 bl, 3'b011->8bl


parameter	SDRAM_Dimm1_En			=	1'h0;
parameter	SDRAM_Dimm_Bank_Map		=	1'h1;


parameter	SDRAM_Dimm_Map			=	4'h4;    //for device set

parameter	SDRAM_Conti_Setting	=	6'h00;	//SDRAM Controller Main Arbt Conti Mode 1 cycle
											//SDRAM Controller SubB Arbt Conti Mode 1 cycle
											//SDRAM Controller SubA Arbt Conti Mode 1 cycle
parameter	SDRAM_CPU_Req_Prio		=	1'b1;	//CPU priority set 1 High priority
parameter	SDRAM_Main_Arbt_Mode	=	1'h0;	//SDRAM Controller Main Arbt Priority Mode
parameter	SDRAM_SubA_Arbt_Mode	=	1'h0;	//SDRAM Controller Sub Arbt Priority Mode
parameter	SDRAM_SubB_Arbt_Mode	=	1'h0;	//SDRAM Controller Sub-B Arbt Priority Mode

//--------FOR LOAD  PARAMTER
parameter	SDRAM_Row_Map			=	3'h4;	// 2 banks, 8 bit column addr  	3'h0 for load
                                            	// 4 banks, 8 bit column addr   3'h4
                                            	// 4 banks, 9 bit column addr 	3'h5
                                            	// 4 banks, 10 bit column addr	3'h6
parameter	SDRAM_Row1_Base		=	32'h0100_0000;	//d1_2m_2b  		32'h0040_0000;
                                                    //d1_4m_4b	12/8    32'h0100_0000;
                                                    //d1_8m_4b	12/9    32'h0200_0000;
                                                    //d1_16m_4b 12/10	32'h0400_0000;
`else	// use old sdram parameter


`endif
	//
parameter	SDRAM_WRDLock_Timer_En	=	1'b0;	//Write Dead Lock Timer: 1-> Enable, 0-> Disable
parameter	SDRAM_WRDLock_Exit_En	=	1'b0;	//Write Dead Auto-Exit: 1-> Enable, 0-> Disable

parameter	SDRAM_Refresh_Time = 8'h0d;
parameter	SDRAM_Clk_Tree_Dly = 3'h0;
parameter	SDRAM_Interleave = 1'b0;	// SDRAM Linear Mode
/*
parameter	SDRAM_Main_Arbt_Mode	=	2'h0;	//SDRAM Controller Main Arbt Priority Mode
parameter	SDRAM_Sub_Arbt_Mode		=	2'h0;	//SDRAM Controller Sub Arbt Priority Mode
parameter	SDRAM_SubB_Arbt_Mode	=	2'h0;	//SDRAM Controller Sub-B Arbt Priority Mode
parameter	SDRAM_Dimm_Map			=	5'h0;	//{bank_w,row_w[1:0],col_w[1:0]}
parameter	SDRAM_WRDLock_Timer_En	=	1'b0;	//Write Dead Lock Timer: 1-> Enable, 0-> Disable
parameter	SDRAM_WRDLock_Exit_En	=	1'b0;	//Write Dead Auto-Exit: 1-> Enable, 0-> Disable
*/

//==================================================================
//***************************************************************
//for IO operation, system and local device base,yuky,2002-06-19
//***************************************************************
parameter   CPU_IOBase 		= 32'h1800_0000;
parameter   SB_IOBase 		= 32'h1800_1000;
parameter	GPU_IOBase 		= 32'h1800_2000;
parameter	Display_IOBase 	= 32'h1800_3000;
// parameter	Video_IOBase	= 32'h1800_4000;	// Snow Yi, 2003.09.02
parameter	Video_IOBase0	= 32'h1800_4000;
parameter	Video_IOBase1	= 32'h1800_4100;
parameter	Video_IOBase2	= 32'h1800_4200;
parameter	Video_IOBase3	= 32'h1800_4300;
parameter	DSP_IOBase		= 32'h1800_5000;		// Snow Yi, 2003.09.02
parameter	VSB_IOBase		= 32'h1800_6000;
parameter	TvEnc_IOBase	= 32'h1800_7000;		// Snow Yi, 2003.09.02
parameter	IDE_IOBase		= 32'h1800_8000;
parameter	PCI_IOBase		= 32'h1000_0000;


`ifdef	NEW_MEM


//***************************************************************
//for SDRAM space distribution.yuky,2002-06-19
//***************************************************************
parameter	SDRAM_Base			= 32'h0000_0000;
parameter	Sys_SDRAM_Base		= 32'h0000_0000; //	(0.5MB space)
parameter	CPPM_SDRAM_Base		= 32'h0008_0000; // (0.5MB space)
parameter	VSB_SDRAM_Base		= 32'h0010_0000; // (384KB space)
parameter	IDE_SDRAM_Base		= 32'h0018_0000; // (0.5MB space)
parameter	DSP_SDRAM_Base		= 32'h0020_0000; // (0.5MB space)
parameter	Servo_SDRAM_Base	= 32'h0028_0000; //	(0.5MB space)
parameter	PCI_SDRAM_Base		= 32'h0030_0000; // (0.5MB space)
parameter	USB_SDRAM_Base		= 32'h0038_0000; // (0.5MB space)
parameter	Video_SDRAM_Base 	= 32'h0040_0000; // (4MB space)
parameter	UP_SDRAM_Base		= 32'h00f8_0000; // (0.5MB space)
parameter	Display_SDRAM_Base 	= 32'h0100_0000; // (4MB space)
parameter	Sys_Ctrl_Load_Base	= 32'h0004_0000; // (For MIPS System CTRL_Initialize)
//***************************************************************
//for Other address space Mapping.yuky,2002-06-19
//***************************************************************
parameter	Flash_Mem_Base	= 32'h1fc0_0000; // (16M)
//parameter	PCI_Mem_Base	= 32'h3000_0000; // (256M)
parameter	PCI_Mem_Base	= 32'h0800_0000; // (128M)


//***************************************************************
//define CD buffer_a and buffer_b lurb,2002-07-19
//***************************************************************
parameter	BUFFER_A = 32'h0;
parameter	BUFFER_B = 32'h8000;
//***************************************************************
//for program loading to sdram.yuky,2002-06-19
//***************************************************************
parameter	SDRAM_Load_Base = 32'h00000000;
//espical for wafer test
parameter	CTRL_Load_Base			= 32'h001f_0000;	//CTRL_Initialize
parameter	NB_GPU_IDCT_DISP_Load_Base 	= 32'h0000_0000;	//NB_GPU_IDCT_DISP_Initialize
parameter	CPU_Load_Base			= 32'h0000_0000;	//CPU_Initialize
parameter	AUDIO_Load_Base			= 32'h0020_0000;	//AUDIO_Initialize
//SB part
parameter	USB_Load_Base			= 32'h0000_0000;	//USB_Initialize
parameter	I2C_Load_Base			= 32'h0000_0000;	//I2C_Initialize
parameter	SPI_Load_Base			= 32'h0000_0000;	//SPI_Initialize
parameter	IRC_Load_Base			= 32'h0000_0000;	//IRC_Initialize
parameter	SUBQ_Load_Base			= 32'h0000_0000;	//SUBQ_Initialize
parameter	CD_DSP_Load_Base		= 32'h0000_0000;	//CD_DSP_Initialize
parameter	MICOM_Load_Base			= 32'h0000_0000;	//MICOM_Initialize

`else
//***************************************************************
//for SDRAM space distribution.yuky,2002-06-19
//***************************************************************
parameter	SDRAM_Base			= 32'h0000_0000;
parameter	Sys_SDRAM_Base		= 32'h0000_0000; //(2MB space)
parameter	GPU_SDRAM_Base		= 32'h0020_0000; //(6MB space)
parameter	GPU_FrameBufferBase = 32'h0020_0000;	// start at 2MB
parameter	GPU_TextureMemoryBase = 32'h0020_0000;	// start at 2MB
parameter	Video_SDRAM_Base 	= 32'h0080_0000; // (3MB space)
//parameter	Display_SDRAM_Base 	= 32'h00b0_0000; // (1MB space)
parameter	DSP_SDRAM_Base		= 32'h00c0_0000; // (1MB space)
parameter	CD_SDRAM_Base		= 32'h00d0_0000; // (0.5MB space)
parameter	VSB_SDRAM_Base		= 32'h00d8_0000; // (384KB space)
parameter	SUBPICT_SDRAM_Base	= 32'h00de_0000; // (128KB space)
parameter	PCI_SDRAM_Base		= 32'h00e0_0000; // (0.5MB space)
parameter	USB_SDRAM_Base		= 32'h00e8_0000; // (0.5MB space)
parameter	IDE_SDRAM_Base		= 32'h00f0_0000; // (0.5MB space)
parameter	UP_SDRAM_Base		= 32'h00f8_0000; // (0.5MB space)
parameter	Display_SDRAM_Base 	= 32'h0100_0000; // (4MB space)
parameter	Sys_Ctrl_Load_Base	= 32'h0180_0000; // (For MIPS System CTRL_Initialize)
//***************************************************************
//for Other address space Mapping.yuky,2002-06-19
//***************************************************************
parameter	Flash_Mem_Base	= 32'h1fc0_0000; // (16M)
//parameter	PCI_Mem_Base	= 32'h3000_0000; // (256M)
parameter	PCI_Mem_Base	= 32'h0800_0000; // (128M)


//***************************************************************
//define CD buffer_a and buffer_b lurb,2002-07-19
//***************************************************************
parameter	BUFFER_A = 32'h0;
parameter	BUFFER_B = 32'h8000;
//***************************************************************
//for program loading to sdram.yuky,2002-06-19
//***************************************************************
parameter	SDRAM_Load_Base = 32'h00000000;
//espical for wafer test
parameter	CTRL_Load_Base			= 32'h001f_0000;	//CTRL_Initialize
parameter	NB_GPU_IDCT_DISP_Load_Base 	= 32'h0000_0000;	//NB_GPU_IDCT_DISP_Initialize
parameter	CPU_Load_Base			= 32'h0000_0000;	//CPU_Initialize
parameter	AUDIO_Load_Base			= 32'h00c0_0000;	//AUDIO_Initialize
//SB part
parameter	USB_Load_Base			= 32'h0000_0000;	//USB_Initialize
parameter	I2C_Load_Base			= 32'h0000_0000;	//I2C_Initialize
parameter	SPI_Load_Base			= 32'h0000_0000;	//SPI_Initialize
parameter	IRC_Load_Base			= 32'h0000_0000;	//IRC_Initialize
parameter	SUBQ_Load_Base			= 32'h0000_0000;	//SUBQ_Initialize
parameter	CD_DSP_Load_Base		= 32'h0000_0000;	//CD_DSP_Initialize
parameter	MICOM_Load_Base			= 32'h0000_0000;	//MICOM_Initialize

`endif
//===============================================================
//use the PRO_ID to select the priority
parameter	PRO00_ID	=	5'h00;
parameter	PRO01_ID	=	5'h01;
parameter	PRO02_ID	=	5'h02;
parameter	PRO03_ID	=	5'h03;
parameter	PRO04_ID	=	5'h04;
parameter	PRO05_ID	=	5'h05;
parameter	PRO06_ID	=	5'h06;
parameter	PRO07_ID	=	5'h07;
parameter	PRO08_ID	=	5'h08;
parameter	PRO09_ID	=	5'h09;
parameter	PRO10_ID	=	5'h0a;
parameter	PRO11_ID	=	5'h0b;
parameter	PRO12_ID	=	5'h0c;
parameter	PRO13_ID	=	5'h0d;
parameter	PRO14_ID	=	5'h0e;
parameter	PRO15_ID	=	5'h0f;
parameter	PRO16_ID	=	5'h10;
parameter	PRO17_ID	=	5'h11;
parameter	PRO18_ID	=	5'h12;
parameter	PRO19_ID	=	5'h13;
parameter	PRO20_ID	=	5'h14;
parameter	PRO21_ID	=	5'h15;
parameter	PRO22_ID	=	5'h16;
parameter	PRO23_ID	=	5'h17;
parameter	PRO24_ID	=	5'h18;
parameter	PRO25_ID	=	5'h19;
parameter	PRO26_ID	=	5'h1a;
parameter	PRO27_ID	=	5'h1b;
parameter	PRO28_ID	=	5'h1c;
parameter	PRO29_ID	=	5'h1d;
parameter	PRO30_ID	=	5'h1e;
parameter	PRO31_ID	=	5'h1f;
//========== define the strap pins status=============
`ifdef	POST_GSIM
// PLL_M = 8'h16 (22*4.5=99MHz)
defparam	cpu_clk_pll_m0.set_one = 0;
defparam	cpu_clk_pll_m1.set_one = 1;
defparam	cpu_clk_pll_m2.set_one = 1;
defparam	cpu_clk_pll_m3.set_one = 0;
defparam	cpu_clk_pll_m4.set_one = 1;
defparam	cpu_clk_pll_m5.set_one = 0;
//defparam	pll_m6.set_one = 1;
//defparam	pll_m7.set_one = 0;
`else
`ifdef	CPU_NET	//use cpu netlist to do simulation
// PLL_M = 8'h2C (44*4.5=198.5MHz)
defparam	cpu_clk_pll_m0.set_one = 0;
defparam	cpu_clk_pll_m1.set_one = 0;
defparam	cpu_clk_pll_m2.set_one = 1;
defparam	cpu_clk_pll_m3.set_one = 1;
defparam	cpu_clk_pll_m4.set_one = 0;
defparam	cpu_clk_pll_m5.set_one = 1;
//defparam	pll_m6.set_one = 1;
//defparam	pll_m7.set_one = 0;
`else	//cpu behavior model and cpu source code
// PLL_M = 8'h2c (44*4.5=198.5MHz)
defparam	cpu_clk_pll_m0.set_one = 0;
defparam	cpu_clk_pll_m1.set_one = 0;
defparam	cpu_clk_pll_m2.set_one = 1;
defparam	cpu_clk_pll_m3.set_one = 1;
defparam	cpu_clk_pll_m4.set_one = 0;
defparam	cpu_clk_pll_m5.set_one = 1;
//defparam	pll_m6.set_one = 0;
//defparam	pll_m7.set_one = 1;
`endif
`endif
`ifdef	POST_GSIM
// MEM_FS = 8'h16 (32*4.5=144MHz)
defparam	mem_clk_pll_m0.set_one = 0;
defparam	mem_clk_pll_m1.set_one = 0;
defparam	mem_clk_pll_m2.set_one = 0;
defparam	mem_clk_pll_m3.set_one = 0;
defparam	mem_clk_pll_m4.set_one = 0;
defparam	mem_clk_pll_m5.set_one = 1;
`else
// MEM_FS = 8'h16 (32*4.5=144MHz)
defparam	mem_clk_pll_m0.set_one = 0;
defparam	mem_clk_pll_m1.set_one = 0;
defparam	mem_clk_pll_m2.set_one = 0;
defparam	mem_clk_pll_m3.set_one = 0;
defparam	mem_clk_pll_m4.set_one = 0;
defparam	mem_clk_pll_m5.set_one = 1;
`endif
// PCI_T2RISC_FS = 11 (1:8)
defparam	pci_t2risc_fs0.set_one = 0;
defparam	pci_t2risc_fs1.set_one = 0;
// WORK_MODE = 001 (COMBO mode)
defparam	work_mode0.set_one = 0;
defparam	work_mode1.set_one = 0;
defparam	work_mode2.set_one = 0;
`ifdef	WAFER_TEST
// PLL_BYPASS = 0 (bypass mode)
defparam	pll_bypass.set_one = 1; //modified for wafer test,tod,2002-03-19
`else
// PLL_BYPASS = 0 (normal mode)
defparam	pll_bypass.set_one = 0;
`endif
// CPU_PROBE_EN = 0 (disable)
defparam    cpu_probe_en.set_one = 0;
// REFERENCE_INFO = 1
defparam	reference_info0.set_one = 0;
defparam	reference_info1.set_one = 0;
defparam	reference_info2.set_one = 0;
defparam	reference_info3.set_one = 0;
defparam	reference_info4.set_one = 0;
defparam	reference_info5.set_one = 0;
defparam	reference_info6.set_one = 0;
defparam	reference_info7.set_one = 0;
defparam	reference_info8.set_one = 0;
defparam	reference_info9.set_one = 0;
defparam	reference_info10.set_one = 0;
defparam	reference_info11.set_one = 0;
defparam	reference_info12.set_one = 0;

//==================================================
//for performance

parameter	CPU_START		=	32'h2	;
parameter    CPU_FINISH		=	32'h100	;

parameter	DISP_START	    =	32'h0	;
parameter    DISP_FINISH	=	32'h0	;

parameter	DSP_START	    =	32'h0	;
parameter    DSP_FINISH	    =	32'h0	;

parameter	SB_START	    =	32'h0	;
parameter    SB_FINISH	    =	32'h0	;

parameter	VSB_START	    =	32'h0	;
parameter    VSB_FINISH	    =	32'h0	;

parameter	IDE_START	    =	32'h0	;
parameter    IDE_FINISH	    =	32'h0	;

parameter	MEM_START	    =	32'h0	;
parameter    MEM_FINISH	    =	32'h0	;



