// Snow Yi, 2003-06-19	Add tasks for VIDEO_CORE system IO operation
// (PCI_IO_Write, PCI_IO_Read, PCI_IO_Readbk)	 -- for I2C
// (PCI_IO1_Write, PCI_IO1_Read, PCI_IO1_Readbk) -- for VPOST
// (PCI_IO2_Write, PCI_IO2_Read, PCI_IO2_Readbk) -- for VDEC

task Video_IO_Write;
input [9:0] index;
input [3:0] be;
input [31:0] data;
begin
    if (index[9:8] == 2'b00) begin
        $display("Video IO0 Write: IO Address=%h\tBE#=%b\tData=%h",
        	PCI_IOBase+index[7:0], ~be[3:0], data);
    	io_write (PCI_IOBase+index[7:0], 1, ~be[3:0], 0, data);
    end
    else if (index[9:8] == 2'b01) begin
        $display("Video IO1 Write: IO Address=%h\tBE#=%b\tData=%h",
        	PCI_IOBase1+index[7:0], ~be[3:0], data);
    	io_write (PCI_IOBase1+index[7:0], 1, ~be[3:0], 0, data);
    end
    else if (index[9:8] == 2'b10) begin
        $display("Video IO2 Write: IO Address=%h\tBE#=%b\tData=%h",
        	PCI_IOBase2+index[7:0], ~be[3:0], data);
    	io_write (PCI_IOBase2+index[7:0], 1, ~be[3:0], 0, data);
    end
    else begin
        $display("Video IO3 Write: IO Address=%h\tBE#=%b\tData=%h",
        	PCI_IOBase3+index[7:0], ~be[3:0], data);
    	io_write (PCI_IOBase3+index[7:0], 1, ~be[3:0], 0, data);
    end    
end
endtask	// end of task "Video_IO_Write"

task Video_IO_Readbk;  // Video_IO_Readbk
input [9:0] index;
input [3:0] be;
inout [31:0] data;
reg   [31:0] data;
begin
    data = 32'hxxxx_xxxx;
    if (index[9:8] == 2'b00)
    	io_read (PCI_IOBase+index[7:0], 1, ~be[3:0], 0, data[31:0]);
    else if (index[9:8] == 2'b01)
    	io_read (PCI_IOBase1+index[7:0], 1, ~be[3:0], 0, data[31:0]);
    else if (index[9:8] == 2'b10)
    	io_read (PCI_IOBase2+index[7:0], 1, ~be[3:0], 0, data[31:0]);
    else
    	io_read (PCI_IOBase3+index[7:0], 1, ~be[3:0], 0, data[31:0]);
end
endtask // end of "Video_IO_Readbk"

task Video_IO_Read;  // No Readback
input [9:0] index;
input [3:0] be;
input [31:0] data;
begin
    if (index[9:8] == 2'b00)
    	io_read (PCI_IOBase+index[7:0], 1, ~be[3:0], 0, data[31:0]);
    else if (index[9:8] == 2'b01)
    	io_read (PCI_IOBase1+index[7:0], 1, ~be[3:0], 0, data[31:0]);
    else if (index[9:8] == 2'b10)
    	io_read (PCI_IOBase2+index[7:0], 1, ~be[3:0], 0, data[31:0]);
    else
    	io_read (PCI_IOBase3+index[7:0], 1, ~be[3:0], 0, data[31:0]);
end
endtask // end of "Video_IO_Read"

task PCI_Load_Data_To_Memory;
input	ram_base;// ram_base is based for byte,so if you use DW,
				 // low two bits should be '0'
input	ram_size;//DW number
input	ram_index;

integer	ram_base,
		ram_size,
		ram_index;
integer	i;
begin
	
	if (ram_index == 0)
		begin
		$display ("@ 32`h%h ... RB 32'd%d ...",ram_base,ram_index);
		$readmemh({`home_dir, "/rsim/stimu.t/","vcd_y.txt"}, 
					mem_array, 0, ram_size-1);
		end
	else if (ram_index == 1)
		begin
		$display ("@ 32`h%h ... RB 32'd%d ...",ram_base,ram_index);
		$readmemh({`home_dir, "/rsim/stimu.t/","vcd_cb.txt"}, 
					mem_array, 0, ram_size-1);
		end
	else if (ram_index == 2)
		begin
		$display ("@ 32`h%h ... RB 32'd%d ...",ram_base,ram_index);
		$readmemh({`home_dir, "/rsim/stimu.t/","vcd_cr.txt"}, 
					mem_array, 0, ram_size-1);
		end
	else if (ram_index == 3)
		begin
		$display ("@ 32`h%h ... RB 32'd%d ...",ram_base,ram_index);
		$readmemh({`home_dir, "/rsim/stimu.t/","spdif_out.txt"}, 
					mem_array, 0, ram_size-1);
		end
	
	//--tranfer data to pci memory
	
	for (i=0;i<ram_size;i=i+1)
	begin
		p_master.writeBuffer[i] = {12'h000,mem_array[i]};
	end
	
	i=ram_size;
	while(i>64)
	begin
		p_master.PCI_memwr_burst(ram_base+(ram_size-i)*4,64);
		i=i-64;
	end
	
	while(i>16)
	begin
		p_master.PCI_memwr_burst(ram_base+(ram_size-i)*4,16);
		i=i-16;
	end
	
	while(i>4)
	begin
		p_master.PCI_memwr_burst(ram_base+(ram_size-i)*4,4);
		i=i-4;
	end
	
	while(i>0)
	begin
		p_master.PCI_memwr_burst(ram_base+(ram_size-i)*4,i);
		i=0;
	end
	
	$display("Load Data From System To Memory OK!\n");
	
end
endtask


task PCI_Dump_Data_From_Memory;
input	ram_base;//ram_base is based for byte,
				 //so if you use DW,low two bits should be '0'
input	ram_size;//DW number

integer	ram_base,
		ram_size;
integer	i,file_handle,data_read;
begin
	file_handle = $fopen("mem_data.dat");
	for(i=0;i<ram_size;i=i+1)
	begin
		p_master.PCI_memrd_dw(ram_base+i*4,data_read);
		$fdisplay(file_handle,"%h",data_read);
	end
	
end
endtask

// Snow Yi, 2003.07.02, refresh by local.p
// reg	[43:0]	input_mem_dvd	[INPUT_MEM_SIZE -1 : 0]; 
reg [51:0]	pattern_array	[0 : INPUT_CMD_SIZE - 1];

task VIDEO_CMD_LIST;		// Load all commands in the list

reg	[51:0]	vcommand;		// VIDEO command
reg	[3:0]	iosegement;		// bit51 ~ 48
reg	[3:0]	iocmd;			// bit47 ~ 43
reg	[7:0]	ioindex;		// bit43 ~ 36
reg	[3:0]	iobe;			// bit35 ~ 32
reg	[31:0]	iodata;			// bit31 ~ 0

reg	[19:0] 	mem_size;		// Data size to load into MEM
reg [25:0] 	mem_base;		// MEM Base to load	// max 64MB space
reg	[3:0]	mem_index;		// 

integer    	ci;				// command lines

begin 

$display("\n============Load Command List Begins Here");
$readmemh({`Pattern_Path,"/vdec_pattern.dat"}, 
			pattern_array, 0, INPUT_CMD_SIZE - 1);
ci=0;

vcommand	= pattern_array[ci];
iosegement	= vcommand[51:48];	// 1
iocmd		= vcommand[47:44];	// 1
ioindex		= vcommand[43:36];	// 2
iobe		= vcommand[35:32];	// 1
iodata		= vcommand[31:0];	// 8

while (vcommand[51:0] != 52'h3_1_ff_f_ffffffff)	// not end of pattern
begin
  if(iosegement == 4'h3 & iocmd == 4'h0 & ioindex[7:4] == 4'h0) begin
  		// load data into memory
  		mem_base	= {iobe, iodata[31:20], 10'h0};
  		mem_size	= iodata[19:0];
  		mem_index	= ioindex[3:0];
  		$display ("***** Parameters Loading *****");
 		$display ("Base = %h Size = %h Index = %h", 
 				   mem_base, mem_size, mem_index);
 		VIDEO_DATA_LOAD (mem_base, mem_size, mem_index);
  	end
  else if (iosegement == 4'h3 & iocmd == 4'h0 & ioindex[7:4] == 4'h1) begin
  		// check status of register
		// check_reg = ioindex[3:0]
		// check_bit = iodata[31:0]
  		VIDEO_CHECK_STATUS (ioindex[3:0], iobe, iodata[31:0]);
  	end
  else if (iosegement != 4'h3 & iocmd == 4'h1) begin
  		// write data into IO register	// pay attention to PCI IOBASE setting
  										// IOBASE 0 is used for I2C in platfrom
  		Video_IO_Write	({iosegement[1:0]+1,ioindex}, iobe, iodata);
  	end
  else if (iosegement != 4'h3 & iocmd == 4'h0) begin
  		Video_IO_Read	({iosegement[1:0]+1,ioindex}, iobe, iodata);
  		// read data from IO register	// pay attention to PCI IOBASE setting
  	end

ci= ci + 1;

vcommand	= pattern_array[ci];
iosegement	= vcommand[51:48];	// 1
iocmd		= vcommand[47:44];	// 1
ioindex		= vcommand[43:36];	// 2
iobe		= vcommand[35:32];	// 1
iodata		= vcommand[31:0];	// 8
end
$display("\n============Load Command List Ended Here");
end
endtask // end of task VIDEO_CMD_LIST

task VIDEO_DATA_LOAD;

input [25:0]	mem_base;
input [19:0]	mem_size;
input [3:0]		mem_index;

integer			sdram_base;
integer			sdram_size;

begin
	sdram_base	= mem_base;
	sdram_size	= mem_size >> 2;
	
	if (mem_index == 4'h0) begin
		$display ("***** Loading VIDEO data FRAME0 ***** %h",mem_size);
		$readmemh({`Pattern_Path,"/vdec_frame0.dat"}, 
					mem_array, 0, mem_size-1);
	end
	else if (mem_index == 4'h1) begin
		$display ("***** Loading VIDEO data FRAME1 ***** %h",mem_size);
		$readmemh({`Pattern_Path,"/vdec_frame1.dat"}, 
					mem_array, 0, mem_size-1);
	end
	else if (mem_index == 4'h2) begin
		$display ("***** Loading VIDEO data FRAME2 ***** %h",mem_size);
		$readmemh({`Pattern_Path,"/vdec_frame2.dat"}, 
					mem_array, 0, mem_size-1);
	end
	else if (mem_index == 4'h3) begin
		$display ("***** Loading VIDEO data BITSTEAM ***** %h",mem_size);
		$readmemh({`Pattern_Path,"/vdec_bitstream.dat"}, 
					mem_array, 0, mem_size-1);
	end
	else if (mem_index == 4'h4) begin
		$display ("***** Loading VIDEO data BWD_MBH ***** %h",mem_size);
		$readmemh({`Pattern_Path,"/vdec_bwd_mbh.dat"}, 
					mem_array, 0, mem_size-1);
	end
	else if (mem_index == 4'h5) begin
		$display ("***** Loading VIDEO data BWD_NOT_CODE ***** %h",mem_size);
		$readmemh({`Pattern_Path,"/vdec_bwd_not_code.dat"}, 
					mem_array, 0, mem_size-1);
	end
	else if (mem_index == 4'h6) begin
		$display ("***** Loading VIDEO data SP_MEM ***** %h",mem_size);
		$readmemh({`Pattern_Path,"/sp_mem.dat"}, 
					mem_array, 0, mem_size-1);		// Snow Yi, 2003.07.09
	end
	else if (mem_index == 4'h7) begin
		$display ("***** Loading VIDEO data OSD_MEM ***** %h",mem_size);
		$readmemh({`Pattern_Path,"/osd_mem.dat"}, 
					mem_array, 0, mem_size-1);		// Snow Yi, 2003.07.09
	end
						
	SDRAM_load_memory (sdram_base, sdram_size);

end

endtask	// end of task VIDEO_DATA_LOAD;

task VIDEO_CHECK_STATUS;
input	[3:0]	check_reg;
input	[3:0]	check_active;	//	"0" or "1"
input	[31:0]	check_bit;
reg		[31:0]	check_data;
reg		[31:0]	readbk_data;
begin
	
	if (check_reg == 4'h0) begin
		check_data	= check_bit;
		readbk_data	= 32'hxxxxxxxx;
		$display ("***** Waiting for Status %h *****", check_bit);
		while (check_data != 32'h0) begin
			Video_IO_Readbk (10'h318, 4'hf, readbk_data);
			check_data	= readbk_data & check_data;
		end
	end
	else if (check_reg == 4'h1) begin
		check_data	= ~check_bit;
		readbk_data	= 32'hxxxxxxxx;
		$display ("***** Waiting for Status %h *****", check_bit);
		while (check_data != 32'hffff_ffff) begin
			Video_IO_Readbk (10'h318, 4'hf, readbk_data);
			check_data	= readbk_data | check_data;
		end
	end
//	else if () begin
//		// VPOST or VDVI (TBD)
//	end
	
end
endtask	// end of task VIDEO_CHECK_STATUS

/*
task	DE_SDRAM_load_memory_New;
input	mem_base;
input	mem_size;
input	mem_index;
integer	mem_size, mem_base,mem_index;
begin
	if (mem_index == 0)
		begin
		$display ("@ 32`h%h ... RB 32'd%d ...",mem_base,mem_index);
		$readmemh({`Pattern_Path,"/InputY.txt"}, mem_array, 0, mem_size-1);
		end
	else if (mem_index == 1)
		begin
		$display ("@ 32`h%h ... RB 32'd%d ...",mem_base,mem_index);
		$readmemh({`Pattern_Path,"/InputCb.txt"}, mem_array, 0, mem_size-1);
		end
	else if (mem_index == 2)
		begin
		$display ("@ 32`h%h ... RB 32'd%d ...",mem_base,mem_index);
		$readmemh({`Pattern_Path,"/InputCr.txt"}, mem_array, 0, mem_size-1);
		end
		
	SDRAM_load_memory(mem_base,mem_size);
end	
endtask
*/

// `endif


//I2c
task SendDataToI2C;
input i2c_int_flag;
input [7:0] i2c_data;
begin
	$display ("\n---I2c Send one byte Start-----");
	if(i2c_int_flag==0)
	begin
		$display ("---I2c Initial Start-----");
    	$display ("---Interrupt Enable,Host Controller Enable---");
		PCI_IO2_Write  (8'h00,4'b1111,32'hff01_0080);
		$display ("---HPCC/LPCC for Speed:40KHz,ID Address=0x4a---");
		PCI_IO2_Write  (8'h04,4'b1111,32'h0904_ff94);
		$display ("---SHDR=200ns,RSUR=200ns,PHDR=7us,PSUR=200ns---");
		PCI_IO2_Write  (8'h08,4'b1111,32'h0303_2803);
		$display ("---I2c Initial End-----");
	end
	
    $display ("---FIFO Flush and 2 Byte need send,one for index,one for data");
	PCI_IO2_Write  (8'h0c,4'b1111,32'h0000_0082);
	$display ("---Send one Data ---");
	PCI_IO2_Write  (8'h10,4'b0001,0);
	PCI_IO2_Write  (8'h10,4'b0001,i2c_data);
    $display ("----Start Send---");
	PCI_IO2_Write  (8'h00,4'b1111,32'hff01_0081);
	$display ("---Wait I2C interrupt---");
	
	#10;
	temp = 32'hXXXX_XXXX;
	PCI_IO2_Readbk (8'h00,4'b1111,temp);
	while (!temp[24])
	begin
		#2000;
		temp = 32'hXXXX_XXXX;
		PCI_IO2_Readbk (8'h00,4'b1111,temp);
	end
	
	PCI_IO2_Readbk(8'h00,4'b1111,temp);
	$display("---The Status is %h---",temp);	
    $display ("---Clear I2C interrupt----");
	PCI_IO2_Write  (8'h00,4'b1111,{8'hff,temp[23:0]});
	$display ("---I2c Send one byte Finish-----\n");

end
endtask
	
//Spy Status
task Wait_Line_To_End;
input [12:0] line_num;
reg	[31:0] line_num_temp;
reg	[12:0] line_num_old;
begin
	$display("\n===== waiting for line %d finish =====\n",line_num);
	line_num_old = 0;
	Video_IO_Readbk(10'h108,'b1111,line_num_temp);
	while(line_num_temp[28:16] !=  line_num)	
	begin
		#200;
		Video_IO_Readbk   (10'h108,4'b1111,line_num_temp);
    	if(line_num_temp[28:16]!=line_num_old)
    	begin
    		line_num_old = line_num_temp[28:16];
    		$display("----- line %d begins -----",line_num_old);
    	end
    end
    
    while(line_num_temp[28:16] == line_num)	
	begin
		Video_IO_Readbk   (10'h108,4'b1111,line_num_temp);
		#200;
    end
    $display("\n===== line %d ended =====\n",line_num);
end
endtask


task MEM_BYTE_TEST_WRITE;				//Joyous 03_03_19

input	[31:0]	addr		;
input	[31:0]	data		;
input	[3:0]	byte_enable	;

reg		[31:2]	addr_hi		;
reg		[1:0]	addr_low	;
reg		[7:0]	test_data	;
reg		[15:0]	test_data1	;
reg		[31:0]	test_data2	;
reg		[2:0]	byte_num	;
begin
	addr_hi[31:2]	=	addr[31:2]	;
//if (byte_enable[3:0] == 4'h0) begin
//   $fdisplay (f_name,"Error_T2_biu: %m WARNING ByteEnable is full 0",addr, $realtime);
//   $display ("Error_T2_biu:  %m WARNING ByteEnable is full 0",addr, $realtime);
//  end

casex (byte_enable)
    4'b0001: begin	addr_low	= 	2'b00	;byte_num	=3'b1;	end
    4'b0010: begin	addr_low	= 	2'b01	;byte_num	=3'b1;	end
    4'b0100: begin	addr_low	= 	2'b10	;byte_num	=3'b1;	end
    4'b1000: begin	addr_low	= 	2'b11	;byte_num	=3'b1;	end
    4'b0011: begin	addr_low	=	2'b00	;byte_num	=3'h2;	end
    4'b0110: begin	addr_low	= 	2'b01	;byte_num	=3'h2;	end
    4'b1100: begin	addr_low	= 	2'b10	;byte_num	=3'h2;	end
    4'b1111: begin	addr_low	= 	2'b00	;byte_num	=3'h4;	end
    default: begin	addr_low	= 	2'b00	;byte_num	=3'h0;	end
   endcase

	if	(byte_num	==3'h1)
		begin
	    $display("PCI memory byte write begin...");

	      test_data={{8{byte_enable[3]}}&{data[31:24]}}|{{8{byte_enable[2]}}&{data[23:16]}}|
    				{{8{byte_enable[1]}}&{data[15:8]}}|{{8{byte_enable[0]}}&{data[7:0]}};
	    $display("PCI memory byte write:\tStart ADDR =%h,DATA =%h, Byte_en =%h",addr,data,byte_enable);
	    p_master.PCI_memwr_b({addr_hi,addr_low},test_data);
	    $display ("PCI Byte Write Addr[%h]: %h", {addr_hi,addr_low}, test_data);
	    $display("PCI memory burst write finish");
		end
	else if	(byte_num	==3'h2)

	begin
	    $display("PCI memory byte write begin...");
	    if 	(byte_enable==4'b1100)
    		test_data1=data[31:16];
    	else if (byte_enable==4'b0110)
    		test_data1=data[23:8];
    	else if 	(byte_enable==4'b0011)
    		test_data1=data[15:0];
	    $display("PCI memory byte write:\tStart ADDR =%h,DATA =%h, Byte_en =%h",addr,data,byte_enable);
	    	p_master.PCI_memwr_w({addr_hi,addr_low},test_data1);
	    $display ("PCI Byte Write Addr[%h]: %h", {addr_hi,addr_low}, test_data1);
	    $display("PCI memory burst write finish");
	end
	else if	(byte_num	==3'h4)
	begin
	    $display("PCI memory byte write begin...");
	    test_data2	=	data	;
	    $display("PCI memory byte write:\tStart ADDR =%h,DATA =%h, Byte_en =%h",addr,data,byte_enable);
	    p_master.PCI_memwr_dw({addr_hi,addr_low},data);
	    $display ("PCI Byte Write Addr[%h]: %h", {addr_hi,addr_low}, test_data2);
	    $display("PCI memory burst write finish");
	end
end

endtask

task MEM_BYTE_TEST_READ;
input  	[31:0] 	addr		;
input	[31:0]	data_exp	;
input   [3:0]   byte_enable	;

reg  	[7:0] 	data		;
reg		[15:0]	data1		;
reg		[31:0]	data2		;
reg		[31:2]	addr_hi		;
reg		[1:0]	addr_low	;
reg		[31:0]	data_actual	;
reg		[2:0]	byte_num	;
begin
	addr_hi[31:2]	=	addr[31:2]	;
//if (byte_enable[3:0] == 4'h0) begin
//   $fdisplay ( f_name,"Error_T2_biu:  %m WARNING ByteEnable is full 0",addr, $realtime);
//   $display ("Error_T2_biu:  %m WARNING ByteEnable is full 0",addr, $realtime);
//  end
casex (byte_enable)
    4'b0001: begin	addr_low	= 	2'b00	;byte_num	=3'b1;	end
    4'b0010: begin	addr_low	= 	2'b01	;byte_num	=3'b1;	end
    4'b0100: begin	addr_low	= 	2'b10	;byte_num	=3'b1;	end
    4'b1000: begin	addr_low	= 	2'b11	;byte_num	=3'b1;	end
    4'b0011: begin	addr_low	=	2'b00	;byte_num	=3'h2;	end
    4'b0110: begin	addr_low	= 	2'b01	;byte_num	=3'h2;	end
    4'b1100: begin	addr_low	= 	2'b10	;byte_num	=3'h2;	end
    4'b1111: begin	addr_low	= 	2'b00	;byte_num	=3'h4;	end
    default: begin	addr_low	= 	2'b00	;byte_num	=3'h0;	end
   endcase

if	(byte_num	==3'h1)
	begin
    $display("PCI memory byte read begin...");
    $display("PCI memory byte read:\tStart ADDR =%h, Byte_en =%h",addr,byte_enable);
    p_master.PCI_memrd_b({addr_hi,addr_low},data);
    $display ("PCI Byte Read:%h", {addr_hi,addr_low}, data);
    data_actual={{{8{byte_enable[3]}}&{data}},{{8{byte_enable[2]}}&{data}},
    			{{8{byte_enable[1]}}&{data}},{{8{byte_enable[0]}}&{data}}};

		 if(data_actual[31:0] !== data_exp[31:0]) begin
	    	$display("Error_T2_rsim:Memory Byte Read\t%0t ADDR =%h, Exp =%h, Actua =%h", $realtime,
			{addr_hi,addr_low},data_exp[31:0], data_actual[31:0]);
	    	$fdisplay(f_name,"Error_T2_rsim:Memory Burst Read\t%0t ADDR =%h, Exp =%h, Actua =%h", $realtime,
			{addr_hi,addr_low},data_exp[31:0], data_actual[31:0]);
				end
    	$display("PCI memory burst read finish,exp=actual=%h", data_actual);
		end
else if	(byte_num	==3'h2)
	begin
    $display("PCI memory byte read begin...");
    $display("PCI memory byte read:\tStart ADDR =%h, Byte_en =%h",addr,byte_enable);
    p_master.PCI_memrd_w({addr_hi,addr_low},data1);
    $display ("PCI Byte Read Addr[%h]: %h", {addr_hi,addr_low}, data1);
    if 	(byte_enable==4'b1100)
    data_actual={{data1},16'h0};
    else if (byte_enable==4'b0110)
    data_actual={8'h0,{data1},8'h0};
    else if 	(byte_enable==4'b0011)
    data_actual={16'h0,{data1}};

    if(data_actual[31:0] !== data_exp[31:0]) begin
	    	$display("Error_T2_rsim:Memory Byte Read\t%0t ADDR =%h, Exp =%h, Actua =%h", $realtime,
			{addr_hi,addr_low},data_exp[31:0], data_actual[31:0]);
	    	$fdisplay(f_name,"Error_T2_rsim:Memory Burst Read\t%0t ADDR =%h, Exp =%h, Actua =%h", $realtime,
			{addr_hi,addr_low},data_exp[31:0], data_actual[31:0]);
		end
    $display("PCI memory burst read finish,exp=actual=%h", data_actual);
 end
else if	(byte_num	==3'h4)
	begin
    $display("PCI memory byte read begin...");
    $display("PCI memory byte read:\tStart ADDR =%h, Byte_en =%h",addr,byte_enable);
    p_master.PCI_memrd_dw({addr_hi,addr_low},data2);
    $display ("PCI Byte Read Addr[%h]: %h", {addr_hi,addr_low}, data2);
    data_actual=data2;
    if(data_actual[31:0] !== data_exp[31:0]) begin
	    	$display("Error_T2_rsim:Memory Byte Read\t%0t ADDR =%h, Exp =%h, Actua =%h", $realtime,
			{addr_hi,addr_low},data_exp[31:0], data_actual[31:0]);
	    	$fdisplay(f_name,"Error_T2_rsim:Memory Burst Read\t%0t ADDR =%h, Exp =%h, Actua =%h", $realtime,
			{addr_hi,addr_low},data_exp[31:0], data_actual[31:0]);
		end
	else
    $display("PCI memory burst read finish,exp=actual=%h", data_actual);
   end
  end
endtask

