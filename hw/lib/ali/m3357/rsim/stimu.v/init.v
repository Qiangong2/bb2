

/*******************************************************************
          (c) copyrights 1997-1998. All rights reserved

           T-Square Design, Inc.

          Company confidential and Properietary information.
          This information may not be disclosed to unauthorized
          individual.
*******************************************************************/

//********************************************************************
//     File:    	init.v
//     Description: used to initialize the circuit for simulation
//					and control to finish the simulation
//	   History:		2002-05-16 first version
//********************************************************************


initial begin
	init_end = 0;
	pci_vec_end = 0;
	cpu_vec_end	= 0;
	cpu_vec_00_end = 0;
	cpu_vec_01_end = 0;
	cpu_vec_02_end = 0;
	cpu_vec_03_end = 0;
	cpu_vec_04_end = 0;
	cpu_vec_05_end = 0;
	cpu_vec_06_end = 0;
	cpu_vec_07_end = 0;
	cpu_vec_08_end = 0;
	cpu_vec_09_end = 0;
	cpu_vec_10_end = 0;
	cpu_vec_11_end = 0;
	cpu_vec_12_end = 0;
	cpu_vec_13_end = 0;
	cpu_vec_14_end = 0;
	cpu_vec_15_end = 0;
	cpu_vec_16_end = 0;
	cpu_vec_17_end = 0;
	cpu_vec_18_end = 0;
	cpu_vec_19_end = 0;
	cpu_vec_20_end = 0;
	int_end = 1;
	`ifdef  INC_AUDIO
	dsp_end = 0;
	`endif

	ResetChip;
`ifdef INC_CORE	//include core of m3357
	ROM_Initialize;
	load_sdram_param;	//load sdram parameters to flash for program use
	ice_ram_initial;
    flash_access_disable; //disable the rom access
	SDRAM_Set_ClkDly;
	FLASH_Speed_Select (8'h01);

	//SDRAM Initialization:
	SDRAM_Init;
	`ifdef NEW_MEM
	SDRAM_Set_Arbt(SDRAM_Conti_Setting,SDRAM_CPU_Req_Prio,SDRAM_SubA_Arbt_Mode,SDRAM_SubB_Arbt_Mode,SDRAM_Main_Arbt_Mode);
	`else
	SDRAM_Set_Arbt(SDRAM_Sub_Arbt_Mode,SDRAM_SubB_Arbt_Mode,SDRAM_Main_Arbt_Mode);
	`endif
	SDRAM_Set_Refresh_Timer(SDRAM_Refresh_Time);	//Auto-Ref Enable

//for debug temporary, should be open later JFR030618
    Initialize;	                                // Software Driver Initial
	CTRL_RAMInitialize;

//`ifdef WAFER_TEST
//	SDRAM_Initialize;
//`else

	STRAP_Pin_Info;

	/*
	//Load the MIPS program to the SDRAM
	`ifdef INC_CPU

		if (CPU_mode) begin
			if (Tri_CPU_Enable)
				//bypass the wait;
				#1;
			else begin
				wait (dump_num == 890);	//first stop condition
				#1;
				wait (dump_num == 891); //second stop condition
			end
		end
		else begin
			wait (dump_num == 1130);//first stop condition
			`ifdef STOP_FLAG
			$stop;	//enter the interactive intf (TCL)of ncsim
			`endif
			wait (dump_num == 1135);//second stop condition
		end

		SDRAM_Initialize;
	`endif
	*/
//`endif
	`ifdef INC_CPU
		 begin
			wait (dump_num == 2605);//first stop condition
			`ifdef STOP_FLAG
			$stop;	//enter the interactive intf (TCL)of ncsim
			`endif
			wait (dump_num == 2610);//second stop condition
		end

		SDRAM_Initialize;
	`endif


	`ifdef  INC_AUDIO
//			dsp_mem_inital;
//			initial_dsp;
	`endif

	`ifdef  INC_IDE
			ext_ide_device_enable;
	`endif

/*
	`ifdef INC_VIDEO //include video
		$readmemh("ve_pattern.dat",ve_input_mem);
		$display("\n==========================================================");
		$display("=======    Load Reference Frame to MPEG Buffers   ========");
		$display("==========================================================\n");
		VE_SDRAM_load_memory(Video_SDRAM_Base,'h86fff,1);	// Snow Yi, 2003.09.01
	`endif
*/

	`ifdef INC_DISPLAY	//include display
		//make the tv encoder reset unactive
		tv_encoder_rst_disable;
	`else			//include display device behaviour model
//		DISP_BH_SDRAM_load_memory(Display_SDRAM_Base,1024*256);
//Remove for debug,need modify, JFR 030708
//		DISP_BH_SDRAM_load_memory(Display_SDRAM_Base,1024*256);
//		DISP_BH_SDRAM_dump_memory(Display_SDRAM_Base,1024*256,disp_bh_file);
	`endif

`endif //include the core of m3357
	//*********************************************************
	init_end = 1;
	$display("==========================================================");
	$display("============<<< Test Pattern Start >>>====================");
	$display("==========================================================");
	wait(pci_vec_end);
	wait(cpu_vec_00_end &
		cpu_vec_01_end &
		cpu_vec_02_end &
		cpu_vec_03_end &
		cpu_vec_04_end &
		cpu_vec_05_end &
		cpu_vec_06_end &
		cpu_vec_07_end &
		cpu_vec_08_end &
		cpu_vec_09_end &
		cpu_vec_10_end &
		cpu_vec_11_end &
		cpu_vec_12_end &
		cpu_vec_13_end &
		cpu_vec_14_end &
		cpu_vec_15_end &
		cpu_vec_16_end &
		cpu_vec_17_end &
		cpu_vec_18_end &
		cpu_vec_19_end &
		cpu_vec_20_end);
	#100;

	wait(cpu_vec_00_end &//wait twice for video mips pattern JFR 031030
		cpu_vec_01_end &
		cpu_vec_02_end &
		cpu_vec_03_end &
		cpu_vec_04_end &
		cpu_vec_05_end &
		cpu_vec_06_end &
		cpu_vec_07_end &
		cpu_vec_08_end &
		cpu_vec_09_end &
		cpu_vec_10_end &
		cpu_vec_11_end &
		cpu_vec_12_end &
		cpu_vec_13_end &
		cpu_vec_14_end &
		cpu_vec_15_end &
		cpu_vec_16_end &
		cpu_vec_17_end &
		cpu_vec_18_end &
		cpu_vec_19_end &
		cpu_vec_20_end);
	wait(int_end);

//	`ifdef  INC_AUDIO
//		wait(dsp_end);	//DANIEL0713, some audio pattern is no dsp binary code
//	`endif

//		`ifdef INC_VIDEO
//			$display("\n==============================================================");
//			$display("=======    Dump Reconstruct Buffer to video_data.dat  ========");
//			$display("==============================================================\n");
//			SDRAM_dump_memory ((Video_SDRAM_Base+'h16_8000), ve_dump_length, ve_data_handle);	// 184320 DWs
//		`endif

	//dump data for IDE check
	/*	//Used in old IDE, new IDE don't these data, yuky, 2002-11-27
	`ifdef INC_CPU
		`ifdef INC_IDE
        	$display("\n==============================================================");
        	$display("================    Dump Data to ide_mem.dat  ================");
        	$display("==============================================================\n");
        	SDRAM_dump_memory(IDE_SDRAM_Base, 'hff, ide_mem);
    	`endif
	`endif
	*/
	#100;
/*
`ifdef WAFER_TEST
	$toggle_test_summary;
	$toggle_count_report;
`endif
*/
	$finish;

end

//`ifdef SERVO_DEBUG
`ifdef INC_SERVO
	initial begin
		SV_INITIAL	;
	end
`else
`endif