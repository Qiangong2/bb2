#!/usr/bin/perl -w

#==============================================================================
#	Function:	read in io register list file and generate
#			test pattern for io read/write
#			Add the 32'h55555555 and 32'haaaaaaaa read/write testing
#			Add fixed number testing
#==============================================================================

	if ($#ARGV < 1)
	{
		print	"Not enough input\n";
		die	"Usage auto_pattern.pl input_file output_file \n";
	}

	$infile	= $ARGV[0];
	$outfile = $ARGV[1];
	#$rw_str	= $ARGV[2];

	&open_input;
	&open_output;

	print	"Please wait \n";
	print	"Converting is begin ...\n";

	#======================================================================
	# read in register information and store in three hashes
	#======================================================================
	$i = 0;
	while(<INPUT>)
	{
		chop	$_;
		tr/A-Z#/a-z_/;
		($ide_mode_port,	$ide_mode_inout,
		 $cpu_mode_port,	$cpu_mode_inout,
		 $test_mode_port,	$test_mode_inout) = split;
		$ide_mode_port[$i]	= $ide_mode_port;
		$i++;
		$ide_mode_port{$ide_mode_port}	= $ide_mode_port;
		$ide_mode_inout{$ide_mode_port}	= $ide_mode_inout;
		$cpu_mode_port{$ide_mode_port}	= $cpu_mode_port;
		$cpu_mode_inout{$ide_mode_port}	= $cpu_mode_inout;
		$test_mode_port{$ide_mode_port}	= $test_mode_port;
		$test_mode_inout{$ide_mode_port} = $test_mode_inout;

		@tmp	= ($ide_mode_port, $ide_mode_inout);
		$ide_mode{$ide_mode_port}	= @tmp;
		@tmp	= ($cpu_mode_port, $cpu_mode_inout);
		$cpu_mode{$ide_mode_port}	= @tmp;
		@tmp	= ($test_mode_port, $test_mode_inout);
		$test_mode{$ide_mode_port}	= @tmp;

	}
	$modes{ide_mode}	= %ide_mode;
	$modes{cpu_mode}	= %cpu_mode;
	$modes{test_mode}	= %test_mode;

	#	generate the output file
	foreach	$ide_mode_port	(@ide_mode_port) {
		$bracket_num	= 0;
		&print_share_pin;
	}

	#======================================================================
	#	Print the input part
	#======================================================================
	foreach $ide_mode_port (@ide_mode_port)
	{
		$ide_mode_port	= $ide_mode_port{$ide_mode_port};
		$ide_mode_inout	= $ide_mode_inout{$ide_mode_port};
		&print_input_data($ide_mode_port, "ide_mode", $ide_mode_port, $ide_mode_inout);
	}

	foreach $ide_mode_port (@ide_mode_port)
	{
		$cpu_mode_port	= $cpu_mode_port{$ide_mode_port};
		$cpu_mode_inout	= $cpu_mode_inout{$ide_mode_port};
		&print_input_data($ide_mode_port, "cpu_mode", $cpu_mode_port, $cpu_mode_inout);
	}

	foreach $ide_mode_port (@ide_mode_port)
	{
		$analog_mode_port	= $analog_mode_port{$ide_mode_port};
		$analog_mode_inout	= $analog_mode_inout{$ide_mode_port};
		&print_input_data($ide_mode_port, "analog_mode", $analog_mode_port, $analog_mode_inout);
	}


	print	"Converting is finished, totally ", $#ide_mode_port + 1, " port read/write created!\n";

	close INPUT;
	close OUTPUT;

    exit;



#================================================
#	sub routine
#================================================
sub	open_input
{
	if (! -r $infile)
	{
    		die "Can't read input $infile\n";
	}
	if (! -f $infile)
	{
    		die "Input $infile is not a plain file\n";
	}
	if (! -s $infile)
	{
		die "Input $infile length is zero\n";
	}
	$infile_len = -s $infile;
	print $infile_len, "\n";


	open(INPUT,"<$infile") ||
		die "Can't input $infile $!";
}

sub	open_output
{
	# Validate files
	# Or statements "||" short-circuit, so that if an early part
	# evaluates as true, Perl doesn't bother to evaluate the rest.
	# Here, if the file opens successfully, we don't abort:
	if ( -e $outfile)
	{
	    print STDERR "Output file $outfile exists!\n";
	    $ans = '';
	    until ($ans eq 'r' || $ans eq 'a' || $ans eq 'e' )
	    {
	        print STDERR "replace, append, or exit? ";
	        $ans = getc(STDIN);
	    }
	    if ($ans eq 'e') {exit}
	}

	if ($ans eq 'a') {$mode='>>'}
	else {$mode='>'}
	open(OUTPUT,"$mode$outfile") ||
	    die "Can't output $outfile $!";
}

sub	print_share_pin
{
	$ide_mode_port	= $ide_mode_port{$ide_mode_port};
	$cpu_mode_port	= $cpu_mode_port{$ide_mode_port};
	$test_mode_port	= $test_mode_port{$ide_mode_port};

	$ide_mode_inout	= $ide_mode_inout{$ide_mode_port};
	$cpu_mode_inout	= $cpu_mode_inout{$ide_mode_port};
	$test_mode_inout	= $test_mode_inout{$ide_mode_port};

my	$ide_mode_port_tmp	= $ide_mode_port;
my	$ide_low			= "";
	if ($ide_mode_port_tmp =~ /_$/) {
		$ide_low = "_";
		$ide_mode_port_tmp	=~ s/_$//;
	}
	# if data is multiple bit one, check the left [ for multiple bit
	$ide_mode_port_tmp =~ s/\[/_out2pad[/;
	if ($ide_mode_port_tmp =~ /_out2pad/) {
	}
	else {
		# add _out2pad at the end of the data
		$ide_mode_port_tmp	= "${ide_mode_port_tmp}_out2pad";
	}

	#	set the bracket number to 0 before start
	$bracket_num	= 0;
	$head_line_end	= 0;

	print	OUTPUT	"assign\t${ide_mode_port_tmp}${ide_low}\t=";

	&print_first_output_data	("test_mode", $test_mode_port, $test_mode_inout);
	&print_first_output_data	("cpu_mode", $cpu_mode_port, $cpu_mode_inout);
	&print_last_output_data		("ide_mode", $ide_mode_port, $ide_mode_inout);


	$ide_mode_port_tmp =~ s/out2pad/oe_out2pad_/;
	#	set the bracket number to 0 before start
	$bracket_num	= 0;
	$head_line_end	= 0;

	print	OUTPUT	"assign\t", $ide_mode_port_tmp, "\t=";

	&print_first_output_enable	("test_mode", $test_mode_port, $test_mode_inout);
	&print_first_output_enable	("cpu_mode", $cpu_mode_port, $cpu_mode_inout);
	&print_last_output_enable	("ide_mode", $ide_mode_port, $ide_mode_inout);
}


sub	print_first_output_data
{
	local ($mode, $data, $inout) = @_;
	local	$active_low	= '';
	local	$tmp;
	if ($data ne 'unuse') {
		if ($data =~ /[_#]$/ )	{
			$active_low	= "_";
			$data =~ s/[_#]$//;	# remove last "_"
		}
		# if data has multiple bit
		$data =~ s/(\[[0-9]+\])//;
		$tmp	= "${active_low}$1";
		$active_low	= $tmp;
		if ($data =~ /_fromcore/) {
		}
		else {
			# add _fromcore at the end of the data
			$data	= "${data}_fromcore";
		}
		$tmp	= "${data}${active_low}";
		$data	= $tmp;

		if ($inout =~ /o/)	{
			printf	OUTPUT	("\n\t\t\t%11s ? %30s : (", ${mode}, ${data});
			$bracket_num++;
		}
	}
}


sub	print_first_output_enable
{
	local ($mode, $data, $inout) = @_;
	local	$active_low	= '';
	local	$tmp;
	if ($data ne 'unuse') {
		if ($data =~ /[_#]$/ )	{
			$active_low	= "_";
			$data =~ s/[_#]$//;	# remove last "_"
		}
		# if data has multiple bit
		$data =~ s/(\[[0-9]+\])//;
		$tmp	= "${active_low}$1";
		$active_low	= $tmp;
		if ($data =~ /_oe_fromcore_/) {
		}
		else {
			# add _fromcore at the end of the data
			$data	= "${data}_oe_fromcore_";
		}
		$tmp	= "${data}${active_low}";
		$data	= $tmp;

		if ($inout eq 'o')	{	# output only
			printf	OUTPUT	("\n\t\t\t%11s ? %30s :(", ${mode}, "SIGNAL_OUTPUT_ENABLE_");
			$bracket_num++;
		}
		elsif ($inout eq 'i') {	# input only
			printf	OUTPUT	("\n\t\t\t%11s ? %30s :(", ${mode}, "SIGNAL_INPUT_ENABLE_");
			$bracket_num++;
		}
		elsif ($inout =~ /o/) {
			printf	OUTPUT	("\n\t\t\t%11s ? %30s : (", ${mode}, ${data});
			$bracket_num++;
		}
	}
}


sub	print_mid_output_data
{
	local ($mode, $data, $inout) = @_;
	local	$active_low	= '';
	if ($data ne 'unuse') {
		if ($data =~ /[_#]$/ )	{
			$active_low	= "_";
			$data =~ s/[_#]$//;	# remove last "_"
		}
		# if data is multiple bit one, check the left [
		# if data is multiple bit one,
		$data =~ s/(\[[0-9]+\])//;
		$active_low	= "${active_low}${1}";
		if ($data =~ /_fromcore/) {
		}
		else {
			# add _fromcore at the end of the data
			$data	= "${data}_fromcore";
		}

		if ($inout =~ /o/)	{
			print	OUTPUT	"\n\t\t\t(${mode} & ${data}${active_low}) |";
		}
	}
}


sub	print_last_output_data
{
	my ($mode, $data, $inout) = @_;
	my	$active_low	= "";
	local	$tmp;

	if ($data ne 'unuse') {		# The last data in simple mode is the default mode,
								# so it will not be 'unuse'
		if ($data =~ /[_#]$/)	{
			$active_low	= "_";
			$data =~ s/[_#]$//;
		}
		# if data is multiple bit one, check the left [
		$data =~ s/(\[[0-9]+\])//;
		$tmp	= "${active_low}${1}";
		$active_low	= $tmp;
		if ($data =~ /_fromcore/) {
		}
		else {
			# add _fromcore at the end of the data
			$data	= "${data}_fromcore";
		}
		$tmp	= "$data$active_low";
		$data	= $tmp;
		if ($inout =~ /o/)	{
			printf	OUTPUT	("%26s", ${data});
			print	OUTPUT	"\t";
			for ($i = 0; $i <$bracket_num; $i++) {
				print	OUTPUT	')';
			}
			print	OUTPUT	";\n\n";
		}
		else {	# If the last port is an input port
			printf	OUTPUT	("%26s", "1\'b0");
			print	OUTPUT	"\t";
			for ($i = 0; $i <$bracket_num; $i++) {
				print	OUTPUT	')';
			}
			print	OUTPUT	";\n\n";
		}
	}
	else {
			printf	OUTPUT	("%26s", "1\'b0");
			print	OUTPUT	"\t";
			for ($i = 0; $i <$bracket_num; $i++) {
				print	OUTPUT	')';
			}
			print	OUTPUT	";\n\n";
	}
	$bracket_num = 0;
}


sub	print_mid_output_enable
{
	my ($mode, $data, $inout) = @_;
	my	$active_low	= "";
	if ($data ne 'unuse') {
		$data =~ s/[_#]$//;
		# if data is multiple bit one,
		$data =~ s/(\[[0-9]+\])//;
		$active_low	= "${active_low}${1}";
		if ($data =~ /_oe_fromcore_/) {
		}
		else {
			# add _oe_fromcore_ at the end of the data
			$data	= "${data}_oe_fromcore_";
		}

		if ($inout eq 'o')	{	# output only
			print	OUTPUT	"\n\t\t\t(${mode} & SIGNAL_OUTPUT_ENABLE_) |";
		}
		elsif ($inout eq 'i') {	# input only
			print	OUTPUT	"\n\t\t\t(${mode} & SIGNAL_INPUT_ENABLE) |";
		}
		elsif ($inout =~ /o/) {
			print	OUTPUT	"\n\t\t\t(${mode} & ${data}${active_low}) |";
		}
	}
}


sub	print_last_output_enable
{
	my ($mode, $data, $inout) = @_;
	my	$active_low	= "";
	my	$tmp	= "";
	if ($data ne 'unuse') {			# the last data in simple mode will be the default working mode
		if ($data =~ /[_#]$/)	{
			$active_low	= "_";
			$data =~ s/[_#]$//;
		}
		# if data is multiple bit one
		$data =~ s/(\[[0-9]+\])//;
		$tmp	= "${active_low}${1}";
		$active_low	= $tmp;
		if ($data =~ /_oe_fromcore_/) {
		}
		else {
			# add _oe_fromcore_ at the end of the data
			$tmp	= "${data}_oe_fromcore_";
			$data		= $tmp;
		}
		if ($inout eq 'o')	{	# output only
			printf	OUTPUT	(" %26s", "SIGNAL_OUTPUT_ENABLE_");
			print	OUTPUT	"\t";
			for ($i = 0; $i <$bracket_num; $i++) {
				print	OUTPUT	')';
			}
			print	OUTPUT	";\n\n";
		}
		elsif ($inout eq 'i') {	# input only
			printf	OUTPUT	("%26s", "SIGNAL_INPUT_ENABLE");
			print	OUTPUT	"\t";
			for ($i = 0; $i <$bracket_num; $i++) {
				print	OUTPUT	')';
			}
			print	OUTPUT	";\n\n";
		}
		elsif ($inout =~ /o/)	{
			printf	OUTPUT	("%26s", ${data});
			print	OUTPUT	"\t";
			for ($i = 0; $i <$bracket_num; $i++) {
				print	OUTPUT	')';
			}
			print	OUTPUT	";\n\n";
		}
	}
	else {
		printf	OUTPUT	("%26s", "1\'b0");
		print	OUTPUT	"\t";
		for ($i = 0; $i <$bracket_num; $i++) {
			print	OUTPUT	')';
		}
		print	OUTPUT	";\n\n";
	}
}


sub	print_input_data
{
	my ($ide_mode_port, $mode, $data, $inout) = @_;
	my	$active_low	= "";
	my	$ide_active_low = "";
	my	$tmp;

	if ($data =~ /[_#]$/)
	{
		$active_low	= "_";
		$data =~ s/[_#]$//;
	}
	# if data is multiple bit one,
	$data =~ s/(\[[0-9]+\])//;
	$tmp	= "${active_low}$1";
	$active_low	= $tmp;

	if ($data =~ /_out2core/) {
	}
	else {
		# add _out2core at the end
		$data = "${data}_out2core";
	}
	$tmp	= "${data}${active_low}";
	$data	= $tmp;

	if ($ide_mode_port =~ /[_#]$/) {
		$ide_active_low	= "_";
		$ide_mode_port =~ s/[_#]$//;
	}

	if ($ide_mode_port	=~ s/(\[[0-9]+\])//) {
		$tmp	= "$ide_active_low$1";
		$ide_active_low	= $tmp;
	}


	if ($ide_mode_port =~ /_frompad/) {
	}
	else {
		# add _frompad at the end of the data
		$ide_mode_port	= "${ide_mode_port}_frompad";
	}
	$tmp	= "${ide_mode_port}${ide_active_low}";
	$ide_mode_port	= $tmp;

	if (!($data =~ /unuse/)) {
		if ($active_low =~ /_/) {
			$disable	= "1\'b1";
		}
		else {
			$disable	= "1\'b0";
		}

		if ($inout =~ /i/)
		{
			print	OUTPUT	"assign	${data}\t= $mode ? ${ide_mode_port} : $disable;\n";
		}
	}
}
		