#!/usr/bin/perl -w

	#==========================================================================
	#	Read in the share pin list TXT file and generate verilog source code
	#	for it.
	#	Created for M3357 project
	#
	#	All the signal name will be convert to lower case
	#	Signal end with '#' or '_' will be treat as active low.
	#
	#	The pin that did not share any signal will be treat as output only at
	#	that mode with output value 1'b0
	#
	#	Inout column support 4 type: O (output only), I/O (inout), I (input only)
	#	and OE (output with enable control)
	#
	#	Usage:	auto_pattern.pl input_file output_file
	#==========================================================================
	if ($#ARGV < 1)
	{
		print	"Not enough input\n";
		die	"Usage share_pin_list.pl input_file output_file \n";
	}

	$infile	= $ARGV[0];
	$outfile = $ARGV[1];

	# the default mode is complex mode
	if ($#ARGV >= 2) {
		$complex_mode = $ARGV[2];
	} else {
		$complex_mode = 1;
	}

	if ($#ARGV >= 3) {
		$share_pin = $ARGV[3];
	} else {
		$share_pin = 1;
	}

	&open_input;
	&open_output;

	print	"Please wait \n";
	print	"Converting is begin ...\n";

	#======================================================================
	# read in register information and store in three hashes
	#======================================================================
#	print share pin
if ($share_pin == 1) {
	$i = 0;
	while(<INPUT>)
	{
		chop	$_;
		$_	=~ tr/A-Z#/a-z_/;		# transfer the upper case to lower case, # to _

		($mode_0_port, $mode_0_inout,
		 $mode_1_port, $mode_1_inout,
		 $mode_2_port, $mode_2_inout,
		 $mode_3_port, $mode_3_inout,
		 $mode_4_port, $mode_4_inout,
		 $mode_5_port, $mode_5_inout,
		 $mode_6_port, $mode_6_inout,
		 $mode_7_port, $mode_7_inout,
		 $mode_8_port, $mode_8_inout) = split;

		print	"$mode_0_port, $mode_0_inout,
		 		 $mode_1_port, $mode_1_inout,
		 		 $mode_2_port, $mode_2_inout,
		 		 $mode_3_port, $mode_3_inout,
		 		 $mode_4_port, $mode_4_inout,
		 		 $mode_5_port, $mode_5_inout,
		 		 $mode_6_port, $mode_6_inout,
		 		 $mode_7_port, $mode_7_inout,
		 		 $mode_8_port, $mode_8_inout\n";

		$mode_0_port[$i] = $mode_0_port;

		$mode_0_port	{$mode_0_port}		= $mode_0_port;
		$mode_0_inout	{$mode_0_port}		= $mode_0_inout;
		$i++;

		$mode_1_port	{$mode_0_port}		= $mode_1_port;
		$mode_1_inout	{$mode_0_port}		= $mode_1_inout;
		$mode_2_port	{$mode_0_port}		= $mode_2_port;
		$mode_2_inout	{$mode_0_port}		= $mode_2_inout;
		$mode_3_port	{$mode_0_port}		= $mode_3_port;
		$mode_3_inout	{$mode_0_port}		= $mode_3_inout;
		$mode_4_port	{$mode_0_port}		= $mode_4_port;
		$mode_4_inout	{$mode_0_port}		= $mode_4_inout;
		$mode_5_port	{$mode_0_port}		= $mode_5_port;
		$mode_5_inout	{$mode_0_port}		= $mode_5_inout;
		$mode_6_port	{$mode_0_port}		= $mode_6_port;
		$mode_6_inout	{$mode_0_port}		= $mode_6_inout;
		$mode_7_port	{$mode_0_port}		= $mode_7_port;
		$mode_7_inout	{$mode_0_port}		= $mode_7_inout;
		$mode_8_port	{$mode_0_port}		= $mode_8_port;
		$mode_8_inout	{$mode_0_port}		= $mode_8_inout;

		# The mode text array make the mode connect with name.
		$mode_name[0]	= "combo_mode";			# rom_mode or combo_mode
		$mode_name[1]	= "ide_mode";
		$mode_name[2]	= "pci_mode";
		$mode_name[3]	= "tvenc_test_mode";
		$mode_name[4]	= "servo_only_mode";
		$mode_name[5]	= "servo_sram_mode";
		$mode_name[6]	= "servo_test_mode";
		$mode_name[7]	= "bist_test_mode";
		$mode_name[8]	= "scan_test_mode";

	}


	foreach $mode_0_port (@mode_0_port)
	{
		$bracket_num	= 0;
		&print_share_pin;
	}

	# generate input logic
	foreach $mode_0_port (@mode_0_port)
	{
		$mode_0_port	= $mode_0_port{$mode_0_port};
		$mode_0_inout	= $mode_0_inout{$mode_0_port};
		&print_input_data($mode_0_port, $mode_name[0], $mode_0_port, $mode_0_inout);
	}

	print	OUTPUT	"\n\n\t//==========================================================\n\n";
	foreach $mode_0_port (@mode_0_port)
	{
		$mode_1_port	= $mode_1_port{$mode_0_port};
		$mode_1_inout	= $mode_1_inout{$mode_0_port};
		&print_input_data($mode_0_port, $mode_name[1], $mode_1_port, $mode_1_inout);
	}

	print	OUTPUT	"\n\n\t//==========================================================\n\n";
	foreach $mode_0_port (@mode_0_port)
	{
		$mode_2_port	= $mode_2_port{$mode_0_port};
		$mode_2_inout	= $mode_2_inout{$mode_0_port};
		&print_input_data($mode_0_port, $mode_name[2], $mode_2_port, $mode_2_inout);
	}

	print	OUTPUT	"\n\n\t//==========================================================\n\n";
	foreach $mode_0_port (@mode_0_port)
	{
		$mode_3_port	= $mode_3_port{$mode_0_port};
		$mode_3_inout	= $mode_3_inout{$mode_0_port};
		&print_input_data($mode_0_port, $mode_name[3], $mode_3_port, $mode_3_inout);
	}

	print	OUTPUT	"\n\n\t//==========================================================\n\n";
	foreach $mode_0_port (@mode_0_port)
	{
		$mode_4_port	= $mode_4_port{$mode_0_port};
		$mode_4_inout	= $mode_4_inout{$mode_0_port};
		&print_input_data($mode_0_port, $mode_name[4], $mode_4_port, $mode_4_inout);
	}

	print	OUTPUT	"\n\n\t//==========================================================\n\n";
	foreach $mode_0_port (@mode_0_port)
	{
		$mode_5_port	= $mode_5_port{$mode_0_port};
		$mode_5_inout	= $mode_5_inout{$mode_0_port};
		&print_input_data($mode_0_port, $mode_name[5], $mode_5_port, $mode_5_inout);
	}

	print	OUTPUT	"\n\n\t//==========================================================\n\n";
	foreach $mode_0_port (@mode_0_port)
	{
		$mode_6_port	= $mode_6_port{$mode_0_port};
		$mode_6_inout	= $mode_6_inout{$mode_0_port};
		&print_input_data($mode_0_port, $mode_name[6], $mode_6_port, $mode_6_inout);
	}

	print	OUTPUT	"\n\n\t//==========================================================\n\n";
	foreach $mode_0_port (@mode_0_port)
	{
		$mode_7_port	= $mode_7_port{$mode_0_port};
		$mode_7_inout	= $mode_7_inout{$mode_0_port};
		&print_input_data($mode_0_port, $mode_name[7], $mode_7_port, $mode_7_inout);
	}

	print	OUTPUT	"\n\n\t//==========================================================\n\n";
	foreach $mode_0_port (@mode_0_port)
	{
		$mode_8_port	= $mode_8_port{$mode_0_port};
		$mode_8_inout	= $mode_8_inout{$mode_0_port};
		&print_input_data($mode_0_port, $mode_name[8], $mode_8_port, $mode_8_inout);
	}

	print	"Converting is finished, totally ", $#mode_0_port + 1, " port read/write created!\n";
} else { # print share name list
	$i = 0;
	while(<INPUT>)	{
		chop	$_;
		$_	=~ tr/A-Z#/a-z_/;		# transfer the upper case to lower case, # to _

		($mode_0_port, $mode_0_inout,
		 $mode_1_port, $mode_1_inout,
		 $mode_2_port, $mode_2_inout,
		 $mode_3_port, $mode_3_inout) = split;

		print	"$mode_0_port, $mode_0_inout,
		 		 $mode_1_port, $mode_1_inout,
		 		 $mode_2_port, $mode_2_inout,
		 		 $mode_3_port, $mode_3_inout\n";

		$mode_0_port[$i] = $mode_0_port;

		$mode_0_port	{$mode_0_port}		= $mode_0_port;
		$mode_0_inout	{$mode_0_port}		= $mode_0_inout;
		$i++;

		$mode_1_port	{$mode_0_port}		= $mode_1_port;
		$mode_1_inout	{$mode_0_port}		= $mode_1_inout;
		$mode_2_port	{$mode_0_port}		= $mode_2_port;
		$mode_2_inout	{$mode_0_port}		= $mode_2_inout;
		$mode_3_port	{$mode_0_port}		= $mode_3_port;
		$mode_3_inout	{$mode_0_port}		= $mode_3_inout;
	}

	foreach $mode_0_port (@mode_0_port)
	{
		&print_share_name;
	}
	print	OUTPUT "\n\n";

	foreach $mode_0_port (@mode_0_port)
	{
		&print_share_name_oe;
	}
}




	close INPUT;
	close OUTPUT;

    exit;



#================================================
#	sub routine
#================================================
BEGIN	{
	print	"\n\n";
	print	"\t============================================================\n";
	print	"\t\|	Hello! Welcome to the T2-Design Perl Environment!   \|\n";
	print	"\t\|	Version	2.0					    \|\n";
	print	"\t\|	Copyright	1996~2003			    \|\n";
	print	"\t============================================================\n";
	print	"\n\n";
	print	"\t\t	M3357 project share pin logic generator\n";
	print	"\t\t	Parameter: Input_File, Output_file, [Complex_mode], [Share_pin]\n";
	print	"\t\t	Complex_mode, 1: complex_mode (default), 0: simple_mode\n";
	print	"\t\t	Share_pin, 1: Share_pin (default), 0: share_name\n";
	print	"\t============================================================\n";
}

END	{
	print	"\n\n";
	print	"Thanks for using T2-Design Perl Environment!\n";
}

#	open_input: open the input file and the file handle is INPUT
sub	open_input
{
	if (! -r $infile)
	{
    		die "Can't read input $infile\n";
	}
	if (! -f $infile)
	{
    		die "Input $infile is not a plain file\n";
	}
	if (! -s $infile)
	{
		die "Input $infile length is zero\n";
	}
	$infile_len = -s $infile;
	print $infile_len, "\n";


	open(INPUT,"<$infile") ||
		die "Can't input $infile $!";
}

#	open_output: open the output file and the file handle is OUTPUT
sub	open_output
{
	# Validate files
	# Or statements "||" short-circuit, so that if an early part
	# evaluates as true, Perl doesn't bother to evaluate the rest.
	# Here, if the file opens successfully, we don't abort:
	if ( -e $outfile)
	{
	    print STDERR "Output file $outfile exists!\n";
	    $ans = '';
	    until ($ans eq 'r' || $ans eq 'a' || $ans eq 'e' )
	    {
	        print STDERR "replace, append, or exit? ";
	        $ans = getc(STDIN);
	    }
	    if ($ans eq 'e') {exit}
	}

	if ($ans eq 'a') {$mode='>>'}
	else {$mode='>'}
	open(OUTPUT,"$mode$outfile") ||
	    die "Can't output $outfile $!";
}

#	Read out the share pin port and inout property and generate the
#	share pin logic.
sub	print_share_pin
{
	$mode_0_port	= $mode_0_port{$mode_0_port};
	$mode_1_port	= $mode_1_port{$mode_0_port};
	$mode_2_port	= $mode_2_port{$mode_0_port};
	$mode_3_port	= $mode_3_port{$mode_0_port};
	$mode_4_port	= $mode_4_port{$mode_0_port};
	$mode_5_port	= $mode_5_port{$mode_0_port};
	$mode_6_port	= $mode_6_port{$mode_0_port};
	$mode_7_port	= $mode_7_port{$mode_0_port};
	$mode_8_port	= $mode_8_port{$mode_0_port};

	$mode_0_inout	= $mode_0_inout{$mode_0_port};
	$mode_1_inout	= $mode_1_inout{$mode_0_port};
	$mode_2_inout	= $mode_2_inout{$mode_0_port};
	$mode_3_inout	= $mode_3_inout{$mode_0_port};
	$mode_4_inout	= $mode_4_inout{$mode_0_port};
	$mode_5_inout	= $mode_5_inout{$mode_0_port};
	$mode_6_inout	= $mode_6_inout{$mode_0_port};
	$mode_7_inout	= $mode_7_inout{$mode_0_port};
	$mode_8_inout	= $mode_8_inout{$mode_0_port};


	my	$mode_0_port_tmp	= $mode_0_port;
	my	$low				= "";
	if ($mode_0_port_tmp =~ /_$/) {
		$low = "_";
		$mode_0_port_tmp =~ s/_$//;
	}

	# if data is multiple bit one, check the left [ for multiple bit
	$mode_0_port_tmp =~ s/\[/_out2pad[/;
	if ($mode_0_port_tmp =~ /_out2pad/) {
	}
	else {
		# add _out2pad at the end of the data
		$mode_0_port_tmp	= "${mode_0_port_tmp}_out2pad";
	}


	#	set the bracket number to 0 before start
	$bracket_num	= 0;
	$head_line_end	= 0;

	if	($complex_mode == 1) {
		print	OUTPUT	"assign\t${mode_0_port_tmp}${low}\t= ";

		&print_first_output_data	($mode_name[8],	$mode_8_port, $mode_8_inout);
		&print_first_output_data	($mode_name[7],	$mode_7_port, $mode_7_inout);
		&print_first_output_data	($mode_name[0],	$mode_0_port, $mode_0_inout);
		&print_mid_output_data		($mode_name[1],	$mode_1_port, $mode_1_inout);
		&print_mid_output_data		($mode_name[2],	$mode_2_port, $mode_2_inout);
		&print_mid_output_data		($mode_name[3],	$mode_3_port, $mode_3_inout);
		&print_mid_output_data		($mode_name[4],	$mode_4_port, $mode_4_inout);
		&print_mid_output_data		($mode_name[5],	$mode_5_port, $mode_5_inout);
		&print_last_output_data		($mode_name[6],	$mode_6_port, $mode_6_inout);

		#	set the bracket number to 0 before start
		$bracket_num	= 0;
		$head_line_end	= 0;

		$mode_0_port_tmp =~ s/out2pad/oe_out2pad_/;
		print	OUTPUT	"assign\t", $mode_0_port_tmp, "\t= ";

		&print_first_output_enable	($mode_name[8],	$mode_8_port, $mode_8_inout);
		&print_first_output_enable	($mode_name[7],	$mode_7_port, $mode_7_inout);
		&print_first_output_enable	($mode_name[0],	$mode_0_port, $mode_0_inout);
		&print_mid_output_enable	($mode_name[1],	$mode_1_port, $mode_1_inout);
		&print_mid_output_enable	($mode_name[2],	$mode_2_port, $mode_2_inout);
		&print_mid_output_enable	($mode_name[3],	$mode_3_port, $mode_3_inout);
		&print_mid_output_enable	($mode_name[4],	$mode_4_port, $mode_4_inout);
		&print_mid_output_enable	($mode_name[5],	$mode_5_port, $mode_5_inout);
    	&print_last_output_enable	($mode_name[6],	$mode_6_port, $mode_6_inout);
    }
    else {
		print	OUTPUT	"assign\t${mode_0_port_tmp}${low}\t= ";
		&print_first_output_data	($mode_name[8],	$mode_8_port, $mode_8_inout);
		&print_first_output_data	($mode_name[5],	$mode_5_port, $mode_5_inout);
		&print_last_output_data		($mode_name[0],	$mode_0_port, $mode_0_inout);

		#	set the bracket number to 0 before start
		$bracket_num	= 0;
		$head_line_end	= 0;

		$mode_0_port_tmp =~ s/out2pad/oe_out2pad_/;
		print	OUTPUT	"assign\t", $mode_0_port_tmp, "\t= ";
		# generate the strap enable
		if ($mode_0_inout =~ /_s/) {
			print	"port $mode_0_port is a strap pin port!\n";
			printf	OUTPUT	("\n\t\t\t%20s ? 1\'b1 : ", '!warm_rst_');
		}
		&print_first_output_enable	($mode_name[8],	$mode_8_port, $mode_8_inout);
		&print_first_output_enable	($mode_name[5],	$mode_5_port, $mode_5_inout);
		&print_last_output_enable	($mode_name[0],	$mode_0_port, $mode_0_inout);
    }
}

#	first output is logic of "? :"
sub	print_first_output_data
{
	local ($mode, $data, $inout) = @_;
	local	$active_low	= '';
	if ($data ne 'unuse') {
		if ($data =~ /[_#]$/ )	{
			$active_low	= "_";
			$data =~ s/[_#]$//;	# remove last "_"
		}
		# if data is multiple bit one,
		if ($data =~ /\[[0-9]+\]/) {
			$data =~ s/(\[[0-9]+\])/_fromcore$active_low$1/;
		} else {
			$data = "${data}_fromcore${active_low}";
		}

		if ($inout =~ /1|0/) {
			print	"Error in INOUT type. Please check the input file\n";
			print	"Mode: $mode\n";
			print	"Signal: $data\n";
			print	"Inout: $inout\n";
		}

		if ($inout =~ /o/)	{
			printf	OUTPUT	("\n\t\t\t%20s ? %-25s : (", ${mode}, ${data});
			$bracket_num++;
		}
	}
}


#	mid output is logic of "&" and "|"
sub	print_mid_output_data
{
	local ($mode, $data, $inout) = @_;
	local	$active_low	= '';
	if ($data ne 'unuse') {
		if ($data =~ /[_#]$/ )	{
			$active_low	= "_";
			$data =~ s/[_#]$//;	# remove last "_"
		}
		# if data is multiple bit one,
		if ($data =~ /\[[0-9]+\]/) {
			$data =~ s/(\[[0-9]+\])/_fromcore$active_low$1/;
		} else {
			$data = "${data}_fromcore${active_low}";
		}

		if ($inout =~ /1|0/) {
			print	"Error in INOUT type. Please check the input file\n";
		}

		if ($inout =~ /o/)	{
			if ($complex_mode == 1) {
				if ($head_line_end) {
					printf	OUTPUT	(" | \n\t\t\t\t(%20s & %-40s)", ${mode}, ${data});
				}
				else {
					printf	OUTPUT	("\n\t\t\t\t(%20s & %-40s)", ${mode}, ${data});
					$head_line_end = 1;
				}
			} else {
				print	OUTPUT	"\n\t\t\t(${mode} & ${data}${active_low}) |";
			}
		}
	}
}

#	the last output is logic of "&" and "|"
#	it will add the correct number of ")" at the end of the statement
sub	print_last_output_data
{
	my ($mode, $data, $inout) = @_;
	my	$active_low	= "";
	if ($data ne 'unuse') {
		if ($data =~ /[_#]$/)	{
			$active_low	= "_";
			$data =~ s/[_#]$//;
		}
		# if data is multiple bit one,
		if ($data =~ /\[[0-9]+\]/) {
			$data =~ s/(\[[0-9]+\])/_fromcore$active_low$1/;
		} else {
			$data = "${data}_fromcore${active_low}";
		}

		if ($inout =~ /1|0/) {
			print	"Error in INOUT type. Please check the input file\n";
		}

		if ($inout =~ /o/)	{
			if ($complex_mode == 1) {
				printf	OUTPUT	(" |\n\t\t\t\t(%20s & %-40s)", ${mode}, ${data});
				print	OUTPUT	"\t";
			} else {
				printf	OUTPUT	("%-40s", ${data});
				print	OUTPUT	"\t";
			}
			for ($i = 0; $i <$bracket_num; $i++) {
				print	OUTPUT	')';
			}
			print	OUTPUT	";\n\n";
		}
		else {	# If the last port is an input port
			if ($complex_mode == 1) {
				print	OUTPUT	"\t";
			} else {
				printf	OUTPUT	("%-40s", "1\'b0");
				print	OUTPUT	"\t";
			}
			for ($i = 0; $i <$bracket_num; $i++) {
				print	OUTPUT	')';
			}
			print	OUTPUT	";\n\n";
		}
	}
	else {	# If this port is unuse
		if ($complex_mode == 1) {
			print	OUTPUT	"\t";
		} else {
			printf	OUTPUT	("%-40s", "1\'b0");
		}
		for ($i = 0; $i <$bracket_num; $i++) {
			print	OUTPUT	')';
		}
		print	OUTPUT	";\n\n";
	}
	$bracket_num = 0;
}

#	first output enable is the logic of "? :"
sub	print_first_output_enable
{
	my ($mode, $data, $inout) = @_;
	my	$active_low	= "";
	if ($data ne 'unuse') {
		if ($data =~ /[_#]$/ )	{
			$active_low	= "_";
			$data =~ s/[_#]$//;	# remove last "_"
		}
		# if data is multiple bit one,
		if ($data =~ /\[[0-9]+\]/) {
			$data =~ s/(\[[0-9]+\])/_oe_fromcore_$1/;
		} else {
			$data = "${data}_oe_fromcore_";
		}

		if ($inout eq 'o')	{	# output only
			printf	OUTPUT	("\n\t\t\t%20s ? %-25s : (", ${mode}, "SIGNAL_OUTPUT_ENABLE_");
			$bracket_num++;
		}
		elsif ($inout eq 'i') {	# input only
			printf	OUTPUT	("\n\t\t\t%20s ? %-25s : (", ${mode}, "SIGNAL_INPUT_ENABLE");
			$bracket_num++;
		}
		elsif ($inout =~ /o/) {	# oe or inout type
			printf	OUTPUT	("\n\t\t\t%20s ? %-25s : (", ${mode}, ${data});
			$bracket_num++;
		}
	}
}

#	the mid output enable logic is "&" and "|"
sub	print_mid_output_enable
{
	my ($mode, $data, $inout) = @_;
	my	$active_low	= "";
	my	$data_tmp	= "";
	if ($data ne 'unuse') {
		if ($data =~ /[_#]$/ )	{
			$active_low	= "_";
			$data =~ s/[_#]$//;	# remove last "_"
		}
		# if data is multiple bit one,
		if ($data =~ /\[[0-9]+\]/) {
			$data =~ s/(\[[0-9]+\])/_oe_fromcore_$1/;
		} else {
			$data = "${data}_oe_fromcore_";
		}

		if ($inout eq 'o')	{	# output only
			if ($complex_mode == 1) {
				if ($head_line_end) {
					printf	OUTPUT	(" | \n\t\t\t\t(%20s & %-40s)", ${mode}, "SIGNAL_OUTPUT_ENABLE_");
					$head_line_end = 1;
				} else {
					printf	OUTPUT	("\n\t\t\t\t(%20s & %-40s)", ${mode}, "SIGNAL_OUTPUT_ENABLE_");
					$head_line_end = 1;
				}
			} else {
				printf	OUTPUT	("\n\t\t\t\t(%20s & %-40s)", ${mode}, "SIGNAL_OUTPUT_ENABLE_");
			}
		}
		elsif ($inout eq 'i') {	# input only
			if ($complex_mode == 1) {
				if ($head_line_end) {
					printf	OUTPUT	(" | \n\t\t\t\t(%20s & %-40s)", ${mode}, "SIGNAL_INPUT_ENABLE");
					$head_line_end = 1;
				}
				else {
					printf	OUTPUT	("\n\t\t\t\t(%20s & %-40s)", ${mode}, "SIGNAL_INPUT_ENABLE");
					$head_line_end = 1;
				}
			} else {
				printf	OUTPUT	("\n\t\t\t\t(%20s & %-40s)", ${mode}, "SIGNAL_INPUT_ENABLE");
			}
		}
		elsif ($inout =~ /o/) {
			if ($complex_mode == 1) {
				if ($head_line_end) {
					printf	OUTPUT	(" | \n\t\t\t\t(%20s & %-40s)", ${mode}, ${data});
					$head_line_end = 1;
				}
				else {
					printf	OUTPUT	("\n\t\t\t\t(%20s & %-40s)", ${mode}, ${data});
					$head_line_end = 1;
				}
			} else {
				printf	OUTPUT	("\n\t\t\t\t(%20s & %-40s)", ${mode}, ${data});
			}
		}
	}
}

# the last output enable is logic of "&" and "|"
# it will add the correct number of ")" and the end of the statement
sub	print_last_output_enable
{
	my ($mode, $data, $inout) = @_;
	my	$active_low	= "";
	my	$data_tmp	= "";
	if ($data ne 'unuse') {
		if ($data =~ /[_#]$/)	{
			$active_low	= "_";
			$data =~ s/[_#]$//;
		}
		# if data is multiple bit one, check the left [
		if ($data =~ /\[[0-9]+\]/) {
			$data =~ s/(\[[0-9]+\])/_oe_fromcore_$1/;
		} else {
			$data = "${data}_oe_fromcore_";
		}

		if ($inout =~ /i\/o|oe/)	{ #oe or inout type
			if ($complex_mode == 1) {
				printf	OUTPUT	(" |\n\t\t\t\t(%20s & %-40s)", ${mode}, ${data});
			} else {
				printf	OUTPUT	("%-40s", ${data});
			}
			print	OUTPUT	"\t";
			for ($i = 0; $i <$bracket_num; $i++) {
				print	OUTPUT	')';
			}
			print	OUTPUT	";\n\n";
		}
		elsif ($inout =~ /o/)	{	# output only
			if ($complex_mode == 1) {
				printf	OUTPUT	(" |\n\t\t\t\t(%20s & %-40s)", ${mode}, "SIGNAL_OUTPUT_ENABLE_");
			} else {
				printf	OUTPUT	("%-40s", "SIGNAL_OUTPUT_ENABLE_");
			}
			print	OUTPUT	"\t";
			for ($i = 0; $i <$bracket_num; $i++) {
				print	OUTPUT	')';
			}
			print	OUTPUT	";\n\n";
		}
		elsif ($inout =~ 'i') {	# input only
			if ($complex_mode == 1) {
				printf	OUTPUT	(" |\n\t\t\t\t(%20s & %-40s)", ${mode}, "SIGNAL_INPUT_ENABLE");
			} else {
				printf	OUTPUT	("%-40s", "SIGNAL_INPUT_ENABLE");
			}
			print	OUTPUT	"\t";
			for ($i = 0; $i <$bracket_num; $i++) {
				print	OUTPUT	')';
			}
			print	OUTPUT	";\n\n";
		}
		else {	# if the last port is an input port
			if ($complex_mode == 1) {
				print	OUTPUT	"\t";
			} else {
				printf	OUTPUT	("%-40s", "1\'b0");
			}
			for ($i = 0; $i <$bracket_num; $i++) {
				print	OUTPUT	')';
			}
			print	OUTPUT	";\n\n";
		}
	}
	else {
		print	OUTPUT	"\t";
		for ($i = 0; $i <$bracket_num; $i++) {
			print	OUTPUT	')';
		}
		print	OUTPUT	";\n\n";
	}
}

sub	print_input_data
{
	my ($ide_mode_port, $mode, $data, $inout) = @_;
	my	$active_low	= "";
	my	$ide_active_low = "";
	my	$tmp;
	# If the $data is same as the mode_0_port and the $inout is same as the mode_0_inout
	#  dont output it.
	if (($data eq $mode_0_port{$ide_mode_port}) &&
		($inout eq $mode_0_inout{$ide_mode_port}) &&
		($mode ne $mode_name[0])) {
		print	OUTPUT	"//";
		return 0;
	}

	if ($data =~ /[_#]$/)	{
		$active_low	= "_";
		$data =~ s/[_#]$//;
	}
	# if data is multiple bit one,
	if ($data =~ /\[[0-9]+\]/) {
		$data =~ s/(\[[0-9]+\])/_out2core$active_low$1/;
	} else {
		$data = "${data}_out2core$active_low";
	}
	if ($ide_mode_port =~ /[_#]$/)	{
		$ide_active_low	= "_";
		$ide_mode_port =~ s/[_#]$//;
	}

	# if data is multiple bit one,
	if ($ide_mode_port	=~ /(\[[0-9]+\])/) {
		$ide_mode_port	=~ s/(\[[0-9]+\])/_frompad$ide_active_low$1/;
	} else {
		$ide_mode_port	= "${ide_mode_port}_frompad$ide_active_low";
	}

	if (!($data =~ /unuse/)) {
		if ($active_low =~ /_/) {
			$disable	= "1\'b1";
		}
		else {
			$disable	= "1\'b0";
		}

		if ($inout =~ /i/)
		{
			print	OUTPUT	"assign	${data}\t= $mode ? ${ide_mode_port} : $disable;\n";
		}
	}
}

#	print the name share that related to control logic instead of working mode
sub	print_share_name
{
	$mode_0_port	= $mode_0_port{$mode_0_port};
	$mode_1_port	= $mode_1_port{$mode_0_port};
	$mode_2_port	= $mode_2_port{$mode_0_port};
	$mode_3_port	= $mode_3_port{$mode_0_port};	# The control logic name

	$mode_0_inout	= $mode_0_inout{$mode_0_port};
	$mode_1_inout	= $mode_1_inout{$mode_0_port};
	$mode_2_inout	= $mode_2_inout{$mode_0_port};
	$mode_3_inout	= $mode_3_inout{$mode_0_port};	# The control logic selection

	my	$active_low	= '';

	my	$mode_0_port_tmp	= $mode_0_port;
	my	$low				= "";
	if ($mode_0_port_tmp =~ /_$/) {
		$low = "_";
		$mode_0_port_tmp =~ s/_$//;
	}

	my	$mode_1_port_tmp	= $mode_1_port;
	if ($mode_1_port_tmp =~ /[_#]$/)	{
		$active_low	= "_";
		$mode_1_port_tmp =~ s/[_#]$//;
	}
	# if data is multiple bit one,
	if ($mode_1_port_tmp =~ /\[[0-9]+\]/) {
		$mode_1_port_tmp =~ s/(\[[0-9]+\])/_fromcore$active_low$1/;
	} else {
		$mode_1_port_tmp = "${mode_1_port_tmp}_fromcore$active_low";
	}

	$active_low = '';
	my	$mode_2_port_tmp	= $mode_2_port;
	if ($mode_2_port_tmp =~ /[_#]$/)	{
		$active_low	= "_";
		$mode_2_port_tmp =~ s/[_#]$//;
	}
	# if data is multiple bit one,
	if ($mode_2_port_tmp =~ /\[[0-9]+\]/) {
		$mode_2_port_tmp =~ s/(\[[0-9]+\])/_fromcore$active_low$1/;
	} else {
		$mode_2_port_tmp = "${mode_2_port_tmp}_fromcore$active_low";
	}

	# if the signal is a inout type
	if ($mode_0_inout =~ /o/) {
		$mode_0_port_tmp = "${mode_0_port_tmp}_fromcore";
	} else {
		print	"$mode_0_port_tmp is an input port";
		exit
	}
	if (!($mode_1_inout =~ /o/)) {
		$mode_1_port_tmp = "1\'b1";
	}
	if (!($mode_2_inout =~ /o/)) {
		$mode_2_port_tmp = "1\'b1";
	}

	print	OUTPUT	"wire\t${mode_0_port_tmp}${low}\t= ";
	print	OUTPUT	"${mode_3_port}_fromcore ? ";
	if ($mode_3_inout eq '1') {
		print	OUTPUT "$mode_1_port_tmp : $mode_2_port_tmp\n";
	} elsif ($mode_3_inout eq '2') {
		print	OUTPUT "$mode_2_port_tmp : $mode_1_port_tmp\n";
	} else {
		print	"Error in $mode_0_port_tmp logic select mode, get $mode_3_inout now!\n";
	}
}


#	print the name share oe that related to control logic instead of working mode
sub	print_share_name_oe
{
	$mode_0_port	= $mode_0_port{$mode_0_port};
	$mode_1_port	= $mode_1_port{$mode_0_port};
	$mode_2_port	= $mode_2_port{$mode_0_port};
	$mode_3_port	= $mode_3_port{$mode_0_port};	# The control logic name

	$mode_0_inout	= $mode_0_inout{$mode_0_port};
	$mode_1_inout	= $mode_1_inout{$mode_0_port};
	$mode_2_inout	= $mode_2_inout{$mode_0_port};
	$mode_3_inout	= $mode_3_inout{$mode_0_port};	# The control logic selection

	my	$mode_0_port_tmp	= $mode_0_port;
	if ($mode_0_port_tmp =~ /[_#]$/) {
		$mode_0_port_tmp =~ s/[_#]$//;
	}

	my	$mode_1_port_tmp	= $mode_1_port;
	if ($mode_1_port_tmp =~ /[_#]$/)	{
		$mode_1_port_tmp =~ s/[_#]$//;
	}
	# if data is multiple bit one,
	if ($mode_1_port_tmp =~ /\[[0-9]+\]/) {
		$mode_1_port_tmp =~ s/(\[[0-9]+\])/_oe_fromcore_$1/;
	} else {
		$mode_1_port_tmp = "${mode_1_port_tmp}_oe_fromcore_";
	}

	my	$mode_2_port_tmp	= $mode_2_port;
	if ($mode_2_port_tmp =~ /[_#]$/)	{
		$mode_2_port_tmp =~ s/[_#]$//;
	}
	# if data is multiple bit one,
	if ($mode_2_port_tmp =~ /\[[0-9]+\]/) {
		$mode_2_port_tmp =~ s/(\[[0-9]+\])/_oe_fromcore_$1/;
	} else {
		$mode_2_port_tmp = "${mode_2_port_tmp}_oe_fromcore_";
	}

	# if the signal is a inout type
	if ($mode_0_inout =~ /o/) {
		$mode_0_port_tmp = "${mode_0_port_tmp}_oe_fromcore_";
	} else {
		print	"$mode_0_port_tmp is an input port";
		exit
	}
	if ($mode_1_inout eq 'i') {
		$mode_1_port_tmp = "1\'b1";
	} elsif ($mode_1_inout eq 'o') {
		$mode_1_port_tmp = "1\'b0";
	}

	if ($mode_2_inout eq 'i') {
		$mode_2_port_tmp = "1\'b1";
	} elsif ($mode_2_inout eq 'o') {
		$mode_2_port_tmp = "1\'b0";
	}
	print	OUTPUT	"wire\t${mode_0_port_tmp}\t= ";
	print	OUTPUT	"${mode_3_port}_fromcore ? ";
	if ($mode_3_inout eq '1') {
		print	OUTPUT "$mode_1_port_tmp : $mode_2_port_tmp\n";
	} elsif ($mode_3_inout eq '2') {
		print	OUTPUT "$mode_2_port_tmp : $mode_1_port_tmp\n";
	} else {
		print	"Error in $mode_0_port_tmp logic select mode, get $mode_3_inout now!\n";
	}
}
		