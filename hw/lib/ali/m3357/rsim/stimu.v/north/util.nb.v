/***************************************************************************************
* File	: util.nb.v		---------- Runner Yang
* Description : 	The following task refine CPU task for PCI config/memory/IO access
*					In m6304 rsim environment, there's 5 PCI devices on PCI bus:
*					PCI host, USB, IDE, external PCI master(mm), external PCI target(tt)
*					CPU can access 4 target's config/memory/IO space
*					In read back task(task name end with 'RDBK'), you can provide any 32
*					bit data, and task will return the read back data
*
* Task format:
*	CPU2XX_CFGxx		(addr_, ben_, data);	// 8bit addr, 4bit ben_, 32bit data
*	CPU2PCITARGET_IOxx	(addr, ben_, data);		// 32bit addr, 4bit ben_, 32bit data
*	CPU2PCITARGET_MEMxx_WORD	(addr, ben_, data);	// 32bit addr, 4bit ben_, 32bit data
*	CPU2PCITARGET_MEMRD_BURST	(start_addr, length);	//32bit addr, interger length
*	CPU2PCITARGET_MEMWR_BURST	(start_addr, length, 1st_data);	// 32bit addr, integer length, 32bit 1st_data
*
* History:			2002-10-11 Yuky,	Delete all the IDE tasks here. Because the new IDE
*										did not use the PCI bus, but use local IO/MEM bus
***************************************************************************************/
/*******************************************************************
 Task list:
	 CPU2PCIHOST_CFGRD;
	 CPU2PCIHOST_CFGWR;
	 CPU2PCIHOST_CFGRDBK;
	 CPU2USB_CFGRD;
	 CPU2USB_CFGWR;
	 CPU2USB_CFGRDBK;
	 CPU2IDE_CFGRD;
	 CPU2IDE_CFGWR;
	 CPU2IDE_CFGRDBK;
	 CPU2IDE_IORD;
	 CPU2IDE_IOWR;
	 CPU2IDE_IORDBK;
	 CPU2PCITARGET_CFGRD;
	 CPU2PCITARGET_CFGWR;
	 CPU2PCITARGET_CFGRDBK;
	 CPU2PCITARGET_IORD;
	 CPU2PCITARGET_IOWR;
	 CPU2PCITARGET_IORDBK;
	 CPU2PCITARGET_MEMRD_WORD;
	 CPU2PCITARGET_MEMWR_WORD;
	 CPU2PCITARGET_MEMRDBK_WORD;
	 CPU2PCITARGET_MEMRD_BURST;
	 CPU2PCITARGET_MEMWR_BURST;

	 pci_int_active;
	 pci_inta_processor;

	 cpu2flash_read1w;
	 cpu2flash_read2w;
	 cpu2flash_read4w;
	 cpu2flash_readbk1w;
	 cpu2flash_readbk2w;
	 cpu2flash_readbk4w;
	 cpu2flash_write1w;
	 cpu2flash_write2w;
	 cpu2flash_write4w;
	 flash_access_enable;
	 flash_access_disable;
	 flash_write_enable;
	 flash_write_disable;
	 FLASH_Speed_Select;

	 SYS_IO_Read;
	 SYS_IO_Readbk;
	 SYS_IO_Write;

	 // test PCI memory
	Memory_Burst_Test_Read
	Mem_Burst_Read
	Memory_Burst_Test_Write
	Mem_Burst_Write
	Mem_Read_Multi_Test
	Mem_Read_Line_Test
	Mem_Write_Inv_Test

//	DISP_BH_SDRAM_load_memory
//	DISP_BH_SDRAM_dump_memory  // Snow Yi, 2003.08.28
*******************************************************************/

//cpu2pci_cfgwr	(8'h90, 5'h1, 4'b1110, 3'h0, 32'h0000_0000);
//cpu2pci_cfgrd	(8'h90, 5'h1, 4'b1110, 3'h0, 32'h0000_0000);
//cpu2pci_cfgwr	(8'h90, 5'h1, 4'b1110, 3'h0, 32'hffff_ffff);
//cpu2pci_cfgrd	(8'h90, 5'h1, 4'b1110, 3'h0, 32'h0000_0001);

//	parameter PCI_IOBase = 32'h1000_0000;	// defined in top.param.v
//	parameter PCI_Mem_Base = 32'h3000_0000;	// defined in top.param.v
// define pci output file
integer pci_outfile_vec;
initial begin
	pci_outfile_vec = $fopen("pci_outfile.log");
end

integer pci_i, pci_j, pci_k;

// ================ PCI host (device 1) task =============
task CPU2PCIHOST_CFGRD;
input	[7:0]	addr;
input	[3:0]	ben_;
input	[31:0]	exp_data;

reg	[31:0]	data_tmp;
reg [31:0]	xdata, rdata;
begin
	cpu2pci_cfgrdbk	(addr, 5'h1, ben_, 3'h0, data_tmp);
	xdata = exp_data | ({{8{ben_[3]}},{8{ben_[2]}},{8{ben_[1]}},{8{ben_[0]}}});
	rdata = data_tmp | ({{8{ben_[3]}},{8{ben_[2]}},{8{ben_[1]}},{8{ben_[0]}}});
	if (xdata !== rdata)	begin
		$display ("Error_T2_rsim : CPU2PCIHOST_CFGRD: Addr = %hh  ben_ = %bb  RD_data = %hh  EXP_data = %hh", addr, ben_, data_tmp, exp_data);
		$fdisplay (pci_outfile_vec, "Error_T2_rsim : CPU2PCIHOST_CFGRD: Addr = %hh  ben_ = %bb  RD_data = %hh  EXP_data = %hh", addr, ben_, data_tmp, exp_data);
	end	else begin
		$display ("CPU2PCIHOST_CFGRD: Addr = %hh  ben_ = %bb  RD_data = EXP_data = %hh", addr, ben_, data_tmp);
		$fdisplay (pci_outfile_vec, "CPU2PCIHOST_CFGRD: Addr = %hh  ben_ = %bb  RD_data = EXP_data = %hh", addr, ben_, data_tmp);
	end
end
endtask

task CPU2PCIHOST_CFGWR;
input	[7:0]	addr;
input	[3:0]	ben_;
input	[31:0]	wr_data;

begin
	cpu2pci_cfgwr	(addr, 5'h1, ben_, 3'h0, wr_data);
	$display ("CPU2PCIHOST_CFGWR: Addr = %hh  ben_ = %bb  WR_data = %hh", addr, ben_, wr_data);
	$fdisplay (pci_outfile_vec, "CPU2PCIHOST_CFGWR: Addr = %hh  ben_ = %bb  WR_data = %hh", addr, ben_, wr_data);
end
endtask

task CPU2PCIHOST_CFGRDBK;
input	[7:0]	addr;
input	[3:0]	ben_;
input	[31:0]	exp_data;

reg	[31:0]	data_tmp;

begin
	cpu2pci_cfgrdbk	(addr, 5'h1, ben_, 3'h0, data_tmp);
	$display ("CPU2PCIHOST_CFGRDBK: Addr = %hh  ben_ = %bb  RD_data = %hh", addr, ben_, data_tmp);
	$fdisplay (pci_outfile_vec, "CPU2PCIHOST_CFGRDBK: Addr = %hh  ben_ = %bb  RD_data = %hh", addr, ben_, data_tmp);
end
endtask

// ====================== USB(device 2) task ==============================
task CPU2USB_CFGRD;
input	[7:0]	addr;
input	[3:0]	ben_;
input	[31:0]	exp_data;

reg	[31:0]	data_tmp;
reg [31:0]	xdata, rdata;
begin
	cpu2pci_cfgrdbk	(addr, 5'h2, ben_, 3'h0, data_tmp);// usb is device 2
	xdata = exp_data | ({{8{ben_[3]}},{8{ben_[2]}},{8{ben_[1]}},{8{ben_[0]}}});
	rdata = data_tmp | ({{8{ben_[3]}},{8{ben_[2]}},{8{ben_[1]}},{8{ben_[0]}}});
	if (xdata !== rdata) begin
		$display ("Error_T2_rsim :	CPU2USB_CFGRD: Addr = %hh  ben_ = %bb  RD_data = %hh  EXP_data = %hh", addr, ben_, data_tmp, exp_data);
		$fdisplay (pci_outfile_vec, "Error_T2_rsim :	CPU2USB_CFGRD: Addr = %hh  ben_ = %bb  RD_data = %hh  EXP_data = %hh", addr, ben_, data_tmp, exp_data);
	end else begin
		$display ("CPU2USB_CFGRD: Addr = %hh  ben_ = %bb  RD_data = EXP_data = %hh", addr, ben_, data_tmp);
		$fdisplay (pci_outfile_vec, "CPU2USB_CFGRD: Addr = %hh  ben_ = %bb  RD_data = EXP_data = %hh", addr, ben_, data_tmp);
	end
end
endtask


task CPU2USB_CFGWR;
input	[7:0]	addr;
input	[3:0]	ben_;
input	[31:0]	wr_data;

begin
	cpu2pci_cfgwr	(addr, 5'h2, ben_, 3'h0, wr_data);
	$display ("CPU2USB_CFGWR: Addr = %hh  ben_ = %bb  WR_data = %hh", addr, ben_, wr_data);
	$fdisplay (pci_outfile_vec, "CPU2USB_CFGWR: Addr = %hh  ben_ = %bb  WR_data = %hh", addr, ben_, wr_data);
end
endtask

task CPU2USB_CFGRDBK;
input	[7:0]	addr;
input	[3:0]	ben_;
inout	[31:0]	rd_data;

reg	[31:0]	data_tmp;

begin
	cpu2pci_cfgrdbk	(addr, 5'h2, ben_, 3'h0, data_tmp);
	$display ("CPU2USB_CFGRDBK: Addr = %hh  ben_ = %bb  RD_data = %hh", addr, ben_, data_tmp);
	$fdisplay (pci_outfile_vec, "CPU2USB_CFGRDBK: Addr = %hh  ben_ = %bb  RD_data = %hh", addr, ben_, data_tmp);
	rd_data = data_tmp;
end
endtask

// ================== IDE (device 3) task ===========================
/*
task CPU2IDE_CFGRD;
input	[7:0]	addr;
input	[3:0]	ben_;
input	[31:0]	exp_data;

reg	[31:0]	data_tmp;
reg [31:0]	xdata, rdata;
begin
	cpu2pci_cfgrdbk	(addr, 5'h3, ben_, 3'h0, data_tmp);
	xdata = exp_data | ({{8{ben_[3]}},{8{ben_[2]}},{8{ben_[1]}},{8{ben_[0]}}});
	rdata = data_tmp | ({{8{ben_[3]}},{8{ben_[2]}},{8{ben_[1]}},{8{ben_[0]}}});
	if (xdata !== rdata)	begin
		$display ("Error_T2_rsim :	CPU2IDE_CFGRD: Addr = %hh  ben_ = %bb  RD_data = %hh  EXP_data = %hh", addr, ben_, data_tmp, exp_data);
		$fdisplay (pci_outfile_vec, "Error_T2_rsim :	CPU2IDE_CFGRD: Addr = %hh  ben_ = %bb  RD_data = %hh  EXP_data = %hh", addr, ben_, data_tmp, exp_data);
	end	else begin
		$display ("CPU2IDE_CFGRD: Addr = %hh  ben_ = %bb  RD_data = EXP_data = %hh", addr, ben_, data_tmp);
		$fdisplay (pci_outfile_vec, "CPU2IDE_CFGRD: Addr = %hh  ben_ = %bb  RD_data = EXP_data = %hh", addr, ben_, data_tmp);
	end
end
endtask

task CPU2IDE_CFGWR;
input	[7:0]	addr;
input	[3:0]	ben_;
input	[31:0]	wr_data;

begin
	cpu2pci_cfgwr	(addr, 5'h3, ben_, 3'h0, wr_data);
	$display ("CPU2IDE_CFGWR: Addr = %hh  ben_ = %bb  WR_data = %hh", addr, ben_, wr_data);
	$fdisplay (pci_outfile_vec, "CPU2IDE_CFGWR: Addr = %hh  ben_ = %bb  WR_data = %hh", addr, ben_, wr_data);
end
endtask

task CPU2IDE_CFGRDBK;
input	[7:0]	addr;
input	[3:0]	ben_;
input	[31:0]	exp_data;

reg	[31:0]	data_tmp;

begin
	cpu2pci_cfgrdbk	(addr, 5'h3, ben_, 3'h0, data_tmp);
	$display ("CPU2IDE_CFGRDBK: Addr = %hh  ben_ = %bb  RD_data = %hh", addr, ben_, data_tmp);
	$fdisplay (pci_outfile_vec, "CPU2IDE_CFGRDBK: Addr = %hh  ben_ = %bb  RD_data = %hh", addr, ben_, data_tmp);
end
endtask



// ================= CPU to IDE (device 4) IO task =======================
task CPU2IDE_IORD;
input	[31:0]	addr;
input	[3:0]	ben_;
input	[31:0]	exp_data;

reg		[31:0]	data_tmp;
reg 	[31:0]	xdata, rdata;
begin
	`pci_cpu_readbk1w	(PCI_IOBase + addr, ~ben_, data_tmp);

	xdata = exp_data | ({{8{ben_[3]}},{8{ben_[2]}},{8{ben_[1]}},{8{ben_[0]}}});
	rdata = data_tmp | ({{8{ben_[3]}},{8{ben_[2]}},{8{ben_[1]}},{8{ben_[0]}}});
	if (xdata !== rdata)	begin
		$display ("Error_T2_rsim :	CPU2IDE_IORD: Offset_Addr = %hh  ben_ = %bb  RD_data = %hh  EXP_data = %hh", addr, ben_, data_tmp, exp_data);
		$fdisplay (pci_outfile_vec, "Error_T2_rsim :	CPU2IDE_IORD: Addr = %hh  ben_ = %bb  RD_data = %hh  EXP_data = %hh", addr, ben_, data_tmp, exp_data);
	end	else begin
		$display ("CPU2IDE_IORD: Addr = %hh  ben_ = %bb  RD_data = EXP_data = %hh", addr, ben_, data_tmp);
		$fdisplay (pci_outfile_vec, "CPU2IDE_IORD: Addr = %hh  ben_ = %bb  RD_data = EXP_data = %hh", addr, ben_, data_tmp);
	end
end
endtask

task CPU2IDE_IOWR;
input	[31:0]	addr;
input	[3:0]	ben_;
input	[31:0]	wr_data;
begin
	`pci_cpu_write1w	(PCI_IOBase + addr, ~ben_, wr_data);
	$display ("CPU2IDE_IOWR: Addr = %hh  ben_ = %bb  WR_data = %hh", addr, ben_, wr_data);
	$fdisplay (pci_outfile_vec, "CPU2IDE_IOWR: Addr = %hh  ben_ = %bb  WR_data = %hh", addr, ben_, wr_data);
end
endtask


task CPU2IDE_IORDBK;
input	[31:0]	addr;
input	[3:0]	ben_;
input	[31:0]	exp_data;

reg		[31:0]	data_tmp;
begin
	`pci_cpu_readbk1w	(PCI_IOBase + addr, ~ben_, data_tmp);
	$display ("CPU2IDE_IORDBK: Addr = %hh  ben_ = %bb  RD_data = %hh", addr, ben_, data_tmp);
	$fdisplay (pci_outfile_vec, "CPU2IDE_IORDBK: Addr = %hh  ben_ = %bb  RD_data = %hh", addr, ben_, data_tmp);
end
endtask
*/


// ================= PCI target (device 4) config task =======================
task CPU2PCITARGET_CFGRD;
input	[7:0]	addr;
input	[3:0]	ben_;
input	[31:0]	exp_data;

reg	[31:0]	data_tmp;
reg [31:0]	xdata, rdata;
begin
	cpu2pci_cfgrdbk	(addr, 5'h4, ben_, 3'h0, data_tmp);
	xdata = exp_data | ({{8{ben_[3]}},{8{ben_[2]}},{8{ben_[1]}},{8{ben_[0]}}});
	rdata = data_tmp | ({{8{ben_[3]}},{8{ben_[2]}},{8{ben_[1]}},{8{ben_[0]}}});
	if (xdata !== rdata) begin
		$display ("Error_T2_rsim :	CPU2PCITARGET_CFGRD: Addr = %hh  ben_ = %bb  RD_data = %hh  EXP_data = %hh", addr, ben_, data_tmp, exp_data);
		$fdisplay (pci_outfile_vec, "Error_T2_rsim :	CPU2PCITARGET_CFGRD: Addr = %hh  ben_ = %bb  RD_data = %hh  EXP_data = %hh", addr, ben_, data_tmp, exp_data);
	end else begin
		$display ("CPU2PCITARGET_CFGRD: Addr = %hh  ben_ = %bb  RD_data = EXP_data = %hh", addr, ben_, data_tmp);
		$fdisplay (pci_outfile_vec, "CPU2PCITARGET_CFGRD: Addr = %hh  ben_ = %bb  RD_data = EXP_data = %hh", addr, ben_, data_tmp);
	end
end
endtask


task CPU2PCITARGET_CFGWR;
input	[7:0]	addr;
input	[3:0]	ben_;
input	[31:0]	wr_data;

begin
	cpu2pci_cfgwr	(addr, 5'h4, ben_, 3'h0, wr_data);
	$display ("CPU2PCITARGET_CFGWR: Addr = %hh  ben_ = %bb  WR_data = %hh", addr, ben_, wr_data);
	$fdisplay (pci_outfile_vec, "CPU2PCITARGET_CFGWR: Addr = %hh  ben_ = %bb  WR_data = %hh", addr, ben_, wr_data);
end
endtask

task CPU2PCITARGET_CFGRDBK;
input	[7:0]	addr;
input	[3:0]	ben_;
input	[31:0]	exp_data;

reg	[31:0]	data_tmp;

begin
	cpu2pci_cfgrdbk	(addr, 5'h4, ben_, 3'h0, data_tmp);
	$display ("CPU2PCITARGET_CFGRDBK: Addr = %hh  ben_ = %bb  RD_data = %hh", addr, ben_, data_tmp);
	$fdisplay (pci_outfile_vec, "CPU2PCITARGET_CFGRDBK: Addr = %hh  ben_ = %bb  RD_data = %hh", addr, ben_, data_tmp);
end
endtask

// ================= PCI target (device 4) IO task =======================
task CPU2PCITARGET_IORD;
input	[31:0]	addr;
input	[3:0]	ben_;
input	[31:0]	exp_data;

reg		[31:0]	data_tmp;
reg 	[31:0]	xdata, rdata;
begin
	`pci_cpu_readbk1w	(PCI_IOBase + addr, ~ben_, data_tmp);

	xdata = exp_data | ({{8{ben_[3]}},{8{ben_[2]}},{8{ben_[1]}},{8{ben_[0]}}});
	rdata = data_tmp | ({{8{ben_[3]}},{8{ben_[2]}},{8{ben_[1]}},{8{ben_[0]}}});
	if (xdata !== rdata)	begin
		$display ("Error_T2_rsim :	CPU2PCITARGET_IORD: Offset_Addr = %hh  ben_ = %bb  RD_data = %hh  EXP_data = %hh", addr, ben_, data_tmp, exp_data);
		$fdisplay (pci_outfile_vec, "Error_T2_rsim :	CPU2PCITARGET_IORD: Addr = %hh  ben_ = %bb  RD_data = %hh  EXP_data = %hh", addr, ben_, data_tmp, exp_data);
	end	else begin
		$display ("CPU2PCITARGET_IORD: Addr = %hh  ben_ = %bb  RD_data = EXP_data = %hh", addr, ben_, data_tmp);
		$fdisplay (pci_outfile_vec, "CPU2PCITARGET_IORD: Addr = %hh  ben_ = %bb  RD_data = EXP_data = %hh", addr, ben_, data_tmp);
	end
end
endtask

task CPU2PCITARGET_IOWR;
input	[31:0]	addr;
input	[3:0]	ben_;
input	[31:0]	wr_data;
begin
	`pci_cpu_write1w	(PCI_IOBase + addr, ~ben_, wr_data);
	$display ("CPU2PCITARGET_IOWR: Addr = %hh  ben_ = %bb  WR_data = %hh", addr, ben_, wr_data);
	$fdisplay (pci_outfile_vec, "CPU2PCITARGET_IOWR: Addr = %hh  ben_ = %bb  WR_data = %hh", addr, ben_, wr_data);
end
endtask


task CPU2PCITARGET_IORDBK;
input	[31:0]	addr;
input	[3:0]	ben_;
input	[31:0]	exp_data;

reg		[31:0]	data_tmp;
begin
	`pci_cpu_readbk1w	(PCI_IOBase + addr, ~ben_, data_tmp);
	$display ("CPU2PCITARGET_IORDBK: Addr = %hh  ben_ = %bb  RD_data = %hh", addr, ben_, data_tmp);
	$fdisplay (pci_outfile_vec, "CPU2PCITARGET_IORDBK: Addr = %hh  ben_ = %bb  RD_data = %hh", addr, ben_, data_tmp);
end
endtask


// ================= PCI target (device 4) memory task =======================
task	 CPU2PCITARGET_MEMRD_WORD;
input	[31:0]	addr;
input	[3:0]	ben_;
input	[31:0]	exp_data;

reg		[31:0]	data_tmp;
reg 	[31:0]	xdata, rdata;
begin
	`pci_cpu_readbk1w	(PCI_Mem_Base + addr, ~ben_, data_tmp);

	xdata = exp_data | ({{8{ben_[3]}},{8{ben_[2]}},{8{ben_[1]}},{8{ben_[0]}}});
	rdata = data_tmp | ({{8{ben_[3]}},{8{ben_[2]}},{8{ben_[1]}},{8{ben_[0]}}});
	if (xdata !== rdata)	begin
		$display ("Error_T2_rsim :	CPU2PCITARGET_MEMRD_WORD: Offset_Addr = %hh  ben_ = %bb  RD_data = %hh  EXP_data = %hh", addr, ben_, data_tmp, exp_data);
		$fdisplay (pci_outfile_vec, "Error_T2_rsim :	CPU2PCITARGET_MEMRD_WORD: Addr = %hh  ben_ = %bb  RD_data = %hh  EXP_data = %hh", addr, ben_, data_tmp, exp_data);
	end	else begin
		$display ("CPU2PCITARGET_MEMRD_WORD: Addr = %hh  ben_ = %bb  RD_data = EXP_data = %hh", addr, ben_, data_tmp);
		$fdisplay (pci_outfile_vec, "CPU2PCITARGET_MEMRD_WORD: Addr = %hh  ben_ = %bb  RD_data = EXP_data = %hh", addr, ben_, data_tmp);
	end
end
endtask

task	 CPU2PCITARGET_MEMWR_WORD;
input	[31:0]	addr;
input	[3:0]	ben_;
input	[31:0]	wr_data;

begin
	`pci_cpu_write1w	(PCI_Mem_Base + addr, ~ben_, wr_data);
	$display ("CPU2PCITARGET_MEMWR_WORD: Addr = %hh  ben_ = %bb  WR_data = %hh", PCI_Mem_Base + addr, ben_, wr_data);
	$fdisplay (pci_outfile_vec, "CPU2PCITARGET_MEMWR_WORD: Addr = %hh  ben_ = %bb  WR_data = %hh", PCI_Mem_Base + addr, ben_, wr_data);
end
endtask

task	 CPU2PCITARGET_MEMRDBK_WORD;
input	[31:0]	addr;
input	[3:0]	ben_;
input	[31:0]	exp_data;

reg		[31:0]	data_tmp;
begin
	`pci_cpu_read1w	(PCI_Mem_Base + addr, ~ben_, data_tmp);
	$display ("CPU2PCITARGET_MEMRDBK_WORD: Addr = %hh  ben_ = %bb  RD_data = %hh", addr, ben_, data_tmp);
	$fdisplay (pci_outfile_vec, "CPU2PCITARGET_MEMRDBK_WORD: Addr = %hh  ben_ = %bb  RD_data = %hh", addr, ben_, data_tmp);
end
endtask

task	 CPU2PCITARGET_MEMRD_BURST;
input	[31:0]	start_addr;	// address offset
input	[7:0]	length	;	// maximun 256 word burst

integer i;
integer	data_num;
reg		[31:0]	addr_reg;
reg	[31:0]	rdata0, rdata1, rdata2, rdata3, rdata4, rdata5, rdata6, rdata7;
begin
	addr_reg = PCI_Mem_Base + {start_addr[31:2],2'b00};
	$display ( "CPU2PCITARGET_MEMRD_BURST : Start_Addr = %hh	Legth = %d", addr_reg, length);
	$fdisplay ( pci_outfile_vec, "CPU2PCITARGET_MEMRD_BURST : Start_Addr = %hh	Legth = %d", addr_reg, length);
	for (i=0;i<length[7:2];i=i+1) begin
		`pci_cpu_readbk4w(addr_reg, rdata0, rdata1, rdata2, rdata3);

		$display ("BURST RD_DATA [%hH]: %hh", addr_reg + 0 , rdata0);
		$display ("BURST RD_DATA [%hH]: %hh", addr_reg + 4 , rdata1);
		$display ("BURST RD_DATA [%hH]: %hh", addr_reg + 8 , rdata2);
		$display ("BURST RD_DATA [%hH]: %hh", addr_reg + 12, rdata3);
		$fdisplay (pci_outfile_vec, "BURST RD_DATA [%hH]: %hh", addr_reg + 0 , rdata0);
		$fdisplay (pci_outfile_vec, "BURST RD_DATA [%hH]: %hh", addr_reg + 4 , rdata1);
		$fdisplay (pci_outfile_vec, "BURST RD_DATA [%hH]: %hh", addr_reg + 8 , rdata2);
		$fdisplay (pci_outfile_vec, "BURST RD_DATA [%hH]: %hh", addr_reg + 12, rdata3);

		addr_reg = addr_reg + 32'h0000_0010;
	end

	data_num = i*4;

	if (length[1]==1'b1) begin
		`pci_cpu_readbk2w(addr_reg, rdata0, rdata1);

		$display ("BURST RD_DATA [%hH]: %hh", addr_reg + 0, rdata0);
		$display ("BURST RD_DATA [%hH]: %hh", addr_reg + 4, rdata1);
		$fdisplay (pci_outfile_vec, "BURST RD_DATA [%hH]: %hh", addr_reg + 0, rdata0);
		$fdisplay (pci_outfile_vec, "BURST RD_DATA [%hH]: %hh", addr_reg + 4, rdata1);

		addr_reg = addr_reg + 32'h0000_0008;
	end

	data_num = data_num + 2*length[1];

	if (length[0]==1'b1) begin
		`pci_cpu_readbk1w(addr_reg, 4'b1111, rdata0);

		$display ("BURST RD_DATA [%hH]: %hh", addr_reg + 0, rdata0);
		$fdisplay (pci_outfile_vec, "BURST RD_DATA [%hH]: %hh", addr_reg + 0, rdata0);

		addr_reg = addr_reg + 32'h0000_0004;
	end

	data_num = data_num + length[0];

end
endtask


task	 CPU2PCITARGET_MEMWR_BURST;
input	[31:0]	start_addr;
input	[7:0]	length;
input	[31:0]	first_data;

integer i;
integer	data_num;
reg		[31:0]	addr_reg;

reg	[31:0]	wdata0, wdata1, wdata2, wdata3;

begin
	addr_reg = PCI_Mem_Base + {start_addr[31:2],2'b00};
	wdata0 = first_data;
	wdata1 = wdata0 + 1;
	wdata2 = wdata1 + 1;
	wdata3 = wdata2 + 1;

	$display ( "CPU2PCITARGET_MEMWR_BURST : Start_Addr = %hh	Legth = %d", addr_reg, length);
	$fdisplay ( pci_outfile_vec, "CPU2PCITARGET_MEMWR_BURST : Start_Addr = %hh	Legth = %d", addr_reg, length);

	for (i=0;i<length[7:2];i=i+1) begin
		`pci_cpu_write4w(addr_reg, wdata0, wdata1, wdata2, wdata3);

		$display ("BURST WR_DATA [%hH]: %hh", addr_reg + 4'h0, wdata0);
		$display ("BURST WR_DATA [%hH]: %hh", addr_reg + 4'h4, wdata1);
		$display ("BURST WR_DATA [%hH]: %hh", addr_reg + 4'h8, wdata2);
		$display ("BURST WR_DATA [%hH]: %hh", addr_reg + 4'hc, wdata3);

		$fdisplay (pci_outfile_vec, "BURST WR_DATA [%hH]: %hh", addr_reg + 4'h0, wdata0);
		$fdisplay (pci_outfile_vec, "BURST WR_DATA [%hH]: %hh", addr_reg + 4'h4, wdata1);
		$fdisplay (pci_outfile_vec, "BURST WR_DATA [%hH]: %hh", addr_reg + 4'h8, wdata2);
		$fdisplay (pci_outfile_vec, "BURST WR_DATA [%hH]: %hh", addr_reg + 4'hc, wdata3);

		addr_reg = addr_reg + 32'h0000_0010;

		wdata0 = wdata3 + 1;
		wdata1 = wdata0 + 1;
		wdata2 = wdata1 + 1;
		wdata3 = wdata2 + 1;
	end

	if (length[1]==1'b1) begin
		`pci_cpu_write2w(addr_reg, wdata0, wdata1);

		$display ("BURST WR_DATA [%hH]: %hh", addr_reg + 4'h0, wdata0);
		$display ("BURST WR_DATA [%hH]: %hh", addr_reg + 4'h4, wdata1);

		$fdisplay (pci_outfile_vec, "BURST WR_DATA [%hH]: %hh", addr_reg + 4'h0, wdata0);
		$fdisplay (pci_outfile_vec, "BURST WR_DATA [%hH]: %hh", addr_reg + 4'h4, wdata1);

		addr_reg = addr_reg + 32'h0000_0008;

		wdata0 = wdata1 + 1;
		wdata1 = wdata0 + 1;

	end

	if (length[0]==1'b1) begin
		`pci_cpu_write1w (addr_reg, 4'b1111, wdata0);

		$display ("BURST WR_DATA [%hH]: %hh", addr_reg + 4'h0, wdata0);

		$fdisplay (pci_outfile_vec, "BURST WR_DATA [%hH]: %hh", addr_reg + 4'h0, wdata0);

		addr_reg = addr_reg + 32'h0000_0004;

		wdata0 = wdata1 + 1;

	end


end
endtask

task	 CPU2PCITARGET_MEMRD_BURST_TEST;
input	[31:0]	start_addr;	// address offset
input	[7:0]	length	;	// maximun 256 word burst
input 	[31:0]	begin_data;
integer i;
integer	data_num;
reg	[31:0] start_data;
reg		[31:0]	addr_reg;
reg	[31:0]	rdata0, rdata1, rdata2, rdata3, rdata4, rdata5, rdata6, rdata7;
begin
	start_data = begin_data;
	addr_reg = PCI_Mem_Base + {start_addr[31:2],2'b00};
	$display ( "CPU2PCITARGET_MEMRD_BURST : Start_Addr = %hh	Legth = %d", addr_reg, length);
	$fdisplay ( pci_outfile_vec, "CPU2PCITARGET_MEMRD_BURST : Start_Addr = %hh	Legth = %d", addr_reg, length);
	for (i=0;i<length[7:2];i=i+1) begin
		`pci_cpu_readbk4w(addr_reg, rdata0, rdata1, rdata2, rdata3);

		$display ("BURST RD_DATA [%hH]: %hh", addr_reg + 0 , rdata0);
		$display ("BURST RD_DATA [%hH]: %hh", addr_reg + 4 , rdata1);
		$display ("BURST RD_DATA [%hH]: %hh", addr_reg + 8 , rdata2);
		$display ("BURST RD_DATA [%hH]: %hh", addr_reg + 12, rdata3);
		$fdisplay (pci_outfile_vec, "BURST RD_DATA [%hH]: %hh", addr_reg + 0 , rdata0);
		$fdisplay (pci_outfile_vec, "BURST RD_DATA [%hH]: %hh", addr_reg + 4 , rdata1);
		$fdisplay (pci_outfile_vec, "BURST RD_DATA [%hH]: %hh", addr_reg + 8 , rdata2);
		$fdisplay (pci_outfile_vec, "BURST RD_DATA [%hH]: %hh", addr_reg + 12, rdata3);
		if( (rdata0 !== start_data )	||
		   	(rdata1 !== start_data + 1)	||
		   	(rdata2 !== start_data + 2)	||
			(rdata3 !== start_data + 3))	begin
			$display ("Error_T2_rsim : BURST RD_DATA error!");
			$fdisplay(pci_outfile_vec,"Error_T2_rsim : BURST RD_DATA error!");
			end
		start_data = start_data + 4;
		addr_reg = addr_reg + 32'h0000_0010;

	end

	data_num = i*4;

	if (length[1]==1'b1) begin
		`pci_cpu_readbk2w(addr_reg, rdata0, rdata1);

		$display ("BURST RD_DATA [%hH]: %hh", addr_reg + 0, rdata0);
		$display ("BURST RD_DATA [%hH]: %hh", addr_reg + 4, rdata1);
		$fdisplay (pci_outfile_vec, "BURST RD_DATA [%hH]: %hh", addr_reg + 0, rdata0);
		$fdisplay (pci_outfile_vec, "BURST RD_DATA [%hH]: %hh", addr_reg + 4, rdata1);
		if( (rdata0 !== start_data )	||
		   	(rdata1 !== start_data + 1)	)	begin
			$display ("Error_T2_rsim : BURST RD_DATA error!");
			$fdisplay(pci_outfile_vec,"Error_T2_rsim : BURST RD_DATA error!");
			end
		start_data = start_data + 2;
		addr_reg = addr_reg + 32'h0000_0008;
	end

	data_num = data_num + 2*length[1];

	if (length[0]==1'b1) begin
		`pci_cpu_readbk1w(addr_reg, 4'b1111, rdata0);

		$display ("BURST RD_DATA [%hH]: %hh", addr_reg + 0, rdata0);
		$fdisplay (pci_outfile_vec, "BURST RD_DATA [%hH]: %hh", addr_reg + 0, rdata0);
		if (rdata0 !== start_data )	begin
			$display ("Error_T2_rsim : BURST RD_DATA error!");
			$fdisplay(pci_outfile_vec,"Error_T2_rsim : BURST RD_DATA error!");
			end
		start_data = start_data + 1;
		addr_reg = addr_reg + 32'h0000_0004;
	end

	data_num = data_num + length[0];

end
endtask

//================= PCI interrupt generation task =========================
task	pci_int_active;
input	int_index;
integer	int_index;
begin
	$display("\tActive General Purpose Interrupt No. %d\n",int_index);
	p_target.set_int_level(int_index,0);
end
endtask

//================= PCI interrupt process task =========================
task pci_inta_processor;
begin
`ifdef NO_CPU
	$display ("\tThe PCI Interrupt A is processing, Please Wait!!!");
	int_cpu_write1w	(32'h1000_6300, 4'b0001, 32'h0000_00ff); //Clear the interrupt
	int_cpu_read1w	(32'h1000_6300, 4'b0001, 32'h0000_00ff); //Wait and Confirm clear
	$display ("\tThe PCI Interrupt A has been resolved!!!");
`endif
end
endtask


//================== Rom(Flash) operation tasks =========================
task cpu2flash_read1w;
input	[31:0]	addr;
input	[3:0]	byte;
input	[31:0]	exp_data;
begin
`ifdef NO_CPU
	`rom_cpu_read1w(addr,byte,exp_data);
`endif
end
endtask

task cpu2flash_read2w;
input	[31:0]	addr;
input	[31:0]	exp_data0;
input	[31:0]	exp_data1;
begin
`ifdef NO_CPU
	`rom_cpu_read2w(addr,exp_data0,exp_data1);
`endif
end
endtask

task cpu2flash_read4w;
input	[31:0]	addr;
input	[31:0]	exp_data0;
input	[31:0]	exp_data1;
input	[31:0]	exp_data2;
input	[31:0]	exp_data3;
begin
`ifdef NO_CPU
	`rom_cpu_read4w(addr,exp_data0,exp_data1,exp_data2,exp_data3);
`endif
end
endtask

task cpu2flash_readbk1w;
input	[31:0]	addr;
input	[3:0]	byte;
inout	[31:0]	tmp_data;
begin
`ifdef NO_CPU
	`rom_cpu_readbk1w(addr,byte,tmp_data);
`endif
end
endtask

task cpu2flash_readbk2w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1;
begin
`ifdef NO_CPU
	`rom_cpu_readbk2w(addr,tmp_data0,tmp_data1);
`endif
end
endtask

task cpu2flash_readbk4w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1,tmp_data2,tmp_data3;
begin
`ifdef NO_CPU
	`rom_cpu_readbk4w(addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
`endif
end
endtask

task cpu2flash_write1w;
input	[31:0]	addr;
input	[3:0]	byte;
input	[31:0] 	data;
begin
`ifdef NO_CPU
	`rom_cpu_write1w(addr,byte,data);
`endif
end
endtask

task cpu2flash_write2w;
input	[31:0]	addr;
input	[31:0] 	data0,data1;
begin
`ifdef NO_CPU
	`rom_cpu_write2w(addr,data0,data1);
`endif
end
endtask

task cpu2flash_write4w;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
begin
`ifdef NO_CPU
	`rom_cpu_write4w(addr,data0,data1,data2,data3);
`endif
end
endtask

task flash_access_enable;
reg	[31:0] tmp_data;
begin
`ifdef NO_CPU
	$display("Enable the FLASH memory access");
	`rom_cpu_readbk1w(CPU_IOBase + 32'h90, 4'b0001, tmp_data);
	`rom_cpu_write1w(CPU_IOBase + 32'h90, 4'b0001, {tmp_data[31:2],1'b0,tmp_data[0]});
`endif
end
endtask

task flash_access_disable;
reg	[31:0] tmp_data;
begin
`ifdef NO_CPU
	$display("Disable the FLASH memory access");
	`rom_cpu_readbk1w(CPU_IOBase + 32'h90, 4'b0001, tmp_data);
	`rom_cpu_write1w(CPU_IOBase + 32'h90, 4'b0001, {tmp_data[31:2],1'b1,tmp_data[0]});
`endif
end
endtask

task flash_write_enable;
begin
`ifdef NO_CPU
	$display("Enable the FLASH memory access and writable");
	`rom_cpu_write1w(CPU_IOBase + 32'h90, 4'b0001, 32'h01);//include access enable
`endif
end
endtask

task flash_write_disable;
reg	[31:0] tmp_data;
begin
`ifdef NO_CPU
	$display("Disable the FLASH memory writable");
	`rom_cpu_readbk1w(CPU_IOBase + 32'h90, 4'b0001, tmp_data);
	`rom_cpu_write1w(CPU_IOBase + 32'h90, 4'b0001, {tmp_data[31:1],1'b0});//only read
`endif
end
endtask

task	FLASH_Speed_Select;
input	[7:0] speed;
begin
	$display ("*** Select Flash Memory Speed ***");
	`rom_cpu_write1w(CPU_IOBase + 32'h90, 4'b0100, {8'h0,speed[7:0],16'h0});
end
endtask
//====================== low task format=================================
//`define	pci_cpu_write1w		pro04_cpu_write1w
//`define	pci_cpu_write2w    	pro04_cpu_write2w
//`define	pci_cpu_write4w    	pro04_cpu_write4w
//`define	pci_cpu_write8w    	pro04_cpu_write8w
//`define	pci_cpu_readbk1w   	pro04_cpu_readbk1w
//`define	pci_cpu_readbk2w   	pro04_cpu_readbk2w
//`define	pci_cpu_readbk4w   	pro04_cpu_readbk4w
//`define	pci_cpu_readbk8w   	pro04_cpu_readbk8w
//`define	pci_cpu_read1w     	pro04_cpu_read1w
//`define	pci_cpu_read2w     	pro04_cpu_read2w
//`define	pci_cpu_read4w     	pro04_cpu_read4w
//`define	pci_cpu_read8w     	pro04_cpu_read8w

//=============== the basic task for pro04 processer ========================
//task	pro04_cpu_write1w;
//input	[31:0]	addr;
//input	[3:0]	byte;
//input	[31:0] 	data;
//
//
//task	pro04_cpu_write2w;
//input	[31:0]	addr;
//input	[31:0] 	data0,data1;
//
//task	pro04_cpu_write4w;
//input	[31:0]	addr;
//input	[31:0] 	data0,data1,data2,data3;
//
//task	pro04_cpu_write8w;
//input	[31:0]	addr;
//input	[31:0] 	data0,data1,data2,data3;
//input	[31:0] 	data4,data5,data6,data7;
//
//task	pro04_cpu_readbk1w;
//input	[31:0]	addr;
//input	[3:0]	byte;
//inout	[31:0]	tmp_data;
//
//task	pro04_cpu_readbk2w;
//
//task	pro04_cpu_readbk4w;
//input	[31:0]	addr;
//inout	[31:0]	tmp_data0,tmp_data1,tmp_data2,tmp_data3;
//
//task	pro04_cpu_readbk8w;
//input	[31:0]	addr;
//inout	[31:0]	tmp_data0,tmp_data1,tmp_data2,tmp_data3;
//inout	[31:0]	tmp_data4,tmp_data5,tmp_data6,tmp_data7;
//
//task	pro04_cpu_read1w;
//input	[31:0]	addr;
//input	[3:0]	byte;
//input	[31:0]	exp_data;
//
//task	pro04_cpu_read2w;
//input	[31:0]	addr;
//input	[31:0]	exp_data0;
//input	[31:0]	exp_data1;
//
//task	pro04_cpu_read4w;
//input	[31:0]	addr;
//input	[31:0]	exp_data0,
//
//task	pro04_cpu_read8w;
//input	[31:0]	addr;
//input	[31:0]	exp_data0,
//		exp_data1,
//		exp_data2,
//		exp_data3,
//		exp_data4,
//		exp_data5,
//		exp_data6,
//		exp_data7;

task	SYS_IO_Write;
input [7:0] index;
input [3:0] be;
input [31:0] data;
begin
	`nb_cpu_write1w(CPU_IOBase + index, be, data);
end
endtask

task	SYS_IO_Read;
input [7:0] index;
input [3:0] be;
input [31:0] data;
begin
	`nb_cpu_read1w(CPU_IOBase + index, be, data);
end
endtask

task	SYS_IO_Readbk;
input [7:0] index;
input [3:0] be;
inout [31:0] data;
reg   [31:0] data;
begin
    `nb_cpu_readbk1w(CPU_IOBase + index, be, data);
end
endtask

//========================================================PCI interface
task Memory_Burst_Test_Read;          //98-06-12, Yi
input  [31:0] addr;
input  [31:0] startdata;
input         length;
integer length;
reg [31:0] data_exp;
reg [43:0] data_actual;
integer ki;

begin
    $display("PCI memory burst read:\tStart ADDR =%h,DATA =%h, Len =%h",addr,startdata,length);
    p_master.PCI_memrd_burst(addr,length);
    data_exp = startdata;
    for( ki = 0; ki < length; ki = ki + 1)	begin
	#1
	data_actual[43:0] = p_master.readBuffer[ki];
	$display ("PCI Burst Read Data[%d]:%hh", ki[7:0], data_actual[31:0]);		// add by runner
		if(data_actual[31:0] !== data_exp[31:0]) begin
	    	$display("Error_T2_rsim:Memory Burst Read\t%0t ADDR =%h, Exp =%h, Actua =%h", $realtime,
//			{(addr[31:2]+ki),2'h0},data_exp[31:0], data_actual[31:0]);
			({addr[31:2],2'h0}+ki*4),data_exp[31:0], data_actual[31:0]);//bug fixed by Figo 02-08-17
	    	$fdisplay(f_name,"Error_T2_rsim:Memory Burst Read\t%0t ADDR =%h, Exp =%h, Actua =%h", $realtime,
//			{(addr[31:2]+ki),2'h0},data_exp[31:0], data_actual[31:0]);
			({addr[31:2],2'h0}+ki*4),data_exp[31:0], data_actual[31:0]);//bug fixed by Figo 02-08-17
		end
	data_exp = {data_exp[30:0], data_exp[31]};
    end
    $display ("PCI Burst Read End\n\n");
end
endtask

task Mem_Burst_Read;          //98-06-12, Yi
input  [31:0] addr;
input         length;
integer length;
reg [43:0] data_actual;
integer ki;

begin
    $display("PCI memory burst read begin...");
    p_master.PCI_memrd_burst(addr,length);
    $display("PCI memory burst read finish");
    for( ki = 0; ki < length; ki = ki + 1)
	begin
          data_actual[43:0] = p_master.readBuffer[ki];
          $fdisplay(p_master.pb_name,"%h",data_actual[19:0]);
	end
end
endtask

task Memory_Burst_Test_Write;   //98-06-12, Yi
input  [31:0] addr;			//99-05-10 Snow
input  [31:0] startdata;
input         length;
integer length;
integer ki;
reg	[31:0]	test_data;

begin
     test_data = startdata;
     for( ki = 0; ki < length; ki = ki + 1)	begin
		p_master.writeBuffer[ki] = {p_master.cbe_dw[addr[1:0]],p_master.irdyCntP,test_data};
		$display ("PCI Burst Write Data[%d]: %h", ki[7:0], test_data);		// Add by Runner
		test_data = {test_data[30:0], test_data[31]};
     end
//     $display("PCI memory burst write begin...");
     p_master.PCI_memwr_burst(addr,length);
     $display("PCI Burst Write End\n\n");
end
endtask

task Mem_Burst_Write;   //98-06-12, Yi
input  [31:0] addr;
input         length;
integer length;
integer ki;

begin
     for( ki = 0; ki < length; ki = ki + 1)
       p_master.writeBuffer[ki] = {p_master.cbe_dw[addr[1:0]],p_master.irdyCntP,
                                   p_master.burst_buffer[p_master.burst_adr + ki]};
//     $display("PCI memory burst write begin...");
     p_master.PCI_memwr_burst(addr,length);
//     $display("PCI memory burst write finish");
     p_master.burst_adr = (p_master.burst_adr + length)%1792;
end
endtask

task Mem_Read_Multi_Test;
input  [31:0] addr;
input  [31:0] testdata;
input         length;

integer length;
reg [31:0] data_exp;
reg [43:0] data_actual;
integer ki;

begin
    $display("PCI memory read multiple begin... Address = %h", addr);
    p_master.PCI_memrd_mul(addr,0,length);
    $display("PCI memory read multiple finish");
    data_exp = testdata;
    for( ki = 0; ki < length; ki = ki + 1)	begin
	data_actual[43:0] = p_master.readBuffer[ki];
	if(data_actual[31:0] !== data_exp[31:0]) begin
	    $display("Error_T2_rsim:Memory Read Multi\t%0t ADDR =%h, Exp =%h, Actua =%h", $realtime,
//		{(addr[31:2]+ki),2'h0},data_exp[31:0], data_actual[31:0]);
		({addr[31:2],2'h0}+ki*4),data_exp[31:0], data_actual[31:0]);//bug fixed by Figo 02-08-17
	    $fdisplay(f_name,"Error_T2_rsim:Memory Read Multi\t%0t ADDR =%h, Exp =%h, Actua =%h", $realtime,
//		{(addr[31:2]+ki),2'h0},data_exp[31:0], data_actual[31:0]);
		({addr[31:2],2'h0}+ki*4),data_exp[31:0], data_actual[31:0]);//bug fixed by Figo 02-08-17
	end
	data_exp = {data_exp[30:0], data_exp[31]};
    end
end
endtask

task Mem_Read_Line_Test;
input  [31:0] addr;
input  [31:0] testdata;
input         length;

integer length;
reg [31:0] data_exp;
reg [43:0] data_actual;
integer ki;

begin
    $display("PCI memory read line begin...Address = %h", addr);
    p_master.PCI_memrd_line(addr,0,length);
    $display("PCI memory read line finish");
    data_exp = testdata;
    for( ki = 0; ki < length; ki = ki + 1)	begin
	data_actual[43:0] = p_master.readBuffer[ki];
	if(data_actual[31:0] !== data_exp[31:0]) begin
	    $display("Error_T2_rsim:Memory Read Line\t%0t ADDR =%h, Exp =%h, Actua =%h", $realtime,
//		{(addr[31:2]+ki),2'h0},data_exp[31:0], data_actual[31:0]);
		({addr[31:2],2'h0}+ki*4),data_exp[31:0], data_actual[31:0]);//bug fixed by Figo 02-08-17
	    $fdisplay(f_name,"Error_T2_rsim:Memory Read Line\t%0t ADDR =%h, Exp =%h, Actua =%h", $realtime,
//		{(addr[31:2]+ki),2'h0},data_exp[31:0], data_actual[31:0]);
		({addr[31:2],2'h0}+ki*4),data_exp[31:0], data_actual[31:0]);//bug fixed by Figo 02-08-17
	end
	data_exp = {data_exp[30:0], data_exp[31]};
    end
end
endtask

task Mem_Write_Inv_Test;
input  [31:0] addr;
input  [31:0] startdata;
input         length;

integer length;
reg [31:0] test_data;
integer ki;

begin
    test_data = startdata;
    for( ki = 0; ki < length; ki = ki + 1)	begin
	p_master.writeBuffer[ki] = {p_master.cbe_dw[addr[1:0]],p_master.irdyCntP,test_data};
	test_data = {test_data[30:0], test_data[31]};
    end

    $display("PCI memory write invalidate begin...Address = %h", addr);
    p_master.PCI_memwr_inv(addr,0,length);
    $display("PCI memory write invalidate finish");

end
endtask
/*

//Display Engine Device Behaviour Model Initial
task	DISP_BH_SDRAM_load_memory;
input	mem_base;
input	mem_size;
integer	mem_size, mem_base;
begin
	`ifdef	NO_DISPLAY
        $display("\n");
        $display ("@ 32`h%h ... Display Engine Behaviour Model Init", mem_base);
        $readmemh({`home_dir, "/rsim/stimu.t/north/disp_device_bh.txt"}, mem_array, 0, mem_size-1);
		SDRAM_load_memory(mem_base, mem_size);
        $display("\n");
	`else
	`endif
	end
endtask
*/
/*
task	DISP_BH_SDRAM_dump_memory;
input	mem_base;
input	mem_size;
input	file_handle;
integer	mem_size, mem_base, file_handle;
begin
	for(i=0;i<mem_size;i=i+1)begin
		mem_array[i]	=	32'hxxxx_xxxx;
	end
	`ifdef	NO_DISPLAY
		SDRAM_dump_memory(mem_base,mem_size,file_handle);
	`else
	`endif
end
endtask
*/

