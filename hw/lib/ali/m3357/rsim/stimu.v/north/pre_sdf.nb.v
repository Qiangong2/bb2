
/*===========================================================
file:			pre_sdf.nb.v
description:	add your ip sdf file here for prelayout gsim
============================================================*/

initial begin
//Syntax
//$sdf_annotate( "sdf_file", module_instance, "config_file", "log_file",
//				 "mtm_spec", "scale_factors", "scale_type" );
//Sample
//$sdf_annotate	("../../../gsrc/metal_fix/chip.sdf", CHIP,	"../../../gsrc/sdf.config", "chip_sdf.log",
//				 "MAXIMUM", "1.0:1.0:1.0", 	"FROM_MAXIMUM");


`ifdef	INC_NB //add NB sdf file annotation here
//$sdf_annotate	("/beta/design/m3357/gsrc/prelayout/north/pbiu/p_biu.sdf",
//			top.CHIP.CORE.T2_CHIPSET.P_BIU,
//			,
//			"pbiu_sdf.log",
//			"MAXIMUM",
//			,
//			"FROM_MTM");
$sdf_annotate	("/beta/design/m3357/gsrc/prelayout/north/nb_cpu_biu/nb_cpu_biu.sdf",
			top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU,
			,
			"nb_cpu_biu_anno.log",
			"MAXIMUM",
			,
			"FROM_MTM");
//$sdf_annotate	("/beta/design/m3357/gsrc/prelayout/north/mem/mem_intf_post_ins.sdf",
//			top.CHIP.CORE.T2_CHIPSET.MEM_INTF,
//			,
//			"mem_intf_anno.log",
//			"MAXIMUM",
//			,
//			"FROM_MTM");

`endif
end
