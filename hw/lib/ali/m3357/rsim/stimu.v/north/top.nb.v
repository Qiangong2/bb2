
//==============================================================================
// Description: Top module for NB of m6304 RTL simulation
//              include the chip of m6304, pci behavior model, monitor, sdram
//              behavior model, flash rom behavior model....
// Author:      Yuky
// History:     2002-06-08      initial version, copy from m6303 and modify it for m6304
//              2002-06-17      del the pci master2 and pci ports of chip and add the IDE
//                              ports to the chip
//				2002-07-11		move all the system define and the chip module to top.v
//								make this file only include the nb behavior model and
//								the IBUS protocol monitor
//				2002-11-06		add monitors for mem_intf debug
//				2002-11-08		add monitors for sdram command design requirement debug
//				2003-07-01		modified pci port name from p_xx to pci_xx
//==============================================================================
`ifdef  POST_GSIM
wire	int_bh_rst_   = `path_chipset.RST_   ;
wire	int_bh_clk    = `path_chipset.MEM_CLK;
`else
wire	int_bh_rst_   = `path_chipset.rst_   ;
wire	int_bh_clk    = `path_chipset.mem_clk;
`endif

wire [31:0] tmp_int;
int_bh int_bh(
	.ext_int_out({tmp_int[31:10],ext_gpio[9:3],tmp_int[2:0]}),	// output interrupt, default 32'hz
	.clk(int_bh_clk		),
	.rst_(int_bh_rst_	)
);

monitor p_monitor (
//      .req_           (p_req_         ),
//      .gnt_           (p_gnt_         ),
//      .idsel          (p_idsel        ),
//      .idsel          (p_ad[31:16]    ),
        .ad             (pci_ad           ),
        .cbe_           (pci_cbe_         ),
        .frame_         (moniter_frame_ ),
        .irdy_          (pci_irdy_        ),
        .devsel_        (pci_devsel_      ),
        .trdy_          (pci_trdy_        ),
        .stop_          (pci_stop_        ),
        .lock_          (p_lock_        ),
        .par            (pci_par          ),
        .perr_          (p_perr_        ),
        .serr_          (p_serr_        ),
        .clk            (monitor_clk    ),
        .rst_           (ext_rst_		)	//2002-08-14,rom_control_rst_)//if the rom accessing,pci will be reset
        );

master p_master (       // PCI 33 MHz device
        .gnt_           ( pci_gnta_       ),
        .req_           ( pci_req_        ),
        .ad             ( pci_ad          ),
        .cbe_           ( pci_cbe_        ),
        .par            ( pci_par         ),
        .frame_         ( pci_frame_      ),
        .irdy_          ( pci_irdy_       ),
        .trdy_          ( pci_trdy_       ),
        .stop_          ( pci_stop_       ),
        .lock_          ( p_lock_       ),
        .perr_          ( p_perr_       ),
        .serr_          ( p_serr_       ),
        .devsel_        ( pci_devsel_     ),
        .own_bus        ( bus_0         ),
        .enable         ( 1'b1          ),
        .back2back      ( b2b_0         ),
        .clock          ( p_master_clk  ),
        .rst_           (ext_rst_		)	//2002-08-14,rom_control_rst_)//if the rom accessing,pci will be reset
        );
/*
master p_master2 (      // PCI 33 MHz device
        .gnt_           (p_mas2_gnt_    ),
        .req_           (p_mas2_req_    ),
        .ad             (p_ad           ),
        .cbe_           (p_cbe_         ),
        .par            (p_par          ),
        .frame_         (p_frame_       ),
        .irdy_          (p_irdy_        ),
        .trdy_          (p_trdy_        ),
        .stop_          (p_stop_        ),
        .lock_          (p_lock_        ),
        .perr_          (p_perr_        ),
        .serr_          (p_serr_        ),
        .devsel_        (p_devsel_      ),
        .own_bus        (bus_0          ),
        .enable         (1'b1           ),
        .back2back      (b2b_0          ),
        .clock          (p_master_clk   ),
        .rst_           (ext_rst_       )
        );
*/
target p_target (       // PCI 33 MHz device
        .ad             (pci_ad           ),
        .cbe_           (pci_cbe_         ),
        .par            (pci_par          ),
        .frame_         (pci_frame_       ),
        .irdy_          (pci_irdy_        ),
        .trdy_          (pci_trdy_        ),
        .stop_          (pci_stop_        ),
        .lock_          (p_lock_        ),
        .perr_          (p_perr_        ),
        .serr_          (p_serr_        ),
        .devsel_        (pci_devsel_      ),
        //.idsel        (p_ad[24]       ),      // Device 8
        .idsel          (pci_ad[20]       ),      // Device 4 --- Runner
        .pci_int_       (ext_int        ),
        .enable         (1'b1           ),
        .clock          (p_target_clk   ),
        .rst_           (ext_rst_		)	//2002-08-14,rom_control_rst_)//if the rom accessing,pci will be reset
        );
//==================================================================================

//================== invoke the SDRAM behavior model(DIMM) =========================
`ifdef	CS_11BIT
	wire	[11:0]	ram_addr_tmp	=	{1'b0,ram_addr[10:0]};
`else
	wire	[11:0]	ram_addr_tmp	=	ram_addr;
`endif

dimm_bh dimm_bh (
        .dq             (ext_ram_dq     ),
        .addr           (ram_addr_tmp   ),
        .ba             (ram_ba         ),      // Eric 04/03/00
        .cs_            (ram_cs_ 		),
//        .d1_cke         (1'b1           ),
//        .d2_cke         (1'b1           ),
        .d1_cke         (ext_rst_       ),      // yuky 2001-10-12
        .d2_cke         (ext_rst_       ),      // yuky 2001-10-12
        .dqm            (ram_dqm        ),
        .ras_           (ram_ras_       ),
        .cas_           (ram_cas_       ),
        .we_            (ram_we_        ),
        .sd_clk0        (sd_clk         ),
        .sd_clk1        (sd_clk         )
        );

//dimm performance monitor
performance performance(
           //SDR SDRAM memory interface:
	.dq		   		(ext_ram_dq			),
	.addr			(ram_addr			),
	.ba				(ram_ba  			),
	.cs_			(ram_cs_ 			),
	.d1_cke			(ext_rst_   		),
	.d2_cke			(ext_rst_   		),
	.dqm			(ram_dqm    		),
	.ras_			(ram_ras_   		),
	.cas_			(ram_cas_   		),
	.we_			(ram_we_    		),
	.sd_clk0 		(sd_clk     		),
	.sd_clk1 		(sd_clk     		)
					);

// BOOT_ROM
boot_rom boot_rom (
//      .eeprom_data    (ram_dq[7:0]    ),
//      .eeprom_data    (atadd[7:0]    	),
		.eeprom_data    (ext_eeprom_data   	),
//        .eeprom_addr    ({3'b0, ext_eeprom_addr[20:0]}    ),
        .eeprom_addr    (ext_eeprom_addr[20:0]    ),//JFR030719
        .eeprom_oe_     (ext_eeprom_oe_     ),
        .eeprom_cs_     (ext_eeprom_ce_     ),
        .eeprom_we_     (ext_eeprom_we_     )
        );
//==============================================================================


//==============================protocol monitor here =========================

//invoke the cpu internal bus protocol monitor
`ifdef  POST_GSIM
wire 		ix_ready 		= `path_chipset.X_READY;
wire [31:0] ix_data_in 		= `path_chipset.X_DATA_IN;
wire        ix_bus_error	= `path_chipset.X_BUS_ERROR;
wire        ix_ack			= `path_chipset.X_ACK;
wire		ix_err_int 		= `path_chipset.X_ERR_INT;
wire		ix_ext_int 		= `path_chipset.X_EXT_INT;

wire        iu_request		= `path_chipset.U_REQUEST;
wire        iu_write		= `path_chipset.U_WRITE;
wire [1:0]  iu_wordsize		= `path_chipset.U_WORDSIZE;
wire [31:0] iu_startpaddr	= `path_chipset.U_STARTPADDR;
wire [31:0] iu_data_to_send	= `path_chipset.U_DATA_TO_SEND;
wire [3:0]  iu_bytemask		= `path_chipset.U_BYTEMASK;
wire        ig_reset_		= `path.CORE_WARMRST_;
wire		icpu_clk		= `path_chipset.CPU_CLK;

`else //rsim and pre-layout gsim
wire 		ix_ready 		= `path_chipset.x_ready;
wire [31:0] ix_data_in 		= `path_chipset.x_data_in;
wire        ix_bus_error	= `path_chipset.x_bus_error;
wire        ix_ack			= `path_chipset.x_ack;
wire		ix_err_int 		= `path_chipset.x_err_int;
wire		ix_ext_int 		= `path_chipset.x_ext_int;

wire        iu_request		= `path_chipset.u_request;
wire        iu_write		= `path_chipset.u_write;
wire [1:0]  iu_wordsize		= `path_chipset.u_wordsize;
wire [31:0] iu_startpaddr	= `path_chipset.u_startpaddr;
wire [31:0] iu_data_to_send	= `path_chipset.u_data_to_send;
wire [3:0]  iu_bytemask		= `path_chipset.u_bytemask;
wire        ig_reset_		= `path.core_warmrst_;
wire		icpu_clk		= `path_chipset.cpu_clk;
`endif

`ifdef	POST_GSIM
`else
t2risc_intf_monitor t2risc_intf_monitor(
//input from t2chipset signals
	.x_ack			(ix_ack		),		// last data tranfer
	.x_ready		(ix_ready	),		// In read, means data is on the bus x_data_in
										// In write, means data on u_data_to_send be accepted
										// This signal should assert one sclk for every word
	.x_bus_error	(ix_bus_error),		// Asserted one clock when bus data error
	.x_data_in		(ix_data_in	),		// The recieved data
	.x_err_int		(ix_err_int	),		// The cpu bus error interrupt
	.x_ext_int		(ix_ext_int	),		// The external interrupt

//input from t2risc signals
	.u_request		(iu_request	),		// Request reading or writing,asserted until the x_ack
										// for last word being accepted
	.u_write		(iu_write	),		// 1: write, 0: read
	.u_wordsize		(iu_wordsize),		// 0 -> 1words, 1 -> 2words,  2 -> 4words,  3 -> 8words
	.u_startpaddr	(iu_startpaddr),	// Request address
	.u_data_to_send	(iu_data_to_send),	// Write data
	.u_bytemask		(iu_bytemask),		// four bits bytemask signals for u_data_to_send,
										// 1 means valid, 0 means invalid

//global signals
	.g_sclk			(icpu_clk	),	        // memory interface clock, it is strickly half of
	    									// pipeline clk
	.g_reset_		(ig_reset_	)			// global reset
	);
`endif

//invoke the sdram internal bus protocol monitor
//##############################################
//`ifdef  POST_GSIM
//##############################################
`define	mem_intf_path	`path_chipset.MEM_INTF
wire		imem_rst_   = `mem_intf_path.RST_   ;
wire		imem_clk    = `mem_intf_path.MEM_CLK;

//===cpu2mem(read and write)
wire		icpu_ram_req    = `mem_intf_path.CPU_RAM_REQ	 ;
wire		icpu_ram_rw     = `mem_intf_path.CPU_RAM_RW      ;
wire [29:2]	icpu_ram_addr   = `mem_intf_path.CPU_RAM_ADDR    ;
wire [2:0]	icpu_ram_rbl    = `mem_intf_path.CPU_RAM_RBL     ;
wire		icpu_ram_wlast  = `mem_intf_path.CPU_RAM_WLAST   ;
wire [3:0]	icpu_ram_wbe    = `mem_intf_path.CPU_RAM_WBE     ;
wire [31:0]	icpu_ram_wdata  = `mem_intf_path.CPU_RAM_WDATA   ;

wire		icpu_ram_ack    = `mem_intf_path.CPU_RAM_ACK	 ;
wire		icpu_ram_err    = `mem_intf_path.CPU_RAM_ERR     ;//no use now
wire		icpu_ram_rrdy   = `mem_intf_path.CPU_RAM_RRDY    ;
wire		icpu_ram_rlast  = `mem_intf_path.CPU_RAM_RLAST   ;
wire [31:0]	icpu_ram_rdata  = `mem_intf_path.CPU_RAM_RDATA   ;
wire		icpu_ram_wrdy   = `mem_intf_path.CPU_RAM_WRDY    ;

/*
//===disp2mem(only read)
wire		idisp_ram_req   = `mem_intf_path.DISP_RAM_REQ   ;
wire [29:2]	idisp_ram_addr  = `mem_intf_path.DISP_RAM_ADDR  ;
wire [2:0]	idisp_ram_rbl   = `mem_intf_path.DISP_RAM_RBL   ;
wire		idisp_ram_ack   = `mem_intf_path.DISP_RAM_ACK   ;
wire		idisp_ram_err   = `mem_intf_path.DISP_RAM_ERR   ;//no use now
wire		idisp_ram_rrdy  = `mem_intf_path.DISP_RAM_RRDY  ;
wire		idisp_ram_rlast = `mem_intf_path.DISP_RAM_RLAST ;
wire [31:0]	idisp_ram_rdata = `mem_intf_path.DISP_RAM_RDATA ;
*/

//===pci2mem(read and write independent)
//read
wire		ipci_ram_rreq   = `mem_intf_path.PCI_RAM_RREQ   ;
wire [29:2]	ipci_ram_raddr  = `mem_intf_path.PCI_RAM_RADDR  ;
wire [2:0]	ipci_ram_rbl    = `mem_intf_path.PCI_RAM_RBL    ;
wire		ipci_ram_rack   = `mem_intf_path.PCI_RAM_RACK   ;
wire		ipci_ram_rerr   = `mem_intf_path.PCI_RAM_RERR   ;//no use now
wire		ipci_ram_rrdy   = `mem_intf_path.PCI_RAM_RRDY   ;
wire		ipci_ram_rlast  = `mem_intf_path.PCI_RAM_RLAST  ;
wire [31:0]	ipci_ram_rdata  = `mem_intf_path.PCI_RAM_RDATA  ;
//write
wire		ipci_ram_wreq 	= `mem_intf_path.PCI_RAM_WREQ 	;
wire [29:2]	ipci_ram_waddr  = `mem_intf_path.PCI_RAM_WADDR  ;
wire		ipci_ram_wlast  = `mem_intf_path.PCI_RAM_WLAST  ;
wire [3:0]	ipci_ram_wbe    = `mem_intf_path.PCI_RAM_WBE    ;
wire [31:0]	ipci_ram_wdata  = `mem_intf_path.PCI_RAM_WDATA  ;
wire		ipci_ram_wack   = `mem_intf_path.PCI_RAM_WACK   ;
wire		ipci_ram_werr   = `mem_intf_path.PCI_RAM_WERR   ;//no use now
wire		ipci_ram_wrdy   = `mem_intf_path.PCI_RAM_WRDY   ;
/*
//===gpu2mem(read and write)
wire		igpu_ram_req   	= `mem_intf_path.GPU_RAM_REQ    ;
wire		igpu_ram_rw    	= `mem_intf_path.GPU_RAM_RW     ;
wire [29:2]	igpu_ram_addr  	= `mem_intf_path.GPU_RAM_ADDR   ;
wire [2:0]	igpu_ram_rbl   	= `mem_intf_path.GPU_RAM_RBL    ;
wire		igpu_ram_wlast 	= `mem_intf_path.GPU_RAM_WLAST  ;
wire [3:0]	igpu_ram_wbe   	= `mem_intf_path.GPU_RAM_WBE    ;
wire [31:0]	igpu_ram_wdata 	= `mem_intf_path.GPU_RAM_WDATA  ;
wire		igpu_ram_ack   	= `mem_intf_path.GPU_RAM_ACK    ;
wire		igpu_ram_err   	= `mem_intf_path.GPU_RAM_ERR    ;//no use now
wire		igpu_ram_rrdy  	= `mem_intf_path.GPU_RAM_RRDY   ;
wire		igpu_ram_rlast 	= `mem_intf_path.GPU_RAM_RLAST  ;
wire [31:0]	igpu_ram_rdata 	= `mem_intf_path.GPU_RAM_RDATA  ;
wire		igpu_ram_wrdy  	= `mem_intf_path.GPU_RAM_WRDY   ;
*/
//===video2mem(read and write)
wire 		ivideo_ram_req  = `mem_intf_path.VIDEO_RAM_REQ   ;
wire		ivideo_ram_rw   = `mem_intf_path.VIDEO_RAM_RW    ;
wire [29:2]	ivideo_ram_addr = `mem_intf_path.VIDEO_RAM_ADDR  ;
wire [2:0]	ivideo_ram_rbl  = `mem_intf_path.VIDEO_RAM_RBL   ;
wire		ivideo_ram_wlast= `mem_intf_path.VIDEO_RAM_WLAST ;
wire [3:0]	ivideo_ram_wbe  = `mem_intf_path.VIDEO_RAM_WBE   ;
wire [31:0]	ivideo_ram_wdata= `mem_intf_path.VIDEO_RAM_WDATA ;
wire		ivideo_ram_ack  = `mem_intf_path.VIDEO_RAM_ACK   ;
wire		ivideo_ram_err  = `mem_intf_path.VIDEO_RAM_ERR   ;//no use now
wire		ivideo_ram_rrdy = `mem_intf_path.VIDEO_RAM_RRDY  ;
wire		ivideo_ram_rlast= `mem_intf_path.VIDEO_RAM_RLAST ;
wire [31:0]	ivideo_ram_rdata= `mem_intf_path.VIDEO_RAM_RDATA ;
wire		ivideo_ram_wrdy = `mem_intf_path.VIDEO_RAM_WRDY  ;
//VSB
wire 		ivsb_ram_req  = `mem_intf_path.VSB_RAM_REQ   ;
wire		ivsb_ram_rw   = `mem_intf_path.VSB_RAM_RW    ;
wire [29:2]	ivsb_ram_addr = `mem_intf_path.VSB_RAM_ADDR  ;
wire [2:0]	ivsb_ram_rbl  = `mem_intf_path.VSB_RAM_RBL   ;
wire		ivsb_ram_wlast= `mem_intf_path.VSB_RAM_WLAST ;
wire [3:0]	ivsb_ram_wbe  = `mem_intf_path.VSB_RAM_WBE   ;
wire [31:0]	ivsb_ram_wdata= `mem_intf_path.VSB_RAM_WDATA ;
wire		ivsb_ram_ack  = `mem_intf_path.VSB_RAM_ACK   ;
wire		ivsb_ram_err  = `mem_intf_path.VSB_RAM_ERR   ;//no use now
wire		ivsb_ram_rrdy = `mem_intf_path.VSB_RAM_RRDY  ;
wire		ivsb_ram_rlast= `mem_intf_path.VSB_RAM_RLAST ;
wire [31:0]	ivsb_ram_rdata= `mem_intf_path.VSB_RAM_RDATA ;
wire		ivsb_ram_wrdy = `mem_intf_path.VSB_RAM_WRDY  ;

//===dsp2mem(read and write)
wire		idsp_ram_req	= `mem_intf_path.DSP_RAM_REQ   	;
wire		idsp_ram_rw     = `mem_intf_path.DSP_RAM_RW     ;
wire [29:2]	idsp_ram_addr   = `mem_intf_path.DSP_RAM_ADDR   ;
wire [2:0]	idsp_ram_rbl    = `mem_intf_path.DSP_RAM_RBL    ;
wire		idsp_ram_wlast  = `mem_intf_path.DSP_RAM_WLAST  ;
wire [3:0]	idsp_ram_wbe    = `mem_intf_path.DSP_RAM_WBE    ;
wire [31:0]	idsp_ram_wdata  = `mem_intf_path.DSP_RAM_WDATA  ;
wire		idsp_ram_ack    = `mem_intf_path.DSP_RAM_ACK    ;
wire		idsp_ram_err    = `mem_intf_path.DSP_RAM_ERR    ;//no use now
wire		idsp_ram_rrdy   = `mem_intf_path.DSP_RAM_RRDY   ;
wire		idsp_ram_rlast  = `mem_intf_path.DSP_RAM_RLAST  ;
wire [31:0]	idsp_ram_rdata  = `mem_intf_path.DSP_RAM_RDATA  ;
wire		idsp_ram_wrdy   = `mem_intf_path.DSP_RAM_WRDY   ;
/*
//===sb2mem(read and write)
wire		isb_ram_req     = `mem_intf_path.SB_RAM_REQ   	;
wire		isb_ram_rw      = `mem_intf_path.SB_RAM_RW      ;
wire [29:2]	isb_ram_addr    = `mem_intf_path.SB_RAM_ADDR    ;
wire [2:0]	isb_ram_rbl     = `mem_intf_path.SB_RAM_RBL     ;
wire		isb_ram_wlast   = `mem_intf_path.SB_RAM_WLAST   ;
wire [3:0]	isb_ram_wbe     = `mem_intf_path.SB_RAM_WBE     ;
wire [31:0]	isb_ram_wdata   = `mem_intf_path.SB_RAM_WDATA   ;
wire		isb_ram_ack     = `mem_intf_path.SB_RAM_ACK     ;
wire		isb_ram_err     = `mem_intf_path.SB_RAM_ERR     ;//no use now
wire		isb_ram_rrdy    = `mem_intf_path.SB_RAM_RRDY    ;
wire		isb_ram_rlast   = `mem_intf_path.SB_RAM_RLAST   ;
wire [31:0]	isb_ram_rdata   = `mem_intf_path.SB_RAM_RDATA   ;
wire		isb_ram_wrdy    = `mem_intf_path.SB_RAM_WRDY    ;
*/
//===ide2mem(read and write)
wire		iide_ram_req     = `mem_intf_path.IDE_RAM_REQ   	;
wire		iide_ram_rw      = `mem_intf_path.IDE_RAM_RW      ;
wire [29:2]	iide_ram_addr    = `mem_intf_path.IDE_RAM_ADDR    ;
wire [2:0]	iide_ram_rbl     = `mem_intf_path.IDE_RAM_RBL     ;
wire		iide_ram_wlast   = `mem_intf_path.IDE_RAM_WLAST   ;
wire [3:0]	iide_ram_wbe     = `mem_intf_path.IDE_RAM_WBE     ;
wire [31:0]	iide_ram_wdata   = `mem_intf_path.IDE_RAM_WDATA   ;
wire		iide_ram_ack     = `mem_intf_path.IDE_RAM_ACK     ;
wire		iide_ram_err     = `mem_intf_path.IDE_RAM_ERR     ;//no use now
wire		iide_ram_rrdy    = `mem_intf_path.IDE_RAM_RRDY    ;
wire		iide_ram_rlast   = `mem_intf_path.IDE_RAM_RLAST   ;
wire [31:0]	iide_ram_rdata   = `mem_intf_path.IDE_RAM_RDATA   ;
wire		iide_ram_wrdy    = `mem_intf_path.IDE_RAM_WRDY    ;
/*
//	----------------------
//Priority Mode:
wire[1:0]	iarbta_prior_mode	=	`mem_intf_path.SUBA_ARBT_CTRL;
wire[1:0]	iarbtb_prior_mode	=	`mem_intf_path.SUBB_ARBT_CTRL;
wire[1:0]	iarbtm_prior_mode	=	`mem_intf_path.MAIN_ARBT_CTRL;

wire		iauto_ref_req		=	`mem_intf_path.AUTO_REF_TIMER.REFRESH_REQ;
wire		iauto_ref_ack		=	`mem_intf_path.AUTO_REF_TIMER.REFRESH_ACK;

//Arbiters States:
wire[31:0]	isuba_arbt_st		=	`mem_intf_path.SUBA_ARBT_ST;
wire[31:0]	isubb_arbt_st		=	`mem_intf_path.SUBB_ARBT_ST;
wire[31:0]	imain_arbt_st		=	`mem_intf_path.MAIN_ARBT_ST;
*/
wire		iram_wrlock_timer	=	`mem_intf_path.RAM_WRLOCK_TIMER;
wire		iram_wrlock_resume	=	`mem_intf_path.RAM_WRLOCK_RESUME;
//wire		iram_wrlock_tag		=	`mem_intf_path.RAM_WRLOCK_TAG;
//wire[7:0]	iram_wrlock_id		=	`mem_intf_path.RAM_WRLOCK_ID;

//Timing Parameters:
wire[15:0]	iram_timeparam		=	`mem_intf_path.RAM_TIMEPARAM;
wire[11:0]	iram_mode_set		=	`mem_intf_path.RAM_MODE_SET;
wire		iram_16bits_mode	=	`mem_intf_path.RAM_16BITS_MODE;
//wire		iram_pre_oe_en		=	`mem_intf_path.RAM_PRE_OE_EN;
wire		iram_post_oe_en		=	`mem_intf_path.RAM_POST_OE_EN;
wire		iram_r2w_ctrl		=	`mem_intf_path.RAM_R2W_CTRL;
//SDR SDRAM memory interface:
wire		iram_cke			=	`mem_intf_path.RAM_CKE;
wire[1:0]	iram_cs_			=	`mem_intf_path.RAM_CS_;
wire		iram_ras_			=	`mem_intf_path.RAM_RAS_;
wire		iram_cas_			=	`mem_intf_path.RAM_CAS_;
wire		iram_we_			=	`mem_intf_path.RAM_WE_;
wire[3:0]	iram_dqm			=	`mem_intf_path.RAM_DQM;
wire[1:0]	iram_ba				=	`mem_intf_path.RAM_BA;
wire[11:0]	iram_addr			=	`mem_intf_path.RAM_ADDR;
wire[31:0]	iram_wdata			=	`mem_intf_path.RAM_WDATA;
wire[31:0]	iram_rdata			=	`mem_intf_path.RAM_RDATA;
wire[31:0]	iram_dq_oe_			=	`mem_intf_path.RAM_DQ_OE_;


//##############################################
//`else //rsim and pre-gsim
//##############################################
/*
`define	mem_intf_path	`path_chipset.MEM_INTF
wire		imem_rst_   = ig_reset_;// `mem_intf_path.rst_   ;
wire		imem_clk    = `mem_intf_path.mem_clk;
//===cpu2mem
wire		icpu_ram_req    = `mem_intf_path.cpu_ram_req		;
wire		icpu_ram_rw     = `mem_intf_path.cpu_ram_rw      ;
wire [29:2]	icpu_ram_addr   = `mem_intf_path.cpu_ram_addr    ;
wire [2:0]	icpu_ram_rbl    = `mem_intf_path.cpu_ram_rbl     ;
wire		icpu_ram_wlast  = `mem_intf_path.cpu_ram_wlast   ;
wire [3:0]	icpu_ram_wbe    = `mem_intf_path.cpu_ram_wbe     ;
wire [31:0]	icpu_ram_wdata  = `mem_intf_path.cpu_ram_wdata   ;

wire		icpu_ram_ack    = `mem_intf_path.cpu_ram_ack		;
wire		icpu_ram_err    = `mem_intf_path.cpu_ram_err     ;//no use now
wire		icpu_ram_rrdy   = `mem_intf_path.cpu_ram_rrdy    ;
wire		icpu_ram_rlast  = `mem_intf_path.cpu_ram_rlast   ;
wire [31:0]	icpu_ram_rdata  = `mem_intf_path.cpu_ram_rdata   ;
wire		icpu_ram_wrdy   = `mem_intf_path.cpu_ram_wrdy    ;

//===pci2mem(read and write independent)
`ifdef	NEW_MEM
//read
//===sb2mem(read and write)
wire		isb_ram_req     = `mem_intf_path.aud_ram_req   	;
wire		isb_ram_rw      = `mem_intf_path.aud_ram_rw      ;
wire [29:2]	isb_ram_addr    = `mem_intf_path.aud_ram_addr    ;
wire [2:0]	isb_ram_rbl     = `mem_intf_path.aud_ram_rbl     ;
wire		isb_ram_wlast   = `mem_intf_path.aud_ram_wlast   ;
wire [3:0]	isb_ram_wbe     = `mem_intf_path.aud_ram_wbe     ;
wire [31:0]	isb_ram_wdata   = `mem_intf_path.aud_ram_wdata   ;
wire		isb_ram_ack     = `mem_intf_path.aud_ram_ack     ;
wire		isb_ram_err     = `mem_intf_path.aud_ram_err     ;//no use now
wire		isb_ram_rrdy    = `mem_intf_path.aud_ram_rrdy    ;
wire		isb_ram_rlast   = `mem_intf_path.aud_ram_rlast   ;
wire [31:0]	isb_ram_rdata   = `mem_intf_path.aud_ram_rdata   ;
wire		isb_ram_wrdy    = `mem_intf_path.aud_ram_wrdy    ;


//===gpu2mem(read an write)
wire		igpu_ram_req   	= `mem_intf_path.ser_ram_req    ;
wire		igpu_ram_rw    	= `mem_intf_path.ser_ram_rw     ;
wire [29:2]	igpu_ram_addr  	= `mem_intf_path.ser_ram_addr   ;
wire [2:0]	igpu_ram_rbl   	= `mem_intf_path.ser_ram_rbl    ;
wire		igpu_ram_wlast 	= `mem_intf_path.ser_ram_wlast  ;
wire [3:0]	igpu_ram_wbe   	= `mem_intf_path.ser_ram_wbe    ;
wire [31:0]	igpu_ram_wdata 	= `mem_intf_path.ser_ram_wdata  ;
wire		igpu_ram_ack   	= `mem_intf_path.ser_ram_ack    ;
wire		igpu_ram_err   	= `mem_intf_path.ser_ram_err    ;//no use now
wire		igpu_ram_rrdy  	= `mem_intf_path.ser_ram_rrdy   ;
wire		igpu_ram_rlast 	= `mem_intf_path.ser_ram_rlast  ;
wire [31:0]	igpu_ram_rdata 	= `mem_intf_path.ser_ram_rdata  ;
wire		igpu_ram_wrdy  	= `mem_intf_path.ser_ram_wrdy   ;
`else
//===sb2mem(read and write)
wire		isb_ram_req     = `mem_intf_path.sb_ram_req   	;
wire		isb_ram_rw      = `mem_intf_path.sb_ram_rw      ;
wire [29:2]	isb_ram_addr    = `mem_intf_path.sb_ram_addr    ;
wire [2:0]	isb_ram_rbl     = `mem_intf_path.sb_ram_rbl     ;
wire		isb_ram_wlast   = `mem_intf_path.sb_ram_wlast   ;
wire [3:0]	isb_ram_wbe     = `mem_intf_path.sb_ram_wbe     ;
wire [31:0]	isb_ram_wdata   = `mem_intf_path.sb_ram_wdata   ;
wire		isb_ram_ack     = `mem_intf_path.sb_ram_ack     ;
wire		isb_ram_err     = `mem_intf_path.sb_ram_err     ;//no use now
wire		isb_ram_rrdy    = `mem_intf_path.sb_ram_rrdy    ;
wire		isb_ram_rlast   = `mem_intf_path.sb_ram_rlast   ;
wire [31:0]	isb_ram_rdata   = `mem_intf_path.sb_ram_rdata   ;
wire		isb_ram_wrdy    = `mem_intf_path.sb_ram_wrdy    ;

//===gpu2mem(read an write)
wire		igpu_ram_req   	= `mem_intf_path.gpu_ram_req    ;
wire		igpu_ram_rw    	= `mem_intf_path.gpu_ram_rw     ;
wire [29:2]	igpu_ram_addr  	= `mem_intf_path.gpu_ram_addr   ;
wire [2:0]	igpu_ram_rbl   	= `mem_intf_path.gpu_ram_rbl    ;
wire		igpu_ram_wlast 	= `mem_intf_path.gpu_ram_wlast  ;
wire [3:0]	igpu_ram_wbe   	= `mem_intf_path.gpu_ram_wbe    ;
wire [31:0]	igpu_ram_wdata 	= `mem_intf_path.gpu_ram_wdata  ;
wire		igpu_ram_ack   	= `mem_intf_path.gpu_ram_ack    ;
wire		igpu_ram_err   	= `mem_intf_path.gpu_ram_err    ;//no use now
wire		igpu_ram_rrdy  	= `mem_intf_path.gpu_ram_rrdy   ;
wire		igpu_ram_rlast 	= `mem_intf_path.gpu_ram_rlast  ;
wire [31:0]	igpu_ram_rdata 	= `mem_intf_path.gpu_ram_rdata  ;
wire		igpu_ram_wrdy  	= `mem_intf_path.gpu_ram_wrdy   ;
`endif
//read
wire		ipci_ram_rreq   = `mem_intf_path.pci_ram_rreq   ;
wire [29:2]	ipci_ram_raddr  = `mem_intf_path.pci_ram_raddr  ;
wire [2:0]	ipci_ram_rbl    = `mem_intf_path.pci_ram_rbl    ;
wire		ipci_ram_rack   = `mem_intf_path.pci_ram_rack   ;
wire		ipci_ram_rerr   = `mem_intf_path.pci_ram_rerr   ;//no use now
wire		ipci_ram_rrdy   = `mem_intf_path.pci_ram_rrdy   ;
wire		ipci_ram_rlast  = `mem_intf_path.pci_ram_rlast  ;
wire [31:0]	ipci_ram_rdata  = `mem_intf_path.pci_ram_rdata  ;
//write
wire		ipci_ram_wreq 	= `mem_intf_path.pci_ram_wreq 	;
wire [29:2]	ipci_ram_waddr  = `mem_intf_path.pci_ram_waddr  ;
wire		ipci_ram_wlast  = `mem_intf_path.pci_ram_wlast  ;
wire [3:0]	ipci_ram_wbe    = `mem_intf_path.pci_ram_wbe    ;
wire [31:0]	ipci_ram_wdata  = `mem_intf_path.pci_ram_wdata  ;
wire		ipci_ram_wack   = `mem_intf_path.pci_ram_wack   ;
wire		ipci_ram_werr   = `mem_intf_path.pci_ram_werr   ;//no use now
wire		ipci_ram_wrdy   = `mem_intf_path.pci_ram_wrdy   ;
//===video2mem(read and write)
wire 		ivideo_ram_req  = `mem_intf_path.video_ram_req   ;
wire		ivideo_ram_rw   = `mem_intf_path.video_ram_rw    ;
wire [29:2]	ivideo_ram_addr = `mem_intf_path.video_ram_addr  ;
wire [2:0]	ivideo_ram_rbl  = `mem_intf_path.video_ram_rbl   ;
wire		ivideo_ram_wlast= `mem_intf_path.video_ram_wlast ;
wire [3:0]	ivideo_ram_wbe  = `mem_intf_path.video_ram_wbe   ;
wire [31:0]	ivideo_ram_wdata= `mem_intf_path.video_ram_wdata ;
wire		ivideo_ram_ack  = `mem_intf_path.video_ram_ack   ;
wire		ivideo_ram_err  = `mem_intf_path.video_ram_err   ;//no use now
wire		ivideo_ram_rrdy = `mem_intf_path.video_ram_rrdy  ;
wire		ivideo_ram_rlast= `mem_intf_path.video_ram_rlast ;
wire [31:0]	ivideo_ram_rdata= `mem_intf_path.video_ram_rdata ;
wire		ivideo_ram_wrdy = `mem_intf_path.video_ram_wrdy  ;

//VSB
wire 		ivsb_ram_req  = `mem_intf_path.vsb_ram_req   ;
wire		ivsb_ram_rw   = `mem_intf_path.vsb_ram_rw    ;
wire [29:2]	ivsb_ram_addr = `mem_intf_path.vsb_ram_addr  ;
wire [2:0]	ivsb_ram_rbl  = `mem_intf_path.vsb_ram_rbl   ;
wire		ivsb_ram_wlast= `mem_intf_path.vsb_ram_wlast ;
wire [3:0]	ivsb_ram_wbe  = `mem_intf_path.vsb_ram_wbe   ;
wire [31:0]	ivsb_ram_wdata= `mem_intf_path.vsb_ram_wdata ;
wire		ivsb_ram_ack  = `mem_intf_path.vsb_ram_ack   ;
wire		ivsb_ram_err  = `mem_intf_path.vsb_ram_err   ;//no use now
wire		ivsb_ram_rrdy = `mem_intf_path.vsb_ram_rrdy  ;
wire		ivsb_ram_rlast= `mem_intf_path.vsb_ram_rlast ;
wire [31:0]	ivsb_ram_rdata= `mem_intf_path.vsb_ram_rdata ;
wire		ivsb_ram_wrdy = `mem_intf_path.vsb_ram_wrdy  ;


//===dsp2mem(read and write)
wire		idsp_ram_req	= `mem_intf_path.dsp_ram_req   	;
wire		idsp_ram_rw     = `mem_intf_path.dsp_ram_rw     ;
wire [29:2]	idsp_ram_addr   = `mem_intf_path.dsp_ram_addr   ;
wire [2:0]	idsp_ram_rbl    = `mem_intf_path.dsp_ram_rbl    ;
wire		idsp_ram_wlast  = `mem_intf_path.dsp_ram_wlast  ;
wire [3:0]	idsp_ram_wbe    = `mem_intf_path.dsp_ram_wbe    ;
wire [31:0]	idsp_ram_wdata  = `mem_intf_path.dsp_ram_wdata  ;
wire		idsp_ram_ack    = `mem_intf_path.dsp_ram_ack    ;
wire		idsp_ram_err    = `mem_intf_path.dsp_ram_err    ;//no use now
wire		idsp_ram_rrdy   = `mem_intf_path.dsp_ram_rrdy   ;
wire		idsp_ram_rlast  = `mem_intf_path.dsp_ram_rlast  ;
wire [31:0]	idsp_ram_rdata  = `mem_intf_path.dsp_ram_rdata  ;
wire		idsp_ram_wrdy   = `mem_intf_path.dsp_ram_wrdy   ;


//===ide2mem(read and write)
wire		iide_ram_req     = `mem_intf_path.ide_ram_req   	;
wire		iide_ram_rw      = `mem_intf_path.ide_ram_rw      ;
wire [29:2]	iide_ram_addr    = `mem_intf_path.ide_ram_addr    ;
wire [2:0]	iide_ram_rbl     = `mem_intf_path.ide_ram_rbl     ;
wire		iide_ram_wlast   = `mem_intf_path.ide_ram_wlast   ;
wire [3:0]	iide_ram_wbe     = `mem_intf_path.ide_ram_wbe     ;
wire [31:0]	iide_ram_wdata   = `mem_intf_path.ide_ram_wdata   ;
wire		iide_ram_ack     = `mem_intf_path.ide_ram_ack     ;
wire		iide_ram_err     = `mem_intf_path.ide_ram_err     ;//no use now
wire		iide_ram_rrdy    = `mem_intf_path.ide_ram_rrdy    ;
wire		iide_ram_rlast   = `mem_intf_path.ide_ram_rlast   ;
wire [31:0]	iide_ram_rdata   = `mem_intf_path.ide_ram_rdata   ;
wire		iide_ram_wrdy    = `mem_intf_path.ide_ram_wrdy    ;

//	----------------------------
//Priority Mode:
wire[1:0]	iarbta_prior_mode	=	`mem_intf_path.suba_arbt_ctrl;
wire[1:0]	iarbtb_prior_mode	=	`mem_intf_path.subb_arbt_ctrl;
wire[1:0]	iarbtm_prior_mode	=	`mem_intf_path.main_arbt_ctrl;

wire		iauto_ref_req		=	`mem_intf_path.auto_ref_timer.refresh_req;
wire		iauto_ref_ack		=	`mem_intf_path.auto_ref_timer.refresh_ack;

//Arbiters States:
wire[31:0]	isuba_arbt_st		=	`mem_intf_path.suba_arbt_st;
wire[31:0]	isubb_arbt_st		=	`mem_intf_path.subb_arbt_st;
wire[31:0]	imain_arbt_st		=	`mem_intf_path.main_arbt_st;
wire		iram_wrlock_timer	=	`mem_intf_path.ram_wrlock_timer;
wire		iram_wrlock_resume	=	`mem_intf_path.ram_wrlock_resume;
wire		iram_wrlock_tag		=	`mem_intf_path.ram_wrlock_tag;
wire[7:0]	iram_wrlock_id		=	`mem_intf_path.ram_wrlock_id;
//Timing Parameters:
wire[15:0]	iram_timeparam		=	`mem_intf_path.ram_timeparam;
wire[11:0]	iram_mode_set		=	`mem_intf_path.ram_mode_set;
wire		iram_16bits_mode	=	`mem_intf_path.ram_16bits_mode;
//wire		iram_pre_oe_en		=	`mem_intf_path.ram_pre_oe_en;
wire		iram_post_oe_en		=	`mem_intf_path.ram_post_oe_en;
wire		iram_r2w_ctrl		=	`mem_intf_path.ram_r2w_ctrl;
//SDR SDRAM memory interface:
wire		iram_cke			=	`mem_intf_path.ram_cke;
wire[1:0]	iram_cs_			=	`mem_intf_path.ram_cs_;
wire		iram_ras_			=	`mem_intf_path.ram_ras_;
wire		iram_cas_			=	`mem_intf_path.ram_cas_;
wire		iram_we_			=	`mem_intf_path.ram_we_;
wire[3:0]	iram_dqm			=	`mem_intf_path.ram_dqm;
wire[1:0]	iram_ba				=	`mem_intf_path.ram_ba;
wire[11:0]	iram_addr			=	`mem_intf_path.ram_addr;
wire[31:0]	iram_wdata			=	`mem_intf_path.ram_wdata;
wire[31:0]	iram_rdata			=	`mem_intf_path.ram_rdata;
wire[31:0]	iram_dq_oe_			=	`mem_intf_path.ram_dq_oe_;
*/
//##############################################
//`endif
//##############################################
`ifdef	POST_GSIM
`else
internal_mem_bus_monitor #(00,67,80,85) cpu2mem_monitor( //"CPU" ASCII NUMBER
	.xxx_ram_req	(icpu_ram_req),
	.xxx_ram_rw		(icpu_ram_rw),
	.xxx_ram_addr	(icpu_ram_addr),
	.xxx_ram_rbl	(icpu_ram_rbl),
	.xxx_ram_ack	(icpu_ram_ack),

	.xxx_ram_wlast	(icpu_ram_wlast),
	.xxx_ram_wrdy	(icpu_ram_wrdy),
	.xxx_ram_wbe	(icpu_ram_wbe),
	.xxx_ram_wdata	(icpu_ram_wdata),
	.xxx_ram_rlast	(icpu_ram_rlast),
	.xxx_ram_rrdy	(icpu_ram_rrdy),
	.xxx_ram_rdata	(icpu_ram_rdata),

	.clk			(imem_clk),
	.rst_			(imem_rst_)
	);
/*
internal_mem_bus_monitor #(68,73,83,80) disp2mem_monitor( //"DISP" ASCII NUMBER
	.xxx_ram_req	(idisp_ram_req),
	.xxx_ram_rw		(1'b0),//only read
	.xxx_ram_addr	(idisp_ram_addr),
	.xxx_ram_rbl	(idisp_ram_rbl),
	.xxx_ram_ack	(idisp_ram_ack),

	.xxx_ram_wlast	(1'b0),
	.xxx_ram_wrdy	(1'b0),
	.xxx_ram_wbe	(4'h0),
	.xxx_ram_wdata	(32'h0),
	.xxx_ram_rlast	(idisp_ram_rlast),
	.xxx_ram_rrdy	(idisp_ram_rrdy),
	.xxx_ram_rdata	(idisp_ram_rdata),

	.clk			(imem_clk),
	.rst_			(imem_rst_)
	);
*/
`ifdef NEW_MEM
`else
internal_mem_bus_monitor #(00,80,67,73) pci2mem_rd_monitor ( //"PCI" ASCII NUMBER
	.xxx_ram_req	(ipci_ram_rreq),
	.xxx_ram_rw		(1'b0),//only read
	.xxx_ram_addr	(ipci_ram_raddr),
	.xxx_ram_rbl	(ipci_ram_rbl),
	.xxx_ram_ack	(ipci_ram_rack),

	.xxx_ram_wlast	(1'b0),
	.xxx_ram_wrdy	(1'b0),
	.xxx_ram_wbe	(4'h0),
	.xxx_ram_wdata	(32'h0),
	.xxx_ram_rlast	(ipci_ram_rlast),
	.xxx_ram_rrdy	(ipci_ram_rrdy),
	.xxx_ram_rdata	(ipci_ram_rdata),

	.clk			(imem_clk),
	.rst_			(imem_rst_)
	);

internal_mem_bus_monitor #(00,80,67,73) pci2mem_wr_monitor ( //"PCI" ASCII NUMBER
	.xxx_ram_req	(ipci_ram_wreq),
	.xxx_ram_rw		(1'b1),//write read
	.xxx_ram_addr	(ipci_ram_waddr),
	.xxx_ram_rbl	(3'b111),//at most is 8
	.xxx_ram_ack	(ipci_ram_wack),

	.xxx_ram_wlast	(ipci_ram_wlast),
	.xxx_ram_wrdy	(ipci_ram_wrdy),
	.xxx_ram_wbe	(ipci_ram_wbe),
	.xxx_ram_wdata	(ipci_ram_wdata),
	.xxx_ram_rlast	(1'b0),
	.xxx_ram_rrdy	(1'b0),
	.xxx_ram_rdata	(32'h0),

	.clk			(imem_clk),
	.rst_			(imem_rst_)
	);
`endif
/*
internal_mem_bus_monitor #(00,71,80,85) gpu2mem_monitor( //"GPU" ASCII NUMBER
	.xxx_ram_req	(igpu_ram_req),
	.xxx_ram_rw		(igpu_ram_rw),
	.xxx_ram_addr	(igpu_ram_addr),
	.xxx_ram_rbl	(igpu_ram_rbl),
	.xxx_ram_ack	(igpu_ram_ack),

	.xxx_ram_wlast	(igpu_ram_wlast),
	.xxx_ram_wrdy	(igpu_ram_wrdy),
	.xxx_ram_wbe	(igpu_ram_wbe),
	.xxx_ram_wdata	(igpu_ram_wdata),
	.xxx_ram_rlast	(igpu_ram_rlast),
	.xxx_ram_rrdy	(igpu_ram_rrdy),
	.xxx_ram_rdata	(igpu_ram_rdata),

	.clk			(imem_clk),
	.rst_			(imem_rst_)
	);
*/
internal_mem_bus_monitor #(00,00,86,69) video2mem_monitor( //"VE" ASCII NUMBER
	.xxx_ram_req	(ivideo_ram_req),
	.xxx_ram_rw		(ivideo_ram_rw),
	.xxx_ram_addr	(ivideo_ram_addr),
	.xxx_ram_rbl	(ivideo_ram_rbl),
	.xxx_ram_ack	(ivideo_ram_ack),

	.xxx_ram_wlast	(ivideo_ram_wlast),
	.xxx_ram_wrdy	(ivideo_ram_wrdy),
	.xxx_ram_wbe	(ivideo_ram_wbe),
	.xxx_ram_wdata	(ivideo_ram_wdata),
	.xxx_ram_rlast	(ivideo_ram_rlast),
	.xxx_ram_rrdy	(ivideo_ram_rrdy),
	.xxx_ram_rdata	(ivideo_ram_rdata),

	.clk			(imem_clk),
	.rst_			(imem_rst_)
	);

internal_mem_bus_monitor #(00,86,83,66) vsb2mem_monitor( //"VSB" ASCII NUMBER
	.xxx_ram_req	(ivsb_ram_req),
	.xxx_ram_rw		(ivsb_ram_rw),
	.xxx_ram_addr	(ivsb_ram_addr),
	.xxx_ram_rbl	(ivsb_ram_rbl),
	.xxx_ram_ack	(ivsb_ram_ack),

	.xxx_ram_wlast	(ivsb_ram_wlast),
	.xxx_ram_wrdy	(ivsb_ram_wrdy),
	.xxx_ram_wbe	(ivsb_ram_wbe),
	.xxx_ram_wdata	(ivsb_ram_wdata),
	.xxx_ram_rlast	(ivsb_ram_rlast),
	.xxx_ram_rrdy	(ivsb_ram_rrdy),
	.xxx_ram_rdata	(ivsb_ram_rdata),

	.clk			(imem_clk),
	.rst_			(imem_rst_)
	);


internal_mem_bus_monitor #(00,68,83,80) dsp2mem_monitor( //"DSP" ASCII NUMBER
	.xxx_ram_req	(idsp_ram_req),
	.xxx_ram_rw		(idsp_ram_rw),
	.xxx_ram_addr	(idsp_ram_addr),
	.xxx_ram_rbl	(idsp_ram_rbl),
	.xxx_ram_ack	(idsp_ram_ack),

	.xxx_ram_wlast	(idsp_ram_wlast),
	.xxx_ram_wrdy	(idsp_ram_wrdy),
	.xxx_ram_wbe	(idsp_ram_wbe),
	.xxx_ram_wdata	(idsp_ram_wdata),
	.xxx_ram_rlast	(idsp_ram_rlast),
	.xxx_ram_rrdy	(idsp_ram_rrdy),
	.xxx_ram_rdata	(idsp_ram_rdata),

	.clk			(imem_clk),
	.rst_			(imem_rst_)
	);
/*JFR030624
internal_mem_bus_monitor #(00,00,83,66) sb2mem_monitor( //"SB" ASCII NUMBER
	.xxx_ram_req	(isb_ram_req),
	.xxx_ram_rw		(isb_ram_rw),
	.xxx_ram_addr	(isb_ram_addr),
	.xxx_ram_rbl	(isb_ram_rbl),
	.xxx_ram_ack	(isb_ram_ack),

	.xxx_ram_wlast	(isb_ram_wlast),
	.xxx_ram_wrdy	(isb_ram_wrdy),
	.xxx_ram_wbe	(isb_ram_wbe),
	.xxx_ram_wdata	(isb_ram_wdata),
	.xxx_ram_rlast	(isb_ram_rlast),
	.xxx_ram_rrdy	(isb_ram_rrdy),
	.xxx_ram_rdata	(isb_ram_rdata),

	.clk			(imem_clk),
	.rst_			(imem_rst_)
	);
*/
internal_mem_bus_monitor #(00,73,68,69) ide2mem_monitor( //"IDE" ASCII NUMBER
	.xxx_ram_req	(iide_ram_req),
	.xxx_ram_rw		(iide_ram_rw),
	.xxx_ram_addr	(iide_ram_addr),
	.xxx_ram_rbl	(iide_ram_rbl),
	.xxx_ram_ack	(iide_ram_ack),

	.xxx_ram_wlast	(iide_ram_wlast),
	.xxx_ram_wrdy	(iide_ram_wrdy),
	.xxx_ram_wbe	(iide_ram_wbe),
	.xxx_ram_wdata	(iide_ram_wdata),
	.xxx_ram_rlast	(iide_ram_rlast),
	.xxx_ram_rrdy	(iide_ram_rrdy),
	.xxx_ram_rdata	(iide_ram_rdata),

	.clk			(imem_clk),
	.rst_			(imem_rst_)
	);
`endif

//	----------------------------------------------------------------------------------

`ifdef	POST_GSIM
`else

//	----------------------------------------------------------------------------------
//SUB-A Arbiter Priority Monitor:
wire	arbta_req, arbta_ack;

arbta_prior_monitor	arbta_prior_monitor(
	//Configure Ports:
//	.arbta_prior_mode		(iarbta_prior_mode	),
	//Devices Ports:
	//GPU:
//	.gpu_req				(igpu_ram_req		),
//	.gpu_ack				(igpu_ram_ack		),
	//Video:
	.video_req				(ivideo_ram_req		),
	.video_ack				(ivideo_ram_ack		),
	//DSP:
	.dsp_req				(idsp_ram_req		),
	.dsp_ack				(idsp_ram_ack		),
	//PCI_RD:
	.pci_rreq				(ipci_ram_rreq		),
	.pci_rack				(ipci_ram_rack		),
	//PCI_WR:
	.pci_wreq				(ipci_ram_wreq		),
	.pci_wack				(ipci_ram_wack		),
	//For arbtm_prior_monitor:
	.arbta_req				(arbta_req			),
	.arbta_ack				(arbta_ack			),
	//Others:
	.rst_					(imem_rst_ 			),
	.mem_clk				(imem_clk			)
	);

//SUB-B Arbiter Priority Monitor:
wire	arbtb_req;
wire	arbtb_ack;

arbtb_prior_monitor	arbtb_prior_monitor(
	//Configure Ports:
//	.arbtb_prior_mode		(iarbtb_prior_mode	),
	//Devices Ports:
	//IDE:
	.ide_req				(iide_ram_req		),
	.ide_ack				(iide_ram_ack		),
	//SB:
//	.sb_req					(isb_ram_req		),
//	.sb_ack					(isb_ram_ack		),
	//VSB:
	.vsb_req				(ivsb_ram_req		),
	.vsb_ack				(ivsb_ram_ack		),
	//For arbtm_prior_monitor:
	.arbtb_req				(arbtb_req			),
	.arbtb_ack				(arbtb_ack			),
	//Others:
	.rst_					(imem_rst_ 			),
	.mem_clk				(imem_clk			)
	);

//Main-Arbiter Priority Monitor:
arbtm_prior_monitor	arbtm_prior_monitor(
	`ifdef	NEW_MEM
	//Configure Ports:
//	.arbtm_prior_mode		(iarbtm_prior_mode	),
	//Refresh:
	.ref_req				(iauto_ref_req		),
	.ref_ack				(iauto_ref_ack		),
	//Display:
//	.ser_req				(igpu_ram_req		),
//	.ser_ack				(igpu_ram_ack		),
	//CPU:
	.cpu_req				(icpu_ram_req		),
	.cpu_ack				(icpu_ram_ack		),
	//Arbiter-A:
	.arbta_req				(arbta_req			),
	.arbta_ack				(arbta_ack			),
	//Arbiter-B:
	.arbtb_req				(arbtb_req			),
	.arbtb_ack				(arbtb_ack			),
	//Others:
	.rst_					(imem_rst_ 			),
	.mem_clk				(imem_clk			)
	);
	`else
	//Configure Ports:
	.arbtm_prior_mode		(iarbtm_prior_mode	),
	//Refresh:
	.ref_req				(iauto_ref_req		),
	.ref_ack				(iauto_ref_ack		),
	//Display:
	.disp_req				(idisp_ram_req		),
	.disp_ack				(idisp_ram_ack		),
	//CPU:
	.cpu_req				(icpu_ram_req		),
	.cpu_ack				(icpu_ram_ack		),
	//Arbiter-A:
	.arbta_req				(arbta_req			),
	.arbta_ack				(arbta_ack			),
	//Arbiter-B:
	.arbtb_req				(arbtb_req			),
	.arbtb_ack				(arbtb_ack			),
	//Others:
	.rst_					(imem_rst_ 			),
	.mem_clk				(imem_clk			)
	);
`endif
imb_status_monitor	imb_status_monitor(
	//the memory bus arbiter states:
//	.suba_arbt_st				(isuba_arbt_st		),		//store the latest four states of the Sub-A Arbiter
//	.subb_arbt_st				(isubb_arbt_st		),		//store the latest four states of the Sub-B Arbiter
//	.main_arbt_st				(imain_arbt_st		),		//store the latest four states of the Main Arbiter
	//for write dead lock:
	.ram_wrlock_timer			(iram_wrlock_timer	),	//used to enable the write dead lock timer
	.ram_wrlock_resume			(iram_wrlock_resume	),	//used to enable exiting from write dead lock state
	.ram_wrlock_tag				(iram_wrlock_tag	),		//report the write dead lock error
//	.ram_wrlock_id				(iram_wrlock_id		),		//report the write dead lock device id
	//bus arbiting:
	//Display:
//	.disp_ack					(idisp_ram_ack		),
//	.disp_addr					(idisp_ram_addr		),
	//CPU:
	.cpu_ack					(icpu_ram_ack		),
	.cpu_rw						(icpu_ram_rw		),
	.cpu_addr					(icpu_ram_addr		),
	//GPU:
//	.gpu_ack					(igpu_ram_ack		),//JFR031103
//	.gpu_rw						(igpu_ram_rw		),//JFR031103
//	.gpu_addr					(igpu_ram_addr		),//JFR031103
	//Video:
	.video_ack					(ivideo_ram_ack		),
	.video_rw					(ivideo_ram_rw		),
	.video_addr					(ivideo_ram_addr	),
	//DSP:
	.dsp_ack					(idsp_ram_ack		),
	.dsp_rw						(idsp_ram_rw		),
	.dsp_addr					(idsp_ram_addr		),
	//PCI:
	.pci_rack					(ipci_ram_rack		),
	.pci_raddr					(ipci_ram_raddr		),
	.pci_wack					(ipci_ram_wack		),
	.pci_waddr					(ipci_ram_waddr		),
	//IDE:
	.ide_ack					(iide_ram_ack		),
	.ide_rw						(iide_ram_rw		),
	.ide_addr					(iide_ram_addr		),
	//SB:
//	.sb_ack						(isb_ram_ack		),//JFR031103
//	.sb_rw						(isb_ram_rw			),//JFR031103
//	.sb_addr					(isb_ram_addr		),//JFR031103
	//VSB:
	.vsb_ack					(ivsb_ram_ack		),
	.vsb_rw						(ivsb_ram_rw		),
	.vsb_addr					(ivsb_ram_addr		),

	//Read Data-Path:
//	.disp_rrdy					(idisp_ram_rrdy		),
//	.disp_rlast					(idisp_ram_rlast	),
//	.disp_rdata					(idisp_ram_rdata	),
	.cpu_rrdy					(icpu_ram_rrdy		),
	.cpu_rlast					(icpu_ram_rlast		),
	.cpu_rdata					(icpu_ram_rdata		),
//	.gpu_rrdy					(igpu_ram_rrdy		),
//	.gpu_rlast					(igpu_ram_rlast		),
//	.gpu_rdata					(igpu_ram_rdata		),
	.video_rrdy					(ivideo_ram_rrdy	),
	.video_rlast				(ivideo_ram_rlast	),
	.video_rdata				(ivideo_ram_rdata	),
	.dsp_rrdy					(idsp_ram_rrdy		),
	.dsp_rlast					(idsp_ram_rlast		),
	.dsp_rdata					(idsp_ram_rdata		),
	.pci_rrdy					(ipci_ram_rrdy		),
	.pci_rlast					(ipci_ram_rlast		),
	.pci_rdata					(ipci_ram_rdata		),
	.ide_rrdy					(iide_ram_rrdy		),
	.ide_rlast					(iide_ram_rlast		),
	.ide_rdata					(iide_ram_rdata		),
//	.sb_rrdy					(isb_ram_rrdy		),
//	.sb_rlast					(isb_ram_rlast		),
//	.sb_rdata					(isb_ram_rdata		),
	.vsb_rrdy					(ivsb_ram_rrdy		),
	.vsb_rlast					(ivsb_ram_rlast		),
	.vsb_rdata					(ivsb_ram_rdata		),
	//Write Data-Path:
	.cpu_wrdy					(icpu_ram_wrdy		),
	.cpu_wlast					(icpu_ram_wlast		),
	.cpu_wbe					(icpu_ram_wbe		),
	.cpu_wdata					(icpu_ram_wdata		),
//	.gpu_wrdy					(igpu_ram_wrdy		),
//	.gpu_wlast					(igpu_ram_wlast		),
//	.gpu_wbe					(igpu_ram_wbe		),
//	.gpu_wdata					(igpu_ram_wdata		),
	.video_wrdy					(ivideo_ram_wrdy	),
	.video_wlast				(ivideo_ram_wlast	),
	.video_wbe					(ivideo_ram_wbe		),
	.video_wdata				(ivideo_ram_wdata	),
	.dsp_wrdy					(idsp_ram_wrdy		),
	.dsp_wlast					(idsp_ram_wlast		),
	.dsp_wbe					(idsp_ram_wbe		),
	.dsp_wdata					(idsp_ram_wdata		),
	.pci_wrdy					(ipci_ram_wrdy		),
	.pci_wlast					(ipci_ram_wlast		),
	.pci_wbe					(ipci_ram_wbe		),
	.pci_wdata					(ipci_ram_wdata		),
	.ide_wrdy					(iide_ram_wrdy		),
	.ide_wlast					(iide_ram_wlast		),
	.ide_wbe					(iide_ram_wbe		),
	.ide_wdata					(iide_ram_wdata		),
//	.sb_wrdy					(isb_ram_wrdy		),
//	.sb_wlast					(isb_ram_wlast		),
//	.sb_wbe						(isb_ram_wbe		),
//	.sb_wdata					(isb_ram_wdata		),
	.vsb_wrdy					(ivsb_ram_wrdy		),
	.vsb_wlast					(ivsb_ram_wlast		),
	.vsb_wbe					(ivsb_ram_wbe		),
	.vsb_wdata					(ivsb_ram_wdata		),
	//Others:
	.rst_						(imem_rst_ 			),
	.mem_clk					(imem_clk			)
	);

`endif

sdram_dimms_monitor	sdram_dimms_monitor(
	//System IO:
	.ram_timeparam				(iram_timeparam		),
	.ram_mode_set				(iram_mode_set		),
	.ram_16bits_mode			(iram_16bits_mode	),
//	.ram_pre_oe_en				(iram_pre_oe_en		),
	.ram_post_oe_en				(iram_post_oe_en	),
	.ram_r2w_ctrl				(iram_r2w_ctrl		),
	//SDRAM Memory Bus:
	.ram_cke					(ext_rst_			),
	.ram_cs_					(ram_cs_			),
	.ram_ras_					(ram_ras_			),
	.ram_cas_					(ram_cas_			),
	.ram_we_					(ram_we_			),
	.ram_dqm					(ram_dqm			),
	.ram_ba						(ram_ba				),
	.ram_addr					(ram_addr			),
	.ram_wdata					(iram_wdata			),
	.ram_rdata					(iram_rdata			),
	.ram_dq_oe_					(iram_dq_oe_		),
	//Others:
	.rst_						(imem_rst_ 			),
	.mem_clk					(sd_clk				)
	);

//	----------------------------------------------------------------------------------
//invoke the internal local bus protocol monitor
//##############################################
`ifdef  POST_GSIM
//##############################################
`define	nb_cpu_biu_path	`path_chipset.NB_CPU_BIU
wire		icpu_slave_rw		= `nb_cpu_biu_path.CPU_SLAVE_RW   ;
wire [15:2]	icpu_slave_addr     = `nb_cpu_biu_path.CPU_SLAVE_ADDR ;
wire [3:0]	icpu_slave_be       = `nb_cpu_biu_path.CPU_SLAVE_BE   ;
wire [31:0]	icpu_slave_wdata    = `nb_cpu_biu_path.CPU_SLAVE_WDATA;

wire		icpu_dsp_req        = `nb_cpu_biu_path.CPU_DSP_REQ    ;
wire		icpu_sb_req         = `nb_cpu_biu_path.CPU_SB_REQ     ;
//wire		icpu_ve_req         = `nb_cpu_biu_path.CPU_VE_REQ     ;
//wire		icpu_disp_req       = `nb_cpu_biu_path.CPU_DISP_REQ   ;
wire		icpu_vsb_req		= `nb_cpu_biu_path.CPU_VSB_REQ	  ;
wire		icpu_ide_req	    = `nb_cpu_biu_path.CPU_IDE_REQ	  ;
wire		icpu_tvenc_req      = `nb_cpu_biu_path.CPU_TVENC_REQ  ;

//wire		icpu_gpu_ack        = `nb_cpu_biu_path.CPU_GPU_ACK    ;
wire		icpu_dsp_ack        = `nb_cpu_biu_path.CPU_DSP_ACK    ;
wire		icpu_sb_ack         = `nb_cpu_biu_path.CPU_SB_ACK     ;
//wire		icpu_ve_ack         = `nb_cpu_biu_path.CPU_VE_ACK     ;
//wire		icpu_disp_ack       = `nb_cpu_biu_path.CPU_DISP_ACK   ;
wire		icpu_vsb_ack		= `nb_cpu_biu_path.CPU_VSB_ACK	  ;
wire		icpu_ide_ack        = `nb_cpu_biu_path.CPU_IDE_ACK    ;
wire		icpu_tvenc_ack      = `nb_cpu_biu_path.CPU_TVENC_ACK  ;

//wire [31:0]	icpu_gpu_rdata      = `nb_cpu_biu_path.CPU_GPU_RDATA  ;
wire [31:0]	icpu_dsp_rdata      = `nb_cpu_biu_path.CPU_DSP_RDATA  ;
wire [31:0]	icpu_sb_rdata       = `nb_cpu_biu_path.CPU_SB_RDATA   ;
//wire [31:0]	icpu_ve_rdata       = `nb_cpu_biu_path.CPU_VE_RDATA   ;
//wire [31:0]	icpu_disp_rdata     = `nb_cpu_biu_path.CPU_DISP_RDATA ;
wire [31:0] icpu_vsb_rdata		= `nb_cpu_biu_path.CPU_VSB_RDATA  ;
wire [31:0] icpu_ide_rdata	    = `nb_cpu_biu_path.CPU_IDE_RDATA  ;
wire [31:0] icpu_tvenc_rdata    = `nb_cpu_biu_path.CPU_TVENC_RDATA;

//##############################################
`else //rsim and pre-gsim
//##############################################
`define	nb_cpu_biu_path	`path_chipset.NB_CPU_BIU
wire		icpu_slave_rw		= `nb_cpu_biu_path.cpu_slave_rw   ;
wire [15:2]	icpu_slave_addr     = `nb_cpu_biu_path.cpu_slave_addr ;
wire [3:0]	icpu_slave_be       = `nb_cpu_biu_path.cpu_slave_be   ;
wire [31:0]	icpu_slave_wdata    = `nb_cpu_biu_path.cpu_slave_wdata;

//wire		icpu_gpu_req        = `nb_cpu_biu_path.cpu_gpu_req    ;
wire		icpu_dsp_req        = `nb_cpu_biu_path.cpu_dsp_req    ;
wire		icpu_sb_req         = `nb_cpu_biu_path.cpu_sb_req     ;
//wire		icpu_ve_req         = `nb_cpu_biu_path.cpu_ve_req     ;
//wire		icpu_disp_req       = `nb_cpu_biu_path.cpu_disp_req   ;
wire		icpu_vsb_req		= `nb_cpu_biu_path.cpu_vsb_req	  ;
wire		icpu_ide_req	    = `nb_cpu_biu_path.cpu_ide_req	  ;
wire		icpu_tvenc_req      = `nb_cpu_biu_path.cpu_tvenc_req  ;

//wire		icpu_gpu_ack        = `nb_cpu_biu_path.cpu_gpu_ack    ;
wire		icpu_dsp_ack        = `nb_cpu_biu_path.cpu_dsp_ack    ;
wire		icpu_sb_ack         = `nb_cpu_biu_path.cpu_sb_ack     ;
//wire		icpu_ve_ack         = `nb_cpu_biu_path.cpu_ve_ack     ;
//wire		icpu_disp_ack       = `nb_cpu_biu_path.cpu_disp_ack   ;
wire		icpu_vsb_ack		= `nb_cpu_biu_path.cpu_vsb_ack	  ;
wire		icpu_ide_ack        = `nb_cpu_biu_path.cpu_ide_ack    ;
wire		icpu_tvenc_ack      = `nb_cpu_biu_path.cpu_tvenc_ack  ;

//wire [31:0]	icpu_gpu_rdata      = `nb_cpu_biu_path.cpu_gpu_rdata  ;
wire [31:0]	icpu_dsp_rdata      = `nb_cpu_biu_path.cpu_dsp_rdata  ;
wire [31:0]	icpu_sb_rdata       = `nb_cpu_biu_path.cpu_sb_rdata   ;
//wire [31:0]	icpu_ve_rdata       = `nb_cpu_biu_path.cpu_ve_rdata   ;
//wire [31:0]	icpu_disp_rdata     = `nb_cpu_biu_path.cpu_disp_rdata ;
wire [31:0] icpu_vsb_rdata		= `nb_cpu_biu_path.cpu_vsb_rdata  ;
wire [31:0] icpu_ide_rdata	    = `nb_cpu_biu_path.cpu_ide_rdata  ;
wire [31:0] icpu_tvenc_rdata    = `nb_cpu_biu_path.cpu_tvenc_rdata;
//##############################################
`endif
//##############################################
`ifdef	POST_GSIM
`else

internal_local_bus_monitor #(00,68,83,80) cpu2dsp_io_monitor( //"DSP" ASCII number
	.cpu_xxx_req	(icpu_dsp_req),
	.cpu_xxx_rw		(icpu_slave_rw),
	.cpu_xxx_addr	(icpu_slave_addr),
	.cpu_xxx_be		(icpu_slave_be),
	.cpu_xxx_wdata	(icpu_slave_wdata),
	.cpu_xxx_rdata	(icpu_dsp_rdata),
	.cpu_xxx_ack	(icpu_dsp_ack),

	.clk			(imem_clk),//local io clock is as same as sdram
	.rst_           (imem_rst_)
	);

internal_local_bus_monitor #(00,00,83,66) cpu2sb_io_monitor( //"SB" ASCII number
	.cpu_xxx_req	(icpu_sb_req),
	.cpu_xxx_rw		(icpu_slave_rw),
	.cpu_xxx_addr	(icpu_slave_addr),
	.cpu_xxx_be		(icpu_slave_be),
	.cpu_xxx_wdata	(icpu_slave_wdata),
	.cpu_xxx_rdata	(icpu_sb_rdata),
	.cpu_xxx_ack	(icpu_sb_ack),

	.clk			(imem_clk),//local io clock is as same as sdram
	.rst_           (imem_rst_)
	);

//internal_local_bus_monitor #(00,00,86,69) cpu2ve_io_monitor( //"VE" ASCII number
//	.cpu_xxx_req	(icpu_ve_req),
//	.cpu_xxx_rw		(icpu_slave_rw),
//	.cpu_xxx_addr	(icpu_slave_addr),
//	.cpu_xxx_be		(icpu_slave_be),
//	.cpu_xxx_wdata	(icpu_slave_wdata),
//	.cpu_xxx_rdata	(icpu_ve_rdata),
//	.cpu_xxx_ack	(icpu_ve_ack),
//
//	.clk			(imem_clk),//local io clock is as same as sdram
//	.rst_           (imem_rst_)
//	);

//internal_local_bus_monitor #(68,73,83,80) cpu2disp_io_monitor( //"DISP" ASCII number
//	.cpu_xxx_req	(icpu_disp_req),
//	.cpu_xxx_rw		(icpu_slave_rw),
//	.cpu_xxx_addr	(icpu_slave_addr),
//	.cpu_xxx_be		(icpu_slave_be),
//	.cpu_xxx_wdata	(icpu_slave_wdata),
//	.cpu_xxx_rdata	(icpu_disp_rdata),
//	.cpu_xxx_ack	(icpu_disp_ack),
//
//	.clk			(imem_clk),//local io clock is as same as sdram
//	.rst_           (imem_rst_)
//	);

internal_local_bus_monitor #(00,86,83,66) cpu2vsb_io_monitor( //"VSB" ASCII number
	.cpu_xxx_req	(icpu_vsb_req),
	.cpu_xxx_rw		(icpu_slave_rw),
	.cpu_xxx_addr	(icpu_slave_addr),
	.cpu_xxx_be		(icpu_slave_be),
	.cpu_xxx_wdata	(icpu_slave_wdata),
	.cpu_xxx_rdata	(icpu_vsb_rdata),
	.cpu_xxx_ack	(icpu_vsb_ack),

	.clk			(imem_clk),//local io clock is as same as sdram
	.rst_           (imem_rst_)
	);

internal_local_bus_monitor #(00,73,68,69) cpu2ide_io_monitor( //"IDE" ASCII number
	.cpu_xxx_req	(icpu_ide_req),
	.cpu_xxx_rw		(icpu_slave_rw),
	.cpu_xxx_addr	(icpu_slave_addr),
	.cpu_xxx_be		(icpu_slave_be),
	.cpu_xxx_wdata	(icpu_slave_wdata),
	.cpu_xxx_rdata	(icpu_ide_rdata),
	.cpu_xxx_ack	(icpu_ide_ack),

	.clk			(imem_clk),//local io clock is as same as sdram
	.rst_           (imem_rst_)
	);

internal_local_bus_monitor #(84,86,69,78) cpu2tvenc_io_monitor( //"TVEN" ASCII number
	.cpu_xxx_req	(icpu_tvenc_req),
	.cpu_xxx_rw		(icpu_slave_rw),
	.cpu_xxx_addr	(icpu_slave_addr),
	.cpu_xxx_be		(icpu_slave_be),
	.cpu_xxx_wdata	(icpu_slave_wdata),
	.cpu_xxx_rdata	(icpu_tvenc_rdata),
	.cpu_xxx_ack	(icpu_tvenc_ack),

	.clk			(imem_clk),//local io clock is as same as sdram
	.rst_           (imem_rst_)
	);
`endif

//invoke the clock monitor
//##############################################
`ifdef  POST_GSIM
//##############################################
wire	core_sb_clk			= `path.SB_CLK		;
wire	core_usb_clk		= `path.USB_CLK	    ;
//wire	core_disp_clk		= `path.DISP_CLK	;
//wire	core_gpu_clk		= `path.GPU_CLK	    ;
wire	core_cpu_clk		= `path.CPU_CLK	    ;
wire	core_mem_clk		= `path.MEM_CLK	    ;
wire	core_pci_clk		= `path.PCI_CLK	    ;
wire	core_cpu_clk_src	= `path.CPU_CLK_SRC ;
wire	core_dsp_clk		= `path.DSP_CLK	    ;
wire	core_video_clk		= `path.VIDEO_CLK	;
wire	core_spdif_clk		= `path.SPDIF_CLK	;
wire	core_mbus_clk		= `path.MBUS_CLK	;
//wire	core_ide100m_clk	= `path.IDE100M_CLK ;
//wire	core_ide66m_clk		= `path.IDE66M_CLK	;
//##############################################
`else
//##############################################
wire	core_sb_clk			= `path.sb_clk		;
wire	core_usb_clk		= `path.usb_clk	    ;
//wire	core_disp_clk		= `path.disp_clk	;
//wire	core_gpu_clk		= `path.gpu_clk	    ;
wire	core_cpu_clk		= `path.cpu_clk	    ;
wire	core_mem_clk		= `path.mem_clk	    ;
wire	core_pci_clk		= `path.pci_clk	    ;
wire	core_cpu_clk_src	= `path.cpu_clk_src ;
wire	core_dsp_clk		= `path.dsp_clk	    ;
wire	core_video_clk		= `path.video_clk	;
wire	core_spdif_clk		= `path.spdif_clk	;
wire	core_mbus_clk		= `path.mbus_clk	;
//wire	core_ide100m_clk	= `path.ide100m_clk ;
//wire	core_ide66m_clk		= `path.ide66m_clk	;
//##############################################
`endif
//##############################################
`ifdef	NO_CLK_MONITOR
`else
//before invoke the clock monitor, must define a file handle
//for record the monitor report
integer clk_monitor_handle;
initial  begin
	clk_monitor_handle = $fopen ("clk_monitor.rpt");
end
clock_monitor #(0,0,0,0,83,66,	//"SB" ASCII number
				0.005,				//frequency tolerance (percent)
				0.005)				//Duty cycle tolerance (percent)
		SB_CLOCK_MONITOR (
		.IN_CLK					(core_sb_clk	),//12M
		.START_TRIGGER_POINT	(ig_reset_),//(1'b1	),
		.REGULAR				(1'b1	),  //regular clock
		.SENSITIVE_EDGE			(1'b1	),	//rising edge trigger clock
		.SENSITIVE_DUTY			(1'b1	),	//snoop duty cycle changing
		.OBJECT_CLK_SCALE		(2'b1	)	//scale: 2M ~200M
		);

clock_monitor #(0,0,0,85,83,66,	//"USB" ASCII number
				0.005,				//frequency tolerance (percent)
				0.005)				//Duty cycle tolerance (percent)
		USB_CLOCK_MONITOR (
		.IN_CLK					(core_usb_clk	),//48M
		.START_TRIGGER_POINT	(ig_reset_),//(1'b1	),
		.REGULAR				(1'b1	),  //regular clock
		.SENSITIVE_EDGE			(1'b1	),	//rising edge trigger clock
		.SENSITIVE_DUTY			(1'b1	),	//snoop duty cycle changing
		.OBJECT_CLK_SCALE		(2'b1	)	//scale: 2M ~200M
		);

clock_monitor #(0,0,68,73,83,80,	//"DISP" ASCII number
				0.005,				//frequency tolerance (percent)
				0.005)				//Duty cycle tolerance (percent)
		DISP_CLOCK_MONITOR (
//		.IN_CLK					(core_disp_clk	),//27M
		.START_TRIGGER_POINT	(ig_reset_),//(1'b1	),
		.REGULAR				(1'b1	),  //regular clock
		.SENSITIVE_EDGE			(1'b1	),	//rising edge trigger clock
		.SENSITIVE_DUTY			(1'b1	),	//snoop duty cycle changing
		.OBJECT_CLK_SCALE		(2'b1	)	//scale: 2M ~200M
		);
/*
clock_monitor #(0,0,0,71,80,85,		//"GPU" ASCII number
				0.005,				//frequency tolerance (percent)
				0.005)				//Duty cycle tolerance (percent)
		GPU_CLOCK_MONITOR (
		.IN_CLK					(core_gpu_clk	),//125M
		.START_TRIGGER_POINT	(ig_reset_),//(1'b1	),
		.REGULAR				(1'b1	),  //regular clock
		.SENSITIVE_EDGE			(1'b1	),	//rising edge trigger clock
		.SENSITIVE_DUTY			(1'b1	),	//snoop duty cycle changing
		.OBJECT_CLK_SCALE		(2'b1	)	//scale: 2M ~200M
		);
*/
clock_monitor #(0,0,0,67,80,85,		//"CPU" ASCII number
				0.005,				//frequency tolerance (percent)
				0.005)				//Duty cycle tolerance (percent)
		CPU_CLOCK_MONITOR (
		.IN_CLK					(core_cpu_clk	),//250M
		.START_TRIGGER_POINT	(ig_reset_),//(1'b1	),
		.REGULAR				(1'b1	),  //regular clock
		.SENSITIVE_EDGE			(1'b1	),	//rising edge trigger clock
		.SENSITIVE_DUTY			(1'b1	),	//snoop duty cycle changing
		.OBJECT_CLK_SCALE		(2'b0	)	//scale: 100M ~10G
		);

clock_monitor #(0,0,0,77,69,77,		//"MEM" ASCII number
				0.005,				//frequency tolerance (percent)
				0.005)				//Duty cycle tolerance (percent)
		MEM_CLOCK_MONITOR (
		.IN_CLK					(core_mem_clk	),//166M
		.START_TRIGGER_POINT	(ig_reset_),//(1'b1	),
		.REGULAR				(1'b1	),  //regular clock
		.SENSITIVE_EDGE			(1'b1	),	//rising edge trigger clock
		.SENSITIVE_DUTY			(1'b1	),	//snoop duty cycle changing
		.OBJECT_CLK_SCALE		(2'b1	)	//scale: 2M ~200M
		);

clock_monitor #(0,0,0,80,67,73,	//"PCI" ASCII number
				0.015,				//frequency tolerance (percent)
				0.015)				//Duty cycle tolerance (percent)
		PCI_CLOCK_MONITOR (
		.IN_CLK					(core_pci_clk	),//33M
		.START_TRIGGER_POINT	(ig_reset_),//(1'b1	),
		.REGULAR				(1'b1	),  //regular clock
		.SENSITIVE_EDGE			(1'b1	),	//rising edge trigger clock
		.SENSITIVE_DUTY			(1'b1	),	//snoop duty cycle changing
		.OBJECT_CLK_SCALE		(2'b1	)	//scale: 2M ~200M
		);

clock_monitor #(0,0,67,80,85,83,	//"CPUS" ASCII number
				0.005,				//frequency tolerance (percent)
				0.005)				//Duty cycle tolerance (percent)
		CPUS_CLOCK_MONITOR (
		.IN_CLK					(core_cpu_clk_src	),//250M
		.START_TRIGGER_POINT	(ig_reset_),//(1'b1	),
		.REGULAR				(1'b1	),  //regular clock
		.SENSITIVE_EDGE			(1'b1	),	//rising edge trigger clock
		.SENSITIVE_DUTY			(1'b1	),	//snoop duty cycle changing
		.OBJECT_CLK_SCALE		(2'b0	)	//scale: 100M ~10G
		);

clock_monitor #(0,0,0,68,83,80,		//"DSP" ASCII number
				0.015,				//frequency tolerance (percent)
				0.005)				//Duty cycle tolerance (percent)
		DSP_CLOCK_MONITOR (
		.IN_CLK					(core_dsp_clk	),//125M
		.START_TRIGGER_POINT	(ig_reset_),//(1'b1	),
		.REGULAR				(1'b0	),  //unregular clock
		.SENSITIVE_EDGE			(1'b1	),	//rising edge trigger clock
		.SENSITIVE_DUTY			(1'b1	),	//snoop duty cycle changing
		.OBJECT_CLK_SCALE		(2'b0	)	//scale: 2M ~200M
		);

clock_monitor #(0,86,73,68,69,79,	//"VIDEO" ASCII number
				0.005,				//frequency tolerance (percent)
				0.005)				//Duty cycle tolerance (percent)
		VIDEO_CLOCK_MONITOR (
		.IN_CLK					(core_video_clk	),//133M
		.START_TRIGGER_POINT	(ig_reset_),//(1'b1	),
		.REGULAR				(1'b1	),  //regular clock
		.SENSITIVE_EDGE			(1'b1	),	//rising edge trigger clock
		.SENSITIVE_DUTY			(1'b1	),	//snoop duty cycle changing
		.OBJECT_CLK_SCALE		(2'b1	)	//scale: 2M ~200M
		);

clock_monitor #(0,83,80,68,73,70,	//"SPDIF" ASCII number
				0.005,				//frequency tolerance (percent)
				0.005)				//Duty cycle tolerance (percent)
		SPDIF_CLOCK_MONITOR (
		.IN_CLK					(core_spdif_clk	),//24M
		.START_TRIGGER_POINT	(ig_reset_),//(1'b1	),
		.REGULAR				(1'b1	),  //regular clock
		.SENSITIVE_EDGE			(1'b1	),	//rising edge trigger clock
		.SENSITIVE_DUTY			(1'b1	),	//snoop duty cycle changing
		.OBJECT_CLK_SCALE		(2'b1	)	//scale: 2M ~200M
		);

clock_monitor #(0,0,77,66,85,83,	//"MBUS" ASCII number
				0.005,				//frequency tolerance (percent)
				0.005)				//Duty cycle tolerance (percent)
		MBUS_CLOCK_MONITOR (
		.IN_CLK					(core_mbus_clk	),//40M
		.START_TRIGGER_POINT	(ig_reset_),//(1'b1	),
		.REGULAR				(1'b1	),  //regular clock
		.SENSITIVE_EDGE			(1'b1	),	//rising edge trigger clock
		.SENSITIVE_DUTY			(1'b1	),	//snoop duty cycle changing
		.OBJECT_CLK_SCALE		(2'b1	)	//scale: 2M ~200M
		);

/*
clock_monitor #(0,73,49,48,48,77,	//"I100M" ASCII number
				0.005,				//frequency tolerance (percent)
				0.005)				//Duty cycle tolerance (percent)
		IDE100M_CLOCK_MONITOR (
		.IN_CLK					(core_ide100m_clk	),//100M
		.START_TRIGGER_POINT	(ig_reset_),//(1'b1	),
		.REGULAR				(1'b1	),  //regular clock
		.SENSITIVE_EDGE			(1'b1	),	//rising edge trigger clock
		.SENSITIVE_DUTY			(1'b1	),	//snoop duty cycle changing
		.OBJECT_CLK_SCALE		(2'b1	)	//scale: 2M ~200M
		);

clock_monitor #(0,0,73,54,54,77,	//"I66M" ASCII number
				0.100,				//frequency tolerance (percent)
				0.100)				//Duty cycle tolerance (percent)
		IDE66M_CLOCK_MONITOR (
		.IN_CLK					(core_ide66m_clk	),//66M
		.START_TRIGGER_POINT	(ig_reset_),//(1'b1	),
		.REGULAR				(1'b1	),  //regular clock
		.SENSITIVE_EDGE			(1'b1	),  //rising edge trigger clock
		.SENSITIVE_DUTY			(1'b1	),  //snoop duty cycle changing
		.OBJECT_CLK_SCALE		(2'b1	)	//scale: 2M ~200M
		);
*/

`endif	//NO_CLK_MONITOR

//==============================end monitor invoke ============================
//`ifdef GSIM
//`else
// Flash ROM fastest timing config
//defparam `path_chipset.p_biu.pci_reg.rom_cycle_width = 4'h0;
//defparam `path_chipset.p_biu.pci_reg.wrst_length = 6'h10;
//defparam `path_chip.pad_rst_strap.clock_gen_rst_length = 10'h1ff;
//defparam `path_chip.pad_rst_strap.warm_rst_length = 9'h1e0;     // only 16 clock of F48M
//`endif

