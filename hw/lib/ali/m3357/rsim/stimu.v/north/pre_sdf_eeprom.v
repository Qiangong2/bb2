
/*===========================================================
file:			pre_sdf.eeprom.v
description:	add your ip sdf file here for prelayout gsim
============================================================*/

initial begin
//Syntax
//$sdf_annotate( "sdf_file", module_instance, "config_file", "log_file",
//				 "mtm_spec", "scale_factors", "scale_type" );
//Sample
//$sdf_annotate	("../../../gsrc/metal_fix/chip.sdf", CHIP,	"../../../gsrc/sdf.config", "chip_sdf.log",
//				 "MAXIMUM", "1.0:1.0:1.0", 	"FROM_MAXIMUM");


$sdf_annotate	("/beta/design/m3357/gsrc/prelayout/north/eeprom/EEPROM.sdf",
			top.CHIP.CORE.T2_CHIPSET.EEPROM,
			,
			"eeprom_sdf.log",
			"MAXIMUM",
			,
			"FROM_MTM");

end
			