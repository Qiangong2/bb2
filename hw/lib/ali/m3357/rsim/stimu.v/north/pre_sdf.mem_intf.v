
/*===========================================================
file:			pre_sdf.nb.v
description:	add your ip sdf file here for prelayout gsim
============================================================*/

initial begin
//Syntax
//$sdf_annotate( "sdf_file", module_instance, "config_file", "log_file",
//				 "mtm_spec", "scale_factors", "scale_type" );
//Sample
//$sdf_annotate	("../../../gsrc/metal_fix/chip.sdf", CHIP,	"../../../gsrc/sdf.config", "chip_sdf.log",
//				 "MAXIMUM", "1.0:1.0:1.0", 	"FROM_MAXIMUM");


`ifdef	INC_NB //add NB sdf file annotation here
/*
$sdf_annotate	("/beta/design/m6304/gsrc/prelayout/north/pbiu/p_biu.sdf",
			top.chip.core.t2_chipset.p_biu,
			,
			"pbiu_sdf.log",
			"MAXIMUM",
			,
			"FROM_MTM");

*/

$sdf_annotate	("/beta/design/m3357/gsrc/prelayout/north/mem/mem_intf_post_ins.sdf",
			top.chip.core.t2_chipset.mem_intf,
			,
			"mem_intf.log",
			"MAXIMUM",
			,
			"FROM_MTM");

`endif
end
