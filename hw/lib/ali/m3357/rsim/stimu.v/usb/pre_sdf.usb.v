
/*===========================================================
file:			pre_sdf.usb.v
description:	add your ip sdf file here for prelayout gsim
============================================================*/

initial begin
//Syntax
//$sdf_annotate( "sdf_file", module_instance, "config_file", "log_file",
//				 "mtm_spec", "scale_factors", "scale_type" );
//Sample
//$sdf_annotate	("../../../gsrc/metal_fix/chip.sdf", CHIP,	"../../../gsrc/sdf.config", "chip_sdf.log",
//				 "MAXIMUM", "1.0:1.0:1.0", 	"FROM_MAXIMUM");

`ifdef	INC_USB //add USB sdf file annotation here

        $sdf_annotate("../../../gsrc/prelayout/usb/hydra_core.sdf", CHIP.CORE.T2_CHIPSET.P_BIU.hydra_core, , "./hydra_core_sdf.log",
		"MAXIMUM", "1.0:1.0:1.0", "FROM_MAXIMUM");

`endif
end
