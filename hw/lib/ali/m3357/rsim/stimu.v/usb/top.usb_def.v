`define         INC_USB		//include usb

`define GINFO           "Info"
	`define	display		$display

`define HYDRA_REQ_GNT 2    // 6/18/1996 KuoBa


`define Port60		     16'h0060
`define Port64		     16'h0064

// status configuartion register

`define DetectedParityError  32'h8000_0000
`define Serr_Status          32'h4000_0000
`define ReceivedMasterAbort  32'h2000_0000
`define ReceivedTargetAbort  32'h1000_0000
`define SignaledTargetAbort  32'h0800_0000
`define DataParityReported   32'h0100_0000


// dummy epd addresses for interrupt list

// HcCommandStatus defines

`define HostControllerReset    32'h0000_0001
`define ControlListFilled      32'h0000_0002
`define BulkListFilled         32'h0000_0004
`define OwnershipChangeRequest 32'h0000_0008

// HcControl defines

`define CBSR_1		       32'h0000_0000
`define CBSR_2		       32'h0000_0001
`define CBSR_3		       32'h0000_0002
`define CBSR_4		       32'h0000_0003

`define PeriodicListEnable     32'h0000_0004
`define IsochronousEnable      32'h0000_0008
`define ControlListEnable      32'h0000_0010
`define BulkListEnable         32'h0000_0020

// USB States
`define UsbReset               32'h0000_0000
`define UsbResume              32'h0000_0040
`define UsbOperational         32'h0000_0080
`define UsbSuspend             32'h0000_00c0

`define InterruptRouting       32'h0000_0100

// -----------------------------------------------------------------------------
// LIST MEMORY RANGES
// -----------------------------------------------------------------------------

`define MAXEPD           40
`define MAXTD            40
`define HccaMem_Begin    14'h 0000
`define HccaMem_End      14'h 00FF
`define DummyEDMem_Begin 14'h 0100
`define DummyEDMem_End   14'h 02FF
`define IsoEDMem_Begin   14'h 0300
`define IsoEDMem_End     14'h 06FF
`define IntEDMem_Begin   14'h 0700
`define IntEDMem_End     14'h 0AFF
`define BulkEDMem_Begin  14'h 0B00
`define BulkEDMem_End    14'h 0EFF
`define CtrlEDMem_Begin  14'h 0F00
`define CtrlEDMem_End    14'h 12FF
`define IsoTDMem_Begin   14'h 1300
`define IsoTDMem_End     14'h 22FF
`define IntTDMem_Begin   14'h 2300
`define IntTDMem_End     14'h 2AFF
`define BulkTDMem_Begin  14'h 2B00
`define BulkTDMem_End    14'h 32FF
`define CtrlTDMem_Begin  14'h 3300
`define CtrlTDMem_End    14'h 3AFF

`define IsoED_Size       12'h 10
`define IntED_Size       12'h 10
`define BulkED_Size      12'h 10
`define CtrlED_Size      12'h 10
`define IsoTD_Size       12'h 40
`define IntTD_Size       12'h 20
`define BulkTD_Size      12'h 20
`define CtrlTD_Size      12'h 20

`define IsoED_Offset     12'h  0
`define IntED_Offset     12'h  0
`define BulkED_Offset    12'h  0
`define CtrlED_Offset    12'h  0
`define IsoTD_Offset     12'h 20
`define IntTD_Offset     12'h 10
`define BulkTD_Offset    12'h 10
`define CtrlTD_Offset    12'h 10


`define IsoED            3'h 0
`define IntED            3'h 1
`define BulkED           3'h 2
`define CtrlED           3'h 3
`define IsoTD            3'h 4
`define IntTD            3'h 5
`define BulkTD           3'h 6
`define CtrlTD           3'h 7

`define NUMBER_OF_DATASLV      3'h5
`define BIGGEST_DATASLV_SIZE   15'h4000
`define PAGE                   15'h1000

// -----------------------------------------------------------------------------
// HYDRA TEST MODE
// -----------------------------------------------------------------------------
//`define LP  listproc_bfm

`define SIE_TEST_MODE		32'h 8000_0000  // 31
`define TM_DB_BYPASS_SIE	32'h 4000_0000  // 30
`define CNTR_TEST_MODE		32'h 2000_0000  // 29
`define CLK12_OVERDRIVE		32'h 1000_0000  // 28
`define SPEED_TEST_MODE		32'h 0800_0000  // 27
`define TEST_IO_ENABLE		32'h 0400_0000  // 26
`define TM_DB_NOWRITE		32'h 0200_0000  // 25
`define TM_DB_COUNT		32'h 0100_0000  // 24
`define TM_LP			32'h 0080_0000  // 23
`define TM_FM1			32'h 0040_0000  // 22
`define TM_FM2			32'h 0020_0000  // 21

// -----------------------------------------------------------------------------
// MODULE PATH MACROS
// -----------------------------------------------------------------------------

`define simlib          simkernel
// -----------------------------------------------------------------------------
// STIMULUS MACROS
// -----------------------------------------------------------------------------


// --- PCI MASTER INSTANTIATION
// -----------------------------------------------------------------------------
// --- PCI SLAVE INSTANTIATION
// -----------------------------------------------------------------------------

// PCI Slave BFM Definitions
`define  CLOCK      2'h0
`define  ADDRESS    2'h1
`define  RETRY      2'b00
`define  DISCONNECT 2'b01
`define  ABORT      2'b10
`define  CLEARTERM  2'b11

`define  CHKRD      1'b1
`define  NOCHKRD    1'b0
`define  WRITE      1'b1
`define  READ       1'b0

// PCI Master BFM Definitions
`define  MEM             32'b11111111_11111111_11111111_11111111
`define  IO              32'b11111111_11111111_11111111_11111110
`define  CONFG           32'b11111111_11111111_11111111_11111101
`define  BCK2BCK         32'b11111111_11111111_11111110_11111111
`define  BYTELANE        32'b11111111_11111111_11110111_11111111
`define  PACK            32'b11111111_11111111_11101111_11111111

`define  DATA_PARITY	 32'b11111111_11110111_11111111_11111111
`define  ADDR_PARITY	 32'b11111111_11111011_11111111_11111111

`define  PERR   	 32'b11111111_11011111_11111111_11111111
`define  SERR   	 32'b11111111_10111111_11111111_11111111


// USB Definitions

`define  CONTROL      2'b00
`define  BULK         2'b01
`define  INTERRUPT    2'b10
`define  ISOCHRONOUS  2'b11
`define  DTD0       2'b00
`define  DOUT       2'b01
`define  DIN        2'b10
`define  DTD3       2'b11
`define  SKIP       1'b1
`define  NOSKIP     1'b0
`define  HALT       1'b1
`define  NOHALT     1'b0
`define  FREQ_1     6'h01
`define  FREQ_2     6'h02
`define  FREQ_4     6'h04
`define  FREQ_8     6'h08
`define  FREQ_16    6'h10
`define  FREQ_32    6'h20
`define  SECT_0     0
`define  SECT_1     1
`define  SECT_2     2
`define  SECT_3     3
`define  SECT_4     4
`define  SECT_5     5
`define  SECT_6     6
`define  SECT_7     7
`define  SECT_8     8
`define  SECT_9     9
`define  SECT_10    10
`define  SECT_11    11
`define  SECT_12    12
`define  SECT_13    13
`define  SECT_14    14
`define  SECT_15    15
`define  SECT_16    16
`define  SECT_17    17
`define  SECT_18    18
`define  SECT_19    19
`define  SECT_20    20
`define  SECT_21    21
`define  SECT_22    22
`define  SECT_23    23
`define  SECT_24    24
`define  SECT_25    25
`define  SECT_26    26
`define  SECT_27    27
`define  SECT_28    28
`define  SECT_29    29
`define  SECT_30    30
`define  SECT_31    31
`define  EP_0       4'h0
`define  EP_1       4'h1
`define  EP_2       4'h2
`define  EP_3       4'h3
`define  EP_4       4'h4
`define  EP_5       4'h5
`define  EP_6       4'h6
`define  EP_7       4'h7
`define  EP_8       4'h8
`define  EP_9       4'h9
`define  EP_10      4'ha
`define  EP_11      4'hb
`define  EP_12      4'hc
`define  EP_13      4'hd
`define  EP_14      4'he
`define  EP_15      4'hf
`define  PORT_1     1
`define  PORT_2     2
`define  PORT_3     3
`define  PORT_4     4
`define  PORT_5     5
`define  PORT_6     6
`define  PORT_7     7
`define  PORT_8     8
`define  PORT_9     9
`define  PORT_10    10
`define  PORT_11    11
`define  PORT_12    12
`define  PORT_13    13
`define  PORT_14    14
`define  PORT_15    15

// endpoint descriptor address

`define  EDCONTROL  4'h0
`define  EDTAILP    4'h4
`define  EDHEADP    4'h8
`define  EDNEXTED   4'hc

// general and isoc transfer descriptor address

`define  TDCONTROL  4'h0
`define  TDBP       4'h4
`define  TDNEXTTD   4'h8
`define  TDBE       4'hc

// Define fields used by HCD to keep track of information necessary to
// process completed TDs. Note that the following addresses are
// SUBTRACTED from the base address of the TD. Hence, the actual
// memory required is 6 Dwords for a GTD and 10 Dwords for an ITD. To
// keep the hardware portion of the TD cacheline aligned, round GTD to
// 8 Dwords and ITD to 16 Dwords.

`define  HCD_TD_MEMSTART     8'h04
`define  HCD_TD_EPADDR       8'h08
`define  HCD_TD_FORMAT       8'h0c

`define  HCD_TD_OFFSET1_0    8'h14
`define  HCD_TD_OFFSET3_2    8'h18
`define  HCD_TD_OFFSET5_4    8'h1c
`define  HCD_TD_OFFSET7_6    8'h20

// isochronous transfer descriptor address

`define  TDOFFSET1_0   8'h10
`define  TDOFFSET3_2   8'h14
`define  TDOFFSET5_4   8'h18
`define  TDOFFSET7_6   8'h1c

// GTD specifics
`define  DSETUP     2'b00
`define  EXACTBUF   1'b0
`define  NOEXACTBUF 1'b1

// Completion Codes
`define CC_ACK                4'h0
`define CC_CRC                4'h1
`define CC_STUFF              4'h2
`define CC_DATA_TOGGLE        4'h3
`define CC_STALL              4'h4
`define CC_TIMEOUT            4'h5
`define CC_PID_CHECK          4'h6
`define CC_INVALID_PID        4'h7
`define CC_PACKET_OVERRUN     4'h8
`define CC_PACKET_UNDERRUN    4'h9
`define CC_BUFFER_OVERRUN     4'hc
`define CC_BUFFER_UNDERRUN    4'hd
`define CC_EOF                4'he
`define CC_NAK                4'hf

`define SETUP                 2'b00
`define OUT                   2'b01
`define IN                    2'b10
`define ISOC                  1'b1
`define NON_ISOC              1'b0
`define FS                    1'b0
`define LS                    1'b1
`define DATA0                 1'b0
`define DATA1                 1'b1
`define OVERRUN               1'b1
`define UNDERRUN              1'b1
`define ZERO_BYTE             1'b1
`define J_STATE               2'b10
`define K_STATE               2'b01
`define SE0_STATE             2'b00

// Interrupt Definitions
`define SchedulingOverrun     32'h0000_0001
`define WritebackDoneHead     32'h0000_0002
`define StartOfFrame          32'h0000_0004
`define ResumeDetected        32'h0000_0008
`define UnrecoverableError    32'h0000_0010
`define FrameNumberOverflow   32'h0000_0020
`define RootHubStatusChange   32'h0000_0040
`define OwnershipChange       32'h4000_0000
`define MasterInterrupt       32'h8000_0000

// Null
`define NULL                  32'h0000_0000

// Hub/Port Control Command definitions

`define ClearPortEnable       32'h0000_0001
`define SetPortEnable         32'h0000_0002
`define SetPortSuspend        32'h0000_0004
`define ClearPortSuspend      32'h0000_0008
`define ClearPortSuspend      32'h0000_0008
`define SetPortReset          32'h0000_0010
`define SetPortPower          32'h0000_0100
`define ClearPortPower        32'h0000_0200
`define ClearConnectStatusChange      32'h0001_0000
`define ClearPortEnableStatusChange   32'h0002_0000
`define ClearPortSuspendStatusChange  32'h0004_0000
`define ClearPortOverCurrentIndicatorChange 32'h0008_0000
`define ClearPortResetStatusChange   32'h0010_0000
`define ClearGlobalPower      32'h0000_0001
`define SetGlobalPower        32'h0001_0000
`define ClearOverCurrentIndicatorChange     32'h0002_0000
`define SetRemoteWakeupEnable 32'h0000_8000
`define ClearRemoteWakeupEnable             32'h8000_0000

// HcRhPortStatus Field definitions

`define CurrentConnectStatus             32'h0000_0101
`define PortEnableStatus                 32'h0000_0103
`define PortSuspendStatus                32'h0000_0107
`define PortOverCurrentIndicator         32'h0000_0008
`define PortResetStatus                  32'h0000_0113
`define PortPowerStatus                  32'h0000_0100
`define LowSpeedDeviceAttached           32'h0000_0200
`define ConnectStatusChange              32'h0001_0000
`define PortEnableStatusChange           32'h0002_0000
`define PortSuspendStatusChange          32'h0004_0000
`define PortOverCurrentIndicatorChange   32'h0008_0000
`define PortResetStatusChange            32'h0010_0000

// HcRhStatus Field Definitions

`define OverCurrentIndicator             32'h0000_0002
`define OverCurrentIndicatorChange       32'h0002_0000
`define DeviceRemoteWakeupEnable         32'h0000_8000

// Root Hub Descriptor Register Field Definitions

`define NumberDownstreamPorts            32'h0000_0002
`define PowerOnToPowerGoodTime           32'h0100_0000
`define IndividualSwitching              32'h0000_0100
`define NoPowerSwitching                 32'h0000_0200
`define IndividualOverCurrent            32'h0000_0800
`define NoOverCurrent                    32'h0000_1000
`define GlobalSwitching                  32'h0000_0000
`define GlobalOverCurrent                32'h0000_0000

`define DeviceRemovable_P1               32'h0000_0002
`define DeviceRemovable_P2               32'h0000_0004
`define DeviceRemovable_P3               32'h0000_0008
`define DeviceRemovable_P4               32'h0000_0010
`define DeviceRemovable_P5               32'h0000_0020
`define DeviceRemovable_P6               32'h0000_0040
`define DeviceRemovable_P7               32'h0000_0080
`define DeviceRemovable_P8               32'h0000_0100
`define DeviceRemovable_P9               32'h0000_0200
`define DeviceRemovable_P10              32'h0000_0400
`define DeviceRemovable_P11              32'h0000_0800
`define DeviceRemovable_P12              32'h0000_1000
`define DeviceRemovable_P13              32'h0000_2000
`define DeviceRemovable_P14              32'h0000_4000
`define DeviceRemovable_P15              32'h0000_8000

`define PortPowerControlMask_P1          32'h0002_0000
`define PortPowerControlMask_P2          32'h0004_0000
`define PortPowerControlMask_P3          32'h0008_0000
`define PortPowerControlMask_P4          32'h0010_0000
`define PortPowerControlMask_P5          32'h0020_0000
`define PortPowerControlMask_P6          32'h0040_0000
`define PortPowerControlMask_P7          32'h0080_0000
`define PortPowerControlMask_P8          32'h0100_0000
`define PortPowerControlMask_P9          32'h0200_0000
`define PortPowerControlMask_P10         32'h0400_0000
`define PortPowerControlMask_P11         32'h0800_0000
`define PortPowerControlMask_P12         32'h1000_0000
`define PortPowerControlMask_P13         32'h2000_0000
`define PortPowerControlMask_P14         32'h4000_0000
`define PortPowerControlMask_P15         32'h8000_0000

// USB BFM definitions
//`define usb1                  system.usb1
//`define usb2                  system.usb2
`define usb1                  top.usb1
`define usb2                  top.usb2
`define REMOVE                2'b01
`define ADD                   2'b00
`define BfmErrorOff           1
`define BfmError2Status       1

// USB BFM Endpoint Error Types
`define SetBitStuffSync       12'h101
`define SetBitStuffPid        12'h102
`define SetBitStuffData       12'h104
`define SetBitStuffCrc        12'h108
`define SetInvalidSync        12'h110
`define SetInvalidPid         12'h120
`define SetInvalidPidCheck    12'h140
`define SetInvalidCrc         12'h180
`define ClearBitStuffSync     12'h001
`define ClearBitStuffPid      12'h002
`define ClearBitStuffData     12'h004
`define ClearBitStuffCrc      12'h008
`define ClearInvalidSync      12'h010
`define ClearInvalidPid       12'h020
`define ClearInvalidPidCheck  12'h040
`define ClearInvalidCrc       12'h080

// init_TD_buffer defines
`define INC_MEM		4'h 0
`define DEC_MEM		4'h 1
`define RAND_MEM	4'h 2
`define FF_MEM 		4'h 3
`define WALK0_MEM 	4'h 4
`define DEC_2BYTE_MEM 	4'h 5

// interrupt wait time limit control
//`define INT_WAIT_TIME   300000  // 6/18/1996 KuoBa
`define INT_WAIT_TIME   8700000  // 6/18/1996 KuoBa; 8/14/1996 revised due to test cycle by wyw

`define HcRhPortStatus        `HcRhPortStatus1
`define usb                   `usb1
`define portn                 `port1n
`define portp                 `port1p
`define PortPowerControlMask  `PortPowerControlMask_P1
`define PORT                  `PORT_1

`define HcOpReg               32'h3700_0000

`define HcRevision           `HcOpReg+8'h00
`define HcControl            `HcOpReg+8'h04
`define HcCommandStatus      `HcOpReg+8'h08
`define HcInterruptStatus    `HcOpReg+8'h0c
`define HcInterruptEnable    `HcOpReg+8'h10
`define HcInterruptDisable   `HcOpReg+8'h14
`define HcHCCA               `HcOpReg+8'h18
`define HcPeriodCurrentED    `HcOpReg+8'h1c
`define HcControlHeadED      `HcOpReg+8'h20
`define HcControlCurrentED   `HcOpReg+8'h24
`define HcBulkHeadED         `HcOpReg+8'h28
`define HcBulkCurrentED      `HcOpReg+8'h2c
`define HcDoneHead           `HcOpReg+8'h30
`define HcFmInterval         `HcOpReg+8'h34
`define HcFmRemaining        `HcOpReg+8'h38
`define HcFmNumber           `HcOpReg+8'h3c
`define HcPeriodicStart      `HcOpReg+8'h40
`define HcLSThreshold        `HcOpReg+8'h44
`define HcRhDescriptorA      `HcOpReg+8'h48
`define HcRhDescriptorB      `HcOpReg+8'h4c
`define HcRhStatus           `HcOpReg+8'h50
`define HcRhPortStatus1      `HcOpReg+8'h54
`define HcRhPortStatus2      `HcOpReg+8'h58

`define BitStuff          0
`define TokenCrc          1
`define DataCrc           2
`define DifferentialData  3
`define SyncPattern       4
`define PidCheck          5
`define InvalidPid        6
`define TokenTooLong      7
`define HandshakeTooLong  8
`define TarTooShort       9
`define TarTooLong        10
`define NoCrcInData       11
`define AckAfterData      12
`define IncorrectStall    14
`define IncorrectNak      13
`define BadHandshake      16
`define HandshakeOnIsoc   17
`define DataOnInactivePort 15
`define ResumeTooShort    19
`define BadTypeInHandshake 20

