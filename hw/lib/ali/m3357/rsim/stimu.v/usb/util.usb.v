task USB_Gate_Clock_Disable;
reg [31:0] sysio_reg_60h;
begin
	`usb_cpu_readbk1w(CPU_IOBase+32'h0000_0060, 4'hf, sysio_reg_60h);
	sysio_reg_60h = sysio_reg_60h | {8'h00, 8'b0000_1000, 8'h00, 8'h00};	// bit19=1 will make usb power down
	`usb_cpu_write1w(CPU_IOBase+32'h0000_0060, 4'hf, sysio_reg_60h);
	$display ("\n------- !!!! USB Gate Clock Disable !!!!!!!-----------\n");
end
endtask

task USB_Gate_Clock_Enable;
reg [31:0] sysio_reg_60h;
begin
	`usb_cpu_readbk1w(CPU_IOBase+32'h0000_0060, 4'hf, sysio_reg_60h);
	sysio_reg_60h = sysio_reg_60h & {8'hff, 8'b1111_0111, 8'hff, 8'hff};	// bit19=1 will make usb power down
	`usb_cpu_write1w(CPU_IOBase+32'h0000_0060, 4'hf, sysio_reg_60h);
	$display ("\n------- !!!! USB Gate Clock Enable !!!!!!!-----------\n");
end
endtask

task USB_Initialize;
begin
end
endtask

task usb_int_processor;
begin
`ifdef NO_CPU
	$display ("\Mask USB int...");
	int_cpu_write1w(CPU_IOBase + 32'h3c, 4'b1111, 32'h0000_0000);
    #1000;
	int_cpu_read1w (CPU_IOBase + 32'h38,4'b0100,32'h0000_0000);
   $display("\n---- disable SOF int");
	int_cpu_write1w   (32'h0a00_0014, 4'hf, 32'h0000_0000);

`endif
end
endtask

`ifdef GSIM
`else
//annotate by yuky,
//	parameter       EP_DESC_WIDTH = 57;     // width of endpoint descriptor
//	parameter       EP_ID_WIDTH = 43;       // width of endpoint id :=
//
//	integer         valid_ep_list;
//
//	initial
//	  begin
//	//    valid_ep_list = $mlist2Init( EP_DESC_WIDTH, EP_ID_WIDTH);
//	  end
//
//
//	task program_usb;
//
//	/*   Modified for ALi HCI   ... MD Chen 04/08      */
//	/*******************************************************
//	This task programs a USB device
//	ex: program_usb(`ADD,`PORT_1,`EP_0,20,`FS,`BULK,16,`DATA0);
//	*******************************************************/
//
//	  input [1:0] action;
//	  input [3:0] port_num;
//	  input [3:0] ep_num;
//	  input [6:0] function_adr;
//	  input speed;
//	  input isoc;
//	//  input [10:0] maxpsize;
//	  input [9:0] maxpsize;
//	  input datatoggle;
//
//	  reg isoc;
//
//	  begin
//
//	// program usb device
//
//	    //
//	    // configure the endpoint in the usb_watcher
//	    //
//	    if (action === 2'b00)
//	      begin
//	        SetEndpoint( port_num, function_adr, ep_num, speed, isoc,
//	                                  datatoggle, maxpsize);
//	      end
//	    else if (action === 2'b01)
//	      begin
//	        RemoveEndpoint( port_num, function_adr, ep_num);
//	      end
//
//	    case (port_num)
//	//      1: usb1.Endpoint(action,{ep_num,function_adr,speed,isoc,maxpsize,datatoggle});
//	//      2: usb2.Endpoint(action,{ep_num,function_adr,speed,isoc,maxpsize,datatoggle});
//
//	      default: begin
//	                 `ifdef GMDR_VERSION
//	                 $GDisplay(`GWARNING,ModuleName,ModuleRev,"Attempt to program a non-exist usb device.");
//	                 `else
//	                     $display("Attempt to program a non-exist usb device.");
//	                 `endif
//	               end
//	    endcase
//
//	  end
//	endtask
//
//	task RemoveEndpoint;
//	input   [31:0]          port;
//	input   [6:0]           address;
//	input   [3:0]           endpnt;
//
//	begin
//	  rm_endpoint( port, address, endpnt);
//	end
//	endtask
//
//
//	task SetEndpoint;
//	input   [31:0]          port;
//	input   [6:0]           address;
//	input   [3:0]           endpnt;
//	input                   speed;
//	input                   isoc;
//	input                   data_tgl;
//	input   [9:0]           max_size;
//
//	begin
//	  set_endpoint( port, address, endpnt, speed, isoc, data_tgl, max_size);
//	end
//	endtask
//
//
//	task rm_endpoint;
//	input   [31:0]          port;
//	input   [6:0]           address;
//	input   [3:0]           endpnt;
//
//	integer                 ref;
//
//	begin
//	//  ref = $mlist2Flush( valid_ep_list, 6'h08, {port, address, endpnt});
//	end
//	endtask
//
//	task set_endpoint;
//	input   [31:0]          port;
//	input   [6:0]           address;
//	input   [3:0]           endpnt;
//	input                   speed;
//	input                   isoc;
//	input                   data_tgl;
//	input   [9:0]           max_size;
//
//	reg                     err_config;
//	integer                 ref;
//
//	begin
//	  err_config = 1'b0;
//	//  ref = $mlist2Write( valid_ep_list, 6'h08, {port, address, endpnt,
//	//                      speed, isoc, data_tgl, max_size, err_config});
//	end
//	endtask
//
//	parameter       num_rules = 22;
//	reg    [ num_rules - 1:0 ]      RuleControl;
//
//	task rules_switch;
//	input   [31:0]          rule;
//	input                   value;
//
//	begin
//	  if (rule > num_rules)
//	    begin
//	    end
//	  else
//	    RuleControl[ rule ] = value;
//	end
//	endtask
//
//	`define CLK             top.chip.core.t2_chipset.p_biu.hydra_core.I_CLK48
//	`define IRDYJ           top.chip.core.t2_chipset.p_biu.hydra_core.I_IRDYJ
//	`define TRDYJ           top.chip.core.t2_chipset.p_biu.hydra_core.I_TRDYJ
//	`define C_BEJ           top.chip.core.t2_chipset.p_biu.hydra_core.I_CBEJ
//	`define PCI_AD          top.chip.core.t2_chipset.p_biu.hydra_core.O_AD
//
//
//	task snoop_pci_cycle;
//
//	  input  [31:0] addr;
//	  input         direction;      // 1 = write
//	  output [31:0] data;
//
//	  reg          done;
//	  reg          cycle_hit;
//
//	  begin
//
//	    done = 0;
//	    cycle_hit = 0;
//	    while (!done)
//	      begin
//	        @(posedge `CLK);
//	          if (`IRDYJ && `TRDYJ &&
//	             (`PCI_AD == addr) &&
//	             (`C_BEJ[0] == direction))  cycle_hit = 1;
//	        if (!`IRDYJ && !`TRDYJ && (cycle_hit == 1))
//	        begin
//	            done = 1;
//	            data = `PCI_AD;
//	        end
//	      end
//	  end
//	endtask
//
//
//	task err_config_usb;
//
//	/*   Modified for ALi HCI   ... MD Chen 04/08      */
//	/*******************************************************
//	This task configures an Endpoint with an error conditions
//	in both the BFM and BW.
//	ex: err_config_usb(`SetInvalidPid,16'h0000,`EP_0,7'h00,`PORT_1);
//	*******************************************************/
//
//	  input [11:0] err_type;
//	  input [15:0] pattern;
//	  input [3:0] ep_num;
//	  input [6:0] function_adr;
//	  input [3:0] port_num;
//
//	  begin
//
//	// configure usb device
//
//	    EndpErrConfig( port_num, function_adr, ep_num, err_type[8]);
//	    case (port_num)
//	//      1: top.usb1.Err( err_type, pattern, {ep_num, function_adr});
//	//      2: top.usb2.Err( err_type, pattern, {ep_num, function_adr});
//	        default: begin
//
//	                     $display("Attempt to configure a non-existant usb port.");
//
//	               end
//	    endcase
//
//	  end
//	endtask
//
//	task EndpErrConfig;
//	input   [31:0]          port;
//	input   [6:0]           address;
//	input   [3:0]           endpnt;
//	input                   state;
//
//	begin
//	  set_err_config( port, address, endpnt, state);
//	end
//	endtask
//
//
//	task set_err_config;
//	input   [31:0]          port;
//	input   [6:0]           address;
//	input   [3:0]           endpnt;
//	input                   state;
//
//	reg                     speed;
//	reg                     isoc;
//	reg                     data_tgl;
//	reg  [9:0]              max_size;
//	reg                     err_config;
//	integer                 ref;
//
//	begin
//	//  ref = $mlist2Read( valid_ep_list, 6'h08, port, address, endpnt,
//	//                     speed, isoc, data_tgl, max_size, err_config);
//	//  ref = $mlist2Write( valid_ep_list, 6'h08, {port, address, endpnt},
//	//                      {speed, isoc, data_tgl, max_size, state});
//	end
//	endtask
//
//	task snoop_pci_cyclen;
//
//	  input  [31:0] addr;
//	  input         cycle_num;
//	  input         direction;      // 1 = write
//	  output [31:0] data;
//
//	  reg          done;
//	  reg          cycle_hit;
//	  reg          cycle_cnt;
//
//	  begin
//
//	    done = 0;
//	    cycle_hit = 0;
//	    cycle_cnt = 1;
//	    while (!done)
//	      begin
//	        @(posedge `CLK);
//	        if (`IRDYJ && `TRDYJ &&
//	             (`PCI_AD == addr) &&
//	             (`C_BEJ[0] == direction))  cycle_hit = 1;
//	        if (!`IRDYJ && !`TRDYJ && (cycle_hit == 1))
//	        begin
//	            if (cycle_cnt === cycle_num)
//	            begin
//	                done = 1;
//	                data = `PCI_AD;
//	            end
//	            else
//	                cycle_cnt = cycle_cnt + 1;
//	        end
//	      end
//	  end
//	endtask
//
	reg pwren_status_enable;
//	//reg o_ovrcur;
//	reg ovrcur_enable;
//
//	//initial o_ovrcur = 1;     // 4/3/1997 KuoBa
//	initial ovrcur_enable = 1;
	initial pwren_status_enable = 0;
//
//
	task enable_pwren_status;
	begin
	    pwren_status_enable = 1;
	end
	endtask
//
//
	`ifdef	POST_GSIM
		`define clk48             top.CHIP.CORE.T2_CHIPSET.P_BIU.HYDRA_CORE.I_CLK48
	`else
		`define clk48             top.CHIP.CORE.T2_CHIPSET.P_BIU.HYDRA_CORE.I_CLK48
	`endif
	task set_ovrcur;
	begin
	    @(`clk48);
	    $display("OVRCUR set");     // 6/18/1996 KuoBa change `display to $display
	    // o_ovrcur = 1;
	    usb_ovc = 0;   // 4/3/1997 KuoBa
	end
	endtask

	task clear_ovrcur;
	begin
	    @(`clk48);
	    $display("OVRCUR cleared");     // 6/18/1996 KuoBa change `display to $display
	    // o_ovrcur = 0;
	    usb_ovc = 1;   // 4/3/1997 KuoBa
	end
	endtask
//	`define port1n          top.usb1_dn
//	`define port1p          top.usb1_dp
//	`define port2n          top.usb2_dn
//	`define port2p          top.usb2_dp
//
//	function usb_port_state_check;
//	input [03:0] port_num;
//	input [01:0] port_state;
//	input        port_speed;
//	reg   [01:0] bus_state;
//	begin
//	    usb_port_state_check = 0;
//	    case (port_num)
//	         1 : if (port_speed) bus_state = {`port1n, `port1p};
//	                   else bus_state = {`port1p, `port1n};
//	         2  : if (port_speed) bus_state = {`port2n, `port2p};
//	                   else bus_state = {`port2p, `port2n};
//	        default  : begin
//	                            $display("Attempt to check a non-existant usb port.");
//	                   end
//	    endcase
//	    if ( port_state != bus_state )
//	             case (bus_state)
//	                 2'b10    : begin
//	                                   $display("Error:  USB Port Bus State Check Falure.");
//	                                   $display("        Detected J");
//	                               end
//	                 2'b01    : begin
//	                                   $display("Error:  USB Port Bus State Check Falure.");
//	                                   $display("        Detected K");
//	                               end
//	                 2'b00  : begin
//	                                   $display("Error:  USB Port Bus State Check Falure.");
//	                                   $display("        Detected SE0");
//	                               end
//	             endcase
//
//
//	   else usb_port_state_check = 1;
//
//	end
//	endfunction
//
//	reg    [ 7:0 ]                  data            [ 4:1 ];
//	integer                         se0_cnt         [ 4:1 ];
//	integer                         bit_cnt         [ 4:1 ];
//	integer                         timer_cnt       [ 4:1 ];
//	integer                         stuff_cnt       [ 4:1 ];
//
//	reg   [8*25:1]   ModuleRev;      // Revision of Module
//	reg                             sel_fs_ls;
//	reg                             error_endp;
//	reg                             XNprog;
//	reg                             LsXNprog;
//	reg                             Xend;
//	reg    [ 4:1 ]          act_list;
//	reg    [ 4:1 ]          timer_en;
//	reg    [ 4:1 ]          ls_en;
//	reg    [ 4:1 ]          sync;
//	reg    [ 4:1 ]          packet_unknown;
//	reg    [ 4:1 ]          invalid;
//	reg    [ 4:1 ]          port_speed;       // 1 = LS, 0 = FS
//	reg    [ 4:1 ]          connected;
//	reg    [ 4:1 ]          resume;
//	reg    [ 4:1 ]          eop;
//
//	time                            half_clk;
//	initial
//	  begin : startup_values
//	    integer     j;
//
//	    half_clk = 10;
//	  end
//
//	task WatcherReset;
//
//	integer         j;
//
//	begin
//	  $display("--- %0s --- USB_BW: %0s : USB watcher reset task\n",
//	           "Info", ModuleRev, "*** %0t: %m\n",$time,
//	           "--- End ---\n");
//	  half_clk = 10;
//	  sel_fs_ls = 1'b1;
//	  ls_en = 0;
//	  error_endp = 1'b0;
//	  XNprog = 1'b0;
//	  LsXNprog = 1'b0;
//	  Xend = 1'b0;
//	  for (j = 1; j <= 4; j = j + 1)
//	    begin
//	      act_list[ j ] = 1'b0;
//	      timer_en[ j ] = 1'b0;
//	      sync[ j ] = 1'b0;
//	      eop[ j ] = 1'b0;
//	      packet_unknown[ j ] = 1'b0;
//	      stuff_cnt[ j ] = 0;
//	      timer_cnt[ j ] = 0;
//	      bit_cnt[ j ] = 0;
//	      se0_cnt[ j ] = 0;
//	      data[ j ] = 8'hz;
//	      invalid[ j ] = 1'b0;
//	      resume[ j ] = 1'b0;
//	    end
//	end
//	endtask

`endif
