// Top module for SB of m6303 RTL simulation
// Author:	Yuky 	
// Date:	2001-03-19 initial version
// Description:	CPU core + North bridge + USB
// History:	2001-03-28 copy from the top.v of m6301

// Note : the publish part is in the top.nb.v including the USB signals define
//============== USB begin here ================================
integer	usb_fn;
initial	usb_fn = $fopen("usbdev.rpt");

usbdev usb1
               (  .dmns ( usb1_dn	),
                  .dpls ( usb1_dp	));


usbdev usb2
               (  .dmns ( usb2_dn	),
                  .dpls ( usb2_dp	)); 
