/**************************************************************
//	File: cmdu.v
//	Description: for m3357 project. Because the tasks number of
//				 cpu behavior model is much. All the basic tasks
//				 are saved in the cpu.cmdu.v
//				 Here is just to redefine the tasks name for
//				 use convenience.
//	History:	 2002-06-10 initial version yuky
//			 2002-06-18 add the tasks for cpu access sdram memory, Figo
//			 2002-06-19 Move the access sdram memory tasks to util.v, yuky
***************************************************************/
`define cpu_write1w      pro00_cpu_write1w
`define cpu_write2w      pro00_cpu_write2w
`define cpu_write4w      pro00_cpu_write4w
`define cpu_readbk1w     pro00_cpu_readbk1w
`define cpu_readbk2w     pro00_cpu_readbk2w
`define cpu_readbk4w     pro00_cpu_readbk4w
`define cpu_read1w       pro00_cpu_read1w
`define cpu_read2w       pro00_cpu_read2w
`define cpu_read4w       pro00_cpu_read4w


`define	nb_cpu_write1w		pro01_cpu_write1w
`define	nb_cpu_write2w    	pro01_cpu_write2w
`define	nb_cpu_write4w    	pro01_cpu_write4w
`define	nb_cpu_write8w    	pro01_cpu_write8w
`define	nb_cpu_readbk1w   	pro01_cpu_readbk1w
`define	nb_cpu_readbk2w   	pro01_cpu_readbk2w
`define	nb_cpu_readbk4w   	pro01_cpu_readbk4w
`define	nb_cpu_readbk8w   	pro01_cpu_readbk8w
`define	nb_cpu_read1w     	pro01_cpu_read1w
`define	nb_cpu_read2w     	pro01_cpu_read2w
`define	nb_cpu_read4w     	pro01_cpu_read4w
`define	nb_cpu_read8w     	pro01_cpu_read8w

`define	ram_cpu_write1w		pro02_cpu_write1w
`define	ram_cpu_write2w    	pro02_cpu_write2w
`define	ram_cpu_write4w    	pro02_cpu_write4w
`define	ram_cpu_write8w    	pro02_cpu_write8w
`define	ram_cpu_readbk1w   	pro02_cpu_readbk1w
`define	ram_cpu_readbk2w   	pro02_cpu_readbk2w
`define	ram_cpu_readbk4w   	pro02_cpu_readbk4w
`define	ram_cpu_readbk8w   	pro02_cpu_readbk8w
`define	ram_cpu_read1w     	pro02_cpu_read1w
`define	ram_cpu_read2w     	pro02_cpu_read2w
`define	ram_cpu_read4w     	pro02_cpu_read4w
`define	ram_cpu_read8w     	pro02_cpu_read8w

`define	rom_cpu_write1w		pro03_cpu_write1w
`define	rom_cpu_write2w    	pro03_cpu_write2w
`define	rom_cpu_write4w    	pro03_cpu_write4w
`define	rom_cpu_write8w    	pro03_cpu_write8w
`define	rom_cpu_readbk1w   	pro03_cpu_readbk1w
`define	rom_cpu_readbk2w   	pro03_cpu_readbk2w
`define	rom_cpu_readbk4w   	pro03_cpu_readbk4w
`define	rom_cpu_readbk8w   	pro03_cpu_readbk8w
`define	rom_cpu_read1w     	pro03_cpu_read1w
`define	rom_cpu_read2w     	pro03_cpu_read2w
`define	rom_cpu_read4w     	pro03_cpu_read4w
`define	rom_cpu_read8w     	pro03_cpu_read8w

`define	pci_cpu_write1w		pro04_cpu_write1w
`define	pci_cpu_write2w    	pro04_cpu_write2w
`define	pci_cpu_write4w    	pro04_cpu_write4w
`define	pci_cpu_write8w    	pro04_cpu_write8w
`define	pci_cpu_readbk1w   	pro04_cpu_readbk1w
`define	pci_cpu_readbk2w   	pro04_cpu_readbk2w
`define	pci_cpu_readbk4w   	pro04_cpu_readbk4w
`define	pci_cpu_readbk8w   	pro04_cpu_readbk8w
`define	pci_cpu_read1w     	pro04_cpu_read1w
`define	pci_cpu_read2w     	pro04_cpu_read2w
`define	pci_cpu_read4w     	pro04_cpu_read4w
`define	pci_cpu_read8w     	pro04_cpu_read8w

`define	usb_cpu_write1w		pro05_cpu_write1w
`define	usb_cpu_write2w    	pro05_cpu_write2w
`define	usb_cpu_write4w    	pro05_cpu_write4w
`define	usb_cpu_write8w    	pro05_cpu_write8w
`define	usb_cpu_readbk1w   	pro05_cpu_readbk1w
`define	usb_cpu_readbk2w   	pro05_cpu_readbk2w
`define	usb_cpu_readbk4w   	pro05_cpu_readbk4w
`define	usb_cpu_readbk8w   	pro05_cpu_readbk8w
`define	usb_cpu_read1w     	pro05_cpu_read1w
`define	usb_cpu_read2w     	pro05_cpu_read2w
`define	usb_cpu_read4w     	pro05_cpu_read4w
`define	usb_cpu_read8w     	pro05_cpu_read8w

`define	ide_cpu_write1w		pro06_cpu_write1w
`define	ide_cpu_write2w    	pro06_cpu_write2w
`define	ide_cpu_write4w    	pro06_cpu_write4w
`define	ide_cpu_write8w    	pro06_cpu_write8w
`define	ide_cpu_readbk1w   	pro06_cpu_readbk1w
`define	ide_cpu_readbk2w   	pro06_cpu_readbk2w
`define	ide_cpu_readbk4w   	pro06_cpu_readbk4w
`define	ide_cpu_readbk8w   	pro06_cpu_readbk8w
`define	ide_cpu_read1w     	pro06_cpu_read1w
`define	ide_cpu_read2w     	pro06_cpu_read2w
`define	ide_cpu_read4w     	pro06_cpu_read4w
`define	ide_cpu_read8w     	pro06_cpu_read8w

`define	gpu_cpu_write1w		pro07_cpu_write1w
`define	gpu_cpu_write2w    	pro07_cpu_write2w
`define	gpu_cpu_write4w    	pro07_cpu_write4w
`define	gpu_cpu_write8w    	pro07_cpu_write8w
`define	gpu_cpu_readbk1w   	pro07_cpu_readbk1w
`define	gpu_cpu_readbk2w   	pro07_cpu_readbk2w
`define	gpu_cpu_readbk4w   	pro07_cpu_readbk4w
`define	gpu_cpu_readbk8w   	pro07_cpu_readbk8w
`define	gpu_cpu_read1w     	pro07_cpu_read1w
`define	gpu_cpu_read2w     	pro07_cpu_read2w
`define	gpu_cpu_read4w     	pro07_cpu_read4w
`define	gpu_cpu_read8w     	pro07_cpu_read8w

`define	ve_cpu_write1w		pro08_cpu_write1w
`define	ve_cpu_write2w    	pro08_cpu_write2w
`define	ve_cpu_write4w    	pro08_cpu_write4w
`define	ve_cpu_write8w    	pro08_cpu_write8w
`define	ve_cpu_readbk1w   	pro08_cpu_readbk1w
`define	ve_cpu_readbk2w   	pro08_cpu_readbk2w
`define	ve_cpu_readbk4w   	pro08_cpu_readbk4w
`define	ve_cpu_readbk8w   	pro08_cpu_readbk8w
`define	ve_cpu_read1w     	pro08_cpu_read1w
`define	ve_cpu_read2w     	pro08_cpu_read2w
`define	ve_cpu_read4w     	pro08_cpu_read4w
`define	ve_cpu_read8w     	pro08_cpu_read8w

`define	de_cpu_write1w		pro09_cpu_write1w
`define	de_cpu_write2w    	pro09_cpu_write2w
`define	de_cpu_write4w    	pro09_cpu_write4w
`define	de_cpu_write8w    	pro09_cpu_write8w
`define	de_cpu_readbk1w   	pro09_cpu_readbk1w
`define	de_cpu_readbk2w   	pro09_cpu_readbk2w
`define	de_cpu_readbk4w   	pro09_cpu_readbk4w
`define	de_cpu_readbk8w   	pro09_cpu_readbk8w
`define	de_cpu_read1w     	pro09_cpu_read1w
`define	de_cpu_read2w     	pro09_cpu_read2w
`define	de_cpu_read4w     	pro09_cpu_read4w
`define	de_cpu_read8w     	pro09_cpu_read8w

`define	dsp_cpu_write1w		pro10_cpu_write1w
`define	dsp_cpu_write2w    	pro10_cpu_write2w
`define	dsp_cpu_write4w    	pro10_cpu_write4w
`define	dsp_cpu_write8w    	pro10_cpu_write8w
`define	dsp_cpu_readbk1w   	pro10_cpu_readbk1w
`define	dsp_cpu_readbk2w   	pro10_cpu_readbk2w
`define	dsp_cpu_readbk4w   	pro10_cpu_readbk4w
`define	dsp_cpu_readbk8w   	pro10_cpu_readbk8w
`define	dsp_cpu_read1w     	pro10_cpu_read1w
`define	dsp_cpu_read2w     	pro10_cpu_read2w
`define	dsp_cpu_read4w     	pro10_cpu_read4w
`define	dsp_cpu_read8w     	pro10_cpu_read8w

`define	irc_cpu_write1w		pro11_cpu_write1w
`define	irc_cpu_write2w    	pro11_cpu_write2w
`define	irc_cpu_write4w    	pro11_cpu_write4w
`define	irc_cpu_write8w    	pro11_cpu_write8w
`define	irc_cpu_readbk1w   	pro11_cpu_readbk1w
`define	irc_cpu_readbk2w   	pro11_cpu_readbk2w
`define	irc_cpu_readbk4w   	pro11_cpu_readbk4w
`define	irc_cpu_readbk8w   	pro11_cpu_readbk8w
`define	irc_cpu_read1w     	pro11_cpu_read1w
`define	irc_cpu_read2w     	pro11_cpu_read2w
`define	irc_cpu_read4w     	pro11_cpu_read4w
`define	irc_cpu_read8w     	pro11_cpu_read8w

`define	scb_cpu_write1w		pro12_cpu_write1w
`define	scb_cpu_write2w    	pro12_cpu_write2w
`define	scb_cpu_write4w    	pro12_cpu_write4w
`define	scb_cpu_write8w    	pro12_cpu_write8w
`define	scb_cpu_readbk1w   	pro12_cpu_readbk1w
`define	scb_cpu_readbk2w   	pro12_cpu_readbk2w
`define	scb_cpu_readbk4w   	pro12_cpu_readbk4w
`define	scb_cpu_readbk8w   	pro12_cpu_readbk8w
`define	scb_cpu_read1w     	pro12_cpu_read1w
`define	scb_cpu_read2w     	pro12_cpu_read2w
`define	scb_cpu_read4w     	pro12_cpu_read4w
`define	scb_cpu_read8w     	pro12_cpu_read8w

`define	uart_cpu_write1w		pro13_cpu_write1w
`define	uart_cpu_write2w    	pro13_cpu_write2w
`define	uart_cpu_write4w    	pro13_cpu_write4w
`define	uart_cpu_write8w    	pro13_cpu_write8w
`define	uart_cpu_readbk1w   	pro13_cpu_readbk1w
`define	uart_cpu_readbk2w   	pro13_cpu_readbk2w
`define	uart_cpu_readbk4w   	pro13_cpu_readbk4w
`define	uart_cpu_readbk8w   	pro13_cpu_readbk8w
`define	uart_cpu_read1w     	pro13_cpu_read1w
`define	uart_cpu_read2w     	pro13_cpu_read2w
`define	uart_cpu_read4w     	pro13_cpu_read4w
`define	uart_cpu_read8w     	pro13_cpu_read8w

`define	cd_cpu_write1w		pro14_cpu_write1w
`define	cd_cpu_write2w    	pro14_cpu_write2w
`define	cd_cpu_write4w    	pro14_cpu_write4w
`define	cd_cpu_write8w    	pro14_cpu_write8w
`define	cd_cpu_readbk1w   	pro14_cpu_readbk1w
`define	cd_cpu_readbk2w   	pro14_cpu_readbk2w
`define	cd_cpu_readbk4w   	pro14_cpu_readbk4w
`define	cd_cpu_readbk8w   	pro14_cpu_readbk8w
`define	cd_cpu_read1w     	pro14_cpu_read1w
`define	cd_cpu_read2w     	pro14_cpu_read2w
`define	cd_cpu_read4w     	pro14_cpu_read4w
`define	cd_cpu_read8w     	pro14_cpu_read8w

`define	up_cpu_write1w		pro15_cpu_write1w
`define	up_cpu_write2w    	pro15_cpu_write2w
`define	up_cpu_write4w    	pro15_cpu_write4w
`define	up_cpu_write8w    	pro15_cpu_write8w
`define	up_cpu_readbk1w   	pro15_cpu_readbk1w
`define	up_cpu_readbk2w   	pro15_cpu_readbk2w
`define	up_cpu_readbk4w   	pro15_cpu_readbk4w
`define	up_cpu_readbk8w   	pro15_cpu_readbk8w
`define	up_cpu_read1w     	pro15_cpu_read1w
`define	up_cpu_read2w     	pro15_cpu_read2w
`define	up_cpu_read4w     	pro15_cpu_read4w
`define	up_cpu_read8w     	pro15_cpu_read8w

`define	vsb_cpu_write1w		pro16_cpu_write1w
`define	vsb_cpu_write2w    	pro16_cpu_write2w
`define	vsb_cpu_write4w    	pro16_cpu_write4w
`define	vsb_cpu_write8w    	pro16_cpu_write8w
`define	vsb_cpu_readbk1w   	pro16_cpu_readbk1w
`define	vsb_cpu_readbk2w   	pro16_cpu_readbk2w
`define	vsb_cpu_readbk4w   	pro16_cpu_readbk4w
`define	vsb_cpu_readbk8w   	pro16_cpu_readbk8w
`define	vsb_cpu_read1w     	pro16_cpu_read1w
`define	vsb_cpu_read2w     	pro16_cpu_read2w
`define	vsb_cpu_read4w     	pro16_cpu_read4w
`define	vsb_cpu_read8w     	pro16_cpu_read8w

`define	subpict_cpu_write1w		pro17_cpu_write1w
`define	subpict_cpu_write2w    	pro17_cpu_write2w
`define	subpict_cpu_write4w    	pro17_cpu_write4w
`define	subpict_cpu_write8w    	pro17_cpu_write8w
`define	subpict_cpu_readbk1w   	pro17_cpu_readbk1w
`define	subpict_cpu_readbk2w   	pro17_cpu_readbk2w
`define	subpict_cpu_readbk4w   	pro17_cpu_readbk4w
`define	subpict_cpu_readbk8w   	pro17_cpu_readbk8w
`define	subpict_cpu_read1w     	pro17_cpu_read1w
`define	subpict_cpu_read2w     	pro17_cpu_read2w
`define	subpict_cpu_read4w     	pro17_cpu_read4w
`define	subpict_cpu_read8w     	pro17_cpu_read8w

`define	tvenc_cpu_write1w		pro18_cpu_write1w
`define	tvenc_cpu_write2w    	pro18_cpu_write2w
`define	tvenc_cpu_write4w    	pro18_cpu_write4w
`define	tvenc_cpu_write8w    	pro18_cpu_write8w
`define	tvenc_cpu_readbk1w   	pro18_cpu_readbk1w
`define	tvenc_cpu_readbk2w   	pro18_cpu_readbk2w
`define	tvenc_cpu_readbk4w   	pro18_cpu_readbk4w
`define	tvenc_cpu_readbk8w   	pro18_cpu_readbk8w
`define	tvenc_cpu_read1w     	pro18_cpu_read1w
`define	tvenc_cpu_read2w     	pro18_cpu_read2w
`define	tvenc_cpu_read4w     	pro18_cpu_read4w
`define	tvenc_cpu_read8w     	pro18_cpu_read8w

`define	sv_cpu_write1w		pro19_cpu_write1w
`define	sv_cpu_write2w    	pro19_cpu_write2w
`define	sv_cpu_write4w    	pro19_cpu_write4w
`define	sv_cpu_write8w    	pro19_cpu_write8w
`define	sv_cpu_readbk1w   	pro19_cpu_readbk1w
`define	sv_cpu_readbk2w   	pro19_cpu_readbk2w
`define	sv_cpu_readbk4w   	pro19_cpu_readbk4w
`define	sv_cpu_readbk8w   	pro19_cpu_readbk8w
`define	sv_cpu_read1w     	pro19_cpu_read1w
`define	sv_cpu_read2w     	pro19_cpu_read2w
`define	sv_cpu_read4w     	pro19_cpu_read4w
`define	sv_cpu_read8w     	pro19_cpu_read8w
// below 3 tasks for CPU access PCI config space
task	cpu2pci_cfgwr;
input	[7:0]	offset;
input	[4:0]	id_sel;
input	[3:0]	ben_;
input	[2:0]	func;
input	[31:0]	data;
begin
	`pci_cpu_write1w(CPU_IOBase + 32'h2c, 4'hf, {16'h8000, id_sel, func, offset});
	`pci_cpu_write1w(CPU_IOBase + 32'h28, ~ben_, data);
end
endtask

task	cpu2pci_cfgrd;
input	[7:0]	offset;
input	[4:0]	id_sel;
input	[3:0]	ben_;
input	[2:0]	func;
input	[31:0]	data;
begin
//JFR030708
	`pci_cpu_write1w(CPU_IOBase + 32'h2c, 4'hf, {16'h8000, id_sel, func, offset});
	`pci_cpu_read1w(CPU_IOBase + 32'h28, ~ben_, data);
end
endtask

task	cpu2pci_cfgrdbk;
input	[7:0]	offset;
input	[4:0]	id_sel;
input	[3:0]	ben_;
input	[2:0]	func;
output	[31:0]	data;
begin
	`pci_cpu_write1w(CPU_IOBase + 32'h2c, 4'hf, {16'h8000, id_sel, func, offset});
	`pci_cpu_readbk1w(CPU_IOBase + 32'h28, ~ben_, data);
end
endtask




// ------- External PCI master access SoC config space ---------------

task err_report;
input [3:0] command;
input [31:0] addr;
input [3:0] be_;

input [31:0] data_exp;
input [31:0] data_read;

begin
  case (command)
  4'h2: begin
       $display("Error_T2_rsim: %0t ERR in IORD",$realtime);
       $fdisplay(f_name, "Error_T2_rsim: ERR in IORD");
     end
  4'h6: begin
       $display("Error_T2_rsim: %0t ERR in MEMRD",$realtime);
       $fdisplay(f_name, "Error_T2_rsim: ERR in MEMRD");
     end
  4'ha: begin
       $display("Error_T2_rsim: %0t ERR in CONFRD",$realtime);
       $fdisplay(f_name, "Error_T2_rsim: ERR in CONFRD");
     end
  endcase

  //  $stop;
  $display (" ***** addr= %h, be_=%b, data_exp= %h, data_read=%h",
            addr, be_, data_exp, data_read);
  $fdisplay(f_name, " ***** addr= %h, be_=%b, data_exp= %h, data_read=%h",
            addr, be_, data_exp, data_read);

end
endtask

task config_read;
input [31:0] addr;
input [4:0] id_sel;
input [3:0] ben_;
input [2:0] func_num;
input [31:0] data_exp;
reg [31:0] data_exp1;
 begin
 $display("P_CMD:  cf_read : addr=%h, ben_=%h, data_exp=%h", addr, ben_, data_exp);

 if (addr[1:0] == 2'b00) // config type 0
     p_master.cfg_type0=1;
 else if (addr[1:0] == 2'b01)
     p_master.cfg_type0=0;

 case (ben_)
   4'b0000: begin p_master.PCI_confrd_dw(id_sel, func_num, {addr[7:2], 2'b00},   data_dw);
	      if (data_exp===32'hxxxxxxxx)
		data_exp1 = data_dw;
              else if (data_dw !== data_exp)
              err_report(4'ha, addr, ben_, data_exp, data_dw);
            end
   4'b1100: begin p_master.PCI_confrd_w (id_sel, func_num, {addr[7:2], 2'b00},   data_w);
	      if (data_exp[15:0]===16'hxxxx)
		data_exp1[15:0] = data_w;
              else if (data_w  !== data_exp[15:0])
                err_report(4'ha, addr, ben_, data_exp,{16'h0000,data_w}); // Benjamin Wan 1997-03-06
            end
   4'b0011: begin p_master.PCI_confrd_w (id_sel, func_num, {addr[7:2], 2'b10}, data_w);
	      if (data_exp[31:16]===16'hxxxx)
		data_exp1[31:16] = data_w;
              else if (data_w  !== data_exp[31:16])
                err_report(4'ha, addr, ben_, data_exp,{data_w,16'h0000}); // Benjamin Wan 1997-03-06
            end
   4'b1110: begin p_master.PCI_confrd_b (id_sel, func_num, {addr[7:2], 2'b00}, data_b);
	      if (data_exp[7:0]===8'hxx)
		data_exp1[7:0] = data_b;
              else if (data_b  !== data_exp[7:0])
                err_report(4'ha, addr, ben_, data_exp, {24'h000000,data_b}); // Benjamin Wan 1997-03-06
            end
   4'b1101: begin p_master.PCI_confrd_b (id_sel, func_num, {addr[7:2], 2'b01}, data_b);
	      if (data_exp[15:8]===8'hxx)
		data_exp1[15:8] = data_b;
              else if (data_b  !== data_exp[15:8])
                err_report(4'ha, addr, ben_, data_exp, {16'h0000,data_b,8'h00}); // Benjamin Wan 1997-03-06
            end
   4'b1011: begin p_master.PCI_confrd_b (id_sel, func_num, {addr[7:2], 2'b10}, data_b);
	      if (data_exp[23:16]===8'hxx)
		data_exp1[23:16] = data_b;
              else if (data_b  !== data_exp[23:16])
                err_report(4'ha, addr, ben_, data_exp, {8'h00,data_b,16'h0000}); // Benjamin Wan 1997-03-06
            end
   4'b0111: begin p_master.PCI_confrd_b (id_sel, func_num, {addr[7:2], 2'b11}, data_b);
	      if (data_exp[31:24]===8'hxx)
		data_exp1[31:24] = data_b;
              else if (data_b  !== data_exp[31:24])
                err_report(4'ha, addr, ben_, data_exp, {data_b,24'h000000}); // Benjamin Wan 1997-03-06
            end
   default: begin
		$fdisplay(f_name,"Error_T2_rsim: P_ERR: confrd, wrong ben=%h", ben_ );
		$display($realtime,,"Error_T2_rsim: P_ERR: confrd, wrong ben=%h", ben_ );
	    end
 endcase

     p_master.cfg_type0=1;

end
endtask


////////////////////////////////////////////////////////////////
task config_read_back;
input [7:0] addr;
input [4:0] id_sel;
input [3:0] ben_;
input [2:0] func_num;
inout [31:0] data_exp;
reg [31:0] data_exp;

begin

 if (addr[1:0] == 2'b00) // config type 0
     p_master.cfg_type0=1;
 else if (addr[1:0] == 2'b01)
     p_master.cfg_type0=0;

 case (ben_)
   4'b0000: begin p_master.PCI_confrd_dw(id_sel, func_num, {addr[7:2], 2'b00},   data_dw);
		data_exp = data_dw;
            end
   4'b1100: begin p_master.PCI_confrd_w (id_sel, func_num, {addr[7:2], 2'b00},   data_w);
		data_exp[15:0] = data_w;
            end
   4'b0011: begin p_master.PCI_confrd_w (id_sel, func_num, {addr[7:2], 2'b10}, data_w);
		data_exp[31:16] = data_w;
            end
   4'b1110: begin p_master.PCI_confrd_b (id_sel, func_num, {addr[7:2], 2'b00}, data_b);
		data_exp[7:0] = data_b;
            end
   4'b1101: begin p_master.PCI_confrd_b (id_sel, func_num, {addr[7:2], 2'b01}, data_b);
		data_exp[15:8] = data_b;
            end
   4'b1011: begin p_master.PCI_confrd_b (id_sel, func_num, {addr[7:2], 2'b10}, data_b);
		data_exp[23:16] = data_b;
            end
   4'b0111: begin p_master.PCI_confrd_b (id_sel, func_num, {addr[7:2], 2'b11}, data_b);
		data_exp[31:24] = data_b;
            end
   default: begin
		$fdisplay(f_name,"Error_T2_rsim: P_ERR: confrd, wrong ben=%h", ben_ );
		$display($realtime,,"Error_T2_rsim: P_ERR: confrd, wrong ben=%h", ben_ );
	    end
 endcase
     p_master.cfg_type0=1;

 $display("P_CMD:  cf_read : addr=%h, ben_=%h, data_readback=%h", addr, ben_, data_exp);
end
endtask

task config_write;
input [7:0] addr;
input [4:0] id_sel;
input [3:0] ben_;
input [2:0] func_num;
input [31:0] data_in;

begin

 $display("P_CMD:  cf_write: addr=%h, ben_=%h, data_in =%h", addr, ben_, data_in);
 if (addr[1:0] == 2'b00) // config type 0
   p_master.cfg_type0=1;
 else if (addr[1:0] == 2'b01)
   p_master.cfg_type0=0;

 case (ben_)
   4'b0000: p_master.PCI_confwr_dw(id_sel, func_num, {addr[7:2],2'b00}, data_in);
   4'b1000: begin
              p_master.PCI_confwr_w (id_sel, func_num, {addr[7:2],2'b00}, data_in[15:0]);
              p_master.PCI_confwr_b (id_sel, func_num, {addr[7:2],2'b10}, data_in[23:16]);
            end
   4'b1010: begin
              p_master.PCI_confwr_b (id_sel, func_num, {addr[7:2],2'b00}, data_in[8: 0]);
              p_master.PCI_confwr_b (id_sel, func_num, {addr[7:2],2'b10}, data_in[23:16]);
            end
   4'b1100: p_master.PCI_confwr_w (id_sel, func_num, {addr[7:2],2'b00}, data_in[15: 0]);
   4'b0011: p_master.PCI_confwr_w (id_sel, func_num, {addr[7:2],2'b10}, data_in[31:16]);
   4'b1110: p_master.PCI_confwr_b (id_sel, func_num, {addr[7:2],2'b00}, data_in[ 7: 0]);
   4'b1101: p_master.PCI_confwr_b (id_sel, func_num, {addr[7:2],2'b01}, data_in[15: 8]);
   4'b1011: p_master.PCI_confwr_b (id_sel, func_num, {addr[7:2],2'b10}, data_in[23:16]);
   4'b0111: p_master.PCI_confwr_b (id_sel, func_num, {addr[7:2],2'b11}, data_in[31:24]);
   default: begin
		$fdisplay(f_name,"Error_T2_rsim: P_ERR: confwr, wrong ben=%h", ben_ );
		$display($realtime,,"Error_T2_rsim: P_ERR: confwr, wrong ben=%h", ben_ );
	    end
 endcase

   p_master.cfg_type0=1;

end
endtask

