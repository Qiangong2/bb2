
`ifdef  NO_CPU_RTL_INI
`else
	`define	CPU_RTL_INI	//only use in RTL CPU simulation
`endif

`ifdef	NO_TRI_CPU
`else
	`define	TRI_CPU		//test CPU trigger command
`endif

`ifdef	POST_GSIM
//-------------
`define	top_path 			top.CHIP.CORE.T2_CPU.CPU_CORE.CPU_TOP
`define	cpu_path			top.CHIP.CORE.T2_CPU
//`define	ejtag_path 			top.CHIP.CORE.T2_CPU.T2_EJTAG
//`define	excp_path 			top.CHIP.CORE.T2_CPU.T2_RISC_TOP.T2_MAIN_CTRL.COP_EXCP.EXCP
//`define pc_gent_path 		top.CHIP.CORE.T2_CPU.T2_RISC_TOP.T2_MAIN_CTRL.NEXT_PC.PC_GENT
//`define pp_ctrl_path 		top.CHIP.CORE.T2_CPU.T2_RISC_TOP.T2_MAIN_CTRL.PP_CTRL
//`define stall_path 			top.CHIP.CORE.T2_CPU.T2_RISC_TOP.T2_MAIN_CTRL.PP_CTRL.STALL_DATA
`define d_cache_con_path 	top.CHIP.CORE.T2_CPU.CPU_CORE.CPU_TOP.T2_D_CACHE.D_CACHE_CONTROL_1
`define cp0_reg_path 		top.CHIP.CORE.T2_CPU.CPU_CORE.CPU_TOP.T2_MAIN_CTRL.COP_EXCP.CP0_1.CPX_REG
`define cp1_reg_path 		top.CHIP.CORE.T2_CPU.CPU_CORE.CPU_TOP.T2_MAIN_CTRL.COP_EXCP.CP0_1
`define cop_excp_path 		top.CHIP.CORE.T2_CPU.CPU_CORE.CPU_TOP.T2_MAIN_CreL.COP_EXCP
//------------
`else
//------------
	`ifdef	CPU_NET
	`define	top_path 			top.CHIP.CORE.T2_CPUP.CPU_CORE.CPU_TOP
	`define	cpu_path			top.CHIP.CORE.T2_CPU
	//`define	ejtag_path 			top.CHIP.CORE.T2_CPU.T2_EJTAG
	`define	excp_path 			top.CHIP.CORE.T2_CPU.CPU_CORE.CPU_TOP.T2_MAIN_CTRL.COP_EXCP.EXCP
	//`define pc_gent_path 		top.CHIP.CORE.T2_CPU.T2_RISC_TOP.T2_MAIN_CTRL.NEXT_PC.PC_GENT
	//`define pp_ctrl_path 		top.CHIP.CORE.T2_CPU.T2_RISC_TOP.T2_MAIN_CTRL.PP_CTRL
	//`define stall_path 			top.CHIP.CORE.T2_CPU.T2_RISC_TOP.T2_MAIN_CTRL.PP_CTRL.STALL_DATA
	`define d_cache_con_path 	top.CHIP.CORE.T2_CPU.CPU_CORE.CPU_TOP.T2_D_CACHE.D_CACHE_CONTROL_1
	`define cp0_reg_path 		top.CHIP.CORE.T2_CPU.CPU_CORE.CPU_TOP.T2_MAIN_CTRL.COP_EXCP.CP0_1.CPX_REG
	`define cp1_reg_path 		top.CHIP.CORE.T2_CPU.CPU_CORE.CPU_TOP.T2_MAIN_CTRL.COP_EXCP.CP0_1
	`define cop_excp_path 		top.CHIP.CORE.T2_CPU.CPU_CORE.CPU_TOP.T2_MAIN_CTRL.COP_EXCP
	`else
	`define	top_path 			top.CHIP.CORE.T2_CPU.cpu_core.cpu_top
	`define	cpu_path			top.CHIP.CORE.T2_CPU
	//`define	ejtag_path 			top.chip.core.T2_CPU.t2_ejtag
	//`define	excp_path 			top.chip.core.T2_CPU.t2_risc_top.t2_main_ctrl.cop_excp.excp
	//`define pc_gent_path 		top.chip.core.T2_CPU.t2_risc_top.t2_main_ctrl.next_pc.pc_gent
	//`define pp_ctrl_path 		top.chip.core.T2_CPU.t2_risc_top.t2_main_ctrl.pp_ctrl
	//`define stall_path 			top.chip.core.T2_CPU.t2_risc_top.t2_main_ctrl.pp_ctrl.stall_data
	`define d_cache_con_path 	top.CHIP.CORE.T2_CPU.cpu_core.cpu_top.t2_d_cache.d_cache_control_1
	`define cp0_reg_path 		top.CHIP.CORE.T2_CPU.cpu_core.cpu_top.t2_main_ctrl.cop_excp.cp0_1.cpx_reg
	`define cp1_reg_path 		top.CHIP.CORE.T2_CPU.cpu_core.cpu_top.t2_main_ctrl.cop_excp.cp0_1
	`define cop_excp_path 		top.CHIP.CORE.T2_CPU.cpu_core.cpu_top.t2_main_ctrl.cop_excp
	`endif
//------------
`endif
/* move to top.v for both cpu and video bist test JFR031118
`ifdef	POST_GSIM
wire	warm_reset_	= top.CHIP.CORE.CORE_WARMRST_;
wire	cold_reset_	= top.CHIP.CORE.CORE_COLDRST_;
wire	clk			= top.CHIP.CORE.CPU_CLK;
`else
wire	warm_reset_	= top.CHIP.CORE.core_warmrst_;
wire	cold_reset_	= top.CHIP.CORE.core_coldrst_;
wire	clk			= top.CHIP.CORE.cpu_clk;
`endif
*/
//CPU BIST Control
//======== invoke the CPU bist control behavior model ===========

cpu_bist_ext_agent cpu_bist_ext_agent(
	.bist_mode			(cpu_bist_mode		),
	.bist_finish		(cpu_bist_finish	),
	.bist_err_vec		(cpu_bist_err_vec	),
	.bist_test_mode_sel	(bist_test_mode_sel	),
	.clk				(clk				),
	.rst_				(warm_reset_		)
	);
/* Removed for m3357 JFR 030806
parameter	Tri_CPU_Enable = 0;	//default disable
`ifdef	TRI_CPU
wire	[4:0]	o_cpu_testin;
wire	[9:0]	o_cpu_trigcond;
wire			o_cpu_trig_en;

cpu_trigger_ext_agent cpu_trigger_ext_agent(
	.i_cpu_testout		(cpu_testout    ),
	.i_cpu_trig_signal	(cpu_trig_signal),

	.o_cpu_testin		(o_cpu_testin   ),
	.o_cpu_trigcond		(o_cpu_trigcond ),
	.o_cpu_trig_en		(o_cpu_trig_en  ),

	.clk				(clk            ),
	.rst_               (warm_reset_    )
	);

tranif1	(o_cpu_trig_en	  	,    cpu_trig_en		, 		Tri_CPU_Enable);
tranif1 (o_cpu_testin[0]    ,    cpu_testin[0]      ,     	Tri_CPU_Enable);
tranif1 (o_cpu_testin[1]    ,    cpu_testin[1]      ,     	Tri_CPU_Enable);
tranif1 (o_cpu_testin[2]    ,    cpu_testin[2]      ,     	Tri_CPU_Enable);
tranif1 (o_cpu_testin[3]    ,    cpu_testin[3]      ,     	Tri_CPU_Enable);
tranif1 (o_cpu_testin[4]    ,    cpu_testin[4]      ,     	Tri_CPU_Enable);

tranif1 (o_cpu_trigcond[00] ,    cpu_trigcond[00]   ,     	Tri_CPU_Enable);
tranif1 (o_cpu_trigcond[01] ,    cpu_trigcond[01]   ,     	Tri_CPU_Enable);
tranif1 (o_cpu_trigcond[02] ,    cpu_trigcond[02]   ,     	Tri_CPU_Enable);
tranif1 (o_cpu_trigcond[03] ,    cpu_trigcond[03]   ,     	Tri_CPU_Enable);
tranif1 (o_cpu_trigcond[04] ,    cpu_trigcond[04]   ,     	Tri_CPU_Enable);
tranif1 (o_cpu_trigcond[05] ,    cpu_trigcond[05]   ,     	Tri_CPU_Enable);
tranif1 (o_cpu_trigcond[06] ,    cpu_trigcond[06]   ,     	Tri_CPU_Enable);
tranif1 (o_cpu_trigcond[07] ,    cpu_trigcond[07]   ,     	Tri_CPU_Enable);
tranif1 (o_cpu_trigcond[08] ,    cpu_trigcond[08]   ,     	Tri_CPU_Enable);
tranif1 (o_cpu_trigcond[09] ,    cpu_trigcond[09]   ,     	Tri_CPU_Enable);

`endif
*/

//Removed, M3357 have no cpu mode JFR 030806
/*
//======== invoke the chipset behavior model with mips bus ======
bus_test bus_test(
			.sysad		(sysad		),
			.syscmd		(syscmd		),
			.pvalid_	(pvalid_	),
			.preq_		(preq_		),
			.pmaster_	(pmaster_	),
			.eok_		(eok_		),
			.evalid_	(evalid_	),
			.ereq_		(ereq_		),
			.int_		(sysint_	),
			.nmi_		(sysnmi_	),
			.tclk		(tclk		),
			.rst_		(bus_rst_	)
		);
//===============================================================
*/

//========= invoke the ICE behavior model =======================

`ifdef	POST_GSIM
	wire	probe_en	= top.CHIP.CORE.CPU_PROBE_EN;
`else
	wire	probe_en	= top.CHIP.CORE.cpu_probe_en;
`endif
//in normal mode, make the ejtag always in reset status
wire	ice_rst_	= warm_reset_ & probe_en;

ice_test ice_test(
   			.j_tms		(ext_p_j_tms		),
   			.j_tdi		(ext_p_j_tdi		),
   			.j_tdo		(ext_p_j_tdo		),
   			.j_tclk		(ext_p_j_tclk		),
   			.j_trst_	(ext_p_j_rst_		),
   			.warm_rst_	(ice_rst_		) //warm_reset_	)
			);

/*
ice_bh ice_bh (
			.j_tdo			(p_j_tdo	),
			.j_tdi			(p_j_tdi    ),
			.j_tms			(p_j_tms    ),
			.j_tclk			(p_j_tclk   ),
			.j_trst_		(p_j_rst_   ),
			.cpu_warmrst_   (ice_rst_   )
);
*/
//############################################################
//###### 	Migrate form run.all, sh  				###########
//########  	Jeffrey 2003-08-05					###########
//###########################################################
/**********************************************************
	Function: This test bench is for fpga rsim.
	Author:   Jim
	Date:     2002/9/20
***********************************************************/



//module risc;

//reg		clk;
reg		rst_;
reg		reset_;

wire		u_request;
wire		u_write;
wire	[1:0]	u_wordsize;
wire	[3:0]	u_bytemask;
wire	[31:0]	u_startpaddr;
wire	[31:0]	u_data_to_send;
wire		o_ready;
wire		o_ack;
wire		o_bus_error;
wire	[31:0]	o_data_in;

wire		j_tclk;
wire		j_trst_;
wire		j_tdi;
wire		j_tdo;
wire		j_tms;
wire		ProbEn;

//wire		bist_mode;
//wire		bist_finish;
//wire	[5:0]	bist_error_vec;
/*

`ifdef EJTAG
ice_bh ice_bh(
        .j_tdi          (j_tdi                  ),
        .j_tdo          (j_tdo                  ),
        .j_tms          (j_tms                  ),
        .j_trst_        (j_trst_                ),
	.j_tclk		(j_tclk			),
	.cpu_warmrst_	(reset_			)
);
assign	ProbEn = 1'b1;
`else
assign	ProbEn = 1'b0;
`endif
*/

//========= invoke the MIPS program report model =================
`ifdef POST_GSIM
//wire [31:0] debug_data 		= top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.CTRL_REG.DEBUG_PORT;
wire [31:0]	core_gpio_out	= top.CHIP.CORE.GPIO_OUT;
wire		error_rpt_clk 	= top.CHIP.CORE.T2_CHIPSET.CPU_CLK;
wire		error_rpt_rst_ 	= top.CHIP.CORE.CORE_WARMRST_;
wire		sys_mem_clk		= top.CHIP.CORE.T2_CHIPSET.MEM_CLK;
wire		sys_chipset_rst_= top.CHIP.CORE.T2_CHIPSET.RST_;

wire		sysctrl_req		= top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.CTRL_REG.SYSCTRL_REQ		;
wire		sysctrl_gnt     = top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.CTRL_REG.SYSCTRL_GNT      ;
//wire [15:2]	sysctrl_addr    = top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.CTRL_REG.SYSCTRL_ADDR     ;
wire [15:2]	sysctrl_addr    = { top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.MEM_ADDR_15_1,
								top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.MEM_ADDR_14_1,
								top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.MEM_ADDR_13_1,
        						top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.MEM_ADDR_12_1,
        						top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.MEM_ADDR_11_1,
        						top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.MEM_ADDR_10_1,
        						top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.MEM_ADDR_9_1,
        						top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.MEM_ADDR_8_1,
        						top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.MEM_ADDR_7_1,
        						top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.MEM_ADDR_6_1,
        						top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.MEM_ADDR_5_1,
        						top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.MEM_ADDR_4_1,
        						top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.MEM_ADDR_3_1,
        						top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.MEM_ADDR_2_1
        						};
//wire [3:0]	sysctrl_be      = top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.CTRL_REG.SYSCTRL_BE       ;
wire [3:0]	sysctrl_be      = {	top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.N4846,
								top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.N4847,
								top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.N4848,
								top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.N4849
								};
wire		sysctrl_cmd     = top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.CTRL_REG.SYSCTRL_CMD      ;
wire [31:0]	sysctrlrd_data  = top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.CTRL_REG.SYSCTRLRD_DATA   ;
//wire [31:0]	sysctrlwr_data  = top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.CTRL_REG.SYSCTRLWR_DATA   ;
//postgsim has change the signals name
wire [31:0]	sysctrlwr_data  = {	top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.CPU_SLAVE_WDATA[31] ,
								top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.CPU_SLAVE_WDATA[30] ,
								top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.CPU_SLAVE_WDATA[29] ,
								top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.CPU_SLAVE_WDATA[28] ,
								top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.CPU_SLAVE_WDATA[27] ,
								top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.CPU_SLAVE_WDATA[26] ,
								top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.CPU_SLAVE_WDATA[25] ,
								top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.CPU_SLAVE_WDATA[24] ,
								top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.CPU_SLAVE_WDATA[23] ,
								top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.CPU_SLAVE_WDATA[22] ,
								top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.CPU_SLAVE_WDATA[21] ,
								top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.CPU_SLAVE_WDATA[20] ,
								top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.CPU_SLAVE_WDATA[19] ,
								top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.CPU_SLAVE_WDATA[18] ,
								top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.CPU_SLAVE_WDATA[17] ,
								top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.CPU_SLAVE_WDATA[16] ,
								top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.CPU_SLAVE_WDATA[15] ,
								top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.CPU_SLAVE_WDATA[14] ,
								top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.CPU_SLAVE_WDATA[13] ,
								top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.CPU_SLAVE_WDATA[12] ,
								top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.CPU_SLAVE_WDATA[11] ,
								top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.CPU_SLAVE_WDATA[10] ,
								top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.CPU_SLAVE_WDATA[9] ,
								top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.CPU_SLAVE_WDATA_8_ ,
								top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.CPU_SLAVE_WDATA[7] ,
								top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.CPU_SLAVE_WDATA[6] ,
								top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.CPU_SLAVE_WDATA[5] ,
								top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.CPU_SLAVE_WDATA[4] ,
								top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.CPU_SLAVE_WDATA[3] ,
								top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.CPU_SLAVE_WDATA[2] ,
								top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.CPU_SLAVE_WDATA[1] ,
								top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.CPU_SLAVE_WDATA[0]
								};
`else
//wire [31:0] debug_data = top.chip.core.t2_chipset.nb_cpu_biu.ctrl_reg.debug_port;
wire [31:0]	core_gpio_out	= top.CHIP.CORE.gpio_out;
wire		error_rpt_clk 	= top.CHIP.CORE.T2_CHIPSET.cpu_clk;
wire		error_rpt_rst_ 	= top.CHIP.CORE.core_warmrst_;
wire		sys_mem_clk		= top.CHIP.CORE.T2_CHIPSET.mem_clk;
wire		sys_chipset_rst_= top.CHIP.CORE.T2_CHIPSET.rst_;

wire		sysctrl_req		= top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.ctrl_reg.sysctrl_req		;
wire		sysctrl_gnt     = top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.ctrl_reg.sysctrl_gnt      ;
wire [15:2]	sysctrl_addr    = top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.ctrl_reg.sysctrl_addr     ;
wire [3:0]	sysctrl_be      = top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.ctrl_reg.sysctrl_be       ;
wire		sysctrl_cmd     = top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.ctrl_reg.sysctrl_cmd      ;
wire [31:0]	sysctrlrd_data  = top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.ctrl_reg.sysctrlrd_data   ;
wire [31:0]	sysctrlwr_data  = top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.ctrl_reg.sysctrlwr_data   ;

`endif

//use the ctrl_reg signal to latch the scratch data for debug
parameter	t_udly = 1;
wire	sys_byte0 = sysctrl_be[0] == 1'b1;
wire	sys_byte1 = sysctrl_be[1] == 1'b1;
wire	sys_byte2 = sysctrl_be[2] == 1'b1;
wire	sys_byte3 = sysctrl_be[3] == 1'b1;
wire	index_30h = sysctrl_addr[15:2] == 'h0c;
wire	ctrlwr_30h = index_30h & sysctrl_cmd & sysctrl_gnt;

reg	[31:0]	debug_data;
always @(posedge sys_mem_clk or negedge sys_chipset_rst_)
  if (!sys_chipset_rst_)
     debug_data	<= #t_udly 32'h0;
  else if (ctrlwr_30h)	begin
     if (sys_byte0)	debug_data[7:0]		<= #t_udly sysctrlwr_data[7:0]	;
     if (sys_byte1)	debug_data[15:8]	<= #t_udly sysctrlwr_data[15:8]	;
     if (sys_byte2)	debug_data[23:16]	<= #t_udly sysctrlwr_data[23:16]	;
     if (sys_byte3)	debug_data[31:24]	<= #t_udly sysctrlwr_data[31:24]	;
  end
//wire [3:0]	gpio_3t0	= {ext_gpio[15:13], core_gpio_out[12]}; //because ext_gpio[12] has been use the flash address [19]
wire [3:0]	gpio_3t0	= core_gpio_out[15:12];
error_rpt error_rpt(
			.gpio_3t0		(gpio_3t0		),
			.rpt_data_31t0	(debug_data		),
			.clk			(error_rpt_clk	),
			.rst_           (error_rpt_rst_	)
			);
/*
//=================== snoop the finish condition ===============================
always	@(posedge eeprom_cs_ or posedge tclk)
	if (~CPU_mode &&(eeprom_addr == 21'h1f_ffff))
		begin
		cpu_vec_end = 1'b1;
		#100;
		cpu_vec_00_end = 1'b1;
		#100;
		cpu_vec_01_end = 1'b1;
		#100;
		cpu_vec_02_end = 1'b1;
		#100;
		cpu_vec_03_end = 1'b1;
		#100;
		cpu_vec_04_end = 1'b1;
		#100;
		cpu_vec_05_end = 1'b1;
		#100;
		cpu_vec_06_end = 1'b1;
		#100;
		cpu_vec_07_end = 1'b1;
		#100;
		cpu_vec_08_end = 1'b1;
		#100;
		cpu_vec_09_end = 1'b1;
		#100;
		cpu_vec_10_end = 1'b1;
		#100;
		cpu_vec_11_end = 1'b1;
		#100;
		cpu_vec_12_end = 1'b1;
		#100;
		cpu_vec_13_end = 1'b1;
		#100;
		cpu_vec_14_end = 1'b1;
		#100;
		cpu_vec_15_end = 1'b1;
		#100;
		cpu_vec_16_end = 1'b1;
		#100;
		cpu_vec_17_end = 1'b1;
		#100;
		cpu_vec_18_end = 1'b1;
		#100;
		cpu_vec_19_end = 1'b1;//JFR 030722
		#100;
		cpu_vec_20_end = 1'b1;
		#100;
		end
	else if (CPU_mode &&(bus_test.bootrom.eeprom_addr[20:2] == 19'h7_ffff))
		begin
		cpu_vec_end = 1'b1;
		#100;
		cpu_vec_00_end = 1'b1;
		#100;
		cpu_vec_01_end = 1'b1;
		#100;
		cpu_vec_02_end = 1'b1;
		#100;
		cpu_vec_03_end = 1'b1;
		#100;
		cpu_vec_04_end = 1'b1;
		#100;
		cpu_vec_05_end = 1'b1;
		#100;
		cpu_vec_06_end = 1'b1;
		#100;
		cpu_vec_07_end = 1'b1;
		#100;
		cpu_vec_08_end = 1'b1;
		#100;
		cpu_vec_09_end = 1'b1;
		#100;
		cpu_vec_10_end = 1'b1;
		#100;
		cpu_vec_11_end = 1'b1;
		#100;
		cpu_vec_12_end = 1'b1;
		#100;
		cpu_vec_13_end = 1'b1;
		#100;
		cpu_vec_14_end = 1'b1;
		#100;
		cpu_vec_15_end = 1'b1;
		#100;
		cpu_vec_16_end = 1'b1;
		#100;
		cpu_vec_17_end = 1'b1;
		#100;
		cpu_vec_18_end = 1'b1;
		#100;
		cpu_vec_19_end = 1'b1;//JFR 030722
		#100;
		cpu_vec_20_end = 1'b1;
		#100;
		end
		*/
//==============================================================================

/******************************************************
* T2-RISC core begin
******************************************************/
//### Monitor the PC and IR.
//### Migrate from the CPU Env. yuky, 2002-07-31

integer inst_cnt, dump_num;
reg		dump_enable;
initial begin
	inst_cnt = 32'b0;
	dump_num = 0;
	dump_enable = 1;
end

wire pd_valid,pe_valid,pc_valid,pw_v;
wire	cw_word_arrival;
//instruction sequence monitor
wire	[31:0]	fd_instout;
reg		[31:0]	ir_out;

wire  [31:0]  gw_pc;
wire  [31:0]  gw_pc_dump = 	cw_word_arrival?
			   				gw_pc+4:
			   				gw_pc;

`ifdef POST_GSIM
assign	cw_word_arrival = `d_cache_con_path.CW_WORD_ARRIVAL ;
//assign	fd_instout		= `top_path.T2_IFETCH.FF_INST_OUT.OUT[37:6];
assign	fd_instout		= `top_path.T2_IFETCH.I_CACHE_CONTROL_1.FF_INST_OUT.DOUT[37:6]; //change at 2002-08-22, yuky
assign	pd_valid		= `top_path.T2_MAIN_CTRL.PP_CTRL.PD_VALID;
assign	pe_valid        = `top_path.T2_MAIN_CTRL.PP_CTRL.PE_VALID;
assign	pc_valid        = `top_path.T2_MAIN_CTRL.PP_CTRL.PC_VALID;
//assign 	gw_pc 			= `top_path.GW_PC;
	`define	path_excp `top_path.T2_MAIN_CTRL.COP_EXCP.EXCP
	assign	gw_pc = {	`path_excp.GW_PC_PART1_INV_REG_8.QN,
			`path_excp.GW_PC_PART2_REG_22.Q,
			`path_excp.GW_PC_PART1_INV_REG_7.QN,
			`path_excp.GW_PC_PART1_INV_REG_6.QN,
			`path_excp.GW_PC_PART1_INV_REG_5.QN,
			`path_excp.GW_PC_PART1_INV_REG_4.QN,
			`path_excp.GW_PC_PART1_INV_REG_3.QN,
			`path_excp.GW_PC_PART1_INV_REG_2.QN,
			`path_excp.GW_PC_PART1_INV_REG_1.QN,
			`path_excp.GW_PC_PART1_INV_REG_0.QN,
			`path_excp.GW_PC_PART2_REG_21.Q,
			`path_excp.GW_PC_PART2_REG_20.Q,
			`path_excp.GW_PC_PART2_REG_19.Q,
			`path_excp.GW_PC_PART2_REG_18.Q,
			`path_excp.GW_PC_PART2_REG_17.Q,
			`path_excp.GW_PC_PART2_REG_16.Q,
			`path_excp.GW_PC_PART2_REG_15.Q,
			`path_excp.GW_PC_PART2_REG_14.Q,
			`path_excp.GW_PC_PART2_REG_13.Q,
			`path_excp.GW_PC_PART2_REG_12.Q,
			`path_excp.GW_PC_PART2_REG_11.Q,
			`path_excp.GW_PC_PART2_REG_10.Q,
			`path_excp.GW_PC_PART2_REG_9.Q,
			`path_excp.GW_PC_PART2_REG_8.Q,
			`path_excp.GW_PC_PART2_REG_7.Q,
			`path_excp.GW_PC_PART2_REG_6.Q,
			`path_excp.GW_PC_PART2_REG_5.Q,
			`path_excp.GW_PC_PART2_REG_4.Q,
			`path_excp.GW_PC_PART2_REG_3.Q,
			`path_excp.GW_PC_PART2_REG_2.Q,
			`path_excp.GW_PC_PART2_REG_1.Q,
			`path_excp.GW_PC_PART2_REG_0.Q };
assign 	pw_v  			= `top_path.PW_V;
wire	pw_valid 		= `top_path.PW_VALID;
`else
	`ifdef CPU_NET
	assign	cw_word_arrival = `d_cache_con_path.CW_WORD_ARRIVAL ;
	//assign	fd_instout		= `top_path.T2_IFETCH.FF_INST_OUT.OUT[37:6];
	//assign	fd_instout		= `top_path.FD_INSTRUCTION_OUT_3; //change at 2002-08-22, yuky
	assign		fd_instout	= top.CHIP.CORE.T2_CPU.T2_RISC_TOP.T2_IFETCH.I_CACHE_CONTROL_1.FF_INST_OUT.DOUT[37:6];

	assign	pd_valid		= `top_path.T2_MAIN_CTRL.PP_CTRL.PD_VALID;
	assign	pe_valid        = `top_path.T2_MAIN_CTRL.PP_CTRL.PE_VALID;
	assign	pc_valid        = `top_path.T2_MAIN_CTRL.PP_CTRL.PC_VALID;

	//assign 	gw_pc 			= `top_path.GW_PC;
	//assign  gw_pc 			= `excp_path.GW_PC ;
	assign 	pw_v  			= `top_path.PW_V;
	`define	path_excp `top_path.T2_MAIN_CTRL.COP_EXCP.EXCP
	assign	gw_pc = {	`path_excp.GW_PC_PART1_INV_REG_8.QN,
			`path_excp.GW_PC_PART2_REG_22.Q,
			`path_excp.GW_PC_PART1_INV_REG_7.QN,
			`path_excp.GW_PC_PART1_INV_REG_6.QN,
			`path_excp.GW_PC_PART1_INV_REG_5.QN,
			`path_excp.GW_PC_PART1_INV_REG_4.QN,
			`path_excp.GW_PC_PART1_INV_REG_3.QN,
			`path_excp.GW_PC_PART1_INV_REG_2.QN,
			`path_excp.GW_PC_PART1_INV_REG_1.QN,
			`path_excp.GW_PC_PART1_INV_REG_0.QN,
			`path_excp.GW_PC_PART2_REG_21.Q,
			`path_excp.GW_PC_PART2_REG_20.Q,
			`path_excp.GW_PC_PART2_REG_19.Q,
			`path_excp.GW_PC_PART2_REG_18.Q,
			`path_excp.GW_PC_PART2_REG_17.Q,
			`path_excp.GW_PC_PART2_REG_16.Q,
			`path_excp.GW_PC_PART2_REG_15.Q,
			`path_excp.GW_PC_PART2_REG_14.Q,
			`path_excp.GW_PC_PART2_REG_13.Q,
			`path_excp.GW_PC_PART2_REG_12.Q,
			`path_excp.GW_PC_PART2_REG_11.Q,
			`path_excp.GW_PC_PART2_REG_10.Q,
			`path_excp.GW_PC_PART2_REG_9.Q,
			`path_excp.GW_PC_PART2_REG_8.Q,
			`path_excp.GW_PC_PART2_REG_7.Q,
			`path_excp.GW_PC_PART2_REG_6.Q,
			`path_excp.GW_PC_PART2_REG_5.Q,
			`path_excp.GW_PC_PART2_REG_4.Q,
			`path_excp.GW_PC_PART2_REG_3.Q,
			`path_excp.GW_PC_PART2_REG_2.Q,
			`path_excp.GW_PC_PART2_REG_1.Q,
			`path_excp.GW_PC_PART2_REG_0.Q };

	wire	pw_valid 		= `top_path.PW_VALID;
	`else
	assign	cw_word_arrival = `d_cache_con_path.cw_word_arrival ;
	//assign	fd_instout		= `top_path.t2_ifetch.ff_inst_out.out[37:6];
	assign	fd_instout		= `top_path.fd_instruction_out_3; //change at 2002-08-22, yuky
	assign	pd_valid		= `top_path.t2_main_ctrl.pd_valid;
	assign	pe_valid        = `top_path.t2_main_ctrl.pe_valid;
	assign	pc_valid        = `top_path.t2_main_ctrl.pc_valid;
	assign 	gw_pc 			= `top_path.t2_main_ctrl.gw_pc;
	assign 	pw_v  			= `top_path.t2_main_ctrl.pw_v;
	wire	pw_valid 		= `top_path.t2_main_ctrl.pw_valid;
	`endif
`endif
reg	[31:0]	e_id_inst;
reg	[31:0]	c_id_inst;
reg	[31:0]	w_id_inst;

always @(fd_instout)
begin
	restore_ir(fd_instout,ir_out);
end

always @(posedge clk ) begin
	if( pd_valid )
		e_id_inst	<= #1 ir_out;
end

always @(posedge clk ) begin
	if( pe_valid )
		c_id_inst	<= #1 e_id_inst;
end

always @(posedge clk ) begin
	if( pc_valid )
		w_id_inst	<= #1 c_id_inst;
end

always @(posedge clk) begin
	#0.2 if(warm_reset_ & cold_reset_ & pw_v) begin
		inst_cnt = inst_cnt + 1;
//		`ifdef  DUMP_DEBUG
		if(dump_enable)
			dump_num = dump_num + 1;
//		$display("********* Current Inst counter : %d   Dump No.: %d ***********",
//		  inst_cnt, dump_num);
		#0.2 $display("\t\tPC=%h \tIR=%h \tDump No.:", gw_pc_dump, w_id_inst, dump_num);
//		`endif
	end
end
/*
//changed by steven 990913 for reorder instructions
task	restore_ir;
input	[31:0]		ir_in;
output	[31:0]		ir_out;
reg		[5:0]		opcode;
reg		[8:0]		funcode;
reg		[2:0]		order_cmd;
begin
	opcode = ir_in[31:26];
	funcode = {ir_in[25:21],ir_in[3:0]};

            	casex({opcode,funcode})
                15'b001xxxxxxxxxxxx :
                    order_cmd = 3'b001;     //immediate number instructions
				15'b010001xxxxx0100 :		// cvt.w.fmt
					order_cmd = 3'b110;
                15'b100x0xxxxxxxxxx :
                    order_cmd = 3'b001;     //load instructions
                15'b100x11xxxxxxxxx :
                    order_cmd = 3'b001;     //load instructions
                15'b110000xxxxxxxxx :
                    order_cmd = 3'b001;     //ll
                15'b110001xxxxxxxxx :
                    order_cmd = 3'b001;     //lwc1/lwc2
                15'b110010xxxxxxxxx :
                    order_cmd = 3'b001;
                15'b011101xxxxxxxxx :
                    order_cmd = 3'b001;     //lwc2c
                15'b01000000000xxxx :
                    order_cmd = 3'b001;     //mfc0/mfc1/mfc2
                15'b01000100000xxxx :
                    order_cmd = 3'b001;
                15'b01001000000xxxx :
                    order_cmd = 3'b001;
                15'b01000100010xxxx :
                    order_cmd = 3'b001;     //cfc1/cfc2
                15'b01001000010xxxx :
                    order_cmd = 3'b001;
                15'b010011xxxxx0000 :
                    order_cmd = 3'b010;     //lwxc1
                15'b010011xxxxx0001 :
                    order_cmd = 3'b011;     //ldxc1
                15'b110101xxxxxxxxx :
                    order_cmd = 3'b100;     //ldc1/ldc2
                15'b110110xxxxxxxxx :
                    order_cmd = 3'b100;
                15'b111101xxxxxxxxx :
                    order_cmd = 3'b101;     //sdc1/sdc2
                15'b111110xxxxxxxxx :
                    order_cmd = 3'b101;
                default :
                    order_cmd = 3'b000;
                endcase

	case(order_cmd)
		3'b001 :
			ir_out = {ir_in[31:21],ir_in[15:11],ir_in[20:16],ir_in[10:0]};
		3'b010 :
			ir_out = {ir_in[31:16],ir_in[10:6],ir_in[15:11],ir_in[5:0]};
		3'b011 :
			ir_out = {ir_in[31:16],ir_in[10:6],ir_in[11],ir_in[15:12],ir_in[5:0]};
		3'b100 :
			ir_out = {ir_in[31:21],ir_in[11],ir_in[15:12],ir_in[20:16],ir_in[10:0]};
		3'b101 :
			ir_out = {ir_in[31:21],ir_in[16],ir_in[20:17],ir_in[15:0]};
		3'b110 :
			ir_out = {ir_in[31:21],ir_in[15:6],ir_in[20:16],ir_in[5:0]};
		default :
			ir_out = ir_in;
	endcase

end
endtask
*/

/*--------------------------------------------------------------
         Signal fix for dumping
----------------------------------------------------------------*/
/*
wire	[31:0]	gf_pc	= `top_path.t2_main_ctrl.cop_excp.excp.gf_pc;
wire	[31:0]	gd_pc	= `top_path.t2_main_ctrl.cop_excp.excp.gd_pc;
wire	[31:0]	ge_pc	= `top_path.t2_main_ctrl.cop_excp.excp.ge_pc;
wire	[31:0]	gc_pc	= `top_path.t2_main_ctrl.cop_excp.excp.gc_pc;
wire	[31:0]	gw_pc	= `top_path.t2_main_ctrl.cop_excp.excp.gw_pc;

wire	[31:0]	fd_instruction_out	= `top_path.t2_ifetch.fd_instruction_out_3;

wire		pd_valid = `top_path.t2_main_ctrl.pd_valid;
wire		pe_valid = `top_path.t2_main_ctrl.pe_valid;
wire		pc_valid = `top_path.t2_main_ctrl.pc_valid;
wire		pw_valid = `top_path.t2_main_ctrl.pw_valid;

wire		pw_v	 = `top_path.t2_main_ctrl.pw_v;//JFR030808


reg	[31:0]	ir_out;
reg	[31:0]	d_id_inst;
reg	[31:0]	e_id_inst;
reg	[31:0]	c_id_inst;
reg	[31:0]	w_id_inst;

always @ (fd_instruction_out) begin
	restore_ir (fd_instruction_out, ir_out);
end

always @ (posedge clk) begin
	if (pd_valid)
	   e_id_inst <= #1 ir_out;
end

always @ (posedge clk) begin
	if (pe_valid)
	   c_id_inst <= #1 e_id_inst;
end

always @ (posedge clk) begin
	if (pc_valid)
	   w_id_inst <= #1 c_id_inst;
end

integer	inst_cnt;
always @ (posedge clk) begin
	if (~rst_)
	   inst_cnt = 0;
//	else if (pw_valid)
//JFR030808
		else if (pw_v)
	   inst_cnt = inst_cnt + 1;
end

integer cycle_cnt;
always @ (posedge clk) begin
	if (~rst_)
	   cycle_cnt <= 0;
	else if (pw_valid)
	   cycle_cnt <= 0;
	else
	   cycle_cnt <= cycle_cnt + 1;
end
*/
/*------------------------------------------------------
       Dump information for debugging patterns
--------------------------------------------------------*/
/*
always @ (posedge clk) begin
	if (pw_valid) begin
//	   $display ("%d\tPC=%h\tIR=%h", inst_cnt, gw_pc, w_id_inst);
//JFR 030808
	   		#0.2 $display("\t\tPC=%h \tIR=%h \tDump No.:", gw_pc, w_id_inst, inst_cnt);
	   $display ("\t\t...");
	end
end

`define		ptn_success_addr  32'h40123000
`define		ptn_success_data  32'h88888888
`define		ptn_fail_addr     32'h00123000
`define		ptn_fail_data     32'h65616063

wire	[31:0]	cp0_epc			= `top_path.t2_main_ctrl.cop_excp.cp0_1.cpx_reg.cp0_epc;
wire	[31:0]	cp0_badvaddr	= `top_path.t2_main_ctrl.cop_excp.cp0_1.cpx_reg.cp0_bad_vaddr;
wire	[31:0]	cp0_cause		= `top_path.t2_main_ctrl.cop_excp.cp0_1.cpx_reg.cp0_cause;
wire	[31:0]	cp0_status		= `top_path.t2_main_ctrl.cop_excp.cp0_1.cpx_reg.cp0_status;


always @ (posedge clk) begin
	if (u_request & u_bytemask==4'b1111 &
		u_startpaddr==`ptn_success_addr & u_data_to_send==`ptn_success_data) begin
	   $display ("###################################################");
	   $display ("               Pattern Happy !!!");
	   $display ("###################################################");
	   #10
	   $finish;
	end
	else if (u_request & u_bytemask==4'b1111 &
		u_startpaddr==`ptn_fail_addr & u_data_to_send==`ptn_fail_data) begin
	   $display ("###################################################");
	   $display ("               Pattern Sad !!!");
	   $display ("###################################################");
	   #10
	   $finish;
	end
	else if ((w_id_inst == 32'h3c000000 | w_id_inst == 32'h24000000) & pw_valid) begin
	   if (cp0_status[21])
		$display ("Warning: TLB shutdown occurs !!!");
	   else begin
	   	$display ("###################################################");
	   	$display ("               Pattern Pass !!!");
	   	$display ("###################################################");
	   end
	   #10
	   $finish;
	end
end

always @ (posedge clk) begin
	if (cycle_cnt > 50000) begin
	   $display ("###################################################");
	   $display ("      Pattern Stop Because of Deadlock !!!");
	   $display ("###################################################");
	   #10
	   $finish;
	end
end

//exception monitor
always @ (gc_pc) begin
	if (gc_pc==32'h80000000 | gc_pc==32'h80000180 | gc_pc==32'hbfc00200 | gc_pc==32'hbfc00380) begin
	   $display ("\n");
	   $display ("Exception Ocurr");
	   $display ("EXCPC = %h", cp0_epc);
	   $display ("BADVA = %h", cp0_badvaddr);
	   $display ("CAUSE = %h", cp0_cause);
	   case (cp0_cause[6:2])
		5'b00000 :$display( "Exception Type 0: Interrupt");
		5'b00001 :$display( "Exception Type 1: TLB Modification exception");
		5'b00010 :$display( "Exception Type 2: TLB Miss exception(load or instruction fetch)");
		5'b00011 :$display( "Exception Type 3: TLB Miss exception(store)");
		5'b00100 :$display( "Exception Type 4: Address Error exception(load or instruction fetch)");
		5'b00101 :$display( "Exception Type 5: Address Error exception(store)");
		5'b00110 :$display( "Exception Type 6: Bus Error exception(instruction fetch)");
		5'b00111 :$display( "Exception Type 7: Bus Error exception(data reference:load or store)");
		5'b01000 :$display( "Exception Type 8: Syscall exception");
		5'b01001 :$display( "Exception Type 9: Breakpoint exception");
		5'b01010 :$display( "Exception Type 10: Reserved Instruction exception");
		5'b01011 :$display( "Exception Type 11: Coprocessor Unusable exception");
		5'b01100 :$display( "Exception Type 12: Arithmetic Overflow exception");
		5'b01101 :$display( "Exception Type 13: Trap exception");
		5'b10000 :$display( "Exception Type 16: Gte_cmd Exception");
		5'b10001 :$display( "Exception Type 17: Gte_reg Exception");
		5'b10010 :$display( "Exception Type 18: Fpu_emu Exception");
		5'b10111 :$display( "Exception Type 23: Watch exception");
	   endcase
	   $display ("\n");
	end
end
*/
/*----------------------------------------------------
       Define task
------------------------------------------------------*/

task    restore_ir;
input   [31:0]          ir_in;
output  [31:0]          ir_out;
reg     [5:0]           opcode;
reg     [8:0]           funcode;
reg     [2:0]           order_cmd;
begin
        opcode = ir_in[31:26];
        funcode = {ir_in[25:21],ir_in[3:0]};

                casex({opcode,funcode})
                15'b001xxxxxxxxxxxx :
                    order_cmd = 3'b001;     //immediate number instructions
                15'b010001xxxxx0100 :       // cvt.w.fmt
                    order_cmd = 3'b110;
                15'b100x0xxxxxxxxxx :
                    order_cmd = 3'b001;     //load instructions
                15'b100x11xxxxxxxxx :
                    order_cmd = 3'b001;     //load instructions
                15'b110000xxxxxxxxx :
                    order_cmd = 3'b001;     //ll
                15'b110001xxxxxxxxx :
                    order_cmd = 3'b001;     //lwc1/lwc2
                15'b110010xxxxxxxxx :
                    order_cmd = 3'b001;
                15'b011101xxxxxxxxx :
                    order_cmd = 3'b001;     //lwc2c
                15'b01000000000xxxx :
                    order_cmd = 3'b001;     //mfc0/mfc1/mfc2
                15'b01000100000xxxx :
                    order_cmd = 3'b001;
                15'b01001000000xxxx :
                    order_cmd = 3'b001;
                15'b01000100010xxxx :
                    order_cmd = 3'b001;     //cfc1/cfc2
                15'b01001000010xxxx :
                    order_cmd = 3'b001;
                15'b010011xxxxx0000 :
                    order_cmd = 3'b010;     //lwxc1
                15'b010011xxxxx0001 :
                    order_cmd = 3'b011;     //ldxc1
                15'b110101xxxxxxxxx :
                    order_cmd = 3'b100;     //ldc1/ldc2
                15'b110110xxxxxxxxx :
                    order_cmd = 3'b100;
                15'b111101xxxxxxxxx :
                    order_cmd = 3'b101;     //sdc1/sdc2
                15'b111110xxxxxxxxx :
                    order_cmd = 3'b101;
                default :
                    order_cmd = 3'b000;
                endcase
        case(order_cmd)
                3'b001 :
                        ir_out = {ir_in[31:21],ir_in[15:11],ir_in[20:16],ir_in[10:0]};
                3'b010 :
                        ir_out = {ir_in[31:16],ir_in[10:6],ir_in[15:11],ir_in[5:0]};
                3'b011 :
                        ir_out = {ir_in[31:16],ir_in[10:6],ir_in[11],ir_in[15:12],ir_in[5:0]};
                3'b100 :
                        ir_out = {ir_in[31:21],ir_in[11],ir_in[15:12],ir_in[20:16],ir_in[10:0]};
                3'b101 :
                        ir_out = {ir_in[31:21],ir_in[16],ir_in[20:17],ir_in[15:0]};
                3'b110 :
                        ir_out = {ir_in[31:21],ir_in[15:6],ir_in[20:16],ir_in[5:0]};
                default :
                        ir_out = ir_in;
        endcase

end
endtask

//################################################################
//####	Monitor the simulation End condition 				######
//####	Compatible with SH, yuky, 2002-07-31				######
//################################################################

always @(w_id_inst or pw_valid)
begin
	#1.0;
	if ((w_id_inst == 32'h3c000000 | w_id_inst == 32'h24000000) & (pw_valid))
		begin
		#5.0;
		cpu_vec_end = 1'b1;
		cpu_vec_00_end = 1'b1;
		cpu_vec_01_end = 1'b1;
		cpu_vec_02_end = 1'b1;
		cpu_vec_03_end = 1'b1;
		cpu_vec_04_end = 1'b1;
		cpu_vec_05_end = 1'b1;
		cpu_vec_06_end = 1'b1;
		cpu_vec_07_end = 1'b1;
		cpu_vec_08_end = 1'b1;
		cpu_vec_09_end = 1'b1;
		cpu_vec_10_end = 1'b1;
		cpu_vec_11_end = 1'b1;
		cpu_vec_12_end = 1'b1;
		cpu_vec_13_end = 1'b1;
		cpu_vec_14_end = 1'b1;
		cpu_vec_15_end = 1'b1;
		cpu_vec_16_end = 1'b1;
		cpu_vec_17_end = 1'b1;
		cpu_vec_18_end = 1'b1;
		cpu_vec_19_end = 1'b1;
		cpu_vec_20_end = 1'b1;
		end
end


/*-----------------------------------------------------
                 intial cache
-------------------------------------------------------*/
/*
always @(posedge rst_) begin
#10
    for(i=0; i<512; i=i+1) begin
	`cpu_path.d_tag_0.mem[i] = 24'b0;
	`cpu_path.d_tag_1.mem[i] = 24'b0;
	`cpu_path.i_tag_0.mem[i] = 23'b0;
	`cpu_path.i_tag_1.mem[i] = 23'b0;
    end
end
*/
/*
always @(posedge rst_) begin
#10
    for(i=0; i<512; i=i+1) begin
	`cpu_path.d_tag_0.u0.mem_core_array[i] = 24'b0;
	`cpu_path.d_tag_1.u0.mem_core_array[i] = 24'b0;
	`cpu_path.i_tag_0.u0.mem_core_array[i] = 22'b0;
	`cpu_path.i_tag_1.u0.mem_core_array[i] = 22'b0;
    end
end

*/
/*
`ifdef	CPU_RTL_INI
//################################################################
//####	Initial TLB, Cache, RF, VRF			 				######
//####	Migrate from SH Env. yuky, 2002-07-31				######
//################################################################
`ifdef	POST_GSIM
`define         mmu_path `top_path.T2_MMU
`else
`define         mmu_path `top_path.t2_mmu
`endif
*/
/*		//initial in the program
parameter PCLK_PERIOD = 4.0;
wire	rst_ = cold_reset_;
// tlb
always@ (negedge rst_)
#100
    begin
    #PCLK_PERIOD
    #PCLK_PERIOD
    #PCLK_PERIOD
    #2
    `ifdef	POST_GSIM
        force  `mmu_path.JTLB_TLBWR    = 1'b1;
        force  `mmu_path.TLBW          = 1'b1;
        force  `mmu_path.JTLB_RW_INDEX = 5'b00000;
        force  `mmu_path.JTLB_WR_DATA  = 90'h3_f000_8004_0008_0000_010a;
    #PCLK_PERIOD
        force  `mmu_path.JTLB_RW_INDEX = 5'b00001;
        force  `mmu_path.JTLB_WR_DATA  = 90'h3_c000_8004_0000_0000_c035;
    #PCLK_PERIOD
        force  `mmu_path.JTLB_RW_INDEX = 5'b00010;
        force  `mmu_path.JTLB_WR_DATA  = 90'hf_c002_0004_0020_0003_019f;
    #PCLK_PERIOD
        force  `mmu_path.JTLB_RW_INDEX = 5'b00011;
        force  `mmu_path.JTLB_WR_DATA  = 90'h3_c004_0004_0040_0004_014f;
    #PCLK_PERIOD
        force  `mmu_path.JTLB_RW_INDEX = 5'b00100;
        force  `mmu_path.JTLB_WR_DATA  = 90'h3_c004_8004_0048_0000_000a;
    #PCLK_PERIOD
        force  `mmu_path.JTLB_RW_INDEX = 5'b00101;
        force  `mmu_path.JTLB_WR_DATA  = 90'h2ff0_0000_0000_0000_0000;
    #PCLK_PERIOD
        force  `mmu_path.JTLB_RW_INDEX = 5'b00110;
        force  `mmu_path.JTLB_WR_DATA  = 90'h2ff0_0000_0000_0000_0000;
    #PCLK_PERIOD
        force  `mmu_path.JTLB_RW_INDEX = 5'b00111;
        force  `mmu_path.JTLB_WR_DATA  = 90'h2ff0_0000_0000_0000_0000;
    #PCLK_PERIOD
        force  `mmu_path.JTLB_RW_INDEX = 5'b01000;
        force  `mmu_path.JTLB_WR_DATA  = 90'h2ff0_0000_0000_0000_0000;
    #PCLK_PERIOD
        force  `mmu_path.JTLB_RW_INDEX = 5'b01001;
        force  `mmu_path.JTLB_WR_DATA  = 90'h2ff0_0000_0000_0000_0000;
    #PCLK_PERIOD
        force  `mmu_path.JTLB_RW_INDEX = 5'b01010;
        force  `mmu_path.JTLB_WR_DATA  = 90'h2ff0_0000_0000_0000_0000;
    #PCLK_PERIOD
        force  `mmu_path.JTLB_RW_INDEX = 5'b01011;
        force  `mmu_path.JTLB_WR_DATA  = 90'h2ff0_0000_0000_0000_0000;
    #PCLK_PERIOD
        force  `mmu_path.JTLB_RW_INDEX = 5'b01100;
        force  `mmu_path.JTLB_WR_DATA  = 90'h2ff0_0000_0000_0000_0000;
    #PCLK_PERIOD
        force  `mmu_path.JTLB_RW_INDEX = 5'b01101;
        force  `mmu_path.JTLB_WR_DATA  = 90'h2ff0_0000_0000_0000_0000;
    #PCLK_PERIOD
        force  `mmu_path.JTLB_RW_INDEX = 5'b01110;
        force  `mmu_path.JTLB_WR_DATA  = 90'h2ff0_0000_0000_0000_0000;
    #PCLK_PERIOD
        force  `mmu_path.JTLB_RW_INDEX = 5'b01111;
        force  `mmu_path.JTLB_WR_DATA  = 90'h2ff0_0000_0000_0000_0000;
    #PCLK_PERIOD
        force  `mmu_path.JTLB_RW_INDEX = 5'b10000;
        force  `mmu_path.JTLB_WR_DATA  = 90'h2ff0_0000_0000_0000_0000;
    #PCLK_PERIOD
        force  `mmu_path.JTLB_RW_INDEX = 5'b10001;
        force  `mmu_path.JTLB_WR_DATA  = 90'h2ff0_0000_0000_0000_0000;
    #PCLK_PERIOD
        force  `mmu_path.JTLB_RW_INDEX = 5'b10010;
        force  `mmu_path.JTLB_WR_DATA  = 90'h2ff0_0000_0000_0000_0000;
    #PCLK_PERIOD
        force  `mmu_path.JTLB_RW_INDEX = 5'b10011;
        force  `mmu_path.JTLB_WR_DATA  = 90'h2ff0_0000_0000_0000_0000;
    #PCLK_PERIOD
        force  `mmu_path.JTLB_RW_INDEX = 5'b10100;
        force  `mmu_path.JTLB_WR_DATA  = 90'h2ff0_0000_0000_0000_0000;
    #PCLK_PERIOD
        force  `mmu_path.JTLB_RW_INDEX = 5'b10101;
        force  `mmu_path.JTLB_WR_DATA  = 90'h2ff0_0000_0000_0000_0000;
    #PCLK_PERIOD
        force  `mmu_path.JTLB_RW_INDEX = 5'b10110;
        force  `mmu_path.JTLB_WR_DATA  = 90'h2ff0_0000_0000_0000_0000;
    #PCLK_PERIOD
        force  `mmu_path.JTLB_RW_INDEX = 5'b10111;
        force  `mmu_path.JTLB_WR_DATA  = 90'h2ff0_0000_0000_0000_0000;
    #PCLK_PERIOD
        force  `mmu_path.JTLB_RW_INDEX = 5'b11000;
        force  `mmu_path.JTLB_WR_DATA  = 90'h2ff0_0000_0000_0000_0000;
    #PCLK_PERIOD
        force  `mmu_path.JTLB_RW_INDEX = 5'b11001;
        force  `mmu_path.JTLB_WR_DATA  = 90'h2ff0_0000_0000_0000_0000;
    #PCLK_PERIOD
        force  `mmu_path.JTLB_RW_INDEX = 5'b11010;
        force  `mmu_path.JTLB_WR_DATA  = 90'h2ff0_0000_0000_0000_0000;
    #PCLK_PERIOD
        force  `mmu_path.JTLB_RW_INDEX = 5'b11011;
        force  `mmu_path.JTLB_WR_DATA  = 90'h2ff0_0000_0000_0000_0000;
    #PCLK_PERIOD
        force  `mmu_path.JTLB_RW_INDEX = 5'b11100;
        force  `mmu_path.JTLB_WR_DATA  = 90'h2ff0_0000_0000_0000_0000;
    #PCLK_PERIOD
        force  `mmu_path.JTLB_RW_INDEX = 5'b11101;
        force  `mmu_path.JTLB_WR_DATA  = 90'h2ff0_0000_0000_0000_0000;
    #PCLK_PERIOD
        force  `mmu_path.JTLB_RW_INDEX = 5'b11110;
        force  `mmu_path.JTLB_WR_DATA  = 90'h2ff0_0000_0000_0000_0000;
    #PCLK_PERIOD
        force  `mmu_path.JTLB_RW_INDEX = 5'b11111;
        force  `mmu_path.JTLB_WR_DATA  = 90'h2ff0_0000_0000_0000_0000;
    #PCLK_PERIOD
    release    `mmu_path.JTLB_TLBWR;
    release    `mmu_path.TLBW;
    release    `mmu_path.JTLB_RW_INDEX;
    release    `mmu_path.JTLB_WR_DATA;

    `else	//rsim or pregsim
        force  `mmu_path.jtlb_tlbwr    = 1'b1;
        force  `mmu_path.tlbw          = 1'b1;
        force  `mmu_path.jtlb_rw_index = 5'b00000;
        force  `mmu_path.jtlb_wr_data  = 90'h3_f000_8004_0008_0000_010a;
    #PCLK_PERIOD
        force  `mmu_path.jtlb_rw_index = 5'b00001;
        force  `mmu_path.jtlb_wr_data  = 90'h3_c000_8004_0000_0000_c035;
    #PCLK_PERIOD
        force  `mmu_path.jtlb_rw_index = 5'b00010;
        force  `mmu_path.jtlb_wr_data  = 90'hf_c002_0004_0020_0003_019f;
    #PCLK_PERIOD
        force  `mmu_path.jtlb_rw_index = 5'b00011;
        force  `mmu_path.jtlb_wr_data  = 90'h3_c004_0004_0040_0004_014f;
    #PCLK_PERIOD
        force  `mmu_path.jtlb_rw_index = 5'b00100;
        force  `mmu_path.jtlb_wr_data  = 90'h3_c004_8004_0048_0000_000a;
    #PCLK_PERIOD
        force  `mmu_path.jtlb_rw_index = 5'b00101;
        force  `mmu_path.jtlb_wr_data  = 90'h2ff0_0000_0000_0000_0000;
    #PCLK_PERIOD
        force  `mmu_path.jtlb_rw_index = 5'b00110;
        force  `mmu_path.jtlb_wr_data  = 90'h2ff0_0000_0000_0000_0000;
    #PCLK_PERIOD
        force  `mmu_path.jtlb_rw_index = 5'b00111;
        force  `mmu_path.jtlb_wr_data  = 90'h2ff0_0000_0000_0000_0000;
    #PCLK_PERIOD
        force  `mmu_path.jtlb_rw_index = 5'b01000;
        force  `mmu_path.jtlb_wr_data  = 90'h2ff0_0000_0000_0000_0000;
    #PCLK_PERIOD
        force  `mmu_path.jtlb_rw_index = 5'b01001;
        force  `mmu_path.jtlb_wr_data  = 90'h2ff0_0000_0000_0000_0000;
    #PCLK_PERIOD
        force  `mmu_path.jtlb_rw_index = 5'b01010;
        force  `mmu_path.jtlb_wr_data  = 90'h2ff0_0000_0000_0000_0000;
    #PCLK_PERIOD
        force  `mmu_path.jtlb_rw_index = 5'b01011;
        force  `mmu_path.jtlb_wr_data  = 90'h2ff0_0000_0000_0000_0000;
    #PCLK_PERIOD
        force  `mmu_path.jtlb_rw_index = 5'b01100;
        force  `mmu_path.jtlb_wr_data  = 90'h2ff0_0000_0000_0000_0000;
    #PCLK_PERIOD
        force  `mmu_path.jtlb_rw_index = 5'b01101;
        force  `mmu_path.jtlb_wr_data  = 90'h2ff0_0000_0000_0000_0000;
    #PCLK_PERIOD
        force  `mmu_path.jtlb_rw_index = 5'b01110;
        force  `mmu_path.jtlb_wr_data  = 90'h2ff0_0000_0000_0000_0000;
    #PCLK_PERIOD
        force  `mmu_path.jtlb_rw_index = 5'b01111;
        force  `mmu_path.jtlb_wr_data  = 90'h2ff0_0000_0000_0000_0000;
    #PCLK_PERIOD
        force  `mmu_path.jtlb_rw_index = 5'b10000;
        force  `mmu_path.jtlb_wr_data  = 90'h2ff0_0000_0000_0000_0000;
    #PCLK_PERIOD
        force  `mmu_path.jtlb_rw_index = 5'b10001;
        force  `mmu_path.jtlb_wr_data  = 90'h2ff0_0000_0000_0000_0000;
    #PCLK_PERIOD
        force  `mmu_path.jtlb_rw_index = 5'b10010;
        force  `mmu_path.jtlb_wr_data  = 90'h2ff0_0000_0000_0000_0000;
    #PCLK_PERIOD
        force  `mmu_path.jtlb_rw_index = 5'b10011;
        force  `mmu_path.jtlb_wr_data  = 90'h2ff0_0000_0000_0000_0000;
    #PCLK_PERIOD
        force  `mmu_path.jtlb_rw_index = 5'b10100;
        force  `mmu_path.jtlb_wr_data  = 90'h2ff0_0000_0000_0000_0000;
    #PCLK_PERIOD
        force  `mmu_path.jtlb_rw_index = 5'b10101;
        force  `mmu_path.jtlb_wr_data  = 90'h2ff0_0000_0000_0000_0000;
    #PCLK_PERIOD
        force  `mmu_path.jtlb_rw_index = 5'b10110;
        force  `mmu_path.jtlb_wr_data  = 90'h2ff0_0000_0000_0000_0000;
    #PCLK_PERIOD
        force  `mmu_path.jtlb_rw_index = 5'b10111;
        force  `mmu_path.jtlb_wr_data  = 90'h2ff0_0000_0000_0000_0000;
    #PCLK_PERIOD
        force  `mmu_path.jtlb_rw_index = 5'b11000;
        force  `mmu_path.jtlb_wr_data  = 90'h2ff0_0000_0000_0000_0000;
    #PCLK_PERIOD
        force  `mmu_path.jtlb_rw_index = 5'b11001;
        force  `mmu_path.jtlb_wr_data  = 90'h2ff0_0000_0000_0000_0000;
    #PCLK_PERIOD
        force  `mmu_path.jtlb_rw_index = 5'b11010;
        force  `mmu_path.jtlb_wr_data  = 90'h2ff0_0000_0000_0000_0000;
    #PCLK_PERIOD
        force  `mmu_path.jtlb_rw_index = 5'b11011;
        force  `mmu_path.jtlb_wr_data  = 90'h2ff0_0000_0000_0000_0000;
    #PCLK_PERIOD
        force  `mmu_path.jtlb_rw_index = 5'b11100;
        force  `mmu_path.jtlb_wr_data  = 90'h2ff0_0000_0000_0000_0000;
    #PCLK_PERIOD
        force  `mmu_path.jtlb_rw_index = 5'b11101;
        force  `mmu_path.jtlb_wr_data  = 90'h2ff0_0000_0000_0000_0000;
    #PCLK_PERIOD
        force  `mmu_path.jtlb_rw_index = 5'b11110;
        force  `mmu_path.jtlb_wr_data  = 90'h2ff0_0000_0000_0000_0000;
    #PCLK_PERIOD
        force  `mmu_path.jtlb_rw_index = 5'b11111;
        force  `mmu_path.jtlb_wr_data  = 90'h2ff0_0000_0000_0000_0000;
    #PCLK_PERIOD
    release    `mmu_path.jtlb_tlbwr;
    release    `mmu_path.tlbw;
    release    `mmu_path.jtlb_rw_index;
    release    `mmu_path.jtlb_wr_data;
    `endif
    end
//initial jtlb end
*/
/*
//initial cache
`ifdef	POST_GSIM
`define d_cachefile_path `top_path.T2_D_CACHE.D_CACHEFILE_1
`define path_i_tag 		 `top_path.T2_IFETCH.I_CACHEFILE_1
`else
`ifdef	CPU_NET
`define d_cachefile_path `top_path.T2_D_CACHE.D_CACHEFILE_1
`define path_i_tag 		 `top_path.T2_IFETCH.I_CACHEFILE_1
`else
`define d_cachefile_path `top_path.t2_d_cache.d_cachefile_1
`define path_i_tag 		 `top_path.t2_ifetch.i_cachefile_1
`endif
`endif

always @(posedge cold_reset_)
begin
#5

        for (i=0;i<128;i=i+1)
		begin
		`ifdef POST_GSIM
		  `d_cachefile_path.DC_TAG_0.dc_tag_array.tag_way0.tag_content[i] = 20'b0;
		  `d_cachefile_path.DC_TAG_0.dc_tag_array.tag_way0.tag_state[i]   = 7'b0;
		  `d_cachefile_path.DC_TAG_0.dc_tag_array.tag_way1.tag_content[i] = 20'b0;
		  `d_cachefile_path.DC_TAG_0.dc_tag_array.tag_way1.tag_state[i]   = 7'b0;
		  `d_cachefile_path.DC_TAG_0.dc_tag_array.tag_way2.tag_content[i] = 20'b0;
		  `d_cachefile_path.DC_TAG_0.dc_tag_array.tag_way2.tag_state[i]   = 7'b0;
		  `d_cachefile_path.DC_TAG_0.dc_tag_array.tag_way3.tag_content[i] = 20'b0;
		  `d_cachefile_path.DC_TAG_0.dc_tag_array.tag_way3.tag_state[i]   = 7'b0;
          `d_cachefile_path.DC_TAG_0.dc_tag_array.dc_lru_1.lru[i]         = 2'b00;
		  `d_cachefile_path.DC_TAG_1.dc_tag_array.tag_way0.tag_content[i] = 20'b0;
		  `d_cachefile_path.DC_TAG_1.dc_tag_array.tag_way0.tag_state[i]   = 7'b0;
		  `d_cachefile_path.DC_TAG_1.dc_tag_array.tag_way1.tag_content[i] = 20'b0;
		  `d_cachefile_path.DC_TAG_1.dc_tag_array.tag_way1.tag_state[i]   = 7'b0;
		  `d_cachefile_path.DC_TAG_1.dc_tag_array.tag_way2.tag_content[i] = 20'b0;
		  `d_cachefile_path.DC_TAG_1.dc_tag_array.tag_way2.tag_state[i]   = 7'b0;
		  `d_cachefile_path.DC_TAG_1.dc_tag_array.tag_way3.tag_content[i] = 20'b0;
		  `d_cachefile_path.DC_TAG_1.dc_tag_array.tag_way3.tag_state[i]   = 7'b0;
          `d_cachefile_path.DC_TAG_1.dc_tag_array.dc_lru_1.lru[i]         = 2'b00;
		`else
		`ifdef	CPU_NET
		  `d_cachefile_path.DC_TAG_0.dc_tag_array.tag_way0.tag_content[i] = 20'b0;
		  `d_cachefile_path.DC_TAG_0.dc_tag_array.tag_way0.tag_state[i]   = 7'b0;
		  `d_cachefile_path.DC_TAG_0.dc_tag_array.tag_way1.tag_content[i] = 20'b0;
		  `d_cachefile_path.DC_TAG_0.dc_tag_array.tag_way1.tag_state[i]   = 7'b0;
		  `d_cachefile_path.DC_TAG_0.dc_tag_array.tag_way2.tag_content[i] = 20'b0;
		  `d_cachefile_path.DC_TAG_0.dc_tag_array.tag_way2.tag_state[i]   = 7'b0;
		  `d_cachefile_path.DC_TAG_0.dc_tag_array.tag_way3.tag_content[i] = 20'b0;
		  `d_cachefile_path.DC_TAG_0.dc_tag_array.tag_way3.tag_state[i]   = 7'b0;
          `d_cachefile_path.DC_TAG_0.dc_tag_array.dc_lru_1.lru[i]         = 2'b00;
		  `d_cachefile_path.DC_TAG_1.dc_tag_array.tag_way0.tag_content[i] = 20'b0;
		  `d_cachefile_path.DC_TAG_1.dc_tag_array.tag_way0.tag_state[i]   = 7'b0;
		  `d_cachefile_path.DC_TAG_1.dc_tag_array.tag_way1.tag_content[i] = 20'b0;
		  `d_cachefile_path.DC_TAG_1.dc_tag_array.tag_way1.tag_state[i]   = 7'b0;
		  `d_cachefile_path.DC_TAG_1.dc_tag_array.tag_way2.tag_content[i] = 20'b0;
		  `d_cachefile_path.DC_TAG_1.dc_tag_array.tag_way2.tag_state[i]   = 7'b0;
		  `d_cachefile_path.DC_TAG_1.dc_tag_array.tag_way3.tag_content[i] = 20'b0;
		  `d_cachefile_path.DC_TAG_1.dc_tag_array.tag_way3.tag_state[i]   = 7'b0;
          `d_cachefile_path.DC_TAG_1.dc_tag_array.dc_lru_1.lru[i]         = 2'b00;
		`else
		  `d_cachefile_path.dc_tag_0.dc_tag_array.tag_way0.tag_content[i] = 20'b0;
		  `d_cachefile_path.dc_tag_0.dc_tag_array.tag_way0.tag_state[i]   = 7'b0;
		  `d_cachefile_path.dc_tag_0.dc_tag_array.tag_way1.tag_content[i] = 20'b0;
		  `d_cachefile_path.dc_tag_0.dc_tag_array.tag_way1.tag_state[i]   = 7'b0;
		  `d_cachefile_path.dc_tag_0.dc_tag_array.tag_way2.tag_content[i] = 20'b0;
		  `d_cachefile_path.dc_tag_0.dc_tag_array.tag_way2.tag_state[i]   = 7'b0;
		  `d_cachefile_path.dc_tag_0.dc_tag_array.tag_way3.tag_content[i] = 20'b0;
		  `d_cachefile_path.dc_tag_0.dc_tag_array.tag_way3.tag_state[i]   = 7'b0;
          `d_cachefile_path.dc_tag_0.dc_tag_array.dc_lru_1.lru[i]         = 2'b00;
		  `d_cachefile_path.dc_tag_1.dc_tag_array.tag_way0.tag_content[i] = 20'b0;
		  `d_cachefile_path.dc_tag_1.dc_tag_array.tag_way0.tag_state[i]   = 7'b0;
		  `d_cachefile_path.dc_tag_1.dc_tag_array.tag_way1.tag_content[i] = 20'b0;
		  `d_cachefile_path.dc_tag_1.dc_tag_array.tag_way1.tag_state[i]   = 7'b0;
		  `d_cachefile_path.dc_tag_1.dc_tag_array.tag_way2.tag_content[i] = 20'b0;
		  `d_cachefile_path.dc_tag_1.dc_tag_array.tag_way2.tag_state[i]   = 7'b0;
		  `d_cachefile_path.dc_tag_1.dc_tag_array.tag_way3.tag_content[i] = 20'b0;
		  `d_cachefile_path.dc_tag_1.dc_tag_array.tag_way3.tag_state[i]   = 7'b0;
          `d_cachefile_path.dc_tag_1.dc_tag_array.dc_lru_1.lru[i]         = 2'b00;
        `endif
        `endif
		end

#5		for (i=0;i<128;i=i+1) begin
		`ifdef	POST_GSIM
		    `path_i_tag.I_TAG.IC_TAG_0.ic_tag0.tag_content[i] = 20'b0;
			`path_i_tag.I_TAG.IC_TAG_0.ic_tag0.tag_state[i]   = 2'b0;
			`path_i_tag.I_TAG.IC_TAG_0.ic_tag1.tag_content[i] = 20'b0;
			`path_i_tag.I_TAG.IC_TAG_0.ic_tag1.tag_state[i]   = 2'b0;
			`path_i_tag.I_TAG.IC_TAG_0.ic_tag2.tag_content[i] = 20'b0;
			`path_i_tag.I_TAG.IC_TAG_0.ic_tag2.tag_state[i]   = 2'b0;
			`path_i_tag.I_TAG.IC_TAG_0.ic_tag3.tag_content[i] = 20'b0;
			`path_i_tag.I_TAG.IC_TAG_0.ic_tag3.tag_state[i]   = 2'b0;
			`path_i_tag.I_TAG.IC_TAG_0.ic_lru_0.lru_mem[i]    = 2'b0;
			`path_i_tag.I_TAG.IC_TAG_1.ic_tag0.tag_content[i] = 20'b0;
			`path_i_tag.I_TAG.IC_TAG_1.ic_tag0.tag_state[i]   = 2'b0;
			`path_i_tag.I_TAG.IC_TAG_1.ic_tag1.tag_content[i] = 20'b0;
			`path_i_tag.I_TAG.IC_TAG_1.ic_tag1.tag_state[i]   = 2'b0;
			`path_i_tag.I_TAG.IC_TAG_1.ic_tag2.tag_content[i] = 20'b0;
			`path_i_tag.I_TAG.IC_TAG_1.ic_tag2.tag_state[i]   = 2'b0;
			`path_i_tag.I_TAG.IC_TAG_1.ic_tag3.tag_content[i] = 20'b0;
			`path_i_tag.I_TAG.IC_TAG_1.ic_tag3.tag_state[i]   = 2'b0;
			`path_i_tag.I_TAG.IC_TAG_1.ic_lru_0.lru_mem[i]    = 2'b0;
		`else
		`ifdef	CPU_NET
		    `path_i_tag.I_TAG.IC_TAG_0.ic_tag0.tag_content[i] = 20'b0;
			`path_i_tag.I_TAG.IC_TAG_0.ic_tag0.tag_state[i]   = 2'b0;
			`path_i_tag.I_TAG.IC_TAG_0.ic_tag1.tag_content[i] = 20'b0;
			`path_i_tag.I_TAG.IC_TAG_0.ic_tag1.tag_state[i]   = 2'b0;
			`path_i_tag.I_TAG.IC_TAG_0.ic_tag2.tag_content[i] = 20'b0;
			`path_i_tag.I_TAG.IC_TAG_0.ic_tag2.tag_state[i]   = 2'b0;
			`path_i_tag.I_TAG.IC_TAG_0.ic_tag3.tag_content[i] = 20'b0;
			`path_i_tag.I_TAG.IC_TAG_0.ic_tag3.tag_state[i]   = 2'b0;
			`path_i_tag.I_TAG.IC_TAG_0.ic_lru_0.lru_mem[i]    = 2'b0;
			`path_i_tag.I_TAG.IC_TAG_1.ic_tag0.tag_content[i] = 20'b0;
			`path_i_tag.I_TAG.IC_TAG_1.ic_tag0.tag_state[i]   = 2'b0;
			`path_i_tag.I_TAG.IC_TAG_1.ic_tag1.tag_content[i] = 20'b0;
			`path_i_tag.I_TAG.IC_TAG_1.ic_tag1.tag_state[i]   = 2'b0;
			`path_i_tag.I_TAG.IC_TAG_1.ic_tag2.tag_content[i] = 20'b0;
			`path_i_tag.I_TAG.IC_TAG_1.ic_tag2.tag_state[i]   = 2'b0;
			`path_i_tag.I_TAG.IC_TAG_1.ic_tag3.tag_content[i] = 20'b0;
			`path_i_tag.I_TAG.IC_TAG_1.ic_tag3.tag_state[i]   = 2'b0;
			`path_i_tag.I_TAG.IC_TAG_1.ic_lru_0.lru_mem[i]    = 2'b0;
		`else
			`path_i_tag.i_tag.ic_tag_0.ic_tag0.tag_content[i] = 20'b0;
			`path_i_tag.i_tag.ic_tag_0.ic_tag0.tag_state[i]   = 2'b0;
			`path_i_tag.i_tag.ic_tag_0.ic_tag1.tag_content[i] = 20'b0;
			`path_i_tag.i_tag.ic_tag_0.ic_tag1.tag_state[i]   = 2'b0;
			`path_i_tag.i_tag.ic_tag_0.ic_tag2.tag_content[i] = 20'b0;
			`path_i_tag.i_tag.ic_tag_0.ic_tag2.tag_state[i]   = 2'b0;
			`path_i_tag.i_tag.ic_tag_0.ic_tag3.tag_content[i] = 20'b0;
			`path_i_tag.i_tag.ic_tag_0.ic_tag3.tag_state[i]   = 2'b0;
			`path_i_tag.i_tag.ic_tag_0.ic_lru_0.lru_mem[i]    = 2'b0;
			`path_i_tag.i_tag.ic_tag_1.ic_tag0.tag_content[i] = 20'b0;
			`path_i_tag.i_tag.ic_tag_1.ic_tag0.tag_state[i]   = 2'b0;
			`path_i_tag.i_tag.ic_tag_1.ic_tag1.tag_content[i] = 20'b0;
			`path_i_tag.i_tag.ic_tag_1.ic_tag1.tag_state[i]   = 2'b0;
			`path_i_tag.i_tag.ic_tag_1.ic_tag2.tag_content[i] = 20'b0;
			`path_i_tag.i_tag.ic_tag_1.ic_tag2.tag_state[i]   = 2'b0;
			`path_i_tag.i_tag.ic_tag_1.ic_tag3.tag_content[i] = 20'b0;
			`path_i_tag.i_tag.ic_tag_1.ic_tag3.tag_state[i]   = 2'b0;
			`path_i_tag.i_tag.ic_tag_1.ic_lru_0.lru_mem[i]    = 2'b0;
		`endif
		`endif
		end
end

//initial rf and vrf
`ifdef	POST_GSIM
`define	rf_path 	`top_path.T2_REG_FILE
`define vrf_path 	`top_path.T2_VRF
`else
`ifdef	CPU_NET
`define	rf_path 	`top_path.T2_REG_FILE
`define vrf_path 	`top_path.T2_VRF
`else
`define	rf_path 	`top_path.t2_reg_file
`define vrf_path 	`top_path.t2_vrf
`endif
`endif

always @(posedge cold_reset_) begin
	#5
	for(i=0;i<32;i=i+1)begin
	`ifdef	POST_GSIM
	   `rf_path.GPR.rf_array.rf_array[i] 	= 32'b0;
       `vrf_path.ERFH.rf_array.rf_array[i] 	= 32'b0;
       `vrf_path.ERFL.rf_array.rf_array[i] 	= 32'b0;
	`else
	`ifdef	CPU_NET
	   `rf_path.GPR.rf_array.rf_array[i] 	= 32'b0;
       `vrf_path.ERFH.rf_array.rf_array[i] 	= 32'b0;
       `vrf_path.ERFL.rf_array.rf_array[i] 	= 32'b0;
	`else
	   `rf_path.gpr.rf_array.rf_array[i] 	= 32'b0;
       `vrf_path.ERFH.rf_array.rf_array[i] 	= 32'b0;
       `vrf_path.ERFL.rf_array.rf_array[i] 	= 32'b0;
    `endif
    `endif
     end
end
//##################################################
`endif
*/