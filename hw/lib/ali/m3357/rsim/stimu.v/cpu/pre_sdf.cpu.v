
/*===========================================================
file:			pre_sdf.cpu.v
description:	add your ip sdf file here for prelayout gsim
============================================================*/

initial begin
//Syntax
//$sdf_annotate( "sdf_file", module_instance, "config_file", "log_file",
//				 "mtm_spec", "scale_factors", "scale_type" );
//Sample
//$sdf_annotate	("../../../gsrc/metal_fix/chip.sdf", CHIP,	"../../../gsrc/sdf.config", "chip_sdf.log",
//				 "MAXIMUM", "1.0:1.0:1.0", 	"FROM_MAXIMUM");

`ifdef	INC_CPU //add CPU sdf file annotation here

$sdf_annotate	("../../../gsrc/prelayout/cpu_core/t2_cpu.sdf",
//$sdf_annotate	("./t2_cpu.sdf",
				top.chip.core.T2_CPU,
				,
				"t2_cpu_sdf.log",
				 "MAXIMUM", "1.0:1.0:1.0", 	"FROM_MAXIMUM");
`endif
end
