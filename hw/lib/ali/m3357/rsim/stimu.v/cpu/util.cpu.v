/**************************************
task list:
	int_processor;
	err_int_processor;
	ext00_int_processor;
	ext01_int_processor;
	ext02_int_processor;
	ext03_int_processor;
	ext04_int_processor;
	ext05_int_processor;
	ext06_int_processor;
	ext07_int_processor;
	ext08_int_processor;
	ext09_int_processor;
	ext10_int_processor;
	ext11_int_processor;
	ext12_int_processor;
	ext13_int_processor;
	ext14_int_processor;
	ext15_int_processor;
	ext16_int_processor;
	ext17_int_processor;
	ext18_int_processor;
	ext19_int_processor;

	int_enable;		//open all interrupt mask
	int_disable;	//close all interrupt mask
	int_mask_en;	//mask the interrupt what you want

	u_latency_set;  //set the latency between two operation of cpu interface
	priority_set; //reorder the priority of the 16 CPU general processing

	cpu_alone_bootrom;//load the boot program to flash of the chipset behavior in the cpu mode
	cpu_alone_sdram;//load the program to sdram of the chipset behavior in the cpu mode
	ice_ram_initial;
	cpu_bist_enable;
******************************************/
//snoop the interrupt of the cpu behavior model
`ifdef NO_CPU
always @(`CPU_BH.int_flag)
	if(`CPU_BH.int_flag) begin
		int_end = 0;
		int_processor;
		int_end = 1;
	end
`endif

task int_processor;
reg		err_int_flag;
reg		ext_int_flag;
reg	[31:0]	ext_int_info;
reg	[31:0]	err_int_mask;
reg	[31:0]	ext_int_mask;
begin
`ifdef	NO_CPU
	int_req_lock; //lock the common processing can start
	`CPU_BH.int_check	(err_int_flag, ext_int_flag);
	if (err_int_flag) begin
		$display("\n\tThe CPU BUS error interrupt Acknowledge!!!");
		int_cpu_readbk1w	(CPU_IOBase + 32'h38, 4'b0001, err_int_mask);//save current mask
		int_cpu_readbk1w	(CPU_IOBase + 32'h3c, 4'b1111, ext_int_mask);//save current mask
		int_disable;//disable all Int mask
		err_int_processor;//error int processing
		int_mask_en(err_int_mask[1],ext_int_mask[19:0]);//restore int mask
	end
	else if (ext_int_flag) begin
		$display("\n\tThe external interrupt Acknowledge!!!");
		int_cpu_readbk1w 	(CPU_IOBase + 32'h38, 4'b1110, ext_int_info);//read back the external Int Info
		int_cpu_readbk1w	(CPU_IOBase + 32'h38, 4'b0001, err_int_mask);//save current mask
		int_cpu_readbk1w	(CPU_IOBase + 32'h3c, 4'b1111, ext_int_mask);//save current mask
//		int_disable;//disable all Int mask
		if (ext_int_info[0])
			ext00_int_processor;
		else if (ext_int_info[1])
			ext01_int_processor;
		else if (ext_int_info[2])
			ext02_int_processor;
		else if (ext_int_info[3])
			ext03_int_processor;
		else if (ext_int_info[4])
			ext04_int_processor;
		else if (ext_int_info[5])
			ext05_int_processor;
		else if (ext_int_info[6])
			ext06_int_processor;
		else if (ext_int_info[7])
			ext07_int_processor;
		else if (ext_int_info[8])
			ext08_int_processor;
		else if (ext_int_info[9])
			ext09_int_processor;
		else if (ext_int_info[10])
			ext10_int_processor;
		else if (ext_int_info[11])
			ext11_int_processor;
//		else if (ext_int_info[20])
//			ext12_int_processor;
//		else if (ext_int_info[21])
//			ext13_int_processor;
//		else if (ext_int_info[22])
//			ext14_int_processor;
//		else if (ext_int_info[23])
//			ext15_int_processor;
//		else if (ext_int_info[24])
//			ext16_int_processor;
//		else if (ext_int_info[25])
//			ext17_int_processor;
//		else if (ext_int_info[26])
//			ext18_int_processor;
//		else if (ext_int_info[27])
//			ext19_int_processor;
//28-->31 have been reserved
//		else if (ext_int_info[28])
//			ext14_int_processor;
//		else if (ext_int_info[29])
//			ext15_int_processor;
//		else if (ext_int_info[30])
//			ext16_int_processor;
//		else if (ext_int_info[31])
//			ext17_int_processor;

		int_mask_en(err_int_mask[1],ext_int_mask[19:0]);//restore int mask
	end
	int_req_unlock;//unlock the common processing
`endif
end
endtask //tasn end
//================================================================================

//======== the int processer sub tasks ===========================================
//--CPU Bus error interrupt processing
task err_int_processor;
reg	[31:0]	err_status;
begin
`ifdef NO_CPU
	$display ("\tThe CPU Bus Error Interrupt is processing, Please Wait!!!");
	int_cpu_readbk1w(CPU_IOBase + 32'h34, 4'b1111, err_status);
	case (err_status[1:0])
	2'b00:	begin
			$display("\tCPU access SDRAM error-->\tADDR =%h",{err_status[31:2],2'b00});
			$display("\tThe address is outside of the SDRAM defined space");
			end
	2'b01:	begin
			$display("\tCPU access PCI error-->\tADDR =%h",{err_status[31:2],2'b00});
			$display("\tThe address is outside the window of the PCI device");
			end
	2'b10:	begin
			$display("\tCPU access Ctrl_reg error-->\tADDR =%h",{err_status[31:2],2'b00});
			$display("\tThe address is outside the size of the CTRL_REGISTER(CPU IO)");
			end
	2'b11:	begin
			$display("\tCPU access Boot_rom error-->\tADDR =%h",{err_status[31:2],2'b00});
			$display("\tThe address is outside the size of the FLASH space");
			end
	endcase
	int_cpu_write1w	(CPU_IOBase + 32'h38, 4'b0001, 32'h0000_0001); //Clear the interrupt
	$display ("\tThe CPU Bus Error Interrupt processing End, Return to previous Status");
`endif
end
endtask //task end

//---external interrupt processing
task ext00_int_processor;
begin
	$display("ext00 interrupt");
	pci_inta_processor;
end
endtask //task end


task ext01_int_processor;
begin
	$display("ext01 interrupt");
	gpio_int3_processor;
end
endtask //task end


task ext02_int_processor;
begin
	$display("ext02 interrupt");
	gpio_int4_processor;
end
endtask //task end

task ext03_int_processor;
begin
`ifdef INC_USB
	$display("ext08 interrupt");
	usb_int_processor;
`endif
end
endtask //task end

task ext04_int_processor;
begin
`ifdef INC_IDE
	$display("ext19 interrupt");
	ide_int_processor;
`endif
end
endtask //task end

task ext05_int_processor;
begin
`ifdef INC_SB
	$display("ext11 interrupt");
	scb_int_processor;
`endif
end
endtask //task end

task ext06_int_processor;
begin
`ifdef INC_AUDIO
	$display("ext12 interrupt");
	dsp_int_processor;
`endif
end
endtask //task end

task ext07_int_processor;
begin
`ifdef INC_SB
	$display("ext17 interrupt");
	irc_int_processor;
`endif
end
endtask //task end

task ext08_int_processor;
begin
`ifdef INC_SB
	$display("ext08 interrupt");
	uart_int_processor;
`endif
end
endtask //task end

task ext09_int_processor;
begin
`ifdef INC_VIDEO
	$display("ext10 interrupt");
	video_int_processor;
`endif
end
endtask //task end

task ext10_int_processor;
begin
`ifdef INC_AUDIO
	$display("ext12 interrupt");
	dsp_int_processor;
`endif
end
endtask //task end

task ext11_int_processor;
begin
`ifdef INC_SERVO
	$display("ext11 interrupt");
	servo_int_processor;
`endif
end
endtask //task end
/*
task ext12_int_processor;
begin
`ifdef INC_AUDIO
	$display("ext12 interrupt");
	dsp_int_processor;
`endif
end
endtask //task end
*/
/*JFR030624
task ext13_int_processor;
reg	[31:0]	mask_status;
begin
`ifdef INC_SB
	$display("ext13 interrupt");
	cd_int_processor;
`endif
end
endtask //task end

task ext14_int_processor;
begin
`ifdef INC_SB
	$display("ext14 interrupt");
	subq_int_processor;
`endif
end
endtask //task end

task ext15_int_processor;
begin
`ifdef INC_SB
	$display("ext15 interrupt");
	up_int_processor;
`endif
end
endtask //task end

task ext16_int_processor;
begin
`ifdef INC_SB
	$display("ext16 interrupt");
	spi_int_processor;
`endif
end
endtask //task end
*/
/*
task ext17_int_processor;
begin
`ifdef INC_SB
	$display("ext17 interrupt");
	irc_int_processor;
`endif
end
endtask //task end


task ext18_int_processor;
begin
`ifdef INC_DISPLAY
	$display("ext18 interrupt");
	disp_int_processor;
`endif
end
endtask //task end

task ext19_int_processor;
begin
`ifdef INC_IDE
	$display("ext19 interrupt");
	ide_int_processor;
`endif
end
endtask //task end
*/

task	int_enable;
reg	[31:0]	mask_info;
begin
`ifdef	NO_CPU
	int_cpu_readbk1w(CPU_IOBase + 32'h38, 4'b0001, mask_info);
	int_cpu_write1w(CPU_IOBase + 32'h38, 4'b0001, (mask_info | 32'h0000_0002 )); //enable CPU bus error int
	int_cpu_write1w(CPU_IOBase + 32'h3c, 4'b1111, 32'hffff_ffff);//enable external Int
`endif
end
endtask


task	int_disable;
reg	[31:0]	mask_info;
begin
`ifdef	NO_CPU
	int_cpu_readbk1w(CPU_IOBase + 32'h38, 4'b0001, mask_info);
	int_cpu_write1w(CPU_IOBase + 32'h38, 4'b0001, (mask_info & (~32'h0000_0002)));//disable CPU bus error Int
	int_cpu_write1w(CPU_IOBase + 32'h3c, 4'b1111, 32'h0000_0000);//disable external Int
`endif
end
endtask

task	int_mask_en;
input	cpu_err_en;
input	[19:0]	ext_int_en;
//ext_int_en:{IDE,Disp,IRC,SPI,up,SubQ,CDIntf,DSP,I2C,VE,GPU,USB,GPIO[9:3],P_INTA}
reg	[31:0]	mask_info;
begin
`ifdef	NO_CPU
	int_cpu_readbk1w(CPU_IOBase + 32'h38, 4'b0001, mask_info);
	int_cpu_write1w(CPU_IOBase + 32'h38, 4'b0001,{mask_info[31:2],cpu_err_en,mask_info[0]});
	int_cpu_write1w(CPU_IOBase + 32'h3c, 4'b1111, {12'h0,ext_int_en});
`endif
end
endtask

task 	u_latency_set;
input	[7:0]	lat_set;
begin
`ifdef NO_CPU
	`CPU_BH.u_latency_set(lat_set);
`endif
end
endtask

task	priority_set;
input	[4:0]	id00;
input	[4:0]	id01;
input	[4:0]	id02;
input	[4:0]	id03;
input	[4:0]	id04;
input	[4:0]	id05;
input	[4:0]	id06;
input	[4:0]	id07;
input	[4:0]	id08;
input	[4:0]	id09;
input	[4:0]	id10;
input	[4:0]	id11;
input	[4:0]	id12;
input	[4:0]	id13;
input	[4:0]	id14;
input	[4:0]	id15;
input	[4:0]	id16;
input	[4:0]	id17;
input	[4:0]	id18;
input	[4:0]	id19;
input	[4:0]	id20;
input	[4:0]	id21;
input	[4:0]	id22;
input	[4:0]	id23;
input	[4:0]	id24;
input	[4:0]	id25;
input	[4:0]	id26;
input	[4:0]	id27;
input	[4:0]	id28;
input	[4:0]	id29;
input	[4:0]	id30;
input	[4:0]	id31;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.priority_switch.priority_set(id00,id01,id02,id03,id04,id05,id06,id07,
	id08,id09,id10,id11,id12,id13,id14,id15,id16,id17,id18,id19,id20,id21,id22,id23,
	id24,id25,id26,id27,id28,id29,id30,id31);
`endif
end
endtask

task cpu_alone_bootrom;
begin
`ifdef	NO_CPU
`else
	$display("\nInitial CPU Program Data in BOOTROM in CPU stand alone");
    $readmemh("cpu_mode_code.txt", byte_array,'h0);
    for (i = 0; i < 'h200000; i = i + 4)
    	begin
    	bus_test.bootrom.rom_reg[i>>2] = {	byte_array[i+3],
        							byte_array[i+2],
        							byte_array[i+1],
        							byte_array[i]};
        end
`endif
end
endtask

task cpu_alone_sdram;
begin
`ifdef	NO_CPU
`else
	$display("\nInitial CPU Program Data in SDRAM in CPU stand alone");
    $readmemh("data.txt", byte_array,'h0);
    for (i = 0; i < 'h800000; i = i + 4)
    	begin
    	bus_test.sdram.ram3[i>>2]	=	byte_array[i+3];
        bus_test.sdram.ram2[i>>2]	=	byte_array[i+2];
        bus_test.sdram.ram1[i>>2]	=	byte_array[i+1];
        bus_test.sdram.ram0[i>>2]	=	byte_array[i];
        end
`endif
end
endtask

task ice_ram_initial;
reg     [31:0]  probrom_reg     [2*256*1024-1:0];      //2M bytes
reg     [31:0]  ram_tmp;
begin
`ifdef	NO_CPU
`else
	if (top.cpu_probe_en.nmos_ctrl) //PROBE_EN = 1
	begin
	$display("\nInitial Probe Program Data to ICE mem...\n");
	$readmemh("probe.code.map", probrom_reg, 19'h80, 19'h7ffff);//load to 0xff200200
	for(i=0; i<2*256*1024; i=i+1)
		begin
        ram_tmp = probrom_reg[i];
        top.ice_test.ice_test_rom.rom_mem[i] = { ram_tmp[7:0], ram_tmp[15:8], ram_tmp[23:16], ram_tmp[31:24] };
//        top.ice_bh.ice_ram_ctrl.ice_ram.mem[i] = { ram_tmp[7:0]};
//		top.ice_bh.ice_ram_ctrl.ice_ram.mem[i] = { ram_tmp[7:0], ram_tmp[15:8], ram_tmp[23:16], ram_tmp[31:24] };

        end
    end
`endif
end
endtask

task cpu_bist_enable;
begin
`ifdef	INC_CPU
	top.cpu_bist_ext_agent.cpu_bist_enable;
`endif
end
endtask



/* By now the trigcond will always zero
   Following is the cpu_testin's value and its test target
   cpu_testin  = 5'b00000; //TRIG_JTLB
   cpu_testin  = 5'b00001; //TRIG_DTLB
   cpu_testin  = 5'b00010; //TRIG_ITLB
   cpu_testin  = 5'b00011; //TRIG_ITAG
   cpu_testin  = 5'b00100; //TRIG_DTAG
   cpu_testin  = 5'b00101; //TRIG_IDATA
   cpu_testin  = 5'b00110; //TRIG_DDATA
   cpu_testin  = 5'b00111; //TRIG_RF_0
   cpu_testin  = 5'b01000; //TRIG_RF_1
   cpu_testin  = 5'b01001; //TRIG_RF_2
   cpu_testin  = 5'b01010; //TRIG_VRFH_0
   cpu_testin  = 5'b01011; //TRIG_VRFH_1
   cpu_testin  = 5'b01100; //TRIG_VRFH_2
   cpu_testin  = 5'b01101; //TRIG_VRFL_0
   cpu_testin  = 5'b01110; //TRIG_VRFL_1
   cpu_testin  = 5'b01111; //TRIG_VRFL_2
*/
/*
task	cpu_trigger_test;
input	[9:0]	tricond;
input	[4:0]	testin;
begin
`ifdef	INC_CPU
	`ifdef TRI_CPU
	top.cpu_trigger_ext_agent.cpu_trigger_test(tricond,testin);
	`endif
`endif
end
endtask


task	cpu_trigger_test_onetime;
input	[9:0]	tricond;
input	[4:0]	testin;
begin
`ifdef	INC_CPU
	`ifdef TRI_CPU
	top.cpu_trigger_ext_agent.cpu_trigger_test_onetime(tricond,testin);
	`endif
`endif
end
endtask
*/