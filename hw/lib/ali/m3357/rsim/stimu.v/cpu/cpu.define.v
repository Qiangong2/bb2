//=================================================================
//First must define the hierarchy path to access the low level
//tasks in the CPU behavior model
//Because in different project, the system hierarchy will be changed,
//for modify convenient, define the hierarcht path in the file head
`define CPU_BH			top.CHIP.CORE.T2_CPU	//t2risc_intf
`define	CPU_BH_TASKCORE `CPU_BH.pro_task_core

//If you don't want to invoke all the CPU BH list here, you must
//define the parameter: NO_CPU

//Because the fdisplay's handle is limited, only have 31 for user used,
//one for standard output. So if you want to use the file to record the
//operation message, you should define some parameters to open the file
//handle
/*
`define FDISP_PRO00
`define FDISP_PRO01
`define FDISP_PRO02
`define FDISP_PRO03
`define FDISP_PRO04
`define FDISP_PRO05
`define FDISP_PRO06
`define FDISP_PRO07
`define FDISP_PRO08
`define FDISP_PRO09
`define FDISP_PRO10
`define FDISP_PRO11
`define FDISP_PRO12
`define FDISP_PRO13
`define FDISP_PRO14
`define FDISP_PRO15
`define FDISP_PRO16
`define FDISP_PRO17
`define FDISP_PRO18
`define FDISP_PRO19
`define FDISP_PRO20
`define FDISP_PRO21
`define FDISP_PRO22
`define FDISP_PRO23
`define FDISP_PRO24
`define FDISP_PRO25
`define FDISP_PRO26
`define FDISP_PRO27
`define FDISP_PRO28
`define FDISP_PRO29
`define FDISP_PRO30
`define FDISP_PRO31
`define FDISP_PROINT
*/
//=================================================================
//include the macro define for the CPU real core source code
`include	"../../../rsrc/cpu/d_cache/defines.h"
`ifdef CPU_NET
	`include	"../../../rsrc/hblk/cpu/defines.h"
`endif


