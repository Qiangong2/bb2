//define cpu
`ifdef	INC_CPU
	`undef	NO_CPU
`else
	`define	NO_CPU
`endif
//define nb
`ifdef	INC_NB
	`undef	NO_NB
`else
	`define	NO_NB
`endif
//define sb
//define USB
`ifdef	INC_USB
	`undef	NO_USB
`else
	`define	NO_USB
`endif
//define CORE
`ifdef	INC_CORE
	`undef	NO_CORE
`else
	`define	NO_CORE
`endif
//for use the file io operation
`define		T2_FILE_IO_EN

//for new sdram control
`define  	NEW_MEM

//for set mem parameters
`define		NEW_MEM_SET

//for performance
`define     CLOSE_PERFORM

//not print clk_monitor.rpt
`define		NO_CLK_MONITOR
