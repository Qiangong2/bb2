
/*=============================================================================
// Description: Top module for m3357 simulation
//              include the chip of m3357, and all system signals define here.
//				one external device behavior module invoke here. It only used
//				to test the chip logic connection, not test the transaction
//				mechanism of the m3357.
// Author:      Yuky
// History:     2003-06-04	--copy from m 6304
//				2003-06-27	--modifid for m3357



//============================================================================*/

`timescale 1ns/10ps

`ifdef  POST_GSIM
// global path for RSIM and Pre-GSIM
`define path_chip       top.CHIP
`define path            `path_chip.CORE
`define path_chipset    `path.T2_CHIPSET

`else
// global path for RSIM and Pre-GSIM
`define path_chip       top.CHIP
`define path            `path_chip.CORE
`define path_chipset    `path.T2_CHIPSET
`endif

`define path_m          top.boot_rom
/*
`ifdef	NEW_MEM_8M
//`define sdram_d0_4m_4b  // 1st row of SDRAM is 4Mx4B
//`define sdram_d1_4m_4b  // 2nd row of SDRAM is 4Mx4B
`define	sdram_d0_8m_4b	// 1st row of SDRAM is 8Mx4B
`define	sdram_d1_8m_4b	// 2nd row of SDRAM is 8Mx4B
//`define	sdram_d0_16m_4b	// 1st row of SDRAM is 16Mx4B
//`define	sdram_d1_16m_4b	// 2nd row of SDRAM is 16Mx4B
//`define sdram_d1_2m_2b
//`define sdram_d0_2m_2b
`else
`endif*/
`ifdef	NEW_MEM
//`define sdram_d0_4m_4b  // 1st row of SDRAM is 4Mx4B
//`define sdram_d1_4m_4b  // 2nd row of SDRAM is 4Mx4B
//`define	sdram_d0_8m_4b	// 1st row of SDRAM is 8Mx4B
//`define	sdram_d1_8m_4b	// 2nd row of SDRAM is 8Mx4B
`define	sdram_d0_16m_4b	// 1st row of SDRAM is 16Mx4B
`define	sdram_d1_16m_4b	// 2nd row of SDRAM is 16Mx4B
//`define sdram_d1_2m_2b
//`define sdram_d0_2m_2b
`else
//`define sdram_d0_4m_4b  // 1st row of SDRAM is 4Mx4B
//`define sdram_d1_4m_4b  // 2nd row of SDRAM is 4Mx4B
//`define	sdram_d0_8m_4b	// 1st row of SDRAM is 8Mx4B
//`define	sdram_d1_8m_4b	// 2nd row of SDRAM is 8Mx4B
`define	sdram_d0_16m_4b	// 1st row of SDRAM is 16Mx4B
`define	sdram_d1_16m_4b	// 2nd row of SDRAM is 16Mx4B
//`define sdram_d1_2m_2b
//`define sdram_d0_2m_2b
`endif
module  top;

//====================== define the 32bit mode or 16bits =======================
parameter	SDRAM_DQ_Mode		=	1'b1;	//0->32bits mode, 1->16bits mode

//====================== generate the crystal clock ============================
reg             f27_clk;
reg     [31:0]  f27_clk_data;
reg             f48_clk;
reg             f24_clk;

reg             f40_clk;
reg             ejtag_clk;
`ifdef	WAFER_TEST //modified for wafer test,tod.2003-03-20
parameter       cyclef27_clk    = 100;
parameter       cyclef48_clk    = 25;
parameter       cyclef24_clk    = 25;
parameter       cycle_ejtag     = 50;//10MHz
parameter       cyclef40_clk    = 50;
`else
parameter       cyclef27_clk    = 18.51852;
parameter       cyclef48_clk    = 10.417;
parameter       cyclef24_clk    = 20.34505;
parameter       cycle_ejtag     = 50;//10MHz
parameter       cyclef40_clk    = 12.5;
`endif

initial begin
        f27_clk = 0;    //all clock change the first phase by yuky,2002-01-15
        f48_clk = 0;
        f24_clk = 0;
        f40_clk = 0;
        ejtag_clk = 0;
        end

always #cyclef27_clk f27_clk = ~f27_clk;
always #cyclef48_clk f48_clk = ~f48_clk;
always #cyclef24_clk f24_clk = ~f24_clk;
always #cyclef40_clk f40_clk = ~f40_clk;        //for test chipset 133MHz
always #cycle_ejtag  ejtag_clk = ~ejtag_clk;
//==============================================================================

//================== define the system signals =================================
`ifdef	POST_GSIM
//wire            CPU_mode        =       ~top.CHIP.PAD_RST_STRAP.CPU_MODE_;		// post netlist port inconsistent
wire      		CPU_mode		=	0; //added for debug JFR 030722
//added for m3357 JFR030614
wire            COMBO_mode        =     top.CHIP.PAD_RST_STRAP.COMBO_MODE;
wire            PCI_mode        =       top.CHIP.PAD_RST_STRAP.N180;			// post netlist port inconsistent
wire            IDE_mode    	=       top.CHIP.PAD_RST_STRAP.IDE_MODE;
wire			TVENC_TEST_mode	=		top.CHIP.PAD_RST_STRAP.TVENC_TEST_MODE;
wire			SERVO_ONLY_mode =		top.CHIP.PAD_RST_STRAP.SERVO_ONLY_MODE;
wire			SERVO_TEST_mode =		top.CHIP.PAD_RST_STRAP.SERVO_TEST_MODE;
wire			SERVO_SRAM_mode =		top.CHIP.PAD_RST_STRAP.SERVO_SRAM_MODE;
wire			BIST_TEST_mode	=		top.CHIP.PAD_MISC.BIST_TEST_MODE;
wire            Rom_en          =       ~top.CHIP.PAD_MISC.ROM_EN_FROMCORE_;
wire			Cpu_probe_en	=		top.CHIP.PAD_RST_STRAP.CPU_PROBE_EN;
wire			pci_mode_ide_en =		top.CHIP.PAD_MISC.PCI_MODE_IDE_EN_FROMCORE;
wire			video_in_en		=		 top.CHIP.PAD_MISC.VIDEO_IN_INTF_EN_FROMCORE
`else
//wire            CPU_mode        =       top.CHIP.cpu_mode;
wire      		CPU_mode		=	0; //added for debug JFR 030722
//added for m3357 JFR030614
wire            COMBO_mode        =     top.CHIP.PAD_RST_STRAP.combo_mode;
wire            PCI_mode        =       top.CHIP.PAD_RST_STRAP.pci_mode;
wire            IDE_mode    	=       top.CHIP.PAD_RST_STRAP.ide_mode;
wire			TVENC_TEST_mode	=		top.CHIP.PAD_RST_STRAP.tvenc_test_mode;
wire			SERVO_ONLY_mode =		top.CHIP.PAD_RST_STRAP.servo_only_mode;
wire			SERVO_TEST_mode =		top.CHIP.PAD_RST_STRAP.servo_test_mode;
wire			SERVO_SRAM_mode =		top.CHIP.PAD_RST_STRAP.servo_sram_mode;
wire			BIST_TEST_mode	=		top.CHIP.PAD_MISC.bist_test_mode;
//wire			CD_mode			=		top.CHIP.cd_mode;
//wire            UP_mode         =       top.CHIP.up_mode;
//wire			ANALOGT_mode	=		top.CHIP.analogt_mode;
wire            Rom_en          =       ~top.CHIP.PAD_MISC.rom_en_fromcore_;
wire			Cpu_probe_en	=		top.CHIP.PAD_RST_STRAP.cpu_probe_en;
wire            pci_gntb_en		=       top.CHIP.PAD_MISC.pci_gntb_en_fromcore;
wire			scb_en			= 		top.CHIP.PAD_MISC.scb_en_fromcore;
wire			i2si_data_en	=		top.CHIP.PAD_MISC.i2si_data_en_fromcore;
wire			SV_TST_PIN_EN	=		top.CHIP.PAD_MISC.sv_tst_pin_en_fromcore;
wire			pci_mode_ide_en =		top.CHIP.PAD_MISC.pci_mode_ide_en_fromcore;
wire			video_in_en		=		top.CHIP.PAD_MISC.video_in_intf_en_fromcore;
wire			vdata_en		=		top.CHIP.PAD_MISC.vdata_en_fromcore;

wire	ext_ide_device_en_src	= top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.ext_ide_device_en;
wire	scb_en_src				= top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.scb_en;
wire	sv_gpio_en_src			= top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.servo_gpio_en;
wire	hv_sync_en_src			= top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.hv_sync_en;
wire	i2si_data_en_src		= top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.i2si_data_en;
wire	vdata_en_src			= top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.vdata_en;
wire	pci_gntb_en_src			= top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.pci_gntb_en;
wire	de_h_sync_src_			= top.CHIP.CORE.T2_CHIPSET.h_sync_out_;
wire	de_v_sync_src_			= top.CHIP.CORE.T2_CHIPSET.v_sync_out_;
wire	tvenc_h_sync_src_		= top.CHIP.CORE.T2_CHIPSET.tvenc_h_sync_;
wire	tvenc_v_sync_src_		= top.CHIP.CORE.T2_CHIPSET.tvenc_v_sync_;
wire	sync_source_sel_src		= top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.sync_source_sel;

wire	ext_ide_device_en_ter	= top.CHIP.PAD_MISC.ext_ide_device_en_fromcore;
wire	scb_en_ter				= top.CHIP.PAD_MISC.scb_en_fromcore;
wire	sv_gpio_en_ter			= top.CHIP.PAD_MISC.sv_gpio_en_fromcore;
wire	hv_sync_en_ter			= top.CHIP.PAD_MISC.hv_sync_en_fromcore;
wire	i2si_data_en_ter		= top.CHIP.PAD_MISC.i2si_data_en_fromcore;
wire	vdata_en_ter			= top.CHIP.PAD_MISC.vdata_en_fromcore;
wire	pci_gntb_en_ter			= top.CHIP.PAD_MISC.pci_gntb_en_fromcore;
wire	sync_source_sel_ter		= top.CHIP.PAD_MISC.sync_source_sel_fromcore;
wire	h_sync_ter_				= top.CHIP.PAD_MISC.h_sync_fromcore_;
wire	v_sync_ter_				= top.CHIP.PAD_MISC.v_sync_fromcore_;
wire	h_sync_true				= h_sync_ter_ === (sync_source_sel_src ? de_h_sync_src_ : tvenc_h_sync_src_);
wire	v_sync_true				= v_sync_ter_ === (sync_source_sel_src ? de_v_sync_src_ : tvenc_v_sync_src_);

wire	svga_mode_en_src		= top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.svga_mode_en;
wire	svga_mode_en_ter		= top.CHIP.VIDEO_CLOCK_GEN.SVGA_MODE_EN;

wire	[3:0]	tv_enc_clk_sel_src	= top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.tv_enc_clk_sel;
wire	[3:0]	tv_enc_clk_sel_ter	= top.CHIP.VIDEO_CLOCK_GEN.TV_ENC_CLK_SEL;

wire	video_in_intf_en_src	= top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.video_in_intf_en;
wire	video_in_intf_en_ter	= top.CHIP.PAD_MISC.video_in_intf_en_fromcore;

wire	pci_mode_ide_en_src		= top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.pci_mode_ide_en;
wire	pci_mode_ide_en_ter		= top.CHIP.PAD_MISC.pci_mode_ide_en_fromcore;


// pragma property  Correct_Connection =
//	always ((ext_ide_device_en_src 	== ext_ide_device_en_ter) &
//			(scb_en_src 			== scb_en_ter) &
//			(sv_gpio_en_src 		== sv_gpio_en_ter) &
//			(hv_sync_en_src 		== hv_sync_en_ter) &
//			(i2si_data_en_src 		== i2si_data_en_ter) &
//			(vdata_en_src 			== vdata_en_ter) &
//			(pci_gntb_en_src 		== pci_gntb_en_ter) &
//			(sync_source_sel_src	== sync_source_sel_ter) &
//			(svga_mode_en_src		== svga_mode_en_ter) &
//			(tv_enc_clk_sel_src		== tv_enc_clk_sel_src) &
//			(video_in_intf_en_src	== video_in_intf_en_ter) &
//			(pci_mode_ide_en_src	== pci_mode_ide_en_ter)
//			) @ (posedge sd_clk);
// assert Correct_Connection;

//	pragma property	Correct_Function =
//	always (h_sync_true & v_sync_true)
//		@ (posedge sd_clk);
//	assert Correct_Function;



`endif

reg             cold_rst_;
tri1            warm_rst_;
tri1            ext_rst_;
tri1			rom_control_rst_;
assign          warm_rst_ = cold_rst_;
assign          ext_rst_  = warm_rst_;
assign			rom_control_rst_ = warm_rst_ & ~Rom_en;
wire			cold_rst_wire_	=	cold_rst_;
//==============================================================================

wire			f24_clk_wire;
wire			sv_f24_clk_wire;
wire			f24_clk_wire_tmp;
`ifdef  POST_GSIM
	wire i2so_mclk_sel_ = top.CHIP.IOPAD.I2SO_MCLK_OE_FROMPADMISC_;//0: output; 1: input
`else
	wire i2so_mclk_sel_ = top.CHIP.IOPAD.i2so_mclk_oe_frompadmisc_;
`endif
//		Carcy modify for external up test
assign		f24_clk_wire = (SERVO_SRAM_mode | SERVO_TEST_mode)? sv_f24_clk_wire : f24_clk_wire_tmp ;
assign	 	f24_clk_wire_tmp = i2so_mclk_sel_ ? f24_clk	: 1'bz;

/*
`ifdef	SERVO_TEST_DEBUG
`else
assign		f24_clk_wire = i2so_mclk_sel_ ? f24_clk : 1'bz;
`endif
*/
//================= define the signals for tasks ===============================
reg     [31:0]  temp_mem;       // used for Memory Task
reg 	[31:0]  temp2;      	// used for
reg     [31:0]  temp;           // used for Control Task
reg     [31:0]  cpu_temp,
                pci_temp;
reg     [31:0]  irq_temp;       // used for IRQ Task

parameter       mem_max_size = 32'h0080_0000;
reg     [31:0]  mem_array [0:(mem_max_size>>2) - 1];// : 0];
reg     [7:0]   byte_array [0:mem_max_size - 1];// : 0];
reg     [13:0]  sdram_ram0_haddr, sdram_ram1_haddr;
reg     [13:0]  sdram_ram0_haddr_next, sdram_ram1_haddr_next;

integer         curn_offset;
reg     [2:0]   sdram_row0_map,
                sdram_row1_map;
reg				sequence_mode	;
integer         sdram_row1_base,SDRAM_DIMM1_BASE;
reg     [31:0]  temp_xxx;
initial begin
        sdram_row0_map = 0;
        sdram_row1_map = 0;
        for(temp_xxx = 32'h0; temp_xxx < mem_max_size ; temp_xxx = temp_xxx + 1)
                byte_array[temp_xxx] = 0;
        for(temp_xxx = 32'h0; temp_xxx < (mem_max_size>>2) ; temp_xxx = temp_xxx + 1)
                mem_array[temp_xxx] = 0;
end
integer curn_addr, sdram_real_addr;
integer	curn_addr_next, sdram_real_addr_next;
integer f_name,i,j;


integer	i_cpu,j_cpu,k_cpu;		//for cpu pattern
integer	i_disp,j_disp,k_disp;	//for display pattern
integer	i_pci,j_pci,k_pci;		//for pci pattern
integer	i_gpu,j_gpu,k_gpu;		//for gpu pattern
integer	i_dsp,j_dsp,k_dsp;		//for dsp pattern
integer	i_video,j_video,k_video;//for video pattern
integer i_vin,j_vin,k_vin;		//for vin pattern
integer	i_vsb,j_vsb,k_vsb;		//for vsb pattern
integer	i_sb,j_sb,k_sb;			//for south bridge pattern
integer	i_ide,j_ide,k_ide;		//for ide pattern
integer	disp_bh_file;			//for display engine behaviour model dump file

//`ifdef OPEN_DISP_BH_DUMP_RPT
//	initial	begin
//		disp_bh_file = $fopen("disp_bh_dump.rpt");
//	end
//`else
//`endif	// Snow Yi, 2003.08.28

reg     [31:0]  data_dw;
reg     [15:0]  data_w;
reg     [7:0]   data_b;
reg             flag;
reg				SV_RST_ ;

reg             init_end,
                cpu_vec_end,
                pci_vec_end,
                cpu_vec_00_end,
                cpu_vec_01_end,
                cpu_vec_02_end,
                cpu_vec_03_end,
                cpu_vec_04_end,
                cpu_vec_05_end,
                cpu_vec_06_end,
                cpu_vec_07_end,
                cpu_vec_08_end,
                cpu_vec_09_end,
                cpu_vec_10_end,
                cpu_vec_11_end,
                cpu_vec_12_end,
                cpu_vec_13_end,
                cpu_vec_14_end,
                cpu_vec_15_end,
                cpu_vec_16_end,
                cpu_vec_17_end,
                cpu_vec_18_end,
                cpu_vec_19_end,
                cpu_vec_20_end,
                int_end;

//==============================================================================

//========================= define the register for patterns use ===============
reg     [31:0]  temp_data0,temp_data1,temp_data2,temp_data3;
reg     [31:0]  temp_data4,temp_data5,temp_data6,temp_data7;
reg     [31:0]  temp_data8,temp_data9,temp_data10,temp_data11;
//==============================================================================


//==================== define the sdram signals ================================
wire    [31:0]  ram_dq, ext_ram_dq;
wire            sd_clk;
tri1    	   ram_cs0_;
tri1            ram_ras_, ram_cas_, ram_we_;
wire    [11:0]  ram_addr;
wire    [1:0]   ram_ba;
wire    [3:0]   ram_dqm;

//==================== define PCI signals ======================================
//pci_xx for m3357 JFR030701
//p_xx for m6304
wire			pci_clk   ;
tri1			pci_gnta_ ;
tri1			pci_gntb_ ;
tri1			pci_inta_ ;
tri1			pci_req_  ;
tri1 	[31:0]	pci_ad    ;
tri1    [3:0]   pci_cbe_  ;
tri1            pci_devsel_;
tri1            pci_frame_;
tri1            pci_irdy_;
tri1            pci_par;
tri1            pci_perr_;
tri1            pci_serr_;
tri1            pci_stop_;
tri1            pci_trdy_;
tri1            pci_lock_;

/*
tri1    [31:0]  p_ad;
tri1    [3:0]   p_cbe_;
tri1            p_devsel_;
tri1            p_frame_;
tri1            p_irdy_;
tri1            p_par;
*/
tri1            p_perr_;
tri1            p_serr_;
//tri1            p_stop_;
//tri1            p_trdy_;
tri1            p_lock_;
//tri1  [3:0]   p_req_,p_gnt_;
//tri1            p_req_,p_gnt_;
tri1    [3:0]   ext_int;
//wire            p_clk;
wire            monitor_clk     = pci_clk & PCI_mode;
wire            moniter_frame_  = pci_frame_;
wire            p_master_clk    = pci_clk & PCI_mode;
wire            p_target_clk    = pci_clk & PCI_mode;
wire            xout_48;
wire            xout_27;
wire            xout_40;
tri1            p_mas1_req_,    p_mas2_req_;
//assign                p_req_[0] = p_mas1_req_;        // PCI 33 request from p_master
//assign                p_req_[1] = p_mas2_req_;
//wire          p_mas1_gnt_ = p_gnt_[0];
//wire          p_mas2_gnt_ = p_gnt_[1];
//==============================================================================

//==================== define the CPU MIPS bus signals =========================
tri1    [31:0]  sysad;
tri1    [4:0]   syscmd;
tri1            pvalid_,
                pmaster_,
                preq_,
                eok_,
                ereq_,
                evalid_;
tri1    [4:0]   sysint_;
tri1            sysnmi_;
//==============================================================================

//===================== define CPU BIST signals ================================
wire			cpu_bist_mode;
wire			cpu_bist_finish;
wire	[5:0]	cpu_bist_err_vec;

wire			bist_test_mode_sel;
//==============================================================================
//===================== define VIDEO BIST signals ================================
wire			video_bist_mode;
wire			video_bist_finish;
wire	[10:0]	video_bist_err_vec;


//===================== define IDE signals =====================================
wire    [15:0]  atadd;
wire    [2:0]   atada;
tri1			atadiow_  ;
tri1			atadior_  ;
tri1			ataiordy  ;
tri1			ataintrq  ;
tri1			atacs0_   ;
tri1			atacs1_   ;
//wire			atareset_ ;
tri1			atadmarq  ;
tri1			atadmack_ ;

//===================== define GPIO signals=====================================
//tri1    [31:0]  gpio;
wire [31:0] ext_gpio, gpio;
//set gpio[15:0] to 0xa55a;
//-----a
pullup		(ext_gpio[15]),(ext_gpio[13]);
pulldown	(ext_gpio[14]),(ext_gpio[12]);
//-----5
pullup		(ext_gpio[10]),(ext_gpio[8]);
pulldown	(ext_gpio[11]),(ext_gpio[9]);
//-----5
pullup		(ext_gpio[6]),(ext_gpio[4]);
pulldown	(ext_gpio[7]),(ext_gpio[5]);
//-----a
pullup		(ext_gpio[3]),(ext_gpio[1]);
pulldown	(ext_gpio[2]),(ext_gpio[0]);

//set gpio[31:16] to 0x873c;
//-----a
pullup		(ext_gpio[31]);
pulldown	(ext_gpio[30]),(ext_gpio[29]),(ext_gpio[28]);
//-----7
pulldown	(ext_gpio[27]);
pullup		(ext_gpio[26]),(ext_gpio[25]),(ext_gpio[24]);
//-----3
pulldown	(ext_gpio[23]),(ext_gpio[22]);
pullup		(ext_gpio[21]),(ext_gpio[20]);
//-----c
pullup		(ext_gpio[19]),(ext_gpio[18]);
pulldown	(ext_gpio[17]),(ext_gpio[16]);
//===================== define south bridge signals ============================
// consumer Infra-red
tri1            irrx;

// I2C bus
tri1            scbsda;
tri1            scbscl;

// CD-DSP data and SubQ interface
//wire          sqck, scor, sqso;
//wire    subq_sqck, subq_scor, subq_sqso;
//wire            cd_bck, cd_lrck, cd_data;
//wire            cd_c2po;        // -------- runner


//==================================================================
//define for test_dly_chain verification
integer	test_dly_chain_flag	;
//========== define the flash rom signals ==========================
wire    [7:0]   eeprom_data;
wire    [20:0]  eeprom_addr;
wire			eeprom_ce_;
wire			eeprom_we_;
wire			eeprom_oe_;
//wire	[7:0]	p_rom_data;
//wire	[20:0]	p_rom_addr;
tri1			ext_eeprom_ce_;
tri1			ext_eeprom_we_;
tri1			ext_eeprom_oe_;
//tri1   [7:0]    ext_eeprom_data;
//tri1   [20:0]	ext_eeprom_addr;
wire	   [7:0]    ext_eeprom_data;
wire	   [20:0]	ext_eeprom_addr;


//================ define the USB signals ======================================
// Both port1 and port2 are Full_speed
pulldown        (weak0) (usb1_dn);
pulldown        (weak0) (usb1_dp);

pulldown        (weak0) (usb2_dn);
pulldown        (weak0) (usb2_dp);

`ifdef INC_CORE
//wire            usb_ovc = 1'b1; // over current
reg usb_ovc;
initial         usb_ovc = 1'b1;
wire            usb_pon;        // power enable
`endif
//==============================================================================
/*
//=================== define Audio signals =====================================
wire            i2si_data       ;
wire            i2si_bck        ;
wire            i2si_lrck       ;
wire            ac97bclk        ;
wire            ac97din0        ;
//==============================================================================
*/
//SERVO
wire	[1:0]	xsflag;

//=================== define the ejtag signals =================================
tri0            p_j_tdo         ;
//tri0            p_j_tdi         ;
//tri0            p_j_tms         ;
//tri0            p_j_tclk        ;
wire            p_j_tms         ;//Carcy 031120
wire            p_j_tclk        ;//Carcy 031120

tri1            p_j_rst_        ;

wire			ext_p_j_tdo		;
wire			ext_p_j_tdi 	;
wire			ext_p_j_tms 	;
wire			ext_p_j_tclk	;
wire			ext_p_j_rst_	;

tri0			spdif;
/*
reg             j_rst_;
initial begin
                j_rst_  =       1;
        #100    j_rst_  =       0;
        #100    j_rst_  =       1;
        end
assign          p_j_rst_        =       j_rst_;
*/
//==============================================================================

//=================== define the TV encoder signals ============================
tri1            pix_clk;
tri1            hsync_;
tri1            vsync_;
wire    [7:0]   vdata;          // Lanny 2001-07-12

tri0	vssa;
tri1	vdda;
//==============================================================================


//========================= define the strap pins ==============================
tri0	tmp_line;
//-----Note: the strap pins use the SDRAM address,bankaddress and dqm bus
//-----invoke the strap_pin_bh module
//modified for m3357 Jeffrey 030603
// PLL_M
strap_pin_bh    cpu_clk_pll_m0          (ram_addr[0]);
strap_pin_bh    cpu_clk_pll_m1          (ram_addr[1]);
strap_pin_bh    cpu_clk_pll_m2          (ram_addr[2]);
strap_pin_bh    cpu_clk_pll_m3          (ram_addr[3]);
strap_pin_bh    cpu_clk_pll_m4          (ram_addr[4]);
strap_pin_bh    cpu_clk_pll_m5          (ram_addr[5]);
//strap_pin_bh    pll_m6          (ram_addr[6]);
//strap_pin_bh    pll_m7          (ram_addr[7]);
// MEM_FS MEM_CLK_PLL_M
strap_pin_bh    mem_clk_pll_m0         (ram_addr[6]);
strap_pin_bh    mem_clk_pll_m1         (ram_addr[7]);
strap_pin_bh    mem_clk_pll_m2         (ram_addr[8]);
strap_pin_bh    mem_clk_pll_m3         (ram_addr[9]);
strap_pin_bh    mem_clk_pll_m4         (ram_addr[10]);
strap_pin_bh    mem_clk_pll_m5         (ram_addr[11]);
// PCI_T2RISC_FS
//strap_pin_bh    pci_t2risc_fs0  (ram_addr[10]);
//strap_pin_bh    pci_t2risc_fs1  (ram_addr[11]);	//metal fix, only use one bit for pci frequency select
//strap_pin_bh    pci_t2risc_fs1  (tmp_line);			//just for environment compatible after metal fix, it is no real use.
// WORK_MODE
strap_pin_bh    work_mode0      (ram_ba[0]);
//strap_pin_bh    WORK_MODE1      (RAM_BA[1]);		//metal fix, for mode select can be programable
strap_pin_bh    work_mode1      (ram_ba[1]);	//metal fix, for mode select can be programable
strap_pin_bh    work_mode2      (ram_dqm[0]);
// PLL_BYPASS
strap_pin_bh    pll_bypass      (ram_dqm[1]);
// CPU_PROBE_EN
strap_pin_bh    cpu_probe_en 	(ram_dq[0]);
//RESERVED STRAPE PINS
//strap_pin_bh	reserved_strp0	(ram_dq[1]);
// PCI_T2RISC_FS
strap_pin_bh    pci_t2risc_fs0  (ram_dq[1]);
strap_pin_bh    pci_t2risc_fs1  (ram_dq[2]);
// REFERENCE_INFO
strap_pin_bh    reference_info0 (ram_dq[3]);
strap_pin_bh    reference_info1 (ram_dq[4]);
strap_pin_bh    reference_info2 (ram_dq[5]);
strap_pin_bh    reference_info3 (ram_dq[6]);
strap_pin_bh    reference_info4 (ram_dq[7]);
strap_pin_bh    reference_info5 (ram_dq[8]);
strap_pin_bh    reference_info6 (ram_dq[9]);
strap_pin_bh    reference_info7 (ram_dq[10]);
strap_pin_bh    reference_info8 (ram_dq[11]);
strap_pin_bh    reference_info9 (ram_dq[12]);
strap_pin_bh    reference_info10 (ram_dq[13]);
strap_pin_bh    reference_info11 (ram_dq[14]);
strap_pin_bh    reference_info12 (ram_dq[15]);
//==============================================================================
tri0    test_mode;
//tri1    test_mode;//for bist debug JFR030930

`ifdef	POST_GSIM
T3357    CHIP    (
// SDRAM & FLASH(56)
        .P_D_CLK                (sd_clk         ),
        .P_D_ADDR               (ram_addr       ),// SDRAM Row/Column Address
        .P_D_BADDR              (ram_ba         ),// SDRAM bank address
        .P_D_DQ                 (ram_dq         ),// SDRAM 16 bits output data pins
        .P_D_DQM                (ram_dqm        ),// SDRAM data mask signal
        .P_D_CS_                (ram_cs0_        ),// SDRAM chip select
        .P_D_CAS_               (ram_cas_       ),// SDRAM cas#
        .P_D_WE_                (ram_we_        ),// SDRAM we#
        .P_D_RAS_               (ram_ras_       ),// SDRAM ras#

        // FLASH ROM(3)
        .P_ROM_CE_              (eeprom_ce_     ),// 1,   Flash rom chip select
        .P_ROM_WE_              (eeprom_we_     ),// 1,   Flash rom write enable
        .P_ROM_OE_              (eeprom_oe_     ),// 1,   Flash rom output enable
//added for m3357 JFR030613
		.P_ROM_DATA				(eeprom_data	),// 8 	Flash rom data
        .P_ROM_ADDR				(eeprom_addr	),// 21	Flash rom address


// I2S Audio Interface (8)
        .I2SO_DATA0             (i2so_data0     ),// 1,   Serial data for I2S output
        .I2SO_DATA1				(i2so_data1		),// 1,   Serial data for I2S output 1
        .I2SO_DATA2				(i2so_data2		),// 1,   Serial data for I2S output 2
        .I2SO_DATA3				(i2so_data3		),// 1,   Serial data for I2S output 3
        .I2SO_BCK               (i2so_bck       ),// 1,   Bit clock for I2S output
        .I2SO_LRCK              (i2so_lrck      ),// 1,   Channel clock for I2S output
        .I2SO_MCLK              (f24_clk_wire   ),// 1,   over sample clock for i2s DAC
//        .I2SI_DATA              (i2si_data      ),// 1,   serial data for I2S input
//      .i2si_bck               (i2si_bck       ),// 1,   bit clock for I2S input
//      .i2si_lrck              (i2si_lrck      ),// 1,   channel clock for I2S input
//TV encoder Interface (10)
		.GNDA_TVDAC				(gnda_tvdac		),	// 1,	Analog GND, input
		.VD33A_TVDAC			(vd33a_tvdac	),	// 3,	Analog Power, input
		.IOUT1					(iout1			),	// 1,   Analog video output 1
		.IOUT2					(iout2			),	// 1,   Analog video output 2
		.IOUT3					(iout3			),	// 1,   Analog video output 3
		.IOUT4					(iout4			),	// 1,   Analog video output 4
		.IOUT5					(iout5			),	// 1,   Analog video output 5
		.IOUT6					(iout6			),	// 1,   Analog video output 6
//aeede for m3357 JFR 030613
		.XIREF1					(xiref1			),	// 1,	connect external via capacitance toward VDDA
		.XIEXT					(xiext			),	// 1,	470 Ohms resistor connect pin, I/O
		.XIDUMP					(xidump			),	// 1,	For performance and chip temperature, output
//		vbg,			// 1,	reference power, will not be bonded to Pin.
// SPDIF (1)
        .SPDIF                  (spdif			),// 1, output only

        // PCI (13)
//		.PCI_AD					(pci_ad[31:29]	),// 3, pci address/data [29:31]
//		.PCI_CBE_				(pci_cbe_		),// 4, pci control/byte enable [0:3]
//		.PCI_FRAME_				(pci_frame_		),// 1,
//		.PCI_IRDY_				(pci_irdy_		),// 1,
//		.PCI_TRDY_				(pci_trdy_		),// 1,
//		.PCI_STOP_				(pci_stop_		),// 1,
//		.PCI_DEVSEL_			(pci_devsel_ 	),// 1,
//		.PCI_PAR				(pci_par	    ),// 1,

//UART port (2)JFR030730
		.uart_tx				(uart_tx		),// 1
//		.uart_rx                (uart_rx		),// 1

// USB port (6)
//      .usb4dp                 (),// 1,   D+ for USB port4
//      .usb4dn                 (),// 1,   D- for USB port4
//      .usb3dp                 (),// 1,   D+ for USB port3
//      .usb3dn                 (),// 1,   D- for USB port3
        .USB2DP                 (usb2_dp        ),// 1,   D+ for USB port2
        .USB2DN                 (usb2_dn        ),// 1,   D- for USB port2
        .USB1DP                 (usb1_dp        ),// 1,   D+ for USB port1
        .USB1DN                 (usb1_dn        ),// 1,   D- for USB port1
        .USBPON                 (usb_pon        ),// 1,   USB port power control
        .USBOVC                 (usb_ovc        ),// 1,   USB port over current
//SERVO port (70) JFR030611
 		.XADCIN		   		 (XADCIN		),
 		.XADCIP		   		 (XADCIP		),
 		.XATTON		   		 (XATTON		),
 		.XATTOP		   		 (XATTOP		),
 		.XBIASR		   		 (XBIASR		),
 		.XCDLD		   		 (XCDLD			),
 		.XCDPD		   		 (XCDPD			),
 		.XCDRF		   		 (XCDRF			),
 		.XCD_A		   		 (XCD_A			),
 		.XCD_B		   		 (XCD_B			),
 		.XCD_C		   		 (XCD_C			),
 		.XCD_D		   		 (XCD_D			),
 		.XCD_E		   		 (XCD_E			),
 		.XCD_F		   		 (XCD_F			),
 		.XCELPFO	   		 (XCELPFO		),
 		.XDPD_A		   		 (XDPD_A		),
 		.XDPD_B		   		 (XDPD_B		),
 		.XDPD_C		   		 (XDPD_C		),
 		.XDPD_D		   		 (XDPD_D		),
 		.XDVDLD		   		 (XDVDLD		),
 		.XDVDPD		   		 (XDVDPD		),
 		.XDVDRFN	   		 (XDVDRFN		),
 		.XDVDRFP	   		 (XDVDRFP		),
 		.XDVD_A		   		 (XDVD_A		),
 		.XDVD_B		   		 (XDVD_B		),
 		.XDVD_C		   		 (XDVD_C		),
 		.XDVD_D		   		 (XDVD_D		),
 		.XFELPFO	   		 (XFELPFO		),
 		.XFOCUS		   		 (XFOCUS		),
 		.XGMBIASR	   		 (XGMBIASR		),
 		.XLPFON		   		 (XLPFON		),
 		.XLPFOP		   		 (XLPFOP		),
 		.XPDAUX1	   		 (XPDAUX1		),
 		.XPDAUX2	   		 (XPDAUX2		),
 		.XSBLPFO	   		 (XSBLPFO		),
 		.XSFGN		   		 (XSFGN			),
 		.XSFGP		   		 (XSFGP			),
 		.XSLEGN		   		 (XSLEGN		),
 		.XSLEGP		   		 (XSLEGP		),
 		.XSPINDLE	   		 (XSPINDLE		),
 		.XTELP		   		 (XTELP			),
 		.XTELPFO	   		 (XTELPFO		),
 		.XTESTDA	   		 (XTESTDA		),
 		.XTEXO		   		 (XTEXO			),
 		.XTRACK		   		 (XTRACK		),
 		.XTRAY		   		 (XTRAY			),
 		.XVGAIN		   		 (XVGAIN		),
 		.XVGAIP		   		 (XVGAIP		),
 		.XVREF15	   		 (XVREF15		),
 		.XVREF21	   		 (XVREF21		),

 		.VD33D		  		 (VD33D			),
 		.VD18D		  		 (VD18D			),
 		.VDD3MIX1	  		 (VDD3MIX1		),
 		.VDD3MIX2	  		 (VDD3MIX2		),
 		.AVDD_AD	  		 (AVDD_AD		),
 		.AVDD_ATT	  		 (AVDD_ATT		),
 		.AVDD_DA	  		 (AVDD_DA		),
 		.AVDD_DPD	  		 (AVDD_DPD		),
 		.AVDD_LPF	  		 (AVDD_LPF		),
 		.AVDD_RAD	  		 (AVDD_RAD		),
 		.AVDD_REF	  		 (AVDD_REF		),
 		.AVDD_SVO	  		 (AVDD_SVO		),
 		.AVDD_VGA	  		 (AVDD_VGA		),
 		.AVSS_AD	  		 (AVSS_AD		),
 		.AVSS_ATT	  		 (AVSS_ATT		),
 		.AVSS_DA	  		 (AVSS_DA		),
 		.AVSS_DPD	  		 (AVSS_DPD		),
 		.AVSS_GA	  		 (AVSS_GA		),
 		.AVSS_GD	  		 (AVSS_GD		),
 		.AVSS_LPF	  		 (AVSS_LPF		),
 		.AVSS_RAD	  		 (AVSS_RAD		),
 		.AVSS_REF	  		 (AVSS_REF		),
 		.AVSS_SVO	  		 (AVSS_SVO		),
 		.AVSS_VGA	  		 (AVSS_VGA		),
 		.AZEC_GND	  		 (AZEC_GND		),
 		.AZEC_VDD	  		 (AZEC_VDD		),
 		.DV18_RCKA	  		 (DV18_RCKA		),
 		.DV18_RCKD	  		 (DV18_RCKD		),
 		.DVDD	      		 (DVDD	    	),
 		.DVSS	      		 (DVSS	    	),
 		.DVSS_RCKA	  		 (DVSS_RCKA		),
 		.DVSS_RCKD	  		 (DVSS_RCKD		),
 		.FAD_GND	  		 (FAD_GND		),
 		.FAD_VDD	  		 (FAD_VDD		),
 		.GNDD		  		 (GNDD			),


// Consumer IR Interface (1)
        .IRRX                   (irrx           ),// 1,   consumer infra-red remote controller/wireless keyboard

// PS Device Interface (3)
        .PSCS0_                 (pscs0_         ),// 1,   PS device chip select for port0
        .PSCS1_                 (pscs1_         ),// 1,   PS device chip select for port1
        .PSACK_                 (psack_         ),// 1,   PS device chip acknowledge

// General Purpose IO (7,GPIO)
        .GPIO                   (gpio[28:0]		), // 15,  General purpose io
// EJTAG Interface (5)
        .P_J_TDO                (p_j_tdo        ), // 1,   serial test data output
        .P_J_TDI                (p_j_tdi        ), // 1,   serial test data input
        .P_J_TMS                (p_j_tms        ), // 1,   test mode select
        .P_J_TCLK               (p_j_tclk       ), // 1,   test clock
        .P_J_RST_               (p_j_rst_       ), // 1,   test reset
// power and ground
		.VDDCORE				(1'b1),//		core vdd
//		.VSSCORE				(1'b0),//		core vss
		.VDDIO					(1'b1),//		io vdd
		.GND					(1'b0),//		io vss
		.cpu_pllvdd				(1'b1),//		pll vdd for cpu pll
		.cpu_pllvss				(1'b0),//		pll vss for cpu pll
		.F27_PLLVDD				(1'b1),//		pll vdd for f27 pll
		.F27_PLLVSS				(1'b0),//		pll vss for f27 pll
// system (8)
        .P_CRST_                (cold_rst_      ), // 1,   cold reset
        .TEST_MODE              (test_mode      ),
//        .test_sel				(test_sel		),
     //   .p_x48in                (f48_clk        ), // 1,   crystal input (48 MHz for USB)
     //   .p_x48out               (xout_48        )  // 1,   crystal output
        .P_X27IN				(f27_clk	   	),// 1,   crystal input (27 MHz for video output)
        .P_X27OUT        		(xout_27   		), // 1,   crystal output
//ADC (5)
		.VD33A_ADC				(vd33a_adc	 	),// 1,	3.3V Analog VDD for ADC.
		.XMIC1IN     			(xmic1in     	),// 1,   ADC microphone input 1.
		.XADC_VREF   			(xadc_vref   	),// 1,   ADC reference voltage (refered to the reference circuit).
		.XMIC2IN     			(xmic2in     	),// 1,   ADC microphone input 2.
		.GNDA_ADC    			(gnda_adc    	) // 1,   Analog GND for ADC.

        );
`else

chip    CHIP    (
// SDRAM & FLASH(56)
        .p_d_clk                 (sd_clk         ),
        .p_d_addr               (ram_addr       ),// SDRAM Row/Column Address
        .p_d_baddr              (ram_ba         ),// SDRAM bank address
        .p_d_dq                 (ram_dq         ),// SDRAM 32 bits output data pins
        .p_d_dqm                (ram_dqm        ),// SDRAM data mask signal
        .p_d_cs_                (ram_cs0_        ),// SDRAM chip select
        .p_d_cas_               (ram_cas_       ),// SDRAM cas#
        .p_d_we_                (ram_we_        ),// SDRAM we#
        .p_d_ras_               (ram_ras_       ),// SDRAM ras#

        // FLASH ROM(3)
        .p_rom_ce_              (eeprom_ce_     ),// 1,   Flash rom chip select
        .p_rom_we_              (eeprom_we_     ),// 1,   Flash rom write enable
        .p_rom_oe_              (eeprom_oe_     ),// 1,   Flash rom output enable
//added for m3357 JFR030613
		.p_rom_data				(eeprom_data	),// 8 	Flash rom data
        .p_rom_addr				(eeprom_addr	),// 21	Flash rom address

// I2S Audio Interface (8)
        .i2so_data0             (i2so_data0     ),// 1,   Serial data for I2S output
        .i2so_data1				(i2so_data1		),// 1,   Serial data for I2S output 1
        .i2so_data2				(i2so_data2		),// 1,   Serial data for I2S output 2
        .i2so_data3				(i2so_data3		),// 1,   Serial data for I2S output 3
        .i2so_bck               (i2so_bck       ),// 1,   Bit clock for I2S output
        .i2so_lrck              (i2so_lrck      ),// 1,   Channel clock for I2S output
        .i2so_mclk              (f24_clk_wire	),// 1,   over sample clock for i2s DAC
//        .i2si_data              (i2si_data      ),// 1,   serial data for I2S input
//      .i2si_bck               (i2si_bck       ),// 1,   bit clock for I2S input
//      .i2si_lrck              (i2si_lrck      ),// 1,   channel clock for I2S input
//TV encoder Interface (10)
		.gnda_tvdac				(gnda_tvdac		),	// 1,	Analog GND, input
		.vd33a_tvdac			(vd33a_tvdac	),	// 3,	Analog Power, input
		.iout1					(iout1			),	// 1,   Analog video output 1
		.iout2					(iout2			),	// 1,   Analog video output 2
		.iout3					(iout3			),	// 1,   Analog video output 3
		.iout4					(iout4			),	// 1,   Analog video output 4
		.iout5					(iout5			),	// 1,   Analog video output 5
		.iout6					(iout6			),	// 1,   Analog video output 6
//aeede for m3357 JFR 030613
		.xiref1					(xiref1			),	// 1,	connect external via capacitance toward VDDA
		.xiext					(xiext			),	// 1,	470 Ohms resistor connect pin, I/O
		.xidump					(xidump			),	// 1,	For performance and chip temperature, output
//		vbg,			// 1,	reference power, will not be bonded to Pin.
// SPDIF (1)
        .spdif                  (spdif			),// 1, output only

         // PCI (13)
//		.pci_ad					(pci_ad[31:29]	),// 3, pci address/data [29:31]
//		.pci_cbe_				(pci_cbe_		),// 4, pci control/byte enable [0:3]
//		.pci_frame_				(pci_frame_		),// 1,
//		.pci_irdy_				(pci_irdy_		),// 1,
//		.pci_trdy_				(pci_trdy_		),// 1,
//		.pci_stop_				(pci_stop_		),// 1,
//		.pci_devsel_			(pci_devsel_ 	),// 1,
//		.pci_par				(pci_par	    ),// 1,

//UART port (2)JFR030730
		.uart_tx				(uart_tx		),// 1
//		.uart_rx                (uart_rx		),// 1

// USB port (6)
//      .usb4dp                 (),// 1,   D+ for USB port4
//      .usb4dn                 (),// 1,   D- for USB port4
//      .usb3dp                 (),// 1,   D+ for USB port3
//      .usb3dn                 (),// 1,   D- for USB port3
        .usb2dp                 (usb2_dp        ),// 1,   D+ for USB port2
        .usb2dn                 (usb2_dn        ),// 1,   D- for USB port2
        .usb1dp                 (usb1_dp        ),// 1,   D+ for USB port1
        .usb1dn                 (usb1_dn        ),// 1,   D- for USB port1
        .usbpon                 (usb_pon        ),// 1,   USB port power control
        .usbovc                 (usb_ovc        ),// 1,   USB port over current
//SERVO port (70) JFR030611
 		.XADCIN		   		 (XADCIN		),
 		.XADCIP		   		 (XADCIP		),
 		.XATTON		   		 (XATTON		),
 		.XATTOP		   		 (XATTOP		),
 		.XBIASR		   		 (XBIASR		),
 		.XCDLD		   		 (XCDLD			),
 		.XCDPD		   		 (XCDPD			),
 		.XCDRF		   		 (XCDRF			),
 		.XCD_A		   		 (XCD_A			),
 		.XCD_B		   		 (XCD_B			),
 		.XCD_C		   		 (XCD_C			),
 		.XCD_D		   		 (XCD_D			),
 		.XCD_E		   		 (XCD_E			),
 		.XCD_F		   		 (XCD_F			),
 		.XCELPFO	   		 (XCELPFO		),
 		.XDPD_A		   		 (XDPD_A		),
 		.XDPD_B		   		 (XDPD_B		),
 		.XDPD_C		   		 (XDPD_C		),
 		.XDPD_D		   		 (XDPD_D		),
 		.XDVDLD		   		 (XDVDLD		),
 		.XDVDPD		   		 (XDVDPD		),
 		.XDVDRFN	   		 (XDVDRFN		),
 		.XDVDRFP	   		 (XDVDRFP		),
 		.XDVD_A		   		 (XDVD_A		),
 		.XDVD_B		   		 (XDVD_B		),
 		.XDVD_C		   		 (XDVD_C		),
 		.XDVD_D		   		 (XDVD_D		),
 		.XFELPFO	   		 (XFELPFO		),
 		.XFOCUS		   		 (XFOCUS		),
 		.XGMBIASR	   		 (XGMBIASR		),
 		.XLPFON		   		 (XLPFON		),
 		.XLPFOP		   		 (XLPFOP		),
 		.XPDAUX1	   		 (XPDAUX1		),
 		.XPDAUX2	   		 (XPDAUX2		),
 		.XSBLPFO	   		 (XSBLPFO		),
 		.XSFGN		   		 (XSFGN			),
 		.XSFGP		   		 (XSFGP			),
 		.XSLEGN		   		 (XSLEGN		),
 		.XSLEGP		   		 (XSLEGP		),
 		.XSPINDLE	   		 (XSPINDLE		),
 		.XTELP		   		 (XTELP			),
 		.XTELPFO	   		 (XTELPFO		),
 		.XTESTDA	   		 (XTESTDA		),
 		.XTEXO		   		 (XTEXO			),
 		.XTRACK		   		 (XTRACK		),
 		.XTRAY		   		 (XTRAY			),
 		.XVGAIN		   		 (XVGAIN		),
 		.XVGAIP		   		 (XVGAIP		),
 		.XVREF15	   		 (XVREF15		),
 		.XVREF21	   		 (XVREF21		),

// 		.VD33D		  		 (VD33D			),
// 		.VD18D		  		 (VD18D			),
 		.VDD3MIX1	  		 (VDD3MIX1		),
 		.VDD3MIX2	  		 (VDD3MIX2		),
 		.AVDD_AD	  		 (AVDD_AD		),
 		.AVDD_ATT	  		 (AVDD_ATT		),
 		.AVDD_DA	  		 (AVDD_DA		),
 		.AVDD_DPD	  		 (AVDD_DPD		),
 		.AVDD_LPF	  		 (AVDD_LPF		),
 		.AVDD_RAD	  		 (AVDD_RAD		),
 		.AVDD_REF	  		 (AVDD_REF		),
 		.AVDD_SVO	  		 (AVDD_SVO		),
 		.AVDD_VGA	  		 (AVDD_VGA		),
 		.AVSS_AD	  		 (AVSS_AD		),
 		.AVSS_ATT	  		 (AVSS_ATT		),
 		.AVSS_DA	  		 (AVSS_DA		),
 		.AVSS_DPD	  		 (AVSS_DPD		),
 		.AVSS_GA	  		 (AVSS_GA		),
 		.AVSS_GD	  		 (AVSS_GD		),
 		.AVSS_LPF	  		 (AVSS_LPF		),
 		.AVSS_RAD	  		 (AVSS_RAD		),
 		.AVSS_REF	  		 (AVSS_REF		),
 		.AVSS_SVO	  		 (AVSS_SVO		),
 		.AVSS_VGA	  		 (AVSS_VGA		),
// 		.AZEC_GND	  		 (AZEC_GND		),
// 		.AZEC_VDD	  		 (AZEC_VDD		),
 		.DV18_RCKA	  		 (DV18_RCKA		),
 		.DV18_RCKD	  		 (DV18_RCKD		),
// 		.DVDD	      		 (DVDD	    	),
// 		.DVSS	      		 (DVSS	    	),
 		.DVSS_RCKA	  		 (DVSS_RCKA		),
 		.DVSS_RCKD	  		 (DVSS_RCKD		),
// 		.FAD_GND	  		 (FAD_GND		),
// 		.FAD_VDD	  		 (FAD_VDD		),
// 		.GNDD		  		 (GNDD			),

// Consumer IR Interface (1)
        .irrx                   (irrx           ),// 1,   consumer infra-red remote controller/wireless keyboard

// General Purpose IO (8,GPIO)
        .gpio                   (gpio[28:0]		), // 15,  General purpose io
// EJTAG Interface (5)
        .p_j_tdo                (p_j_tdo        ), // 1,   serial test data output
        .p_j_tdi                (p_j_tdi        ), // 1,   serial test data input
        .p_j_tms                (p_j_tms        ), // 1,   test mode select
        .p_j_tclk               (p_j_tclk       ), // 1,   test clock
        .p_j_rst_               (p_j_rst_       ), // 1,   test reset
// power and ground
		.vddcore				(1'b1),//		core vdd
//		.vsscore				(1'b0),//		core vss
		.vddio					(1'b1),//		io vdd
		.gnd					(1'b0),//		io vss
		.cpu_pllvdd				(1'b1),//		pll vdd for cpu pll
		.cpu_pllvss				(1'b0),//		pll vss for cpu pll
		.f27_pllvdd				(1'b1),//		pll vdd for f27 pll
		.f27_pllvss				(1'b0),//		pll vss for f27 pll
// system (4)
        .p_crst_                (cold_rst_      ), // 1,   cold reset
        .test_mode              (test_mode      ),
//        .test_sel				(test_sel		),
     //   .p_x48in                (f48_clk        ), // 1,   crystal input (48 MHz for USB)
     //   .p_x48out               (xout_48        )  // 1,   crystal output
        .p_x27in				(f27_clk	   	),// 1,   crystal input (27 MHz for video output)

       .p_x27out        		(xout_27   		), // 1,   crystal output
//ADC (5)
		.vd33a_adc				(vd33a_adc	 	),// 1,	3.3V Analog VDD for ADC.
		.xmic1in     			(xmic1in     	),// 1,   ADC microphone input 1.
		.xadc_vref   			(xadc_vref   	),// 1,   ADC reference voltage (refered to the reference circuit).
		.xmic2in     			(xmic2in     	),// 1,   ADC microphone input 2.
		.gnda_adc    			(gnda_adc    	) // 1,   Analog GND for ADC.

        );
`endif	// End ifdef POST_GSIM for chip

`ifdef INC_CORE
//========COMBO mode share pin	==================================
//ejtag interface
//`ifdef	INC_SB
//When test uart, open this connection
// tranif1 (p_j_tdi,       uart_rx    	,       (COMBO_mode | IDE_mode | PCI_mode | TVENC_TEST_mode | SERVO_ONLY_mode | BIST_TEST_mode) & ~Cpu_probe_en);
//`else
 tranif1 (p_j_tdi,       ext_gpio[16]	,       (COMBO_mode | IDE_mode | PCI_mode | TVENC_TEST_mode | SERVO_ONLY_mode | BIST_TEST_mode) & ~Cpu_probe_en);
//`endif

 tranif1 (p_j_tdo		,       ext_gpio[17]    ,       (COMBO_mode | IDE_mode | PCI_mode | TVENC_TEST_mode | SERVO_ONLY_mode ) & ~Cpu_probe_en);	//JFR2003-12-02
 tranif1 (p_j_tms		,       ext_gpio[18]    ,       (COMBO_mode | IDE_mode | PCI_mode | TVENC_TEST_mode | SERVO_ONLY_mode ) & ~Cpu_probe_en);	//JFR2003-12-02
 tranif1 (p_j_tclk		,       ext_gpio[19]    ,       (COMBO_mode | IDE_mode | PCI_mode | TVENC_TEST_mode | SERVO_ONLY_mode ) & ~Cpu_probe_en);	//JFR2003-12-02
 tranif1 (p_j_rst_		,       ext_gpio[20]    ,       (COMBO_mode | IDE_mode | PCI_mode | TVENC_TEST_mode | SERVO_ONLY_mode ) & ~Cpu_probe_en);	//JFR2003-12-02

 tranif1 (p_j_tdi		,       ext_p_j_tdi	    ,       (COMBO_mode | IDE_mode | PCI_mode | TVENC_TEST_mode | SERVO_ONLY_mode ) & Cpu_probe_en);	//JFR2003-12-02
 tranif1 (p_j_tdo		,       ext_p_j_tdo	    ,       (COMBO_mode | IDE_mode | PCI_mode | TVENC_TEST_mode | SERVO_ONLY_mode ) & Cpu_probe_en);	//JFR2003-12-02
 tranif1 (p_j_tms		,       ext_p_j_tms	    ,       (COMBO_mode | IDE_mode | PCI_mode | TVENC_TEST_mode | SERVO_ONLY_mode ) & Cpu_probe_en);	//JFR2003-12-02
 tranif1 (p_j_tclk		,       ext_p_j_tclk	,       (COMBO_mode | IDE_mode | PCI_mode | TVENC_TEST_mode | SERVO_ONLY_mode ) & Cpu_probe_en);	//JFR2003-12-02
 tranif1 (p_j_rst_		,       ext_p_j_rst_	,       (COMBO_mode | IDE_mode | PCI_mode | TVENC_TEST_mode | SERVO_ONLY_mode ) & Cpu_probe_en);	//JFR2003-12-02


// wire		scbscl ;
// wire		scbsda ;

// tranif1 (xsflag0       	,       vsync_            ,       COMBO_mode);
// tranif1 (xsflag1       	,       hsync_         	  ,       COMBO_mode);
// tranif1 (gpio[4]       	,       ext_gpio[4]       ,       COMBO_mode);
 tranif1 (gpio[5]       	,       scbscl            ,       COMBO_mode & scb_en);
 tranif1 (gpio[6]       	,       scbsda            ,       COMBO_mode & scb_en);
 tranif1 (gpio[7]       	,       i2si_data         ,       COMBO_mode & i2si_data_en);

 tranif1 (gpio[0]       	,       ext_gpio[0]       ,       COMBO_mode | IDE_mode);//JFR,2003-11-19
 tranif1 (gpio[1]       	,       ext_gpio[1]       ,       COMBO_mode | IDE_mode);//JFR,2003-11-19
// tranif1 (gpio[2]       	,       ext_gpio[2]       ,       COMBO_mode | PCI_mode);//JFR,2003-11-19
// tranif1 (gpio[3]       	,       ext_gpio[3]       ,       COMBO_mode | PCI_mode);//JFR,2003-11-19
 tranif1 (gpio[2]       	,       ext_gpio[2]       ,       COMBO_mode | (PCI_mode & ~pci_mode_ide_en));//JFR,2003-12-03
 tranif1 (gpio[3]       	,       ext_gpio[3]       ,       COMBO_mode | (PCI_mode & ~pci_mode_ide_en & ~i2si_data_en));//JFR,2003-12-03

 tranif1 (gpio[4]       	,       ext_gpio[4]       ,       COMBO_mode);

 tranif1 (gpio[5]       	,       ext_gpio[5]       ,       (COMBO_mode | IDE_mode | PCI_mode) & ~scb_en);
 tranif1 (gpio[6]       	,       ext_gpio[6]       ,       (COMBO_mode | IDE_mode | PCI_mode) & ~scb_en);
 tranif1 (gpio[7]       	,       ext_gpio[7]       ,       (COMBO_mode | IDE_mode) & ~i2si_data_en);

//256 pin package JFR 030820
 tranif1 (gpio[8]        ,   	 ext_gpio[8]  	 ,       COMBO_mode | IDE_mode);
 tranif1 (gpio[9]        ,   	 ext_gpio[9]  	 ,       COMBO_mode | IDE_mode);
 tranif1 (gpio[10]       ,		 ext_gpio[10]    ,		 COMBO_mode);
// tranif1 (gpio[11]       ,       ext_gpio[11]    ,       COMBO_mode | IDE_mode);
// tranif1 (gpio[12]       ,       ext_gpio[12]    ,       COMBO_mode | IDE_mode);
 tranif1 (gpio[11]       ,       ext_gpio[11]    ,       COMBO_mode );//JFR031119
 tranif1 (gpio[12]       ,       ext_gpio[12]    ,       COMBO_mode );//JFR031119
 tranif1 (gpio[13]       ,       ext_gpio[13]    ,       COMBO_mode | IDE_mode);
 tranif1 (gpio[14]       ,       ext_gpio[14]    ,       COMBO_mode | IDE_mode | TVENC_TEST_mode);
 tranif1 (gpio[15]       ,       ext_gpio[15]    ,       COMBO_mode | IDE_mode | TVENC_TEST_mode);

// tranif1 (eeprom_addr[13],       ext_gpio[16]    ,       COMBO_mode & ~Rom_en);             	//JFR2003-12-02
// tranif1 (eeprom_addr[14],       ext_gpio[17]    ,       (COMBO_mode | IDE_mode) & ~Rom_en);	//JFR2003-12-02
// tranif1 (eeprom_addr[15],       ext_gpio[18]    ,       (COMBO_mode | IDE_mode) & ~Rom_en);	//JFR2003-12-02
// tranif1 (eeprom_addr[16],       ext_gpio[19]    ,       (COMBO_mode | IDE_mode) & ~Rom_en);	//JFR2003-12-02
// tranif1 (eeprom_addr[17],       ext_gpio[20]    ,       (COMBO_mode | IDE_mode) & ~Rom_en);	//JFR2003-12-02
 tranif1 (eeprom_addr[18],       ext_gpio[21]    ,       (COMBO_mode | IDE_mode) & ~Rom_en);	//JFR2003-12-02
 tranif1 (eeprom_addr[19],       ext_gpio[22]    ,       (COMBO_mode | IDE_mode) & ~Rom_en);  //JFR2003-12-02
 tranif1 (eeprom_addr[20],       ext_gpio[23]    ,       (COMBO_mode | IDE_mode) & ~Rom_en);  //JFR2003-12-02

 tranif1 (gpio[24]       ,       ext_gpio[24]    ,       ~PCI_mode);
 tranif1 (gpio[25]       ,       ext_gpio[25]    ,       ~PCI_mode);
 tranif1 (gpio[26]       ,       ext_gpio[26]    ,       ~PCI_mode);
 tranif1 (gpio[27]       ,       ext_gpio[27]    ,       ~PCI_mode);
 tranif1 (gpio[28]       ,   	 ext_gpio[28]  	 ,       ~PCI_mode);



//======================= IDE mode share pin =============================
wire	ext_atacs0_, ext_atacs1_;
wire	ext_atadiow_    ,
		ext_atadior_    ,
		ext_ataiordy    ,
		ext_ataintrq    ,
		ext_atareset_   ,
		ext_atadmarq 	,
		ext_atadmack_	;
wire[15:0]ext_atadd;
wire[2:0]ext_atada;

//tranif1 (gpio[0]   		 ,       ext_ataiordy   ,       IDE_mode);   // JFR,2003-11-19
//tranif1 (gpio[1]   		 ,       ext_ataintrq   ,       IDE_mode);   // JFR,2003-11-19
tranif1 (gpio[2]   		 ,       ext_atacs0_    ,       IDE_mode | (PCI_mode & pci_mode_ide_en));// JFR,2003-11-19
tranif1 (gpio[3]   		 ,       ext_atacs1_    ,       IDE_mode | (PCI_mode & pci_mode_ide_en));// JFR,2003-11-19
tranif1 (gpio[4]   		 ,       ext_atadmarq   ,       IDE_mode);   // yuky,2002-06-17
tranif1 (gpio[10]  		 ,       ext_atareset_  ,       IDE_mode);   // yuky,2002-06-17
tranif1 (gpio[11]   	 ,       ext_ataiordy   ,       IDE_mode| (PCI_mode & pci_mode_ide_en));// JFR,2003-11-19
tranif1 (gpio[12]   	 ,       ext_ataintrq   ,       IDE_mode);   // JFR,2003-11-19
//add !ROM_en select in ide mode, tod 2003-12-10
tranif1 (eeprom_data[0]  ,       ext_atadd[0]   ,       IDE_mode | (PCI_mode & pci_mode_ide_en));   // JFR 2003-11-19
tranif1 (eeprom_data[1]  ,       ext_atadd[1]   ,       IDE_mode | (PCI_mode & pci_mode_ide_en));   // JFR 2003-11-19
tranif1 (eeprom_data[2]  ,       ext_atadd[2]   ,       IDE_mode | (PCI_mode & pci_mode_ide_en));   // JFR 2003-11-19
tranif1 (eeprom_data[3]  ,       ext_atadd[3]   ,       IDE_mode | (PCI_mode & pci_mode_ide_en));   // JFR 2003-11-19
tranif1 (eeprom_data[4]  ,       ext_atadd[4]   ,       IDE_mode | (PCI_mode & pci_mode_ide_en));   // JFR 2003-11-19
tranif1 (eeprom_data[5]  ,       ext_atadd[5]   ,       IDE_mode | (PCI_mode & pci_mode_ide_en));   // JFR 2003-11-19
tranif1 (eeprom_data[6]  ,       ext_atadd[6]   ,       IDE_mode | (PCI_mode & pci_mode_ide_en));   // JFR 2003-11-19
tranif1 (eeprom_data[7]  ,       ext_atadd[7]   ,       IDE_mode | (PCI_mode & pci_mode_ide_en));   // JFR 2003-11-19
tranif1 (eeprom_addr[0]  ,       ext_atadd[8]   ,       IDE_mode | (PCI_mode & pci_mode_ide_en));   // JFR 2003-11-19
tranif1 (eeprom_addr[1]  ,       ext_atadd[9]   ,       IDE_mode | (PCI_mode & pci_mode_ide_en));   // JFR 2003-11-19
tranif1 (eeprom_addr[2]  ,       ext_atadd[10]  ,       IDE_mode | (PCI_mode & pci_mode_ide_en));   // JFR 2003-11-19
tranif1 (eeprom_addr[3]  ,       ext_atadd[11]  ,       IDE_mode | (PCI_mode & pci_mode_ide_en));   // JFR 2003-11-19
tranif1 (eeprom_addr[4]  ,       ext_atadd[12]  ,       IDE_mode | (PCI_mode & pci_mode_ide_en));   // JFR 2003-11-19
tranif1 (eeprom_addr[5]  ,       ext_atadd[13]  ,       IDE_mode | (PCI_mode & pci_mode_ide_en));   // JFR 2003-11-19
tranif1 (eeprom_addr[6]  ,       ext_atadd[14]  ,       IDE_mode | (PCI_mode & pci_mode_ide_en));   // JFR 2003-11-19
tranif1 (eeprom_addr[7]  ,       ext_atadd[15]  ,       IDE_mode | (PCI_mode & pci_mode_ide_en));   // JFR 2003-11-19
tranif1 (eeprom_addr[8]  ,       ext_atada[0]   ,       IDE_mode | (PCI_mode & pci_mode_ide_en));   // JFR 2003-11-19
tranif1 (eeprom_addr[9]  ,       ext_atada[1]   ,       IDE_mode | (PCI_mode & pci_mode_ide_en));   // JFR 2003-11-19
tranif1 (eeprom_addr[10] ,       ext_atada[2]   ,       IDE_mode | (PCI_mode & pci_mode_ide_en));   // JFR 2003-11-19
tranif1 (eeprom_addr[11] ,       ext_atadiow_   ,       IDE_mode | (PCI_mode & pci_mode_ide_en));   // JFR 2003-11-19
tranif1 (eeprom_addr[12] ,       ext_atadior_   ,       IDE_mode | (PCI_mode & pci_mode_ide_en));   // JFR 2003-11-19
tranif1 (eeprom_addr[13] ,       ext_atadmack_  ,       IDE_mode);   // JFR 2003-11-25


wire	[39:0]	cpu_testout;
wire			cpu_trig_signal;
wire	[4:0]	cpu_testin;
wire	[9:0]	cpu_trigcond;
wire			cpu_trig_en;

//======== PCI mode share pin ============================
 tranif1 (gpio[0]       	 ,       ext_int[0]        ,       PCI_mode);
 tranif1 (gpio[1]       	 ,       pci_req_          ,       PCI_mode);
// tranif1 (gpio[2]       	 ,       pci_gnta_         ,       PCI_mode);//JFR031119
 tranif1 (gpio[3]       	 ,       i2si_data         ,       PCI_mode & i2si_data_en);//JFR 2003-12-03
// tranif1 (gpio[3]       	 ,       pci_frame_        ,       PCI_mode);//JFR031119
 tranif1 (gpio[4]       	 ,       pci_clk           ,       PCI_mode);//JFR031119
 tranif1 (gpio[7]       	 ,       pci_cbe_[0]       ,       PCI_mode);//JFR031119
 tranif1 (gpio[8]       	 ,       pci_cbe_[1]       ,       PCI_mode);//JFR031119
 tranif1 (gpio[9]       	 ,       pci_cbe_[2]       ,       PCI_mode);//JFR031119
 tranif1 (gpio[10]       	 ,       pci_cbe_[3]       ,       PCI_mode);//JFR031119
 tranif1 (gpio[11]       	 ,       pci_ad[29]        ,       PCI_mode);//JFR031119
 tranif1 (gpio[12]       	 ,       pci_ad[30]        ,       PCI_mode);//JFR031119
 tranif1 (gpio[13]       	 ,       pci_ad[31]        ,       PCI_mode);//JFR031119
 tranif1 (gpio[14]       	,        pci_gnta_      	,      PCI_mode);//JFR031119
 tranif1 (gpio[15]       	,        pci_frame_      	,      PCI_mode);//JFR031119
 tranif1 (uart_tx			 ,		 pci_gntb_		   ,	   PCI_mode & pci_gntb_en);
 tranif1 (uart_tx			 ,		 ext_ataintrq	   ,	   PCI_mode & pci_mode_ide_en);//JFR031119
 tranif1 (gpio[24]       	 ,       pci_irdy_		   ,	   PCI_mode);
 tranif1 (gpio[25]       	 ,       pci_trdy_		   ,	   PCI_mode);
 tranif1 (gpio[26]       	 ,       pci_stop_		   ,	   PCI_mode);
 tranif1 (gpio[27]       	 ,       pci_devsel_	   ,	   PCI_mode);
 tranif1 (gpio[28]       	 ,       pci_par		   ,	   PCI_mode);

 tranif1 (cold_rst_wire_	 ,       ext_atareset_	   ,       PCI_mode);   // JFR 2003-11-24, norman, 2003-11-25


 tranif1 (eeprom_data[0]     ,       pci_ad[0]         ,       PCI_mode);
 tranif1 (eeprom_data[1]     ,       pci_ad[1]         ,       PCI_mode);
 tranif1 (eeprom_data[2]     ,       pci_ad[2]         ,       PCI_mode);
 tranif1 (eeprom_data[3]     ,       pci_ad[3]         ,       PCI_mode);
 tranif1 (eeprom_data[4]     ,       pci_ad[4]         ,       PCI_mode);
 tranif1 (eeprom_data[5]     ,       pci_ad[5]         ,       PCI_mode);
 tranif1 (eeprom_data[6]     ,       pci_ad[6]         ,       PCI_mode);
 tranif1 (eeprom_data[7]     ,       pci_ad[7]         ,       PCI_mode);
 tranif1 (eeprom_addr[0]     ,       pci_ad[8]         ,       PCI_mode);
 tranif1 (eeprom_addr[1]     ,       pci_ad[9]         ,       PCI_mode);
 tranif1 (eeprom_addr[2]     ,       pci_ad[10]        ,       PCI_mode);
 tranif1 (eeprom_addr[3]     ,       pci_ad[11]        ,       PCI_mode);
 tranif1 (eeprom_addr[4]     ,       pci_ad[12]        ,       PCI_mode);
 tranif1 (eeprom_addr[5]     ,       pci_ad[13]        ,       PCI_mode);
 tranif1 (eeprom_addr[6]     ,       pci_ad[14]        ,       PCI_mode);
 tranif1 (eeprom_addr[7]     ,       pci_ad[15]        ,       PCI_mode);
 tranif1 (eeprom_addr[8]     ,       pci_ad[16]        ,       PCI_mode);
 tranif1 (eeprom_addr[9]     ,       pci_ad[17]        ,       PCI_mode);
 tranif1 (eeprom_addr[10]    ,       pci_ad[18]        ,       PCI_mode);
 tranif1 (eeprom_addr[11]    ,       pci_ad[19]        ,       PCI_mode);
 tranif1 (eeprom_addr[12]    ,       pci_ad[20]        ,       PCI_mode);
 tranif1 (eeprom_addr[13]    ,       pci_ad[21]        ,       PCI_mode);
 tranif1 (eeprom_addr[14]    ,       pci_ad[22]        ,       PCI_mode);
 tranif1 (eeprom_addr[15]    ,       pci_ad[23]        ,       PCI_mode);
 tranif1 (eeprom_addr[16]    ,   	 pci_ad[24]    	   ,       PCI_mode);
 tranif1 (eeprom_addr[17]    ,   	 pci_ad[25]    	   ,       PCI_mode);
 tranif1 (eeprom_addr[18]    ,   	 pci_ad[26]    	   ,       PCI_mode);
 tranif1 (eeprom_addr[19]    ,   	 pci_ad[27]    	   ,       PCI_mode);
 tranif1 (eeprom_addr[20]    ,   	 pci_ad[28]   	   ,       PCI_mode);

//====================== Servo test/sram Mode ===================================
//====================== Servo test/sram Mode ===================================
wire	[22:0]	ex_up	;
tranif1 (eeprom_ce_			,	ex_up[22]		,SERVO_SRAM_mode | SERVO_TEST_mode );
tranif1 (eeprom_oe_			,	ex_up[21]		,SERVO_SRAM_mode | SERVO_TEST_mode );
tranif1 (eeprom_addr[20]	,	ex_up[20]		,SERVO_SRAM_mode | SERVO_TEST_mode );
tranif1 (eeprom_addr[19]	,	ex_up[19]		,SERVO_SRAM_mode | SERVO_TEST_mode );
tranif1 (eeprom_addr[18]	,	ex_up[18]		,SERVO_SRAM_mode | SERVO_TEST_mode );
tranif1 (eeprom_addr[17]	,	ex_up[17]		,SERVO_SRAM_mode | SERVO_TEST_mode );
tranif1 (eeprom_addr[16]	,	ex_up[16]		,SERVO_SRAM_mode | SERVO_TEST_mode );
tranif1 (eeprom_addr[15]	,	ex_up[15]		,SERVO_SRAM_mode | SERVO_TEST_mode );
tranif1 (eeprom_addr[14]	,	ex_up[14]		,SERVO_SRAM_mode | SERVO_TEST_mode );
tranif1 (irrx				,	ex_up[13]		,SERVO_SRAM_mode | SERVO_TEST_mode );
tranif1 (spdif				,	ex_up[12]		,SERVO_SRAM_mode | SERVO_TEST_mode );
tranif1 (sv_f24_clk_wire	,	ex_up[11]		,SERVO_SRAM_mode | SERVO_TEST_mode );
tranif1 (i2so_lrck			,	ex_up[10]		,SERVO_SRAM_mode | SERVO_TEST_mode );
tranif1 (gpio[9]			,	ex_up[9]		,SERVO_SRAM_mode | SERVO_TEST_mode );
tranif1 (gpio[8]			,	ex_up[8]		,SERVO_SRAM_mode | SERVO_TEST_mode );
tranif1 (gpio[7]			,	ex_up[7]		,SERVO_SRAM_mode | SERVO_TEST_mode );
tranif1 (gpio[6]			,	ex_up[6]		,SERVO_SRAM_mode | SERVO_TEST_mode );
tranif1 (gpio[5]			,	ex_up[5]		,SERVO_SRAM_mode | SERVO_TEST_mode );
tranif1 (gpio[4]			,	ex_up[4]		,SERVO_SRAM_mode | SERVO_TEST_mode );
tranif1 (gpio[3]			,	ex_up[3]		,SERVO_SRAM_mode | SERVO_TEST_mode );
tranif1 (gpio[2]			,	ex_up[2]		,SERVO_SRAM_mode | SERVO_TEST_mode );
tranif1 (gpio[1]			,	ex_up[1]		,SERVO_SRAM_mode | SERVO_TEST_mode );
tranif1 (gpio[0]			,	ex_up[0]		,SERVO_SRAM_mode | SERVO_TEST_mode );



//==============================================================================
//	FLASH in
//==============================================================================
assign	ext_eeprom_ce_	=	eeprom_ce_;
assign	ext_eeprom_oe_	=	eeprom_oe_;
assign	ext_eeprom_we_	=	eeprom_we_;

// tranif1	(eeprom_data[0]		,	ext_eeprom_data[0],		COMBO_mode);
// tranif1	(eeprom_data[1]		,	ext_eeprom_data[1],		COMBO_mode);
// tranif1	(eeprom_data[2]		,	ext_eeprom_data[2],		COMBO_mode);
// tranif1	(eeprom_data[3]		,	ext_eeprom_data[3],		COMBO_mode);
// tranif1	(eeprom_data[4]		,	ext_eeprom_data[4],		COMBO_mode);
// tranif1	(eeprom_data[5]		,	ext_eeprom_data[5],		COMBO_mode);
// tranif1	(eeprom_data[6]		,	ext_eeprom_data[6],		COMBO_mode);
// tranif1	(eeprom_data[7]		,	ext_eeprom_data[7],		COMBO_mode);
//
// tranif1	(eeprom_addr[0]		,	ext_eeprom_addr[0],		COMBO_mode);
// tranif1	(eeprom_addr[1]		,	ext_eeprom_addr[1],		COMBO_mode);
// tranif1	(eeprom_addr[2]		,	ext_eeprom_addr[2],		COMBO_mode);
// tranif1	(eeprom_addr[3]		,	ext_eeprom_addr[3],		COMBO_mode);
// tranif1	(eeprom_addr[4]		,	ext_eeprom_addr[4],		COMBO_mode);
// tranif1	(eeprom_addr[5]		,	ext_eeprom_addr[5],		COMBO_mode);
// tranif1	(eeprom_addr[6]		,	ext_eeprom_addr[6],		COMBO_mode);
// tranif1	(eeprom_addr[7]		,	ext_eeprom_addr[7],		COMBO_mode);
// tranif1	(eeprom_addr[8]		,	ext_eeprom_addr[8],		COMBO_mode);
// tranif1	(eeprom_addr[9]		,	ext_eeprom_addr[9],		COMBO_mode);
// tranif1	(eeprom_addr[10]	,	ext_eeprom_addr[10],	COMBO_mode);
// tranif1	(eeprom_addr[11]	,	ext_eeprom_addr[11],	COMBO_mode);
// tranif1	(eeprom_addr[12]	,	ext_eeprom_addr[12],	COMBO_mode);
// tranif1	(eeprom_addr[13]	,	ext_eeprom_addr[13],	COMBO_mode);
// tranif1	(eeprom_addr[14]	,	ext_eeprom_addr[14],	COMBO_mode);
// tranif1	(eeprom_addr[15]	,	ext_eeprom_addr[15],	COMBO_mode);
// tranif1	(eeprom_addr[16]	,	ext_eeprom_addr[16],	COMBO_mode);
// tranif1	(eeprom_addr[17]	,	ext_eeprom_addr[17],	COMBO_mode);
// tranif1	(eeprom_addr[18]	,	ext_eeprom_addr[18],	COMBO_mode);
// tranif1	(eeprom_addr[19]	,	ext_eeprom_addr[19],	COMBO_mode);
// tranif1	(eeprom_addr[20]	,	ext_eeprom_addr[20],	COMBO_mode);

assign	ext_eeprom_addr[0]		=	eeprom_addr[0]		;
assign	ext_eeprom_addr[1]      =	eeprom_addr[1]	    ;
assign	ext_eeprom_addr[2]      =	eeprom_addr[2]	    ;
assign	ext_eeprom_addr[3]      =	eeprom_addr[3]	    ;
assign	ext_eeprom_addr[4]      =	eeprom_addr[4]	    ;
assign	ext_eeprom_addr[5]      =	eeprom_addr[5]	    ;
assign	ext_eeprom_addr[6]      =	eeprom_addr[6]	    ;
assign	ext_eeprom_addr[7]      =	eeprom_addr[7]	    ;
assign	ext_eeprom_addr[8]      =	eeprom_addr[8]	    ;
assign	ext_eeprom_addr[9]      =	eeprom_addr[9]	    ;
assign	ext_eeprom_addr[10]     =	eeprom_addr[10]     ;
assign	ext_eeprom_addr[11]     =	eeprom_addr[11]     ;
assign	ext_eeprom_addr[12]     =	eeprom_addr[12]     ;
assign	ext_eeprom_addr[13]     =	eeprom_addr[13]     ;
assign	ext_eeprom_addr[14]     =	eeprom_addr[14]     ;
assign	ext_eeprom_addr[15]     =	eeprom_addr[15]     ;
assign	ext_eeprom_addr[16]     =	eeprom_addr[16]     ;
assign	ext_eeprom_addr[17]     =	eeprom_addr[17]     ;
assign	ext_eeprom_addr[18]     =	eeprom_addr[18]     ;
assign	ext_eeprom_addr[19]     =	eeprom_addr[19]     ;
assign	ext_eeprom_addr[20]     =	eeprom_addr[20]     ;


 tranif1	(eeprom_data[0]	,	ext_eeprom_data[0],		IDE_mode | COMBO_mode | PCI_mode | SERVO_ONLY_mode);
 tranif1	(eeprom_data[1]	,	ext_eeprom_data[1],		IDE_mode | COMBO_mode | PCI_mode | SERVO_ONLY_mode);
 tranif1	(eeprom_data[2]	,	ext_eeprom_data[2],		IDE_mode | COMBO_mode | PCI_mode | SERVO_ONLY_mode);
 tranif1	(eeprom_data[3]	,	ext_eeprom_data[3],		IDE_mode | COMBO_mode | PCI_mode | SERVO_ONLY_mode);
 tranif1	(eeprom_data[4]	,	ext_eeprom_data[4],		IDE_mode | COMBO_mode | PCI_mode | SERVO_ONLY_mode);
 tranif1	(eeprom_data[5]	,	ext_eeprom_data[5],		IDE_mode | COMBO_mode | PCI_mode | SERVO_ONLY_mode);
 tranif1	(eeprom_data[6]	,	ext_eeprom_data[6],		IDE_mode | COMBO_mode | PCI_mode | SERVO_ONLY_mode);
 tranif1	(eeprom_data[7]	,	ext_eeprom_data[7],		IDE_mode | COMBO_mode | PCI_mode | SERVO_ONLY_mode);

//BIST TEST MODE
 tranif1 (eeprom_data[0]     ,       cpu_bist_mode     		,       BIST_TEST_mode);
 tranif1 (eeprom_data[1]     ,       cpu_bist_finish   		,       BIST_TEST_mode);
 tranif1 (eeprom_data[2]     ,       cpu_bist_err_vec[0]  	,       BIST_TEST_mode);
 tranif1 (eeprom_data[3]     ,       cpu_bist_err_vec[1]  	,       BIST_TEST_mode);
 tranif1 (eeprom_data[4]     ,       cpu_bist_err_vec[2]  	,       BIST_TEST_mode);
 tranif1 (eeprom_data[5]     ,       cpu_bist_err_vec[3]  	,       BIST_TEST_mode);
 tranif1 (eeprom_data[6]     ,       cpu_bist_err_vec[4]  	,       BIST_TEST_mode);
 tranif1 (eeprom_data[7]     ,       cpu_bist_err_vec[5]  	,       BIST_TEST_mode);


 tranif1 (eeprom_addr[0]     ,       video_bist_mode     		,       BIST_TEST_mode);
 tranif1 (eeprom_addr[1]     ,       video_bist_finish   		,       BIST_TEST_mode);
 tranif1 (eeprom_addr[2]     ,       video_bist_err_vec[0]  	,       BIST_TEST_mode);
 tranif1 (eeprom_addr[3]     ,       video_bist_err_vec[1]  	,       BIST_TEST_mode);
 tranif1 (eeprom_addr[4]     ,       video_bist_err_vec[2]  	,       BIST_TEST_mode);
 tranif1 (eeprom_addr[5]     ,       video_bist_err_vec[3]  	,       BIST_TEST_mode);
 tranif1 (eeprom_addr[6]     ,       video_bist_err_vec[4]  	,       BIST_TEST_mode);
 tranif1 (eeprom_addr[7]     ,       video_bist_err_vec[5]  	,       BIST_TEST_mode);
 tranif1 (eeprom_addr[8]     ,       video_bist_err_vec[6]  	,       BIST_TEST_mode);
 tranif1 (eeprom_addr[9]     ,       video_bist_err_vec[7]  	,       BIST_TEST_mode);
 tranif1 (eeprom_addr[10]     ,      video_bist_err_vec[8]  	,       BIST_TEST_mode);
 tranif1 (eeprom_addr[11]     ,      video_bist_err_vec[9]  	,       BIST_TEST_mode);
 tranif1 (eeprom_addr[12]     ,      video_bist_err_vec[10]  	,       BIST_TEST_mode);


`ifdef TEST_MODE
//tranif1 (test_mode, 1, 1);
		assign	test_mode	=	1;
	`ifdef	BIST_TEST_MODE
		assign	eeprom_addr[19]	=	0;
	 `endif
	 `ifdef	SCAN_TEST_MODE
	 	assign	eeprom_addr[19]	=	1;
	 `endif
//`else
//tranif0 (test_mode, 0,1);
`endif



//ANALOGT_mode
wire	[9:0] vdi4vdac;
wire	[9:0] vdo4vdac;
wire	cpu_pll_fck;
wire	cpu_pll_lock;
wire	f27_pll_fck;
wire	f27_pll_lock;
wire	audio_pll_fck;
wire	audio_pll_lock;
// tranif1	(atadd[0]       ,		vdi4vdac[0]		,		ANALOGT_mode);	// Yuky, 2002-11-06//high active
// tranif1	(atadd[1]       ,		vdi4vdac[1]		,		ANALOGT_mode);  // Yuky, 2002-11-06
// tranif1	(atadd[2]       ,		vdi4vdac[2]		,		ANALOGT_mode);  // Yuky, 2002-11-06
// tranif1	(atadd[3]       ,		vdi4vdac[3]		,		ANALOGT_mode);  // Yuky, 2002-11-06
// tranif1	(atadd[4]       ,		vdi4vdac[4]		,		ANALOGT_mode);  // Yuky, 2002-11-06
// tranif1	(atadd[5]       ,		vdi4vdac[5]		,		ANALOGT_mode);  // Yuky, 2002-11-06
// tranif1	(atadd[6]       ,		vdi4vdac[6]		,		ANALOGT_mode);  // Yuky, 2002-11-06
// tranif1	(atadd[7]       ,		vdi4vdac[7]		,		ANALOGT_mode);  // Yuky, 2002-11-06
// tranif1	(atadd[8]       ,		vdi4vdac[8]		,		ANALOGT_mode);  // Yuky, 2002-11-06
// tranif1	(atadd[9]       ,		vdi4vdac[9]		,		ANALOGT_mode);  // Yuky, 2002-11-06
// tranif1	(atadd[10]      ,		vdo4vdac[0]		,		ANALOGT_mode);  // Yuky, 2002-11-06
// tranif1	(atadd[11]      ,		vdo4vdac[1]		,		ANALOGT_mode);  // Yuky, 2002-11-06
// tranif1	(atadd[12]      ,		vdo4vdac[2]		,		ANALOGT_mode);  // Yuky, 2002-11-06
// tranif1	(atadd[13]      ,		vdo4vdac[3]		,		ANALOGT_mode);  // Yuky, 2002-11-06
// tranif1	(atadd[14]      ,		vdo4vdac[4]		,		ANALOGT_mode);  // Yuky, 2002-11-06
// tranif1	(atadd[15]      ,		vdo4vdac[5]		,		ANALOGT_mode);  // Yuky, 2002-11-06
// tranif1	(atada[0]       ,		vdo4vdac[6]		,		ANALOGT_mode);  // Yuky, 2002-11-06
// tranif1	(atada[1]       ,		vdo4vdac[7]		,		ANALOGT_mode);  // Yuky, 2002-11-06
// tranif1	(atada[2]       ,		vdo4vdac[8]		,		ANALOGT_mode);  // Yuky, 2002-11-06
// tranif1	(atadiow_       ,		vdo4vdac[9]		,		ANALOGT_mode);  // Yuky, 2002-11-06
//
// tranif1	(gpio[4]        ,		cpu_pll_fck		,   	ANALOGT_mode);  // Yuky, 2002-11-06
// tranif1	(gpio[5]		,		cpu_pll_lock	,  		ANALOGT_mode);  // Yuky, 2002-11-06
// tranif1	(gpio[6]		,		f27_pll_fck		,   	ANALOGT_mode);  // Yuky, 2002-11-06
// tranif1	(gpio[7]		,		f27_pll_lock	,      	ANALOGT_mode);  // Yuky, 2002-11-06
// tranif1	(gpio[8]		,		audio_pll_fck	, 		ANALOGT_mode);  // Yuky, 2002-11-06
// tranif1	(gpio[9]		,		audio_pll_lock	,		ANALOGT_mode);  // Yuky, 2002-11-06
//=========================== end of the share pin control ========================

//=========================== SDRAM 32bit mode and 16bit mode select ==============
//0-->32bits mode ; 1-->16bits mode
tranif0 (ram_dq[00]       ,       ext_ram_dq[00]    ,       1'b0);       // yuky,2002-07-23
tranif0 (ram_dq[01]       ,       ext_ram_dq[01]    ,       1'b0);       // yuky,2002-07-23
tranif0 (ram_dq[02]       ,       ext_ram_dq[02]    ,       1'b0);       // yuky,2002-07-23
tranif0 (ram_dq[03]       ,       ext_ram_dq[03]    ,       1'b0);       // yuky,2002-07-23
tranif0 (ram_dq[04]       ,       ext_ram_dq[04]    ,       1'b0);       // yuky,2002-07-23
tranif0 (ram_dq[05]       ,       ext_ram_dq[05]    ,       1'b0);       // yuky,2002-07-23
tranif0 (ram_dq[06]       ,       ext_ram_dq[06]    ,       1'b0);       // yuky,2002-07-23
tranif0 (ram_dq[07]       ,       ext_ram_dq[07]    ,       1'b0);       // yuky,2002-07-23

tranif0 (ram_dq[08]       ,       ext_ram_dq[08]    ,       1'b0);       // yuky,2002-07-23
tranif0 (ram_dq[09]       ,       ext_ram_dq[09]    ,       1'b0);       // yuky,2002-07-23
tranif0 (ram_dq[10]       ,       ext_ram_dq[10]    ,       1'b0);       // yuky,2002-07-23
tranif0 (ram_dq[11]       ,       ext_ram_dq[11]    ,       1'b0);       // yuky,2002-07-23
tranif0 (ram_dq[12]       ,       ext_ram_dq[12]    ,       1'b0);       // yuky,2002-07-23
tranif0 (ram_dq[13]       ,       ext_ram_dq[13]    ,       1'b0);       // yuky,2002-07-23
tranif0 (ram_dq[14]       ,       ext_ram_dq[14]    ,       1'b0);       // yuky,2002-07-23
tranif0 (ram_dq[15]       ,       ext_ram_dq[15]    ,       1'b0);       // yuky,2002-07-23

tranif0 (ram_dq[16]       ,       ext_ram_dq[16]    ,       vdata_en);       // yuky,2002-07-23
tranif0 (ram_dq[17]       ,       ext_ram_dq[17]    ,       vdata_en);       // yuky,2002-07-23
tranif0 (ram_dq[18]       ,       ext_ram_dq[18]    ,       vdata_en);       // yuky,2002-07-23
tranif0 (ram_dq[19]       ,       ext_ram_dq[19]    ,       vdata_en);       // yuky,2002-07-23
tranif0 (ram_dq[20]       ,       ext_ram_dq[20]    ,       vdata_en);       // yuky,2002-07-23
tranif0 (ram_dq[21]       ,       ext_ram_dq[21]    ,       vdata_en);       // yuky,2002-07-23
tranif0 (ram_dq[22]       ,       ext_ram_dq[22]    ,       vdata_en);       // yuky,2002-07-23
tranif0 (ram_dq[23]       ,       ext_ram_dq[23]    ,       vdata_en);       // yuky,2002-07-23
tranif0 (ram_dq[24]       ,       ext_ram_dq[24]    ,       vdata_en);       // yuky,2002-07-23
tranif0 (ram_dq[25]       ,       ext_ram_dq[25]    ,       vdata_en);       // yuky,2002-07-23
tranif0 (ram_dq[26]       ,       ext_ram_dq[26]    ,       vdata_en);       // yuky,2002-07-23
tranif0 (ram_dq[27]       ,       ext_ram_dq[27]    ,       vdata_en);       // yuky,2002-07-23
tranif0 (ram_dq[28]       ,       ext_ram_dq[28]    ,       vdata_en);       // yuky,2002-07-23
tranif0 (ram_dq[29]       ,       ext_ram_dq[29]    ,       vdata_en);       // yuky,2002-07-23
tranif0 (ram_dq[30]       ,       ext_ram_dq[30]    ,       vdata_en);       // yuky,2002-07-23
tranif0 (ram_dq[31]       ,       ext_ram_dq[31]    ,       vdata_en);       // yuky,2002-07-23


//external video_in video_out signals
wire	[7:0]	ext_vin_data, ext_vout_data;
wire			ext_vin_pixel_clk, ext_vout_pixel_clk;
wire			ext_vin_v_syncj, ext_vout_v_syncj ;
wire			ext_vin_h_syncj, ext_vout_h_syncj ;

tranif1 (ram_dq[16],ext_vin_data[0] ,(COMBO_mode | IDE_mode | PCI_mode ) & vdata_en & video_in_en) ;       //JFR031128
tranif1 (ram_dq[17],ext_vin_data[1] ,(COMBO_mode | IDE_mode | PCI_mode ) & vdata_en & video_in_en) ;       //JFR031128
tranif1 (ram_dq[18],ext_vin_data[2] ,(COMBO_mode | IDE_mode | PCI_mode ) & vdata_en & video_in_en) ;       //JFR031128
tranif1 (ram_dq[19],ext_vin_data[3] ,(COMBO_mode | IDE_mode | PCI_mode ) & vdata_en & video_in_en) ;       //JFR031128
tranif1 (ram_dq[20],ext_vin_data[4] ,(COMBO_mode | IDE_mode | PCI_mode ) & vdata_en & video_in_en) ;       //JFR031128
tranif1 (ram_dq[21],ext_vin_data[5] ,(COMBO_mode | IDE_mode | PCI_mode ) & vdata_en & video_in_en) ;       //JFR031128
tranif1 (ram_dq[22],ext_vin_data[6] ,(COMBO_mode | IDE_mode | PCI_mode ) & vdata_en & video_in_en) ;       //JFR031128
tranif1 (ram_dq[23],ext_vin_data[7] ,(COMBO_mode | IDE_mode | PCI_mode ) & vdata_en & video_in_en) ;       //JFR031128

tranif1 (ram_dq[16],ext_vout_data[0],(COMBO_mode | IDE_mode | PCI_mode ) & vdata_en & ~video_in_en);       //JFR031128
tranif1 (ram_dq[17],ext_vout_data[1],(COMBO_mode | IDE_mode | PCI_mode ) & vdata_en & ~video_in_en);       //JFR031128
tranif1 (ram_dq[18],ext_vout_data[2],(COMBO_mode | IDE_mode | PCI_mode ) & vdata_en & ~video_in_en);       //JFR031128
tranif1 (ram_dq[19],ext_vout_data[3],(COMBO_mode | IDE_mode | PCI_mode ) & vdata_en & ~video_in_en);       //JFR031128
tranif1 (ram_dq[20],ext_vout_data[4],(COMBO_mode | IDE_mode | PCI_mode ) & vdata_en & ~video_in_en);       //JFR031128
tranif1 (ram_dq[21],ext_vout_data[5],(COMBO_mode | IDE_mode | PCI_mode ) & vdata_en & ~video_in_en);       //JFR031128
tranif1 (ram_dq[22],ext_vout_data[6],(COMBO_mode | IDE_mode | PCI_mode ) & vdata_en & ~video_in_en);       //JFR031128
tranif1 (ram_dq[23],ext_vout_data[7],(COMBO_mode | IDE_mode | PCI_mode ) & vdata_en & ~video_in_en);       //JFR031128

tranif1 (ram_dqm[2],ext_vin_pixel_clk,(COMBO_mode | IDE_mode | PCI_mode ) & vdata_en & video_in_en);
tranif1 (ram_dqm[3],ext_vin_v_syncj  ,(COMBO_mode | IDE_mode | PCI_mode ) & vdata_en & video_in_en);
tranif1 (ram_dq[31],ext_vin_h_syncj  ,(COMBO_mode | IDE_mode | PCI_mode ) & vdata_en & video_in_en);

tranif1 (ram_dqm[2],ext_vout_pixel_clk,(COMBO_mode | IDE_mode | PCI_mode ) & vdata_en & ~video_in_en);
tranif1 (ram_dqm[3],ext_vout_v_syncj  ,(COMBO_mode | IDE_mode | PCI_mode ) & vdata_en & ~video_in_en);
tranif1 (ram_dq[31],ext_vout_h_syncj  ,(COMBO_mode | IDE_mode | PCI_mode ) & vdata_en & ~video_in_en);


//=========================== end ofSDRAM 32bit mode and 16bit mode select ========

`endif
//ram_cs modified for M3357
`ifdef	CS_11BIT
wire	ram_cs1_	=	ram_addr[11];
`else
wire	ram_cs1_	=	1'b1;
`endif
wire	[1:0]	ram_cs_	=	{ram_cs1_,ram_cs0_};

parameter	Local_Parameter_Set_En	= 0;//0--> don't affect MIPS program,
										//1--> affect MIPS to use following parameters
parameter	Clock_Tree_Delay_Chain_Control_Register	= 32'h0000_0101;//SysIO offset:0x64
parameter	Sdram_Timing_Parameter_Register	= 16'h0eeb;//SysIO offset:0x84

reg			LPSE;
reg	[31:0]	CTDCCR;
reg	[15:0]	STPR;
integer		sdram_param_hadle;
initial	begin
	sdram_param_hadle	= $fopen("sdram_ini_param.txt", "w");
	LPSE	=	Local_Parameter_Set_En;
	CTDCCR	=	Clock_Tree_Delay_Chain_Control_Register;
	STPR	=	Sdram_Timing_Parameter_Register;
	$fdisplay(sdram_param_hadle,"%h %h %h %h",{8{LPSE}},{8{LPSE}},{8{LPSE}},{8{LPSE}});
	$fdisplay(sdram_param_hadle,"%h %h %h %h",CTDCCR[7:0],CTDCCR[15:8],CTDCCR[23:16],CTDCCR[31:24]);
	$fdisplay(sdram_param_hadle,"%h %h %h %h",STPR[7:0],STPR[15:8],8'h0,8'h0);
	$fclose(sdram_param_hadle);
end
`ifdef 		WAFER_TEST  //modified for wafer test,tod,2003-03-24
reg	[31:0]	toggle_clk_cnt;
reg			t_clk_cnt_stop;
initial toggle_clk_cnt = 0;
initial t_clk_cnt_stop = 0;
always @(posedge f48_clk)
	if (toggle_clk_cnt != 32'hffff_ffff)
		toggle_clk_cnt <= #1 toggle_clk_cnt + 1;
	else if ((toggle_clk_cnt == 32'hffff_ffff)& ~t_clk_cnt_stop)
		begin
		t_clk_cnt_stop <= #1 1;
		$display("Warning.......The toggle clock counter has been full: %h",toggle_clk_cnt);
		end
/*JFR031120
always @(posedge f48_clk)
	if (toggle_clk_cnt[13:0] == 14'h3fff)
	begin	//report the toggle rate and its count
	$toggle_test_summary;
//	$toggle_count_report;
	$display("The clock counter: %h",toggle_clk_cnt);
	end
	*/
reg		warm_rst_latch1,
		warm_rst_latch2,
		cold_rst_latch_,
		warm_reset_pulse1,
		warm_reset_pulse2;

wire	warm_reset_pulse,
		warm_reset_pulse_tmp;
initial begin
		warm_rst_latch1	= 0;
		warm_rst_latch2	= 0;
		cold_rst_latch_	= 0;
		end

always	@(posedge f48_clk)
		begin
		warm_rst_latch1 <= #1 ~warm_rst_;
		warm_rst_latch2 <= #1 warm_rst_latch1;
		cold_rst_latch_	<= #1 cold_rst_;
		warm_reset_pulse1	<= #1 warm_reset_pulse_tmp;
		warm_reset_pulse2	<= #1 warm_reset_pulse1;
		end

assign	warm_reset_pulse_tmp = warm_rst_latch1 & ~warm_rst_latch2 & cold_rst_latch_;
assign	warm_reset_pulse	= warm_reset_pulse1 & ~warm_reset_pulse2;
`endif

//define for cpu and video bist test
`ifdef	POST_GSIM
wire	warm_reset_	= top.CHIP.CORE.CORE_WARMRST_;
wire	cold_reset_	= top.CHIP.CORE.CORE_COLDRST_;
wire	clk			= top.CHIP.CORE.CPU_CLK;
`else
wire	warm_reset_	= top.CHIP.CORE.core_warmrst_;
wire	cold_reset_	= top.CHIP.CORE.core_coldrst_;
wire	clk			= top.CHIP.CORE.cpu_clk;
`endif

