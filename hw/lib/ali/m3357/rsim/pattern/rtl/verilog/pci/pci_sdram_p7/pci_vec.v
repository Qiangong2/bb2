/***********************************************************************************
*	Description:	test memory write and read with one byte via PCI behavior model,
*			including burst write and read, read line, read multiple,
*			write and invalidate
*	Date:		2001-03-31
*	Author:		Yuky
*	History:	2002-08-12 Runner change random task
***********************************************************************************/
//------Format: 		(address[31:0], startdata[31:0], length[integer])
//------Example:
//	Mem_Read_Multi_Test	(32'h0000_0050, 32'h12345678, 4);
//	Mem_Read_Line_Test	(32'h0000_0070, 32'h0fedcba9, 4);
//	Mem_Write_Inv_Test	(32'h0000_ff00, 32'h11223344, 4);
//	Memory_Burst_Test_Read	(32'h0000_0000, 32'h01234567, 8);
//	Memory_Burst_Test_Write	(32'h0000_0000, 32'h01234567, 8);

//--------------Read/Write SDRAM DIMM0--------------------------
for (temp_data0 = 0; temp_data0[15:0] < 16'hff; temp_data0 = temp_data0 + 1 )
	begin
	temp_data3 	= {$random};
	temp_data2[7:0]= ({$random} % 256);
	if (temp_data2[3:0] == 0)	temp_data2[3:0] = 1;

	temp_data1	= {11'h0, temp_data3[20:2],2'b00};
	$display ("\n\tThe select command order type is %h",temp_data3[7:4]);
	$display ("\tThe startaddr = %h, startdata = %h, length = %h\n",temp_data1,temp_data3, temp_data2[3:0]);

		Memory_Burst_Test_Write	(temp_data1, temp_data3, temp_data2[3:0]);
		repeat (16) @(posedge pci_clk);
		Mem_Read_Multi_Test		(temp_data1, temp_data3, temp_data2[3:0]);
		repeat (16) @(posedge pci_clk);
		Mem_Read_Line_Test		(temp_data1, temp_data3, temp_data2[3:0]);
		repeat (16) @(posedge pci_clk);
		Memory_Burst_Test_Read	(temp_data1, temp_data3, temp_data2[3:0]);


		#1000;
	$display ("\n\tThe select command order type is %h",temp_data3[7:4]);
	$display ("\tThe startaddr = %h, startdata = %h, length = %h\n",temp_data1,~temp_data3, temp_data2[3:0]);
		Mem_Write_Inv_Test		(temp_data1, ~temp_data3, temp_data2[3:0]);
		repeat (16) @(posedge pci_clk);
		Mem_Read_Multi_Test		(temp_data1, ~temp_data3, temp_data2[3:0]);
		repeat (16) @(posedge pci_clk);
		Mem_Read_Line_Test		(temp_data1, ~temp_data3, temp_data2[3:0]);
		repeat (16) @(posedge pci_clk);
		Memory_Burst_Test_Read	(temp_data1, ~temp_data3, temp_data2[3:0]);

		#1000;
	end
//end-----------------------------------------------------------

