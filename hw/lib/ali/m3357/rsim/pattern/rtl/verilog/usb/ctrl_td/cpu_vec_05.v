   #1000;
   $display("\n---- simulating pattern : ctrl_td ----\n");
    #10000;
    top.usb1.usbdev_connect = 1;
    top.usb2.usbdev_connect = 1;
//    top.usb3.usbdev_connect = 1;
//    top.usb4.usbdev_connect = 1;

//     top.usb4.slect_port = 1;

//    top.usb2.Connect(1'b0);

//program_usb(2'b00, 4'h1, 4'h0, 7'h00, 1'b0, 0, 11'h008, 1'b0);

   $display("\n---- init_reg");
//	cpu2pci_cfgwr (8'h04, 5'h2, 4'h0, 3'h0, 32'hffff_ffff);
//	cpu2pci_cfgwr (8'h0c, 5'h2, 4'h0, 3'h0, 32'h0000_f100);
//	cpu2pci_cfgwr (8'h10, 5'h2, 4'h0, 3'h0, 32'h0a00_0000);
//    cpu2pci_cfgwr (8'h40, 5'h2, 4'h0, 3'h0, 32'h0000_0000);
//        cpu2pci_cfgwr (8'h44, 5'h2, 4'h0, 3'h0, 32'h0000_0002);
    CPU2USB_CFGWR (8'h04, 4'h0, 32'hffff_ffff);
    CPU2USB_CFGWR (8'h0c, 4'h0, 32'h0000_f100);
    CPU2USB_CFGWR (8'h10, 4'h0, 32'h0a00_0000);
    CPU2USB_CFGWR (8'h40, 4'h0, 32'h0000_0000);

	#1000;
   $display("\n---- initial HCCA");
	`usb_cpu_write1w   (32'h0a00_0018, 4'hf, 32'h000a_4000);		//HcHCCA
	#1000;

   $display("\n---- initial HCCA Interrupt Table");
   	`usb_cpu_write1w   (32'h000a_4000, 4'hf, 32'h000b_0000);
	#1000;

   $display("\n---- initial HcControlHeadED");
   	`usb_cpu_write1w   (32'h0a00_0020, 4'hf, 32'h000a_4410);		//HcControlHeadED

////////////////////////////////////////////////////////////////////////////////////
   $display("\n---- initial memory: Control ED");
	`usb_cpu_write1w   (32'h000a_4410, 4'hf, {5'h00,11'h008,1'b0,1'b0,1'b0,2'b00,4'h0,7'h00});	//MPS F K S D EN FA
	`usb_cpu_write1w   (32'h000a_4414, 4'hf, {28'h000a_4f2,4'h0});	//TaiP
	`usb_cpu_write1w   (32'h000a_4418, 4'hf, {28'h000a_4f1,2'h0,1'b0,1'b0});	//HeadP ToggleCarry Halted
	`usb_cpu_write1w   (32'h000a_441c, 4'hf, {28'h0000_000,4'h0});	//NextED


   $display("\n---- initial memory: Control TD");
   	`usb_cpu_write1w   (32'h000a_4f10, 4'hf, {4'b1110,2'h0,2'b00,3'h7,2'b00,1'b1,18'h00000});	//CC EC T DI DP R
	`usb_cpu_write1w   (32'h000a_4f14, 4'hf, 32'h000d_0000);	//CBP
	`usb_cpu_write1w   (32'h000a_4f18, 4'hf, {28'h000a_4f2,4'h0});	//NextTD
	`usb_cpu_write1w   (32'h000a_4f1c, 4'hf, 32'h000d_0000);	//BE

   $display("\n---- initial memory: Buffer");
   	`usb_cpu_write1w   (32'h000d_0000, 4'hf, 32'h1234_5678);
   	`usb_cpu_write1w   (32'h000d_0004, 4'hf, 32'h1234_56aa);

/////////////////////////////////////////////////////////////////////////////////////
   $display("\n---- clear SOF int if any");
	`usb_cpu_write1w   (32'h0a00_000c, 4'hf, 32'h0000_0004);

   $display("\n---- enable SOF int");
	`usb_cpu_write1w   (32'h0a00_0010, 4'hf, 32'h8000_0004);

   $display("\n---- set frame interval to a small value for testing purpose");
 	`usb_cpu_write1w   (32'h0a00_0034, 4'hf, 32'h8100_2250);

   $display("\n---- enable USBOperational");
   	`usb_cpu_write1w   (32'h0a00_0004, 4'hf, 32'h0000_0080);

   $display("\n---- turn hub ports on -- port enable, port power on");
   	`usb_cpu_write1w   (32'h0a00_0050, 4'hf, 32'h0001_0000);
   	`usb_cpu_read1w    (32'h0a00_0050, 4'hf, 32'h0000_0000);

   #8000;
   $display("\n---- wait at least 30 bit times");
	`usb_cpu_write1w   (32'h0a00_0054, 4'hf, 32'h0000_0002);	// port1 status
	`usb_cpu_read1w    (32'h0a00_0054, 4'hf, 32'h0001_0103);	// full speed device
	`usb_cpu_write1w   (32'h0a00_0058, 4'hf, 32'h0000_0002);	// port2 status
	`usb_cpu_read1w    (32'h0a00_0058, 4'hf, 32'h0001_0103);	// full speed device
////	`usb_cpu_write1w   (32'h0a00_005c, 4'hf, 32'h0000_0002);	// port1 status
////	`cpu_read1w    (32'h0a00_005c, 4'hf, 32'h0001_0103);	// full speed device
////	`usb_cpu_write1w   (32'h0a00_0060, 4'hf, 32'h0000_0002);	// port2 status
////	`cpu_read1w    (32'h0a00_0060, 4'hf, 32'h0001_0103);	// full speed device
//
   $display("\n---- set ControlListFilled & BulkListFilled");
	`usb_cpu_write1w   (32'h0a00_0008, 4'hf, 32'h0000_0006);	// BLF CLF
	`usb_cpu_read1w    (32'h0a00_0008, 4'hf, 32'h0000_0006);

   $display("\n---- set USBOperational  & Enable ControlList");
	`usb_cpu_write1w   (32'h0a00_0004, 4'hf, 32'h0000_0090);

	temp_usb[0] = 0;
        while (temp_usb[0]==0) begin
		#10000;
		temp = 32'hxxxx_xxxx;
		`usb_cpu_readbk1w   (32'h0a00_000c,4'b0000,temp);
		temp_usb[0] = temp[2];
	end

   #50000;
   $display("\n**** Pattern Completed : ctrl_td !!! ****\n");

