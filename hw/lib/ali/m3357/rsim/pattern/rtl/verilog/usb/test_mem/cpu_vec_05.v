//   #10000;
   $display("\n---- simulating pattern : test ----\n");

    top.usb1.usbdev_connect = 1;
//    top.usb2.usbdev_connect = 1;
//    top.usb1.Connect(1'b0);
//    top.usb2.Connect(1'b0);
//   repeat (30) @(posedge `CLK12);
//  #300;
   $display("\n---- init_reg");
//	cpu2pci_cfgwr (8'h04, 5'h2, 4'h0, 3'h0, 32'hffff_ffff);
//	cpu2pci_cfgwr (8'h0c, 5'h2, 4'h0, 3'h0, 32'h0000_f100);
//	cpu2pci_cfgwr (8'h10, 5'h2, 4'h0, 3'h0, OHCI_BASE + 8'h00);
//  cpu2pci_cfgwr (8'h40, 5'h2, 4'h0, 3'h0, 32'h0000_0000);
    CPU2USB_CFGWR (8'h04, 4'h0, 32'hffff_ffff);
    CPU2USB_CFGWR (8'h0c, 4'h0, 32'h0000_f100);
    CPU2USB_CFGWR (8'h10, 4'h0, OHCI_BASE + 8'h00);
    CPU2USB_CFGWR (8'h40, 4'h0, 32'h0000_0000);

//	#1000;
   $display("\n---- initial HCCA");
	`usb_cpu_write1w   (OHCI_BASE + 8'h18, 4'hf, 32'h003a_4000);		//HcHCCA
//	#1000;

   $display("\n---- initial HCCA Interrupt Table");
   	`usb_cpu_write1w   (32'h003a_4000, 4'hf, 32'h003b_0000);
//	#1000;

   $display("\n---- initial HcControlHeadED");
   	`usb_cpu_write1w   (OHCI_BASE + 8'h20, 4'hf, 32'h003a_4410);		//HcControlHeadED

   $display("\n---- initial HcBulkHeadED");
   	`usb_cpu_write1w   (OHCI_BASE + 8'h28, 4'hf, 32'h003b_8000);		//HcControlHeadED

//    #10000;

   $display("\n---- initial memory: Bulk ED",$time);
	`usb_cpu_write1w   (32'h003b_8000, 4'hf, {5'h00,11'h015,1'b0,1'b0,1'b0,2'b00,4'h0,7'h00});	//MPS F K S D EN FA
	`usb_cpu_write1w   (32'h003b_8004, 4'hf, {28'h003a_4f5,4'h0});	//TaiP
	`usb_cpu_write1w   (32'h003b_8008, 4'hf, {28'h003a_4f1,2'h0,1'b0,1'b0});	//HeadP ToggleCarry Halted
	`usb_cpu_write1w   (32'h003b_800c, 4'hf, {28'h0000_000,4'h0});	//NextED

//program_usb(2'b00, 4'h2, 4'h0, 7'h00, 1'b0, 0, 11'h008, 1'b0);

   $display("\n---- initial memory: Control TD1");
   	`usb_cpu_write1w   (32'h003a_4f10, 4'hf, {4'b1110,2'h0,2'b00,3'h7,2'b10,1'b1,18'h00000});	//CC EC T DI DP R
	`usb_cpu_write1w   (32'h003a_4f14, 4'hf, 32'h003d_0000);	//CBP
	`usb_cpu_write1w   (32'h003a_4f18, 4'hf, {28'h003a_4f2,4'h0});	//NextTD
	`usb_cpu_write1w   (32'h003a_4f1c, 4'hf, 32'h003d_0000);	//BE


   $display("\n---- initial memory: Control TD2");
   	`usb_cpu_write1w   (32'h003a_4f20, 4'hf, {4'b1110,2'h0,2'b00,3'h7,2'b01,1'b1,18'h00000});	//CC EC T DI DP R
	`usb_cpu_write1w   (32'h003a_4f24, 4'hf, 32'h003d_0020);	//CBP
	`usb_cpu_write1w   (32'h003a_4f28, 4'hf, {28'h003a_4f5,4'h0});	//NextTD
	`usb_cpu_write1w   (32'h003a_4f2c, 4'hf, 32'h003d_013f);	//BE

//   $display("\n---- initial memory: Control TD3");
//   	`usb_cpu_write1w   (32'h003a_4f30, 4'hf, {4'b1110,2'h0,2'b00,3'h7,2'b01,1'b1,18'h00000});	//CC EC T DI DP R
//	`usb_cpu_write1w   (32'h003a_4f34, 4'hf, 32'h003d_0028);	//CBP
//	`usb_cpu_write1w   (32'h003a_4f38, 4'hf, {28'h003a_4f5,4'h0});	//NextTD
//	`usb_cpu_write1w   (32'h003a_4f3c, 4'hf, 32'h003d_002f);	//BE

//   $display("\n---- initial memory: Control TD3");
//   	`usb_cpu_write1w   (32'h003a_4f30, 4'hf, {4'b1110,2'h0,2'b00,3'h7,2'b10,1'b1,18'h00000});	//CC EC T DI DP R
//	`usb_cpu_write1w   (32'h003a_4f34, 4'hf, 32'h003d_0040);	//CBP
//	`usb_cpu_write1w   (32'h003a_4f38, 4'hf, {28'h003a_4f5,4'h0});	//NextTD
//	`usb_cpu_write1w   (32'h003a_4f3c, 4'hf, 32'h003d_004f);	//BE
   $display("\n---- initial memory: Buffer");
   for(i=0;i<16'h0120;i=i+16)begin
   	`usb_cpu_write1w   (32'h003d_0020+i, 4'hf, 32'h4433_2211);
   	`usb_cpu_write1w   (32'h003d_0020+i+4, 4'hf, 32'h8877_6655);
   	`usb_cpu_write1w   (32'h003d_0020+i+8, 4'hf, 32'hccbb_aa99);
   	`usb_cpu_write1w   (32'h003d_0020+i+12, 4'hf, 32'h00ff_eedd);
   end

//////////////////////////////////////////////////////////////////////////////////////
   $display("\n---- clear SOF int if any");
	`usb_cpu_write1w   (OHCI_BASE + 8'h0c, 4'hf, 32'h0000_0004);

   $display("\n---- enable SOF int");
	`usb_cpu_write1w   (OHCI_BASE + 8'h10, 4'hf, 32'h8000_0044);

   $display("\n---- set frame interval to a small value for testing purpose");
 	`usb_cpu_write1w   (OHCI_BASE + 8'h34, 4'hf, 32'h8f00_0300);

   $display("\n---- enable USBOperational");
   	`usb_cpu_write1w   (OHCI_BASE + 8'h04, 4'hf, 32'h0000_0080);

   $display("\n---- turn hub ports on -- port enable, port power on");
   	`usb_cpu_write1w   (OHCI_BASE + 8'h50, 4'hf, 32'h0001_0000);
   	`usb_cpu_read1w    (OHCI_BASE + 8'h50, 4'hf, 32'h0000_0000);

   	`usb_cpu_read1w    (OHCI_BASE + 8'h50, 4'hf, 32'h0000_0000);
   	`usb_cpu_read1w    (OHCI_BASE + 8'h50, 4'hf, 32'h0000_0000);
   	`usb_cpu_read1w    (OHCI_BASE + 8'h34, 4'hf, 32'h8f00_0300);
   	`usb_cpu_read1w    (OHCI_BASE + 8'h34, 4'hf, 32'h8f00_0300);
   	`usb_cpu_read1w    (OHCI_BASE + 8'h50, 4'hf, 32'h0000_0000);
   	`usb_cpu_read1w    (OHCI_BASE + 8'h34, 4'hf, 32'h8f00_0300);
//   #2000;
//   repeat (35) @(posedge `CLK12);
   $display("\n---- wait at least 30 bit times");
	`usb_cpu_write1w   (OHCI_BASE + 8'h54, 4'hf, 32'h0000_0002);	// port1 status
	`usb_cpu_read1w    (OHCI_BASE + 8'h54, 4'hf, 32'h0001_0103);	// full speed device
//	`usb_cpu_write1w   (OHCI_BASE + 8'h58, 4'hf, 32'h0000_0002);	// port2 status
//	`usb_cpu_read1w    (OHCI_BASE + 8'h58, 4'hf, 32'h0001_0103);	// full speed device

   $display("\n---- set ControlListFilled & BulkListFilled");
	`usb_cpu_write1w   (OHCI_BASE + 8'h08, 4'hf, 32'h0000_0006);	// BLF CLF
	`usb_cpu_read1w    (OHCI_BASE + 8'h08, 4'hf, 32'h0000_0006);

   $display("\n---- set USBOperational  & Enable ControlList");
	`usb_cpu_write1w   (OHCI_BASE + 8'h04, 4'hf, 32'h0000_00a0);
//
//	temp_usb[0] = 0;
//        while (temp_usb[0]==0) begin
//		#10000;
//		temp = 32'hxxxx_xxxx;
//		`usb_cpu_readbk1w     (OHCI_BASE + 8'h0c,4'b0000,temp);
//		temp_usb[0] = temp[2];
//	end
//	`usb_cpu_write1w   (OHCI_BASE + 8'h0c, 4'hf, 32'hffff_ffff);
//	temp_usb[0] = 0;
//        while (temp_usb[0]==0) begin
//		#10000;
//		temp = 32'hxxxx_xxxx;
//		`usb_cpu_readbk1w     (OHCI_BASE + 8'h0c,4'b0000,temp);
//		temp_usb[0] = temp[2];
//	end
//	`usb_cpu_write1w   (OHCI_BASE + 8'h0c, 4'hf, 32'hffff_ffff);
	temp_usb = 0;
	temp = 0;
        while (temp[31:4]!= 28'h003a_4f5) begin
//		#50;
		temp = 32'hxxxx_xxxx;
		temp_usb = 32'hxxxx_xxxx;
		`usb_cpu_readbk1w     (32'h003b_8008,4'b1111,temp);
		`usb_cpu_readbk1w     (OHCI_BASE + 8'h08,4'b1111,temp_usb);
	end
	`usb_cpu_write1w   (OHCI_BASE + 8'h0c, 4'hf, 32'hffff_ffff);
	#25000;
/*
//#20000;
   $display("\n---- initial memory: Control ED",$time);
	`usb_cpu_write1w   (32'h003b_8000, 4'hf, {5'h00,11'h015,1'b0,1'b0,1'b0,2'b00,4'h0,7'h00});	//MPS F K S D EN FA
	`usb_cpu_write1w   (32'h003b_8004, 4'hf, {28'h003a_4f5,4'h0});	//TaiP
	`usb_cpu_write1w   (32'h003b_8008, 4'hf, {28'h003a_4f1,2'h0,1'b0,1'b0});	//HeadP ToggleCarry Halted
	`usb_cpu_write1w   (32'h003b_800c, 4'hf, {28'h0000_000,4'h0});	//NextED

//program_usb(2'b00, 4'h2, 4'h0, 7'h00, 1'b0, 0, 11'h008, 1'b0);

   $display("\n---- initial memory: Control TD1");
   	`usb_cpu_write1w   (32'h003a_4f10, 4'hf, {4'b1110,2'h0,2'b00,3'h7,2'b10,1'b1,18'h00000});	//CC EC T DI DP R
	`usb_cpu_write1w   (32'h003a_4f14, 4'hf, 32'h003d_0000);	//CBP
	`usb_cpu_write1w   (32'h003a_4f18, 4'hf, {28'h003a_4f2,4'h0});	//NextTD
	`usb_cpu_write1w   (32'h003a_4f1c, 4'hf, 32'h003d_0000);	//BE


   $display("\n---- initial memory: Control TD2");
   	`usb_cpu_write1w   (32'h003a_4f20, 4'hf, {4'b1110,2'h0,2'b00,3'h7,2'b01,1'b1,18'h00000});	//CC EC T DI DP R
	`usb_cpu_write1w   (32'h003a_4f24, 4'hf, 32'h003d_0040);	//CBP
	`usb_cpu_write1w   (32'h003a_4f28, 4'hf, {28'h003a_4f5,4'h0});	//NextTD
	`usb_cpu_write1w   (32'h003a_4f2c, 4'hf, 32'h003d_005f);	//BE

//   $display("\n---- initial memory: Control TD3");
//   	`usb_cpu_write1w   (32'h003a_4f30, 4'hf, {4'b1110,2'h0,2'b00,3'h7,2'b01,1'b1,18'h00000});	//CC EC T DI DP R
//	`usb_cpu_write1w   (32'h003a_4f34, 4'hf, 32'h003d_0028);	//CBP
//	`usb_cpu_write1w   (32'h003a_4f38, 4'hf, {28'h003a_4f5,4'h0});	//NextTD
//	`usb_cpu_write1w   (32'h003a_4f3c, 4'hf, 32'h003d_002f);	//BE

//   $display("\n---- initial memory: Control TD3");
//   	`usb_cpu_write1w   (32'h003a_4f30, 4'hf, {4'b1110,2'h0,2'b00,3'h7,2'b10,1'b1,18'h00000});	//CC EC T DI DP R
//	`usb_cpu_write1w   (32'h003a_4f34, 4'hf, 32'h003d_0040);	//CBP
//	`usb_cpu_write1w   (32'h003a_4f38, 4'hf, {28'h003a_4f5,4'h0});	//NextTD
//	`usb_cpu_write1w   (32'h003a_4f3c, 4'hf, 32'h003d_004f);	//BE
   $display("\n---- initial memory: Buffer");
   for(i=0;i<16'h0020;i=i+16)begin
   	`usb_cpu_write1w   (32'h003d_0040+i, 4'hf, 32'h4232_2212);
   	`usb_cpu_write1w   (32'h003d_0040+i+4, 4'hf, 32'h8272_6252);
   	`usb_cpu_write1w   (32'h003d_0040+i+8, 4'hf, 32'hc2b2_a292);
   	`usb_cpu_write1w   (32'h003d_0040+i+12, 4'hf, 32'h00f2_e2d2);
   end

//////////////////////////////////////////////////////////////////////////////////////
   $display("\n---- enable USBOperational");
   	`usb_cpu_write1w   (OHCI_BASE + 8'h04, 4'hf, 32'h0000_0080);

//   #2000;
//   repeat (35) @(posedge `CLK12);
//   $display("\n---- wait at least 30 bit times");
//	`usb_cpu_write1w   (OHCI_BASE + 8'h54, 4'hf, 32'h0000_0002);	// port1 status
//	`usb_cpu_read1w    (OHCI_BASE + 8'h54, 4'hf, 32'h0001_0103);	// full speed device
//	`usb_cpu_write1w   (OHCI_BASE + 8'h58, 4'hf, 32'h0000_0002);	// port2 status
//	`usb_cpu_read1w    (OHCI_BASE + 8'h58, 4'hf, 32'h0001_0103);	// full speed device

//   $display("\n---- set ControlListFilled & BulkListFilled");
	`usb_cpu_write1w   (OHCI_BASE + 8'h08, 4'hf, 32'h0000_0006);	// BLF CLF
//	`usb_cpu_read1w    (OHCI_BASE + 8'h08, 4'hf, 32'h0000_0006);

   $display("\n---- set USBOperational  & Enable ControlList");
	`usb_cpu_write1w   (OHCI_BASE + 8'h04, 4'hf, 32'h0000_00a0);
//
//	temp_usb[0] = 0;
//        while (temp_usb[0]==0) begin
//		#10000;
//		temp = 32'hxxxx_xxxx;
//		`usb_cpu_readbk1w     (OHCI_BASE + 8'h0c,4'b0000,temp);
//		temp_usb[0] = temp[2];
//	end
//	`usb_cpu_write1w   (OHCI_BASE + 8'h0c, 4'hf, 32'hffff_ffff);
//	temp_usb[0] = 0;
//        while (temp_usb[0]==0) begin
//		#10000;
//		temp = 32'hxxxx_xxxx;
//		`usb_cpu_readbk1w     (OHCI_BASE + 8'h0c,4'b0000,temp);
//		temp_usb[0] = temp[2];
//	end
//	`usb_cpu_write1w   (OHCI_BASE + 8'h0c, 4'hf, 32'hffff_ffff);
	temp_usb = 0;
        while (temp_usb[31:4]!= 28'h003a_4f5) begin
//		#50;
		temp = 32'hxxxx_xxxx;
		`usb_cpu_readbk1w     (32'h003b_8008,4'b0000,temp);
		temp_usb = temp;
	end
	`usb_cpu_write1w   (OHCI_BASE + 8'h0c, 4'hf, 32'hffff_ffff);
//
//#20000;
   $display("\n---- initial memory: Control ED",$time);
	`usb_cpu_write1w   (32'h003b_8000, 4'hf, {5'h00,11'h040,1'b0,1'b0,1'b0,2'b00,4'h0,7'h00});	//MPS F K S D EN FA
	`usb_cpu_write1w   (32'h003b_8004, 4'hf, {28'h003a_4f5,4'h0});	//TaiP
	`usb_cpu_write1w   (32'h003b_8008, 4'hf, {28'h003a_4f1,2'h0,1'b0,1'b0});	//HeadP ToggleCarry Halted
	`usb_cpu_write1w   (32'h003b_800c, 4'hf, {28'h0000_000,4'h0});	//NextED

//program_usb(2'b00, 4'h2, 4'h0, 7'h00, 1'b0, 0, 11'h008, 1'b0);

   $display("\n---- initial memory: Control TD1");
   	`usb_cpu_write1w   (32'h003a_4f10, 4'hf, {4'b1110,2'h0,2'b00,3'h7,2'b10,1'b1,18'h00000});	//CC EC T DI DP R
	`usb_cpu_write1w   (32'h003a_4f14, 4'hf, 32'h003d_0000);	//CBP
	`usb_cpu_write1w   (32'h003a_4f18, 4'hf, {28'h003a_4f2,4'h0});	//NextTD
	`usb_cpu_write1w   (32'h003a_4f1c, 4'hf, 32'h003d_0000);	//BE


   $display("\n---- initial memory: Control TD2");
   	`usb_cpu_write1w   (32'h003a_4f20, 4'hf, {4'b1110,2'h0,2'b00,3'h7,2'b01,1'b1,18'h00000});	//CC EC T DI DP R
	`usb_cpu_write1w   (32'h003a_4f24, 4'hf, 32'h003d_0020);	//CBP
	`usb_cpu_write1w   (32'h003a_4f28, 4'hf, {28'h003a_4f5,4'h0});	//NextTD
	`usb_cpu_write1w   (32'h003a_4f2c, 4'hf, 32'h003d_007f);	//BE

//   $display("\n---- initial memory: Control TD3");
//   	`usb_cpu_write1w   (32'h003a_4f30, 4'hf, {4'b1110,2'h0,2'b00,3'h7,2'b01,1'b1,18'h00000});	//CC EC T DI DP R
//	`usb_cpu_write1w   (32'h003a_4f34, 4'hf, 32'h003d_0028);	//CBP
//	`usb_cpu_write1w   (32'h003a_4f38, 4'hf, {28'h003a_4f5,4'h0});	//NextTD
//	`usb_cpu_write1w   (32'h003a_4f3c, 4'hf, 32'h003d_002f);	//BE

//   $display("\n---- initial memory: Control TD3");
//   	`usb_cpu_write1w   (32'h003a_4f30, 4'hf, {4'b1110,2'h0,2'b00,3'h7,2'b10,1'b1,18'h00000});	//CC EC T DI DP R
//	`usb_cpu_write1w   (32'h003a_4f34, 4'hf, 32'h003d_0040);	//CBP
//	`usb_cpu_write1w   (32'h003a_4f38, 4'hf, {28'h003a_4f5,4'h0});	//NextTD
//	`usb_cpu_write1w   (32'h003a_4f3c, 4'hf, 32'h003d_004f);	//BE
//   $display("\n---- initial memory: Buffer");
   for(i=0;i<16'h0020;i=i+16)begin
   	`usb_cpu_write1w   (32'h003d_0060+i, 4'hf, 32'h4232_2212);
   	`usb_cpu_write1w   (32'h003d_0060+i+4, 4'hf, 32'h8272_6252);
   	`usb_cpu_write1w   (32'h003d_0060+i+8, 4'hf, 32'hc2b2_a292);
   	`usb_cpu_write1w   (32'h003d_0060+i+12, 4'hf, 32'h00f2_e2d2);
   end

//////////////////////////////////////////////////////////////////////////////////////
   $display("\n---- enable USBOperational");
   	`usb_cpu_write1w   (OHCI_BASE + 8'h04, 4'hf, 32'h0000_0080);

//   #2000;
//   repeat (35) @(posedge `CLK12);
//   $display("\n---- wait at least 30 bit times");
//	`usb_cpu_write1w   (OHCI_BASE + 8'h54, 4'hf, 32'h0000_0002);	// port1 status
//	`usb_cpu_read1w    (OHCI_BASE + 8'h54, 4'hf, 32'h0001_0103);	// full speed device
//	`usb_cpu_write1w   (OHCI_BASE + 8'h58, 4'hf, 32'h0000_0002);	// port2 status
//	`usb_cpu_read1w    (OHCI_BASE + 8'h58, 4'hf, 32'h0001_0103);	// full speed device

//   $display("\n---- set ControlListFilled & BulkListFilled");
	`usb_cpu_write1w   (OHCI_BASE + 8'h08, 4'hf, 32'h0000_0006);	// BLF CLF
//	`usb_cpu_read1w    (OHCI_BASE + 8'h08, 4'hf, 32'h0000_0006);

   $display("\n---- set USBOperational  & Enable ControlList");
	`usb_cpu_write1w   (OHCI_BASE + 8'h04, 4'hf, 32'h0000_00a0);
//
//	temp_usb[0] = 0;
//        while (temp_usb[0]==0) begin
//		#10000;
//		temp = 32'hxxxx_xxxx;
//		`usb_cpu_readbk1w     (OHCI_BASE + 8'h0c,4'b0000,temp);
//		temp_usb[0] = temp[2];
//	end
//	`usb_cpu_write1w   (OHCI_BASE + 8'h0c, 4'hf, 32'hffff_ffff);
//	temp_usb[0] = 0;
//        while (temp_usb[0]==0) begin
//		#10000;
//		temp = 32'hxxxx_xxxx;
//		`usb_cpu_readbk1w     (OHCI_BASE + 8'h0c,4'b0000,temp);
//		temp_usb[0] = temp[2];
//	end
//	`usb_cpu_write1w   (OHCI_BASE + 8'h0c, 4'hf, 32'hffff_ffff);
	temp_usb = 0;
        while (temp_usb[31:4]!= 28'h003a_4f5) begin
//		#50;
		temp = 32'hxxxx_xxxx;
		`usb_cpu_readbk1w     (32'h003b_8008,4'b0000,temp);
		temp_usb = temp;
	end
	`usb_cpu_write1w   (OHCI_BASE + 8'h0c, 4'hf, 32'hffff_ffff);
	//
//#20000;
   //service_int   (32'h02900157,32'h0000_0044);

   //service_int   (32'h02900157,32'h0000_0004);

//	temp_usb[0] = 0;
//        while (temp_usb[0]==0) begin
//		#10000;
//		temp = 32'hxxxx_xxxx;
//		`usb_cpu_readbk1w   (OHCI_BASE + 8'h0c,4'b0000,temp);
//		temp_usb[0] = temp[2];
//	end

//   	`usb_cpu_read1w   (32'h003d_0040, 4'hf, 32'h00ff_eedd);
//   	`usb_cpu_read1w   (32'h003d_0044, 4'hf, 32'h00ff_eedd);
//   	`usb_cpu_read1w   (32'h003d_0048, 4'hf, 32'h00ff_eedd);
//   	`usb_cpu_read1w   (32'h003d_004c, 4'hf, 32'h00ff_eedd);
//  #130000;
//   repeat (500) @(posedge `pciclk);
*/
   $display("\n**** Pattern Completed : test_mem !!! ****\n");

