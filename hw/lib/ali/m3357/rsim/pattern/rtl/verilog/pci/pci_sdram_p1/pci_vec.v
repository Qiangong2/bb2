// File : pci_vec.v
// Test Vector
// Snow Yi, 1998-12-17
// Read/Write SDRAM DIMM0
$display ("\n\n\t\t-----------<<<< Memory_Burst_Test>>>>-----------\n\n");
	$display("Memory_Burst_Test_Write: Addr=32'h0000_0054\tLength=11");
	Memory_Burst_Test_Write(32'h0000_0050,32'h0000_0001,24);

	$display("Memory_Burst_Test_Write: Addr=32'h0000_0070\tLength=4");
	Memory_Burst_Test_Write(32'h0000_0100,32'h0000_0010, 4);

	$display ("Memory_Burst_Test_Read: Addr=32'h0000_0054\tLength=4");
	Memory_Burst_Test_Read (32'h0000_0054,32'h0000_0002, 4);

	$display ("Memory_Burst_Test_Read: Addr=32'h0000_0070\tLength=4");
	Memory_Burst_Test_Read(32'h0000_00100,32'h0000_0010, 4);



	$display(" Test PCI Read_Multiple, Read_Line, Write_Invalidate Command");

	Mem_Read_Multi_Test	(32'h0000_0050,32'h0000_0001, 16);
	Mem_Read_Line_Test	(32'h0000_0100,32'h0000_0010, 4);

	Mem_Write_Inv_Test	(32'h0000ff00, 32'h11223344, 4);
	#1000
	Mem_Write_Inv_Test	(32'h0000ff80, 32'h55667788, 4);
	#1000

	Mem_Read_Line_Test	(32'h0000ff00, 32'h11223344, 4);
	Mem_Read_Multi_Test	(32'h0000ff80, 32'h55667788, 4);

