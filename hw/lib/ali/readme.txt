//----------- The directory of simulation environment ---------------
//----------- The directory of simulation environment ---------------

package
	|----m3357
			|	document		// some file about environment
			|	rsrc			//	RTL source code dir
				|model			//	behavior source code
			|	rsim			//	simulation code	dir
				|pattern		// place simulation pattern
					|usb
						|cpu_vec_5.v	// T2 usb access IO space task
				|stimu.c		// simulation dir and cmd parameter
					|usb		// usb source code directory
					|north		// north source code directory
				|stimu.v		// simulation tasks package
					|usb		// usb simulation task
						|util.usb.v
				|stimu.t		// some txt files
			|	lib				//	library dir
			|	work			//	simulation work dir
				|carcy			// user dir
					|rsim		// user work dir

//----------- How to run the simulation environment -----------
1, Place "package" to a unix/linux os computer.
2, Go to the "package/m3357/work/carcy/rsim"
3, Execute "run nu verilog/pci/pci_sdram_p7"  

	the "nu" is northbridge and usb, 
	the "verilog/pci/pci_sdram_p7" is test pattern name, 
	the "s" is download ".vcd" files command. The .vcd or .fsdb file is in current directory


---------------- 1/13/04 addition from Norman Yang -----------------


	Read me file for the USB block instance in the T6304 chip.

The USB block is a PCI interface based USB host controller. The top level USB block
signal contain the PCI interface signal and the USB related special signals.

The T6304 has a PCI host controller, which also have the PCI interface.

The two module's PCI interface are not connected together directly. The output signal
will be combined together by OR controlled by the oe signal.

In the PCI mode, the combined signal will be transferred to control the PAD. The PAD
will connect with other PCI device directly. The PAD input will be connected to each
module's pci input signal.

In the none pci mode, the combined siganl will be connect to each module's pci input
signal directly, but this connection is not showed in the module that instance the
design. It is done in the module named pad_misc.

In the T6304 design, the USB block is instanced in PBIU level. The pci signal combine
is also done in this module. The pci or none pci mode signal connection is in the
pad_misc module.

These two module is attached. Please check it for detailed coding.
