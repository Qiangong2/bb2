#! /bin/csh -f
set design_root = ../../../..
set home_dir	= $design_root/m3358
set user_dir	= $home_dir/user/$USER
set u_rsrc_dir	= $user_dir/rsrc
set u_rsim_dir	= $user_dir/rsim
set u_fpga_rsim	= $user_dir/fpga/rsim
set u_fpga_rsrc	= $user_dir/fpga/rsrc
set u_gsrc_dir	= $user_dir/gsrc
set f_rsim_dir	= $user_dir/fpga/rsim
set rsim_dir	= $home_dir/rsim
set rsrc_dir	= $home_dir/rsrc
set gsrc_dir	= $home_dir/gsrc
set lib_dir		= $home_dir/lib

set fpga_dir 	= $design_root/m3357/fpga
set fpga_rsim	= $fpga_dir/rsim
set fpga_rsrc	= $fpga_dir/rsrc
set fpga_lib	= $fpga_dir/lib
