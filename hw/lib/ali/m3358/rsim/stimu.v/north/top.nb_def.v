`define		INC_CORE	//include CORE
`define     INC_NB		//include Nb
//`define     SDRAM_WRPATH_DEBUG	//debug SDRAM Write Path
//`define     SDRAM_RDPATH_DEBUG	//debug SDRAM Read Path


/******************************************************************************

 User Define File:

*******************************************************************************/
/*=============================================================================
  Fixed terms
 =============================================================================*/
`define	       ON	      1'b1
`define	      OFF	      1'b0
`define	      YES	      1'b1
`define        NO	      1'b0
`define SUPPORTED 	      `YES
`define UNSUPPORTED	       `NO
`define mem_space_indicate    1'b0
`define  io_space_indicate    1'b1
`define locate_32bit_space   2'b00
`define locate_1Mbelow_space 2'b01
`define locate_64bit_space   2'b10
`define fast                 2'b00
`define medium		     2'b01
`define slow                 2'b10
`define NON_INT 8'h00
`define INTA 8'h01
`define INTB 8'h02
`define INTC 8'h03
`define INTD 8'h04
`define	BUS_WIDTH	32  /* PCI AD width */
`define CBE_WIDTH	 4
 /* DEVSEL# Encoded Timing -------------------------------------------*/
`define DEVSEL_TIMING `medium
//`define DEVSEL_TIMING   2'b10
/*++++++++++++++++++++++++++++
   Attribute of Access Target
 +++++++++++++++++++++++++++++*/
`define IOACC       (ut_cmd[3:1]==3'b001)
`define CNFACC      (ut_cmd[3:1]==3'b101)
`define MEMACC      (ut_cmd[2:1]==2'b11 | ut_cmd==4'b1100)
/*++++++++++++++++++
   Target Sequencer
 +++++++++++++++++*/
`define IDLE    2'b00
`define SDATA   2'b01
`define BBUSY   2'b10
`define BACKOFF 2'b11
/*++++++++++++++++++
   Master Sequencer
 +++++++++++++++++*/
`define MIDLE	3'b000
`define DRBUS	3'b001
`define ADDR	3'b011
`define MDATA	3'b111
`define TURNAR	3'b110
`define STAR	3'b101

  /* User Command */
`define tgt_wrcmd 	 ut_cmd[0]
`define tgt_rdcmd 	~ut_cmd[0]
`define mst_wrcmd 	 mstcmd[0]
`define mst_rdcmd 	~mstcmd[0]

/*=============================================================================
  The Followings are User Define Area
 =============================================================================*/
/*=========================== Configration Registers ========================*/
//`define DEVICE_ID	16'hffff
//`define DEVICE_ID	16'h808a
`define DEVICE_ID       16'h5471
//`define VENDOR_ID       16'hffff
// Vender SONY
//`define VENDOR_ID       16'h104D
`define VENDOR_ID       16'h10B9 
`define BASE_CLASSCODE 8'h01                      
`define SUB_CLASSCODE  8'h80                      
`define PRG_INF        8'h00
`define CLASS_CODE {`BASE_CLASSCODE,`SUB_CLASSCODE,`PRG_INF}
`define REVISION_ID    8'h01
//`define HEADER_TYPE    8'b0_0000000               
`define HEADER_TYPE 7'b0000000
`define INTPIN         `INTA               
`define Min_Gnt        8'h00			  
`define Max_Lat        8'h00		
`define SUB_SYSID		16'h8085
`define SUB_VENDERID	16'h104D
`define CAP_PTR			8'hDC
`define CAP_IDENT		8'h01
`define CAP_NEXTITEM	8'h00
`define CAP_PMC			16'h0002
`define	PWM_data		8'h00
`define	PMCSR			8'h00
/*=========== Attribute of Memory Base Address Register =====================*/
`define MEM_BASE_PREFETCH `NO
`define MEM_BASE_TYPE     `locate_32bit_space

  /* Base Address Register 0 for I/O      ------------------------------*/
  /* Base Address Register 1/2 for Memory ------------------------------*/
//`define MAX_BA0        8  // User Address0 Bit Width (I/O) 256Byte
`define MAX_BA0        0  // User Address0 Bit Width (I/O) 256Byte
//`define MAX_BA1       20  // User Address1 Bit Width (MEM) 1MByte
//`define MAX_BA0       10  // User Address1 Bit Width (MEM) 64KByte
//`define MAX_BA1       0  // User Address1 Bit Width (MEM) 64KByte
`define MAX_BA1      10  // User Address1 Bit Width (MEM) 64KByte
`define MAX_BA2       0  // User Address2 Bit Width (MEM) 1MByte
//`define MAX_BA2        0  // User Address2 Bit Width (MEM) 1MByte
`define MAX_BA3        0  // User Address3 Bit Width (MEM) 
`define MAX_BA4        0  // User Address4 Bit Width (MEM) 
`define MAX_BA5        0  // User Address5 Bit Width (MEM) 
`define MAX_ROM       0  // User Address Bit Width (MEM) 64KByte

//1999.09.17
`define badr0_attr        io_base
//`define badr0_attr      mem_base
`define badr1_attr       mem_base
`define badr2_attr       mem_base
`define IO_BASEHIT      ba0_hit

/*=========== Target Memory Access Select Post Write Mode ===================*/
`define POSTWR_MODE	`OFF

`define	BST_LNGTH	6	// 6bit = 0 - 63


