
//==============================================================================
// Description: Top module for NB of m6304 RTL simulation
//              include the chip of m6304, pci behavior model, monitor, sdram
//              behavior model, flash rom behavior model....
// Author:      Yuky
// History:     2002-06-08      initial version, copy from m6303 and modify it for m6304
//              2002-06-17      del the pci master2 and pci ports of chip and add the IDE
//                              ports to the chip
//				2002-07-11		move all the system define and the chip module to top.v
//								make this file only include the nb behavior model and
//								the IBUS protocol monitor
//				2002-11-06		add monitors for mem_intf debug
//				2002-11-08		add monitors for sdram command design requirement debug
//				2003-07-01		modified pci port name from p_xx to pci_xx
//==============================================================================
`ifdef  POST_GSIM
wire	int_bh_rst_   = `path_chipset.RST_   ;
wire	int_bh_clk    = `path_chipset.MEM_CLK;
`else
wire	int_bh_rst_   = `path_chipset.rst_   ;
wire	int_bh_clk    = `path_chipset.mem_clk;
`endif

wire [31:0] tmp_int;
`ifdef new_rom
`else
int_bh int_bh(
	.ext_int_out({tmp_int[31:11],ext_irrx,ext_gpio[9:3],tmp_int[2:0]}),	// output interrupt, default 32'hz
	.clk(int_bh_clk		),
	.rst_(int_bh_rst_	)
);
`endif

monitor p_monitor (
//      .req_           (p_req_         ),
//      .gnt_           (p_gnt_         ),
//      .idsel          (p_idsel        ),
//      .idsel          (p_ad[31:16]    ),
        .ad             (pci_ad           ),
        .cbe_           (pci_cbe_         ),
        .frame_         (moniter_frame_ ),
        .irdy_          (pci_irdy_        ),
        .devsel_        (pci_devsel_      ),
        .trdy_          (pci_trdy_        ),
        .stop_          (pci_stop_        ),
        .lock_          (p_lock_        ),
        .par            (pci_par          ),
        .perr_          (p_perr_        ),
        .serr_          (p_serr_        ),
        .clk            (monitor_clk    ),
        .rst_           (ext_rst_		)	//2002-08-14,rom_control_rst_)//if the rom accessing,pci will be reset
        );

master p_master (       // PCI 33 MHz device
        .gnt_           ( pci_gnta_       ),
        .req_           ( pci_req_        ),
        .ad             ( pci_ad          ),
        .cbe_           ( pci_cbe_        ),
        .par            ( pci_par         ),
        .frame_         ( pci_frame_      ),
        .irdy_          ( pci_irdy_       ),
        .trdy_          ( pci_trdy_       ),
        .stop_          ( pci_stop_       ),
        .lock_          ( p_lock_       ),
        .perr_          ( p_perr_       ),
        .serr_          ( p_serr_       ),
        .devsel_        ( pci_devsel_     ),
        .own_bus        ( bus_0         ),
        .enable         ( 1'b1          ),
        .back2back      ( b2b_0         ),
        .clock          ( p_master_clk  ),
        .rst_           (ext_rst_		)	//2002-08-14,rom_control_rst_)//if the rom accessing,pci will be reset
        );

master p_master2 (      // PCI 33 MHz device
        .gnt_           (pci_gntb_    ),
        .req_           (p_mas2_req_    ),
        .ad             (pci_ad           ),
        .cbe_           (pci_cbe_         ),
        .par            (pci_par          ),
        .frame_         (pci_frame_       ),
        .irdy_          (pci_irdy_        ),
        .trdy_          (pci_trdy_        ),
        .stop_          (pci_stop_        ),
        .lock_          (p_lock_        ),
        .perr_          (p_perr_        ),
        .serr_          (p_serr_        ),
        .devsel_        (pci_devsel_      ),
        .own_bus        (bus_0          ),
        .enable         (1'b1           ),
        .back2back      (b2b_0          ),
        .clock          (p_master_clk   ),
        .rst_           (ext_rst_       )
        );

// turn off p_target for broadon pci tests;

reg p_target_off;
initial p_target_off = 1'b0;

target p_target (       // PCI 33 MHz device
        .ad             (pci_ad           ),
        .cbe_           (pci_cbe_         ),
        .par            (pci_par          ),
        .frame_         (pci_frame_ | p_target_off      ),
        .irdy_          (pci_irdy_        ),
        .trdy_          (pci_trdy_        ),
        .stop_          (pci_stop_        ),
        .lock_          (p_lock_        ),
        .perr_          (p_perr_        ),
        .serr_          (p_serr_        ),
        .devsel_        (pci_devsel_      ),
        //.idsel        (p_ad[24]       ),      // Device 8
        .idsel          (pci_ad[20]       ),      // Device 4 --- Runner
        .pci_int_       (ext_int        ),
        .enable         (1'b1           ),
        .clock          (p_target_clk   ),
        .rst_           (ext_rst_		)	//2002-08-14,rom_control_rst_)//if the rom accessing,pci will be reset
        );
//==================================================================================

//================== invoke the SDRAM behavior model(DIMM) =========================


dimm_bh dimm_bh (
        .dq             (ext_ram_dq     ),
        .addr           (ram_addr		),
        .ba             (ram_ba         ),      // Eric 04/03/00
        .cs_            (ram_cs_ 		),
//        .d1_cke         (1'b1           ),
//        .d2_cke         (1'b1           ),
        .d1_cke         (ext_rst_       ),      // yuky 2001-10-12
        .d2_cke         (ext_rst_       ),      // yuky 2001-10-12
        .dqm            (ram_dqm),
        .ras_           (ram_ras_       ),
        .cas_           (ram_cas_       ),
        .we_            (ram_we_        ),
        .sd_clk0        (sd_clk         ),
        .sd_clk1        (sd_clk         )
        );


//=================== MS/SD behave moudle===================
`ifdef	MSorSD
MS_SD_BH	ms_sd_bh	(
		.sd_clk			(ext_sd_clk			),
		.ms_clk			(ext_ms_clk      	),
		.sd_cmd			(ext_sd_cmd      	),
		.ms_bs			(ext_ms_bs       	),
		.sd_data_0		(ext_sd_data_0   	),
		.sd_data_1		(ext_sd_data_1   	),
		.sd_data_2		(ext_sd_data_2   	),
		.sd_data_3		(ext_sd_data_3   	),
		.ms_sdio		(ext_ms_sdio     	),
		.sd_det      	(ext_sd_det      	),
		.ms_ins      	(ext_ms_ins      	),
		.sd_wr_prct  	(ext_sd_wr_prct  	),
		.ms_pwr_ctrl 	(ext_ms_pwr_ctrl 	),
		.rstinj			(int_bh_rst_		)
				);
`else
`endif
`ifdef	BW_DEBUG
	bandwidth_debug	bandwidth_debug();
`else
`endif
//74373 behave module
`ifdef	POST_GSIM
wire	[1:0]	flash_mode_sel	= top.CHIP.CORE.T2_CHIPSET.EEPROM.FLASH_MODE;
`else
wire	[1:0]	flash_mode_sel	= top.CHIP.flash_mode;
`endif
wire	[7:0]	ext_eeprom_addr_373_ale;	// 8	addres input from 3357
wire	[7:0]	ext_eeprom_addr_373_in;		// 8	address output from 373 to external flash
wire	[15:0]	flash_data;					// 16	boot_room data
wire	[21:0]	flash_addr;					//	21	boot_room address
wire	flash_mode00	=	flash_mode_sel == 2'b00	;// 8bit mode
wire	flash_mode01	=	flash_mode_sel == 2'b01	;// 16bit mode with 373
wire	flash_mode23	=	flash_mode_sel == 2'b10	|
							flash_mode_sel == 2'b11	;// 8bit mode with A/D mux

bh_373 bh_373(
		.rom_ale			(ext_eeprom_ale			),		// 1	Flash rom external 373 latch enable
		.rom_addr			(ext_eeprom_addr_373_in	),		// 8	addres input from 3357
		.eeprom_addr_ale	(ext_eeprom_addr_373_ale)		// 8	address output from 373 to external flash
);

////	The valid ALE width need to be at least 4 clock cycle
////	pragma property	ALE_WIDTH_4 =
////	always	{Rom_en & !ext_eeprom_ale; Rom_en & ext_eeprom_ale} |=>
////			{ext_eeprom_ale; ext_eeprom_ale; ext_eeprom_ale}
////		@ (posedge sd_clk);
////	assert ALE_WIDTH_4;

//	The ROM WE# and OE# will never active at the same time
//	pragma property	NO_OE_WE =
//	always	(!(ext_eeprom_oe_ === 0 & ext_eeprom_we_ === 0))
//		@ (posedge sd_clk);
//	assert NO_OE_WE;

//	We ROM is disabled, there will be no active ROM_OE# signal
//	pragma property	NO_EN_NO_OE =
//	always	(!(!Rom_en & !ext_eeprom_oe_))
//		@ (posedge sd_clk);
//	assert NO_EN_NO_OE;

// BOOT_ROM
wire 	   xce;
assign #50 xce = int_bh_rst_ ? 1'b0 : 1'b1;
`ifdef new_rom
MBM29F160E boot_rom (
		.DQ    		(flash_data			),
        .A    		(flash_addr[21:1]	),
        .XOE     	(ext_eeprom_oe_     ),
        .XCE     	(xce     			),
        .XWE     	(ext_eeprom_we_     ),
        .XRESET 	(int_bh_rst_ 		),// Hard Reset Input
  		.XBYTE  	(flash_mode01		),// Byte/Word Configuration Input
  		.RY_XBY 	(					),// Ready/Busy Output
  		.XWP		(1'b1				)
        );
`else
boot_rom boot_rom (
		.eeprom_data    	(flash_data			),
        .eeprom_addr    	(flash_addr			),
        .eeprom_oe_     	(ext_eeprom_oe_     ),
        .eeprom_cs_     	(1'b0     			),
        .eeprom_we_     	(ext_eeprom_we_     ),
        .eeprom_16bit_sel	(flash_mode01		)
        );
`endif

//flash mode 00 and 01, 3357 data connect to flash data
 tranif1	(ext_eeprom_data[0],	flash_data[0],	flash_mode00 |flash_mode01 	);
 tranif1	(ext_eeprom_data[1],	flash_data[1],	flash_mode00 |flash_mode01	);
 tranif1	(ext_eeprom_data[2],	flash_data[2],	flash_mode00 |flash_mode01	);
 tranif1	(ext_eeprom_data[3],	flash_data[3],	flash_mode00 |flash_mode01	);
 tranif1	(ext_eeprom_data[4],	flash_data[4],	flash_mode00 |flash_mode01	);
 tranif1	(ext_eeprom_data[5],	flash_data[5],	flash_mode00 |flash_mode01	);
 tranif1	(ext_eeprom_data[6],	flash_data[6],	flash_mode00 |flash_mode01	);
 tranif1	(ext_eeprom_data[7],	flash_data[7],	flash_mode00 |flash_mode01	);

//flash mode 00, 3357 addr[7:0] connect to flash addr [7:0]
`ifdef new_rom
tranif1	(ext_eeprom_addr[0]		,	flash_data[15]	,		flash_mode00 );
`else
 tranif1	(ext_eeprom_addr[0]		,	flash_addr[0]	,		flash_mode00);
 `endif
 tranif1	(ext_eeprom_addr[1]		,	flash_addr[1]	,		flash_mode00);
 tranif1	(ext_eeprom_addr[2]		,	flash_addr[2]	,		flash_mode00);
 tranif1	(ext_eeprom_addr[3]		,	flash_addr[3]	,		flash_mode00);
 tranif1	(ext_eeprom_addr[4]		,	flash_addr[4]	,		flash_mode00);
 tranif1	(ext_eeprom_addr[5]		,	flash_addr[5]	,		flash_mode00);
 tranif1	(ext_eeprom_addr[6]		,	flash_addr[6]	,		flash_mode00);
 tranif1	(ext_eeprom_addr[7]		,	flash_addr[7]	,		flash_mode00);

//all flash mode, 3357 addr[20:8] connect to flash addr[20:8]
 tranif1	(ext_eeprom_addr[8]		,	flash_addr[8]	,		1'b1 );
 tranif1	(ext_eeprom_addr[9]		,	flash_addr[9]	,		1'b1 );
 tranif1	(ext_eeprom_addr[10]	,	flash_addr[10]	,		1'b1 );
 tranif1	(ext_eeprom_addr[11]	,	flash_addr[11]	,		1'b1 );
 tranif1	(ext_eeprom_addr[12]	,	flash_addr[12]	,		1'b1 );
 tranif1	(ext_eeprom_addr[13]	,	flash_addr[13]	,		1'b1 );
 tranif1	(ext_eeprom_addr[14]	,	flash_addr[14]	,		1'b1 );
 tranif1	(ext_eeprom_addr[15]	,	flash_addr[15]	,		1'b1 );
 tranif1	(ext_eeprom_addr[16]	,	flash_addr[16]	,		1'b1 );
 tranif1	(ext_eeprom_addr[17]	,	flash_addr[17]	,		1'b1 );
 tranif1	(ext_eeprom_addr[18]	,	flash_addr[18]	,		1'b1 );
 tranif1	(ext_eeprom_addr[19]	,	flash_addr[19]	,		1'b1 );
 tranif1	(ext_eeprom_addr[20]	,	flash_addr[20]	,		1'b1 );
 tranif1	(ext_eeprom_addr[21]	,	flash_addr[21]	,		1'b1 );

//flash mode 01, 23, 3357 addr[7:0] connect to 373 addr [7:0]
 tranif1	(ext_eeprom_addr[0]		,	ext_eeprom_addr_373_in[0]	,	flash_mode01 | flash_mode23);
 tranif1	(ext_eeprom_addr[1]		,	ext_eeprom_addr_373_in[1]	,	flash_mode01 | flash_mode23);
 tranif1	(ext_eeprom_addr[2]		,	ext_eeprom_addr_373_in[2]	,	flash_mode01 | flash_mode23);
 tranif1	(ext_eeprom_addr[3]		,	ext_eeprom_addr_373_in[3]	,	flash_mode01 | flash_mode23);
 tranif1	(ext_eeprom_addr[4]		,	ext_eeprom_addr_373_in[4]	,	flash_mode01 | flash_mode23);
 tranif1	(ext_eeprom_addr[5]		,	ext_eeprom_addr_373_in[5]	,	flash_mode01 | flash_mode23);
 tranif1	(ext_eeprom_addr[6]		,	ext_eeprom_addr_373_in[6]	,	flash_mode01 | flash_mode23);
 tranif1	(ext_eeprom_addr[7]		,	ext_eeprom_addr_373_in[7]	,	flash_mode01 | flash_mode23);

 //flash mode 01, 23, 373 addr[7:0] connect to flash addr [7:0]
 `ifdef new_rom
 tranif1	(ext_eeprom_addr_373_ale[0]	,	flash_data[15]	,	flash_mode23 );
 tranif1	(ext_eeprom_addr_373_ale[0] ,   flash_addr[0]	,   flash_mode01 );
 `else
 tranif1	(ext_eeprom_addr_373_ale[0]	,	flash_addr[0]	,	flash_mode01 | flash_mode23);
 `endif
 tranif1	(ext_eeprom_addr_373_ale[1]	,	flash_addr[1]	,	flash_mode01 | flash_mode23);
 tranif1	(ext_eeprom_addr_373_ale[2]	,	flash_addr[2]	,	flash_mode01 | flash_mode23);
 tranif1	(ext_eeprom_addr_373_ale[3]	,	flash_addr[3]	,	flash_mode01 | flash_mode23);
 tranif1	(ext_eeprom_addr_373_ale[4]	,	flash_addr[4]	,	flash_mode01 | flash_mode23);
 tranif1	(ext_eeprom_addr_373_ale[5]	,	flash_addr[5]	,	flash_mode01 | flash_mode23);
 tranif1	(ext_eeprom_addr_373_ale[6]	,	flash_addr[6]	,	flash_mode01 | flash_mode23);
 tranif1	(ext_eeprom_addr_373_ale[7]	,	flash_addr[7]	,	flash_mode01 | flash_mode23);

//flash mode 01, 3357 addr[7:0] connect to flash data [15:8]
 tranif1	(ext_eeprom_addr[0],	flash_data[8],		flash_mode01 	);
 tranif1	(ext_eeprom_addr[1],	flash_data[9],		flash_mode01	);
 tranif1	(ext_eeprom_addr[2],	flash_data[10],		flash_mode01	);
 tranif1	(ext_eeprom_addr[3],	flash_data[11],		flash_mode01	);
 tranif1	(ext_eeprom_addr[4],	flash_data[12],		flash_mode01	);
 tranif1	(ext_eeprom_addr[5],	flash_data[13],		flash_mode01	);
 tranif1	(ext_eeprom_addr[6],	flash_data[14],		flash_mode01	);
 tranif1	(ext_eeprom_addr[7],	flash_data[15],		flash_mode01	);

//flash mode 23, 3357 addr[7:0] connect to flash data
 tranif1	(ext_eeprom_addr[0],	flash_data[0],		flash_mode23 	);
 tranif1	(ext_eeprom_addr[1],	flash_data[1],		flash_mode23	);
 tranif1	(ext_eeprom_addr[2],	flash_data[2],		flash_mode23	);
 tranif1	(ext_eeprom_addr[3],	flash_data[3],		flash_mode23	);
 tranif1	(ext_eeprom_addr[4],	flash_data[4],		flash_mode23	);
 tranif1	(ext_eeprom_addr[5],	flash_data[5],		flash_mode23	);
 tranif1	(ext_eeprom_addr[6],	flash_data[6],		flash_mode23	);
 tranif1	(ext_eeprom_addr[7],	flash_data[7],		flash_mode23	);

//==============================================================================


//`ifdef GSIM
//`else
// Flash ROM fastest timing config
//defparam `path_chipset.p_biu.pci_reg.rom_cycle_width = 4'h0;
//defparam `path_chipset.p_biu.pci_reg.wrst_length = 6'h10;
//defparam `path_chip.pad_rst_strap.clock_gen_rst_length = 10'h1ff;
//defparam `path_chip.pad_rst_strap.warm_rst_length = 9'h1e0;     // only 16 clock of F48M
//`endif

