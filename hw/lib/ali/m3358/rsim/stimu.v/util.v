/**************************************************************
//	File: util.v
//	Description: 	for m3357 project. Mem access tasks
//	History:	 2002-06-10 initial version yuky
//			 2002-06-18 add the tasks for cpu access sdram memory, Figo
***************************************************************/
/**************************************
task list:
	// special purpose
	ResetChip
	Initialize
	POST_initial
	PCI_pre_fetch
	Set_pos_write_timer
	STRAP_Pin_Info //JFR030919

SDRAM iterface
	SDRAM_Wait_Idle
	SDRAM_Mode_Set
	SDRAM_PrechargeAll
	SDRAM_Refresh
	SDRAM_Init
	SDRAM_Set_Arbt
	SDRAM_Ref_Disable

	SDRAM_Set_Refresh_Timer
	SDRAM_Set_DIMM_Map
	SDRAM_Set_Row_Base
	SDRAM_Set_TimeParam
	SDRAM_Set_ClkDly
	DIMM_Set_Map_Mode
	SDRAM_PREOE_CTRL
	SDRAM_POSTOE_CTRL
	SDRAM_R2W_CTRL
	SDRAM_2TCMD_CTRL

Special
	SDRAM_load_memory
	SDRAM_dump_memory
	SDRAM_dump_array

function list:
	SDRAM_ram_ma_dec

CPU utiltities
	ROM_Initialize
	flash_test_load_data
	load_sdram_param
	SDRAM_Initialize
	SDRAM_Initialize1
	CTRL_RAMInitialize

	CPU_Burst_WR_SDRAM
	CPU_Burst_RD_SDRAM

******************************************/

task ResetChip;
begin
	cold_rst_ = 1;
   	$display("\nTask:Reset SOC chip >> Start Reset");

    	#10 			cold_rst_ = 0;
	#(2000*cyclef48_clk)	cold_rst_ = 1;//for wafer test (20MHz clock, 50*2000=0.1ms)
   	$display("\nTask:Reset SOC chip >> Finished Reset!!!");
`ifdef	POST_GSIM
   	wait(top.CHIP.CORE.T2_CHIPSET.RST_);
`else
   	wait(top.CHIP.chipset_rst_);//For new coding style Jeffdrey030531
`endif
end
endtask // reset_M3357

`ifdef	INC_CORE //all the tasks should writed under this condition compiler

task Initialize;
begin

	$display("\n\nEnter task Initialize in util.v");
	cpu2pci_cfgwr(8'h04,CHIPSET_IDSEL,4'hE,CHIPSET_FUNC,32'h7);
	cpu2pci_cfgrd(8'h04,CHIPSET_IDSEL,4'hE,CHIPSET_FUNC,32'h6);
end
endtask

task	POST_initial;
begin
   $display("\nPOST initial CHIP....");
   cpu2pci_cfgwr(8'h3c,CHIPSET_IDSEL,4'hE,CHIPSET_FUNC,32'hffffff0d);       // IRQ vector
// config_write(8'h00000004,CHIPSET_IDSEL,4'hE,CHIPSET_FUNC,32'hffffff05);       // Enable IO space and PCI master
   cpu2pci_cfgwr(8'h04,CHIPSET_IDSEL,4'hE,CHIPSET_FUNC,32'hffffff07);       // Enable IO space and Mem space

   $display("\nPOST initial CHIP Done !!!!");
end
endtask

task PCI_pre_fetch;
input	able;
begin
	cpu2pci_cfgrdbk (8'h90,CHIPSET_IDSEL,4'h0,CHIPSET_FUNC,temp);
	temp[0]=able;
	cpu2pci_cfgwr (8'h90,CHIPSET_IDSEL,4'h0,CHIPSET_FUNC,temp);
end
endtask

task Set_pos_write_timer;
input	[7:0]	timer;
begin
	cpu2pci_cfgwr (8'h90,CHIPSET_IDSEL,4'b1011,CHIPSET_FUNC,{8'h0, timer, 16'h0});
end
endtask

task STRAP_Pin_Info;

reg	[5:0]	cpu_clk_pll_m;
reg	[11:5]	mem_clk_pll_m;
reg	[14:12]	work_mode;
reg			pll_bypass;
reg			cpu_prob_en;
reg	[18:17]	flash_mode;
//reg	[18:17]	pci_fs;
reg	[31:19]	refer_info;
reg	[3:0]	pci_clk_fs;
begin
//get strp pin information
`ifdef POST_GSIM
cpu_clk_pll_m	=	top.CHIP.PAD_RST_STRAP.CPU_PLL_M;
mem_clk_pll_m	=	top.CHIP.PAD_RST_STRAP.MEM_PLL_M;
work_mode		=	top.CHIP.PAD_RST_STRAP.STRAP_PIN_IN[11:9];
pll_bypass      =   top.CHIP.PAD_RST_STRAP.PLL_BYPASS;
cpu_prob_en     =	top.CHIP.PAD_RST_STRAP.CPU_PROBE_EN;
//pci_fs          =   top.CHIP.PAD_RST_STRAP.PCI_T2RISC_FS;
flash_mode      =   top.CHIP.PAD_RST_STRAP.STRAP_PIN_IN[8:7];
refer_info      =   top.CHIP.PAD_RST_STRAP.STRAP_PIN_IN[31:19];
`else
cpu_clk_pll_m	=	top.CHIP.PAD_RST_STRAP.cpu_pll_m;
mem_clk_pll_m	=	top.CHIP.PAD_RST_STRAP.mem_pll_m;
work_mode		=	top.CHIP.PAD_RST_STRAP.work_mode_config;
pll_bypass      =   top.CHIP.PAD_RST_STRAP.pll_bypass;
cpu_prob_en     =	top.CHIP.PAD_RST_STRAP.cpu_probe_en;
//pci_fs          =   top.CHIP.PAD_RST_STRAP.pci_t2risc_fs;
flash_mode      =   top.CHIP.PAD_RST_STRAP.flash_mode_config;
refer_info      =   top.CHIP.PAD_RST_STRAP.reference_info_config;

`endif
//cpu_clk_pll_m	=	temp_data7[5:0];
//mem_clk_pll_m	=	temp_data7[11:6];
//work_mode		=	temp_data7[14:12];
//pll_bypass		=	temp_data7[15];
//cpu_prob_en		=	temp_data7[16];
//pci_fs			=	temp_data7[18:17];

 $display ("\nSTRAP PIN INFORMATION");
 $display ("CPU_clk frequency is %f MHz", (4.5*cpu_clk_pll_m));
 $display ("MEM_clk frequency is %f MHz", (4.5*mem_clk_pll_m));
// $display ("PCI_clk frequency is %f MHz", (4.5*cpu_clk_pll_m/pci_clk_fs));

case(work_mode)
	3'b000:	$display ("Current WORK_MODE is 	COMBO_MODE");
	3'b001:	$display ("Current WORK_MODE is 	IDE_MODE");
	3'b010:	$display ("Current WORK_MODE is 	SERVO_SRAM_MODE");
	3'b011:	$display ("Current WORK_MODE is 	EV_ENC_TEST_MODE");
	3'b100:	$display ("Current WORK_MODE is 	SERVO_ONLY_MODE");
	3'b101:	$display ("Current WORK_MODE is 	SERVO_TEST_MODE");
	3'b110:	$display ("Current WORK_MODE is 	SERVO_SRAM_MODE");
	3'b111:	$display ("Current WORK_MODE is 	SERVO_SRAM_MODE");
endcase

case(flash_mode)
	2'b00:	$display ("Current FLASH_INTERFACE_MODE is 	8 BIT MODE");
	2'b01:	$display ("Current FLASH_INTERFACE_MODE is 	16 BIT MODE");
	2'b10:	$display ("Current FLASH_INTERFACE_MODE is 	8 BIT MODE WITH D/A MUX");
	2'b11:	$display ("Current FLASH_INTERFACE_MODE is 	8 BIT MODE WITH D/A MUX");
endcase
 $display ("PLL_BYPASS is 	%D ",pll_bypass);
 $display ("CPU_PROB_EN is 	%D ",cpu_prob_en);
end
endtask

//============================================SDRAM interface
task	SDRAM_Init;
begin
	SDRAM_Ref_Disable;
	DIMM_Set_Map_Mode;
	SDRAM_Set_TimeParam;
	SDRAM_NOP;
	SDRAM_NOP;
	SDRAM_NOP;
	SDRAM_PrechargeAll;
	SDRAM_Mode_Set;
	SDRAM_Refresh;SDRAM_Refresh;SDRAM_Refresh;SDRAM_Refresh;
	SDRAM_Refresh;SDRAM_Refresh;SDRAM_Refresh;SDRAM_Refresh;
	SDRAM_Wait_Idle;
end
endtask

`ifdef NEW_MEM
task	SDRAM_Set_Arbt;
input[5:0]	conti_req_setting	;
input		suba_req_prio		;
input		cpu_req_prio		;
input		suba_arbt_mode		;
input		subb_arbt_mode		;
input		main_arbt_mode		;

reg[5:0]	conti_tmp	;
reg		suba_pri_tmp	;
reg		cpu_pri_tmp	;
reg		suba_tmp;
reg		subb_tmp;
reg		main_tmp;
reg[31:0]	mode_rdbk_tmp;
begin
	conti_tmp		=	conti_req_setting	;
	suba_pri_tmp		=	suba_req_prio		;
	cpu_pri_tmp		=   	cpu_req_prio		;
	suba_tmp		=	suba_arbt_mode		;
	subb_tmp		=	subb_arbt_mode		;
	main_tmp		=	main_arbt_mode		;
	mode_rdbk_tmp	=	32'hxxxx_xxxx;
	$display("\n");
	SYS_IO_Write(8'h88,4'b0111,{8'h0,{2'h0,conti_tmp},{5'h0,suba_pri_tmp,cpu_pri_tmp,main_tmp,
					5'h0,subb_tmp,1'b0,suba_tmp}});
	SYS_IO_Readbk(8'h88,4'b0111,mode_rdbk_tmp);
	if((mode_rdbk_tmp[21:16]==conti_tmp)&&(mode_rdbk_tmp[0] == suba_tmp))begin
		$display("\nSDRAM Arbiter Priority Mode has been set successfully!");
		if (cpu_pri_tmp)
		begin
			$display("SDRAM CPU Priority  is set as:HIGHEST");
		end
		if (suba_pri_tmp)
		begin
			$display("SDRAM suba Priority  is set as:HIGHEST");
		end
		case(main_tmp)
		1'b0:	begin
			$display("SDRAM Main Arbiter Priority Mode is set as:");
			$display("Rotate: Display Engine -> CPU -> servo -> SubA-Arbt -> SubB-Arbt");
		end
		1'b1:	begin
			$display("SDRAM Main Arbiter Priority Mode is set as:");
			$display("Fixed: Display Engine SubB-Arbt -> servo -> cpu ->SubA-Arbt");
		end
		endcase
		case(suba_tmp)
		1'b0:	begin
			$display("SDRAM SubA-Arbiter Priority Mode is set as:");
			$display("Rotate: Video -> DSP -> PCI");
		end
		1'b1:	begin
			$display("SDRAM SubA-Arbiter Priority Mode is set as:");
			$display("Fixed:>> PCI >> DSP>>Video  ");
		end
		endcase
		case(subb_tmp)
		1'b0:	begin
			$display("SDRAM SubB-Arbiter Priority Mode is set as:");
			$display("Rotate: IDE -> VSB ->AUD");
		end
		1'b1:	begin
			$display("SDRAM SubB-Arbiter Priority Mode is set as:");
			$display("Rotate: AUD>>  VSB >>IDE");
		end
		endcase
		$display("\n");
	end
	else	begin
		$display("\nError_T2_rsim:\tDuring setting SDRAM Arbiter Priority Mode");
		$display("Expected Main-Arbt Mode = %b\tActual Main-Arbt Mode = %b\n",conti_tmp,mode_rdbk_tmp[21:16]);
		$display("Expected Main-Arbt Mode = %b\tActual Main-Arbt Mode = %b\n",main_tmp,mode_rdbk_tmp[8]);
		$display("Expected SubA-Arbt Mode = %b\tActual SubA-Arbt Mode = %b\n",suba_tmp,mode_rdbk_tmp[0]);
		$display("Expected SubB-Arbt Mode = %b\tActual SubB-Arbt Mode = %b\n",subb_tmp,mode_rdbk_tmp[2]);
	end
end

endtask
`else
task	SDRAM_Set_Arbt;
input[1:0]	sub_arbt_mode;
input[1:0]	subb_arbt_mode;
input[1:0]	main_arbt_mode;
reg[1:0]	sub_tmp;
reg[1:0]	subb_tmp;
reg[1:0]	main_tmp;
reg[31:0]	mode_rdbk_tmp;
begin
	sub_tmp			=	sub_arbt_mode;
	subb_tmp		=	subb_arbt_mode;
	main_tmp		=	main_arbt_mode;
	mode_rdbk_tmp	=	32'hxxxx_xxxx;
	$display("\n");
	SYS_IO_Write(8'h88,4'b0011,{16'h0,{6'h0,main_tmp},{4'h0,subb_tmp,sub_tmp}});
	SYS_IO_Readbk(8'h88,4'b0011,mode_rdbk_tmp);
	if((mode_rdbk_tmp[1:0] == sub_tmp) && (mode_rdbk_tmp[9:8] == main_tmp))begin
		$display("\nSDRAM Arbiter Priority Mode has been set successfully!");
		case(main_tmp)
		2'b00:	begin
			$display("SDRAM Main Arbiter Priority Mode is set as:");
			$display("Rotate: Display Engine -> CPU -> SubA-Arbt -> SubB-Arbt");
		end
		2'b01:	begin
			$display("SDRAM Main Arbiter Priority Mode is set as:");
			$display("Fixed: Display Engine >> CPU >> SubA-Arbt >> SubB-Arbt");
		end
		2'b10:	begin
			$display("SDRAM Main Arbiter Priority Mode is set as:");
			$display("Fixed: Display Engine >> SubA-Arbt >> SubB-Arbt >> CPU");
		end
		2'b11:	begin
			$display("SDRAM Main Arbiter Priority Mode is set as:");
			$display("Fixed: Display Engine >> SubB-Arbt >> CPU >> SubA-Arbt");
		end
		endcase
		case(sub_tmp)
		2'b00:	begin
			$display("SDRAM SubA-Arbiter Priority Mode is set as:");
			$display("Rotate: GPU -> Video -> DSP -> PCI");
		end
		2'b01:	begin
			$display("SDRAM SubA-Arbiter Priority Mode is set as:");
			$display("Fixed: GPU >> Video >> DSP >> PCI");
		end
		2'b10:	begin
			$display("SDRAM SubA-Arbiter Priority Mode is set as:");
			$display("Fixed: DSP >> GPU >> Video >> PCI");
		end
		2'b11:	begin
			$display("SDRAM SubA-Arbiter Priority Mode is set as:");
			$display("Fixed:  PCI >> GPU >> Video >> DSP");
		end
		endcase
		case(subb_tmp)
		2'b00:	begin
			$display("SDRAM SubB-Arbiter Priority Mode is set as:");
			$display("Rotate: IDE -> SB -> VSB");
		end
		2'b01:	begin
			$display("SDRAM SubB-Arbiter Priority Mode is set as:");
			$display("Rotate: IDE >> SB >> VSB");
		end
		2'b10:	begin
			$display("SDRAM SubB-Arbiter Priority Mode is set as:");
			$display("Rotate: VSB >> IDE >> SB");
		end
		2'b11:	begin
			$display("SDRAM SubB-Arbiter Priority Mode is set as:");
			$display("Rotate: IDE >> VSB >> SB");
		end
		endcase
		$display("\n");
	end
	else	begin
		$display("\nError_T2_rsim:\tDuring setting SDRAM Arbiter Priority Mode");
		$display("Expected Conti_Req Set  = %b\tActual Conti_Req Set  = %b",main_tmp,mode_rdbk_tmp[9:8]);
		$display("Expected SubA-Arbt Mode = %b\tActual SubA-Arbt Mode = %b\n",sub_tmp,mode_rdbk_tmp[1:0]);
		$display("Expected SubB-Arbt Mode = %b\tActual SubB-Arbt Mode = %b\n",subb_tmp,mode_rdbk_tmp[3:2]);
	end
end
endtask
`endif

task	SDRAM_Wait_Idle;
begin
	$display("*** Waiting SDRAM idle ......");	// SN19981031
	flag = 1;
	//#1000 flag = 0;
	while (flag)	begin
		temp_mem = 32'hxxxx_xxxx;
		//nec_rdbk_single(32'h1800_0004, 4'b0001, temp_mem);
//		cpu2pci_cfgrdbk(8'h54,CHIPSET_IDSEL,4'b1110,CHIPSET_FUNC,temp_mem);
		cpu2mem_cfgrdbk(4'h0,4'b1110,temp_mem);
		if (temp_mem[2:0] === 3'h0) #40 flag = 0;
		else	#200 flag = 1;
	end
end
endtask

task	SDRAM_Mode_Set;
begin
	SDRAM_Wait_Idle;
	$display ("*** Setting SDRAM mode ......");	// SN19981031
//	cpu2pci_cfgwr(8'h58,CHIPSET_IDSEL,4'b1011,CHIPSET_FUNC,{12'h0, SDRAM_Interleave, 19'h0});
//	cpu2pci_cfgwr(8'h54,CHIPSET_IDSEL,4'b1110,CHIPSET_FUNC,32'h1);
//	cpu2mem_cfgwr(4'h4,4'b1011,{12'h0,SDRAM_Interleave,SDRAM_Burst_Length,16'h0});
	cpu2mem_cfgwr(4'h4,4'b1011,
					{8'h0,
					1'b0,{2'b01,SDRAM_Cas_Latency},SDRAM_Interleave,SDRAM_Burst_Length,
					16'h0});
	cpu2mem_cfgwr(4'h0,4'b1110,32'h1);
end
endtask

task	SDRAM_NOP;
begin
	SDRAM_Wait_Idle;
	$display ("*** Send NOP Command to SDRAMs ......");
	cpu2mem_cfgwr(4'h0,4'b1110,32'h0);
end
endtask

task	SDRAM_PrechargeAll;
begin
	SDRAM_Wait_Idle;
	$display ("*** Precharging All Banks of SDRAMs ......");// SN19981031
//	cpu2pci_cfgwr(8'h54,CHIPSET_IDSEL,4'b1110,CHIPSET_FUNC,32'h2);
	cpu2mem_cfgwr(4'h0,4'b1110,32'h2);
end
endtask

task	SDRAM_Refresh;
begin
	SDRAM_Wait_Idle;
	$display ("*** SDRAM auto refreshing ......");	// SN19981031
	//cpu2pci_cfgwr(8'h54,CHIPSET_IDSEL,4'b1110,CHIPSET_FUNC,32'h3);
	cpu2mem_cfgwr(4'h0,4'b1110,32'h3);

end
endtask

task	SDRAM_Ref_Disable;
begin
	SDRAM_Wait_Idle;
	$display ("*** SDRAM Disable Auto-Refresh Timer......");
	SDRAM_Set_Refresh_Timer(8'h0);
	SDRAM_Wait_Idle;
end
endtask

task	SDRAM_Set_Refresh_Timer;
input	[7:0]	ref_init;
begin
	$display ("*** SDRAM set refresh timer......");	// SN19981031
//	cpu2pci_cfgwr(8'h54,CHIPSET_IDSEL,4'b1101,CHIPSET_FUNC,{16'h0, ref_init, 8'h0});
	cpu2mem_cfgwr(4'h0,4'b1101,{16'h0,ref_init,8'h0});
end
endtask

task	SDRAM_Set_DIMM_Map;
input		dimm1_active;
input		dimm_banks;
input[3:0]	map_mode;
input		dimm1_banks;
input[3:0]	map1_mode;

begin
	$display("Set SDRAM Address Mapping Split Mode:");
	if(dimm1_active)begin
		$display("External SDRAM DIMM1 is actived");
	end
	else	begin
		$display("External SDRAM DIMM1 is unactived");
	end
	if(dimm_banks)begin
		$display("The SDRAM DIMM0 is four banks");
	end
	else	begin
		$display("The SDRAM DIMM0 is two banks");
	end
	case(map_mode[1:0])
	2'b00:	begin
		$display("The SDRAM DIMM0 Column Address Width is 8-bits");
	end
	2'b01:	begin
		$display("The SDRAM DIMM0 Column Address Width is 9-bits");
	end
	2'b10:	begin
		$display("The SDRAM DIMM0 Column Address Width is 10-bits");
	end
	default:	begin
		$display("T2_rsim_Error...............");
	end
	endcase
	case(map_mode[3:2])
	2'b00:	begin
		$display("The SDRAM DIMM0 Row Address Width is 11-bits");
	end
	2'b01:	begin
		$display("The SDRAM DIMM0 Row Address Width is 12-bits");
	end
	default:	begin
		$display("T2_rsim_Error...............");
	end
	endcase
	if(dimm1_active)begin
		if(dimm1_banks)begin
			$display("The SDRAM DIMM1 is four banks");
		end
		else	begin
			$display("The SDRAM DIMM1 is two banks");
		end
		case(map1_mode[1:0])
		2'b00:	begin
			$display("The SDRAM DIMM1 Column Address Width is 8-bits");
		end
		2'b01:	begin
			$display("The SDRAM DIMM1 Column Address Width is 9-bits");
		end
		2'b10:	begin
			$display("The SDRAM DIMM1 Column Address Width is 10-bits");
		end
		default:	begin
			$display("T2_rsim_Error...............");
		end
		endcase
		case(map1_mode[3:2])
		2'b00:	begin
			$display("The SDRAM DIMM1 Row Address Width is 11-bits");
		end
		2'b01:	begin
			$display("The SDRAM DIMM1 Row Address Width is 12-bits");
		end
		default:	begin
			$display("T2_rsim_Error...............");
		end
		endcase
	end
	`ifdef NEW_MEM
	cpu2mem_cfgwr(4'h0,4'b0011,{SDRAM_TurnAround_Mode,SDRAM_OE_Post_En,SDRAM_OE_Pre_En,SDRAM_DQ_Mode,dimm1_active,1'b0,dimm1_banks,dimm_banks,
								map1_mode[3:0],map_mode[3:0],16'h0});
	`else
	cpu2mem_cfgwr(4'h0,4'b0011,{3'b0,SDRAM_DQ_Mode,dimm1_active,1'b0,dimm1_banks,dimm_banks,
								map1_mode[3:0],map_mode[3:0],16'h0});
	`endif
end
endtask

task	SDRAM_Set_Row_Base;
input	[1:0]	row_no;
input	[7:0]	row_base;
begin
case (row_no)
	2'b00:	cpu2pci_cfgwr(8'h5c,CHIPSET_IDSEL,4'b1110,CHIPSET_FUNC,{24'h0, row_base});
	2'b01:	cpu2pci_cfgwr(8'h5c,CHIPSET_IDSEL,4'b1101,CHIPSET_FUNC,{16'h0, row_base, 8'h0});
	2'b10:	cpu2pci_cfgwr(8'h5c,CHIPSET_IDSEL,4'b1011,CHIPSET_FUNC,{8'h0, row_base, 16'h0});
	2'b11:	cpu2pci_cfgwr(8'h5c,CHIPSET_IDSEL,4'b0111,CHIPSET_FUNC,{row_base, 24'h0});
endcase
end
endtask
`ifdef 	NEW_MEM
task	SDRAM_Set_TimeParam;
begin

	$display ("*** SDRAM set timing parameter......");	// SN19981031
/*
	cpu2pci_cfgwr(8'h58,CHIPSET_IDSEL,4'b1110,CHIPSET_FUNC,
		{23'h0,
		SDRAM_Ras_CylTime,	// 3 bit
		SDRAM_Ras_PreTime,	// 2 bit
		SDRAM_RtoC_dly,		// 2 bit
		1'b1,SDRAM_Cas_Latency});

*/
/*
	cpu2mem_cfgwr(4'h4,4'b1100,
		{
		20'h0,
		SDRAM_Ras_CylTime,
		1'b0,SDRAM_RtoR_dly,
		SDRAM_Ras_PreTime,		//2bits
		SDRAM_RtoC_dly,			//2bits
		1'b1,SDRAM_Cas_Latency});
*/
	cpu2mem_cfgwr(4'h4,4'b1100,
		{16'h0,
		SDRAM_Ras_ActTime,SDRAM_Ras_RefCyc,
		SDRAM_CtoC_dly,SDRAM_RtoR_dly,2'b00,
		SDRAM_RtoC_dly,SDRAM_Ras_PreTime
		});
end
endtask

`else
task	SDRAM_Set_TimeParam;
begin

	$display ("*** SDRAM set timing parameter......");	// SN19981031
/*
	cpu2pci_cfgwr(8'h58,CHIPSET_IDSEL,4'b1110,CHIPSET_FUNC,
		{23'h0,
		SDRAM_Ras_CylTime,	// 3 bit
		SDRAM_Ras_PreTime,	// 2 bit
		SDRAM_RtoC_dly,		// 2 bit
		1'b1,SDRAM_Cas_Latency});

*/
/*
	cpu2mem_cfgwr(4'h4,4'b1100,
		{
		20'h0,
		SDRAM_Ras_CylTime,
		1'b0,SDRAM_RtoR_dly,
		SDRAM_Ras_PreTime,		//2bits
		SDRAM_RtoC_dly,			//2bits
		1'b1,SDRAM_Cas_Latency});
*/
	cpu2mem_cfgwr(4'h4,4'b1100,
		{16'h0,
		SDRAM_Ras_ActTime,SDRAM_Ras_RefCyc,
		SDRAM_CtoC_dly,SDRAM_RtoR_dly,2'b00,
		SDRAM_RtoC_dly,SDRAM_Ras_PreTime
		});
end
endtask
`endif
task	SDRAM_Set_ClkDly;
begin
	$display ("*** SDRAM set Clock Tree Delay parameter......");
	`cpu_write1w(CPU_IOBase + 32'h64, 4'hf, {16'h0, 8'h10, 8'h0+SDRAM_Clk_Tree_Dly});
end
endtask

task	DIMM_Set_Map_Mode;
reg			dimm0_banks;
reg[3:0]	dimm0_map;
reg			dimm1_en;
reg			dimm1_banks;
reg[3:0]	dimm1_map;

begin

`ifdef sdram_d0_16m_4b	// 12/10
	sdram_row0_map		=	3'h6;	// 4 banks, 10 bit column addr
	sdram_row1_base		=	32'h0400_0000;
	if(SDRAM_DQ_Mode)begin
		SDRAM_DIMM1_BASE	=	32'h0200_0000;
	end
	else	begin
		SDRAM_DIMM1_BASE	=	32'h0400_0000;
	end
	dimm0_banks		 	=	1'b1;
	dimm0_map			=	4'b0110;

//	SDRAM_Set_DIMM_Map(0, sdram_row0_map[2], {2'h1, sdram_row0_map[1:0]});
`else
`ifdef sdram_d0_8m_4b	// 12/9
	sdram_row0_map		=	3'h5;	// 4 banks, 9 bit column addr
	sdram_row1_base		=	32'h0200_0000;
	if(SDRAM_DQ_Mode)begin
		SDRAM_DIMM1_BASE	=	32'h0100_0000;
	end
	else	begin
		SDRAM_DIMM1_BASE	=	32'h0200_0000;
	end
	dimm0_banks		 	=	1'b1;
	dimm0_map			=	4'b0101;

//	SDRAM_Set_DIMM_Map(0, sdram_row0_map[2], {2'h1, sdram_row0_map[1:0]});
`else
`ifdef sdram_d0_4m_4b	// 12/8
	sdram_row0_map		=	3'h4;	// 4 banks, 8 bit column addr
	sdram_row1_base		=	32'h0100_0000;
	if(SDRAM_DQ_Mode)begin
		SDRAM_DIMM1_BASE	=	32'h0080_0000;
	end
	else	begin
		SDRAM_DIMM1_BASE	=	32'h0100_0000;
	end
	dimm0_banks		 	=	1'b1;
	dimm0_map			=	4'b0100;

//	SDRAM_Set_DIMM_Map(0, sdram_row0_map[2], {2'h1, sdram_row0_map[1:0]});
`else
	sdram_row0_map		=	3'h0;	// 2 banks, 8 bit column addr
	sdram_row1_base		=	32'h0040_0000;
	if(SDRAM_DQ_Mode)begin
		SDRAM_DIMM1_BASE	=	32'h0020_0000;
	end
	else	begin
		SDRAM_DIMM1_BASE	=	32'h0040_0000;
	end
	dimm0_banks		 	=	1'b0;
	dimm0_map			=	4'b0000;

//	SDRAM_Set_DIMM_Map(0, sdram_row0_map[2], {2'h0, sdram_row0_map[1:0]});
`endif
`endif
`endif

`ifdef sdram_d1_16m_4b	// 12/10
	dimm1_en			=	1'b1;
	sdram_row1_map		=	3'h6;	// 4 banks, 10 bit column addr
	dimm1_banks		 	=	1'b1;
	dimm1_map			=	4'b0110;

`else
`ifdef sdram_d1_8m_4b	// 12/9
	dimm1_en			=	1'b1;
	sdram_row1_map		=	3'h5;	// 4 banks, 9 bit column addr
	dimm1_banks		 	=	1'b1;
	dimm1_map			=	4'b0101;

`else
`ifdef sdram_d1_4m_4b	// 12/8
	dimm1_en			=	1'b1;
	sdram_row1_map		=	3'h4;	// 4 banks, 8 bit column addr
	dimm1_banks		 	=	1'b1;
	dimm1_map			=	4'b0100;

`else
`ifdef sdram_d1_2m_2b
	dimm1_en			=	1'b1;
	sdram_row1_map		=	3'h0;	// 2 banks, 8 bit column addr
	dimm1_banks		 	=	1'b0;
	dimm1_map			=	4'b0000;
`else
	dimm1_en			=	1'b0;
	sdram_row1_map		=	3'h0;	// 2 banks, 8 bit column addr
	dimm1_banks		 	=	1'b0;
	dimm1_map			=	4'b0000;
`endif	//`ifdef sdram_d1_2m_2b
`endif	//`ifdef sdram_d1_4m_4b	// 12/8
`endif	//`ifdef sdram_d1_8m_4b	// 12/9
`endif	//`ifdef sdram_d1_16m_4b	// 12/10

`ifdef	NEW_MEM_SET
	sdram_row0_map		=	SDRAM_Row_Map	;			// 2 banks, 8 bit column addr
	sdram_row1_base		=	SDRAM_Row1_Base	;
	dimm0_banks	=	SDRAM_Dimm_Bank_Map	;
	dimm0_map	=	SDRAM_Dimm_Map		;
	dimm1_en	=	SDRAM_Dimm1_En		;


//	SDRAM_Set_DIMM_Map(SDRAM_Dimm1_En,SDRAM_Dimm_Bank_Map,SDRAM_Dimm_Map,dimm1_banks,dimm1_map);
`else
`endif
	SDRAM_Set_DIMM_Map(dimm1_en,dimm0_banks,dimm0_map,dimm1_banks,dimm1_map);

end
endtask

//	---------------------------------------------------------------------------------
//pre-oe control:
task	SDRAM_PREOE_CTRL;
input	pre_oe_ctrl;	//0->Disable, 1->Enable
reg[31:0]	rdback_tmp;

begin
	rdback_tmp	=	32'hxxxx_xxxx;
	cpu2mem_cfgrdbk(4'h0,4'b0000,rdback_tmp);
	cpu2mem_cfgwr(4'h0,4'b0111,{rdback_tmp[31:30],pre_oe_ctrl,rdback_tmp[28:0]});
end
endtask

//post-oe control:
task	SDRAM_POSTOE_CTRL;
input	post_oe_ctrl;	//0->Disable, 1->Enable
reg[31:0]	rdback_tmp;

begin
	rdback_tmp	=	32'hxxxx_xxxx;
	cpu2mem_cfgrdbk(4'h0,4'b0000,rdback_tmp);
	cpu2mem_cfgwr(4'h0,4'b0111,{rdback_tmp[31],post_oe_ctrl,rdback_tmp[29:0]});
end
endtask

//read-to-write turn-around control:

task	SDRAM_R2W_CTRL;
input	ram_r2w_ctrl;	//0->2 Turn-Around Cycles, 1->1 Turn-Around Cycle
reg[31:0]	rdback_tmp;

begin
	rdback_tmp	=	32'hxxxx_xxxx;
	cpu2mem_cfgrdbk(4'h0,4'b0000,rdback_tmp);
	cpu2mem_cfgwr(4'h0,4'b0111,{ram_r2w_ctrl,rdback_tmp[30:0]});
end
endtask


//2T command cycle control:
task	SDRAM_2TCMD_CTRL;
input	set_2tcmd;	//0->Disable, 1->Enable
reg[31:0]	rdback_tmp;

begin
	rdback_tmp	=	32'hxxxx_xxxx;
	cpu2mem_cfgrdbk(4'h4,4'b0000,rdback_tmp);
	cpu2mem_cfgwr(4'h4,4'b1110,{rdback_tmp[31:8],set_2tcmd,rdback_tmp[6:0]});
end
endtask

//	---------------------------------------------------------------------------------
task	SDRAM_load_memory;
input	mem_base;
input	mem_size;
//input	mem_index;
integer	mem_size, mem_base;
reg	[31:0]	mem_dw_temp;
begin
	$display("task:SDRAM_load_memory>\tSize 'h%d to address 'h%h loading...",mem_size << 2,mem_base);
	for (curn_offset = 0; curn_offset < mem_size; curn_offset = curn_offset + 1)
	begin
		mem_dw_temp = mem_array[curn_offset];
		// convert address
		//curn_addr = (curn_offset << 2) + mem_base;
		/*
		curn_addr = (curn_offset << 2) + mem_base;
		if (curn_addr[29:2] < sdram_row1_base)begin
			if(SDRAM_DQ_Mode)begin
			curn_addr 		=	((curn_offset << 2) + mem_base)*2;
			end
	//	if (curn_addr < sdram_row1_base)	begin	// at row 0 SDRAM   JOYOUS
			sdram_ram0_haddr 		= SDRAM_ram_ma_dec(curn_addr[29:2], sdram_row0_map);*/
	if(SDRAM_DQ_Mode)begin
			curn_addr	= ((curn_offset << 2) + mem_base)*2;
		end
		else	begin
			curn_addr = (curn_offset << 2) + mem_base;
		end
		if (curn_addr < sdram_row1_base)	begin	// at row 0 SDRAM
			//sys_ram0_addr = curn_addr;
//			sdram_ram0_haddr_next	= SDRAM_ram_ma_dec(curn_addr[29:2], sdram_row0_map);
//			sdram_ram0_haddr		=	SDRAM_Dimm_Bank_Map	?	sdram_ram0_haddr_next
//										:	{1'b0, sdram_ram0_haddr_next[12:0]}	;
		sdram_ram0_haddr = SDRAM_ram_ma_dec(curn_addr[29:2], sdram_row0_map);
				// wait ram_ma_dec module calculate the real SDRAM address
			//sdram_real_addr = {sdram_ram0_haddr[11:0], curn_addr[9:2]};
	`ifdef	NEW_MEM
			case(sdram_row0_map[2:0])
			3'b000:	begin
			`ifdef sdram_d0_16m_4b
				sdram_real_addr = {{1'b0,sdram_ram0_haddr[10:0]},{2'b0,curn_addr[9:2]}};
			`else
				sdram_real_addr = {sdram_ram0_haddr[10:0], curn_addr[9:2]};
			`endif
			end
			3'b001:	begin
			`ifdef sdram_d0_16m_4b
				sdram_real_addr = {{1'b0,sdram_ram0_haddr[10:0]},{1'b0,curn_addr[10:2]}};
			`else
				sdram_real_addr = {sdram_ram0_haddr[10:0], curn_addr[10:2]};
			`endif
			end
			3'b100:	begin
			`ifdef sdram_d0_16m_4b
				sdram_real_addr = {sdram_ram0_haddr[11:0],{2'b0,curn_addr[9:2]}};
			`else
				sdram_real_addr = {sdram_ram0_haddr[11:0], curn_addr[9:2]};
			`endif
			end
			3'b101:	begin
			`ifdef sdram_d0_16m_4b
				sdram_real_addr = {sdram_ram0_haddr[11:0],{1'b0,curn_addr[10:2]}};
			`else
				sdram_real_addr = {sdram_ram0_haddr[11:0], curn_addr[10:2]};
			`endif
			end
			3'b110:	begin
				sdram_real_addr = {sdram_ram0_haddr[11:0], curn_addr[11:2]};
			end
			default:	begin
				sdram_real_addr = {sdram_ram0_haddr[11:0], curn_addr[11:2]};
			end
			endcase
	`else
			case(sdram_row0_map[1:0])
			2'b00:	begin
				sdram_real_addr 		= {sdram_ram0_haddr[11:0], curn_addr[9:2]};
			end
			2'b01:	begin
				sdram_real_addr = {sdram_ram0_haddr[11:0], curn_addr[10:2]};
			end
			default:	begin
				sdram_real_addr = {sdram_ram0_haddr[11:0], curn_addr[11:2]};
			end
			endcase
	`endif
	`ifdef DISP_LOAD
	$display("task:SDRAM0 curnset =%h ,curn addr= %h ,r_row =%h ,r_col= %h ,bk= %h ",curn_offset,curn_addr[29:2],sdram_ram0_haddr[11:0],curn_addr[9:2],sdram_ram0_haddr[13:12]);//joyous
	`else
	`endif
			case (sdram_ram0_haddr[13:12])
	`ifdef	sdram_d0_16m_4b
			2'b00:	begin
				dimm_bh.dimm0.sdram0.ram_0[sdram_real_addr] = mem_dw_temp[7:0];
				dimm_bh.dimm0.sdram1.ram_0[sdram_real_addr] = mem_dw_temp[15:8];
				if(SDRAM_DQ_Mode)begin
					dimm_bh.dimm0.sdram0.ram_0[sdram_real_addr+1] = mem_dw_temp[23:16];
					dimm_bh.dimm0.sdram1.ram_0[sdram_real_addr+1] = mem_dw_temp[31:24];
				end
				else	begin
					dimm_bh.dimm0.sdram2.ram_0[sdram_real_addr] = mem_dw_temp[23:16];
					dimm_bh.dimm0.sdram3.ram_0[sdram_real_addr] = mem_dw_temp[31:24];
				end
//$display("SDRAM_load curnset =%h ,curn addr= %h ,real_addr= %h",curn_offset,curn_addr[29:2],sdram_real_addr);//joyous
			end
			2'b01:	begin
				dimm_bh.dimm0.sdram0.ram_1[sdram_real_addr] = mem_dw_temp[7:0];
				dimm_bh.dimm0.sdram1.ram_1[sdram_real_addr] = mem_dw_temp[15:8];
				if(SDRAM_DQ_Mode)begin
					dimm_bh.dimm0.sdram0.ram_1[sdram_real_addr+1] = mem_dw_temp[23:16];
					dimm_bh.dimm0.sdram1.ram_1[sdram_real_addr+1] = mem_dw_temp[31:24];
				end
				else	begin
					dimm_bh.dimm0.sdram2.ram_1[sdram_real_addr] = mem_dw_temp[23:16];
					dimm_bh.dimm0.sdram3.ram_1[sdram_real_addr] = mem_dw_temp[31:24];
				end
			end
	`else
	`ifdef	sdram_d0_8m_4b
			2'b00:	begin
				dimm_bh.dimm0.sdram0.ram_0[sdram_real_addr] = mem_dw_temp[7:0];
				dimm_bh.dimm0.sdram1.ram_0[sdram_real_addr] = mem_dw_temp[15:8];
				if(SDRAM_DQ_Mode)begin
					dimm_bh.dimm0.sdram0.ram_0[sdram_real_addr+1] = mem_dw_temp[23:16];
					dimm_bh.dimm0.sdram1.ram_0[sdram_real_addr+1] = mem_dw_temp[31:24];
				end
				else	begin
					dimm_bh.dimm0.sdram2.ram_0[sdram_real_addr] = mem_dw_temp[23:16];
					dimm_bh.dimm0.sdram3.ram_0[sdram_real_addr] = mem_dw_temp[31:24];
				end
			end
			2'b01:	begin
				dimm_bh.dimm0.sdram0.ram_1[sdram_real_addr] = mem_dw_temp[7:0];
				dimm_bh.dimm0.sdram1.ram_1[sdram_real_addr] = mem_dw_temp[15:8];
				if(SDRAM_DQ_Mode)begin
					dimm_bh.dimm0.sdram0.ram_1[sdram_real_addr+1] = mem_dw_temp[23:16];
					dimm_bh.dimm0.sdram1.ram_1[sdram_real_addr+1] = mem_dw_temp[31:24];
				end
				else	begin
					dimm_bh.dimm0.sdram2.ram_1[sdram_real_addr] = mem_dw_temp[23:16];
					dimm_bh.dimm0.sdram3.ram_1[sdram_real_addr] = mem_dw_temp[31:24];
				end
			end
	`else
			2'b00:	begin
				dimm_bh.dimm0.sdram0.ram_0[sdram_real_addr] = mem_dw_temp[15:0] ;
				if(SDRAM_DQ_Mode)begin
					dimm_bh.dimm0.sdram0.ram_0[sdram_real_addr+1] = mem_dw_temp[31:16];
				end
				else	begin
					dimm_bh.dimm0.sdram1.ram_0[sdram_real_addr] = mem_dw_temp[31:16];
				end
			end
			2'b01:	begin
				dimm_bh.dimm0.sdram0.ram_1[sdram_real_addr] = mem_dw_temp[15:0] ;
				if(SDRAM_DQ_Mode)begin
					dimm_bh.dimm0.sdram0.ram_1[sdram_real_addr+1] = mem_dw_temp[31:16];
				end
				else	begin
					dimm_bh.dimm0.sdram1.ram_1[sdram_real_addr] = mem_dw_temp[31:16];
				end
			end
	`endif
	`endif
//	---------------------------------------------------------------------------
	`ifdef	sdram_d0_16m_4b
			2'b10:	begin
				dimm_bh.dimm0.sdram0.ram_2[sdram_real_addr] = mem_dw_temp[7:0];
				dimm_bh.dimm0.sdram1.ram_2[sdram_real_addr] = mem_dw_temp[15:8];
				if(SDRAM_DQ_Mode)begin
					dimm_bh.dimm0.sdram0.ram_2[sdram_real_addr+1] = mem_dw_temp[23:16];
					dimm_bh.dimm0.sdram1.ram_2[sdram_real_addr+1] = mem_dw_temp[31:24];
				end
				else	begin
					dimm_bh.dimm0.sdram2.ram_2[sdram_real_addr] = mem_dw_temp[23:16];
					dimm_bh.dimm0.sdram3.ram_2[sdram_real_addr] = mem_dw_temp[31:24];
				end
			end
			2'b11:	begin
				dimm_bh.dimm0.sdram0.ram_3[sdram_real_addr] = mem_dw_temp[7:0];
				dimm_bh.dimm0.sdram1.ram_3[sdram_real_addr] = mem_dw_temp[15:8];
				if(SDRAM_DQ_Mode)begin
					dimm_bh.dimm0.sdram0.ram_3[sdram_real_addr+1] = mem_dw_temp[23:16];
					dimm_bh.dimm0.sdram1.ram_3[sdram_real_addr+1] = mem_dw_temp[31:24];
				end
				else	begin
					dimm_bh.dimm0.sdram2.ram_3[sdram_real_addr] = mem_dw_temp[23:16];
					dimm_bh.dimm0.sdram3.ram_3[sdram_real_addr] = mem_dw_temp[31:24];
				end
			end
	`else
	`ifdef	sdram_d0_8m_4b
			2'b10:	begin
				dimm_bh.dimm0.sdram0.ram_2[sdram_real_addr] = mem_dw_temp[7:0];
				dimm_bh.dimm0.sdram1.ram_2[sdram_real_addr] = mem_dw_temp[15:8];
				if(SDRAM_DQ_Mode)begin
					dimm_bh.dimm0.sdram0.ram_2[sdram_real_addr+1] = mem_dw_temp[23:16];
					dimm_bh.dimm0.sdram1.ram_2[sdram_real_addr+1] = mem_dw_temp[31:24];
				end
				else	begin
					dimm_bh.dimm0.sdram2.ram_2[sdram_real_addr] = mem_dw_temp[23:16];
					dimm_bh.dimm0.sdram3.ram_2[sdram_real_addr] = mem_dw_temp[31:24];
				end
			end
			2'b11:	begin
				dimm_bh.dimm0.sdram0.ram_3[sdram_real_addr] = mem_dw_temp[7:0];
				dimm_bh.dimm0.sdram1.ram_3[sdram_real_addr] = mem_dw_temp[15:8];
				if(SDRAM_DQ_Mode)begin
					dimm_bh.dimm0.sdram0.ram_3[sdram_real_addr+1] = mem_dw_temp[23:16];
					dimm_bh.dimm0.sdram1.ram_3[sdram_real_addr+1] = mem_dw_temp[31:24];
				end
				else	begin
					dimm_bh.dimm0.sdram2.ram_3[sdram_real_addr] = mem_dw_temp[23:16];
					dimm_bh.dimm0.sdram3.ram_3[sdram_real_addr] = mem_dw_temp[31:24];
				end
			end
	`else
	`ifdef	sdram_d0_4m_4b
			2'b10:	begin
				dimm_bh.dimm0.sdram0.ram_2[sdram_real_addr] = mem_dw_temp[15:0] ;
				if(SDRAM_DQ_Mode)begin
					dimm_bh.dimm0.sdram0.ram_2[sdram_real_addr+1] = mem_dw_temp[31:16];
				end
				else	begin
					dimm_bh.dimm0.sdram1.ram_2[sdram_real_addr] = mem_dw_temp[31:16];
				end
			end
			2'b11:	begin
				dimm_bh.dimm0.sdram0.ram_3[sdram_real_addr] = mem_dw_temp[15:0] ;
				if(SDRAM_DQ_Mode)begin
					dimm_bh.dimm0.sdram0.ram_3[sdram_real_addr+1] = mem_dw_temp[31:16];
				end
				else	begin
					dimm_bh.dimm0.sdram1.ram_3[sdram_real_addr] = mem_dw_temp[31:16];
				end
			end
	`endif
	`endif
	`endif
			endcase
		end
		else	begin	// at row 1 SDRAM
//		sdram_ram1_haddr_next	= SDRAM_ram_ma_dec(curn_addr[29:2], sdram_row0_map);
//		sdram_ram1_haddr		= SDRAM_Dimm_Bank_Map	?	sdram_ram0_haddr_next
//										:	{1'b0, sdram_ram0_haddr_next[12:0]}	;
		sdram_ram1_haddr = SDRAM_ram_ma_dec(curn_addr[29:2], sdram_row0_map);
//			sdram_real_addr = {sdram_ram1_haddr[11:0], curn_addr[9:2]};
		`ifdef	NEW_MEM
			case(sdram_row0_map[2:0])
			3'b000:	begin
			`ifdef sdram_d1_16m_4b
				sdram_real_addr = {{1'b0,sdram_ram1_haddr[10:0]},{2'b0,curn_addr[9:2]}};
			`else
				sdram_real_addr = {sdram_ram1_haddr[10:0], curn_addr[9:2]};
			`endif
			end
			3'b001:	begin
			`ifdef sdram_d1_16m_4b
				sdram_real_addr = {{1'b0,sdram_ram1_haddr[10:0]},{1'b0,curn_addr[10:2]}};
			`else
				sdram_real_addr = {sdram_ram1_haddr[10:0], curn_addr[10:2]};
			`endif
			end
			3'b100:	begin
			`ifdef sdram_d1_16m_4b
				sdram_real_addr = {sdram_ram1_haddr[11:0],{2'b0,curn_addr[9:2]}};
			`else
				sdram_real_addr = {sdram_ram1_haddr[11:0], curn_addr[9:2]};
			`endif
			end
			3'b101:	begin
			`ifdef sdram_d1_16m_4b
				sdram_real_addr = {sdram_ram1_haddr[11:0],{1'b0,curn_addr[10:2]}};
			`else
				sdram_real_addr = {sdram_ram1_haddr[11:0], curn_addr[10:2]};
			`endif
			end
			3'b110:	begin
				sdram_real_addr = {sdram_ram1_haddr[11:0], curn_addr[11:2]};
			end
			default:	begin
				sdram_real_addr = {sdram_ram1_haddr[11:0], curn_addr[11:2]};
			end
			endcase
		`else
			case(sdram_row1_map[1:0])
			2'b00:	begin
				sdram_real_addr = {sdram_ram1_haddr[11:0], curn_addr[9:2]};
			end
			2'b01:	begin
				sdram_real_addr = {sdram_ram1_haddr[11:0], curn_addr[10:2]};
			end
			default:	begin
				sdram_real_addr = {sdram_ram1_haddr[11:0], curn_addr[11:2]};
			end
			endcase
		`endif
`ifdef	DISP_LOAD
$display("task:SDRAM1 curnset =%h ,curn addr= %h ,r_row =%h ,r_col= %h ,bk= %h ",curn_offset,curn_addr[29:2],sdram_ram1_haddr[11:0],curn_addr[9:2],sdram_ram1_haddr[13:12]);//joyous
`else
`endif
			case (sdram_ram1_haddr[13:12])
	`ifdef	sdram_d1_16m_4b
				2'b00:	begin
					dimm_bh.dimm1.sdram0.ram_0[sdram_real_addr] = mem_dw_temp[7:0];
					dimm_bh.dimm1.sdram1.ram_0[sdram_real_addr] = mem_dw_temp[15:8];
					if(SDRAM_DQ_Mode)begin
						dimm_bh.dimm1.sdram0.ram_0[sdram_real_addr+1] = mem_dw_temp[23:16];
						dimm_bh.dimm1.sdram1.ram_0[sdram_real_addr+1] = mem_dw_temp[31:24];
					end
					else	begin
						dimm_bh.dimm1.sdram2.ram_0[sdram_real_addr] = mem_dw_temp[23:16];
						dimm_bh.dimm1.sdram3.ram_0[sdram_real_addr] = mem_dw_temp[31:24];
					end
				end
				2'b01:	begin
					dimm_bh.dimm1.sdram0.ram_1[sdram_real_addr] = mem_dw_temp[7:0];
					dimm_bh.dimm1.sdram1.ram_1[sdram_real_addr] = mem_dw_temp[15:8];
					if(SDRAM_DQ_Mode)begin
						dimm_bh.dimm1.sdram0.ram_1[sdram_real_addr+1] = mem_dw_temp[23:16];
						dimm_bh.dimm1.sdram1.ram_1[sdram_real_addr+1] = mem_dw_temp[31:24];
					end
					else	begin
						dimm_bh.dimm1.sdram2.ram_1[sdram_real_addr] = mem_dw_temp[23:16];
						dimm_bh.dimm1.sdram3.ram_1[sdram_real_addr] = mem_dw_temp[31:24];
					end
				end
				2'b10:	begin
					dimm_bh.dimm1.sdram0.ram_2[sdram_real_addr] = mem_dw_temp[7:0];
					dimm_bh.dimm1.sdram1.ram_2[sdram_real_addr] = mem_dw_temp[15:8];
					if(SDRAM_DQ_Mode)begin
						dimm_bh.dimm1.sdram0.ram_2[sdram_real_addr+1] = mem_dw_temp[23:16];
						dimm_bh.dimm1.sdram1.ram_2[sdram_real_addr+1] = mem_dw_temp[31:24];
					end
					else	begin
						dimm_bh.dimm1.sdram2.ram_2[sdram_real_addr] = mem_dw_temp[23:16];
						dimm_bh.dimm1.sdram3.ram_2[sdram_real_addr] = mem_dw_temp[31:24];
					end
				end
				2'b11:	begin
					dimm_bh.dimm1.sdram0.ram_3[sdram_real_addr] = mem_dw_temp[7:0];
					dimm_bh.dimm1.sdram1.ram_3[sdram_real_addr] = mem_dw_temp[15:8];
					if(SDRAM_DQ_Mode)begin
						dimm_bh.dimm1.sdram0.ram_3[sdram_real_addr+1] = mem_dw_temp[23:16];
						dimm_bh.dimm1.sdram1.ram_3[sdram_real_addr+1] = mem_dw_temp[31:24];
					end
					else	begin
						dimm_bh.dimm1.sdram2.ram_3[sdram_real_addr] = mem_dw_temp[23:16];
						dimm_bh.dimm1.sdram3.ram_3[sdram_real_addr] = mem_dw_temp[31:24];
					end
				end
	`else
	`ifdef	sdram_d1_8m_4b
				2'b00:	begin
					dimm_bh.dimm1.sdram0.ram_0[sdram_real_addr] = mem_dw_temp[7:0];
					dimm_bh.dimm1.sdram1.ram_0[sdram_real_addr] = mem_dw_temp[15:8];
					if(SDRAM_DQ_Mode)begin
						dimm_bh.dimm1.sdram0.ram_0[sdram_real_addr+1] = mem_dw_temp[23:16];
						dimm_bh.dimm1.sdram1.ram_0[sdram_real_addr+1] = mem_dw_temp[31:24];
					end
					else	begin
						dimm_bh.dimm1.sdram2.ram_0[sdram_real_addr] = mem_dw_temp[23:16];
						dimm_bh.dimm1.sdram3.ram_0[sdram_real_addr] = mem_dw_temp[31:24];
					end
				end
				2'b01:	begin
					dimm_bh.dimm1.sdram0.ram_1[sdram_real_addr] = mem_dw_temp[7:0];
					dimm_bh.dimm1.sdram1.ram_1[sdram_real_addr] = mem_dw_temp[15:8];
					if(SDRAM_DQ_Mode)begin
						dimm_bh.dimm1.sdram0.ram_1[sdram_real_addr+1] = mem_dw_temp[23:16];
						dimm_bh.dimm1.sdram1.ram_1[sdram_real_addr+1] = mem_dw_temp[31:24];
					end
					else	begin
						dimm_bh.dimm1.sdram2.ram_1[sdram_real_addr] = mem_dw_temp[23:16];
						dimm_bh.dimm1.sdram3.ram_1[sdram_real_addr] = mem_dw_temp[31:24];
					end
				end
				2'b10:	begin
					dimm_bh.dimm1.sdram0.ram_2[sdram_real_addr] = mem_dw_temp[7:0];
					dimm_bh.dimm1.sdram1.ram_2[sdram_real_addr] = mem_dw_temp[15:8];
					if(SDRAM_DQ_Mode)begin
						dimm_bh.dimm1.sdram0.ram_2[sdram_real_addr+1] = mem_dw_temp[23:16];
						dimm_bh.dimm1.sdram1.ram_2[sdram_real_addr+1] = mem_dw_temp[31:24];
					end
					else	begin
						dimm_bh.dimm1.sdram2.ram_2[sdram_real_addr] = mem_dw_temp[23:16];
						dimm_bh.dimm1.sdram3.ram_2[sdram_real_addr] = mem_dw_temp[31:24];
					end
				end
				2'b11:	begin
					dimm_bh.dimm1.sdram0.ram_3[sdram_real_addr] = mem_dw_temp[7:0];
					dimm_bh.dimm1.sdram1.ram_3[sdram_real_addr] = mem_dw_temp[15:8];
					if(SDRAM_DQ_Mode)begin
						dimm_bh.dimm1.sdram0.ram_3[sdram_real_addr+1] = mem_dw_temp[23:16];
						dimm_bh.dimm1.sdram1.ram_3[sdram_real_addr+1] = mem_dw_temp[31:24];
					end
					else	begin
						dimm_bh.dimm1.sdram2.ram_3[sdram_real_addr] = mem_dw_temp[23:16];
						dimm_bh.dimm1.sdram3.ram_3[sdram_real_addr] = mem_dw_temp[31:24];
					end
				end
	`else
	`ifdef	sdram_d1_4m_4b
				2'b00:	begin
					dimm_bh.dimm1.sdram0.ram_0[sdram_real_addr] = mem_dw_temp[15:0] ;
					if(SDRAM_DQ_Mode)begin
						dimm_bh.dimm1.sdram0.ram_0[sdram_real_addr+1] = mem_dw_temp[31:16];
					end
					else	begin
						dimm_bh.dimm1.sdram1.ram_0[sdram_real_addr] = mem_dw_temp[31:16];
					end
				end
				2'b01:	begin
					dimm_bh.dimm1.sdram0.ram_1[sdram_real_addr] = mem_dw_temp[15:0] ;
					if(SDRAM_DQ_Mode)begin
						dimm_bh.dimm1.sdram0.ram_1[sdram_real_addr+1] = mem_dw_temp[31:16];
					end
					else	begin
						dimm_bh.dimm1.sdram1.ram_1[sdram_real_addr] = mem_dw_temp[31:16];
					end
				end
				2'b10:	begin
					dimm_bh.dimm1.sdram0.ram_2[sdram_real_addr] = mem_dw_temp[15:0] ;
					if(SDRAM_DQ_Mode)begin
						dimm_bh.dimm1.sdram0.ram_2[sdram_real_addr+1] = mem_dw_temp[31:16];
					end
					else	begin
						dimm_bh.dimm1.sdram1.ram_2[sdram_real_addr] = mem_dw_temp[31:16];
					end
				end
				2'b11:	begin
					dimm_bh.dimm1.sdram0.ram_3[sdram_real_addr] = mem_dw_temp[15:0] ;
					if(SDRAM_DQ_Mode)begin
						dimm_bh.dimm1.sdram0.ram_3[sdram_real_addr+1] = mem_dw_temp[31:16];
					end
					else	begin
						dimm_bh.dimm1.sdram1.ram_3[sdram_real_addr] = mem_dw_temp[31:16];
					end
				end
	`else
	`ifdef	sdram_d1_2m_2b
				2'b00:	begin
					dimm_bh.dimm1.sdram0.ram_0[sdram_real_addr] = mem_dw_temp[15:0] ;
					if(SDRAM_DQ_Mode)begin
						dimm_bh.dimm1.sdram0.ram_0[sdram_real_addr+1] = mem_dw_temp[31:16];
					end
					else	begin
						dimm_bh.dimm1.sdram1.ram_0[sdram_real_addr] = mem_dw_temp[31:16];
					end
				end
				2'b01:	begin
					dimm_bh.dimm1.sdram0.ram_1[sdram_real_addr] = mem_dw_temp[15:0] ;
					if(SDRAM_DQ_Mode)begin
						dimm_bh.dimm1.sdram0.ram_1[sdram_real_addr+1] = mem_dw_temp[31:16];
					end
					else	begin
						dimm_bh.dimm1.sdram1.ram_1[sdram_real_addr] = mem_dw_temp[31:16];
					end
				end
	`endif
	`endif
	`endif
	`endif
				default	;
			endcase
		end
	end
end
endtask

task	SDRAM_dump_memory;
input	mem_base;
input	mem_size;
input	file_handle;
integer	mem_size, mem_base, file_handle;
begin
	SDRAM_dump_array(mem_base,mem_size);
	for (curn_offset = 0; curn_offset < mem_size; curn_offset = curn_offset + 1)
	begin
		$fdisplay(file_handle,"%h",mem_array[curn_offset]);
	end
end
endtask

task	SDRAM_dump_array;
input	mem_base;
input	mem_size;
integer	mem_size, mem_base;
reg	[31:0]	mem_dump_temp;
begin
    $display ("\tDump SDRAM context: address = %h, size = %h\n", mem_base, mem_size);
	for (curn_offset = 0; curn_offset < mem_size; curn_offset = curn_offset + 1)
	begin
		// convert address
		//curn_addr = (curn_offset << 2) + mem_base;

		if(SDRAM_DQ_Mode)begin
			curn_addr	= ((curn_offset << 2) + mem_base)*2;
		end
		else	begin
			curn_addr = (curn_offset << 2) + mem_base;
		end
		if (curn_addr < sdram_row1_base)	begin	// at row 0 SDRAM
			//sys_ram0_addr = curn_addr;
//			sdram_ram0_haddr_next	= SDRAM_ram_ma_dec(curn_addr[29:2], sdram_row0_map);
//			sdram_ram0_haddr		=	SDRAM_Dimm_Bank_Map	?	sdram_ram0_haddr_next
//										:	{1'b0, sdram_ram0_haddr_next[12:0]}	;
	sdram_ram0_haddr = SDRAM_ram_ma_dec(curn_addr[29:2], sdram_row0_map);
				// wait ram_ma_dec module calculate the real SDRAM address
//			sdram_real_addr = {sdram_ram0_haddr[11:0], curn_addr[9:2]};
		`ifdef	NEW_MEM
			case(sdram_row0_map[2:0])
			3'b000:	begin
			`ifdef sdram_d0_16m_4b
				sdram_real_addr = {{1'b0,sdram_ram0_haddr[10:0]},{2'b0,curn_addr[9:2]}};
			`else
				sdram_real_addr = {sdram_ram0_haddr[10:0], curn_addr[9:2]};
			`endif
			end
			3'b001:	begin
			`ifdef sdram_d0_16m_4b
				sdram_real_addr = {{1'b0,sdram_ram0_haddr[10:0]},{1'b0,curn_addr[10:2]}};
			`else
				sdram_real_addr = {sdram_ram0_haddr[10:0], curn_addr[10:2]};
			`endif
			end
			3'b100:	begin
			`ifdef sdram_d0_16m_4b
				sdram_real_addr = {sdram_ram0_haddr[11:0],{2'b0,curn_addr[9:2]}};
			`else
				sdram_real_addr = {sdram_ram0_haddr[11:0], curn_addr[9:2]};
			`endif
			end
			3'b101:	begin
			`ifdef sdram_d0_16m_4b
				sdram_real_addr = {sdram_ram0_haddr[11:0],{1'b0,curn_addr[10:2]}};
			`else
				sdram_real_addr = {sdram_ram0_haddr[11:0], curn_addr[10:2]};
			`endif
			end
			3'b110:	begin
				sdram_real_addr = {sdram_ram0_haddr[11:0], curn_addr[11:2]};
			end
			default:	begin
				sdram_real_addr = {sdram_ram0_haddr[11:0], curn_addr[11:2]};
			end
			endcase
		`endif
			case (sdram_ram0_haddr[13:12])
	`ifdef	sdram_d0_16m_4b
			2'b00:	begin
				if(SDRAM_DQ_Mode)begin
					mem_dump_temp = {dimm_bh.dimm0.sdram1.ram_0[sdram_real_addr+1], dimm_bh.dimm0.sdram0.ram_0[sdram_real_addr+1],
					dimm_bh.dimm0.sdram1.ram_0[sdram_real_addr], dimm_bh.dimm0.sdram0.ram_0[sdram_real_addr]};
				end
				else	begin
					mem_dump_temp = {dimm_bh.dimm0.sdram3.ram_0[sdram_real_addr], dimm_bh.dimm0.sdram2.ram_0[sdram_real_addr],
					dimm_bh.dimm0.sdram1.ram_0[sdram_real_addr], dimm_bh.dimm0.sdram0.ram_0[sdram_real_addr]};
				end
			end
			2'b01:	begin
				if(SDRAM_DQ_Mode)begin
					mem_dump_temp = {dimm_bh.dimm0.sdram1.ram_1[sdram_real_addr+1], dimm_bh.dimm0.sdram0.ram_1[sdram_real_addr+1],
					dimm_bh.dimm0.sdram1.ram_1[sdram_real_addr], dimm_bh.dimm0.sdram0.ram_1[sdram_real_addr]};
				end
				else	begin
					mem_dump_temp = {dimm_bh.dimm0.sdram3.ram_1[sdram_real_addr], dimm_bh.dimm0.sdram2.ram_1[sdram_real_addr],
					dimm_bh.dimm0.sdram1.ram_1[sdram_real_addr], dimm_bh.dimm0.sdram0.ram_1[sdram_real_addr]};
				end
			end
	`else
	`ifdef	sdram_d0_8m_4b
			2'b00:	begin
				if(SDRAM_DQ_Mode)begin
					mem_dump_temp = {dimm_bh.dimm0.sdram1.ram_0[sdram_real_addr+1], dimm_bh.dimm0.sdram0.ram_0[sdram_real_addr+1],
					dimm_bh.dimm0.sdram1.ram_0[sdram_real_addr], dimm_bh.dimm0.sdram0.ram_0[sdram_real_addr]};
				end
				else	begin
					mem_dump_temp = {dimm_bh.dimm0.sdram3.ram_0[sdram_real_addr], dimm_bh.dimm0.sdram2.ram_0[sdram_real_addr],
					dimm_bh.dimm0.sdram1.ram_0[sdram_real_addr], dimm_bh.dimm0.sdram0.ram_0[sdram_real_addr]};
				end
			end
			2'b01:	begin
				if(SDRAM_DQ_Mode)begin
					mem_dump_temp = {dimm_bh.dimm0.sdram1.ram_1[sdram_real_addr+1], dimm_bh.dimm0.sdram0.ram_1[sdram_real_addr+1],
					dimm_bh.dimm0.sdram1.ram_1[sdram_real_addr], dimm_bh.dimm0.sdram0.ram_1[sdram_real_addr]};
				end
				else	begin
					mem_dump_temp = {dimm_bh.dimm0.sdram3.ram_1[sdram_real_addr], dimm_bh.dimm0.sdram2.ram_1[sdram_real_addr],
					dimm_bh.dimm0.sdram1.ram_1[sdram_real_addr], dimm_bh.dimm0.sdram0.ram_1[sdram_real_addr]};
				end
			end
	`else
			2'b00:	begin
				if(SDRAM_DQ_Mode)begin
					mem_dump_temp = {dimm_bh.dimm0.sdram0.ram_0[sdram_real_addr+1], dimm_bh.dimm0.sdram0.ram_0[sdram_real_addr]};
				end
				else	begin
					mem_dump_temp = {dimm_bh.dimm0.sdram1.ram_0[sdram_real_addr], dimm_bh.dimm0.sdram0.ram_0[sdram_real_addr]};
				end
			end
			2'b01:	begin
				if(SDRAM_DQ_Mode)begin
					mem_dump_temp = {dimm_bh.dimm0.sdram0.ram_1[sdram_real_addr+1], dimm_bh.dimm0.sdram0.ram_1[sdram_real_addr]};
				end
				else	begin
					mem_dump_temp = {dimm_bh.dimm0.sdram1.ram_1[sdram_real_addr], dimm_bh.dimm0.sdram0.ram_1[sdram_real_addr]};
				end
			end
	`endif
	`endif
//	--------------------------------------------------
	`ifdef	sdram_d0_16m_4b
			2'b10:	begin
				if(SDRAM_DQ_Mode)begin
					mem_dump_temp = {dimm_bh.dimm0.sdram1.ram_2[sdram_real_addr+1], dimm_bh.dimm0.sdram0.ram_2[sdram_real_addr+1],
					dimm_bh.dimm0.sdram1.ram_2[sdram_real_addr], dimm_bh.dimm0.sdram0.ram_2[sdram_real_addr]};
				end
				else	begin
					mem_dump_temp = {dimm_bh.dimm0.sdram3.ram_2[sdram_real_addr], dimm_bh.dimm0.sdram2.ram_2[sdram_real_addr],
					dimm_bh.dimm0.sdram1.ram_2[sdram_real_addr], dimm_bh.dimm0.sdram0.ram_2[sdram_real_addr]};
				end
			end
			2'b11:	begin
				if(SDRAM_DQ_Mode)begin
					mem_dump_temp = {dimm_bh.dimm0.sdram1.ram_3[sdram_real_addr+1], dimm_bh.dimm0.sdram0.ram_3[sdram_real_addr+1],
					dimm_bh.dimm0.sdram1.ram_3[sdram_real_addr], dimm_bh.dimm0.sdram0.ram_3[sdram_real_addr]};
				end
				else	begin
					mem_dump_temp = {dimm_bh.dimm0.sdram3.ram_3[sdram_real_addr], dimm_bh.dimm0.sdram2.ram_3[sdram_real_addr],
					dimm_bh.dimm0.sdram1.ram_3[sdram_real_addr], dimm_bh.dimm0.sdram0.ram_3[sdram_real_addr]};
				end
			end
	`else
	`ifdef	sdram_d0_8m_4b
			2'b10:	begin
				if(SDRAM_DQ_Mode)begin
					mem_dump_temp = {dimm_bh.dimm0.sdram1.ram_2[sdram_real_addr+1], dimm_bh.dimm0.sdram0.ram_2[sdram_real_addr+1],
					dimm_bh.dimm0.sdram1.ram_2[sdram_real_addr], dimm_bh.dimm0.sdram0.ram_2[sdram_real_addr]};
				end
				else	begin
					mem_dump_temp = {dimm_bh.dimm0.sdram3.ram_2[sdram_real_addr], dimm_bh.dimm0.sdram2.ram_2[sdram_real_addr],
					dimm_bh.dimm0.sdram1.ram_2[sdram_real_addr], dimm_bh.dimm0.sdram0.ram_2[sdram_real_addr]};
				end
			end
			2'b11:	begin
				if(SDRAM_DQ_Mode)begin
					mem_dump_temp = {dimm_bh.dimm0.sdram1.ram_3[sdram_real_addr+1], dimm_bh.dimm0.sdram0.ram_3[sdram_real_addr+1],
					dimm_bh.dimm0.sdram1.ram_3[sdram_real_addr], dimm_bh.dimm0.sdram0.ram_3[sdram_real_addr]};
				end
				else	begin
					mem_dump_temp = {dimm_bh.dimm0.sdram3.ram_3[sdram_real_addr], dimm_bh.dimm0.sdram2.ram_3[sdram_real_addr],
					dimm_bh.dimm0.sdram1.ram_3[sdram_real_addr], dimm_bh.dimm0.sdram0.ram_3[sdram_real_addr]};
				end
			end
	`else
	`ifdef	sdram_d0_4m_4b
			2'b10:	begin
				if(SDRAM_DQ_Mode)begin
					mem_dump_temp = {dimm_bh.dimm0.sdram0.ram_2[sdram_real_addr+1], dimm_bh.dimm0.sdram0.ram_2[sdram_real_addr]};
				end
				else	begin
					mem_dump_temp = {dimm_bh.dimm0.sdram1.ram_2[sdram_real_addr], dimm_bh.dimm0.sdram0.ram_2[sdram_real_addr]};
				end
			end
			2'b11:	begin
				if(SDRAM_DQ_Mode)begin
					mem_dump_temp = {dimm_bh.dimm0.sdram0.ram_3[sdram_real_addr+1], dimm_bh.dimm0.sdram0.ram_3[sdram_real_addr]};
				end
				else	begin
					mem_dump_temp = {dimm_bh.dimm0.sdram1.ram_3[sdram_real_addr], dimm_bh.dimm0.sdram0.ram_3[sdram_real_addr]};
				end
			end
	`endif
	`endif
	`endif
			endcase
		end
		else	begin
//		sdram_ram1_haddr_next	= SDRAM_ram_ma_dec(curn_addr[29:2], sdram_row0_map);
//		sdram_ram1_haddr		= SDRAM_Dimm_Bank_Map	?	sdram_ram0_haddr_next
//										:	{1'b0, sdram_ram0_haddr_next[12:0]}	;
			sdram_ram1_haddr = SDRAM_ram_ma_dec(curn_addr[29:2], sdram_row0_map);
				// wait ram_ma_dec module calculate the real SDRAM address
			//sdram_real_addr = {sdram_ram1_haddr[11:0], curn_addr[9:2]};
		`ifdef	NEW_MEM
			case(sdram_row0_map[2:0])
			3'b000:	begin
			`ifdef sdram_d1_16m_4b
				sdram_real_addr = {{1'b0,sdram_ram1_haddr[10:0]},{2'b0,curn_addr[9:2]}};
			`else
				sdram_real_addr = {sdram_ram1_haddr[10:0], curn_addr[9:2]};
			`endif
			end
			3'b001:	begin
			`ifdef sdram_d1_16m_4b
				sdram_real_addr = {{1'b0,sdram_ram1_haddr[10:0]},{1'b0,curn_addr[10:2]}};
			`else
				sdram_real_addr = {sdram_ram1_haddr[10:0], curn_addr[10:2]};
			`endif
			end
			3'b100:	begin
			`ifdef sdram_d1_16m_4b
				sdram_real_addr = {sdram_ram1_haddr[11:0],{2'b0,curn_addr[9:2]}};
			`else
				sdram_real_addr = {sdram_ram1_haddr[11:0], curn_addr[9:2]};
			`endif
			end
			3'b101:	begin
			`ifdef sdram_d1_16m_4b
				sdram_real_addr = {sdram_ram1_haddr[11:0],{1'b0,curn_addr[10:2]}};
			`else
				sdram_real_addr = {sdram_ram1_haddr[11:0], curn_addr[10:2]};
			`endif
			end
			3'b110:	begin
				sdram_real_addr = {sdram_ram1_haddr[11:0], curn_addr[11:2]};
			end
			default:	begin
				sdram_real_addr = {sdram_ram1_haddr[11:0], curn_addr[11:2]};
			end
			endcase
		`else
			case(sdram_row1_map[1:0])
			2'b00:	begin
				sdram_real_addr = {sdram_ram1_haddr[11:0], curn_addr[9:2]};
			end
			2'b01:	begin
				sdram_real_addr = {sdram_ram1_haddr[11:0], curn_addr[10:2]};
			end
			default:	begin
				sdram_real_addr = {sdram_ram1_haddr[11:0], curn_addr[11:2]};
			end
			endcase
		`endif
			case (sdram_ram1_haddr[13:12])
	`ifdef	sdram_d1_16m_4b
			2'b00:	begin
				if(SDRAM_DQ_Mode)begin
					mem_dump_temp = {dimm_bh.dimm1.sdram1.ram_0[sdram_real_addr+1], dimm_bh.dimm1.sdram0.ram_0[sdram_real_addr+1],
					dimm_bh.dimm1.sdram1.ram_0[sdram_real_addr], dimm_bh.dimm1.sdram0.ram_0[sdram_real_addr]};
				end
				else	begin
					mem_dump_temp = {dimm_bh.dimm1.sdram3.ram_0[sdram_real_addr], dimm_bh.dimm1.sdram2.ram_0[sdram_real_addr],
					dimm_bh.dimm1.sdram1.ram_0[sdram_real_addr], dimm_bh.dimm1.sdram0.ram_0[sdram_real_addr]};
				end
			end
			2'b01:	begin
				if(SDRAM_DQ_Mode)begin
					mem_dump_temp = {dimm_bh.dimm1.sdram1.ram_1[sdram_real_addr+1], dimm_bh.dimm1.sdram0.ram_1[sdram_real_addr+1],
					dimm_bh.dimm1.sdram1.ram_1[sdram_real_addr], dimm_bh.dimm1.sdram0.ram_1[sdram_real_addr]};
				end
				else	begin
					mem_dump_temp = {dimm_bh.dimm1.sdram3.ram_1[sdram_real_addr], dimm_bh.dimm1.sdram2.ram_1[sdram_real_addr],
					dimm_bh.dimm1.sdram1.ram_1[sdram_real_addr], dimm_bh.dimm1.sdram0.ram_1[sdram_real_addr]};
				end
			end
			2'b10:	begin
				if(SDRAM_DQ_Mode)begin
					mem_dump_temp = {dimm_bh.dimm1.sdram1.ram_2[sdram_real_addr+1], dimm_bh.dimm1.sdram0.ram_2[sdram_real_addr+1],
					dimm_bh.dimm1.sdram1.ram_2[sdram_real_addr], dimm_bh.dimm1.sdram0.ram_2[sdram_real_addr]};
				end
				else	begin
					mem_dump_temp = {dimm_bh.dimm1.sdram3.ram_2[sdram_real_addr], dimm_bh.dimm1.sdram2.ram_2[sdram_real_addr],
					dimm_bh.dimm1.sdram1.ram_2[sdram_real_addr], dimm_bh.dimm1.sdram0.ram_2[sdram_real_addr]};
				end
			end
			2'b11:	begin
				if(SDRAM_DQ_Mode)begin
					mem_dump_temp = {dimm_bh.dimm1.sdram1.ram_3[sdram_real_addr+1], dimm_bh.dimm1.sdram0.ram_3[sdram_real_addr+1],
					dimm_bh.dimm1.sdram1.ram_3[sdram_real_addr], dimm_bh.dimm1.sdram0.ram_3[sdram_real_addr]};
				end
				else	begin
					mem_dump_temp = {dimm_bh.dimm1.sdram3.ram_3[sdram_real_addr], dimm_bh.dimm1.sdram2.ram_3[sdram_real_addr],
					dimm_bh.dimm1.sdram1.ram_3[sdram_real_addr], dimm_bh.dimm1.sdram0.ram_3[sdram_real_addr]};
				end
			end
	`else
	`ifdef	sdram_d1_8m_4b
			2'b00:	begin
				if(SDRAM_DQ_Mode)begin
					mem_dump_temp = {dimm_bh.dimm1.sdram1.ram_0[sdram_real_addr+1], dimm_bh.dimm1.sdram0.ram_0[sdram_real_addr+1],
					dimm_bh.dimm1.sdram1.ram_0[sdram_real_addr], dimm_bh.dimm1.sdram0.ram_0[sdram_real_addr]};
				end
				else	begin
					mem_dump_temp = {dimm_bh.dimm1.sdram3.ram_0[sdram_real_addr], dimm_bh.dimm1.sdram2.ram_0[sdram_real_addr],
					dimm_bh.dimm1.sdram1.ram_0[sdram_real_addr], dimm_bh.dimm1.sdram0.ram_0[sdram_real_addr]};
				end
			end
			2'b01:	begin
				if(SDRAM_DQ_Mode)begin
					mem_dump_temp = {dimm_bh.dimm1.sdram1.ram_1[sdram_real_addr+1], dimm_bh.dimm1.sdram0.ram_1[sdram_real_addr+1],
					dimm_bh.dimm1.sdram1.ram_1[sdram_real_addr], dimm_bh.dimm1.sdram0.ram_1[sdram_real_addr]};
				end
				else	begin
					mem_dump_temp = {dimm_bh.dimm1.sdram3.ram_1[sdram_real_addr], dimm_bh.dimm1.sdram2.ram_1[sdram_real_addr],
					dimm_bh.dimm1.sdram1.ram_1[sdram_real_addr], dimm_bh.dimm1.sdram0.ram_1[sdram_real_addr]};
				end
			end
			2'b10:	begin
				if(SDRAM_DQ_Mode)begin
					mem_dump_temp = {dimm_bh.dimm1.sdram1.ram_2[sdram_real_addr+1], dimm_bh.dimm1.sdram0.ram_2[sdram_real_addr+1],
					dimm_bh.dimm1.sdram1.ram_2[sdram_real_addr], dimm_bh.dimm1.sdram0.ram_2[sdram_real_addr]};
				end
				else	begin
					mem_dump_temp = {dimm_bh.dimm1.sdram3.ram_2[sdram_real_addr], dimm_bh.dimm1.sdram2.ram_2[sdram_real_addr],
					dimm_bh.dimm1.sdram1.ram_2[sdram_real_addr], dimm_bh.dimm1.sdram0.ram_2[sdram_real_addr]};
				end
			end
			2'b11:	begin
				if(SDRAM_DQ_Mode)begin
					mem_dump_temp = {dimm_bh.dimm1.sdram1.ram_3[sdram_real_addr+1], dimm_bh.dimm1.sdram0.ram_3[sdram_real_addr+1],
					dimm_bh.dimm1.sdram1.ram_3[sdram_real_addr], dimm_bh.dimm1.sdram0.ram_3[sdram_real_addr]};
				end
				else	begin
					mem_dump_temp = {dimm_bh.dimm1.sdram3.ram_3[sdram_real_addr], dimm_bh.dimm1.sdram2.ram_3[sdram_real_addr],
					dimm_bh.dimm1.sdram1.ram_3[sdram_real_addr], dimm_bh.dimm1.sdram0.ram_3[sdram_real_addr]};
				end
			end
	`else
	`ifdef	sdram_d1_4m_4b
			2'b00:	begin
				if(SDRAM_DQ_Mode)begin
					mem_dump_temp = {dimm_bh.dimm1.sdram0.ram_0[sdram_real_addr+1], dimm_bh.dimm1.sdram0.ram_0[sdram_real_addr]};
				end
				else	begin
					mem_dump_temp = {dimm_bh.dimm1.sdram1.ram_0[sdram_real_addr], dimm_bh.dimm1.sdram0.ram_0[sdram_real_addr]};
				end
			end
			2'b01:	begin
				if(SDRAM_DQ_Mode)begin
					mem_dump_temp = {dimm_bh.dimm1.sdram0.ram_1[sdram_real_addr+1], dimm_bh.dimm1.sdram0.ram_1[sdram_real_addr]};
				end
				else	begin
					mem_dump_temp = {dimm_bh.dimm1.sdram1.ram_1[sdram_real_addr], dimm_bh.dimm1.sdram0.ram_1[sdram_real_addr]};
				end
			end
			2'b10:	begin
				if(SDRAM_DQ_Mode)begin
					mem_dump_temp = {dimm_bh.dimm1.sdram0.ram_2[sdram_real_addr+1], dimm_bh.dimm1.sdram0.ram_2[sdram_real_addr]};
				end
				else	begin
					mem_dump_temp = {dimm_bh.dimm1.sdram1.ram_2[sdram_real_addr], dimm_bh.dimm1.sdram0.ram_2[sdram_real_addr]};
				end
			end
			2'b11:	begin
				if(SDRAM_DQ_Mode)begin
					mem_dump_temp = {dimm_bh.dimm1.sdram0.ram_3[sdram_real_addr+1], dimm_bh.dimm1.sdram0.ram_3[sdram_real_addr]};
				end
				else	begin
					mem_dump_temp = {dimm_bh.dimm1.sdram1.ram_3[sdram_real_addr], dimm_bh.dimm1.sdram0.ram_3[sdram_real_addr]};
				end
			end
	`else
	`ifdef	sdram_d1_2m_2b
			2'b00:	begin
				if(SDRAM_DQ_Mode)begin
					mem_dump_temp = {dimm_bh.dimm1.sdram0.ram_0[sdram_real_addr+1], dimm_bh.dimm1.sdram0.ram_0[sdram_real_addr]};
				end
				else	begin
					mem_dump_temp = {dimm_bh.dimm1.sdram1.ram_0[sdram_real_addr], dimm_bh.dimm1.sdram0.ram_0[sdram_real_addr]};
				end
			end
			2'b01:	begin
				if(SDRAM_DQ_Mode)begin
					mem_dump_temp = {dimm_bh.dimm1.sdram0.ram_1[sdram_real_addr+1], dimm_bh.dimm1.sdram0.ram_1[sdram_real_addr]};
				end
				else	begin
					mem_dump_temp = {dimm_bh.dimm1.sdram1.ram_1[sdram_real_addr], dimm_bh.dimm1.sdram0.ram_1[sdram_real_addr]};
				end
			end
	`endif
	`endif
	`endif
	`endif
			default: ;
			endcase
		end
		mem_array[curn_offset] = mem_dump_temp;
	end
end
endtask

`ifdef	sdram_bus_debug	// Kandy 2000.05.08 for check Internal SDRAM bus
reg	[31:2]	sdram_bus_raddr, sdram_bus_waddr, sdram_bus_raddr_tmp;
always @(posedge `path_chipset.mem_clk)
	`ifdef GSIM
	if (`path_chipset.mem_intf.ram_ctrl.set_drdy &&	`path_chipset.mem_intf.mbus_rw)
		sdram_bus_raddr_tmp <= #1 {2'h0, `path_chipset.sdram_addr};
	`else
	if (`path_chipset.mem_intf.ram_ctrl.bank_rd)
		sdram_bus_raddr_tmp <= #1 {2'h0, `path_chipset.sdram_addr};
	`endif

always @(posedge `path_chipset.mem_clk)
	if (`path_chipset.mem_intf.ram_ctrl.ram_rd_rdy)	begin
		sdram_bus_raddr <= #1 {sdram_bus_raddr[31:4], sdram_bus_raddr[3:2] + 2'h1};
		$fdisplay(dimm_bh.dimm_rpt,"%0t Read  addr=32'h%h, data=32'h%h, byte_en=4'b1111", $realtime,
			{sdram_bus_raddr, 2'h0}, ram_dq);
	end
	else
		sdram_bus_raddr <= #1 sdram_bus_raddr_tmp;

always @(posedge `path_chipset.mem_clk)
	if (`path_chipset.mem_intf.ram_wr_rdy)	begin
		sdram_bus_waddr <= #1 {sdram_bus_waddr[31:4], sdram_bus_waddr[3:2] + 2'h1};
		$fdisplay(dimm_bh.dimm_rpt,"%0t Write addr=32'h%h, data=32'h%h, byte_en=4'b%b", $realtime,
			{sdram_bus_waddr, 2'h0}, `path_chipset.ram_dq_out, `path_chipset.sdram_wbe);
	end
	else
		sdram_bus_waddr <= #1 {2'h0, `path_chipset.sdram_addr};

`endif
`ifdef	NEW_MEM
//====================================decode function===========================
function	[13:0]	SDRAM_ram_ma_dec;	// return bank addr and row addr
input	[27:0]	ma;
input	[2:0]	rowx_map;
//wire	sequence_mode			;
reg		banks_1bit				;
begin

  	banks_1bit				=	SDRAM_Dimm_Bank_Map	;
	sequence_mode			=	{rowx_map == 3'b000}	&	!ma[20]	|
								{rowx_map == 3'b001}	&	!ma[21]	|
								{rowx_map == 3'b100}	&	!ma[21]	|
								{rowx_map == 3'b101}	&	!ma[22]	|
								{rowx_map == 3'b110}	&	!ma[23]	;


	SDRAM_ram_ma_dec[13]	=	banks_1bit	?	(
								{rowx_map == 3'b000}	&	ma[20]	|
								{rowx_map == 3'b001}	&	ma[21]	|
								{rowx_map == 3'b100}	&	ma[21]	|
								{rowx_map == 3'b101}	&	ma[22]	|
								{rowx_map == 3'b110}	&	ma[23]	)
								:	1'b0		;

	 SDRAM_ram_ma_dec[12] =		{sequence_mode}	&{rowx_map == 3'b000} & ma[19] 	|
	                            {sequence_mode}	&{rowx_map == 3'b001} & ma[20] 	|
	                            {sequence_mode}	&{rowx_map == 3'b100} & ma[20] 	|
	                            {sequence_mode}	&{rowx_map == 3'b101} & ma[21]	|
	                            {sequence_mode}	&{rowx_map == 3'b110} & ma[22]	|
								{!sequence_mode}&{rowx_map == 3'b000} & ma[8] 	|
								{!sequence_mode}&{rowx_map == 3'b001} & ma[9] 	|
								{!sequence_mode}&{rowx_map == 3'b100} & ma[8] 	|
								{!sequence_mode}&{rowx_map == 3'b101} & ma[9] 	|
								{!sequence_mode}&{rowx_map == 3'b110} & ma[10]	;

	SDRAM_ram_ma_dec[7:0]= ma[18:11];

	SDRAM_ram_ma_dec[11:8]=  														// Rw/Cl/B
	{4{(rowx_map==3'h0)&sequence_mode}}  & {1'b0	,ma[8]	, ma[10], ma[9]	}	|	// 11/08/1  11/08/2
	{4{(rowx_map==3'h1)&sequence_mode}}  & {1'b0	,ma[19]	, ma[10], ma[9]	}	|	// 11/09/1  11/09/2
	{4{(rowx_map==3'h4)&sequence_mode}}  & {ma[8]	,ma[19] , ma[10], ma[9]	}	|	// 12/08/2
	{4{(rowx_map==3'h5)&sequence_mode}}  & {ma[20]	,ma[19] , ma[10], ma[9]	}	|	// 12/09/2
	{4{(rowx_map==3'h6)&sequence_mode}}  & {ma[21]	,ma[19] , ma[10], ma[20]}	|	// 12/10/2
	{4{(rowx_map==3'h0)&!sequence_mode}} & {1'b0	,ma[19] , ma[10], ma[9]	}	|   // 11/08/1  11/08/2
	{4{(rowx_map==3'h1)&!sequence_mode}} & {1'b0	,ma[19] , ma[10], ma[20]}	|   // 11/09/1  11/09/2
	{4{(rowx_map==3'h4)&!sequence_mode}} & {ma[20]	,ma[19] , ma[10], ma[9]	}	|   // 12/08/2
	{4{(rowx_map==3'h5)&!sequence_mode}} & {ma[21]	,ma[19] , ma[10], ma[20]}	|   // 12/09/2
	{4{(rowx_map==3'h6)&!sequence_mode}} & {ma[22]	,ma[19] , ma[21], ma[20]}	;   // 12/10/2

end
endfunction

`else
//==================================================function
function	[13:0]	SDRAM_ram_ma_dec;	// return bank addr and row addr

input	[27:0]	ma;
input	[2:0]	rowx_map;
begin

SDRAM_ram_ma_dec[12] =	{rowx_map[1:0] == 2'b00} & ma[8] |
						{rowx_map[1:0] == 2'b01} & ma[9] |
						{rowx_map[1:0] == 2'b10} & ma[10] |
						{rowx_map[1:0] == 2'b11} & ma[10];

SDRAM_ram_ma_dec[13] = rowx_map[2] &&
	(	(rowx_map[1:0] == 2'b00) & ma[9] |
		(rowx_map[1:0] == 2'b01) & ma[10] |
		(rowx_map[1:0] == 2'b10) & ma[11] |
		(rowx_map[1:0] == 2'b11) & ma[11] );

SDRAM_ram_ma_dec[6:0] = ma[19:13];

SDRAM_ram_ma_dec[10:7]=							// Rw/Cl/B
	{4{rowx_map==3'h0}} & {ma[12], ma[9], ma[10], ma[11]} |			// xx/08/1
	{4{rowx_map==3'h1}} & {ma[12], ma[20], ma[10], ma[11]} |			// xx/09/1
	{4{rowx_map==3'h2}} & {ma[12], ma[20], ma[21], ma[11]} |			// xx/10/1
	{4{rowx_map==3'h3}} & {ma[12], ma[20], ma[21], ma[11]} |			// xx/11/1
	{4{rowx_map==3'h4}} & {ma[12], ma[20], ma[10], ma[11]} |			// xx/08/2
	{4{rowx_map==3'h5}} & {ma[12], ma[20], ma[21], ma[11]} |			// xx/09/2
	{4{rowx_map==3'h6}} & {ma[12], ma[20], ma[21], ma[22]} |			// xx/10/2
	{4{rowx_map==3'h7}} & {ma[12], ma[20], ma[21], ma[22]};			// xx/11/2

SDRAM_ram_ma_dec[11] =	(rowx_map==3'h0) & ma[20] |
			(rowx_map==3'h1) & ma[21] |
			(rowx_map==3'h2) & ma[22] |
			(rowx_map==3'h3) & ma[22] |
			(rowx_map==3'h4) & ma[21] |
			(rowx_map==3'h5) & ma[22] |
			(rowx_map==3'h6) & ma[23] |
			(rowx_map==3'h7) & ma[23];

end
endfunction

//================================================================
`endif

// Flash rom initial
task	ROM_Initialize;
begin
`ifdef	NO_CPU
`else
//		cpu_alone_bootrom;//NO CPU_MODE IN M3357 JFR	031103
		begin
		$display("\nInitial CPU Boot Code to Flash Memory");
		//$readmemh("code.txt",boot_rom.rom_reg,'hc0_0000, 'hc0_ffff);// From 32'h1FC0_0000, Size 64K,
		$readmemh("pattern.code.map",boot_rom.rom_reg,'h0_0000, 'h0_ffff);// From 32'h1FC0_0000, Size 64K,
		end
`endif
end
endtask

task	flash_test_load_data;
`ifdef	FLASH_BURST_TEST
		begin
		$display("\nLoda program to Flash for Flash Burst Test");
		//$readmemh("code.txt",boot_rom.rom_reg,'hc0_0000, 'hc0_ffff);// From 32'h1FC0_0000, Size 64K,
		$readmemh("pattern.data.map",boot_rom.rom_reg,'h0_0000, 'h0_ffff);// From 32'h1FC1_0000, Size 64K,
		end
`else
begin
		$display("\nload data to flash for read operation test");
		$readmemh("../../../rsim/stimu.t/north/flash_test.txt",boot_rom.rom_reg,'h0_0000, 'h0_ffff);// From 32'h1FC0_0000, Size 64K,
end
`endif
endtask

task	load_sdram_param;
begin
		$display("\nload sdram init data to flash for program use");
		$readmemh("sdram_ini_param.txt",boot_rom.rom_reg,'hf_f000, 'hf_ffff);// From 32'h1FFF_FF00, Size 256 byte,
end
endtask
//Modified for system pattern, Main pattern and video pattern have to be load
//to different space, because video pattern was writen in C langu and can not
//be compired with assemble languaged pattern --- 2004-04-14 JFR
// SDRAM initial
task	SDRAM_Initialize;
integer i;
begin
`ifdef	NO_CPU
`else
`ifdef	SYSTEM_VIDEO_TEST
		begin
		$display("\nInitial Main Program Data to SDRAM");
        $readmemh("data.txt", byte_array,'h0);
        for (i = 0; i < 'h40000; i = i + 4)	begin
        	mem_array[i>>2] = {	byte_array[i+3],
        				byte_array[i+2],
        				byte_array[i+1],
        				byte_array[i]};
        end
//        for (i=0; i<10; i=i+2)
//        	$display("mem_array[%h]= %h\tmem_array[%h]= %h",i,mem_array[i],i+1,mem_array[i+1]);
  		SDRAM_load_memory (SDRAM_Load_Base, ('h40000>>2));	// load "data.txt" into SDRAM
						// 200 select "data.txt"


		$display("\nInitial Video Program Data to SDRAM");
        $readmemh("system_video_data.txt", byte_array,'h0);
        for (i = 0; i < 'h20000; i = i + 4)	begin
        	mem_array[i>>2] = {	byte_array[i+3],
        				byte_array[i+2],
        				byte_array[i+1],
        				byte_array[i]};
        end
//        for (i=0; i<10; i=i+2)
//        	$display("mem_array[%h]= %h\tmem_array[%h]= %h",i,mem_array[i],i+1,mem_array[i+1]);
  		SDRAM_load_memory (VIDEO_SDRAM_Load_Base, ('h20000>>2));	// load "data.txt" into SDRAM
						// 200 select "data.txt"
		end
`else
		begin
		$display("\nInitial CPU Program Data to SDRAM");
        $readmemh("data.txt", byte_array,'h0);
        for (i = 0; i < 'h80000; i = i + 4)	begin
        	mem_array[i>>2] = {	byte_array[i+3],
        				byte_array[i+2],
        				byte_array[i+1],
        				byte_array[i]};
        end
//        for (i=0; i<10; i=i+2)
//        	$display("mem_array[%h]= %h\tmem_array[%h]= %h",i,mem_array[i],i+1,mem_array[i+1]);
  		SDRAM_load_memory (SDRAM_Load_Base, ('h80000>>2));	// load "data.txt" into SDRAM
						// 200 select "data.txt"
		end
//		cpu_alone_sdram; //NO CPU_MODE in M3357 JFR031103
`endif
`endif
end
endtask
//==== The following SDRAM_Initialize1 task is for standby mode test========
//Tod 2004-04-02
task SDRAM_Initialize1;
integer i;
begin
`ifdef NO_CPU
`else
  begin
  $display("\nInitial CPU Program Data to SDRAM");
        $readmemh("data.maps", byte_array,'h0);
        for (i = 0; i < 'h800; i = i + 4) begin
         mem_array[i>>2] = { byte_array[i+3],
            byte_array[i+2],
            byte_array[i+1],
            byte_array[i]};
        end
//        for (i=0; i<10; i=i+2)
//         $display("mem_array[%h]= %h\tmem_array[%h]= %h",i,mem_array[i],i+1,mem_array[i+1]);
    SDRAM_load_memory (SDRAM_Load_Base, ('h800>>2)); // load "data.txt" into SDRAM
      // 200 select "data.txt"
  end
`endif
end
endtask


//======The following sdram initialize task is used to load the application program to sdram
//	yuky, 2001-11-29

task	CTRL_RAMInitialize;	//used to load the nb gpu idct display program to sdram
integer i;
begin
	begin
	$display("\nInitial SYSTEM Control Program Data in SDRAM");
        $readmemh("data_sysctrl.txt", byte_array,'h0);
        for (i = 0; i < 'hf000; i = i + 4)	begin
        	mem_array[i>>2] = {	byte_array[i+3],
        				byte_array[i+2],
        				byte_array[i+1],
        				byte_array[i]};
        end
	SDRAM_load_memory (Sys_Ctrl_Load_Base, ('hf000>>2));
	end
end
endtask

task	NB_GPU_IDCT_DISP_RAMInitialize;	//used to load the nb gpu idct display program to sdram
integer i;
begin
	begin
	$display("\nInitial NB_GPU_IDCT_DISP Program Data in SDRAM");
        $readmemh("data_nb_gpu_idct_disp.txt", byte_array,'h0);
        for (i = 0; i < 'h200000; i = i + 4)	begin
        	mem_array[i>>2] = {	byte_array[i+3],
        				byte_array[i+2],
        				byte_array[i+1],
        				byte_array[i]};
        end
	SDRAM_load_memory (NB_GPU_IDCT_DISP_Load_Base, ('h200000>>2));
	end
end
endtask

task	CPU_RAMInitialize;	//used to load the cpu test program to sdram
integer i;
begin
	begin
	$display("\nInitial CPU Program Data in SDRAM");
        $readmemh("data_cpu.txt", byte_array,'h0);
        for (i = 0; i < 'h200000; i = i + 4)	begin
        	mem_array[i>>2] = {	byte_array[i+3],
        				byte_array[i+2],
        				byte_array[i+1],
        				byte_array[i]};
        end
	SDRAM_load_memory (CPU_Load_Base, ('h200000>>2));
	end
end
endtask

task	AUDIO_RAMInitialize;	//used to load the audio test program to sdram
integer i;
begin
	begin
	$display("\nInitial AUDIO Program Data in SDRAM");
        $readmemh("data_audio.txt", byte_array,'h0);
        for (i = 0; i < 'h200000; i = i + 4)	begin
        	mem_array[i>>2] = {	byte_array[i+3],
        				byte_array[i+2],
        				byte_array[i+1],
        				byte_array[i]};
        end
	SDRAM_load_memory (AUDIO_Load_Base, ('h200000>>2));
	end
end
endtask

task	USB_RAMInitialize;	//used to load the usb test program to sdram
integer i;
begin
	begin
	$display("\nInitial USB Program Data in SDRAM");
        $readmemh("data_usb.txt", byte_array,'h0);
        for (i = 0; i < 'h200000; i = i + 4)	begin
        	mem_array[i>>2] = {	byte_array[i+3],
        				byte_array[i+2],
        				byte_array[i+1],
        				byte_array[i]};
        end
	SDRAM_load_memory (USB_Load_Base, ('h200000>>2));
	end
end
endtask

task	I2C_RAMInitialize;	//used to load the i2c test program to sdram
integer i;
begin
	begin
	$display("\nInitial I2C Program Data in SDRAM");
        $readmemh("data_i2c.txt", byte_array,'h0);
        for (i = 0; i < 'h200000; i = i + 4)	begin
        	mem_array[i>>2] = {	byte_array[i+3],
        				byte_array[i+2],
        				byte_array[i+1],
        				byte_array[i]};
        end
	SDRAM_load_memory (I2C_Load_Base, ('h200000>>2));
	end
end
endtask

task	SPI_RAMInitialize;	//used to load the spi test program to sdram
integer i;
begin
	begin
	$display("\nInitial SPI Program Data in SDRAM");
        $readmemh("data_spi.txt", byte_array,'h0);
        for (i = 0; i < 'h200000; i = i + 4)	begin
        	mem_array[i>>2] = {	byte_array[i+3],
        				byte_array[i+2],
        				byte_array[i+1],
        				byte_array[i]};
        end
	SDRAM_load_memory (SPI_Load_Base, ('h200000>>2));
	end
end
endtask

task	IRC_RAMInitialize;	//used to load the irc test program to sdram
integer i;
begin
	begin
	$display("\nInitial IRC Program Data in SDRAM");
        $readmemh("data_irc.txt", byte_array,'h0);
        for (i = 0; i < 'h200000; i = i + 4)	begin
        	mem_array[i>>2] = {	byte_array[i+3],
        				byte_array[i+2],
        				byte_array[i+1],
        				byte_array[i]};
        end
	SDRAM_load_memory (IRC_Load_Base, ('h200000>>2));
	end
end
endtask

task	SUBQ_RAMInitialize;	//used to load the subq test program to sdram
integer i;
begin
	begin
	$display("\nInitial SUBQ Program Data in SDRAM");
        $readmemh("data_subq.txt", byte_array,'h0);
        for (i = 0; i < 'h200000; i = i + 4)	begin
        	mem_array[i>>2] = {	byte_array[i+3],
        				byte_array[i+2],
        				byte_array[i+1],
        				byte_array[i]};
        end
	SDRAM_load_memory (SUBQ_Load_Base, ('h200000>>2));
	end
end
endtask

task	CD_DSP_RAMInitialize;	//used to load the cd_dsp test program to sdram
integer i;
begin
	begin
	$display("\nInitial CD_DSP Program Data in SDRAM");
        $readmemh("data_cd_dsp.txt", byte_array,'h0);
        for (i = 0; i < 'h200000; i = i + 4)	begin
        	mem_array[i>>2] = {	byte_array[i+3],
        				byte_array[i+2],
        				byte_array[i+1],
        				byte_array[i]};
        end
	SDRAM_load_memory (CD_DSP_Load_Base, ('h200000>>2));
	end
end
endtask

task	MICOM_RAMInitialize;	//used to load the micom test program to sdram
integer i;
begin
	begin
	$display("\nInitial MICOM Program Data in SDRAM");
        $readmemh("data_micom.txt", byte_array,'h0);
        for (i = 0; i < 'h200000; i = i + 4)	begin
        	mem_array[i>>2] = {	byte_array[i+3],
        				byte_array[i+2],
        				byte_array[i+1],
        				byte_array[i]};
        end
	SDRAM_load_memory (MICOM_Load_Base, ('h200000>>2));
	end
end
endtask

// Boot rom
task	CACHE_ROM_Initialize;
begin
`ifdef	NO_CPU
`else
		begin
		$display("\nInitial CPU Boot Code(OPEN CACHE)");
		$readmemh("code_cache.txt",boot_rom.rom_reg,'h0_0000, 'h0_ffff);// From 32'h1FC0_0000, Size 64K,
		end
`endif
end
endtask

// Boot rom
task	NOCACHE_ROM_Initialize;
begin
`ifdef	NO_CPU
`else
		begin
		$display("\nInitial CPU Boot Code(DONT OPEN CACHE)");
		$readmemh("code_nocache.txt",boot_rom.rom_reg,'h0_0000, 'h0_ffff);// From 32'h1FC0_0000, Size 64K,
		end
`endif
end
endtask

// Boot rom
task	SH_ROM_Initialize;
begin
`ifdef	NO_CPU
`else
		begin
		$display("\nInitial CPU Boot Code(SH program)");
		$readmemh("code_sh.txt",boot_rom.rom_reg,'h0_0000, 'h0_ffff);// From 32'h1FC0_0000, Size 64K,
		end
`endif
end
endtask


//	-----------------------------------------------------------------------------------
//below three tasks is for CPU access the IO Register for Memory Interface Conigure
//write configure register:
task	cpu2mem_cfgwr;
input	[3:0]	offset;
input	[3:0]	ben_;
input	[31:0]	data_in;
begin
	`cpu_write1w(CPU_IOBase + 32'h80 + offset, ~ben_, data_in);
end
endtask

//read back configure register:
task	cpu2mem_cfgrdbk;
input	[3:0]	offset;
input	[3:0]	ben_;
output	[31:0]	data_out;
reg[31:0]		data_tmp;
begin
	data_tmp	=	32'hxxxx_xxxx;
	`cpu_readbk1w(CPU_IOBase + 32'h80 + offset, ~ben_, data_tmp);
	data_out	=	data_tmp;
end
endtask

//read and compare configure register:
task	cpu2mem_cfgrd;
input	[3:0]	offset;
input	[3:0]	ben_;
input	[31:0]	data_in;
begin
	`cpu_read1w(CPU_IOBase + 32'h80 + offset, ~ben_, data_in);
end
endtask

//	--------------------------------------------------------------------------------------
//Below Tasks is for CPU Read and Write SDRAM Memory:
//Write One Word:
task	cpu2mem_write1w;
input	[31:0]	mem_addr;
input	[3:0]	wbe;
input	[31:0]	wdata0;
begin
	`ram_cpu_write1w(mem_addr, wbe, wdata0);
end
endtask

//Write Two Words:
task	cpu2mem_write2w;
input	[31:0]	mem_addr;
input	[31:0]	wdata0;
input	[31:0]	wdata1;
begin
	`ram_cpu_write2w(mem_addr, wdata0, wdata1);
end
endtask

//Write Four Words:
task	cpu2mem_write4w;
input	[31:0]	mem_addr;
input	[31:0]	wdata0;
input	[31:0]	wdata1;
input	[31:0]	wdata2;
input	[31:0]	wdata3;
begin
	`ram_cpu_write4w(mem_addr, wdata0, wdata1, wdata2, wdata3);
end
endtask

//Write Eight Words:
task	cpu2mem_write8w;
input	[31:0]	mem_addr;
input	[31:0]	wdata0;
input	[31:0]	wdata1;
input	[31:0]	wdata2;
input	[31:0]	wdata3;
input	[31:0]	wdata4;
input	[31:0]	wdata5;
input	[31:0]	wdata6;
input	[31:0]	wdata7;
begin
	`ram_cpu_write8w(mem_addr, wdata0, wdata1, wdata2, wdata3,
					wdata4, wdata5, wdata6, wdata7);
end
endtask

//Read Back One Word:
task	cpu2mem_readbk1w;
input	[31:0]	mem_addr;
input	[3:0]	rbe;
output	[31:0]	rdata0;
reg		[31:0]	rdata_tmp0;
begin
	rdata_tmp0	=	32'hxxxx_xxxx;
	`ram_cpu_readbk1w(mem_addr, rbe, rdata_tmp0);
	rdata0	=	rdata_tmp0;
end
endtask

//Read Back Two Words:
task	cpu2mem_readbk2w;
input	[31:0]	mem_addr;
output	[31:0]	rdata0;
output	[31:0]	rdata1;
reg		[31:0]	rdata_tmp0;
reg		[31:0]	rdata_tmp1;
begin
	rdata_tmp0	=	32'hxxxx_xxxx;
	rdata_tmp1	=	32'hxxxx_xxxx;
	`ram_cpu_readbk2w(mem_addr, rdata_tmp0, rdata_tmp1);
	rdata0	=	rdata_tmp0;
	rdata1	=	rdata_tmp1;
end
endtask

//Read Back Four Words:
task	cpu2mem_readbk4w;
input	[31:0]	mem_addr;
output	[31:0]	rdata0;
output	[31:0]	rdata1;
output	[31:0]	rdata2;
output	[31:0]	rdata3;
reg		[31:0]	rdata_tmp0;
reg		[31:0]	rdata_tmp1;
reg		[31:0]	rdata_tmp2;
reg		[31:0]	rdata_tmp3;
begin
	rdata_tmp0	=	32'hxxxx_xxxx;
	rdata_tmp1	=	32'hxxxx_xxxx;
	rdata_tmp2	=	32'hxxxx_xxxx;
	rdata_tmp3	=	32'hxxxx_xxxx;
	`ram_cpu_readbk4w(mem_addr, rdata_tmp0, rdata_tmp1, rdata_tmp2, rdata_tmp3);
	rdata0	=	rdata_tmp0;
	rdata1	=	rdata_tmp1;
	rdata2	=	rdata_tmp2;
	rdata3	=	rdata_tmp3;
end
endtask

//Read Back Eight Words:
task	cpu2mem_readbk8w;
input	[31:0]	mem_addr;
output	[31:0]	rdata0;
output	[31:0]	rdata1;
output	[31:0]	rdata2;
output	[31:0]	rdata3;
output	[31:0]	rdata4;
output	[31:0]	rdata5;
output	[31:0]	rdata6;
output	[31:0]	rdata7;
reg		[31:0]	rdata_tmp0;
reg		[31:0]	rdata_tmp1;
reg		[31:0]	rdata_tmp2;
reg		[31:0]	rdata_tmp3;
reg		[31:0]	rdata_tmp4;
reg		[31:0]	rdata_tmp5;
reg		[31:0]	rdata_tmp6;
reg		[31:0]	rdata_tmp7;
begin
	rdata_tmp0	=	32'hxxxx_xxxx;
	rdata_tmp1	=	32'hxxxx_xxxx;
	rdata_tmp2	=	32'hxxxx_xxxx;
	rdata_tmp3	=	32'hxxxx_xxxx;
	rdata_tmp4	=	32'hxxxx_xxxx;
	rdata_tmp5	=	32'hxxxx_xxxx;
	rdata_tmp6	=	32'hxxxx_xxxx;
	rdata_tmp7	=	32'hxxxx_xxxx;
	`ram_cpu_readbk8w(mem_addr, rdata_tmp0, rdata_tmp1, rdata_tmp2, rdata_tmp3,
						rdata_tmp4, rdata_tmp5, rdata_tmp6, rdata_tmp7);
	rdata0	=	rdata_tmp0;
	rdata1	=	rdata_tmp1;
	rdata2	=	rdata_tmp2;
	rdata3	=	rdata_tmp3;
	rdata4	=	rdata_tmp4;
	rdata5	=	rdata_tmp5;
	rdata6	=	rdata_tmp6;
	rdata7	=	rdata_tmp7;
end
endtask

//Read One Word:
task	cpu2mem_read1w;
input	[31:0]	mem_addr;
input	[3:0]	rbe;
input	[31:0]	rdata_exp0;
begin
	`ram_cpu_read1w(mem_addr, rbe, rdata_exp0);
end
endtask

//Read Two Word:
task	cpu2mem_read2w;
input	[31:0]	mem_addr;
input	[31:0]	rdata_exp0;
input	[31:0]	rdata_exp1;
begin
	`ram_cpu_read2w(mem_addr, rdata_exp0, rdata_exp1);
end
endtask

//Read Four Word:
task	cpu2mem_read4w;
input	[31:0]	mem_addr;
input	[31:0]	rdata_exp0;
input	[31:0]	rdata_exp1;
input	[31:0]	rdata_exp2;
input	[31:0]	rdata_exp3;
begin
	`ram_cpu_read4w(mem_addr, rdata_exp0, rdata_exp1, rdata_exp2, rdata_exp3);
end
endtask

//Read Eight Word:
task	cpu2mem_read8w;
input	[31:0]	mem_addr;
input	[31:0]	rdata_exp0;
input	[31:0]	rdata_exp1;
input	[31:0]	rdata_exp2;
input	[31:0]	rdata_exp3;
input	[31:0]	rdata_exp4;
input	[31:0]	rdata_exp5;
input	[31:0]	rdata_exp6;
input	[31:0]	rdata_exp7;
begin
	`ram_cpu_read8w(mem_addr, rdata_exp0, rdata_exp1, rdata_exp2, rdata_exp3,
						rdata_exp4, rdata_exp5, rdata_exp6, rdata_exp7);
end
endtask

//CPU Burst Read and Write SDRAM Memory
task	CPU_Burst_WR_SDRAM;
input	[31:0]	start_waddr;
input	[31:0]	start_wdata;
input	[7:0]	wr_length;

reg		burst_end;
reg	[7:0]	length_cnt;
reg	[31:0]	waddr_tmp;
reg	[1:0]	dword_addr;
reg	[31:0]	wdata_tmp0;
reg	[31:0]	wdata_tmp1;
reg	[31:0]	wdata_tmp2;
reg	[31:0]	wdata_tmp3;

begin
	burst_end	=	1'b0;
	length_cnt	=	wr_length;
	waddr_tmp	=	start_waddr;
	dword_addr	=	waddr_tmp[3:2];
	wdata_tmp0	=	start_wdata;
	wdata_tmp1	=	{wdata_tmp0[30:0],wdata_tmp0[31]};
	wdata_tmp2	=	{wdata_tmp1[30:0],wdata_tmp1[31]};
	wdata_tmp3	=	{wdata_tmp2[30:0],wdata_tmp2[31]};
	while(!burst_end)begin
		if(length_cnt == 8'h0)begin
			cpu2mem_write1w(waddr_tmp, 4'b1111, wdata_tmp0);
			burst_end	=	1'b1;
		end
		else if(length_cnt == 8'h1)begin
			if(dword_addr == 2'h3)begin
				cpu2mem_write1w(waddr_tmp, 4'b1111, wdata_tmp0);
				length_cnt	=	length_cnt - 8'h1;
				waddr_tmp	=	waddr_tmp + 32'h4;
				dword_addr	=	waddr_tmp[3:2];
				wdata_tmp0	=	{wdata_tmp0[30:0],wdata_tmp0[31]};
				wdata_tmp1	=	{wdata_tmp0[30:0],wdata_tmp0[31]};
				wdata_tmp2	=	{wdata_tmp1[30:0],wdata_tmp1[31]};
				wdata_tmp3	=	{wdata_tmp2[30:0],wdata_tmp2[31]};
				burst_end	=	1'b0;
			end
			else	begin
				cpu2mem_write2w(waddr_tmp, wdata_tmp0, wdata_tmp1);
				burst_end	=	1'b1;
			end
		end
		else if(length_cnt == 8'h2)begin
			if(dword_addr == 2'h3)begin
				cpu2mem_write1w(waddr_tmp, 4'b1111, wdata_tmp0);
				length_cnt	=	length_cnt - 8'h1;
				waddr_tmp	=	waddr_tmp + 32'h4;
				dword_addr	=	waddr_tmp[3:2];
				wdata_tmp0	=	{wdata_tmp0[30:0],wdata_tmp0[31]};
				wdata_tmp1	=	{wdata_tmp0[30:0],wdata_tmp0[31]};
				wdata_tmp2	=	{wdata_tmp1[30:0],wdata_tmp1[31]};
				wdata_tmp3	=	{wdata_tmp2[30:0],wdata_tmp2[31]};
				burst_end	=	1'b0;
			end
			else	begin
				cpu2mem_write2w(waddr_tmp, wdata_tmp0, wdata_tmp1);
				length_cnt	=	length_cnt - 8'h2;
				waddr_tmp	=	waddr_tmp + 32'h8;
				dword_addr	=	waddr_tmp[3:2];
				wdata_tmp0	=	{wdata_tmp1[30:0],wdata_tmp1[31]};
				wdata_tmp1	=	{wdata_tmp0[30:0],wdata_tmp0[31]};
				wdata_tmp2	=	{wdata_tmp1[30:0],wdata_tmp1[31]};
				wdata_tmp3	=	{wdata_tmp2[30:0],wdata_tmp2[31]};
				burst_end	=	1'b0;
			end
		end
		else if(length_cnt == 8'h3)begin
			if(dword_addr == 2'h0)begin
				cpu2mem_write4w(waddr_tmp, wdata_tmp0, wdata_tmp1, wdata_tmp2, wdata_tmp3);
				burst_end	=	1'b1;
			end
			else if(dword_addr == 2'h3)begin
				cpu2mem_write1w(waddr_tmp, 4'b1111, wdata_tmp0);
				length_cnt	=	length_cnt - 8'h1;
				waddr_tmp	=	waddr_tmp + 32'h4;
				dword_addr	=	waddr_tmp[3:2];
				wdata_tmp0	=	{wdata_tmp0[30:0],wdata_tmp0[31]};
				wdata_tmp1	=	{wdata_tmp0[30:0],wdata_tmp0[31]};
				wdata_tmp2	=	{wdata_tmp1[30:0],wdata_tmp1[31]};
				wdata_tmp3	=	{wdata_tmp2[30:0],wdata_tmp2[31]};
				burst_end	=	1'b0;
			end
			else	begin
				cpu2mem_write2w(waddr_tmp, wdata_tmp0, wdata_tmp1);
				length_cnt	=	length_cnt - 8'h2;
				waddr_tmp	=	waddr_tmp + 32'h8;
				dword_addr	=	waddr_tmp[3:2];
				wdata_tmp0	=	{wdata_tmp1[30:0],wdata_tmp1[31]};
				wdata_tmp1	=	{wdata_tmp0[30:0],wdata_tmp0[31]};
				wdata_tmp2	=	{wdata_tmp1[30:0],wdata_tmp1[31]};
				wdata_tmp3	=	{wdata_tmp2[30:0],wdata_tmp2[31]};
				burst_end	=	1'b0;
			end
		end
		else	begin
			if(dword_addr == 2'h0)begin
				cpu2mem_write4w(waddr_tmp, wdata_tmp0, wdata_tmp1, wdata_tmp2, wdata_tmp3);
				length_cnt	=	length_cnt - 8'h4;
				waddr_tmp	=	waddr_tmp + 32'h10;
				dword_addr	=	waddr_tmp[3:2];
				wdata_tmp0	=	{wdata_tmp3[30:0],wdata_tmp3[31]};
				wdata_tmp1	=	{wdata_tmp0[30:0],wdata_tmp0[31]};
				wdata_tmp2	=	{wdata_tmp1[30:0],wdata_tmp1[31]};
				wdata_tmp3	=	{wdata_tmp2[30:0],wdata_tmp2[31]};
				burst_end	=	1'b0;
			end
			else if(dword_addr == 2'h3)begin
				cpu2mem_write1w(waddr_tmp, 4'b1111, wdata_tmp0);
				length_cnt	=	length_cnt - 8'h1;
				waddr_tmp	=	waddr_tmp + 32'h4;
				dword_addr	=	waddr_tmp[3:2];
				wdata_tmp0	=	{wdata_tmp0[30:0],wdata_tmp0[31]};
				wdata_tmp1	=	{wdata_tmp0[30:0],wdata_tmp0[31]};
				wdata_tmp2	=	{wdata_tmp1[30:0],wdata_tmp1[31]};
				wdata_tmp3	=	{wdata_tmp2[30:0],wdata_tmp2[31]};
				burst_end	=	1'b0;
			end
			else	begin
				cpu2mem_write2w(waddr_tmp, wdata_tmp0, wdata_tmp1);
				length_cnt	=	length_cnt - 8'h2;
				waddr_tmp	=	waddr_tmp + 32'h8;
				dword_addr	=	waddr_tmp[3:2];
				wdata_tmp0	=	{wdata_tmp1[30:0],wdata_tmp1[31]};
				wdata_tmp1	=	{wdata_tmp0[30:0],wdata_tmp0[31]};
				wdata_tmp2	=	{wdata_tmp1[30:0],wdata_tmp1[31]};
				wdata_tmp3	=	{wdata_tmp2[30:0],wdata_tmp2[31]};
				burst_end	=	1'b0;
			end
		end
	end
end

endtask
//	--------------------------
task	CPU_Burst_RD_SDRAM;
input	[31:0]	start_raddr;
input	[31:0]	start_rdata_exp;
input	[7:0]	rd_length;

reg		burst_end;
reg	[7:0]	length_cnt;
reg	[31:0]	raddr_tmp;
reg	[1:0]	dword_addr;
reg	[31:0]	rdata_exp0;
reg	[31:0]	rdata_exp1;
reg	[31:0]	rdata_exp2;
reg	[31:0]	rdata_exp3;

begin
	burst_end	=	1'b0;
	length_cnt	=	rd_length;
	raddr_tmp	=	start_raddr;
	dword_addr	=	raddr_tmp[3:2];
	rdata_exp0	=	start_rdata_exp;
	rdata_exp1	=	{rdata_exp0[30:0],rdata_exp0[31]};
	rdata_exp2	=	{rdata_exp1[30:0],rdata_exp1[31]};
	rdata_exp3	=	{rdata_exp2[30:0],rdata_exp2[31]};
	while(!burst_end)begin
		if(length_cnt == 8'h0)begin
			cpu2mem_read1w(raddr_tmp, 4'b1111, rdata_exp0);
			burst_end	=	1'b1;
		end
		else if(length_cnt == 8'h1)begin
			if(dword_addr == 2'h3)begin
				cpu2mem_read1w(raddr_tmp, 4'b1111, rdata_exp0);
				length_cnt	=	length_cnt - 8'h1;
				raddr_tmp	=	raddr_tmp + 32'h4;
				dword_addr	=	raddr_tmp[3:2];
				rdata_exp0	=	{rdata_exp0[30:0],rdata_exp0[31]};
				rdata_exp1	=	{rdata_exp0[30:0],rdata_exp0[31]};
				rdata_exp2	=	{rdata_exp1[30:0],rdata_exp1[31]};
				rdata_exp3	=	{rdata_exp2[30:0],rdata_exp2[31]};
				burst_end	=	1'b0;
			end
			else	begin
				cpu2mem_read2w(raddr_tmp, rdata_exp0, rdata_exp1);
				burst_end	=	1'b1;
			end
		end
		else if(length_cnt == 8'h2)begin
			if(dword_addr == 2'h3)begin
				cpu2mem_read1w(raddr_tmp, 4'b1111, rdata_exp0);
				length_cnt	=	length_cnt - 8'h1;
				raddr_tmp	=	raddr_tmp + 32'h4;
				dword_addr	=	raddr_tmp[3:2];
				rdata_exp0	=	{rdata_exp0[30:0],rdata_exp0[31]};
				rdata_exp1	=	{rdata_exp0[30:0],rdata_exp0[31]};
				rdata_exp2	=	{rdata_exp1[30:0],rdata_exp1[31]};
				rdata_exp3	=	{rdata_exp2[30:0],rdata_exp2[31]};
				burst_end	=	1'b0;
			end
			else	begin
				cpu2mem_read2w(raddr_tmp, rdata_exp0, rdata_exp1);
				length_cnt	=	length_cnt - 8'h2;
				raddr_tmp	=	raddr_tmp + 32'h8;
				dword_addr	=	raddr_tmp[3:2];
				rdata_exp0	=	{rdata_exp1[30:0],rdata_exp1[31]};
				rdata_exp1	=	{rdata_exp0[30:0],rdata_exp0[31]};
				rdata_exp2	=	{rdata_exp1[30:0],rdata_exp1[31]};
				rdata_exp3	=	{rdata_exp2[30:0],rdata_exp2[31]};
				burst_end	=	1'b0;
			end
		end
		else if(length_cnt == 8'h3)begin
			if(dword_addr == 2'h0)begin
				cpu2mem_read4w(raddr_tmp, rdata_exp0, rdata_exp1, rdata_exp2, rdata_exp3);
				burst_end	=	1'b1;
			end
			else if(dword_addr == 2'h3)begin
				cpu2mem_read1w(raddr_tmp, 4'b1111, rdata_exp0);
				length_cnt	=	length_cnt - 8'h1;
				raddr_tmp	=	raddr_tmp + 32'h4;
				dword_addr	=	raddr_tmp[3:2];
				rdata_exp0	=	{rdata_exp0[30:0],rdata_exp0[31]};
				rdata_exp1	=	{rdata_exp0[30:0],rdata_exp0[31]};
				rdata_exp2	=	{rdata_exp1[30:0],rdata_exp1[31]};
				rdata_exp3	=	{rdata_exp2[30:0],rdata_exp2[31]};
				burst_end	=	1'b0;
			end
			else	begin
				cpu2mem_read2w(raddr_tmp, rdata_exp0, rdata_exp1);
				length_cnt	=	length_cnt - 8'h2;
				raddr_tmp	=	raddr_tmp + 32'h8;
				dword_addr	=	raddr_tmp[3:2];
				rdata_exp0	=	{rdata_exp1[30:0],rdata_exp1[31]};
				rdata_exp1	=	{rdata_exp0[30:0],rdata_exp0[31]};
				rdata_exp2	=	{rdata_exp1[30:0],rdata_exp1[31]};
				rdata_exp3	=	{rdata_exp2[30:0],rdata_exp2[31]};
				burst_end	=	1'b0;
			end
		end
		else	begin
			if(dword_addr == 2'h0)begin
				cpu2mem_read4w(raddr_tmp, rdata_exp0, rdata_exp1, rdata_exp2, rdata_exp3);
				length_cnt	=	length_cnt - 8'h4;
				raddr_tmp	=	raddr_tmp + 32'h10;
				dword_addr	=	raddr_tmp[3:2];
				rdata_exp0	=	{rdata_exp3[30:0],rdata_exp3[31]};
				rdata_exp1	=	{rdata_exp0[30:0],rdata_exp0[31]};
				rdata_exp2	=	{rdata_exp1[30:0],rdata_exp1[31]};
				rdata_exp3	=	{rdata_exp2[30:0],rdata_exp2[31]};
				burst_end	=	1'b0;
			end
			else if(dword_addr == 2'h3)begin
				cpu2mem_read1w(raddr_tmp, 4'b1111, rdata_exp0);
				length_cnt	=	length_cnt - 8'h1;
				raddr_tmp	=	raddr_tmp + 32'h4;
				dword_addr	=	raddr_tmp[3:2];
				rdata_exp0	=	{rdata_exp0[30:0],rdata_exp0[31]};
				rdata_exp1	=	{rdata_exp0[30:0],rdata_exp0[31]};
				rdata_exp2	=	{rdata_exp1[30:0],rdata_exp1[31]};
				rdata_exp3	=	{rdata_exp2[30:0],rdata_exp2[31]};
				burst_end	=	1'b0;
			end
			else	begin
				cpu2mem_read2w(raddr_tmp, rdata_exp0, rdata_exp1);
				length_cnt	=	length_cnt - 8'h2;
				raddr_tmp	=	raddr_tmp + 32'h8;
				dword_addr	=	raddr_tmp[3:2];
				rdata_exp0	=	{rdata_exp1[30:0],rdata_exp1[31]};
				rdata_exp1	=	{rdata_exp0[30:0],rdata_exp0[31]};
				rdata_exp2	=	{rdata_exp1[30:0],rdata_exp1[31]};
				rdata_exp3	=	{rdata_exp2[30:0],rdata_exp2[31]};
				burst_end	=	1'b0;
			end
		end
	end
end

endtask


task	CPU_Burst_RDBK_SDRAM;
input	[31:0]	start_raddr;
input	[31:0]	start_rdata_exp;
input	[7:0]	rd_length;

reg		burst_end;
reg	[7:0]	length_cnt;
reg	[31:0]	raddr_tmp;
reg	[1:0]	dword_addr;
reg	[31:0]	rdata_exp0;
reg	[31:0]	rdata_exp1;
reg	[31:0]	rdata_exp2;
reg	[31:0]	rdata_exp3;

begin
	burst_end	=	1'b0;
	length_cnt	=	rd_length;
	raddr_tmp	=	start_raddr;
	dword_addr	=	raddr_tmp[3:2];
	while(!burst_end)begin
		if(length_cnt == 8'h0)begin
			cpu2mem_readbk1w(raddr_tmp, 4'b1111, rdata_exp0);
			burst_end	=	1'b1;
		end
		else if(length_cnt == 8'h1)begin
			if(dword_addr == 2'h3)begin
				cpu2mem_readbk1w(raddr_tmp, 4'b1111, rdata_exp0);
				length_cnt	=	length_cnt - 8'h1;
				raddr_tmp	=	raddr_tmp + 32'h4;
				dword_addr	=	raddr_tmp[3:2];
				burst_end	=	1'b0;
			end
			else	begin
				cpu2mem_readbk2w(raddr_tmp, rdata_exp0, rdata_exp1);
				burst_end	=	1'b1;
			end
		end
		else if(length_cnt == 8'h2)begin
			if(dword_addr == 2'h3)begin
				cpu2mem_readbk1w(raddr_tmp, 4'b1111, rdata_exp0);
				length_cnt	=	length_cnt - 8'h1;
				raddr_tmp	=	raddr_tmp + 32'h4;
				dword_addr	=	raddr_tmp[3:2];
				burst_end	=	1'b0;
			end
			else	begin
				cpu2mem_readbk2w(raddr_tmp, rdata_exp0, rdata_exp1);
				length_cnt	=	length_cnt - 8'h2;
				raddr_tmp	=	raddr_tmp + 32'h8;
				dword_addr	=	raddr_tmp[3:2];
				burst_end	=	1'b0;
			end
		end
		else if(length_cnt == 8'h3)begin
			if(dword_addr == 2'h0)begin
				cpu2mem_readbk4w(raddr_tmp, rdata_exp0, rdata_exp1, rdata_exp2, rdata_exp3);
				burst_end	=	1'b1;
			end
			else if(dword_addr == 2'h3)begin
				cpu2mem_readbk1w(raddr_tmp, 4'b1111, rdata_exp0);
				length_cnt	=	length_cnt - 8'h1;
				raddr_tmp	=	raddr_tmp + 32'h4;
				dword_addr	=	raddr_tmp[3:2];
				burst_end	=	1'b0;
			end
			else	begin
				cpu2mem_readbk2w(raddr_tmp, rdata_exp0, rdata_exp1);
				length_cnt	=	length_cnt - 8'h2;
				raddr_tmp	=	raddr_tmp + 32'h8;
				dword_addr	=	raddr_tmp[3:2];
				burst_end	=	1'b0;
			end
		end
		else	begin
			if(dword_addr == 2'h0)begin
				cpu2mem_readbk4w(raddr_tmp, rdata_exp0, rdata_exp1, rdata_exp2, rdata_exp3);
				length_cnt	=	length_cnt - 8'h4;
				raddr_tmp	=	raddr_tmp + 32'h10;
				dword_addr	=	raddr_tmp[3:2];
				burst_end	=	1'b0;
			end
			else if(dword_addr == 2'h3)begin
				cpu2mem_readbk1w(raddr_tmp, 4'b1111, rdata_exp0);
				length_cnt	=	length_cnt - 8'h1;
				raddr_tmp	=	raddr_tmp + 32'h4;
				dword_addr	=	raddr_tmp[3:2];
				burst_end	=	1'b0;
			end
			else	begin
				cpu2mem_readbk2w(raddr_tmp, rdata_exp0, rdata_exp1);
				length_cnt	=	length_cnt - 8'h2;
				raddr_tmp	=	raddr_tmp + 32'h8;
				dword_addr	=	raddr_tmp[3:2];
				burst_end	=	1'b0;
			end
		end
	end
end

endtask

//	------------------------------------------------------------------------------

//========== the gpio[9:3] interrupt processor tasks =======
task gpio_int3_processor;
reg	[31:0]	ext_gpio_int_info;
reg	[7:0]	ext_int_priority;
begin
`ifdef NO_CPU
	$display("Start to Clear the GPIO3 external interrupt");
	int_cpu_readbk1w (32'h1800_0040, 4'b0001, ext_int_priority);
	//enable flash access and write enable
	int_cpu_write1w	(CPU_IOBase + 32'h90, 4'b0001, 32'h01);//include access enable
	int_cpu_readbk1w(32'h1fff_f0e4, 4'b1111,ext_gpio_int_info);
	int_cpu_write1w	(32'h1fff_f0e4, 4'b1111,{ext_gpio_int_info[31:4],~ext_int_priority[1],ext_gpio_int_info[2:0]});
	int_cpu_write1w	(32'h1fff_f0e0, 4'b0001,32'h0000_0033);
`endif
end
endtask

task gpio_int4_processor;
reg	[31:0]	ext_gpio_int_info;
reg	[7:0]	ext_int_priority;
begin
`ifdef NO_CPU
	$display("Start to Clear the GPIO4 external interrupt");
	int_cpu_readbk1w (32'h1800_0040, 4'b0001, ext_int_priority);
	//enable flash access and write enable
	int_cpu_write1w	(CPU_IOBase + 32'h90, 4'b0001, 32'h01);//include access enable
	int_cpu_readbk1w(32'h1fff_f0e4, 4'b1111,ext_gpio_int_info);
	int_cpu_write1w	(32'h1fff_f0e4, 4'b1111,{ext_gpio_int_info[31:5],~ext_int_priority[2],ext_gpio_int_info[3:0]});
	int_cpu_write1w	(32'h1fff_f0e0, 4'b0001,32'h0000_0033);
`endif
end
endtask

task gpio_int5_processor;
reg	[31:0]	ext_gpio_int_info;
reg	[7:0]	ext_int_priority;
begin
`ifdef NO_CPU
	$display("Start to Clear the GPIO5 external interrupt");
	int_cpu_readbk1w (32'h1800_0040, 4'b0001, ext_int_priority);
	//enable flash access and write enable
	int_cpu_write1w	(CPU_IOBase + 32'h90, 4'b0001, 32'h01);//include access enable
	int_cpu_readbk1w(32'h1fff_f0e4, 4'b1111,ext_gpio_int_info);
	int_cpu_write1w	(32'h1fff_f0e4, 4'b1111,{ext_gpio_int_info[31:6],~ext_int_priority[3],ext_gpio_int_info[4:0]});
	int_cpu_write1w	(32'h1fff_f0e0, 4'b0001,32'h0000_0033);
`endif
end
endtask

task gpio_int6_processor;
reg	[31:0]	ext_gpio_int_info;
reg	[7:0]	ext_int_priority;
begin
`ifdef NO_CPU
	$display("Start to Clear the GPIO6 external interrupt");
	int_cpu_readbk1w (32'h1800_0040, 4'b0001, ext_int_priority);
	//enable flash access and write enable
	int_cpu_write1w	(CPU_IOBase + 32'h90, 4'b0001, 32'h01);//include access enable
	int_cpu_readbk1w(32'h1fff_f0e4, 4'b1111,ext_gpio_int_info);
	int_cpu_write1w	(32'h1fff_f0e4, 4'b1111,{ext_gpio_int_info[31:7],~ext_int_priority[4],ext_gpio_int_info[5:0]});
	int_cpu_write1w	(32'h1fff_f0e0, 4'b0001,32'h0000_0033);
`endif
end
endtask

task gpio_int7_processor;
reg	[31:0]	ext_gpio_int_info;
reg	[7:0]	ext_int_priority;
begin
`ifdef NO_CPU
	$display("Start to Clear the GPIO7 external interrupt");
	int_cpu_readbk1w (32'h1800_0040, 4'b0001, ext_int_priority);
	//enable flash access and write enable
	int_cpu_write1w	(CPU_IOBase + 32'h90, 4'b0001, 32'h01);//include access enable
	int_cpu_readbk1w(32'h1fff_f0e4, 4'b1111,ext_gpio_int_info);
	int_cpu_write1w	(32'h1fff_f0e4, 4'b1111,{ext_gpio_int_info[31:8],~ext_int_priority[5],ext_gpio_int_info[6:0]});
	int_cpu_write1w	(32'h1fff_f0e0, 4'b0001,32'h0000_0033);
`endif
end
endtask

task gpio_int8_processor;
reg	[31:0]	ext_gpio_int_info;
reg	[7:0]	ext_int_priority;
begin
`ifdef NO_CPU
	$display("Start to Clear the GPIO8 external interrupt");
	int_cpu_readbk1w (32'h1800_0040, 4'b0001, ext_int_priority);
	//enable flash access and write enable
	int_cpu_write1w	(CPU_IOBase + 32'h90, 4'b0001, 32'h01);//include access enable
	int_cpu_readbk1w(32'h1fff_f0e4, 4'b1111,ext_gpio_int_info);
	int_cpu_write1w	(32'h1fff_f0e4, 4'b1111,{ext_gpio_int_info[31:9],~ext_int_priority[6],ext_gpio_int_info[7:0]});
	int_cpu_write1w	(32'h1fff_f0e0, 4'b0001,32'h0000_0033);
`endif
end
endtask

task gpio_int9_processor;
reg	[31:0]	ext_gpio_int_info;
reg	[7:0]	ext_int_priority;
begin
`ifdef NO_CPU
	$display("Start to Clear the GPIO9 external interrupt");
	int_cpu_readbk1w (32'h1800_0040, 4'b0001, ext_int_priority);
	//enable flash access and write enable
	int_cpu_write1w	(CPU_IOBase + 32'h90, 4'b0001, 32'h01);//include access enable
	int_cpu_readbk1w(32'h1fff_f0e4, 4'b1111,ext_gpio_int_info);
	int_cpu_write1w	(32'h1fff_f0e4, 4'b1111,{ext_gpio_int_info[31:10],~ext_int_priority[7],ext_gpio_int_info[8:0]});
	int_cpu_write1w	(32'h1fff_f0e0, 4'b0001,32'h0000_0033);
`endif
end
endtask

//	======================================================================================
//	--------------------------------------------------------------------------
//			The pattern tasks for device behaviour model used to test
//			SDRAM memory  interface:
//	--------------------------------------------------------------------------
`ifdef	CPU_NET
`else

//	--------------------------------------------------------------------------
//For NO Video Core:
`ifdef	NO_VIDEO
`define	VSB_BH_PATH		top.CHIP.CORE.T2_CHIPSET.VSB_DEVICE_BH

//VSB Write One Double Words:
task	vsb_wr_1dw;
input[27:0]	wr_addr;
input[31:0]	wr_data0;
input[3:0]	wr_wbe0;

begin
	`VSB_BH_PATH.SDRAM_WR_1DW(wr_addr, wr_data0, wr_wbe0);
end
endtask

//VSB Write Two Double Words:
task	vsb_wr_2dw;
input[27:0]	wr_addr;
input[31:0]	wr_data0;
input[3:0]	wr_wbe0;
input[31:0]	wr_data1;
input[3:0]	wr_wbe1;

begin
	`VSB_BH_PATH.SDRAM_WR_2DW(wr_addr, wr_data0, wr_wbe0,
							 wr_data1, wr_wbe1);
end
endtask

//VSB Write Three Double Words:
task	vsb_wr_3dw;
input[27:0]	wr_addr;
input[31:0]	wr_data0;
input[3:0]	wr_wbe0;
input[31:0]	wr_data1;
input[3:0]	wr_wbe1;
input[31:0]	wr_data2;
input[3:0]	wr_wbe2;

begin
	`VSB_BH_PATH.SDRAM_WR_3DW(wr_addr, wr_data0, wr_wbe0,
							 wr_data1, wr_wbe1,
							 wr_data2, wr_wbe2);
end
endtask

//VSB Write Four Double Words:
task	vsb_wr_4dw;
input[27:0]	wr_addr;
input[31:0]	wr_data0;
input[3:0]	wr_wbe0;
input[31:0]	wr_data1;
input[3:0]	wr_wbe1;
input[31:0]	wr_data2;
input[3:0]	wr_wbe2;
input[31:0]	wr_data3;
input[3:0]	wr_wbe3;

begin
	`VSB_BH_PATH.SDRAM_WR_4DW(wr_addr, wr_data0, wr_wbe0,
							 wr_data1, wr_wbe1,
							 wr_data2, wr_wbe2,
							 wr_data3, wr_wbe3);
end
endtask

//VSB Read One Double Words:
task	vsb_rd_1dw;
input[27:0]	rd_addr;
input[31:0]	exp_data0;

reg[31:0]		rd_tmp0;
reg[31:0]		exp_tmp0;

begin
	rd_tmp0	=	32'hxxxx_xxxx;
	exp_tmp0	=	exp_data0;

	`VSB_BH_PATH.SDRAM_RD_1DW(rd_addr, rd_tmp0);
	//Compare the 1st read data0:
	if(!(rd_tmp0 == exp_tmp0))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data0 = %h\tThe Expected Read Data0 = %h",rd_tmp0,exp_tmp0);
	end
end
endtask

//VSB Read Two Double Words:
task	vsb_rd_2dw;
input[27:0]	rd_addr;
input[31:0]	exp_data0;
input[31:0]	exp_data1;

reg[31:0]		rd_tmp0;
reg[31:0]		rd_tmp1;
reg[31:0]		exp_tmp0;
reg[31:0]		exp_tmp1;

begin
	rd_tmp0	=	32'hxxxx_xxxx;
	rd_tmp1	=	32'hxxxx_xxxx;
	exp_tmp0	=	exp_data0;
	exp_tmp1	=	exp_data1;

	`VSB_BH_PATH.SDRAM_RD_2DW(rd_addr, rd_tmp0, rd_tmp1);
	//Compare the 1st read data0:
	if(!(rd_tmp0 == exp_tmp0))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data0 = %h\tThe Expected Read Data0 = %h",rd_tmp0,exp_tmp0);
	end
	//Compare the 2nd read data1:
	if(!(rd_tmp1 == exp_tmp1))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data1 = %h\tThe Expected Read Data1 = %h",rd_tmp1,exp_tmp1);
	end
end
endtask

//VSB Read Three Double Words:
task	vsb_rd_3dw;
input[27:0]	rd_addr;
input[31:0]	exp_data0;
input[31:0]	exp_data1;
input[31:0]	exp_data2;

reg[31:0]		rd_tmp0;
reg[31:0]		rd_tmp1;
reg[31:0]		rd_tmp2;
reg[31:0]		exp_tmp0;
reg[31:0]		exp_tmp1;
reg[31:0]		exp_tmp2;

begin
	rd_tmp0	=	32'hxxxx_xxxx;
	rd_tmp1	=	32'hxxxx_xxxx;
	rd_tmp2	=	32'hxxxx_xxxx;
	exp_tmp0	=	exp_data0;
	exp_tmp1	=	exp_data1;
	exp_tmp2	=	exp_data2;

	`VSB_BH_PATH.SDRAM_RD_3DW(rd_addr, rd_tmp0, rd_tmp1, rd_tmp2);
	//Compare the 1st read data0:
	if(!(rd_tmp0 == exp_tmp0))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data0 = %h\tThe Expected Read Data0 = %h",rd_tmp0,exp_tmp0);
	end
	//Compare the 2nd read data1:
	if(!(rd_tmp1 == exp_tmp1))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data1 = %h\tThe Expected Read Data1 = %h",rd_tmp1,exp_tmp1);
	end
	//Compare the 3rd read data2:
	if(!(rd_tmp2 == exp_tmp2))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data2 = %h\tThe Expected Read Data2 = %h",rd_tmp2,exp_tmp2);
	end
end
endtask

//VSB Read Four Double Words:
task	vsb_rd_4dw;
input[27:0]	rd_addr;
input[31:0]	exp_data0;
input[31:0]	exp_data1;
input[31:0]	exp_data2;
input[31:0]	exp_data3;

reg[31:0]		rd_tmp0;
reg[31:0]		rd_tmp1;
reg[31:0]		rd_tmp2;
reg[31:0]		rd_tmp3;
reg[31:0]		exp_tmp0;
reg[31:0]		exp_tmp1;
reg[31:0]		exp_tmp2;
reg[31:0]		exp_tmp3;

begin
	rd_tmp0	=	32'hxxxx_xxxx;
	rd_tmp1	=	32'hxxxx_xxxx;
	rd_tmp2	=	32'hxxxx_xxxx;
	rd_tmp3	=	32'hxxxx_xxxx;
	exp_tmp0	=	exp_data0;
	exp_tmp1	=	exp_data1;
	exp_tmp2	=	exp_data2;
	exp_tmp3	=	exp_data3;

	`VSB_BH_PATH.SDRAM_RD_4DW(rd_addr, rd_tmp0, rd_tmp1, rd_tmp2, rd_tmp3);
	//Compare the 1st read data0:
	if(!(rd_tmp0 == exp_tmp0))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data0 = %h\tThe Expected Read Data0 = %h",rd_tmp0,exp_tmp0);
	end
	//Compare the 2nd read data1:
	if(!(rd_tmp1 == exp_tmp1))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data1 = %h\tThe Expected Read Data1 = %h",rd_tmp1,exp_tmp1);
	end
	//Compare the 3rd read data2:
	if(!(rd_tmp2 == exp_tmp2))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data2 = %h\tThe Expected Read Data2 = %h",rd_tmp2,exp_tmp2);
	end
	//Compare the 4th read data3:
	if(!(rd_tmp3 == exp_tmp3))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data3 = %h\tThe Expected Read Data3 = %h",rd_tmp3,exp_tmp3);
	end
end
endtask

`endif	//	end of `ifdef NO_VIDEO


//	--------------------------------------------------------------------------
//For NO VIDEO Core:
`ifdef	NO_VIDEO
`define	VIDEO_BH_PATH		top.CHIP.CORE.T2_CHIPSET.VIDEO_DEVICE_BH

//VIDEO Write One Double Words:
task	video_wr_1dw;
input[27:0]	wr_addr;
input[31:0]	wr_data0;
input[3:0]	wr_wbe0;

begin
	`VIDEO_BH_PATH.SDRAM_WR_1DW(wr_addr, wr_data0, wr_wbe0);
end
endtask

//VIDEO Write Two Double Words:
task	video_wr_2dw;
input[27:0]	wr_addr;
input[31:0]	wr_data0;
input[3:0]	wr_wbe0;
input[31:0]	wr_data1;
input[3:0]	wr_wbe1;

begin
	`VIDEO_BH_PATH.SDRAM_WR_2DW(wr_addr, wr_data0, wr_wbe0,
							 wr_data1, wr_wbe1);
end
endtask

//VIDEO Write Three Double Words:
task	video_wr_3dw;
input[27:0]	wr_addr;
input[31:0]	wr_data0;
input[3:0]	wr_wbe0;
input[31:0]	wr_data1;
input[3:0]	wr_wbe1;
input[31:0]	wr_data2;
input[3:0]	wr_wbe2;

begin
	`VIDEO_BH_PATH.SDRAM_WR_3DW(wr_addr, wr_data0, wr_wbe0,
							 wr_data1, wr_wbe1,
							 wr_data2, wr_wbe2);
end
endtask

//VIDEO Write Four Double Words:
task	video_wr_4dw;
input[27:0]	wr_addr;
input[31:0]	wr_data0;
input[3:0]	wr_wbe0;
input[31:0]	wr_data1;
input[3:0]	wr_wbe1;
input[31:0]	wr_data2;
input[3:0]	wr_wbe2;
input[31:0]	wr_data3;
input[3:0]	wr_wbe3;

begin
	`VIDEO_BH_PATH.SDRAM_WR_4DW(wr_addr, wr_data0, wr_wbe0,
							 wr_data1, wr_wbe1,
							 wr_data2, wr_wbe2,
							 wr_data3, wr_wbe3);
end
endtask

//VIDEO Read One Double Words:
task	video_rd_1dw;
input[27:0]	rd_addr;
input[31:0]	exp_data0;

reg[31:0]		rd_tmp0;
reg[31:0]		exp_tmp0;

begin
	rd_tmp0	=	32'hxxxx_xxxx;
	exp_tmp0	=	exp_data0;

	`VIDEO_BH_PATH.SDRAM_RD_1DW(rd_addr, rd_tmp0);
	//Compare the 1st read data0:
	if(!(rd_tmp0 == exp_tmp0))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data0 = %h\tThe Expected Read Data0 = %h",rd_tmp0,exp_tmp0);
	end
end
endtask

//VIDEO Read Two Double Words:
task	video_rd_2dw;
input[27:0]	rd_addr;
input[31:0]	exp_data0;
input[31:0]	exp_data1;

reg[31:0]		rd_tmp0;
reg[31:0]		rd_tmp1;
reg[31:0]		exp_tmp0;
reg[31:0]		exp_tmp1;

begin
	rd_tmp0	=	32'hxxxx_xxxx;
	rd_tmp1	=	32'hxxxx_xxxx;
	exp_tmp0	=	exp_data0;
	exp_tmp1	=	exp_data1;

	`VIDEO_BH_PATH.SDRAM_RD_2DW(rd_addr, rd_tmp0, rd_tmp1);
	//Compare the 1st read data0:
	if(!(rd_tmp0 == exp_tmp0))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data0 = %h\tThe Expected Read Data0 = %h",rd_tmp0,exp_tmp0);
	end
	//Compare the 2nd read data1:
	if(!(rd_tmp1 == exp_tmp1))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data1 = %h\tThe Expected Read Data1 = %h",rd_tmp1,exp_tmp1);
	end
end
endtask

//VIDEO Read Three Double Words:
task	video_rd_3dw;
input[27:0]	rd_addr;
input[31:0]	exp_data0;
input[31:0]	exp_data1;
input[31:0]	exp_data2;

reg[31:0]		rd_tmp0;
reg[31:0]		rd_tmp1;
reg[31:0]		rd_tmp2;
reg[31:0]		exp_tmp0;
reg[31:0]		exp_tmp1;
reg[31:0]		exp_tmp2;

begin
	rd_tmp0	=	32'hxxxx_xxxx;
	rd_tmp1	=	32'hxxxx_xxxx;
	rd_tmp2	=	32'hxxxx_xxxx;
	exp_tmp0	=	exp_data0;
	exp_tmp1	=	exp_data1;
	exp_tmp2	=	exp_data2;

	`VIDEO_BH_PATH.SDRAM_RD_3DW(rd_addr, rd_tmp0, rd_tmp1, rd_tmp2);
	//Compare the 1st read data0:
	if(!(rd_tmp0 == exp_tmp0))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data0 = %h\tThe Expected Read Data0 = %h",rd_tmp0,exp_tmp0);
	end
	//Compare the 2nd read data1:
	if(!(rd_tmp1 == exp_tmp1))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data1 = %h\tThe Expected Read Data1 = %h",rd_tmp1,exp_tmp1);
	end
	//Compare the 3rd read data2:
	if(!(rd_tmp2 == exp_tmp2))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data2 = %h\tThe Expected Read Data2 = %h",rd_tmp2,exp_tmp2);
	end
end
endtask

//VIDEO Read Four Double Words:
task	video_rd_4dw;
input[27:0]	rd_addr;
input[31:0]	exp_data0;
input[31:0]	exp_data1;
input[31:0]	exp_data2;
input[31:0]	exp_data3;

reg[31:0]		rd_tmp0;
reg[31:0]		rd_tmp1;
reg[31:0]		rd_tmp2;
reg[31:0]		rd_tmp3;
reg[31:0]		exp_tmp0;
reg[31:0]		exp_tmp1;
reg[31:0]		exp_tmp2;
reg[31:0]		exp_tmp3;

begin
	rd_tmp0	=	32'hxxxx_xxxx;
	rd_tmp1	=	32'hxxxx_xxxx;
	rd_tmp2	=	32'hxxxx_xxxx;
	rd_tmp3	=	32'hxxxx_xxxx;
	exp_tmp0	=	exp_data0;
	exp_tmp1	=	exp_data1;
	exp_tmp2	=	exp_data2;
	exp_tmp3	=	exp_data3;

	`VIDEO_BH_PATH.SDRAM_RD_4DW(rd_addr, rd_tmp0, rd_tmp1, rd_tmp2, rd_tmp3);
	//Compare the 1st read data0:
	if(!(rd_tmp0 == exp_tmp0))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data0 = %h\tThe Expected Read Data0 = %h",rd_tmp0,exp_tmp0);
	end
	//Compare the 2nd read data1:
	if(!(rd_tmp1 == exp_tmp1))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data1 = %h\tThe Expected Read Data1 = %h",rd_tmp1,exp_tmp1);
	end
	//Compare the 3rd read data2:
	if(!(rd_tmp2 == exp_tmp2))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data2 = %h\tThe Expected Read Data2 = %h",rd_tmp2,exp_tmp2);
	end
	//Compare the 4th read data3:
	if(!(rd_tmp3 == exp_tmp3))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data3 = %h\tThe Expected Read Data3 = %h",rd_tmp3,exp_tmp3);
	end
end
endtask

`endif	//	end of `ifdef NO_VIDEO

//For NO VIDEO IN :
`ifdef	NO_VIDEO
`define	VIN_BH_PATH		top.CHIP.CORE.T2_CHIPSET.VIN_DEVICE_BH

//VIN Write One Double Words:
task	vin_wr_1dw;
input[27:0]	wr_addr;
input[31:0]	wr_data0;
input[3:0]	wr_wbe0;

begin
	`VIN_BH_PATH.SDRAM_WR_1DW(wr_addr, wr_data0, wr_wbe0);
end
endtask

//VIN Write Two Double Words:
task	vin_wr_2dw;
input[27:0]	wr_addr;
input[31:0]	wr_data0;
input[3:0]	wr_wbe0;
input[31:0]	wr_data1;
input[3:0]	wr_wbe1;

begin
	`VIN_BH_PATH.SDRAM_WR_2DW(wr_addr, wr_data0, wr_wbe0,
							 wr_data1, wr_wbe1);
end
endtask

//VIN Write Three Double Words:
task	vin_wr_3dw;
input[27:0]	wr_addr;
input[31:0]	wr_data0;
input[3:0]	wr_wbe0;
input[31:0]	wr_data1;
input[3:0]	wr_wbe1;
input[31:0]	wr_data2;
input[3:0]	wr_wbe2;

begin
	`VIN_BH_PATH.SDRAM_WR_3DW(wr_addr, wr_data0, wr_wbe0,
							 wr_data1, wr_wbe1,
							 wr_data2, wr_wbe2);
end
endtask

//VIN Write Four Double Words:
task	vin_wr_4dw;
input[27:0]	wr_addr;
input[31:0]	wr_data0;
input[3:0]	wr_wbe0;
input[31:0]	wr_data1;
input[3:0]	wr_wbe1;
input[31:0]	wr_data2;
input[3:0]	wr_wbe2;
input[31:0]	wr_data3;
input[3:0]	wr_wbe3;

begin
	`VIN_BH_PATH.SDRAM_WR_4DW(wr_addr, wr_data0, wr_wbe0,
							 wr_data1, wr_wbe1,
							 wr_data2, wr_wbe2,
							 wr_data3, wr_wbe3);
end
endtask

//VIN Read One Double Words:
task	vin_rd_1dw;
input[27:0]	rd_addr;
input[31:0]	exp_data0;

reg[31:0]		rd_tmp0;
reg[31:0]		exp_tmp0;

begin
	rd_tmp0	=	32'hxxxx_xxxx;
	exp_tmp0	=	exp_data0;

	`VIN_BH_PATH.SDRAM_RD_1DW(rd_addr, rd_tmp0);
	//Compare the 1st read data0:
	if(!(rd_tmp0 == exp_tmp0))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data0 = %h\tThe Expected Read Data0 = %h",rd_tmp0,exp_tmp0);
	end
end
endtask

//VIN Read Two Double Words:
task	vin_rd_2dw;
input[27:0]	rd_addr;
input[31:0]	exp_data0;
input[31:0]	exp_data1;

reg[31:0]		rd_tmp0;
reg[31:0]		rd_tmp1;
reg[31:0]		exp_tmp0;
reg[31:0]		exp_tmp1;

begin
	rd_tmp0	=	32'hxxxx_xxxx;
	rd_tmp1	=	32'hxxxx_xxxx;
	exp_tmp0	=	exp_data0;
	exp_tmp1	=	exp_data1;

	`VIN_BH_PATH.SDRAM_RD_2DW(rd_addr, rd_tmp0, rd_tmp1);
	//Compare the 1st read data0:
	if(!(rd_tmp0 == exp_tmp0))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data0 = %h\tThe Expected Read Data0 = %h",rd_tmp0,exp_tmp0);
	end
	//Compare the 2nd read data1:
	if(!(rd_tmp1 == exp_tmp1))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data1 = %h\tThe Expected Read Data1 = %h",rd_tmp1,exp_tmp1);
	end
end
endtask

//VIN Read Three Double Words:
task	vin_rd_3dw;
input[27:0]	rd_addr;
input[31:0]	exp_data0;
input[31:0]	exp_data1;
input[31:0]	exp_data2;

reg[31:0]		rd_tmp0;
reg[31:0]		rd_tmp1;
reg[31:0]		rd_tmp2;
reg[31:0]		exp_tmp0;
reg[31:0]		exp_tmp1;
reg[31:0]		exp_tmp2;

begin
	rd_tmp0	=	32'hxxxx_xxxx;
	rd_tmp1	=	32'hxxxx_xxxx;
	rd_tmp2	=	32'hxxxx_xxxx;
	exp_tmp0	=	exp_data0;
	exp_tmp1	=	exp_data1;
	exp_tmp2	=	exp_data2;

	`VIN_BH_PATH.SDRAM_RD_3DW(rd_addr, rd_tmp0, rd_tmp1, rd_tmp2);
	//Compare the 1st read data0:
	if(!(rd_tmp0 == exp_tmp0))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data0 = %h\tThe Expected Read Data0 = %h",rd_tmp0,exp_tmp0);
	end
	//Compare the 2nd read data1:
	if(!(rd_tmp1 == exp_tmp1))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data1 = %h\tThe Expected Read Data1 = %h",rd_tmp1,exp_tmp1);
	end
	//Compare the 3rd read data2:
	if(!(rd_tmp2 == exp_tmp2))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data2 = %h\tThe Expected Read Data2 = %h",rd_tmp2,exp_tmp2);
	end
end
endtask

//VIN Read Four Double Words:
task	vin_rd_4dw;
input[27:0]	rd_addr;
input[31:0]	exp_data0;
input[31:0]	exp_data1;
input[31:0]	exp_data2;
input[31:0]	exp_data3;

reg[31:0]		rd_tmp0;
reg[31:0]		rd_tmp1;
reg[31:0]		rd_tmp2;
reg[31:0]		rd_tmp3;
reg[31:0]		exp_tmp0;
reg[31:0]		exp_tmp1;
reg[31:0]		exp_tmp2;
reg[31:0]		exp_tmp3;

begin
	rd_tmp0	=	32'hxxxx_xxxx;
	rd_tmp1	=	32'hxxxx_xxxx;
	rd_tmp2	=	32'hxxxx_xxxx;
	rd_tmp3	=	32'hxxxx_xxxx;
	exp_tmp0	=	exp_data0;
	exp_tmp1	=	exp_data1;
	exp_tmp2	=	exp_data2;
	exp_tmp3	=	exp_data3;

	`VIN_BH_PATH.SDRAM_RD_4DW(rd_addr, rd_tmp0, rd_tmp1, rd_tmp2, rd_tmp3);
	//Compare the 1st read data0:
	if(!(rd_tmp0 == exp_tmp0))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data0 = %h\tThe Expected Read Data0 = %h",rd_tmp0,exp_tmp0);
	end
	//Compare the 2nd read data1:
	if(!(rd_tmp1 == exp_tmp1))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data1 = %h\tThe Expected Read Data1 = %h",rd_tmp1,exp_tmp1);
	end
	//Compare the 3rd read data2:
	if(!(rd_tmp2 == exp_tmp2))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data2 = %h\tThe Expected Read Data2 = %h",rd_tmp2,exp_tmp2);
	end
	//Compare the 4th read data3:
	if(!(rd_tmp3 == exp_tmp3))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data3 = %h\tThe Expected Read Data3 = %h",rd_tmp3,exp_tmp3);
	end
end
endtask


`endif	// end of `ifdef NO_VIDEO
/*
//	--------------------------------------------------------------------------
//For NO GPU Core:
`ifdef	NO_GPU
`define	GPU_BH_PATH		top.CHIP.CORE.T2_CHIPSET.GPU_DEVICE_BH

//GPU Write One Double Words:
task	gpu_wr_1dw;
input[27:0]	wr_addr;
input[31:0]	wr_data0;
input[3:0]	wr_wbe0;

begin
	`GPU_BH_PATH.SDRAM_WR_1DW(wr_addr, wr_data0, wr_wbe0);
end
endtask

//GPU Write Two Double Words:
task	gpu_wr_2dw;
input[27:0]	wr_addr;
input[31:0]	wr_data0;
input[3:0]	wr_wbe0;
input[31:0]	wr_data1;
input[3:0]	wr_wbe1;

begin
	`GPU_BH_PATH.SDRAM_WR_2DW(wr_addr, wr_data0, wr_wbe0,
							 wr_data1, wr_wbe1);
end
endtask

//GPU Write Three Double Words:
task	gpu_wr_3dw;
input[27:0]	wr_addr;
input[31:0]	wr_data0;
input[3:0]	wr_wbe0;
input[31:0]	wr_data1;
input[3:0]	wr_wbe1;
input[31:0]	wr_data2;
input[3:0]	wr_wbe2;

begin
	`GPU_BH_PATH.SDRAM_WR_3DW(wr_addr, wr_data0, wr_wbe0,
							 wr_data1, wr_wbe1,
							 wr_data2, wr_wbe2);
end
endtask

//GPU Write Four Double Words:
task	gpu_wr_4dw;
input[27:0]	wr_addr;
input[31:0]	wr_data0;
input[3:0]	wr_wbe0;
input[31:0]	wr_data1;
input[3:0]	wr_wbe1;
input[31:0]	wr_data2;
input[3:0]	wr_wbe2;
input[31:0]	wr_data3;
input[3:0]	wr_wbe3;

begin
	`GPU_BH_PATH.SDRAM_WR_4DW(wr_addr, wr_data0, wr_wbe0,
							 wr_data1, wr_wbe1,
							 wr_data2, wr_wbe2,
							 wr_data3, wr_wbe3);
end
endtask

//GPU Read One Double Words:
task	gpu_rd_1dw;
input[27:0]	rd_addr;
input[31:0]	exp_data0;

reg[31:0]		rd_tmp0;
reg[31:0]		exp_tmp0;

begin
	rd_tmp0	=	32'hxxxx_xxxx;
	exp_tmp0	=	exp_data0;

	`GPU_BH_PATH.SDRAM_RD_1DW(rd_addr, rd_tmp0);
	//Compare the 1st read data0:
	if(!(rd_tmp0 == exp_tmp0))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data0 = %h\tThe Expected Read Data0 = %h",rd_tmp0,exp_tmp0);
	end
end
endtask

//GPU Read Two Double Words:
task	gpu_rd_2dw;
input[27:0]	rd_addr;
input[31:0]	exp_data0;
input[31:0]	exp_data1;

reg[31:0]		rd_tmp0;
reg[31:0]		rd_tmp1;
reg[31:0]		exp_tmp0;
reg[31:0]		exp_tmp1;

begin
	rd_tmp0	=	32'hxxxx_xxxx;
	rd_tmp1	=	32'hxxxx_xxxx;
	exp_tmp0	=	exp_data0;
	exp_tmp1	=	exp_data1;

	`GPU_BH_PATH.SDRAM_RD_2DW(rd_addr, rd_tmp0, rd_tmp1);
	//Compare the 1st read data0:
	if(!(rd_tmp0 == exp_tmp0))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data0 = %h\tThe Expected Read Data0 = %h",rd_tmp0,exp_tmp0);
	end
	//Compare the 2nd read data1:
	if(!(rd_tmp1 == exp_tmp1))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data1 = %h\tThe Expected Read Data1 = %h",rd_tmp1,exp_tmp1);
	end
end
endtask

//GPU Read Three Double Words:
task	gpu_rd_3dw;
input[27:0]	rd_addr;
input[31:0]	exp_data0;
input[31:0]	exp_data1;
input[31:0]	exp_data2;

reg[31:0]		rd_tmp0;
reg[31:0]		rd_tmp1;
reg[31:0]		rd_tmp2;
reg[31:0]		exp_tmp0;
reg[31:0]		exp_tmp1;
reg[31:0]		exp_tmp2;

begin
	rd_tmp0	=	32'hxxxx_xxxx;
	rd_tmp1	=	32'hxxxx_xxxx;
	rd_tmp2	=	32'hxxxx_xxxx;
	exp_tmp0	=	exp_data0;
	exp_tmp1	=	exp_data1;
	exp_tmp2	=	exp_data2;

	`GPU_BH_PATH.SDRAM_RD_3DW(rd_addr, rd_tmp0, rd_tmp1, rd_tmp2);
	//Compare the 1st read data0:
	if(!(rd_tmp0 == exp_tmp0))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data0 = %h\tThe Expected Read Data0 = %h",rd_tmp0,exp_tmp0);
	end
	//Compare the 2nd read data1:
	if(!(rd_tmp1 == exp_tmp1))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data1 = %h\tThe Expected Read Data1 = %h",rd_tmp1,exp_tmp1);
	end
	//Compare the 3rd read data2:
	if(!(rd_tmp2 == exp_tmp2))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data2 = %h\tThe Expected Read Data2 = %h",rd_tmp2,exp_tmp2);
	end
end
endtask

//GPU Read Four Double Words:
task	gpu_rd_4dw;
input[27:0]	rd_addr;
input[31:0]	exp_data0;
input[31:0]	exp_data1;
input[31:0]	exp_data2;
input[31:0]	exp_data3;

reg[31:0]		rd_tmp0;
reg[31:0]		rd_tmp1;
reg[31:0]		rd_tmp2;
reg[31:0]		rd_tmp3;
reg[31:0]		exp_tmp0;
reg[31:0]		exp_tmp1;
reg[31:0]		exp_tmp2;
reg[31:0]		exp_tmp3;

begin
	rd_tmp0	=	32'hxxxx_xxxx;
	rd_tmp1	=	32'hxxxx_xxxx;
	rd_tmp2	=	32'hxxxx_xxxx;
	rd_tmp3	=	32'hxxxx_xxxx;
	exp_tmp0	=	exp_data0;
	exp_tmp1	=	exp_data1;
	exp_tmp2	=	exp_data2;
	exp_tmp3	=	exp_data3;

	`GPU_BH_PATH.SDRAM_RD_4DW(rd_addr, rd_tmp0, rd_tmp1, rd_tmp2, rd_tmp3);
	//Compare the 1st read data0:
	if(!(rd_tmp0 == exp_tmp0))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data0 = %h\tThe Expected Read Data0 = %h",rd_tmp0,exp_tmp0);
	end
	//Compare the 2nd read data1:
	if(!(rd_tmp1 == exp_tmp1))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data1 = %h\tThe Expected Read Data1 = %h",rd_tmp1,exp_tmp1);
	end
	//Compare the 3rd read data2:
	if(!(rd_tmp2 == exp_tmp2))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data2 = %h\tThe Expected Read Data2 = %h",rd_tmp2,exp_tmp2);
	end
	//Compare the 4th read data3:
	if(!(rd_tmp3 == exp_tmp3))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data3 = %h\tThe Expected Read Data3 = %h",rd_tmp3,exp_tmp3);
	end
end
endtask

`endif	//	end of `ifdef NO_GPU
*/
//	--------------------------------------------------------------------------
//For NO Audio Core:
`ifdef	NO_DSP
`define	DSP_BH_PATH		top.CHIP.CORE.T2_CHIPSET.DSP_DEVICE_BH

//DSP Write One Double Words:
task	dsp_wr_1dw;
input[27:0]	wr_addr;
input[31:0]	wr_data0;
input[3:0]	wr_wbe0;

begin
	`DSP_BH_PATH.SDRAM_WR_1DW(wr_addr, wr_data0, wr_wbe0);
end
endtask

//DSP Write Two Double Words:
task	dsp_wr_2dw;
input[27:0]	wr_addr;
input[31:0]	wr_data0;
input[3:0]	wr_wbe0;
input[31:0]	wr_data1;
input[3:0]	wr_wbe1;

begin
	`DSP_BH_PATH.SDRAM_WR_2DW(wr_addr, wr_data0, wr_wbe0,
							 wr_data1, wr_wbe1);
end
endtask

//DSP Write Three Double Words:
task	dsp_wr_3dw;
input[27:0]	wr_addr;
input[31:0]	wr_data0;
input[3:0]	wr_wbe0;
input[31:0]	wr_data1;
input[3:0]	wr_wbe1;
input[31:0]	wr_data2;
input[3:0]	wr_wbe2;

begin
	`DSP_BH_PATH.SDRAM_WR_3DW(wr_addr, wr_data0, wr_wbe0,
							 wr_data1, wr_wbe1,
							 wr_data2, wr_wbe2);
end
endtask

//DSP Write Four Double Words:
task	dsp_wr_4dw;
input[27:0]	wr_addr;
input[31:0]	wr_data0;
input[3:0]	wr_wbe0;
input[31:0]	wr_data1;
input[3:0]	wr_wbe1;
input[31:0]	wr_data2;
input[3:0]	wr_wbe2;
input[31:0]	wr_data3;
input[3:0]	wr_wbe3;

begin
	`DSP_BH_PATH.SDRAM_WR_4DW(wr_addr, wr_data0, wr_wbe0,
							 wr_data1, wr_wbe1,
							 wr_data2, wr_wbe2,
							 wr_data3, wr_wbe3);
end
endtask

//DSP Read One Double Words:
task	dsp_rd_1dw;
input[27:0]	rd_addr;
input[31:0]	exp_data0;

reg[31:0]		rd_tmp0;
reg[31:0]		exp_tmp0;

begin
	rd_tmp0	=	32'hxxxx_xxxx;
	exp_tmp0	=	exp_data0;

	`DSP_BH_PATH.SDRAM_RD_1DW(rd_addr, rd_tmp0);
	//Compare the 1st read data0:
	if(!(rd_tmp0 == exp_tmp0))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data0 = %h\tThe Expected Read Data0 = %h",rd_tmp0,exp_tmp0);
	end
end
endtask

//DSP Read Two Double Words:
task	dsp_rd_2dw;
input[27:0]	rd_addr;
input[31:0]	exp_data0;
input[31:0]	exp_data1;

reg[31:0]		rd_tmp0;
reg[31:0]		rd_tmp1;
reg[31:0]		exp_tmp0;
reg[31:0]		exp_tmp1;

begin
	rd_tmp0	=	32'hxxxx_xxxx;
	rd_tmp1	=	32'hxxxx_xxxx;
	exp_tmp0	=	exp_data0;
	exp_tmp1	=	exp_data1;

	`DSP_BH_PATH.SDRAM_RD_2DW(rd_addr, rd_tmp0, rd_tmp1);
	//Compare the 1st read data0:
	if(!(rd_tmp0 == exp_tmp0))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data0 = %h\tThe Expected Read Data0 = %h",rd_tmp0,exp_tmp0);
	end
	//Compare the 2nd read data1:
	if(!(rd_tmp1 == exp_tmp1))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data1 = %h\tThe Expected Read Data1 = %h",rd_tmp1,exp_tmp1);
	end
end
endtask

//DSP Read Three Double Words:
task	dsp_rd_3dw;
input[27:0]	rd_addr;
input[31:0]	exp_data0;
input[31:0]	exp_data1;
input[31:0]	exp_data2;

reg[31:0]		rd_tmp0;
reg[31:0]		rd_tmp1;
reg[31:0]		rd_tmp2;
reg[31:0]		exp_tmp0;
reg[31:0]		exp_tmp1;
reg[31:0]		exp_tmp2;

begin
	rd_tmp0	=	32'hxxxx_xxxx;
	rd_tmp1	=	32'hxxxx_xxxx;
	rd_tmp2	=	32'hxxxx_xxxx;
	exp_tmp0	=	exp_data0;
	exp_tmp1	=	exp_data1;
	exp_tmp2	=	exp_data2;

	`DSP_BH_PATH.SDRAM_RD_3DW(rd_addr, rd_tmp0, rd_tmp1, rd_tmp2);
	//Compare the 1st read data0:
	if(!(rd_tmp0 == exp_tmp0))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data0 = %h\tThe Expected Read Data0 = %h",rd_tmp0,exp_tmp0);
	end
	//Compare the 2nd read data1:
	if(!(rd_tmp1 == exp_tmp1))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data1 = %h\tThe Expected Read Data1 = %h",rd_tmp1,exp_tmp1);
	end
	//Compare the 3rd read data2:
	if(!(rd_tmp2 == exp_tmp2))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data2 = %h\tThe Expected Read Data2 = %h",rd_tmp2,exp_tmp2);
	end
end
endtask

//DSP Read Four Double Words:
task	dsp_rd_4dw;
input[27:0]	rd_addr;
input[31:0]	exp_data0;
input[31:0]	exp_data1;
input[31:0]	exp_data2;
input[31:0]	exp_data3;

reg[31:0]		rd_tmp0;
reg[31:0]		rd_tmp1;
reg[31:0]		rd_tmp2;
reg[31:0]		rd_tmp3;
reg[31:0]		exp_tmp0;
reg[31:0]		exp_tmp1;
reg[31:0]		exp_tmp2;
reg[31:0]		exp_tmp3;

begin
	rd_tmp0	=	32'hxxxx_xxxx;
	rd_tmp1	=	32'hxxxx_xxxx;
	rd_tmp2	=	32'hxxxx_xxxx;
	rd_tmp3	=	32'hxxxx_xxxx;
	exp_tmp0	=	exp_data0;
	exp_tmp1	=	exp_data1;
	exp_tmp2	=	exp_data2;
	exp_tmp3	=	exp_data3;

	`DSP_BH_PATH.SDRAM_RD_4DW(rd_addr, rd_tmp0, rd_tmp1, rd_tmp2, rd_tmp3);
	//Compare the 1st read data0:
	if(!(rd_tmp0 == exp_tmp0))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data0 = %h\tThe Expected Read Data0 = %h",rd_tmp0,exp_tmp0);
	end
	//Compare the 2nd read data1:
	if(!(rd_tmp1 == exp_tmp1))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data1 = %h\tThe Expected Read Data1 = %h",rd_tmp1,exp_tmp1);
	end
	//Compare the 3rd read data2:
	if(!(rd_tmp2 == exp_tmp2))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data2 = %h\tThe Expected Read Data2 = %h",rd_tmp2,exp_tmp2);
	end
	//Compare the 4th read data3:
	if(!(rd_tmp3 == exp_tmp3))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data3 = %h\tThe Expected Read Data3 = %h",rd_tmp3,exp_tmp3);
	end
end
endtask

`endif	//	end of `ifdef NO_AUDIO

`ifdef	NO_AUDIO
`define	AUDIO_BH_PATH		top.CHIP.CORE.T2_CHIPSET.AUDIO_DEVICE_BH

//AUDIO Write One Double Words:
task	audio_wr_1dw;
input[27:0]	wr_addr;
input[31:0]	wr_data0;
input[3:0]	wr_wbe0;

begin
	`AUDIO_BH_PATH.SDRAM_WR_1DW(wr_addr, wr_data0, wr_wbe0);
end
endtask

//AUDIO Write Two Double Words:
task	audio_wr_2dw;
input[27:0]	wr_addr;
input[31:0]	wr_data0;
input[3:0]	wr_wbe0;
input[31:0]	wr_data1;
input[3:0]	wr_wbe1;

begin
	`AUDIO_BH_PATH.SDRAM_WR_2DW(wr_addr, wr_data0, wr_wbe0,
							 wr_data1, wr_wbe1);
end
endtask

//AUDIO Write Three Double Words:
task	audio_wr_3dw;
input[27:0]	wr_addr;
input[31:0]	wr_data0;
input[3:0]	wr_wbe0;
input[31:0]	wr_data1;
input[3:0]	wr_wbe1;
input[31:0]	wr_data2;
input[3:0]	wr_wbe2;

begin
	`AUDIO_BH_PATH.SDRAM_WR_3DW(wr_addr, wr_data0, wr_wbe0,
							 wr_data1, wr_wbe1,
							 wr_data2, wr_wbe2);
end
endtask

//AUDIO Write Four Double Words:
task	audio_wr_4dw;
input[27:0]	wr_addr;
input[31:0]	wr_data0;
input[3:0]	wr_wbe0;
input[31:0]	wr_data1;
input[3:0]	wr_wbe1;
input[31:0]	wr_data2;
input[3:0]	wr_wbe2;
input[31:0]	wr_data3;
input[3:0]	wr_wbe3;

begin
	`AUDIO_BH_PATH.SDRAM_WR_4DW(wr_addr, wr_data0, wr_wbe0,
							 wr_data1, wr_wbe1,
							 wr_data2, wr_wbe2,
							 wr_data3, wr_wbe3);
end
endtask

//AUDIO Read One Double Words:
task	audio_rd_1dw;
input[27:0]	rd_addr;
input[31:0]	exp_data0;

reg[31:0]		rd_tmp0;
reg[31:0]		exp_tmp0;

begin
	rd_tmp0	=	32'hxxxx_xxxx;
	exp_tmp0	=	exp_data0;

	`AUDIO_BH_PATH.SDRAM_RD_1DW(rd_addr, rd_tmp0);
	//Compare the 1st read data0:
	if(!(rd_tmp0 == exp_tmp0))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data0 = %h\tThe Expected Read Data0 = %h",rd_tmp0,exp_tmp0);
	end
end
endtask

//AUDIO Read Two Double Words:
task	audio_rd_2dw;
input[27:0]	rd_addr;
input[31:0]	exp_data0;
input[31:0]	exp_data1;

reg[31:0]		rd_tmp0;
reg[31:0]		rd_tmp1;
reg[31:0]		exp_tmp0;
reg[31:0]		exp_tmp1;

begin
	rd_tmp0	=	32'hxxxx_xxxx;
	rd_tmp1	=	32'hxxxx_xxxx;
	exp_tmp0	=	exp_data0;
	exp_tmp1	=	exp_data1;

	`AUDIO_BH_PATH.SDRAM_RD_2DW(rd_addr, rd_tmp0, rd_tmp1);
	//Compare the 1st read data0:
	if(!(rd_tmp0 == exp_tmp0))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data0 = %h\tThe Expected Read Data0 = %h",rd_tmp0,exp_tmp0);
	end
	//Compare the 2nd read data1:
	if(!(rd_tmp1 == exp_tmp1))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data1 = %h\tThe Expected Read Data1 = %h",rd_tmp1,exp_tmp1);
	end
end
endtask

//AUDIO Read Three Double Words:
task	audio_rd_3dw;
input[27:0]	rd_addr;
input[31:0]	exp_data0;
input[31:0]	exp_data1;
input[31:0]	exp_data2;

reg[31:0]		rd_tmp0;
reg[31:0]		rd_tmp1;
reg[31:0]		rd_tmp2;
reg[31:0]		exp_tmp0;
reg[31:0]		exp_tmp1;
reg[31:0]		exp_tmp2;

begin
	rd_tmp0	=	32'hxxxx_xxxx;
	rd_tmp1	=	32'hxxxx_xxxx;
	rd_tmp2	=	32'hxxxx_xxxx;
	exp_tmp0	=	exp_data0;
	exp_tmp1	=	exp_data1;
	exp_tmp2	=	exp_data2;

	`AUDIO_BH_PATH.SDRAM_RD_3DW(rd_addr, rd_tmp0, rd_tmp1, rd_tmp2);
	//Compare the 1st read data0:
	if(!(rd_tmp0 == exp_tmp0))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data0 = %h\tThe Expected Read Data0 = %h",rd_tmp0,exp_tmp0);
	end
	//Compare the 2nd read data1:
	if(!(rd_tmp1 == exp_tmp1))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data1 = %h\tThe Expected Read Data1 = %h",rd_tmp1,exp_tmp1);
	end
	//Compare the 3rd read data2:
	if(!(rd_tmp2 == exp_tmp2))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data2 = %h\tThe Expected Read Data2 = %h",rd_tmp2,exp_tmp2);
	end
end
endtask

//AUDIO Read Four Double Words:
task	audio_rd_4dw;
input[27:0]	rd_addr;
input[31:0]	exp_data0;
input[31:0]	exp_data1;
input[31:0]	exp_data2;
input[31:0]	exp_data3;

reg[31:0]		rd_tmp0;
reg[31:0]		rd_tmp1;
reg[31:0]		rd_tmp2;
reg[31:0]		rd_tmp3;
reg[31:0]		exp_tmp0;
reg[31:0]		exp_tmp1;
reg[31:0]		exp_tmp2;
reg[31:0]		exp_tmp3;

begin
	rd_tmp0	=	32'hxxxx_xxxx;
	rd_tmp1	=	32'hxxxx_xxxx;
	rd_tmp2	=	32'hxxxx_xxxx;
	rd_tmp3	=	32'hxxxx_xxxx;
	exp_tmp0	=	exp_data0;
	exp_tmp1	=	exp_data1;
	exp_tmp2	=	exp_data2;
	exp_tmp3	=	exp_data3;

	`AUDIO_BH_PATH.SDRAM_RD_4DW(rd_addr, rd_tmp0, rd_tmp1, rd_tmp2, rd_tmp3);
	//Compare the 1st read data0:
	if(!(rd_tmp0 == exp_tmp0))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data0 = %h\tThe Expected Read Data0 = %h",rd_tmp0,exp_tmp0);
	end
	//Compare the 2nd read data1:
	if(!(rd_tmp1 == exp_tmp1))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data1 = %h\tThe Expected Read Data1 = %h",rd_tmp1,exp_tmp1);
	end
	//Compare the 3rd read data2:
	if(!(rd_tmp2 == exp_tmp2))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data2 = %h\tThe Expected Read Data2 = %h",rd_tmp2,exp_tmp2);
	end
	//Compare the 4th read data3:
	if(!(rd_tmp3 == exp_tmp3))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data3 = %h\tThe Expected Read Data3 = %h",rd_tmp3,exp_tmp3);
	end
end
endtask

`endif	//	end of `ifdef NO_AUDIO

//	--------------------------------------------------------------------------
//For NO South Bridge:
`ifdef	NO_SB
`define	SB_BH_PATH		top.CHIP.CORE.T2_CHIPSET.SB_DEVICE_BH

//SB Write One Double Words:
task	sb_wr_1dw;
input[27:0]	wr_addr;
input[31:0]	wr_data0;
input[3:0]	wr_wbe0;

begin
	`SB_BH_PATH.SDRAM_WR_1DW(wr_addr, wr_data0, wr_wbe0);
end
endtask

//SB Write Two Double Words:
task	sb_wr_2dw;
input[27:0]	wr_addr;
input[31:0]	wr_data0;
input[3:0]	wr_wbe0;
input[31:0]	wr_data1;
input[3:0]	wr_wbe1;

begin
	`SB_BH_PATH.SDRAM_WR_2DW(wr_addr, wr_data0, wr_wbe0,
							 wr_data1, wr_wbe1);
end
endtask

//SB Write Three Double Words:
task	sb_wr_3dw;
input[27:0]	wr_addr;
input[31:0]	wr_data0;
input[3:0]	wr_wbe0;
input[31:0]	wr_data1;
input[3:0]	wr_wbe1;
input[31:0]	wr_data2;
input[3:0]	wr_wbe2;

begin
	`SB_BH_PATH.SDRAM_WR_3DW(wr_addr, wr_data0, wr_wbe0,
							 wr_data1, wr_wbe1,
							 wr_data2, wr_wbe2);
end
endtask

//SB Write Four Double Words:
task	sb_wr_4dw;
input[27:0]	wr_addr;
input[31:0]	wr_data0;
input[3:0]	wr_wbe0;
input[31:0]	wr_data1;
input[3:0]	wr_wbe1;
input[31:0]	wr_data2;
input[3:0]	wr_wbe2;
input[31:0]	wr_data3;
input[3:0]	wr_wbe3;

begin
	`SB_BH_PATH.SDRAM_WR_4DW(wr_addr, wr_data0, wr_wbe0,
							 wr_data1, wr_wbe1,
							 wr_data2, wr_wbe2,
							 wr_data3, wr_wbe3);
end
endtask

//SB Read One Double Words:
task	sb_rd_1dw;
input[27:0]	rd_addr;
input[31:0]	exp_data0;

reg[31:0]		rd_tmp0;
reg[31:0]		exp_tmp0;

begin
	rd_tmp0	=	32'hxxxx_xxxx;
	exp_tmp0	=	exp_data0;

	`SB_BH_PATH.SDRAM_RD_1DW(rd_addr, rd_tmp0);
	//Compare the 1st read data0:
	if(!(rd_tmp0 == exp_tmp0))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data0 = %h\tThe Expected Read Data0 = %h",rd_tmp0,exp_tmp0);
	end
end
endtask

//SB Read Two Double Words:
task	sb_rd_2dw;
input[27:0]	rd_addr;
input[31:0]	exp_data0;
input[31:0]	exp_data1;

reg[31:0]		rd_tmp0;
reg[31:0]		rd_tmp1;
reg[31:0]		exp_tmp0;
reg[31:0]		exp_tmp1;

begin
	rd_tmp0	=	32'hxxxx_xxxx;
	rd_tmp1	=	32'hxxxx_xxxx;
	exp_tmp0	=	exp_data0;
	exp_tmp1	=	exp_data1;

	`SB_BH_PATH.SDRAM_RD_2DW(rd_addr, rd_tmp0, rd_tmp1);
	//Compare the 1st read data0:
	if(!(rd_tmp0 == exp_tmp0))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data0 = %h\tThe Expected Read Data0 = %h",rd_tmp0,exp_tmp0);
	end
	//Compare the 2nd read data1:
	if(!(rd_tmp1 == exp_tmp1))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data1 = %h\tThe Expected Read Data1 = %h",rd_tmp1,exp_tmp1);
	end
end
endtask

//SB Read Three Double Words:
task	sb_rd_3dw;
input[27:0]	rd_addr;
input[31:0]	exp_data0;
input[31:0]	exp_data1;
input[31:0]	exp_data2;

reg[31:0]		rd_tmp0;
reg[31:0]		rd_tmp1;
reg[31:0]		rd_tmp2;
reg[31:0]		exp_tmp0;
reg[31:0]		exp_tmp1;
reg[31:0]		exp_tmp2;

begin
	rd_tmp0	=	32'hxxxx_xxxx;
	rd_tmp1	=	32'hxxxx_xxxx;
	rd_tmp2	=	32'hxxxx_xxxx;
	exp_tmp0	=	exp_data0;
	exp_tmp1	=	exp_data1;
	exp_tmp2	=	exp_data2;

	`SB_BH_PATH.SDRAM_RD_3DW(rd_addr, rd_tmp0, rd_tmp1, rd_tmp2);
	//Compare the 1st read data0:
	if(!(rd_tmp0 == exp_tmp0))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data0 = %h\tThe Expected Read Data0 = %h",rd_tmp0,exp_tmp0);
	end
	//Compare the 2nd read data1:
	if(!(rd_tmp1 == exp_tmp1))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data1 = %h\tThe Expected Read Data1 = %h",rd_tmp1,exp_tmp1);
	end
	//Compare the 3rd read data2:
	if(!(rd_tmp2 == exp_tmp2))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data2 = %h\tThe Expected Read Data2 = %h",rd_tmp2,exp_tmp2);
	end
end
endtask

//SB Read Four Double Words:
task	sb_rd_4dw;
input[27:0]	rd_addr;
input[31:0]	exp_data0;
input[31:0]	exp_data1;
input[31:0]	exp_data2;
input[31:0]	exp_data3;

reg[31:0]		rd_tmp0;
reg[31:0]		rd_tmp1;
reg[31:0]		rd_tmp2;
reg[31:0]		rd_tmp3;
reg[31:0]		exp_tmp0;
reg[31:0]		exp_tmp1;
reg[31:0]		exp_tmp2;
reg[31:0]		exp_tmp3;

begin
	rd_tmp0	=	32'hxxxx_xxxx;
	rd_tmp1	=	32'hxxxx_xxxx;
	rd_tmp2	=	32'hxxxx_xxxx;
	rd_tmp3	=	32'hxxxx_xxxx;
	exp_tmp0	=	exp_data0;
	exp_tmp1	=	exp_data1;
	exp_tmp2	=	exp_data2;
	exp_tmp3	=	exp_data3;

	`SB_BH_PATH.SDRAM_RD_4DW(rd_addr, rd_tmp0, rd_tmp1, rd_tmp2, rd_tmp3);
	//Compare the 1st read data0:
	if(!(rd_tmp0 == exp_tmp0))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data0 = %h\tThe Expected Read Data0 = %h",rd_tmp0,exp_tmp0);
	end
	//Compare the 2nd read data1:
	if(!(rd_tmp1 == exp_tmp1))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data1 = %h\tThe Expected Read Data1 = %h",rd_tmp1,exp_tmp1);
	end
	//Compare the 3rd read data2:
	if(!(rd_tmp2 == exp_tmp2))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data2 = %h\tThe Expected Read Data2 = %h",rd_tmp2,exp_tmp2);
	end
	//Compare the 4th read data3:
	if(!(rd_tmp3 == exp_tmp3))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data3 = %h\tThe Expected Read Data3 = %h",rd_tmp3,exp_tmp3);
	end
end
endtask

`endif	//	end of `ifdef NO_SB

//	-------------------------------------------------------------------------------------
//For NO Display Engine:
`ifdef	NO_DISPLAY
//`define	DISP_BH_PATH		top.CHIP.CORE.T2_CHIPSET.DISP_DEVICE_BH

//Display Engine Read Four Double Words:
//task	disp_bh_frame_buffer_rd;
//input[27:0]	rd_addr_start;
//input[31:0]	exp_data_start;

//begin

//	`DISP_BH_PATH.DISP_Device_BH_Frame_Buffer_RD(rd_addr_start, exp_data_start);

//end
//endtask  // Snow Yi, 2003.08.28

`endif	//	end of `ifdef NO_DISPLAY



//	--------------------------------------------------------------------------
//For NO IDE Core:
`ifdef	NO_IDE
`define	IDE_BH_PATH		top.CHIP.CORE.T2_CHIPSET.IDE_DEVICE_BH

//IDE Write One Double Words:
task	ide_wr_1dw;
input[27:0]	wr_addr;
input[31:0]	wr_data0;
input[3:0]	wr_wbe0;

begin
	`IDE_BH_PATH.SDRAM_WR_1DW(wr_addr, wr_data0, wr_wbe0);
end
endtask

//IDE Write Two Double Words:
task	ide_wr_2dw;
input[27:0]	wr_addr;
input[31:0]	wr_data0;
input[3:0]	wr_wbe0;
input[31:0]	wr_data1;
input[3:0]	wr_wbe1;

begin
	`IDE_BH_PATH.SDRAM_WR_2DW(wr_addr, wr_data0, wr_wbe0,
							 wr_data1, wr_wbe1);
end
endtask

//IDE Write Three Double Words:
task	ide_wr_3dw;
input[27:0]	wr_addr;
input[31:0]	wr_data0;
input[3:0]	wr_wbe0;
input[31:0]	wr_data1;
input[3:0]	wr_wbe1;
input[31:0]	wr_data2;
input[3:0]	wr_wbe2;

begin
	`IDE_BH_PATH.SDRAM_WR_3DW(wr_addr, wr_data0, wr_wbe0,
							 wr_data1, wr_wbe1,
							 wr_data2, wr_wbe2);
end
endtask

//IDE Write Four Double Words:
task	ide_wr_4dw;
input[27:0]	wr_addr;
input[31:0]	wr_data0;
input[3:0]	wr_wbe0;
input[31:0]	wr_data1;
input[3:0]	wr_wbe1;
input[31:0]	wr_data2;
input[3:0]	wr_wbe2;
input[31:0]	wr_data3;
input[3:0]	wr_wbe3;

begin
	`IDE_BH_PATH.SDRAM_WR_4DW(wr_addr, wr_data0, wr_wbe0,
							 wr_data1, wr_wbe1,
							 wr_data2, wr_wbe2,
							 wr_data3, wr_wbe3);
end
endtask

//IDE Read One Double Words:
task	ide_rd_1dw;
input[27:0]	rd_addr;
input[31:0]	exp_data0;

reg[31:0]		rd_tmp0;
reg[31:0]		exp_tmp0;

begin
	rd_tmp0	=	32'hxxxx_xxxx;
	exp_tmp0	=	exp_data0;

	`IDE_BH_PATH.SDRAM_RD_1DW(rd_addr, rd_tmp0);
	//Compare the 1st read data0:
	if(!(rd_tmp0 == exp_tmp0))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data0 = %h\tThe Expected Read Data0 = %h",rd_tmp0,exp_tmp0);
	end
end
endtask

//IDE Read Two Double Words:
task	ide_rd_2dw;
input[27:0]	rd_addr;
input[31:0]	exp_data0;
input[31:0]	exp_data1;

reg[31:0]		rd_tmp0;
reg[31:0]		rd_tmp1;
reg[31:0]		exp_tmp0;
reg[31:0]		exp_tmp1;

begin
	rd_tmp0	=	32'hxxxx_xxxx;
	rd_tmp1	=	32'hxxxx_xxxx;
	exp_tmp0	=	exp_data0;
	exp_tmp1	=	exp_data1;

	`IDE_BH_PATH.SDRAM_RD_2DW(rd_addr, rd_tmp0, rd_tmp1);
	//Compare the 1st read data0:
	if(!(rd_tmp0 == exp_tmp0))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data0 = %h\tThe Expected Read Data0 = %h",rd_tmp0,exp_tmp0);
	end
	//Compare the 2nd read data1:
	if(!(rd_tmp1 == exp_tmp1))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data1 = %h\tThe Expected Read Data1 = %h",rd_tmp1,exp_tmp1);
	end
end
endtask

//IDE Read Three Double Words:
task	ide_rd_3dw;
input[27:0]	rd_addr;
input[31:0]	exp_data0;
input[31:0]	exp_data1;
input[31:0]	exp_data2;

reg[31:0]		rd_tmp0;
reg[31:0]		rd_tmp1;
reg[31:0]		rd_tmp2;
reg[31:0]		exp_tmp0;
reg[31:0]		exp_tmp1;
reg[31:0]		exp_tmp2;

begin
	rd_tmp0	=	32'hxxxx_xxxx;
	rd_tmp1	=	32'hxxxx_xxxx;
	rd_tmp2	=	32'hxxxx_xxxx;
	exp_tmp0	=	exp_data0;
	exp_tmp1	=	exp_data1;
	exp_tmp2	=	exp_data2;

	`IDE_BH_PATH.SDRAM_RD_3DW(rd_addr, rd_tmp0, rd_tmp1, rd_tmp2);
	//Compare the 1st read data0:
	if(!(rd_tmp0 == exp_tmp0))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data0 = %h\tThe Expected Read Data0 = %h",rd_tmp0,exp_tmp0);
	end
	//Compare the 2nd read data1:
	if(!(rd_tmp1 == exp_tmp1))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data1 = %h\tThe Expected Read Data1 = %h",rd_tmp1,exp_tmp1);
	end
	//Compare the 3rd read data2:
	if(!(rd_tmp2 == exp_tmp2))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data2 = %h\tThe Expected Read Data2 = %h",rd_tmp2,exp_tmp2);
	end
end
endtask

//IDE Read Four Double Words:
task	ide_rd_4dw;
input[27:0]	rd_addr;
input[31:0]	exp_data0;
input[31:0]	exp_data1;
input[31:0]	exp_data2;
input[31:0]	exp_data3;

reg[31:0]		rd_tmp0;
reg[31:0]		rd_tmp1;
reg[31:0]		rd_tmp2;
reg[31:0]		rd_tmp3;
reg[31:0]		exp_tmp0;
reg[31:0]		exp_tmp1;
reg[31:0]		exp_tmp2;
reg[31:0]		exp_tmp3;

begin
	rd_tmp0	=	32'hxxxx_xxxx;
	rd_tmp1	=	32'hxxxx_xxxx;
	rd_tmp2	=	32'hxxxx_xxxx;
	rd_tmp3	=	32'hxxxx_xxxx;
	exp_tmp0	=	exp_data0;
	exp_tmp1	=	exp_data1;
	exp_tmp2	=	exp_data2;
	exp_tmp3	=	exp_data3;

	`IDE_BH_PATH.SDRAM_RD_4DW(rd_addr, rd_tmp0, rd_tmp1, rd_tmp2, rd_tmp3);
	//Compare the 1st read data0:
	if(!(rd_tmp0 == exp_tmp0))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data0 = %h\tThe Expected Read Data0 = %h",rd_tmp0,exp_tmp0);
	end
	//Compare the 2nd read data1:
	if(!(rd_tmp1 == exp_tmp1))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data1 = %h\tThe Expected Read Data1 = %h",rd_tmp1,exp_tmp1);
	end
	//Compare the 3rd read data2:
	if(!(rd_tmp2 == exp_tmp2))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data2 = %h\tThe Expected Read Data2 = %h",rd_tmp2,exp_tmp2);
	end
	//Compare the 4th read data3:
	if(!(rd_tmp3 == exp_tmp3))begin
		$display("\n%m\tAt Time: %0t\tError_T2_rsim is found!",$realtime);
		$display("%m\tThe Real Read Data3 = %h\tThe Expected Read Data3 = %h",rd_tmp3,exp_tmp3);
	end
end
endtask


`endif	//	end of `ifdef NO_IDE

//	---------------------------------------------------------------------------------------

//	======================================================================================
`endif	//	end of ifdef	CPU_NET



`endif	//all common tasks should add in above INC_CORE condition compile
// end of util.v
