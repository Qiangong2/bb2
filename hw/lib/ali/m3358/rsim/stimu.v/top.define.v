//define cpu
`ifdef	INC_CPU
	`undef	NO_CPU
`else
	`define	NO_CPU
`endif
//define nb
`ifdef	INC_NB
	`undef	NO_NB
`else
	`define	NO_NB
`endif
//define sb
`ifdef	INC_SB
	`undef	NO_SB
`else
	`define	NO_SB
`endif
//define gpu
`ifdef	INC_GPU
	`undef	NO_GPU
`else
	`define	NO_GPU
`endif
//define Video
`ifdef	INC_VIDEO
	`undef	NO_VIDEO
`else
	`define	NO_VIDEO
`endif
//define Display
`ifdef	INC_DISPLAY
	`undef	NO_DISPLAY
`else
	`define	NO_DISPLAY
`endif
//define Audio
`ifdef	INC_AUDIO
	`undef	NO_AUDIO
`else
	`define	NO_AUDIO
`endif
//define USB
`ifdef	INC_USB
	`undef	NO_USB
`else
	`define	NO_USB
`endif
//define IDE
`ifdef	INC_IDE
	`undef	NO_IDE
`else
	`define	NO_IDE
`endif
//define CORE
`ifdef	INC_CORE
	`undef	NO_CORE
`else
	`define	NO_CORE
`endif
//define DSP JFR 030709
`ifdef	INC_DSP
	`undef	NO_DSP
`else
	`define	NO_DSP
`endif

`ifdef	INC_SERVO
	`undef	NO_SERVO
`else
	`define	NO_SERVO
`endif

//define T2_FILE_IO_EN
//for use the file io operation
`define		T2_FILE_IO_EN

//for new sdram control
`define  	NEW_MEM

//for set mem parameters
`define		NEW_MEM_SET

//for performance
`define     CLOSE_PERFORM

//not print clk_monitor.rpt
`define		NO_CLK_MONITOR
