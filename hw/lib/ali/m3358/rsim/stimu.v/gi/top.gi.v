// gi top level sim models;
// ALI cat'ing of files locates this inside a module;

	// gi environment;

	reg alt_boot;
	reg test_in;

	initial
	begin
		#1;
		if ($test$plusargs("bb2_boot")) begin 
			alt_boot = 0;
			test_in = 0;
		end else begin 
			alt_boot = 1'b1;
			test_in = 1'b1;
		end
	end

	assign gc_altboot = alt_boot;
	assign gc_testin = test_in;

	always @(alt_boot)
		$display("%t: %M: alt_boot=%b", $time, alt_boot);

	// instantiate pci bus monitor;
	// monitors external bus traffic;

	pci_mon pci_mon_ext (
		.bus(1'b1),
		.clk(pci_clk),
		.rst(pci_rst_),
		.ad(pci_ad),
		.cbe(pci_cbe_),
		.par(pci_par),
		.frame(pci_frame_),
		.devsel(pci_devsel_),
		.irdy(pci_irdy_),
		.trdy(pci_trdy_),
		.stop(pci_stop_),
		.lock(pci_lock_),
		.perr(pci_perr_),
		.serr(pci_serr_),
		.intr(pci_inta_),
		.req({3'b111,pci_req_}),
		.gnt({2'b11,pci_gntb_,pci_gnta_})
	);

	// instantiate pci bus monitor;
	// monitors external bus traffic;

`define	PCIINT	top.CHIP.CORE.T2_CHIPSET.P_BIU

	wire [3:0] pciint_req;
	wire [3:0] pciint_gnt;

	assign pciint_req = { `PCIINT.in_req_[1], `PCIINT.in_req_[0],
		`PCIINT.usb_req_, `PCIINT.gi_pci_req };
	assign pciint_gnt = { `PCIINT.out_gnt_[1], `PCIINT.out_gnt_[0],
		`PCIINT.usb_gnt_, `PCIINT.gi_pci_gnt };

	pci_mon pci_mon_int (
		.bus(1'b0),
		.clk(`PCIINT.PCI_CLK),
		.rst(`PCIINT.GI_PCI_RST),
		.ad(`PCIINT.in_ad),
		.cbe(`PCIINT.in_cbe_),
		.par(`PCIINT.in_par),
		.frame(`PCIINT.in_frame_),
		.devsel(`PCIINT.in_devsel_),
		.irdy(`PCIINT.in_irdy_),
		.trdy(`PCIINT.in_trdy_),
		.stop(`PCIINT.in_stop_),
		.lock(1'b1), //XXX
		.perr(1'b1),
		.serr(1'b1),
		.intr(1'b1),
		.req(pciint_req),
		.gnt(pciint_gnt)
	);

	// instantiate di host;
	// XXX add support for di_flipper;

	di_host di_host (
		.reset(gc_dirstb),
		.dir(gc_didir),
		.dd(gc_did),
		.hstrb(gc_dihstrbb),
		.dstrb(gc_didstrbb),
		.err(gc_dierrb),
		.brk(gc_dibrk),
		.cover(gc_dicover)
	);

	// instantiate di monitor;

	di_mon di_mon (
		.reset(gc_dirstb),
		.dir(gc_didir),
		.dd(gc_did),
		.hstrb(gc_dihstrbb),
		.dstrb(gc_didstrbb),
		.err(gc_dierrb),
		.brk(gc_dibrk),
		.cover(gc_dicover)
	);

	// instantiate ais_host;

	ais_host ais_host (
		.reset(gc_dirstb),
		.ais_clk(gc_aisclk),
		.ais_lr(gc_aislr),
		.ais_d(gc_aisd)
	);

	// initial jtag pins 
	jtag_tests jtag_tests(
		.trst(p_j_rst_),
		.tdi(p_j_tdi),
		.tms(p_j_tms),
		.tck(p_j_tclk),
		.tdo(p_j_tdo)
	);

	// instantiate bi monitor;
	bi_mon bi_mon (
		.clk(`gi_top.clk),
		.reset(`gi_top.reset),
		.sys_addr({14'd0, `gi_top.sys_addr, 2'b0}),
		.sys_cmd(`gi_top.sys_cmd),
		.sys_out(`gi_top.sys_in),
		.sys_in(`gi_top.sys_out),
		.sys_intr(`gi_top.sys_intr),
		.dma_req(`gi_top.dma_req),
		.dma_bx(`gi_top.dma_bx),
		.dma_addr(`gi_top.dma_addr),
		.dma_wr(`gi_top.dma_wr),
		.dma_done(`gi_top.dma_done)
	);
