/*=============================================================================
// Description: Top module for m3358 simulation
//              include the chip of m3358, and all system signals define here.
// Author:      Tod
// History:     2004-04-20	Initial version

//============================================================================*/

`timescale 1ns/10ps

`ifdef  POST_GSIM
// global path for RSIM and Pre-GSIM
`define path_chip       top.CHIP
`define path            `path_chip.CORE
`define path_chipset    `path.T2_CHIPSET

`else
// global path for RSIM and Pre-GSIM
`define path_chip       top.CHIP
`define path            `path_chip.CORE
`define path_chipset    `path.T2_CHIPSET
`endif

`define path_m          top.boot_rom
/*
`ifdef	NEW_MEM_8M
//`define sdram_d0_4m_4b  // 1st row of SDRAM is 4Mx4B
//`define sdram_d1_4m_4b  // 2nd row of SDRAM is 4Mx4B
`define	sdram_d0_8m_4b	// 1st row of SDRAM is 8Mx4B
`define	sdram_d1_8m_4b	// 2nd row of SDRAM is 8Mx4B
//`define	sdram_d0_16m_4b	// 1st row of SDRAM is 16Mx4B
//`define	sdram_d1_16m_4b	// 2nd row of SDRAM is 16Mx4B
//`define sdram_d1_2m_2b
//`define sdram_d0_2m_2b
`else
`endif*/
`ifdef	NEW_MEM
//`define sdram_d0_4m_4b  // 1st row of SDRAM is 4Mx4B
//`define sdram_d1_4m_4b  // 2nd row of SDRAM is 4Mx4B
//`define	sdram_d0_8m_4b	// 1st row of SDRAM is 8Mx4B
//`define	sdram_d1_8m_4b	// 2nd row of SDRAM is 8Mx4B
`define	sdram_d0_16m_4b	// 1st row of SDRAM is 16Mx4B
`define	sdram_d1_16m_4b	// 2nd row of SDRAM is 16Mx4B
//`define sdram_d1_2m_2b
//`define sdram_d0_2m_2b
`else
//`define sdram_d0_4m_4b  // 1st row of SDRAM is 4Mx4B
//`define sdram_d1_4m_4b  // 2nd row of SDRAM is 4Mx4B
//`define	sdram_d0_8m_4b	// 1st row of SDRAM is 8Mx4B
//`define	sdram_d1_8m_4b	// 2nd row of SDRAM is 8Mx4B
`define	sdram_d0_16m_4b	// 1st row of SDRAM is 16Mx4B
`define	sdram_d1_16m_4b	// 2nd row of SDRAM is 16Mx4B
//`define sdram_d1_2m_2b
//`define sdram_d0_2m_2b
`endif
module  top;

//====================== define the 32bit mode or 16bits =======================
parameter	SDRAM_DQ_Mode		=	1'b1;	//0->32bits mode, 1->16bits mode

//====================== generate the crystal clock ============================
reg             f27_clk;
reg     [31:0]  f27_clk_data;
reg             f48_clk;
reg             f24_clk;

reg             f40_clk;
reg             ejtag_clk;

parameter       cyclef27_clk    = 18.51852;
parameter       cyclef48_clk    = 10.417;
parameter       cyclef24_clk    = 20.34505;
parameter       cycle_ejtag     = 50;//10MHz
parameter       cyclef40_clk    = 12.5;

initial begin
        f27_clk = 0;    //all clock change the first phase by yuky,2002-01-15
        f48_clk = 0;
        f24_clk = 0;
        f40_clk = 0;
        ejtag_clk = 0;
        end

always #cyclef27_clk f27_clk = ~f27_clk;
always #cyclef48_clk f48_clk = ~f48_clk;
always #cyclef24_clk f24_clk = ~f24_clk;
always #cyclef40_clk f40_clk = ~f40_clk;        //for test chipset 133MHz
always #cycle_ejtag  ejtag_clk = ~ejtag_clk;
//==============================================================================

wire            Rom_en          =       ~top.CHIP.PAD_MISC.rom_en_fromcore_;
reg             cold_rst_;
tri1            warm_rst_;
tri1            ext_rst_;
tri1			rom_control_rst_;
assign          warm_rst_ = cold_rst_;
assign          ext_rst_  = warm_rst_;
assign			rom_control_rst_ = warm_rst_ & ~Rom_en;
wire			cold_rst_wire_	=	cold_rst_;
//==============================================================================

wire			f24_clk_wire;
wire			sv_f24_clk_wire;
wire			f24_clk_wire_tmp;

//		Carcy modify for external up test
assign		f24_clk_wire = f24_clk_wire_tmp ;
assign	 	f24_clk_wire_tmp = f24_clk	;

//================= define the signals for tasks ===============================
reg     [31:0]  temp_mem;       // used for Memory Task
reg 	[31:0]  temp2;      	// used for
reg     [31:0]  temp;           // used for Control Task
reg     [31:0]  cpu_temp,
                pci_temp;
reg     [31:0]  irq_temp;       // used for IRQ Task

parameter       mem_max_size = 32'h0080_0000;
reg     [31:0]  mem_array [0:(mem_max_size>>2) - 1];// : 0];
reg     [7:0]   byte_array [0:mem_max_size - 1];// : 0];
reg     [13:0]  sdram_ram0_haddr, sdram_ram1_haddr;
reg     [13:0]  sdram_ram0_haddr_next, sdram_ram1_haddr_next;

integer         curn_offset;
reg     [2:0]   sdram_row0_map,
                sdram_row1_map;
reg				sequence_mode	;
integer         sdram_row1_base,SDRAM_DIMM1_BASE;
reg     [31:0]  temp_xxx;
initial begin
        sdram_row0_map = 0;
        sdram_row1_map = 0;
        for(temp_xxx = 32'h0; temp_xxx < mem_max_size ; temp_xxx = temp_xxx + 1)
                byte_array[temp_xxx] = 0;
        for(temp_xxx = 32'h0; temp_xxx < (mem_max_size>>2) ; temp_xxx = temp_xxx + 1)
                mem_array[temp_xxx] = 0;
end
integer curn_addr, sdram_real_addr;
integer	curn_addr_next, sdram_real_addr_next;
integer f_name,i,j;


integer	i_cpu,j_cpu,k_cpu;		//for cpu pattern
integer	i_disp,j_disp,k_disp;	//for display pattern
integer	i_pci,j_pci,k_pci;		//for pci pattern
integer	i_gpu,j_gpu,k_gpu;		//for gpu pattern
integer	i_dsp,j_dsp,k_dsp;		//for dsp pattern
integer	i_video,j_video,k_video;//for video pattern
integer i_vin,j_vin,k_vin;		//for vin pattern
integer	i_vsb,j_vsb,k_vsb;		//for vsb pattern
integer	i_sb,j_sb,k_sb;			//for south bridge pattern
integer	i_ide,j_ide,k_ide;		//for ide pattern
integer	disp_bh_file;			//for display engine behaviour model dump file

//`ifdef OPEN_DISP_BH_DUMP_RPT
//	initial	begin
//		disp_bh_file = $fopen("disp_bh_dump.rpt");
//	end
//`else
//`endif	// Snow Yi, 2003.08.28

reg     [31:0]  data_dw;
reg     [15:0]  data_w;
reg     [7:0]   data_b;
reg             flag;
reg				SV_RST_ ;

reg             init_end,
                cpu_vec_end,
                pci_vec_end,
                cpu_vec_00_end,
                cpu_vec_01_end,
                cpu_vec_02_end,
                cpu_vec_03_end,
                cpu_vec_04_end,
                cpu_vec_05_end,
                cpu_vec_06_end,
                cpu_vec_07_end,
                cpu_vec_08_end,
                cpu_vec_09_end,
                cpu_vec_10_end,
                cpu_vec_11_end,
                cpu_vec_12_end,
                cpu_vec_13_end,
                cpu_vec_14_end,
                cpu_vec_15_end,
                cpu_vec_16_end,
                cpu_vec_17_end,
                cpu_vec_18_end,
                cpu_vec_19_end,
                cpu_vec_20_end,
                int_end;

//==============================================================================

//========================= define the register for patterns use ===============
reg     [31:0]  temp_data0,temp_data1,temp_data2,temp_data3;
reg     [31:0]  temp_data4,temp_data5,temp_data6,temp_data7;
reg     [31:0]  temp_data8,temp_data9,temp_data10,temp_data11;
//==============================================================================

reg	[3:0]	mem_fout_dly;
initial		mem_fout_dly = 0;
//==================== define the sdram signals ================================
wire    [31:0]  ram_dq, ext_ram_dq;
wire            sd_clk;
tri1    	   	ram_cs0_;
tri1			ram_cs1_;
tri1            ram_ras_, ram_cas_, ram_we_;
wire    [11:0]  ram_addr;
wire    [1:0]   ram_ba;
wire    [3:0]   ram_dqm;

//==================== define PCI signals ======================================
//pci_xx for m3357 JFR030701
//p_xx for m6304
wire			pci_clk   ;
tri1			pci_gnta_ ;
tri1			pci_gntb_ ;
tri1			pci_inta_ ;
tri1			pci_req_  ;
tri1 	[31:0]	pci_ad    ;
tri1    [3:0]   pci_cbe_  ;
tri1            pci_devsel_;
tri1            pci_frame_;
tri1            pci_irdy_;
tri1            pci_par;
//tri1            pci_perr_;
//tri1            pci_serr_;
tri1            pci_stop_;
tri1            pci_trdy_;
tri1            pci_lock_;

tri1            p_perr_;
tri1            p_serr_;
//tri1            p_stop_;
//tri1            p_trdy_;
tri1            p_lock_;
//tri1  [3:0]   p_req_,p_gnt_;
//tri1            p_req_,p_gnt_;
tri1    [3:0]   ext_int;
//wire            p_clk;
wire            monitor_clk     = pci_clk ;
wire            moniter_frame_  = pci_frame_;
wire            p_master_clk    = pci_clk ;
wire            p_target_clk    = pci_clk ;
wire            xout_48;
wire            xout_27;
wire            xout_40;
tri1            p_mas1_req_,    p_mas2_req_;
//assign                p_req_[0] = p_mas1_req_;        // PCI 33 request from p_master
//assign                p_req_[1] = p_mas2_req_;
//wire          p_mas1_gnt_ = p_gnt_[0];
//wire          p_mas2_gnt_ = p_gnt_[1];
//==============================================================================

wire	spdif_to_chip;

//==============================================================================
//===================== define IDE signals =====================================
wire    [15:0]  atadd;
wire    [2:0]   atada;
wire			atadiow_  ;
wire			atadior_  ;
wire			ataiordy  ;
wire			ataintrq  ;
wire			atacs0_   ;
wire			atacs1_   ;
//wire			atareset_ ;
wire			atadmarq  ;
wire			atadmack_ ;

//===================== define GPIO signals=====================================
tri1 [31:0]  gpio;
wire [31:0]  ext_gpio;
// I2C bus
tri1            scbsda;
tri1            scbscl;

//==================================================================
//define for test_dly_chain verification
integer	test_dly_chain_flag	;
//========== define the flash rom signals ==========================
wire    [7:0]   eeprom_data;
wire    [21:0]  eeprom_addr;
wire	[18:17]	eeprom_addr_set2;
wire	[20:0]	eeprom_addr_temp;//for debug
wire			eeprom_ce_;
wire			eeprom_ale;
wire			eeprom_we_;
wire			eeprom_oe_;
//wire	[7:0]	p_rom_data;
//wire	[20:0]	p_rom_addr;
tri1			ext_eeprom_ce_;
tri0			ext_eeprom_ale;
tri1			ext_eeprom_we_;
tri1			ext_eeprom_oe_;
//tri1   [7:0]    ext_eeprom_data;
//tri1   [20:0]	ext_eeprom_addr;
wire	   [7:0]    ext_eeprom_data;
wire	   [21:0]	ext_eeprom_addr;

//================ define the USB signals ======================================
// Both port1 and port2 are Full_speed
pulldown        (weak0) (usb1_dn);
pulldown        (weak0) (usb1_dp);

pulldown        (weak0) (usb2_dn);
pulldown        (weak0) (usb2_dp);

`ifdef INC_CORE
//wire            usb_ovc = 1'b1; // over current
reg usb_ovc;
initial         usb_ovc = 1'b1;
wire            usb_pon;        // power enable
`endif
//==============================================================================

wire            ext_i2si_bck        ;
wire            ext_i2si_lrck       ;

//SERVO
wire	[1:0]	xsflag;

//=================== define the ejtag signals =================================
tri0            p_j_tdo         ;
//tri0            p_j_tdi         ;
//tri0            p_j_tms         ;
//tri0            p_j_tclk        ;
wire            p_j_tms         ;//Carcy 031120
wire            p_j_tclk        ;//Carcy 031120

tri1            p_j_rst_        ;

wire			ext_p_j_tdo		;
wire			ext_p_j_tdi 	;
wire			ext_p_j_tms 	;
wire			ext_p_j_tclk	;
wire			ext_p_j_rst_	;

tri0			spdif;

//==============================================================================
//=====SD MS singals
tri1			ext_sd_clk		;
tri1			ext_ms_clk      ;
tri1			ext_sd_cmd      ;
tri1			ext_ms_bs       ;
tri1			ext_sd_data_0   ;
tri1			ext_sd_data_1   ;
tri1			ext_sd_data_2   ;
tri1			ext_sd_data_3   ;
tri1			ext_ms_sdio     ;
wire			ext_sd_det      ;
wire			ext_ms_ins      ;
tri1			ext_sd_wr_prct  ;
tri1			ext_ms_pwr_ctrl ;

wire			sd_card_clk		;
wire			sd_cmd          ;
wire	[3:0]	sd_data         ;
wire			sd_wr_prct      ;
wire			sd_det          ;
wire			ms_clk          ;
wire			ms_pwr_ctrl     ;
wire			ms_bs           ;
wire			ms_sdio         ;
wire			ms_ins          ;

//===== GI signals

wire			gc_vidclk;		// video clock to GC;
wire	[1:0]		gc_sysclk;		// ppc/flipper clocks;
wire			gc_rstb;		// reset to flipper;
wire			gc_altboot;		// alternate boot;
wire			gc_testin;		// alternate boot;
wire			gc_testen;		// force test on;
wire			gc_promscl;		// serial flash clk;
wire			gc_promsda;		// serial flash data;
wire	[7:0]		gc_did;			// di data, bi-dir;
wire			gc_dibrk;		// di break, bi-dir;
wire			gc_dierrb;		// di error to GC;
wire			gc_didir;		// di direction from GC;
wire			gc_didstrbb;	// di drive strobe/ready;
wire			gc_dihstrbb;	// di host strobe/ready;
wire			gc_dicover;		// di cover open to GC;
wire			gc_dirstb;		// di bus reset;
wire			gc_aisclk;		// ais clock from GC;
wire			gc_aislr;		// ais l/r from GC;
wire			gc_aisd;		// ais data to GC;
//=================== define the TV encoder signals ============================
tri1            pix_clk;
tri1            hsync_;
tri1            vsync_;
wire    [7:0]   vdata;          // Lanny 2001-07-12

tri0	vssa;
tri1	vdda;
//==============================================================================


//========================= define the strap pins ==============================
tri0	tmp_line;
tri0	tmp_line_0;
//-----Note: the strap pins use the SDRAM address,bankaddress and dqm bus
//-----invoke the strap_pin_bh module
//modified for m3357 Jeffrey 030603
// PLL_M
`ifdef COMPATIBLE
strap_pin_bh    cpu_clk_pll_m0          ();
strap_pin_bh    cpu_clk_pll_m1          ();
strap_pin_bh    cpu_clk_pll_m2          ();
strap_pin_bh    cpu_clk_pll_m3          ();
strap_pin_bh    cpu_clk_pll_m4          ();
strap_pin_bh    cpu_clk_pll_m5          ();
`else
strap_pin_bh    cpu_clk_pll_m0          (ram_addr[0]);
strap_pin_bh    cpu_clk_pll_m1          (ram_addr[1]);
strap_pin_bh    cpu_clk_pll_m2          (ram_addr[2]);
strap_pin_bh    cpu_clk_pll_m3          (ram_addr[3]);
strap_pin_bh    cpu_clk_pll_m4          (ram_addr[4]);
strap_pin_bh    cpu_clk_pll_m5          (ram_addr[5]);
`endif
// CPU_PROBE_EN
strap_pin_bh    cpu_probe_en 			(ram_addr[6]);

//FLASH_MODE
strap_pin_bh    flash_mode0      		(ram_addr[7]);
strap_pin_bh    flash_mode1      		(ram_addr[8]);

// WORK_MODE
strap_pin_bh    work_mode0      		(ram_addr[9]);
`ifdef COMPATIBLE
strap_pin_bh    work_mode1      		();	//metal fix, for mode select can be programable
strap_pin_bh    work_mode2      		();
`else
strap_pin_bh    work_mode1      		(ram_addr[10]);	//metal fix, for mode select can be programable
strap_pin_bh    work_mode2      		(ram_addr[11]);
`endif
// MEM_FS MEM_CLK_PLL_M
`ifdef COMPATIBLE
strap_pin_bh    mem_clk_pll_m0         ();
strap_pin_bh    mem_clk_pll_m1         ();
strap_pin_bh    mem_clk_pll_m2         ();
strap_pin_bh    mem_clk_pll_m3         ();
strap_pin_bh    mem_clk_pll_m4         ();
strap_pin_bh    mem_clk_pll_m5         ();
`else
strap_pin_bh    mem_clk_pll_m0         (ram_ba[0]);
strap_pin_bh    mem_clk_pll_m1         (ram_ba[1]);
strap_pin_bh    mem_clk_pll_m2         (ram_dqm[0]);
strap_pin_bh    mem_clk_pll_m3         (ram_dq[0]);
strap_pin_bh    mem_clk_pll_m4         (ram_dq[1]);
strap_pin_bh    mem_clk_pll_m5         (ram_dq[2]);
`endif
// PLL_BYPASS
`ifdef COMPATIBLE
strap_pin_bh    pll_bypass      		();
`else
strap_pin_bh    pll_bypass      		(ram_dq[3]);
`endif
// PCI_T2RISC_FS
strap_pin_bh    pci_t2risc_fs0  (tmp_line_0);
//strap_pin_bh    pci_t2risc_fs1  (ram_addr[11]);	//metal fix, only use one bit for pci frequency select
strap_pin_bh    pci_t2risc_fs1  (tmp_line);			//just for environment compatible after metal fix, it is no real use.
// REFERENCE_INFO
strap_pin_bh    reference_info0 (ram_dq[4]);
strap_pin_bh    reference_info1 (ram_dq[5]);
strap_pin_bh    reference_info2 (ram_dq[6]);
strap_pin_bh    reference_info3 (ram_dq[7]);
strap_pin_bh    reference_info4 (ram_dq[8]);
strap_pin_bh    reference_info5 (ram_dq[9]);
strap_pin_bh    reference_info6 (ram_dq[10]);
strap_pin_bh    reference_info7 (ram_dq[11]);
strap_pin_bh    reference_info8 (ram_dq[12]);
strap_pin_bh    reference_info9 (ram_dq[13]);
strap_pin_bh    reference_info10 (ram_dq[14]);
strap_pin_bh    reference_info11 (ram_dq[15]);
//strap_pin_bh    reference_info12 (ram_dq[15]);
//==============================================================================
tri0    test_mode;
//tri1    test_mode;//for bist debug JFR030930

chip    CHIP    (
// SDRAM & FLASH(56)
        .p_d_clk                (sd_clk         ),
        .p_d_addr               (ram_addr       ),// SDRAM Row/Column Address
        .p_d_baddr              (ram_ba         ),// SDRAM bank address
        .p_d_dq                 (ram_dq   		),// SDRAM 32 bits output data pins		// carcy 2004-3-17 8:56
        .p_d_dqm                (ram_dqm		),// SDRAM data mask signal				// carcy 2004-3-17 8:59
        .p_d_cs_                (ram_cs0_       ),// SDRAM chip select
        .p_d_cas_               (ram_cas_       ),// SDRAM cas#
        .p_d_we_                (ram_we_        ),// SDRAM we#
        .p_d_ras_               (ram_ras_       ),// SDRAM ras#

        // FLASH ROM(3)
        .p_rom_ale				(eeprom_ale		), // External 373 latch enable
//        .p_rom_ce_              (eeprom_ce_     ),// 1,   Flash rom chip select
        .p_rom_we_              (eeprom_we_     ),// 1,   Flash rom write enable
        .p_rom_oe_              (eeprom_oe_     ),// 1,   Flash rom output enable
//added for m3357 JFR030613
		.p_rom_data				(eeprom_data	),// 8 	Flash rom data
        .p_rom_addr				(eeprom_addr	),// 21	Flash rom address
		.p_rom_addr_set2		(eeprom_addr_set2),// 2	Flash rom address set 2 [18:17] for bonding

// I2S Audio Interface (11)
        .i2so_data0             (i2so_data0     ),// 1,   Serial data for I2S output
        .i2so_data1				(i2so_data1		),// 1,   Serial data for I2S output 1
        .i2so_data2				(i2so_data2		),// 1,   Serial data for I2S output 2
        .i2so_data3				(i2so_data3		),// 1,   Serial data for I2S output 3
        .i2so_bck               (i2so_bck       ),// 1,   Bit clock for I2S output
        .i2so_lrck              (i2so_lrck      ),// 1,   Channel clock for I2S output
        .i2so_mclk              (f24_clk_wire	),// 1,   over sample clock for i2s DAC
        .i2si_data              (i2si_data      ),// 1,   serial data for I2S input
        .i2si_bck               (i2si_bck       ),// 1,   bit clock for I2S input
        .i2si_lrck              (i2si_lrck      ),// 1,   channel clock for I2S input
        .i2si_mclk				(i2si_mclk		),
//TV encoder Interface (14)
		.gnda_tvdac				(gnda_tvdac		),	// 1,	Analog GND, input
		.vd33a_tvdac			(vd33a_tvdac	),	// 3,	Analog Power, input
		.iout1					(iout1			),	// 1,   Analog video output 1
		.iout2					(iout2			),	// 1,   Analog video output 2
		.iout3					(iout3			),	// 1,   Analog video output 3
		.iout4					(iout4			),	// 1,   Analog video output 4
		.iout5					(iout5			),	// 1,   Analog video output 5
		.iout6					(iout6			),	// 1,   Analog video output 6
//aeede for m3357 JFR 030613
		.xiref1					(xiref1			),	// 1,	connect external via capacitance toward VDDA
		.xiext					(xiext			),	// 1,	470 Ohms resistor connect pin, I/O
		.xidump					(xidump			),	// 1,	For performance and chip temperature, output
//		vbg,			// 1,	reference power, will not be bonded to Pin.
// SPDIF (2)
        .spdif                  (spdif			),// 1, output only
        .spdif_in				(spdif_to_chip    	),
// PCI Interface
		.p_clk					(pci_clk	    		),
		.p_gnta_     			(pci_gnta_    			),
		.p_gntb_     			(pci_gntb_        		),
		.p_rst_      			(pci_rst_        		),
		.p_req_					(pci_req_    			),
		.p_inta_     			(pci_inta_        		),
		.p_ad					(pci_ad					),
		.p_par       			(pci_par         		),
		.p_frame_    			(pci_frame_   			),
		.p_irdy_     			(pci_irdy_        		),
		.p_trdy_     			(pci_trdy_        		),
		.p_stop_     			(pci_stop_    			),
		.p_devsel_   			(pci_devsel_  			),
		.p_cbe_      			(pci_cbe_     			),
		.p_serr_                (              ),
		.p_perr_                (              ),
//IDE Interface
		.atadiow_				(atadiow_				),
		.atadior_    			(atadior_				),
		.atacs0_     			(atacs0_ 				),
		.atacs1_     			(atacs1_ 				),
		.atada       			(atada   				),
		.atareset_   			(atareset_				),
		.atadmack_   			(atadmack_				),
		.ataiordy				(ataiordy				),
		.ataintrq    			(ataintrq				),
		.atadmarq    			(atadmarq				),
		.atadd					(atadd					),
//SD_MS Interface
	    .sd_clk					(sd_card_clk		    ),
		.sd_cmd         		(sd_cmd         		),
		.sd_data        		(sd_data        		),
		.sd_wr_prct     		(sd_wr_prct     		),
		.sd_det         		(sd_det         		),
		.ms_clk         		(ms_clk         		),
		.ms_pwr_ctrl    		(ms_pwr_ctrl    		),
		.ms_bs          		(ms_bs          		),
		.ms_sdio        		(ms_sdio        		),
		.ms_ins         		(ms_ins         		),
//GI (from broadon)
		.GCVIDCLK		(gc_vidclk	 		),
		.GCSYSCLK		(gc_sysclk	 		),
		.GCRSTB  		(gc_rstb	 		),
		.ALTBOOT    	        (gc_altboot	 		),
		.TESTEN               	(gc_testen	 		),
		.PROMSCL               	(gc_promscl	 		),
		.PROMSDA               	(gc_promsda	 		),
		.DID                	(gc_did		 		),
		.DIBRK                  (gc_dibrk	 		),
		.DIDIR                  (gc_didir	 		),
		.DIERRB                 (gc_dierrb	 		),
		.DIDSTRBB               (gc_didstrbb 			),
		.DIHSTRBB               (gc_dihstrbb 			),
		.DICOVER                (gc_dicover	 		),
		.DIRSTB			(gc_dirstb			),
		.AISCLK                 (gc_aisclk	 		),
		.AISLR                  (gc_aislr	 		),
		.AISD                   (gc_aisd	 		),
//add 2 digital pin(M3358)
		.tven_hsync_			(tven_hsync_			),
		.tven_vsync_            (tven_vsync_            ),
//DIGITAL VIDEO OUT (11)
		.pix_clk				(pix_clk		        ),
		.vout_data				(vout_data				),
		.vout_hsync_			(vout_hsync_			),
		.vout_vsync_ 			(vout_vsync_ 			),
//DIGITAL VIDEO IN
		.vin_pix_clk			(vin_pix_clk			),
		.vin_pix_data           (vin_pix_data           ),
		.vin_hsync_             (vin_hsync_             ),
		.vin_vsync_             (vin_vsync_             ),

//UART port (2)JFR030730
		.uart_tx				(uart_tx		),//1
		.uart_rx                (uart_rx		),// 1
// USB port (6)
        .usb2dp                 (usb2_dp        ),// 1,   D+ for USB port2
        .usb2dn                 (usb2_dn        ),// 1,   D- for USB port2
        .usb1dp                 (usb1_dp        ),// 1,   D+ for USB port1
        .usb1dn                 (usb1_dn        ),// 1,   D- for USB port1
        .usbpon                 (usb_pon        ),// 1,   USB port power control
        .usbovc                 (usb_ovc        ),// 1,   USB port over current
//SERVO port (70) JFR030611
 		.XADCIN		   		 	(XADCIN			),
 		.XADCIP		   		 	(XADCIP			),
 		.XATTON		   		 	(XATTON			),
 		.XATTOP		   		 	(XATTOP			),
 		.XBIASR		   		 	(XBIASR			),
 		.XCDLD		   		 	(XCDLD			),
 		.XCDPD		   		 	(XCDPD			),
 		.XCDRF		   		 	(XCDRF			),
 		.XCD_A		   		 	(XCD_A			),
 		.XCD_B		   		 	(XCD_B			),
 		.XCD_C		   		 	(XCD_C			),
 		.XCD_D		   		 	(XCD_D			),
 		.XCD_E		   		 	(XCD_E			),
 		.XCD_F		   		 	(XCD_F			),
 		.XCELPFO	   		 	(XCELPFO		),
 		.XDPD_A		   		 	(XDPD_A			),
 		.XDPD_B		   		 	(XDPD_B			),
 		.XDPD_C		   		 	(XDPD_C			),
 		.XDPD_D		   		 	(XDPD_D			),
 		.XDVDLD		   		 	(XDVDLD			),
 		.XDVDPD		   		 	(XDVDPD			),
 		.XDVDRFN	   		 	(XDVDRFN		),
 		.XDVDRFP	   		 	(XDVDRFP		),
 		.XDVD_A		   		 	(XDVD_A			),
 		.XDVD_B		   		 	(XDVD_B			),
 		.XDVD_C		   		 	(XDVD_C			),
 		.XDVD_D		   		 	(XDVD_D			),
 		.XFELPFO	   		 	(XFELPFO		),
 		.XFOCUS		   		 	(XFOCUS			),
 		.XGMBIASR	   		 	(XGMBIASR		),
 		.XLPFON		   		 	(XLPFON			),
 		.XLPFOP		   		 	(XLPFOP			),
 		.XPDAUX1	   		 	(XPDAUX1		),
 		.XPDAUX2	   		 	(XPDAUX2		),
 		.XSBLPFO	   		 	(XSBLPFO		),
 		.XSFGN		   		 	(XSFGN			),
 		.XSFGP		   		 	(XSFGP			),
 		.XSFLAG				 	(xsflag			),//For debug 2004-04-12 JFR
 		.XSLEGN		   		 	(XSLEGN			),
 		.XSLEGP		   		 	(XSLEGP			),
 		.XSPINDLE	   		 	(XSPINDLE		),
 		.XTELP		   		 	(XTELP			),
 		.XTELPFO	   		 	(XTELPFO		),
 		.XTESTDA	   		 	(XTESTDA		),
 		.XTEXO		   		 	(XTEXO			),
 		.XTRACK		   		 	(XTRACK			),
 		.XTRAY		   		 	(XTRAY			),
 		.XVGAIN		   		 	(XVGAIN			),
 		.XVGAIP		   		 	(XVGAIP			),
 		.XVREF15	   		 	(XVREF15		),
 		.XVREF21	   		 	(XVREF21		),

// 		.VD33D		  		 	(VD33D			),
// 		.VD18D		  		 	(VD18D			),
 		.VDD3MIX1	  		 	(VDD3MIX1		),
 		.VDD3MIX2	  		 	(VDD3MIX2		),
 		.AVDD_AD	  		 	(AVDD_AD		),
 		.AVDD_ATT	  		 	(AVDD_ATT		),
 		.AVDD_DA	  		 	(AVDD_DA		),
 		.AVDD_DPD	  		 	(AVDD_DPD		),
 		.AVDD_LPF	  		 	(AVDD_LPF		),
 		.AVDD_RAD	  		 	(AVDD_RAD		),
 		.AVDD_REF	  		 	(AVDD_REF		),
 		.AVDD_SVO	  		 	(AVDD_SVO		),
 		.AVDD_VGA	  		 	(AVDD_VGA		),
 		.AVSS_AD	  		 	(AVSS_AD		),
 		.AVSS_ATT	  		 	(AVSS_ATT		),
 		.AVSS_DA	  		 	(AVSS_DA		),
 		.AVSS_DPD	  		 	(AVSS_DPD		),
 		.AVSS_GA	  		 	(AVSS_GA		),
 		.AVSS_GD	  		 	(AVSS_GD		),
 		.AVSS_LPF	  		 	(AVSS_LPF		),
 		.AVSS_RAD	  		 	(AVSS_RAD		),
 		.AVSS_REF	  		 	(AVSS_REF		),
 		.AVSS_SVO	  		 	(AVSS_SVO		),
 		.AVSS_VGA	  		 	(AVSS_VGA		),
// 		.AZEC_GND	  		 	(AZEC_GND		),
// 		.AZEC_VDD	  		 	(AZEC_VDD		),
 		.DV18_RCKA	  		 	(DV18_RCKA		),
 		.DV18_RCKD	  		 	(DV18_RCKD		),
// 		.DVDD	      		 	(DVDD	    	),
// 		.DVSS	      		 	(DVSS	    	),
 		.DVSS_RCKA	  		 	(DVSS_RCKA		),
 		.DVSS_RCKD	  		 	(DVSS_RCKD		),
// 		.FAD_GND	  		 	(FAD_GND		),
// 		.FAD_VDD	  		 	(FAD_VDD		),
// 		.GNDD		  		 	(GNDD			),
// Consumer IR Interface (1)
        .irrx                   (ext_irrx       ),// 1,   consumer infra-red remote controller/wireless keyboard
// I2c Interface
		.scbsda					(scbsda			),
		.scbscl					(scbscl			),
// General Purpose IO (8,GPIO)
        .gpio                   (gpio[31:0]		), // 15,  General purpose io		// carcy 2004-3-17 9:09
// EJTAG Interface (5)
        .p_j_tdo                (p_j_tdo        ), // 1,   serial test data output
        .p_j_tdi                (p_j_tdi        ), // 1,   serial test data input
        .p_j_tms                (p_j_tms        ), // 1,   test mode select
        .p_j_tclk               (p_j_tclk       ), // 1,   test clock
        .p_j_rst_               (p_j_rst_       ), // 1,   test reset
// power and ground
		.vddcore				(1'b1),//		core vdd
//		.vsscore				(1'b0),//		core vss
		.vddio					(1'b1),//		io vdd
		.gnd					(1'b0),//		io vss
//		.cpu_pllvdd				(1'b1),//		pll vdd for cpu pll
//		.cpu_pllvss				(1'b0),//		pll vss for cpu pll
		.f27_pllvdd				(1'b1),//		pll vdd for f27 pll
		.f27_pllvss				(1'b0),//		pll vss for f27 pll
// system (4)
        .p_crst_                (cold_rst_      ), // 1,   cold reset
        .test_mode              (test_mode      ),
//        .test_sel				(test_sel		),
     //   .p_x48in                (f48_clk        ), // 1,   crystal input (48 MHz for USB)
     //   .p_x48out               (xout_48        )  // 1,   crystal output
        .p_x27in				(f27_clk	   	),// 1,   crystal input (27 MHz for video output)

       .p_x27out        		(xout_27   		), // 1,   crystal output
//ADC (5)
		.vd33a_adc				(vd33a_adc	 	),// 1,	3.3V Analog VDD for ADC.
		.xmic1in     			(xmic1in     	),// 1,   ADC microphone input 1.
		.xadc_vref   			(xadc_vref   	),// 1,   ADC reference voltage (refered to the reference circuit).
		.xmic2in     			(xmic2in     	),// 1,   ADC microphone input 2.
		.gnda_adc    			(gnda_adc    	) // 1,   Analog GND for ADC.

        );

`ifdef INC_CORE
//========COMBO mode share pin	==================================
//ejtag
 tranif1 (p_j_tdi		,       ext_p_j_tdi	    ,      1'b1);
 tranif1 (p_j_tdo		,       ext_p_j_tdo	    ,      1'b1);
 tranif1 (p_j_tms		,       ext_p_j_tms	    ,      1'b1);
 tranif1 (p_j_tclk		,       ext_p_j_tclk	,      1'b1);
 tranif1 (p_j_rst_		,       ext_p_j_rst_	,      1'b1);
//irrx
tranif1 (ext_irrx      	,       irrx            ,      1'b1);
//sd_ms
tranif1 (sd_wr_prct     ,       ext_sd_wr_prct    ,    1'b1);//JFR,2003-12-03
tranif1 (ms_pwr_ctrl    ,       ext_ms_pwr_ctrl   ,    1'b1);//JFR,2003-12-03
tranif1 (sd_det       	,       ext_sd_det        ,    1'b1);
tranif1 (ms_ins       	,       ext_ms_ins        ,    1'b1);
tranif1 (sd_data[0]     ,       ext_sd_data_0     ,    1'b1);
tranif1 (ms_sdio      	,       ext_ms_sdio       ,    1'b1);
tranif1 (sd_data[1]     ,   	ext_sd_data_1	 ,     1'b1);
tranif1 (sd_data[2]     ,   	ext_sd_data_2	 ,     1'b1);
tranif1 (sd_data[3]     ,		ext_sd_data_3   ,	   1'b1);
tranif1 (sd_cmd       	,      	ext_sd_cmd	     ,     1'b1);
tranif1 (ms_bs      	,       ext_ms_bs	     ,     1'b1);
tranif1 (sd_card_clk    ,       ext_sd_clk		 ,     1'b1);
tranif1 (ms_clk       	,       ext_ms_clk	     ,     1'b1);

//======================= IDE mode share pin =============================
wire	ext_atacs0_, ext_atacs1_;
wire	ext_atadiow_    ,
		ext_atadior_    ,
		ext_ataiordy    ,
		ext_ataintrq    ,
		ext_atareset_   ,
		ext_atadmarq 	,
		ext_atadmack_	;
wire[15:0]ext_atadd;
wire[2:0]ext_atada;
tri1	high_net;

tranif1 (atacs0_   		 	,   ext_atacs0_    ,       1'b1);
tranif1 (atacs1_   		 	,   ext_atacs1_    ,       1'b1);
tranif1 (atadmarq			,   ext_atadmarq   ,       1'b1);
tranif1 (cold_rst_wire_		,   ext_atareset_  ,       1'b1);   // JFR 2003-11-24, norman, 2003-11-25
tranif1 (ataiordy	 		,   ext_ataiordy   ,       1'b1);
tranif1 (ataintrq	 		,   ext_ataintrq   ,       1'b1);
tranif1 (atadd[0]   	,       ext_atadd[0]   ,       1'b1);
tranif1 (atadd[1]   	,       ext_atadd[1]   ,       1'b1);
tranif1 (atadd[2]   	,       ext_atadd[2]   ,       1'b1);
tranif1 (atadd[3]   	,       ext_atadd[3]   ,       1'b1);
tranif1 (atadd[4]   	,       ext_atadd[4]   ,       1'b1);
tranif1 (atadd[5]   	,       ext_atadd[5]   ,       1'b1);
tranif1 (atadd[6]  		,       ext_atadd[6]   ,       1'b1);
tranif1 (atadd[7]  		,       ext_atadd[7]   ,       1'b1);
tranif1 (atadd[8]  		,       ext_atadd[8]   ,       1'b1);
tranif1 (atadd[9]  		,       ext_atadd[9]   ,       1'b1);
tranif1 (atadd[10] 		,       ext_atadd[10]  ,       1'b1);
tranif1 (atadd[11] 		,       ext_atadd[11]  ,       1'b1);
tranif1 (atadd[12] 		,       ext_atadd[12]  ,       1'b1);
tranif1 (atadd[13]		,       ext_atadd[13]  ,       1'b1 );
tranif1 (atadd[14]		,       ext_atadd[14]  ,       1'b1 );
tranif1 (atadd[15] 		,       ext_atadd[15]  ,       1'b1);

tranif1 (atada[0]		,       ext_atada[0]   ,   1'b1);
tranif1 (atada[1] 		,       ext_atada[1]   ,   1'b1);
tranif1 (atada[2] 		,       ext_atada[2]   ,   1'b1);
tranif1 (atadiow_		,       ext_atadiow_   ,   1'b1);
tranif1 (atadior_		,       ext_atadior_   ,   1'b1);
tranif1 (atadmack_		,       ext_atadmack_  ,   1'b1);

//==============================================================================
//	FLASH in
//==============================================================================
//

assign	ext_eeprom_ce_	=	eeprom_ce_;
assign	ext_eeprom_we_	=	eeprom_we_;
assign	ext_eeprom_ale	=	eeprom_ale;
//modified for servo test mode debug JFR2004-04-29
//assign	ext_eeprom_oe_	=	eeprom_oe_;
tranif1	(ext_eeprom_oe_		,	eeprom_oe_,		1'b1);


 tranif1	(eeprom_addr[0]		,	ext_eeprom_addr[0],		1'b1);
 tranif1	(eeprom_addr[1]		,	ext_eeprom_addr[1],		1'b1);
 tranif1	(eeprom_addr[2]		,	ext_eeprom_addr[2],		1'b1);
 tranif1	(eeprom_addr[3]		,	ext_eeprom_addr[3],		1'b1);
 tranif1	(eeprom_addr[4]		,	ext_eeprom_addr[4],		1'b1);
 tranif1	(eeprom_addr[5]		,	ext_eeprom_addr[5],		1'b1);
 tranif1	(eeprom_addr[6]		,	ext_eeprom_addr[6],		1'b1);
 tranif1	(eeprom_addr[7]		,	ext_eeprom_addr[7],		1'b1);
 tranif1	(eeprom_addr[8]		,	ext_eeprom_addr[8],		1'b1);
 tranif1	(eeprom_addr[9]		,	ext_eeprom_addr[9],		1'b1);
 tranif1	(eeprom_addr[10]	,	ext_eeprom_addr[10],	1'b1);
 tranif1	(eeprom_addr[11]	,	ext_eeprom_addr[11],	1'b1);
 tranif1	(eeprom_addr[12]	,	ext_eeprom_addr[12],	1'b1);
 tranif1	(eeprom_addr[13]	,	ext_eeprom_addr[13],	1'b1);
 tranif1	(eeprom_addr[14]	,	ext_eeprom_addr[14],	1'b1);
 tranif1	(eeprom_addr[15]	,	ext_eeprom_addr[15],	1'b1);
 tranif1	(eeprom_addr[16]	,	ext_eeprom_addr[16],	1'b1);
 tranif1	(eeprom_addr[17]	,	ext_eeprom_addr[17],	1'b1);
 tranif1	(eeprom_addr[18]	,	ext_eeprom_addr[18],	1'b1);
 tranif1	(eeprom_addr[19]	,	ext_eeprom_addr[19],	1'b1);
 tranif1	(eeprom_addr[20]	,	ext_eeprom_addr[20],	1'b1);
 tranif1	(eeprom_addr[21]	,	ext_eeprom_addr[21],	1'b1);

 tranif1	(eeprom_data[0]	,	ext_eeprom_data[0],		1'b1);
 tranif1	(eeprom_data[1]	,	ext_eeprom_data[1],		1'b1);
 tranif1	(eeprom_data[2]	,	ext_eeprom_data[2],		1'b1);
 tranif1	(eeprom_data[3]	,	ext_eeprom_data[3],		1'b1);
 tranif1	(eeprom_data[4]	,	ext_eeprom_data[4],		1'b1);
 tranif1	(eeprom_data[5]	,	ext_eeprom_data[5],		1'b1);
 tranif1	(eeprom_data[6]	,	ext_eeprom_data[6],		1'b1);
 tranif1	(eeprom_data[7]	,	ext_eeprom_data[7],		1'b1);

//=========================== SDRAM 32bit mode and 16bit mode select ==============
//0-->32bits mode ; 1-->16bits mode
tranif0 (ram_dq[00]       ,       ext_ram_dq[00]    ,       1'b0);       // yuky,2002-07-23
tranif0 (ram_dq[01]       ,       ext_ram_dq[01]    ,       1'b0);       // yuky,2002-07-23
tranif0 (ram_dq[02]       ,       ext_ram_dq[02]    ,       1'b0);       // yuky,2002-07-23
tranif0 (ram_dq[03]       ,       ext_ram_dq[03]    ,       1'b0);       // yuky,2002-07-23
tranif0 (ram_dq[04]       ,       ext_ram_dq[04]    ,       1'b0);       // yuky,2002-07-23
tranif0 (ram_dq[05]       ,       ext_ram_dq[05]    ,       1'b0);       // yuky,2002-07-23
tranif0 (ram_dq[06]       ,       ext_ram_dq[06]    ,       1'b0);       // yuky,2002-07-23
tranif0 (ram_dq[07]       ,       ext_ram_dq[07]    ,       1'b0);       // yuky,2002-07-23

tranif0 (ram_dq[08]       ,       ext_ram_dq[08]    ,       1'b0);       // yuky,2002-07-23
tranif0 (ram_dq[09]       ,       ext_ram_dq[09]    ,       1'b0);       // yuky,2002-07-23
tranif0 (ram_dq[10]       ,       ext_ram_dq[10]    ,       1'b0);       // yuky,2002-07-23
tranif0 (ram_dq[11]       ,       ext_ram_dq[11]    ,       1'b0);       // yuky,2002-07-23
tranif0 (ram_dq[12]       ,       ext_ram_dq[12]    ,       1'b0);       // yuky,2002-07-23
tranif0 (ram_dq[13]       ,       ext_ram_dq[13]    ,       1'b0);       // yuky,2002-07-23
tranif0 (ram_dq[14]       ,       ext_ram_dq[14]    ,       1'b0);       // yuky,2002-07-23
tranif0 (ram_dq[15]       ,       ext_ram_dq[15]    ,       1'b0);       // yuky,2002-07-23

tranif0 (ram_dq[16]       ,       ext_ram_dq[16]    ,       1'b0);       // yuky,2002-07-23
tranif0 (ram_dq[17]       ,       ext_ram_dq[17]    ,       1'b0);       // yuky,2002-07-23
tranif0 (ram_dq[18]       ,       ext_ram_dq[18]    ,       1'b0);       // yuky,2002-07-23
tranif0 (ram_dq[19]       ,       ext_ram_dq[19]    ,       1'b0);       // yuky,2002-07-23
tranif0 (ram_dq[20]       ,       ext_ram_dq[20]    ,       1'b0);       // yuky,2002-07-23
tranif0 (ram_dq[21]       ,       ext_ram_dq[21]    ,       1'b0);       // yuky,2002-07-23
tranif0 (ram_dq[22]       ,       ext_ram_dq[22]    ,       1'b0);       // yuky,2002-07-23
tranif0 (ram_dq[23]       ,       ext_ram_dq[23]    ,       1'b0);       // yuky,2002-07-23
tranif0 (ram_dq[24]       ,       ext_ram_dq[24]    ,       1'b0);       // yuky,2002-07-23
tranif0 (ram_dq[25]       ,       ext_ram_dq[25]    ,       1'b0);       // yuky,2002-07-23
tranif0 (ram_dq[26]       ,       ext_ram_dq[26]    ,       1'b0);       // yuky,2002-07-23
tranif0 (ram_dq[27]       ,       ext_ram_dq[27]    ,       1'b0);       // yuky,2002-07-23
tranif0 (ram_dq[28]       ,       ext_ram_dq[28]    ,       1'b0);       // yuky,2002-07-23
tranif0 (ram_dq[29]       ,       ext_ram_dq[29]    ,       1'b0);       // yuky,2002-07-23
tranif0 (ram_dq[30]       ,       ext_ram_dq[30]    ,       1'b0);       // yuky,2002-07-23
tranif0 (ram_dq[31]       ,       ext_ram_dq[31]    ,       1'b0);       // yuky,2002-07-23

`endif

wire	[1:0]	ram_cs_	=	{ram_cs1_,ram_cs0_};

parameter	Local_Parameter_Set_En	= 0;//0--> don't affect MIPS program,
										//1--> affect MIPS to use following parameters
parameter	Clock_Tree_Delay_Chain_Control_Register	= 32'h0000_0101;//SysIO offset:0x64
parameter	Sdram_Timing_Parameter_Register	= 16'h0eeb;//SysIO offset:0x84

reg			LPSE;
reg	[31:0]	CTDCCR;
reg	[15:0]	STPR;
integer		sdram_param_hadle;
initial	begin
	sdram_param_hadle	= $fopen("sdram_ini_param.txt", "w");
	LPSE	=	Local_Parameter_Set_En;
	CTDCCR	=	Clock_Tree_Delay_Chain_Control_Register;
	STPR	=	Sdram_Timing_Parameter_Register;
	$fdisplay(sdram_param_hadle,"%h %h %h %h",{8{LPSE}},{8{LPSE}},{8{LPSE}},{8{LPSE}});
	$fdisplay(sdram_param_hadle,"%h %h %h %h",CTDCCR[7:0],CTDCCR[15:8],CTDCCR[23:16],CTDCCR[31:24]);
	$fdisplay(sdram_param_hadle,"%h %h %h %h",STPR[7:0],STPR[15:8],8'h0,8'h0);
	$fclose(sdram_param_hadle);
end

//define for cpu and video bist test
`ifdef	POST_GSIM
wire	warm_reset_	= top.CHIP.CORE.CORE_WARMRST_;
wire	cold_reset_	= top.CHIP.CORE.CORE_COLDRST_;
wire	clk			= top.CHIP.CORE.CPU_CLK;
`else
wire	warm_reset_	= top.CHIP.CORE.core_warmrst_;
wire	cold_reset_	= top.CHIP.CORE.core_coldrst_;
wire	clk			= top.CHIP.CORE.cpu_clk;
`endif




