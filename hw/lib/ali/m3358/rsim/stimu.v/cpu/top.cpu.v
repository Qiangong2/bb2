`define	top_path 			top.CHIP.CORE.T2_CPU.cpu_core.cpu_top

`define	cpu_path			top.CHIP.CORE.T2_CPU

`define d_cache_con_path 	top.CHIP.CORE.T2_CPU.cpu_core.cpu_top.t2_d_cache.d_cache_control_1
`define cp0_reg_path 		top.CHIP.CORE.T2_CPU.cpu_core.cpu_top.t2_main_ctrl.cop_excp.cp0_1.cpx_reg
`define cp1_reg_path 		top.CHIP.CORE.T2_CPU.cpu_core.cpu_top.t2_main_ctrl.cop_excp.cp0_1
`define cop_excp_path 		top.CHIP.CORE.T2_CPU.cpu_core.cpu_top.t2_main_ctrl.cop_excp

//module risc;
`ifdef	POST_GSIM
	wire	probe_en	= top.CHIP.CORE.CPU_PROBE_EN;
`else
	wire	probe_en	= top.CHIP.CORE.cpu_probe_en;
`endif
wire	ice_rst_	= warm_reset_ & probe_en;

ice_test ice_test(
   			.j_tms		(ext_p_j_tms		),
   			.j_tdi		(ext_p_j_tdi		),
   			.j_tdo		(ext_p_j_tdo		),
   			.j_tclk		(ext_p_j_tclk		),
   			.j_trst_	(ext_p_j_rst_		),
   			.warm_rst_	(ice_rst_		) //warm_reset_	)
			);
//############################################################
//###### 	Migrate form run.all, sh  				###########
//########  	Jeffrey 2003-08-05					###########
//###########################################################
/**********************************************************
	Function: This test bench is for fpga rsim.
	Author:   Jim
	Date:     2002/9/20
***********************************************************/

//reg		clk;
reg		rst_;
reg		reset_;

wire		u_request;
wire		u_write;
wire	[1:0]	u_wordsize;
wire	[3:0]	u_bytemask;
wire	[31:0]	u_startpaddr;
wire	[31:0]	u_data_to_send;
wire		o_ready;
wire		o_ack;
wire		o_bus_error;
wire	[31:0]	o_data_in;

wire		j_tclk;
wire		j_trst_;
wire		j_tdi;
wire		j_tdo;
wire		j_tms;
wire		ProbEn;

//========= invoke the MIPS program report model =================

wire [31:0]	core_gpio_out	= top.CHIP.CORE.gpio_out;
wire		error_rpt_clk 	= top.CHIP.CORE.T2_CHIPSET.cpu_clk;
wire		error_rpt_rst_ 	= top.CHIP.CORE.core_warmrst_;
wire		sys_mem_clk		= top.CHIP.CORE.T2_CHIPSET.mem_clk;
wire		sys_chipset_rst_= top.CHIP.CORE.T2_CHIPSET.rst_;

wire		sysctrl_req		= top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.ctrl_reg.sysctrl_req		;
wire		sysctrl_gnt     = top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.ctrl_reg.sysctrl_gnt      ;
wire [15:2]	sysctrl_addr    = top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.ctrl_reg.sysctrl_addr     ;
wire [3:0]	sysctrl_be      = top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.ctrl_reg.sysctrl_be       ;
wire		sysctrl_cmd     = top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.ctrl_reg.sysctrl_cmd      ;
wire [31:0]	sysctrlrd_data  = top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.ctrl_reg.sysctrlrd_data   ;
wire [31:0]	sysctrlwr_data  = top.CHIP.CORE.T2_CHIPSET.NB_CPU_BIU.ctrl_reg.sysctrlwr_data   ;

//use the ctrl_reg signal to latch the scratch data for debug
parameter	t_udly = 1;
wire	sys_byte0 = sysctrl_be[0] == 1'b1;
wire	sys_byte1 = sysctrl_be[1] == 1'b1;
wire	sys_byte2 = sysctrl_be[2] == 1'b1;
wire	sys_byte3 = sysctrl_be[3] == 1'b1;
wire	index_30h = sysctrl_addr[15:2] == 'h0c;
wire	ctrlwr_30h = index_30h & sysctrl_cmd & sysctrl_gnt;

reg	[31:0]	debug_data;
always @(posedge sys_mem_clk or negedge sys_chipset_rst_)
  if (!sys_chipset_rst_)
     debug_data	<= #t_udly 32'h0;
  else if (ctrlwr_30h)	begin
     if (sys_byte0)	debug_data[7:0]		<= #t_udly sysctrlwr_data[7:0]	;
     if (sys_byte1)	debug_data[15:8]	<= #t_udly sysctrlwr_data[15:8]	;
     if (sys_byte2)	debug_data[23:16]	<= #t_udly sysctrlwr_data[23:16]	;
     if (sys_byte3)	debug_data[31:24]	<= #t_udly sysctrlwr_data[31:24]	;
  end
//wire [3:0]	gpio_3t0	= {ext_gpio[15:13], core_gpio_out[12]}; //because ext_gpio[12] has been use the flash address [19]
wire [3:0]	gpio_3t0	= core_gpio_out[15:12];
error_rpt error_rpt(
			.gpio_3t0		(gpio_3t0		),
			.rpt_data_31t0	(debug_data		),
			.clk			(error_rpt_clk	),
			.rst_           (error_rpt_rst_	)
			);
`ifdef MIPS_VIDEO_INFO_DISP
video_mips_rpt video_mips_rpt(
			.gpio_3t0		(gpio_3t0		),
			.rpt_data_31t0	(debug_data		),
			.clk			(error_rpt_clk	),
			.rst_           (error_rpt_rst_	)
			);
`endif

/***************************************************************
 * history:
 * 06/15/2000        Initial version by Farmer
 *					 add GPR ERF TLB CP0 CP1 here
 ***************************************************************/
`define cp0_path                `top_path.t2_main_ctrl.cop_excp.cp0_1.cpx_reg
`define cp1_path                `top_path.t2_main_ctrl.cop_excp.cp0_1.cpx_ctrl
`define gpr_path                `top_path.t2_reg_file.gpr
`define gpr_content_path        `top_path.t2_reg_file
`define erfh_path               `top_path.t2_vrf.erfh
`define erfl_path               `top_path.t2_vrf.erfl
//`define hi_path                 t2_cpu.CPU_CORE.CPU_TOP.T2_EXE_ALU.ALU_MULT.HI
//`define lo_path                 t2_cpu.CPU_CORE.CPU_TOP.T2_EXE_ALU.ALU_MULT.LO
`define ldst_data               `top_path.t2_ldst_buf.buf_flip
`define biu_path                `top_path.t2_biu_risc.biu_ctrl
`define path_main_ctrl  		`top_path.t2_main_ctrl.pp_ctrl.pipe_ctrl
`define path_mbus               top.CHIP.CORE.t2_cpu.cpu_core.t2_m_bus
//`define path_excp               t2_cpu.CPU_CORE.CPU_TOP.T2_MAIN_CTRL.COP_EXCP.EXCP
//`define tlb_data_path           t2_cpu.T2_MMU.TLB_HB.TLB_DATA
//`define tlb_tag_path            t2_cpu.T2_MMU.TLB_HB.TLB_TAG.TLB_TAG_REG

`ifdef AMPKG
initial begin
/*
`cpu_path.GPR_00_content = 32'h0	;
`cpu_path.GPR_01_content = 32'h0	;
`cpu_path.GPR_02_content = 32'h0	;
`cpu_path.GPR_03_content = 32'h0	;
`cpu_path.GPR_04_content = 32'h0	;
`cpu_path.GPR_05_content = 32'h0	;
`cpu_path.GPR_06_content = 32'h0	;
`cpu_path.GPR_07_content = 32'h0	;
`cpu_path.GPR_08_content = 32'h0	;
`cpu_path.GPR_09_content = 32'h0	;
`cpu_path.GPR_10_content = 32'h0	;
`cpu_path.GPR_11_content = 32'h0	;
`cpu_path.GPR_12_content = 32'h0	;
`cpu_path.GPR_13_content = 32'h0	;
`cpu_path.GPR_14_content = 32'h0	;
`cpu_path.GPR_15_content = 32'h0	;
`cpu_path.GPR_16_content = 32'h0	;
`cpu_path.GPR_17_content = 32'h0	;
`cpu_path.GPR_18_content = 32'h0	;
`cpu_path.GPR_19_content = 32'h0	;
`cpu_path.GPR_20_content = 32'h0	;
`cpu_path.GPR_21_content = 32'h0	;
`cpu_path.GPR_22_content = 32'h0	;
`cpu_path.GPR_23_content = 32'h0	;
`cpu_path.GPR_24_content = 32'h0	;
`cpu_path.GPR_25_content = 32'h0	;
`cpu_path.GPR_26_content = 32'h0	;
`cpu_path.GPR_27_content = 32'h0	;
`cpu_path.GPR_28_content = 32'h0	;
`cpu_path.GPR_29_content = 32'h0	;
`cpu_path.GPR_30_content = 32'h0	;
`cpu_path.GPR_31_content = 32'h0	;
*/
end
`else
integer mm;
initial begin
       for (mm=0; mm<32; mm=mm+1)
           `gpr_path.mem[mm] = 32'h0;
end
`endif

/******************************************************
* T2-RISC core begin
******************************************************/
//### Monitor the PC and IR.
//### Migrate from the CPU Env. yuky, 2002-07-31

integer inst_cnt, dump_num;
reg		dump_enable;
initial begin
	inst_cnt = 32'b0;
	dump_num = 0;
	dump_enable = 1;
end

wire pd_valid,pe_valid,pc_valid,pw_v;
wire	cw_word_arrival;
//instruction sequence monitor
wire	[31:0]	fd_instout;
reg		[31:0]	ir_out;

wire  [31:0]  gw_pc;
wire  [31:0]  gw_pc_dump = 	cw_word_arrival?
			   				gw_pc+4:
			   				gw_pc;

`ifdef	AMPKG
	assign	cw_word_arrival = `cpu_path.cw_word_arrival ;
	assign	fd_instout		= `cpu_path.fd_instruction_out_0; //change at 2002-08-22, yuky
	assign	pd_valid		= `cpu_path.pd_valid;
	assign	pe_valid        = `cpu_path.pe_valid;
	assign	pc_valid        = `cpu_path.pc_valid;
	assign 	gw_pc 			= `cpu_path.gw_pc;
	assign 	pw_v  			= `cpu_path.pw_v;
	wire	pw_valid 		= `cpu_path.pw_valid;
`else
	assign	cw_word_arrival = `d_cache_con_path.cw_word_arrival ;
	assign	fd_instout		= `top_path.fd_instruction_out_3; //change at 2002-08-22, yuky
	assign	pd_valid		= `top_path.t2_main_ctrl.pd_valid;
	assign	pe_valid        = `top_path.t2_main_ctrl.pe_valid;
	assign	pc_valid        = `top_path.t2_main_ctrl.pc_valid;
	assign 	gw_pc 			= `top_path.t2_main_ctrl.gw_pc;
	assign 	pw_v  			= `top_path.t2_main_ctrl.pw_v;
	wire	pw_valid 		= `top_path.t2_main_ctrl.pw_valid;
`endif
reg	[31:0]	e_id_inst;
reg	[31:0]	c_id_inst;
reg	[31:0]	w_id_inst;

always @(fd_instout)
begin
	restore_ir(fd_instout,ir_out);
end

always @(posedge clk ) begin
	if( pd_valid )
		e_id_inst	<= #1 ir_out;
end

always @(posedge clk ) begin
	if( pe_valid )
		c_id_inst	<= #1 e_id_inst;
end

always @(posedge clk ) begin
	if( pc_valid )
		w_id_inst	<= #1 c_id_inst;
end

always @(posedge clk) begin
	#0.2 if(warm_reset_ & cold_reset_ & pw_v) begin
		inst_cnt = inst_cnt + 1;
//		`ifdef  DUMP_DEBUG
		if(dump_enable)
			dump_num = dump_num + 1;
//		$display("********* Current Inst counter : %d   Dump No.: %d ***********",
//		  inst_cnt, dump_num);
`ifdef	MASK_DISP_CPU_PC
`else
		#0.2 $display("\t\tPC=%h \tIR=%h \tDump No.:", gw_pc_dump, w_id_inst, dump_num);
`endif
//		`endif
	end
end


/*----------------------------------------------------
       Define task
------------------------------------------------------*/

task    restore_ir;
input   [31:0]          ir_in;
output  [31:0]          ir_out;
reg     [5:0]           opcode;
reg     [8:0]           funcode;
reg     [2:0]           order_cmd;
begin
        opcode = ir_in[31:26];
        funcode = {ir_in[25:21],ir_in[3:0]};

                casex({opcode,funcode})
                15'b001xxxxxxxxxxxx :
                    order_cmd = 3'b001;     //immediate number instructions
                15'b010001xxxxx0100 :       // cvt.w.fmt
                    order_cmd = 3'b110;
                15'b100x0xxxxxxxxxx :
                    order_cmd = 3'b001;     //load instructions
                15'b100x11xxxxxxxxx :
                    order_cmd = 3'b001;     //load instructions
                15'b110000xxxxxxxxx :
                    order_cmd = 3'b001;     //ll
                15'b110001xxxxxxxxx :
                    order_cmd = 3'b001;     //lwc1/lwc2
                15'b110010xxxxxxxxx :
                    order_cmd = 3'b001;
                15'b011101xxxxxxxxx :
                    order_cmd = 3'b001;     //lwc2c
                15'b01000000000xxxx :
                    order_cmd = 3'b001;     //mfc0/mfc1/mfc2
                15'b01000100000xxxx :
                    order_cmd = 3'b001;
                15'b01001000000xxxx :
                    order_cmd = 3'b001;
                15'b01000100010xxxx :
                    order_cmd = 3'b001;     //cfc1/cfc2
                15'b01001000010xxxx :
                    order_cmd = 3'b001;
                15'b010011xxxxx0000 :
                    order_cmd = 3'b010;     //lwxc1
                15'b010011xxxxx0001 :
                    order_cmd = 3'b011;     //ldxc1
                15'b110101xxxxxxxxx :
                    order_cmd = 3'b100;     //ldc1/ldc2
                15'b110110xxxxxxxxx :
                    order_cmd = 3'b100;
                15'b111101xxxxxxxxx :
                    order_cmd = 3'b101;     //sdc1/sdc2
                15'b111110xxxxxxxxx :
                    order_cmd = 3'b101;
                default :
                    order_cmd = 3'b000;
                endcase
        case(order_cmd)
                3'b001 :
                        ir_out = {ir_in[31:21],ir_in[15:11],ir_in[20:16],ir_in[10:0]};
                3'b010 :
                        ir_out = {ir_in[31:16],ir_in[10:6],ir_in[15:11],ir_in[5:0]};
                3'b011 :
                        ir_out = {ir_in[31:16],ir_in[10:6],ir_in[11],ir_in[15:12],ir_in[5:0]};
                3'b100 :
                        ir_out = {ir_in[31:21],ir_in[11],ir_in[15:12],ir_in[20:16],ir_in[10:0]};
                3'b101 :
                        ir_out = {ir_in[31:21],ir_in[16],ir_in[20:17],ir_in[15:0]};
                3'b110 :
                        ir_out = {ir_in[31:21],ir_in[15:6],ir_in[20:16],ir_in[5:0]};
                default :
                        ir_out = ir_in;
        endcase

end
endtask

//################################################################
//####	Monitor the simulation End condition 				######
//####	Compatible with SH, yuky, 2002-07-31				######
//################################################################

always @(w_id_inst or pw_valid)
begin
	#1.0;
	if ((w_id_inst == 32'h3c000000 | w_id_inst == 32'h24000000) & (pw_valid))
		begin
		#5.0;
		cpu_vec_end = 1'b1;
		cpu_vec_00_end = 1'b1;
		cpu_vec_01_end = 1'b1;
		cpu_vec_02_end = 1'b1;
		cpu_vec_03_end = 1'b1;
		cpu_vec_04_end = 1'b1;
		cpu_vec_05_end = 1'b1;
		cpu_vec_06_end = 1'b1;
		cpu_vec_07_end = 1'b1;
		cpu_vec_08_end = 1'b1;
		cpu_vec_09_end = 1'b1;
		cpu_vec_10_end = 1'b1;
		cpu_vec_11_end = 1'b1;
		cpu_vec_12_end = 1'b1;
		cpu_vec_13_end = 1'b1;
		cpu_vec_14_end = 1'b1;
		cpu_vec_15_end = 1'b1;
		cpu_vec_16_end = 1'b1;
		cpu_vec_17_end = 1'b1;
		cpu_vec_18_end = 1'b1;
		cpu_vec_19_end = 1'b1;
		cpu_vec_20_end = 1'b1;
		end
end


/*-----------------------------------------------------
                 intial cache
-------------------------------------------------------*/
/*
`ifdef POST_GSIM
always @(posedge cold_reset_)
begin
#10
                for (i=0;i<512;i=i+1)
                begin
                        `top_path.D_TAG_0.u0.mem_core_array[i] = 24'b0;
                        `top_path.I_TAG_0.u0.mem_core_array[i] = 22'b0;
                        `top_path.D_TAG_1.u0.mem_core_array[i] = 24'b0;
                        `top_path.I_TAG_1.u0.mem_core_array[i] = 22'b0;
                end

end
`endif
*/
/*
always @(posedge rst_) begin
#10
    for(i=0; i<512; i=i+1) begin
	`cpu_path.d_tag_0.mem[i] = 24'b0;
	`cpu_path.d_tag_1.mem[i] = 24'b0;
	`cpu_path.i_tag_0.mem[i] = 23'b0;
	`cpu_path.i_tag_1.mem[i] = 23'b0;
    end
end
*/
/*
always @(posedge rst_) begin
#10
    for(i=0; i<512; i=i+1) begin
	`cpu_path.d_tag_0.u0.mem_core_array[i] = 24'b0;
	`cpu_path.d_tag_1.u0.mem_core_array[i] = 24'b0;
	`cpu_path.i_tag_0.u0.mem_core_array[i] = 22'b0;
	`cpu_path.i_tag_1.u0.mem_core_array[i] = 22'b0;
    end
end

*/
