//*****************************************************************
// this file only includes Multi-threading CPU(behavior model) tasks
// Author:	yuky
// History: 2002-05-16 	initial version, write 4*3 interrupt tasks and
//						4*3*16 common processor tasks
//			2002-05-16  add two cpu busrt write task(2w and 4w), tod
/* tasks list:
CPU basic operation tasks

	int_cpu_write1w;
	int_cpu_write2w;
	int_cpu_write4w;
	int_cpu_write8w;
	int_cpu_readbk1w;
	int_cpu_readbk2w;
	int_cpu_readbk4w;
	int_cpu_readbk8w;
	int_cpu_read1w;
	int_cpu_read2w;
	int_cpu_read4w;
	int_cpu_read8w;

	pro00_cpu_write1w
	pro00_cpu_write2w
	pro00_cpu_write4w_new
	pro00_cpu_write2w_new
	pro00_cpu_write4w
	pro00_cpu_write8w
	pro00_cpu_readbk1w
	pro00_cpu_readbk2w
	pro00_cpu_readbk4w
	pro00_cpu_readbk8w
	pro00_cpu_read1w
	pro00_cpu_read2w
	pro00_cpu_read4w
	pro00_cpu_read8w

	pro01_cpu_write1w
	pro01_cpu_write2w
	pro01_cpu_write4w
	pro01_cpu_write8w
	pro01_cpu_readbk1w
	pro01_cpu_readbk2w
	pro01_cpu_readbk4w
	pro01_cpu_readbk8w
	pro01_cpu_read1w
	pro01_cpu_read2w
	pro01_cpu_read4w
	pro01_cpu_read8w

	pro02_cpu_write1w
	pro02_cpu_write2w
	pro02_cpu_write4w
	pro02_cpu_write8w
	pro02_cpu_readbk1w
	pro02_cpu_readbk2w
	pro02_cpu_readbk4w
	pro02_cpu_readbk8w
	pro02_cpu_read1w
	pro02_cpu_read2w
	pro02_cpu_read4w
	pro02_cpu_read8w

	pro03_cpu_write1w
	pro03_cpu_write2w
	pro03_cpu_write4w
	pro03_cpu_write8w
	pro03_cpu_readbk1w
	pro03_cpu_readbk2w
	pro03_cpu_readbk4w
	pro03_cpu_readbk8w
	pro03_cpu_read1w
	pro03_cpu_read2w
	pro03_cpu_read4w
	pro03_cpu_read8w

	pro04_cpu_write1w
	pro04_cpu_write2w
	pro04_cpu_write4w
	pro04_cpu_write8w
	pro04_cpu_readbk1w
	pro04_cpu_readbk2w
	pro04_cpu_readbk4w
	pro04_cpu_readbk8w
	pro04_cpu_read1w
	pro04_cpu_read2w
	pro04_cpu_read4w
	pro04_cpu_read8w

	pro05_cpu_write1w
	pro05_cpu_write2w
	pro05_cpu_write4w
	pro05_cpu_write8w
	pro05_cpu_readbk1w
	pro05_cpu_readbk2w
	pro05_cpu_readbk4w
	pro05_cpu_readbk8w
	pro05_cpu_read1w
	pro05_cpu_read2w
	pro05_cpu_read4w
	pro05_cpu_read8w

	pro06_cpu_write1w
	pro06_cpu_write2w
	pro06_cpu_write4w
	pro06_cpu_write8w
	pro06_cpu_readbk1w
	pro06_cpu_readbk2w
	pro06_cpu_readbk4w
	pro06_cpu_readbk8w
	pro06_cpu_read1w
	pro06_cpu_read2w
	pro06_cpu_read4w
	pro06_cpu_read8w

	pro07_cpu_write1w
	pro07_cpu_write2w
	pro07_cpu_write4w
	pro07_cpu_write8w
	pro07_cpu_readbk1w
	pro07_cpu_readbk2w
	pro07_cpu_readbk4w
	pro07_cpu_readbk8w
	pro07_cpu_read1w
	pro07_cpu_read2w
	pro07_cpu_read4w
	pro07_cpu_read8w

	pro08_cpu_write1w
	pro08_cpu_write2w
	pro08_cpu_write4w
	pro08_cpu_write8w
	pro08_cpu_readbk1w
	pro08_cpu_readbk2w
	pro08_cpu_readbk4w
	pro08_cpu_readbk8w
	pro08_cpu_read1w
	pro08_cpu_read2w
	pro08_cpu_read4w
	pro08_cpu_read8w

	pro09_cpu_write1w
	pro09_cpu_write2w
	pro09_cpu_write4w
	pro09_cpu_write8w
	pro09_cpu_readbk1w
	pro09_cpu_readbk2w
	pro09_cpu_readbk4w
	pro09_cpu_readbk8w
	pro09_cpu_read1w
	pro09_cpu_read2w
	pro09_cpu_read4w
	pro09_cpu_read8w

	pro10_cpu_write1w
	pro10_cpu_write2w
	pro10_cpu_write4w
	pro10_cpu_write8w
	pro10_cpu_readbk1w
	pro10_cpu_readbk2w
	pro10_cpu_readbk4w
	pro10_cpu_readbk8w
	pro10_cpu_read1w
	pro10_cpu_read2w
	pro10_cpu_read4w
	pro10_cpu_read8w

	pro11_cpu_write1w
	pro11_cpu_write2w
	pro11_cpu_write4w
	pro11_cpu_write8w
	pro11_cpu_readbk1w
	pro11_cpu_readbk2w
	pro11_cpu_readbk4w
	pro11_cpu_readbk8w
	pro11_cpu_read1w
	pro11_cpu_read2w
	pro11_cpu_read4w
	pro11_cpu_read8w

	pro12_cpu_write1w
	pro12_cpu_write2w
	pro12_cpu_write4w
	pro12_cpu_write8w
	pro12_cpu_readbk1w
	pro12_cpu_readbk2w
	pro12_cpu_readbk4w
	pro12_cpu_readbk8w
	pro12_cpu_read1w
	pro12_cpu_read2w
	pro12_cpu_read4w
	pro12_cpu_read8w

	pro13_cpu_write1w
	pro13_cpu_write2w
	pro13_cpu_write4w
	pro13_cpu_write8w
	pro13_cpu_readbk1w
	pro13_cpu_readbk2w
	pro13_cpu_readbk4w
	pro13_cpu_readbk8w
	pro13_cpu_read1w
	pro13_cpu_read2w
	pro13_cpu_read4w
	pro13_cpu_read8w

	pro14_cpu_write1w
	pro14_cpu_write2w
	pro14_cpu_write4w
	pro14_cpu_write8w
	pro14_cpu_readbk1w
	pro14_cpu_readbk2w
	pro14_cpu_readbk4w
	pro14_cpu_readbk8w
	pro14_cpu_read1w
	pro14_cpu_read2w
	pro14_cpu_read4w
	pro14_cpu_read8w

	pro15_cpu_write1w
	pro15_cpu_write2w
	pro15_cpu_write4w
	pro15_cpu_write8w
	pro15_cpu_readbk1w
	pro15_cpu_readbk2w
	pro15_cpu_readbk4w
	pro15_cpu_readbk8w
	pro15_cpu_read1w
	pro15_cpu_read2w
	pro15_cpu_read4w
	pro15_cpu_read8w

	pro16_cpu_write1w
	pro16_cpu_write2w
	pro16_cpu_write4w
	pro16_cpu_write8w
	pro16_cpu_readbk1w
	pro16_cpu_readbk2w
	pro16_cpu_readbk4w
	pro16_cpu_readbk8w
	pro16_cpu_read1w
	pro16_cpu_read2w
	pro16_cpu_read4w
	pro16_cpu_read8w

	pro17_cpu_write1w
	pro17_cpu_write2w
	pro17_cpu_write4w
	pro17_cpu_write8w
	pro17_cpu_readbk1w
	pro17_cpu_readbk2w
	pro17_cpu_readbk4w
	pro17_cpu_readbk8w
	pro17_cpu_read1w
	pro17_cpu_read2w
	pro17_cpu_read4w
	pro17_cpu_read8w

	pro18_cpu_write1w
	pro18_cpu_write2w
	pro18_cpu_write4w
	pro18_cpu_write8w
	pro18_cpu_readbk1w
	pro18_cpu_readbk2w
	pro18_cpu_readbk4w
	pro18_cpu_readbk8w
	pro18_cpu_read1w
	pro18_cpu_read2w
	pro18_cpu_read4w
	pro18_cpu_read8w

	pro19_cpu_write1w
	pro19_cpu_write2w
	pro19_cpu_write4w
	pro19_cpu_write8w
	pro19_cpu_readbk1w
	pro19_cpu_readbk2w
	pro19_cpu_readbk4w
	pro19_cpu_readbk8w
	pro19_cpu_read1w
	pro19_cpu_read2w
	pro19_cpu_read4w
	pro19_cpu_read8w

	pro20_cpu_write1w
	pro20_cpu_write2w
	pro20_cpu_write4w
	pro20_cpu_write8w
	pro20_cpu_readbk1w
	pro20_cpu_readbk2w
	pro20_cpu_readbk4w
	pro20_cpu_readbk8w
	pro20_cpu_read1w
	pro20_cpu_read2w
	pro20_cpu_read4w
	pro20_cpu_read8w


	pro00_req_lock;
	pro00_req_unlock;
	pro01_req_lock;
	pro01_req_unlock;
	pro02_req_lock;
	pro02_req_unlock;
	pro03_req_lock;
	pro03_req_unlock;
	pro04_req_lock;
	pro04_req_unlock;
	pro05_req_lock;
	pro05_req_unlock;
	pro06_req_lock;
	pro06_req_unlock;
	pro07_req_lock;
	pro07_req_unlock;
	pro08_req_lock;
	pro08_req_unlock;
	pro09_req_lock;
	pro09_req_unlock;
	pro10_req_lock;
	pro10_req_unlock;
	pro11_req_lock;
	pro11_req_unlock;
	pro12_req_lock;
	pro12_req_unlock;
	pro13_req_lock;
	pro13_req_unlock;
	pro14_req_lock;
	pro14_req_unlock;
	pro15_req_lock;
	pro15_req_unlock;
	pro16_req_lock;
	pro16_req_unlock;
	pro17_req_lock;
	pro17_req_unlock;
	pro18_req_lock;
	pro18_req_unlock;
	pro19_req_lock;
	pro19_req_unlock;
	pro20_req_lock;
	pro20_req_unlock;
	int_req_lock;
	int_req_unlock;

*/
`ifdef FDISP_PRO00
	integer f_pro00_msg;
	initial f_pro00_msg = $fopen("pro00.rpt");
`endif

`ifdef FDISP_PRO01
	integer f_pro01_msg;
	initial f_pro01_msg = $fopen("pro01.rpt");
`endif

`ifdef FDISP_PRO02
	integer f_pro02_msg;
	initial f_pro02_msg = $fopen("pro02.rpt");
`endif

`ifdef FDISP_PRO03
	integer f_pro03_msg;
	initial f_pro03_msg = $fopen("pro03.rpt");
`endif

`ifdef FDISP_PRO04
	integer f_pro04_msg;
	initial f_pro04_msg = $fopen("pro04.rpt");
`endif

`ifdef FDISP_PRO05
	integer f_pro05_msg;
	initial f_pro05_msg = $fopen("pro05.rpt");
`endif

`ifdef FDISP_PRO06
	integer f_pro06_msg;
	initial f_pro06_msg = $fopen("pro06.rpt");
`endif

`ifdef FDISP_PRO07
	integer f_pro07_msg;
	initial f_pro07_msg = $fopen("pro07.rpt");
`endif

`ifdef FDISP_PRO08
	integer f_pro08_msg;
	initial f_pro08_msg = $fopen("pro08.rpt");
`endif

`ifdef FDISP_PRO09
	integer f_pro09_msg;
	initial f_pro09_msg = $fopen("pro09.rpt");
`endif

`ifdef FDISP_PRO10
	integer f_pro10_msg;
	initial f_pro10_msg = $fopen("pro10.rpt");
`endif

`ifdef FDISP_PRO11
	integer f_pro11_msg;
	initial f_pro11_msg = $fopen("pro11.rpt");
`endif

`ifdef FDISP_PRO12
	integer f_pro12_msg;
	initial f_pro12_msg = $fopen("pro12.rpt");
`endif

`ifdef FDISP_PRO13
	integer f_pro13_msg;
	initial f_pro13_msg = $fopen("pro13.rpt");
`endif

`ifdef FDISP_PRO14
	integer f_pro14_msg;
	initial f_pro14_msg = $fopen("pro14.rpt");
`endif

`ifdef FDISP_PRO15
	integer f_pro15_msg;
	initial f_pro15_msg = $fopen("pro15.rpt");
`endif

`ifdef FDISP_PRO16
	integer f_pro16_msg;
	initial f_pro16_msg = $fopen("pro16.rpt");
`endif

`ifdef FDISP_PRO17
	integer f_pro17_msg;
	initial f_pro17_msg = $fopen("pro17.rpt");
`endif

`ifdef FDISP_PRO18
	integer f_pro18_msg;
	initial f_pro18_msg = $fopen("pro18.rpt");
`endif

//`ifdef FDISP_PROINT
//	integer f_proint_msg;
//	initial f_proint_msg = $fopen("proint.rpt");
//`endif
//
`ifdef FDISP_PRO19
	integer f_pro19_msg;
	initial f_pro19_msg = $fopen("pro19.rpt");
`endif

`ifdef FDISP_PRO20
	integer f_pro20_msg;
	initial f_pro20_msg = $fopen("pro20.rpt");
`endif

`ifdef FDISP_PROINT
	integer f_proint_msg;
	initial f_proint_msg = $fopen("proint.rpt");
`endif
//=============== the basic task for int processer ========================
task	int_cpu_write1w;
input	[31:0]	addr;
input	[3:0]	byte;
input	[31:0] 	data;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.int_tasks.write_1w (addr,byte,data);
	$display("CPU write(int  ):\t\tADDR =%h, byte =%h, D =%h",addr,byte,data);
	`ifdef FDISP_PROINT
	$fdisplay(f_proint_msg,"CPU write(int  ):\t\tADDR =%h, byte =%h, D =%h",addr,byte,data);
	`endif
`endif
end
endtask

task	int_cpu_write2w;
input	[31:0]	addr;
input	[31:0] 	data0,data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.int_tasks.write_2w (addr,data0,data1);
	$display("CPU write(int  ):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`ifdef FDISP_PROINT
	$fdisplay(f_proint_msg,"CPU write(int  ):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`endif
`endif
end
endtask

task	int_cpu_write4w;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.int_tasks.write_4w (addr,data0,data1,data2,data3);
	$display("CPU write(int  ):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`ifdef FDISP_PROINT
	$fdisplay(f_proint_msg,"CPU write(int  ):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`endif
`endif
end
endtask

task	int_cpu_write8w;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
input	[31:0] 	data4,data5,data6,data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.int_tasks.write_8w (addr,data0,data1,data2,data3,data4,data5,data6,data7);
	$display("CPU write(int  ):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	$display("\t\t\t\t\t\tD4 =%h, D5 =%h, D6 =%h, D7 =%h",data4,data5,data6,data7);
	`ifdef FDISP_PROINT
	$fdisplay(f_proint_msg,"CPU write(int  ):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	$fdisplay(f_proint_msg,"\t\t\t\t\t\tD4 =%h, D5 =%h, D6 =%h, D7 =%h",data4,data5,data6,data7);
	`endif
`endif
end
endtask

task	int_cpu_readbk1w;
input	[31:0]	addr;
input	[3:0]	byte;
inout	[31:0]	tmp_data;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.int_tasks.read_1w (addr,byte,tmp_data);
	$display("CPU read(int  ):\t\tADDR =%h, byte =%h, D =%h",addr,byte,tmp_data);
	`ifdef FDISP_PROINT
	$fdisplay(f_proint_msg,"CPU read(int  ):\t\tADDR =%h, byte =%h, D =%h",addr,byte,tmp_data);
	`endif
`else
	tmp_data = 32'h0;
`endif
end
endtask

task	int_cpu_readbk2w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.int_tasks.read_2w (addr,tmp_data0,tmp_data1);
	$display("CPU read(int  ):\t\tADDR =%h, D0 =%h, D1 =%H",addr,tmp_data0,tmp_data1 );
	`ifdef FDISP_PROINT
	$fdisplay(f_proint_msg,"CPU read(int  ):\t\tADDR =%h, D0 =%h, D1 =%H",addr,tmp_data0,tmp_data1 );
	`endif
`endif
end
endtask

task	int_cpu_readbk4w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1,tmp_data2,tmp_data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.int_tasks.read_4w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("CPU read(int  ):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`ifdef FDISP_PROINT
	$fdisplay(f_proint_msg,"CPU read(int  ):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`endif
`endif
end
endtask

task	int_cpu_readbk8w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1,tmp_data2,tmp_data3;
inout	[31:0]	tmp_data4,tmp_data5,tmp_data6,tmp_data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.int_tasks.read_8w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	$display("CPU read(int  ):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("\t\t\t\t\t\tD4 =%h, D5 =%H, D6 =%h, D7 =%H",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`ifdef FDISP_PROINT
	$fdisplay(f_proint_msg,"CPU read(int  ):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$fdisplay(f_proint_msg,"\t\t\t\t\t\tD4 =%h, D5 =%H, D6 =%h, D7 =%H",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`endif
`endif
end
endtask

task	int_cpu_read1w;
input	[31:0]	addr;
input	[3:0]	byte;
input	[31:0]	exp_data;

reg	[31:0]	cpu_temp;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.int_tasks.read_1w (addr,byte,cpu_temp);
	cpu_temp = cpu_temp & {{8{byte[3]}},{8{byte[2]}},{8{byte[1]}},{8{byte[0]}}};
	if (cpu_temp !== ({{8{byte[3]}},{8{byte[2]}},{8{byte[1]}},{8{byte[0]}}} & exp_data)) begin
	  	$display("Error_T2_rsim:CPU read(int  )\t%0t ADDR =%h, byte =%h, RDBK--D =%h, EXP--D =%h",
		$realtime,addr,byte,cpu_temp,exp_data);
		`ifdef FDISP_PROINT
		$fdisplay(f_proint_msg,"Error_T2_rsim:CPU read(int  )\t%0t ADDR =%h, byte =%h, RDBK--D =%h, EXP--D =%h",
		$realtime,addr,byte,cpu_temp,exp_data);
		`endif
	end else begin
	$display("CPU read(int  ):\t\tADDR =%h, byte =%h, D =%h",addr,byte,cpu_temp);
	`ifdef FDISP_PROINT
	$fdisplay(f_proint_msg,"CPU read(int  ):\t\tADDR =%h, byte =%h, D =%h",addr,byte,cpu_temp);
	`endif
	end
`endif
end
endtask

task	int_cpu_read2w;
input	[31:0]	addr;
input	[31:0]	exp_data0;
input	[31:0]	exp_data1;
reg	[31:0]	tmp_data0,
			tmp_data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.int_tasks.read_2w (addr,tmp_data0,tmp_data1);
	if(tmp_data0!==exp_data0 || tmp_data1!==exp_data1) begin
	  	$display("Error_T2_rsim:CPU read(int  )\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h",
		$realtime,addr,tmp_data0,tmp_data1,exp_data0,exp_data1);
		`ifdef FDISP_PROINT
		$fdisplay(f_proint_msg,"Error_T2_rsim:CPU read(int  )\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h",
		$realtime,addr,tmp_data0,tmp_data1,exp_data0,exp_data1);
		`endif
	end else begin
	$display("CPU read(int  ):\t\tADDR =%h, D0 =%h, D1 =%h",addr,tmp_data0,tmp_data1);
	`ifdef FDISP_PROINT
	$fdisplay(f_proint_msg,"CPU read(int  ):\t\tADDR =%h, D0 =%h, D1 =%h",addr,tmp_data0,tmp_data1);
	`endif
	end
`endif
end
endtask

task	int_cpu_read4w;
input	[31:0]	addr;
input	[31:0]	exp_data0,
		exp_data1,
		exp_data2,
		exp_data3;
reg	[31:0]	tmp_data0,
		tmp_data1,
		tmp_data2,
		tmp_data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.int_tasks.read_4w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	if(tmp_data0!==exp_data0||tmp_data1!==exp_data1||
	   tmp_data2!==exp_data2||tmp_data3!==exp_data3) begin
	  	$display("Error_T2_rsim:CPU read(int  )\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		`ifdef FDISP_PROINT
		$fdisplay(f_proint_msg,"Error_T2_rsim:CPU read(int  )\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		`endif
	end else begin
	$display("CPU read(int  ):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`ifdef FDISP_PROINT
	$fdisplay(f_proint_msg,"CPU read(int  ):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`endif
	end
`endif
end
endtask

task	int_cpu_read8w;
input	[31:0]	addr;
input	[31:0]	exp_data0,
		exp_data1,
		exp_data2,
		exp_data3,
		exp_data4,
		exp_data5,
		exp_data6,
		exp_data7;
reg	[31:0]	tmp_data0,
		tmp_data1,
		tmp_data2,
		tmp_data3,
		tmp_data4,
		tmp_data5,
		tmp_data6,
		tmp_data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.int_tasks.read_8w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	if(tmp_data0!==exp_data0||tmp_data1!==exp_data1|| tmp_data2!==exp_data2||tmp_data3!==exp_data3 ||
	   tmp_data4!==exp_data4||tmp_data5!==exp_data5|| tmp_data6!==exp_data6||tmp_data7!==exp_data7)
	   begin
	  	$display("Error_T2_rsim:CPU read(int  )\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		$display("\t\t\t\t\t\tRDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		tmp_data4,tmp_data5,tmp_data6,tmp_data7,exp_data4,exp_data5,exp_data6,exp_data7);
		`ifdef FDISP_PROINT
		$fdisplay(f_proint_msg,"Error_T2_rsim:CPU read(int  )\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		$fdisplay(f_proint_msg,"\t\t\t\t\t\tRDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		tmp_data4,tmp_data5,tmp_data6,tmp_data7,exp_data4,exp_data5,exp_data6,exp_data7);
		`endif
	   end
	else begin
	$display("CPU read(int  ):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("\t\t\t\t\t\tD0 =%h, D1 =%h, D2 =%h, D3 =%h",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`ifdef FDISP_PROINT
	$fdisplay(f_proint_msg,"CPU read(int  ):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$fdisplay(f_proint_msg,"\t\t\t\t\t\tD0 =%h, D1 =%h, D2 =%h, D3 =%h",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`endif
	end
`endif
end
endtask

//=============== the basic task for pro00 processer ========================
task	pro00_cpu_write1w;
input	[31:0]	addr;
input	[3:0]	byte;
input	[31:0] 	data;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro00_tasks.write_1w (addr,byte,data);
	$display("CPU write(pro00):\t\tADDR =%h, byte =%h, D =%h",addr,byte,data);
	`ifdef FDISP_PRO00
	$fdisplay(f_pro00_msg,"CPU write(pro00):\t\tADDR =%h, byte =%h, D =%h",addr,byte,data);
	`endif
`endif
end
endtask

task	pro00_cpu_write2w;
input	[31:0]	addr;
input	[31:0] 	data0,data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro00_tasks.write_2w (addr,data0,data1);
	$display("CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`ifdef FDISP_PRO00
	$fdisplay(f_pro00_msg,"CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`endif
`endif
end
endtask

task	pro00_cpu_write4w;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro00_tasks.write_4w (addr,data0,data1,data2,data3);
	$display("CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`ifdef FDISP_PRO00
	$fdisplay(f_pro00_msg,"CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`endif
`endif
end
endtask

task	pro00_cpu_write8w;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
input	[31:0] 	data4,data5,data6,data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro00_tasks.write_8w (addr,data0,data1,data2,data3,data4,data5,data6,data7);
	$display("CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	$display("\t\t\t\t\t\tD4 =%h, D5 =%h, D6 =%h, D7 =%h",data4,data5,data6,data7);
	`ifdef FDISP_PRO00
	$fdisplay(f_pro00_msg,"CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	$fdisplay(f_pro00_msg,"\t\t\t\t\t\tD4 =%h, D5 =%h, D6 =%h, D7 =%h",data4,data5,data6,data7);
	`endif
`endif
end
endtask

task	pro00_cpu_write2w_new;
input	[31:0]	addr;
input	[31:0] 	data0,data1;
input	[3:0]	byte0,byte1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro00_tasks.write_2w_new (addr,data0,data1,byte0,byte1);
	$display("CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, byte0 = %h, byte1 = %h", addr,data0,data1,byte0,byte1);
	`ifdef FDISP_PRO00
//	$fdisplay(f_pro00_msg,"CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`endif
`endif
end
endtask

task	pro00_cpu_write4w_new;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
input	[3:0]	byte0,byte1,byte2,byte3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro00_tasks.write_4w_new (addr,data0,data1,data2,data3,byte0,byte1,byte2,byte3);
	$display("CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h, byte0 = %h, byte1 = %h, byte2 = %h, byte3 = %h",addr,data0,data1,data2,data3,byte0,byte1,byte2,byte3);
	`ifdef FDISP_PRO00
//	$fdisplay(f_pro00_msg,"CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`endif
`endif
end
endtask

task	pro00_cpu_readbk1w;
input	[31:0]	addr;
input	[3:0]	byte;
inout	[31:0]	tmp_data;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro00_tasks.read_1w (addr,byte,tmp_data);
	$display("CPU read(pro00):\t\tADDR =%h, byte =%h, D =%h",addr,byte,tmp_data);
	`ifdef FDISP_PRO00
	$fdisplay(f_pro00_msg,"CPU read(pro00):\t\tADDR =%h, byte =%h, D =%h",addr,byte,tmp_data);
	`endif
`else
	tmp_data = 32'h0;
`endif
end
endtask

task	pro00_cpu_readbk2w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro00_tasks.read_2w (addr,tmp_data0,tmp_data1);
	$display("CPU read(pro00):\t\tADDR =%h, D0 =%h, D1 =%H",addr,tmp_data0,tmp_data1 );
	`ifdef FDISP_PRO00
	$fdisplay(f_pro00_msg,"CPU read(pro00):\t\tADDR =%h, D0 =%h, D1 =%H",addr,tmp_data0,tmp_data1 );
	`endif
`endif
end
endtask

task	pro00_cpu_readbk4w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1,tmp_data2,tmp_data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro00_tasks.read_4w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("CPU read(pro00):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`ifdef FDISP_PRO00
	$fdisplay(f_pro00_msg,"CPU read(pro00):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`endif
`endif
end
endtask

task	pro00_cpu_readbk8w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1,tmp_data2,tmp_data3;
inout	[31:0]	tmp_data4,tmp_data5,tmp_data6,tmp_data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro00_tasks.read_8w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	$display("CPU read(pro00):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("\t\t\t\t\t\tD4 =%h, D5 =%H, D6 =%h, D7 =%H",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`ifdef FDISP_PRO00
	$fdisplay(f_pro00_msg,"CPU read(pro00):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$fdisplay(f_pro00_msg,"\t\t\t\t\t\tD4 =%h, D5 =%H, D6 =%h, D7 =%H",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`endif
`endif
end
endtask

task	pro00_cpu_read1w;
input	[31:0]	addr;
input	[3:0]	byte;
input	[31:0]	exp_data;

reg	[31:0]	cpu_temp;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro00_tasks.read_1w (addr,byte,cpu_temp);
	cpu_temp = cpu_temp & {{8{byte[3]}},{8{byte[2]}},{8{byte[1]}},{8{byte[0]}}};
	if (cpu_temp !== ({{8{byte[3]}},{8{byte[2]}},{8{byte[1]}},{8{byte[0]}}} & exp_data))begin
	  	$display("Error_T2_rsim:CPU read(pro00)\t%0t ADDR =%h, byte =%h, RDBK--D =%h, EXP--D =%h",
		$realtime,addr,byte,cpu_temp,exp_data);
		`ifdef FDISP_PRO00
		$fdisplay(f_pro00_msg,"Error_T2_rsim:CPU read(pro00)\t%0t ADDR =%h, byte =%h, RDBK--D =%h, EXP--D =%h",
		$realtime,addr,byte,cpu_temp,exp_data);
		`endif
	end else begin
	$display("CPU read(pro00):\t\tADDR =%h, byte =%h, D =%h",addr,byte,cpu_temp);
	`ifdef FDISP_PRO00
	$fdisplay(f_pro00_msg,"CPU read(pro00):\t\tADDR =%h, byte =%h, D =%h",addr,byte,cpu_temp);
	`endif
	end
`endif
end
endtask

task	pro00_cpu_read2w;
input	[31:0]	addr;
input	[31:0]	exp_data0;
input	[31:0]	exp_data1;
reg	[31:0]	tmp_data0,
			tmp_data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro00_tasks.read_2w (addr,tmp_data0,tmp_data1);
	if(tmp_data0!==exp_data0 || tmp_data1!==exp_data1) begin
	  	$display("Error_T2_rsim:CPU read(pro00)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h",
		$realtime,addr,tmp_data0,tmp_data1,exp_data0,exp_data1);
		`ifdef FDISP_PRO00
		$fdisplay(f_pro00_msg,"Error_T2_rsim:CPU read(pro00)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h",
		$realtime,addr,tmp_data0,tmp_data1,exp_data0,exp_data1);
		`endif
	end else begin
	$display("CPU read(pro00):\t\tADDR =%h, D0 =%h, D1 =%h",addr,tmp_data0,tmp_data1);
	`ifdef FDISP_PRO00
	$fdisplay(f_pro00_msg,"CPU read(pro00):\t\tADDR =%h, D0 =%h, D1 =%h",addr,tmp_data0,tmp_data1);
	`endif
	end
`endif
end
endtask

task	pro00_cpu_read4w;
input	[31:0]	addr;
input	[31:0]	exp_data0,
		exp_data1,
		exp_data2,
		exp_data3;
reg	[31:0]	tmp_data0,
		tmp_data1,
		tmp_data2,
		tmp_data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro00_tasks.read_4w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	if(tmp_data0!==exp_data0||tmp_data1!==exp_data1||
	   tmp_data2!==exp_data2||tmp_data3!==exp_data3) begin
	  	$display("Error_T2_rsim:CPU read(pro00)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		`ifdef FDISP_PRO00
		$fdisplay(f_pro00_msg,"Error_T2_rsim:CPU read(pro00)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		`endif
	end else begin
	$display("CPU read(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`ifdef FDISP_PRO00
	$fdisplay(f_pro00_msg,"CPU read(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`endif
	end
`endif
end
endtask

task	pro00_cpu_read8w;
input	[31:0]	addr;
input	[31:0]	exp_data0,
		exp_data1,
		exp_data2,
		exp_data3,
		exp_data4,
		exp_data5,
		exp_data6,
		exp_data7;
reg	[31:0]	tmp_data0,
		tmp_data1,
		tmp_data2,
		tmp_data3,
		tmp_data4,
		tmp_data5,
		tmp_data6,
		tmp_data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro00_tasks.read_8w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	if(tmp_data0!==exp_data0||tmp_data1!==exp_data1|| tmp_data2!==exp_data2||tmp_data3!==exp_data3 ||
	   tmp_data4!==exp_data4||tmp_data5!==exp_data5|| tmp_data6!==exp_data6||tmp_data7!==exp_data7)
	   begin
	  	$display("Error_T2_rsim:CPU read(pro00)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		$display("\t\t\t\t\t\tRDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		tmp_data4,tmp_data5,tmp_data6,tmp_data7,exp_data4,exp_data5,exp_data6,exp_data7);
		`ifdef FDISP_PRO00
		$fdisplay(f_pro00_msg,"Error_T2_rsim:CPU read(pro00)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		$fdisplay(f_pro00_msg,"\t\t\t\t\t\tRDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		tmp_data4,tmp_data5,tmp_data6,tmp_data7,exp_data4,exp_data5,exp_data6,exp_data7);
		`endif
	end
	else begin
	$display("CPU read(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("\t\t\t\t\t\tD0 =%h, D1 =%h, D2 =%h, D3 =%h",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`ifdef FDISP_PRO00
	$fdisplay(f_pro00_msg,"CPU read(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$fdisplay(f_pro00_msg,"\t\t\t\t\t\tD0 =%h, D1 =%h, D2 =%h, D3 =%h",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`endif
	end
`endif
end
endtask

//=============== the basic task for pro01 processer ========================
task	pro01_cpu_write1w;
input	[31:0]	addr;
input	[3:0]	byte;
input	[31:0] 	data;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro01_tasks.write_1w (addr,byte,data);
	$display("CPU write(pro01):\t\tADDR =%h, byte =%h, D =%h",addr,byte,data);
	`ifdef FDISP_PRO01
	$fdisplay(f_pro01_msg,"CPU write(pro01):\t\tADDR =%h, byte =%h, D =%h",addr,byte,data);
	`endif
`endif
end
endtask

task	pro01_cpu_write2w;
input	[31:0]	addr;
input	[31:0] 	data0,data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro01_tasks.write_2w (addr,data0,data1);
	$display("CPU write(pro01):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`ifdef FDISP_PRO01
	$fdisplay(f_pro01_msg,"CPU write(pro01):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`endif
`endif
end
endtask

task	pro01_cpu_write4w;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro01_tasks.write_4w (addr,data0,data1,data2,data3);
	$display("CPU write(pro01):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`ifdef FDISP_PRO01
	$fdisplay(f_pro01_msg,"CPU write(pro01):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`endif
`endif
end
endtask

task	pro01_cpu_write8w;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
input	[31:0] 	data4,data5,data6,data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro01_tasks.write_8w (addr,data0,data1,data2,data3,data4,data5,data6,data7);
	$display("CPU write(pro01):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	$display("\t\t\t\t\t\tD4 =%h, D5 =%h, D6 =%h, D7 =%h",data4,data5,data6,data7);
	`ifdef FDISP_PRO01
	$fdisplay(f_pro01_msg,"CPU write(pro01):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	$fdisplay(f_pro01_msg,"\t\t\t\t\t\tD4 =%h, D5 =%h, D6 =%h, D7 =%h",data4,data5,data6,data7);
	`endif
`endif
end
endtask

task	pro01_cpu_readbk1w;
input	[31:0]	addr;
input	[3:0]	byte;
inout	[31:0]	tmp_data;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro01_tasks.read_1w (addr,byte,tmp_data);
	$display("CPU read(pro01):\t\tADDR =%h, byte =%h, D =%h",addr,byte,tmp_data);
	`ifdef FDISP_PRO01
	$fdisplay(f_pro01_msg,"CPU read(pro01):\t\tADDR =%h, byte =%h, D =%h",addr,byte,tmp_data);
	`endif
`else
	tmp_data = 32'h0;
`endif
end
endtask

task	pro01_cpu_readbk2w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro01_tasks.read_2w (addr,tmp_data0,tmp_data1);
	$display("CPU read(pro01):\t\tADDR =%h, D0 =%h, D1 =%H",addr,tmp_data0,tmp_data1 );
	`ifdef FDISP_PRO01
	$fdisplay(f_pro01_msg,"CPU read(pro01):\t\tADDR =%h, D0 =%h, D1 =%H",addr,tmp_data0,tmp_data1 );
	`endif
`endif
end
endtask

task	pro01_cpu_readbk4w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1,tmp_data2,tmp_data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro01_tasks.read_4w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("CPU read(pro01):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`ifdef FDISP_PRO01
	$fdisplay(f_pro01_msg,"CPU read(pro01):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`endif
`endif
end
endtask

task	pro01_cpu_readbk8w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1,tmp_data2,tmp_data3;
inout	[31:0]	tmp_data4,tmp_data5,tmp_data6,tmp_data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro01_tasks.read_8w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	$display("CPU read(pro01):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("\t\t\t\t\t\tD4 =%h, D5 =%H, D6 =%h, D7 =%H",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`ifdef FDISP_PRO01
	$fdisplay(f_pro01_msg,"CPU read(pro01):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$fdisplay(f_pro01_msg,"\t\t\t\t\t\tD4 =%h, D5 =%H, D6 =%h, D7 =%H",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`endif
`endif
end
endtask

task	pro01_cpu_read1w;
input	[31:0]	addr;
input	[3:0]	byte;
input	[31:0]	exp_data;

reg	[31:0]	cpu_temp;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro01_tasks.read_1w (addr,byte,cpu_temp);
	cpu_temp = cpu_temp & {{8{byte[3]}},{8{byte[2]}},{8{byte[1]}},{8{byte[0]}}};
	if (cpu_temp !== ({{8{byte[3]}},{8{byte[2]}},{8{byte[1]}},{8{byte[0]}}} & exp_data)) begin
	  	$display("Error_T2_rsim:CPU read(pro01)\t%0t ADDR =%h, byte =%h, RDBK--D =%h, EXP--D =%h",
		$realtime,addr,byte,cpu_temp,exp_data);
		`ifdef FDISP_PRO01
		$fdisplay(f_pro01_msg,"Error_T2_rsim:CPU read(pro01)\t%0t ADDR =%h, byte =%h, RDBK--D =%h, EXP--D =%h",
		$realtime,addr,byte,cpu_temp,exp_data);
		`endif
	end else begin
	$display("CPU read(pro01):\t\tADDR =%h, byte =%h, D =%h",addr,byte,cpu_temp);
	`ifdef FDISP_PRO01
	$fdisplay(f_pro01_msg,"CPU read(pro01):\t\tADDR =%h, byte =%h, D =%h",addr,byte,cpu_temp);
	`endif
	end
`endif
end
endtask

task	pro01_cpu_read2w;
input	[31:0]	addr;
input	[31:0]	exp_data0;
input	[31:0]	exp_data1;
reg	[31:0]	tmp_data0,
			tmp_data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro01_tasks.read_2w (addr,tmp_data0,tmp_data1);
	if(tmp_data0!==exp_data0 || tmp_data1!==exp_data1) begin
	  	$display("Error_T2_rsim:CPU read(pro01)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h",
		$realtime,addr,tmp_data0,tmp_data1,exp_data0,exp_data1);
		`ifdef FDISP_PRO01
		$fdisplay(f_pro01_msg,"Error_T2_rsim:CPU read(pro01)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h",
		$realtime,addr,tmp_data0,tmp_data1,exp_data0,exp_data1);
		`endif
	end else begin
	$display("CPU read(pro01):\t\tADDR =%h, D0 =%h, D1 =%h",addr,tmp_data0,tmp_data1);
	`ifdef FDISP_PRO01
	$fdisplay(f_pro01_msg,"CPU read(pro01):\t\tADDR =%h, D0 =%h, D1 =%h",addr,tmp_data0,tmp_data1);
	`endif
	end
`endif
end
endtask

task	pro01_cpu_read4w;
input	[31:0]	addr;
input	[31:0]	exp_data0,
		exp_data1,
		exp_data2,
		exp_data3;
reg	[31:0]	tmp_data0,
		tmp_data1,
		tmp_data2,
		tmp_data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro01_tasks.read_4w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	if(tmp_data0!==exp_data0||tmp_data1!==exp_data1||
	   tmp_data2!==exp_data2||tmp_data3!==exp_data3) begin
	  	$display("Error_T2_rsim:CPU read(pro01)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		`ifdef FDISP_PRO01
		$fdisplay(f_pro01_msg,"Error_T2_rsim:CPU read(pro01)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		`endif
	end else begin
	$display("CPU read(pro01):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`ifdef FDISP_PRO01
	$fdisplay(f_pro01_msg,"CPU read(pro01):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`endif
	end
`endif
end
endtask

task	pro01_cpu_read8w;
input	[31:0]	addr;
input	[31:0]	exp_data0,
		exp_data1,
		exp_data2,
		exp_data3,
		exp_data4,
		exp_data5,
		exp_data6,
		exp_data7;
reg	[31:0]	tmp_data0,
		tmp_data1,
		tmp_data2,
		tmp_data3,
		tmp_data4,
		tmp_data5,
		tmp_data6,
		tmp_data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro01_tasks.read_8w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	if(tmp_data0!==exp_data0||tmp_data1!==exp_data1|| tmp_data2!==exp_data2||tmp_data3!==exp_data3 ||
	   tmp_data4!==exp_data4||tmp_data5!==exp_data5|| tmp_data6!==exp_data6||tmp_data7!==exp_data7)
	   begin
	  	$display("Error_T2_rsim:CPU read(pro01)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		$display("\t\t\t\t\t\tRDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		tmp_data4,tmp_data5,tmp_data6,tmp_data7,exp_data4,exp_data5,exp_data6,exp_data7);
		`ifdef FDISP_PRO01
		$fdisplay(f_pro01_msg,"Error_T2_rsim:CPU read(pro01)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		$fdisplay(f_pro01_msg,"\t\t\t\t\t\tRDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		tmp_data4,tmp_data5,tmp_data6,tmp_data7,exp_data4,exp_data5,exp_data6,exp_data7);
		`endif
	   end
	else begin
	$display("CPU read(pro01):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("\t\t\t\t\t\tD0 =%h, D1 =%h, D2 =%h, D3 =%h",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`ifdef FDISP_PRO01
	$fdisplay(f_pro01_msg,"CPU read(pro01):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$fdisplay(f_pro01_msg,"\t\t\t\t\t\tD0 =%h, D1 =%h, D2 =%h, D3 =%h",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`endif
	end
`endif
end
endtask

//=============== the basic task for pro02 processer ========================
task	pro02_cpu_write1w;
input	[31:0]	addr;
input	[3:0]	byte;
input	[31:0] 	data;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro02_tasks.write_1w (addr,byte,data);
	$display("CPU write(pro02):\t\tADDR =%h, byte =%h, D =%h",addr,byte,data);
	`ifdef FDISP_PRO02
	$fdisplay(f_pro02_msg,"CPU write(pro02):\t\tADDR =%h, byte =%h, D =%h",addr,byte,data);
	`endif
`endif
end
endtask

task	pro02_cpu_write2w;
input	[31:0]	addr;
input	[31:0] 	data0,data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro02_tasks.write_2w (addr,data0,data1);
	$display("CPU write(pro02):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`ifdef FDISP_PRO02
	$fdisplay(f_pro02_msg,"CPU write(pro02):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`endif
`endif
end
endtask

task	pro02_cpu_write4w;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro02_tasks.write_4w (addr,data0,data1,data2,data3);
	$display("CPU write(pro02):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`ifdef FDISP_PRO02
	$fdisplay(f_pro02_msg,"CPU write(pro02):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`endif
`endif
end
endtask

task	pro02_cpu_write8w;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
input	[31:0] 	data4,data5,data6,data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro02_tasks.write_8w (addr,data0,data1,data2,data3,data4,data5,data6,data7);
	$display("CPU write(pro02):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	$display("\t\t\t\t\t\tD4 =%h, D5 =%h, D6 =%h, D7 =%h",data4,data5,data6,data7);
	`ifdef FDISP_PRO02
	$fdisplay(f_pro02_msg,"CPU write(pro02):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	$fdisplay(f_pro02_msg,"\t\t\t\t\t\tD4 =%h, D5 =%h, D6 =%h, D7 =%h",data4,data5,data6,data7);
	`endif
`endif
end
endtask

task	pro02_cpu_readbk1w;
input	[31:0]	addr;
input	[3:0]	byte;
inout	[31:0]	tmp_data;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro02_tasks.read_1w (addr,byte,tmp_data);
	$display("CPU read(pro02):\t\tADDR =%h, byte =%h, D =%h",addr,byte,tmp_data);
	`ifdef FDISP_PRO02
	$fdisplay(f_pro02_msg,"CPU read(pro02):\t\tADDR =%h, byte =%h, D =%h",addr,byte,tmp_data);
	`endif
`else
	tmp_data = 32'h0;
`endif
end
endtask

task	pro02_cpu_readbk2w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro02_tasks.read_2w (addr,tmp_data0,tmp_data1);
	$display("CPU read(pro02):\t\tADDR =%h, D0 =%h, D1 =%H",addr,tmp_data0,tmp_data1 );
	`ifdef FDISP_PRO02
	$fdisplay(f_pro02_msg,"CPU read(pro02):\t\tADDR =%h, D0 =%h, D1 =%H",addr,tmp_data0,tmp_data1 );
	`endif
`endif
end
endtask

task	pro02_cpu_readbk4w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1,tmp_data2,tmp_data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro02_tasks.read_4w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("CPU read(pro02):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`ifdef FDISP_PRO02
	$fdisplay(f_pro02_msg,"CPU read(pro02):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`endif
`endif
end
endtask

task	pro02_cpu_readbk8w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1,tmp_data2,tmp_data3;
inout	[31:0]	tmp_data4,tmp_data5,tmp_data6,tmp_data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro02_tasks.read_8w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	$display("CPU read(pro02):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("\t\t\t\t\t\tD4 =%h, D5 =%H, D6 =%h, D7 =%H",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`ifdef FDISP_PRO02
	$fdisplay(f_pro02_msg,"CPU read(pro02):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$fdisplay(f_pro02_msg,"\t\t\t\t\t\tD4 =%h, D5 =%H, D6 =%h, D7 =%H",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`endif
`endif
end
endtask

task	pro02_cpu_read1w;
input	[31:0]	addr;
input	[3:0]	byte;
input	[31:0]	exp_data;

reg	[31:0]	cpu_temp;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro02_tasks.read_1w (addr,byte,cpu_temp);
	cpu_temp = cpu_temp & {{8{byte[3]}},{8{byte[2]}},{8{byte[1]}},{8{byte[0]}}};
	if (cpu_temp !== ({{8{byte[3]}},{8{byte[2]}},{8{byte[1]}},{8{byte[0]}}} & exp_data)) begin
	  	$display("Error_T2_rsim:CPU read(pro02)\t%0t ADDR =%h, byte =%h, RDBK--D =%h, EXP--D =%h",
		$realtime,addr,byte,cpu_temp,exp_data);
		`ifdef FDISP_PRO02
		$fdisplay(f_pro02_msg,"Error_T2_rsim:CPU read(pro02)\t%0t ADDR =%h, byte =%h, RDBK--D =%h, EXP--D =%h",
		$realtime,addr,byte,cpu_temp,exp_data);
		`endif
	end else begin
	$display("CPU read(pro02):\t\tADDR =%h, byte =%h, D =%h",addr,byte,cpu_temp);
	`ifdef FDISP_PRO02
	$fdisplay(f_pro02_msg,"CPU read(pro02):\t\tADDR =%h, byte =%h, D =%h",addr,byte,cpu_temp);
	`endif
	end
`endif
end
endtask

task	pro02_cpu_read2w;
input	[31:0]	addr;
input	[31:0]	exp_data0;
input	[31:0]	exp_data1;
reg	[31:0]	tmp_data0,
			tmp_data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro02_tasks.read_2w (addr,tmp_data0,tmp_data1);
	if(tmp_data0!==exp_data0 || tmp_data1!==exp_data1) begin
	  	$display("Error_T2_rsim:CPU read(pro02)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h",
		$realtime,addr,tmp_data0,tmp_data1,exp_data0,exp_data1);
		`ifdef FDISP_PRO02
		$fdisplay(f_pro02_msg,"Error_T2_rsim:CPU read(pro02)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h",
		$realtime,addr,tmp_data0,tmp_data1,exp_data0,exp_data1);
		`endif
	end else begin
	$display("CPU read(pro02):\t\tADDR =%h, D0 =%h, D1 =%h",addr,tmp_data0,tmp_data1);
	`ifdef FDISP_PRO02
	$fdisplay(f_pro02_msg,"CPU read(pro02):\t\tADDR =%h, D0 =%h, D1 =%h",addr,tmp_data0,tmp_data1);
	`endif
	end
`endif
end
endtask

task	pro02_cpu_read4w;
input	[31:0]	addr;
input	[31:0]	exp_data0,
		exp_data1,
		exp_data2,
		exp_data3;
reg	[31:0]	tmp_data0,
		tmp_data1,
		tmp_data2,
		tmp_data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro02_tasks.read_4w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	if(tmp_data0!==exp_data0||tmp_data1!==exp_data1||
	   tmp_data2!==exp_data2||tmp_data3!==exp_data3) begin
	  	$display("Error_T2_rsim:CPU read(pro02)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		`ifdef FDISP_PRO02
		$fdisplay(f_pro02_msg,"Error_T2_rsim:CPU read(pro02)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		`endif
	end else begin
	$display("CPU read(pro02):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`ifdef FDISP_PRO02
	$fdisplay(f_pro02_msg,"CPU read(pro02):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`endif
	end
`endif
end
endtask

task	pro02_cpu_read8w;
input	[31:0]	addr;
input	[31:0]	exp_data0,
		exp_data1,
		exp_data2,
		exp_data3,
		exp_data4,
		exp_data5,
		exp_data6,
		exp_data7;
reg	[31:0]	tmp_data0,
		tmp_data1,
		tmp_data2,
		tmp_data3,
		tmp_data4,
		tmp_data5,
		tmp_data6,
		tmp_data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro02_tasks.read_8w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	if(tmp_data0!==exp_data0||tmp_data1!==exp_data1|| tmp_data2!==exp_data2||tmp_data3!==exp_data3 ||
	   tmp_data4!==exp_data4||tmp_data5!==exp_data5|| tmp_data6!==exp_data6||tmp_data7!==exp_data7)
	   begin
	  	$display("Error_T2_rsim:CPU read(pro02)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		$display("\t\t\t\t\t\tRDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		tmp_data4,tmp_data5,tmp_data6,tmp_data7,exp_data4,exp_data5,exp_data6,exp_data7);
		`ifdef FDISP_PRO02
		$fdisplay(f_pro02_msg,"Error_T2_rsim:CPU read(pro02)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		$fdisplay(f_pro02_msg,"\t\t\t\t\t\tRDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		tmp_data4,tmp_data5,tmp_data6,tmp_data7,exp_data4,exp_data5,exp_data6,exp_data7);
		`endif
	   end
	else begin
	$display("CPU read(pro02):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("\t\t\t\t\t\tD0 =%h, D1 =%h, D2 =%h, D3 =%h",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`ifdef FDISP_PRO02
	$fdisplay(f_pro02_msg,"CPU read(pro02):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$fdisplay(f_pro02_msg,"\t\t\t\t\t\tD0 =%h, D1 =%h, D2 =%h, D3 =%h",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`endif
	end
`endif
end
endtask

//=============== the basic task for pro03 processer ========================
task	pro03_cpu_write1w;
input	[31:0]	addr;
input	[3:0]	byte;
input	[31:0] 	data;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro03_tasks.write_1w (addr,byte,data);
	$display("CPU write(pro03):\t\tADDR =%h, byte =%h, D =%h",addr,byte,data);
	`ifdef FDISP_PRO03
	$fdisplay(f_pro03_msg,"CPU write(pro03):\t\tADDR =%h, byte =%h, D =%h",addr,byte,data);
	`endif
`endif
end
endtask

task	pro03_cpu_write2w;
input	[31:0]	addr;
input	[31:0] 	data0,data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro03_tasks.write_2w (addr,data0,data1);
	$display("CPU write(pro03):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`ifdef FDISP_PRO03
	$fdisplay(f_pro03_msg,"CPU write(pro03):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`endif
`endif
end
endtask

task	pro03_cpu_write4w;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro03_tasks.write_4w (addr,data0,data1,data2,data3);
	$display("CPU write(pro03):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`ifdef FDISP_PRO03
	$fdisplay(f_pro03_msg,"CPU write(pro03):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`endif
`endif
end
endtask

task	pro03_cpu_write8w;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
input	[31:0] 	data4,data5,data6,data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro03_tasks.write_8w (addr,data0,data1,data2,data3,data4,data5,data6,data7);
	$display("CPU write(pro03):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	$display("\t\t\t\t\t\tD4 =%h, D5 =%h, D6 =%h, D7 =%h",data4,data5,data6,data7);
	`ifdef FDISP_PRO03
	$fdisplay(f_pro03_msg,"CPU write(pro03):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	$fdisplay(f_pro03_msg,"\t\t\t\t\t\tD4 =%h, D5 =%h, D6 =%h, D7 =%h",data4,data5,data6,data7);
	`endif
`endif
end
endtask

task	pro03_cpu_readbk1w;
input	[31:0]	addr;
input	[3:0]	byte;
inout	[31:0]	tmp_data;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro03_tasks.read_1w (addr,byte,tmp_data);
	$display("CPU read(pro03):\t\tADDR =%h, byte =%h, D =%h",addr,byte,tmp_data);
	`ifdef FDISP_PRO03
	$fdisplay(f_pro03_msg,"CPU read(pro03):\t\tADDR =%h, byte =%h, D =%h",addr,byte,tmp_data);
	`endif
`else
	tmp_data = 32'h0;
`endif
end
endtask

task	pro03_cpu_readbk2w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro03_tasks.read_2w (addr,tmp_data0,tmp_data1);
	$display("CPU read(pro03):\t\tADDR =%h, D0 =%h, D1 =%H",addr,tmp_data0,tmp_data1 );
	`ifdef FDISP_PRO03
	$fdisplay(f_pro03_msg,"CPU read(pro03):\t\tADDR =%h, D0 =%h, D1 =%H",addr,tmp_data0,tmp_data1 );
	`endif
`endif
end
endtask

task	pro03_cpu_readbk4w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1,tmp_data2,tmp_data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro03_tasks.read_4w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("CPU read(pro03):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`ifdef FDISP_PRO03
	$fdisplay(f_pro03_msg,"CPU read(pro03):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`endif
`endif
end
endtask

task	pro03_cpu_readbk8w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1,tmp_data2,tmp_data3;
inout	[31:0]	tmp_data4,tmp_data5,tmp_data6,tmp_data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro03_tasks.read_8w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	$display("CPU read(pro03):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("\t\t\t\t\t\tD4 =%h, D5 =%H, D6 =%h, D7 =%H",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`ifdef FDISP_PRO03
	$fdisplay(f_pro03_msg,"CPU read(pro03):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$fdisplay(f_pro03_msg,"\t\t\t\t\t\tD4 =%h, D5 =%H, D6 =%h, D7 =%H",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`endif
`endif
end
endtask

task	pro03_cpu_read1w;
input	[31:0]	addr;
input	[3:0]	byte;
input	[31:0]	exp_data;

reg	[31:0]	cpu_temp;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro03_tasks.read_1w (addr,byte,cpu_temp);
	cpu_temp = cpu_temp & {{8{byte[3]}},{8{byte[2]}},{8{byte[1]}},{8{byte[0]}}};
	if (cpu_temp !== ({{8{byte[3]}},{8{byte[2]}},{8{byte[1]}},{8{byte[0]}}} & exp_data)) begin
	  	$display("Error_T2_rsim:CPU read(pro03)\t%0t ADDR =%h, byte =%h, RDBK--D =%h, EXP--D =%h",
		$realtime,addr,byte,cpu_temp,exp_data);
		`ifdef FDISP_PRO03
		$fdisplay(f_pro03_msg,"Error_T2_rsim:CPU read(pro03)\t%0t ADDR =%h, byte =%h, RDBK--D =%h, EXP--D =%h",
		$realtime,addr,byte,cpu_temp,exp_data);
		`endif
	end else begin
	$display("CPU read(pro03):\t\tADDR =%h, byte =%h, D =%h",addr,byte,cpu_temp);
	`ifdef FDISP_PRO03
	$fdisplay(f_pro03_msg,"CPU read(pro03):\t\tADDR =%h, byte =%h, D =%h",addr,byte,cpu_temp);
	`endif
	end
`endif
end
endtask

task	pro03_cpu_read2w;
input	[31:0]	addr;
input	[31:0]	exp_data0;
input	[31:0]	exp_data1;
reg	[31:0]	tmp_data0,
			tmp_data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro03_tasks.read_2w (addr,tmp_data0,tmp_data1);
	if(tmp_data0!==exp_data0 || tmp_data1!==exp_data1) begin
	  	$display("Error_T2_rsim:CPU read(pro03)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h",
		$realtime,addr,tmp_data0,tmp_data1,exp_data0,exp_data1);
		`ifdef FDISP_PRO03
		$fdisplay(f_pro03_msg,"Error_T2_rsim:CPU read(pro03)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h",
		$realtime,addr,tmp_data0,tmp_data1,exp_data0,exp_data1);
		`endif
	end else begin
	$display("CPU read(pro03):\t\tADDR =%h, D0 =%h, D1 =%h",addr,tmp_data0,tmp_data1);
	`ifdef FDISP_PRO03
	$fdisplay(f_pro03_msg,"CPU read(pro03):\t\tADDR =%h, D0 =%h, D1 =%h",addr,tmp_data0,tmp_data1);
	`endif
	end
`endif
end
endtask

task	pro03_cpu_read4w;
input	[31:0]	addr;
input	[31:0]	exp_data0,
		exp_data1,
		exp_data2,
		exp_data3;
reg	[31:0]	tmp_data0,
		tmp_data1,
		tmp_data2,
		tmp_data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro03_tasks.read_4w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	if(tmp_data0!==exp_data0||tmp_data1!==exp_data1||
	   tmp_data2!==exp_data2||tmp_data3!==exp_data3) begin
	  	$display("Error_T2_rsim:CPU read(pro03)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		`ifdef FDISP_PRO03
		$fdisplay(f_pro03_msg,"Error_T2_rsim:CPU read(pro03)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		`endif
	end else begin
	$display("CPU read(pro03):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`ifdef FDISP_PRO03
	$fdisplay(f_pro03_msg,"CPU read(pro03):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`endif
	end
`endif
end
endtask

task	pro03_cpu_read8w;
input	[31:0]	addr;
input	[31:0]	exp_data0,
		exp_data1,
		exp_data2,
		exp_data3,
		exp_data4,
		exp_data5,
		exp_data6,
		exp_data7;
reg	[31:0]	tmp_data0,
		tmp_data1,
		tmp_data2,
		tmp_data3,
		tmp_data4,
		tmp_data5,
		tmp_data6,
		tmp_data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro03_tasks.read_8w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	if(tmp_data0!==exp_data0||tmp_data1!==exp_data1|| tmp_data2!==exp_data2||tmp_data3!==exp_data3 ||
	   tmp_data4!==exp_data4||tmp_data5!==exp_data5|| tmp_data6!==exp_data6||tmp_data7!==exp_data7)
	   begin
	  	$display("Error_T2_rsim:CPU read(pro03)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		$display("\t\t\t\t\t\tRDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		tmp_data4,tmp_data5,tmp_data6,tmp_data7,exp_data4,exp_data5,exp_data6,exp_data7);
		`ifdef FDISP_PRO03
		$fdisplay(f_pro03_msg,"Error_T2_rsim:CPU read(pro03)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		$fdisplay(f_pro03_msg,"\t\t\t\t\t\tRDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		tmp_data4,tmp_data5,tmp_data6,tmp_data7,exp_data4,exp_data5,exp_data6,exp_data7);
		`endif
	   end
	else begin
	$display("CPU read(pro03):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("\t\t\t\t\t\tD0 =%h, D1 =%h, D2 =%h, D3 =%h",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`ifdef FDISP_PRO03
	$fdisplay(f_pro03_msg,"CPU read(pro03):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$fdisplay(f_pro03_msg,"\t\t\t\t\t\tD0 =%h, D1 =%h, D2 =%h, D3 =%h",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`endif
	end
`endif
end
endtask

//=============== the basic task for pro04 processer ========================
task	pro04_cpu_write1w;
input	[31:0]	addr;
input	[3:0]	byte;
input	[31:0] 	data;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro04_tasks.write_1w (addr,byte,data);
	$display("CPU write(pro04):\t\tADDR =%h, byte =%h, D =%h",addr,byte,data);
	`ifdef FDISP_PRO04
	$fdisplay(f_pro04_msg,"CPU write(pro04):\t\tADDR =%h, byte =%h, D =%h",addr,byte,data);
	`endif
`endif
end
endtask

task	pro04_cpu_write2w;
input	[31:0]	addr;
input	[31:0] 	data0,data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro04_tasks.write_2w (addr,data0,data1);
	$display("CPU write(pro04):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`ifdef FDISP_PRO04
	$fdisplay(f_pro04_msg,"CPU write(pro04):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`endif
`endif
end
endtask

task	pro04_cpu_write4w;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro04_tasks.write_4w (addr,data0,data1,data2,data3);
	$display("CPU write(pro04):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`ifdef FDISP_PRO04
	$fdisplay(f_pro04_msg,"CPU write(pro04):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`endif
`endif
end
endtask

task	pro04_cpu_write8w;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
input	[31:0] 	data4,data5,data6,data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro04_tasks.write_8w (addr,data0,data1,data2,data3,data4,data5,data6,data7);
	$display("CPU write(pro04):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	$display("\t\t\t\t\t\tD4 =%h, D5 =%h, D6 =%h, D7 =%h",data4,data5,data6,data7);
	`ifdef FDISP_PRO04
	$fdisplay(f_pro04_msg,"CPU write(pro04):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	$fdisplay(f_pro04_msg,"\t\t\t\t\t\tD4 =%h, D5 =%h, D6 =%h, D7 =%h",data4,data5,data6,data7);
	`endif
`endif
end
endtask

task	pro04_cpu_readbk1w;
input	[31:0]	addr;
input	[3:0]	byte;
inout	[31:0]	tmp_data;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro04_tasks.read_1w (addr,byte,tmp_data);
	$display("CPU read(pro04):\t\tADDR =%h, byte =%h, D =%h",addr,byte,tmp_data);
	`ifdef FDISP_PRO04
	$fdisplay(f_pro04_msg,"CPU read(pro04):\t\tADDR =%h, byte =%h, D =%h",addr,byte,tmp_data);
	`endif
`else
	tmp_data = 32'h0;
`endif
end
endtask

task	pro04_cpu_readbk2w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro04_tasks.read_2w (addr,tmp_data0,tmp_data1);
	$display("CPU read(pro04):\t\tADDR =%h, D0 =%h, D1 =%H",addr,tmp_data0,tmp_data1 );
	`ifdef FDISP_PRO04
	$fdisplay(f_pro04_msg,"CPU read(pro04):\t\tADDR =%h, D0 =%h, D1 =%H",addr,tmp_data0,tmp_data1 );
	`endif
`endif
end
endtask

task	pro04_cpu_readbk4w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1,tmp_data2,tmp_data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro04_tasks.read_4w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("CPU read(pro04):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`ifdef FDISP_PRO04
	$fdisplay(f_pro04_msg,"CPU read(pro04):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`endif
`endif
end
endtask

task	pro04_cpu_readbk8w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1,tmp_data2,tmp_data3;
inout	[31:0]	tmp_data4,tmp_data5,tmp_data6,tmp_data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro04_tasks.read_8w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	$display("CPU read(pro04):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("\t\t\t\t\t\tD4 =%h, D5 =%H, D6 =%h, D7 =%H",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`ifdef FDISP_PRO04
	$fdisplay(f_pro04_msg,"CPU read(pro04):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$fdisplay(f_pro04_msg,"\t\t\t\t\t\tD4 =%h, D5 =%H, D6 =%h, D7 =%H",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`endif
`endif
end
endtask

task	pro04_cpu_read1w;
input	[31:0]	addr;
input	[3:0]	byte;
input	[31:0]	exp_data;

reg	[31:0]	cpu_temp;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro04_tasks.read_1w (addr,byte,cpu_temp);
	cpu_temp = cpu_temp & {{8{byte[3]}},{8{byte[2]}},{8{byte[1]}},{8{byte[0]}}};
	if (cpu_temp !== ({{8{byte[3]}},{8{byte[2]}},{8{byte[1]}},{8{byte[0]}}} & exp_data)) begin
	  	$display("Error_T2_rsim:CPU read(pro04)\t%0t ADDR =%h, byte =%h, RDBK--D =%h, EXP--D =%h",
		$realtime,addr,byte,cpu_temp,exp_data);
		`ifdef FDISP_PRO04
		$fdisplay(f_pro04_msg,"Error_T2_rsim:CPU read(pro04)\t%0t ADDR =%h, byte =%h, RDBK--D =%h, EXP--D =%h",
		$realtime,addr,byte,cpu_temp,exp_data);
		`endif
	end else begin
	$display("CPU read(pro04):\t\tADDR =%h, byte =%h, D =%h",addr,byte,cpu_temp);
	`ifdef FDISP_PRO04
	$fdisplay(f_pro04_msg,"CPU read(pro04):\t\tADDR =%h, byte =%h, D =%h",addr,byte,cpu_temp);
	`endif
	end
`endif
end
endtask

task	pro04_cpu_read2w;
input	[31:0]	addr;
input	[31:0]	exp_data0;
input	[31:0]	exp_data1;
reg	[31:0]	tmp_data0,
			tmp_data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro04_tasks.read_2w (addr,tmp_data0,tmp_data1);
	if(tmp_data0!==exp_data0 || tmp_data1!==exp_data1) begin
	  	$display("Error_T2_rsim:CPU read(pro04)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h",
		$realtime,addr,tmp_data0,tmp_data1,exp_data0,exp_data1);
		`ifdef FDISP_PRO04
		$fdisplay(f_pro04_msg,"Error_T2_rsim:CPU read(pro04)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h",
		$realtime,addr,tmp_data0,tmp_data1,exp_data0,exp_data1);
		`endif
	end else begin
	$display("CPU read(pro04):\t\tADDR =%h, D0 =%h, D1 =%h",addr,tmp_data0,tmp_data1);
	`ifdef FDISP_PRO04
	$fdisplay(f_pro04_msg,"CPU read(pro04):\t\tADDR =%h, D0 =%h, D1 =%h",addr,tmp_data0,tmp_data1);
	`endif
	end
`endif
end
endtask

task	pro04_cpu_read4w;
input	[31:0]	addr;
input	[31:0]	exp_data0,
		exp_data1,
		exp_data2,
		exp_data3;
reg	[31:0]	tmp_data0,
		tmp_data1,
		tmp_data2,
		tmp_data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro04_tasks.read_4w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	if(tmp_data0!==exp_data0||tmp_data1!==exp_data1||
	   tmp_data2!==exp_data2||tmp_data3!==exp_data3) begin
	  	$display("Error_T2_rsim:CPU read(pro04)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		`ifdef FDISP_PRO04
		$fdisplay(f_pro04_msg,"Error_T2_rsim:CPU read(pro04)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		`endif
	end else begin
	$display("CPU read(pro04):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`ifdef FDISP_PRO04
	$fdisplay(f_pro04_msg,"CPU read(pro04):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`endif
	end
`endif
end
endtask

task	pro04_cpu_read8w;
input	[31:0]	addr;
input	[31:0]	exp_data0,
		exp_data1,
		exp_data2,
		exp_data3,
		exp_data4,
		exp_data5,
		exp_data6,
		exp_data7;
reg	[31:0]	tmp_data0,
		tmp_data1,
		tmp_data2,
		tmp_data3,
		tmp_data4,
		tmp_data5,
		tmp_data6,
		tmp_data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro04_tasks.read_8w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	if(tmp_data0!==exp_data0||tmp_data1!==exp_data1|| tmp_data2!==exp_data2||tmp_data3!==exp_data3 ||
	   tmp_data4!==exp_data4||tmp_data5!==exp_data5|| tmp_data6!==exp_data6||tmp_data7!==exp_data7)
	   begin
	  	$display("Error_T2_rsim:CPU read(pro04)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		$display("\t\t\t\t\t\tRDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		tmp_data4,tmp_data5,tmp_data6,tmp_data7,exp_data4,exp_data5,exp_data6,exp_data7);
		`ifdef FDISP_PRO04
		$fdisplay(f_pro04_msg,"Error_T2_rsim:CPU read(pro04)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		$fdisplay(f_pro04_msg,"\t\t\t\t\t\tRDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		tmp_data4,tmp_data5,tmp_data6,tmp_data7,exp_data4,exp_data5,exp_data6,exp_data7);
		`endif
	   end
	else begin
	$display("CPU read(pro04):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("\t\t\t\t\t\tD0 =%h, D1 =%h, D2 =%h, D3 =%h",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`ifdef FDISP_PRO04
	$fdisplay(f_pro04_msg,"CPU read(pro04):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$fdisplay(f_pro04_msg,"\t\t\t\t\t\tD0 =%h, D1 =%h, D2 =%h, D3 =%h",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`endif
	end
`endif
end
endtask

//=============== the basic task for pro05 processer ========================
task	pro05_cpu_write1w;
input	[31:0]	addr;
input	[3:0]	byte;
input	[31:0] 	data;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro05_tasks.write_1w (addr,byte,data);
	$display("CPU write(pro05):\t\tADDR =%h, byte =%h, D =%h",addr,byte,data);
	`ifdef FDISP_PRO05
	$fdisplay(f_pro05_msg,"CPU write(pro05):\t\tADDR =%h, byte =%h, D =%h",addr,byte,data);
	`endif
`endif
end
endtask

task	pro05_cpu_write2w;
input	[31:0]	addr;
input	[31:0] 	data0,data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro05_tasks.write_2w (addr,data0,data1);
	$display("CPU write(pro05):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`ifdef FDISP_PRO05
	$fdisplay(f_pro05_msg,"CPU write(pro05):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`endif
`endif
end
endtask

task	pro05_cpu_write4w;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro05_tasks.write_4w (addr,data0,data1,data2,data3);
	$display("CPU write(pro05):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`ifdef FDISP_PRO05
	$fdisplay(f_pro05_msg,"CPU write(pro05):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`endif
`endif
end
endtask

task	pro05_cpu_write8w;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
input	[31:0] 	data4,data5,data6,data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro05_tasks.write_8w (addr,data0,data1,data2,data3,data4,data5,data6,data7);
	$display("CPU write(pro05):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	$display("\t\t\t\t\t\tD4 =%h, D5 =%h, D6 =%h, D7 =%h",data4,data5,data6,data7);
	`ifdef FDISP_PRO05
	$fdisplay(f_pro05_msg,"CPU write(pro05):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	$fdisplay(f_pro05_msg,"\t\t\t\t\t\tD4 =%h, D5 =%h, D6 =%h, D7 =%h",data4,data5,data6,data7);
	`endif
`endif
end
endtask

task	pro05_cpu_readbk1w;
input	[31:0]	addr;
input	[3:0]	byte;
inout	[31:0]	tmp_data;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro05_tasks.read_1w (addr,byte,tmp_data);
	$display("CPU read(pro05):\t\tADDR =%h, byte =%h, D =%h",addr,byte,tmp_data);
	`ifdef FDISP_PRO05
	$fdisplay(f_pro05_msg,"CPU read(pro05):\t\tADDR =%h, byte =%h, D =%h",addr,byte,tmp_data);
	`endif
`else
	tmp_data = 32'h0;
`endif
end
endtask

task	pro05_cpu_readbk2w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro05_tasks.read_2w (addr,tmp_data0,tmp_data1);
	$display("CPU read(pro05):\t\tADDR =%h, D0 =%h, D1 =%H",addr,tmp_data0,tmp_data1 );
	`ifdef FDISP_PRO05
	$fdisplay(f_pro05_msg,"CPU read(pro05):\t\tADDR =%h, D0 =%h, D1 =%H",addr,tmp_data0,tmp_data1 );
	`endif
`endif
end
endtask

task	pro05_cpu_readbk4w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1,tmp_data2,tmp_data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro05_tasks.read_4w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("CPU read(pro05):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`ifdef FDISP_PRO05
	$fdisplay(f_pro05_msg,"CPU read(pro05):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`endif
`endif
end
endtask

task	pro05_cpu_readbk8w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1,tmp_data2,tmp_data3;
inout	[31:0]	tmp_data4,tmp_data5,tmp_data6,tmp_data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro05_tasks.read_8w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	$display("CPU read(pro05):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("\t\t\t\t\t\tD4 =%h, D5 =%H, D6 =%h, D7 =%H",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`ifdef FDISP_PRO05
	$fdisplay(f_pro05_msg,"CPU read(pro05):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$fdisplay(f_pro05_msg,"\t\t\t\t\t\tD4 =%h, D5 =%H, D6 =%h, D7 =%H",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`endif
`endif
end
endtask

task	pro05_cpu_read1w;
input	[31:0]	addr;
input	[3:0]	byte;
input	[31:0]	exp_data;

reg	[31:0]	cpu_temp;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro05_tasks.read_1w (addr,byte,cpu_temp);
	cpu_temp = cpu_temp & {{8{byte[3]}},{8{byte[2]}},{8{byte[1]}},{8{byte[0]}}};
	if (cpu_temp !== ({{8{byte[3]}},{8{byte[2]}},{8{byte[1]}},{8{byte[0]}}} & exp_data)) begin
	  	$display("Error_T2_rsim:CPU read(pro05)\t%0t ADDR =%h, byte =%h, RDBK--D =%h, EXP--D =%h",
		$realtime,addr,byte,cpu_temp,exp_data);
		`ifdef FDISP_PRO05
		$fdisplay(f_pro05_msg,"Error_T2_rsim:CPU read(pro05)\t%0t ADDR =%h, byte =%h, RDBK--D =%h, EXP--D =%h",
		$realtime,addr,byte,cpu_temp,exp_data);
		`endif
	end else begin
	$display("CPU read(pro05):\t\tADDR =%h, byte =%h, D =%h",addr,byte,cpu_temp);
	`ifdef FDISP_PRO05
	$fdisplay(f_pro05_msg,"CPU read(pro05):\t\tADDR =%h, byte =%h, D =%h",addr,byte,cpu_temp);
	`endif
	end
`endif
end
endtask

task	pro05_cpu_read2w;
input	[31:0]	addr;
input	[31:0]	exp_data0;
input	[31:0]	exp_data1;
reg	[31:0]	tmp_data0,
			tmp_data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro05_tasks.read_2w (addr,tmp_data0,tmp_data1);
	if(tmp_data0!==exp_data0 || tmp_data1!==exp_data1) begin
	  	$display("Error_T2_rsim:CPU read(pro05)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h",
		$realtime,addr,tmp_data0,tmp_data1,exp_data0,exp_data1);
		`ifdef FDISP_PRO05
		$fdisplay(f_pro05_msg,"Error_T2_rsim:CPU read(pro05)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h",
		$realtime,addr,tmp_data0,tmp_data1,exp_data0,exp_data1);
		`endif
	end else begin
	$display("CPU read(pro05):\t\tADDR =%h, D0 =%h, D1 =%h",addr,tmp_data0,tmp_data1);
	`ifdef FDISP_PRO05
	$fdisplay(f_pro05_msg,"CPU read(pro05):\t\tADDR =%h, D0 =%h, D1 =%h",addr,tmp_data0,tmp_data1);
	`endif
	end
`endif
end
endtask

task	pro05_cpu_read4w;
input	[31:0]	addr;
input	[31:0]	exp_data0,
		exp_data1,
		exp_data2,
		exp_data3;
reg	[31:0]	tmp_data0,
		tmp_data1,
		tmp_data2,
		tmp_data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro05_tasks.read_4w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	if(tmp_data0!==exp_data0||tmp_data1!==exp_data1||
	   tmp_data2!==exp_data2||tmp_data3!==exp_data3) begin
	  	$display("Error_T2_rsim:CPU read(pro05)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		`ifdef FDISP_PRO05
		$fdisplay(f_pro05_msg,"Error_T2_rsim:CPU read(pro05)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		`endif
	end else begin
	$display("CPU read(pro05):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`ifdef FDISP_PRO05
	$fdisplay(f_pro05_msg,"CPU read(pro05):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`endif
	end
`endif
end
endtask

task	pro05_cpu_read8w;
input	[31:0]	addr;
input	[31:0]	exp_data0,
		exp_data1,
		exp_data2,
		exp_data3,
		exp_data4,
		exp_data5,
		exp_data6,
		exp_data7;
reg	[31:0]	tmp_data0,
		tmp_data1,
		tmp_data2,
		tmp_data3,
		tmp_data4,
		tmp_data5,
		tmp_data6,
		tmp_data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro05_tasks.read_8w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	if(tmp_data0!==exp_data0||tmp_data1!==exp_data1|| tmp_data2!==exp_data2||tmp_data3!==exp_data3 ||
	   tmp_data4!==exp_data4||tmp_data5!==exp_data5|| tmp_data6!==exp_data6||tmp_data7!==exp_data7)
	   begin
	  	$display("Error_T2_rsim:CPU read(pro05)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		$display("\t\t\t\t\t\tRDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		tmp_data4,tmp_data5,tmp_data6,tmp_data7,exp_data4,exp_data5,exp_data6,exp_data7);
		`ifdef FDISP_PRO05
		$fdisplay(f_pro05_msg,"Error_T2_rsim:CPU read(pro05)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		$fdisplay(f_pro05_msg,"\t\t\t\t\t\tRDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		tmp_data4,tmp_data5,tmp_data6,tmp_data7,exp_data4,exp_data5,exp_data6,exp_data7);
		`endif
	   end
	else begin
	$display("CPU read(pro05):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("\t\t\t\t\t\tD0 =%h, D1 =%h, D2 =%h, D3 =%h",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`ifdef FDISP_PRO05
	$fdisplay(f_pro05_msg,"CPU read(pro05):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$fdisplay(f_pro05_msg,"\t\t\t\t\t\tD0 =%h, D1 =%h, D2 =%h, D3 =%h",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`endif
	end
`endif
end
endtask

//=============== the basic task for pro06 processer ========================
task	pro06_cpu_write1w;
input	[31:0]	addr;
input	[3:0]	byte;
input	[31:0] 	data;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro06_tasks.write_1w (addr,byte,data);
	$display("CPU write(pro06):\t\tADDR =%h, byte =%h, D =%h",addr,byte,data);
	`ifdef FDISP_PRO06
	$fdisplay(f_pro06_msg,"CPU write(pro06):\t\tADDR =%h, byte =%h, D =%h",addr,byte,data);
	`endif
`endif
end
endtask

task	pro06_cpu_write2w;
input	[31:0]	addr;
input	[31:0] 	data0,data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro06_tasks.write_2w (addr,data0,data1);
	$display("CPU write(pro06):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`ifdef FDISP_PRO06
	$fdisplay(f_pro06_msg,"CPU write(pro06):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`endif
`endif
end
endtask

task	pro06_cpu_write4w;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro06_tasks.write_4w (addr,data0,data1,data2,data3);
	$display("CPU write(pro06):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`ifdef FDISP_PRO06
	$fdisplay(f_pro06_msg,"CPU write(pro06):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`endif
`endif
end
endtask

task	pro06_cpu_write8w;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
input	[31:0] 	data4,data5,data6,data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro06_tasks.write_8w (addr,data0,data1,data2,data3,data4,data5,data6,data7);
	$display("CPU write(pro06):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	$display("\t\t\t\t\t\tD4 =%h, D5 =%h, D6 =%h, D7 =%h",data4,data5,data6,data7);
	`ifdef FDISP_PRO06
	$fdisplay(f_pro06_msg,"CPU write(pro06):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	$fdisplay(f_pro06_msg,"\t\t\t\t\t\tD4 =%h, D5 =%h, D6 =%h, D7 =%h",data4,data5,data6,data7);
	`endif
`endif
end
endtask

task	pro06_cpu_readbk1w;
input	[31:0]	addr;
input	[3:0]	byte;
inout	[31:0]	tmp_data;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro06_tasks.read_1w (addr,byte,tmp_data);
	$display("CPU read(pro06):\t\tADDR =%h, byte =%h, D =%h",addr,byte,tmp_data);
	`ifdef FDISP_PRO06
	$fdisplay(f_pro06_msg,"CPU read(pro06):\t\tADDR =%h, byte =%h, D =%h",addr,byte,tmp_data);
	`endif
`else
	tmp_data = 32'h0;
`endif
end
endtask

task	pro06_cpu_readbk2w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro06_tasks.read_2w (addr,tmp_data0,tmp_data1);
	$display("CPU read(pro06):\t\tADDR =%h, D0 =%h, D1 =%H",addr,tmp_data0,tmp_data1 );
	`ifdef FDISP_PRO06
	$fdisplay(f_pro06_msg,"CPU read(pro06):\t\tADDR =%h, D0 =%h, D1 =%H",addr,tmp_data0,tmp_data1 );
	`endif
`endif
end
endtask

task	pro06_cpu_readbk4w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1,tmp_data2,tmp_data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro06_tasks.read_4w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("CPU read(pro06):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`ifdef FDISP_PRO06
	$fdisplay(f_pro06_msg,"CPU read(pro06):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`endif
`endif
end
endtask

task	pro06_cpu_readbk8w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1,tmp_data2,tmp_data3;
inout	[31:0]	tmp_data4,tmp_data5,tmp_data6,tmp_data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro06_tasks.read_8w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	$display("CPU read(pro06):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("\t\t\t\t\t\tD4 =%h, D5 =%H, D6 =%h, D7 =%H",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`ifdef FDISP_PRO06
	$fdisplay(f_pro06_msg,"CPU read(pro06):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$fdisplay(f_pro06_msg,"\t\t\t\t\t\tD4 =%h, D5 =%H, D6 =%h, D7 =%H",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`endif
`endif
end
endtask

task	pro06_cpu_read1w;
input	[31:0]	addr;
input	[3:0]	byte;
input	[31:0]	exp_data;

reg	[31:0]	cpu_temp;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro06_tasks.read_1w (addr,byte,cpu_temp);
	cpu_temp = cpu_temp & {{8{byte[3]}},{8{byte[2]}},{8{byte[1]}},{8{byte[0]}}};
	if (cpu_temp !== ({{8{byte[3]}},{8{byte[2]}},{8{byte[1]}},{8{byte[0]}}} & exp_data)) begin
	  	$display("Error_T2_rsim:CPU read(pro06)\t%0t ADDR =%h, byte =%h, RDBK--D =%h, EXP--D =%h",
		$realtime,addr,byte,cpu_temp,exp_data);
		`ifdef FDISP_PRO06
		$fdisplay(f_pro06_msg,"Error_T2_rsim:CPU read(pro06)\t%0t ADDR =%h, byte =%h, RDBK--D =%h, EXP--D =%h",
		$realtime,addr,byte,cpu_temp,exp_data);
		`endif
	end else begin
	$display("CPU read(pro06):\t\tADDR =%h, byte =%h, D =%h",addr,byte,cpu_temp);
	`ifdef FDISP_PRO06
	$fdisplay(f_pro06_msg,"CPU read(pro06):\t\tADDR =%h, byte =%h, D =%h",addr,byte,cpu_temp);
	`endif
	end
`endif
end
endtask

task	pro06_cpu_read2w;
input	[31:0]	addr;
input	[31:0]	exp_data0;
input	[31:0]	exp_data1;
reg	[31:0]	tmp_data0,
			tmp_data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro06_tasks.read_2w (addr,tmp_data0,tmp_data1);
	if(tmp_data0!==exp_data0 || tmp_data1!==exp_data1) begin
	  	$display("Error_T2_rsim:CPU read(pro06)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h",
		$realtime,addr,tmp_data0,tmp_data1,exp_data0,exp_data1);
		`ifdef FDISP_PRO06
		$fdisplay(f_pro06_msg,"Error_T2_rsim:CPU read(pro06)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h",
		$realtime,addr,tmp_data0,tmp_data1,exp_data0,exp_data1);
		`endif
	end else begin
	$display("CPU read(pro06):\t\tADDR =%h, D0 =%h, D1 =%h",addr,tmp_data0,tmp_data1);
	`ifdef FDISP_PRO06
	$fdisplay(f_pro06_msg,"CPU read(pro06):\t\tADDR =%h, D0 =%h, D1 =%h",addr,tmp_data0,tmp_data1);
	`endif
	end
`endif
end
endtask

task	pro06_cpu_read4w;
input	[31:0]	addr;
input	[31:0]	exp_data0,
		exp_data1,
		exp_data2,
		exp_data3;
reg	[31:0]	tmp_data0,
		tmp_data1,
		tmp_data2,
		tmp_data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro06_tasks.read_4w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	if(tmp_data0!==exp_data0||tmp_data1!==exp_data1||
	   tmp_data2!==exp_data2||tmp_data3!==exp_data3) begin
	  	$display("Error_T2_rsim:CPU read(pro06)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		`ifdef FDISP_PRO06
		$fdisplay(f_pro06_msg,"Error_T2_rsim:CPU read(pro06)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		`endif
	end else begin
	$display("CPU read(pro06):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`ifdef FDISP_PRO06
	$fdisplay(f_pro06_msg,"CPU read(pro06):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`endif
	end
`endif
end
endtask

task	pro06_cpu_read8w;
input	[31:0]	addr;
input	[31:0]	exp_data0,
		exp_data1,
		exp_data2,
		exp_data3,
		exp_data4,
		exp_data5,
		exp_data6,
		exp_data7;
reg	[31:0]	tmp_data0,
		tmp_data1,
		tmp_data2,
		tmp_data3,
		tmp_data4,
		tmp_data5,
		tmp_data6,
		tmp_data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro06_tasks.read_8w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	if(tmp_data0!==exp_data0||tmp_data1!==exp_data1|| tmp_data2!==exp_data2||tmp_data3!==exp_data3 ||
	   tmp_data4!==exp_data4||tmp_data5!==exp_data5|| tmp_data6!==exp_data6||tmp_data7!==exp_data7)
	   begin
	  	$display("Error_T2_rsim:CPU read(pro06)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		$display("\t\t\t\t\t\tRDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		tmp_data4,tmp_data5,tmp_data6,tmp_data7,exp_data4,exp_data5,exp_data6,exp_data7);
		`ifdef FDISP_PRO06
		$fdisplay(f_pro06_msg,"Error_T2_rsim:CPU read(pro06)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		$fdisplay(f_pro06_msg,"\t\t\t\t\t\tRDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		tmp_data4,tmp_data5,tmp_data6,tmp_data7,exp_data4,exp_data5,exp_data6,exp_data7);
		`endif
	   end
	else begin
	$display("CPU read(pro06):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("\t\t\t\t\t\tD0 =%h, D1 =%h, D2 =%h, D3 =%h",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`ifdef FDISP_PRO06
	$fdisplay(f_pro06_msg,"CPU read(pro06):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$fdisplay(f_pro06_msg,"\t\t\t\t\t\tD0 =%h, D1 =%h, D2 =%h, D3 =%h",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`endif
	end
`endif
end
endtask

//=============== the basic task for pro07 processer ========================
task	pro07_cpu_write1w;
input	[31:0]	addr;
input	[3:0]	byte;
input	[31:0] 	data;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro07_tasks.write_1w (addr,byte,data);
	$display("CPU write(pro07):\t\tADDR =%h, byte =%h, D =%h",addr,byte,data);
	`ifdef FDISP_PRO07
	$fdisplay(f_pro07_msg,"CPU write(pro07):\t\tADDR =%h, byte =%h, D =%h",addr,byte,data);
	`endif
`endif
end
endtask

task	pro07_cpu_write2w;
input	[31:0]	addr;
input	[31:0] 	data0,data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro07_tasks.write_2w (addr,data0,data1);
	$display("CPU write(pro07):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`ifdef FDISP_PRO07
	$fdisplay(f_pro07_msg,"CPU write(pro07):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`endif
`endif
end
endtask

task	pro07_cpu_write4w;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro07_tasks.write_4w (addr,data0,data1,data2,data3);
	$display("CPU write(pro07):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`ifdef FDISP_PRO07
	$fdisplay(f_pro07_msg,"CPU write(pro07):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`endif
`endif
end
endtask

task	pro07_cpu_write8w;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
input	[31:0] 	data4,data5,data6,data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro07_tasks.write_8w (addr,data0,data1,data2,data3,data4,data5,data6,data7);
	$display("CPU write(pro07):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	$display("\t\t\t\t\t\tD4 =%h, D5 =%h, D6 =%h, D7 =%h",data4,data5,data6,data7);
	`ifdef FDISP_PRO07
	$fdisplay(f_pro07_msg,"CPU write(pro07):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	$fdisplay(f_pro07_msg,"\t\t\t\t\t\tD4 =%h, D5 =%h, D6 =%h, D7 =%h",data4,data5,data6,data7);
	`endif
`endif
end
endtask

task	pro07_cpu_readbk1w;
input	[31:0]	addr;
input	[3:0]	byte;
inout	[31:0]	tmp_data;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro07_tasks.read_1w (addr,byte,tmp_data);
	$display("CPU read(pro07):\t\tADDR =%h, byte =%h, D =%h",addr,byte,tmp_data);
	`ifdef FDISP_PRO07
	$fdisplay(f_pro07_msg,"CPU read(pro07):\t\tADDR =%h, byte =%h, D =%h",addr,byte,tmp_data);
	`endif
`else
	tmp_data = 32'h0;
`endif
end
endtask

task	pro07_cpu_readbk2w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro07_tasks.read_2w (addr,tmp_data0,tmp_data1);
	$display("CPU read(pro07):\t\tADDR =%h, D0 =%h, D1 =%H",addr,tmp_data0,tmp_data1 );
	`ifdef FDISP_PRO07
	$fdisplay(f_pro07_msg,"CPU read(pro07):\t\tADDR =%h, D0 =%h, D1 =%H",addr,tmp_data0,tmp_data1 );
	`endif
`endif
end
endtask

task	pro07_cpu_readbk4w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1,tmp_data2,tmp_data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro07_tasks.read_4w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("CPU read(pro07):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`ifdef FDISP_PRO07
	$fdisplay(f_pro07_msg,"CPU read(pro07):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`endif
`endif
end
endtask

task	pro07_cpu_readbk8w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1,tmp_data2,tmp_data3;
inout	[31:0]	tmp_data4,tmp_data5,tmp_data6,tmp_data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro07_tasks.read_8w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	$display("CPU read(pro07):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("\t\t\t\t\t\tD4 =%h, D5 =%H, D6 =%h, D7 =%H",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`ifdef FDISP_PRO07
	$fdisplay(f_pro07_msg,"CPU read(pro07):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$fdisplay(f_pro07_msg,"\t\t\t\t\t\tD4 =%h, D5 =%H, D6 =%h, D7 =%H",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`endif
`endif
end
endtask

task	pro07_cpu_read1w;
input	[31:0]	addr;
input	[3:0]	byte;
input	[31:0]	exp_data;

reg	[31:0]	cpu_temp;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro07_tasks.read_1w (addr,byte,cpu_temp);
	cpu_temp = cpu_temp & {{8{byte[3]}},{8{byte[2]}},{8{byte[1]}},{8{byte[0]}}};
	if (cpu_temp !== ({{8{byte[3]}},{8{byte[2]}},{8{byte[1]}},{8{byte[0]}}} & exp_data)) begin
	  	$display("Error_T2_rsim:CPU read(pro07)\t%0t ADDR =%h, byte =%h, RDBK--D =%h, EXP--D =%h",
		$realtime,addr,byte,cpu_temp,exp_data);
		`ifdef FDISP_PRO07
		$fdisplay(f_pro07_msg,"Error_T2_rsim:CPU read(pro07)\t%0t ADDR =%h, byte =%h, RDBK--D =%h, EXP--D =%h",
		$realtime,addr,byte,cpu_temp,exp_data);
		`endif
	end else begin
	$display("CPU read(pro07):\t\tADDR =%h, byte =%h, D =%h",addr,byte,cpu_temp);
	`ifdef FDISP_PRO07
	$fdisplay(f_pro07_msg,"CPU read(pro07):\t\tADDR =%h, byte =%h, D =%h",addr,byte,cpu_temp);
	`endif
	end
`endif
end
endtask

task	pro07_cpu_read2w;
input	[31:0]	addr;
input	[31:0]	exp_data0;
input	[31:0]	exp_data1;
reg	[31:0]	tmp_data0,
			tmp_data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro07_tasks.read_2w (addr,tmp_data0,tmp_data1);
	if(tmp_data0!==exp_data0 || tmp_data1!==exp_data1) begin
	  	$display("Error_T2_rsim:CPU read(pro07)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h",
		$realtime,addr,tmp_data0,tmp_data1,exp_data0,exp_data1);
		`ifdef FDISP_PRO07
		$fdisplay(f_pro07_msg,"Error_T2_rsim:CPU read(pro07)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h",
		$realtime,addr,tmp_data0,tmp_data1,exp_data0,exp_data1);
		`endif
	end else begin
	$display("CPU read(pro07):\t\tADDR =%h, D0 =%h, D1 =%h",addr,tmp_data0,tmp_data1);
	`ifdef FDISP_PRO07
	$fdisplay(f_pro07_msg,"CPU read(pro07):\t\tADDR =%h, D0 =%h, D1 =%h",addr,tmp_data0,tmp_data1);
	`endif
	end
`endif
end
endtask

task	pro07_cpu_read4w;
input	[31:0]	addr;
input	[31:0]	exp_data0,
		exp_data1,
		exp_data2,
		exp_data3;
reg	[31:0]	tmp_data0,
		tmp_data1,
		tmp_data2,
		tmp_data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro07_tasks.read_4w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	if(tmp_data0!==exp_data0||tmp_data1!==exp_data1||
	   tmp_data2!==exp_data2||tmp_data3!==exp_data3) begin
	  	$display("Error_T2_rsim:CPU read(pro07)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		`ifdef FDISP_PRO07
		$fdisplay(f_pro07_msg,"Error_T2_rsim:CPU read(pro07)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		`endif
	end else begin
	$display("CPU read(pro07):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`ifdef FDISP_PRO07
	$fdisplay(f_pro07_msg,"CPU read(pro07):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`endif
	end
`endif
end
endtask

task	pro07_cpu_read8w;
input	[31:0]	addr;
input	[31:0]	exp_data0,
		exp_data1,
		exp_data2,
		exp_data3,
		exp_data4,
		exp_data5,
		exp_data6,
		exp_data7;
reg	[31:0]	tmp_data0,
		tmp_data1,
		tmp_data2,
		tmp_data3,
		tmp_data4,
		tmp_data5,
		tmp_data6,
		tmp_data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro07_tasks.read_8w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	if(tmp_data0!==exp_data0||tmp_data1!==exp_data1|| tmp_data2!==exp_data2||tmp_data3!==exp_data3 ||
	   tmp_data4!==exp_data4||tmp_data5!==exp_data5|| tmp_data6!==exp_data6||tmp_data7!==exp_data7)
	   begin
	  	$display("Error_T2_rsim:CPU read(pro07)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		$display("\t\t\t\t\t\tRDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		tmp_data4,tmp_data5,tmp_data6,tmp_data7,exp_data4,exp_data5,exp_data6,exp_data7);
		`ifdef FDISP_PRO07
		$fdisplay(f_pro07_msg,"Error_T2_rsim:CPU read(pro07)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		$fdisplay(f_pro07_msg,"\t\t\t\t\t\tRDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		tmp_data4,tmp_data5,tmp_data6,tmp_data7,exp_data4,exp_data5,exp_data6,exp_data7);
		`endif
	   end
	else begin
	$display("CPU read(pro07):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("\t\t\t\t\t\tD0 =%h, D1 =%h, D2 =%h, D3 =%h",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`ifdef FDISP_PRO07
	$fdisplay(f_pro07_msg,"CPU read(pro07):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$fdisplay(f_pro07_msg,"\t\t\t\t\t\tD0 =%h, D1 =%h, D2 =%h, D3 =%h",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`endif
	end
`endif
end
endtask

//=============== the basic task for pro08 processer ========================
task	pro08_cpu_write1w;
input	[31:0]	addr;
input	[3:0]	byte;
input	[31:0] 	data;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro08_tasks.write_1w (addr,byte,data);
	$display("CPU write(pro08):\t\tADDR =%h, byte =%h, D =%h",addr,byte,data);
	`ifdef FDISP_PRO08
	$fdisplay(f_pro08_msg,"CPU write(pro08):\t\tADDR =%h, byte =%h, D =%h",addr,byte,data);
	`endif
`endif
end
endtask

task	pro08_cpu_write2w;
input	[31:0]	addr;
input	[31:0] 	data0,data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro08_tasks.write_2w (addr,data0,data1);
	$display("CPU write(pro08):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`ifdef FDISP_PRO08
	$fdisplay(f_pro08_msg,"CPU write(pro08):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`endif
`endif
end
endtask

task	pro08_cpu_write4w;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro08_tasks.write_4w (addr,data0,data1,data2,data3);
	$display("CPU write(pro08):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`ifdef FDISP_PRO08
	$fdisplay(f_pro08_msg,"CPU write(pro08):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`endif
`endif
end
endtask

task	pro08_cpu_write8w;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
input	[31:0] 	data4,data5,data6,data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro08_tasks.write_8w (addr,data0,data1,data2,data3,data4,data5,data6,data7);
	$display("CPU write(pro08):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	$display("\t\t\t\t\t\tD4 =%h, D5 =%h, D6 =%h, D7 =%h",data4,data5,data6,data7);
	`ifdef FDISP_PRO08
	$fdisplay(f_pro08_msg,"CPU write(pro08):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	$fdisplay(f_pro08_msg,"\t\t\t\t\t\tD4 =%h, D5 =%h, D6 =%h, D7 =%h",data4,data5,data6,data7);
	`endif
`endif
end
endtask

task	pro08_cpu_readbk1w;
input	[31:0]	addr;
input	[3:0]	byte;
inout	[31:0]	tmp_data;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro08_tasks.read_1w (addr,byte,tmp_data);
	`ifdef	MASK_DISP_PRO08
	`else
	$display("CPU read(pro08):\t\tADDR =%h, byte =%h, D =%h",addr,byte,tmp_data);
	`endif
	`ifdef FDISP_PRO08
	$fdisplay(f_pro08_msg,"CPU read(pro08):\t\tADDR =%h, byte =%h, D =%h",addr,byte,tmp_data);
	`endif
`else
	tmp_data = 32'h0;
`endif
end
endtask

task	pro08_cpu_readbk2w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro08_tasks.read_2w (addr,tmp_data0,tmp_data1);
	$display("CPU read(pro08):\t\tADDR =%h, D0 =%h, D1 =%H",addr,tmp_data0,tmp_data1 );
	`ifdef FDISP_PRO08
	$fdisplay(f_pro08_msg,"CPU read(pro08):\t\tADDR =%h, D0 =%h, D1 =%H",addr,tmp_data0,tmp_data1 );
	`endif
`endif
end
endtask

task	pro08_cpu_readbk4w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1,tmp_data2,tmp_data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro08_tasks.read_4w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("CPU read(pro08):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`ifdef FDISP_PRO08
	$fdisplay(f_pro08_msg,"CPU read(pro08):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`endif
`endif
end
endtask

task	pro08_cpu_readbk8w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1,tmp_data2,tmp_data3;
inout	[31:0]	tmp_data4,tmp_data5,tmp_data6,tmp_data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro08_tasks.read_8w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	$display("CPU read(pro08):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("\t\t\t\t\t\tD4 =%h, D5 =%H, D6 =%h, D7 =%H",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`ifdef FDISP_PRO08
	$fdisplay(f_pro08_msg,"CPU read(pro08):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$fdisplay(f_pro08_msg,"\t\t\t\t\t\tD4 =%h, D5 =%H, D6 =%h, D7 =%H",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`endif
`endif
end
endtask

task	pro08_cpu_read1w;
input	[31:0]	addr;
input	[3:0]	byte;
input	[31:0]	exp_data;

reg	[31:0]	cpu_temp;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro08_tasks.read_1w (addr,byte,cpu_temp);
	cpu_temp = cpu_temp & {{8{byte[3]}},{8{byte[2]}},{8{byte[1]}},{8{byte[0]}}};
	if (cpu_temp !== ({{8{byte[3]}},{8{byte[2]}},{8{byte[1]}},{8{byte[0]}}} & exp_data)) begin

	  	$display("Error_T2_rsim:CPU read(pro08)\t%0t ADDR =%h, byte =%h, RDBK--D =%h, EXP--D =%h",
		$realtime,addr,byte,cpu_temp,exp_data);
		`ifdef FDISP_PRO08
		$fdisplay(f_pro08_msg,"Error_T2_rsim:CPU read(pro08)\t%0t ADDR =%h, byte =%h, RDBK--D =%h, EXP--D =%h",
		$realtime,addr,byte,cpu_temp,exp_data);
		`endif
	end else begin
	$display("CPU read(pro08):\t\tADDR =%h, byte =%h, D =%h",addr,byte,cpu_temp);
	`ifdef FDISP_PRO08
	$fdisplay(f_pro08_msg,"CPU read(pro08):\t\tADDR =%h, byte =%h, D =%h",addr,byte,cpu_temp);
	`endif
	end
`endif
end
endtask

task	pro08_cpu_read2w;
input	[31:0]	addr;
input	[31:0]	exp_data0;
input	[31:0]	exp_data1;
reg	[31:0]	tmp_data0,
			tmp_data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro08_tasks.read_2w (addr,tmp_data0,tmp_data1);
	if(tmp_data0!==exp_data0 || tmp_data1!==exp_data1) begin
	  	$display("Error_T2_rsim:CPU read(pro08)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h",
		$realtime,addr,tmp_data0,tmp_data1,exp_data0,exp_data1);
		`ifdef FDISP_PRO08
		$fdisplay(f_pro08_msg,"Error_T2_rsim:CPU read(pro08)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h",
		$realtime,addr,tmp_data0,tmp_data1,exp_data0,exp_data1);
		`endif
	end else begin
	$display("CPU read(pro08):\t\tADDR =%h, D0 =%h, D1 =%h",addr,tmp_data0,tmp_data1);
	`ifdef FDISP_PRO08
	$fdisplay(f_pro08_msg,"CPU read(pro08):\t\tADDR =%h, D0 =%h, D1 =%h",addr,tmp_data0,tmp_data1);
	`endif
	end
`endif
end
endtask

task	pro08_cpu_read4w;
input	[31:0]	addr;
input	[31:0]	exp_data0,
		exp_data1,
		exp_data2,
		exp_data3;
reg	[31:0]	tmp_data0,
		tmp_data1,
		tmp_data2,
		tmp_data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro08_tasks.read_4w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	if(tmp_data0!==exp_data0||tmp_data1!==exp_data1||
	   tmp_data2!==exp_data2||tmp_data3!==exp_data3) begin
	  	$display("Error_T2_rsim:CPU read(pro08)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		`ifdef FDISP_PRO08
		$fdisplay(f_pro08_msg,"Error_T2_rsim:CPU read(pro08)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		`endif
	end else begin
	$display("CPU read(pro08):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`ifdef FDISP_PRO08
	$fdisplay(f_pro08_msg,"CPU read(pro08):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`endif
	end
`endif
end
endtask

task	pro08_cpu_read8w;
input	[31:0]	addr;
input	[31:0]	exp_data0,
		exp_data1,
		exp_data2,
		exp_data3,
		exp_data4,
		exp_data5,
		exp_data6,
		exp_data7;
reg	[31:0]	tmp_data0,
		tmp_data1,
		tmp_data2,
		tmp_data3,
		tmp_data4,
		tmp_data5,
		tmp_data6,
		tmp_data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro08_tasks.read_8w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	if(tmp_data0!==exp_data0||tmp_data1!==exp_data1|| tmp_data2!==exp_data2||tmp_data3!==exp_data3 ||
	   tmp_data4!==exp_data4||tmp_data5!==exp_data5|| tmp_data6!==exp_data6||tmp_data7!==exp_data7)
	   begin
	  	$display("Error_T2_rsim:CPU read(pro08)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		$display("\t\t\t\t\t\tRDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		tmp_data4,tmp_data5,tmp_data6,tmp_data7,exp_data4,exp_data5,exp_data6,exp_data7);
		`ifdef FDISP_PRO08
		$fdisplay(f_pro08_msg,"Error_T2_rsim:CPU read(pro08)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		$fdisplay(f_pro08_msg,"\t\t\t\t\t\tRDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		tmp_data4,tmp_data5,tmp_data6,tmp_data7,exp_data4,exp_data5,exp_data6,exp_data7);
		`endif
	   end
	else begin
	$display("CPU read(pro08):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("\t\t\t\t\t\tD0 =%h, D1 =%h, D2 =%h, D3 =%h",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`ifdef FDISP_PRO08
	$fdisplay(f_pro08_msg,"CPU read(pro08):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$fdisplay(f_pro08_msg,"\t\t\t\t\t\tD0 =%h, D1 =%h, D2 =%h, D3 =%h",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`endif
	end
`endif
end
endtask

//=============== the basic task for pro09 processer ========================
task	pro09_cpu_write1w;
input	[31:0]	addr;
input	[3:0]	byte;
input	[31:0] 	data;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro09_tasks.write_1w (addr,byte,data);
	$display("CPU write(pro09):\t\tADDR =%h, byte =%h, D =%h",addr,byte,data);
	`ifdef FDISP_PRO09
	$fdisplay(f_pro09_msg,"CPU write(pro09):\t\tADDR =%h, byte =%h, D =%h",addr,byte,data);
	`endif
`endif
end
endtask

task	pro09_cpu_write2w;
input	[31:0]	addr;
input	[31:0] 	data0,data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro09_tasks.write_2w (addr,data0,data1);
	$display("CPU write(pro09):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`ifdef FDISP_PRO09
	$fdisplay(f_pro09_msg,"CPU write(pro09):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`endif
`endif
end
endtask

task	pro09_cpu_write4w;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro09_tasks.write_4w (addr,data0,data1,data2,data3);
	$display("CPU write(pro09):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`ifdef FDISP_PRO09
	$fdisplay(f_pro09_msg,"CPU write(pro09):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`endif
`endif
end
endtask

task	pro09_cpu_write8w;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
input	[31:0] 	data4,data5,data6,data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro09_tasks.write_8w (addr,data0,data1,data2,data3,data4,data5,data6,data7);
	$display("CPU write(pro09):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	$display("\t\t\t\t\t\tD4 =%h, D5 =%h, D6 =%h, D7 =%h",data4,data5,data6,data7);
	`ifdef FDISP_PRO09
	$fdisplay(f_pro09_msg,"CPU write(pro09):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	$fdisplay(f_pro09_msg,"\t\t\t\t\t\tD4 =%h, D5 =%h, D6 =%h, D7 =%h",data4,data5,data6,data7);
	`endif
`endif
end
endtask

task	pro09_cpu_readbk1w;
input	[31:0]	addr;
input	[3:0]	byte;
inout	[31:0]	tmp_data;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro09_tasks.read_1w (addr,byte,tmp_data);
	$display("CPU read(pro09):\t\tADDR =%h, byte =%h, D =%h",addr,byte,tmp_data);
	`ifdef FDISP_PRO09
	$fdisplay(f_pro09_msg,"CPU read(pro09):\t\tADDR =%h, byte =%h, D =%h",addr,byte,tmp_data);
	`endif
`else
	tmp_data = 32'h0;
`endif
end
endtask

task	pro09_cpu_readbk2w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro09_tasks.read_2w (addr,tmp_data0,tmp_data1);
	$display("CPU read(pro09):\t\tADDR =%h, D0 =%h, D1 =%H",addr,tmp_data0,tmp_data1 );
	`ifdef FDISP_PRO09
	$fdisplay(f_pro09_msg,"CPU read(pro09):\t\tADDR =%h, D0 =%h, D1 =%H",addr,tmp_data0,tmp_data1 );
	`endif
`endif
end
endtask

task	pro09_cpu_readbk4w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1,tmp_data2,tmp_data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro09_tasks.read_4w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("CPU read(pro09):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`ifdef FDISP_PRO09
	$fdisplay(f_pro09_msg,"CPU read(pro09):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`endif
`endif
end
endtask

task	pro09_cpu_readbk8w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1,tmp_data2,tmp_data3;
inout	[31:0]	tmp_data4,tmp_data5,tmp_data6,tmp_data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro09_tasks.read_8w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	$display("CPU read(pro09):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("\t\t\t\t\t\tD4 =%h, D5 =%H, D6 =%h, D7 =%H",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`ifdef FDISP_PRO09
	$fdisplay(f_pro09_msg,"CPU read(pro09):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$fdisplay(f_pro09_msg,"\t\t\t\t\t\tD4 =%h, D5 =%H, D6 =%h, D7 =%H",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`endif
`endif
end
endtask

task	pro09_cpu_read1w;
input	[31:0]	addr;
input	[3:0]	byte;
input	[31:0]	exp_data;

reg	[31:0]	cpu_temp;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro09_tasks.read_1w (addr,byte,cpu_temp);
	cpu_temp = cpu_temp & {{8{byte[3]}},{8{byte[2]}},{8{byte[1]}},{8{byte[0]}}};
	if (cpu_temp !== ({{8{byte[3]}},{8{byte[2]}},{8{byte[1]}},{8{byte[0]}}} & exp_data)) begin
	  	$display("Error_T2_rsim:CPU read(pro09)\t%0t ADDR =%h, byte =%h, RDBK--D =%h, EXP--D =%h",
		$realtime,addr,byte,cpu_temp,exp_data);
		`ifdef FDISP_PRO09
		$fdisplay(f_pro09_msg,"Error_T2_rsim:CPU read(pro09)\t%0t ADDR =%h, byte =%h, RDBK--D =%h, EXP--D =%h",
		$realtime,addr,byte,cpu_temp,exp_data);
		`endif
	end else begin
	$display("CPU read(pro09):\t\tADDR =%h, byte =%h, D =%h",addr,byte,cpu_temp);
	`ifdef FDISP_PRO09
	$fdisplay(f_pro09_msg,"CPU read(pro09):\t\tADDR =%h, byte =%h, D =%h",addr,byte,cpu_temp);
	`endif
	end
`endif
end
endtask

task	pro09_cpu_read2w;
input	[31:0]	addr;
input	[31:0]	exp_data0;
input	[31:0]	exp_data1;
reg	[31:0]	tmp_data0,
			tmp_data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro09_tasks.read_2w (addr,tmp_data0,tmp_data1);
	if(tmp_data0!==exp_data0 || tmp_data1!==exp_data1) begin
	  	$display("Error_T2_rsim:CPU read(pro09)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h",
		$realtime,addr,tmp_data0,tmp_data1,exp_data0,exp_data1);
		`ifdef FDISP_PRO09
		$fdisplay(f_pro09_msg,"Error_T2_rsim:CPU read(pro09)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h",
		$realtime,addr,tmp_data0,tmp_data1,exp_data0,exp_data1);
		`endif
	end else begin
	$display("CPU read(pro09):\t\tADDR =%h, D0 =%h, D1 =%h",addr,tmp_data0,tmp_data1);
	`ifdef FDISP_PRO09
	$fdisplay(f_pro09_msg,"CPU read(pro09):\t\tADDR =%h, D0 =%h, D1 =%h",addr,tmp_data0,tmp_data1);
	`endif
	end
`endif
end
endtask

task	pro09_cpu_read4w;
input	[31:0]	addr;
input	[31:0]	exp_data0,
		exp_data1,
		exp_data2,
		exp_data3;
reg	[31:0]	tmp_data0,
		tmp_data1,
		tmp_data2,
		tmp_data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro09_tasks.read_4w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	if(tmp_data0!==exp_data0||tmp_data1!==exp_data1||
	   tmp_data2!==exp_data2||tmp_data3!==exp_data3) begin
	  	$display("Error_T2_rsim:CPU read(pro09)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		`ifdef FDISP_PRO09
		$fdisplay(f_pro09_msg,"Error_T2_rsim:CPU read(pro09)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		`endif
	end else begin
	$display("CPU read(pro09):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`ifdef FDISP_PRO09
	$fdisplay(f_pro09_msg,"CPU read(pro09):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`endif
	end
`endif
end
endtask

task	pro09_cpu_read8w;
input	[31:0]	addr;
input	[31:0]	exp_data0,
		exp_data1,
		exp_data2,
		exp_data3,
		exp_data4,
		exp_data5,
		exp_data6,
		exp_data7;
reg	[31:0]	tmp_data0,
		tmp_data1,
		tmp_data2,
		tmp_data3,
		tmp_data4,
		tmp_data5,
		tmp_data6,
		tmp_data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro09_tasks.read_8w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	if(tmp_data0!==exp_data0||tmp_data1!==exp_data1|| tmp_data2!==exp_data2||tmp_data3!==exp_data3 ||
	   tmp_data4!==exp_data4||tmp_data5!==exp_data5|| tmp_data6!==exp_data6||tmp_data7!==exp_data7)
	   begin
	  	$display("Error_T2_rsim:CPU read(pro09)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		$display("\t\t\t\t\t\tRDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		tmp_data4,tmp_data5,tmp_data6,tmp_data7,exp_data4,exp_data5,exp_data6,exp_data7);
		`ifdef FDISP_PRO09
		$fdisplay(f_pro09_msg,"Error_T2_rsim:CPU read(pro09)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		$fdisplay(f_pro09_msg,"\t\t\t\t\t\tRDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		tmp_data4,tmp_data5,tmp_data6,tmp_data7,exp_data4,exp_data5,exp_data6,exp_data7);
		`endif
	   end
	else begin
	$display("CPU read(pro09):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("\t\t\t\t\t\tD0 =%h, D1 =%h, D2 =%h, D3 =%h",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`ifdef FDISP_PRO09
	$fdisplay(f_pro09_msg,"CPU read(pro09):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$fdisplay(f_pro09_msg,"\t\t\t\t\t\tD0 =%h, D1 =%h, D2 =%h, D3 =%h",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`endif
	end
`endif
end
endtask

//=============== the basic task for pro10 processer ========================
task	pro10_cpu_write1w;
input	[31:0]	addr;
input	[3:0]	byte;
input	[31:0] 	data;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro10_tasks.write_1w (addr,byte,data);
	$display("CPU write(pro10):\t\tADDR =%h, byte =%h, D =%h",addr,byte,data);
	`ifdef FDISP_PRO10
	$fdisplay(f_pro10_msg,"CPU write(pro10):\t\tADDR =%h, byte =%h, D =%h",addr,byte,data);
	`endif
`endif
end
endtask

task	pro10_cpu_write2w;
input	[31:0]	addr;
input	[31:0] 	data0,data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro10_tasks.write_2w (addr,data0,data1);
	$display("CPU write(pro10):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`ifdef FDISP_PRO10
	$fdisplay(f_pro10_msg,"CPU write(pro10):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`endif
`endif
end
endtask

task	pro10_cpu_write4w;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro10_tasks.write_4w (addr,data0,data1,data2,data3);
	$display("CPU write(pro10):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`ifdef FDISP_PRO10
	$fdisplay(f_pro10_msg,"CPU write(pro10):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`endif
`endif
end
endtask

task	pro10_cpu_write8w;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
input	[31:0] 	data4,data5,data6,data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro10_tasks.write_8w (addr,data0,data1,data2,data3,data4,data5,data6,data7);
	$display("CPU write(pro10):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	$display("\t\t\t\t\t\tD4 =%h, D5 =%h, D6 =%h, D7 =%h",data4,data5,data6,data7);
	`ifdef FDISP_PRO10
	$fdisplay(f_pro10_msg,"CPU write(pro10):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	$fdisplay(f_pro10_msg,"\t\t\t\t\t\tD4 =%h, D5 =%h, D6 =%h, D7 =%h",data4,data5,data6,data7);
	`endif
`endif
end
endtask

task	pro10_cpu_readbk1w;
input	[31:0]	addr;
input	[3:0]	byte;
inout	[31:0]	tmp_data;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro10_tasks.read_1w (addr,byte,tmp_data);
	$display("CPU read(pro10):\t\tADDR =%h, byte =%h, D =%h",addr,byte,tmp_data);
	`ifdef FDISP_PRO10
	$fdisplay(f_pro10_msg,"CPU read(pro10):\t\tADDR =%h, byte =%h, D =%h",addr,byte,tmp_data);
	`endif
`else
	tmp_data = 32'h0;
`endif
end
endtask

task	pro10_cpu_readbk2w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro10_tasks.read_2w (addr,tmp_data0,tmp_data1);
	$display("CPU read(pro10):\t\tADDR =%h, D0 =%h, D1 =%H",addr,tmp_data0,tmp_data1 );
	`ifdef FDISP_PRO10
	$fdisplay(f_pro10_msg,"CPU read(pro10):\t\tADDR =%h, D0 =%h, D1 =%H",addr,tmp_data0,tmp_data1 );
	`endif
`endif
end
endtask

task	pro10_cpu_readbk4w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1,tmp_data2,tmp_data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro10_tasks.read_4w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("CPU read(pro10):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`ifdef FDISP_PRO10
	$fdisplay(f_pro10_msg,"CPU read(pro10):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`endif
`endif
end
endtask

task	pro10_cpu_readbk8w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1,tmp_data2,tmp_data3;
inout	[31:0]	tmp_data4,tmp_data5,tmp_data6,tmp_data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro10_tasks.read_8w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	$display("CPU read(pro10):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("\t\t\t\t\t\tD4 =%h, D5 =%H, D6 =%h, D7 =%H",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`ifdef FDISP_PRO10
	$fdisplay(f_pro10_msg,"CPU read(pro10):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$fdisplay(f_pro10_msg,"\t\t\t\t\t\tD4 =%h, D5 =%H, D6 =%h, D7 =%H",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`endif
`endif
end
endtask

task	pro10_cpu_read1w;
input	[31:0]	addr;
input	[3:0]	byte;
input	[31:0]	exp_data;

reg	[31:0]	cpu_temp;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro10_tasks.read_1w (addr,byte,cpu_temp);
	cpu_temp = cpu_temp & {{8{byte[3]}},{8{byte[2]}},{8{byte[1]}},{8{byte[0]}}};
	if (cpu_temp !== ({{8{byte[3]}},{8{byte[2]}},{8{byte[1]}},{8{byte[0]}}} & exp_data)) begin
	  	$display("Error_T2_rsim:CPU read(pro10)\t%0t ADDR =%h, byte =%h, RDBK--D =%h, EXP--D =%h",
		$realtime,addr,byte,cpu_temp,exp_data);
		`ifdef FDISP_PRO10
		$fdisplay(f_pro10_msg,"Error_T2_rsim:CPU read(pro10)\t%0t ADDR =%h, byte =%h, RDBK--D =%h, EXP--D =%h",
		$realtime,addr,byte,cpu_temp,exp_data);
		`endif
	end else begin
	$display("CPU read(pro10):\t\tADDR =%h, byte =%h, D =%h",addr,byte,cpu_temp);
	`ifdef FDISP_PRO10
	$fdisplay(f_pro10_msg,"CPU read(pro10):\t\tADDR =%h, byte =%h, D =%h",addr,byte,cpu_temp);
	`endif
	end
`endif
end
endtask

task	pro10_cpu_read2w;
input	[31:0]	addr;
input	[31:0]	exp_data0;
input	[31:0]	exp_data1;
reg	[31:0]	tmp_data0,
			tmp_data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro10_tasks.read_2w (addr,tmp_data0,tmp_data1);
	if(tmp_data0!==exp_data0 || tmp_data1!==exp_data1) begin
	  	$display("Error_T2_rsim:CPU read(pro10)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h",
		$realtime,addr,tmp_data0,tmp_data1,exp_data0,exp_data1);
		`ifdef FDISP_PRO10
		$fdisplay(f_pro10_msg,"Error_T2_rsim:CPU read(pro10)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h",
		$realtime,addr,tmp_data0,tmp_data1,exp_data0,exp_data1);
		`endif
	end else begin
	$display("CPU read(pro10):\t\tADDR =%h, D0 =%h, D1 =%h",addr,tmp_data0,tmp_data1);
	`ifdef FDISP_PRO10
	$fdisplay(f_pro10_msg,"CPU read(pro10):\t\tADDR =%h, D0 =%h, D1 =%h",addr,tmp_data0,tmp_data1);
	`endif
	end
`endif
end
endtask

task	pro10_cpu_read4w;
input	[31:0]	addr;
input	[31:0]	exp_data0,
		exp_data1,
		exp_data2,
		exp_data3;
reg	[31:0]	tmp_data0,
		tmp_data1,
		tmp_data2,
		tmp_data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro10_tasks.read_4w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	if(tmp_data0!==exp_data0||tmp_data1!==exp_data1||
	   tmp_data2!==exp_data2||tmp_data3!==exp_data3) begin
	  	$display("Error_T2_rsim:CPU read(pro10)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		`ifdef FDISP_PRO10
		$fdisplay(f_pro10_msg,"Error_T2_rsim:CPU read(pro10)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		`endif
	end else begin
	$display("CPU read(pro10):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`ifdef FDISP_PRO10
	$fdisplay(f_pro10_msg,"CPU read(pro10):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`endif
	end
`endif
end
endtask

task	pro10_cpu_read8w;
input	[31:0]	addr;
input	[31:0]	exp_data0,
		exp_data1,
		exp_data2,
		exp_data3,
		exp_data4,
		exp_data5,
		exp_data6,
		exp_data7;
reg	[31:0]	tmp_data0,
		tmp_data1,
		tmp_data2,
		tmp_data3,
		tmp_data4,
		tmp_data5,
		tmp_data6,
		tmp_data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro10_tasks.read_8w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	if(tmp_data0!==exp_data0||tmp_data1!==exp_data1|| tmp_data2!==exp_data2||tmp_data3!==exp_data3 ||
	   tmp_data4!==exp_data4||tmp_data5!==exp_data5|| tmp_data6!==exp_data6||tmp_data7!==exp_data7)
	   begin
	  	$display("Error_T2_rsim:CPU read(pro10)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		$display("\t\t\t\t\t\tRDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		tmp_data4,tmp_data5,tmp_data6,tmp_data7,exp_data4,exp_data5,exp_data6,exp_data7);
		`ifdef FDISP_PRO10
		$fdisplay(f_pro10_msg,"Error_T2_rsim:CPU read(pro10)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		$fdisplay(f_pro10_msg,"\t\t\t\t\t\tRDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		tmp_data4,tmp_data5,tmp_data6,tmp_data7,exp_data4,exp_data5,exp_data6,exp_data7);
		`endif
	   end
	else begin
	$display("CPU read(pro10):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("\t\t\t\t\t\tD0 =%h, D1 =%h, D2 =%h, D3 =%h",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`ifdef FDISP_PRO10
	$fdisplay(f_pro10_msg,"CPU read(pro10):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$fdisplay(f_pro10_msg,"\t\t\t\t\t\tD0 =%h, D1 =%h, D2 =%h, D3 =%h",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`endif
	end
`endif
end
endtask

//=============== the basic task for pro11 processer ========================
task	pro11_cpu_write1w;
input	[31:0]	addr;
input	[3:0]	byte;
input	[31:0] 	data;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro11_tasks.write_1w (addr,byte,data);
	$display("CPU write(pro11):\t\tADDR =%h, byte =%h, D =%h",addr,byte,data);
	`ifdef FDISP_PRO11
	$fdisplay(f_pro11_msg,"CPU write(pro11):\t\tADDR =%h, byte =%h, D =%h",addr,byte,data);
	`endif
`endif
end
endtask

task	pro11_cpu_write2w;
input	[31:0]	addr;
input	[31:0] 	data0,data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro11_tasks.write_2w (addr,data0,data1);
	$display("CPU write(pro11):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`ifdef FDISP_PRO11
	$fdisplay(f_pro11_msg,"CPU write(pro11):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`endif
`endif
end
endtask

task	pro11_cpu_write4w;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro11_tasks.write_4w (addr,data0,data1,data2,data3);
	$display("CPU write(pro11):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`ifdef FDISP_PRO11
	$fdisplay(f_pro11_msg,"CPU write(pro11):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`endif
`endif
end
endtask

task	pro11_cpu_write8w;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
input	[31:0] 	data4,data5,data6,data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro11_tasks.write_8w (addr,data0,data1,data2,data3,data4,data5,data6,data7);
	$display("CPU write(pro11):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	$display("\t\t\t\t\t\tD4 =%h, D5 =%h, D6 =%h, D7 =%h",data4,data5,data6,data7);
	`ifdef FDISP_PRO11
	$fdisplay(f_pro11_msg,"CPU write(pro11):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	$fdisplay(f_pro11_msg,"\t\t\t\t\t\tD4 =%h, D5 =%h, D6 =%h, D7 =%h",data4,data5,data6,data7);
	`endif
`endif
end
endtask

task	pro11_cpu_readbk1w;
input	[31:0]	addr;
input	[3:0]	byte;
inout	[31:0]	tmp_data;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro11_tasks.read_1w (addr,byte,tmp_data);
	$display("CPU read(pro11):\t\tADDR =%h, byte =%h, D =%h",addr,byte,tmp_data);
	`ifdef FDISP_PRO11
	$fdisplay(f_pro11_msg,"CPU read(pro11):\t\tADDR =%h, byte =%h, D =%h",addr,byte,tmp_data);
	`endif
`else
	tmp_data = 32'h0;
`endif
end
endtask

task	pro11_cpu_readbk2w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro11_tasks.read_2w (addr,tmp_data0,tmp_data1);
	$display("CPU read(pro11):\t\tADDR =%h, D0 =%h, D1 =%H",addr,tmp_data0,tmp_data1 );
	`ifdef FDISP_PRO11
	$fdisplay(f_pro11_msg,"CPU read(pro11):\t\tADDR =%h, D0 =%h, D1 =%H",addr,tmp_data0,tmp_data1 );
	`endif
`endif
end
endtask

task	pro11_cpu_readbk4w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1,tmp_data2,tmp_data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro11_tasks.read_4w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("CPU read(pro11):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`ifdef FDISP_PRO11
	$fdisplay(f_pro11_msg,"CPU read(pro11):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`endif
`endif
end
endtask

task	pro11_cpu_readbk8w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1,tmp_data2,tmp_data3;
inout	[31:0]	tmp_data4,tmp_data5,tmp_data6,tmp_data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro11_tasks.read_8w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	$display("CPU read(pro11):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("\t\t\t\t\t\tD4 =%h, D5 =%H, D6 =%h, D7 =%H",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`ifdef FDISP_PRO11
	$fdisplay(f_pro11_msg,"CPU read(pro11):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$fdisplay(f_pro11_msg,"\t\t\t\t\t\tD4 =%h, D5 =%H, D6 =%h, D7 =%H",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`endif
`endif
end
endtask

task	pro11_cpu_read1w;
input	[31:0]	addr;
input	[3:0]	byte;
input	[31:0]	exp_data;

reg	[31:0]	cpu_temp;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro11_tasks.read_1w (addr,byte,cpu_temp);
	cpu_temp = cpu_temp & {{8{byte[3]}},{8{byte[2]}},{8{byte[1]}},{8{byte[0]}}};
	if (cpu_temp !== ({{8{byte[3]}},{8{byte[2]}},{8{byte[1]}},{8{byte[0]}}} & exp_data)) begin
	  	$display("Error_T2_rsim:CPU read(pro11)\t%0t ADDR =%h, byte =%h, RDBK--D =%h, EXP--D =%h",
		$realtime,addr,byte,cpu_temp,exp_data);
		`ifdef FDISP_PRO11
		$fdisplay(f_pro11_msg,"Error_T2_rsim:CPU read(pro11)\t%0t ADDR =%h, byte =%h, RDBK--D =%h, EXP--D =%h",
		$realtime,addr,byte,cpu_temp,exp_data);
		`endif
	end else begin
	$display("CPU read(pro11):\t\tADDR =%h, byte =%h, D =%h",addr,byte,cpu_temp);
	`ifdef FDISP_PRO11
	$fdisplay(f_pro11_msg,"CPU read(pro11):\t\tADDR =%h, byte =%h, D =%h",addr,byte,cpu_temp);
	`endif
	end
`endif
end
endtask

task	pro11_cpu_read2w;
input	[31:0]	addr;
input	[31:0]	exp_data0;
input	[31:0]	exp_data1;
reg	[31:0]	tmp_data0,
			tmp_data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro11_tasks.read_2w (addr,tmp_data0,tmp_data1);
	if(tmp_data0!==exp_data0 || tmp_data1!==exp_data1) begin
	  	$display("Error_T2_rsim:CPU read(pro11)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h",
		$realtime,addr,tmp_data0,tmp_data1,exp_data0,exp_data1);
		`ifdef FDISP_PRO11
		$fdisplay(f_pro11_msg,"Error_T2_rsim:CPU read(pro11)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h",
		$realtime,addr,tmp_data0,tmp_data1,exp_data0,exp_data1);
		`endif
	end else begin
	$display("CPU read(pro11):\t\tADDR =%h, D0 =%h, D1 =%h",addr,tmp_data0,tmp_data1);
	`ifdef FDISP_PRO11
	$fdisplay(f_pro11_msg,"CPU read(pro11):\t\tADDR =%h, D0 =%h, D1 =%h",addr,tmp_data0,tmp_data1);
	`endif
	end
`endif
end
endtask

task	pro11_cpu_read4w;
input	[31:0]	addr;
input	[31:0]	exp_data0,
		exp_data1,
		exp_data2,
		exp_data3;
reg	[31:0]	tmp_data0,
		tmp_data1,
		tmp_data2,
		tmp_data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro11_tasks.read_4w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	if(tmp_data0!==exp_data0||tmp_data1!==exp_data1||
	   tmp_data2!==exp_data2||tmp_data3!==exp_data3) begin
	  	$display("Error_T2_rsim:CPU read(pro11)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		`ifdef FDISP_PRO11
		$fdisplay(f_pro11_msg,"Error_T2_rsim:CPU read(pro11)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		`endif
	end else begin
	$display("CPU read(pro11):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`ifdef FDISP_PRO11
	$fdisplay(f_pro11_msg,"CPU read(pro11):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`endif
	end
`endif
end
endtask

task	pro11_cpu_read8w;
input	[31:0]	addr;
input	[31:0]	exp_data0,
		exp_data1,
		exp_data2,
		exp_data3,
		exp_data4,
		exp_data5,
		exp_data6,
		exp_data7;
reg	[31:0]	tmp_data0,
		tmp_data1,
		tmp_data2,
		tmp_data3,
		tmp_data4,
		tmp_data5,
		tmp_data6,
		tmp_data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro11_tasks.read_8w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	if(tmp_data0!==exp_data0||tmp_data1!==exp_data1|| tmp_data2!==exp_data2||tmp_data3!==exp_data3 ||
	   tmp_data4!==exp_data4||tmp_data5!==exp_data5|| tmp_data6!==exp_data6||tmp_data7!==exp_data7)
	   begin
	  	$display("Error_T2_rsim:CPU read(pro11)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		$display("\t\t\t\t\t\tRDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		tmp_data4,tmp_data5,tmp_data6,tmp_data7,exp_data4,exp_data5,exp_data6,exp_data7);
		`ifdef FDISP_PRO11
		$fdisplay(f_pro11_msg,"Error_T2_rsim:CPU read(pro11)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		$fdisplay(f_pro11_msg,"\t\t\t\t\t\tRDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		tmp_data4,tmp_data5,tmp_data6,tmp_data7,exp_data4,exp_data5,exp_data6,exp_data7);
		`endif
	   end
	else begin
	$display("CPU read(pro11):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("\t\t\t\t\t\tD0 =%h, D1 =%h, D2 =%h, D3 =%h",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`ifdef FDISP_PRO11
	$fdisplay(f_pro11_msg,"CPU read(pro11):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$fdisplay(f_pro11_msg,"\t\t\t\t\t\tD0 =%h, D1 =%h, D2 =%h, D3 =%h",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`endif
	end
`endif
end
endtask

//=============== the basic task for pro12 processer ========================
task	pro12_cpu_write1w;
input	[31:0]	addr;
input	[3:0]	byte;
input	[31:0] 	data;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro12_tasks.write_1w (addr,byte,data);
	$display("CPU write(pro12):\t\tADDR =%h, byte =%h, D =%h",addr,byte,data);
	`ifdef FDISP_PRO12
	$fdisplay(f_pro12_msg,"CPU write(pro12):\t\tADDR =%h, byte =%h, D =%h",addr,byte,data);
	`endif
`endif
end
endtask

task	pro12_cpu_write2w;
input	[31:0]	addr;
input	[31:0] 	data0,data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro12_tasks.write_2w (addr,data0,data1);
	$display("CPU write(pro12):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`ifdef FDISP_PRO12
	$fdisplay(f_pro12_msg,"CPU write(pro12):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`endif
`endif
end
endtask

task	pro12_cpu_write4w;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro12_tasks.write_4w (addr,data0,data1,data2,data3);
	$display("CPU write(pro12):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`ifdef FDISP_PRO12
	$fdisplay(f_pro12_msg,"CPU write(pro12):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`endif
`endif
end
endtask

task	pro12_cpu_write8w;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
input	[31:0] 	data4,data5,data6,data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro12_tasks.write_8w (addr,data0,data1,data2,data3,data4,data5,data6,data7);
	$display("CPU write(pro12):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	$display("\t\t\t\t\t\tD4 =%h, D5 =%h, D6 =%h, D7 =%h",data4,data5,data6,data7);
	`ifdef FDISP_PRO12
	$fdisplay(f_pro12_msg,"CPU write(pro12):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	$fdisplay(f_pro12_msg,"\t\t\t\t\t\tD4 =%h, D5 =%h, D6 =%h, D7 =%h",data4,data5,data6,data7);
	`endif
`endif
end
endtask

task	pro12_cpu_readbk1w;
input	[31:0]	addr;
input	[3:0]	byte;
inout	[31:0]	tmp_data;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro12_tasks.read_1w (addr,byte,tmp_data);
	$display("CPU read(pro12):\t\tADDR =%h, byte =%h, D =%h",addr,byte,tmp_data);
	`ifdef FDISP_PRO12
	$fdisplay(f_pro12_msg,"CPU read(pro12):\t\tADDR =%h, byte =%h, D =%h",addr,byte,tmp_data);
	`endif
`else
	tmp_data = 32'h0;
`endif
end
endtask

task	pro12_cpu_readbk2w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro12_tasks.read_2w (addr,tmp_data0,tmp_data1);
	$display("CPU read(pro12):\t\tADDR =%h, D0 =%h, D1 =%H",addr,tmp_data0,tmp_data1 );
	`ifdef FDISP_PRO12
	$fdisplay(f_pro12_msg,"CPU read(pro12):\t\tADDR =%h, D0 =%h, D1 =%H",addr,tmp_data0,tmp_data1 );
	`endif
`endif
end
endtask

task	pro12_cpu_readbk4w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1,tmp_data2,tmp_data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro12_tasks.read_4w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("CPU read(pro12):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`ifdef FDISP_PRO12
	$fdisplay(f_pro12_msg,"CPU read(pro12):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`endif
`endif
end
endtask

task	pro12_cpu_readbk8w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1,tmp_data2,tmp_data3;
inout	[31:0]	tmp_data4,tmp_data5,tmp_data6,tmp_data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro12_tasks.read_8w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	$display("CPU read(pro12):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("\t\t\t\t\t\tD4 =%h, D5 =%H, D6 =%h, D7 =%H",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`ifdef FDISP_PRO12
	$fdisplay(f_pro12_msg,"CPU read(pro12):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$fdisplay(f_pro12_msg,"\t\t\t\t\t\tD4 =%h, D5 =%H, D6 =%h, D7 =%H",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`endif
`endif
end
endtask

task	pro12_cpu_read1w;
input	[31:0]	addr;
input	[3:0]	byte;
input	[31:0]	exp_data;

reg	[31:0]	cpu_temp;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro12_tasks.read_1w (addr,byte,cpu_temp);
	cpu_temp = cpu_temp & {{8{byte[3]}},{8{byte[2]}},{8{byte[1]}},{8{byte[0]}}};
	if (cpu_temp !== ({{8{byte[3]}},{8{byte[2]}},{8{byte[1]}},{8{byte[0]}}} & exp_data)) begin
	  	$display("Error_T2_rsim:CPU read(pro12)\t%0t ADDR =%h, byte =%h, RDBK--D =%h, EXP--D =%h",
		$realtime,addr,byte,cpu_temp,exp_data);
		`ifdef FDISP_PRO12
		$fdisplay(f_pro12_msg,"Error_T2_rsim:CPU read(pro12)\t%0t ADDR =%h, byte =%h, RDBK--D =%h, EXP--D =%h",
		$realtime,addr,byte,cpu_temp,exp_data);
		`endif
	end else begin
	$display("CPU read(pro12):\t\tADDR =%h, byte =%h, D =%h",addr,byte,cpu_temp);
	`ifdef FDISP_PRO12
	$fdisplay(f_pro12_msg,"CPU read(pro12):\t\tADDR =%h, byte =%h, D =%h",addr,byte,cpu_temp);
	`endif
	end
`endif
end
endtask

task	pro12_cpu_read2w;
input	[31:0]	addr;
input	[31:0]	exp_data0;
input	[31:0]	exp_data1;
reg	[31:0]	tmp_data0,
			tmp_data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro12_tasks.read_2w (addr,tmp_data0,tmp_data1);
	if(tmp_data0!==exp_data0 || tmp_data1!==exp_data1) begin
	  	$display("Error_T2_rsim:CPU read(pro12)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h",
		$realtime,addr,tmp_data0,tmp_data1,exp_data0,exp_data1);
		`ifdef FDISP_PRO12
		$fdisplay(f_pro12_msg,"Error_T2_rsim:CPU read(pro12)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h",
		$realtime,addr,tmp_data0,tmp_data1,exp_data0,exp_data1);
		`endif
	end else begin
	$display("CPU read(pro12):\t\tADDR =%h, D0 =%h, D1 =%h",addr,tmp_data0,tmp_data1);
	`ifdef FDISP_PRO12
	$fdisplay(f_pro12_msg,"CPU read(pro12):\t\tADDR =%h, D0 =%h, D1 =%h",addr,tmp_data0,tmp_data1);
	`endif
	end
`endif
end
endtask

task	pro12_cpu_read4w;
input	[31:0]	addr;
input	[31:0]	exp_data0,
		exp_data1,
		exp_data2,
		exp_data3;
reg	[31:0]	tmp_data0,
		tmp_data1,
		tmp_data2,
		tmp_data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro12_tasks.read_4w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	if(tmp_data0!==exp_data0||tmp_data1!==exp_data1||
	   tmp_data2!==exp_data2||tmp_data3!==exp_data3) begin
	  	$display("Error_T2_rsim:CPU read(pro12)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		`ifdef FDISP_PRO12
		$fdisplay(f_pro12_msg,"Error_T2_rsim:CPU read(pro12)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		`endif
	end else begin
	$display("CPU read(pro12):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`ifdef FDISP_PRO12
	$fdisplay(f_pro12_msg,"CPU read(pro12):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`endif
	end
`endif
end
endtask

task	pro12_cpu_read8w;
input	[31:0]	addr;
input	[31:0]	exp_data0,
		exp_data1,
		exp_data2,
		exp_data3,
		exp_data4,
		exp_data5,
		exp_data6,
		exp_data7;
reg	[31:0]	tmp_data0,
		tmp_data1,
		tmp_data2,
		tmp_data3,
		tmp_data4,
		tmp_data5,
		tmp_data6,
		tmp_data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro12_tasks.read_8w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	if(tmp_data0!==exp_data0||tmp_data1!==exp_data1|| tmp_data2!==exp_data2||tmp_data3!==exp_data3 ||
	   tmp_data4!==exp_data4||tmp_data5!==exp_data5|| tmp_data6!==exp_data6||tmp_data7!==exp_data7)
	   begin
	  	$display("Error_T2_rsim:CPU read(pro12)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		$display("\t\t\t\t\t\tRDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		tmp_data4,tmp_data5,tmp_data6,tmp_data7,exp_data4,exp_data5,exp_data6,exp_data7);
		`ifdef FDISP_PRO12
		$fdisplay(f_pro12_msg,"Error_T2_rsim:CPU read(pro12)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		$fdisplay(f_pro12_msg,"\t\t\t\t\t\tRDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		tmp_data4,tmp_data5,tmp_data6,tmp_data7,exp_data4,exp_data5,exp_data6,exp_data7);
		`endif
	   end
	else begin
	$display("CPU read(pro12):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("\t\t\t\t\t\tD0 =%h, D1 =%h, D2 =%h, D3 =%h",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`ifdef FDISP_PRO12
	$fdisplay(f_pro12_msg,"CPU read(pro12):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$fdisplay(f_pro12_msg,"\t\t\t\t\t\tD0 =%h, D1 =%h, D2 =%h, D3 =%h",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`endif
	end
`endif
end
endtask

//=============== the basic task for pro13 processer ========================
task	pro13_cpu_write1w;
input	[31:0]	addr;
input	[3:0]	byte;
input	[31:0] 	data;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro13_tasks.write_1w (addr,byte,data);
	$display("CPU write(pro13):\t\tADDR =%h, byte =%h, D =%h",addr,byte,data);
	`ifdef FDISP_PRO13
	$fdisplay(f_pro13_msg,"CPU write(pro13):\t\tADDR =%h, byte =%h, D =%h",addr,byte,data);
	`endif
`endif
end
endtask

task	pro13_cpu_write2w;
input	[31:0]	addr;
input	[31:0] 	data0,data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro13_tasks.write_2w (addr,data0,data1);
	$display("CPU write(pro13):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`ifdef FDISP_PRO13
	$fdisplay(f_pro13_msg,"CPU write(pro13):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`endif
`endif
end
endtask

task	pro13_cpu_write4w;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro13_tasks.write_4w (addr,data0,data1,data2,data3);
	$display("CPU write(pro13):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`ifdef FDISP_PRO13
	$fdisplay(f_pro13_msg,"CPU write(pro13):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`endif
`endif
end
endtask

task	pro13_cpu_write8w;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
input	[31:0] 	data4,data5,data6,data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro13_tasks.write_8w (addr,data0,data1,data2,data3,data4,data5,data6,data7);
	$display("CPU write(pro13):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	$display("\t\t\t\t\t\tD4 =%h, D5 =%h, D6 =%h, D7 =%h",data4,data5,data6,data7);
	`ifdef FDISP_PRO13
	$fdisplay(f_pro13_msg,"CPU write(pro13):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	$fdisplay(f_pro13_msg,"\t\t\t\t\t\tD4 =%h, D5 =%h, D6 =%h, D7 =%h",data4,data5,data6,data7);
	`endif
`endif
end
endtask

task	pro13_cpu_readbk1w;
input	[31:0]	addr;
input	[3:0]	byte;
inout	[31:0]	tmp_data;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro13_tasks.read_1w (addr,byte,tmp_data);
	$display("CPU read(pro13):\t\tADDR =%h, byte =%h, D =%h",addr,byte,tmp_data);
	`ifdef FDISP_PRO13
	$fdisplay(f_pro13_msg,"CPU read(pro13):\t\tADDR =%h, byte =%h, D =%h",addr,byte,tmp_data);
	`endif
`else
	tmp_data = 32'h0;
`endif
end
endtask

task	pro13_cpu_readbk2w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro13_tasks.read_2w (addr,tmp_data0,tmp_data1);
	$display("CPU read(pro13):\t\tADDR =%h, D0 =%h, D1 =%H",addr,tmp_data0,tmp_data1 );
	`ifdef FDISP_PRO13
	$fdisplay(f_pro13_msg,"CPU read(pro13):\t\tADDR =%h, D0 =%h, D1 =%H",addr,tmp_data0,tmp_data1 );
	`endif
`endif
end
endtask

task	pro13_cpu_readbk4w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1,tmp_data2,tmp_data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro13_tasks.read_4w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("CPU read(pro13):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`ifdef FDISP_PRO13
	$fdisplay(f_pro13_msg,"CPU read(pro13):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`endif
`endif
end
endtask

task	pro13_cpu_readbk8w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1,tmp_data2,tmp_data3;
inout	[31:0]	tmp_data4,tmp_data5,tmp_data6,tmp_data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro13_tasks.read_8w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	$display("CPU read(pro13):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("\t\t\t\t\t\tD4 =%h, D5 =%H, D6 =%h, D7 =%H",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`ifdef FDISP_PRO13
	$fdisplay(f_pro13_msg,"CPU read(pro13):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$fdisplay(f_pro13_msg,"\t\t\t\t\t\tD4 =%h, D5 =%H, D6 =%h, D7 =%H",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`endif
`endif
end
endtask

task	pro13_cpu_read1w;
input	[31:0]	addr;
input	[3:0]	byte;
input	[31:0]	exp_data;

reg	[31:0]	cpu_temp;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro13_tasks.read_1w (addr,byte,cpu_temp);
	cpu_temp = cpu_temp & {{8{byte[3]}},{8{byte[2]}},{8{byte[1]}},{8{byte[0]}}};
	if (cpu_temp !== ({{8{byte[3]}},{8{byte[2]}},{8{byte[1]}},{8{byte[0]}}} & exp_data)) begin
	  	$display("Error_T2_rsim:CPU read(pro13)\t%0t ADDR =%h, byte =%h, RDBK--D =%h, EXP--D =%h",
		$realtime,addr,byte,cpu_temp,exp_data);
		`ifdef FDISP_PRO13
		$fdisplay(f_pro13_msg,"Error_T2_rsim:CPU read(pro13)\t%0t ADDR =%h, byte =%h, RDBK--D =%h, EXP--D =%h",
		$realtime,addr,byte,cpu_temp,exp_data);
		`endif
	end else begin
	$display("CPU read(pro13):\t\tADDR =%h, byte =%h, D =%h",addr,byte,cpu_temp);
	`ifdef FDISP_PRO13
	$fdisplay(f_pro13_msg,"CPU read(pro13):\t\tADDR =%h, byte =%h, D =%h",addr,byte,cpu_temp);
	`endif
	end
`endif
end
endtask

task	pro13_cpu_read2w;
input	[31:0]	addr;
input	[31:0]	exp_data0;
input	[31:0]	exp_data1;
reg	[31:0]	tmp_data0,
			tmp_data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro13_tasks.read_2w (addr,tmp_data0,tmp_data1);
	if(tmp_data0!==exp_data0 || tmp_data1!==exp_data1) begin
	  	$display("Error_T2_rsim:CPU read(pro13)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h",
		$realtime,addr,tmp_data0,tmp_data1,exp_data0,exp_data1);
		`ifdef FDISP_PRO13
		$fdisplay(f_pro13_msg,"Error_T2_rsim:CPU read(pro13)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h",
		$realtime,addr,tmp_data0,tmp_data1,exp_data0,exp_data1);
		`endif
	end else begin
	$display("CPU read(pro13):\t\tADDR =%h, D0 =%h, D1 =%h",addr,tmp_data0,tmp_data1);
	`ifdef FDISP_PRO13
	$fdisplay(f_pro13_msg,"CPU read(pro13):\t\tADDR =%h, D0 =%h, D1 =%h",addr,tmp_data0,tmp_data1);
	`endif
	end
`endif
end
endtask

task	pro13_cpu_read4w;
input	[31:0]	addr;
input	[31:0]	exp_data0,
		exp_data1,
		exp_data2,
		exp_data3;
reg	[31:0]	tmp_data0,
		tmp_data1,
		tmp_data2,
		tmp_data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro13_tasks.read_4w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	if(tmp_data0!==exp_data0||tmp_data1!==exp_data1||
	   tmp_data2!==exp_data2||tmp_data3!==exp_data3) begin
	  	$display("Error_T2_rsim:CPU read(pro13)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		`ifdef FDISP_PRO13
		$fdisplay(f_pro13_msg,"Error_T2_rsim:CPU read(pro13)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		`endif
	end else begin
	$display("CPU read(pro13):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`ifdef FDISP_PRO13
	$fdisplay(f_pro13_msg,"CPU read(pro13):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`endif
	end
`endif
end
endtask

task	pro13_cpu_read8w;
input	[31:0]	addr;
input	[31:0]	exp_data0,
		exp_data1,
		exp_data2,
		exp_data3,
		exp_data4,
		exp_data5,
		exp_data6,
		exp_data7;
reg	[31:0]	tmp_data0,
		tmp_data1,
		tmp_data2,
		tmp_data3,
		tmp_data4,
		tmp_data5,
		tmp_data6,
		tmp_data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro13_tasks.read_8w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	if(tmp_data0!==exp_data0||tmp_data1!==exp_data1|| tmp_data2!==exp_data2||tmp_data3!==exp_data3 ||
	   tmp_data4!==exp_data4||tmp_data5!==exp_data5|| tmp_data6!==exp_data6||tmp_data7!==exp_data7)
	   begin
	  	$display("Error_T2_rsim:CPU read(pro13)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		$display("\t\t\t\t\t\tRDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		tmp_data4,tmp_data5,tmp_data6,tmp_data7,exp_data4,exp_data5,exp_data6,exp_data7);
		`ifdef FDISP_PRO13
		$fdisplay(f_pro13_msg,"Error_T2_rsim:CPU read(pro13)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		$fdisplay(f_pro13_msg,"\t\t\t\t\t\tRDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		tmp_data4,tmp_data5,tmp_data6,tmp_data7,exp_data4,exp_data5,exp_data6,exp_data7);
		`endif
	   end
	else begin
	$display("CPU read(pro13):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("\t\t\t\t\t\tD0 =%h, D1 =%h, D2 =%h, D3 =%h",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`ifdef FDISP_PRO13
	$fdisplay(f_pro13_msg,"CPU read(pro13):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$fdisplay(f_pro13_msg,"\t\t\t\t\t\tD0 =%h, D1 =%h, D2 =%h, D3 =%h",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`endif
	end
`endif
end
endtask

//=============== the basic task for pro14 processer ========================
task	pro14_cpu_write1w;
input	[31:0]	addr;
input	[3:0]	byte;
input	[31:0] 	data;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro14_tasks.write_1w (addr,byte,data);
	$display("CPU write(pro14):\t\tADDR =%h, byte =%h, D =%h",addr,byte,data);
	`ifdef FDISP_PRO14
	$fdisplay(f_pro14_msg,"CPU write(pro14):\t\tADDR =%h, byte =%h, D =%h",addr,byte,data);
	`endif
`endif
end
endtask

task	pro14_cpu_write2w;
input	[31:0]	addr;
input	[31:0] 	data0,data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro14_tasks.write_2w (addr,data0,data1);
	$display("CPU write(pro14):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`ifdef FDISP_PRO14
	$fdisplay(f_pro14_msg,"CPU write(pro14):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`endif
`endif
end
endtask

task	pro14_cpu_write4w;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro14_tasks.write_4w (addr,data0,data1,data2,data3);
	$display("CPU write(pro14):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`ifdef FDISP_PRO14
	$fdisplay(f_pro14_msg,"CPU write(pro14):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`endif
`endif
end
endtask

task	pro14_cpu_write8w;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
input	[31:0] 	data4,data5,data6,data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro14_tasks.write_8w (addr,data0,data1,data2,data3,data4,data5,data6,data7);
	$display("CPU write(pro14):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	$display("\t\t\t\t\t\tD4 =%h, D5 =%h, D6 =%h, D7 =%h",data4,data5,data6,data7);
	`ifdef FDISP_PRO14
	$fdisplay(f_pro14_msg,"CPU write(pro14):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	$fdisplay(f_pro14_msg,"\t\t\t\t\t\tD4 =%h, D5 =%h, D6 =%h, D7 =%h",data4,data5,data6,data7);
	`endif
`endif
end
endtask

task	pro14_cpu_readbk1w;
input	[31:0]	addr;
input	[3:0]	byte;
inout	[31:0]	tmp_data;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro14_tasks.read_1w (addr,byte,tmp_data);
	$display("CPU read(pro14):\t\tADDR =%h, byte =%h, D =%h",addr,byte,tmp_data);
	`ifdef FDISP_PRO14
	$fdisplay(f_pro14_msg,"CPU read(pro14):\t\tADDR =%h, byte =%h, D =%h",addr,byte,tmp_data);
	`endif
`else
	tmp_data = 32'h0;
`endif
end
endtask

task	pro14_cpu_readbk2w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro14_tasks.read_2w (addr,tmp_data0,tmp_data1);
	$display("CPU read(pro14):\t\tADDR =%h, D0 =%h, D1 =%H",addr,tmp_data0,tmp_data1 );
	`ifdef FDISP_PRO14
	$fdisplay(f_pro14_msg,"CPU read(pro14):\t\tADDR =%h, D0 =%h, D1 =%H",addr,tmp_data0,tmp_data1 );
	`endif
`endif
end
endtask

task	pro14_cpu_readbk4w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1,tmp_data2,tmp_data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro14_tasks.read_4w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("CPU read(pro14):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`ifdef FDISP_PRO14
	$fdisplay(f_pro14_msg,"CPU read(pro14):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`endif
`endif
end
endtask

task	pro14_cpu_readbk8w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1,tmp_data2,tmp_data3;
inout	[31:0]	tmp_data4,tmp_data5,tmp_data6,tmp_data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro14_tasks.read_8w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	$display("CPU read(pro14):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("\t\t\t\t\t\tD4 =%h, D5 =%H, D6 =%h, D7 =%H",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`ifdef FDISP_PRO14
	$fdisplay(f_pro14_msg,"CPU read(pro14):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$fdisplay(f_pro14_msg,"\t\t\t\t\t\tD4 =%h, D5 =%H, D6 =%h, D7 =%H",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`endif
`endif
end
endtask

task	pro14_cpu_read1w;
input	[31:0]	addr;
input	[3:0]	byte;
input	[31:0]	exp_data;

reg	[31:0]	cpu_temp;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro14_tasks.read_1w (addr,byte,cpu_temp);
	cpu_temp = cpu_temp & {{8{byte[3]}},{8{byte[2]}},{8{byte[1]}},{8{byte[0]}}};
	if (cpu_temp !== ({{8{byte[3]}},{8{byte[2]}},{8{byte[1]}},{8{byte[0]}}} & exp_data)) begin
	  	$display("Error_T2_rsim:CPU read(pro14)\t%0t ADDR =%h, byte =%h, RDBK--D =%h, EXP--D =%h",
		$realtime,addr,byte,cpu_temp,exp_data);
		`ifdef FDISP_PRO14
		$fdisplay(f_pro14_msg,"Error_T2_rsim:CPU read(pro14)\t%0t ADDR =%h, byte =%h, RDBK--D =%h, EXP--D =%h",
		$realtime,addr,byte,cpu_temp,exp_data);
		`endif
	end else begin
	$display("CPU read(pro14):\t\tADDR =%h, byte =%h, D =%h",addr,byte,cpu_temp);
	`ifdef FDISP_PRO14
	$fdisplay(f_pro14_msg,"CPU read(pro14):\t\tADDR =%h, byte =%h, D =%h",addr,byte,cpu_temp);
	`endif
	end
`endif
end
endtask

task	pro14_cpu_read2w;
input	[31:0]	addr;
input	[31:0]	exp_data0;
input	[31:0]	exp_data1;
reg	[31:0]	tmp_data0,
			tmp_data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro14_tasks.read_2w (addr,tmp_data0,tmp_data1);
	if(tmp_data0!==exp_data0 || tmp_data1!==exp_data1) begin
	  	$display("Error_T2_rsim:CPU read(pro14)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h",
		$realtime,addr,tmp_data0,tmp_data1,exp_data0,exp_data1);
		`ifdef FDISP_PRO14
		$fdisplay(f_pro14_msg,"Error_T2_rsim:CPU read(pro14)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h",
		$realtime,addr,tmp_data0,tmp_data1,exp_data0,exp_data1);
		`endif
	end else begin
	$display("CPU read(pro14):\t\tADDR =%h, D0 =%h, D1 =%h",addr,tmp_data0,tmp_data1);
	`ifdef FDISP_PRO14
	$fdisplay(f_pro14_msg,"CPU read(pro14):\t\tADDR =%h, D0 =%h, D1 =%h",addr,tmp_data0,tmp_data1);
	`endif
	end
`endif
end
endtask

task	pro14_cpu_read4w;
input	[31:0]	addr;
input	[31:0]	exp_data0,
		exp_data1,
		exp_data2,
		exp_data3;
reg	[31:0]	tmp_data0,
		tmp_data1,
		tmp_data2,
		tmp_data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro14_tasks.read_4w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	if(tmp_data0!==exp_data0||tmp_data1!==exp_data1||
	   tmp_data2!==exp_data2||tmp_data3!==exp_data3) begin
	  	$display("Error_T2_rsim:CPU read(pro14)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		`ifdef FDISP_PRO14
		$fdisplay(f_pro14_msg,"Error_T2_rsim:CPU read(pro14)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		`endif
	end else begin
	$display("CPU read(pro14):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`ifdef FDISP_PRO14
	$fdisplay(f_pro14_msg,"CPU read(pro14):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`endif
	end
`endif
end
endtask

task	pro14_cpu_read8w;
input	[31:0]	addr;
input	[31:0]	exp_data0,
		exp_data1,
		exp_data2,
		exp_data3,
		exp_data4,
		exp_data5,
		exp_data6,
		exp_data7;
reg	[31:0]	tmp_data0,
		tmp_data1,
		tmp_data2,
		tmp_data3,
		tmp_data4,
		tmp_data5,
		tmp_data6,
		tmp_data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro14_tasks.read_8w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	if(tmp_data0!==exp_data0||tmp_data1!==exp_data1|| tmp_data2!==exp_data2||tmp_data3!==exp_data3 ||
	   tmp_data4!==exp_data4||tmp_data5!==exp_data5|| tmp_data6!==exp_data6||tmp_data7!==exp_data7)
	   begin
	  	$display("Error_T2_rsim:CPU read(pro14)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		$display("\t\t\t\t\t\tRDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		tmp_data4,tmp_data5,tmp_data6,tmp_data7,exp_data4,exp_data5,exp_data6,exp_data7);
		`ifdef FDISP_PRO14
		$fdisplay(f_pro14_msg,"Error_T2_rsim:CPU read(pro14)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		$fdisplay(f_pro14_msg,"\t\t\t\t\t\tRDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		tmp_data4,tmp_data5,tmp_data6,tmp_data7,exp_data4,exp_data5,exp_data6,exp_data7);
		`endif
	   end
	else begin
	$display("CPU read(pro14):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("\t\t\t\t\t\tD0 =%h, D1 =%h, D2 =%h, D3 =%h",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`ifdef FDISP_PRO14
	$fdisplay(f_pro14_msg,"CPU read(pro14):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$fdisplay(f_pro14_msg,"\t\t\t\t\t\tD0 =%h, D1 =%h, D2 =%h, D3 =%h",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`endif
	end
`endif
end
endtask

//=============== the basic task for pro15 processer ========================
task	pro15_cpu_write1w;
input	[31:0]	addr;
input	[3:0]	byte;
input	[31:0] 	data;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro15_tasks.write_1w (addr,byte,data);
	$display("CPU write(pro15):\t\tADDR =%h, byte =%h, D =%h",addr,byte,data);
	`ifdef FDISP_PRO15
	$fdisplay(f_pro15_msg,"CPU write(pro15):\t\tADDR =%h, byte =%h, D =%h",addr,byte,data);
	`endif
`endif
end
endtask

task	pro15_cpu_write2w;
input	[31:0]	addr;
input	[31:0] 	data0,data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro15_tasks.write_2w (addr,data0,data1);
	$display("CPU write(pro15):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`ifdef FDISP_PRO15
	$fdisplay(f_pro15_msg,"CPU write(pro15):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`endif
`endif
end
endtask

task	pro15_cpu_write4w;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro15_tasks.write_4w (addr,data0,data1,data2,data3);
	$display("CPU write(pro15):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`ifdef FDISP_PRO15
	$fdisplay(f_pro15_msg,"CPU write(pro15):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`endif
`endif
end
endtask

task	pro15_cpu_write8w;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
input	[31:0] 	data4,data5,data6,data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro15_tasks.write_8w (addr,data0,data1,data2,data3,data4,data5,data6,data7);
	$display("CPU write(pro15):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	$display("\t\t\t\t\t\tD4 =%h, D5 =%h, D6 =%h, D7 =%h",data4,data5,data6,data7);
	`ifdef FDISP_PRO15
	$fdisplay(f_pro15_msg,"CPU write(pro15):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	$fdisplay(f_pro15_msg,"\t\t\t\t\t\tD4 =%h, D5 =%h, D6 =%h, D7 =%h",data4,data5,data6,data7);
	`endif
`endif
end
endtask

task	pro15_cpu_readbk1w;
input	[31:0]	addr;
input	[3:0]	byte;
inout	[31:0]	tmp_data;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro15_tasks.read_1w (addr,byte,tmp_data);
	$display("CPU read(pro15):\t\tADDR =%h, byte =%h, D =%h",addr,byte,tmp_data);
	`ifdef FDISP_PRO15
	$fdisplay(f_pro15_msg,"CPU read(pro15):\t\tADDR =%h, byte =%h, D =%h",addr,byte,tmp_data);
	`endif
`else
	tmp_data = 32'h0;
`endif
end
endtask

task	pro15_cpu_readbk2w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro15_tasks.read_2w (addr,tmp_data0,tmp_data1);
	$display("CPU read(pro15):\t\tADDR =%h, D0 =%h, D1 =%H",addr,tmp_data0,tmp_data1 );
	`ifdef FDISP_PRO15
	$fdisplay(f_pro15_msg,"CPU read(pro15):\t\tADDR =%h, D0 =%h, D1 =%H",addr,tmp_data0,tmp_data1 );
	`endif
`endif
end
endtask

task	pro15_cpu_readbk4w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1,tmp_data2,tmp_data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro15_tasks.read_4w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("CPU read(pro15):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`ifdef FDISP_PRO15
	$fdisplay(f_pro15_msg,"CPU read(pro15):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`endif
`endif
end
endtask

task	pro15_cpu_readbk8w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1,tmp_data2,tmp_data3;
inout	[31:0]	tmp_data4,tmp_data5,tmp_data6,tmp_data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro15_tasks.read_8w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	$display("CPU read(pro15):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("\t\t\t\t\t\tD4 =%h, D5 =%H, D6 =%h, D7 =%H",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`ifdef FDISP_PRO15
	$fdisplay(f_pro15_msg,"CPU read(pro15):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$fdisplay(f_pro15_msg,"\t\t\t\t\t\tD4 =%h, D5 =%H, D6 =%h, D7 =%H",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`endif
`endif
end
endtask

task	pro15_cpu_read1w;
input	[31:0]	addr;
input	[3:0]	byte;
input	[31:0]	exp_data;

reg	[31:0]	cpu_temp;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro15_tasks.read_1w (addr,byte,cpu_temp);
	cpu_temp = cpu_temp & {{8{byte[3]}},{8{byte[2]}},{8{byte[1]}},{8{byte[0]}}};
	if (cpu_temp !== ({{8{byte[3]}},{8{byte[2]}},{8{byte[1]}},{8{byte[0]}}} & exp_data)) begin
	  	$display("Error_T2_rsim:CPU read(pro15)\t%0t ADDR =%h, byte =%h, RDBK--D =%h, EXP--D =%h",
		$realtime,addr,byte,cpu_temp,exp_data);
		`ifdef FDISP_PRO15
		$fdisplay(f_pro15_msg,"Error_T2_rsim:CPU read(pro15)\t%0t ADDR =%h, byte =%h, RDBK--D =%h, EXP--D =%h",
		$realtime,addr,byte,cpu_temp,exp_data);
		`endif
	end else begin
	$display("CPU read(pro15):\t\tADDR =%h, byte =%h, D =%h",addr,byte,cpu_temp);
	`ifdef FDISP_PRO15
	$fdisplay(f_pro15_msg,"CPU read(pro15):\t\tADDR =%h, byte =%h, D =%h",addr,byte,cpu_temp);
	`endif
	end
`endif
end
endtask

task	pro15_cpu_read2w;
input	[31:0]	addr;
input	[31:0]	exp_data0;
input	[31:0]	exp_data1;
reg	[31:0]	tmp_data0,
			tmp_data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro15_tasks.read_2w (addr,tmp_data0,tmp_data1);
	if(tmp_data0!==exp_data0 || tmp_data1!==exp_data1) begin
	  	$display("Error_T2_rsim:CPU read(pro15)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h",
		$realtime,addr,tmp_data0,tmp_data1,exp_data0,exp_data1);
		`ifdef FDISP_PRO15
		$fdisplay(f_pro15_msg,"Error_T2_rsim:CPU read(pro15)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h",
		$realtime,addr,tmp_data0,tmp_data1,exp_data0,exp_data1);
		`endif
	end else begin
	$display("CPU read(pro15):\t\tADDR =%h, D0 =%h, D1 =%h",addr,tmp_data0,tmp_data1);
	`ifdef FDISP_PRO15
	$fdisplay(f_pro15_msg,"CPU read(pro15):\t\tADDR =%h, D0 =%h, D1 =%h",addr,tmp_data0,tmp_data1);
	`endif
	end
`endif
end
endtask

task	pro15_cpu_read4w;
input	[31:0]	addr;
input	[31:0]	exp_data0,
		exp_data1,
		exp_data2,
		exp_data3;
reg	[31:0]	tmp_data0,
		tmp_data1,
		tmp_data2,
		tmp_data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro15_tasks.read_4w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	if(tmp_data0!==exp_data0||tmp_data1!==exp_data1||
	   tmp_data2!==exp_data2||tmp_data3!==exp_data3) begin
	  	$display("Error_T2_rsim:CPU read(pro15)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		`ifdef FDISP_PRO15
		$fdisplay(f_pro15_msg,"Error_T2_rsim:CPU read(pro15)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		`endif
	end else begin
	$display("CPU read(pro15):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`ifdef FDISP_PRO15
	$fdisplay(f_pro15_msg,"CPU read(pro15):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`endif
	end
`endif
end
endtask

task	pro15_cpu_read8w;
input	[31:0]	addr;
input	[31:0]	exp_data0,
		exp_data1,
		exp_data2,
		exp_data3,
		exp_data4,
		exp_data5,
		exp_data6,
		exp_data7;
reg	[31:0]	tmp_data0,
		tmp_data1,
		tmp_data2,
		tmp_data3,
		tmp_data4,
		tmp_data5,
		tmp_data6,
		tmp_data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro15_tasks.read_8w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	if(tmp_data0!==exp_data0||tmp_data1!==exp_data1|| tmp_data2!==exp_data2||tmp_data3!==exp_data3 ||
	   tmp_data4!==exp_data4||tmp_data5!==exp_data5|| tmp_data6!==exp_data6||tmp_data7!==exp_data7)
	   begin
	  	$display("Error_T2_rsim:CPU read(pro15)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		$display("\t\t\t\t\t\tRDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		tmp_data4,tmp_data5,tmp_data6,tmp_data7,exp_data4,exp_data5,exp_data6,exp_data7);
		`ifdef FDISP_PRO15
		$fdisplay(f_pro15_msg,"Error_T2_rsim:CPU read(pro15)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		$fdisplay(f_pro15_msg,"\t\t\t\t\t\tRDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		tmp_data4,tmp_data5,tmp_data6,tmp_data7,exp_data4,exp_data5,exp_data6,exp_data7);
		`endif
	   end
	else begin
	$display("CPU read(pro15):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("\t\t\t\t\t\tD0 =%h, D1 =%h, D2 =%h, D3 =%h",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`ifdef FDISP_PRO15
	$fdisplay(f_pro15_msg,"CPU read(pro15):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$fdisplay(f_pro15_msg,"\t\t\t\t\t\tD0 =%h, D1 =%h, D2 =%h, D3 =%h",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`endif
	end
`endif
end
endtask


task	pro16_cpu_write1w;
input	[31:0]	addr;
input	[3:0]	byte;
input	[31:0] 	data;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro16_tasks.write_1w (addr,byte,data);
	$display("CPU write(pro16):\t\tADDR =%h, byte =%h, D =%h",addr,byte,data);
	`ifdef FDISP_PRO16
	$fdisplay(f_pro16_msg,"CPU write(pro16):\t\tADDR =%h, byte =%h, D =%h",addr,byte,data);
	`endif
`endif
end
endtask

task	pro16_cpu_write2w;
input	[31:0]	addr;
input	[31:0] 	data0,data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro16_tasks.write_2w (addr,data0,data1);
	$display("CPU write(pro16):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`ifdef FDISP_PRO16
	$fdisplay(f_pro16_msg,"CPU write(pro16):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`endif
`endif
end
endtask

task	pro16_cpu_write4w;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro16_tasks.write_4w (addr,data0,data1,data2,data3);
	$display("CPU write(pro16):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`ifdef FDISP_PRO16
	$fdisplay(f_pro16_msg,"CPU write(pro16):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`endif
`endif
end
endtask

task	pro16_cpu_write8w;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
input	[31:0] 	data4,data5,data6,data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro16_tasks.write_8w (addr,data0,data1,data2,data3,data4,data5,data6,data7);
	$display("CPU write(pro16):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	$display("\t\t\t\t\t\tD4 =%h, D5 =%h, D6 =%h, D7 =%h",data4,data5,data6,data7);
	`ifdef FDISP_PRO16
	$fdisplay(f_pro16_msg,"CPU write(pro16):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	$fdisplay(f_pro16_msg,"\t\t\t\t\t\tD4 =%h, D5 =%h, D6 =%h, D7 =%h",data4,data5,data6,data7);
	`endif
`endif
end
endtask

task	pro16_cpu_readbk1w;
input	[31:0]	addr;
input	[3:0]	byte;
inout	[31:0]	tmp_data;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro16_tasks.read_1w (addr,byte,tmp_data);
	$display("CPU read(pro16):\t\tADDR =%h, byte =%h, D =%h",addr,byte,tmp_data);
	`ifdef FDISP_PRO16
	$fdisplay(f_pro16_msg,"CPU read(pro16):\t\tADDR =%h, byte =%h, D =%h",addr,byte,tmp_data);
	`endif
`endif
end
endtask

task	pro16_cpu_readbk2w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro16_tasks.read_2w (addr,tmp_data0,tmp_data1);
	$display("CPU read(pro16):\t\tADDR =%h, D0 =%h, D1 =%H",addr,tmp_data0,tmp_data1 );
	`ifdef FDISP_PRO16
	$fdisplay(f_pro16_msg,"CPU read(pro16):\t\tADDR =%h, D0 =%h, D1 =%H",addr,tmp_data0,tmp_data1 );
	`endif
`endif
end
endtask

task	pro16_cpu_readbk4w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1,tmp_data2,tmp_data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro16_tasks.read_4w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("CPU read(pro16):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`ifdef FDISP_PRO16
	$fdisplay(f_pro16_msg,"CPU read(pro16):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`endif
`endif
end
endtask

task	pro16_cpu_readbk8w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1,tmp_data2,tmp_data3;
inout	[31:0]	tmp_data4,tmp_data5,tmp_data6,tmp_data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro16_tasks.read_8w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	$display("CPU read(pro16):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("\t\t\t\t\t\tD4 =%h, D5 =%H, D6 =%h, D7 =%H",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`ifdef FDISP_PRO16
	$fdisplay(f_pro16_msg,"CPU read(pro16):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$fdisplay(f_pro16_msg,"\t\t\t\t\t\tD4 =%h, D5 =%H, D6 =%h, D7 =%H",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`endif
`endif
end
endtask

task	pro16_cpu_read1w;
input	[31:0]	addr;
input	[3:0]	byte;
input	[31:0]	exp_data;

reg	[31:0]	cpu_temp;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro16_tasks.read_1w (addr,byte,cpu_temp);
	cpu_temp = cpu_temp & {{8{byte[3]}},{8{byte[2]}},{8{byte[1]}},{8{byte[0]}}};
	if (cpu_temp !== ({{8{byte[3]}},{8{byte[2]}},{8{byte[1]}},{8{byte[0]}}} & exp_data))begin
	  	$display("Error_T2_rsim:CPU read(pro16)\t%0t ADDR =%h, byte =%h, RDBK--D =%h, EXP--D =%h",
		$realtime,addr,byte,cpu_temp,exp_data);
		`ifdef FDISP_PRO16
		$fdisplay(f_pro16_msg,"Error_T2_rsim:CPU read(pro16)\t%0t ADDR =%h, byte =%h, RDBK--D =%h, EXP--D =%h",
		$realtime,addr,byte,cpu_temp,exp_data);
		`endif
	end else begin
	$display("CPU read(pro16):\t\tADDR =%h, byte =%h, D =%h",addr,byte,cpu_temp);
	`ifdef FDISP_PRO16
	$fdisplay(f_pro16_msg,"CPU read(pro16):\t\tADDR =%h, byte =%h, D =%h",addr,byte,cpu_temp);
	`endif
	end
`endif
end
endtask

task	pro16_cpu_read2w;
input	[31:0]	addr;
input	[31:0]	exp_data0;
input	[31:0]	exp_data1;
reg	[31:0]	tmp_data0,
			tmp_data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro16_tasks.read_2w (addr,tmp_data0,tmp_data1);
	if(tmp_data0!==exp_data0 || tmp_data1!=exp_data1) begin
	  	$display("Error_T2_rsim:CPU read(pro16)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h",
		$realtime,addr,tmp_data0,tmp_data1,exp_data0,exp_data1);
		`ifdef FDISP_PRO16
		$fdisplay(f_pro16_msg,"Error_T2_rsim:CPU read(pro16)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h",
		$realtime,addr,tmp_data0,tmp_data1,exp_data0,exp_data1);
		`endif
	end else begin
	$display("CPU read(pro16):\t\tADDR =%h, D0 =%h, D1 =%h",addr,tmp_data0,tmp_data1);
	`ifdef FDISP_PRO16
	$fdisplay(f_pro16_msg,"CPU read(pro16):\t\tADDR =%h, D0 =%h, D1 =%h",addr,tmp_data0,tmp_data1);
	`endif
	end
`endif
end
endtask

task	pro16_cpu_read4w;
input	[31:0]	addr;
input	[31:0]	exp_data0,
		exp_data1,
		exp_data2,
		exp_data3;
reg	[31:0]	tmp_data0,
		tmp_data1,
		tmp_data2,
		tmp_data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro16_tasks.read_4w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	if(tmp_data0!==exp_data0||tmp_data1!==exp_data1||
	   tmp_data2!==exp_data2||tmp_data3!==exp_data3) begin
	  	$display("Error_T2_rsim:CPU read(pro16)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		`ifdef FDISP_PRO16
		$fdisplay(f_pro16_msg,"Error_T2_rsim:CPU read(pro16)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		`endif
	end else begin
	$display("CPU read(pro16):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`ifdef FDISP_PRO16
	$fdisplay(f_pro16_msg,"CPU read(pro16):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`endif
	end
`endif
end
endtask

task	pro16_cpu_read8w;
input	[31:0]	addr;
input	[31:0]	exp_data0,
		exp_data1,
		exp_data2,
		exp_data3,
		exp_data4,
		exp_data5,
		exp_data6,
		exp_data7;
reg	[31:0]	tmp_data0,
		tmp_data1,
		tmp_data2,
		tmp_data3,
		tmp_data4,
		tmp_data5,
		tmp_data6,
		tmp_data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro16_tasks.read_8w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	if(tmp_data0!==exp_data0||tmp_data1!==exp_data1|| tmp_data2!==exp_data2||tmp_data3!==exp_data3 ||
	   tmp_data4!==exp_data4||tmp_data5!==exp_data5|| tmp_data6!==exp_data6||tmp_data7!==exp_data7)
	   begin
	  	$display("Error_T2_rsim:CPU read(pro16)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		$display("\t\t\t\t\t\tRDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		tmp_data4,tmp_data5,tmp_data6,tmp_data7,exp_data4,exp_data5,exp_data6,exp_data7);
		`ifdef FDISP_PRO16
		$fdisplay(f_pro16_msg,"Error_T2_rsim:CPU read(pro16)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		$fdisplay(f_pro16_msg,"\t\t\t\t\t\tRDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		tmp_data4,tmp_data5,tmp_data6,tmp_data7,exp_data4,exp_data5,exp_data6,exp_data7);
		`endif
	end
	else begin
	$display("CPU read(pro16):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("\t\t\t\t\t\tD0 =%h, D1 =%h, D2 =%h, D3 =%h",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`ifdef FDISP_PRO16
	$fdisplay(f_pro16_msg,"CPU read(pro16):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$fdisplay(f_pro16_msg,"\t\t\t\t\t\tD0 =%h, D1 =%h, D2 =%h, D3 =%h",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`endif
	end
`endif
end
endtask

//=============== the basic task for pro17 processer ========================
task	pro17_cpu_write1w;
input	[31:0]	addr;
input	[3:0]	byte;
input	[31:0] 	data;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro17_tasks.write_1w (addr,byte,data);
	$display("CPU write(pro17):\t\tADDR =%h, byte =%h, D =%h",addr,byte,data);
	`ifdef FDISP_PRO17
	$fdisplay(f_pro17_msg,"CPU write(pro17):\t\tADDR =%h, byte =%h, D =%h",addr,byte,data);
	`endif
`endif
end
endtask

task	pro17_cpu_write2w;
input	[31:0]	addr;
input	[31:0] 	data0,data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro17_tasks.write_2w (addr,data0,data1);
	$display("CPU write(pro17):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`ifdef FDISP_PRO17
	$fdisplay(f_pro17_msg,"CPU write(pro17):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`endif
`endif
end
endtask

task	pro17_cpu_write4w;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro17_tasks.write_4w (addr,data0,data1,data2,data3);
	$display("CPU write(pro17):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`ifdef FDISP_PRO17
	$fdisplay(f_pro17_msg,"CPU write(pro17):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`endif
`endif
end
endtask

task	pro17_cpu_write8w;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
input	[31:0] 	data4,data5,data6,data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro17_tasks.write_8w (addr,data0,data1,data2,data3,data4,data5,data6,data7);
	$display("CPU write(pro17):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	$display("\t\t\t\t\t\tD4 =%h, D5 =%h, D6 =%h, D7 =%h",data4,data5,data6,data7);
	`ifdef FDISP_PRO17
	$fdisplay(f_pro17_msg,"CPU write(pro17):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	$fdisplay(f_pro17_msg,"\t\t\t\t\t\tD4 =%h, D5 =%h, D6 =%h, D7 =%h",data4,data5,data6,data7);
	`endif
`endif
end
endtask

task	pro17_cpu_readbk1w;
input	[31:0]	addr;
input	[3:0]	byte;
inout	[31:0]	tmp_data;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro17_tasks.read_1w (addr,byte,tmp_data);
	$display("CPU read(pro17):\t\tADDR =%h, byte =%h, D =%h",addr,byte,tmp_data);
	`ifdef FDISP_PRO17
	$fdisplay(f_pro17_msg,"CPU read(pro17):\t\tADDR =%h, byte =%h, D =%h",addr,byte,tmp_data);
	`endif
`endif
end
endtask

task	pro17_cpu_readbk2w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro17_tasks.read_2w (addr,tmp_data0,tmp_data1);
	$display("CPU read(pro17):\t\tADDR =%h, D0 =%h, D1 =%H",addr,tmp_data0,tmp_data1 );
	`ifdef FDISP_PRO17
	$fdisplay(f_pro17_msg,"CPU read(pro17):\t\tADDR =%h, D0 =%h, D1 =%H",addr,tmp_data0,tmp_data1 );
	`endif
`endif
end
endtask

task	pro17_cpu_readbk4w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1,tmp_data2,tmp_data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro17_tasks.read_4w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("CPU read(pro17):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`ifdef FDISP_PRO17
	$fdisplay(f_pro17_msg,"CPU read(pro17):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`endif
`endif
end
endtask

task	pro17_cpu_readbk8w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1,tmp_data2,tmp_data3;
inout	[31:0]	tmp_data4,tmp_data5,tmp_data6,tmp_data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro17_tasks.read_8w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	$display("CPU read(pro17):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("\t\t\t\t\t\tD4 =%h, D5 =%H, D6 =%h, D7 =%H",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`ifdef FDISP_PRO17
	$fdisplay(f_pro17_msg,"CPU read(pro17):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$fdisplay(f_pro17_msg,"\t\t\t\t\t\tD4 =%h, D5 =%H, D6 =%h, D7 =%H",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`endif
`endif
end
endtask

task	pro17_cpu_read1w;
input	[31:0]	addr;
input	[3:0]	byte;
input	[31:0]	exp_data;

reg	[31:0]	cpu_temp;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro17_tasks.read_1w (addr,byte,cpu_temp);
	cpu_temp = cpu_temp & {{8{byte[3]}},{8{byte[2]}},{8{byte[1]}},{8{byte[0]}}};
	if (cpu_temp !== ({{8{byte[3]}},{8{byte[2]}},{8{byte[1]}},{8{byte[0]}}} & exp_data)) begin
	  	$display("Error_T2_rsim:CPU read(pro17)\t%0t ADDR =%h, byte =%h, RDBK--D =%h, EXP--D =%h",
		$realtime,addr,byte,cpu_temp,exp_data);
		`ifdef FDISP_PRO17
		$fdisplay(f_pro17_msg,"Error_T2_rsim:CPU read(pro17)\t%0t ADDR =%h, byte =%h, RDBK--D =%h, EXP--D =%h",
		$realtime,addr,byte,cpu_temp,exp_data);
		`endif
	end else begin
	$display("CPU read(pro17):\t\tADDR =%h, byte =%h, D =%h",addr,byte,cpu_temp);
	`ifdef FDISP_PRO17
	$fdisplay(f_pro17_msg,"CPU read(pro17):\t\tADDR =%h, byte =%h, D =%h",addr,byte,cpu_temp);
	`endif
	end
`endif
end
endtask

task	pro17_cpu_read2w;
input	[31:0]	addr;
input	[31:0]	exp_data0;
input	[31:0]	exp_data1;
reg	[31:0]	tmp_data0,
			tmp_data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro17_tasks.read_2w (addr,tmp_data0,tmp_data1);
	if(tmp_data0!==exp_data0 || tmp_data1!=exp_data1) begin
	  	$display("Error_T2_rsim:CPU read(pro17)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h",
		$realtime,addr,tmp_data0,tmp_data1,exp_data0,exp_data1);
		`ifdef FDISP_PRO17
		$fdisplay(f_pro17_msg,"Error_T2_rsim:CPU read(pro17)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h",
		$realtime,addr,tmp_data0,tmp_data1,exp_data0,exp_data1);
		`endif
	end else begin
	$display("CPU read(pro17):\t\tADDR =%h, D0 =%h, D1 =%h",addr,tmp_data0,tmp_data1);
	`ifdef FDISP_PRO17
	$fdisplay(f_pro17_msg,"CPU read(pro17):\t\tADDR =%h, D0 =%h, D1 =%h",addr,tmp_data0,tmp_data1);
	`endif
	end
`endif
end
endtask

task	pro17_cpu_read4w;
input	[31:0]	addr;
input	[31:0]	exp_data0,
		exp_data1,
		exp_data2,
		exp_data3;
reg	[31:0]	tmp_data0,
		tmp_data1,
		tmp_data2,
		tmp_data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro17_tasks.read_4w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	if(tmp_data0!==exp_data0||tmp_data1!==exp_data1||
	   tmp_data2!==exp_data2||tmp_data3!==exp_data3) begin
	  	$display("Error_T2_rsim:CPU read(pro17)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		`ifdef FDISP_PRO17
		$fdisplay(f_pro17_msg,"Error_T2_rsim:CPU read(pro17)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		`endif
	end else begin
	$display("CPU read(pro17):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`ifdef FDISP_PRO17
	$fdisplay(f_pro17_msg,"CPU read(pro17):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`endif
	end
`endif
end
endtask

task	pro17_cpu_read8w;
input	[31:0]	addr;
input	[31:0]	exp_data0,
		exp_data1,
		exp_data2,
		exp_data3,
		exp_data4,
		exp_data5,
		exp_data6,
		exp_data7;
reg	[31:0]	tmp_data0,
		tmp_data1,
		tmp_data2,
		tmp_data3,
		tmp_data4,
		tmp_data5,
		tmp_data6,
		tmp_data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro17_tasks.read_8w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	if(tmp_data0!==exp_data0||tmp_data1!==exp_data1|| tmp_data2!==exp_data2||tmp_data3!==exp_data3 ||
	   tmp_data4!==exp_data4||tmp_data5!==exp_data5|| tmp_data6!==exp_data6||tmp_data7!==exp_data7)
	   begin
	  	$display("Error_T2_rsim:CPU read(pro17)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		$display("\t\t\t\t\t\tRDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		tmp_data4,tmp_data5,tmp_data6,tmp_data7,exp_data4,exp_data5,exp_data6,exp_data7);
		`ifdef FDISP_PRO17
		$fdisplay(f_pro17_msg,"Error_T2_rsim:CPU read(pro17)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		$fdisplay(f_pro17_msg,"\t\t\t\t\t\tRDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		tmp_data4,tmp_data5,tmp_data6,tmp_data7,exp_data4,exp_data5,exp_data6,exp_data7);
		`endif
	   end
	else begin
	$display("CPU read(pro17):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("\t\t\t\t\t\tD0 =%h, D1 =%h, D2 =%h, D3 =%h",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`ifdef FDISP_PRO17
	$fdisplay(f_pro17_msg,"CPU read(pro17):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$fdisplay(f_pro17_msg,"\t\t\t\t\t\tD0 =%h, D1 =%h, D2 =%h, D3 =%h",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`endif
	end
`endif
end
endtask

//=============== the basic task for pro18 processer ========================
task	pro18_cpu_write1w;
input	[31:0]	addr;
input	[3:0]	byte;
input	[31:0] 	data;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro18_tasks.write_1w (addr,byte,data);
	$display("CPU write(pro18):\t\tADDR =%h, byte =%h, D =%h",addr,byte,data);
	`ifdef FDISP_PRO18
	$fdisplay(f_pro18_msg,"CPU write(pro18):\t\tADDR =%h, byte =%h, D =%h",addr,byte,data);
	`endif
`endif
end
endtask

task	pro18_cpu_write2w;
input	[31:0]	addr;
input	[31:0] 	data0,data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro18_tasks.write_2w (addr,data0,data1);
	$display("CPU write(pro18):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`ifdef FDISP_PRO18
	$fdisplay(f_pro18_msg,"CPU write(pro18):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`endif
`endif
end
endtask

task	pro18_cpu_write4w;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro18_tasks.write_4w (addr,data0,data1,data2,data3);
	$display("CPU write(pro18):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`ifdef FDISP_PRO18
	$fdisplay(f_pro18_msg,"CPU write(pro18):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`endif
`endif
end
endtask

task	pro18_cpu_write8w;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
input	[31:0] 	data4,data5,data6,data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro18_tasks.write_8w (addr,data0,data1,data2,data3,data4,data5,data6,data7);
	$display("CPU write(pro18):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	$display("\t\t\t\t\t\tD4 =%h, D5 =%h, D6 =%h, D7 =%h",data4,data5,data6,data7);
	`ifdef FDISP_PRO18
	$fdisplay(f_pro18_msg,"CPU write(pro18):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	$fdisplay(f_pro18_msg,"\t\t\t\t\t\tD4 =%h, D5 =%h, D6 =%h, D7 =%h",data4,data5,data6,data7);
	`endif
`endif
end
endtask

task	pro18_cpu_readbk1w;
input	[31:0]	addr;
input	[3:0]	byte;
inout	[31:0]	tmp_data;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro18_tasks.read_1w (addr,byte,tmp_data);
	$display("CPU read(pro18):\t\tADDR =%h, byte =%h, D =%h",addr,byte,tmp_data);
	`ifdef FDISP_PRO18
	$fdisplay(f_pro18_msg,"CPU read(pro18):\t\tADDR =%h, byte =%h, D =%h",addr,byte,tmp_data);
	`endif
`endif
end
endtask

task	pro18_cpu_readbk2w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro18_tasks.read_2w (addr,tmp_data0,tmp_data1);
	$display("CPU read(pro18):\t\tADDR =%h, D0 =%h, D1 =%H",addr,tmp_data0,tmp_data1 );
	`ifdef FDISP_PRO18
	$fdisplay(f_pro18_msg,"CPU read(pro18):\t\tADDR =%h, D0 =%h, D1 =%H",addr,tmp_data0,tmp_data1 );
	`endif
`endif
end
endtask

task	pro18_cpu_readbk4w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1,tmp_data2,tmp_data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro18_tasks.read_4w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("CPU read(pro18):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`ifdef FDISP_PRO18
	$fdisplay(f_pro18_msg,"CPU read(pro18):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`endif
`endif
end
endtask

task	pro18_cpu_readbk8w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1,tmp_data2,tmp_data3;
inout	[31:0]	tmp_data4,tmp_data5,tmp_data6,tmp_data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro18_tasks.read_8w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	$display("CPU read(pro18):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("\t\t\t\t\t\tD4 =%h, D5 =%H, D6 =%h, D7 =%H",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`ifdef FDISP_PRO18
	$fdisplay(f_pro18_msg,"CPU read(pro18):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$fdisplay(f_pro18_msg,"\t\t\t\t\t\tD4 =%h, D5 =%H, D6 =%h, D7 =%H",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`endif
`endif
end
endtask

task	pro18_cpu_read1w;
input	[31:0]	addr;
input	[3:0]	byte;
input	[31:0]	exp_data;

reg	[31:0]	cpu_temp;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro18_tasks.read_1w (addr,byte,cpu_temp);
	cpu_temp = cpu_temp & {{8{byte[3]}},{8{byte[2]}},{8{byte[1]}},{8{byte[0]}}};
	if (cpu_temp !== ({{8{byte[3]}},{8{byte[2]}},{8{byte[1]}},{8{byte[0]}}} & exp_data)) begin
	  	$display("Error_T2_rsim:CPU read(pro18)\t%0t ADDR =%h, byte =%h, RDBK--D =%h, EXP--D =%h",
		$realtime,addr,byte,cpu_temp,exp_data);
		`ifdef FDISP_PRO18
		$fdisplay(f_pro18_msg,"Error_T2_rsim:CPU read(pro18)\t%0t ADDR =%h, byte =%h, RDBK--D =%h, EXP--D =%h",
		$realtime,addr,byte,cpu_temp,exp_data);
		`endif
	end else begin
	$display("CPU read(pro18):\t\tADDR =%h, byte =%h, D =%h",addr,byte,cpu_temp);
	`ifdef FDISP_PRO18
	$fdisplay(f_pro18_msg,"CPU read(pro18):\t\tADDR =%h, byte =%h, D =%h",addr,byte,cpu_temp);
	`endif
	end
`endif
end
endtask

task	pro18_cpu_read2w;
input	[31:0]	addr;
input	[31:0]	exp_data0;
input	[31:0]	exp_data1;
reg	[31:0]	tmp_data0,
			tmp_data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro18_tasks.read_2w (addr,tmp_data0,tmp_data1);
	if(tmp_data0!==exp_data0 || tmp_data1!=exp_data1) begin
	  	$display("Error_T2_rsim:CPU read(pro18)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h",
		$realtime,addr,tmp_data0,tmp_data1,exp_data0,exp_data1);
		`ifdef FDISP_PRO18
		$fdisplay(f_pro18_msg,"Error_T2_rsim:CPU read(pro18)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h",
		$realtime,addr,tmp_data0,tmp_data1,exp_data0,exp_data1);
		`endif
	end else begin
	$display("CPU read(pro18):\t\tADDR =%h, D0 =%h, D1 =%h",addr,tmp_data0,tmp_data1);
	`ifdef FDISP_PRO18
	$fdisplay(f_pro18_msg,"CPU read(pro18):\t\tADDR =%h, D0 =%h, D1 =%h",addr,tmp_data0,tmp_data1);
	`endif
	end
`endif
end
endtask

task	pro18_cpu_read4w;
input	[31:0]	addr;
input	[31:0]	exp_data0,
		exp_data1,
		exp_data2,
		exp_data3;
reg	[31:0]	tmp_data0,
		tmp_data1,
		tmp_data2,
		tmp_data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro18_tasks.read_4w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	if(tmp_data0!==exp_data0||tmp_data1!==exp_data1||
	   tmp_data2!==exp_data2||tmp_data3!==exp_data3) begin
	  	$display("Error_T2_rsim:CPU read(pro18)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		`ifdef FDISP_PRO18
		$fdisplay(f_pro18_msg,"Error_T2_rsim:CPU read(pro18)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		`endif
	end else begin
	$display("CPU read(pro18):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`ifdef FDISP_PRO18
	$fdisplay(f_pro18_msg,"CPU read(pro18):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`endif
	end
`endif
end
endtask

task	pro18_cpu_read8w;
input	[31:0]	addr;
input	[31:0]	exp_data0,
		exp_data1,
		exp_data2,
		exp_data3,
		exp_data4,
		exp_data5,
		exp_data6,
		exp_data7;
reg	[31:0]	tmp_data0,
		tmp_data1,
		tmp_data2,
		tmp_data3,
		tmp_data4,
		tmp_data5,
		tmp_data6,
		tmp_data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro18_tasks.read_8w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	if(tmp_data0!==exp_data0||tmp_data1!==exp_data1|| tmp_data2!==exp_data2||tmp_data3!==exp_data3 ||
	   tmp_data4!==exp_data4||tmp_data5!==exp_data5|| tmp_data6!==exp_data6||tmp_data7!==exp_data7)
	   begin
	  	$display("Error_T2_rsim:CPU read(pro18)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		$display("\t\t\t\t\t\tRDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		tmp_data4,tmp_data5,tmp_data6,tmp_data7,exp_data4,exp_data5,exp_data6,exp_data7);
		`ifdef FDISP_PRO18
		$fdisplay(f_pro18_msg,"Error_T2_rsim:CPU read(pro18)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		$fdisplay(f_pro18_msg,"\t\t\t\t\t\tRDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		tmp_data4,tmp_data5,tmp_data6,tmp_data7,exp_data4,exp_data5,exp_data6,exp_data7);
		`endif
	   end
	else begin
	$display("CPU read(pro18):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("\t\t\t\t\t\tD0 =%h, D1 =%h, D2 =%h, D3 =%h",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`ifdef FDISP_PRO18
	$fdisplay(f_pro18_msg,"CPU read(pro18):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$fdisplay(f_pro18_msg,"\t\t\t\t\t\tD0 =%h, D1 =%h, D2 =%h, D3 =%h",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`endif
	end
`endif
end
endtask

//=============== the basic task for pro19 processer ========================
task	pro19_cpu_write1w;
input	[31:0]	addr;
input	[3:0]	byte;
input	[31:0] 	data;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro19_tasks.write_1w (addr,byte,data);
	$display("CPU write(pro19):\t\tADDR =%h, byte =%h, D =%h",addr,byte,data);
	`ifdef FDISP_PRO19
	$fdisplay(f_pro19_msg,"CPU write(pro19):\t\tADDR =%h, byte =%h, D =%h",addr,byte,data);
	`endif
`endif
end
endtask

task	pro19_cpu_write2w;
input	[31:0]	addr;
input	[31:0] 	data0,data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro19_tasks.write_2w (addr,data0,data1);
	$display("CPU write(pro19):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`ifdef FDISP_PRO19
	$fdisplay(f_pro19_msg,"CPU write(pro19):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`endif
`endif
end
endtask

task	pro19_cpu_write4w;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro19_tasks.write_4w (addr,data0,data1,data2,data3);
	$display("CPU write(pro19):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`ifdef FDISP_PRO19
	$fdisplay(f_pro19_msg,"CPU write(pro19):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`endif
`endif
end
endtask

task	pro19_cpu_write8w;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
input	[31:0] 	data4,data5,data6,data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro19_tasks.write_8w (addr,data0,data1,data2,data3,data4,data5,data6,data7);
	$display("CPU write(pro19):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	$display("\t\t\t\t\t\tD4 =%h, D5 =%h, D6 =%h, D7 =%h",data4,data5,data6,data7);
	`ifdef FDISP_PRO19
	$fdisplay(f_pro19_msg,"CPU write(pro19):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	$fdisplay(f_pro19_msg,"\t\t\t\t\t\tD4 =%h, D5 =%h, D6 =%h, D7 =%h",data4,data5,data6,data7);
	`endif
`endif
end
endtask

task	pro19_cpu_readbk1w;
input	[31:0]	addr;
input	[3:0]	byte;
inout	[31:0]	tmp_data;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro19_tasks.read_1w (addr,byte,tmp_data);
	$display("CPU read(pro19):\t\tADDR =%h, byte =%h, D =%h",addr,byte,tmp_data);
	`ifdef FDISP_PRO19
	$fdisplay(f_pro19_msg,"CPU read(pro19):\t\tADDR =%h, byte =%h, D =%h",addr,byte,tmp_data);
	`endif
`endif
end
endtask

task	pro19_cpu_readbk2w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro19_tasks.read_2w (addr,tmp_data0,tmp_data1);
	$display("CPU read(pro19):\t\tADDR =%h, D0 =%h, D1 =%H",addr,tmp_data0,tmp_data1 );
	`ifdef FDISP_PRO19
	$fdisplay(f_pro19_msg,"CPU read(pro19):\t\tADDR =%h, D0 =%h, D1 =%H",addr,tmp_data0,tmp_data1 );
	`endif
`endif
end
endtask

task	pro19_cpu_readbk4w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1,tmp_data2,tmp_data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro19_tasks.read_4w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("CPU read(pro19):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`ifdef FDISP_PRO19
	$fdisplay(f_pro19_msg,"CPU read(pro19):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`endif
`endif
end
endtask

task	pro19_cpu_readbk8w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1,tmp_data2,tmp_data3;
inout	[31:0]	tmp_data4,tmp_data5,tmp_data6,tmp_data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro19_tasks.read_8w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	$display("CPU read(pro19):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("\t\t\t\t\t\tD4 =%h, D5 =%H, D6 =%h, D7 =%H",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`ifdef FDISP_PRO19
	$fdisplay(f_pro19_msg,"CPU read(pro19):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$fdisplay(f_pro19_msg,"\t\t\t\t\t\tD4 =%h, D5 =%H, D6 =%h, D7 =%H",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`endif
`endif
end
endtask

task	pro19_cpu_read1w;
input	[31:0]	addr;
input	[3:0]	byte;
input	[31:0]	exp_data;

reg	[31:0]	cpu_temp;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro19_tasks.read_1w (addr,byte,cpu_temp);
	cpu_temp = cpu_temp & {{8{byte[3]}},{8{byte[2]}},{8{byte[1]}},{8{byte[0]}}};
	if (cpu_temp !== ({{8{byte[3]}},{8{byte[2]}},{8{byte[1]}},{8{byte[0]}}} & exp_data)) begin
	  	$display("Error_T2_rsim:CPU read(pro19)\t%0t ADDR =%h, byte =%h, RDBK--D =%h, EXP--D =%h",
		$realtime,addr,byte,cpu_temp,exp_data);
		`ifdef FDISP_PRO19
		$fdisplay(f_pro19_msg,"Error_T2_rsim:CPU read(pro19)\t%0t ADDR =%h, byte =%h, RDBK--D =%h, EXP--D =%h",
		$realtime,addr,byte,cpu_temp,exp_data);
		`endif
	end else begin
	$display("CPU read(pro19):\t\tADDR =%h, byte =%h, D =%h",addr,byte,cpu_temp);
	`ifdef FDISP_PRO19
	$fdisplay(f_pro19_msg,"CPU read(pro19):\t\tADDR =%h, byte =%h, D =%h",addr,byte,cpu_temp);
	`endif
	end
`endif
end
endtask

task	pro19_cpu_read2w;
input	[31:0]	addr;
input	[31:0]	exp_data0;
input	[31:0]	exp_data1;
reg	[31:0]	tmp_data0,
			tmp_data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro19_tasks.read_2w (addr,tmp_data0,tmp_data1);
	if(tmp_data0!==exp_data0 || tmp_data1!=exp_data1) begin
	  	$display("Error_T2_rsim:CPU read(pro19)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h",
		$realtime,addr,tmp_data0,tmp_data1,exp_data0,exp_data1);
		`ifdef FDISP_PRO19
		$fdisplay(f_pro19_msg,"Error_T2_rsim:CPU read(pro19)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h",
		$realtime,addr,tmp_data0,tmp_data1,exp_data0,exp_data1);
		`endif
	end else begin
	$display("CPU read(pro19):\t\tADDR =%h, D0 =%h, D1 =%h",addr,tmp_data0,tmp_data1);
	`ifdef FDISP_PRO19
	$fdisplay(f_pro19_msg,"CPU read(pro19):\t\tADDR =%h, D0 =%h, D1 =%h",addr,tmp_data0,tmp_data1);
	`endif
	end
`endif
end
endtask

task	pro19_cpu_read4w;
input	[31:0]	addr;
input	[31:0]	exp_data0,
		exp_data1,
		exp_data2,
		exp_data3;
reg	[31:0]	tmp_data0,
		tmp_data1,
		tmp_data2,
		tmp_data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro19_tasks.read_4w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	if(tmp_data0!==exp_data0||tmp_data1!==exp_data1||
	   tmp_data2!==exp_data2||tmp_data3!==exp_data3) begin
	  	$display("Error_T2_rsim:CPU read(pro19)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		`ifdef FDISP_PRO19
		$fdisplay(f_pro19_msg,"Error_T2_rsim:CPU read(pro19)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		`endif
	end else begin
	$display("CPU read(pro19):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`ifdef FDISP_PRO19
	$fdisplay(f_pro19_msg,"CPU read(pro19):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`endif
	end
`endif
end
endtask

task	pro19_cpu_read8w;
input	[31:0]	addr;
input	[31:0]	exp_data0,
		exp_data1,
		exp_data2,
		exp_data3,
		exp_data4,
		exp_data5,
		exp_data6,
		exp_data7;
reg	[31:0]	tmp_data0,
		tmp_data1,
		tmp_data2,
		tmp_data3,
		tmp_data4,
		tmp_data5,
		tmp_data6,
		tmp_data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro19_tasks.read_8w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	if(tmp_data0!==exp_data0||tmp_data1!==exp_data1|| tmp_data2!==exp_data2||tmp_data3!==exp_data3 ||
	   tmp_data4!==exp_data4||tmp_data5!==exp_data5|| tmp_data6!==exp_data6||tmp_data7!==exp_data7)
	   begin
	  	$display("Error_T2_rsim:CPU read(pro19)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		$display("\t\t\t\t\t\tRDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		tmp_data4,tmp_data5,tmp_data6,tmp_data7,exp_data4,exp_data5,exp_data6,exp_data7);
		`ifdef FDISP_PRO19
		$fdisplay(f_pro19_msg,"Error_T2_rsim:CPU read(pro19)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		$fdisplay(f_pro19_msg,"\t\t\t\t\t\tRDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		tmp_data4,tmp_data5,tmp_data6,tmp_data7,exp_data4,exp_data5,exp_data6,exp_data7);
		`endif
	   end
	else begin
	$display("CPU read(pro19):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("\t\t\t\t\t\tD0 =%h, D1 =%h, D2 =%h, D3 =%h",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`ifdef FDISP_PRO19
	$fdisplay(f_pro19_msg,"CPU read(pro19):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$fdisplay(f_pro19_msg,"\t\t\t\t\t\tD0 =%h, D1 =%h, D2 =%h, D3 =%h",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`endif
	end
`endif
end
endtask

//=============== the basic task for pro20 processer ========================
task	pro20_cpu_write1w;
input	[31:0]	addr;
input	[3:0]	byte;
input	[31:0] 	data;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro20_tasks.write_1w (addr,byte,data);
	$display("CPU write(pro20):\t\tADDR =%h, byte =%h, D =%h",addr,byte,data);
	`ifdef FDISP_PRO20
	$fdisplay(f_pro20_msg,"CPU write(pro20):\t\tADDR =%h, byte =%h, D =%h",addr,byte,data);
	`endif
`endif
end
endtask

task	pro20_cpu_write2w;
input	[31:0]	addr;
input	[31:0] 	data0,data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro20_tasks.write_2w (addr,data0,data1);
	$display("CPU write(pro20):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`ifdef FDISP_PRO20
	$fdisplay(f_pro20_msg,"CPU write(pro20):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`endif
`endif
end
endtask

task	pro20_cpu_write4w;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro20_tasks.write_4w (addr,data0,data1,data2,data3);
	$display("CPU write(pro20):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`ifdef FDISP_PRO20
	$fdisplay(f_pro20_msg,"CPU write(pro20):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`endif
`endif
end
endtask

task	pro20_cpu_write8w;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
input	[31:0] 	data4,data5,data6,data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro20_tasks.write_8w (addr,data0,data1,data2,data3,data4,data5,data6,data7);
	$display("CPU write(pro20):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	$display("\t\t\t\t\t\tD4 =%h, D5 =%h, D6 =%h, D7 =%h",data4,data5,data6,data7);
	`ifdef FDISP_PRO20
	$fdisplay(f_pro20_msg,"CPU write(pro20):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	$fdisplay(f_pro20_msg,"\t\t\t\t\t\tD4 =%h, D5 =%h, D6 =%h, D7 =%h",data4,data5,data6,data7);
	`endif
`endif
end
endtask

task	pro20_cpu_readbk1w;
input	[31:0]	addr;
input	[3:0]	byte;
inout	[31:0]	tmp_data;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro20_tasks.read_1w (addr,byte,tmp_data);
	$display("CPU read(pro20):\t\tADDR =%h, byte =%h, D =%h",addr,byte,tmp_data);
	`ifdef FDISP_PRO20
	$fdisplay(f_pro20_msg,"CPU read(pro20):\t\tADDR =%h, byte =%h, D =%h",addr,byte,tmp_data);
	`endif
`else
	tmp_data = 32'h0;
`endif
end
endtask

task	pro20_cpu_readbk2w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro20_tasks.read_2w (addr,tmp_data0,tmp_data1);
	$display("CPU read(pro20):\t\tADDR =%h, D0 =%h, D1 =%H",addr,tmp_data0,tmp_data1 );
	`ifdef FDISP_PRO20
	$fdisplay(f_pro20_msg,"CPU read(pro20):\t\tADDR =%h, D0 =%h, D1 =%H",addr,tmp_data0,tmp_data1 );
	`endif
`endif
end
endtask

task	pro20_cpu_readbk4w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1,tmp_data2,tmp_data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro20_tasks.read_4w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("CPU read(pro20):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`ifdef FDISP_PRO20
	$fdisplay(f_pro20_msg,"CPU read(pro20):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`endif
`endif
end
endtask

task	pro20_cpu_readbk8w;
input	[31:0]	addr;
inout	[31:0]	tmp_data0,tmp_data1,tmp_data2,tmp_data3;
inout	[31:0]	tmp_data4,tmp_data5,tmp_data6,tmp_data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro20_tasks.read_8w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	$display("CPU read(pro20):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("\t\t\t\t\t\tD4 =%h, D5 =%H, D6 =%h, D7 =%H",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`ifdef FDISP_PRO20
	$fdisplay(f_pro20_msg,"CPU read(pro20):\t\tADDR =%h, D0 =%h, D1 =%H, D2 =%h, D3 =%H",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$fdisplay(f_pro20_msg,"\t\t\t\t\t\tD4 =%h, D5 =%H, D6 =%h, D7 =%H",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`endif
`endif
end
endtask

task	pro20_cpu_read1w;
input	[31:0]	addr;
input	[3:0]	byte;
input	[31:0]	exp_data;

reg	[31:0]	cpu_temp;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro20_tasks.read_1w (addr,byte,cpu_temp);
	cpu_temp = cpu_temp & {{8{byte[3]}},{8{byte[2]}},{8{byte[1]}},{8{byte[0]}}};
	if (cpu_temp !== ({{8{byte[3]}},{8{byte[2]}},{8{byte[1]}},{8{byte[0]}}} & exp_data)) begin
	  	$display("Error_T2_rsim:CPU read(pro20)\t%0t ADDR =%h, byte =%h, RDBK--D =%h, EXP--D =%h",
		$realtime,addr,byte,cpu_temp,exp_data);
		`ifdef FDISP_PRO20
		$fdisplay(f_pro20_msg,"Error_T2_rsim:CPU read(pro20)\t%0t ADDR =%h, byte =%h, RDBK--D =%h, EXP--D =%h",
		$realtime,addr,byte,cpu_temp,exp_data);
		`endif
	end else begin
	$display("CPU read(pro20):\t\tADDR =%h, byte =%h, D =%h",addr,byte,cpu_temp);
	`ifdef FDISP_PRO20
	$fdisplay(f_pro20_msg,"CPU read(pro20):\t\tADDR =%h, byte =%h, D =%h",addr,byte,cpu_temp);
	`endif
	end
`endif
end
endtask

task	pro20_cpu_read2w;
input	[31:0]	addr;
input	[31:0]	exp_data0;
input	[31:0]	exp_data1;
reg	[31:0]	tmp_data0,
			tmp_data1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro20_tasks.read_2w (addr,tmp_data0,tmp_data1);
	if(tmp_data0!==exp_data0 || tmp_data1!==exp_data1) begin
	  	$display("Error_T2_rsim:CPU read(pro20)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h",
		$realtime,addr,tmp_data0,tmp_data1,exp_data0,exp_data1);
		`ifdef FDISP_PRO20
		$fdisplay(f_pro20_msg,"Error_T2_rsim:CPU read(pro20)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h",
		$realtime,addr,tmp_data0,tmp_data1,exp_data0,exp_data1);
		`endif
	end else begin
	$display("CPU read(pro20):\t\tADDR =%h, D0 =%h, D1 =%h",addr,tmp_data0,tmp_data1);
	`ifdef FDISP_PRO20
	$fdisplay(f_pro20_msg,"CPU read(pro20):\t\tADDR =%h, D0 =%h, D1 =%h",addr,tmp_data0,tmp_data1);
	`endif
	end
`endif
end
endtask

task	pro20_cpu_read4w;
input	[31:0]	addr;
input	[31:0]	exp_data0,
		exp_data1,
		exp_data2,
		exp_data3;
reg	[31:0]	tmp_data0,
		tmp_data1,
		tmp_data2,
		tmp_data3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro20_tasks.read_4w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	if(tmp_data0!==exp_data0||tmp_data1!==exp_data1||
	   tmp_data2!==exp_data2||tmp_data3!==exp_data3) begin
	  	$display("Error_T2_rsim:CPU read(pro20)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		`ifdef FDISP_PRO20
		$fdisplay(f_pro20_msg,"Error_T2_rsim:CPU read(pro20)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		`endif
	end else begin
	$display("CPU read(pro20):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`ifdef FDISP_PRO20
	$fdisplay(f_pro20_msg,"CPU read(pro20):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	`endif
	end
`endif
end
endtask

task	pro20_cpu_read8w;
input	[31:0]	addr;
input	[31:0]	exp_data0,
		exp_data1,
		exp_data2,
		exp_data3,
		exp_data4,
		exp_data5,
		exp_data6,
		exp_data7;
reg	[31:0]	tmp_data0,
		tmp_data1,
		tmp_data2,
		tmp_data3,
		tmp_data4,
		tmp_data5,
		tmp_data6,
		tmp_data7;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro20_tasks.read_8w (addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	if(tmp_data0!==exp_data0||tmp_data1!==exp_data1|| tmp_data2!==exp_data2||tmp_data3!==exp_data3 ||
	   tmp_data4!==exp_data4||tmp_data5!==exp_data5|| tmp_data6!==exp_data6||tmp_data7!==exp_data7)
	   begin
	  	$display("Error_T2_rsim:CPU read(pro20)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		$display("\t\t\t\t\t\tRDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		tmp_data4,tmp_data5,tmp_data6,tmp_data7,exp_data4,exp_data5,exp_data6,exp_data7);
		`ifdef FDISP_PRO20
		$fdisplay(f_pro20_msg,"Error_T2_rsim:CPU read(pro20)\t%0t ADDR =%h, RDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		$realtime,addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3,exp_data0,exp_data1,exp_data2,exp_data3);
		$fdisplay(f_pro20_msg,"\t\t\t\t\t\tRDBK--D0 =%h, D1 =%h, D2 =%h, D3 =%h,\n\t\t\t\t\t\tEXP---D0 =%h, D1 =%h, D2 =%h, D3 =%h",
		tmp_data4,tmp_data5,tmp_data6,tmp_data7,exp_data4,exp_data5,exp_data6,exp_data7);
		`endif
	   end
	else begin
	$display("CPU read(pro20):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$display("\t\t\t\t\t\tD0 =%h, D1 =%h, D2 =%h, D3 =%h",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`ifdef FDISP_PRO20
	$fdisplay(f_pro20_msg,"CPU read(pro20):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,tmp_data0,tmp_data1,tmp_data2,tmp_data3);
	$fdisplay(f_pro20_msg,"\t\t\t\t\t\tD0 =%h, D1 =%h, D2 =%h, D3 =%h",tmp_data4,tmp_data5,tmp_data6,tmp_data7);
	`endif
	end
`endif
end
endtask



//============== request lock task and unlock tasks ===============
task pro00_req_lock;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro00_tasks.request_lock;
`endif
end
endtask

task pro00_req_unlock;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro00_tasks.request_unlock;
`endif
end
endtask

task pro01_req_lock;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro01_tasks.request_lock;
`endif
end
endtask

task pro01_req_unlock;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro01_tasks.request_unlock;
`endif
end
endtask

task pro02_req_lock;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro02_tasks.request_lock;
`endif
end
endtask

task pro02_req_unlock;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro02_tasks.request_unlock;
`endif
end
endtask

task pro03_req_lock;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro03_tasks.request_lock;
`endif
end
endtask

task pro03_req_unlock;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro03_tasks.request_unlock;
`endif
end
endtask

task pro04_req_lock;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro04_tasks.request_lock;
`endif
end
endtask

task pro04_req_unlock;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro04_tasks.request_unlock;
`endif
end
endtask

task pro05_req_lock;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro05_tasks.request_lock;
`endif
end
endtask

task pro05_req_unlock;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro05_tasks.request_unlock;
`endif
end
endtask

task pro06_req_lock;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro06_tasks.request_lock;
`endif
end
endtask

task pro06_req_unlock;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro06_tasks.request_unlock;
`endif
end
endtask

task pro07_req_lock;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro07_tasks.request_lock;
`endif
end
endtask

task pro07_req_unlock;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro07_tasks.request_unlock;
`endif
end
endtask

task pro08_req_lock;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro08_tasks.request_lock;
`endif
end
endtask

task pro08_req_unlock;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro08_tasks.request_unlock;
`endif
end
endtask

task pro09_req_lock;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro09_tasks.request_lock;
`endif
end
endtask

task pro09_req_unlock;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro09_tasks.request_unlock;
`endif
end
endtask

task pro10_req_lock;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro10_tasks.request_lock;
`endif
end
endtask

task pro10_req_unlock;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro10_tasks.request_unlock;
`endif
end
endtask

task pro11_req_lock;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro11_tasks.request_lock;
`endif
end
endtask

task pro11_req_unlock;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro11_tasks.request_unlock;
`endif
end
endtask

task pro12_req_lock;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro12_tasks.request_lock;
`endif
end
endtask

task pro12_req_unlock;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro12_tasks.request_unlock;
`endif
end
endtask

task pro13_req_lock;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro13_tasks.request_lock;
`endif
end
endtask

task pro13_req_unlock;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro13_tasks.request_unlock;
`endif
end
endtask

task pro14_req_lock;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro14_tasks.request_lock;
`endif
end
endtask

task pro14_req_unlock;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro14_tasks.request_unlock;
`endif
end
endtask

task pro15_req_lock;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro15_tasks.request_lock;
`endif
end
endtask

task pro15_req_unlock;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro15_tasks.request_unlock;
`endif
end
endtask

task pro16_req_lock;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro16_tasks.request_lock;
`endif
end
endtask

task pro16_req_unlock;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro16_tasks.request_unlock;
`endif
end
endtask

task pro17_req_lock;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro17_tasks.request_lock;
`endif
end
endtask

task pro17_req_unlock;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro17_tasks.request_unlock;
`endif
end
endtask

task pro18_req_lock;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro18_tasks.request_lock;
`endif
end
endtask

task pro18_req_unlock;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro18_tasks.request_unlock;
`endif
end
endtask

task pro19_req_lock;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro19_tasks.request_lock;
`endif
end
endtask

task pro19_req_unlock;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro19_tasks.request_unlock;
`endif
end
endtask

task pro20_req_lock;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro20_tasks.request_lock;
`endif
end
endtask

task pro20_req_unlock;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro20_tasks.request_unlock;
`endif
end
endtask

task int_req_lock;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.int_tasks.request_lock;
`endif
end
endtask

task int_req_unlock;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.int_tasks.request_unlock;
`endif
end
endtask

task pro00_req_lock_num_set;
input [7:0] num;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro00_tasks.request_lock_num_set(num);
`endif
end
endtask

task pro01_req_lock_num_set;
input [7:0] num;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro01_tasks.request_lock_num_set(num);
`endif
end
endtask

task pro02_req_lock_num_set;
input [7:0] num;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro02_tasks.request_lock_num_set(num);
`endif
end
endtask

task pro03_req_lock_num_set;
input [7:0] num;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro03_tasks.request_lock_num_set(num);
`endif
end
endtask

task pro04_req_lock_num_set;
input [7:0] num;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro04_tasks.request_lock_num_set(num);
`endif
end
endtask

task pro05_req_lock_num_set;
input [7:0] num;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro05_tasks.request_lock_num_set(num);
`endif
end
endtask

task pro06_req_lock_num_set;
input [7:0] num;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro06_tasks.request_lock_num_set(num);
`endif
end
endtask

task pro07_req_lock_num_set;
input [7:0] num;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro07_tasks.request_lock_num_set(num);
`endif
end
endtask

task pro08_req_lock_num_set;
input [7:0] num;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro08_tasks.request_lock_num_set(num);
`endif
end
endtask

task pro09_req_lock_num_set;
input [7:0] num;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro09_tasks.request_lock_num_set(num);
`endif
end
endtask

task pro10_req_lock_num_set;
input [7:0] num;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro10_tasks.request_lock_num_set(num);
`endif
end
endtask

task pro11_req_lock_num_set;
input [7:0] num;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro11_tasks.request_lock_num_set(num);
`endif
end
endtask

task pro12_req_lock_num_set;
input [7:0] num;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro12_tasks.request_lock_num_set(num);
`endif
end
endtask

task pro13_req_lock_num_set;
input [7:0] num;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro13_tasks.request_lock_num_set(num);
`endif
end
endtask

task pro14_req_lock_num_set;
input [7:0] num;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro14_tasks.request_lock_num_set(num);
`endif
end
endtask

task pro15_req_lock_num_set;
input [7:0] num;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro15_tasks.request_lock_num_set(num);
`endif
end
endtask

task pro16_req_lock_num_set;
input [7:0] num;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro16_tasks.request_lock_num_set(num);
`endif
end
endtask

task pro17_req_lock_num_set;
input [7:0] num;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro17_tasks.request_lock_num_set(num);
`endif
end
endtask

task pro18_req_lock_num_set;
input [7:0] num;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro18_tasks.request_lock_num_set(num);
`endif
end
endtask

task pro19_req_lock_num_set;
input [7:0] num;
begin
`ifdef NO_CPU
	`CPU_BH_TASKCORE.pro19_tasks.request_lock_num_set(num);
`endif
end
endtask

task	pro01_cpu_write2w_new;
input	[31:0]	addr;
input	[31:0] 	data0,data1;
input	[3:0]	byte0,byte1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro01_tasks.write_2w_new (addr,data0,data1,byte0,byte1);
	$display("CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, byte0 = %h, byte1 = %h", addr,data0,data1,byte0,byte1);
	`ifdef FDISP_PRO01
//	$fdisplay(f_pro00_msg,"CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`endif
`endif
end
endtask

task	pro01_cpu_write4w_new;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
input	[3:0]	byte0,byte1,byte2,byte3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro01_tasks.write_4w_new (addr,data0,data1,data2,data3,byte0,byte1,byte2,byte3);
	$display("CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h, byte0 = %h, byte1 = %h, byte2 = %h, byte3 = %h",addr,data0,data1,data2,data3,byte0,byte1,byte2,byte3);
	`ifdef FDISP_PRO01
//	$fdisplay(f_pro00_msg,"CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`endif
`endif
end
endtask

task	pro02_cpu_write2w_new;
input	[31:0]	addr;
input	[31:0] 	data0,data1;
input	[3:0]	byte0,byte1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro02_tasks.write_2w_new (addr,data0,data1,byte0,byte1);
	$display("CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, byte0 = %h, byte1 = %h", addr,data0,data1,byte0,byte1);
	`ifdef FDISP_PRO02
//	$fdisplay(f_pro00_msg,"CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`endif
`endif
end
endtask

task	pro02_cpu_write4w_new;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
input	[3:0]	byte0,byte1,byte2,byte3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro02_tasks.write_4w_new (addr,data0,data1,data2,data3,byte0,byte1,byte2,byte3);
	$display("CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h, byte0 = %h, byte1 = %h, byte2 = %h, byte3 = %h",addr,data0,data1,data2,data3,byte0,byte1,byte2,byte3);
	`ifdef FDISP_PRO02
//	$fdisplay(f_pro00_msg,"CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`endif
`endif
end
endtask

task	pro03_cpu_write2w_new;
input	[31:0]	addr;
input	[31:0] 	data0,data1;
input	[3:0]	byte0,byte1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro03_tasks.write_2w_new (addr,data0,data1,byte0,byte1);
	$display("CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, byte0 = %h, byte1 = %h", addr,data0,data1,byte0,byte1);
	`ifdef FDISP_PRO03
//	$fdisplay(f_pro00_msg,"CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`endif
`endif
end
endtask

task	pro03_cpu_write4w_new;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
input	[3:0]	byte0,byte1,byte2,byte3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro03_tasks.write_4w_new (addr,data0,data1,data2,data3,byte0,byte1,byte2,byte3);
	$display("CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h, byte0 = %h, byte1 = %h, byte2 = %h, byte3 = %h",addr,data0,data1,data2,data3,byte0,byte1,byte2,byte3);
	`ifdef FDISP_PRO03
//	$fdisplay(f_pro00_msg,"CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`endif
`endif
end
endtask

task	pro04_cpu_write2w_new;
input	[31:0]	addr;
input	[31:0] 	data0,data1;
input	[3:0]	byte0,byte1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro04_tasks.write_2w_new (addr,data0,data1,byte0,byte1);
	$display("CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, byte0 = %h, byte1 = %h", addr,data0,data1,byte0,byte1);
	`ifdef FDISP_PRO04
//	$fdisplay(f_pro00_msg,"CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`endif
`endif
end
endtask

task	pro04_cpu_write4w_new;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
input	[3:0]	byte0,byte1,byte2,byte3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro04_tasks.write_4w_new (addr,data0,data1,data2,data3,byte0,byte1,byte2,byte3);
	$display("CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h, byte0 = %h, byte1 = %h, byte2 = %h, byte3 = %h",addr,data0,data1,data2,data3,byte0,byte1,byte2,byte3);
	`ifdef FDISP_PRO04
//	$fdisplay(f_pro00_msg,"CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`endif
`endif
end
endtask

task	pro05_cpu_write2w_new;
input	[31:0]	addr;
input	[31:0] 	data0,data1;
input	[3:0]	byte0,byte1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro05_tasks.write_2w_new (addr,data0,data1,byte0,byte1);
	$display("CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, byte0 = %h, byte1 = %h", addr,data0,data1,byte0,byte1);
	`ifdef FDISP_PRO05
//	$fdisplay(f_pro00_msg,"CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`endif
`endif
end
endtask

task	pro05_cpu_write4w_new;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
input	[3:0]	byte0,byte1,byte2,byte3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro05_tasks.write_4w_new (addr,data0,data1,data2,data3,byte0,byte1,byte2,byte3);
	$display("CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h, byte0 = %h, byte1 = %h, byte2 = %h, byte3 = %h",addr,data0,data1,data2,data3,byte0,byte1,byte2,byte3);
	`ifdef FDISP_PRO05
//	$fdisplay(f_pro00_msg,"CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`endif
`endif
end
endtask

task	pro06_cpu_write2w_new;
input	[31:0]	addr;
input	[31:0] 	data0,data1;
input	[3:0]	byte0,byte1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro06_tasks.write_2w_new (addr,data0,data1,byte0,byte1);
	$display("CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, byte0 = %h, byte1 = %h", addr,data0,data1,byte0,byte1);
	`ifdef FDISP_PRO06
//	$fdisplay(f_pro00_msg,"CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`endif
`endif
end
endtask

task	pro06_cpu_write4w_new;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
input	[3:0]	byte0,byte1,byte2,byte3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro06_tasks.write_4w_new (addr,data0,data1,data2,data3,byte0,byte1,byte2,byte3);
	$display("CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h, byte0 = %h, byte1 = %h, byte2 = %h, byte3 = %h",addr,data0,data1,data2,data3,byte0,byte1,byte2,byte3);
	`ifdef FDISP_PRO06
//	$fdisplay(f_pro00_msg,"CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`endif
`endif
end
endtask

task	pro07_cpu_write2w_new;
input	[31:0]	addr;
input	[31:0] 	data0,data1;
input	[3:0]	byte0,byte1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro07_tasks.write_2w_new (addr,data0,data1,byte0,byte1);
	$display("CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, byte0 = %h, byte1 = %h", addr,data0,data1,byte0,byte1);
	`ifdef FDISP_PRO07
//	$fdisplay(f_pro00_msg,"CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`endif
`endif
end
endtask

task	pro07_cpu_write4w_new;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
input	[3:0]	byte0,byte1,byte2,byte3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro07_tasks.write_4w_new (addr,data0,data1,data2,data3,byte0,byte1,byte2,byte3);
	$display("CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h, byte0 = %h, byte1 = %h, byte2 = %h, byte3 = %h",addr,data0,data1,data2,data3,byte0,byte1,byte2,byte3);
	`ifdef FDISP_PRO07
//	$fdisplay(f_pro00_msg,"CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`endif
`endif
end
endtask

task	pro08_cpu_write2w_new;
input	[31:0]	addr;
input	[31:0] 	data0,data1;
input	[3:0]	byte0,byte1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro08_tasks.write_2w_new (addr,data0,data1,byte0,byte1);
	$display("CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, byte0 = %h, byte1 = %h", addr,data0,data1,byte0,byte1);
	`ifdef FDISP_PRO08
//	$fdisplay(f_pro00_msg,"CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`endif
`endif
end
endtask

task	pro08_cpu_write4w_new;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
input	[3:0]	byte0,byte1,byte2,byte3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro08_tasks.write_4w_new (addr,data0,data1,data2,data3,byte0,byte1,byte2,byte3);
	$display("CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h, byte0 = %h, byte1 = %h, byte2 = %h, byte3 = %h",addr,data0,data1,data2,data3,byte0,byte1,byte2,byte3);
	`ifdef FDISP_PRO08
//	$fdisplay(f_pro00_msg,"CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`endif
`endif
end
endtask

task	pro09_cpu_write2w_new;
input	[31:0]	addr;
input	[31:0] 	data0,data1;
input	[3:0]	byte0,byte1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro09_tasks.write_2w_new (addr,data0,data1,byte0,byte1);
	$display("CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, byte0 = %h, byte1 = %h", addr,data0,data1,byte0,byte1);
	`ifdef FDISP_PRO09
//	$fdisplay(f_pro00_msg,"CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`endif
`endif
end
endtask

task	pro09_cpu_write4w_new;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
input	[3:0]	byte0,byte1,byte2,byte3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro09_tasks.write_4w_new (addr,data0,data1,data2,data3,byte0,byte1,byte2,byte3);
	$display("CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h, byte0 = %h, byte1 = %h, byte2 = %h, byte3 = %h",addr,data0,data1,data2,data3,byte0,byte1,byte2,byte3);
	`ifdef FDISP_PRO09
//	$fdisplay(f_pro00_msg,"CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`endif
`endif
end
endtask

task	pro10_cpu_write2w_new;
input	[31:0]	addr;
input	[31:0] 	data0,data1;
input	[3:0]	byte0,byte1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro10_tasks.write_2w_new (addr,data0,data1,byte0,byte1);
	$display("CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, byte0 = %h, byte1 = %h", addr,data0,data1,byte0,byte1);
	`ifdef FDISP_PRO10
//	$fdisplay(f_pro00_msg,"CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`endif
`endif
end
endtask

task	pro10_cpu_write4w_new;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
input	[3:0]	byte0,byte1,byte2,byte3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro10_tasks.write_4w_new (addr,data0,data1,data2,data3,byte0,byte1,byte2,byte3);
	$display("CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h, byte0 = %h, byte1 = %h, byte2 = %h, byte3 = %h",addr,data0,data1,data2,data3,byte0,byte1,byte2,byte3);
	`ifdef FDISP_PRO10
//	$fdisplay(f_pro00_msg,"CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`endif
`endif
end
endtask

task	pro11_cpu_write2w_new;
input	[31:0]	addr;
input	[31:0] 	data0,data1;
input	[3:0]	byte0,byte1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro11_tasks.write_2w_new (addr,data0,data1,byte0,byte1);
	$display("CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, byte0 = %h, byte1 = %h", addr,data0,data1,byte0,byte1);
	`ifdef FDISP_PRO11
//	$fdisplay(f_pro00_msg,"CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`endif
`endif
end
endtask

task	pro11_cpu_write4w_new;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
input	[3:0]	byte0,byte1,byte2,byte3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro11_tasks.write_4w_new (addr,data0,data1,data2,data3,byte0,byte1,byte2,byte3);
	$display("CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h, byte0 = %h, byte1 = %h, byte2 = %h, byte3 = %h",addr,data0,data1,data2,data3,byte0,byte1,byte2,byte3);
	`ifdef FDISP_PRO11
//	$fdisplay(f_pro00_msg,"CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`endif
`endif
end
endtask

task	pro12_cpu_write2w_new;
input	[31:0]	addr;
input	[31:0] 	data0,data1;
input	[3:0]	byte0,byte1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro12_tasks.write_2w_new (addr,data0,data1,byte0,byte1);
	$display("CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, byte0 = %h, byte1 = %h", addr,data0,data1,byte0,byte1);
	`ifdef FDISP_PRO12
//	$fdisplay(f_pro00_msg,"CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`endif
`endif
end
endtask

task	pro12_cpu_write4w_new;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
input	[3:0]	byte0,byte1,byte2,byte3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro12_tasks.write_4w_new (addr,data0,data1,data2,data3,byte0,byte1,byte2,byte3);
	$display("CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h, byte0 = %h, byte1 = %h, byte2 = %h, byte3 = %h",addr,data0,data1,data2,data3,byte0,byte1,byte2,byte3);
	`ifdef FDISP_PRO12
//	$fdisplay(f_pro00_msg,"CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`endif
`endif
end
endtask

task	pro13_cpu_write2w_new;
input	[31:0]	addr;
input	[31:0] 	data0,data1;
input	[3:0]	byte0,byte1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro13_tasks.write_2w_new (addr,data0,data1,byte0,byte1);
	$display("CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, byte0 = %h, byte1 = %h", addr,data0,data1,byte0,byte1);
	`ifdef FDISP_PRO13
//	$fdisplay(f_pro00_msg,"CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`endif
`endif
end
endtask

task	pro13_cpu_write4w_new;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
input	[3:0]	byte0,byte1,byte2,byte3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro13_tasks.write_4w_new (addr,data0,data1,data2,data3,byte0,byte1,byte2,byte3);
	$display("CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h, byte0 = %h, byte1 = %h, byte2 = %h, byte3 = %h",addr,data0,data1,data2,data3,byte0,byte1,byte2,byte3);
	`ifdef FDISP_PRO13
//	$fdisplay(f_pro00_msg,"CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`endif
`endif
end
endtask

task	pro14_cpu_write2w_new;
input	[31:0]	addr;
input	[31:0] 	data0,data1;
input	[3:0]	byte0,byte1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro14_tasks.write_2w_new (addr,data0,data1,byte0,byte1);
	$display("CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, byte0 = %h, byte1 = %h", addr,data0,data1,byte0,byte1);
	`ifdef FDISP_PRO14
//	$fdisplay(f_pro00_msg,"CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`endif
`endif
end
endtask

task	pro14_cpu_write4w_new;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
input	[3:0]	byte0,byte1,byte2,byte3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro14_tasks.write_4w_new (addr,data0,data1,data2,data3,byte0,byte1,byte2,byte3);
	$display("CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h, byte0 = %h, byte1 = %h, byte2 = %h, byte3 = %h",addr,data0,data1,data2,data3,byte0,byte1,byte2,byte3);
	`ifdef FDISP_PRO14
//	$fdisplay(f_pro00_msg,"CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`endif
`endif
end
endtask

task	pro15_cpu_write2w_new;
input	[31:0]	addr;
input	[31:0] 	data0,data1;
input	[3:0]	byte0,byte1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro15_tasks.write_2w_new (addr,data0,data1,byte0,byte1);
	$display("CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, byte0 = %h, byte1 = %h", addr,data0,data1,byte0,byte1);
	`ifdef FDISP_PRO15
//	$fdisplay(f_pro00_msg,"CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`endif
`endif
end
endtask

task	pro15_cpu_write4w_new;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
input	[3:0]	byte0,byte1,byte2,byte3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro15_tasks.write_4w_new (addr,data0,data1,data2,data3,byte0,byte1,byte2,byte3);
	$display("CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h, byte0 = %h, byte1 = %h, byte2 = %h, byte3 = %h",addr,data0,data1,data2,data3,byte0,byte1,byte2,byte3);
	`ifdef FDISP_PRO15
//	$fdisplay(f_pro00_msg,"CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`endif
`endif
end
endtask

task	pro16_cpu_write2w_new;
input	[31:0]	addr;
input	[31:0] 	data0,data1;
input	[3:0]	byte0,byte1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro16_tasks.write_2w_new (addr,data0,data1,byte0,byte1);
	$display("CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, byte0 = %h, byte1 = %h", addr,data0,data1,byte0,byte1);
	`ifdef FDISP_PRO16
//	$fdisplay(f_pro00_msg,"CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`endif
`endif
end
endtask

task	pro16_cpu_write4w_new;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
input	[3:0]	byte0,byte1,byte2,byte3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro16_tasks.write_4w_new (addr,data0,data1,data2,data3,byte0,byte1,byte2,byte3);
	$display("CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h, byte0 = %h, byte1 = %h, byte2 = %h, byte3 = %h",addr,data0,data1,data2,data3,byte0,byte1,byte2,byte3);
	`ifdef FDISP_PRO16
//	$fdisplay(f_pro00_msg,"CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`endif
`endif
end
endtask

task	pro17_cpu_write2w_new;
input	[31:0]	addr;
input	[31:0] 	data0,data1;
input	[3:0]	byte0,byte1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro17_tasks.write_2w_new (addr,data0,data1,byte0,byte1);
	$display("CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, byte0 = %h, byte1 = %h", addr,data0,data1,byte0,byte1);
	`ifdef FDISP_PRO17
//	$fdisplay(f_pro00_msg,"CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`endif
`endif
end
endtask

task	pro17_cpu_write4w_new;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
input	[3:0]	byte0,byte1,byte2,byte3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro17_tasks.write_4w_new (addr,data0,data1,data2,data3,byte0,byte1,byte2,byte3);
	$display("CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h, byte0 = %h, byte1 = %h, byte2 = %h, byte3 = %h",addr,data0,data1,data2,data3,byte0,byte1,byte2,byte3);
	`ifdef FDISP_PRO17
//	$fdisplay(f_pro00_msg,"CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`endif
`endif
end
endtask

task	pro18_cpu_write2w_new;
input	[31:0]	addr;
input	[31:0] 	data0,data1;
input	[3:0]	byte0,byte1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro18_tasks.write_2w_new (addr,data0,data1,byte0,byte1);
	$display("CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, byte0 = %h, byte1 = %h", addr,data0,data1,byte0,byte1);
	`ifdef FDISP_PRO18
//	$fdisplay(f_pro00_msg,"CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`endif
`endif
end
endtask

task	pro18_cpu_write4w_new;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
input	[3:0]	byte0,byte1,byte2,byte3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro18_tasks.write_4w_new (addr,data0,data1,data2,data3,byte0,byte1,byte2,byte3);
	$display("CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h, byte0 = %h, byte1 = %h, byte2 = %h, byte3 = %h",addr,data0,data1,data2,data3,byte0,byte1,byte2,byte3);
	`ifdef FDISP_PRO18
//	$fdisplay(f_pro00_msg,"CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`endif
`endif
end
endtask

task	pro19_cpu_write2w_new;
input	[31:0]	addr;
input	[31:0] 	data0,data1;
input	[3:0]	byte0,byte1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro19_tasks.write_2w_new (addr,data0,data1,byte0,byte1);
	$display("CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, byte0 = %h, byte1 = %h", addr,data0,data1,byte0,byte1);
	`ifdef FDISP_PRO19
//	$fdisplay(f_pro00_msg,"CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`endif
`endif
end
endtask

task	pro19_cpu_write4w_new;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
input	[3:0]	byte0,byte1,byte2,byte3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro19_tasks.write_4w_new (addr,data0,data1,data2,data3,byte0,byte1,byte2,byte3);
	$display("CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h, byte0 = %h, byte1 = %h, byte2 = %h, byte3 = %h",addr,data0,data1,data2,data3,byte0,byte1,byte2,byte3);
	`ifdef FDISP_PRO19
//	$fdisplay(f_pro00_msg,"CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`endif
`endif
end
endtask

task	pro20_cpu_write2w_new;
input	[31:0]	addr;
input	[31:0] 	data0,data1;
input	[3:0]	byte0,byte1;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro20_tasks.write_2w_new (addr,data0,data1,byte0,byte1);
	$display("CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, byte0 = %h, byte1 = %h", addr,data0,data1,byte0,byte1);
	`ifdef FDISP_PRO20
//	$fdisplay(f_pro00_msg,"CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h",addr,data0,data1);
	`endif
`endif
end
endtask

task	pro20_cpu_write4w_new;
input	[31:0]	addr;
input	[31:0] 	data0,data1,data2,data3;
input	[3:0]	byte0,byte1,byte2,byte3;
begin
`ifdef	NO_CPU
	`CPU_BH_TASKCORE.pro20_tasks.write_4w_new (addr,data0,data1,data2,data3,byte0,byte1,byte2,byte3);
	$display("CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h, byte0 = %h, byte1 = %h, byte2 = %h, byte3 = %h",addr,data0,data1,data2,data3,byte0,byte1,byte2,byte3);
	`ifdef FDISP_PRO20
//	$fdisplay(f_pro00_msg,"CPU write(pro00):\t\tADDR =%h, D0 =%h, D1 =%h, D2 =%h, D3 =%h",addr,data0,data1,data2,data3);
	`endif
`endif
end
endtask

 