

/*******************************************************************
          (c) copyrights 1997-1998. All rights reserved

           T-Square Design, Inc.

          Company confidential and Properietary information.
          This information may not be disclosed to unauthorized
          individual.
*******************************************************************/

//********************************************************************
//     File:    	init.v
//     Description: used to initialize the circuit for simulation
//					and control to finish the simulation
//	   History:		2002-05-16 first version
//********************************************************************


initial begin
	init_end = 0;
	pci_vec_end = 0;
	cpu_vec_end	= 0;
	cpu_vec_00_end = 0;
	cpu_vec_01_end = 0;
	cpu_vec_02_end = 0;
	cpu_vec_03_end = 0;
	cpu_vec_04_end = 0;
	cpu_vec_05_end = 0;
	cpu_vec_06_end = 0;
	cpu_vec_07_end = 0;
	cpu_vec_08_end = 0;
	cpu_vec_09_end = 0;
	cpu_vec_10_end = 0;
	cpu_vec_11_end = 0;
	cpu_vec_12_end = 0;
	cpu_vec_13_end = 0;
	cpu_vec_14_end = 0;
	cpu_vec_15_end = 0;
	cpu_vec_16_end = 0;
	cpu_vec_17_end = 0;
	cpu_vec_18_end = 0;
	cpu_vec_19_end = 0;
	cpu_vec_20_end = 0;
	int_end = 1;
	`ifdef  INC_AUDIO
	dsp_end = 0;
	`endif

	ResetChip;
`ifdef INC_CORE	//include core of m3357
	ROM_Initialize;
	load_sdram_param;	//load sdram parameters to flash for program use
	ice_ram_initial;
    flash_access_disable; //disable the rom access
	SDRAM_Set_ClkDly;
//	FLASH_Speed_Select (8'h01);

//open  cs1#  enable
	SYS_IO_Write(8'h94, 4'b1000, {2'h0,SDRAM_Dimm1_En,29'h0});

	//SDRAM Initialization:
	SDRAM_Init;
	`ifdef NEW_MEM
	SDRAM_Set_Arbt(SDRAM_Conti_Setting,SDRAM_SubA_Req_Prio,SDRAM_CPU_Req_Prio,SDRAM_SubA_Arbt_Mode,SDRAM_SubB_Arbt_Mode,SDRAM_Main_Arbt_Mode);
	`else
	SDRAM_Set_Arbt(SDRAM_Sub_Arbt_Mode,SDRAM_SubB_Arbt_Mode,SDRAM_Main_Arbt_Mode);
	`endif
	SDRAM_Set_Refresh_Timer(SDRAM_Refresh_Time);	//Auto-Ref Enable

//for debug temporary, should be open later JFR030618
    Initialize;	                                // Software Driver Initial
	CTRL_RAMInitialize;

`ifdef WAFER_TEST
	SDRAM_Initialize;
`else

		`ifdef	FLASH_BURST_TEST
		 begin
			wait (dump_num == 2600);//first stop condition
		flash_test_load_data;
			wait (dump_num == 2601);//second stop condition
		end
		`else
		`endif
	STRAP_Pin_Info;


	`ifdef INC_CPU
		 begin
			wait (dump_num == 2605);//first stop condition
			`ifdef STOP_FLAG
			$stop;	//enter the interactive intf (TCL)of ncsim
			`endif
			wait (dump_num == 2610);//second stop condition
		end
		`ifdef	FLASH_BURST_TEST
//		flash_test_load_data;
		`else
		SDRAM_Initialize;
		`endif
	`endif
`endif

	`ifdef  INC_AUDIO
//			dsp_mem_inital;
//			initial_dsp;
	`endif

	`ifdef  INC_IDE
			ext_ide_device_enable;
			ata_share_mode_2_enable;
			ata_ad1314_source_sel(1);
	`endif

	`ifdef INC_DISPLAY	//include display
		//make the tv encoder reset unactive
		tv_encoder_rst_disable;
	`else			//include display device behaviour model
//		DISP_BH_SDRAM_load_memory(Display_SDRAM_Base,1024*256);
//Remove for debug,need modify, JFR 030708
//		DISP_BH_SDRAM_load_memory(Display_SDRAM_Base,1024*256);
//		DISP_BH_SDRAM_dump_memory(Display_SDRAM_Base,1024*256,disp_bh_file);
	`endif

`endif //include the core of m3357
	//*********************************************************
	init_end = 1;
	$display("==========================================================");
	$display("============<<< Test Pattern Start >>>====================");
	$display("==========================================================");
	wait(pci_vec_end);
	wait(cpu_vec_00_end &
		cpu_vec_01_end &
		cpu_vec_02_end &
		cpu_vec_03_end &
		cpu_vec_04_end &
		cpu_vec_05_end &
		cpu_vec_06_end &
		cpu_vec_07_end &
		cpu_vec_08_end &
		cpu_vec_09_end &
		cpu_vec_10_end &
		cpu_vec_11_end &
		cpu_vec_12_end &
		cpu_vec_13_end &
		cpu_vec_14_end &
		cpu_vec_15_end &
		cpu_vec_16_end &
		cpu_vec_17_end &
		cpu_vec_18_end &
		cpu_vec_19_end &
		cpu_vec_20_end);

	wait(int_end);

	#100;

	$finish;

end

//`ifdef SERVO_DEBUG
`ifdef INC_SERVO
	initial begin
		SV_INITIAL	;
	end
`else
`endif