//test for Broadon
for(i = 0; i < 32; i=i+1) begin
temp_data3 	= {$random};	// random start data
temp_data1	= {11'h0, temp_data3[20:2],2'b00};
//fill 0x0800_0000..7f (GI sram) with pattern

//fill memory+128..255 with pattern
Memory_Burst_Test_Write(temp_data1+32'h80,32'hab00_0c01,32);
Memory_Burst_Test_Read (temp_data1+32'h80,32'hab00_0c01,32);
//PCI write to memory+0, should contain pattern 1
Memory_Burst_Test_Write(temp_data1+32'h00,32'h1111_2201,32);
Memory_Burst_Test_Read (temp_data1+32'h00,32'h1111_2201,32);
//delay n clocks
delay(i);
//PCI read from memory+128, should contain pattern 3, and check data
Memory_Burst_Test_Read (temp_data1+32'h80,32'hab00_0c01,32);

end