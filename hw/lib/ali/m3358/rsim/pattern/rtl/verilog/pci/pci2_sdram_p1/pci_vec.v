// File : pci_vec.v
// Test Vector
// Snow Yi, 1998-12-17
// Read/Write SDRAM DIMM0
	PCI_pre_fetch (1);
$display ("\n\n\t\t-----------<<<< Memory_Burst_Test>>>>-----------\n\n");
	$display("Memory_Burst_Test_Write: Addr=32'h0000_0054\tLength=11");

	PCI2_Memory_Burst_Test_Write(32'h0000_0050,32'h0000_0001,24);
	PCI2_Memory_Burst_Test_Read (32'h0000_0050,32'h0000_0001,24);

	$display("Memory_Burst_Test_Write: Addr=32'h0000_0070\tLength=4");
	PCI2_Memory_Burst_Test_Write(32'h0000_0700,32'h0000_0010, 4);

	$display ("Memory_Burst_Test_Read: Addr=32'h0000_0050\tLength=4");
	PCI2_Memory_Burst_Test_Read (32'h0000_0054,32'h0000_0002, 3);



	$display ("Memory_Burst_Test_Read: Addr=32'h0000_0070\tLength=4");
	PCI2_Memory_Burst_Test_Read(32'h0000_00700,32'h0000_0010, 4);



	$display(" Test PCI Read_Multiple, Read_Line, Write_Invalidate Command");

	PCI2_Mem_Read_Multi_Test	(32'h0000_0050,32'h0000_0001, 16);
	PCI2_Mem_Read_Line_Test	(32'h0000_0700,32'h0000_0010, 4);

	PCI2_Mem_Write_Inv_Test	(32'h0000ff00, 32'h11223344, 4);
	#1000
	PCI2_Mem_Write_Inv_Test	(32'h0000ff80, 32'h55667788, 4);
	#1000

	PCI2_Mem_Read_Line_Test	(32'h0000ff00, 32'h11223344, 4);
	PCI2_Mem_Read_Multi_Test	(32'h0000ff80, 32'h55667788, 4);

