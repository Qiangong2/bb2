/***********************************************************************************
*	Description:	test external PCI target configuration operation via
*					CPU behavior model
*	Test Method		Write FFFF_FFFFh and 0000_0000h to each unit and then read back
*					(Only test  offset 0x6200 to 0x62ff doubble word read write
*
*	Date:		2002-06-26
*	Author:		Runner
*	CPU IO:		data-->28h,	addr-->2ch
*	Device_Num:	4, idsel = AD[20]
*	Function:	0
***********************************************************************************/
/***********************************************************************************
 Task format:
	CPU2PCITARGET_CFGxx (add, ben_, data); // 8bit offset adder, 4bit ben_, 32bit data
	CPU2PCITARGET_IOxx	(addr, ben_, data);	// 32bit offset addr, 4bit ben_, 32bit data
	CPU2PCITARGET_MEMxx_WORD	(addr, ben_, data);		// 32bit offset addr, 4bit ben_, 32bit data
	CPU2PCITARGET_MEMRD_BURST	(start_addr, length);	//32bit offset addr, interger length
	CPU2PCITARGET_MEMWR_BURST	(start_addr, length, 1st_data);	// 32bit offset addr, integer length, 32bit 1st_data

  Note: xx can RD(stand for read) and WR(stand for write)
  		See detail about those tasks in util.nb.v !!!!
************************************************************************************/
$display ("\n\n=========== <<<< Start Testing External PCI Target >>>>> =============\n\n");
$fdisplay (pci_outfile_vec, "\n\n=========== <<<< Start Testing External PCI Target >>>>> =============\n\n");
// -------------  You can find only PCI read/write info in local pci_outfile.v -----------

$display ("\n--------- << PCI Target IO Double Word Test>> ---------\n");
$fdisplay (pci_outfile_vec, "\n-------- << PCI Target IO Double Word Test >> ----------\n");

// tt(external pci target)'s IO space is 0x0000_6200~0x0000_62ff
for (pci_i = 0; pci_i < 256; pci_i = pci_i + 4) begin
	CPU2PCITARGET_IOWR	(32'h0000_6200 + pci_i, 4'b0000, 32'hffff_ffff);
	CPU2PCITARGET_IORD	(32'h0000_6200 + pci_i, 4'b0000, 32'hffff_ffff );
end

for (pci_i = 0; pci_i < 256; pci_i = pci_i + 4) begin
	CPU2PCITARGET_IOWR	(32'h0000_6200 + pci_i, 4'b0000, 32'h0000_0000);
	CPU2PCITARGET_IORD	(32'h0000_6200 + pci_i, 4'b0000, 32'h0000_0000 );
end

$display ("\n---------- <<<<< Test External PCI Target End >>>>>----------\n\n\n");
$fdisplay (pci_outfile_vec, "\n---------- <<<<< Test External PCI Target End >>>>>>----------\n\n\n");

	 