/***********************************************************************************
*	Description:	test external PCI target configuration operation via
*					CPU behavior model
*	Test Method		Write 1234_5678h and to each unit and then read back
*
*	Date:		2002-06-26
*	Author:		Runner
*	CPU IO:		data-->28h,	addr-->2ch
*	Device_Num:	4, idsel = AD[20]
*	Function:	0
***********************************************************************************/
/***********************************************************************************
 Task format:
	CPU2PCITARGET_CFGxx (add, ben_, data); // 8bit offset adder, 4bit ben_, 32bit data
	CPU2PCITARGET_IOxx	(addr, ben_, data);	// 32bit offset addr, 4bit ben_, 32bit data
	CPU2PCITARGET_MEMxx_WORD	(addr, ben_, data);		// 32bit offset addr, 4bit ben_, 32bit data
	CPU2PCITARGET_MEMRD_BURST	(start_addr, length);	//32bit offset addr, interger length
	CPU2PCITARGET_MEMWR_BURST	(start_addr, length, 1st_data);	// 32bit offset addr, integer length, 32bit 1st_data

  Note: xx can RD(stand for read) and WR(stand for write)
  		See detail about those tasks in util.nb.v !!!!
************************************************************************************/
$display ("\n\n=========== <<<< Start Testing External PCI Target >>>>> =============\n\n");
$fdisplay (pci_outfile_vec, "\n\n=========== <<<< Start Testing External PCI Target >>>>> =============\n\n");
// -------------  You can find only PCI read/write info in local pci_outfile.v -----------

$display ("\n--------- << PCI Configuration Byte Test>> ---------\n");
$fdisplay (pci_outfile_vec, "\n-------- << PCI Configuration Byte test >> ----------\n");

	CPU2PCITARGET_CFGWR	(8'h3c, 4'b1110, 32'h1234_5678 );
	CPU2PCITARGET_CFGRD	(8'h3c, 4'b1110, 32'hxxxx_xx78 );

	CPU2PCITARGET_CFGWR	(8'h3c, 4'b1101, 32'h1234_5678 );
	CPU2PCITARGET_CFGRD	(8'h3c, 4'b1101, 32'hxxxx_56xx );

	CPU2PCITARGET_CFGWR	(8'h3c, 4'b1011, 32'h1234_5678 );
	CPU2PCITARGET_CFGRD	(8'h3c, 4'b1011, 32'hxx34_xxxx );

	CPU2PCITARGET_CFGWR	(8'h3c, 4'b0111, 32'h1234_5678 );
	CPU2PCITARGET_CFGRD	(8'h3c, 4'b0111, 32'h12xx_xxxx );

   	CPU2PCITARGET_CFGRD	(8'h3c, 4'b0000, 32'h1234_5678 );
$display ("\n---------- <<<<< Test External PCI Target End >>>>>----------\n\n\n");
$fdisplay (pci_outfile_vec, "\n---------- <<<<< Test External PCI Target End >>>>>>----------\n\n\n");

	 