/**************************************************************************
	File:			local.p
	Description:	local parameter define here
	Author:			Runner Yang

****************************************************************************/
//========== define the strap pins status=============
// PLL_M = 8'h86 (134*1.5=201MHz)
//defparam	pll_m0.set_one = 0;
//defparam	pll_m1.set_one = 1;
//defparam	pll_m2.set_one = 1;
//defparam	pll_m3.set_one = 0;
//defparam	pll_m4.set_one = 0;
//defparam	pll_m5.set_one = 0;
//defparam	pll_m6.set_one = 0;
//defparam	pll_m7.set_one = 1;
// MEM_FS = 01 (1:1.5, 33%duty)
//defparam	mem_fs0.set_one = 1;
//defparam	mem_fs1.set_one = 0;
// PCI_T2RISC_FS = 00 (1:4)
defparam	pci_t2risc_fs0.set_one = 0;
defparam	pci_t2risc_fs1.set_one = 0;
// WORK_MODE = 010 (pci mode)
defparam work_mode0.set_one = 0;
defparam work_mode1.set_one = 1;
defparam work_mode2.set_one = 0;
// PLL_BYPASS = 0 (normal mode)
defparam	pll_bypass.set_one = 0;
// REFERENCE_INFO = 7

//==================================================
