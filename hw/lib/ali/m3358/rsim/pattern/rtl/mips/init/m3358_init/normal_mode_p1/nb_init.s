############################################
# 	This the program loaded to FLASH ROM for
#	initial NB (SDRAM & PCI host).
#	Because the T2 RISC Initial (TLB,cache) is
#	loaded into the SDRAM 0xa1800000,
#	Then this program just need to jump
#	to 0xa1800000.
#	Because the flash access is slower than
#	sdram access, the less use flash, the less
#	time simulation.
#
#	Yuky Liu, 2003-08-01
############################################
.nolist
#include "eregdef.h"    /* processor register definitions */
#include "osboot.h"     /* OSBOOT definitions */
#include "regsave.h"
.list
#define T2RISC_IOBASE 		0xb8000000
#define	Sys_Ctrl_MemBase	0xa0040000
#define Program_Base		0x80000000
#define	Sdram_Param_Base	0xbfcff000
#define TLB_NC_D_V_G    	0x17
#define	INFO_BASE_ADDR		0xa180ff00
#define Boot_Info			0x20
#define	TLB_Cache_FLAG		0x0000bcaf	##for map sdram address to cache
#define	TLB_NCache_FLAG		0xff00bcaf	##for did not map sdram address to cache
#define	SW_IniCache_FLAG	0x000cccaf	##for init cache with sw if open cache in TLB
#define	SW_NIniCache_FLAG	0xff0cccaf	##for init cache with verilog task if open cache in TLB

################ Main Program ###################
 		.set noreorder
	  	#---    Set the endianess bit config_be: default to big endian.
        #---    For ORIONs, it is set by boot strap, and the following
        #---    operations have no effect.  But it is necessary for
        #---    4300.
/*#if defined( MIPSEL ) && defined( ddb430 )*/

        mfc0    s0,C0_CONFIG
        li      s1,~CONFIG_BE	## Set to Little Endian: BE = 0 (bit15)
        and     s0,s1
        nop
        mtc0    s0,C0_CONFIG
        nop
        nop
/*#endif*/

        #--- kseg0 is not defined at boot-up. set it to what we wish
        #--- kseg0: 0x80000000 ~ 0xa0000000
        mfc0    s0,C0_CONFIG
        li      t0,~0x7
        and     s0,t0
#        ori     s0,s0,0x2		##close cache
        ori     s0,s0,0x3		##open cache
        mtc0    s0,C0_CONFIG    ##cacheable, write-back ,noncoherent
        nop

        #--- Turn off the cache parity exception bit (SR_DE). Reset does not
        #--- assure this bit is turned off and leaving it on is fatal.  Also,
        #--- if RAM-based, be sure to switch SR_BEV for RAM exception handler.
        lui     t0,0x6000	##open the cp1 and cp2, BEV = 0
        mtc0    t0,C0_SR
        nop
        mtc0    zero,C0_CAUSE	##clear cause register
        nop

	## Initial SDRAM controller
		jal		Mem_Init
		nop;
	## set init info for system init program
		li		t0,	INFO_BASE_ADDR
		li		t1,	TLB_Cache_FLAG		#it means open cache in TLB
#		li		t1,	TLB_NCache_FLAG		#it means don't not open cache
		sw		t1,	Boot_Info(t0)
		lw		t1,	Boot_Info(t0)

		li		t1,	SW_IniCache_FLAG	#it means init cache with software if open cache
#		li		t1,	SW_NIniCache_FLAG	#it means init cache with verilog task if open cache
		sw		t1,	Boot_Info+4(t0)
		lw		t1,	Boot_Info+4(t0)

		nop
		li		t0,	Sys_Ctrl_MemBase	#jump to system control area
#		li		t0,	Program_Base	#jump to program area directly
		jr		t0
		nop;

################### Function Area ##############################
#===============================================================
#--Function:			Mem_Init
#--Description:  		Initialize the SDRAM controler of m3357
#--Entry parameters:	No Entry parameters of input and output
#--return address:		save to the v0 register
#---------------------------------------------------------------
	.globl Mem_Init
	.ent	Mem_Init
Mem_Init:
	.set    noreorder
	move	v0,ra
	li	t1, T2RISC_IOBASE

/* ---------- Config SDRAM Controler-----------------*/
	###check the sdram command/status register for normal mode
	jal 	Wait_Normal
	nop

	###disable refresh cycle (write 0x00 to 0x81)
	li	t0,  0x00
	sb	t0,  0x81(t1)

	###set split mode (write value to 0x82)
##	li	t0,  0x11	###16MB
	li	t0,  0x1504	###16bit mode, 8M,  row0  active
	sh	t0,  0x82(t1)

	###set timing parameter (write value to 0x84)
	li	t9,	 Sdram_Param_Base
	lw	t8,	 0x0(t9)
	li	t7,	 0xffffffff
	bne	t7,	 t8, 1f
	nop
	lw	t7,	 0x8(t9)	##timing parameter
	sh	t7,  0x84(t1)
	b	2f
	nop
##	li	t0,  0x073e
1:	li	t0,  0x3505
	sh	t0,  0x84(t1)
2:
	###set mode set register (write value to 0x86)
	li	t0,  0x0133	###Linear & Burst Len =8
	sh	t0,  0x86(t1)

	###output nop command of SDRAM (write 0x00 to 0x80)
	li	t0,  0x00
	sb	t0,  0x80(t1)

	###check the sdram command/status register for normal mode
	jal 	Wait_Normal
	nop

	###precharge all banks,PALL command (write 0x02 to 0x80)
	li	t0,  0x02
	sb	t0,  0x80(t1)

	###check the sdram command/status register for normal mode
	jal	Wait_Normal
	nop

	###8 times refresh cycle, CBR command(write 0x03 to 0x80)
	li	t3,  0x8	###8 times
	li	r0,  0x0
2:	li	t0,  0x03
	sb	t0,  0x80(t1)
	###check the sdram command/status register for normal mode
	jal	Wait_Normal
	nop
	addiu	t3,  -1
	bne	t3,  r0,  2b
	nop

	### MRS command, (write 0x01 to 0x80)
	li	t0,  0x01
	sb	t0,  0x80(t1)

	###check the sdram command/status register for normal mode
	jal	Wait_Normal
	nop

	###Set refresh timer value(write value to 0x81)
	li	t0,  0x14	###166MHz SDRAM
	sb	t0,  0x81(t1)

    ###set mem control register (write 0x001f0000 to 0x90)
    ###set mem control register (write 0x003f0000 to 0x90)
    li	t0,  0x003f0000
    sw	t0,  0x90(t1)

	## Set clock tree delay chain
	li	t9,	 Sdram_Param_Base
	lw	t8,	 0x0(t9)
	li	t7,	 0xffffffff
	bne	t7,	 t8, 1f
	nop
	lw	t7,	 0x4(t9)	##clock tree delay parameter
	sh	t7,  0x64(t1)
	b	2f
	nop
1:	li	t0,	0x00000101		#set read clk delay as 0x1, ext clk and int clk dly as 0x1
	sw	t0, 0x64(t1)
2:
	###Open the PCI host bridge mem enable (write 0x02 to 0x28)
	li	t0,  0x80000804
    sw	t0,  0x2c(t1)
    li	t0,  0x0002
    sh	t0,  0x28(t1)

    move	ra,v0
    j	ra
    nop
    .end Mem_Init
/*-------- End of memory Initial -----------------*/
#================================================================


#===============================================================
#--Function:		Wait_Normal
#--Description:  	Wait SDRAM controler return to normal mode
#--Entry parameters:	No parameter input and output
#--return address:	save to the ra register
#---------------------------------------------------------------
/*---------------Wait normal mode----------------*/
	.globl	Wait_Normal
	.ent	Wait_Normal
Wait_Normal:
	li	t1, T2RISC_IOBASE
1:	lb	t0,  0x80(t1)
	li	t8,  0x00000003
	and	t0,  t8,  t0
	bne	t0,  zero,  1b
	nop
	j	ra
	nop
	.end	Wait_Normal
/*-------- End of Wait normal -----------------*/
#================================================================
