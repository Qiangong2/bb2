.nolist
#include "eregdef.h"
#include "usercfg.h"
#include "osboot.h"
#include "regsave.h"
.set noreorder
.list
###################################################################
##	This pattern is mainly used to Initialize the CPU (TLB & cache)
##	It is saved in the sdram space 0xa1800000 ~ 0xa180ffff
##	Author:	Yuky
##	2002-10-15 Initial version
##
###################################################################
#define NEC4300
#define TLB_C_D_V_G     0x1f
#define TLB_C_ND_V_G    0x1b
#define TLB_C_D_NV_G    0x1d
#define TLB_NC_D_V_G    0x17
#define TLB_NC_D_NV_G   0x15

#define	INFO_BASE_ADDR		0xa180ff00
#define T2RISC_IOBASE 		0xb8000000
#define	ErrStackSize		64
#define	ExpStackSize		128
##----About the INFO_BASE_ADDR space usage:
##	offset: 0x00---save the information of Load program step
##	offset:	0x10---save the stack pointer
##	offset:	0x20---save the boot information for cpu initialization
#define PRG_Load_step	0x00
#define	Stack_pointer	0x10
#define Boot_Inf		0x20
#define	RESET_Inf		0x30

#define	SW_IniCache_FLAG	0x000cccaf
#define	SW_NIniCache_FLAG	0xff0cccaf

#define	INI_Finish_FLAG		0xffffeeee
#define	RESET_FLAG			0x87828384	#"crst" ASCII value

########### Main Program #####################

	### judge the cpu initial information
		li		t0, INFO_BASE_ADDR
		lw		t1, Boot_Inf(t0)
		nop

		li		t2,	INI_Finish_FLAG
		beq		t1,	t2,	CPU_Init_End
		nop

		jal		SW_Cache_Initial			##initial the cache
		nop

		### Reset the Boot Info
		li		t0, INFO_BASE_ADDR
		li		t1, INI_Finish_FLAG
		sw		t1, Boot_Inf(t0)	#indicate the cpu has been initialized
		nop
		j		CPU_Init_End
		nop

CPU_Init_End:
		### Disable the flash access
		li		a0, T2RISC_IOBASE
		li		a1, 0x2
		sb		a1, 0x90(a0)

		## Set BEV == 0
		mfc0	t0,C0_SR
		li		t1,~(0x00400000)
		and		t0, t0, t1
		mtc0	t0,C0_SR
		##nop

		## enable the INT_[1] (external interrupt)
		mfc0	t0,C0_SR
		ori		t0,0x801
		mtc0	t0,C0_SR

		li		a0,	0x80000000		##Virtual address
		jr		a0					##jump the sdram 0x0 to run the application program
		nop
		### aviod dead lock, go to simulation end
		jal		simulation_end
		nop

Boot_Info_Error:
		li		a0,	0x12345678
		li		a1,	0x23456789
		li		a2,	0x3456789a
		li		a3,	0x456789ab
		jal		Error_Rpt
		nop
		jal		simulation_end
		nop

##===============================================================
##--Function Name:	Error_Rpt
##--Description: 	Used to report the error message to the pins.
##					and display the message for debug
##--Entry parameters:	( 	a0:	Error type
##				  			a1:	Error address
##				  			a2:	Error data
##				  			a3:	Expected data  )
##--return Address:	save to ra register
##--Used registers:	ra, t7, t8, t9, a0, a1, a2, a3
##---------------------------------------------------------------
	.globl	Error_Rpt
	.ent	Error_Rpt
Error_Rpt:
	## save the GPIO and Scratch register data to Stack
	la	t8, ErrStackBase
	li	t9, T2RISC_IOBASE	#--Store Expected data

	lw	t7, 0x30(t9)	#--Save Scratch register
	sw	t7, 0x00(t8)

	lb	t7, 0x55(t9)	#--Save GPIO Output
	sw	t7, 0x04(t8)

	lb	t7, 0x59(t9)	#--Save GPIO Output Ctrl
	sw	t7, 0x08(t8)

	## enable the gpio[15:12]
	li	t7, 0xf0
	sb	t7, 0x59(t9)

	## output the display data
	sw	a3, 0x30(t9)	#-- Expected data
	li	t7, 0x60
	sb	t7, 0x55(t9)

	sw	a2, 0x30(t9)	#-- Error data
	li	t7, 0x30
	sb	t7, 0x55(t9)

	sw	a1, 0x30(t9)	#-- Error address
	li	t7, 0x00
	sb	t7, 0x55(t9)

	sw	a0, 0x30(t9)	#-- Error type, trigger the error display
	li	t7, 0x40
	sb	t7, 0x55(t9)

	## restore the GPIO and Scratch register data from Stack
	lw	t7, 0x00(t8)
	sw	t7, 0x30(t9)	#--Restore Scratch register

	lw	t7, 0x04(t8)
	sb	t7, 0x55(t9)	#--Restore GPIO Output

	lw	t7, 0x08(t8)
	sb	t7, 0x59(t9)	#--Restore GPIO Output Ctrl

	## function return
	jr	ra		#-- function return
	nop
	.end 	Error_Rpt
	nop
##===============================================================

##===============================================================
##--Function Name:	Info_Display
##--Description:	Used to display the MIPS program info for
##					debug
##--Entry parameters:
##					 	a2:	Info 1 (such as address)
##				  		a3:	Info 2 (such as data)
##--return Address:	save to ra register
##--Used registers:	ra, t7, t8, t9, a2, a3
##---------------------------------------------------------------
	.globl	Info_Display
	.ent	Info_Display
Info_Display:
	## save the GPIO and Scratch register data to Stack
	la	t8, ErrStackBase
	li	t9, T2RISC_IOBASE	#--Store Expected data

	lw	t7, 0x30(t9)	#--Save Scratch register
	sw	t7, 0x00(t8)

	lb	t7, 0x55(t9)	#--Save GPIO Output
	sw	t7, 0x04(t8)

	lb	t7, 0x59(t9)	#--Save GPIO Output Ctrl
	sw	t7, 0x08(t8)

	## enable the gpio[15:12]
	li	t7, 0xf0
	sb	t7, 0x59(t9)

	## output the display data
	sw	a3, 0x30(t9)	#-- Expected data
	li	t7, 0x60
	sb	t7, 0x55(t9)

	sw	a2, 0x30(t9)	#-- Error data
	li	t7, 0x30
	sb	t7, 0x55(t9)

	sw	a1, 0x30(t9)	#-- Error address
	li	t7, 0x00
	sb	t7, 0x55(t9)

	li	a0, 0x88888888	#-- Only Display info flag
	sw	a0, 0x30(t9)	#-- Error type, trigger the error display
	li	t7, 0x40
	sb	t7, 0x55(t9)

	## restore the GPIO and Scratch register data from Stack
	lw	t7, 0x00(t8)
	sw	t7, 0x30(t9)	#--Restore Scratch register

	lw	t7, 0x04(t8)
	sb	t7, 0x55(t9)	#--Restore GPIO Output

	lw	t7, 0x08(t8)
	sb	t7, 0x59(t9)	#--Restore GPIO Output Ctrl

	## function return
	jr	ra		#-- function return
	nop
	.end 	Info_Display
	nop
##===============================================================

##===============================================================
##--Function Name:	simulation_end
##--Description:	Used to make the simulation end
##--Entry parameter:
##				(no parameters input and output)
##--return address:	save to ra
##--Used register:	ra, t0, t1,
##---------------------------------------------------------------
	.globl	simulation_end
	.ent	simulation_end
simulation_end:
	##--Stop simulation via access the 0xbfffffff address
	##enable the flash access
	li	a0, T2RISC_IOBASE
	li	a1, 0x0
	sb	a1, 0x90(a0)
	##access the last byte of flash
	li	t0, 0xbfffffff
	lb	t1, 0x(t0)
	sync
	nop;nop;nop;nop;nop;nop;nop;nop;nop;
    nop;nop;nop;nop;nop;nop;nop;nop;nop;
    nop;nop;nop;nop;nop;nop;nop;nop;nop;
    nop;nop;nop;nop;nop;nop;nop;nop;nop;
    nop;nop;nop;nop;nop;nop;nop;nop;nop;
    nop;nop;nop;nop;nop;nop;nop;nop;nop;
    addiu	zero,0
    nop;nop;nop;nop;nop;nop;nop;nop;nop;
    	jr	ra
    	nop
	.end	simulation_end
	nop
##=================================================================

#===============================================================
#--Function	SW_Cache_Initial
#		(no parameters input)
#---------------------------------------------------------------
	.globl	SW_Cache_Initial
	.ent	SW_Cache_Initial
SW_Cache_Initial:
		nop
		li		t0, INFO_BASE_ADDR
		sw		ra, Stack_pointer(t0)		##save the return pointer

###from SH
#
# //init 8Kbyte I-Cache
#
	mtc0	zero, C0_TAGLO
	li	t0, 0x80000000
	li	t1, 0x80002000
_loop_icache:
	cache	I_INDEX_STORE_TAG, 0(t0)
	bne	t0, t1, _loop_icache
	addiu	t0, 0x10

#
# //init 4Kbyte D-Cache
#
	mtc0    zero, C0_TAGLO
	li      t0, 0x80000000
	li      t1, 0x80001000
_loop_dcache:
        cache   D_INDEX_STORE_TAG, 0(t0)
        bne     t0, t1, _loop_dcache
        addiu   t0, 0x10

	nop;nop;nop;nop
	nop;nop;nop;nop


#
# //update cp0_status register
#change 0x74020003 to 0x74020001 for  int JFE030930
	li	t0, 0x74020001
	mtc0	t0, C0_STATUS

#
# //update cp0_epc register
#
	li	t0, 0x80010000
	mtc0	t0, C0_EPC
#remove interrupt, JFR030813
#	eret
	nop

    	nop
		li		t0, INFO_BASE_ADDR
		lw		ra, Stack_pointer(t0)		##get the return pointer
		jr		ra
		nop
	.end	SW_Cache_Initial
		nop
#================================================================




################## The stack area #############
##Error rpt stack
	.align 8
ErrStackBase:
	.space ErrStackSize

##exception stack
_exp_reg_save:
	.space ExpStackSize	##64
	nop
	