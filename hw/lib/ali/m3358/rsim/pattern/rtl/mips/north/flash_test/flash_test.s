.nolist
#include "eregdef.h"
#include "usercfg.h"
#include "osboot.h"
#include "regsave.h"
.set noreorder
.list
######################################################################
## File name:	flash_test.s
## Author:		Yuky
## Description:	Test flash read/write with byte/half word/word operation
##				and test the Flash read/write control and flash cycle width
##				(can be run in IDE_CD mode, PCI mode, uP mode; but all
##				share bus device must be in IDLE status)
## History:		2002-08-26 Initial version.
##################### NB CONFIG ######################################

#define	 	SysIoBase    	0xb8000000
#define		FlashBase		0xbfc00000
#define		PciMemBase		0x30000000
####### Stack Size Define #############
#define		ErrStackSize	64
#define		ExpStackSize	128
#define		IntStackSize	128
####### Error report Type Macro #######
#define		SysIoError		0x01
#define		SdramError		0x02
#define		FlashError		0x03
#define		Pci33Error		0x04
#define		UsbError		0x05
#define		IdeError		0x06
#define		GpuError		0x07
#define		VideoError		0x08
#define		DisplayError	0x09
#define		DspError		0x10
#define		SbCirError		0x11
#define		SbI2cError		0x12
#define		SbGsiError		0x13
#define		SbCdiError		0x14
#define		SbUpError		0x15
#define		VsbError		0x16
#define		SubPictureError	0x17
#######################################
.text
	.set 	noreorder
	j	Main_program
	nop
	.align	8
	nop
	.align	7
	j	gen_exception
	nop
	.align	8

########## Main Program Area ##########
Main_program:
	## inidicate test step 1
	li		a2,	0x01111111
	li		a3,	0x01111111
	jal		Info_Display
	nop
	jal	Flash_Byte_Access_Test
	nop

	## inidicate test step 2
	li		a2,	0x02222222
	li		a3,	0x02222222
	jal		Info_Display
	nop
	## change cycle width
	li		a3, 0x20
	jal	Flash_Cycle_Change_Test
	nop
	jal	Flash_HalfW_Access_Test
	nop

	## inidicate test step 3
	li		a2,	0x03333333
	li		a3,	0x03333333
	jal		Info_Display
	nop
	## change cycle width
	li		a3, 0x3f
	jal	Flash_Cycle_Change_Test
	nop
	jal	Flash_Word_Access_Test
	nop

#--Stop simulation
Main_End:
	## inidicate simulation end
	li		a2,	0xeeeeeeee
	li		a3,	0xeeeeeeee
	jal		Info_Display
	nop
	jal		simulation_end
	nop;nop

################### function area ###############################
##===============================================================
##--Function Name:	Flash_Byte_Access_Test
##--Description: 	Test the Flash byte operation
##--Entry parameters:	( 	no entry )
##--return Address:	save to stack
##--Used registers:	maybe all, before invoke this function,
##					all useful info should be saved to stack
##---------------------------------------------------------------
	.globl	Flash_Byte_Access_Test
	.ent	Flash_Byte_Access_Test
Flash_Byte_Access_Test:
	## save the return address to stack
	la		t0,	_jump_addr_save
	sw		ra,	0x0(t0)

	## enable the flash access and write
	li		a0, 	SysIoBase
	li		a1, 	0x01
	sb		a1, 	0x90(a0)

	## confirm Flash access and write enable
	lb		a1,		0x90(a0)

	## test flash byte operation
	li		a1,	FlashBase	#--address base
	ori		a1,	a1, 0x1000
	li		t1,	16			##-- test time
Byte_Operation:
	li		a3,	0xa5
	sb		a3, 0x0(a1)
	lb		a2,	0x0(a1)
	andi	a2, a2, 0xff
	beq		a2, a3, 1f
	nop
	li		a0, FlashError
	jal		Error_Rpt
	nop
1:	li		a3,	0x5a
	addi	a1,	a1, 0x1
	sb		a3, 0x0(a1)
	lbu		a2,	0x0(a1)
	andi	a2, a2, 0xff
	beq		a2, a3, 1f
	nop
	li		a0, FlashError
	jal		Error_Rpt
	nop
1:	addi	a1,	a1, 0x1
	addi	t1, t1, -1
	bne		t1, zero, Byte_Operation
	nop

	## disable the flash access and write
	li		a0, 	SysIoBase
	li		a1, 	0x02
	sb		a1, 	0x90(a0)

	## confirm Flash access and write disable
	lb		a1,		0x90(a0)

	## restore the return address from stack
	la		t0,	_jump_addr_save
	lw		ra,	0x0(t0)
	jr		ra
	nop
	.end	Flash_Byte_Access_Test
	nop
##===============================================================

##===============================================================
##--Function Name:	Flash_HalfW_Access_Test
##--Description: 	Test the Flash half word operation
##--Entry parameters:	( 	no entry )
##--return Address:	save to stack
##--Used registers:	maybe all, before invoke this function,
##					all useful info should be saved to stack
##---------------------------------------------------------------
	.globl	Flash_HalfW_Access_Test
	.ent	Flash_HalfW_Access_Test
Flash_HalfW_Access_Test:
	## save the return address to stack
	la		t0,	_jump_addr_save
	sw		ra,	0x0(t0)

	## enable the flash access and write
	li		a0, 	SysIoBase
	li		a1, 	0x01
	sb		a1, 	0x90(a0)

	## confirm Flash access and write enable
	lb		a1,		0x90(a0)

	## test flash byte operation
	li		a1,	FlashBase	#--address base
	ori		a1, a1, 0x1100
	li		t1,	10			##-- test time
HalfW_Operation:
	li		a3,	0xa5a5
	sh		a3, 0x0(a1)
	lh		a2,	0x0(a1)
	andi	a2, a2, 0xffff
	beq		a2, a3, 1f
	nop
	li		a0, FlashError
	jal		Error_Rpt
	nop
1:	li		a3,	0x5a5a
	addi	a1, a1, 0x2
	sh		a3, 0x0(a1)
	lhu		a2,	0x0(a1)
	andi	a2, a2, 0xffff
	beq		a2, a3, 1f
	nop
	li		a0, FlashError
	jal		Error_Rpt
	nop
1:	addi	a1,	a1, 0x2
	addi	t1, t1, -1
	bne		t1, zero, HalfW_Operation
	nop

	## disable the flash access and write
	li		a0, 	SysIoBase
	li		a1, 	0x02
	sb		a1, 	0x90(a0)

	## confirm Flash access and write disable
	lb		a1,		0x90(a0)

	## restore the return address from stack
	la		t0,	_jump_addr_save
	lw		ra,	0x0(t0)
	jr		ra
	nop
	.end	Flash_HalfW_Access_Test
	nop
##===============================================================

##===============================================================
##--Function Name:	Flash_Word_Access_Test
##--Description: 	Test the Flash word operation
##--Entry parameters:	( 	no entry )
##--return Address:	save to stack
##--Used registers:	maybe all, before invoke this function,
##					all useful info should be saved to stack
##---------------------------------------------------------------
	.globl	Flash_Word_Access_Test
	.ent	Flash_Word_Access_Test
Flash_Word_Access_Test:
	## save the return address to stack
	la		t0,	_jump_addr_save
	sw		ra,	0x0(t0)

	## enable the flash access and write
	li		a0, 	SysIoBase
	li		a1, 	0x01
	sb		a1, 	0x90(a0)

	## confirm Flash access and write enable
	lb		a1,		0x90(a0)

	## test flash byte operation
	li		a1,	FlashBase	#--address base
	ori		a1, a1, 0x1200
	li		t1,	6			##-- test time
Word_Operation:
	li		a3,	0xa5a5a5a5
	sw		a3, 0x0(a1)
	lw		a2,	0x0(a1)
	beq		a2, a3, 1f
	nop
	li		a0, FlashError
	jal		Error_Rpt
	nop
1:	li		a3,	0x5a5a5a5a
	addi	a1, a1, 0x4
	sw		a3, 0x0(a1)
	lw		a2,	0x0(a1)
	beq		a2, a3, 1f
	nop
	li		a0, FlashError
	jal		Error_Rpt
	nop
1:	addi	a1,	a1, 0x4
	addi	t1, t1, -1
	bne		t1, zero, Word_Operation
	nop

	## disable the flash access and write
	li		a0, 	SysIoBase
	li		a1, 	0x02
	sb		a1, 	0x90(a0)

	## confirm Flash access and write disable
	lb		a1,		0x90(a0)

	## restore the return address from stack
	la		t0,	_jump_addr_save
	lw		ra,	0x0(t0)
	jr		ra
	nop
	.end	Flash_Word_Access_Test
	nop
##===============================================================

##===============================================================
##--Function Name:	Flash_Cycle_Change_Test
##--Description: 	Test the Flash word operation
##--Entry parameters:	( 	a3: Cycle Half Width Value )
##--return Address:	save to stack
##--Used registers:	maybe all, before invoke this function,
##					all useful info should be saved to stack
##---------------------------------------------------------------
	.globl	Flash_Cycle_Change_Test
	.ent	Flash_Cycle_Change_Test
Flash_Cycle_Change_Test:
	## save the return address to stack
	la		t0,	_jump_addr_save
	sw		ra,	0x0(t0)

	## set the Flash Memory Cycle Half Width Value
	li		a1, SysIoBase
	sh		a3, 0x92(a1)

	## confirm The Flash Memory Cycle Half Width Value has been set
	lh		a2,	0x92(a1)
	andi	a2,	a2, 0x003f	##bit[31:20] of 0x90 are reserved
	ori		a3,	a3, 0x1		##bit16 of 0x90 has been hardwared to 1
	beq		a2,	a3,	1f
	nop
	li		a0,	FlashError
	jal		Error_Rpt
	nop
1:
	## restore the return address from stack
	la		t0,	_jump_addr_save
	lw		ra,	0x0(t0)
	jr		ra
	nop
	.end	Flash_Cycle_Change_Test
	nop
##===============================================================

##===============================================================
##--Function Name:	Error_Rpt
##--Description: 	Used to report the error message to the pins.
##					and display the message for debug
##--Entry parameters:	( 	a0:	Error type
##				  			a1:	Error address
##				  			a2:	Error data
##				  			a3:	Expected data  )
##--return Address:	save to ra register
##--Used registers:	ra, t7, t8, t9, a0, a1, a2, a3
##---------------------------------------------------------------
	.globl	Error_Rpt
	.ent	Error_Rpt
Error_Rpt:
	## save the GPIO and Scratch register data to Stack
	la	t8, ErrStackBase
	li	t9, SysIoBase	#--Store Expected data

	lw	t7, 0x30(t9)	#--Save Scratch register
	sw	t7, 0x00(t8)

	lb	t7, 0x55(t9)	#--Save GPIO Output
	sw	t7, 0x04(t8)

	lb	t7, 0x59(t9)	#--Save GPIO Output Ctrl
	sw	t7, 0x08(t8)

	## enable the gpio[15:12]
	li	t7, 0xf0
	sb	t7, 0x59(t9)

	## output the display data
	sw	a3, 0x30(t9)	#-- Expected data
	li	t7, 0x60
	sb	t7, 0x55(t9)

	sw	a2, 0x30(t9)	#-- Error data
	li	t7, 0x30
	sb	t7, 0x55(t9)

	sw	a1, 0x30(t9)	#-- Error address
	li	t7, 0x00
	sb	t7, 0x55(t9)

	sw	a0, 0x30(t9)	#-- Error type, trigger the error display
	li	t7, 0x40
	sb	t7, 0x55(t9)

	## restore the GPIO and Scratch register data from Stack
	lw	t7, 0x00(t8)
	sw	t7, 0x30(t9)	#--Restore Scratch register

	lw	t7, 0x04(t8)
	sb	t7, 0x55(t9)	#--Restore GPIO Output

	lw	t7, 0x08(t8)
	sb	t7, 0x59(t9)	#--Restore GPIO Output Ctrl

	## function return
	jr	ra		#-- function return
	nop
	.end 	Error_Rpt
	nop
##===============================================================

##===============================================================
##--Function Name:	Info_Display
##--Description:	Used to display the MIPS program info for
##					debug
##--Entry parameters:
##					 	a2:	Info 1 (such as address)
##				  		a3:	Info 2 (such as data)
##--return Address:	save to ra register
##--Used registers:	ra, t7, t8, t9, a2, a3
##---------------------------------------------------------------
	.globl	Info_Display
	.ent	Info_Display
Info_Display:
	## save the GPIO and Scratch register data to Stack
	la	t8, ErrStackBase
	li	t9, SysIoBase	#--Store Expected data

	lw	t7, 0x30(t9)	#--Save Scratch register
	sw	t7, 0x00(t8)

	lb	t7, 0x55(t9)	#--Save GPIO Output
	sw	t7, 0x04(t8)

	lb	t7, 0x59(t9)	#--Save GPIO Output Ctrl
	sw	t7, 0x08(t8)

	## enable the gpio[15:12]
	li	t7, 0xf0
	sb	t7, 0x59(t9)

	## output the display data
	sw	a3, 0x30(t9)	#-- Expected data
	li	t7, 0x60
	sb	t7, 0x55(t9)

	sw	a2, 0x30(t9)	#-- Error data
	li	t7, 0x30
	sb	t7, 0x55(t9)

	sw	a1, 0x30(t9)	#-- Error address
	li	t7, 0x00
	sb	t7, 0x55(t9)

	li	a0, 0x88888888	#-- Only Display info flag
	sw	a0, 0x30(t9)	#-- Error type, trigger the error display
	li	t7, 0x40
	sb	t7, 0x55(t9)

	## restore the GPIO and Scratch register data from Stack
	lw	t7, 0x00(t8)
	sw	t7, 0x30(t9)	#--Restore Scratch register

	lw	t7, 0x04(t8)
	sb	t7, 0x55(t9)	#--Restore GPIO Output

	lw	t7, 0x08(t8)
	sb	t7, 0x59(t9)	#--Restore GPIO Output Ctrl

	## function return
	jr	ra		#-- function return
	nop
	.end 	Info_Display
	nop
##===============================================================

##===============================================================
##--Function Name:	simulation_end
##--Description:	Used to make the simulation end
##--Entry parameter:
##				(no parameters input and output)
##--return address:	save to ra
##--Used register:	ra, t0, t1,
##---------------------------------------------------------------
	.globl	simulation_end
	.ent	simulation_end
simulation_end:
	##--Stop simulation via access the 0xbfffffff address
	##enable the flash access
	li	a0, SysIoBase
	li	a1, 0x0
	sb	a1, 0x90(a0)
	##access the last byte of flash
	li	t0, 0xbfffffff
	lb	t1, 0x(t0)
	sync
	nop;nop;nop;nop;nop;nop;nop;nop;nop;
    nop;nop;nop;nop;nop;nop;nop;nop;nop;
    nop;nop;nop;nop;nop;nop;nop;nop;nop;
    nop;nop;nop;nop;nop;nop;nop;nop;nop;
    nop;nop;nop;nop;nop;nop;nop;nop;nop;
    nop;nop;nop;nop;nop;nop;nop;nop;nop;
    addiu	zero,0
    nop;nop;nop;nop;nop;nop;nop;nop;nop;
    	jr	ra
    	nop
	.end	simulation_end
	nop
##=================================================================

##=================================================================
##--Function Name: gen_exception
##--Description:   General/Debug exception entry. Exceptions come here
##                 via the general exception handler.
##--return address:EPC
##--Used registers:All registers
##-----------------------------------------------------------------
		.globl	gen_exception
        .ent    gen_exception
gen_exception:
        .set    noreorder
        .set    noat
        #--- get register frame pointer, allocate a new frame,
        #--- save r16,r17 (e.g. s0,s1) and get the exccode field from
        #--- status register.
        ##save the register status
        la      k0,_exp_reg_save
        sw      zero,0(k0)
        sw      t0,_exp_save_t0*4(k0)
        sw      t1,_exp_save_t1*4(k0)
        sw      t2,_exp_save_t2*4(k0)
        sw      t3,_exp_save_t3*4(k0)
        sw      t4,_exp_save_t4*4(k0)
        sw      t5,_exp_save_t5*4(k0)
        sw      t6,_exp_save_t6*4(k0)
        sw      t7,_exp_save_t7*4(k0)
        sw      t8,_exp_save_t8*4(k0)
        sw      t9,_exp_save_t9*4(k0)
        sw      a0,_exp_save_a0*4(k0)
        sw      a1,_exp_save_a1*4(k0)
        sw      a2,_exp_save_a2*4(k0)
        sw      a3,_exp_save_a3*4(k0)
        sw      s0,_exp_save_s0*4(k0)
        sw      s1,_exp_save_s1*4(k0)
        sw      s2,_exp_save_s2*4(k0)
        sw      s3,_exp_save_s3*4(k0)
        sw      s4,_exp_save_s4*4(k0)
        sw      s5,_exp_save_s5*4(k0)
        sw      s6,_exp_save_s6*4(k0)
        sw      s7,_exp_save_s7*4(k0)
        sw      v0,_exp_save_v0*4(k0)
        sw      v1,_exp_save_v1*4(k0)
        sw      AT,_exp_save_at*4(k0)
        sw      ra,_exp_save_ra*4(k0)

        sw      gp,_exp_save_gp*4(k0)
        sw      fp,_exp_save_fp*4(k0)
        sw      sp,_exp_save_sp*4(k0)

        mfc0    t0,C0_SR
        sw      t0,_exp_save_sr*4(k0)
        mfhi    t0
        sw      t0,_exp_save_hi*4(k0)
        mflo    t1
        sw      t1,_exp_save_lo*4(k0)

        mfc0    s1,C0_CAUSE
        sw      s1,_exp_save_cause*4(k0)
        andi    s1,CAUSE_EXCMASK
        srl     s1,s1,2
        nop
        mfc0    s0,C0_BADVADDR
        sw      s0,_exp_save_BadVaddr*4(k0)
        mfc0    s1,C0_EPC
        sw      s1,_exp_save_epc*4(k0)
        add     s1,4
        mtc0    s1,C0_EPC

        ### Indicate the Interrupt Start
        li		a2, 0x12345678
        li		a3, 0x12345678
        jal		Info_Display
		nop

		### Load the CAUSE registers to display for debug
		mfc0	a2, C0_CAUSE
		or		a3, a2, a2
		jal		Info_Display
		nop

		### Jump to the interrupt processing
		#jal		Ext_Int_Process
		#nop

		### Indicate the Interrupt end
        li		a2, 0x87654321
        li		a3, 0x87654321
        jal		Info_Display
		nop

		###
		jal		simulation_end
		nop

        ##restore the register status
        la      k0,_exp_reg_save
        lw      t0,_exp_save_sr*4(k0)
        mtc0    t0,C0_SR
        lw      t0,_exp_save_hi*4(k0)
        mthi    t0
        lw      t1,_exp_save_lo*4(k0)
        mtlo    t1

        lw      t0,_exp_save_t0*4(k0)
        lw      t1,_exp_save_t1*4(k0)
        lw      t2,_exp_save_t2*4(k0)
        lw      t3,_exp_save_t3*4(k0)
        lw      t4,_exp_save_t4*4(k0)
        lw      t5,_exp_save_t5*4(k0)
        lw      t6,_exp_save_t6*4(k0)
        lw      t7,_exp_save_t7*4(k0)
        lw      t8,_exp_save_t8*4(k0)
        lw      t9,_exp_save_t9*4(k0)
        lw      a0,_exp_save_a0*4(k0)
        lw      a1,_exp_save_a1*4(k0)
        lw      a2,_exp_save_a2*4(k0)
        lw      a3,_exp_save_a3*4(k0)
        lw      s0,_exp_save_s0*4(k0)
        lw      s1,_exp_save_s1*4(k0)
        lw      s2,_exp_save_s2*4(k0)
        lw      s3,_exp_save_s3*4(k0)
        lw      s4,_exp_save_s4*4(k0)
        lw      s5,_exp_save_s5*4(k0)
        lw      s6,_exp_save_s6*4(k0)
        lw      s7,_exp_save_s7*4(k0)
        lw      v0,_exp_save_v0*4(k0)
        lw      v1,_exp_save_v1*4(k0)
        lw      AT,_exp_save_at*4(k0)
        lw      ra,_exp_save_ra*4(k0)

        lw      gp,_exp_save_gp*4(k0)
        lw      fp,_exp_save_fp*4(k0)
        lw      sp,_exp_save_sp*4(k0)

        nop;nop;nop;nop
        eret
        nop;nop;nop
        nop
        .set    at
        .end    gen_exception

################## The stack area #############
##Error rpt stack
	.align 8
ErrStackBase:
	.space ErrStackSize

##exception stack
_exp_reg_save:
	.space ExpStackSize	##128

##int processor stack
_int_info_save:
	.space	IntStackSize ##128

##Jump and link address stack
_jump_addr_save:
	.space	IntStackSize ##128
