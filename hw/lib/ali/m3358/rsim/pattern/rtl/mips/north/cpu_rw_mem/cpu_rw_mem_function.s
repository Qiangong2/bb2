##===============================================================
##--Function Name:	half_test0
##--Description:	Used to test the cpu sh operation
##					1) increasing the address of base:
##						0x80008000
##					2) decreasing the address of base:
##						0x80008000
##--Entry parameter: No entry
##--return address:	save to stack
##--Used register:	ra, t1, t2, t3, a0, a1, v0
##---------------------------------------------------------------
	.globl	half_test0
	.ent	half_test0
half_test0:
	## Push the return to stack
	addi	sp,		sp,		-4
	sw		ra,		0x0(sp)

	li		a2,		sdram_test_addr0
	li		a3,		sdram_test_addr0
	jal		Info_Display
	nop

	##-----------------------------Write
	## increase the addr
	li		t0,		sdram_test_addr0
	li		t1,		sdram_test_addr0 + 0x200
1:	sh		t0,		0x0(t0)
	addi	t0,		t0,		2
	bne		t0,		t1,		1b
	nop

	##-----------------------------Read and Check
	## increase the addr
	li		t0,		sdram_test_addr0
	li		t1,		sdram_test_addr0 + 0x200
1:	lh		a2,		0x0(t0)
	andi	a2,		a2,		0xffff
	andi	a3,		t0,		0xffff
	beq		a2,		a3,		2f
	nop
	li		a0,		SdramError
	or 		a1,		t0,		t0
	jal		Error_Rpt
	nop
2:	addi	t0,		t0,		2
	bne		t0,		t1,		1b
	nop

	##-----------------------------Write
	## decrease the addr
	li		t0,		sdram_test_addr0 + 0x200
	li		t1,		sdram_test_addr0 + 0x400
1:	addi	t1,		t1,		-2
	sh		t1,		0x0(t1)
	bne		t0,		t1,		1b
	nop

	##-----------------------------Read and Check
	## decrease the addr
	li		t0,		sdram_test_addr0 + 0x200
	li		t1,		sdram_test_addr0 + 0x400
1:	addi	t1,		t1,		-2
	lh		a2,		0x0(t1)
	andi	a2,		a2,		0xffff
	andi	a3,		t1,		0xffff
	beq		a2,		a3,		2f
	nop
	li		a0,		SdramError
	or 		a1,		t1,		t1
	jal		Error_Rpt
	nop
2:	bne		t0,		t1,		1b
	nop

	## Pop out the return to stack
	lw		ra,		0x0(sp)
	addi	sp,		sp,		4
	jr	ra
    nop
	.end	half_test0
	nop
##===============================================================

##===============================================================
##--Function Name:	word_test0
##--Description:	Used to test the cpu sw operation
##					1) increasing the address of base:
##						0x80009000
##					2) decreasing the address of base:
##						0x80009000
##--Entry parameter: No entry
##--return address:	save to stack
##--Used register:	ra, t1, t2, t3, a0, a1, v0
##---------------------------------------------------------------
	.globl	word_test0
	.ent	word_test0
word_test0:
	## Push the return to stack
	addi	sp,		sp,		-4
	sw		ra,		0x0(sp)

	li		a2,		sdram_test_addr1
	li		a3,		sdram_test_addr1
	jal		Info_Display
	nop

	##-----------------------------Write
	## increase the addr
	li		t0,		sdram_test_addr1
	li		t1,		sdram_test_addr1 + 0x200
1:	sw		t0,		0x0(t0)
	addi	t0,		t0,		4
	bne		t0,		t1,		1b
	nop

	##-----------------------------Read and Check
	## increase the addr
	li		t0,		sdram_test_addr1
	li		t1,		sdram_test_addr1 + 0x200
1:	lw		a2,		0x0(t0)
	move  a3,    t0
	beq		a2,		a3,		2f
	nop
	li		a0,		SdramError
	or 		a1,		t0,		t0
	jal		Error_Rpt
	nop
2:	addi	t0,		t0,		4
	bne		t0,		t1,		1b
	nop

	##-----------------------------Write
	## decrease the addr
	li		t0,		sdram_test_addr1 + 0x200
	li		t1,		sdram_test_addr1 + 0x400
1:	addi	t1,		t1,		-4
	sw		t1,		0x0(t1)
	bne		t0,		t1,		1b
	nop

	##-----------------------------Read and Check
	## decrease the addr
	li		t0,		sdram_test_addr1 + 0x200
	li		t1,		sdram_test_addr1 + 0x400
1:	addi	t1,		t1,		-4
	lw		a2,		0x0(t1)
	move  a3,    t1
	beq		a2,		a3,		2f
	nop
	li		a0,		SdramError
	or 		a1,		t1,		t1
	jal		Error_Rpt
	nop
2:	bne		t0,		t1,		1b
	nop

	## Pop out the return to stack
	lw		ra,		0x0(sp)
	addi	sp,		sp,		4
	jr	ra
    nop
	.end	word_test0
	nop
##===============================================================

##===============================================================
##--Function Name:	half_test1
##--Description:	Used to test the cpu sh operation
##					1) increasing the address of base:
##						0x80108000
##					2) decreasing the address of base:
##						0x80108000
##--Entry parameter: No entry
##--return address:	save to stack
##--Used register:	ra, t1, t2, t3, a0, a1, v0
##---------------------------------------------------------------
	.globl	half_test1
	.ent	half_test1
half_test1:
	## Push the return to stack
	addi	sp,		sp,		-4
	sw		ra,		0x0(sp)

	li		a2,		sdram_test_addr2
	li		a3,		sdram_test_addr2
	jal		Info_Display
	nop

	##-----------------------------Write
	## increase the addr
	li		t0,		sdram_test_addr2
	li		t1,		sdram_test_addr2 + 0x200
1:	sh		t0,		0x0(t0)
	addi	t0,		t0,		2
	bne		t0,		t1,		1b
	nop

	##-----------------------------Read and Check
	## increase the addr
	li		t0,		sdram_test_addr2
	li		t1,		sdram_test_addr2 + 0x200
1:	lh		a2,		0x0(t0)
	andi	a2,		a2,		0xffff
	andi	a3,		t0,		0xffff
	beq		a2,		a3,		2f
	nop
	li		a0,		SdramError
	or 		a1,		t0,		t0
	jal		Error_Rpt
	nop
2:	addi	t0,		t0,		2
	bne		t0,		t1,		1b
	nop

	##-----------------------------Write
	## decrease the addr
	li		t0,		sdram_test_addr2 + 0x200
	li		t1,		sdram_test_addr2 + 0x400
1:	addi	t1,		t1,		-2
	sh		t1,		0x0(t1)
	bne		t0,		t1,		1b
	nop

	##-----------------------------Read and Check
	## decrease the addr
	li		t0,		sdram_test_addr2 + 0x200
	li		t1,		sdram_test_addr2 + 0x400
1:	addi	t1,		t1,		-2
	lh		a2,		0x0(t1)
	andi	a2,		a2,		0xffff
	andi	a3,		t1,		0xffff
	beq		a2,		a3,		2f
	nop
	li		a0,		SdramError
	or 		a1,		t1,		t1
	jal		Error_Rpt
	nop
2:	bne		t0,		t1,		1b
	nop

	## Pop out the return to stack
	lw		ra,		0x0(sp)
	addi	sp,		sp,		4
	jr	ra
    nop
	.end	half_test1
	nop
##===============================================================

##===============================================================
##--Function Name:	word_test1
##--Description:	Used to test the cpu sw operation
##					1) increasing the address of base:
##						0x80109000
##					2) decreasing the address of base:
##						0x80109000
##--Entry parameter: No entry
##--return address:	save to stack
##--Used register:	ra, t1, t2, t3, a0, a1, v0
##---------------------------------------------------------------
	.globl	word_test1
	.ent	word_test1
word_test1:
	## Push the return to stack
	addi	sp,		sp,		-4
	sw		ra,		0x0(sp)

	li		a2,		sdram_test_addr3
	li		a3,		sdram_test_addr3
	jal		Info_Display
	nop

	##-----------------------------Write
	## increase the addr
	li		t0,		sdram_test_addr3
	li		t1,		sdram_test_addr3 + 0x200
1:	sw		t0,		0x0(t0)
	addi	t0,		t0,		4
	bne		t0,		t1,		1b
	nop

	##-----------------------------Read and Check
	## increase the addr
	li		t0,		sdram_test_addr3
	li		t1,		sdram_test_addr3 + 0x200
1:	lw		a2,		0x0(t0)
	move  a3,    t0
	beq		a2,		a3,		2f
	nop
	li		a0,		SdramError
	or 		a1,		t0,		t0
	jal		Error_Rpt
	nop
2:	addi	t0,		t0,		4
	bne		t0,		t1,		1b
	nop

	##-----------------------------Write
	## decrease the addr
	li		t0,		sdram_test_addr3 + 0x200
	li		t1,		sdram_test_addr3 + 0x400
1:	addi	t1,		t1,		-4
	sw		t1,		0x0(t1)
	bne		t0,		t1,		1b
	nop

	##-----------------------------Read and Check
	## decrease the addr
	li		t0,		sdram_test_addr3 + 0x200
	li		t1,		sdram_test_addr3 + 0x400
1:	addi	t1,		t1,		-4
	lw		a2,		0x0(t1)
	move  a3,    t1
	beq		a2,		a3,		2f
	nop
	li		a0,		SdramError
	or 		a1,		t1,		t1
	jal		Error_Rpt
	nop
2:	bne		t0,		t1,		1b
	nop

	## Pop out the return to stack
	lw		ra,		0x0(sp)
	addi	sp,		sp,		4
	jr	ra
    nop
	.end	word_test1
	nop
##===============================================================

##===============================================================
##--Function Name:	half_test2
##--Description:	Used to test the cpu sh operation
##					1) increasing the address of base:
##						0x80208000
##					2) decreasing the address of base:
##						0x80208000
##--Entry parameter: No entry
##--return address:	save to stack
##--Used register:	ra, t1, t2, t3, a0, a1, v0
##---------------------------------------------------------------
	.globl	half_test2
	.ent	half_test2
half_test2:
	## Push the return to stack
	addi	sp,		sp,		-4
	sw		ra,		0x0(sp)

	li		a2,		sdram_test_addr4
	li		a3,		sdram_test_addr4
	jal		Info_Display
	nop

	##-----------------------------Write
	## increase the addr
	li		t0,		sdram_test_addr4
	li		t1,		sdram_test_addr4 + 0x200
1:	sh		t0,		0x0(t0)
	addi	t0,		t0,		2
	bne		t0,		t1,		1b
	nop

	##-----------------------------Read and Check
	## increase the addr
	li		t0,		sdram_test_addr4
	li		t1,		sdram_test_addr4 + 0x200
1:	lh		a2,		0x0(t0)
	andi	a2,		a2,		0xffff
	andi	a3,		t0,		0xffff
	beq		a2,		a3,		2f
	nop
	li		a0,		SdramError
	or 		a1,		t0,		t0
	jal		Error_Rpt
	nop
2:	addi	t0,		t0,		2
	bne		t0,		t1,		1b
	nop

	##-----------------------------Write
	## decrease the addr
	li		t0,		sdram_test_addr4 + 0x200
	li		t1,		sdram_test_addr4 + 0x400
1:	addi	t1,		t1,		-2
	sh		t1,		0x0(t1)
	bne		t0,		t1,		1b
	nop

	##-----------------------------Read and Check
	## decrease the addr
	li		t0,		sdram_test_addr4 + 0x200
	li		t1,		sdram_test_addr4 + 0x400
1:	addi	t1,		t1,		-2
	lh		a2,		0x0(t1)
	andi	a2,		a2,		0xffff
	andi	a3,		t1,		0xffff
	beq		a2,		a3,		2f
	nop
	li		a0,		SdramError
	or 		a1,		t1,		t1
	jal		Error_Rpt
	nop
2:	bne		t0,		t1,		1b
	nop

	## Pop out the return to stack
	lw		ra,		0x0(sp)
	addi	sp,		sp,		4
	jr	ra
    nop
	.end	half_test2
	nop
##===============================================================

##===============================================================
##--Function Name:	word_test2
##--Description:	Used to test the cpu sw operation
##					1) increasing the address of base:
##						0x80209000
##					2) decreasing the address of base:
##						0x80209000
##--Entry parameter: No entry
##--return address:	save to stack
##--Used register:	ra, t1, t2, t3, a0, a1, v0
##---------------------------------------------------------------
	.globl	word_test2
	.ent	word_test2
word_test2:
	## Push the return to stack
	addi	sp,		sp,		-4
	sw		ra,		0x0(sp)

	li		a2,		sdram_test_addr5
	li		a3,		sdram_test_addr5
	jal		Info_Display
	nop

	##-----------------------------Write
	## increase the addr
	li		t0,		sdram_test_addr5
	li		t1,		sdram_test_addr5 + 0x200
1:	sw		t0,		0x0(t0)
	addi	t0,		t0,		4
	bne		t0,		t1,		1b
	nop

	##-----------------------------Read and Check
	## increase the addr
	li		t0,		sdram_test_addr5
	li		t1,		sdram_test_addr5 + 0x200
1:	lw		a2,		0x0(t0)
	move  a3,    t0
	beq		a2,		a3,		2f
	nop
	li		a0,		SdramError
	or 		a1,		t0,		t0
	jal		Error_Rpt
	nop
2:	addi	t0,		t0,		4
	bne		t0,		t1,		1b
	nop

	##-----------------------------Write
	## decrease the addr
	li		t0,		sdram_test_addr5 + 0x200
	li		t1,		sdram_test_addr5 + 0x400
1:	addi	t1,		t1,		-4
	sw		t1,		0x0(t1)
	bne		t0,		t1,		1b
	nop

	##-----------------------------Read and Check
	## decrease the addr
	li		t0,		sdram_test_addr5 + 0x200
	li		t1,		sdram_test_addr5 + 0x400
1:	addi	t1,		t1,		-4
	lw		a2,		0x0(t1)
	move  a3,    t1
	beq		a2,		a3,		2f
	nop
	li		a0,		SdramError
	or 		a1,		t1,		t1
	jal		Error_Rpt
	nop
2:	bne		t0,		t1,		1b
	nop

	## Pop out the return to stack
	lw		ra,		0x0(sp)
	addi	sp,		sp,		4
	jr	ra
    nop
	.end	word_test2
	nop
##===============================================================

##===============================================================
##--Function Name:	half_test3
##--Description:	Used to test the cpu sh operation
##					1) increasing the address of base:
##						0x80308000
##					2) decreasing the address of base:
##						0x80308000
##--Entry parameter: No entry
##--return address:	save to stack
##--Used register:	ra, t1, t2, t3, a0, a1, v0
##---------------------------------------------------------------
	.globl	half_test3
	.ent	half_test3
half_test3:
	## Push the return to stack
	addi	sp,		sp,		-4
	sw		ra,		0x0(sp)

	li		a2,		sdram_test_addr6
	li		a3,		sdram_test_addr6
	jal		Info_Display
	nop

	##-----------------------------Write
	## increase the addr
	li		t0,		sdram_test_addr6
	li		t1,		sdram_test_addr6 + 0x200
1:	sh		t0,		0x0(t0)
	addi	t0,		t0,		2
	bne		t0,		t1,		1b
	nop

	##-----------------------------Read and Check
	## increase the addr
	li		t0,		sdram_test_addr6
	li		t1,		sdram_test_addr6 + 0x200
1:	lh		a2,		0x0(t0)
	andi	a2,		a2,		0xffff
	andi	a3,		t0,		0xffff
	beq		a2,		a3,		2f
	nop
	li		a0,		SdramError
	or 		a1,		t0,		t0
	jal		Error_Rpt
	nop
2:	addi	t0,		t0,		2
	bne		t0,		t1,		1b
	nop

	##-----------------------------Write
	## decrease the addr
	li		t0,		sdram_test_addr6 + 0x200
	li		t1,		sdram_test_addr6 + 0x400
1:	addi	t1,		t1,		-2
	sh		t1,		0x0(t1)
	bne		t0,		t1,		1b
	nop

	##-----------------------------Read and Check
	## decrease the addr
	li		t0,		sdram_test_addr6 + 0x200
	li		t1,		sdram_test_addr6 + 0x400
1:	addi	t1,		t1,		-2
	lh		a2,		0x0(t1)
	andi	a2,		a2,		0xffff
	andi	a3,		t1,		0xffff
	beq		a2,		a3,		2f
	nop
	li		a0,		SdramError
	or 		a1,		t1,		t1
	jal		Error_Rpt
	nop
2:	bne		t0,		t1,		1b
	nop

	## Pop out the return to stack
	lw		ra,		0x0(sp)
	addi	sp,		sp,		4
	jr	ra
    nop
	.end	half_test3
	nop
##===============================================================

##===============================================================
##--Function Name:	word_test3
##--Description:	Used to test the cpu sw operation
##					1) increasing the address of base:
##						0x80309000
##					2) decreasing the address of base:
##						0x80309000
##--Entry parameter: No entry
##--return address:	save to stack
##--Used register:	ra, t1, t2, t3, a0, a1, v0
##---------------------------------------------------------------
	.globl	word_test3
	.ent	word_test3
word_test3:
	## Push the return to stack
	addi	sp,		sp,		-4
	sw		ra,		0x0(sp)

	li		a2,		sdram_test_addr7
	li		a3,		sdram_test_addr7
	jal		Info_Display
	nop

	##-----------------------------Write
	## increase the addr
	li		t0,		sdram_test_addr7
	li		t1,		sdram_test_addr7 + 0x200
1:	sw		t0,		0x0(t0)
	addi	t0,		t0,		4
	bne		t0,		t1,		1b
	nop

	##-----------------------------Read and Check
	## increase the addr
	li		t0,		sdram_test_addr7
	li		t1,		sdram_test_addr7 + 0x200
1:	lw		a2,		0x0(t0)
	move  a3,    t0
	beq		a2,		a3,		2f
	nop
	li		a0,		SdramError
	or 		a1,		t0,		t0
	jal		Error_Rpt
	nop
2:	addi	t0,		t0,		4
	bne		t0,		t1,		1b
	nop

	##-----------------------------Write
	## decrease the addr
	li		t0,		sdram_test_addr7 + 0x200
	li		t1,		sdram_test_addr7 + 0x400
1:	addi	t1,		t1,		-4
	sw		t1,		0x0(t1)
	bne		t0,		t1,		1b
	nop

	##-----------------------------Read and Check
	## decrease the addr
	li		t0,		sdram_test_addr7 + 0x200
	li		t1,		sdram_test_addr7 + 0x400
1:	addi	t1,		t1,		-4
	lw		a2,		0x0(t1)
	move  a3,    t1
	beq		a2,		a3,		2f
	nop
	li		a0,		SdramError
	or 		a1,		t1,		t1
	jal		Error_Rpt
	nop
2:	bne		t0,		t1,		1b
	nop

	## Pop out the return to stack
	lw		ra,		0x0(sp)
	addi	sp,		sp,		4
	jr	ra
    nop
	.end	word_test3
	nop
##===============================================================

##===============================================================
##--Function Name:	half_test4
##--Description:	Used to test the cpu sh operation
##					1) increasing the address of base:
##						0x80408000
##					2) decreasing the address of base:
##						0x80408000
##--Entry parameter: No entry
##--return address:	save to stack
##--Used register:	ra, t1, t2, t3, a0, a1, v0
##---------------------------------------------------------------
	.globl	half_test4
	.ent	half_test4
half_test4:
	## Push the return to stack
	addi	sp,		sp,		-4
	sw		ra,		0x0(sp)

	li		a2,		sdram_test_addr0
	li		a3,		sdram_test_addr0
	jal		Info_Display
	nop

	##-----------------------------Write
	## increase the addr
	li		t0,		sdram_test_addr8
	li		t1,		sdram_test_addr8 + 0x200
1:	sh		t0,		0x0(t0)
	addi	t0,		t0,		2
	bne		t0,		t1,		1b
	nop

	##-----------------------------Read and Check
	## increase the addr
	li		t0,		sdram_test_addr8
	li		t1,		sdram_test_addr8 + 0x200
1:	lh		a2,		0x0(t0)
	andi	a2,		a2,		0xffff
	andi	a3,		t0,		0xffff
	beq		a2,		a3,		2f
	nop
	li		a0,		SdramError
	or 		a1,		t0,		t0
	jal		Error_Rpt
	nop
2:	addi	t0,		t0,		2
	bne		t0,		t1,		1b
	nop

	##-----------------------------Write
	## decrease the addr
	li		t0,		sdram_test_addr8 + 0x200
	li		t1,		sdram_test_addr8 + 0x400
1:	addi	t1,		t1,		-2
	sh		t1,		0x0(t1)
	bne		t0,		t1,		1b
	nop

	##-----------------------------Read and Check
	## decrease the addr
	li		t0,		sdram_test_addr8 + 0x200
	li		t1,		sdram_test_addr8 + 0x400
1:	addi	t1,		t1,		-2
	lh		a2,		0x0(t1)
	andi	a2,		a2,		0xffff
	andi	a3,		t1,		0xffff
	beq		a2,		a3,		2f
	nop
	li		a0,		SdramError
	or 		a1,		t1,		t1
	jal		Error_Rpt
	nop
2:	bne		t0,		t1,		1b
	nop

	## Pop out the return to stack
	lw		ra,		0x0(sp)
	addi	sp,		sp,		4
	jr	ra
    nop
	.end	half_test4
	nop
##===============================================================

##===============================================================
##--Function Name:	word_test4
##--Description:	Used to test the cpu sw operation
##					1) increasing the address of base:
##						0x80409000
##					2) decreasing the address of base:
##						0x80409000
##--Entry parameter: No entry
##--return address:	save to stack
##--Used register:	ra, t1, t2, t3, a0, a1, v0
##---------------------------------------------------------------
	.globl	word_test4
	.ent	word_test4
word_test4:
	## Push the return to stack
	addi	sp,		sp,		-4
	sw		ra,		0x0(sp)

	li		a2,		sdram_test_addr9
	li		a3,		sdram_test_addr9
	jal		Info_Display
	nop

	##-----------------------------Write
	## increase the addr
	li		t0,		sdram_test_addr9
	li		t1,		sdram_test_addr9 + 0x200
1:	sw		t0,		0x0(t0)
	addi	t0,		t0,		4
	bne		t0,		t1,		1b
	nop

	##-----------------------------Read and Check
	## increase the addr
	li		t0,		sdram_test_addr9
	li		t1,		sdram_test_addr9 + 0x200
1:	lw		a2,		0x0(t0)
	move  a3,    t0
	beq		a2,		a3,		2f
	nop
	li		a0,		SdramError
	or 		a1,		t0,		t0
	jal		Error_Rpt
	nop
2:	addi	t0,		t0,		4
	bne		t0,		t1,		1b
	nop

	##-----------------------------Write
	## decrease the addr
	li		t0,		sdram_test_addr9 + 0x200
	li		t1,		sdram_test_addr9 + 0x400
1:	addi	t1,		t1,		-4
	sw		t1,		0x0(t1)
	bne		t0,		t1,		1b
	nop

	##-----------------------------Read and Check
	## decrease the addr
	li		t0,		sdram_test_addr9 + 0x200
	li		t1,		sdram_test_addr9 + 0x400
1:	addi	t1,		t1,		-4
	lw		a2,		0x0(t1)
	move  a3,    t1
	beq		a2,		a3,		2f
	nop
	li		a0,		SdramError
	or 		a1,		t1,		t1
	jal		Error_Rpt
	nop
2:	bne		t0,		t1,		1b
	nop

	## Pop out the return to stack
	lw		ra,		0x0(sp)
	addi	sp,		sp,		4
	jr	ra
    nop
	.end	word_test0
	nop
##===============================================================

##===============================================================
##--Function Name:	half_test5
##--Description:	Used to test the cpu sh operation
##					1) increasing the address of base:
##						0x80508000
##					2) decreasing the address of base:
##						0x80508000
##--Entry parameter: No entry
##--return address:	save to stack
##--Used register:	ra, t1, t2, t3, a0, a1, v0
##---------------------------------------------------------------
	.globl	half_test5
	.ent	half_test5
half_test5:
	## Push the return to stack
	addi	sp,		sp,		-4
	sw		ra,		0x0(sp)

	li		a2,		sdram_test_addr10
	li		a3,		sdram_test_addr10
	jal		Info_Display
	nop

	##-----------------------------Write
	## increase the addr
	li		t0,		sdram_test_addr10
	li		t1,		sdram_test_addr10+ 0x200
1:	sh		t0,		0x0(t0)
	addi	t0,		t0,		2
	bne		t0,		t1,		1b
	nop

	##-----------------------------Read and Check
	## increase the addr
	li		t0,		sdram_test_addr10
	li		t1,		sdram_test_addr10 + 0x200
1:	lh		a2,		0x0(t0)
	andi	a2,		a2,		0xffff
	andi	a3,		t0,		0xffff
	beq		a2,		a3,		2f
	nop
	li		a0,		SdramError
	or 		a1,		t0,		t0
	jal		Error_Rpt
	nop
2:	addi	t0,		t0,		2
	bne		t0,		t1,		1b
	nop

	##-----------------------------Write
	## decrease the addr
	li		t0,		sdram_test_addr10 + 0x200
	li		t1,		sdram_test_addr10 + 0x400
1:	addi	t1,		t1,		-2
	sh		t1,		0x0(t1)
	bne		t0,		t1,		1b
	nop

	##-----------------------------Read and Check
	## decrease the addr
	li		t0,		sdram_test_addr10 + 0x200
	li		t1,		sdram_test_addr10 + 0x400
1:	addi	t1,		t1,		-2
	lh		a2,		0x0(t1)
	andi	a2,		a2,		0xffff
	andi	a3,		t1,		0xffff
	beq		a2,		a3,		2f
	nop
	li		a0,		SdramError
	or 		a1,		t1,		t1
	jal		Error_Rpt
	nop
2:	bne		t0,		t1,		1b
	nop

	## Pop out the return to stack
	lw		ra,		0x0(sp)
	addi	sp,		sp,		4
	jr	ra
    nop
	.end	half_test5
	nop
##===============================================================

##===============================================================
##--Function Name:	word_test5
##--Description:	Used to test the cpu sw operation
##					1) increasing the address of base:
##						0x80509000
##					2) decreasing the address of base:
##						0x80509000
##--Entry parameter: No entry
##--return address:	save to stack
##--Used register:	ra, t1, t2, t3, a0, a1, v0
##---------------------------------------------------------------
	.globl	word_test5
	.ent	word_test5
word_test5:
	## Push the return to stack
	addi	sp,		sp,		-4
	sw		ra,		0x0(sp)

	li		a2,		sdram_test_addr11
	li		a3,		sdram_test_addr11
	jal		Info_Display
	nop

	##-----------------------------Write
	## increase the addr
	li		t0,		sdram_test_addr11
	li		t1,		sdram_test_addr11+ 0x200
1:	sw		t0,		0x0(t0)
	addi	t0,		t0,		4
	bne		t0,		t1,		1b
	nop

	##-----------------------------Read and Check
	## increase the addr
	li		t0,		sdram_test_addr11
	li		t1,		sdram_test_addr11+ 0x200
1:	lw		a2,		0x0(t0)
	move  a3,    t0
	beq		a2,		a3,		2f
	nop
	li		a0,		SdramError
	or 		a1,		t0,		t0
	jal		Error_Rpt
	nop
2:	addi	t0,		t0,		4
	bne		t0,		t1,		1b
	nop

	##-----------------------------Write
	## decrease the addr
	li		t0,		sdram_test_addr11 + 0x200
	li		t1,		sdram_test_addr11 + 0x400
1:	addi	t1,		t1,		-4
	sw		t1,		0x0(t1)
	bne		t0,		t1,		1b
	nop

	##-----------------------------Read and Check
	## decrease the addr
	li		t0,		sdram_test_addr11 + 0x200
	li		t1,		sdram_test_addr11 + 0x400
1:	addi	t1,		t1,		-4
	lw		a2,		0x0(t1)
	move  a3,    t1
	beq		a2,		a3,		2f
	nop
	li		a0,		SdramError
	or 		a1,		t1,		t1
	jal		Error_Rpt
	nop
2:	bne		t0,		t1,		1b
	nop

	## Pop out the return to stack
	lw		ra,		0x0(sp)
	addi	sp,		sp,		4
	jr	ra
    nop
	.end	word_test5
	nop
##===============================================================

##===============================================================
##--Function Name:	half_test6
##--Description:	Used to test the cpu sh operation
##					1) increasing the address of base:
##						0x80608000
##					2) decreasing the address of base:
##						0x80608000
##--Entry parameter: No entry
##--return address:	save to stack
##--Used register:	ra, t1, t2, t3, a0, a1, v0
##---------------------------------------------------------------
	.globl	half_test6
	.ent	half_test6
half_test6:
	## Push the return to stack
	addi	sp,		sp,		-4
	sw		ra,		0x0(sp)

	li		a2,		sdram_test_addr12
	li		a3,		sdram_test_addr12
	jal		Info_Display
	nop

	##-----------------------------Write
	## increase the addr
	li		t0,		sdram_test_addr12
	li		t1,		sdram_test_addr12+ 0x200
1:	sh		t0,		0x0(t0)
	addi	t0,		t0,		2
	bne		t0,		t1,		1b
	nop

	##-----------------------------Read and Check
	## increase the addr
	li		t0,		sdram_test_addr12
	li		t1,		sdram_test_addr12 + 0x200
1:	lh		a2,		0x0(t0)
	andi	a2,		a2,		0xffff
	andi	a3,		t0,		0xffff
	beq		a2,		a3,		2f
	nop
	li		a0,		SdramError
	or 		a1,		t0,		t0
	jal		Error_Rpt
	nop
2:	addi	t0,		t0,		2
	bne		t0,		t1,		1b
	nop

	##-----------------------------Write
	## decrease the addr
	li		t0,		sdram_test_addr12 + 0x200
	li		t1,		sdram_test_addr12 + 0x400
1:	addi	t1,		t1,		-2
	sh		t1,		0x0(t1)
	bne		t0,		t1,		1b
	nop

	##-----------------------------Read and Check
	## decrease the addr
	li		t0,		sdram_test_addr12 + 0x200
	li		t1,		sdram_test_addr12 + 0x400
1:	addi	t1,		t1,		-2
	lh		a2,		0x0(t1)
	andi	a2,		a2,		0xffff
	andi	a3,		t1,		0xffff
	beq		a2,		a3,		2f
	nop
	li		a0,		SdramError
	or 		a1,		t1,		t1
	jal		Error_Rpt
	nop
2:	bne		t0,		t1,		1b
	nop

	## Pop out the return to stack
	lw		ra,		0x0(sp)
	addi	sp,		sp,		4
	jr	ra
    nop
	.end	half_test6
	nop
##===============================================================

##===============================================================
##--Function Name:	word_test6
##--Description:	Used to test the cpu sw operation
##					1) increasing the address of base:
##						0x80609000
##					2) decreasing the address of base:
##						0x80609000
##--Entry parameter: No entry
##--return address:	save to stack
##--Used register:	ra, t1, t2, t3, a0, a1, v0
##---------------------------------------------------------------
	.globl	word_test6
	.ent	word_test6
word_test6:
	## Push the return to stack
	addi	sp,		sp,		-4
	sw		ra,		0x0(sp)

	li		a2,		sdram_test_addr13
	li		a3,		sdram_test_addr13
	jal		Info_Display
	nop

	##-----------------------------Write
	## increase the addr
	li		t0,		sdram_test_addr13
	li		t1,		sdram_test_addr13 + 0x200
1:	sw		t0,		0x0(t0)
	addi	t0,		t0,		4
	bne		t0,		t1,		1b
	nop

	##-----------------------------Read and Check
	## increase the addr
	li		t0,		sdram_test_addr13
	li		t1,		sdram_test_addr13 + 0x200
1:	lw		a2,		0x0(t0)
	move  a3,    t0
	beq		a2,		a3,		2f
	nop
	li		a0,		SdramError
	or 		a1,		t0,		t0
	jal		Error_Rpt
	nop
2:	addi	t0,		t0,		4
	bne		t0,		t1,		1b
	nop

	##-----------------------------Write
	## decrease the addr
	li		t0,		sdram_test_addr13 + 0x200
	li		t1,		sdram_test_addr13 + 0x400
1:	addi	t1,		t1,		-4
	sw		t1,		0x0(t1)
	bne		t0,		t1,		1b
	nop

	##-----------------------------Read and Check
	## decrease the addr
	li		t0,		sdram_test_addr13 + 0x200
	li		t1,		sdram_test_addr13 + 0x400
1:	addi	t1,		t1,		-4
	lw		a2,		0x0(t1)
	move  a3,    t1
	beq		a2,		a3,		2f
	nop
	li		a0,		SdramError
	or 		a1,		t1,		t1
	jal		Error_Rpt
	nop
2:	bne		t0,		t1,		1b
	nop

	## Pop out the return to stack
	lw		ra,		0x0(sp)
	addi	sp,		sp,		4
	jr	ra
    nop
	.end	word_test6
	nop
##===============================================================

##===============================================================
##--Function Name:	half_test7
##--Description:	Used to test the cpu sh operation
##					1) increasing the address of base:
##						0x80708000
##					2) decreasing the address of base:
##						0x80708000
##--Entry parameter: No entry
##--return address:	save to stack
##--Used register:	ra, t1, t2, t3, a0, a1, v0
##---------------------------------------------------------------
	.globl	half_test7
	.ent	half_test7
half_test7:
	## Push the return to stack
	addi	sp,		sp,		-4
	sw		ra,		0x0(sp)

	li		a2,		sdram_test_addr14
	li		a3,		sdram_test_addr14
	jal		Info_Display
	nop

	##-----------------------------Write
	## increase the addr
	li		t0,		sdram_test_addr14
	li		t1,		sdram_test_addr14 + 0x200
1:	sh		t0,		0x0(t0)
	addi	t0,		t0,		2
	bne		t0,		t1,		1b
	nop

	##-----------------------------Read and Check
	## increase the addr
	li		t0,		sdram_test_addr14
	li		t1,		sdram_test_addr14 + 0x200
1:	lh		a2,		0x0(t0)
	andi	a2,		a2,		0xffff
	andi	a3,		t0,		0xffff
	beq		a2,		a3,		2f
	nop
	li		a0,		SdramError
	or 		a1,		t0,		t0
	jal		Error_Rpt
	nop
2:	addi	t0,		t0,		2
	bne		t0,		t1,		1b
	nop

	##-----------------------------Write
	## decrease the addr
	li		t0,		sdram_test_addr14 + 0x200
	li		t1,		sdram_test_addr14 + 0x400
1:	addi	t1,		t1,		-2
	sh		t1,		0x0(t1)
	bne		t0,		t1,		1b
	nop

	##-----------------------------Read and Check
	## decrease the addr
	li		t0,		sdram_test_addr14 + 0x200
	li		t1,		sdram_test_addr14 + 0x400
1:	addi	t1,		t1,		-2
	lh		a2,		0x0(t1)
	andi	a2,		a2,		0xffff
	andi	a3,		t1,		0xffff
	beq		a2,		a3,		2f
	nop
	li		a0,		SdramError
	or 		a1,		t1,		t1
	jal		Error_Rpt
	nop
2:	bne		t0,		t1,		1b
	nop

	## Pop out the return to stack
	lw		ra,		0x0(sp)
	addi	sp,		sp,		4
	jr	ra
    nop
	.end	half_test7
	nop
##===============================================================

##===============================================================
##--Function Name:	word_test7
##--Description:	Used to test the cpu sw operation
##					1) increasing the address of base:
##						0x80709000
##					2) decreasing the address of base:
##						0x80709000
##--Entry parameter: No entry
##--return address:	save to stack
##--Used register:	ra, t1, t2, t3, a0, a1, v0
##---------------------------------------------------------------
	.globl	word_test7
	.ent	word_test7
word_test7:
	## Push the return to stack
	addi	sp,		sp,		-4
	sw		ra,		0x0(sp)

	li		a2,		sdram_test_addr15
	li		a3,		sdram_test_addr15
	jal		Info_Display
	nop

	##-----------------------------Write
	## increase the addr
	li		t0,		sdram_test_addr15
	li		t1,		sdram_test_addr15+ 0x200
1:	sw		t0,		0x0(t0)
	addi	t0,		t0,		4
	bne		t0,		t1,		1b
	nop

	##-----------------------------Read and Check
	## increase the addr
	li		t0,		sdram_test_addr15
	li		t1,		sdram_test_addr15 + 0x200
1:	lw		a2,		0x0(t0)
	move  a3,    t0
	beq		a2,		a3,		2f
	nop
	li		a0,		SdramError
	or 		a1,		t0,		t0
	jal		Error_Rpt
	nop
2:	addi	t0,		t0,		4
	bne		t0,		t1,		1b
	nop

	##-----------------------------Write
	## decrease the addr
	li		t0,		sdram_test_addr15 + 0x200
	li		t1,		sdram_test_addr15 + 0x400
1:	addi	t1,		t1,		-4
	sw		t1,		0x0(t1)
	bne		t0,		t1,		1b
	nop

	##-----------------------------Read and Check
	## decrease the addr
	li		t0,		sdram_test_addr15 + 0x200
	li		t1,		sdram_test_addr15 + 0x400
1:	addi	t1,		t1,		-4
	lw		a2,		0x0(t1)
	move  a3,    t1
	beq		a2,		a3,		2f
	nop
	li		a0,		SdramError
	or 		a1,		t1,		t1
	jal		Error_Rpt
	nop
2:	bne		t0,		t1,		1b
	nop

	## Pop out the return to stack
	lw		ra,		0x0(sp)
	addi	sp,		sp,		4
	jr	ra
    nop
	.end	word_test7
	nop
##===============================================================