######################################################
## File name:	cpu_fill_data.s
## Author:		Tod
## Description:	This program is used to test CPU rw
##				memory space with word and half word
##				commands
## History:		2002-12-07 Initial version
######################################################
.nolist
#include "eregdef.h"
#include "usercfg.h"
#include "osboot.h"
#include "regsave.h"
.list
.set noreorder
#define	 	SysIoBase    	0xb8000000
#define		sdram_test_addr0  0x80008000
#define		sdram_test_addr1  0x80009000
#define		sdram_test_addr2  0x80108000
#define		sdram_test_addr3  0x80109000
#define		sdram_test_addr4  0x80208000
#define		sdram_test_addr5  0x80209000
#define		sdram_test_addr6  0x80308000
#define		sdram_test_addr7  0x80309000
#define		sdram_test_addr8  0x80408000
#define		sdram_test_addr9  0x80409000
#define		sdram_test_addr10 0x80508000
#define		sdram_test_addr11 0x80509000
#define		sdram_test_addr12 0x80608000
#define		sdram_test_addr13 0x80609000
#define		sdram_test_addr14 0x80708000
#define		sdram_test_addr15 0x80709000

#define		ErrStackSize	64
#define		ExpStackSize	128
#define		CallStackSize 	1024
####### int processor #################
#define		IntStackSize	128
#define		IntProPointer	0x1
#define		ExtIntInfo		0x2
#define		ErrIntMask		0x3
#define		ExtIntMask		0x4
####### Error report Type Macro #######
#define		SysIoError		0x01
#define		SdramError		0x02
#define		FlashError		0x03
#define		Pci33Error		0x04
#define		UsbError		0x05
#define		IdeError		0x06
#define		GpuError		0x07
#define		VideoError		0x08
#define		DisplayError	0x09
#define		DspError		0x10
#define		SbCirError		0x11
#define		SbI2cError		0x12
#define		SbGsiError		0x13
#define		SbCdiError		0x14
#define		SbUpError		0x15
#define		VsbError		0x16
#define		SubPictureError	0x17
#######################################

	.text
	.set 	noreorder
	j	Main_program
	nop
	.align	8
	nop
	.align	7
	j	gen_exception
	nop
	.align	8

########## Main Program Area ##########
Main_program:
	### load the stack top address to sp
	la		sp,		_stack_frame + CallStackSize	##bottom<--> high addr, sub the sp before push stack
## display Info to indicate start
	li	a2, 	0x11111111
	li	a3, 	0x11111111
	jal	Info_Display
	nop
	jal	word_test0
	nop
	li	a2,		0x00001111
	li	a3,		0x00001111
	jal	Info_Display
	nop
	jal half_test0
	nop
	li	a2, 	0x22222222
	li	a3, 	0x22222222
	jal	Info_Display
	nop
	jal	word_test1
	nop
	li	a2,		0x00002222
	li	a3,		0x00002222
	jal	Info_Display
	nop
	jal half_test1
	nop
	li	a2, 	0x33333333
	li	a3, 	0x33333333
	jal	Info_Display
	nop
	jal	word_test2
	nop
	li	a2,		0x00003333
	li	a3,		0x00003333
	jal	Info_Display
	nop
	jal half_test2
	nop
	li	a2, 	0x44444444
	li	a3, 	0x44444444
	jal	Info_Display
	nop
	jal	word_test3
	nop
	li	a2,		0x00004444
	li	a3,		0x00004444
	jal	Info_Display
	nop
	jal half_test3
	nop
	li	a2, 	0x55555555
	li	a3, 	0x55555555
	jal	Info_Display
	nop
	jal	word_test4
	nop
	li	a2,		0x00005555
	li	a2,		0x00005555
	jal	Info_Display
	nop
	jal half_test4
	nop
	li	a2, 	0x66666666
	li	a3, 	0x66666666
	jal	Info_Display
	nop
	jal	word_test5
	nop
	li	a2,		0x00006666
	li	a3,		0x00006666
	jal	Info_Display
	nop
	jal half_test5
	nop
	li	a2, 	0x77777777
	li	a3, 	0x77777777
	jal	Info_Display
	nop
	jal	word_test6
	nop
	li	a2,		0x00007777
	li	a3,		0x00007777
	jal	Info_Display
	nop
	jal half_test6
	nop
	li	a2, 	0x88888888
	li	a3, 	0x88888888
    jal	Info_Display
    nop
    jal	word_test7
    nop
    li	a2,		0x00008888
    li	a3,		0x00008888
    jal	Info_Display
	nop
    jal half_test7
    nop

    jal simulation_end
    nop

####function area##############
#include	"public_func.s"
#include	"cpu_rw_mem_function.s"
################## The stack area #############
##Error rpt stack
	.align 8
ErrStackBase:
	.space ErrStackSize

##exception stack
_exp_reg_save:
	.space ExpStackSize	##128

##int processor stack
_int_info_save:
	.space	IntStackSize ##128

##Jump and link address stack
_stack_frame:
	.space	CallStackSize ##1024
	.align	4

##DE int Info
_frame_int_cnt:
	.word	0
_vb_int_cnt:
	.word	0
_hb_int_cnt:
	.word	0
_fifo_int_cnt:
	.word	0
_line_int_cnt:
	.word	0

##I2C int Info
_i2c_int_status:
	.word	0

##GPU int Info
_gpu_int_status:
	.word	0

##IDE int Info:
_ide_int_status:
	.word	0        