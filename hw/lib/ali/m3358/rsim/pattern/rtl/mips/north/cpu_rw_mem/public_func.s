######################################################
## File name:	public_func.s
## Author:		Yuky
## Description:	file is used to saved the public fucntions
##				which will be used in all program:
##				1)Error_Rpt
##				2)Info_Display
##				3)simulation_end
##				4)gen_exception
##				5)Ext_Int_Process
## History:		2002-12-03 setup a independent file
##				2003-12-12 modified by Tod
######################################################

##===============================================================
##--Function Name:	Error_Rpt
##--Description: 	Used to report the error message to the pins.
##					and display the message for debug
##--Entry parameters:	( 	a0:	Error type
##				  			a1:	Error address
##				  			a2:	Error data
##				  			a3:	Expected data  )
##--return Address:	save to ra register
##--Used registers:	ra, t7, t8, t9, a0, a1, a2, a3
##---------------------------------------------------------------
	.globl	Error_Rpt
	.ent	Error_Rpt
Error_Rpt:
	## save the GPIO and Scratch register data to Stack
	la	t8, ErrStackBase
	li	t9, SysIoBase	#--Store Expected data

	lw	t7, 0x30(t9)	#--Save Scratch register
	sw	t7, 0x00(t8)

	lb	t7, 0x55(t9)	#--Save GPIO Output
	sw	t7, 0x04(t8)

	lb	t7, 0x59(t9)	#--Save GPIO Output Ctrl
	sw	t7, 0x08(t8)

	## enable the gpio[15:12]
	li	t7, 0xf0
	sb	t7, 0x59(t9)

	## output the display data
	sw	a3, 0x30(t9)	#-- Expected data
	li	t7, 0x60
	sb	t7, 0x55(t9)

	sw	a2, 0x30(t9)	#-- Error data
	li	t7, 0x30
	sb	t7, 0x55(t9)

	sw	a1, 0x30(t9)	#-- Error address
	li	t7, 0x00
	sb	t7, 0x55(t9)

	sw	a0, 0x30(t9)	#-- Error type, trigger the error display
	li	t7, 0x40
	sb	t7, 0x55(t9)

	## restore the GPIO and Scratch register data from Stack
	lw	t7, 0x00(t8)
	sw	t7, 0x30(t9)	#--Restore Scratch register

	lw	t7, 0x04(t8)
	sb	t7, 0x55(t9)	#--Restore GPIO Output

	lw	t7, 0x08(t8)
	sb	t7, 0x59(t9)	#--Restore GPIO Output Ctrl

	## function return
	jr	ra		#-- function return
	nop
	.end 	Error_Rpt
	nop
##===============================================================

##===============================================================
##--Function Name:	Info_Display
##--Description:	Used to display the MIPS program info for
##					debug
##--Entry parameters:
##					 	a2:	Info 1 (such as address)
##				  		a3:	Info 2 (such as data)
##--return Address:	save to ra register
##--Used registers:	ra, t7, t8, t9, a2, a3
##---------------------------------------------------------------
	.globl	Info_Display
	.ent	Info_Display
Info_Display:
	## save the GPIO and Scratch register data to Stack
	la	t8, ErrStackBase
	li	t9, SysIoBase	#--Store Expected data

	lw	t7, 0x30(t9)	#--Save Scratch register
	sw	t7, 0x00(t8)

	lb	t7, 0x55(t9)	#--Save GPIO Output
	sw	t7, 0x04(t8)

	lb	t7, 0x59(t9)	#--Save GPIO Output Ctrl
	sw	t7, 0x08(t8)

	## enable the gpio[15:12]
	li	t7, 0xf0
	sb	t7, 0x59(t9)

	## output the display data
	sw	a3, 0x30(t9)	#-- Expected data
	li	t7, 0x60
	sb	t7, 0x55(t9)

	sw	a2, 0x30(t9)	#-- Error data
	li	t7, 0x30
	sb	t7, 0x55(t9)

	sw	a1, 0x30(t9)	#-- Error address
	li	t7, 0x00
	sb	t7, 0x55(t9)

	li	a0, 0x88888888	#-- Only Display info flag
	sw	a0, 0x30(t9)	#-- Error type, trigger the error display
	li	t7, 0x40
	sb	t7, 0x55(t9)

	## restore the GPIO and Scratch register data from Stack
	lw	t7, 0x00(t8)
	sw	t7, 0x30(t9)	#--Restore Scratch register

	lw	t7, 0x04(t8)
	sb	t7, 0x55(t9)	#--Restore GPIO Output

	lw	t7, 0x08(t8)
	sb	t7, 0x59(t9)	#--Restore GPIO Output Ctrl

	## function return
	jr	ra		#-- function return
	nop
	.end 	Info_Display
	nop
##===============================================================

##===============================================================
##--Function Name:	Trigger_ColdRst
##--Description:	Used to trigger a cold reset flag.
##					Then the task pattern can snoop this flag
##					to reset the m6304
##--Entry parameters: no entry
##--return Address:	save to ra register
##--Used registers:	ra, t7, t8, t9, a2, a3
##---------------------------------------------------------------
	.globl	Trigger_ColdRst
	.ent	Trigger_ColdRst
Trigger_ColdRst:
	## save the GPIO and Scratch register data to Stack
	la	t8, ErrStackBase
	li	t9, SysIoBase	#--Store Expected data

	lw	t7, 0x30(t9)	#--Save Scratch register
	sw	t7, 0x00(t8)

	lb	t7, 0x55(t9)	#--Save GPIO Output
	sw	t7, 0x04(t8)

	lb	t7, 0x59(t9)	#--Save GPIO Output Ctrl
	sw	t7, 0x08(t8)

	## enable the gpio[15:12]
	li	t7, 0xf0
	sb	t7, 0x59(t9)

	## output the display data
	sw	a3, 0x30(t9)	#-- Expected data
	li	t7, 0x60
	sb	t7, 0x55(t9)

	sw	a2, 0x30(t9)	#-- Error data
	li	t7, 0x30
	sb	t7, 0x55(t9)

	sw	a1, 0x30(t9)	#-- Error address
	li	t7, 0x00
	sb	t7, 0x55(t9)

	li	a0, 0x43525354	#-- Only Display info flag
	sw	a0, 0x30(t9)	#-- Error type, trigger the error display
	li	t7, 0x40
	sb	t7, 0x55(t9)

	## restore the GPIO and Scratch register data from Stack
	lw	t7, 0x00(t8)
	sw	t7, 0x30(t9)	#--Restore Scratch register

	lw	t7, 0x04(t8)
	sb	t7, 0x55(t9)	#--Restore GPIO Output

	lw	t7, 0x08(t8)
	sb	t7, 0x59(t9)	#--Restore GPIO Output Ctrl

	## function return
	jr	ra		#-- function return
	nop
	.end 	Trigger_ColdRst
	nop
##===============================================================


##===============================================================
##--Function Name:	simulation_end
##--Description:	Used to make the simulation end
##--Entry parameter:
##				(no parameters input and output)
##--return address:	save to ra
##--Used register:	ra, t0, t1,
##---------------------------------------------------------------
	.globl	simulation_end
	.ent	simulation_end
simulation_end:
	##--Stop simulation via access the 0xbfffffff address
	##enable the flash access
	li	a0, SysIoBase
	li	a1, 0x0
	sb	a1, 0x90(a0)
	##access the last byte of flash
	li	t0, 0xbfffffff
	lb	t1, 0x(t0)
	sync
	nop;nop;nop;nop;nop;nop;nop;nop;nop;
    nop;nop;nop;nop;nop;nop;nop;nop;nop;
    nop;nop;nop;nop;nop;nop;nop;nop;nop;
    nop;nop;nop;nop;nop;nop;nop;nop;nop;
    nop;nop;nop;nop;nop;nop;nop;nop;nop;
    nop;nop;nop;nop;nop;nop;nop;nop;nop;
    addiu	zero,0
    nop;nop;nop;nop;nop;nop;nop;nop;nop;
    	jr	ra
    	nop
	.end	simulation_end
	nop
##=================================================================

##=================================================================
##--Function Name: gen_exception
##--Description:   General/Debug exception entry. Exceptions come here
##                 via the general exception handler.
##--return address:EPC
##--Used registers:All registers
##-----------------------------------------------------------------
		.globl	gen_exception
        .ent    gen_exception
gen_exception:
        .set    noreorder
        .set    noat
        #--- get register frame pointer, allocate a new frame,
        #--- save r16,r17 (e.g. s0,s1) and get the exccode field from
        #--- status register.
        ##save the register status
        la      k0,_exp_reg_save
        sw      zero,0(k0)
        sw      t0,_exp_save_t0*4(k0)
        sw      t1,_exp_save_t1*4(k0)
        sw      t2,_exp_save_t2*4(k0)
        sw      t3,_exp_save_t3*4(k0)
        sw      t4,_exp_save_t4*4(k0)
        sw      t5,_exp_save_t5*4(k0)
        sw      t6,_exp_save_t6*4(k0)
        sw      t7,_exp_save_t7*4(k0)
        sw      t8,_exp_save_t8*4(k0)
        sw      t9,_exp_save_t9*4(k0)
        sw      a0,_exp_save_a0*4(k0)
        sw      a1,_exp_save_a1*4(k0)
        sw      a2,_exp_save_a2*4(k0)
        sw      a3,_exp_save_a3*4(k0)
        sw      s0,_exp_save_s0*4(k0)
        sw      s1,_exp_save_s1*4(k0)
        sw      s2,_exp_save_s2*4(k0)
        sw      s3,_exp_save_s3*4(k0)
        sw      s4,_exp_save_s4*4(k0)
        sw      s5,_exp_save_s5*4(k0)
        sw      s6,_exp_save_s6*4(k0)
        sw      s7,_exp_save_s7*4(k0)
        sw      v0,_exp_save_v0*4(k0)
        sw      v1,_exp_save_v1*4(k0)
        sw      AT,_exp_save_at*4(k0)
        sw      ra,_exp_save_ra*4(k0)

        sw      gp,_exp_save_gp*4(k0)
        sw      fp,_exp_save_fp*4(k0)
        sw      sp,_exp_save_sp*4(k0)

        mfc0    t0,C0_SR
        sw      t0,_exp_save_sr*4(k0)
        mfhi    t0
        sw      t0,_exp_save_hi*4(k0)
        mflo    t1
        sw      t1,_exp_save_lo*4(k0)

        mfc0    s1,C0_CAUSE
        sw      s1,_exp_save_cause*4(k0)
        andi    s1,CAUSE_EXCMASK
        srl     s1,s1,2
        nop
        mfc0    s0,C0_BADVADDR
        sw      s0,_exp_save_BadVaddr*4(k0)
        mfc0    s1,C0_EPC
        sw      s1,_exp_save_epc*4(k0)

        ### Indicate the Interrupt Start
        li		a2, 0x12345678
        li		a3, 0x12345678
        jal		Info_Display
		nop

		### Jump to the interrupt processing
//		jal		Ext_Int_Process
//		nop

		### Indicate the Interrupt end
        li		a2, 0x87654321
        li		a3, 0x87654321
        jal		Info_Display
		nop

        ##restore the register status
        la      k0,_exp_reg_save
        lw      t0,_exp_save_sr*4(k0)
        mtc0    t0,C0_SR
        lw      t0,_exp_save_hi*4(k0)
        mthi    t0
        lw      t1,_exp_save_lo*4(k0)
        mtlo    t1

        lw      t0,_exp_save_t0*4(k0)
        lw      t1,_exp_save_t1*4(k0)
        lw      t2,_exp_save_t2*4(k0)
        lw      t3,_exp_save_t3*4(k0)
        lw      t4,_exp_save_t4*4(k0)
        lw      t5,_exp_save_t5*4(k0)
        lw      t6,_exp_save_t6*4(k0)
        lw      t7,_exp_save_t7*4(k0)
        lw      t8,_exp_save_t8*4(k0)
        lw      t9,_exp_save_t9*4(k0)
        lw      a0,_exp_save_a0*4(k0)
        lw      a1,_exp_save_a1*4(k0)
        lw      a2,_exp_save_a2*4(k0)
        lw      a3,_exp_save_a3*4(k0)
        lw      s0,_exp_save_s0*4(k0)
        lw      s1,_exp_save_s1*4(k0)
        lw      s2,_exp_save_s2*4(k0)
        lw      s3,_exp_save_s3*4(k0)
        lw      s4,_exp_save_s4*4(k0)
        lw      s5,_exp_save_s5*4(k0)
        lw      s6,_exp_save_s6*4(k0)
        lw      s7,_exp_save_s7*4(k0)
        lw      v0,_exp_save_v0*4(k0)
        lw      v1,_exp_save_v1*4(k0)
        lw      AT,_exp_save_at*4(k0)
        lw      ra,_exp_save_ra*4(k0)

        lw      gp,_exp_save_gp*4(k0)
        lw      fp,_exp_save_fp*4(k0)
        lw      sp,_exp_save_sp*4(k0)

        nop;nop;nop;nop
        eret
        nop;nop;nop
        nop
        .set    at
        .end    gen_exception
##===============================================================


