	ncxlmode \
	-l verilog.log	\
#	+assert					\
#	+noassert_synth_pragma	\
	+ieee +pathpulse \
	+define+verbose_2 \
	+define+nobanner	\
	+libext+.udp+.v+.vmd \
	+pulse_r/100 +pulse_e/100 +no_pulse_msg \
	+nolibcell	\
	+notimingchecks		\
	+access+r+w+c \
	+mixedlang				\
	+nosdfwarn	\
	+debug \
	+ncredmem	\
	top_all.v +bus_conflict_off\
	+ncvlogargs+-NOMEMPACK \
	-y ${SYNOPSYS}/dw/sim_ver\
	+incdir+${SYNOPSYS}/dw/sim_ver \
	