############ CPU souce code ##########################	\
#+define+INSERT_SCAN_CPU					\
-v $rsrc_dir/cpu/cpu.vp						\
############### behavior model ###############		\
$rsrc_dir/hblk/cpu/U018SRA12X_1024X32M4.v       \
$rsrc_dir/hblk/cpu/U018SRA12X_128X24M4.v        \
$rsrc_dir/hblk/cpu/U018SRA12X_256X22M4.v        \
$rsrc_dir/hblk/cpu/U018SRA12X_256X64M4.v        \
$rsrc_dir/hblk/cpu/U018SRA12X_1024X32M8.v		\
$rsrc_dir/model/north/error_rpt.v					\
############### behavior model ###############		\
######### ICE behavior model						\
$rsrc_dir/model/ice/ice_test_all.vp               		\
