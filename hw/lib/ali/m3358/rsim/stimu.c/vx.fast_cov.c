	ncverilog  				\
	-l verilog.log			\
#	+assert					\
#	+noassert_synth_pragma	\
	+nccoverage \
	+linedebug \
	+tcl+cov.tcl \
	+ieee +pathpulse \
	+define+verbose_2 \
	+define+nobanner	\
	+libext+.udp+.v+.vmd \
	+pulse_r/0 +pulse_e/100 +no_pulse_msg \
	+notimingchecks		\
	+access+rwc \
	+loadpli1=fileio:fileio_register \
	+ncvlogargs+-NOMEMPACK \
	+bus_conflict_off\
	+mixedlang				\
	top_all.v 				\
	-y ${SYNOPSYS}/dw/sim_ver\
	+incdir+${SYNOPSYS}/dw/sim_ver \
