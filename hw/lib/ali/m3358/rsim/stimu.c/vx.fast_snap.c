	ncxlmode \
	-l verilog.log	\
	-r myworklib.mips_test:v\
	+tcl+mytcl.cmd\
	+ieee +pathpulse \
	+define+verbose_2 \
	+define+nobanner	\
	+pulse_r/0 +pulse_e/100 +no_pulse_msg \
	+nolibcell	\
	+notimingchecks		\
	+access+r \
	+mixedlang				\
	+loadpli1=fileio:fileio_register \
	+ncvlogargs+-NOMEMPACK \
	+bus_conflict_off
