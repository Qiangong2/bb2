verilog \
-c \
+incdir+$design_root/video_m3357/xilinx/verilog/src \
-y $design_root/video_m3357/xilinx/verilog/src/unisims +libext+.v \
-y $design_root/video_m3357/xilinx/verilog/src/simprims +libext+.v \
-y $design_root/video_m3357/xilinx/verilog/src/XilinxCoreLib		\
-y $design_root/video_m3357/synopsys/dw/sim_ver +libext+.v \
+loadpli=libpli:veriusertfs		\
-l verilog.log		\
+ieee +pathpulse \
+define+verbose_2 \
+define+nobanner	\
+pulse_r/0 +pulse_e/0 	\
+typdelays +logpsterr +logpllerr +hier	\
+nowarnTFNPC 				\
+nowarnLFSNN 				\
+nowarnTMREN 				\
+notimingchecks				\
+nosdfwarn				\
+debug		\
top_all.v	\
