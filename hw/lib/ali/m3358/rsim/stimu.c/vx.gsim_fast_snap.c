ncxlmode	\
-l verilog.log	\
-r myworklib.mips_test:v\
+tcl+mytcl.cmd\
+no_notifier \
+notchk \
+nosdfwarn \
+bus_conflict_off \
+sdf_verbose	\
+define+nobanner	\
+access+r\
+loadpli1=fileio:fileio_register \
+ncvlogargs+-NOMEMPACK \
+ieee +defparam +pulse_x/0 +pulse_r/0 +pathpulse +define+verbose_0
