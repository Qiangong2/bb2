	ncxlmode -c		 \
	-l verilog.log	\
	+ieee +pathpulse \
	+define+verbose_2 \
	+define+nobanner\
	+libext+.udp+.v+.vmd \
	+pulse_r/0 +pulse_e/100 +no_pulse_msg \
	+nolibcell	\
	+notimingchecks	\
	+mixedlang				\
	+loadpli1=fileio:fileio_register \
	+ncvlogargs+-NOMEMPACK \
    top_all.v +bus_conflict_off\
	-y ${SYNOPSYS}/dw/sim_ver\
	+incdir+${SYNOPSYS}/dw/sim_ver \
