ncxlmode	\
-l verilog.log	\
+no_notifier \
+libext+.v \
+nosdfwarn \
+ncredmem	\
+bus_conflict_off \
+sdf_verbose	\
+define+nobanner	\
+access+r\
+mixedlang				\
+ieee +defparam +pulse_x/0 +pulse_r/0 +pathpulse +define+verbose_0	\
+loadpli1=fileio:fileio_register \
+ncvlogargs+-NOMEMPACK \
-y ${SYNOPSYS}/dw/sim_ver \
+incdir+${SYNOPSYS}/dw/sim_ver \
+ntc_warn +neg_tchk			\
+ntc_verbose				\
+extend_tcheck_data_limit/500\
top_all.v	\
