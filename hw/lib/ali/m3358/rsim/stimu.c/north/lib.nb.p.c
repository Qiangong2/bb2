#########  cpu behavior model	#########	\
 $rsrc_dir/model/cpu/cpu_model_all.vp		\
#### Strap Pins behavior model #######	\
 $rsrc_dir/model/north/strap_pin_bh.v	\
#### North Bridge behavior module ####	\
 $rsrc_dir/model/north/mm.vp				\
 $rsrc_dir/model/north/tt.vp				\
 $rsrc_dir/model/north/int_bh.v			\
 $rsrc_dir/model/north/boot_rom.v		\
 $rsrc_dir/model/north/bh_373.v			\
 $rsrc_dir/model/north/dimm_bh.v  		\
 $rsrc_dir/model/north/mt48lc1m16a1.v  	\
 $rsrc_dir/model/north/mt48lc4m16a2.v  	\
 $rsrc_dir/model/north/mt48lc8m8a2.v  	\
 $rsrc_dir/model/north/mt48lc16m8a2_144.v  	\
 $rsrc_dir/model/north/monitor.v		\
#### The device behavior model for testing SDRAM memory interface ###			\
 $rsrc_dir/model/chipset/device_bh.v     										\
# $rsrc_dir/model/chipset/disp_device_bh.v     									\
#### North_Bridge synthesizable code start ##################					\
################# Memory controller	###############								\
 $rsrc_dir/north/sdram/mem_intf_all.vp                    							\
###### PCI bus BIU and Host	######		\
 $rsrc_dir/north/pbiu/p_biu.v			\
 $rsrc_dir/north/pbiu/p_host_all.vp			\
####  Local bus BIU and other/top module #########\
 $rsrc_dir/north/mbiu/nb_cpu_biu_all.vp		\
#### T2 chipset level	#########		\
 $rsrc_dir/chipset/t2_chipset.v		\
#### Hard block used by North or Chip #	\
 $rsrc_dir/hblk/chip/GATX20.v 		\
 $rsrc_dir/hblk/north/ram4x32d.v	\
 $rsrc_dir/hblk/north/ram8x32d1.v		\
 $rsrc_dir/hblk/north/ram8x32d4.v		\
 $rsrc_dir/hblk/north/ram32x32d4.v		\
 $rsrc_dir/hblk/north/ram16x32d1.v		\
#### for CPU and DE #####						\
 $rsrc_dir/hblk/cpu/DW01_add.v	    		\
 $rsrc_dir/hblk/cpu/DW01_sub.v	    		\
 $rsrc_dir/hblk/cpu/DW02_mult.v	    		\
#### clock generator #############				\
 $rsrc_dir/hblk/chip/CPU_PLL.v 					\
 $rsrc_dir/hblk/chip/MEM_PLL.v 					\
 $rsrc_dir/hblk/chip/F27_PLL.v 					\
 $rsrc_dir/hblk/chip/AUDIO_PLL.v				\
 $rsrc_dir/hblk/chip/DLYCELL02.v				\
 $rsrc_dir/hblk/chip/P_DLY_CHAIN32.v			\
 $rsrc_dir/hblk/chip/TEST_DLY_CHAIN64.v			\
 $rsrc_dir/hblk/chip/VIDEO_CLK_DLY_CHAIN.v		\
####  Top moudle for smulation					\
 $rsrc_dir/chip/usb_pad.v						\
 $rsrc_dir/chip/IOPAD.v							\
 $rsrc_dir/chip/PAD_RST_STRAP.v					\
 $rsrc_dir/chip/PAD_MISC.v						\
 $rsrc_dir/chip/core.v                 			\
 $rsrc_dir/chip/chip.v							\
 $rsrc_dir/chip/scan_mux.v						\
 $rsrc_dir/chip/clock_divider.v			\
 $rsrc_dir/chip/clock_gateway.v			\
 $rsrc_dir/chip/clock_gen.v				\
 $rsrc_dir/chip/AUDIO_CLOCK_GEN.v			\
 $rsrc_dir/chip/VIDEO_CLOCK_GEN.v			\
#### other 						\
-v $lib_dir/umc18/verilog/UMC018AG_ABSG_005.v	\
-v $lib_dir/umc18/verilog/UMC018AG_AGSG_005.v	\
-v $rsrc_dir/hblk/chip/SEQ_DUMMYJ.v				\
-v $rsrc_dir/hblk/chip/COMB_DUMMY.v				\
-v $lib_dir/umc18/verilog/UMC018AG_AASG_019.v	\
-v $lib_dir/umc18/verilog/UMC018AG_OEG_IODACADC_G_001.v   \
-v $lib_dir/umc18/verilog/UMC018AG_OBG_ALIIO_G_000.v

