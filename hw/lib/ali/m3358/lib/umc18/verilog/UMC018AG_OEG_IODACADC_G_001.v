/* ALi Library Version: lib_001 */
/* Export Time: 2003.12.16-13:37:30 */
/*$Id: UMC018AG_OEG_IODACADC_G_001.v,v 1.3 2004/06/25 01:00:38 whs Exp $*/
/*$Name:  $*/
/* Verilog Model Created from ECS Schematic analog_macro.sch -- Sep 23, 2003 22:48 */

module ANOLOG_MACRO( AVDDA_ADC, AVDDD_ADC, AVDDP_ADC, AVSSA_ADC, AVSSD_ADC, AVSSP_ADC,
                     CORE_GND, CORE_POW, DAC0CLK, DAC0DO, DAC1CLK, DAC1DO,
                     DAC2CLK, DAC2DO, DAC3CLK, DAC3DO, DAC4CLK, DAC4DO,
                     DAC5CLK, DAC5DO, DAC_BLK, DVDD, DVSS, GIO_POW, LP_0DAC,
                     LP_1DAC, LP_2DAC, LP_3DAC, LP_4DAC, LP_5DAC, MADCCLKP1_L,
                     MADCCLKP2_L, MLEFTCH, MODCLK, PD, PD_0DAC, PD_1DAC,
                     PD_2DAC, PD_3DAC, PD_4DAC, PD_5DAC, PI_VA_C, PO_VA_DR0,
                     PO_VA_DR1, PO_VA_I, PO_VA_OEN, PO_VA_PDJ, PO_VA_PU0,
                     PO_VA_PU1, PO_VA_PU2, PO_VA_S, SLEEP_DAC, TST_MOD, VDDA,
                     VSSA, VSSA1, VSUD, XADC_VREF, XAMIC1IN, XIDUMP, XIREF1,
                     XIREF2, XVDAC1OUT, XVDAC2OUT, XVDAC3OUT, XVDAC4OUT,
                     XVDAC5OUT, XVDAC6OUT, XVOUT_DMUX1, XVOUT_DMUX2,
                     XVOUT_DMUX3, XVOUT_DMUX4, XVOUT_DMUX5, XVOUT_DMUX6,
                     XVREXT, XVREXT1 );
 input AVDDA_ADC, AVDDD_ADC, AVDDP_ADC, AVSSA_ADC, AVSSD_ADC, AVSSP_ADC,
      CORE_GND, CORE_POW, DAC0CLK;
 input [11:0] DAC0DO;
 input DAC1CLK;
 input [11:0] DAC1DO;
 input DAC2CLK;
 input [11:0] DAC2DO;
 input DAC3CLK;
 input [11:0] DAC3DO;
 input DAC4CLK;
 input [11:0] DAC4DO;
 input DAC5CLK;
 input [11:0] DAC5DO;
 input [5:0] DAC_BLK;
 input DVDD, DVSS, GIO_POW, LP_0DAC, LP_1DAC, LP_2DAC, LP_3DAC, LP_4DAC,
      LP_5DAC;
output MADCCLKP1_L, MADCCLKP2_L, MLEFTCH;
 input MODCLK;
 input [2:0] PD;
 input PD_0DAC, PD_1DAC, PD_2DAC, PD_3DAC, PD_4DAC, PD_5DAC;
output [33:28] PI_VA_C;
 input [33:28] PO_VA_DR0;
 input [33:28] PO_VA_DR1;
 input [33:28] PO_VA_I;
 input [33:28] PO_VA_OEN;
 input [33:28] PO_VA_PDJ;
 input [33:28] PO_VA_PU0;
 input [33:28] PO_VA_PU1;
 input [33:28] PO_VA_PU2;
 input [33:28] PO_VA_S;
 input SLEEP_DAC;
 input [3:0] TST_MOD;
 input VDDA, VSSA, VSSA1, VSUD;
 inout XADC_VREF, XAMIC1IN, XIDUMP, XIREF1, XIREF2, XVDAC1OUT, XVDAC2OUT,
      XVDAC3OUT, XVDAC4OUT, XVDAC5OUT, XVDAC6OUT, XVOUT_DMUX1, XVOUT_DMUX2,
      XVOUT_DMUX3, XVOUT_DMUX4, XVOUT_DMUX5, XVOUT_DMUX6, XVREXT, XVREXT1;
wire [10:0] PIO_VAA_ANAO;
wire PIO_AVA_ANAI;
wire PIO_AVA_ANA0;
wire I_50;

analog_vp_io I_ANALOG_VP_IO ( .AVDDP_ADC(AVDDP_ADC), .AVSSP_ADC(AVSSP_ADC),
                           .CORE_GND(CORE_GND), .CORE_POW(CORE_POW),
                           .PI_VA_C(PI_VA_C[33:28]),
                           .PIO_DMUX1(XVOUT_DMUX1), .PIO_DMUX2(XVOUT_DMUX2),
                           .PIO_DMUX3(XVOUT_DMUX3), .PIO_DMUX4(XVOUT_DMUX4),
                           .PIO_DMUX5(XVOUT_DMUX5), .PIO_DMUX6(XVOUT_DMUX6),
                           .PIO_IDUMP(XIDUMP), .PIO_IREF1(XIREF1),
                           .PIO_IREF2(XIREF2),
                           .PIO_VAA_ANAO(PIO_VAA_ANAO[10:0]),
                           .PIO_VDAC1OUT(XVDAC1OUT),
                           .PIO_VDAC2OUT(XVDAC2OUT),
                           .PIO_VDAC3OUT(XVDAC3OUT),
                           .PIO_VDAC4OUT(XVDAC4OUT),
                           .PIO_VDAC5OUT(XVDAC5OUT),
                           .PIO_VDAC6OUT(XVDAC6OUT), .PIO_VREXT(XVREXT),
                           .PIO_VREXT1(XVREXT1),
                           .PO_VA_DR0(PO_VA_DR0[33:28]),
                           .PO_VA_DR1(PO_VA_DR1[33:28]),
                           .PO_VA_I(PO_VA_I[33:28]),
                           .PO_VA_OEN(PO_VA_OEN[33:28]),
                           .PO_VA_PDJ(PO_VA_PDJ[33:28]),
                           .PO_VA_PU0(PO_VA_PU0[33:28]),
                           .PO_VA_PU1(PO_VA_PU1[33:28]),
                           .PO_VA_PU2(PO_VA_PU2[33:28]),
                           .PO_VA_S(PO_VA_S[33:28]), .VDDA(VDDA),
                           .VSSA(VSSA), .VSSA1(VSSA1), .VSUD(VSUD) );
analog_au_io I_ANALOG_AU_IO ( .AVDDA_ADC(AVDDA_ADC), .AVDDD_ADC(AVDDD_ADC),
                           .AVDDP_ADC(AVDDP_ADC), .AVSSA_ADC(AVSSA_ADC),
                           .AVSSD_ADC(AVSSD_ADC), .AVSSP_ADC(AVSSP_ADC),
                           .PIO_ADC_VREF(XADC_VREF), .PIO_AMIC1IN(XAMIC1IN),
                           .PIO_AVA_ANAI(PIO_AVA_ANAI),
                           .PIO_AVA_ANAO(PIO_AVA_ANA0) );
MOD_TOP I_MOD_TOP ( .AVDDA(AVDDA_ADC), .AVDDD(AVDDD_ADC), .AVSSA(AVSSA_ADC),
                 .AVSSD(AVSSD_ADC), .CK1(MODCLK), .CLK1P1(MADCCLKP1_L),
                 .CLK1P2(MADCCLKP2_L), .DVDD(DVDD), .DVSS(DVSS), .I50UN(I_50),
                 .PD1(PD[1]), .PD_CLK(PD[2]), .PD_REF(PD[0]), .Q1(MLEFTCH),
                 .TST_MOD(TST_MOD[3:0]), .VCM(PIO_AVA_ANA0),
                 .VI1(PIO_AVA_ANAI) );
DAC12_6CH I_DAC12_6CH ( .BLK1(DAC_BLK[0]), .BLK2(DAC_BLK[1]), .BLK3(DAC_BLK[2]),
                     .BLK4(DAC_BLK[3]), .BLK5(DAC_BLK[4]), .BLK6(DAC_BLK[5]),
                     .CLK1(DAC0CLK), .CLK2(DAC1CLK), .CLK3(DAC2CLK),
                     .CLK4(DAC3CLK), .CLK5(DAC4CLK), .CLK6(DAC5CLK),
                     .D1(DAC0DO[11:0]), .D2(DAC1DO[11:0]), .D3(DAC2DO[11:0]),
                     .D4(DAC3DO[11:0]), .D5(DAC4DO[11:0]), .D6(DAC5DO[11:0]),
                     .GNDA(VSSA), .GNDA1(VSSA1), .GNDD(CORE_GND), .I_50(I_50),
                     .IOUT1(PIO_VAA_ANAO[0]), .IOUT2(PIO_VAA_ANAO[1]),
                     .IOUT3(PIO_VAA_ANAO[2]), .IOUT4(PIO_VAA_ANAO[3]),
                     .IOUT5(PIO_VAA_ANAO[4]), .IOUT6(PIO_VAA_ANAO[5]),
                     .IOUTB(PIO_VAA_ANAO[10]), .IREF1(PIO_VAA_ANAO[8]),
                     .IREF3(PIO_VAA_ANAO[9]), .LP1(LP_0DAC), .LP2(LP_1DAC),
                     .LP3(LP_2DAC), .LP4(LP_3DAC), .LP5(LP_4DAC),
                     .LP6(LP_5DAC), .PD1(PD_0DAC), .PD2(PD_1DAC),
                     .PD3(PD_2DAC), .PD4(PD_3DAC), .PD5(PD_4DAC),
                     .PD6(PD_5DAC), .REXT(PIO_VAA_ANAO[6]),
                     .REXT1(PIO_VAA_ANAO[7]), .SLEEP(SLEEP_DAC),
                     .VD18D(CORE_POW), .VD33A(VDDA), .VD33D(GIO_POW),
                     .VSUD(VSUD) );

endmodule // analog_macro
