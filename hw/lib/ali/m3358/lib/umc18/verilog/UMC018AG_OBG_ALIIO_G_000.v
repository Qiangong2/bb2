/* ALi Library Version: lib_000 */
/* Export Time: 2003.12.26-14:46:01 */
/*$Id: UMC018AG_OBG_ALIIO_G_000.v,v 1.3 2004/06/25 01:00:38 whs Exp $*/
/*$Name:  $*/


// type: BANAx
`celldefine
module BANA1  (PAD, C,VD33, VSSPST, VSS );
    input VD33;
    buf I1 (VD33_buf, VD33);
    input VSSPST;
    buf I2 (VSSPST_buf, VSSPST);
    input VSS;
    buf I3 (VSS_buf, VSS);
   inout PAD;
   inout C;

   tran (PAD,C);

endmodule
`endcelldefine
/*$Id: UMC018AG_OBG_ALIIO_G_000.v,v 1.3 2004/06/25 01:00:38 whs Exp $*/
/*$Name:  $*/


// type: BANAx
`celldefine
module BANA1P  (PAD, C,TAVDD, TAVSS );
    input TAVDD;
    buf I1 (TAVDD_buf, TAVDD);
    input TAVSS;
    buf I2 (TAVSS_buf, TAVSS);
   inout PAD;
   inout C;

   tran (PAD,C);

endmodule
`endcelldefine
/*$Id: UMC018AG_OBG_ALIIO_G_000.v,v 1.3 2004/06/25 01:00:38 whs Exp $*/
/*$Name:  $*/


// type: BANAx
`celldefine
module BANA1P1  (PAD, C,TAVD33, TAVSSPST, TAVSS );
    input TAVD33;
    buf I1 (TAVD33_buf, TAVD33);
    input TAVSSPST;
    buf I2 (TAVSSPST_buf, TAVSSPST);
    input TAVSS;
    buf I3 (TAVSS_buf, TAVSS);
   inout PAD;
   inout C;

   tran (PAD,C);

endmodule
`endcelldefine
/*$Id: UMC018AG_OBG_ALIIO_G_000.v,v 1.3 2004/06/25 01:00:38 whs Exp $*/
/*$Name:  $*/


// type: BANAx
`celldefine
module BANA2  (PAD, C,VD33, VSSPST );
    input VD33;
    buf I1 (VD33_buf, VD33);
    input VSSPST;
    buf I2 (VSSPST_buf, VSSPST);
   inout PAD;
   inout C;

   tran (PAD,C);

endmodule
`endcelldefine
/*$Id: UMC018AG_OBG_ALIIO_G_000.v,v 1.3 2004/06/25 01:00:38 whs Exp $*/
/*$Name:  $*/


// type: BANAx
`celldefine
module BANA2P  (PAD, C,TAVDD, TAVSS );
    input TAVDD;
    buf I1 (TAVDD_buf, TAVDD);
    input TAVSS;
    buf I2 (TAVSS_buf, TAVSS);
   inout PAD;
   inout C;

   tran (PAD,C);

endmodule
`endcelldefine
/*$Id: UMC018AG_OBG_ALIIO_G_000.v,v 1.3 2004/06/25 01:00:38 whs Exp $*/
/*$Name:  $*/


// type: BANAx
`celldefine
module BANA2P1  (PAD, C,TAVD33, TAVSSPST );
    input TAVD33;
    buf I1 (TAVD33_buf, TAVD33);
    input TAVSSPST;
    buf I2 (TAVSSPST_buf, TAVSSPST);
   inout PAD;
   inout C;

   tran (PAD,C);

endmodule
`endcelldefine
/*$Id: UMC018AG_OBG_ALIIO_G_000.v,v 1.3 2004/06/25 01:00:38 whs Exp $*/
/*$Name:  $*/


// type: BANAx
`celldefine
module BANA2PB  (PAD, C,TAVDDB, TAVSSB );
    input TAVDDB;
    buf I1 (TAVDDB_buf, TAVDDB);
    input TAVSSB;
    buf I2 (TAVSSB_buf, TAVSSB);
   inout PAD;
   inout C;

   tran (PAD,C);

endmodule
`endcelldefine
/*$Id: UMC018AG_OBG_ALIIO_G_000.v,v 1.3 2004/06/25 01:00:38 whs Exp $*/
/*$Name:  $*/
`timescale 1ns / 10ps
`celldefine

module BIOHVU1 (PAD, C, I, OEN, PE, PS, RHL,IE, IT, DS0, DS1, VDD,VD33,VSS,VSSPST);
inout  PAD;
output C;
input  I, OEN, PE, PS, RHL,IE, IT, DS0, DS1;
input VDD,VD33,VSS,VSSPST;

bufif0 (pad_i,I,OEN);
bufif1 (weak0,weak1) (pad_i,PS,PE);
nmos (PAD, pad_i, 1);
buf (C,c_i);
assign c_i = (IE) ? PAD : 1'b0;

  specify
    specparam tNull=1.0;
    specparam tD=1.0;


    if ( IT === 1'b1 )
      ( IE => C ) = (tD,tD) ;
    if ( IT === 1'b0 )
      ( IE => C ) = (tD,tD) ;

    ifnone
      ( PE => PAD ) = (tNull,tNull,tD,tNull,tD,tNull) ;
    if ( PS === 1'b0 && RHL === 1'b0 )
      ( PE => PAD ) = (tNull,tNull,tD,tNull,tNull,tNull) ;
    if ( PS === 1'b1 && RHL === 1'b0 )
      ( PE => PAD ) = (tNull,tNull,tNull,tNull,tD,tNull) ;

    if ( PE === 1'b1 && DS1 === 1'b0 && DS0 === 1'b0 && PS === 1'b1 && RHL === 1'b0 )
      ( I => PAD ) = (tD,tD) ;
    if ( PE === 1'b0 && DS1 === 1'b1 && DS0 === 1'b1 )
      ( I => PAD ) = (tD,tD) ;
    if ( PE === 1'b0 && DS1 === 1'b1 && DS0 === 1'b0 )
      ( I => PAD ) = (tD,tD) ;
    if ( PE === 1'b0 && DS1 === 1'b0 && DS0 === 1'b1 )
      ( I => PAD ) = (tD,tD) ;
    if ( PE === 1'b0 && DS1 === 1'b0 && DS0 === 1'b0 )
      ( I => PAD ) = (tD,tD) ;
    if ( PE === 1'b1 && DS1 === 1'b1 && DS0 === 1'b1 && PS === 1'b0 && RHL === 1'b1 )
      ( I => PAD ) = (tD,tD) ;
    if ( PE === 1'b1 && DS1 === 1'b1 && DS0 === 1'b1 && PS === 1'b0 && RHL === 1'b0 )
      ( I => PAD ) = (tD,tD) ;
    if ( PE === 1'b1 && DS1 === 1'b1 && DS0 === 1'b1 && PS === 1'b1 && RHL === 1'b1 )
      ( I => PAD ) = (tD,tD) ;
    if ( PE === 1'b1 && DS1 === 1'b1 && DS0 === 1'b1 && PS === 1'b1 && RHL === 1'b0 )
      ( I => PAD ) = (tD,tD) ;
    if ( PE === 1'b1 && DS1 === 1'b1 && DS0 === 1'b0 && PS === 1'b0 && RHL === 1'b1 )
      ( I => PAD ) = (tD,tD) ;
    if ( PE === 1'b1 && DS1 === 1'b1 && DS0 === 1'b0 && PS === 1'b0 && RHL === 1'b0 )
      ( I => PAD ) = (tD,tD) ;
    if ( PE === 1'b1 && DS1 === 1'b1 && DS0 === 1'b0 && PS === 1'b1 && RHL === 1'b1 )
      ( I => PAD ) = (tD,tD) ;
    if ( PE === 1'b1 && DS1 === 1'b1 && DS0 === 1'b0 && PS === 1'b1 && RHL === 1'b0 )
      ( I => PAD ) = (tD,tD) ;
    if ( PE === 1'b1 && DS1 === 1'b0 && DS0 === 1'b1 && PS === 1'b0 && RHL === 1'b1 )
      ( I => PAD ) = (tD,tD) ;
    if ( PE === 1'b1 && DS1 === 1'b0 && DS0 === 1'b1 && PS === 1'b0 && RHL === 1'b0 )
      ( I => PAD ) = (tD,tD) ;
    if ( PE === 1'b1 && DS1 === 1'b0 && DS0 === 1'b1 && PS === 1'b1 && RHL === 1'b1 )
      ( I => PAD ) = (tD,tD) ;
    if ( PE === 1'b1 && DS1 === 1'b0 && DS0 === 1'b1 && PS === 1'b1 && RHL === 1'b0 )
      ( I => PAD ) = (tD,tD) ;
    if ( PE === 1'b1 && DS1 === 1'b0 && DS0 === 1'b0 && PS === 1'b0 && RHL === 1'b1 )
      ( I => PAD ) = (tD,tD) ;
    if ( PE === 1'b1 && DS1 === 1'b0 && DS0 === 1'b0 && PS === 1'b0 && RHL === 1'b0 )
      ( I => PAD ) = (tD,tD) ;
    if ( PE === 1'b1 && DS1 === 1'b0 && DS0 === 1'b0 && PS === 1'b1 && RHL === 1'b1 )
      ( I => PAD ) = (tD,tD) ;

    if ( DS1 === 1'b1 && DS0 === 1'b1 )
      ( OEN => PAD ) = (tNull,tNull,tD,tD,tD,tD) ;
    if ( DS1 === 1'b0 && DS0 === 1'b0 )
      ( OEN => PAD ) = (tNull,tNull,tD,tD,tD,tD) ;
    if ( DS1 === 1'b0 && DS0 === 1'b1 )
      ( OEN => PAD ) = (tNull,tNull,tD,tD,tD,tD) ;
    if ( DS1 === 1'b1 && DS0 === 1'b0 )
      ( OEN => PAD ) = (tNull,tNull,tD,tD,tD,tD) ;

    if ( IT === 1'b1 )
      ( PAD => C ) = (tD,tD) ;
    if ( IT === 1'b0 )
      ( PAD => C ) = (tD,tD) ;

  endspecify
endmodule

`endcelldefine
/*$Id: UMC018AG_OBG_ALIIO_G_000.v,v 1.3 2004/06/25 01:00:38 whs Exp $*/
/*$Name:  $*/
`timescale 1ns / 10ps
`celldefine
module BIORU1 (I, OEN, PE, PS, IE, IT, RHL, DS0, DS1, C, PAD, CA, VDD,VD33,VSS,VSSPST);
input  I, OEN, PE, PS, IE, IT, RHL, DS0, DS1;
output C;
inout  PAD;
inout CA;
input VDD,VD33,VSS,VSSPST;

bufif0 (pad_i,I,OEN);
bufif1 (weak0,weak1) (pad_i,PS,PE);
nmos (PAD,pad_i,1);
nmos (CA,ca_i,1);
buf (C,c_i);
assign ca_i = PAD;
assign c_i = (IE) ? PAD : 1'b0;

  specify
    specparam tNull=1.0;
    specparam tD=1.0;
//    specparam tD=0.0;


    if ( IT === 1'b1 )
     ( IE => C ) = (tD,tD) ;
    if ( IT === 1'b0 )
      ( IE => C ) = (tD,tD) ;

    ifnone
      ( PE => PAD ) = (tNull,tNull,tD,tNull,tD,tNull) ;
    if ( PS === 1'b0 && RHL === 1'b0 )
      ( PE => PAD ) = (tNull,tNull,tD,tNull,tNull,tNull) ;
    if ( PS === 1'b1 && RHL === 1'b0 )
      ( PE => PAD ) = (tNull,tNull,tNull,tNull,tD,tNull) ;

    if ( PE === 1'b1 && DS1 === 1'b0 && DS0 === 1'b0 && PS === 1'b1 && RHL === 1'b0 )
      ( I => PAD ) = (tD,tD) ;
    if ( PE === 1'b0 && DS1 === 1'b1 && DS0 === 1'b1 )
      ( I => PAD ) = (tD,tD) ;
    if ( PE === 1'b0 && DS1 === 1'b1 && DS0 === 1'b0 )
      ( I => PAD ) = (tD,tD) ;
    if ( PE === 1'b0 && DS1 === 1'b0 && DS0 === 1'b1 )
      ( I => PAD ) = (tD,tD) ;
    if ( PE === 1'b0 && DS1 === 1'b0 && DS0 === 1'b0 )
      ( I => PAD ) = (tD,tD) ;
    if ( PE === 1'b1 && DS1 === 1'b1 && DS0 === 1'b1 && PS === 1'b0 && RHL === 1'b1 )
      ( I => PAD ) = (tD,tD) ;
    if ( PE === 1'b1 && DS1 === 1'b1 && DS0 === 1'b1 && PS === 1'b0 && RHL === 1'b0 )
      ( I => PAD ) = (tD,tD) ;
    if ( PE === 1'b1 && DS1 === 1'b1 && DS0 === 1'b1 && PS === 1'b1 && RHL === 1'b1 )
      ( I => PAD ) = (tD,tD) ;
    if ( PE === 1'b1 && DS1 === 1'b1 && DS0 === 1'b1 && PS === 1'b1 && RHL === 1'b0 )
      ( I => PAD ) = (tD,tD) ;
    if ( PE === 1'b1 && DS1 === 1'b1 && DS0 === 1'b0 && PS === 1'b0 && RHL === 1'b1 )
      ( I => PAD ) = (tD,tD) ;
    if ( PE === 1'b1 && DS1 === 1'b1 && DS0 === 1'b0 && PS === 1'b0 && RHL === 1'b0 )
      ( I => PAD ) = (tD,tD) ;
    if ( PE === 1'b1 && DS1 === 1'b1 && DS0 === 1'b0 && PS === 1'b1 && RHL === 1'b1 )
      ( I => PAD ) = (tD,tD) ;
    if ( PE === 1'b1 && DS1 === 1'b1 && DS0 === 1'b0 && PS === 1'b1 && RHL === 1'b0 )
      ( I => PAD ) = (tD,tD) ;
    if ( PE === 1'b1 && DS1 === 1'b0 && DS0 === 1'b1 && PS === 1'b0 && RHL === 1'b1 )
      ( I => PAD ) = (tD,tD) ;
    if ( PE === 1'b1 && DS1 === 1'b0 && DS0 === 1'b1 && PS === 1'b0 && RHL === 1'b0 )
      ( I => PAD ) = (tD,tD) ;
    if ( PE === 1'b1 && DS1 === 1'b0 && DS0 === 1'b1 && PS === 1'b1 && RHL === 1'b1 )
      ( I => PAD ) = (tD,tD) ;
    if ( PE === 1'b1 && DS1 === 1'b0 && DS0 === 1'b1 && PS === 1'b1 && RHL === 1'b0 )
      ( I => PAD ) = (tD,tD) ;
    if ( PE === 1'b1 && DS1 === 1'b0 && DS0 === 1'b0 && PS === 1'b0 && RHL === 1'b1 )
      ( I => PAD ) = (tD,tD) ;
    if ( PE === 1'b1 && DS1 === 1'b0 && DS0 === 1'b0 && PS === 1'b0 && RHL === 1'b0 )
      ( I => PAD ) = (tD,tD) ;
    if ( PE === 1'b1 && DS1 === 1'b0 && DS0 === 1'b0 && PS === 1'b1 && RHL === 1'b1 )
      ( I => PAD ) = (tD,tD) ;

    if ( DS1 === 1'b1 && DS0 === 1'b1 )
      ( OEN => PAD ) = (tNull,tNull,tD,tD,tD,tD) ;
    if ( DS1 === 1'b0 && DS0 === 1'b0 )
      ( OEN => PAD ) = (tNull,tNull,tD,tD,tD,tD) ;
    if ( DS1 === 1'b0 && DS0 === 1'b1 )
      ( OEN => PAD ) = (tNull,tNull,tD,tD,tD,tD) ;
    if ( DS1 === 1'b1 && DS0 === 1'b0 )
      ( OEN => PAD ) = (tNull,tNull,tD,tD,tD,tD) ;

    if ( IT === 1'b1 )
      ( PAD => C ) = (tD,tD) ;
    if ( IT === 1'b0 )
      ( PAD => C ) = (tD,tD) ;

  endspecify
endmodule
`endcelldefine
/*$Id: UMC018AG_OBG_ALIIO_G_000.v,v 1.3 2004/06/25 01:00:38 whs Exp $*/
/*$Name:  $*/


// type: BOSCXE
`celldefine
module BOSC3E  (XC, XOUT, XIN, E,VD33, VDD, VSSPST, VSS );
    input VD33;
    buf I1 (VD33_buf, VD33);
    input VDD;
    buf I2 (VDD_buf, VDD);
    input VSSPST;
    buf I3 (VSSPST_buf, VSSPST);
    input VSS;
    buf I4 (VSS_buf, VSS);
    input XIN, E;
    output XC, XOUT;
    not                  (XC, XOUT);
    nand                 (XOUT, E, XIN);
    specify
       (E => XC)=(0, 0);
       (E => XOUT)=(0, 0);
       (XIN => XC)=(0, 0);
       (XIN => XOUT)=(0, 0);
    endspecify
endmodule
`endcelldefine
/*$Id: UMC018AG_OBG_ALIIO_G_000.v,v 1.3 2004/06/25 01:00:38 whs Exp $*/
/*$Name:  $*/


// type: PNOPIN
`celldefine
module PCUT1  (VSS1, VSS2 );
    input VSS1;
    buf I1 (VSS1_buf, VSS1);
    input VSS2;
    buf I2 (VSS2_buf, VSS2);
endmodule
`endcelldefine
/*$Id: UMC018AG_OBG_ALIIO_G_000.v,v 1.3 2004/06/25 01:00:38 whs Exp $*/
/*$Name:  $*/


// type: PNOPIN
`celldefine
module PCUT2  (VDD1, VDD2, VSS1, VSS2 );
    input VDD1;
    buf I1 (VDD1_buf, VDD1);
    input VDD2;
    buf I2 (VDD2_buf, VDD2);
    input VSS1;
    buf I3 (VSS1_buf, VSS1);
    input VSS2;
    buf I4 (VSS2_buf, VSS2);
endmodule
`endcelldefine
/*$Id: UMC018AG_OBG_ALIIO_G_000.v,v 1.3 2004/06/25 01:00:38 whs Exp $*/
/*$Name:  $*/


// type: VDD1A
`celldefine
module VDD1A  (TVDD1A,VD33, VSSPST );
    input VD33;
    buf I1 (VD33_buf, VD33);
    input VSSPST;
    buf I3 (VSSPST_buf, VSSPST);
    input   TVDD1A;
    supply1 TVDD1A;
endmodule
`endcelldefine
/*$Id: UMC018AG_OBG_ALIIO_G_000.v,v 1.3 2004/06/25 01:00:38 whs Exp $*/
/*$Name:  $*/


// type: VDDx
`celldefine
module VDD1D  (VDD,VD33, VSSPST );
    input VD33;
    buf I1 (VD33_buf, VD33);
    input VSSPST;
    buf I3 (VSSPST_buf, VSSPST);
    input   VDD;
    supply1 VDD;
endmodule
`endcelldefine
/*$Id: UMC018AG_OBG_ALIIO_G_000.v,v 1.3 2004/06/25 01:00:38 whs Exp $*/
/*$Name:  $*/


// type: VDD1P
`celldefine
module VDD1P  (TVDD1P,TAVDD, TAVSS );
    input TAVDD;
    buf I1 (TAVDD_buf, TAVDD);
    input TAVSS;
    buf I3 (TAVSS_buf, TAVSS);
    input   TVDD1P;
    supply1 TVDD1P;
endmodule
`endcelldefine
/*$Id: UMC018AG_OBG_ALIIO_G_000.v,v 1.3 2004/06/25 01:00:38 whs Exp $*/
/*$Name:  $*/


// type: VDD1P1
`celldefine
module VDD1P1  (TVDD1P1,TAVD33, TAVSSPST );
    input TAVD33;
    buf I1 (TAVD33_buf, TAVD33);
    input TAVSSPST;
    buf I3 (TAVSSPST_buf, TAVSSPST);
    input   TVDD1P1;
    supply1 TVDD1P1;
endmodule
`endcelldefine
/*$Id: UMC018AG_OBG_ALIIO_G_000.v,v 1.3 2004/06/25 01:00:38 whs Exp $*/
/*$Name:  $*/


// type: VDD1PB
`celldefine
module VDD1PB  (TVDD1PB,TAVDD, TAVSS );
    input TAVDD;
    buf I1 (TAVDD_buf, TAVDD);
    input TAVSS;
    buf I3 (TAVSS_buf, TAVSS);
    input   TVDD1PB;
    supply1 TVDD1PB;
endmodule
`endcelldefine
/*$Id: UMC018AG_OBG_ALIIO_G_000.v,v 1.3 2004/06/25 01:00:38 whs Exp $*/
/*$Name:  $*/


// type: PNOPIN
`celldefine
module VDD2D  (VD33, VSSPST );
    input VD33;
    buf I1 (VD33_buf, VD33);
    input VSSPST;
    buf I2 (VSSPST_buf, VSSPST);
endmodule
`endcelldefine
/*$Id: UMC018AG_OBG_ALIIO_G_000.v,v 1.3 2004/06/25 01:00:38 whs Exp $*/
/*$Name:  $*/


// type: PNOPIN
`celldefine
module VDD2P  (TAVD33, TAVSSPST );
    input TAVD33;
    buf I1 (TAVD33_buf, TAVD33);
    input TAVSSPST;
    buf I2 (TAVSSPST_buf, TAVSSPST);
endmodule
`endcelldefine
/*$Id: UMC018AG_OBG_ALIIO_G_000.v,v 1.3 2004/06/25 01:00:38 whs Exp $*/
/*$Name:  $*/


// type: VDD3P
`celldefine
module VDD3P  (TAVDD,TAVSS );
    input TAVSS;
    buf I2 (TAVSS_buf, TAVSS);
    input   TAVDD;
    supply1 TAVDD;
endmodule
`endcelldefine
/*$Id: UMC018AG_OBG_ALIIO_G_000.v,v 1.3 2004/06/25 01:00:38 whs Exp $*/
/*$Name:  $*/


// type: PNOPIN
`celldefine
module VDD4D  (VD33, VSSPST );
    input VD33;
    supply1 VD33;
    input VSSPST;
    buf I2 (VSSPST_buf, VSSPST);
endmodule
`endcelldefine
/*$Id: UMC018AG_OBG_ALIIO_G_000.v,v 1.3 2004/06/25 01:00:38 whs Exp $*/
/*$Name:  $*/


// type: PNOPIN
`celldefine
module VDD4P  (TAVD33, TAVDD, TAVSSPST );
    input TAVD33;
    buf I1 (TAVD33_buf, TAVD33);
    input TAVDD;
    buf I2 (TAVDD_buf, TAVDD);
    input TAVSSPST;
    buf I3 (TAVSSPST_buf, TAVSSPST);
endmodule
`endcelldefine
/*$Id: UMC018AG_OBG_ALIIO_G_000.v,v 1.3 2004/06/25 01:00:38 whs Exp $*/
/*$Name:  $*/


// type: PNOPIN
`celldefine
module VDD5P  (TAVDD, TAVSS );
    input TAVDD;
    buf I1 (TAVDD_buf, TAVDD);
    input TAVSS;
    buf I2 (TAVSS_buf, TAVSS);
endmodule
`endcelldefine
/*$Id: UMC018AG_OBG_ALIIO_G_000.v,v 1.3 2004/06/25 01:00:38 whs Exp $*/
/*$Name:  $*/


// type: VSS1A
`celldefine
module VSS1A  (TVSS1A,VD33, VSSPST, );
    input VD33;
    buf I1 (VD33_buf, VD33);
    input VSSPST;
    buf I2 (VSSPST_buf, VSSPST);
    input   TVSS1A;
    supply0 TVSS1A;
endmodule
`endcelldefine
/*$Id: UMC018AG_OBG_ALIIO_G_000.v,v 1.3 2004/06/25 01:00:38 whs Exp $*/
/*$Name:  $*/


// type: VSSx
`celldefine
module VSS1D  (VSS,VD33, VSSPST, );
    input VD33;
    buf I1 (VD33_buf, VD33);
    input VSSPST;
    buf I2 (VSSPST_buf, VSSPST);
    input   VSS;
    supply0 VSS;
endmodule
`endcelldefine
/*$Id: UMC018AG_OBG_ALIIO_G_000.v,v 1.3 2004/06/25 01:00:38 whs Exp $*/
/*$Name:  $*/


// type: VSS1P
`celldefine
module VSS1P  (TVSS1P,TAVDD, TAVSS, );
    input TAVDD;
    buf I1 (TAVDD_buf, TAVDD);
    input TAVSS;
    buf I2 (TAVSS_buf, TAVSS);
    input   TVSS1P;
    supply0 TVSS1P;
endmodule
`endcelldefine
/*$Id: UMC018AG_OBG_ALIIO_G_000.v,v 1.3 2004/06/25 01:00:38 whs Exp $*/
/*$Name:  $*/


// type: VSS1P1
`celldefine
module VSS1P1  (TVSS1P1,TAVD33, TAVSSPST, );
    input TAVD33;
    buf I1 (TAVD33_buf, TAVD33);
    input TAVSSPST;
    buf I2 (TAVSSPST_buf, TAVSSPST);
    input   TVSS1P1;
    supply0 TVSS1P1;
endmodule
`endcelldefine
/*$Id: UMC018AG_OBG_ALIIO_G_000.v,v 1.3 2004/06/25 01:00:38 whs Exp $*/
/*$Name:  $*/


// type: PNOPIN
`celldefine
module VSS2D  (VD33, VSSPST );
    input VD33;
    buf I1 (VD33_buf, VD33);
    input VSSPST;
    buf I2 (VSSPST_buf, VSSPST);
endmodule
`endcelldefine
/*$Id: UMC018AG_OBG_ALIIO_G_000.v,v 1.3 2004/06/25 01:00:38 whs Exp $*/
/*$Name:  $*/


// type: PNOPIN
`celldefine
module VSS2P  (TAVD33, TAVSSPST );
    input TAVD33;
    buf I1 (TAVD33_buf, TAVD33);
    input TAVSSPST;
    buf I2 (TAVSSPST_buf, TAVSSPST);
endmodule
`endcelldefine
/*$Id: UMC018AG_OBG_ALIIO_G_000.v,v 1.3 2004/06/25 01:00:38 whs Exp $*/
/*$Name:  $*/


// type: VSSx
`celldefine
module VSS3D  (VSS,VD33, );
    input VD33;
    buf I1 (VD33_buf, VD33);
    input   VSS;
    supply0 VSS;
endmodule
`endcelldefine
/*$Id: UMC018AG_OBG_ALIIO_G_000.v,v 1.3 2004/06/25 01:00:38 whs Exp $*/
/*$Name:  $*/


// type: VSS3P
`celldefine
module VSS3P  (TAVSS,TAVDD, );
    input TAVDD;
    buf I1 (TAVDD_buf, TAVDD);
    input   TAVSS;
    supply0 TAVSS;
endmodule
`endcelldefine
/*$Id: UMC018AG_OBG_ALIIO_G_000.v,v 1.3 2004/06/25 01:00:38 whs Exp $*/
/*$Name:  $*/


// type: PNOPIN
`celldefine
module VSS4P  (TAVD33, TAVSSPST, TAVSS );
    input TAVD33;
    buf I1 (TAVD33_buf, TAVD33);
    input TAVSSPST;
    buf I2 (TAVSSPST_buf, TAVSSPST);
    input TAVSS;
    buf I3 (TAVSS_buf, TAVSS);
endmodule
`endcelldefine
/*$Id: UMC018AG_OBG_ALIIO_G_000.v,v 1.3 2004/06/25 01:00:38 whs Exp $*/
/*$Name:  $*/


// type: PNOPIN
`celldefine
module VSS5P  (TAVDD, TAVSS );
    input TAVDD;
    buf I1 (TAVDD_buf, TAVDD);
    input TAVSS;
    buf I2 (TAVSS_buf, TAVSS);
endmodule
`endcelldefine
