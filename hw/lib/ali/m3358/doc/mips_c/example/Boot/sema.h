/*---------------------------------------------------*/
/* the area to exchange message between pc and board */
/*---------------------------------------------------*/

#define DAT_EXCHG_AREA	0xa0700000

#define IRQ_COUNTER	(DAT_EXCHG_AREA+0x0)
#define SEMAPHORE	(DAT_EXCHG_AREA+0x4)

/* preserve 16 bytes for semephore data area */
#define SEMA_DAT_AREA	(DAT_EXCHG_AREA+0x8)

/* new compare register's value
   -- to be load into compare register in timer interrupt handler
   -- and it can be modified dynamically by PC application */
#define COMP_FLAG	(DAT_EXCHG_AREA+0x18)
#define INI_COMP	0x20000		#should be modified to a suitable value


#define	REG_SAVE_ADDR	(DAT_EXCHG_AREA+0x20)	/*registers save area of Execption*/
#define GTE_REG_ADDR	(DAT_EXCHG_AREA+0x24)	/*my emulation of gte registers */

#define FLUSH_CACHE	(DAT_EXCHG_AREA+0x30)	/*fuctions of MiniOS*/
#define RAM_CACHEABLE	(DAT_EXCHG_AREA+0x34)

#define PSX_INT_HDLR	(DAT_EXCHG_AREA+0x40)	/*entries of Duddie's fuctions */
#define SYSCALL_HDLR	(DAT_EXCHG_AREA+0x44)
#define COP3_HDLR	(DAT_EXCHG_AREA+0x48)
#define TLB_HDLR	(DAT_EXCHG_AREA+0x4C)

#define MessageBox	(DAT_EXCHG_AREA+0x50)   /*fuctions to output message for windows*/
#define Outlog		(DAT_EXCHG_AREA+0x54)


/*entries of Davil's fuctions */
#define SPUwriteReg	(DAT_EXCHG_AREA+0x100)
#define SPUreadReg	(DAT_EXCHG_AREA+0x104)


#define GPUwriteData    (DAT_EXCHG_AREA+0x140)
#define GPUreadData	(DAT_EXCHG_AREA+0x144)
#define GPUwriteStatus  (DAT_EXCHG_AREA+0x148)
#define GPUreadStatus   (DAT_EXCHG_AREA+0x14c)
#define GPUdmaChain	(DAT_EXCHG_AREA+0x150)
#define GPUupdateLace   (DAT_EXCHG_AREA+0x154)

//parameters of load(char*,EXEC *);
#define FILENAME	(DAT_EXCHG_AREA+0x200)
#define EXECHEADER	(DAT_EXCHG_AREA+0x204)
