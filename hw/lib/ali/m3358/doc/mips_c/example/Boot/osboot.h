/**************************************************************************/
/*                               OSBOOT.H                                 */
/**************************************************************************/
/*																		  */
/*  Copyright(c) 1987-98 Embedded Performance, Inc.. All rights reserved  */
/*																		  */
/*  A purchasing customer is hereby granted rights to use, modify and     */
/*  distribute(in binary form).                                           */
/*																		  */	
/**************************************************************************/
/*                                                                        */
/*  Embedded Performance, Inc. (EPI) makes no warranty  of any kind,      */
/*  express or implied, with regard to this software.  In no event shall  */
/*  EPI be liable for incidental or consequential damages in connection   */
/*  with or arising from  the furnishing,  performance, or use of this    */
/*  software.                                                             */
/*                                                                        */
/**************************************************************************/
/*																		  */
/*  This file defines various constants and data structures used by       */
/*  ostboot.s and shared by programs linked to osboot.                    */
/*																		  */
/**************************************************************************/


#ifndef OSBOOT_INCLUDED
#define OSBOOT_INCLUDED


/*----------------------------------------------------------------------
**
** Define the HIF function codes osboot is concerned about.
**
**----------------------------------------------------------------------
*/
#define EPI_EXIT		0x1
#define EPI_COPYARGS	0x42	/* 66 */
#define EPI_SYSALLOC	0x101	/* 257 */
#define EPI_SYSFREE		0x102	/* 258 */
#define EPI_GETPAGESIZE 0x103	/* 259 */
#define EPI_GETARGS		0x104	/* 260 */

#define EPI_CLOCK		0x111	/* 273 */
#define EPI_CYCLES		0x112	/* 274 */

#define EPI_ONINTR		0x122   /* 290 */
#define EPI_SETIM		0x123   /* 291 */

#define EPI_FLUSH_CACHE	0x12c	/* 300 */
#define EPI_INIT_HEAP	0x12d	/* 301 */


#define EPI_OS_SUCCESS  0x80000000  /* negative number is what matters */
#define EPI_OS_FAILURE  1			/* positive number "               */




/*----------------------------------------------------------------------
**
** Constants used by the flush_cache HIF call.
**
**----------------------------------------------------------------------
*/
#define ICACHE		0
#define DCACHE		1



/*-------------------------------------------------------------------
** 
**  osboot_reg_save_area: This memory block is used save ra, 
**                        s0-s4 while handling exceptions. 
**
**-------------------------------------------------------------------
*/
#define	OSB_REG_RA_OFFSET	0x0
#define	OSB_REG_S0_OFFSET	0x8
#define	OSB_REG_S1_OFFSET	0x10
#define	OSB_REG_S2_OFFSET	0x18
#define	OSB_REG_S3_OFFSET	0x20
#define	OSB_REG_S4_OFFSET	0x28


	
/*-------------------------------------------------------------------
**
**  HIF parameter block:  This memory block is used to communicate 
**                        HIF calls to the emulator and RSS. 
**  
**-------------------------------------------------------------------
*/
#define HIF_SIGNATURE   0x6A59EEB3
#define HIF_SIGNATURE2  0xFEEFEEF0
#define HIF_ADDR        _hif_addr

#define HIF_SEMAP       0               /* HIF parameter block offsets */
#define HIF_FCODE       8               
#define HIF_PARM1       0x10
#define HIF_PARM2       0x18
#define HIF_PARM3       0x20
#define HIF_PARM4       0x28





/*-------------------------------------------------------------------
**
**  Various useful globals, set up by osboot and
**  used by both osboot and RSS.
**
**-------------------------------------------------------------------
*/
#ifdef LANGUAGE_C
extern unsigned int _has_float;
extern unsigned int _has_tlb;
extern unsigned int _icache_size;
extern unsigned int _icache_line_size;
extern unsigned int _dcache_size;
extern unsigned int _dcache_line_size;
extern unsigned int _cpu_sub_family;
#endif


/*-------------------------------------------------------------------
**
**  Size of a break point table entry. Number of breakpoints is 
**  user configurable.  Do not change this value.
**
**-------------------------------------------------------------------
*/
#define BKPT_ENTRY_SIZE   	24  





#endif /* OSBOOT_INCLUDED */

