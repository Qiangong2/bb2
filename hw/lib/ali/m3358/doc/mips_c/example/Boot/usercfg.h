/**************************************************************************/
/*                              USERCFG.H                                 */
/**************************************************************************/
/*																		  */
/*  Copyright(c) 1987-98 Embedded Performance, Inc.. All rights reserved  */
/*																		  */
/*  A purchasing customer is hereby granted rights to use, modify and     */
/*  distribute(in binary form).                                           */
/*																		  */	
/**************************************************************************/
/*                                                                        */
/*  Embedded Performance, Inc. (EPI) makes no warranty  of any kind,      */
/*  express or implied, with regard to this software.  In no event shall  */
/*  EPI be liable for incidental or consequential damages in connection   */
/*  with or arising from  the furnishing,  performance, or use of this    */
/*  software.                                                             */
/*                                                                        */
/**************************************************************************/
/*																		  */
/*  This file defines values for various user customizable items in       */
/*  OSBOOT and RSS.														  */
/*																		  */
/**************************************************************************/





/*----------------------------------------------------------------------
**
** OSBOOT Control Information:
**
**  CPU_SUB_FAMILY: CPU sub groups (e.g. R3000/LR33000). Legal values are
**                  defined in eregdef.h.
**
**  END_OF_PHYS_MEM:
**		    is normally the highest physical address of available
**                  RAM.  Users may wish to lower this value to make
**                  room above for some use osboot/rss is unaware of.
**
**                  This value can also be set to 0 to cause auto sizing
**                  of memory if your target board wraps memory back on
**                  itself.  For example:  On a target with 1 Meg of
**                  RAM the address a0000000 and a0100000 must reference
**  		    the same physical memory.  If the target board does not
**		    wrap memory, but does generate a bus-error on reads
**                  beyond memory then you can also use 0.  Otherwise, you
**                  must specifiy the size.
**
**  APPL_SR:        is the application's initial status register value.
**
**  APPL_FCR31:     is the application's floating point control/config
**                  setup value assigned to control register $31 of
**                  coprocessor 1.
**
**  APPL_START_ADDR:
**                  is the application's start address. A value of
**                  -1 (0xFFFFFFF) indicates the start address is to be
**                  downloaded in register r2. If osboot is linked directly
**                  with an application for final product, then setting it
**                  to an external like <start> in crt0.s is appropriate.
**
**  APPL_STACK_SIZE:
**					is the application's default stack size.
**
**  APPL_RSS_PC:    if APPL_START_ADDR is -1, then you should supply a
**                  default application PC that RSS will default the
**                  pc to.
**
**  RESET_OVERLAYS_RAM0:
**			 	    This define must be active if your target board
**                  maps 1fc00000:p to 0:p.  Systems that do not have ROM
**				    memory boot code typically are setup this way (e.g. 
**				    PC plug in cards, EPI's confidence board, etc).
**
**	IPM:			Base address of EPI ICE's Intercepter Program Memory
**                  If the debugger MC command is used to modify the base
**                  address the ICE will use then IPM must change to
**                  to match and OSBOOT rebuilt.
**
**
** Remote Sever Software(RSS) Control Information:
**
**  RSS_STACKSIZE:  remote server's stack size >= 1024.  This value
**                  should only be changed if you have modified RSS in 
**                  such a way as to increase its stack requirements.
**
**  RSS_BRKOPCODE:  opcode used for breakpoint
**
**  RSS_NBKPTS:     default is 16 break points, must be < 256
**
**  RSS_SR:         remote server's status register
**
**	RSS_BASE:		base address of serial chip (see 85C30.s in rssc/r4k
**				    directory)
**
**  SCC_NUM_DELAY:  The number of loops causing delay between the access of
**					serial chip's control and data registers.  It is function
**					of the PCLOCK frequency and memory access time.  Thus,
**					users need to tweak this number to ensure proper character
**					transmission.
**
**	LED_BASE:		base address of LEDs if used.
**
**  RSS_BACKGROUND: Uncomment this define to cause RSS to be started
**                  in background mode. This mode is useful for embedding 
**                  RSS in final products.
**
**  RSS_DETECT_RESET Set to 1 to cause RSS to attempt reset detection
**                  and notification, 0 otherwise.  It is recommended
**                  this setting be 0 if RSS's read/write data usage is
**                  configured as non-volatile RAM, otherwise 1.
**
**----------------------------------------------------------------------
*/


/*----------------------------------------------------------------------
**
** ICE IPM base memory address
**
**----------------------------------------------------------------------
*/
#if defined(IDT465) || defined(idt465) || defined(GAL4) || defined(gal4)
#define IPM	0xbf500000
#endif
#if defined(DDB4373) || defined(ddb4373) || defined(DDB430) || defined(ddb430)
#define IPM	0xb0000000
#endif

/*----------------------------------------------------------------------
**
**	DS_TRACE: a compilation flag.
**
**	For DS users, when this flag is set, the trace is turned off when
**	the process enters RSS and is turned back on when exits.
**
**			  0: the trace of RSS activity is not turned off;
**			  1: two addresses, _entry_of_rss (0xbfc00016) and
**				 _return_to_user (0xbfc00024) will be touched by
**				 load-word instructions at the point of entering and
**				 exiting RSS respectively.  Thus, DS is able to turn
**				 off the trace of RSS activities.
**
**----------------------------------------------------------------------
*/
#define	DS_TRACE	1


/*----------------------------------------------------------------------
**
**  CONFIG_K0: kseg0 coherency algorithm
**
**		At boot-up, CONFIG_K0 ( CP0_CONFIG register bits 2:0 ) are not
**		defined.  At the very beginning of boot code it is set with
**		the following options:
**			0: write-through, no write allocate
**			1: write-through, write allocate
**			2: uncacheable
**			3: write-back
**			4-7: n/a
**		Users should define this number according to the need.
**
**----------------------------------------------------------------------
*/
#define CONFIG_K0	3
#define CONFIG_K0_MASK	0x00000007


/*----------------------------------------------------------------------
**
**  EPI vrs GNU tool issue.
**
**----------------------------------------------------------------------
*/
#ifdef GNU
#define START_SYM start
#else
#define START_SYM _start
#endif




/*----------------------------------------------------------------------
**
**  IDT's IDT79S465 Target Board Configuration (4650/4700)
**
**----------------------------------------------------------------------
*/
#if defined( IDT465 ) || defined( idt465 )

#define CPU_SUB_FAMILY  R4000_CORE		/* need to work on that	*/
#define END_OF_PHYS_MEM 0x0			/* 01FFFFF: not sizing		0:sizing	*/
#define APPL_SR         0x74000801	/* SR setting before starting user app. */
#define APPL_FCR31      0x00000f00   
#ifdef FINAL
#define APPL_START_ADDR START_SYM
#else
#define APPL_START_ADDR 0xffffffff  
#endif
#define APPL_STACK_SIZE 0x2000          /* 8K */    
#define APPL_RSS_PC     0x80003000
#define LED_BASE		0xbf800030

/* #define RESET_OVERLAYS_RAM0 */

/*-------------------------------------------------------------------
** Default special register values.  If you need values different than
** the reset defaults, change the values below.  Note that a RAM based
** version of RSS assumes the current registers values should be used,
** and no programming of them will occur.
**-------------------------------------------------------------------*/

#define CONFIG_REG_MASK 0x40000000
#define CONFIG_REG_VAL  0x40000000

#ifdef RSS

#define RSS_STACKSIZE   1024
#define RSS_BRKOPCODE   0xD
#define RSS_NBKPTS      16
#define RSS_SR          0x74000002

#define RSS_BASE		0xbf300000 /* base address of serial chip */
#define SCC_BASEADD		RSS_BASE  
#define SCC_A_CTRLREG	SCC_BASEADD+0x08  /* A-Ctrl Register's address 	*/
#define SCC_B_CTRLREG	SCC_BASEADD+0x00  /* B-Ctrl Register's address	*/
#define SCC_DATAREG		0x04  	/* Data Reg's address rel. to Ctrl Addr.*/
#define SCC_FREQ		3686400	/* Baud rate clock frequency		*/
#define	SCC_NUM_DELAY	5		/* 10 for ram version			*/

#define RSS_SC_REG_WIDTH 4
#ifdef MIPSEB
#define RSS_SC_BYTEPOS  3
#else
#define RSS_SC_BYTEPOS  0
#endif
#define RSS_DETECT_RESET 1
#endif /* RSS */

#endif /* IDT465 */

/*----------------------------------------------------------------------
**
**  NEC's DDB-Vr4300 Target Board Configuration (4300)
**
**----------------------------------------------------------------------
*/
#if defined( DDB430 ) || defined( ddb430 )

#define CPU_SUB_FAMILY  R4000_CORE	/* need to work on that	*/
#define END_OF_PHYS_MEM 0x0			/* 07FFFFF: not sizing		0:sizing	*/
#define APPL_SR         0x74000801	/* SR setting before starting user app. */
#define APPL_FCR31      0x00000f00   
#ifdef FINAL
#define APPL_START_ADDR START_SYM
#else
#define APPL_START_ADDR 0xffffffff  
#endif
#define APPL_STACK_SIZE 0x2000          /* 8K */    
#define APPL_RSS_PC     0x80003000

/* #define RESET_OVERLAYS_RAM0 */

/*-------------------------------------------------------------------
** Default special register values.  If you need values different than
** the reset defaults, change the values below.  Note that a RAM based
** version of RSS assumes the current registers values should be used,
** and no programming of them will occur.
**-------------------------------------------------------------------*/

#define CONFIG_REG_MASK 0x40000000
#define CONFIG_REG_VAL  0x40000000

#ifdef RSS
#define RSS_STACKSIZE	1024
#define RSS_BRKOPCODE   0xD
#define RSS_NBKPTS      16
#define RSS_SR          0x74000002

#define RSS_BASE		0xc1000000		  /* base address of serial chip */
#define SCC_BASEADD		RSS_BASE  
#define SCC_A_CTRLREG	SCC_BASEADD+0x04  /* A-Ctrl Register's address 	*/
#define SCC_B_CTRLREG	SCC_BASEADD+0x00  /* B-Ctrl Register's address	*/
#define SCC_DATAREG		0x08			  /* Data Reg's addr rel to Ctrl addr */
#define SCC_FREQ		4915200			  /* Baud rate clock frequency */
#define	SCC_NUM_DELAY	3				  /* number of delay loops			*/
						/* change this number to 10 for ram version rss		*/
#define RSS_SC_REG_WIDTH 4
#ifdef MIPSEB
#define RSS_SC_BYTEPOS  3
#else
#define RSS_SC_BYTEPOS  0
#endif
#define RSS_DETECT_RESET 1
#endif /* RSS */

#include	"ddb4300.h"

#endif /* ddb430 */



/*----------------------------------------------------------------------
**
**  NEC's DDB-Vrc4373 Target Board Configuration (4300)
**
**----------------------------------------------------------------------
*/
#if defined( DDB4373 ) || defined( ddb4373 )

#define CPU_SUB_FAMILY  R4000_CORE	/* need to work on that	*/
#define END_OF_PHYS_MEM 0x0			/* 07FFFFF: not sizing		0:sizing	*/
#define APPL_SR         0x74000801	/* SR setting before starting user app. */
#define APPL_FCR31      0x00000f00   
#ifdef FINAL
#define APPL_START_ADDR START_SYM
#else
#define APPL_START_ADDR 0xffffffff  
#endif
#define APPL_STACK_SIZE 0x2000          /* 8K */    
#define APPL_RSS_PC     0x80003000

/* #define RESET_OVERLAYS_RAM0 */

/*-------------------------------------------------------------------
** Default special register values.  If you need values different than
** the reset defaults, change the values below.  Note that a RAM based
** version of RSS assumes the current registers values should be used,
** and no programming of them will occur.
**-------------------------------------------------------------------*/

#define CONFIG_REG_MASK 0x40000000
#define CONFIG_REG_VAL  0x40000000

#ifdef RSS
#define RSS_STACKSIZE	1024
#define RSS_BRKOPCODE   0xD
#define RSS_NBKPTS      16
#define RSS_SR          0x74000002

#define RSS_BASE		0xc1000000		  /* base address of serial chip */
#define SCC_BASEADD		RSS_BASE  
#define SCC_A_CTRLREG	SCC_BASEADD+0x04  /* A-Ctrl Register's address 	*/
#define SCC_B_CTRLREG	SCC_BASEADD+0x00  /* B-Ctrl Register's address	*/
#define SCC_DATAREG		0x08			  /* Data Reg's addr rel to Ctrl addr */
#define SCC_FREQ		4915200			  /* Baud rate clock frequency */
#define	SCC_NUM_DELAY	5				  /* number of delay loops			*/
						/* change this number to 10 for ram version rss		*/
#define RSS_SC_REG_WIDTH 4
#ifdef MIPSEB
#define RSS_SC_BYTEPOS  3
#else
#define RSS_SC_BYTEPOS  0
#endif
#define RSS_DETECT_RESET 1
#endif /* RSS */

#include	"ddb4300.h"

#endif /* ddb4373 */





/*----------------------------------------------------------------------
**
**  Galileo's GAL4 Target Board Configuration (4600 or 4700)
**
**----------------------------------------------------------------------
*/
#if defined( GAL4 ) || defined( gal4 )

#define CPU_SUB_FAMILY  R4000_CORE		/* need to work on that	*/
#define END_OF_PHYS_MEM 0x0			/* 01FFFFF: not sizing		0:sizing	*/
#define APPL_SR         0x74000801	/* SR setting before starting user app. */
#define APPL_FCR31      0x00000f00   
#ifdef FINAL
#define APPL_START_ADDR START_SYM
#else
#define APPL_START_ADDR 0xffffffff  
#endif
#define APPL_STACK_SIZE 0x2000          /* 8K */    
#define APPL_RSS_PC     0x80003000

/* #define RESET_OVERLAYS_RAM0 */

/*-------------------------------------------------------------------
** Default special register values.  If you need values different than
** the reset defaults, change the values below.  Note that a RAM based
** version of RSS assumes the current registers values should be used,
** and no programming of them will occur.
**-------------------------------------------------------------------*/

#define CONFIG_REG_MASK 0x40000000
#define CONFIG_REG_VAL  0x40000000

#ifdef RSS

#define RSS_STACKSIZE   1024
#define RSS_BRKOPCODE   0xD
#define RSS_NBKPTS      16
#define RSS_SR          0x74000002

#define RSS_BASE		0xbc000000 /* base address of serial chip */
#define SCC_BASEADD		RSS_BASE  
#define SCC_A_CTRLREG	SCC_BASEADD+0x08  /* A-Ctrl Register's address 	*/
#define SCC_B_CTRLREG	SCC_BASEADD+0x00  /* B-Ctrl Register's address	*/
#define SCC_DATAREG		0x04  	/* Data Reg's address rel. to Ctrl Addr.*/
#define SCC_FREQ		3686400	/* Baud rate clock frequency		*/
#define	SCC_NUM_DELAY	5		/* 10 for ram version			*/

#define RSS_SC_REG_WIDTH 4
#ifdef MIPSEB
#define RSS_SC_BYTEPOS  3
#else
#define RSS_SC_BYTEPOS  0
#endif
#define RSS_DETECT_RESET 1
#endif /* RSS */

#include	"gal4.h"

#endif /* GAL4 */

/*----------------------------------------------------------------------
**
**  Galileo's GAL4 Target Board Configuration (4600 or 4700)
**
**----------------------------------------------------------------------
*/
#if defined( sim )

#define CPU_SUB_FAMILY  R4000_CORE		/* need to work on that	*/
#define END_OF_PHYS_MEM 0x01FFFFF		/* not sizing, default to 2M	*/
#define APPL_SR         0x74000801	/* SR setting before starting user app. */
#define APPL_FCR31      0x00000f00   
#ifdef FINAL
#define APPL_START_ADDR START_SYM
#else
#define APPL_START_ADDR 0xffffffff  
#endif
#define APPL_STACK_SIZE 0x2000          /* 8K */    
#define APPL_RSS_PC     0x80003000

#endif /*sim	*/

