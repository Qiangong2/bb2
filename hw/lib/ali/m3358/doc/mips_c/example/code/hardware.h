#ifndef _HARDWARE_H_
#define _HARDWARE_H_

#define inline __inline

#define SYS_IO_BASE		0xb8000000
#define VDVI_IO_BASE	0xb8004000
#define VPOST_IO_BASE	0xb8004100
#define VDEC_IO_BASE	0xb8004200

//0->4M,1->8M
#define VIDEO_MEM_MODE		   0

#define VDEC_FRM0_Y_BASE	   0x200000
#define VDEC_FRM0_C_BASE	   0x265400
#define VDEC_FRM1_Y_BASE	   0x298000
#define VDEC_FRM1_C_BASE	   0x2fd400
#define VDEC_FRM2_Y_BASE	   0x330000
#define VDEC_FRM2_C_BASE	   0x395400
#define VDEC_OSD_BASE		   0x3c8000

#define VDEC_DVIEW_BASE		   0x3d5000

#define VDEC_MAF_LA_BASE	   0x3d6800
#define VDEC_MV_FLAG_BASE	   0x3e3800
#define VDEC_LA_FLAG_BASE	   0x3e5000
#define VDEC_COMB_FLAG_BASE	   0x3e6800
#define VDEC_SPU_BASE		   0x3f3000

//MPEG4
#define VDEC_MBH_BASE		   0x3e8000
#define VDEC_DCAC_BASE		   0x3f1000
#define VDEC_QP_BASE		   0x3f1c00
#define VDEC_GMC_BASE		   0x3f2800
#define VDEC_NOTCODE_BASE	   0x3f2c00

#define VE_STATUS_FINISH		0x1
#define VE_STATUS_ERROR			0x2
#define VE_STATUS_VLD_REQ		0x4
#define VE_STATUS_VLD_BUSY		0x100
#define VE_STATUS_DCAC_BUSY		0x200
#define VE_STATUS_IQIS_BUSY		0x400
#define VE_STATUS_IDCT_BUSY		0x800
#define VE_STATUS_MC_BUSY		0x1000
#define VE_STATUS_VE_BUSY		0x2000
#define VE_STATUS_IDCT_BUF_EMPTY 0x4000
#define VE_STATUS_IQ_BUF_EMPTY  0x8000
#define VE_STATUS_MAF_BUSY		0x10000
#define VE_STATUS_DBLK_BUSY		0x40000

#define DVI_ODD_FLD_END_BIT		0x200
#define DVI_FRAME_END_BIT		0x100
#define DVI_LINE_MEET_BIT		0x1000
#define DVI_FRAME_STATUS_BIT	0x40000

#define MPHR		0x00
#define MVOPHR		0x04
#define FSIZE		0x08
#define MBADDR		0x0C
#define VECTRL		0x10
#define VETRIGGER	0x14
#define VESTAT		0x18
#define TRBTRDFLD	0x1C
#define CRTMBADDR	0x20
#define VLDBADDR	0x24
#define VLDOFFSET	0x28
#define VLDLEN		0x2C
#define VBVENDADDR  0x30
#define MBHADDR		0x34
#define DCACADDR	0x38
#define QPADDR		0x3C
#define FRM0ADDR	0x40
#define FRM1ADDR	0x44
#define FRM2ADDR	0x48
#define FRM3ADDR	0x4C
#define FRM4ADDR	0x50
#define FRM5ADDR	0x54
#define MAFADDR		0x58
#define MAFADDRSEL	0x5C
#define GMCADDR		0x64
#define NCFADDR		0x68
#define DVIEWYADDR	0x70
#define DVIEWCADDR	0x74
#define VDMBADDR	0x78
#define PICSIZE		0x7C
#define IQMINPUT	0x80
#define QCINPUT		0x88
#define MBH			0x8C
#define MV1			0x90
#define MV2			0x94
#define MV3			0x98
#define MV4			0x9C
#define MV5			0xA0
#define MV6			0xA4
#define MV7			0xA8
#define MV8			0xAC
#define IQIDCTINPUT	0xB0
#define SOL			0xB4
#define SDX			0xB8
#define SDY			0xBC
#define ORDERCTRL	0xC0
#define ERRFLAG		0xC4
#define DFF0		0xC8
#define DFF1		0xCC
#define DFF2		0xD0
#define DFF3		0xD4
#define DVIEWCTRL	0xD8
#define DEBLKCTRL	0xDC
#define MRLCTRL		0xE0
#define DBEMB		0xE4
#define SSR			0xE8
#define DMCOEFRM	0xEC
#define DSXYINBCK	0xF0
#define MAFMVFLAGCTRL		0xF4
#define MAFMVFLAGCTRL1		0xF8
#define MAFMVFLAGCTRL2		0xFC

//Infomation ID
#define INFO_FRAME_DECODE_END		0x10000001
#define INFO_FRAME_DECODE_ERROR		0x10000002
#define INFO_INTER_OFIELD_END		0x10000003
#define INFO_INTER_FRAME_END		0x10000004
#define INFO_PROG_FRAME_END			0x10000005
#define INFO_PROG_OFRAME_END		0x10000006
#define INFO_PROG_EFRAME_END		0x10000007
#define INFO_VPOST_NOT_COMPLET		0x10000008
#define INFO_INTER_LINE_MEET		0x10000009
#define INFO_PROG_LINE_MEET			0x1000000a
#define INFO_PIC_MB_SIZE			0x02000000
#define INFO_PIC_STRUCTURE			0x03000000
#define INFO_DECODE_POS				0x04000000
#define INFO_INTER_DISP_POS			0x05000000
#define INFO_PROG_DISP_POS			0x06000000
#define INFO_VLD_POS				0x07000000
#define INFO_DUMP_WAVE				0x08000000
#define INFO_VOB					0x80000000

#define vio0_write_4b(offset,val) vio_write_4b(VDVI_IO_BASE,offset,val)
#define vio1_write_4b(offset,val) vio_write_4b(VPOST_IO_BASE,offset,val)
#define vio2_write_4b(offset,val) vio_write_4b(VDEC_IO_BASE,offset,val)

#define vio0_write_1b(offset,val) vio_write_1b(VDVI_IO_BASE,offset,val)
#define vio1_write_1b(offset,val) vio_write_1b(VPOST_IO_BASE,offset,val)
#define vio2_write_1b(offset,val) vio_write_1b(VDEC_IO_BASE,offset,val)

#define vio0_read_4b(offset) vio_read_4b(VDVI_IO_BASE,offset)
#define vio1_read_4b(offset) vio_read_4b(VPOST_IO_BASE,offset)
#define vio2_read_4b(offset) vio_read_4b(VDEC_IO_BASE,offset)

#define vio0_read_1b(offset) vio_read_1b(VDVI_IO_BASE,offset)
#define vio1_read_1b(offset) vio_read_1b(VPOST_IO_BASE,offset)
#define vio2_read_1b(offset) vio_read_1b(VDEC_IO_BASE,offset)

void vio_write_4b(unsigned long base,unsigned long offset, unsigned long val);
unsigned long vio_read_4b(unsigned long base,unsigned long offset);
unsigned char vio_read_1b(unsigned long base,unsigned long offset);
void vio_write_1b(unsigned long base,unsigned long offset, unsigned long val);

void  GPIOOutputInfo(int flag,unsigned long info_0,unsigned long info_1);

#endif
