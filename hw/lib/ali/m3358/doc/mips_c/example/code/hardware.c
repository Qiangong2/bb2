#include "hardware.h"

void vio_write_4b(unsigned long base,unsigned long offset, unsigned long val)
{	
	*(volatile unsigned long *)(base+offset) = val;
}

void vio_write_1b(unsigned long base,unsigned long offset, unsigned long val)
{	
	*(volatile unsigned char *)(base+offset) = val;
}

unsigned long vio_read_4b(unsigned long base,unsigned long offset)
{
	return *(volatile unsigned long *)(base+offset);
}

unsigned char vio_read_1b(unsigned long base,unsigned long offset)
{
	return *(volatile unsigned char *)(base+offset);
}



//compatible with HW Mips Stimulation Env

void  GPIOOutputInfo(int flag,unsigned long info_0,unsigned long info_1)
{
	unsigned long store_buf[3];
	const unsigned char info0_id	=	0x60;
	const unsigned char info1_id	=	0x50;
	const unsigned char info2_id	=	0x30;
	
	const unsigned long info2_data	=	0x99999999;
	
	//save GPIO register	
	store_buf[0] = *(volatile unsigned char *)(SYS_IO_BASE+0x59);
	store_buf[1] = *(volatile unsigned long *)(SYS_IO_BASE+0x30);
	store_buf[2] = *(volatile unsigned char *)(SYS_IO_BASE+0x55);
		
	//enable gpio_out
	*(volatile unsigned char *)(SYS_IO_BASE+0x59) = 0xf0;
	
	//output information
	*(volatile unsigned long *)(SYS_IO_BASE+0x30) = info_0;
	*(volatile unsigned char *)(SYS_IO_BASE+0x55) = info0_id;
	
	if(flag==1)
	{
		*(volatile unsigned long *)(SYS_IO_BASE+0x30) = info_1;
		*(volatile unsigned char *)(SYS_IO_BASE+0x55) = info1_id;
	}	
		
	//output ID
	*(volatile unsigned long *)(SYS_IO_BASE+0x30) = info2_data;
	*(volatile unsigned char *)(SYS_IO_BASE+0x55) = info2_id;
		
	//restore GPIO register	
	*(volatile unsigned char *)(SYS_IO_BASE+0x59) = store_buf[0];
	*(volatile unsigned long *)(SYS_IO_BASE+0x30) = store_buf[1];
	*(volatile unsigned char *)(SYS_IO_BASE+0x55) = store_buf[2];
}
