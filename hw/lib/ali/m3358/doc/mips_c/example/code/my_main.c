#include "hardware.h"

//#define MAKE_SOC_PATTERN

//extern volatile unsigned char *p_video_buf;
//extern unsigned long video_buf_byte_size;

void force_stimulation_end();

void my_main()
{
	
	GPIOOutputInfo(0,INFO_VOB|0x40,0);
	
#ifndef MAKE_SOC_PATTERN	
	force_stimulation_end();
#endif

}


void force_stimulation_end()
{	
	//Stop simulation mips IP addiu $0,$0,0	 --> 24000000
	asm volatile (
				"addiu	$0,0"
				::
				);
	while(1);
}

