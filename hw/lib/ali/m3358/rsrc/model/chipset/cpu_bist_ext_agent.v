/***********************************************************
File:		cpu_bist_ext_agent.v
Description:Control cpu whether enter bist test mode.
			In the test mode, check the test whether finish
			and report the test whether correct.
Author:		yuky
History:	2002-08-20	Initial version
			2003-09-29	add port bist_test_sel, when bist_test_mode_sel is 0
			and test mode is 1, bist_test_mode issude; when  bist_test_mode_sel = 1
			and test mode = 1, scan_mode issued Jeffrey
************************************************************/
module cpu_bist_ext_agent(
	bist_mode,
	bist_finish,
	bist_err_vec,
	bist_test_mode_sel,

	clk,
	rst_
	);

parameter	udly = 1;
output			bist_mode;
input			bist_finish;
input	[5:0]	bist_err_vec;
output			bist_test_mode_sel;
input			clk;
input			rst_;

//bist_enable; control by task
reg	bist_enable;
initial	bist_enable = 0;

//------------------------------

//reset delay two clock for sync
reg	rst_dly1_, rst_dly2_;
initial begin
		rst_dly1_ = 0;
		rst_dly2_ = 0;
end
always @(posedge clk) begin
	rst_dly1_ <= #udly rst_;
	rst_dly2_ <= #udly rst_dly1_;
	end

//-------------------------------

//finish delay two clock for sync
reg	bist_finish_dly1, bist_finish_dly2;
initial begin
	bist_finish_dly1 = 0;
	bist_finish_dly2 = 0;
end
always @(posedge clk) begin
	bist_finish_dly1 <= #udly bist_finish;
	bist_finish_dly2 <= #udly bist_finish_dly1;
	end

//-------------------------------

//control the bist_mode and bist_test_mode_sel
reg				bist_mode;
reg				bist_test_mode_sel;
initial			bist_mode = 0;
initial			bist_test_mode_sel = 1;
always @(posedge clk)
	if (rst_dly2_) begin
		if (bist_finish_dly2) begin
			bist_mode <= #udly 0;
			bist_test_mode_sel <= #udly 1;
		end
		else if (bist_enable) begin
			bist_mode <= #udly 1;
			bist_test_mode_sel <= #udly 0;
		end
	end

//-------------------------------

//report the error vec
wire	bist_finish_dly1_pulse = bist_finish_dly1 & ~bist_finish_dly2;
always @(posedge clk)
	if (rst_dly2_ && bist_finish_dly1_pulse) begin
		if (bist_err_vec[0])
			$display("Error_T2_rsim: CPU bist error -- ErrVec[0] at time %0t", $realtime);
		if (bist_err_vec[1])
			$display("Error_T2_rsim: CPU bist error -- ErrVec[1] at time %0t", $realtime);
		if (bist_err_vec[2])
			$display("Error_T2_rsim: CPU bist error -- ErrVec[2] at time %0t", $realtime);
		if (bist_err_vec[3])
			$display("Error_T2_rsim: CPU bist error -- ErrVec[3] at time %0t", $realtime);
		if (bist_err_vec[4])
			$display("Error_T2_rsim: CPU bist error -- ErrVec[4] at time %0t", $realtime);
		if (bist_err_vec[5])
			$display("Error_T2_rsim: CPU bist error -- ErrVec[5] at time %0t", $realtime);
//		if (bist_err_vec[6])
//			$display("Error_T2_rsim: CPU bist error -- ErrVec[6] at time %0t", $realtime);
//		if (bist_err_vec[7])
//			$display("Error_T2_rsim: CPU bist error -- ErrVec[7] at time %0t", $realtime);
//		if (bist_err_vec[8])
//			$display("Error_T2_rsim: CPU bist error -- ErrVec[8] at time %0t", $realtime);
//		if (bist_err_vec[9])
//			$display("Error_T2_rsim: CPU bist error -- ErrVec[9] at time %0t", $realtime);
//		if (!(|bist_err_vec))
//			$display("Good News: CPU bist Pass -- at time %0t", $realtime);
	end

//-------------------------

//Task control
task cpu_bist_enable;
begin
	wait (rst_dly2_);
	repeat (3) @(posedge clk); //wait three clock cycle after reset
	bist_enable = 1;
	$display("Start to enable the CPU BIST......");
	wait (bist_finish_dly1);
	bist_enable = 0;
	$display("CPU BIST Scan End!!!");
end
endtask

endmodule