// snow 1999-11-03
//
// 1M bytes Boot Rom for WinCE Emulation
//
// Version 0.1	---	1> 32-bit word read mode
//			2> 1 clock read fast mode
//			3> read only	
//			3> Physical Address: 1FC0_0000 ~ 1FDF_FFFF	

module 	bootrom	(eeprom_addr,
		 eeprom_data,
		 eeprom_cs_,
		 eeprom_oe_
		);

output	[31:0]	eeprom_data;
input	[20:2]	eeprom_addr;
input		eeprom_cs_;
input		eeprom_oe_;

parameter	udly	= 1;		// Unit delay
parameter	size	= 2*256*1024;	// 2M bytes, 256K words

//reg	[31:0]	rom_reg	[0:size - 1];
reg		[31:0]	rom_reg [size - 1:0];

wire	[20:2]	rom_addr 	= eeprom_addr[20:2];	// word address
wire	[31:0]	rom_data	= rom_reg[rom_addr];	

assign  eeprom_data	= (eeprom_oe_ | eeprom_cs_) ? 32'hz : rom_data[31:0];

endmodule

