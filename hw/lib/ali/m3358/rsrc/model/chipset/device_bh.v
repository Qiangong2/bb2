/*******************************************************************************************
//File 	: 	device_bh.v -- used for testing SDRAM memory interface module
//Author: 	Figo
//Date	: 	2002-07-12	Initial version
*******************************************************************************************/
`timescale	1ns/100ps

module	device_bh(
	//To Memory Interface:
	xxx_ram_req,
	xxx_ram_rw,
	xxx_ram_addr,
	xxx_ram_rbl,
	xxx_ram_wdata,
	xxx_ram_wbe,
	xxx_ram_wlast,
	//From Memory Interface:
	xxx_ram_ack,
	xxx_ram_err,
	xxx_ram_rrdy,
	xxx_ram_rlast,
	xxx_ram_rdata,
	xxx_ram_wrdy,

	//others:
	rst_,
	mem_clk
	);

parameter	udly	=	1'b1;
//	------------------------------------------------------------------------------------
//To Memory Interface:
output			xxx_ram_req;
output			xxx_ram_rw;
output[27:0]	xxx_ram_addr;
output[1:0]		xxx_ram_rbl;
output[31:0]	xxx_ram_wdata;
output[3:0]		xxx_ram_wbe;
output			xxx_ram_wlast;
//From Memory Interface:
input			xxx_ram_ack;
input			xxx_ram_err;
input			xxx_ram_rrdy;
input			xxx_ram_rlast;
input[31:0]		xxx_ram_rdata;
input			xxx_ram_wrdy;

//others:
input			rst_,
				mem_clk;
//	--------------------------------------------------------------------------------------
reg			xxx_ram_req;
reg			xxx_ram_rw;
reg[27:0]	xxx_ram_addr;
reg[1:0]	xxx_ram_rbl;
wire[31:0]	xxx_ram_wdata;
wire[3:0]	xxx_ram_wbe;
wire		xxx_ram_wlast;

reg			keep_mem_rw;

reg			mem_access_en;
reg			mem_wr_en;
reg[27:0]	mem_access_addr;
reg[1:0]	mem_access_rbl;

reg[31:0]	rd_data_tmp0,rd_data_tmp1,rd_data_tmp2,rd_data_tmp3;

reg[31:0]	mem_wdata_buf[3:0];
reg[3:0]	mem_wbe_buf[3:0];
reg			mem_wlast_buf[3:0];
reg[1:0]	mem_wbuf_rpointer;

reg			rd_disp_open, wr_disp_open;

integer i;

initial	begin
	for(i=0;i<=3;i=i+1)begin
		mem_wdata_buf[i]	=	0;
		mem_wbe_buf[i]		=	0;
		mem_wlast_buf[i]	=	0;
	end
	mem_wbuf_rpointer	=	0;
end

initial	begin
	rd_disp_open	=	0;
	wr_disp_open	=	0;
end

initial	begin
	xxx_ram_req		=	0;
	xxx_ram_rw		=	0;
	xxx_ram_addr	=	0;
	xxx_ram_rbl		=	0;
end

initial	begin
	keep_mem_rw	=	1'b0;
end

initial	begin
	mem_access_en	=	0;
	mem_wr_en		=	0;
	mem_access_addr	=	0;
	mem_access_rbl	=	0;
end

always	@(posedge mem_clk or negedge rst_)
	if(!rst_)begin
		xxx_ram_req		<=	#udly	1'b0;
	end
	else if(xxx_ram_ack)begin
		xxx_ram_req		<=	#udly	1'b0;
	end
	else if(mem_access_en)begin
		xxx_ram_req		<=	#udly	1'b1;
	end

always	@(posedge mem_clk or negedge rst_)
	if(!rst_)begin
		xxx_ram_rw		<=	#udly	1'b0;
		xxx_ram_addr	<=	#udly	28'h0;
		xxx_ram_rbl		<=	#udly	2'h0;
	end
	else if(mem_access_en)begin
		xxx_ram_rw		<=	#udly	mem_wr_en;
		xxx_ram_addr	<=	#udly	mem_access_addr;
		xxx_ram_rbl		<=	#udly	mem_access_rbl;
	end


always	@(posedge mem_clk or negedge rst_)
	if(!rst_)begin
		mem_wbuf_rpointer	<=	#udly	0;
	end
	else if(xxx_ram_wlast && xxx_ram_wrdy)begin
		mem_wbuf_rpointer	<=	#udly	0;
	end
	else if(xxx_ram_wrdy)begin
		mem_wbuf_rpointer	<=	#udly	mem_wbuf_rpointer + 1;
	end
	
assign	xxx_ram_wdata	=	mem_wdata_buf[mem_wbuf_rpointer];
assign	xxx_ram_wbe		=	mem_wbe_buf[mem_wbuf_rpointer];
assign	xxx_ram_wlast	=	mem_wlast_buf[mem_wbuf_rpointer];

//	---------------------------------------------------------------------------------------
//Pattern Drivers:

//Read One Double Words:
task	SDRAM_RD_1DW;
input[27:0]		rd_addr;
output[31:0]	rd_data0;
reg[31:0]		rd_data0;
begin
	rd_data0		=	32'hxxxx_xxxx;
	mem_wr_en		=	1'b0;
	mem_access_rbl	=	2'h0;
	mem_access_addr	=	rd_addr;
	keep_mem_rw		=	1'b1;
	mem_access_en	=	1'b1;
//	$display ("\n%m :\nRead SDRAM One Double Words");
//	$display ("%m :The Start Address = %h\tThe Read Burst Length = 1",mem_access_addr);
	$display ("\n%m :\nThe Start Address = %h",mem_access_addr);
	while(keep_mem_rw)begin
		@(posedge mem_clk)begin
			if(xxx_ram_ack)begin
				mem_access_en	=	1'b0;
			end
			if(xxx_ram_rrdy)begin
				rd_data_tmp0	=	xxx_ram_rdata;
				if(rd_disp_open)begin
					$display ("\n%m :\nGet the read data0 = %h",rd_data_tmp0);
				end
			end
			if(xxx_ram_rlast)begin
				keep_mem_rw	=	1'b0;
				if(rd_disp_open)begin
					$display ("\n%m :\nGet the last read data");
				end
			end
		end
	end
	rd_data0	=	rd_data_tmp0;
end
endtask

//Read Two Double Words:
task	SDRAM_RD_2DW;
input[27:0]		rd_addr;
output[31:0]	rd_data0;
output[31:0]	rd_data1;
reg[31:0]		rd_data0;
reg[31:0]		rd_data1;
reg[1:0]		rd_tag;
begin
	rd_data0		=	32'hxxxx_xxxx;
	rd_data1		=	32'hxxxx_xxxx;
	rd_tag			=	2'b01;
	mem_wr_en		=	1'b0;
	mem_access_rbl	=	2'h1;
	mem_access_addr	=	rd_addr;
	keep_mem_rw		=	1'b1;
	mem_access_en	=	1'b1;
//	$display ("\n%m :\nRead SDRAM Two Double Words");
//	$display ("%m :The Start Address = %h\tThe Read Burst Length = 2",mem_access_addr);
	$display ("\n%m :\nThe Start Address = %h",mem_access_addr);
	while(keep_mem_rw)begin
		@(posedge mem_clk)begin
			if(xxx_ram_ack)begin
				mem_access_en	=	1'b0;
			end
			if(xxx_ram_rrdy)begin
				if(rd_tag[0])begin
					rd_data_tmp0	=	xxx_ram_rdata;
					if(rd_disp_open)begin
						$display ("\n%m :\nGet the read data0 = %h",rd_data_tmp0);
					end
				end
				if(rd_tag[1])begin
					rd_data_tmp1	=	xxx_ram_rdata;
					if(rd_disp_open)begin
						$display ("\n%m :\nGet the read data1 = %h",rd_data_tmp1);
					end
				end
				rd_tag	=	{rd_tag[0],rd_tag[1]};
			end
			if(xxx_ram_rlast)begin
				keep_mem_rw	=	1'b0;
				if(rd_disp_open)begin
					$display ("\n%m :\nGet the last read data");
				end
			end
		end
	end
	rd_data0	=	rd_data_tmp0;
	rd_data1	=	rd_data_tmp1;
end
endtask

//Read Three Double Words:
task	SDRAM_RD_3DW;
input[27:0]		rd_addr;
output[31:0]	rd_data0;
output[31:0]	rd_data1;
output[31:0]	rd_data2;
reg[31:0]		rd_data0;
reg[31:0]		rd_data1;
reg[31:0]		rd_data2;
reg[2:0]		rd_tag;
begin
	rd_data0		=	32'hxxxx_xxxx;
	rd_data1		=	32'hxxxx_xxxx;
	rd_data2		=	32'hxxxx_xxxx;
	rd_tag			=	3'b001;
	mem_wr_en		=	1'b0;
	mem_access_rbl	=	2'h2;
	mem_access_addr	=	rd_addr;
	keep_mem_rw		=	1'b1;
	mem_access_en	=	1'b1;
//	$display ("\n%m :\nRead SDRAM Three Double Words");
//	$display ("%m :The Start Address = %h\tThe Read Burst Length = 3",mem_access_addr);
	$display ("\n%m :\nThe Start Address = %h",mem_access_addr);
	while(keep_mem_rw)begin
		@(posedge mem_clk)begin
			if(xxx_ram_ack)begin
				mem_access_en	=	1'b0;
			end
			if(xxx_ram_rrdy)begin
				if(rd_tag[0])begin
					rd_data_tmp0	=	xxx_ram_rdata;
					if(rd_disp_open)begin
						$display ("\n%m :\nGet the read data0 = %h",rd_data_tmp0);
					end
				end
				if(rd_tag[1])begin
					rd_data_tmp1	=	xxx_ram_rdata;
					if(rd_disp_open)begin
						$display ("\n%m :\nGet the read data1 = %h",rd_data_tmp1);
					end
				end
				if(rd_tag[2])begin
					rd_data_tmp2	=	xxx_ram_rdata;
					if(rd_disp_open)begin
						$display ("\n%m :\nGet the read data2 = %h",rd_data_tmp2);
					end
				end
				rd_tag	=	{rd_tag[1:0],rd_tag[2]};
			end
			if(xxx_ram_rlast)begin
				keep_mem_rw	=	1'b0;
				if(rd_disp_open)begin
					$display ("\n%m :\nGet the last read data");
				end
			end
		end
	end
	rd_data0	=	rd_data_tmp0;
	rd_data1	=	rd_data_tmp1;
	rd_data2	=	rd_data_tmp2;
end
endtask

//Read Four Double Words:
task	SDRAM_RD_4DW;
input[27:0]		rd_addr;
output[31:0]	rd_data0;
output[31:0]	rd_data1;
output[31:0]	rd_data2;
output[31:0]	rd_data3;

reg[31:0]		rd_data0;
reg[31:0]		rd_data1;
reg[31:0]		rd_data2;
reg[31:0]		rd_data3;
reg[3:0]		rd_tag;
begin
	rd_data0		=	32'hxxxx_xxxx;
	rd_data1		=	32'hxxxx_xxxx;
	rd_data2		=	32'hxxxx_xxxx;
	rd_data3		=	32'hxxxx_xxxx;
	rd_tag			=	4'b0001;
	mem_wr_en		=	1'b0;
	mem_access_rbl	=	2'h3;
	mem_access_addr	=	rd_addr;
	keep_mem_rw		=	1'b1;
	mem_access_en	=	1'b1;
//	$display ("\n%m :\nRead SDRAM Four Double Words");
//	$display ("%m :The Start Address = %h\tThe Read Burst Length = 4",mem_access_addr);
	$display ("\n%m :\nThe Start Address = %h",mem_access_addr);
	while(keep_mem_rw)begin
		@(posedge mem_clk)begin
			if(xxx_ram_ack)begin
				mem_access_en	=	1'b0;
			end
			if(xxx_ram_rrdy)begin
				if(rd_tag[0])begin
					rd_data_tmp0	=	xxx_ram_rdata;
					if(rd_disp_open)begin
						$display ("\n%m :\nGet the read data0 = %h",rd_data_tmp0);
					end
				end
				if(rd_tag[1])begin
					rd_data_tmp1	=	xxx_ram_rdata;
					if(rd_disp_open)begin
						$display ("\n%m :\nGet the read data1 = %h",rd_data_tmp1);
					end
				end
				if(rd_tag[2])begin
					rd_data_tmp2	=	xxx_ram_rdata;
					if(rd_disp_open)begin
						$display ("\n%m :\nGet the read data2 = %h",rd_data_tmp2);
					end
				end
				if(rd_tag[3])begin
					rd_data_tmp3	=	xxx_ram_rdata;
					if(rd_disp_open)begin
						$display ("\n%m :\nGet the read data3 = %h",rd_data_tmp3);
					end
				end
				rd_tag	=	{rd_tag[2:0],rd_tag[3]};
			end
			if(xxx_ram_rlast)begin
				keep_mem_rw	=	1'b0;
				if(rd_disp_open)begin
					$display ("\n%m :\nGet the last read data");
				end
			end
		end
	end
	rd_data0	=	rd_data_tmp0;
	rd_data1	=	rd_data_tmp1;
	rd_data2	=	rd_data_tmp2;
	rd_data3	=	rd_data_tmp3;
end
endtask

//Write One Double Words:
task	SDRAM_WR_1DW;
input[27:0]	wr_addr;
input[31:0]	wr_data0;
input[3:0]	wr_wbe0;

begin
	mem_wdata_buf[0]	<=	#udly	wr_data0;
	mem_wdata_buf[1]	<=	#udly	0;
	mem_wdata_buf[2]	<=	#udly	0;
	mem_wdata_buf[3]	<=	#udly	0;
	mem_wbe_buf[0]	<=	#udly	wr_wbe0;
	mem_wbe_buf[1]	<=	#udly	0;
	mem_wbe_buf[2]	<=	#udly	0;
	mem_wbe_buf[3]	<=	#udly	0;
	mem_wlast_buf[0]	<=	#udly	1;
	mem_wlast_buf[1]	<=	#udly	0;
	mem_wlast_buf[2]	<=	#udly	0;
	mem_wlast_buf[3]	<=	#udly	0;
	//----------------------------
	mem_wr_en		=	1'b1;
	mem_access_rbl	=	2'h0;
	mem_access_addr	=	wr_addr;
	keep_mem_rw		=	1'b1;
	mem_access_en	=	1'b1;
//	$display ("\n%m :\nWrite SDRAM One Double Words");
//	$display ("\n%m :\nThe Start Address = %h\tThe Write Burst Length = 1",mem_access_addr);
	$display ("\n%m :\nThe Start Address = %h",mem_access_addr);
	while(keep_mem_rw)begin
		@(posedge mem_clk)begin
			if(xxx_ram_ack)begin
				mem_access_en	=	0;
			end
			if(xxx_ram_wrdy)begin
				if(wr_disp_open)begin
					if(mem_wbuf_rpointer == 2'b00)begin
						$display ("\n%m :\nGive the write data0 = %h\tGive the write wbe0 = %b",
									mem_wdata_buf[0],mem_wbe_buf[0]);
//						$display ("\n%m :\nGive the write wbe0 = %b",mem_wbe_buf[0]);
					end
					else if(mem_wbuf_rpointer == 2'b01)begin
						$display ("\n%m :\nGive the write data1 = %h\tGive the write wbe1 = %b",
									mem_wdata_buf[1],mem_wbe_buf[1]);
//						$display ("\n%m :\nGive the write wbe1 = %b",mem_wbe_buf[1]);
					end
					else if(mem_wbuf_rpointer == 2'b10)begin
						$display ("\n%m :\nGive the write data2 = %h\tGive the write wbe2 = %b",
									mem_wdata_buf[2],mem_wbe_buf[2]);
//						$display ("\n%m :\nGive the write wbe2 = %b",mem_wbe_buf[2]);
					end
					else	begin
						$display ("\n%m :\nGive the write data3 = %h\tGive the write wbe3 = %b",
									mem_wdata_buf[3],mem_wbe_buf[3]);
//						$display ("\n%m :\nGive the write wbe3 = %b",mem_wbe_buf[3]);
					end
				end
			end
			if(xxx_ram_wrdy && xxx_ram_wlast)begin
				keep_mem_rw	=	1'b0;
				if(wr_disp_open)begin
					$display ("\n%m :\nGive the last write data");
				end
			end
		end	
	end
	
end
endtask

//Write Two Double Words:
task	SDRAM_WR_2DW;
input[27:0]	wr_addr;
input[31:0]	wr_data0;
input[3:0]	wr_wbe0;
input[31:0]	wr_data1;
input[3:0]	wr_wbe1;

begin
	mem_wdata_buf[0]	<=	#udly	wr_data0;
	mem_wdata_buf[1]	<=	#udly	wr_data1;
	mem_wdata_buf[2]	<=	#udly	0;
	mem_wdata_buf[3]	<=	#udly	0;
	mem_wbe_buf[0]	<=	#udly	wr_wbe0;
	mem_wbe_buf[1]	<=	#udly	wr_wbe1;
	mem_wbe_buf[2]	<=	#udly	0;
	mem_wbe_buf[3]	<=	#udly	0;
	mem_wlast_buf[0]	<=	#udly	0;
	mem_wlast_buf[1]	<=	#udly	1;
	mem_wlast_buf[2]	<=	#udly	0;
	mem_wlast_buf[3]	<=	#udly	0;
	//----------------------------
	mem_wr_en		=	1'b1;
	mem_access_rbl	=	2'h0;
	mem_access_addr	=	wr_addr;
	keep_mem_rw		=	1'b1;
	mem_access_en	=	1'b1;
//	$display ("\n%m :\nWrite SDRAM Two Double Words");
//	$display ("\n%m :\nThe Start Address = %h\tThe Write Burst Length = 2",mem_access_addr);
	$display ("\n%m :\nThe Start Address = %h",mem_access_addr);
	while(keep_mem_rw)begin
		@(posedge mem_clk)begin
			if(xxx_ram_ack)begin
				mem_access_en	=	0;
			end
			if(xxx_ram_wrdy)begin
				if(wr_disp_open)begin
					if(mem_wbuf_rpointer == 2'b00)begin
						$display ("\n%m :\nGive the write data0 = %h\tGive the write wbe0 = %b",
									mem_wdata_buf[0],mem_wbe_buf[0]);
//						$display ("\n%m :\nGive the write wbe0 = %b",mem_wbe_buf[0]);
					end
					else if(mem_wbuf_rpointer == 2'b01)begin
						$display ("\n%m :\nGive the write data1 = %h\tGive the write wbe1 = %b",
									mem_wdata_buf[1],mem_wbe_buf[1]);
//						$display ("\n%m :\nGive the write wbe1 = %b",mem_wbe_buf[1]);
					end
					else if(mem_wbuf_rpointer == 2'b10)begin
						$display ("\n%m :\nGive the write data2 = %h\tGive the write wbe2 = %b",
									mem_wdata_buf[2],mem_wbe_buf[2]);
//						$display ("\n%m :\nGive the write wbe2 = %b",mem_wbe_buf[2]);
					end
					else	begin
						$display ("\n%m :\nGive the write data3 = %h\tGive the write wbe3 = %b",
									mem_wdata_buf[3],mem_wbe_buf[3]);
//						$display ("\n%m :\nGive the write wbe3 = %b",mem_wbe_buf[3]);
					end
				end
			end
			if(xxx_ram_wrdy && xxx_ram_wlast)begin
				keep_mem_rw	=	1'b0;
				if(wr_disp_open)begin
					$display ("\n%m :\nGive the last write data");
				end
			end
		end	
	end
	
end
endtask

//Write Three Double Words:
task	SDRAM_WR_3DW;
input[27:0]	wr_addr;
input[31:0]	wr_data0;
input[3:0]	wr_wbe0;
input[31:0]	wr_data1;
input[3:0]	wr_wbe1;
input[31:0]	wr_data2;
input[3:0]	wr_wbe2;

begin
	mem_wdata_buf[0]	<=	#udly	wr_data0;
	mem_wdata_buf[1]	<=	#udly	wr_data1;
	mem_wdata_buf[2]	<=	#udly	wr_data2;
	mem_wdata_buf[3]	<=	#udly	0;
	mem_wbe_buf[0]	<=	#udly	wr_wbe0;
	mem_wbe_buf[1]	<=	#udly	wr_wbe1;
	mem_wbe_buf[2]	<=	#udly	wr_wbe2;
	mem_wbe_buf[3]	<=	#udly	0;
	mem_wlast_buf[0]	<=	#udly	0;
	mem_wlast_buf[1]	<=	#udly	0;
	mem_wlast_buf[2]	<=	#udly	1;
	mem_wlast_buf[3]	<=	#udly	0;
	//----------------------------
	mem_wr_en		=	1'b1;
	mem_access_rbl	=	2'h0;
	mem_access_addr	=	wr_addr;
	keep_mem_rw		=	1'b1;
	mem_access_en	=	1'b1;
//	$display ("\n%m :\nWrite SDRAM Three Double Words");
//	$display ("\n%m :\nThe Start Address = %h\tThe Write Burst Length = 3",mem_access_addr);
	$display ("\n%m :\nThe Start Address = %h",mem_access_addr);
	while(keep_mem_rw)begin
		@(posedge mem_clk)begin
			if(xxx_ram_ack)begin
				mem_access_en	=	0;
			end
			if(xxx_ram_wrdy)begin
				if(wr_disp_open)begin
					if(mem_wbuf_rpointer == 2'b00)begin
						$display ("\n%m :\nGive the write data0 = %h\tGive the write wbe0 = %b",
									mem_wdata_buf[0],mem_wbe_buf[0]);
//						$display ("\n%m :\nGive the write wbe0 = %b",mem_wbe_buf[0]);
					end
					else if(mem_wbuf_rpointer == 2'b01)begin
						$display ("\n%m :\nGive the write data1 = %h\tGive the write wbe1 = %b",
									mem_wdata_buf[1],mem_wbe_buf[1]);
//						$display ("\n%m :\nGive the write wbe1 = %b",mem_wbe_buf[1]);
					end
					else if(mem_wbuf_rpointer == 2'b10)begin
						$display ("\n%m :\nGive the write data2 = %h\tGive the write wbe2 = %b",
									mem_wdata_buf[2],mem_wbe_buf[2]);
//						$display ("\n%m :\nGive the write wbe2 = %b",mem_wbe_buf[2]);
					end
					else	begin
						$display ("\n%m :\nGive the write data3 = %h\tGive the write wbe3 = %b",
									mem_wdata_buf[3],mem_wbe_buf[3]);
//						$display ("\n%m :\nGive the write wbe3 = %b",mem_wbe_buf[3]);
					end
				end
			end
			if(xxx_ram_wrdy && xxx_ram_wlast)begin
				keep_mem_rw	=	1'b0;
				if(wr_disp_open)begin
					$display ("\n%m :\nGive the last write data");
				end
			end
		end	
	end
	
end
endtask

//Write Four Double Words:
task	SDRAM_WR_4DW;
input[27:0]	wr_addr;
input[31:0]	wr_data0;
input[3:0]	wr_wbe0;
input[31:0]	wr_data1;
input[3:0]	wr_wbe1;
input[31:0]	wr_data2;
input[3:0]	wr_wbe2;
input[31:0]	wr_data3;
input[3:0]	wr_wbe3;

begin
	mem_wdata_buf[0]	<=	#udly	wr_data0;
	mem_wdata_buf[1]	<=	#udly	wr_data1;
	mem_wdata_buf[2]	<=	#udly	wr_data2;
	mem_wdata_buf[3]	<=	#udly	wr_data3;
	mem_wbe_buf[0]	<=	#udly	wr_wbe0;
	mem_wbe_buf[1]	<=	#udly	wr_wbe1;
	mem_wbe_buf[2]	<=	#udly	wr_wbe2;
	mem_wbe_buf[3]	<=	#udly	wr_wbe3;
	mem_wlast_buf[0]	<=	#udly	0;
	mem_wlast_buf[1]	<=	#udly	0;
	mem_wlast_buf[2]	<=	#udly	0;
	mem_wlast_buf[3]	<=	#udly	1;
	//----------------------------
	mem_wr_en		=	1'b1;
	mem_access_rbl	=	2'h0;
	mem_access_addr	=	wr_addr;
	keep_mem_rw		=	1'b1;
	mem_access_en	=	1'b1;
//	$display ("\n%m :\nWrite SDRAM Four Double Words");
//	$display ("\n%m :\nThe Start Address = %h\tThe Write Burst Length = 4",mem_access_addr);
	$display ("\n%m :\nThe Start Address = %h",mem_access_addr);
	while(keep_mem_rw)begin
		@(posedge mem_clk)begin
			if(xxx_ram_ack)begin
				mem_access_en	=	0;
			end
			if(xxx_ram_wrdy)begin
				if(wr_disp_open)begin
					if(mem_wbuf_rpointer == 2'b00)begin
						$display ("\n%m :\nGive the write data0 = %h\tGive the write wbe0 = %b",
									mem_wdata_buf[0],mem_wbe_buf[0]);
//						$display ("\n%m :\nGive the write wbe0 = %b",mem_wbe_buf[0]);
					end
					else if(mem_wbuf_rpointer == 2'b01)begin
						$display ("\n%m :\nGive the write data1 = %h\tGive the write wbe1 = %b",
									mem_wdata_buf[1],mem_wbe_buf[1]);
//						$display ("\n%m :\nGive the write wbe1 = %b",mem_wbe_buf[1]);
					end
					else if(mem_wbuf_rpointer == 2'b10)begin
						$display ("\n%m :\nGive the write data2 = %h\tGive the write wbe2 = %b",
									mem_wdata_buf[2],mem_wbe_buf[2]);
//						$display ("\n%m :\nGive the write wbe2 = %b",mem_wbe_buf[2]);
					end
					else	begin
						$display ("\n%m :\nGive the write data3 = %h\tGive the write wbe3 = %b",
									mem_wdata_buf[3],mem_wbe_buf[3]);
//						$display ("\n%m :\nGive the write wbe3 = %b",mem_wbe_buf[3]);
					end
				end
			end
			if(xxx_ram_wrdy && xxx_ram_wlast)begin
				keep_mem_rw	=	1'b0;
				if(wr_disp_open)begin
					$display ("\n%m :\nGive the last write data");
				end
			end
		end	
	end
	
end
endtask

endmodule


