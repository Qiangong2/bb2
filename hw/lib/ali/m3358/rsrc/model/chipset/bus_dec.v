// snow 1999-11-03
//
// MIPS Bus Decode for WinCE Emulation
//
// Version 0.1	---	1> decode for RAM, ROM, IO
//			2> fast mode
//			3> roger io space is extend into 16 words

module 	bus_dec	(
		sysad,
		syscmd,
		evalid_,
		pvalid_,
		eok_,
		preq_,
		ereq_,
		pmaster_,

		eeprom_data,
		eeprom_addr,
//		eeprom_oe_,
		eeprom_cs_,

		io_addr,
		io_data,
		io_cs_,
		io_oe_,
		io_we_,

		sdram_addr,
		sdram_data,
		sdram_dqm,
		sdram_cs_,
		sdram_oe_,
		sdram_we_,
		last_end,

		tclk,
		rst_
		);

inout	[31:0]	sysad;
inout	[4:0]	syscmd;
output		evalid_;
input		pvalid_;
output		eok_;
input		preq_;
output		ereq_;
input		pmaster_;

input	[31:0]	eeprom_data;
output	[20:2]	eeprom_addr;
//output		eeprom_oe_;
output		eeprom_cs_;

output	[5:2]	io_addr;
inout	[31:0]	io_data;
output		io_cs_;
output		io_oe_;
output		io_we_;

output	[27:2]	sdram_addr;
inout	[31:0]	sdram_data;
output	[3:0]	sdram_dqm;
output		sdram_cs_;
output		sdram_oe_;
output		sdram_we_;
output		last_end;

input		tclk;
input		rst_;

parameter udly  = 1;

assign ereq_   = 1;                     // no extenal request

reg  [31:0]     in_sysad;
reg  [4:0]      in_syscmd;
reg             master_proc;  // processor write or read
reg		rw_discard;
reg		rw_discard_d; // SN19981031
wire		cmd_phase;
wire		dat_phase;
wire		eok_;
reg		d_eok_;

always @(posedge tclk or negedge rst_)
  if (!rst_)
     d_eok_ <= #udly 1'b0;
  else
     d_eok_ <= #udly eok_;

always @(posedge tclk or negedge rst_)
  if (!rst_)
     rw_discard  <= #udly 1'b0;
  else if (!pvalid_ && syscmd[4] == 1'b0) 	// SN19981031
     rw_discard  <= #udly (eok_ | d_eok_);

always @(posedge tclk or negedge rst_)
  if (!rst_) begin
     in_sysad <= #udly 32'h0;
     in_syscmd <= #udly 5'h0;
  end
  else if (~pvalid_) begin
     in_sysad <= #udly sysad[31:0];
     in_syscmd <= #udly syscmd[4:0];
  end

always @(posedge tclk or negedge rst_)
  if (!rst_)
     master_proc <= #udly 1'b0;
  else if (~pvalid_)
     master_proc <= #udly 1'b1;
  else
     master_proc <= #udly 1'b0;

assign  cmd_phase = in_syscmd[4] == 1'b0 && master_proc && ~rw_discard;
assign  dat_phase = in_syscmd[4] == 1'b1 && master_proc && ~rw_discard;

wire	[3:0]	rdata_cnt;
wire	[31:28]	wdata_cnt;

wire    pread_cmd   = cmd_phase && in_syscmd[3] == 1'b0;
wire    blk2w_read  = pread_cmd && in_syscmd[2:0] == 3'b100;
wire    blk4w_read  = pread_cmd && in_syscmd[2:0] == 3'b101;
wire    blk8w_read  = pread_cmd && in_syscmd[2:0] == 3'b110;
wire    sgl1b_read  = pread_cmd && in_syscmd[2:0] == 3'b000;
wire    sgl2b_read  = pread_cmd && in_syscmd[2:0] == 3'b001;
wire    sgl3b_read  = pread_cmd && in_syscmd[2:0] == 3'b010;
wire    sgl4b_read  = pread_cmd && in_syscmd[2:0] == 3'b011;

wire    pwrite_cmd  = cmd_phase  && in_syscmd[3] == 1'b1;
wire    blk2w_write = pwrite_cmd && in_syscmd[2:0] == 3'b100;
wire    blk4w_write = pwrite_cmd && in_syscmd[2:0] == 3'b101;
wire    blk8w_write = pwrite_cmd && in_syscmd[2:0] == 3'b110;
wire    sgl1b_write = pwrite_cmd && in_syscmd[2:0] == 3'b000;
wire    sgl2b_write = pwrite_cmd && in_syscmd[2:0] == 3'b001;
wire    sgl3b_write = pwrite_cmd && in_syscmd[2:0] == 3'b010;
wire    sgl4b_write = pwrite_cmd && in_syscmd[2:0] == 3'b011;

assign  rdata_cnt   = blk2w_read                            ? 4'b1100 :
		   blk4w_read			         ? 4'b1101 :
		   blk8w_read			         ? 4'b1110 : // multi words
                         (sgl1b_read && in_sysad[1:0] == 2'b00) ? 4'b0000 :
                         (sgl1b_read && in_sysad[1:0] == 2'b01) ? 4'b0001 :
                         (sgl1b_read && in_sysad[1:0] == 2'b10) ? 4'b0010 :
                         (sgl1b_read && in_sysad[1:0] == 2'b11) ? 4'b0011 :
                         (sgl2b_read && in_sysad[1:0] == 2'b00) ? 4'b0100 :
                         (sgl2b_read && in_sysad[1:0] == 2'b10) ? 4'b0101 :
                         (sgl3b_read && in_sysad[1:0] == 2'b00) ? 4'b0110 :
                         (sgl3b_read && in_sysad[1:0] == 2'b01) ? 4'b0111 :
                         (sgl4b_read && in_sysad[1:0] == 2'b00) ? 4'b1000 : 4'b1111;

assign wdata_cnt   = blk2w_write                            ? 4'b1100 :
			   blk4w_write			          ? 4'b1101 :
			   blk8w_write			          ? 4'b1110 :// multi words
                          (sgl1b_write && in_sysad[1:0] == 2'b00) ? 4'b0000 :
                          (sgl1b_write && in_sysad[1:0] == 2'b01) ? 4'b0001 :
                          (sgl1b_write && in_sysad[1:0] == 2'b10) ? 4'b0010 :
                          (sgl1b_write && in_sysad[1:0] == 2'b11) ? 4'b0011 :
                          (sgl2b_write && in_sysad[1:0] == 2'b00) ? 4'b0100 :
                          (sgl2b_write && in_sysad[1:0] == 2'b10) ? 4'b0101 :
                          (sgl3b_write && in_sysad[1:0] == 2'b00) ? 4'b0110 :
                          (sgl3b_write && in_sysad[1:0] == 2'b01) ? 4'b0111 :
                          (sgl4b_write && in_sysad[1:0] == 2'b00) ? 4'b1000 : 4'b1111;

reg	      dimm0_cs,
	      sysio_cs,
	      bootrom_cs;

//assign #udly  dimm0_cs    = (in_sysad[29:20] == 10'h000);   	// 256K Words
//assign #udly  sysio_cs    = (in_sysad[29:5]  == 25'h088_0000); 	// 8
//assign #udly  bootrom_cs  = (in_sysad[29:20] == 10'h1FC);   	// 256K Words

wire	sysio_select = sysad[29:6] == 24'h440000;

always @(posedge tclk or negedge rst_)
	if (!rst_) begin
		dimm0_cs <= 1'b0;
		sysio_cs <= 1'b0;
		bootrom_cs <= 1'b0;
	end
	else if (~pvalid_ & ~syscmd[4]) begin
// roger
		dimm0_cs <= #udly sysad[29:23] == 7'h00;
//		dimm0_cs <= #udly (sysad[29:28] == 2'b00) | (sysad[29:28] == 2'b01) & ~sysad[23] & ~ sysio_select;
//		sysio_cs <= #udly sysad[29:5]  == 25'h088_0000;
		sysio_cs <= #udly (sysad[29:6] == 24'h44_0000 | // 1100_0000
				   sysad[29:6] == 24'h40_0208 | // 1000_8200
				   sysad[29:6] == 24'h60_0050 ); // 1800_1400
//		bootrom_cs <= #udly sysad[29:21] == 9'h0FE;
		bootrom_cs <= #udly sysad[29:22] == 8'h7F;	//1fc0_0000 ~ 1fff_ffff
	end

reg	[27:2]	in_addr;
reg	sdram_cs_;
wire	sdram_oe_, sdram_we_;
reg	io_cs_;
wire	io_oe_, io_we_;
reg	eeprom_cs_;
reg	bus_err_cs_;
wire    bus_err_we_, bus_err_oe_;
wire	extlast_dat;
wire	pwrite_last;
wire	last_rw;
wire	last_end;
reg	read_req;
reg	write_req;

assign	last_rw	  = !(sdram_oe_ & io_oe_ & eeprom_cs_ & bus_err_oe_) ? extlast_dat : pwrite_last;
assign	last_end  = last_rw & ~(sdram_cs_ & io_cs_ & eeprom_cs_ & bus_err_cs_);
//assign	read_req  = in_syscmd[3] == 1'b0;
//assign	write_req = in_syscmd[3] == 1'b1;

always @(posedge tclk or negedge rst_)
	if (!rst_)
		read_req <= 1'b0;
	else if (cmd_phase)
		read_req <= #udly in_syscmd[3] == 1'b0;

always @(posedge tclk or negedge rst_)
	if (!rst_)
		write_req <= 1'b0;
	else if (cmd_phase)
		write_req <= #udly in_syscmd[3] == 1'b1;

always @(posedge tclk or negedge rst_)
	if (!rst_)
		in_addr		<= 28'h0;
	else if (cmd_phase)
		in_addr		<= #udly in_sysad[27:2];
	else if (last_end)
		in_addr		<= #udly 28'h0;

always @(posedge tclk or negedge rst_)
	if (!rst_) begin
		sdram_cs_	<= 1'b1;
		io_cs_		<= 1'b1;
		eeprom_cs_	<= 1'b1;
		bus_err_cs_	<= 1'b1;
	end
	else if (~pvalid_ & syscmd[4] | pread_cmd) begin
		if (dimm0_cs)
			sdram_cs_	<= #udly 1'b0;
		else if (sysio_cs)
			io_cs_	<= #udly 1'b0;
		else if (bootrom_cs)
			eeprom_cs_	<= #udly 1'b0;
		else
			bus_err_cs_	<= #udly 1'b1;
	end
	else if (read_req & last_end | write_req) begin
		sdram_cs_	<= #udly 1'b1;
		io_cs_		<= #udly 1'b1;
		eeprom_cs_	<= #udly 1'b1;
		bus_err_cs_	<= #udly 1'b1;
	end

assign	sdram_we_ 	= write_req ? sdram_cs_ : 1'b1;
assign	io_we_		= write_req ? io_cs_    : 1'b1;
assign  bus_err_we_	= write_req ? bus_err_cs_ : 1'b1;

assign	sdram_oe_ 	= read_req ? sdram_cs_ : 1'b1;
assign	io_oe_		= read_req ? io_cs_    : 1'b1;
assign  bus_err_oe_	= read_req ? bus_err_cs_ : 1'b1;

/*
always @(posedge tclk or negedge rst_)
	if (!rst_) begin
		sdram_oe_	<= 1'b1;
		io_oe_		<= 1'b1;
		bus_err_oe_	<= 1'b1;
	end
	else if (pread_cmd)	begin
		if (dimm0_cs)
			sdram_oe_	<= #udly 1'b0;
		else if (sysio_cs)
			io_oe_		<= #udly 1'b0;
		else if (bootrom_cs)
			bus_err_oe_	<= #udly 1'b1;
		else
			bus_err_oe_	<= #udly 1'b0;

	end
	else if (extlast_dat & (sdram_oe_ == 1'b0 | io_oe_ == 1'b0 | bus_err_oe_ == 1'b0)) begin
		sdram_oe_	<= #udly 1'b1;
		io_oe_		<= #udly 1'b1;
		bus_err_oe_	<= #udly 1'b1;
	end

always @(posedge tclk or negedge rst_)
	if (!rst_) begin
		sdram_we_	<= 1'b1;
		io_we_		<= 1'b1;
		bus_err_we_	<= 1'b1;
	end
	else if (pwrite_cmd)	begin
		if (dimm0_cs)
			sdram_we_	<= #udly 1'b0;
		else if (sysio_cs)
			io_we_		<= #udly 1'b0;
		else if (bootrom_cs)
			bus_err_we_	<= #udly 1'b1;
		else
			bus_err_we_	<= #udly 1'b0;
	end
	else if (pwrite_last & (sdram_we_ == 1'b0 | io_we_ == 1'b0 | bus_err_we_ == 1'b0)) begin
		sdram_we_	<= #udly 1'b1;
		io_we_		<= #udly 1'b1;
		bus_err_we_	<= #udly 1'b1;
	end
*/

reg    [2:0]  pwr_length;        // pwr_last;
reg    [2:0]  prd_length;
reg    [3:0]  prd_be, pwr_be;

always @(posedge tclk or negedge rst_)
  if (!rst_)
     pwr_length <= #udly 3'b111;
  else if (cmd_phase) begin
          if (wdata_cnt[31:30] != 2'b11)
             pwr_length <= #udly 3'b0;         // 1 word
          else begin
             case (wdata_cnt[29:28])
               2'b00:   pwr_length[2:0] <= #udly 3'b001;        // 2 words
               2'b01:   pwr_length[2:0] <= #udly 3'b011;        // 4 words
               2'b10:   pwr_length[2:0] <= #udly 3'b111;        // 8 words
               default: pwr_length[2:0] <= #udly 3'b000;
             endcase
          end
  end
  else if ((bus_err_we_ == 1'b0 | sdram_we_ == 1'b0) && pwr_length != 3'b0)
     pwr_length <= #udly pwr_length - 1;

always @(posedge tclk or negedge rst_)
  if (!rst_)
     prd_length <= #udly 3'b111;
  else if (cmd_phase) begin
          if (rdata_cnt[3:2] != 2'b11)
             prd_length <= #udly 3'b0;         // 1 word
          else begin
             case (rdata_cnt[1:0])
               2'b00:   prd_length[2:0] <= #udly 3'b001;        // 2 words
               2'b01:   prd_length[2:0] <= #udly 3'b011;        // 4 words
               2'b10:   prd_length[2:0] <= #udly 3'b111;        // 8 words
               default: prd_length[2:0] <= #udly 3'b000;
             endcase
          end
  end
  else if ((bus_err_oe_ == 1'b0 | sdram_oe_ == 1'b0) && prd_length != 3'b0)
     prd_length <= #udly prd_length - 1;


assign  pwrite_last     = pwr_length == 3'b0;
assign  extlast_dat     = prd_length == 3'b0;

assign  evalid_	     = sdram_oe_ & eeprom_cs_ & io_oe_ & bus_err_oe_;
assign  eok_	     = 1'b0;

assign  sdram_addr  = in_addr[27:2];
assign  io_addr     = in_addr[5:2];
assign  eeprom_addr = in_addr[20:2];
assign  sdram_dqm   = sdram_we_ == 1'b0 ? pwr_be :
			   sdram_oe_ == 1'b0 ? prd_be : 4'b1111;

// assign #udly sdram_data    = sdram_we_    == 1'b0 ? in_sysad[31:0] : 32'hz;
// assign #udly io_data       = io_we_       == 1'b0 ? in_sysad[31:0] : 32'hz;

/*
assign #udly sdram_data    = sdram_we_    == 1'b0 ? {in_sysad[7:0],
													 in_sysad[15:8],
													 in_sysad[23:16],
													 in_sysad[31:24]
													} : 32'hz;
assign #udly io_data       = io_we_       == 1'b0 ? {in_sysad[7:0],
													 in_sysad[15:8],
													 in_sysad[23:16],
													 in_sysad[31:24]
													} : 32'hz;
*/
assign  sdram_data = sdram_we_ == 1'b0 ? in_sysad : 32'hz;
assign  io_data = io_we_ == 1'b0 ? in_sysad : 32'hz;

wire 	[31:0]	sysad_tmp;
wire	[31:0]	sysad_tmp1;
wire	[4:0]	syscmd_tmp;

assign 	sysad_tmp1	= sdram_oe_    == 1'b0 ? sdram_data[31:0] :
				  eeprom_cs_   == 1'b0 ? eeprom_data[31:0] :
				  io_oe_       == 1'b0 ? io_data[31:0] : 32'h0;
// assign			sysad_tmp = {sysad_tmp1[7:0],sysad_tmp1[15:8],sysad_tmp1[23:16],sysad_tmp1[31:24]};

assign	sysad_tmp = sysad_tmp1;

// assign #udly 	syscmd_tmp	= {1'b1, ~extlast_dat, 1'b0, bus_err, 1'b1};

assign  	syscmd_tmp	= {1'b1, ~extlast_dat, 1'b0, 1'b0, 1'b1};
					// Charlse 1999-11-05

assign  	sysad		= evalid_ == 1'b0 ? sysad_tmp : 32'hz;
assign     	syscmd		= evalid_ == 1'b0 ? syscmd_tmp : 5'hz;

always @(posedge tclk or negedge rst_)
  if (!rst_) begin
     pwr_be     <= #udly 4'h0;
  end
  else if (pwrite_cmd) begin
          if (wdata_cnt[31] == 1)
             pwr_be     <= #udly 4'hF;
          else begin
                 case (wdata_cnt[30:28])
                   3'b000: begin
                              pwr_be     [3:0] <= #udly 4'h1; // 1 byte, [7:0]
                            end
                   3'b001: begin
                              pwr_be     [3:0] <= #udly 4'h2; // 1 byte, [15:8]
                            end
                   3'b010: begin
                              pwr_be     [3:0] <= #udly 4'h4; // 1 byte, [23:16]
                            end
                   3'b011: begin
                              pwr_be     [3:0] <= #udly 4'h8; // 1 byte, [31:24]
                            end
                   3'b100: begin
                              pwr_be     [3:0] <= #udly 4'h3; // 2 bytes, [15:0]
                            end
                   3'b101: begin
                              pwr_be     [3:0] <= #udly 4'hc; // 2 bytes, [31:16]
                            end
                   3'b110: begin
                              pwr_be     [3:0] <= #udly 4'h7; // 3 bytes, [23:0]
                            end
                   3'b111: begin
                              pwr_be     [3:0] <= #udly 4'he; // 3 bytes, [31:8]
                            end
                   default: begin
                              pwr_be     [3:0] <= #udly 4'h0; // 0 byte
                            end
                 endcase
    end
  end

always @(posedge tclk or negedge rst_)
  if (!rst_) begin
     prd_be     <= #udly 4'h0;
  end
  else if (pread_cmd) begin
           if (rdata_cnt[3:2] == 2'b11) begin
              prd_be     <= #udly 4'h0;
           end
           else if (rdata_cnt[3:2] == 2'b10) begin
              prd_be     <= #udly 4'hF;
           end
           else begin
                  case (rdata_cnt[2:0])
                    3'b000: begin
                              prd_be     [3:0] <= #udly 4'h1; // 1 byte, [7:0]
                            end
                    3'b001: begin
                              prd_be     [3:0] <= #udly 4'h2; // 1 byte, [15:8]
                            end
                    3'b010: begin
                              prd_be     [3:0] <= #udly 4'h4; // 1 byte, [23:16]
                            end
                    3'b011: begin
                              prd_be     [3:0] <= #udly 4'h8; // 1 byte, [31:24]
                            end
                    3'b100: begin
                              prd_be     [3:0] <= #udly 4'h3; // 2 bytes, [15:0]
                            end
                    3'b101: begin
                              prd_be     [3:0] <= #udly 4'hc; // 2 bytes, [31:16]
                            end
                    3'b110: begin
                              prd_be     [3:0] <= #udly 4'h7; // 3 bytes, [23:0]
                            end
                    3'b111: begin
                              prd_be     [3:0] <= #udly 4'he; // 3 bytes, [31:8]
                            end
                   default: begin
                              prd_be     [3:0] <= #udly 4'h0; // 0 byte
                            end
                 endcase
    end
  end

endmodule
