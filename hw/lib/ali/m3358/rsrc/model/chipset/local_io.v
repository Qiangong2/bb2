// snow 1999-11-03
//
// Local IO device for WinCE Emulation
//
// Version 0.1	---	1> For output program status
//			2> Physical Address: 1100_0000 ~ 1100_0040
//			3> 1999-11-25 Roger extend another 8 registers,
//			  Then 1100_0000 ~ 1100_003c
//			4> 2002-08-15 Yuky, add the interrupt signals(INT_ and NMI_)

module	local_io (io_addr,
		  io_data,
		  io_cs_,
		  io_oe_,
		  io_we_,
		  sel_io,    // roger
		  int_,
		  nmi_,
		  io_clk
		);

inout	[31:0]	io_data;
input	[5:2]	io_addr;
input		io_cs_;
input		io_oe_;
input		io_we_;
input		io_clk;
input		sel_io;
//interruput bus
output [4:0] int_;
output		 nmi_;

parameter	udly	= 1;

integer	io_file;

initial begin
//	io_file = $fopen("wince_io.file");
//	$fdisplay(io_file,"WinCE RTL Emulation IO Dump File !!!");
//	$fdisplay(io_file,"");
end

reg	[31:0]	io_reg0, io_reg1, io_reg2, io_reg3;
reg	[31:0]	io_reg4, io_reg5, io_reg6, io_reg7;
reg	[31:0]	io_reg8, io_reg9, io_rega, io_regb;
reg	[31:0]	io_regc, io_regd, io_rege, io_regf;

wire	sel_io_snow = 1;	// snow 010329

assign		io_data	= (io_cs_ | io_oe_) ? 32'hz : (sel_io_snow ?	// snow 010329
			  (io_addr[5:2] == 4'b0000 ? 32'h0000_00ff :
			   io_addr[5:2] == 4'b0001 ? 32'hffff_ffff :
			   io_addr[5:2] == 4'b0010 ? 32'h0 :
			   io_addr[5:2] == 4'b0011 ? 32'h0 :		// snow 010330
			   io_addr[5:2] == 4'b0100 ? io_reg4 :
			   io_addr[5:2] == 4'b0101 ? io_reg5 :
			   io_addr[5:2] == 4'b0110 ? io_reg6 :
			   io_addr[5:2] == 4'b0111 ? io_reg7 :
			   io_addr[5:2] == 4'b1000 ? io_reg8 :
			   io_addr[5:2] == 4'b1001 ? io_reg9 :
			   io_addr[5:2] == 4'b1010 ? io_rega :
			   io_addr[5:2] == 4'b1011 ? io_regb :
			   io_addr[5:2] == 4'b1100 ? io_regc :
			   io_addr[5:2] == 4'b1101 ? io_regd :
			   io_addr[5:2] == 4'b1110 ? io_rege :
			   io_addr[5:2] == 4'b1111 ? io_regf : 32'h0) : 32'h00000010);

always @(posedge io_clk)
	if (io_cs_ == 1'b0 & io_we_ == 1'b0 & sel_io == 1'b1) begin
		if (io_addr[5:2] == 4'b0000) begin
			io_reg0	<= #udly io_data;
			$display ("\tWinCE: IO[0]_DATA=%h",io_data[31:0]);
			$fdisplay(io_file,"\tWinCE: IO[0]_DATA=%h",io_data[31:0]);
			$display ("");
			$fdisplay(io_file,"");
		end
		if (io_addr[5:2] == 4'b0001) begin
			io_reg1	<= #udly io_data;
			$display ("\tWinCE: IO[1]_DATA=%h",io_data[31:0]);
//			$fdisplay(io_file,"\tWinCE: IO[1]_DATA=%h",io_data[31:0]);
			$fwrite(io_file,"%c",io_data[7:0]);
			$display ("");
			$fdisplay(io_file,"");
		end
		if (io_addr[5:2] == 4'b0010) begin
			io_reg2	<= #udly io_data;
			$display ("\tWinCE: IO[2]_DATA=%h",io_data[31:0]);
			$fdisplay(io_file,"\tWinCE: IO[2]_DATA=%h",io_data[31:0]);
			$display ("");
			$fdisplay(io_file,"");
		end
		if (io_addr[5:2] == 4'b0011) begin
			io_reg3	<= #udly io_data;
			$display ("\tWinCE: IO[3]_DATA=%h",io_data[31:0]);
			$fdisplay(io_file,"\tWinCE: IO[3]_DATA=%h",io_data[31:0]);
			$display ("");
			$fdisplay(io_file,"");
		end
		if (io_addr[5:2] == 4'b0100) begin
			io_reg4	<= #udly io_data;
			$display ("\tWinCE: IO[4]_DATA=%h",io_data[31:0]);
			$fdisplay(io_file,"\tWinCE: IO[4]_DATA=%h",io_data[31:0]);
			$display ("");
			$fdisplay(io_file,"");
		end
		if (io_addr[5:2] == 4'b0101) begin
			io_reg5	<= #udly io_data;
			$display ("\tWinCE: IO[5]_DATA=%h",io_data[31:0]);
			$fdisplay(io_file,"\tWinCE: IO[5]_DATA=%h",io_data[31:0]);
			$display ("");
			$fdisplay(io_file,"");
		end
		if (io_addr[5:2] == 4'b0110) begin
			io_reg6	<= #udly io_data;
			$display ("\tWinCE: IO[6]_DATA=%h",io_data[31:0]);
			$fdisplay(io_file,"\tWinCE: IO[6]_DATA=%h",io_data[31:0]);
			$display ("");
			$fdisplay(io_file,"");
		end
		if (io_addr[5:2] == 4'b0111) begin
			io_reg7	<= #udly io_data;
			$display ("\tWinCE: IO[7]_DATA=%h",io_data[31:0]);
			$fdisplay(io_file,"\tWinCE: IO[7]_DATA=%h",io_data[31:0]);
			$display ("");
			$fdisplay(io_file,"");
		end
		if (io_addr[5:2] == 4'b1000)
			io_reg8 <= #udly io_data;
		if (io_addr[5:2] == 4'b1001)
			io_reg9 <= #udly io_data;
		if (io_addr[5:2] == 4'b1010)
			io_rega <= #udly io_data;
		if (io_addr[5:2] == 4'b1011)
			io_regb <= #udly io_data;
		if (io_addr[5:2] == 4'b1100) begin
			io_regc <= #udly io_data;
			case (io_data[31:0])
			32'h100:	begin
				$display ("Error_T2_rsim: SDRAM Read ERROR  \tWrong Address=%h,ReadData=%h,Expected=%h",
						io_regd[31:0],io_rege[31:0],io_regf[31:0]);
				$fdisplay(io_file,"Error_T2_rsim: SDRAM Read ERROR  \tWrong Address=%h,ReadData=%h,Expected=%h",
						io_regd[31:0],io_rege[31:0],io_regf[31:0]);
				$display ("");
				$fdisplay(io_file,"");
				end
			32'h108:	begin
				$display ("Error_T2_rsim: Local IO Read Error  \tWrong Address=%h,ReadData=%h,Expected=%h",
						io_regd[31:0],io_rege[31:0],io_regf[31:0]);
				$fdisplay(io_file,"Error_T2_rsim: Local IO Read Error  \tWrong Address=%h,ReadData=%h,Expected=%h",
						io_regd[31:0],io_rege[31:0],io_regf[31:0]);
				$display ("");
				$fdisplay(io_file,"");
				end
			32'hffff_ffff:	$display("Trigger the rsim pattern");
			//default:	$display("Error_T2_rsim: an unsupported error type %h issued!",io_data);
			endcase
		end
		if (io_addr[5:2] == 4'b1101)
			io_regd <= #udly io_data;
		if (io_addr[5:2] == 4'b1110)
			io_rege <= #udly io_data;
		if (io_addr[5:2] == 4'b1111)
			io_regf <= #udly io_data;
	end

//========== add the interrupt control part ================
reg	[4:0] 	int_;
reg			nmi_;
reg [3:0] 	nmi_active_cnt;

initial	begin
	int_ = 5'h1f;
	nmi_ = 1'b1;
	nmi_active_cnt = 0;
end

always @(posedge io_clk)
	if (io_cs_ == 1'b0 & io_we_ == 1'b0 & sel_io == 1'b1) begin
		//int_[4:0]
		if (io_addr[5:2] == 4'b1000) begin
			if (io_data == 32'h3838_0000)	begin
				int_[0]	<= #udly 0;
				$display("INT_[0] become active");
			end
			else if (io_data == 32'h8383_0000) begin
				int_[0]	<= #udly 1;
				$display("INT_[0] has been clear");
			end
		end
		if (io_addr[5:2] == 4'b1001) begin
			if (io_data == 32'h3838_0001) begin
				int_[1]	<= #udly 0;
				$display("INT_[1] become active");
			end
			else if (io_data == 32'h8383_0001) begin
				int_[1]	<= #udly 1;
				$display("INT_[1] has been clear");
			end
		end
		if (io_addr[5:2] == 4'b1010) begin
			if (io_data == 32'h3838_0002) begin
				int_[2]	<= #udly 0;
				$display("INT_[2] become active");
			end
			else if (io_data == 32'h8383_0002) begin
				int_[2]	<= #udly 1;
				$display("INT_[2] has been clear");
			end
		end
		if (io_addr[5:2] == 4'b1011) begin
			if (io_data == 32'h3838_0003) begin
				int_[3]	<= #udly 0;
				$display("INT_[3] become active");
			end
			else if (io_data == 32'h8383_0003) begin
				int_[3]	<= #udly 1;
				$display("INT_[3] has been clear");
			end
		end
		if (io_addr[5:2] == 4'b1100) begin
			if (io_data == 32'h3838_0004) begin
				int_[4]	<= #udly 0;
				$display("INT_[4] become active");
			end
			else if (io_data == 32'h8383_0004) begin
				int_[4]	<= #udly 1;
				$display("INT_[4] has been clear");
			end
		end
		//nmi_
		if (io_addr[5:2] == 4'b1101) begin
			if (io_data == 32'h3838_0005) begin
				nmi_	<= #udly 0;
				//nmi_active_cnt <= #udly 0;
				$display("NMI_ become active");
			end
			else if (io_data == 32'h8383_0005) begin
				nmi_	<= #udly 1;
				$display("NMI_ has been clear");
			end
		end
	end

//nmi_ automatically to clear
/*
always @(posedge io_clk)
	if (nmi_active_cnt == 4'hf) begin
		nmi_	<= #udly 1;
		$display("NMI_ has been clear");
	end
	else if (!nmi_)
		nmi_active_cnt <= #udly nmi_active_cnt + 1;
*/
//==========================================================
endmodule
