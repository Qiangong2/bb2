/*******************************************************************************************
//File 	: 	disp_device_bh.v -- Display Engine Behaviour Model
//								used for testing SDRAM memory interface module
//								For 4DW x 8 Frame Buffer
//Author: 	Figo
//Date	: 	2002-07-17	Initial version
*******************************************************************************************/
`timescale	1ns/100ps

module	disp_device_bh(
	//To Memory Interface:
	disp_ram_req,
	disp_ram_addr,
	disp_ram_rbl,
	//From Memory Interface:
	disp_ram_ack,
	disp_ram_err,
	disp_ram_rrdy,
	disp_ram_rlast,
	disp_ram_rdata,

	//others:
	rst_,
	mem_clk
	);

parameter	udly	=	1'b1;
//	------------------------------------------------------------------------------------
//To Memory Interface:
output			disp_ram_req;
output[27:0]	disp_ram_addr;
output[1:0]		disp_ram_rbl;
//From Memory Interface:
input			disp_ram_ack;
input			disp_ram_err;
input			disp_ram_rrdy;
input			disp_ram_rlast;
input[31:0]		disp_ram_rdata;

//others:
input			rst_,
				mem_clk;
//	--------------------------------------------------------------------------------------
reg			disp_ram_req;
reg[27:0]	disp_ram_addr;
reg[1:0]	disp_ram_rbl;


reg			rd_process_en;
reg[27:0]	mem_access_addr;
reg[31:0]	mem_access_data;


reg			rd_disp_open;

reg	rd_process, rd_process_dly;
wire	rd_process_begin;
reg		rd_process_end;
reg		rd_process_end_dly;
reg		rd_process_end_2dly;

reg[2:0]	rd_req_cnt;
wire		rd_req_end;

reg[4:0]	rdata_cnt;
reg[2:0]	rlast_cnt;
wire		rdata_cnt_end;
wire		rlast_cnt_end;

reg[31:0]	rdata_exp;


integer i;

initial	begin
	rd_disp_open	=	0;
end

initial	begin
	disp_ram_req		=	0;
	disp_ram_addr	=	0;
	disp_ram_rbl		=	0;
end


initial	begin
	rd_process_en	=	0;
	mem_access_addr	=	0;
	mem_access_data	=	0;
end

initial	begin
	rd_process			=	0;
	rd_process_dly		=	0;
	rd_req_cnt			=	3'h0;
	rdata_cnt			=	5'h0;
	rlast_cnt			=	3'h0;
	rd_process_end		=	0;
	rd_process_end_dly	=	0;
	rd_process_end_2dly	=	0;
end

initial	begin
	rdata_exp	=	32'hxxxx_xxxx;
end


always	@(posedge mem_clk or negedge rst_)
	if(!rst_)begin
		rd_process		<=	#udly	1'b0;
		rd_process_dly	<=	#udly	1'b0;
	end
	else	begin
		rd_process		<=	#udly	rd_process_en;
		rd_process_dly	<=	#udly	rd_process;
	end

assign	rd_process_begin	=	!rd_process_dly	&& rd_process;

always	@(posedge mem_clk or negedge rst_)
	if(!rst_)begin
		rd_req_cnt	<=	#udly	3'h0;
	end
	else if(rd_process_begin)begin
		rd_req_cnt	<=	#udly	3'h7;
	end
	else if(!rd_req_end && disp_ram_ack)begin
		rd_req_cnt	<=	#udly	rd_req_cnt - 1'b1;
	end

assign	rd_req_end	=	rd_req_cnt	==	3'h0;

always	@(posedge mem_clk or negedge rst_)
	if(!rst_)begin
		disp_ram_req	<=	#udly	1'b0;
		disp_ram_rbl	<=	#udly	2'h0;
	end
	else if(rd_process_begin)begin
		disp_ram_req	<=	#udly	1'b1;
		disp_ram_rbl	<=	#udly	2'b11;
	end
	else if(rd_req_end && disp_ram_ack)begin
		disp_ram_req	<=	#udly	1'b0;
		disp_ram_rbl	<=	#udly	2'b0;
	end


always	@(posedge mem_clk or negedge rst_)
	if(!rst_)begin
		disp_ram_addr	<=	#udly	28'h0;
	end
	else if(rd_process_begin)begin
		disp_ram_addr	<=	#udly	mem_access_addr;
	end
	else if(disp_ram_ack)begin
		disp_ram_addr	<=	#udly	disp_ram_addr + 4;
	end

always	@(posedge mem_clk or negedge rst_)
	if(!rst_)begin
		rdata_exp	<=	#udly	0;
	end
	else if(rd_process_begin)begin
		rdata_exp	<=	#udly	mem_access_data;
	end
	else if(disp_ram_rrdy)begin
		rdata_exp	<=	#udly	rdata_exp + 1;
	end

always	@(posedge mem_clk or negedge rst_)
	if(!rst_)begin
		rdata_cnt	<=	#udly	5'h0;
	end
	else if(rd_process_begin)begin
		rdata_cnt	<=	#udly	5'h1f;
	end
	else if(!rdata_cnt_end && disp_ram_rrdy)begin
		rdata_cnt	<=	#udly	rdata_cnt - 1'b1;
	end

always	@(posedge mem_clk or negedge rst_)
	if(!rst_)begin
		rlast_cnt	<=	#udly	3'h0;
	end
	else if(rd_process_begin)begin
		rlast_cnt	<=	#udly	3'h7;
	end
	else if(!rlast_cnt_end && disp_ram_rlast)begin
		rlast_cnt	<=	#udly	rlast_cnt - 1'b1;
	end

always	@(posedge mem_clk or negedge rst_)
	if(!rst_)begin
		rd_process_end		<=	#udly	1'b0;
		rd_process_end_dly	<=	#udly	1'b0;
		rd_process_end_2dly	<=	#udly	1'b0;
	end
	else	begin
		rd_process_end		<=	#udly	rdata_cnt_end && disp_ram_rrdy && rlast_cnt_end && disp_ram_rlast;
		rd_process_end_dly	<=	#udly	rd_process_end;
		rd_process_end_2dly	<=	#udly	rd_process_end_dly;
	end

assign	rdata_cnt_end	=	rdata_cnt == 5'h0;
assign	rlast_cnt_end	=	rlast_cnt == 3'h0;


always	@(posedge mem_clk)
	if(disp_ram_rrdy)begin
		if(!(disp_ram_rdata == rdata_exp))begin
			$display ("\n%m :\nError_T2_rsim is found:\nThe Read Data = %h\tThe Expected Data = %h",disp_ram_rdata, rdata_exp);
		end
	end
//	---------------------------------------------------------------------------------------
//Pattern Drivers:

//Dispaly Engine Behaviour Model Read Frame Buffer:
task	DISP_Device_BH_Frame_Buffer_RD;
input[27:0]		rd_addr_start;
input[31:0]		rd_data_start;

begin
	mem_access_addr	=	rd_addr_start;
	mem_access_data	=	rd_data_start;
	rd_process_en	=	1'b1;
	$display ("\n%m :\nThe Start Address = %h",mem_access_addr);
	while(rd_process_en)begin
		@(posedge mem_clk)begin
			if(rd_process_end)begin
				rd_process_en = #udly 1'b0;
			end
		end
	end
	wait(rd_process_end_2dly);

end
endtask

endmodule


