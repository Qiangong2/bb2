/*************************************************************
file name:		int_bh.v
description:	used to generate the external interrupt.
				support generate 8 interrupt signals.
				use the flash signals to accept the
				commands
				set_int_level: 	24'hff_f0e4 ~ 24'hff_f0e7
				set_int_tri:	24'hff_f0e0
author:			yuky
history:		2002-08-13	initial version
***************************************************************/
module int_bh(
	ext_int_out,	// output interrupt, default high level
	clk,
	rst_
);
parameter	udly = 1;
parameter	int_num = 32;
output	[int_num - 1:0]	ext_int_out;
input	clk;
input	rst_;

reg	[int_num - 1:0]	ext_int;
reg	ext_int_oe;
initial	begin
	ext_int = 0;
	ext_int_oe = 0;
end
wire [int_num - 1:0]	ext_int_out = ext_int_oe ? ext_int : 32'hz;

task	set_int_level;
input [int_num - 1:0]	int_level;
begin
	ext_int = int_level;
	@(posedge clk);
end
endtask

/*
task	set_int_edge;
input	int_idx;
input	edge_dly;	// pulse width
integer	int_idx, edge_dly;
begin
	@(posedge clk) ext_int[int_idx] = !ext_int[int_idx];
	repeat (edge_dly) @(posedge clk);
	ext_int[int_idx] = !ext_int[int_idx];
end
endtask
*/
wire	rom_cs_ = top.boot_rom.eeprom_cs_;
wire	rom_we_ = top.boot_rom.eeprom_we_;
wire [23:0]	rom_addr= top.boot_rom.eeprom_addr;
wire [7:0]	rom_data= top.boot_rom.dly_data;
wire	valid_we_ = rom_cs_ | rom_we_;

parameter	int_tri_addr	=	24'h1f_f0e0;//change ff_fxxx to 1f_fxxx for m3357 JFR031105
parameter	int_lev_addr0	=	24'h1f_f0e4;//change ff_fxxx to 1f_fxxx for m3357 JFR031105
parameter	int_lev_addr1	=	24'h1f_f0e5;//change ff_fxxx to 1f_fxxx for m3357 JFR031105
parameter	int_lev_addr2	=	24'h1f_f0e6;//change ff_fxxx to 1f_fxxx for m3357 JFR031105
parameter	int_lev_addr3	=	24'h1f_f0e7;//change ff_fxxx to 1f_fxxx for m3357 JFR031105

wire	tri_hit  = rom_addr == int_tri_addr;
wire	lev_hit0 = rom_addr == int_lev_addr0;
wire	lev_hit1 = rom_addr == int_lev_addr1;
wire	lev_hit2 = rom_addr == int_lev_addr2;
wire	lev_hit3 = rom_addr == int_lev_addr3;

reg		[int_num - 1:0] int_lev;

always	@(posedge valid_we_)
	if (rst_) begin
		if(lev_hit0)
			int_lev[7:0] <= #udly rom_data;
		if(lev_hit1)
			int_lev[15:8] <= #udly rom_data;
		if(lev_hit2)
			int_lev[23:16] <= #udly rom_data;
		if(lev_hit3)
			int_lev[31:24] <= #udly rom_data;
	end

parameter	oe_value = 8'h33;
wire	oe_hit	=	rom_data == oe_value;
always	@(posedge valid_we_)
	if (rst_ && tri_hit) begin
		if (oe_hit) begin
			ext_int_oe <= #udly 1;
			set_int_level(int_lev);
		end else
			ext_int_oe <= #udly 0;
	end

endmodule
