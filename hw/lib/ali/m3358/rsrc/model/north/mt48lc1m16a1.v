/****************************************************************************************
*
*    File Name:  MT48LC1M16A1.V
*      Version:  1.8
*         Date:  May 15th, 1998
*        Model:  BUS Functional
*    Simulator:  Model Technology VLOG (PC version 4.7h)
*
* Dependencies:  None
*
*       Author:  Son P. Huynh
*        Email:  sphuynh@micron.com
*        Phone:  (208) 368-3825
*      Company:  Micron Technology, Inc.
*        Model:  MT48LC1M16A1 (1Meg x 16)
*
*   Limitation:  - CAS latency of 1 not supported.
*                - No tWR violation check
*
*   Disclaimer:  THESE DESIGNS ARE PROVIDED "AS IS" WITH NO WARRANTY 
*                WHATSOEVER AND MICRON SPECIFICALLY DISCLAIMS ANY 
*                IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
*                A PARTICULAR PURPOSE, OR AGAINST INFRINGEMENT.
*
*                Copyright (c) 1997 Micron Semiconductor Products, Inc.
*                All rights researved
*
* Rev  Author                        Date        Changes
* ---  ----------------------------  ----------  ---------------------------------------
* 1.8  Larice Robert                 05/15/1998  Fix CKE for clock suspend during read
*      Larice@vidisys.de                            or write burst              
*
* 1.7  Son Huynh  208-368-3825       03/31/1998  Fix t_RP and t_RCD timing check
*      Micron Technology, Inc.                   Fix Read/Write with AP enable
*                                                Add t_DS and t_DH timing check
*
* 1.6  Son Huynh  208-368-3825       02/10/1998  Fix Read/Write Burst length of 1
*      Micron Technology, Inc.                   - Binary Burst Counter
*
* 1.5  Son Huynh  208-368-3825       01/19/1998  Fix Read/Write Burst length of 1
*      Micron Technology, Inc.                   - Fix Precharge bit in Full Page Burst
*                                                - Add Burst Read Single Write
*
* 1.4  Son Huynh  208-368-3825       11/18/1997  Convert to 1Meg x 16 SDRAM
*      Micron Technology, Inc.                   - Dqm [1 : 0] = {DQMH, DQML}
*
* 1.3  Son Huynh  208-368-3825       11/14/1997  Fix burst termination
*      Micron Technology, Inc.                   - Improved the column sizing method
*                                                  for x4 and x8 using parameters below
*
* 1.2  Son Huynh  208-368-3825       11/06/1997  Convert to SDRAM Verilog model
*      Micron Technology, Inc.                   - Remove Block Write
*                                                - Remove Load Special Mode Register
*
* 1.1  Son Huynh  208-368-3825       10/15/1997  Add timing checks
*      Micron Technology, Inc.                   - Fix several bugs
*                                                - Add simple testbench vectors
*
* 1.0  Jason Chow 408-573-8880 x102  10/13/1997  This model is converted from Micron's
*      Allayer Technologies Corp.                VHDL sgram model.  All file load/unload
*                                                and timing check had been taken out.
*
****************************************************************************************/

module mt48lc1m16a1 (Add, We_n, Cas_n, Ras_n, Clk, Cke, Cs_n, Dqm, Ba, Dq);

    parameter                     add_bits = 11;                        // add_bits    = 11
    parameter                     datapath = 16;                        // datapath    = 16
    parameter                     col_add_bits = 8;                     // col_add_bit =  8
    parameter                     add_precharge_bit = 10;               // Precharge bit

    input    [(add_bits - 1) : 0] Add;                                  // Address Bus
    input                         We_n;                                 // Command Input: WE#    
    input                         Cas_n;                                // Command Input: CAS#
    input                         Ras_n;                                // Command Input: RAS#
    input                         Clk;                                  // Clock
    input                         Cke;                                  // Clock Enable
    input                         Cs_n;                                 // Chip Select  : CS#
    input                 [1 : 0] Dqm;                                  // Input/Output Mask
    input                         Ba;                                   // Bank Address

    inout    [(datapath - 1) : 0] Dq;                                   // Data I/O: Data Bus

    parameter                     Read         = 4'b0000;               // Command_state
    parameter                     Write        = 4'b0001;
    parameter                     Nop          = 4'b0010;
    parameter                     Illegal      = 4'b0011;
    parameter                     Precharge    = 4'b0100;
    parameter                     Active       = 4'b0101;
    parameter                     Idle         = 4'b0110;
    parameter                     Mode_Reg_Set = 4'b0111;
    parameter                     Auto_Ref     = 4'b1000;
    parameter                     Self_Ref     = 4'b1001;
    parameter                     Read_A       = 4'b1010;
    parameter                     Write_A      = 4'b1011;
    parameter                     Bst_Term     = 4'b1100;

    parameter                     t_ac         = 2;                    // ns
    parameter                     t_hz         = 3;                    // ns

    reg                           initialize;
    reg      [(datapath - 1) : 0] Dq_out;
    wire                          Prech_enable, Mode_reg_enable;
    reg                           Sys_clk;
    wire                          Bank_select_enable, Bank_active_enable, Read_1_enable, Col_bank_select;
    wire                          We_int, Data_in_enable, Write_1_enable, Rw_chain_enable, Burst_term;
    wire                          Data_count_select, Data_out_enable, Lat_select, Ap_int_select;
    reg                           Col_bank_select_int, Pc_b0, Pc_b1;
    wire                          Ap_int_signal;
    wire                          Data_count_enable;
    reg                           Read_0_L, Read_0, Read_1, Write_0, Write_1;
    wire                          Row_add_enable;
    reg                           Bank_select_0, Bank_select_1, Data_out_enable_reg;
    wire                          Burst_count_enable;
    reg                           Data_count_read_reg;
    wire     [(datapath - 1) : 0] Read_data;
    reg      [(datapath - 1) : 0] Read_data_0, Read_data_1, Write_data, Write_data_array;
    reg      [(add_bits - 1) : 0] B0_row_add, B1_row_add, Row_add;
    wire [(col_add_bits - 1) : 0] Col_add_int, Col_add_int_0, Col_add_int_1;
    reg  [(col_add_bits - 1) : 0] Col_add, Col_add_sig0, Col_add_sig1;
    reg                   [1 : 0] Dqm_0_int, Dqm_1_int;
    reg            [add_bits : 0] Mode_reg;
    reg  [(col_add_bits - 1) : 0] B0_col_add, B1_col_add, B0_col_add_array, B1_col_add_array;
    reg                   [2 : 0] Data_out_count;
    wire                          Ap_0, Ap_1, Ap_2, Ap_3, Ap_4, Ap_5, Ap_6, Ap_7, Ap_8;
    wire                          Ap_int, Ap_int0, Ap_int1, Ap_int2, Ap_int3, Ap_int4, Ap_int5, Ap_int6, Ap_int7;
    reg                           Apr_0, Apr_1, Apr_2, Apr_3, Apr_4, Apr_5, Apr_6, Apr_7, Apr_8;
    wire                          D_c_zero0;
    reg                           D_c_zero1;
    wire                          Data_read_count_enable, Bank_op;
    wire                          Ras_in, Cas_in, We_in;
    reg                           Data_count_write_reg;
    wire                          Burst_term_f;
    reg                           Burst_term_2, Burst_term_1;
    reg                   [3 : 0] Command;

    wire                          Burst_length_1, Burst_length_2, Burst_length_4, Burst_length_8;
    wire                          Cas_lat_2, Cas_lat_3;

    event                         rw_chk;
    
    initial begin
        initialize = 1'b0;
        Dq_out = 16'hz;
        {Col_bank_select_int, Pc_b0, Pc_b1} = 3'b100;
        {Read_0_L, Read_0, Read_1, Write_0, Write_1} = 5'b11111;
        {Bank_select_0, Bank_select_1, Data_out_enable_reg, Data_count_read_reg} = 4'b1111;
        {Read_data_0, Read_data_1, Write_data, Write_data_array} = 64'b0;
        {B0_row_add, B1_row_add, Row_add} = 33'b0;
        {Col_add, Col_add_sig0, Col_add_sig1} = 24'b0;
        {Dqm_0_int, Dqm_1_int} = 4'b1111;
        Mode_reg = 12'b0;
        {B0_col_add, B1_col_add, B0_col_add_array, B1_col_add_array} = 32'b0;
        Data_out_count = 3'b0;
        {Apr_0, Apr_1, Apr_2, Apr_3, Apr_4, Apr_5, Apr_6, Apr_7, Apr_8} = 9'b0;
        {D_c_zero1, Data_count_write_reg, Burst_term_2, Burst_term_1} = 4'b1111;
        Command = Nop;
        $timeformat (-9, 1, " ns", 10);
    end

    assign Dq = Dq_out;

    // CKE fix for clock syspend during read or write
    reg CkeZ;
    always begin
        @ (posedge Clk) begin
            Sys_clk = CkeZ;
            CkeZ = Cke;
        end
        @ (negedge Clk) begin
            Sys_clk = 0;
        end
    end

    // For checking Dq setup/hold time
    not (not_Data_in_enable, Data_in_enable);
    and (Enable_data_in, not_Data_in_enable, Cke);

    //Cs Decode
    assign Ras_in = ~Cs_n ? Ras_n : 1'b1;
    assign Cas_in = ~Cs_n ? Cas_n : 1'b1;
    assign We_in  = ~Cs_n ? We_n  : 1'b1;

    // Burst Length Decode
    assign Burst_length_1 = Mode_reg[2] |  Mode_reg[1] |  Mode_reg[0];
    assign Burst_length_2 = Mode_reg[2] |  Mode_reg[1] | ~Mode_reg[0];
    assign Burst_length_4 = Mode_reg[2] | ~Mode_reg[1] |  Mode_reg[0];
    assign Burst_length_8 = Mode_reg[2] | ~Mode_reg[1] | ~Mode_reg[0];

    // CAS Latency Decode
    assign Cas_lat_2 = Mode_reg[6] | ~Mode_reg[5] |  Mode_reg[4];
    assign Cas_lat_3 = Mode_reg[6] | ~Mode_reg[5] | ~Mode_reg[4];

    //Peripheral logic
    assign Prech_enable = Ras_in | ~(Cas_in);
    assign Mode_reg_enable = Ras_in | Cas_in | We_in | ~(Pc_b0) | ~(Pc_b1);
    assign Bank_select_enable = ~(Ras_in) | Cas_in;
    assign Bank_active_enable = ~(Cas_in) | ~(We_in) | Ras_in;
    assign Row_add_enable = ((~(Ba) & Pc_b0) | (Ba & Pc_b1)) & ~Ras_in & We_in & Cas_in;
    assign Read_1_enable = (D_c_zero1) & Read_0 & Burst_term_f & Write_0;
    assign Data_read_count_enable = (D_c_zero0) & (Read_0 & (~(Ras_in) | ~(We_in))) & Burst_term;
    assign Col_bank_select = Ras_in & ~(Cas_in) & Sys_clk;
    assign We_int = Write_0 & Write_1;
    assign Data_in_enable = (We_int & Cas_in) | (We_in & ~(Cas_in));
    assign Write_1_enable = (D_c_zero1) & (Write_0 & Burst_term_2) & Read_0;
    assign Rw_chain_enable = (~(Ras_in) & ~(Cas_in));
    assign Burst_term = ~(Cas_in) | We_in | (Bank_op & ~(Ras_in) & ~(Add[add_precharge_bit]));
    assign Data_count_select = (~(Ras_in) & ~(Cas_in & ~(We_in))) | (~(Cas_in) & We_in) | (We_int & Cas_in);
    assign Burst_count_enable = ~(Cas_in);
    assign Ap_int_select = Cas_in | ~(Ras_in) | (Mode_reg[2] & Mode_reg[1] & Mode_reg[0]);

    assign Data_out_enable = (Cas_lat_2 | Read_0) &
                             (Cas_lat_3 | Read_0_L) &
                             (Cas_lat_3 | Read_1 | D_c_zero1) & 
                             (Cas_lat_2 | Read_1 | D_c_zero0) &
                             (Cas_lat_3 | Burst_length_1) & 
                             (~(Ras_in) | Cas_in | We_in) & Burst_term_f;

    assign Lat_select = Mode_reg[6] | ~(Mode_reg[5]) | ~(Mode_reg[4]);
    assign D_c_zero0 = Data_out_count[2] | Data_out_count[1] | Data_out_count[0];
    assign Bank_op = Bank_select_0 ^ Ba;

    assign Read_data = ~Lat_select ? Read_data_1 : Read_data_0;
                                     
    assign Burst_term_f = ~Lat_select ? Burst_term_2 : Burst_term_1;

    assign Data_count_enable = Data_count_select ? Data_count_read_reg : Data_count_write_reg;

    assign Col_add_int = Data_count_enable ? Col_add_sig1 : Col_add_sig0;

    assign Ap_int_signal = ~Ap_int_select ? Add[add_precharge_bit] : 1'b0;

    assign Ap_0 = ((Cas_lat_2 & Cas_lat_3) | Burst_length_1) & Ap_1;
    assign Ap_1 = Burst_length_2 & Ap_2;
    assign Ap_2 = Ap_3;
    assign Ap_3 = Burst_length_4 & Ap_4;
    assign Ap_4 = Ap_7;
    assign Ap_5 = Ap_7;
    assign Ap_6 = Ap_7;
    assign Ap_7 = Burst_length_8;
    assign Ap_8 = 1;
 
    assign Ap_int0 = Ap_0 ? Ap_int_signal : Apr_0;
    assign Ap_int1 = Ap_1 ? Ap_int0 : Apr_1;
    assign Ap_int2 = Ap_2 ? Ap_int1 : Apr_2;
    assign Ap_int3 = Ap_3 ? Ap_int2 : Apr_3;
    assign Ap_int4 = Ap_4 ? Ap_int3 : Apr_4;
    assign Ap_int5 = Ap_5 ? Ap_int4 : Apr_5;
    assign Ap_int6 = Ap_6 ? Ap_int5 : Apr_6;
    assign Ap_int7 = Ap_7 ? Ap_int6 : Apr_7;
    assign Ap_int  = Ap_8 ? Ap_int7 : Apr_8;

    //================================================
    //row_add_demux: PROCESS
    //This process demuxes the row address to the correct bank
    always @ (negedge Row_add_enable) begin
        if (Ba == 1'b0 && Pc_b0 == 1'b0) begin
              B0_row_add <= Add;   
        end else if (Ba == 1'b1 && Pc_b1 == 1'b0) begin
              B1_row_add <= Add;   
        end
    end

    //================================================
    //bank_select_mux: PROCESS
    //This process makes sure that the registered bank access continues
    //with the correct address for simulateous bank operations
    always @ (Col_bank_select) begin
        if (Col_bank_select == 1'b0) begin
            Col_bank_select_int <= Bank_select_0;
        end else if (Col_bank_select == 1'b1) begin
            Col_bank_select_int <= @ (negedge Sys_clk) Ba;
        end
    end

    //================================================
    // col_add_demux: PROCESS
    // This process demuxes the col address to the correct bank
    always @ (Col_add or Bank_select_1 or negedge Read_1 or negedge Write_1) begin
        if (Col_bank_select_int == 1'b0) begin
            B0_col_add <= Col_add;
        end else if (Col_bank_select_int == 1'b1) begin
            B1_col_add <= Col_add;
        end
    end

    //================================================
    // burst_counter: PROCESS
    reg                  [2 : 0] D_o_c_reg;
    reg [(col_add_bits - 1) : 0] Col_add_int0, Col_add_int1;
    reg                  [2 : 0] Inter_burst_count, Burst_count;
    reg [(col_add_bits - 1) : 0] Burst_start;

    initial begin
        Col_add_int0 = 8'b0;
        Col_add_int1 = 8'b0;
        Inter_burst_count = 3'b0;
        Burst_count = 3'b0;
        D_o_c_reg = 3'b0;
    end

    always @ (posedge Sys_clk) begin

        if (Cas_in == 1'b0 && Ras_in == 1'b1) begin
            Col_add_int0 [(col_add_bits - 1) : 0] = Add[(col_add_bits - 1) : 0];
            Col_add_int1 [(col_add_bits - 1) : 0] = Col_add_int0 [(col_add_bits - 1) : 0];
            Inter_burst_count = 3'b000;
        end

        if (Burst_count_enable == 1'b0) begin
            if (Mode_reg[3] == 1'b0 && ~(Data_out_count == 3'b000)) begin           // Linear Burst
                Burst_start = Col_add_int1 + 1;
                Burst_count = Burst_start;
            end else if (Mode_reg[3] == 1'b1 && ~(Data_out_count == 3'b000)) begin  // Interleaved Burst
                Inter_burst_count = Inter_burst_count + 1;
                Burst_count[2] = Inter_burst_count[2] ^ Col_add_int0[2];
                Burst_count[1] = Inter_burst_count[1] ^ Col_add_int0[1];
                Burst_count[0] = Inter_burst_count[0] ^ Col_add_int0[0];
            end
            if      (Mode_reg[2 : 0] == 3'b001) Col_add_int1[0] = Burst_count[0];
            else if (Mode_reg[2 : 0] == 3'b010) Col_add_int1[1:0] = Burst_count[1:0];
            else if (Mode_reg[2 : 0] == 3'b011) Col_add_int1[2:0] = Burst_count[2:0];
            else if (Mode_reg[2 : 0] == 3'b111) Col_add_int1 = Burst_start;
        end
   
        Col_add_sig0 <= Col_add_int0;  // column address latch
        Col_add_sig1 <= Col_add_int1;
   
        Col_add <= Col_add_int;

        //================================================
        // state_registers: PROCESS
        if (Ras_in == 1'b1 && Cas_in == 1'b0 && We_in == 1'b0 && Mode_reg[9] == 1'b1) begin
            Data_out_count <= 3'b000;      
        end else if (Ras_in == 1'b1 && Cas_in == 1'b0) begin            // preload data counter
            Data_out_count <= Mode_reg [2 : 0];      
        end else if (Data_count_enable == 1'b1 && Mode_reg[2] == 1'b0) begin
            //see model documentation for logic solution to data counter state machine
            D_o_c_reg = Data_out_count;                                 //load previous state
            Data_out_count[2] <= D_o_c_reg[1];
            Data_out_count[1] <= D_o_c_reg[0] & (D_o_c_reg[1] | D_o_c_reg[2]);
            Data_out_count[0] <= (D_o_c_reg[2] & ~(D_o_c_reg[0])) | (D_o_c_reg[0] & D_o_c_reg[1] & ~(D_o_c_reg[2]));
        end
 
        //Auto precharge variable length pipeline registers
        if (Ap_0 == 1'b0) Apr_0 <= Ap_int_signal;  
        if (Ap_1 == 1'b0) Apr_1 <= Ap_int0;
        if (Ap_2 == 1'b0) Apr_2 <= Ap_int1;
        if (Ap_3 == 1'b0) Apr_3 <= Ap_int2;
        if (Ap_4 == 1'b0) Apr_4 <= Ap_int3;
        if (Ap_5 == 1'b0) Apr_5 <= Ap_int4;
        if (Ap_6 == 1'b0) Apr_6 <= Ap_int5;
        if (Ap_7 == 1'b0) Apr_7 <= Ap_int6;
        if (Ap_8 == 1'b0) Apr_8 <= Ap_int7;
     
        //Data input latch
        if (Data_in_enable == 1'b0) begin
            Write_data <= Dq;
        end
         
        Write_data_array <= Write_data;
         
        //Read_0_L Latency Register
        Read_0_L <= Read_0;      
     
        //Burst_term_1 Register
        Burst_term_1 <= Burst_term;
        Burst_term_2 <= Burst_term_1;
         
        //Write command register 1
        if (Rw_chain_enable == 1'b0) begin
            Write_0 <= ~(Ras_in) | Cas_in | We_in;
        end
     
        //Write command register 2
        if (Write_1_enable == 1'b0) begin
            Write_1 <= Write_0;
        end
   
        //Data count end register
        D_c_zero1 <= D_c_zero0;
     
        //Data count write register
        Data_count_write_reg <= ~(Data_in_enable);
    
        //Read command register 1
        if (Rw_chain_enable == 1'b0) begin
            Read_0 <= ~(Ras_in) | Cas_in | ~(We_in);
        end
     
        //Read command register 2
        if (Read_1_enable == 1'b0) begin        
            Read_1 <= Read_0;   
        end
     
        //Data count enable (reads)
        if (Data_read_count_enable == 1'b0) begin
            Data_count_read_reg <= Cas_in | ~(We_in) | ~(Ras_in);
        end   
     
        //DQM registers
        Dqm_0_int <= Dqm;
        Dqm_1_int <= Dqm_0_int;

        // Load Mode Register     
        if ((Mode_reg_enable == 0) && (Pc_b1 == 1) && (Pc_b0 == 1)) begin
            Mode_reg[(add_bits - 1) : 0] <= Add;
            Mode_reg[add_bits] <= Ba;
        end
         
        // Address/bank registers
        if (Row_add_enable == 1'b1) begin
            Row_add <= Add;
        end
         
        if (Bank_select_enable == 1'b0) begin
            Bank_select_0 <= Ba;
        end
         
        Bank_select_1 <= Bank_select_0;
         
        // Active / Precharge Block
        if (Prech_enable == 1'b0) begin
            if (We_in == 1'b0) begin
                if (Add [add_precharge_bit] == 1'b0) begin              // Precharge Bank
                    if (Ba == 1'b0) begin
                        Pc_b0 <= 1'b1;
                    end else if (Ba == 1'b1) begin
                        Pc_b1 <= 1'b1;
                    end
                end else if (Add [add_precharge_bit] == 1'b1) begin     // Precharge All
                    Pc_b0 <= 1'b1;
                    Pc_b1 <= 1'b1;
                end
            end else if (We_in == 1'b1) begin                           // Active Bank
                if (Ba == 1'b0) begin
                    Pc_b0 <= 1'b0;
                end else if (Ba == 1'b1) begin
                    Pc_b1 <= 1'b0;
                end
            end
        end

        // Internal Auto Precharge Done    
        if (Ap_int == 1'b1) begin
            if (Bank_select_0 == 1'b0) begin
                Pc_b0 <= 1'b1;
            end else if (Bank_select_0 == 1'b1) begin
                Pc_b1 <= 1'b1;
            end
        end
     
        // Data output enable register
        if (Data_out_enable == 1'b0) begin
            if (Mode_reg[6] == 1'b0 && Mode_reg[5] == 1'b1 && Mode_reg[4] == 1'b1) begin
                Data_out_enable_reg <= Read_0_L | ~(Burst_term_f);
            end else begin
                Data_out_enable_reg <= Read_0 | ~(Burst_term_f);
            end      
        end

        // Data output register for latency of 3
        if (Lat_select == 1'b0) begin
            Read_data_1 <= Read_data_0;
        end

        // Read/Write Subroutine
	#0.1 -> rw_chk;
        
    end // ALWAYS
   
    //================================================
    // col_address_generate: PROCESS                     
    reg     [(datapath - 1) : 0] array_buffer;
    reg     [(datapath - 1) : 0] ram_0 [524287 : 0];                    // 512K x 16
    reg     [(datapath - 1) : 0] ram_1 [524287 : 0];                    // 512K x 16
    reg                 [10 : 0] row_index;
    reg                  [7 : 0] col_index;
    integer                      i;

    initial begin
        array_buffer  = 16'b0;
        row_index     = 11'b0;
        col_index     =  8'b0;
        // Initialize all memory content to ZEROs
       for (i = 0; i <= 524287; i = i + 1) begin
       	ram_0[i] = 0;
       	ram_1[i] = 0;
       end
    end

    always @ (rw_chk) begin
        if (Bank_select_1 == 1'b0) begin
            row_index = B0_row_add;
            col_index = B0_col_add;
            // Read data into array
            array_buffer = ram_0[{row_index, col_index}];               // read current location
            // Data read/write algorithm
            if (Write_1 == 1'b0) begin
                if (Dqm_1_int [0] == 1'b0) begin
                    array_buffer [((datapath / 2) - 1) : 0] = Write_data_array [((datapath / 2) - 1) : 0];
                end if (Dqm_1_int [1] == 1'b0) begin
                    array_buffer [(datapath - 1) : (datapath / 2)] = Write_data_array [(datapath - 1) : (datapath / 2)];
                end
                ram_0 [{row_index, col_index}] = array_buffer;          // write data
            end else if (Read_1 == 1'b0) begin
                Read_data_0 <= array_buffer;                                 
            end
        end else if (Bank_select_1 == 1'b1) begin
            row_index = B1_row_add;
            col_index = B1_col_add;
            // Read data into array
            array_buffer = ram_1[{row_index, col_index}];               // read current location
            // Data read/write algorithm
            if (Write_1 == 1'b0) begin
                if (Dqm_1_int [0] == 1'b0) begin
                    array_buffer [((datapath / 2) - 1) : 0] = Write_data_array [((datapath / 2) - 1) : 0];
                end if (Dqm_1_int [1] == 1'b0) begin
                    array_buffer [(datapath - 1) : (datapath / 2)] = Write_data_array [(datapath - 1) : (datapath / 2)];
                end
                ram_1 [{row_index, col_index}] = array_buffer;          // write data
            end else if (Read_1 == 1'b0) begin
                Read_data_0 <= array_buffer;                                 
            end
        end          
    end

    //================================================
    // output_drivers:PROCESS
    always @ (Data_out_enable_reg or Read_data or Dqm_1_int) begin
        // Data output registers             
        if (Data_out_enable_reg == 1'b0) begin   
            if (Dqm_1_int [0] == 1'b0) begin
                Dq_out [((datapath / 2) - 1) : 0] <= #(t_ac) Read_data [((datapath / 2) - 1) : 0];
            end else if (Dqm_1_int [0] == 1'b1) begin
                Dq_out [((datapath / 2) - 1) : 0] <= #(t_hz) 8'bz;            
            end
            if (Dqm_1_int [1] == 1'b0) begin
                Dq_out [(datapath - 1) : (datapath / 2)] <= #(t_ac) Read_data [(datapath - 1) : (datapath / 2)];
            end else if (Dqm_1_int [1] == 1'b1) begin
                Dq_out [(datapath - 1) : (datapath / 2)] <= #(t_hz) 8'bz;            
            end
        end
        
        if (Data_out_enable_reg == 1'b1) begin
            Dq_out <= #(t_hz) 16'bz;
        end
    end //ALWAYS
   

    //command_decode:PROCESS
    reg [2 : 0] brst_cmnd_vector;
    reg         auto_precharge;

    initial begin
        brst_cmnd_vector = 3'b111;
        auto_precharge = 1'b0;
    end
`ifdef	sdram_timing_check
    // command_timing:PROCESS
    integer         MRD_count, RC_count;
    time            RAS_chk0, RAS_chk1, RC_chk, RCD_chk0, RCD_chk1, RRD_chk;
    time            RP_ref_chk, RP_chk0, RP_chk1;
    reg     [1 : 0] prev_bank;
    reg     [3 : 0] Prev_command;
                                    // Note: The following timing parameters are for 200 MHz.
    parameter       t_mrd = 2;      // Load Mode Register command to command (2 t_CK)
    parameter       t_ras = 25;     // Active to Precharge command
    parameter       t_rc  = 30;     // Auto Refresh, Active command period
    parameter       t_rcd = 10;     // Active to Read or Write delay
    parameter       t_rrd = 10;     // Active Bank A to Active Bank B
    parameter       t_rp  = 10;     // Precharge command period
    parameter       t_wr  = 10;     // Write recovery time

    initial begin
        MRD_count    =   0;
        RC_count     =   0;
        RAS_chk0     =   0;
        RAS_chk1     =   0;
        RC_chk       =   0;
        RRD_chk      =   0;
        RP_ref_chk   =   0;         // PRECHARGE to AUTO REFRESH
        RP_chk0      =   0;         // PRECHARGE to ACTIVE
        RP_chk1      =   0;         // PRECHARGE to ACTIVE
        prev_bank    =   0;
        Prev_command = Nop;
    end

    always @ (posedge Sys_clk) begin
        brst_cmnd_vector[0] = Ras_in;                   // load command vector into variable
        brst_cmnd_vector[1] = Cas_in;
        brst_cmnd_vector[2] = We_in;
        auto_precharge = Add [10]; 
          
        if (brst_cmnd_vector == 3'b111 || Cs_n == 1'b1) begin
            Command = Nop;
        end else if (brst_cmnd_vector == 3'b110) begin
            Command = Active;
        end else if (brst_cmnd_vector == 3'b101) begin
            if (auto_precharge == 1'b1) begin
                Command = Read_A;
            end else begin
                Command = Read;
            end     
        end else if (brst_cmnd_vector == 3'b001) begin
            if (auto_precharge == 1'b1) begin
                Command = Write_A;
            end else begin
                Command = Write;
            end
        end else if (brst_cmnd_vector == 3'b010) begin
            Command = Precharge;
        end else if (brst_cmnd_vector == 3'b011) begin
            Command = Bst_Term;
        end else if (brst_cmnd_vector == 3'b100) begin
            if (Cke == 1'b0) begin
                Command = Self_Ref;
            end else begin
                Command = Auto_Ref;
            end
        end else if (brst_cmnd_vector == 3'b000) begin
            Command = Mode_Reg_Set;
        end
    
        if (Command == Auto_Ref) begin
            if (initialize == 1'b0) begin
                if (RC_count == 0) begin
                    if (($time - RP_ref_chk) < t_rp) begin
                        $display ("at time %t WARNING: t_RP violation during Refresh (initialize not complete).", $time);
                        $display ("at time %t SDRAM DIMM Error_T2_rsim is found!", $time);
                    end
                end if (RC_count == 1) begin
                    if (($time - RC_chk) < t_rc) begin
                        $display ("at time %t WARNING: t_RC violation during Refresh (initialize not complete).", $time);
                        $display ("at time %t SDRAM DIMM Error_T2_rsim is found!", $time);
                    end
                end
            end else if (($time - RC_chk) < t_rc) begin
                $display ("at time %t WARNING: t_RC violation during Refresh.", $time);
                $display ("at time %t SDRAM DIMM Error_T2_rsim is found!", $time);
            end
            RC_chk = $time;
            RC_count = RC_count + 1;
        end
    
        if (Command == Precharge) begin
            RP_ref_chk = $time;

            if (Add[10] == 1'b1) begin
                RP_chk0 = $time;
                RP_chk1 = $time;
            end else if (Add[10] == 1'b0) begin
                if (Ba == 1'b0) begin
                    RP_chk0 = $time;
                end else if (Ba == 1'b1) begin
                    RP_chk1 = $time;
                end
            end
            // Checking t_RAS violation - ACTIVE to PRECHARGE
            if (initialize == 1'b1) begin 
                if (Add[10] == 1'b1) begin
                    if (($time - RAS_chk0) < t_ras) begin
                        $display ("at time %t WARNING: t_RAS violation bank 0", $time);
                        $display ("at time %t SDRAM DIMM Error_T2_rsim is found!", $time);
                    end
                    if (($time - RAS_chk1) < t_ras) begin
                        $display ("at time %t WARNING: t_RAS violation bank 1", $time);
                        $display ("at time %t SDRAM DIMM Error_T2_rsim is found!", $time);
                    end
                end else if (Add[10] == 1'b0) begin
                    if (Ba == 1'b0) begin
                        if (($time - RAS_chk0) < t_ras) begin
                            $display ("at time %t WARNING: t_RAS violation bank 0", $time);
                        	$display ("at time %t SDRAM DIMM Error_T2_rsim is found!", $time);
                        end
                    end else if (Ba == 1'b1) begin
                        if (($time - RAS_chk1) < t_ras) begin
                            $display ("at time %t WARNING: t_RAS violation bank 1", $time);
                        	$display ("at time %t SDRAM DIMM Error_T2_rsim is found!", $time);
                        end
                    end
                end
            end
        end

        // Checking t_RCD violation - ACTIVE to READ/WRITE
        if (Command == Read || Command == Read_A || Command == Write || Command == Write_A) begin
            if (Ba == 1'b0) begin
                if ($time - RCD_chk0 < t_rcd) begin
                    $display ("at time %t WARNING: t_RCD violation", $time);
                    $display ("at time %t SDRAM DIMM Error_T2_rsim is found!", $time);
                end
            end else if (Ba == 1'b1) begin
                if ($time - RCD_chk1 < t_rcd) begin
                    $display ("at time %t WARNING: t_RCD violation", $time);
                    $display ("at time %t SDRAM DIMM Error_T2_rsim is found!", $time);
                end
            end
        end
        
        if (Command == Mode_Reg_Set) begin
            MRD_count = 0;
            if (~Pc_b0 | ~Pc_b1) begin
                $display ("at time %t WARNING: All banks must be idle to set Mode Register.", $time);
                $display ("at time %t SDRAM DIMM Error_T2_rsim is found!", $time);
            end
        end

        if (Command == Active) begin
            if (Ba == 1'b0) begin
                RAS_chk0 = $time;
                RCD_chk0 = $time;
            end else if (Ba == 1'b1) begin
                RAS_chk1 = $time;
                RCD_chk1 = $time;
            end

            if (initialize == 1'b1) begin
                // Checking t_RC violation  - ACTIVE to ACTIVE same bank
                if (Ba == prev_bank) begin
                    if (($time - RC_chk) < t_rc) begin
                        $display ("at time %t WARNING: t_RC violation.", $time);
                        $display ("at time %t SDRAM DIMM Error_T2_rsim is found!", $time);
                    end
                end
                // Checking t_RRD violation - ACTIVE to ACTIVE different bank
                if (Ba != prev_bank) begin
                    if (($time - RRD_chk) < t_rrd) begin
                        $display ("at time %t WARNING: t_RRD violation.", $time);
                        $display ("at time %t SDRAM DIMM Error_T2_rsim is found!", $time);
                    end
                end
                // Checking t_RP violation  - PRECHARGE to ACTIVE
                if (Ba == 1'b0) begin
                    if (($time - RP_chk0) < t_rp) begin
                        $display ("at time %t WARNING: t_RP violation.", $time);
                        $display ("at time %t SDRAM DIMM Error_T2_rsim is found!", $time);
                    end
                end else if (Ba == 1'b1) begin
                    if (($time - RP_chk1) < t_rp) begin
                        $display ("at time %t WARNING: t_RP violation.", $time);
                        $display ("at time %t SDRAM DIMM Error_T2_rsim is found!", $time);
                    end
                end
                // Check to see if bank precharged
                if (Ba == 1'b0 && Pc_b0 == 1'b0) begin
                    $display("at time %t ERROR: Bank 0 is not precharged, data corruption possible", $time);
                    $display ("at time %t SDRAM DIMM Error_T2_rsim is found!", $time);
                end else if (Ba == 1'b1 && Pc_b1 == 1'b0) begin
                    $display("at time %t ERROR: Bank 1 is not precharged, data corruption possible", $time);
                    $display ("at time %t SDRAM DIMM Error_T2_rsim is found!", $time);
                end
            end
            RRD_chk    = $time;
            RC_chk     = $time;
            prev_bank  = Ba;
            initialize = 1'b1;
        end

        // Checking t_MRD violation - LOAD MODE REGISTER to COMMAND
        if (initialize == 1'b1 && ((Ras_in == 1'b0 || Cas_in == 1'b0) && (Command != Mode_Reg_Set))) begin
            if (MRD_count < t_mrd) begin
                $display ("at time %t WARNING: t_MRD violation.", $time);
                $display ("at time %t SDRAM DIMM Error_T2_rsim is found!", $time);
            end
        end

        MRD_count    = MRD_count + 1;
        Prev_command = Command;

    end

    /**************************************************************************************************
    *
    *   Check For Timing Violation
    *
    *   Note: The following timing parameters are for 100 MHz device
    *
    **************************************************************************************************/
/*  
    specify
        specparam   t_as        =  1.5,                 // Address setup time
                    t_ah        =  0.5,                 // Address hold time
                    t_cl        =  1.8,                 // CLK low level width
                    t_ch        =  1.8,                 // CLK high level width
                    t_ck        =  5.0,                 // System clock cycle time
                    t_ckh       =  0.5,                 // CKE hold time
                    t_cks       =  1.5,                 // CKE setup time
                    t_cmh       =  0.5,                 // CS#, RAS#, CAS#, WE#, DSF, DQM hold time
                    t_cms       =  1.5,                 // CS#, RAS#, CAS#, WE#, DSF, DQM setup time
                    t_dh        =  0.5,                 // Data in hold time
                    t_ds        =  1.5;                 // Data in setup time
        $width     (negedge Clk,        t_cl        );  // Checking Clk Width LOW
        $width     (posedge Clk,        t_ch        );  // Checking Clk Width HIGH
        $period    (negedge Clk,        t_ck        );  // Checking Clk Period LOW
        $period    (posedge Clk,        t_ck        );  // Checking Clk Period HIGH
        $setuphold (posedge Clk, Add,   t_as,  t_ah );  // Checking Add   SETUP/HOLD Time
        $setuphold (posedge Clk, Ba,    t_as,  t_ah );  // Checking Ba    SETUP/HOLD Time
        $setuphold (posedge Clk, Cke,   t_cks, t_ckh);  // Checking Cke   SETUP/HOLD Time
        $setuphold (posedge Clk, Cs_n,  t_cms, t_cmh);  // Checking Cs_n  SETUP/HOLD Time
        $setuphold (posedge Clk, Ras_n, t_cms, t_cmh);  // Checking Ras_n SETUP/HOLD Time
        $setuphold (posedge Clk, Cas_n, t_cms, t_cmh);  // Checking Cas_n SETUP/HOLD Time
        $setuphold (posedge Clk, We_n,  t_cms, t_cmh);  // Checking We_n  SETUP/HOLD Time
        $setuphold (posedge Clk, Dqm,   t_cms, t_cmh);  // Checking Dqm   SETUP/HOLD Time
        $setup     (Dq, posedge Clk &&& Enable_data_in, t_ds);
        $hold      (posedge Clk, Dq &&& Enable_data_in, t_dh);
    endspecify
*/
`endif
endmodule
