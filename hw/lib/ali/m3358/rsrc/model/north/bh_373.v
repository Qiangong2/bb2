/******************************************************************************
	  (c) copyrights 2000-. All rights reserved

	   T-Square Design, Inc.

	  Company confidential and Properietary	information.
	  This information may not be disclosed	to unauthorized individual.
*******************************************************************************/

/******************************************************************************
 *
 *    DESCRIPTION:	external 74373 behavial module. Latch eeprom address[7:0] by ale
 *
 *    AUTHOR:		Jeffrey
 *
 *    HISTORY: 		2004-1-8 initial version


 *****************************************************************************/

module bh_373(
			rom_ale,		// 1	Flash rom external 373 latch enable
        	rom_addr,		// 21	Flash rom address

			eeprom_addr_ale,
//			flash_mode_sel
);

input			rom_ale;		// 1	Flash rom external 373 latch enable
input   [7:0]	rom_addr;		// 8	Flash rom address
//input	[1:0]	flash_mode_sel;	// 00	8bit mode
output	[7:0]	eeprom_addr_ale	;

parameter udly = 1;

reg		[7:0]	eeprom_addr_ale;
//373
always @(rom_ale)
  if (rom_ale)
	eeprom_addr_ale	<= #udly rom_addr[7:0];

endmodule
