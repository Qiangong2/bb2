/***********************************************************
*	Description: 	used to control the strap pin
*			status during system initialization
*	Author:		Yuky
*	Date:		2001-04-05
***********************************************************/
module strap_pin_bh (	strap_pin_out	);

parameter udly = 1;
parameter set_one = 1;

output 	strap_pin_out;

tri1	highlevel;
tri0	lowlevel;
wire	strap_pin_out;
reg		strap_mask;
reg		set_one_reg;
initial strap_mask = 1;
initial	set_one_reg	= 0;

wire	pmos_ctrl = set_one & strap_mask | set_one_reg & ~strap_mask;
wire	nmos_ctrl = set_one & strap_mask | set_one_reg & ~strap_mask;

pmos	(strap_pin_out, lowlevel ,  pmos_ctrl);//zero(0) active
nmos	(strap_pin_out, highlevel , nmos_ctrl);//one(1) active
endmodule