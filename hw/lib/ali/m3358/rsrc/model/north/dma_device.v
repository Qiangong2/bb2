/*****************************************************************************
          (c) copyrights 2000-. All rights reserved

           T-Square Design, Inc.

          Company confidential and Properietary information.
          This information may not be disclosed to unauthorized
          individual.
 *****************************************************************************/

/*****************************************************************************
 *
 *    FILE NAME:  dma_device.v
 *
 *    DESCRIPTION: dma engine for idma interface
 *
 *    AUTHOR:  Joyous Wang
 *
 *****************************************************************************/
module	dma_device(		
	//To
		dma_ram_req		,      
		dma_ram_rw		,   
		dma_ram_addr	, 
		dma_ram_rbl		,  
		dma_ram_wlast	,
        dma_ram_wbe		,  
        dma_ram_wdata	,
    //From         
        dma_ram_ack		,  
        dma_ram_err		,  
        dma_ram_rrdy	, 
        dma_ram_rlast	,
        dma_ram_rdata	,
        dma_ram_wrdy	, 
        
        mem_clk			,
        rst_	
        ); 
        
input			dma_ram_ack		;
input			dma_ram_err		;
input			dma_ram_rrdy	;
input			dma_ram_rlast	;
input[31:0]		dma_ram_rdata	;
input			dma_ram_wrdy    ;

output			dma_ram_req		;	
output			dma_ram_rw		;
output[29:2]	dma_ram_addr	;
output[1:0]		dma_ram_rbl		;
output			dma_ram_wlast	;
output[3:0]		dma_ram_wbe		;
output[31:0]	dma_ram_wdata	;
	            	                
input			mem_clk		    ;
input			rst_			;
	        	
parameter		udly	=	1	;
parameter		fifo_lg	=	512	;

wire			time_en0,time_en1,time_en			;
wire[31:0]		state_bits		;
/*
reg		pci_dma_wrdy_io		;
reg		pci_dma_wrdy_buf	;
always	@(posedge mem_clk or negedge rst_)
	if	(!rst_)begin
		pci_dma_wrdy_io	<=	#udly	1'b0	;
		pci_dma_wrdy_io	<=	#udly	1'b0	;
		end
	else if	(pci_dma_wlast)begin
		pci_dma_wrdy_io	<=	#udly	1'b0	;
		pci_dma_wrdy_io	<=	#udly	1'b0	;
		end	  	
	else if	(pci_dma_req&pci_dma_rw&fifo_en)
		pci_dma_wrdy	<=	#udly	1'b	;
		
reg		pci_dma_ack		;
always	@(posedge mem_clk or negedge rst_)
	if	(!rst_)
		pci_dma_ack		<=	#udly	1'b0	;
	else if	(pci_dma_ack)	  
		pci_dma_ack		<=	#udly	1'b0	;	  	
	else if	(pci_dma_req&(fifo_en|reg_en))
		pci_dma_ack		<=	#udly	1'b1	;
		
wire	fifo_sel	=	pci_wbuf_addr[0]	;
		
reg[fifo_lg-1:0]	pci_wbuf_addr	;	
always	@(posedge mem_clk or negedge rst_)
	if	(!rst_)
		pci_wbuf_addr	<=	#udly	1'b0					;
	else if	(pci_dma_wrdy)
		pci_wbuf_addr	<=	#udly	pci_wbuf_addr + 1'b1	;
		
wire	fifo_en		=	(fifo_lg-pci_wbuf_addr>4)&!pci_dma_wrdy	&!reg_en;
wire	pci_dma_err	=	(fifo_lg-1)==pci_wbuf_addr				;

reg		reg_en		;
always	@(posedge mem_clk or negedge rst_)
	if	(!rst_)
		reg_en		<=	#udly	1'b0	;
	else if	(pci_dma_req&(pci_dma_addr==24'hff_ffff))
		reg_en		<=	#udly	1'b1	;
*/
reg			begin_flag			;
reg	[31:0]	finish_reg			;
reg			finish_flag			;
reg	[31:0]	clk_reg				;
reg			dma_ram_req0		;
reg			dma_ram_req1		;
wire		begin_pulse			;

reg			begin_flag_reg		;
always	@(posedge mem_clk or negedge rst_)
	if	(!rst_)begin
		begin_flag		<=	#udly	1'b0		;		
		begin_flag_reg	<=	#udly	1'b0		; 
		end
	else
		begin_flag_reg	<=	#udly	begin_flag	;		
assign	begin_pulse	=	begin_flag & !begin_flag_reg	;

reg		begin_pulse_dly1	;
reg		begin_pulse_dly2	;
always	@(posedge mem_clk or negedge rst_)
	if	(!rst_)begin
		begin_pulse_dly1	<=	#udly	1'b0	;
		begin_pulse_dly2	<=	#udly	1'b0	;
		end
	else	begin
		begin_pulse_dly1	<=	#udly	begin_pulse			;
		begin_pulse_dly2	<=	#udly	begin_pulse_dly1	;
		end


reg		dma_ram_req0_reg		;
wire	dma_ram_req0_pulse		;
always	@(posedge mem_clk or negedge rst_)
	if	(!rst_)
		dma_ram_req0_reg	<=	#udly	1'b0		;
	else
		dma_ram_req0_reg	<=	#udly	dma_ram_req0				;		
assign	dma_ram_req0_pulse	=	dma_ram_req0 & !dma_ram_req0_reg	;

reg		dma_ram_req0_dly1	;
reg		dma_ram_req0_dly2	;
always	@(posedge mem_clk or negedge rst_)
	if	(!rst_)begin
		dma_ram_req0_dly1	<=	#udly	1'b0	;
		dma_ram_req0_dly2	<=	#udly	1'b0	;
		end
	else	begin
		dma_ram_req0_dly1	<=	#udly	dma_ram_req0_pulse	;
		dma_ram_req0_dly2	<=	#udly	dma_ram_req0_dly1	;
		end


reg		dma_ram_req1_reg		;
wire	dma_ram_req1_pulse		;
always	@(posedge mem_clk or negedge rst_)
	if	(!rst_)
		dma_ram_req1_reg	<=	#udly	1'b0		;
	else
		dma_ram_req1_reg	<=	#udly	dma_ram_req1				;		
assign	dma_ram_req1_pulse	=	dma_ram_req1 & !dma_ram_req1_reg	;

reg		dma_ram_req1_dly1	;
reg		dma_ram_req1_dly2	;
always	@(posedge mem_clk or negedge rst_)
	if	(!rst_)begin
		dma_ram_req1_dly1	<=	#udly	1'b0	;
		dma_ram_req1_dly2	<=	#udly	1'b0	;
		end
	else	begin
		dma_ram_req1_dly1	<=	#udly	dma_ram_req1_pulse	;
		dma_ram_req1_dly2	<=	#udly	dma_ram_req1_dly1	;
		end		
		
always	@(posedge mem_clk or negedge rst_)
	if	(!rst_)
		dma_ram_req0		<=	#udly	1'b0	;
	else if	(dma_ram_ack&dma_ram_req0)
		dma_ram_req0		<=	#udly	1'b0	;
	else if	(time_en0&!dma_ram_req1)
		dma_ram_req0		<=	#udly	1'b1	;
	else if	(time_en0&dma_ram_req1&dma_ram_ack)
		dma_ram_req0		<=	#udly	1'b1	;

always	@(posedge mem_clk or negedge rst_)
	if	(!rst_)
		dma_ram_req1		<=	#udly	1'b0	;
	else if	(dma_ram_ack&dma_ram_req1)
		dma_ram_req1		<=	#udly	1'b0	;
	else if	(time_en1&!dma_ram_req0)
		dma_ram_req1		<=	#udly	1'b1	;
	else if	(time_en1&dma_ram_req0&dma_ram_ack)
		dma_ram_req1		<=	#udly	1'b1	;	

wire	req_ack_pulse	=	dma_ram_ack&dma_ram_req	;
reg		req_ack_dly1		;		
reg     req_ack_dly2		;
always	@(posedge mem_clk or negedge rst_)
	if	(!rst_)	begin
		req_ack_dly1	<=	#udly	1'b0	;
		req_ack_dly2	<=	#udly	1'b0	;
		end
	else begin
		req_ack_dly1	<=	#udly	req_ack_pulse	;
		req_ack_dly2	<=	#udly	req_ack_dly1	;
		end


wire	raddr_add	=	dma_ram_req0_pulse	|	dma_ram_req0_dly1	|
						dma_ram_req1_pulse	|	dma_ram_req1_dly1	;
		
reg	[20:0]		fifo_raddr		;
reg	[31:0]		time_reg0		;
reg	[31:0]		state_reg0		;
reg	[31:0]		time_reg1		;
reg	[31:0]		state_reg1		;
wire[31:0]		fifo_out		;

always	@(posedge mem_clk or negedge rst_)
	if	(!rst_)
		fifo_raddr		<=	#udly	21'b0				;
	else if	(begin_pulse|begin_pulse_dly1|raddr_add)
		fifo_raddr		<=	#udly	fifo_raddr + 1'b1	;
		
always	@(posedge mem_clk or negedge rst_)
	if	(!rst_)	begin
		time_reg0	<=	#udly	1'b0	;
		state_reg0	<=	#udly	1'b0	;
		time_reg1	<=	#udly	1'b0	;
		state_reg1	<=	#udly	1'b0	;
		end
	else if	(begin_pulse)begin
		time_reg0	<=	#udly	fifo_out	;
		end
	else if	(begin_pulse_dly1)begin
		state_reg0	<=	#udly	fifo_out	;
		end
	else if	(dma_ram_req0_pulse)begin
		time_reg1	<=	#udly	fifo_out	;
		end
	else if	(dma_ram_req0_dly1)begin
		state_reg1	<=	#udly	fifo_out	;
		end
	else if	(dma_ram_req1_pulse)begin
		time_reg0	<=	#udly	fifo_out	;
		end
	else if	(dma_ram_req1_dly1)begin
		state_reg0	<=	#udly	fifo_out	;
		end

assign	time_en0	=	(clk_reg==time_reg0)&(clk_reg!=1'b0)	;
assign	time_en1	=	(clk_reg==time_reg1)&(clk_reg!=1'b0)	;
assign	time_en		=	time_en0	|	time_en1				;	

always	@(posedge mem_clk or negedge rst_)
	if	(!rst_)
		clk_reg	<=	#udly	1'b0			;
	else if	(time_en&dma_ram_req&!dma_ram_ack)
		clk_reg	<=	#udly	clk_reg			;
	else
		clk_reg	<=	#udly	clk_reg + 1'b1	;	



reg	[31:0]	dma_wdata_reg	;
always	@(posedge mem_clk or negedge rst_)
	if	(!rst_)begin
		dma_wdata_reg	<=	#udly	32'h1234567	;
		end
	else if	(dma_ram_wlast)begin
		dma_wdata_reg	<=	#udly	32'h1234567	;
		end		
	else if	(dma_ram_wrdy)	begin
		dma_wdata_reg	<=	#udly	{dma_wdata_reg[23:0],dma_wdata_reg[31:24]};
		end
				
reg	[2:0]	bl_cnt		;
always	@(posedge mem_clk or negedge rst_)
	if	(!rst_)
		bl_cnt		<=	#udly	3'h7			;
	else if	(dma_ram_wlast)//(dma_ram_req0_pulse|dma_ram_req1_pulse)	
		bl_cnt		<=	#udly	dma_ram_rbl		;	
//	else if	(dma_ram_ack)//(dma_ram_req0_pulse|dma_ram_req1_pulse)	
//		bl_cnt		<=	#udly	dma_ram_rbl-1'b1;
	else if	(dma_ram_wrdy&!dma_ram_wlast)	
		bl_cnt		<=	#udly	bl_cnt - 1'b1	;

reg	[31:0]	read_data	;
always	@(posedge mem_clk or negedge rst_)
	if	(!rst_)
		read_data		<=	#udly	32'h0			;	
	else if	(dma_ram_rrdy)	
		read_data		<=	#udly	dma_ram_rdata	;

		
always	@(posedge mem_clk or negedge rst_)
	if	(!rst_)
		finish_flag		<=	#udly	1'h0			;	
	//else if	(fifo_raddr==32'h0000_ffff)	
	else if	(fifo_raddr==finish_reg )
		finish_flag		<=	#udly	1'h1			;	
		
assign	dma_ram_req	=	(dma_ram_req0	|	dma_ram_req1)&!finish_flag	;

assign	state_bits	=	{32{dma_ram_req0}}&state_reg0	|
						{32{dma_ram_req1}}&state_reg1	;

assign	dma_ram_rw		=	state_bits[31]				;	
assign	dma_ram_addr	=   {6'h0,state_bits[21:0]}    	;
assign	dma_ram_rbl		=	{2{(state_bits[30:29]==2'b00)}}&2'h0	|   
							{2{(state_bits[30:29]==2'b01)}}&2'h1	|
							{2{(state_bits[30:29]==2'b10)}}&2'h3	;

assign	dma_ram_wbe		=	state_bits[28:25]	;
assign	dma_ram_wdata	=	dma_wdata_reg		;
assign	dma_ram_wlast	=	(bl_cnt==3'h0)&dma_ram_wrdy	;	

parameter       length = 32'h0080_0000;
reg     [31:0]  datapipeline [0:(length>>2) - 1];// : 0];
		
assign	fifo_out	=	datapipeline[fifo_raddr]	;

reg     [2:0]   sdram_row0_map,
                sdram_row1_map;
reg     [31:0]  temp_xxx;
initial begin
        sdram_row0_map = 0;
        sdram_row1_map = 0;
        for(temp_xxx = 32'h0; temp_xxx < length ; temp_xxx = temp_xxx + 1)
                datapipeline[temp_xxx] = 0;
        for(temp_xxx = 32'h0; temp_xxx < (length>>2) ; temp_xxx = temp_xxx + 1)
                datapipeline[temp_xxx] = 0;
end

always	@(posedge mem_clk or negedge rst_)
	if(dma_ram_wrdy)begin
				$display("\n%m\tAt Time: %t\nDMA write MEM ADDRESS:%h DATA:%h  \n",$realtime,dma_ram_addr,dma_wdata_reg);
			end

always	@(posedge mem_clk or negedge rst_)
	if(dma_ram_rrdy)begin
				$display("\n%m\tAt Time: %t\nDMA read MEM ADDRESS:%h DATA:%h \n",$realtime,dma_ram_addr,read_data);
			end
//ram 512x32d1	mbus_wbuf0	(
//				.DIN	(32'h0					),
//				.DOUT	(fifo_out				),
//				.RADDR	(fifo_raddr				),
//				.WADDR	(20'h0					),
//				.WE		(1'h0					),
//				.RST_	(1'b1					),
//				.CLK	(mem_clk				)
//				);
									
endmodule	


	  
	  
	  
	  
	  
	  