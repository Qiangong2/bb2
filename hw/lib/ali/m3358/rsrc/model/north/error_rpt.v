/*************************************************************
file name:		error_rpt.v
description:	use to report the error message of MIPS pattern
				rpt_data_31t0 map to the scratch register of
				host bridge. it only support the error info.
				gpio_3t0 map to the gpio pins of m6304. it only
				support the trigger command

author:			yuky
history:		2002-08-07	initial version
***************************************************************/
module error_rpt(
	gpio_3t0,
	rpt_data_31t0,

	clk,
	rst_
	);
parameter udly = 1;

input	[3:0]	gpio_3t0;
input	[31:0]	rpt_data_31t0;
input	clk;
input	rst_;

reg		[3:0]	step_flag;
reg		[31:0]	exp_data_info;
reg		[31:0]	err_data_info;
reg		[31:0]	err_addr_info;
reg		[31:0]	err_type_info;

reg		step1_hit_latch;
reg		step2_hit_latch;
reg		step3_hit_latch;
reg		step4_hit_latch;

reg		cpu_set_cold_rst_flag;	//use to reset the m6304 if cpu set this bit
//command key word is 6304
wire	step1_hit	= gpio_3t0 == 4'h6;
wire	step2_hit	= gpio_3t0 == 4'h3;
wire	step3_hit	= gpio_3t0 == 4'h0;
wire	step4_hit	= gpio_3t0 == 4'h4;

wire	step1_hit_pulse = step1_hit & ~step1_hit_latch;
wire	step2_hit_pulse = step2_hit & ~step2_hit_latch;
wire	step3_hit_pulse = step3_hit & ~step3_hit_latch;
wire	step4_hit_pulse = step4_hit & ~step4_hit_latch;

initial	begin
	step_flag		= 0;
	exp_data_info	= 0;
	err_data_info	= 0;
	err_addr_info	= 0;
	err_type_info	= 0;
	step1_hit_latch = 1;
	step2_hit_latch = 1;
	step3_hit_latch = 1;
	step4_hit_latch = 1;
end

always @(posedge clk)
	if (rst_) begin
		step1_hit_latch <= #udly step1_hit;
		step2_hit_latch <= #udly step2_hit;
		step3_hit_latch <= #udly step3_hit;
		step4_hit_latch <= #udly step4_hit;
	end

always @(posedge clk)
	if (rst_) begin
		if (step1_hit_pulse & ~step_flag[0]) begin
			exp_data_info 	<= #udly rpt_data_31t0;
			step_flag[0]		<= #udly 1;
		end

		if (step2_hit_pulse & step_flag[0]) begin
			err_data_info 	<= #udly rpt_data_31t0;
			step_flag[1]		<= #udly 1;
		end

		if (step3_hit_pulse & step_flag[1]) begin
			err_addr_info 	<= #udly rpt_data_31t0;
			step_flag[2]		<= #udly 1;
		end

		if (step4_hit_pulse & step_flag[2]) begin
			err_type_info 	<= #udly rpt_data_31t0;
			step_flag[3]		<= #udly 1;
		end

		if (step_flag[3])
			step_flag			<= #udly 0;
	end

wire	step_flag_active = &step_flag;
always @(posedge clk)
	if (~rst_)
		cpu_set_cold_rst_flag	<= #udly 0;
	else if (step_flag_active) begin
		case (err_type_info)
		//New error type judgement for m6304
		32'h0000_0001:	$display("Error_T2_rsim: SYS IO ERROR\tErr_Addr=%h,Err_Data=%h,Exp_Data=%h",
						err_addr_info,err_data_info,exp_data_info);
		32'h0000_0002:	$display("Error_T2_rsim: SDRAM  ERROR\tErr_Addr=%h,Err_Data=%h,Exp_Data=%h",
						err_addr_info,err_data_info,exp_data_info);
		32'h0000_0003:	$display("Error_T2_rsim: FLASH  ERROR\tErr_Addr=%h,Err_Data=%h,Exp_Data=%h",
						err_addr_info,err_data_info,exp_data_info);
		32'h0000_0004:	$display("Error_T2_rsim: PCI33  ERROR\tErr_Addr=%h,Err_Data=%h,Exp_Data=%h",
						err_addr_info,err_data_info,exp_data_info);
		32'h0000_0005:	$display("Error_T2_rsim: USB    ERROR\tErr_Addr=%h,Err_Data=%h,Exp_Data=%h",
						err_addr_info,err_data_info,exp_data_info);
		32'h0000_0006:	$display("Error_T2_rsim: IDE    ERROR\tErr_Addr=%h,Err_Data=%h,Exp_Data=%h",
						err_addr_info,err_data_info,exp_data_info);
		32'h0000_0007:	$display("Error_T2_rsim: GPU    ERROR\tErr_Addr=%h,Err_Data=%h,Exp_Data=%h",
						err_addr_info,err_data_info,exp_data_info);
		32'h0000_0008:	$display("Error_T2_rsim: VIDEO  ERROR\tErr_Addr=%h,Err_Data=%h,Exp_Data=%h",
						err_addr_info,err_data_info,exp_data_info);
		32'h0000_0009:	$display("Error_T2_rsim: MS_SD	ERROR\tErr_Addr=%h,Err_Data=%h,Exp_Data=%h",
						err_addr_info,err_data_info,exp_data_info);
		32'h0000_0010:	$display("Error_T2_rsim: DSP	ERROR\tErr_Addr=%h,Err_Data=%h,Exp_Data=%h",
						err_addr_info,err_data_info,exp_data_info);
		32'h0000_0011:	$display("Error_T2_rsim: SB CIR ERROR\tErr_Addr=%h,Err_Data=%h,Exp_Data=%h",
						err_addr_info,err_data_info,exp_data_info);
		32'h0000_0012:	$display("Error_T2_rsim: SB I2C ERROR\tErr_Addr=%h,Err_Data=%h,Exp_Data=%h",
						err_addr_info,err_data_info,exp_data_info);
		32'h0000_0013:	$display("Error_T2_rsim: SB GSI ERROR\tErr_Addr=%h,Err_Data=%h,Exp_Data=%h",
						err_addr_info,err_data_info,exp_data_info);
		32'h0000_0014:	$display("Error_T2_rsim: SB CDI ERROR\tErr_Addr=%h,Err_Data=%h,Exp_Data=%h",
						err_addr_info,err_data_info,exp_data_info);
		32'h0000_0015:	$display("Error_T2_rsim: SB uP  ERROR\tErr_Addr=%h,Err_Data=%h,Exp_Data=%h",
						err_addr_info,err_data_info,exp_data_info);
		32'h0000_0016:	$display("Error_T2_rsim: VSB	ERROR\tErr_Addr=%h,Err_Data=%h,Exp_Data=%h",
						err_addr_info,err_data_info,exp_data_info);
		32'h0000_0017:	$display("Error_T2_rsim: SubPIC ERROR\tErr_Addr=%h,Err_Data=%h,Exp_Data=%h",
						err_addr_info,err_data_info,exp_data_info);
		//Old error type compatible with m6303
		32'h0000_0100:	$display("Error_T2_rsim: SDRAM Read ERROR  \tErr_Addr=%h,Err_Data=%h,Exp_Data=%h",
						err_addr_info,err_data_info,exp_data_info);
		32'h0000_0101:	$display("Error_T2_rsim: PCI33IO Read ERROR\tErr_Addr=%h,Err_Data=%h,Exp_Data=%h",
						err_addr_info,err_data_info,exp_data_info);
		32'h0000_0102:	$display("Error_T2_rsim: PCI33MEM Read ERROR\tErr_Addr=%h,Err_Data=%h,Exp_Data=%h",
						err_addr_info,err_data_info,exp_data_info);
		32'h0000_0105:	$display("Error_T2_rsim: IO Read ERROR      \tErr_Addr=%h,Err_Data=%h,Exp_Data=%h",
						err_addr_info,err_data_info,exp_data_info);
		32'h0000_0108:	$display("Error_T2_rsim: Local IO Read Error\tErr_Addr=%h,Err_Data=%h,Exp_Data=%h",
						err_addr_info,err_data_info,exp_data_info);
		32'h0000_0109:	$display("Error_T2_rsim: Config Read ERROR  \tErr_Addr=%h,Err_Data=%h,Exp_Data=%h",
						err_addr_info,err_data_info,exp_data_info);
		32'h0000_0110:	$display("Error_T2_rsim: EEPROM Read ERROR  \tErr_Addr=%h,Err_Data=%h,Exp_Data=%h",
						err_addr_info,err_data_info,exp_data_info);
		32'h0000_0111:	$display("Error_T2_rsim: GPU IO ERROR  \tErr_Addr=%h, Err_Data=%h,Exp_Data=%h",
						err_addr_info,err_data_info,exp_data_info);
		32'h0000_0112:	$display("Error_T2_rsim: IDCT IO ERROR  \tErr_Addr=%h, Err_Data=%h,Exp_Data=%h",
						err_addr_info,err_data_info,exp_data_info);
		32'h0000_0113:	$display("Error_T2_rsim: SB IO ERROR  \tErr_Addr=%h, Err_Data=%h,Exp_Data=%h",
						err_addr_info,err_data_info,exp_data_info);
		32'h8000_0001:	$display("External Control trigger 1");
		32'h8000_0002:	$display("External Control trigger 2");
		32'h8000_0003:	$display("External Control trigger 3");
		32'h8000_0004:	$display("External Control trigger 4");
		32'h8000_0005:	$display("External Control trigger 5");
		32'h8000_0006:	$display("External Control trigger 6");
		32'h8000_0007:	$display("External Control trigger 7");
		32'h8000_0008:	$display("External Control trigger 8");
		32'h8000_0009:	$display("External Control trigger 9");
		32'h8000_000a:	$display("External Control trigger 10");
		32'h8000_000b:	$display("External Control trigger 11");
		32'h8000_000c:	$display("External Control trigger 12");
		32'h8000_000d:	$display("External Control trigger 13");
		32'h8000_000e:	$display("External Control trigger 14");
		32'h8000_000f:	$display("External Control trigger 15");
		32'h8000_0011:	$display("External Control trigger 16");
		32'h8000_0012:	$display("External Control trigger 17");
		32'h8000_0013:	$display("External Control trigger 18");
		32'h8000_0014:	$display("External Control trigger 19");
		32'h8000_0015:	$display("External Control trigger 20");
		32'h8000_0016:	$display("External Control trigger 21");
		32'h8000_0017:	$display("External Control trigger 22");
		32'h8000_0018:	$display("External Control trigger 23");
		32'h8000_0019:	$display("External Control trigger 24");
		32'h8000_001a:	$display("External Control trigger 25");
		32'h8000_001b:	$display("External Control trigger 26");
		32'h8000_001c:	$display("External Control trigger 27");
		32'h8000_001d:	$display("External Control trigger 28");
		32'h8000_001e:	$display("External Control trigger 29");
		32'h8000_001f:	$display("External Control trigger 30");
		32'h8888_8888:	$display("MIPS Pattern Display Info: info1 =%h, info2 =%h",
						err_data_info, exp_data_info);
		32'h4352_5354:	begin
						$display("External Cold Reset Trigger!!!"); //CRST--ascii:0x43525354
						cpu_set_cold_rst_flag <= #udly 1'b1;
						end
		32'hffff_ffff:	$display("Trigger the rsim pattern");
		default:	begin
						$display("Error_T2_rsim: an unsupported error type %h issued!Err_Addr=%h,Err_Data=%h,Exp_Data=%h",
						err_type_info,err_addr_info,err_data_info,exp_data_info);
					end
		endcase
	end

endmodule