/*******************************************************************
          (c) copyrights 1997-1998. All rights reserved

           T-Square Design, Inc.

          Company confidential and Properietary information.
          This information may not be disclosed to unauthorized
          individual.
*******************************************************************/
// Summary :monitor.v is a PCI bus moniitor, it monitors possible  error
// happening on PCI bus

//`timescale      1ns/1ps
module monitor (
//       req_,    gnt_,
//       idsel,
       ad,      cbe_,
       frame_,  irdy_,
       devsel_,      trdy_,   stop_,
       lock_,
       par,     perr_,         serr_,
       clk,   rst_ );


input  [31:0]  ad;
input  [ 3:0]  cbe_;
//input  [ 15:0]  idsel;
//input  [ 3:0]  req_,    gnt_;
input  frame_,
       irdy_,
       devsel_,
       trdy_,
       stop_,
       lock_,
       par,
       perr_,
       serr_,
       clk,   rst_;

parameter bus_sel=0;  //default


integer  f_name;
/*
initial begin

    case (bus_sel)
    0:   f_name = $fopen("p_monitor.rpt");
    1:   f_name = $fopen("a_monitor.rpt");
    2:   f_name = $fopen("b_monitor.rpt");
    default: begin
               $display("Error_T2_biu: Wrong bus number for master model");
               $stop;
             end
    endcase

    $fdisplay(f_name, "START HERE");
end
*/


/////////////////////////////////////////////////////////////////
always @(posedge clk ) begin
    if (!perr_) begin
	$fdisplay(f_name, "Error_T2_biu: MONITOR: Parity Error!");
	$display("Error_T2_biu: MONITOR: Parity Error!");
    end
end

/////////////////////////////////////////////////////////////////
always @(posedge clk ) begin
    if (!serr_) begin
	$fdisplay(f_name, "Error_T2_biu: MONITOR: System Error!");
	$display("Error_T2_biu: MONITOR: System Error!");
    end
end


/////////////////////////////////////////////////////////////////
//
// To detect error that FRAME_ is not low one clock after GNT_ is low");
//
/////////////////////////////////////////////////////////////////
/***
reg [3:0] rg_gnt;
reg [3:0] rg_gnt1;
wire [3:0] gnt_hlos;

always @(posedge clk or negedge rst_) begin
  if (!rst_) rg_gnt[0]=0;
  else rg_gnt[0]= #1 gnt_;
end
assign gnt_hlos[0]= rg_gnt[0] & !gnt_[0];
always @(posedge clk or negedge rst_) begin
  if (!rst_) rg_gnt1[0]=0;
  else  rg_gnt1[0]= #1 (gnt_hlos[0] & frame_ & irdy_);
end

//if neither frame_ nor irdy_ is low when gnt_ is low during first clock
//frame_ must be low during second clock
always @(posedge clk) begin
    if (rg_gnt1[0] & frame_) begin
	$display("Error_T2_biu: MONITOR: ERROR, FRAME_ should be low one clock after GNT_ is first low");
	$fdisplay(f_name, "Error_T2_biu: MONITOR: ERROR, FRAME_ should be low one clock after GNT_ is first low");
    end
end


always @(posedge clk or negedge rst_) begin
  if (!rst_) rg_gnt[1]=0;
  else rg_gnt[1]= #1 gnt_;
end
assign gnt_hlos[1]= rg_gnt[1] & !gnt_;
always @(posedge clk or negedge rst_) begin
  if (!rst_) rg_gnt1[1]=0;
  else  rg_gnt1[1]= #1 (gnt_hlos[1] & frame_ & irdy_);
end
//if neither frame_ nor irdy_ is low when gnt_ is low during first clock
//frame_ must be low during second clock
always @(posedge clk) begin
    if (rg_gnt1[1] & frame_) begin
	$display("Error_T2_biu: MONITOR: ERROR, FRAME_ should be low one clock after GNT_ is first low");
	$fdisplay(f_name, "Error_T2_biu: MONITOR: ERROR, FRAME_ should be low one clock after GNT_ is first low");
    end
end



always @(posedge clk or negedge rst_) begin
  if (!rst_) rg_gnt[2]=0;
  else rg_gnt[2]= #1 gnt_;
end
assign gnt_hlos[2]= rg_gnt[2] & !gnt_;
always @(posedge clk or negedge rst_) begin
  if (!rst_) rg_gnt1[2]=0;
  else  rg_gnt1[2]= #1 (gnt_hlos[2] & frame_ & irdy_);
end
//if neither frame_ nor irdy_ is low when gnt_ is low during first clock
//frame_ must be low during second clock
always @(posedge clk) begin
    if (rg_gnt1[2] & frame_) begin
	$fdisplay(f_name, "Error_T2_biu: MONITOR: ERROR, FRAME_ should be low one clock after GNT_ is first low");
	$display("Error_T2_biu: MONITOR: ERROR, FRAME_ should be low one clock after GNT_ is first low");
    end
end



always @(posedge clk or negedge rst_) begin
  if (!rst_) rg_gnt[3]=0;
  else rg_gnt[3]= #1 gnt_;
end
assign gnt_hlos[3]= rg_gnt[3] & !gnt_;
always @(posedge clk or negedge rst_) begin
  if (!rst_) rg_gnt1[3]=0;
  else  rg_gnt1[3]= #1 (gnt_hlos[3] & frame_ & irdy_);
end
//if neither frame_ nor irdy_ is low when gnt_ is low during first clock
//frame_ must be low during second clock
always @(posedge clk) begin
    if (rg_gnt1[3] & frame_) begin
	$display("Error_T2_biu: MONITOR: ERROR, FRAME_ should be low one clock after GNT_ is first low");
	$fdisplay(f_name, "Error_T2_biu: MONITOR: ERROR, FRAME_ should be low one clock after GNT_ is first low");
    end
end

****************/
/////////////////////////////////////////////////////////////////
//
// To detect error that IRDY_ is not low one clock after FRAME_ is low
//
/////////////////////////////////////////////////////////////////


reg rg_frame;
always @(posedge clk or negedge rst_) begin
  if (!rst_) rg_frame=0;
  else rg_frame= #1 frame_;
end

wire frame_hlos= rg_frame & !frame_;

reg rg_frame1;
always @(posedge clk or negedge rst_) begin
  if (!rst_) rg_frame1=0;
  else rg_frame1= #1 frame_hlos;
end

// the following situation is allowable in pci PCI spec

//always @(posedge clk) begin
//    if (rg_frame1 & irdy_) begin
//	$fdisplay(f_name, "Error_T2_biu: MONITOR: WARNING, IRDY_ should be low one clock after FRAME_ is first low");
//	$display("Error_T2_biu: MONITOR: WARNING, IRDY_ should be low one clock after FRAME_ is first low");
//    end
//end

/////////////////////////////////////////////////////////////////
//
// To detect unused commands of 194,
//
//  0: interrupt ACK
//  1: Special cycle
//  4: Reserved
//  5: reserved
//  8: Reserved
//  9: Reserved
// 13: Dual Address cycle
//
/////////////////////////////////////////////////////////////////

always @(posedge clk)
    if (frame_hlos)  begin
	case (cbe_)
//	4'h0,	// Interrupt Acknowlege
	4'h1,	// Special Cycle
	4'h4,	// Reserved
	4'h5,	// Reserved
	4'h8,	// Reserved
	4'h9,	// Reserved
	4'hd: 	// Dual Address Cycle
	begin
	    $fdisplay(f_name, "Error_T2_biu: MONITOR: ERROR, Unsupported commands , command=%h", cbe_);
	    $display("Error_T2_biu: MONITOR: ERROR, Unsupported commands, command=%h", cbe_);
	end
     endcase
  end


endmodule
