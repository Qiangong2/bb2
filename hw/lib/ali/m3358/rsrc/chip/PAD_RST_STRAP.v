/*********************************************************************
                                                                     *
File:           pad_rst_strap.v                                      *
Author:         Snow Yi, Roger Fu, Kandy Kong                        *
Version:        2000-03-20      initial version                      *
Descript:       Move all misc functions here, clean top level module *
Modification:   2000-03-30      Ver 1.0                              *
                2000_05_10      cd_mode not use strap pins mode      *
                2000-08-03      Add pc_trace for JTAG mode           *
                2001-03-28      Only remain strap pin, reset logic   *
                2002-06-21      Modify the strap pins for m6304,yuky *
                2002-08-02		Modified the logic of the
                2002-08-08		Add test_mode to control the output reset
                				signal to other module. It includes:
                				core_coldrst_
                				core_warmrst_
                				chipset_rst_
                2002-10-07		Change the working mode, total 6 modes:
                					ide, cd, pci, up, cpu, analog test
                				Delete the "fin_flag"
                2003-05-06		T6304 Metal Fix.
                				IDE/PCI mode be programmable by software.
                				CPU_PCI_FS changed to 1:4 or 1:8
                				PCI_FS[1] connect to 1.
                				The middle bit of work mode config replaced by PCI_FS[1]
                2003-05-26		Modify the strap pins for m3357, JFR
                				add combo mode
                				remove cpu mode
                2003-07-23		Make the CPU reset to be async enable and sync disable
                				The reset active time become much longer than before.
				2003-08-29		Add work_mode trigger function	JFR
				2004-01-02		Add flash_mode and remove pci_t2risc_fs JFR
				2004-02-13		In scan test mode the bypass PLL will be set to 1.
				2004-02-13		The CPU_PLL_M valus is modified. When the input CPU_PLL_M is lower than
								6'b10_0000, the M value will use the {PLL_M[4:0], 1'b0} and output another signal
								to select the FOUT12 port signal as the CPU_CLK
				2004-04-12		Modified reset sequence according to ALI suggestions. JFR
*********************************************************************/

module  pad_rst_strap(
// Strap pin
                cpu_pll_m,
                cpu_pll_fout_sel,	// 0-> select FOUT, 1-> select FOUT12 as the CPU_CLK
                mem_pll_m,
//                pci_t2risc_fs,
				flash_mode,
//                work_mode,
                ide_mode,
                combo_mode,		//add for m3357 by JFR 030526
//                cd_mode,		//no cd mode in m3357, tie to 0 JFR 030526
//                pci_mode,
//                up_mode,		//no up mode in m3357, tie to 0 JFR 030526
//                cpu_mode,		//no cpu mode in m3357, tie to 0 JFR 030526
//               cpu_mode_,		//no pci mode in m3357, tie to 1 JFR 030526
//                analogt_mode,
				tvenc_test_mode,//analogt_mode in m6304
				servo_only_mode,//added for m3357	JFR030616
				servo_test_mode,//added for m3357	JFR030616
				servo_sram_mode,//added for m3357	JFR030616
                pll_bypass,
                cpu_probe_en,
                //fixed setting
                cpu_endian_mode,//little endian
// Strap pin program
                cpu_pll_m_ack,
                mem_pll_m_ack,
//                pci_t2risc_fs_ack,
                work_mode_ack,
// roger
                clk_gen_rst_,
                core_coldrst_,
                core_warmrst_,
                boot_config_finish_,
                pad_boot_config,
                chipset_rst_,
                pad_rst_,//reset padmisc for strape pin scrap
// input
// strap pin program
                cpu_pll_m_tri,
                cpu_pll_m_ctrldata,
                mem_pll_m_tri,
                mem_pll_m_ctrldata,
 //               pci_t2risc_fs_tri,
 //               pci_t2risc_fs_ctrldata,
                work_mode_tri,
                work_mode_ctrldata,

                strap_pin_in,
                sw_rst_pulse,
                clk_src_in,
                warm_rst_in_,
                cold_rst_in_,
                j_pr_rst_,
                cpu_bus_clk,
                test_mode
                );

//strap pins
output  [5:0]   cpu_pll_m;		  //JFR 030526
output	        cpu_pll_fout_sel; // 0-> select FOUT, 1-> select FOUT12 as the CPU_CLK

//output  [1:0]   pci_t2risc_fs;
output  [1:0]   flash_mode;
output  [5:0]   mem_pll_m		 ;//JFR 030526
output          ide_mode,
                combo_mode,		//add for m3357 by JFR 030526
//                pci_mode,
				tvenc_test_mode,//analogt_mode in m6304
				servo_only_mode,//added for m3357	JFR030616
				servo_test_mode,//added for m3357	JFR030616
				servo_sram_mode,//added for m3357	JFR030616
                pll_bypass,
                cpu_probe_en;

output          cpu_endian_mode;

                // roger
output          clk_gen_rst_,
                core_coldrst_,
                core_warmrst_,
                boot_config_finish_,
                chipset_rst_,
                pad_rst_;
output  [31:0]  pad_boot_config;

output          cpu_pll_m_ack;
output          mem_pll_m_ack;
//output          pci_t2risc_fs_ack;
output			work_mode_ack;

input   [31:0]  strap_pin_in;

input           sw_rst_pulse;

input           clk_src_in;
input           warm_rst_in_;
input           cold_rst_in_;
input           j_pr_rst_;
input           cpu_bus_clk ;
input			test_mode;					// chip test mode

input           cpu_pll_m_tri;
input[5:0]      cpu_pll_m_ctrldata;//JFR 030526
input           mem_pll_m_tri;
input[5:0]      mem_pll_m_ctrldata;//JFR 030526
//input           pci_t2risc_fs_tri;
//input[1:0]      pci_t2risc_fs_ctrldata;
input			work_mode_tri;
input[2:0]		work_mode_ctrldata;

parameter udly  = 1;
parameter clock_gen_rst_length = 10'h0;
parameter warm_rst_length = 9'h0;
// roger 's code begin
//reg     [15:0]  coldrst_sync_;
//reg             cold_gen_rst_;
reg             core_coldrst_syna_;
reg             core_coldrst_synb_;
reg             core_coldrst_sync_;
// reg             core_coldrst_;
// reg             core_warmrst_;
reg     [8:0]   warm_rst_cnt;
reg     [9:0]   gen_cnt;
reg             clk_gen_rst_;
reg     [7:0]   warmrst_in_sync_;
//reg             warmrst_input_;
// roger 's code end
reg     [5:0]   cpu_pll_m_config;	//JFR 030526
reg				cpu_probe_en_config;
reg     [8:7] 	flash_mode_config;
reg     [11:9] 	work_mode_config;
reg     [17:12]  mem_pll_m_config;
reg             pll_bypass_config;
reg     [31:19]	reference_info_config;

//strap program signal handshake
reg             cpu_pll_m_tri_latch_1,
                cpu_pll_m_tri_latch_2,
                cpu_pll_m_tri_latch_3;
reg             mem_pll_m_tri_latch_1,
                mem_pll_m_tri_latch_2,
                mem_pll_m_tri_latch_3;
reg             work_mode_tri_latch_1,
                work_mode_tri_latch_2,
                work_mode_tri_latch_3;
wire    [31:0]  pad_boot_config;

//working mode
reg  	ide_mode    ;
reg	 	combo_mode	 ;//add for m3357 by JFR 030526
//reg  cd_mode     ;
//reg  	pci_mode    ;
//reg  up_mode     ;
//reg  cpu_mode    ;
//reg  cpu_mode_   ;
//reg  analogt_mode;
reg		tvenc_test_mode;//analogt_mode in m6304
reg		servo_only_mode;//added for m3357	JFR030616
reg		servo_test_mode;//added for m3357	JFR030616
reg		servo_sram_mode;//added for m3357	JFR030616

wire 	cpu_mode	=	0;//for worm rst JFR030616
//////////////////////////////////////// PAD LATCH ////////////////////////////////////////////////////////
//scrap_pad_ctrl_, 2 clock delay of cold_rst_in_
//pad_rst_temp_, 4 clock delay of cold_rst_in_
reg		cold_rst_in_dly1_;
reg		scrap_pad_ctrl_;
reg		cold_rst_in_dly3_;
reg		pad_rst_temp_;

wire	pad_rst_ = test_mode ? 1'b1 : pad_rst_temp_;

        always @(posedge clk_src_in or negedge cold_rst_in_)
        begin
                if (~cold_rst_in_)
                        cold_rst_in_dly1_ <= #udly 1'b0;
                else
                        cold_rst_in_dly1_ <= #udly cold_rst_in_;
        end

        always @(posedge clk_src_in or negedge cold_rst_in_)
        begin
                if (~cold_rst_in_)
                        scrap_pad_ctrl_ <= #udly 1'b0;
                else
                        scrap_pad_ctrl_ <= #udly cold_rst_in_dly1_;
        end

        always @(posedge clk_src_in or negedge cold_rst_in_)
        begin
                if (~cold_rst_in_)
                        cold_rst_in_dly3_ <= #udly 1'b0;
                else
                        cold_rst_in_dly3_ <= #udly scrap_pad_ctrl_;
        end

        always @(posedge clk_src_in or negedge cold_rst_in_)
        begin
                if (~cold_rst_in_)
                        pad_rst_temp_ <= #udly 1'b0;
                else
                        pad_rst_temp_ <= #udly cold_rst_in_dly3_;
        end

// roger's code begin

        wire    last_gen_cnt = gen_cnt == 10'h3ff;
        //	Release the clock generator reset when the gen_cnt is 10'h380
        //	So the reset time for the chipset will hold 7'h7f 27 MHz clock cycles.
        wire    start_clk_gen = gen_cnt[9:7] == 3'h7;

        always @(posedge clk_src_in or negedge cold_rst_in_)
        begin
                if (~cold_rst_in_)
                        gen_cnt <= #udly clock_gen_rst_length;
                else if (~last_gen_cnt)
                        gen_cnt <= #udly gen_cnt + 1;
        end

        always @(posedge clk_src_in or negedge cold_rst_in_)
        begin
                if (~cold_rst_in_)
                        clk_gen_rst_ <= #udly 1'b0;
                else if (start_clk_gen)
                        clk_gen_rst_ <= #udly 1'b1;
        end

        always @(posedge clk_src_in or negedge cold_rst_in_)
        begin
                if (~cold_rst_in_)
                        core_coldrst_syna_ <= #udly 1'b0;
                else if (last_gen_cnt)
                        core_coldrst_syna_ <= #udly 1'b1;
        end

        reg     [3:0]   coldrst_cnt;
        wire    coldrst_cnt_en = ~(coldrst_cnt == 4'hf);
        wire    last_coldrst = coldrst_cnt == 4'hf;
        always @(posedge cpu_bus_clk or negedge core_coldrst_syna_)
        begin
                if (~core_coldrst_syna_)
                        coldrst_cnt <= #udly 4'h0;
                else if (coldrst_cnt_en)
                        coldrst_cnt <= #udly coldrst_cnt + 1;
        end

        always @(posedge cpu_bus_clk or negedge core_coldrst_syna_)
        begin
                if (~core_coldrst_syna_)
                        core_coldrst_synb_ <= #udly 1'b0;
                else
                        core_coldrst_synb_ <= #udly 1'b1;
        end

        always @(posedge cpu_bus_clk or negedge core_coldrst_synb_)
        begin
        	if (!core_coldrst_synb_)
        		core_coldrst_sync_ <= #udly 0;
        	else
                core_coldrst_sync_ <= #udly 1;
        end

		reg		core_coldrst_tmp_;

        always @(posedge cpu_bus_clk or negedge core_coldrst_sync_)
        begin
                if (~core_coldrst_sync_)
                        core_coldrst_tmp_ <= #udly 1'b0;
                else if (last_coldrst)
                        core_coldrst_tmp_ <= #udly 1'b1;
        end

		assign	core_coldrst_	= test_mode ? cold_rst_in_ : core_coldrst_tmp_;

//        always @(posedge clk_src_in)
//        begin
//                warmrst_in_sync_[0]     <= #udly warm_rst_in_;
//                warmrst_in_sync_[1]     <= #udly warmrst_in_sync_[0];
//                warmrst_in_sync_[2]     <= #udly warmrst_in_sync_[1];
//                warmrst_in_sync_[3]     <= #udly warmrst_in_sync_[2];
//                warmrst_in_sync_[4]     <= #udly warmrst_in_sync_[3];
//                warmrst_in_sync_[5]     <= #udly warmrst_in_sync_[4];
//                warmrst_in_sync_[6]     <= #udly warmrst_in_sync_[5];
//                warmrst_in_sync_[7]     <= #udly warmrst_in_sync_[6];
//                warmrst_input_          <= #udly | warmrst_in_sync_;
//        end

		wire	rst_warm_cnt_ = core_coldrst_;

        wire    en_rst_cnt = warm_rst_cnt == 9'h1ff;
        always @(posedge cpu_bus_clk or negedge rst_warm_cnt_)
        begin
                if (~rst_warm_cnt_)
                        warm_rst_cnt <= #udly warm_rst_length;
                else if (!en_rst_cnt)
                        warm_rst_cnt <= #udly warm_rst_cnt + 1;
        end

		reg		core_warmrst_tmp_;
        always @(posedge cpu_bus_clk or negedge rst_warm_cnt_)
        begin
        	if (!rst_warm_cnt_)
        		core_warmrst_tmp_	<= #udly 0;
        	else
                core_warmrst_tmp_ <= #udly en_rst_cnt;
        end

		assign	core_warmrst_	= test_mode ? cold_rst_in_ : core_warmrst_tmp_;

	// No warm reset input in M6304.
	// The chipset reset is an asynchronous reset, it should be long enough and
	// disabled before cpu_core reset.
	assign	chipset_rst_ = test_mode ? cold_rst_in_ : core_coldrst_syna_;

// Strap pins, and its programable logic,JFR030616
//------program setting signals
        //cpu_pll_m_set
        always @(posedge clk_src_in)
        if (!scrap_pad_ctrl_)
                cpu_pll_m_tri_latch_1 <= #udly 1'b0;
        else
                cpu_pll_m_tri_latch_1 <= #udly cpu_pll_m_tri;

        always @(posedge clk_src_in)
        if (!scrap_pad_ctrl_)
                cpu_pll_m_tri_latch_2 <= #udly 1'b0;
        else
                cpu_pll_m_tri_latch_2 <= #udly cpu_pll_m_tri_latch_1;

        always @(posedge clk_src_in)
        if (!scrap_pad_ctrl_)
                cpu_pll_m_tri_latch_3 <= #udly 1'b0;
        else
                cpu_pll_m_tri_latch_3 <= #udly cpu_pll_m_tri_latch_2;

        wire    cpu_pll_m_set       = cpu_pll_m_tri_latch_2 & ~cpu_pll_m_tri_latch_3; //one pulse

        //mem_pll_m_set
        always @(posedge clk_src_in)
        if (!scrap_pad_ctrl_)
                mem_pll_m_tri_latch_1 <= #udly 1'b0;
        else
                mem_pll_m_tri_latch_1 <= #udly mem_pll_m_tri;

        always @(posedge clk_src_in)
        if (!scrap_pad_ctrl_)
                mem_pll_m_tri_latch_2 <= #udly 1'b0;
        else
                mem_pll_m_tri_latch_2 <= #udly mem_pll_m_tri_latch_1;

        always @(posedge clk_src_in)
        if (!scrap_pad_ctrl_)
                mem_pll_m_tri_latch_3 <= #udly 1'b0;
        else
                mem_pll_m_tri_latch_3 <= #udly mem_pll_m_tri_latch_2;

        wire    mem_pll_m_set      = mem_pll_m_tri_latch_2 & ~mem_pll_m_tri_latch_3; //one pulse

        //work_mode_set
        always @(posedge clk_src_in)
        if (!scrap_pad_ctrl_)
                work_mode_tri_latch_1 <= #udly 1'b0;
        else
                work_mode_tri_latch_1 <= #udly work_mode_tri;

        always @(posedge clk_src_in)
        if (!scrap_pad_ctrl_)
                work_mode_tri_latch_2 <= #udly 1'b0;
        else
                work_mode_tri_latch_2 <= #udly work_mode_tri_latch_1;

        always @(posedge clk_src_in)
        if (!scrap_pad_ctrl_)
                work_mode_tri_latch_3 <= #udly 1'b0;
        else
                work_mode_tri_latch_3 <= #udly work_mode_tri_latch_2;

        wire    work_mode_set      = work_mode_tri_latch_2 & ~work_mode_tri_latch_3; //one pulse


//-------strap pins config during cold reset and program
        //cpu_pll_m_config
        always @(posedge clk_src_in)
        if (!scrap_pad_ctrl_)
                cpu_pll_m_config    <= #udly strap_pin_in[5:0];//JFR 030526
        else if (cpu_pll_m_set)
                cpu_pll_m_config    <= #udly cpu_pll_m_ctrldata;


        //mem_pll_m_config
        always @(posedge clk_src_in)
        if (!scrap_pad_ctrl_)
                mem_pll_m_config   <= #udly strap_pin_in[17:12];
        else if (mem_pll_m_set)
                mem_pll_m_config   <= #udly mem_pll_m_ctrldata;


        //work_mode_config
        always @(posedge clk_src_in)
        if (!scrap_pad_ctrl_)
                work_mode_config <= #udly strap_pin_in[11:9];
		else if (work_mode_set)
                work_mode_config <= #udly work_mode_ctrldata;

//-----strap pin config only during cold reset
        //pll_bypass_config
        always @(posedge clk_src_in)
        if (!scrap_pad_ctrl_)
                pll_bypass_config <= #udly strap_pin_in[18];

		//cpu_probe_en_config
		always @(posedge clk_src_in)
        if (!scrap_pad_ctrl_)
                cpu_probe_en_config	 <= #udly strap_pin_in[6];

         //flash_mode_config
        always @(posedge clk_src_in)
        if (!scrap_pad_ctrl_)
                flash_mode_config <= #udly strap_pin_in[8:7];


        //reference_info_config
        always @(posedge clk_src_in)
        if (!scrap_pad_ctrl_)
                reference_info_config <= #udly strap_pin_in[31:19];


		assign  pad_boot_config = {     reference_info_config,		// 13 bits
										pll_bypass_config,			// 1 bit
                                        mem_pll_m_config[17:12],	// 6 bits
                                        work_mode_config[11:9],		// 3 bits
										flash_mode_config[8:7],		// 2 bits
                                        cpu_probe_en_config,		// 1 bit
                                        cpu_pll_m_config[5:0]		// 6 bits
                                   };

		wire    cpu_pll_m_ack       =       cpu_pll_m_tri_latch_3;
        wire    mem_pll_m_ack     	=       mem_pll_m_tri_latch_3;
      	wire	work_mode_ack		=		work_mode_tri_latch_3;

        assign boot_config_finish_ = test_mode ? 1'b0 : scrap_pad_ctrl_;

// roger's code end

//modify for m3357 JFR 030526
wire	combo_mode_tmp			= ~work_mode_config[11] & ~work_mode_config[10] & ~work_mode_config[9]	;//000
wire  	ide_mode_tmp     		= ~work_mode_config[11] & ~work_mode_config[10] &  work_mode_config[9] ;//001
//wire  	pci_mode_tmp        	= ~work_mode_config[14] &  work_mode_config[13] & ~work_mode_config[12]	;//010
wire	tvenc_test_mode_tmp		= ~work_mode_config[11] &  work_mode_config[10] &  work_mode_config[9]	;//011
wire	servo_only_mode_tmp     =  work_mode_config[11] & ~work_mode_config[10] & ~work_mode_config[9]	;//100
wire	servo_test_mode_tmp     =  work_mode_config[11] & ~work_mode_config[10] &  work_mode_config[9]	;//101
wire	servo_sram_mode_tmp     =  (work_mode_config[11] &  work_mode_config[10])	|
									(~work_mode_config[11] &  work_mode_config[10] & ~work_mode_config[9])	; //010,110 or 111


		always @(posedge clk_src_in)	//latch the working mode
		begin
			combo_mode			<= #udly	combo_mode_tmp	;
			ide_mode    		<= #udly	ide_mode_tmp    ;
//			pci_mode    		<= #udly	pci_mode_tmp    ;
			tvenc_test_mode		<= #udly	tvenc_test_mode_tmp;
			servo_only_mode		<= #udly	servo_only_mode_tmp ;
			servo_test_mode		<= #udly	servo_test_mode_tmp ;
			servo_sram_mode		<= #udly	servo_sram_mode_tmp ;
		end

//JFR 030526
//	When the cpu_pll_m_config is less then 6'b10_0000, it will be shift left for 1 bit and
//	it will indicate the MUX outside CPU_PLL to use the CPU_PLL FOUT12
assign  cpu_pll_m           = cpu_pll_m_config[5] ? cpu_pll_m_config[5:0] : {cpu_pll_m_config[4:0], 1'b0};    // PLL frequency selection
assign	cpu_pll_fout_sel	= !cpu_pll_m_config[5];
assign	cpu_probe_en		= test_mode ? 1'b0 : cpu_probe_en_config;
assign  flash_mode   		= test_mode ? 2'b0 : flash_mode_config[8:7];
assign  mem_pll_m          	= mem_pll_m_config[17:12];
assign  pll_bypass      	= test_mode ? 1'b1 : pll_bypass_config;
assign  cpu_endian_mode 	= 1'b0; //little endian

endmodule
