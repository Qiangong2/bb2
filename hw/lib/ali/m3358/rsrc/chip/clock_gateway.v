/******************************************************************************
	  (c) copyrights 2000-. All rights reserved

	   T-Square Design, Inc.

	  Company confidential and Properietary	information.
	  This information may not be disclosed	to unauthorized individual.
*******************************************************************************/

/*******************************************************************************
 *
 *	DESCRIPTION:	The clock gateway in M6303E.
 *					Generate clock output in PCI/CD/uP/CPU mode
 *
 *	AUTHOR:			Norman
 *
 *	HISTORY:		2002.03.11	initial version
 					2002.06.12	add the selection to PCI_CLK output pad
 								add f27m, f24m input, add ve_clk, pixel_clk, spdif_clk
 					2002-06-18	change the port width mem_dly_sel from [2:0] to [3:0]
 					2002-07-04	add input div_100_clk, div_66_clk, div_33_clk
 								The PCI clock output pad have 8 source to select now.
 								The pci_mbus_sel changed from [1:0] to [2:0].
 								Add pci_clk_cfg to control internal PCI clock source.
 					2002-07-12	Add two delay cell for ATA66 and ATA100 clock
 					2002-09-05	Add CPU_CLK in test mode should be switch to jtag clock
 					2002-10-08	Remove f24m, div_100_clk, div_66_clk, div_33_clk, div_trace_clk,
 								pci_clk_cfg, cpu_clk_src,
 								ata100_clk, ata66_clk, spdif_clk
 					2003-12-09	Switch the MEM_CLK to inverted jtag clock in scan test mode
 					2004-3-31	Add the clock switch and the clock gate for standby mode

 *
 *********************************************************************************/
module clock_gateway(
		f48m,
		f12m,
		f27m,
		f54m_in,
		f162m,
		src_clk_in,				// clock input from the F27_IN
		src_clk_16,				//	1/16 SRC clock
		src_clk_64,				//	1/64 SRC clock
		cpu_mem_sb_clk_sel,		// the source select of the CPU/MEM/SB clocks,
		all_clock_en,			// the clock enable of all the clocks except the CPU/MEM/SB clocks
		cpu_mem_sb_en,			// CPU/MEM/SB clocks

		div_cpu_clk,
		div_mem_clk,
		div_dsp_clk,
		div_ve_clk,
		div_pci_m_clk,
		div_servo_clk,
		div_ide_clk,
		j_tclk,
		test_en,
		cpu_mode,
		gcsys_dly_sel_0,
		gcsys_dly_sel_1,
		mem_dly_sel,
//		pci_mbus_sel,
//		pci_clk_cfg,
		test_mode,
		pscan_en,


		cpu_clk,
//		cpu_clk_src,
		mbus_clk,
		mem_clk,
		pci_clk,
		out_mem_clk,
		pci_mbus_clk,
		sb_clk,
		usb_clk,
		dsp_clk,
		gpu_clk,
		ve_clk,
		f54m_clk,
		ide_clk,
		servo_clk,
		div_gcsys_dly_clk_0,
		div_gcsys_dly_clk_1
//		pixel_clk
		);

	input			f48m,				// crystal clock input
					f54m_in,			// 54MHz clock input
					src_clk_in,			// clock input from the F27_IN
					f12m,				// 1/4 crystal clock
					f27m,				// 27MHz for video application
					f162m,
					div_cpu_clk,		// cpu clock from clock_divider
					div_mem_clk,		// mem clock from clock_divider
					div_dsp_clk,		// dsp clock from clock divider
					div_ve_clk,			// video engine clock from clock divider
					div_pci_m_clk,		// pci_m_clk form clock divider
					div_servo_clk,		// servo clock from clock divider
					div_ide_clk,		// ide clock
					j_tclk,				// EJTAG clock from ICE
					test_en,			// CPU test mode enable
					cpu_mode;			// CPU mode of M6303E
	input			src_clk_16;			//	1/16 SRC clock
	input			src_clk_64;			//	1/64 SRC clock
	input			cpu_mem_sb_clk_sel;	// the source select of the CPU/MEM/SB clocks,
	input			all_clock_en;		// the clock enable of all the clocks except the CPU/MEM/SB clocks
	input			cpu_mem_sb_en;		// CPU/MEM/SB clocks
	
	input	[4:0]	gcsys_dly_sel_0;
	input	[4:0]	gcsys_dly_sel_1;
	input	[4:0]	mem_dly_sel;		// memory clock delay select JFR030718
//	input	[1:0]	pci_mbus_sel;		// PCI_CLK output pad source selection
	input			test_mode;			// Chip in Test Mode
	input			pscan_en;			// Progressive scan enable

	output			cpu_clk,			// cpu clock
					mbus_clk,			// mbus clock
					mem_clk,			// memory clock
					pci_clk,			// pci clock
					out_mem_clk,		// memory write clock
					pci_mbus_clk,		// pci_mbus_clk
					sb_clk,				// south bridge clock
					usb_clk,			// USB clock
					dsp_clk,			// DSP clock
					gpu_clk,			// GPU clock
					ve_clk,				// video engine clock
					f54m_clk,			// 54 MHz clock for IDE and TV encoder
					ide_clk,			// ide clock
					servo_clk,			// servo clock
					div_gcsys_dly_clk_0,
					div_gcsys_dly_clk_1;
//					pixel_clk			// pixel_clk


	/*=========================================================================
		Memory clock
		Insert fixed delay in internal memory clock
		Insert programmable delay in external memory clock
	=========================================================================*/
	wire	mem_dly_prg_out;

	//	MEM clock be the PLL output clock or the BYPASSED clock
	wire	mem_clk_tmp = test_mode ? src_clk_in : (cpu_mem_sb_clk_sel ? src_clk_16 : div_mem_clk);

	P_DLY_CHAIN32 gcsys_clk_out_chain_0(
						.A			(f162m		),
						.Y			(div_gcsys_dly_clk_0),
						.YN			(),
						.DLY_SEL	(gcsys_dly_sel_0	)
						);
	
	P_DLY_CHAIN32 gcsys_clk_out_chain_1(
						.A			(f162m		),
						.Y			(div_gcsys_dly_clk_1),
						.YN			(),
						.DLY_SEL	(gcsys_dly_sel_1	)
						);
	
	P_DLY_CHAIN32 mem_clk_out_chain(
						.A			(mem_clk_tmp		),
						.Y			(mem_dly_prg_out	),
						.YN			(),
						.DLY_SEL	(mem_dly_sel		)
						);

	//	MEM clock will be switch to the F27M clock from the external crystal in TEST_MODE
	//	The switch is done in up level. So the test_mode switch here is removed.

	//	assign	mem_clk		= mem_clk_tmp;
	//	Add the GATX20 for the output clocks
	GATX20	MEM_CLK_GATE (
					.CLK	(mem_clk_tmp		),
					.EN		(cpu_mem_sb_en		),	// The clock for internal module use the mem_clk
					.SE		(test_mode			),
					.YGCLK	(mem_clk			)
					);

	//	assign	out_mem_clk = mem_dly_prg_out;
	//	Add the GATX20 for the output clocks
	GATX20	MEM_CLK_OUT_GATE (
					.CLK	(mem_dly_prg_out	),
					.EN		(all_clock_en		),	// The memory clock for the external DRAM will be closed in standby mode
					.SE		(test_mode			),
					.YGCLK	(out_mem_clk		)
					);

	/*=========================================================================
		CPU clock
	=========================================================================*/
	wire	cpu_clk_tmp 	= (test_en | test_mode) ? j_tclk :
								(cpu_mem_sb_clk_sel ? src_clk_16 : div_cpu_clk);
	//	Add the GATX20 for the output clocks
	GATX20	CPU_CLK_GATE (
					.CLK	(cpu_clk_tmp		),
					.EN		(cpu_mem_sb_en		),
					.SE		(test_mode			),
					.YGCLK	(cpu_clk			)
					);

	/*=========================================================================
		pci_mbus clock, output to PCI_CLK PAD
		pci clock, internal PCI_CLK

	=========================================================================*/
	//	assign	pci_mbus_clk= div_pci_m_clk;
	//	Add the GTAX20 for the output pci_mbus_clk
	GATX20	PCI_MBUS_CLK_GATE	(
					.CLK	(div_pci_m_clk		),
					.EN		(all_clock_en		),
					.SE		(test_mode			),
					.YGCLK	(pci_mbus_clk		)		//	Be used as the clock to external PCI device
					);

	wire	pci_clk_tmp		= test_mode ? j_tclk : div_pci_m_clk;
	GATX20	PCI_CLK_GATE	(
					.CLK	(pci_clk_tmp		),
					.EN		(all_clock_en		),
					.SE		(test_mode			),
					.YGCLK	(pci_clk			)		//	Be used as the internal PCI clock
					);

	wire	mbus_clk_tmp 	= test_en ? j_tclk : div_pci_m_clk;
	GATX20	MBUS_CLK_GATE	(
					.CLK	(mbus_clk_tmp		),
					.EN		(all_clock_en		),
					.SE		(test_mode			),
					.YGCLK	(mbus_clk			)		//	Be used as the MBUS clock
					);

	/*=========================================================================
		dsp_clk
	=========================================================================*/
	wire	dsp_clk_tmp		= test_mode ? j_tclk : div_dsp_clk;
	GATX20	DSP_CLK_GATE	(
					.CLK	(dsp_clk_tmp		),
					.EN		(all_clock_en		),
					.SE		(test_mode			),
					.YGCLK	(dsp_clk			)
					);

	/*=========================================================================
		servo_clk
	=========================================================================*/
	wire	servo_clk_tmp	= test_mode ? j_tclk : div_servo_clk;
	GATX20	SERVO_CLK_GATE	(
					.CLK	(servo_clk_tmp		),
					.EN		(all_clock_en		),
					.SE		(test_mode			),
					.YGCLK	(servo_clk			)
					);

	/*=========================================================================
		ve_clk
	=========================================================================*/
	wire	ve_clk_tmp		= test_mode ? j_tclk : div_ve_clk;
	GATX20	VE_CLK_GATE	(
					.CLK	(ve_clk_tmp			),
					.EN		(all_clock_en		),
					.SE		(test_mode			),
					.YGCLK	(ve_clk				)
					);

	/*=========================================================================
		South bridge clock
		USB clock
	=========================================================================*/
	wire	sb_clk_tmp		= test_mode ? j_tclk : (cpu_mem_sb_clk_sel ? src_clk_64 : f12m);
	GATX20	SB_CLK_GATE	(
					.CLK	(sb_clk_tmp			),
					.EN		(cpu_mem_sb_en		),	//	SB clock need to be enabled in the standby mode
					.SE		(test_mode			),
					.YGCLK	(sb_clk				)
					);

	wire	usb_clk_tmp 	= test_mode ? src_clk_in : f48m;
	GATX20	USB_CLK_GATE	(
					.CLK	(usb_clk_tmp		),
					.EN		(all_clock_en		),
					.SE		(test_mode			),
					.YGCLK	(usb_clk			)
					);

	/*=========================================================================
		Pixel Clock
	=========================================================================*/
//	assign	pixel_clk	= test_mode ? j_tclk :
//						  pscan_en	? f54m_in : f27m;

	//=========================================================================
	//	SERVO DSP clock 54MHz
	wire	f54m_clk_tmp	= test_mode ? j_tclk : f54m_in;
	GATX20	F54M_CLK_GATE	(
					.CLK	(f54m_clk_tmp		),
					.EN		(all_clock_en		),
					.SE		(test_mode			),
					.YGCLK	(f54m_clk			)
					);

	//=========================================================================
	//	IDE clock
	wire	ide_clk_tmp	= test_mode ? j_tclk : div_ide_clk;
	GATX20	IDE_CLK_GATE	(
					.CLK	(ide_clk_tmp		),
					.EN		(all_clock_en		),
					.SE		(test_mode			),
					.YGCLK	(ide_clk			)
					);

endmodule


