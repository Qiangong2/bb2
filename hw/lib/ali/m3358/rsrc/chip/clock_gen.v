/******************************************************************************
	  (c) copyrights 2000-. All rights reserved

	   T-Square Design, Inc.

	  Company confidential and Properietary	information.
	  This information may not be disclosed	to unauthorized individual.
*******************************************************************************/

/*******************************************************************************
 *	DESCRIPTION:	The clock generator in M6303E.
 *					Generate clock output in PCI/CD/uP/CPU mode
 *
 *	AUTHOR: 		Norman
 *
 *	HISTORY:   		2002.03.11	initial version
 					2002-06-18	change the SDRAM clock delay select from [2:0] to
 								[3:0]
 					2002-07-16	Remove clock source to PLL output. The PLL source is
 								from the first 27_48MHz crystal.
 								Remove the MEM_FS input.
 								Add MEM_CLK_FOUT input
 					2002-08-12	Add externl_pixel_clk_in and pixel_clk_src_sel
 								input.
 					2002-10-08	Remove the F27M, F100M input. The source clock is
 								fixed at 27MHz now and the F100M for IDE clock is not
 								needed.
 								Add F54M input, PSCAN_EN
 								Remove the PCI_CLK_CFG, PCI clock is generate from CPU clock
 								Remove EXT_PIXEL_CLK_IN, PIXEL_CLK_SRC_SEL input.
 								Remove CPU_SRC_CLK, JDCLK
 								Remove SPDIF_CLK, ATA100_CLK, ATA66_CLK
 					2003-07-01	M3357 version.
 								Add SERVO_CLK
 					2003-12-18	In TEST_MODE, the mem_clk will be switch to the SRC_CLK
 								from the external crystal
 					2004-03-30	Add the standby mode control
 								Add the output port of the ALL_CLOCK_EN
 *
 *********************************************************************************/
module CLOCK_GEN (

	// input
		SRC_CLK,		// from clock input pad
		F48M,			// from F27_PLL 48M output
		F54M,			// from F27_PLL output
		F162M,
		PCI_MIPS_FS,	// select the divisor of PCI/MIPS bus clock
		IDE_FS,			// ide clock frequency select
		DSP_FS,			// dsp clock frequency select
		DSP_DIV_SRC_SEL,// dsp source clock select
		PCI_FS,			// pci clock frequency select
		VE_FS,			// ve clock frequency select
		VE_DIV_SRC_SEL,	// ve source clock select
		J_TCLK,			// JTAG test mode clock
		TEST_EN,		// Test Mode Enable
		CPU_MODE,		// CPU standalone mode
		MEM_DLY_SEL,	// SDRAM clock pad delay chain control
		PLL_FVCO,		// Clock output from PLL
		MEM_CLK_FOUT,	// MEM Clock output from CPU PLL
		BYPASS_PLL,		// BY PASS CPU_PLL
//		PCI_MBUS_SEL,	// PCI_MBUS clock output select
		TEST_MODE,		// CHIP TEST MODE
		PSCAN_EN,		// Progressive scan enable
		STANDBY,		// standby mode enable
		ALL_CLOCK_EN,	// enable all the clock except the CPU/MEM/SB

	// output
		USB_CLK,		// USB clock 48M
		SB_CLK,			// SouthBridge Clock 12M
		CPU_CLK,		// CPU clock (with JTAG test mode mask)
		MBUS_CLK,		// MIPS clock
		MEM_CLK,		// internal SDRAM, NorthBridge clock
		DSP_CLK,		// DSP clock
		VE_CLK,			// VIDEO ENGINE CLOCK
//		PIXEL_CLK,		// PIXEL CLOCK, 27/54MHz
		F54M_CLK,		// 54MHz clock for  TV encoder
		IDE_CLK,		// IDE clock
		PCI_CLK,		// internal PCI bus clock
		OUT_MEM_CLK,	// output to SDRAM clock pad
		PCI_MBUS_CLK,	// output to PCI/MIPS clock pad
		SERVO_CLK,		// SERVO clock, it is 1/2 mem_clk
		
		GCVIDCLK,		// GameCube Video clock, it is 54MHz    
		GCSYSCLK,       // GameCube System clock, it is 162MHz 
		GI_CLK, 

		COLD_RST_,
		RST_
		);

	// input
	input			SRC_CLK,		// clock pad input
					F48M,			// from 48M crystal
					F162M,
					J_TCLK,			// JTAG clock
					TEST_EN,		// Test Mode Enable
					CPU_MODE,		// CPU standalone mode
					PLL_FVCO;		// PLL clock input from cpu_pll
	input			MEM_CLK_FOUT;	// Memory clock output from CPU PLL
	input			F54M;			// 54 MHz clock input

	input	[1:0]	PCI_MIPS_FS;	// select the divisor of PCI/MIPS bus clock
	input	[1:0]   IDE_FS;			//ide clock frequency select
	input	[1:0]	DSP_FS;			// dsp clock frequency select
	input			DSP_DIV_SRC_SEL;// dsp source clock select
	input	[1:0]	PCI_FS;			// pci clock frequency select
	input	[1:0]	VE_FS;			// ve clock frequency select
	input			VE_DIV_SRC_SEL;	// ve source clock select

	input	[4:0]	MEM_DLY_SEL;	// SDRAM clock pad delay chain control
	input			BYPASS_PLL;		// by pass cpu_pll
//	input	[1:0]	PCI_MBUS_SEL;	// PCI_MUBS output select
	input			TEST_MODE;		// CHIP IN TEST MODE
	input			PSCAN_EN;		// Progressive Scan Enable
	input			STANDBY;		// standby mode enable
	output			ALL_CLOCK_EN;	// enable all the clock except the CPU/MEM/SB

	// output
	output			USB_CLK,		// USB clock 48M
					SB_CLK,			// SouthBridge Clock 12M
					CPU_CLK,		// CPU clock (with JTAG test mode mask)
					MBUS_CLK,		// MIPS clock
					MEM_CLK,		// internal SDRAM, NorthBridge clock
					DSP_CLK,		// DSP clock
					VE_CLK,			// VIDEO ENGINE CLOCK
//					PIXEL_CLK,		// PIXEL_CLK
					PCI_CLK,		// internal PCI bus clock
					OUT_MEM_CLK,	// output to SDRAM clock pad
					PCI_MBUS_CLK;	// output to PCI/MIPS clock pad
	output			F54M_CLK;		// 54MHz clock for TV
	output			IDE_CLK;		// IDE clock
	output			SERVO_CLK;		// SERVO clock, it is 1/2 mem_clk
	
	output			GCVIDCLK;		// GameCube Video clock, it is 54MHz
	output	[1:0]	GCSYSCLK;		// GameCube System clock, it is 162MHz
	output			GI_CLK;			// GI clock, it is 1/2 cpu_clk

	input			COLD_RST_;
	input			RST_;


	/*=========================================================================
		The clock input to CPU_PLL is 12MHz.
		The clock input to F27_PLL is 12MHz.
		The clock for SPDIF is 24MHz.
		Those clocks could be got by divide the F48M in by 2.
	=========================================================================*/
	wire	f48m_bypass_tmp = BYPASS_PLL ? SRC_CLK : F48M;

	reg		f24m;		// 24MHz clock to SPDIF and to 12MHz divider
	reg		f12m;		// 12Mhz clock to CPU_PLL, F27_PLL and SB
	
	wire [4:0]	GCSYS_DLY_SEL_0 = 5'h0;
	wire [4:0]	GCSYS_DLY_SEL_1 = 5'h0;

	always @ (negedge COLD_RST_ or posedge f48m_bypass_tmp)
	   if (!COLD_RST_)
	      f24m <= 0;
	   else
	      f24m <= ~f24m;

	always @ (negedge COLD_RST_ or posedge f24m)
	   if (!COLD_RST_)
	      f12m <= 0;
	   else
	      f12m <= ~f12m;

	reg			SRC_CLK_16;			// 1/16 SRC_CLK for CPU and MEM in standby mode
	reg			SRC_CLK_64;			// 1/64 SRC_CLK for the SB in standby mode
	reg	[4:0]	SRC_CLK_CNT;		// the standby mode clock count

	always @ (negedge COLD_RST_ or posedge SRC_CLK)
		if (!COLD_RST_) begin
		   SRC_CLK_CNT	<= 0;
		end else begin
		   SRC_CLK_CNT	<= SRC_CLK_CNT + 1;
		end

	always @ (negedge COLD_RST_ or posedge SRC_CLK)
		if (!COLD_RST_) begin
		   SRC_CLK_16 	<= 0;
		   SRC_CLK_64 	<= 0;
		end else begin
			if (SRC_CLK_CNT[2:0] == 3'b111)
		   	   SRC_CLK_16	<= ~SRC_CLK_16;
		    if (SRC_CLK_CNT[4:0] == 5'b11111)
		       SRC_CLK_64	<= ~SRC_CLK_64;
		end

	/*=========================================================================
		standby mode FSM
	=========================================================================*/
	reg	[2:0]	standby_curr_st;	// current state of the standby control FSM
	reg	[2:0]	standby_next_st;	// next state of the standby control FSM

	parameter	IDLE_ST			= 3'b000;	// The idle state
	parameter	STOP1_ST		= 3'b001;	// The state to stop all the clocks
	parameter	SWITCH1_ST		= 3'b010;	// The state to change the CPU/MEM/SB clock source.
	parameter	STANDBY_ST		= 3'b011;	// The stand by state, enable the CPU/MEM/SB clock
	parameter	STOP2_ST		= 3'b100;	// The state to stop the CPU/MEM_/SB clocks
	parameter	SWITCH2_ST		= 3'b101;	// The state to change the CPU/MEM/SB clock source
	parameter	ENABLE_CLK_ST	= 3'b110;	// The state to enable all the clocks

	reg		standby_dly;
	reg		standby_dly_dly;
	wire	standby_req		= standby_dly & standby_dly_dly;	// The request to let the system go into standby mode
	wire	resume_req		= !standby_dly & !standby_dly_dly;	// The request to let the system wakeup from standby mode

	always @ (posedge SRC_CLK or negedge RST_)
		if (!RST_) begin
		   standby_dly		<= 0;
		   standby_dly_dly	<= 0;
		end else begin
		   standby_dly		<= STANDBY;
		   standby_dly_dly	<= standby_dly;
		end

	always @ (posedge SRC_CLK or negedge RST_)
		if (!RST_)
		   standby_curr_st	<= 0;
		else
		   standby_curr_st	<= standby_next_st;

	always @ (standby_curr_st or standby_req or resume_req)
		case (standby_curr_st)
		IDLE_ST : begin
				if (standby_req)
				   standby_next_st <= STOP1_ST;
				else
				   standby_next_st <= standby_curr_st;
			   end
		STOP1_ST : begin
					standby_next_st <= SWITCH1_ST;
				end
		SWITCH1_ST :
				begin
					standby_next_st <= STANDBY_ST;
				end
		STANDBY_ST :
				begin
				   if (resume_req)
				      standby_next_st	<= STOP2_ST;
				   else
				      standby_next_st	<= standby_curr_st;
				end
		STOP2_ST :
				begin
					standby_next_st	<= SWITCH2_ST;
				end
		SWITCH2_ST :
				begin
					standby_next_st <= ENABLE_CLK_ST;
				end
		ENABLE_CLK_ST :
				begin
					standby_next_st <= IDLE_ST;
				end
		default :
				begin
					standby_next_st	<= IDLE_ST;
				end
		endcase

	reg		ALL_CLOCK_EN;				// Enable all the clocks except the CPU/MEM/SB clocks
	reg		CPU_MEM_SB_EN;				// Enable the CPU/MEM/SB clocks
	always @ (posedge SRC_CLK or negedge RST_)
		if (!RST_) begin
		   ALL_CLOCK_EN		<= 1;
		   CPU_MEM_SB_EN	<= 1;
		end else if (standby_curr_st == STOP1_ST) begin
		   ALL_CLOCK_EN		<= 0;
		   CPU_MEM_SB_EN	<= 0;
		end else if (standby_curr_st == STANDBY_ST) begin
		   CPU_MEM_SB_EN	<= 1;
		end else if (standby_curr_st == STOP2_ST) begin
		   CPU_MEM_SB_EN	<= 0;
		end else if (standby_curr_st == ENABLE_CLK_ST) begin
		   ALL_CLOCK_EN		<= 1;
		   CPU_MEM_SB_EN	<= 1;
		end else if (standby_curr_st == IDLE_ST) begin
		   ALL_CLOCK_EN		<= 1;
		   CPU_MEM_SB_EN	<= 1;
		end

	reg		CPU_MEM_SB_CLK_SEL;			// the source select of the CPU/MEM/SB clocks,
										// 0 -> select the normal output, 1 -> select the standby mode clocks
	always @ (posedge SRC_CLK or negedge RST_)
		if (!RST_) begin
		   CPU_MEM_SB_CLK_SEL	<= 0;
		end else if (standby_curr_st == SWITCH1_ST) begin
		   CPU_MEM_SB_CLK_SEL	<= 1;
		end else if (standby_curr_st == SWITCH2_ST) begin
		   CPU_MEM_SB_CLK_SEL	<= 0;
		end else if (standby_curr_st == IDLE_ST) begin
		   CPU_MEM_SB_CLK_SEL	<= 0;
		end


	/*=========================================================================
		The CPU_PLL could be by passed and the CPU_CLK would be 27MHz in
		In bypass mode
		CPU_CLK = input clock
		MEM_CLK = input clock
		F27M	= 1/4 input clock
		F48M	= 1/4 input clock
		F54M	= 1/4 input clock

	=========================================================================*/
	wire	div_clk_src;	// source clock send to clock_divider
	assign	div_clk_src 	= BYPASS_PLL ? SRC_CLK : PLL_FVCO;
	wire	mem_clk_src		= BYPASS_PLL ? SRC_CLK : MEM_CLK_FOUT;
	wire	f54m_clk_src	= BYPASS_PLL ? f12m : F54M;
	wire	f27m_clk_src	= BYPASS_PLL ? f12m : SRC_CLK;
	wire	f48m_clk_src 	= BYPASS_PLL ? f12m : F48M;
	wire	f162m_clk_src	= BYPASS_PLL ? f12m : F162M;

	clock_divider clock_divider (
			.pci_mips_fs	(PCI_MIPS_FS	),
			.ide_fs			(IDE_FS			),	// ide clock frequency select
			.dsp_fs			(DSP_FS			),	// dsp clock frequency select
			.dsp_div_src_sel(DSP_DIV_SRC_SEL),	// dsp source clock select
			.pci_fs			(PCI_FS			),	// pci clock frequency select
			.ve_fs			(VE_FS			),	// ve clock frequency select
			.ve_div_src_sel	(VE_DIV_SRC_SEL	),	// ve source clock select
			.div_clk_src	(div_clk_src	),
			.mem_clk_src	(mem_clk_src	),	// memory clock after mux
			.f54m_clk_src	(f54m_clk_src	),	// 54MHz clock

//			.div_cpu_clk	(DIV_CPU_CLK	),
			.div_dsp_clk	(DIV_DSP_CLK	),
			.div_ve_clk		(DIV_VE_CLK		),
			.div_pci_m_clk	(DIV_PCI_M_CLK	),
			.div_servo_clk	(DIV_SERVO_CLK	),	// output SERVO clock to clock gateway
			.div_ide_clk	(DIV_IDE_CLK	),
			.div_gi_clk		(DIV_GI_CLK		),
			.rst_			(RST_			)
			);


	clock_gateway clock_gateway (
			.f48m			(f48m_clk_src	),
			.f54m_in		(f54m_clk_src	),
			.f12m			(f12m			),
			.f27m			(f27m_clk_src	),
			.f162m			(f162m_clk_src	),
			.src_clk_in		(SRC_CLK		),	// clock input from the F27_IN
			.src_clk_16		(SRC_CLK_16		),	//	1/16 SRC clock
			.src_clk_64		(SRC_CLK_64		),	//	1/64 SRC clock
			.cpu_mem_sb_clk_sel(CPU_MEM_SB_CLK_SEL),		// the source select of the CPU/MEM/SB clocks,
			.all_clock_en	(ALL_CLOCK_EN	),	// the clock enable of all the clocks except the CPU/MEM/SB clocks
			.cpu_mem_sb_en	(CPU_MEM_SB_EN	),	// CPU/MEM/SB clocks

			.div_cpu_clk	(div_clk_src	),	// use the div_clk_src instead of the DIV_CPU_CLK from divider.
			.div_mem_clk	(mem_clk_src	),
			.div_ve_clk		(DIV_VE_CLK		),
			.div_dsp_clk	(DIV_DSP_CLK	),
			.div_pci_m_clk	(DIV_PCI_M_CLK	),
			.div_servo_clk	(DIV_SERVO_CLK	),
			.div_ide_clk	(DIV_IDE_CLK	),
			.j_tclk			(J_TCLK			),
			.test_en		(TEST_EN		),
			.cpu_mode		(CPU_MODE		),
			.gcsys_dly_sel_0(GCSYS_DLY_SEL_0),
			.gcsys_dly_sel_1(GCSYS_DLY_SEL_1),
			.mem_dly_sel	(MEM_DLY_SEL	),
//			.pci_mbus_sel	(PCI_MBUS_SEL	),
			.test_mode		(TEST_MODE		),
			.pscan_en		(PSCAN_EN		),

			.cpu_clk		(CPU_CLK		),
			.mbus_clk		(MBUS_CLK		),
			.mem_clk		(MEM_CLK		),
			.pci_clk		(PCI_CLK		),
			.out_mem_clk	(OUT_MEM_CLK	),
			.pci_mbus_clk	(PCI_MBUS_CLK	),
			.sb_clk			(SB_CLK			),
			.dsp_clk		(DSP_CLK		),
			.gpu_clk		(		),
			.usb_clk		(USB_CLK		),
//			.pixel_clk		(PIXEL_CLK		),
			.f54m_clk		(F54M_CLK		),
			.ide_clk		(IDE_CLK		),
			.servo_clk		(SERVO_CLK		),
			.ve_clk			(VE_CLK			),
			.div_gcsys_dly_clk_0	(DIV_GCSYS_DLY_CLK_0	),
			.div_gcsys_dly_clk_1	(DIV_GCSYS_DLY_CLK_1	)
			);

//Dove 20040422
wire			GCVIDCLK = f54m_clk_src;
wire	[1:0]	GCSYSCLK = {DIV_GCSYS_DLY_CLK_1,DIV_GCSYS_DLY_CLK_0};
wire			GI_CLK	= DIV_GI_CLK;

endmodule
