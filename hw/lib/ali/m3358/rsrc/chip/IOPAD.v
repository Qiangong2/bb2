/*=============================================================================
// TSMC pad select for m3357
// File:        iopad.v
// Author:      Tod
// History		2004-04-19 initial version
// 2004/05/21   add perr and serr by rico
=============================================================================*/
module  iopad   (
//==================== pad ========================================
// SDRAM Interface (56)
        p_d_clk,         // 1,   SDRAM master clock input
        p_d_addr,       // 12,  SDRAM address
        p_d_baddr,      // 2,   SDRAM bank address
        p_d_dq,         // 32,  SDRAM 32 bits output data pins
        p_d_dqm,        // 4,   SDRAM data mask signal
        p_d_cs_,        // 1,   SDRAM chip select
        p_d_cas_,       // 1,   SDRAM cas#
        p_d_we_,        // 1,   SDRAM we#
        p_d_ras_,       // 1,   SDRAM ras#
// FLASH ROM Interface(3, others share with IDE bus)
		p_rom_ale,		// 1	Flash external 373 latch enable
//        p_rom_ce_,    // 1,   Flash rom chip select
        p_rom_we_,      // 1,   Flash rom write enable
        p_rom_oe_,      // 1,   Flash rom output enable

        p_rom_data,		// 8 	Flash rom data
        p_rom_addr,		// 21	Flash rom address
		p_rom_addr_set2,// 2	Flash rom address set 2 [18:17] for bonding
//pci interface 48
		p_clk		,
		p_gnta_     ,
		p_gntb_     ,
		p_rst_      ,

		p_req_		,
		p_inta_     ,

		p_ad		,
		p_par       ,
		p_frame_    ,
		p_irdy_     ,
		p_trdy_     ,
		p_stop_     ,
		p_devsel_   ,
		p_cbe_      ,
		//serr and perr
		p_serr_     ,
		p_perr_     ,

		pci_clk_frompadmisc			,
		pci_gnta_frompadmisc_	    ,
		pci_gntb_frompadmisc_	    ,
		pci_rst_frompadmisc_	    ,
		pci_frame_out2padmisc_		,
		pci_frame_oe_frompadmisc_	,
		pci_frame_frompadmisc_		,
		//serr and perr
		pci_perr_out2padmisc_		,
		pci_perr_oe_frompadmisc_	,
		pci_perr_frompadmisc_		,
		pci_serr_out2padmisc_		,
		pci_serr_oe_frompadmisc_	,
		pci_serr_frompadmisc_		,
		//=====================
		pci_irdy_out2padmisc_		,
		pci_irdy_oe_frompadmisc_	,
		pci_irdy_frompadmisc_		,
		pci_trdy_out2padmisc_		,
		pci_trdy_oe_frompadmisc_	,
		pci_trdy_frompadmisc_		,
		pci_stop_out2padmisc_		,
		pci_stop_oe_frompadmisc_	,
		pci_stop_frompadmisc_		,
		pci_devsel_out2padmisc_		,
		pci_devsel_oe_frompadmisc_	,
		pci_devsel_frompadmisc_		,
		pci_ad_out2padmisc			,
		pci_ad_oe_frompadmisc_		,
		pci_ad_frompadmisc			,
		pci_par_out2padmisc			,
		pci_par_oe_frompadmisc_		,
		pci_par_frompadmisc			,
		pci_cbe_out2padmisc_		,
		pci_cbe_oe_frompadmisc_		,
		pci_cbe_frompadmisc_	    ,
		pci_req_out2padmisc_		,
		pci_inta_out2padmisc_		,
//------GI Interface (from broadon)
		gc_vid_clk					,
		gc_sys_clk0	                ,
		gc_sys_clk1	                ,
		alt_boot                    ,
		test_ena                    ,
		gc_test_rst                 ,

		prom_scl                    ,
		prom_sda                    ,

		di_data                     ,
		di_brk                      ,
		di_dir                      ,
		di_err                      ,
		di_dstrb                    ,
		di_hstrb                    ,
		di_cover                    ,
		di_stb						,

		ais_clk                     ,
		ais_lr                      ,
		ais_d                       ,

		//---------DISC Interface
		//system (6)
		gc_vid_clk_frompadmisc	,
		gc_sys_clk_frompadmisc0	,
		gc_sys_clk_frompadmisc1	,
		alt_boot_out2padmisc	,
		test_ena_out2padmisc	,
		gc_rst_frompadmisc		,
		//serial eeproom interface
		i2c_clk_frompadmisc		,
		i2c_din_out2padmisc		,
		i2c_dout_frompadmisc	,
		i2c_doe_frompadmisc		,
		//disc interface
		di_in_out2padmisc		,
		di_out_frompadmisc		,
		di_oe_frompadmisc		,
		di_brk_in_out2padmisc	,
		di_brk_out_frompadmisc	,
		di_brk_oe_frompadmisc	,
		di_dir_out2padmisc		,
		di_hstrb_out2padmisc	,
		di_dstrb_frompadmisc	,
		di_err_frompadmisc		,
		di_cover_frompadmisc	,
		di_stb_out2padmisc		,

		ais_clk_out2padmisc		,
		ais_lr_out2padmisc		,
		ais_d_frompadmisc	    ,

//ide interface
		atadiow_	,
		atadior_    ,
		atacs0_     ,
		atacs1_     ,
		atada       ,
		atareset_   ,
		atadmack_   ,

		ataiordy	,
		ataintrq    ,
		atadmarq    ,

		atadd		,

		atadiow_frompadmisc_		,
		atadior_frompadmisc_        ,
		atacs0_frompadmisc_         ,
		atacs1_frompadmisc_         ,
		atadd_frompadmisc           ,
		atadd_oe_frompadmisc_       ,
		atada_frompadmisc           ,
		atareset_frompadmisc_       ,
		atadmack_frompadmisc_       ,
		                            ,
		ataiordy_out2padmisc        ,
		ataintrq_out2padmisc        ,
		atadd_out2padmisc           ,
		atadmarq_out2padmisc        ,
                                    ,
		atareset_oe_frompadmisc_    ,

//video in interface
		vin_pix_clk					,
		vin_pix_data                ,
		vin_hsync_                  ,
		vin_vsync_                  ,

		video_in_pixel_clk_out2padmisc	,
		video_in_pixel_data_out2padmisc ,
		video_in_hsync_out2padmisc_     ,
		video_in_vsync_out2padmisc_     ,

//digital video out
		pix_clk		,
		vout_data	,
		vout_hsync_	,
		vout_vsync_ ,
		pix_clk_frompadmisc,
		pix_data_frompadmisc,
		de_h_sync_frompadmisc,
		de_v_sync_frompadmisc,

//digital tvencoder
		tven_hsync_ ,
        tven_vsync_ ,
        tvenc_h_sync_frompadmisc_,
		tvenc_v_sync_frompadmisc_,

// I2S Audio Interface (11)
        i2so_data0,     // 1,   Serial data for I2S output 0
        i2so_data1,     // 1,   Serial data for I2S output 1
        i2so_data2,     // 1,   Serial data for I2S output 2
        i2so_data3,     // 1,   Serial data for I2S output 3
        i2so_bck,       // 1,   Bit clock for I2S output
        i2so_lrck,      // 1,   Channel clock for I2S output
        i2so_mclk,      // 1,   over sample clock for i2s DAC

        i2si_data,
        i2si_mclk,
        i2si_bck,
        i2si_lrck,

//TV encoder Interface (14)
		vd33a_tvdac,	// 1,	Analog Power, input
		gnda_tvdac,		// 1,	Analog GND, input
		gnda1_tvdac,	// 1,	Analog GND, input
		vsud_tvdac,		// 1,	Analog GND, input
		iout1,			// 1,   Analog video output 1
		iout2,			// 1,   Analog video output 2
		iout3,			// 1,   Analog video output 3
		iout4,			// 1,   Analog video output 4
		iout5,			// 1,   Analog video output 5
		iout6,			// 1,   Analog video output 6
		xiref1,			// 1,	connect external via capacitance toward VDDA
//		xiref2,			// 1,	connect external via capacitance toward VDDA
		xiext,			// 1,	470 Ohms resistor connect pin, I/O
		xiext1,			// 1,	470 Ohms resistor connect pin, I/O
		xidump,			// 1,	For performance and chip temperature, output

// SPDIF (HIGH)
        spdif,          // 1, output only
        spdif_in,

//UART port (2)JFR030730
		uart_tx,		// 1
		uart_rx,

//i2c
		scbsda,
		scbscl,

		scbsda_out2padmisc,
		scbsda_oe_frompadmisc_,
		scbsda_frompadmisc,

		scbscl_out2padmisc,
		scbscl_oe_frompadmisc_,
		scbscl_frompadmisc,

// USB port (6)
        usb2dp,         // 1,   D+ for USB port2
        usb2dn,         // 1,   D- for USB port2
        usb1dp,         // 1,   D+ for USB port1
        usb1dn,         // 1,   D- for USB port1
        usbpon,         // 1,   USB port power control
        usbovc,         // 1,   USB port over current

// Consumer IR Interface (HIGH)
        irrx,           // 1,   consumer infra-red remote controller/wireless keyboard

// General Purpose IO (15~0, 28~24,GPIO)
        gpio,           // 19,  General purpose io

//MS_SD Interface
		sd_clk					,
		sd_cmd                  ,
		sd_data                 ,
		sd_wr_prct              ,
		sd_det                  ,
		ms_clk                  ,
		ms_pwr_ctrl             ,
		ms_bs                   ,
		ms_sdio                 ,
		ms_ins                  ,

		sd_clk_frompadmisc		,

		sd_cmd_frompadmisc		,
		sd_cmd_oe_frompadmisc_	,
		sd_cmd_out2padmisc		,

		sd_data_frompadmisc		,
		sd_data_oe_frompadmisc_	,
		sd_data_out2padmisc		,

		sd_wr_prct_out2padmisc	,
		sd_det_out2padmisc		,

		ms_clk_frompadmisc		,
		ms_pwr_ctrl_frompadmisc	,
		ms_bs_frompadmisc		,

		ms_sdio_frompadmisc		,
		ms_sdio_oe_frompadmisc_	,
		ms_sdio_out2padmisc		,

		ms_ins_out2padmisc		,

// EJTAG Interface (5)
        p_j_tdo,        // 1,   serial test data output
        p_j_tdi,        // 1,   serial test data input
        p_j_tms,        // 1,   test mode select
        p_j_tclk,       // 1,   test clock
        p_j_rst_,       // 1,   test reset

// system (4)
        p_crst_,        // 1,   cold reset
        test_mode,      // 1,   chip test mode input, (for scan chain)
        p_x27in,        // 1,   crystal input (27 MHz for video output)
        p_x27out,       // 1,   crystal output

// power and ground
		vddcore,		// 		core vdd
		vddio,			//		io vdd
		gnd,			//		ground
//		cpu_pllvdd,		//	pll vdd
//		cpu_pllvss,		//	pll vss
		f27_pllvdd,		//  pll vdd
		f27_pllvss,		//  pll vss

//ADC (5)
		vd33a_adc	,	// 1,	3.3V Analog VDD for ADC.
		xmic1in     ,   // 1,   ADC microphone input 1.
		xadc_vref   ,   // 1,   ADC reference voltage (refered to the reference circuit).
		xmic2in     ,   // 1,   ADC microphone input 2.
		gnda_adc    ,   // 1,   Analog GND for ADC.
//========================== internal net ===========================
        // SDRAM & FLASH
        ram_addr_frompadmisc,
        ram_addr_oe_frompadmisc_,
        ram_ba_frompadmisc ,
        ram_ba_oe_frompadmisc_,
        ram_dq_frompadmisc ,
        ram_dq_oe_frompadmisc_,
        ram_cas_frompadmisc_,
        ram_cas_oe_frompadmisc_,
        ram_we_frompadmisc_,
        ram_we_oe_frompadmisc_,
        ram_ras_frompadmisc_,
        ram_ras_oe_frompadmisc_,

        ram_dqm_frompadmisc,
        ram_dqm_oe_frompadmisc_,
        ram_cs_frompadmisc_,
        ram_cs_oe_frompadmisc_,

		ram_dq_out2padmisc,
        ram_addr_out2padmisc,   //strap pins
        ram_ba_out2padmisc, 	//strap pins
        ram_dqm_out2padmisc,    //strap pins
        ram_cs_out2padmisc_,	// scan chain
        ram_cas_out2padmisc_,	// scan chain
        // EEPROM
//        rom_ce_frompadmisc_,
        rom_oe_frompadmisc_,
        rom_we_frompadmisc_,
//added for m3357 JFR030613
        rom_data_frompadmisc,
        rom_addr_frompadmisc,

//        rom_ce_oe_frompadmisc_,
        rom_oe_oe_frompadmisc_,
        rom_we_oe_frompadmisc_,
//added for m3357 JFR030613
        rom_data_oe_frompadmisc_,
        rom_addr_oe_frompadmisc_,

//        rom_ce_out2padmisc_,
        rom_oe_out2padmisc_,
        rom_we_out2padmisc_,

        rom_ale_frompadmisc	      	,
		rom_ale_oe_frompadmisc_     ,
		rom_ale_out2padmisc	        ,

//added for m3357 JFR030613
        rom_data_out2padmisc,
        rom_addr_out2padmisc,
        rom_addr_set2_out2padmisc,

//------GPIO
        gpio_frompadmisc        ,
        gpio_oe_frompadmisc_    ,
        gpio_out2padmisc        ,

//       	i2si_data_out2padmisc    	,
		i2so_data0_out2padmisc    	,
		i2so_data1_out2padmisc    	,
		i2so_data2_out2padmisc    	,
		i2so_data3_out2padmisc    	,
		i2so_mclk_out2padmisc      	,
		i2so_bck_out2padmisc		,
		i2so_lrck_out2padmisc		,


//        i2si_data_frompadmisc		,
//		i2si_data_oe_frompadmisc_	,
		i2so_data0_frompadmisc      ,
		i2so_data1_frompadmisc      ,
		i2so_data2_frompadmisc      ,
		i2so_data3_frompadmisc      ,
		i2so_data0_oe_frompadmisc_	,
		i2so_data1_oe_frompadmisc_	,
		i2so_data2_oe_frompadmisc_	,
		i2so_data3_oe_frompadmisc_	,
		i2so_mclk_frompadmisc		,
		i2so_mclk_oe_frompadmisc_	,
		i2so_lrck_frompadmisc      	,
		i2so_lrck_oe_frompadmisc_	,
		i2so_bck_frompadmisc       	,
		i2so_bck_oe_frompadmisc_	,

		i2si_bck_frompadmisc		,
		i2si_lrck_frompadmisc		,
		i2si_mclk_frompadmisc		,
		i2si_data_out2padmisc		,


		j_tdi_frompadmisc		,
		j_tdo_frompadmisc		,
		j_tms_frompadmisc		,
		j_rst_frompadmisc_	    ,
		j_tclk_frompadmisc		,

		j_tdi_out2padmisc       ,
		j_tms_out2padmisc       ,
		j_tclk_out2padmisc      ,
		j_rst_out2padmisc_      ,
		j_tdo_out2padmisc       ,

		j_tdi_oe_frompadmisc_	,
		j_tdo_oe_frompadmisc_	,
		j_tms_oe_frompadmisc_	,
		j_rst_oe_frompadmisc_	,
		j_tclk_oe_frompadmisc_	,


        //spdif
        spdif_frompadmisc,
        spdif_oe_frompadmisc_,
        spdif_out2padmisc,

        spdif_in_data_out2padmisc,

        p1_ls_mode_fromcore,
        p1_txd_fromcore    ,
        p1_txd_oej_fromcore,
        p1_txd_se0_fromcore,
        p2_ls_mode_fromcore,
        p2_txd_fromcore    ,
        p2_txd_oej_fromcore,
        p2_txd_se0_fromcore,
     //   p3_ls_mode_fromcore,
     //   p3_txd_fromcore    ,
     //   p3_txd_oej_fromcore,
     //   p3_txd_se0_fromcore,
     //   p4_ls_mode_fromcore,
     //   p4_txd_fromcore    ,
     //   p4_txd_oej_fromcore,
     //   p4_txd_se0_fromcore,
        i_p1_rxd_out2core    ,
        i_p1_rxd_se0_out2core,
        i_p2_rxd_out2core    ,
        i_p2_rxd_se0_out2core,
     //   i_p3_rxd_out2core    ,
     //   i_p3_rxd_se0_out2core,
     //   i_p4_rxd_out2core    ,
     //   i_p4_rxd_se0_out2core,
        usbpon_frompadmisc,
        usbpon_oe_frompadmisc_,
        usbovc_out2padmisc,

        irrx_out2padmisc,
        irrx_frompadmisc,
        irrx_oe_frompadmisc_,
//UART JFR030730
		uart_tx_frompadmisc		,
		uart_tx_oe_frompadmisc_ ,
		uart_tx_out2padmisc,

		uart_rx_out2padmisc,

		//TV encoder
		tvenc_iout1_fromcore,
		tvenc_iout2_fromcore,
		tvenc_iout3_fromcore,
		tvenc_iout4_fromcore,
		tvenc_iout5_fromcore,
		tvenc_iout6_fromcore,
		tvenc_xiref1_fromcore,
//		tvenc_xiref2_fromcore,		//	The second reference power
		tvenc_xidump_fromcore,
		tvenc_xiext_fromcore,
		tvenc_xiext1_fromcore,		//	The IEXT1 from tv encoder

//ADC start
		xmic1in_out2core	,
		xadc_vref_fromcore,
		xmic2in_out2core	,
//ADC end

		gpio_pad_driv_fromcore	   		,// default 8  mA
		rom_da_pad_driv_fromcore    	,// default 8  mA
		sdram_data_pad_driv_fromcore	,// default 12 mA
		sdram_ca_pad_driv_fromcore		,// default 12 mA

        //--crystal
        f27m_clk_out2pll,

        //clock
        ext_mem_dclk_fromclkgen,

        test_mode_out2padmisc,
        bond_option_out2padmisc,
        cold_rst_out2padrststrap_


                );
parameter udly  = 1;
//============define the input/output/inout signals===============
//--------------pad-----------------
//--------------SDRAM Interface (55)
output          p_d_clk;                 // 1,   SDRAM master clock input
inout   [11:0]  p_d_addr;               // 12,  SDRAM address
inout   [1:0]   p_d_baddr;      // 2,   SDRAM bank address
inout   [31:0]  p_d_dq;         // 32,  SDRAM 32 bits output data pins
inout   [3:0]   p_d_dqm;        // 4,   SDRAM data mask signal
//inout   [1:0]   p_d_cs_;        // 2,   SDRAM chip select
inout      		p_d_cs_;        // 1,   SDRAM chip select
inout           p_d_cas_;               // 1,   SDRAM cas#
inout           p_d_we_;                // 1,   SDRAM we#
inout           p_d_ras_;               // 1,   SDRAM ras#
//--------------FLASH ROM Interface(3, others share with IDE bus)
inout			p_rom_ale;               // 1,   Flash rom chip select
//inout           p_rom_ce_;              // 1,   Flash rom chip select
inout           p_rom_we_;              // 1,   Flash rom write enable
inout           p_rom_oe_;              // 1,   Flash rom output enable
inout  [7:0]    p_rom_data;             // 8,   Flash rom data
inout  [21:0]   p_rom_addr;             // 21,   Flash rom address
inout  [18:17]	p_rom_addr_set2;		// 2	Flash rom address set 2 [18:17] for bonding
//--------------I2S Audio Interface (8)
inout           i2so_data0;             // 1,   Serial data for I2S output
inout 			i2so_data1;     		// 1,   Serial data for I2S output 1
inout   	    i2so_data2;     		// 1,   Serial data for I2S output 2
inout       	i2so_data3;		     	// 1,   Serial data for I2S output 3
inout           i2so_bck;               // 1,   Bit clock for I2S output
inout           i2so_lrck;              // 1,   Channel clock for I2S output
inout           i2so_mclk;              // 1,   over sample clock for i2s DAC

output			i2si_lrck;
output			i2si_bck;
output			i2si_mclk;
input			i2si_data;

//input           i2si_data;              // 1,   serial data for I2S input

//TV encoder Interface (13)
input			vd33a_tvdac;	// 1,	Analog Power, input
input			gnda_tvdac;		// 1,	Analog GND, input
input			gnda1_tvdac;	// 1,	Analog GND, input
input			vsud_tvdac;		// 1,	Analog GND, input
output			iout1;			// 1,   Analog video output 1
output			iout2;			// 1,   Analog video output 2
output			iout3;			// 1,   Analog video output 3
output			iout4;			// 1,   Analog video output 4
output			iout5;			// 1,   Analog video output 5
output			iout6;			// 1,   Analog video output 6
output			xiref1;			// 1,	connect external via capacitance toward VDDA
//output			xiref2;			// 1,	connect external via capacitance toward VDDA
output			xiext;			// 1,	470 Ohms resistor connect pin, I/O
output			xiext1;			// 1,	470 Ohms resistor connect pin, I/O
output			xidump;			// 1,	For performance and chip temperature, output

//--------------SPDIF (HIGH)
inout			spdif;                  // 1, output
input			spdif_in;
//-------------- UART (HIGH)
inout			uart_tx;		// 1, CARCY
input			uart_rx;

inout		scbsda;
inout		scbscl;

output		scbsda_out2padmisc;
input		scbsda_oe_frompadmisc_;
input		scbsda_frompadmisc;

output  	scbscl_out2padmisc;
input		scbscl_oe_frompadmisc_;
input		scbscl_frompadmisc;

//i2c


//--------------USB port (6)
//inout           usb4dp;                 // 1,   D+ for USB port4
//inout           usb4dn;                 // 1,   D- for USB port4
//inout           usb3dp;                 // 1,   D+ for USB port3
//inout           usb3dn;                 // 1,   D- for USB port3
inout           usb2dp;                 // 1,   D+ for USB port2
inout           usb2dn;                 // 1,   D- for USB port2
inout           usb1dp;                 // 1,   D+ for USB port1
inout           usb1dn;                 // 1,   D- for USB port1
output          usbpon;                 // 1,   USB port power control
input           usbovc;                 // 1,   USB port over current

//--------------Consumer IR Interface (HIGH)
inout           irrx;                   // 1,   consumer infra-red remote controller/wireless keyboard

//--------------General Purpose IO (13~0, 28~24,GPIO)
inout   [15:0]  gpio;                   // 19,  General purpose io

//--------------EJTAG Interface (5)
inout			p_j_tdo;                // 1,   serial test data output
inout           p_j_tdi;                // 1,   serial test data input
inout           p_j_tms;                // 1,   test mode select
inout           p_j_tclk;               // 1,   test clock
inout           p_j_rst_;               // 1,   test reset

//--------------system (5)
input           p_crst_;            // 1,   cold reset
input           test_mode;          // 1,   chip test mode input for scan
//input			test_sel;			// 1,	select the scan chain port
//input           p_x48in;          // 1,   crystal input (48 MHz for USB)
//output          p_x48out;         // 1,   crystal output
input           p_x27in;            // 1,   crystal input (27 MHz for video output)
output          p_x27out;           // 1,   crystal output
// power and ground
input			vddcore;			//	core vdd
//input			vsscore;			//	core vss
input			vddio;				//	io vdd
input			gnd;				//	ground
//input			cpu_pllvdd;			//	pll vdd
//input			cpu_pllvss;			//	pll vss
input			f27_pllvdd;			//	pll vdd for f27 pll
input			f27_pllvss;			// l vss for f27 pll

//--------------ADC (5)
input			vd33a_adc	;		// 1,	3.3V Analog VDD for ADC.
input			xmic1in     ;       // 1,   ADC microphone input 1.
output			xadc_vref   ;       // 1,   ADC reference voltage (refered to the reference circuit).
input			xmic2in     ;       // 1,   ADC microphone input 2.
input			gnda_adc    ;		// 1,   Analog GND for ADC.
//--------------- internal net --------------------------------
// SDRAM & FLASH
input   [11:0]  ram_addr_frompadmisc;
input	[11:0]	ram_addr_oe_frompadmisc_;
input   [1:0]   ram_ba_frompadmisc;
input	[1:0]	ram_ba_oe_frompadmisc_;
input   [31:0]  ram_dq_frompadmisc;
input	[31:0]	ram_dq_oe_frompadmisc_;
input           ram_cas_frompadmisc_,
				ram_cas_oe_frompadmisc_,
                ram_we_frompadmisc_,
                ram_we_oe_frompadmisc_,
                ram_ras_frompadmisc_,
                ram_ras_oe_frompadmisc_;
input			ram_cs_frompadmisc_;//JFR 030704
input			ram_cs_oe_frompadmisc_;//JFR 030704
input   [3:0]   ram_dqm_frompadmisc;
input	[3:0]	ram_dqm_oe_frompadmisc_;

output  [31:0]  ram_dq_out2padmisc;
output  [11:0]  ram_addr_out2padmisc;   //strap pins
output  [1:0]   ram_ba_out2padmisc; 	//strap pins
output  [3:0]   ram_dqm_out2padmisc;    //strap pins
output			ram_cs_out2padmisc_;	// scan chain JFR 030704
output			ram_cas_out2padmisc_;	// scan chain

//pci interface
output	p_clk  	;
output	p_gnta_ ;
output	p_gntb_ ;
output	p_rst_  ;

input	p_req_ 	;
input	p_inta_ ;

inout [31:0]	p_ad   ;
inout	p_par          ;
inout	p_frame_       ;
inout	p_irdy_        ;
inout	p_trdy_        ;
inout	p_stop_        ;
inout	p_devsel_      ;
inout [3:0]	p_cbe_     ;
inout   p_serr_         ;
inout   p_perr_         ;

input	pci_clk_frompadmisc			;
input	pci_gnta_frompadmisc_	    ;
input	pci_gntb_frompadmisc_	    ;
input	pci_rst_frompadmisc_	    ;
output	pci_frame_out2padmisc_		;
input	pci_frame_oe_frompadmisc_	;
input	pci_frame_frompadmisc_		;
//serr and perr
output  pci_perr_out2padmisc_		;
input   pci_perr_oe_frompadmisc_	;
input	pci_perr_frompadmisc_		;
output	pci_serr_out2padmisc_		;
input	pci_serr_oe_frompadmisc_	;
input	pci_serr_frompadmisc_		;
		//==================
output	pci_irdy_out2padmisc_		;
input	pci_irdy_oe_frompadmisc_	;
input	pci_irdy_frompadmisc_		;
output	pci_trdy_out2padmisc_		;
input	pci_trdy_oe_frompadmisc_	;
input	pci_trdy_frompadmisc_		;
output	pci_stop_out2padmisc_		;
input	pci_stop_oe_frompadmisc_	;
input	pci_stop_frompadmisc_		;
output	pci_devsel_out2padmisc_		;
input	pci_devsel_oe_frompadmisc_	;
input	pci_devsel_frompadmisc_		;
output	[31:0] pci_ad_out2padmisc			;
input	[31:0] pci_ad_oe_frompadmisc_		;
input	[31:0] pci_ad_frompadmisc			;
output	pci_par_out2padmisc			;
input	pci_par_oe_frompadmisc_		;
input	pci_par_frompadmisc			;
output	[3:0] pci_cbe_out2padmisc_		;
input	[3:0] pci_cbe_oe_frompadmisc_		;
input	[3:0] pci_cbe_frompadmisc_	    ;
input	pci_req_out2padmisc_		;
input	pci_inta_out2padmisc_		;

//GI Interface (from broadon)
output	gc_vid_clk			;
output	gc_sys_clk0	        ;
output	gc_sys_clk1	        ;
input	alt_boot            ;
input	test_ena    		;
output	gc_test_rst         ;

output	prom_scl 			;
inout	prom_sda            ;

inout [7:0]	di_data         ;
inout	di_brk              ;
input	di_dir              ;
output	di_err              ;
output	di_dstrb            ;
input	di_hstrb            ;
output	di_cover            ;
input	di_stb				;

input	ais_clk             ;
input	ais_lr              ;
output	ais_d               ;

//system (6)
input	gc_vid_clk_frompadmisc	;
input	gc_sys_clk_frompadmisc0	;
input	gc_sys_clk_frompadmisc1	;
output	alt_boot_out2padmisc	;
output	test_ena_out2padmisc	;
input	gc_rst_frompadmisc		;
//serial eeproom interfa
input	i2c_clk_frompadmisc		;
output	i2c_din_out2padmisc     ;
input	i2c_dout_frompadmisc    ;
input	i2c_doe_frompadmisc     ;
//disc interface
output[7:0]	di_in_out2padmisc	;
input [7:0]	di_out_frompadmisc  ;
input [7:0]	di_oe_frompadmisc   ;
output	di_brk_in_out2padmisc   ;
input	di_brk_out_frompadmisc  ;
input	di_brk_oe_frompadmisc   ;
output	di_dir_out2padmisc      ;
output	di_hstrb_out2padmisc    ;
input	di_dstrb_frompadmisc    ;
input	di_err_frompadmisc      ;
input	di_cover_frompadmisc    ;
output	di_stb_out2padmisc		;

output	ais_clk_out2padmisc		;
output	ais_lr_out2padmisc      ;
input	ais_d_frompadmisc       ;


output	atadiow_		;
output	atadior_        ;
output	atacs0_         ;
output	atacs1_         ;
output	[2:0] atada     ;
output	atareset_       ;
output	atadmack_       ;

input	ataiordy		;
input	ataintrq		;
input	atadmarq		;

inout	[15:0] atadd			;

input        atadiow_frompadmisc_	;
input        atadior_frompadmisc_  ;
input        atacs0_frompadmisc_   ;
input        atacs1_frompadmisc_   ;
input [15:0] atadd_frompadmisc     ;
input [15:0] atadd_oe_frompadmisc_ ;
input [2:0]  atada_frompadmisc     ;
input        atareset_frompadmisc_ ;
input        atadmack_frompadmisc_ ;

output         ataiordy_out2padmisc  ;
output         ataintrq_out2padmisc  ;
output [15:0] atadd_out2padmisc     ;
output        atadmarq_out2padmisc  ;

input        atareset_oe_frompadmisc_ ;

//video in interface
input		 vin_pix_clk	;
input [7:0]	vin_pix_data    ;
input		 vin_hsync_     ;
input		 vin_vsync_     ;

output		 video_in_pixel_clk_out2padmisc	;
output [7:0] video_in_pixel_data_out2padmisc ;
output		 video_in_hsync_out2padmisc_    ;
output		 video_in_vsync_out2padmisc_    ;

//digital video out
input		pix_clk_frompadmisc;
input [7:0]	pix_data_frompadmisc;
input		de_h_sync_frompadmisc;
input		de_v_sync_frompadmisc;

output		pix_clk			;
output[7:0]	vout_data	    ;
output		vout_hsync_	    ;
output		vout_vsync_     ;

//digital tvencoder
output		tven_hsync_ ;
output		tven_vsync_ ;
input		tvenc_h_sync_frompadmisc_;
input		tvenc_v_sync_frompadmisc_;

// EEPROM
//input           rom_ce_frompadmisc_;
input           rom_oe_frompadmisc_;
input           rom_we_frompadmisc_;
//added for m3357 JFR030613
input	[7:0]   rom_data_frompadmisc;
input	[21:0]  rom_addr_frompadmisc;
//input	        rom_ce_oe_frompadmisc_;
input	        rom_oe_oe_frompadmisc_;
input	        rom_we_oe_frompadmisc_;
//added for m3357 JFR030613
input   [7:0] 	rom_data_oe_frompadmisc_;
input   [21:0]	rom_addr_oe_frompadmisc_;
//output			rom_ce_out2padmisc_;
output  	    rom_oe_out2padmisc_;
output	        rom_we_out2padmisc_;

input			rom_ale_frompadmisc	   ;
input			rom_ale_oe_frompadmisc_;
output			rom_ale_out2padmisc	   ;

//added for m3357 JFR030613
output  [7:0]   rom_data_out2padmisc;
output  [21:0]  rom_addr_out2padmisc;
output	[18:17]	rom_addr_set2_out2padmisc;

//------MS_SD Interface
output		sd_clk		;
inout		sd_cmd      ;
inout [3:0]	sd_data     ;
input		sd_wr_prct  ;
input		sd_det      ;
output		ms_clk      ;
output		ms_pwr_ctrl ;
output		ms_bs       ;
inout		ms_sdio     ;
input		ms_ins      ;

input		sd_clk_frompadmisc			;

input		sd_cmd_frompadmisc          ;
input		sd_cmd_oe_frompadmisc_      ;
output		sd_cmd_out2padmisc          ;

input [3:0]	sd_data_frompadmisc         ;
input [3:0]	sd_data_oe_frompadmisc_     ;
output[3:0]	sd_data_out2padmisc         ;

output		sd_wr_prct_out2padmisc      ;
output		sd_det_out2padmisc          ;

input		ms_clk_frompadmisc          ;
input		ms_pwr_ctrl_frompadmisc     ;
input		ms_bs_frompadmisc           ;

input		ms_sdio_frompadmisc        ;
input		ms_sdio_oe_frompadmisc_     ;
output		ms_sdio_out2padmisc         ;

output		ms_ins_out2padmisc  		;
//------GPIO
input   [31:0]  gpio_frompadmisc,
                gpio_oe_frompadmisc_;
output  [31:0]  gpio_out2padmisc;

input			j_tdi_frompadmisc		;
input			j_tdo_frompadmisc		;
input			j_tms_frompadmisc		;
input			j_rst_frompadmisc_	    ;
input			j_tclk_frompadmisc		;

output			j_tdi_out2padmisc       ;
output			j_tms_out2padmisc       ;
output			j_tclk_out2padmisc      ;
output			j_rst_out2padmisc_      ;
output			j_tdo_out2padmisc       ;

input			j_tdi_oe_frompadmisc_	;
input			j_tdo_oe_frompadmisc_	;
input			j_tms_oe_frompadmisc_	;
input			j_rst_oe_frompadmisc_	;
input			j_tclk_oe_frompadmisc_	;


//output          i2si_data_out2padmisc    	;
output          i2so_data0_out2padmisc    	;
output          i2so_data1_out2padmisc    	;
output          i2so_data2_out2padmisc    	;
output          i2so_data3_out2padmisc    	;
output          i2so_mclk_out2padmisc      	;
output	        i2so_bck_out2padmisc		;
output	        i2so_lrck_out2padmisc		;

//input			i2si_data_frompadmisc		;
//input			i2si_data_oe_frompadmisc_	;
input           i2so_data0_frompadmisc      ;
input           i2so_data1_frompadmisc      ;
input           i2so_data2_frompadmisc      ;
input           i2so_data3_frompadmisc      ;
input			i2so_data0_oe_frompadmisc_	;
input			i2so_data1_oe_frompadmisc_	;
input			i2so_data2_oe_frompadmisc_	;
input			i2so_data3_oe_frompadmisc_	;
input			i2so_mclk_frompadmisc		;
input			i2so_mclk_oe_frompadmisc_	;
input           i2so_lrck_frompadmisc      	;
input			i2so_lrck_oe_frompadmisc_	;
input           i2so_bck_frompadmisc       	;
input	        i2so_bck_oe_frompadmisc_	;

input			i2si_bck_frompadmisc		;
input			i2si_lrck_frompadmisc       ;
input			i2si_mclk_frompadmisc       ;
output			i2si_data_out2padmisc       ;




//spdif
input       	spdif_frompadmisc;
input       	spdif_oe_frompadmisc_;
output      	spdif_out2padmisc;

output			spdif_in_data_out2padmisc;

input           p1_ls_mode_fromcore     ,
                p1_txd_fromcore         ,
                p1_txd_oej_fromcore     ,
                p1_txd_se0_fromcore     ,
                p2_ls_mode_fromcore     ,
                p2_txd_fromcore         ,
                p2_txd_oej_fromcore     ,
                p2_txd_se0_fromcore     ;
      //          p3_ls_mode_fromcore     ,
      //          p3_txd_fromcore         ,
      //          p3_txd_oej_fromcore     ,
      //          p3_txd_se0_fromcore     ,
      //          p4_ls_mode_fromcore     ,
      //          p4_txd_fromcore         ,
      //          p4_txd_oej_fromcore     ,
      //          p4_txd_se0_fromcore     ;
output          i_p1_rxd_out2core       ,
                i_p1_rxd_se0_out2core   ,
                i_p2_rxd_out2core       ,
                i_p2_rxd_se0_out2core   ;
      //          i_p3_rxd_out2core       ,
      //          i_p3_rxd_se0_out2core   ,
      //          i_p4_rxd_out2core       ,
      //          i_p4_rxd_se0_out2core   ;
input           usbpon_frompadmisc;
input			usbpon_oe_frompadmisc_;
output          usbovc_out2padmisc;

output          irrx_out2padmisc;
input			irrx_frompadmisc,
        		irrx_oe_frompadmisc_;

//UART JFR030730
input			uart_tx_frompadmisc		;
input			uart_tx_oe_frompadmisc_ ;
output			uart_tx_out2padmisc;
output			uart_rx_out2padmisc;

input           ext_mem_dclk_fromclkgen    ;
//input           pci_mbus_clk    ;

output          cold_rst_out2padrststrap_    ;

//TV encoder
//input			tvenc_iout0_fromcore;
input			tvenc_iout1_fromcore;
input			tvenc_iout2_fromcore;
input			tvenc_iout3_fromcore;
input			tvenc_iout4_fromcore;
input			tvenc_iout5_fromcore;
input			tvenc_iout6_fromcore;
//aeede for m3357 JFR 030613
input			tvenc_xiref1_fromcore;
//input			tvenc_xiref2_fromcore;		//	The second reference power
input			tvenc_xidump_fromcore;
input			tvenc_xiext_fromcore;
input			tvenc_xiext1_fromcore;		//	The IEXT1 from tv encoder

output		xmic1in_out2core	;
input		xadc_vref_fromcore;
output		xmic2in_out2core	;

input	[1:0]	gpio_pad_driv_fromcore	   		; // default 8  mA
input	[1:0]	rom_da_pad_driv_fromcore    	; // default 8  mA
input	[1:0]	sdram_data_pad_driv_fromcore	; // default 12 mA
input	[1:0]	sdram_ca_pad_driv_fromcore		; // default 12 mA

output          f27m_clk_out2pll;
//input           pci_clk_oe_frompadmisc_;
output          test_mode_out2padmisc;
//output			test_sel_out2padmisc;
output	[2:0]	bond_option_out2padmisc;
//input           warm_rst_frompadmisc;
//================================================================

wire HIGH, LOW;
/*================================================================
	PAD list
================================================================*/
//SDRAM-------------------
//output signals
assign	p_d_clk			= ext_mem_dclk_fromclkgen;
assign	p_d_addr 		= ram_addr_oe_frompadmisc_[0] ? 12'hz : ram_addr_frompadmisc;
assign	p_d_baddr		= ram_ba_oe_frompadmisc_[0] ? 2'hz : ram_ba_frompadmisc;
assign	p_d_cs_			= ram_cs_frompadmisc_;
assign	p_d_ras_		= ram_ras_frompadmisc_;
assign	p_d_cas_		= ram_cas_frompadmisc_;
assign	p_d_we_			= ram_we_frompadmisc_;
assign	p_d_dqm			= ram_dqm_oe_frompadmisc_[0] ? 4'hz : ram_dqm_frompadmisc;

assign	ram_addr_out2padmisc = p_d_addr;
assign	ram_ba_out2padmisc	 = p_d_baddr;
assign  ram_dqm_out2padmisc  = p_d_dqm;

//inout signals
assign	p_d_dq					= ram_dq_oe_frompadmisc_[0]	? 32'hz : ram_dq_frompadmisc;
assign  ram_dq_out2padmisc		= p_d_dq;

//FLASH------------------
//output signals
assign 	p_rom_addr[7:0]		 = rom_addr_oe_frompadmisc_[0] ? 8'hz : rom_addr_frompadmisc[7:0];
assign	p_rom_addr[21:8]	 = rom_addr_oe_frompadmisc_[8] ? 14'hz : rom_addr_frompadmisc[21:8];
assign  rom_addr_out2padmisc = p_rom_addr;
//assign	p_rom_ce_			= rom_ce_frompadmisc_;
assign	p_rom_oe_			= rom_oe_frompadmisc_;
assign	p_rom_we_			= rom_we_frompadmisc_;
//assign  p_rom_ce_			= rom_oe_frompadmisc_;
//inout signals
assign 	p_rom_data			= rom_data_oe_frompadmisc_[0] ? 8'hz : rom_data_frompadmisc;
assign	rom_data_out2padmisc	= p_rom_data;

assign	p_rom_ale			= rom_ale_frompadmisc;

//PCI--------------------
//output signals
assign	p_clk			= pci_clk_frompadmisc;
assign	p_rst_			= pci_rst_frompadmisc_;
assign	p_gnta_			= pci_gnta_frompadmisc_;
assign	p_gntb_			= pci_gntb_frompadmisc_;
//input signals
assign  pci_req_out2padmisc_	= p_req_;
assign	pci_inta_out2padmisc_	= p_inta_;
//inout signals
assign	p_frame_		= pci_frame_oe_frompadmisc_ ? 1'hz : pci_frame_frompadmisc_;
assign	pci_frame_out2padmisc_ = p_frame_;
//serr and perr
assign	p_serr_		= pci_serr_oe_frompadmisc_ ? 1'hz : pci_serr_frompadmisc_;
assign	pci_serr_out2padmisc_ = p_serr_;

assign	p_perr_		= pci_perr_oe_frompadmisc_ ? 1'hz : pci_perr_frompadmisc_;
assign	pci_perr_out2padmisc_ = p_perr_;
//===============================

assign	p_ad		= pci_ad_oe_frompadmisc_[0] ? 32'hz : pci_ad_frompadmisc;
assign	pci_ad_out2padmisc = p_ad;

assign	p_par		= pci_par_oe_frompadmisc_ ? 1'hz : pci_par_frompadmisc;
assign	pci_par_out2padmisc = p_par;

assign	p_irdy_		= pci_irdy_oe_frompadmisc_ ? 1'hz : pci_irdy_frompadmisc_;
assign	pci_irdy_out2padmisc_ = p_irdy_;

assign	p_trdy_		= pci_trdy_oe_frompadmisc_ ? 1'hz : pci_trdy_frompadmisc_;
assign	pci_trdy_out2padmisc_ = p_trdy_;

assign	p_stop_		= pci_stop_oe_frompadmisc_ ? 1'hz : pci_stop_frompadmisc_;
assign	pci_stop_out2padmisc_ = p_stop_;

assign	p_devsel_		= pci_devsel_oe_frompadmisc_ ? 1'hz : pci_devsel_frompadmisc_;
assign	pci_devsel_out2padmisc_ = p_devsel_;

assign	p_cbe_		= pci_cbe_oe_frompadmisc_[0] ? 4'hz : pci_cbe_frompadmisc_;
assign	pci_cbe_out2padmisc_ = p_cbe_;

//ide interface---------------
//output signals
assign	atadiow_	=	atadiow_frompadmisc_;
assign	atadior_	=	atadior_frompadmisc_;
assign	atacs0_		=	atacs0_frompadmisc_;
assign	atacs1_		= 	atacs1_frompadmisc_;
assign	atada		=	atada_frompadmisc;
assign	atareset_	=	atareset_oe_frompadmisc_ ? 1'hz : atareset_frompadmisc_;
assign	atadmack_	=	atadmack_frompadmisc_;

//input signals
assign	ataiordy_out2padmisc = ataiordy;
assign	ataintrq_out2padmisc = ataintrq;
assign	atadmarq_out2padmisc = atadmarq;

//inout signals
assign	atadd		=	atadd_oe_frompadmisc_[0] ? 16'hz : atadd_frompadmisc;
assign	atadd_out2padmisc = atadd;

//south bridge
//irrx
assign	irrx_out2padmisc	= irrx;

//uart
assign	uart_tx				= uart_tx_frompadmisc;
assign	uart_rx_out2padmisc = uart_rx;

//i2c
assign	scbsda				= scbsda_oe_frompadmisc_ ? 1'hz : scbsda_frompadmisc;
assign	scbsda_out2padmisc  = scbsda;

assign	scbscl				= scbscl_oe_frompadmisc_ ? 1'hz : scbscl_frompadmisc;
assign	scbscl_out2padmisc  = scbscl;

//audio interface-----------------
//i2s
//output signals
assign	i2so_data0			= i2so_data0_frompadmisc;
assign	i2so_data1			= i2so_data1_frompadmisc;
assign	i2so_data2			= i2so_data2_frompadmisc;
assign	i2so_data3			= i2so_data3_frompadmisc;
assign	i2so_bck			= i2so_bck_frompadmisc;
assign	i2so_lrck			= i2so_lrck_frompadmisc;
assign	i2si_mclk			= i2si_mclk_frompadmisc;
assign	i2si_bck			= i2si_bck_frompadmisc;
assign	i2si_lrck			= i2si_lrck_frompadmisc;

//inout signals
assign	i2so_mclk			= i2so_mclk_oe_frompadmisc_ ? 1'hz : i2so_mclk_frompadmisc;
assign	i2so_mclk_out2padmisc = i2so_mclk;

//input signals
assign	i2si_data_out2padmisc = i2si_data;

//spdif----
assign	spdif_in_data_out2padmisc = spdif_in;
assign	spdif	= spdif_frompadmisc;

//video interface--------------------
//video in
assign	video_in_pixel_clk_out2padmisc		= vin_pix_clk ;
assign	video_in_pixel_data_out2padmisc     = vin_pix_data;
assign	video_in_hsync_out2padmisc_         = vin_hsync_  ;
assign	video_in_vsync_out2padmisc_         = vin_vsync_  ;

//digital video out
assign  pix_clk		= pix_clk_frompadmisc;
assign	vout_data	= pix_data_frompadmisc;
assign	vout_hsync_	= de_h_sync_frompadmisc;
assign	vout_vsync_ = de_v_sync_frompadmisc;

//digtal tv encoder
assign  tven_hsync_ = tvenc_h_sync_frompadmisc_;
assign  tven_vsync_ = tvenc_v_sync_frompadmisc_;

//usb pad
usb_pad 	pin146_pin147_NC
					( .A(p2_txd_fromcore), .ANT(HIGH), .DM(usb2dn), .DP(usb2dp),
                      .H_LB(p2_ls_mode_fromcore), .OEB(p2_txd_oej_fromcore),
                      .RCVD(i_p2_rxd_out2core), .SE0(p2_txd_se0_fromcore),
                      .SE0D(i_p2_rxd_se0_out2core)
                	);

usb_pad 	pin142_pin143_NC
					( .A(p1_txd_fromcore), .ANT(HIGH), .DM(usb1dn), .DP(usb1dp),
                      .H_LB(p1_ls_mode_fromcore), .OEB(p1_txd_oej_fromcore),
                      .RCVD(i_p1_rxd_out2core), .SE0(p1_txd_se0_fromcore),
                      .SE0D(i_p1_rxd_se0_out2core)
                	);

assign		usbpon	= usbpon_frompadmisc;
assign		usbovc_out2padmisc = usbovc;

//ejtag interface
assign		p_j_tdo 			= j_tdo_frompadmisc;
assign		j_tdi_out2padmisc 	= p_j_tdi;
assign		j_rst_out2padmisc_ 	= p_j_rst_;
assign		j_tms_out2padmisc  	= p_j_tms;
assign		j_tclk_out2padmisc	= p_j_tclk;

//system
assign		f27m_clk_out2pll 			= p_x27in;
assign		cold_rst_out2padrststrap_ 	= p_crst_;
assign		test_mode_out2padmisc	  	= test_mode;

//gpio
assign		gpio[0]		= gpio_oe_frompadmisc_[0] ? 1'hz : gpio_frompadmisc[0];
assign		gpio[1]		= gpio_oe_frompadmisc_[1] ? 1'hz : gpio_frompadmisc[1];
assign		gpio[2]		= gpio_oe_frompadmisc_[2] ? 1'hz : gpio_frompadmisc[2];
assign		gpio[3]		= gpio_oe_frompadmisc_[3] ? 1'hz : gpio_frompadmisc[3];
assign		gpio[4]		= gpio_oe_frompadmisc_[4] ? 1'hz : gpio_frompadmisc[4];
assign		gpio[5]		= gpio_oe_frompadmisc_[5] ? 1'hz : gpio_frompadmisc[5];
assign		gpio[6]		= gpio_oe_frompadmisc_[6] ? 1'hz : gpio_frompadmisc[6];
assign		gpio[7]		= gpio_oe_frompadmisc_[7] ? 1'hz : gpio_frompadmisc[7];
assign		gpio[8]		= gpio_oe_frompadmisc_[8] ? 1'hz : gpio_frompadmisc[8];
assign		gpio[9]		= gpio_oe_frompadmisc_[9] ? 1'hz : gpio_frompadmisc[9];
assign		gpio[10]		= gpio_oe_frompadmisc_[10] ? 1'hz : gpio_frompadmisc[10];
assign		gpio[11]		= gpio_oe_frompadmisc_[11] ? 1'hz : gpio_frompadmisc[11];
assign		gpio[12]		= gpio_oe_frompadmisc_[12] ? 1'hz : gpio_frompadmisc[12];
assign		gpio[13]		= gpio_oe_frompadmisc_[13] ? 1'hz : gpio_frompadmisc[13];
assign		gpio[14]		= gpio_oe_frompadmisc_[14] ? 1'hz : gpio_frompadmisc[14];
assign		gpio[15]		= gpio_oe_frompadmisc_[15] ? 1'hz : gpio_frompadmisc[15];
assign		gpio_out2padmisc = gpio;

//ms_sd
assign		sd_clk				= sd_clk_frompadmisc;

assign		sd_cmd				= sd_cmd_oe_frompadmisc_ ? 1'hz : sd_cmd_frompadmisc;
assign		sd_cmd_out2padmisc	= sd_cmd;

assign		sd_data[0]			= sd_data_oe_frompadmisc_[0] ? 1'hz : sd_data_frompadmisc[0];
assign		sd_data[1]			= sd_data_oe_frompadmisc_[1] ? 1'hz : sd_data_frompadmisc[1];
assign		sd_data[2]			= sd_data_oe_frompadmisc_[2] ? 1'hz : sd_data_frompadmisc[2];
assign		sd_data[3]			= sd_data_oe_frompadmisc_[3] ? 1'hz : sd_data_frompadmisc[3];
assign		sd_data_out2padmisc = sd_data;

//assign		sd_wr_prct			= sd_wr_prct_out2padmisc;
assign		sd_wr_prct_out2padmisc		= sd_wr_prct;
assign		sd_det_out2padmisc			= sd_det;

assign		ms_clk				= ms_clk_frompadmisc;
assign		ms_pwr_ctrl			= ms_pwr_ctrl_frompadmisc;
assign		ms_bs				= ms_bs_frompadmisc;

assign		ms_sdio				= ms_sdio_oe_frompadmisc_ ? 1'hz : ms_sdio_frompadmisc;
assign		ms_sdio_out2padmisc	= ms_sdio;

assign		ms_ins_out2padmisc	= ms_ins;

//Broadon pins
//---------DISC Interface
//system (6)
assign		gc_vid_clk				= gc_vid_clk_frompadmisc;
assign		gc_sys_clk0				= gc_sys_clk_frompadmisc0;
assign		gc_sys_clk1				= gc_sys_clk_frompadmisc1;
assign		alt_boot_out2padmisc	= alt_boot;
assign		test_ena_out2padmisc	= test_ena;
assign		gc_test_rst				= gc_rst_frompadmisc;
//serial eeproom interface
assign		prom_scl				= i2c_clk_frompadmisc;
assign		i2c_din_out2padmisc		= prom_sda;
assign		prom_sda				= i2c_doe_frompadmisc ? i2c_dout_frompadmisc : 1'hz;

//disc interface
assign		di_in_out2padmisc		= di_data;
assign		di_data					= di_oe_frompadmisc[0] ? di_out_frompadmisc : 8'hz;

assign		di_brk_in_out2padmisc	= di_brk;
assign		di_brk					= di_brk_oe_frompadmisc ? di_brk_out_frompadmisc : 1'hz;

assign		di_dir_out2padmisc		= di_dir;
assign		di_hstrb_out2padmisc	= di_hstrb;
assign		di_dstrb				= di_dstrb_frompadmisc;
assign		di_err					= di_err_frompadmisc;
assign		di_cover				= di_cover_frompadmisc;
assign		di_stb_out2padmisc		= di_stb;

assign		ais_clk_out2padmisc		= ais_clk;
assign		ais_lr_out2padmisc		= ais_lr;
assign		ais_d					= ais_d_frompadmisc;

endmodule
















