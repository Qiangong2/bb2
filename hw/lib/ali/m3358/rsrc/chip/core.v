/**********************************************************************************
// Description: M3358 core module, includes cpu behavior model and real cpu core
//              and ejatg, t2chipset core
// File:        core.v
// Author:      Tod
// History:		2004-04-22, Initial version, copy from m3357

***********************************************************************************/
module core (
//------chipset bus
        // SDRAM & FLASH
        ram_addr,
        ram_ba,
        ram_dq_out,
        ram_dq_in,
        ram_dq_oe_,
        ram_cas_,
        ram_we_,
        ram_ras_,
        ram_cke,
        ram_dqm,
        ram_cs_,
        // EEPROM
        eeprom_addr,
        eeprom_rdata,
//        eeprom_ce_,
        eeprom_oe_,
        eeprom_we_,
        rom_data_oe_,
        rom_addr_oe,
        eeprom_ale		,
//		eeprom_ale_oe_	,
        eeprom_wdata,
        rom_en_,                 //indicates start rom operation
        // LED
//      ledpin,
        // PCI
        oe_ad_,
        out_ad,
        out_rst_,
        oe_cbe_,
        out_cbe_,
        oe_frame_,
        out_frame_,
        oe_irdy_,
        out_irdy_,
        oe_devsel_,
        out_devsel_,
        oe_trdy_,
        out_trdy_,
        oe_stop_,
        out_stop_,
 //serr_ and perr_
        oe_serr_,
        out_serr_,
        oe_perr_,
        out_perr_,
        oe_par_,
        out_par,
        out_gnt_,
        in_req_,
        in_ad,
        in_cbe_,
        in_frame_,
        in_irdy_,
        in_devsel_,
        in_trdy_,
        in_stop_,
        in_perr_,
        in_serr_,
        in_par,
        ext_int,
        gpio_out,
        gpio_oe_,
        gpio_in,

        gpio_set2_out,
        gpio_set2_oe_,
        gpio_set2_in,

        // Audio
        out_i2so_data0,
        out_i2so_data1,
        out_i2so_data2,
        out_i2so_data3,
        out_i2so_data4,
        i2so_ch10_en,
        out_i2so_lrck,
        out_i2so_bick,
		out_i2si_lrck,
		out_i2si_bick,
        in_i2si_data,
  		slave_i2si_bick,
  		slave_i2si_lrck,
		i2si_mode_sel,
   //     in_i2si_lrck,
   //     in_i2si_bick,
        in_i2so_mclk,
        iadc_mclk,
		iadc_smtsen,
		iadc_smtsmode,
		iadc_smts_err,
		iadc_smts_end,
		iadc_adcclkp1,
		iadc_adcclkp2,
		iadc_tst_mod,

        //spdif
        out_spdif,
        in_spdif,
        // South Bridge
        ir_rx,

//        scb_en,
        scb_scl_out,
        scb_scl_oe,
        scb_sda_out,
        scb_sda_oe,
        scb_scl_in,
        scb_sda_in,
//UAAT
		uart_tx,
		uart_rx,

        // USB
        p1_ls_mode,
        p1_txd  ,
        p1_txd_oej,
        p1_txd_se0,
        p2_ls_mode,
        p2_txd  ,
        p2_txd_oej,
        p2_txd_se0,
  //      p3_ls_mode,
  //      p3_txd  ,
  //      p3_txd_oej,
  //      p3_txd_se0,
  //      p4_ls_mode,
  //      p4_txd  ,
  //      p4_txd_oej,
  //      p4_txd_se0,
        usb_pon_,

        i_p1_rxd,
        i_p1_rxd_se0,
        i_p2_rxd,
        i_p2_rxd_se0,
  //      i_p3_rxd,
  //      i_p3_rxd_se0,
  //      i_p4_rxd,
  //      i_p4_rxd_se0,
        usb_overcur_,

        // Display Engine
//        pclk_oe_,
        vdata_oe_,
        vdata_out,
        h_sync_out_,
        h_sync_oe_,
//        in_h_sync_,
        v_sync_out_,
        v_sync_oe_,
//        in_v_sync_,

// TV Encoder intf
		tvenc_vdi4vdac,
		tvenc_vdo4vdac,
		tvenc_idump,
		tvenc_iext,
		tvenc_iext1,		// 470 Ohm connects for the full scale amplitudes
		tvenc_iout1,
		tvenc_iout2,
		tvenc_iout3,
		tvenc_iout4,
		tvenc_iout5,
		tvenc_iout6,

		tvenc_iref1,
//		tvenc_iref2,
		tvenc_gnda,
		tvenc_gnda1,		// ground for DAC
		tvenc_vsud,			// ground for DAC
		tvenc_vd33a,

//added for m3358 JFR030626
		tvenc_h_sync_,	//output to mux with ext TV encoder for DE slaver sync
		tvenc_v_sync_,
		tvenc_vdactest,

//VIDEO IN
		VIN_PIXEL_DATA	,
		VIN_PIXEL_CLK	,
		VIN_H_SYNCJ	    ,
		VIN_V_SYNCJ	    ,

        // IDE Interface
        atadiow_out_,
        atadior_out_,
        atacs0_out_,
        atacs1_out_,
        atadd_out,
        atadd_oe_,
        atada_out,
  	    atareset_out_,
  	    atareset_oe_,
        atadmack_out_,

        ataiordy_in,
        ataintrq_in,
        atadd_in,
        atadmarq_in,
// System Interrupt
        x_err_int,
        x_ext_int,

// CPU Interface
        // MIPS bus interface
        sysad_out,
        sysad_in,
        syscmd_out,
        syscmd_in,
        sysout_oe,
        sys_int_,
        sys_nmi_,
        pvalid_,
        preq_,
        pmaster_,
        eok_,
        evalid_,
        ereq_,

        // JTAG interface
        j_tdi,
        j_tms,
        j_tclk,
        j_rst_,
        j_tdo,

		// CPU BIST intf
		cpu_bist_mode,
		cpu_scan_test_mode,		//	The scan test mode for CPU, this will be active in CPU_BIST and CHIP_SCAN mode
		cpu_bist_error_vec,
		cpu_bist_finish,

		//cpu test intf
//		cpu_testin,
//		cpu_trigcond,
//		cpu_trig_en,
//		cpu_testout,
//		cpu_trig_signal,

// Clock signals
        sb_clk,
        usb_clk,
        cpu_clk,                // pipeline clock   (CPU core)
        mem_clk,                // transfer clock   (Memory Controller, IO, ROM)
        pci_clk,
        cpu_clk_src,
        dsp_clk,
        spdif_clk,
        mbus_clk,
		ata_clk,
		servo_clk,				// servo clock
        pad_boot_config,

		inter_only,				// Interlace output
		dvi_rwbuf_sel,			// DVI buffer read/write

		cvbs2x_clk,				// 54MHZ clock
		cvbs_clk,				// 27MHz clock
		cav_clk,				// 54 or 27 MHZ clock
		dvi_buf0_clk,			// CAV_CLK or VIDEO_CLK for buffer 0
		dvi_buf1_clk,			// CAV_CLK or VIDEO_CLK for buffer 1
		video_clk,				// VIDEO clock output
		vsb_clk,				// VSB clock 80M

		tv_f108m_clk,			// tv_encoder 108MHz clock
		tv_cvbs2x_clk,			// tv_encoder 54MHZ clock
		tv_cvbs_clk,			// tv_encoder 27MHz clock
		tv_cav2x_clk,			// tv_encoder 108 or 54 MHZ clock
		tv_cav_clk,				// tv_encoder 54 or 27 MHZ clock

//	PLL control
		cpu_clk_pll_m_value,	// new cpu_clk_pll m parameter
		cpu_clk_pll_m_tri,      // cpu_clk_pll m modification trigger
		cpu_clk_pll_m_ack,      // cpu_clk_pll m modification ack

		mem_clk_pll_m_value,    // new mem_clk_pll m parameter
		mem_clk_pll_m_tri,      // mem_clk_pll m modification trigger
		mem_clk_pll_m_ack,      // mem_clk_pll m modification ack

//		pci_fs_value,			// new pci frequency select parameter
//		pci_fs_tri,             // pci frequency select modification trigger
//		pci_fs_ack,             // pci frequency select modification ack

		work_mode_value,		// new work_mode
		work_mode_tri,          // work_mode modification trigger
		work_mode_ack,          // work_mode modification ack


//	clock frequency select, clock misc control
		ide_fs,					// ide clock frequency select
		dsp_fs,					// dsp clock frequency select
		dsp_div_src_sel,		// dsp source clock select
		pci_fs,					// pci clock frequency select
		ve_fs,					// ve clock frequency select
		ve_div_src_sel,			// ve source clock select
//		pci_clk_out_sel,
		svga_mode_en,			// SVGA mode output enable
		tv_mode,				// video core output pixel for tv mode
		out_mode_etv_sel,		// output pixel clock select, 0 -> 54 MHz, 1 -> 27 MHz
//		tv_enc_clk_sel,			// TV encoder clock setting

        //sdram delay select
        mem_dly_sel,
        mem_rd_dly_sel,
		test_dly_chain_sel,		// TEST_DLY_CHAIN delay select
		test_dly_chain_flag,	// TEST_DLY_CHAIN flag output

//	external interface control
		audio_pll_fout_sel,		// Audio PLL FOUT frequency select
		i2s_mclk_sel,			// I2S MCLK source select
		i2s_mclk_out_en_,		// I2S MCLK output enable
		spdif_clk_sel,			// S/PDIF clock select
		i2so_data2_1_gpio_en,	//I2SO DATA[2:1] GPIO enable
		i2so_data3_gpio_en,		// I2SO DATA3 GPIO enable
		xsflag_gpio_en,			// Xsflag[1:0] share with gpio[25:24] enable
		spdif_gpio_en,			// Spdif share with gpio[21] en
		uart_gpio_en,			// Uart_tx share with gpio[24] en
		hv_sync_gpio_en,		// H_sync_, v_sync_ share with gpio[11:10]
		hv_sync_en,				// h_sync_, v_sync_ enable on the xsflag[1:0] pins
		i2si_clk_en,			// I2S input data enable on the gpio[7] pin
		scb_en,					// SCB interface enable on the gpio[6:5] pins
		servo_gpio_en,			// servo_gpio[2:0] enable on the gpio[2:0] pins
		ext_ide_device_en,		// external IDE device enable
//		pci_gntb_en,			// PCI GNTB# enable
		vdata_en,				// video data enable
		sync_source_sel,		// sync signal source select, 0 -> TVENC, 1 -> DE.
		video_in_intf_en,		// video in interface enable
//		pci_mode_ide_en,		// pci_mode ide interface enable
		sdram_cs1j_en,			// sdram cs1# enable
		ata_share_mode_2_en,	// atadmack_ and atada[0] share with rom_oe_ and rom_we_
		ata_ad_src_sel,			// rom_addr[18:17] bond selection
		ide_active,				// ide interface is active in the pci mode
		standby_mode_en,		// system standby mode

//PAD driver capability
		gpio_pad_driv	   	,	// gpio pad driver capability
		rom_da_pad_driv    	,   // rom data/address driver capability
		sdram_data_pad_driv	,   // sdram data driver capability
		sdram_ca_pad_driv	,   // sdram command/address driver capability
//----------------- SERVO Interface begin -------------------------
		//SERVO INTERFACE
//		dmckg_mpgclk		,		// MEM_CLK
//		sv_top_clkdiv		,
		sv_pmata_somd		,
		sv_pmsvd_trcclk		,
		sv_pmsvd_trfclk		,
		sv_sv_tslrf			,
		sv_sv_tplck			,
//		sv_sv_dev_true     	,
		sv_tst_svoio_i			,
		sv_svo_svoio_o			,
		sv_svo_svoio_oej		,
		sv_tst_addata			,
		sv_reg_rctst_2       	,
//		sv_top_sv_mppdn			,
//		sv_dmckg_sysclk			,
		sv_mckg_dspclk			,
//		sv_sycg_svoata 			,
//		sv_sv_reg_c2ftst		,
//		sv_pmdm_ideslave		,
		sv_pmsvd_tplck          ,
		sv_pmsvd_tslrf          ,
		sv_sv_tst_pin			,
//		sv_sv_reg_tstoen        ,
		sv_pmsvd_c2ftstin		,
		sv_sycg_somd			,
		// Servo analog
//		sv_sycg_updbgout 		,		// NB controller output
//		sv_cpu_gpioin			,
//		sv_sycg_burnin			,
		sv_ex_up_in				,
		sv_ex_up_out			,
		sv_ex_up_oe_			,
		// Servo Atapi inf
		sv_sv_hi_hdrq_o        ,
		sv_sv_hi_hdrq_oej      ,
		sv_sv_hi_hcs16j_o      ,
		sv_sv_hi_hcs16j_oej    ,
		sv_sv_hi_hint_o        ,
		sv_sv_hi_hint_oej      ,
		sv_sv_hi_hpdiagj_o     ,
		sv_sv_hi_hpdiagj_oej   ,
		sv_sv_hi_hdaspj_o      ,
		sv_sv_hi_hdaspj_oej    ,
		sv_sv_hi_hiordy_o      ,
		sv_sv_hi_hiordy_oej    ,
		sv_sv_hi_hd_o			,
		sv_sv_hi_hd_oej        ,
		sv_pmata_hrstj         ,
		sv_pmata_hcs1j         ,
		sv_pmata_hcs3j         ,
		sv_pmata_hiorj         ,
		sv_pmata_hiowj         ,
		sv_pmata_hdackj        ,
		sv_pmata_hpdiagj       ,
		sv_pmata_hdaspj        ,
		sv_pmata_ha			,
		sv_pmata_hd			,
		// Servo Test
		sv_sv_svo_sflag		,
		sv_sv_svo_soma			,
		sv_sv_svo_somd			,
		sv_sv_svo_somd_oej     ,
		sv_sv_svo_somdh_oej    ,
//		sv_sv_reg_dvdram_toe   ,
//		sv_sv_reg_dfctsel      ,
//		sv_sv_reg_trfsel 		,
//		sv_sv_reg_flgtoen 		,
//		sv_sv_reg_gpioten		,
//		sv_sv_reg_13e_7		,
//		sv_sv_reg_13e_6		,
//		sv_reg_trg_plcken		,
//		sv_reg_sv_tplck_oej	,
		//	Servo analog ANA_DTSV

		sv_pad_clkref			,
		sv_xadcin              ,
		sv_xadcip              ,
		sv_xatton              ,
		sv_xattop              ,
		sv_xbiasr              ,
		sv_xcdld               ,
		sv_xcdpd               ,
		sv_xcdrf               ,
		sv_xcd_a               ,
		sv_xcd_b               ,
		sv_xcd_c               ,
		sv_xcd_d               ,
		sv_xcd_e               ,
		sv_xcd_f               ,
		sv_xcelpfo             ,
		sv_xdpd_a              ,
		sv_xdpd_b              ,
		sv_xdpd_c              ,
		sv_xdpd_d              ,
		sv_xdvdld              ,
		sv_xdvdpd              ,
		sv_xdvdrfn             ,
		sv_xdvdrfp             ,
		sv_xdvd_a              ,
		sv_xdvd_b              ,
		sv_xdvd_c              ,
		sv_xdvd_d              ,
		sv_xfelpfo             ,
		sv_xfocus              ,
		sv_xgmbiasr            ,
		sv_xlpfon              ,
		sv_xlpfop              ,
		sv_xpdaux1             ,
		sv_xpdaux2             ,
		sv_xsblpfo             ,
		sv_xsfgn               ,
		sv_xsfgp               ,

		sv_pata_sflag			,
		sv_pmata_sflag			,
		sv_pmata_sflag_oej		,

		sv_xsflag              ,
		sv_xslegn              ,
		sv_xslegp              ,
		sv_xspindle            ,
		sv_xtelp               ,
		sv_xtelpfo             ,
		sv_xtestda             ,
		sv_xtexo               ,
		sv_xtrack              ,
		sv_xtray               ,
		sv_xvgain              ,
		sv_xvgaip              ,
		sv_xvref15             ,
		sv_xvref21             ,
		//Servo power
		sv_vd33d            	,
		sv_vd18d            	,
		sv_vdd3mix1				,
		sv_vdd3mix2				,
		sv_avdd_ad          	,
		sv_avdd_att         	,
		sv_avdd_da          	,
		sv_avdd_dpd         	,
		sv_avdd_lpf         	,
		sv_avdd_rad         	,
		sv_avdd_ref         	,
		sv_avdd_svo         	,
		sv_avdd_vga         	,
		sv_avss_ad          	,
		sv_avss_att         	,
		sv_avss_da          	,
		sv_avss_dpd         	,
		sv_avss_ga          	,
		sv_avss_gd          	,
		sv_avss_lpf         	,
		sv_avss_rad         	,
		sv_avss_ref         	,
		sv_avss_svo         	,
		sv_avss_vga         	,
		sv_azec_gnd         	,
		sv_azec_vdd         	,
		sv_dv18_rcka        	,
		sv_dv18_rckd        	,
		sv_dvdd             	,
		sv_dvss             	,
		sv_dvss_rcka        	,
		sv_dvss_rckd        	,
		sv_fad_gnd          	,
		sv_fad_vdd          	,
		sv_gndd             	,

//------------------- Servo interface end-------------------

//	SERVO control
		sv_tst_pin_en,			// SERVO test pin enable
		sv_ide_mode_part0_en,	// ide mode SERVO test pin part 0 enable
		sv_ide_mode_part1_en,	// ide mode SERVO test pin part 1 enable
		sv_ide_mode_part2_en,	// ide mode SERVO test pin part 2 enable
		sv_ide_mode_part3_en,	// ide mode SERVO test pin part 3 enable
		sv_c2ftstin_en,			// SERVO c2 flag enable
//		sv_sycg_updbgout,		// SERVO cpu interface debug enable
		sv_trg_plcken,			// SERVO tslrf and tplck enable
		sv_pmsvd_trcclk_trfclk_en,	// SERVO pmsvd_trcclk, pmsvd_trfclk enable

//======================  MS/SD  interface =======================
		ms_ins			,
		ms_serin_in   	,
		sd_data_in   	,

		sd_mmc_cmd_in 	,
		ms_clk        	,
		ms_bsst       	,
		ms_pw_ctrl    	,
		ms_serout     	,
		msde          	,
		sd_data_oej  	,

		sd_mmc_cmd_oej	,
		sd_data_out  	,

		sd_mmc_cmd_out	,
		sd_mmc_clk    	,
		test_h        	,
		test_done     	,
		fail_h        	,
		det_pin       	,
		wr_prct_pin		,

		sd_ip_enable 	,
        ms_ip_enable 	,
//======================  MS/SD  interface end =======================

//=====GI Interface(from broadon)
//system
	    alt_boot		,
//		test_ena	    ,
		gc_rst_	        ,
		pin_rst_		,
		gi_chip_rst_	,
		gi_clk			,
		gi_clk_lock	    ,
//--serial pro
		i2c_clk	        ,
		i2c_din	        ,
		i2c_dout	    ,
		i2c_doe	        ,
//--dsic inter
		di_in		    ,
		di_out		    ,
		di_oe		    ,
		di_brk_in	    ,
		di_brk_out	    ,
		di_brk_oe	    ,
		di_dir		    ,
		di_hstrb	    ,
		di_dstrb	    ,
		di_err		    ,
		di_cover	    ,
		di_stb			,

		ais_clk	        ,
		ais_lr		    ,
		ais_d		    ,

//ADC
		iadc_v1p2u ,
		iadc_vs    ,
		iadc_vd    ,
		iadc_dvdd,			//	ADC digital power
		iadc_dvss,			//	ADC digital ground
		iadc_vin_l ,
		iadc_vin_r ,


        // working mode select
//        cpu_mode,
//        cpu_mode_,
//        pci_mode		,
        ide_mode		,
		tvenc_test_mode	,
		servo_only_mode ,
		servo_test_mode ,
		servo_sram_mode ,
        test_mode		,
        bist_test_mode	,

        dsp_bist_mode	,

//DSP bist interface
		dsp_bist_finish,
		dsp_bist_err_vec,

//VIDEO bist interface
		video_bist_tri		,
		video_bist_finish	,
		video_bist_vec		,

        //to cpu
        pci_mips_fs,
        cpu_endian_mode,
        core_warmrst_,
        core_coldrst_,
        cpu_probe_en,
        flash_mode,
	//test port
`ifdef  SCAN_EN
	DFT_TEST_SE,
	DFT_TEST_SI,
	DFT_TEST_SO,
`endif

	//connect to chipset
        sw_rst_pulse,
        chipset_rst_
        );

parameter       udly = 1;
parameter       ram_dly = 1,
                pci_dly = 1,
                oth_dly = 1;
//============define the input/output/inout signals=============
//---------------chip set bus
// SDRAM & FLASH
output  [11:0]  ram_addr;
output  [1:0]   ram_ba;
output  [31:0]  ram_dq_out;
input   [31:0]  ram_dq_in;
output  [31:0]  ram_dq_oe_;
output      ram_cas_;
output      ram_we_;
output      ram_ras_;
output      ram_cke;
output  [3:0]   ram_dqm;
output  [1:0]   ram_cs_;

// EEPROM
output  [21:0]  eeprom_addr;
input   [15:0]   eeprom_rdata;
//output          eeprom_ce_;
output          eeprom_oe_;
output          eeprom_we_;
output          rom_data_oe_;
output	[21:0]	rom_addr_oe;
output			eeprom_ale;
//output			eeprom_ale_oe_;
output  [7:0]   eeprom_wdata;
output          rom_en_;
// LED
//output        [7:0]   ledpin;

// PCI
output          oe_ad_;
output  [31:0]  out_ad;
output          oe_cbe_;
output			out_rst_;
output  [3:0]   out_cbe_;
output          oe_frame_;
output          out_frame_;
output          oe_irdy_;
output          out_irdy_;
output          oe_devsel_;
output          out_devsel_;
output          oe_trdy_;
output          out_trdy_;
output          oe_stop_;
output          out_stop_;
output          oe_par_;
output          out_par;
output          out_serr_;
output          oe_serr_ ;
output          oe_perr_ ;
output          out_perr_;

output  [1:0]   out_gnt_;
input   [1:0]   in_req_;

input   [31:0]  in_ad;
input   [3:0]   in_cbe_;

input           in_frame_;
input           in_irdy_;
input           in_devsel_;
input           in_trdy_;
input           in_stop_;
input           in_par;
input           in_serr_;
input           in_perr_;

input           ext_int;
output  [31:0]  gpio_out;
output  [31:0]  gpio_oe_;
input   [31:0]  gpio_in;

output  [31:0]  gpio_set2_out;
output  [31:0]  gpio_set2_oe_;
input   [31:0]  gpio_set2_in;

// Audio
output          out_i2so_data0,
		out_i2so_data1,
		out_i2so_data2,
		out_i2so_data3,
		out_i2so_data4;
output			i2so_ch10_en;

output          out_i2so_lrck;
output          out_i2so_bick;
output			out_i2si_lrck,
				out_i2si_bick;

input           in_i2si_data;
input   		slave_i2si_bick;
input   		slave_i2si_lrck;
output			i2si_mode_sel;
//input           in_i2si_lrck;
//input           in_i2si_bick;
input           in_i2so_mclk;
input	        iadc_mclk;					// IADC master clock
input	[24:23]	iadc_smtsen;				// ADC SRAM Test Enable
input	[2:0]	iadc_smtsmode;				// ADC SRAM Test Mode
output	[24:23]	iadc_smts_err;              // ADC SRAM Test Error Flag
output	[24:23]	iadc_smts_end;              // ADC SRAM Self Test End
output			iadc_adcclkp1;				// ADC digital test pin 1
output			iadc_adcclkp2;              // ADC digital test pin 2
input	[3:0]	iadc_tst_mod;               // ADC Internal SDM Test Selection

//		ac97_i2s_sel;
//spdif
output          out_spdif;
//output          oe_spdif;
input           in_spdif;
// South Bridge
input           ir_rx;
//output          scb_en;
output          scb_scl_out;
output          scb_scl_oe;
output          scb_sda_out;
output          scb_sda_oe;
input           scb_scl_in;
input           scb_sda_in;
// UART
output			uart_tx;

input			uart_rx;

// USB
output          p1_ls_mode      ;
output          p1_txd          ;
output          p1_txd_oej      ;
output          p1_txd_se0      ;
output          p2_ls_mode      ;
output          p2_txd          ;
output          p2_txd_oej      ;
output          p2_txd_se0      ;
//add two ports p3,p4 by yuky, 2002-06-14
//output          p3_ls_mode      ;
//output          p3_txd          ;
//output          p3_txd_oej      ;
//output          p3_txd_se0      ;
//output          p4_ls_mode      ;
//output          p4_txd          ;
//output          p4_txd_oej      ;
//output          p4_txd_se0      ;
output          usb_pon_;
input           i_p1_rxd        ;
input           i_p1_rxd_se0;
input           i_p2_rxd        ;
input           i_p2_rxd_se0;
//add two ports p3,p4 by yuky, 2002-06-14
//input           i_p3_rxd        ;
//input           i_p3_rxd_se0;
//input           i_p4_rxd        ;
//input           i_p4_rxd_se0;
input           usb_clk;
input           usb_overcur_;

// Display Engine
//output  pclk_oe_;
output  vdata_oe_;
output  h_sync_out_;
output  h_sync_oe_;
output  v_sync_out_;
output  v_sync_oe_;
//input   in_h_sync_;
//input   in_v_sync_;
output  [7:0]   vdata_out;

//TV encoder intf
input	[11:0]	tvenc_vdi4vdac;	// input, for debug
output	[11:0]	tvenc_vdo4vdac;	// output, for debug
output			tvenc_idump;	// output, for external load resistance
output			tvenc_iext;		// inout, for external load resistance	!!! bi-direction
output			tvenc_iext1;	// 470 Ohm connects for the full scale amplitudes
output			tvenc_iout1;	// output, analog
output			tvenc_iout2;	// output, analog
output			tvenc_iout3;	// output, analog
output			tvenc_iout4;	// output, analog
output			tvenc_iout5;	// output, analog
output			tvenc_iout6;	// output, analog

output			tvenc_iref1;	// added for m3358JFR 030626
//output			tvenc_iref2;	// IREF2 output

output			tvenc_h_sync_;
output			tvenc_v_sync_;
input			tvenc_vdactest;
input			tvenc_gnda;
input			tvenc_gnda1;	// ground for DAC
input			tvenc_vsud;		// ground for DAC
input			tvenc_vd33a;

input	[7:0]	VIN_PIXEL_DATA	;
input		VIN_PIXEL_CLK	;
input		VIN_H_SYNCJ	    ;
input		VIN_V_SYNCJ	    ;


output  	x_err_int;
output  	x_ext_int;
// MIPS bus interface
output  [31:0]  sysad_out;
input   [31:0]  sysad_in;
output  [4:0]   syscmd_out;
input   [4:0]   syscmd_in;
output          sysout_oe;
input   [4:0]   sys_int_;
input           sys_nmi_;
output          pvalid_;
output  preq_;
output  pmaster_;
input   eok_;
input   evalid_;
input   ereq_;

// JTAG interface
input   j_tdi;
input   j_tms;
input   j_tclk;
input   j_rst_;
output  j_tdo;

// CPU Bist Intf
input			cpu_bist_mode;
input			cpu_scan_test_mode;		//	The scan test mode for CPU, this will be active in CPU_BIST and CHIP_SCAN mode
output	[5:0] 	cpu_bist_error_vec;//JFR030806
output			cpu_bist_finish;

//input   [4:0]	cpu_testin;
//input   [9:0]   cpu_trigcond;
//input           cpu_trig_en;
//output  [39:0]  cpu_testout;
//output          cpu_trig_signal;

input           sb_clk;
input           cpu_clk;                        // pipeline clock   (CPU core)
input           mem_clk;                        // transfer clock   (Memory Controller, IO, ROM)
input           pci_clk;
input           mbus_clk;
input           cpu_clk_src;
input           dsp_clk;
input           spdif_clk;
input			ata_clk;
input			servo_clk;				// servo clock
input   [31:0]  pad_boot_config;

output			inter_only;				// Interlace output
output			dvi_rwbuf_sel;			// DVI buffer read/write

input			cvbs2x_clk;				// 54MHZ clock
input			cvbs_clk;				// 27MHz clock
input			cav_clk;				// 54 or 27 MHZ clock
input			dvi_buf0_clk;			// CAV_CLK or VIDEO_CLK for buffer 0
input			dvi_buf1_clk;			// CAV_CLK or VIDEO_CLK for buffer 1
input			video_clk;				// VIDEO clock output
input			vsb_clk;				// VSB clokc 80M

input			tv_f108m_clk;			// tv_encoder 108MHz clock
input			tv_cvbs2x_clk;			// tv_encoder 54MHZ clock
input			tv_cvbs_clk;			// tv_encoder 27MHz clock
input			tv_cav2x_clk;			// tv_encoder 108 or 54 MHZ clock
input			tv_cav_clk;				// tv_encoder 54 or 27 MHZ clock

//	PLL control
output	[5:0]	cpu_clk_pll_m_value;// new cpu_clk_pll m parameter
output			cpu_clk_pll_m_tri;	// cpu_clk_pll m modification trigger
input			cpu_clk_pll_m_ack;	// cpu_clk_pll m modification ack

output	[5:0]	mem_clk_pll_m_value;// new mem_clk_pll m parameter
output			mem_clk_pll_m_tri;	// mem_clk_pll m modification trigger
input			mem_clk_pll_m_ack;	// mem_clk_pll m modification ack

//output	[1:0]	pci_fs_value;		// new pci_fs parameter
//output			pci_fs_tri;			// pci_fs modification trigger
//input			pci_fs_ack;			// pci_fs modification ack

output	[2:0]	work_mode_value;	// new work_mode
output			work_mode_tri;      // work_mode modification trigger
input			work_mode_ack;      // work_mode modification ack

//	clock frequency select, clock misc control
output	[1:0]   ide_fs;				//ide clock frequency select
output	[1:0]	dsp_fs;				// dsp clock frequency select
output			dsp_div_src_sel;	// dsp source clock select
output	[1:0]	pci_fs;				// pci clock frequency select
output	[1:0]	ve_fs;				// ve clock frequency select
output			ve_div_src_sel;		// ve source clock select
//output	[1:0]	pci_clk_out_sel;	// JFR 030830
output			svga_mode_en;		// SVGA mode output enable
output			tv_mode;			// video core output pixel for tv mode
output			out_mode_etv_sel;	// output pixel clock select, 0 -> 54 MHz, 1 -> 27 MHz
//output	[11:8]	tv_enc_clk_sel;		// TV encoder clock setting

output          sw_rst_pulse;
input           core_warmrst_;
input           chipset_rst_;

// IDE Interface
output  atadiow_out_;
output  atadior_out_;
output  atacs0_out_;
output  atacs1_out_;
output[15:0]atadd_out;
output[3:0] atadd_oe_;
output[2:0] atada_out;
output  atareset_out_;
output	atareset_oe_;
output  atadmack_out_;

input   ataiordy_in;
input   ataintrq_in;
input[15:0]     atadd_in;
input   atadmarq_in;

output  [4:0]   mem_dly_sel;
output	[4:0]	mem_rd_dly_sel;		//	SDRAM read clock delay control
output	[5:0]	test_dly_chain_sel;	// TEST_DLY_CHAIN delay select
input	[1:0]	test_dly_chain_flag;// TEST_DLY_CHAIN flag output

//	external interface control
output			audio_pll_fout_sel;	// Audio PLL FOUT frequency select
output	[1:0]	i2s_mclk_sel;		// I2S MCLK source select
output			i2s_mclk_out_en_;	// I2S MCLK output enable
output	[1:0]	spdif_clk_sel;		// S/PDIF clock select
output			i2so_data2_1_gpio_en;//I2SO DATA[2:1] GPIO enable
output			i2so_data3_gpio_en;	// I2SO DATA3 GPIO enable
output			xsflag_gpio_en	;	// Xsflag[1:0] share with gpio[25:24] enable
output			spdif_gpio_en	;	// Spdif share with gpio[21] en
output			uart_gpio_en	;	// Uart_tx share with gpio[24] en
output			hv_sync_gpio_en;	// H_sync_, v_sync_ share with gpio[11:10]
output			hv_sync_en;			// h_sync_, v_sync_ enable on the xsflag[1:0] pins
output			i2si_clk_en;		// I2S input data enable on the gpio[7] pin
output			scb_en;				// SCB interface enable on the gpio[6:5] pins
output			servo_gpio_en;		// servo_gpio[2:0] enable on the gpio[2:0] pins
output			ext_ide_device_en;	// external IDE device enable
//output			pci_gntb_en;		// PCI GNTB# enable
output			vdata_en;			// video data enable
output			sync_source_sel;	// sync signal source select, 0 -> TVENC, 1 -> DE.
output			video_in_intf_en;	// video in interface enable
//output			pci_mode_ide_en;	// pci_mode ide interface enable
output			sdram_cs1j_en;		// sdram cs1# enable
output			ata_share_mode_2_en;// atadmack_ and atada[0] share with rom_oe_ and rom_we_
output			ata_ad_src_sel;		// rom_addr[18:17] bond selection

output			ide_active;			// ide interface is active in the pci mode
output			standby_mode_en;	// system standby mode

output	[1:0]	gpio_pad_driv	   	,  	// gpio pad driver capability
				rom_da_pad_driv    	,   // rom data/address driver capability
				sdram_data_pad_driv	,   // sdram data driver capability
				sdram_ca_pad_driv	;   // sdram command/address driver capability

// Servo interface
//input			sv_top_clkdiv			;	//// sERVO iNTERNAL CLOCK DIVISE
input	[23:0]	sv_pmata_somd  		;	////
input			sv_pmsvd_trcclk		;
input			sv_pmsvd_trfclk		;
output			sv_sv_tslrf			;
output			sv_sv_tplck			;
//output			sv_sv_dev_true     	;
output	[2:0]	sv_svo_svoio_o 		;
output	[2:0]	sv_svo_svoio_oej	;
output			sv_reg_rctst_2       	;
input	[2:0]	sv_tst_svoio_i			;
input	[5:0]	sv_tst_addata 			;
//input			sv_top_sv_mppdn		;
//input			sv_dmckg_sysclk        ;	//// Servo System clock decided by TOP_CLKDIV
input			sv_mckg_dspclk         ;
//input			sv_sycg_svoata 		;
//output			sv_sv_reg_c2ftst       ;
//input			sv_pmdm_ideslave       ;
input			sv_pmsvd_tplck         ;
input			sv_pmsvd_tslrf         ;
output	[44:0]	sv_sv_tst_pin    		;
//output			sv_sv_reg_tstoen       ;
input	[1:0]	sv_pmsvd_c2ftstin 		;
input	[23:0]	sv_sycg_somd     		;
//				analog
//input	[3:0]	sv_cpu_gpioin			;
//input	[1:0]	sv_sycg_burnin			;
//		External uP interface
input	[22:0]	sv_ex_up_in			;
output	[22:0]	sv_ex_up_out		;
output	[22:0]	sv_ex_up_oe_		;
//			Atapi inf
output			sv_sv_hi_hdrq_o        ;
output			sv_sv_hi_hdrq_oej      ;
output			sv_sv_hi_hcs16j_o      ;
output			sv_sv_hi_hcs16j_oej    ;
output			sv_sv_hi_hint_o        ;
output			sv_sv_hi_hint_oej      ;
output			sv_sv_hi_hpdiagj_o     ;
output			sv_sv_hi_hpdiagj_oej   ;
output			sv_sv_hi_hdaspj_o      ;
output			sv_sv_hi_hdaspj_oej    ;
output			sv_sv_hi_hiordy_o      ;
output			sv_sv_hi_hiordy_oej    ;
output	[15:0] 	sv_sv_hi_hd_o   		;
output			sv_sv_hi_hd_oej        ;
input			sv_pmata_hrstj         ;
input			sv_pmata_hcs1j         ;
input			sv_pmata_hcs3j         ;
input			sv_pmata_hiorj         ;
input			sv_pmata_hiowj         ;
input			sv_pmata_hdackj        ;
input			sv_pmata_hpdiagj       ;
input			sv_pmata_hdaspj        ;
input	[2:0]	sv_pmata_ha           	;
input	[15:0]	sv_pmata_hd          	;

////// SERVO INTERFACE
output	[1:0]	sv_sv_svo_sflag   		;
output	[12:0]	sv_sv_svo_soma   		;
output	[23:0]	sv_sv_svo_somd   		;
output	[21:0]	sv_sv_svo_somd_oej     ;
output	[23:22]	sv_sv_svo_somdh_oej    ;
//output			sv_sv_reg_dvdram_toe   ;
//output			sv_sv_reg_dfctsel      ;
//output			sv_sv_reg_trfsel 		;
//output			sv_sv_reg_flgtoen 		;
//output	[3:0]	sv_sv_reg_gpioten		;
//output			sv_sv_reg_13e_7		;
//output			sv_sv_reg_13e_6		;
//output			sv_reg_trg_plcken		;
//output			sv_reg_sv_tplck_oej	;
//				ANA_DTSV

input			sv_pad_clkref			;
input			sv_xadcin              ;	// Differential input signal to ADC
input			sv_xadcip              ;	// Differential input signal to ADC
output			sv_xatton              ;	// Differential output from attenuator
output			sv_xattop              ;	// Differential output from attenuator
input			sv_xbiasr              ;	// Bi-direction pin for internal bias current external R=12K
output			sv_xcdld               ;	// CD laser power control output
input			sv_xcdpd               ;	// CD laser power control input from the monitor photo diode
input			sv_xcdrf               ;	// CD RF input from pick up head
input			sv_xcd_a               ;	// Photo detect signal input from pickup head for CD sub beam
input			sv_xcd_b               ;	// Photo detect signal input from pickup head for CD sub beam
input			sv_xcd_c               ;	// Photo detect signal input from pickup head for CD sub beam
input			sv_xcd_d               ;	// Photo detect signal input from pickup head for CD sub beam
input			sv_xcd_e               ;	// Photo detect signal input from pickup head for CD sub beam
input			sv_xcd_f               ;	// Photo detect signal input from pickup head for CD sub beam
output			sv_xcelpfo             ;	// Output for outside low pass filter of central error signal
input			sv_xdpd_a              ;	// Photo detect signal input from pickup head for DPD function
input			sv_xdpd_b              ;	// Photo detect signal input from pickup head for DPD function
input			sv_xdpd_c              ;	// Photo detect signal input from pickup head for DPD function
input			sv_xdpd_d              ;	// Photo detect signal input from pickup head for DPD function
output			sv_xdvdld              ;	// DVD laser power control output
input			sv_xdvdpd              ;	// DVD laser power control input from the monitor photo diode
input			sv_xdvdrfn             ;	// DVD RF differential input from pickup head
input			sv_xdvdrfp             ;	// DVD RF differential input from pickup head
input			sv_xdvd_a              ;	// Photo detect signal input from pickup head for DVD servo signal
input			sv_xdvd_b              ;	// Photo detect signal input from pickup head for DVD servo signal
input			sv_xdvd_c              ;	// Photo detect signal input from pickup head for DVD servo signal
input			sv_xdvd_d              ;	// Photo detect signal input from pickup head for DVD servo signal
output			sv_xfelpfo             ;	// Output for outside low pass filter of focus error signal
output			sv_xfocus              ;	// Focus actuator control signal output
input			sv_xgmbiasr            ;	// Bi-derection pin for Gm cell bias, external R=43K
output			sv_xlpfon              ;	// Differential output from low pass filter
output			sv_xlpfop              ;	// Differential output from low pass filter
input			sv_xpdaux1             ;	// Analog test pin 1
input			sv_xpdaux2             ;	// Analog test pin 2
output			sv_xsblpfo             ;	// Output for outside low pass filter of SBAD signal
input			sv_xsfgn               ;	// Spindle rotarion speed detection input pin
input			sv_xsfgp               ;	// Spindle rotarion speed detection input pin


output	[1:0]	sv_pata_sflag     		;
input	[1:0]	sv_pmata_sflag    		;
input	[1:0]	sv_pmata_sflag_oej		;

inout	[1:0]	sv_xsflag				;
output			sv_xslegn              ;	// Control signal output for sledge motor(-)
output			sv_xslegp              ;	// Control signal output for sledge motor(+)
output			sv_xspindle            ;	// PWM output for spindle motor control
input			sv_xtelp               ;	// Low pass filter of tracking center level for TEZC
output			sv_xtelpfo             ;	// Output for outside low passfilter of tracking error signal
output			sv_xtestda             ;	// Reserved DA output pin for testing
output			sv_xtexo               ;	// Tracking error output
output			sv_xtrack              ;	// Track actuator control signal output
output			sv_xtray               ;	// PWM output for tray motor control
input			sv_xvgain              ;	// Differential input signal to VGA
input			sv_xvgaip              ;	// Differential input signal to VGA
output			sv_xvref15             ;	// 1.5V reference voltage output for servo control output signals
output			sv_xvref21             ;	// 2.1V reference voltage output for input I/F from pick_up head

//				power
input			sv_vd33d               ;
input			sv_vd18d               ;
input			sv_vdd3mix1				;	// Digital Power +3.3V inside analog block
input			sv_vdd3mix2				;	// Digital Power +3.3V inside analog block
input			sv_avdd_ad             ;	// Analog Power +3.3V for Servo ADC part
input			sv_avdd_att            ;
input			sv_avdd_da             ;	// Analog Power +3.3V for servo DAC part
input			sv_avdd_dpd            ;	// Analog Power +3.3V for DPD part
input			sv_avdd_lpf            ;
input			sv_avdd_rad            ;	// Analog Power +3.3V for Read Channel ADC part
input			sv_avdd_ref            ;
input			sv_avdd_svo            ;	// Analog Power +3.3V for Servo Signal generation part
input			sv_avdd_vga            ;
input			sv_avss_ad             ;	// Analog Ground for Servo ADC part
input			sv_avss_att            ;
input			sv_avss_da             ;	// Analog Ground for servo DAC part
input			sv_avss_dpd            ;	// Analog Ground for DPD part
input			sv_avss_ga             ;	// Guard Ring Ground for Analog part
input			sv_avss_gd             ;	// Guard Ring Ground for Digital part
input			sv_avss_lpf            ;
input			sv_avss_rad            ;	// Analog Ground for Read Channel ADC part
input			sv_avss_ref            ;
input			sv_avss_svo            ;	// Analog Ground for Servo Signal generation part
input			sv_avss_vga            ;
input			sv_azec_gnd            ;
input			sv_azec_vdd            ;
input			sv_dv18_rcka           ;	// Digital Power +1.8V for Read Channel clock?
input			sv_dv18_rckd           ;	// Digital Power +1.8V for Read Channel clock?
input			sv_dvdd                ;
input			sv_dvss                ;
input			sv_dvss_rcka           ;	// Digital Ground for Read Channel clock?
input			sv_dvss_rckd           ;	// Digital Ground for Read Channel clock?
input			sv_fad_gnd             ;
input			sv_fad_vdd             ;
input			sv_gndd                ;

//	SERVO control
output			sv_tst_pin_en;			// SERVO test pin enable
output			sv_ide_mode_part0_en;	// ide mode SERVO test pin part 0 enable
output			sv_ide_mode_part1_en;	// ide mode SERVO test pin part 1 enable
output			sv_ide_mode_part2_en;	// ide mode SERVO test pin part 2 enable
output			sv_ide_mode_part3_en;	// ide mode SERVO test pin part 3 enable
output			sv_c2ftstin_en;			// SERVO c2 flag enable
//output		sv_sycg_updbgout;		// SERVO cpu interface debug enable
output			sv_trg_plcken;			// SERVO tslrf and tplck enable
output			sv_pmsvd_trcclk_trfclk_en;	// SERVO pmsvd_trcclk, pmsvd_trfclk enable

//======================  MS/SD  interface Define =======================
input			ms_ins				;
input			ms_serin_in   	    ;
input	[3:0]	sd_data_in   	    ;
//input			sd_data1_in   	    ;
//input			sd_data2_in   	    ;
//input			sd_data3_in   	    ;
input			sd_mmc_cmd_in 	    ;

output			ms_clk        	    ;
output			ms_bsst       	    ;
output			ms_pw_ctrl    	    ;
output			ms_serout     	    ;
output			msde          	    ;
output	[3:0]	sd_data_oej  	    ;
//output			sd_data1_oej  	    ;
//output			sd_data2_oej  	    ;
//output			sd_data3_oej  	    ;
output			sd_mmc_cmd_oej	    ;
output	[3:0]	sd_data_out  	    ;
//output			sd_data1_out  	    ;
//output			sd_data2_out  	    ;
//output			sd_data3_out  	    ;
output			sd_mmc_cmd_out	    ;
output			sd_mmc_clk    	    ;
input			test_h        	    ;
output			test_done     	    ;
output			fail_h        	    ;
input			det_pin       	    ;
input			wr_prct_pin   	    ;

output			sd_ip_enable		;
output			ms_ip_enable        ;
//===================  MS/SD interface define end  ==============
//system
input	    alt_boot		;
//input		test_ena
output		gc_rst_         ;
input		pin_rst_        ;
output		gi_chip_rst_    ;
input		gi_clk          ;
input		gi_clk_lock     ;
//--serial pro
output		i2c_clk         ;
input		i2c_din         ;
output		i2c_dout        ;
output		i2c_doe         ;
//--dsic inter
input[7:0]	di_in           ;
output[7:0]	di_out          ;
output[7:0]	di_oe           ;
input		di_brk_in       ;
output		di_brk_out      ;
output		di_brk_oe       ;
input		di_dir          ;
input		di_hstrb        ;
output		di_dstrb        ;
output		di_err          ;
output		di_cover        ;
input		di_stb          ;

input		ais_clk         ;
input		ais_lr          ;
output		ais_d           ;

//--------------ADC (5)
inout			iadc_v1p2u ;			// 1,	Reference voltage for ADC
input			iadc_vs    ;			// 1,   Analog ground
input			iadc_vd    ;       		// 1,   Analog power
inout			iadc_dvdd;				//	ADC digital power
inout			iadc_dvss;				//	ADC digital ground
input			iadc_vin_l ;       		//	ADC microphone input 1.
input			iadc_vin_r ;			//	ADC microphone input 1.

//input           pci_mode;
input           ide_mode;
input			tvenc_test_mode	;
input			servo_only_mode ;
input			servo_test_mode ;
input			servo_sram_mode ;
input           cpu_endian_mode;
input           test_mode;
input			bist_test_mode;

input			dsp_bist_mode	;

output			dsp_bist_finish	;
output	[2:0]	dsp_bist_err_vec;

input			video_bist_tri		;

output			video_bist_finish	;
output	[10:0]	video_bist_vec		;


input   [1:0]   pci_mips_fs;
//test port
`ifdef  SCAN_EN
input			DFT_TEST_SE;
input	[86:0]	DFT_TEST_SI;
output	[86:0]	DFT_TEST_SO;
`endif



//------------------CPU bus
input           core_coldrst_;
input           cpu_probe_en;
input	[1:0]	flash_mode;
//==============================================================

//===================define the signal types===================
//--------------interface to T2 chipset core
// Internal bus or signals
//wire            x_rd_last;
//wire            x_wr_last;
wire            x_ready;
wire    [31:0]  x_data_in;
//wire            x_bus_idle;
wire            x_bus_error;    // To t2core
wire            x_ack;

wire            u_request;
wire            u_write;
wire    [1:0]   u_wordsize;
wire    [31:0]  u_startpaddr;
wire    [31:0]  u_data_to_send;
wire    [3:0]   u_bytemask;

//wire            g_config_ep;
//wire            g_reset_;       // From t2core

wire            x_err_int;
wire            x_ext_int;
wire            x_cnt_int;
wire            wait_pc;

// SDRAM & FLASH
wire    [11:0]  ram_addr;
wire    [1:0]   ram_ba;
wire    [31:0]  ram_dq_out;
wire    [31:0]  ram_dq_in;
wire    [31:0]  ram_dq_oe_;
wire            ram_cas_,
                ram_we_,
                ram_ras_,
                ram_cke;
wire    [3:0]   ram_dqm;
wire    [1:0]   ram_cs_;

// EEPROM
wire    [21:0]  eeprom_addr;
wire    [15:0]   eeprom_rdata;
//wire            eeprom_ce_;
wire            eeprom_oe_;
wire            eeprom_we_;
wire            rom_data_oe_;
wire	[21:0] 	rom_addr_oe;
wire    [7:0]   eeprom_wdata;

// LED
wire    [7:0]   ledpin;

// PCI
wire            oe_ad_;
wire    [31:0]  out_ad;
wire            oe_cbe_;
wire    [3:0]   out_cbe_;
wire            oe_frame_,
                out_frame_;
wire            oe_irdy_;
wire            out_irdy_;
wire            oe_devsel_;
wire            out_devsel_;
wire            oe_trdy_;
wire            out_trdy_;
wire            oe_stop_;
wire            out_stop_;
wire            oe_par_;
wire            out_par;

wire    [1:0]   out_gnt_;
wire    [1:0]   in_req_;

wire    [31:0]  in_ad;
wire    [3:0]   in_cbe_;

wire            in_frame_;
wire            in_irdy_;
wire            in_devsel_;
wire            in_trdy_;
wire            in_stop_;
wire            in_par;

wire            ext_int;
wire    [31:0]  gpio_out,
                gpio_oe_;
wire    [31:0]  gpio_in;

wire    [31:0]  gpio_set2_out,
                gpio_set2_oe_;
wire    [31:0]  gpio_set2_in;

wire            ir_rx;

wire            scb_en;
wire            scb_scl_out,
                scb_scl_oe,
                scb_sda_out,
                scb_sda_oe;
wire            scb_scl_in,
                scb_sda_in;

wire			uart_tx,
				uart_rx;

wire            sb_clk;

// USB
wire            p1_ls_mode      ,
                p1_txd          ,
                p1_txd_oej      ,
                p1_txd_se0      ,
                p2_ls_mode      ,
                p2_txd          ,
                p2_txd_oej      ,
                p2_txd_se0      ;
wire            usb_pon_;
wire            i_p1_rxd        ,
                i_p1_rxd_se0,
                i_p2_rxd        ,
                i_p2_rxd_se0;
wire            usb_clk;
wire            usb_overcur_;

//------------interface to CPU BUS
wire	cpu_mode_	=	1;//for m3358 mips pattern debug JFR 030722

wire    [3:0]   syscmd_ext_out;
wire            sysout_oe;
wire            pvalid_;
wire            preq_;
wire            pmaster_;
wire            j_tdo;
wire            eok_;
wire            evalid_;
wire            ereq_;
wire            j_tdi, j_tms, j_tclk, j_rst_;
// code change by roger
// output from cpu
wire    [18:0]  core_test_out;
wire            soft_config_wr;
wire    [31:0]  ec_wb_data;
wire            test_so;
wire	[8:0]	CPU_TEST_SO;
`ifdef  SCAN_EN
wire	[86:0]	DFT_TEST_SO;
wire	[77:0]	CHIPSET_TEST_SO;
wire	[77:0]	CHIPSET_TEST_SI = {DFT_TEST_SI[86:47],DFT_TEST_SI[37:0]};
wire	[8:0]	CPU_TEST_SI	=	DFT_TEST_SI[46:38];	//CPU chain46-38

assign		DFT_TEST_SO = {	CHIPSET_TEST_SO[77],  	    //chain 86 EEPROM
							CHIPSET_TEST_SO[76],  	    //chain 85 SERVO
							CHIPSET_TEST_SO[75:46],		//chain 84-55 VIDEO
							CHIPSET_TEST_SO[45:39],		//chain 54-48 M3357_TVENC
							CHIPSET_TEST_SO[38],		//chain 47 TVENC_INTF
							CPU_TEST_SO[8:0],			//chain 46-38 CPU
							CHIPSET_TEST_SO[37:36],		//chain 37-35 IDE
							CHIPSET_TEST_SO[35:33],		//chain 34-33 VSB
							CHIPSET_TEST_SO[32:26],		//chain 32-26 AUDIO
							CHIPSET_TEST_SO[25:23],		//chain 25-23 VIDEO_IN
							CHIPSET_TEST_SO[22:16],		//chain 22-16 DSP
							CHIPSET_TEST_SO[15:12],		//chain 15-12 SOUTH
							CHIPSET_TEST_SO[11:6],		//chain 11-6 PBIU & USB
							CHIPSET_TEST_SO[5:0]};		//chain 5-0 NB & MEM

`endif
wire            tpc0;
wire            u_request_to_j;
wire    [1:0]   u_wordsize_to_j;
wire            acpt_debug_excp;
wire            pj_debug_mode;
// output from ejtag
wire            test_si;
wire            test_se;
//wire          j_tdo;
wire            tap_reset_;
wire            j_pr_rst_;
wire            j_per_rst_;
wire            j_ready;
wire    [31:0]  j_data_in;
//wire          j_pr_acc;
wire            asid_sup;
wire            j_prob_en;
wire            j_set_dev;
wire            j_jtag_brk;
wire    [3:0]   j_st;
wire            enter_trace_mode;

// output from pd_misc
//=====================================================

//====================invoke cpu  module===============
`ifdef NO_CPU
t2risc_intf T2_CPU (
                .u_request              (u_request              ),      //signal a request
                .u_write                (u_write                ),      //flag write request
                .u_wordsize             (u_wordsize             ),  //01=>single word(or less) 10=>double word 11=>quad word
                .u_bytemask             (u_bytemask             ),  //indicate which byte lane contains valid data
                .u_startpaddr           (u_startpaddr           ),  //the start address of a transaction
                .u_data_to_send         (u_data_to_send         ),  //data to send in a write request

                .x_ready                (x_ready                ),  //signal data ready
                .x_ack                  (x_ack                  ),  //indicate a request is finished normally
                .x_bus_error            (x_bus_error            ),  //bus error exception
                .x_data_in              (x_data_in              ),  //data loaded from memory
                .x_err_int              (x_err_int              ),
                .x_ext_int              (x_ext_int              ),

                .g_sclk                 (cpu_clk                ),
                .g_reset_               (core_warmrst_          )
                );
`else

wire    cpu_test_se;
//**************************************
//M3358 cpu JFR030801
//  `ifdef	NEW_CPU //M3358 cpu JFR030801
PRM3358CPU T2_CPU(
//output ports
		`ifdef  SCAN_EN
			.TEST_SO            (CPU_TEST_SO[8:0]		),
		`endif
            .J_TDO				(j_tdo                  ),
            .STATUS_RP			(STATUS_RP              ),
            .STATUS_SUSPEND		(STATUS_SUSPEND         ),
            .STATUS_STANDBY		(                       ),
            .STATUS_HIBERNATE	(                       ),
            .STATUS_EXL			(STATUS_EXL             ),
            .STATUS_ERL			(STATUS_ERL             ),
            .U_REQUEST_TO_O		(u_request              ),
            .U_WRITE			(u_write                ),
            .U_WORDSIZE			(u_wordsize             ),
            .U_BYTEMASK			(u_bytemask             ),
            .U_STARTPADDR		(u_startpaddr           ),
            .U_DATA_TO_SEND		(u_data_to_send         ),
	    	.BIST_FINISH		(cpu_bist_finish        ),
	    	.ERROR_VEC			(cpu_bist_error_vec     ),
//INPUT  PORTS
		`ifdef	INSERT_SCAN_CPU			// YUKY_2002_0919
        	.TEST_SE				(1'b0				),
        	.TEST_SI                (9'h0				),
        `endif
		`ifdef  SCAN_EN
            .TEST_SE                (DFT_TEST_SE		),
            .TEST_SI                (CPU_TEST_SI[8:0]	),
        `endif
            .SCAN_MODE			(cpu_scan_test_mode		),
	    	.BIST_MODE			(cpu_bist_mode          ),
            .NMIJ				(1'b1	            ),
            .INTJ				(sys_int_	            ),
            .J_TDI				(j_tdi                  ),
            .J_TMS				(j_tms                  ),
            .CLK				(cpu_clk_src            ),
            .J_TCLK				(j_tclk                 ),
            .PD_PROB_EN			(cpu_probe_en           ),
            .O_READY			(x_ready                ),
            .O_ACK				(x_ack                  ),
            .O_BUS_ERROR		(x_bus_error            ),
            .O_DATA_IN			(x_data_in              ),
            .RESETJ				(core_warmrst_          ),
            .RSTJ				(core_coldrst_          ),
            .TRSTJ				(j_rst_                 )
            );


`endif          // yuky,2002-07-08
//===============================================================

//=========================invoke the t2 chipset module =========
t2_chipset T2_CHIPSET (
        // internal core
        //.x_rd_last      	(x_rd_last      	),
        //.x_wr_last      	(x_wr_last      	),
        .x_ready        	(x_ready        	),
        //.x_bus_idle   	(x_bus_idle     	),
        .x_bus_error    	(x_bus_error    	),
        .x_data_in      	(x_data_in      	),
        .x_ack          	(x_ack          	),

        .u_request      	(u_request      	),
        .u_write        	(u_write        	),
        .u_wordsize     	(u_wordsize     	),
        .u_startpaddr   	(u_startpaddr   	),
        .u_data_to_send 	(u_data_to_send 	),
        .u_bytemask     	(u_bytemask     	),

        .x_err_int      	(x_err_int      	),      // snow 2000-01-12
        .x_ext_int      	(x_ext_int      	),      // snow 2000-01-12

//        .cpu_testout		(cpu_testout		),

        // controller
        // SDRAM & FLASH
        .ram_addr       	(ram_addr        	),      // SDRAM address pins & ROM partial address pins
        .ram_ba         	(ram_ba          	),      // SDRAM bank address
        .ram_dq_out     	(ram_dq_out      	),      // SDRAM 32 bits output data pins
        .ram_dq_in      	(ram_dq_in       	),      // SDRAM 32 bit input data bus
        .ram_dq_oe_     	(ram_dq_oe_      	),		// Data Port Output Enable
        .ram_dqm        	(ram_dqm         	),      // SDRAM data mask signal
        .ram_cs_        	(ram_cs_         	),      // SDRAM chip select
        .ram_cas_       	(ram_cas_        	),      // SDRAM cas#
        .ram_we_        	(ram_we_         	),      // SDRAM we#
        .ram_ras_       	(ram_ras_        	),      // SDRAM ras#
        .ram_cke        	(ram_cke         	),      // SDRAM clock enable

        // EEPROM
        .eeprom_addr    	(eeprom_addr     	),
        .eeprom_rdata   	(eeprom_rdata    	),
//        .eeprom_ce_     	(eeprom_ce_      	),
        .eeprom_oe_     	(eeprom_oe_      	),
        .eeprom_wdata   	(eeprom_wdata    	),
        .eeprom_we_     	(eeprom_we_      	),
        .rom_data_oe_   	(rom_data_oe_    	),
        .eeprom_ale			(eeprom_ale			),
//		.eeprom_ale_oe_		(eeprom_ale_oe_		),
        .rom_addr_oe    	(rom_addr_oe     	),
        .rom_en_        	(rom_en_         	),

        //PCI
        .out_rst_			(out_rst_			),
        .oe_ad_         	(oe_ad_             ),
        .out_ad         	(out_ad             ),
        .oe_cbe_        	(oe_cbe_            ),
        .out_cbe_       	(out_cbe_           ),
        .oe_frame_      	(oe_frame_          ),      // SN000616 for gsim delay
        .out_frame_     	(out_frame_         ),      // SN000616 for gsim delay
        .oe_irdy_       	(oe_irdy_           ),
        .out_irdy_      	(out_irdy_          ),
        .oe_devsel_     	(oe_devsel_         ),
        .out_devsel_    	(out_devsel_        ),
        .oe_trdy_       	(oe_trdy_           ),
        .out_trdy_      	(out_trdy_          ),
        .oe_stop_       	(oe_stop_           ),
        .out_stop_      	(out_stop_          ),
        .oe_par_        	(oe_par_            ),      // SN000616 for gsim delay
        .out_par        	(out_par            ),      // SN000616 for gsim delay
        .out_gnt_       	(out_gnt_           ),
        .oe_perr_            (oe_perr_            ),
	    .out_perr_          (out_perr_          ),
	    .oe_serr_            (oe_serr_            ),
	    .out_serr_          (out_serr_          ),

        .in_req_        	(in_req_            ),      // snow 2000-01-05
        .in_ad          	(in_ad              ),
        .in_cbe_        	(in_cbe_            ),
        .in_frame_      	(in_frame_          ),
        .in_irdy_       	(in_irdy_           ),
        .in_devsel_     	(in_devsel_         ),
        .in_trdy_       	(in_trdy_           ),
        .in_stop_       	(in_stop_           ),
        .in_par         	(in_par             ),
        .in_serr_           (in_serr_           ),
        .in_perr_           (in_perr_           ),

        .ext_int        	(ext_int            ),
        .gpio_out       	(gpio_out           ),     // Add pc_trace 2000-08-03
        .gpio_in        	(gpio_in            ),
        .gpio_oe_       	(gpio_oe_           ),

        .gpio_set2_out      (gpio_set2_out      ),
        .gpio_set2_in       (gpio_set2_in       ),
        .gpio_set2_oe_      (gpio_set2_oe_      ),

        .out_i2so_data0  	(out_i2so_data0     ),
        .out_i2so_data1 	(out_i2so_data1     ),
        .out_i2so_data2 	(out_i2so_data2     ),
        .out_i2so_data3		(out_i2so_data3		),
        .out_i2so_data4		(out_i2so_data4		),
        .out_i2so_lrck  	(out_i2so_lrck      ),
        .i2so_ch10_en		(i2so_ch10_en		),
        .out_i2so_bick  	(out_i2so_bick      ),
        .out_i2si_lrck      (out_i2si_lrck      ),
		.out_i2si_bick		(out_i2si_bick		),

        .in_i2si_data   	(in_i2si_data       ),
  		.slave_i2si_bick	(slave_i2si_bick	),
  		.slave_i2si_lrck	(slave_i2si_lrck    ),
		.i2si_mode_sel  	(i2si_mode_sel      ),
  //    .in_i2si_lrck   	(in_i2si_lrck       ),
  //    .in_i2si_bick   	(in_i2si_bick       ),
        .in_i2so_mclk   	(in_i2so_mclk       ),
        .iadc_mclk          (iadc_mclk			),
		.iadc_smtsen        (iadc_smtsen		),
		.iadc_smtsmode      (iadc_smtsmode		),
		.iadc_smts_err      (iadc_smts_err		),
		.iadc_smts_end      (iadc_smts_end		),
		.iadc_adcclkp1      (iadc_adcclkp1		),
		.iadc_adcclkp2      (iadc_adcclkp2		),
		.iadc_tst_mod 		(iadc_tst_mod		),

        //spdif
        .out_spdif      	(out_spdif          ),
		.in_spdif			(in_spdif			),

        // south bridge
        .ir_rx          	(ir_rx              ),

        .scb_scl_out    	(scb_scl_out        ),
        .scb_scl_oe     	(scb_scl_oe         ),
        .scb_sda_out    	(scb_sda_out        ),
        .scb_sda_oe     	(scb_sda_oe         ),
        .scb_scl_in     	(scb_scl_in         ),
        .scb_sda_in     	(scb_sda_in         ),

        .uart_tx			(uart_tx			),
        .uart_rx			(uart_rx			),

        // eric000430: USB added below
        .p1_ls_mode     	(p1_ls_mode         ),      // to USB pad
        .p1_txd         	(p1_txd             ),
        .p1_txd_oej     	(p1_txd_oej         ),
        .p1_txd_se0     	(p1_txd_se0         ),
        .p2_ls_mode     	(p2_ls_mode         ),
        .p2_txd         	(p2_txd             ),
        .p2_txd_oej     	(p2_txd_oej         ),
        .p2_txd_se0     	(p2_txd_se0         ),
        //add 2 ports p3,p4 by yuky,2002-06-14
 //     .p3_ls_mode     	(p3_ls_mode         ),      // to USB pad
 //     .p3_txd         	(p3_txd             ),
 //     .p3_txd_oej     	(p3_txd_oej         ),
 //     .p3_txd_se0     	(p3_txd_se0         ),
 //     .p4_ls_mode     	(p4_ls_mode         ),
 //     .p4_txd         	(p4_txd             ),
 //     .p4_txd_oej     	(p4_txd_oej         ),
 //     .p4_txd_se0     	(p4_txd_se0         ),
        .usb_pon_       	(usb_pon_           ),      // power control
        .i_p1_rxd       	(i_p1_rxd           ),      // from USB pad
        .i_p1_rxd_se0   	(i_p1_rxd_se0       ),
        .i_p2_rxd       	(i_p2_rxd           ),
        .i_p2_rxd_se0   	(i_p2_rxd_se0       ),
        //add 2 ports p3,p4 by yuky,2002-06-14
 //     .i_p3_rxd       	(i_p3_rxd           ),      // from USB pad
 //     .i_p3_rxd_se0   	(i_p3_rxd_se0       ),
 //     .i_p4_rxd       	(i_p4_rxd           ),
 //     .i_p4_rxd_se0   	(i_p4_rxd_se0       ),
        .usb_overcur_   	(usb_overcur_       ),      // over current input

//        .pclk_oe_           (pclk_oe_           ),
        .vdata_oe_          (vdata_oe_          ),
        .vdata_out          (vdata_out          ),
        .h_sync_out_        (h_sync_out_        ),
        .h_sync_oe_         (h_sync_oe_         ),
//        .in_h_sync_         (in_h_sync_         ),
        .v_sync_out_        (v_sync_out_        ),
        .v_sync_oe_         (v_sync_oe_         ),
//        .in_v_sync_         (in_v_sync_         ),

		// TV Encoder intf
		.tvenc_vdi4vdac		(tvenc_vdi4vdac			),
		.tvenc_vdo4vdac		(tvenc_vdo4vdac			),

		.tvenc_idump		(tvenc_idump			),
		.tvenc_iext			(tvenc_iext				),
		.tvenc_iext1		(tvenc_iext1			),
		.tvenc_iout1		(tvenc_iout1			),
		.tvenc_iout2		(tvenc_iout2			),
		.tvenc_iout3		(tvenc_iout3			),
		.tvenc_iout4		(tvenc_iout4			),
		.tvenc_iout5		(tvenc_iout5			),
		.tvenc_iout6		(tvenc_iout6			),
		.tvenc_iref1		(tvenc_iref1			),
//		.tvenc_iref2		(tvenc_iref2			),
		.tvenc_h_sync_		(tvenc_h_sync_			),	//output to mux with ext TV encoder for DE slaver sync
		.tvenc_v_sync_      (tvenc_v_sync_          ),
		.tvenc_vdactest		(tvenc_vdactest			),	// this signal is useless in t2_chipset in 2003-12-05
		.tvenc_gnda			(tvenc_gnda				),	// ground for DAC
		.tvenc_gnda1		(tvenc_gnda1			),	// ground for DAC
		.tvenc_vsud			(tvenc_vsud				),	// ground for DAC
		.tvenc_vd33a		(tvenc_vd33a			),	// Analog 3.3 power for DAC
//VIDEO IN
		.VIN_PIXEL_DATA		(VIN_PIXEL_DATA			),
		.VIN_PIXEL_CLK		(VIN_PIXEL_CLK	        ),
		.VIN_H_SYNCJ	    (VIN_H_SYNCJ	        ),
		.VIN_V_SYNCJ	    (VIN_V_SYNCJ	        ),

        // IDE Interface
        .atadiow_out_       (atadiow_out_           ),
        .atadior_out_       (atadior_out_           ),
        .atacs0_out_        (atacs0_out_            ),
        .atacs1_out_        (atacs1_out_            ),
        .atadd_out          (atadd_out              ),
        .atadd_oe_          (atadd_oe_              ),
        .atada_out          (atada_out              ),
      	.atareset_out_      (atareset_out_          ),
      	.atareset_oe_		(atareset_oe_			),
        .atadmack_out_      (atadmack_out_          ),

        .ataiordy_in        (ataiordy_in            ),
        .ataintrq_in        (ataintrq_in            ),
        .atadd_in           (atadd_in               ),
        .atadmarq_in        (atadmarq_in            ),


        // Clock
        .usb_clk            (usb_clk                ),      // 48M clock for USB, from PLL
        .cpu_clk            (cpu_clk                ),
        .mem_clk            (mem_clk                ),
        .pci_clk            (pci_clk                ),
        .sb_clk             (sb_clk                 ),      // SN000607
        .dsp_clk            (dsp_clk                ),
        .spdif_clk          (spdif_clk              ),
		.ata_clk			(ata_clk				),		// IDE clock, 54MHz
		.servo_clk			(servo_clk				),		// servo clock

        .pad_boot_config    (pad_boot_config        ),

		.inter_only			(inter_only				),		// Interlace output
		.dvi_rwbuf_sel		(dvi_rwbuf_sel			),		// DVI buffer read/write

		.cvbs2x_clk			(cvbs2x_clk				),		// 54MHZ clock
		.cvbs_clk			(cvbs_clk				),		// 27MHz clock
		.cav_clk			(cav_clk				),		// 54 or 27 MHZ clock
		.dvi_buf0_clk		(dvi_buf0_clk			),		// CAV_CLK or VIDEO_CLK for buffer 0
		.dvi_buf1_clk		(dvi_buf1_clk			),		// CAV_CLK or VIDEO_CLK for buffer 1
		.video_clk			(video_clk				),		// VIDEO clock output
		.vsb_clk			(vsb_clk				),		// VSB clock 80M

		.tv_f108m_clk		(tv_f108m_clk			),		// tv_encoder 108MHz clock
		.tv_cvbs2x_clk		(tv_cvbs2x_clk			),		// tv_encoder 54MHZ clock
		.tv_cvbs_clk		(tv_cvbs_clk			),		// tv_encoder 27MHz clock
		.tv_cav2x_clk		(tv_cav2x_clk			),		// tv_encoder 108 or 54 MHZ clock
		.tv_cav_clk			(tv_cav_clk				),		// tv_encoder 54 or 27 MHZ clock

//      PLL control
		.cpu_clk_pll_m_value	(cpu_clk_pll_m_value	),	// new cpu_clk_pll m parameter
		.cpu_clk_pll_m_tri      (cpu_clk_pll_m_tri		),  // cpu_clk_pll m modification trigger
		.cpu_clk_pll_m_ack      (cpu_clk_pll_m_ack		),  // cpu_clk_pll m modification ack

		.mem_clk_pll_m_value    (mem_clk_pll_m_value	),  // new mem_clk_pll m parameter
		.mem_clk_pll_m_tri      (mem_clk_pll_m_tri		),  // mem_clk_pll m modification trigger
		.mem_clk_pll_m_ack      (mem_clk_pll_m_ack		),  // mem_clk_pll m modification ack

//		.pci_fs_value 			(pci_fs_value			),	// new pci frequency select parameter
//		.pci_fs_tri             (pci_fs_tri				),	// pci frequency select modification trigger
//		.pci_fs_ack             (pci_fs_ack				),	// pci frequency select modification ack

		.work_mode_value		(work_mode_value		),	// new work_mode
		.work_mode_tri  		(work_mode_tri			),  // work_mode modification trigger
		.work_mode_ack  		(work_mode_ack			),  // work_mode modification ack

        //      clock frequency control, clock misc control
		.ide_fs					(ide_fs					),	// ide clock frequency select
		.dsp_fs					(dsp_fs					),	// dsp clock frequency select
		.dsp_div_src_sel		(dsp_div_src_sel		),	// dsp source clock select
		.pci_fs					(pci_fs					),	// pci clock frequency select
		.ve_fs					(ve_fs					),	// ve clock frequency select
		.ve_div_src_sel			(ve_div_src_sel			),	// ve source clock select
//		.pci_clk_out_sel		(pci_clk_out_sel		),
		.svga_mode_en			(svga_mode_en			),	// SVGA mode output enable
		.tv_mode				(tv_mode				),	// video core output pixel for tv mode
		.out_mode_etv_sel		(out_mode_etv_sel		),	// output pixel clock select, 0 -> 54 MHz, 1 -> 27 MHz
//		.tv_enc_clk_sel			(tv_enc_clk_sel			),	// TV encoder clock setting

        //sdram delay select
        .mem_dly_sel        	(mem_dly_sel            ),
        .mem_rd_dly_sel			(mem_rd_dly_sel			),
		.test_dly_chain_sel		(test_dly_chain_sel		),	// TEST_DLY_CHAIN delay select
		.test_dly_chain_flag	(test_dly_chain_flag	),	// TEST_DLY_CHAIN flag output

//	external interface control
		.audio_pll_fout_sel		(audio_pll_fout_sel		),	// Audio PLL FOUT frequency select
		.i2s_mclk_sel			(i2s_mclk_sel			),	// I2S MCLK source select
		.i2s_mclk_out_en_		(i2s_mclk_out_en_		),	// I2S MCLK output enable
		.spdif_clk_sel			(spdif_clk_sel			),	// S/PDIF clock select
		.i2so_data2_1_gpio_en	(i2so_data2_1_gpio_en	),	// I2SO DATA[2:1] GPIO enable
		.i2so_data3_gpio_en		(i2so_data3_gpio_en		),	// I2SO DATA3 GPIO enable
		.xsflag_gpio_en			(xsflag_gpio_en			),	// Xsflag[1:0] share with gpio[25:24] enable
		.spdif_gpio_en			(spdif_gpio_en			),	// Spdif share with gpio[21] en
		.uart_gpio_en			(uart_gpio_en			),	// Uart_tx share with gpio[24] en
		.hv_sync_gpio_en		(hv_sync_gpio_en		),	// H_sync_, v_sync_ share with gpio[11:10]
		.hv_sync_en				(hv_sync_en				),	// h_sync_, v_sync_ enable on the xsflag[1:0] pins
		.i2si_clk_en			(i2si_clk_en			),	// I2S input data enable on the gpio[7] pin
		.scb_en					(scb_en					),	// SCB interface enable on the gpio[6:5] pins
		.servo_gpio_en			(servo_gpio_en			),	// servo_gpio[2:0] enable on the gpio[2:0] pins
		.ext_ide_device_en		(ext_ide_device_en		),	// external IDE device enable
//		.pci_gntb_en			(pci_gntb_en			),	// PCI GNTB# enable
		.vdata_en				(vdata_en				),	// video data enable
		.sync_source_sel		(sync_source_sel		),	// sync signal source select, 0 -> TVENC, 1 -> DE.
		.video_in_intf_en		(video_in_intf_en		),	// video in interface enable
//		.pci_mode_ide_en		(pci_mode_ide_en		),	// pci_mode ide interface enable
		.sdram_cs1j_en			(sdram_cs1j_en			),	// sdram cs1# enable
		.ata_share_mode_2_en	(ata_share_mode_2_en	),	// atadmack_ and atada[0] share with rom_oe_ and rom_we_
		.ata_ad_src_sel			(ata_ad_src_sel			),	// rom_addr[18:17] bond selection
		.ide_active				(ide_active				),	// ide interface is active in the pci mode
		.standby_mode_en		(standby_mode_en		),	// system standby mode

//	PAD Driever Capability
		.gpio_pad_driv	   		(gpio_pad_driv	   		),	// gpio pad driver capability
		.rom_da_pad_driv    	(rom_da_pad_driv    	),  // rom data/address driver capability
		.sdram_data_pad_driv	(sdram_data_pad_driv	),  // sdram data driver capability
		.sdram_ca_pad_driv		(sdram_ca_pad_driv		),  // sdram command/address driver capability

		//SERVO INTERFACE
//		dmckg_mpgclk		,		// MEM_CLK
//		sv_top_clkdiv		,
		.sv_pmata_somd			(sv_pmata_somd			),
		.sv_pmsvd_trcclk		(sv_pmsvd_trcclk		),
		.sv_pmsvd_trfclk		(sv_pmsvd_trfclk		),
		.sv_sv_tslrf			(sv_sv_tslrf			),
		.sv_sv_tplck			(sv_sv_tplck			),
//		.sv_sv_dev_true  		(sv_sv_dev_true     	),
		.sv_tst_svoio_i			(sv_tst_svoio_i			),
		.sv_svo_svoio_o			(sv_svo_svoio_o			),
		.sv_svo_svoio_oej		(sv_svo_svoio_oej		),
		.sv_tst_addata			(sv_tst_addata			),
		.sv_reg_rctst_2  		(sv_reg_rctst_2       	),
//		sv_top_sv_mppdn		,
//		.sv_dmckg_sysclk 		(sv_dmckg_sysclk        ),
		.sv_mckg_dspclk  		(sv_mckg_dspclk         ),
//		.sv_sycg_svoata 		(sv_sycg_svoata 		),
//		sv_sv_reg_c2ftst       ,
//		sv_pmdm_ideslave      ,
		.sv_pmsvd_tplck      	(sv_pmsvd_tplck         ),
		.sv_pmsvd_tslrf      	(sv_pmsvd_tslrf         ),
		.sv_sv_tst_pin			(sv_sv_tst_pin			),
//		.sv_sv_reg_tstoen    	(sv_sv_reg_tstoen       ),
		.sv_pmsvd_c2ftstin		(sv_pmsvd_c2ftstin		),
		.sv_sycg_somd			(sv_sycg_somd			),
		// Servo analog
//		.sv_cpu_gpioin			(sv_cpu_gpioin			),
//		.sv_sycg_burnin			(sv_sycg_burnin			),
		// External uP interface
		.sv_ex_up_in			(sv_ex_up_in			),
		.sv_ex_up_out			(sv_ex_up_out			),
		.sv_ex_up_oe_			(sv_ex_up_oe_			),
		// Servo Atapi inf
		.sv_sv_hi_hdrq_o     	(sv_sv_hi_hdrq_o        ),
		.sv_sv_hi_hdrq_oej   	(sv_sv_hi_hdrq_oej      ),
		.sv_sv_hi_hcs16j_o   	(sv_sv_hi_hcs16j_o      ),
		.sv_sv_hi_hcs16j_oej 	(sv_sv_hi_hcs16j_oej    ),
		.sv_sv_hi_hint_o     	(sv_sv_hi_hint_o        ),
		.sv_sv_hi_hint_oej   	(sv_sv_hi_hint_oej      ),
		.sv_sv_hi_hpdiagj_o  	(sv_sv_hi_hpdiagj_o     ),
		.sv_sv_hi_hpdiagj_oej	(sv_sv_hi_hpdiagj_oej   ),
		.sv_sv_hi_hdaspj_o   	(sv_sv_hi_hdaspj_o      ),
		.sv_sv_hi_hdaspj_oej 	(sv_sv_hi_hdaspj_oej    ),
		.sv_sv_hi_hiordy_o   	(sv_sv_hi_hiordy_o      ),
		.sv_sv_hi_hiordy_oej 	(sv_sv_hi_hiordy_oej    ),
		.sv_sv_hi_hd_o			(sv_sv_hi_hd_o			),
		.sv_sv_hi_hd_oej     	(sv_sv_hi_hd_oej        ),
		.sv_pmata_hrstj      	(sv_pmata_hrstj         ),
		.sv_pmata_hcs1j      	(sv_pmata_hcs1j         ),
		.sv_pmata_hcs3j      	(sv_pmata_hcs3j         ),
		.sv_pmata_hiorj      	(sv_pmata_hiorj         ),
		.sv_pmata_hiowj      	(sv_pmata_hiowj         ),
		.sv_pmata_hdackj     	(sv_pmata_hdackj        ),
		.sv_pmata_hpdiagj    	(sv_pmata_hpdiagj       ),
		.sv_pmata_hdaspj     	(sv_pmata_hdaspj        ),
		.sv_pmata_ha			(sv_pmata_ha			),
		.sv_pmata_hd			(sv_pmata_hd			),
		// Servo Test
		.sv_sv_svo_sflag		(sv_sv_svo_sflag		),
		.sv_sv_svo_soma			(sv_sv_svo_soma			),
		.sv_sv_svo_somd			(sv_sv_svo_somd			),
		.sv_sv_svo_somd_oej  	(sv_sv_svo_somd_oej     ),
		.sv_sv_svo_somdh_oej 	(sv_sv_svo_somdh_oej    ),
//		.sv_sv_reg_dvdram_toe	(sv_sv_reg_dvdram_toe   ),
//		.sv_sv_reg_dfctsel   	(sv_sv_reg_dfctsel      ),
//		.sv_sv_reg_trfsel 		(sv_sv_reg_trfsel 		),
//		.sv_sv_reg_flgtoen 		(sv_sv_reg_flgtoen 		),
//		.sv_sv_reg_gpioten		(sv_sv_reg_gpioten		),
//		.sv_sv_reg_13e_7		(sv_sv_reg_13e_7		),
//		.sv_sv_reg_13e_6		(sv_sv_reg_13e_6		),
//		sv_reg_trg_plcken		,
//		.sv_reg_sv_tplck_oej	(sv_reg_sv_tplck_oej	),
		//	Servo analog ANA_DTSV

		.sv_pad_clkref			(sv_pad_clkref			),
		.sv_xadcin       		(sv_xadcin              ),
		.sv_xadcip       		(sv_xadcip              ),
		.sv_xatton       		(sv_xatton              ),
		.sv_xattop       		(sv_xattop              ),
		.sv_xbiasr       		(sv_xbiasr              ),
		.sv_xcdld        		(sv_xcdld               ),
		.sv_xcdpd        		(sv_xcdpd               ),
		.sv_xcdrf        		(sv_xcdrf               ),
		.sv_xcd_a        		(sv_xcd_a               ),
		.sv_xcd_b        		(sv_xcd_b               ),
		.sv_xcd_c        		(sv_xcd_c               ),
		.sv_xcd_d        		(sv_xcd_d               ),
		.sv_xcd_e        		(sv_xcd_e               ),
		.sv_xcd_f        		(sv_xcd_f               ),
		.sv_xcelpfo      		(sv_xcelpfo             ),
		.sv_xdpd_a       		(sv_xdpd_a              ),
		.sv_xdpd_b       		(sv_xdpd_b              ),
		.sv_xdpd_c       		(sv_xdpd_c              ),
		.sv_xdpd_d       		(sv_xdpd_d              ),
		.sv_xdvdld       		(sv_xdvdld              ),
		.sv_xdvdpd       		(sv_xdvdpd              ),
		.sv_xdvdrfn      		(sv_xdvdrfn             ),
		.sv_xdvdrfp      		(sv_xdvdrfp             ),
		.sv_xdvd_a       		(sv_xdvd_a              ),
		.sv_xdvd_b       		(sv_xdvd_b              ),
		.sv_xdvd_c       		(sv_xdvd_c              ),
		.sv_xdvd_d       		(sv_xdvd_d              ),
		.sv_xfelpfo      		(sv_xfelpfo             ),
		.sv_xfocus       		(sv_xfocus              ),
		.sv_xgmbiasr     		(sv_xgmbiasr            ),
		.sv_xlpfon       		(sv_xlpfon              ),
		.sv_xlpfop       		(sv_xlpfop              ),
		.sv_xpdaux1      		(sv_xpdaux1             ),
		.sv_xpdaux2      		(sv_xpdaux2             ),
		.sv_xsblpfo      		(sv_xsblpfo             ),
		.sv_xsfgn        		(sv_xsfgn               ),
		.sv_xsfgp        		(sv_xsfgp               ),

		.sv_pata_sflag			(sv_pata_sflag			),
		.sv_pmata_sflag			(sv_pmata_sflag			),
		.sv_pmata_sflag_oej		(sv_pmata_sflag_oej		),
		.sv_xsflag       		(sv_xsflag              ),
		.sv_xslegn       		(sv_xslegn              ),
		.sv_xslegp       		(sv_xslegp              ),
		.sv_xspindle     		(sv_xspindle            ),
		.sv_xtelp        		(sv_xtelp               ),
		.sv_xtelpfo      		(sv_xtelpfo             ),
		.sv_xtestda      		(sv_xtestda             ),
		.sv_xtexo        		(sv_xtexo               ),
		.sv_xtrack       		(sv_xtrack              ),
		.sv_xtray        		(sv_xtray               ),
		.sv_xvgain       		(sv_xvgain              ),
		.sv_xvgaip       		(sv_xvgaip              ),
		.sv_xvref15      		(sv_xvref15             ),
		.sv_xvref21      		(sv_xvref21             ),
		//Servo power
		.sv_vd33d        		(sv_vd33d            	),
		.sv_vd18d        		(sv_vd18d            	),
		.sv_vdd3mix1			(sv_vdd3mix1			),
		.sv_vdd3mix2			(sv_vdd3mix2			),
		.sv_avdd_ad      		(sv_avdd_ad          	),
		.sv_avdd_att     		(sv_avdd_att         	),
		.sv_avdd_da      		(sv_avdd_da          	),
		.sv_avdd_dpd     		(sv_avdd_dpd         	),
		.sv_avdd_lpf     		(sv_avdd_lpf         	),
		.sv_avdd_rad     		(sv_avdd_rad         	),
		.sv_avdd_ref     		(sv_avdd_ref         	),
		.sv_avdd_svo     		(sv_avdd_svo         	),
		.sv_avdd_vga     		(sv_avdd_vga         	),
		.sv_avss_ad      		(sv_avss_ad          	),
		.sv_avss_att     		(sv_avss_att         	),
		.sv_avss_da      		(sv_avss_da          	),
		.sv_avss_dpd     		(sv_avss_dpd         	),
		.sv_avss_ga      		(sv_avss_ga          	),
		.sv_avss_gd      		(sv_avss_gd          	),
		.sv_avss_lpf     		(sv_avss_lpf         	),
		.sv_avss_rad     		(sv_avss_rad         	),
		.sv_avss_ref     		(sv_avss_ref         	),
		.sv_avss_svo     		(sv_avss_svo         	),
		.sv_avss_vga     		(sv_avss_vga         	),
		.sv_azec_gnd     		(sv_azec_gnd         	),
		.sv_azec_vdd     		(sv_azec_vdd         	),
		.sv_dv18_rcka    		(sv_dv18_rcka        	),
		.sv_dv18_rckd    		(sv_dv18_rckd        	),
		.sv_dvdd         		(sv_dvdd             	),
		.sv_dvss         		(sv_dvss             	),
		.sv_dvss_rcka    		(sv_dvss_rcka        	),
		.sv_dvss_rckd    		(sv_dvss_rckd        	),
		.sv_fad_gnd      		(sv_fad_gnd          	),
		.sv_fad_vdd      		(sv_fad_vdd          	),
		.sv_gndd         		(sv_gndd             	),


//	SERVO control
		.sv_tst_pin_en					(sv_tst_pin_en			),	// SERVO test pin enable
		.sv_ide_mode_part0_en			(sv_ide_mode_part0_en	),	// ide mode SERVO test pin part 0 enable
		.sv_ide_mode_part1_en			(sv_ide_mode_part1_en	),	// ide mode SERVO test pin part 1 enable
		.sv_ide_mode_part2_en			(sv_ide_mode_part2_en	),	// ide mode SERVO test pin part 2 enable
		.sv_ide_mode_part3_en			(sv_ide_mode_part3_en	),	// ide mode SERVO test pin part 3 enable
		.sv_c2ftstin_en					(sv_c2ftstin_en			),	// SERVO c2 flag enable
//		.sv_sycg_updbgout				(sv_sycg_updbgout		),	// SERVO cpu interface debug enable
		.sv_trg_plcken					(sv_trg_plcken			),	// SERVO tslrf and tplck enable
		.sv_pmsvd_trcclk_trfclk_en		(sv_pmsvd_trcclk_trfclk_en),	// SERVO pmsvd_trcclk, pmsvd_trfclk enable

		.iadc_v1p2u 					(iadc_v1p2u 			),
		.iadc_vs    					(iadc_vs    			),
		.iadc_vd    					(iadc_vd    			),
		.iadc_dvdd						(iadc_dvdd				),			//	ADC digital power
		.iadc_dvss						(iadc_dvss				),			//	ADC digital ground
		.iadc_vin_l 					(iadc_vin_l 			),
		.iadc_vin_r 					(iadc_vin_r 			),

		.flash_mode						(flash_mode				),
//       .cpu_mode               		(cpu_mode               ),
//        .pci_mode               		(pci_mode               ),
        .ide_mode            			(ide_mode            	),
        .tvenc_test_mode				(tvenc_test_mode		),
		.servo_only_mode        		(servo_only_mode        ),
		.servo_test_mode        		(servo_test_mode        ),
		.servo_sram_mode        		(servo_sram_mode        ),
        .test_mode              		(test_mode              ),
        .bist_test_mode					(bist_test_mode			),

		.dsp_bist_mode					(dsp_bist_mode			),

//DSP bist interface
		.dsp_bist_finish				(dsp_bist_finish		),
		.dsp_bist_err_vec				(dsp_bist_err_vec       ),

//VIDEO bist interface
		.video_bist_tri					(video_bist_tri			),
		.video_bist_finish				(video_bist_finish	    ),
		.video_bist_vec					(video_bist_vec		    ),

//======================ms / sd  ===================
		.ms_ins				(ms_ins				)	,       //port
		.ms_serin_in   	    (ms_serin_in   		)	,		//port
		.sd_data0_in   	    (sd_data_in[0]   	)	,
		.sd_data1_in   	    (sd_data_in[1]   	)	,
		.sd_data2_in   	    (sd_data_in[2]   	)	,
		.sd_data3_in   	    (sd_data_in[3]   	)	,
		.sd_mmc_cmd_in 	    (sd_mmc_cmd_in 		)	,
		.ms_clk        	    (ms_clk        		)	,
		.ms_bsst       	    (ms_bsst       		)	,
		.ms_pw_ctrl    	    (ms_pw_ctrl    		)	,
		.ms_serout     	    (ms_serout     		)	,
		.msde          	    (msde          		)	,
		.sd_data0_oej  	    (sd_data_oej[0]  	)	,
		.sd_data1_oej  	    (sd_data_oej[1]  	)	,
		.sd_data2_oej  	    (sd_data_oej[2]  	)	,
		.sd_data3_oej  	    (sd_data_oej[3]  	)	,
		.sd_mmc_cmd_oej	    (sd_mmc_cmd_oej		)	,
		.sd_data0_out  	    (sd_data_out[0]  	)	,
		.sd_data1_out  	    (sd_data_out[1]  	)	,
		.sd_data2_out  	    (sd_data_out[2]  	)	,
		.sd_data3_out  	    (sd_data_out[3]  	)	,
		.sd_mmc_cmd_out	    (sd_mmc_cmd_out		)	,
		.sd_mmc_clk    	    (sd_mmc_clk    		)	,
		.test_h        	    (test_h        		)	,
		.test_done     	    (test_done     		)	,
		.fail_h        	    (fail_h        		)	,
		.det_pin       	    (det_pin       		)	,
		.wr_prct_pin   	    (wr_prct_pin   		)	,
//From  NB
		.sd_ip_enable		(sd_ip_enable		)	,
		.ms_ip_enable		(ms_ip_enable		)	,

		//GI Interface
        //--system
        .alt_boot		    (alt_boot			),
//		.test_ena	        (test_ena	    	),
		.gc_rst_	        (gc_rst_	        ),
		.pin_rst_		    (pin_rst_			),
		.gi_chip_rst_	    (gi_chip_rst_	    ),
		.gi_clk			    (gi_clk				),
		.gi_clk_lock	    (gi_clk_lock	    ),
		//nmask interrupt
		.sec_ack			(					),
		.sec_intr			(sec_intr			),
		//--serial prom
		.i2c_clk	        (i2c_clk	        ),
		.i2c_din	        (i2c_din	        ),
		.i2c_dout	        (i2c_dout	    	),
		.i2c_doe	        (i2c_doe	        ),
		//--dsic interfa
		.di_in		        (di_in		    	),
		.di_out		        (di_out		    	),
		.di_oe		        (di_oe		    	),
		.di_brk_in	        (di_brk_in	    	),
		.di_brk_out	        (di_brk_out	    	),
		.di_brk_oe	        (di_brk_oe	    	),
		.di_dir		        (di_dir		    	),
		.di_hstrb	        (di_hstrb	    	),
		.di_dstrb	        (di_dstrb	    	),
		.di_err		        (di_err		    	),
		.di_cover	        (di_cover	    	),
		.di_stb				(di_stb				),

		.ais_clk	        (ais_clk	        ),
		.ais_lr		        (ais_lr		    	),
		.ais_d		        (ais_d		    	),

`ifdef  SCAN_EN
		.TEST_SI				(CHIPSET_TEST_SI[77:0]	),
		.TEST_SE				(DFT_TEST_SE			),
		.TEST_SO				(CHIPSET_TEST_SO[77:0]	),
`endif

        .sw_rst_pulse           (sw_rst_pulse           ),
        .rst_                   (chipset_rst_           )
        );
//======================================================================

endmodule
