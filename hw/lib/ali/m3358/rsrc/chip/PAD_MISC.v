/*********************************************************************
File:           pad_misc.v
Descript:       control the share pins for m3358 and some pins logic
Author:         Tod
History			2004-04-17 Initial version

Note:           in this module, all logic of the "*_oe_out2pad_" are positive logic
                some will use the negative logic
*********************************************************************/
module  PAD_MISC(

//------strap pins control
        strap_pin_out,               //out to pad_rst_strap module

//------SDRAM
        //connect to pad,
        ram_dq_out2pad  ,
        ram_dq_oe_out2pad_	,
        ram_dq_frompad  ,

        //connect to core
        ram_dq_fromcore ,
        ram_dq_oe_fromcore_,
        ram_dq_out2core ,

//------SDRAM DQM, Command Line
		ram_dqm_out2pad,
		ram_dqm_oe_out2pad_,
		ram_dqm_fromcore,
        ram_dqm_frompad,
//------SDRAM bank address, address line
		ram_ba_out2pad,
		ram_ba_oe_out2pad_,
		ram_ba_fromcore,
        ram_ba_frompad,

		ram_addr_out2pad,
		ram_addr_oe_out2pad_,
		ram_addr_fromcore,
        ram_addr_frompad,

//-----	SDRAM Command Line
		ram_cs_out2pad_,
		ram_cs_fromcore_,
		ram_we_out2pad_,
		ram_we_fromcore_,
//		ram_we_frompad_,

		ram_cas_out2pad_,
		ram_cas_fromcore_,
		ram_ras_out2pad_,
		ram_ras_fromcore_,
//		ram_ras_frompad_,

		ram_cs_oe_out2pad_,
		ram_we_oe_out2pad_,
		ram_cas_oe_out2pad_,
		ram_ras_oe_out2pad_,

		ram_cs_frompad_,
		ram_cas_frompad_,

//pci interface-----------------
		//------PCI Bus
        //connect to pad
        pci_clk_fromcore,

        //connect to core
        pci_ad_out2core,
        pci_cbe_out2core_,
        pci_par_out2core,
        pci_frame_out2core_,
        pci_irdy_out2core_,
        pci_trdy_out2core_,
        pci_stop_out2core_,
        pci_devsel_out2core_,
        pci_req_out2core_,
        pci_inta_out2core_,
//serr and perr
        pci_serr_out2core_,
        pci_perr_out2core_,

        pci_ad_fromcore,
        pci_cbe_fromcore_,
        pci_par_fromcore,
        pci_frame_fromcore_,
        pci_irdy_fromcore_,
        pci_gnta_fromcore_,
        pci_gntb_fromcore_,
        pci_trdy_fromcore_,
        pci_stop_fromcore_,
        pci_devsel_fromcore_,
//serr and perr
        pci_serr_fromcore_,
        pci_perr_fromcore_,
//=================================
        pci_ad_oe_fromcore_,
        pci_cbe_oe_fromcore_,
        pci_par_oe_fromcore_,
        pci_frame_oe_fromcore_,
        pci_irdy_oe_fromcore_,
        pci_trdy_oe_fromcore_,
        pci_stop_oe_fromcore_,
        pci_devsel_oe_fromcore_,
//serr and perr oe signals
        pci_serr_oe_fromcore_,
        pci_perr_oe_fromcore_,
//=============================
        pci_clk_out2pad		,
		pci_gnta_out2pad_	,
		pci_gntb_out2pad_	,

		pci_rst_fromcore_	,
		pci_rst_out2pad_	,

		pci_frame_frompad_		,
		pci_frame_oe_out2pad_	,
		pci_frame_out2pad_		,
//serr and perr
		pci_serr_frompad_		,
		pci_serr_oe_out2pad_	,
		pci_serr_out2pad_		,

		pci_perr_frompad_		,
		pci_perr_oe_out2pad_	,
		pci_perr_out2pad_		,
//==========================
		pci_irdy_frompad_		,
		pci_irdy_oe_out2pad_	,
		pci_irdy_out2pad_		,

		pci_trdy_frompad_		,
		pci_trdy_oe_out2pad_	,
		pci_trdy_out2pad_		,

		pci_stop_frompad_		,
		pci_stop_oe_out2pad_	,
		pci_stop_out2pad_		,

		pci_devsel_frompad_		,
		pci_devsel_oe_out2pad_	,
		pci_devsel_out2pad_		,

		pci_ad_frompad			,
		pci_ad_oe_out2pad_		,
		pci_ad_out2pad			,

		pci_par_frompad			,
		pci_par_oe_out2pad_		,
		pci_par_out2pad			,

		pci_cbe_frompad_		,
		pci_cbe_oe_out2pad_		,
		pci_cbe_out2pad_		,

		pci_req_frompad_		,
		pci_inta_frompad_		,

//flash interface---------------
//------FLASH
        rom_rdata_out2core,
        rom_wdata_fromcore,
        rom_data_oe_fromcore_,
        rom_data_out2pad,
        rom_data_oe_out2pad_,
        rom_data_frompad,

        rom_addr_fromcore,
        rom_addr_oe_fromcore,
        rom_addr_out2pad,
        rom_addr_oe_out2pad_,
        rom_addr_frompad,
        rom_addr2_frompad,			// Rom address [18:17] from the bottom side PAD

        rom_oe_fromcore_,
        rom_oe_out2pad_,
//        rom_oe_oe_fromcore_,
        rom_oe_oe_out2pad_,
        rom_oe_frompad_,

        rom_ale_fromcore,
        rom_ale_out2pad,
//        rom_ce_oe_fromcore_,
        rom_ale_oe_out2pad_,
        rom_ale_frompad,

        rom_we_fromcore_,
        rom_we_out2pad_,
//        rom_we_oe_fromcore_,
        rom_we_oe_out2pad_,
        rom_we_frompad_,

        rom_intf_mode_fromcore,

//ide interface----------------
		//------IDE
        //connect to core
        atadiow_fromcore_,
        atadior_fromcore_,
        atacs0_fromcore_,
        atacs1_fromcore_,
        atadd_fromcore,
        atadd_oe_fromcore_,
        atada_fromcore,
        atareset_fromcore_,
        atadmack_fromcore_,

        ataiordy_out2core,
        ataintrq_out2core,
        atadd_out2core,
        atadmarq_out2core,

        atareset_oe_fromcore_,

        //connect to chip
        atadiow_out2pad_,
        atadior_out2pad_,
        atacs0_out2pad_,
        atacs1_out2pad_,
        atadd_out2pad,
        atadd_oe_out2pad_,
        atada_out2pad,
        atareset_out2pad_,
        atadmack_out2pad_,

        ataiordy_frompad,
        ataintrq_frompad,
        atadd_frompad,
        atadmarq_frompad,

        atareset_oe_out2pad_,

//cpu interrupt signals
		//------CPU Bus
        cpu_sysint_out2core_,   //only in for processor
        cpu_sysnmi_out2core_,   //only in for processor

        cpu_errint_fromcore,
        cpu_extint_fromcore,

		cpu_bist_tri_out2core,
		cpu_scan_test_mode,			//	The scan test mode for CPU, this will be active in CPU_BIST and CHIP_SCAN mode
		cpu_bist_finish_fromcore,
		cpu_bist_vec_fromcore,

		//------EJTAG
		j_tdi_out2pad,
		j_tdo_out2pad,
		j_tms_out2pad,
		j_trst_out2pad_,
		j_tclk_out2pad,

		j_tdi_oe_out2pad_,
		j_tdo_oe_out2pad_,
		j_tms_oe_out2pad_,
		j_trst_oe_out2pad_,
		j_tclk_oe_out2pad_,

		j_tdo_fromcore,

		j_tdi_frompad,
		j_tdo_frompad,
		j_tms_frompad,
		j_trst_frompad_,
		j_tclk_frompad,

		j_tdi_out2core,
		j_tms_out2core,
		j_trst_out2core_,
		j_tclk_out2core,

//source bridge signals
		//------I2C Intf
        //connect to core
        scbsda_out2core,
        scbscl_out2core,

        scbsda_fromcore,
        scbscl_fromcore,
        scbsda_oe_fromcore,
        scbscl_oe_fromcore,

        scbsda_frompad 		,
		scbscl_frompad      ,
                            ,
		scbsda_out2pad      ,
		scbscl_out2pad      ,
		scbsda_oe_out2pad_   ,
		scbscl_oe_out2pad_   ,


//------IR
		irrx_frompad,
		irrx_out2pad,
		irrx_oe_out2pad_,
		irrx_out2core,

//------UART
		uart_tx_fromcore	,
		uart_tx_out2pad	    ,
		uart_tx_oe_out2pad_  ,
		uart_tx_frompad,

		uart_rx_out2core	,
		uart_rx_frompad		,

//audio interface---------------
//------I2S
		i2so_data0_out2pad,
		i2so_data1_out2pad,
		i2so_data2_out2pad,
		i2so_data3_out2pad,
		i2so_data0_oe_out2pad_,
		i2so_data1_oe_out2pad_,
		i2so_data2_oe_out2pad_,
		i2so_data3_oe_out2pad_,
		i2so_data0_frompad,
		i2so_data1_frompad,
		i2so_data2_frompad,
		i2so_data3_frompad,

        i2si_data_out2core,
        i2si_bick_out2core,
		i2si_lrck_out2core,
		i2si_mode_sel_fromcore,
        i2so_data0_fromcore,
		i2so_data1_fromcore,
		i2so_data2_fromcore,
		i2so_data3_fromcore,
		i2so_data4_fromcore,
		i2so_ch10_en_fromcore,
		i2so_mclk_fromcore,
		i2so_mclk_oe_fromcore_,
		i2so_mclk_out2pad,
		i2so_mclk_oe_out2pad_,
		i2so_mclk_out2core,
		i2so_mclk_frompad,

        i2so_bck_out2pad,
        i2so_bck_oe_out2pad_,
        i2so_bck_fromcore,
        i2so_bck_frompad,

        i2so_lrck_out2pad,
        i2so_lrck_oe_out2pad_,
        i2so_lrck_fromcore,
        i2so_lrck_frompad,

        i2si_mclk_fromcore,
        i2si_bck_fromcore,
        i2si_lrck_fromcore,

        i2si_mclk_out2pad	,
		i2si_data_frompad   ,
		i2si_bck_out2pad	,
		i2si_lrck_out2pad   ,

		iadc_smtsen_out2core,		// ADC SRAM Test Enable
		iadc_smtsmode_out2core,		// ADC SRAM Test Mode
		iadc_smts_err_fromcore,		// ADC SRAM Test Error Flag
		iadc_smts_end_fromcore,		// ADC SRAM Self Test End
		iadc_clkp1_fromcore,		// ADC digital test pin 1
		iadc_clkp2_fromcore,		// ADC digital test pin 2
		iadc_tst_mod_out2core,		// ADC Internal SDM Test Selection

//spdif interface---------------
//------SPDIF
        spdif_out2pad,
        spdif_oe_out2pad_,
        spdif_fromcore,
        spdif_frompad,

		spdif_in_data_out2core,		// SPDIF in data to core
		spdif_in_data_frompad,
//video interface---------------
// VIDEO_IN
//connect to core
		video_in_pixel_clk_out2core,		// pixel clock from external video source
		video_in_pixel_data_out2core,		// pixel data from external video source
		video_in_hsync_out2core_,			// hsync_ from external video source
		video_in_vsync_out2core_,			// vsync_ from external video source
//connext to chip
		video_in_pixel_clk_frompad,
		video_in_pixel_data_frompad,
		video_in_hsync_frompad_,
		video_in_vsync_frompad_,

//TV ENCODER
		//connect to core
		tvenc_h_sync_fromcore_,		// TV Encoder H_SYNC#
		tvenc_v_sync_fromcore_,		// TV Encoder V_SYNC#
		//connect to chip
		tvenc_h_sync_out2pad_,
        tvenc_v_sync_out2pad_,
//digital video output interface
//connect to core
		vdata_fromcore,
		de_h_sync_fromcore_,		// DE H_SYNC#
		de_v_sync_fromcore_,		// DE_V_SYNC#
		pix_clk_fromcore,
//connect to chip
		vdata_out2pad,
		de_h_sync_out2pad_,		// DE H_SYNC#
		de_v_sync_out2pad_,		// DE_V_SYNC#
		pix_clk_out2pad,
        //for test
		vdi4vdac_out2core,
		vdo4vdac_fromcore,

//usb interface
		//------USB
        //connect to pad
        usbpon_out2pad,
        usbpon_oe_out2pad_,
        //connect to core
        usbpon_fromcore,

        usbovc_frompad,
        usbovc_out2core,

//------------------------------
//mem_rd_cld delay chain
		mem_clk_in,
		mem_rd_dly_sel,					//	SDRAM read data delay select
        test_dly_chain_sel,				//	TEST_DLY_CHAIN delay select
        test_dly_chain_flag,			//	TEST_DLY_CHAIN flag output

//no use by now, tod, 2004-04-17
        //------SD MS
		sd_ip_en_fromcore,					// SD IP enable
		sd_clk_fromcore,					// SD card clock

		sd_cmd_fromcore,					// SD card command/status from core
		sd_cmd_oe_fromcore_,				// SD card command/status oe# from core
		sd_cmd_out2core,					// SD card command/status out 2 core

		sd_data_fromcore,					// SD card data from core
		sd_data_oe_fromcore_,				// SD card data oe# from core
		sd_data_out2core,					// SD card data out 2 core

		sd_wr_prct_out2core,				// SD card locked
		sd_det_out2core,					// SD card inserted

		ms_ip_en_fromcore,					// MS IO enable
		ms_clk_fromcore,					// MS clock
		ms_pwr_ctrl_fromcore,				// MS power control from core
		ms_bs_fromcore,						// MS bus status from core

		ms_sdio_fromcore,					// MS sdio from core
		ms_sdio_oe_fromcore_,				// MS sdio oe# from core
		ms_sdio_out2core,					// MS sdio out 2 core

		ms_ins_out2core,					// MS card insert indicated signal

		sd_ms_bist_tri_out2core,			// SD/MS bist test trigger
		sd_ms_bist_finish_fromcore,			// SD/MS bist finish
		sd_ms_bist_vec_fromcore,			// SD/MS bist result vector

		sd_clk_out2pad,
		sd_cmd_out2pad,
		sd_cmd_oe_out2pad_,
		sd_cmd_frompad,

		sd_data_out2pad,
		sd_data_oe_out2pad_,
		sd_data_frompad,

		sd_wr_prct_frompad,
		sd_det_frompad,

		ms_ip_en_out2pad,
		ms_clk_out2pad,
		ms_pwr_ctrl_out2pad,
		ms_bs_out2pad,

		ms_sdio_out2pad,
		ms_sdio_oe_out2pad_,
		ms_sdio_frompad,

		ms_ins_frompad,
//GI Interface
		//system (6)
		gc_vid_clk_fromcore		,
		gc_sys_clk_fromcore0    ,
		gc_sys_clk_fromcore1    ,
		alt_boot_out2core       ,
		test_ena_out2core       ,
		gc_rst_fromcore         ,
		//serial eeproom interface
		i2c_clk_fromcore		,
		i2c_din_out2core        ,
		i2c_dout_fromcore       ,
		i2c_doe_fromcore        ,
		//disc interface
		di_in_out2core			,
		di_out_fromcore         ,
		di_oe_fromcore          ,
		di_brk_in_out2core      ,
		di_brk_out_fromcore     ,
		di_brk_oe_fromcore      ,
		di_dir_out2core         ,
		di_hstrb_out2core       ,
		di_dstrb_fromcore       ,
		di_err_fromcore         ,
		di_cover_fromcore       ,
		di_stb_out2core			,

		ais_clk_out2core		,
		ais_lr_out2core         ,
		ais_d_fromcore          ,

		//connect to pad
		//system (6)
		gc_vid_clk_out2pad		,
		gc_sys_clk_out2pad0		,
		gc_sys_clk_out2pad1		,
		alt_boot_frompad		,
		test_ena_frompad		,
		gc_rst_out2pad			,
//serial eeproom interface
		i2c_clk_out2pad			,
		i2c_din_frompad			,
		i2c_dout_out2pad		,
		i2c_doe_out2pad			,
		//disc interface
		di_in_frompad			,
		di_out_out2pad			,
		di_oe_out2pad			,
		di_brk_in_frompad		,
		di_brk_out_out2pad		,
		di_brk_oe_out2pad		,
		di_dir_frompad			,
		di_hstrb_frompad		,
		di_dstrb_out2pad		,
		di_err_out2pad			,
		di_cover_out2pad		,
		di_stb_frompad			,

		ais_clk_frompad			,
		ais_lr_frompad			,
		ais_d_out2pad			,

//------SERVO
//		sv_gpio_en_fromcore,
		sv_gpio_fromcore,
		sv_gpio_oe_fromcore_,
		sv_gpio_out2core,

		sv_atadd_fromcore,
		sv_ataiordy_fromcore,
		sv_ataintrq_fromcore,
		sv_atadmarq_fromcore,
		sv_ataiocs16_fromcore_,
		sv_atapdiag_fromcore_,
		sv_atadasp_fromcore_,

		sv_tst_pin_fromcore,
		sram_ifx_fromcore,

		sv_atadd_oe_fromcore_,
		sv_ataiocs16_oe_fromcore_,
		sv_atapdiag_oe_fromcore_,
		sv_atadasp_oe_fromcore_,

		sram_ifx_oe_fromcore_,
		sram_ifx_out2core,

		sv_atadd_out2core,
		sv_atada_out2core,
		sv_atacs0_out2core_,
		sv_atacs1_out2core_,
		sv_atadior_out2core_,
		sv_atadiow_out2core_,
		sv_atadmack_out2core_,
		sv_atareset_out2core_,
		sv_atapdiag_out2core_,
		sv_atadasp_out2core_,

		ext_up_out2core,
		ext_up_fromcore,
		ext_up_oe_fromcore_,

		sv_tplck_fromcore,
		sv_tslrf_fromcore,

		pmsvd_trcclk_out2core,
		pmsvd_trfclk_out2core,
		pmsvd_tplck_out2core,
		pmsvd_tslrf_out2core,
		pmsvd_c2ftstin_0_out2core,
		pmsvd_c2ftstin_1_out2core,

		sv_tst_addata_out2core,			// Servo External A/D data input
		sv_reg_rctst_2_fromcore,		// Servo RC test mode enable

		xsflag_0_out2pad,
		xsflag_1_out2pad,
		xsflag_0_oe_out2pad_,
		xsflag_1_oe_out2pad_,
		xsflag_0_frompad,
		xsflag_1_frompad,
		xsflag_0_fromcore,
		xsflag_1_fromcore,
		xsflag_0_oe_fromcore_,
		xsflag_1_oe_fromcore_,

//	SERVO control
		sv_tst_pin_en_fromcore,			// SERVO test pin enable
		sv_ide_mode_part0_en_fromcore,	// ide mode SERVO test pin part 0 enable
		sv_ide_mode_part1_en_fromcore,	// ide mode SERVO test pin part 1 enable
		sv_ide_mode_part2_en_fromcore,	// ide mode SERVO test pin part 2 enable
		sv_ide_mode_part3_en_fromcore,	// ide mode SERVO test pin part 3 enable
		sv_c2ftstin_en_fromcore,		// SERVO c2 flag enable
		sv_trg_plcken_fromcore,			// SERVO tslrf and tplck enable
		sv_pmsvd_trcclk_trfclk_en_fromcore,		// SERVO pmsvd_trcclk, pmsvd_trfclk enable

//	Control signals
		hv_sync_en_fromcore,			// h_sync_, v_sync_ enable on the xsflag[1:0] pins
		i2si_clk_en_fromcore,			// I2SI clock enable
		scb_en_fromcore,				// SCB interface enable on the gpio[6:5] pins
		sv_gpio_en_fromcore,			// servo_gpio[2:0] enable on the gpio[2:0] pins
		ext_ide_device_en_fromcore,		// external IDE device enable
		vdata_en_fromcore ,				// video data enable on sdram [23:16]
		sync_source_sel_fromcore,		// sync signal source select, 0 -> TVENC, 1 -> DE.
		pci_gntb_en_fromcore,			// pci GNTB enable
		video_in_intf_en_fromcore,		// video in interface enable
		pci_mode_ide_en_fromcore,		// pci_mode ide interface enable
		ram_cs1_en_fromcore,			// sdram cs1# enable
		ata_share_mode_2_en_fromcore,	// ATA interface sharing mode 2 enable
		xsflag_gpio_en_fromcore,		// gpio on xsflag[1:0] port enable
		spdif_gpio_en_fromcore,			// Spdif share with gpio[21] en
		i2so_data12_gpio_en_fromcore,	// gpio on i2so_data1, i2so_data2 enable
		i2so_data3_gpio_en_fromcore,	// gpio on i2so_data3 enable
		uart_gpio_en_fromcore,			// gpio on uart enable

		ide_active_fromcore,			// ide interface is active in the pci mode
		ata_ad_1413_sel_fromcore,		// ide interface AD[14:13] source select
		hv_sync_gpio_en_fromcore,		// HV_SYNC signal on the GPIO[11:10] enable

        ice_probe_en_fromcore,			// ICE probe enable. When disabled, the EJTAG signals will
        								// be used as the GPIO[20:16]

//------TEST_MODE
		dsp_bist_tri_out2core,
		dsp_bist_finish_fromcore,
		dsp_bist_vec_fromcore,

		video_bist_tri_out2core,
		video_bist_finish_fromcore,
		video_bist_vec_fromcore,

//		test_mode,
        rom_en_fromcore_,				// means rom operation.

        combo_mode,						// select combo_mode, high active
        ide_mode,               		// select ide_mode, 1 means ide mode, 0 else mode
        pci_mode,						// select pci mode, high active
        tvenc_test_mode,				// select tv encoder test mode, high active
        servo_only_mode,				// select servo only mode, high active
        servo_test_mode,				// select servo test mode, high active
        servo_sram_mode,				// select servo sram mode, high active

        cpu_pll_fck_fromcore,
        cpu_pll_lock_fromcore,
        mem_pll_fck_fromcore,
        mem_pll_lock_fromcore,
        audio_pll_fck_fromcore,
        audio_pll_lock_fromcore,
        f27_pll_fck_fromcore,
        f27_pll_lock_fromcore,

        ide_clk_fromcore,				//	ide clk output for tester
        dsp_clk_fromcore,				// 	dsp clk output for tester
        video_clk_fromcore,				//	video clk output for tester

        test_mode_frompad,      		//	select the scan chain
        scan_en_frompad,				//	scan enable from pad
        scan_en_out2core,				//	scan enable out to core
        scan_chain_sel_out2core,		//	scan chain select out to core
        scan_dout_fromcore,				//	scan out data from core
        scan_din_out2core,				//	scan in data to core

		bond_option_frompad,

		bist_test_mode,
		scan_test_mode,

		//------GPIO(0~15)
        //connect to pad,only gpio[15:0]
        gpio_out2pad,
        gpio_oe_out2pad_,
        gpio_frompad,
        //connect to core, there are 32bits,[31,0]
        gpio_out2core,
        gpio_fromcore,
        gpio_oe_fromcore_,
//------GPIO Set 2
		gpio2_fromcore,			// GPIO Set 2 from core
		gpio2_oe_fromcore_,		// GPIO Set 2 oe from core
		gpio2_out2core,			// GPIO Set 2 out 2 core

        rst_					//	chipset reset
                );
parameter udly  = 1;

//------------------------------
//------strap pins control
output  [31:0]  strap_pin_out;			// Strap pin out to PAD_RST_STRAP
//------SDRAM & FLASH
output  [31:0]  ram_dq_out2pad,			//
				ram_dq_oe_out2pad_,		//
                ram_dq_out2core;		//
input   [31:0]  ram_dq_frompad,
                ram_dq_fromcore;
input   [31:0]  ram_dq_oe_fromcore_;

//------SDRAM DQM, Command Line
output	[3:0]	ram_dqm_out2pad;
output	[3:0]	ram_dqm_oe_out2pad_;
input	[3:0]	ram_dqm_fromcore;
input   [3:0]   ram_dqm_frompad;		//
//------SDRAM bank address, address line
output	[1:0]	ram_ba_out2pad;
output	[1:0]	ram_ba_oe_out2pad_;
input	[1:0]	ram_ba_fromcore;
input   [1:0]   ram_ba_frompad;			//

output	[11:0]	ram_addr_out2pad;
output	[11:0]	ram_addr_oe_out2pad_;
input	[11:0]	ram_addr_fromcore;
input   [11:0]  ram_addr_frompad;		//

//-----	SDRAM Command Line
output			ram_cs_out2pad_;
output			ram_cs_oe_out2pad_;
input	[1:0]	ram_cs_fromcore_;
input			ram_cs_frompad_;//JFR 030704 change bit width

output			ram_we_out2pad_;
output			ram_we_oe_out2pad_;
input			ram_we_fromcore_;
// input			ram_we_frompad_;

output			ram_cas_out2pad_;
output			ram_cas_oe_out2pad_;
input			ram_cas_fromcore_;
input			ram_cas_frompad_;

output			ram_ras_out2pad_;
output			ram_ras_oe_out2pad_;
input			ram_ras_fromcore_;
//input			ram_ras_frompad_;

//pci signals----------------------
input			pci_clk_fromcore;		//no dedicated pci_clk in M6305

//connect to core
output  [31:0]  pci_ad_out2core;
output  [3:0]   pci_cbe_out2core_;
output          pci_par_out2core;
output          pci_frame_out2core_;
output          pci_irdy_out2core_;
output          pci_trdy_out2core_;
output          pci_stop_out2core_;
output          pci_devsel_out2core_;
output  [1:0]   pci_req_out2core_;
output			pci_inta_out2core_;
output          pci_serr_out2core_;
output          pci_perr_out2core_;

input   [31:0]  pci_ad_fromcore;
input   [3:0]   pci_cbe_fromcore_;
input           pci_par_fromcore;
input           pci_frame_fromcore_;
input           pci_irdy_fromcore_;
input           pci_gnta_fromcore_;
input	        pci_gntb_fromcore_;
input           pci_trdy_fromcore_;
input           pci_stop_fromcore_;
input           pci_devsel_fromcore_;

input	[31:0]	pci_ad_oe_fromcore_;
input	[3:0]	pci_cbe_oe_fromcore_;
input           pci_par_oe_fromcore_;
input           pci_frame_oe_fromcore_;
input           pci_irdy_oe_fromcore_;
input           pci_trdy_oe_fromcore_;
input           pci_stop_oe_fromcore_;
input           pci_devsel_oe_fromcore_;
//serr and perr
input           pci_serr_oe_fromcore_;
input           pci_perr_oe_fromcore_;
input           pci_serr_fromcore_  ;
input           pci_perr_fromcore_  ;
//==================
output			pci_clk_out2pad		;
output			pci_gnta_out2pad_   ;
output			pci_gntb_out2pad_   ;

output			pci_rst_out2pad_	;
input			pci_rst_fromcore_	;

input			pci_frame_frompad_		;
output			pci_frame_oe_out2pad_	;
output			pci_frame_out2pad_		;
//serr and perr
input			pci_perr_frompad_		;
output			pci_perr_oe_out2pad_	;
output			pci_perr_out2pad_		;

input			pci_serr_frompad_		;
output			pci_serr_oe_out2pad_	;
output			pci_serr_out2pad_		;
//==========================================

input			pci_irdy_frompad_		;
output			pci_irdy_oe_out2pad_	;
output			pci_irdy_out2pad_		;

input			pci_trdy_frompad_		;
output			pci_trdy_oe_out2pad_	;
output			pci_trdy_out2pad_		;

input			pci_stop_frompad_		;
output			pci_stop_oe_out2pad_	;
output			pci_stop_out2pad_		;

input			pci_devsel_frompad_		;
output			pci_devsel_oe_out2pad_	;
output			pci_devsel_out2pad_		;

input [31:0]	pci_ad_frompad			;
output[31:0]	pci_ad_oe_out2pad_		;
output[31:0]	pci_ad_out2pad			;

input 			pci_par_frompad			;
output			pci_par_oe_out2pad_		;
output			pci_par_out2pad			;

input	[3:0]	pci_cbe_frompad_		;
output	[3:0]	pci_cbe_oe_out2pad_		;
output	[3:0]	pci_cbe_out2pad_		;

input			pci_req_frompad_		;
input			pci_inta_frompad_		;

//flash interface-----------------------
//	Flash data
output  [15:0]  rom_rdata_out2core;
input   [7:0]   rom_wdata_fromcore;
input   [7:0]   rom_data_oe_fromcore_;
input	[7:0]	rom_data_frompad;
output	[7:0]	rom_data_out2pad;
output	[7:0]	rom_data_oe_out2pad_;

input   [21:0]  rom_addr_fromcore;
output	[21:0]	rom_addr_out2pad;
input   [21:0]  rom_addr_oe_fromcore;
input	[21:0]	rom_addr_frompad;
input	[18:17]	rom_addr2_frompad;			// Rom address [18:17] from the bottom side PAD

output	[21:0]	rom_addr_oe_out2pad_;

input	        rom_oe_fromcore_;
output			rom_oe_out2pad_;
//input			rom_oe_oe_fromcore_;
output			rom_oe_oe_out2pad_;
input			rom_oe_frompad_;

input	        rom_ale_fromcore;
output	        rom_ale_out2pad;
//input	        rom_ce_oe_fromcore_;
output	        rom_ale_oe_out2pad_;
input			rom_ale_frompad;

input	        rom_we_fromcore_;
output	        rom_we_out2pad_;
//input	        rom_we_oe_fromcore_;
output			rom_we_oe_out2pad_;
input	        rom_we_frompad_;

input	[1:0]	rom_intf_mode_fromcore;

//ide interface------------------------
//------IDE
//connect to core
input           atadiow_fromcore_;
input           atadior_fromcore_;
input           atacs0_fromcore_;
input           atacs1_fromcore_;
input   [15:0]  atadd_fromcore;
input   [15:0]   atadd_oe_fromcore_;
input   [2:0]   atada_fromcore;
input           atareset_fromcore_;
input           atadmack_fromcore_;

input			atareset_oe_fromcore_;

output          ataiordy_out2core;
output          ataintrq_out2core;
output  [15:0]  atadd_out2core;
output          atadmarq_out2core;

output        atadiow_out2pad_	;
output        atadior_out2pad_  ;
output        atacs0_out2pad_   ;
output        atacs1_out2pad_   ;
output [15:0] atadd_out2pad     ;
output [15:0] atadd_oe_out2pad_ ;
output [2:0]  atada_out2pad     ;
output        atareset_out2pad_ ;
output        atadmack_out2pad_ ;

input         ataiordy_frompad  ;
input         ataintrq_frompad  ;
input  [15:0] atadd_frompad     ;
output        atadmarq_frompad  ;

output		  atareset_oe_out2pad_ ;


//cpu interface
//------CPU Bus
output  [4:0]   cpu_sysint_out2core_;   //only in for processor
output			cpu_sysnmi_out2core_;   //only in for processor

input           cpu_errint_fromcore,
                cpu_extint_fromcore;

//------EJTAG
output			j_tdi_out2pad;
output			j_tdo_out2pad;
output			j_tms_out2pad;
output			j_trst_out2pad_;
output			j_tclk_out2pad;

output			j_tdi_oe_out2pad_;
output			j_tdo_oe_out2pad_;
output			j_tms_oe_out2pad_;
output			j_trst_oe_out2pad_;
output			j_tclk_oe_out2pad_;

input			j_tdo_fromcore;

input			j_tdi_frompad;
input			j_tdo_frompad;
input			j_tms_frompad;
input			j_trst_frompad_;
input			j_tclk_frompad;

output			j_tdi_out2core;
output			j_tms_out2core;
output			j_trst_out2core_;
output			j_tclk_out2core;

//------TEST_MODE
output			cpu_bist_tri_out2core;
output			cpu_scan_test_mode;				//	The scan test mode for CPU, this will be active in CPU_BIST and CHIP_SCAN mode
input			cpu_bist_finish_fromcore;
input	[5:0]	cpu_bist_vec_fromcore;//JFR 030806

//south bridge signals
//------I2C Intf
        //connect to core
output          scbsda_out2core;
output          scbscl_out2core;

input           scbsda_fromcore;
input           scbscl_fromcore;
input           scbsda_oe_fromcore;
input           scbscl_oe_fromcore;

input			scbsda_frompad;
input			scbscl_frompad;

output			scbsda_out2pad		;
output			scbscl_out2pad      ;
output			scbsda_oe_out2pad_   ;
output			scbscl_oe_out2pad_   ;

//------IR Device Intf

input			irrx_frompad;
output			irrx_out2pad;
output			irrx_oe_out2pad_;
output			irrx_out2core;

//---------UART
input			uart_tx_fromcore	;
output			uart_tx_out2pad	    ;
output			uart_tx_oe_out2pad_  ;
input			uart_tx_frompad;

output			uart_rx_out2core	;
input			uart_rx_frompad		;

//------I2S Interface
        //connect to pad
output  i2si_data_out2core;

output	i2si_bick_out2core;
output	i2si_lrck_out2core;
input	i2si_mode_sel_fromcore;

output	i2so_data0_out2pad;
output	i2so_data1_out2pad;
output	i2so_data2_out2pad;
output	i2so_data3_out2pad;
output	i2so_data0_oe_out2pad_;
output	i2so_data1_oe_out2pad_;
output	i2so_data2_oe_out2pad_;
output	i2so_data3_oe_out2pad_;
input	i2so_data0_fromcore;
input	i2so_data1_fromcore;
input	i2so_data2_fromcore;
input	i2so_data3_fromcore;
input	i2so_data4_fromcore;
input	i2so_ch10_en_fromcore;

input	i2so_data0_frompad;
input	i2so_data1_frompad;
input	i2so_data2_frompad;
input	i2so_data3_frompad;

input	i2so_mclk_fromcore;
input	i2so_mclk_oe_fromcore_;
output	i2so_mclk_out2pad;
output	i2so_mclk_oe_out2pad_;
output	i2so_mclk_out2core;
input	i2so_mclk_frompad;

output	i2so_bck_out2pad;
output	i2so_bck_oe_out2pad_;
input	i2so_bck_fromcore;
input	i2so_bck_frompad;

output	i2so_lrck_out2pad;
output	i2so_lrck_oe_out2pad_;
input	i2so_lrck_fromcore;
input	i2so_lrck_frompad;

input	i2si_mclk_fromcore;
input	i2si_bck_fromcore;
input	i2si_lrck_fromcore;

output	i2si_mclk_out2pad	;
input	i2si_data_frompad   ;
output	i2si_bck_out2pad	;
output	i2si_lrck_out2pad   ;


output	[24:23]	iadc_smtsen_out2core;		// ADC SRAM Test Enable
output	[2:0]	iadc_smtsmode_out2core;		// ADC SRAM Test Mode
input	[24:23]	iadc_smts_err_fromcore;		// ADC SRAM Test Error Flag
input	[24:23]	iadc_smts_end_fromcore;		// ADC SRAM Self Test End
input			iadc_clkp1_fromcore;		// ADC digital test pin 1
input			iadc_clkp2_fromcore;		// ADC digital test pin 2
output	[3:0]	iadc_tst_mod_out2core;		// ADC Internal SDM Test Selection

//------SPDIF Interface
output	spdif_out2pad;
output	spdif_oe_out2pad_;
input	spdif_fromcore;
input	spdif_frompad;

output	spdif_in_data_out2core;				// SPDIF in data to core
input	spdif_in_data_frompad;

//video interface------------------
// VIDEO_IN
//connect to core
output			video_in_pixel_clk_out2core;		// pixel clock from external video source
output	[7:0]	video_in_pixel_data_out2core;		// pixel data from external video source
output			video_in_hsync_out2core_;			// hsync_ from external video source
output			video_in_vsync_out2core_;			// vsync_ from external video source
//connect to chip
input			video_in_pixel_clk_frompad ;
input	[7:0]	video_in_pixel_data_frompad;
input			video_in_hsync_frompad_    ;
input			video_in_vsync_frompad_    ;

//tv encoder
input	        tvenc_h_sync_fromcore_;		// TV Encoder H_SYNC#
input			tvenc_v_sync_fromcore_;		// TV Encoder V_SYNC#

output			tvenc_h_sync_out2pad_;
output			tvenc_v_sync_out2pad_;

//digital video out
input	[7:0]	vdata_fromcore;
input			de_h_sync_fromcore_;		// DE H_SYNC#
input			de_v_sync_fromcore_;		// DE_V_SYNC#
input			pix_clk_fromcore;
//for test
output	[11:0]	vdi4vdac_out2core;
input	[11:0]	vdo4vdac_fromcore;

output	[7:0]	vdata_out2pad;
output			de_h_sync_out2pad_;		// DE H_SYNC#
output			de_v_sync_out2pad_;		// DE_V_SYNC#
output			pix_clk_out2pad;

//usb interface--------------------
//------USB
//connect to pad
output          usbpon_out2pad;
output			usbpon_oe_out2pad_;
//connect to core
input           usbpon_fromcore;
input			usbovc_frompad;
output			usbovc_out2core;

//---------------------------------
input			mem_clk_in;
input [4:0]			mem_rd_dly_sel;
input [5:0]		test_dly_chain_sel;
output[1:0]		test_dly_chain_flag;

input			rst_;

//------SD MS
input			sd_ip_en_fromcore;					// SD IP enable
input			sd_clk_fromcore;					// SD card clock

input			sd_cmd_fromcore;					// SD card command/status from core
input			sd_cmd_oe_fromcore_;				// SD card command/status oe# from core
output			sd_cmd_out2core;					// SD card command/status out 2 core

input	[3:0]	sd_data_fromcore;					// SD card data from core
input	[3:0]	sd_data_oe_fromcore_;				// SD card data oe# from core
output	[3:0]	sd_data_out2core;					// SD card data out 2 core

output			sd_wr_prct_out2core;				// SD card locked
output			sd_det_out2core;					// SD card inserted

input			ms_ip_en_fromcore;					// MS IO enable
input			ms_clk_fromcore;					// MS clock
input			ms_pwr_ctrl_fromcore;				// MS power control from core
input			ms_bs_fromcore;						// MS bus status from core

input			ms_sdio_fromcore;					// MS sdio from core
input			ms_sdio_oe_fromcore_;				// MS sdio oe# from core
output			ms_sdio_out2core;					// MS sdio out 2 core

output			ms_ins_out2core;					// MS card insert indicated signal

output			sd_ms_bist_tri_out2core;			// SD/MS bist test trigger
input			sd_ms_bist_finish_fromcore;			// SD/MS bist finish
input			sd_ms_bist_vec_fromcore;			// SD/MS bist result vector

output			sd_clk_out2pad;
output			sd_cmd_out2pad;					// SD card command/status from core
output			sd_cmd_oe_out2pad_;				// SD card command/status oe# from core
input			sd_cmd_frompad;					// SD card command/status out 2 core

output	[3:0]	sd_data_out2pad;					// SD card data from core
output	[3:0]	sd_data_oe_out2pad_;				// SD card data oe# from core
input	[3:0]	sd_data_frompad;					// SD card data out 2 core

input			sd_wr_prct_frompad;				// SD card locked
input			sd_det_frompad;					// SD card inserted

output			ms_ip_en_out2pad;					// MS IO enable
output			ms_clk_out2pad;					// MS clock
output			ms_pwr_ctrl_out2pad;				// MS power control from core
output			ms_bs_out2pad;						// MS bus status from core

output			ms_sdio_out2pad;					// MS sdio from core
output			ms_sdio_oe_out2pad_;				// MS sdio oe# from core
input			ms_sdio_frompad;					// MS sdio out 2 core

output			ms_ins_frompad;					// MS card insert indicated signal

//GI Interface
		//system (6)
input		gc_vid_clk_fromcore		;
input		gc_sys_clk_fromcore0    ;
input		gc_sys_clk_fromcore1    ;
output		alt_boot_out2core       ;
output		test_ena_out2core       ;
input		gc_rst_fromcore         ;
		//serial eeproom interface
input		i2c_clk_fromcore		;
output		i2c_din_out2core        ;
input		i2c_dout_fromcore       ;
input		i2c_doe_fromcore        ;
		//disc interface
output[7:0] di_in_out2core			;
input [7:0]	di_out_fromcore         ;
input [7:0]	di_oe_fromcore          ;
output		di_brk_in_out2core      ;
input		di_brk_out_fromcore     ;
input		di_brk_oe_fromcore      ;
output		di_dir_out2core         ;
output		di_hstrb_out2core       ;
input		di_dstrb_fromcore       ;
input		di_err_fromcore         ;
input		di_cover_fromcore       ;
output		di_stb_out2core			;

output		ais_clk_out2core		;
output		ais_lr_out2core         ;
input		ais_d_fromcore          ;

		//connect to pad
		//system (6)
output		gc_vid_clk_out2pad		;
output		gc_sys_clk_out2pad0		;
output		gc_sys_clk_out2pad1		;
input		alt_boot_frompad		;
input		test_ena_frompad		;
output		gc_rst_out2pad			;
//serial eeproom interface
output		i2c_clk_out2pad			;
input		i2c_din_frompad			;
output		i2c_dout_out2pad		;
output		i2c_doe_out2pad			;
		//disc interface
input [7:0] di_in_frompad			;
output[7:0]	di_out_out2pad			;
output[7:0]	di_oe_out2pad			;
input		di_brk_in_frompad		;
output		di_brk_out_out2pad		;
output		di_brk_oe_out2pad		;
input		di_dir_frompad			;
input		di_hstrb_frompad		;
output		di_dstrb_out2pad		;
output		di_err_out2pad			;
output		di_cover_out2pad		;
input		di_stb_frompad			;

input		ais_clk_frompad			;
input		ais_lr_frompad			;
output		ais_d_out2pad			;

//------SERVO
//input			sv_gpio_en_fromcore;
input	[2:0]	sv_gpio_fromcore;
input	[2:0]	sv_gpio_oe_fromcore_;
output	[2:0]	sv_gpio_out2core;

input	[15:0]	sv_atadd_fromcore;
input			sv_ataiordy_fromcore;
input			sv_ataintrq_fromcore;
input			sv_atadmarq_fromcore;
input			sv_ataiocs16_fromcore_;
input			sv_atapdiag_fromcore_;
input			sv_atadasp_fromcore_;
input	[44:0]	sv_tst_pin_fromcore;
input	[36:0]	sram_ifx_fromcore;

input	[15:0]	sv_atadd_oe_fromcore_;
input			sv_ataiocs16_oe_fromcore_;
input			sv_atapdiag_oe_fromcore_;
input			sv_atadasp_oe_fromcore_;
input	[36:0]	sram_ifx_oe_fromcore_;

output	[36:0]	sram_ifx_out2core;

output	[15:0]	sv_atadd_out2core;
output	[2:0]	sv_atada_out2core;
output			sv_atacs0_out2core_;
output			sv_atacs1_out2core_;
output			sv_atadior_out2core_;
output			sv_atadiow_out2core_;
output			sv_atadmack_out2core_;
output			sv_atareset_out2core_;
output			sv_atapdiag_out2core_;
output			sv_atadasp_out2core_;

output	[22:0]	ext_up_out2core;					// Servo external up interface
input	[22:0]	ext_up_fromcore;					// Servo up output data
input	[22:0]	ext_up_oe_fromcore_;				// Servo up output data enable

input			sv_tplck_fromcore;
input			sv_tslrf_fromcore;

output			pmsvd_trcclk_out2core;
output			pmsvd_trfclk_out2core;
output			pmsvd_tplck_out2core;
output			pmsvd_tslrf_out2core;
output			pmsvd_c2ftstin_0_out2core;
output			pmsvd_c2ftstin_1_out2core;

output	[5:0]	sv_tst_addata_out2core;				// Servo External A/D data input
input			sv_reg_rctst_2_fromcore;			// Servo RC test mode enable

output			xsflag_0_out2pad;
output			xsflag_1_out2pad;
output			xsflag_0_oe_out2pad_;
output			xsflag_1_oe_out2pad_;
input			xsflag_0_frompad;
input			xsflag_1_frompad;
input			xsflag_0_fromcore;
input			xsflag_1_fromcore;
input			xsflag_0_oe_fromcore_;
input			xsflag_1_oe_fromcore_;

//	SERVO control
input			sv_tst_pin_en_fromcore;			// SERVO test pin enable
input			sv_ide_mode_part0_en_fromcore;	// ide mode SERVO test pin part 0 enable
input			sv_ide_mode_part1_en_fromcore;	// ide mode SERVO test pin part 1 enable
input			sv_ide_mode_part2_en_fromcore;	// ide mode SERVO test pin part 2 enable
input			sv_ide_mode_part3_en_fromcore;	// ide mode SERVO test pin part 3 enable
input			sv_c2ftstin_en_fromcore;		// SERVO c2 flag enable
input			sv_trg_plcken_fromcore;			// SERVO tslrf and tplck enable
input			sv_pmsvd_trcclk_trfclk_en_fromcore;		// SERVO pmsvd_trcclk, pmsvd_trfclk enable

//	Control signals
input			hv_sync_en_fromcore;			// h_sync_, v_sync_ enable on the xsflag[1:0] pins
input			i2si_clk_en_fromcore;			// I2SI clock enable
input			scb_en_fromcore;				// SCB interface enable on the gpio[6:5] pins
input			sv_gpio_en_fromcore;			// servo_gpio[2:0] enable on the gpio[2:0] pins
input			ext_ide_device_en_fromcore;		// external IDE device enable
input			vdata_en_fromcore;				// video data enable on sdram [23:16]
input			sync_source_sel_fromcore;		// sync signal source select, 0 -> TVENC, 1 -> DE.
input			pci_gntb_en_fromcore;			// pci GNTB enable
input			video_in_intf_en_fromcore;		// video in interface enable
input			pci_mode_ide_en_fromcore;		// pci_mode ide interface enable
input			ram_cs1_en_fromcore;			// sdram cs1# enable
input			ata_share_mode_2_en_fromcore;	// ATA interface sharing mode 2 enable
input			xsflag_gpio_en_fromcore;		// gpio on xsflag[1:0] port enable
input			spdif_gpio_en_fromcore;			// Spdif share with gpio[21] en
input			i2so_data12_gpio_en_fromcore;	// gpio on i2so_data1, i2so_data2 enable
input			i2so_data3_gpio_en_fromcore;	// gpio on i2so_data3 enable
input			uart_gpio_en_fromcore;			// gpio on uart enable

input			ide_active_fromcore;			// ide interface is active in the pci mode
input			ata_ad_1413_sel_fromcore;		// ide interface AD[14:13] source select
input			hv_sync_gpio_en_fromcore;		// HV_SYNC signal on the GPIO[11:10] enable
input	        ice_probe_en_fromcore;			// ICE probe enable. When disabled, the EJTAG signals will
        										// be used as the GPIO[20:16]
output			dsp_bist_tri_out2core;
input			dsp_bist_finish_fromcore;
input	[2:0]	dsp_bist_vec_fromcore;

output			video_bist_tri_out2core;
input			video_bist_finish_fromcore;
input	[11:0]	video_bist_vec_fromcore;

input			combo_mode;
input			pci_mode;
input			ide_mode;
input			tvenc_test_mode;
input			servo_sram_mode;
input			servo_test_mode;
input			servo_only_mode;
input           rom_en_fromcore_;

input			cpu_pll_fck_fromcore;
input			cpu_pll_lock_fromcore;
input			mem_pll_fck_fromcore;
input			mem_pll_lock_fromcore;
input			audio_pll_fck_fromcore;
input			audio_pll_lock_fromcore;
input			f27_pll_fck_fromcore;
input			f27_pll_lock_fromcore;

input	        ide_clk_fromcore;		//	ide clk output for tester
input	        dsp_clk_fromcore;		// 	dsp clk output for tester
input	        video_clk_fromcore;		//	video clk output for tester

input			test_mode_frompad;		//	scan_test or bist_test mode active
input	        scan_en_frompad;		//	scan enable from pad
output			scan_en_out2core;		//	scan enable out to core
output	        scan_chain_sel_out2core;//	scan chain select out to core
input	[47:0]	scan_dout_fromcore;		//	scan out data from core
output	[47:0]	scan_din_out2core;		//	scan in data to core


//output          inta_out2core_;
input	[2:0]	bond_option_frompad;	//	Three bond option pad

output			bist_test_mode;
output			scan_test_mode;

//------GPIO(0~13, 28~24)
        //connect to pad
output  [31:0]   gpio_out2pad;
output  [31:0]   gpio_oe_out2pad_;
input   [31:0]   gpio_frompad;
        //connect to core
output  [31:0]  gpio_out2core;
input   [31:0]  gpio_fromcore;
input   [31:0]  gpio_oe_fromcore_;
//------GPIO Set 2
input	[31:0]	gpio2_fromcore;			// GPIO Set 2 from core
input	[31:0]	gpio2_oe_fromcore_;		// GPIO Set 2 oe from core
output	[31:0]	gpio2_out2core;			// GPIO Set 2 out 2 core

//oe signal, 0 output, 1 input
parameter	SIGNAL_OUTPUT_ENABLE_	= 1'b0;
parameter	SIGNAL_INPUT_ENABLE		= 1'b1;

//test mode, for bist and scan test
//test mode
assign	scan_test_mode		  = test_mode_frompad;
assign	bist_test_mode		  = test_mode_frompad;
//rom_mode, in this mode, external flash interface is enabled
wire	rom_mode			  = ( !rom_en_fromcore_);

//strap
assign  strap_pin_out[31:0]   =
		{1'b0, ram_dq_frompad[15:0], ram_dqm_frompad[0], ram_ba_frompad[1:0], ram_addr_frompad[11:0]};

//================ SDRAM & rom ================================
	//	programmble delay chain for sdram read clock
	P_DLY_CHAIN32 mem_clk_rd_chain(
						.A			(mem_clk_in		),
						.Y			(mem_rd_dly_out	),
						.YN			(),
						.DLY_SEL	(mem_rd_dly_sel	)
						);

//=============================================================================
	//	delay chain for performance test
	//	Add one more test delay chain in PAD_MISC. They use the same delay level
	//	input but they will be put on different side
	//=============================================================================
	TEST_DLY_CHAIN64	TEST_DLY_CHAIN64(
		.CK			(mem_clk_in				),	// input clock source
		.FLAG		(test_dly_chain_flag[0]	),	// Delay chain output flag
		.DLY_SEL	(test_dly_chain_sel		)	// delay select
		);

	TEST_DLY_CHAIN64	TEST_DLY_CHAIN64_2(
		.CK			(mem_clk_in				),	// input clock source
		.FLAG		(test_dly_chain_flag[1]	),	// Delay chain output flag
		.DLY_SEL	(test_dly_chain_sel		)	// delay select
		);

//================= define the singals/data type =================
//----SDRAM & FLASH //32
reg     [31:0]    ram_dq_out2core_latch;

always  @(posedge mem_rd_dly_out or negedge rst_)
        if (~rst_)
                ram_dq_out2core_latch   <= #udly        32'h0;
        else
                ram_dq_out2core_latch   <= #udly        ram_dq_frompad;

assign	ram_dq_out2core = ram_dq_out2core_latch ;

assign	ram_dq_out2pad  = ram_dq_fromcore;
/*=============================================================================*/

//output signals
assign	ram_ba_out2pad		=	ram_ba_fromcore;    //2
assign	ram_addr_out2pad	=	ram_addr_fromcore;	//12
assign	ram_cas_out2pad_	=	ram_cas_fromcore_;	//1
assign	ram_cs_out2pad_		=	ram_cs_fromcore_;	//1
assign	ram_ras_out2pad_	=	ram_ras_fromcore_;  //1
assign	ram_we_out2pad_		=	ram_we_fromcore_;	//1
assign	ram_dqm_out2pad		=	ram_dqm_fromcore;  //4

//oe signals
assign	ram_addr_oe_out2pad_  = (!rst_) ? 12'hfff : 12'h0;
assign	ram_ba_oe_out2pad_	  = (!rst_) ? 2'h3	  : 2'h0;
assign	ram_dq_oe_out2pad_	  = (!rst_) ? 32'hffffffff : ram_dq_oe_fromcore_;
assign  ram_dqm_oe_out2pad_ 	  = (!rst_) ? 4'hf : 4'h0;


//====================pci inteface===============================
//output signals
assign	pci_clk_out2pad		=	pci_clk_fromcore;
assign	pci_gnta_out2pad_	=	pci_gnta_fromcore_;
assign	pci_gntb_out2pad_	=	pci_gntb_fromcore_;
assign	pci_rst_out2pad_	=	pci_rst_fromcore_;

//I/O signals
assign	pci_frame_out2pad_		=	pci_frame_fromcore_;
assign	pci_frame_oe_out2pad_	=	pci_frame_oe_fromcore_;
assign	pci_frame_out2core_		=	pci_frame_frompad_;
//assign	pci_frame_out2core_		=	pci_frame_fromcore_;

assign	pci_irdy_out2pad_		=	pci_irdy_fromcore_;
assign	pci_irdy_oe_out2pad_	=	pci_irdy_oe_fromcore_;
assign	pci_irdy_out2core_		=	pci_irdy_frompad_;
//assign	pci_irdy_out2core_		=	pci_irdy_fromcore_;

assign	pci_trdy_out2pad_		=	pci_trdy_fromcore_;
assign	pci_trdy_oe_out2pad_	=	pci_trdy_oe_fromcore_;
assign	pci_trdy_out2core_		=	pci_trdy_frompad_;
//assign	pci_trdy_out2core_		=	pci_trdy_fromcore_;

assign	pci_stop_out2pad_		=	pci_stop_fromcore_;
assign	pci_stop_oe_out2pad_	=	pci_stop_oe_fromcore_;
assign	pci_stop_out2core_		=	pci_stop_frompad_;
//assign	pci_stop_out2core_		=	pci_stop_fromcore_;


assign	pci_devsel_out2pad_		=	pci_devsel_fromcore_;
assign	pci_devsel_oe_out2pad_	=	pci_devsel_oe_fromcore_;
assign	pci_devsel_out2core_	=	pci_devsel_frompad_;
//assign	pci_devsel_out2core_	=	pci_devsel_fromcore_;

//assign	pci_devsel_out2pad_		=	pci_devsel_fromcore_;
//assign	pci_devsel_oe_out2pad_	=	pci_devsel_oe_fromcore_;
//assign	pci_devsel_out2core_	=	pci_devsel_frompad_;

assign	pci_ad_out2pad		=	pci_ad_fromcore;
assign	pci_ad_oe_out2pad_	=	pci_ad_oe_fromcore_;
assign	pci_ad_out2core		=   pci_ad_frompad;
//assign	pci_ad_out2core		=	pci_ad_fromcore;

assign	pci_par_out2pad		=	pci_par_fromcore;
assign	pci_par_oe_out2pad_	=	pci_par_oe_fromcore_;
assign	pci_par_out2core	=	pci_par_frompad;
//assign	pci_par_out2core	=	pci_par_fromcore;

assign	pci_cbe_out2pad_	=	pci_cbe_fromcore_;
assign	pci_cbe_oe_out2pad_	=	pci_cbe_oe_fromcore_;
assign	pci_cbe_out2core_	=	pci_cbe_frompad_;
//assign	pci_cbe_out2core_	=	pci_cbe_fromcore_;

//input signals
assign	pci_req_out2core_[1]		=   1'b0;
assign	pci_req_out2core_[0]		=	pci_req_frompad_;
assign	pci_inta_out2core_		=	pci_inta_frompad_;
//serr and perr
assign	pci_serr_out2core_		=	pci_serr_frompad_;
assign	pci_serr_oe_out2pad_	=   pci_serr_oe_fromcore_;
assign  pci_serr_out2pad_		=   pci_serr_fromcore_;

assign	pci_perr_out2core_		=	pci_perr_frompad_;
assign	pci_perr_oe_out2pad_	=   pci_perr_oe_fromcore_;
assign  pci_perr_out2pad_		=   pci_perr_fromcore_;
//assign	pci_req_out2core_		=	1'b1;
//assign	pci_inta_out2core_		=	1'b1;
//==================flash interface====================
//assign	rom_ce_out2pad_			=	rom_ce_fromcore_;
assign		rom_oe_out2pad_			=	rom_oe_fromcore_;
assign		rom_we_out2pad_			=	rom_we_fromcore_;

assign		rom_data_out2pad		=	rom_wdata_fromcore;
assign		rom_data_oe_out2pad_	=	rom_data_oe_fromcore_;
assign		rom_rdata_out2core[7:0]	=	rom_data_frompad;
assign		rom_rdata_out2core[15:8]= 	rom_addr_frompad[7:0];
assign		rom_addr_out2pad		=	rom_addr_fromcore;
assign		rom_addr_oe_out2pad_	=	~rom_addr_oe_fromcore;
assign		rom_ale_out2pad			=	rom_ale_fromcore;

//	CPU
	//==========================================================================
assign	cpu_sysnmi_out2core_	= 1'b1;

assign	cpu_sysint_out2core_[0]	= 1'b1;
assign	cpu_sysint_out2core_[1]	= ~cpu_extint_fromcore;
assign	cpu_sysint_out2core_[2]	= 1'b1;
assign	cpu_sysint_out2core_[3]	= 1'b1;
assign	cpu_sysint_out2core_[4]	= 1'b1;

assign	j_tdo_out2pad			=	j_tdo_fromcore;
assign	j_tdi_out2core			=	j_tdi_frompad;
assign	j_tms_out2core			=	j_tms_frompad;
assign	j_tclk_out2core			=	j_tclk_frompad;
assign	j_trst_out2core_		=	j_trst_frompad_;

//ide interface====================================================
//output signals
assign	atadiow_out2pad_		=	atadiow_fromcore_;
assign	atadior_out2pad_		=	atadior_fromcore_;
assign	atacs0_out2pad_			=	atacs0_fromcore_;
assign	atacs1_out2pad_			=	atacs1_fromcore_;
assign	atada_out2pad			=	atada_fromcore;
assign	atadmack_out2pad_		=	atadmack_fromcore_;
assign	atareset_out2pad_		=	atareset_fromcore_;
assign	atareset_oe_out2pad_	=	atareset_oe_fromcore_;

//inout signals
assign	atadd_out2pad			=	atadd_fromcore;
assign	atadd_oe_out2pad_		=	atadd_oe_fromcore_;
assign	atadd_out2core			=	atadd_frompad;

//input signals
assign	ataiordy_out2core		=	ataiordy_frompad;
assign	ataintrq_out2core		=	ataintrq_frompad;
assign	atadmarq_out2core		=	atadmarq_frompad;

//south bridge signals
//ir
assign	irrx_out2core			=	irrx_frompad;
//uart
assign	uart_tx_out2pad			=	uart_tx_fromcore;
assign	uart_rx_out2core		=	uart_rx_frompad;
//i2c
assign	scbsda_out2pad			=	scbsda_fromcore;
assign	scbsda_oe_out2pad_		=  ~scbsda_oe_fromcore;
assign	scbsda_out2core			=	scbsda_frompad;

assign	scbscl_out2pad			=	scbscl_fromcore;
assign	scbscl_oe_out2pad_		=  ~scbscl_oe_fromcore;
assign	scbscl_out2core			=	scbscl_frompad;

//==============audio interface=======================
//output signals
assign	i2so_data0_out2pad		=	i2so_data0_fromcore;
assign	i2so_data1_out2pad		=	i2so_data1_fromcore;
assign	i2so_data2_out2pad		=	i2so_data2_fromcore;
assign	i2so_data3_out2pad		=	i2so_data3_fromcore;
assign	i2so_bck_out2pad		=	i2so_bck_fromcore;
assign	i2so_lrck_out2pad		=	i2so_lrck_fromcore;
assign	i2si_mclk_out2pad		=	i2si_mclk_fromcore;
assign	i2si_bck_out2pad		=	i2si_bck_fromcore;
assign	i2si_lrck_out2pad		=	i2si_lrck_fromcore;
//inout signals
assign	i2so_mclk_out2pad		=	i2so_mclk_fromcore;
assign	i2so_mclk_oe_out2pad_	=	i2so_mclk_oe_fromcore_;
assign	i2so_mclk_out2core		=	i2so_mclk_frompad;
assign	i2si_data_out2core		=	i2si_data_frompad;

//spdif interface
//output signals
assign	spdif_out2pad			=	spdif_fromcore;
//input signals
assign	spdif_in_data_out2core	=	spdif_in_data_frompad;

//==========video interface======================
//video in
assign	video_in_pixel_clk_out2core		= video_in_pixel_clk_frompad;
assign	video_in_pixel_data_out2core	= video_in_pixel_data_frompad;
assign	video_in_hsync_out2core_		= video_in_hsync_frompad_;
assign	video_in_vsync_out2core_		= video_in_vsync_frompad_;
//tv encoder
assign	tvenc_h_sync_out2pad_			= tvenc_h_sync_fromcore_;
assign	tvenc_v_sync_out2pad_			= tvenc_v_sync_fromcore_;

//digital video out
assign	vdata_out2pad 					= vdata_fromcore;
assign	de_h_sync_out2pad_		        = de_h_sync_fromcore_;
assign	de_v_sync_out2pad_		        = de_v_sync_fromcore_;
assign	pix_clk_out2pad                 = pix_clk_fromcore;

//=============usb interface========================
assign	usbpon_out2pad					= usbpon_fromcore;
assign	usbovc_out2core					= usbovc_frompad;

//gpio
assign	gpio_out2pad		= gpio_fromcore;
assign	gpio_oe_out2pad_	= gpio_oe_fromcore_;
assign	gpio_out2core		= gpio_frompad;


//no use wires
wire	h_sync_fromcore_;
wire	v_sync_fromcore_;

//ms_sd interfacesd_clk_out2pad;
assign	sd_clk_out2pad			=	sd_clk_fromcore;
assign	sd_cmd_out2pad			=	sd_cmd_fromcore;
assign	sd_cmd_oe_out2pad_		=	sd_cmd_oe_fromcore_;
assign	sd_cmd_out2core			=	sd_cmd_frompad;

assign	sd_data_out2pad			=	sd_data_fromcore;
assign	sd_data_oe_out2pad_		=	sd_data_oe_fromcore_;
assign	sd_data_out2core		=	sd_data_frompad;

//assign	sd_wr_prct_out2core		=	sd_wr_prct_frompad;
assign	sd_wr_prct_out2core		= scan_test_mode ? (sd_cmd_fromcore & sd_cmd_oe_fromcore_) :
										(sd_ip_en_fromcore & gpio_frompad[3]);

assign	sd_det_out2core			=	sd_det_frompad;

assign	ms_clk_out2pad			=	ms_clk_fromcore;
assign	ms_pwr_ctrl_out2pad		=	ms_pwr_ctrl_fromcore;
assign	ms_bs_out2pad			=	ms_bs_fromcore;

assign	ms_sdio_out2pad			=	ms_sdio_fromcore;
assign	ms_sdio_oe_out2pad_		=	ms_sdio_oe_fromcore_;
assign	ms_sdio_out2core		=	ms_sdio_frompad;

assign	ms_ins_out2core			=	ms_ins_frompad;

//GI Interface(from broadon)
assign	gc_vid_clk_out2pad		=   gc_vid_clk_fromcore;
assign	gc_sys_clk_out2pad0		=	gc_sys_clk_fromcore0;
assign	gc_sys_clk_out2pad1		=	gc_sys_clk_fromcore1;
assign	alt_boot_out2core		=	alt_boot_frompad;
assign	test_ena_out2core		=	test_ena_frompad;
assign	gc_rst_out2pad			=	gc_rst_fromcore;
//serial eeproom interface
assign	i2c_clk_out2pad			=	i2c_clk_fromcore;
assign	i2c_din_out2core		=	i2c_din_frompad;
assign	i2c_dout_out2pad		=	i2c_dout_fromcore;
assign	i2c_doe_out2pad			=	i2c_doe_fromcore;
		//disc interface
assign	di_in_out2core			=	di_in_frompad		;
assign	di_out_out2pad			=	di_out_fromcore		;
assign	di_oe_out2pad			=	di_oe_fromcore		;
assign	di_brk_in_out2core		=	di_brk_in_frompad	;
assign	di_brk_out_out2pad		=	di_brk_out_fromcore	;
assign	di_brk_oe_out2pad		=	di_brk_oe_fromcore	;
assign	di_dir_out2core			=	di_dir_frompad		;
assign	di_hstrb_out2core		=	di_hstrb_frompad	;
assign	di_dstrb_out2pad		=	di_dstrb_fromcore	;
assign	di_err_out2pad			=	di_err_fromcore		;
assign	di_cover_out2pad		=	di_cover_fromcore	;
assign	di_stb_out2core			=	di_stb_frompad		;

assign	ais_clk_out2core		=	ais_clk_frompad		;
assign	ais_lr_out2core			=	ais_lr_frompad      ;
assign	ais_d_out2pad			=	ais_d_fromcore      ;

//for test mode
assign	cpu_scan_test_mode		= 1'b0;
assign	cpu_bist_tri_out2core	= 1'b0;
assign	video_bist_tri_out2core	= 1'b0;
assign	dsp_bist_tri_out2core	= 1'b0;
assign	sd_ms_bist_tri_out2core	= 1'b0;




endmodule





















































