/******************************************************************************
	  (c) copyrights 2000-. All rights reserved

	   T-Square Design, Inc.

	  Company confidential and Properietary	information.
	  This information may not be disclosed	to unauthorized individual.
*******************************************************************************/

/******************************************************************************
 *
 *    DESCRIPTION:	clock divider in clock generator
 *
 *    AUTHOR:		norman
 *
 *    HISTORY: 		2002.03.10 initial version
 					2002-07-15	change div_src_23 from 33% duty cycle to 66% duty
 								cycle by invert.
 					2002-07-16	remove input mem_fs, output div_mem_clk
 								The 1 : 1.5 divider is still kept
 					2002-09-05	Add one option 1:1.5 option to GPU_CLK
 								Add DSP source clock selection between 2/3 or 1/2
 								cpu clock.
 								DSP clock programmable step changed from 1/32 to
 								1/16 DSP source clock.
 					2002-10-08	Remove PC trace clock
 					2003-11-10	Update the video clock when configuration set to '11'
 					2003-12-18	Change the PCI:CPU frequency ratio item 1:5 to 1:3
 					2004-02-05	Remove the div_cpu_clk output port that is the
 								input equal to output directly.


 *****************************************************************************/

module clock_divider(

	// input
	pci_mips_fs,	// select the divisor of PCI/MIPS bus clock
	ide_fs,			// ide clock frequency select
	dsp_fs,			// dsp clock frequency select
	dsp_div_src_sel,// dsp source clock select
	pci_fs,			// pci clock frequency select
	ve_fs,			// ve clock frequency select
	ve_div_src_sel,	// ve source clock select
	div_clk_src,	// source clock
	mem_clk_src,	// memory clock from the mem_clk_pll
	f54m_clk_src,	//54MHz clock

	//output
//	div_cpu_clk,	// output CPU clock
	div_dsp_clk,	// output dsp clock
	div_ve_clk,		// output video engine clock
	div_pci_m_clk,	// output PCI/MIPS bus clock
	div_servo_clk,	// output SERVO clock
	div_ide_clk,	// output IDE clock
	div_gi_clk,
	rst_
	);

//input	[1:0]	pci_mips_fs;
input	[1:0]	pci_mips_fs;	// select the divisor of PCI/MIPS bus clock
input	[1:0]   ide_fs;			//ide clock frequency select
input	[1:0]	dsp_fs;			// dsp clock frequency select
input			dsp_div_src_sel;// dsp source clock select
input	[1:0]	pci_fs;			// pci clock frequency select
input	[1:0]	ve_fs;			// ve clock frequency select
input			ve_div_src_sel;	// ve source clock select

input			div_clk_src;
input			mem_clk_src;	// memory clock from the mem_clk_pll
input			f54m_clk_src;	// 54MHz clock

//output		div_cpu_clk,
output		div_pci_m_clk,
			div_dsp_clk,
			div_ve_clk;
output		div_servo_clk,		// output SERVO clock
			div_ide_clk,
			div_gi_clk;
input		rst_;

parameter	udly = 0.3;
reg			div_servo_clk;

	/*=========================================================================
		Divide input clock to get 1:1, 1:1.5, 1:2, 1:3, 1:4, 1:8
		clocks
		clk_src_14 is generated by clk_src_12,
		clk_src_18 is generated by clk_src_14
		other clock is generated by div_clk_src_
	=========================================================================*/
	wire	div_clk_src_	= !div_clk_src;		// inverted input clock

	wire	clk_src_11	= div_clk_src;			// 1:1 sub clock
	wire	clk_src_23;							// 2:3 sub clock
	reg		clk_src23_flag;

	reg		clk_src_12;							// 1:2 sub clock
	reg		clk_src_13;							// 1:3 sub clock
	reg		clk_src_14;							// 1:4 sub clock
	reg		clk_src_15;							// 1:5 sub clock
	reg		clk_src_16;							// 1:6 sub clock
	reg		clk_src_18;							// 1:8 sub clock

	reg	[1:0]	clk_src3_cnt;					// counter 3 of clk_src
	reg	[2:0]	clk_src5_cnt;					// counter 5 of clk_src
	reg	[2:0]	clk_src6_cnt;					// counter 6 of clk_src

	// clk_src3_cnt
	always @ (posedge div_clk_src or negedge rst_)
	   if (!rst_)
	      clk_src3_cnt <= #udly 0;
	   else if (clk_src3_cnt == 2'b10 | clk_src3_cnt == 2'b11)
	      clk_src3_cnt <= #udly 0;
	   else
	      clk_src3_cnt <= #udly clk_src3_cnt + 2'b1;

	//clk_src5_cnt
	always @ (posedge div_clk_src or negedge rst_)
	   if (!rst_)
	      clk_src5_cnt <= #udly 0;
	   else if (clk_src5_cnt == 3'b100 | clk_src5_cnt == 3'b101
	   		 | clk_src5_cnt == 3'b110 | clk_src5_cnt == 3'b111)
	      clk_src5_cnt <= #udly 0;
	   else
	      clk_src5_cnt <= #udly clk_src5_cnt + 3'b1;

	//clk_src6_cnt
	always @ (posedge div_clk_src or negedge rst_)
	   if (!rst_)
	      clk_src6_cnt <= #udly 0;
	   else if (clk_src6_cnt == 3'b101 | clk_src6_cnt == 3'b110 | clk_src6_cnt == 3'b111)
	      clk_src6_cnt <= #udly 0;
	   else
	      clk_src6_cnt <= #udly clk_src6_cnt + 3'b1;

	always @ (posedge div_clk_src_ or negedge rst_)
	   if (!rst_)
	      clk_src23_flag <= #udly 0;
	   else if (clk_src3_cnt == 2'b10)
	      clk_src23_flag <= #udly 1;
	   else if (clk_src3_cnt == 2'b00)
	      clk_src23_flag <= #udly 0;

	assign	clk_src_23	= (clk_src3_cnt == 2'b00) & clk_src23_flag & div_clk_src |
						  (clk_src3_cnt == 2'b01) & !clk_src23_flag & div_clk_src_;

	// clk_src_12
	always @ (posedge div_clk_src or negedge rst_)
	   if (!rst_)
	      clk_src_12 <= #udly 0;
	   else
	      clk_src_12 <= #udly !clk_src_12;

	// clk_src_13
	always @ (posedge div_clk_src or negedge rst_)
	   if (!rst_)
	      clk_src_13 <= #udly 0;
	   else if (clk_src3_cnt == 2'b10)			// clk_src_13 will be high when cnt3 is 0
	      clk_src_13 <= #udly 1;
	   else
	      clk_src_13 <= #udly 0;

	// clk_src_14
	always @ (posedge clk_src_12 or negedge rst_)
	   if (!rst_)
	      clk_src_14 <= #udly 0;
	   else
	      clk_src_14 <= #udly !clk_src_14;

	// clk_src_15
	always @ (posedge div_clk_src or negedge rst_)
	   if (!rst_)
	      clk_src_15 <= #udly 0;
	   else if (clk_src5_cnt == 3'b000 |
	   			clk_src5_cnt == 3'b001)			// clk_src_15 will be high after cnt5 is 0, 1
	      clk_src_15 <= #udly 1;
	   else
	      clk_src_15 <= #udly 0;

	// clk_src_16
	always @ (posedge div_clk_src or negedge rst_)
	   if (!rst_)
	      clk_src_16 <= #udly 0;
	   else if (clk_src6_cnt == 3'b000 |
	   			clk_src6_cnt == 3'b001 |
	   			clk_src6_cnt == 3'b010)			// clk_src_16 will be high after cnt6 is 0, 1, 2
	      clk_src_16 <= #udly 1;
	   else
	      clk_src_16 <= #udly 0;

	// clk_src_18
	always @ (posedge clk_src_14 or negedge rst_)
	   if (!rst_)
	      clk_src_18 <= #udly 0;
	   else
	      clk_src_18 <= #udly !clk_src_18;

	/*=========================================================================
		cpu_clk
		CPU clock is the output from CPU_PLL output and no divider is added
		for cpu clock in this module.
	=========================================================================*/
//	assign	div_cpu_clk = clk_src_11;

	/*=========================================================================
		Divide mem input clock to get 1:1, 1:1.5, 1:2, 1:3, 1:4, 1:8
		clocks
		mem_clk_src_14 is generated by mem_clk_src_12,
		mem_clk_src_18 is generated by mem_clk_src_14
		other clock is generated by mem_clk_src_
	=========================================================================*/
	wire	mem_clk_src_	= !mem_clk_src;		// inverted mem input clock

	wire	mem_clk_src_11	= mem_clk_src;			// 1:1 sub clock
	wire	mem_clk_src_23;							// 2:3 sub clock
	reg		mem_clk_src23_flag;

	reg		mem_clk_src_12;							// 1:2 sub clock
	reg		mem_clk_src_13;							// 1:3 sub clock
	reg		mem_clk_src_14;							// 1:4 sub clock
	reg		mem_clk_src_15;							// 1:5 sub clock
	reg		mem_clk_src_16;							// 1:6 sub clock
	reg		mem_clk_src_18;							// 1:8 sub clock

	reg	[1:0]	mem_clk_src3_cnt;					// counter 3 of mem_clk_src
	reg	[2:0]	mem_clk_src5_cnt;					// counter 5 of mem_clk_src
	reg	[2:0]	mem_clk_src6_cnt;					// counter 6 of mem_clk_src

	// mem_clk_src3_cnt
	always @ (posedge mem_clk_src or negedge rst_)
	   if (!rst_)
	      mem_clk_src3_cnt <= #udly 0;
	   else if (mem_clk_src3_cnt == 2'b10 | mem_clk_src3_cnt == 2'b11)
	      mem_clk_src3_cnt <= #udly 0;
	   else
	      mem_clk_src3_cnt <= #udly mem_clk_src3_cnt + 2'b1;

	//mem_clk_src5_cnt
	always @ (posedge mem_clk_src or negedge rst_)
	   if (!rst_)
	      mem_clk_src5_cnt <= #udly 0;
	   else if (mem_clk_src5_cnt == 3'b100 | mem_clk_src5_cnt == 3'b101
	   		 | mem_clk_src5_cnt == 3'b110 | mem_clk_src5_cnt == 3'b111)
	      mem_clk_src5_cnt <= #udly 0;
	   else
	      mem_clk_src5_cnt <= #udly mem_clk_src5_cnt + 3'b1;

	//mem_clk_src6_cnt
	always @ (posedge mem_clk_src or negedge rst_)
	   if (!rst_)
	      mem_clk_src6_cnt <= #udly 0;
	   else if (mem_clk_src6_cnt == 3'b101 | mem_clk_src6_cnt == 3'b110 | mem_clk_src6_cnt == 3'b111)
	      mem_clk_src6_cnt <= #udly 0;
	   else
	      mem_clk_src6_cnt <= #udly mem_clk_src6_cnt + 3'b1;

	always @ (posedge mem_clk_src_ or negedge rst_)
	   if (!rst_)
	      mem_clk_src23_flag <= #udly 0;
	   else if (mem_clk_src3_cnt == 2'b10)
	      mem_clk_src23_flag <= #udly 1;
	   else if (mem_clk_src3_cnt == 2'b00)
	      mem_clk_src23_flag <= #udly 0;

	assign	mem_clk_src_23	= (mem_clk_src3_cnt == 2'b00) & mem_clk_src23_flag & mem_clk_src |
						  (mem_clk_src3_cnt == 2'b01) & !mem_clk_src23_flag & mem_clk_src_;

	// mem_clk_src_12
	always @ (posedge mem_clk_src or negedge rst_)
	   if (!rst_)
	      mem_clk_src_12 <= #udly 0;
	   else
	      mem_clk_src_12 <= #udly !mem_clk_src_12;

	// mem_clk_src_13
	always @ (posedge mem_clk_src or negedge rst_)
	   if (!rst_)
	      mem_clk_src_13 <= #udly 0;
	   else if (mem_clk_src3_cnt == 2'b10)			// mem_clk_src_13 will be high when cnt3 is 0
	      mem_clk_src_13 <= #udly 1;
	   else
	      mem_clk_src_13 <= #udly 0;

	// mem_clk_src_14
	always @ (posedge mem_clk_src_12 or negedge rst_)
	   if (!rst_)
	      mem_clk_src_14 <= #udly 0;
	   else
	      mem_clk_src_14 <= #udly !mem_clk_src_14;

	// mem_clk_src_15
	always @ (posedge mem_clk_src or negedge rst_)
	   if (!rst_)
	      mem_clk_src_15 <= #udly 0;
	   else if (mem_clk_src5_cnt == 3'b000 |
	   			mem_clk_src5_cnt == 3'b001)			// mem_clk_src_15 will be high after cnt5 is 0, 1
	      mem_clk_src_15 <= #udly 1;
	   else
	      mem_clk_src_15 <= #udly 0;

	// mem_clk_src_16
	always @ (posedge mem_clk_src or negedge rst_)
	   if (!rst_)
	      mem_clk_src_16 <= #udly 0;
	   else if (mem_clk_src6_cnt == 3'b000 |
	   			mem_clk_src6_cnt == 3'b001 |
	   			mem_clk_src6_cnt == 3'b010)			// mem_clk_src_16 will be high after cnt6 is 0, 1, 2
	      mem_clk_src_16 <= #udly 1;
	   else
	      mem_clk_src_16 <= #udly 0;

	// mem_clk_src_18
	always @ (posedge mem_clk_src_14 or negedge rst_)
	   if (!rst_)
	      mem_clk_src_18 <= #udly 0;
	   else
	      mem_clk_src_18 <= #udly !mem_clk_src_18;

	/*=========================================================================
		mem_clk
		mem clock is the output from MEM_PLL output and no divider is added
		for mem clock in this module.
	=========================================================================*/
	wire	div_mem_clk = mem_clk_src_11;

	/*=========================================================================
		video engine clock
		The ve divider source clock could be selected between MEM clock or
		 CPU clock.
		video_fs	ve_clk : ve divider source clock
		2'b00		1:3
		2'b01		1:2
		2'b10		2:3
		2'b11		1:1
	=========================================================================*/
	//	ve clock divider source select
	wire	ve_clk_src_sel	= ve_div_src_sel;	// 1-> MEM clock, 0->  CPU clock
	wire	ve_clk_src_11	= ve_clk_src_sel ? mem_clk_src_11		: clk_src_11;
	wire	ve_clk_src_12	= ve_clk_src_sel ? mem_clk_src_12		: clk_src_12;
	wire	ve_clk_src_13	= ve_clk_src_sel ? mem_clk_src_13		: clk_src_13;
	wire	ve_clk_src_23	= ve_clk_src_sel ? mem_clk_src_23		: clk_src_23;


	assign	div_ve_clk = (ve_fs == 2'b00) & ve_clk_src_13 |
						 (ve_fs == 2'b01) & ve_clk_src_12 |
						 (ve_fs == 2'b10) & ve_clk_src_23 |
						 (ve_fs == 2'b11) & ve_clk_src_11;

	/*=========================================================================
		pci_mips_clk
		pci_fs		pci_mips_clk : cpu_clk
		00				1 : 8
		01				1 : 6
		10				1 : 4
		11				1 : 3
	=========================================================================*/
	assign	div_pci_m_clk	= (pci_fs == 2'b00) & clk_src_18 |
							  (pci_fs == 2'b01) & clk_src_16 |
							  (pci_fs == 2'b10) & clk_src_14 |
							  (pci_fs == 2'b11) & clk_src_13;

	/*=========================================================================
		servo clock
	=========================================================================*/
	always @ (posedge mem_clk_src or negedge rst_)
	   if (!rst_)
	      div_servo_clk	<= 0;
	   else
	      div_servo_clk <= !div_servo_clk;

	/*=========================================================================
		ide_clk
		IDE clock can be 1:4,1:3, 1:2, 54MHz CPU divider source clock
	=========================================================================*/
	assign	div_ide_clk	= 	(ide_fs == 2'b00) & f54m_clk_src|
							(ide_fs == 2'b01) & clk_src_14 	|
							(ide_fs == 2'b10) & clk_src_13 	|
							(ide_fs == 2'b11) & clk_src_12;

	/*=========================================================================
		dsp_clk
		The DSP divider source clock could be selected between MEM clock or
		 CPU clock.
		DSP clock can be 1:3, 1:2, 1:1.5 or 1:1 to  DSP divider source clock
	=========================================================================*/
	//	dsp clock divider source select
	wire	dsp_clk_src_sel	= dsp_div_src_sel;	// 1-> MEM clock, 0->  CPU clock
	wire	dsp_clk_src_11	= dsp_clk_src_sel ? mem_clk_src_11		: clk_src_11;
	wire	dsp_clk_src_12	= dsp_clk_src_sel ? mem_clk_src_12		: clk_src_12;
	wire	dsp_clk_src_13	= dsp_clk_src_sel ? mem_clk_src_13		: clk_src_13;
	wire	dsp_clk_src_23	= dsp_clk_src_sel ? mem_clk_src_23		: clk_src_23;


	assign	div_dsp_clk = 	(dsp_fs == 2'b00) & dsp_clk_src_13 |
						 	(dsp_fs == 2'b01) & dsp_clk_src_12 |
						 	(dsp_fs == 2'b10) & dsp_clk_src_23 |
						 	(dsp_fs == 2'b11) & dsp_clk_src_11;


wire	div_gi_clk = clk_src_12;
/*
	wire	dsp_clk_src_	= !dsp_clk_src;
	wire	dsp_clk_src_12_	= !dsp_clk_src_12;

	//	dsp clock mask part
	wire	dsp_grp1_en		= dsp_fs[3];
	wire	dsp_grp2_en		= !dsp_fs[3];

	reg		dsp_grp1_mask, dsp_grp2_mask;

	reg	[3:0]	dsp_grp1_cnt;		// count from 0 to 15
	reg	[3:0]	dsp_grp1_cnt2;		// count from 15 down to dsp_fs

	reg	[2:0]	dsp_grp2_cnt;		// count from 0 to 7
	reg	[2:0]	dsp_grp2_cnt2;		// count from 7 to dsp_fs
*/
	/*=========================================================================
		DSP clock group one 100 ~ 50 MHz
		The source is 1/2 cpu clock and useless clock pulse will be masked when
		the counter exceed the DSP fs setting.
	=========================================================================*/
/*	always @ (negedge rst_ or posedge dsp_clk_src)
	   if (!rst_)
	      dsp_grp1_cnt <= #udly 0;
	   else if (dsp_grp1_cnt == 4'b1111)
	      dsp_grp1_cnt <= #udly 0;
	   else
	      dsp_grp1_cnt <= #udly dsp_grp1_cnt + 1;

	// How many clock cycle has been masked.
	// count down from 4'b1111 to dsp_fs
	always @ (negedge rst_ or posedge dsp_clk_src)
	   if (!rst_)
	      dsp_grp1_cnt2 <= #udly 4'b1111;
	   else if (dsp_grp1_cnt == 4'b1111)		// set cnt2 to 4'hf when cnt1 grow to 1111
	      dsp_grp1_cnt2 <= #udly 4'b1111;
	   else if (dsp_grp1_mask & (dsp_grp1_cnt2 != dsp_fs[3:0]))
	      dsp_grp1_cnt2 <= #udly dsp_grp1_cnt2 - 1;

	// using clk_src_12 to generate the mask signal
	always @ (negedge rst_ or posedge dsp_clk_src_)
	   if (!rst_)
	      dsp_grp1_mask <= #udly 0;
	   else if ((dsp_grp1_cnt[0] == 1'b1) & (dsp_grp1_cnt2 != dsp_fs[3:0]))
	      dsp_grp1_mask <= #udly 1;
	   else
	      dsp_grp1_mask <= #udly 0;

	wire	dsp_grp1_clk	= dsp_clk_src & !dsp_grp1_mask;
*/
	/*=========================================================================
		DSP clock group two 50 ~ 25 MHz
		The source is 1/4 cpu clock and useless clock pulse will be masked when
		the counter exceed the DSP fs setting.
	=========================================================================*/
/*
	always @ (negedge rst_ or posedge dsp_clk_src_12)
	   if (!rst_)
	      dsp_grp2_cnt <= #udly 0;
	   else if (dsp_grp2_cnt == 3'b111)
	      dsp_grp2_cnt <= #udly 0;
	   else
	      dsp_grp2_cnt <= #udly dsp_grp2_cnt + 1;

	// How many clock cycle has been masked.
	// count down from 4'b1111 to dsp_fs
	always @ (negedge rst_ or posedge dsp_clk_src_12)
	   if (!rst_)
	      dsp_grp2_cnt2 <= #udly 3'b111;
	   else if (dsp_grp2_cnt == 3'b111)		// set cnt2 to 5'h1f when cnt1 grow to 11111
	      dsp_grp2_cnt2 <= #udly 3'b111;
	   else if (dsp_grp2_mask & (dsp_grp2_cnt2 != dsp_fs[2:0]))
	      dsp_grp2_cnt2 <= #udly dsp_grp2_cnt2 - 1;

	// using clk_src_14_ to generate mask signal
	always @ (negedge rst_ or posedge dsp_clk_src_12_)
	   if (!rst_)
	      dsp_grp2_mask <= #udly 0;
	   else if ((dsp_grp2_cnt[0] == 1'b1) & (dsp_grp2_cnt2 != dsp_fs[2:0]))
	      dsp_grp2_mask <= #udly 1;
	   else
	      dsp_grp2_mask <= #udly 0;

	wire	dsp_grp2_clk	= dsp_clk_src_12 & !dsp_grp2_mask;

	// DSP clock could be selected from group 1 or 2 clock output.
	assign	div_dsp_clk	= dsp_grp1_clk & dsp_grp1_en |
						  dsp_grp2_clk & dsp_grp2_en;
*/
endmodule
