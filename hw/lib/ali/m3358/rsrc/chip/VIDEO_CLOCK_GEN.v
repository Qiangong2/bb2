/******************************************************************************
	  (c) copyrights 2000-. All rights reserved

	   T-Square Design, Inc.

	  Company confidential and Properietary	information.
	  This information may not be disclosed	to unauthorized individual.
*******************************************************************************/

/*******************************************************************************
 *	DESCRIPTION:	The clock generator in M3357 for Video.
 *
 *	AUTHOR: 		Norman
 *
 *	HISTORY:   		2003.08.29	initial version
 					2003-12-06	Add the 80 MHz clock input.
 								Add SVGA mode enable.
 								Add the clock delay and polarity adjustment
 					2003-12-16	Add TV_MODE input, SVGA_MODE_EN means the 800x600 vga mode
 					2004-02-26	Add the VSB clock output, which is fixed at 80MHz
 					2004-03-31	Add the input ALL_CLOCK_EN for the standby mode
 *********************************************************************************/
module VIDEO_CLOCK_GEN (

	// input
		SRC_CLK,			// from clock input pad
		F108M_IN,			// 108MHz clock input from PLL
		F80M_IN,			// 80MHz clock input from PLL
		JTCLK,				// jtclk from pad
		VIDEO_CLK_IN,		// VIDEO clock from clock gen

		BYPASS_PLL,			// By pass audio PLL
		TEST_MODE,			// Chip in scan test mode
		INTER_ONLY,			// Interlace output
//		DVI_RWBUF_SEL,		// DVI buffer read/write
		SVGA_MODE_EN,		// SVGA mode enable, TV encoder output the
		TV_MODE,			// TV mode
		OUT_MODE_ETV_SEL,	// output pixel clock select, 0 -> 54 MHz, 1 -> 27 MHz
		ALL_CLOCK_EN,	// enable all the clock except the CPU/MEM/SB

	// output
		CVBS2X_CLK,			// 54MHZ clock, dvi0_clk
		CVBS_CLK,			// 27MHz clock, dvi1_clk
		CAV_CLK,			// 54 or 27 MHZ clock, dvi_buf2754_clk
//		DVI_BUF0_CLK,		// CAV_CLK or VIDEO_CLK for buffer 0
//		DVI_BUF1_CLK,		// CAV_CLK or VIDEO_CLK for buffer 1
		VIDEO_CLK,			// VIDEO clock output
		OUT_PIXEL_CLK,		// output pixel clock for external tv encoder
		VSB_CLK,			// VSB clock, 80MHz

//		TV_ENC_CLK_SEL,		// video clock delay select
		F108M_CLK_TV,		// 108MHz output for TV encoder
		CVBS2X_CLK_TV,		// 54MHZ clock for TV encoder
//		CVBS_CLK_TV,		// 27MHz clock for TV encoder
		CAV2X_CLK_TV,		// 108 or 54 MHZ clock for TV encoder
		CAV_CLK_TV,			// 54 or 27 MHZ clock for TV encoder

		RST_
		);

	// input
input			SRC_CLK;			// from clock input pad
input			F108M_IN;			// 108MHz clock input from PLL
input			F80M_IN;			// 80MHz clock input from PLL
input			JTCLK;				// jtclk from pad
input			VIDEO_CLK_IN;		// VIDEO clock from clock gen

input			BYPASS_PLL;			// By pass audio PLL
input			TEST_MODE;			// Chip in test mode
input			INTER_ONLY;			// Interlace output
//input			DVI_RWBUF_SEL;		// DVI buffer read/write
input			SVGA_MODE_EN;		// SVGA mode enable, TV encoder output the
input			TV_MODE;			// TV mode
input			OUT_MODE_ETV_SEL;	// output pixel clock select, 0 -> 54 MHz, 1 -> 27 MHz
input			ALL_CLOCK_EN;		// enable all the clock except the CPU/MEM/SB

// output
output			CVBS2X_CLK;			// 54MHZ clock
output			CVBS_CLK;			// 27MHz clock
output			CAV_CLK;			// 54 or 27 MHZ clock
//output			DVI_BUF0_CLK;		// CAV_CLK or VIDEO_CLK for buffer 0
//output			DVI_BUF1_CLK;		// CAV_CLK or VIDEO_CLK for buffer 1
output			VIDEO_CLK;			// VIDEO clock output
output			OUT_PIXEL_CLK;		// output pixel clock for external tv encoder
output			VSB_CLK;			// VSB clock, 80MHz

//input	[11:8]	TV_ENC_CLK_SEL;	// video clock delay select
output			F108M_CLK_TV;		// 108MHz output for TV encoder
output			CVBS2X_CLK_TV;		// 54MHZ clock for TV encoder
//output			CVBS_CLK_TV;		// 27MHz clock for TV encoder
output			CAV2X_CLK_TV;		// 108 or 54 MHZ clock for TV encoder
output			CAV_CLK_TV;			// 54 or 27 MHZ clock for TV encoder

input			RST_;

	//=========================================================================
	// Get 1/2 input source clock
	//=========================================================================
	reg		src_12;		// 1/2 source clock

	always @ (negedge RST_ or posedge SRC_CLK)
	   if (!RST_)
	      src_12 <= 0;
  	   else
	      src_12 <= ~src_12;

	//=========================================================================
	// Get 108, 80, 54, 27 MHz clocks
	//=========================================================================
	wire	f108m_clk_tmp	= BYPASS_PLL ? src_12 : F108M_IN;
	reg		f54m_clk_tmp;
	reg		f27m_clk_tmp;
	reg		f40m_clk_tmp;	// 40 MHz for the DVI0_CLK
	wire	f80m_clk_tmp	= BYPASS_PLL ? src_12 : F80M_IN;

	always @ (negedge RST_ or posedge f108m_clk_tmp)
		if (!RST_)
		   f54m_clk_tmp <= 0;
		else
		   f54m_clk_tmp <= ~f54m_clk_tmp;

	always @ (negedge RST_ or posedge f54m_clk_tmp)
		if (!RST_)
		   f27m_clk_tmp <= 0;
		else
		   f27m_clk_tmp <= ~f27m_clk_tmp;

	always @ (negedge RST_ or posedge f80m_clk_tmp)
		if (!RST_)
		   f40m_clk_tmp <= 0;
		else
		   f40m_clk_tmp <= ~f40m_clk_tmp;

	//=========================================================================
	// outputs
	//=========================================================================
	// dvi0_clk
	wire	CVBS2X_CLK_PRE_GATE	= TEST_MODE ? JTCLK :
							(TV_MODE ? f54m_clk_tmp :
							(SVGA_MODE_EN ? f40m_clk_tmp : f27m_clk_tmp));
	GATX20	CVBS2X_CLK_GATE	(
					.CLK	(CVBS2X_CLK_PRE_GATE	),
					.EN		(ALL_CLOCK_EN			),
					.SE		(TEST_MODE				),
					.YGCLK	(CVBS2X_CLK				)
					);

	// dvi1_clk
	wire	CVBS_CLK_PRE_GATE	= TEST_MODE ? JTCLK : f27m_clk_tmp;
	GATX20	CVBS_CLK_GATE	(
					.CLK	(CVBS_CLK_PRE_GATE		),
					.EN		(ALL_CLOCK_EN			),
					.SE		(TEST_MODE				),
					.YGCLK	(CVBS_CLK				)
					);

	// dvi_buf2754_clk
	wire	CAV_CLK_PRE_GATE	= TEST_MODE ? JTCLK :
							(INTER_ONLY ? f27m_clk_tmp : CVBS2X_CLK_PRE_GATE);	// Select the clock before the gating.
	GATX20	CAV_CLK_GATE	(
					.CLK	(CAV_CLK_PRE_GATE		),
					.EN		(ALL_CLOCK_EN			),
					.SE		(TEST_MODE				),
					.YGCLK	(CAV_CLK				)
					);


	wire	OUT_PIXEL_CLK_PRE_GATE	= OUT_MODE_ETV_SEL ? f27m_clk_tmp : f54m_clk_tmp;
	GATX20	OUT_PIXEL_CLK_GATE	(
					.CLK	(OUT_PIXEL_CLK_PRE_GATE	),
					.EN		(ALL_CLOCK_EN			),
					.SE		(TEST_MODE				),
					.YGCLK	(OUT_PIXEL_CLK			)
					);

	wire	CVBS2X_CLK_TV_PRE_GATE	= TEST_MODE ? JTCLK : f54m_clk_tmp;
	GATX20	CVBS2X_CLK_TV_GATE	(
					.CLK	(CVBS2X_CLK_TV_PRE_GATE	),
					.EN		(ALL_CLOCK_EN			),
					.SE		(TEST_MODE				),
					.YGCLK	(CVBS2X_CLK_TV			)
					);


	wire	CAV2X_CLK_TV_PRE_GATE	= TEST_MODE ? JTCLK :
									(SVGA_MODE_EN ? f80m_clk_tmp :
									(INTER_ONLY ? f54m_clk_tmp : f108m_clk_tmp));
	GATX20	CAV2X_CLK_TV_GATE	(
					.CLK	(CAV2X_CLK_TV_PRE_GATE	),
					.EN		(ALL_CLOCK_EN			),
					.SE		(TEST_MODE				),
					.YGCLK	(CAV2X_CLK_TV			)
					);


	wire	CAV_CLK_TV_PRE_GATE		= TEST_MODE ? JTCLK :
									(SVGA_MODE_EN ? f80m_clk_tmp :
									(INTER_ONLY ? f27m_clk_tmp : f54m_clk_tmp));
	GATX20	CAV_CLK_TV_GATE	(
					.CLK	(CAV_CLK_TV_PRE_GATE),
					.EN		(ALL_CLOCK_EN		),
					.SE		(TEST_MODE			),
					.YGCLK	(CAV_CLK_TV			)
					);

	//	This clock has already been mask off in the clockgen.v
	assign	VIDEO_CLK		= TEST_MODE ? JTCLK : VIDEO_CLK_IN;

	//	F108 MHz delay
	//	assign	F108M_CLK_TV	= F108M_CLK;
	wire	F108M_CLK_TV_PRE_GATE	= TEST_MODE ? JTCLK : f108m_clk_tmp;
	GATX20	F108M_CLK_TV_GATE	(
					.CLK	(F108M_CLK_TV_PRE_GATE			),
					.EN		(ALL_CLOCK_EN		),
					.SE		(TEST_MODE			),
					.YGCLK	(F108M_CLK_TV		)
					);

	//	80MHz for the
	wire	VSB_CLK_PRE_GATE	= TEST_MODE ? JTCLK : f80m_clk_tmp;
	GATX20	VSB_CLK_GATE	(
					.CLK	(VSB_CLK_PRE_GATE	),
					.EN		(ALL_CLOCK_EN		),
					.SE		(TEST_MODE			),
					.YGCLK	(VSB_CLK			)
					);

endmodule
