/******************************************************************************
	  (c) copyrights 2000-. All rights reserved

	   T-Square Design, Inc.

	  Company confidential and Properietary	information.
	  This information may not be disclosed	to unauthorized individual.
*******************************************************************************/

/******************************************************************************
 *
 *    DESCRIPTION:	ide clock divider in clock generator
 *
 *    AUTHOR:		norman
 *
 *    HISTORY: 		2002.07.03	initial version
 *****************************************************************************/

module ide_clock_divider(
	// input
	ide_clk_src,	// source clock, 100MHz

	//output
	div_100_clk,	// output 100MHz clock
	div_66_clk,		// output 66MHz clock
	div_33_clk,		// output 33MHz clock, also the PCI clock
	rst_
	);

input	ide_clk_src;

output	div_100_clk;
output	div_66_clk;
output	div_33_clk;

input		rst_;

parameter	udly = 0.3;

	/*=========================================================================
		Divide input clock to get 1:1, 1:1.5, 1:3,
		clocks
	=========================================================================*/
	wire	ide_clk_src_	= !ide_clk_src;		// inverted input clock

	wire	clk_src_11	= ide_clk_src;			// 1:1 sub clock
	wire	clk_src_23;							// 2:3 sub clock
	reg		clk_src23_flag;

	reg		clk_src_13;							// 1:3 sub clock

	reg	[1:0]	clk_src3_cnt;					// counter 3 of clk_src
	// clk_src3_cnt
	always @ (posedge ide_clk_src or negedge rst_)
	   if (!rst_)
	      clk_src3_cnt <= #udly 0;
	   else if (clk_src3_cnt == 2'b10 | clk_src3_cnt == 2'b11)
	      clk_src3_cnt <= #udly 0;
	   else
	      clk_src3_cnt <= #udly clk_src3_cnt + 2'b1;

	always @ (posedge ide_clk_src_ or negedge rst_)
	   if (!rst_)
	      clk_src23_flag <= #udly 0;
	   else if (clk_src3_cnt == 2'b10)
	      clk_src23_flag <= #udly 1;
	   else if (clk_src3_cnt == 2'b00)
	      clk_src23_flag <= #udly 0;

	assign	clk_src_23	= (clk_src3_cnt == 2'b00) & clk_src23_flag & ide_clk_src |
						  (clk_src3_cnt == 2'b01) & !clk_src23_flag & ide_clk_src_;

	// clk_src_13
	always @ (posedge ide_clk_src or negedge rst_)
	   if (!rst_)
	      clk_src_13 <= #udly 0;
	   else if (clk_src3_cnt == 2'b10)			// clk_src_13 will be high when cnt3 is 0
	      clk_src_13 <= #udly 1;
	   else
	      clk_src_13 <= #udly 0;

	/*=========================================================================
		output clock
		ATA100 clock is ide_clk_src
		ATA66 clock is clk_src_23
		ATA33 clock is clk_src_13
	=========================================================================*/
	assign	div_100_clk = clk_src_11;
	assign	div_66_clk 	= clk_src_23;
	assign	div_33_clk	= clk_src_13;

endmodule
