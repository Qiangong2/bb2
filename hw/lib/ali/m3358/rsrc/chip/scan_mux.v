/*********************************************************************
File:           scan_mux.v
Descript:       control the scan chain input/output mux
				The scan chain number in M6304 is limited by avaliable
				I/O.
Author:         Norman
Histroy:        2002-10-15      initial
				2003-09-18		Change the scan chain number to 94
*********************************************************************/
module  scan_mux(
		scan_din_out2core,
		scan_dout_fromcore,
		scan_din_frompadmisc,
		scan_dout_out2padmisc,

		test_mode,
		test_sel
		);


output	[89:0]	scan_din_out2core;			// In put data out to core
input	[89:0]	scan_dout_fromcore;			// Output data from core

input	[47:0]	scan_din_frompadmisc;		// Input data from padmisc
output	[47:0]	scan_dout_out2padmisc;		// Output data to padmisc

input	test_mode;							// chip scan test mode
input	test_sel;							// scan chain select

	//=========================================================================
	//	In test mode, when the test_sel = 1, transfer the scan din data to high
	//	part of scan_din of core, otherwise transfer the scan din data to low
	//	part of scan_din of core.
	//	If test mode not enable, set the scan din to core to 0
	//=========================================================================
assign  scan_din_out2core[5:0] = scan_din_frompadmisc[5:0];	//nb, mem
    // chain 47 Reserved
assign	scan_din_out2core[46:6]
			= test_mode ? (!test_sel ? scan_din_frompadmisc[46:6] : 'h0) : 'h0;
    // chain 89-88 Reserved
assign	scan_din_out2core[86:47]
			= test_mode ? (test_sel ? scan_din_frompadmisc[45:6] : 'h0) : 'h0;

	//=========================================================================
	//	In test mode, when the test_sel = 1, select the high part of scan_dout
	//	to PAD, otherwise transfer the low part of scan_dout to PAD
	//	If test mode not enabled, set the scan dout to PAD to 0
	//=========================================================================
assign  scan_dout_out2padmisc[5:0] = scan_dout_fromcore[5:0];	//nb, mem

assign	scan_dout_out2padmisc[47:6]
			= test_mode ? (test_sel ? {2'b0,scan_dout_fromcore[86:47]} :{1'b0, scan_dout_fromcore[46:6]}) : 'h0;

endmodule
