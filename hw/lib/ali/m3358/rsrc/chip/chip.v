/**********************************************************************************
// Description: M3357 top level module
// File:        chip.v
// Author:      Tod
// History		2003-04-19, initial version

***********************************************************************************/

module chip (
// SDRAM Interface 16 bit sdram(55) (56 for m6304)
        p_d_clk,         // 1,   SDRAM master clock input
        p_d_addr,       // 12,  SDRAM address
        p_d_baddr,      // 2,   SDRAM bank address
        p_d_dq,         // 32,  SDRAM 32 bits output data pins
        p_d_dqm,        // 4,   SDRAM data mask signal
        p_d_cs_,        // 2,   SDRAM chip select
        p_d_cas_,       // 1,   SDRAM cas#
        p_d_we_,        // 1,   SDRAM we#
        p_d_ras_,       // 1,   SDRAM ras#
// FLASH ROM Interface(33, do not share with IDE bus as m6304)
		p_rom_ale,		// 1	Flash rom external 373 latch enable
//        p_rom_ce_,      // 1,   Flash rom chip select
        p_rom_we_,      // 1,   Flash rom write enable
        p_rom_oe_,      // 1,   Flash rom output enable
        p_rom_data,		// 8 	Flash rom data
        p_rom_addr,		// 21	Flash rom address
		p_rom_addr_set2,// 2	Flash rom address set 2 [18:17] for bonding
// PCI Interface (48)
		p_clk		,
		p_gnta_     ,
		p_gntb_     ,
		p_rst_      ,
		p_req_		,
		p_inta_     ,
		p_ad		,
		p_par       ,
		p_frame_    ,
		p_irdy_     ,
		p_trdy_     ,
		p_stop_     ,
		p_devsel_   ,
		p_cbe_      ,
		p_serr_     ,
		p_perr_     ,
//IDE Interface (28)
		atadiow_	,
		atadior_    ,
		atacs0_     ,
		atacs1_     ,
		atada       ,
		atareset_   ,
		atadmack_   ,
		ataiordy	,
		ataintrq    ,
		atadmarq    ,
		atadd		,
// I2S Audio Interface (11)
        i2so_data0,     // 1,   Serial data for I2S output 0
        i2so_data1,     // 1,   Serial data for I2S output 1
        i2so_data2,     // 1,   Serial data for I2S output 2
        i2so_data3,     // 1,   Serial data for I2S output 3
        i2so_bck,       // 1,   Bit clock for I2S output
        i2so_lrck,      // 1,   Channel clock for I2S output
        i2so_mclk,      // 1,   over sample clock for i2s DAC
		i2si_data,
        i2si_mclk,
        i2si_bck,
        i2si_lrck,
//sd_ms interface
		sd_clk					,
		sd_cmd                  ,
		sd_data                 ,
		sd_wr_prct              ,
		sd_det                  ,
		ms_clk                  ,
		ms_pwr_ctrl             ,
		ms_bs                   ,
		ms_sdio                 ,
		ms_ins                  ,
//------GI Interface (from broadon)
		GCVIDCLK					,
		GCSYSCLK					,
		ALTBOOT 	                ,
		TESTEN                    	,
		GCRSTB		                ,

		PROMSCL	                    ,
		PROMSDA	                    ,

		DID		                    ,
		DIBRK                       ,
		DIDIR                       ,
		DIERRB                      ,
		DIDSTRBB                    ,
		DIHSTRBB                    ,
		DICOVER                     ,
		DIRSTB						,

		AISCLK                     ,
		AISLR                      ,
		AISD                       ,

//video in interface
		vin_pix_clk					,
		vin_pix_data                ,
		vin_hsync_                  ,
		vin_vsync_                  ,
//digital video out
		pix_clk		,
		vout_data	,
		vout_hsync_	,
		vout_vsync_ ,
//digital TV encoder
		tven_hsync_ ,
        tven_vsync_ ,
//TV encoder Interface (14)
		vd33a_tvdac,	// 1,	Analog Power, input
		gnda_tvdac,		// 1,	Analog GND, input
		gnda1_tvdac,	// 1,	Analog GND, input
		vsud_tvdac,		// 1,	Analog GND, input
		iout1,			// 1,   Analog video output 1
		iout2,			// 1,   Analog video output 2
		iout3,			// 1,   Analog video output 3
		iout4,			// 1,   Analog video output 4
		iout5,			// 1,   Analog video output 5
		iout6,			// 1,   Analog video output 6
		xiref1,			// 1,	connect external via capacitance toward VDDA
//		xiref2,			// 1,	connect external via capacitance toward VDDA, not valid in 2003-12-05
		xiext,			// 1,	470 Ohms resistor connect pin, I/O
		xiext1,			// 1,	470 Ohms resistor connect pin, I/O
		xidump,			// 1,	For performance and chip temperature, output
// I2C	 (2)
		scbsda			,
		scbscl			,
// SPDIF (2)
        spdif,          // 1, output
		spdif_in,
//UART  (2)
		uart_tx,		// 1
		uart_rx,
// USB port (6)
        usb2dp,         // 1,   D+ for USB port2
        usb2dn,         // 1,   D- for USB port2
        usb1dp,         // 1,   D+ for USB port1
        usb1dn,         // 1,   D- for USB port1
        usbpon,         // 1,   USB port power control
        usbovc,         // 1,   USB port over current
//SERVO port (70)
   		XADCIN		,
   		XADCIP		,
   		XATTON		,
   		XATTOP		,
   		XBIASR		,
   		XCDLD		,
   		XCDPD		,
   		XCDRF		,
   		XCD_A		,
   		XCD_B		,
   		XCD_C		,
   		XCD_D		,
   		XCD_E		,
   		XCD_F		,
   		XCELPFO		,
   		XDPD_A		,
   		XDPD_B		,
   		XDPD_C		,
   		XDPD_D		,
   		XDVDLD		,
   		XDVDPD		,
   		XDVDRFN		,
   		XDVDRFP		,
   		XDVD_A		,
   		XDVD_B		,
   		XDVD_C		,
   		XDVD_D		,
   		XFELPFO		,
   		XFOCUS		,
   		XGMBIASR	,
   		XLPFON		,
   		XLPFOP		,
   		XPDAUX1		,
   		XPDAUX2		,
   		XSBLPFO		,
   		XSFGN		,
   		XSFGP		,
   		XSFLAG		,		//	carcy add 2003/11/17 18:52
   		XSLEGN		,
   		XSLEGP		,
   		XSPINDLE	,
   		XTELP		,
   		XTELPFO		,
   		XTESTDA		,
   		XTEXO		,
   		XTRACK		,
   		XTRAY		,
   		XVGAIN		,
   		XVGAIP		,
   		XVREF15		,
   		XVREF21		,
  		VDD3MIX1	,
  		VDD3MIX2	,
  		AVDD_AD		,
  		AVDD_ATT	,
  		AVDD_DA		,
  		AVDD_DPD	,
  		AVDD_LPF	,
  		AVDD_RAD	,
  		AVDD_REF	,
  		AVDD_SVO	,
  		AVDD_VGA	,
  		AVSS_AD		,
  		AVSS_ATT	,
  		AVSS_DA		,
  		AVSS_DPD	,
  		AVSS_GA		,
  		AVSS_GD		,
  		AVSS_LPF	,
  		AVSS_RAD	,
  		AVSS_REF	,
  		AVSS_SVO	,
  		AVSS_VGA	,
  		DV18_RCKA	,
  		DV18_RCKD	,
  		DVSS_RCKA	,
  		DVSS_RCKD	,
// Consumer IR Interface (1)
        irrx,           // 1,   consumer infra-red remote controller/wireless keyboard
// General Purpose IO (14,GPIO)
        gpio,           // 14,  General purpose io[7:0]
// EJTAG Interface (5)
        p_j_tdo,        // 1,   serial test data output
        p_j_tdi,        // 1,   serial test data input
        p_j_tms,        // 1,   test mode select
        p_j_tclk,       // 1,   test clock
        p_j_rst_,       // 1,   test reset
// power and ground
		vddcore,		// 		core vdd
		vddio,			//		io vdd
		gnd,			//		io vss
		f27_pllvdd,			//		pll vdd for f27 pll
		f27_pllvss,			//		pll vss for f27 pll
// system (4)
        p_crst_,        	// 1,   cold reset
        test_mode,      	// 1,   chip test mode input, (for scan chain)
        p_x27in,        	// 1,   crystal input (27 MHz for video output)
        p_x27out   ,     	// 1,   crystal output
//ADC (5)
		vd33a_adc	,		// 1,	3.3V Analog VDD for ADC.
		xmic1in     ,        // 1,   ADC microphone input 1.
		xadc_vref   ,        // 1,   ADC reference voltage (refered to the reference circuit).
		xmic2in     ,        // 1,   ADC microphone input 2.
		gnda_adc            // 1,   Analog GND for ADC.

        );

parameter       udly = 1;
parameter       ram_dly = 1,
                pci_dly = 1,
                oth_dly = 1;
//============define the input/output/inout signals===============
//--------------SDRAM Interface (55)
output          p_d_clk;                 // 1,   SDRAM master clock input
inout   [11:0]  p_d_addr;               // 12,  SDRAM address
inout   [1:0]   p_d_baddr;      		// 2,   SDRAM bank address
inout   [31:0]  p_d_dq;         		// 32,  SDRAM 32 bits output data pins
inout   [3:0]   p_d_dqm;        		// 4,   SDRAM data mask signal
inout   		p_d_cs_;        		// 1,   SDRAM chip select
inout           p_d_cas_;               // 1,   SDRAM cas#
inout           p_d_we_;                // 1,   SDRAM we#
inout           p_d_ras_;               // 1,   SDRAM ras#
//--------------FLASH ROM Interface(33, others share with IDE bus)
inout			p_rom_ale;				// 1	Flash rom external 373 latch enable
//inout           p_rom_ce_;              // 1,   Flash rom chip select
inout           p_rom_we_;              // 1,   Flash rom write enable
inout           p_rom_oe_;              // 1,   Flash rom output enable
inout   [7:0]   p_rom_data;             // 8,   Flash rom data
inout	[21:0]	p_rom_addr;             // 21,   Flash rom address
inout  [18:17]	p_rom_addr_set2;		// 2	Flash rom address set 2 [18:17] for bonding
//--------------PCI Interface(48)
output			p_clk  	;
output			p_gnta_ ;
output			p_gntb_ ;
output			p_rst_  ;
input			p_req_ 	;
input			p_inta_ ;
inout [31:0]	p_ad   ;
inout			p_par          ;
inout			p_frame_       ;
inout			p_irdy_        ;
inout			p_trdy_        ;
inout			p_stop_        ;
inout			p_devsel_      ;
inout [3:0]		p_cbe_          ;
inout           p_serr_         ;
inout           p_perr_         ;
//--------------IDE Interface (28)
output			atadiow_		;
output			atadior_        ;
output			atacs0_         ;
output			atacs1_         ;
output	[2:0] 	atada     ;
output			atareset_       ;
output			atadmack_       ;

input			ataiordy		;
input			ataintrq		;
input			atadmarq		;
inout	[15:0]	atadd			;
//------GI Interface (from broadon)
output	GCVIDCLK			;
output	[1:0] GCSYSCLK	    ;
input	ALTBOOT	            ;
input	TESTEN	    		;
output	GCRSTB         		;

output	PROMSCL 			;
inout	PROMSDA            ;

inout [7:0]	DID		        ;
inout	DIBRK               ;
input	DIDIR               ;
output	DIERRB              ;
output	DIDSTRBB            ;
input	DIHSTRBB            ;
output	DICOVER             ;
input	DIRSTB				;

input	AISCLK             	;
input	AISLR              	;
output	AISD               	;
//--------------SD_MS Interface
//------MS_SD Interface
output			sd_clk		;
inout			sd_cmd      ;
inout	[3:0]	sd_data     ;
input			sd_wr_prct  ;
input			sd_det      ;
output			ms_clk      ;
output			ms_pwr_ctrl ;
output			ms_bs       ;
inout			ms_sdio     ;
input			ms_ins      ;
//--------------I2S Audio Interface (11)
inout           i2so_data0;             // 1,   Serial data for I2S output
inout 			i2so_data1;     		// 1,   Serial data for I2S output 1
inout   	    i2so_data2;     		// 1,   Serial data for I2S output 2
inout       	i2so_data3;		     	// 1,   Serial data for I2S output 3
inout           i2so_bck;               // 1,   Bit clock for I2S output
inout           i2so_lrck;              // 1,   Channel clock for I2S output
inout           i2so_mclk;              // 1,   over sample clock for i2s DAC
input			i2si_data;
input			i2si_mclk;
input			i2si_bck;
input			i2si_lrck;
//inout           i2si_data;              // 1,   serial data for I2S input
//--------------video in interface
input		 	vin_pix_clk	;
input [7:0]		vin_pix_data    ;
input		 	vin_hsync_     ;
input		 	vin_vsync_     ;
//--------------DIGITAL VIDEO out
output			pix_clk			;
output[7:0]		vout_data	    ;
output			vout_hsync_	    ;
output			vout_vsync_     ;
//--------------digital tvencoder
output		tven_hsync_ ;
output		tven_vsync_ ;
//TV encoder Interface (14)
input			vd33a_tvdac;	// 1,	Analog Power, input
input			gnda_tvdac;		// 1,	Analog GND, input
input			gnda1_tvdac;	// 1,	Analog GND, input
input			vsud_tvdac;		// 1,	Analog GND, input
output			iout1;			// 1,   Analog video output 1
output			iout2;			// 1,   Analog video output 2
output			iout3;			// 1,   Analog video output 3
output			iout4;			// 1,   Analog video output 4
output			iout5;			// 1,   Analog video output 5
output			iout6;			// 1,   Analog video output 6
output			xiref1;			// 1,	connect external via capacitance toward VDDA
//output			xiref2;			// 1,	connect external via capacitance toward VDDA
output			xiext;			// 1,	470 Ohms resistor connect pin, I/O
output			xiext1;			// 1,	470 Ohms resistor connect pin, I/O
output			xidump;			// 1,	For performance and chip temperature, output
//--------------SPDIF (2)
inout			spdif;                  // 1, output
input			spdif_in;
//--------------I2C   (2)
inout			scbsda;
inout			scbscl;
//UART(2)
inout			uart_tx;		// 1
input			uart_rx;
//--------------USB port (6)
inout           usb2dp;                 // 1,   D+ for USB port2
inout           usb2dn;                 // 1,   D- for USB port2
inout           usb1dp;                 // 1,   D+ for USB port1
inout           usb1dn;                 // 1,   D- for USB port1
output          usbpon;                 // 1,   USB port power control
input           usbovc;                 // 1,   USB port over current
//--------------SEVO port (70)
input  			XADCIN;
input  			XADCIP;
output 			XATTON;
output 			XATTOP;
input  			XBIASR;
output 			XCDLD;
input  			XCDPD;
input  			XCDRF;
input  			XCD_A;
input  			XCD_B;
input  			XCD_C;
input  			XCD_D;
input  			XCD_E;
input  			XCD_F;
output 			XCELPFO;
input  			XDPD_A;
input  			XDPD_B;
input  			XDPD_C;
input  			XDPD_D;
output 			XDVDLD;
input  			XDVDPD;
input  			XDVDRFN;
input  			XDVDRFP;
input  			XDVD_A;
input  			XDVD_B;
input  			XDVD_C;
input  			XDVD_D;
output 			XFELPFO;
output 			XFOCUS;
input  			XGMBIASR;
output 			XLPFON;
output 			XLPFOP;
input  			XPDAUX1;
input  			XPDAUX2;
output 			XSBLPFO;
input  			XSFGN;
input  			XSFGP;
inout [1:0] 	XSFLAG;		//	carcy add 2003/11/17 18:52
output 			XSLEGN;
output 			XSLEGP;
output 			XSPINDLE;
input  			XTELP;
output 			XTELPFO;
output 			XTESTDA;
output 			XTEXO;
output 			XTRACK;
output 			XTRAY;
input  			XVGAIN;
input  			XVGAIP;
output 			XVREF15;
output 			XVREF21;
input  			VDD3MIX1;
input  			VDD3MIX2;
input  			AVDD_AD;
input  			AVDD_ATT;
input  			AVDD_DA;
input  			AVDD_DPD;
input  			AVDD_LPF;
input  			AVDD_RAD;
input  			AVDD_REF;
input  			AVDD_SVO;
input  			AVDD_VGA;
input  			AVSS_AD;
input  			AVSS_ATT;
input  			AVSS_DA;
input  			AVSS_DPD;
input  			AVSS_GA;
input  			AVSS_GD;
input  			AVSS_LPF;
input  			AVSS_RAD;
input  			AVSS_REF;
input  			AVSS_SVO;
input  			AVSS_VGA;
input  			DV18_RCKA;
input  			DV18_RCKD;
input  			DVSS_RCKA;
input  			DVSS_RCKD;
//--------------Consumer IR Interface (1)
inout			irrx;                   // 1,   consumer infra-red remote controller/wireless keyboard
//--------------General Purpose IO (13:0, 28~24,GPIO)
inout   [15:0]  gpio;                   // 19,  General purpose io
//--------------EJTAG Interface (5)
inout           p_j_tdo;                // 1,   serial test data output
inout           p_j_tdi;                // 1,   serial test data input
inout           p_j_tms;                // 1,   test mode select
inout           p_j_tclk;               // 1,   test clock
inout           p_j_rst_;               // 1,   test reset
// power and ground
input			vddcore;			// 		core vdd
input			vddio;				//		io vdd
input			gnd;				//		io vss
input			f27_pllvdd;			//		pll vdd for f27 pll
input			f27_pllvss;			//		pll vss for f27 pll
//--------------system (4)
input           p_crst_;            // 1,   cold reset
input           test_mode;          // 1,   chip test mode input for scan
input           p_x27in;            // 1,   crystal input (27 MHz for video output)
output          p_x27out;           // 1,   crystal output
//--------------ADC (5)
inout			vd33a_adc	;		// 1,	3.3V Analog VDD for ADC.
input			xmic1in     ;       // 1,   ADC microphone input 1.
inout			xadc_vref   ;       // 1,   ADC reference voltage (refered to the reference circuit).
input			xmic2in     ;       // 1,   ADC microphone input 2.
inout			gnda_adc    ;       // 1,   Analog GND for ADC.
//================================================================

//===================define the signal types======================
//------------ clock generater
wire            usb_clk_gen2core,       // USB clock 48M
                sb_clk_gen2core ,       // SouthBridge Clock 12.288M
                pixel_clk_gen2core   ,  // Display Engine clock,27MHz
                cpu_clk_gen2core,       // CPU clock (with JTAG test mode mask)
                mbus_clk_gen2core,      // MIPS clock
                inter_mem_clk_gen,      // internal SDRAM, NorthBridge clock
                ext_mem_clk_gen,    	// external SDRAM clock,output to pad
                dsp_clk_gen2core,       // SDP Clock
                pci_clk_gen2core,       // internal PCI bus clock
                ve_clk_gen2core,    	// Video clock
                spdif_clk_gen2core, 	// SPDIF clock
                pci_mbus_clk_gen;       // output to PCI/MIPS clock pad

//================================================================
//SDRAM	signals wires
wire    [11:0]  ram_addr_core2padmisc;
wire	[11:0]	ram_addr_padmisc2pad;
wire	[11:0]	ram_addr_oe_padmisc2pad;
wire    [1:0]   ram_ba_core2padmisc;
wire	[1:0]	ram_ba_padmisc2pad;
wire	[1:0]	ram_ba_oe_padmisc2pad_;
wire    [31:0]  ram_dq_core2padmisc, ram_dq_padmisc2core;
wire    [31:0]  ram_dq_pad2padmisc;
wire	[31:0]	ram_dq_padmisc2pad;
wire	[31:0]	ram_dq_oe_padmisc2pad_;
wire    [3:0]   ram_dqm_core2padmisc;
wire	[3:0]	ram_dqm_padmisc2pad,	ram_dqm_oe_padmisc2pad_;
wire    [1:0]   ram_cs_core2padmisc;
wire			ram_cs_padmisc2pad_, ram_cs_pad2padmisc_;
wire			ram_cs_oe_padmisc2pad_;
wire	[31:0]	ram_dq_oe_core2padmisc_;
wire    [11:0]  ram_addr_pad2padmisc;
wire    [1:0]   ram_ba_pad2padmisc;
wire    [3:0]   ram_dqm_pad2padmisc;
//PCI signals wires
wire    [31:0]  pci_ad_core2padmisc, pci_ad_padmisc2core,
				pci_ad_padmisc2pad,	 pci_ad_pad2padmisc,
				pci_ad_oe_padmisc2pad_;
wire    [3:0]   pci_cbe_core2padmisc_, pci_cbe_padmisc2core_,
				pci_cbe_padmisc2pad_,  pci_cbe_pad2padmisc_,
				pci_cbe_oe_padmisc2pad_;
wire    [1:0]   pci_gnt_core2padmisc_    ;
wire    [1:0]   pci_req_padmisc2core_     ;
//FLASH signals wires
wire    [21:0]  rom_addr_core2padmisc;
wire	[21:0]	rom_addr_oe_core2padmisc;
wire    [15:0]   rom_rdata_padmisc2core;
wire	[7:0]	rom_wdata_core2padmisc    ;
wire	[7:0]	rom_data_pad2padmisc;
wire	[7:0]	rom_data_padmisc2pad;
wire	[7:0]	rom_data_oe_padmisc2pad_;
wire	[21:0]	rom_addr_pad2padmisc;
wire	[18:17] rom_addr_set2_pad2padmisc;
wire	[21:0]	rom_addr_padmisc2pad;
wire	[21:0]	rom_addr_oe_padmisc2pad_;
//GPIO signals wires
wire	[28:0]	gpio_padmisc2pad;
wire	[28:0]	gpio_pad2padmisc;
wire	[28:0]	gpio_oe_padmisc2pad_;
wire	[31:0]	gpio_core2padmisc;
wire	[31:0]	gpio_oe_core2padmisc_;
wire	[31:0]	gpio_padmisc2core;

wire	[31:0]	gpio_set2_padmisc2pad;
wire	[31:0]	gpio_set2_pad2padmisc;
wire	[31:0]	gpio_set2_oe_padmisc2pad_;
wire	[31:0]	gpio_set2_core2padmisc;
wire	[31:0]	gpio_set2_oe_core2padmisc_;
wire	[31:0]	gpio_set2_padmisc2core;
//AUDIO ADC test
wire	[24:23]	iadc_smtsen_padmisc2core;		// ADC SRAM Test Enable
wire	[2:0]	iadc_smtsmode_padmisc2core;		// ADC SRAM Test Mode
wire	[24:23]	iadc_smts_err_core2padmisc;		// ADC SRAM Test Error Flag
wire	[24:23]	iadc_smts_end_core2padmisc;		// ADC SRAM Self Test End
wire	[3:0]	iadc_tst_mod_padmisc2core;		// ADC Internal SDM Test Selection
//PAD BOOT config
wire    [31:0]  pad_boot_config;
//-----CPU Interface
//interrupt
wire    [4:0]   sys_int_padmisc2core_;
wire            sys_nmi_padmisc2core_;
//bist test
wire			cpu_bist_finish_core2padmisc;
wire	[5:0]	cpu_bist_error_vec_core2padmisc;
//MEMORY CLK delay chain signals
wire    [5:0]   cpu_clk_pll_m;
wire    [4:0]   mem_dly_sel;
wire	[4:0]	mem_rd_dly_sel;
wire	[5:0]	test_dly_chain_sel;		// TEST_DLY_CHAIN delay select
wire	[1:0]	test_dly_chain_flag;	// TEST_DLY_CHAIN flag output
//CLOCK SELECT SIGNALS
wire    [1:0]   pci_mips_fs;
wire	[5:0]	mem_clk_pll_m;

wire	[1:0]   IDE_FS_core2gen;		//ide clock frequency select
wire	[1:0]	DSP_FS_core2gen;		// dsp clock frequency select
wire	[1:0]	PCI_FS_core2gen;		// pci clock frequency select
wire	[1:0]	VE_FS_core2gen;			// ve clock frequency select
//wire    [1:0]   pci_clk_out_sel;
wire	[2:0]	work_mode_value_core2str;
//wire    [1:0]   pci_fs_value_core2str;
wire	[1:0]	flash_mode;
//wire	[1:0]	pci_clk_out_sel_core2gen;
//wire	[11:8]	tv_enc_clk_sel_core2gen;
wire    [5:0]   mem_clk_pll_m_value_core2str;
wire    [5:0]   cpu_clk_pll_m_value_core2str;
wire	[1:0]	i2s_mclk_sel_core2gen,
				spdif_clk_sel_core2gen;
//VIDEO signals wires
wire    [7:0]   vdata_core2padmisc;
wire    [7:0]   vdata_padmisc2pad;
//VIDEO IN
wire	[7:0]	video_in_pixel_data_padmisc2core;
wire	[7:0]	video_in_pixel_data_pad2padmisc;

wire			hv_sync_gpio_encore2padmisc;
wire			ata_ad_src_sel_core2padmisc;
//TVENCODER sram test
wire	[11:0]	tvenc_vdi4vdac_padmisc2core;
wire	[11:0]	tvenc_vdo4vdac_core2padmisc;
//IDE signals wires
wire    [15:0]  atadd_core2padmisc, atadd_padmisc2core,
                atadd_padmisc2pad, atadd_oe_padmisc2pad_,
                atadd_pad2padmisc;
wire	[3:0]	atadd_oe_core2padmisc_;
wire    [2:0]   atada_core2padmisc,
                atada_padmisc2pad, atada_oe_padmisc2pad_,
                atada_pad2padmisc;

//	SERVO signals
//wire	[23:0]	sv_pmata_somd_padmisc2core;
//wire	[2:0]	sv_tst_svoio_i_padmisc2core;
//wire	[2:0]	sv_svo_svoio_o_core2padmisc	 ;
//wire	[2:0]	sv_svo_svoio_oe_core2padmisc_;
wire	[5:0]	sv_tst_addata_padmisc2core;
wire	[44:0]	sv_tst_pin_core2padmisc;
wire	[1:0]	sv_pmsvd_c2ftstin_padmisc2core;
wire	[23:0]	sv_sycg_somd_padmisc2core;
wire	[15:0]	sv_sv_hi_hd_o_core2padmisc;
wire	[2:0]	sv_pmata_ha_padmisc2core;
wire	[15:0]	sv_pmata_hd_padmisc2core;
//wire	[1:0]	sv_sv_svo_sflag_core2padmisc;
wire	[12:0]	sv_sv_svo_soma_core2padmisc;
//wire	[23:0]	sv_sv_svo_somd_core2padmisc;
wire	[3:0]	sv_sv_reg_gpioten;
//wire	[1:0]	sv_pata_sflag_core2padmisc		;
//wire	[1:0]	sv_pmata_sflag_padmisc2core	    ;
//wire	[1:0]	sv_pmata_sflag_oe_core2padmisc_ ;
wire	[1:0]	sv_xsflag_padmisc2core;
wire	[1:0]	sv_xsflag_core2padmisc;
wire	[1:0]	sv_xsflag_oe_core2padmisc_;
wire	[1:0]	sv_xsflag_padmisc2pad		;
wire	[1:0]	sv_xsflag_oe_padmisc2pad_	;
wire	[1:0]	sv_xsflag_pad2padmisc		;
wire	[2:0]	sv_gpio_core2padmisc;
wire	[2:0]	sv_gpio_oe_core2padmisc_;
wire	[2:0]	sv_gpio_padmisc2core;
wire	[15:0]	sv_atadd_core2padmisc;
wire			sv_atadd_oe_core2padmisc_1bit_;		// servo atadd oe signal
wire	[15:0]	sv_atadd_oe_core2padmisc_		= {16{sv_atadd_oe_core2padmisc_1bit_}};
wire	[15:0]	sv_atadd_padmisc2core;
wire	[2:0]	sv_atada_padmisc2core;
wire	[36:0]	sram_ifx_core2padmisc;
wire	[36:0]	sram_ifx_oe_core2padmisc_;		// could be replaced
assign			sram_ifx_oe_core2padmisc_[36:24]	=	13'h0;		// could be replaced
wire	[36:0]	sram_ifx_padmisc2core;

//PAD DRIVER signls
wire	[1:0]	gpio_pad_driv_core2pad	   	,
				rom_da_pad_driv_core2pad    ,
				sdram_data_pad_driv_core2pad,
				sdram_ca_pad_driv_core2pad	;

//	------------------ Servo External uP interface -------------------
wire	[22:0]	ext_up_padmisc2core;
wire	[22:0]	ext_up_core2padmisc;
wire	[22:0]	ext_up_oe_core2padmisc_;
wire			sv_mpg_pcs1j		;
wire	[7:0]	sv_mpg_pdatao		;
wire			sv_mpg_prdj			;
wire			sv_mpg_pwrj			;
wire	[9:0]	sv_mpg_paddr		;
wire			sv_sv_pwaitj		;
wire	[7:0]	sv_sv_pdatai		;
wire			sv_sv_mpi_intj		;

//	------------------ Servo External uP interface end-------------------

//=================== MS/SD interface ===================================
wire			ms_ins_padmisc2core			;
wire			ms_serin_in_padmisc2core   	;
wire	[3:0]	sd_data_in_padmisc2core   	;
wire	[3:0]	sd_data_out_padmisc2pad		,
				sd_data_oej_padmisc2pad		,
				sd_data_in_pad2padmisc		;

//wire			sd_data1_in_padmisc2core   	;
//wire			sd_data2_in_padmisc2core   	;
//wire			sd_data3_in_padmisc2core   	;
wire			sd_mmc_cmd_in_padmisc2core 	;
wire			det_pin_padmisc2core       	;
wire			wr_prct_pin_padmisc2core   	;
wire			test_h_padmisc2core        	;

wire			test_done_core2padmisc     	;
wire			fail_h_core2padmisc        	;
wire			ms_clk_core2padmisc        	;
wire			ms_bsst_core2padmisc       	;
wire			ms_pw_ctrl_core2padmisc    	;
wire			ms_serout_core2padmisc     	;
wire			msde_core2padmisc          	;
wire	[3:0]	sd_data_oej_core2padmisc  	;
//wire			sd_data1_oej_core2padmisc  	;
//wire			sd_data2_oej_core2padmisc  	;
//wire			sd_data3_oej_core2padmisc  	;
wire			sd_mmc_cmd_oej_core2padmisc	;
wire	[3:0]	sd_data_out_core2padmisc  	;
//wire			sd_data1_out_core2padmisc  	;
//wire			sd_data2_out_core2padmisc  	;
//wire			sd_data3_out_core2padmisc  	;
wire			sd_mmc_cmd_out_core2padmisc	;
wire			sd_mmc_clk_core2padmisc    	;

wire			sd_ip_enable_core2padmisc	;
wire			ms_ip_enable_core2padmisc	;
//====================	END MS/SD INTERFACE	========================

//=================GI Interface(from broadon)=======================
wire	[7:0]	di_in_padmisc2core,	di_in_pad2padmisc,
				di_out_core2padmisc,di_out_padmisc2pad,
				di_oe_padmisc2pad;
//PAD BOND signals
wire	[2:0]	bond_option_pad2padmisc;

//TEST_MODE signals
wire	[2:0]	dsp_bist_vec_core2padmisc;
wire	[10:0]	video_bist_vec_core2padmisc_10_0;//video_bist_vec from core [10:0]JFR
wire	[11:0]	video_bist_vec_core2padmisc	=	{1'b0,video_bist_vec_core2padmisc_10_0};

wire	[89:0]	scan_dout;					// scan chain dout from core
wire	[89:0]	scan_din;					// scan chain din to core
wire	[47:0]	scan_dout_mux2padmisc;		// scan chain dout to pad_misc
wire	[47:0]	scan_din_padmisc2mux;		// scan chain din from pad_misc

//=================invoke the core ===============================
`ifdef INC_CORE
core    CORE    (
//------chipset bus
        // SDRAM & FLASH
        .ram_addr               (ram_addr_core2padmisc          ),
        .ram_ba                 (ram_ba_core2padmisc            ),
        .ram_dq_out             (ram_dq_core2padmisc            ),
        .ram_dq_in              (ram_dq_padmisc2core   			),
        .ram_dq_oe_             (ram_dq_oe_core2padmisc_        ),
        .ram_cas_               (ram_cas_core2padmisc           ),
        .ram_we_                (ram_we_core2padmisc            ),
        .ram_ras_               (ram_ras_core2padmisc           ),
        .ram_cke                (ram_cke_out            		),//no use by now. yuky, 2002-10-09
        .ram_dqm                (ram_dqm_core2padmisc           ),
        .ram_cs_                (ram_cs_core2padmisc            ),
        // EEPROM
        .eeprom_addr            (rom_addr_core2padmisc       ),
        .eeprom_rdata           (rom_rdata_padmisc2core      ),
 //       .eeprom_ce_             (rom_ce_core2padmisc         ),
        .eeprom_oe_             (rom_oe_core2padmisc         ),
        .eeprom_we_             (rom_we_core2padmisc         ),
        .rom_data_oe_           (rom_data_oe_core2padmisc_   ),
        .rom_addr_oe            (rom_addr_oe_core2padmisc    ),
        .eeprom_ale				(eeprom_ale_core2padmisc	 ),
//		.eeprom_ale_oe_			(eeprom_ale_oe_core2padmisc_ ),
        .eeprom_wdata           (rom_wdata_core2padmisc      ),
        .rom_en_                (rom_en_core2padmisc_        ),
        // PCI
        .oe_ad_                 (pci_ad_oe_core2padmisc_        ),
        .out_ad                 (pci_ad_core2padmisc            ),
        .oe_cbe_                (pci_cbe_oe_core2padmisc_       ),
        .out_cbe_               (pci_cbe_core2padmisc_          ),
        .oe_frame_              (pci_frame_oe_core2padmisc_     ),
        .out_frame_             (pci_frame_core2padmisc_        ),
        .oe_irdy_               (pci_irdy_oe_core2padmisc_      ),
        .out_irdy_              (pci_irdy_core2padmisc_         ),
        .oe_devsel_             (pci_devsel_oe_core2padmisc_    ),
        .out_rst_				(pci_rst_core2padmisc_			),
        .out_devsel_            (pci_devsel_core2padmisc_       ),
        .oe_trdy_               (pci_trdy_oe_core2padmisc_      ),
        .out_trdy_              (pci_trdy_core2padmisc_         ),
        .oe_stop_               (pci_stop_oe_core2padmisc_      ),
        .out_stop_              (pci_stop_core2padmisc_         ),
        .oe_serr_               (pci_serr_oe_core2padmisc_      ),
        .out_serr_              (pci_serr_core2padmisc_         ),
        .oe_perr_               (pci_perr_oe_core2padmisc_      ),
        .out_perr_              (pci_perr_core2padmisc_         ),
        .oe_par_                (pci_par_oe_core2padmisc_       ),
        .out_par                (pci_par_core2padmisc           ),
        .out_gnt_               (pci_gnt_core2padmisc_          ),
        .in_req_                (pci_req_padmisc2core_          ),
        .in_ad                  (pci_ad_padmisc2core            ),
        .in_cbe_                (pci_cbe_padmisc2core_          ),
        .in_frame_              (pci_frame_padmisc2core_        ),
        .in_irdy_               (pci_irdy_padmisc2core_         ),
        .in_devsel_             (pci_devsel_padmisc2core_       ),
        .in_trdy_               (pci_trdy_padmisc2core_         ),
        .in_stop_               (pci_stop_padmisc2core_         ),
        .in_par                 (pci_par_padmisc2core           ),
        .in_serr_               (pci_serr_padmisc2core_         ),
        .in_perr_               (pci_perr_padmisc2core_         ),
        .ext_int                (pci_inta_padmisc2core_         ),
        //GPIO
        .gpio_out               (gpio_core2padmisc    			),
        .gpio_oe_               (gpio_oe_core2padmisc_          ),
        .gpio_in                (gpio_padmisc2core      		),
        .gpio_set2_out          (gpio_set2_core2padmisc    		),
        .gpio_set2_oe_          (gpio_set2_oe_core2padmisc_     ),
        .gpio_set2_in           (gpio_set2_padmisc2core      	),
        // Audio
        .out_i2so_data0         (i2so_data3_core2padmisc        ),//for M3355 compatible,lucky,2004/03/01
		.out_i2so_data1			(i2so_data0_core2padmisc		),//for M3355 compatible,lucky,2004/03/01
		.out_i2so_data2			(i2so_data1_core2padmisc		),//for M3355 compatible,lucky,2004/03/01
		.out_i2so_data3			(i2so_data2_core2padmisc		),//for M3355 compatible,lucky,2004/03/01
		.out_i2so_data4			(i2so_data4_core2padmisc		),
	//	.ac97_i2s_sel			(ac97_i2s_sel					),	//no use by now, yuky, 2002-10-09
		.i2so_ch10_en			(i2so_ch10_en_core2padmisc		),
        .out_i2so_lrck          (i2so_lrck_core2padmisc         ),
        .out_i2so_bick          (i2so_bick_core2padmisc         ),
        .out_i2si_lrck  		(i2si_lrck_core2padmisc			),
		.out_i2si_bick			(i2si_bck_core2padmisc			),
        .in_i2si_data           (i2si_data_padmisc2core         ),
  		.slave_i2si_bick		(slave_i2si_bick_padmisc2core	),
  		.slave_i2si_lrck		(slave_i2si_lrck_padmisc2core   ),
		.i2si_mode_sel  		(i2si_mode_sel_core2padmisc     ),
        .in_i2so_mclk           (i2so_mclk_gen2core				),
        .iadc_mclk    			(iadc_mclk_gen2core				),
		.iadc_smtsen  			(iadc_smtsen_padmisc2core		),
		.iadc_smtsmode			(iadc_smtsmode_padmisc2core		),
		.iadc_smts_err			(iadc_smts_err_core2padmisc		),
		.iadc_smts_end			(iadc_smts_end_core2padmisc		),
		.iadc_adcclkp1			(iadc_clkp1_core2padmisc		),
		.iadc_adcclkp2			(iadc_clkp2_core2padmisc		),
		.iadc_tst_mod 			(iadc_tst_mod_padmisc2core		),
        //spdif
        .out_spdif              (spdif_core2padmisc             ),
        .in_spdif				(spdif_in_data_padmisc2core		),
        //GI Interface
        //--system
        .alt_boot				(alt_boot_padmisc2core			),
//		.test_ena	        	(test_ena_padmisc2core	    	),
		.gc_rst_	            (gc_rst_core2padmisc	    	),
		.pin_rst_				(cold_rst_to_gi_				),
		.gi_chip_rst_			(cold_rst_in_					),
		.gi_clk					(gi_clk							),
		.gi_clk_lock			(f27_pll_lock					),
		//--serial prom
		.i2c_clk	        	(i2c_clk_core2padmisc	    	),
		.i2c_din	        	(i2c_din_padmisc2core	    	),
		.i2c_dout	        	(i2c_dout_core2padmisc	    	),
		.i2c_doe	        	(i2c_doe_core2padmisc	    	),
		//--dsic interface
		.di_in		        	(di_in_padmisc2core		    	),
		.di_out		        	(di_out_core2padmisc	    	),
		.di_oe		        	(di_oe_core2padmisc		    	),
		.di_brk_in	        	(di_brk_in_padmisc2core	    	),
		.di_brk_out	        	(di_brk_out_core2padmisc 		),
		.di_brk_oe	        	(di_brk_oe_core2padmisc	    	),
		.di_dir		        	(di_dir_padmisc2core	    	),
		.di_hstrb	        	(di_hstrb_padmisc2core	    	),
		.di_dstrb	        	(di_dstrb_core2padmisc	    	),
		.di_err		        	(di_err_core2padmisc	    	),
		.di_cover	        	(di_cover_core2padmisc	    	),
		.di_stb					(di_stb_padmisc2core			),

		.ais_clk	        	(ais_clk_padmisc2core			),
		.ais_lr		        	(ais_lr_padmisc2core	    	),
		.ais_d		        	(ais_d_core2padmisc		    	),
         //South Bridge
		//IR
        .ir_rx                  (irrx_padmisc2core  			),
		//i2C
        .scb_scl_out            (scbscl_core2padmisc    		),
        .scb_scl_oe             (scbscl_oe_core2padmisc 		),
        .scb_sda_out            (scbsda_core2padmisc    		),
        .scb_sda_oe             (scbsda_oe_core2padmisc 		),
        .scb_scl_in             (scbscl_padmisc2core    		),
        .scb_sda_in             (scbsda_padmisc2core    		),
		//UART
		.uart_tx				(uart_tx_core2padmisc				),
		.uart_rx				(uart_rx_padmisc2core				),
        // USB
        .p1_ls_mode             (p1_ls_mode_core2padmisc        ),
        .p1_txd                 (p1_txd_core2padmisc            ),
        .p1_txd_oej             (p1_txd_oej_core2padmisc        ),
        .p1_txd_se0             (p1_txd_se0_core2padmisc        ),
        .p2_ls_mode             (p2_ls_mode_core2padmisc        ),
        .p2_txd                 (p2_txd_core2padmisc            ),
        .p2_txd_oej             (p2_txd_oej_core2padmisc        ),
        .p2_txd_se0             (p2_txd_se0_core2padmisc        ),
        .usb_pon_               (usbpon_core2padmisc_    		),	//Dont care the polarity (low or high)
        .i_p1_rxd               (i_p1_rxd_padmisc2core          ),
        .i_p1_rxd_se0           (i_p1_rxd_se0_padmisc2core      ),
        .i_p2_rxd               (i_p2_rxd_padmisc2core          ),
        .i_p2_rxd_se0           (i_p2_rxd_se0_padmisc2core      ),
        .usb_overcur_           (usbovc_padmisc2core   			),	//Dont care the polarity (low or high)
		//VIDEO
        .vdata_oe_              (vdata_oe_core2padmisc_         ),
        .vdata_out              (vdata_core2padmisc             ),
        .h_sync_out_            (de_h_sync_core2padmisc_   		),		// DE H_SYNC#
        .h_sync_oe_             (h_sync_oe_core2padmisc_        ),
//        .in_h_sync_             (video_in_hsync_padmisc2core_   ),
        .v_sync_out_            (de_v_sync_core2padmisc_		),		// DE_V_SYNC#
        .v_sync_oe_             (v_sync_oe_core2padmisc_        ),
//        .in_v_sync_             (video_in_vsync_padmisc2core_   ),
		// TV Encoder intf
		.tvenc_vdi4vdac			(tvenc_vdi4vdac_padmisc2core	),
		.tvenc_vdo4vdac			(tvenc_vdo4vdac_core2padmisc	),
		.tvenc_idump			(tvenc_idump_core2pad			),
		.tvenc_iext				(tvenc_iext_core2pad			),	// IEXT changed to output
		.tvenc_iext1			(tvenc_iext1_core2pad			),	// IEXT1 is output
		.tvenc_iout1			(tvenc_iout1_core2pad			),
		.tvenc_iout2			(tvenc_iout2_core2pad			),
		.tvenc_iout3			(tvenc_iout3_core2pad			),
		.tvenc_iout4			(tvenc_iout4_core2pad			),
		.tvenc_iout5			(tvenc_iout5_core2pad			),
		.tvenc_iout6			(tvenc_iout6_core2pad			),
		.tvenc_iref1			(tvenc_iref1_core2pad			),
//		.tvenc_iref2			(tvenc_iref2_core2pad			),
		.tvenc_gnda				(gnda_tvdac						),	//TVENC GND JFR 031112
		.tvenc_gnda1			(gnda1_tvdac					),	// Ground for DAC, Norman 2003-12-05
		.tvenc_vsud				(vsud_tvdac						),	// Ground for DAC, Norman 2003-12-05
		.tvenc_vd33a			(vd33a_tvdac					),	//TVENC PWR JFR 031112
		.tvenc_h_sync_			(tvenc_h_sync_core2padmisc_		),	// TV Encoder H_SYNC#
		.tvenc_v_sync_      	(tvenc_v_sync_core2padmisc_     ),	// TV Encoder V_SYNC#
		.tvenc_vdactest			(tvenc_test_mode				),
		//VIDEO IN
		.VIN_PIXEL_DATA	       	(video_in_pixel_data_padmisc2core	),
		.VIN_PIXEL_CLK	        (video_in_pixel_clk_padmisc2core	),
		.VIN_H_SYNCJ	        (video_in_hsync_padmisc2core_		),
		.VIN_V_SYNCJ	        (video_in_vsync_padmisc2core_		),
        // IDE Interface
        .atadiow_out_           (atadiow_core2padmisc_          ),
        .atadior_out_           (atadior_core2padmisc_          ),
        .atacs0_out_            (atacs0_core2padmisc_           ),
        .atacs1_out_            (atacs1_core2padmisc_           ),
        .atadd_out              (atadd_core2padmisc             ),
        .atadd_oe_              (atadd_oe_core2padmisc_ 		),
        .atada_out              (atada_core2padmisc             ),
        .atareset_out_          (atareset_core2padmisc_         ),
        .atareset_oe_			(atareset_oe_core2padmisc_		),
        .atadmack_out_          (atadmack_core2padmisc_         ),
        .ataiordy_in            (ataiordy_padmisc2core          ),
        .ataintrq_in            (ataintrq_padmisc2core          ),
        .atadd_in               (atadd_padmisc2core             ),
        .atadmarq_in            (atadmarq_padmisc2core          ),
// System Interrupt, output from Hostbridge
        .x_err_int              (x_err_int_core2padmisc         ),
        .x_ext_int              (x_ext_int_core2padmisc         ),
//NO USE NOW
//CPU Interface
        // MIPS bus interface
        .sysad_out              (          ),
        .sysad_in               (          ),
        .syscmd_out             (          ),
        .syscmd_in              (          ),
        .sysout_oe              (          ),
        .sys_int_               (sys_int_padmisc2core_          ),
        .sys_nmi_               (sys_nmi_padmisc2core_          ),
        .pvalid_                (          ),
        .preq_                  (          ),
        .pmaster_               (          ),
        .eok_                   (          ),
        .evalid_                (          ),
        .ereq_                  (          ),
        // JTAG interface
        .j_tdi                  (j_tdi_padmisc2core				),
        .j_tms                  (j_tms_padmisc2core				),
        .j_tclk                 (j_tclk_pad2padmisc       		),		// j_tclk from pad not from padmisc
        .j_rst_                 (j_trst_padmisc2core_      		),
        .j_tdo                  (j_tdo_core2padmisc             ),
		// CPU BIST intf
		.cpu_bist_mode			(cpu_bist_mode_padmisc2core		),
		.cpu_scan_test_mode		(cpu_scan_test_mode_padmisc2core),
		.cpu_bist_error_vec		(cpu_bist_error_vec_core2padmisc),
		.cpu_bist_finish		(cpu_bist_finish_core2padmisc	),
		// Clock signals
        .sb_clk                 (sb_clk_gen2core        		),
        .usb_clk                (usb_clk_gen2core       		),
        .cpu_clk                (cpu_clk_gen2core       		), // pipeline clock   (CPU core)
        .mem_clk                (inter_mem_clk_gen      		), // transfer clock   (Memory Controller, IO, ROM)
        .pci_clk                (pci_clk_gen2core       		),
        .cpu_clk_src            (cpu_clk_gen2core   			), // same as the pipeline clock (did not mask by JTAG mode
        .dsp_clk                (dsp_clk_gen2core       		),
        .spdif_clk              (spdif_clk_gen2core     		),
        .mbus_clk               (mbus_clk_gen2core      		),
		.ata_clk				(ata_clk_gen2core				),
		.servo_clk				(servo_clk_gen2core				),
        .pad_boot_config        (pad_boot_config        		),
		//TO VIDEO CLOCK GEN
		.inter_only				(inter_only_core2gen			),		// Interlace output
		.dvi_rwbuf_sel			(dvi_rwbuf_sel_core2gen			),		// DVI buffer read/write
		.cvbs2x_clk				(cvbs2x_clk_gen2core			),		// 54MHZ clock
		.cvbs_clk				(cvbs_clk_gen2core				),		// 27MHz clock
		.cav_clk				(cav_clk_gen2core				),		// 54 or 27 MHZ clock
		.dvi_buf0_clk			(1'b0							),		// CAV_CLK or VIDEO_CLK for buffer 0
		.dvi_buf1_clk			(1'b0							),		// CAV_CLK or VIDEO_CLK for buffer 1
		.video_clk				(video_clk_gen2core				),		// VIDEO clock output
		.vsb_clk				(vsb_clk_gen2core				),		// VSB clock 80M
		.tv_f108m_clk			(tv_f108m_clk_gen2core			),		// tv_encoder 108MHz clock
		.tv_cvbs2x_clk			(tv_cvbs2x_clk_gen2core			),		// tv_encoder 54MHZ clock
		.tv_cvbs_clk			(cvbs_clk_gen2core				),		// tv_encoder 27MHz clock, use the cvbs clock too
		.tv_cav2x_clk			(tv_cav2x_clk_gen2core			),		// tv_encoder 108 or 54 MHZ clock
		.tv_cav_clk				(tv_cav_clk_gen2core			),		// tv_encoder 54 or 27 MHZ clock
		//	PLL control
		.cpu_clk_pll_m_value	(cpu_clk_pll_m_value_core2str	),	// new cpu_clk_pll m parameter
		.cpu_clk_pll_m_tri		(cpu_clk_pll_m_tri_core2str		),  // cpu_clk_pll m modification trigger
		.cpu_clk_pll_m_ack		(cpu_clk_pll_m_ack_str2core		),  // cpu_clk_pll m modification ack
		.mem_clk_pll_m_value	(mem_clk_pll_m_value_core2str	),  // new mem_clk_pll m parameter
		.mem_clk_pll_m_tri		(mem_clk_pll_m_tri_core2str		),  // mem_clk_pll m modification trigger
		.mem_clk_pll_m_ack		(mem_clk_pll_m_ack_str2core		),  // mem_clk_pll m modification ack
		.work_mode_value		(work_mode_value_core2str		),	// new work_mode
		.work_mode_tri  		(work_mode_tri_core2str			),  // work_mode modification trigger
		.work_mode_ack  		(work_mode_ack_str2core			),  // work_mode modification ack
//	clock frequency select, clock misc control
		.ide_fs					(IDE_FS_core2gen				),	// ide clock frequency select
		.dsp_fs					(DSP_FS_core2gen				),	// dsp clock frequency select
		.dsp_div_src_sel		(DSP_DIV_SRC_SEL_core2gen		),	// dsp source clock select
		.pci_fs					(PCI_FS_core2gen				),	// pci clock frequency select
		.ve_fs					(VE_FS_core2gen					),	// ve clock frequency select
		.ve_div_src_sel			(VE_DIV_SRC_SEL_core2gen		),	// ve source clock select
//		.pci_clk_out_sel		(pci_clk_out_sel_core2gen		),	//
		.svga_mode_en			(svga_mode_en_core2gen			),	// SVGA mode output enable
		.tv_mode				(tv_mode_core2gen				),	// video core output pixel for tv mode
		.out_mode_etv_sel		(out_mode_etv_sel_core2gen		),	// output pixel clock select, 0 -> 54 MHz, 1 -> 27 MHz
//		.tv_enc_clk_sel			(tv_enc_clk_sel_core2gen		),	// TV encoder clock setting
		//MEMORY clock delay chain
        .mem_dly_sel            (mem_dly_sel            		),
        .mem_rd_dly_sel			(mem_rd_dly_sel					),
		.test_dly_chain_sel		(test_dly_chain_sel				),	// TEST_DLY_CHAIN delay select
		.test_dly_chain_flag	(test_dly_chain_flag			),	// TEST_DLY_CHAIN flag output
		//	external interface control
		.audio_pll_fout_sel		(audio_pll_fout_sel_core2gen	),	// Audio PLL FOUT frequency select
		.i2s_mclk_sel			(i2s_mclk_sel_core2gen			),	// I2S MCLK source select
		.i2s_mclk_out_en_		(i2so_mclk_oe_core2padmisc_		),	// I2S MCLK output enable
		.spdif_clk_sel			(spdif_clk_sel_core2gen			),	// S/PDIF clock select
		.i2so_data2_1_gpio_en	(i2so_data2_1_gpio_en_core2padmisc),	// I2SO DATA[2:1] GPIO enable
		.i2so_data3_gpio_en		(i2so_data3_gpio_en_core2padmisc),	// I2SO DATA3 GPIO enable
		.xsflag_gpio_en			(xsflag_gpio_en_core2padmisc	),	// Xsflag[1:0] share with gpio[25:24] enable
		.spdif_gpio_en			(spdif_gpio_en_core2padmisc		),	// Spdif share with gpio[21] en
		.uart_gpio_en			(uart_gpio_en_core2padmisc		),	// Uart_tx share with gpio[24] en
		.hv_sync_gpio_en		(hv_sync_gpio_encore2padmisc	),	// H_sync_, v_sync_ share with gpio[11:10]
		.hv_sync_en				(hv_sync_en_core2padmisc		),	// h_sync_, v_sync_ enable on the xsflag[1:0] pins
		.i2si_clk_en			(i2si_clk_en_core2padmisc		),	// I2S input data enable on the gpio[7] pin
		.scb_en					(scb_en_core2padmisc			),	// SCB interface enable on the gpio[6:5] pins
		.servo_gpio_en			(sv_gpio_en_core2padmisc		),	// servo_gpio[2:0] enable on the gpio[2:0] pins
		.ext_ide_device_en		(ext_ide_device_en_core2padmisc	),	// external IDE device enable
//		.pci_gntb_en			(pci_gntb_en_core2padmisc		),	// PCI GNTB# enable
		.vdata_en				(vdata_en_core2padmisc			),	// video data enable
		.sync_source_sel		(sync_source_sel_core2padmisc	),	// sync signal source select, 0 -> TVENC, 1 -> DE.
		.video_in_intf_en		(video_in_intf_en_core2padmisc	),	// video in interface enable
//		.pci_mode_ide_en		(pci_mode_ide_en_core2padmisc	),	// pci_mode ide interface enable
		.sdram_cs1j_en			(sdram_cs1j_en_core2padmisc		),	// sdram cs1# enable
		.ata_share_mode_2_en	(ata_share_mode_2_en_core2padmisc),	// atadmack_ and atada[0] share with rom_oe_ and rom_we_
		.ata_ad_src_sel			(ata_ad_src_sel_core2padmisc	),	// rom_addr[18:17] bond selection
		.ide_active				(ide_active_core2padmisc		),	// ide interface is active in the pci mode
		.standby_mode_en		(standby_mode_en_core2clockgen	),	// system standby mode
//	PAD Driever Capability
		.gpio_pad_driv	   		(gpio_pad_driv_core2pad 	   		),	// gpio pad driver capability
		.rom_da_pad_driv    	(rom_da_pad_driv_core2pad 	    	),  // rom data/address driver capability
		.sdram_data_pad_driv	(sdram_data_pad_driv_core2pad		),  // sdram data driver capability
		.sdram_ca_pad_driv		(sdram_ca_pad_driv_core2pad			),  // sdram command/address driver capability
		//SERVO INTERFACE
		.sv_pmata_somd			(sram_ifx_padmisc2core[23:0]		), // ok
		.sv_pmsvd_trcclk		(sv_pmsvd_trcclk_padmisc2core		),	// ok
		.sv_pmsvd_trfclk		(sv_pmsvd_trfclk_padmisc2core		),	// ok
		.sv_sv_tslrf			(sv_sv_tslrf_core2padmisc			),	// ok
		.sv_sv_tplck			(sv_sv_tplck_core2padmisc			),	// ok
//		.sv_sv_dev_true  		(sv_sv_dev_true_core2padmisc     	), // hardwire 1 in sv_servo
		.sv_tst_svoio_i			(sv_gpio_padmisc2core				), // ok
		.sv_svo_svoio_o			(sv_gpio_core2padmisc				), // ok
		.sv_svo_svoio_oej		(sv_gpio_oe_core2padmisc_			), // ok
		.sv_tst_addata			(sv_tst_addata_padmisc2core			),	// ok
		.sv_reg_rctst_2  		(sv_reg_rctst_2_core2padmisc       	),	// ok
//		sv_top_sv_mppdn		,
//		.sv_dmckg_sysclk 		(sv_dmckg_sysclk					),	//Need modify
		.sv_mckg_dspclk  		(f54m_clk							),	// use  54MHz clock JFR 2004-01-02
//		.sv_sycg_svoata 		(sv_sycg_svoata_padmisc2core 		),	// Useless
//		sv_sv_reg_c2ftst       ,
//		sv_pmdm_ideslave      ,
		.sv_pmsvd_tplck      	(sv_pmsvd_tplck_padmisc2core        ),	// Ok
		.sv_pmsvd_tslrf      	(sv_pmsvd_tslrf_padmisc2core        ),	// Ok
		.sv_sv_tst_pin			(sv_tst_pin_core2padmisc			),	// Ok
//		.sv_sv_reg_tstoen    	(sv_reg_tstoen       				), // Dangling
		.sv_pmsvd_c2ftstin		(sv_pmsvd_c2ftstin_padmisc2core		),	// Ok
		.sv_sycg_somd			(sram_ifx_padmisc2core[23:0]		),	// Ok
		// Servo analog
//		.sv_sycg_updbgout 		(sv_sycg_updbgout					), // NB controller output
//		.sv_cpu_gpioin			(sv_cpu_gpioin						), // Dangling
//		.sv_sycg_burnin			(sv_sycg_burnin						), //Need modify [1:0]
		//	External uP interface
		.sv_ex_up_in			(ext_up_padmisc2core				),		// Ok
		.sv_ex_up_out			(ext_up_core2padmisc				),		// Ok
		.sv_ex_up_oe_			(ext_up_oe_core2padmisc_			),		// Ok
		// Servo Atapi inf
		.sv_sv_hi_hdrq_o     	(sv_atadmarq_core2padmisc			),		// Ok
		.sv_sv_hi_hdrq_oej   	(sv_sv_hi_hdrq_oe_core2padmisc_     ),		// Useless
		.sv_sv_hi_hcs16j_o   	(sv_ataiocs16_core2padmisc_			),		// Ok
		.sv_sv_hi_hcs16j_oej 	(sv_ataiocs16_oe_core2padmisc_		),		// Ok
		.sv_sv_hi_hint_o     	(sv_ataintrq_core2padmisc			),		// Ok
		.sv_sv_hi_hint_oej   	(sv_sv_hi_hint_oe_core2padmisc_     ),		// Useless
		.sv_sv_hi_hpdiagj_o  	(sv_atapdiag_core2padmisc_			),		// Ok
		.sv_sv_hi_hpdiagj_oej	(sv_atapdiag_oe_core2padmisc_		),		// Ok
		.sv_sv_hi_hdaspj_o   	(sv_atadasp_core2padmisc_			),		// Ok
		.sv_sv_hi_hdaspj_oej 	(sv_atadasp_oe_core2padmisc_		),		// Ok
		.sv_sv_hi_hiordy_o   	(sv_ataiordy_core2padmisc			),		// Ok
		.sv_sv_hi_hiordy_oej 	(sv_sv_hi_hiordy_oe_core2padmisc_   ),		// Useless
		.sv_sv_hi_hd_o			(sv_atadd_core2padmisc				),		// Ok
		.sv_sv_hi_hd_oej     	(sv_atadd_oe_core2padmisc_1bit_     ),		// Ok
		.sv_pmata_hrstj      	(sv_atareset_padmisc2core_			),		// Ok
		.sv_pmata_hcs1j      	(sv_atacs0_padmisc2core_        	),		// Ok
		.sv_pmata_hcs3j      	(sv_atacs1_padmisc2core_	        ),		// Ok
		.sv_pmata_hiorj      	(sv_atadior_padmisc2core_			),		// Ok
		.sv_pmata_hiowj      	(sv_atadiow_padmisc2core_			),		// Ok
		.sv_pmata_hdackj     	(sv_atadmack_padmisc2core_			),		// Ok
		.sv_pmata_hpdiagj    	(sv_atapdiag_padmisc2core_			),		// Ok
		.sv_pmata_hdaspj     	(sv_atadasp_padmisc2core_			),		// Ok
		.sv_pmata_ha			(sv_atada_padmisc2core				),		// Ok
		.sv_pmata_hd			(sv_atadd_padmisc2core				),		// Ok
		// Servo Test
//		.sv_sv_svo_sflag		(sv_sv_svo_sflag_core2padmisc		),
		.sv_sv_svo_sflag		(sv_xsflag_core2padmisc[1:0]		), // to PAD_MISC, output only
		.sv_sv_svo_soma			(sram_ifx_core2padmisc[36:24]		), // ok
		.sv_sv_svo_somd			(sram_ifx_core2padmisc[23:0]		), // ok
		.sv_sv_svo_somd_oej  	(sram_ifx_oe_core2padmisc_[21:0]    ), // ok
		.sv_sv_svo_somdh_oej 	(sram_ifx_oe_core2padmisc_[23:22]   ), // ok
//		.sv_sv_reg_dvdram_toe	(sv_sv_reg_dvdram_toe   			), // Dangling
//		.sv_sv_reg_dfctsel   	(sv_sv_reg_dfctsel      			), // Dangling
//		.sv_sv_reg_trfsel 		(sv_sv_reg_trfsel 					), // Dangling
//		.sv_sv_reg_flgtoen 		(sv_sv_reg_flgtoen 					), // Dandling
//		.sv_sv_reg_gpioten		(sv_sv_reg_gpioten					), // Dandling
//		.sv_sv_reg_13e_7		(sv_sv_reg_13e_7					), // Dandling
//		.sv_sv_reg_13e_6		(sv_sv_reg_13e_6					), // Dandling
//		.sv_reg_trg_plcken		(									),
//		.sv_reg_sv_tplck_oej	(									), // useless in 3357
		//	Servo analog ANA_DTSV
		.sv_pad_clkref			(f27m_clk			  ),				// 27 MHz from the crystall
		.sv_xadcin       		(XADCIN       	      ),
		.sv_xadcip       		(XADCIP       	      ),
		.sv_xatton       		(XATTON       	      ),
		.sv_xattop       		(XATTOP       	      ),
		.sv_xbiasr       		(XBIASR       	      ),
		.sv_xcdld        		(XCDLD        	      ),
		.sv_xcdpd        		(XCDPD        	      ),
		.sv_xcdrf        		(XCDRF        	      ),
		.sv_xcd_a        		(XCD_A        	      ),
		.sv_xcd_b        		(XCD_B        	      ),
		.sv_xcd_c        		(XCD_C        	      ),
		.sv_xcd_d        		(XCD_D        	      ),
		.sv_xcd_e        		(XCD_E        	      ),
		.sv_xcd_f        		(XCD_F        	      ),
		.sv_xcelpfo      		(XCELPFO      	      ),
		.sv_xdpd_a       		(XDPD_A       	      ),
		.sv_xdpd_b       		(XDPD_B       	      ),
		.sv_xdpd_c       		(XDPD_C       	      ),
		.sv_xdpd_d       		(XDPD_D       	      ),
		.sv_xdvdld       		(XDVDLD       	      ),
		.sv_xdvdpd       		(XDVDPD       	      ),
		.sv_xdvdrfn      		(XDVDRFN      	      ),
		.sv_xdvdrfp      		(XDVDRFP      	      ),
		.sv_xdvd_a       		(XDVD_A       	      ),
		.sv_xdvd_b       		(XDVD_B       	      ),
		.sv_xdvd_c       		(XDVD_C       	      ),
		.sv_xdvd_d       		(XDVD_D       	      ),
		.sv_xfelpfo      		(XFELPFO      	      ),
		.sv_xfocus       		(XFOCUS       	      ),
		.sv_xgmbiasr     		(XGMBIASR     	      ),
		.sv_xlpfon       		(XLPFON       	      ),
		.sv_xlpfop       		(XLPFOP       	      ),
		.sv_xpdaux1      		(XPDAUX1      	      ),
		.sv_xpdaux2      		(XPDAUX2      	      ),
		.sv_xsblpfo      		(XSBLPFO      	      ),
		.sv_xsfgn        		(XSFGN        	      ),
		.sv_xsfgp        		(XSFGP        	      ),

		.sv_pata_sflag			(sv_xsflag_pad2padmisc[1:0]),	// signal from PAD to padmisc
		.sv_pmata_sflag			(sv_xsflag_padmisc2pad[1:0]),	// ok	carcy
		.sv_pmata_sflag_oej		(sv_xsflag_oe_padmisc2pad_[1:0]),	// ok	carcy

		.sv_xsflag				(XSFLAG				),	//	carcy add 2003/11/17 18:52 				),	// need check
		.sv_xslegn       		(XSLEGN             ),
		.sv_xslegp       		(XSLEGP             ),
		.sv_xspindle     		(XSPINDLE           ),
		.sv_xtelp        		(XTELP              ),
		.sv_xtelpfo      		(XTELPFO            ),
		.sv_xtestda      		(XTESTDA            ),
		.sv_xtexo        		(XTEXO              ),
		.sv_xtrack       		(XTRACK             ),
		.sv_xtray        		(XTRAY              ),
		.sv_xvgain       		(XVGAIN             ),
		.sv_xvgaip       		(XVGAIP             ),
		.sv_xvref15      		(XVREF15            ),
		.sv_xvref21      		(XVREF21            ),
		//Servo power
		.sv_vd33d        		(vddio            	),	// Use the IO power
		.sv_vd18d        		(vddcore           	),	// Use the core power
		.sv_vdd3mix1			(VDD3MIX1			),
		.sv_vdd3mix2			(VDD3MIX2			),
		.sv_avdd_ad      		(AVDD_AD         	),
		.sv_avdd_att     		(AVDD_ATT         	),
		.sv_avdd_da      		(AVDD_DA          	),
		.sv_avdd_dpd     		(AVDD_DPD        	),
		.sv_avdd_lpf     		(AVDD_LPF        	),
		.sv_avdd_rad     		(AVDD_RAD        	),
		.sv_avdd_ref     		(AVDD_REF        	),
		.sv_avdd_svo     		(AVDD_SVO        	),
		.sv_avdd_vga     		(AVDD_VGA         	),
		.sv_avss_ad      		(AVSS_AD         	),
		.sv_avss_att     		(AVSS_ATT         	),
		.sv_avss_da      		(AVSS_DA         	),
		.sv_avss_dpd     		(AVSS_DPD         	),
		.sv_avss_ga      		(AVSS_GA         	),
		.sv_avss_gd      		(AVSS_GD        	),
		.sv_avss_lpf     		(AVSS_LPF         	),
		.sv_avss_rad     		(AVSS_RAD         	),
		.sv_avss_ref     		(AVSS_REF         	),
		.sv_avss_svo     		(AVSS_SVO         	),
		.sv_avss_vga     		(AVSS_VGA         	),
		.sv_azec_gnd     		(gnd	         	),	//	digital ground
		.sv_azec_vdd     		(vddcore         	),	//	vdd core 1.8v
		.sv_dv18_rcka    		(DV18_RCKA        	),
		.sv_dv18_rckd    		(DV18_RCKD        	),
		.sv_dvdd         		(vddcore		  	),	//	vdd core 1.8v
		.sv_dvss         		(gnd             	),	//	digital ground
		.sv_dvss_rcka    		(DVSS_RCKA        	),
		.sv_dvss_rckd    		(DVSS_RCKD        	),
		.sv_fad_gnd      		(gnd	          	),	//	digital ground
		.sv_fad_vdd      		(vddcore         	),	//  vdd core 1.8v
		.sv_gndd         		(gnd             	),	//	digital ground

//	SERVO control
		.sv_tst_pin_en			(sv_tst_pin_en_core2padmisc		),	// SERVO test pin enable
		.sv_ide_mode_part0_en	(sv_ide_mode_part0_en_core2padmisc),// ide mode SERVO test pin part 0 enable
		.sv_ide_mode_part1_en	(sv_ide_mode_part1_en_core2padmisc),// ide mode SERVO test pin part 1 enable
		.sv_ide_mode_part2_en	(sv_ide_mode_part2_en_core2padmisc),// ide mode SERVO test pin part 2 enable
		.sv_ide_mode_part3_en	(sv_ide_mode_part3_en_core2padmisc),// ide mode SERVO test pin part 3 enable
		.sv_c2ftstin_en			(sv_c2ftstin_en_core2padmisc	),	// SERVO c2 flag enable
		.sv_trg_plcken			(sv_trg_plcken_core2padmisc		),	// SERVO tslrf and tplck enable
		.sv_pmsvd_trcclk_trfclk_en (sv_pmsvd_trcclk_trfclk_en_core2padmisc),	// SERVO pmsvd_trcclk, pmsvd_trfclk enable

//============================	MS/SD Interface ====================
		.ms_ins				(ms_ins_padmisc2core				)	,   //input	port
		.ms_serin_in   	    (ms_serin_in_padmisc2core   		)	,	//input	port
		.sd_data_in   	    (sd_data_in_padmisc2core   		)	,	//input	port
//		.sd_data1_in   	    (sd_data1_in_padmisc2core   		)	,   //input	port
//		.sd_data2_in   	    (sd_data2_in_padmisc2core   		)	,   //input	port
//		.sd_data3_in   	    (sd_data3_in_padmisc2core   		)	,   //input	port
		.sd_mmc_cmd_in 	    (sd_mmc_cmd_in_padmisc2core 		)	,   //input	port
		.ms_clk        	    (ms_clk_core2padmisc        		)	,       //output	port
		.ms_bsst       	    (ms_bsst_core2padmisc       		)	,       //output	port
		.ms_pw_ctrl    	    (ms_pw_ctrl_core2padmisc    		)	,       //output	port
		.ms_serout     	    (ms_serout_core2padmisc     		)	,       //output	port
		.msde          	    (msde_core2padmisc          		)	,       //output	port
		.sd_data_oej  	    (sd_data_oej_core2padmisc  		)	,       //output	port
//		.sd_data1_oej  	    (sd_data1_oej_core2padmisc  		)	,       //output	port
//		.sd_data2_oej  	    (sd_data2_oej_core2padmisc  		)	,       //output	port
//		.sd_data3_oej  	    (sd_data3_oej_core2padmisc  		)	,       //output	port
		.sd_mmc_cmd_oej	    (sd_mmc_cmd_oej_core2padmisc		)	,       //output	port
		.sd_data_out  	    (sd_data_out_core2padmisc  		)	,       //output	port
//		.sd_data1_out  	    (sd_data1_out_core2padmisc  		)	,       //output	port
//		.sd_data2_out  	    (sd_data2_out_core2padmisc  		)	,       //output	port
//		.sd_data3_out  	    (sd_data3_out_core2padmisc  		)	,       //output	port
		.sd_mmc_cmd_out	    (sd_mmc_cmd_out_core2padmisc		)	,       //output	port
		.sd_mmc_clk    	    (sd_mmc_clk_core2padmisc    		)	,       //output	port
		.test_h        	    (test_h_padmisc2core        		)	,   //input	port
		.test_done     	    (test_done_core2padmisc     		)	,       //output	port
		.fail_h        	    (fail_h_core2padmisc        		)	,       //output	port
		.det_pin       	    (det_pin_padmisc2core       		)	,   //input	port
		.wr_prct_pin   	    (wr_prct_pin_padmisc2core   		)	,   //input	port
		.sd_ip_enable		(sd_ip_enable_core2padmisc			)	,
		.ms_ip_enable		(ms_ip_enable_core2padmisc			)	,
		//audio adc
		.iadc_v1p2u 			(xadc_vref_core2pad				),
		.iadc_vs                (gnda_adc						),
		.iadc_vd                (vd33a_adc						),
		.iadc_dvdd				(vddcore						),			//	ADC digital power
		.iadc_dvss				(gnd							),			//	ADC digital ground
		.iadc_vin_l             (xmic1in_pad2core				),
		.iadc_vin_r             (xmic2in_pad2core				),
        // working mode select
//        .pci_mode               (pci_mode               		),
        .ide_mode            	(ide_mode            			),
        .tvenc_test_mode        (tvenc_test_mode        		),
        .servo_only_mode		(servo_only_mode				),
        .servo_test_mode        (servo_test_mode        		),
        .servo_sram_mode		(servo_sram_mode				),
        .test_mode              (scan_test_mode_padmisc2core	),
        .bist_test_mode			(bist_test_mode_padmisc2core	),
        .dsp_bist_mode			(dsp_bist_tri_padmisc2core		),
        .dsp_bist_finish		(dsp_bist_finish_core2padmisc	),
        .dsp_bist_err_vec		(dsp_bist_vec_core2padmisc		),
		.video_bist_tri			(video_bist_tri_padmisc2core	),
		.video_bist_finish		(video_bist_finish_core2padmisc	),
		.video_bist_vec			(video_bist_vec_core2padmisc_10_0),//[10:0]JFR031106

        //to cpu
        .pci_mips_fs            (pci_mips_fs            		),
        .cpu_endian_mode        (cpu_endian_mode        		),
        .core_warmrst_          (core_warmrst_          		),
        .core_coldrst_          (core_coldrst_          		),
		.cpu_probe_en			(cpu_probe_en					),
		.flash_mode				(flash_mode						),//flash interface select mode
	//DFT test port
`ifdef  SCAN_EN
	   .DFT_TEST_SE		        (scan_en_padmisc2core           ),
	   .DFT_TEST_SI		        (scan_din[86:0]                 ),     // Reserve {scan chain [112:100]}
	   .DFT_TEST_SO		        (scan_dout[86:0]                ),
`endif
        //connect to chipset
        .sw_rst_pulse           (sw_rst_pulse       ),
        .chipset_rst_           (chipset_rst_   	)
        );

`else
//include core_bh, delete by yuky, 2002-10-09
`endif
//========================End of core invoking==========================

//======================== Clock Generater =============================

wire    [31:0]  strap_pin_in;

F27_PLL F27_PLL(
                .FIN            (f27m_clk       	),      // 27MHz input
                .FOUT_48		(f48m_clk_pll2gen	),		// 48MHz output
                .FOUT_54        (f54m_clk_pll2gen   ),		// 54MHz output only for IDE
                .FOUT_80		(f80m_clk_pll2gen	),		// 80MHz output
                .FOUT_108		(f108m_clk_pll2gen	),		// 108MHz output
                .FOUT_162		(f162m_clk_pll2gen  ),
                .PD				(1'b0				),
                .TFCK			(f27_pll_fck		),
                .TLOCK			(f27_pll_lock		),
                .LOCK_EN		(1'b0				),
                .PLLAVSS		(f27_pllvss			),
                .PLLAVDD		(f27_pllvdd			),
				.PLLDVDD		(vddcore			),
				.PLLDVSS		(gnd				)
                );

CPU_PLL CPU_CLK_PLL(
                .FIN            (f27m_clk   		),	// 27 input
                .PLL_M      	(cpu_clk_pll_m      ),	//cpu_pll_m,from strap pins
                .PD				(1'b0				),
                .FOUT           (cpu_pll_fout       ),
                .FOUT12			(cpu_pll_fout12		),	// 1/2 CPU_PLL FOUT
                .TFCK			(cpu_clk_pll_fck	),
                .TLOCK			(cpu_clk_pll_lock	),
                .LOCK_EN		(1'b0				),
                .PLLAVSS		(f27_pllvss			),
                .PLLAVDD		(f27_pllvdd			),
				.PLLDVDD		(vddcore			),
				.PLLDVSS		(gnd				)
                );

//	MUX for the CPU_CLK
//	cpu_pll_fout_sel,	// 0-> select FOUT, 1-> select FOUT12 as the CPU_CLK
//wire	cpu_pll_fout_sel;		// output from the PAD_RST_STRAP
//wire	cpu_clk_fout2gen		= cpu_pll_fout_sel ? cpu_pll_fout12 : cpu_pll_fout;

MX2X4	U_CPU_CLK_SEL	(	.Y	(cpu_clk_fout2gen	),
							.A	(cpu_pll_fout		),
							.B	(cpu_pll_fout12		),
							.S0	(cpu_pll_fout_sel	)
						);

MEM_PLL	MEM_CLK_PLL(
                .FIN            (f27m_clk   		),	// 27 input
                .PLL_M      	(mem_clk_pll_m		),	//cpu_pll_m,from strap pins
                .PD				(1'b0				),
                .FOUT           (mem_clk_fout2gen	),
                .FOUT12			(					),	// not used
                .TFCK			(mem_clk_pll_fck	),
                .TLOCK			(mem_clk_pll_lock	),
                .LOCK_EN		(1'b0				),
                .PLLAVSS		(f27_pllvss			),
                .PLLAVDD		(f27_pllvdd			),
				.PLLDVDD		(vddcore			),
				.PLLDVSS		(gnd				)
                );

AUDIO_PLL AUDIO_PLL	(
				.FIN			(f27m_clk  			),
				.LOCK_EN		(1'b0	 			),
				.PD				(1'b0     			),
				.FOUT_SEL		(audio_pll_fout_sel_core2gen	),

				.FOUT			(audio_clk_pll2gen	),
				.TFCK			(audio_pll_fck		),
				.TLOCK      	(audio_pll_lock		),
				.PLLAVSS		(f27_pllvss			),
				.PLLAVDD		(f27_pllvdd			),
				.PLLDVDD		(vddcore			),
				.PLLDVSS		(gnd				)
				);

CLOCK_GEN CLOCK_GEN(
// input
				.SRC_CLK		(f27m_clk				),	// from clock input pad
                .F48M           (f48m_clk_pll2gen		),  // from F27_PLL 48M output
                .F54M           (f54m_clk_pll2gen		),	// from F27_PLL output
                .F162M			(f162m_clk_pll2gen		),
                .PCI_MIPS_FS    (pci_mips_fs			),  // select the divisor of PCI/MIPS bus clock,from strap pins
				.IDE_FS			(IDE_FS_core2gen		),	// ide clock frequency select
				.DSP_FS			(DSP_FS_core2gen		),	// dsp clock frequency select
				.DSP_DIV_SRC_SEL(DSP_DIV_SRC_SEL_core2gen),	// dsp source clock select
				.PCI_FS			(PCI_FS_core2gen		),	// pci clock frequency select
				.VE_FS			(VE_FS_core2gen			),	// ve clock frequency select
				.VE_DIV_SRC_SEL	(VE_DIV_SRC_SEL_core2gen),	// ve source clock select
                .J_TCLK         (j_tclk_pad2padmisc		),  // JTAG test mode clock
                .TEST_EN        (scan_test_mode_padmisc2core),  // Test Mode Enable
                .CPU_MODE       (1'b0					),  // CPU standalone mode
                .MEM_DLY_SEL    (mem_dly_sel			),  // SDRAM clock pad delay chain control
                .PLL_FVCO       (cpu_clk_fout2gen		), 	// Clock output from PLL
                .MEM_CLK_FOUT	(mem_clk_fout2gen		),	// memory clock output from CPU_PLL
                .BYPASS_PLL     (pll_bypass				),  // BY PASS CPU_PLL
//                .PCI_MBUS_SEL   (pci_clk_out_sel_core2gen),	// PCI_MBUS clock output select
                .TEST_MODE      (scan_test_mode_padmisc2core),	// CHIP TEST MODE
                .PSCAN_EN		(1'b0					),	// Progressive scan enable
				.STANDBY		(standby_mode_en_core2clockgen),		// standby mode enable
				.ALL_CLOCK_EN	(all_clock_en			),	// enable all the clock except the CPU/MEM/SB

// output
                .USB_CLK        (usb_clk_gen2core		),  // USB clock 48M
                .SB_CLK         (sb_clk_gen2core		),  // SouthBridge Clock 12M
                .CPU_CLK        (cpu_clk_gen2core		),  // CPU clock (with JTAG test mode mask)
                .MBUS_CLK       (mbus_clk_gen2core		),  // MIPS clock
                .MEM_CLK        (inter_mem_clk_gen		),  // internal SDRAM, NorthBridge clock
                .DSP_CLK        (dsp_clk_gen2core		),  // DSP clock
                .VE_CLK         (ve_clk_gen2gen			),  // VIDEO ENGINE CLOCK to VIDEO_CLOCK_GEN
//                .F54M_CLK		(ata_clk_gen2core		),	// 54 MHz clock for IDE
                .F54M_CLK		(f54m_clk				),	// 54 MHz clock JFR 2004-01-02
				.IDE_CLK		(ata_clk_gen2core		),	// IDE clock JFR 2004-01-02
                .PCI_CLK        (pci_clk_gen2core		),  // internal PCI bus clock
                .OUT_MEM_CLK    (ext_mem_clk_gen		),  // output to SDRAM clock pad
                .PCI_MBUS_CLK   (pci_mbus_clk_gen		),	// output to PCI/MIPS clock pad
				.SERVO_CLK		(servo_clk_gen2core		),	// SERVO clock, it is 1/2 mem_clk
				.GCVIDCLK		(gc_vid_clk_gen2padmisc				),
				.GCSYSCLK		({gc_sys_clk_gen2padmisc1,gc_sys_clk_gen2padmisc0}),
				.GI_CLK			(gi_clk					),
                .COLD_RST_      (boot_config_finish_),
                .RST_           (clk_gen_rst_)
                );

AUDIO_CLOCK_GEN AUDIO_CLOCK_GEN (

			// input
				.SRC_CLK		(f27m_clk				),		// from clock input pad
				.AUDIO_PLL_FOUT	(audio_clk_pll2gen		),		// 24.576 or 16.9344 MHz from audio PLL
				.JTCLK			(j_tclk_pad2padmisc		),		// jtclk from pad
				.I2S_MCLK_IN	(i2so_mclk_padmisc2core	),		// I2S master clock from PAD input
				.AC97BCK		(1'b0),							// no use in m3357
				.I2S_MCLK_SEL	(i2s_mclk_sel_core2gen	),		// I2S Master clock frequency select
				.SPDIF_CLK_SEL	(spdif_clk_sel_core2gen	),		// SPDIF clock frequency selectDSP_FS

				.BYPASS_PLL		(pll_bypass				),		// By pass audio PLL
				.TEST_MODE		(scan_test_mode_padmisc2core),		// Chip in test mode
				.ALL_CLOCK_EN	(all_clock_en			),	// enable all the clock except the CPU/MEM/SB

			// output
				.I2S_MCLK		(i2so_mclk_gen2core		),		// I2S master clock to audio core and PAD
				.SPDIF_CLK		(spdif_clk_gen2core		),		// spdif clock to audio core
//				.AC97BCK_OUT	(		),						// no use in m3357 JFR 030627
				.IADC_MCLK		(iadc_mclk_gen2core		),		// IADC master clock

				.RST_			(clk_gen_rst_			)

				);

VIDEO_CLOCK_GEN VIDEO_CLOCK_GEN (

			// input
				.SRC_CLK			(f27m_clk				),		// from clock input pad
				.F108M_IN			(f108m_clk_pll2gen		),		// 108MHz clock input from PLL
				.F80M_IN			(f80m_clk_pll2gen		),		// 80MHz clock input from PLL
				.JTCLK				(j_tclk_pad2padmisc		),		// jtclk from pad
				.VIDEO_CLK_IN		(ve_clk_gen2gen			),		// VIDEO clock from clock gen

				.BYPASS_PLL			(pll_bypass				),		// By pass audio PLL
				.TEST_MODE			(scan_test_mode_padmisc2core),	// Chip in scan test mode
				.INTER_ONLY			(inter_only_core2gen	),		// Interlace output
//				.DVI_RWBUF_SEL		(dvi_rwbuf_sel_core2gen	),		// DVI buffer read/write
				.SVGA_MODE_EN		(svga_mode_en_core2gen	),		// SVGA mode enable, TV encoder output the
				.TV_MODE			(tv_mode_core2gen		),		// TV mode
				.OUT_MODE_ETV_SEL	(out_mode_etv_sel_core2gen),	// output pixel clock select, 0 -> 54 MHz, 1 -> 27 MHz
				.ALL_CLOCK_EN		(all_clock_en			),	// enable all the clock except the CPU/MEM/SB

			// output
				.CVBS2X_CLK			(cvbs2x_clk_gen2core	),		// 54MHZ clock
				.CVBS_CLK			(cvbs_clk_gen2core		),		// 27MHz clock
				.CAV_CLK			(cav_clk_gen2core		),		// 54 or 27 MHZ clock, or 80MHz in SVGA mode
//				.DVI_BUF0_CLK		(dvi_buf0_clk_gen2core	),		// CAV_CLK or VIDEO_CLK for buffer 0
//				.DVI_BUF1_CLK		(dvi_buf1_clk_gen2core	),		// CAV_CLK or VIDEO_CLK for buffer 1
				.VIDEO_CLK			(video_clk_gen2core		),		// VIDEO clock output
				.OUT_PIXEL_CLK		(out_pixel_clk_gen2padmisc),	// output pixel_clk
				.VSB_CLK			(vsb_clk_gen2core		),		// VSB clock 80M

//				.TV_ENC_CLK_SEL		(tv_enc_clk_sel_core2gen),		// video clock delay select
				.F108M_CLK_TV		(tv_f108m_clk_gen2core	),		// 108MHz output for TV encoder
				.CVBS2X_CLK_TV		(tv_cvbs2x_clk_gen2core	),		// 54MHZ clock for TV encoder
//				.CVBS_CLK_TV		(tv_cvbs_clk_gen2core	),		// 27MHz clock for TV encoder
				.CAV2X_CLK_TV		(tv_cav2x_clk_gen2core	),		// 108 or 54 MHZ clock for TV encoder
				.CAV_CLK_TV			(tv_cav_clk_gen2core	),		// 54 or 27 MHZ clock for TV encoder

				.RST_				(clk_gen_rst_			)
				);

//====================================================================
// Strap Pin and Reset Logic
//there is not warm reset input in m3357
wire    warm_rst_in_ = 1'b1; //unactive
pad_rst_strap PAD_RST_STRAP(
//<<output>>
//                .warm_rst_oe_           (warm_rst_oe_           ), //no use in m3357, to pad.v
// Strap pin
                .cpu_pll_m              (cpu_clk_pll_m			),
                .cpu_pll_fout_sel		(cpu_pll_fout_sel		),	// 0-> select FOUT, 1-> select FOUT12 as the CPU_CLK
                .mem_pll_m              (mem_clk_pll_m			),
				.combo_mode				(combo_mode				),//add for m3357 by JFR 030526
                .ide_mode            	(ide_mode            	),
//                .pci_mode               (pci_mode               ),
  				.tvenc_test_mode		(tvenc_test_mode		),//analogt_mode in m6304
				.servo_only_mode		(servo_only_mode	    ),//added for m3357	JFR030616
				.servo_test_mode		(servo_test_mode	    ),//added for m3357	JFR030616
				.servo_sram_mode		(servo_sram_mode	    ),//added for m3357	JFR030616
                .pll_bypass             (pll_bypass             ),
                .cpu_probe_en			(cpu_probe_en			),
                .flash_mode				(flash_mode				),//flash interface select mode
                //fixed setting, only little endian
                .cpu_endian_mode        (cpu_endian_mode        ),

// Strap pin program
                .cpu_pll_m_ack          (cpu_clk_pll_m_ack_str2core 	),
                .mem_pll_m_ack          (mem_clk_pll_m_ack_str2core		),
                .work_mode_ack			(work_mode_ack_str2core			),
// roger
                .clk_gen_rst_           (clk_gen_rst_           		),
                .core_coldrst_          (core_coldrst_          		),
                .core_warmrst_          (core_warmrst_          		),
                .chipset_rst_           (chipset_rst_           		),
                .pad_rst_				(pad_rst_						),
                .boot_config_finish_    (boot_config_finish_    		),
                .pad_boot_config        (pad_boot_config        		),
//<<input>>
// strap pin program
                .cpu_pll_m_tri          (cpu_clk_pll_m_tri_core2str  	),
                .cpu_pll_m_ctrldata     (cpu_clk_pll_m_value_core2str	),
                .mem_pll_m_tri          (mem_clk_pll_m_tri_core2str  	),
                .mem_pll_m_ctrldata     (mem_clk_pll_m_value_core2str	),
                .work_mode_tri			(work_mode_tri_core2str			),
                .work_mode_ctrldata		(work_mode_value_core2str		),

				.test_mode				(scan_test_mode_padmisc2core	),
                .strap_pin_in           (strap_pin_in           		),
                .sw_rst_pulse           (sw_rst_pulse           		),
                .clk_src_in             (f27m_clk               		),
                .warm_rst_in_           (warm_rst_in_           		),
                .cold_rst_in_           (cold_rst_in_           		),

                .j_pr_rst_              (1'b1                   		),      // from JTAG reset
                .cpu_bus_clk            (mbus_clk_gen2core      		)
                );

//=================== invoke the pad_misc =============================
PAD_MISC PAD_MISC(
//--------------strap pins control
                .strap_pin_out          			(strap_pin_in           	),
//------SDRAM
                //connect to pad
                .ram_dq_out2pad						(ram_dq_padmisc2pad         ),
                .ram_dq_oe_out2pad_					(ram_dq_oe_padmisc2pad_		),
                .ram_dq_frompad         			(ram_dq_pad2padmisc         ),
                //connect to core
                .ram_dq_fromcore        			(ram_dq_core2padmisc        ),
                .ram_dq_oe_fromcore_				(ram_dq_oe_core2padmisc_	),
                .ram_dq_out2core        			(ram_dq_padmisc2core        ),
//------SDRAM DQM, Command Line
                .ram_dqm_out2pad					(ram_dqm_padmisc2pad		),
                .ram_dqm_oe_out2pad_				(ram_dqm_oe_padmisc2pad_	),
                .ram_dqm_fromcore					(ram_dqm_core2padmisc		),
                .ram_dqm_frompad        			(ram_dqm_pad2padmisc    	),
//------SDRAM bank address, address line
                .ram_ba_out2pad						(ram_ba_padmisc2pad			),
                .ram_ba_oe_out2pad_					(ram_ba_oe_padmisc2pad_		),
                .ram_ba_fromcore					(ram_ba_core2padmisc		),
                .ram_ba_frompad         			(ram_ba_pad2padmisc     	),
                .ram_addr_out2pad					(ram_addr_padmisc2pad		),
                .ram_addr_oe_out2pad_				(ram_addr_oe_padmisc2pad	),
                .ram_addr_fromcore					(ram_addr_core2padmisc		),
                .ram_addr_frompad       			(ram_addr_pad2padmisc   	),
//-----	SDRAM Command Line
                .ram_cs_out2pad_					(ram_cs_padmisc2pad_		),
                .ram_cs_fromcore_					(ram_cs_core2padmisc		),
                .ram_we_out2pad_					(ram_we_padmisc2pad_		),
                .ram_we_fromcore_					(ram_we_core2padmisc		),
//                .ram_we_frompad_					(ram_we_pad2padmisc_		),//JFR030627
                .ram_cas_out2pad_					(ram_cas_padmisc2pad_		),
                .ram_cas_fromcore_					(ram_cas_core2padmisc		),
                .ram_ras_out2pad_					(ram_ras_padmisc2pad_		),
                .ram_ras_fromcore_					(ram_ras_core2padmisc		),
//                .ram_ras_frompad_					(ram_ras_pad2padmisc_		),//JFR030627
                .ram_cs_oe_out2pad_					(ram_cs_oe_padmisc2pad_		),
                .ram_we_oe_out2pad_					(ram_we_oe_padmisc2pad_		),
                .ram_cas_oe_out2pad_				(ram_cas_oe_padmisc2pad_	),
                .ram_ras_oe_out2pad_				(ram_ras_oe_padmisc2pad_	),
                .ram_cs_frompad_					(ram_cs_pad2padmisc_		),
                .ram_cas_frompad_					(ram_cas_pad2padmisc_		),
//------FLASH
                .rom_rdata_out2core  				(rom_rdata_padmisc2core		),
                .rom_wdata_fromcore  				(rom_wdata_core2padmisc      ),
                .rom_data_oe_fromcore_				({8{rom_data_oe_core2padmisc_}}),
                .rom_data_out2pad					(rom_data_padmisc2pad		),
                .rom_data_oe_out2pad_				(rom_data_oe_padmisc2pad_	),
                .rom_data_frompad					(rom_data_pad2padmisc		),
                .rom_addr_fromcore   				(rom_addr_core2padmisc ),
                .rom_addr_oe_fromcore				(rom_addr_oe_core2padmisc),
                .rom_addr_out2pad					(rom_addr_padmisc2pad		),
                .rom_addr_oe_out2pad_				(rom_addr_oe_padmisc2pad_	),
                .rom_addr_frompad					(rom_addr_pad2padmisc		),
				.rom_addr2_frompad					(rom_addr_set2_pad2padmisc	),
		        .rom_oe_fromcore_					(rom_oe_core2padmisc		),
		        .rom_oe_out2pad_					(rom_oe_padmisc2pad_		),
		        .rom_oe_oe_out2pad_					(rom_oe_oe_padmisc2pad_		),
		        .rom_oe_frompad_					(rom_oe_pad2padmisc_		),
				.rom_ale_fromcore					(eeprom_ale_core2padmisc	),
        		.rom_ale_out2pad					(eeprom_ale_padmisc2pad	    ),
        		.rom_ale_oe_out2pad_				(eeprom_ale_oe_padmisc2pad_ ),
        		.rom_ale_frompad					(eeprom_ale_pad2padmisc	    ),
//		        .rom_ce_fromcore_					(rom_ce_core2padmisc		),
//		        .rom_ce_out2pad_					(rom_ce_padmisc2pad_		),
//		        .rom_ce_oe_out2pad_					(rom_ce_oe_padmisc2pad_		),
//		        .rom_ce_frompad_					(rom_ce_pad2padmisc_		),
		        .rom_we_fromcore_					(rom_we_core2padmisc		),
		        .rom_we_out2pad_					(rom_we_padmisc2pad_		),
		        .rom_we_oe_out2pad_					(rom_we_oe_padmisc2pad_		),
		        .rom_we_frompad_					(rom_we_pad2padmisc_		),
		        .rom_intf_mode_fromcore				(flash_mode					),
//--------------CPU Bus
                .cpu_sysint_out2core_   			(sys_int_padmisc2core_       		),
                .cpu_sysnmi_out2core_   			(sys_nmi_padmisc2core_          	),
                .cpu_errint_fromcore    			(x_err_int_core2padmisc         	),
                .cpu_extint_fromcore    			(x_ext_int_core2padmisc         	),
				.cpu_bist_tri_out2core				(cpu_bist_mode_padmisc2core			),
				.cpu_scan_test_mode					(cpu_scan_test_mode_padmisc2core	),
				.cpu_bist_finish_fromcore 			(cpu_bist_finish_core2padmisc		),
				.cpu_bist_vec_fromcore				(cpu_bist_error_vec_core2padmisc	),
//------EJTAG
				.j_tdi_out2pad						(j_tdi_padmisc2pad				),
				.j_tdo_out2pad						(j_tdo_padmisc2pad				),
				.j_tms_out2pad						(j_tms_padmisc2pad				),
				.j_trst_out2pad_					(j_trst_padmisc2pad_			),
				.j_tclk_out2pad						(j_tclk_padmisc2pad				),
				.j_tdi_oe_out2pad_					(j_tdi_oe_padmisc2pad_			),
				.j_tdo_oe_out2pad_					(j_tdo_oe_padmisc2pad_			),
				.j_tms_oe_out2pad_					(j_tms_oe_padmisc2pad_			),
				.j_trst_oe_out2pad_					(j_trst_oe_padmisc2pad_			),
				.j_tclk_oe_out2pad_					(j_tclk_oe_padmisc2pad_			),
				.j_tdo_fromcore						(j_tdo_core2padmisc				),
				.j_tdi_frompad						(j_tdi_pad2padmisc				),
				.j_tdo_frompad						(j_tdo_pad2padmisc				),
				.j_tms_frompad						(j_tms_pad2padmisc				),
				.j_trst_frompad_					(j_trst_pad2padmisc_			),
				.j_tclk_frompad						(j_tclk_pad2padmisc				),
				.j_tdi_out2core						(j_tdi_padmisc2core				),
				.j_tms_out2core						(j_tms_padmisc2core				),
				.j_trst_out2core_					(j_trst_padmisc2core_			),
				.j_tclk_out2core					(j_tclk_padmisc2core			),
//------IDE
                //connect to core
                .atadiow_fromcore_      			(atadiow_core2padmisc_   		),
                .atadior_fromcore_      			(atadior_core2padmisc_   		),
                .atacs0_fromcore_       			(atacs0_core2padmisc_    		),
                .atacs1_fromcore_       			(atacs1_core2padmisc_    		),
                .atadd_fromcore         			(atadd_core2padmisc      		),
                .atadd_oe_fromcore_     			({16{atadd_oe_core2padmisc_[0]}}),
                .atada_fromcore         			(atada_core2padmisc      		),
                .atareset_fromcore_     			(atareset_core2padmisc_  		),
                .atadmack_fromcore_     			(atadmack_core2padmisc_  		),
                .ataiordy_out2core      			(ataiordy_padmisc2core   		),
                .ataintrq_out2core      			(ataintrq_padmisc2core   		),
                .atadd_out2core         			(atadd_padmisc2core      		),
                .atadmarq_out2core      			(atadmarq_padmisc2core   		),
                .atareset_oe_fromcore_				(atareset_oe_core2padmisc_		),
                //connect to pad
                .atadiow_out2pad_     				(atadiow_padmisc2pad_     	  ),
				.atadior_out2pad_     				(atadior_padmisc2pad_         ),
				.atacs0_out2pad_      				(atacs0_padmisc2pad_          ),
				.atacs1_out2pad_      				(atacs1_padmisc2pad_          ),
				.atadd_out2pad        				(atadd_padmisc2pad            ),
				.atadd_oe_out2pad_    				(atadd_oe_padmisc2pad_        ),
				.atada_out2pad        				(atada_padmisc2pad            ),
				.atareset_out2pad_    				(atareset_padmisc2pad_        ),
				.atadmack_out2pad_    				(atadmack_padmisc2pad_        ),
				.ataiordy_frompad     				(ataiordy_pad2padmisc         ),
				.ataintrq_frompad     				(ataintrq_pad2padmisc         ),
				.atadd_frompad        				(atadd_pad2padmisc            ),
				.atadmarq_frompad     				(atadmarq_pad2padmisc         ),
//--------------PCI Bus
                //connect to core
                .pci_clk_fromcore					(pci_mbus_clk_gen				),
                .pci_ad_out2core        			(pci_ad_padmisc2core            ),
                .pci_cbe_out2core_      			(pci_cbe_padmisc2core_          ),
                .pci_par_out2core       			(pci_par_padmisc2core           ),
                .pci_frame_out2core_    			(pci_frame_padmisc2core_        ),
                .pci_irdy_out2core_     			(pci_irdy_padmisc2core_         ),
                .pci_trdy_out2core_     			(pci_trdy_padmisc2core_         ),
                .pci_stop_out2core_     			(pci_stop_padmisc2core_         ),
                .pci_devsel_out2core_   			(pci_devsel_padmisc2core_       ),
                .pci_req_out2core_      			(pci_req_padmisc2core_          ),//2 bits width
				.pci_inta_out2core_					(pci_inta_padmisc2core_			),
                .pci_serr_out2core_                 (pci_serr_padmisc2core_         ),
                .pci_perr_out2core_                 (pci_perr_padmisc2core_         ),
                //from core
                .pci_ad_fromcore        			(pci_ad_core2padmisc            ),
                .pci_cbe_fromcore_      			(pci_cbe_core2padmisc_          ),
                .pci_par_fromcore       			(pci_par_core2padmisc           ),
                .pci_frame_fromcore_    			(pci_frame_core2padmisc_        ),
                .pci_irdy_fromcore_     			(pci_irdy_core2padmisc_         ),
                .pci_gnta_fromcore_     			(pci_gnt_core2padmisc_[0]       ),
                .pci_gntb_fromcore_					(pci_gnt_core2padmisc_[1]		),
                .pci_trdy_fromcore_     			(pci_trdy_core2padmisc_         ),
                .pci_stop_fromcore_     			(pci_stop_core2padmisc_         ),
                .pci_devsel_fromcore_   			(pci_devsel_core2padmisc_       ),
                .pci_ad_oe_fromcore_    			({32{pci_ad_oe_core2padmisc_}}  ),
                .pci_cbe_oe_fromcore_   			({4{pci_cbe_oe_core2padmisc_}}  ),
                .pci_par_oe_fromcore_   			(pci_par_oe_core2padmisc_       ),
                .pci_frame_oe_fromcore_ 			(pci_frame_oe_core2padmisc_     ),
                .pci_irdy_oe_fromcore_  			(pci_irdy_oe_core2padmisc_      ),
                .pci_trdy_oe_fromcore_  			(pci_trdy_oe_core2padmisc_      ),
                .pci_stop_oe_fromcore_  			(pci_stop_oe_core2padmisc_      ),
                .pci_devsel_oe_fromcore_			(pci_devsel_oe_core2padmisc_    ),
                .pci_rst_fromcore_					(pci_rst_core2padmisc_			),
                //serr and perr
                .pci_serr_fromcore_                 (pci_serr_core2padmisc_         ),
                .pci_perr_fromcore_                 (pci_perr_core2padmisc_         ),
                .pci_serr_oe_fromcore_              (pci_serr_oe_core2padmisc_      ),
                .pci_perr_oe_fromcore_              (pci_perr_oe_core2padmisc_      ),
                //connect to pad
				.pci_clk_out2pad				    (pci_clk_padmisc2pad		    ),
				.pci_gnta_out2pad_	        		(pci_gnta_padmisc2pad_	        ),
				.pci_gntb_out2pad_	        		(pci_gntb_padmisc2pad_	        ),
				.pci_rst_out2pad_	        		(pci_rst_padmisc2pad_	        ),
				.pci_frame_frompad_		    		(pci_frame_pad2padmisc_		    ),
				.pci_frame_oe_out2pad_	    		(pci_frame_oe_padmisc2pad_	    ),
				.pci_frame_out2pad_		    		(pci_frame_padmisc2pad_		    ),
				.pci_irdy_frompad_		    		(pci_irdy_pad2padmisc_		    ),
				.pci_irdy_oe_out2pad_	    		(pci_irdy_oe_padmisc2pad_	    ),
				.pci_irdy_out2pad_		    		(pci_irdy_padmisc2pad_		    ),
				.pci_trdy_frompad_		    		(pci_trdy_pad2padmisc_		    ),
				.pci_trdy_oe_out2pad_	    		(pci_trdy_oe_padmisc2pad_	    ),
				.pci_trdy_out2pad_		    		(pci_trdy_padmisc2pad_		    ),
				.pci_stop_frompad_		    		(pci_stop_pad2padmisc_		    ),
				.pci_stop_oe_out2pad_	    		(pci_stop_oe_padmisc2pad_	    ),
				.pci_stop_out2pad_		    		(pci_stop_padmisc2pad_		    ),
				.pci_devsel_frompad_		    	(pci_devsel_pad2padmisc_		),
				.pci_devsel_oe_out2pad_	    		(pci_devsel_oe_padmisc2pad_	    ),
				.pci_devsel_out2pad_		    	(pci_devsel_padmisc2pad_		),
				.pci_ad_frompad			    		(pci_ad_pad2padmisc			    ),
				.pci_ad_oe_out2pad_		    		(pci_ad_oe_padmisc2pad_		    ),
				.pci_ad_out2pad			    		(pci_ad_padmisc2pad			    ),
				.pci_par_frompad			    	(pci_par_pad2padmisc			),
				.pci_par_oe_out2pad_		    	(pci_par_oe_padmisc2pad_		),
				.pci_par_out2pad			    	(pci_par_padmisc2pad			),
				.pci_cbe_frompad_		    		(pci_cbe_pad2padmisc_		    ),
				.pci_cbe_oe_out2pad_		    	(pci_cbe_oe_padmisc2pad_		),
				.pci_cbe_out2pad_	        		(pci_cbe_padmisc2pad_	        ),
				.pci_req_frompad_		    		(pci_req_pad2padmisc_		    ),
				.pci_inta_frompad_		    		(pci_inta_pad2padmisc_		    ),
				//serr and perr
                .pci_serr_frompad_		             (pci_serr_pad2padmisc_         ),
		        .pci_serr_oe_out2pad_	             (pci_serr_oe_padmisc2pad_      ),
		        .pci_serr_out2pad_		             (pci_serr_padmisc2pad_         ),
		        .pci_perr_frompad_		             (pci_perr_pad2padmisc_         ),
		        .pci_perr_oe_out2pad_	             (pci_perr_oe_padmisc2pad_      ),
		        .pci_perr_out2pad_		             (pci_perr_padmisc2pad_         ),
//--------------GPIO(0~13, 28~24)
                //connect to pad
                .gpio_out2pad           			(gpio_padmisc2pad[28:0]        	),
                .gpio_oe_out2pad_       			(gpio_oe_padmisc2pad_[28:0]   	),
                .gpio_frompad           			(gpio_pad2padmisc[28:0]        	),
                //connect to core
                .gpio_fromcore          			(gpio_core2padmisc[31:0]		),
                .gpio_out2core          			(gpio_padmisc2core[31:0]   		),
                .gpio_oe_fromcore_      			(gpio_oe_core2padmisc_[31:0]	),

        		.gpio2_fromcore		    			(gpio_set2_core2padmisc    		),
        		.gpio2_oe_fromcore_	    			(gpio_set2_oe_core2padmisc_     ),
        		.gpio2_out2core		    			(gpio_set2_padmisc2core      	),

//--------------GI Interface(from broadon)
				//system (6)
				.gc_vid_clk_fromcore				(gc_vid_clk_gen2padmisc			),
				.gc_sys_clk_fromcore0				(gc_sys_clk_gen2padmisc0		),
				.gc_sys_clk_fromcore1				(gc_sys_clk_gen2padmisc1		),
				.alt_boot_out2core					(alt_boot_padmisc2core			),
				.test_ena_out2core					(test_ena_padmisc2core	    	),
				.gc_rst_fromcore					(gc_rst_core2padmisc	    	),
				//serial eeproom interface
				.i2c_clk_fromcore					(i2c_clk_core2padmisc	    	),
				.i2c_din_out2core					(i2c_din_padmisc2core	    	),
				.i2c_dout_fromcore					(i2c_dout_core2padmisc	    	),
				.i2c_doe_fromcore					(i2c_doe_core2padmisc	    	),
				//disc interface
				.di_in_out2core						(di_in_padmisc2core		    	),
				.di_out_fromcore					(di_out_core2padmisc	    	),
				.di_oe_fromcore						({8{di_oe_core2padmisc}}		),
				.di_brk_in_out2core					(di_brk_in_padmisc2core	    	),
				.di_brk_out_fromcore				(di_brk_out_core2padmisc 		),
				.di_brk_oe_fromcore					(di_brk_oe_core2padmisc	    	),
				.di_dir_out2core					(di_dir_padmisc2core	    	),
				.di_hstrb_out2core					(di_hstrb_padmisc2core	    	),
				.di_dstrb_fromcore					(di_dstrb_core2padmisc	    	),
				.di_err_fromcore					(di_err_core2padmisc	    	),
				.di_cover_fromcore					(di_cover_core2padmisc	    	),
				.di_stb_out2core					(di_stb_padmisc2core			),

				.ais_clk_out2core					(ais_clk_padmisc2core			),
				.ais_lr_out2core					(ais_lr_padmisc2core	    	),
				.ais_d_fromcore						(ais_d_core2padmisc		    	),

				//connect to pad
				//system (6)
				.gc_vid_clk_out2pad					(gc_vid_clk_padmisc2pad			),
				.gc_sys_clk_out2pad0				(gc_sys_clk_padmisc2pad0		),
				.gc_sys_clk_out2pad1				(gc_sys_clk_padmisc2pad1		),
				.alt_boot_frompad					(alt_boot_pad2padmisc			),
				.test_ena_frompad					(test_ena_pad2padmisc	    	),
				.gc_rst_out2pad						(gc_rst_padmisc2pad	    		),
				//serial eeproom interface
				.i2c_clk_out2pad					(i2c_clk_padmisc2pad	    	),
				.i2c_din_frompad					(i2c_din_pad2padmisc	    	),
				.i2c_dout_out2pad					(i2c_dout_padmisc2pad	    	),
				.i2c_doe_out2pad					(i2c_doe_padmisc2pad	    	),
				//disc interface
				.di_in_frompad						(di_in_pad2padmisc		    	),
				.di_out_out2pad						(di_out_padmisc2pad	    		),
				.di_oe_out2pad						(di_oe_padmisc2pad		    	),
				.di_brk_in_frompad					(di_brk_in_pad2padmisc	    	),
				.di_brk_out_out2pad					(di_brk_out_padmisc2pad 		),
				.di_brk_oe_out2pad					(di_brk_oe_padmisc2pad	    	),
				.di_dir_frompad						(di_dir_pad2padmisc	    		),
				.di_hstrb_frompad					(di_hstrb_pad2padmisc	    	),
				.di_dstrb_out2pad					(di_dstrb_padmisc2pad	    	),
				.di_err_out2pad						(di_err_padmisc2pad	    		),
				.di_cover_out2pad					(di_cover_padmisc2pad	    	),
				.di_stb_frompad						(di_stb_pad2padmisc				),

				.ais_clk_frompad					(ais_clk_pad2padmisc			),
				.ais_lr_frompad						(ais_lr_pad2padmisc	    		),
				.ais_d_out2pad						(ais_d_padmisc2pad		    	),

//--------------I2C Intf
                //connect to core
                .scbsda_out2core        			(scbsda_padmisc2core            ),
                .scbscl_out2core        			(scbscl_padmisc2core            ),
                .scbsda_fromcore        			(scbsda_core2padmisc            ),
                .scbscl_fromcore        			(scbscl_core2padmisc            ),
                .scbsda_oe_fromcore     			(scbsda_oe_core2padmisc         ),
                .scbscl_oe_fromcore     			(scbscl_oe_core2padmisc         ),
				//connect to pad
				.scbsda_frompad	        			(scbsda_pad2padmisc             ),
                .scbscl_frompad	        			(scbscl_pad2padmisc             ),
                .scbsda_out2pad        				(scbsda_padmisc2pad             ),
                .scbscl_out2pad        				(scbscl_padmisc2pad             ),
                .scbsda_oe_out2pad_     			(scbsda_oe_padmisc2pad_         ),
                .scbscl_oe_out2pad_     			(scbscl_oe_padmisc2pad_         ),
//------IR
				.irrx_frompad						(irrx_pad2padmisc				),
				.irrx_out2pad						(irrx_padmisc2pad				),
				.irrx_oe_out2pad_					(irrx_oe_padmisc2pad_			),
				.irrx_out2core						(irrx_padmisc2core				),
//-----UART
				//connect to core
				.uart_tx_fromcore					(uart_tx_core2padmisc			),
				.uart_rx_out2core					(uart_rx_padmisc2core			),
				//connect to pad
				.uart_tx_out2pad					(uart_tx_padmisc2pad			),
				.uart_tx_oe_out2pad_				(uart_tx_oe_padmisc2pad_		),
				.uart_tx_frompad					(uart_tx_pad2padmisc			),
				.uart_rx_frompad					(uart_rx_pad2padmisc			),
//--------------USB
                //connect to pad
				.usbpon_out2pad						(usbpon_padmisc2pad				),
				.usbpon_oe_out2pad_					(usbpon_oe_padmisc2pad_			),
				.usbovc_frompad						(usbovc_pad2padmisc				),
				//connect to core
				.usbovc_out2core					(usbovc_padmisc2core			),
				.usbpon_fromcore					(usbpon_core2padmisc_			),
//------I2S
				//connect to pad
        		.i2so_data0_out2pad					(i2so_data0_padmisc2pad			),
				.i2so_data1_out2pad					(i2so_data1_padmisc2pad			),
				.i2so_data2_out2pad					(i2so_data2_padmisc2pad			),
				.i2so_data3_out2pad					(i2so_data3_padmisc2pad			),
				.i2so_data0_oe_out2pad_				(i2so_data0_oe_padmisc2pad_		),
				.i2so_data1_oe_out2pad_				(i2so_data1_oe_padmisc2pad_		),
				.i2so_data2_oe_out2pad_				(i2so_data2_oe_padmisc2pad_		),
				.i2so_data3_oe_out2pad_				(i2so_data3_oe_padmisc2pad_		),
				.i2so_data0_frompad					(i2so_data0_pad2padmisc			),
				.i2so_data1_frompad					(i2so_data1_pad2padmisc			),
				.i2so_data2_frompad					(i2so_data2_pad2padmisc			),
				.i2so_data3_frompad					(i2so_data3_pad2padmisc			),
				.i2si_data_frompad					(i2si_data_pad2padmisc			),
				.i2si_mclk_out2pad					(i2si_mclk_padmisc2pad			),
				.i2si_bck_out2pad					(i2si_bck_padmisc2pad			),
				.i2si_lrck_out2pad					(i2si_lrck_pad2padmisc			),
                .i2si_data_out2core     			(i2si_data_padmisc2core         ),
  				.i2si_bick_out2core					(slave_i2si_bick_padmisc2core	),
  				.i2si_lrck_out2core					(slave_i2si_lrck_padmisc2core   ),
				.i2si_mode_sel_fromcore  			(i2si_mode_sel_core2padmisc     ),
				.i2so_data0_fromcore				(i2so_data0_core2padmisc		),
				.i2so_data1_fromcore				(i2so_data1_core2padmisc		),
				.i2so_data2_fromcore				(i2so_data2_core2padmisc		),
				.i2so_data3_fromcore				(i2so_data3_core2padmisc		),
				.i2so_data4_fromcore				(i2so_data4_core2padmisc		),
				.i2so_ch10_en_fromcore				(i2so_ch10_en_core2padmisc		),
				.i2so_mclk_fromcore					(i2so_mclk_gen2core				),
				.i2so_mclk_oe_fromcore_				(i2so_mclk_oe_core2padmisc_		),
				.i2so_mclk_out2pad					(i2so_mclk_padmisc2pad			),
				.i2so_mclk_oe_out2pad_				(i2so_mclk_oe_padmisc2pad_		),
				.i2so_mclk_out2core					(i2so_mclk_padmisc2core			),
				.i2so_mclk_frompad					(i2so_mclk_pad2padmisc			),
				.i2so_bck_out2pad					(i2so_bck_padmisc2pad			),
				.i2so_bck_oe_out2pad_				(i2so_bck_oe_padmisc2pad_		),
				.i2so_bck_fromcore					(i2so_bick_core2padmisc			),
				.i2so_bck_frompad					(i2so_bck_pad2padmisc			),
				.i2so_lrck_out2pad					(i2so_lrck_padmisc2pad			),
				.i2so_lrck_oe_out2pad_				(i2so_lrck_oe_padmisc2pad_		),
				.i2so_lrck_fromcore					(i2so_lrck_core2padmisc			),
				.i2so_lrck_frompad					(i2so_lrck_pad2padmisc			),
        		.i2si_mclk_fromcore					(i2so_mclk_gen2core				),		// I2SO and I2SI use the same master clock
				.i2si_bck_fromcore					(i2si_bck_core2padmisc			),
				.i2si_lrck_fromcore					(i2si_lrck_core2padmisc			),

				.iadc_smtsen_out2core				(iadc_smtsen_padmisc2core		),		// ADC SRAM Test Enable
				.iadc_smtsmode_out2core				(iadc_smtsmode_padmisc2core		),		// ADC SRAM Test Mode
				.iadc_smts_err_fromcore				(iadc_smts_err_core2padmisc		),		// ADC SRAM Test Error Flag
				.iadc_smts_end_fromcore				(iadc_smts_end_core2padmisc		),		// ADC SRAM Self Test End
				.iadc_clkp1_fromcore				(iadc_clkp1_core2padmisc		),		// ADC digital test pin 1
				.iadc_clkp2_fromcore				(iadc_clkp2_core2padmisc		),		// ADC digital test pin 2
				.iadc_tst_mod_out2core				(iadc_tst_mod_padmisc2core		),		// ADC Internal SDM Test Selection
//--------------spdif output enable
                //connect to pad
				.spdif_out2pad						(spdif_padmisc2pad				),
				.spdif_oe_out2pad_					(spdif_oe_padmisc2pad_			),
				.spdif_fromcore						(spdif_core2padmisc				),
				.spdif_frompad						(spdif_pad2padmisc				),

				.spdif_in_data_out2core				(spdif_in_data_padmisc2core		),		// SPDIF in data to core
				.spdif_in_data_frompad				(spdif_in_data_pad2padmisc		),
//----- video reset and its enable,share with gpio[27]
                //tv encoder
                //connect to core
				.tvenc_h_sync_fromcore_				(tvenc_h_sync_core2padmisc_		),
				.tvenc_v_sync_fromcore_				(tvenc_v_sync_core2padmisc_		),
				//connect to pad
				.tvenc_h_sync_out2pad_				(tvenc_h_sync_padmisc2pad_		),
				.tvenc_v_sync_out2pad_				(tvenc_v_sync_padmisc2pad_		),
			    //this tvencoder signals for sram test
				.vdi4vdac_out2core					(tvenc_vdi4vdac_padmisc2core	),
				.vdo4vdac_fromcore					(tvenc_vdo4vdac_core2padmisc	),
				//digital video out
				//connect to core
				.vdata_fromcore						(vdata_core2padmisc				),
				.de_h_sync_fromcore_				(de_h_sync_core2padmisc_		),
				.de_v_sync_fromcore_				(de_v_sync_core2padmisc_		),
				.pix_clk_fromcore					(out_pixel_clk_gen2padmisc		),	//from clock generator directly
				//connect to pad
			    .vdata_out2pad 						(vdata_padmisc2pad				),
				.de_h_sync_out2pad_	                (de_h_sync_padmisc2pad_	        ),
				.de_v_sync_out2pad_	                (de_v_sync_padmisc2pad_	        ),
				.pix_clk_out2pad                     (pixel_clk_padmisc2pad			),
				// VIDEO_IN
				//connect to core
				.video_in_pixel_clk_out2core		(video_in_pixel_clk_padmisc2core	),
				.video_in_pixel_data_out2core		(video_in_pixel_data_padmisc2core	),
				.video_in_hsync_out2core_			(video_in_hsync_padmisc2core_		),
				.video_in_vsync_out2core_			(video_in_vsync_padmisc2core_		),
				//connect to pad
				.video_in_pixel_clk_frompad			 (video_in_pixel_clk_pad2padmisc		),
				.video_in_pixel_data_frompad         (video_in_pixel_data_pad2padmisc	),
				.video_in_hsync_frompad_             (video_in_hsync_pad2padmisc_		),
				.video_in_vsync_frompad_             (video_in_vsync_pad2padmisc_		),

//==================== MS/SD =========================
				.sd_ip_en_fromcore					(sd_ip_enable_core2padmisc	),					// SD IP enable
				.sd_clk_fromcore					(sd_mmc_clk_core2padmisc	),					// SD card clock

				.sd_cmd_fromcore					(sd_mmc_cmd_out_core2padmisc),					// SD card command/status from core
				.sd_cmd_oe_fromcore_				(sd_mmc_cmd_oej_core2padmisc),				// SD card command/status oe# from core
				.sd_cmd_out2core					(sd_mmc_cmd_in_padmisc2core	),					// SD card command/status out 2 core

				.sd_data_fromcore					(sd_data_out_core2padmisc	),					// SD card data from core
				.sd_data_oe_fromcore_				(sd_data_oej_core2padmisc	),				// SD card data oe# from core
				.sd_data_out2core					(sd_data_in_padmisc2core	),					// SD card data out 2 core

				.sd_wr_prct_out2core				(wr_prct_pin_padmisc2core	),				// SD card locked
				.sd_det_out2core					(det_pin_padmisc2core		),					// SD card inserted

				.ms_ip_en_fromcore					(ms_ip_enable_core2padmisc	),					// MS IO enable
				.ms_clk_fromcore					(ms_clk_core2padmisc		),					// MS clock
				.ms_pwr_ctrl_fromcore				(ms_pw_ctrl_core2padmisc	),				// MS power control from core
				.ms_bs_fromcore						(ms_bsst_core2padmisc		),						// MS bus status from core

				.ms_sdio_fromcore					(ms_serout_core2padmisc		),					// MS sdio from core
				.ms_sdio_oe_fromcore_				(msde_core2padmisc			),				// MS sdio oe# from core
				.ms_sdio_out2core					(ms_serin_in_padmisc2core	),					// MS sdio out 2 core

				.ms_ins_out2core					(ms_ins_padmisc2core		),					// MS card insert indicated signal

				.sd_ms_bist_tri_out2core			(test_h_padmisc2core		),			// SD/MS bist test trigger
				.sd_ms_bist_finish_fromcore			(test_done_core2padmisc		),			// SD/MS bist finish
				.sd_ms_bist_vec_fromcore			(fail_h_core2padmisc		),			// SD/MS bist result vector

				.sd_clk_out2pad						(sd_mmc_clk_padmisc2pad	),					// SD card clock

				.sd_cmd_out2pad						(sd_mmc_cmd_out_padmisc2pad	),					// SD card command/status from core
				.sd_cmd_oe_out2pad_					(sd_mmc_cmd_oej_padmisc2pad	),				// SD card command/status oe# from core
				.sd_cmd_frompad						(sd_mmc_cmd_in_pad2padmisc	),					// SD card command/status out 2 core

				.sd_data_out2pad					(sd_data_out_padmisc2pad	),					// SD card data from core
				.sd_data_oe_out2pad_				(sd_data_oej_padmisc2pad	),				// SD card data oe# from core
				.sd_data_frompad					(sd_data_in_pad2padmisc		),					// SD card data out 2 core

				.sd_wr_prct_frompad					(wr_prct_pin_pad2padmisc	),				// SD card locked
				.sd_det_frompad						(det_pin_pad2padmisc		),					// SD card inserted

//				.ms_ip_en_out2pad					(ms_ip_enable_padmisc2pad	),					// MS IO enable
				.ms_clk_out2pad						(ms_clk_padmisc2pad			),					// MS clock
				.ms_pwr_ctrl_out2pad				(ms_pw_ctrl_padmisc2pad		),				// MS power control from core
				.ms_bs_out2pad						(ms_bsst_padmisc2pad		),						// MS bus status from core

				.ms_sdio_out2pad					(ms_serout_padmisc2pad		),					// MS sdio from core
				.ms_sdio_oe_out2pad_				(msde_padmisc2pad			),				// MS sdio oe# from core
				.ms_sdio_frompad					(ms_serin_in_pad2padmisc	),					// MS sdio out 2 core

				.ms_ins_frompad						(ms_ins_pad2padmisc			),					// MS card insert indicated signal

//------SERVO
				.sv_gpio_fromcore					(sv_gpio_core2padmisc			),
				.sv_gpio_oe_fromcore_				(sv_gpio_oe_core2padmisc_		),
				.sv_gpio_out2core					(sv_gpio_padmisc2core			),

				.sv_atadd_fromcore					(sv_atadd_core2padmisc			),
				.sv_ataiordy_fromcore				(sv_ataiordy_core2padmisc		),
				.sv_ataintrq_fromcore				(sv_ataintrq_core2padmisc		),
				.sv_atadmarq_fromcore				(sv_atadmarq_core2padmisc		),
				.sv_ataiocs16_fromcore_				(sv_ataiocs16_core2padmisc_		),
				.sv_atapdiag_fromcore_				(sv_atapdiag_core2padmisc_		),
				.sv_atadasp_fromcore_				(sv_atadasp_core2padmisc_		),

				.sv_tst_pin_fromcore				(sv_tst_pin_core2padmisc    	),
				.sram_ifx_fromcore					(sram_ifx_core2padmisc			),

				.sv_atadd_oe_fromcore_				(sv_atadd_oe_core2padmisc_		),
				.sv_ataiocs16_oe_fromcore_			(sv_ataiocs16_oe_core2padmisc_	),
				.sv_atapdiag_oe_fromcore_			(sv_atapdiag_oe_core2padmisc_	),
				.sv_atadasp_oe_fromcore_			(sv_atadasp_oe_core2padmisc_	),

				.sram_ifx_oe_fromcore_				(sram_ifx_oe_core2padmisc_		),

				.sram_ifx_out2core					(sram_ifx_padmisc2core			),

				.sv_atadd_out2core					(sv_atadd_padmisc2core			),
				.sv_atada_out2core					(sv_atada_padmisc2core			),
				.sv_atacs0_out2core_				(sv_atacs0_padmisc2core_		),
				.sv_atacs1_out2core_				(sv_atacs1_padmisc2core_		),
				.sv_atadior_out2core_				(sv_atadior_padmisc2core_		),
				.sv_atadiow_out2core_				(sv_atadiow_padmisc2core_		),
				.sv_atadmack_out2core_				(sv_atadmack_padmisc2core_		),
				.sv_atareset_out2core_				(sv_atareset_padmisc2core_		),
				.sv_atapdiag_out2core_				(sv_atapdiag_padmisc2core_		),
				.sv_atadasp_out2core_				(sv_atadasp_padmisc2core_		),

				.ext_up_out2core					(ext_up_padmisc2core			),
				.ext_up_fromcore					(ext_up_core2padmisc			),
				.ext_up_oe_fromcore_				(ext_up_oe_core2padmisc_		),

				.sv_tplck_fromcore					(sv_sv_tplck_core2padmisc			),
				.sv_tslrf_fromcore					(sv_sv_tslrf_core2padmisc			),

				.pmsvd_trcclk_out2core				(sv_pmsvd_trcclk_padmisc2core		),
				.pmsvd_trfclk_out2core				(sv_pmsvd_trfclk_padmisc2core		),
				.pmsvd_tplck_out2core				(sv_pmsvd_tplck_padmisc2core		),
				.pmsvd_tslrf_out2core				(sv_pmsvd_tslrf_padmisc2core		),
				.pmsvd_c2ftstin_0_out2core			(sv_pmsvd_c2ftstin_padmisc2core[0]),
				.pmsvd_c2ftstin_1_out2core			(sv_pmsvd_c2ftstin_padmisc2core[1]),

				.sv_tst_addata_out2core				(sv_tst_addata_padmisc2core		),		// Servo External A/D data input
				.sv_reg_rctst_2_fromcore			(sv_reg_rctst_2_core2padmisc	),		// Servo RC test mode enable

				.xsflag_0_out2pad					(sv_xsflag_padmisc2pad[0]		),
				.xsflag_1_out2pad					(sv_xsflag_padmisc2pad[1]		),
				.xsflag_0_oe_out2pad_				(sv_xsflag_oe_padmisc2pad_[0]	),
				.xsflag_1_oe_out2pad_				(sv_xsflag_oe_padmisc2pad_[1]	),
				.xsflag_0_frompad					(sv_xsflag_pad2padmisc[0]		),
				.xsflag_1_frompad					(sv_xsflag_pad2padmisc[1]		),
//				.xsflag_0_out2core					(sv_xsflag_padmisc2core[0]		),
//				.xsflag_1_out2core					(sv_xsflag_padmisc2core[1]		),
				.xsflag_0_fromcore					(sv_xsflag_core2padmisc[0]		),
				.xsflag_1_fromcore					(sv_xsflag_core2padmisc[1]		),
				.xsflag_1_oe_fromcore_				(1'b0							),		//	servo flag is output only
				.xsflag_0_oe_fromcore_				(1'b0							),		//	servo flag is output only

//	SERVO control
				.sv_tst_pin_en_fromcore				(sv_tst_pin_en_core2padmisc				),	// SERVO test pin enable
				.sv_ide_mode_part0_en_fromcore		(sv_ide_mode_part0_en_core2padmisc		),	// ide mode SERVO test pin part 0 enable
				.sv_ide_mode_part1_en_fromcore		(sv_ide_mode_part1_en_core2padmisc		),	// ide mode SERVO test pin part 1 enable
				.sv_ide_mode_part2_en_fromcore		(sv_ide_mode_part2_en_core2padmisc		),	// ide mode SERVO test pin part 2 enable
				.sv_ide_mode_part3_en_fromcore		(sv_ide_mode_part3_en_core2padmisc		),	// ide mode SERVO test pin part 3 enable
				.sv_c2ftstin_en_fromcore			(sv_c2ftstin_en_core2padmisc			),	// SERVO c2 flag enable
				.sv_trg_plcken_fromcore				(sv_trg_plcken_core2padmisc				),	// SERVO tslrf and tplck enable
				.sv_pmsvd_trcclk_trfclk_en_fromcore	(sv_pmsvd_trcclk_trfclk_en_core2padmisc	),	// SERVO pmsvd_trcclk, pmsvd_trfclk enable

//	Control signals
				.hv_sync_en_fromcore				(hv_sync_en_core2padmisc		),		// h_sync_, v_sync_ enable on the xsflag[1:0] pins
				.i2si_clk_en_fromcore				(i2si_clk_en_core2padmisc		),		// I2S input data enable on the gpio[7] pin
				.scb_en_fromcore					(scb_en_core2padmisc			),		// SCB interface enable on the gpio[6:5] pins
				.sv_gpio_en_fromcore				(sv_gpio_en_core2padmisc		),		// servo_gpio[2:0] enable on the gpio[2:0] pins
				.ext_ide_device_en_fromcore			(ext_ide_device_en_core2padmisc	),		// external IDE device enable
				.vdata_en_fromcore					(vdata_en_core2padmisc			),		// video data enable on sdram [23:16]
				.sync_source_sel_fromcore			(sync_source_sel_core2padmisc	),		// sync signal source select, 0 -> TVENC, 1 -> DE.
				.pci_gntb_en_fromcore				(1'b0							),		// pci GNTB enable
				.video_in_intf_en_fromcore			(video_in_intf_en_core2padmisc	),		// video in interface enable
				.pci_mode_ide_en_fromcore			(1'b0							),		// pci_mode ide interface enable
				.ram_cs1_en_fromcore				(sdram_cs1j_en_core2padmisc		),		// sdram cs1# enable
				.ata_share_mode_2_en_fromcore		(ata_share_mode_2_en_core2padmisc),	// ATA interface sharing mode 2 enable
				.xsflag_gpio_en_fromcore	 		(xsflag_gpio_en_core2padmisc 	),	// gpio on xsflag[1:0] port enable
				.spdif_gpio_en_fromcore		 		(spdif_gpio_en_core2padmisc	),
				.i2so_data12_gpio_en_fromcore		(i2so_data2_1_gpio_en_core2padmisc),	// gpio on i2so_data1, i2so_data2 enable
				.i2so_data3_gpio_en_fromcore 		(i2so_data3_gpio_en_core2padmisc),	// gpio on i2so_data3 enable
				.uart_gpio_en_fromcore		 		(uart_gpio_en_core2padmisc		),	// gpio on uart enable

				.ide_active_fromcore				(ide_active_core2padmisc		),		// ide interface is active in the pci mode
				.ata_ad_1413_sel_fromcore	 		(ata_ad_src_sel_core2padmisc	),
				.hv_sync_gpio_en_fromcore			(hv_sync_gpio_encore2padmisc	),

				.ice_probe_en_fromcore				(cpu_probe_en					),		// ICE probe enable. When disabled, the EJTAG signals will

//------TEST_MODE
				.dsp_bist_tri_out2core				(dsp_bist_tri_padmisc2core		),
				.dsp_bist_finish_fromcore			(dsp_bist_finish_core2padmisc	),
				.dsp_bist_vec_fromcore				(dsp_bist_vec_core2padmisc		),

				.video_bist_tri_out2core			(video_bist_tri_padmisc2core	),
				.video_bist_finish_fromcore			(video_bist_finish_core2padmisc	),
				.video_bist_vec_fromcore			(video_bist_vec_core2padmisc	),

                .rom_en_fromcore_       			(rom_en_core2padmisc_   		),

                .combo_mode							(combo_mode						),
                .ide_mode            				(ide_mode            			),      //select ide mode, 1 means ide mode
                .pci_mode               			(1'b0		               		),      //select pci_mode, 1 means pci
                .tvenc_test_mode					(tvenc_test_mode				),
                .servo_only_mode					(servo_only_mode				),
                .servo_test_mode					(servo_test_mode				),
                .servo_sram_mode					(servo_sram_mode				),

                .mem_clk_in             			(inter_mem_clk_gen      		),
        		.cpu_pll_fck_fromcore				(cpu_clk_pll_fck				),
        		.cpu_pll_lock_fromcore				(cpu_clk_pll_lock				),
        		.mem_pll_fck_fromcore				(mem_clk_pll_fck				),
        		.mem_pll_lock_fromcore				(mem_clk_pll_lock				),
        		.audio_pll_fck_fromcore				(audio_pll_fck					),
        		.audio_pll_lock_fromcore			(audio_pll_lock					),
        		.f27_pll_fck_fromcore				(f27_pll_fck					),
        		.f27_pll_lock_fromcore				(f27_pll_lock					),

        		.ide_clk_fromcore					(ata_clk_gen2core				),		// for test
        		.dsp_clk_fromcore					(dsp_clk_gen2core				),		// 	dsp clk output for tester
        		.video_clk_fromcore					(video_clk_gen2core				),		//	video clk output for tester

                .test_mode_frompad      			(test_mode_frompad      		),
                .scan_en_frompad					(j_tms_pad2padmisc				),
                .scan_en_out2core					(scan_en_padmisc2core			),
                .scan_chain_sel_out2core			(test_sel_padmisc2core			),
                .scan_dout_fromcore					(scan_dout_mux2padmisc			),
                .scan_din_out2core					(scan_din_padmisc2mux			),
                .mem_rd_dly_sel						(mem_rd_dly_sel					),
				.test_dly_chain_sel					(test_dly_chain_sel				),	// TEST_DLY_CHAIN delay select
				.test_dly_chain_flag				(test_dly_chain_flag			),	// TEST_DLY_CHAIN flag output

                .bond_option_frompad				(bond_option_pad2padmisc		),

                .bist_test_mode						(bist_test_mode_padmisc2core	),
                .scan_test_mode						(scan_test_mode_padmisc2core	),
                .rst_                   			(pad_rst_		           		)

                );
//======================End of invoke the pad_misc================================
scan_mux  SCAN_MUX(
				.scan_din_out2core		(scan_din						),
				.scan_dout_fromcore		(scan_dout						),
				.scan_din_frompadmisc	(scan_din_padmisc2mux			),
				.scan_dout_out2padmisc	(scan_dout_mux2padmisc			),

				.test_mode				(scan_test_mode_padmisc2core	),
				.test_sel				(test_sel_padmisc2core			)
				);
//=================== invoke the pad =================================

iopad   IOPAD   (
//========================== pad ===========================================
// SDRAM Interface (55)
                .p_d_clk                (p_d_clk                ),      // 1,   SDRAM master clock input
                .p_d_addr               (p_d_addr               ),      // 12,  SDRAM address
                .p_d_baddr              (p_d_baddr              ),      // 2,   SDRAM bank address
                .p_d_dq                 (p_d_dq                 ),      // 32,  SDRAM 32 bits output data pins
                .p_d_dqm                (p_d_dqm                ),      // 4,   SDRAM data mask signal
                .p_d_cs_                (p_d_cs_                ),      // 2,   SDRAM chip select
                .p_d_cas_               (p_d_cas_               ),      // 1,   SDRAM cas#
                .p_d_we_                (p_d_we_                ),      // 1,   SDRAM we#
                .p_d_ras_               (p_d_ras_               ),      // 1,   SDRAM ras#
// FLASH ROM Interface(33)
				.p_rom_ale				(p_rom_ale				),		// 1	Flash rom external 373 latch enable
//                .p_rom_ce_              (p_rom_ce_              ),      // 1,   Flash rom chip select
                .p_rom_we_              (p_rom_we_              ),      // 1,   Flash rom write enable
                .p_rom_oe_              (p_rom_oe_              ),      // 1,   Flash rom output enable
				.p_rom_data				(p_rom_data				),		// 8 	Flash rom data
        		.p_rom_addr				(p_rom_addr				),		// 21	Flash rom address
				.p_rom_addr_set2		(p_rom_addr_set2		),		// 2	Flash rom address set 2 [18:17] for bonding
// IDE Interface
				.atadiow_				(atadiow_				),
				.atadior_    			(atadior_				),
				.atacs0_     			(atacs0_ 				),
				.atacs1_     			(atacs1_ 				),
				.atada       			(atada   				),
				.atareset_   			(atareset_				),
				.atadmack_   			(atadmack_				),
				.ataiordy				(ataiordy				),
				.ataintrq    			(ataintrq				),
				.atadmarq    			(atadmarq				),
				.atadd					(atadd					),
// PCI Interface
				.p_clk					(p_clk					),
				.p_gnta_     			(p_gnta_    			),
				.p_gntb_     			(p_gntb_    			),
				.p_rst_      			(p_rst_     			),
				.p_req_					(p_req_					),
				.p_inta_     			(p_inta_    			),
				.p_ad					(p_ad					),
				.p_par       			(p_par      			),
				.p_frame_    			(p_frame_   			),
				.p_irdy_     			(p_irdy_    			),
				.p_trdy_     			(p_trdy_    			),
				.p_stop_     			(p_stop_    			),
				.p_devsel_   			(p_devsel_  			),
				.p_cbe_      			(p_cbe_     			),
				.p_serr_                (p_serr_                ),
				.p_perr_                (p_perr_                ),
// I2S Audio Interface (11)
                .i2so_data0             (i2so_data0             ),      // 1,   Serial data0 for I2S output
                .i2so_data1				(i2so_data1				),		// 1,	Serial data1 for I2S output
                .i2so_data2				(i2so_data2				),		// 1,	Serial data2 for I2S output
                .i2so_data3				(i2so_data3				),		// 1,	Serial data3 for I2S output
                .i2so_bck               (i2so_bck               ),      // 1,   Bit clock for I2S output
                .i2so_lrck              (i2so_lrck              ),      // 1,   Channel clock for I2S output
                .i2so_mclk              (i2so_mclk              ),      // 1,   over sample clock for i2s DAC
                .i2si_data              (i2si_data              ),      // 1,   serial data for I2S input
				.i2si_lrck				(i2si_lrck				),
				.i2si_bck				(i2si_bck				),
				.i2si_mclk				(i2si_mclk				),
//TV encoder Interface (14+2)
				.gnda_tvdac				(gnda_tvdac				),		// 1,	Analog GND, input
				.gnda1_tvdac			(gnda1_tvdac			),		// 1,	Analog GND, input
				.vsud_tvdac				(vsud_tvdac				),		// 1,	Analog GND, input
				.vd33a_tvdac			(vd33a_tvdac 			),		// 3,	Analog Power, input
				.iout1					(iout1					),		// 1,   Analog video output 1
				.iout2					(iout2					),		// 1,   Analog video output 2
				.iout3					(iout3					),		// 1,   Analog video output 3
				.iout4					(iout4					),		// 1,   Analog video output 4
				.iout5					(iout5					),		// 1,   Analog video output 5
				.iout6					(iout6					),		// 1,   Analog video output 6

//added for m3357 JFR030613
				.xiref1					(xiref1					),		// 1,	connect external via capacitance toward VDDA
//				.xiref2					(xiref2					),		// 1,	connect external via capacitance toward VDDA
				.xiext					(xiext					),		// 1,	470 Ohms resistor connect pin, I/O
				.xiext1					(xiext1					),		// 1,	470 Ohms resistor connect pin, I/O
				.xidump					(xidump					),		// 1,	For performance and chip temperature, output
				//add 2 digital pin(M3358)
				.tven_hsync_			(tven_hsync_			),
				.tven_vsync_            (tven_vsync_            ),
//DIGITAL VIDEO OUT (11)
			    .pix_clk				(pix_clk		        ),
				.vout_data				(vout_data				),
				.vout_hsync_			(vout_hsync_			),
				.vout_vsync_ 			(vout_vsync_ 			),
//DIGITAL VIDEO IN
				.vin_pix_clk			(vin_pix_clk			),
				.vin_pix_data           (vin_pix_data           ),
				.vin_hsync_             (vin_hsync_             ),
				.vin_vsync_             (vin_vsync_             ),
//GI (from broadon)
			    .gc_vid_clk				(GCVIDCLK	 			),
				.gc_sys_clk0	        (GCSYSCLK[0] 			),
				.gc_sys_clk1	        (GCSYSCLK[1] 			),
				.alt_boot               (ALTBOOT    			),
				.test_ena               (TESTEN   				),
				.gc_test_rst  			(GCRSTB					),
				.prom_scl               (PROMSCL    			),
				.prom_sda               (PROMSDA    			),
				.di_data                (DID	     			),
				.di_brk                 (DIBRK      			),
				.di_dir                 (DIDIR      			),
				.di_err                 (DIERRB      			),
				.di_dstrb               (DIDSTRBB	    		),
				.di_hstrb               (DIHSTRBB    			),
				.di_cover               (DICOVER    			),
				.di_stb					(DIRSTB					),
				.ais_clk                (AISCLK     			),
				.ais_lr                 (AISLR      			),
				.ais_d                  (AISD       			),

//SD_MS(13)
				.sd_clk					(sd_clk					),
				.sd_cmd         		(sd_cmd         		),
				.sd_data        		(sd_data        		),
				.sd_wr_prct     		(sd_wr_prct     		),
				.sd_det         		(sd_det         		),
				.ms_clk         		(ms_clk         		),
				.ms_pwr_ctrl    		(ms_pwr_ctrl    		),
				.ms_bs          		(ms_bs          		),
				.ms_sdio        		(ms_sdio        		),
				.ms_ins         		(ms_ins         		),
// SPDIF (2)
                .spdif                  (spdif                  ),      // 1, 	output only
				.spdif_in				(spdif_in				),
//UART port (2)JFR030730
				.uart_tx				(uart_tx				),		// 1
				.uart_rx				(uart_rx				),
// USB port (6)
                .usb2dp                 (usb2dp                 ),      // 1,   D+ for USB port2
                .usb2dn                 (usb2dn                 ),      // 1,   D- for USB port2
                .usb1dp                 (usb1dp                 ),      // 1,   D+ for USB port1
                .usb1dn                 (usb1dn                 ),      // 1,   D- for USB port1
                .usbpon                 (usbpon                 ),      // 1,   USB port power control
                .usbovc                 (usbovc                 ),      // 1,   USB port over current
// Consumer IR Interface (1)
                .irrx                   (irrx                   ),      // 1,   consumer infra-red remote controller/wireless keyboard
// I2C Interface
				.scbsda					(scbsda					),
				.scbscl					(scbscl					),
// General Purpose IO (32,GPIO)
                .gpio                   (gpio                   ),      // 16,   General purpose io[15:0]
// EJTAG Interface (5)
                .p_j_tdo                (p_j_tdo                ),      // 1,   serial test data output
                .p_j_tdi                (p_j_tdi                ),      // 1,   serial test data input
                .p_j_tms                (p_j_tms    			),      // 1,   test mode select
                .p_j_tclk               (p_j_tclk               ),      // 1,   test clock
                .p_j_rst_               (p_j_rst_               ),      // 1,   test reset
// system (4)
                .p_crst_                (p_crst_                ),      // 1,   cold reset
                .test_mode              (test_mode              ),
				.p_x27in				(p_x27in				),      // 1,   crystal input (27 MHz for video output)
        		.p_x27out        		(p_x27out				),		// 1,   crystal output
// power and ground
				.vddcore				(vddcore				),		// 		core vdd
//				.vsscore				(vsscore				),		//		core vss
				.vddio					(vddio					),		//		io vdd
				.gnd					(gnd					),		//		io vss
//				.cpu_pllvdd				(cpu_pllvdd				),		//		cpu pll vdd
//				.cpu_pllvss				(cpu_pllvss				),		//		cpu pll vss
				.f27_pllvdd				(f27_pllvdd				),		//		f27 pll vdd
				.f27_pllvss             (f27_pllvss             ),      //		f27 pll vss
//ADC (5)
				.vd33a_adc				(vd33a_adc	 			),		// 1,	3.3V Analog VDD for ADC.
				.xmic1in     			(xmic1in     			),      // 1,   ADC microphone input 1.
				.xadc_vref   			(xadc_vref   			),      // 1,   ADC reference voltage (refered to the reference circuit).
				.xmic2in     			(xmic2in     			),      // 1,   ADC microphone input 2.
				.gnda_adc    			(gnda_adc    			),   	// 1,   Analog GND for ADC.
//=========================== internal net =======================================
                // SDRAM & FLASH
                .ram_addr_frompadmisc   	(ram_addr_padmisc2pad		),
                .ram_addr_oe_frompadmisc_	(ram_addr_oe_padmisc2pad	),
                .ram_ba_frompadmisc     	(ram_ba_padmisc2pad     	),
                .ram_ba_oe_frompadmisc_		(ram_ba_oe_padmisc2pad_		),
                .ram_dq_frompadmisc     	(ram_dq_padmisc2pad			),//p_ram_dq_out, change the dq bus from core directly,yuky,2002-07-09
                .ram_dq_oe_frompadmisc_ 	(ram_dq_oe_padmisc2pad_     ),
                .ram_cas_frompadmisc_   	(ram_cas_padmisc2pad_       ),
                .ram_cas_oe_frompadmisc_	(ram_cas_oe_padmisc2pad_	),
                .ram_we_frompadmisc_    	(ram_we_padmisc2pad_        ),
                .ram_we_oe_frompadmisc_		(ram_we_oe_padmisc2pad_		),
                .ram_ras_frompadmisc_   	(ram_ras_padmisc2pad_       ),
                .ram_ras_oe_frompadmisc_	(ram_ras_oe_padmisc2pad_	),

                .ram_dqm_frompadmisc    	(ram_dqm_padmisc2pad        ),
                .ram_dqm_oe_frompadmisc_	(ram_dqm_oe_padmisc2pad_	),
                .ram_cs_frompadmisc_    	(ram_cs_padmisc2pad_        ),
                .ram_cs_oe_frompadmisc_		(ram_cs_oe_padmisc2pad_		),

                .ram_dq_out2padmisc     	(ram_dq_pad2padmisc         ),		//strap pins
                .ram_addr_out2padmisc   	(ram_addr_pad2padmisc   	),      //strap pins
                .ram_ba_out2padmisc     	(ram_ba_pad2padmisc     	),      //strap pins
                .ram_dqm_out2padmisc    	(ram_dqm_pad2padmisc    	),      //strap pins
                .ram_cs_out2padmisc_		(ram_cs_pad2padmisc_		),
                .ram_cas_out2padmisc_		(ram_cas_pad2padmisc_		),

                // EEPROM
//                .rom_ce_frompadmisc_		(rom_ce_padmisc2pad_	),
                .rom_oe_frompadmisc_		(rom_oe_padmisc2pad_	),
                .rom_we_frompadmisc_		(rom_we_padmisc2pad_	),
         		.rom_data_frompadmisc    	(rom_data_padmisc2pad	),
        		.rom_addr_frompadmisc		(rom_addr_padmisc2pad	),
//                .rom_ce_oe_frompadmisc_		(rom_ce_oe_padmisc2pad_	),
                .rom_oe_oe_frompadmisc_		(rom_oe_oe_padmisc2pad_	),
                .rom_we_oe_frompadmisc_		(rom_we_oe_padmisc2pad_	),
				.rom_data_oe_frompadmisc_ 	(rom_data_oe_padmisc2pad_),
				.rom_addr_oe_frompadmisc_ 	(rom_addr_oe_padmisc2pad_),
//                .rom_ce_out2padmisc_		(rom_ce_pad2padmisc_	),
                .rom_oe_out2padmisc_		(rom_oe_pad2padmisc_	),
                .rom_we_out2padmisc_		(rom_we_pad2padmisc_	),
                .rom_ale_frompadmisc		(eeprom_ale_padmisc2pad	    ),
        		.rom_ale_oe_frompadmisc_	(eeprom_ale_oe_padmisc2pad_ ),
        		.rom_ale_out2padmisc		(eeprom_ale_pad2padmisc	    ),
//added for m3357 JFR030613
				.rom_data_out2padmisc		(rom_data_pad2padmisc	),
        		.rom_addr_out2padmisc    	(rom_addr_pad2padmisc	),
				.rom_addr_set2_out2padmisc	(rom_addr_set2_pad2padmisc),
//--------------PCI
			    .pci_clk_frompadmisc		(pci_clk_padmisc2pad			),
				.pci_gnta_frompadmisc_	    (pci_gnta_padmisc2pad_	        ),
				.pci_gntb_frompadmisc_	    (pci_gntb_padmisc2pad_	        ),
				.pci_rst_frompadmisc_	    (pci_rst_padmisc2pad_	        ),
				.pci_frame_out2padmisc_		(pci_frame_pad2padmisc_		    ),
				.pci_frame_oe_frompadmisc_	(pci_frame_oe_padmisc2pad_	    ),
				.pci_frame_frompadmisc_		(pci_frame_padmisc2pad_		    ),
				.pci_irdy_out2padmisc_		(pci_irdy_pad2padmisc_		    ),
				.pci_irdy_oe_frompadmisc_	(pci_irdy_oe_padmisc2pad_	    ),
				.pci_irdy_frompadmisc_		(pci_irdy_padmisc2pad_		    ),
				.pci_trdy_out2padmisc_		(pci_trdy_pad2padmisc_		    ),
				.pci_trdy_oe_frompadmisc_	(pci_trdy_oe_padmisc2pad_	    ),
				.pci_trdy_frompadmisc_		(pci_trdy_padmisc2pad_		    ),
				.pci_stop_out2padmisc_		(pci_stop_pad2padmisc_		    ),
				.pci_stop_oe_frompadmisc_	(pci_stop_oe_padmisc2pad_	    ),
				.pci_stop_frompadmisc_		(pci_stop_padmisc2pad_		    ),
				.pci_devsel_out2padmisc_	(pci_devsel_pad2padmisc_		),
				.pci_devsel_oe_frompadmisc_	(pci_devsel_oe_padmisc2pad_	    ),
				.pci_devsel_frompadmisc_	(pci_devsel_padmisc2pad_		),
				.pci_ad_out2padmisc			(pci_ad_pad2padmisc			    ),
				.pci_ad_oe_frompadmisc_		(pci_ad_oe_padmisc2pad_		    ),
				.pci_ad_frompadmisc			(pci_ad_padmisc2pad			    ),
				.pci_par_out2padmisc		(pci_par_pad2padmisc			),
				.pci_par_oe_frompadmisc_	(pci_par_oe_padmisc2pad_		),
				.pci_par_frompadmisc		(pci_par_padmisc2pad			),
				.pci_cbe_out2padmisc_		(pci_cbe_pad2padmisc_		    ),
				.pci_cbe_oe_frompadmisc_	(pci_cbe_oe_padmisc2pad_		),
				.pci_cbe_frompadmisc_	    (pci_cbe_padmisc2pad_	        ),
				.pci_req_out2padmisc_		(pci_req_pad2padmisc_		    ),
				.pci_inta_out2padmisc_		(pci_inta_pad2padmisc_		    ),
    //serr and perr
                .pci_perr_out2padmisc_		(pci_perr_pad2padmisc_          ),
		        .pci_perr_oe_frompadmisc_	(pci_perr_oe_padmisc2pad_       ),
		        .pci_perr_frompadmisc_		(pci_perr_padmisc2pad_          ),
		        .pci_serr_out2padmisc_		(pci_serr_pad2padmisc_          ),
		        .pci_serr_oe_frompadmisc_	(pci_serr_oe_padmisc2pad_       ),
		        .pci_serr_frompadmisc_		(pci_serr_padmisc2pad_          ),
//---------------IDE
				.atadiow_frompadmisc_     	(atadiow_padmisc2pad_     	  ),
				.atadior_frompadmisc_     	(atadior_padmisc2pad_         ),
				.atacs0_frompadmisc_      	(atacs0_padmisc2pad_          ),
				.atacs1_frompadmisc_      	(atacs1_padmisc2pad_          ),
				.atadd_frompadmisc        	(atadd_padmisc2pad            ),
				.atadd_oe_frompadmisc_    	(atadd_oe_padmisc2pad_        ),
				.atada_frompadmisc        	(atada_padmisc2pad            ),
				.atareset_frompadmisc_    	(atareset_padmisc2pad_        ),
				.atadmack_frompadmisc_    	(atadmack_padmisc2pad_        ),
				.ataiordy_out2padmisc     	(ataiordy_pad2padmisc         ),
				.ataintrq_out2padmisc     	(ataintrq_pad2padmisc         ),
				.atadd_out2padmisc        	(atadd_pad2padmisc            ),
				.atadmarq_out2padmisc    	(atadmarq_pad2padmisc         ),

//--------------GI Interface (from broadon)
				//system (6)
				.gc_vid_clk_frompadmisc		(gc_vid_clk_padmisc2pad			),
				.gc_sys_clk_frompadmisc0	(gc_sys_clk_padmisc2pad0		),
				.gc_sys_clk_frompadmisc1	(gc_sys_clk_padmisc2pad1		),
				.alt_boot_out2padmisc		(alt_boot_pad2padmisc			),
				.test_ena_out2padmisc		(test_ena_pad2padmisc	    	),
				.gc_rst_frompadmisc			(gc_rst_padmisc2pad	    		),
				//serial eeproom interface
				.i2c_clk_frompadmisc		(i2c_clk_padmisc2pad	    	),
				.i2c_din_out2padmisc		(i2c_din_pad2padmisc	    	),
				.i2c_dout_frompadmisc		(i2c_dout_padmisc2pad	    	),
				.i2c_doe_frompadmisc		(i2c_doe_padmisc2pad	    	),
				//disc interface
				.di_in_out2padmisc			(di_in_pad2padmisc		    	),
				.di_out_frompadmisc			(di_out_padmisc2pad	    		),
				.di_oe_frompadmisc			(di_oe_padmisc2pad		    	),
				.di_brk_in_out2padmisc		(di_brk_in_pad2padmisc	    	),
				.di_brk_out_frompadmisc		(di_brk_out_padmisc2pad 		),
				.di_brk_oe_frompadmisc		(di_brk_oe_padmisc2pad	    	),
				.di_dir_out2padmisc			(di_dir_pad2padmisc	    		),
				.di_hstrb_out2padmisc		(di_hstrb_pad2padmisc	    	),
				.di_dstrb_frompadmisc		(di_dstrb_padmisc2pad	    	),
				.di_err_frompadmisc			(di_err_padmisc2pad	    		),
				.di_cover_frompadmisc		(di_cover_padmisc2pad	    	),
				.di_stb_out2padmisc			(di_stb_pad2padmisc				),

				.ais_clk_out2padmisc		(ais_clk_pad2padmisc			),
				.ais_lr_out2padmisc			(ais_lr_pad2padmisc	    		),
				.ais_d_frompadmisc			(ais_d_padmisc2pad		    	),
//------GPIO
                .gpio_frompadmisc       	(gpio_padmisc2pad           ),
                .gpio_oe_frompadmisc_   	(gpio_oe_padmisc2pad_       ),
                .gpio_out2padmisc       	(gpio_pad2padmisc           ),
//------AUDIO
//				.i2si_data_out2padmisc    	(i2si_data_pad2padmisc   	),
				.i2so_data0_out2padmisc    	(i2so_data0_pad2padmisc  	),
				.i2so_data1_out2padmisc    	(i2so_data1_pad2padmisc  	),
				.i2so_data2_out2padmisc    	(i2so_data2_pad2padmisc  	),
				.i2so_data3_out2padmisc    	(i2so_data3_pad2padmisc  	),
				.i2so_mclk_out2padmisc      (i2so_mclk_pad2padmisc   	),
				.i2so_bck_out2padmisc		(i2so_bck_pad2padmisc		),
				.i2so_lrck_out2padmisc		(i2so_lrck_pad2padmisc		),

				.i2si_data_out2padmisc		(i2si_data_pad2padmisc			),
				.i2si_mclk_frompadmisc		(i2si_mclk_padmisc2pad			),
				.i2si_bck_frompadmisc		(i2si_bck_padmisc2pad			),
				.i2si_lrck_frompadmisc		(i2si_lrck_pad2padmisc			),

//        		.i2si_data_frompadmisc		(i2si_data_padmisc2pad		),
//				.i2si_data_oe_frompadmisc_	(i2si_data_oe_padmisc2pad_	),
				.i2so_data0_frompadmisc     (i2so_data0_padmisc2pad		),
				.i2so_data1_frompadmisc     (i2so_data1_padmisc2pad		),
				.i2so_data2_frompadmisc     (i2so_data2_padmisc2pad		),
				.i2so_data3_frompadmisc     (i2so_data3_padmisc2pad		),
				.i2so_data0_oe_frompadmisc_	(i2so_data0_oe_padmisc2pad_	),
				.i2so_data1_oe_frompadmisc_	(i2so_data1_oe_padmisc2pad_	),
				.i2so_data2_oe_frompadmisc_	(i2so_data2_oe_padmisc2pad_	),
				.i2so_data3_oe_frompadmisc_	(i2so_data3_oe_padmisc2pad_	),
				.i2so_mclk_frompadmisc		(i2so_mclk_padmisc2pad		),
				.i2so_mclk_oe_frompadmisc_	(i2so_mclk_oe_padmisc2pad_	),
				.i2so_lrck_frompadmisc      (i2so_lrck_padmisc2pad		),
				.i2so_lrck_oe_frompadmisc_	(i2so_lrck_oe_padmisc2pad_	),
				.i2so_bck_frompadmisc       (i2so_bck_padmisc2pad		),
				.i2so_bck_oe_frompadmisc_	(i2so_bck_oe_padmisc2pad_	),
//--------------EJTAG
				.j_tdi_frompadmisc			(j_tdi_padmisc2pad			),
				.j_tdo_frompadmisc			(j_tdo_padmisc2pad			),
				.j_tms_frompadmisc			(j_tms_padmisc2pad			),
				.j_rst_frompadmisc_			(j_trst_padmisc2pad_		),
				.j_tclk_frompadmisc			(j_tclk_padmisc2pad			),

                .j_tdi_out2padmisc      	(j_tdi_pad2padmisc          ),
                .j_tms_out2padmisc      	(j_tms_pad2padmisc          ),
                .j_tclk_out2padmisc        	(j_tclk_pad2padmisc      	),
                .j_rst_out2padmisc_        	(j_trst_pad2padmisc_        ),
                .j_tdo_out2padmisc      	(j_tdo_pad2padmisc          ),

				.j_tdi_oe_frompadmisc_		(j_tdi_oe_padmisc2pad_		),
				.j_tdo_oe_frompadmisc_		(j_tdo_oe_padmisc2pad_		),
				.j_tms_oe_frompadmisc_		(j_tms_oe_padmisc2pad_		),
				.j_rst_oe_frompadmisc_		(j_trst_oe_padmisc2pad_		),
				.j_tclk_oe_frompadmisc_		(j_tclk_oe_padmisc2pad_		),

//---------------spdif
                .spdif_frompadmisc      	(spdif_padmisc2pad			),
                .spdif_oe_frompadmisc_  	(spdif_oe_padmisc2pad_  	),
				.spdif_out2padmisc			(spdif_pad2padmisc		),
				.spdif_in_data_out2padmisc 	(spdif_in_data_pad2padmisc	),
//---------------usb
                .p1_ls_mode_fromcore    	(p1_ls_mode_core2padmisc    ),
                .p1_txd_fromcore        	(p1_txd_core2padmisc        ),
                .p1_txd_oej_fromcore    	(p1_txd_oej_core2padmisc    ),
                .p1_txd_se0_fromcore    	(p1_txd_se0_core2padmisc    ),
                .p2_ls_mode_fromcore    	(p2_ls_mode_core2padmisc    ),
                .p2_txd_fromcore        	(p2_txd_core2padmisc        ),
                .p2_txd_oej_fromcore    	(p2_txd_oej_core2padmisc    ),
                .p2_txd_se0_fromcore    	(p2_txd_se0_core2padmisc    ),
                .i_p1_rxd_out2core      	(i_p1_rxd_padmisc2core      ),
                .i_p1_rxd_se0_out2core  	(i_p1_rxd_se0_padmisc2core  ),
                .i_p2_rxd_out2core      	(i_p2_rxd_padmisc2core      ),
                .i_p2_rxd_se0_out2core  	(i_p2_rxd_se0_padmisc2core  ),
                .usbpon_frompadmisc     	(usbpon_padmisc2pad         ),
                .usbpon_oe_frompadmisc_		(usbpon_oe_padmisc2pad_		),
                .usbovc_out2padmisc     	(usbovc_pad2padmisc     ),
//--------------IR
				.irrx_out2padmisc			(irrx_pad2padmisc		),
				.irrx_frompadmisc			(irrx_padmisc2pad		),
				.irrx_oe_frompadmisc_		(irrx_oe_padmisc2pad_	),
//--------------UART
				.uart_tx_frompadmisc		(uart_tx_padmisc2pad	),
				.uart_tx_oe_frompadmisc_	(uart_tx_oe_padmisc2pad_),
				.uart_tx_out2padmisc		(uart_tx_pad2padmisc	),
				.uart_rx_out2padmisc		(uart_rx_pad2padmisc	),
//TV encoder
				//analog pins
				.tvenc_iout1_fromcore		(tvenc_iout1_core2pad	),
				.tvenc_iout2_fromcore		(tvenc_iout2_core2pad	),
				.tvenc_iout3_fromcore		(tvenc_iout3_core2pad	),
				.tvenc_iout4_fromcore		(tvenc_iout4_core2pad	),
				.tvenc_iout5_fromcore		(tvenc_iout5_core2pad	),
				.tvenc_iout6_fromcore		(tvenc_iout6_core2pad	),
				.tvenc_xiref1_fromcore		(tvenc_iref1_core2pad	),
				.tvenc_xidump_fromcore		(tvenc_idump_core2pad	),	// output from tv encoder
				.tvenc_xiext_fromcore		(tvenc_iext_core2pad	),	// output from tv encoder
				.tvenc_xiext1_fromcore		(tvenc_iext1_core2pad	),	// output from tv encoder
				//digital pins
				.tvenc_h_sync_frompadmisc_	(tvenc_h_sync_padmisc2pad_	),
				.tvenc_v_sync_frompadmisc_	(tvenc_v_sync_padmisc2pad_  ),
//---------------DIGITAL VIDEO OUT
			    .pix_data_frompadmisc  	    (vdata_padmisc2pad			),
				.de_h_sync_frompadmisc      (de_h_sync_padmisc2pad_	    ),
				.de_v_sync_frompadmisc      (de_v_sync_padmisc2pad_	    ),
				.pix_clk_frompadmisc        (pixel_clk_padmisc2pad		),
//---------------VIDEO IN
				.video_in_pixel_clk_out2padmisc	   (video_in_pixel_clk_pad2padmisc	),
				.video_in_pixel_data_out2padmisc   (video_in_pixel_data_pad2padmisc	),
				.video_in_hsync_out2padmisc_       (video_in_hsync_pad2padmisc_		),
				.video_in_vsync_out2padmisc_       (video_in_vsync_pad2padmisc_		),

//sd_ms interface
				.sd_clk_frompadmisc					(sd_mmc_clk_padmisc2pad	),					// SD card clock

				.sd_cmd_frompadmisc					(sd_mmc_cmd_out_padmisc2pad	),					// SD card command/status from core
				.sd_cmd_oe_frompadmisc_				(sd_mmc_cmd_oej_padmisc2pad	),				// SD card command/status oe# from core
				.sd_cmd_out2padmisc					(sd_mmc_cmd_in_pad2padmisc	),					// SD card command/status out 2 core

				.sd_data_frompadmisc				(sd_data_out_padmisc2pad	),					// SD card data from core
				.sd_data_oe_frompadmisc_			(sd_data_oej_padmisc2pad	),				// SD card data oe# from core
				.sd_data_out2padmisc				(sd_data_in_pad2padmisc		),					// SD card data out 2 core

				.sd_wr_prct_out2padmisc				(wr_prct_pin_pad2padmisc	),				// SD card locked
				.sd_det_out2padmisc					(det_pin_pad2padmisc		),					// SD card inserted

//				.ms_ip_en_frompadmisc				(ms_ip_enable_padmisc2pad	),					// MS IO enable
				.ms_clk_frompadmisc					(ms_clk_padmisc2pad			),					// MS clock
				.ms_pwr_ctrl_frompadmisc			(ms_pw_ctrl_padmisc2pad		),				// MS power control from core
				.ms_bs_frompadmisc					(ms_bsst_padmisc2pad		),						// MS bus status from core

				.ms_sdio_frompadmisc				(ms_serout_padmisc2pad		),					// MS sdio from core
				.ms_sdio_oe_frompadmisc_			(msde_padmisc2pad			),				// MS sdio oe# from core
				.ms_sdio_out2padmisc				(ms_serin_in_pad2padmisc	),					// MS sdio out 2 core

				.ms_ins_out2padmisc					(ms_ins_pad2padmisc			),					// MS card insert indicated signal

//ADC start
				.xmic1in_out2core		(xmic1in_pad2core		),
				.xadc_vref_fromcore     (xadc_vref_core2pad		),
				.xmic2in_out2core	    (xmic2in_pad2core	    ),
//ADC end

//	PAD Driever Capability
				.gpio_pad_driv_fromcore	   		(gpio_pad_driv_core2pad 	   		),	// gpio pad driver capability
				.rom_da_pad_driv_fromcore    	(rom_da_pad_driv_core2pad 	    	),  // rom data/address driver capability
				.sdram_data_pad_driv_fromcore	(sdram_data_pad_driv_core2pad		),  // sdram data driver capability
				.sdram_ca_pad_driv_fromcore		(sdram_ca_pad_driv_core2pad			),  // sdram command/address driver capability

        		// One Crystal Clock ouput
                .f27m_clk_out2pll               (f27m_clk               	),

                //clock
                .ext_mem_dclk_fromclkgen        (ext_mem_clk_gen        	),

                .test_mode_out2padmisc          (test_mode_frompad          ),
//        		.test_sel_out2padmisc			(test_sel_pad2padmisc		),
//        		.warm_rst_frompadmisc			(chipset_rst_padmisc2pad	),       //used to enable(OEN) the only output signals
  		        .bond_option_out2padmisc		(bond_option_pad2padmisc	),
        		.cold_rst_out2padrststrap_		(cold_rst_to_gi_				)
                );
//===========================End of invoke the pad==========================

endmodule
