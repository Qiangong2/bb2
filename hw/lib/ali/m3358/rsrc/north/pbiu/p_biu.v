/*******************************************************************
          (c) copyrights 1997-1998. All rights reserved

           T-Square Design, Inc.

          Company confidential and Properietary information.
          This information may not be disclosed to unauthorized
          individual.
*******************************************************************/
/*******************************************************************
 *
 *    DESCRIPTION: PCI device integeration, include pci host, usb, ide
 *				   and pci arbiter
 *    AUTHOR:      Runner
 *
 *    HISTORY:	06/14/02	edit for m6304  -- Runner
 *				06/15/02	sdram parameter is no more in pci host config
 *				10/09/02	Delete the IDE IP and corresponding signals/intf
 				2003-08-27	Remove the GATX20 for USB.
 				2003-08-28	Connect the second external PCI_MASTER request to
 							always active. Which is the default and lowest
 							request
 *******************************************************************/

module	p_biu(
// external pci bus	output
	OE_AD_		,
	OUT_AD		,
	OE_CBE_		,
	OUT_CBE_	,
	OE_PAR_		,
	OUT_PAR		,
	OE_FRAME_	,
	OUT_FRAME_	,
	OE_IRDY_	,
	OUT_IRDY_	,
	OE_DEVSEL_	,
	OUT_DEVSEL_	,
	OE_TRDY_	,
	OUT_TRDY_	,
	OE_STOP_	,
	OE_PERR_     ,
	OUT_PERR_   ,
	OE_SERR_     ,
	OUT_SERR_   ,
	OUT_STOP_	,
	OUT_GNT_	, // only provide one request on external pci bus in m6304 -- runner

// Input from External pci Bus
	IN_AD		,
	IN_CBE_		,
	IN_PAR		,
	IN_FRAME_	,
	IN_IRDY_	,
	IN_DEVSEL_	,
	IN_TRDY_	,
	IN_STOP_	,
	IN_PERR_    ,
	IN_SERR_    ,
	IN_REQ_		, // only provide one request on external pci bus in m6304 -- runner
    RETRY_ENABLE,
// North Bridge to pci host interface
	PBIU_REQ	,
	PBIU_GNT	,
	PBIU_RW		,
	PBIU_BL		,
	PBIU_BE		,
	PBIU_WR_RDY	,
	PBIU_RD_RDY	,
	PBIU_LAST	,
	PBIU_ERR	,
	PBIU_ADDR	,
	PBIU_WR_DATA,
	PBIU_RD_DATA,

	P_CFG_ADDR	,
	P_CFG_BE	,
	P_CFG_DIN	,
	P_CFG_DOUT	,
	P_CFG_RW	,
	P_CFG_CS	,
	P_CFG_GNT	,

// PCI to sdram controller read cycle interface
	PCI_RAM_RREQ	,
	PCI_RAM_RACK	,
	PCI_RAM_RBL		,
	PCI_RAM_RRDY	,
	PCI_RAM_RLAST	,
	PCI_RAM_RERR	,
	PCI_RAM_RADDR	,
	PCI_RAM_RDATA	,

// PCI Posted Write FIFO interface
	PCI_RAM_WREQ	,
	PCI_RAM_WACK	,
	PCI_RAM_WBL		,
	PCI_RAM_WBE		,
	PCI_RAM_WRDY	,
	PCI_RAM_WLAST	,
	PCI_RAM_WERR	,
	PCI_RAM_WADDR	,
	PCI_RAM_WDATA	,


	PCI_PAR_ERR	,


// USB interface
	P1_LS_MODE	,
	P1_TXD		,
	P1_TXD_OEJ	,
	P1_TXD_SE0	,
	P2_LS_MODE	,
	P2_TXD		,
	P2_TXD_OEJ	,
	P2_TXD_SE0	,
/*
	p3_ls_mode	,
	p3_txd		,
	p3_txd_oej	,
	p3_txd_se0	,
	p4_ls_mode	,
	p4_txd		,
	p4_txd_oej	,
	p4_txd_se0	,
*/
	I_P1_RXD	,
	I_P1_RXD_SE0,
	I_P2_RXD	,
	I_P2_RXD_SE0,
/*
	i_p3_rxd	,
	i_p3_rxd_se0,
	i_p4_rxd	,
	i_p4_rxd_se0,
*/
	USB_PON_	,	// POWER CONTROL
	USB_CLK		,	// 48m CLOCK
	USB_OVERCUR_,	// OVER CURRENT
//	USB_CLK_EN	,
	USB_INT_	,	// USB interrupt output

// IDE interface
	PCI_MODE	,
	PCI_MODE_IDE_EN	,
	PINS_STOP	,
	PINS_IDLE	,
/*
	ch2_detect		, // 40 lines or 80 line cable select
	atadiow_out_	,
	atadior_out_	,
	atacs0_out_		,
	atacs1_out_		,
	atadd_out		,
	atadd_oe_		,
	atada_out		,
//	atareset_out_	,
	atadmack_out_	,
	ataiordy_in		,
	ataintrq_in		,
	atadd_in		,
	atadmarq_in		,
	ide_int_		,
*/
//=================FOR MS/SD CARD PORT================


	//	EN_CLK2 			,
		MS_IP_ENABLE        ,
	//	MULTIFN_BIT         ,
		MS_INS              ,
		MS_SERIN_IN         ,
		M66EN               ,
	//	MS_EN_SLAVECLK      ,
		SD_IP_ENABLE        ,
		SD_DATA0_IN         ,
		SD_DATA1_IN         ,
		SD_DATA2_IN         ,
		SD_DATA3_IN         ,
		SD_MMC_CMD_IN       ,
	//	CLKRUNJ             ,
		TEST_H              ,
		MS_CLK              ,
		MS_BSST             ,
		MS_PW_CTRL          ,
		MS_SEROUT           ,
		MSDE                ,
		MS_ACCESS           ,
//		MS_TESTOUT          ,
		SD_DATA0_OEJ        ,
		SD_DATA1_OEJ        ,
		SD_DATA2_OEJ        ,
		SD_DATA3_OEJ        ,
		SD_MMC_CMD_OEJ      ,
		SD_DATA0_OUT        ,
		SD_DATA1_OUT        ,
		SD_DATA2_OUT        ,
		SD_DATA3_OUT        ,
		SD_MMC_CMD_OUT      ,
		SD_MMC_CLK          ,
//		SD_TEST_PIN         ,

//		VDD25M              ,
//		VSSCORE             ,

	//	MS_CLKRUNJ_OEJ      ,
		TEST_DONE           ,
		FAIL_H              ,
	//	SD_CLKRUNJ_OUT      ,
	//	SD_CLKRUNJ_OEJ      ,
		DET_PIN             ,
		WR_PRCT_PIN         ,
//		INT_SEL             ,
	//	CLK_32K             ,
//		MSSD_SROM_EN        ,
//		SROM_DATA           ,

		SD_FNUM_BIT         ,
		MS_FNUM_BIT         ,

	//	MS_PCI_ADOUT        ,
	//	SD_PCI_ADOUT        ,
	//	MS_PAROUT           ,
	//	SD_PAROUT           ,
	//	MS_TRDYOUTJ         ,
	//	SD_TRDYOUTJ         ,
	//	MS_STOPOUTJ         ,
	//	SD_STOPOUTJ         ,
	//	MS_DEVSELOUTJ       ,
	//	SD_DEVSELOUTJ       ,
	//	MS_SERROUTJ         ,
	//	MS_PERROUTJ         ,
	//	MS_PERR_OENBJ       ,
	//	MS_PCI_AD_OENBJ     ,
	//	SD_PCI_AD_OENBJ     ,
	//	MS_PAR_OENBJ        ,
	//	SD_PAR_OENBJ        ,
	//	MS_CTRL_OENBJ       ,
	//	SD_STOP_OENBJ       ,
	//	SD_TRDY_OENBJ       ,
	//	SD_DEVSEL_OENBJ     ,
		MS_INTAOUTJ         ,
		SD_INTAOUTJ         ,
		SD_LED              ,
		SWITCH_MS_SD        ,

		GI_CLK		,
		GI_CLK_LOCK ,
		PIN_RST	,
		RESET		,
		ALT_BOOT	,
		TEST_ENA	,
		GC_RST		,
		SEC_INTR	,
		SEC_ACK		,
		//SERIAL PROM
		I2C_CLK		,
		I2C_DIN		,
		I2C_DOUT	,
		I2C_DOE		,
		//PCI
		GI_PCI_INTR	,
		GI_PCI_RST	,
		//DISC INTERFACE
		DI_IN			,
		DI_OUT			,
		DI_OE			,
		DI_BRK_IN		,
		DI_BRK_OUT		,
		DI_BRK_OE		,
		DI_DIR			,
		DI_HSTRB		,
		DI_DSTRB	    ,
		DI_ERR		    ,
		DI_COVER	    ,
		DI_RESET	    ,
		AIS_CLK	        ,
		AIS_LR		    ,
		AIS_D		    ,


// system input
	TESTMD		,

	PCI_CLK		,		// pci cLOCK	--	33mhZ
//	IDE100M_CLK	,
//	IDE66M_CLK	,
	MEM_CLK		,		// cHIPSET cLOCK
	RST_
	);


//////////////////////////////////////////////////////////////////////
input 		GI_CLK;					// gi core clock, 3x pci_clk;
input 		GI_CLK_LOCK;				// clk is stable, pll locked;
input 		PIN_RST;				// pin reset;
output 		RESET;				// system reset;
input 		ALT_BOOT;				// alternative boot;
output 		TEST_ENA;			// enable test/debug logic;
output 		GC_RST;				// reset to flipper;
output 		SEC_INTR;			// cause secure exception;
input 		SEC_ACK;				// initial fetch for secure exception;
output 		I2C_CLK;				// i2c clock;
input 		I2C_DIN;				// i2c data from io cell;
output 		I2C_DOUT;			// i2c data to io cell;
output 		I2C_DOE;				// i2c data output enable;
//pci
input 		GI_PCI_RST;				// pci bus reset;
output		GI_PCI_INTR;
// disk interface;
input [7:0] DI_IN;			// DI data from input cell;
output[7:0] DI_OUT;		// DI data to output cell;
output 		DI_OE;				// DI output enable;
input 		DI_BRK_IN;			// DI break from input cell;
output 		DI_BRK_OUT;			// DI break to output cell;
output 		DI_BRK_OE;			// DI break output enable;
input 		DI_DIR;				// DI direction;
input 		DI_HSTRB;				// DI host strobe;
output 		DI_DSTRB;			// DI drive strobe;
output 		DI_ERR;				// DI error;
output		DI_COVER;			// DI cover;
input 		DI_RESET;				// DI reset;
// ais interface;
input 		AIS_CLK;				// AIS clock from GC;
input 		AIS_LR;				// AIS left/right signal;
output 		AIS_D;				// AIS data out;

// PCI ousput Signals
output			OE_AD_	;
output	[31:0]	OUT_AD	;
output			OE_CBE_	;
output	[3:0]	OUT_CBE_;
output			OE_PAR_,    OUT_PAR,
				OE_FRAME_,  OUT_FRAME_,
				OE_PERR_ ,   OUT_PERR_   ,
				OE_SERR_,    OUT_SERR_,
				OE_IRDY_, 	OUT_IRDY_,
				OE_DEVSEL_, OUT_DEVSEL_,
				OE_TRDY_,   OUT_TRDY_,
				OE_STOP_,   OUT_STOP_;

// external PCI bus	input
input  [31:0]	IN_AD		;
input  [ 3:0]	IN_CBE_		;
input			IN_PAR		,
				IN_FRAME_	,
				IN_IRDY_	,
				IN_DEVSEL_	,
				IN_TRDY_	,
				IN_PERR_    ,
				IN_SERR_    ,
				IN_STOP_	;

output	[1:0]	OUT_GNT_	;
input	[1:0]	IN_REQ_		;
input           RETRY_ENABLE;

// North Bridge to pci host interface
input			PBIU_REQ	;
output			PBIU_GNT	;
input			PBIU_RW		;
input	[1:0]	PBIU_BL		;
input	[3:0]	PBIU_BE		;
output			PBIU_WR_RDY	,
				PBIU_RD_RDY	,
				PBIU_LAST	,
				PBIU_ERR	;
input	[31:0]	PBIU_WR_DATA;
output	[31:0]	PBIU_RD_DATA;
input	[29:2]	PBIU_ADDR	;

input			P_CFG_CS	;
output			P_CFG_GNT	;
input			P_CFG_RW	;
input	[3:0]	P_CFG_BE	;
output	[31:0]	P_CFG_DIN	;
input	[31:0]	P_CFG_DOUT	;
input	[31:0]	P_CFG_ADDR	;

//*******************************************************************
	// PCI read cycle interface
output	 		PCI_RAM_RREQ	;
input			PCI_RAM_RACK	;
output	[1:0]	PCI_RAM_RBL		;
input			PCI_RAM_RRDY	,
				PCI_RAM_RLAST	,
				PCI_RAM_RERR	;
input	[31:0]	PCI_RAM_RDATA	;
output	[29:2]	PCI_RAM_RADDR	;

	// PCI Posted Write FIFO interface
output			PCI_RAM_WREQ	;
input			PCI_RAM_WACK	;
output	[1:0]	PCI_RAM_WBL		;
output	[3:0]	PCI_RAM_WBE		;
input			PCI_RAM_WRDY	,
				PCI_RAM_WERR	;
output			PCI_RAM_WLAST	;
output	[31:0]	PCI_RAM_WDATA	;
output	[29:2]	PCI_RAM_WADDR	;

//*******************************************************************

output			PCI_PAR_ERR;

// USB interface
output	P1_LS_MODE	,
		P1_TXD		,
		P1_TXD_OEJ	,
		P1_TXD_SE0	,
		P2_LS_MODE	,
		P2_TXD		,
		P2_TXD_OEJ	,
		P2_TXD_SE0	;
input	I_P1_RXD	,
		I_P1_RXD_SE0,
		I_P2_RXD	,
		I_P2_RXD_SE0;

/*
output	p3_ls_mode	,
		p3_txd		,
		p3_txd_oej	,
		p3_txd_se0	,
		p4_ls_mode	,
		p4_txd		,
		p4_txd_oej	,
		p4_txd_se0	;

input	i_p3_rxd	,
		i_p3_rxd_se0,
		i_p4_rxd	,
		i_p4_rxd_se0;
*/
input	USB_CLK		;
output	USB_PON_	;
input	USB_OVERCUR_;
//input	USB_CLK_EN	;	//Kandy
output	USB_INT_	;	// USB interrupt output

// IDE interface
input	PCI_MODE	;
input	PCI_MODE_IDE_EN	;
output	PINS_STOP	;
input	PINS_IDLE	;

/*
input	ch2_detect	;
output  atadiow_out_;
output  atadior_out_;
output  atacs0_out_;
output  atacs1_out_;
output	[15:0]	atadd_out;
output  [3:0]  	atadd_oe_ ;
output	[2:0]   atada_out;
//output  atareset_out_;
output  atadmack_out_;

input   ataiordy_in;
input   ataintrq_in;
input	[15:0]	atadd_in;
input   atadmarq_in;
output	ide_int_;
*/
//============================MS/CARD PORTS================================

//	output		EN_CLK2			;
	input		MS_IP_ENABLE    ;
//	output		MULTIFN_BIT     ;
	input		MS_INS          ;
	input		MS_SERIN_IN     ;
	input		M66EN           ;
//	output		MS_EN_SLAVECLK  ;
	input		SD_IP_ENABLE    ;
	input		SD_DATA0_IN     ;
	input		SD_DATA1_IN     ;
	input		SD_DATA2_IN     ;
	input		SD_DATA3_IN     ;
	input		SD_MMC_CMD_IN   ;
//	output		CLKRUNJ         ;
	input		TEST_H          ;
	output		MS_CLK          ;
	output	[1:0]	MS_BSST         ;
	output		MS_PW_CTRL      ;
	output		MS_SEROUT       ;
	output		MSDE            ;
	output		MS_ACCESS       ;
//	output	[3:0]	MS_TESTOUT      ;
	output		SD_DATA0_OEJ    ;
	output		SD_DATA1_OEJ    ;
	output		SD_DATA2_OEJ    ;
	output		SD_DATA3_OEJ    ;
	output		SD_MMC_CMD_OEJ  ;
	output		SD_DATA0_OUT    ;
	output		SD_DATA1_OUT    ;
	output		SD_DATA2_OUT    ;
	output		SD_DATA3_OUT    ;
	output		SD_MMC_CMD_OUT  ;
	output		SD_MMC_CLK      ;
//	output	[23:0]	SD_TEST_PIN     ;

//	input		VDD25M          ;
//	input		VSSCORE         ;

//	output		MS_CLKRUNJ_OEJ  ;
	output		TEST_DONE       ;
	output		FAIL_H          ;
//	output		SD_CLKRUNJ_OUT  ;
//	output		SD_CLKRUNJ_OEJ  ;
	input		DET_PIN         ;
	input		WR_PRCT_PIN     ;
//	input	[2:0]	INT_SEL         ;
//	output		CLK_32K         ;
//	input		MSSD_SROM_EN    ;
//	input	[31:0]	SROM_DATA       ;

	input	[2:0]	SD_FNUM_BIT     ;
	input	[2:0]	MS_FNUM_BIT     ;

//	output		MS_PCI_ADOUT    ;
//	output		SD_PCI_ADOUT    ;
//	output		MS_PAROUT       ;
//	output		SD_PAROUT       ;
//	output		MS_TRDYOUTJ     ;
//	output		SD_TRDYOUTJ     ;
//	output		MS_STOPOUTJ     ;
//	output		SD_STOPOUTJ     ;
//	output		MS_DEVSELOUTJ   ;
//	output		SD_DEVSELOUTJ   ;
//	output		MS_SERROUTJ     ;
//	output		MS_PERROUTJ     ;
//	output		MS_PERR_OENBJ   ;
//	output		MS_PCI_AD_OENBJ ;
//	output		SD_PCI_AD_OENBJ ;
//	output		MS_PAR_OENBJ    ;
//	output		SD_PAR_OENBJ    ;
//	output		MS_CTRL_OENBJ   ;
//	output		SD_STOP_OENBJ   ;
//	output		SD_TRDY_OENBJ   ;
//	output		SD_DEVSEL_OENBJ ;
	output		MS_INTAOUTJ     ;
	output		SD_INTAOUTJ     ;
	output		SD_LED          ;
	output		SWITCH_MS_SD    ;
//===========================FINISH MS/CARD PORTS=================

// system input
input		PCI_CLK,
			MEM_CLK,
//			ide100m_clk,
//			ide66m_clk,
			TESTMD,
			RST_;

wire			oe_ad_	;
wire	[31:0]	out_ad	;
wire			oe_cbe_	;
wire	[3:0]	out_cbe_;
wire			oe_par_,    out_par,
				oe_frame_,  out_frame_,
				oe_irdy_, 	out_irdy_,
				oe_devsel_, out_devsel_,
				oe_trdy_,   out_trdy_,
				oe_perr_,    out_perr_,
				oe_serr_,    out_serr_,
				oe_stop_,   out_stop_;
wire            oe_perr, oe_serr   ;

assign		OE_AD_		=	oe_ad_		;
assign		OUT_AD	    =	out_ad	    ;
assign		OE_CBE_	    =	oe_cbe_	    ;
assign		OUT_CBE_    =	out_cbe_    ;
assign		OE_PAR_     =	oe_par_     ;
assign		OE_FRAME_	=	oe_frame_	;
assign		OE_IRDY_	=	oe_irdy_	;
assign		OE_DEVSEL_	=	oe_devsel_	;
assign		OE_TRDY_	=	oe_trdy_	;
assign		OE_STOP_	=	oe_stop_	;
assign		OUT_PAR		=	out_par		;
assign		OUT_FRAME_	=	out_frame_	;
assign		OUT_IRDY_	=	out_irdy_	;
assign		OUT_DEVSEL_ =	out_devsel_ ;
assign		OUT_TRDY_	=	out_trdy_	;
assign		OUT_STOP_	=	out_stop_	;
assign      OUT_PERR_   =   out_perr_   ;
assign      OUT_SERR_   =   out_serr_   ;
assign      OE_PERR_     =   oe_perr_   ;
assign      OE_SERR_     =   oe_serr_   ;
assign      oe_perr_    = ~ oe_perr     ;
assign      oe_serr_    = ~ oe_serr     ;
// external PCI bus	input
wire	[1:0]	out_gnt_	;
assign    		OUT_GNT_	=	out_gnt_	;

wire  [31:0]	in_ad		=	IN_AD		;
wire  [ 3:0]	in_cbe_		=	IN_CBE_		;
wire			in_par		=	IN_PAR		;
wire			in_frame_	=	IN_FRAME_	;
wire			in_irdy_	=	IN_IRDY_	;
wire			in_devsel_	=	IN_DEVSEL_	;
wire			in_trdy_	=	IN_TRDY_	;
wire			in_stop_	=	IN_STOP_	;
wire	[1:0]	in_req_		=	IN_REQ_		;
wire            in_perr_    =   IN_PERR_    ;
wire            in_serr_    =   IN_SERR_    ;
//wire            retry_enable = RETRY_ENABLE ;
wire            retry_enable = 1'b0 ;
// North Bridge to pci host interface
wire			pbiu_req	=	PBIU_REQ	;
wire	[29:2]	pbiu_addr	=	PBIU_ADDR	;
wire			pbiu_rw		=	PBIU_RW		;
wire	[1:0]	pbiu_bl		=	PBIU_BL		;
wire	[3:0]	pbiu_be		=	PBIU_BE		;
wire	[31:0]	pbiu_wr_data=	PBIU_WR_DATA;

wire			pbiu_wr_rdy	;
wire			pbiu_rd_rdy	;
wire			pbiu_last	;
wire			pbiu_err	;
wire	[31:0]	pbiu_rd_data;
wire			pbiu_gnt	;

assign			PBIU_WR_RDY		=	pbiu_wr_rdy		;
assign			PBIU_RD_RDY	    =	pbiu_rd_rdy	    ;
assign			PBIU_LAST	    =	pbiu_last	    ;
assign			PBIU_ERR	    =	pbiu_err	    ;
assign			PBIU_RD_DATA    =	pbiu_rd_data    ;
assign			PBIU_GNT	    =	pbiu_gnt	    ;

wire			p_cfg_cs	=P_CFG_CS	;
wire			p_cfg_rw	=P_CFG_RW	;
wire	[3:0]	p_cfg_be	=P_CFG_BE	;
wire	[31:0]	p_cfg_dout	=P_CFG_DOUT	;
wire	[31:0]	p_cfg_addr	=P_CFG_ADDR	;

wire	[31:0]	p_cfg_din	;
wire			p_cfg_gnt	;
assign			P_CFG_DIN	=	p_cfg_din	;
assign			P_CFG_GNT   =	p_cfg_gnt   ;
	// PCI read cycle interface

wire	      	pci_ram_rack	=PCI_RAM_RACK	;
wire	      	pci_ram_rrdy	=PCI_RAM_RRDY	;
wire       		pci_ram_rlast	=PCI_RAM_RLAST	;
wire       		pci_ram_rerr	=PCI_RAM_RERR	;
wire	[31:0]	pci_ram_rdata	=PCI_RAM_RDATA	;

wire         [29:2]	pci_ram_raddr	;
wire         [1:0]	pci_ram_rbl		;
wire          		pci_ram_rreq	;
assign		   	PCI_RAM_RADDR		=	pci_ram_raddr		;
assign		    PCI_RAM_RBL		    =	pci_ram_rbl		    ;
assign		    PCI_RAM_RREQ	    =	pci_ram_rreq	    ;
	// PCI Posted Write FIFO interface
wire			pci_ram_wreq	;
wire			pci_ram_wlast	;
wire	[31:0]	pci_ram_wdata	;
wire	[29:2]	pci_ram_waddr	;
wire	[1:0]	pci_ram_wbl		;
wire	[3:0]	pci_ram_wbe		;
assign			PCI_RAM_WREQ	=	pci_ram_wreq	;
assign			PCI_RAM_WLAST	=	pci_ram_wlast	;
assign			PCI_RAM_WDATA	=	pci_ram_wdata	;
assign			PCI_RAM_WADDR	=	pci_ram_waddr	;
assign			PCI_RAM_WBL		=	pci_ram_wbl		;
assign			PCI_RAM_WBE		=	pci_ram_wbe		;

wire			pci_ram_wrdy=PCI_RAM_WRDY	;
wire			pci_ram_werr=PCI_RAM_WERR	;
wire			pci_ram_wack=PCI_RAM_WACK	;
// USB interface
wire	p1_ls_mode	,
		p1_txd		,
		p1_txd_oej	,
		p1_txd_se0	,
		p2_ls_mode	,
		p2_txd		,
		p2_txd_oej	,
		p2_txd_se0	;
assign	P1_LS_MODE	=	p1_ls_mode	;
assign	P1_TXD		=	p1_txd		;
assign	P1_TXD_OEJ	=	p1_txd_oej	;
assign	P1_TXD_SE0	=	p1_txd_se0	;
assign	P2_LS_MODE	=	p2_ls_mode	;
assign	P2_TXD		=	p2_txd		;
assign	P2_TXD_OEJ	=	p2_txd_oej	;
assign	P2_TXD_SE0	=	p2_txd_se0	;

wire	i_p1_rxd	=I_P1_RXD		;
wire	i_p1_rxd_se0=I_P1_RXD_SE0	;
wire	i_p2_rxd	=I_P2_RXD		;
wire	i_p2_rxd_se0=I_P2_RXD_SE0	;

wire	usb_clk		=USB_CLK		;
wire	usb_overcur_=USB_OVERCUR_	;
//wire	usb_clk_en	=USB_CLK_EN		;	//Kandy

wire	usb_int_	;	// USB interrupt output
wire	usb_pon_	;
assign	USB_INT_	=usb_int_	;
assign	USB_PON_    =usb_pon_   ;
// system input
wire	pci_clk	=	PCI_CLK	;
wire	mem_clk	=	MEM_CLK	;
wire	testmd	=	TESTMD	;
wire	rst_	=	RST_	;


assign PCI_PAR_ERR = 1'b0;
parameter	udly = 1;

// Norman 2003-08-28
// assign out_gnt_[1] = 1'b1;	// runner 020816
// ======= module inter-connect wire
wire	[4:0]	curr_owner;

wire	host_oe_ad		,
		host_oe_cbe	    ,
		host_oe_par	    ,
		host_oe_frame	,
		host_oe_irdy	,
		host_oe_devsel  ,
		host_oe_trdy	,
		host_oe_stop	;

wire	[31:0]	host_out_ad		;
wire	[ 3:0]	host_out_cbe_	;
wire	host_out_par	,
		host_out_frame  ,
		host_out_irdy_  ,
		host_out_devse  ,
		host_out_trdy_  ,
		host_out_stop_  ,
		host_req_		;

// usb interface wire
wire			usb_req_		;
wire	[31:0]	usb_out_ad		;
wire	[3:0]	usb_out_cbe_	;
wire			usb_out_par		,
				usb_out_frame_	,
				usb_out_irdy_	,
				usb_out_devsel_	,
				usb_out_trdy_	,
				usb_out_stop_	,
				usb_out_perr_	,
				usb_out_serr_	;

wire	usb_oe_ad		,
		usb_oe_cbe		,
		usb_oe_par		,
		usb_oe_frame	,
		usb_oe_irdy		,
		usb_tdsoe		;
/*
wire	atadiow_out_	;
wire	atadior_out_	;
wire	atacs0_out_		;
wire	atacs1_out_		;
wire	[15:0]	atadd_out		;

wire	[3:0]	atadd_oe		;
wire	[3:0]	atadd_oe_		;

wire	[2:0]	atada_out		;
//assign	atareset_out_	= 1'b1;
wire	atadmack_out_	;

//wire	ide_int_ 	;
*/
// wire	ide_req_	;
/*
wire	[31:0]	ide_out_ad		;
wire	[3:0]	ide_out_cbe_ 	;
wire	ide_out_par 	;
wire	ide_out_frame_ 	;
wire	ide_out_irdy_ 	;
wire	ide_out_devsel_ ;
wire	ide_out_trdy_ 	;
wire	ide_out_perr_ 	;
wire	ide_out_serr_ 	;
wire	ide_out_stop_ 	;

wire	ide_oe_ad 		;
wire	ide_oe_cbe 		;
wire	ide_oe_frame 	;
wire	ide_oe_par 		;
wire	ide_oe_irdy 	;

//wire	ide_oe_devsel	;
//wire	ide_oe_trdy		;
//wire	ide_oe_stop		;
wire	ide_oe_dts		;
*/
// ====================== spg cell ================================
SEQ_DUMMYJ	PBIU_SEQ_DUMMYJ_0(
	.SDMY_OUT	(		),
	.DMY_IN		(1'b0	),
	.CLK		(mem_clk),
	.RSTJ		(rst_	)
	);
COMB_DUMMY	PBIU_COMB_DUMMY_0(
	.DMY_IN		(1'b0	),
	.DMY_OUT	(		)
	);
SEQ_DUMMYJ	PBIU_SEQ_DUMMYJ_1(
	.SDMY_OUT	(		),
	.DMY_IN		(1'b0	),
	.CLK		(mem_clk),
	.RSTJ		(rst_	)
	);
COMB_DUMMY	PBIU_COMB_DUMMY_1(
	.DMY_IN		(1'b0	),
	.DMY_OUT	(		)
	);

SEQ_DUMMYJ	PBIU_SEQ_DUMMYJ_2(
	.SDMY_OUT	(		),
	.DMY_IN		(1'b0	),
	.CLK		(pci_clk),
	.RSTJ		(rst_	)
	);
COMB_DUMMY	PBIU_COMB_DUMMY_2(
	.DMY_IN		(1'b0	),
	.DMY_OUT	(		)
	);


// ========= PCI Bus arbiter ===============
// assign	ide_req_	= 1'b1;	//no IDE now
wire		PCI_IDE_STOP	;
wire		IDE_PCI_IDLE    ;

p_arbiter p_arbiter (
	.curr_owner	( curr_owner	),
	.p_req0_	( gi_pci_req	),	// PCI request	-- PCI host master request
	.p_req1_	( host_req_		),	// PCI request	-- USB Device request
	.p_req2_	( usb_req_		),	// PCI request	-- External PCI master request
	.p_req3_	( in_req_[0]	),	// PCI request 	-- The lowest request always connect to active
	.p_req4_	( in_req_[1]	),

	.p_gnt0_	( gi_pci_gnt	),	// PCI grant	-- PCI host master grant
	.p_gnt1_	( host_gnt_		),	// PCI grant	-- USB grant
	.p_gnt2_	( usb_gnt_		),	// PCI grant	-- External PCI master request
	.p_gnt3_	( out_gnt_[0]	),	// PCI grant	-- The lowest request grant
	.p_gnt4_	( out_gnt_[1]	),

	.pci_mode	(PCI_MODE		),	//PCI_MODE
	.ata_pins_en(PCI_MODE_IDE_EN),	//ATA_PINS_EN
	.pins_stop	(PCI_IDE_STOP	),
	.pins_idle	(IDE_PCI_IDLE	),

	.p_frame_	( in_frame_	),
	.p_irdy_	( in_irdy_	),
	.pci_clk	( pci_clk	),
	.rst_		( rst_		)

		);

p_host	p_host(
// ===== external pci bus output
	.oe_ad		( host_oe_ad		),
	.oe_cbe		( host_oe_cbe		),
	.oe_par		( host_oe_par		),
	.oe_frame	( host_oe_frame		),
	.oe_irdy	( host_oe_irdy		),
	.oe_devsel	( host_oe_devsel	),
	.oe_trdy	( host_oe_trdy		),
	.oe_stop	( host_oe_stop		),
	.oe_perr    ( oe_perr           ),
	.oe_serr    ( oe_serr           ),

	.out_ad		( host_out_ad		),
	.out_cbe_	( host_out_cbe_		),
	.out_par	( host_out_par	    ),
	.out_frame_	( host_out_frame_	),
	.out_irdy_	( host_out_irdy_	),
	.out_devsel_( host_out_devsel_ 	),
	.out_trdy_	( host_out_trdy_	),
	.out_stop_	( host_out_stop_	),
	.out_req_	( host_req_		    ),
	.out_serr_  ( out_serr_         ),
	.out_perr_  ( out_perr_         ),

// ==== Input from External pci Bus
	.in_ad		( in_ad			),
	.in_cbe_	( in_cbe_	    ),
	.in_frame_	( in_frame_		),
	.in_irdy_	( in_irdy_		),
	.in_devsel_	( in_devsel_	),
	.in_trdy_	( in_trdy_		),
	.in_stop_	( in_stop_		),
	.in_par		( in_par		),
	.in_gnt_	( host_gnt_	    ),
	.in_perr_   ( in_perr_      ),
	.in_serr_   ( in_serr_      ),
	.retry_enable (retry_enable ),

// ===== North Bridge to PCI host interface
////// ----- PCI IO and memory access path
	.pbiu_req		( pbiu_req		),
	.pbiu_gnt		( pbiu_gnt		),
	.pbiu_rw		( pbiu_rw		),
	.pbiu_bl		( pbiu_bl		),
	.pbiu_be		( pbiu_be		),
	.pbiu_wr_rdy	( pbiu_wr_rdy	),
	.pbiu_rd_rdy	( pbiu_rd_rdy	),
	.pbiu_last		( pbiu_last		),
	.pbiu_err		( pbiu_err		),
	.pbiu_addr		( pbiu_addr		),
	.pbiu_wr_data	( pbiu_wr_data	),
	.pbiu_rd_data	( pbiu_rd_data	),

///// ----- PCI configuration access path
	.p_cfg_cs		( p_cfg_cs		),
	.p_cfg_gnt		( p_cfg_gnt		),
	.p_cfg_rw		( p_cfg_rw		),
	.p_cfg_be		( p_cfg_be		),
	.p_cfg_din		( p_cfg_din		),
	.p_cfg_dout		( p_cfg_dout	),
	.p_cfg_addr		( p_cfg_addr	),

// ===== PCI to sdram controller read cycle interface
	.pci_ram_rreq	( pci_ram_rreq	),
	.pci_ram_rack	( pci_ram_rack	),
	.pci_ram_rbl	( pci_ram_rbl	),
	.pci_ram_rrdy	( pci_ram_rrdy	),
	.pci_ram_rlast	( pci_ram_rlast	),
	.pci_ram_rerr	( pci_ram_rerr	),
	.pci_ram_raddr	( pci_ram_raddr	),
	.pci_ram_rdata	( pci_ram_rdata	),

// ===== PCI Posted Write FIFO interface
	.pci_ram_wreq	( pci_ram_wreq	),
	.pci_ram_wack	( pci_ram_wack	),
	.pci_ram_wbl	( pci_ram_wbl	),
	.pci_ram_wbe	( pci_ram_wbe	),
	.pci_ram_wrdy	( pci_ram_wrdy	),
	.pci_ram_wlast	( pci_ram_wlast	),
	.pci_ram_werr	( pci_ram_werr	),
	.pci_ram_waddr	( pci_ram_waddr	),
	.pci_ram_wdata	( pci_ram_wdata	),


// =====  system signal
	.pci_clk	( pci_clk	),		// PCI Clock	--	33MHz
	.mem_clk	( mem_clk	),		// Chipset Clock
	.TESTMD		(TESTMD		),
	.rst_		( rst_		)
	);
//wire	PINS_STOP_TMP	;
ctrl_sync	ctrl_sync(
	.PCI_IDE_STOP		(PCI_IDE_STOP	),
	.IDE_PCI_IDLE	    (IDE_PCI_IDLE	),
	.SHARE_PINS_STOP	(PINS_STOP		),
	.SHARE_PINS_IDLE	(PINS_IDLE		),
	.PCI_CLK			(pci_clk		),
	.MEM_CLK			(mem_clk		),
	.RSTJ               (rst_			)
	);
////reg	[31:0]	STOP_CNT		;
////always 	@(posedge mem_clk or negedge rst_)
////	if (!rst_)begin
////		STOP_CNT	<=	#1	32'b0				;
////		end
////	else if(PINS_IDLE)	begin
////		STOP_CNT	<=	#1	STOP_CNT +	1'b1	;
////		end
////	else	begin
////		STOP_CNT	<=	#1	32'b0				;
////		end
////
////reg    PINS_STOP_TMP_DLY1	;
////
////
////reg    PINS_STOP_DLY1	;
////reg    PINS_STOP_DLY2	;
////always 	@(posedge mem_clk or negedge rst_)
////	if (!rst_)
////		PINS_STOP_TMP_DLY1	<=	#1	1'b0	;
////	else if(STOP_CNT	==	32'h100)
////		PINS_STOP_TMP_DLY1	<=	#1	1'b0	;
////	else
////		PINS_STOP_TMP_DLY1	<=	#1	1'b1	;
////assign	PINS_STOP	=	PINS_STOP_TMP_DLY1;
////
////always 	@(posedge mem_clk or negedge rst_)
////	if (!rst_)begin
////		PINS_STOP_DLY1	<=	#1	1'b0	;
////		PINS_STOP_DLY2	<=	#1	1'b0	;
////		end
////	else	begin
////		PINS_STOP_DLY1	<=	#1	PINS_STOP	;
////		PINS_STOP_DLY2	<=	#1	PINS_STOP_DLY1	;
////	end
////wire stop_one_cycle =	PINS_STOP&!PINS_STOP_DLY1&PINS_STOP_DLY2;
////always	@(posedge	stop_one_cycle)
////	$display ("**Found :IDE_STOP falling one cycle at time %d ns",$time );
//for debug  produce one cycle falling stop

// ================== USB core instance =====================
`ifdef	INC_USB

//wire	usb_pon_;			// power control
//wire	usb_overcur_;
wire	usb_gate_clk;
wire	i_p1_ovrcurj, i_p2_ovrcurj ;	// over current
//wire	i_p3_ovrcurj, i_p4_ovrcurj, ;	// over current

wire	p3_ls_mode 	= 1'b0,
		p3_txd 		= 1'b0,
		p3_txd_oej 	= 1'b1,
		p3_txd_se0 	= 1'b0,
		p4_ls_mode 	= 1'b0,
		p4_txd 		= 1'b0,
		p4_txd_oej 	= 1'b1,
		p4_txd_se0 	= 1'b0;

assign	usb_gate_clk	= usb_clk;

// instance USB cocr
hydra_core HYDRA_CORE (

// UBS interface
	// output
	.PWREN1J	( pwren1j		),
	.PWREN2J	( pwren2j		),
//	.PWREN3J	( pwren3j		),
//	.PWREN4J	( pwren4j		),

	.P1_LS_MODE	( p1_ls_mode	),
	.P1_TXD		( p1_txd		),
	.P1_TXD_OEJ	( p1_txd_oej	),
	.P1_TXD_SE0	( p1_txd_se0	),

	.P2_LS_MODE	( p2_ls_mode	),
	.P2_TXD		( p2_txd		),
	.P2_TXD_OEJ	( p2_txd_oej	),
	.P2_TXD_SE0	( p2_txd_se0	),

//	.P3_LS_MODE	( p3_ls_mode	),
//	.P3_TXD		( p3_txd		),
//	.P3_TXD_OEJ	( p3_txd_oej	),
//	.P3_TXD_SE0	( p3_txd_se0	),

//	.P4_LS_MODE	( p4_ls_mode	),
//	.P4_TXD		( p4_txd		),
//	.P4_TXD_OEJ	( p4_txd_oej	),
//	.P4_TXD_SE0	( p4_txd_se0	),
// input
	.I_P1_OVRCURJ	( i_p1_ovrcurj	),
	.I_P2_OVRCURJ	( i_p2_ovrcurj	),
//	.I_P3_OVRCURJ	( i_p3_ovrcurj	),
//	.I_P4_OVRCURJ	( i_p4_ovrcurj	),
	.I_P1_RXD		( i_p1_rxd		),
	.I_P1_RXD_SE0	( i_p1_rxd_se0	),
	.I_P2_RXD		( i_p2_rxd		),
	.I_P2_RXD_SE0	( i_p2_rxd_se0	),
//	.I_P3_RXD		( i_p3_rxd		),
//	.I_P3_RXD_SE0	( i_p3_rxd_se0	),
//	.I_P4_RXD		( i_p4_rxd		),
//	.I_P4_RXD_SE0	( i_p4_rxd_se0	),

// PCI bus output
	.O_DEVSELJ	( usb_out_devsel_	),
	.O_AD		( usb_out_ad		),
	.O_CBEJ		( usb_out_cbe_		),
	.O_PAR		( usb_out_par		),
	.O_FRAMEJ	( usb_out_frame_	),
	.O_IRDYJ	( usb_out_irdy_		),
	.O_TRDYJ	( usb_out_trdy_		),
	.O_PERRJ	( usb_out_perr_		),
	.O_SERRJ	( usb_out_serr_		),
	.O_STOPJ	( usb_out_stop_		),

	.ADOE		( usb_oe_ad		),
	.CBEJOE		( usb_oe_cbe	),
	.PAROE		( usb_oe_par	),
	.FRAMEJOE	( usb_oe_frame	),
	.IRDYJOE	( usb_oe_irdy	),
	.TDSOE		( usb_tdsoe			),	// trdy_, devsel_, stop_ output enable
	.O_USBREQJ	( usb_req_		),
	.O_PCIINTJ	( usb_int_		),

// PCI bus input
//T2-eric000425	.I_IDSEL	(PCI_AD[31]	),
	.I_IDSEL	( in_ad[18]		),	// Slow PCI 33 Device	-- Snow 2000-04-30
	.I_AD		( in_ad			),
	.I_CBEJ		( in_cbe_		),
	.I_PAR		( in_par		),
	.I_FRAMEJ	( in_frame_		),
	.I_IRDYJ	( in_irdy_		),
	.I_DEVSELJ	( in_devsel_	),
	.I_TRDYJ	( in_trdy_		),
	.I_STOPJ	( in_stop_		),

	.I_USBGNTJ	( usb_gnt_		),

// others
	.I_ACPI_CLK3( pci_clk		),
	.RSM_RSTJ	( rst_			),

	.I_CLK48	( usb_gate_clk	),
	.I_PCICLK	( pci_clk		),	// PCI clock
	.TESTMD		( TESTMD		),
	.I_PCIRSTJ	( rst_			)

	);

assign	usb_pon_ 	= pwren1j && pwren2j;
assign	i_p1_ovrcurj	= usb_overcur_;
assign	i_p2_ovrcurj	= usb_overcur_;
//assign	i_p3_ovrcurj	= usb_overcur_;
//assign	i_p4_ovrcurj	= usb_overcur_;

`else
// =============== No USB ============================
assign	usb_pon_ = 1'b1;		// power control

assign	p1_ls_mode 	= 1'b0,
		p1_txd 		= 1'b0,
		p1_txd_oej 	= 1'b1,
		p1_txd_se0 	= 1'b0,
		p2_ls_mode 	= 1'b0,
		p2_txd 		= 1'b0,
		p2_txd_oej 	= 1'b1,
		p2_txd_se0 	= 1'b0;
wire	p3_ls_mode 	= 1'b0,
		p3_txd 		= 1'b0,
		p3_txd_oej 	= 1'b1,
		p3_txd_se0 	= 1'b0,
		p4_ls_mode 	= 1'b0,
		p4_txd 		= 1'b0,
		p4_txd_oej 	= 1'b1,
		p4_txd_se0 	= 1'b0;

assign	usb_req_ = 1'b1;
assign	usb_int_ = 1'b1;

assign	usb_out_ad	= 32'h0000_0000;
assign	usb_out_cbe_ = 4'hf;
assign	usb_out_par = 1'b0;
assign	usb_out_frame_ = 1'b1;
assign	usb_out_irdy_ = 1'b1;
assign	usb_out_devsel_ = 1'b1;
assign	usb_out_trdy_ = 1'b1;
assign	usb_out_perr_ = 1'b1;
assign	usb_out_serr_ = 1'b1;
assign	usb_out_stop_ = 1'b1;

assign	usb_oe_ad = 1'b0;
assign	usb_oe_cbe = 1'b0;
assign	usb_oe_frame = 1'b0;
assign	usb_oe_par = 1'b0;
assign	usb_oe_irdy = 1'b0;
assign	usb_tdsoe = 1'b0;

`endif
// ============== Above No USB =====================


//================ MS/SD CARD ======================
	//===================make a 32k clk================
	reg	[9:0]	clk_cnt	;
	reg			clkgen_32k	;
	always 	@(posedge usb_clk or negedge rst_)
		if (!rst_)begin
			clk_cnt		<=	#udly	10'h0		;
			clkgen_32k	<=  #udly	1'b0		;
			end
		else if(clk_cnt==10'h2dc)	begin
			clk_cnt		<=	#udly	10'h0		;
			clkgen_32k	<=	#udly	!clkgen_32k	;
			end
		else	begin
			clk_cnt		<=	#udly	clk_cnt + 1'b1	;
			clkgen_32k	<=	#udly	clkgen_32k	;
			end


	//========================32k clk==================

wire			DFTTM			=	TESTMD; 		//input single
wire			EN_CLK2         =	1'b1;   //input single 		tie 1
//	wire			MS_IP_ENABLE    =	1'b1;   //input single 		register
wire			MULTIFN_BIT     =	1'b1;   //input single 		tie 1
//	wire			MS_INS          =	1'b1;   //input single
//	wire			MS_SERIN_IN    ;// =	1'b0;   //input single
//	wire			M66EN           =	1'b0;   //input single 		register
wire			MS_EN_SLAVECLK  =	1'b1;   //input single 		tie 1
//	wire			SD_IP_ENABLE    =	1'b1;   //input single 		register
//	wire			SD_DATA0_IN     ;//=	1'b0;   //input single
//	wire			SD_DATA1_IN     ;//=	1'b0;   //input single
//	wire			SD_DATA2_IN     ;//=	1'b0;   //input single
//	wire			SD_DATA3_IN     ;//=	1'b0;   //input single
//	wire			SD_MMC_CMD_IN   ;//=	1'b0;   //input single
wire			CLKRUNJ         =	1'b0;   //input single 		tie 0
//		wire			PCICLK          ;   //input single  PCI INTF
//		wire			RSTINJ          ;   //input single  PCI INTF
//		wire			IDSELIN         ;   //input single  PCI INTF
//		wire	[31:0]	PCI_ADIN        ;   //input single  PCI INTF
//		wire	[3:0]	CBEINJ          ;   //input single  PCI INTF
//		wire			FRAMEINJ        ;   //input single  PCI INTF
//		wire			IRDYINJ         ;   //input single  PCI INTF
//		wire			PARIN           ;   //input single  PCI INTF
//	wire			TEST_H		=	1'b0    ;   //input single

wire			MS_CLK          ;   //			output single
wire	[1:0]	MS_BSST         ;   //			output single
wire			MS_PW_CTRL      ;   //			output single
wire			MS_SEROUT       ;   //			output single
wire			MSDEN           ;   //			output single
assign			MSDE	=	~MSDEN	;
wire			MS_ACCESS       ;   //			output single
wire	[3:0]	MS_TESTOUT      ;   //			output single
wire			SD_DATA0_OEJ    ;   //			output single
wire			SD_DATA1_OEJ    ;   //			output single
wire			SD_DATA2_OEJ    ;   //			output single
wire			SD_DATA3_OEJ    ;   //			output single
wire			SD_MMC_CMD_OEJ  ;   //			output single
wire			SD_DATA0_OUT    ;   //			output single
wire			SD_DATA1_OUT    ;   //			output single
wire			SD_DATA2_OUT    ;   //			output single
wire			SD_DATA3_OUT    ;   //			output single
wire			SD_MMC_CMD_OUT  ;   //			output single
wire			SD_MMC_CLK      ;   //			output single
wire	[23:0]	SD_TEST_PIN     ;   //			output single

	wire			VDD25M          =	1'b1;   //input single
	wire			VSSCORE         =	1'b0;   //input single

	wire			MS_CLKRUNJ_OEJ  ;   //			output single
	wire			TEST_DONE       ;   //			output single
	wire			FAIL_H          ;   //			output single
	wire			SD_CLKRUNJ_OUT  ;   //			output single
	wire			SD_CLKRUNJ_OEJ  ;   //			output single


//	wire			DET_PIN         =	1'h0;   //input single
//	wire			WR_PRCT_PIN     =	1'h0;   //input single
	wire	[2:0]	INT_SEL         =	3'h4;   //input single


	wire			CLK_32K         =	clkgen_32k	;   //input single
	wire			MSSD_SROM_EN    =	1'h0;   //input single
	wire	[31:0]	SROM_DATA       =	32'h0;   //input single
//	wire	[2:0]	SD_FNUM_BIT     =	3'h6;   //input single		register
//	wire	[2:0]	MS_FNUM_BIT     =	3'h5;   //input single      register

	wire	[31:0]	ms_pci_adout    ;   //			output single
	wire	[31:0]	sd_pci_adout    ;   //			output single
	wire			ms_parout       ;   //			output single
	wire			sd_parout       ;   //			output single
	wire			ms_trdyoutj     ;   //			output single
	wire			sd_trdyoutj     ;   //			output single
	wire			ms_stopoutj     ;   //			output single
	wire			sd_stopoutj     ;   //			output single
	wire			ms_devseloutj   ;   //			output single
	wire			sd_devseloutj   ;   //			output single
	wire			ms_serroutj     ;   //			output single
	wire			ms_perroutj     ;   //			output single
	wire			ms_perr_oenbj   ;   //			output single
	wire			ms_pci_ad_oenbj ;   //			output single
	wire			sd_pci_ad_oenbj ;   //			output single
	wire			ms_par_oenbj    ;   //			output single
	wire			sd_par_oenbj    ;   //			output single
	wire			ms_ctrl_oenbj   ;   //			output single
	wire			sd_stop_oenbj   ;   //			output single
	wire			sd_trdy_oenbj   ;   //			output single
	wire			sd_devsel_oenbj ;   //			output single
//	wire			ms_intaoutj     ;   //			output single
//	wire			sd_intaoutj     ;   //			output single
//	wire			SD_LED          ;   //			output single
//	wire			SWITCH_MS_SD    ;   //			output single

//======================= TEST_H DELAY A CYCLE ==================
reg		TEST_H_DLY	;
always 	@(posedge usb_clk or negedge rst_)
		if (!rst_)begin
			TEST_H_DLY	<=  #udly	1'b0		;
			end
		else	begin
			TEST_H_DLY	<=	#udly	TEST_H		;
			end

//===========================FINISH DELAY========================
`ifdef INC_MS_SD
MS_SD_TOP MS_SD_TOP (
				.DFTTM           		(DFTTM 				),	//input single
				.EN_CLK2         		(EN_CLK2            ),  //input single
				.MS_IP_ENABLE    		(MS_IP_ENABLE       ),  //input single
				.MULTIFN_BIT     		(MULTIFN_BIT        ),  //input single
                .MS_INS             	(MS_INS             ),  //input single
                .MS_SERIN_IN        	(MS_SERIN_IN        ),  //input single
                .M66EN              	(M66EN              ),  //input single
                .MS_EN_SLAVECLK     	(MS_EN_SLAVECLK     ),  //input single
                .SD_IP_ENABLE       	(SD_IP_ENABLE       ),  //input single
                .SD_DATA0_IN        	(SD_DATA0_IN        ),  //input single
                .SD_DATA1_IN        	(SD_DATA1_IN        ),  //input single
                .SD_DATA2_IN        	(SD_DATA2_IN        ),  //input single
                .SD_DATA3_IN        	(SD_DATA3_IN        ),  //input single
                .SD_MMC_CMD_IN      	(SD_MMC_CMD_IN      ),  //input single
                .CLKRUNJ            	(CLKRUNJ            ),  //input single
                .PCICLK             	(pci_clk            ),  //input single 	PCI INTERFACE
                .RSTINJ             	(rst_             	),  //input single  PCI INTERFACE
                .IDSELIN            	(in_ad[19]          ),  //input single  PCI INTERFACE
                .PCI_ADIN           	(in_ad		        ),  //input single  PCI INTERFACE
                .CBEINJ             	(in_cbe_            ),  //input single  PCI INTERFACE
                .FRAMEINJ           	(in_frame_          ),  //input single  PCI INTERFACE
                .IRDYINJ            	(in_irdy_           ),  //input single  PCI INTERFACE
                .PARIN              	(in_par             ),  //input single  PCI INTERFACE
                .MS_CLK             	(MS_CLK             ),	 //			output single
                .MS_BSST            	(MS_BSST            ),   //			output single
                .MS_PW_CTRL         	(MS_PW_CTRL         ),   //			output single
                .MS_SEROUT          	(MS_SEROUT          ),   //			output single
                .MSDE               	(MSDEN              ),   //			output single
                .MS_ACCESS          	(MS_ACCESS          ),   //			output single
                .MS_TESTOUT         	(MS_TESTOUT         ),   //			output single
                .SD_DATA0_OEJ       	(SD_DATA0_OEJ       ),   //			output single
                .SD_DATA1_OEJ       	(SD_DATA1_OEJ       ),   //			output single
                .SD_DATA2_OEJ       	(SD_DATA2_OEJ       ),   //			output single
                .SD_DATA3_OEJ       	(SD_DATA3_OEJ       ),   //			output single
                .SD_MMC_CMD_OEJ     	(SD_MMC_CMD_OEJ     ),   //			output single
                .SD_DATA0_OUT       	(SD_DATA0_OUT       ),   //			output single
                .SD_DATA1_OUT       	(SD_DATA1_OUT       ),   //			output single
                .SD_DATA2_OUT       	(SD_DATA2_OUT       ),   //			output single
                .SD_DATA3_OUT       	(SD_DATA3_OUT       ),   //			output single
                .SD_MMC_CMD_OUT     	(SD_MMC_CMD_OUT     ),   //			output single
                .SD_MMC_CLK         	(SD_MMC_CLK         ),   //			output single
                .SD_TEST_PIN        	(SD_TEST_PIN        ),   //			output single

                .VDD25M             	(VDD25M             ),	 //input single
                .VSSCORE            	(VSSCORE            ),   //input single

                .MS_CLKRUNJ_OEJ     	(MS_CLKRUNJ_OEJ     ),	//			output single
                .TEST_H             	(TEST_H_DLY         ),  //input single
                .TEST_DONE          	(TEST_DONE          ),  //			output single
                .FAIL_H             	(FAIL_H             ),  //			output single
                .SD_CLKRUNJ_OUT     	(SD_CLKRUNJ_OUT     ),  //			output single
                .SD_CLKRUNJ_OEJ     	(SD_CLKRUNJ_OEJ     ),  //			output single
                .DET_PIN            	(DET_PIN            ),  //input single
                .WR_PRCT_PIN        	(WR_PRCT_PIN        ), 	//input single
                .INT_SEL            	(INT_SEL            ),  //input single
                .CLK_32K            	(CLK_32K            ),  //input single
                .MSSD_SROM_EN       	(MSSD_SROM_EN       ),  //input single
                .SROM_DATA          	(SROM_DATA          ),  //input single
                .SD_FNUM_BIT        	(SD_FNUM_BIT        ),  //input single
                .MS_FNUM_BIT        	(MS_FNUM_BIT        ),  //input single
                .MS_PCI_ADOUT       	(ms_pci_adout       ),	//			output single
                .SD_PCI_ADOUT       	(sd_pci_adout       ),  //			output single
                .MS_PAROUT          	(ms_parout          ),  //			output single
                .SD_PAROUT          	(sd_parout          ),  //			output single
                .MS_TRDYOUTJ        	(ms_trdyoutj        ),  //			output single
                .SD_TRDYOUTJ        	(sd_trdyoutj        ),  //			output single
                .MS_STOPOUTJ        	(ms_stopoutj        ),  //			output single
                .SD_STOPOUTJ        	(sd_stopoutj        ),  //			output single
                .MS_DEVSELOUTJ      	(ms_devseloutj      ),  //			output single
                .SD_DEVSELOUTJ      	(sd_devseloutj      ),  //			output single
                .MS_SERROUTJ        	(ms_serroutj        ),  //			output single
                .MS_PERROUTJ        	(ms_perroutj        ),  //			output single
                .MS_PERR_OENBJ      	(ms_perr_oenbj      ),  //			output single
                .MS_PCI_AD_OENBJ    	(ms_pci_ad_oenbj    ),  //			output single
                .SD_PCI_AD_OENBJ    	(sd_pci_ad_oenbj    ),  //			output single
                .MS_PAR_OENBJ       	(ms_par_oenbj       ),  //			output single
                .SD_PAR_OENBJ       	(sd_par_oenbj       ),  //			output single
                .MS_CTRL_OENBJ      	(ms_ctrl_oenbj      ),  //			output single
                .SD_STOP_OENBJ      	(sd_stop_oenbj      ),  //			output single
                .SD_TRDY_OENBJ      	(sd_trdy_oenbj      ),  //			output single
                .SD_DEVSEL_OENBJ    	(sd_devsel_oenbj    ),  //			output single
                .MS_INTAOUTJ        	(MS_INTAOUTJ        ),  //			output single
                .SD_INTAOUTJ        	(SD_INTAOUTJ        ),  //			output single
                .SD_LED             	(SD_LED             ),  //			output single
                .SWITCH_MS_SD        	(SWITCH_MS_SD       )  //			output single
                   	);
`else
assign ms_pci_adout = 32'h0;  
assign sd_pci_adout = 32'h0;  
assign ms_parout = 1'h0;      
assign sd_parout = 1'h0;      
assign ms_trdyoutj = 1'h1;    
assign sd_trdyoutj = 1'h1;    
assign ms_stopoutj = 1'h1;    
assign sd_stopoutj = 1'h1;    
assign ms_devseloutj = 1'h1;  
assign sd_devseloutj = 1'h1;  
assign ms_serroutj = 1'h1;    
assign ms_perroutj = 1'h1;    
assign ms_perr_oenbj = 1'h1;  
assign ms_pci_ad_oenbj = 1'h1;
assign sd_pci_ad_oenbj = 1'h1;
assign ms_par_oenbj = 1'h1;   
assign sd_par_oenbj = 1'h1;   
assign ms_ctrl_oenbj = 1'h1;  
assign sd_stop_oenbj = 1'h1;  
assign sd_trdy_oenbj = 1'h1;  
assign sd_devsel_oenbj = 1'h1;
assign MS_INTAOUTJ = 1'h1;    
assign SD_INTAOUTJ = 1'h1;    
assign SD_LED = 1'h0;         
assign SWITCH_MS_SD = 1'h0;   
                              
assign MS_CLK = 1'h0;         
assign MS_PW_CTRL = 1'h0;     
assign MS_SEROUT = 1'h0;      
assign MSDE = 1'h0;           
assign MS_ACCESS = 1'h0;      
assign MS_BSST = 2'h0;        
assign MS_TESTOUT = 4'h0;     
assign SD_DATA0_OEJ = 1'h1;   
assign SD_DATA1_OEJ = 1'h1;   
assign SD_DATA2_OEJ = 1'h1;   
assign SD_DATA3_OEJ = 1'h1;   
assign SD_MMC_CMD_OEJ = 1'h1; 
assign SD_DATA0_OUT = 1'h0;   
assign SD_DATA1_OUT = 1'h0;   
assign SD_DATA2_OUT = 1'h0;   
assign SD_DATA3_OUT = 1'h0;   
assign SD_MMC_CMD_OUT = 1'h0; 
assign SD_MMC_CLK = 1'h0;     
assign SD_TEST_PIN = 24'h0;   
assign TEST_DONE = 1'h0;      
assign FAIL_H = 1'h0;         
assign MS_CLKRUNJ_OEJ = 1'h1; 
assign SD_CLKRUNJ_OUT = 1'h0; 
assign SD_CLKRUNJ_OEJ = 1'h1; 


`endif
wire	[31:0]	gi_ad_out;
wire	[3:0]	gi_cbe_out;
wire			gi_pci_ognt;
wire	CPU_RST = 1'b1;	//XXX make this an input;
wire	TEST_IN = 1'b1;	//XXX make this an input, comes from test_in pin;

assign	gi_pci_ognt = 1'b0; //XXX other grants;

`ifdef	INC_GI
`define	GI_MOD	gi_pci
`else	// INC_GI
`define	GI_MOD	gi_dummy
`endif	// INC_GI

`GI_MOD GI (
        .clk		(GI_CLK			),
        .clk_lock	(GI_CLK_LOCK	),
        .pin_rst	(PIN_RST		),
        .sys_rst	(RESET			),
        .cpu_rst	(CPU_RST		),
        //system pin connect to flipper
        .alt_boot	(ALT_BOOT		),
        .test_in	(TEST_IN		),
        .test_ena	(TEST_ENA		),
        .gc_rst	 	(GC_RST			),
        //excption connect to cpu?
        .sec_intr	(SEC_INTR		),
        .sec_ack	(SEC_ACK		),//connect to north bridge
        //serial EEPROM Interface
        .i2c_clk	(I2C_CLK		),
        .i2c_din	(I2C_DIN		),
        .i2c_dout	(I2C_DOUT		),
        .i2c_doe	(I2C_DOE		),
        //pci Interface
        .pci_rst	(GI_PCI_RST		),
        .pci_intr	(GI_PCI_INTR	),//connect to north bridge
        .pci_clk	(pci_clk		),
        .pci_req	(gi_pci_req		),
        .pci_gnt	(gi_pci_gnt		),
        .pci_ognt	(gi_pci_ognt	),
//        .idsel		(in_ad[20]		),
        .ad_in		(in_ad			),
        .ad_out		(gi_ad_out		),
        .ad_oe		(gi_ad_oe		),
        .cbe_in		(in_cbe_		),
        .cbe_out	(gi_cbe_out		),
        .cbe_oe		(gi_cbe_oe		),
        .par_in		(in_par			),
        .par_out	(gi_par_out		),
        .par_oe		(gi_par_oe		),
        .frame_in	(in_frame_		),
        .frame_out	(gi_frame_out	),
        .frame_oe	(gi_frame_oe	),
        .devsel_in	(in_devsel_		),
        .devsel_out	(gi_devsel_out	),
        .devsel_oe	(gi_devsel_oe	),
        .irdy_in	(in_irdy_		),
        .irdy_out	(gi_irdy_out	),
        .irdy_oe	(gi_irdy_oe		),
        .trdy_in	(in_trdy_		),
        .trdy_out	(gi_trdy_out	),
        .trdy_oe	(gi_trdy_oe		),
        .stop_in	(in_stop_		),
        .stop_out	(gi_stop_out	),
        .stop_oe	(gi_stop_oe		),
        //Di Interface
        .di_in		(DI_IN			),
        .di_out		(DI_OUT			),
        .di_oe		(DI_OE			),
        .di_brk_in	(DI_BRK_IN		),
        .di_brk_out	(DI_BRK_OUT		),
        .di_brk_oe	(DI_BRK_OE		),
        .di_dir		(DI_DIR			),
        .di_hstrb	(DI_HSTRB		),
        .di_dstrb	(DI_DSTRB		),
        .di_err		(DI_ERR			),
        .di_cover	(DI_COVER		),
        .di_reset	(DI_RESET		),
        .ais_clk	(AIS_CLK		),
        .ais_lr		(AIS_LR			),
        .ais_d		(AIS_D			)
        );

//================ Above MS/SD CARD=================
//============== Invoke IDE IP======================
/*
`ifdef INC_IDE
CORE5229 CORE5229 (
	// input port
     	.BCLK_66M 		(ide66m_clk  ),
        .BCLK_100M 		(ide100m_clk ),
        .CH2_DETECT 	( ch2_detect ),
        .IDSEL 			( in_ad[19]			),	// device 3
        .PAD_AD 		( in_ad				),
        .PAD_CBEJ 		( in_cbe_			),
        .PAD_CLK 		( pci_clk			),
        .PAD_DEVSELJ 	( in_devsel_		),
        .PAD_FRAMEJ 	( in_frame_			),
        .PAD_GNTJ 		( ide_gnt_			),
        .PAD_IRDYJ 		( in_irdy_			),
        .PAD_PAR 		( in_par			),
        .PAD_RSTJ 		( rst_				),
        .PAD_SHD 		( atadd_in		),
        .PAD_SHDRQ 		( atadmarq_in	),
        .PAD_SHINT 		( ataintrq_in	),
        .PAD_SNDIORDY 	( ataiordy_in	),
        .PAD_STOPJ 		( in_stop_	),
        .PAD_TRDYJ 		( in_trdy_	),

	// output port
        .IDE_INTB 		( ide_int_	)	,
        .PI_AD_O 		( ide_out_ad		)	,
        .PI_AD_OE 		( ide_oe_ad			)	,
        .PI_CBEJ_O 		( ide_out_cbe_		)	,
        .PI_CBEJ_OE 	( ide_oe_cbe		)	,
        .PI_DEVSELJ_O 	( ide_out_devsel_	)	,
        .PI_DTS_OE 		( ide_oe_dts		)	,	// pci devsel_, trdy_, stop_ output eable
        .PI_FRAMEJ_O 	( ide_out_frame_	)	,
        .PI_FRAMEJ_OE 	( ide_oe_frame		)	,
        .PI_IRDYJ_O 	( ide_out_irdy_		)	,
        .PI_IRDYJ_OE 	( ide_oe_irdy		)	,
        .PI_PAR_O 		( ide_out_par		)	,
        .PI_PAR_OE 		( ide_oe_par		)	,
        .PI_PERRJ_O 	( ide_out_perr_		)	,
        .PI_REQJ_O 		( ide_req_			)	,
        .PI_STOPJ_O 	( ide_out_stop_		)	,
        .PI_TRDYJ_O 	( ide_out_trdy_		)	,
        .SLV_SHA 		( atada_out		)	,
        .SLV_SHCS1J 	( atacs0_out_	)	,
        .SLV_SHCS3J 	( atacs1_out_	)	,
        .SLV_SHD_OE 	( atadd_oe		)	,	// 4bit width for 16 bit data oe
        .SLV_SHDACKJ 	( atadmack_out_	)	,
        .SLV_SHIORJ 	( atadior_out_	)	,
        .SLV_SHIOWJ 	( atadiow_out_	)	,
        .SLV_SND_HD_O   ( atadd_out		)
);

assign 	atadd_oe_	= ~atadd_oe;

`else
// IDE interface
assign	atadiow_out_	= 1'b1;
assign	atadior_out_	= 1'b1;
assign	atacs0_out_		= 1'b1;
assign	atacs1_out_		= 1'b1;
assign	atadd_out		= 16'h0;
assign	atadd_oe_		= 4'hf;
assign	atada_out		= 3'h0;
//assign	atareset_out_	= 1'b1;
assign	atadmack_out_	= 1'b1;

assign	ide_int_ 	= 1'b1;

assign	ide_out_ad		= 32'h0000_0000;
assign	ide_out_cbe_ 	= 4'hf;
assign	ide_out_par 	= 1'b0;
assign	ide_out_frame_ 	= 1'b1;
assign	ide_out_irdy_ 	= 1'b1;
assign	ide_out_devsel_ = 1'b1;
assign	ide_out_trdy_ 	= 1'b1;
assign	ide_out_perr_ 	= 1'b1;
assign	ide_out_serr_ 	= 1'b1;
assign	ide_out_stop_ 	= 1'b1;

assign	ide_oe_ad 		= 1'b0;
assign	ide_oe_cbe 		= 1'b0;
assign	ide_oe_frame 	= 1'b0;
assign	ide_oe_par 		= 1'b0;
assign	ide_oe_irdy 	= 1'b0;
assign	ide_oe_dts		= 1'b0;
//assign	ide_oe_devsel	= 1'b0;
//assign	ide_oe_trdy		= 1'b0;
//assign	ide_oe_stop		= 1'b0;

`endif
*/
assign	out_ad 	=		({32{gi_ad_oe}} 		& gi_ad_out  	) 	|
					 	({32{host_oe_ad}} 		& host_out_ad  	) 	|
					 	({32{usb_oe_ad}} 		& usb_out_ad 	) 	|
						({32{!ms_pci_ad_oenbj}} & ms_pci_adout 	) 	|
						({32{!sd_pci_ad_oenbj}} & sd_pci_adout 	) 	;
					 	//|({32{ide_oe_ad}} 	& ide_out_ad 	) ;

assign 	out_cbe_= 		({4{gi_cbe_oe}} 	& gi_cbe_out	) 	|
						({4{host_oe_cbe}} 	& host_out_cbe_	) 	|
						({4{usb_oe_cbe}} 	& usb_out_cbe_	)	;
						//({4{ide_oe_cbe}} 	& ide_out_cbe_	)	;

assign	out_par	=		( gi_par_oe 	& gi_par_out 	)	|
						( host_oe_par 	& host_out_par 	)	|
						( usb_oe_par 	& usb_out_par 	)	|
						( !ms_par_oenbj & ms_parout 	)	|
						( !sd_par_oenbj & sd_parout 	)	;//|
						//( ide_oe_par 	& ide_out_par 	);

assign  out_frame_ 	=  	gi_frame_out	&	host_out_frame_	& usb_out_frame_ ;//& ide_out_frame_	;
assign  out_irdy_	= 	gi_irdy_out	&	host_out_irdy_  & usb_out_irdy_	 ;//& ide_out_irdy_	;
assign	out_devsel_	=	gi_devsel_out	&	host_out_devsel_& usb_out_devsel_&
						ms_devseloutj	& sd_devseloutj	 ;//& ide_out_devsel_	;
assign	out_trdy_	= 	gi_trdy_out	&	host_out_trdy_	& usb_out_trdy_	 &
						ms_trdyoutj		& sd_trdyoutj	 ;//& ide_out_trdy_	;
assign	out_stop_	=	gi_stop_out	&	host_out_stop_	& usb_out_stop_	 &
						ms_stopoutj		& sd_stopoutj	 ;//& ide_out_stop_	;

assign	oe_ad_ 		= ~(gi_ad_oe	| host_oe_ad		| usb_oe_ad		 |
						!ms_pci_ad_oenbj| !sd_pci_ad_oenbj);//| ide_oe_ad		);
assign	oe_cbe_		= ~(gi_cbe_oe | host_oe_cbe	| usb_oe_cbe	 );//| ide_oe_cbe	    );
assign	oe_par_		= ~(gi_par_oe | host_oe_par	| usb_oe_par	 |
						!ms_par_oenbj	| !sd_par_oenbj	  );//| ide_oe_par	    );
assign	oe_frame_	= ~(gi_frame_oe | host_oe_frame	| usb_oe_frame	 );//| ide_oe_frame		);
assign	oe_irdy_	= ~(gi_irdy_oe | host_oe_irdy	| usb_oe_irdy	 );//| ide_oe_irdy		);
assign	oe_devsel_	= ~(gi_devsel_oe | host_oe_devsel	| usb_tdsoe		 |
						 !ms_ctrl_oenbj | !sd_devsel_oenbj);//| ide_oe_dts		);
assign	oe_trdy_	= ~(gi_trdy_oe | host_oe_trdy	| usb_tdsoe      |
						 !ms_ctrl_oenbj	| !sd_trdy_oenbj);//| ide_oe_dts		);
assign	oe_stop_	= ~(gi_stop_oe | host_oe_stop	| usb_tdsoe      |
						!ms_ctrl_oenbj	|!sd_stop_oenbj	 );//| ide_oe_dts		);


/*
MS_SD_BH	MS_SD_BH	(

				.MS_SERIN_IN     (MS_SERIN_IN		) , 		//out
				.SD_DATA0_OUT    (SD_DATA0_IN      )	,	// out
				.SD_DATA1_OUT    (SD_DATA1_IN      )  ,     // out
				.SD_DATA2_OUT    (SD_DATA2_IN      )  ,     // out
				.SD_DATA3_OUT    (SD_DATA3_IN      )  ,     // out
				.SD_MMC_CMD_OUT  (SD_MMC_CMD_IN    )  ,     // out
				.PCICLK          (pci_clk            ),		//in
				.RSTINJ          (rst_            )		,//in

				.MS_CLK          (MS_CLK            )	,	//in
				.MS_BSST         (MS_BSST           ) ,      //in
				.MS_PW_CTRL      (MS_PW_CTRL        ) ,      //in
				.MS_SEROUT       (MS_SEROUT         ) ,      //in
				.MSDE            (MSDE              ) ,      //in
				.MS_ACCESS       (MS_ACCESS         ) ,      //in
				.MS_TESTOUT      (MS_TESTOUT        ) ,      //in
				.SD_DATA0_OEJ    (SD_DATA0_OEJ      ) ,      //in
				.SD_DATA1_OEJ    (SD_DATA1_OEJ      ) ,      //in
				.SD_DATA2_OEJ    (SD_DATA2_OEJ      ) ,      //in
				.SD_DATA3_OEJ    (SD_DATA3_OEJ      ) ,      //in
				.SD_MMC_CMD_OEJ  (SD_MMC_CMD_OEJ    ) ,      //in
				.SD_DATA0_IN     (SD_DATA0_IN       ) ,      //in
				.SD_DATA1_IN     (SD_DATA1_IN       ) ,      //in
				.SD_DATA2_IN     (SD_DATA2_IN       ) ,      //in
				.SD_DATA3_IN     (SD_DATA3_IN       ) ,      //in
				.SD_MMC_CMD_IN   (SD_MMC_CMD_IN     ) ,      //in
				.SD_MMC_CLK      (SD_MMC_CLK        ) ,      // in
				.SD_TEST_PIN     (SD_TEST_PIN       ) ,      // in
				.MS_CLKRUNJ_OEJ  (MS_CLKRUNJ_OEJ    ) ,      // in
				.TEST_DONE       (TEST_DONE         ) ,      // in
				.FAIL_H          (FAIL_H            ) ,      // in
				.SD_CLKRUNJ_OUT  (SD_CLKRUNJ_OUT    ) ,      // in
				.SD_CLKRUNJ_OEJ  (SD_CLKRUNJ_OEJ    ) ,      // in

				.SD_LED          (SD_LED            )	,	//in
				.SWITCH_MS_SD    (SWITCH_MS_SD      )       //in

						);
  */
endmodule // p_biu
