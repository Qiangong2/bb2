/************************************************************************
//      Description:    t2_chipset of m3357,integrate all the Application IP
//                      core here:
//                      Host brdg, South brdg, p_biu(pci host,USB,IDE), audio
//                      DSP, Video, Display, GPU, SDRAM controler, eeprom,
//                      chipset_mux
//      Author:         many
//      History:        2003-06-26      copy from m6304
//                      2003-06-26		add servo
						2003-07-03		Add IDE TEST_MODE input
						2003-08-06		Add dsp bist interface
						2003-08-18		Integrate IDE netlist
						2003-08-29		Add work_mode trigger
						2003-09-01		Add video clock part
										connect the IDE clock to ata_clk
						2003-09-18		Add iadc_mclk input
										Add ADC test output
										vdi4dac and vdo4dac changed to 12 bits wide
						2003-09-26	Add test_dly_chain_sel and test_dly_chain_flag port
						2003-10-09	Add sync_source_sel
									TV Encoder sync and DE sync signal are both connected out
						2003-10-16	Add audio_pll_fout_sel
						2003-11-11	Add the VIDEO_IN <-> NB related signals
						2003-11-20	Add the PCI / IDE switch part
						2003-12-01	Add dvb_spi enable
						2003-12-05	Add the tv encoder analog signals
						2003-12-05	Add the SVGA mode enable and the tv encoder clock setting
									Add the clocks for tv encoder only
									Add the IREF2 output
						2003-12-10	Update TVENCODER dft signals by Snow Yi
						2003-12-16	Add output TV mode, change the svga_mode_en from the nb_cpu_biu
									output to the video_core.
									Add the output pixel clock select signal
									Change the audio ADC related port from input to inout.
									Add the ADC_DVDD, ADC_DVSS port
						2004-02-24	Connect the VIDEO_IN reset to the vin_rst_
						2004-02-28	Change the scan chain connection of the ATAINTF_IBUS
									The third chain was put at the head of the second chain
						2004-03-04  Adjust scan chain  for P_BIU. Leiwen
						2004-03-30	add the standby_mode_en
                        2004/05/21  add perr and serr from pbiu by rico
*************************************************************************/
module t2_chipset (

/////////////////////////////
//      x_rd_last,
//      x_wr_last,
        x_ready,
        x_data_in,
//      x_bus_idle,
        x_bus_error,    // To t2core
        x_ack,          // to t2core clear u_request

        u_request,
        u_write,
        u_wordsize,
        u_startpaddr,
        u_data_to_send,
        u_bytemask,

//      g_config_ep,
//        g_reset_,       // From t2core

        x_err_int,
        x_ext_int,

//        cpu_testout,

// external controller

// SDRAM & FLASH
        ram_addr,       // SDRAM address pins & ROM partial address pins
        ram_ba,         // SDRAM bank address
        ram_dq_out,     // SDRAM 32 bits output data pins
        ram_dq_in,      // SDRAM 32 bit input data bus(latch-up),
        ram_dq_oe_,     // Data Port Output Enable
        ram_dqm,        // SDRAM data mask signal
        ram_cs_,        // SDRAM chip select
        ram_cas_,       // SDRAM cas#
        ram_we_,        // SDRAM we#
        ram_ras_,       // SDRAM ras#
        ram_cke,        // SDRAM clock enable

// EEPROM
        eeprom_addr,
        eeprom_rdata,
//        eeprom_ce_,
        eeprom_oe_,
        eeprom_we_,
        eeprom_wdata,
        rom_data_oe_,
        rom_addr_oe,
        eeprom_ale	,
//		eeprom_ale_oe_,

        rom_en_,                 //add by yuky, 20020613, for rom operation
// LED
//      ledpin,

//PCI
        out_rst_,
        oe_ad_,
        out_ad,
        oe_cbe_,
        out_cbe_,
        oe_frame_,
        out_frame_,
        oe_irdy_,
        out_irdy_,
        oe_devsel_,
        out_devsel_,
        oe_trdy_,
        out_trdy_,
        oe_stop_,
        out_stop_,
//========serr and perr=======
        oe_serr_,
        out_serr_,
        oe_perr_,
        out_perr_,
        oe_par_,
        out_par,
        out_gnt_,       //only one gnt_ output in m6304
        in_req_,        //only one requst input in m6304
        in_ad,
        in_cbe_,
        in_frame_,
        in_irdy_,
        in_perr_,
        in_serr_,
        in_devsel_,
        in_trdy_,
        in_stop_,
        in_par,

        ext_int,        // external int, only one int input in m6304
        gpio_out,
        gpio_in,
        gpio_oe_,

        gpio_set2_out,
        gpio_set2_in,
        gpio_set2_oe_,

// -------------------------------------------------------------------------- //
// Audio pads
        //i2s
        out_i2so_data0,
        out_i2so_data1,
        out_i2so_data2,
        out_i2so_data3,
        out_i2so_data4,
        out_i2so_lrck,
        out_i2so_bick,
        i2so_ch10_en,
        out_i2si_lrck,
        out_i2si_bick,
        in_i2si_data,
        slave_i2si_bick,
        slave_i2si_lrck,
        i2si_mode_sel,
        in_i2so_mclk,

//ADC
		iadc_v1p2u ,
		iadc_vs    ,
		iadc_vd    ,
		iadc_dvdd,			//	ADC digital power
		iadc_dvss,			//	ADC digital ground
		iadc_vin_l ,
		iadc_vin_r ,

        iadc_mclk,
		iadc_smtsen,
		iadc_smtsmode,
		iadc_smts_err,
		iadc_smts_end,
		iadc_adcclkp1,
		iadc_adcclkp2,
		iadc_tst_mod,

        //spdif
        out_spdif,
        in_spdif,
// -------------------------------------------------------------------------- //
// South Bridge

        ir_rx,

        scb_scl_out,
        scb_scl_oe,
        scb_sda_out,
        scb_sda_oe,
        scb_scl_in,
        scb_sda_in,
// UART interface
		uart_tx,
		uart_rx,

// eric000430: add USB below,
// add more 2 ports in m6304
        p1_ls_mode      ,
        p1_txd          ,
        p1_txd_oej      ,
        p1_txd_se0      ,
        p2_ls_mode      ,
        p2_txd          ,
        p2_txd_oej      ,
        p2_txd_se0      ,
        //add two ports p3,p4 yuky, 2002-06-13
 //       p3_ls_mode      ,
 //       p3_txd          ,
 //       p3_txd_oej      ,
 //       p3_txd_se0      ,
 //       p4_ls_mode      ,
 //       p4_txd          ,
 //       p4_txd_oej      ,
 //       p4_txd_se0      ,
        usb_pon_,
        i_p1_rxd        ,
        i_p1_rxd_se0,
        i_p2_rxd,
        i_p2_rxd_se0,
        //add two ports p3,p4 yuky, 2002-06-13
 //       i_p3_rxd        ,
 //       i_p3_rxd_se0,
 //       i_p4_rxd,
 //       i_p4_rxd_se0,
        usb_clk,
        usb_overcur_,
// eric000430: add USB above

// Display Engine
//        pclk_oe_,
        vdata_oe_,
        vdata_out,
        h_sync_out_,
        h_sync_oe_,
        v_sync_out_,
        v_sync_oe_,

// TV Encoder intf
		tvenc_vdi4vdac,
		tvenc_vdo4vdac,
		tvenc_idump,
		tvenc_iext,
		tvenc_iext1,		// 470 Ohm connects for the full scale amplitudes
		tvenc_iout1,
		tvenc_iout2,
		tvenc_iout3,
		tvenc_iout4,
		tvenc_iout5,
		tvenc_iout6,

		tvenc_iref1,
		tvenc_iref2,
		tvenc_gnda,
		tvenc_gnda1,		// ground for DAC
		tvenc_vsud,			// ground for DAC
		tvenc_vd33a,

//added for m3357 JFR030626
		tvenc_h_sync_,	//output to mux with ext TV encoder for DE slaver sync
		tvenc_v_sync_,
		tvenc_vdactest,

//VIDEO IN JFR 031127
		VIN_PIXEL_DATA	,
		VIN_PIXEL_CLK	,
		VIN_H_SYNCJ	    ,
		VIN_V_SYNCJ	    ,

//----------------- SERVO Interface begin -------------------------
		//SERVO INTERFACE
//		dmckg_mpgclk		,		// MEM_CLK
//		sv_top_clkdiv		,
		sv_pmata_somd		,
		sv_pmsvd_trcclk		,
		sv_pmsvd_trfclk		,
		sv_sv_tslrf			,
		sv_sv_tplck			,
//		sv_sv_dev_true     	,
		sv_tst_svoio_i			,
		sv_svo_svoio_o			,
		sv_svo_svoio_oej		,
		sv_tst_addata			,
		sv_reg_rctst_2       	,
//		sv_top_sv_mppdn		,
//		sv_dmckg_sysclk        ,
		sv_mckg_dspclk         ,
//		sv_sycg_svoata 		,
//		sv_sv_reg_c2ftst       ,
//		sv_pmdm_ideslave      ,
		sv_pmsvd_tplck         ,
		sv_pmsvd_tslrf         ,
		sv_sv_tst_pin			,
//		sv_sv_reg_tstoen       ,
		sv_pmsvd_c2ftstin		,
		sv_sycg_somd			,
		// Servo analog
//		sv_cpu_gpioin			,
//		sv_sycg_burnin			,
//		External uP interface
		sv_ex_up_in				,
		sv_ex_up_out			,
		sv_ex_up_oe_			,
		// Servo Atapi inf
		sv_sv_hi_hdrq_o        ,
		sv_sv_hi_hdrq_oej      ,
		sv_sv_hi_hcs16j_o      ,
		sv_sv_hi_hcs16j_oej    ,
		sv_sv_hi_hint_o        ,
		sv_sv_hi_hint_oej      ,
		sv_sv_hi_hpdiagj_o     ,
		sv_sv_hi_hpdiagj_oej   ,
		sv_sv_hi_hdaspj_o      ,
		sv_sv_hi_hdaspj_oej    ,
		sv_sv_hi_hiordy_o      ,
		sv_sv_hi_hiordy_oej    ,
		sv_sv_hi_hd_o			,
		sv_sv_hi_hd_oej        ,
		sv_pmata_hrstj         ,
		sv_pmata_hcs1j         ,
		sv_pmata_hcs3j         ,
		sv_pmata_hiorj         ,
		sv_pmata_hiowj         ,
		sv_pmata_hdackj        ,
		sv_pmata_hpdiagj       ,
		sv_pmata_hdaspj        ,
		sv_pmata_ha			,
		sv_pmata_hd			,
		// Servo Test
		sv_sv_svo_sflag		,
		sv_sv_svo_soma			,
		sv_sv_svo_somd			,
		sv_sv_svo_somd_oej     ,
		sv_sv_svo_somdh_oej    ,
//		sv_sv_reg_dvdram_toe   ,
//		sv_sv_reg_dfctsel      ,
//		sv_sv_reg_trfsel 		,
//		sv_sv_reg_flgtoen 		,
//		sv_sv_reg_gpioten		,
//		sv_sv_reg_13e_7		,
//		sv_sv_reg_13e_6		,
//		sv_reg_trg_plcken		,
//		sv_reg_sv_tplck_oej	,
		//	Servo analog ANA_DTSV

		sv_pad_clkref			,
		sv_xadcin              ,
		sv_xadcip              ,
		sv_xatton              ,
		sv_xattop              ,
		sv_xbiasr              ,
		sv_xcdld               ,
		sv_xcdpd               ,
		sv_xcdrf               ,
		sv_xcd_a               ,
		sv_xcd_b               ,
		sv_xcd_c               ,
		sv_xcd_d               ,
		sv_xcd_e               ,
		sv_xcd_f               ,
		sv_xcelpfo             ,
		sv_xdpd_a              ,
		sv_xdpd_b              ,
		sv_xdpd_c              ,
		sv_xdpd_d              ,
		sv_xdvdld              ,
		sv_xdvdpd              ,
		sv_xdvdrfn             ,
		sv_xdvdrfp             ,
		sv_xdvd_a              ,
		sv_xdvd_b              ,
		sv_xdvd_c              ,
		sv_xdvd_d              ,
		sv_xfelpfo             ,
		sv_xfocus              ,
		sv_xgmbiasr            ,
		sv_xlpfon              ,
		sv_xlpfop              ,
		sv_xpdaux1             ,
		sv_xpdaux2             ,
		sv_xsblpfo             ,
		sv_xsfgn               ,
		sv_xsfgp               ,

		sv_pata_sflag			,
		sv_pmata_sflag			,
		sv_pmata_sflag_oej		,

		sv_xsflag              ,
		sv_xslegn              ,
		sv_xslegp              ,
		sv_xspindle            ,
		sv_xtelp               ,
		sv_xtelpfo             ,
		sv_xtestda             ,
		sv_xtexo               ,
		sv_xtrack              ,
		sv_xtray               ,
		sv_xvgain              ,
		sv_xvgaip              ,
		sv_xvref15             ,
		sv_xvref21             ,
		//Servo power
		sv_vd33d            	,
		sv_vd18d            	,
		sv_vdd3mix1				,
		sv_vdd3mix2				,
		sv_avdd_ad          	,
		sv_avdd_att         	,
		sv_avdd_da          	,
		sv_avdd_dpd         	,
		sv_avdd_lpf         	,
		sv_avdd_rad         	,
		sv_avdd_ref         	,
		sv_avdd_svo         	,
		sv_avdd_vga         	,
		sv_avss_ad          	,
		sv_avss_att         	,
		sv_avss_da          	,
		sv_avss_dpd         	,
		sv_avss_ga          	,
		sv_avss_gd          	,
		sv_avss_lpf         	,
		sv_avss_rad         	,
		sv_avss_ref         	,
		sv_avss_svo         	,
		sv_avss_vga         	,
		sv_azec_gnd         	,
		sv_azec_vdd         	,
		sv_dv18_rcka        	,
		sv_dv18_rckd        	,
		sv_dvdd             	,
		sv_dvss             	,
		sv_dvss_rcka        	,
		sv_dvss_rckd        	,
		sv_fad_gnd          	,
		sv_fad_vdd          	,
		sv_gndd             	,

//------------------- Servo interface end-------------------

// IDE Interface
        atadiow_out_,
        atadior_out_,
        atacs0_out_,
        atacs1_out_,
        atadd_out,
        atadd_oe_,
        atada_out,
        atareset_out_,
        atareset_oe_,
        atadmack_out_,

        ataiordy_in,
        ataintrq_in,
        atadd_in,
        atadmarq_in,

//=====GI Interface(from broadon)
//system
	    alt_boot		,
//		test_ena	    ,
		gc_rst_	        ,
		pin_rst_		,
		gi_chip_rst_	,
		gi_clk			,
		gi_clk_lock	    ,
//--serial pro
		i2c_clk	        ,
		i2c_din	        ,
		i2c_dout	    ,
		i2c_doe	        ,
//nmask interrupt
		sec_intr		,
		sec_ack			,
//--dsic inter
		di_in		    ,
		di_out		    ,
		di_oe		    ,
		di_brk_in	    ,
		di_brk_out	    ,
		di_brk_oe	    ,
		di_dir		    ,
		di_hstrb	    ,
		di_dstrb	    ,
		di_err		    ,
		di_cover	    ,
		di_stb			,

		ais_clk	        ,
		ais_lr		    ,
		ais_d		    ,




// -------------------------------------------------------------------------- //
        cpu_clk,                        // pipeline clock   (CPU core)
        mem_clk,                        // transfer clock   (Memory Controller, IO, ROM)
        vsb_clk,						//80M
        pci_clk,
        sb_clk,
        dsp_clk,
        spdif_clk,
		ata_clk,
		servo_clk,				// servo clock
        pad_boot_config,

		inter_only,				// Interlace output
		dvi_rwbuf_sel,			// DVI buffer read/write

		cvbs2x_clk,				// dvi0_clk, 27/40/54 MHz clock
		cvbs_clk,				// dvi1_clk, 27MHz clock
		cav_clk,				// dvi_buf2754_clk, 27/40/54 MHz clock
		dvi_buf0_clk,			// CAV_CLK or VIDEO_CLK for buffer 0
		dvi_buf1_clk,			// CAV_CLK or VIDEO_CLK for buffer 1
		video_clk,				// VIDEO clock output

		tv_f108m_clk,			// tv_encoder 108MHz clock
		tv_cvbs2x_clk,			// tv_encoder 54MHZ clock
		tv_cvbs_clk,			// tv_encoder 27MHz clock
		tv_cav2x_clk,			// tv_encoder 108 or 54 MHZ clock
		tv_cav_clk,				// tv_encoder 54 or 27 MHZ clock


//	PLL control
		cpu_clk_pll_m_value,	// new cpu_clk_pll m parameter
		cpu_clk_pll_m_tri,      // cpu_clk_pll m modification trigger
		cpu_clk_pll_m_ack,      // cpu_clk_pll m modification ack

		mem_clk_pll_m_value,    // new mem_clk_pll m parameter
		mem_clk_pll_m_tri,      // mem_clk_pll m modification trigger
		mem_clk_pll_m_ack,      // mem_clk_pll m modification ack

//		pci_fs_value,			// new pci frequency select parameter
//		pci_fs_tri,             // pci frequency select modification trigger
//		pci_fs_ack,             // pci frequency select modification ack

		work_mode_value,		// new work_mode
		work_mode_tri,          // work_mode modification trigger
		work_mode_ack,          // work_mode modification ack

//	clock frequency select, clock misc control
		ide_fs,					// ide clock frequency select
		dsp_fs,					// dsp clock frequency select
		dsp_div_src_sel,		// dsp source clock select
		pci_fs,					// pci clock frequency select
		ve_fs,					// ve clock frequency select
		ve_div_src_sel,			// ve source clock select
//		pci_clk_out_sel,
		svga_mode_en,			// SVGA mode output enable
		tv_mode,				// video core output pixel for tv mode
		out_mode_etv_sel,		// output pixel clock select, 0 -> 54 MHz, 1 -> 27 MHz
//		tv_enc_clk_sel,			// TV encoder clock setting


        //sdram delay select
        mem_dly_sel,
        mem_rd_dly_sel,
		test_dly_chain_sel,		// TEST_DLY_CHAIN delay select
		test_dly_chain_flag,	// TEST_DLY_CHAIN flag output

//	external interface control
		audio_pll_fout_sel,		// Audio PLL FOUT frequency select
		i2s_mclk_sel,			// I2S MCLK source select
		i2s_mclk_out_en_,		// I2S MCLK output enable
		spdif_clk_sel,			// S/PDIF clock select
		i2so_data2_1_gpio_en,	//I2SO DATA[2:1] GPIO enable
		i2so_data3_gpio_en,		// I2SO DATA3 GPIO enable
		xsflag_gpio_en,			// Xsflag[1:0] share with gpio[25:24] enable
		spdif_gpio_en,			// Spdif share with gpio[21] en
		uart_gpio_en,			// Uart_tx share with gpio[24] en
		hv_sync_gpio_en,		// H_sync_, v_sync_ share with gpio[11:10]
		hv_sync_en,				// h_sync_, v_sync_ enable on the xsflag[1:0] pins
		i2si_clk_en,			// I2S input data enable on the gpio[7] pin
		scb_en,					// SCB interface enable on the gpio[6:5] pins
		servo_gpio_en,			// servo_gpio[2:0] enable on the gpio[2:0] pins
		ext_ide_device_en,		// external IDE device enable
//		pci_gntb_en,			// PCI GNTB# enable
		vdata_en,				// video data enable
		sync_source_sel,		// sync signal source select, 0 -> TVENC, 1 -> DE.
		video_in_intf_en,		// video in interface enable
//		pci_mode_ide_en,		// pci_mode ide interface enable
		sdram_cs1j_en,			// sdram cs1# enable
		ata_share_mode_2_en,	// atadmack_ and atada[0] share with rom_oe_ and rom_we_
		ata_ad_src_sel,			// rom_addr[18:17] bond selection
		ide_active,				// ide interface is active in the pci mode

		standby_mode_en,		// system standby mode

//	SERVO control
		sv_tst_pin_en,			// SERVO test pin enable
		sv_ide_mode_part0_en,	// ide mode SERVO test pin part 0 enable
		sv_ide_mode_part1_en,	// ide mode SERVO test pin part 1 enable
		sv_ide_mode_part2_en,	// ide mode SERVO test pin part 2 enable
		sv_ide_mode_part3_en,	// ide mode SERVO test pin part 3 enable
		sv_c2ftstin_en,			// SERVO c2 flag enable
//		sv_sycg_updbgout,		// SERVO cpu interface debug enable
		sv_trg_plcken,			// SERVO tslrf and tplck enable
		sv_pmsvd_trcclk_trfclk_en,	// SERVO pmsvd_trcclk, pmsvd_trfclk enable

//PAD driver capability
		gpio_pad_driv	   	,	// gpio pad driver capability
		rom_da_pad_driv    	,   // rom data/address driver capability
		sdram_data_pad_driv	,   // sdram data driver capability
		sdram_ca_pad_driv	,   // sdram command/address driver capability

		flash_mode,
//        pci_mode,
        ide_mode,
		tvenc_test_mode	,
		servo_only_mode ,
		servo_test_mode ,
		servo_sram_mode ,
		bist_test_mode	,

		dsp_bist_mode	,

//DSP bist test interface
		dsp_bist_finish,
		dsp_bist_err_vec,

//VIDEO bist interface
		video_bist_tri		,
		video_bist_finish	,
		video_bist_vec		,

//====================== SD/MS interface ====================
        ms_ins				,
        ms_serin_in   		,
        sd_data0_in   		,
        sd_data1_in   		,
        sd_data2_in   		,
        sd_data3_in   		,
        sd_mmc_cmd_in 		,
        ms_clk        		,
        ms_bsst       		,
        ms_pw_ctrl    		,
        ms_serout     		,
        msde          		,
        sd_data0_oej  		,
        sd_data1_oej  		,
        sd_data2_oej  		,
        sd_data3_oej  		,
        sd_mmc_cmd_oej		,
        sd_data0_out  		,
        sd_data1_out  		,
        sd_data2_out  		,
        sd_data3_out  		,
        sd_mmc_cmd_out		,
        sd_mmc_clk    		,
        test_h        		,
        test_done     		,
        fail_h        		,
        det_pin       		,
        wr_prct_pin   		,

//From  NB
		sd_ip_enable		,
		ms_ip_enable		,

        test_mode,
`ifdef  SCAN_EN
	TEST_SE,
	TEST_SI,
	TEST_SO,
`endif
        sw_rst_pulse,
        rst_                    // all chipset reset pin
        );
//////////////////////
//output                x_rd_last;
//output                x_wr_last;
output          x_ready;
output  [31:0]  x_data_in;
//output                x_bus_idle;
output          x_bus_error;    // To t2core
output          x_ack;

input           u_request;
input           u_write;
input   [1:0]   u_wordsize;
input   [31:0]  u_startpaddr;
input   [31:0]  u_data_to_send;
input   [3:0]   u_bytemask;

//input         g_config_ep;
//input           g_reset_;       // From t2core

output          x_err_int;
output          x_ext_int;

//input	[39:0]	cpu_testout;	// Add for test

// chipset controller
// SDRAM & FLASH
output  [11:0]  ram_addr;       // SDRAM address pins & ROM partial address pins
output  [1:0]   ram_ba;         // SDRAM bank address
output  [31:0]  ram_dq_out;     // SDRAM & Flash ROM 32 bits output data pins
input   [31:0]  ram_dq_in;      // SDRAM and Flash 32 bit input data bus
output  [31:0]  ram_dq_oe_;     // Data Port Output Enable
output          ram_cas_;       // SDRAM cas# and flash we#
output          ram_we_;        // SDRAM we#
output          ram_ras_;       // SDRAM ras# and Flash address
output          ram_cke;        // SDRAM clock enable
output  [3:0]	ram_dqm;        // SDRAM data mask signal
output  [1:0]   ram_cs_;        // SDRAM chip select

// EEPROM
output  [21:0]  eeprom_addr;
input   [15:0]   eeprom_rdata;
//output          eeprom_ce_;
output          eeprom_oe_;
output          eeprom_we_;
output          rom_data_oe_;
output	[21:0]	rom_addr_oe;
output			eeprom_ale		;
//output			eeprom_ale_oe_	;
output  [7:0]   eeprom_wdata;
output          rom_en_; //means rom operation
// LED
//output        [7:0]   ledpin;

// AGP,PCI
output			out_rst_;
output          oe_ad_;
output  [31:0]  out_ad;
output          oe_cbe_;
output  [3:0]   out_cbe_;
output          oe_frame_;
output          out_frame_;
output          oe_irdy_;
output          out_irdy_;
output          oe_devsel_;
output          out_devsel_;
output          oe_trdy_;
output          out_trdy_;
output          oe_stop_;
output          out_stop_;
output          out_serr_;
output          oe_serr_ ;
output          oe_perr_ ;
output          out_perr_;
output          oe_par_;
output          out_par;

output  [1:0]   out_gnt_;
input   [1:0]   in_req_;

input   [31:0]  in_ad;
input   [3:0]   in_cbe_;

input           in_frame_;
input           in_irdy_;
input           in_devsel_;
input           in_trdy_;
input           in_stop_;
input           in_par;
input           in_serr_;
input           in_perr_;


input           ext_int;
output  [31:0]  gpio_out;
output  [31:0]  gpio_oe_;
input   [31:0]  gpio_in;

output  [31:0]  gpio_set2_out;
output  [31:0]  gpio_set2_oe_;
input   [31:0]  gpio_set2_in;

output          out_i2so_data0,
	        	out_i2so_data1,
        		out_i2so_data2,
        		out_i2so_data3;
output          out_i2so_data4;
output			i2so_ch10_en;
output          out_i2so_lrck;
output          out_i2so_bick;
output          out_i2si_lrck;
output          out_i2si_bick;
input           in_i2si_data;
input   		slave_i2si_bick;
input   		slave_i2si_lrck;
output			i2si_mode_sel;

input           in_i2so_mclk;
input	        iadc_mclk;					// IADC master clock
input	[24:23]	iadc_smtsen;				// ADC SRAM Test Enable
input	[2:0]	iadc_smtsmode;				// ADC SRAM Test Mode
output	[24:23]	iadc_smts_err;              // ADC SRAM Test Error Flag
output	[24:23]	iadc_smts_end;              // ADC SRAM Self Test End
output			iadc_adcclkp1;				// ADC digital test pin 1
output			iadc_adcclkp2;              // ADC digital test pin 2
input	[3:0]	iadc_tst_mod;               // ADC Internal SDM Test Selection

inout			iadc_v1p2u 	;
inout			iadc_vs    	;
inout			iadc_vd    	;
inout			iadc_dvdd;					//	ADC digital power
inout			iadc_dvss;					//	ADC digital ground
input			iadc_vin_l 	;
input			iadc_vin_r 	;

//spdif
output          out_spdif;
input			in_spdif;

// South Bridge
input           ir_rx;

//output          scb_en;
output          scb_scl_out;
output          scb_scl_oe;
output          scb_sda_out;
output          scb_sda_oe;
input           scb_scl_in;
input           scb_sda_in;

output			uart_tx;

input			uart_rx;


// eric000430: add USB below
output  p1_ls_mode;
output  p1_txd          ;
output  p1_txd_oej      ;
output  p1_txd_se0      ;
output  p2_ls_mode      ;
output  p2_txd          ;
output  p2_txd_oej      ;
output  p2_txd_se0      ;
//output  p3_ls_mode;
//output  p3_txd          ;
//output  p3_txd_oej      ;
//output  p3_txd_se0      ;
//output  p4_ls_mode      ;
//output  p4_txd          ;
//output  p4_txd_oej      ;
//output  p4_txd_se0      ;
output  usb_pon_;

input   i_p1_rxd        ;
input   i_p1_rxd_se0;
input   i_p2_rxd        ;
input   i_p2_rxd_se0;
//input   i_p3_rxd        ;
//input   i_p3_rxd_se0;
//input   i_p4_rxd        ;
//input   i_p4_rxd_se0;
input   usb_clk;
input   usb_overcur_;
// eric000430: add USB above

// Display Engine
//output  pclk_oe_;
output  vdata_oe_;
output  h_sync_out_;
output  h_sync_oe_;
output  v_sync_out_;
output  v_sync_oe_;
output  [7:0]   vdata_out;

//TV encoder intf
input	[11:0]	tvenc_vdi4vdac;	// input, for debug
output	[11:0]	tvenc_vdo4vdac;	// output, for debug
output		tvenc_idump;		// output, for external load resistance
output		tvenc_iext;			// inout, for external load resistance	!!! bi-direction
output		tvenc_iext1;		// 470 Ohm connects for the full scale amplitudes
output		tvenc_iout1;		// output, analog
output		tvenc_iout2;		// output, analog
output		tvenc_iout3;		// output, analog
output		tvenc_iout4;		// output, analog
output		tvenc_iout5;		// output, analog
output		tvenc_iout6;		// output, analog

output		tvenc_iref1;		// added for m3357JFR 030626
output		tvenc_iref2;		// tv encoder IREF2 output

output		tvenc_h_sync_;
output		tvenc_v_sync_;
input		tvenc_vdactest;
input		tvenc_gnda;
input		tvenc_gnda1;		// ground for DAC
input		tvenc_vsud;			// ground for DAC
input		tvenc_vd33a;

input	[7:0]	VIN_PIXEL_DATA	;
input		VIN_PIXEL_CLK	;
input		VIN_H_SYNCJ	    ;
input		VIN_V_SYNCJ	    ;



// Servo interface
//input			sv_top_clkdiv			;	//// sERVO iNTERNAL CLOCK DIVISE
input	[23:0]	sv_pmata_somd  		;	////
input			sv_pmsvd_trcclk		;
input			sv_pmsvd_trfclk		;
output			sv_sv_tslrf			;
output			sv_sv_tplck			;
//output			sv_sv_dev_true     	;
output	[2:0]	sv_svo_svoio_o 		;
output	[2:0]	sv_svo_svoio_oej	;
output			sv_reg_rctst_2       	;
input	[2:0]	sv_tst_svoio_i			;
input	[5:0]	sv_tst_addata 			;
//input			sv_top_sv_mppdn		;
//input			sv_dmckg_sysclk        ;	//// Servo System clock decided by TOP_CLKDIV
input			sv_mckg_dspclk         ;
//input			sv_sycg_svoata 		;
//output			sv_sv_reg_c2ftst       ;
//input			sv_pmdm_ideslave       ;
input			sv_pmsvd_tplck         ;
input			sv_pmsvd_tslrf         ;
output	[44:0]	sv_sv_tst_pin    		;
//output			sv_sv_reg_tstoen       ;
input	[1:0]	sv_pmsvd_c2ftstin 		;
input	[23:0]	sv_sycg_somd     		;
//				analog
//input	[3:0]	sv_cpu_gpioin			;
//input	[1:0]	sv_sycg_burnin			;
input	[22:0]	sv_ex_up_in				;
output	[22:0]	sv_ex_up_out			;
output	[22:0]	sv_ex_up_oe_			;

//			Atapi inf
output			sv_sv_hi_hdrq_o        ;
output			sv_sv_hi_hdrq_oej      ;
output			sv_sv_hi_hcs16j_o      ;
output			sv_sv_hi_hcs16j_oej    ;
output			sv_sv_hi_hint_o        ;
output			sv_sv_hi_hint_oej      ;
output			sv_sv_hi_hpdiagj_o     ;
output			sv_sv_hi_hpdiagj_oej   ;
output			sv_sv_hi_hdaspj_o      ;
output			sv_sv_hi_hdaspj_oej    ;
output			sv_sv_hi_hiordy_o      ;
output			sv_sv_hi_hiordy_oej    ;
output	[15:0] 	sv_sv_hi_hd_o   		;
output			sv_sv_hi_hd_oej        ;
input			sv_pmata_hrstj         ;
input			sv_pmata_hcs1j         ;
input			sv_pmata_hcs3j         ;
input			sv_pmata_hiorj         ;
input			sv_pmata_hiowj         ;
input			sv_pmata_hdackj        ;
input			sv_pmata_hpdiagj       ;
input			sv_pmata_hdaspj        ;
input	[2:0]	sv_pmata_ha           	;
input	[15:0]	sv_pmata_hd          	;

////// SERVO INTERFACE
output	[1:0]	sv_sv_svo_sflag   		;
output	[12:0]	sv_sv_svo_soma   		;
output	[23:0]	sv_sv_svo_somd   		;
output	[21:0]	sv_sv_svo_somd_oej     ;
output	[23:22]	sv_sv_svo_somdh_oej    ;
//output			sv_sv_reg_dvdram_toe   ;
//output			sv_sv_reg_dfctsel      ;
//output			sv_sv_reg_trfsel 		;
//output			sv_sv_reg_flgtoen 		;
//output	[3:0]	sv_sv_reg_gpioten		;
//output			sv_sv_reg_13e_7		;
//output			sv_sv_reg_13e_6		;
//output			sv_reg_trg_plcken		;
//output			sv_reg_sv_tplck_oej	;
//				ANA_DTSV

input			sv_pad_clkref			;
input			sv_xadcin              ;	// Differential input signal to ADC
input			sv_xadcip              ;	// Differential input signal to ADC
output			sv_xatton              ;	// Differential output from attenuator
output			sv_xattop              ;	// Differential output from attenuator
input			sv_xbiasr              ;	// Bi-direction pin for internal bias current external R=12K
output			sv_xcdld               ;	// CD laser power control output
input			sv_xcdpd               ;	// CD laser power control input from the monitor photo diode
input			sv_xcdrf               ;	// CD RF input from pick up head
input			sv_xcd_a               ;	// Photo detect signal input from pickup head for CD sub beam
input			sv_xcd_b               ;	// Photo detect signal input from pickup head for CD sub beam
input			sv_xcd_c               ;	// Photo detect signal input from pickup head for CD sub beam
input			sv_xcd_d               ;	// Photo detect signal input from pickup head for CD sub beam
input			sv_xcd_e               ;	// Photo detect signal input from pickup head for CD sub beam
input			sv_xcd_f               ;	// Photo detect signal input from pickup head for CD sub beam
output			sv_xcelpfo             ;	// Output for outside low pass filter of central error signal
input			sv_xdpd_a              ;	// Photo detect signal input from pickup head for DPD function
input			sv_xdpd_b              ;	// Photo detect signal input from pickup head for DPD function
input			sv_xdpd_c              ;	// Photo detect signal input from pickup head for DPD function
input			sv_xdpd_d              ;	// Photo detect signal input from pickup head for DPD function
output			sv_xdvdld              ;	// DVD laser power control output
input			sv_xdvdpd              ;	// DVD laser power control input from the monitor photo diode
input			sv_xdvdrfn             ;	// DVD RF differential input from pickup head
input			sv_xdvdrfp             ;	// DVD RF differential input from pickup head
input			sv_xdvd_a              ;	// Photo detect signal input from pickup head for DVD servo signal
input			sv_xdvd_b              ;	// Photo detect signal input from pickup head for DVD servo signal
input			sv_xdvd_c              ;	// Photo detect signal input from pickup head for DVD servo signal
input			sv_xdvd_d              ;	// Photo detect signal input from pickup head for DVD servo signal
output			sv_xfelpfo             ;	// Output for outside low pass filter of focus error signal
output			sv_xfocus              ;	// Focus actuator control signal output
input			sv_xgmbiasr            ;	// Bi-derection pin for Gm cell bias, external R=43K
output			sv_xlpfon              ;	// Differential output from low pass filter
output			sv_xlpfop              ;	// Differential output from low pass filter
input			sv_xpdaux1             ;	// Analog test pin 1
input			sv_xpdaux2             ;	// Analog test pin 2
output			sv_xsblpfo             ;	// Output for outside low pass filter of SBAD signal
input			sv_xsfgn               ;	// Spindle rotarion speed detection input pin
input			sv_xsfgp               ;	// Spindle rotarion speed detection input pin

output	[1:0]	sv_pata_sflag     		;
input	[1:0]	sv_pmata_sflag    		;
input	[1:0]	sv_pmata_sflag_oej		;

inout	[1:0]	sv_xsflag				;
output			sv_xslegn              ;	// Control signal output for sledge motor(-)
output			sv_xslegp              ;	// Control signal output for sledge motor(+)
output			sv_xspindle            ;	// PWM output for spindle motor control
input			sv_xtelp               ;	// Low pass filter of tracking center level for TEZC
output			sv_xtelpfo             ;	// Output for outside low passfilter of tracking error signal
output			sv_xtestda             ;	// Reserved DA output pin for testing
output			sv_xtexo               ;	// Tracking error output
output			sv_xtrack              ;	// Track actuator control signal output
output			sv_xtray               ;	// PWM output for tray motor control
input			sv_xvgain              ;	// Differential input signal to VGA
input			sv_xvgaip              ;	// Differential input signal to VGA
output			sv_xvref15             ;	// 1.5V reference voltage output for servo control output signals
output			sv_xvref21             ;	// 2.1V reference voltage output for input I/F from pick_up head

//				power
input			sv_vd33d               ;
input			sv_vd18d               ;
input			sv_vdd3mix1				;	// Digital Power +3.3V inside analog block
input			sv_vdd3mix2				;	// Digital Power +3.3V inside analog block
input			sv_avdd_ad             ;	// Analog Power +3.3V for Servo ADC part
input			sv_avdd_att            ;
input			sv_avdd_da             ;	// Analog Power +3.3V for servo DAC part
input			sv_avdd_dpd            ;	// Analog Power +3.3V for DPD part
input			sv_avdd_lpf            ;
input			sv_avdd_rad            ;	// Analog Power +3.3V for Read Channel ADC part
input			sv_avdd_ref            ;
input			sv_avdd_svo            ;	// Analog Power +3.3V for Servo Signal generation part
input			sv_avdd_vga            ;
input			sv_avss_ad             ;	// Analog Ground for Servo ADC part
input			sv_avss_att            ;
input			sv_avss_da             ;	// Analog Ground for servo DAC part
input			sv_avss_dpd            ;	// Analog Ground for DPD part
input			sv_avss_ga             ;	// Guard Ring Ground for Analog part
input			sv_avss_gd             ;	// Guard Ring Ground for Digital part
input			sv_avss_lpf            ;
input			sv_avss_rad            ;	// Analog Ground for Read Channel ADC part
input			sv_avss_ref            ;
input			sv_avss_svo            ;	// Analog Ground for Servo Signal generation part
input			sv_avss_vga            ;
input			sv_azec_gnd            ;
input			sv_azec_vdd            ;
input			sv_dv18_rcka           ;	// Digital Power +1.8V for Read Channel clock?
input			sv_dv18_rckd           ;	// Digital Power +1.8V for Read Channel clock?
input			sv_dvdd                ;
input			sv_dvss                ;
input			sv_dvss_rcka           ;	// Digital Ground for Read Channel clock?
input			sv_dvss_rckd           ;	// Digital Ground for Read Channel clock?
input			sv_fad_gnd             ;
input			sv_fad_vdd             ;
input			sv_gndd                ;

// IDE Interface
output  	atadiow_out_;
output  	atadior_out_;
output  	atacs0_out_;
output  	atacs1_out_;
output[15:0]atadd_out;
output[3:0] atadd_oe_;
output[2:0] atada_out;
output  	atareset_out_;
output  	atareset_oe_;
output  	atadmack_out_;

input   	ataiordy_in;
input   	ataintrq_in;
input[15:0] atadd_in;
input   	atadmarq_in;

//GI Interface, from broadon
//system
input	    alt_boot		;
//input		test_ena
output		gc_rst_         ;
input		pin_rst_        ;
output		gi_chip_rst_    ;
input		gi_clk          ;
input		gi_clk_lock     ;
//--serial pro
output		i2c_clk         ;
input		i2c_din         ;
output		i2c_dout        ;
output		i2c_doe         ;
//nmask interrupt
output		sec_intr		;
input		sec_ack			;
//--dsic inter
input[7:0]	di_in           ;
output[7:0]	di_out          ;
output[7:0]	di_oe           ;
input		di_brk_in       ;
output		di_brk_out      ;
output		di_brk_oe       ;
input		di_dir          ;
input		di_hstrb        ;
output		di_dstrb        ;
output		di_err          ;
output		di_cover        ;
input		di_stb          ;

input		ais_clk         ;
input		ais_lr          ;
output		ais_d           ;

//clk
input      	cpu_clk;                        // pipeline clock   (CPU core)
input      	mem_clk;                        // transfer clock   (Memory Controller, IO, ROM)
input		vsb_clk;						//
input      	pci_clk;                        // AGP clock        (66)
input      	sb_clk;
input      	dsp_clk;
input      	spdif_clk;
input		ata_clk;
input		servo_clk;					// servo clock
input   [31:0]  pad_boot_config;

output			inter_only;				// Interlace output
output			dvi_rwbuf_sel;			// DVI buffer read/write

//input			f108m_clk;				// 108MHz output
input			cvbs2x_clk;				// 54MHZ clock
input			cvbs_clk;				// 27MHz clock
//input			cav2x_clk;				// 108 or 54 MHZ clock
input			cav_clk;				// 54 or 27 MHZ clock
input			dvi_buf0_clk;			// CAV_CLK or VIDEO_CLK for buffer 0
input			dvi_buf1_clk;			// CAV_CLK or VIDEO_CLK for buffer 1
input			video_clk;				// VIDEO clock output

input			tv_f108m_clk;			// tv_encoder 108MHz clock
input			tv_cvbs2x_clk;			// tv_encoder 54MHZ clock
input			tv_cvbs_clk;			// tv_encoder 27MHz clock
input			tv_cav2x_clk;			// tv_encoder 108 or 54 MHZ clock
input			tv_cav_clk;				// tv_encoder 54 or 27 MHZ clock

//	PLL control
output	[5:0]	cpu_clk_pll_m_value;// new cpu_clk_pll m parameter
output			cpu_clk_pll_m_tri;	// cpu_clk_pll m modification trigger
input			cpu_clk_pll_m_ack;	// cpu_clk_pll m modification ack

output	[5:0]	mem_clk_pll_m_value;// new mem_clk_pll m parameter
output			mem_clk_pll_m_tri;	// mem_clk_pll m modification trigger
input			mem_clk_pll_m_ack;	// mem_clk_pll m modification ack

//output	[1:0]	pci_fs_value;		// new pci_fs parameter
//output			pci_fs_tri;			// pci_fs modification trigger
//input			pci_fs_ack;			// pci_fs modification ack

output	[2:0]	work_mode_value;	// new work_mode
output			work_mode_tri;      // work_mode modification trigger
input			work_mode_ack;      // work_mode modification ack

//	clock frequency select, clock misc control
output	[1:0]   ide_fs;				//ide clock frequency select
output	[1:0]	dsp_fs;				// dsp clock frequency select
output			dsp_div_src_sel;	// dsp source clock select
output	[1:0]	pci_fs;				// pci clock frequency select
output	[1:0]	ve_fs;				// ve clock frequency select
output			ve_div_src_sel;		// ve source clock select
//output	[1:0]	pci_clk_out_sel;	// JFR030830
output			svga_mode_en;		// SVGA mode output enable
output			tv_mode;			// video core output pixel for tv mode
output			out_mode_etv_sel;	// output pixel clock select, 0 -> 54 MHz, 1 -> 27 MHz
//output	[11:8]	tv_enc_clk_sel;		// TV encoder clock setting

output  [4:0]   mem_dly_sel;
output	[4:0]	mem_rd_dly_sel;		//	SDRAM read clock delay control
output	[5:0]	test_dly_chain_sel;	// TEST_DLY_CHAIN delay select
input	[1:0]	test_dly_chain_flag;// TEST_DLY_CHAIN flag output

//	external interface control
output			audio_pll_fout_sel;	// Audio PLL FOUT frequency select
output	[1:0]	i2s_mclk_sel;		// I2S MCLK source select
output			i2s_mclk_out_en_;	// I2S MCLK output enable
output	[1:0]	spdif_clk_sel;		// S/PDIF clock select
output			i2so_data2_1_gpio_en;//I2SO DATA[2:1] GPIO enable
output			i2so_data3_gpio_en;	// I2SO DATA3 GPIO enable
output			xsflag_gpio_en	;	// Xsflag[1:0] share with gpio[25:24] enable
output			spdif_gpio_en	;	// Spdif share with gpio[21] en
output			uart_gpio_en	;	// Uart_tx share with gpio[24] en
output			hv_sync_gpio_en;	// H_sync_, v_sync_ share with gpio[11:10]
output			hv_sync_en;			// h_sync_, v_sync_ enable on the xsflag[1:0] pins
output			i2si_clk_en;		// I2S input data enable on the gpio[7] pin
output			scb_en;				// SCB interface enable on the gpio[6:5] pins
output			servo_gpio_en;		// servo_gpio[2:0] enable on the gpio[2:0] pins
output			ext_ide_device_en;	// external IDE device enable
//output			pci_gntb_en;		// PCI GNTB# enable
output			vdata_en;			// video data enable
output			sync_source_sel;	// sync signal source select, 0 -> TVENC, 1 -> DE.
output			video_in_intf_en;	// video in interface enable
//output			pci_mode_ide_en;	// pci_mode ide interface enable
output			sdram_cs1j_en;		// sdram cs1# enable
output			ata_share_mode_2_en;// atadmack_ and atada[0] share with rom_oe_ and rom_we_
output			ata_ad_src_sel;		// rom_addr[18:17] bond selection

output			ide_active;			// ide interface is active in the pci mode

output			standby_mode_en;	// system standby mode

//	SERVO control
output			sv_tst_pin_en;			// SERVO test pin enable
output			sv_ide_mode_part0_en;	// ide mode SERVO test pin part 0 enable
output			sv_ide_mode_part1_en;	// ide mode SERVO test pin part 1 enable
output			sv_ide_mode_part2_en;	// ide mode SERVO test pin part 2 enable
output			sv_ide_mode_part3_en;	// ide mode SERVO test pin part 3 enable
output			sv_c2ftstin_en;			// SERVO c2 flag enable
wire			sv_sycg_updbgout;		// SERVO cpu interface debug enable
output			sv_trg_plcken;			// SERVO tslrf and tplck enable
output			sv_pmsvd_trcclk_trfclk_en;	// SERVO pmsvd_trcclk, pmsvd_trfclk enable

output	[1:0]	gpio_pad_driv	   	,  	// gpio pad driver capability
				rom_da_pad_driv    	,   // rom data/address driver capability
				sdram_data_pad_driv	,   // sdram data driver capability
				sdram_ca_pad_driv	;   // sdram command/address driver capability

input  [1:0]    flash_mode;
//input			pci_mode,
input			ide_mode,
                tvenc_test_mode,
				servo_only_mode,
				servo_test_mode,
				servo_sram_mode,
				bist_test_mode	;

input			dsp_bist_mode	;

output			dsp_bist_finish;
output	[2:0]	dsp_bist_err_vec;

input			video_bist_tri		;

output			video_bist_finish	;
output	[10:0]	video_bist_vec		;

//====================	SD/MS interface===================
input				ms_ins				;
input				ms_serin_in   		;
input				sd_data0_in   		;
input				sd_data1_in   		;
input				sd_data2_in   		;
input				sd_data3_in   		;
input				sd_mmc_cmd_in 		;

output				ms_clk        		;
output				ms_bsst       		;
output				ms_pw_ctrl    		;
output				ms_serout     		;
output				msde          		;

output				sd_data0_oej  		;
output				sd_data1_oej  		;
output				sd_data2_oej  		;
output				sd_data3_oej  		;
output				sd_mmc_cmd_oej		;
output				sd_data0_out  		;
output				sd_data1_out  		;
output				sd_data2_out  		;
output				sd_data3_out  		;
output				sd_mmc_cmd_out		;
output				sd_mmc_clk    		;
input				test_h        		;
output				test_done     		;
output				fail_h        		;
input				det_pin       		;
input				wr_prct_pin   		;

output				sd_ip_enable		;
output				ms_ip_enable		;

`ifdef  SCAN_EN
input		TEST_SE;
input	[77:0]	TEST_SI;
output	[77:0]	TEST_SO;
wire	[77:0]	TEST_SO;
`endif

output          sw_rst_pulse;
input           rst_;
input           test_mode;

parameter       udly            = 1;

//// Decode Bus ////
//wire    test_mode = 1'b0;    // test mode for GATX20, should be from PAD!

wire    [31:0]  x_data_in;
wire    [1:0]   u_wordsize;
wire    [31:0]  u_startpaddr;
wire    [31:0]  u_data_to_send;
wire    [3:0]   u_bytemask;

wire    [31:0]  ioreg_rd_data,ioreg_wr_data,
                rom_rd_data, rom_wr_data,
                pbiu_rd_data,pbiu_wr_data;

wire    [29:2]  ioreg_addr,rom_addr,
                pbiu_addr;

wire    [1:0]   pbiu_bl;
wire    [3:0]   pbiu_be;
wire    [3:0]   ioreg_be, rom_rw_be;

//FOR PCI AND IDE SWITCH SHARE PINS
wire			IDE_STOP	;
wire			IDE_IDLE    ;
// CPU slave interface
wire    [31:0]  cpu_ram_wdata;
wire    [31:0]  cpu_ram_rdata;
wire    [3:0]   cpu_ram_wbe;
wire    [29:2]  cpu_ram_addr;
wire    [1:0]   cpu_ram_rbl;              // burst length from 1 to 8
wire            cpu_ram_req;
wire            cpu_ram_rw;
wire            cpu_ram_wlast;
wire            cpu_ram_rlast;
wire            cpu_ram_rrdy;
wire            cpu_ram_wrdy;
wire            cpu_ram_err;
wire            cpu_ram_ack;

//dsp aud
wire		DSP_SLAVE_CS	;
wire		DSP_SLAVE_WR	;
wire	[10:0]	DSP_SLAVE_ADDR	;
wire	[23:0]	DSP_SLAVE_WDATA	;
wire	[23:0]	AUD_DSP_RDATA	;

//// IO Register ////
//wire    [7:0]   ledpin;

wire    [31:0]  p_cfg_addr,
                p_cfg_din,
                p_cfg_dout;
wire    [3:0]   p_cfg_be;

wire    [7:0]   ledpin;
// local device io read/write signal
// all the signals are in memory clock domain
// all read/write control, address, byte enable, wdata are connect to all device.
wire            cpu_slave_rw;           // cpu read/write command, 0-> read; 1-> write
wire    [15:2]  cpu_slave_addr;         // cpu read/write address
wire    [3:0]   cpu_slave_be;           // cpu read/write byte enable
wire    [31:0]  cpu_slave_wdata;        // cpu write data

wire            cpu_gpu_req;            // cpu to gpu read/write request
wire            cpu_dsp_req;            // cpu to dsp read/write request
wire            cpu_sb_req;             // cpu to sb read/write request
wire            cpu_ve_req;             // cpu to ve read/write request
wire            cpu_disp_req;           // cpu to display read/write request
wire			cpu_vsb_req;			// cpu to vsb read/write request
wire			cpu_ide_req;			// cpu to ide read/write request
wire			cpu_tvenc_req;			// cpu to tv encoder read/write request
wire			cpu_video_in_req;		// cpu to VIDEO_IN read/write request

wire            cpu_gpu_ack;            // gpu to cpu ack
wire            cpu_dsp_ack;            // dsp to cpu ack
wire            cpu_sb_ack;             // sb to cpu ack
wire            cpu_ve_ack;             // ve to cpu ack
wire            cpu_disp_ack;           // display to cpu ack
wire			cpu_vsb_ack;			// vsb to cpu ack
wire			cpu_ide_ack;			// ide to cpu ack
wire			cpu_tvenc_ack;			// tv encoder to cpu ack
wire			cpu_video_in_ack;		// cpu to VIDEO_IN ack

wire    [31:0]  cpu_gpu_rdata;          // gpu to cpu read data
wire    [31:0]  cpu_dsp_rdata;          // dsp to cpu read data
wire    [31:0]  cpu_sb_rdata;           // sb to cpu read data
//wire    [31:0]  cpu_ve_rdata;           // ve to cpu read data
wire    [31:0]  cpu_disp_rdata;         // display to cpu read data
wire	[31:0]	cpu_vsb_rdata;			// vsb to cpu read data
wire	[31:0]	cpu_ide_rdata;			// ide to cpu read data
wire	[31:0]	cpu_tvenc_rdata;		// TV encoder to cpu read data
wire	[31:0]	cpu_audio_rdata;		// audio to cpu read data
wire	[31:0]	cpu_video_rdata	;		//for video 1060
wire	[31:0]	cpu_servo_rdata	;		//for servo 1061   joyous
wire	[31:0]	cpu_video_in_rdata;		// VIDEO_IN to cpu read data

// PCI read cycle interface
wire            pci_ram_rreq;
wire    [29:2]  pci_ram_raddr;
wire    [1:0]   pci_ram_rbl;
wire            pci_ram_rack;
wire            pci_ram_rerr;
wire            pci_ram_rrdy;
wire            pci_ram_rlast;
wire    [31:0]  pci_ram_rdata;
// PCI Posted Write FIFO interface
wire            pci_ram_wreq;
wire    [29:2]  pci_ram_waddr;
wire            pci_ram_wack;
wire            pci_ram_werr;
wire    [31:0]  pci_ram_wdata;
wire    [1:0]   pci_ram_wbl;
wire    [3:0]   pci_ram_wbe;
wire            pci_ram_wlast;
wire            pci_ram_wrdy;

// Video interface
wire            video_ram_req;
wire            video_ram_rw;
wire    [29:2]  video_ram_addr;
wire    [3:0]   video_ram_wbe;
wire    [1:0]   video_ram_rbl;
wire            video_ram_ack;
wire            video_ram_err;
wire            video_ram_wrdy, video_ram_rrdy;
wire            video_ram_wlast, video_ram_rlast;
wire    [31:0]  video_ram_wdata, video_ram_rdata;

// Video_In interface
wire            vin_ram_req;
wire            vin_ram_rw;
wire    [29:2]  vin_ram_addr;
wire    [3:0]   vin_ram_wbe;
wire    [1:0]   vin_ram_rbl;
wire            vin_ram_ack;
wire            vin_ram_err;
wire            vin_ram_wrdy, vin_ram_rrdy;
wire            vin_ram_wlast, vin_ram_rlast;
wire    [31:0]  vin_ram_wdata, vin_ram_rdata;

// Display Engine interface
wire            disp_ram_req;
wire    [1:0]   disp_ram_rbl;
wire    [29:2]  disp_ram_addr;
wire            disp_ram_ack;
wire            disp_ram_err;
wire            disp_ram_rrdy;
wire            disp_ram_rlast;
wire    [31:0]  disp_ram_rdata;
// South Bridge Device
wire            sb_ram_req, sb_ram_ack, sb_ram_wrdy, sb_ram_wlast, sb_ram_err;
wire            sb_ram_rrdy, sb_ram_rw, sb_ram_rlast;
wire    [3:0]   sb_ram_wbe;
wire    [31:0]  sb_ram_wdata;
wire    [1:0]   sb_ram_rbl;
wire    [27:0]  sb_ram_addr;
wire    [31:0]  sb_ram_rdata;
// ---------------------------------------dsp------------------------------ //
// Local device slave interface
wire            dsp_ram_req;
wire            dsp_ram_rw;
wire    [27:0]  dsp_ram_addr;   // DWORD
wire    [1:0]   dsp_ram_rbl;    // 2'b0 means 1 DW.
wire            dsp_ram_ack;
wire            dsp_ram_err;
wire    [3:0]   dsp_ram_wbe;
wire            dsp_ram_wrdy, dsp_ram_rrdy;
wire            dsp_ram_wlast, dsp_ram_rlast;
wire    [31:0]  dsp_ram_wdata, dsp_ram_rdata;

//--------------------------------audio------------------------------------//
// Local device slave interface
wire            aud_ram_req;
wire            aud_ram_rw;
wire    [27:0]  aud_ram_addr;   // DWORD
wire    [1:0]   aud_ram_rbl;    // 2'b0 means 1 DW.
wire            aud_ram_ack;
wire            aud_ram_err;
wire    [3:0]   aud_ram_wbe;
wire            aud_ram_wrdy, aud_ram_rrdy;
wire            aud_ram_wlast, aud_ram_rlast;
wire    [31:0]  aud_ram_wdata, aud_ram_rdata;

wire			IADC_EXTINR   ;
wire			IADC_EXTINL   ;

//	-----------------------------------------------------------------------------
//Video Support Block Interface:
wire			vsb_ram_req;
wire			vsb_ram_rw;
wire[27:0]		vsb_ram_addr;
wire[1:0]		vsb_ram_rbl;
wire			vsb_ram_wlast;
wire[3:0]		vsb_ram_wbe;
wire[31:0]		vsb_ram_wdata;
wire			vsb_ram_ack;
wire			vsb_ram_err;
wire			vsb_ram_rrdy;
wire			vsb_ram_rlast;
wire[31:0]		vsb_ram_rdata;
wire			vsb_ram_wrdy;

//      --------------------------
//IDE Interface:
wire			ide_ram_req;
wire			ide_ram_rw;
wire[29:2]		ide_ram_addr;
wire[1:0]		ide_ram_rbl;
wire			ide_ram_wlast;
wire[3:0]		ide_ram_wbe;
wire[31:0]		ide_ram_wdata;
wire			ide_ram_ack;
wire			ide_ram_err;
wire			ide_ram_rrdy;
wire			ide_ram_rlast;
wire[31:0]		ide_ram_rdata;
wire			ide_ram_wrdy;

//--------------------------------
//SERVO Interface
///////////// MBUS INTERFACE
//From:
wire			sv_ram_req		;
wire			sv_ram_rw		;
wire	[29:2]	sv_ram_addr		;
wire	[1:0]	sv_ram_rbl		;
//wire	[1:0]	sv_ram_wbl		;
wire			sv_ram_wlast	;
wire	[3:0]	sv_ram_wbe		;
wire	[31:0]  sv_ram_wdata	;
//To:
wire			sv_ram_ack		;
wire			sv_ram_err		;
wire			sv_ram_rrdy		;
wire			sv_ram_rlast	;
wire	[31:0]	sv_ram_rdata	;
wire			sv_ram_wrdy		;

wire    [1:0]   mdm_pmcsr;
wire	[7:0]	sv_ecc_latency_ctrl	;
wire	[1:0]	sv_sycg_burnin		;

wire	[22:0]	sv_ex_up_in				;
wire	[22:0]	sv_ex_up_out			;
wire	[22:0]	sv_ex_up_oe_			;
// -------------------------------------------------------------------------- //
//      -------------------
//for memory interface:
wire            ctrl_ram_req;
wire            ctrl_ram_rst;
wire[1:0]       ctrl_ram_cmd;
wire[7:0]       ram_ref_init;
wire            ram_16bits_mode;
//wire			ram_pre_oe_en;
wire			ram_post_oe_en;
wire			ram_r2w_ctrl;
wire            ram_ba_width;
wire[1:0]       ram_ra_width;
wire[1:0]       ram_ca_width;
wire[1:0]		ram_row1_ra_width;
wire[1:0]		ram_row1_ca_width;
wire            ram_row0_active;
wire            ram_row1_active;
wire[15:0]      ram_timeparam;
wire[11:0]      ram_mode_set;

//      SDRAM arbiter control
wire	[21:0]	devs_reqs_num;

//wire    [1:0]   suba_arbt_ctrl;  // SDRAM sub arbiter A control
//wire    [1:0]   subb_arbt_ctrl;  // SDRAM sub arbiter B control
//wire    [1:0]   main_arbt_ctrl; // SDRAM main arbiter control

//the memory bus arbiter states:
//wire[31:0]		suba_arbt_st;		//store the latest four states of the Sub-A Arbiter
//wire[31:0]		subb_arbt_st;		//store the latest four states of the Sub-B Arbiter
//wire[31:0]		main_arbt_st;		//store the latest four states of the Main Arbiter
//for write dead lock:
wire			ram_wrlock_timer;	//1->enable the write dead lock timer
wire			ram_wrlock_resume;	//1->enable the wr-lock auto resume
wire			ram_wrlock_tag;		//1->write dead lock occur
wire[7:0]		ram_wrlock_id;		//indicate the device id that make dead lock



//	--------------------------------------------------------------------------
wire    [15:0]  intnl_int,
                int_pol_sel,    // polarity
                int_mask_sel;

wire    [19:0]  eeprom_waddr;
wire    [4:0]   fsh_cyl_width;
wire    [5:0]   eeprom_rw_cnt;

wire	[21:0]	rom_addr_oe_tmp_;
assign 	rom_addr_oe = ~(rom_addr_oe_tmp_[21:0]);

wire    sw_rst_pulse = 1'b0; //not support software reset whold chip

// Use gpio signals to control directly
// Includes: ATA reset JFR030717
wire	atareset_out_		= gpio_out[31];
wire    atareset_oe_		= gpio_oe_[31];
/*
wire	ps_cs0_			= gpio_out[31];
wire    ps_cs1_			= gpio_out[30];
wire    ps_cs0_oe_ 		= gpio_oe_[31];
wire    ps_cs1_oe_	 	= gpio_oe_[30];
*/
//================================   for MS/SD       ===============
wire			ms_ctrl_int_		;
wire			sd_ctrl_int_        ;
wire	[2:0]	sd_fnum_bit			;
wire	[2:0]	ms_fnum_bit			;
wire			sd_led         		;
wire			switch_ms_sd        ;
wire			ms_access			;
wire			sd_enable			;
wire			ms_enable	        ;
wire			m66_clk_en	        ;
wire	[1:0]	ms_bsst_dummy		;

assign			ms_bsst			=	ms_bsst_dummy[0]	;
assign			sd_ip_enable	=	sd_enable		;
assign			ms_ip_enable	=	ms_enable       ;

nb_cpu_biu NB_CPU_BIU(
        .X_RD_LAST              (x_rd_last              ),
        .X_WR_LAST              (x_wr_last              ),
        .X_READY                (x_ready                ),
        .X_DATA_IN              (x_data_in              ),
        .X_BUS_IDLE             (x_bus_idle             ),
        .X_BUS_ERROR            (x_bus_error            ),
        .X_ACK                  (x_ack                  ),

        .U_REQUEST              (u_request              ),
        .U_WRITE                (u_write                ),
        .U_WORDSIZE             (u_wordsize             ),
        .U_STARTPADDR           (u_startpaddr           ),
        .U_DATA_TO_SEND         (u_data_to_send         ),
        .U_BYTEMASK             (u_bytemask             ),

        .ENDIANMODE             (endianmode             ),
//		.CPU_TESTOUT			(cpu_testout			),
//*******************************************************************
        .MEM_WDATA              (cpu_ram_wdata          ),
        .MEM_RDATA              (cpu_ram_rdata          ),
        .MEM_WBE                (cpu_ram_wbe            ),
        .MEM_ADDR               (cpu_ram_addr           ),
        .MEM_REQ                (cpu_ram_req            ),
        .MEM_RW                 (cpu_ram_rw             ),
        .MEM_BL                 (cpu_ram_rbl[1:0]       ),
        .MEM_WLAST              (cpu_ram_wlast          ),
        .MEM_RLAST              (cpu_ram_rlast          ),
        .MEM_RRDY               (cpu_ram_rrdy           ),
        .MEM_WRDY               (cpu_ram_wrdy           ),
        .MEM_ERR                (cpu_ram_err            ),
        .MEM_ACK                (cpu_ram_ack            ),

        .PBIU_REQ               (pbiu_req               ),
        .PBIU_RW                (pbiu_rw                ),
        .PBIU_RD_DATA           (pbiu_rd_data           ),
        .PBIU_ADDR              (pbiu_addr              ),
        .PBIU_BL                (pbiu_bl                ),
        .PBIU_GNT               (pbiu_gnt               ),
        .PBIU_RD_RDY            (pbiu_rd_rdy            ),
        .PBIU_LAST              (pbiu_last              ),
        .PBIU_ERR               (pbiu_err               ),
        .PBIU_WR_DATA           (pbiu_wr_data           ),
        .PBIU_BE                (pbiu_be                ),
        .PBIU_WR_RDY            (pbiu_wr_rdy            ),

        .ROM_REQ                (rom_req                ),
        .ROM_ADDR               (rom_addr               ),
        .ROM_GNT                (rom_gnt                ),
        .ROM_RD_DATA            (rom_rd_data            ),
        .ROM_WR_DATA            (rom_wr_data            ),
        .ROM_RW_BE              (rom_rw_be              ),
        .ROM_RW                 (bootrom_rw             ),
        .ROM_ERR                (1'b0                   ),

//        .ledpin                 (ledpin),//no use now

        .P_CFG_ADDR             (p_cfg_addr             ),
        .P_CFG_BE               (p_cfg_be               ),
        .P_CFG_DIN              (p_cfg_din              ),
        .P_CFG_DOUT             (p_cfg_dout             ),
        .P_CFG_RW               (p_cfg_rw               ),
        .P_CFG_CS               (p_cfg_cs               ),
        .P_CFG_GNT              (p_cfg_gnt              ),

// interrupt
// South Bridge Interrupt
		.SERVO_INT				(servo_int				),	//	SERVO interrupt
		.AUDIO_INT				(audio_int				),	//	AUDIO interrupt
		.VIDEO_INT				(video_int				),	//	VIDEO interrupt

		.IRC_INT				(irc_int				),	//	IR interrupt
		.DSP_INT				(dsp_int				),  //	DSP interrupt
		.I2C_INT				(scb_int				),  //	SCB interrupt
		.UART_INT				(uart_int				),	//  UART interrupt
		.IDE_INT_				(ide_int_				),  //	IDE interrupt
		.USB_INT_				(usb_int_				),  //	USB interrupt
		.EXT_INT				(ext_int				),  //	PCI inta# interrupt
		.VIDEO_IN_INT			(video_in_int			),	//	VIDEO_IN interrupt
		.DECSS_INT				(decss_int				),
		.MS_CTRL_INT_			(ms_ctrl_int_           ),
		.SD_CTRL_INT_			(sd_ctrl_int_			),
		.GI_PCI_INT_			(gi_pci_int_			),

        .X_ERR_INT              (x_err_int              ),
        .X_EXT_INT              (x_ext_int              ),

//== CPU Slave Bus
        .CPU_SLAVE_RW           (cpu_slave_rw           ),
        .CPU_SLAVE_ADDR         (cpu_slave_addr         ),
        .CPU_SLAVE_BE           (cpu_slave_be           ),
        .CPU_SLAVE_WDATA        (cpu_slave_wdata        ),

// chip select for slave device
		.CPU_AUDIO_REQ			(cpu_audio_req			),	// cpu to audio io request
		.CPU_VIDEO_REQ			(cpu_video_req			),	// cpu to video io request
		.CPU_SERVO_REQ			(cpu_servo_req			),	// cpu to servo io request
		.CPU_SB_REQ				(cpu_sb_req				),	// cpu to south bridge io request
		.CPU_DSP_REQ			(cpu_dsp_req			),	// cpu to dsp io request
		.CPU_VSB_REQ			(cpu_vsb_req			),	// cpu to vsb io request
		.CPU_IDE_REQ			(cpu_ide_req			),	// cpu to ide io request
		.CPU_TVENC_REQ			(cpu_tvenc_req			),	// cpu to tv encoder io request
		.CPU_VIDEO_IN_REQ		(cpu_video_in_req		),	// cpu to video in io request

		.CPU_AUDIO_ACK			(cpu_audio_ack			),	// audio to cpu ack
		.CPU_VIDEO_ACK			(cpu_video_ack			),	// video to cpu ack
		.CPU_SERVO_ACK			(cpu_servo_ack			),	// servo to cpu ack
		.CPU_SB_ACK				(cpu_sb_ack				),  // south bridge to cpu ack
		.CPU_DSP_ACK			(cpu_dsp_ack			),  // DSP to CPU ack
		.CPU_VSB_ACK			(cpu_vsb_ack			),  // VSB to CPU ack
		.CPU_IDE_ACK			(cpu_ide_ack			),  // IDE to CPU ack
		.CPU_TVENC_ACK			(cpu_tvenc_ack			),  // TV encoder to CPU ack
		.CPU_VIDEO_IN_ACK		(cpu_video_in_ack		),	// VIDEO_IN to cpu ack

        .CPU_AUDIO_RDATA        (cpu_audio_rdata        ),
        .CPU_VIDEO_RDATA        (cpu_video_rdata        ),
        .CPU_SERVO_RDATA        (cpu_servo_rdata        ),
        .CPU_SB_RDATA           (cpu_sb_rdata           ),
        .CPU_DSP_RDATA			(cpu_dsp_rdata			),
        .CPU_VSB_RDATA			(cpu_vsb_rdata			),
        .CPU_IDE_RDATA			(cpu_ide_rdata			),
        .CPU_TVENC_RDATA		(cpu_tvenc_rdata		),
        .CPU_VIDEO_IN_RDATA		(cpu_video_in_rdata		),	// VIDEO_IN to cpu read data

// GPIO
        .GPIO_OUT               (gpio_out               ),
        .GPIO_IN                (gpio_in                ),
        .GPIO_OE_               (gpio_oe_               ),

// GPIO	SET2
        .GPIO_SET2_OUT          (gpio_set2_out          ),
        .GPIO_SET2_IN           (gpio_set2_in           ),
        .GPIO_SET2_OE_          (gpio_set2_oe_          ),

        .PCI_PAR_ERR            (pci_par_err            ),

//      Local Device Control
		.DE_RST_				(video_rst_				), 	// Snow: 2003.11.20 RESET to VIDEO_CORE
		.DSP_RST_				(dsp_rst_				),
		.VE_RST_				(vdec_rst_				),	// Snow: 2003.11.20 RESET to VDEC, VSB and VIN
		.AUDIO_RST_				(audio_rst_				),
		.TVENC_RST_				(tvenc_rst_				),
		.SERVO_RST_				(servo_rst_				),
		.VIN_RST_				(vin_rst_				),
		.PCI_RST_				(out_rst_				),

// Power Manager, Gated clock
		.DSP_CLK_EN				(dsp_clk_en				),
		.VE_CLK_EN				(ve_clk_en				),
		.SERVO_CLK_EN			(servo_clk_en			),	// SERVO clock enable
		.TVENC_CLK_EN			(tvenc_clk_en			),	// TV encoder
		.VIDEO_IN_CLK_EN		(video_in_clk_en		),	// VIDEO_IN clock enable

//      PLL control
		.CPU_CLK_PLL_M_VALUE	(cpu_clk_pll_m_value	),	// new cpu_clk_pll m parameter
		.CPU_CLK_PLL_M_TRI      (cpu_clk_pll_m_tri		),  // cpu_clk_pll m modification trigger
		.CPU_CLK_PLL_M_ACK      (cpu_clk_pll_m_ack		),  // cpu_clk_pll m modification ack

		.MEM_CLK_PLL_M_VALUE    (mem_clk_pll_m_value	),  // new mem_clk_pll m parameter
		.MEM_CLK_PLL_M_TRI      (mem_clk_pll_m_tri		),  // mem_clk_pll m modification trigger
		.MEM_CLK_PLL_M_ACK      (mem_clk_pll_m_ack		),  // mem_clk_pll m modification ack

//		.PCI_FS_VALUE 			(pci_fs_value			),	// new pci frequency select parameter
//		.PCI_FS_TRI             (pci_fs_tri				),	// pci frequency select modification trigger
//		.PCI_FS_ACK             (pci_fs_ack				),	// pci frequency select modification ack

		.WORK_MODE_VALUE		(work_mode_value		),	// new work_mode
		.WORK_MODE_TRI  		(work_mode_tri			),  // work_mode modification trigger
		.WORK_MODE_ACK  		(work_mode_ack			),  // work_mode modification ack
        //      clock frequency control, clock misc control
		.IDE_FS					(ide_fs					),	// ide clock frequency select
		.DSP_FS					(dsp_fs					),	// dsp clock frequency select
		.DSP_DIV_SRC_SEL		(dsp_div_src_sel		),	// dsp source clock select
		.PCI_FS					(pci_fs					),	// pci clock frequency select
		.VE_FS					(ve_fs					),	// ve clock frequency select
		.VE_DIV_SRC_SEL			(ve_div_src_sel			),	// ve source clock select
//		.PCI_CLK_OUT_SEL		(pci_clk_out_sel		),
//		.SVGA_MODE_EN			(						),	// Useless
//		.TV_ENC_CLK_SEL			(tv_enc_clk_sel			),	// TV encoder clock setting
        //      -----------------------------------------------------------
        // SDRAM cmd & parameter
        .CTRL_RAM_REQ           (ctrl_ram_req       	),  // SDRAM CMD request
        .ACK_RAM_CMD            (ctrl_ram_rst       	),  // SDRAM CMD ack
        .CTRL_RAM_CMD           (ctrl_ram_cmd       	),  // SDRAM CMD
        .RAM_REF_INIT           (ram_ref_init       	),  // Refresh rate register
        .RAM_16BITS_MODE        (ram_16bits_mode    	),  // 1 -> RAM in 16 bits mode, 0 -> RAM in 32 bits mode
		.RAM_POST_OE_EN			(ram_post_oe_en			),	// ram data oe post drive enable
        .RAM_BA_WIDTH           (ram_ba_width       	),  // SDRAM bank address width
        .RAM_RA_WIDTH           (ram_ra_width       	),  // SDRAM row address width
        .RAM_CA_WIDTH           (ram_ca_width       	),  // SDRAM col address width
        .RAM_ROW0_ACTIVE        (ram_row0_active    	),  // SDRAM row0 active, hardware to 1
        .RAM_ROW1_ACTIVE        (ram_row1_active    	),  // SDRAM row1 active, hardware to 0 in M6304
        .RAM_TIMEPARAM          (ram_timeparam      	),  // SDRAM timing parameter
        .RAM_MODE_SET           (ram_mode_set       	),  // SDRAM mode set value
		.RAM_R2W_CTRL			(ram_r2w_ctrl			),	// sdram read to write ture around 0-> 2T, 1 -> 1T
        //Flash:
        .FSH_ACC_EN_            (rom_en_            	),  // out to pad_misc and to eeprom
        .FSH_WR_EN              (eeprom_wr_en       	),  // Flash write enable
        .FSH_CYL_WIDTH          (eeprom_rw_cnt      	),  // Flash read/write cycle width counter

        //      arbiter control
        .DEVS_REQS_NUM			(devs_reqs_num			),
		.RAM_WRLOCK_TIMER_EN	(ram_wrlock_timer		),	//
		.RAM_WRLOCK_RESUME_EN	(ram_wrlock_resume		),	//
		.RAM_WRLOCK_TAG			(ram_wrlock_tag			),	//
		.RAM_WRLOCK_ID			(ram_wrlock_id			),	// SDRAM write deadlock device

//	external interface control
		.AUDIO_PLL_FOUT_SEL		(audio_pll_fout_sel		),	// Audio PLL FOUT frequency select
		.I2S_MCLK_SEL			(i2s_mclk_sel			),	// I2S MCLK source select
		.I2S_MCLK_OUT_EN_		(i2s_mclk_out_en_		),	// I2S MCLK output enable
		.SPDIF_CLK_SEL			(spdif_clk_sel			),	// S/PDIF clock select
		.I2SO_DATA2_1_GPIO_EN	(i2so_data2_1_gpio_en	),	// I2SO DATA[2:1] GPIO enable
		.I2SO_DATA3_GPIO_EN		(i2so_data3_gpio_en		),	// I2SO DATA3 GPIO enable
		.XSFLAG_GPIO_EN			(xsflag_gpio_en			),	// Xsflag[1:0] share with gpio[25:24] enable
		.SPDIF_GPIO_EN			(spdif_gpio_en			),	// Spdif share with gpio[21] en
		.UART_GPIO_EN			(uart_gpio_en			),	// Uart_tx share with gpio[24] en
		.HV_SYNC_GPIO_EN		(hv_sync_gpio_en		),	// H_sync_, v_sync_ share with gpio[11:10]
		.HV_SYNC_EN				(hv_sync_en				),	// h_sync_, v_sync_ enable on the xsflag[1:0] pins
		.I2SI_CLK_EN			(i2si_clk_en			),	// I2S input data enable on the gpio[7] pin
		.SCB_EN					(scb_en					),	// SCB interface enable on the gpio[6:5] pins
		.SERVO_GPIO_EN			(servo_gpio_en			),	// servo_gpio[2:0] enable on the gpio[2:0] pins
		.EXT_IDE_DEVICE_EN		(ext_ide_device_en		),	// external IDE device enable
//		.PCI_GNTB_EN			(pci_gntb_en			),	// PCI GNTB# enable
		.VDATA_EN				(vdata_en				),	// video data enable
		.SYNC_SOURCE_SEL		(sync_source_sel		),	// sync signal source select, 0 -> TVENC, 1 -> DE.
		.VIDEO_IN_INTF_EN		(video_in_intf_en		),	// video in interface enable
//		.PCI_MODE_IDE_EN		(pci_mode_ide_en		),	// pci_mode ide interface enable
		.DVB_SPI_EN				(dvb_spi_en				),	// DVB SPI interface enable
		.SDRAM_CS1J_EN			(sdram_cs1j_en			),	// sdram cs1# enable
		.ATA_SHARE_MODE_2_EN	(ata_share_mode_2_en	),	// atadmack_ and atada[0] share with rom_oe_ and rom_we_
		.ATA_AD_SRC_SEL			(ata_ad_src_sel			),	// rom_addr[18:17] bond selection
		.STANDBY_MODE_EN		(standby_mode_en		),	// system standby mode

//	SERVO control
		.SV_TST_PIN_EN				(sv_tst_pin_en			),	// SERVO test pin enable
		.SV_IDE_MODE_PART0_EN		(sv_ide_mode_part0_en	),	// ide mode SERVO test pin part 0 enable
		.SV_IDE_MODE_PART1_EN		(sv_ide_mode_part1_en	),	// ide mode SERVO test pin part 1 enable
		.SV_IDE_MODE_PART2_EN		(sv_ide_mode_part2_en	),	// ide mode SERVO test pin part 2 enable
		.SV_IDE_MODE_PART3_EN		(sv_ide_mode_part3_en	),	// ide mode SERVO test pin part 3 enable
		.SV_C2FTSTIN_EN				(sv_c2ftstin_en			),	// SERVO c2 flag enable
		.SV_SYCG_UPDBGOUT			(sv_sycg_updbgout		),	// SERVO cpu interface debug enable
		.SV_TRG_PLCKEN				(sv_trg_plcken			),	// SERVO tslrf and tplck enable
		.SV_PMSVD_TRCCLK_TRFCLK_EN	(sv_pmsvd_trcclk_trfclk_en),	// SERVO pmsvd_trcclk, pmsvd_trfclk enable
		.SV_ECC_LATENCY_CTRL		(sv_ecc_latency_ctrl	),
		.SV_SYCG_BURNIN				(sv_sycg_burnin			),	// SERVO burn in test control

//Ext IR int control
		.EXT_IR_INT					(ir_rx					),	// external int shared with IR int
//	PAD Driever Capability
	.GPIO_PAD_DRIV	   		(gpio_pad_driv	   		),	// gpio pad driver capability
	.ROM_DA_PAD_DRIV    	(rom_da_pad_driv    	),  // rom data/address driver capability
	.SDRAM_DATA_PAD_DRIV	(sdram_data_pad_driv	),  // sdram data driver capability
	.SDRAM_CA_PAD_DRIV		(sdram_ca_pad_driv		),  // sdram command/address driver capability

//==================  for MS/SD	===============================
		.SD_CTRL_PCI_NUM	(sd_fnum_bit		)	,
        .MS_CTRL_PCI_NUM	(ms_fnum_bit		)	,
        .SD_IP_EN			(sd_enable				)	,
        .MS_IP_EN			(ms_enable				)	,
        .M66_EN				(m66_clk_en			)	,
        .SD_LED_STATUS		(sd_led         	)	,
        .MS_ACCESS_STATUS	(ms_access   		)	,
        .SWITCH_MS_SD       (switch_ms_sd		)	,
    //------------------------SWITCH_MS_SD		------------------------------------
        .MEM_DLY_SEL            (mem_dly_sel        	),
        .MEM_RD_DLY_SEL			(mem_rd_dly_sel			),
		.TEST_DLY_CHAIN_SEL		(test_dly_chain_sel		),	// TEST_DLY_CHAIN delay select
		.TEST_DLY_CHAIN_FLAG	(test_dly_chain_flag	),	// TEST_DLY_CHAIN flag output
        .STRAP_PIN_IN           (pad_boot_config    	),

        .CPU_CLK                (cpu_clk            	),  // CPU clock
        .PCI_CLK                (pci_clk            	),  // PCI Clock
        .MEM_CLK                (mem_clk            	),  // Chipset (Mem) clock
        .TEST_MODE              (test_mode          	),

        `ifdef	INSERT_SCAN_NB
        .TEST_SE				(1'b0					),
        `endif

`ifdef  SCAN_EN
		.TEST_SE				(TEST_SE				),
		.TEST_SI1				(TEST_SI[0]		),
		.TEST_SI2				(TEST_SI[1]		),
		.TEST_SI3				(TEST_SI[2]		),
		.TEST_SO1				(TEST_SO[0]		),
		.TEST_SO2				(TEST_SO[1]		),
		.TEST_SO3				(TEST_SO[2]		),
`endif
//temp use for cpu boot mode, 0 normal(boot from flash), 1 boot from gi pci memory
`ifdef BOOT_FROM_GI
		.BOOT_FROM_GI			(1'b1			),
`else
		.BOOT_FROM_GI			(1'b0			),
`endif

        .RST_                   (rst_               )
                );


//// Boot Rom ////
wire [21:0]	eeprom_addr_tmp	;
assign		eeprom_addr		=	eeprom_addr_tmp[21:0];
eeprom EEPROM           (
        .EEPROM_ADDR            (eeprom_addr_tmp	),
        .EEPROM_RDATA           (eeprom_rdata      	),
        .EEPROM_WDATA           (eeprom_wdata      	),
        .EEPROM_DATA_OEJ        (rom_data_oe_      	),
        .EEPROM_ADDR_OEJ        (rom_addr_oe_tmp_   ),
        .EEPROM_ALE				(eeprom_ale			),		//	external 373 latch enable
//		.EEPROM_ALE_OEJ			(eeprom_ale_oe_		),		//  ale out enable
        .EEPROM_OEJ             (eeprom_oe_         ),
//        .EEPROM_CSJ             (eeprom_ce_         ),
        .EEPROM_WEJ             (eeprom_we_         ),
        .XXX_EEPROM_ACK         (rom_gnt            ),
        .XXX_EEPROM_RDATA       (rom_rd_data        ),
        .XXX_EEPROM_WDATA       (rom_wr_data        ),
        .XXX_EEPROM_ADDR        (rom_addr[21:2]     ),
        .XXX_EEPROM_BE          (rom_rw_be          ),
//      .bootrom_cs             (bootrom_ce         ),
        .XXX_EEPROM_REQ         (rom_req            ),
        .XXX_EEPROM_RW          (bootrom_rw         ),
//        .mips_boot_sel          (1'b0             ),
        .EEPROM_WR_EN           (eeprom_wr_en       ),
        .EEPROM_ACCESS_ENJ		(rom_en_			),
        .EEPROM_RW_CNT          (eeprom_rw_cnt      ),
        .FLASH_MODE				(flash_mode			),
        .CLK	                (mem_clk            ),
`ifdef  SCAN_EN
		.TEST_SE				(TEST_SE			),
		.TEST_SI				(TEST_SI[77]		),
		.TEST_SO				(TEST_SO[77]		),
`endif
        .RSTJ                   (rst_               )
         );

//======================== invoke Memory Interface===============
`ifdef	DMA_DEVICE1
wire			dma_ram_req		;
wire			dma_ram_rw		;
wire	[29:2]	dma_ram_addr	;
wire	[1:0]	dma_ram_rbl	    ;
wire			dma_ram_wlast	;
wire	[3:0]	dma_ram_wbe	    ;
wire	[31:0]	dma_ram_wdata	;

wire			dma_ram_ack	    ;
wire			dma_ram_err	    ;
wire			dma_ram_rrdy	;
wire			dma_ram_rlast	;
wire	[31:0]	dma_ram_rdata	;
wire			dma_ram_wrdy	;
dma_device	DMA_DEVICE1(
		.dma_ram_req		(dma_ram_req		),
		.dma_ram_rw			(dma_ram_rw			),
		.dma_ram_addr		(dma_ram_addr		),
		.dma_ram_rbl		(dma_ram_rbl		),
		.dma_ram_wlast		(dma_ram_wlast		),
        .dma_ram_wbe		(dma_ram_wbe		),
        .dma_ram_wdata		(dma_ram_wdata		),
    //From
        .dma_ram_ack		(dma_ram_ack		),
        .dma_ram_err		(dma_ram_err		),
        .dma_ram_rrdy		(dma_ram_rrdy		),
        .dma_ram_rlast		(dma_ram_rlast		),
        .dma_ram_rdata		(dma_ram_rdata		),
        .dma_ram_wrdy		(dma_ram_wrdy		),

        .mem_clk			(mem_clk			),
        .rst_				(rst_				)
        );

`else
`endif
//	`ifdef	NEW_MEM
//wire[27:0]	sv_ram_addr_tmp	=	{11'h5,sv_ram_addr[18:2]}	;
mem_intf MEM_INTF(
        //CPU interface:
       	`ifdef	CPU_DMA
       	 //From:
        .CPU_RAM_REQ            (dma_ram_req		    ),
        .CPU_RAM_RW             (dma_ram_rw				),
        .CPU_RAM_ADDR           (dma_ram_addr			),
        .CPU_RAM_RBL            (dma_ram_rbl			),
        .CPU_RAM_WLAST          (dma_ram_wlast			),
        .CPU_RAM_WBE            (dma_ram_wbe			),
        .CPU_RAM_WDATA          (dma_ram_wdata			),
        //To:
        .CPU_RAM_ACK            (dma_ram_ack		    ),
        .CPU_RAM_ERR            (dma_ram_err		    ),
        .CPU_RAM_RRDY           (dma_ram_rrdy			),
        .CPU_RAM_RLAST          (dma_ram_rlast			),
        .CPU_RAM_RDATA          (dma_ram_rdata			),
        .CPU_RAM_WRDY           (dma_ram_wrdy			),
       	`else
        //From:
        .CPU_RAM_REQ            (cpu_ram_req            ),
        .CPU_RAM_RW             (cpu_ram_rw             ),
        .CPU_RAM_ADDR           (cpu_ram_addr[29:2]     ),
        .CPU_RAM_RBL            (cpu_ram_rbl			),
        .CPU_RAM_WLAST          (cpu_ram_wlast          ),
        .CPU_RAM_WBE            (cpu_ram_wbe            ),
        .CPU_RAM_WDATA          (cpu_ram_wdata          ),
        //To:
        .CPU_RAM_ACK            (cpu_ram_ack            ),
        .CPU_RAM_ERR            (cpu_ram_err            ),
        .CPU_RAM_RRDY           (cpu_ram_rrdy           ),
        .CPU_RAM_RLAST          (cpu_ram_rlast          ),
        .CPU_RAM_RDATA          (cpu_ram_rdata          ),
        .CPU_RAM_WRDY           (cpu_ram_wrdy           ),
        `endif
        //      ---------------------------------------
        //PCI Read Buffer Interface:
        //From:
        .PCI_RAM_RREQ           (pci_ram_rreq           ),
        .PCI_RAM_RADDR          (pci_ram_raddr[29:2]    ),
        .PCI_RAM_RBL            (pci_ram_rbl[1:0]		),
        //To:
        .PCI_RAM_RACK           (pci_ram_rack           ),
        .PCI_RAM_RERR           (pci_ram_rerr           ),
        .PCI_RAM_RRDY           (pci_ram_rrdy           ),
        .PCI_RAM_RLAST          (pci_ram_rlast          ),
        .PCI_RAM_RDATA          (pci_ram_rdata          ),
        //      ---------------------------------------
        //PCI Write Buffer Interface:
        //From:
        .PCI_RAM_WREQ           (pci_ram_wreq           ),
        .PCI_RAM_WADDR          (pci_ram_waddr[29:2]    ),
        .PCI_RAM_WLAST          (pci_ram_wlast          ),
        .PCI_RAM_WBE            (pci_ram_wbe            ),
        .PCI_RAM_WDATA          (pci_ram_wdata          ),
        //To:
        .PCI_RAM_WACK           (pci_ram_wack           ),
        .PCI_RAM_WERR           (pci_ram_werr           ),
        .PCI_RAM_WRDY           (pci_ram_wrdy           ),
        //      ---------------------------------------
        //SERVO Write Buffer Interface:
        //From:
        .SER_RAM_REQ            (sv_ram_req			),
        .SER_RAM_ADDR           (sv_ram_addr		),
        .SER_RAM_RBL            (sv_ram_rbl			),
        .SER_RAM_RW             (sv_ram_rw			),
        .SER_RAM_WLAST          (sv_ram_wlast		),
        .SER_RAM_WBE   			(sv_ram_wbe			),
        .SER_RAM_WDATA          (sv_ram_wdata		),
        //To:
        .SER_RAM_ACK            (sv_ram_ack        	),
        .SER_RAM_ERR   			(sv_ram_err			),
        .SER_RAM_RRDY  			(sv_ram_rrdy		),
        .SER_RAM_RLAST          (sv_ram_rlast		),
        .SER_RAM_RDATA          (sv_ram_rdata		),
        .SER_RAM_WRDY			(sv_ram_wrdy		),
        //      ---------------------------------------
        //AUDIO Engine Interface:
        //From:
        .AUD_RAM_REQ            (aud_ram_req         ),
        .AUD_RAM_RW             (aud_ram_rw          ),
        .AUD_RAM_ADDR           (aud_ram_addr[27:0]  ),
        .AUD_RAM_RBL            (aud_ram_rbl[1:0]	 ),
        .AUD_RAM_WLAST          (aud_ram_wlast       ),
        .AUD_RAM_WBE            (aud_ram_wbe         ),
        .AUD_RAM_WDATA          (aud_ram_wdata       ),
        //to:
        .AUD_RAM_ACK            (aud_ram_ack         ),
        .AUD_RAM_ERR            (aud_ram_err         ),
        .AUD_RAM_RRDY           (aud_ram_rrdy        ),
        .AUD_RAM_RLAST          (aud_ram_rlast       ),
        .AUD_RAM_RDATA          (aud_ram_rdata       ),
        .AUD_RAM_WRDY           (aud_ram_wrdy        ),
        //      ---------------------------------------
        //Video Engine Interface:
         //From:
         `ifdef	VIDEO_DMA
        .VIDEO_RAM_REQ          (dma_ram_req		    ),
        .VIDEO_RAM_RW           (dma_ram_rw				),
        .VIDEO_RAM_ADDR         (dma_ram_addr			),
        .VIDEO_RAM_RBL          (dma_ram_rbl			),
        .VIDEO_RAM_WLAST        (dma_ram_wlast			),
        .VIDEO_RAM_WBE          (dma_ram_wbe			),
        .VIDEO_RAM_WDATA        (dma_ram_wdata			),
        //To:
        .VIDEO_RAM_ACK          (dma_ram_ack		    ),
        .VIDEO_RAM_ERR          (dma_ram_err		    ),
        .VIDEO_RAM_RRDY         (dma_ram_rrdy			),
        .VIDEO_RAM_RLAST        (dma_ram_rlast			),
        .VIDEO_RAM_RDATA        (dma_ram_rdata			),
        .VIDEO_RAM_WRDY         (dma_ram_wrdy			),
        `else
        //From:
        .VIDEO_RAM_REQ          (video_ram_req          ),
        .VIDEO_RAM_RW           (video_ram_rw           ),
        .VIDEO_RAM_ADDR         (video_ram_addr[29:2]	),
        .VIDEO_RAM_RBL          (video_ram_rbl			),
        .VIDEO_RAM_WLAST        (video_ram_wlast        ),
        .VIDEO_RAM_WBE          (video_ram_wbe          ),
        .VIDEO_RAM_WDATA        (video_ram_wdata        ),
        //To:
        .VIDEO_RAM_ACK          (video_ram_ack          ),
        .VIDEO_RAM_ERR          (video_ram_err          ),
        .VIDEO_RAM_RRDY         (video_ram_rrdy         ),
        .VIDEO_RAM_RLAST        (video_ram_rlast        ),
        .VIDEO_RAM_RDATA        (video_ram_rdata        ),
        .VIDEO_RAM_WRDY         (video_ram_wrdy         ),
         //      ---------------------------------------
         `endif
         //      ---------------------------------------
        //Video_In  Interface:
        //From:
        .VIN_RAM_REQ          	(vin_ram_req	    	),
        .VIN_RAM_RW	            (vin_ram_rw				),
        .VIN_RAM_ADDR	        (vin_ram_addr[29:2]		),
        .VIN_RAM_RBL          	(vin_ram_rbl	    	),
        .VIN_RAM_WLAST        	(vin_ram_wlast			),
        .VIN_RAM_WBE          	(vin_ram_wbe	    	),
        .VIN_RAM_WDATA        	(vin_ram_wdata			),
        //To:
        .VIN_RAM_ACK          	(vin_ram_ack	    	),
        .VIN_RAM_ERR          	(vin_ram_err	    	),
        .VIN_RAM_RRDY         	(vin_ram_rrdy			),
        .VIN_RAM_RLAST        	(vin_ram_rlast   		),
        .VIN_RAM_RDATA        	(vin_ram_rdata   		),
        .VIN_RAM_WRDY         	(vin_ram_wrdy    		),
        //      ---------------------------------------
        //      ---------------------------------------
        //DSP Interface:
        //From:
        .DSP_RAM_REQ            (dsp_ram_req            ),
        .DSP_RAM_RW             (dsp_ram_rw             ),
        .DSP_RAM_ADDR           (dsp_ram_addr[27:0]     ),
        .DSP_RAM_RBL            (dsp_ram_rbl			),
        .DSP_RAM_WLAST          (dsp_ram_wlast          ),
        .DSP_RAM_WBE            (dsp_ram_wbe            ),
        .DSP_RAM_WDATA          (dsp_ram_wdata          ),
        //To:
        .DSP_RAM_ACK            (dsp_ram_ack            ),
        .DSP_RAM_ERR            (dsp_ram_err            ),
        .DSP_RAM_RRDY           (dsp_ram_rrdy           ),
        .DSP_RAM_RLAST          (dsp_ram_rlast          ),
        .DSP_RAM_RDATA          (dsp_ram_rdata          ),
        .DSP_RAM_WRDY           (dsp_ram_wrdy           ),
        //      ---------------------------------------
		//Video Support Block Interface:
		//From:
		.VSB_RAM_REQ			(vsb_ram_req	    	),
		.VSB_RAM_RW				(vsb_ram_rw				),
		.VSB_RAM_ADDR			(vsb_ram_addr[27:0]		),
		.VSB_RAM_RBL			(vsb_ram_rbl			),
		.VSB_RAM_WLAST			(vsb_ram_wlast			),
		.VSB_RAM_WBE			(vsb_ram_wbe	    	),
		.VSB_RAM_WDATA			(vsb_ram_wdata			),
		//To:
		.VSB_RAM_ACK			(vsb_ram_ack	    	),
		.VSB_RAM_ERR			(vsb_ram_err	    	),
		.VSB_RAM_RRDY			(vsb_ram_rrdy			),
		.VSB_RAM_RLAST			(vsb_ram_rlast			),
		.VSB_RAM_RDATA			(vsb_ram_rdata			),
		.VSB_RAM_WRDY			(vsb_ram_wrdy			),
		//		-----------------------------------------
		//IDE Interface:
		//From:
		.IDE_RAM_REQ			(ide_ram_req	    	),
		.IDE_RAM_RW				(ide_ram_rw				),
		.IDE_RAM_ADDR			(ide_ram_addr[29:2]		),
		.IDE_RAM_RBL			(ide_ram_rbl			),
		.IDE_RAM_WLAST			(ide_ram_wlast			),
		.IDE_RAM_WBE			(ide_ram_wbe	    	),
		.IDE_RAM_WDATA			(ide_ram_wdata			),
		//To:
		.IDE_RAM_ACK			(ide_ram_ack	    	),
		.IDE_RAM_ERR			(ide_ram_err	    	),
		.IDE_RAM_RRDY			(ide_ram_rrdy			),
		.IDE_RAM_RLAST			(ide_ram_rlast			),
		.IDE_RAM_RDATA			(ide_ram_rdata			),
		.IDE_RAM_WRDY			(ide_ram_wrdy			),
//      --------------------------------------------
        //config interface:
        .CTRL_RAM_REQ           (ctrl_ram_req           ),
        .CTRL_RAM_CMD           (ctrl_ram_cmd           ),
        .CTRL_RAM_RST           (ctrl_ram_rst           ),
        //Auto-Refresh Rate:
        .RAM_REF_INIT           (ram_ref_init           ),
		//Mapping:
		.RAM_ROW1_ACTIVE		(ram_row1_active        ),
		.RAM_ROW0_ACTIVE		(ram_row0_active        ),
		.RAM_ROW0_BA_WIDTH		(ram_ba_width           ),
		.RAM_ROW0_RA_WIDTH		(ram_ra_width           ),
		.RAM_ROW0_CA_WIDTH		(ram_ca_width           ),
		.RAM_ROW1_BA_WIDTH		(1'b0					),
		.RAM_ROW1_RA_WIDTH		(2'b0					),
		.RAM_ROW1_CA_WIDTH		(2'b0					),
		//Timing Parameters:
        .RAM_TIMEPARAM          (ram_timeparam          ),
        .RAM_MODE_SET           (ram_mode_set           ),
        .RAM_16BITS_MODE        (ram_16bits_mode        ),
//		.RAM_PRE_OE_EN			(ram_pre_oe_en			),
		.RAM_POST_OE_EN			(ram_post_oe_en			),
       	.RAM_R2W_CTRL			(ram_r2w_ctrl			),
       	.DEVS_REQS_NUM			(devs_reqs_num			),//(22'h3f_f2ff	),
       	//for arbiter
//		.SUBA_ARBT_CTRL			(2'b0			),
//		.SUBB_ARBT_CTRL			(2'b0			),
//		.MAIN_ARBT_CTRL			(2'b0			),
		//the memory bus arbiter states:
//		.SUBA_ARBT_ST			(suba_arbt_st			),	//store the latest four states of the Sub-A Arbiter
//		.SUBB_ARBT_ST			(subb_arbt_st			),	//store the latest four states of the Sub-B Arbiter
//		.MAIN_ARBT_ST			(main_arbt_st			),	//store the latest four states of the Main Arbiter
		//for write dead lock:
		.RAM_WRLOCK_TIMER		(1'b0					),	//used to enable the write dead lock timer
		.RAM_WRLOCK_RESUME		(ram_wrlock_resume		),	//used to enable exiting from write dead lock state
//		.RAM_WRLOCK_TAG			(ram_wrlock_tag			),	//report the write dead lock error
//		.RAM_WRLOCK_ID			(ram_wrlock_id			),	//report the write dead lock device id
        //      ---------------------------------------
        //SDR SDRAM memory interface:
        .RAM_CKE                (ram_cke                ),
        .RAM_CS_                (ram_cs_                ),
        .RAM_RAS_               (ram_ras_               ),
        .RAM_CAS_               (ram_cas_               ),
        .RAM_WE_                (ram_we_                ),
        .RAM_DQM                (ram_dqm                ),
        .RAM_BA                 (ram_ba                 ),
        .RAM_ADDR               (ram_addr               ),
        .RAM_WDATA              (ram_dq_out             ),
        .RAM_RDATA              (ram_dq_in              ),
        .RAM_DQ_OE_             (ram_dq_oe_             ),
        //      ---------------------------------------
        //others:
        `ifdef	INSERT_SCAN_MEM
        .TEST_SE				(1'b0					),
        `endif
`ifdef  SCAN_EN
		.TEST_SE				(TEST_SE				),
		.TEST_SI1				(TEST_SI[3]				),
		.TEST_SI2				(TEST_SI[4]				),
		.TEST_SI3				(TEST_SI[5]				),
		.TEST_SO1				(TEST_SO[3]				),
		.TEST_SO2				(TEST_SO[4]				),
		.TEST_SO3				(TEST_SO[5]				),
`endif
        .RST_                   (rst_                   ),
        .MEM_CLK                (mem_clk                )
        );


//=================================================================

//===================invoke PCI Bus Unit===========================

p_biu   P_BIU(
        .OE_AD_                 (oe_ad_                 ),
        .OUT_AD                 (out_ad                 ),
        .OE_CBE_                (oe_cbe_                ),
        .OUT_CBE_               (out_cbe_               ),
        .OE_FRAME_              (oe_frame_              ),
        .OUT_FRAME_             (out_frame_             ),
        .OE_IRDY_               (oe_irdy_               ),
        .OUT_IRDY_              (out_irdy_              ),
        .OE_DEVSEL_             (oe_devsel_             ),
        .OUT_DEVSEL_            (out_devsel_            ),
        .OE_TRDY_               (oe_trdy_               ),
        .OUT_TRDY_              (out_trdy_              ),
        .OE_STOP_               (oe_stop_               ),
        .OUT_STOP_              (out_stop_              ),
        .OE_PERR_                (oe_perr_                ),
	    .OUT_PERR_              (out_perr_              ),
	    .OE_SERR_                (oe_serr_                ),
	    .OUT_SERR_              (out_serr_              ),
        .OE_PAR_                (oe_par_                ),
        .OUT_PAR                (out_par                ),
        .OUT_GNT_               (out_gnt_               ),

        .USB_INT_               (usb_int_               ),      // Snow 2000-04-30

// Input from External Bus
        .IN_AD                  (in_ad                  ),
        .IN_CBE_                (in_cbe_                ),
        .IN_FRAME_              (in_frame_              ),
        .IN_IRDY_               (in_irdy_               ),
        .IN_DEVSEL_             (in_devsel_             ),
        .IN_TRDY_               (in_trdy_               ),
        .IN_STOP_               (in_stop_               ),
        .IN_PAR                 (in_par                 ),
        .IN_REQ_                (in_req_                ),
        .IN_SERR_               (in_serr_               ),
        .IN_PERR_               (in_perr_               ),

// Bus Decode
        .PBIU_REQ               (pbiu_req               ),
        .PBIU_RW                (pbiu_rw                ),
        .PBIU_RD_DATA           (pbiu_rd_data           ),
        .PBIU_ADDR              (pbiu_addr              ),
        .PBIU_BL                (pbiu_bl                ),
        .PBIU_GNT               (pbiu_gnt               ),
        .PBIU_RD_RDY            (pbiu_rd_rdy            ),
        .PBIU_LAST              (pbiu_last              ),
        .PBIU_ERR               (pbiu_err               ),
        .PBIU_WR_DATA           (pbiu_wr_data           ),
        .PBIU_BE                (pbiu_be                ),
        .PBIU_WR_RDY            (pbiu_wr_rdy            ),

// Memory Interface

// PCI slave
        .PCI_RAM_RREQ           (pci_ram_rreq           ),
        .PCI_RAM_RDATA          (pci_ram_rdata          ),
        .PCI_RAM_RADDR          (pci_ram_raddr          ),
        .PCI_RAM_RBL            (pci_ram_rbl[1:0]       ),
        .PCI_RAM_RACK           (pci_ram_rack           ),
        .PCI_RAM_RRDY           (pci_ram_rrdy           ),
        .PCI_RAM_RLAST          (pci_ram_rlast          ),
        .PCI_RAM_RERR           (pci_ram_rerr           ),
// PCI Posted Write FIFO interface
        .PCI_RAM_WREQ           (pci_ram_wreq           ),
        .PCI_RAM_WDATA          (pci_ram_wdata          ),
        .PCI_RAM_WADDR          (pci_ram_waddr          ),
        .PCI_RAM_WBL            (pci_ram_wbl            ),
        .PCI_RAM_WBE            (pci_ram_wbe            ),
        .PCI_RAM_WACK           (pci_ram_wack           ),
        .PCI_RAM_WRDY           (pci_ram_wrdy           ),
        .PCI_RAM_WLAST          (pci_ram_wlast          ),
        .PCI_RAM_WERR           (pci_ram_werr           ),

        .P_CFG_ADDR             (p_cfg_addr             ),
        .P_CFG_BE               (p_cfg_be               ),
        .P_CFG_DIN              (p_cfg_din              ),
        .P_CFG_DOUT             (p_cfg_dout             ),
        .P_CFG_RW               (p_cfg_rw               ),
        .P_CFG_CS               (p_cfg_cs               ),
        .P_CFG_GNT              (p_cfg_gnt              ),

// eric000430: USB added below
        .P1_LS_MODE             (p1_ls_mode             ),      // to USB pad
        .P1_TXD                 (p1_txd                 ),
        .P1_TXD_OEJ             (p1_txd_oej             ),
        .P1_TXD_SE0             (p1_txd_se0             ),
        .P2_LS_MODE             (p2_ls_mode             ),
        .P2_TXD                 (p2_txd                 ),
        .P2_TXD_OEJ             (p2_txd_oej             ),
        .P2_TXD_SE0             (p2_txd_se0             ),
 //       .p3_ls_mode             (p3_ls_mode             ),      // to USB pad
 //       .p3_txd                 (p3_txd                 ),
 //       .p3_txd_oej             (p3_txd_oej             ),
 //       .p3_txd_se0             (p3_txd_se0             ),
 //       .p4_ls_mode             (p4_ls_mode             ),
 //       .p4_txd                 (p4_txd                 ),
 //       .p4_txd_oej             (p4_txd_oej             ),
 //       .p4_txd_se0             (p4_txd_se0             ),
        .USB_PON_               (usb_pon_               ),      // power control
        .I_P1_RXD               (i_p1_rxd               ),      // from USB pad
        .I_P1_RXD_SE0           (i_p1_rxd_se0           ),
        .I_P2_RXD               (i_p2_rxd               ),
        .I_P2_RXD_SE0           (i_p2_rxd_se0           ),
 //       .i_p3_rxd               (i_p3_rxd               ),      // from USB pad
 //       .i_p3_rxd_se0           (i_p3_rxd_se0           ),
 //       .i_p4_rxd               (i_p4_rxd               ),
 //       .i_p4_rxd_se0           (i_p4_rxd_se0           ),
        .USB_CLK                (usb_clk                ),      // 48M clock for USB, from PLL
        .USB_OVERCUR_           (usb_overcur_           ),      // over current input

//        .USB_CLK_EN             (usb_clk_en             ),      // controll USB clock
//        .USB_CLK_EN             (1'b1             ),      // Suker 2004-02-26

// eric000430: USB added above
//======================SD_MS interface========================
		.MS_IP_ENABLE   		(ms_enable    	 )    ,	//From NB register
		.MS_INS         		(ms_ins          )    ,
		.MS_SERIN_IN    		(ms_serin_in     )    ,
		.M66EN          		(m66_clk_en      )    ,	//From NB register
		.SD_IP_ENABLE   		(sd_enable    	 )    ,	//From NB register
		.SD_DATA0_IN    		(sd_data0_in     )    ,
		.SD_DATA1_IN    		(sd_data1_in     )    ,
		.SD_DATA2_IN    		(sd_data2_in     )    ,
		.SD_DATA3_IN    		(sd_data3_in     )    ,
		.SD_MMC_CMD_IN  		(sd_mmc_cmd_in   )    ,

		.MS_CLK         		(ms_clk          )    ,
		.MS_BSST        		(ms_bsst_dummy   )    ,
		.MS_PW_CTRL     		(ms_pw_ctrl      )    ,
		.MS_SEROUT      		(ms_serout       )    ,
		.MSDE           		(msde            )    ,
		.MS_ACCESS      		(ms_access       )    ,
		.SD_DATA0_OEJ   		(sd_data0_oej    )    ,
		.SD_DATA1_OEJ   		(sd_data1_oej    )    ,
		.SD_DATA2_OEJ   		(sd_data2_oej    )    ,
		.SD_DATA3_OEJ   		(sd_data3_oej    )    ,
		.SD_MMC_CMD_OEJ 		(sd_mmc_cmd_oej  )    ,
		.SD_DATA0_OUT   		(sd_data0_out    )    ,
		.SD_DATA1_OUT   		(sd_data1_out    )    ,
		.SD_DATA2_OUT   		(sd_data2_out    )    ,
		.SD_DATA3_OUT   		(sd_data3_out    )    ,
		.SD_MMC_CMD_OUT 		(sd_mmc_cmd_out  )    ,
		.SD_MMC_CLK     		(sd_mmc_clk      )    ,
		.TEST_H         		(test_h          )    ,
		.TEST_DONE      		(test_done       )    ,
		.FAIL_H         		(fail_h          )    ,
		.DET_PIN        		(det_pin         )    ,
		.WR_PRCT_PIN    		(wr_prct_pin     )    ,
		.SD_FNUM_BIT    		(sd_fnum_bit     )    ,	//From NB register
		.MS_FNUM_BIT    		(ms_fnum_bit     )    , //From NB register
		.MS_INTAOUTJ  			(ms_ctrl_int_  	 )	  ,
		.SD_INTAOUTJ            (sd_ctrl_int_    )	  ,
		.SD_LED         		(sd_led          )    ,	//To NB register
		.SWITCH_MS_SD   		(switch_ms_sd    )    ,	//To NB register

		//GI Interface
        //--system
        .ALT_BOOT		    	(alt_boot			),
//		.test_ena	        	(test_ena	    	),
		.GC_RST	        		(gc_rst_	        ),
		.PIN_RST		    	(pin_rst_			),
		.RESET			    	(gi_chip_rst_	    ),
		.GI_CLK			    	(gi_clk				),
		.GI_CLK_LOCK	    	(gi_clk_lock	    ),
		//interrupt
		.SEC_INTR				(sec_intr			),
		.SEC_ACK				(sec_ack			),
		.GI_PCI_INTR			(gi_pci_int_		),
		.GI_PCI_RST				(rst_			),
		//--serial prom
		.I2C_CLK	        	(i2c_clk	        ),
		.I2C_DIN	        	(i2c_din	        ),
		.I2C_DOUT	        	(i2c_dout	    	),
		.I2C_DOE	       	 	(i2c_doe	        ),
		//--dsic interfa
		.DI_IN		        	(di_in		    	),
		.DI_OUT		        	(di_out		    	),
		.DI_OE		        	(di_oe		    	),
		.DI_BRK_IN	        	(di_brk_in	    	),
		.DI_BRK_OUT	        	(di_brk_out	    	),
		.DI_BRK_OE	        	(di_brk_oe	    	),
		.DI_DIR		        	(di_dir		    	),
		.DI_HSTRB	        	(di_hstrb	    	),
		.DI_DSTRB	        	(di_dstrb	    	),
		.DI_ERR		        	(di_err		    	),
		.DI_COVER	        	(di_cover	    	),
		.DI_RESET				(di_stb				),

		.AIS_CLK	        	(ais_clk	        ),
		.AIS_LR		        	(ais_lr		    	),
		.AIS_D		        	(ais_d		    	),
// Others
		.PCI_MODE				(1'b0					),
		.PCI_MODE_IDE_EN		(1'b0					),
        .PINS_STOP	            (IDE_STOP				),
        .PINS_IDLE	            (IDE_IDLE				),
//        .pad_boot_config        (pad_boot_config        ),
       	.PCI_PAR_ERR            (pci_par_err            ),
        .PCI_CLK                (pci_clk        		),
        .MEM_CLK                (mem_clk        		),
        .TESTMD                 (test_mode      		),
`ifdef	INSERT_SCAN_PBIU
		.TEST_SE				(1'b0					),
`endif
`ifdef  SCAN_EN

		.TEST_SE				(TEST_SE				),
		.TEST_SI1				(TEST_USB2PBIU_USBCLK	),
		.TEST_SI2				(TEST_SI[7]				),
		.TEST_SI3				(TEST_SI[8]				),
		.TEST_SI4				(TEST_SI[9]		     	),
		.TEST_SI5				(TEST_SI[10]			),
		.TEST_SI6				(TEST_USB_PCICLKDOMAIN	),
        .TEST_SI7				(TEST_SI[11]			),
        .TEST_SI8				(TEST_SI[6]				),

		.TEST_SO1				(TEST_SO[6]				),
		.TEST_SO2				(TEST_SO[7]				),
		.TEST_SO3				(TEST_SO[8]				),
		.TEST_SO4				(TEST_SO[9]				),
		.TEST_SO5				(TEST_SO[10]			),
		.TEST_SO6				(TEST_SO[11]			),
		.TEST_SO7				(TEST_USB_PCICLKDOMAIN  ),
		.TEST_SO8				(TEST_USB2PBIU_USBCLK	),

`endif
        .RST_                   (rst_           )
        );
//===================================================================


//============================Invoke South Bridge==============================
// South Bridge
//No use now
//assign  gsi_en          = 1'b0;
//assign  subq_en         = 1'b0;
//assign  scb_en          = 1'b0;
//above no use now
`ifdef  NO_SB   //add by yuky,2002-06-08
/*
assign  sb_ram_req      = 1'b0;
assign  sb_ram_wdata    = 32'h0;
assign  sb_ram_wlast    = 1'b0;
assign  sb_ram_rw       = 1'b0;
assign  sb_ram_wbe      = 4'h0;
assign  sb_ram_addr     = 28'h0;
assign  sb_ram_rbl      = 2'h0;
*/
device_bh	SB_DEVICE_BH(
			//To Memory Interface:
			.xxx_ram_req		(sb_ram_req		),
			.xxx_ram_rw			(sb_ram_rw		),
			.xxx_ram_addr		(sb_ram_addr	),
			.xxx_ram_rbl		(sb_ram_rbl[1:0]),
			.xxx_ram_wdata		(sb_ram_wdata	),
			.xxx_ram_wbe		(sb_ram_wbe		),
			.xxx_ram_wlast		(sb_ram_wlast	),
			//From Memory Interface:
			.xxx_ram_ack		(sb_ram_ack		),
			.xxx_ram_err		(sb_ram_err		),
			.xxx_ram_rrdy		(sb_ram_rrdy	),
			.xxx_ram_rlast		(sb_ram_rlast	),
			.xxx_ram_rdata		(sb_ram_rdata	),
			.xxx_ram_wrdy		(sb_ram_wrdy	),
			//others:
			.rst_				(rst_			),
			.mem_clk			(mem_clk		)
			);
//	-------------------------
//assign  gsi_sdo         = 1'b0;
//assign  gsi_sck         = 1'b0;
//assign  subq_sqck       = 1'b0;
//assign  cd_int          = 1'b0;
//assign  gsi_int         = 1'b0;
assign  irc_int         = 1'b0;
assign  scb_int         = 1'b0;
assign	uart_int		= 1'b0;//JFR030716
//assign  micom_int       = 1'b0,
//assign  subq_int        = 1'b0;
assign  cpu_sb_rdata    = 32'h0;
//assign  m_upda_out      = 8'h00;
//assign  m_upda_oe_      = 1'b1;
//assign  m_upa           = 10'h0;
//assign  m_upcs_         = 1'b1;         //uP Host Chip Select.
//assign  m_upwr_         = 1'b1;         //uP Write strobe (Intel mode).
//assign  m_uprd_         = 1'b1;         //uP Read strobe (Intel mode).
//assign  m_uprst_        = 1'b1;         //uP Host Reset.

assign  scb_scl_out     = 1'b0,
        scb_scl_oe      = 1'b0,
        scb_sda_out     = 1'b0,
        scb_sda_oe      = 1'b0;

`else

SOUTH_BRDG SOUTH_BRDG(
/*No use in m3357 JFR030624
        .sb_ram_req     (sb_ram_req             ),
        .sb_ram_rw      (sb_ram_rw              ),
        .sb_ram_ack     (sb_ram_ack             ),
        .sb_ram_wrdy    (sb_ram_wrdy            ),
        .sb_ram_wlast   (sb_ram_wlast           ),
        .sb_ram_err     (sb_ram_err             ),
        .sb_ram_wdata   (sb_ram_wdata           ),
        .sb_ram_wbe     (sb_ram_wbe             ),
        .sb_ram_addr    (sb_ram_addr            ),
        .sb_ram_bl      (sb_ram_rbl             ),
        .sb_ram_rrdy    (sb_ram_rrdy            ),
        .sb_ram_rlast   (sb_ram_rlast           ),
        .sb_ram_rdata   (sb_ram_rdata           ),
*/
        .CPU_SB_REQ     (cpu_sb_req             ),
        .CPU_SB_ADDR    (cpu_slave_addr[15:2]   ),
        .CPU_SB_BE      (cpu_slave_be           ),
        .CPU_SB_RW      (cpu_slave_rw           ),
        .CPU_SB_WDATA   (cpu_slave_wdata        ),
        .CPU_SB_RDATA   (cpu_sb_rdata           ),
        .CPU_SB_ACK     (cpu_sb_ack             ),



// Consumer IR interface
        .IR_RX          (ir_rx                  ),



// I2C interface for Philips CD-DSP and TV encoder
        .SCB_SCL_OUT    (scb_scl_out            ),
        .SCB_SCL_OE     (scb_scl_oe             ),
        .SCB_SDA_OUT    (scb_sda_out            ),
        .SCB_SDA_OE     (scb_sda_oe             ),
        .SCB_SCL_IN     (scb_scl_in             ),
        .SCB_SDA_IN     (scb_sda_in             ),
//UART interface
		.UART_TX		(uart_tx				),
		.UART_RX		(uart_rx				),


        .IRC_INT        (irc_int                ),
        .SCB_INT        (scb_int                ),
        .UART_INT		(uart_int				),

//Remove sb gate JFR030806
//        .cir_clk_en     (cir_clk_en             ),      // Consumer IR
//        .scb_clk_en     (scb_clk_en             ),      // I2C
//        .uart_clk_en	(uart_clk_en			),
        .F48M_CLK       (usb_clk                ),
        .SB_CLK         (sb_clk                 ),
        .SYS_CLK        (mem_clk                ),
        .RSTJ           (rst_                   ),
`ifdef  SCAN_EN
		.TEST_SE		(TEST_SE				),
		.TEST_SI1		(TEST_SI[12]			),
		.TEST_SI2		(TEST_SI[13]			),
		.TEST_SI3		(TEST_SI[14]			),
		.TEST_SI4		(TEST_SI[15]			),
		.TEST_SO1		(TEST_SO[12]			),
		.TEST_SO2		(TEST_SO[13]			),
		.TEST_SO3		(TEST_SO[14]			),
		.TEST_SO4		(TEST_SO[15]			),
`endif
        .TEST_MODE      (test_mode             )
        );

`endif

//==============================Invoke DSP and Audio =================

`ifdef NO_AUDIO
/*
assign          dsp_ram_req     = 1'b0;
assign          dsp_ram_addr    = 28'h0;
assign          dsp_ram_rbl     = 2'h0;
assign          dsp_ram_rw      = 1'b0;
assign          dsp_ram_wbe     = 4'h0;
assign          dsp_ram_wdata   = 32'h0;
assign          dsp_ram_wlast   = 1'b0;
*/



//	------------------
/*
assign          dsp_int       = 1'b0;
assign          out_ac97_rst_   = 1'b1;
assign          out_ac97_sdo    = 1'b0;
assign          out_ac97_sync   = 1'b0;
*/
assign          out_i2so_data0  = 1'b0;
assign			out_i2so_data1	= 1'b0;
assign			out_i2so_data2	= 1'b0;
assign			out_i2so_data3	= 1'b0;
assign			out_i2so_data4	= 1'b0;
assign          out_i2so_lrck   = 1'b0;
assign          out_i2so_bick   = 1'b0;
assign          out_i2si_lrck   = 1'b0;
assign          out_i2si_bick   = 1'b0;
assign          i2so_ch10_en    = 1'b0;
assign          out_spdif       = 1'b0;
assign			audio_int		= 1'b0;
`else
wire    aud_ram_req_temp;
`ifdef DEBUG_AUD
aud_core_glue aud_core_glue (
        .aud_ram_req_temp  (aud_ram_req_temp     ),
        .aud_ram_req       (aud_ram_req          ),

        .cpu_audio_req     (cpu_audio_req        ),
        .cpu_slave_addr    (cpu_slave_addr       ),
        .cpu_slave_wdata   (cpu_slave_wdata      ),
        .cpu_slave_rw      (cpu_slave_rw         ),
        .sys_clk           (mem_clk              ),
        .rstj              (audio_rst_           )
);
`else
assign aud_ram_req = aud_ram_req_temp;
`endif

AUD_CORE        AUD_CORE        (
// interface from/to mem_controller.
        .AUD_RAM_REQ            (aud_ram_req_temp       ),
        .AUD_RAM_ADDR           (aud_ram_addr           ),
        .AUD_RAM_RBL            (aud_ram_rbl[1:0]       ),
        .AUD_RAM_RW             (aud_ram_rw             ),
        .AUD_RAM_WBE            (aud_ram_wbe            ),
        .AUD_RAM_WDATA          (aud_ram_wdata          ),
        .AUD_RAM_WLAST          (aud_ram_wlast          ),

        .AUD_RAM_ACK            (aud_ram_ack            ),
        .AUD_RAM_RDATA          (aud_ram_rdata          ),
        .AUD_RAM_RRDY           (aud_ram_rrdy           ),
        .AUD_RAM_WRDY           (aud_ram_wrdy           ),
        .AUD_RAM_RLAST          (aud_ram_rlast          ),
        .AUD_RAM_ERR            (aud_ram_err            ),

// interface from/to cpu.
        .CPU_AUD_REQ            (cpu_audio_req          ),
        .CPU_AUD_ADDR           (cpu_slave_addr         ),
        .CPU_AUD_BE             (cpu_slave_be           ),
        .CPU_AUD_WDATA          (cpu_slave_wdata        ),
        .CPU_AUD_RW             (cpu_slave_rw           ),

        .CPU_AUD_ACK            (cpu_audio_ack          ),
        .CPU_AUD_RDATA          (cpu_audio_rdata        ),
/*JFR030624
// pad interface
        .AC97_RST_              (out_ac97_rst_          ),
        .AC97_SDATA_OUT         (out_ac97_sdo           ),
        .AC97_SYNC              (out_ac97_sync          ),
        .AC97_SDATA_IN0         (in_ac97_sdi0           ),
        .AC97_SDATA_IN1         (in_ac97_sdi1           ),
        .AC97_CLK               (ac97_clk               ),
*/
// dsp interface
	.DSP_SLAVE_CS		(DSP_SLAVE_CS	),
	.DSP_SLAVE_WR	    (DSP_SLAVE_WR	),
	.DSP_SLAVE_ADDR	    (DSP_SLAVE_ADDR	),
	.DSP_SLAVE_WDATA	(DSP_SLAVE_WDATA),
	.DSP_SLAVE_RDATA	(AUD_DSP_RDATA),

//ADC INTF
		.IADC_MCLK	   			(iadc_mclk	   			),
		.IADC_SMTSEN   			(iadc_smtsen   			),
		.IADC_SMTSMODE 			(iadc_smtsmode 			),
		.IADC_SMTS_ERR 			(iadc_smts_err 			),
		.IADC_SMTS_END 			(iadc_smts_end 			),
		.IADC_ADCCLKP1 			(iadc_adcclkp1 			),
		.IADC_ADCCLKP2 			(iadc_adcclkp2 			),
		.IADC_TST_MOD  			(iadc_tst_mod  			),
		.IADC_AVSS       		(iadc_vs	       		),
		.IADC_AVDD       		(iadc_vd    	   		),
		.IADC_DVSS       		(iadc_dvss    	   		),	// ADC digital ground
		.IADC_DVDD       		(iadc_dvdd 				),	// ADC digital power
		.IADC_VIN_L    			(iadc_vin_l    			),
		.IADC_VIN_R    			(iadc_vin_r    			),
		.IADC_VCM				(iadc_v1p2u				),
        .IADC_ADCX2EN			(1'b0					),

        .I2S_SOUT0              (out_i2so_data0         ),
        .I2S_SOUT1              (out_i2so_data1         ),
        .I2S_SOUT2              (out_i2so_data2         ),
        .I2S_SOUT3				(out_i2so_data3			),
        .I2S_SOUT4				(out_i2so_data4			),
		.I2S_OUT_CH10_EN        (i2so_ch10_en			),
        .I2S_OUT_LRCK           (out_i2so_lrck          ),
        .I2S_OUT_BICK           (out_i2so_bick          ),
        .I2S_SIN                (in_i2si_data           ),
        .I2S_IN_LRCK            (out_i2si_lrck          ),
        .I2S_IN_BICK            (out_i2si_bick          ),

        .SLAVE_I2SI_BICK        (slave_i2si_bick          ),
        .SLAVE_I2SI_LRCK        (slave_i2si_lrck          ),
		.I2SI_MODE_SEL			(i2si_mode_sel			),

        .SPO_TXP                (out_spdif              ),
        .SPI_SIN                (in_spdif               ),
        .AUD_INTF_INT           (audio_int             	),	//modified by Danial 02/07/16

        .SPO_SOURCE_CLK         (spdif_clk              ),//spdif_clk
        .MCLK                   (in_i2so_mclk           ),
        .SYS_CLK                (mem_clk                ),
        .DSP_CLK                (dsp_clk                ),
		.CVBS_CLK				(cvbs_clk			),
        `ifdef	INSERT_SCAN_AUD
        .TEST_SE				(1'b0					),
        `endif

`ifdef  SCAN_EN
		.TEST_SE				(TEST_SE				),
		.TEST_SI1				(TEST_SI[26]			),
		.TEST_SI2				(TEST_SI[27]			),
		.TEST_SI3				(TEST_SI[28]			),
		.TEST_SI4				(TEST_SI[29]			),
		.TEST_SI5				(TEST_SI[30]			),
		.TEST_SI6				(TEST_SI[31]			),
		.TEST_SI7				(TEST_SI[32]			),
		.TEST_SO1				(TEST_SO[26]			),
		.TEST_SO2				(TEST_SO[27]			),
		.TEST_SO3				(TEST_SO[28]			),
		.TEST_SO4				(TEST_SO[29]			),
		.TEST_SO5				(TEST_SO[30]			),
		.TEST_SO6				(TEST_SO[31]			),
		.TEST_SO7				(TEST_SO[32]			),
`endif
        .RSTJ                   (audio_rst_       	    ),
    	.TESTMD                 (test_mode              )
);      // aud_core
`endif
//=============================Audio access memory behave module==
`ifdef NO_AUDIO

device_bh	AUDIO_DEVICE_BH(
			//To Memory Interface:
			.xxx_ram_req			(aud_ram_req        ),
			.xxx_ram_rw				(aud_ram_rw         ),
			.xxx_ram_addr			(aud_ram_addr		),
			.xxx_ram_rbl			(aud_ram_rbl[1:0]	),
			.xxx_ram_wdata			(aud_ram_wlast      ),
			.xxx_ram_wbe			(aud_ram_wbe        ),
			.xxx_ram_wlast			(aud_ram_wdata      ),
			//From Memory Interface:
			.xxx_ram_ack			(aud_ram_ack        ),
			.xxx_ram_err			(aud_ram_err        ),
			.xxx_ram_rrdy			(aud_ram_rrdy       ),
			.xxx_ram_rlast			(aud_ram_rlast      ),
			.xxx_ram_rdata			(aud_ram_rdata      ),
			.xxx_ram_wrdy			(aud_ram_wrdy       ),
			//others:
			.rst_					(rst_				),
			.mem_clk				(mem_clk			)
			);

`else
`endif
//=============================Invoke DSP
`ifdef NO_DSP
assign          dsp_int       = 1'b0;
assign	DSP_SLAVE_CS	= 0;
assign	DSP_SLAVE_WR	= 0;
assign	DSP_SLAVE_ADDR	= 0;
assign	DSP_SLAVE_WDATA	= 0;


device_bh	DSP_DEVICE_BH(
			//To Memory Interface:
			.xxx_ram_req		(dsp_ram_req	),
			.xxx_ram_rw			(dsp_ram_rw		),
			.xxx_ram_addr		(dsp_ram_addr	),
			.xxx_ram_rbl		(dsp_ram_rbl[1:0]),
			.xxx_ram_wdata		(dsp_ram_wdata	),
			.xxx_ram_wbe		(dsp_ram_wbe	),
			.xxx_ram_wlast		(dsp_ram_wlast	),
			//From Memory Interface:
			.xxx_ram_ack		(dsp_ram_ack	),
			.xxx_ram_err		(dsp_ram_err	),
			.xxx_ram_rrdy		(dsp_ram_rrdy	),
			.xxx_ram_rlast		(dsp_ram_rlast	),
			.xxx_ram_rdata		(dsp_ram_rdata	),
			.xxx_ram_wrdy		(dsp_ram_wrdy	),
			//others:
			.rst_				(rst_			),
			.mem_clk			(mem_clk		)
			);

`else
DSP_IDMA_CORE DSP_IDMA_CORE(
// interface from/to mem_controller.
	.DSP_RAM_REQ		(dsp_ram_req	),
	.DSP_RAM_ADDR    (dsp_ram_addr   ),
	.DSP_RAM_BL      (dsp_ram_rbl[1:0]),
	.DSP_RAM_RW      (dsp_ram_rw     ),
	.DSP_RAM_WBE     (dsp_ram_wbe    ),
	.DSP_RAM_WDATA   (dsp_ram_wdata  ),
	.DSP_RAM_WLAST   (dsp_ram_wlast  ),

	.DSP_RAM_ACK     (dsp_ram_ack    ),
	.DSP_RAM_RDATA   (dsp_ram_rdata  ),
	.DSP_RAM_RRDY    (dsp_ram_rrdy   ),
	.DSP_RAM_WRDY    (dsp_ram_wrdy   ),
	.DSP_RAM_LAST    (dsp_ram_rlast  ),
	.DSP_RAM_ERR     (dsp_ram_err    ),

// interface from/to cpu.
	.DSP_INT	 	 (dsp_int		  ),
	.CPU_DSP_REQ	 (cpu_dsp_req     ),
	.CPU_DSP_ADDR    (cpu_slave_addr  ),
	.CPU_DSP_BE      (cpu_slave_be    ),
	.CPU_DSP_WDATA   (cpu_slave_wdata ),
	.CPU_DSP_RW      (cpu_slave_rw    ),

	.CPU_DSP_RDATA   (cpu_dsp_rdata ),
	.CPU_DSP_ACK     (cpu_dsp_ack   ),
// bist inerface
	.DSP_BIST_FINISH (dsp_bist_finish	),
	.DSP_BIST_ERR_VEC(dsp_bist_err_vec	),
	.DSP_BIST_TRI	 (dsp_bist_mode		),//JFR 030805

	.DSP_SLAVE_CS		(DSP_SLAVE_CS	),
	.DSP_SLAVE_WR	    (DSP_SLAVE_WR	),
	.DSP_SLAVE_ADDR	    (DSP_SLAVE_ADDR	),
	.DSP_SLAVE_WDATA	(DSP_SLAVE_WDATA),
	.AUD_DSP_RDATA	    (AUD_DSP_RDATA),

	//.pm_tdone		(),
	//.pm_tfail		(),
	//.dmx_tdone		(),
	//.dmx_tfail		(),
	//.dmy_tdone		(),
	//.dmy_tfail		(),

	//.pm_test_pin	(1'b0),
	//.dmx_test_pin	(1'b0),
	//.dmy_test_pin	(1'b0),
        `ifdef	INSERT_SCAN_DSP
        .TEST_SE				(1'b0					),
        `endif

`ifdef  SCAN_EN
		.TEST_SE				(TEST_SE				),
		.TEST_SI1				(TEST_SI[16]			),
		.TEST_SI2				(TEST_SI[17]			),
		.TEST_SI3				(TEST_SI[18]			),
		.TEST_SI4				(TEST_SI[19]			),
		.TEST_SI5				(TEST_SI[20]			),
		.TEST_SI6				(TEST_SI[21]			),
		.TEST_SI7				(TEST_SI[22]			),
		.TEST_SO1				(TEST_SO[16]			),
		.TEST_SO2				(TEST_SO[17]			),
		.TEST_SO3				(TEST_SO[18]			),
		.TEST_SO4				(TEST_SO[19]			),
		.TEST_SO5				(TEST_SO[20]			),
		.TEST_SO6				(TEST_SO[21]			),
		.TEST_SO7				(TEST_SO[22]			),
`endif

	.DSP_CLK_EN      (dsp_clk_en     ),
	.MEM_CLK         (mem_clk        ),
	.DSP_CLK         (dsp_clk        ),
	.DSP_RST_        (dsp_rst_       ),
	.ANALOGT_MODE    (1'b0 ),
	.TESTMD          (test_mode         )
	);
`endif


//====================== Invoke Video ==============
`ifdef  NO_VIDEO
//Video Engine:

device_bh	VIDEO_DEVICE_BH(
			//To Memory Interface:
			.xxx_ram_req		(video_ram_req		),
			.xxx_ram_rw			(video_ram_rw		),
			.xxx_ram_addr		(video_ram_addr		),
			.xxx_ram_rbl		(video_ram_rbl[1:0]	),
			.xxx_ram_wdata		(video_ram_wdata	),
			.xxx_ram_wbe		(video_ram_wbe		),
			.xxx_ram_wlast		(video_ram_wlast	),
			//From Memory Interface:
			.xxx_ram_ack		(video_ram_ack		),
			.xxx_ram_err		(video_ram_err		),
			.xxx_ram_rrdy		(video_ram_rrdy		),
			.xxx_ram_rlast		(video_ram_rlast	),
			.xxx_ram_rdata		(video_ram_rdata	),
			.xxx_ram_wrdy		(video_ram_wrdy		),
			//others:
			.rst_				(rst_				),
			.mem_clk			(mem_clk			)
			);

device_bh	VSB_DEVICE_BH(
			//To Memory Interface:
			.xxx_ram_req		(vsb_ram_req	),
			.xxx_ram_rw			(vsb_ram_rw		),
			.xxx_ram_addr		(vsb_ram_addr	),
			.xxx_ram_rbl		(vsb_ram_rbl[1:0]	),
			.xxx_ram_wdata		(vsb_ram_wdata	),
			.xxx_ram_wbe		(vsb_ram_wbe	),
			.xxx_ram_wlast		(vsb_ram_wlast	),
			//From Memory Interface:
			.xxx_ram_ack		(vsb_ram_ack	),
			.xxx_ram_err		(vsb_ram_err	),
			.xxx_ram_rrdy		(vsb_ram_rrdy	),
			.xxx_ram_rlast		(vsb_ram_rlast	),
			.xxx_ram_rdata		(vsb_ram_rdata	),
			.xxx_ram_wrdy		(vsb_ram_wrdy	),
			//others:
			.rst_				(rst_			),
			.mem_clk			(mem_clk		)
			);

device_bh	VIN_DEVICE_BH(
			//To Memory Interface:
			.xxx_ram_req		(vin_ram_req		),
			.xxx_ram_rw			(vin_ram_rw			),
			.xxx_ram_addr		(vin_ram_addr		),
			.xxx_ram_rbl		(vin_ram_rbl[1:0]	),
			.xxx_ram_wdata		(vin_ram_wdata		),
			.xxx_ram_wbe		(vin_ram_wbe		),
			.xxx_ram_wlast		(vin_ram_wlast		),
			//From Memory Interface:
			.xxx_ram_ack		(vin_ram_ack		),
			.xxx_ram_err		(vin_ram_err		),
			.xxx_ram_rrdy		(vin_ram_rrdy		),
			.xxx_ram_rlast		(vin_ram_rlast		),
			.xxx_ram_rdata		(vin_ram_rdata		),
			.xxx_ram_wrdy		(vin_ram_wrdy		),
			//others:
			.rst_				(rst_				),
			.mem_clk			(mem_clk			)
			);
`else

// Snow Yi, 2003.08.28
wire			TVENC_P_HSYNC, TVENC_P_VSYNC;

//				TVENC_I_HSYNC, TVENC_I_VSYNC;	replaced by the h_sync_out_ and v_sync_out_

wire	[7:0]	TVENC_P_Y, TVENC_P_U, TVENC_P_V,
				TVENC_I_Y, TVENC_I_U, TVENC_I_V;
wire	[7:0]	VIDEO_PIXEL_OUT4221;
wire	[11:0]	TVENC_VDI6VDAC, TVENC_VDO6VDAC;

// Snow Yi, 2003.08.27
VIDEO_CORE    VIDEO_CORE (  // IO Interface
//VIDEO BIST
				.VIDEO_BIST_END		(video_bist_finish	),
				.VIDEO_BIST_STATUS	(video_bist_vec		),
				.VIDEO_BIST_EN		(video_bist_tri		),
				.TEST_MODE			(test_mode			),
//TO TV ENCODER
				.VIDEO_PIXEL_OUTY0	(TVENC_P_Y		),
				.VIDEO_PIXEL_OUTCB0	(TVENC_P_U		),
				.VIDEO_PIXEL_OUTCR0	(TVENC_P_V		),
//				.VIDEO_PIXEL_OUT4220(				),		// no 422 PSCAN output

				.VIDEO_PIXEL_OEJ0	(				),
				.VIDEO_HSYNC_OUTJ0	(TVENC_P_HSYNC	),
				.VIDEO_HSYNC_OEJ0	(				),
				.VIDEO_VSYNC_OUTJ0	(TVENC_P_VSYNC	),
				.VIDEO_VSYNC_OEJ0	(				),
			//---------------------- -------------------
				.VIDEO_PIXEL_OUTY1	(TVENC_I_Y		),
				.VIDEO_PIXEL_OUTCB1	(TVENC_I_U		),
				.VIDEO_PIXEL_OUTCR1	(TVENC_I_V		),
//				.VIDEO_PIXEL_OUT4221(vdata_out[7:0]	),	// BT656 to external TVENCODER

				.VIDEO_PIXEL_OEJ1	(vdata_oe_		),
				.VIDEO_HSYNC_OUTJ1	(h_sync_out_	),
				.VIDEO_HSYNC_OEJ1	(h_sync_oe_		),
				.VIDEO_VSYNC_OUTJ1	(v_sync_out_	),
				.VIDEO_VSYNC_OEJ1	(v_sync_oe_		),

				.DVI_OUT_RESETJ		(DVI_OUT_RESETJ	),

// Snow Yi, 2003.12.16 	// begin
				.VIDEO_PIXEL_OUT422	(vdata_out[7:0]	),
				.OUT_MODE_ETV_SEL	(out_mode_etv_sel),
				.TV_MODE			(tv_mode		),
				.VGA_800X600_MODE	(svga_mode_en	),
// Snow Yi, 2003.12.16	// end

//INTERNAL LOCAL IO BUS              IO BUS
				.CPU_VIDEO_REQ		(cpu_video_req		),
				.CPU_VIDEO_RW		(cpu_slave_rw		),
				.CPU_VIDEO_ADDR		(cpu_slave_addr[9:2]),
				.CPU_VIDEO_BE		(cpu_slave_be		),
				.CPU_VIDEO_WDATA	(cpu_slave_wdata	),
				.CPU_VIDEO_ACK		(cpu_video_ack		),
				.CPU_VIDEO_RDATA	(cpu_video_rdata	),
				.CPU_VIDEO_INT		(video_int			),//JFR031031

//INTERNAL MEMORY BUS                 BUS
				.VIDEO_RAM_REQ		(video_ram_req		),
				.VIDEO_RAM_RW		(video_ram_rw		),
				.VIDEO_RAM_ADDR		(video_ram_addr		),
				.VIDEO_RAM_ACK		(video_ram_ack		),
				.VIDEO_RAM_BL		(video_ram_rbl		), // ? Snow Yi
				.VIDEO_RAM_RRDY		(video_ram_rrdy		),
				.VIDEO_RAM_RDATA	(video_ram_rdata	),
				.VIDEO_RAM_RLAST	(video_ram_rlast	),
				.VIDEO_RAM_WBE		(video_ram_wbe		),
				.VIDEO_RAM_WRDY		(video_ram_wrdy		),
				.VIDEO_RAM_WDATA	(video_ram_wdata	),
				.VIDEO_RAM_WLAST	(video_ram_wlast	),
`ifdef VIDEO_GSIM
				.TEST_SE			(1'B0				),
`else
`endif

`ifdef  SCAN_EN
				.TEST_SE			(TEST_SE			),
				.TEST_SI1			(TEST_SI[46]		),
				.TEST_SI2			(TEST_SI[47]		),
				.TEST_SI3			(TEST_SI[48]		),
				.TEST_SI4			(TEST_SI[49]		),
				.TEST_SI5			(TEST_SI[50]		),
				.TEST_SI6			(TEST_SI[51]		),
				.TEST_SI7			(TEST_SI[52]		),
				.TEST_SI8			(TEST_SI[53]		),
				.TEST_SI9			(TEST_SI[54]		),
				.TEST_SI10			(TEST_SI[55]		),
				.TEST_SI11			(TEST_SI[56]		),
				.TEST_SI12			(TEST_SI[57]		),
				.TEST_SI13			(TEST_SI[58]		),
				.TEST_SI14			(TEST_SI[59]		),
				.TEST_SI15			(TEST_SI[60]		),
				.TEST_SI16			(TEST_SI[61]		),
				.TEST_SI17			(TEST_SI[62]		),
				.TEST_SI18			(TEST_SI[63]		),
				.TEST_SI19			(TEST_SI[64]		),
				.TEST_SI20			(TEST_SI[65]		),
				.TEST_SI21			(TEST_SI[66]		),
				.TEST_SI22			(TEST_SI[67]		),
				.TEST_SI23			(TEST_SI[68]		),
				.TEST_SI24			(TEST_SI[69]		),
				.TEST_SI25			(TEST_SI[70]		),
				.TEST_SI26			(TEST_SI[71]		),
				.TEST_SI27			(TEST_SI[72]		),
				.TEST_SI28			(TEST_SI[73]		),
				.TEST_SI29			(TEST_SI[74]		),
				.TEST_SI30			(TEST_SI[75]		),

				.TEST_SO1			(TEST_SO[46]		),
				.TEST_SO2			(TEST_SO[47]		),
				.TEST_SO3			(TEST_SO[48]		),
				.TEST_SO4			(TEST_SO[49]		),
				.TEST_SO5			(TEST_SO[50]		),
				.TEST_SO6			(TEST_SO[51]		),
				.TEST_SO7			(TEST_SO[52]		),
				.TEST_SO8			(TEST_SO[53]		),
				.TEST_SO9			(TEST_SO[54]		),
				.TEST_SO10			(TEST_SO[55]		),
				.TEST_SO11			(TEST_SO[56]		),
				.TEST_SO12			(TEST_SO[57]		),
				.TEST_SO13			(TEST_SO[58]		),
				.TEST_SO14			(TEST_SO[59]		),
				.TEST_SO15			(TEST_SO[60]		),
				.TEST_SO16			(TEST_SO[61]		),
				.TEST_SO17			(TEST_SO[62]		),
				.TEST_SO18			(TEST_SO[63]		),
				.TEST_SO19			(TEST_SO[64]		),
				.TEST_SO20			(TEST_SO[65]		),
				.TEST_SO21			(TEST_SO[66]		),
				.TEST_SO22			(TEST_SO[67]		),
				.TEST_SO23			(TEST_SO[68]		),
				.TEST_SO24			(TEST_SO[69]		),
				.TEST_SO25			(TEST_SO[70]		),
				.TEST_SO26			(TEST_SO[71]		),
				.TEST_SO27			(TEST_SO[72]		),
				.TEST_SO28			(TEST_SO[73]		),
				.TEST_SO29			(TEST_SO[74]		),
				.TEST_SO30			(TEST_SO[75]		),
`endif

//				.DVI_BUF0_CLK		(dvi_buf0_clk		),
//				.DVI_BUF1_CLK		(dvi_buf1_clk		),
//				.DVI_RWBUF_SEL		(dvi_rwbuf_sel		),
				.INTER_ONLY			(inter_only			),  // output to chip
				.DVI_BUF2754_CLK	(cav_clk			),
				.MEM_CLK			(mem_clk			),
				.VIDEO_CLK			(video_clk			),
				.VIDEO_CLK_EN		(ve_clk_en			),	// SNOW 2003-11-07
				.F27M_CLK			(cvbs_clk			),
				.F54M_CLK			(cvbs2x_clk			),
				.VIDEO_VDEC_RSTJ	(vdec_rst_			), 	// Snow 2003.11.20
				.VIDEO_RSTJ			(video_rst_			)
				);

// Snow Yi, 2003.08.27
`ifdef VSB_GSIM
VSB_ENGINE    VSB_ENGINE (
// IO Interface
				.VSB_CS			(cpu_vsb_req	),
				.VSB_RW			(cpu_slave_rw	),			// 1: write, 0: read
				.VSB_ADDR		(cpu_slave_addr[8:2]),			// IO address
				.VSB_WBE		(cpu_slave_be	),			// IO write byte enable
				.VSB_DIN		(cpu_slave_wdata),			// IO write data
				.VSB_DOUT		(cpu_vsb_rdata	),			// IO read data
				.VSB_ACK		(cpu_vsb_ack	),			// IO data ready for Write/Read
/*CN20031209
// MEM Interface
				.VSB_RAM_REQ	(vsb_ram_req	),		// request to memory controller
				.VSB_RAM_RW		(vsb_ram_rw		),		// 1: write, 0: read
				.VSB_RAM_ADDR	(vsb_ram_addr	),		// requeat address
				.VSB_RAM_BL		(vsb_ram_rbl	),		// read burst length
				.VSB_RAM_ACK	(vsb_ram_ack	),		// acknowledge to request
				.VSB_RAM_RRDY	(vsb_ram_rrdy	),		// read ready
				.VSB_RAM_RDATA	(vsb_ram_rdata	),		// read data
				.VSB_RAM_RLAST	(vsb_ram_rlast	),		// read last
				.VSB_RAM_WRDY	(vsb_ram_wrdy	),		// write ready
				.VSB_RAM_WBE	(vsb_ram_wbe	),		// write byte enable
				.VSB_RAM_WDATA	(vsb_ram_wdata	),		// write data
				.VSB_RAM_WLAST	(vsb_ram_wlast	),		// write last
*/
// MEM Interface
				.VCPPM_RAM_REQ	(vsb_ram_req	),		// request to memory controller
				.VCPPM_RAM_RW		(vsb_ram_rw		),		// 1: write, 0: read
				.VCPPM_RAM_ADDR	(vsb_ram_addr	),		// requeat address
				.VCPPM_RAM_BL		(vsb_ram_rbl	),		// read burst length
				.VCPPM_RAM_ACK	(vsb_ram_ack	),		// acknowledge to request
				.VCPPM_RAM_RRDY	(vsb_ram_rrdy	),		// read ready
				.VCPPM_RAM_RDATA	(vsb_ram_rdata	),		// read data
				.VCPPM_RAM_RLAST	(vsb_ram_rlast	),		// read last
				.VCPPM_RAM_WRDY	(vsb_ram_wrdy	),		// write ready
				.VCPPM_RAM_WBE	(vsb_ram_wbe	),		// write byte enable
				.VCPPM_RAM_WDATA	(vsb_ram_wdata	),		// write data
				.VCPPM_RAM_WLAST	(vsb_ram_wlast	),		// write last

				.VSB_INT		(decss_int		),

`else
VSB_ENGINE	VSB_ENGINE (
// IO Interface
				.VSB_CS			(cpu_vsb_req	),
				.VSB_RW			(cpu_slave_rw	),			// 1: write, 0: read
				.VSB_ADDR		(cpu_slave_addr[8:2]),			// IO address
				.VSB_WBE		(cpu_slave_be	),			// IO write byte enable
				.VSB_DIN		(cpu_slave_wdata),			// IO write data
				.VSB_DOUT		(cpu_vsb_rdata	),			// IO read data
				.VSB_ACK		(cpu_vsb_ack	),			// IO data ready for Write/Read
// MEM Interface
				.VCPPM_RAM_REQ	(vsb_ram_req	),		// request to memory controller
				.VCPPM_RAM_RW	(vsb_ram_rw		),		// 1: write, 0: read
				.VCPPM_RAM_ADDR	(vsb_ram_addr	),		// requeat address
				.VCPPM_RAM_BL	(vsb_ram_rbl	),		// read burst length
				.VCPPM_RAM_ACK	(vsb_ram_ack	),		// acknowledge to request
				.VCPPM_RAM_RRDY	(vsb_ram_rrdy	),		// read ready
				.VCPPM_RAM_RDATA(vsb_ram_rdata	),		// read data
				.VCPPM_RAM_RLAST(vsb_ram_rlast	),		// read last
				.VCPPM_RAM_WRDY	(vsb_ram_wrdy	),		// write ready
				.VCPPM_RAM_WBE	(vsb_ram_wbe	),		// write byte enable
				.VCPPM_RAM_WDATA(vsb_ram_wdata	),		// write data
				.VCPPM_RAM_WLAST(vsb_ram_wlast	),		// write last

				.VSB_INT		(decss_int		),
`endif

`ifdef  SCAN_EN
				.TEST_SE		(TEST_SE				),
				.TEST_SI1		(TEST_SI[33]			),
				.TEST_SI2		(TEST_SI[34]			),
				.TEST_SI3		(TEST_SI[35]			),
				.TEST_SO1		(TEST_SO[33]			),
				.TEST_SO2		(TEST_SO[34]			),
				.TEST_SO3		(TEST_SO[35]			),
`endif

`ifdef VSB_GSIM
				.TEST_SE		(1'b0			),			// Snow Yi, 2003.10.21
				.TEST_SI1		(1'b0			),
				.TEST_SI2		(1'b0			),
				.TEST_SI3		(1'b0			),
// system
//				.VSB_CLK_EN		(1'b1			),			// clock enable to total IP core
				.TEST_MODE		(test_mode		),
				.RSTJ			(vdec_rst_		),
				.VSB_CLK		(vsb_clk		),
				.MEM_CLK		(mem_clk		)			// video clock form PLL
`else
// system
//				.VSB_CLK_EN		(1'b1			),			// clock enable to total IP core
				.TEST_MODE		(test_mode		),
				.RSTJ			(vdec_rst_		),			// Snow Yi, 2003.11.20
				.VSB_CLK		(vsb_clk		),
				.MEM_CLK		(mem_clk		)			// video clock form PLL
`endif
				);

// Snow Yi, 2003.08.27
TVENC_INTF TVENC_INTF (
				.IO_HOST_REQ	(cpu_tvenc_req	),
				.IO_HOST_RW		(cpu_slave_rw	),
				.IO_HOST_ADDR	(cpu_slave_addr[7:2]),
				.IO_HOST_BE		(cpu_slave_be	),
				.IO_HOST_DIN	(cpu_slave_wdata),
			   	.IO_SLAVE_RST_	(tvenc_rst_		),

				.F54M_CLK		(tv_cvbs2x_clk	),		// 54MHZ CLOCK, "TVECLK"
				.F54M_CLK_EN	(tvenc_clk_en	),		// SNOW 2003-11-07
				.TEST_MODE		(test_mode		),		//JFR 031010

			   	.IO_HOST_CLK	(mem_clk		),		// 144 MHz, same as MEMORY clock
				.CVBS_CLK		(tv_cvbs_clk	),		// 27 MHz
				.CAV_CLK		(tv_cav_clk		),		// 27 or 54 MHz (I or P)
				.F108M_CLK		(tv_f108m_clk	),		// 108 MHz
				.CVBS2X_CLK		(tv_cvbs2x_clk	),		// 2*CVBS
				.CAV2X_CLK		(tv_cav2x_clk	),		// 2*CAV

				.TVENC_I_HSYNC	(h_sync_out_	),
				.TVENC_I_U		(TVENC_I_U		),
				.TVENC_I_V		(TVENC_I_V		),
				.TVENC_I_VSYNC	(v_sync_out_	),
				.TVENC_I_Y		(TVENC_I_Y		),

				.TVENC_P_HSYNC	(TVENC_P_HSYNC	),
				.TVENC_P_U		(TVENC_P_U		),
				.TVENC_P_V		(TVENC_P_V		),
				.TVENC_P_VSYNC	(TVENC_P_VSYNC	),
				.TVENC_P_Y		(TVENC_P_Y		),

				.TVENC_IOUT1	(tvenc_iout1	),
				.TVENC_IOUT2	(tvenc_iout2	),
				.TVENC_IOUT3	(tvenc_iout3	),
				.TVENC_IOUT4	(tvenc_iout4	),
				.TVENC_IOUT5	(tvenc_iout5	),
				.TVENC_IOUT6	(tvenc_iout6	),
				.TVENC_IOUTB	(tvenc_idump	),	// Dump current output, triple bonding
				.TVENC_TV_HSYNC	(tvenc_h_sync_	),	// Horizontal sync for VGA mode
				.TVENC_TV_VSYNC	(tvenc_v_sync_	),	// Vertical sync for VGA mode

				.TVENC_REXT		(tvenc_iext		),	// 1 pin, single bonding
				.TVENC_REXT1	(tvenc_iext1	),	// 470 Ohms register b/t this pin to analog GND
				.TVENC_IREF1	(tvenc_iref1	),	// 0.1 uF from this pin to analog VDD

				.TVENC_VDI6VDAC	(tvenc_vdi4vdac	),	// 12-bit data for checking DAC
				.TVENC_VDO6VDAC	(tvenc_vdo4vdac	),	// 12-bit data for checking DAC

				.TVENC_GNDD		(				),	// digital ground for TV encoder
				.TVENC_GNDA		(tvenc_gnda		),	// analog ground for DAC
				.TVENC_GNDA1	(tvenc_gnda1	),	// analog ground for DAC
				.TVENC_VDD18D	(				),	// 1.8v digital power for TV encoder
				.TVENC_VD33D	(				),	// 3.3v digital power for TV encoder
				.TVENC_VD33A	(tvenc_vd33a	),	// 3.3v analog power for DAC
				.TVENC_VSUD		(tvenc_vsud		),	// Gound for DAC
// snow 2003.12.10
//				.TVENC_IREF2	(tvenc_iref2	),
				.TEST_SE		(1'b0			),
//				.TEST_SI		(1'b0			),
//				.TEST_SO		(),
				.TEST_SI_0      (1'b0			),
				.TEST_SO_0      (),
				.TEST_SI_1      (1'b0			),
				.TEST_SO_1      (),
				.TEST_SI_2      (1'b0			),
				.TEST_SO_2      (),
				.TEST_SI_3      (1'b0			),
				.TEST_SO_3      (),
				.TEST_SI_4      (1'b0			),
				.TEST_SO_4      (),
				.TEST_SI_5      (1'b0			),
				.TEST_SO_5      (),
				.TEST_SI_6      (1'b0			),
				.TEST_SO_6      (),

`ifdef  SCAN_EN
				.TEST_SE0		(TEST_SE		),
				.TEST_SE		(TEST_SE		),
				.TEST_SI		(TEST_SI[38]	),
				.TEST_SO		(TEST_SO[38]	),
				.TEST_SI_0      (TEST_SI[39]	),
				.TEST_SO_0      (TEST_SO[39]	),
				.TEST_SI_1      (TEST_SI[40]	),
				.TEST_SO_1      (TEST_SO[40]	),
				.TEST_SI_2      (TEST_SI[41]	),
				.TEST_SO_2      (TEST_SO[41]	),
				.TEST_SI_3      (TEST_SI[42]	),
				.TEST_SO_3      (TEST_SO[42]	),
				.TEST_SI_4      (TEST_SI[43]	),
				.TEST_SO_4      (TEST_SO[43]	),
				.TEST_SI_5      (TEST_SI[44]	),
				.TEST_SO_5      (TEST_SO[44]	),
				.TEST_SI_6      (TEST_SI[45]	),
				.TEST_SO_6      (TEST_SO[45]	),
`endif
			   	.S2H_ACK		(cpu_tvenc_ack	),
			   	.S2H_DOUT		(cpu_tvenc_rdata)
			   	);



//// Snow Yi, 2003.11.11 ////
// VIDEO_IN
VIDEO_IN	VIDEO_IN (
//From TV decoder
				.VIN_PIXEL_DATA	(VIN_PIXEL_DATA	),		// pixel data from external TV decoder
				.VIN_PIXEL_CLK	(VIN_PIXEL_CLK	),		// pixel clock from external (27MHz)
				.VIN_H_SYNCJ	(VIN_H_SYNCJ	),		// HORIZONTAL SYNC
				.VIN_V_SYNCJ	(VIN_V_SYNCJ	),		// VERTICAL SYNC
//				.VIN_OUT_RSTJ	(VIN_OUT_RSTJ	),		// reset to external TV device (?)

//INTERNAL LOCAL IO BUS
				.CPU_VIN_REQ	(cpu_video_in_req	),		// CPU to VIDEO_IN request
				.CPU_VIN_RW		(cpu_slave_rw		),		// CPU to VIDEO_IN command type
				.CPU_VIN_ADDR	(cpu_slave_addr[9:2]),		// CPU to VIDEO_IN command address
				.CPU_VIN_BE		(cpu_slave_be		),		// CPU to VIDEO_IN command byte enable
				.CPU_VIN_WDATA	(cpu_slave_wdata	),		// CPU to VIDEO_IN command data
				.CPU_VIN_ACK	(cpu_video_in_ack	),		// VIDEO_IN to CPU command acknowledge
				.CPU_VIN_RDATA	(cpu_video_in_rdata	),		// VIDEO_IN to CPU read back data
				.CPU_VIN_INT	(video_in_int		), 		// VIDEO_IN to CPU interrupt

//INTERNAL MEMORY BUS
				.VIN_RAM_REQ	(vin_ram_req	    ),		// VIDEO_IN to MEM request
				.VIN_RAM_RW		(vin_ram_rw			),		// VIDEO_IN to MEM command, hardwired to "1" for WRITE
				.VIN_RAM_ADDR	(vin_ram_addr		),		// VIDEO_IN to MEM address
				.VIN_RAM_ACK	(vin_ram_ack	    ),		// MEM to VIDEO_IN request acknowledged
				.VIN_RAM_BL		(vin_ram_rbl		),		// VIDEO_IN to MEM burst length
				.VIN_RAM_WBE	(vin_ram_wbe	    ),		// VIDEO_IN to MEM write byte enable
				.VIN_RAM_WRDY	(vin_ram_wrdy		),		// MEM to VIDEO_IN write ready
				.VIN_RAM_WDATA	(vin_ram_wdata		),		// VIDEO_IN to MEM write data
				.VIN_RAM_WLAST	(vin_ram_wlast		),		// VIDEO_IN to MEM write last flag

`ifdef SCAN_EN
				.TEST_SE		(TEST_SE			),
				.TEST_SI1       (TEST_SI[23]	 	),
				.TEST_SI2       (TEST_SI[24]	 	),
				.TEST_SI3       (TEST_SI[25]	 	),
				.TEST_SO1       (TEST_SO[23]	 	),
				.TEST_SO2       (TEST_SO[24]	 	),
				.TEST_SO3       (TEST_SO[25]	 	),
`endif

`ifdef VIN_GSIM
				.TEST_SE		(1'b0				),
`endif
// system signals
				.TEST_MODE      (test_mode      ),      // add test_mode to control async rst
				.DVB_SPI_EN		(dvb_spi_en		),		// SNOWYI 2003.12.01
				.MEM_CLK		(mem_clk		),		// CPU and MEM interface clock
				.VIN_RSTJ		(vin_rst_		)		// global reset to VIDEO_IN module
				);

`endif	//end control DE code or netlis
//	--------------------------------------------------------------------------------
//	IDE core
`ifdef	NO_IDE
//---------

assign			cpu_ide_rdata	=	32'h0; // IO read data
assign			cpu_ide_ack     =	1'b0; // IO ack

//---------

assign			atadiow_out_	= 1'b1;
assign			atadior_out_	= 1'b1;
assign			atacs0_out_		= 1'b1;
assign			atacs1_out_		= 1'b1;
assign			atadd_out		= 16'h0;
assign			atadd_oe_		= 4'hf;
assign			atada_out		= 3'h0;
assign			atadmack_out_	= 1'b1;
assign			ide_int_ 	= 1'b1;

device_bh	IDE_DEVICE_BH(
			//To Memory Interface:
			.xxx_ram_req		(ide_ram_req	    ),
			.xxx_ram_rw			(ide_ram_rw			),
			.xxx_ram_addr		(ide_ram_addr[29:2]	),
			.xxx_ram_rbl		(ide_ram_rbl[1:0]	    ),
			.xxx_ram_wdata		(ide_ram_wdata		),
			.xxx_ram_wbe		(ide_ram_wbe	    ),
			.xxx_ram_wlast		(ide_ram_wlast		),
			//From Memory Interface:
			.xxx_ram_ack		(ide_ram_ack		),
			.xxx_ram_err		(ide_ram_err		),
			.xxx_ram_rrdy		(ide_ram_rrdy		),
			.xxx_ram_rlast		(ide_ram_rlast		),
			.xxx_ram_rdata		(ide_ram_rdata		),
			.xxx_ram_wrdy		(ide_ram_wrdy		),
			//others:
			.rst_				(rst_				),
			.mem_clk			(mem_clk			)
			);

`else
wire [1:0]	p_cs;
wire [ 2:0] p_da;
wire [15:0] p_datao;
wire [15:0] p_datain;

wire [31:0] ide_ram_addr_out;
ATAINTF_IBUS ATAINTF_IBUS(
    // Output(s):
        // ataintf
        .P_ATADOEJ				(p_atadoej				),	// ATA bus data out enable, low active
        .P_CS					(p_cs					),	// ATA bus CS
        .P_DA					(p_da					),	// ATA bus ADDRESS
        .P_DATAO				(p_datao				),	// ATA bus data output
        .P_DIORJ				(p_diorj				),	// ATA bus DIORJ
        .P_DIOWJ				(p_diowj				),	// ATA bus DIOWJ
        .P_DMACKJ				(p_dmackj				),	// ATA bus DMACKJ
        .P_RESETJ				(p_resetj				),	// ATA bus RESETJ
        // Host
        .IDE_RDATA				(cpu_ide_rdata			),	// Data output
        .IDE_ACK				(cpu_ide_ack			),	// Slave ready
        .IDE_INTJ				(ide_int_				),	// Interrupt output, low active

    // Input(s):
        // ataintf
        .P_DATAIN				(p_datain				),               // ATA bus data input
        .P_DMARQ				(p_dmarq	    		),                // ATA bus DMARQ
        .P_INTRQ				(p_intrq	    		),                // ATA bus INTRQ
        .P_IORDY				(p_iordy	    		),                // ATA bus IORDY
        // Host
        .IDE_WDATA				(cpu_slave_wdata		),	// Data write by host
        .IDE_ADDR				({4'h0,cpu_slave_addr[11:2]}),	// Address for host access
        .IDE_RW					(cpu_slave_rw			),	// Host write IO register
        .IDE_WBE				(cpu_slave_be			),	// Byte enable for host access
        .IDE_CS					(cpu_ide_req			),	// ATA io registers select
//I_bus
		.DMA_RAM_ACK			(ide_ram_ack			),
		.DMA_RAM_WRDY			(ide_ram_wrdy			),
        .DMA_RAM_RRDY			(ide_ram_rrdy			),	// RAM controller read ready
        .DMA_RAM_RDATA			(ide_ram_rdata			),	// RAM controller data to DMA engine
		.DMA_RAM_REQ			(ide_ram_req			),
		.DMA_RAM_RW				(ide_ram_rw				),
		.DMA_RAM_WLAST			(ide_ram_wlast			),
		.DMA_RAM_WBL			(),				//no use
		.DMA_RAM_RBL			(ide_ram_rbl			),		// DMA RAM read burst length
		.DMA_RAM_WBE			(ide_ram_wbe			),
		.DMA_RAM_ADDR			(ide_ram_addr_out		),
		.DMA_RAM_DOUT			(ide_ram_wdata			),

        .IDE_CLK_EN				(1'b1					),		// useless port
        .TEST_MODE				(test_mode				),		// chip test mode
        .IDE_STOP				(1'b0					),		// IDE controller stop transfer
        .IDE_IDLE				(IDE_IDLE				),		// IDE interface is idle
        .IDE_ACTIVE  			(ide_active				),		// IDE interface need to use the external bus.

`ifdef	INSERT_SCAN_IDE
		.TEST_SE				(1'b0					),
`endif

`ifdef  SCAN_EN
		.TEST_SE				(TEST_SE				),
		.TEST_SI1				(ATA_TEST_SO3_TO_SI1	),
		.TEST_SI2				(TEST_SI[36]			),
		.TEST_SI3				(TEST_SI[37]			),

		.TEST_SO1				(TEST_SO[37]			),
		.TEST_SO2				(TEST_SO[36]			),
		.TEST_SO3				(ATA_TEST_SO3_TO_SI1	),
`endif

        .CLK_ATA				(ata_clk				),
        .CLK_SYS				(mem_clk			    ),	// clock for host access
        .RSTJ           		(rst_					) 	// system reset
        );

//ata output
assign	atacs0_out_ 	= p_cs[0];
assign	atacs1_out_ 	= p_cs[1];
assign	atadd_oe_  		= {4{p_atadoej}};
assign	atada_out  		= p_da;
assign	atadd_out  		= p_datao;
assign  atadior_out_ 	= p_diorj;
assign	atadiow_out_	= p_diowj;
assign	atadmack_out_	= p_dmackj;
//ata input
assign	p_datain		= atadd_in;
assign	p_dmarq			= atadmarq_in;
assign	p_intrq         = ataintrq_in;
assign	p_iordy         = ataiordy_in;

assign 	ide_ram_addr[29:2]  = ide_ram_addr_out[29:2];
// assign	ide_ram_rbl = 3'b0; //no read operation for IDE

`endif		// end of define NO_IDE

//	----------------------------------------------------------------------------------
//	SERVO

`ifdef NO_SERVO
assign		servo_int = 1'b0;
device_bh	SV_DEVICE_BH(
			//To Memory Interface:
			.xxx_ram_req		(sv_ram_req		),
			.xxx_ram_rw			(sv_ram_rw		),
			.xxx_ram_addr		(sv_ram_addr	),
			.xxx_ram_rbl		(sv_ram_rbl		),
			.xxx_ram_wdata		(sv_ram_wdata	),
			.xxx_ram_wbe		(sv_ram_wbe		),
			.xxx_ram_wlast		(sv_ram_wlast	),
			//From Memory Interface:
			.xxx_ram_ack		(sv_ram_ack		),
			.xxx_ram_err		(sv_ram_err		),
			.xxx_ram_rrdy		(sv_ram_rrdy	),
			.xxx_ram_rlast		(sv_ram_rlast	),
			.xxx_ram_rdata		(sv_ram_rdata	),
			.xxx_ram_wrdy		(sv_ram_wrdy	),
			//others:
			.rst_				(rst_			),
			.mem_clk			(mem_clk		)
			);

`else
SV_SERVO	SV_SERVO(
//////////// SERVO INTERFACE
		.DMCKG_MPGCLK			(mem_clk				),
//		.TOP_CLKDIV				(1'b0					),
		.PMATA_SOMD				(sv_pmata_somd			),
		.PMSVD_TRCCLK			(sv_pmsvd_trcclk		),
		.PMSVD_TRFCLK			(sv_pmsvd_trfclk		),
		.SV_TSLRF				(sv_sv_tslrf			),
		.SV_TPLCK				(sv_sv_tplck			),
//		.SV_DEV_TRUE     		(sv_sv_dev_true     	),
		.TST_SVOIO_I			(sv_tst_svoio_i			),
		.SVO_SVOIO_O			(sv_svo_svoio_o			),
		.SVO_SVOIO_OEJ			(sv_svo_svoio_oej		),
		.TST_ADDATA				(sv_tst_addata			),
		.REG_RCTST_2       		(sv_reg_rctst_2       	),
		.TOP_SV_MPPDN			(!servo_clk_en			),
		.DMCKG_SYSCLK        	(servo_clk				),
		.MCKG_DSPCLK         	(sv_mckg_dspclk			),
//		.SYCG_SVOATA 			(sv_sycg_svoata 		),
//		.SV_REG_C2FTST       	(sv_c2ftstin_en			),
//		.PMDM_IDESLAVE       	(1'b1					),
		.PMUP_HARDRSTJ       	(servo_rst_				),
		.PMSVD_TPLCK         	(sv_pmsvd_tplck			),
		.PMSVD_TSLRF         	(sv_pmsvd_tslrf			),
		.SV_TST_PIN				(sv_sv_tst_pin			),
//		.SV_REG_TSTOEN       	(sv_sv_reg_tstoen		),
		.PMSVD_C2FTSTIN			(sv_pmsvd_c2ftstin		),
		.SYCG_SOMD				(sv_sycg_somd			),
		//analog             	( ANALOG				)
		.SYCG_UPDBGOUT 			(sv_sycg_updbgout 		),
//		.CPU_GPIOIN				(sv_cpu_gpioin			),
		.SYCG_BURNIN			(sv_sycg_burnin			),
//	 Mpeg uP inf
		.EX_SV_UP_IN			(sv_ex_up_in			),
		.EX_SV_UP_OUT			(sv_ex_up_out			),
		.EX_SV_UP_OEJ			(sv_ex_up_oe_			),
// Atapi inf
		.SV_HI_HDRQ_O        	(sv_sv_hi_hdrq_o		),
		.SV_HI_HDRQ_OEJ      	(sv_sv_hi_hdrq_oej		),
		.SV_HI_HCS16J_O      	(sv_sv_hi_hcs16j_o		),
		.SV_HI_HCS16J_OEJ    	(sv_sv_hi_hcs16j_oej	),
		.SV_HI_HINT_O        	(sv_sv_hi_hint_o		),
		.SV_HI_HINT_OEJ      	(sv_sv_hi_hint_oej		),
		.SV_HI_HPDIAGJ_O     	(sv_sv_hi_hpdiagj_o		),
		.SV_HI_HPDIAGJ_OEJ   	(sv_sv_hi_hpdiagj_oej	),
		.SV_HI_HDASPJ_O      	(sv_sv_hi_hdaspj_o		),
		.SV_HI_HDASPJ_OEJ    	(sv_sv_hi_hdaspj_oej	),
		.SV_HI_HIORDY_O      	(sv_sv_hi_hiordy_o		),
		.SV_HI_HIORDY_OEJ    	(sv_sv_hi_hiordy_oej	),
		.SV_HI_HD_O				(sv_sv_hi_hd_o			),
		.SV_HI_HD_OEJ        	(sv_sv_hi_hd_oej		),
		.PMATA_HRSTJ         	(sv_pmata_hrstj			),
		.PMATA_HCS1J         	(sv_pmata_hcs1j			),
		.PMATA_HCS3J         	(sv_pmata_hcs3j			),
		.PMATA_HIORJ         	(sv_pmata_hiorj			),
		.PMATA_HIOWJ         	(sv_pmata_hiowj			),
		.PMATA_HDACKJ        	(sv_pmata_hdackj		),
		.PMATA_HPDIAGJ       	(sv_pmata_hpdiagj		),
		.PMATA_HDASPJ        	(sv_pmata_hdaspj		),
		.PMATA_HA				(sv_pmata_ha			),
		.PMATA_HD				(sv_pmata_hd			),

///////////// SERVO INTERFACe
		.SV_SVO_SFLAG			(sv_sv_svo_sflag	),
		.SV_SVO_SOMA			(sv_sv_svo_soma		),
		.SV_SVO_SOMD			(sv_sv_svo_somd		),
		.SV_SVO_SOMD_OEJ     	(sv_sv_svo_somd_oej ),
		.SV_SVO_SOMDH_OEJ    	(sv_sv_svo_somdh_oej),
//		.SV_REG_DVDRAM_TOE   	(sv_sv_reg_dvdram_toe),
//		.SV_REG_DFCTSEL      	(sv_sv_reg_dfctsel  ),
//		.SV_REG_TRFSEL 			(sv_sv_reg_trfsel 	),
//		.SV_REG_FLGTOEN 		(sv_sv_reg_flgtoen 	),
//		.SV_REG_GPIOTEN			(sv_sv_reg_gpioten	),
//		.SV_REG_13E_7			(sv_sv_reg_13e_7	),
//		.SV_REG_13E_6			(sv_sv_reg_13e_6	),
//		.REG_TRG_PLCKEN			(sv_reg_trg_plcken	),
//		.REG_TRG_PLCKEN			(sv_trg_plcken		),
//		.REG_SV_TPLCK_OEJ		(sv_reg_sv_tplck_oej),
//		ANA_DTSV

		.PAD_CLKREF				(sv_pad_clkref		   ),
		.XADCIN              	(sv_xadcin             ),
		.XADCIP              	(sv_xadcip             ),
		.XATTON              	(sv_xatton             ),
		.XATTOP              	(sv_xattop             ),
		.XBIASR              	(sv_xbiasr             ),
		.XCDLD               	(sv_xcdld              ),
		.XCDPD               	(sv_xcdpd              ),
		.XCDRF               	(sv_xcdrf              ),
		.XCD_A               	(sv_xcd_a              ),
		.XCD_B               	(sv_xcd_b              ),
		.XCD_C               	(sv_xcd_c              ),
		.XCD_D               	(sv_xcd_d              ),
		.XCD_E               	(sv_xcd_e              ),
		.XCD_F               	(sv_xcd_f              ),
		.XCELPFO             	(sv_xcelpfo            ),
		.XDPD_A              	(sv_xdpd_a             ),
		.XDPD_B              	(sv_xdpd_b             ),
		.XDPD_C              	(sv_xdpd_c             ),
		.XDPD_D              	(sv_xdpd_d             ),
		.XDVDLD              	(sv_xdvdld             ),
		.XDVDPD              	(sv_xdvdpd             ),
		.XDVDRFN             	(sv_xdvdrfn            ),
		.XDVDRFP             	(sv_xdvdrfp            ),
		.XDVD_A              	(sv_xdvd_a             ),
		.XDVD_B              	(sv_xdvd_b             ),
		.XDVD_C              	(sv_xdvd_c             ),
		.XDVD_D              	(sv_xdvd_d             ),
		.XFELPFO             	(sv_xfelpfo            ),
		.XFOCUS              	(sv_xfocus             ),
		.XGMBIASR            	(sv_xgmbiasr           ),
		.XLPFON              	(sv_xlpfon             ),
		.XLPFOP              	(sv_xlpfop             ),
		.XPDAUX1             	(sv_xpdaux1            ),
		.XPDAUX2             	(sv_xpdaux2            ),
		.XSBLPFO             	(sv_xsblpfo            ),
		.XSFGN               	(sv_xsfgn              ),
		.XSFGP               	(sv_xsfgp              ),

		.PATA_SFLAG				(sv_pata_sflag			),
		.PMATA_SFLAG			(sv_pmata_sflag			),
		.PMATA_SFLAG_OEJ		(sv_pmata_sflag_oej		),

		.XSFLAG					(sv_xsflag				),
		.XSLEGN              	(sv_xslegn             ),
		.XSLEGP              	(sv_xslegp             ),
		.XSPINDLE            	(sv_xspindle           ),
		.XTELP               	(sv_xtelp              ),
		.XTELPFO             	(sv_xtelpfo            ),
		.XTESTDA             	(sv_xtestda            ),
		.XTEXO               	(sv_xtexo              ),
		.XTRACK              	(sv_xtrack             ),
		.XTRAY               	(sv_xtray              ),
		.XVGAIN              	(sv_xvgain             ),
		.XVGAIP              	(sv_xvgaip             ),
		.XVREF15             	(sv_xvref15            ),
		.XVREF21             	(sv_xvref21            ),
//power               	(POWER              )
		.VD33D            		(sv_vd33d            	),
		.VD18D            		(sv_vd18d            	),
		.VDD3MIX1				(sv_vdd3mix1			),
		.VDD3MIX2				(sv_vdd3mix2			),
		.AVDD_AD          		(sv_avdd_ad          	),
		.AVDD_ATT         		(sv_avdd_att         	),
		.AVDD_DA          		(sv_avdd_da          	),
		.AVDD_DPD         		(sv_avdd_dpd         	),
		.AVDD_LPF         		(sv_avdd_lpf         	),
		.AVDD_RAD         		(sv_avdd_rad         	),
		.AVDD_REF         		(sv_avdd_ref         	),
		.AVDD_SVO         		(sv_avdd_svo         	),
		.AVDD_VGA         		(sv_avdd_vga         	),
		.AVSS_AD          		(sv_avss_ad          	),
		.AVSS_ATT         		(sv_avss_att         	),
		.AVSS_DA          		(sv_avss_da          	),
		.AVSS_DPD         		(sv_avss_dpd         	),
		.AVSS_GA          		(sv_avss_ga          	),
		.AVSS_GD          		(sv_avss_gd          	),
		.AVSS_LPF         		(sv_avss_lpf         	),
		.AVSS_RAD         		(sv_avss_rad         	),
		.AVSS_REF         		(sv_avss_ref         	),
		.AVSS_SVO         		(sv_avss_svo         	),
		.AVSS_VGA         		(sv_avss_vga         	),
		.AZEC_GND         		(sv_azec_gnd         	),
		.AZEC_VDD         		(sv_azec_vdd         	),
		.DV18_RCKA        		(sv_dv18_rcka        	),
		.DV18_RCKD        		(sv_dv18_rckd        	),
		.DVDD             		(sv_dvdd             	),
		.DVSS             		(sv_dvss             	),
		.DVSS_RCKA        		(sv_dvss_rcka        	),
		.DVSS_RCKD        		(sv_dvss_rckd        	),
		.FAD_GND          		(sv_fad_gnd          	),
		.FAD_VDD          		(sv_fad_vdd          	),
		.GNDD             		(sv_gndd             	),

///////////// MBUS INTERFACE
//From:
		.SV_RAM_REQ				(sv_ram_req			),
		.SV_RAM_RW				(sv_ram_rw			),
		.SV_RAM_ADDR			(sv_ram_addr		),
		.SV_RAM_RBL				(sv_ram_rbl			),
//		.SV_RAM_WBL				(sv_ram_wbl			),
		.SV_RAM_WLAST			(sv_ram_wlast		),
		.SV_RAM_WBE				(sv_ram_wbe			),
		.SV_RAM_WDATA			(sv_ram_wdata		),
//To:
		.SV_RAM_ACK				(sv_ram_ack			),
		.SV_RAM_RRDY			(sv_ram_rrdy		),
		.SV_RAM_RLAST			(sv_ram_rlast		),
		.SV_RAM_RDATA			(sv_ram_rdata		),
		.SV_RAM_WRDY			(sv_ram_wrdy		),
// SYSTEM INTERFACE
        .CPU_SV_REQ         	(cpu_servo_req      ),
        .CPU_SV_ADDR        	(cpu_slave_addr     ),
        .CPU_SV_BE          	(cpu_slave_be       ),
        .CPU_SV_WDATA       	(cpu_slave_wdata    ),
        .CPU_SV_RW          	(cpu_slave_rw       ),
        .CPU_SV_ACK         	(cpu_servo_ack      ),
        .CPU_SV_RDATA       	(cpu_servo_rdata    ),
		.SERVO_INT				(servo_int			),
`ifdef  SCAN_EN
		.TEST_SE				(TEST_SE			),
		.TEST_SI				(TEST_SI[76]		),
		.TEST_SO				(TEST_SO[76]		),
`endif
`ifdef	INSET_SCAN_SERVO
		.TEST_SE				(1'b0				),
`endif
// SYSTEM CFG
		.TEST_MODE				(test_mode			),
		.SERVO_ONLY_MODE		(servo_only_mode	),
		.SERVO_TEST_MODE		(servo_test_mode	),
		.SERVO_SRAM_MODE		(servo_sram_mode	),
		.SV_ECC_LATENCY_CTRL	(sv_ecc_latency_ctrl)
		);
`endif
endmodule
