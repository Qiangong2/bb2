/******************************************************************************
	  (c) copyrights 2000-. All rights reserved

	   T-Square Design, Inc.

	  Company confidential and Properietary	information.
	  This information may not be disclosed	to unauthorized individual.
*******************************************************************************/

/*******************************************************************************
 *
 *
 *       AUTHOR: Kandy Kong
 *
 *       HISTORY:   29/03/01        initial version
 					2002-06-18		change the port CHAIN_DLY_SEL from [2:0] to
 									[3:0]. The cell delay changed from 0.2 to 0.1ns.
 									The delay chain contain 15 steps with 0.1ns
 									programmable.
 *      			2003-07-18		change the port CHAIN_DLY_SEL from [3:0] to
 									[4:0] The delay chain contain 31 steps with 0.1 ns
 									programmable.
 					2003-08-13		change module name to P_DLY_CHAIN32
 									change port name to A,DLY_SEL[4:0],Y
 *********************************************************************************/
module	P_DLY_CHAIN32(
		A,		// input source
		Y,		// programmable delay output, controlled by "CHAIN_DLY_SEL"
		YN,		//the inverted output of Y
		DLY_SEL	// select which level chain
		);


parameter		cell_dly = 0.1;	// 0.1 ns delay of each cell

input			A;
input	[4:0]	DLY_SEL;
output			Y;
output			YN;

wire			CHAIN_IN		=	A;
wire	[4:0]	CHAIN_DLY_SEL	=	DLY_SEL;


wire	[31:0]	dly_stage;


assign	dly_stage[0] = CHAIN_IN;

// below must be replaced with certain delay cell
assign	#cell_dly	dly_stage[ 1] = dly_stage[ 0];
assign	#cell_dly	dly_stage[ 2] = dly_stage[ 1];
assign	#cell_dly	dly_stage[ 3] = dly_stage[ 2];
assign	#cell_dly	dly_stage[ 4] = dly_stage[ 3];
assign	#cell_dly	dly_stage[ 5] = dly_stage[ 4];
assign	#cell_dly	dly_stage[ 6] = dly_stage[ 5];
assign	#cell_dly	dly_stage[ 7] = dly_stage[ 6];
assign	#cell_dly	dly_stage[ 8] = dly_stage[ 7];
assign	#cell_dly	dly_stage[ 9] = dly_stage[ 8];
assign	#cell_dly	dly_stage[10] = dly_stage[ 9];
assign	#cell_dly	dly_stage[11] = dly_stage[10];
assign	#cell_dly	dly_stage[12] = dly_stage[11];
assign	#cell_dly	dly_stage[13] = dly_stage[12];
assign	#cell_dly	dly_stage[14] = dly_stage[13];
assign	#cell_dly	dly_stage[15] = dly_stage[14];
assign	#cell_dly	dly_stage[16] = dly_stage[15];
assign	#cell_dly	dly_stage[17] = dly_stage[16];
assign	#cell_dly	dly_stage[18] = dly_stage[17];
assign	#cell_dly	dly_stage[19] = dly_stage[18];
assign	#cell_dly	dly_stage[20] = dly_stage[19];
assign	#cell_dly	dly_stage[21] = dly_stage[20];
assign	#cell_dly	dly_stage[22] = dly_stage[21];
assign	#cell_dly	dly_stage[23] = dly_stage[22];
assign	#cell_dly	dly_stage[24] = dly_stage[23];
assign	#cell_dly	dly_stage[25] = dly_stage[24];
assign	#cell_dly	dly_stage[26] = dly_stage[25];
assign	#cell_dly	dly_stage[27] = dly_stage[26];
assign	#cell_dly	dly_stage[28] = dly_stage[27];
assign	#cell_dly	dly_stage[29] = dly_stage[28];
assign	#cell_dly	dly_stage[30] = dly_stage[29];
assign	#cell_dly	dly_stage[31] = dly_stage[30];

// clock MUX for Programmable output
wire	DLY_PRG_OUT = dly_stage[CHAIN_DLY_SEL];
assign	Y	=	DLY_PRG_OUT;
wire	YN	=	~Y;

endmodule
