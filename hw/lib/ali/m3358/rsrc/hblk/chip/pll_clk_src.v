/******************************************************************************
	  (c) copyrights 2000-. All rights reserved
 
	   T-Square Design, Inc.
 
	  Company confidential and Properietary	information.
	  This information may not be disclosed	to unauthorized individual.	  
*******************************************************************************/

/*******************************************************************************
 *
 *
 *       AUTHOR: Kandy Kong
 *
 *       HISTORY:   17/03/01         initial version
 *      
 *********************************************************************************/
module pll_clk_src (

	// input
		f48m,		// from 48M crystal
		fin_sel,	// select 48M/24M as source of PLL
		pll_fvco,	// PLL clock input		
		pll_vco_sel,	// PLL clock select
	// output
		pll_sclk,	// to PLL as source
		div_clk_src,	// clock source to clock divider
		f12m,
	// other
		rst_
		);
		
	// input
input		f48m,		// from 48M crystal
		fin_sel,	// select 48M/24M as source of PLL
		pll_fvco;	// PLL clock input		
input	[1:0]	pll_vco_sel;	// PLL clock select
	// output
output		pll_sclk,	// to PLL as source
		div_clk_src;	// clock source to clock divider
output		f12m;
	// other
input		rst_;

parameter	udly = 0.1;

reg	f24m_div_reg;
always	@(posedge f48m or negedge rst_)
	if (!rst_)
		f24m_div_reg <= #udly 1'b0;
	else
		f24m_div_reg <= #udly !f24m_div_reg;

assign	pll_sclk = fin_sel ? f48m : f24m_div_reg;

// KD 2001/04/03
reg	f12m;
always	@(posedge f24m_div_reg or negedge rst_)
	if (!rst_)
		f12m <= #udly 1'b0;
	else
		f12m <= #udly !f12m;

assign	div_clk_src =	pll_vco_sel[1] ? pll_fvco :		// normal clock
				pll_vco_sel[0] ? pll_sclk :	// by-pass PLL
						1'b0;		// disable divider clock

		
endmodule	
