/******************************************************************************
	  (c) copyrights 2000-. All rights reserved

	   T-Square Design, Inc.

	  Company confidential and Properietary	information.
	  This information may not be disclosed	to unauthorized individual.
*******************************************************************************/

/*******************************************************************************
 *	DESCRIPTION:	The AUDIO_PLL behavior module in M6304.
 *
 *	AUTHOR: 		Yuky
 *
 *	HISTORY:   		2002.10.07	initial version
								output 16.9344 MHz and 24.576MHz
					2003-08-28	Change analog power name
 *********************************************************************************/
`timescale	1ns/1ps

module	AUDIO_PLL	(
		FIN,
		LOCK_EN,
		PD,
		FOUT_SEL,

		FOUT,
		TFCK,
		TLOCK,
		PLLAVSS,
		PLLAVDD,
		PLLDVDD,
		PLLDVSS
		);

input			FIN;		// 27MHz input
input           PD;			// power down control
input			LOCK_EN;		// PLL clock gate off enable
input			FOUT_SEL;	// PLL FOUT frequency select

output			FOUT;		// 24.576MHz or 16.9344MHz output
output			TFCK;		// PLL TEST FCK
output			TLOCK;		// PLL TEST LOCK
input			PLLAVSS;	// PLL analog ground
input			PLLAVDD;	// PLL analog power
input			PLLDVDD;	// PLL digital power
input			PLLDVSS;	// PLL digital ground

tri0	TFCK, TLOCK;	//temporary use, for kill no driver warning
/*=============================================================================
	input monitor
=============================================================================*/
reg		[9:0]	fin_clk_cnt;
real			fin_clk_1000times;
real			fin_clk_start;

initial	begin
	fin_clk_cnt = 0;
	fin_clk_1000times = 0;
	fin_clk_start = 0;
	wait	(fin_clk_cnt == 10'd1001);
    $display ("AUDIO PLL FIN frequency is %f MHz", (1000000/fin_clk_1000times));
//    if (fin_clk_1000times != 1000000/48) begin
//    	$display ("FIN clock frequency Error! It must be fixed at 48MHz");
//    end
//    else begin
//    	$display ("FIN clock frequency Correct!");
//    end
end

// fin clock monitor
always @ (posedge FIN)
	if (fin_clk_cnt == 10'd0) begin
	   fin_clk_start	= $realtime;
	   fin_clk_cnt		= fin_clk_cnt + 1;
	end
	else if (fin_clk_cnt == 10'd1000) begin
	   fin_clk_1000times	= $realtime - fin_clk_start;
	   fin_clk_cnt			= fin_clk_cnt + 1;
	end
	else if (fin_clk_cnt != 10'h3ff) begin
	   fin_clk_cnt		= fin_clk_cnt + 1;
	end

/*=============================================================================
	output clock generator
	No swith time now
	No relationship with the input frequency
=============================================================================*/
reg			FOUT_16_tmp;
reg			FOUT_24_tmp;
reg	[3:0]	fout_clk_cnt;
real		fout_clk_start;
real		fout_clk_10times;

initial begin
	FOUT_16_tmp = 10;
	FOUT_24_tmp = 10;
	fout_clk_cnt = 0;
	fout_clk_start = 1;
	fout_clk_10times = 1;
end

always #20.345052 FOUT_24_tmp = !FOUT_24_tmp;
always #29.525699 FOUT_16_tmp = !FOUT_16_tmp;

// fout clock monitor
always @ (posedge FOUT)
	if (fout_clk_cnt == 4'd1) begin
	   fout_clk_start	= $realtime;
	   fout_clk_cnt		= fout_clk_cnt + 1;
	end
	else if (fout_clk_cnt == 4'd11) begin
	   fout_clk_10times	= $realtime - fout_clk_start;
	   fout_clk_cnt		= fout_clk_cnt + 1;
	   $display("FOUT frequency is %fMHz at %m", 10000/fout_clk_10times);
	end
	else if (fout_clk_cnt != 4'hf) begin
	   fout_clk_cnt		= fout_clk_cnt + 1;
	end

	/*=========================================================================
		Output Part
	=========================================================================*/
	assign	FOUT	= FOUT_SEL ? FOUT_16_tmp : FOUT_24_tmp;

endmodule

