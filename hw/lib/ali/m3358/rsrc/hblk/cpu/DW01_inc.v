//---------------------------------------------------------------------------
//
//        This confidential and proprietary software may be used only            
//     as authorized by a licensing agreement from Synopsys Inc.                 
//     In the event of publication, the following notice is applicable:          
//                                                                               
//                    (C) COPYRIGHT 1992   SYNOPSYS INC.                         
//                          ALL RIGHTS RESERVED                                  
//                                                                               
//        The entire notice above must be reproduced on all authorized           
//       copies.                                                                 
//
//
// AUTHOR:   PS		Nov. 7, 1991
//
// ABSTRACT: Incrementer
//
// MODIFIED: 
//
// Sheela    - May 12,1995
// Converted from vhdl to verilog
//
// GN  Feb. 15, 1996 
// changed dw01 to DW01 star 33068
//
//--------------------------------------------------------------------------

module DW01_inc ( A, SUM );
   
    parameter width=4;

    // port list declaration in order
    output   [ width-1 : 0]  SUM;  
    input    [ width-1 : 0]  A; 

    reg      [ width-1 : 0]  SUM;  

always @ (A) 
  begin 
    SUM  =  increment(A);
  end

function  [width-1 : 0]  increment;

    input  [width-1 : 0 ]   A;
    reg                     carry;
    reg    [width-1 : 0 ]   SUM;
    integer                 i;

    begin
	carry = 1'b1;
	for (i = 0; i <= width-1; i = i + 1) begin 
	    SUM[i] = A[i] ^ carry;
	    carry = (A[i] & carry);
	end                     // loop
	increment = SUM;
    end
endfunction // ;


endmodule // DW01_inc;
