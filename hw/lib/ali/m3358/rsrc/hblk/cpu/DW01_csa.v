//----------------------------------------------------------------------------
//
//        This confidential and proprietary software may be used only            
//     as authorized by a licensing agreement from Synopsys Inc.                 
//     In the event of publication, the following notice is applicable:          
//                                                                               
//                    (C) COPYRIGHT 1992   SYNOPSYS INC.                         
//                          ALL RIGHTS RESERVED                                  
//                                                                               
//        The entire notice above must be reproduced on all authorized           
//       copies.                                                                 
//
// AUTHOR:   SS			April 4, 1996
//
// VERSION:  Verilog simulation model 
//
// ABSTRACT: CSA 
//
// MODIFIED: 
//
//---------------------------------------------------------------------------

module DW01_csa (a,b,c,ci,carry,sum,co);

  parameter width=4;

  // port decalrations

  input  [width-1 : 0]   a,b,c;
  input                  ci;
  output [width-1 : 0]   carry,sum;
  output                 co;

  reg    [width-1 : 0]   carry,sum;
  reg                    co;
     integer               i;

always @(a or b or c or ci)
  begin
    carry[0] = c[0];
    carry[1] = (a[0]&b[0])|((a[0]^b[0])&ci);
    sum[0] = a[0]^b[0]^ci;
    for (i = 1; i <= width-2; i = i + 1) begin 
       carry[i+1] = (a[i]&b[i])|((a[i]^b[i])&c[i]);
       sum[i] = a[i]^b[i]^c[i];
    end // loop
    sum[width-1] = a[width-1]^b[width-1]^c[width-1];
    co = (a[width-1]&b[width-1])|((a[width-1]^b[width-1])&c[width-1]);
  end // process

endmodule  // DW01_csa;
