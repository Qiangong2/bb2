module mux4to1_f(dout,din0,din1,din2,din3,sel0,sel1);
//synopsys template
parameter WIDTH=2;
    output [WIDTH-1:0]  dout;
    input  [WIDTH-1:0]  din0;
    input  [WIDTH-1:0]  din1;
    input  [WIDTH-1:0]  din2;
    input  [WIDTH-1:0]  din3;
	input  [WIDTH-1:0]  sel0;
    input  [WIDTH-1:0]  sel1;

	reg  [WIDTH-1:0]  dout;
    reg  tmp_in0,tmp_in1,tmp_in2,tmp_in3,tmp_sel0,tmp_sel1,tmp_dout;
    integer i;

    always @(din0 or din1 or din2 or din3 or sel0 or sel1) begin  
		for (i=0;i<WIDTH;i=i+1) begin
            tmp_in0 = din0[i];
            tmp_in1 = din1[i];
            tmp_in2 = din2[i];
            tmp_in3 = din3[i];
            tmp_sel0 = sel0[i];
            tmp_sel1 = sel1[i];
			case ({tmp_sel1,tmp_sel0}) // synopsys infer_mux
				2'b00 : tmp_dout = tmp_in0;
				2'b01 : tmp_dout = tmp_in1;
                2'b10 : tmp_dout = tmp_in2;
                2'b11 : tmp_dout = tmp_in3;
			endcase
            dout[i] = tmp_dout;
		end
	end
/* synopsys dc_script_begin
 compile_create_mux_op_hierarchy = false
*/
endmodule
