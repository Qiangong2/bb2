module sedff(din, enable, dout, clk);
//synopsys template
parameter WIDTH=1;
parameter U_DLY=1;
input  [WIDTH-1:0] din;
input              enable;
input              clk;
output [WIDTH-1:0] dout;
/* synopsys dc_script_begin
   set_register_type  -exact -flip_flop SEDFFX2
*/
reg [WIDTH-1:0] dout;

always @(posedge clk)
  if(enable)
    dout <= #U_DLY din;

endmodule
