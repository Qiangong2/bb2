module sedfftr(din, enable, dout, clk, rst_);
//synopsys template
parameter WIDTH=1;
parameter U_DLY=1;
input  [WIDTH-1:0] din;
input              enable;
input              clk;
input              rst_;
output [WIDTH-1:0] dout;
/* synopsys dc_script_begin
   set_register_type  -exact -flip_flop SEDFFTRX2
*/
reg [WIDTH-1:0] dout;

//synopsys async_set_reset "rst_"
always @(posedge clk or negedge rst_)
  if(!rst_)
    dout <= {WIDTH{1'b0}};
  else if(enable)
    dout <= #U_DLY din;

/* synopsys dc_script_begin
   set_drive 0 rst_
   set_dont_touch_network rst_
*/
endmodule
