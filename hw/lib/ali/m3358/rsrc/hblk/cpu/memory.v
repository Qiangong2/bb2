/**********************************************
* File: memory.v
* Author: Farmer
* Description: this memory model emulate the
* EPROM, SDRAM, and system io, using T2RISC
* host bus transaction protocol
**********************************************/
//`define SUBBLOCK
module memory(
  //outputs
  o_data_in,
  o_ready,
  o_ack,
  o_bus_error,
 
  //inputs
  u_request,
  u_write,
  u_bytemask,
  u_wordsize,
  u_startpaddr,
  u_data_to_send,

  clk,
  rst_
);
output [31:0] o_data_in;
output        o_ready;
output        o_ack;
output        o_bus_error;

input  [31:0] u_data_to_send;
input  [31:0] u_startpaddr;
input  [1:0]  u_wordsize;
input  [3:0]  u_bytemask;
input         u_request;
input         u_write;

input         clk, rst_;

reg [31:0]    o_data_in;
reg           o_ready;
reg           o_ack;
reg           o_bus_error;

parameter  SDRAM_LATENCY=8;
parameter  EPROM_LATENCY=15;
parameter  SYSIO_LATENCY=20;
parameter  UDLY=1;
parameter  DIMMSIZE=  8*1024*1024;
parameter  EPROMSIZE= 2*1024*1024;
parameter  SYSIOSIZE= 64*1024;

initial begin
  o_ready = 1'b0;
  o_ack   = 1'b0;
  o_bus_error = 1'b0;
  o_data_in = 32'b0;
end

reg [31:0] dimm0[DIMMSIZE/4-1:0];
reg [31:0] dimm1[DIMMSIZE/4-1:0];
reg [31:0] eprom[EPROMSIZE/4-1:0];
reg [31:0] sysio[SYSIOSIZE/4-1:0];

wire error_cs;
wire spec_cs;
wire dimm0_cs, dimm1_cs;
wire eprom_cs;
wire sysio_cs;
wire [18:0] eprom_addr;
wire [20:0] sdram_addr;
wire [13:0] sysio_addr;
wire [2:0]  word_cnt;
wire [3:0]  WR_BURST_LATENCY=0;
wire [3:0]  RD_BURST_LATENCY=4;

//error paddr 0x7fff0000 ~ 0x7fffffff 0.5M
assign error_cs=(u_startpaddr[31:16]==16'h7fff);

//special paddr
assign spec_cs=(u_startpaddr[31:23]==9'h020)||(u_startpaddr[31:23]==9'h002);

//dimm0 paddr 0x00000000 ~ 0x007fffff 8M memory
assign dimm0_cs=(u_startpaddr[31:23]==9'h000)||(u_startpaddr[31:23]==9'h180)||(u_startpaddr[31:23]==9'h080);

//dimm1 paddr 0x08000000 ~ 0x087fffff 8M memory
assign dimm1_cs=(u_startpaddr[31:23]==9'h010)||spec_cs;

//eprom paddr 0x1fc00000 ~ 0x1fdfffff 2M memory
assign eprom_cs=(u_startpaddr[31:21]==12'b11111110);

//sysio paddr >0x17000000
assign sysio_cs=(u_startpaddr[31:24]==8'h11 || 
                 u_startpaddr[31:24]==8'h17) || error_cs;

assign sdram_addr=u_startpaddr[22:2];
assign eprom_addr=u_startpaddr[20:2];
assign sysio_addr=u_startpaddr[15:2];

//modeling EPROM
always @(posedge clk) 
  if(u_request & eprom_cs) begin
    repeat(EPROM_LATENCY) @(posedge clk);
    if(u_write) begin
      o_ready <= #UDLY 1'b1;
      o_ack   <= #UDLY 1'b1;
      o_bus_error <= #UDLY 1'b1;
      @(posedge clk)
      o_ready <= #UDLY 1'b0;
      o_ack   <= #UDLY 1'b0;
      o_bus_error <= #UDLY 1'b0;
    end
    else begin
      o_ready <= #UDLY 1'b1;
      o_ack   <= #UDLY 1'b1;
      o_bus_error <= #UDLY 1'b0;
      o_data_in <= #UDLY eprom[eprom_addr];
      @(posedge clk)
      o_ready <= #UDLY 1'b0;
      o_ack   <= #UDLY 1'b0;
    end
  end 

reg [31:0] word_be;

//modeling SYSIO
always @(posedge clk) 
  if(u_request & sysio_cs) begin
    repeat(SYSIO_LATENCY) @(posedge clk);
    if(u_write) begin
      $display("SYSIO: %h %b %b %h", u_startpaddr, u_wordsize, u_bytemask, u_data_to_send);
      word_be = { {8{u_bytemask[3]}}, {8{u_bytemask[2]}}, {8{u_bytemask[1]}}, {8{u_bytemask[0]}} };
      sysio[sysio_addr] <= #UDLY u_data_to_send & word_be | sysio[sysio_addr] & ~word_be;
      o_ready <= #UDLY 1'b1;
      o_ack   <= #UDLY 1'b1;
      @(posedge clk)
      o_ready <= #UDLY 1'b0;
      o_ack   <= #UDLY 1'b0;
    end
    else if(error_cs) begin
      o_ready     <= #UDLY 1'b1;
      o_bus_error <= #UDLY 1'b1;
      repeat(3) @(posedge clk);
      o_ack       <= #UDLY 1'b1;
      @(posedge clk)
      o_ready <= #UDLY 1'b0;
      o_ack   <= #UDLY 1'b0;
      o_bus_error <= #UDLY 1'b0;
    end
    else begin
      o_ready <= #UDLY 1'b1;
      o_ack   <= #UDLY 1'b1;
      o_bus_error <= #UDLY 1'b0;
      o_data_in <= #UDLY sysio[sysio_addr];
      @(posedge clk)
      o_ready <= #UDLY 1'b0;
      o_ack   <= #UDLY 1'b0;
    end
  end 

//modeling SDRAM
assign word_cnt[0] = ~u_wordsize[0] & ~u_wordsize[1];
assign word_cnt[1] = u_wordsize[0] & ~u_wordsize[1];
assign word_cnt[2] = ~u_wordsize[0] & u_wordsize[1];
 
reg [2:0]  i;
reg [31:0] tmp_val;
reg [31:0] word_msk;
reg [1:0]  addrl;

always @(posedge clk)
  if(u_request & (dimm0_cs | dimm1_cs)) begin
    repeat(SDRAM_LATENCY) @(posedge clk);
    if(u_write) begin
      for(i=0; i<word_cnt; i=i+1) begin
        #(UDLY*2)
        word_msk = {{8{u_bytemask[3]}}, {8{u_bytemask[2]}}, {8{u_bytemask[1]}}, {8{u_bytemask[0]}}};
        tmp_val = dimm0_cs ? dimm0[sdram_addr+i]:dimm1[sdram_addr+i];
        tmp_val = tmp_val & ~word_msk;
        if(dimm0_cs)
          dimm0[sdram_addr+i] = tmp_val | u_data_to_send & word_msk; 
        else
          dimm1[sdram_addr+i] = tmp_val | u_data_to_send & word_msk; 
        o_ready <= #UDLY 1'b1;
        if(word_cnt==i+1)
          o_ack <= #UDLY 1'b1;
        @(posedge clk) begin
          if(WR_BURST_LATENCY!=0 && ~o_ack) begin
            o_ready <= #UDLY 1'b0;
            repeat(WR_BURST_LATENCY-1) @(posedge clk);
          end // if
        end // posedge clk
      end // for
      o_ready <= #UDLY 1'b0;
      o_ack <= #UDLY 1'b0;
    end // if
    else begin
      for(i=0; i<word_cnt; i=i+1) begin
        `ifdef SUBBLOCK
        addrl[1:0] = sdram_addr[1:0] ^ i;
        `else
        addrl[1:0] = sdram_addr[1:0] + i;
        `endif
        if(dimm0_cs)
          o_data_in=dimm0[{sdram_addr[20:2], addrl}];
        else
          o_data_in=dimm1[{sdram_addr[20:2], addrl}];
        o_ready <= #UDLY 1'b1;
        if(word_cnt==i+1)
          o_ack <= #UDLY 1'b1;
        @(posedge clk) begin
          if(RD_BURST_LATENCY!=0 && ~o_ack) begin
            o_ready <= #UDLY 1'b0;
            repeat(RD_BURST_LATENCY-1) @(posedge clk);
          end 
        end //posedge clk
      end //for
      o_ready <= #UDLY 1'b0;
      o_ack <= #UDLY 1'b0;
    end //if
  end //if

integer j;
reg [31:0] ram_reg[DIMMSIZE-1:0];
initial begin
  $display("Clear EPROM...");
  for(j=0; j<EPROMSIZE/4; j=j+1)begin
    eprom[j]=32'h0;
  end
  $display("Clear DIMM0...");
  $display("Clear DIMM1...");
  for(j=0; j<DIMMSIZE/4; j=j+1)begin
    dimm0[j]=32'h0;
    dimm1[j]=32'h0;
  end 

  /* 
    memory loader 
    need to check CPU endian to decide if needs to do
    endian adjust
  //EPROM
  $readmemh("pattern.code.map", ram_reg);
  for(j=0; j<EPROMSIZE/4; j=j+1)begin
    tmp_val=ram_reg[j];
    eprom[j]={tmp_val[7:0], tmp_val[15:8], tmp_val[23:16], tmp_val[31:24]};
  end

  //DIMM0
  $readmemh("pattern.data.map", ram_reg);
  for(j=0; j<DIMMSIZE/4; j=j+1)begin
    tmp_val=ram_reg[j];
    dimm0[j]={tmp_val[7:0], tmp_val[15:8], tmp_val[23:16], tmp_val[31:24]};
  end

  //DIMM1
  */
  
end

`ifdef DUMP_BUS
event u_over;
integer bus_file;
initial begin
  bus_file=$fopen("bus.file");
end

always @(posedge clk)begin
  if(u_request) begin
    if(u_write) begin
      $fdisplay(bus_file, "Write to memory %h", u_startpaddr);
    end
    else begin
      $fdisplay(bus_file, "Read from memory %h", u_startpaddr);
    end
    @(u_over);
  end
end

always @(negedge u_request)
  ->u_over;

always @(posedge u_request)
  $fdisplay(bus_file, "");

always @(posedge clk) begin
  if(o_ready & u_write)
    $fdisplay(bus_file, "Core data: %h bytemask: %b", u_data_to_send, u_bytemask);
  else if(o_ready & ~u_write)
    $fdisplay(bus_file, "External data: %h", o_data_in);
end
`endif

endmodule
