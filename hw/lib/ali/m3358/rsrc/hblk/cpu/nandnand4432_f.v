module nandnand4432_f(
	dout ,
	din00 ,
	din01 ,
	din02 ,
	din03 ,
	din10 ,
	din11 ,
	din12 ,
	din13 ,
	din20 ,
	din21 ,
	din22 ,
	din30 ,
	din31
);
//synopsys template
parameter WIDTH=1;
	output	[WIDTH-1:0]	dout ;
	input	[WIDTH-1:0]	din00 ;
	input	[WIDTH-1:0]	din01 ;
	input	[WIDTH-1:0]	din02 ;
	input   [WIDTH-1:0] din03 ;
	input	[WIDTH-1:0]	din10 ;
	input	[WIDTH-1:0]	din11 ;
	input   [WIDTH-1:0] din12 ;
	input   [WIDTH-1:0] din13 ;
	input	[WIDTH-1:0]	din20 ;
	input	[WIDTH-1:0]	din21 ;
	input   [WIDTH-1:0] din22 ;
	input   [WIDTH-1:0] din30 ;
	input   [WIDTH-1:0] din31 ;

	assign	dout = (din00 & din01 & din02 & din03) 	|
					(din10 & din11 & din12 & din13)			|
					(din20 & din21 & din22)		|
					(din30 & din31);
 
endmodule
