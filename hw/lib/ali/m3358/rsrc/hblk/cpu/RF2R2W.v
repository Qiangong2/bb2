/****************************************************************
       Project Name :      T2RISC1C
          File Name :      RF2R2W_fpga.v
        Description :      Register file configued with register array
                              Depth: 32
                              Width: 32

             Author :      Jim Xia
      Creation Date :      2003/5/9
*****************************************************************/

module RF2R2W (
	//read port 1
	QA,
	CENA,
	AA,
	CLKA,

	//read port 2
	QC,
	CENC,
	AC,
	CLKC,

	//write port 1
	CENB,
	AB,
	DB,
	CLKB,

	//write port 2
	CEND,
	AD,
	DD,
	CLKD
);

output	[31:0]	QA;
output	[31:0]	QC;
input		CENA;
input		CENB;
input		CENC;
input		CEND;
input	[4:0]	AA;
input	[4:0]	AB;
input	[4:0]	AC;
input	[4:0]	AD;
input	[31:0]	DB;
input	[31:0]	DD;
input		CLKA;
input		CLKB;
input		CLKC;
input		CLKD;

parameter	u_dly = 1;
integer		i;

reg	[31:0]	mem[31:0];
reg	[31:0]	QA;
reg	[31:0]	QC;

wire		clkr = CLKA;
wire		clkw = CLKB;

wire	[31:0]	AB_dec = 32'b1 << AB;
wire	[31:0]	AD_dec = 32'b1 << AD;

always @(posedge clkr) begin
  if(~CENA)
    QA <= #u_dly mem[AA];
end

always @(posedge clkr) begin
  if(~CENC)
    QC <= #u_dly mem[AC];
end

always @(posedge clkw) begin
  for(i=0; i<32; i=i+1) begin
    if(~CENB & AB_dec[i])
      mem[i] <= #u_dly DB;
    if(~CEND & AD_dec[i])
      mem[i] <= #u_dly DD;
  end
end


endmodule /* end of RF2R2W */
