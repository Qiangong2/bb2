module nor4_f(
	dout ,
	din0 ,
	din1 ,
	din2 ,
	din3
);
//synopsys template
parameter WIDTH=1;
	output	[WIDTH-1:0]	dout ;
	input	[WIDTH-1:0]	din0 ;
	input	[WIDTH-1:0]	din1 ;
	input	[WIDTH-1:0]	din2 ;
	input	[WIDTH-1:0]	din3 ;

	assign	dout = ~(din0 | din1 | din2 | din3) ;
 
endmodule
