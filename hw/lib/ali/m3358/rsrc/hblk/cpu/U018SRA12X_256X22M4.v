//---------------------------------------------------------------------
//               Copyright(c) Virage Logic Corporation.                
//     All Rights reserved - Unpublished -rights reserved under        
//     the Copyright laws of the United States of America.             
//                                                                     
//  This file includes the Confidential information of Virage Logic    
//  and UMC                                                            
//  The receiver of this Confidential Information shall not disclose   
//  it to any third party and shall protect its confidentiality by     
//  using the same degree of care, but not less then a reasonable      
//  degree of care, as the receiver uses to protect receiver's own     
//  Confidential Information.                                          
//                                                                     
//                    Virage Logic Corporation.                        
//                    46501 Landing Parkway                            
//                    Fremont , California 94538.                      
//                                                                     
//---------------------------------------------------------------------
//                                                                     
//  Software           : Rev: 3.1.3 (build REL-3-1-3-2002-06-30)       
//  Library Format     : Rev: 1.05.00                                  
//  Compiler Name      : um18s1p11hssb05                               
//  Date of Generation : Wed Jun 25 13:50:23 CST 2003                  
//                                                                     
//---------------------------------------------------------------------
//   --------------------------------------------------------------     
//                       Template Revision : 1.2.0                      
//   --------------------------------------------------------------     
//  ************************************************************
//     *    Synchronous, 1-Port Read/Write RAM      *
//            *  Verilog Behavioral Model  *
// *************************************************************
//
// THIS IS A SYNCHRONOUS 1-PORT MEMORY MODEL
// WHICH TAKES BOTH TIMING CHECKS AND DELAY DATA FROM SDF ONLY
//
// Memory Name:       U018SRA12X_256X22M4
// Memory Size:       256 words x 22 bits
//
//                 PORT NAME
//                 ---------
// Input Ports:
//
//                 ADR [7:0]
//                 D  [21:0]
//                 WEB
//                 OEB
//                 MEB
//                 CLK
//                 RM  [3:0]

// Output Ports:
//
//                 Q [21:0]
//
// *************************************************************
`resetall 

`timescale 1 ns / 1 ps 
`celldefine 
`suppress_faults 
`enable_portfaults 

`define True    1'b1
`define False   1'b0
module U018SRA12X_256X22M4 ( Q, ADR, D,WEB, OEB, MEB,  CLK, RM);

parameter MES_ALL = "ON";

output [21:0] Q;
wire [21:0] Q;

input [7:0] ADR;
input [21:0] D;
input WEB;
input OEB;
input MEB;
input CLK;
input [3:0] RM;

// Local registers, wires, etc

wire [21:0]    Q_tmp;
wire [7:0]    ADR_buf;
wire [21:0]    D_buf;
wire OEB_eff;
wire OEBeff_del;
reg  notif_a_we, notif_a_adr, notif_a_d, notif_a_clk, notif_a_me;
reg  MEB_buf_L;
reg  WEB_buf_L;
reg [7:0] ADR_buf_L;
reg [7:0] preADR;
reg ADRFLAGA;
wire CHKEN1A;
wire CHKEN2A;
wire CHKEN3A;
wire CHKEN4A;
wire CHKEN7A;
wire [3:0]  RM_buf;
reg notif_a_rm;

buf ADR_buf_u0 ( ADR_buf[0], ADR[0] );
buf ADR_buf_u1 ( ADR_buf[1], ADR[1] );
buf ADR_buf_u2 ( ADR_buf[2], ADR[2] );
buf ADR_buf_u3 ( ADR_buf[3], ADR[3] );
buf ADR_buf_u4 ( ADR_buf[4], ADR[4] );
buf ADR_buf_u5 ( ADR_buf[5], ADR[5] );
buf ADR_buf_u6 ( ADR_buf[6], ADR[6] );
buf ADR_buf_u7 ( ADR_buf[7], ADR[7] );

buf D_buf_u0 ( D_buf[0], D[0] );
buf D_buf_u1 ( D_buf[1], D[1] );
buf D_buf_u2 ( D_buf[2], D[2] );
buf D_buf_u3 ( D_buf[3], D[3] );
buf D_buf_u4 ( D_buf[4], D[4] );
buf D_buf_u5 ( D_buf[5], D[5] );
buf D_buf_u6 ( D_buf[6], D[6] );
buf D_buf_u7 ( D_buf[7], D[7] );
buf D_buf_u8 ( D_buf[8], D[8] );
buf D_buf_u9 ( D_buf[9], D[9] );
buf D_buf_u10 ( D_buf[10], D[10] );
buf D_buf_u11 ( D_buf[11], D[11] );
buf D_buf_u12 ( D_buf[12], D[12] );
buf D_buf_u13 ( D_buf[13], D[13] );
buf D_buf_u14 ( D_buf[14], D[14] );
buf D_buf_u15 ( D_buf[15], D[15] );
buf D_buf_u16 ( D_buf[16], D[16] );
buf D_buf_u17 ( D_buf[17], D[17] );
buf D_buf_u18 ( D_buf[18], D[18] );
buf D_buf_u19 ( D_buf[19], D[19] );
buf D_buf_u20 ( D_buf[20], D[20] );
buf D_buf_u21 ( D_buf[21], D[21] );

buf WEB_buf_u0 ( WEB_buf, WEB );
buf OEB_buf_u0 ( OEB_buf, OEB );
buf MEB_buf_u0 ( MEB_buf, MEB );
buf CLK_buf_u0 ( CLK_buf, CLK );


buf RM_buf_u0 ( RM_buf[0], RM[0] );
buf RM_buf_u1 ( RM_buf[1], RM[1] );
buf RM_buf_u2 ( RM_buf[2], RM[2] );
buf RM_buf_u3 ( RM_buf[3], RM[3] );
IntComp_U018SRA12X_256X22M4 #(MES_ALL,8, 22, 256, 4) u0  ( Q_tmp, CLK_buf,  ADR_buf, D_buf, WEB_buf, OEB_buf, MEB_buf, RM_buf, notif_a_we, notif_a_adr, notif_a_d, notif_a_me,   notif_a_rm, notif_a_clk);
and u_OEB_0 (OEB_eff, !OEB_buf, 1'b1);
function is_adr_Valid;  // 1-bit return value

 input [7:0] addr_to_check;

 integer addr_bit_count;
 reg     ret_value;
 begin
  ret_value = `True;
  for (addr_bit_count = 0; addr_bit_count <= 7; addr_bit_count = addr_bit_count +1)
   if ((addr_to_check[addr_bit_count] !== 1'b0) && (addr_to_check[addr_bit_count] !== 1'b1))
    ret_value = `False;
   if((addr_to_check > 255) || (addr_to_check < 0))
    begin
     ret_value = `False;
    end
   is_adr_Valid = ret_value;
 end

endfunction


initial
 begin
  MEB_buf_L = 1'b1;
  ADRFLAGA = 1;
 end

always @( ADR_buf or negedge CLK_buf)
 begin
   if ( CLK_buf == 1'b0) preADR = ADR_buf;
 end

always @( posedge CLK_buf)
 begin
  MEB_buf_L = MEB_buf;
  WEB_buf_L = WEB_buf;

 end



always @ ( ADR_buf )
 begin
  if ( $realtime != 0) begin
   if (( ADR_buf > 255) || (ADR_buf < 0 ))
    ADRFLAGA = 0;
   else
    ADRFLAGA = 1;
  end
 end

buf #0.001 u_OEBeff_del_0 ( OEBeff_del, OEB_eff );

bufif1 u_Q_0 (Q[0], Q_tmp[0], OEBeff_del);
bufif1 u_Q_1 (Q[1], Q_tmp[1], OEBeff_del);
bufif1 u_Q_2 (Q[2], Q_tmp[2], OEBeff_del);
bufif1 u_Q_3 (Q[3], Q_tmp[3], OEBeff_del);
bufif1 u_Q_4 (Q[4], Q_tmp[4], OEBeff_del);
bufif1 u_Q_5 (Q[5], Q_tmp[5], OEBeff_del);
bufif1 u_Q_6 (Q[6], Q_tmp[6], OEBeff_del);
bufif1 u_Q_7 (Q[7], Q_tmp[7], OEBeff_del);
bufif1 u_Q_8 (Q[8], Q_tmp[8], OEBeff_del);
bufif1 u_Q_9 (Q[9], Q_tmp[9], OEBeff_del);
bufif1 u_Q_10 (Q[10], Q_tmp[10], OEBeff_del);
bufif1 u_Q_11 (Q[11], Q_tmp[11], OEBeff_del);
bufif1 u_Q_12 (Q[12], Q_tmp[12], OEBeff_del);
bufif1 u_Q_13 (Q[13], Q_tmp[13], OEBeff_del);
bufif1 u_Q_14 (Q[14], Q_tmp[14], OEBeff_del);
bufif1 u_Q_15 (Q[15], Q_tmp[15], OEBeff_del);
bufif1 u_Q_16 (Q[16], Q_tmp[16], OEBeff_del);
bufif1 u_Q_17 (Q[17], Q_tmp[17], OEBeff_del);
bufif1 u_Q_18 (Q[18], Q_tmp[18], OEBeff_del);
bufif1 u_Q_19 (Q[19], Q_tmp[19], OEBeff_del);
bufif1 u_Q_20 (Q[20], Q_tmp[20], OEBeff_del);
bufif1 u_Q_21 (Q[21], Q_tmp[21], OEBeff_del);
and u_CHKEN1A_0 ( CHKEN1A, !MEB_buf, ADRFLAGA);
and u_CHKEN4A_0 ( CHKEN4A, !MEB_buf, ADRFLAGA, !WEB_buf );
buf u_CHKEN3A_0 ( CHKEN3A, !MEB_buf );

buf u_CHKEN2A_0 ( CHKEN2A, ADRFLAGA );

// The following is Timing-dependent section of this model

specify

    specparam PATHPULSE$OEB$Q = ( 0, 0.001 );
    specparam
        Tcq  =  2.027,
        Tcqx =  0.996,

        Toq  =  0.825,
        Toqz =  0.876,

        Tcc  =  2.047,
        Tcl  =  0.579,
        Tch  =  0.662,

        Tac  =  0.409,
        Tcax =  0.000,

        Twc  =  0.350,
        Tcwx =  0.000,
        Tdc  =  0.350,
        Tcdx =  0.189,

        Trmc =  0.817,
        Tcrmx =  2.047,

        Tmc  =  0.360,
        Tcmx =  0.000;

                                            /* using the Z transition place holder for Tcqx so that we
                                               can control the X-transition of the output */

    if (  !OEB )
       ( posedge CLK => ( Q[0] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[1] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[2] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[3] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[4] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[5] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[6] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[7] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[8] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[9] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[10] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[11] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[12] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[13] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[14] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[15] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[16] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[17] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[18] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[19] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[20] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[21] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);

       ( OEB => Q[0] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[1] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[2] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[3] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[4] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[5] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[6] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[7] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[8] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[9] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[10] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[11] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[12] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[13] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[14] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[15] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[16] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[17] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[18] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[19] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[20] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[21] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);

// Timing Checks
    $width (negedge CLK, Tcl, 0, notif_a_clk);
    $width (posedge CLK, Tch, 0, notif_a_clk);
    $period (negedge CLK, Tcc, notif_a_clk);
    $period (posedge CLK, Tcc, notif_a_clk);
    $setuphold (posedge CLK &&& CHKEN3A, ADR[0], Tac, Tcax, notif_a_adr );
    $setuphold (posedge CLK &&& CHKEN3A, ADR[1], Tac, Tcax, notif_a_adr );
    $setuphold (posedge CLK &&& CHKEN3A, ADR[2], Tac, Tcax, notif_a_adr );
    $setuphold (posedge CLK &&& CHKEN3A, ADR[3], Tac, Tcax, notif_a_adr );
    $setuphold (posedge CLK &&& CHKEN3A, ADR[4], Tac, Tcax, notif_a_adr );
    $setuphold (posedge CLK &&& CHKEN3A, ADR[5], Tac, Tcax, notif_a_adr );
    $setuphold (posedge CLK &&& CHKEN3A, ADR[6], Tac, Tcax, notif_a_adr );
    $setuphold (posedge CLK &&& CHKEN3A, ADR[7], Tac, Tcax, notif_a_adr );

    $setuphold (posedge CLK &&& CHKEN2A, MEB, Tmc, Tcmx, notif_a_me );

    $setuphold (posedge CLK &&& CHKEN1A, WEB, Twc, Tcwx, notif_a_we );

    $setuphold (posedge CLK &&& CHKEN4A, D[0], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[1], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[2], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[3], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[4], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[5], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[6], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[7], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[8], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[9], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[10], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[11], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[12], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[13], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[14], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[15], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[16], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[17], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[18], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[19], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[20], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[21], Tdc, Tcdx, notif_a_d );

    $setuphold (posedge CLK &&& CHKEN1A, RM[0], Trmc, Tcrmx, notif_a_rm );
    $setuphold (posedge CLK &&& CHKEN1A, RM[1], Trmc, Tcrmx, notif_a_rm );
    $setuphold (posedge CLK &&& CHKEN1A, RM[2], Trmc, Tcrmx, notif_a_rm );
    $setuphold (posedge CLK &&& CHKEN1A, RM[3], Trmc, Tcrmx, notif_a_rm );


endspecify

endmodule

`disable_portfaults 
`nosuppress_faults 
`endcelldefine

// The following is the sub-module used in definition of overall memory module
// ===========================================================================
module IntComp_U018SRA12X_256X22M4   ( Q_tmp, CLK_buf,  ADR_buf, D_buf, WEB_buf, OEB_buf, MEB_buf, RM_buf, notif_a_we, notif_a_adr, notif_a_d, notif_a_me,   notif_a_rm, notif_a_clk); 
parameter MES_ALL = "ON", A_bit=8, I_bit=22, I_word=256, rm_bit=4;

output  [I_bit -1:0]  Q_tmp;
reg  [I_bit -1:0]  Q_tmp;
reg  [I_bit -1:0]  Q_local;


wire   [I_bit -1:0]  Q_temp;
reg [I_bit-1:0] allX , allZ ;

input CLK_buf;
input  [A_bit -1:0]  ADR_buf;
input  [I_bit -1:0]  D_buf;
input WEB_buf;
input OEB_buf;
input MEB_buf;
input  [rm_bit -1:0]  RM_buf;

input notif_a_we, notif_a_adr, notif_a_d, notif_a_me, notif_a_clk;
input notif_a_rm;
integer i;
initial begin
        for ( i=0; i < I_bit ; i=i+1 ) begin
                allX[i] = 1'bx ; allZ[i] = 1'bz ;
        end
end 
reg [I_bit -1:0] mem_core_array [0:I_word -1];
reg [I_bit -1:0] X_a_corrupt;

reg
        flaga_read_ok,
        flaga_we_ok,
        flaga_d_ok,
        flaga_wem_ok,
        flaga_rm_ok,
        flaga_adr_ok,
        flaga_range_ok;
reg     flaga_adr_viol;
reg     flaga_clk_valid;
reg     flaga_adr_x;

event   ev_read_out_a_port;

reg[A_bit -1:0] ADRlatched;
reg[I_bit -1:0] Dlatched;
reg WEBlatched;
reg MEBlatched;
reg MEB_chk;
real CLKA_T;

function is_adr_Valid;  // 1-bit return value

 input [A_bit -1:0] addr_to_check;

 integer addr_bit_count;
 reg     ret_value;
 begin
  ret_value = `True;
  for (addr_bit_count = 0; addr_bit_count <= A_bit -1; addr_bit_count = addr_bit_count +1)
   if ((addr_to_check[addr_bit_count] !== 1'b0) && (addr_to_check[addr_bit_count] !== 1'b1)) begin
    flaga_adr_x = `True;
    ret_value = `False;
   end
   if((addr_to_check > I_word -1) || (addr_to_check < 0))
    begin
     ret_value = `False;
    end
   is_adr_Valid = ret_value;
 end

endfunction

function is_x;  // 1-bit return value

 input [I_bit -1:0] addr_to_check;

 integer addr_bit_count;
 reg     ret_value;
 begin
  ret_value = `False;
  for (addr_bit_count = 0; addr_bit_count <= I_bit -1; addr_bit_count = addr_bit_count +1)
   if ((addr_to_check[addr_bit_count] !== 1'b0) && (addr_to_check[addr_bit_count] !== 1'b1)) begin
    ret_value = `True;
   end
   is_x = ret_value;
 end

endfunction

// -------------------------------------------------------------------
// Common tasks
// -------------------------------------------------------------------


task corrupt_all_loc;

 input flag_range_ok;
 integer addr_index;
 begin
                                     // do not corrupt entire memory if write to
                                    // out of range address, for all other cases
                                    // flag_range_ok is true, therefore corruption
                                    // will occur as before
  if( flag_range_ok == `True)
   begin
    for( addr_index = 0; addr_index < I_word; addr_index = addr_index + 1) begin
     mem_core_array[ addr_index] = allX;
    end
   end
 end

endtask

task corrupt_cur_loc;

 input[A_bit -1:0] loc_to_corrupt;

 if (is_adr_Valid (loc_to_corrupt))
  mem_core_array[loc_to_corrupt] = allX;
 else
  corrupt_all_loc(`True);

endtask


function is_rm_Valid;

 input [rm_bit -1:0] rm_to_chk;

 integer rm_bit_cnt;
 reg ret_val;

 begin
  ret_val = `True;
  for (rm_bit_cnt = 0; rm_bit_cnt < rm_bit; rm_bit_cnt = rm_bit_cnt + 1)
   if ( (rm_to_chk[rm_bit_cnt] !== 1'b0) && (rm_to_chk[rm_bit_cnt] !== 1'b1) )
    ret_val = `False;
  is_rm_Valid = ret_val;
 end

endfunction



// Perform Sanity Check on Port A, Corrupt memory if required

task checkSanityOnAport;

 #0                // let CLOCK and NOTIFIER stuff execute first
 case ( {flaga_adr_ok, flaga_we_ok, flaga_d_ok} ) // only 1 and 0

  3'b111   : ;                                                // everything ok!!!

  3'b101,
  3'b100   : corrupt_cur_loc(ADRlatched);          // WE is unstable

  3'b110   : if (WEBlatched !== 1'b1)
              corrupt_cur_loc(ADRlatched);         // Data is unstable
  3'b000,
  3'b001   : corrupt_all_loc(flaga_range_ok);            // ADR and WE unstable
  3'b010,
  3'b011   : if (WEBlatched !== 1'b1)    // ADR unstable, WE stable
              corrupt_all_loc(flaga_range_ok);
 endcase
endtask

// PORT A FUNCTIONALITY (Handle violation behavior)

initial
 begin
  flaga_adr_ok  = `True;
  flaga_range_ok = `True;
  flaga_we_ok   = `True;
  flaga_d_ok    = `True;
  flaga_wem_ok   = `True;
  flaga_read_ok = `True;
  flaga_rm_ok = `True;
  flaga_adr_viol = `True;
  flaga_clk_valid = `True;
  flaga_adr_x = `False;
 end

always @(notif_a_we)            // PORT A WE violation
 begin
  if ( $realtime == CLKA_T )
   MEB_chk = MEB_buf;
  else
   MEB_chk = MEBlatched;
  if (MEB_chk !== 1'b1)
   begin
    flaga_we_ok = `False;
    checkSanityOnAport;
    disable OUTPUT_a;
    flaga_read_ok = `False;
    -> ev_read_out_a_port;
   end
 end

always @(notif_a_clk)            // PORT A CLK violation
 begin
  disable OUTPUT_a;
  flaga_adr_ok = `False;
  flaga_we_ok  = `False;
  checkSanityOnAport;
  flaga_read_ok = `False;     // irrespective of WE
  -> ev_read_out_a_port;
 end

always @(notif_a_me)            // PORT A ME violation
 begin
  flaga_adr_ok = `False;
  flaga_read_ok = `False;     // irrespective of WE
  disable OUTPUT_a;
  checkSanityOnAport;
  -> ev_read_out_a_port;
 end

always @(notif_a_adr)           // PORT A ADR violation
 begin
  if ( $realtime == CLKA_T )
   MEB_chk = MEB_buf;
  else
   MEB_chk = MEBlatched;
  if (MEB_chk !== 1'b1)
   begin
    if (WEBlatched === 1'b1)
      begin
    corrupt_all_loc(`True);
    flaga_read_ok = `False; // irrespective of WE
    disable OUTPUT_a;
    -> ev_read_out_a_port;
      end
     else  begin
    flaga_adr_ok = `False;
    flaga_read_ok = `False; // irrespective of WE
    disable OUTPUT_a;
    checkSanityOnAport;
    -> ev_read_out_a_port;
      end
   end
 end

always @(notif_a_d)             // PORT A D violation
 begin
  if ( $realtime == CLKA_T )
   MEB_chk = MEB_buf;
  else
   MEB_chk = MEBlatched;
  if (MEB_chk !== 1'b1)
   begin
    flaga_d_ok = `False;
    disable OUTPUT_a;
    checkSanityOnAport;
    flaga_read_ok = `False;
    -> ev_read_out_a_port;
   end
 end

always @(notif_a_rm)               //PORT A RM violation
 begin
  if ( $realtime == CLKA_T )
   MEB_chk = MEB_buf;
  else
   MEB_chk = MEBlatched;
  if (MEB_chk !== 1'b1)
   begin
    disable OUTPUT_a;
    flaga_read_ok = `False;
    -> ev_read_out_a_port;
    flaga_d_ok  = `False;
    checkSanityOnAport;
   end
 end




always @(negedge CLK_buf)          // reset for next cycle
 begin
  if ( CLK_buf !== 1'bx ) begin
   #0.001;
   flaga_range_ok  = `True;
   flaga_read_ok = `True;
   flaga_rm_ok = `True;
   flaga_adr_viol = `True;
   flaga_clk_valid = `True;
   flaga_adr_x = `False;
  end
  else
   begin
    if( MES_ALL=="ON" && $realtime != 0) begin
//     $display("<<CLK unknown>>");
//     $display("      time=%t; instance=%m (U018SRA12X_256X22M4_core)",$realtime);
    end
    flaga_clk_valid = `False;
    Q_local = allX;
    corrupt_all_loc(`True);
   end
 end

// PORT A FUNCTIONALITY (Handle normal read/write)

always @(posedge CLK_buf)
 begin
   flaga_adr_ok  = `True;
   flaga_we_ok   = `True;
   flaga_d_ok    = `True;
   flaga_wem_ok   = `True;
  if ( CLK_buf === 1'bx) begin
   if( MES_ALL=="ON" && $realtime != 0) begin
//    $display("<<CLK unknown>>");
//    $display("      time=%t; instance=%m (U018SRA12X_256X22M4_core)",$realtime);
   end
    flaga_clk_valid = `False;
    Q_local = allX;
    corrupt_all_loc(`True);
  end
  CLKA_T = $realtime;
  MEBlatched = MEB_buf;
  WEBlatched = WEB_buf;
  ADRlatched = ADR_buf;
  Dlatched = D_buf;
   if( MES_ALL=="ON" && $realtime != 0) begin
    if ( is_x(Dlatched) && WEBlatched == 1'b0 && MEBlatched == 1'b0) begin
//      $display("<<D unknown>>");
//      $display("      time=%t; instance=%m (U018SRA12X_256X22M4_core)",$realtime);
    end  
   end 
  if (!flaga_clk_valid)
   Q_local = allX;
  else if ( CLK_buf === 1'bx )
   begin
    if( MES_ALL=="ON" && $realtime != 0) begin
//     $display("<<CLK unknown>>");
//     $display("      time=%t; instance=%m (U018SRA12X_256X22M4_core)",$realtime);
    end
    Q_local = allX;
    corrupt_all_loc(flaga_range_ok);
   end
  else
   begin
    if (MEB_buf !== 1'b1)
     begin
      if (WEB_buf === 1'bx) begin
        if( MES_ALL=="ON" && $realtime != 0) begin
//          $display("<<WEB unknown>>");
//          $display("      time=%t; instance=%m (U018SRA12X_256X22M4_core)",$realtime);
        end
       flaga_we_ok = `False;
      end
      if (MEB_buf !== 1'b0)
       flaga_we_ok = `False;       // don't know if cycle is On or Off
      if (is_adr_Valid(ADRlatched) != `True ) begin
        if( MES_ALL=="ON" && $realtime != 0) begin
         if ( flaga_adr_x ) begin
//           $display("<<ADR unknown>>");
//           $display("      time=%t; instance=%m (U018SRA12X_256X22M4_core)",$realtime);
         end
        end
       flaga_adr_ok = `False;
      if(((ADRlatched > I_word -1)|| ( ADRlatched < 0)) && (MEBlatched != 1'b1))
       begin
        $display("\n%m WARNING:address is out of range\n RANGE:0 to 255\n");
        flaga_range_ok = `False;
       end
      else begin
        if (WEBlatched === 1'b1 && MEB_buf === 1'b0 )
         corrupt_all_loc(`True);
      end
      end
      if ( MEB_buf === 1'bx ) begin
        if( MES_ALL=="ON" && $realtime != 0) begin
//          $display("<<MEB unknown>>");
//          $display("      time=%t; instance=%m (U018SRA12X_256X22M4_core)",$realtime);
        end
       Q_local = allX;
      if (WEB_buf !== 1'b1)
       corrupt_all_loc(flaga_range_ok);
      end
      else
       begin
        if (flaga_we_ok && flaga_adr_ok && flaga_d_ok && (WEB_buf == 1'b0))
         mem_core_array[ADRlatched] = Dlatched;  // write data in
        else
         begin
          checkSanityOnAport;
         end
        -> ev_read_out_a_port;
       end
     end
   end
 end


// PORT A READ-OUT

always @(ev_read_out_a_port)
 begin
  #0                // let CLOCK and NOTIFIER module execute first
  if (is_rm_Valid(RM_buf) != `True)
   flaga_rm_ok = `False;
  if (flaga_adr_ok && (ADRlatched <= I_word -1) && flaga_read_ok && flaga_rm_ok) begin : OUTPUT_a
   if (WEBlatched == 1'b0) begin
    Q_local = allX;
    #0.001
    Q_local = Dlatched;
   end
   else
    begin
     Q_local = allX;
     #0.001
      Q_local = mem_core_array[ADRlatched];
    end
  end
  else
   begin
        if (is_adr_Valid(ADRlatched) != `True && WEBlatched == 1'b0) begin
    Q_local = allX;
    #0.001
    Q_local = Dlatched;
   end
   else
    Q_local = allX;
    flaga_read_ok = `True;
   end
 end
assign Q_temp = Q_local;

always @( Q_temp or OEB_buf )
 begin
  if ( OEB_buf == 1'b0 )
   Q_tmp = Q_temp;
  else if ( OEB_buf == 1'b1 )
   begin
    Q_tmp = allX;
    #0.001;
    if ( OEB_buf == 1'b0 )
      Q_tmp = Q_temp;
    else if ( OEB_buf == 1'b1 )
      Q_tmp = allZ;
    else begin
      Q_tmp = allX;
    end
   end
  else begin
   Q_tmp = allX;
  end
 end
always @( OEB_buf )
 begin
  if ( OEB_buf ===  1'bx )
  begin
      if( MES_ALL=="ON" && $realtime != 0) begin
//        $display("<<OEB unknown>>");
//        $display("      time=%t; instance=%m (U018SRA12X_256X22M4_core)",$realtime);
      end
  end
 end


endmodule
