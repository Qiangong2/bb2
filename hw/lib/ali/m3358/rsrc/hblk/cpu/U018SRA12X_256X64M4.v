//---------------------------------------------------------------------
//               Copyright(c) Virage Logic Corporation.                
//     All Rights reserved - Unpublished -rights reserved under        
//     the Copyright laws of the United States of America.             
//                                                                     
//  This file includes the Confidential information of Virage Logic    
//  and UMC                                                            
//  The receiver of this Confidential Information shall not disclose   
//  it to any third party and shall protect its confidentiality by     
//  using the same degree of care, but not less then a reasonable      
//  degree of care, as the receiver uses to protect receiver's own     
//  Confidential Information.                                          
//                                                                     
//                    Virage Logic Corporation.                        
//                    46501 Landing Parkway                            
//                    Fremont , California 94538.                      
//                                                                     
//---------------------------------------------------------------------
//                                                                     
//  Software           : Rev: 3.1.3 (build REL-3-1-3-2002-06-30)       
//  Library Format     : Rev: 1.05.00                                  
//  Compiler Name      : um18s1p11hssb05                               
//  Date of Generation : Wed Jun 25 14:02:08 CST 2003                  
//                                                                     
//---------------------------------------------------------------------
//   --------------------------------------------------------------     
//                       Template Revision : 1.2.0                      
//   --------------------------------------------------------------     
//  ************************************************************
//     *    Synchronous, 1-Port Read/Write RAM      *
//            *  Verilog Behavioral Model  *
// *************************************************************
//
// THIS IS A SYNCHRONOUS 1-PORT MEMORY MODEL
// WHICH TAKES BOTH TIMING CHECKS AND DELAY DATA FROM SDF ONLY
//
// Memory Name:       U018SRA12X_256X64M4
// Memory Size:       256 words x 64 bits
//
//                 PORT NAME
//                 ---------
// Input Ports:
//
//                 ADR [7:0]
//                 D  [63:0]
//                 WEB
//                 OEB
//                 MEB
//                 CLK
//                 RM  [3:0]

// Output Ports:
//
//                 Q [63:0]
//
// *************************************************************
`resetall 

`timescale 1 ns / 1 ps 
`celldefine 
`suppress_faults 
`enable_portfaults 

`define True    1'b1
`define False   1'b0
module U018SRA12X_256X64M4 ( Q, ADR, D,WEB, OEB, MEB,  CLK, RM);

parameter MES_ALL = "ON";

output [63:0] Q;
wire [63:0] Q;

input [7:0] ADR;
input [63:0] D;
input WEB;
input OEB;
input MEB;
input CLK;
input [3:0] RM;

// Local registers, wires, etc

wire [63:0]    Q_tmp;
wire [7:0]    ADR_buf;
wire [63:0]    D_buf;
wire OEB_eff;
wire OEBeff_del;
reg  notif_a_we, notif_a_adr, notif_a_d, notif_a_clk, notif_a_me;
reg  MEB_buf_L;
reg  WEB_buf_L;
reg [7:0] ADR_buf_L;
reg [7:0] preADR;
reg ADRFLAGA;
wire CHKEN1A;
wire CHKEN2A;
wire CHKEN3A;
wire CHKEN4A;
wire CHKEN7A;
wire [3:0]  RM_buf;
reg notif_a_rm;

buf ADR_buf_u0 ( ADR_buf[0], ADR[0] );
buf ADR_buf_u1 ( ADR_buf[1], ADR[1] );
buf ADR_buf_u2 ( ADR_buf[2], ADR[2] );
buf ADR_buf_u3 ( ADR_buf[3], ADR[3] );
buf ADR_buf_u4 ( ADR_buf[4], ADR[4] );
buf ADR_buf_u5 ( ADR_buf[5], ADR[5] );
buf ADR_buf_u6 ( ADR_buf[6], ADR[6] );
buf ADR_buf_u7 ( ADR_buf[7], ADR[7] );

buf D_buf_u0 ( D_buf[0], D[0] );
buf D_buf_u1 ( D_buf[1], D[1] );
buf D_buf_u2 ( D_buf[2], D[2] );
buf D_buf_u3 ( D_buf[3], D[3] );
buf D_buf_u4 ( D_buf[4], D[4] );
buf D_buf_u5 ( D_buf[5], D[5] );
buf D_buf_u6 ( D_buf[6], D[6] );
buf D_buf_u7 ( D_buf[7], D[7] );
buf D_buf_u8 ( D_buf[8], D[8] );
buf D_buf_u9 ( D_buf[9], D[9] );
buf D_buf_u10 ( D_buf[10], D[10] );
buf D_buf_u11 ( D_buf[11], D[11] );
buf D_buf_u12 ( D_buf[12], D[12] );
buf D_buf_u13 ( D_buf[13], D[13] );
buf D_buf_u14 ( D_buf[14], D[14] );
buf D_buf_u15 ( D_buf[15], D[15] );
buf D_buf_u16 ( D_buf[16], D[16] );
buf D_buf_u17 ( D_buf[17], D[17] );
buf D_buf_u18 ( D_buf[18], D[18] );
buf D_buf_u19 ( D_buf[19], D[19] );
buf D_buf_u20 ( D_buf[20], D[20] );
buf D_buf_u21 ( D_buf[21], D[21] );
buf D_buf_u22 ( D_buf[22], D[22] );
buf D_buf_u23 ( D_buf[23], D[23] );
buf D_buf_u24 ( D_buf[24], D[24] );
buf D_buf_u25 ( D_buf[25], D[25] );
buf D_buf_u26 ( D_buf[26], D[26] );
buf D_buf_u27 ( D_buf[27], D[27] );
buf D_buf_u28 ( D_buf[28], D[28] );
buf D_buf_u29 ( D_buf[29], D[29] );
buf D_buf_u30 ( D_buf[30], D[30] );
buf D_buf_u31 ( D_buf[31], D[31] );
buf D_buf_u32 ( D_buf[32], D[32] );
buf D_buf_u33 ( D_buf[33], D[33] );
buf D_buf_u34 ( D_buf[34], D[34] );
buf D_buf_u35 ( D_buf[35], D[35] );
buf D_buf_u36 ( D_buf[36], D[36] );
buf D_buf_u37 ( D_buf[37], D[37] );
buf D_buf_u38 ( D_buf[38], D[38] );
buf D_buf_u39 ( D_buf[39], D[39] );
buf D_buf_u40 ( D_buf[40], D[40] );
buf D_buf_u41 ( D_buf[41], D[41] );
buf D_buf_u42 ( D_buf[42], D[42] );
buf D_buf_u43 ( D_buf[43], D[43] );
buf D_buf_u44 ( D_buf[44], D[44] );
buf D_buf_u45 ( D_buf[45], D[45] );
buf D_buf_u46 ( D_buf[46], D[46] );
buf D_buf_u47 ( D_buf[47], D[47] );
buf D_buf_u48 ( D_buf[48], D[48] );
buf D_buf_u49 ( D_buf[49], D[49] );
buf D_buf_u50 ( D_buf[50], D[50] );
buf D_buf_u51 ( D_buf[51], D[51] );
buf D_buf_u52 ( D_buf[52], D[52] );
buf D_buf_u53 ( D_buf[53], D[53] );
buf D_buf_u54 ( D_buf[54], D[54] );
buf D_buf_u55 ( D_buf[55], D[55] );
buf D_buf_u56 ( D_buf[56], D[56] );
buf D_buf_u57 ( D_buf[57], D[57] );
buf D_buf_u58 ( D_buf[58], D[58] );
buf D_buf_u59 ( D_buf[59], D[59] );
buf D_buf_u60 ( D_buf[60], D[60] );
buf D_buf_u61 ( D_buf[61], D[61] );
buf D_buf_u62 ( D_buf[62], D[62] );
buf D_buf_u63 ( D_buf[63], D[63] );

buf WEB_buf_u0 ( WEB_buf, WEB );
buf OEB_buf_u0 ( OEB_buf, OEB );
buf MEB_buf_u0 ( MEB_buf, MEB );
buf CLK_buf_u0 ( CLK_buf, CLK );


buf RM_buf_u0 ( RM_buf[0], RM[0] );
buf RM_buf_u1 ( RM_buf[1], RM[1] );
buf RM_buf_u2 ( RM_buf[2], RM[2] );
buf RM_buf_u3 ( RM_buf[3], RM[3] );
IntComp_U018SRA12X_256X64M4 #(MES_ALL,8, 64, 256, 4) u0  ( Q_tmp, CLK_buf,  ADR_buf, D_buf, WEB_buf, OEB_buf, MEB_buf, RM_buf, notif_a_we, notif_a_adr, notif_a_d, notif_a_me,   notif_a_rm, notif_a_clk);
and u_OEB_0 (OEB_eff, !OEB_buf, 1'b1);
function is_adr_Valid;  // 1-bit return value

 input [7:0] addr_to_check;

 integer addr_bit_count;
 reg     ret_value;
 begin
  ret_value = `True;
  for (addr_bit_count = 0; addr_bit_count <= 7; addr_bit_count = addr_bit_count +1)
   if ((addr_to_check[addr_bit_count] !== 1'b0) && (addr_to_check[addr_bit_count] !== 1'b1))
    ret_value = `False;
   if((addr_to_check > 255) || (addr_to_check < 0))
    begin
     ret_value = `False;
    end
   is_adr_Valid = ret_value;
 end

endfunction


initial
 begin
  MEB_buf_L = 1'b1;
  ADRFLAGA = 1;
 end

always @( ADR_buf or negedge CLK_buf)
 begin
   if ( CLK_buf == 1'b0) preADR = ADR_buf;
 end

always @( posedge CLK_buf)
 begin
  MEB_buf_L = MEB_buf;
  WEB_buf_L = WEB_buf;

 end



always @ ( ADR_buf )
 begin
  if ( $realtime != 0) begin
   if (( ADR_buf > 255) || (ADR_buf < 0 ))
    ADRFLAGA = 0;
   else
    ADRFLAGA = 1;
  end
 end

buf #0.001 u_OEBeff_del_0 ( OEBeff_del, OEB_eff );

bufif1 u_Q_0 (Q[0], Q_tmp[0], OEBeff_del);
bufif1 u_Q_1 (Q[1], Q_tmp[1], OEBeff_del);
bufif1 u_Q_2 (Q[2], Q_tmp[2], OEBeff_del);
bufif1 u_Q_3 (Q[3], Q_tmp[3], OEBeff_del);
bufif1 u_Q_4 (Q[4], Q_tmp[4], OEBeff_del);
bufif1 u_Q_5 (Q[5], Q_tmp[5], OEBeff_del);
bufif1 u_Q_6 (Q[6], Q_tmp[6], OEBeff_del);
bufif1 u_Q_7 (Q[7], Q_tmp[7], OEBeff_del);
bufif1 u_Q_8 (Q[8], Q_tmp[8], OEBeff_del);
bufif1 u_Q_9 (Q[9], Q_tmp[9], OEBeff_del);
bufif1 u_Q_10 (Q[10], Q_tmp[10], OEBeff_del);
bufif1 u_Q_11 (Q[11], Q_tmp[11], OEBeff_del);
bufif1 u_Q_12 (Q[12], Q_tmp[12], OEBeff_del);
bufif1 u_Q_13 (Q[13], Q_tmp[13], OEBeff_del);
bufif1 u_Q_14 (Q[14], Q_tmp[14], OEBeff_del);
bufif1 u_Q_15 (Q[15], Q_tmp[15], OEBeff_del);
bufif1 u_Q_16 (Q[16], Q_tmp[16], OEBeff_del);
bufif1 u_Q_17 (Q[17], Q_tmp[17], OEBeff_del);
bufif1 u_Q_18 (Q[18], Q_tmp[18], OEBeff_del);
bufif1 u_Q_19 (Q[19], Q_tmp[19], OEBeff_del);
bufif1 u_Q_20 (Q[20], Q_tmp[20], OEBeff_del);
bufif1 u_Q_21 (Q[21], Q_tmp[21], OEBeff_del);
bufif1 u_Q_22 (Q[22], Q_tmp[22], OEBeff_del);
bufif1 u_Q_23 (Q[23], Q_tmp[23], OEBeff_del);
bufif1 u_Q_24 (Q[24], Q_tmp[24], OEBeff_del);
bufif1 u_Q_25 (Q[25], Q_tmp[25], OEBeff_del);
bufif1 u_Q_26 (Q[26], Q_tmp[26], OEBeff_del);
bufif1 u_Q_27 (Q[27], Q_tmp[27], OEBeff_del);
bufif1 u_Q_28 (Q[28], Q_tmp[28], OEBeff_del);
bufif1 u_Q_29 (Q[29], Q_tmp[29], OEBeff_del);
bufif1 u_Q_30 (Q[30], Q_tmp[30], OEBeff_del);
bufif1 u_Q_31 (Q[31], Q_tmp[31], OEBeff_del);
bufif1 u_Q_32 (Q[32], Q_tmp[32], OEBeff_del);
bufif1 u_Q_33 (Q[33], Q_tmp[33], OEBeff_del);
bufif1 u_Q_34 (Q[34], Q_tmp[34], OEBeff_del);
bufif1 u_Q_35 (Q[35], Q_tmp[35], OEBeff_del);
bufif1 u_Q_36 (Q[36], Q_tmp[36], OEBeff_del);
bufif1 u_Q_37 (Q[37], Q_tmp[37], OEBeff_del);
bufif1 u_Q_38 (Q[38], Q_tmp[38], OEBeff_del);
bufif1 u_Q_39 (Q[39], Q_tmp[39], OEBeff_del);
bufif1 u_Q_40 (Q[40], Q_tmp[40], OEBeff_del);
bufif1 u_Q_41 (Q[41], Q_tmp[41], OEBeff_del);
bufif1 u_Q_42 (Q[42], Q_tmp[42], OEBeff_del);
bufif1 u_Q_43 (Q[43], Q_tmp[43], OEBeff_del);
bufif1 u_Q_44 (Q[44], Q_tmp[44], OEBeff_del);
bufif1 u_Q_45 (Q[45], Q_tmp[45], OEBeff_del);
bufif1 u_Q_46 (Q[46], Q_tmp[46], OEBeff_del);
bufif1 u_Q_47 (Q[47], Q_tmp[47], OEBeff_del);
bufif1 u_Q_48 (Q[48], Q_tmp[48], OEBeff_del);
bufif1 u_Q_49 (Q[49], Q_tmp[49], OEBeff_del);
bufif1 u_Q_50 (Q[50], Q_tmp[50], OEBeff_del);
bufif1 u_Q_51 (Q[51], Q_tmp[51], OEBeff_del);
bufif1 u_Q_52 (Q[52], Q_tmp[52], OEBeff_del);
bufif1 u_Q_53 (Q[53], Q_tmp[53], OEBeff_del);
bufif1 u_Q_54 (Q[54], Q_tmp[54], OEBeff_del);
bufif1 u_Q_55 (Q[55], Q_tmp[55], OEBeff_del);
bufif1 u_Q_56 (Q[56], Q_tmp[56], OEBeff_del);
bufif1 u_Q_57 (Q[57], Q_tmp[57], OEBeff_del);
bufif1 u_Q_58 (Q[58], Q_tmp[58], OEBeff_del);
bufif1 u_Q_59 (Q[59], Q_tmp[59], OEBeff_del);
bufif1 u_Q_60 (Q[60], Q_tmp[60], OEBeff_del);
bufif1 u_Q_61 (Q[61], Q_tmp[61], OEBeff_del);
bufif1 u_Q_62 (Q[62], Q_tmp[62], OEBeff_del);
bufif1 u_Q_63 (Q[63], Q_tmp[63], OEBeff_del);
and u_CHKEN1A_0 ( CHKEN1A, !MEB_buf, ADRFLAGA);
and u_CHKEN4A_0 ( CHKEN4A, !MEB_buf, ADRFLAGA, !WEB_buf );
buf u_CHKEN3A_0 ( CHKEN3A, !MEB_buf );

buf u_CHKEN2A_0 ( CHKEN2A, ADRFLAGA );

// The following is Timing-dependent section of this model

specify

    specparam PATHPULSE$OEB$Q = ( 0, 0.001 );
    specparam
        Tcq  =  2.174,
        Tcqx =  1.050,

        Toq  =  0.888,
        Toqz =  0.940,

        Tcc  =  2.196,
        Tcl  =  0.579,
        Tch  =  0.672,

        Tac  =  0.394,
        Tcax =  0.000,

        Twc  =  0.350,
        Tcwx =  0.011,
        Tdc  =  0.350,
        Tcdx =  0.268,

        Trmc =  0.789,
        Tcrmx =  2.196,

        Tmc  =  0.360,
        Tcmx =  0.000;

                                            /* using the Z transition place holder for Tcqx so that we
                                               can control the X-transition of the output */

    if (  !OEB )
       ( posedge CLK => ( Q[0] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[1] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[2] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[3] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[4] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[5] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[6] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[7] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[8] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[9] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[10] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[11] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[12] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[13] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[14] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[15] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[16] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[17] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[18] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[19] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[20] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[21] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[22] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[23] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[24] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[25] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[26] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[27] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[28] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[29] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[30] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[31] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[32] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[33] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[34] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[35] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[36] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[37] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[38] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[39] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[40] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[41] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[42] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[43] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[44] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[45] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[46] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[47] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[48] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[49] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[50] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[51] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[52] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[53] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[54] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[55] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[56] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[57] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[58] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[59] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[60] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[61] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[62] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);
    if (  !OEB )
       ( posedge CLK => ( Q[63] : 1'bx )) = (Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq);

       ( OEB => Q[0] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[1] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[2] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[3] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[4] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[5] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[6] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[7] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[8] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[9] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[10] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[11] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[12] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[13] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[14] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[15] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[16] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[17] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[18] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[19] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[20] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[21] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[22] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[23] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[24] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[25] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[26] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[27] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[28] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[29] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[30] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[31] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[32] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[33] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[34] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[35] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[36] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[37] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[38] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[39] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[40] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[41] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[42] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[43] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[44] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[45] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[46] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[47] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[48] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[49] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[50] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[51] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[52] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[53] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[54] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[55] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[56] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[57] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[58] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[59] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[60] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[61] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[62] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);
       ( OEB => Q[63] ) = (Toq, Toq, Toqz, Toq, Toqz, Toq);

// Timing Checks
    $width (negedge CLK, Tcl, 0, notif_a_clk);
    $width (posedge CLK, Tch, 0, notif_a_clk);
    $period (negedge CLK, Tcc, notif_a_clk);
    $period (posedge CLK, Tcc, notif_a_clk);
    $setuphold (posedge CLK &&& CHKEN3A, ADR[0], Tac, Tcax, notif_a_adr );
    $setuphold (posedge CLK &&& CHKEN3A, ADR[1], Tac, Tcax, notif_a_adr );
    $setuphold (posedge CLK &&& CHKEN3A, ADR[2], Tac, Tcax, notif_a_adr );
    $setuphold (posedge CLK &&& CHKEN3A, ADR[3], Tac, Tcax, notif_a_adr );
    $setuphold (posedge CLK &&& CHKEN3A, ADR[4], Tac, Tcax, notif_a_adr );
    $setuphold (posedge CLK &&& CHKEN3A, ADR[5], Tac, Tcax, notif_a_adr );
    $setuphold (posedge CLK &&& CHKEN3A, ADR[6], Tac, Tcax, notif_a_adr );
    $setuphold (posedge CLK &&& CHKEN3A, ADR[7], Tac, Tcax, notif_a_adr );

    $setuphold (posedge CLK &&& CHKEN2A, MEB, Tmc, Tcmx, notif_a_me );

    $setuphold (posedge CLK &&& CHKEN1A, WEB, Twc, Tcwx, notif_a_we );

    $setuphold (posedge CLK &&& CHKEN4A, D[0], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[1], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[2], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[3], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[4], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[5], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[6], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[7], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[8], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[9], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[10], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[11], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[12], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[13], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[14], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[15], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[16], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[17], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[18], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[19], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[20], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[21], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[22], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[23], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[24], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[25], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[26], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[27], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[28], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[29], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[30], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[31], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[32], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[33], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[34], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[35], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[36], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[37], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[38], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[39], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[40], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[41], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[42], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[43], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[44], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[45], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[46], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[47], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[48], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[49], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[50], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[51], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[52], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[53], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[54], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[55], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[56], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[57], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[58], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[59], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[60], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[61], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[62], Tdc, Tcdx, notif_a_d );
    $setuphold (posedge CLK &&& CHKEN4A, D[63], Tdc, Tcdx, notif_a_d );

    $setuphold (posedge CLK &&& CHKEN1A, RM[0], Trmc, Tcrmx, notif_a_rm );
    $setuphold (posedge CLK &&& CHKEN1A, RM[1], Trmc, Tcrmx, notif_a_rm );
    $setuphold (posedge CLK &&& CHKEN1A, RM[2], Trmc, Tcrmx, notif_a_rm );
    $setuphold (posedge CLK &&& CHKEN1A, RM[3], Trmc, Tcrmx, notif_a_rm );


endspecify

endmodule

`disable_portfaults 
`nosuppress_faults 
`endcelldefine

// The following is the sub-module used in definition of overall memory module
// ===========================================================================
module IntComp_U018SRA12X_256X64M4   ( Q_tmp, CLK_buf,  ADR_buf, D_buf, WEB_buf, OEB_buf, MEB_buf, RM_buf, notif_a_we, notif_a_adr, notif_a_d, notif_a_me,   notif_a_rm, notif_a_clk); 
parameter MES_ALL = "ON", A_bit=8, I_bit=64, I_word=256, rm_bit=4;

output  [I_bit -1:0]  Q_tmp;
reg  [I_bit -1:0]  Q_tmp;
reg  [I_bit -1:0]  Q_local;


wire   [I_bit -1:0]  Q_temp;
reg [I_bit-1:0] allX , allZ ;

input CLK_buf;
input  [A_bit -1:0]  ADR_buf;
input  [I_bit -1:0]  D_buf;
input WEB_buf;
input OEB_buf;
input MEB_buf;
input  [rm_bit -1:0]  RM_buf;

input notif_a_we, notif_a_adr, notif_a_d, notif_a_me, notif_a_clk;
input notif_a_rm;
integer i;
initial begin
        for ( i=0; i < I_bit ; i=i+1 ) begin
                allX[i] = 1'bx ; allZ[i] = 1'bz ;
        end
end 
reg [I_bit -1:0] mem_core_array [0:I_word -1];
reg [I_bit -1:0] X_a_corrupt;

reg
        flaga_read_ok,
        flaga_we_ok,
        flaga_d_ok,
        flaga_wem_ok,
        flaga_rm_ok,
        flaga_adr_ok,
        flaga_range_ok;
reg     flaga_adr_viol;
reg     flaga_clk_valid;
reg     flaga_adr_x;

event   ev_read_out_a_port;

reg[A_bit -1:0] ADRlatched;
reg[I_bit -1:0] Dlatched;
reg WEBlatched;
reg MEBlatched;
reg MEB_chk;
real CLKA_T;

function is_adr_Valid;  // 1-bit return value

 input [A_bit -1:0] addr_to_check;

 integer addr_bit_count;
 reg     ret_value;
 begin
  ret_value = `True;
  for (addr_bit_count = 0; addr_bit_count <= A_bit -1; addr_bit_count = addr_bit_count +1)
   if ((addr_to_check[addr_bit_count] !== 1'b0) && (addr_to_check[addr_bit_count] !== 1'b1)) begin
    flaga_adr_x = `True;
    ret_value = `False;
   end
   if((addr_to_check > I_word -1) || (addr_to_check < 0))
    begin
     ret_value = `False;
    end
   is_adr_Valid = ret_value;
 end

endfunction

function is_x;  // 1-bit return value

 input [I_bit -1:0] addr_to_check;

 integer addr_bit_count;
 reg     ret_value;
 begin
  ret_value = `False;
  for (addr_bit_count = 0; addr_bit_count <= I_bit -1; addr_bit_count = addr_bit_count +1)
   if ((addr_to_check[addr_bit_count] !== 1'b0) && (addr_to_check[addr_bit_count] !== 1'b1)) begin
    ret_value = `True;
   end
   is_x = ret_value;
 end

endfunction

// -------------------------------------------------------------------
// Common tasks
// -------------------------------------------------------------------


task corrupt_all_loc;

 input flag_range_ok;
 integer addr_index;
 begin
                                     // do not corrupt entire memory if write to
                                    // out of range address, for all other cases
                                    // flag_range_ok is true, therefore corruption
                                    // will occur as before
  if( flag_range_ok == `True)
   begin
    for( addr_index = 0; addr_index < I_word; addr_index = addr_index + 1) begin
     mem_core_array[ addr_index] = allX;
    end
   end
 end

endtask

task corrupt_cur_loc;

 input[A_bit -1:0] loc_to_corrupt;

 if (is_adr_Valid (loc_to_corrupt))
  mem_core_array[loc_to_corrupt] = allX;
 else
  corrupt_all_loc(`True);

endtask


function is_rm_Valid;

 input [rm_bit -1:0] rm_to_chk;

 integer rm_bit_cnt;
 reg ret_val;

 begin
  ret_val = `True;
  for (rm_bit_cnt = 0; rm_bit_cnt < rm_bit; rm_bit_cnt = rm_bit_cnt + 1)
   if ( (rm_to_chk[rm_bit_cnt] !== 1'b0) && (rm_to_chk[rm_bit_cnt] !== 1'b1) )
    ret_val = `False;
  is_rm_Valid = ret_val;
 end

endfunction



// Perform Sanity Check on Port A, Corrupt memory if required

task checkSanityOnAport;

 #0                // let CLOCK and NOTIFIER stuff execute first
 case ( {flaga_adr_ok, flaga_we_ok, flaga_d_ok} ) // only 1 and 0

  3'b111   : ;                                                // everything ok!!!

  3'b101,
  3'b100   : corrupt_cur_loc(ADRlatched);          // WE is unstable

  3'b110   : if (WEBlatched !== 1'b1)
              corrupt_cur_loc(ADRlatched);         // Data is unstable
  3'b000,
  3'b001   : corrupt_all_loc(flaga_range_ok);            // ADR and WE unstable
  3'b010,
  3'b011   : if (WEBlatched !== 1'b1)    // ADR unstable, WE stable
              corrupt_all_loc(flaga_range_ok);
 endcase
endtask

// PORT A FUNCTIONALITY (Handle violation behavior)

initial
 begin
  flaga_adr_ok  = `True;
  flaga_range_ok = `True;
  flaga_we_ok   = `True;
  flaga_d_ok    = `True;
  flaga_wem_ok   = `True;
  flaga_read_ok = `True;
  flaga_rm_ok = `True;
  flaga_adr_viol = `True;
  flaga_clk_valid = `True;
  flaga_adr_x = `False;
 end

always @(notif_a_we)            // PORT A WE violation
 begin
  if ( $realtime == CLKA_T )
   MEB_chk = MEB_buf;
  else
   MEB_chk = MEBlatched;
  if (MEB_chk !== 1'b1)
   begin
    flaga_we_ok = `False;
    checkSanityOnAport;
    disable OUTPUT_a;
    flaga_read_ok = `False;
    -> ev_read_out_a_port;
   end
 end

always @(notif_a_clk)            // PORT A CLK violation
 begin
  disable OUTPUT_a;
  flaga_adr_ok = `False;
  flaga_we_ok  = `False;
  checkSanityOnAport;
  flaga_read_ok = `False;     // irrespective of WE
  -> ev_read_out_a_port;
 end

always @(notif_a_me)            // PORT A ME violation
 begin
  flaga_adr_ok = `False;
  flaga_read_ok = `False;     // irrespective of WE
  disable OUTPUT_a;
  checkSanityOnAport;
  -> ev_read_out_a_port;
 end

always @(notif_a_adr)           // PORT A ADR violation
 begin
  if ( $realtime == CLKA_T )
   MEB_chk = MEB_buf;
  else
   MEB_chk = MEBlatched;
  if (MEB_chk !== 1'b1)
   begin
    if (WEBlatched === 1'b1)
      begin
    corrupt_all_loc(`True);
    flaga_read_ok = `False; // irrespective of WE
    disable OUTPUT_a;
    -> ev_read_out_a_port;
      end
     else  begin
    flaga_adr_ok = `False;
    flaga_read_ok = `False; // irrespective of WE
    disable OUTPUT_a;
    checkSanityOnAport;
    -> ev_read_out_a_port;
      end
   end
 end

always @(notif_a_d)             // PORT A D violation
 begin
  if ( $realtime == CLKA_T )
   MEB_chk = MEB_buf;
  else
   MEB_chk = MEBlatched;
  if (MEB_chk !== 1'b1)
   begin
    flaga_d_ok = `False;
    disable OUTPUT_a;
    checkSanityOnAport;
    flaga_read_ok = `False;
    -> ev_read_out_a_port;
   end
 end

always @(notif_a_rm)               //PORT A RM violation
 begin
  if ( $realtime == CLKA_T )
   MEB_chk = MEB_buf;
  else
   MEB_chk = MEBlatched;
  if (MEB_chk !== 1'b1)
   begin
    disable OUTPUT_a;
    flaga_read_ok = `False;
    -> ev_read_out_a_port;
    flaga_d_ok  = `False;
    checkSanityOnAport;
   end
 end




always @(negedge CLK_buf)          // reset for next cycle
 begin
  if ( CLK_buf !== 1'bx ) begin
   #0.001;
   flaga_range_ok  = `True;
   flaga_read_ok = `True;
   flaga_rm_ok = `True;
   flaga_adr_viol = `True;
   flaga_clk_valid = `True;
   flaga_adr_x = `False;
  end
  else
   begin
    if( MES_ALL=="ON" && $realtime != 0) begin
//     $display("<<CLK unknown>>");
//     $display("      time=%t; instance=%m (U018SRA12X_256X64M4_core)",$realtime);
    end
    flaga_clk_valid = `False;
    Q_local = allX;
    corrupt_all_loc(`True);
   end
 end

// PORT A FUNCTIONALITY (Handle normal read/write)

always @(posedge CLK_buf)
 begin
   flaga_adr_ok  = `True;
   flaga_we_ok   = `True;
   flaga_d_ok    = `True;
   flaga_wem_ok   = `True;
  if ( CLK_buf === 1'bx) begin
   if( MES_ALL=="ON" && $realtime != 0) begin
//    $display("<<CLK unknown>>");
//    $display("      time=%t; instance=%m (U018SRA12X_256X64M4_core)",$realtime);
   end
    flaga_clk_valid = `False;
    Q_local = allX;
    corrupt_all_loc(`True);
  end
  CLKA_T = $realtime;
  MEBlatched = MEB_buf;
  WEBlatched = WEB_buf;
  ADRlatched = ADR_buf;
  Dlatched = D_buf;
   if( MES_ALL=="ON" && $realtime != 0) begin
    if ( is_x(Dlatched) && WEBlatched == 1'b0 && MEBlatched == 1'b0) begin
//      $display("<<D unknown>>");
//      $display("      time=%t; instance=%m (U018SRA12X_256X64M4_core)",$realtime);
    end  
   end 
  if (!flaga_clk_valid)
   Q_local = allX;
  else if ( CLK_buf === 1'bx )
   begin
    if( MES_ALL=="ON" && $realtime != 0) begin
//     $display("<<CLK unknown>>");
//     $display("      time=%t; instance=%m (U018SRA12X_256X64M4_core)",$realtime);
    end
    Q_local = allX;
    corrupt_all_loc(flaga_range_ok);
   end
  else
   begin
    if (MEB_buf !== 1'b1)
     begin
      if (WEB_buf === 1'bx) begin
        if( MES_ALL=="ON" && $realtime != 0) begin
//          $display("<<WEB unknown>>");
//          $display("      time=%t; instance=%m (U018SRA12X_256X64M4_core)",$realtime);
        end
       flaga_we_ok = `False;
      end
      if (MEB_buf !== 1'b0)
       flaga_we_ok = `False;       // don't know if cycle is On or Off
      if (is_adr_Valid(ADRlatched) != `True ) begin
        if( MES_ALL=="ON" && $realtime != 0) begin
         if ( flaga_adr_x ) begin
//           $display("<<ADR unknown>>");
//           $display("      time=%t; instance=%m (U018SRA12X_256X64M4_core)",$realtime);
         end
        end
       flaga_adr_ok = `False;
      if(((ADRlatched > I_word -1)|| ( ADRlatched < 0)) && (MEBlatched != 1'b1))
       begin
        $display("\n%m WARNING:address is out of range\n RANGE:0 to 255\n");
        flaga_range_ok = `False;
       end
      else begin
        if (WEBlatched === 1'b1 && MEB_buf === 1'b0 )
         corrupt_all_loc(`True);
      end
      end
      if ( MEB_buf === 1'bx ) begin
        if( MES_ALL=="ON" && $realtime != 0) begin
//          $display("<<MEB unknown>>");
//          $display("      time=%t; instance=%m (U018SRA12X_256X64M4_core)",$realtime);
        end
       Q_local = allX;
      if (WEB_buf !== 1'b1)
       corrupt_all_loc(flaga_range_ok);
      end
      else
       begin
        if (flaga_we_ok && flaga_adr_ok && flaga_d_ok && (WEB_buf == 1'b0))
         mem_core_array[ADRlatched] = Dlatched;  // write data in
        else
         begin
          checkSanityOnAport;
         end
        -> ev_read_out_a_port;
       end
     end
   end
 end


// PORT A READ-OUT

always @(ev_read_out_a_port)
 begin
  #0                // let CLOCK and NOTIFIER module execute first
  if (is_rm_Valid(RM_buf) != `True)
   flaga_rm_ok = `False;
  if (flaga_adr_ok && (ADRlatched <= I_word -1) && flaga_read_ok && flaga_rm_ok) begin : OUTPUT_a
   if (WEBlatched == 1'b0) begin
    Q_local = allX;
    #0.001
    Q_local = Dlatched;
   end
   else
    begin
     Q_local = allX;
     #0.001
      Q_local = mem_core_array[ADRlatched];
    end
  end
  else
   begin
        if (is_adr_Valid(ADRlatched) != `True && WEBlatched == 1'b0) begin
    Q_local = allX;
    #0.001
    Q_local = Dlatched;
   end
   else
    Q_local = allX;
    flaga_read_ok = `True;
   end
 end
assign Q_temp = Q_local;

always @( Q_temp or OEB_buf )
 begin
  if ( OEB_buf == 1'b0 )
   Q_tmp = Q_temp;
  else if ( OEB_buf == 1'b1 )
   begin
    Q_tmp = allX;
    #0.001;
    if ( OEB_buf == 1'b0 )
      Q_tmp = Q_temp;
    else if ( OEB_buf == 1'b1 )
      Q_tmp = allZ;
    else begin
      Q_tmp = allX;
    end
   end
  else begin
   Q_tmp = allX;
  end
 end
always @( OEB_buf )
 begin
  if ( OEB_buf ===  1'bx )
  begin
      if( MES_ALL=="ON" && $realtime != 0) begin
//        $display("<<OEB unknown>>");
//        $display("      time=%t; instance=%m (U018SRA12X_256X64M4_core)",$realtime);
      end
  end
 end


endmodule
