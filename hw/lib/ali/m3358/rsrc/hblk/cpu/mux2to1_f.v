module mux2to1_f(dout,din0,din1,sel0);
//synopsys template
parameter WIDTH=2;
	output [WIDTH-1:0]  dout;
    input  [WIDTH-1:0]  din0;
    input  [WIDTH-1:0]  din1;
	input  [WIDTH-1:0]  sel0;

	reg  [WIDTH-1:0]  dout;
    reg  tmp_in0,tmp_in1,tmp_sel0,tmp_dout;
    integer i;

    always @(din0 or din1 or sel0) begin  
		for (i=0;i<WIDTH;i=i+1) begin
            tmp_in0 = din0[i];
            tmp_in1 = din1[i];
            tmp_sel0 = sel0[i];
			case (tmp_sel0) // synopsys infer_mux
				1'b0 : tmp_dout = tmp_in0;
				1'b1 : tmp_dout = tmp_in1;
			endcase
            dout[i] = tmp_dout;
		end
	end
/* synopsys dc_script_begin
 compile_create_mux_op_hierarchy = false
*/
endmodule
