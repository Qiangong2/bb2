//------------------------------------------------------------------------------
//
//        This confidential and proprietary software may be used only
//     as authorized by a licensing agreement from Synopsys Inc.
//     In the event of publication, the following notice is applicable:
//
//                    (C) COPYRIGHT 1998 - 1999   SYNOPSYS INC.
//                          ALL RIGHTS RESERVED
//
//        The entire notice above must be reproduced on all authorized
//       copies.
//
// AUTHOR:    Rick Kelly        11/3/98
//
// VERSION:   Simulation Architecture
// 
//-------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
//
// ABSTRACT:  Multiplier, parital products
//
//    **** >>>>  NOTE:	This model is architecturally different
//			from the 'wall' implementation of DW02_multp
//			but will generate exactly the same result
//			once the two partial product outputs are
//			added together
//
// MODIFIED:
//
//------------------------------------------------------------------------------
//
module DW02_multp( a, b, tc, out0, out1 );

parameter a_width = 8;
parameter b_width = 8;
parameter out_width = 18;

`define npp ((a_width/2) + 2)
`define xdim (a_width+b_width+1)

input [a_width-1 : 0]	a;
input [b_width-1 : 0]	b;
input			tc;
output [out_width-1:0]	out0, out1;

reg [`xdim-1 : 0] pp_array [0 : `npp-1];


reg   [`xdim-1 : 0]	tmp_OUT0, tmp_OUT1;
wire  [a_width+2 : 0]	a_padded;
wire  [b_width+1 : 0]	b_padded;
wire  			a_sign, b_sign, out_sign;

// synopsys synthesis_off

    initial
	begin : param_check

	if (out_width < (a_width+b_width+2))
	    begin
	    $write( "\nERROR: DW02_multp: Invalid parameter value: out_width MUST BE >= (a_width+b_width+2)\n\n" );
	    $finish;
	    end
	
	end


    assign a_sign = tc & a[a_width-1];
    assign b_sign = tc & b[b_width-1];

    assign a_padded = {a_sign, a_sign, a, 1'b0};
    assign b_padded = {b_sign, b_sign, b};

    always @ (a_padded or b_padded)
	begin : mk_pp_array
	    reg [`xdim-1 : 0] temp_pp_array [0 : `npp-1];
	    reg [`xdim-1 : 0] next_pp_array [0 : `npp-1];
	    reg [`xdim+3 : 0] temp_pp;
	    reg [`xdim-1 : 0] new_pp;
	    reg [`xdim-1 : 0] tmp_pp_carry;
	    reg [a_width+2 : 0] temp_a_padded;
	    reg [2 : 0] temp_bitgroup;
	    integer bit_pair, pp_count, i;

	temp_pp_array[0] = {`xdim{1'b0}};

	for (bit_pair=0 ; bit_pair < `npp-1 ; bit_pair = bit_pair+1)
	    begin

	    temp_a_padded = (a_padded >> (bit_pair*2));
	    temp_bitgroup = temp_a_padded[2 : 0];

	    if ( b_padded[b_width] === 1'b0)
		begin
		case (temp_bitgroup)

		    3'b000, 3'b111 :
			    temp_pp = {`xdim+3{1'b0}};

		    3'b001, 3'b010 :
			    temp_pp = {{a_width+2{1'b0}}, b_padded, 1'b0};

		    3'b011 :
			    temp_pp = {{a_width+1{1'b0}}, b_padded, 2'b00};

		    3'b100 :
			    temp_pp = {{a_width+1{1'b1}}, ~b_padded, 2'b10};

		    3'b101, 3'b110 :
			    temp_pp = {{a_width+2{1'b1}}, ~b_padded, 1'b0};

		    default : temp_pp = {`xdim+3{1'bx}};
		endcase
		end

	    else
		begin
		case (temp_bitgroup)

		    3'b000, 3'b111 :
			    temp_pp = {`xdim+3{1'b0}};

		    3'b001, 3'b010 :
			    temp_pp = {{a_width+2{1'b1}}, b_padded, 1'b0};

		    3'b011 :
			    temp_pp = {{a_width+1{1'b1}}, b_padded, 2'b00};

		    3'b100 :
			    temp_pp = {{a_width+1{1'b0}}, ~b_padded, 2'b10};

		    3'b101, 3'b110 :
			    temp_pp = {{a_width+2{1'b0}}, ~b_padded, 1'b0};

		    default : temp_pp = {`xdim+3{1'bx}};
		endcase
		end

	    temp_pp = temp_pp << (2 * bit_pair);
	    new_pp = temp_pp[`xdim : 1];
	    temp_pp_array[bit_pair+1] = new_pp;

	    if ((a_padded[bit_pair*2+2] & ~(a_padded[bit_pair*2+1] & a_padded[bit_pair*2])) === 1'b1)
		temp_pp_array[0] = temp_pp_array[0] | ({{`xdim-1{1'b0}}, 1'b1} << (bit_pair*2));
	    end

	pp_count = `npp;

	while (pp_count > 2)
	    begin

	    for (i=0 ; i < (pp_count/3) ; i = i+1)
		begin
		next_pp_array[i*2] = temp_pp_array[i*3] ^
					temp_pp_array[i*3+1] ^
					temp_pp_array[i*3+2];

		tmp_pp_carry = (temp_pp_array[i*3] & temp_pp_array[i*3+1]) |
					(temp_pp_array[i*3+1] & temp_pp_array[i*3+2]) |
					(temp_pp_array[i*3] & temp_pp_array[i*3+2]);

		next_pp_array[i*2+1] = tmp_pp_carry << 1;
		end

	    if ((pp_count % 3) > 0)
		begin
		for (i=0 ; i < (pp_count % 3) ; i = i + 1)
		    next_pp_array[2 * (pp_count/3) + i] = temp_pp_array[3 * (pp_count/3) + i];
		end

	    for (i=0 ; i < `npp ; i = i + 1)
		temp_pp_array[i] = next_pp_array[i];

	    pp_count = pp_count - (pp_count/3);
	    end

	    tmp_OUT0 <= temp_pp_array[0];

	    if (pp_count > 1)
		tmp_OUT1 <= temp_pp_array[1];

	    else
		tmp_OUT1 <= {`xdim{1'b0}};

	end // mk_pp_array


    assign out_sign = tmp_OUT0[`xdim-1] | tmp_OUT1[`xdim-1];
    assign out0 = {{out_width-`xdim{1'b0}}, tmp_OUT0};
    assign out1 = {{out_width-`xdim{out_sign}}, tmp_OUT1};

// synopsys synthesis_on

endmodule
