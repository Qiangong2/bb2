module ram16x32d1 (
	DIN,
	DOUT,
	WE,
	CLK,
	RADDR,
	WADDR
		);

parameter	rd_dly = 1;
parameter	wr_dly = 1;

input	[31:0]	DIN;
output	[31:0]	DOUT;
input		WE;
input		CLK;
input	[3:0]	RADDR, WADDR;

reg	[31:0]	mem_0, mem_1, mem_2, mem_3, mem_4, mem_5, mem_6, mem_7;
reg	[31:0]	mem_8, mem_9, mem_10, mem_11, mem_12, mem_13, mem_14, mem_15;

assign	#rd_dly	DOUT	= {32{RADDR == 4'b0000}} & mem_0 |
			  {32{RADDR == 4'b0001}} & mem_1 |
			  {32{RADDR == 4'b0010}} & mem_2 |
			  {32{RADDR == 4'b0011}} & mem_3 |
			  {32{RADDR == 4'b0100}} & mem_4 |
			  {32{RADDR == 4'b0101}} & mem_5 |
			  {32{RADDR == 4'b0110}} & mem_6 |
			  {32{RADDR == 4'b0111}} & mem_7 |
			  {32{RADDR == 4'b1000}} & mem_8 |
			  {32{RADDR == 4'b1001}} & mem_9 |
			  {32{RADDR == 4'b1010}} & mem_10 |
			  {32{RADDR == 4'b1011}} & mem_11 |
			  {32{RADDR == 4'b1100}} & mem_12 |
			  {32{RADDR == 4'b1101}} & mem_13 |
			  {32{RADDR == 4'b1110}} & mem_14 |
			  {32{RADDR == 4'b1111}} & mem_15 ;
//0~~7
always @(posedge CLK)
	if (WE == 1'b1 & WADDR == 4'b0000)
		mem_0	<= #wr_dly DIN;

always @(posedge CLK)
	if (WE == 1'b1 & WADDR == 4'b0001)
		mem_1	<= #wr_dly DIN;

always @(posedge CLK)
	if (WE == 1'b1 & WADDR == 4'b0010)
		mem_2	<= #wr_dly DIN;

always @(posedge CLK)
	if (WE == 1'b1 & WADDR == 4'b0011)
		mem_3	<= #wr_dly DIN;

always @(posedge CLK)
	if (WE == 1'b1 & WADDR == 4'b0100)
		mem_4	<= #wr_dly DIN;

always @(posedge CLK)
	if (WE == 1'b1 & WADDR == 4'b0101)
		mem_5	<= #wr_dly DIN;

always @(posedge CLK)
	if (WE == 1'b1 & WADDR == 4'b0110)
		mem_6	<= #wr_dly DIN;

always @(posedge CLK)
	if (WE == 1'b1 & WADDR == 4'b0111)
		mem_7	<= #wr_dly DIN;
//8~~~15
always @(posedge CLK)
	if (WE == 1'b1 & WADDR == 4'b1000)
		mem_8	<= #wr_dly DIN;

always @(posedge CLK)
	if (WE == 1'b1 & WADDR == 4'b1001)
		mem_9	<= #wr_dly DIN;

always @(posedge CLK)
	if (WE == 1'b1 & WADDR == 4'b1010)
		mem_10	<= #wr_dly DIN;

always @(posedge CLK)
	if (WE == 1'b1 & WADDR == 4'b1011)
		mem_11	<= #wr_dly DIN;

always @(posedge CLK)
	if (WE == 1'b1 & WADDR == 4'b1100)
		mem_12	<= #wr_dly DIN;

always @(posedge CLK)
	if (WE == 1'b1 & WADDR == 4'b1101)
		mem_13	<= #wr_dly DIN;

always @(posedge CLK)
	if (WE == 1'b1 & WADDR == 4'b1110)
		mem_14	<= #wr_dly DIN;

always @(posedge CLK)
	if (WE == 1'b1 & WADDR == 4'b1111)
		mem_15	<= #wr_dly DIN;
endmodule
