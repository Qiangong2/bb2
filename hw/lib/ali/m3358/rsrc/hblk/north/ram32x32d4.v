module ram32x32d4 (
	DIN,
	DOUT,
	WE,
	CLK,
	WADDR,
	RADDR
		);

parameter	rd_dly = 1;
parameter	wr_dly = 1;

input	[31:0]	DIN;
output	[31:0]	DOUT;
input	[3:0]	WE;
input		CLK;
input	[4:0]	WADDR, RADDR;

reg	[7:0]	mem_00, mem_01, mem_02, mem_03, mem_04, mem_05, mem_06, mem_07;
reg	[7:0]	mem_10, mem_11, mem_12, mem_13, mem_14, mem_15, mem_16, mem_17;
reg	[7:0]	mem_20, mem_21, mem_22, mem_23, mem_24, mem_25, mem_26, mem_27;
reg	[7:0]	mem_30, mem_31, mem_32, mem_33, mem_34, mem_35, mem_36, mem_37;

reg	[7:0]	mem_08, mem_09, mem_010, mem_011, mem_012, mem_013, mem_014, mem_015;
reg	[7:0]	mem_18, mem_19, mem_110, mem_111, mem_112, mem_113, mem_114, mem_115;
reg	[7:0]	mem_28, mem_29, mem_210, mem_211, mem_212, mem_213, mem_214, mem_215;
reg	[7:0]	mem_38, mem_39, mem_310, mem_311, mem_312, mem_313, mem_314, mem_315;

//last 16 data
reg	[7:0]	mem_016, mem_017, mem_018, mem_019, mem_020, mem_021, mem_022, mem_023;
reg	[7:0]	mem_116, mem_117, mem_118, mem_119, mem_120, mem_121, mem_122, mem_123;
reg	[7:0]	mem_216, mem_217, mem_218, mem_219, mem_220, mem_221, mem_222, mem_223;
reg	[7:0]	mem_316, mem_317, mem_318, mem_319, mem_320, mem_321, mem_322, mem_323;

reg	[7:0]	mem_024, mem_025, mem_026, mem_027, mem_028, mem_029, mem_030, mem_031;
reg	[7:0]	mem_124, mem_125, mem_126, mem_127, mem_128, mem_129, mem_130, mem_131;
reg	[7:0]	mem_224, mem_225, mem_226, mem_227, mem_228, mem_229, mem_230, mem_231;
reg	[7:0]	mem_324, mem_325, mem_326, mem_327, mem_328, mem_329, mem_330, mem_331;

wire	[31:0]	dout_0	= {mem_30, mem_20, mem_10, mem_00};
wire	[31:0]	dout_1	= {mem_31, mem_21, mem_11, mem_01};
wire	[31:0]	dout_2	= {mem_32, mem_22, mem_12, mem_02};
wire	[31:0]	dout_3	= {mem_33, mem_23, mem_13, mem_03};
wire	[31:0]	dout_4	= {mem_34, mem_24, mem_14, mem_04};
wire	[31:0]	dout_5	= {mem_35, mem_25, mem_15, mem_05};
wire	[31:0]	dout_6	= {mem_36, mem_26, mem_16, mem_06};
wire	[31:0]	dout_7	= {mem_37, mem_27, mem_17, mem_07};
wire	[31:0]	dout_8	= {mem_38, mem_28, mem_18, mem_08};
wire	[31:0]	dout_9	= {mem_39, mem_29, mem_19, mem_09};
wire	[31:0]	dout_10	= {mem_310, mem_210, mem_110, mem_010};
wire	[31:0]	dout_11	= {mem_311, mem_211, mem_111, mem_011};
wire	[31:0]	dout_12	= {mem_312, mem_212, mem_112, mem_012};
wire	[31:0]	dout_13	= {mem_313, mem_213, mem_113, mem_013};
wire	[31:0]	dout_14	= {mem_314, mem_214, mem_114, mem_014};
wire	[31:0]	dout_15	= {mem_315, mem_215, mem_115, mem_015};

wire	[31:0]	dout_16	= {mem_316, mem_216, mem_116, mem_016};
wire	[31:0]	dout_17	= {mem_317, mem_217, mem_117, mem_017};
wire	[31:0]	dout_18	= {mem_318, mem_218, mem_118, mem_018};
wire	[31:0]	dout_19	= {mem_319, mem_219, mem_119, mem_019};
wire	[31:0]	dout_20	= {mem_320, mem_220, mem_120, mem_020};
wire	[31:0]	dout_21	= {mem_321, mem_221, mem_121, mem_021};
wire	[31:0]	dout_22	= {mem_322, mem_222, mem_122, mem_022};
wire	[31:0]	dout_23	= {mem_323, mem_223, mem_123, mem_023};
wire	[31:0]	dout_24	= {mem_324, mem_224, mem_124, mem_024};
wire	[31:0]	dout_25	= {mem_325, mem_225, mem_125, mem_025};
wire	[31:0]	dout_26	= {mem_326, mem_226, mem_126, mem_026};
wire	[31:0]	dout_27	= {mem_327, mem_227, mem_127, mem_027};
wire	[31:0]	dout_28	= {mem_328, mem_228, mem_128, mem_028};
wire	[31:0]	dout_29	= {mem_329, mem_229, mem_129, mem_029};
wire	[31:0]	dout_30	= {mem_330, mem_230, mem_130, mem_030};
wire	[31:0]	dout_31	= {mem_331, mem_231, mem_131, mem_031};


assign	#rd_dly	DOUT	= {32{RADDR == 5'b00000}} & dout_0 |
			  {32{RADDR == 5'b00001}} & dout_1 |
			  {32{RADDR == 5'b00010}} & dout_2 |
			  {32{RADDR == 5'b00011}} & dout_3 |
			  {32{RADDR == 5'b00100}} & dout_4 |
			  {32{RADDR == 5'b00101}} & dout_5 |
			  {32{RADDR == 5'b00110}} & dout_6 |
			  {32{RADDR == 5'b00111}} & dout_7 |
			  {32{RADDR == 5'b01000}} & dout_8 |
			  {32{RADDR == 5'b01001}} & dout_9 |
			  {32{RADDR == 5'b01010}} & dout_10 |
			  {32{RADDR == 5'b01011}} & dout_11 |
			  {32{RADDR == 5'b01100}} & dout_12 |
			  {32{RADDR == 5'b01101}} & dout_13 |
			  {32{RADDR == 5'b01110}} & dout_14 |
			  {32{RADDR == 5'b01111}} & dout_15|
			  {32{RADDR == 5'b10000}} & dout_16  |
			  {32{RADDR == 5'b10001}} & dout_17 |
			  {32{RADDR == 5'b10010}} & dout_18 |
			  {32{RADDR == 5'b10011}} & dout_19 |
			  {32{RADDR == 5'b10100}} & dout_20 |
			  {32{RADDR == 5'b10101}} & dout_21 |
			  {32{RADDR == 5'b10110}} & dout_22 |
			  {32{RADDR == 5'b10111}} & dout_23 |
			  {32{RADDR == 5'b11000}} & dout_24 |
			  {32{RADDR == 5'b11001}} & dout_25 |
			  {32{RADDR == 5'b11010}} & dout_26 |
			  {32{RADDR == 5'b11011}} & dout_27 |
			  {32{RADDR == 5'b11100}} & dout_28 |
			  {32{RADDR == 5'b11101}} & dout_29 |
			  {32{RADDR == 5'b11110}} & dout_30 |
			  {32{RADDR == 5'b11111}} & dout_31;

always @(posedge CLK)
	if (WADDR == 5'b00000) begin
		if (WE[0] == 1'b1)
			mem_00 <= #wr_dly DIN[7:0];
		if (WE[1] == 1'b1)
			mem_10 <= #wr_dly DIN[15:8];
		if (WE[2] == 1'b1)
			mem_20 <= #wr_dly DIN[23:16];
		if (WE[3] == 1'b1)
			mem_30 <= #wr_dly DIN[31:24];
	end

always @(posedge CLK)
	if (WADDR == 5'b00001) begin
		if (WE[0] == 1'b1)
			mem_01 <= #wr_dly DIN[7:0];
		if (WE[1] == 1'b1)
			mem_11 <= #wr_dly DIN[15:8];
		if (WE[2] == 1'b1)
			mem_21 <= #wr_dly DIN[23:16];
		if (WE[3] == 1'b1)
			mem_31 <= #wr_dly DIN[31:24];
	end

always @(posedge CLK)
	if (WADDR == 5'b00010) begin
		if (WE[0] == 1'b1)
			mem_02 <= #wr_dly DIN[7:0];
		if (WE[1] == 1'b1)
			mem_12 <= #wr_dly DIN[15:8];
		if (WE[2] == 1'b1)
			mem_22 <= #wr_dly DIN[23:16];
		if (WE[3] == 1'b1)
			mem_32 <= #wr_dly DIN[31:24];
	end

always @(posedge CLK)
	if (WADDR == 5'b00011) begin
		if (WE[0] == 1'b1)
			mem_03 <= #wr_dly DIN[7:0];
		if (WE[1] == 1'b1)
			mem_13 <= #wr_dly DIN[15:8];
		if (WE[2] == 1'b1)
			mem_23 <= #wr_dly DIN[23:16];
		if (WE[3] == 1'b1)
			mem_33 <= #wr_dly DIN[31:24];
	end

always @(posedge CLK)
	if (WADDR == 5'b00100) begin
		if (WE[0] == 1'b1)
			mem_04 <= #wr_dly DIN[7:0];
		if (WE[1] == 1'b1)
			mem_14 <= #wr_dly DIN[15:8];
		if (WE[2] == 1'b1)
			mem_24 <= #wr_dly DIN[23:16];
		if (WE[3] == 1'b1)
			mem_34 <= #wr_dly DIN[31:24];
	end

always @(posedge CLK)
	if (WADDR == 5'b00101) begin
		if (WE[0] == 1'b1)
			mem_05 <= #wr_dly DIN[7:0];
		if (WE[1] == 1'b1)
			mem_15 <= #wr_dly DIN[15:8];
		if (WE[2] == 1'b1)
			mem_25 <= #wr_dly DIN[23:16];
		if (WE[3] == 1'b1)
			mem_35 <= #wr_dly DIN[31:24];
	end

always @(posedge CLK)
	if (WADDR == 5'b00110) begin
		if (WE[0] == 1'b1)
			mem_06 <= #wr_dly DIN[7:0];
		if (WE[1] == 1'b1)
			mem_16 <= #wr_dly DIN[15:8];
		if (WE[2] == 1'b1)
			mem_26 <= #wr_dly DIN[23:16];
		if (WE[3] == 1'b1)
			mem_36 <= #wr_dly DIN[31:24];
	end

always @(posedge CLK)
	if (WADDR == 5'b00111) begin
		if (WE[0] == 1'b1)
			mem_07 <= #wr_dly DIN[7:0];
		if (WE[1] == 1'b1)
			mem_17 <= #wr_dly DIN[15:8];
		if (WE[2] == 1'b1)
			mem_27 <= #wr_dly DIN[23:16];
		if (WE[3] == 1'b1)
			mem_37 <= #wr_dly DIN[31:24];
	end

	//8~15
always @(posedge CLK)
	if (WADDR == 5'b01000) begin
		if (WE[0] == 1'b1)
			mem_08 <= #wr_dly DIN[7:0];
		if (WE[1] == 1'b1)
			mem_18 <= #wr_dly DIN[15:8];
		if (WE[2] == 1'b1)
			mem_28 <= #wr_dly DIN[23:16];
		if (WE[3] == 1'b1)
			mem_38 <= #wr_dly DIN[31:24];
	end

always @(posedge CLK)
	if (WADDR == 5'b01001) begin
		if (WE[0] == 1'b1)
			mem_09 <= #wr_dly DIN[7:0];
		if (WE[1] == 1'b1)
			mem_19 <= #wr_dly DIN[15:8];
		if (WE[2] == 1'b1)
			mem_29 <= #wr_dly DIN[23:16];
		if (WE[3] == 1'b1)
			mem_39 <= #wr_dly DIN[31:24];
	end

always @(posedge CLK)
	if (WADDR == 5'b01010) begin
		if (WE[0] == 1'b1)
			mem_010 <= #wr_dly DIN[7:0];
		if (WE[1] == 1'b1)
			mem_110 <= #wr_dly DIN[15:8];
		if (WE[2] == 1'b1)
			mem_210 <= #wr_dly DIN[23:16];
		if (WE[3] == 1'b1)
			mem_310 <= #wr_dly DIN[31:24];
	end

always @(posedge CLK)
	if (WADDR == 5'b01011) begin
		if (WE[0] == 1'b1)
			mem_011 <= #wr_dly DIN[7:0];
		if (WE[1] == 1'b1)
			mem_111 <= #wr_dly DIN[15:8];
		if (WE[2] == 1'b1)
			mem_211 <= #wr_dly DIN[23:16];
		if (WE[3] == 1'b1)
			mem_311 <= #wr_dly DIN[31:24];
	end

always @(posedge CLK)
	if (WADDR == 5'b01100) begin
		if (WE[0] == 1'b1)
			mem_012 <= #wr_dly DIN[7:0];
		if (WE[1] == 1'b1)
			mem_112 <= #wr_dly DIN[15:8];
		if (WE[2] == 1'b1)
			mem_212 <= #wr_dly DIN[23:16];
		if (WE[3] == 1'b1)
			mem_312 <= #wr_dly DIN[31:24];
	end

always @(posedge CLK)
	if (WADDR == 5'b01101) begin
		if (WE[0] == 1'b1)
			mem_013 <= #wr_dly DIN[7:0];
		if (WE[1] == 1'b1)
			mem_113 <= #wr_dly DIN[15:8];
		if (WE[2] == 1'b1)
			mem_213 <= #wr_dly DIN[23:16];
		if (WE[3] == 1'b1)
			mem_313 <= #wr_dly DIN[31:24];
	end

always @(posedge CLK)
	if (WADDR == 5'b01110) begin
		if (WE[0] == 1'b1)
			mem_014 <= #wr_dly DIN[7:0];
		if (WE[1] == 1'b1)
			mem_114 <= #wr_dly DIN[15:8];
		if (WE[2] == 1'b1)
			mem_214 <= #wr_dly DIN[23:16];
		if (WE[3] == 1'b1)
			mem_314 <= #wr_dly DIN[31:24];
	end

always @(posedge CLK)
	if (WADDR == 5'b01111) begin
		if (WE[0] == 1'b1)
			mem_015 <= #wr_dly DIN[7:0];
		if (WE[1] == 1'b1)
			mem_115 <= #wr_dly DIN[15:8];
		if (WE[2] == 1'b1)
			mem_215 <= #wr_dly DIN[23:16];
		if (WE[3] == 1'b1)
			mem_315 <= #wr_dly DIN[31:24];
	end
//16~31
always @(posedge CLK)
	if (WADDR == 5'b10000) begin
		if (WE[0] == 1'b1)
			mem_016<= #wr_dly DIN[7:0];
		if (WE[1] == 1'b1)
			mem_116 <= #wr_dly DIN[15:8];
		if (WE[2] == 1'b1)
			mem_216 <= #wr_dly DIN[23:16];
		if (WE[3] == 1'b1)
			mem_316 <= #wr_dly DIN[31:24];
	end

always @(posedge CLK)
	if (WADDR == 5'b10001) begin
		if (WE[0] == 1'b1)
			mem_017 <= #wr_dly DIN[7:0];
		if (WE[1] == 1'b1)
			mem_117 <= #wr_dly DIN[15:8];
		if (WE[2] == 1'b1)
			mem_217 <= #wr_dly DIN[23:16];
		if (WE[3] == 1'b1)
			mem_317 <= #wr_dly DIN[31:24];
	end

always @(posedge CLK)
	if (WADDR == 5'b10010) begin
		if (WE[0] == 1'b1)
			mem_018 <= #wr_dly DIN[7:0];
		if (WE[1] == 1'b1)
			mem_118 <= #wr_dly DIN[15:8];
		if (WE[2] == 1'b1)
			mem_218 <= #wr_dly DIN[23:16];
		if (WE[3] == 1'b1)
			mem_318 <= #wr_dly DIN[31:24];
	end

always @(posedge CLK)
	if (WADDR == 5'b10011) begin
		if (WE[0] == 1'b1)
			mem_019 <= #wr_dly DIN[7:0];
		if (WE[1] == 1'b1)
			mem_119 <= #wr_dly DIN[15:8];
		if (WE[2] == 1'b1)
			mem_219 <= #wr_dly DIN[23:16];
		if (WE[3] == 1'b1)
			mem_319 <= #wr_dly DIN[31:24];
	end

always @(posedge CLK)
	if (WADDR == 5'b10100) begin
		if (WE[0] == 1'b1)
			mem_020 <= #wr_dly DIN[7:0];
		if (WE[1] == 1'b1)
			mem_120 <= #wr_dly DIN[15:8];
		if (WE[2] == 1'b1)
			mem_220 <= #wr_dly DIN[23:16];
		if (WE[3] == 1'b1)
			mem_320 <= #wr_dly DIN[31:24];
	end

always @(posedge CLK)
	if (WADDR == 5'b10101) begin
		if (WE[0] == 1'b1)
			mem_021 <= #wr_dly DIN[7:0];
		if (WE[1] == 1'b1)
			mem_121 <= #wr_dly DIN[15:8];
		if (WE[2] == 1'b1)
			mem_221 <= #wr_dly DIN[23:16];
		if (WE[3] == 1'b1)
			mem_321 <= #wr_dly DIN[31:24];
	end

always @(posedge CLK)
	if (WADDR == 5'b10110) begin
		if (WE[0] == 1'b1)
			mem_022 <= #wr_dly DIN[7:0];
		if (WE[1] == 1'b1)
			mem_122 <= #wr_dly DIN[15:8];
		if (WE[2] == 1'b1)
			mem_222 <= #wr_dly DIN[23:16];
		if (WE[3] == 1'b1)
			mem_322 <= #wr_dly DIN[31:24];
	end

always @(posedge CLK)
	if (WADDR == 5'b10111) begin
		if (WE[0] == 1'b1)
			mem_023 <= #wr_dly DIN[7:0];
		if (WE[1] == 1'b1)
			mem_123 <= #wr_dly DIN[15:8];
		if (WE[2] == 1'b1)
			mem_223 <= #wr_dly DIN[23:16];
		if (WE[3] == 1'b1)
			mem_323 <= #wr_dly DIN[31:24];
	end

	//8~15
always @(posedge CLK)
	if (WADDR == 5'b11000) begin
		if (WE[0] == 1'b1)
			mem_024 <= #wr_dly DIN[7:0];
		if (WE[1] == 1'b1)
			mem_124 <= #wr_dly DIN[15:8];
		if (WE[2] == 1'b1)
			mem_224 <= #wr_dly DIN[23:16];
		if (WE[3] == 1'b1)
			mem_324 <= #wr_dly DIN[31:24];
	end

always @(posedge CLK)
	if (WADDR == 5'b11001) begin
		if (WE[0] == 1'b1)
			mem_025 <= #wr_dly DIN[7:0];
		if (WE[1] == 1'b1)
			mem_125 <= #wr_dly DIN[15:8];
		if (WE[2] == 1'b1)
			mem_225 <= #wr_dly DIN[23:16];
		if (WE[3] == 1'b1)
			mem_325 <= #wr_dly DIN[31:24];
	end

always @(posedge CLK)
	if (WADDR == 5'b11010) begin
		if (WE[0] == 1'b1)
			mem_026 <= #wr_dly DIN[7:0];
		if (WE[1] == 1'b1)
			mem_126 <= #wr_dly DIN[15:8];
		if (WE[2] == 1'b1)
			mem_226 <= #wr_dly DIN[23:16];
		if (WE[3] == 1'b1)
			mem_326 <= #wr_dly DIN[31:24];
	end

always @(posedge CLK)
	if (WADDR == 5'b11011) begin
		if (WE[0] == 1'b1)
			mem_027 <= #wr_dly DIN[7:0];
		if (WE[1] == 1'b1)
			mem_127 <= #wr_dly DIN[15:8];
		if (WE[2] == 1'b1)
			mem_227 <= #wr_dly DIN[23:16];
		if (WE[3] == 1'b1)
			mem_327 <= #wr_dly DIN[31:24];
	end

always @(posedge CLK)
	if (WADDR == 5'b11100) begin
		if (WE[0] == 1'b1)
			mem_028 <= #wr_dly DIN[7:0];
		if (WE[1] == 1'b1)
			mem_128 <= #wr_dly DIN[15:8];
		if (WE[2] == 1'b1)
			mem_228 <= #wr_dly DIN[23:16];
		if (WE[3] == 1'b1)
			mem_328 <= #wr_dly DIN[31:24];
	end

always @(posedge CLK)
	if (WADDR == 5'b11101) begin
		if (WE[0] == 1'b1)
			mem_029 <= #wr_dly DIN[7:0];
		if (WE[1] == 1'b1)
			mem_129 <= #wr_dly DIN[15:8];
		if (WE[2] == 1'b1)
			mem_229 <= #wr_dly DIN[23:16];
		if (WE[3] == 1'b1)
			mem_329 <= #wr_dly DIN[31:24];
	end

always @(posedge CLK)
	if (WADDR == 5'b11110) begin
		if (WE[0] == 1'b1)
			mem_030 <= #wr_dly DIN[7:0];
		if (WE[1] == 1'b1)
			mem_130 <= #wr_dly DIN[15:8];
		if (WE[2] == 1'b1)
			mem_230 <= #wr_dly DIN[23:16];
		if (WE[3] == 1'b1)
			mem_330 <= #wr_dly DIN[31:24];
	end

always @(posedge CLK)
	if (WADDR == 5'b11111) begin
		if (WE[0] == 1'b1)
			mem_031 <= #wr_dly DIN[7:0];
		if (WE[1] == 1'b1)
			mem_131 <= #wr_dly DIN[15:8];
		if (WE[2] == 1'b1)
			mem_231 <= #wr_dly DIN[23:16];
		if (WE[3] == 1'b1)
			mem_331 <= #wr_dly DIN[31:24];
	end

endmodule
