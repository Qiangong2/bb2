module ram8x32d1 (
	DIN,
	DOUT,
	WE,
	CLK,
	RADDR,
	WADDR
		);

parameter	rd_dly = 1;
parameter	wr_dly = 1;

input	[31:0]	DIN;
output	[31:0]	DOUT;
input		WE;
input		CLK;
input	[2:0]	RADDR, WADDR;

reg	[31:0]	mem_0, mem_1, mem_2, mem_3, mem_4, mem_5, mem_6, mem_7;

assign	#rd_dly	DOUT	= {32{RADDR == 3'b000}} & mem_0 |
			  {32{RADDR == 3'b001}} & mem_1 |
			  {32{RADDR == 3'b010}} & mem_2 |
			  {32{RADDR == 3'b011}} & mem_3 |
			  {32{RADDR == 3'b100}} & mem_4 |
			  {32{RADDR == 3'b101}} & mem_5 |
			  {32{RADDR == 3'b110}} & mem_6 |
			  {32{RADDR == 3'b111}} & mem_7 ;

always @(posedge CLK)
	if (WE == 1'b1 & WADDR == 3'b000)
		mem_0	<= #wr_dly DIN;

always @(posedge CLK)
	if (WE == 1'b1 & WADDR == 3'b001)
		mem_1	<= #wr_dly DIN;

always @(posedge CLK)
	if (WE == 1'b1 & WADDR == 3'b010)
		mem_2	<= #wr_dly DIN;

always @(posedge CLK)
	if (WE == 1'b1 & WADDR == 3'b011)
		mem_3	<= #wr_dly DIN;
		
always @(posedge CLK)
	if (WE == 1'b1 & WADDR == 3'b100)
		mem_4	<= #wr_dly DIN;

always @(posedge CLK)
	if (WE == 1'b1 & WADDR == 3'b101)
		mem_5	<= #wr_dly DIN;

always @(posedge CLK)
	if (WE == 1'b1 & WADDR == 3'b110)
		mem_6	<= #wr_dly DIN;

always @(posedge CLK)
	if (WE == 1'b1 & WADDR == 3'b111)
		mem_7	<= #wr_dly DIN;
		
endmodule
