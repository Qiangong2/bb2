module ram8x32d4 (
	DIN,
	DOUT,
	WE,
	CLK,
	WADDR,
	RADDR
		);

parameter	rd_dly = 1;
parameter	wr_dly = 1;

input	[31:0]	DIN;
output	[31:0]	DOUT;
input	[3:0]	WE;
input		CLK;
input	[2:0]	WADDR, RADDR;

reg	[7:0]	mem_00, mem_01, mem_02, mem_03, mem_04, mem_05, mem_06, mem_07;
reg	[7:0]	mem_10, mem_11, mem_12, mem_13, mem_14, mem_15, mem_16, mem_17;
reg	[7:0]	mem_20, mem_21, mem_22, mem_23, mem_24, mem_25, mem_26, mem_27;
reg	[7:0]	mem_30, mem_31, mem_32, mem_33, mem_34, mem_35, mem_36, mem_37;

wire	[31:0]	dout_0	= {mem_30, mem_20, mem_10, mem_00};
wire	[31:0]	dout_1	= {mem_31, mem_21, mem_11, mem_01};
wire	[31:0]	dout_2	= {mem_32, mem_22, mem_12, mem_02};
wire	[31:0]	dout_3	= {mem_33, mem_23, mem_13, mem_03};
wire	[31:0]	dout_4	= {mem_34, mem_24, mem_14, mem_04};
wire	[31:0]	dout_5	= {mem_35, mem_25, mem_15, mem_05};
wire	[31:0]	dout_6	= {mem_36, mem_26, mem_16, mem_06};
wire	[31:0]	dout_7	= {mem_37, mem_27, mem_17, mem_07};

assign	#rd_dly	DOUT	= {32{RADDR == 3'b000}} & dout_0 |
			  {32{RADDR == 3'b001}} & dout_1 |
			  {32{RADDR == 3'b010}} & dout_2 |
			  {32{RADDR == 3'b011}} & dout_3 |
			  {32{RADDR == 3'b100}} & dout_4 |
			  {32{RADDR == 3'b101}} & dout_5 |
			  {32{RADDR == 3'b110}} & dout_6 |
			  {32{RADDR == 3'b111}} & dout_7;

always @(posedge CLK)
	if (WADDR == 3'b000) begin
		if (WE[0] == 1'b1)
			mem_00 <= #wr_dly DIN[7:0];
		if (WE[1] == 1'b1)
			mem_10 <= #wr_dly DIN[15:8];
		if (WE[2] == 1'b1)
			mem_20 <= #wr_dly DIN[23:16];
		if (WE[3] == 1'b1)
			mem_30 <= #wr_dly DIN[31:24];
	end			
	 			  
always @(posedge CLK)
	if (WADDR == 3'b001) begin
		if (WE[0] == 1'b1)
			mem_01 <= #wr_dly DIN[7:0];
		if (WE[1] == 1'b1)
			mem_11 <= #wr_dly DIN[15:8];
		if (WE[2] == 1'b1)
			mem_21 <= #wr_dly DIN[23:16];
		if (WE[3] == 1'b1)
			mem_31 <= #wr_dly DIN[31:24];
	end			
	 			  
always @(posedge CLK)
	if (WADDR == 3'b010) begin
		if (WE[0] == 1'b1)
			mem_02 <= #wr_dly DIN[7:0];
		if (WE[1] == 1'b1)
			mem_12 <= #wr_dly DIN[15:8];
		if (WE[2] == 1'b1)
			mem_22 <= #wr_dly DIN[23:16];
		if (WE[3] == 1'b1)
			mem_32 <= #wr_dly DIN[31:24];
	end			
	 			  
always @(posedge CLK)
	if (WADDR == 3'b011) begin
		if (WE[0] == 1'b1)
			mem_03 <= #wr_dly DIN[7:0];
		if (WE[1] == 1'b1)
			mem_13 <= #wr_dly DIN[15:8];
		if (WE[2] == 1'b1)
			mem_23 <= #wr_dly DIN[23:16];
		if (WE[3] == 1'b1)
			mem_33 <= #wr_dly DIN[31:24];
	end			
	 			  
always @(posedge CLK)
	if (WADDR == 3'b100) begin
		if (WE[0] == 1'b1)
			mem_04 <= #wr_dly DIN[7:0];
		if (WE[1] == 1'b1)
			mem_14 <= #wr_dly DIN[15:8];
		if (WE[2] == 1'b1)
			mem_24 <= #wr_dly DIN[23:16];
		if (WE[3] == 1'b1)
			mem_34 <= #wr_dly DIN[31:24];
	end			
	 			  
always @(posedge CLK)
	if (WADDR == 3'b101) begin
		if (WE[0] == 1'b1)
			mem_05 <= #wr_dly DIN[7:0];
		if (WE[1] == 1'b1)
			mem_15 <= #wr_dly DIN[15:8];
		if (WE[2] == 1'b1)
			mem_25 <= #wr_dly DIN[23:16];
		if (WE[3] == 1'b1)
			mem_35 <= #wr_dly DIN[31:24];
	end			
	 			  
always @(posedge CLK)
	if (WADDR == 3'b110) begin
		if (WE[0] == 1'b1)
			mem_06 <= #wr_dly DIN[7:0];
		if (WE[1] == 1'b1)
			mem_16 <= #wr_dly DIN[15:8];
		if (WE[2] == 1'b1)
			mem_26 <= #wr_dly DIN[23:16];
		if (WE[3] == 1'b1)
			mem_36 <= #wr_dly DIN[31:24];
	end			
	 			  
always @(posedge CLK)
	if (WADDR == 3'b111) begin
		if (WE[0] == 1'b1)
			mem_07 <= #wr_dly DIN[7:0];
		if (WE[1] == 1'b1)
			mem_17 <= #wr_dly DIN[15:8];
		if (WE[2] == 1'b1)
			mem_27 <= #wr_dly DIN[23:16];
		if (WE[3] == 1'b1)
			mem_37 <= #wr_dly DIN[31:24];
	end			
 
endmodule
