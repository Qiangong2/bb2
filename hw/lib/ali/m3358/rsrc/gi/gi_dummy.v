// gi_dummy.v Frank Berndt;
// dummy gi top level;
// :set tabstop=4

// io cells are outside this module;
// clk and pciclk must be be rising edge aligned;
// clk must be an integer multiple of pciclk;

// gi_dummy will be replaced by gi_pci;
// all signals are active 1;

`timescale 1ns/1ns

module gi_dummy (
	clk, clk_lock, pin_rst, sys_rst, alt_boot, test_ena,
	gc_rst, sec_intr, sec_ack,
	i2c_clk, i2c_din, i2c_dout, i2c_doe,
	pci_clk, pci_rst, pci_req, pci_gnt, pci_ognt, pci_intr,
	ad_in, ad_out, ad_oe, cbe_in, cbe_out, cbe_oe,
	par_in, par_out, par_oe, frame_in, frame_out, frame_oe,
	idsel, devsel_in, devsel_out, devsel_oe,
	irdy_in, irdy_out, irdy_oe, trdy_in, trdy_out, trdy_oe,
	stop_in, stop_out, stop_oe,
	di_in, di_out, di_oe,
	di_brk_in, di_brk_out, di_brk_oe,
	di_dir, di_hstrb, di_dstrb, di_err, di_cover, di_reset,
	ais_clk, ais_lr, ais_d
);
	input clk;					// gi core clock, 3x pci_clk;
	input clk_lock;				// clk is stable, pll locked;
	input pin_rst;				// pin reset;
	output sys_rst;				// system reset;
	input alt_boot;				// alternative boot;
	output test_ena;			// enable test/debug logic;
	output gc_rst;				// reset to flipper;
	output sec_intr;			// cause secure exception;
	input sec_ack;				// initial fetch for secure exception;
	output i2c_clk;				// i2c clock;
	input i2c_din;				// i2c data from io cell;
	output i2c_dout;			// i2c data to io cell;
	output i2c_doe;				// i2c data output enable;

	// pci interface;
	input idsel;
	input pci_clk;				// gi pci clock;
	input pci_rst;				// pci bus reset;
	input [31:0] ad_in;			// addr/data inputs;
	output [31:0] ad_out;		// addr/data outputs;
	output ad_oe;				// addr/data output enable;
	input [3:0] cbe_in;			// byte enable inputs;
	output [3:0] cbe_out;		// byte enable outputs;
	output cbe_oe;				// byte enable output enable;
	input par_in;				// parity from master;
	output par_out;				// parity when master;
	output par_oe;				// parity output enable;
	input frame_in;				// frame when target;
	output frame_out;			// frame when master;
	output frame_oe;			// frame output enable;
	input devsel_in;			// device selected;
	output devsel_out;			// device select;
	output devsel_oe;			// device select output enable;
	input irdy_in;				// initiator ready;
	output irdy_out;			// ready when master;
	output irdy_oe;				// irdy output enable;
	input trdy_in;				// target ready;
	output trdy_out;			// when target;
	output trdy_oe;				// trdy output enable;
	input stop_in;				// initiator stops transfer;
	output stop_out;			// target stops;
	output stop_oe;				// stop output enable;
	output pci_req;				// pci bus request;
	input pci_gnt;				// bus granted;
	input pci_ognt;				// OR of other grants, not host;
	output pci_intr;			// interrupt to processor;

	// disk interface;

	input [7:0] di_in;			// DI data from input cell;
	output [7:0] di_out;		// DI data to output cell;
	output di_oe;				// DI output enable;
	input di_brk_in;			// DI break from input cell;
	output di_brk_out;			// DI break to output cell;
	output di_brk_oe;			// DI break output enable;
	input di_dir;				// DI direction;
	input di_hstrb;				// DI host strobe;
	output di_dstrb;			// DI drive strobe;
	output di_err;				// DI error;
	output di_cover;			// DI cover;
	input di_reset;				// DI reset;

	// ais interface;

	input ais_clk;				// AIS clock from GC;
	input ais_lr;				// AIS left/right signal;
	output ais_d;				// AIS data out;

	// tie signals inactive;

	assign sys_rst = pin_rst;
	assign test_ena = 1'b1;

	assign ad_out = 32'd0;
	assign ad_oe = 1'b0;
	assign cbe_out = 4'd1;
	assign cbe_oe = 1'b0;
	assign par_out = 1'b0;
	assign par_oe = 1'b0;
	assign frame_out = 1'b1;
	assign frame_oe = 1'b0;
	assign devsel_out = 1'b0;
	assign devsel_oe = 1'b0;
	assign irdy_out = 1'b0;
	assign irdy_oe = 1'b0;
	assign trdy_out = 1'b1;
	assign trdy_oe = 1'b0;
	assign stop_out = 1'b1;
	assign stop_oe = 1'b0;
	assign pci_req = 1'b1;
	assign pci_intr = 1'b0;

	assign di_out = 8'd0;
	assign di_oe = 1'b0;
	assign di_brk_out = 1'b0;
	assign di_brk_oe = 1'b0;
	assign di_dstrb = 1'b1;
	assign di_err = 1'b0;
	assign di_cover = 1'b0;

	assign ais_d = 1'b0;

	assign i2c_clk 	= 1'b0;
	assign i2c_dout = 1'b1;
	assign i2c_doe	= 1'b1;
	assign gc_rst	= 1'b1;

	assign	sec_intr = 1'b1;

endmodule

