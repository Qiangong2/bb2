/*---------------------------------------------------------------------*/
/*               Copyright(c) Virage Logic Corporation.                */
/*     All Rights reserved - Unpublished -rights reserved under        */
/*     the Copyright laws of the United States of America.             */
/*                                                                     */
/*  This file includes the Confidential information of Virage Logic    */
/*  and UMC.                                                           */
/*  The receiver of this Confidential Information shall not disclose   */
/*  it to any third party and shall protect its confidentiality by     */
/*  using the same degree of care, but not less then a reasonable      */
/*  degree of care, as the receiver uses to protect receiver's own     */
/*  Confidential Information.                                          */
/*                                                                     */
/*                    Virage Logic Corporation.                        */
/*                    47100 Bayside Prkwy                              */
/*                    Fremont , California 94538.                      */
/*                                                                     */
/*---------------------------------------------------------------------*/
/*                                                                     */
/*  Software           : Rev: A03                                      */
/*  Library Format     : Rev: 1.05.00                                  */
/*  Compiler Name      : um18ugssvpnnnvncn000sa03                      */
/*  Date of Generation : Fri Feb 20 15:51:29 PST 2004:                 */
/*                                                                     */
/*   ---------------------------------------------------------------   */

//=======================================================================
//==================     NOVeA Memory System      =======================
//=======================================================================

`resetall

`timescale 1 ns / 10 ps


// ================  nms128x32  (NOVeA Memory System) =============
module nms128x32 (
      SYS_CK,
      WRCK,
      WRSTN,
      WSI,
      UpdateWR,
      ShiftWR,
      SelectWIR,
      TIME_B,
      ADR_p0, ADR_p1, ADR_p2, ADR_p3, ADR_p4, ADR_p5, ADR_p6, 
      D_p0, D_p1, D_p2, D_p3, D_p4, D_p5, D_p6, D_p7, D_p8, D_p9, D_p10, D_p11, D_p12, D_p13, D_p14, D_p15, D_p16, D_p17, D_p18, D_p19, D_p20, D_p21, D_p22, D_p23, D_p24, D_p25, D_p26, D_p27, D_p28, D_p29, D_p30, D_p31, 
      WE_p,
      ME_p,
      OE_p,
      PAE_p,
      PA_p,
      RA_p,

      WSO,
      Q0, Q1, Q2, Q3, Q4, Q5, Q6, Q7, Q8, Q9, Q10, Q11, Q12, Q13, Q14, Q15, Q16, Q17, Q18, Q19, Q20, Q21, Q22, Q23, Q24, Q25, Q26, Q27, Q28, Q29, Q30, Q31, 
      NMS_READY,
      NMS_PASS,
      KEEP_ON 
  );

 input SYS_CK;
 input WRCK;
 input WRSTN;
 input WSI;
 input UpdateWR;
 input ShiftWR;
 input SelectWIR;
 input TIME_B;
 input ADR_p0, ADR_p1, ADR_p2, ADR_p3, ADR_p4, ADR_p5, ADR_p6;
 input D_p0, D_p1, D_p2, D_p3, D_p4, D_p5, D_p6, D_p7, D_p8, D_p9, D_p10, D_p11, D_p12, D_p13, D_p14, D_p15, D_p16, D_p17, D_p18, D_p19, D_p20, D_p21, D_p22, D_p23, D_p24, D_p25, D_p26, D_p27, D_p28, D_p29, D_p30, D_p31;
 input WE_p;
 input ME_p;
 input OE_p;
 input PAE_p;
 input [2:0] PA_p;
 input RA_p;

 output WSO;
 output Q0, Q1, Q2, Q3, Q4, Q5, Q6, Q7, Q8, Q9, Q10, Q11, Q12, Q13, Q14, Q15, Q16, Q17, Q18, Q19, Q20, Q21, Q22, Q23, Q24, Q25, Q26, Q27, Q28, Q29, Q30, Q31;
 output NMS_READY;
 output NMS_PASS;
 output KEEP_ON; 

//  -----------       Wires        --------------
  tri Q0, Q1, Q2, Q3, Q4, Q5, Q6, Q7, Q8, Q9, Q10, Q11, Q12, Q13, Q14, Q15, Q16, Q17, Q18, Q19, Q20, Q21, Q22, Q23, Q24, Q25, Q26, Q27, Q28, Q29, Q30, Q31;
  wire WSO;
  wire NMS_READY;
  wire NMS_PASS;
  wire KEEP_ON;
  wire RCREADY;
  wire SO_NV;
  wire MATCH;
  wire UNLOCK;
  wire PORST;
  wire RST_i;
  wire PE;
  wire VPPRANGE;
  wire VPPSEL0, VPPSEL1, VPPSEL2, VPPSEL3;
  wire UDATA;
  wire PCLKE;
  wire SI_NV;
  wire SME_NV;
  wire STORE;
  wire COMP;
  wire RECALL;
  wire MRCL0;
  wire MRCL1;
  wire TECC0;
  wire TECC1;
  wire BIAS0;
  wire BIAS1;
  wire PU1;
  wire PU2;
  wire PU3;
  wire VPP;
  wire WE_NV;
  wire OE_NV;
  wire ME_NV;
  
  wire ADR_NV0, ADR_NV1, ADR_NV2, ADR_NV3, ADR_NV4, ADR_NV5, ADR_NV6;
  wire D_NV0, D_NV1, D_NV2, D_NV3, D_NV4, D_NV5, D_NV6, D_NV7, D_NV8, D_NV9, D_NV10, D_NV11, D_NV12, D_NV13, D_NV14, D_NV15, D_NV16, D_NV17, D_NV18, D_NV19, D_NV20, D_NV21, D_NV22, D_NV23, D_NV24, D_NV25, D_NV26, D_NV27, D_NV28, D_NV29, D_NV30, D_NV31;


// ----------  nms128x32_ctr (NMS Controller) Instantiation  ---------
  nms128x32_ctr U_NMSC (
      .SYS_CK (SYS_CK),
      .TIME_B (TIME_B),
      .WRCK (WRCK),
      .WRSTN (WRSTN),
      .WSI (WSI),
      .UpdateWR (UpdateWR),
      .ShiftWR (ShiftWR),
      .SelectWIR (SelectWIR),
      .PAE_p (PAE_p),
      .PA_p (PA_p),
      .RCREADY (RCREADY),
      .SO_NV (SO_NV),
      .MATCH (MATCH),
      .UNLOCK (UNLOCK),
      .PORST (PORST),
      .ADR_p ({ADR_p6, ADR_p5, ADR_p4, ADR_p3, ADR_p2, ADR_p1, ADR_p0}), 
      .D_p ({D_p31, D_p30, D_p29, D_p28, D_p27, D_p26, D_p25, D_p24, D_p23, D_p22, D_p21, D_p20, D_p19, D_p18, D_p17, D_p16, D_p15, D_p14, D_p13, D_p12, D_p11, D_p10, D_p9, D_p8, D_p7, D_p6, D_p5, D_p4, D_p3, D_p2, D_p1, D_p0}), 
      .RA_p (RA_p),
      .WE_p (WE_p),
      .ME_p (ME_p),
      .OE_p (OE_p),

      .WSO (WSO),
      .NMS_READY (NMS_READY),
      .NMS_PASS (NMS_PASS),
      .KEEP_ON (KEEP_ON),
      .RST_i (RST_i),
      .PE (PE),
      .VPPRANGE (VPPRANGE),
      .VPPSEL ({VPPSEL3, VPPSEL2, VPPSEL1, VPPSEL0}), 
      .UDATA (UDATA),
      .PCLKE (PCLKE),
      .SI_NV (SI_NV),
      .SME_NV (SME_NV),
      .STORE (STORE),
      .COMP (COMP),
      .RECALL (RECALL),
      .MRCL0 (MRCL0),
      .MRCL1 (MRCL1),
      .TECC0 (TECC0),
      .TECC1 (TECC1),
      .BIAS0 (BIAS0),
      .BIAS1 (BIAS1),
      .Q ({Q31, Q30, Q29, Q28, Q27, Q26, Q25, Q24, Q23, Q22, Q21, Q20, Q19, Q18, Q17, Q16, Q15, Q14, Q13, Q12, Q11, Q10, Q9, Q8, Q7, Q6, Q5, Q4, Q3, Q2, Q1, Q0}), 
      .ADR_NV ({ADR_NV6, ADR_NV5, ADR_NV4, ADR_NV3, ADR_NV2, ADR_NV1, ADR_NV0}), 
      .D_NV ({D_NV31, D_NV30, D_NV29, D_NV28, D_NV27, D_NV26, D_NV25, D_NV24, D_NV23, D_NV22, D_NV21, D_NV20, D_NV19, D_NV18, D_NV17, D_NV16, D_NV15, D_NV14, D_NV13, D_NV12, D_NV11, D_NV10, D_NV9, D_NV8, D_NV7, D_NV6, D_NV5, D_NV4, D_NV3, D_NV2, D_NV1, D_NV0}), 
      .WE_NV (WE_NV),
      .OE_NV (OE_NV),
      .ME_NV (ME_NV)

  );


// -----------  nvrm_um18ugs_128x32 (NOVeA Memory) Instantiation  ----------
  nvrm_um18ugs_128x32 U_NOVEA (
      .ADR0 (ADR_NV0), .ADR1 (ADR_NV1), .ADR2 (ADR_NV2), .ADR3 (ADR_NV3), .ADR4 (ADR_NV4), .ADR5 (ADR_NV5), .ADR6 (ADR_NV6), 
      .DIN0 (D_NV0), .DIN1 (D_NV1), .DIN2 (D_NV2), .DIN3 (D_NV3), .DIN4 (D_NV4), .DIN5 (D_NV5), .DIN6 (D_NV6), .DIN7 (D_NV7), .DIN8 (D_NV8), .DIN9 (D_NV9), .DIN10 (D_NV10), .DIN11 (D_NV11), .DIN12 (D_NV12), .DIN13 (D_NV13), .DIN14 (D_NV14), .DIN15 (D_NV15), .DIN16 (D_NV16), .DIN17 (D_NV17), .DIN18 (D_NV18), .DIN19 (D_NV19), .DIN20 (D_NV20), .DIN21 (D_NV21), .DIN22 (D_NV22), .DIN23 (D_NV23), .DIN24 (D_NV24), .DIN25 (D_NV25), .DIN26 (D_NV26), .DIN27 (D_NV27), .DIN28 (D_NV28), .DIN29 (D_NV29), .DIN30 (D_NV30), .DIN31 (D_NV31), 
      .WE (WE_NV),
      .ME (ME_NV),
      .OE (OE_NV),
      .CLK (SYS_CK),
      .SI (SI_NV),
      .SCLK (WRCK),
      .SME (SME_NV),
      .COMP (COMP),
      .STORE (STORE),
      .RECALL (RECALL),
      .VPP (VPP),
      .MRCL0 (MRCL0),
      .MRCL1 (MRCL1),
      .TECC0 (TECC0),
      .TECC1 (TECC1),
      .BIAS0 (BIAS0),
      .BIAS1 (BIAS1),
      .PU1 (PU1),
      .PU2 (PU2),
      .PU3 (PU3),

      .Q0 (Q0), .Q1 (Q1), .Q2 (Q2), .Q3 (Q3), .Q4 (Q4), .Q5 (Q5), .Q6 (Q6), .Q7 (Q7), .Q8 (Q8), .Q9 (Q9), .Q10 (Q10), .Q11 (Q11), .Q12 (Q12), .Q13 (Q13), .Q14 (Q14), .Q15 (Q15), .Q16 (Q16), .Q17 (Q17), .Q18 (Q18), .Q19 (Q19), .Q20 (Q20), .Q21 (Q21), .Q22 (Q22), .Q23 (Q23), .Q24 (Q24), .Q25 (Q25), .Q26 (Q26), .Q27 (Q27), .Q28 (Q28), .Q29 (Q29), .Q30 (Q30), .Q31 (Q31), 
      .SO (SO_NV),
      .MATCH (MATCH),
      .RCREADY (RCREADY)
  );

//-----------  nvcp_um18gfs  (Charge Pump)  Instantiation  ---------
  nvcp_um18gfs U_ChPump (
      .CLK (SYS_CK),
      .RST (RST_i),
      .VPPSEL ({VPPSEL3, VPPSEL2, VPPSEL1, VPPSEL0}), 
      .VPPRANGE (VPPRANGE),
      .PE (PE),
      .D (UDATA),
      .CLKE (PCLKE),
      .STORE (STORE),

      .VPP (VPP),
      .UNLOCK (UNLOCK),
      .PU1 (PU1),
      .PU2 (PU2),
      .PU3 (PU3),
      .PORST (PORST)
    );

endmodule   // ----  nms128x32  (NMS)

