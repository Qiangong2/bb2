/*---------------------------------------------------------------------*/
/*               Copyright(c) Virage Logic Corporation.                */
/*     All Rights reserved - Unpublished -rights reserved under        */
/*     the Copyright laws of the United States of America.             */
/*                                                                     */
/*  This file includes the Confidential information of Virage Logic    */
/*  and UMC.                                                           */
/*  The receiver of this Confidential Information shall not disclose   */
/*  it to any third party and shall protect its confidentiality by     */
/*  using the same degree of care, but not less then a reasonable      */
/*  degree of care, as the receiver uses to protect receiver's own     */
/*  Confidential Information.                                          */
/*                                                                     */
/*                    Virage Logic Corporation.                        */
/*                    47100 Bayside Prkwy                              */
/*                    Fremont , California 94538.                      */
/*                                                                     */
/*---------------------------------------------------------------------*/
/*                                                                     */
/*  Software           : Rev: A03                                      */
/*  Library Format     : Rev: 1.05.00                                  */
/*  Compiler Name      : um18ugssvpnnnvncn000sa03                      */
/*  Date of Generation : Fri Feb 20 15:51:30 PST 2004:                 */
/*                                                                     */
/*   ---------------------------------------------------------------   */

//=======================================================================
//=============       NOVeA Memory System Controller       ==============
//=======================================================================

`resetall
`timescale 1 ns / 10 ps


// ----------------       Instructions       -----------------
  `define I_BYPASS           6'd0
  `define I_IDLE             `I_BYPASS
  `define I_SERIAL           6'd1
  `define I_FULL_STORE       6'd2
  `define I_NRECALL          6'd3
  `define I_NCOMPARE         6'd4
  `define I_KEEP             6'd5
  `define I_RECALLAM         6'd6

  `define I_SOFT_STORE       6'd8
  `define I_MRECALL          6'd9
  `define I_MCOMPARE         6'd10
  `define I_SER_LOOP         6'd11
  `define I_AUTO_SHIFT       6'd12
  `define I_RESET            6'd63

  `define FSTORE_DEF_CFG     45'b010101_010110_011001_011010_010000_001_00_10_0000_00_01

  `define TESTCFG_DEF        6'b010000


// ----------------       FSM States       -----------------
  `define ST_RESET_INIT       5'd0
  `define ST_RESETTING        5'd1
  `define ST_RESET_RECOV      5'd2
  `define ST_READY            5'd3
  `define ST_UNLOCKING        5'd4
  `define ST_STOR_SET_VPP     5'd5
  `define ST_STOR_S1          5'd6
  `define ST_STOR_P1          5'd7
  `define ST_STOR_P0          5'd8
  `define ST_STOR_S0          5'd9
  `define ST_STOR_INC_VPP     5'd10
  `define ST_COMP_INIT        5'd11
  `define ST_COMPARING        5'd12
  `define ST_COMP_RECOV       5'd13
  `define ST_RECALL_INIT      5'd14
  `define ST_RECALLING        5'd15
  `define ST_RECALL_RECOV     5'd16
  `define ST_KEEP_INIT        5'd17
  `define ST_KEEP_ON          5'd18
  `define ST_AUTOSHIFT        5'd19

// ================         nms128x32_ctr  (NMS_Controller)     ==============
module nms128x32_ctr (
      SYS_CK,
      TIME_B,
      WRCK,
      WRSTN,
      WSI,
      UpdateWR,
      ShiftWR,
      SelectWIR,
      PAE_p,
      PA_p,
      RCREADY,
      SO_NV,
      MATCH,
      UNLOCK,
      PORST,
      ADR_p,
      D_p,
      RA_p,
      WE_p,
      ME_p,
      OE_p,

      WSO,
      NMS_READY,
      NMS_PASS,
      KEEP_ON,
      RST_i,
      PE,
      VPPRANGE,
      VPPSEL,
      UDATA,
      PCLKE,
      SI_NV,
      SME_NV,
      STORE,
      COMP,
      RECALL,
      MRCL0,
      MRCL1,
      TECC0,
      TECC1,
      BIAS0,
      BIAS1,
      Q,
      WE_NV,
      OE_NV,
      ME_NV,
      ADR_NV,
      D_NV
      );

 input SYS_CK;
 input TIME_B;
 input WRCK;
 input WRSTN;
 input WSI;
 input UpdateWR;
 input ShiftWR;
 input SelectWIR;
 input PAE_p;
 input [2:0] PA_p;
 input RCREADY;
 input SO_NV;
 input MATCH;
 input UNLOCK;
 input PORST;
 input [6:0] ADR_p;
 input [31:0] D_p;
 input RA_p;
 input WE_p;
 input ME_p;
 input OE_p;

 output WSO;
 output NMS_READY;
 output NMS_PASS;
 output KEEP_ON;
 output RST_i;
 output PE;
 output VPPRANGE;
 output [3:0] VPPSEL;
 output UDATA;
 output PCLKE;
 output SI_NV;
 output SME_NV;
 output STORE;
 output COMP;
 output RECALL;
 output MRCL0;
 output MRCL1;
 output TECC0;
 output TECC1;
 output BIAS0;
 output BIAS1;
 output [31:0] Q;
 output WE_NV;
 output OE_NV;
 output ME_NV;

 output [6:0] ADR_NV;
 output [31:0] D_NV;

// ----------       Wires       ------------
  wire WSO;
  wire NMS_READY;
  wire NMS_PASS;
  wire KEEP_ON;
  wire RST_i;
  wire PE;
  wire VPPRANGE;
  wire [3:0] VPPSEL;
  wire UDATA;
  wire PCLKE;
  wire STORE;
  wire COMP;
  wire RECALL;
  wire MRCL0;
  wire MRCL1;
  wire TECC0;
  wire TECC1;
  wire BIAS0;
  wire BIAS1;
  wire rst_NVC_a;
  wire rst_WIR_a;
  wire SME_NV;
  wire SI_NV;
  wire OE_CR;
  wire WE_NV;
  wire OE_NV;
  wire ME_NV;

  wire TIME_B_Pedge;
  wire Instr_En;
  wire Instr_Chng;
  wire Bypass;
  wire FullStore;
  wire SoftStore;
  wire MCompare;
  wire NCompare;
  wire MRecall;
  wire NRecall;
  wire Serial;
  wire Serial_Loop;
  wire AutoShift;
  wire Keep_Start;
  wire Keep_End;
  wire Recall_AM;
  wire Reset_Cmd;
  wire MATCH_met;
  wire RCREADY_met;
  wire SerEn_NVC;
  wire SerIn_NVC;
  wire SerOut_NVC;
  wire rst_ShCounter;
  wire AutoShift_En;
  wire ShCounter_End;

  wire [5:0] TestCfg_1;
  wire [5:0] TestCfg_2;
  wire [5:0] TestCfg_3;
  wire [5:0] TestCfg_4;
  wire [5:0] TestCfg_5;
  wire [2:0] N_Dumb;
  wire [1:0] Delta_VPP;
  wire [1:0] VPP_Max;
  wire [3:0] VPP_Level;
  wire [1:0] N_Max;
  wire [1:0] P_Width;
  wire [7:0] Q_CR;
  wire [31:0] Q;

  wire [2:0] Addr_CR;
  wire [7:0] D_CR;

  wire [6:0] ADR_NV;
  wire [31:0] D_NV;

//--------------------    Continuous Assignments    ---------
  assign Addr_CR = ADR_p[2:0];
  assign D_CR = D_p[7:0];

  assign ADR_NV   = ADR_p;
  assign D_NV     = D_p;

// -----  NOVEA_Controller  Instantiation ------------------
  NOVEA_Controller U_NVC (
      .clk_NVC(SYS_CK),
      .rst_NVC_a(rst_NVC_a),
      .TIME_B_Pedge(TIME_B_Pedge),
      .Instr_En(Instr_En),
      .Instr_Chng(Instr_Chng),
      .Bypass(Bypass),
      .FullStore(FullStore),
      .SoftStore(SoftStore),
      .MCompare(MCompare),
      .NCompare(NCompare),
      .MRecall(MRecall),
      .NRecall(NRecall),
      .Serial(Serial),
      .Serial_Loop(Serial_Loop),
      .AutoShift(AutoShift),
      .Keep_Start(Keep_Start),
      .Keep_End(Keep_End),
      .Recall_AM(Recall_AM),
      .Reset_Cmd(Reset_Cmd),
      .TestCfg_1(TestCfg_1),
      .TestCfg_2(TestCfg_2),
      .TestCfg_3(TestCfg_3),
      .TestCfg_4(TestCfg_4),
      .TestCfg_5(TestCfg_5),
      .N_Dumb(N_Dumb),
      .Delta_VPP(Delta_VPP),
      .VPP_Max(VPP_Max),
      .VPP_Level(VPP_Level),
      .N_Max(N_Max),
      .P_Width(P_Width),
      .MATCH_met(MATCH_met),
      .RCREADY_met(RCREADY_met),
      .UNLOCK(UNLOCK),
      .ShCounter_End(ShCounter_End),

      .COMP(COMP),
      .STORE(STORE),
      .RECALL(RECALL),
      .RST_i(RST_i),
      .TECC0(TECC0),
      .TECC1(TECC1),
      .MRCL0(MRCL0),
      .MRCL1(MRCL1),
      .BIAS0(BIAS0),
      .BIAS1(BIAS1),
      .NMS_READY(NMS_READY),
      .NMS_PASS(NMS_PASS),
      .KEEP_ON(KEEP_ON),
      .VPPRANGE(VPPRANGE),
      .VPPSEL(VPPSEL),
      .PE(PE),
      .UDATA(UDATA),
      .PCLKE(PCLKE),
      .rst_ShCounter(rst_ShCounter),
      .AutoShift_En(AutoShift_En)
  );

// -----  Serial_Controller  Instantiation ------------------
  Serial_Controller U_SerC (
      .WRCK(WRCK),
      .rst_WIR_a(rst_WIR_a),
      .rst_ShCounter(rst_ShCounter),
      .SO_NV(SO_NV),
      .SerEn_NVC(SerEn_NVC),
      .SerIn_NVC(SerIn_NVC),
      .Serial(Serial),
      .Serial_Loop(Serial_Loop),
      .AutoShift_En(AutoShift_En),

      .SME_NV(SME_NV),
      .SI_NV(SI_NV),
      .SerOut_NVC(SerOut_NVC),
      .ShCounter_End(ShCounter_End)
  );


// -----  P1500 Interface  Instantiation ------------------
  interface U_interface (
      .WRCK(WRCK),
      .WRSTN(WRSTN),
      .WSI(WSI),
      .UpdateWR(UpdateWR),
      .ShiftWR(ShiftWR),
      .SelectWIR(SelectWIR),
      .clk_NVC(SYS_CK),
      .PORST(PORST),
      .MATCH(MATCH),
      .RCREADY(RCREADY),
      .TIME_B(TIME_B),
      .PAE_p(PAE_p),
      .PA_p(PA_p),
      .KEEP_ON(KEEP_ON),
      .NMS_READY(NMS_READY),
      .SerOut_NVC(SerOut_NVC),
      .Addr_CR(Addr_CR),
      .D_CR(D_CR),
      .RA_p(RA_p),
      .WE_p(WE_p),
      .ME_p(ME_p),
      .OE_p(OE_p),

      .WSO(WSO),
      .SerIn_NVC(SerIn_NVC),
      .SerEn_NVC(SerEn_NVC),
      .Instr_En(Instr_En),
      .Instr_Chng(Instr_Chng),
      .Bypass(Bypass),
      .FullStore(FullStore),
      .SoftStore(SoftStore),
      .MCompare(MCompare),
      .NCompare(NCompare),
      .MRecall(MRecall),
      .NRecall(NRecall),
      .Serial(Serial),
      .Serial_Loop(Serial_Loop),
      .AutoShift(AutoShift),
      .Keep_Start(Keep_Start),
      .Keep_End(Keep_End),
      .Recall_AM(Recall_AM),
      .Reset_Cmd(Reset_Cmd),
      .rst_NVC_a(rst_NVC_a),
      .rst_WIR_a(rst_WIR_a),
      .MATCH_met(MATCH_met),
      .RCREADY_met(RCREADY_met),
      .TIME_B_Pedge(TIME_B_Pedge),
      .TestCfg_1(TestCfg_1),
      .TestCfg_2(TestCfg_2),
      .TestCfg_3(TestCfg_3),
      .TestCfg_4(TestCfg_4),
      .TestCfg_5(TestCfg_5),
      .N_Dumb(N_Dumb),
      .Delta_VPP(Delta_VPP),
      .VPP_Max(VPP_Max),
      .VPP_Level(VPP_Level),
      .N_Max(N_Max),
      .P_Width(P_Width),
      .Q_CR(Q_CR),
      .OE_CR(OE_CR),
      .WE_NV(WE_NV),
      .OE_NV(OE_NV),
      .ME_NV(ME_NV)

  );


// -----------       tri_Q  Instantiation       ----------
  tri_Q U_tri_Q(
      .Q_CR(Q_CR),
      .OE_CR(OE_CR),

      .Q(Q)
  );

endmodule  // ---   nms128x32_ctr  (NMS_Controller)


// =============================================================================


// ===================         NOVeA  Controller    ==============
module NOVEA_Controller(
      clk_NVC,
      rst_NVC_a,
      TIME_B_Pedge,
      Instr_En,
      Instr_Chng,
      Bypass,
      FullStore,
      SoftStore,
      MCompare,
      NCompare,
      MRecall,
      NRecall,
      Serial,
      Serial_Loop,
      AutoShift,
      Keep_Start,
      Keep_End,
      Recall_AM,
      Reset_Cmd,
      TestCfg_1,
      TestCfg_2,
      TestCfg_3,
      TestCfg_4,
      TestCfg_5,
      N_Dumb,
      Delta_VPP,
      VPP_Max,
      VPP_Level,
      N_Max,
      P_Width,
      MATCH_met,
      RCREADY_met,
      UNLOCK,
      ShCounter_End,

      COMP,
      STORE,
      RECALL,
      RST_i,
      TECC0,
      TECC1,
      MRCL0,
      MRCL1,
      BIAS0,
      BIAS1,
      NMS_READY,
      NMS_PASS,
      KEEP_ON,
      VPPRANGE,
      VPPSEL,
      PE,
      UDATA,
      PCLKE,
      rst_ShCounter,
      AutoShift_En
      );

// -----------------       Port List       ------------
 input clk_NVC;              // Clock of NOVeA Controller
 input rst_NVC_a;            // Asynchronous Positive Reset
 input TIME_B_Pedge;         // Posedge of TIME_B
 input Instr_En;             // Instruction Enable
 input Instr_Chng;           // Instruction will be Changed
 input Bypass;               // Instruction
 input FullStore;            // Instruction
 input SoftStore;            // Instruction
 input MCompare;             // Instruction
 input NCompare;             // Instruction
 input MRecall;              // Instruction
 input NRecall;              // Instruction
 input Serial;               // Instruction
 input Serial_Loop;          // Instruction
 input AutoShift;            // Instruction
 input Keep_Start;           // Instruction
 input Keep_End;             // Instruction
 input Recall_AM;            // Instruction
 input Reset_Cmd;            // Instruction

 input [5:0] TestCfg_1;      // Margin Test 1 Configuration
 input [5:0] TestCfg_2;      // Margin Test 2 Configuration
 input [5:0] TestCfg_3;      // Margin Test 3 Configuration
 input [5:0] TestCfg_4;      // Margin Test 4 Configuration
 input [5:0] TestCfg_5;      // Margin Test 5 Configuration
 input [2:0] N_Dumb;         // Number of Dumb Store Pulses
 input [1:0] Delta_VPP;      // Determines VPP Increment Value
 input [1:0] VPP_Max;        // Determines Max Value of VPP
 input [3:0] VPP_Level;      // Initial Level of VPP
 input [1:0] N_Max;          // Max Number of Store Pulses at VPP_Max
 input [1:0] P_Width;        // Store Pulse Width

 input MATCH_met;            // Double FlipFloped MATCH
 input RCREADY_met;          // Double FlipFloped RCREADY
 input UNLOCK;               // Unlock status of Charge Pump

 input ShCounter_End;        // AutoShift has been Completed


 output COMP;
 output STORE;
 output RECALL;
 output TECC0;
 output TECC1;
 output MRCL0;
 output MRCL1;
 output BIAS0;
 output BIAS1;
 output NMS_READY;
 output NMS_PASS;
 output KEEP_ON;
 output RST_i;
 output VPPRANGE;
 output [3:0] VPPSEL;
 output PE;
 output UDATA;
 output PCLKE;

 output rst_ShCounter;        // Reset ShiftCounter
 output AutoShift_En;         // Autoshift Enable

//-------------------       Registers      ----------

 reg [4:0] CurrentState;
 reg [4:0] NextState;
 reg VPPRANGE_r;              // VPPRANGE
 reg [3:0] VPPSEL_r;          // VPPSEL with Carry
 reg [2:0] CmpCounter_r;      // Compare Test Counter
 reg [17:0] Timer_r;          // Timer used for Store, Recall, Compare ...
 reg Unlock_r;                // from UNLOCK input
 reg NVSRAM_Indef_r;          // NOVeA SRAM Contents (init state) is Indefinite (flag)
 reg Oper_Pass_r;             // Operation Passed (flag)
 reg Illeg_Action_r;          // Illegal Action has been done (flag)
 reg Instr_En_pip_r;          // Instr_En Piped
 reg Instr_Chng_pip_r;        // Instr_Chng Piped
 reg [7:0] Unlock_Seq_r;      // Register containing Unlock Sequence
 reg STORE_r;                 // to Output
 reg COMP_r;                  // to Output
 reg RECALL_r;                // to Output
 reg NMS_Ready_r;             // to Output
 reg NMS_Pass_r;              // to Output
 reg Keep_On_r;               // to Output
 reg TECC0_r;                 // to Output
 reg TECC1_r;                 // to Output
 reg MRCL0_r;                 // to Output
 reg MRCL1_r;                 // to Output
 reg BIAS0_r;                 // to Output
 reg BIAS1_r;                 // to Output
 reg RST_i_r;                 // to Output
 reg PE_r;                    // to Output
 reg [7:0] NMax_Decoded;      // Decoded Value of N_Max
 reg [3:0] VPPMax_Decoded;    // Decoded Value of VPP_Max
 reg [3:0] DeltaVPP_Decoded;  // Decoded Value of Delta_VPP
 reg [7:0] VMaxPulse_Cntr_r;  // Counter of Store Pulses at VPP_Max
 reg [2:0] DumbPulse_Cntr_r;  // Counter of Dumb Store Pulses

 reg [1:0] ShCounter_End_met_r;// Antimetastability Double FlipFlops

//-------------------       Wires      ----------
 wire STORE;                  // Output
 wire COMP;                   // Output
 wire RECALL;                 // Output
 wire PE;                     // Output
 wire PCLKE;                  // Output
 wire UDATA;                  // Output
 wire RST_i;                  // Output
 wire NMS_READY;              // Output
 wire NMS_PASS;               // Output
 wire VPPRANGE;               // Output
 wire [3:0] VPPSEL;           // Output
 wire KEEP_ON;                // Output
 wire rst_ShCounter;          // Output
 wire AutoShift_En;           // Output

 wire Transition;             // FSM State Transition
 wire Set_OperPass;           // Set Oper_Pass_r to 1
 wire Reset_OperPass;         // Reset Oper_Pass_r to 0
 wire Instr_En_Pedge;         // Posedge of Instr_En
 wire [4:0] VPPSEL_Inc;       // Increased Value of VPPSEL
 wire VPP_Eq_VPPMax;          // VPPSEL is equal to VPP_Max
 wire NMS_Ready_nxt;          // Next Value for NMS_Ready_r

 wire ShCounter_End_met;      // ShCounter_End Double FlipFloped

 wire Time_Reset_End;
 wire Time_Reset_Recov;
 wire Time_Stor_P1;
 wire Time_Stor_P0;
 wire Time_Stor_S0;
 wire Time_Stor_Recov;
 wire Time_Comp_Recov;
 wire Time_Recall_Recov;
 wire Time_Cfg_Setup;
 wire rst_WatchDog;           // Reset from WatchDog

 wire TECC0;
 wire TECC1;
 wire MRCL0;
 wire MRCL1;
 wire BIAS0;
 wire BIAS1;

//--------------------    Continuous Assignments    ---------
  assign NMS_READY = NMS_Ready_r;
  assign NMS_PASS  = NMS_Pass_r;
  assign KEEP_ON   = Keep_On_r;
  assign STORE  = STORE_r;
  assign COMP   = COMP_r;
  assign RECALL = RECALL_r;
  assign TECC0 = TECC0_r;
  assign TECC1 = TECC1_r;
  assign MRCL0 = MRCL0_r;
  assign MRCL1 = MRCL1_r;
  assign BIAS0 = BIAS0_r;
  assign BIAS1 = BIAS1_r;
  assign #1.1 VPPSEL = VPPSEL_r;
  assign #1.1 VPPRANGE = VPPRANGE_r;
  assign NMS_Ready_nxt = ((CurrentState == `ST_READY) || ((CurrentState == `ST_KEEP_ON) && RCREADY_met));
  assign PE = PE_r;
  assign RST_i = rst_NVC_a | RST_i_r;
  assign #1.1 PCLKE = ((CurrentState == `ST_UNLOCKING) || (CurrentState == `ST_STOR_SET_VPP));
  assign #1.1 UDATA = (CurrentState == `ST_UNLOCKING) ? Unlock_Seq_r[7] : 1'b0;
  assign rst_ShCounter = (rst_NVC_a | RECALL | STORE);
  assign AutoShift_En = ((CurrentState == `ST_AUTOSHIFT) & ~NVSRAM_Indef_r);
  assign Transition = (CurrentState != NextState);

  assign Set_OperPass = (((CurrentState == `ST_COMPARING) & RCREADY_met & MATCH_met)
                | ((CurrentState == `ST_READY) & (Serial | Serial_Loop | Bypass))
                | ((CurrentState == `ST_AUTOSHIFT) & ShCounter_End_met)
                |  (CurrentState == `ST_RECALL_RECOV)
                | ((CurrentState == `ST_STOR_S0) & SoftStore)
                |  (CurrentState == `ST_KEEP_ON));

  assign Reset_OperPass = (((CurrentState == `ST_READY) & Instr_En_Pedge &
                            ~(Serial | Serial_Loop | Bypass))
                | ((CurrentState == `ST_COMPARING) & RCREADY_met & ~MATCH_met) | rst_WatchDog);

  assign Instr_En_Pedge = (Instr_En & ~Instr_En_pip_r);
  assign VPPSEL_Inc = VPPSEL_r + (Oper_Pass_r ? 4'd1 : DeltaVPP_Decoded);
  assign VPP_Eq_VPPMax = (VPPSEL_r == VPPMax_Decoded);

  assign ShCounter_End_met = ShCounter_End_met_r[1];

  assign Time_Reset_End     = Timer_r == 16;
  assign Time_Reset_Recov   = Timer_r == 3;
  assign Time_Stor_P1       = Timer_r == 751;
  assign Time_Stor_P0       = Timer_r == ((P_Width == 2'b00) ?  2001 :
                                          (P_Width == 2'b01) ? 10001 :
                                          (P_Width == 2'b10) ? 50001 :
                                                              250001);
  assign Time_Stor_S0       = Timer_r == 3001;
  assign Time_Stor_Recov    = Timer_r == 3001;
  assign Time_Comp_Recov    = Timer_r == 7;
  assign Time_Recall_Recov  = Timer_r == 7;
  assign Time_Cfg_Setup     = Timer_r == 31;

  assign rst_WatchDog       = Timer_r == 260001;

//--------------------    NMax_Decoded    ---------
  always @(N_Max)
    case (N_Max)
        2'b00:    NMax_Decoded = 8'd2;
        2'b01:    NMax_Decoded = 8'd10;
        2'b10:    NMax_Decoded = 8'd50;
        2'b11:    NMax_Decoded = 8'd250;
    endcase

//--------------------    VPPMax_Decoded    ---------
  always @(VPP_Max)
    case (VPP_Max)
        2'b00:    VPPMax_Decoded = 4'b1001;
        2'b01:    VPPMax_Decoded = 4'b1011;
        2'b10:    VPPMax_Decoded = 4'b1101;
        2'b11:    VPPMax_Decoded = 4'b1111;
    endcase

//--------------------    DeltaVPP_Decoded    ---------
  always @(Delta_VPP)
    case (Delta_VPP)
        2'b00:    DeltaVPP_Decoded = 4'd1;
        2'b01:    DeltaVPP_Decoded = 4'd2;
        2'b10:    DeltaVPP_Decoded = 4'd4;
        2'b11:    DeltaVPP_Decoded = 4'd8;
    endcase

//---------------------    NOVeA_Controller FSM  CurrentState    --------
  always @(posedge clk_NVC or posedge rst_NVC_a)
    begin
      if (rst_NVC_a)
          CurrentState <= #1 `ST_RESET_INIT;
      else if (Reset_Cmd | rst_WatchDog)
              CurrentState <= #1 `ST_RESET_INIT;
            else  CurrentState <= #1 NextState;
    end

//---------------------    NOVeA_Controller FSM  NextState    --------
  always @(CurrentState or Unlock_r or MRecall or NRecall or FullStore or SoftStore
           or CmpCounter_r or AutoShift or ShCounter_End_met or Instr_En_Pedge
           or Time_Reset_End or Time_Reset_Recov or Time_Stor_P1 or Time_Stor_P0 or Time_Stor_S0
           or Time_Stor_Recov or Time_Cfg_Setup or Time_Comp_Recov or Time_Recall_Recov
           or MCompare or NCompare or Keep_Start or Oper_Pass_r or DumbPulse_Cntr_r
           or N_Dumb or VPP_Eq_VPPMax or VMaxPulse_Cntr_r or NMax_Decoded or RCREADY_met
           or Keep_End or Recall_AM or NVSRAM_Indef_r)
    begin
      case (CurrentState)


  // - - - - - - - - - - - -      Reset States       - - - - - - - - - - - -
        `ST_RESET_INIT:
            NextState = `ST_RESETTING;


        `ST_RESETTING:
            if (Time_Reset_End)  NextState = `ST_RESET_RECOV;
            else NextState = CurrentState;


        `ST_RESET_RECOV:
            if (Time_Reset_Recov)
              begin
                if (Unlock_r) NextState = `ST_RESET_INIT;
                else NextState = `ST_READY;
              end
            else NextState = CurrentState;


  // - - - - - - - - - - - -      Ready States       - - - - - - - - - - - -
        `ST_READY:
            if (Instr_En_Pedge)
              begin
                if (AutoShift) NextState = `ST_AUTOSHIFT;
                else if (MCompare | NCompare)  NextState = `ST_COMP_INIT;
                    else if (MRecall | NRecall | Recall_AM)  NextState = `ST_RECALL_INIT;
                        else if (Keep_Start) NextState = `ST_KEEP_INIT;
                            else if (FullStore | SoftStore)  NextState = `ST_UNLOCKING;
                                else   NextState = CurrentState;
              end
            else  NextState = CurrentState;


  // - - - - - - - - - - - -      Store States       - - - - - - - - - - - -
        `ST_UNLOCKING:
            if (Unlock_r) NextState = `ST_STOR_SET_VPP;
            else NextState = CurrentState;


        `ST_STOR_SET_VPP:
            NextState = `ST_STOR_S1;


        `ST_STOR_S1:
            if (Time_Stor_P1)  NextState = `ST_STOR_P1;
            else NextState = CurrentState;


        `ST_STOR_P1:
            if (Time_Stor_P0)  NextState = `ST_STOR_P0;
            else NextState = CurrentState;


        `ST_STOR_P0:
            if (Time_Stor_S0)  NextState = `ST_STOR_S0;
            else NextState = CurrentState;


        `ST_STOR_S0:
            if (Time_Stor_Recov)
              begin
                if (~(FullStore | Recall_AM))         // SoftStore
                    NextState = `ST_RESET_INIT;
                else if (~Oper_Pass_r)  // Smart FullStore
                        NextState = `ST_COMP_INIT;
                        else    NextState = `ST_STOR_INC_VPP;    // Dumb Store
              end
            else NextState = CurrentState;


        `ST_STOR_INC_VPP:    // Here Oper_Pass_r is a Result of Comparison
            if (Oper_Pass_r)  // Dumb Store
              begin
                if ((DumbPulse_Cntr_r == N_Dumb) || VPP_Eq_VPPMax) // Dumb Store End
                      NextState = `ST_RESET_INIT;  // Store Passed
                else  NextState = `ST_STOR_SET_VPP;
              end
            else   // Smart FullStore
              begin
                if (VPP_Eq_VPPMax & (VMaxPulse_Cntr_r == NMax_Decoded))
                      NextState = `ST_RESET_INIT;  // Store Failed
                else  NextState = `ST_STOR_SET_VPP;
              end


  // - - - - - - - - - - -      Compare States       - - - - - - - - - - - -
        `ST_COMP_INIT:
            if (Time_Cfg_Setup) NextState = `ST_COMPARING;
            else NextState = CurrentState;


        `ST_COMPARING:
            if (RCREADY_met)  NextState = `ST_COMP_RECOV;
            else NextState = CurrentState;


        `ST_COMP_RECOV:    // Here Oper_Pass_r is a Result of Comparison
            if (Time_Comp_Recov)
              begin
                if ((FullStore | MCompare | Recall_AM) & Oper_Pass_r
                   & (CmpCounter_r != 3'd4)) // Compare is not Completed
                    NextState = `ST_COMP_INIT;
                else if (FullStore | (Recall_AM & Unlock_r)) // FullStore or (Store during Recall_AM)
                        NextState = `ST_STOR_INC_VPP;
                    else if (Recall_AM & ~Oper_Pass_r & ~Unlock_r) // Compare during Recall_AM Failed
                            NextState = `ST_UNLOCKING;
                        else  // (NCompare | (MCompare & (CmpCounter_r == 3'd4))) | ...
                               NextState = `ST_RESET_INIT;
              end
            else NextState = CurrentState;


  // - - - - - - - - - - - -     Recall States       - - - - - - - - - - - -
        `ST_RECALL_INIT:
            if (Time_Cfg_Setup) NextState = `ST_RECALLING;
            else NextState = CurrentState;


        `ST_RECALLING:
            if (RCREADY_met) NextState = `ST_RECALL_RECOV;
            else NextState = CurrentState;


        `ST_RECALL_RECOV:
            if (Time_Recall_Recov)
              begin
                if (Recall_AM) NextState = `ST_COMP_INIT;
                else  NextState = `ST_RESET_INIT;
              end
            else NextState = CurrentState;


  // - - - - - - - - - - - -     Keep States       - - - - - - - - - - -
        `ST_KEEP_INIT:
            if (Time_Cfg_Setup) NextState = `ST_KEEP_ON;
            else NextState = CurrentState;


        `ST_KEEP_ON:
            if (RCREADY_met & Keep_End) NextState = `ST_RECALL_RECOV;
            else NextState = CurrentState;


  // - - - - - - - - - - - -     AutoShift States       - - - - - - - - - - -
        `ST_AUTOSHIFT:
            if (ShCounter_End_met | NVSRAM_Indef_r) NextState = `ST_READY;
            else NextState = CurrentState;


        default:
            NextState = `ST_RESET_INIT;


      endcase
    end
// -------------------------------------------     End of FSM code  ------------


//--------------------    Timer_r    ---------
  always @(posedge clk_NVC or posedge rst_NVC_a)
    if (rst_NVC_a)
        Timer_r <= #1 18'b0;
    else if (Transition | NMS_Ready_nxt)
            Timer_r <= #1 18'b0;
        else if (TIME_B_Pedge)
                Timer_r <= #1 Timer_r + 1;
            else Timer_r <= #1 Timer_r;

//--------------------    NMS_Ready_r    -----------
  always @(posedge clk_NVC or posedge rst_NVC_a)
      if (rst_NVC_a)
          NMS_Ready_r <= #1 1'b0;
      else
          NMS_Ready_r <= #1 NMS_Ready_nxt;

//--------------------    NMS_Pass_r    -----------
  always @(posedge clk_NVC or posedge rst_NVC_a)
      if (rst_NVC_a)
          NMS_Pass_r <= #1 1'b0;
      else
          NMS_Pass_r <= #1 Oper_Pass_r & ~Illeg_Action_r;

//--------------------    Keep_On_r    -----------
  always @(posedge clk_NVC or posedge rst_NVC_a)
      if (rst_NVC_a)
          Keep_On_r <= #1 1'b0;
      else
          Keep_On_r <= #1 (CurrentState == `ST_KEEP_ON);

//--------------------    Oper_Pass_r    -----------
  always @(posedge clk_NVC or posedge rst_NVC_a)
      if (rst_NVC_a)
          Oper_Pass_r <= #1 1'b0;
      else if (Reset_OperPass)
              Oper_Pass_r <= #1 1'b0;
          else if (Set_OperPass)
                      Oper_Pass_r <= #1 1'b1;
              else  Oper_Pass_r <= #1 Oper_Pass_r;

//--------------------    Illeg_Action_r    -----------
  always @(posedge clk_NVC or posedge rst_NVC_a)
      if (rst_NVC_a)
          Illeg_Action_r <= #1 1'b1;
      else if (Instr_En_Pedge & ((CurrentState == `ST_READY)
              | ((CurrentState == `ST_KEEP_ON) && RCREADY_met && Keep_End)))
              Illeg_Action_r <= #1 1'b0;
          else if (Instr_En_Pedge | Instr_Chng_pip_r)
                  Illeg_Action_r <= #1 1'b1;
              else  Illeg_Action_r <= #1 Illeg_Action_r;

//--------------------    Instr_En_pip_r    -----------
  always @(posedge clk_NVC)
      Instr_En_pip_r <= #1 Instr_En;

//--------------------    Instr_Chng_pip_r    -----------
  always @(posedge clk_NVC)
      Instr_Chng_pip_r <= #1 Instr_Chng;

//--------------------    NVSRAM_Indef_r    -----------
  always @(posedge clk_NVC or posedge rst_NVC_a)
      if (rst_NVC_a)
          NVSRAM_Indef_r <= #1 1'b1;
      else if (Serial)
              NVSRAM_Indef_r <= #1 1'b1;
          else if (Serial_Loop | RECALL | STORE)
                  NVSRAM_Indef_r <= #1 1'b0;
              else  // AutoShift, etc.
                  NVSRAM_Indef_r <= #1 NVSRAM_Indef_r;

//---------------------    Unlock_Seq_r    --------
  always @(posedge clk_NVC)
    if (CurrentState == `ST_RESET_INIT)
        Unlock_Seq_r <= #1 8'h96;
    else if (CurrentState == `ST_UNLOCKING)
            Unlock_Seq_r <= #1 {Unlock_Seq_r[6:0], 1'b0};
        else    Unlock_Seq_r <= #1 Unlock_Seq_r;

//---------------------    CmpCounter_r    --------
  always @(posedge clk_NVC or posedge rst_NVC_a)
    if (rst_NVC_a)
        CmpCounter_r <= #1 3'b0;
    else if ((CurrentState == `ST_READY) || (CurrentState == `ST_STOR_S0))
            CmpCounter_r <= #1 3'b0;
        else if ((CurrentState == `ST_COMP_RECOV) && Time_Comp_Recov)
                CmpCounter_r <= #1 CmpCounter_r + 3'b1;
            else    CmpCounter_r <= #1 CmpCounter_r;

//---------------------    VMaxPulse_Cntr_r    --------
  always @(posedge clk_NVC)
    if (CurrentState == `ST_RESET_INIT)
        VMaxPulse_Cntr_r <= #1 8'b0;
    else if ((CurrentState == `ST_STOR_S0) & Time_Stor_Recov & VPP_Eq_VPPMax)
            VMaxPulse_Cntr_r <= #1 VMaxPulse_Cntr_r + 8'b1;
        else  VMaxPulse_Cntr_r <= #1 VMaxPulse_Cntr_r;

//---------------------    DumbPulse_Cntr_r    --------
  always @(posedge clk_NVC)
    if (CurrentState == `ST_RESET_INIT)
        DumbPulse_Cntr_r <= #1 3'b0;
    else if ((CurrentState == `ST_STOR_S0) & Time_Stor_Recov & Oper_Pass_r) // Previous Compare Passed
            DumbPulse_Cntr_r <= #1 DumbPulse_Cntr_r + 3'b1;
        else  DumbPulse_Cntr_r <= #1 DumbPulse_Cntr_r;

//---------------------    Unlock_r    --------
  always @(posedge clk_NVC)
      Unlock_r <= #1 UNLOCK;

//--------------------    STORE_r    -----------
  always @(posedge clk_NVC or posedge rst_NVC_a)
      if (rst_NVC_a)
          STORE_r <= #1 1'b0;
      else    STORE_r <= #1 ((CurrentState == `ST_STOR_S1)
                   || (CurrentState == `ST_STOR_P1) || (CurrentState == `ST_STOR_P0));

//--------------------    COMP_r    -----------
  always @(posedge clk_NVC or posedge rst_NVC_a)
      if (rst_NVC_a)
          COMP_r <= #1 1'b0;
      else    COMP_r <= #1 (CurrentState == `ST_COMPARING);

//--------------------    RECALL_r    -----------
  always @(posedge clk_NVC or posedge rst_NVC_a)
      if (rst_NVC_a)
          RECALL_r <= #1 1'b0;
      else    RECALL_r <= #1 ((CurrentState == `ST_RECALLING)
                   || (CurrentState == `ST_KEEP_ON));

//---------------------    RST_i_r    --------
  always @(posedge clk_NVC or posedge rst_NVC_a)
      if (rst_NVC_a)
          RST_i_r <= #1 1'b1;
      else  RST_i_r <= #1 ((CurrentState == `ST_RESETTING) || (CurrentState == `ST_RESET_INIT));

//---------------------    PE_r    ----------
  always @(posedge clk_NVC or posedge rst_NVC_a)
      if (rst_NVC_a)
          PE_r <= #1 1'b0;
      else  PE_r <= #1 (CurrentState == `ST_STOR_P1);

//---------------------    VPPSEL_r, VPPRANGE_r    --------
  always @(posedge clk_NVC or posedge rst_NVC_a)
    if (rst_NVC_a)
        begin
          VPPSEL_r   <= #1 4'b0;
          VPPRANGE_r <= #1 1'b0;
        end
    else
        case (CurrentState)

          `ST_UNLOCKING:
            begin
              VPPSEL_r   <= #1 VPP_Level;
              VPPRANGE_r <= #1 (FullStore | Recall_AM) ? 1'b0 : 1'b1;
            end

          `ST_STOR_INC_VPP:
            begin
              VPPSEL_r   <= #1 (VPPSEL_Inc < /*<=*/VPPMax_Decoded) ? VPPSEL_Inc[3:0] : VPPMax_Decoded;
              VPPRANGE_r <= #1 1'b0;
            end

          default:
            begin
              VPPSEL_r   <= #1 VPPSEL_r;
              VPPRANGE_r <= #1 VPPRANGE_r;
            end

        endcase

//---------------------    Compare Control Signals    --------
  always @(posedge clk_NVC or posedge rst_NVC_a)
    if (rst_NVC_a)
        {BIAS1_r, BIAS0_r, MRCL1_r, MRCL0_r, TECC1_r, TECC0_r} <= #1 `TESTCFG_DEF;
    else
        case (CurrentState)
          `ST_RESET_INIT:
              {BIAS1_r, BIAS0_r, MRCL1_r, MRCL0_r, TECC1_r, TECC0_r} <= #1 `TESTCFG_DEF;

          `ST_RECALL_INIT:
              if (MRecall)
                  {BIAS1_r, BIAS0_r, MRCL1_r, MRCL0_r, TECC1_r, TECC0_r} <= #1 TestCfg_1;
              else    // NRecall | Recall_AM
                  {BIAS1_r, BIAS0_r, MRCL1_r, MRCL0_r, TECC1_r, TECC0_r} <= #1 `TESTCFG_DEF;

          `ST_KEEP_INIT:
              {BIAS1_r, BIAS0_r, MRCL1_r, MRCL0_r, TECC1_r, TECC0_r} <= #1 `TESTCFG_DEF;

          `ST_COMP_INIT:
              if (MCompare | FullStore | Recall_AM)
                case (CmpCounter_r)
                  3'd0 :  {BIAS1_r, BIAS0_r, MRCL1_r, MRCL0_r, TECC1_r, TECC0_r} <= #1 TestCfg_1;

                  3'd1 :  {BIAS1_r, BIAS0_r, MRCL1_r, MRCL0_r, TECC1_r, TECC0_r} <= #1 TestCfg_2;

                  3'd2 :  {BIAS1_r, BIAS0_r, MRCL1_r, MRCL0_r, TECC1_r, TECC0_r} <= #1 TestCfg_3;

                  3'd3 :  {BIAS1_r, BIAS0_r, MRCL1_r, MRCL0_r, TECC1_r, TECC0_r} <= #1 TestCfg_4;

                  default: // 3'd4
                          {BIAS1_r, BIAS0_r, MRCL1_r, MRCL0_r, TECC1_r, TECC0_r} <= #1 TestCfg_5;
                endcase

              else    // (NCompare)
                  {BIAS1_r, BIAS0_r, MRCL1_r, MRCL0_r, TECC1_r, TECC0_r} <= #1 `TESTCFG_DEF;

          default:
              {BIAS1_r, BIAS0_r, MRCL1_r, MRCL0_r, TECC1_r, TECC0_r} <= #1
                         {BIAS1_r, BIAS0_r, MRCL1_r, MRCL0_r, TECC1_r, TECC0_r};
        endcase

//---------------------    AntiMetastability    ----------
  always @(posedge clk_NVC)
      ShCounter_End_met_r <= #1 {ShCounter_End_met_r[0], ShCounter_End};

//-----------------------------

endmodule  // ---   NOVEA_Controller




// ===================         Serial_Controller     ==============
module Serial_Controller(
      WRCK,
      rst_WIR_a,
      rst_ShCounter,
      SO_NV,
      SerEn_NVC,
      SerIn_NVC,
      Serial,
      Serial_Loop,
      AutoShift_En,

      SME_NV,
      SI_NV,
      SerOut_NVC,
      ShCounter_End
      );

// -------------        Port List      ----------------
 input WRCK;                  // NOVeA Serial Clock
 input rst_WIR_a;             // Asynchronous Reset
 input rst_ShCounter;         // Shift Counter Reset
 input SO_NV;                 // from Serial Output of NOVeA
 input SerEn_NVC;             // Serial Enable of Serial_Controller
 input SerIn_NVC;             // Serial Input of Serial_Controller
 input Serial;                // Serial Mode
 input Serial_Loop;           // Serial Loop Mode
 input AutoShift_En;          // AutoShift Enable

 output SME_NV;               // to Serial Memory Enable of NOVeA
 output SI_NV;                // to Serial Input of NOVeA
 output SerOut_NVC;           // Serial Output of Serial_Controller
 output ShCounter_End;        // Shift Counter reaches the End

//-------------------       Registers      ----------
 reg [11:0] ShCounter_r;       // Shift Counter used in AutoShift Mode
 reg  ShCounter_End_r;        // Shift Counter reaches the End

 reg [1:0] rst_ShCounter_met_r; // Antimetastability Double FlipFlops
 reg [1:0] AutoShift_En_met_r;  // Antimetastability Double FlipFlops

//-------------------       Wires      ----------
 wire SME_NV;                 // Output
 wire SI_NV;                  // Output
 wire SerOut_NVC;             // Output
 wire ShCounter_End;          // Output

 wire ShCounter_End_nxt;      // Next Value for ShCounter_End

 wire rst_ShCounter_met;      // rst_ShCounter Double FlipFloped
 wire AutoShift_En_met;       // AutoShift_En  Double FlipFloped

//--------------------    Continuous Assignments    ---------
  assign SI_NV = (Serial) ? SerIn_NVC : SO_NV;
  assign #2 SME_NV = (AutoShift_En_met & ~ShCounter_End) || (SerEn_NVC & (Serial | Serial_Loop));
  assign SerOut_NVC = SO_NV;

  assign ShCounter_End_nxt = (SME_NV) ? (ShCounter_r == (32 * 128 - 1)) : ShCounter_End_r;
  assign ShCounter_End = ShCounter_End_r;
  assign rst_ShCounter_met = rst_ShCounter_met_r[1];
  assign AutoShift_En_met = AutoShift_En_met_r[1];


//--------------------    AntiMetastability    ---------
  always @(posedge WRCK)
      rst_ShCounter_met_r <= #1 {rst_ShCounter_met_r[0], rst_ShCounter};

  always @(posedge WRCK or posedge rst_WIR_a)
      if (rst_WIR_a)
          AutoShift_En_met_r  <= #1 2'b0;
      else
          AutoShift_En_met_r  <= #1 {AutoShift_En_met_r[0], AutoShift_En};

//--------------------    ShCounter_r    ---------    Shift Counter
  always @(posedge WRCK or posedge rst_WIR_a)
      if (rst_WIR_a)
          ShCounter_r <= #1 12'b0;
      else if (rst_ShCounter_met | ShCounter_End_nxt | (Serial & SME_NV))
              ShCounter_r <= #1 12'b0;
          else if (SME_NV)
                  ShCounter_r <= #1 ShCounter_r + 1;
              else
                  ShCounter_r <= #1 ShCounter_r;

//--------------------    ShCounter_End_r    ---------
  always @(posedge WRCK)
    if (rst_ShCounter_met)
        ShCounter_End_r <= #1 1'b1;
    else
        ShCounter_End_r <= #1 ShCounter_End_nxt;


endmodule    // ---    Serial_Controller




//=====================         interface          ===================

module interface(
      clk_NVC,
      WRCK,
      WRSTN,
      WSI,
      UpdateWR,
      ShiftWR,
      SelectWIR,
      PORST,
      MATCH,
      RCREADY,
      TIME_B,
      PAE_p,
      PA_p,
      KEEP_ON,
      NMS_READY,
      SerOut_NVC,
      Addr_CR,
      D_CR,
      RA_p,
      WE_p,
      ME_p,
      OE_p,

      WSO,
      SerIn_NVC,
      SerEn_NVC,
      Instr_En,
      Instr_Chng,
      Bypass,
      FullStore,
      SoftStore,
      MCompare,
      NCompare,
      MRecall,
      NRecall,
      Serial,
      Serial_Loop,
      AutoShift,
      Keep_Start,
      Keep_End,
      Recall_AM,
      Reset_Cmd,
      rst_NVC_a,
      rst_WIR_a,
      MATCH_met,
      RCREADY_met,
      TIME_B_Pedge,
      TestCfg_1,
      TestCfg_2,
      TestCfg_3,
      TestCfg_4,
      TestCfg_5,
      N_Dumb,
      Delta_VPP,
      VPP_Max,
      VPP_Level,
      N_Max,
      P_Width,
      Q_CR,
      OE_CR,
      WE_NV,
      OE_NV,
      ME_NV
      );

// -------------------      Port List       ---------------------------
 input clk_NVC;               // Clock of NOVeA Controller
 input WRCK;                  // P1500 Clock
 input WRSTN;                 // P1500 Asynchronous Negative Reset
 input WSI;                   // P1500 Wrapper Serial Input
 input UpdateWR;              // P1500 Update signal
 input ShiftWR;               // P1500 Shift Enable
 input SelectWIR;             // P1500 signal, selects WIR or Data Register
 input PORST;                 // Power On ReSeT from Charge Pump
 input MATCH;                 // "MATCH" from NOVeA
 input RCREADY;               // "Recall or Compare Ready" from NOVeA
 input TIME_B;                // Time Base
 input PAE_p;                 // Controller Enable in Parallel Mode
 input [2:0] PA_p;            // Instruction in Parallel
 input KEEP_ON;               // Indicates that NOVeA Controller is in Keep Mode
 input NMS_READY;             // Indicates that Controller doesn't perform any operation
 input SerOut_NVC;            // NOVeA Controller Serial Output
 input [2:0] Addr_CR;         // Address of Configuration Register
 input [7:0] D_CR;            // Data to Configuration Register
 input RA_p;                  // Register Access in Parallel
 input WE_p;                  // Write Enable of NMS
 input ME_p;                  // Memory Enable of NMS
 input OE_p;                  // Output Enable of NMS

 output WSO;                  // P1500 Wrapper Serial Output
 output SerIn_NVC;            // Serial Input of NOVeA Controller
 output SerEn_NVC;            // Serial Shift Enable of NOVeA Controller

 output Instr_En;             // Instruction Enable (to NOVeA Controller)
 output Instr_Chng;           // Instruction will be Changed (to NOVeA Controller)
 output Bypass;               // Instruction (to NOVeA Controller)
 output FullStore;            // Instruction (to NOVeA Controller)
 output SoftStore;            // Instruction (to NOVeA Controller)
 output MCompare;             // Instruction (to NOVeA Controller)
 output NCompare;             // Instruction (to NOVeA Controller)
 output MRecall;              // Instruction (to NOVeA Controller)
 output NRecall;              // Instruction (to NOVeA Controller)
 output Serial;               // Instruction (to NOVeA Controller)
 output Serial_Loop;          // Instruction (to NOVeA Controller)
 output AutoShift;            // Instruction (to NOVeA Controller)
 output Keep_Start;           // Instruction (to NOVeA Controller)
 output Keep_End;             // Instruction (to NOVeA Controller)
 output Recall_AM;            // Instruction (to NOVeA Controller)
 output Reset_Cmd;            // Instruction (to NOVeA Controller)
 output rst_NVC_a;            // Summed Reset to NOVeA Controller (Asynchronous, Positive)
 output rst_WIR_a;            // Summed WIR Reset (to Serial_Controller) (Asynchronous, Positive)

 output MATCH_met;            // Double FlipFloped MATCH (to NOVeA Controller)
 output RCREADY_met;          // Double FlipFloped RCREADY (to NOVeA Controller)
 output TIME_B_Pedge;         // Posedge of TIME_B (to NOVeA Controller)

 output [5:0] TestCfg_1;      // Margin Test 1 Configuration
 output [5:0] TestCfg_2;      // Margin Test 2 Configuration
 output [5:0] TestCfg_3;      // Margin Test 3 Configuration
 output [5:0] TestCfg_4;      // Margin Test 4 Configuration
 output [5:0] TestCfg_5;      // Margin Test 5 Configuration
 output [2:0] N_Dumb;         // Number of Dumb Store Pulses
 output [1:0] Delta_VPP;      // Determines VPP Increment Value
 output [1:0] VPP_Max;        // Determines Max Value of VPP
 output [3:0] VPP_Level;      // Initial Level of VPP
 output [1:0] N_Max;          // Max Number of Store Pulses at VPP_Max
 output [1:0] P_Width;        // Store Pulse Width

 output [7:0] Q_CR;           // Output of Configuration Registers
 output OE_CR;                // Output Enable of Configuration Registers
 output WE_NV;                // Write Enable of NOVeA
 output OE_NV;                // Output Enable of NOVeA
 output ME_NV;                // Memory Enable of NOVeA


// ---------------      Registers         -----------------------
 reg [50:0] ScanWIR_r;        // P1500 Wrapper Instruction Register Scannable Part
 reg bypass_r;                // P1500 Bypass Register
 reg UpdateWIR_r;             // WIR was Updated
 reg ScanWIRout_neg_r;        // Reg after ScanWIR , @(negedge WRCK)
 reg DR_out_neg_r;            // Reg after Data Register , @(negedge WRCK)
 reg [5:0] Instr_r;           // Instruction from WIR or from Parallel Pins
 reg Instr_En_r;              // Instruction Enable
 reg OE_CR_pip_r;             // OE_CR delayed by one clock
 reg OE_NV_pip_r;             // OE_NV delayed by one clock

 reg [7:0] CRSTO_0_r;         // Store related Configuration Register
 reg [6:0] CRSTO_1_r;         // Store related Configuration Register
 reg [7:0] CRM_0_r;           // Margin related Configuration Register
 reg [7:0] CRM_1_r;           // Margin related Configuration Register
 reg [7:0] CRM_2_r;           // Margin related Configuration Register
 reg [5:0] CRM_3_r;           // Margin related Configuration Register
 reg [7:0] Q_CR;              // Output of Configuration Registers (Mux)

 reg [1:0] PAE_p_met_r;       // AntiMetastability Double FlipFlops
 reg [2:0] Instr_p_met0_r;    // AntiMetastability Double FlipFlops
 reg [1:0] PORST_WRCK_met_r;  // AntiMetastability Double FlipFlops, @ WRCK
 reg [1:0] WRST_PORST_met_r;  // AntiMetastability Double FlipFlops
 reg [1:0] MATCH_met_r;       // AntiMetastability Double FlipFlops
 reg [2:0] RCREADY_met_r;     // AntiMetastability Double FlipFlops
 reg [2:0] TIME_B_met_r;      // AntiMetastability Double FlipFlops
 reg [1:0] UpdateWIR_met_r;   // AntiMetastability Double FlipFlops

// ---------------      Wires         -----------------------
 wire rst_NVC_a;              // Output
 wire rst_WIR_a;              // WIR Asynchronous Reset (Positive)
 wire [5:0] Instr;            // Instruction from WIR or from Parallel Pins
 wire ScanWIR_out;            // Output of Scannable part of WIR (MSB)
 wire NVC_SerIO_En;           // Indicates that WSI and WSO must be connected to NOVeA Controller
 wire Instr_En_nxt;           // Next Value for Instr_En_r
 wire WE_CR;                  // Write Enable of Configuration Registers

 wire WSO;                    // Output
 wire SerIn_NVC;              // Output
 wire SerEn_NVC;              // Output
 wire Instr_En;               // Output
 wire Instr_Chng;             // Output
 wire Bypass;                 // Output
 wire FullStore;              // Output
 wire SoftStore;              // Output
 wire MCompare;               // Output
 wire NCompare;               // Output
 wire MRecall;                // Output
 wire NRecall;                // Output
 wire Serial;                 // Output
 wire Serial_Loop;            // Output
 wire AutoShift;              // Output
 wire Keep_Start;             // Output
 wire Keep_End;               // Output
 wire Recall_AM;              // Output
 wire Reset_Cmd;              // Output

 wire [5:0] TestCfg_1;        // Output
 wire [5:0] TestCfg_2;        // Output
 wire [5:0] TestCfg_3;        // Output
 wire [5:0] TestCfg_4;        // Output
 wire [5:0] TestCfg_5;        // Output
 wire [2:0] N_Dumb;           // Output
 wire [1:0] Delta_VPP;        // Output
 wire [1:0] VPP_Max;          // Output
 wire [3:0] VPP_Level;        // Output
 wire [1:0] N_Max;            // Output
 wire [1:0] P_Width;          // Output

 wire OE_CR;                  // Output
 wire WE_NV;                  // Output
 wire OE_NV;                  // Output
 wire ME_NV;                  // Output

 wire PAE_p_met;              // Double FlipFloped
 wire [2:0] Instr_p_met;      // Double FlipFloped
 wire PORST_WRCK_met;         // Double FlipFloped
 wire WRST_PORST_met;         // Double FlipFloped
 wire MATCH_met;              // Double FlipFloped
 wire RCREADY_met;            // Double FlipFloped
 wire TIME_B_Pedge;           // TIME_B posedge after Double FlipFlop
 wire UpdateWIR_met;          // Double FlipFloped

//-----------     Continuous Assignments     ---------------
  assign Instr = Instr_r;
  assign NVC_SerIO_En = ((Instr == `I_SERIAL) || (Instr == `I_SER_LOOP));
  assign ScanWIR_out = ScanWIR_r[50];

  assign WSO = SelectWIR ? ScanWIRout_neg_r : DR_out_neg_r;
  assign SerIn_NVC = WSI;
  assign SerEn_NVC = (~SelectWIR & ShiftWR & NVC_SerIO_En);
  assign {TestCfg_5, TestCfg_4, TestCfg_3, TestCfg_2, TestCfg_1, N_Dumb,
          Delta_VPP, VPP_Max, VPP_Level, N_Max, P_Width} =
            {CRM_3_r, CRM_2_r, CRM_1_r, CRM_0_r, CRSTO_1_r, CRSTO_0_r};
  assign Instr_En_nxt = PAE_p_met | UpdateWIR_met;
  assign Instr_En = Instr_En_r & Instr_En_nxt;
  assign Instr_Chng = Instr_En_nxt & ~Instr_En_r;
  assign Bypass =       (Instr == `I_BYPASS)    ;
  assign FullStore =    (Instr == `I_FULL_STORE);
  assign SoftStore =    (Instr == `I_SOFT_STORE);
  assign MCompare =     (Instr == `I_MCOMPARE)  ;
  assign NCompare =     (Instr == `I_NCOMPARE)  ;
  assign MRecall =      (Instr == `I_MRECALL)   ;
  assign NRecall =      (Instr == `I_NRECALL)   ;
  assign Serial =       (Instr == `I_SERIAL)    ;
  assign Serial_Loop =  (Instr == `I_SER_LOOP)  ;
  assign AutoShift =    (Instr == `I_AUTO_SHIFT);
  assign Keep_Start =   (Instr == `I_KEEP)      ;
  assign Keep_End =     (Instr == `I_IDLE)      ;
  assign Recall_AM =    (Instr == `I_RECALLAM)  ;
  assign Reset_Cmd =   ((Instr == `I_RESET) & Instr_En);
  assign rst_NVC_a = (~WRSTN | PORST | WRST_PORST_met);
  assign rst_WIR_a = (~WRSTN | PORST | PORST_WRCK_met);

  assign PAE_p_met       = PAE_p_met_r[1];
  assign Instr_p_met     = Instr_p_met0_r;
  assign PORST_WRCK_met  = PORST_WRCK_met_r[1];
  assign WRST_PORST_met  = WRST_PORST_met_r[1];
  assign MATCH_met       = MATCH_met_r[1];
  assign RCREADY_met     = RCREADY_met_r[2];
  assign TIME_B_Pedge    = TIME_B_met_r[1] & ~TIME_B_met_r[2]; // posedge detector
  assign UpdateWIR_met   = UpdateWIR_met_r[1];

  assign WE_CR =  RA_p & ME_p & WE_p & NMS_READY;
  assign OE_CR =  RA_p & OE_p & ~OE_NV_pip_r;
  assign OE_NV = ~RA_p & OE_p & ~OE_CR_pip_r;
  assign WE_NV = ~RA_p & WE_p & ~KEEP_ON & NMS_READY;
  assign ME_NV = ~RA_p & ME_p;

//-----------------      Q_CR      -----------------
  always @(Addr_CR or CRSTO_0_r or CRSTO_1_r or CRM_0_r or CRM_1_r or CRM_2_r or CRM_3_r)
    case (Addr_CR)
      3'b000:   Q_CR = CRSTO_0_r;
      3'b001:   Q_CR = {1'b0, CRSTO_1_r};
      3'b010:   Q_CR = CRM_0_r;
      3'b011:   Q_CR = CRM_1_r;
      3'b100:   Q_CR = CRM_2_r;
      3'b101:   Q_CR = {2'b0, CRM_3_r};
      3'b110:   Q_CR = 8'b01001111;
      3'b111:   Q_CR = 8'b00000001;
    endcase

//-----------     AntiMetastability       ---------------
  always @(posedge clk_NVC)
      PAE_p_met_r       <= #1 {PAE_p_met_r[0], PAE_p};

  always @(posedge clk_NVC)
    begin
      Instr_p_met0_r    <= #1 PA_p;
    end

  always @(posedge WRCK)
      PORST_WRCK_met_r  <= #1 {PORST_WRCK_met_r[0], PORST};

  always @(posedge clk_NVC)
      WRST_PORST_met_r  <= #1 {WRST_PORST_met_r[0], (~WRSTN | PORST)};

  always @(posedge clk_NVC)
      MATCH_met_r       <= #1 {MATCH_met_r[0], MATCH};

  always @(posedge clk_NVC)
      RCREADY_met_r     <= #1 {RCREADY_met_r[1:0], RCREADY};

  always @(posedge clk_NVC)
      TIME_B_met_r      <= #1 {TIME_B_met_r[1:0], TIME_B};

  always @(posedge clk_NVC)
      UpdateWIR_met_r   <= #1 {UpdateWIR_met_r[0], UpdateWIR_r};

//-----------     ScanWIR_r       ---------------
  always @(posedge WRCK)
    if (SelectWIR & ShiftWR)
        ScanWIR_r <= #1 {ScanWIR_r[49:0], WSI};
    else ScanWIR_r <= #1 ScanWIR_r;

//----------     UpdateWIR_r       -----------------
  always @(negedge WRCK or posedge rst_WIR_a)
    if (rst_WIR_a)
          UpdateWIR_r <= #1 1'b0;
    else  UpdateWIR_r <= #1 (SelectWIR & UpdateWR);

//----------     Instr_r       -----------------
  always @(posedge clk_NVC or posedge rst_NVC_a)
    if (rst_NVC_a)
        Instr_r <= #1 `I_BYPASS;
    else if (PAE_p_met & ~Instr_En_r)
            Instr_r <= #1 {3'b0, Instr_p_met};
        else if (UpdateWIR_met & ~Instr_En_r)
                Instr_r <= #1 ScanWIR_r[50:45];
            else Instr_r <= #1 Instr_r;

//------    {CRM_3_r, CRM_2_r, CRM_1_r, CRM_0_r, CRSTO_1_r, CRSTO_0_r}    ------
  always @(posedge clk_NVC or posedge rst_NVC_a)
    if (rst_NVC_a)
        {CRM_3_r, CRM_2_r, CRM_1_r, CRM_0_r, CRSTO_1_r, CRSTO_0_r} <= #1 `FSTORE_DEF_CFG;
    else if (Reset_Cmd)
            {CRM_3_r, CRM_2_r, CRM_1_r, CRM_0_r, CRSTO_1_r, CRSTO_0_r} <= #1 `FSTORE_DEF_CFG;
        else if (WE_CR)
              case (Addr_CR)
                3'b000:  {CRM_3_r, CRM_2_r, CRM_1_r, CRM_0_r, CRSTO_1_r, CRSTO_0_r} <= #1
                         {CRM_3_r, CRM_2_r, CRM_1_r, CRM_0_r, CRSTO_1_r, D_CR     };

                3'b001:  {CRM_3_r, CRM_2_r, CRM_1_r, CRM_0_r, CRSTO_1_r, CRSTO_0_r} <= #1
                         {CRM_3_r, CRM_2_r, CRM_1_r, CRM_0_r, D_CR[6:0], CRSTO_0_r};

                3'b010:  {CRM_3_r, CRM_2_r, CRM_1_r, CRM_0_r, CRSTO_1_r, CRSTO_0_r} <= #1
                         {CRM_3_r, CRM_2_r, CRM_1_r, D_CR,    CRSTO_1_r, CRSTO_0_r};

                3'b011:  {CRM_3_r, CRM_2_r, CRM_1_r, CRM_0_r, CRSTO_1_r, CRSTO_0_r} <= #1
                         {CRM_3_r, CRM_2_r, D_CR,    CRM_0_r, CRSTO_1_r, CRSTO_0_r};

                3'b100:  {CRM_3_r, CRM_2_r, CRM_1_r, CRM_0_r, CRSTO_1_r, CRSTO_0_r} <= #1
                         {CRM_3_r, D_CR,    CRM_1_r, CRM_0_r, CRSTO_1_r, CRSTO_0_r};

                3'b101:  {CRM_3_r, CRM_2_r, CRM_1_r, CRM_0_r, CRSTO_1_r, CRSTO_0_r} <= #1
                       {D_CR[5:0], CRM_2_r, CRM_1_r, CRM_0_r, CRSTO_1_r, CRSTO_0_r};

                default: {CRM_3_r, CRM_2_r, CRM_1_r, CRM_0_r, CRSTO_1_r, CRSTO_0_r} <= #1
                         {CRM_3_r, CRM_2_r, CRM_1_r, CRM_0_r, CRSTO_1_r, CRSTO_0_r};

              endcase

            else if (UpdateWIR_met)
                    {CRM_3_r, CRM_2_r, CRM_1_r, CRM_0_r, CRSTO_1_r, CRSTO_0_r} <= #1
                    ScanWIR_r[44:0];
                else
                    {CRM_3_r, CRM_2_r, CRM_1_r, CRM_0_r, CRSTO_1_r, CRSTO_0_r} <= #1
                    {CRM_3_r, CRM_2_r, CRM_1_r, CRM_0_r, CRSTO_1_r, CRSTO_0_r};

//----------     Instr_En_r       -----------------
  always @(posedge clk_NVC or posedge rst_NVC_a)
    if (rst_NVC_a)
          Instr_En_r <= #1 1'b0;
    else  Instr_En_r <= #1 Instr_En_nxt;

//----------     bypass_r       ------------- P1500 Bypass Register
  always @(posedge WRCK)
    if (~SelectWIR & ShiftWR & ~NVC_SerIO_En)
          bypass_r <= #1 WSI;
    else  bypass_r <= #1 bypass_r;

//----------     ScanWIRout_neg_r       -----------------
  always @(negedge WRCK)
    ScanWIRout_neg_r <= #1 ScanWIR_out;

//----------     DR_out_neg_r       -----------------
  always @(negedge WRCK)
    DR_out_neg_r <= #1 (NVC_SerIO_En) ? SerOut_NVC : bypass_r;

//----------     OE_CR_pip_r       -----------------
  always @(posedge clk_NVC or posedge rst_NVC_a)
    if (rst_NVC_a)
          OE_CR_pip_r <= #1 1'b1;
    else  OE_CR_pip_r <= #1 OE_CR;

//----------     OE_NV_pip_r       -----------------
  always @(posedge clk_NVC or posedge rst_NVC_a)
    if (rst_NVC_a)
          OE_NV_pip_r <= #1 1'b1;
    else  OE_NV_pip_r <= #1 OE_NV;


endmodule  // ---   interface



//=====================         tri_Q          ===================
module tri_Q(
      Q_CR,
      OE_CR,

      Q
      );
 input [7:0] Q_CR;
 input OE_CR;

 output [31:0] Q;

  wire  [31:0] Q;

  //XXX Frank assign Q = (OE_CR) ? {24'b0, Q_CR} : {32 {1'bz}};
  assign Q = {24'b0, Q_CR};

endmodule  // ---   tri_Q


//=======================================================================
