/*---------------------------------------------------------------------*/
/*               Copyright(c) Virage Logic Corporation.                */
/*     All Rights reserved - Unpublished -rights reserved under        */
/*     the Copyright laws of the United States of America.             */
/*                                                                     */
/*  This file includes the Confidential information of Virage Logic    */
/*  and UMC                                                            */
/*  The receiver of this Confidential Information shall not disclose   */
/*  it to any third party and shall protect its confidentiality by     */
/*  using the same degree of care, but not less then a reasonable      */
/*  degree of care, as the receiver uses to protect receiver's own     */
/*  Confidential Information.                                          */
/*                                                                     */
/*                    Virage Logic Corporation.                        */
/*                    47100 Bayside Pkwy                               */
/*                    Fremont , California 94538.                      */
/*                                                                     */
/*---------------------------------------------------------------------*/
/*                                                                     */
/*  Software           : Rev: 3.4.1 (build REL-3-4-1-2003-04-26)       */
/*  Library Format     : Rev: 1.05.00                                  */
/*  Compiler Name      : um18ugssvpnnnvncn000sa03                      */
/*  Date of Generation : Fri Feb 20 15:51:30 PST 2004                  */
/*                                                                     */
/*---------------------------------------------------------------------*/

//=======================================================================
//=============     TestBench for NOVeA Memory System      ==============
//=======================================================================

/*
`include "nvrm_um18ugs_128x32.v"
`include "nvcp_um18gfs.v"
`include "nms128x32_ctr.v"
`include "nms128x32.v"
*/

`resetall

`timescale 1 ns / 10 ps

//  `define POST_SYNTH
//  `define GATE

  
  `define STORSC              2                     /*STORe Success Count*/
  `define SYS_CK_FREQ         100.0                   /*System Clock Frequency, MHz*/
  `define TIME_B_FREQ         1.0                     /*Time Base Frequency, MHz*/
  `define WRCK_FREQ           2.5                   /*Serial Clock Frequency, MHz*/
// -------------------------------------------------------------------------

// ----------------       Instructions       -----------------
  `define I_BYPASS           6'd0
  `define I_IDLE             `I_BYPASS
  `define I_SERIAL           6'd1
  `define I_FULL_STORE       6'd2
  `define I_NRECALL          6'd3
  `define I_NCOMPARE         6'd4
  `define I_KEEP             6'd5
  `define I_RECALLAM         6'd6

  `define I_SOFT_STORE       6'd8
  `define I_MRECALL          6'd9
  `define I_MCOMPARE         6'd10
  `define I_SER_LOOP         6'd11
  `define I_AUTO_SHIFT       6'd12
  `define I_RESET            6'd63

  `define FSTORE_DEF_CFG     45'b010101_010110_011001_011010_010000_001_00_10_0000_00_01

//  `define CFG_CODE           `FSTORE_DEF_CFG

// -------------------------------------------------------------------------
  
`ifdef POST_SYNTH
`else
  `define PRE_SYNTH
`endif
 

//   --------------------       nms128x32_tb          -------------------
module nms128x32_tb;


// ------------      Parameters --------------  
  parameter PAR_LOAD = 1'b1;
  parameter SER_LOAD = 1'b0;

//  -----------       Regs      --------------
  reg SYS_CK;
  reg TIME_B;
  reg WRCK;
  reg WRSTN ;
  reg WSI;
  reg UpdateWR;
  reg ShiftWR;
  reg SelectWIR;
  reg ME_p;
  reg WE_p;
  reg OE_p;
  reg RA_p;
  reg [31:0] D_p;
  reg [6:0] ADR_p;

  reg PAE_p ;
  reg [2:0] PA_p;

  reg [31:0] MemArray [127:0];
  reg [31:0] TempWord;
  reg WRCK_Enable;
  reg [44:0] ConfigCode;
  reg [44:0] ConfigCode_Array [3:0];
  reg WatchDog_Rst_Occured;
  reg Testing_WatchDog;
  reg Testing_Reset;
  reg InstrLoad_Mode;
  
  reg plus_max_cover_1;
  reg ExitOnFail;

  reg [16*8-1:0] TestName;
  integer TestNum;

  integer Fails;              // Total Unexpected Fail Count
  integer ExpectedFails;      // Total Expected Fail Count
  integer IncorrCells;
  integer StorePulse_Count;
  integer StorSC_Val;
  
  time Time_STORE_rise;
  time Time_PE_rise;
  time Time_VPP_rise;
  time Width_STORE;
  time Width_PE;
  time Width_VPP;

//  -----------       Wires      --------------
  wire [31:0] Q;

// --------         NOVeA Memory System Instantiation       -----
  nms128x32 U_NMS(
      .SYS_CK(SYS_CK),
      .TIME_B(TIME_B),
      .WRSTN(WRSTN),
      .WRCK(WRCK),
      .WSI(WSI),
      .UpdateWR(UpdateWR),
      .ShiftWR(ShiftWR),
      .SelectWIR(SelectWIR),
      .ADR_p0 (ADR_p[0]), .ADR_p1 (ADR_p[1]), .ADR_p2 (ADR_p[2]), .ADR_p3 (ADR_p[3]), .ADR_p4 (ADR_p[4]), .ADR_p5 (ADR_p[5]), .ADR_p6 (ADR_p[6]), 
      .D_p0 (D_p[0]), .D_p1 (D_p[1]), .D_p2 (D_p[2]), .D_p3 (D_p[3]), .D_p4 (D_p[4]), .D_p5 (D_p[5]), .D_p6 (D_p[6]), .D_p7 (D_p[7]), .D_p8 (D_p[8]), .D_p9 (D_p[9]), .D_p10 (D_p[10]), .D_p11 (D_p[11]), .D_p12 (D_p[12]), .D_p13 (D_p[13]), .D_p14 (D_p[14]), .D_p15 (D_p[15]), .D_p16 (D_p[16]), .D_p17 (D_p[17]), .D_p18 (D_p[18]), .D_p19 (D_p[19]), .D_p20 (D_p[20]), .D_p21 (D_p[21]), .D_p22 (D_p[22]), .D_p23 (D_p[23]), .D_p24 (D_p[24]), .D_p25 (D_p[25]), .D_p26 (D_p[26]), .D_p27 (D_p[27]), .D_p28 (D_p[28]), .D_p29 (D_p[29]), .D_p30 (D_p[30]), .D_p31 (D_p[31]), 
      .WE_p(WE_p),
      .ME_p(ME_p),
      .OE_p(OE_p),
      .PAE_p(PAE_p),
      .PA_p(PA_p),
      .RA_p(RA_p),

      .WSO(WSO),
      .Q0 (Q[0]), .Q1 (Q[1]), .Q2 (Q[2]), .Q3 (Q[3]), .Q4 (Q[4]), .Q5 (Q[5]), .Q6 (Q[6]), .Q7 (Q[7]), .Q8 (Q[8]), .Q9 (Q[9]), .Q10 (Q[10]), .Q11 (Q[11]), .Q12 (Q[12]), .Q13 (Q[13]), .Q14 (Q[14]), .Q15 (Q[15]), .Q16 (Q[16]), .Q17 (Q[17]), .Q18 (Q[18]), .Q19 (Q[19]), .Q20 (Q[20]), .Q21 (Q[21]), .Q22 (Q[22]), .Q23 (Q[23]), .Q24 (Q[24]), .Q25 (Q[25]), .Q26 (Q[26]), .Q27 (Q[27]), .Q28 (Q[28]), .Q29 (Q[29]), .Q30 (Q[30]), .Q31 (Q[31]), 
      .NMS_READY(NMS_READY),
      .NMS_PASS(NMS_PASS),
      .KEEP_ON(KEEP_ON)
  );



// ------------------    Clock Generators    ----------------
  initial
    begin
      #2 SYS_CK = 0;
      forever #(500.0 / `SYS_CK_FREQ) SYS_CK = ~SYS_CK;
    end

  initial
    begin
      #4 WRCK = 0;
      forever #(500.0 / `WRCK_FREQ) if (WRCK_Enable) WRCK = ~WRCK;
    end

  initial
    begin
      TIME_B = 0;
`ifdef VIRAGE_FAST
      forever #(500.0 / `TIME_B_FREQ / ((`TIME_B_FREQ * 20 < `SYS_CK_FREQ / 2.0) ? 20.0 : (`SYS_CK_FREQ / `TIME_B_FREQ / 2.0)))
                  TIME_B = ~TIME_B;
`else
      forever #(500.0 / `TIME_B_FREQ) TIME_B = ~TIME_B;
`endif
    end

// ------------------    Main initial    ----------------
  initial
    begin
`ifdef GATE
      $sdf_annotate("nms128x32_nms100.sdf", U_NMS);
`endif
      Fails = 0;
      ExpectedFails = 0;
      TestNum = 0;
      WatchDog_Rst_Occured = 0;
      Testing_WatchDog = 0;
      Testing_Reset = 0;
      plus_max_cover_1 = 0;
      ExitOnFail = 0;
      InstrLoad_Mode = SER_LOAD;
      $readmemb("nvrm_um18ugs_128x32_contents.dat", MemArray, 0, 127);
`ifdef CFG_CODE
      ConfigCode = `CFG_CODE;
`else
      $readmemb("nms128x32_ConfigCode.dat", ConfigCode_Array, 0, 3);
      ConfigCode = ConfigCode_Array[0];
`endif
      U_NMS.U_NOVEA.uut.store_success_count <= #1 `STORSC;
      WRCK_Enable = 1;

// ------------------    Parsing  plusargs    ----------------
      if ($test$plusargs("help"))
        begin
          $display("  -- I N F O :    ---  plusarg  +help  detected  ---\n");
          Help;
          $finish;
        end

      $display("  -- I N F O :    ====#=#=#=#=#        TESTING  NOVEA MEMORY SYSTEM         #=#=#=#=#====\n");

      if ($test$plusargs("wrck_off"))
        begin
          $display("  -- I N F O :    ---  plusarg  +wrck_off  detected  ---  Turning WRCK Off\n");
          WRCK_Enable = 0;
        end

      if ($test$plusargs("load_par"))
        begin
          $display("  -- I N F O :    ---  plusarg  +load_par  detected  --- Parallel Loading of Instructions\n");
          InstrLoad_Mode = PAR_LOAD;
        end

      if ($test$plusargs("conf_1"))
        begin
          $display("  -- I N F O :    ---  plusarg  +conf_1  detected  --- Using line 1 of ConfigCode file\n");
          ConfigCode = ConfigCode_Array[0];
        end

      if ($test$plusargs("conf_2"))
        begin
          $display("  -- I N F O :    ---  plusarg  +conf_2  detected  --- Using line 2 of ConfigCode file\n");
          ConfigCode = ConfigCode_Array[1];
        end

      if ($test$plusargs("conf_3"))
        begin
          $display("  -- I N F O :    ---  plusarg  +conf_3  detected  --- Using line 3 of ConfigCode file\n");
          ConfigCode = ConfigCode_Array[2];
        end

      if ($test$plusargs("conf_4"))
        begin
          $display("  -- I N F O :    ---  plusarg  +conf_4  detected  --- Using line 4 of ConfigCode file\n");
          ConfigCode = ConfigCode_Array[3];
        end

      if ($test$plusargs("storsc="))
        begin
          StorSC_Val = Get_PlusargVal("storsc=", 3);
          if (StorSC_Val === 1'bx)
              $display("<<>> F A I L :    ---  Incorrect Format of  +storsc=NNN  plusarg  --- \n");
          else
            begin
              $display("  -- I N F O :    ---  plusarg  +storsc=%0d  detected  --- \n", StorSC_Val);
              U_NMS.U_NOVEA.uut.store_success_count <= #1 StorSC_Val;
            end
        end

      if ($test$plusargs("exit_on_fail"))
        begin
          $display("  -- I N F O :    ---  plusarg  +exit_on_fail  detected  --- Simulation will stop if Failed\n");
          ExitOnFail = 1;
        end

// ------------------ 
      if ($test$plusargs("fullstore"))
        begin
          $display("  -- I N F O :    ---  plusarg  +fullstore  detected  ---\n");
          Run_FullStore;
        end
      else if ($test$plusargs("run_all")) // testset_1
        begin
          $display("  -- I N F O :    ---  plusarg  +run_all  detected  ---\n");
          Run_TestSet_1;
        end
      else if ($test$plusargs("max_cover_1"))
        begin
          $display("  -- I N F O :    ---  plusarg  +max_cover_1  detected  ---\n");
          plus_max_cover_1 = 1;
          Run_Test_MaxCover;
        end
      else if ($test$plusargs("max_cover"))
        begin
          $display("  -- I N F O :    ---  plusarg  +max_cover  detected  ---\n");
          Run_Test_MaxCover;
        end
      else if ($test$plusargs("test_watchdog"))
        begin
          $display("  -- I N F O :    ---  plusarg  +test_watchdog  detected  ---\n");
          WatchDog_Test;
        end
      else
          Run_TestSet_1;
          
// ------------------    End of  Parsing  plusargs    ----------------

      if (Fails === 0)
        begin
          if (ExpectedFails === 0)
            $display("\n  >> P A S S :    ===========     ALL TESTS PASSED SUCCESSFULLY      ===========\n");
          else
            $display("\n  >> P A S S :    ======     TESTS PASSED SUCCESSFULLY (%0d Failed as Expected)    =======\n", ExpectedFails);
        end
      else 
            $display("\n<<>> F A I L :    ======     TESTS FAILED : %0d Unexpected , %0d as Expected     ===========\n", Fails,  ExpectedFails);

      #1000;
      $finish;

    end  //    -------       Main initial
    

// ==========================       Tasks         ==============================

// ------------------    NMS_Reset    ----------------
 task NMS_Reset;    // Sets Default Values on the NMS inputs and Resets NMS
  begin
      TestName = "NMS_Reset";
      TestNum = TestNum + 1;

      ME_p   = 1'b0;
      WE_p   = 1'b0;
      OE_p   = 1'b0;
      RA_p   = 1'b0;
      PAE_p  = 1'b0;

      UpdateWR = 0;
      ShiftWR = 0;
      SelectWIR = 0;

      WRSTN  = 1'b0;
      #0 $display("  -- I N F O :   *****     Resetting NOVeA Memory System ...     *****");

      if (WRCK_Enable)
        begin
          @(posedge WRCK);
          @(negedge WRCK);
        end
      else  #100;
      WRSTN = #1 1'b1;

      if (NMS_READY !== 0 || NMS_PASS !== 0)
        begin
              $display("\n<<>> F A I L :    NMS_READY or NMS_PASS are not 0 during Reset:    NMS_READY = %b  , NMS_PASS = %b", NMS_READY, NMS_PASS);
              Fails = Fails + 1;
        end

      $display("  -- I N F O :     Waiting for NMS_READY ...\n");
      wait(NMS_READY);

      Display_Status;

      TestName = 1'bx;

  end
 endtask // ---   NMS_Reset



// ------------------    Load_WIR    ----------------
 task Load_WIR;    // Loads WIR with given instruction
    input [50:0] Instr;
    parameter WAIT_READY = 0;
    integer i;
  begin
    if ((Instr ^ Instr) !== 0)
      begin
        $display("<<>> F A I L :     Instruction to be Loaded contains X  !!! Possibly incorrect Format of ConfigCode file.");
        Fails = Fails + 1;
      end
      
    if (NMS_READY !== 1)
      begin
        if (WAIT_READY)
          begin
            $display("  -- I N F O :     Waiting for NMS_READY before Loading WIR ...");
            wait(NMS_READY);
          end
        else
            $display("  -- I N F O :     Warning:   NMS_READY is not 1 during Load WIR : NMS_READY = %b   !!!!", NMS_READY);
      end

    if (WRCK_Enable)
      begin
       $display("\n  -- I N F O :    Loading WIR:  %b .\n", Instr);

        @(negedge WRCK);
        SelectWIR = 1;
        ShiftWR = 1;

        for (i = 50; i >= 0; i = i - 1)
          begin
            WSI = Instr[i];
            @(negedge WRCK);
          end

        ShiftWR = 0;

        @(posedge WRCK);
        UpdateWR = 1;

        @(posedge WRCK);
        UpdateWR = 0;
        SelectWIR = 0;
      end
    else
      begin
        $display("\n<<>> F A I L :    Cannot Load WIR  because WRCK is OFF   !!!");
        Fails = Fails + 1;
        $finish;
      end
  end
 endtask   // ---   Load_WIR


// ------------------    Load_Instr_Par    ----------------
 task Load_Instr_Par;    // Loads Given Instruction via Parallel pins
    input [5:0] Instr;
    parameter WAIT_READY = 1;
    parameter PAE_WIDTH = 2;

    reg [2:0] Instr_Par;

  begin
      if (NMS_READY !== 1)
        begin
          if (WAIT_READY)
            begin
              $display("  -- I N F O :     Waiting for NMS_READY ...");
              wait(NMS_READY);
            end
          else
              $display("  -- I N F O :     Warning:   NMS_READY is not 1 during Parallel Instruction Loading : NMS_READY = %b !!!", NMS_READY);
        end

      Instr_Par = Instr;
      if (Instr === Instr_Par)
          $display("\n  -- I N F O :    Loading Parallel Instruction:  %b\n", Instr_Par);
      else
        begin
          $display("\n<<>> F A I L :    This Instruction cannot be Loaded in Parallel way:  %b\n", Instr);
          Fails = Fails + 1;
        end

      @(negedge SYS_CK);
      PA_p = Instr_Par;
      PAE_p = 1;

      repeat (PAE_WIDTH) @(negedge SYS_CK);
      PA_p = 0;
      PAE_p = 0;

  end
 endtask   // ---   Load_Instr_Par



// ------------------    Load_Config_Par    ----------------
 task Load_Config_Par;    // Loads Given Configuration via Parallel bus
    input [7:0] CRSTO_0;
    input [6:0] CRSTO_1;
    input [7:0] CRM_0;
    input [7:0] CRM_1;
    input [7:0] CRM_2;
    input [5:0] CRM_3;
  begin
      $display("  -- I N F O :    Loading Configuration:  CRSTO_0 = %b, CRSTO_1 = %b, CRM_0 = %b, CRM_1 = %b, CRM_2 = %b, CRM_3 = %b.\n",
                CRSTO_0, CRSTO_1, CRM_0, CRM_1, CRM_2, CRM_3);

      if (((CRSTO_0 ^ CRSTO_1 ^ CRM_0 ^ CRM_1 ^ CRM_2 ^ CRM_3) ^
           (CRSTO_0 ^ CRSTO_1 ^ CRM_0 ^ CRM_1 ^ CRM_2 ^ CRM_3)) !== 0)
        begin
          $display("<<>> F A I L :     Configuration to be Loaded contains X  !!! Possibly incorrect Format of ConfigCode file.");
          Fails = Fails + 1;
        end
      
      @(negedge SYS_CK);
      RA_p = 1;
      ME_p = 1;
      WE_p = 1;

      ADR_p = 0;
      D_p = CRSTO_0;

      @(negedge SYS_CK);
      ADR_p = 1;
      D_p = CRSTO_1;

      @(negedge SYS_CK);
      ADR_p = 2;
      D_p = CRM_0;

      @(negedge SYS_CK);
      ADR_p = 3;
      D_p = CRM_1;

      @(negedge SYS_CK);
      ADR_p = 4;
      D_p = CRM_2;

      @(negedge SYS_CK);
      ADR_p = 5;
      D_p = CRM_3;

      @(negedge SYS_CK);
      RA_p = 0;
      ME_p = 0;
      WE_p = 0;

  end
 endtask   // ---   Load_Config_Par



// ------------------    Load_SRAM    ----------------
 task Load_SRAM;    // Loads NOVeA SRAM from the file
    integer i;
  begin
      $display("  -- I N F O :    ----    Loading NOVeA SRAM    ----");
      for (i = 0; i < 128; i = i + 1)
          U_NMS.U_NOVEA.uut.memb[i] = MemArray[i];
  end
 endtask   // ---   Load_SRAM



// ------------------    Load_EAROM    ----------------
 task Load_EAROM;    // Loads NOVeA EAROM from the file
    integer i;
  begin
      $display("  -- I N F O :    ----    Loading NOVeA EAROM    ----");
      for (i = 0; i < 128; i = i + 1)
          U_NMS.U_NOVEA.uut.earom[i] = MemArray[i];
  end
 endtask   // ---   Load_EAROM



// ------------------    Corrupt_SRAM    ----------------
 task Corrupt_SRAM;    // Corrupts data in NOVeA SRAM
  begin
      $display("  -- I N F O :    ----    Corrupting NOVeA SRAM Data for Test purposes  ----");
      U_NMS.U_NOVEA.uut.memb[0] = 1'bx;
  end
 endtask   // ---   Corrupt_SRAM



// ------------------    Corrupt_EAROM    ----------------
 task Corrupt_EAROM;    // Corrupts data in NOVeA EAROM
  begin
      $display("  -- I N F O :    ----    Corrupting NOVeA EAROM Data for Test purposes  ----");
      U_NMS.U_NOVEA.uut.earom[0] = 1'bx;
  end
 endtask   // ---   Corrupt_EAROM



// ------------------    Comp_SRAM_EAROM    ----------------
 task Comp_SRAM_EAROM;    // Compares NOVeA SRAM with NOVeA EAROM
   output [31:0] IncorrCells;    // Quantity of Mismatched Words
   integer i;
  begin
      IncorrCells = 0;
      for (i = 0; i < 128; i = i + 1)   // Comparing SRAM Contents with EAROM
        begin
          if ((U_NMS.U_NOVEA.uut.memb[i] ^ U_NMS.U_NOVEA.uut.earom[i]) !== 0) IncorrCells = IncorrCells + 1;
        end
  end
 endtask   // ---   Comp_SRAM_EAROM



// ------------------    Display_Status    ----------------
 task Display_Status;    // Displays Status of NMS
  begin
    $display("  -- I N F O :    NMS Status:     NMS_READY = %b,   NMS_PASS = %b,    KEEP_ON = %b.",
                 NMS_READY, NMS_PASS, KEEP_ON);
  end
 endtask   // ---   Display_Status



// ------------------    Get_PlusargVal    ----------------
 function [31:0] Get_PlusargVal;    // Get integer Value from plusarg
   input [127:0] PlusargStart;
   input [2:0] Digits;
   
   reg [191:0] Plusarg;
   reg [7:0] s;
   integer i;
   integer Found;
  begin
    Get_PlusargVal = 0;
    Found = 0;
    Plusarg = PlusargStart;
    
    for (i = 1; (i <= Digits) && (Found == i - 1); i = i + 1)
      begin
        Plusarg = Plusarg << 8;
        for (s = "0"; (s <= "9") && (Found != i); s = s + 1)
          begin
            Plusarg[7:0] = s;
            if ($test$plusargs(Plusarg))
              begin
                Found = Found + 1;
                Get_PlusargVal = Get_PlusargVal * 10 + (s - "0");
              end
          end
      end
      
    if (Found == 0) Get_PlusargVal = 1'bx;
    
  end
 endfunction   // ---   Get_PlusargVal



// ------------------    TDelay    ----------------
 task TDelay;    // Provides Delays between Tests
  begin
    #5; //#500;   // This delay can be removed
  end
 endtask   // ---   TDelay



// ------------------    Parall_WriteRead_Test    ----------------
 task Parall_WriteRead_Test;    // Tests Write and Read of NOVeA SRAM through Parallel pins
    integer i;
  begin
      TestName = "Parall_WriteRead";
      TestNum = TestNum + 1;
      $display("\n\n  -- I N F O :   ***********    Testing NOVeA SRAM Parallel Write/Read    ***********");

      @(negedge SYS_CK);
      RA_p = 1'b0;
      OE_p = 1'b1;
      ME_p = 1'b1;

      IncorrCells = 0;
      for (i = 127; i >= 0; i = i - 1)
          begin
            ADR_p = i;
            D_p   = ~MemArray[i];
            WE_p  = 1'b1;              //  Writing

            @(negedge SYS_CK);
          end

      ME_p = 1'b0;
      @(negedge SYS_CK);
      ME_p = 1'b1;

      for (i = 127; i >= 0; i = i - 1)
          begin
            ADR_p = i;
            D_p   = MemArray[i];
            WE_p  = 1'b0;              //  Reading

            @(negedge SYS_CK);
            D_p   = MemArray[i];
            WE_p  = 1'b1;              //  Writing

            @(posedge SYS_CK);
            TempWord = ~MemArray[i];
            if ((Q ^ TempWord) !== 32'b0)
              begin
                  IncorrCells = IncorrCells + 1;
                  $display("<<>> F A I L :     NOVeA SRAM Parallel Write/Read Failed(1)  !!!   ADR_p = %d, Q = %b,  Expected: Q = %b", ADR_p, Q, TempWord);
              end

            @(negedge SYS_CK);
            D_p   = ~MemArray[i];
            WE_p  = 1'b0;              //  Reading

            @(negedge SYS_CK);
            @(posedge SYS_CK);
            if ((Q ^ MemArray[i]) !== 32'b0)
              begin
                  IncorrCells = IncorrCells + 1;
                  $display("<<>> F A I L :     NOVeA SRAM Parallel Write/Read Failed(2)  !!!   ADR_p = %d, Q = %b,  Expected: Q = %b", ADR_p, Q, MemArray[i]);
              end

            @(negedge SYS_CK);
          end

      if (IncorrCells === 0)
            $display("\n  >> P A S S :     NOVeA SRAM Parallel Write/Read Passed    !!! \n");
      else  Fails = Fails + 1;

      ME_p = 1'b0;
      WE_p = 1'b0;
      OE_p = 1'b0;

      TestName = 1'bx;

  end
 endtask   // ---   Parall_WriteRead_Test



// ------------------    ConfigReg_Test    ----------------
 task ConfigReg_Test;    // Tests Access to NMS Configuration Registers
    reg [7:0] ConfReg_Expect [7:0];
    integer i;
  begin  // Note: Config Regs should be reset before Running this Test
      TestName = "ConfigReg";
      TestNum = TestNum + 1;
      $display("\n\n  -- I N F O :   ***********    Testing Access to NMS Configuration Registers    ********");

      ConfReg_Expect[3'b000] = 8'b00000001; // Initial (Default) Values
      ConfReg_Expect[3'b001] = 8'b00010010;
      ConfReg_Expect[3'b010] = 8'b10010000;
      ConfReg_Expect[3'b011] = 8'b10010110;
      ConfReg_Expect[3'b100] = 8'b01011001;
      ConfReg_Expect[3'b101] = 8'b00010101;
      ConfReg_Expect[3'b110] = 8'b01001111;
      ConfReg_Expect[3'b111] = 8'b00000001;

      @(negedge SYS_CK);
      OE_p = 1'b1;
      RA_p = 1'b1;

      IncorrCells = 0;
// -    -    -    -    -  Reading Initial values then Writing New values   -    -    -
      for (i = 0; i < 8; i = i + 1)
          begin
            ADR_p = i;
            D_p   = 'bx;
            ME_p  = 1'b1;
            WE_p  = 1'b0;              //  Reading

            @(negedge SYS_CK);
            @(posedge SYS_CK);
            if ((Q ^ ConfReg_Expect[ADR_p]) !== 0)
              begin
                  IncorrCells = IncorrCells + 1;
                  $display("\n<<>> F A I L :    Incorrect Initial Value of Configuration Register:  ADR_p = %b;    Q = %b, Expected: %b \n",
                           ADR_p, Q, ConfReg_Expect[ADR_p]);
              end

            case (ADR_p)
              3'b000:   ConfReg_Expect[ADR_p] = 8'b01000101;  // New Values
              3'b001:   ConfReg_Expect[ADR_p] = 8'b00101011;
              3'b010:   ConfReg_Expect[ADR_p] = 8'b00111001;
              3'b011:   ConfReg_Expect[ADR_p] = 8'b00100001;
              3'b100:   ConfReg_Expect[ADR_p] = 8'b10100110;
              3'b101:   ConfReg_Expect[ADR_p] = 8'b00001000;
            endcase

            @(negedge SYS_CK);
            D_p   = ConfReg_Expect[ADR_p];
            ME_p  = 1'b1;
            WE_p  = 1'b1;              //  Writing

            @(negedge SYS_CK);
          end

// -    -    -    -    -  Reading previously Written values   -    -     -     -
      for (i = 0; i < 8; i = i + 1)
          begin
            ADR_p = i;
            D_p   = 'bx;
            ME_p  = 1'b1;
            WE_p  = 1'b0;              //  Reading

            @(negedge SYS_CK);
            @(posedge SYS_CK);
            if ((Q ^ ConfReg_Expect[ADR_p]) !== 0)
              begin
                  IncorrCells = IncorrCells + 1;
                  $display("\n<<>> F A I L :    Incorrect Value of Configuration Register:  ADR_p = %b;   Q = %b, Expected:Q = %b \n",
                           ADR_p, Q, ConfReg_Expect[ADR_p]);
              end
            @(negedge SYS_CK);
          end

// -    -    -    -    -    -    -    -    -     -     -     -
      ME_p   = 1'b0;
      WE_p   = 1'b0;
      OE_p   = 1'b0;
      RA_p   = 1'b0;

      if (IncorrCells === 0)
            $display("\n  >> P A S S :     NMS Configuration Registers Test Passed    !!! \n");
      else  Fails = Fails + 1;

      TestName = 1'bx;

  end
 endtask   // ---   ConfigReg_Test



// ------------------    Bypass_Test    ----------------
 task Bypass_Test;    // Tests Bypass Mode
    input Parall;
    reg [8:0] TestData;
    integer CorrectBitCnt;
    integer i;
  begin
      TestName = "Bypass";
      TestNum = TestNum + 1;
      $display("\n\n  -- I N F O :   ***********    Testing Bypass Mode    ***********");

      if (Parall)
          Load_Instr_Par(`I_BYPASS);
      else
          Load_WIR({`I_BYPASS, ConfigCode});

      TestData = 9'b100011000;
      CorrectBitCnt = 0;

      @(negedge WRCK);
      ShiftWR = 1'b1;
      SelectWIR = 1'b0;

      for (i = 0; i <= 8; i = i + 1)
        begin
          WSI = TestData[8-i];
          if (i == 4)
            begin
              ShiftWR = 0;
              repeat(3) @(negedge WRCK);
              ShiftWR = 1;
            end
          @(posedge WRCK);
          if ((i > 0) && ((WSO ^ TestData[8-i+1]) === 0)) CorrectBitCnt = CorrectBitCnt + 1;
          @(negedge WRCK);
        end

      ShiftWR = 1'b0;

      Display_Status;
      if (CorrectBitCnt === 8 && NMS_PASS && NMS_READY)
            $display("\n  >> P A S S :     Bypass Mode Test  Passed    !!! \n");
      else
        begin
            $display("\n<<>> F A I L :     Bypass Mode Test  Failed    !!! \n");
            Fails = Fails + 1;
        end

      TestName = 1'bx;

  end
 endtask   // ---   Bypass_Test



// ------------------    Serial_Test    ----------------
 task Serial_Test;    // Tests Serial Access to NOVeA SRAM
    input Parall;
    integer i, j;
  begin
      TestName = "Serial";
      TestNum = TestNum + 1;
      $display("\n\n  -- I N F O :   ***********    Testing Serial Access    ***********");

      if (Parall)
        begin
          @(negedge WRCK);
          Load_Instr_Par(`I_SERIAL);
        end
      else
          Load_WIR({`I_SERIAL, ConfigCode});

      IncorrCells = 0;
      @(negedge WRCK);
      SelectWIR = 0;
      ShiftWR = 1;

      for (i = 127; i >= 0; i = i - 1)
        for (j = 31; j >= 0; j = j - 1)
          begin
            TempWord = MemArray[i];
            WSI = ~TempWord[j];
            @(posedge WRCK);
            if ((WSO ^ TempWord[j]) !== 1'b0)
              begin
                IncorrCells = IncorrCells + 1;
                $display("<<>> F A I L :    Incorrect Data on Serial Output(1):  WSO = %b,   Expected = %b.", WSO, TempWord[j]);
              end
            @(negedge WRCK);
          end

      ShiftWR = 0;

      @(negedge WRCK);
      ShiftWR = 1;

      for (i = 127; i >= 0; i = i - 1)
        for (j = 31; j >= 0; j = j - 1)
          begin
            TempWord = MemArray[i];
            WSI = TempWord[j];
            @(posedge WRCK);
            if ((WSO ^ ~TempWord[j]) !== 1'b0)
              begin
                IncorrCells = IncorrCells + 1;
                $display("<<>> F A I L :    Incorrect Data on Serial Output(2):  WSO = %b,   Expected = %b.", WSO, ~TempWord[j]);
              end
            @(negedge WRCK);
          end

      ShiftWR = 0;

      Display_Status;
      if (IncorrCells === 0 && NMS_PASS && NMS_READY)
            $display("\n  >> P A S S :     Serial Access to NOVeA SRAM  Passed    !!! \n");
      else
        begin
            $display("\n<<>> F A I L :     Serial Access to NOVeA SRAM  Failed    !!! \n");
            Fails = Fails + 1;
        end

      TestName = 1'bx;

  end
 endtask   // ---   Serial_Test



// ------------------    Serial_Loop_Test    ----------------
 task Serial_Loop_Test;    // Tests Serial Loop Mode of NMS
    integer i, j;
  begin
      TestName = "Serial_Loop";
      TestNum = TestNum + 1;
      $display("\n\n  -- I N F O :   ***********    Testing Serial Loop Mode    ***********");

      Load_WIR({`I_SER_LOOP, ConfigCode});

      IncorrCells = 0;
      @(negedge WRCK);
      SelectWIR = 0;
      ShiftWR = 1;

      for (i = 127; i >= 0; i = i - 1)
        for (j = 31; j >= 0; j = j - 1)
          begin
            TempWord = MemArray[i];
            WSI = 'bx;
            @(posedge WRCK);
            if ((WSO ^ TempWord[j]) !== 0)
              begin
                IncorrCells = IncorrCells + 1;
                $display("<<>> F A I L :    Incorrect Data on Serial Output:  WSO = %b,   Expected = %b.", WSO, TempWord[j]);
              end
            @(negedge WRCK);

            if (i == 5)
              begin
                ShiftWR = 0;
                repeat(3) @(negedge WRCK);
                ShiftWR = 1;
              end
          end

      ShiftWR = 0;

      Display_Status;
      if (IncorrCells === 0 && NMS_PASS && NMS_READY)
            $display("\n  >> P A S S :     Test of Serial Loop Mode  Passed    !!! \n");
      else
        begin
            $display("\n<<>> F A I L :     Test of Serial Loop Mode  Failed    !!! \n");
            Fails = Fails + 1;
        end

      TestName = 1'bx;

  end
 endtask   // ---   Serial_Loop_Test



// ------------------    AutoShift_Test    ----------------
 task AutoShift_Test;    // Tests Serial Loop Mode of NMS
    integer i;
  begin
      TestName = "AutoShift";
      TestNum = TestNum + 1;
      $display("\n\n  -- I N F O :   ***********    Testing Cyclic AutoShift    ***********");

      Load_WIR({`I_SER_LOOP, ConfigCode});

      @(negedge WRCK);
      SelectWIR = 0;
      ShiftWR = 1;

      repeat(7 * 32) @(negedge WRCK); // Shifting NOVeA SRAM Contents
      ShiftWR = 0;

      fork
        Load_WIR({`I_AUTO_SHIFT, ConfigCode});

        @(posedge NMS_READY);
      join
      
      @(posedge SYS_CK);
      Display_Status;

      IncorrCells = 0;
      for (i = 0; i < 128; i = i + 1)   // Comparing SRAM Contents with expected data
        begin
          if ((U_NMS.U_NOVEA.uut.memb[i] ^ MemArray[i]) !== 0) IncorrCells = IncorrCells + 1;
        end

      if (IncorrCells === 0 && NMS_PASS && NMS_READY)
            $display("\n  >> P A S S :     Test of Cyclic AutoShift  Passed    !!! \n");
      else
        begin
          if (Testing_Reset)
            begin
              $display("\n  >> P A S S :     Test of Cyclic AutoShift  Failed    (as Expected). \n");
              ExpectedFails = ExpectedFails + 1;
            end
          else
            begin
              $display("\n<<>> F A I L :     Test of Cyclic AutoShift  Failed    !!! \n");
              Fails = Fails + 1;
            end
        end

      TestName = 1'bx;

  end
 endtask   // ---   AutoShift_Test



// ------------------    FullStore_Test    ----------------
 task FullStore_Test;    // Tests Full Store Operation
    input Parall;
  begin
      TestName = "FullStore";
      TestNum = TestNum + 1;
      $display("\n\n  -- I N F O :   ***********    Testing Full Store Operation    ***************");
      $display("\n  -- I N F O :     NOVeA store_success_count == %0d", U_NMS.U_NOVEA.uut.store_success_count);
      StorePulse_Count = 0;

      if (Parall)
        begin
          Load_Instr_Par(`I_FULL_STORE);
        end
      else
          Load_WIR({`I_FULL_STORE, ConfigCode});

      @(posedge NMS_READY);
      @(posedge SYS_CK);
      Display_Status;
      
      Comp_SRAM_EAROM(IncorrCells);
      
      if (NMS_PASS && NMS_READY && (IncorrCells === 0) &&
          (U_NMS.U_NOVEA.uut.store_ctr >= U_NMS.U_NOVEA.uut.store_success_count))
              $display("\n  >> P A S S :     Full Store Operation Passed    !!! \n");
      else if ((Testing_WatchDog && !NMS_PASS) ||
          ((U_NMS.U_NOVEA.uut.store_ctr < U_NMS.U_NOVEA.uut.store_success_count) && !WatchDog_Rst_Occured))
            begin
              $display("\n  >> P A S S :     Full Store Operation Failed   (as expected). \n");
              ExpectedFails = ExpectedFails + 1;
            end
          else
            begin
              $display("\n<<>> F A I L :     Full Store Operation Failed    !!! \n");
              Fails = Fails + 1;
            end

      TestName = 1'bx;

  end
 endtask   // ---   FullStore_Test



// ------------------    SoftStore_Test    ----------------
 task SoftStore_Test;    // Tests Soft Store Operation
  begin
      TestName = "SoftStore";
      TestNum = TestNum + 1;
      $display("\n\n  -- I N F O :   ***********    Testing Soft Store Operation    ***************");

      Load_WIR({`I_SOFT_STORE, ConfigCode});

      @(posedge NMS_READY);
      @(posedge SYS_CK);
      Display_Status;

      if (NMS_PASS && NMS_READY)
              $display("\n  >> P A S S :     Soft Store Operation Passed    !!! \n");
      else
        begin
              $display("\n<<>> F A I L :     Soft Store Operation Failed    !!! \n");
              Fails = Fails + 1;
        end

      TestName = 1'bx;

  end
 endtask   // ---   SoftStore_Test



// ------------------    NRecall_Test    ----------------
 task NRecall_Test;    // Tests Normal Recall Operation
   input Parall;
  begin
      TestName = "NRecall";
      TestNum = TestNum + 1;
      $display("\n\n  -- I N F O :   ***********    Testing Normal Recall Operation    ***************");

      if (Parall)
          Load_Instr_Par(`I_NRECALL);
      else
          Load_WIR({`I_NRECALL, ConfigCode});

      @(posedge NMS_READY);
      @(posedge SYS_CK);
      Display_Status;

      Comp_SRAM_EAROM(IncorrCells);
      
      if (NMS_PASS && NMS_READY && (IncorrCells === 0))
              $display("\n  >> P A S S :     Normal Recall Operation Passed    !!! \n");
      else
        begin
              $display("\n<<>> F A I L :     Normal Recall Operation Failed    !!! \n");
              Fails = Fails + 1;
        end

      TestName = 1'bx;

  end
 endtask   // ---   NRecall_Test



// ------------------    MRecall_Test    ----------------
 task MRecall_Test;    // Tests Margin Recall Operation through P1500
  begin
      TestName = "MRecall";
      TestNum = TestNum + 1;
      $display("\n\n  -- I N F O :   ***********    Testing Margin Recall Operation    ***************");

      Load_WIR({`I_MRECALL, ConfigCode});

      @(posedge NMS_READY);
      @(posedge SYS_CK);
      Display_Status;

      Comp_SRAM_EAROM(IncorrCells);
      
      if (NMS_PASS && NMS_READY && (IncorrCells === 0))
              $display("\n  >> P A S S :     Margin Recall Operation Passed    !!! \n");
      else
        begin
              $display("\n<<>> F A I L :     Margin Recall Operation Failed    !!! \n");
              Fails = Fails + 1;
        end

      TestName = 1'bx;

  end
 endtask   // ---   MRecall_Test



// ------------------    NCompare_Test    ----------------
 task NCompare_Test;    // Tests Normal Compare Operation
   input Parall;
  begin
      TestName = "NCompare";
      TestNum = TestNum + 1;
      $display("\n\n  -- I N F O :   ***********    Testing Normal Compare Operation    ***************");

      if (Parall)
          Load_Instr_Par(`I_NCOMPARE);
      else
          Load_WIR({`I_NCOMPARE, ConfigCode});

      @(posedge NMS_READY);
      @(posedge SYS_CK);
      Display_Status;

      Comp_SRAM_EAROM(IncorrCells);
      
      if (NMS_PASS && NMS_READY && (IncorrCells === 0))
              $display("\n  >> P A S S :     Normal Compare Operation Passed    !!! \n");
      else
        begin
              $display("\n<<>> F A I L :     Normal Compare Operation Failed    !!! \n");
              Fails = Fails + 1;
        end

      TestName = 1'bx;

  end
 endtask   // ---   NCompare_Test



// ------------------    MCompare_Test    ----------------
 task MCompare_Test;    // Tests Margin Compare Operation through P1500
  begin
      TestName = "MCompare";
      TestNum = TestNum + 1;
      $display("\n\n  -- I N F O :   ***********    Testing Margin Compare Operation    ***************");

      Load_WIR({`I_MCOMPARE, ConfigCode});

      @(posedge NMS_READY);
      @(posedge SYS_CK);
      Display_Status;

      Comp_SRAM_EAROM(IncorrCells);
      
      if (NMS_PASS && NMS_READY && (IncorrCells === 0))
              $display("\n  >> P A S S :     Margin Compare Operation Passed    !!! \n");
      else
        begin
              $display("\n<<>> F A I L :     Margin Compare Operation Failed    !!! \n");
              Fails = Fails + 1;
        end

      TestName = 1'bx;

  end
 endtask   // ---   MCompare_Test



// ------------------    Keep_Test    ----------------
 task Keep_Test;    // Tests Keep Mode
    input Parall;
    input LongDelay;
    integer i;
  begin
      TestName = "Keep_On";
      TestNum = TestNum + 1;
      $display("\n\n  -- I N F O :   ***********    Testing Keep Mode    ***************");

      if (Parall)
          Load_Instr_Par(`I_KEEP);
      else
          Load_WIR({`I_KEEP, ConfigCode});


      @(posedge NMS_READY);
      @(posedge SYS_CK);
      if (KEEP_ON !== 1)
        begin
              $display("\n<<>> F A I L :     KEEP_ON is not 1 in Keep Mode : KEEP_ON = %b .", KEEP_ON);
              Fails = Fails + 1;
        end
      else
        begin
              $display("\n  -- I N F O :     Keep Mode is ON    ...");

//--------------------------------------------------
              $display("\n\n  -- I N F O :     Performing Read from NOVeA SRAM in Keep Mode ...");
              @(negedge SYS_CK);
              RA_p = 1'b0;
              ME_p = 1'b1;
              WE_p = 1'b0;
              OE_p = 1'b1;

              IncorrCells = 0;
              for (i = 127; i >= 0; i = i - 1)
                  begin
                    ADR_p = i;
                    D_p   = ~MemArray[i];

                    @(negedge SYS_CK);
                    @(posedge SYS_CK);              //  Reading
                    if ((Q ^ MemArray[i]) !== 0)
                      begin
                          IncorrCells = IncorrCells + 1;
                          $display("\n<<>> F A I L :     NOVeA SRAM Parallel Read Failed  !!!   ADR_p = %d, Q = %b, Expected: %b", ADR_p, Q, MemArray[i]);
                      end

                    @(negedge SYS_CK);
                  end

              if (IncorrCells === 0)
                    $display("\n  >> P A S S :     NOVeA SRAM Parallel Read Passed    !!! \n");
              else  Fails = Fails + 1;

              ME_p = 1'b0;
              WE_p = 1'b0;
              OE_p = 1'b0;

//--------------------------------------------------
              if (LongDelay)
                begin
                  TestName = "Keep_Stay";
                  $display("  -- I N F O :    Staying in Keep Mode .......");
`ifdef VIRAGE_FAST
                  #15000000;
`else
                  #300000000;
`endif
                end

              TestName = "Keep_Off";

              if (Parall)
                  Load_Instr_Par(`I_IDLE);
              else
                  Load_WIR({`I_IDLE, ConfigCode});

              @(posedge NMS_READY);
              @(posedge SYS_CK);
              if (KEEP_ON !== 0)
                begin
                    $display("\n<<>> F A I L :     KEEP_ON is not 0 after exiting Keep Mode : KEEP_ON = %b .\n", KEEP_ON);
                    Fails = Fails + 1;
                end

              else
                begin
                    $display("\n  -- I N F O :     Keep Mode is OFF    ... ");
                    if (NMS_PASS && NMS_READY)
                          $display("\n  >> P A S S :     Keep Mode Test Passed    !!! \n");
                    else
                      begin
                          $display("\n<<>> F A I L :     Keep Mode Test Failed    !!! \n");
                          Fails = Fails + 1;
                      end
                end

        end

      TestName = 1'bx;

  end
 endtask   // ---   Keep_Test




// ------------------    Recall_AM_Test    ----------------
 task Recall_AM_Test;    // Tests Recall Auto Margin Operation
   input Parall;
   input Corrupt;
  begin
      TestName = "Recall_AM";
      TestNum = TestNum + 1;
      $display("\n\n  -- I N F O :   ***********    Testing Recall Auto Margin Operation    *************");
      $display("\n  -- I N F O :     NOVeA store_success_count == %0d", U_NMS.U_NOVEA.uut.store_success_count);

      if (Parall)
          Load_Instr_Par(`I_RECALLAM);
      else
          Load_WIR({`I_RECALLAM, ConfigCode});

      @(negedge U_NMS.U_NOVEA.RECALL);
      if (Corrupt) Corrupt_EAROM;
      
      @(posedge NMS_READY);
      @(posedge SYS_CK);
      Display_Status;

      Comp_SRAM_EAROM(IncorrCells);
      
      if (NMS_PASS && NMS_READY && (IncorrCells === 0))
              $display("\n  >> P A S S :     Recall Auto Margin Operation Passed    !!! \n");
      else
        begin
              $display("\n<<>> F A I L :     Recall Auto Margin Operation Failed    !!! \n");
              Fails = Fails + 1;
        end

      TestName = 1'bx;

  end
 endtask   // ---   Recall_AM_Test



// -----------------------------------------------------------------------------------------



// ------------------    Reset_Test    ----------------
 task Reset_Test;    // Tests Reset Instruction
   integer StepCntr;
  begin
      TestName = "Reset";
      TestNum = TestNum + 1;
      $display("\n\n  -- I N F O :   ***********    Testing Reset Instruction    ***************");

      Load_WIR({`I_RESET, ConfigCode});

      StepCntr = 0;
      fork
        begin : RstTest_Steps
          wait(U_NMS.U_NMSC.RST_i);
          StepCntr = StepCntr + 1;
          @(negedge U_NMS.U_NMSC.RST_i);
          StepCntr = StepCntr + 1;
          @(posedge NMS_READY);
          StepCntr = StepCntr + 1;
          disable RstTest_WtchDog;
        end

        begin : RstTest_WtchDog
          repeat (8) @(posedge SYS_CK);
          if (StepCntr < 1)
              disable RstTest_Steps;
          else
            begin
              #3000000;
              if (StepCntr < 2) disable RstTest_Steps;
            end
        end
      join

      if (StepCntr == 3)
              $display("\n  >> P A S S :     Reset Instruction Test Passed    !!! \n");
      else
        begin
              $display("\n<<>> F A I L :     Reset Instruction Test Failed after Step %0d  !!! \n", StepCntr);
              Fails = Fails + 1;
        end

      TestName = 1'bx;

  end
 endtask   // ---   Reset_Test



// ------------------    WatchDog_Test    ----------------
 task WatchDog_Test;    // Tests WatchDog
  begin
      $display("\n\n  -- I N F O :   ***********    Testing WatchDog    ***************");
      NMS_Reset;
      
      Testing_WatchDog = 1;
      $display("  -- I N F O :    Forcing UNLOCK of ChargePump = 0 ...");
      force U_NMS.U_ChPump.UNLOCK = 0;

      FullStore_Test(PAR_LOAD);

      $display("  -- I N F O :    Releasing UNLOCK of ChargePump ...");
      release U_NMS.U_ChPump.UNLOCK;
      Testing_WatchDog = 0;
      WatchDog_Rst_Occured = 0;
  end
 endtask   // ---   WatchDog_Test



// ------------------    Help    ----------------
 task Help;    // Displays Help information
  begin
      $display("\n    ******     NOVeA Memory System TestBench  Help   ******\n");

      $display("\n  ======     Test mode plusargs (use one of these):");
      $display("      +run_all        runs all tests (default)");
      $display("      +help           displays this help");
      $display("      +max_cover      runs tests for Maximal Code Coverage");
      $display("      +max_cover_1    runs reduced set of tests for nearly Maximal Code Coverage");
      $display("      +fullstore      tests FullStore operation");
      $display("      +test_watchdog  tests WatchDog of NMS Controller");

      $display("\n  ======     Additional test run plusargs (more than one could be used):");
      $display("      +wrck_off       turns off P1500 WRCK serial clock (by default is on)");
      $display("      +load_par       loads instructions in parallel (default is serial), for single tests");
      $display("      +conf_1 .. 4    specifies line (1..4) of ConfigCode file to be used as configuration code");
      $display("      +storsc=NNN     specifies the value (NNN) of NOVEA Store Success Count");
      $display("      +exit_on_fail   stops the simulation if Failed");
      $display("");

  end
 endtask   // ---   Help



// ------------------    Run_FullStore    ----------------
 task Run_FullStore;    // Runs FullStore test
  begin
      NMS_Reset;

      Load_SRAM;

      if (InstrLoad_Mode == PAR_LOAD)
          Load_Config_Par(ConfigCode[7:0], ConfigCode[14:8], ConfigCode[22:15],
                          ConfigCode[30:23], ConfigCode[38:31], ConfigCode[44:39]);
      
      FullStore_Test(InstrLoad_Mode);

  end
 endtask   // ---   Run_FullStore



// ------------------    Run_TestSet_1    ----------------
 task Run_TestSet_1;    // Runs a set of Tests
  begin
      WRCK_Enable = 0;            // Turn Off WRCK Generator

      NMS_Reset;                    TDelay;

      Parall_WriteRead_Test;        TDelay;

      ConfigReg_Test;               TDelay;

      FullStore_Test(PAR_LOAD);     TDelay;

      WRCK_Enable = 1;            // Start WRCK Generator

      NMS_Reset;                    TDelay;

      NRecall_Test(PAR_LOAD);       TDelay;

      ConfigCode = 45'b000000_000000_000000_000000_000000_000_00_00_1010_00_01;
      SoftStore_Test;               TDelay;

      NCompare_Test(PAR_LOAD);      TDelay;

      Recall_AM_Test(PAR_LOAD, 0);  TDelay;

      ConfigCode = 45'b100000_110110_100101_001001_100001_001_00_10_0000_00_00;
      MCompare_Test;                TDelay;
                                    
      MRecall_Test;                 TDelay;
                                    
      Bypass_Test(SER_LOAD);        TDelay;
                                    
      Parall_WriteRead_Test;        TDelay;
                                    
      Serial_Test(SER_LOAD);        TDelay;
                                    
      Serial_Loop_Test;             TDelay;
                                    
      AutoShift_Test;               TDelay;
                                    
      Keep_Test(SER_LOAD, 0);       TDelay;
                                    
      Serial_Test(PAR_LOAD);        TDelay;

      ConfigCode = 45'b111111111111111111111111111111111111111111111;
      Reset_Test;                   TDelay;
                                    
      Keep_Test(PAR_LOAD, 0);       TDelay;
                                    
//      FullStore_Test(SER_LOAD);     TDelay;

  end
 endtask // ---   Run_TestSet_1



// ------------------    Run_Test_MaxCover    ----------------
 task Run_Test_MaxCover;    // Runs a set of Tests to provide Maximal Code Coverage
  begin
`ifdef PRE_SYNTH
    U_NMS.U_NMSC.U_NVC.CurrentState = 31;  // Testing default case of FSM NextState
`endif

      WRCK_Enable = 1;

      NMS_Reset;

      Parall_WriteRead_Test;

      ConfigReg_Test;

      Load_Config_Par(ConfigCode[7:0], ConfigCode[14:8], ConfigCode[22:15],
                      ConfigCode[30:23], ConfigCode[38:31], ConfigCode[44:39]);
      FullStore_Test(PAR_LOAD);

      NRecall_Test(SER_LOAD);
                       
      ConfigCode = 45'b010101_010110_011001_011010_010000_000_00_11_0000_00_00;
      MCompare_Test;

      Corrupt_EAROM;
      
      Serial_Test(PAR_LOAD);

      U_NMS.U_NOVEA.uut.store_success_count = 10;
      ConfigCode = 45'b101010_011101_111110_011001_000010_001_01_10_0010_00_01;
      FullStore_Test(SER_LOAD);
      
      ConfigCode = 45'b000000_110000_001000_110000_011101_111_11_11_0011_01_10;
      Load_Config_Par(ConfigCode[7:0], ConfigCode[14:8], ConfigCode[22:15],
                      ConfigCode[30:23], ConfigCode[38:31], ConfigCode[44:39]);
      FullStore_Test(PAR_LOAD);
      
      NCompare_Test(PAR_LOAD);

      ConfigCode = 45'b110010_100001_010010_100101_001001_011_11_00_1111_11_11;
      SoftStore_Test;

      AutoShift_Test;

      Corrupt_SRAM;

      ConfigCode = 45'b101101_011010_111001_100010_000110_010_10_01_0110_10_10;
      MRecall_Test;

      Bypass_Test(PAR_LOAD);

      Parall_WriteRead_Test;

      U_NMS.U_NOVEA.uut.store_success_count = 3;
      ConfigCode = 45'b110001_100110_010001_101010_001101_100_00_00_0101_10_01;
      Recall_AM_Test(SER_LOAD, 1);

      Serial_Loop_Test;

      Keep_Test(PAR_LOAD, 1);

      Parall_WriteRead_Test;

      Serial_Test(SER_LOAD);

      $display("\n\n  -- I N F O :    ***  Testing Reset Instruction in the Middle of AutoShift  ***\n");
      fork
        begin
          AutoShift_Test;
        end
        begin
          @(negedge UpdateWR);
          @(negedge UpdateWR);
          Testing_Reset = 1;
          #500 Reset_Test;
        end
      join
      Testing_Reset = 0;
      
      WatchDog_Test;
      
      if (!plus_max_cover_1)
        begin
          Parall_WriteRead_Test;

          U_NMS.U_NOVEA.uut.store_success_count = 260;
          ConfigCode = 45'b111110_101001_011110_101101_010000_101_01_01_1010_11_01;
          FullStore_Test(SER_LOAD);
        end
          
  end
 endtask // ---   Run_Test_MaxCover



// -----------------------------------------------------------------------------



// ------------------------       Reset  Messages        ----------------------
  always @(posedge U_NMS.U_ChPump.RST)
    begin
      #0;
      $display("  -- I N F O :   Resetting Charge Pump ... ");
    end

// ---------------     Store  Messages  and  Pulse Width Calculations    ----------------
  always @(posedge U_NMS.U_NOVEA.STORE)
    begin
      Time_STORE_rise = $time;
      StorePulse_Count = StorePulse_Count + 1;
      $display("\n  -- I N F O :   Storing ...    VPPRANGE = %d , VPPSEL = %d",
               U_NMS.U_ChPump.uut.VPPRANGE_latched, U_NMS.U_ChPump.uut.VPPSEL_latched);
    end

  always @(negedge U_NMS.U_NOVEA.STORE)
    begin
      Width_STORE = $time - Time_STORE_rise;
      Time_STORE_rise = 1'bx;
      if ($time > 10)
        begin
          $display("  -- I N F O :   _|--|_ Pulse Width :    STORE: %0d us, PE: %0d us, VPP: %0d us\n",
                   (Width_STORE / 1000), (Width_PE / 1000), (Width_VPP / 1000));
        end
    end

  always @(U_NMS.U_ChPump.PE)
    begin
      if (U_NMS.U_ChPump.PE === 1'b1)
          Time_PE_rise = $time;
      else if (U_NMS.U_ChPump.PE === 1'b0)
            begin
              Width_PE = $time - Time_PE_rise;
              Time_PE_rise = 1'bx;
            end
          else
              Time_PE_rise = 1'bx;
    end

  always @(U_NMS.U_NOVEA.VPP)
    begin
      if (U_NMS.U_NOVEA.VPP === 1'b1)
          Time_VPP_rise = $time;
      else if (U_NMS.U_NOVEA.VPP === 1'b0)
            begin
              Width_VPP = $time - Time_VPP_rise;
              Time_VPP_rise = 1'bx;
            end
          else
              Time_VPP_rise = 1'bx;
    end

// ----------------------      Compare and Recall  Messages    ------------------
  always @(posedge U_NMS.U_NOVEA.COMP)
    begin
      $display("  -- I N F O :   Comparing...  BIAS1= %b, BIAS0= %b, MRCL1= %b, MRCL0= %b, TECC1= %b, TECC0= %b",
               U_NMS.U_NOVEA.BIAS1, U_NMS.U_NOVEA.BIAS0, U_NMS.U_NOVEA.MRCL1, U_NMS.U_NOVEA.MRCL0, U_NMS.U_NOVEA.TECC1, U_NMS.U_NOVEA.TECC0);
    end

  always @(posedge U_NMS.U_NOVEA.RECALL)
    begin
      $display("  -- I N F O :   Recalling...  BIAS1= %b, BIAS0= %b, MRCL1= %b, MRCL0= %b, TECC1= %b, TECC0= %b",
               U_NMS.U_NOVEA.BIAS1, U_NMS.U_NOVEA.BIAS0, U_NMS.U_NOVEA.MRCL1, U_NMS.U_NOVEA.MRCL0, U_NMS.U_NOVEA.TECC1, U_NMS.U_NOVEA.TECC0);
    end

  always @(posedge U_NMS.U_NOVEA.RCREADY)
    begin
      if (U_NMS.U_NOVEA.COMP)
            $display("  -- I N F O :   Compare Completed, MATCH =   %b", U_NMS.U_NOVEA.MATCH);
      else  $display("  -- I N F O :   Recall Completed, RCREADY =  %b\n", U_NMS.U_NOVEA.RCREADY);
    end

// ----------------------      WRCK Generator Messages    ------------------
  always @(WRCK_Enable)
    begin
      if (WRCK_Enable)
          $display("  -- I N F O :    ============    P1500 Clock (WRCK) is ON ...     ===========\n");
      else
          $display("  -- I N F O :    ============    P1500 Clock (WRCK) is OFF ...    ===========\n");
    end

// ----------------------      Exit On Fail    ------------------
  always @(Fails)
    begin
      if (ExitOnFail && (Fails !== 0))
        begin
          #1;
          $display("\n<<>> F A I L :    ********    Fail Detected!!!  Terminating Simulation.    ********\n");
          $finish;
        end
    end

// --------------------------------------------

`ifdef PRE_SYNTH
// --------------------      Timer Overflow  Check    ------------------
  always @(U_NMS.U_NMSC.U_NVC.Timer_r)
    begin
      if (&U_NMS.U_NMSC.U_NVC.Timer_r)
        begin
          $display("<<>> F A I L :   =====  Timer Overflow   =====");
          Fails = Fails + 1;
        end
    end

// --------------------      WatchDog Reset Message    ------------------
  always @(posedge U_NMS.U_NMSC.U_NVC.rst_WatchDog)
    begin
      WatchDog_Rst_Occured = 1;

      if (Testing_WatchDog)
          $display("  -- I N F O :   *****  WatchDog is Resetting the NMS Conrtoller...   *****");
      else
        begin
          $display("<<>> F A I L :   =====  WatchDog is Resetting the NMS Conrtoller...   =====");
          Fails = Fails + 1;
        end
    end

// --------------------------------------------
`endif  // ---  PRE_SYNTH

// --------------------------------------------

endmodule  // ---   NMS_tb
