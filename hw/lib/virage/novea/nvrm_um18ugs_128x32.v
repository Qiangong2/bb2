//---------------------------------------------------------------------
//               Copyright(c) Virage Logic Corporation.                
//     All Rights reserved - Unpublished -rights reserved under        
//     the Copyright laws of the United States of America.             
//                                                                     
//  This file includes the Confidential information of Virage Logic    
//  and UMC                                                            
//  The receiver of this Confidential Information shall not disclose   
//  it to any third party and shall protect its confidentiality by     
//  using the same degree of care, but not less then a reasonable      
//  degree of care, as the receiver uses to protect receiver's own     
//  Confidential Information.                                          
//                                                                     
//                    Virage Logic Corporation.                        
//                    47100 Bayside Pkwy                               
//                    Fremont , California 94538.                      
//                                                                     
//---------------------------------------------------------------------
//                                                                     
//  Software           : Rev: 3.4.1 (build REL-3-4-1-2003-04-26)       
//  Library Format     : Rev: 1.05.00                                  
//  Compiler Name      : um18ugss1p11nvnrm04ksa01                      
//  Date of Generation : Fri Feb 20 15:47:44 PST 2004                  
//                                                                     
//---------------------------------------------------------------------

//   --------------------------------------------------------------     
//                       Template Revision : 1.1.1                      
//   --------------------------------------------------------------     



//                * Synchronous, 1-Port Non Volatile RAM *            
//                      * Verilog Behavioral Model *                  
//                THIS IS A SYNCHRONOUS 1-PORT MEMORY MODEL           
//                                                                    
//   Memory Name:nvrm_um18ugs_128x32                                  
//   Memory Size:128 words x 32 bits                                  
//                                                                    
//                               PORT NAME                            
//                               ---------                            
//               Output Ports                                         
//                                   Q0-Q31                           
//                                   SO                               
//                                   MATCH                            
//                                   RCREADY                          
//               Input Ports:                                         
//                                   ADR0-ADR6                        
//                                   DIN0-DIN31                       
//                                   WE                               
//                                   ME                               
//                                   OE                               
//                                   CLK                              
//                                   SI                               
//                                   SCLK                             
//                                   SME                              
//                                   COMP                             
//                                   STORE                            
//                                   RECALL                           
//                                   VPP                              
//                                   MRCL0                            
//                                   MRCL1                            
//                                   TECC0                            
//                                   TECC1                            
//                                   BIAS0                            
//                                   BIAS1                            
//                                   PU1                              
//                                   PU2                              
//                                   PU3                              



`resetall 

`timescale 1 ns / 10 ps 
`celldefine 

`define True    1'b1
`define False   1'b0

module nvrm_um18ugs_128x32 ( Q31, Q30, Q29, Q28, Q27, Q26, Q25, Q24, Q23, Q22, Q21, Q20, Q19, Q18, Q17, Q16, Q15, Q14, Q13, Q12, Q11, Q10, Q9, Q8, Q7, Q6, Q5, Q4, Q3, Q2, Q1, Q0, SO, MATCH, RCREADY, ADR6, ADR5, ADR4, ADR3, ADR2, ADR1, ADR0, DIN31, DIN30, DIN29, DIN28, DIN27, DIN26, DIN25, DIN24, DIN23, DIN22, DIN21, DIN20, DIN19, DIN18, DIN17, DIN16, DIN15, DIN14, DIN13, DIN12, DIN11, DIN10, DIN9, DIN8, DIN7, DIN6, DIN5, DIN4, DIN3, DIN2, DIN1, DIN0, WE, ME, OE, CLK, SI, SCLK, SME, COMP, STORE, RECALL, VPP, MRCL0, MRCL1, TECC0, TECC1, BIAS0, BIAS1, PU1, PU2, PU3);

output  Q31, Q30, Q29, Q28, Q27, Q26, Q25, Q24, Q23, Q22, Q21, Q20, Q19, Q18, Q17, Q16, Q15, Q14, Q13, Q12, Q11, Q10, Q9, Q8, Q7, Q6, Q5, Q4, Q3, Q2, Q1, Q0;
output SO;
output MATCH;
output RCREADY;
input  ADR6, ADR5, ADR4, ADR3, ADR2, ADR1, ADR0;
input  DIN31, DIN30, DIN29, DIN28, DIN27, DIN26, DIN25, DIN24, DIN23, DIN22, DIN21, DIN20, DIN19, DIN18, DIN17, DIN16, DIN15, DIN14, DIN13, DIN12, DIN11, DIN10, DIN9, DIN8, DIN7, DIN6, DIN5, DIN4, DIN3, DIN2, DIN1, DIN0;
input WE;
input ME;
input OE;
input CLK;
input SI;
input SCLK;
input SME;
input COMP;
input STORE;
input RECALL;
input VPP;
input MRCL0;
input MRCL1;
input TECC0;
input TECC1;
input BIAS0;
input BIAS1;
input PU1;
input PU2;
input PU3;


// Local registers, wires, etc

`ifdef MEM_CHECK_OFF
parameter MES_ALL = "OFF";
`else
parameter MES_ALL = "ON";
`endif


reg notif_adr; 
reg notif_din; 
reg notif_we; 
reg notif_me; 
reg notif_clk; 
reg notif_clk_COMP_rise_rec;
reg notif_clk_SCLK_rise_rec;
reg notif_clk_RECALL_rise_rec;
reg notif_clk_STORE_rise_rec;
reg notif_si; 
reg notif_sclk; 
reg notif_sclk_COMP_rise_rec;
reg notif_sclk_CLK_rise_rec;
reg notif_sclk_RECALL_rise_rec;
reg notif_sclk_STORE_rise_rec;
reg notif_sme; 
reg notif_comp; 
reg notif_comp_RECALL_rise_rec;
reg notif_comp_STORE_rise_rec;
reg notif_comp_SCLK_rise_rec;
reg notif_comp_CLK_rise_rec;
reg notif_store; 
reg notif_store_RECALL_rise_rec;
reg notif_store_VPP_rise_rec;
reg notif_store_COMP_rise_rec;
reg notif_store_SCLK_rise_rec;
reg notif_store_CLK_rise_rec;
reg notif_recall; 
reg notif_recall_STORE_rise_rec;
reg notif_recall_COMP_rise_rec;
reg notif_recall_SCLK_rise_rec;
reg notif_recall_CLK_rise_rec;
reg notif_vpp; 
reg notif_vpp_STORE_fall_rec;
reg notif_mrcl0; 
reg notif_mrcl1; 
reg notif_tecc0; 
reg notif_tecc1; 
reg notif_bias0; 
reg notif_bias1; 
reg ADRFLAGA;
wire CHKENWE;
wire CHKENCLK;
wire CHKENSCLK;
wire CHKENSI;
wire CHKENCLKSCLK;
wire CHKENSCLKCLK;
wire CHKENME;
wire CHKENADR;
wire CHKENDIN;
wire CHKEN;
wire CHKEN6A;
wire CHKEN7A;
wire CHKEN10A;
wire CHKEN11A;
wire CHKEN12A;
wire CHKEN13A;
wire CHKEN14A;
reg SMElatched;
reg chk_tmp0;
reg chk_tp0;
wire OE_eff;
buf  #0.01 (OE_eff_delay , OE_eff);


reg [6:0] ADRlatched;
reg [31:0] DINlatched;
reg WElatched;
reg MElatched;

wire [31:0] Q_buf;
bufif1 (Q0, Q_buf[0], OE_eff_delay );
bufif1 (Q1, Q_buf[1], OE_eff_delay );
bufif1 (Q2, Q_buf[2], OE_eff_delay );
bufif1 (Q3, Q_buf[3], OE_eff_delay );
bufif1 (Q4, Q_buf[4], OE_eff_delay );
bufif1 (Q5, Q_buf[5], OE_eff_delay );
bufif1 (Q6, Q_buf[6], OE_eff_delay );
bufif1 (Q7, Q_buf[7], OE_eff_delay );
bufif1 (Q8, Q_buf[8], OE_eff_delay );
bufif1 (Q9, Q_buf[9], OE_eff_delay );
bufif1 (Q10, Q_buf[10], OE_eff_delay );
bufif1 (Q11, Q_buf[11], OE_eff_delay );
bufif1 (Q12, Q_buf[12], OE_eff_delay );
bufif1 (Q13, Q_buf[13], OE_eff_delay );
bufif1 (Q14, Q_buf[14], OE_eff_delay );
bufif1 (Q15, Q_buf[15], OE_eff_delay );
bufif1 (Q16, Q_buf[16], OE_eff_delay );
bufif1 (Q17, Q_buf[17], OE_eff_delay );
bufif1 (Q18, Q_buf[18], OE_eff_delay );
bufif1 (Q19, Q_buf[19], OE_eff_delay );
bufif1 (Q20, Q_buf[20], OE_eff_delay );
bufif1 (Q21, Q_buf[21], OE_eff_delay );
bufif1 (Q22, Q_buf[22], OE_eff_delay );
bufif1 (Q23, Q_buf[23], OE_eff_delay );
bufif1 (Q24, Q_buf[24], OE_eff_delay );
bufif1 (Q25, Q_buf[25], OE_eff_delay );
bufif1 (Q26, Q_buf[26], OE_eff_delay );
bufif1 (Q27, Q_buf[27], OE_eff_delay );
bufif1 (Q28, Q_buf[28], OE_eff_delay );
bufif1 (Q29, Q_buf[29], OE_eff_delay );
bufif1 (Q30, Q_buf[30], OE_eff_delay );
bufif1 (Q31, Q_buf[31], OE_eff_delay );
buf (SO, SO_buf);
buf (MATCH, MATCH_buf);
buf (RCREADY, RCREADY_buf);
wire [6:0] ADR_buf;
buf (ADR_buf[0], ADR0);
buf (ADR_buf[1], ADR1);
buf (ADR_buf[2], ADR2);
buf (ADR_buf[3], ADR3);
buf (ADR_buf[4], ADR4);
buf (ADR_buf[5], ADR5);
buf (ADR_buf[6], ADR6);
wire [31:0] DIN_buf;
buf (DIN_buf[0], DIN0);
buf (DIN_buf[1], DIN1);
buf (DIN_buf[2], DIN2);
buf (DIN_buf[3], DIN3);
buf (DIN_buf[4], DIN4);
buf (DIN_buf[5], DIN5);
buf (DIN_buf[6], DIN6);
buf (DIN_buf[7], DIN7);
buf (DIN_buf[8], DIN8);
buf (DIN_buf[9], DIN9);
buf (DIN_buf[10], DIN10);
buf (DIN_buf[11], DIN11);
buf (DIN_buf[12], DIN12);
buf (DIN_buf[13], DIN13);
buf (DIN_buf[14], DIN14);
buf (DIN_buf[15], DIN15);
buf (DIN_buf[16], DIN16);
buf (DIN_buf[17], DIN17);
buf (DIN_buf[18], DIN18);
buf (DIN_buf[19], DIN19);
buf (DIN_buf[20], DIN20);
buf (DIN_buf[21], DIN21);
buf (DIN_buf[22], DIN22);
buf (DIN_buf[23], DIN23);
buf (DIN_buf[24], DIN24);
buf (DIN_buf[25], DIN25);
buf (DIN_buf[26], DIN26);
buf (DIN_buf[27], DIN27);
buf (DIN_buf[28], DIN28);
buf (DIN_buf[29], DIN29);
buf (DIN_buf[30], DIN30);
buf (DIN_buf[31], DIN31);
buf (WE_buf, WE);
buf (ME_buf, ME);
buf (OE_buf, OE);
buf (CLK_buf, CLK);
buf (SI_buf, SI);
buf (SCLK_buf, SCLK);
buf (SME_buf, SME);
buf (COMP_buf, COMP);
buf (STORE_buf, STORE);
buf (RECALL_buf, RECALL);
buf (VPP_buf, VPP);
buf (MRCL0_buf, MRCL0);
buf (MRCL1_buf, MRCL1);
buf (TECC0_buf, TECC0);
buf (TECC1_buf, TECC1);
buf (BIAS0_buf, BIAS0);
buf (BIAS1_buf, BIAS1);
buf (PU1_buf, PU1);
buf (PU2_buf, PU2);
buf (PU3_buf, PU3);

// Disable recovery checks for initial 10 ns
reg init_time;
initial begin
 init_time = 0;
 #10 init_time = 1;
end

wire enable_recovery_checks;
assign enable_recovery_checks = init_time;

wire enable_setuphold_checks;
assign enable_setuphold_checks = init_time;

real tstore;
real store_rise_time;
initial
begin
`ifdef VIRAGE_FAST
  tstore = 500000.00;
`else
  tstore = 10000000.00;
`endif
end
always @(posedge STORE_buf)
  store_rise_time = $realtime;
always @(negedge STORE_buf)
begin
 if ( enable_recovery_checks ) begin
  if( ($realtime - store_rise_time) < tstore )
  begin
    if (!$test$plusargs("notimingchecks") && !$test$plusargs("delay_mode_zero") && !$test$plusargs("delay_mode_unit"))
    begin
      $display("Warning! Timing violation on STORE(Tstore)");
      $display("      time=%t; instance=%m (NVM1H)",$realtime);
      if (notif_store === 1'bx && $time > 0 )
        notif_store = 1'b0;
      else
        notif_store = ~notif_store;
    end
  end
 end
end

real tvpp;
real vpp_rise_time;
initial
begin
`ifdef VIRAGE_FAST
  tvpp = 400000.00;
`else
  tvpp = 8000000.00;
`endif
end
always @(posedge VPP_buf)
  vpp_rise_time = $realtime;
always @(negedge VPP_buf)
begin
 if ( enable_recovery_checks ) begin
  if( ($realtime - vpp_rise_time) < tvpp )
  begin
    if (!$test$plusargs("notimingchecks") && !$test$plusargs("delay_mode_zero") && !$test$plusargs("delay_mode_unit"))
    begin
      $display("Warning! Timing violation on VPP(Tvpp)");
      $display("      time=%t; instance=%m (NVM1H)",$realtime);
      if (notif_vpp === 1'bx && $time > 0 )
        notif_vpp = 1'b0;
      else
        notif_vpp = ~notif_vpp;
    end
  end
 end
end




and u_chk_enable_sclk ( CHKENSCLK, SME_buf, enable_recovery_checks);
buf u_CHKENSI ( CHKENSI, SME_buf);
and u_chk_enableclksclk ( CHKENCLKSCLK, MElatched,SME_buf);
and u_chk_enablesclkclk ( CHKENSCLKCLK, SME_buf,ME_buf);
and u_CHKENWE_0 ( CHKENWE, ME_buf, ADRFLAGA);
and u_chk_enable_clk ( CHKENCLK, ME_buf, enable_recovery_checks);
and u_CHKENDIN_0 ( CHKENDIN, ME_buf, ADRFLAGA, WE_buf);
buf u_CHKENADR_0 ( CHKENADR, ME_buf );

buf u_CHKENME_0 ( CHKENME, ADRFLAGA );

and u_CHKEN10A_0 ( CHKEN10A, !ME_buf, !SME_buf, !STORE_buf,  !VPP_buf, !RECALL_buf );
and u_CHKEN11A_0 ( CHKEN11A, !ME_buf, !SME_buf, VPP_buf, !COMP_buf, !RECALL_buf );
and u_CHKEN12A_0 ( CHKEN12A, !WE_buf, !SME_buf, !STORE_buf, !VPP_buf, !COMP_buf );
and u_CHKEN13A_0 ( CHKEN13A, SME_buf, !STORE_buf, !VPP_buf, !COMP_buf );
and u_CHKEN14A_0 ( CHKEN14A, !STORE_buf, !VPP_buf, !COMP_buf );

specify
 
    specparam PATHPULSE$OE$Q0 = ( 0, 0.01 );
    specparam PATHPULSE$OE$Q1 = ( 0, 0.01 );
    specparam PATHPULSE$OE$Q2 = ( 0, 0.01 );
    specparam PATHPULSE$OE$Q3 = ( 0, 0.01 );
    specparam PATHPULSE$OE$Q4 = ( 0, 0.01 );
    specparam PATHPULSE$OE$Q5 = ( 0, 0.01 );
    specparam PATHPULSE$OE$Q6 = ( 0, 0.01 );
    specparam PATHPULSE$OE$Q7 = ( 0, 0.01 );
    specparam PATHPULSE$OE$Q8 = ( 0, 0.01 );
    specparam PATHPULSE$OE$Q9 = ( 0, 0.01 );
    specparam PATHPULSE$OE$Q10 = ( 0, 0.01 );
    specparam PATHPULSE$OE$Q11 = ( 0, 0.01 );
    specparam PATHPULSE$OE$Q12 = ( 0, 0.01 );
    specparam PATHPULSE$OE$Q13 = ( 0, 0.01 );
    specparam PATHPULSE$OE$Q14 = ( 0, 0.01 );
    specparam PATHPULSE$OE$Q15 = ( 0, 0.01 );
    specparam PATHPULSE$OE$Q16 = ( 0, 0.01 );
    specparam PATHPULSE$OE$Q17 = ( 0, 0.01 );
    specparam PATHPULSE$OE$Q18 = ( 0, 0.01 );
    specparam PATHPULSE$OE$Q19 = ( 0, 0.01 );
    specparam PATHPULSE$OE$Q20 = ( 0, 0.01 );
    specparam PATHPULSE$OE$Q21 = ( 0, 0.01 );
    specparam PATHPULSE$OE$Q22 = ( 0, 0.01 );
    specparam PATHPULSE$OE$Q23 = ( 0, 0.01 );
    specparam PATHPULSE$OE$Q24 = ( 0, 0.01 );
    specparam PATHPULSE$OE$Q25 = ( 0, 0.01 );
    specparam PATHPULSE$OE$Q26 = ( 0, 0.01 );
    specparam PATHPULSE$OE$Q27 = ( 0, 0.01 );
    specparam PATHPULSE$OE$Q28 = ( 0, 0.01 );
    specparam PATHPULSE$OE$Q29 = ( 0, 0.01 );
    specparam PATHPULSE$OE$Q30 = ( 0, 0.01 );
    specparam PATHPULSE$OE$Q31 = ( 0, 0.01 );
    specparam PATHPULSE$RECALL$RCREADY = ( 0, 0.01 );
    specparam PATHPULSE$COMP$RCREADY = ( 0, 0.01 );
    specparam


`ifdef VIRAGE_FAST
       Tac = 0.62,
       Tcax = 0.00,
       Tdc = 0.81,
       Tcdx = 0.00,
       Twc = 0.98,
       Tcwx = 0.00,
       Tmc = 0.53,
       Tcmx = 0.00,
       Tcc = 8.89,
       Tcl = 0.41,
       Tch = 0.53,
       Tdcs = 0.40,
       Tcdxs = 0.00,
       Tccs = 130.25,
       Tcls = 0.36,
       Tchs = 0.41,
       Tmcs = 0.35,
       Tcmxs = 0.00,
       Trcomp = 150.00,
       Tcomp = 1541.78,
       Trstore = 25000.00,
       Txvpp = 15000.00,
       Tstore = 500000.00,
       Trrecall = 150.00,
       Trecall = 1541.78,
       Trvpp = 7500.00,
       Tvpp = 400000.00,
       Tscomp = 700.00,
       Thcomp = 0.00,
       Tcq = 4.56,
       Tcqx = 0.39,
       Toq = 0.80,
       Toqz = 1.03,
       Tcqs = 128.96,
       Tcqxs = 42.83,
       Tcso = 4.64,
       Tcsox = 1.43,
       Tcmatch = 1387.60,
       Txmatch = 4.79,
       Trcrdy = 1541.78,
       Txrcrdy = 11.45;
`else
       Tac = 0.62,
       Tcax = 0.00,
       Tdc = 0.81,
       Tcdx = 0.00,
       Twc = 0.98,
       Tcwx = 0.00,
       Tmc = 0.53,
       Tcmx = 0.00,
       Tcc = 8.89,
       Tcl = 0.41,
       Tch = 0.53,
       Tdcs = 0.40,
       Tcdxs = 0.00,
       Tccs = 130.25,
       Tcls = 0.36,
       Tchs = 0.41,
       Tmcs = 0.35,
       Tcmxs = 0.00,
       Trcomp = 3000.00,
       Tcomp = 30835.63,
       Trstore = 500000.00,
       Txvpp = 300000.00,
       Tstore = 10000000.00,
       Trrecall = 3000.00,
       Trecall = 30835.63,
       Trvpp = 150000.00,
       Tvpp = 8000000.00,
       Tscomp = 14000.00,
       Thcomp = 0.00,
       Tcq = 4.56,
       Tcqx = 0.39,
       Toq = 0.80,
       Toqz = 1.03,
       Tcqs = 128.96,
       Tcqxs = 42.83,
       Tcso = 4.64,
       Tcsox = 1.43,
       Tcmatch = 27752.06,
       Txmatch = 4.79,
       Trcrdy = 30835.63,
       Txrcrdy = 11.45;
`endif

     if ( OE )
       ( posedge CLK => (  Q0  : 1'bx )) = (  Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq );
     if ( OE )
       ( posedge CLK => (  Q1  : 1'bx )) = (  Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq );
     if ( OE )
       ( posedge CLK => (  Q2  : 1'bx )) = (  Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq );
     if ( OE )
       ( posedge CLK => (  Q3  : 1'bx )) = (  Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq );
     if ( OE )
       ( posedge CLK => (  Q4  : 1'bx )) = (  Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq );
     if ( OE )
       ( posedge CLK => (  Q5  : 1'bx )) = (  Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq );
     if ( OE )
       ( posedge CLK => (  Q6  : 1'bx )) = (  Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq );
     if ( OE )
       ( posedge CLK => (  Q7  : 1'bx )) = (  Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq );
     if ( OE )
       ( posedge CLK => (  Q8  : 1'bx )) = (  Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq );
     if ( OE )
       ( posedge CLK => (  Q9  : 1'bx )) = (  Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq );
     if ( OE )
       ( posedge CLK => (  Q10  : 1'bx )) = (  Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq );
     if ( OE )
       ( posedge CLK => (  Q11  : 1'bx )) = (  Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq );
     if ( OE )
       ( posedge CLK => (  Q12  : 1'bx )) = (  Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq );
     if ( OE )
       ( posedge CLK => (  Q13  : 1'bx )) = (  Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq );
     if ( OE )
       ( posedge CLK => (  Q14  : 1'bx )) = (  Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq );
     if ( OE )
       ( posedge CLK => (  Q15  : 1'bx )) = (  Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq );
     if ( OE )
       ( posedge CLK => (  Q16  : 1'bx )) = (  Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq );
     if ( OE )
       ( posedge CLK => (  Q17  : 1'bx )) = (  Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq );
     if ( OE )
       ( posedge CLK => (  Q18  : 1'bx )) = (  Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq );
     if ( OE )
       ( posedge CLK => (  Q19  : 1'bx )) = (  Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq );
     if ( OE )
       ( posedge CLK => (  Q20  : 1'bx )) = (  Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq );
     if ( OE )
       ( posedge CLK => (  Q21  : 1'bx )) = (  Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq );
     if ( OE )
       ( posedge CLK => (  Q22  : 1'bx )) = (  Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq );
     if ( OE )
       ( posedge CLK => (  Q23  : 1'bx )) = (  Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq );
     if ( OE )
       ( posedge CLK => (  Q24  : 1'bx )) = (  Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq );
     if ( OE )
       ( posedge CLK => (  Q25  : 1'bx )) = (  Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq );
     if ( OE )
       ( posedge CLK => (  Q26  : 1'bx )) = (  Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq );
     if ( OE )
       ( posedge CLK => (  Q27  : 1'bx )) = (  Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq );
     if ( OE )
       ( posedge CLK => (  Q28  : 1'bx )) = (  Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq );
     if ( OE )
       ( posedge CLK => (  Q29  : 1'bx )) = (  Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq );
     if ( OE )
       ( posedge CLK => (  Q30  : 1'bx )) = (  Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq );
     if ( OE )
       ( posedge CLK => (  Q31  : 1'bx )) = (  Tcq, Tcq, Tcqx, Tcq, Tcqx, Tcq );
       ( OE => ( Q0  : 1'bx )) = (  Toq, Toq, Toqz, Toq, Toqz, Toq );
       ( OE => ( Q1  : 1'bx )) = (  Toq, Toq, Toqz, Toq, Toqz, Toq );
       ( OE => ( Q2  : 1'bx )) = (  Toq, Toq, Toqz, Toq, Toqz, Toq );
       ( OE => ( Q3  : 1'bx )) = (  Toq, Toq, Toqz, Toq, Toqz, Toq );
       ( OE => ( Q4  : 1'bx )) = (  Toq, Toq, Toqz, Toq, Toqz, Toq );
       ( OE => ( Q5  : 1'bx )) = (  Toq, Toq, Toqz, Toq, Toqz, Toq );
       ( OE => ( Q6  : 1'bx )) = (  Toq, Toq, Toqz, Toq, Toqz, Toq );
       ( OE => ( Q7  : 1'bx )) = (  Toq, Toq, Toqz, Toq, Toqz, Toq );
       ( OE => ( Q8  : 1'bx )) = (  Toq, Toq, Toqz, Toq, Toqz, Toq );
       ( OE => ( Q9  : 1'bx )) = (  Toq, Toq, Toqz, Toq, Toqz, Toq );
       ( OE => ( Q10  : 1'bx )) = (  Toq, Toq, Toqz, Toq, Toqz, Toq );
       ( OE => ( Q11  : 1'bx )) = (  Toq, Toq, Toqz, Toq, Toqz, Toq );
       ( OE => ( Q12  : 1'bx )) = (  Toq, Toq, Toqz, Toq, Toqz, Toq );
       ( OE => ( Q13  : 1'bx )) = (  Toq, Toq, Toqz, Toq, Toqz, Toq );
       ( OE => ( Q14  : 1'bx )) = (  Toq, Toq, Toqz, Toq, Toqz, Toq );
       ( OE => ( Q15  : 1'bx )) = (  Toq, Toq, Toqz, Toq, Toqz, Toq );
       ( OE => ( Q16  : 1'bx )) = (  Toq, Toq, Toqz, Toq, Toqz, Toq );
       ( OE => ( Q17  : 1'bx )) = (  Toq, Toq, Toqz, Toq, Toqz, Toq );
       ( OE => ( Q18  : 1'bx )) = (  Toq, Toq, Toqz, Toq, Toqz, Toq );
       ( OE => ( Q19  : 1'bx )) = (  Toq, Toq, Toqz, Toq, Toqz, Toq );
       ( OE => ( Q20  : 1'bx )) = (  Toq, Toq, Toqz, Toq, Toqz, Toq );
       ( OE => ( Q21  : 1'bx )) = (  Toq, Toq, Toqz, Toq, Toqz, Toq );
       ( OE => ( Q22  : 1'bx )) = (  Toq, Toq, Toqz, Toq, Toqz, Toq );
       ( OE => ( Q23  : 1'bx )) = (  Toq, Toq, Toqz, Toq, Toqz, Toq );
       ( OE => ( Q24  : 1'bx )) = (  Toq, Toq, Toqz, Toq, Toqz, Toq );
       ( OE => ( Q25  : 1'bx )) = (  Toq, Toq, Toqz, Toq, Toqz, Toq );
       ( OE => ( Q26  : 1'bx )) = (  Toq, Toq, Toqz, Toq, Toqz, Toq );
       ( OE => ( Q27  : 1'bx )) = (  Toq, Toq, Toqz, Toq, Toqz, Toq );
       ( OE => ( Q28  : 1'bx )) = (  Toq, Toq, Toqz, Toq, Toqz, Toq );
       ( OE => ( Q29  : 1'bx )) = (  Toq, Toq, Toqz, Toq, Toqz, Toq );
       ( OE => ( Q30  : 1'bx )) = (  Toq, Toq, Toqz, Toq, Toqz, Toq );
       ( OE => ( Q31  : 1'bx )) = (  Toq, Toq, Toqz, Toq, Toqz, Toq );
     if ( SME  )
       ( posedge SCLK => (  SO  : 1'bx )) = (  Tcqs, Tcqs, Tcqxs, Tcqs, Tcqxs, Tcqs );
       ( posedge CLK => (  SO  : 1'bx )) = (  Tcso, Tcso, Tcsox, Tcso, Tcsox, Tcso );
       ( COMP =>  MATCH ) = (  Tcmatch, Txmatch );
       ( COMP =>  RCREADY ) = (  Trcrdy, Txrcrdy );
       ( RECALL =>  RCREADY ) = (  Trcrdy, Txrcrdy );
// Timing Checks
    $width (negedge CLK, Tcl, 0, notif_clk);
    $width (posedge CLK, Tch, 0, notif_clk);
    $period (posedge CLK, Tcc, notif_clk);
    $period (negedge CLK, Tcc, notif_clk);
    $width (negedge SCLK, Tcls, 0, notif_sclk);
    $width (posedge SCLK, Tchs, 0, notif_sclk);
    $period (posedge SCLK, Tccs, notif_sclk);
    $period (negedge SCLK, Tccs, notif_sclk);
    $width (posedge COMP, Tcomp, 0, notif_comp);
    $width (posedge RECALL, Trecall, 0, notif_recall);
    $hold (posedge CLK  &&& CHKENADR, posedge ADR0, Tcax, notif_adr );
    $hold (posedge CLK  &&& CHKENADR, negedge ADR0, Tcax, notif_adr );
    $hold (posedge CLK  &&& CHKENADR, posedge ADR1, Tcax, notif_adr );
    $hold (posedge CLK  &&& CHKENADR, negedge ADR1, Tcax, notif_adr );
    $hold (posedge CLK  &&& CHKENADR, posedge ADR2, Tcax, notif_adr );
    $hold (posedge CLK  &&& CHKENADR, negedge ADR2, Tcax, notif_adr );
    $hold (posedge CLK  &&& CHKENADR, posedge ADR3, Tcax, notif_adr );
    $hold (posedge CLK  &&& CHKENADR, negedge ADR3, Tcax, notif_adr );
    $hold (posedge CLK  &&& CHKENADR, posedge ADR4, Tcax, notif_adr );
    $hold (posedge CLK  &&& CHKENADR, negedge ADR4, Tcax, notif_adr );
    $hold (posedge CLK  &&& CHKENADR, posedge ADR5, Tcax, notif_adr );
    $hold (posedge CLK  &&& CHKENADR, negedge ADR5, Tcax, notif_adr );
    $hold (posedge CLK  &&& CHKENADR, posedge ADR6, Tcax, notif_adr );
    $hold (posedge CLK  &&& CHKENADR, negedge ADR6, Tcax, notif_adr );
    $setup ( posedge ADR0,posedge CLK  &&& CHKENADR, Tac, notif_adr );
    $setup ( negedge ADR0,posedge CLK  &&& CHKENADR, Tac, notif_adr );
    $setup ( posedge ADR1,posedge CLK  &&& CHKENADR, Tac, notif_adr );
    $setup ( negedge ADR1,posedge CLK  &&& CHKENADR, Tac, notif_adr );
    $setup ( posedge ADR2,posedge CLK  &&& CHKENADR, Tac, notif_adr );
    $setup ( negedge ADR2,posedge CLK  &&& CHKENADR, Tac, notif_adr );
    $setup ( posedge ADR3,posedge CLK  &&& CHKENADR, Tac, notif_adr );
    $setup ( negedge ADR3,posedge CLK  &&& CHKENADR, Tac, notif_adr );
    $setup ( posedge ADR4,posedge CLK  &&& CHKENADR, Tac, notif_adr );
    $setup ( negedge ADR4,posedge CLK  &&& CHKENADR, Tac, notif_adr );
    $setup ( posedge ADR5,posedge CLK  &&& CHKENADR, Tac, notif_adr );
    $setup ( negedge ADR5,posedge CLK  &&& CHKENADR, Tac, notif_adr );
    $setup ( posedge ADR6,posedge CLK  &&& CHKENADR, Tac, notif_adr );
    $setup ( negedge ADR6,posedge CLK  &&& CHKENADR, Tac, notif_adr );
    $hold (posedge CLK  &&& CHKENDIN, posedge DIN0, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, negedge DIN0, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, posedge DIN1, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, negedge DIN1, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, posedge DIN2, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, negedge DIN2, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, posedge DIN3, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, negedge DIN3, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, posedge DIN4, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, negedge DIN4, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, posedge DIN5, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, negedge DIN5, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, posedge DIN6, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, negedge DIN6, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, posedge DIN7, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, negedge DIN7, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, posedge DIN8, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, negedge DIN8, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, posedge DIN9, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, negedge DIN9, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, posedge DIN10, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, negedge DIN10, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, posedge DIN11, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, negedge DIN11, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, posedge DIN12, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, negedge DIN12, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, posedge DIN13, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, negedge DIN13, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, posedge DIN14, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, negedge DIN14, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, posedge DIN15, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, negedge DIN15, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, posedge DIN16, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, negedge DIN16, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, posedge DIN17, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, negedge DIN17, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, posedge DIN18, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, negedge DIN18, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, posedge DIN19, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, negedge DIN19, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, posedge DIN20, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, negedge DIN20, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, posedge DIN21, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, negedge DIN21, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, posedge DIN22, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, negedge DIN22, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, posedge DIN23, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, negedge DIN23, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, posedge DIN24, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, negedge DIN24, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, posedge DIN25, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, negedge DIN25, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, posedge DIN26, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, negedge DIN26, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, posedge DIN27, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, negedge DIN27, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, posedge DIN28, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, negedge DIN28, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, posedge DIN29, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, negedge DIN29, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, posedge DIN30, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, negedge DIN30, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, posedge DIN31, Tcdx, notif_din );
    $hold (posedge CLK  &&& CHKENDIN, negedge DIN31, Tcdx, notif_din );
    $setup ( posedge DIN0,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( negedge DIN0,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( posedge DIN1,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( negedge DIN1,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( posedge DIN2,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( negedge DIN2,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( posedge DIN3,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( negedge DIN3,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( posedge DIN4,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( negedge DIN4,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( posedge DIN5,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( negedge DIN5,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( posedge DIN6,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( negedge DIN6,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( posedge DIN7,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( negedge DIN7,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( posedge DIN8,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( negedge DIN8,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( posedge DIN9,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( negedge DIN9,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( posedge DIN10,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( negedge DIN10,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( posedge DIN11,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( negedge DIN11,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( posedge DIN12,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( negedge DIN12,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( posedge DIN13,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( negedge DIN13,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( posedge DIN14,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( negedge DIN14,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( posedge DIN15,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( negedge DIN15,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( posedge DIN16,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( negedge DIN16,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( posedge DIN17,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( negedge DIN17,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( posedge DIN18,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( negedge DIN18,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( posedge DIN19,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( negedge DIN19,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( posedge DIN20,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( negedge DIN20,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( posedge DIN21,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( negedge DIN21,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( posedge DIN22,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( negedge DIN22,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( posedge DIN23,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( negedge DIN23,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( posedge DIN24,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( negedge DIN24,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( posedge DIN25,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( negedge DIN25,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( posedge DIN26,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( negedge DIN26,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( posedge DIN27,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( negedge DIN27,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( posedge DIN28,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( negedge DIN28,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( posedge DIN29,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( negedge DIN29,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( posedge DIN30,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( negedge DIN30,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( posedge DIN31,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $setup ( negedge DIN31,posedge CLK  &&& CHKENDIN, Tdc, notif_din );
    $hold (posedge CLK  &&& CHKENWE, posedge WE, Tcwx, notif_we );
    $hold (posedge CLK  &&& CHKENWE, negedge WE, Tcwx, notif_we );
    $setup ( posedge WE,posedge CLK  &&& CHKENWE, Twc, notif_we );
    $setup ( negedge WE,posedge CLK  &&& CHKENWE, Twc, notif_we );
    $hold (posedge CLK  &&& CHKENME, posedge ME, Tcmx, notif_me );
    $hold (posedge CLK  &&& CHKENME, negedge ME, Tcmx, notif_me );
    $setup ( posedge ME,posedge CLK  &&& CHKENME, Tmc, notif_me );
    $setup ( negedge ME,posedge CLK  &&& CHKENME, Tmc, notif_me );
    $hold (posedge SCLK  &&& CHKENSI, posedge SI, Tcdxs, notif_si );
    $hold (posedge SCLK  &&& CHKENSI, negedge SI, Tcdxs, notif_si );
    $setup ( posedge SI,posedge SCLK  &&& CHKENSI, Tdcs, notif_si );
    $setup ( negedge SI,posedge SCLK  &&& CHKENSI, Tdcs, notif_si );
    $hold (posedge SCLK , posedge SME, Tcmxs, notif_sme );
    $hold (posedge SCLK , negedge SME, Tcmxs, notif_sme );
    $setup ( posedge SME,posedge SCLK , Tmcs, notif_sme );
    $setup ( negedge SME,posedge SCLK , Tmcs, notif_sme );
    $hold (negedge COMP  &&& enable_setuphold_checks, posedge MRCL0, Thcomp, notif_mrcl0 );
    $hold (negedge COMP  &&& enable_setuphold_checks, negedge MRCL0, Thcomp, notif_mrcl0 );
    $setup ( posedge MRCL0,posedge COMP  &&& enable_setuphold_checks, Tscomp, notif_mrcl0 );
    $setup ( negedge MRCL0,posedge COMP  &&& enable_setuphold_checks, Tscomp, notif_mrcl0 );
    $hold (negedge RECALL  &&& enable_setuphold_checks, posedge MRCL0, Thcomp, notif_mrcl0 );
    $hold (negedge RECALL  &&& enable_setuphold_checks, negedge MRCL0, Thcomp, notif_mrcl0 );
    $setup ( posedge MRCL0,posedge RECALL  &&& enable_setuphold_checks, Tscomp, notif_mrcl0 );
    $setup ( negedge MRCL0,posedge RECALL  &&& enable_setuphold_checks, Tscomp, notif_mrcl0 );
    $hold (negedge COMP  &&& enable_setuphold_checks, posedge MRCL1, Thcomp, notif_mrcl1 );
    $hold (negedge COMP  &&& enable_setuphold_checks, negedge MRCL1, Thcomp, notif_mrcl1 );
    $setup ( posedge MRCL1,posedge COMP  &&& enable_setuphold_checks, Tscomp, notif_mrcl1 );
    $setup ( negedge MRCL1,posedge COMP  &&& enable_setuphold_checks, Tscomp, notif_mrcl1 );
    $hold (negedge RECALL  &&& enable_setuphold_checks, posedge MRCL1, Thcomp, notif_mrcl1 );
    $hold (negedge RECALL  &&& enable_setuphold_checks, negedge MRCL1, Thcomp, notif_mrcl1 );
    $setup ( posedge MRCL1,posedge RECALL  &&& enable_setuphold_checks, Tscomp, notif_mrcl1 );
    $setup ( negedge MRCL1,posedge RECALL  &&& enable_setuphold_checks, Tscomp, notif_mrcl1 );
    $hold (negedge COMP  &&& enable_setuphold_checks, posedge TECC0, Thcomp, notif_tecc0 );
    $hold (negedge COMP  &&& enable_setuphold_checks, negedge TECC0, Thcomp, notif_tecc0 );
    $setup ( posedge TECC0,posedge COMP  &&& enable_setuphold_checks, Tscomp, notif_tecc0 );
    $setup ( negedge TECC0,posedge COMP  &&& enable_setuphold_checks, Tscomp, notif_tecc0 );
    $hold (negedge RECALL  &&& enable_setuphold_checks, posedge TECC0, Thcomp, notif_tecc0 );
    $hold (negedge RECALL  &&& enable_setuphold_checks, negedge TECC0, Thcomp, notif_tecc0 );
    $setup ( posedge TECC0,posedge RECALL  &&& enable_setuphold_checks, Tscomp, notif_tecc0 );
    $setup ( negedge TECC0,posedge RECALL  &&& enable_setuphold_checks, Tscomp, notif_tecc0 );
    $hold (negedge COMP  &&& enable_setuphold_checks, posedge TECC1, Thcomp, notif_tecc1 );
    $hold (negedge COMP  &&& enable_setuphold_checks, negedge TECC1, Thcomp, notif_tecc1 );
    $setup ( posedge TECC1,posedge COMP  &&& enable_setuphold_checks, Tscomp, notif_tecc1 );
    $setup ( negedge TECC1,posedge COMP  &&& enable_setuphold_checks, Tscomp, notif_tecc1 );
    $hold (negedge RECALL  &&& enable_setuphold_checks, posedge TECC1, Thcomp, notif_tecc1 );
    $hold (negedge RECALL  &&& enable_setuphold_checks, negedge TECC1, Thcomp, notif_tecc1 );
    $setup ( posedge TECC1,posedge RECALL  &&& enable_setuphold_checks, Tscomp, notif_tecc1 );
    $setup ( negedge TECC1,posedge RECALL  &&& enable_setuphold_checks, Tscomp, notif_tecc1 );
    $hold (negedge COMP  &&& enable_setuphold_checks, posedge BIAS0, Thcomp, notif_bias0 );
    $hold (negedge COMP  &&& enable_setuphold_checks, negedge BIAS0, Thcomp, notif_bias0 );
    $setup ( posedge BIAS0,posedge COMP  &&& enable_setuphold_checks, Tscomp, notif_bias0 );
    $setup ( negedge BIAS0,posedge COMP  &&& enable_setuphold_checks, Tscomp, notif_bias0 );
    $hold (negedge RECALL  &&& enable_setuphold_checks, posedge BIAS0, Thcomp, notif_bias0 );
    $hold (negedge RECALL  &&& enable_setuphold_checks, negedge BIAS0, Thcomp, notif_bias0 );
    $setup ( posedge BIAS0,posedge RECALL  &&& enable_setuphold_checks, Tscomp, notif_bias0 );
    $setup ( negedge BIAS0,posedge RECALL  &&& enable_setuphold_checks, Tscomp, notif_bias0 );
    $hold (negedge COMP  &&& enable_setuphold_checks, posedge BIAS1, Thcomp, notif_bias1 );
    $hold (negedge COMP  &&& enable_setuphold_checks, negedge BIAS1, Thcomp, notif_bias1 );
    $setup ( posedge BIAS1,posedge COMP  &&& enable_setuphold_checks, Tscomp, notif_bias1 );
    $setup ( negedge BIAS1,posedge COMP  &&& enable_setuphold_checks, Tscomp, notif_bias1 );
    $hold (negedge RECALL  &&& enable_setuphold_checks, posedge BIAS1, Thcomp, notif_bias1 );
    $hold (negedge RECALL  &&& enable_setuphold_checks, negedge BIAS1, Thcomp, notif_bias1 );
    $setup ( posedge BIAS1,posedge RECALL  &&& enable_setuphold_checks, Tscomp, notif_bias1 );
    $setup ( negedge BIAS1,posedge RECALL  &&& enable_setuphold_checks, Tscomp, notif_bias1 );


    $recovery ( posedge CLK ,posedge COMP  &&& CHKENCLK, Tcc, notif_clk_COMP_rise_rec);




    $recovery ( posedge CLK ,posedge SCLK  &&& CHKENSCLKCLK, Tcc, notif_clk_SCLK_rise_rec);




    $recovery ( posedge CLK ,posedge RECALL  &&& CHKENCLK, Tcc, notif_clk_RECALL_rise_rec);




    $recovery ( posedge CLK ,posedge STORE  &&& CHKENCLK, Tcc, notif_clk_STORE_rise_rec);




    $recovery ( posedge SCLK ,posedge COMP  &&& CHKENSCLK, Tccs, notif_sclk_COMP_rise_rec);




    $recovery ( posedge SCLK ,posedge CLK  &&& CHKENCLKSCLK, Tccs, notif_sclk_CLK_rise_rec);




    $recovery ( posedge SCLK ,posedge RECALL  &&& CHKENSCLK, Tccs, notif_sclk_RECALL_rise_rec);




    $recovery ( posedge SCLK ,posedge STORE  &&& CHKENSCLK, Tccs, notif_sclk_STORE_rise_rec);



    $recovery ( negedge COMP  &&& enable_recovery_checks,posedge RECALL , Trcomp, notif_comp_RECALL_rise_rec);

    $recovery ( posedge COMP  &&& enable_recovery_checks,posedge RECALL , Trcomp, notif_comp_RECALL_rise_rec);



    $recovery ( negedge COMP  &&& enable_recovery_checks,posedge STORE , Trcomp, notif_comp_STORE_rise_rec);

    $recovery ( posedge COMP  &&& enable_recovery_checks,posedge STORE , Trcomp, notif_comp_STORE_rise_rec);



    $recovery ( negedge COMP  &&& enable_recovery_checks,posedge SCLK  &&& CHKENSCLK, Trcomp, notif_comp_SCLK_rise_rec);




    $recovery ( negedge COMP  &&& enable_recovery_checks,posedge CLK  &&& CHKENCLK, Trcomp, notif_comp_CLK_rise_rec);




    $recovery ( negedge STORE  &&& enable_recovery_checks,posedge RECALL , Trstore, notif_store_RECALL_rise_rec);





    $recovery ( posedge STORE  &&& enable_recovery_checks,posedge VPP , Txvpp, notif_store_VPP_rise_rec);



    $recovery ( negedge STORE  &&& enable_recovery_checks,posedge COMP , Trstore, notif_store_COMP_rise_rec);




    $recovery ( negedge STORE  &&& enable_recovery_checks,posedge SCLK  &&& CHKENSCLK, Trstore, notif_store_SCLK_rise_rec);




    $recovery ( negedge STORE  &&& enable_recovery_checks,posedge CLK  &&& CHKENCLK, Trstore, notif_store_CLK_rise_rec);




    $recovery ( negedge RECALL  &&& enable_recovery_checks,posedge STORE , Trrecall, notif_recall_STORE_rise_rec);




    $recovery ( negedge RECALL  &&& enable_recovery_checks,posedge COMP , Trrecall, notif_recall_COMP_rise_rec);




    $recovery ( negedge RECALL  &&& enable_recovery_checks,posedge SCLK  &&& CHKENSCLK, Trrecall, notif_recall_SCLK_rise_rec);




    $recovery ( negedge RECALL  &&& enable_recovery_checks,posedge CLK  &&& CHKENCLK, Trrecall, notif_recall_CLK_rise_rec);






    $recovery ( negedge VPP  &&& enable_recovery_checks,negedge STORE , Trvpp, notif_vpp_STORE_fall_rec);


endspecify



generic_behav_nvrm_um18ugs_128x32 #(MES_ALL) uut (  Q_buf, SO_buf, MATCH_buf, RCREADY_buf, ADR_buf, DIN_buf, WE_buf, ME_buf, OE_buf, CLK_buf, SI_buf, SCLK_buf, SME_buf, COMP_buf, STORE_buf, RECALL_buf, VPP_buf, MRCL0_buf, MRCL1_buf, TECC0_buf, TECC1_buf, BIAS0_buf, BIAS1_buf, PU1_buf, PU2_buf, PU3_buf,notif_adr, notif_din, notif_we, notif_me, notif_clk, notif_clk_COMP_rise_rec, notif_clk_SCLK_rise_rec, notif_clk_RECALL_rise_rec, notif_clk_STORE_rise_rec, notif_si, notif_sclk, notif_sclk_COMP_rise_rec, notif_sclk_CLK_rise_rec, notif_sclk_RECALL_rise_rec, notif_sclk_STORE_rise_rec, notif_sme, notif_comp, notif_comp_RECALL_rise_rec, notif_comp_STORE_rise_rec, notif_comp_SCLK_rise_rec, notif_comp_CLK_rise_rec, notif_store, notif_store_RECALL_rise_rec, notif_store_VPP_rise_rec, notif_store_COMP_rise_rec, notif_store_SCLK_rise_rec, notif_store_CLK_rise_rec, notif_recall, notif_recall_STORE_rise_rec, notif_recall_COMP_rise_rec, notif_recall_SCLK_rise_rec, notif_recall_CLK_rise_rec, notif_vpp, 
notif_vpp_STORE_fall_rec, notif_mrcl0, notif_mrcl1, notif_tecc0, notif_tecc1, notif_bias0, notif_bias1 );

and u_OE_0 (OE_eff, OE_buf, 1'b1);

always @( posedge CLK_buf )
 begin
   ADRlatched = ADR_buf;
   DINlatched = DIN_buf;
   WElatched = WE_buf;
   MElatched = ME_buf;
   ADRFLAGA = 1;
 end

always @( posedge SCLK_buf )
 begin
   SMElatched = SME;
 end


endmodule




module generic_behav_nvrm_um18ugs_128x32 (  Q, SO, MATCH, RCREADY, ADR, DIN, WE, ME, OE, CLK, SI, SCLK, SME, COMP, STORE, RECALL, VPP, MRCL0, MRCL1, TECC0, TECC1, BIAS0, BIAS1, PU1, PU2, PU3,notif_adr, notif_din, notif_we, notif_me, notif_clk, notif_clk_COMP_rise_rec, notif_clk_SCLK_rise_rec, notif_clk_RECALL_rise_rec, notif_clk_STORE_rise_rec, notif_si, notif_sclk, notif_sclk_COMP_rise_rec, notif_sclk_CLK_rise_rec, notif_sclk_RECALL_rise_rec, notif_sclk_STORE_rise_rec, notif_sme, notif_comp, notif_comp_RECALL_rise_rec, notif_comp_STORE_rise_rec, notif_comp_SCLK_rise_rec, notif_comp_CLK_rise_rec, notif_store, notif_store_RECALL_rise_rec, notif_store_VPP_rise_rec, notif_store_COMP_rise_rec, notif_store_SCLK_rise_rec, notif_store_CLK_rise_rec, notif_recall, notif_recall_STORE_rise_rec, notif_recall_COMP_rise_rec, notif_recall_SCLK_rise_rec, notif_recall_CLK_rise_rec, notif_vpp, notif_vpp_STORE_fall_rec, notif_mrcl0, notif_mrcl1, notif_tecc0, notif_tecc1, notif_bias0, notif_bias1 );

parameter MES_ALL = "ON";
parameter words = 128, bits = 32, addrbits = 7, timingmode = 1;

output [bits-1:0] Q;
output SO;
output MATCH;
output RCREADY;
input [addrbits-1:0] ADR;
input [bits-1:0] DIN;
input WE;
input ME;
input OE;
input CLK;
input SI;
input SCLK;
input SME;
input COMP;
input STORE;
input RECALL;
input VPP;
input MRCL0;
input MRCL1;
input TECC0;
input TECC1;
input BIAS0;
input BIAS1;
input PU1;
input PU2;
input PU3;
input notif_adr, notif_din, notif_we, notif_me, notif_clk, notif_clk_COMP_rise_rec, notif_clk_SCLK_rise_rec, notif_clk_RECALL_rise_rec, notif_clk_STORE_rise_rec, notif_si, notif_sclk, notif_sclk_COMP_rise_rec, notif_sclk_CLK_rise_rec, notif_sclk_RECALL_rise_rec, notif_sclk_STORE_rise_rec, notif_sme, notif_comp, notif_comp_RECALL_rise_rec, notif_comp_STORE_rise_rec, notif_comp_SCLK_rise_rec, notif_comp_CLK_rise_rec, notif_store, notif_store_RECALL_rise_rec, notif_store_VPP_rise_rec, notif_store_COMP_rise_rec, notif_store_SCLK_rise_rec, notif_store_CLK_rise_rec, notif_recall, notif_recall_STORE_rise_rec, notif_recall_COMP_rise_rec, notif_recall_SCLK_rise_rec, notif_recall_CLK_rise_rec, notif_vpp, notif_vpp_STORE_fall_rec, notif_mrcl0, notif_mrcl1, notif_tecc0, notif_tecc1, notif_bias0, notif_bias1;


parameter DataX = { bits { 1'bx } };
parameter DataZ = { bits { 1'bz } };

reg [bits-1:0] memb [words-1:0];
reg [bits-1:0] earom [words-1:0];

reg [bits-1:0] temp1;
reg [bits-1:0] temp2;
reg [bits-1:0] temp3;
integer store_success_count;



real comp_start_time;
real comp_end_time;
real recall_start_time;
real recall_end_time;
real CLK_T;
reg
        flaga_read_ok,
        flaga_we_ok,
        flaga_d_ok,
        flaga_adr_ok,
        flaga_range_ok;
reg     flaga_adr_viol;
reg     flaga_clk_valid;
reg     flaga_output_ok;
reg     flaga_serial_read_ok;
reg     flaga_si_ok;
reg     flaga_sclk_valid;
reg     flag_keep_match_low ;
reg     rcready_viol_flag ;


reg [bits-1:0] Q;
reg [bits-1:0] QI;

integer i,j;
integer store_ctr;
integer store_ctr_buf;
reg mes_all_valid;

task BlastCell;
    input [bits - 1 : 0] bits; 
    integer i;
begin
    if ($time > 0) begin
      $display("      time=%t; instance=%m (NVM1H)",$realtime);
      $display("      Corrupting the SRAM");
      if (store_ctr != 0) begin
        store_ctr = 0;
      end
      for (i = 0; i < words; i = i + 1)
        memb[i] = memb[i] ^ (bits & DataX);
    end
end
endtask


task corrupt_all_loc;
    input flag_range_ok; 
    integer i;
begin
   if( flag_range_ok == `True) 
    if ($time > 0) 
     if (store_ctr != 0) begin
      store_ctr = 0;
     end
        for (i = 0; i < words; i = i + 1)
            memb[i] <= DataX;
end
endtask

task corrupt_cur_loc;
    input flag_range_ok; 
    input [6:0] ADRlatched;
    integer i;
begin
   if( flag_range_ok == `False) 
    if ($time > 0) 
     if (store_ctr != 0) begin
      store_ctr = 0;
     end
       if ( ADRlatched < words ) 
         memb[ADRlatched] = DataX;
end

endtask


task BlastEarom;
    integer i;
begin
    if ($time > 0) begin
      $display("      time=%t; instance=%m (NVM1H)",$realtime);
      $display("      Corrupting the EAROM");
      if (store_ctr != 0) begin
        store_ctr = 0;
      end
      for (i = 0; i < words; i = i + 1)
        earom[i] = DataX;
    end
end
endtask

reg RECALL_start;
reg SOI;
reg MATCHI;
reg RCREADYI;
reg CLK_old;
reg SCLK_old;
reg STORE_old;
reg RECALL_old;
reg VPP_old;

reg WElatched;
reg WE_old;
reg MElatched;
reg ME_old;
reg [addrbits-1:0] ADRlatched;

reg [addrbits-1:0] ADR_old;

reg [bits-1:0] DINlatched;

reg [bits-1:0] DIN_old;

reg ME_chk;
reg WE_chk;
reg SME_chk;
reg SME_old;
reg SMElatched;





// Perform Sanity Check on Port A, Corrupt memory if required

task checkSanityOnAport;

 #0                // let CLOCK and NOTIFIER stuff execute first
 case ( {flaga_serial_read_ok,flaga_adr_ok, flaga_we_ok, flaga_d_ok} ) // only 1 and 0

  4'b1111   : ;                                                // everything ok!!!

  4'b1101,
  4'b1100   : corrupt_cur_loc( flaga_we_ok,ADRlatched);          // WE is unstable

  4'b1110   : if (WElatched !== 1'b0)
              corrupt_cur_loc( flaga_d_ok,ADRlatched);         // Data is unstable

  4'b1000,
  4'b1001   : corrupt_all_loc(flaga_range_ok);            // ADR and WE unstable

  4'b1010,
  4'b1011   : corrupt_all_loc(flaga_range_ok);

 endcase
endtask


// PORT FUNCTIONALITY FOR VIOLATION BEHAVIOR
initial
 begin
  RECALL_start = 1'b0;
  store_success_count = 1;
  store_ctr = 0;
  store_ctr_buf = 0;
  flaga_adr_ok  = `True;
  flaga_range_ok = `True;
  flaga_we_ok   = `True;
  flaga_d_ok    = `True;
  flaga_read_ok = `True;
  flaga_output_ok = `True;
  flaga_adr_viol = `True;
  flaga_clk_valid = `True;
  flaga_si_ok    = `True;
  flaga_serial_read_ok = `True;
  flaga_sclk_valid = `True;
  flag_keep_match_low = `False;
  mes_all_valid = 1'b0;
end

always @(notif_bias0)            // BIAS0 violation
begin
 if  ( $realtime == recall_start_time )    // For setup violation with RECALL
  BlastCell(DataX);
 else if ( $realtime == comp_start_time ) begin   // For setup violation with COMP
  flag_keep_match_low = `True;
  MATCHI = 1'b0;
 end
 else if (($realtime - recall_end_time ) > ($realtime - comp_end_time)) begin // For hold violation with COMP
  flag_keep_match_low = `True;
  MATCHI = 1'b0;
  end
 else if (($realtime - recall_end_time) < ($realtime - comp_end_time)) // For hold violation with RECALL
  BlastCell(DataX);
end

always @(notif_bias1)            // BIAS1 violation
begin
 if  ( $realtime == recall_start_time )    // For setup violation with RECALL
  BlastCell(DataX);
 else if ( $realtime == comp_start_time ) begin   // For setup violation with COMP
  flag_keep_match_low = `True;
  MATCHI = 1'b0;
 end
 else if (($realtime - recall_end_time ) > ($realtime - comp_end_time)) begin // For hold violation with COMP
  flag_keep_match_low = `True;
  MATCHI = 1'b0;
  end
 else if (($realtime - recall_end_time) < ($realtime - comp_end_time)) // For hold violation with RECALL
  BlastCell(DataX);
end

always @(notif_mrcl0)            // MRCL0 violation
begin
 if  ( $realtime == recall_start_time )    // For setup violation with RECALL
  BlastCell(DataX);
 else if ( $realtime == comp_start_time ) begin   // For setup violation with COMP
  flag_keep_match_low = `True;
  MATCHI = 1'b0;
 end
 else if (($realtime - recall_end_time ) > ($realtime - comp_end_time)) begin // For hold violation with COMP
  flag_keep_match_low = `True;
  MATCHI = 1'b0;
  end
 else if (($realtime - recall_end_time) < ($realtime - comp_end_time)) // For hold violation with RECALL
  BlastCell(DataX);
end

always @(notif_mrcl1)            // MRCL1 violation
begin
 if  ( $realtime == recall_start_time )    // For setup violation with RECALL
  BlastCell(DataX);
 else if ( $realtime == comp_start_time ) begin   // For setup violation with COMP
  flag_keep_match_low = `True;
  MATCHI = 1'b0;
 end
 else if (($realtime - recall_end_time ) > ($realtime - comp_end_time)) begin // For hold violation with COMP
  flag_keep_match_low = `True;
  MATCHI = 1'b0;
  end
 else if (($realtime - recall_end_time) < ($realtime - comp_end_time)) // For hold violation with RECALL
  BlastCell(DataX);
end

always @(notif_tecc0)            // TECC0 violation
begin
 if  ( $realtime == recall_start_time )    // For setup violation with RECALL
  BlastCell(DataX);
 else if ( $realtime == comp_start_time ) begin   // For setup violation with COMP
  flag_keep_match_low = `True;
  MATCHI = 1'b0;
 end
 else if (($realtime - recall_end_time ) > ($realtime - comp_end_time)) begin // For hold violation with COMP
  flag_keep_match_low = `True;
  MATCHI = 1'b0;
  end
 else if (($realtime - recall_end_time) < ($realtime - comp_end_time)) // For hold violation with RECALL
  BlastCell(DataX);
end

always @(notif_tecc1)            // TECC1 violation
begin
 if  ( $realtime == recall_start_time )    // For setup violation with RECALL
  BlastCell(DataX);
 else if ( $realtime == comp_start_time ) begin   // For setup violation with COMP
  flag_keep_match_low = `True;
  MATCHI = 1'b0;
 end
 else if (($realtime - recall_end_time ) > ($realtime - comp_end_time)) begin // For hold violation with COMP
  flag_keep_match_low = `True;
  MATCHI = 1'b0;
  end
 else if (($realtime - recall_end_time) < ($realtime - comp_end_time)) // For hold violation with RECALL
  BlastCell(DataX);
end


always @(notif_recall)            // RECALL violation
begin
      BlastCell(DataX);
      QI <= DataX;
      SOI <= 1'bx;
    flaga_read_ok = `False;
     RCREADYI <= 1'b0;

 end

always @(notif_store)            // STORE violation 
begin
      BlastEarom;
    flaga_read_ok = `False;
 end



always @(notif_vpp)            // pulse width violation of VPP
begin
      BlastEarom;
      disable STORE_OP;
end

always @(notif_sme)            // pin_name(sme,0) violation 
begin
      BlastCell(DataX);
      QI <= DataX;
    flaga_read_ok = `False;
      disable serial_blk;
      SOI <= 1'bx;
    flaga_serial_read_ok = `False;
      checkSanityOnAport;
 end


always @(notif_sclk)            // SCLK violation 
begin
      BlastCell(DataX);
    flaga_serial_read_ok = `False;
      QI <= DataX;
      SOI <= 1'bx;
      checkSanityOnAport;
 end


always @(notif_si)            // SI violation 
begin
      temp3 = memb[0];
      memb[0] = {temp3[bits-1:1],1'bx};
 end






always @(notif_clk_SCLK_rise_rec)            // RECOVERY between posedge SCLK and posedge CLK
begin
      QI <= DataX;
      BlastCell(DataX);
      SOI <= 1'bx;
    flaga_read_ok = `False;
    flaga_serial_read_ok = `False;
 end

always @(notif_sclk_CLK_rise_rec)            // RECOVERY between posedge CLK and posedge SCLK
begin
      BlastCell(DataX);
      SOI <= 1'bx;
      QI <= DataX;
    flaga_read_ok = `False;
    flaga_serial_read_ok = `False;
 end



always @(notif_clk_COMP_rise_rec)            // RECOVERY between posedge CLK and posedge COMP
begin
      BlastCell(DataX);
      SOI <= 1'bx;
      QI <= DataX;

      MATCHI = 1'b0;
      RCREADYI <= 1'b0;
 end

always @(notif_clk_RECALL_rise_rec)            // RECOVERY between posedge CLK and posedge RECALL
begin
      BlastCell(DataX);
      QI <= DataX;
      SOI <= 1'bx;
    flaga_serial_read_ok = `False;
    flaga_read_ok = `False;
    RCREADYI <= 1'b0;
 end

always @(notif_clk_STORE_rise_rec)            // RECOVERY between posedge CLK and posedge STORE
begin
      BlastEarom;
      BlastCell(DataX);
      QI <= DataX;
      SOI <= 1'bx;
      checkSanityOnAport;
    flaga_read_ok = `False;
 end

always @(notif_comp_CLK_rise_rec)            // RECOVERY between negedge COMP and posedge CLK
begin
      MATCHI = 1'b0;
      BlastCell(DataX);
      QI <= DataX;
      SOI <= 1'bx;
    flaga_serial_read_ok = `False;
      RCREADYI <= 1'b0;
 end

always @(notif_store_CLK_rise_rec)            // RECOVERY between negedge STORE and posedge CLK
begin
      BlastCell(DataX);
      QI <= DataX;
      SOI <= 1'bx;
    flaga_serial_read_ok = `False;
 end

always @(notif_recall_CLK_rise_rec)            // RECOVERY between RECALL and posedge CLK
begin
      BlastCell(DataX);
      QI <= DataX;
      disable WriteArrayA.SERIAL_OUT;
      disable serial_blk;
      SOI <= 1'bx;
    flaga_serial_read_ok = `False;
    flaga_read_ok = `False;
    RCREADYI <= 1'b0;
 end



always @(notif_sclk_COMP_rise_rec)            // RECOVERY between posedge SCLK and posedge COMP
begin
      BlastCell(DataX);
      MATCHI = 1'b0;
      SOI <= 1'bx;
      RCREADYI <= 1'b0;
 end

always @(notif_sclk_RECALL_rise_rec)            // RECOVERY between posedge SCLK and posedge RECALL
begin
      BlastCell(DataX);
      QI <= DataX;
      flaga_read_ok = `False;
      SOI <= 1'bx;
      checkSanityOnAport;
      RCREADYI <= 1'b0;

 end

always @(notif_sclk_STORE_rise_rec)            // RECOVERY between posedge SCLK and negedge STORE
begin
    BlastCell(DataX);
      QI <= DataX;
      flaga_read_ok = `False;
      SOI <= 1'bx;
      flaga_serial_read_ok = `False;
 end

always @(notif_comp_SCLK_rise_rec)            // RECOVERY between negedge COMP and posedge SCLK
begin
    MATCHI = 1'b0;
    BlastCell(DataX);
      QI <= DataX;
      flaga_read_ok = `False;
      SOI <= 1'bx;
      flaga_serial_read_ok = `False;
      RCREADYI <= 1'b0;
 end

always @(notif_store_SCLK_rise_rec)            // RECOVERY between negedge STORE and posedge SCLK
begin
      BlastCell(DataX);
      QI <= DataX;
      flaga_read_ok = `False;
      SOI <= 1'bx;
 end

always @(notif_recall_SCLK_rise_rec)            // RECOVERY between negedge RECALL and posedge SCLK
begin
      BlastCell(DataX);
      QI <= DataX;
      flaga_read_ok = `False;
      SOI <= 1'bx;
      RCREADYI <= 1'b0;
 end


always @(notif_comp_RECALL_rise_rec)            // RECOVERY between COMP and RECALL
begin
    RCREADYI = 1'b0;
    rcready_viol_flag = 1'b1;
    disable RECALL_BLK;
    MATCHI = 1'b0;
      BlastCell(DataX);
      QI <= DataX;
      SOI <= 1'bx;

      flaga_read_ok = `False;
      RCREADYI <= 1'b0;
 end

always @(notif_comp_STORE_rise_rec)            // RECOVERY between posedge COMP and posedge STORE
begin
      BlastEarom;
      BlastCell(DataX);
      flaga_read_ok = `False;
    MATCHI = 1'b0;
      SOI <= 1'bx;
     RCREADYI <= 1'b0;
 end


always @(notif_store_RECALL_rise_rec)            // RECOVERY between negedge STORE and posedge RECALL
begin
      BlastCell(DataX);
      QI <= DataX;
      flaga_read_ok = `False;
      SOI <= 1'bx;
     RCREADYI <= 1'b0;
 end

always @(notif_store_COMP_rise_rec)            // RECOVERY between negedge STORE and posedge COMP
begin
      BlastCell(DataX);
      MATCHI = 1'b0;
      SOI <= 1'bx;
      RCREADYI <= 1'b0;
 end

always @(notif_recall_STORE_rise_rec)            // RECOVERY between negedge RECALL and posedge STORE
begin
      BlastCell(DataX);
      BlastEarom;
      QI <= DataX;
      flaga_read_ok = `False;
      SOI <= 1'bx;
      RCREADYI <= 1'b0;
 end

always @(notif_recall_COMP_rise_rec)            // RECOVERY between negedge RECALL and posedge COMP
begin
    RCREADYI = 1'b0;
    rcready_viol_flag = 1'b1;
    disable COMP_BLK;
    MATCHI = 1'b0;
      BlastCell(DataX);
      QI <= DataX;
      flaga_read_ok = `False;
      RCREADYI <= 1'b0;
 end

always @(notif_vpp_STORE_fall_rec)            // RECOVERY between negedge VPP and negedge STORE
begin
    $display("Trvpp violation.POSSIBLE CHIP DAMAGE!! ");
      BlastEarom;
      BlastCell(DataX);
      SOI <= 1'bx;
    flaga_read_ok = `False;
    end

always @(notif_store_VPP_rise_rec)            // RECOVERY between posedge VPP and posedge STORE
begin
    $display("Txvpp violation.POSSIBLE CHIP DAMAGE!! ");
      BlastEarom;
      BlastCell(DataX);
      SOI <= 1'bx;
    flaga_read_ok = `False;
    end




always @(notif_clk)            // CLK violation
 begin
  flaga_clk_valid = `False;
  flaga_output_ok = `False;
  flaga_adr_ok = `False;
  flaga_we_ok  = `False;
   QI <= DataX;
      SOI <= 1'bx;
  checkSanityOnAport;
  flaga_read_ok = `False;     
 end

always @(notif_din)             // D violation
 begin
  if ( $realtime == CLK_T )
   ME_chk = ME;
  else
   ME_chk = MElatched;
  if (ME_chk !== 1'b0)
   begin
    flaga_d_ok = `False;
    flaga_read_ok = `False;
    checkSanityOnAport;
      QI <= DataX;
     if ( ADRlatched == words - 1 && WElatched !== 1'b0) begin
      disable WriteArrayA.SERIAL_OUT;
      disable serial_blk;
      SOI <= 1'bx;
     end 
   end
 end

always @(notif_comp)             // COMP violation
 begin
     MATCHI = 1'b0;
     RCREADYI <= 1'b0;
 end

always @(notif_we)            // WE violation
 begin
  if ( $realtime == CLK_T )
   ME_chk = ME;
  else
   ME_chk = MElatched;
  if (ME_chk !== 1'b0)
   begin
    flaga_we_ok = `False;
    flaga_output_ok = `False;
    flaga_read_ok = `False;
      QI <= DataX;
     if ( ADRlatched == words - 1 ) begin
      disable WriteArrayA.SERIAL_OUT;
      disable serial_blk;
      SOI <= 1'bx;
     end 
    checkSanityOnAport;
   end
 end

always @(notif_me)            // ME violation
 begin
  if ( $realtime == CLK_T )
   WE_chk = WE;
  else
   WE_chk = WElatched;
  if (WE_chk != 1'b0) begin
  flaga_adr_ok = `False;
  end
  flaga_read_ok = `False;     // irrespective of WE
  flaga_output_ok = `False;
  disable OUTPUT;
  QI <= DataX;
     if ( WE_chk !== 1'b0) begin
  SOI <= 1'bx;
     end 
  checkSanityOnAport;
 end

always @(notif_adr )           // ADR violation
 begin
  if ( $realtime == CLK_T )
   ME_chk = ME;
  else
   ME_chk = MElatched;
  if (ME_chk !== 1'b0)
   begin
    flaga_adr_ok = `False;
    flaga_read_ok = `False; // irrespective of WE
    flaga_output_ok = `False;
   QI <= DataX;
      SOI <= 1'bx;
    checkSanityOnAport;
   end
 end


// PORT FUNCTIONALITY
//---- Clock Negative Edge
always @( negedge CLK) begin
   if ( CLK !== 1'bx ) begin     //RESETTING FLAGS FOR NEXT CYCLE
   #0.01;
   flaga_range_ok  = `True;
   flaga_read_ok = `True;
   flaga_adr_viol = `True;
   flaga_clk_valid = `True;
   flaga_output_ok = `True;
  end
  else
   begin
    flaga_clk_valid = `False;
    corrupt_all_loc(`True);
   end
end
//---- Scan Clock Negative Edge
always @( negedge SCLK) begin
   SCLK_old <= SCLK;
   if ( SCLK_old ===1'b1 && SCLK===1'bx) begin
      if( MES_ALL=="ON" && mes_all_valid) begin 
        $display("<<SCLK unknown>>");
        $display("      time=%t; instance=%m (NVM1H)",$realtime);
      end 
      BlastCell(DataX);
      QI = DataX;
      SOI <= 1'bx;
   end
   else 
    begin
   flaga_serial_read_ok = `True;
   flaga_sclk_valid = `True;
    end
end

//---- Scan Clock Positive Edge
always @( posedge SCLK) begin
  #0
   SCLK_old <= SCLK;
// Scan Clock 0-X
   if ( SCLK===1'bx) begin
      if( MES_ALL=="ON" && $realtime != 0 && mes_all_valid) begin
        $display("<<SCLK unknown>>");
        $display("      time=%t; instance=%m (NVM1H)",$realtime);
      end
      BlastCell(DataX);
      QI = DataX;
      SOI <= 1'bx;
   end
// Scan Clock X-1
   else if ( SCLK_old ===1'bx) begin
      QI = DataX;
      SOI <= 1'bx;
   end
   else begin
     SMElatched = SME_old;
      if ( SMElatched == 1'b1 ) begin
       if ( RECALL == 1'b0 && STORE == 1'b0 && COMP == 1'b0 ) begin : serial_blk
         if ( store_ctr != 0) begin
           if ( store_ctr < store_success_count-1) begin
             $display("Attempt to Write was made in the middle of a store operation from serial port , corrupting EAROM. time = %t",$realtime);
             BlastEarom;
           end
           else begin
             store_ctr = 0;
           end
         end

        for ( i = words-1; i>0; i = i-1 ) begin
         temp1 = memb[i] << 1;
         temp2 = memb[i-1];
         memb[i] = {temp1[bits-1:1],temp2[bits-1]};
        end
        temp1 = memb[0] << 1;
        memb[0] = {temp1[bits-1:1],SI};
        SOI <= 1'bx;
        #0.01
        temp1 = memb[words-1];
        SOI = temp1[bits-1];
        if (!mes_all_valid )
          mes_all_valid = 1'b1;
       end
       else begin
          if ( RECALL !== 1'b0 && SMElatched == 1'b1) begin
            if( MES_ALL=="ON" && $realtime != 0) begin
              $display("<<RECALL non-zero at postive edge of SCLK>>");
              $display("      time=%t; instance=%m (NVM1H)",$realtime);
            end
            BlastCell(DataX);
            SOI <= 1'bx;


          end
          if ( STORE !== 1'b0 && SMElatched == 1'b1) begin
            if( MES_ALL=="ON" && $realtime != 0) begin
              $display("<<STORE non-zero at postive edge of SCLK>>");
              $display("      time=%t; instance=%m (NVM1H)",$realtime);
            end
            BlastEarom;
            BlastCell(DataX);
            SOI <= 1'bx;
          end
          if ( COMP !== 1'b0 && SMElatched == 1'b1) begin
            if( MES_ALL=="ON" && $realtime != 0) begin
              $display("<<COMP non-zero at postive edge of SCLK >>");
              $display("      time=%t; instance=%m (NVM1H)",$realtime);
            end
            BlastCell(DataX);
            SOI <= 1'bx;
            MATCHI = 1'b0;
          end
        end
      end
      else if ( SMElatched === 1'bx ) begin
        BlastCell(DataX);
        SOI <= 1'bx;
        if ( STORE !== 1'b0 ) begin
          if( MES_ALL=="ON" && $realtime != 0) begin
            $display("<<STORE non-zero at postive edge of SCLK>>");
            $display("      time=%t; instance=%m (NVM1H)",$realtime);
          end
          BlastEarom;
        end
      end
   end
end
assign SO = SOI;


always @( BIAS0 )
 begin
   if ( RECALL !== 1'b0  ) 
    BlastCell(DataX);
    else if ( COMP !== 1'b0 ) begin 
     flag_keep_match_low = `True;
     MATCHI = 1'b0;
    end
 end

always @( BIAS1 )
 begin
   if ( RECALL !== 1'b0  ) 
    BlastCell(DataX);
    else if ( COMP !== 1'b0 ) begin 
     flag_keep_match_low = `True;
     MATCHI = 1'b0;
    end
 end

always @( TECC0 )
 begin
   if ( RECALL !== 1'b0  ) 
    BlastCell(DataX);
    else if ( COMP !== 1'b0 ) begin 
     flag_keep_match_low = `True;
     MATCHI = 1'b0;
    end
 end
always @( TECC1 )
 begin
   if ( RECALL !== 1'b0  ) 
    BlastCell(DataX);
    else if ( COMP !== 1'b0 ) begin 
     flag_keep_match_low = `True;
     MATCHI = 1'b0;
    end
 end

always @( MRCL0 )
 begin
   if ( RECALL !== 1'b0  ) 
    BlastCell(DataX);
    else if ( COMP !== 1'b0 ) begin 
     flag_keep_match_low = `True;
     MATCHI = 1'b0;
    end
 end
always @( MRCL1 )
 begin
   if ( RECALL !== 1'b0  ) 
    BlastCell(DataX);
    else if ( COMP !== 1'b0 ) begin 
     flag_keep_match_low = `True;
     MATCHI = 1'b0;
    end
 end

always @( negedge CLK or WE )
 begin
   if ( CLK == 1'b0 )
     WE_old = WE;
 end

always @( negedge CLK or ME )
 begin
   if ( CLK == 1'b0 )
     ME_old = ME;
 end

always @( negedge CLK or ADR )
 begin
   if ( CLK == 1'b0 )
     ADR_old = ADR;
 end

always @( negedge CLK or DIN )
 begin
   if ( CLK == 1'b0 )
     DIN_old = DIN;
 end


always @( negedge SCLK or SME)
 begin
   if ( SCLK == 1'b0 )
     SME_old = SME;
 end


task WriteArrayA;
begin
  if ( (ADRlatched ^ ADRlatched) !== 0 ) begin
     BlastCell(DataX);
     if( MES_ALL=="ON" && $realtime != 0 && mes_all_valid) begin
       $display("<<ADR unknown>>");
       $display("      time=%t; instance=%m (NVM1H)",$realtime);
     end
        QI = DataX;
        SOI = 1'bx;
  end
  else if ( ADRlatched < words ) begin
     memb[ADRlatched] = DINlatched;
      QI = DINlatched;
     if ( ADRlatched == words - 1 ) begin : SERIAL_OUT
        SOI <= 1'bx;
        #0.01
       SOI = DINlatched[bits-1];
     end
       if (!mes_all_valid )
       mes_all_valid = 1'b1;
  end
  else if( MES_ALL=="ON" && $realtime != 0 && mes_all_valid) begin
     $display("<<WARNING:address is out of range\n RANGE:0 to 127>>\n");
     $display("      time=%t; instance=%m (NVM1H)",$realtime);
        QI = DINlatched;
  end
end
endtask

task WriteFaultA;
begin
  if ( (ADRlatched ^ ADRlatched) !== 0 ) begin
     BlastCell(DataX);
  end
  else if ( ADRlatched < words ) begin
     memb[ADRlatched] = DataX;
  end

  else if( MES_ALL=="ON" && $realtime != 0 && mes_all_valid) begin
     $display("<<%m WARNING:address is out of range\n RANGE:0 to 127>>\n");
  end
   QI = DataX;
end
endtask


//---- RECALL Negative Edge
always @( negedge RECALL) begin
   recall_end_time = $realtime ;
   if ( RECALL === 1'b0 && rcready_viol_flag !== 1'b1 ) RCREADYI <= RECALL;
    rcready_viol_flag = 1'b0;
   RECALL_old <= RECALL;
   if ( RECALL_old ===1'b1 && RECALL===1'bx) begin
      if( MES_ALL=="ON" ) begin 
        $display("<<RECALL unknown>>");
        $display("      time=%t; instance=%m (NVM1H)",$realtime);
      end 
      BlastCell(DataX);
      QI <= DataX;
      SOI <= 1'bx;
   end
end

always @( posedge RCREADYI) begin
 RECALL_start = 1'b0;
end

//---- RECALL Positive Edge
always @( posedge RECALL) begin : RECALL_BLK
   recall_start_time = $realtime ;
   RECALL_start = 1'b1;
  if ( RECALL === 1'b1 && rcready_viol_flag !== 1'b1 ) RCREADYI <= RECALL;
    rcready_viol_flag = 1'b0;
   RECALL_old <= RECALL;
// RECALL 0-X
   if ( RECALL===1'bx) begin
      if( MES_ALL=="ON" && $realtime != 0) begin
        $display("<<RECALL unknown>>");
        $display("      time=%t; instance=%m (NVM1H)",$realtime);
      end
      BlastCell(DataX);
      RCREADYI <= 1'b0;
      QI <= DataX;
      SOI <= 1'bx;
   end
// RECALL X-1
   else if ( RECALL_old ===1'bx) begin
      QI <= DataX;
      SOI <= 1'bx;
   end
   else if ( COMP != 1'b0) begin
      BlastCell(DataX);
      MATCHI = 1'b0;
      SOI <= 1'bx;
      RCREADYI <= 1'b0;
   end
   else if ( STORE != 1'b0) begin
      BlastCell(DataX);
      BlastEarom;
      SOI <= 1'bx;
      RCREADYI <= 1'b0;
   end
   else begin
      for ( i = words-1; i>=0; i = i-1 ) begin
        memb[i] = earom[i];
      end
      temp1 = memb[words-1];
      SOI <= temp1[bits-1];
   end
end

task store;
  begin
    for ( i = words-1; i>=0; i = i-1 ) begin
      earom[i] = memb[i];
    end
  end
endtask

task recall;
  begin
     if (store_ctr != 0) begin
      store_ctr = 0;
     end
    for ( i = words-1; i>=0; i = i-1 ) begin
      memb[i] = earom[i];
    end
  end
endtask

reg store_start;
reg vpp_start;
reg vpp_end;
//---- STORE Negative Edge
always @( negedge STORE) begin
   STORE_old <= STORE;
// STORE 1-X
   if ( STORE_old ===1'b1 && STORE===1'bx) begin
      if( MES_ALL=="ON" ) begin 
        $display("<<STORE unknown>>");
        $display("      time=%t; instance=%m (NVM1H)",$realtime);
      end 
      BlastEarom;
   end
   else if ( STORE_old ===1'bx) begin
   end
   else begin
      if ( store_start == 1'b1 && vpp_start == 1'b1 && vpp_end == 1'b1 ) begin
         store_start = 1'b0;
         vpp_start = 1'b0;
         vpp_end = 1'b0;
      end
      else begin
          BlastEarom;
        if( MES_ALL=="ON" ) begin 
          $display("<<STORE AND VPP Sequence Wrong>>");
          $display("      time=%t; instance=%m (NVM1H)",$realtime);
        end 
      end
   end
end

//---- STORE Positive Edge
always @( posedge STORE) begin
  STORE_old <= STORE;
// STORE 0-X
    if ( STORE===1'bx) begin
      if( MES_ALL=="ON" && $realtime != 0) begin
        $display("<<STORE unknown>>");
        $display("      time=%t; instance=%m (NVM1H)",$realtime);
      end
      BlastEarom;
    end
// STORE X-1
    else if ( STORE_old ===1'bx) begin
     store_ctr = 0;
    end
    else if (RECALL == 1'b1) begin
      BlastEarom;
      BlastCell(DataX);
      QI <= DataX;
      SOI <= 1'bx;
      RCREADYI <= 1'b0;
    end
    else if (COMP == 1'b1) begin
      BlastEarom;
      BlastCell(DataX);
      QI <= DataX;
      MATCHI = 1'b0;
      SOI <= 1'bx;
      RCREADYI <= 1'b0;
    end
    else begin
      store_start = 1'b1;
    end
end

//---- VPP Negative Edge
always @( negedge VPP) begin
  VPP_old <= VPP;
// VPP 1-X
    if ( VPP_old ===1'b1 && VPP===1'bx) begin
      if( MES_ALL=="ON" ) begin 
        $display("<<VPP unknown>>");
        $display("      time=%t; instance=%m (NVM1H)",$realtime);
      end 
      BlastEarom;
    end
    else if ( VPP_old ===1'bx) begin
    end
    else begin
      if ( store_start == 1'b1 && vpp_start == 1'b1 ) begin : STORE_OP
        vpp_end = 1;
        if ( store_ctr >= store_success_count  - 1) begin
          store;
          store_ctr = store_ctr + 1;
        end
        else begin 
          store_ctr = store_ctr + 1;
          store_ctr_buf = store_ctr;
          BlastEarom;
          store_ctr = store_ctr_buf;
        end
      end
      else begin
        BlastEarom;
        if( MES_ALL=="ON" ) begin 
          $display("<<STORE AND VPP Sequence Wrong>>");
          $display("      time=%t; instance=%m (NVM1H)",$realtime);
        end 
      end
    end
end

//---- VPP Positive Edge
always @( posedge VPP) begin
   VPP_old <= VPP;
      if ( STORE == 1'b0) begin
    $display("VPP is not low when STORE is low.POSSIBLE CHIP DAMAGE!!.Exiting simulation. ");
    $finish;
      end
// VPP 0-X
   else if ( VPP===1'bx) begin
      if( MES_ALL=="ON" && $realtime != 0) begin
        $display("<<VPP unknown>>");
        $display("      time=%t; instance=%m (NVM1H)",$realtime);
      end
      BlastEarom;
      vpp_start = 0;
      vpp_end = 0;
   end
// VPP X-1
   else if ( VPP_old ===1'bx) begin
      vpp_start = 0;
      vpp_end = 0;
   end
    else if (RECALL == 1'b1) begin
      BlastEarom;
      BlastCell(DataX);
      QI <= DataX;
      SOI <= 1'bx;
      RCREADYI <= 1'b0;
    end
    else if (COMP == 1'b1) begin
      BlastEarom;
      BlastCell(DataX);
      QI <= DataX;
      MATCHI = 1'b0;
      SOI <= 1'bx;
      RCREADYI <= 1'b0;
    end
   else if ( store_start == 1'b1 ) begin
      vpp_start = 1;
      vpp_end = 0;
   end
   else begin
      vpp_start = 0;
      vpp_end = 0;
      if( MES_ALL=="ON" && $realtime != 0) begin
        $display("<<VPP is ON, but STORE is OFF>>");
        $display("      time=%t; instance=%m (NVM1H)",$realtime);
      end
      if ( STORE == 1'b0) begin
       BlastCell(DataX);
      end
      BlastEarom;
   end
end

reg temp_comp;
reg COMP_old;

//---- Compare operation between EAROM & SRAM
always @( negedge COMP) begin
   if ( COMP === 1'b0 && rcready_viol_flag !== 1'b1 ) RCREADYI <= COMP;
    rcready_viol_flag = 1'b0;
   comp_end_time = $realtime ;
   COMP_old <= COMP;
   flag_keep_match_low = `False;
   if ( COMP_old ===1'b1 && COMP===1'bx) begin
      if( MES_ALL=="ON" ) begin 
        $display("<<COMP unknown>>");
        $display("      time=%t; instance=%m (NVM1H)",$realtime);
      end 
      MATCHI = 1'b0;
   end
   else
      MATCHI = 1'b0;
end
//---- COMP Positive Edge
always @( posedge COMP) begin : COMP_BLK
   if ( COMP === 1'b1  && rcready_viol_flag !== 1'b1 ) RCREADYI <= COMP;
    rcready_viol_flag = 1'b0;
   comp_start_time = $realtime ;
   COMP_old <= COMP;
// COMP 0-X
   if ( COMP===1'bx) begin
      if( MES_ALL=="ON" && $realtime != 0) begin
        $display("<<COMP unknown>>");
        $display("      time=%t; instance=%m (NVM1H)",$realtime);
      end
      MATCHI = 1'b0;
      RCREADYI <= 1'b0;
   end
// COMP X-1
   else if ( COMP_old ===1'bx) begin
   end
    else if (RECALL == 1'b1) begin
      BlastCell(DataX);
      QI <= DataX;
      MATCHI = 1'b0;
      SOI <= 1'bx;
      RCREADYI <= 1'b0;
    end

   else if ( STORE != 1'b0) begin
      BlastCell(DataX);
      BlastEarom;
      MATCHI = 1'b0;
      SOI <= 1'bx;
      RCREADYI <= 1'b0;
   end
   else if ( flag_keep_match_low ) begin 
    MATCHI = 1'b0;
   end
   else begin
      temp_comp = 1'b1;
      for ( i = 0; i<words; i = i+1 ) begin
        temp_comp = temp_comp&(&(~(earom[i] ^ memb[i])));
        if ( temp_comp == 1'b0 ) i = words;
      end
        if ( temp_comp === 1'bx ) begin
          MATCHI = 1'b0;
        end
        else begin
          MATCHI = temp_comp;
        end
   end
end

assign MATCH = (flag_keep_match_low?0:MATCHI);
assign RCREADY = RCREADYI;
//---- Clock Negative Edge
always @( negedge CLK) begin
   CLK_old <= CLK;
   if ( CLK_old ===1'b1 && CLK===1'bx) begin
      if( MES_ALL=="ON" && mes_all_valid) begin 
        $display("<<CLK unknown>>");
        $display("      time=%t; instance=%m (NVM1H)",$realtime);
      end 
      BlastCell(DataX);
      QI <= DataX;
     SOI <= 1'bx;
   end
end


always @( posedge CLK )
 begin
  #0
  CLK_T = $realtime ;
  flaga_adr_ok  = `True;
   flaga_we_ok   = `True;
   flaga_d_ok    = `True;

   if ( RECALL !== 1'b0 && ME !== 1'b0 && WE !== 1'b0) begin
     if( MES_ALL=="ON" && $realtime != 0) begin
       $display("<<RECALL non-zero at postive edge of CLK >>");
       $display("      time=%t; instance=%m (NVM1H)",$realtime);
     end
     BlastCell(DataX);
     QI <= DataX;
     SOI <= 1'bx;
   end
  else if ( ( RECALL == 1'b0 || (RECALL == 1'b1 && RCREADY == 1'b1))  &&  flaga_clk_valid ) begin
   CLK_old <= CLK;
// Clock 0-X
   if ( CLK===1'bx && mes_all_valid) begin
      if( MES_ALL=="ON" && $realtime != 0) begin
        $display("<<CLK unknown>>");
        $display("      time=%t; instance=%m (NVM1H)",$realtime);
      end
      BlastCell(DataX);
      QI <= DataX;
     SOI <= 1'bx;
   end

// Clock X-1
   else if ( CLK_old ===1'bx) begin
      QI <= DataX;
     SOI <= 1'bx;
   end
   else if ( STORE !== 1'b0 && ME !== 1'b0) begin
     if( MES_ALL=="ON" && $realtime != 0) begin
       $display("<<STORE non-zero at postive edge of CLK>>");
       $display("      time=%t; instance=%m (NVM1H)",$realtime);
     end
     BlastCell(DataX);
     BlastEarom;
     QI <= DataX;
     SOI <= 1'bx;
   end
   else if ( COMP !== 1'b0 && ME !== 1'b0) begin
     if( MES_ALL=="ON" && $realtime != 0) begin
       $display("<<COMP non-zero at postive edge of CLK >>");
       $display("      time=%t; instance=%m (NVM1H)",$realtime);
     end
     BlastCell(DataX);
     QI <= DataX;
     SOI <= 1'bx;
      MATCHI = 1'b0;
   end

// ---- Normal Mode
   else begin : OUTPUT
   #0
     WElatched = WE_old;
     MElatched = ME_old;
     ADRlatched = ADR_old;
     DINlatched = DIN_old;
     if ( MElatched == 1'b1 ) begin
        if ( WElatched !== 1'b0 &&  flaga_read_ok && flaga_we_ok  &&  flaga_adr_ok &&  flaga_d_ok) begin // Write operation
         if ( store_ctr != 0) begin
           if ( store_ctr < store_success_count-1) begin
             $display("Attempt to Write was made in the middle of a store operation from parallel port, corrupting EAROM. time = %t",$realtime);
             BlastEarom;
           end
           else begin
             store_ctr = 0;
           end
         end
          if ( WElatched == 1'b1 && RECALL == 1'b0 ) begin
             WriteArrayA;
          end
          else begin
             if( MES_ALL=="ON" && $realtime != 0  && mes_all_valid) begin
               $display("<<WE unknown>>");
               $display("      time=%t; instance=%m (NVM1H)",$realtime);
             end
             WriteFaultA;
            if ( ADRlatched == words - 1) begin
                SOI <= 1'bx;
            end
          end
        end // End of Write operation
        else begin // Valid read operation
          if ( WElatched == 1'b0 &&  flaga_read_ok &&  flaga_output_ok &&  flaga_adr_ok) begin 
            if ( (ADRlatched ^ ADRlatched) !== 0 ) begin
              if( MES_ALL=="ON" && $realtime != 0 && mes_all_valid) begin
                $display("<<ADR unknown>>");
                $display("      time=%t; instance=%m (NVM1H)",$realtime);
              end
`ifdef virage_ignore_read_addx
`else
                 BlastCell(DataX);
`endif
              QI <= DataX;
              SOI <= 1'bx;
            end
            else if ( ADRlatched < words ) begin
              QI <= DataX;
              #0.01
              QI <= memb[ADRlatched];
            end
            else begin
              if( MES_ALL=="ON" && $realtime != 0 && mes_all_valid) begin
                $display("<<WARNING:address is out of range\n RANGE:0 to 127>>\n");
                $display("      time=%t; instance=%m (NVM1H)",$realtime);
              end
              QI <= DataX;
              flaga_read_ok = `True;
              flaga_output_ok = `True;
            end
          end
          else begin
              QI <= DataX;
              flaga_read_ok = `True;
              flaga_output_ok = `True;
          end
        end // End of Read operation
      end // MElatched = 1'b1
      else if ( MElatched === 1'bx ) begin
        if( MES_ALL=="ON" && $realtime != 0  && mes_all_valid) begin
          $display("<<ME unknown>>");
          $display("      time=%t; instance=%m (NVM1H)",$realtime);
        end
        if ( WElatched !== 1'b0 ) begin
              SOI <= 1'bx;
          QI <= DataX;
          BlastCell(DataX);
        end
        else
          QI <= DataX;
      end // MElatched = 1'bx
    end
   end

 end



always @( OE or QI )
begin
   if ( OE ) begin
     Q <= QI;
   end
   else if ( !OE ) begin
      Q <= DataX;
      #0.01
      Q <= DataZ;
   end
   else begin
      if( MES_ALL=="ON" && $realtime != 0  && mes_all_valid) begin
         $display("<<OE unknown>>");
         $display("      time=%t; instance=%m (NVM1H)",$realtime);
      end
      Q <= DataX;
   end
end



endmodule
`endcelldefine
