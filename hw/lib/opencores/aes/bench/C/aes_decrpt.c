#include <stdio.h>
#include <aes_api.h>
#include <stdlib.h>

static AesCipherInstance cipher;
static AesKeyInstance keyI;

void output_128bits(FILE *f, unsigned char *prefix, unsigned char *s)
{
    int i;
  
    if (prefix != NULL) 
        fprintf(f, "%s", prefix);

    for (i=0; i<16; i++) 
        fprintf(f, "%02x ", s[i]);

    fprintf(f, "\n");
}

int main(int argc, char **argv)
{
    unsigned char IV[16], key[16];   /* 128(16x8) bits */
    unsigned char text_in[16], text_out[16];
    int seed, count, i, j;
    FILE *f;

    if (argc != 4) {
        printf("Usage: aes_decrpt seed howmany_vectors output_file\n"); 
        return 0;
    }

    seed = atoi(argv[1]);
    count = atoi(argv[2]);
    printf("Random seed = %d # of vectors=%d \n", seed, count);
    f = fopen(argv[3], "wb");
    if (f == NULL) {
        printf("Cannot create file %s \n", argv[2]);
        return -1;
    }
    
    srand(seed);     
    for (i=0; i<16; i++) {
        IV[i]  = (unsigned char) (rand() & 0xff);
        key[i] = (unsigned char) (rand() & 0xff);
    }

        /* 128 bits key */
    if(!aesMakeKey(&keyI, AES_DIR_DECRYPT, 128, key)) {
        printf("Canot create Ase key\n");
        fclose(f);
        return -2; 
    }

    if (!aesCipherInit(&cipher, AES_MODE_CBC, IV)) {
        printf("Cannot init aes Cipher\n");
        fclose(f);
        return -3;
    } 

    output_128bits(f, "Key:", key);
    output_128bits(f, "IV:", IV);    

    for (i=0; i<count; i++) {
        for (j=0; j<16; j++) {
            text_in[j] = (unsigned char) (rand() & 0xff);
            text_out[j] = '\0';
        }
            
        aesBlockDecrypt(&cipher, &keyI, text_in, 128, text_out);
        output_128bits(f, "In:", text_in);
        output_128bits(f, "Out:", text_out);
    }
    
    fclose(f);
    return 0;     
} 
