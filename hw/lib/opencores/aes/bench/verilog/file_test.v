//
// :set tabstop=4
//

`define MAX_TEXT_IN   (1024*1024)

reg[127:0] textf_in[0:`MAX_TEXT_IN];
reg[127:0] bb_text_in;
wire[127:0] bb_text_out;
reg  bb_ld;
reg  bb_kld;
wire bb_kdone;
wire bb_done;
reg [127:0] bb_key[0:1];
reg [127:0] bb_iv[0:1];

aes_inv_cipher_top bb_u(
	.clk(clk),
	.rst(rst),
	.kld(bb_kld),
	.kdone(bb_kdone),
	.ld(bb_ld),
	.done(bb_done),
	.key(bb_key[0]),
	.text_in(bb_text_in),
	.text_out(bb_text_out)
);

task file_test;
	integer f, wh;
	begin
		$readmemh("aes.in",  textf_in);	
		$readmemh("aes.key", bb_key);
		$readmemh("aes.iv", bb_iv);
		
		n = 0;

		f = $fopen("aes.out");
		wh = 0;
		$display("key = %x", bb_key[0]);
		bb_kld = 0;
		bb_ld = 0;
		
		// load key
		@(posedge clk);
		#1;
		bb_kld = 1;
		@(posedge clk);
		#1;
		bb_kld = 0;
		while (!bb_kdone) @(posedge clk);

		while (1) begin
			if ( (|textf_in[wh]) === 1'bx ) begin
				$fclose(f);
				$finish;
			end

			bb_text_in = textf_in[wh];
			wh = wh + 1;

			@(posedge clk);
			#1;
			bb_ld = 1;
			@(posedge clk);
			#1;
			bb_ld = 0;
			while (!bb_done) @(posedge clk);			

			$fdisplay(f, "%x", bb_text_out ^ bb_iv[0]);
			bb_iv[0] = bb_text_in;
		end
	end
endtask
