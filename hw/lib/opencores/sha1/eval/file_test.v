// SHA1 file tests
//
// :set tabstop=4
// Read random data from file data.sha
//     Fisrt word:   random seed 
//     second word:  run how many times     
//     

`define MAX_SHA_LEN            16384
`define MAX_FILE_SIZE          (16384*2 + 8)

reg [7:0] content[0:`MAX_FILE_SIZE];
reg [7:0] data[0:63];
reg [31:0] seed;
reg whs_debug;

initial begin
	whs_debug = ($test$plusargs("debug")) ? 1 : 0;
	$display("Debug is %d", whs_debug);
end 

task file_test;
	reg [31:0] loop;
	reg random_stall;
	reg init_cv;
	integer i, j, where, len, delta, f, k;
	begin
		$readmemh("data.sha", content);
		seed = {content[0], content[1], content[2], content[3]};
		loop = {content[4], content[5], content[6], content[7]};

		$display("%T %M Info: seed = %d loop count = %d", 
				$time, seed, loop);
		seed = $random(seed);
		f = $fopen("sha.sha");

		for (i=0; i<loop; i=i+1) begin
			where = $random(seed) & 32'h3fff;
			len = $random(seed) & 32'h3fff;	
			where = where + 8;
			if (len == 0) len = 1;
			random_stall = $random(seed);

			$display("%T Info: %d where = %d, len = %d   random_stall=%b",
					$time, i, where, len, random_stall);
			cv = 160'h67452301EFCDAB8998BADCFE10325476C3D2E1F0;
			for (j=0; j<len; j=j+delta) begin
				if ((len - j) <= 55) begin
					for (k=0; k<len-j; k=k+1) data[k] = content[where+j+k];
					data[len-j] = 8'h80;
					for (k=len-j+1; k<56; k=k+1) data[k] = 8'h0;
					{data[56], data[57], data[58], data[59], 
					 data[60], data[61], data[62], data[63]} = len*8;
					sha1_one_block((j==0) ? 1'b0 : 1'b1, random_stall);
					delta = len - j;
				end else begin
					if ((len -j) > 64) begin 
						for (k=0; k<64; k=k+1) data[k] = content[where+j+k];
						sha1_one_block((j==0) ? 1'b0 : 1'b1, random_stall);		
						delta = 64;
					end else begin
						for (k=0; k<len-j; k=k+1) data[k] = content[where+j+k];
						if ((len -j) < 64) begin
							data[len-j] = 8'h80;
							for (k=len-j+1; k<64; k=k+1) data[k] = 8'h0;
						end
						sha1_one_block((j==0) ? 1'b0 : 1'b1, random_stall); 
						data[0] = ((len -j) < 64) ? 8'h0 : 8'h80;
						for (k=1; k<56; k=k+1) data[k] = 8'h0;
						{data[56], data[57], data[58], data[59],
						 data[60], data[61], data[62], data[63]} = len*8;
						sha1_one_block(1'b1, random_stall);
						delta = len -j;
					end
				end
				init_cv = 0;
			end

			// output data
			$fdisplay(f, "%d %d %d", i, where, len);
			$fdisplay(f, "%8x", cv_next[159:128]);
			$fdisplay(f, "%8x", cv_next[127:96]);
			$fdisplay(f, "%8x", cv_next[95:64]);
			$fdisplay(f, "%8x", cv_next[63:32]);
			$fdisplay(f, "%8x", cv_next[31:0]);
		end
        $fclose(f);
	end
endtask

task sha1_one_block;
	input use_previous_cv;
	input random_stall;
	begin
		y = {data[0],  data[1],  data[2], data[3], data[4], data[5], data[6], data[7],
			 data[8],  data[9],  data[10], data[11], data[12], data[13], data[14], data[15],
			 data[16], data[17], data[18], data[19], data[20], data[21], data[22], data[23],
			 data[24], data[25], data[26], data[27], data[28], data[29], data[30], data[31],
			 data[32], data[33], data[34], data[35], data[36], data[37], data[38], data[39],
			 data[40], data[41], data[42], data[43], data[44], data[45], data[46], data[47],
			 data[48], data[49], data[50], data[51], data[52], data[53], data[54], data[55],
			 data[56], data[57], data[58], data[59], data[60], data[61], data[62], data[63]};

		if (whs_debug) 
			$display("sha1 %x", y);

			// sha1 this block
		if (random_stall)
			my_inputdata(y, $random(seed) & 32'h7);
		else 
			inputdata(y);
		start = 1;
		use_prev_cv = use_previous_cv;
		@(posedge clk);
		start = 0;
		while (busy == 1'd1) begin
			@(posedge clk);
		end
		@(posedge clk);
	end
endtask

task my_inputdata;
  input [511:0] y;
  input [31:0] random_delay;
  integer i;

  begin
    for (i=0; i<=15; i=i+1)
      begin
        data_in <= (y >> 512-((i+1)*32));
        load_in <= 1'b1;
        // $display("%8x", data_in);
        @(posedge clk) ;

	load_in <= 1'b0;
	repeat(random_delay) @(posedge clk); 
      end
    load_in = 1'b0;
  end
endtask

