/* 
     Create random data file for SHA1 test
     File format
         1st word :  random seed
         2nd word :  test loop count
 */

#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#define  MAX_SHA_LEN  16384*2

int main(int argc, char ** argv)
{
    FILE *f;
    int seed, i, loop;

    if (argc != 4) {
        printf("Usage: randfile rand_seed test_loop randfile\n");
        return -1;
    }
  
    loop = atoi(argv[2]);
    if (loop <= 0) {
        printf("Error: Invalid test loop\n");
        return -2;
    }

    seed = atoi(argv[1]);
    printf("Info: Random seed = %d\n", seed);
    srand(seed);
    if ((f = fopen(argv[3], "wb")) == NULL) {
        printf("Error: Cannot create random file");
        return -3;
    }

    i = htonl(seed);
    fwrite(&i, sizeof(int), 1, f);
    i = htonl(loop);
    fwrite(&i, sizeof(int), 1, f);
    for (i=0; i<MAX_SHA_LEN; i++) 
	fputc(rand() & 0xff, f);

    fclose(f);
    return 0;
}
