
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <getopt.h>
#include  <sha1.h>

int main(int argc, char **argv)
{
    FILE *fIn=NULL, *fOut=NULL;
    unsigned char *s=NULL, hash[20];
    int ret = -1, len;
    SHA1Context sha;

    if (argc != 3) {
        printf("Usage: sha1 input_file output_hash_file\n");
        goto out;
    } 

    if ((fIn = fopen(argv[1], "rb")) == NULL) {
        printf("Error: Cannot read input file");
        goto out;
    }

    if ((fOut = fopen(argv[2], "wb")) == NULL) {
        printf("Error: Cannot write hash file"); 
        goto out;
    }

    fseek(fIn, 0, SEEK_END);
    len = ftell(fIn);
    fseek(fIn, 0, SEEK_SET);
    if (len <= 0) {
        printf("Error: Invalid file size");
        ret = -2;
        goto out;
    }

    s = (unsigned char *) malloc(sizeof(char) * len);
    if (s == NULL) {
        printf("Error: Cannot alloc memory");
        ret = -3;
        goto out;
    }

    fread(s, sizeof(char), len, fIn);
    memset(hash, 0, sizeof(hash));
   
    SHA1Reset(&sha);
    SHA1Input(&sha, s, len);
    SHA1Result(&sha, hash);

    fwrite(hash, sizeof(hash), 1, fOut);
    ret = 0;
out:
    if (s) free(s);
    if (fIn != NULL) fclose(fIn);
    if (fOut != NULL) fclose(fOut);
    return ret;
}



