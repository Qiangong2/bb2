// ais_host.v Frank Berndt
// AIS host model;
// :set tabstop=4

`timescale 1ns/1ns

module ais_host (
	reset, ais_clk, ais_lr, ais_d
);
	input reset;			// interface reset;
	output ais_clk;			// ais clock;
	output ais_lr;			// left/right signal;
	input ais_d;			// audio bit stream;

	// interface signals;
	// keep in reset on simulator start;

	reg ais_clk;			// bus reset;
	reg ais_lr;				// l/r signal;
	reg ais_rst;			// reset ais, abort stream;
	integer period;			// clock period in ns;

	// audio clock is always running;

	initial
	begin
		$display("%M: ais host model");
		period = 651;
		ais_clk = 0;
		ais_lr = 1'b0;
		ais_rst = 0;
	end

	always
		#(period / 2) ais_clk = ~ais_clk;

	// monitor changes in period;
	// 250ns is fastest gi can handle;

	task set;
		input fast;				// set fast clock;
		begin
			period = fast? 250 : 651;
		end
	endtask

	always @(period)
		$display("%t: %M: period=%0dns", $time, period);

	// reset task;
	// guarantees minimum reset time;

	task rst;
		begin
			$display("%t: %M", $time);
			ais_lr = 0;
			ais_rst = 1;
			#1;
			ais_rst = 0;
			#1;
		end
	endtask

	// dv accessible variables;

	reg [31:0] samples [0:16*1024-1];	// sample buffer;

	// get sample;

	task get;
		input [13:0] off;			// sample offset;
		output [31:0] word;			// word;
		begin
			word = samples[off];
		end
	endtask

	// put sample;
	// will be compared to incoming sample;

	task put;
		input [13:0] off;			// sample offset;
		input [31:0] word;			// word;
		begin
			samples[off] = word;
		end
	endtask

	// start streaming audio;
	// samples[] should have been loaded with expected data;

	integer nsamples;				// # of expected samples;
	reg skip;						// skip initial 0 samples;
	reg stream;						// start streaming;

	initial
		stream = 0;

	task start;
		input [15:0] ns;			// # of samples;
		input skip0;				// skip 0s;
		begin
			$display("%t: %M: n=%0d skip=%b", $time, ns, skip0);
			nsamples = ns;
			skip = skip0;
			stream = 1;
		end
	endtask

	// check end of stream;

	task check;
		begin
			if(stream !== 1'b0)
				$display("ERROR: %t: %M: stream=%x exp 0", $time, stream);
		end
	endtask

	// streaming loop;

	always @(posedge stream)
	begin : loop
		integer n, bit;
		reg [31:0] sample, exp;
		reg s0;

		@(posedge ais_clk);
		n = 0;
		while(n < nsamples) begin
			for(bit = 0; bit < 32; bit = bit + 1) begin
				if(bit == 0)
					ais_lr = 1;
				else if(bit == 16)
					ais_lr = 0;
				@(posedge ais_clk or posedge ais_rst);
				if(ais_rst == 1) begin
					bit = 32;
					nsamples = 0;
				end
				sample = { sample[30:0], ais_d };
			end
			s0 = skip & (sample === 32'd0);
			if( !s0 & !ais_rst) begin
				skip = 0;
				exp = samples[n];
				if(sample !== exp) begin
					$display("ERROR: %t: %M: sample[%0d]=0x%h exp 0x%h",
						$time, n, sample, exp);
				end
				n = n + 1;
			end
		end
		ais_lr = 0;
		ais_rst = 0;
		stream = 0;
	end

	// report important changes;

	always @(ais_rst)
		$display("%t: %M: ais_rst=%b", $time, ais_rst);

	always @(stream)
		$display("%t: %M: stream=%b", $time, stream);

endmodule
