
module sha_test;

	initial $display("SHA test start");

	`include "sha1_task.v"

	reg [159:0] state;
	reg [511:0] data;
	reg [159:0] exp_sha;
	
	integer i;

	initial begin
		# 10;
		$display("Test SHA1 tasks");

		data[511:384] = 128'h67c6697351ff4aec29cdbaabf2fbe346;
		data[383:256] = 128'h7cc254f81be8e78d765a2e63339fc99a;
		data[255:128] = 128'h66320db73158a35a255d051758e95ed4;
		data[127:0] =   128'habb2cdc69bb454110e827441213ddc87;

		exp_sha = 160'h4df2160f4316adbe9061cfdf334d41d1064d9952;
		sha1_task(state, data, 1);
		if (state !== exp_sha) 
			$display("Error 0");

		data[511:384] = 128'h70e93ea141e1fc673e017e97eadc6b96;
		data[383:256] = 128'h8f385c2aecb03bfb32af3c54ec18db5c;
		data[255:128] = 128'h021afe43fbfaaa3afb29d1e6053c7c94;
		data[127:0] =   128'h75d8be6189f95cbba8990f95b1ebf1b3;
		sha1_task(state, data, 0);		

		$display("0x%x", state);
		exp_sha = 160'h466214c8c39824db1ef9d47b9ca618ca886c9898;
		if (state !== exp_sha) 
			$display("Error 0");
		else
			$display("SHA = 0x%x", state);
	end
endmodule
