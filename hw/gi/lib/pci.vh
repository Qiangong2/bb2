// pci.vh
// pci definitions;

`define	PCI_INTR_ACK		4'b0000		// intr acknowledge;
`define	PCI_SPECIAL			4'b0001		// special cycle;
`define	PCI_IO_READ			4'b0010		// io read;
`define	PCI_IO_WRITE		4'b0011		// io write;
`define	PCI_RSVD_4			4'b0100		// reserved;
`define	PCI_RSVD_5			4'b0101		// reserved;
`define	PCI_MEM_READ		4'b0110		// memory read;
`define	PCI_MEM_WRITE		4'b0111		// memory write;
`define	PCI_RSVD_8			4'b1000		// reserved;
`define	PCI_RSVD_9			4'b1001		// reserved;
`define	PCI_CONF_READ		4'b1010		// config read;
`define	PCI_CONF_WRITE		4'b1011		// config write;
`define	PCI_MEM_READ_MULT	4'b1100		// memory read multiple;
`define	PCI_DUAL_ADDR		4'b1101		// dual address cycle;
`define	PCI_MEM_READ_LINE	4'b1110		// memory read line;
`define	PCI_MEM_WRITE_INV	4'b1111		// memory write and invalidate;

// pci requestors;

`define	REQ_GI				3'd0		// GI;
`define	REQ_1				3'd1		// requestor 1;
`define	REQ_2				3'd2		// requestor 2;
`define	REQ_3				3'd3		// requestor 3;
`define	REQ_HOST			3'd4		// hsot bridge;
`define	N_REQ				4			// max # of external requestors;

// setup parameters;

`define	HOST_IRDY_STALLS	4'd0		// set requestor priority, value[1:0];
`define	HOST_ADDR_PERR		4'd1		// force host address parity errors;
`define	HOST_DATA_PAR		4'd2		// use data parity from blk buffer, else calculate;
`define	REQ_PRIORITY		4'd8		// set requestor priority, value[1:0];
`define	REQ_BE_TARGET		4'd9		// host response as target, value[0];
`define	REQ_TARGET_CMDS		4'd10		// cmds for which host will respond, value[15:0];
`define	REQ_TARGET_ABORT	4'd11		// how host will abort as target, value[2:0];
`define	REQ_TRDY_STALLS		4'd12		// host randomly stalls as target, value[0];
`define	REQ_MAX_BURST		4'd13		// max # of words per burst before target-abort;

// target abort modes;

`define	TABORT_NONE			3'd0		// normal data operations;
`define	TABORT_RETRY		3'd1		// target retry;
`define	TABORT_COMPL		3'd2		// target completion;
`define	TABORT_NORM			3'd3		// target abort, no data;
`define	TABORT_DD1			3'd8		// disconnect with data 1;
`define	TABORT_DD2			3'd9		// disconnect with data 2;
`define	TABORT_DN1			3'd10		// disconnect without data 1;
`define	TABORT_DN2			3'd11		// disconnect without data 2;

