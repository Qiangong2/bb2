// pci_mon.v Frank Berndt
// pci bus monitor;
// :set tabstop=4

`timescale 1ns/1ns

module pci_mon (
	bus,
	clk, rst,
	ad, cbe, par,
	frame, devsel, irdy, trdy, stop, lock,
	perr, serr, intr,
	req, gnt
);
	input bus;				// bi-directional bus monitor;
	input clk;				// pci clock;
	input rst;				// bus reset;

	input [31:0] ad;		// addr/data bus;
	input [3:0] cbe;		// cmd/byte enables;
	input par;				// parity;
	input frame;			// frame signal;
	input devsel;			// device selected;
	input irdy;				// initiator ready;
	input trdy;				// target ready;
	input stop;				// target stop;
	input lock;				// ressource lock;
	input perr;				// parity error;
	input serr;				// system error;
	input [3:0] intr;		// interrupts;

	input [3:0] req;		// master requests;
	input [3:0] gnt;		// master grants;

	// pci monitor on/off;

	reg mon_pci;

	initial
	begin
		$display("%M: bus=%b", bus);
		mon_pci = $test$plusargs("mon_pci");
	end

	// pci bus command map;
	// bitmap of allowed commands per initiator;
	// 0..3 for each gnt, 4 for host;

	reg [15:0] pci_cmds [0:4];

	initial
	begin
		#1;
		cmd_mask(0, 16'b0);
		cmd_mask(1, 16'b0);
		cmd_mask(2, 16'b0);
		cmd_mask(3, 16'b0);
		cmd_mask(4, 16'b0000_1100_1100_1100);
	end

	// set allowed cmd mask per master;
	// 0..3 for each gnt, 4 for host;

	task cmd_mask;
		input [2:0] master;
		input [15:0] mask;
		begin
			pci_cmds[master] = mask;
			if(mon_pci)
				$display("%t: %M: master=%0d, mask=%b", $time, master, mask);
		end
	endtask

	// max latencies from req to gnt per requestor;
	// in pci clocks;

	reg [31:0] max_req_gnt [0:3];

	initial
	begin
		max_req_gnt[0] = 1000;
		max_req_gnt[1] = 1000;
		max_req_gnt[2] = 1000;
		max_req_gnt[3] = 1000;
	end

	// make for addr/data parity errors;
	// set to 0 if error should be disabled;

	reg addr_par_error;
	reg data_par_error;

	initial
	begin
		addr_par_error = 1;
		data_par_error = 1;
	end

	// monitor pci reset;
	// measure pci clock at deassertion of bus reset;

	always @(rst)
	begin
		if(rst === 1'bx)
			$display("ERROR: %t: %M: rst=%b", $time, rst);
		else
			$display("%t: %M: rst=%b", $time, rst);
	end

	// measure pci clock period;

	reg [31:0] time_clk;

	always @(posedge clk)
	begin
		if(rst == 1'b0)
			time_clk = $time;
		else if((rst == 1'b1) && (time_clk > 0)) begin
			$display("%t: %M: pci clk period %0dns", $time, $time - time_clk);
			time_clk <= 0;
		end
	end

	// monitor frame;
	// determine frame start;
	// frame should never go X outside of reset;

	reg frame_del;			// delayed frame;
	wire frame_start;		// start of frame;

	assign frame_start = rst & ~frame & frame_del;

	always @(posedge clk)
	begin
		frame_del <= frame;
		if((rst == 1'b1) &(frame !== 1'b0) & (frame !== 1'b1))
			$display("ERROR: %t: %M: frame=%b", $time, frame);
	end

	// check read turn-around cycle;

	reg read_ta;			// read turn-around;

	always @(posedge clk)
	begin
		read_ta <= rst & frame_start & pci_func.is_read_cmd(cbe);
		if(read_ta) begin
			if(trdy !== 1'b1)
				$display("ERROR: %t: %M: trdy=%b at read turn-around", $time, trdy);
		end
	end

	// monitor devsel;
	// delay in pipe to detect when target is ready;
	// devsel should be deasserted when bus is idle;

	wire bus_busy;
	reg [4:0] devsel_pipe;	// device select pipe;

	assign bus_busy = ~frame | ~irdy;

	always @(posedge clk)
	begin
		devsel_pipe <= {5{rst}} & { devsel_pipe[3:0], devsel };
		if(rst & (devsel !== 1'b0) & (devsel !== 1'b1))
			$display("ERROR: %t: %M: devsel=%b", $time, devsel);
		if(frame_start & (devsel !== 1'b1))
			$display("ERROR: %t: %M: devsel=%b at frame start", $time, devsel);
		if(rst & (bus_busy !== 1'b1) & (devsel !== 1'b1))
			$display("ERROR: %t: %M: devsel=%b when bus idle", $time, devsel);
	end

	// bus should be busy as long as devsel is active;
	// ie. frame or irdy must be asserted;

	always @(posedge clk)
	begin
		if(rst & ~devsel & (bus_busy !== 1'b1)) begin
			$display("ERROR: %t: %M: broken busy, frame=%b, irdy=%b",
				$time, frame, irdy);
		end
	end

	// target should not drive trdy and stop
	// outside of devsel envelope;

	always @(posedge clk)
	begin
		if(rst & devsel) begin
			if(trdy !== 1'b1)
				$display("ERROR: %t: %M: trdy=%b outside devsel", $time, trdy);
			if(stop !== 1'b1)
				$display("ERROR: %t: %M: stop=%b outside devsel", $time, stop);
		end
	end

	// monitor device requests;
	// complain if requestor has not seen grant within a resonable amount of time;

	reg [3:0] req_pend;				// pending requests;
	reg [31:0] req_gnt [0:3];		// # of clocks since request;

	always @(posedge clk)
	begin : req_chk
		integer n;

		req_pend = {4{rst}} & gnt & ~req;
		for(n = 0; n < 4; n = n + 1) begin
			req_gnt[n] = req_pend[n]? (req_gnt[n] + 1) : 32'd0;
			if(req_gnt[n] == max_req_gnt[n]) begin
				$display("ERROR: %t: %M: req[%0d] violated max gnt latency %0d clks",
					$time, n, max_req_gnt[n]);
			end
		end
	end

	// detect new transaction;
	// check 

	reg tstart;				// start of new transaction;
	reg [31:0] taddr;		// transaction address;
	reg [3:0] tcmd;			// transaction cmd;
	reg tread;				// is a read command;
	reg [3:0] tgnt;			// transaction master;
	reg [2:0] tmst;			// current master;
	reg [31:0] tdata;		// latched data;
	reg [3:0] tcbe;			// latched cbe;
	reg tirdy;				// latched irdy;
	reg ttrdy;				// latched trdy;

	always @(posedge clk)
	begin
		if(frame_start) begin
			taddr <= ad;
			tcmd <= cbe;
			tread <= pci_func.is_read_cmd(cbe);
			tgnt <= gnt;
			tmst <= pci_func.master_index(gnt);
			trans(ad, cbe, gnt);
		end
		tstart <= frame_start;
		tdata <= ad;
		tcbe <= cbe;
		tirdy <= irdy;
		ttrdy <= trdy;
	end

	// check data and parity;
	// address parity can be ERROR if addr_par_error is set;
	// check data parity for all irdy phases;
	// data parity can be ERROR id data_par_error is set;

	always @(posedge clk)
	begin : data_check
		reg dx;

		if(rst == 1'b1) begin

			// address phase errors;

			if(tstart) begin
				dx = ^{ taddr, tcmd };
				if(par != dx) begin
					if(addr_par_error)
						$display("ERROR: %t: %M: addr parity error, par %b exp %b", $time, par, dx);
					else if(mon_pci)
						$display("%t: %M: addr parity error, par %b exp %b", $time, par, dx);
				end
			end

			// check initiator/target data;
			// flag Xs during data phases;
			// check data parity one clock later;

			if((~tread & (tirdy === 1'b0)) | (ttrdy === 1'b0)) begin
				if(mon_pci)
					$display("%t: %M: it=%b ad=%h cbe=%b", $time, { tirdy, ttrdy }, tdata, tcbe);
				dx = ^{ tdata, tcbe };
				if((dx === 1'bx) | (dx === 1'bz))
					$display("ERROR: %t: %M: x/z it=%b ad=%b cbe=%b", $time, { tirdy, ttrdy }, tdata, tcbe);
				if(par != dx) begin
					if(data_par_error)
						$display("ERROR: %t: %M: data parity error, par %b exp %b", $time, par, dx);
					else if(mon_pci)
						$display("%t: %M: data parity error, par %b exp %b", $time, par, dx);
				end
			end
		end
	end

	// check start of transaction;

	task trans;
		input [31:0] ad;
		input [3:0] cbe;
		input [3:0] gnt;
		reg [15:0] cmds;			// valid cmd mask;
		reg cmd_val;				// valid cmd;
		begin
			// addr, cmd, par should not be undefined;

			if(^ad === 1'bx)
				$display("ERROR: %t: %M: ad=%h", $time, ad);
			if(^cbe === 1'bx)
				$display("ERROR: %t: %M: cbe=%b", $time, cbe);

			// this is turn-around for irdy, trdy, stop, and devsel;

			if(devsel !== 1'b1)
				$display("ERROR: %t: %M: devsel=%b exp 1", $time, devsel);
			if(irdy !== 1'b1)
				$display("ERROR: %t: %M: irdy=%b exp 1", $time, irdy);
			if(trdy !== 1'b1)
				$display("ERROR: %t: %M: trdy=%b exp 1", $time, trdy);
			if(stop !== 1'b1)
				$display("ERROR: %t: %M: stop=%b exp 1", $time, stop);

			// check if cmd is legal for this master;

			cmds = pci_cmds[pci_func.master_index(gnt)];
			cmd_val = cmds[cbe];
			if(mon_pci) begin
				$display("%t: %M: gnt=%b addr %h cmd=%b (%s)",
					$time, gnt, ad, cbe, pci_func.cmd_str(cbe));
			end
			if( !cmd_val)
				$display("ERROR: %t: %M: unexpected cmd=%b for gnt=%b", $time, cbe, gnt);
		end
	endtask

	// monitor bus master's grants;
	// only one active at any given clock;

	always @(posedge clk)
	begin : check_gnt
		reg bad_gnt;

		bad_gnt = rst & (^gnt === 1'bx);
		case(gnt)
			'b1111, 'b1110, 'b1101, 'b1011, 'b0111: ;
			default: bad_gnt = 1;
		endcase
		if(bad_gnt)
			$display("ERROR: %t: %M: gnt=%b", $time, gnt);
	end

	// report interrupt status;

	always @(intr)
	begin
		if(mon_pci)
			$display("%t: %M: intr=%b", $time, intr);
	end

endmodule

