// pci_func.v Frank Berndt
// modulized for ALI build method;

module pci_func;

// return 1 for read commands;

function is_read_cmd;
	input [3:0] cmd;
	begin
		case(cmd)
			'b0010,						// io read;
			'b0110,						// mem read;
			'b1010,						// config read;
			'b1100,						// mem read multiple;
			'b1110:	is_read_cmd = 1;	// mem read line;
			default: is_read_cmd = 0;
		endcase
	end
endfunction

// map master to grant index;

function [2:0] master_index;
	input [3:0] gnt;
	begin
		case(gnt)
			4'b1111: master_index = 4;
			4'b1110: master_index = 0;
			4'b1101: master_index = 1;
			4'b1011: master_index = 2;
			4'b0111: master_index = 3;
			default: begin
				master_index = 3'bx;
				$display("ERROR: %t: %M: illegal gnt=%b", $time, gnt);
			end
		endcase
	end
endfunction

// map cmd to string;

function [9*8:0] cmd_str;
	input [3:0] cmd;
	begin
		case(cmd)
			'b0000: cmd_str = "intr-ack ";
			'b0001: cmd_str = "special  ";
			'b0010: cmd_str = "io read  ";
			'b0011: cmd_str = "io write ";
			'b0100: cmd_str = "reserved ";
			'b0101: cmd_str = "reserved ";
			'b0110: cmd_str = "mem-read ";
			'b0111: cmd_str = "mem-write";
			'b1000: cmd_str = "reserved ";
			'b1001: cmd_str = "reserved ";
			'b1010: cmd_str = "cfg-read ";
			'b1010: cmd_str = "cfg-read ";
			'b1100: cmd_str = "mem-rmult";
			'b1101: cmd_str = "dual-addr";
			'b1110: cmd_str = "mem-rline";
			'b1111: cmd_str = "mem-wrinv";
			default: cmd_str = "?????????";
		endcase
	end
endfunction

endmodule

