// bi_sys.v Frank Berndt
// model for GI bi bus interface;
// :set tabstop=4

module bi_sys (
	clk, reset,
	sys_addr, sys_cmd, sys_out, sys_in, sys_intr,
	dma_req, dma_bx, dma_addr, dma_wr, dma_done, dma_retry, dma_err
);
	input clk;				// core clock;
	input reset;			// system reset;

	// interface to gi;

	output [31:0] sys_addr;		// cpu address;
	output [2:0] sys_cmd;		// bus command;
	output [31:0] sys_out;		// write data, cpu and dma;
	input [31:0] sys_in;		// read data. cpu and dma;
	input sys_intr;				// system interrupt;
	input dma_req;				// request a dma transfer;
	input [1:0] dma_bx;			// burst control;
	input [31:0] dma_addr;		// dma physical address;
	input dma_wr;				// dma write to memory;
	output dma_done;			// dma done;
	output dma_retry;			// retry dma;
	output [2:0] dma_err;		// dma errors;

	// cpu cacheline;
	// data exchange for block reads/writes;
	// should be set to maximum block size;

	reg [31:0] blk [0:`BSIZE_MAX-1];

	// main memory;
	// accessible from cpu and gi dma;
	// 1MB physical, mirrowed to 256MB;

	reg [31:0] mem [0:256*1024-1];

	// dma issue delay;
	// must be in the range 0...15;
	// report changes in the log;

	reg [3:0] dma_issue_delay;

	initial
		dma_issue_delay = 6;

	always @(dma_issue_delay)
		$display("%t: %M: dma_issue_delay=%0d", $time, dma_issue_delay);

	// interface;

	reg [31:0] sys_addr;
	reg [2:0] sys_cmd;
	reg [31:0] sys_out;
	reg [31:0] sys_wdel;		// write data delay;
	reg [31:0] sys_rd;			// latched read data;
	reg no_stall;				// stalls are illegal;

	// system address;

	always @(posedge clk)
	begin
		if(reset) begin
			sys_addr <= 32'bx;
			sys_cmd <= 3'b000;
			sys_out <= 32'bx;
			sys_wdel <= 32'bx;
			no_stall <= 0;
		end
		sys_rd <= sys_in;
	end

	// delay dma request;
	// used to simulate initial issue latency;

	reg [15:0] dma_iss;			// dma issue pipe;

	always @(posedge clk)
	begin
		if(reset | ~dma_req)
			dma_iss <= 'd0;
		else
			dma_iss <= { dma_iss[14:0], dma_req };
	end

	// bus arbiter;
	// cpu or gi dma can request cycles;
	// if a single request is pending, it will win;
	// if both are pending, then pick a random winner;

	reg cpu_req;				// cpu request;
	wire [1:0] req;				// bus requestors;
	reg [1:0] win;				// arbitration winner;
	reg [1:0] wdel;				// win delayed by one clock;
	reg rwin;					// random winner;
	wire busy;					// has a winner;

	always @(posedge clk)
	begin
		if(reset) begin
			cpu_req <= 1'b0;
			win <= 2'b0;
		end
	end

	assign req[0] = cpu_req;
	assign req[1] = dma_req & dma_iss[dma_issue_delay];
	assign busy = |(win & req);

	always @(negedge clk)
	begin
		rwin <= $random;
		if(reset) begin
			win <= 2'b0;
			wdel <= 2'b0;
		end else if(~busy) begin
			case(req)
				2'b00, 2'b01, 2'b10:
					win <= req;
				2'b11: begin
					rwin = $random;
					win <= { rwin, ~rwin };
				end
				default: begin
					$display("ERROR: %t: %M: req=%b", $time, req);
					win <= 2'b00;
				end
			endcase
		end
		sys_out <= sys_wdel;
		wdel <= win;
	end

	assign dma_done = ~win[1] & wdel[1];

	// read data capture;

	reg [2:0] rpipe [0:4];		// cmd delay pipe;
	integer blk_off;			// offset in burst;
	reg [31:0] dma_mem;			// dma memory address;

	always @(posedge clk)
	begin
		if(reset) begin
			blk_off <= 'bx;
			rpipe[1] <= 0;
			rpipe[2] <= 0;
			rpipe[3] <= 0;
			rpipe[4] <= 0;
		end else begin
			rpipe[1] <= sys_cmd;
			rpipe[2] <= rpipe[1];
			rpipe[3] <= rpipe[2];
			rpipe[4] <= rpipe[3];
		end
		if(rpipe[4] === 3'b001) begin
			blk[blk_off] <= sys_in;
			blk_off <= blk_off + 1;
		end else if(rpipe[4] === 3'b101) begin
			mem_swrite(dma_mem, sys_in);
			dma_mem = dma_mem + 4;
		end
	end

	// the gi access tasks must be single-threaded;

	task gi_thread;
		input lock;
		begin
			if(lock !== 1'b0) begin
				$display("ERROR: %t: %M: re-entry", $time);
				$finish;
			end
		end
	endtask

	// single write to gi;

	task gi_swrite;
		input [31:0] addr;
		input [31:0] wdata;
		begin
			gi_thread(cpu_req);			// panic on multi-thread;
			cpu_req <= 1;
			@(posedge win[0]);
			no_stall <= 1;
			sys_addr <= addr;
			sys_cmd <= 3'b010;			// write;
			sys_wdel <= wdata;
			@(negedge clk);
			sys_addr <= 32'bx;
			sys_cmd <= 3'b000;
			sys_wdel <= 32'bx;
			@(negedge clk);
			cpu_req <= 0;
			win[0] <= 0;
			@(posedge clk);
		end
	endtask

	// single read from gi;

	task gi_sread;
		input [31:0] addr;
		output [31:0] rdata;
		begin
			gi_thread(cpu_req);			// panic on multi-thread;
			cpu_req <= 1;
			@(posedge win[0]);
			no_stall <= 0;
			sys_addr <= addr;
			sys_cmd <= 3'b001;			// read;
			sys_wdel <= 32'bx;
			@(negedge clk);
			sys_addr <= 32'bx;
			sys_cmd <= 3'b000;
			@(negedge clk);
			@(negedge clk);
			@(negedge clk);
			@(negedge clk);
			cpu_req <= 0;
			win[0] <= 0;
			@(posedge clk);
			rdata = sys_rd;				// read data;
		end
	endtask

	// block write to gi;
	// block writes are sequential;

	task gi_bwrite;
		input [31:0] addr;
		input [3:0] bsize;				// size in words;
		integer n;
		begin
			gi_thread(cpu_req);			// panic on multi-thread;
			cpu_req <= 1;				// request interface;
			@(posedge win[0]);			// wait until won;
			no_stall <= 1;
			for(n = 0; n < bsize; n = n + 1) begin
				sys_addr <= addr ^ (n << 2);
				sys_cmd <= 3'b010;		// issue cycle;
				sys_wdel <= blk[n];
				@(negedge clk);
				sys_addr <= 32'bx;
				sys_cmd <= 3'b000;		// simulate 2 idles for pci;
				sys_wdel <= 32'bx;
				@(negedge clk);
				@(negedge clk);
			end
			cpu_req <= 0;
			win[0] <= 0;
			@(posedge clk);
		end
	endtask

	// block read from gi;
	// stall should never be active;

	task gi_bread;
		input [31:0] addr;
		input [3:0] bsize;				// size in words;
		integer n;
		begin
			gi_thread(cpu_req);			// panic on multi-thread;
			cpu_req <= 1;				// request interface;
			@(posedge win[0]);			// wait until won;
			no_stall <= 1;
			blk_off <= 0;
			for(n = 0; n < bsize; n = n + 1) begin
				sys_addr <= addr ^ (n << 2);
				sys_cmd <= 3'b001;
				sys_wdel <= 32'bx;
				@(negedge clk);
				sys_addr <= 32'bx;
				sys_cmd <= 3'b000;		// simulate 2 idles for pci;
				@(negedge clk);
				@(negedge clk);
			end
			@(negedge clk);				// wait for read pipe;
			@(negedge clk);				// wait for read pipe;
			cpu_req <= 0;
			win[0] <= 0;
			@(posedge clk);
		end
	endtask

	// dma between memory and gi;
	// dma size is fixed at 64 or 128 bytes;
	// dma_addr/dma_wr must not change during dma transaction;
	// sys_addr does not matter during dma, bi is controlling it;

	always @(posedge win[1])
	begin : dma
		reg [31:0] data;				// dma write data;
		reg wr;							// write to memory;
		reg addr_err;					// address error;
		integer n;
		integer nburst;

		$display("%t: %M: mem=0x%h wr=%b b%0d(%b)",
			$time, dma_addr, dma_wr, dma_bx[0]? 128 : 64, dma_bx[1]);
		dma_mem = dma_addr;
		wr = dma_wr;
		addr_err = (dma_mem[5:0] !== 'd0)
			| (dma_bx[0] & (dma_mem[6] !== 1'b0));
		if(addr_err) begin
			$display("ERROR: %t: %M: dma_addr=0x%h/%b",
				$time, dma_mem, dma_mem);
		end
		no_stall <= 1;
		nburst = dma_bx[0]? 128/4 : 64/4;
		for(n = 0; n < nburst; n = n + 1) begin
			sys_addr <= { 25'bx, n[4] | dma_bx[1], n[3:0], 2'bx };
			sys_cmd <= { 1'b1, ~wr, wr };
			data = 32'bx;
			if(wr == 1'b0) begin
				mem_sread(dma_mem, data);
				dma_mem = dma_mem + 4;
			end
			sys_wdel <= data;
			@(negedge clk);
			sys_addr <= 32'bx;
			sys_cmd <= 3'b000;			// simulate 2 idles for pci;
			sys_wdel <= 32'bx;
			@(negedge clk);
			@(negedge clk);
		end
		win[1] <= 0;
		@(posedge clk);
	end

	// write single word to memory;
	// wrap the 1MB physical memory;
	// can be multi-threaded;

	task mem_swrite;
		input [31:0] addr;
		input [31:0] wdata;
		begin
			addr[31:20] = 0;
			addr = addr >> 2;
			mem[addr] = wdata;
		end
	endtask

	// read single word from memory;
	// wrap the 1MB physical memory;
	// can be multi-threaded;

	task mem_sread;
		input [31:0] addr;
		output [31:0] rdata;
		begin
			addr[31:20] = 0;
			addr = addr >> 2;
			rdata = mem[addr];
		end
	endtask

	// write block to memory;
	// wrap the 1MB physical memory;
	// cannot be multi-threaded due to usage of blk;

	task mem_bwrite;
		input [31:0] addr;
		input [3:0] bsize;				// size in words;
		integer n;
		begin
			addr[31:20] = 0;
			addr[4:2] = addr[4:2] & ~(bsize - 1);
			addr = addr >> 2;
			for(n = 0; n < bsize; n = n + 1)
				mem[addr ^ n] = blk[n];
		end
	endtask

	// read block from memory;
	// wrap the 1MB physical memory;
	// cannot be multi-threaded due to usage of blk;

	task mem_bread;
		input [31:0] addr;
		input [3:0] bsize;				// size in words;
		integer n;
		begin
			addr[31:20] = 0;
			addr = addr >> 2;
			for(n = 0; n < bsize; n = n + 1)
				blk[n] = mem[addr ^ n];
		end
	endtask

	// single write;

	task swrite;
		input [31:0] addr;
		input [31:0] wdata;
		begin
			if(reset !== 1'b0)
				$display("ERROR: %t: %M: reset=%b", $time, reset);
			else if(addr[1:0] !== 2'b0)
				$display("ERROR: %t: %M: addr[1:0]=%b", $time, addr[1:0]);
			else if(addr[31:28] === 4'h0)
				mem_swrite(addr, wdata);
			else if(addr[31:20] === 12'h1fc)
				gi_swrite(addr, wdata);
			else
				$display("ERROR: %t: %M: addr=0x%h (%b)", $time, addr, addr);
		end
	endtask

	// single read;

	task sread;
		input [31:0] addr;
		output [31:0] rdata;
		begin
			if(reset !== 1'b0)
				$display("ERROR: %t: %M: reset=%b", $time, reset);
			else if(addr[1:0] !== 2'b0)
				$display("ERROR: %t: %M: addr[1:0]=%b", $time, addr[1:0]);
			else if(addr[31:28] === 4'h0)
				mem_sread(addr, rdata);
			else if(addr[31:20] === 12'h1fc)
				gi_sread(addr, rdata);
			else
				$display("ERROR: %t: %M: addr=0x%h (%b)", $time, addr, addr);
		end
	endtask

	// block write;
	// block writes are sequential;

	task bwrite;
		input [31:0] addr;
		input [3:0] bsize;				// size in words;
		reg [4:0] waddr;				// write block address;
		begin
			waddr[4:2] = addr[4:2] & (bsize - 1);
			waddr[1:0] = addr[1:0];
			if(reset !== 1'b0)
				$display("ERROR: %t: %M: reset=%b", $time, reset);
			else if(waddr !== 5'b0)
				$display("ERROR: %t: %M: addr[4:0]=%b", $time, addr[4:0]);
			else if(addr[31:28] === 4'h0)
				mem_bwrite(addr, bsize);
			else if(addr[31:20] === 12'h1fc)
				gi_bwrite(addr, bsize);
			else
				$display("ERROR: %t: %M: addr=0x%h (%b)", $time, addr, addr);
		end
	endtask

	// block read;
	// return data in critical-word order;

	task bread;
		input [31:0] addr;
		input [3:0] bsize;				// size in words;
		begin
			if(reset !== 1'b0)
				$display("ERROR: %t: %M: reset=%b", $time, reset);
			else if(addr[1:0] !== 5'b0)
				$display("ERROR: %t: %M: addr[1:0]=%b", $time, addr[1:0]);
			else if(addr[31:28] === 4'h0)
				mem_bread(addr, bsize);
			else if(addr[31:20] === 12'h1fc)
				gi_bread(addr, bsize);
			else
				$display("ERROR: %t: %M: addr=0x%h (%b)", $time, addr, addr);
		end
	endtask

	// XXX system interrupt;

	assign dma_retry = 1'b0;
	assign dma_err = 3'b0;

endmodule

