// pci_host.v Frank Berndt
// pci host model;
// :set tabstop=4

`timescale 1ns/1ns

module pci_host (
	clk, rst,
	ad_in, ad_out, ad_oe,
	cbe_in, cbe_out, cbe_oe,
	par_in, par_out, par_oe,
	frame_in, frame_out, frame_oe,
	devsel_in, devsel_out, devsel_oe,
	irdy_in, irdy_out, irdy_oe,
	trdy_in, trdy_out, trdy_oe,
	stop_in, stop_out, stop_oe,
	lock_in, lock_out, lock_oe,
	perr_in, perr_out, perr_oe,
	serr_in,
	intr, req, gnt
);
	input clk;				// pci clock;
	input rst;				// bus reset;

	input [31:0] ad_in;		// addr/data inputs;
	output [31:0] ad_out;	// addr/data outputs;
	output ad_oe;			// addr/data output enable;
	input [3:0] cbe_in;		// cmd/byte enables;
	output [3:0] cbe_out;	// cmd/byte enables;
	output cbe_oe;			// cmd/byte output enable;
	input par_in;			// parity in;
	output par_out;			// parity out;
	output par_oe;			// parity output enable;
	input frame_in;			// frame in;
	output frame_out;		// frame out;
	output frame_oe;		// frame output enable;
	input devsel_in;		// devsel in;
	output devsel_out;		// devsel out;
	output devsel_oe;		// devsel output enable;
	input irdy_in;			// irdy in;
	output irdy_out;		// irdy out;
	output irdy_oe;			// irdy output enable;
	input trdy_in;			// trdy in;
	output trdy_out;		// trdy out;
	output trdy_oe;			// trdy output enable;
	input stop_in;			// stop in;
	output stop_out;		// stop out;
	output stop_oe;			// stop output enable;
	input lock_in;			// lock in;
	output lock_out;		// lock out;
	output lock_oe;			// lock output enable;
	input perr_in;			// parity error in;
	output perr_out;		// parity error out;
	output perr_oe;			// parity error output enable;
	input serr_in;			// system error in;

	input [3:0] intr;		// interrupts;
	input [3:0] req;		// master requests;
	output [3:0] gnt;		// master grants;

	// report my presense;

	initial
		$display("%M: pci host model");

	// cpu data buffer;
	// [31:0] are data bits;
	// [35:32] are byte enables;
	// [36] is parity bit;
	// cpu writes take data from this buffer;
	// cpu reads put data into this buffer;
	// see all_cbe below;

	reg [36:0] blk [0:127];

	// main memory;
	// accessible from cpu and gi dma;
	// 1MB physical, mirrowed to 256MB;

	reg [31:0] mem [0:256*1024-1];

	// pci dv environment;
	// report changes in options;

	reg irdy_stalls;				// random irdy stalls when master;
	reg addr_perr;					// force host addr parity error;
	reg data_par;					// use data parity from blk buffer, else calculate;
	reg [3:0] trdy_stalls;			// random trdy stalls when target;
	reg all_cbe;					// force all cbe active;
	reg [1:0] arb_pri [0:3];		// requestor arbitration priorities;
	reg [3:0] be_target;			// host bridge is target, to test master-abort;
	reg [2:0] target_abort [0:3];	// force target abort mode;
	reg [15:0] target_cmds [0:3];	// cmds to accept, not target when all 0;
	reg [9:0] max_burst [0:3];		// max # of words per burst;

	initial
	begin : init
		integer n;

		#1;
		setup('bx, `HOST_IRDY_STALLS, 1);
		setup('bx, `HOST_ADDR_PERR, 0);
		setup('bx, `HOST_DATA_PAR, 0);
		all_cbe = 1;
		for(n = 0; n < 4; n = n + 1) begin
			setup(n, `REQ_PRIORITY, 0);
			setup(n, `REQ_BE_TARGET, 0);
			setup(n, `REQ_TARGET_ABORT, `TABORT_NONE);
			setup(n, `REQ_TRDY_STALLS, 1);
			setup(n, `REQ_TARGET_CMDS, 0);
			setup(n, `REQ_MAX_BURST, 0);
		end
	end

	always @(all_cbe)
		$display("%t: %M: all_cbe=%b", $time, all_cbe);

	// setup pci system;

	task setup;
		input [2:0] req;		// requestor id;
		input [3:0] param;		// parameter;
		input [15:0] value;		// parameter value;
		reg [12*8:0] str;
		begin
			case(param)
				`HOST_IRDY_STALLS: begin
					irdy_stalls = value[0];
					str = " irdy_stalls";
				end
				`HOST_ADDR_PERR: begin
					addr_perr = value[0];
					str = "   addr_perr";
				end
				`HOST_DATA_PAR: begin
					data_par = value[0];
					str = "    data_par";
				end
				`REQ_PRIORITY: begin
					arb_pri[req] = value[1:0];
					str = "     arb_pri";
				end
				`REQ_BE_TARGET: begin
					be_target[req] = value[0];
					str = "   be_target";
				end
				`REQ_TARGET_CMDS: begin
					target_cmds[req] = value[15:0];
					str = " target_cmds";
				end
				`REQ_TARGET_ABORT: begin
					target_abort[req] = value[2:0];
					str = "target_abort";
				end
				`REQ_TRDY_STALLS: begin
					trdy_stalls[req] = value[0];
					str = " trdy_stalls";
				end
				`REQ_MAX_BURST: begin
					max_burst[req] = value[9:0];
					str = "   max_burst";
				end
				default: begin
					$display("ERROR: %t: %M: param %b", $time, param);
					str = "xxxxxxxxxxxx";
				end
			endcase
			$display("%t: %M: req[%0d] %s=%h", $time, req, str, value);
		end
	endtask

	// bus registers;

	reg [31:0] ad_ir, ad_out;
	reg ad_oe;
	reg [3:0] cbe_ir, cbe_out;
	reg cbe_oe;
	reg par_ir, par_out, par_buf, par_oe;
	reg frame_ir, frame_out, frame_oe;
	reg devsel_ir, devsel_out, devsel_oe;
	reg irdy_ir, irdy_out, irdy_oe;
	reg trdy_ir, trdy_out, trdy_oe;
	reg stop_ir, stop_out, stop_oe;

	always @(posedge clk)
	begin
		ad_ir <= ad_in;
		cbe_ir <= cbe_in;
		par_ir <= par_in;
		frame_ir <= frame_in;
		devsel_ir <= devsel_in;
		irdy_ir <= irdy_in;
		trdy_ir <= trdy_in;
		stop_ir <= stop_in;
		par_out <= par_buf;
		par_oe <= ad_oe;
	end

	// often used data valid;

	reg data_val;

	always @(posedge clk)
	begin
		data_val <= ~irdy_in & ~trdy_in;
	end

	// pci bus arbiter;
	// selects one winner from all pci requestors;
	// allowed to change at any clock;
	// randomly decide when to take away grant with regards to frame;
	// randomly decide if cpu should win, if dma pending;
	// XXX park grant at unit pci m_start without req pending;

	reg [3:0] gnt;			// bus arb winner;
	reg cpu_req;			// cpu requests bus;

	always @(posedge clk or negedge rst)
	begin : req_arb
		integer pri, n;
		reg [1:0] rval;		// random bits;
		reg [3:0] arb;		// abiter bits;
		reg [1:0] pwin;		// previous winner;
		reg cwin;			// let cpu win;
		reg nwin;			// got a new winner;

		rval = $random;
		if(rst === 1'b0) begin
			gnt <= 4'b1111;
			cpu_req <= 1'b0;
			pwin <= 2'd0;
		end else if(frame_in == 1'b0) begin
			if(rval[0])
				gnt <= 4'b1111;			// random release;
		end else begin
			arb = 4'b0000;
			for(pri = 0; (arb == 4'd0) & (pri < 4); pri = pri + 1) begin
				for(n = 0; n < 4; n = n + 1) begin
					if(arb_pri[n] == pri)
						arb[n] = ~req[n];
				end
			end
			nwin = 0;
			n = pwin;
			while(!nwin && (n < (pwin + 4))) begin
				n = n + 1;
				nwin = arb[n & 3];
			end
			cwin = cpu_req & rval[1];
			if(nwin & ~cwin) begin
				pwin = n;
				gnt <= ~(1 << n[1:0]);
			end else
				gnt <= 4'b1111;
		end
	end

	// bus arbiter;
	// arbitrates between cpu and the current dma winner;
	// if a single request is pending, it will win;
	// if multiple requests are pending, then pick a random winner; 

	wire bus_idle;			// bus is idle;
	wire prev_idle;			// bus idle in last clock;
	reg [3:0] gnt_del;		// delayed gnt;
	reg dma_gnt;			// dma won bus;
	reg dma_act;			// dma is active;
	reg [2:0] dma_req;		// winning dma requestor;
	reg [31:0] dma_addr;	// dma address;
	reg [3:0] dma_cmd;		// dma command;
	wire #1 cpu_go;			// cpu can go;
	wire cpu_win;			// cpu won bus;

	assign bus_idle = rst & frame_in & irdy_in;
	assign prev_idle = rst & frame_ir & irdy_ir;

	always @(posedge clk or negedge rst)
	begin
		gnt_del <= gnt;
		if(rst == 1'b0)
			dma_gnt <= 1'b0;
		else
			dma_gnt <= ~&gnt & bus_idle;
		dma_act <= dma_gnt & ~bus_idle;
		if(dma_gnt & ~bus_idle) begin
			dma_req <= pci_func.master_index(gnt_del);
			dma_addr <= ad_in;
			dma_cmd <= cbe_in;
		end
	end

	assign cpu_go = ~dma_gnt & prev_idle;
	assign cpu_win = cpu_req & cpu_go;

	// pci bus reset;

	always @(posedge clk or negedge rst)
	begin
		if(rst == 1'b0) begin
			ad_out <= 32'bx;
			ad_oe <= 0;
			cbe_out <= 4'bx;
			cbe_oe <= 0;
			frame_out <= 1;
			frame_oe <= 0;
			devsel_out <= 1;
			devsel_oe <= 0;
			irdy_out <= 1;
			irdy_oe <= 0;
			trdy_out <= 1;
			trdy_oe <= 0;
			stop_out <= 1;
			stop_oe <= 0;
		end
	end

	// pci access by host;
	// blk is data exchange buffer;
	// cannot be used for INTR_ACK, SPECIAL and DAC;

	task master;
		input [31:0] addr;		// bus address;
		input [3:0] cmd;		// bus command;
		input [3:0] nwords;		// # of words;
		reg is_read;			// is a read command;
		reg [36:0] word;		// data word;
		reg tpar;				// parity;
		integer n;
		reg [31:0] stalls;		// irdy stall pattern;
		reg more;				// more xfer clocks;
		reg frame_end;			// deassert frame for last word;
		begin
			// should never have 0 words;

			if(nwords == 0)
				$display("ERROR: %t: %M: nwords=%0d", $time, nwords);
			#1;

			// one issue at a time;
			// wait for arbiter to grant us the pci bus;

			if(cpu_req !== 1'b0)
				$display("ERROR: %t: %M: re-entry", $time);
			cpu_req <= 1;
			wait(cpu_win);

			// output addr, cmd, frame;
			// this is turn-around for devsel, irdy, trdy, stop;

			ad_oe <= 1;
			ad_out <= addr;
			cbe_oe <= 1;
			cbe_out <= cmd;
			tpar = ^{ addr, cmd };
			par_buf <= addr_perr ^ tpar;
			frame_oe <= 1;
			frame_out <= 0;
			devsel_oe <= 0;
			devsel_out <= 1;
			irdy_oe <= 0;
			irdy_out <= 1;
			trdy_oe <= 0;
			trdy_out <= 1;
			irdy_oe <= 0;
			irdy_out <= 1;
			@(posedge clk);
			#1;

			// host is ready, independent of direction;
			// insert turn-around for reads;
			// drive first word for writes;

			if(nwords <= 1)
				frame_out <= 1;
			irdy_oe <= 1;
			irdy_out <= 0;
			n = 0;
			is_read = pci_func.is_read_cmd(cmd);
			word = blk[0];
			if(all_cbe)
				word[35:32] = 4'b0;
			cbe_out <= word[35:32];
			if(is_read) begin
				ad_oe <= 0;
				ad_out <= 32'bx;
				par_buf <= 1'bx;
				@(posedge clk);
				#1;
				n = 1;
			end else begin
				ad_oe <= 1;
				ad_out <= word[31:0];
				tpar = data_par? word[36] : ^word[35:0];
				par_buf <= tpar;
			end

			// wait for devsel from target;
			// devsel active in the read turn-around does not matter;

			while((devsel_ir !== 1'b0) & (n < 4)) begin
				@(posedge clk);
				#1;
				n = n + 1;
			end

			// if devsel not asserted within 4 clks, then master abort;

			if(devsel_ir !== 1'b0) begin
				frame_out <= 1;
				if(is_read) begin
					for(n = 0; n < nwords; n = n + 1)
						blk[n] = {37{1'bx}};			// X all data;
				end
				@(posedge clk);
				#1;
				irdy_out <= 1;

			end else begin

				// issue data phases;
				// optionally, insert random initiator stalls;
				//XXX handle target retry;
				//XXX handle target disconnect;
				//XXX handle target abort;

				n = 0;
				stalls = $random;
				more = 1;
				while(more) begin
					if(is_read) begin
						word[31:0] = ad_ir;
						word[35:32] = cbe_ir;
						word[36] = par_ir;
					end
					if(data_val) begin
						blk[n] = word;
						n = n + 1;
					end
					frame_end = (n == (nwords - 1));
					if(frame_end)
						frame_out <= 1;
					more = (n < nwords);
					if(more) begin
						// output next write data;

						if(data_val & !is_read) begin
							word = blk[n];
							if(all_cbe)
								word[35:32] = 4'b0;
							ad_out <= word[31:0];
							cbe_out <= word[35:32];
							tpar = data_par? word[36] : ^word[35:0];
							par_buf <= tpar;
						end

						// random irdy stall;

						if(irdy_stalls & data_val & ~frame_end)
							irdy_out <= stalls[n];

						@(posedge clk);
						#1;
						irdy_out <= 1'b0;
					end
				end

				// frame, ad, cbe, par turn around one clk
				// earlier than irdy, trdy, stop, devsel;

				frame_oe <= 0;
				ad_oe <= 0;
				ad_out <= 32'bx;
				cbe_oe <= 0;
				cbe_out <= 4'bx;
				irdy_out <= 1;
			end
			@(posedge clk);
			frame_oe <= 0;
			irdy_oe <= 0;
			cpu_req <= 0;
		end
	endtask

	// host is target;
	// do not respond if requestor's be_target bit is off;
	// force target abort, if requestors target_abort bits are set;

	always @(posedge dma_act)
	begin : target
		reg [15:0] val_cmds;		// valid cmds for this requestor;
		reg respond;				// respond as target;
		reg is_read;				// is a read from bridge;
		reg [1:0] devsel_lat;		// random devsel latency;
		reg [19:2] maddr;			// memory address;
		reg [31:0] word;			// data word;
		reg tpar;					// parity when driving;
		reg trdy;					// send a target ready;
		reg tlast;					// last data phase;
		reg tdone;					// target done;

		#1;
		val_cmds = target_cmds[dma_req];
		respond = be_target[dma_req] & val_cmds[dma_cmd];
		$display("%t: %M: addr=0x%h cmd=%b, respond=%b",
			$time, dma_addr, dma_cmd, respond);
		if(respond) begin
			maddr = dma_addr[19:2];
			is_read = pci_func.is_read_cmd(dma_cmd);
			devsel_lat = $random;

			ad_out <= $random;
			repeat(devsel_lat) @(posedge clk);
			#1;
			devsel_oe <= 1;
			devsel_out <= 0;
			stop_oe <= 1;
			stop_out <= 1;
			trdy_oe <= 1;

			// guaranteee turn-around clock;

			if(is_read & (devsel_lat == 0)) begin
				trdy_out <= 1;
				@(posedge clk);
				#1;
			end
			ad_oe <= is_read;

			// respond to data cycles;
			// optionally, insert random target stalls;
			// optionally, force target abort;

			tdone = 0;
			while(!tdone) begin
				trdy = trdy_stalls[dma_req]? $random : 1;
				trdy_out <= ~trdy;
				if(trdy & is_read) begin
					word = mem[maddr];
					ad_out <= word;
					tpar = ^word ^ ^cbe_ir;
					par_buf <= tpar;
				end
				@(posedge clk);
				#1;
				if(data_val) begin
					if(~is_read) begin
						word = mem[maddr];
						if(cbe_ir[0] === 1'b0)
							word[7:0] = ad_ir[7:0];
						if(cbe_ir[1] === 1'b0)
							word[15:8] = ad_ir[15:8];
						if(cbe_ir[2] === 1'b0)
							word[23:16] = ad_ir[23:16];
						if(cbe_ir[3] === 1'b0)
							word[31:24] = ad_ir[31:24];
						mem[maddr] = word;
					end
					maddr = maddr + 1;
				end
				tlast = frame_ir & data_val;
				tdone = tlast;
			end
			devsel_out <= 1;
			devsel_oe <= 0;
			trdy_out <= 1;
			trdy_oe <= 0;
			stop_out <= 1;
			stop_oe <= 0;
			ad_oe <= 0;
			ad_out <= 32'bx;
		end
	end

	// write single word to memory;
	// wrap the 1MB physical memory;
	// can be multi-threaded;

	task mem_swrite;
		input [31:0] addr;
		input [31:0] wdata;
		begin
			addr[31:20] = 0;
			addr = addr >> 2;
			mem[addr] = wdata;
		end
	endtask

	// read single word from memory;
	// wrap the 1MB physical memory;
	// can be multi-threaded;

	task mem_sread;
		input [31:0] addr;
		output [31:0] rdata;
		begin
			addr[31:20] = 0;
			addr = addr >> 2;
			rdata = mem[addr];
		end
	endtask

	// write block to memory;
	// wrap the 1MB physical memory;
	// cannot be multi-threaded due to usage of blk;

	task mem_bwrite;
		input [31:0] addr;
		input [3:0] bsize;				// size in words;
		integer n;
		begin
			addr[31:20] = 0;
			addr[4:2] = addr[4:2] & ~(bsize - 1);
			addr = addr >> 2;
			for(n = 0; n < bsize; n = n + 1)
				mem[addr ^ n] = blk[n];
		end
	endtask

	// read block from memory;
	// wrap the 1MB physical memory;
	// cannot be multi-threaded due to usage of blk;

	task mem_bread;
		input [31:0] addr;
		input [3:0] bsize;				// size in words;
		integer n;
		begin
			addr[31:20] = 0;
			addr = addr >> 2;
			for(n = 0; n < bsize; n = n + 1)
				blk[n] = mem[addr ^ n];
		end
	endtask

	// single write;

	task swrite;
		input [31:0] addr;
		input [31:0] wdata;
		begin
			if(rst !== 1'b1)
				$display("ERROR: %t: %M: rst=%b", $time, rst);
			else if(addr[1:0] !== 2'b0)
				$display("ERROR: %t: %M: addr[1:0]=%b", $time, addr[1:0]);
			else if(addr[31:28] === 4'h0)
				mem_swrite(addr, wdata);
			else if(addr[31:20] === 12'h1fc) begin
				blk[0] = wdata;
				master(addr, `PCI_MEM_WRITE, 1);
			end else
				$display("ERROR: %t: %M: addr=0x%h (%b)", $time, addr, addr);
		end
	endtask

	// single read;

	task sread;
		input [31:0] addr;
		output [31:0] rdata;
		begin
			if(rst !== 1'b1)
				$display("ERROR: %t: %M: rst=%b", $time, rst);
			else if(addr[1:0] !== 2'b0)
				$display("ERROR: %t: %M: addr[1:0]=%b", $time, addr[1:0]);
			else if(addr[31:28] === 4'h0)
				mem_sread(addr, rdata);
			else if(addr[31:20] === 12'h1fc) begin
				master(addr, `PCI_MEM_READ, 1);
				rdata = blk[0];
			end else
				$display("ERROR: %t: %M: addr=0x%h (%b)", $time, addr, addr);
		end
	endtask

	// block write;
	// block writes are sequential;

	task bwrite;
		input [31:0] addr;
		input [3:0] bsize;				// size in words;
		reg [4:0] waddr;				// write block address;
		begin
			waddr[4:2] = addr[4:2] & (bsize - 1);
			waddr[1:0] = addr[1:0];
			if(rst !== 1'b1)
				$display("ERROR: %t: %M: rst=%b", $time, rst);
			else if(waddr !== 5'b0)
				$display("ERROR: %t: %M: addr[4:0]=%b", $time, addr[4:0]);
			else if(addr[31:28] === 4'h0)
				mem_bwrite(addr, bsize);
			else if(addr[31:20] === 12'h1fc)
				master(addr, `PCI_MEM_WRITE, bsize);
			else
				$display("ERROR: %t: %M: addr=0x%h (%b)", $time, addr, addr);
		end
	endtask

	// block read;
	// return data in critical-word order;

	task bread;
		input [31:0] addr;
		input [3:0] bsize;				// size in words;
		begin
			if(rst !== 1'b1)
				$display("ERROR: %t: %M: rst=%b", $time, rst);
			else if(addr[1:0] !== 5'b0)
				$display("ERROR: %t: %M: addr[1:0]=%b", $time, addr[1:0]);
			else if(addr[31:28] === 4'h0)
				mem_bread(addr, bsize);
			else if(addr[31:20] === 12'h1fc)
				master(addr, `PCI_MEM_READ, bsize);
			else
				$display("ERROR: %t: %M: addr=0x%h (%b)", $time, addr, addr);
		end
	endtask

	// XXX system interrupt;

endmodule

