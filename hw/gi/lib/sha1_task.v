/*   SHA1 tasks  
     cmd = 0 --- reset 
           1 --- hash
 */ 

task sha1_task;
	inout [159:0] state;
	input [511:0] data;
	input init;

	reg debug;
	reg [31:0] K[3:0];
	reg [31:0] W[79:0];
	integer tmp, temp, A, B, D, C, E, t;

	begin
		debug = 0;
		if(init) begin
			state[159:128] = 32'h67452301;
			state[127:96]  = 32'hefcdab89;
			state[95:64]   = 32'h98badcfe;
			state[63:32]   = 32'h10325476;
			state[31:0]    = 32'hc3d2e1f0;
		end

		K[0] = 32'h5A827999;
		K[1] = 32'h6ED9EBA1;
		K[2] = 32'h8F1BBCDC;
		K[3] = 32'hCA62C1D6;

		W[0] = data[511 : 480];
		W[1] = data[479 : 448];
		W[2] = data[447 : 416];
		W[3] = data[415 : 384];
		W[4] = data[383 : 352];
		W[5] = data[351 : 320];
		W[6] = data[319 : 288];
		W[7] = data[287 : 256];
		W[8] = data[255 : 224];
		W[9] = data[223 : 192];
		W[10] = data[191 : 160];
		W[11] = data[159 : 128];
		W[12] = data[127 : 96];
		W[13] = data[95 : 64];
		W[14] = data[63 : 32];
		W[15] = data[31 : 0];
			
		A = state[159:128];
		B = state[127:96];
		C = state[95:64];
		D = state[63:32];
		E = state[31:0];
	
		for (t=0; t<80; t=t+1) begin
			if(t >= 16) begin
				tmp = W[t-3] ^ W[t-8] ^ W[t-14] ^ W[t-16];
				W[t] = {tmp[30:0], tmp[31]};
			end
			if(debug)
				$display("%M: W[%d] = 0x%x", t, W[t]);
		end				
			
		for(t = 0; t < 20; t=t+1) begin
			tmp = {A[26:0], A[31:27]};
			temp = tmp + ((B & C) | ((~B) & D)) + E + W[t] + K[0];
			E = D;
			D = C;
			C = {B[1:0], B[31:2]};
			B = A;
			A = temp;
		end
		if(debug)
			$display("%M: ABCDE 1 %x %x %x %x %x", A, B, C, D, E);

		for(t = 20; t < 40; t=t+1) begin
			tmp = {A[26:0], A[31:27]};
			temp = tmp + (B ^ C ^ D) + E + W[t] + K[1];
			E = D;
			D = C;
			C = {B[1:0], B[31:2]};
			B = A;
			A = temp;
		end
		if(debug)
			$display("%M: ABCDE 2 %x %x %x %x %x", A, B, C, D, E);

		for(t = 40; t < 60; t=t+1) begin
			tmp = {A[26:0], A[31:27]};
			temp = tmp + ((B & C) | (B & D) | (C & D)) + E + W[t] + K[2];
			E = D;
			D = C;
			C = {B[1:0], B[31:2]};
			B = A;
			A = temp;
		end
		if(debug)
			$display("%M: ABCDE 3 %x %x %x %x %x", A, B, C, D, E);

		for(t = 60; t < 80; t=t+1) begin
			tmp = {A[26:0], A[31:27]};
			temp = tmp + (B ^ C ^ D) + E + W[t] + K[3];
			E = D;
			D = C;
			C = {B[1:0], B[31:2]};
			B = A;
			A = temp;
		end	
		if(debug)
			$display("%M: ABCDE 4 %x %x %x %x %x", A, B, C, D, E);

		state[159:128] = state[159:128] + A;
		state[127:96]  = state[127:96] + B;
		state[95:64]   = state[95:64] + C;
		state[63:32]   = state[63:32] + D;
		state[31:0]    = state[31:0] + E;
		if(debug)
			$display("%M: hash=%h", state);
	end
endtask
	

