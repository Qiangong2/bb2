
module aes_test;
	initial $display("AES test start");

	`include "aes_task.v"

	reg [127:0] pt;
	reg [127:0] ct;
	reg [127:0] exp;
	reg [127:0] key;
	reg [127:0] iv;

	initial begin
		# 10;
		$display("Test AES Encrption ");
		iv = 128'h6769514a29baf2e37c541be7762e33c9;
		key = 128'hc673ffeccdabfb46c2f8e88d5a639f9a;
		pt = 128'h66320db73158a35a255d051758e95ed4;
		exp = 128'h66b8f9dbe1b182e14140f79e446578df;
		aes_task(pt, ct, key, iv, 0);
		if (ct !== exp) $display("ERROR");
		else $display("Encrpt OK!!! out = %x", ct);

		#10;
		$display("Test AES Decrption ");
		iv = 128'h3ad851c801ca013a681123de91adb47e;
		ct = 128'h705616c1d3de72d4ce3c58cf4893d9b0;
		key = 128'hd1287cb0f0840b09af27365aa7699eee;
		exp = 128'hfc0a713bdbd5f67975bbd7162f9d7080;
		aes_task(ct, pt, key, iv, 1);
		if (pt !== exp) $display("ERROR exp=0x%x get=0x%x", exp, pt);
		else $display("Encrpt OK!!! out = %x", pt);
	end
endmodule
