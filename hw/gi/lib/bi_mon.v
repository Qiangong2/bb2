// bi_mon.v Frank Berndt
// bi interface monitor;
// :set tabstop=4

module bi_mon (
	clk, reset,
	sys_addr, sys_cmd, sys_out, sys_in, sys_intr,
	dma_req, dma_bx, dma_addr, dma_wr, dma_done
);
	input clk;				// core clock;
	input reset;			// system reset;

	// interface to gi;

	input [31:0] sys_addr;		// cpu address;
	input [2:0] sys_cmd;		// bus command;
	input [31:0] sys_out;		// write data, cpu and dma;
	input [31:0] sys_in;		// read data, cpu and dma;
	input sys_intr;				// system interrupt;
	input dma_req;				// request a dma transfer;
	input [1:0] dma_bx;			// burst control;
	input [31:0] dma_addr;		// dma physical address;
	input dma_wr;				// dma write to memory;
	input dma_done;				// dma done;

	// report my presence;
	// turn monitor on or off;

	reg mon;

	initial
	begin
		$display("%M: bi interface monitor");
		mon = $test$plusargs("mon_bi");
	end

	// monitor dma requests;

	always @(posedge dma_req)
	begin : dma_request
		@(posedge clk);
		if(mon) begin
			$display("%t: %M: b%0d, addr=0x%h, wr=%b",
				$time, dma_bx[0]? 128 : 64, dma_addr, dma_wr);
		end
	end

	// check system address;

	task check_addr;
		input [31:0] addr;
		reg addr_x;				// addr has Xs in it;
		begin
			addr_x = ^addr[17:2];
			if(addr_x === 1'bx)
				$display("ERROR: %t: %M: addr=0x%h/%b", $time, addr, addr);
		end
	endtask

	// monitor reads/writes;

	reg [15:0] rwstr;
	wire [2:0] cmd;
	reg [2:0] cmd1, cmd2, cmd3, cmd4;
	integer seq, seq1, seq2, seq3, seq4;

	assign cmd = reset? 3'b0 : sys_cmd;

	always @(posedge clk)
	begin
		if(reset) begin
			cmd1 <= 0; cmd2 <= 0; cmd3 <= 0; cmd4 <= 0;
			seq <= 0; seq1 <= 0; seq2 <= 0; seq3 <= 0; seq4 <= 0;
		end else begin
			cmd1 <= cmd; cmd2 <= cmd1; cmd3 <= cmd2; cmd4 <= cmd3;
			seq1 <= seq; seq2 <= seq1; seq3 <= seq2; seq4 <= seq3;
		end
		rwstr = cmd[0]? "rd" : "wr";
		if(mon & (cmd1[1:0] === 2'b10))
			$display("%t: %M: wr[%0d] data=0x%h", $time, seq1, sys_out);
		if(mon & (cmd4[1:0] === 2'b01))
			$display("%t: %M: rd[%0d] data=0x%h", $time, seq4, sys_in);
		case(cmd)
			3'b000:
				;
			3'b001, 3'b010: begin
				check_addr(sys_addr);
				if(mon) begin
					$display("%t: %M: %s[%0d] cpu addr=0x%h",
						$time, rwstr, seq, sys_addr);
				end
				seq <= seq + 1;
			end
			3'b101, 3'b110: begin
				if(mon)
					$display("%t: %M: %s[%0d] dma", $time, rwstr, seq);
				seq <= seq + 1;
			end
			default: begin
				$display("ERROR: %t: %M: sys_cmd=%b", $time, cmd);
			end
		endcase
	end

	// monitor system interrupt;

	always @(sys_intr)
		$display("%t: %M: sys_intr=%b", $time, sys_intr);

endmodule

