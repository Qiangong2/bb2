// di_mon.v Frank Berndt
// DI monitor;
// :set tabstop=4

module di_mon (
	reset, dir, dd, hstrb, dstrb, err, brk, cover
);
	input reset;			// interface reset;
	input dir;			// xfer direction;
	input [7:0] dd;			// data bus;
	input hstrb;			// host strobe;
	input dstrb;			// device strobe;
	input err;			// error;
	input brk;			// break;
	input cover;			// cover open;

	// registers for strobe counting

	reg [2:0]	dstrb_adv, hstrb_adv;
	reg [4:0]	dstrb_cnt, hstrb_cnt;
	reg		cmd_done;

	// report my presence;

	initial
		$display("%M: di bus monitor");

	// monitor reset;

	always @(reset)
		$display("%t: %M: reset=%b", $time, reset);

        // monitor defaults at deassertion of reset;

        always @(posedge reset)
        begin
                if(dir !== 1'b0)
                        $display("ERROR: %t: %M: dir=%b exp 0", $time, dir);
                if(hstrb !== 1'b1)
                        $display("ERROR: %t: %M: hstrb=%b exp 1", $time, hstrb);
                if(dstrb !== 1'b1)
                        $display("ERROR: %t: %M: dstrb=%b exp 1", $time, dstrb);
                if(brk !== 1'b0)
                        $display("ERROR: %t: %M: brk=%b exp 0", $time, brk);
                if(err !== 1'b1)
                        $display("ERROR: %t: %M: err=%b exp 1", $time, err);
        end

        // monitor dir;
        // both strobes should be 1 at direction change;

        always @(dir)
        begin
                $display("%t: %M: dir=%b", $time, dir);
                #1;
                if(hstrb !== 1'b1)
                        $display("ERROR: %t: %M: @dir hstrb=%b exp 1", $time, hstrb);
                if(dstrb !== 1'b1)
                        $display("ERROR: %t: %M: @dir dstrb=%b exp 1", $time, dstrb);
        end

        // monitor error;
        // dir should be 1 at posedge error;

        always @(posedge err)
        begin
                $display("%t: %M: err=%b", $time, err);
                #1;
                if(reset & (dir !== 1'b1))
                        $display("ERROR: %t: %M: @err dir=%b exp 1", $time, dir);
        end

        // monitor break;

        always @(brk)
                $display("%t: %M: brk=%b", $time, brk);

	// monitor break and error overlap:  break should take precedence;

        always @(posedge brk)
        begin
                if(err !== 1'b1)
                        $display("BREAK during E R R O R: %t: %M: @brk err=%b", $time, err);
        end

        always @(negedge err)
        begin
                if(brk == 1'b1)
                        $display("E R R O R during BREAK: %t: %M: @err brk=%b", $time, brk);
        end

        // monitor cover;

        always @(cover)
                $display("%t: %M: cover=%b", $time, cover);

        wire    hstrb_even, dstrb_even;
        assign  hstrb_even      =	(~hstrb_cnt[0] & err & reset & ~dir & ~brk);
        assign  dstrb_even      =	(~dstrb_cnt[0] & err & reset &  dir & ~brk);

  `ifdef  TIMING_CHECKS

	specify

	specparam

		THCYCL	=	19.55,	//min host strobe pulse width low
                THCYCH  =       19.55,  //min host strobe pulse width high
                TDCYCL  =       19.55,  //min drive strobe pulse width low
                TDCYCH  =       19.55,  //min drive strobe pulse width high

		TDDS	=	10.00,	//host data out setup versus host strobe rise
                THDS    =       10.00,  //drive data out setup versus drive strobe rise

		TDDH	=	10.00,	//host data out hold versus host strobe rise
                THDH    =       10.00,  //drive data out hold versus drive strobe rise

		TDWRD	=	32.00,	//write to read delay (hstrb rise setup to dir rise) spec 40
		TDRWD	=	22.00,	//read to write delay (dstrb rise setup to dir fall) spec 25
		THRDYD	=	00.00,	//dir rise to host ready (hstrb fall)
		TDRDYD	=	00.00,	//dir fall to drive ready (dstrb fall)

		THDWAITS=	20.00,	//drive stall assert before odd rising edge host strobe; spec 40
		TDDWAITS=	20.00,	//host stall assert before odd rising edge drive strobe; spec 40

					// rely solely on above pair to cover TDWAITREQ,THWAITREQ

		THDWAITH=	10.00,	//drive stall hold after odd rising edge host strobe
		TDDWAITH=	10.00,	//host stall hold after odd rising edge drive strobe

		TBRKH	=	40.00,	//min duration brk high
		TBRKL   =       40.00;  //min duration brk low

        $width(negedge hstrb, THCYCL);
	$width(posedge hstrb, THCYCH);
        $width(negedge dstrb, TDCYCL);
	$width(posedge dstrb, TDCYCH);
        $width(negedge brk, TBRKL);
        $width(posedge brk, TBRKH);

	$setup(dd, posedge hstrb &&& ~dir, TDDS);
	$setup(dd, posedge dstrb &&&  dir, THDS);

        $hold (posedge hstrb &&& ~dir, dd, TDDH);
        $hold (posedge dstrb &&&  dir, dd, THDH);

        $setup(hstrb, posedge dir, TDWRD);
        $setup(dstrb, negedge dir, TDRWD);

	$hold (posedge dir, hstrb, THRDYD);
        $hold (negedge dir, dstrb, TDRDYD);

        $setup(dstrb, posedge hstrb &&& hstrb_even, THDWAITS);
	$setup(hstrb, posedge dstrb &&& dstrb_even, TDDWAITS);

	$hold (posedge hstrb &&& hstrb_even, dstrb, THDWAITH);
	$hold (posedge dstrb &&& dstrb_even, hstrb, TDDWAITH);

	endspecify

  `endif

`ifdef CYCLE_CHECKS

	// monitor host strobe goes high for last 2-3 drive strobes of completed response
	// transaction may be truncated for break, error, reset

	always @(posedge dstrb or negedge reset or posedge brk)
	begin
		hstrb_adv <= hstrb & dir & reset & !brk ? hstrb_adv + !hstrb_adv[2] : 3'b0;
	end

	always @(negedge dir)
	begin
		if(!brk & err & reset & hstrb_adv[2])
			$display("ERROR: host strobe high too early before data xfer completed to host %t: %M: @dir hstrb_adv=%d", $time, hstrb_adv);
		if(!brk & err & reset & !hstrb_adv[2] & !hstrb_adv[1] & |hstrb_adv)
                        $display("ERROR: host strobe high too late before data xfer completed to host %t: %M: @dir hstrb_adv=%d", $time, hstrb_adv);
	end

        // monitor drive strobe goes high for last 2-3 host strobes of completed transfer
	// error, brk, reset terms allow exception for XWRITE case if data cycles arbitrarily truncated

        always @(posedge hstrb or negedge reset or posedge brk)
        begin
                dstrb_adv <= dstrb & !dir & reset & !brk ? dstrb_adv + !dstrb_adv[2] : 3'b0;
        end

        always @(posedge dir)
        begin
                if(!brk & err & reset & dstrb_adv[2])
                        $display("ERROR: drive strobe high too early before data xfer completed to drive %t: %M: @dir dstrb_adv=%d", $time, dstrb_adv);
                if(!brk & err & reset & !dstrb_adv[2] & !dstrb_adv[1] & |dstrb_adv)
                        $display("ERROR: drive strobe high too late before data xfer completed to drive %t: %M: @dir dstrb_adv=%d", $time, dstrb_adv);
        end

	// monitor DIR should not go high before uninterrupted host to drive transfer complete
	// infer transfer complete if # hstrb cycles %32 = 4, and cmd_done	
        // error, brk, reset terms allow exception for write cases e.g. if data cycles arbitrarily truncated
	// note writes mask this check, as no dir turnaround occurs XXX

	always @(posedge hstrb or posedge dir or negedge reset)
	begin
		hstrb_cnt <= #1 (dir | brk | !reset) ? 5'h0 : hstrb_cnt + 1'b1;
	end

	always @(posedge dir)
	begin
		if (!brk & err & reset & (!cmd_done | hstrb_cnt[1:0]))
			$display("ERROR: dir high before xfer completed to drive %t: %M: @dir hstrb_cnt=%d", $time, hstrb_cnt);
	end

        // monitor DIR should not go low before uninterrupted drive to host transfer complete
        // *WAS* infer transfer complete if # dstrb cycles %32 = 0 or 4
	// *NOW* infer transfer complete if # dstrb cycles %4 = 0
	// error, break, reset terms allow drive to arbitrarily truncate response
	// note writes mask this check, as no dir turnaround occurs XXX

        always @(posedge dstrb or negedge dir or negedge reset)
        begin
                dstrb_cnt <= #1 (~dir | brk | !reset) ? 5'h0 : dstrb_cnt + 1'b1;
        end

        always @(negedge dir)
        begin
                if (!brk & err & reset & !(dstrb_cnt[1:0] == 2'h0))
                        $display("ERROR: dir low before xfer completed to host %t: %M: @dir dstrb_cnt=%d", $time, dstrb_cnt);
        end

	// monitor drive observance of host stall when DIR=1
	// must be even number of sender's posedge strobe prior to receiver strobe negedge (end of stall)
	// iff ensure receiver's stall actually heeded by sender (e.g. staller may change its mind) XXXX
	//	**disable test, cannot support if race condition hstrb:dstrb events**
/*
	always @(negedge hstrb)
	begin
		#1;
		if (dstrb_cnt[0])
			$display("ERROR: drive as master heeds host stall on odd transfer cycle %t: %M: @hstrb dstrb_cnt=%b", $time, dstrb_cnt);
	end
*/
        // monitor host observance of drive stall when DIR=0
	// must be even number of sender's posedge strobe prior to receiver strobe negedge (end of stall)
	// iff ensure receiver's stall actually heeded by sender (e.g. staller may change its mind) XXXX
	//	**disable test, cannot support if race condition hstrb:dstrb events**
/*
        always @(negedge dstrb)
        begin
		#1;
                if (hstrb_cnt[0])
                        $display("ERROR: host as master heeds drive stall on odd transfer cycle %t: %M: @dstrb hstrb_cnt=%b", $time, hstrb_cnt);
        end
*/
	// monitor break should never be asserted during command cycles (first 12 bytes host to drive)
	// writes, because of no dir turnaround, mask out this check, must fix  XXXXX

	always @(hstrb_cnt or dir or negedge reset)
	begin
		cmd_done <= ((hstrb_cnt == 5'h0c) | cmd_done) & !(dir | !reset);
	end

	always @(posedge brk)
	begin
		if(!dir & !(cmd_done | !(|hstrb_cnt)))
			$display("ERROR: host asserts break during command xfer %t: %M: @brk hstrb_cnt=%b", $time, hstrb_cnt);
        end

        // monitor command cycles should never be cut short by posedge dir (first 12 bytes host to drive)
	// back to back write commands are not checked here, as need dir turnaround to imply new command

        always @(posedge dir)
        begin
                if(!(cmd_done | !(|hstrb_cnt)))
                        $display("ERROR: data bus turnaround before host finishes sending command %t: %M: @dir hstrb_cnt =%d", $time, hstrb_cnt);
        end

`endif

`ifdef PERF_MON

reg		head_en;
reg [63:0]	head;
reg [63:0]	tail;
reg [63:0]	err_start,  err_acc;
reg		brk_en;
reg [63:0]      brk_start,  brk_acc;
reg [63:0]	dir0_start, dir0_acc;
reg [63:0]	dir1_start, dir1_acc;
reg [31:0]	dir0_count, dir1_count;

`define	HOST_CYC		16'd49376
`define	DRIVE_CYC		16'd50000

wire[31:0]	dir0_time      =       (dir0_count * `HOST_CYC) / 64'd1000;
wire[31:0]	dir1_time      =       (dir1_count * `DRIVE_CYC) / 64'd1000;

wire[31:0]      dir0_eff       =       (dir0_time * 16'd100) / dir0_acc;
wire[31:0]      dir1_eff       =       (dir1_time * 16'd100) / dir1_acc;
wire[31:0]	total_eff	=	((dir0_time + dir1_time) * 16'd100) / (dir0_acc + dir1_acc);
wire[31:0]	total_time	=	tail - head;

initial begin
	head		<=	64'b0;
	tail		<=	64'b0;
	err_acc		<=	64'b0;
	brk_acc		<=	64'b0;
	brk_en		<=	1'b0;
	dir0_acc	<=	64'b0;
	dir1_acc	<=	64'b0;
end

always@(posedge reset) begin
	head_en		<=	1'b1;
end

always@(posedge `send) begin			//calculate idle time at sim start
	head_en		<=	1'b0;
	head		<=	head_en		?	$time	:	head;
end

always@(posedge hstrb or posedge dstrb) begin	//mark idle time before sim end
	tail		<=	(hstrb & dstrb)	?	$time	:	64'b0;
end

always@(err) begin				//sum all error intervals
	err_start	<=	err ? 64'b0 : $time;
	err_acc		<=	reset ? err_acc + (err ? ($time - err_start) : 64'b0) : 64'b0;
end

always@(brk) begin				//sum break intervals (2 pulses duration each)
	brk_en		<=	!brk & reset ? !brk_en : brk_en;
        brk_start	<=      brk & !brk_en ? $time : brk_start;
        brk_acc		<=      reset ? brk_acc + (brk_en & !brk ? ($time - brk_start) : 64'b0) : 64'b0;
end

always@(dir or negedge head_en) begin		//sum dir=0 time
        dir0_start     <=      !dir ? $time : 64'b0;
end
always@(dir) begin
        dir0_acc       <=      reset ? dir0_acc + (dir ? ($time - dir0_start) : 64'b0) : 64'b0;
end

always@(dir) begin				//sum dir=1 time
        dir1_start     <=      dir ? $time : 64'b0;
        dir1_acc       <=      reset ? dir1_acc + (!dir ? ($time - dir1_start) : 64'b0) : 64'b0;
end

always@(posedge hstrb) begin                    //sum #bytes sent to drive
        dir0_count     <=      head_en | !reset ? 32'b0 : dir0_count + (dir == 1'b0);
end

always@(posedge dstrb) begin                    //sum #bytes sent to flipper
        dir1_count     <=      head_en | !reset ? 32'b0 : dir1_count + (dir == 1'b1);
end

`endif

	// XXX more timing checks;
	// XXX more protocol checks;

endmodule
