// di_host.v Frank Berndt
// DI host model;
// :set tabstop=4

`timescale 1ns/1ns

module di_host (
	reset, dir, dd, hstrb, dstrb, err, brk, cover
);
	output reset;			// interface reset;
	output dir;				// xfer direction;
	inout [7:0] dd;			// data bus;
	output hstrb;			// host strobe;
	input dstrb;			// device strobe;
	input err;				// error;
	inout brk;				// break;
	input cover;			// cover open;

	// di timing;

	integer tRST;			// reset active time;
	integer tHCYC;			// host strobe cycle time;
	integer tDSHS;			// dstrb->0 to hstrb->0;
	integer tDWAITREQ;		// delay to ready deassertion;

	initial
	begin
		tRST = 2000;
		tHCYC = 49;
		tDSHS = 6;
		tDWAITREQ = 20;
	end

	// interface signals;
	// keep in reset on simulator start;

	reg reset;				// bus reset;
	reg dir;				// direction;
	wire dd_oe;				// dd output enable;
	reg hstrb;				// host strobe;
	reg brk_out;			// break;
	reg brk_oe;				// break output enable;
	reg brk_busy;			// busy with break protocol;
	reg brk_term;			// device break termination;
	reg [7:0] dd_out;		// output byte;
	reg send;				// start transmission;
	reg recv;				// start reception;
	integer size;			// transmission size;

	initial
	begin
		$display("%M: model");
		#1;
		rst(1);
	end

	// reset task;
	// guarantees minimum reset time;

	task rst;
		input [1:0] cmd;	// 01=assert, 10=deassert, 11=full sequence;
		begin
			$display("%t: %M: cmd=%b", $time, cmd);
			if(cmd[0]) begin
				reset = 1'b0;
				dir <= 1'b0;
				hstrb = 1'b1;
				brk_out = 1'b0;
				brk_oe = 1'b1;
				brk_busy <= 1'b0;
				brk_term <= 1'b0;
				dd_out = 'bx;
				send = 0;
				recv = 0;
				size = 0;
			end
			if(cmd[1]) begin
				#(tRST);
				reset = 1'b1;
			end
		end
	endtask

	// drive data bus;

	assign dd_oe = ~dir;
	assign dd = dd_oe? dd_out : 8'bz;
	assign brk = brk_oe? brk_out : 1'bz;

	// dv accessible variables;

	reg [7:0] data [0:64*1024-1];	// cmd/data buffer;
	integer brk_pos;				// break at this byte position;
	reg jitter_hstrb;				// apply random hstrb jitter;
	reg random_hstall;				// random host stalls;

	initial
		brk_pos = -1;

	initial
		jitter_hstrb = 0;

	always @(jitter_hstrb)
		$display("%t: %M: jitter_hstrb=%b", $time, jitter_hstrb);

	initial
		random_hstall = 0;

	always @(random_hstall)
		$display("%t: %M: random_hstall=%b", $time, random_hstall);

	// get word;

	task get;
		input [15:0] off;			// byte offset;
		output [31:0] word;			// word;
		begin
			word[31:24] = data[off + 0];
			word[23:16] = data[off + 1];
			word[15:8] = data[off + 2];
			word[7:0] = data[off + 3];
		end
	endtask

	// put word;

	task put;
		input [15:0] off;			// byte offset;
		input [31:0] word;			// word;
		begin
			data[off + 0] = word[31:24];
			data[off + 1] = word[23:16];
			data[off + 2] = word[15:8];
			data[off + 3] = word[7:0];
		end
	endtask

	// set break position;

	task break;
		input [31:0] pos;			// byte position;
		begin
			brk_pos = pos;
			$display("%t: %M: pos %0d", $time, brk_pos);
		end
	endtask

	task break_now;
		begin
			brk_pos = rcv.n + 1;
			$display("%t: %M: pos %0d", $time, brk_pos);
		end
	endtask

	// start a cmd/response transaction;
	// write transaction are really two cmds from flipper's view;
	// data is expected to be in the data buffer;

	reg new_dir;					// switch direction;

	task xfer;
		input rcv;					// receive;
		input [15:0] tsize;			// # of bytes to send;
		input [15:0] rsize;			// # of bytes to receive;
		input wr_cmd;				// is a write command;
		input rss;					// randomize strobes/stalls;
		begin
			if(rcv == 0) begin
				size = tsize;
				jitter_hstrb = rss;
				new_dir = 1;
				send = 1;
			end else begin
				size = rsize;
				random_hstall = rss;
				new_dir = 0;
				recv = 1;
			end
			$display("%t: %M: dir=%b, tsize=%0d, rsize=%0d, wr_cmd=%b",
				$time, rcv, tsize, rsize, wr_cmd);
		end
	endtask

	// turn around direction;
	// randomize timing between min and max;
	// min must be >= max/2;
	// internal task, do not call from outside;

	task dir_turn;
		input ndir;
		input [31:0] min, max, mask;
		integer del;
		begin
			del = min + ($random & mask);
			if(del > max)
				del = del / 2;
			 dir <= #(del) ndir;
		end
	endtask

	// send data packet;

	always @(posedge send)
	begin : xmt
		integer n, stall;
		reg [3:0] rdel;

		stall = 0;
		for(n = 0; n < size; n = n + 1) begin
			if(n == stall) begin
				wait(dstrb === 1'b0);		// wait for device ready;
				#(tDSHS);
			end
			hstrb = 0;
			dd_out = data[n];
			#(tHCYC/2);
			if((n[0] == 0) & (dstrb === 1'b0))
				stall = n + 4;				// four more bytes;
			hstrb = 1;
			#(tHCYC/2);
			if(jitter_hstrb) begin
				rdel = $random;
				#(rdel);					// random hstrb jitter;
			end
		end
		dd_out = 'bx;
		send = 0;
		if(new_dir != dir)
			dir_turn(new_dir, 10, 64, 63);	// tDWRD;
	end

	// receive data packet;

	always @(posedge recv)
	begin : rcv
		integer n, cont;
		reg stall;
		reg [5:0] rdel;

		cont = 0;
		for(n = 0; n < size; n = n + 1) begin
			stall = 0;
			if(n == cont) begin
				rdel = $random;				// random start delay;
				#(rdel);
				cont = 0;					// unstall;
				hstrb <= 0;					// ready again;
			end
			@(posedge dstrb or negedge err or posedge brk_term or negedge reset);
			if((err == 1'b0) | brk_term | (reset == 1'b0)) begin
				n = size;					// stop on error;
				hstrb <= 1;
				new_dir = 0;				// turn around for cmd;
			end else begin
				if(brk_pos == n) begin
					brk_pos = -1;			// single issue;
					brk_busy <= 1;			// break here;
				end
				data[n] = dd;
				if(n >= (size - 4))
					stall = 1;				// eom stall is higher prty;
				else if(n[1:0] == 0)
					stall = $random & random_hstall;
				if(stall) begin
					if(hstrb === 1'b0)
						hstrb <= #(tDWAITREQ) 1;
					cont = n + 4;
				end
			end
		end
		recv = 0;
		if(new_dir != dir)
			dir_turn(new_dir, 21, 60, 63);	// tDRWD;
	end

	// handle break protocol;
	// randomize break delay with regard to device strobe;

	always @(posedge brk_busy)
	begin : brk_proc
		reg [3:0] brk_del;				// random break delay;

		$display("%t: %M: break at %0d", $time, brk_pos);
		brk_del = $random;
		#(brk_del);
		brk_out <= 1;					// drive high and release;
		brk_oe <= #5 0;
		@(negedge brk);					// wait for device;
		hstrb <= 1;						// release host;
		@(posedge brk);
		brk_del = $random;
		#(brk_del);						// random delay to DIR=0;
		brk_term <= 1;
		#(40 - brk_del);				// tBRKH;
		brk_out <= 0;
		brk_oe <= 1;					// hsot drives break again;
		brk_term <= 0;
		brk_busy <= 0;
	end

endmodule
