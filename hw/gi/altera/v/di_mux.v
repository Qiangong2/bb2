module di_mux (
               
               di_in,
	            di_out,
	            di_oe,
	            di_brk_in,
	            di_brk_out,
	            di_brk_oe,
	            di_dir_in,
	            di_hstrb_in,
	            di_dstrb_out,
	            di_err_out,
	            di_cover_out,
	            di_reset_in,

               ais_clk_in,
               ais_lr_in,
               ais_d_out,

	            // primary disk interface,

	            pri_di_in,
	            pri_di_out,
	            pri_di_oe,
	            pri_di_brk_in,
	            pri_di_brk_out,
	            pri_di_brk_oe,
	            pri_di_dir_in,
	            pri_di_hstrb_in,
	            pri_di_dstrb_out,
	            pri_di_err_out,
	            pri_di_cover_out,
	            pri_di_reset_in,

               pri_ais_clk_in,
               pri_ais_lr_in,
               pri_ais_d_out,

	            // auxiliary disk interface,

	            aux_di_in,
	            aux_di_out,
	            aux_di_oe,
	            aux_di_brk_in,
	            aux_di_brk_out,
	            aux_di_brk_oe,
	            aux_di_dir_out,
	            aux_di_hstrb_out,
	            aux_di_dstrb_in,
	            aux_di_err_in,
	            aux_di_cover_in,
	            aux_di_reset_out,

               aux_ais_clk_out,
               aux_ais_lr_out,
               aux_ais_d_in,

               di_mux_sel
               );

   // disk interface with Flipper
   
	input [7:0]   di_in;		// DI data from input cell;
	output [7:0]  di_out;		// DI data to output cell;
	output        di_oe;				// DI output enable;
	input         di_brk_in;			// DI break from input cell;
	output        di_brk_out;		// DI break to output cell;
	output        di_brk_oe;			// DI break output enable;
	input         di_dir_in;				// DI direction;
	input         di_hstrb_in;			// DI host strobe;
	output        di_dstrb_out;			// DI drive strobe;
	output        di_err_out;				// DI error;
	output        di_cover_out;			// DI cover;
	input         di_reset_in;			// DI reset;

	input         ais_clk_in;			// AIS clock from GC;
	input         ais_lr_in;				// AIS left/right signal;
	output        ais_d_out;				// AIS data out;
   
	// primary disk interface, Flipper <-> GI
   
	output [7:0]  pri_di_in;			// DI data to GI
	input [7:0]   pri_di_out;			// DI data from GI
	input         pri_di_oe;				// DI output enable;
	output        pri_di_brk_in;		// DI break to GI
	input         pri_di_brk_out;		// DI break from GI
   input         pri_di_brk_oe;		// DI break output enable;
	output        pri_di_dir_in;		// DI direction;
	output        pri_di_hstrb_in;		// DI host strobe;
	input         pri_di_dstrb_out;	// DI drive strobe;
	input         pri_di_err_out;		// DI error;
	input         pri_di_cover_out;	// DI cover;
	output        pri_di_reset_in;		// DI reset;
   
	output        pri_ais_clk_in;			// AIS clock go GI;
	output        pri_ais_lr_in;				// AIS left/right signal to GI;
	input         pri_ais_d_out;				// AIS data out from GI;
   
	// auxiliary disk interface, Flipper <-> NROM Drive
   
	input [7:0]   aux_di_in;		   	// DI data from input cell;
	output [7:0]  aux_di_out;			// DI data to output cell;
	output        aux_di_oe;				// DI output enable;
	input         aux_di_brk_in;		// DI break from input cell;
	output        aux_di_brk_out;		// DI break to output cell;
	output        aux_di_brk_oe;		// DI break output enable;
	output        aux_di_dir_out;		// DI direction;
	output        aux_di_hstrb_out;	// DI host strobe;
	input         aux_di_dstrb_in;		// DI drive strobe;
	input         aux_di_err_in;		// DI error;
	input         aux_di_cover_in;		// DI cover;
	output        aux_di_reset_out;	// DI reset;
   
	output        aux_ais_clk_out;			// AIS clock from GC;
	output        aux_ais_lr_out;				// AIS left/right signal;
	input         aux_ais_d_in;				// AIS data out;
   
   // DI mux selector
   
   input         di_mux_sel;			// 1=Primary, 0=Auxiliary
   
   // outputs from GC (except reset & break) go to both interfaces
   
	assign        pri_di_in = di_in;
   assign        pri_di_brk_in = di_brk_in;
	assign        pri_di_dir_in = di_dir_in;
	assign        pri_di_hstrb_in = di_hstrb_in;

   assign        pri_ais_clk_in = ais_clk_in;
   assign        pri_ais_lr_in = ais_lr_in;
   
   assign        aux_di_out = di_in;
   assign        aux_di_oe = ~di_dir_in;
   assign        aux_di_dir_out = di_dir_in;
   assign        aux_di_hstrb_out = di_hstrb_in;

   assign        aux_ais_clk_out = ais_clk_in;
   assign        aux_ais_lr_out = ais_lr_in;
   
   // DI mux selector determines which interface is held in reset

   assign        pri_di_reset_in = di_mux_sel ? di_reset_in: 1'b0;
   assign        aux_di_reset_out = di_mux_sel ? 1'b0 : di_reset_in;

   // DI mux selector determines driver for GC inputs (except break) 
   
   assign        di_out = di_mux_sel ? pri_di_out : aux_di_in;
   assign        di_oe = di_mux_sel ? pri_di_oe : ~aux_di_oe;
   
   assign        di_dstrb_out = di_mux_sel ? pri_di_dstrb_out : aux_di_dstrb_in;
   
   assign        di_err_out = di_mux_sel ? pri_di_err_out : aux_di_err_in;
   assign        di_cover_out = di_mux_sel ? pri_di_cover_out : aux_di_cover_in;

   assign        ais_d_out = di_mux_sel ? pri_ais_d_out : aux_ais_d_in;
   
   // DI Break signal is wired to both devices (GI and NROM) outside
   // the FPGA; GI drives the Break signal output from the FPGA

   assign        di_brk_out = pri_di_brk_out;
   assign        di_brk_oe = di_mux_sel ? pri_di_brk_oe : aux_di_brk_oe;

   // placeholder in case we decide to actively drive BRK from FPGA <-> NROM

   assign        aux_di_brk_oe = 1'b0;
   
endmodule // di_mux
