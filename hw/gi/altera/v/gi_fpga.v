// sim.v Frank Berndt
// top level gi stand-alone simulator;
// :set tabstop=4

`timescale 1ps/1ps

module gi_fpga (
                RST,
                
                ALT_BOOT, 
                TEST_IN,
                SYS_RST,
                TEST_ENA,

                GC_RST,
                SEC_INTR,
                
//                PROM_SCL,
//                PROM_SDA,
                
                PCI_CLK, 
                PCI_RST, 
                PCI_REQ, 
                PCI_GNT, 
                PCI_OGNT, 
                PCI_INTR,
	             PCI_AD, 
                CBE,
	             PAR, 
                FRAME,
	             DEVSEL,
	             IRDY, 
                TRDY,
	             STOP,
                
	             DID,
	             DIBRK,
	             DIDIR, 
                DIHSTRBB, 
                DIDSTRBB, 
                DIERRB, 
                DICOVER, 
                DIRSTB,

                DI_MUX_SEL,
                 
	             AUX_DID,
	             AUX_DIBRK,
	             AUX_DIDIR, 
                AUX_DIHSTRBB, 
                AUX_DIDSTRBB, 
                AUX_DIERRB, 
                AUX_DICOVER, 
                AUX_DIRSTB,
                
	             AISCLK, 
                AISLR, 
                AISD,

	             AUX_AISCLK, 
                AUX_AISLR, 
                AUX_AISD,
                 
		PCI_CLK_OUT,
		MASTER_CLK_OUT

);
         
   // global reset
   input        RST;
   
   // system configuration
   input        ALT_BOOT; 
   input        TEST_IN;
   output       SYS_RST;
   output       TEST_ENA;
   
	// flipper control;
   
	output       GC_RST;
	output       SEC_INTR;

   // I2C interface for serial EEPROM
//   output       PROM_SCL;
//   inout        PROM_SDA;
   
   // PCI interface to ALi core
   input        PCI_CLK; 
   input        PCI_RST; 
   output       PCI_REQ; 
   input        PCI_GNT; 
   input        PCI_OGNT; 
   output       PCI_INTR;
	inout [31:0] PCI_AD; 
   inout [3:0]  CBE;
	inout        PAR; 
   inout        FRAME;
	inout        DEVSEL;
	inout        IRDY; 
   inout        TRDY;
	inout        STOP;
   
   // DI Interface to Flipper
	inout [7:0]  DID;
	inout        DIBRK;
	input        DIDIR; 
   input        DIHSTRBB; 
   output       DIDSTRBB; 
   output       DIERRB; 
   output       DICOVER; 
   input        DIRSTB;

   // DI Interface to external drive
   
	inout [7:0]  AUX_DID;
	inout        AUX_DIBRK;
	output       AUX_DIDIR; 
   output       AUX_DIHSTRBB; 
   input        AUX_DIDSTRBB; 
   input        AUX_DIERRB; 
   input        AUX_DICOVER; 
   output       AUX_DIRSTB;
   
   input        DI_MUX_SEL;
   
   // AIS Interface to Flipper
	input        AISCLK; 
   input        AISLR; 
   output       AISD;
   
   // AIS Interface to NROM
	output       AUX_AISCLK; 
   output       AUX_AISLR; 
   input        AUX_AISD;
   
   // Test clocks for Altera
	output PCI_CLK_OUT;
	output MASTER_CLK_OUT;
   
	wire         clk;					// gi core clock;
	wire         clk_lock;				// clk is stable, pll locked;
	wire         pin_rst_in;				// reset from pin;
	wire         sys_rst_out;				// system reset;
	wire         cpu_rst;				// cpu reset, active 0;
	wire         alt_boot_in;				// alternative boot;
	wire         test_in_in;				// test enable pin on engr chips;
	wire         test_ena_out;			// enable test/debug logic;
   
	// MCM i2c serial flash;
   
	wire         i2c_clk_out;				// i2c clock;
	wire         i2c_din;				// i2c data from io cell;
	wire         i2c_dout;			// i2c data to io cell;
	wire         i2c_doe;				// i2c data output enable;
   
	// flipper control;
   
	wire         gc_rst_out;				// reset to flipper;
	wire         sec_intr_out;			// cause secure exception;
	wire         sec_ack;				// secure exception fetch;
   
	// pci interface;
   
	wire         pci_clk_in;				// gi pci clock;
	wire         pci_clk_dcm;
	wire         pci_rst_in;				// pci bus reset;
	wire [31:0]  ad_in;		// addr/data inputs;
	wire [31:0]  ad_out;	// addr/data outputs;
	wire         ad_oe;				// addr/data output enable;
	wire [3:0]   cbe_in;		// byte enable inputs;
	wire [3:0]   cbe_out;	// byte enable outputs;
	wire         cbe_oe;				// byte enable output enable;
	wire         par_in;				// parity from master;
	wire         par_out;			// parity when master;
	wire         par_oe;				// parity output enable;
	wire         frame_in;			// frame when target;
	wire         frame_out;			// frame when master;
	wire         frame_oe;			// frame output enable;
	wire         devsel_in;			// device selected;
	wire         devsel_out;		// device select;
	wire         devsel_oe;			// device select output enable;
	wire         irdy_in;				// initiator ready;
	wire         irdy_out;			// ready when master;
	wire         irdy_oe;			// irdy output enable;
	wire         trdy_in;				// target ready;
	wire         trdy_out;			// when target;
	wire         trdy_oe;			// trdy output enable;
	wire         stop_in;			// initiator stops transfer;
	wire         stop_out;			// target stops;
	wire         stop_oe;			// stop output enable;
	wire         pci_req_out;			// pci bus request;
	wire         pci_gnt_in;				// bus granted;
	wire         pci_ognt_in;			// OR of other grants, not host;
	wire         pci_intr_out;			// interrupt to processor;
   
	// disk interface;
   
	wire [7:0]   di_in;		// DI data from input cell;
	wire [7:0]   di_out;		// DI data to output cell;
	wire         di_oe;				// DI output enable;
	wire         di_brk_in;			// DI break from input cell;
	wire         di_brk_out;		// DI break to output cell;
	wire         di_brk_oe;			// DI break output enable;
	wire         di_dir_in;				// DI direction;
	wire         di_hstrb_in;			// DI host strobe;
	wire         di_dstrb_out;			// DI drive strobe;
	wire         di_err_out;				// DI error;
	wire         di_cover_out;			// DI cover;
	wire         di_reset_in;			// DI reset;

	// primary disk interface;
   
	wire [7:0]   pri_di_in;				// DI data from input cell;
	wire [7:0]   pri_di_out;			// DI data to output cell;
	wire         pri_di_oe;				// DI output enable;
	wire         pri_di_brk_in;		// DI break from input cell;
	wire         pri_di_brk_out;		// DI break to output cell;
	wire         pri_di_brk_oe;		// DI break output enable;
	wire         pri_di_dir_in;		// DI direction;
	wire         pri_di_hstrb_in;		// DI host strobe;
	wire         pri_di_dstrb_out;	// DI drive strobe;
	wire         pri_di_err_out;		// DI error;
	wire         pri_di_cover_out;	// DI cover;
	wire         pri_di_reset_in;		// DI reset;

	// auxiliary disk interface;
   
	wire [7:0]   aux_di_in;		   	// DI data from input cell;
	wire [7:0]   aux_di_out;			// DI data to output cell;
	wire         aux_di_oe;				// DI output enable;
	wire         aux_di_brk_in;		// DI break from input cell;
	wire         aux_di_brk_out;		// DI break to output cell;
	wire         aux_di_brk_oe;		// DI break output enable;
	wire         aux_di_dir_out;		// DI direction;
	wire         aux_di_hstrb_out;	// DI host strobe;
	wire         aux_di_dstrb_in;		// DI drive strobe;
	wire         aux_di_err_in;		// DI error;
	wire         aux_di_cover_in;		// DI cover;
	wire         aux_di_reset_out;	// DI reset;

   wire         di_mux_sel_in;      // selector for primary/auxiliary DI mux
          
	// ais interface;
   
	wire         ais_clk_in;			// AIS clock from GC;
	wire         ais_lr_in;				// AIS left/right signal;
	wire         ais_d_out;				// AIS data out;

	wire         pri_ais_clk_in;
	wire         pri_ais_lr_in;
	wire         pri_ais_d_out;

	wire         aux_ais_clk_out;
	wire         aux_ais_lr_out;
	wire         aux_ais_d_in;

   // gi_pci inputs that must be tied off or generated --
   // cpu_rst
   // sec_ack
   // pci_ognt

   assign       cpu_rst = 1'b1;
   assign       sec_ack = 1'b0;     // CPU's ack is tied inactive
//   assign       pci_ognt = 1'b0;    // allow GI to respond to any PCI requestor

   // have PIN_RST also trigger a PCI_RST
	wire	pci_rst_or_pin_rst_in = pci_rst_in & pin_rst_in;  // logic AND since signals are low-true 

   // clock generation

	pll_99mhz u_pll_99mhz(
		.inclk0(pci_clk_in),	// 33.0MHz
		.areset(~pci_rst_or_pin_rst_in),
                .c0(pci_clk_dcm),	// 33.0MHz
		.c1(clk),		// 99.0MHz
		.locked(clk_lock)
		);

   assign       PCI_CLK_OUT = pci_clk_dcm;
   assign       MASTER_CLK_OUT = clk;

   // DI mux

   di_mux di_mux (
                  .di_in(di_in),
                  .di_out(di_out),
                  .di_oe(di_oe),
                  .di_brk_in(di_brk_in),
                  .di_brk_out(di_brk_out),
                  .di_brk_oe(di_brk_oe),
                  .di_dir_in(di_dir_in),
                  .di_hstrb_in(di_hstrb_in),
                  .di_dstrb_out(di_dstrb_out),
                  .di_err_out(di_err_out),
                  .di_cover_out(di_cover_out),
                  .di_reset_in(di_reset_in),
                  .ais_clk_in(ais_clk_in),
                  .ais_lr_in(ais_lr_in),
                  .ais_d_out(ais_d_out),
                  .pri_di_in(pri_di_in),
                  .pri_di_out(pri_di_out),
                  .pri_di_oe(pri_di_oe),
                  .pri_di_brk_in(pri_di_brk_in),
                  .pri_di_brk_out(pri_di_brk_out),
                  .pri_di_brk_oe(pri_di_brk_oe),
                  .pri_di_dir_in(pri_di_dir_in),
                  .pri_di_hstrb_in(pri_di_hstrb_in),
                  .pri_di_dstrb_out(pri_di_dstrb_out),
                  .pri_di_err_out(pri_di_err_out),
                  .pri_di_cover_out(pri_di_cover_out),
                  .pri_di_reset_in(pri_di_reset_in),
                  .pri_ais_clk_in(pri_ais_clk_in),
                  .pri_ais_lr_in(pri_ais_lr_in),
                  .pri_ais_d_out(pri_ais_d_out),
                  .aux_di_in(aux_di_in),
                  .aux_di_out(aux_di_out),
                  .aux_di_oe(aux_di_oe),
                  .aux_di_brk_in(aux_di_brk_in),
                  .aux_di_brk_out(aux_di_brk_out),
                  .aux_di_brk_oe(aux_di_brk_oe),
                  .aux_di_dir_out(aux_di_dir_out),
                  .aux_di_hstrb_out(aux_di_hstrb_out),
                  .aux_di_dstrb_in(aux_di_dstrb_in),
                  .aux_di_err_in(aux_di_err_in),
                  .aux_di_cover_in(aux_di_cover_in),
                  .aux_di_reset_out(aux_di_reset_out),
                  .aux_ais_clk_out(aux_ais_clk_out),
                  .aux_ais_lr_out(aux_ais_lr_out),
                  .aux_ais_d_in(aux_ais_d_in),
                  .di_mux_sel(di_mux_sel_in)
                  );
   
   // main GI core w/ PCI interface
   
	gi_pci gi_pci (
		.clk(clk),
		.clk_lock(clk_lock),
		.pin_rst(pci_rst_or_pin_rst_in),
		.sys_rst(sys_rst_out),
		.cpu_rst(cpu_rst),
		.alt_boot(alt_boot_in),
		.test_in(test_in_in),
		.test_ena(test_ena_out),
      .i2c_clk(i2c_clk_out),
      .i2c_din(i2c_din),
      .i2c_dout(i2c_dout),
      .i2c_doe(i2c_doe),
		.gc_rst(gc_rst_out), //XXX
		.sec_intr(sec_intr_out),
		.sec_ack(sec_ack),
		.pci_clk(pci_clk_dcm),
		.pci_rst(pci_rst_or_pin_rst_in),
		.ad_in(ad_in),
		.ad_out(ad_out),
		.ad_oe(ad_oe),
		.cbe_in(cbe_in),
		.cbe_out(cbe_out),
		.cbe_oe(cbe_oe),
		.par_in(par_in),
		.par_out(par_out),
		.par_oe(par_oe),
		.frame_in(frame_in),
		.frame_out(frame_out),
		.frame_oe(frame_oe),
		.devsel_in(devsel_in),
		.devsel_out(devsel_out),
		.devsel_oe(devsel_oe),
		.irdy_in(irdy_in),
		.irdy_out(irdy_out),
		.irdy_oe(irdy_oe),
		.trdy_in(trdy_in),
		.trdy_out(trdy_out),
		.trdy_oe(trdy_oe),
		.stop_in(stop_in),
		.stop_out(stop_out),
		.stop_oe(stop_oe),
		.pci_req(pci_req_out),
		.pci_gnt(pci_gnt_in),
		.pci_ognt(pci_ognt_in),
		.pci_intr(pci_intr_out),
		.di_in(pri_di_in),
		.di_out(pri_di_out),
		.di_oe(pri_di_oe),
		.di_brk_in(pri_di_brk_in),
		.di_brk_out(pri_di_brk_out),
		.di_brk_oe(pri_di_brk_oe),
		.di_dir(pri_di_dir_in),
		.di_hstrb(pri_di_hstrb_in),
		.di_dstrb(pri_di_dstrb_out),
		.di_err(pri_di_err_out),
		.di_cover(pri_di_cover_out),
		.di_reset(pri_di_reset_in),
		.ais_clk(pri_ais_clk_in),
		.ais_lr(pri_ais_lr_in),
		.ais_d(pri_ais_d_out)
	);

   // I/O Pads

iopads iopads (
	 .AISCLK              (AISCLK),
	 .AISLR               (AISLR),
	 .ALT_BOOT            (ALT_BOOT),
	 .AUX_AISD            (AUX_AISD),
	 .AUX_DICOVER         (AUX_DICOVER),
	 .AUX_DIDSTRBB        (AUX_DIDSTRBB),
	 .AUX_DIERRB          (AUX_DIERRB),
	 .DIDIR               (DIDIR),
	 .DIHSTRBB            (DIHSTRBB),
	 .DIRSTB              (DIRSTB),
	 .DI_MUX_SEL          (DI_MUX_SEL),
	 .PCI_CLK             (PCI_CLK),
	 .PCI_GNT             (PCI_GNT),
	 .PCI_OGNT            (PCI_OGNT),
	 .PCI_RST             (PCI_RST),
	 .RST                 (RST),
	 .TEST_IN             (TEST_IN),
	 .ad_oe               (ad_oe),
	 .ad_out              (ad_out[31:0]),
	 .ais_d_out           (ais_d_out),
	 .aux_ais_clk_out     (aux_ais_clk_out),
	 .aux_ais_lr_out      (aux_ais_lr_out),
	 .aux_di_brk_oe       (aux_di_brk_oe),
	 .aux_di_brk_out      (aux_di_brk_out),
	 .aux_di_dir_out      (aux_di_dir_out),
	 .aux_di_hstrb_out    (aux_di_hstrb_out),
	 .aux_di_oe           (aux_di_oe),
	 .aux_di_out          (aux_di_out[7:0]),
	 .aux_di_reset_out    (aux_di_reset_out),
	 .cbe_oe              (cbe_oe),
	 .cbe_out             (cbe_out[3:0]),
	 .devsel_oe           (devsel_oe),
	 .devsel_out          (devsel_out),
	 .di_brk_oe           (di_brk_oe),
	 .di_brk_out          (di_brk_out),
	 .di_cover_out        (di_cover_out),
	 .di_dstrb_out        (di_dstrb_out),
	 .di_err_out          (di_err_out),
	 .di_oe               (di_oe),
	 .di_out              (di_out[7:0]),
	 .frame_oe            (frame_oe),
	 .frame_out           (frame_out),
	 .gc_rst_out          (gc_rst_out),
	 .i2c_clk_out         (i2c_clk_out),
	 .i2c_doe             (i2c_doe),
	 .i2c_dout            (i2c_dout),
	 .irdy_oe             (irdy_oe),
	 .irdy_out            (irdy_out),
	 .par_oe              (par_oe),
	 .par_out             (par_out),
	 .intr_oe             (pci_intr_out),
	 .pci_intr_out        (~pci_intr_out),
	 .pci_req_out         (pci_req_out),
	 .sec_intr_out        (sec_intr_out),
	 .stop_oe             (stop_oe),
	 .stop_out            (stop_out),
	 .sys_rst_out         (sys_rst_out),
	 .test_ena_out        (test_ena_out),
	 .trdy_oe             (trdy_oe),
	 .trdy_out            (trdy_out),


	 .AISD                (AISD),
	 .AUX_AISCLK          (AUX_AISCLK),
	 .AUX_AISLR           (AUX_AISLR),
	 .AUX_DIDIR           (AUX_DIDIR),
	 .AUX_DIHSTRBB        (AUX_DIHSTRBB),
	 .AUX_DIRSTB          (AUX_DIRSTB),
	 .DICOVER             (DICOVER),
	 .DIDSTRBB            (DIDSTRBB),
	 .DIERRB              (DIERRB),
	 .GC_RST              (GC_RST),
	 .PCI_INTR            (PCI_INTR),
	 .PCI_REQ             (PCI_REQ),
	 .PROM_SCL            (PROM_SCL),
	 .SEC_INTR            (SEC_INTR),
	 .SYS_RST             (SYS_RST),
	 .TEST_ENA            (TEST_ENA),
	 .ad_in               (ad_in[31:0]),
	 .ais_clk_in          (ais_clk_in),
	 .ais_lr_in           (ais_lr_in),
	 .alt_boot_in         (alt_boot_in),
	 .aux_ais_d_in        (aux_ais_d_in),
	 .aux_di_brk_in       (aux_di_brk_in),
	 .aux_di_cover_in     (aux_di_cover_in),
	 .aux_di_dstrb_in     (aux_di_dstrb_in),
	 .aux_di_err_in       (aux_di_err_in),
	 .aux_di_in           (aux_di_in[7:0]),
	 .cbe_in              (cbe_in[3:0]),
	 .devsel_in           (devsel_in),
	 .di_brk_in           (di_brk_in),
	 .di_dir_in           (di_dir_in),
	 .di_hstrb_in         (di_hstrb_in),
	 .di_in               (di_in[7:0]),
	 .di_mux_sel_in       (di_mux_sel_in),
	 .di_reset_in         (di_reset_in),
	 .frame_in            (frame_in),
	 .i2c_din             (i2c_din),
	 .irdy_in             (irdy_in),
	 .par_in              (par_in),
	 .pci_clk_in          (pci_clk_in),
	 .pci_gnt_in          (pci_gnt_in),
	 .pci_ognt_in         (pci_ognt_in),
	 .pci_rst_in          (pci_rst_in),
	 .pin_rst_in          (pin_rst_in),
	 .stop_in             (stop_in),
	 .test_in_in          (test_in_in),
	 .trdy_in             (trdy_in),


	 .AUX_DIBRK           (AUX_DIBRK),
	 .AUX_DID             (AUX_DID[7:0]),
	 .CBE                 (CBE[3:0]),
	 .DEVSEL              (DEVSEL),
	 .DIBRK               (DIBRK),
	 .DID                 (DID[7:0]),
	 .FRAME               (FRAME),
	 .IRDY                (IRDY),
	 .PAR                 (PAR),
	 .PCI_AD              (PCI_AD[31:0]),
	 .PROM_SDA            (PROM_SDA),
	 .STOP                (STOP),
	 .TRDY                (TRDY)


);

endmodule
