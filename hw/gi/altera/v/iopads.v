
module iopads (

	AISCLK,
	AISLR,
	ALT_BOOT,
	AUX_AISD,
	AUX_DICOVER,
	AUX_DIDSTRBB,
	AUX_DIERRB,
	DIDIR,
	DIHSTRBB,
	DIRSTB,
	DI_MUX_SEL,
	PCI_CLK,
	PCI_GNT,
	PCI_OGNT,
	PCI_RST,
	RST,
	TEST_IN,
	ad_oe,
	ad_out,
	ais_d_out,
	aux_ais_clk_out,
	aux_ais_lr_out,
	aux_di_brk_oe,
	aux_di_brk_out,
	aux_di_dir_out,
	aux_di_hstrb_out,
	aux_di_oe,
	aux_di_out,
	aux_di_reset_out,
	cbe_oe,
	cbe_out,
	devsel_oe,
	devsel_out,
	di_brk_oe,
	di_brk_out,
	di_cover_out,
	di_dstrb_out,
	di_err_out,
	di_oe,
	di_out,
	frame_oe,
	frame_out,
	gc_rst_out,
	i2c_clk_out,
	i2c_doe,
	i2c_dout,
	irdy_oe,
	irdy_out,
	par_oe,
	par_out,
	intr_oe,
	pci_intr_out,
	pci_req_out,
	sec_intr_out,
	stop_oe,
	stop_out,
	sys_rst_out,
	test_ena_out,
	trdy_oe,
	trdy_out,
	AISD,
	AUX_AISCLK,
	AUX_AISLR,
	AUX_DIDIR,
	AUX_DIHSTRBB,
	AUX_DIRSTB,
	DICOVER,
	DIDSTRBB,
	DIERRB,
	GC_RST,
	PCI_INTR,
	PCI_REQ,
	PROM_SCL,
	SEC_INTR,
	SYS_RST,
	TEST_ENA,
	ad_in,
	ais_clk_in,
	ais_lr_in,
	alt_boot_in,
	aux_ais_d_in,
	aux_di_brk_in,
	aux_di_cover_in,
	aux_di_dstrb_in,
	aux_di_err_in,
	aux_di_in,
	cbe_in,
	devsel_in,
	di_brk_in,
	di_dir_in,
	di_hstrb_in,
	di_in,
	di_mux_sel_in,
	di_reset_in,
	frame_in,
	i2c_din,
	irdy_in,
	par_in,
	pci_clk_in,
	pci_gnt_in,
	pci_ognt_in,
	pci_rst_in,
	pin_rst_in,
	stop_in,
	test_in_in,
	trdy_in,
	AUX_DIBRK,
	AUX_DID,
	CBE,
	DEVSEL,
	DIBRK,
	DID,
	FRAME,
	IRDY,
	PAR,
	PCI_AD,
	PROM_SDA,
	STOP,
	TRDY

);

	 input AISCLK;
	 input AISLR;
	 input ALT_BOOT;
	 input AUX_AISD;
	 input AUX_DICOVER;
	 input AUX_DIDSTRBB;
	 input AUX_DIERRB;
	 input DIDIR;
	 input DIHSTRBB;
	 input DIRSTB;
	 input DI_MUX_SEL;
	 input PCI_CLK;
	 input PCI_GNT;
	 input PCI_OGNT;
	 input PCI_RST;
	 input RST;
	 input TEST_IN;
	 input ad_oe;
	 input [31:0] ad_out;
	 input ais_d_out;
	 input aux_ais_clk_out;
	 input aux_ais_lr_out;
	 input aux_di_brk_oe;
	 input aux_di_brk_out;
	 input aux_di_dir_out;
	 input aux_di_hstrb_out;
	 input aux_di_oe;
	 input [7:0] aux_di_out;
	 input aux_di_reset_out;
	 input cbe_oe;
	 input [3:0] cbe_out;
	 input devsel_oe;
	 input devsel_out;
	 input di_brk_oe;
	 input di_brk_out;
	 input di_cover_out;
	 input di_dstrb_out;
	 input di_err_out;
	 input di_oe;
	 input [7:0] di_out;
	 input frame_oe;
	 input frame_out;
	 input gc_rst_out;
	 input i2c_clk_out;
	 input i2c_doe;
	 input i2c_dout;
	 input irdy_oe;
	 input irdy_out;
	 input par_oe;
	 input par_out;
	 input intr_oe;
	 input pci_intr_out;
	 input pci_req_out;
	 input sec_intr_out;
	 input stop_oe;
	 input stop_out;
	 input sys_rst_out;
	 input test_ena_out;
	 input trdy_oe;
	 input trdy_out;


	 output AISD;
	 output AUX_AISCLK;
	 output AUX_AISLR;
	 output AUX_DIDIR;
	 output AUX_DIHSTRBB;
	 output AUX_DIRSTB;
	 output DICOVER;
	 output DIDSTRBB;
	 output DIERRB;
	 output GC_RST;
	 output PCI_INTR;
	 output PCI_REQ;
	 output PROM_SCL;
	 output SEC_INTR;
	 output SYS_RST;
	 output TEST_ENA;
	 output [31:0] ad_in;
	 output ais_clk_in;
	 output ais_lr_in;
	 output alt_boot_in;
	 output aux_ais_d_in;
	 output aux_di_brk_in;
	 output aux_di_cover_in;
	 output aux_di_dstrb_in;
	 output aux_di_err_in;
	 output [7:0] aux_di_in;
	 output [3:0] cbe_in;
	 output devsel_in;
	 output di_brk_in;
	 output di_dir_in;
	 output di_hstrb_in;
	 output [7:0] di_in;
	 output di_mux_sel_in;
	 output di_reset_in;
	 output frame_in;
	 output i2c_din;
	 output irdy_in;
	 output par_in;
	 output pci_clk_in;
	 output pci_gnt_in;
	 output pci_ognt_in;
	 output pci_rst_in;
	 output pin_rst_in;
	 output stop_in;
	 output test_in_in;
	 output trdy_in;


	 inout AUX_DIBRK;
	 inout [7:0] AUX_DID;
	 inout [3:0] CBE;
	 inout DEVSEL;
	 inout DIBRK;
	 inout [7:0] DID;
	 inout FRAME;
	 inout IRDY;
	 inout PAR;
	 inout [31:0] PCI_AD;
	 inout PROM_SDA;
	 inout STOP;
	 inout TRDY;


assign pin_rst_in = RST;

assign alt_boot_in = ALT_BOOT;

assign test_in_in = TEST_IN;

assign SYS_RST = sys_rst_out;

assign TEST_ENA = test_ena_out;

assign GC_RST = gc_rst_out;

assign SEC_INTR = sec_intr_out;

assign PROM_SCL = i2c_clk_out;

assign PROM_SDA = i2c_doe ? i2c_dout: 1'bZ;
assign i2c_din = PROM_SDA;

assign pci_clk_in = PCI_CLK;

assign pci_rst_in = PCI_RST;

assign PCI_REQ = pci_req_out;

assign pci_gnt_in = PCI_GNT;

assign pci_ognt_in = PCI_OGNT;

assign PCI_INTR = intr_oe ? pci_intr_out: 1'bZ;

assign PCI_AD = ad_oe ? ad_out: 32'bZ;
assign ad_in = PCI_AD;

assign CBE = cbe_oe ? cbe_out: 4'bZ;
assign cbe_in = CBE;

assign PAR = par_oe ? par_out : 1'bZ;
assign par_in = PAR;

assign FRAME = frame_oe ? frame_out : 1'bZ;
assign frame_in = FRAME;

assign DEVSEL = devsel_oe ? devsel_out : 1'bZ;
assign devsel_in = DEVSEL;

assign IRDY = irdy_oe ? irdy_out : 1'bZ;
assign irdy_in = IRDY;

assign TRDY = trdy_oe ? trdy_out : 1'bZ;
assign trdy_in = TRDY;

assign STOP = stop_oe ? stop_out : 1'bZ;
assign stop_in = STOP;

assign DID = di_oe ? di_out : 8'bZ;
assign di_in = DID;

assign DIBRK = di_brk_oe ? di_brk_out : 1'bZ;
assign di_brk_in = DIBRK;

assign di_dir_in = DIDIR;

assign di_hstrb_in = DIHSTRBB;

assign DIDSTRBB = di_dstrb_out;

assign DIERRB = di_err_out;

assign DICOVER = di_cover_out;

assign di_reset_in = DIRSTB;

assign AUX_DID = aux_di_oe ? aux_di_out : 8'bZ;
assign aux_di_in = AUX_DID;

assign AUX_DIBRK = aux_di_brk_oe ? aux_di_brk_out : 1'bZ;
assign aux_di_brk_in = AUX_DIBRK;

assign AUX_DIDIR = aux_di_dir_out;

assign AUX_DIHSTRBB = aux_di_hstrb_out;

assign aux_di_dstrb_in = AUX_DIDSTRBB;

assign aux_di_err_in = AUX_DIERRB;

assign aux_di_cover_in = AUX_DICOVER;

assign AUX_DIRSTB = aux_di_reset_out;

assign di_mux_sel_in = DI_MUX_SEL;

assign ais_clk_in = AISCLK;

assign ais_lr_in = AISLR;

assign AISD = ais_d_out;

assign AUX_AISCLK = aux_ais_clk_out;

assign AUX_AISLR = aux_ais_lr_out;

assign aux_ais_d_in = AUX_AISD;



endmodule
