
# VCS definitions;
# VCS_HOME is the path to the vcs tool root;
# VCS_NAME is the name of the executable;
# VCS is the path to the executable;
# allow user to overwrite all of them;

VCS_HOME ?= /ecad/vcs
VCS_NAME ?= vcs
VCS_LIB ?= $(VCS_HOME)/intel_i686_linux_2.2/lib
VCS ?= $(VCS_HOME)/bin/$(VCS_NAME)

# VIRSIM definitions;

VIRSIMHOME = $(VCS_HOME)/virsimdir
VCD2VPD = $(VCS_HOME)/virsimdir/bin/vcd2vpd

# Directory to store verilog output files (sim and c files).
# User can overide this variable on make command line, ie.
# 'make SIMDIR=/tmp' or environment variable.

SIMDIR ?= .

# simulator definitions;

SIMDEFS += 

# run-time flags;

#SIMDUMP ?= +dump_all
SIMDUMP ?= 
SIMMON ?=
SIMTEST ?= +test_all 

# all vcs dirt;

VCS_DIRT = $(VCS_NAME).log \
	$(SIMDIR)/c-sys \
	$(SIMDIR)/c-pci \
	$(SIMDIR)/c-pci-fpga \
	$(SIMDIR)/c-pci-fpga-dimux \
	$(SIMDIR)/sim_sys \
	$(SIMDIR)/sim_pci \
	$(SIMDIR)/sim_pci_fpga \
	$(SIMDIR)/sim_pci_fpga_dimux \
	verilog.dump

# all simulation dirt;

SIM_DIRT = 	$(SIMDIR)/runs-sys \
	$(SIMDIR)/runs-pci \
	$(SIMDIR)/runs-pci-fpga \
	$(SIMDIR)/runs-pci-fpga-dimux

# vcs options;

VCS_OPTS = -l $(VCS_NAME).log \
	-M -Mupdate \
	+define+SIM \
	$(SIMDEFS) \
	+vcs+lic+wait \
	-V \
	+libext+.v+.vp+.VP \
	+incdir+../v \
	+incdir+../lib \
	+incdir+../../lib/opencores/aes/rtl/verilog \
	-y . \
	-y ../v \
	-y ../lib \
	-y ../../lib/virage/charge_pump \
	-y ../../lib/virage/novea \
	-y ../../lib/virage/nms128x32 \
	-y ../../lib/opencores/aes/rtl/verilog

# additional vcs options for fpga simulations;

FPGA_OPTS =	-y $(XILINX)/verilog/src \
	-y $(XILINX)/verilog/src/unisims \
	-y $(XILINX)/verilog/src/simprims \
	-y $(XILINX)/verilog/src/XilinxCoreLib \
	-y ../fpga \
	$(XILINX)/verilog/src/glbl.v 


# default target;
# build the stand-alone gi simulator;

TARGETS = test_sys test_pci
FPGA_TARGETS = test_pci_fpga test_pci_fpga_dimux

default install: sim_sys

all: $(TARGETS)

fpga: $(FPGA_TARGETS)

# help

help: _always
	@echo "Make targets are as follows --"
	@echo ""
	@echo " <default>            - build simulator, bi transport"
	@echo ""
	@echo " all                  - build/run sim_sys and sim_pci"
	@echo " fpga                 - build/run sim_pci_fpga and sim_pci_fpga_dimux"
	@echo ""
	@echo " clean                - clean up all vcs temporaries"
	@echo " distclean            - clean up all vcs temporaries and simulation outputs"
	@echo ""
	@echo " sim_sys              - build simulator, bi transport"
	@echo " sim_pci              - build simulator, pci transport"
	@echo " sim_pci_fpga         - build simulator, pci transport, fpga"
	@echo " sim_pci_fpga_dimux   - build simulator, pci transport, fpga, dimux test config"
	@echo ""
	@echo " test_sys             - run tests, bi transport"
	@echo " test_pci             - run tests, pci transport"
	@echo " test_pci_fpga        - run tests, pci transport, fpga"
	@echo " test_pci_fpga_dimux  - run tests, pci transport, fpga, dimux test config"

# clean all vcs files;

clean: _always
	rm -rf $(VCS_DIRT)

# clean all vcs files and simulation output files;

distclean: _always
	rm -rf $(VCS_DIRT) $(SIM_DIRT)

# always dependency;

_always:

# compile the behavioral simulator;
# bi system interface transport;

sim_sys: _always
	$(VCS) $(VCS_OPTS) \
		-Mdir=$(SIMDIR)/c-sys \
		+define+GI_SRAM_MODEL \
		+define+GI_ROM_MODEL \
		-o $(SIMDIR)/$@  \
		sim.v

# pci transport;

sim_pci: _always
	$(VCS) $(VCS_OPTS) \
		-Mdir=$(SIMDIR)/c-pci \
		+define+GI_PCI \
		+define+GI_SRAM_MODEL \
		+define+GI_ROM_MODEL \
		-o $(SIMDIR)/$@  \
		sim.v

sim_pci_fpga: _always
	$(VCS) $(VCS_OPTS) $(FPGA_OPTS) \
		-Mdir=$(SIMDIR)/c-pci-fpga \
		+define+GI_PCI \
		+define+FPGA \
		-o $(SIMDIR)/$@  \
		-notice \
		sim.v

sim_pci_fpga_dimux: _always
	$(VCS) $(VCS_OPTS) $(FPGA_OPTS) \
		-Mdir=$(SIMDIR)/c-pci-fpga-dimux \
		+define+GI_PCI \
		+define+FPGA \
		+define+DI_MUX \
		-o $(SIMDIR)/$@  \
		-notice \
		sim.v

# generate html hypertext version of RTL
v2html_pci_fpga:	_always
	if [ ! -d v2html_pci_fpga ]; then mkdir v2html_pci_fpga; fi
	v2html $(VCS_OPTS) $(FPGA_OPTS) \
		-Mdir=$(SIMDIR)/c-pci-fpga \
		+define+GI_PCI \
		+define+FPGA \
		-o v2html_pci_fpga \
		sim.v

# convert dump to vpd;

sim.vpd: verilog.dump
	$(VCD2VPD) verilog.dump $@

# run gi tests;
# bi transport;

test_sys: sim_sys
	if [ ! -d runs-sys ]; then mkdir runs-sys; fi
	./sim_sys +vcs+lic+wait \
		$(SIMDUMP) \
		$(SIMMON) \
		$(SIMTEST) \
		> $(SIMDIR)/runs-sys/test.log
	-$(VCD2VPD) verilog.dump $(SIMDIR)/runs-sys/test.vpd

# pci transport;

test_pci_fpga: sim_pci_fpga
	if [ ! -d runs-pci-fpga ]; then mkdir runs-pci-fpga; fi
	./sim_pci_fpga +vcs+lic+wait \
		$(SIMDUMP) \
		$(SIMMON) \
		$(SIMTEST) \
		> $(SIMDIR)/runs-pci-fpga/test.log
	-$(VCD2VPD) verilog.dump $(SIMDIR)/runs-pci-fpga/test.vpd

# pci transport; fpga top-level
test_pci_fpga_dimux: sim_pci_fpga_dimux
	if [ ! -d runs-pci-fpga-dimux ]; then mkdir runs-pci-fpga-dimux; fi
	./sim_pci_fpga_dimux +vcs+lic+wait \
		$(SIMDUMP) \
		$(SIMMON) \
		$(SIMTEST) \
		> $(SIMDIR)/runs-pci-fpga-dimux/test.log
	-$(VCD2VPD) verilog.dump $(SIMDIR)/runs-pci-fpga-dimux/test.vpd

# pci transport; fpga top-level; di mux test configuration
test_pci: sim_pci
	if [ ! -d runs-pci ]; then mkdir runs-pci; fi
	./sim_pci +vcs+lic+wait \
		$(SIMDUMP) \
		$(SIMMON) \
		$(SIMTEST) \
		> $(SIMDIR)/runs-pci/test.log
	-$(VCD2VPD) verilog.dump $(SIMDIR)/runs-pci/test.vpd

