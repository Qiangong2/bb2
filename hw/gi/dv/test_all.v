	begin : gi_tests
		reg [31:0] seed [0:1];	// random seed;
		reg seedx;				// seed has Xs in it;
		reg all;

		// initialize seed from cmd line;

		$readmemh("seed", seed);
		seedx = ^seed[0];
		if((seedx === 1'b0) | (seedx === 1'b1)) begin
			$display("test: %M: seed %0d", seed[0]);
			seed[1] = $random(seed[0]);
		end

		// set alt_boot to 0 on ALI sim;
		// this should not do anything, because alt_boot
		// is latched into sec_mode[] coming out of reset;

`ifdef	ALI_GI
		`gi_sim_top.alt_boot = 0;
		`gi_sim_top.p_target_off = 1;
`endif	// ALI_GI

		// wait for gi out of reset;
		// init virage earom array;

		v_init($random);

		// wait for go;
		// align to posedge of clock;

		@(posedge `xclk);

		// launch individual tests;

		all = $test$plusargs("test_all");
		if(all | $test$plusargs("test_gi"))
			test_gi(all);

		// give simulator more time to settle;

		repeat(100) @(posedge clk);

		`ifdef PERF_MON

                $display("total DI bus activity time: %0d ns", `total_time);
                $display("error event time: %0d ns", `err_acc);
                $display("break event time: %0d ns", `brk_acc);

                $display("approx data flipper to drive: %0d bytes", `dir0_count);
                $display("approx data drive to flipper: %0d bytes", `dir1_count);

                $display("transfer time while dir=0: %0d ns", `dir0_time);
                $display("total time dir=0: %0d ns", `dir0_acc);
                $display("flipper use of dir=0 bandwidth: %0d percent", `dir0_eff);

                $display("transfer time while dir=1: %0d ns", `dir1_time);
                $display("total time dir=1: %0d ns", `dir1_acc);
                $display("drive use of dir=1 bandwidth: %0d percent", `dir1_eff);

                $display("overall DI bus bandwidth usage: %0d percent", `total_eff);

                if (`di_brk == 1'b1)
                        $display("host B R E A K event not complete at sim finish: di_brk=%b", `di_brk);
		if (`di_err == 1'b0)
                        $display("drive E R R O R event not complete at sim finish: di_err=%b", `di_err);

		`endif

	end
