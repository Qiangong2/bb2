// tests.v Frank Berndt;
// test wrapper;

// define default sim top;

`ifdef	gi_sim_top
`else	// gi_sim_top
`define	gi_sim_top	sim
`endif	// gi_sim_top

// assign test environment;
`ifdef ALI_GI
`define	xclk		`gi_sim_top.gi_clk
`define blk			top.blk
`else
`define	xclk		sim.sys.clk
`define swrite      sim.sys.swrite
`define sread       sim.sys.sread
`define bwrite      sim.sys.bwrite
`define bread       sim.sys.bread
`define blk         sim.sys.blk
`endif

`define	rom_put		`gi_top.rom.put
`define	rom_get		`gi_top.rom.get
`define	rom_random	`gi_top.rom.random
`define	rom_fill	`gi_top.rom.fill

`ifdef ALI_GI
`define	v_pat		top.tests.v_pat
`define	sram_fill	`gi_top.sram.fill
`define	v_put		top.tests.v_put
`define	v_get		top.tests.v_get
`else
`define	sram_put	`gi_top.sram.put
`define	sram_get	`gi_top.sram.get
`define	sram_fill	`gi_top.sram.fill

`define	sec_bit		`gi_top.secure
`define	v_pat		sim.tests.v_pat
`define	v_put		v_put
`define	v_get		v_get
`endif

`ifdef ALI_GI
`else
`define	di_reset	sim.di_host.rst
`define	di_xfer		sim.di_host.xfer
`define	di_get		sim.di_host.get
`define	di_put		sim.di_host.put
`define	di_break	sim.di_host.break
`define di_sreset       sim.di_host.sreset

`define	ais_rst		sim.ais_host.rst
`define	ais_set		sim.ais_host.set
`define	ais_get		sim.ais_host.get
`define	ais_put		sim.ais_host.put
`define	ais_start	sim.ais_host.start
`define	ais_period	sim.ais_host.period
`define	ais_check	sim.ais_host.check
`endif

`ifdef PERF_MON
`define head            sim.di_mon.head
`define tail            sim.di_mon.tail
`define brk_en          sim.di_mon.brk_en
`define err_acc         sim.di_mon.err_acc
`define brk_acc         sim.di_mon.brk_acc
`define dir0_count      sim.di_mon.dir0_count
`define dir1_count      sim.di_mon.dir1_count
`define dir0_time       sim.di_mon.dir0_time
`define dir1_time       sim.di_mon.dir1_time
`define dir0_acc        sim.di_mon.dir0_acc
`define dir1_acc        sim.di_mon.dir1_acc
`define dir0_eff        sim.di_mon.dir0_eff
`define dir1_eff        sim.di_mon.dir1_eff
`define total_time      sim.di_mon.total_time
`define total_eff      	sim.di_mon.total_eff
`define di_brk		sim.di_brk
`define di_err		sim.di_err

`define	send            sim.di_host.send
`define	recv            sim.di_host.recv

`endif

// test module wrapper;

`timescale 1ns/1ns

module tests ( clk, pci_clk, go );

	input clk;
	input pci_clk;
	input go;

	// include per-unit tests;

	`include "aes_task.v"
	`include "sha1_task.v"
	`include "test_lib.v"
	`include "test_gi.v"
	`include "test_bd.v"

	initial
	begin
		@(posedge go);
		`include "test_all.v"
		$finish;
	end

endmodule
