// test_bd.v v1 Frank Berndt
// collection of backdoors;
// here, because we don't want to modify vendor models;

// virage backdoor;

task v_put;
	input [6:0] addr;
	input [31:0] data;
	   begin
`ifdef FPGA    // Virage isn't supported on the FPGA
`else
		`gi_top.v.novea.uut.earom[addr] = data;
`endif
	end
endtask

function [31:0] v_get;
	input [6:0] addr;
	begin
`ifdef FPGA    // Virage isn't supported on the FPGA
`else
		v_get = `gi_top.v.novea.uut.earom[addr];
`endif
	end
endfunction

// set initial virage content;
// written to earom area, need recall to xfer to sram;

reg [31:0] v_pat;			// initial virage pattern;

task v_init;
	input [31:0] pat;		// initial pattern;
	integer n;
	begin
		$display("%M: pat=0x%h", pat);
		v_pat = pat;
		for(n = 0; n < `GI_V_SIZE; n = n + 1) begin
`ifdef FPGA    // Virage isn't supported on the FPGA
`else
			`v_put(n, pat);
`endif
			pat = next_pat(pat);
		end
	end
endtask

