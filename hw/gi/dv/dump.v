// dump.v Frank Berndt
// dump control module;
// :set tabstop=4

module dump;

	// turn on dumping globally or on a per-module basis;
	// set the dump flag instead of $dumpon or $dumpoff;

	reg dump;

	initial
	begin
		dump = 0;
		if($test$plusargs("dump_all")) $dumpvars;
		if($test$plusargs("dump_gi")) $dumpvars(0, `gi_top);

		// dump clocks and reset for any dump on top level;

		if($test$plusargs("dump")) begin
			dump = 1;
			$dumpvars(1, sim.clk);
		end
	end

	// simulator dumping flag;

	always @(dump)
	begin
		if(dump)
			$dumpon;
		else
			$dumpoff;
		$display("%t: %M: dumping %0s", $time, dump? "on" : "off");
	end

endmodule
