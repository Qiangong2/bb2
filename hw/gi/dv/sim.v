// sim.v Frank Berndt
// top level gi stand-alone simulator;
// :set tabstop=4

`timescale 1ps/1ps

`include "gi.vh"
`ifdef	GI_PCI
 `include "pci.vh"
 `include "pci_func.v"
`endif	// GI_PCI


// full path to the GI module, used by several of the BFM's

`ifdef GI_PCI
 `ifdef FPGA
  `define	gi_top		sim.gi_fpga.gi_pci.gi
 `else
  `define	gi_top		sim.gi_pci.gi
 `endif
`else
 `define		gi_top		sim.gi
`endif


module sim;
	// simulator environment;

	initial
	begin
		$timeformat(-9, 2, "ns", 15);
	end

	// system globals;

	wire clk;					// system clock;
	wire pci_clk;				// pci clock;
	wire clk_lock;				// clk is stable;
	reg pin_rst;				// pin reset;
	wire sys_rst;				// chip-wide reset from gi;
	reg cpu_rst;				// cpu reset;
	reg pci_rst;				// pci reset;
	reg test_in;				// test enable input;
	wire test_ena;				// enable test/debug logic;
	wire sec_intr;				// cause secure exception;
	wire sec_ack;				// secure exception fetch;

	assign clk_lock = 1'b1; //XXX
	assign sec_ack = 1'b1; //XXX

	// instantiate dump control module;

	dump dump ();

	// clock generator;
`ifdef FPGA
   // FGPA's DCM delays outputs relative to inputs by a small amount

   // so drive DCM's input with some fake clock
   // then drive rest of GI and testbench from DCM's outputs
	clkgen sysclk ( .period(10000), .clk(), .pci_clk(pci_clk_raw) );

   assign clk = sim.gi_fpga.clk;

   assign pci_clk = sim.gi_fpga.pci_clk_dcm;
          
`else
	clkgen sysclk ( .period(10000), .clk(clk), .pci_clk(pci_clk) );
`endif

	// power-on reset;
	// start tests;

	reg go;						// when to start the tests;

	initial
	begin
		test_in = 1'b1;
		go = 1'b0;
		pin_rst = 1'b0;
		cpu_rst = 1'b0;
		pci_rst = 1'b0;
		repeat(10) @(posedge clk);
		pin_rst = 1'b1;
		cpu_rst = 1'b1;
		wait(sys_rst);
		repeat(4) @(posedge pci_clk);
		pci_rst = 1'b1;
		repeat(4) @(posedge pci_clk);
		go = 1;
	end

	// monitor pin_rst;

	always @(pin_rst)
		$display("%t: %M: pin_rst=%b", $time, pin_rst);

	// monitor cpu_rst;

	always @(cpu_rst)
		$display("%t: %M: cpu_rst=%b", $time, cpu_rst);

	// monitor chip reset;

	always @(sys_rst)
		$display("%t: %M: sys_rst=%b", $time, sys_rst);

	// alternative boot flag;
	// only useable if debug is enabled;
	// forces reading the rom backwards;
	// jump instruction there can go anywhere;

	reg alt_boot;

	initial
		alt_boot = 0;

	always @(alt_boot)
		$display("%t: %M: alt_boot=%b", $time, alt_boot);

	// monitor test mode;

	always @(test_in)
		$display("%t: %M: test_in=%b", $time, test_in);
	always @(test_ena)
		$display("%t: %M: test_ena=%b", $time, test_ena);

	// di interface;

	wire di_reset;			// interface reset;
	wire di_dir;			// xfer direction;
	wire [7:0] di_dd;		// data bus;
	wire [7:0] di_in;		// data from input cell;
	wire [7:0] di_out;		// data to output cell;
	wire di_oe;				// dd output enable;
	wire di_hstrb;			// host strobe/ready;
	wire di_dstrb;			// device strobe/ready;
	wire di_err;			// device error;
	wire di_brk;			// break;
	wire di_brk_in;			// break from input cell;
	wire di_brk_out;		// break to output cell;
	wire di_brk_oe;			// break output enable;
	wire di_cover;			// cover open;

	// ais interface;

	wire ais_clk;			// audio clock;
	wire ais_lr;			// left/right signal;
	wire ais_d;				// bit stream;

	// chip top-level io cells;

`ifdef FPGA
`else
	assign di_in = di_dd;
	assign di_dd = di_oe? di_out : 8'bz;
	assign di_brk_in = di_brk;
	assign di_brk = di_brk_oe? di_brk_out : 1'bz;
`endif
   
	pullup ( di_brk );

`ifdef	GI_PCI

	// pci interface;
	// bidirectional bus;

	wire [31:0] pci_ad;		// bi-dir bus;
	wire [31:0] pci_ad_out;
	wire pci_ad_oe;
	wire [3:0] pci_cbe;		// bi-dir bus;
	wire [3:0] pci_cbe_out;
	wire pci_cbe_oe;
	wire pci_par;			// bi-dir bus;
	wire pci_par_out;
	wire pci_par_oe;
	wire pci_frame;			// bi-dir bus;
	wire pci_frame_out;
	wire pci_frame_oe;
	wire pci_devsel;		// bi-dir bus;
	wire pci_devsel_out;
	wire pci_devsel_oe;
	wire pci_irdy;			// bi-dir bus;
	wire pci_irdy_out;
	wire pci_irdy_oe;
	wire pci_trdy;			// bi-dir bus;
	wire pci_trdy_out;
	wire pci_trdy_oe;
	wire pci_stop;			// bi-dir bus;
	wire pci_stop_out;
	wire pci_stop_oe;
	wire pci_lock;			// bus locked;
	wire pci_lock_out;
	wire pci_lock_oe;
	wire pci_perr;			// parity error;
	wire pci_perr_out;
	wire pci_perr_oe;
	wire pci_serr;			// system error;

	wire [3:0] pci_req;		// pci bus requests;
	wire [3:0] pci_gnt;		// bus grants;
	wire pci_ognt;			// OR of other grants, not host;
	wire [3:0] pci_intr;	// interrupt to processor;

	// pci bi-dir bus pullups;

	pullup ( pci_frame );
	pullup ( pci_devsel );
	pullup ( pci_irdy );
	pullup ( pci_trdy );
	pullup ( pci_stop );
	pullup ( pci_lock );
	pullup ( pci_perr );
	pullup ( pci_serr );
	pullup ( pci_intr[0] );
	pullup ( pci_intr[1] );
	pullup ( pci_intr[2] );
	pullup ( pci_intr[3] );

	// uni-directional gi;

	wire [31:0] gi_ad_in;	// addr/data inputs;
	wire [31:0] gi_ad_out;	// addr/data outputs;
	wire gi_ad_oe;			// addr/data output enable;
	wire [3:0] gi_cbe_in;	// byte enable inputs;
	wire [3:0] gi_cbe_out;	// byte enable outputs;
	wire gi_cbe_oe;			// byte enable output enable;
	wire gi_par_in;			// parity from master;
	wire gi_par_out;		// parity when master;
	wire gi_par_oe;			// parity output enable;
	wire gi_frame_in;		// frame when target;
	wire gi_frame_out;		// frame when master;
	wire gi_frame_oe;		// frame output enable;
	wire gi_devsel_in;		// device selected;
	wire gi_devsel_out;		// device select;
	wire gi_devsel_oe;		// devsel output enable;
	wire gi_irdy_in;		// initiator ready;
	wire gi_irdy_out;		// ready when master;
	wire gi_irdy_oe;		// irdy output enable;
	wire gi_trdy_in;		// target ready;
	wire gi_trdy_out;		// when target;
	wire gi_trdy_oe;		// trdy output enable;
	wire gi_stop_in;		// initiator stops transfer;
	wire gi_stop_out;		// target stops;
	wire gi_stop_oe;		// stop output enable;
	wire gi_lock_in;		// initiator stops transfer;
	wire gi_lock_out;		// target stops;
	wire gi_lock_oe;		// stop output enable;
	wire gi_perr_in;		// parity error in;
	wire gi_perr_out;		// parity error out;
	wire gi_perr_oe;		// parity error output enable;
	wire gi_serr_out;		// system error output enable;
	wire gi_serr_oe;		// system error output enable;

	assign gi_lock_out = 1'b1;
	assign gi_lock_oe = 1'b0;
	assign gi_perr_out = 1'b1;
	assign gi_perr_oe = 1'b0;
	assign gi_serr_out = 1'b1;
	assign gi_serr_oe = 1'b0;

	// uni-directional host;

	wire [31:0] h_ad_in;	// addr/data inputs;
	wire [31:0] h_ad_out;	// addr/data outputs;
	wire h_ad_oe;			// addr/data output enable;
	wire [3:0] h_cbe_in;	// byte enable inputs;
	wire [3:0] h_cbe_out;	// byte enable outputs;
	wire h_cbe_oe;			// byte enable output enable;
	wire h_par_in;			// parity from master;
	wire h_par_out;			// parity when master;
	wire h_par_oe;			// parity output enable;
	wire h_frame_in;		// frame when target;
	wire h_frame_out;		// frame when master;
	wire h_frame_oe;		// frame output enable;
	wire h_devsel_in;		// device selected;
	wire h_devsel_out;		// device select;
	wire h_devsel_oe;		// devsel output enable;
	wire h_irdy_in;			// initiator ready;
	wire h_irdy_out;		// ready when master;
	wire h_irdy_oe;			// irdy output enable;
	wire h_trdy_in;			// target ready;
	wire h_trdy_out;		// when target;
	wire h_trdy_oe;			// trdy output enable;
	wire h_stop_in;			// initiator stops transfer;
	wire h_stop_out;		// target stops;
	wire h_stop_oe;			// stop output enable;
	wire h_lock_in;			// initiator stops transfer;
	wire h_lock_out;		// target stops;
	wire h_lock_oe;			// stop output enable;
	wire h_perr_in;			// parity error in;
	wire h_perr_out;		// parity error out;
	wire h_perr_oe;			// parity error output enable;
	wire h_serr_in;			// system error in;

	// pci io cells;
	// internal loopback; XXX

	// instantiate gi;

 `ifdef FPGA            // `ifdef GI_PCI && FPGA

	assign gi_ad_oe = 0;
	assign gi_cbe_oe = 0;
	assign gi_par_oe = 0;
	assign gi_frame_oe = 0;
	assign gi_devsel_oe = 0;
	assign gi_irdy_oe = 0;
	assign gi_trdy_oe = 0;
	assign gi_stop_oe = 0;

	assign gi_ad_out = 32'b0;
	assign gi_cbe_out = 4'b0;
	assign gi_par_out = 0;
	assign gi_frame_out = 0;
	assign gi_devsel_out = 0;
	assign gi_irdy_out = 0;
	assign gi_trdy_out = 0;
	assign gi_stop_out = 0;

  `ifdef DI_MUX     // `ifdef GI_PCI && FPGA && DI_MUX

   //
   // to test the DI mux in the FPGA, instantiate a "dummy" FPGA
   // device and configure it to simply pass the DI bus through to
   // the original ("real") FPGA, as shown below --
   //
   //  -------         -------            ------- 
   // |       |  DI   | dummy |  AUX_DI  | real  |
   // |Flipper|-------| FPGA  |----------| FPGA  |
   // |       |       |       |          |       |
   //  -------         -------            ------- 
   //                     |                  |
   //               DI_MUX_SEL=0       DI_MUX_SEL=1
   //
   // In this way, both states of the DI mux are exercised --
   // * dummy FPGA: the path from DI to/from AUX_DI is used
   // * real FPGA: the path from DI to/from internal GI block
   //   is used
   //
   
   // aux di interface;
   
	wire       aux_di_reset;		// interface reset;
	wire       aux_di_dir;			// xfer direction;
	wire [7:0] aux_di_dd;			// data bus;
	wire       aux_di_hstrb;		// host strobe/ready;
	wire       aux_di_dstrb;		// device strobe/ready;
	wire       aux_di_err;			// device error;
	wire       aux_di_brk;			// break;
	wire       aux_di_cover;		// cover open;
   
	// aux ais interface;

	wire       aux_ais_clk;			// audio clock;
	wire       aux_ais_lr;			// left/right signal;
	wire       aux_ais_d;			// bit stream;
   
   gi_fpga gi_fpga_dummy (

      .DI_MUX_SEL(1'b0),     // connect DI to AUX_DI

		.DID(di_dd),
		.DIBRK(di_brk),
		.DIDIR(di_dir),
		.DIHSTRBB(di_hstrb),
		.DIDSTRBB(di_dstrb),
		.DIERRB(di_err),
		.DICOVER(di_cover),
		.DIRSTB(di_reset),

		.AISCLK(ais_clk),
		.AISLR(ais_lr),
		.AISD(ais_d),
                      
		.AUX_DID(aux_di_dd),
		.AUX_DIBRK(di_brk),
		.AUX_DIDIR(aux_di_dir),
		.AUX_DIHSTRBB(aux_di_hstrb),
		.AUX_DIDSTRBB(aux_di_dstrb),
		.AUX_DIERRB(aux_di_err),
		.AUX_DICOVER(aux_di_cover),
		.AUX_DIRSTB(aux_di_reset),

		.AUX_AISCLK(aux_ais_clk),
		.AUX_AISLR(aux_ais_lr),
		.AUX_AISD(aux_ais_d)
	);
  `endif
   
   gi_fpga gi_fpga (
//		.clk(clk),
//		.clk_lock(clk_lock),
		.RST(pin_rst),
		.SYS_RST(sys_rst),
//		.cpu_rst(cpu_rst),
		.ALT_BOOT(alt_boot),
		.TEST_IN(test_in),
      .PROM_SCL(),
      .PROM_SDA(),
		.TEST_ENA(test_ena),
		.GC_RST(), //XXX
		.SEC_INTR(sec_intr),
//		.sec_ack(sec_ack),
		.PCI_CLK(pci_clk_raw),
		.PCI_RST(pci_rst),
		.PCI_AD(pci_ad),
		.CBE(pci_cbe),
		.PAR(pci_par),
		.FRAME(pci_frame),
		.DEVSEL(pci_devsel),
		.IRDY(pci_irdy),
		.TRDY(pci_trdy),
		.STOP(pci_stop),
		.PCI_REQ(pci_req[0]),
		.PCI_GNT(pci_gnt[0]),
		.PCI_OGNT(pci_ognt),
		.PCI_INTR(pci_intr[0]),
   `ifdef DI_MUX            // `ifdef GI_PCI && FPGA && DI_MUX
      .DI_MUX_SEL(1'b1),    // connect DI to GI module

		.DID(aux_di_dd),
		.DIBRK(di_brk),
		.DIDIR(aux_di_dir),
		.DIHSTRBB(aux_di_hstrb),
		.DIDSTRBB(aux_di_dstrb),
		.DIERRB(aux_di_err),
		.DICOVER(aux_di_cover),
		.DIRSTB(aux_di_reset),

		.AISCLK(aux_ais_clk),
		.AISLR(aux_ais_lr),
		.AISD(aux_ais_d)
   `else 						// `ifdef GI_PCI && FPGA && ! DI_MUX
      .DI_MUX_SEL(1'b1),   // connect DI to GI module

		.DID(di_dd),
		.DIBRK(di_brk),
		.DIDIR(di_dir),
		.DIHSTRBB(di_hstrb),
		.DIDSTRBB(di_dstrb),
		.DIERRB(di_err),
		.DICOVER(di_cover),
		.DIRSTB(di_reset),

		.AISCLK(ais_clk),
		.AISLR(ais_lr),
		.AISD(ais_d)
   `endif // !`ifdef DI_MUX
                      
	);

 `else // `ifdef GI_PCI && ! FPGA

   // instantiate gi;
   
	assign gi_ad_in = pci_ad;
	assign gi_cbe_in = pci_cbe;
	assign gi_par_in = pci_par;
	assign gi_frame_in = pci_frame;
	assign gi_devsel_in = pci_devsel;
	assign gi_irdy_in = pci_irdy;
	assign gi_trdy_in = pci_trdy;
	assign gi_stop_in = pci_stop;
   
   gi_pci gi_pci (
                  .clk(clk),
                  .clk_lock(clk_lock),
                  .pin_rst(pin_rst),
                  .sys_rst(sys_rst),
                  .cpu_rst(cpu_rst),
                  .alt_boot(alt_boot),
                  .test_in(test_in),
                  .test_ena(test_ena),
                  .gc_rst(), //XXX
                  .sec_intr(sec_intr),
                  .sec_ack(sec_ack),
                  .pci_clk(pci_clk),
                  .pci_rst(pci_rst),
                  .ad_in(gi_ad_in),
                  .ad_out(gi_ad_out),
                  .ad_oe(gi_ad_oe),
                  .cbe_in(gi_cbe_in),
                  .cbe_out(gi_cbe_out),
                  .cbe_oe(gi_cbe_oe),
                  .par_in(gi_par_in),
                  .par_out(gi_par_out),
                  .par_oe(gi_par_oe),
                  .frame_in(gi_frame_in),
                  .frame_out(gi_frame_out),
                  .frame_oe(gi_frame_oe),
                  .devsel_in(gi_devsel_in),
                  .devsel_out(gi_devsel_out),
                  .devsel_oe(gi_devsel_oe),
                  .irdy_in(gi_irdy_in),
                  .irdy_out(gi_irdy_out),
                  .irdy_oe(gi_irdy_oe),
                  .trdy_in(gi_trdy_in),
                  .trdy_out(gi_trdy_out),
                  .trdy_oe(gi_trdy_oe),
                  .stop_in(gi_stop_in),
                  .stop_out(gi_stop_out),
                  .stop_oe(gi_stop_oe),
                  .pci_req(pci_req[0]),
                  .pci_gnt(pci_gnt[0]),
                  .pci_ognt(pci_ognt),
                  .pci_intr(pci_intr[0]),
                  .di_in(di_in),
                  .di_out(di_out),
                  .di_oe(di_oe),
                  .di_brk_in(di_brk_in),
                  .di_brk_out(di_brk_out),
                  .di_brk_oe(di_brk_oe),
                  .di_dir(di_dir),
                  .di_hstrb(di_hstrb),
                  .di_dstrb(di_dstrb),
                  .di_err(di_err),
                  .di_cover(di_cover),
                  .di_reset(di_reset),
                  .ais_clk(ais_clk),
                  .ais_lr(ais_lr),
                  .ais_d(ais_d)
                  );

 `endif // !`ifdef FPGA

	assign pci_req[3:1] = 3'b111;
	assign pci_ognt = 1'b0;
	assign pci_intr[3:1] = 3'b111;

	// instantiate pci model;
	// this is the trigger for all stand-alone tests;

	assign h_ad_in = pci_ad;
	assign h_cbe_in = pci_cbe;
	assign h_par_in = pci_par;
	assign h_frame_in = pci_frame;
	assign h_devsel_in = pci_devsel;
	assign h_irdy_in = pci_irdy;
	assign h_trdy_in = pci_trdy;
	assign h_stop_in = pci_stop;

	pci_host sys (
		.clk(pci_clk),
		.rst(pci_rst),
		.ad_in(h_ad_in),
		.ad_out(h_ad_out),
		.ad_oe(h_ad_oe),
		.cbe_in(h_cbe_in),
		.cbe_out(h_cbe_out),
		.cbe_oe(h_cbe_oe),
		.par_in(h_par_in),
		.par_out(h_par_out),
		.par_oe(h_par_oe),
		.frame_in(h_frame_in),
		.frame_out(h_frame_out),
		.frame_oe(h_frame_oe),
		.devsel_in(h_devsel_in),
		.devsel_out(h_devsel_out),
		.devsel_oe(h_devsel_oe),
		.irdy_in(h_irdy_in),
		.irdy_out(h_irdy_out),
		.irdy_oe(h_irdy_oe),
		.trdy_in(h_trdy_in),
		.trdy_out(h_trdy_out),
		.trdy_oe(h_trdy_oe),
		.stop_in(h_stop_in),
		.stop_out(h_stop_out),
		.stop_oe(h_stop_oe),
		.lock_in(h_lock_in),
		.lock_out(h_lock_out),
		.lock_oe(h_lock_oe),
		.perr_in(h_perr_in),
		.perr_out(h_perr_out),
		.perr_oe(h_perr_oe),
		.serr_in(h_serr_in),
		.intr(pci_intr),
		.req(pci_req),
		.gnt(pci_gnt)
	);

	// combine internal pci devices;

	assign pci_ad_out = ({32{gi_ad_oe}} & gi_ad_out) | ({32{h_ad_oe}} & h_ad_out);
	assign pci_cbe_out = ({4{gi_cbe_oe}} & gi_cbe_out) | ({4{h_cbe_oe}} & h_cbe_out);
	assign pci_par_out = (gi_par_oe & gi_par_out) | (h_par_oe & h_par_out);
	assign pci_frame_out = (gi_frame_oe & gi_frame_out) | (h_frame_oe & h_frame_out);
	assign pci_devsel_out = (gi_devsel_oe & gi_devsel_out) | (h_devsel_oe & h_devsel_out);
	assign pci_irdy_out = (gi_irdy_oe & gi_irdy_out) | (h_irdy_oe & h_irdy_out);
	assign pci_trdy_out = (gi_trdy_oe & gi_trdy_out) | (h_trdy_oe & h_trdy_out);
	assign pci_stop_out = (gi_stop_oe & gi_stop_out) | (h_stop_oe & h_stop_out);
	assign pci_lock_out = (gi_lock_oe & gi_lock_out) | (h_lock_oe & h_lock_out);
	assign pci_perr_out = (gi_perr_oe & gi_perr_out) | (h_perr_oe & h_perr_out);

	assign pci_ad_oe = gi_ad_oe | h_ad_oe;
	assign pci_cbe_oe = gi_cbe_oe | h_cbe_oe;
	assign pci_par_oe = gi_par_oe | h_par_oe;
	assign pci_frame_oe = gi_frame_oe | h_frame_oe;
	assign pci_devsel_oe = gi_devsel_oe | h_devsel_oe;
	assign pci_irdy_oe = gi_irdy_oe | h_irdy_oe;
	assign pci_trdy_oe = gi_trdy_oe | h_trdy_oe;
	assign pci_stop_oe = gi_stop_oe | h_stop_oe;
	assign pci_lock_oe = gi_lock_oe | h_lock_oe;
	assign pci_perr_oe = gi_perr_oe | h_perr_oe;

	// instantiate pci bus monitor;
	// monitors external bus traffic;

   wire  [31:0] pci_ad_x;
	assign pci_ad_x = pci_ad_oe? pci_ad_out : 32'bz;

	assign pci_ad = pci_ad_oe? pci_ad_out : 32'bz;   
	assign pci_cbe = pci_cbe_oe? pci_cbe_out : 4'bz;
	assign pci_par = pci_par_oe? pci_par_out : 1'bz;
	assign pci_frame = pci_frame_oe? pci_frame_out : 1'bz;
	assign pci_devsel = pci_devsel_oe? pci_devsel_out : 1'bz;
	assign pci_irdy = pci_irdy_oe? pci_irdy_out : 1'bz;
	assign pci_trdy = pci_trdy_oe? pci_trdy_out : 1'bz;
	assign pci_stop = pci_stop_oe? pci_stop_out : 1'bz;

	pci_mon pci_mon_ext (
		.bus(1'b1),
		.clk(pci_clk),
		.rst(pci_rst),
		.ad(pci_ad),
		.cbe(pci_cbe),
		.par(pci_par),
		.frame(pci_frame),
		.devsel(pci_devsel),
		.irdy(pci_irdy),
		.trdy(pci_trdy),
		.stop(pci_stop),
		.lock(pci_lock),
		.perr(pci_perr),
		.serr(pci_serr),
		.intr(pci_intr),
		.req(pci_req),
		.gnt(pci_gnt)
	);

`else // !`ifdef GI_PCI

	// system interface;

	wire gi_rst;				// gi reset;
	wire [31:0] sys_addr;		// cpu address;
	wire [2:0] sys_cmd;			// bus command;
	wire [31:0] sys_out;		// write data, cpu and dma;
	wire [31:0] sys_in;			// read data. cpu and dma;
	wire sys_intr;				// interrupt to system;
	wire dma_req;				// request a dma transfer;
	wire [1:0] dma_bx;			// burst control;
	wire [31:0] dma_addr;		// dma physical address;
	wire dma_wr;				// dma write to memory;
	wire dma_done;				// dma done;
	wire dma_retry;				// retry dma;
	wire [2:0] dma_err;			// dma errors;

	assign sys_rst = pin_rst & cpu_rst;
	assign gi_rst = ~sys_rst;

	// instantiate gi;

	gi gi (
		.clk(clk),
		.clk_lock(clk_lock),
		.reset(gi_rst),
		.alt_boot(alt_boot),
		.test_in(test_in),
		.test_ena(test_ena),
		.gc_rst(), //XXX
		.sec_intr(sec_intr),
		.sec_ack(sec_ack),
		.sys_addr(sys_addr[17:2]),
		.sys_cmd(sys_cmd),
		.sys_in(sys_out),
		.sys_out(sys_in),
		.sys_intr(sys_intr),
		.dma_req(dma_req),
		.dma_bx(dma_bx),
		.dma_addr(dma_addr[27:6]),
		.dma_wr(dma_wr),
		.dma_done(dma_done),
		.dma_retry(dma_retry),
		.dma_err(dma_err),
		.di_in(di_in),
		.di_out(di_out),
		.di_oe(di_oe),
		.di_brk_in(di_brk_in),
		.di_brk_out(di_brk_out),
		.di_brk_oe(di_brk_oe),
		.di_dir(di_dir),
		.di_hstrb(di_hstrb),
		.di_dstrb(di_dstrb),
		.di_err(di_err),
		.di_cover(di_cover),
		.di_reset(di_reset),
		.ais_clk(ais_clk),
		.ais_lr(ais_lr),
		.ais_d(ais_d)
	);

	assign dma_addr[31:28] = 0;
	assign dma_addr[5:0] = 0;

	// instantiate system model;
	// this is the trigger for all stand-alone tests;

	bi_sys sys (
		.clk(clk),
		.reset(gi_rst),
		.sys_addr(sys_addr),
		.sys_cmd(sys_cmd),
		.sys_out(sys_out),
		.sys_in(sys_in),
		.sys_intr(sys_intr),
		.dma_req(dma_req),
		.dma_bx(dma_bx),
		.dma_addr(dma_addr),
		.dma_wr(dma_wr),
		.dma_done(dma_done),
		.dma_retry(dma_retry),
		.dma_err(dma_err)
	);

	// instantiate bi monitor;
	// can be used for stand-alone gi and pci monitoring;

	bi_mon bi_mon (
		.clk(clk),
		.reset(gi_rst),
		.sys_addr(sys_addr),
		.sys_cmd(sys_cmd),
		.sys_out(sys_out),
		.sys_in(sys_in),
		.sys_intr(sys_intr),
		.dma_req(dma_req),
		.dma_bx(dma_bx),
		.dma_addr(dma_addr),
		.dma_wr(dma_wr),
		.dma_done(dma_done)
	);

`endif // !`ifdef GI_PCI

   
`ifdef DI_FLIPPER
	// instantiate flipper di module;
	wire di_clk;
	clkgen diclk ( .period(6172), .clk(di_clk));

	di_flipper di_host (
		.io_clk(di_clk),
		.didir(di_dir),
		.di_dd(di_dd),
		.dihstrbb(di_hstrb),
		.didstrbb(di_dstrb),
		.dierrb(di_err),
		.di_brk(di_brk),
		.dirstb(di_reset),
		.dicover(di_cover)
	);

`else
	// instantiate di host model;

	di_host di_host (
		.reset(di_reset),
		.dir(di_dir),
		.dd(di_dd),
		.hstrb(di_hstrb),
		.dstrb(di_dstrb),
		.err(di_err),
		.brk(di_brk),
		.cover(di_cover)
	);
`endif

	// instantiate di monitor;

	di_mon di_mon (
		.reset(di_reset),
		.dir(di_dir),
		.dd(di_dd),
		.hstrb(di_hstrb),
		.dstrb(di_dstrb),
		.err(di_err),
		.brk(di_brk),
		.cover(di_cover)
	);

	// instantiate ais host model;

	ais_host ais_host (
		.reset(di_reset),
		.ais_clk(ais_clk),
		.ais_lr(ais_lr),
		.ais_d(ais_d)
	);

`ifdef GI_IPC
	ipc ipc(
		.sysclk(clk),
		.coldrst_l(go),
		.warmrst_l(go)
	);
`else
	// instantiate tests;
	tests tests (
		.clk(clk),
		.pci_clk(pci_clk),
		.go(go)
	);
`endif

endmodule
