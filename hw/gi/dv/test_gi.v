// test_gi.v Frank Berndt;
// GI module tests;

// layout for parallel tests;
// units are restricted in their sram and memory usage;
// units that needs serialization:
//		aese & mc encryption;
//		aesd & mc decryption;
//		sha & mc sha;
//		di & dc;
//
// sram
//		1_4000 ... 1_7fff	sha/mc area;
//		1_2000 ... 1_3fff	ais area;
//		1_0000 ... 1_1fff	di/dc area;
//		0_fd80 ... 0_ffff	h0 cache;
//		0_c000 ... 0_fd7f	h1 buffer;
//		0_8000 ... 0xbfff	aesd/mc area;
//		0_4000 ... 0x7fff	aese/mc area;
//		0_0000 ... 0x3fff	bi area;
//
// main memory
//		c_0000 .. f_ffff	dc area;
//		8_0000 .. b_ffff	ais area;
//		4_0000 .. 7_ffff	mc area;
//		0_0000 .. 3_ffff	bi area;

// host commands;

`define	HDIR_OUT	0		// host out direction;
`define	HDIR_IN		1		// host in direction;

`define	BRK_ACK		0		// ack break;
`define	BRK_TERM	1		// terminate break;

// test defines;

`define	NCHAIN		0		// not chained;
`define	CHAIN		1		// chained;

`define	SHA			2'b00	// mc sha;
`define	AESE		2'b01	// mc aes encryption, aese key;
`define	AESD		2'b10	// mc aes decryption, aesd key;
`define	AESDC		2'b11	// mc aes decryption, dc key;

`define	DCOP_WR		2'b00	// dc di write to mem;
`define	DCOP_H0		2'b01	// dc h0 only;
`define	DCOP_RD		2'b10	// dc di read only;
`define	DCOP_H0RD	2'b11	// dc h0 and di read;

// global test flags;

reg test_gi_stress;		// run gi stress thread;
reg test_gi_par;		// run gi tests in parallel;

// thread states;

reg [9:0] thread_run;	// set thread running;
reg [9:0] thread_busy;	// thread is running;

`define	THREAD_STRESS	0
`define	THREAD_ROM		1
`define	THREAD_BI		2
`define	THREAD_DI		3
`define	THREAD_AIS		4
`define	THREAD_AESE		5
`define	THREAD_AESD		6
`define	THREAD_SHA		7
`define	THREAD_MC		8
`define	THREAD_DC		9

initial
begin
	thread_run = 'd0;
	thread_busy = 'd0;
end

// check secure mode after reset;
// TEST_IN is latched into sec_mode[0] bit on reset;

task gi_sec_after_reset;
	reg sec;			// expected sec mode on reset;
	reg [31:0] r;		// read data;
	reg [10:0] rba;		// rom boot word addr;
	reg [31:0] w;		// r/w word;
	begin
		// read SEC_MODE to figure out mode;
		// state of sec_mode[0] must match;

		sec = `gi_sim_top.test_in;
		xsread(`SEC_MODE, r);
		$display("%t: %M: alt_boot=%b, test_in=%b, secure=%b", $time, r[6], r[5], r[0]);
		check_sec_bit(sec);
		if(r[0] !== r[5])
			$display("ERROR: %t: %M: secure=%b, but test_in=%b", $time, r[0], r[5]);

		// if not in secure mode, do fetch to turn it on;
		// alt_boot controls returned first word, if test_in is 1;

		rba = 0;
		if({ r[6], r[5], r[0] } === 3'b111)
			rba = ~rba;
		xsread(`GI_ROM_RST, w);
		sexp(`rom_get(rba), w, 32'hffff_ffff);

		// secure mode must be on now;
		// check that if we came in non-secure, that test_in/alt_boot are 0;
		// turn off alt_boot and test_ena;

		check_sec_bit(1);
		xsread(`SEC_MODE, w);
		if((r[0] !== 1'b1) & (w[5] !== 1'b0))
			$display("ERROR: %t: %M: non-secure reset, but test_in=%b", $time, w[5]);
		xswrite(`SEC_MODE, w);
	end
endtask

// turn off alt_boot;
// must be in secure mode to succeed;
// writing 1 flips the bit;

task gi_alt_boot;
	input alt_boot;			// new state;
	reg [31:0] r;			// read data;
	begin
		$display("%t: %M: alt_boot=%b", $time, alt_boot);
		xsread(`SEC_MODE, r);
		if(r[0] !== 1'b1)
			$display("ERROR: %t: %M: not in secure mode", $time);
		if(r[6] !== alt_boot) begin
			r[6] = 1'b1;
			xswrite(`SEC_MODE, r);
		end
		xsread(`SEC_MODE, r);
		if(r[6] !== alt_boot)
			$display("ERROR: %t: %M: alt_boot %b exp %b", $time, r[6], alt_boot);
	end
endtask

// test gi registers after reset;

task gi_regs_after_reset;
	reg [31:0] r;		// read data;
	reg [31:0] mask;	// bit mask;
	begin
		// BI secure mode registers;

		$display("%M: SEC_MODE");
		xsread(`SEC_MODE, r);
		sexp(`SEC_MODE_RESET | `SEC_MODE_ON, r, 32'hffff_ffdf);

		$display("%M: SEC_TIMER");
		xsread(`SEC_TIMER, r);
		sexp(0, r, 32'hffff_ffff);

		$display("%M: SEC_SRAM");
		xsread(`SEC_SRAM, r);
		sexp(0, r, 32'hffff_ffff);

		// virage registers;

`ifdef FPGA    // Virage isn't supported on the FPGA
`else
		$display("%M: V_CRSTO_0");
		xsread(`V_CRSTO_0, r);
		sexp(`V_CRSTO_0_DEF, r, 32'hffff_ffff);

		$display("%M: V_CRSTO_1");
		xsread(`V_CRSTO_1, r);
		sexp(`V_CRSTO_1_DEF, r, 32'hffff_ffff);

		$display("%M: V_CRM_0");
		xsread(`V_CRM_0, r);
		sexp(`V_CRM_0_DEF, r, 32'hffff_ffff);

		$display("%M: V_CRM_1");
		xsread(`V_CRM_1, r);
		sexp(`V_CRM_1_DEF, r, 32'hffff_ffff);

		$display("%M: V_CRM_2");
		xsread(`V_CRM_2, r);
		sexp(`V_CRM_2_DEF, r, 32'hffff_ffff);

		$display("%M: V_CRM_3");
		xsread(`V_CRM_3, r);
		sexp(`V_CRM_3_DEF, r, 32'hffff_ffff);

		$display("%M: V_REG_6");
		xsread(`V_REG_6, r);
		sexp(`V_REG_6_DEF, r, 32'hffff_ffff);

		$display("%M: V_REG_7");
		xsread(`V_REG_7, r);
		sexp(`V_REG_7_DEF, r, 32'hffff_ffff);

		$display("%M: V_CMD");
		xsread(`V_CMD, r);
		sexp({ 16'd0, 16'h8062 }, r, 32'hffff_f8ff);
`endif // !`ifdef FPGA
      
		// BI dma registers;

		$display("%M: BI_CTRL");
		xsread(`BI_CTRL, r);
		mask = `BI_CTRL_SIZE | `BI_CTRL_WRITE | `BI_CTRL_PTR | `BI_CTRL_ERRS;
		sexp(0, r, ~mask);

		$display("%M: BI_MEM");
		xsread(`BI_MEM, r);
		mask = `BI_MEM_PTR;
		sexp(0, r, ~mask);

		$display("%M: BI_INTR");
		xsread(`BI_INTR, r);
		sexp(0, r, 32'hffff_ffff);

		// DI registers;

		$display("%M: DI_CONF");
		xsread(`DI_CONF, r);
		sexp(32'ha800_0070, r, 32'hffff_ffff);

		$display("%M: DI_CTRL");
		xsread(`DI_CTRL, r);
		mask = `DI_CTRL_COVER;
		sexp(`DI_CTRL_RST_INTR | `DI_CTRL_CMD_IN, r, ~mask);

		$display("%M: DI_BE");
		xsread(`DI_BE, r);
		mask = `DI_BE_SIZE | `DI_BE_WRITE | `DI_BE_PTR;
		sexp(0, r, ~mask);

		// AIS registers;

		$display("%M: AIS_CTRL");
		xsread(`AIS_CTRL, r);
		mask = `AIS_CTRL_ERRS;
		sexp(32'h0, r, ~mask);

		$display("%M: AIS_PTR0");
		xsread(`AIS_PTR0, r);
		mask = `AIS_PTR_ADDR | `AIS_PTR_EMPTY;
		sexp(32'h0, r, ~mask);

		$display("%M: AIS_PTR1");
		xsread(`AIS_PTR1, r);
		mask = `AIS_PTR_ADDR | `AIS_PTR_EMPTY;
		sexp(32'h0, r, ~mask);

		$display("%M: AIS_BUF");
		xsread(`AIS_BUF, r);
		mask = `AIS_BUF_OFF;
		sexp(32'h0, r, ~mask);

		// AES regs;

		$display("%M: AESD_CTRL");
		xsread(`AESD_CTRL, r);
		mask = `AES_CTRL_SIZE | `AES_CTRL_PTR | `AES_CTRL_DCKEY | `AES_CTRL_CHAIN;
		sexp(32'h0, r, ~mask);

		$display("%M: AESD_IV");
		xsread(`AESD_IV, r);
		mask = `AES_IV_OFF;
		sexp(32'h0, r, ~mask);

		$display("%M: AESE_CTRL");
		xsread(`AESE_CTRL, r);
		mask = `AES_CTRL_SIZE | `AES_CTRL_PTR | `AES_CTRL_CHAIN;
		sexp(32'h0, r, ~mask);

		$display("%M: AESE_IV");
		xsread(`AESE_IV, r);
		mask = `AES_IV_OFF;
		sexp(32'h0, r, ~mask);

		$display("%M: AES_KEXP");
		xsread(`AES_KEXP, r);
		mask = `AES_KEXP_UNIT;
		sexp(`AES_KEXP_READY, r, ~mask);

		// SHA1 regs;

		$display("%M: SHA_CTRL");
		xsread(`SHA_CTRL, r);
		mask = `SHA_CTRL_SIZE | `SHA_CTRL_WRITE | `SHA_CTRL_PTR | `SHA_CTRL_CHAIN;
		sexp(32'h0, r, ~mask);

		mask = `SHA_BUF_OFF;
		$display("%M: SHA_BUF");
		xsread(`SHA_BUF, r);
		sexp(32'h0, r, ~mask);

		// MC registers;

		$display("%M: MC_CTRL");
		xsread(`MC_CTRL, r);
		mask = `MC_CTRL_OP | `MC_CTRL_DROP | `MC_CTRL_CHAIN
			| `MC_CTRL_SIZE | `MC_CTRL_ERRS;
		sexp(32'h0, r, ~mask);

		$display("%M: MC_ADDR");
		xsread(`MC_ADDR, r);
		mask = `MC_ADDR_MEM;
		sexp(32'h0, r, ~mask);

		$display("%M: MC_BUF");
		xsread(`MC_BUF, r);
		mask = `MC_BUF_OFF;
		sexp(32'h0, r, ~mask);

		// DC registers;

		$display("%M: DC_CTRL");
		xsread(`DC_CTRL, r);
		mask = `DC_CTRL_OP | `DC_CTRL_CUR | `DC_CTRL_SG | `DC_CTRL_ERRS;
		sexp(32'h0, r, ~mask);

		$display("%M: DC_BUF");
		xsread(`DC_BUF, r);
		mask = `DC_BUF_OFF;
		sexp(32'h0, r, ~mask);

		$display("%M: DC_HADDR");
		xsread(`DC_HADDR, r);
		mask = `DC_HADDR_MEM;
		sexp(32'h0, r, ~mask);

		$display("%M: DC_HIDX");
		xsread(`DC_HIDX, r);
		mask = `DC_HIDX_H0 | `DC_HIDX_H1;
		sexp(32'h0, r, ~mask);

		$display("%M: DC_DIOFF");
		xsread(`DC_DIOFF, r);
		mask = `DC_DI_LAST | `DC_DI_FIRST;
		sexp(32'h0, r, ~mask);

		$display("%M: DC_IV");
		mask = `DC_IV_OFF;
		xsread(`DC_IV, r);
		sexp(32'h0, r, ~mask);

		$display("%M: DC_H0");
		mask = `DC_H0_OFF;
		xsread(`DC_H0, r);
		sexp(32'h0, r, ~mask);

		$display("%M: DC_H1");
		mask = `DC_H1_OFF;
		xsread(`DC_H1, r);
		sexp(32'h0, r, ~mask);
	end
endtask

// wait for a unit to finish;
// either spin on on the EXEC bit or the BI_INTR bit;

task gi_wait_ctrl;
	input intr;				// poll 0=EXEC, 1=BI_INTR;
	input [31:0] addr;		// addr of ctrl reg;
	input [4:0] bbit;		// busy bit;
	input [4:0] ibit;		// BI_INTR interrupt bit;
	input [31:0] to;		// timeout;
	reg [31:0] r;			// read data;
	reg busy;				// unit still busy;
	begin
		// poll ctrl reg or BI_INTR;
		// probe once per usec;

		if(intr)
			addr = `BI_INTR;
		else
			ibit = bbit;
		busy = 1;
		while(busy && (to > 0)) begin
			xsread(addr, r);
			busy = intr ^ r[ibit];
			if(busy) begin
				stall(100);
				to = to - 1;
			end
		end
	end
endtask

// test bi registers;
// must pass for secure and non-secure mode, since
// all register bits are accessible in both modes;
// do not run when bi dma is busy;
// dma operations are checked in another test;

task gi_bi_regs;
	reg [31:0] r;			// read data;
	begin
		xsread(`BI_CTRL, r);
		if(r[31] !== 1'b0)
			$display("ERROR: %t: %M: bi busy, BI_CTRL=0x%h", $time, r);
		r = `BI_CTRL_SIZE | `BI_CTRL_WRITE | `BI_CTRL_PTR;
		$display("%M: BI_CTRL");
		test_reg(`BI_CTRL, r, 0);
		$display("%M: BI_MEM");
		test_reg(`BI_MEM, `BI_MEM_PTR, 0);
	end
endtask

// issue bi dma;
// it is assumed that bi is not busy;

task gi_bi_dma;
	input [31:0] mem;		// memory address;
	input [16:0] off;		// buffer offset;
	input [9:0] size;		// size in blocks;
	input wr;				// dma to memory;
	input intr;				// expect interrupt;
	input [2:0] err;		// expected err state;
	input [31:0] to;		// timeout in usec;
	input kill;				// kill after start;
	reg [31:0] ctrl;		// control register;
	reg [31:0] r;			// read data;
	reg [31:0] mask;		// read check mask;
	begin
		// write memory address;
		// write unused bits with Xs;
		// start dma;

		$display("%t: %M: mem=0x%h off=0x%h blks=%0d wr=%b intr=%b to=%0dus",
			$time, mem, off, size + 1, wr, intr, to);
		xswrite(`BI_MEM, mem);
		ctrl = 'h8000_0000;
		ctrl[30] = intr;
		ctrl[29:20] = size;
		ctrl[19] = wr;
		ctrl[16:4] = off[16:4];
		xswrite(`BI_CTRL, ctrl);

		// read and check that dma is busy;
		// dma could be done before read of BI_CTRL;
		// if dma is already done, then BI_SIZE should be all 1s;

		xsread(`BI_CTRL, r);
		mask = `BI_CTRL_EXEC | `BI_CTRL_SIZE;
		if((r & mask) !== 32'h3ff0_0000) begin
			mask = `BI_CTRL_EXEC | `BI_CTRL_WRITE;
			if((r & mask) != (ctrl & mask)) begin
				$display("ERROR: %t: %M: BI_CTRL=0x%h/%b exp 0x%h/%b",
					$time, r, r, ctrl, ctrl);
			end
		end

		// check kill operation;
		// bi must become unbusy right away;
		// interrupt should not have been issued;

		if(kill) begin
			ctrl[31] = 0;
			xswrite(`BI_CTRL, ctrl);
			xsread(`BI_CTRL, r);
			intr = 0;
		end

		// wait until dma is finished;
		// a 128-byte dma across pci takes ~1usec;

		while((r[31] === 1'b1) && (to > 0)) begin
			stall(100);
			xsread(`BI_CTRL, r);
			to = to - 1;
		end

		// post DMA checks;
		// dma must have finished with correct intr flag;
		// clear pending intr, and kill still going dma;

		if(r[31] !== 1'b0)
			$display("ERROR: %t: %M: timeout, BI_CTRL=%b", $time, r);
		if(r[30] !== intr)
			$display("ERROR: %t: %M: intr=%b exp %b", $time, r[30], intr);
		if(r[2:0] !== err)
			$display("ERROR: %t: %M: err=%b exp %b", $time, r[2:0], err);
		ctrl[31] = 0;
		ctrl[30] = 0;
		xswrite(`BI_CTRL, ctrl);
		xsread(`BI_CTRL, r);
		if(r[31:30] !== 2'd0)
			$display("ERROR: %t: %M: not cleared, BI_CTRL=%b", $time, r);
	end
endtask

// check rom with single reads;
// must be in secure mode;

task gi_check_rom_sgl;
	input [31:0] space;		// address space;
	input reqspc;			// random request spacing;
	input wa;				// addr walking type;
	reg [31:0] past;		// end of space;
	reg [31:0] addr;		// address;
	integer woff, n;
	reg [31:0] r;			// rom read data;
	reg [31:0] e;			// expecetd data;
	begin
		$display("%t: %M: reqspc=%b, wa=%b", $time, reqspc, wa);
		past = space + `GI_ROM_SIZE;
		woff = wa << 2;
		addr = space;
		for(n = 4; addr < past; n = woff | (n << 1)) begin
			if(reqspc)
				stall8($random);
			xsread(addr, r);
			e = `rom_get(addr[12:2]);
			sexp(e, r, 32'hffff_ffff);
			addr = space + n;
		end
	end
endtask

// check rom with block reads;
// check critial word order at random;
// must be in secure mode;

task gi_check_rom_blk;
	input [31:0] space;		// address space;
	input [3:0] bsize;		// block size in words;
	input reqspc;			// random request spacing;
	reg [31:0] addr;		// address;
	reg [31:0] past;		// end of space;
	integer n, w;
	reg [31:0] e;			// expecetd data;
	reg [0:`BSIZE_BITS-1] blk;
	begin
		$display("%t: %M: bsize=%0d, reqspc=%b, wa=1", $time, bsize, reqspc);
		past = space + `GI_ROM_SIZE;
		addr = space;
		for(n = 32; addr < past; n = n | (n << 1)) begin
			if(reqspc)
				stall8($random);
			sbo_addr(addr, bsize);
			xbread(addr, bsize, blk);
			for(w = 0; w < bsize; w = w + 1) begin
				e = `rom_get(addr[12:2] ^ w);
				sexp(e, blk[0:31], 32'hffff_ffff);
				blk = { blk[32:`BSIZE_BITS-1], 32'bx };
			end
			addr = space + n;
		end
	end
endtask

// check rom reads in non-secure mode;
// entire space should read 0;

task gi_check_rom_zero;
	input [31:0] space;		// address space;
	input [3:0] bsize;		// block size in words;
	input reqspc;			// random request spacing;
	reg [31:0] addr;		// address;
	reg [31:0] past;		// end of space;
	integer n, w;
	reg [0:`BSIZE_BITS-1] blk;
	begin
		// read entire rom via block requests;

		$display("%t: %M: bsize=%0d, reqspc=%b", $time, bsize, reqspc);
		past = space + `GI_ROM_SIZE;
		addr = space;
		for(n = 32; addr < past; n = n + 32) begin
			if(reqspc)
				stall8($random);
			sbo_addr(addr, bsize);
			xbread(addr, bsize, blk);
			for(w = 0; w < bsize; w = w + 1) begin
				sexp(0, blk[0:31], 32'hffff_ffff);
				blk = { blk[32:`BSIZE_BITS-1], 32'bx };
			end
			addr = space + n;
		end
	end
endtask

// check rom content;
// must be in secure mode;
// complete checking is left to system dv;

task gi_check_rom;
	input [31:0] space;
	begin
		$display("%M: space=0x%h", space);
		gi_check_rom_sgl(space, 0, 0);
		gi_check_rom_sgl(space, 0, 1);
		gi_check_rom_sgl(space, 1, 0);
		gi_check_rom_sgl(space, 1, 1);
		gi_check_rom_blk(space, `BSIZE_MAX, 0);
		gi_check_rom_blk(space, `BSIZE_MAX, 1);
	end
endtask

// check sram;
// avoid using the sram backdoor for this;

task gi_check_sram;
	input [31:0] space;
	begin
		$display("%M: space=0x%h", space);
		check_mem_sgl(space, `GI_SRAM_SIZE, 0, 0);
		check_mem_sgl(space, `GI_SRAM_SIZE, 0, 1);
		check_mem_sgl(space, `GI_SRAM_SIZE, 1, 0);
		check_mem_sgl(space, `GI_SRAM_SIZE, 1, 1);
		check_mem_blk(space, `GI_SRAM_SIZE, `BSIZE_MAX, 0);
		check_mem_blk(space, `GI_SRAM_SIZE, `BSIZE_MAX, 1);
	end
endtask

// check virage content after reset recall;

task gi_check_vinit;
	input [31:0] pat;		// initial pattern;
	integer n;
	reg [31:0] r;			// read data;
	reg [31:0] pat;			// fill pattern;
	begin
		$display("%M");
		for(n = 0; n < `GI_V_SIZE; n = n + 4) begin
			xsread(`V_MEM + n, r);
			sexp(pat, r, 'hffff_ffff);
			pat = next_pat(pat);
		end
	end
endtask

// check virage sram;
// avoid using the backdoor for this;

task gi_check_vram;
	input [31:0] space;
	begin
		$display("%M: space=0x%h", space);
		check_mem_sgl(space, `GI_V_SIZE, 0, 0);
		check_mem_sgl(space, `GI_V_SIZE, 0, 1);
		check_mem_sgl(space, `GI_V_SIZE, 1, 0);
		check_mem_sgl(space, `GI_V_SIZE, 1, 1);
		check_mem_blk(space, `GI_V_SIZE, `BSIZE_MAX, 0);
		check_mem_blk(space, `GI_V_SIZE, `BSIZE_MAX, 1);
	end
endtask

// test gi after reset;

task gi_after_reset;
	input test_mem;			// test memories;
	reg [31:0] r;			// read data;
	begin
		$display("%M");

		// init rom and X sram;

		`rom_random;
		`sram_fill(32'bx);

		// must read from reset vector to enter secure mode;
		// turn off alt_boot for rom checks;
		// check defaults after reset;

		gi_sec_after_reset;
		gi_regs_after_reset;

		// check rom and sram in reset space;

		if(test_mem) begin
			gi_check_rom(`GI_ROM_RST);
			gi_check_sram(`GI_SRAM_RST);
		end

		// flip rom and sram and recheck;

		xswrite(`SEC_MODE, `SEC_MODE_ON);
		if(test_mem) begin
			gi_check_rom(`GI_ROM_SWP);
			gi_check_sram(`GI_SRAM_SWP);
		end

		// check virage sram;
		// check content after reset recall;

`ifdef FPGA    // Virage isn't supported on the FPGA
`else
		xswrite(`V_CMD, 'h0);			// set store ctrl idle;
		if(test_mem)
			gi_check_vram(`GI_V);			// only one address space;
`endif

	end
endtask

// bi dma tests;
// DMA is always to SRAM;
// however, cpu must go through right space to check;

task gi_test_bi_dma;
	input [31:0] base;		// base address of sram;
	reg [31:0] mem;			// memory address;
	reg [16:0] ptr;			// sram offset;
	reg [31:0] pat;			// data pattern seed;
	reg intr;				// random interrupt check;
	integer size;			// size in dma blocks;
	integer over;			// ptr beyond sram;
	begin
		// setup system environment;
		// single block mem->gi;
		// walk all BI_MEM/BI_CTRL(PTR) address bits;
		// remember, memory model mirrors physical memory;
		// ptr must be aligned to cacheline and cannot wrap sram;

		$display("%M: single block, mem->gi, walk mem addr");
		sys_dma_setup(1, 1);
		sys_mon_setup(1);
		ptr = 0;
		mem = `MEM_BASE;
		while(mem < `MEM_MAX) begin
			`sram_fill(32'bx);				// fill sram with Xs;
			pat = $random;
			dma_bpat(mem, 1, pat);			// fill memory pattern;
			intr = ^pat;
			gi_bi_dma(mem, ptr, 0, 0, intr, 0, 2, 0);
			dma_bexp(base | ptr, 1, pat);
			mem = (mem << 1) | 128;
			ptr = (ptr << 1) | 128;
			if(ptr >= `GI_SRAM_SIZE)
				ptr = 0;					// don't walk out of sram;
		end

		// gi->mem, vary size;
		// however, limit size for short runtime;
		// pick random mem and ptr;
		// mem can wrap (enough physical), but not ptr;

		$display("%M: varying size, mem<-gi, random mem addr");
		for(size = 1; size <= 512; size = (size << 1) + 1) begin
			mem = $random & `MEM_MASK;
			mem[6:0] = 0;
			ptr = { $random, 7'd0 };		// force to cacheline;
			over = ptr + size*128 - `GI_SRAM_SIZE;
			if(over > 0)
				ptr = ptr - over;			// align to end;
			pat = $random;
			dma_bpat(base | ptr, size, pat);
			intr = ^pat;
			gi_bi_dma(mem, ptr, size - 1, 1, intr, 0, 2 * size, 0);
			dma_bexp(mem, size, pat);
		end

		// gi<-mem, vary size;
		// however, limit size for short runtime;
		// pick random mem and ptr;
		// mem can wrap (enough physical), but not ptr;

		$display("%M: varying size, mem->gi, random mem addr");
		for(size = 1; size <= 512; size = (size << 1) + 1) begin
			`sram_fill(32'bx);				// fill sram with Xs;
			mem = $random & `MEM_MASK;
			mem[6:0] = 0;
			ptr = { $random, 7'd0 };		// force to cacheline;
			over = ptr + size*128 - `GI_SRAM_SIZE;
			if(over > 0)
				ptr = ptr - over;			// align to end;
			pat = $random;
			dma_bpat(mem, size, pat);		// fill memory pattern;
			intr = ^pat;
			gi_bi_dma(mem, ptr, size - 1, 0, intr, 0, 2 * size, 0);
			dma_bexp(base | ptr, size, pat);
		end

		// start dma, then kill it;
		// see that it does not dma more than 1 block;
		// must give it timeout of at least one dma block;
		// dma re-arbitration is delayed until bus flushes transaction;

		$display("%M: start & kill");
		intr = 1;
		gi_bi_dma(0, 0, 0, 0, intr, 0, 2, 1);		// read dma;
		gi_bi_dma(0, 0, 0, 1, intr, 0, 2, 1);		// write dma;

		// dma should not write spaces other than sram;
		// dma should not write sram when other spaces are addressed;
		// XXX virage
		// XXX regs;

		// check that it cannot overwrite DI CRB;
		// moving CRB into main memory is allowed;
		// XXX

		// check that non-secure dma does not write to
		// secure sram area, set by SEC_SRAM;
		// XXX

		// tear down dma setup;

		sys_dma_setup(0, 0);
		sys_mon_setup(0);
	end
endtask

// bi dma test;
// write/read sequences;

task gi_bi_dma_wrseq;
	input [15:0] runs;		// # of runs;
	reg [31:0] mem;			// memory address;
	reg [31:0] base;		// base of GI;
	reg [31:0] pat [0:1];	// buf 0/1 patterns;
	reg [31:0] ctrl;		// bi ctrl reg;
	reg [31:0] r;			// read data;
	integer n;
	begin
		$display("%M: %0d runs", runs);
		base = `GI_SRAM_SWP;
		`sram_fill(32'bx);				// fill sram with Xs;
		sys_dma_setup(1, 1);
		sys_mon_setup(1);
		for(n = 0; n < runs; n = n + 1) begin
			// pick random memory address and patterns;

			mem = $random & `MEM_MASK;
			mem[6:0] = 0;
			xswrite(`BI_MEM, mem);
			pat[0] = $random;
			pat[1] = $random;
			$display("%t: %M: run %0d, mem=%h pat=%h/%h", $time, n, mem, pat[0], pat[1]);

			// seed memories with pattern;

			dma_bpat(base, 1, pat[0]);
			xsread(base, r);
			dma_bpat(mem + 128, 1, pat[1]);
			xsread(mem + 128, r);

			// start write dma, wait for completion;
			// sweep delay for n clocks;
			// start read dma, wait for completion;

			$display("%t: %M: start write", $time);
			ctrl = 'h8000_0000;
			ctrl[30] = 0;
			ctrl[29:20] = 0;
			ctrl[19] = 1;
			ctrl[16:4] = 0;
			xswrite(`BI_CTRL, ctrl);
			xsread(`BI_CTRL, r);
			while(r[31] === 1'b1)
				xsread(`BI_CTRL, r);

			stall(n);

			$display("%t: %M: start read", $time);
			ctrl = 'h8000_0000;
			ctrl[30] = 0;
			ctrl[29:20] = 0;
			ctrl[19] = 0;
			ctrl[16:4] = 0;
			xswrite(`BI_CTRL, ctrl);
			xsread(`BI_CTRL, r);
			while(r[31] === 1'b1)
				xsread(`BI_CTRL, r);

			// check expecetd patterns;

			xsread(mem, r);
			dma_bexp(mem, 1, pat[0]);
			dma_bexp(base, 1, pat[1]);
		end
		sys_dma_setup(0, 1);
		sys_mon_setup(0);
	end
endtask

// do a di interface reset;
// test various bits and the reset interrupt;

task gi_di_reset;
	reg [31:0] ctrl;		// di control;
	reg [31:0] r;			// read data;
	begin
		$display("%t: %M", $time);
		`di_reset(1);					// assert reset;
		ctrl[31] = 1;					// write interrupt masks;
		ctrl[30:24] = 7'b000_0001;		// enable RST_MASK;
		ctrl[23:16] = 8'hff;			// clear all pending intr;
		ctrl[15:0] = 0;					// don't affect other bits;
		xswrite(`DI_CTRL, ctrl);
		xsread(`DI_CTRL, r);
		if(r[30:24] !== ctrl[30:24]) begin
			$display("ERROR: %t: %M: INTR_MASK=%b exp %b",
				$time, r[30:24], ctrl[30:24]);
		end
		if(r[16] !== 1'b1)
			$display("ERROR: %t: %M: RST=%b exp 1", $time, r[16]);

		// RST bit is active until reset is released;

		`di_reset(2);					// deassert reset;
		xsread(`DI_CTRL, r);
		if(r[16] !== 1'b1)
			$display("ERROR: %t: %M: RST=%b exp 1", $time, r[16]);

		// clear RST interrupt;
		// RST bit should go back to 0;

		xswrite(`DI_CTRL, ctrl);		// clear RST bit;
		xsread(`DI_CTRL, r);
		if(r[16] !== 1'b0)
			$display("ERROR: %t: %M: RST=%b exp 0", $time, r[16]);

		// should be in CMD_IN state with DIR=0;

		if(r[1] !== 1'b1)
			$display("ERROR: %t: %M: CMD_IN=%b exp 1", $time, r[1]);
		if(r[0] !== 1'b0)
			$display("ERROR: %t: %M: DIR=%b exp 0", $time, r[0]);

		// config DI for fastest GC speed;

		ctrl[31:24] = 8'ha8;
		ctrl[23:16] = 8'h00;
		ctrl[15:0] = `DI_TIM_GC162;
		xswrite(`DI_CONF, ctrl);
		xsread(`DI_CONF, r);
		if(r !== ctrl) begin
			$display("ERROR: %t: %M: DI_CONF=0x%h/%b exp 0x%h/%b",
				$time, r, r, ctrl, ctrl);
		end
	end
endtask

// check di control state;

task gi_di_ctrl;
	input [31:0] mask;
	input [31:0] val;
	reg [31:0] r;
	begin
		xsread(`DI_CTRL, r);
		if((r & mask) !== (val & mask))
			$display("ERROR: %t: %M: DI_CTRL=0x%h exp 0x%h mask 0x%h",
				$time, r, val, mask);
	end
endtask

// start BE burst;
// it is assumed that be is not busy;
// don't wait for BE to complete;

task gi_be_start;
	input [11:0] size;		// size in bytes;
	input out;				// out phase;
	input [16:0] ptr;		// sram pointer;
	reg [31:0] mask;		// ctrl mask;
	reg [31:0] ctrl;		// expected ctrl values;
	reg [31:0] be;			// be cmd code;
	reg [31:0] r;			// read data;
	begin
		// issue BE burst request;

		$display("%t: %M: size=%0d, out=%b, ptr=0x%h",
			$time, size, out, ptr);
		mask = `DI_CTRL_BREAK_STATE | `DI_CTRL_DIR_STATE;
		ctrl = 0;
		ctrl[0] = out;
		gi_di_ctrl(mask, ctrl);
			
		be[31:20] = size - 1;
		be[19] = out;
		be[18] = 1;
		be[17] = 0;
		be[16:0] = ptr;
		xswrite(`DI_BE, be);
		xsread(`DI_BE, r);
		if(r[19] !== out)
			$display("ERROR: %t: %M: BE_OUT=%b exp %b", $time, r[19], out);
		if(r[18] !== 1'b1)
			$display("ERROR: %t: %M: BE_EXEC=%b exp 1", $time, r[18]);
	end
endtask

// wait for BE to finish;
// timeout if it takes longer than to;
// or stop on first wanted interrupt;

task gi_di_wait;
	input [15:0] to;		// timeout in usec;
	input [31:0] intr;		// wanted interrupt;
	reg [31:0] ctrl;		// di ctrl;
	reg [31:0] r;			// read data;
	reg done;				// done with burst;
	begin
		$display("%t: %M: to=%0dus, intr=0x%h", $time, to, intr);
		done = 0;
		while((done !== 1'b1) && (to > 0)) begin
			stall(100);
			if(intr != 0) begin
				xsread(`DI_CTRL, ctrl);
				done = |(ctrl[19:16] & intr[19:16]);
			end else begin
				xsread(`DI_BE, r);
				if(r[18] === 1'b0)
					done = 1;
			end
			to = to - 1;
		end
		if(done !== 1)
			$display("ERROR: %t: %M: timeout, DI_BE=%b", $time, r);
		$display("%t: %M: intr=0x%h size=%0d, out=%b, ptr=0x%h",
			$time, ctrl[19:16], r[31:20], r[19], r[16:0]);
	end
endtask

// formulate command in host;

task gi_di_make_cmd;
	input [31:0] pat;			// pattern seed;
	input [15:0] size;			// size in bytes;
	integer n;
	begin
		$display("%t: %M: pat=0x%h, size=%0d",
			$time, pat, size);
		for(n = 0; n < size; n = n + 4) begin
			`di_put(n, pat);
			pat = next_pat(pat);
		end
	end
endtask

// check cmd in sram;

task gi_di_check_cmd;
	input [31:0] addr;			// address in buffer;
	input [31:0] pat;			// pattern seed;
	input [15:0] size;			// size in bytes;
	integer n;
	reg [31:0] r;				// read data;
	begin
		$display("%t: %M: addr=0x%h pat=0x%h, size=%0d",
			$time, addr, pat, size);
		for(n = 0; n < size; n = n + 4) begin
			xsread(addr, r);
			if(r !== pat) begin
				$display("ERROR: %t: %M: [0x%h] [%0d] 0x%h exp 0x%h",
					$time, addr, n, r, pat);
			end
			pat = next_pat(pat);
			addr = addr + 4;
		end
	end
endtask

// formulate reply data in sram;

task gi_di_make_reply;
	input [31:0] addr;			// sram address;
	input [31:0] pat;			// pattern seed;
	input [15:0] size;			// size in bytes;
	integer n;
	begin
		$display("%t: %M: addr=0x%h pat=0x%h, size=%0d",
			$time, addr, pat, size);
		for(n = 0; n < size; n = n + 4) begin
			xswrite(addr, pat);
			pat = next_pat(pat);
			addr = addr + 4;
		end
	end
endtask

// check reply data in host buffer;

task gi_di_check_reply;
	input [31:0] pat;			// pattern seed;
	input [15:0] size;			// size in bytes;
	integer n;
	reg [31:0] r;				// read data;
	begin
		$display("%t: %M: pat=0x%h, size=%0d",
			$time, pat, size);
		for(n = 0; n < size; n = n + 4) begin
			`di_get(n, r);
			if(r !== pat) begin
				$display("ERROR: %t: %M: [%0d] 0x%h exp 0x%h",
					$time, n, r, pat);
			end
			pat = next_pat(pat);
		end
	end
endtask

// force ptr alignment;
// make sure we don't walk out of sram;

function [16:0] gi_ptr;
	input [16:0] ptr;
	input [16:0] size;
	integer over;
	begin
		ptr[1:0] = 0;
		if(ptr[16])
			ptr[15] = 0;
		over = ptr + size - `GI_SRAM_SIZE;
		if(over > 0)
			ptr = ptr - over;
		gi_ptr = ptr;
	end
endfunction

// start host di transaction;

task di_host_start;
	input dir;					// host->gi;
	input [15:0] tsize;			// host transmit size;
	input [15:0] rsize;			// host response size;
	input wr_cmd;				// is a write transaction;
	input rss;
	begin
		$display("%t: %M: dir=%b, wr_cmd=%b, tsize=%0d, rsize=%0d",
			$time, dir, wr_cmd, tsize, rsize);
		`di_xfer(dir, tsize, rsize, wr_cmd, rss);
	end
endtask
	

// start gi di transaction;

task gi_di_start;
	input dir;					// host->gi;
	input [16:0] ptr;			// sram offset;
	input [15:0] size;			// be size;
	begin
		// start either host or BE first;
		// one should always wait for the other;

		$display("%t: %M: dir=%b, ptr=0x%h, size=%0d",
			$time, dir, ptr, size);
		gi_be_start(size, dir, ptr);
	end
endtask

// be interrupt environment;

task gi_di_intr;
	input setup;				// setup interrupts;
	input [31:0] intr;			// interrupt 
	reg [31:0] r;				// status;
	reg [27:24] mask;			// intr mask;
	begin
		$display("%t: %M: setup=%b, intr=0x%h",
			$time, setup, intr);
		if(setup) begin
			intr[31] = 1;				// write intr mask;
			intr[23:16] = 8'hff;		// clear all pending;
			intr[15:0] = 0;				// do not touch anything else;
			xswrite(`DI_CTRL, intr);
			xsread(`DI_CTRL, r);
			if(r[19:16] !== 4'd0)
				$display("ERROR: %t: %M: intr not cleared, %b", $time, r[19:16]);
		end else begin
			xsread(`DI_CTRL, r);
			mask = intr[27:24];
			if((r[19:16] & mask) !== (intr[19:16] & mask)) begin
				$display("ERROR: %t: %M: mask %b, intr %b exp %b",
					$time, mask, r[19:16], intr[19:16]);
			end
		end
	end
endtask

// simple di request/reply tests;
// tests do not use sha/aes, nor gi ctrl;
// no breaks, errors or BE kills;
// tests BE polling and DONE_INTR;

task gi_di_simple;
	reg [16:0] ptr;				// sram offset;
	reg [31:0] pat;				// di command/reply pattern;
	reg rss;					// random strobes/stalls;
	begin
		// fully reset the di interface;

		$display("%M: reset");
		gi_di_reset;

		// issue inquiry command before di is ready;
		// let device stall host;
		// dstrb should stall the host sending the command;
		// turn around for reception of response;
		// poll the BE EXEC bit for end of bursts;

		$display("%M: inquiry");
		ptr = gi_ptr($random, 12);
		rss = 0;
		pat = 'h1200_0000;
		gi_di_make_cmd(pat, 12);
		gi_di_intr(1, 0);
		di_host_start(`HDIR_OUT, 12, 32, 0, rss);
		gi_di_start(`HDIR_OUT, ptr, 12);
		gi_di_wait(1, 0);
		gi_di_intr(0, `DI_CTRL_ALL_MASK);
		gi_di_check_cmd(`GI_SRAM_SWP + ptr, pat, 12);

		ptr = gi_ptr($random, 32);
		pat = $random;
		gi_di_make_reply(`GI_SRAM_SWP + ptr, pat, 32);
		gi_di_intr(1, 0);
		di_host_start(`HDIR_IN, 12, 32, 0, rss);
		gi_di_start(`HDIR_IN, ptr, 32);
		gi_di_wait(5, 0);
		gi_di_intr(0, `DI_CTRL_ALL_MASK);
		gi_di_check_reply(pat, 32);

		// test simple command with shortest 4-byte response;
		// let host stall device;
		// use the DONE_INTR for end of BE bursts;

		$display("%M: shortest response");
		ptr = gi_ptr($random, 12);
		rss = 1;
		pat = 'he012_3456;
		gi_di_make_cmd(pat, 12);
		gi_di_intr(1, `DI_CTRL_DONE_MASK);
		gi_di_start(`HDIR_OUT, ptr, 12);
		di_host_start(`HDIR_OUT, 12, 4, 0, rss);
		gi_di_wait(1, `DI_CTRL_DONE_INTR);
		gi_di_intr(0, `DI_CTRL_ALL_MASK | `DI_CTRL_DONE_INTR);
		gi_di_check_cmd(`GI_SRAM_SWP + ptr, pat, 12);

		ptr = gi_ptr($random, 4);
		pat = $random;
		gi_di_make_reply(`GI_SRAM_SWP + ptr, pat, 4);
		gi_di_intr(1, `DI_CTRL_DONE_MASK);
		gi_di_start(`HDIR_IN, ptr, 4);
		di_host_start(`HDIR_IN, 12, 4, 0, rss);
		gi_di_wait(2, `DI_CTRL_DONE_INTR);
		gi_di_intr(0, `DI_CTRL_ALL_MASK | `DI_CTRL_DONE_INTR);
		gi_di_check_reply(pat, 4);

		// test command with two BE responses;
		// cmd uses DONE_INTR;
		// first response polls, second uses DONE_INTR;

		$display("%M: two response bursts");
		ptr = gi_ptr($random, 12);
		rss = $random;
		pat = 'h7f80_01fe;
		gi_di_make_cmd(pat, 12);
		gi_di_intr(1, `DI_CTRL_DONE_MASK);
		gi_di_start(`HDIR_OUT, ptr, 12);
		di_host_start(`HDIR_OUT, 12, 40+24, 0, rss);
		gi_di_wait(1, `DI_CTRL_DONE_INTR);
		gi_di_intr(0, `DI_CTRL_ALL_MASK | `DI_CTRL_DONE_INTR);
		gi_di_check_cmd(`GI_SRAM_SWP + ptr, pat, 12);

		ptr = gi_ptr($random, 40+24);
		pat = $random;
		gi_di_make_reply(`GI_SRAM_SWP + ptr, pat, 40+24);
		gi_di_intr(1, 0);
		di_host_start(`HDIR_IN, 12, 40+24, 0, rss);
		gi_di_start(`HDIR_IN, ptr + 0, 40);
		gi_di_wait(5, 0);
		gi_di_intr(0, `DI_CTRL_ALL_MASK);
		gi_di_intr(1, `DI_CTRL_DONE_MASK);
		gi_di_start(`HDIR_IN, ptr + 40, 24);
		gi_di_wait(2, `DI_CTRL_DONE_INTR);
		gi_di_intr(0, `DI_CTRL_ALL_MASK | `DI_CTRL_DONE_INTR);
		gi_di_check_reply(pat, 40+24);

		// test a new write command;
		// the in phase is longer than 12 bytes;
		// first cmd uses DONE_INTR, second polls;
		// response uses DONE_INTR;

		$display("%M: two cmd bursts");
		ptr = gi_ptr($random, 12+32+12);
		rss = $random;
		pat = 'h0123_4567;
		gi_di_make_cmd(pat, 12+32+12);
		gi_di_intr(1, `DI_CTRL_DONE_MASK);
		di_host_start(`HDIR_OUT, 12+32+12, 4, 1, rss);
		gi_di_start(`HDIR_OUT, ptr + 0, 12);
		gi_di_wait(2, `DI_CTRL_DONE_INTR);
		gi_di_intr(0, `DI_CTRL_ALL_MASK | `DI_CTRL_DONE_INTR);
		gi_di_intr(1, 0);
		gi_di_start(`HDIR_OUT, ptr + 12, 32+12);
		gi_di_wait(3, 0);
		gi_di_intr(0, `DI_CTRL_ALL_MASK);
		gi_di_check_cmd(`GI_SRAM_SWP + ptr, pat, 12+32+12);

		ptr = gi_ptr($random, 4);
		pat = $random;
		gi_di_make_reply(`GI_SRAM_SWP + ptr, pat, 4);
		gi_di_intr(1, `DI_CTRL_DONE_MASK);
		di_host_start(`HDIR_IN, 12+32+12, 4, 0, rss);
		gi_di_start(`HDIR_IN, ptr, 4);
		gi_di_wait(1, `DI_CTRL_DONE_INTR);
		gi_di_intr(0, `DI_CTRL_ALL_MASK | `DI_CTRL_DONE_INTR);
		gi_di_check_reply(pat, 4);
	end
endtask

// set error signal;

task gi_di_error;
	input set;
	reg [31:0] ctrl;
	begin
		$display("%t: %M: set=%b", $time, set);
		ctrl = set? `DI_CTRL_ERROR : 0;
		xswrite(`DI_CTRL, `DI_CTRL_ERROR_WE | ctrl);
	end
endtask

// test di response with error;
// also tests the DIR_INTR;

task gi_di_rsp_error;
	reg [16:0] ptr;				// sram offset;
	reg [31:0] pat;				// di command/reply pattern;
	reg rss;					// random strobes/stalls;
	begin
		// error after command;

		$display("%M: error after command");
		ptr = gi_ptr($random, 12);
		rss = $random;
		pat = 'hf055_aa0f;
		gi_di_make_cmd(pat, 12);
		gi_di_intr(1, 0);
		di_host_start(`HDIR_OUT, 12, 32, 0, rss);
		gi_di_start(`HDIR_OUT, ptr, 12);
		gi_di_wait(2, 0);
		gi_di_check_cmd(`GI_SRAM_SWP + ptr, pat, 12);

		// tell host to begin an IN phase;
		// however, do not start BE to send response;
		// instead, assert error;
		// interface should go back to OUT state;
		// errror can stay asserted until next cmd is in;

		rss = $random;
		di_host_start(`HDIR_IN, 12, 32, 0, rss);
		gi_di_intr(1, `DI_CTRL_DIR_MASK);
		gi_di_error(1);
		gi_di_wait(2, `DI_CTRL_DIR_INTR);
		gi_di_intr(0, `DI_CTRL_ALL_MASK | `DI_CTRL_DIR_INTR);

		// get next command;
		// release error;

		$display("%M: next command");
		ptr = gi_ptr($random, 12);
		rss = $random;
		pat = 'he0f5_5aa7;
		gi_di_make_cmd(pat, 12);
		gi_di_intr(1, `DI_CTRL_DONE_MASK);
		di_host_start(`HDIR_OUT, 12, 4, 0, rss);
		gi_di_start(`HDIR_OUT, ptr, 12);
		gi_di_wait(2, `DI_CTRL_DONE_INTR);
		gi_di_error(0);
		gi_di_intr(0, `DI_CTRL_ALL_MASK | `DI_CTRL_DONE_INTR);
		gi_di_check_cmd(`GI_SRAM_SWP + ptr, pat, 12);

		// handle response;

		ptr = gi_ptr($random, 4);
		pat = $random;
		gi_di_make_reply(`GI_SRAM_SWP + ptr, pat, 4);
		gi_di_intr(1, 0);
		gi_di_start(`HDIR_IN, ptr, 4);
		di_host_start(`HDIR_IN, 12, 4, 0, rss);
		gi_di_wait(2, 0);
		gi_di_intr(0, `DI_CTRL_ALL_MASK);
		gi_di_check_reply(pat, 4);
	end
endtask

// test di break protocol;

task gi_di_break;
	reg [16:0] ptr;				// sram offset;
	reg [31:0] pat;				// di command/reply pattern;
	reg rss;					// random strobes/stalls;
	reg [7:0] rdel;				// random length of break phase 4;
	begin
		// issue normal command;

		$display("%M: issue cmd");
		pat = $random;
		ptr = gi_ptr(pat, 12);
		rss = pat[31];
		pat[31:24] = 'h01;
		gi_di_make_cmd(pat, 12);
		gi_di_intr(1, `DI_CTRL_BREAK_MASK);
		di_host_start(`HDIR_OUT, 12, 64, 0, rss);
		gi_di_start(`HDIR_OUT, ptr, 12);
		gi_di_wait(2, 0);
		gi_di_check_cmd(`GI_SRAM_SWP + ptr, pat, 12);

		// set random break position;
		// check only bytes up to the break position;

		$display("%M: issue break");
		`di_break(32);
		pat = $random;
		ptr = gi_ptr(pat, 64);
		rss = pat[31];
		gi_di_make_reply(`GI_SRAM_SWP + ptr, pat, 64);
		gi_di_intr(1, `DI_CTRL_DONE_MASK | `DI_CTRL_BREAK_MASK);
		gi_di_start(`HDIR_IN, ptr, 64);
		di_host_start(`HDIR_IN, 12, 64, 0, rss);
		gi_di_wait(2, `DI_CTRL_DONE_INTR | `DI_CTRL_BREAK_INTR);
		gi_di_intr(0, `DI_CTRL_ALL_MASK | `DI_CTRL_BREAK_INTR);
		gi_di_check_reply(pat, 32);

		// acknowledge break;
		// kill BE burst;
		// terminate break;

		$display("%M: ack break");
		gi_di_ctrl(`DI_CTRL_BREAK_STATE, `DI_CTRL_BREAK_STATE);
		xswrite(`DI_CTRL, `DI_CTRL_BREAK_INTR);
		xswrite(`DI_BE, 'h0);
		xsread(`DI_CTRL, pat);
		gi_di_ctrl(`DI_CTRL_BREAK_STATE, 0);
		rdel = $random;
		#(77 + rdel);
		xswrite(`DI_CTRL, `DI_CTRL_BREAK_TERM);

		// issue new command;

		$display("%M: new command");
		pat = $random;
		ptr = gi_ptr(pat, 12);
		rss = pat[31];
		pat[31:24] = 'h20;
		gi_di_make_cmd(pat, 12);
		gi_di_intr(1, `DI_CTRL_DONE_MASK);
		gi_di_start(`HDIR_OUT, ptr, 12);
		di_host_start(`HDIR_OUT, 12, 4, 0, rss);
		gi_di_wait(2, `DI_CTRL_DONE_INTR);
		gi_di_check_cmd(`GI_SRAM_SWP + ptr, pat, 12);

		ptr = gi_ptr($random, 4);
		pat = $random;
		gi_di_make_reply(`GI_SRAM_SWP + ptr, pat, 4);
		gi_di_intr(1, 0);
		di_host_start(`HDIR_IN, 12, 4, 0, rss);
		gi_di_start(`HDIR_IN, ptr, 4);
		gi_di_wait(2, 0);
		gi_di_intr(0, `DI_CTRL_ALL_MASK);
		gi_di_check_reply(pat, 4);
	end
endtask



// test ais registers;
// only bits without side effects;
// cannot test AIS_CTRL, start is needed to set intr mask;

task gi_ais_regs;
	reg [31:0] r;			// read data;
	begin
		xsread(`AIS_CTRL, r);
		if(r[31] !== 1'b0)
			$display("ERROR: %t: %M: ais busy, BI_CTRL=0x%h", $time, r);
		$display("%M: AIS_PTR0");
		test_reg(`AIS_PTR0, `AIS_PTR_ADDR | `AIS_PTR_EMPTY, 0);
		$display("%M: AIS_PTR1");
		test_reg(`AIS_PTR1, `AIS_PTR_ADDR | `AIS_PTR_EMPTY, 0);
		$display("%M: AIS_BUF");
		test_reg(`AIS_BUF, `AIS_BUF_OFF, 0);
	end
endtask

// test ais controller start/stop;

task gi_ais_kill;
	reg [31:0] mem;		// memory address;
	reg [31:0] pat;		// seed pattern;
	reg [31:0] r;		// read data;
	begin
		// fill one audio buffer with pattern;

		mem = `MEM_BASE;
		pat = $random;
		dma_bpat(mem, 4096/128, pat);			// fill memory pattern;

		// ais_clk from host, but no ais_lr;

		$display("%M: start, host idle");
		xswrite(`AIS_PTR0, `AIS_PTR_FULL | mem);
		xswrite(`AIS_PTR1, `AIS_PTR_EMPTY | 0);
		xswrite(`AIS_CTRL, `AIS_CTRL_EXEC | 0);
		xsread(`AIS_CTRL, r);
		if(r[31] !== 1'b1)
			$display("ERROR: %t: %M: EXEC=%b exp 1", $time, r[31]);
		xswrite(`AIS_CTRL, 0);
		xsread(`AIS_CTRL, r);
		if(r[31] !== 1'b0)
			$display("ERROR: %t: %M: EXEC=%b exp 0", $time, r[31]);
	end
endtask

// fill ais sample buffer with expected pattern;

task gi_ais_pat;
	input [15:0] ns;	// # of samples;
	input [31:0] pat;	// pattern seed;
	integer n;
	begin
		for(n = 0; n < ns; n = n + 1) begin
			`ais_put(n, pat);
			pat = next_pat(pat);
		end
	end
endtask

// check ais interrupt;

task gi_ais_intr;
	input exp;			// expected value;
	reg [31:0] r;		// read data;
	begin
		xsread(`BI_INTR, r);
		if(r[2] !== exp)
			$display("ERROR: %t: %M: ais intr=%b exp %b", $time, r[2], exp);
	end
endtask

// test ais streaming audio;
// clock ais_host as fast as gi can handle;
// ais_host only has a 64kB buffer;

task gi_ais_stream;
	input [3:0] nbuf;		// # of 4kB blocks to stream;
	input fast;				// run ais_host faster than normal;
	input intr;				// use interrupt;
	reg [31:0] mem;			// memory address;
	reg [31:0] pat;			// seed pattern;
	reg [31:0] r;			// read data;
	reg [31:0] ctrl;		// control register;
	integer nb;				// # of bytes;
	integer n;
	reg [31:0] bufs [0:15];	// all 4kB buffers;
	reg [31:0] ptr;			// buffer pointer;
	integer to;				// timeout;
	reg done;				// done with buffer;
	begin
		// run ais_host with optional fast clock,
		// to save time in the simulator;

		$display("%M: nbuf=%0d fast=%b", nbuf, fast);
		`ais_set(fast);
		sys_dma_setup(1, 1);
		sys_mon_setup(1);

		// fill audio buffers with pattern;
		// setup buffer list;

		mem = `MEM_BASE;
		mem[19:12] = $random;				// random 4kB block;
		pat = $random;
		nb = nbuf * 4096;
		gi_ais_pat(nb / 4, pat);			// fill ais_host buffer;
		dma_bpat(mem, nb / 128, pat);		// fill memory pattern;
		for(n = 0; n < 16; n = n + 1) begin
			ptr = mem + n * 4096;
			if(n >= nbuf)
				ptr[0] = 1;					// empty buffer;
			bufs[n] = ptr;
		end

		// start ais_host, toggles ais_lr;
		// pick random buffer in sram;
		// start ais stream;

		`ais_start(nb / 4, 1);
		gi_ais_intr(0);
		xswrite(`AIS_PTR0, bufs[0]);
		xswrite(`AIS_PTR1, bufs[1]);
		ptr = gi_ptr($random, 256);
		xswrite(`AIS_BUF, ptr);
		ctrl = `AIS_CTRL_EXEC;
		if(intr)
			ctrl = ctrl | `AIS_CTRL_MASK;
		xswrite(`AIS_CTRL, ctrl);

		// ais should be busy now;
		// but still no interrupt;

		xsread(`AIS_CTRL, r);
		if(r[31] !== 1'b1)
			$display("ERROR: %t: %M: start EXEC=%b exp 1", $time, r[31]);
		gi_ais_intr(0);

		// wait for buffers to become empty;
		// check interrupts;
		// supply next buffer;

		ptr = `AIS_PTR0;
		for(n = 0; n < nbuf; n = n + 1) begin
			to = 1024 * 40 * `ais_period / 1000;
			done = 0;
//XXX use gi_wait_ctrl;
			while( !done && (to > 0)) begin
				stall(100);
				xsread(ptr, r);
				done = (r[0] === 1'b1);
				to = to - 1;
			end
//XXX check AIS_CTRL for dma errors;
			if(done) begin
				gi_ais_intr(intr);
				xswrite(ptr, bufs[n+2]);
				gi_ais_intr(0);
				ptr = n[0]? `AIS_PTR0 : `AIS_PTR1;
			end else
				n = nbuf;
		end
		if( !done)
			$display("ERROR: %t: %M: timeout buf=%0d", $time, n);

		// ais should still be busy until the last
		// samples have been sent out;

		xsread(`AIS_CTRL, r);
		if(r[31] !== 1'b1)
			$display("ERROR: %t: %M: last EXEC=%b exp 1", $time, r[31]);
		gi_ais_intr(0);

		// dma end is two sram buffers ahead;
		// 2 buffers * 32 samples * 32 bits/sample * cycle time;

		to = 2 * 32 * 32 * `ais_period / 10;
		stall(to);
		xsread(`AIS_CTRL, r);
		if(r[31] !== 1'b0)
			$display("ERROR: %t: %M: done EXEC=%b exp 0", $time, r[31]);
		gi_ais_intr(0);

		// tear down dma channel;

		`ais_rst;
		sys_dma_setup(0, 0);
		sys_mon_setup(0);
		stall(2);
	end
endtask

// test aes registers;
// only bits without side effects;
// aese//aesd/mc must be idle;
// key expander can only be tested via crypto ops;
// CTRL registers cannot be tested without starting an operation;

task gi_aese_regs;
	begin
		$display("%M: AESE_IV");
		test_reg(`AESE_IV, `AES_IV_OFF, 0);
		//XXX non-secure tests;
	end
endtask

task gi_aesd_regs;
	begin
		$display("%M: AESD_IV");
		test_reg(`AESD_IV, `AES_IV_OFF, 0);
		//XXX non-secure tests;
	end
endtask

// load an aes key and expand;

task gi_aes_key;
	input [127:0] key;		// key;
	input [1:0] which;		// key index;
	reg dkey;				// expand decryption key;
	reg [31:0] r;			// read data;
	reg [31:0] exp;			// expected data;
	begin
		// stall if expanding a decryption key;

		$display("%t: %M: kidx=%d key=0x%h", $time, which, key);
		dkey = ~which[1];
		if(dkey) begin
			xswrite(`AES_KEXP, `AES_KEXP_STALL);
			xsread(`AES_KEXP, r);
			stall(40);
			xsread(`AES_KEXP, r);
			exp = { 27'd0, 3'b110, 2'b00 };
			if(r !== exp)
				$display("ERROR: %t: %M: stall KEXP %h exp %h", $time, r, exp);
		end

		// write new key;

		xswrite(`AES_KEY, key[127:96]);
		xswrite(`AES_KEY, key[95:64]);
		xswrite(`AES_KEY, key[63:32]);
		xswrite(`AES_KEY, key[31:0]);

		// start key expansion;
		// key expansion should be done in 15 clocks;
		// then unstall key expander;

		if(dkey) begin
			xswrite(`AES_KEXP, { 27'd0, 3'b111, which });
			xsread(`AES_KEXP, r);
			stall(15);
			xsread(`AES_KEXP, r);
			exp = { 27'd0, 3'b110, 1'b0, which[0] };
			if(r !== exp)
				$display("ERROR: %t: %M: run KEXP 0x%x exp 0x%x", $time, r, exp);
			xswrite(`AES_KEXP, 0);
		end
	end
endtask

// test aes key expansion;
// read expanded keys from hw and compare;

task gi_aes_kexp;
	integer n;
	reg [31:0] kw [0:3];	// key words;
	reg [31:0] r;			// read data;
	reg [127:0] key;		// key;
	integer i;				// key index;
	begin
		$display("%M: key write/read");
		for(n = 0; n < 4; n = n + 1) begin
			kw[n] = $random;
			xswrite(`AES_KEY, kw[n]);
		end
		for(n = 0; n < 4; n = n + 1) begin
			xsread(`AES_KEY, r);
			if(r !== kw[n])
				$display("ERROR: %t: %M: word[%0d]=%h exp %h", $time, n, r, kw[n]);
		end
			

		for(i = `AESD_KEXP_MC; i <= `AESE_KEXP_MC; i = i + 1) begin
			$display("%M: kidx %0d", i);
			key = { $random, $random, $random, $random };
			gi_aes_key(key, i);
			//XXX backdoor check
		end
	end
endtask

// set iv offset;

task gi_aesx_ivoff;
	input [31:0] addr;		// addr of iv base register;
	input [31:0] ivoff;		// new iv offset;
	reg [31:0] sec;			// SEC_MODE;
	reg [31:0] old;			// old iv base;
	reg [31:0] r;			// read back iv offset;
	reg [31:0] exp;			// expected iv off, old or new;
	begin
		xsread(`SEC_MODE, sec);
		$display("%t: %M: secure=%b, addr=0x%h, ivoff=0x%h", $time, sec[0], addr, ivoff[16:0]);
		xsread(addr, old);
		xswrite(addr, ivoff);
		xsread(addr, r);
		ivoff = ivoff & `AES_IV_OFF;
		exp = sec[0]? ivoff : old;
		if(r !== exp)
			$display("ERROR: %t: %M: secure=%b, ivoff 0x%h exp 0x%h", $time, sec[0], r, exp);
	end
endtask

// set iv;

task gi_aesx_iv;
	input [31:0] addr;		// sram address;
	input [127:0] iv;		// iv;
	begin
		$display("%t: %M: addr=0x%h iv=0x%h", $time, addr, iv);
		xswrite(addr +  0, iv[127:96]);
		xswrite(addr +  4, iv[95:64]);
		xswrite(addr +  8, iv[63:32]);
		xswrite(addr + 12, iv[31:0]);
	end
endtask

// start an aes operation;

task gi_aesx_start;
	input [31:0] addr;		// addr of ctrl reg;
	input [31:0] ctrl;		// ctrl reg;
	reg [31:0] r;			// read data;
	begin
		// write aes ctrl reg;
		// read back to flush write and to check start;
		// one block takes 32 clocks, enough time to read ctrl reg;
		// none of the bits should be modified;

		$display("%t: %M: (0x%x)=0x%h %b", $time, addr, ctrl, ctrl);
		xswrite(addr, ctrl);
		xsread(addr, r);
		if(r !== ctrl) begin
			$display("ERROR: %t: %M: (0x%h)=0x%h/%b exp 0x%h/%b",
				$time, addr, r, r, ctrl, ctrl);
		end
	end
endtask

// aesx done checks;
// must have finished with correct intr flag;
// clear pending intr, and kill runaway dma;

task gi_aesx_check;
	input [31:0] addr;		// address of ctrl reg;
	input intr;				// expected an intr;
	input [4:0] ibit;		// BI_INTR bit to check;
	reg [31:0] r;			// read data;
	begin
		xsread(addr, r);
		if(r[31] !== 1'b0)
			$display("ERROR: %t: %M: timeout, (0x%h)=%b", $time, addr, r);
		xsread(`BI_INTR, r);
		if(r[ibit] !== intr)
			$display("ERROR: %t: %M: (0x%h) intr=%b exp %b", $time, addr, r[30], r[ibit]);
		xswrite(addr, 32'd0);
		xsread(addr, r);
		if(r[31:30] !== 2'b00)
			$display("ERROR: %t: %M: not idle, (0x%h)=%b", $time, addr, r);
	end
endtask

// start aes operation;
// wait for completion or time out;

task gi_aesx_op;
	input crypt;			// 0=en 1=decrypt;
	input [29:20] size;		// control register;
	input [16:0] ptr;		// sram data pointer;
	input intr;				// use interrupt;
	input ksel;				// decryption key to use;
	input chain;			// chained request;
	input [15:0] kdel;		// kill delay;
	input [31:0] to;		// timeout in us;
	reg [31:0] addr;		// address of ctrl reg;
	reg [31:0] ctrl;		// aes control register;
	reg [31:0] r;			// read data;
	reg [4:0] ibit;			// index in BI_INTR;
	reg [63:0] t;			// time;
	integer nb;				// total byte count;
	begin
		$display("%t: %M: %scrypt, size=%0d, ptr=0x%h, intr=%b, chain=%b, kdel=%0d",
			$time, crypt? "de" : "en", size, ptr, intr, chain, kdel);
		addr = crypt? `AESD_CTRL : `AESE_CTRL;
		ibit = crypt? 3 : 4;
		ctrl = 'h8000_0000;
		ctrl[30] = intr;
		ctrl[29:20] = size;
		ctrl[16:4] = ptr[16:4];
		ctrl[1] = ksel & crypt;
		ctrl[0] = chain;
		gi_aesx_start(addr, ctrl);
		t = $time;

		// check kill operation;
		// aese must go to idle right away;
		// aesd goes to idle after 16-byte packet finished;
		// interrupt should not have been issued;

		if(kdel > 0) begin
			stall(kdel);
			ctrl[31] = 0;
			xswrite(addr, ctrl);
			xsread(addr, r);
			intr = 0;
		end

		// wait until unit is finished;
		// a 16-byte block takes 24 clocks, if no sram stalls;
		// do not report performance for killed requests;
		// run done checks;

		gi_wait_ctrl(intr, addr, 31, ibit, to);
		if(kdel == 0) begin
			t = $time - t;
			nb = (size + 1) * 16;
			$display("%t: %M: %scrypt perf %0dB/%0dns = %0dMB/s", $time,
				crypt? "de" : "en", nb, t, nb * 1000 / t);
		end
		gi_aesx_check(addr, intr, ibit);
	end
endtask

// get key index for op;

function [1:0] gi_aes_kidx;
	input [1:0] op;			// op code;
	begin
		gi_aes_kidx = 'bx;
		case(op)
			`AESE: gi_aes_kidx = `AESE_KEXP_MC;
			`AESD: gi_aes_kidx = `AESD_KEXP_MC;
			`AESDC: gi_aes_kidx = `AESD_KEXP_DC;
			default: $display("ERROR: %t: %M: op=%b", $time, op);
		endcase
	end
endfunction

// test aes decryption in sram;
// use software interface to regs AESD_*;
// DC and MC are idle;
// test focuses on size and ptr;

task gi_aes_size;
	input [1:0] op;			// op code;
	input chain;			// chain individual tests;
	reg crypt;				// 0=encrypt 1=decrypt;
	reg [1:0] kidx;			// key index;
	reg [31:0] base;		// base of GI;
	integer size;			// size to test;
	reg [127:0] key;		// random key;
	reg [127:0] iv;			// random iv;
	reg [127:0] ivx;		// iv used for comparison;
	reg [16:0] ptr;			// ptr to data;
	reg [16:0] iv_off;		// ptr to iv;
	reg [31:0] iv_addr;		// iv base addr register;
	reg [31:0] pat;			// random seed pattern;
	reg intr;				// use intr;
	integer to;				// timeout (4 blocks / 1usec);
	reg chbit;				// chain bit;
	begin
		// walk all the size bits;
		// randomize almost everything else;
		// try setting iv base;

		$display("%M: op=%b, chain=%b", op, chain);
		kidx = gi_aes_kidx(op);
		crypt = op[1];
		iv_addr = crypt? `AESD_IV : `AESE_IV;
		base = `GI_SRAM_SWP;
		for(size = 0; size < 1024; size = (size << 1) | 1) begin
			$display("%t: %M: size=%0d (%0dB)", $time, size, (size+1)<<4);
			pat = $random;

			// fill sram with random for stress, XXX
			// else with Xs, which we should never touch;

			`sram_fill($random); //XXX

			// generate random key and load it;
			// change key even in chained mode,
			// since chaining is only about the iv;
			// set random iv buffer location,
			// which should not do anything in chained mode;
			// stay one more 16-byte block away from end of
			// sram, as prefetch would walk out of it;

			key = { $random, $random, $random, $random };
			gi_aes_key(key, kidx);
			iv_off = gi_ptr($random, 16*(size+3));
			iv_off[3:2] = 0;
			iv = { $random, $random, $random, $random };
			gi_aesx_ivoff(iv_addr, iv_off);
			gi_aesx_iv(base + iv_off, iv);

			// data starts 16 bytes after iv buffer;
			// randomly pick intr poll or busy poll;
			// the first run in chained mode must have CHAIN=0;
			// for chained mode, ivx returns continuation iv;

			ptr = iv_off + 16;
			intr = pat[0];
			aes_bpat(base | ptr, size + 1, pat);
			to = (size + 1) * 300;		// 24 clks per block, +30%;
			to = (to / 1000) + 1;
			chbit = (size == 0)? 0 : chain;
			gi_aesx_op(crypt, size, ptr, intr, op[0], chbit, 0, to);
			if( !chain | (size == 0))
				ivx = iv;
			aes_bexp(base | ptr, size + 1, pat, crypt, key, ivx);
		end
	end
endtask

// test aes start and kill;
// decryption core must finish work on 16-byte chunk,
// in order to have expanded key rotated full round;
// encryption core has no such limit;
// do useful crypto tests in check op;

task gi_aes_kill;
	input [1:0] op;			// op code;
	reg crypt;				// 0=en, 1=decrypt;
	reg [1:0] kidx;			// key index;
	reg [31:0] base;		// base of GI;
	integer kdel;			// delay from start to kill in clocks;
	integer size;			// random size within limits;
	reg [127:0] key;		// random key;
	reg [127:0] iv;			// random iv;
	reg [16:0] ptr;			// ptr to data;
	reg [16:0] iv_off;		// ptr to iv;
	reg [31:0] iv_addr;		// iv base addr register;
	reg [31:0] pat;			// random seed pattern;
	reg intr;				// use intr;
	integer to;				// timeout (4 blocks / 1usec);
	begin
		// fill sram with random for stress,
		// start an operation, then kill it kdel clocks later;
		// start real operation to check that keys are ok;
		// size stays small for runtime;
		// always stress the bus interface;

		$display("%M: op=%b", op);
		kidx = gi_aes_kidx(op);
		crypt = op[1];
		iv_addr = crypt? `AESD_IV : `AESE_IV;
		base = `GI_SRAM_SWP;
		`sram_fill($random);
		for(kdel = 0; kdel < 200; kdel = kdel + 1) begin
			$display("%t: %M: kill delay=%0d clks", $time, kdel);
			pat = $random;
			size = $random & 'h0f;

			// generate random key and load it;
			// set random iv buffer and iv;
			// data starts 16 bytes after iv buffer;

			key = { $random, $random, $random, $random };
			gi_aes_key(key, kidx);
			iv_off = gi_ptr($random, 16*(size+3));
			iv_off[3:2] = 0;
			iv = { $random, $random, $random, $random };
			gi_aesx_ivoff(iv_addr, iv_off);
			gi_aesx_iv(base + iv_off, iv);
			ptr = iv_off + 16;

			// start aes operation, then kill it;
			// pattern in sram does not matter;
			// 16 bytes take 24 clocks, +4 on initial iv;
			// use size of 10*16 for kdel to walk over;

			to = (10 / 4) + 1;
			gi_aesx_op(crypt, 10-1, ptr & 'hffff, 0, 0, 0, kdel, to);

			// start real operation;
			// use key/iv set up for kileld operation;
			// a corrupted key stream will break data;

			intr = ^pat;
			aes_bpat(base | ptr, size + 1, pat);
			to = (size / 4) + 1;
			gi_aesx_op(crypt, size, ptr, intr, op[0], 0, 0, to);
			aes_bexp(base | ptr, size + 1, pat, crypt, key, iv);
		end
	end
endtask

// test aes with both units busy;

task gi_aes_both;
	input [11:0] runs;		// # of runs;
	input order;			// start order;
	integer n;				// run id;
	reg [31:0] base;		// base address of gi;
	reg [9:0] size;			// size;
	reg [31:0] pat [0:1];	// pattern seed;
	reg [127:0] key [0:1];	// keys;
	reg [16:0] iv_off;		// iv buffer in sram;
	reg [127:0] iv [0:1];	// encryption/decryption iv;
	reg [16:0] ptr [0:1];	// encryption/decryption ptrs;
	reg [31:0] cr [0:1];	// ctrl reg for start;
	reg intr;				// check intr;
	reg [31:0] ctrl;		// ctrl reg;
	integer to;				// timeout;
	begin
		// fill sram with random for stress,
		// size stays small for runtime;
		// always stress the bus interface;

		$display("%M: %0d runs, order=%b", runs, order);
		base = `GI_SRAM_SWP;
		`sram_fill($random);
		for(n = 0; n < runs; n = n + 1) begin
			$display("%t: %M: run %0d", $time, n);

			// make size the same for both to avoid one unit being idle;
			// use different randoms for each unit;

			size = $random & 'h3f;
			pat[0] = $random;
			pat[1] = $random;

			// generate random keys and load them;
			// encryption must be loaded last;

			key[0] = { $random, $random, $random, $random };
			key[1] = { $random, $random, $random, $random };
			gi_aes_key(key[1], `AESD_KEXP_MC);
			gi_aes_key(key[0], `AESE_KEXP_MC);

			// walk iv buffer through upper 32kB;
			// use random ivs for each;

			iv_off = gi_ptr(64*1024 + 16*n, 2*16);
			iv_off[3:2] = 0;
			gi_aesx_ivoff(`AESE_IV, iv_off + 0);
			gi_aesx_ivoff(`AESD_IV, iv_off + 16);
			iv[0] = { $random, $random, $random, $random };
			iv[1] = { $random, $random, $random, $random };
			gi_aesx_iv(base + iv_off + 0, iv[0]);
			gi_aesx_iv(base + iv_off + 16, iv[1]);

			// walk ptr bits;
			// encryption stays in 0..32kB;
			// decryption stays in 32..64kB;

			ptr[0] = base + 'h0000 + 16*n;
			ptr[1] = base + 'h8000 + 16*n;
			intr = ^pat[0];
			aes_bpat(base | ptr[0], size + 1, pat[0]);
			aes_bpat(base | ptr[1], size + 1, pat[1]);

			// start both units, order determines first;
			// wait for last to finish;
			// check after units are done;
			// vary the delay between starts;

			$display("%t: %M: start, ptrs 0x%h,0x%h size=%0d, intr=%b",
				$time, ptr[0], ptr[1], size, intr);
			ctrl = 'h8000_0000;
			ctrl[30] = intr;
			ctrl[29:20] = size;
			ctrl[16:0] = ptr[0];
			cr[0] = ctrl;
			ctrl[16:0] = ptr[1];
			cr[1] = ctrl;
			if(order) begin
				gi_aesx_start(`AESD_CTRL, cr[1]);
				stall(n);
				gi_aesx_start(`AESE_CTRL, cr[0]);
			end else begin
				gi_aesx_start(`AESE_CTRL, cr[0]);
				stall(n);
				gi_aesx_start(`AESD_CTRL, cr[1]);
			end

			// decryption unit has higher priority,
			// should finish first even though started last;

			to = (size / 4) + 2;
			gi_wait_ctrl(intr, `AESE_CTRL, 31, 4, to);
			gi_wait_ctrl(intr, `AESD_CTRL, 31, 3, to);
			gi_aesx_check(`AESE_CTRL, intr, 4);
			gi_aesx_check(`AESD_CTRL, intr, 3);

			// check data;

			aes_bexp(base | ptr[0], size + 1, pat[0], 0, key[0], iv[0]);
			aes_bexp(base | ptr[1], size + 1, pat[1], 1, key[1], iv[1]);
		end
	end
endtask

// test aes stall, without key change;

task gi_aes_stall;
	reg [31:0] base;		// base of GI;
	integer size;			// size to test;
	reg [127:0] key;		// random key;
	reg [127:0] iv;			// random iv;
	reg [127:0] ivx;		// iv used for comparison;
	reg [16:0] ptr;			// ptr to data;
	reg [16:0] iv_off;		// ptr to iv;
	reg [31:0] pat;			// random seed pattern;
	integer to;				// timeout (4 blocks / 1usec);
	reg intr;				// poll=0 use intr=1;
	reg stress;				// stress polling with pio;
	begin
		// walk all the size bits;
		// randomize almost everything else;
		// try setting iv base;

		base = `GI_SRAM_SWP;
		size = 1023;

			$display("%t: %M: size=%0d (%0dB)", $time, size, (size+1)<<4);
			pat = $random;

			// fill sram with random for stress,
			// else with Xs, which we should never touch;

			`sram_fill($random);

			// generate random key and load it;
			// change key even in chained mode,
			// since chaining is only about the iv;
			// set random iv buffer location,
			// which should not do anything in chained mode;
			// stay one more 16-byte block away from end of
			// sram, as prefetch would walk out of it;

			key = { $random, $random, $random, $random };
			gi_aes_key(key, 2);
			iv_off = gi_ptr($random, 16*(size+3));
			iv_off[3:2] = 0;
			iv = { $random, $random, $random, $random };
			gi_aesx_ivoff(`AESE_IV, iv_off);
			gi_aesx_iv(base + iv_off, iv);

			// data starts 16 bytes after iv buffer;
			// the first run in chained mode must have CHAIN=0;
			// for chained mode, ivx returns continuation iv;

			ptr = iv_off + 16;
			intr = pat[0];
			aes_bpat(base | ptr, size + 1, pat);
			to = 300;
			gi_aesx_op(0, size, ptr, intr, 0, 0, 0, to);
			ivx = iv;
			aes_bexp(base | ptr, size + 1, pat, 0, key, ivx);
	end
endtask

// test sha registers;

task gi_sha_regs;
	begin
		$display("%M: SHA_BUF");
		test_reg(`SHA_BUF, `SHA_BUF_OFF, 0);
		//XXX non-secure test;
	end
endtask

// start a sha operation;

task gi_sha_start;
	input [31:0] ctrl;		// ctrl reg;
	reg [31:0] r;			// read data;
	begin
		// write sha ctrl reg;
		// read back to flush write and to check start;
		// one block takes at least 100 clocks, enough time to read ctrl reg;
		// none of the bits should be modified;

		$display("%t: %M: 0x%h %b", $time, ctrl, ctrl);
		xswrite(`SHA_CTRL, ctrl);
		xsread(`SHA_CTRL, r);
		if(r !== ctrl)
			$display("ERROR: %t: %M: 0x%h/%b exp 0x%h/%b", $time, r, r, ctrl, ctrl);
	end
endtask

// sha done checks;
// must have finished with correct intr flag;
// clear pending intr, and kill runaway dma;

task gi_sha_finish;
	input intr;				// expected an intr;
	input [4:0] ibit;		// BI_INTR bit to check;
	reg [31:0] r;			// read data;
	begin
		xsread(`SHA_CTRL, r);
		if(r[31] !== 1'b0)
			$display("ERROR: %t: %M: timeout, %b", $time, r);
		xsread(`BI_INTR, r);
		if(r[ibit] !== intr)
			$display("ERROR: %t: %M: intr=%b exp %b", $time, r[30], r[ibit]);
		xswrite(`SHA_CTRL, 32'd0);
		xsread(`SHA_CTRL, r);
		if(r[31:30] !== 2'b00)
			$display("ERROR: %t: %M: not idle, %b", $time, r);
	end
endtask

// run sha operation;
// wait for completion or time out;
// always write hash result to sram;

task gi_sha_op;
	input [29:20] size;		// control register;
	input [16:0] ptr;		// sram data pointer;
	input intr;				// use interrupt;
	input chain;			// chained request;
	input [15:0] kdel;		// kill delay;
	input [31:0] to;		// timeout in us;
	input stress;			// stress interface;
	reg [31:0] addr;		// address of ctrl reg;
	reg [31:0] ctrl;		// aes control register;
	reg [31:0] r;			// read data;
	reg [4:0] ibit;			// index in BI_INTR;
	reg [63:0] t;			// time;
	integer nb;				// total byte count;
	begin
		$display("%t: %M: size=%0d, ptr=0x%h, intr=%b, chain=%b, stress=%b, kdel=%0d",
			$time, size, ptr, intr, chain, stress, kdel);
		ibit = 6;
		ctrl = 'h8000_0000;
		ctrl[30] = intr;
		ctrl[29:20] = size;
		ctrl[19] = 1;
		ctrl[16:6] = ptr[16:6];
		ctrl[0] = chain;
		gi_sha_start(ctrl);
		t = $time;

		// check kill operation;
		// must go to idle right away;
		// interrupt should not have been issued;

		if(kdel > 0) begin
			stall(kdel);
			ctrl[31] = 0;
			xswrite(`SHA_CTRL, ctrl);
			xsread(`SHA_CTRL, r);
			intr = 0;
		end

		// wait until unit is finished;
		// a 64-byte block takes 100/112 clocks, if no sram stalls;
		// do not report performance for killed requests;
		// run done checks;

		gi_wait_ctrl(intr, `SHA_CTRL, 31, ibit, to);
		if(kdel == 0) begin
			t = $time - t;
			nb = (size + 1) * 64;
			$display("%t: %M: perf %0dB/%0dns = %0dMB/s",
				$time, nb, t, nb * 1000 / t);
		end
		gi_sha_finish(intr, ibit);
	end
endtask

// test sha hashing in sram;
// use software interface to regs SHA_*;
// DC and MC are idle;
// test focuses on size and ptr;

task gi_sha_size;
	input chain;			// chain individual tests;
	reg [31:0] base;		// base of GI;
	integer nb;				// # of bytes;
	integer size;			// size to test;
	reg [16:0] hloc;		// location of hash result;
	reg [16:0] ptr;			// ptr to data;
	reg [31:0] pat;			// random seed pattern;
	reg intr;				// use intr;
	integer to;				// timeout;
	reg stress;				// stress polling with pio;
	reg chbit;				// chain bit;
	reg [159:0] hash;		// expected hash result;
	begin
		// walk all the size bits;
		// randomize almost everything else;
		// try setting iv base;

		$display("%M: chain=%b", chain);
		base = `GI_SRAM_SWP;
		for(size = 0; size < 1024; size = (size << 1) | 1) begin
			nb = (size + 1) * 64;
			$display("%t: %M: size=%0d (%0dB)", $time, size, nb);
			pat = $random;

			// fill sram with random for stress,
			// else with Xs, which we should never touch;

			stress = ^pat;
			`sram_fill(stress? $random : 32'bx);

			// create random ptr for data and hash buffer;
			// 20 byte buffer for hash result is needed;
			// allocate 64 for alignment reasons;
			// only the 5 word of result should be written by unit;
			// force hash result into lower 32 bytes;

			hloc = gi_ptr($random, nb + 64);
			hloc[5] = 0;
			xswrite(`SHA_BUF, hloc);

			// data starts 20 bytes after hash buffer;
			// randomly pick intr poll or busy poll;
			// the first run in chained mode must have CHAIN=0;
			// original data should not have been modified;

			ptr = hloc + 64;
			ptr[5:0] = 0;
			intr = pat[0];
			chbit = (size == 0)? 0 : chain;
			sha_hpat(size + 1, pat, chbit, hash);
			sha_bpat(base + ptr, size + 1, pat);
			to = 1 + (size + 1) * 180 / 100;
			gi_sha_op(size, ptr, intr, chbit, 0, to, stress);
			sha_bexp(base + ptr, size + 1, pat);
			sha_hcmp(base + hloc, hash);
		end
	end
endtask

// test sha start and kill;

task gi_sha_kill;
	reg [31:0] base;		// base of GI;
	integer kdel;			// delay from start to kill in clocks;
	integer size, nb;		// random size within limits;
	reg [16:0] hloc;		// location of hash result;
	reg [16:0] ptr;			// ptr to data;
	reg [31:0] pat;			// random seed pattern;
	reg intr;				// use intr;
	integer to;				// timeout;
	reg stress;				// stress polling with pio;
	reg [159:0] hash;		// expected hash result;
	begin
		// fill sram with random for stress,
		// start an operation, then kill it kdel clocks later;
		// start real operation to check that keys are ok;
		// size stays small for runtime;
		// always stress the bus interface;

		$display("%M:");
		base = `GI_SRAM_SWP;
		`sram_fill($random);
		for(kdel = 0; kdel < 200; kdel = kdel + 1) begin
			$display("%t: %M: kill delay=%0d clks", $time, kdel);
			pat = $random;
			size = $random & 'h0f;
			nb = (size + 1) * 64;

			// fill sram with random for stress,
			// else with Xs, which we should never touch;

			stress = ^pat;
			`sram_fill(stress? $random : 32'bx);

			// create random ptr for data and hash buffer;
			// 20 byte buffer for hash result is needed;
			// allocate 64 for alignment reasons;
			// only the 5 word of result should be written by unit;
			// force hash result into lower 32 bytes;

			hloc = gi_ptr($random, nb + 64);
			hloc[5] = 0;
			xswrite(`SHA_BUF, hloc);

			// data starts 20 bytes after hash buffer;
			// randomly pick intr poll or busy poll;

			ptr = hloc + 64;
			ptr[5:0] = 0;
			intr = pat[0];
			sha_hpat(size + 1, pat, 0, hash);
			sha_bpat(base + ptr, size + 1, pat);

			// start an operation and kill it;

			to = 1 + (size + 1) * 180 / 100;
			gi_sha_op(size, ptr, intr, 0, kdel, to, stress);

			// cannot test chaining due to state changes;
			// original data should not have been modified;

			to = 1 + (size + 1) * 180 / 100;
			gi_sha_op(size, ptr, intr, 0, 0, to, stress);
			sha_bexp(base + ptr, size + 1, pat);
			sha_hcmp(base + hloc, hash);
		end
	end
endtask

// test mc registers;
// only bits without side effects;
// mc and below units must be idle;
// MC_CTRL cannot be tested without starting op;

task gi_mc_regs;
	begin
		$display("%M: MC_ADDR");
		test_reg(`MC_ADDR, `MC_ADDR_MEM, 0);
		$display("%M: MC_BUF");
		test_reg(`MC_BUF, `MC_BUF_OFF, 0);
		//XXX non-secure test;
	end
endtask

// set mc buffer;
// should not succeed in non-secure mode;
// MC_BUF is readable in non-secure mode;

task gi_mc_buf;
	input [16:0] ptr;		// ptr to buffer;
	reg [31:0] sec;			// sec mode reg;
	reg [31:0] exp;			// expected ptr;
	begin
		xsread(`SEC_MODE, sec);
		xsread(`MC_BUF, exp);		// old value;
		xswrite(`MC_BUF, ptr);		// try setting MC_BUF;
		if(sec[0])
			exp = ptr;				// write should have succeeded;
		xsread(`MC_BUF, ptr);
		$display("%t: %M: secure=%b, buf=0x%h", $time, sec[0], ptr);
		if(ptr !== exp)
			$display("ERROR: %t: %M: buf=0x%h exp 0x%h", $time, ptr, exp);
	end
endtask

// start a mc operation;

task gi_mc_start;
	input [31:0] mem;	// mem address;
	input [31:0] ctrl;	// mc ctrl;
	reg [31:0] r;		// read data;
	begin
		// write memory address;
		// write mc ctrl reg;
		// read back to flush write and to check start;
		// XXX nop finishes too fast;

		xswrite(`MC_ADDR, mem);
		xswrite(`MC_CTRL, ctrl);
		xsread(`MC_CTRL, r);
		if(r !== ctrl) begin
			$display("ERROR: %t: %M: MC_CTRL=0x%h/%b exp 0x%h/%b",
				$time, r, r, ctrl, ctrl);
		end
	end
endtask

// kill a mc operation;
// returns MC_CTRL after kill;

task gi_mc_kill;
	output [31:0] ctrl;
	begin
		xswrite(`MC_CTRL, 32'd0);
		xsread(`MC_CTRL, ctrl);			// to flush wbuf;
	end
endtask

// run a mc operation;

reg [127:0] xkey;

task gi_mc_op;
	input [29:28] op;		// operation;
	input [27:26] drop;		// dma buffer drop;
	input chain;			// chain request;
	input [15:0] size;		// size;
	input [31:0] mem;		// memory address;
	input intr;				// want interrupt;
	input kill;				// kill operation;
	input [31:0] to;		// timeout;
	input stress;			// stress during poll;
	reg [31:0] ctrl;		// ctrl register;
	reg [31:0] r;			// read data;
	reg [63:0] t;			// start time;
	integer nb;				// bytes work on;
	begin
		$display("%t: %M: op=%b, drop=%b, size=%0d, chain=%b, mem=0x%h, intr=%b, to=%0dus",
			$time, op, drop, size, chain, mem, intr, to);
		sys_mon_setup(1);
		ctrl = 'h8000_0000;
		ctrl[30] = intr;
		ctrl[29:28] = op;
		ctrl[27:26] = drop;
		ctrl[25] = chain;
		ctrl[15:4] = size;
		gi_mc_start(mem, ctrl);
		t = $time;

		// check kill operation;
		// mc must become unbusy right away; XXX
		// interrupt should not have been issued;

		if(kill) begin
			gi_mc_kill(r);
			ctrl[31] = 0;
			intr = 0;
		end

		// wait until mc is finished;
		// a 16-byte block takes 24 clocks, if no sram stalls;

		xsread(`MC_CTRL, r);
		while((r[31] === 1'b1) && (to > 0)) begin
			stall(100);
	//XXX
			xsread(`MC_CTRL, r);
			to = to - 1;
		end
		t = $time - t;
		sys_mon_setup(0);

		// post checks;
		// do not report performance for killed requests;
		// must have finished with correct intr flag;
		// clear pending intr, and kill still going dma;

		if(r[31] !== 1'b0)
			$display("ERROR: %t: %M: timeout, MC_CTRL=%b", $time, r);
		else if( !kill) begin
			if(op == `SHA)
				nb = (size[15:2] + 1) * 64;
			else
				nb = (size[15:0] + 1) * 16;
			$display("%t: %M: op=%b perf %0dB/%0dns = %0dMB/s", $time,
				op, nb, t, nb * 1000 / t);
		end
		xsread(`BI_INTR, r);
		if(r[5] !== intr)
			$display("ERROR: %t: %M: intr=%b exp %b", $time, r[30], r[5]);
		gi_mc_kill(r);
		if(r[31:30] !== 2'd0)
			$display("ERROR: %t: %M: not cleared, MC_CTRL=%b", $time, r);
	end
endtask

// run mc sha operation;
// randomize almost everything to add stress
// in addition to dedicated sha tests;
// drop tests are done in hmac test;

task gi_mc_sha;
	input [31:0] mem;		// main mem address;
	input [29:20] size;		// size in 64-byte blocks;
	input [31:0] hptr;		// location of hash result;
	input chain;			// chain operation;
	reg [31:0] base;		// base of GI;
	reg [31:0] pat;			// random seed pattern;
	reg intr;				// use intr for polling;
	integer to;				// timeout;
	reg [159:0] hash;		// hash result;
	begin
		// use random patterns;
		// randomly pick intr poll or busy poll;

		$display("%t: %M: mem=0x%h, size=%0d (%0d), chain=%b",
			$time, mem, size, (size+1)*64, chain);
		base = `GI_SRAM_SWP;
		pat = $random;
		intr = pat[0];

		// full 64-byte blocks move;
		// the first run in chained mode must have CHAIN=0;
		// for chained mode, shx returns continuation hash;
		// allow 1us for initial dma burst;
		// give each block 1.5 times the sha op time;

		sha_hpat(size + 1, pat, chain, hash);
		sha_bpat(mem, size + 1, pat);
		to = 1 + (size + 1) * 180 / 100;
		gi_mc_op(`SHA, 2'b00, chain, size << 2, mem, intr, 0, to, 1);
		sha_bexp(mem, size + 1, pat);
		sha_hcmp(base + hptr, hash);
	end
endtask

// run mc aes operation;
// randomize almost everything to add stress
// in addition to dedicated aes tests;

task gi_mc_aes;
	input [1:0] op;			// operation code;
	input [31:0] mem;		// main mem address;
	input [15:4] size;		// size in 16-byte blocks;
	input [16:0] iv_off;	// offset of iv buffer;
	input chain;			// chain operation;
	inout [127:0] ivx;		// iv for chaining;
	reg crypt;				// 0=en- 1=decrypt;
	reg [1:0] kidx;			// key index;
	reg [31:0] base;		// base of GI;
	reg [127:0] key;		// random key;
	reg [127:0] iv;			// random iv;
	reg [31:0] iv_addr;		// iv base addr register;
	reg [31:0] pat;			// random seed pattern;
	integer to;				// timeout;
	reg intr;				// use intr for polling;
	integer bsize;			// size of 64-byte blocks;
	begin
		$display("%t: %M: op=%b, mem=0x%h, size=%0d (%0d), chain=%b",
			$time, op, mem, size, (size+1)*16, chain);
		kidx = gi_aes_kidx(op);
		crypt = op[1];
		iv_addr = crypt? `AESD_IV : `AESE_IV;
		base = `GI_SRAM_SWP;

		// generate random key and load it;
		// change key even in chained mode,
		// since chaining is only about the iv;

		pat = $random;
		key = { $random, $random, $random, $random };
	//XXX
	xkey = key;
		gi_aes_key(key, kidx);
		iv_off[3:0] = 0;
		gi_aesx_ivoff(iv_addr, iv_off);
		iv = { $random, $random, $random, $random };
		gi_aesx_iv(base + iv_off, iv);

		// full 64-byte blocks move, even if less is used;
		// randomly pick intr poll or busy poll;
		// the first run in chained mode must have CHAIN=0;
		// for chained mode, ivx returns continuation iv;
		// allow 3us per 64-byte block, ~20MB/sec;

		intr = pat[0];
		bsize = (size | 3) + 1;
		aes_bpat(mem, bsize, pat);
		to = 3 * bsize / 4;
		gi_mc_op(op, 2'b00, chain, size, mem, intr, 0, to, 1);
		if( !chain)
			ivx = iv;
		aes_bexp(mem, size + 1, pat, crypt, key, ivx);
	end
endtask

// test mc operation;
// test focuses on size and ptr;
// can be called from secure and non-secure mode,
// randomizing the MC_BUF location only in secure mode,
// because writes to MC_BUF are limited to secure mode;

task gi_mc_op_size;
	input [1:0] op;			// operation, same as MC_CTRL bits;
	input chain;			// chain operation;
	integer size;			// size in 16-byte blocks;
	reg [16:0] ptr;			// sram pci buffer, randomize;
	reg [31:0] mem;			// memory address, randomize;
	reg chbit;				// chain bit;
	reg [127:0] ivx;		// iv for chaining;
	begin
		// fill sram with randoms for stress;

		$display("%M: op=%b, chain=%b, sizes adding 1", op, chain);
		`sram_fill($random);
		sys_dma_setup(1, 0);
		for(size = 0; size < 4096; size = (size << 1) | 1) begin
			$display("%t: %M: size=%0d", $time, size);

			// need 2 64-byte pci buffers;
			// also need 16 byte iv for aes;
			// locate iv buffer behind pci buffers;
			// pick random sram locations;

			ptr = gi_ptr($random, 256);
			ptr[6:0] = 0;
			gi_mc_buf(ptr);

			// get random memory address;
			// for chained test, chain all sizes together;
			// call per unit handler;
			// for sha make sizes multiples of 64;

			mem = $random & `MEM_MASK;
			mem[5:0] = 0;
			chbit = (size == 0)? 0 : chain;
			case(op)
				2'b00: gi_mc_sha(mem, size >> 2, ptr + 64, chbit);
				2'b01, 2'b10, 2'b11: gi_mc_aes(op, mem, size, ptr + 128, chbit, ivx);
				default: $display("ERROR: %t: %M: op=%b", $time, op);
			endcase
		end
		sys_dma_setup(0, 0);
	end
endtask

// test dc registers;

task gi_dc_regs;
	begin
		$display("%M: DC_BUF");
		test_reg(`DC_BUF, `DC_BUF_OFF, 0);
		$display("%M: DC_HADDR");
		test_reg(`DC_HADDR, `DC_HADDR_MEM, 0);
		$display("%M: DC_HIDX");
		test_reg(`DC_HIDX, `DC_HIDX_H0 | `DC_HIDX_H1, 0);
		$display("%M: DC_DIOFF");
		test_reg(`DC_DIOFF, `DC_DI_LAST | `DC_DI_FIRST, 0);
		$display("%M: DC_IV");
		test_reg(`DC_IV, `DC_IV_OFF, 0);
		$display("%M: DC_H0");
		test_reg(`DC_H0, `DC_H0_OFF, 0);
		$display("%M: DC_H1");
		test_reg(`DC_H1, `DC_H1_OFF, 0);
		//XXX non-secure test;
	end
endtask

// start a dc operation;

task gi_dc_start;
	input [31:0] ctrl;	// mc ctrl;
	reg [31:0] r;		// read data;
	begin
		// write memory address;
		// write dc ctrl reg;
		// read back to flush write and to check start;

		xswrite(`DC_CTRL, ctrl);
		xsread(`DC_CTRL, r);
		if(r !== ctrl) begin
			$display("ERROR: %t: %M: DC_CTRL=0x%h/%b exp 0x%h/%b",
				$time, r, r, ctrl, ctrl);
		end
	end
endtask

// kill a dc operation;
// returns DC_CTRL after kill;

task gi_dc_kill;
	output [31:0] ctrl;
	begin
		xswrite(`DC_CTRL, 32'd0);
		xsread(`DC_CTRL, ctrl);			// to flush wbuf;
	end
endtask

// run a dc operation;
// other DC register must have been programmed already;

task gi_dc_op;
	input [29:28] op;		// operation;
	input [16:0] sg_off;	// sg list offset;
	input intr;				// want interrupt;
	input kill;				// kill operation;
	input [31:0] to;		// timeout;
	input stress;			// stress during poll;
	reg [31:0] ctrl;		// ctrl register;
	reg [31:0] r;			// read data;
	reg [63:0] t;			// start time;
	begin
		$display("%t: %M: op=%b, sg=%h, intr=%b, to=%0dus",
			$time, op, sg_off, intr, to);
		sys_mon_setup(1);
		ctrl = 'h8000_0000;
		ctrl[30] = intr;
		ctrl[29:28] = op;
		ctrl[16:7] = sg_off[16:7];
		gi_dc_start(ctrl);
		t = $time;

		// check kill operation;
		// dc must become unbusy right away; XXX
		// interrupt should not have been issued;

		if(kill) begin
			gi_dc_kill(r);
			ctrl[31] = 0; //XXX
			intr = 0;
		end

		// wait until dc is finished;
		// timeouts vary depending on operation;

		xsread(`DC_CTRL, r);
		while((r[31] === 1'b1) && (to > 0)) begin
			stall(100);
			xsread(`DC_CTRL, r);
			to = to - 1;
		end
		t = $time - t;
		sys_mon_setup(0);

		// post checks;
		// do not report performance for killed requests;
		// must have finished with correct intr flag;
		// clear pending intr, and kill still going dma;

		if(r[31] !== 1'b0)
			$display("ERROR: %t: %M: timeout, DC_CTRL=%b", $time, r);
		else if( !kill)
			$display("%t: %M: op=%b in %0dus", $time, op, t / 1000);

		xsread(`BI_INTR, r);
		if(r[7] !== intr)
			$display("ERROR: %t: %M: intr=%b exp %b", $time, r[30], r[7]);
		gi_dc_kill(r);
		if(r[31:30] !== 2'd0)
			$display("ERROR: %t: %M: not cleared, DC_CTRL=%b", $time, r);
	end
endtask

// test dc, di write to memory;
// uses sg list for write gatthering;

task gi_dc_in;
	input [15:0] runs;		// # of runs;
	reg [31:0] base;		// base of GI;
	reg [31:0] mem;			// random memory address;
	begin
/*XXX
		$display("%M: %0d runs", runs);
		base = `GI_SRAM_SWP;
		for(n = 0; n < runs; n = n + 1) begin
			// fix dma buffers in upper 32kbytes;
			// pick random dma buffer within that range;

			buf_off = 'h1_0000;
			buf_off[14:8] = $random;
			xswrite(`DC_BUF, buf_off);

			// h0/h1 are not needed;
			// pick random memory address and patterns;
			// pick random poll/intr completion;

			{ intr, h0idx } = $random;

			mem = $random & `MEM_MASK;
			mem[11:0] = 0;
			xswrite(`DC_HADDR, mem);
			pat = $random;
			$display("%t: %M: run %0d, mem=%h buf=%h intr=%b pat=%h",
				$time, n, mem, buf_off, intr, pat);
*/
	end
endtask

// test dc, h0-only handling;
// always operates on 4kB h0 block;

task gi_dc_h0;
	input [15:0] runs;		// # of runs;
	reg [31:0] base;		// base of GI;
	integer size, n, h;
	reg [31:0] mem;			// random memory address;
	reg [31:0] mend;		// end of h0 page;
	reg [16:0] buf_off;		// pci buffer in sram;
	reg [16:0] h0;			// base of h0 cache;
	reg [16:0] h1;			// base of h1 list;
	reg [2:0] h0idx;		// h0 cache index;
	reg [16:0] h1idx;		// h1 index;
	reg intr;				// use 1=intr 0=polling;
	reg [31:0] pat;			// random pattern seed;
	reg [159:0] hash;		// hash over h0 block;
	reg [31:0] m, s;		// memory/sram words;
	integer to;				// timeout;
	begin
		$display("%M: %0d runs", runs);
		base = `GI_SRAM_SWP;
		size = 4096/64;
		for(n = 0; n < runs; n = n + 1) begin
			// fix dma buffers in upper 32kbytes;
			// pick random dma buffer within that range;

			buf_off = 'h1_0000;
			buf_off[14:8] = $random;
			xswrite(`DC_BUF, buf_off);

			// make upper 640 bytes of lower 64kB the H0 cache;
			// pick random base of H1 within 32..48kB range;

			h0 = 64*1024 - 640;
			xswrite(`DC_H0, h0);
			h1 = 32*1024;
			h1[13:4] = $random;
			xswrite(`DC_H1, h1);

			// pick random poll/intr completion;
			// pick random h0 cache index for extraction;
			// pick random h1 index within h1 region, 32k..64k-640;
			// don't bother aligning h1 to hash boundary;

			{ intr, h0idx } = $random;
			h1idx = h1;
			h1idx[13:4] = $random;
			while((h1idx < h1) | (h1idx >= (h0 - 20)))
				h1idx[13:4] = $random;
			xswrite(`DC_HIDX, { 9'd0, h0idx, 3'd0, h1idx });

			// pick random memory address and patterns;

			mem = $random & `MEM_MASK;
			mem[11:0] = 0;
			xswrite(`DC_HADDR, mem);
			pat = $random;
			$display("%t: %M: run %0d, mem=%h buf=%h intr=%b pat=%h",
				$time, n, mem, buf_off, intr, pat);
			$display("%t: %M: run %0d, h0=%h h1=%h h0idx=%h h1idx=%h",
				$time, n, h0, h1, h0idx, h1idx);

			// compute hash over pattern;
			// write hash to h1 index;

			sha_hpat(size, pat, 0, hash);
			xswrite(base + h1idx + 0, hash[159:128]);
			xswrite(base + h1idx + 4, hash[127:96]);
			xswrite(base + h1idx + 8, hash[95:64]);
			xswrite(base + h1idx + 12, hash[63:32]);
			xswrite(base + h1idx + 16, hash[31:0]);

			// seed memory with random content;
			// start operation;
			// check that memory is not modified;

			sha_bpat(mem, size, pat);
			to = 1 + (size + 1) * 180 / 100;
			gi_dc_op(`DCOP_H0, 0, intr, 0, to, 1);
			sha_bexp(mem, size, pat);

			// check that h0 cache contains extracted data;

			mend = mem + 4096;
			mem = mem + (h0idx * 640);
			for(h = 0; (h < 640) & (mem < mend); h = h + 4) begin
				xsread(base + h0, s);
				xsread(mem, m);
				if(s !== m)
					$display("ERROR: %t: %M: h0[%0d]=%h exp %h", $time, h, s, m);
				h0 = h0 + 4;
				mem = mem + 4;
			end

			// check below/above h0 region;
			// XXX

			// check hash comparison result;
			// XXX
		end
	end
endtask

// thread operations;
// wait for completion if not parallel run;

task thread_stop;
	input [7:0] tid;
	begin
		thread_run[tid] = 0;
		@(posedge `xclk);
	end
endtask

task thread_start;
	input [7:0] tid;
	input loop;
	begin
		thread_run[tid] = 1;
		@(posedge `xclk);
		if(!loop)
			thread_run[tid] = 0;
	end
endtask

task thread_wait;
	input [7:0] tid;
	begin
		if(thread_run[tid])
			$display("%t: %M: thread=%0d running forever", $time,  tid);
		wait(test_gi_par | ~thread_busy[tid]);
	end
endtask

// report changes in thread status;

always @(thread_run or thread_busy)
begin : thread_state
	$display("%t: %M: run=%b busy=%b", $time, thread_run, thread_busy);
end

// stress task;
// read random sram locations;
// write random rom locations;
// space requests randomly;
// fix block requests at 16 bytes;

task gi_stress;
	reg rw;				// random 0=read 1=write;
	reg blk;			// random 0=single 1=block;
	reg [1:0] spc;		// random spacing;
	reg [31:0] addr;	// random address;
	reg [31:0] r;		// read data;
	reg [0:`BSIZE_BITS-1] b;		// read data;
	begin
		{ rw, blk, spc, addr[15:2] } = $random;
		addr[31:16] = rw? `GI_ROM_SWP >> 16 : `GI_SRAM_SWP >> 16;
		if(blk)
			addr[3:2] = 2'd0;
		addr[1:0] = 2'd0;
		stall8(spc);
		case({rw,blk})
			2'b00: xsread(addr, r);
			2'b01: xbread(addr, `BSIZE_MAX, b);
			2'b10: xswrite(addr, $random);
			2'b11: xbwrite(addr, `BSIZE_MAX, {4{$random}});
		endcase
	end
endtask

// thread: stress;
// do not start until rom/sram are swapped;

always @(posedge thread_run[`THREAD_STRESS])
begin : stress_thread
	thread_busy[`THREAD_STRESS] = 1;
	while(thread_run[`THREAD_STRESS])
		gi_stress;
	thread_busy[`THREAD_STRESS] = 0;
end

// thread: rom check;
// do not start until rom/sram are swapped;

always @(posedge thread_run[`THREAD_ROM])
begin : rom_thread
	thread_busy[`THREAD_ROM] = 1;
	while(thread_run[`THREAD_ROM])
		gi_check_rom(`GI_ROM_SWP);
	thread_busy[`THREAD_ROM] = 0;
end

// sequence of bi tests;

task gi_test_bi;
	begin
		gi_bi_regs;					// test bi registers;
		//XXX test_ena
		//XXX alt_boot
		gi_test_bi_dma(`GI_SRAM_SWP);
		gi_bi_dma_wrseq(32);
	end
endtask

// thread: bi tests;
// do not start until rom/sram are swapped;

always @(posedge thread_run[`THREAD_BI])
begin : bi_thread
	thread_busy[`THREAD_BI] = 1;
	while(thread_run[`THREAD_BI])
		gi_test_bi;
	thread_busy[`THREAD_BI] = 0;
end

// sequence of di tests;
// must arbitrate with dc;

task gi_test_di;
	begin
		//XXX reg tests;
		gi_di_simple;				// simple di transactions;
		gi_di_rsp_error;			// check error protocol;
		gi_di_break;				// check break protocol;
	end
endtask

// thread: di tests;

always @(posedge thread_run[`THREAD_DI])
begin : di_thread
	thread_busy[`THREAD_DI] = 1;
	while(thread_run[`THREAD_DI])
		gi_test_di;
	thread_busy[`THREAD_DI] = 0;
end

// sequence of ais tests;

task gi_test_ais;
	begin
		gi_ais_regs;				// test ais registers;
		gi_ais_kill;				// start and kill;
		gi_ais_stream(1, 0, 0);		// one buffer, normal speed, no intr;
		gi_ais_stream(2, 1, 0);		// two buffers, fast, no intr;
		gi_ais_stream(3, 1, 1);		// three buffers, fast, intr;
	end
endtask

// thread: ais tests;
// independent of other units;

always @(posedge thread_run[`THREAD_AIS])
begin : ais_thread
	thread_busy[`THREAD_AIS] = 1;
	while(thread_run[`THREAD_AIS])
		gi_test_ais;
	thread_busy[`THREAD_AIS] = 0;
end

// sequence of aese tests;
// must arbitrate with MC;

task gi_test_aese;
	begin
		// secure mode tests;
		gi_aese_regs; //XXX
		gi_aes_size(`AESE, `NCHAIN);		// encryption, not chained;
		gi_aes_size(`AESE, `CHAIN);			// encryption, chained;
		gi_aes_kill(`AESE);					// encrypt, start and kill;
	end
endtask

// thread: aese tests;

always @(posedge thread_run[`THREAD_AESE])
begin : aese_thread
	thread_busy[`THREAD_AESE] = 1;
	while(thread_run[`THREAD_AESE])
		gi_test_aese;
	thread_busy[`THREAD_AESE] = 0;
end

// sequence of aesd tests;
// must arbitrate with MC;

task gi_test_aesd;
	begin
		// secure mode tests;
		gi_aesd_regs;						// test aes registers;
		gi_aes_kexp;
		gi_aes_size(`AESD, `NCHAIN);		// decryption, not chained;
		gi_aes_size(`AESD, `CHAIN);			// decryption, chained;
		gi_aes_kill(`AESD);					// decrypt, start and kill;
		gi_aes_size(`AESDC, `NCHAIN);		// decryption, not chained;
		gi_aes_size(`AESDC, `CHAIN);		// decryption, chained;
		gi_aes_kill(`AESDC);				// decrypt, start and kill;
		gi_aes_both(200, 0);				// both units, encryption first;
		gi_aes_both(200, 1);				// both units, decryption first;
		//XXX decrypt key change while enc;
		//XXX performance tests;
		// non-secure mode tests;
		//XXX start from non-secure;
	end
endtask

// thread: aesd tests;

always @(posedge thread_run[`THREAD_AESD])
begin : aesd_thread
	thread_busy[`THREAD_AESD] = 1;
	while(thread_run[`THREAD_AESD])
		gi_test_aesd;
	thread_busy[`THREAD_AESD] = 0;
end

// sequence of sha tests;
// must arbitrate with MC;

task gi_test_sha;
	begin
		// secure mode tests;
		gi_sha_regs;						// test sha registers;
		gi_sha_size(`NCHAIN);				// sha ops, not chained;
		gi_sha_size(`CHAIN);				// sha ops, chained;
		gi_sha_kill;						// start and kill;
		//XXX non-secure mode tests;
	end
endtask

// thread: sha tests;

always @(posedge thread_run[`THREAD_SHA])
begin : sha_thread
	thread_busy[`THREAD_SHA] = 1;
	while(thread_run[`THREAD_SHA])
		gi_test_sha;
	thread_busy[`THREAD_SHA] = 0;
end

// sequence of mc tests;
// must arbitrate with aese, aesd, sha;

task gi_test_mc;
	begin
		// secure mode tests;
		gi_mc_regs;							// test mc registers;
		gi_mc_op_size(`SHA, `NCHAIN);		// test hashing, not chained;
		gi_mc_op_size(`AESE, `NCHAIN);		// test encryption, not chained;
		gi_mc_op_size(`AESD, `NCHAIN);		// test decryption, not chained;
		gi_mc_op_size(`AESDC, `NCHAIN);		// test decryption, not chained;
		gi_mc_op_size(`SHA, `CHAIN);		// test hashing, chained;
		gi_mc_op_size(`AESE, `CHAIN);		// test encryption, chained;
		gi_mc_op_size(`AESD, `CHAIN);		// test decryption, chained;
		gi_mc_op_size(`AESDC, `CHAIN);		// test decryption, chained;
		//XXX decrypt key change while enc;
		//XXX drop tests;
		//XX non-secure tests;
	end
endtask

// thread: mc tests;

always @(posedge thread_run[`THREAD_MC])
begin : mc_thread
	thread_busy[`THREAD_MC] = 1;
	while(thread_run[`THREAD_MC])
		gi_test_mc;
	thread_busy[`THREAD_MC] = 0;
end

// sequence of dc tests;
// must arbitrate with di;

task gi_test_dc;
	begin
		// secure mode tests;
		gi_dc_regs;							// test dc registers;
		gi_dc_h0(10);						// hash extraction tests;

		//XXX non-secure mode tests;
		//XXX reg tests;
		//XXX aesd and dc at same time;
		//XX aesd/aese/dc at same time;
	end
endtask

// thread: mc tests;

always @(posedge thread_run[`THREAD_DC])
begin : dc_thread
	thread_busy[`THREAD_DC] = 1;
	while(thread_run[`THREAD_DC])
		gi_test_dc;
	thread_busy[`THREAD_DC] = 0;
end

// all gi tests;

task test_gi;
	input all;				// run all tests;
	reg test_gi_loop;		// loop through tests forever;
	reg test_gi_mem;		// run memory tests, rom, sram, v;
	reg test_gi_bi;			// run bi tests;
	reg test_gi_di;			// run di tests;
	reg test_gi_ais;		// run ais tests;
	reg test_gi_aese;		// run aese tests;
	reg test_gi_aesd;		// run aesd tests;
	reg test_gi_sha;		// run sha tests;
	reg test_gi_mc;			// run mc tests;
	reg test_gi_dc;			// run dc tests;
	begin
		$display("%M");

		// global flags;

		test_gi_stress = $test$plusargs("test_gi_stress");
		test_gi_par = $test$plusargs("test_gi_par");
		test_gi_loop = $test$plusargs("test_gi_loop");

		// per-unit flags;

		test_gi_mem = all | $test$plusargs("test_gi_mem");
		test_gi_bi = all | $test$plusargs("test_gi_bi");
		test_gi_di = all | $test$plusargs("test_gi_di");
		test_gi_ais = all | $test$plusargs("test_gi_ais");
		test_gi_aese = all | $test$plusargs("test_gi_aese");
		test_gi_aesd = all | $test$plusargs("test_gi_aesd");
		test_gi_sha = all | $test$plusargs("test_gi_sha");
		test_gi_mc = all | $test$plusargs("test_gi_mc");
		test_gi_dc = all | $test$plusargs("test_gi_dc");

		// always test defaults after reset;
		// they don't need much run time;
		// run memory tests selectively;

		gi_after_reset(test_gi_mem);

		// init sram with randoms when par/stress are enabled;
		// Xs would flag errors in the pci simulator;

		if(test_gi_par | test_gi_stress)
			`sram_fill($random);

		// run debug test;

		if($test$plusargs("test_gi_xxx")) begin
			gi_mc_op_size(`AESE, `NCHAIN);
			$finish;
		end


		// run tests sequentially or parallel;
		// in sequential mode:
		//		units have all system ressources;
		//		randoms should cover all sizes, spaces, ...;
		//		start thread, then wait for its completion;
		//		loop means loop through sequence;
		// in parallel mode;
		//		system ressource are pre-allocated;
		//		randoms limited to allocated sizes, spaces, ...;
		//		all threads started, then wait for all to complete;
		//		loop means that threads will loop;
	//XXX loop mode;

		// bi tests;

		if(test_gi_bi) begin
			thread_start(`THREAD_BI, test_gi_loop);
			thread_wait(`THREAD_BI);
		end

		// di tests;

		if(test_gi_di) begin
			thread_start(`THREAD_DI, test_gi_loop);
			thread_wait(`THREAD_DI);
		end

		// ais tests;

		if(test_gi_ais) begin
			thread_start(`THREAD_AIS, test_gi_loop);
			thread_wait(`THREAD_AIS);
		end

		// aese tests;

		if(test_gi_aese) begin
			thread_start(`THREAD_AESE, test_gi_loop);
			thread_wait(`THREAD_AESE);
		end

		// aesd tests;

		if(test_gi_aesd) begin
			thread_start(`THREAD_AESD, test_gi_loop);
			thread_wait(`THREAD_AESD);
		end

		// sha1 tests;

		if(test_gi_sha) begin
			thread_start(`THREAD_SHA, test_gi_loop);
			thread_wait(`THREAD_SHA);
		end

		// mc tests;

		if(test_gi_mc) begin
			thread_start(`THREAD_MC, test_gi_loop);
			thread_wait(`THREAD_MC);
		end

		// dc tests;

		if(test_gi_dc) begin
			thread_start(`THREAD_DC, test_gi_loop);
			thread_wait(`THREAD_DC);
		end

		// run loop tests for specified amount of time,
		// or run them forever;

		wait(thread_busy == 'd0);
	end
endtask

