// test_lib.v Frank Berndt
// test library functions;
// lib code is needed per thread, as tasks are not multi-thread safe;

// stall for n clocks;
// granularity is 10ns, plus one clk/pci_clk for edge sync;

task stall;
	input [31:0] nclks;
	begin
		if(nclks > 1024*1024)
			$display("ERROR: %t: %M: nclks=%0d seems excessive", $time, nclks);
		repeat(nclks-1) @(posedge clk);
		@(posedge `xclk);
	end
endtask

// stall by a number of system clks;
// either gi or pci clock;
// limit to 7 clocks;

task stall8;
	input [2:0] nclks;
	begin
		repeat(nclks-1) @(posedge clk);
		@(posedge `xclk);
	end
endtask

task xswrite;
	input [31:0] addr;
	input [31:0] data;
	begin
		`swrite(addr, data);
	end
endtask

task xsread;
	input [31:0] addr;
	output [31:0] data;
	begin
		`sread(addr, data);
	end
endtask

task xbwrite;
	input [31:0] addr;
	input [3:0] bsize;
	input [127:0] data;
	begin
		`blk[0] = data[127:96];
		`blk[1] = data[95:64];
		`blk[2] = data[63:32];
		`blk[3] = data[31:0];
		`bwrite(addr, bsize);
	end
endtask

task xbread;
	input [31:0] addr;
	input [3:0] bsize;
	output [127:0] data;
	begin
		`bread(addr, bsize);
		data[127:96] = `blk[0];
		data[95:64] = `blk[1];
		data[63:32] = `blk[2];
		data[31:0] = `blk[3];
	end
endtask

// align address to block boundary;
// check address alignement;

task baddr;
	inout [31:0] addr;
	input [7:0] bsize;
	input check;
	reg [31:0] mask;
	begin
		mask = (bsize - 1) << 2;
		mask[1:0] = 2'b11;
		if(check && (addr & mask))
			$display("ERROR: %t: %M: addr=0x%h", $time, addr);
		addr = addr & ~mask;
	end
endtask

// get a random sub-block order address;
// sequential for PCI, else sub-block;

task sbo_addr;
	inout [31:0] addr;
	input [3:0] bsize;
	reg [4:0] mask;
	begin
		mask = (bsize - 1) << 2;
		addr = addr & ~mask;
`ifdef	GI_PCI
`else	// GI_PCI
		addr = addr | ($random & mask);
`endif	// GI_PCI
	end
endtask

// get next word pattern;
// uses simple crc16 method;

function [31:0] next_pat;
	input [31:0] pat;
	reg bit;
	begin
		bit = pat[15] ^ pat[0] ^ 1;
		next_pat = { bit, pat[31:1] };
	end
endfunction

// check expected single read data;

task sexp;
	input [31:0] exp;
	input [31:0] got;
	input [31:0] mask;
	begin
		if(mask === 32'h0)
			$display("ERROR: %t: %M: mask=%b", $time, mask);
		if((exp & mask) !== (got & mask)) begin
			$display("ERROR: %t: %M: mask 0x%h mismatch 0x%h/%b exp 0x%h/%b",
				$time, mask, got, got, exp, exp);
		end
	end
endtask

// fill block with pattern;

task bpat;
	inout [31:0] pat;		// start pattern;
	input [3:0] bsize;		// size in words;
	output [0:`BSIZE_BITS-1] blk;
	integer n;
	reg [31:0] word;
	begin
		for(n = 0; n < `BSIZE_MAX; n = n + 1) begin
			word = (n < bsize)? pat : 32'bx;
			blk = { blk[32:`BSIZE_BITS-1], word };
			pat = next_pat(pat);
		end
	end
endtask

// check pattern in block;
// consider sub-block order;

task bexp;
	input [31:0] addr;		// address;
	input [3:0] bsize;		// size in words;
	input [0:`BSIZE_BITS-1] blk;
	inout [31:0] pat;		// start pattern;
	input [4:2] sbo;		// sub-block order;
	reg [31:0] xblk [0:`BSIZE_MAX-1];
	integer n;
	reg [3:0] off;			// block offset;
	reg [31:0] got;
	begin
		xblk[0] = blk[0:31];
		xblk[1] = blk[32:63];
		xblk[2] = blk[64:95];
		xblk[3] = blk[96:127];
		baddr(addr, bsize, 0);
		sbo = sbo & (bsize - 1);
		for(n = 0; n < bsize; n = n + 1) begin
			off = n ^ sbo;
			got = xblk[off];
			if(got !== pat) begin
				$display("ERROR: %t: %M: addr 0x%h blk[%0d] mismatch 0x%h/%b exp 0x%h/%b",
					$time, addr + (off << 2), off, got, got, pat, pat);
			end
			pat = next_pat(pat);
		end
	end
endtask

// test a register;
// regs are assumed to be up to 32 bits in width;
// can only test read/write bits that do not have side effect;

task test_reg;
	input [31:0] addr;		// address;
	input [31:0] mask;		// bit mask;
	input [31:0] set;		// bits that must be 1;
	integer b;
	reg [31:0] r;			// read data;
	begin
		$display("%t: %M: addr 0x%h mask 0x%h", $time, addr, mask);
		for(b = 1; b != 0; b = b << 1) begin
			if(b & mask) begin
				xswrite(addr, b | set);
				xsread(addr, r);
				sexp(b, r, mask);
			end
		end
	end
endtask

// check memory with single accesses;
// writes are done first, followed by read checks;
// sparse check that exercises all address bits;
// does not use memory backdoor for running on cpu;

task check_mem_sgl;
	input [31:0] space;		// address space;
	input [31:0] size;		// size of space;
	input reqspc;			// random request spacing;
	input wa;				// address pattern;
	reg [31:0] addr;		// address;
	reg [31:0] past;		// end of space;
	integer n, i, woff;
	reg [31:0] r;			// rom read data;
	reg [31:0] pat [31:0];	// data patterns;
	begin
		// do all writes first;

		$display("%t: %M: write, reqspc=%b, wa=%b", $time, reqspc, wa);
		past = space + size;
		woff = wa << 2;
		addr = space;
		i = 0;
		for(n = 4; addr < past; n = woff | (n << 1)) begin
			if(reqspc)
				stall8($random);
			pat[i] = $random;
			xswrite(addr, pat[i]);
			addr = space + n;
			i = i + 1;
		end

		// then check with reads;

		$display("%t: %M: read, reqspc=%b, wa=%b", $time, reqspc, wa);
		addr = space;
		i = 0;
		for(n = 4; addr < past; n = woff | (n << 1)) begin
			if(reqspc)
				stall8($random);
			xsread(addr, r);
			sexp(pat[i], r, 32'hffff_ffff);
			addr = space + n;
			i = i + 1;
		end
	end
endtask

// check memory with block accesses;
// writes are done first, followed by read checks;
// sparse check that exercises all address bits;
// uses random sub-block order;
// does not use memory backdoor for running on cpu;

task check_mem_blk;
	input [31:0] space;		// address space;
	input [31:0] size;		// size of space;
	input [3:0] bsize;		// block size;
	input reqspc;			// random request spacing;
	reg [31:0] addr;		// address;
	reg [31:0] past;		// end of space;
	reg [31:0] pat [31:0];	// data patterns;
	reg [31:0] npat;		// new pattern;
	integer n, i;
	reg [0:`BSIZE_BITS-1] blk;
	begin
		// do all writes first, in sequential order;

		$display("%t: %M: write, bsize=%0d, reqspc=%b", $time, bsize, reqspc);
		past = space + size;
		addr = space;
		i = 0;
		for(n = 32; addr < past; n = n | (n << 1)) begin
			if(reqspc)
				stall8($random);
			baddr(addr, bsize, 0);
			npat = $random;
			pat[i] = npat;
			bpat(npat, bsize, blk);
			xbwrite(addr, bsize, blk);
			addr = space + n;
			i = i + 1;
		end

		// read and check, in random sub-block order;

		$display("%t: %M: read, bsize=%0d, reqspc=%b", $time, bsize, reqspc);
		addr = space;
		i = 0;
		for(n = 32; addr < past; n = n | (n << 1)) begin
			if(reqspc)
				stall8($random);
			sbo_addr(addr, bsize);
			xbread(addr, bsize, blk);
			npat = pat[i];
			bexp(addr, bsize, blk, npat, addr[3:2]);
			addr = space + n;
			i = i + 1;
		end
	end
endtask

// check state of secure exception;

task check_sec_excp;
	input exp;			// expected value;
	begin
		//XXX
	end
endtask

// check state of secure bit;

task check_sec_bit;
	input exp;			// expected value;
	begin
		if(`sec_bit !== exp)
			$display("ERROR: %t: %M: secure mode %b exp %b", $time, `sec_bit, exp);
	end
endtask

// enter secure mode; XXX
// fetch from secure exception vector;

task sec_mode_enter;
	input [31:0] evec;	// exception vector;
	input sec_excp;		// expected state of secure exception bit;
	reg [31:0] r;		// read data;
	begin
		// the exception signal should be as expected;
		// however, secure mode is not on until after the fetch;

		$display("%t: %M", $time);
		check_sec_excp(sec_excp);
		check_sec_bit(0);

		// read must return valid word for exception handler;

		xsread(evec, r);
		sexp(`rom_get(evec[12:2]), r, 32'hffff_ffff);

		check_sec_excp(sec_excp);
		check_sec_bit(1);
	end
endtask

// fill dma buffer with pattern;
// use cpu instead of backdoor for portability;
// based on cacheline size;
// rotate left seed pattern for each cacheline;

task dma_bpat;
	input [31:0] addr;	// address, aligned to cacheline;
	input [15:0] size;	// # of buffers;
	input [31:0] pat;	// pattern seed;
	integer n;
	reg [0:`BSIZE_BITS-1] blk;
	begin
		baddr(addr, `BSIZE_MAX, 0);
		for(n = size * 32 / `BSIZE_MAX; n > 0; n = n - 1) begin
			bpat(pat, `BSIZE_MAX, blk);
			xbwrite(addr, `BSIZE_MAX, blk);
			addr = addr + (`BSIZE_MAX << 2);
		end
	end
endtask

// check expected pattern in sram;
// use cpu to check instead of backdoor;
// based on cacheline size;
// rotate left seed pattern for each cacheline;

task dma_bexp;
	input [31:0] addr;	// address, aligned to cacheline;
	input [15:0] size;	// # of buffers;
	input [31:0] pat;	// pattern seed;
	integer n;
	reg [0:`BSIZE_BITS-1] blk;
	begin
		baddr(addr, `BSIZE_MAX, 0);
		for(n = size * 32 / `BSIZE_MAX; n > 0; n = n - 1) begin
			xbread(addr, `BSIZE_MAX, blk);
			bexp(addr, `BSIZE_MAX, blk, pat, 0);
			addr = addr + (`BSIZE_MAX << 2);
		end
	end
endtask

// fill aes buffer with pattern;
// use cpu instead of backdoor for portability;
// based on cacheline size;
// rotate left seed pattern for each cacheline;
// read back from last block to flush ALI buffers;

task aes_bpat;
	input [31:0] addr;	// address, aligned to cacheline;
	input [15:0] size;	// # of buffers;
	input [31:0] pat;	// pattern seed;
	reg [31:0] r;		// read data for buffer flush;
	reg [0:`BSIZE_BITS-1] blk;
	begin
		$display("%t: %M: size=%0d pat=0x%h", $time, size, pat);
		baddr(addr, `BSIZE_16, 1);
		while(size) begin
			bpat(pat, `BSIZE_16, blk);
			xbwrite(addr, `BSIZE_16, blk);
			addr = addr + (`BSIZE_16 << 2);
			size = size - 1;
		end
		addr = addr - (`BSIZE_16 << 2);
		xsread(addr, r);
	end
endtask

// check expected pattern in memory;
// use cpu to check instead of backdoor;
// based on cacheline size;
// rotate left seed pattern for each cacheline;

task aes_bexp;
	input [31:0] addr;	// address, aligned to cacheline;
	input [15:0] size;	// # of buffers;
	input [31:0] pat;	// pattern seed;
	input decrypt;		// 0=encrypted, 1=decrypted;
	input [127:0] key;	// key;
	inout [127:0] iv;	// iv in/out;
	reg [127:0] epat;	// expected pattern;
	reg [127:0] exp;	// expected data;
	reg [0:`BSIZE_BITS-1] blk;
	begin
		$display("%t: %M: size=%0d pat=0x%h", $time, size, pat);
		baddr(addr, `BSIZE_16, 1);
		while(size) begin
			epat[127:96] = pat; pat = next_pat(pat);
			epat[95:64] = pat; pat = next_pat(pat);
			epat[63:32] = pat; pat = next_pat(pat);
			epat[31:0] = pat; pat = next_pat(pat);
			aes_task(epat, exp, key, iv, decrypt);
			xbread(addr, `BSIZE_16, blk);
			if(blk != exp)
				$display("ERROR: %t: %M: addr 0x%h blk=%h exp %h", $time, addr, blk, exp);
			addr = addr + (`BSIZE_16 << 2);
			size = size - 1;
			iv = decrypt? epat : exp;
		end
	end
endtask

// fill sha buffer with pattern;
// use cpu instead of backdoor for portability;
// based on cacheline size;
// rotate left seed pattern for each cacheline;
// read back from last block to flush ALI buffers;

task sha_bpat;
	input [31:0] addr;		// address, aligned to cacheline;
	input [15:0] size;		// size in 64-byte blocks;
	input [31:0] pat;		// pattern seed;
	reg [31:0] r;			// read data for buffer flush;
	reg [0:`BSIZE_BITS-1] blk;
	begin
		$display("%t: %M: size=%0d pat=0x%h", $time, size, pat);
		baddr(addr, 16, 1);
		size = size << 2;
		while(size) begin
			bpat(pat, `BSIZE_16, blk);
			xbwrite(addr, `BSIZE_16, blk);
			addr = addr + (`BSIZE_16 << 2);
			size = size - 1;
		end
		addr = addr - (`BSIZE_16 << 2);
		xsread(addr, r);
	end
endtask

// check hash data buffer;
// read the data and check that they are not modified;
// compute hash over buffer;
// rotate left seed pattern for each cacheline;

task sha_bexp;
	input [31:0] addr;		// address, aligned to cacheline;
	input [15:0] size;		// size in 64-byte blocks;
	input [31:0] pat;		// pattern seed;
	reg [31:0] got;			// received data;
	integer n;
	reg [0:`BSIZE_BITS-1] blk;
	begin
		$display("%t: %M: size=%0d pat=0x%h", $time, size, pat);
		baddr(addr, 16, 1);
		size = size << 2;
		while(size) begin
			xbread(addr, `BSIZE_16, blk);
			bexp(addr, `BSIZE_16, blk, pat, 0);
			addr = addr + (`BSIZE_16 << 2);
			size = size - 1;
		end
	end
endtask

// compute hash over pattern sequence;

task sha_hpat;
	input [15:0] size;		// size in 64-byte blocks;
	input [31:0] pat;		// pattern seed;
	input chain;			// chain hash state;
	inout [159:0] hash;		// hash result;
	integer n;
	reg [511:0] msg;		// message block;
	reg init;				// init new hash op;
	begin
		$display("%t: %M: size=%0d pat=0x%h chain=%b", $time, size, pat, chain);
		init = ~chain;
		while(size) begin
			for(n = 0; n < 16; n = n + 1) begin
				msg = { msg[479:0], pat };
				pat = next_pat(pat);
			end
			sha1_task(hash, msg, init);
			init = 0;
			size = size - 1;
		end
	end
endtask

// check hash result;

task sha_hcmp;
	input [31:0] addr;		// location of hw hash result;
	input [159:0] exp;		// expected hash;
	reg [159:0] hash;		// hw computed hash;
	begin
		xsread(addr +  0, hash[159:128]);
		xsread(addr +  4, hash[127:96]);
		xsread(addr +  8, hash[95:64]);
		xsread(addr + 12, hash[63:32]);
		xsread(addr + 16, hash[31:0]);
		if(hash !== exp)
			$display("ERROR: %t: %M: addr=0x%h hash %h exp %h", $time, addr, hash, exp);
	end
endtask

// setup pci host for gi dma;

task sys_dma_setup;
	input enable;			// enable bridge as target;
	input stalls;			// enable bridge trdy stalls;
	begin
`ifdef	PCI_HOST
		if(enable)
			sim.sys.setup(`REQ_GI, `REQ_TARGET_CMDS, 16'b1101_0000_1100_0000);
		sim.sys.setup(`REQ_GI, `REQ_BE_TARGET, enable);
		sim.sys.setup(`REQ_GI, `REQ_TRDY_STALLS, stalls);
`endif	// PCI_HOST
	end
endtask

// setup system bus monitor for gi dma;
// only the ALI env has an internal pci monitor;

task sys_mon_setup;
	input enable;
	begin
`ifdef	GI_PCI
		`gi_sim_top.pci_mon_ext.cmd_mask(`REQ_GI, enable? 16'b0000_0000_1100_0000 : 16'd0);
`ifdef	ALI_GI
		`gi_sim_top.pci_mon_int.cmd_mask(`REQ_GI, enable? 16'b0000_0000_1100_0000 : 16'd0);
`endif	// ALI_GI
`endif	// GI_PCI
	end
endtask

