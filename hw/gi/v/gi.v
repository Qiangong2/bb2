// gi.v Frank Berndt;
// gi with system interface;
// :set tabstop=4

// XXX virage/srom versus battery-backed sram;

module gi (
	clk, clk_lock, reset, alt_boot, test_in, test_ena,
	gc_rst, sec_intr, sec_ack,
	i2c_clk, i2c_din, i2c_dout, i2c_doe,
	sys_addr, sys_cmd, sys_in, sys_out, sys_intr,
	dma_req, dma_bx, dma_addr, dma_wr, dma_done, dma_retry, dma_err,
	di_in, di_out, di_oe,
	di_brk_in, di_brk_out, di_brk_oe,
	di_dir, di_hstrb, di_dstrb, di_err, di_cover, di_reset,
	ais_clk, ais_lr, ais_d
);
	input clk;					// gi core clock;
	input clk_lock;				// clk is stable, pll locked;
	input reset;				// system reset;
	input alt_boot;				// alternative boot;
	input test_in;				// test enable pin on engr chips;
	output test_ena;			// enable test/debug logic;

	// flipper control;

	output gc_rst;				// reset to flipper;
	output sec_intr;			// cause secure exception;
	input sec_ack;				// secure exception fetch;

	// MCM i2c serial flash;

	output i2c_clk;				// i2c clock;
	input i2c_din;				// i2c data from io cell;
	output i2c_dout;			// i2c data to io cell;
	output i2c_doe;				// i2c data output enable;

	// interface to system;

	input [17:2] sys_addr;		// cpu address;
	input [2:0] sys_cmd;		// bus command;
	input [31:0] sys_in;		// write data, cpu and dma;
	output [31:0] sys_out;		// read data. cpu and dma;
	output sys_intr;			// interrupt to system;
	output dma_req;				// request a dma transfer;
	output [1:0] dma_bx;		// burst control;
	output [27:6] dma_addr;		// dma physical address;
	output dma_wr;				// dma write req to memory;
	input dma_done;				// dma done;
	input dma_retry;			// retry dma;
	input [2:0] dma_err;		// dma errors;

	// disk interface;

	input [7:0] di_in;			// DI data from input cell;
	output [7:0] di_out;		// DI data to output cell;
	output di_oe;				// DI output enable;
	input di_brk_in;			// DI break from input cell;
	output di_brk_out;			// DI break to output cell;
	output di_brk_oe;			// DI break output enable;
	input di_dir;				// DI direction;
	input di_hstrb;				// DI host strobe;
	output di_dstrb;			// DI drive strobe;
	output di_err;				// DI error;
	output di_cover;			// DI cover;
	input di_reset;				// DI reset;

	// ais interface;

	input ais_clk;				// AIS clock from GC;
	input ais_lr;				// AIS left/right signal;
	output ais_d;				// AIS data out;

	// XXX implement;
	// XXX wire to bi SEC_MODE;

	assign i2c_clk = 1'b0; //XXX
	assign i2c_dout = 1'b0; //XXX
	assign i2c_doe = 1'b0; //XXX

	// XXX need steady clock for soft_reset;

	wire v_porst;				// charge pump power-on reset;
	wire soft_reset;			// soft reset;

	// important bits;

	wire secure;				// secure mode;
	wire trap_gi;				// gi secure trap;
	wire sec_rsp;				// response must go through crypto;

	assign trap_gi = 1'b0; //XXX

	// define register interface;
	// reg_sel[] and reg_we are active for writes;
	// reg read data are supplied by units in next clock;
	// units must drive 0 when not selected;

	wire [6:0] reg_sel;			// unit selects;
	wire [7:0] reg_addr;		// register addr decodes;
	wire reg_we;				// register write enable;
	wire [31:0] reg_wdata;		// register write data;
	wire [31:0] reg_rdata;		// register read data;

	// instantiate ROM;
	// target is 2kx32, 8kBytes;

	wire [12:2] rom_addr;		// rom address;
	wire [31:0] rom_rdata;		// rom read data;

	gi_rom rom (
		.clk(clk),
		.addr(rom_addr),
		.rdata(rom_rdata)
	);

	// instantiate SRAM;

	wire [16:2] sram_addr;		// sram address;
	wire sram_we;				// sram write-enable;
	wire [31:0] sram_wdata;		// sram write data;
	wire [31:0] sram_rdata;		// sram read data;

	gi_sram sram (
		.clk(clk),
		.addr(sram_addr),
		.we(sram_we),
		.wdata(sram_wdata),
		.rdata(sram_rdata)
	);

	// instantiate virage wrapper;
	// contains novea, controller and charge pump;
	// XXX move to pci clock domain;
	// XXX serial interface to pins;

	wire [9:2] v_addr;			// virage address;
	wire v_me;					// virage access enable;
	wire v_we;					// virage write enable;
	wire [31:0] v_wdata;		// virage write data;
	wire [31:0] v_rdata;		// virage read data;
	wire v_vpp;					// charge pump voltage pad;

	gi_v v (
		.clk(clk),
		.reset(reset),
		.v_addr(v_addr),
		.v_me(v_me),
		.v_we(v_we),
		.v_in(v_wdata),
		.v_out(v_rdata),
		.v_sclk(1'b0), //XXX
		.v_sme(1'b0), //XXX
		.v_si(1'b0), //XXX
		.v_so(), //XXX
		.v_porst(v_porst),
		.v_vpp(v_vpp)
	);

	// sram arbiter;
	// fixed priority arbitration;
	// bus interface;
	//	- may burst at full rate;
	//	- limited to 64/128 bytes (16/32 words);
	//	- if PCI then one word every three clock;
	//	- if full rate interface, then all other units must be
	//	  able be tolerate 32 (128 bytes) clock latency;
	// DI:
	//	- one byte every 50ns, one word every 200ns;
	//	- one word buffer in DI;
	//	- can be stalled;
	// SHA1:
	//	- read/write can be stalled;
	// AES:
	//	- 4 clocks to read 16 bytes;
	//	- ten clocks of processing time;
	//	- 4 clocks to write back decrypted data;
	//	- read/write can be stalled;
	// GI:
	//	- access scatter/gather list;
	//	- not critical, can be delayed;
	// AIS:
	//	- one byte every 20us, one word per 80us;
	//	- one word buffer in AIS, so latency not a problem;
	//	- never writes to SRAM;
	//
	// arbitration priority (highest to lowest);
	//	0=bus, 1=AIS, 2=DI, 3=AES, 4=SHA1, 5=GI;
	// grant is used by unit to increment address;

	wire [5:0] sram_req;		// requests;
	wire [5:0] sram_win;		// arbitration winner;
	reg [5:0] sram_gnt;			// grants;
	reg [5:0] sram_sel;			// copy of grants for addr/data select;

	assign sram_win[0] = sram_req[0];
	assign sram_win[1] = (sram_req[0] == 1'b0) & sram_req[1];
	assign sram_win[2] = (sram_req[1:0] == 2'b0) & sram_req[2];
	assign sram_win[3] = (sram_req[2:0] == 3'b0) & sram_req[3];
	assign sram_win[4] = (sram_req[3:0] == 4'b0) & sram_req[4];
	assign sram_win[5] = (sram_req[4:0] == 5'b0) & sram_req[5];

	always @(posedge clk)
	begin
		if(reset) begin
			sram_gnt <= 6'd0;
			sram_sel <= 6'd0;
		end else begin
			sram_gnt <= sram_win;
			sram_sel <= sram_win;
		end
	end

	// sram address and write-enable mux;
	// addresses from units should come from regs;
	// have full clock to select and make it to sram address reg;
	// the grants can be used directly to advance address from unit;

	wire [16:2] bi_addr;		// bi address;
	wire [16:2] di_addr;		// di address;
	wire [16:2] sha_addr;		// sha1 address;
	wire [16:2] aes_addr;		// aes address;
	wire [16:2] ctrl_addr;		// ctrl address;
	wire [16:2] ais_addr;		// ais address;

	assign sram_addr = (bi_addr & {15{sram_sel[0]}})
		| (ais_addr & {15{sram_sel[1]}})
		| (di_addr & {15{sram_sel[2]}})
		| (aes_addr & {15{sram_sel[3]}})
		| (sha_addr & {15{sram_sel[4]}})
		| (ctrl_addr & {15{sram_sel[5]}});

	// sram write data mux;
	// write data from units should come from regs;
	// have full clock to select and make it to sram data reg;
	// ais never writes;

	wire [31:0] bi_wdata;		// bi write data;
	wire [31:0] di_wdata;		// di write data;
	wire [31:0] sha_wdata;		// sha write data;
	wire [31:0] aes_wdata;		// aes write data;
	wire [31:0] ctrl_wdata;		// ctrl write data;

	assign sram_wdata = (bi_wdata & {32{sram_sel[0]}})
		| (di_wdata & {32{sram_sel[2]}})
		| (aes_wdata & {32{sram_sel[3]}})
		| (sha_wdata & {32{sram_sel[4]}})
		| (ctrl_wdata & {32{sram_sel[5]}});

	// sram write enable mux;
	// XXX add access control;

	wire bi_we;					// bi write enable;
	wire di_we;					// di write enable;
	wire sha_we;				// sha write enable;
	wire aes_we;				// aes write enable;
	wire ctrl_we;				// ctrl write enable;

	assign sram_we = (bi_we & sram_sel[0])
		| (di_we & sram_sel[2])
		| (aes_we & sram_sel[3])
		| (sha_we & sram_sel[4])
		| (ctrl_we & sram_sel[5]);

	// sram read data buffering;
	// loading of the sram output is high;
	// the bus interface is allowed to use the data without buffering;
	// needed to reduce read latency;
	// all other units have time to eat another clock;
	// units may delay even further depending on their constraints;

	reg [31:0] sram_xdata;		// buffered sram data;

	always @(posedge clk)
	begin
		sram_xdata <= sram_rdata;
	end

	// ais/bi interface;

	wire ais_req;				// ais dma request;
	wire [27:6] ais_mem;		// ais dma address;
	wire [16:6] ais_buf;		// dma buffer in sram;
	wire ais_done;				// ais dma done;
	wire ais_abort;				// ais dma aborted;
	wire [2:0] ais_err;			// ais dma error;

	// mc/bi interface;

	wire mc_req;				// mc dma request;
	wire mc_wr;					// mc dma write;
	wire [27:6] mc_mem;			// mc dma address;
	wire [16:6] mc_buf;			// dma buffer in sram;
	wire mc_done;				// mc dma done;
	wire mc_abort;				// mc dma aborted;
	wire [2:0] mc_err;			// mc dma error;

	// mc/aes interface;

	wire md_req;				// mc decrypt request;
	wire md_chain;				// mc decrypt chain;
	wire [5:4] md_size;			// mc decrypt size;
	wire [16:4] md_ptr;			// mc decrypt sram ptr;
	wire md_done;				// mc decrypt done;
	wire md_dckey;				// mc decrypt with dc key;
	wire me_req;				// mc encrypt request;
	wire me_chain;				// mc encrypt chain;
	wire [5:4] me_size;			// mc encrypt size;
	wire [16:4] me_ptr;			// mc encrypt sram ptr;
	wire me_done;				// mc encrypt done;

	// mc/sha interface;

	wire mc_sha_req;			// mc request to sha;
	wire mc_sha_chain;			// chain mc sha;
	wire [16:6] mc_sha_ptr;		// mc ptr to data;
	wire mc_sha_save;			// save hash result to sram;
	wire mc_sha_done;			// mc sha done;

	// dc/bi interface;

	wire dc_busy;				// gc is busy;
	wire dc_req;				// dc dma request;
	wire dc_wr;					// dc dma write to memory;
	wire [27:7] dc_mem;			// dc dma address;
	wire [16:7] dc_buf;			// dma buffer in sram;
	wire dc_dma;				// dma is busy for dc;
	wire dc_done;				// dc dma done;
	wire dc_abort;				// dc dma aborted;
	wire [2:0] dc_err;			// dc dma error;

	// dc/di interface;

	wire dc_di_load;			// load di from gc;
	wire [11:0] gc_di_size;		// di request size; XXX words;
	wire gc_di_out;				// di data direction;
	wire [16:2] gc_di_ptr;		// di_ptr from gc;

	// dc/aes interface;
	// XXX

	// dc/sha interface;

	wire dc_sha_req;			// dc request to sha;
	wire dc_sha_chain;			// chain dc sha;
	wire [16:6] dc_sha_ptr;		// dc ptr to data;
	wire dc_sha_cmp;			// compare hash result;
	wire [16:2] dc_sha_buf;		// ptr to expected hash;
	wire dc_sha_ack;			// sha consumed dc data;
	wire dc_sha_busy;			// sha busy with dc;
	wire dc_sha_fail;			// comparsion failed;

	// interrupts to bi;

	wire di_intr;				// di interrupt;
	wire ais_intr;				// ais interrupt;
	wire [1:0] aes_intr;		// aes interrupts;
	wire mc_intr;				// mc interrupt;
	wire sha_intr;				// sha interrupt;
	wire dc_intr;				// dc interrupt;

	// instantiate bus interface unit;
	// master of the register interface;
	// contains dma arbiter for system bus;

	gi_bi bi (
		.clk(clk),
		.reset(reset),
		.alt_boot(alt_boot),
		.soft_reset(soft_reset),
		.test_in(test_in),
		.test_ena(test_ena),
		.sys_addr(sys_addr),
		.sys_cmd(sys_cmd),
		.sys_in(sys_in),
		.sys_out(sys_out),
		.sys_intr(sys_intr),
		.dma_req(dma_req),
		.dma_bx(dma_bx),
		.dma_addr(dma_addr),
		.dma_wr(dma_wr),
		.dma_done(dma_done),
		.dma_retry(dma_retry),
		.dma_err(dma_err),
		.reg_sel(reg_sel),
		.reg_addr(reg_addr),
		.reg_we(reg_we),
		.reg_wdata(reg_wdata),
		.reg_rdata(reg_rdata),
		.sram_req(sram_req[0]),
		.sram_gnt(sram_gnt[0]),
		.sram_addr(bi_addr),
		.sram_we(bi_we),
		.sram_wdata(bi_wdata),
		.sram_rdata(sram_rdata),
		.rom_addr(rom_addr),
		.rom_rdata(rom_rdata),
		.v_addr(v_addr),
		.v_me(v_me),
		.v_we(v_we),
		.v_wdata(v_wdata),
		.v_rdata(v_rdata),
		.secure(secure),
		.trap_gi(trap_gi),
		.ais_req(ais_req),
		.ais_mem(ais_mem),
		.ais_buf(ais_buf),
		.ais_done(ais_done),
		.ais_abort(ais_abort),
		.ais_err(ais_err),
		.mc_req(mc_req),
		.mc_wr(mc_wr),
		.mc_mem(mc_mem),
		.mc_buf(mc_buf),
		.mc_done(mc_done),
		.mc_abort(mc_abort),
		.mc_err(mc_err),
		.dc_req(dc_req),
		.dc_wr(dc_wr),
		.dc_mem(dc_mem),
		.dc_buf(dc_buf),
		.dc_dma(dc_dma),
		.dc_done(dc_done),
		.dc_abort(dc_abort),
		.dc_err(dc_err),
		.di_intr(di_intr),
		.ais_intr(ais_intr),
		.aes_intr(aes_intr),
		.mc_intr(mc_intr),
		.sha_intr(sha_intr),
		.dc_intr(dc_intr)
	);

	// instantiate di unit;

	wire [31:0] di_rdata;		// di register read data;

	gi_di di (
		.clk(clk),
		.reset(reset),
		.di_in(di_in),
		.di_out(di_out),
		.di_oe(di_oe),
		.di_brk_in(di_brk_in),
		.di_brk_out(di_brk_out),
		.di_brk_oe(di_brk_oe),
		.di_dir(di_dir),
		.di_hstrb(di_hstrb),
		.di_dstrb(di_dstrb),
		.di_err(di_err),
		.di_cover(di_cover),
		.di_reset(di_reset),
		.reg_sel(reg_sel[1]),
		.reg_addr(reg_addr),
		.reg_we(reg_we),
		.reg_wdata(reg_wdata),
		.reg_rdata(di_rdata),
		.sram_req(sram_req[2]),
		.sram_gnt(sram_gnt[2]),
		.sram_addr(di_addr),
		.sram_we(di_we),
		.sram_wdata(di_wdata),
		.sram_rdata(sram_xdata),
		.secure(secure),
		.sec_rsp(sec_rsp),
		.gc_busy(dc_busy),
		.dc_load(dc_di_load),
		.gc_di_size(gc_di_size),
		.gc_di_out(gc_di_out),
		.gc_di_ptr(gc_di_ptr),
		.di_busy(di_busy),
		.di_intr(di_intr)
	);

	// instantiate aes unit;

	wire [31:0] aes_rdata;		// aes register read data;

	gi_aes aes (
		.clk(clk),
		.reset(reset),
		.secure(secure),
		.reg_sel(reg_sel[2]),
		.reg_addr(reg_addr),
		.reg_we(reg_we),
		.reg_wdata(reg_wdata),
		.reg_rdata(aes_rdata),
		.sram_req(sram_req[3]),
		.sram_gnt(sram_gnt[3]),
		.sram_addr(aes_addr),
		.sram_we(aes_we),
		.sram_wdata(aes_wdata),
		.sram_rdata(sram_xdata),
		.dc_req(1'b0), //XXX
		.dc_chain(1'b0), //XXX
		.dc_iv(13'd0), //XXX
		.dc_ptr(13'd0), //XXX
		.dc_done(), //XXX
		.md_req(md_req),
		.md_chain(md_chain),
		.md_size(md_size),
		.md_ptr(md_ptr),
		.md_done(md_done),
		.md_dckey(md_dckey),
		.me_req(me_req),
		.me_chain(me_chain),
		.me_size(me_size),
		.me_ptr(me_ptr),
		.me_done(me_done),
		.aes_intr(aes_intr)
	);

	// instantiate sha1 unit;

	wire [31:0] sha_rdata;		// sha register read data;

	gi_sha sha (
		.clk(clk),
		.reset(reset),
		.secure(secure),
		.reg_sel(reg_sel[3]),
		.reg_addr(reg_addr),
		.reg_we(reg_we),
		.reg_wdata(reg_wdata),
		.reg_rdata(sha_rdata),
		.sram_req(sram_req[4]),
		.sram_gnt(sram_gnt[4]),
		.sram_addr(sha_addr),
		.sram_we(sha_we),
		.sram_wdata(sha_wdata),
		.sram_rdata(sram_xdata),
		.mc_req(mc_sha_req),
		.mc_chain(mc_sha_chain),
		.mc_ptr(mc_sha_ptr),
		.mc_save(mc_sha_save),
		.mc_done(mc_sha_done),
		.dc_req(dc_sha_req),
		.dc_chain(dc_sha_chain),
		.dc_ptr(dc_sha_ptr),
		.dc_cmp(dc_sha_cmp),
		.dc_buf(dc_sha_buf),
		.dc_ack(dc_sha_ack),
		.dc_busy(dc_sha_busy),
		.dc_fail(dc_sha_fail),
		.sha_intr(sha_intr)
	);

	// instantiate dc unit;

	wire [31:0] dc_rdata;		// dc register read data;

	gi_dc dc (
		.clk(clk),
		.reset(reset),
		.secure(secure),
		.reg_sel(reg_sel[4]),
		.reg_addr(reg_addr),
		.reg_we(reg_we),
		.reg_wdata(reg_wdata),
		.reg_rdata(dc_rdata),
		.sram_req(sram_req[5]),
		.sram_gnt(sram_gnt[5]),
		.sram_addr(ctrl_addr),
		.sram_we(ctrl_we),
		.sram_wdata(ctrl_wdata),
		.sram_rdata(sram_xdata),
		.dc_busy(dc_busy),
		.dma_req(dc_req),
		.dma_wr(dc_wr),
		.dma_mem(dc_mem),
		.dma_buf(dc_buf),
		.dma_act(dc_dma),
		.dma_done(dc_done),
		.dma_abort(dc_abort),
		.dma_err(dc_err),
		.di_load(dc_di_load),
		.gc_di_size(gc_di_size),
		.gc_di_out(gc_di_out),
		.gc_di_ptr(gc_di_ptr),
		.sha_req(dc_sha_req),
		.sha_chain(dc_sha_chain),
		.sha_ptr(dc_sha_ptr),
		.sha_cmp(dc_sha_cmp),
		.sha_buf(dc_sha_buf),
		.sha_ack(dc_sha_ack),
		.sha_busy(dc_sha_busy),
		.sha_fail(dc_sha_fail),
		.dc_intr(dc_intr)
	);

	// instantiate ais unit;

	wire [31:0] ais_rdata;		// ais register read data;

	gi_ais ais (
		.clk(clk),
		.reset(reset),
		.secure(secure),
		.ais_clk(ais_clk),
		.ais_lr(ais_lr),
		.ais_d(ais_d),
		.reg_sel(reg_sel[5]),
		.reg_addr(reg_addr),
		.reg_we(reg_we),
		.reg_wdata(reg_wdata),
		.reg_rdata(ais_rdata),
		.sram_req(sram_req[1]),
		.sram_gnt(sram_gnt[1]),
		.sram_addr(ais_addr),
		.sram_rdata(sram_xdata),
		.ais_req(ais_req),
		.ais_mem(ais_mem),
		.ais_buf(ais_buf),
		.ais_done(ais_done),
		.ais_abort(ais_abort),
		.ais_err(ais_err),
		.ais_intr(ais_intr)
	);

	// instantiate mc unit;

	wire [31:0] mc_rdata;		// mc register read data;

	gi_mc mc (
		.clk(clk),
		.reset(reset),
		.secure(secure),
		.reg_sel(reg_sel[6]),
		.reg_addr(reg_addr),
		.reg_we(reg_we),
		.reg_wdata(reg_wdata),
		.reg_rdata(mc_rdata),
		.dma_req(mc_req),
		.dma_wr(mc_wr),
		.dma_mem(mc_mem),
		.dma_buf(mc_buf),
		.dma_done(mc_done),
		.dma_abort(mc_abort),
		.dma_err(mc_err),
		.md_req(md_req),
		.md_chain(md_chain),
		.md_size(md_size),
		.md_ptr(md_ptr),
		.md_done(md_done),
		.md_dckey(md_dckey),
		.me_req(me_req),
		.me_chain(me_chain),
		.me_size(me_size),
		.me_ptr(me_ptr),
		.me_done(me_done),
		.sha_req(mc_sha_req),
		.sha_chain(mc_sha_chain),
		.sha_ptr(mc_sha_ptr),
		.sha_save(mc_sha_save),
		.sha_done(mc_sha_done),
		.mc_intr(mc_intr)
	);

	// register read data;
	// give units one clock to supply reg read data;
	// each unit drives 0 on reg read data when not selected;
	// just OR all of them together here;

	assign reg_rdata = v_rdata | di_rdata | aes_rdata
		| sha_rdata | dc_rdata | ais_rdata | mc_rdata;

endmodule
