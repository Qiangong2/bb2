// gi_mc.v Frank Berndt
// MC (memory crypto) unit;
// :set tabstop=4
// XXX dma prefetch for sha ops to boost performance;

module gi_mc (
	clk, reset, secure,
	reg_sel, reg_addr, reg_we, reg_wdata, reg_rdata,
	dma_req, dma_wr, dma_mem, dma_buf, dma_done, dma_abort, dma_err,
	me_req, me_chain, me_size, me_ptr, me_done,
	md_req, md_chain, md_size, md_ptr, md_done, md_dckey,
	sha_req, sha_chain, sha_ptr, sha_save, sha_done,
	mc_intr
);
	input clk;					// core clock;
	input reset;				// system reset;
	input secure;				// secure mode;

	// register interface;

	input reg_sel;				// register select;
	input [7:0] reg_addr;		// register address decodes;
	input reg_we;				// register write enable;
	input [31:0] reg_wdata;		// register write data;
	output [31:0] reg_rdata;	// register read data;

	// dma interface to bi;

	output dma_req;				// dma request;
	output dma_wr;				// write main memory;
	output [27:6] dma_mem;		// dma address;
	output [16:6] dma_buf;		// dma buffer in sram;
	input dma_done;				// dma done;
	input dma_abort;			// dma aborted;
	input [2:0] dma_err;		// dma error code;

	// aes interface;

	output me_req;				// encrypt request to aese;
	output me_chain;			// load new iv;
	output [5:4] me_size;		// size;
	output [16:4] me_ptr;		// sram pointer;
	input me_done;				// aese done;

	output md_req;				// decrypt request to aesd;
	output md_chain;			// load new iv;
	output [5:4] md_size;		// size;
	output [16:4] md_ptr;		// sram pointer;
	input md_done;				// aesd done;
	output md_dckey;			// use dc key;

	// sha interface;

	output sha_req;				// request sha operation;
	output sha_chain;			// start new hash calculation;
	output [16:6] sha_ptr;		// ptr to sram data;
	output sha_save;			// save result to sram;
	input sha_done;				// sha done;

	// interrupt to bi;

	output mc_intr;				// interrupt;

	// register interface;

	// decode aes registers addresses;

	wire sec_we;				// secure reg writes;
	wire ra_ctrl;
	wire ra_addr;
	wire ra_buf;

	assign sec_we = reg_we & secure;
	assign ra_ctrl = reg_sel & reg_addr[0];
	assign ra_addr = reg_sel & reg_addr[1];
	assign ra_buf = reg_sel & reg_addr[2];

	// mc control register;
	// chaining only means something for the first block,
	// after which chaining must be 1 for the op units;

	wire mc_ctrl_wr;			// write MC_CTRL;
	wire mc_ctrl_start;			// start by write to MC_CTRL;
	wire mc_start;				// start operation;
	reg mc_kill;				// kill operation;
	wire mc_stop;				// stop operation;
	reg mc_busy;				// mc unit busy;
	wire mc_done;				// done with request;
	reg mc_intr_mask;			// intr mask;
	reg [29:28] mc_op;			// operation code;
	reg [27:26] mc_drop;		// drop buffer dma;
	reg mc_chain;				// chain crypto state;
	reg [15:4] mc_size;			// size in 16-byte blocks - 1;
	reg mc_last;				// mc_size is 0;
	wire mc_inc;				// advance size, by 64 bytes;
	wire mc_conflict;			// unit busy conflict; XXX
	reg [3:0] mc_err;			// mc error code;
	wire [31:0] mc_ctrl;		// MC_CTRL bits;

	assign mc_ctrl_wr = reg_we & ra_ctrl;
	assign mc_ctrl_start = mc_ctrl_wr & reg_wdata[31];
	assign mc_conflict = mc_busy; //XXX op and unit busy
	assign mc_start = ~mc_conflict & mc_ctrl_start;
	assign mc_stop = reset | mc_kill | mc_done;

	always @(posedge clk)
	begin
		mc_kill <= mc_ctrl_wr & ~reg_wdata[31];
		mc_busy <= ~mc_stop & (mc_busy | mc_start);
		if(mc_start) begin
			mc_op <= reg_wdata[29:28];
			mc_drop <= reg_wdata[27:26];
			mc_chain <= reg_wdata[25];
			mc_size <= reg_wdata[15:4];
		end else if(mc_inc) begin
			mc_size[15:6] <= mc_size[15:6] - 1;
			mc_drop <= { mc_drop[26], 1'b0 };
			mc_chain <= 1'b1;
		end
		mc_last <= (mc_size[15:6] == 10'd0);
		if(mc_ctrl_wr)
			mc_err <= { mc_conflict & reg_wdata[31], 3'd0 };
		else if(dma_abort)
			mc_err[2:0] <= dma_err;
	end

	assign mc_ctrl = { mc_busy, mc_intr_mask,
		mc_op, mc_drop, mc_chain, 9'd0, mc_size, mc_err };

	// decode mc operation;

	wire [3:0] mc_opx;			// decoded mc op;

	assign mc_opx[0] = (mc_op == 2'b00);	// sha;
	assign mc_opx[1] = (mc_op == 2'b01);	// aese;
	assign mc_opx[2] = (mc_op == 2'b10);	// aesd;
	assign mc_opx[3] = (mc_op == 2'b11);	// aesd, dc key;

	// mc memory address;
	// must be loaded before operation is started;
	// writes are dropped while mc is busy;
	// increments even for dropped dma buffers;

	reg [27:6] mc_addr;			// ptr to main memory;
	wire mc_addr_wr;			// load mem address;
	wire mc_addr_inc;			// increment mem address;

	assign mc_addr_wr = ~mc_busy & reg_we & ra_addr;

	always @(posedge clk)
	begin
		if(mc_addr_wr)
			mc_addr <= reg_wdata[27:6];
		else if(mc_addr_inc)
			mc_addr <= mc_addr + 1;
	end

	assign dma_mem = mc_addr;

	// mc pci buffer address;
	// points to 128-byte buffer in sram;
	// buffer must be 128-byte aligned;
	// only writable in secure mode,
	// so that sw cannot use it to attack other units;
	// should be in non-secure region as SHA writes result
	// into the first 20 bytes of the second buffer;

	reg [16:7] mc_buf;			// pci dma buffer;
	wire mc_buf_wr;				// write to mc_buf;

	assign mc_buf_wr = sec_we & ra_buf;

	always @(posedge clk)
	begin
		if(mc_buf_wr)
			mc_buf <= reg_wdata[16:7];
	end

	// register read data mux;
	// output 0 when not selected;
	// next stage will OR data from all the units;

	reg [31:0] reg_rdata;		// read data flops;

	always @(posedge clk)
	begin
		reg_rdata <= ({32{ra_ctrl}} & mc_ctrl)
			| ({32{ra_addr}} & { 4'd0, mc_addr, 6'd0 })
			| ({32{ra_buf}} & { 15'd0, mc_buf, 7'd0 });
	end

	// mc interrupt handling;

	reg mc_intr;			// intr flags;
	wire mc_intr_clr;		// clear interrupt;
	wire mc_intr_set;		// issue interrupt;

	assign mc_intr_clr = reset | mc_kill;
	assign mc_intr_set = mc_intr_mask & mc_done;

	always @(posedge clk)
	begin
		if(mc_intr_clr)
			mc_intr_mask <= 1'b0;
		else if(mc_start)
			mc_intr_mask <= reg_wdata[30];
		mc_intr <= ~mc_intr_clr & (mc_intr | mc_intr_set);
	end

	// operation sequencer;
	// works on 64-byte blocks;
	//	[0] request block from memory;
	//	[1] run operation on block in pci buffer;
	//	[2] write block back to memory, not for SHA;
	//	[3] increment ptr/size, single clock;

	reg sm_start;			// start operation sequence;
	reg [3:0] sm;			// sequencer state;
	reg sm_buf;				// pci dma buffer index;
	wire sm_next;			// advance sm to next state;

	always @(posedge clk)
	begin
		sm_start <= mc_start;
		sm_buf <= mc_busy & (sm_buf ^ sm[3]);
		if(~mc_busy)
			sm <= 4'd0;
		else if(sm_start)
			sm[0] <= 1'b1;
		else if(sm_next)
			sm <= { sm[2:0], sm[3] & ~mc_last };
	end

	assign mc_inc = sm[3];
	assign mc_done = sm[3] & mc_last;

	// advance sm;
	// state[1] ends via done from unit;
	// mc_drop shifts out with each 64-byte block;

	wire drop;				// drop pci dma;
	wire [3:0] sm_cont;		// continues;

	assign drop = mc_drop[27];

	assign sm_cont[0] = drop | dma_done;
	assign sm_cont[1] = (mc_opx[0] & sha_done)
		| (mc_opx[1] & me_done)
		| (|mc_opx[3:2] & md_done);
	assign sm_cont[2] = mc_opx[0]? 1'b1 : (drop | dma_done);
	assign sm_cont[3] = 1'b1;

	assign sm_next = |(sm & sm_cont);

	// request dma;
	// flip sram buffer after dma burst is done;

	assign dma_req = ~mc_drop[27] & (sm[0] | dma_wr);
	assign dma_wr = sm[2] & ~mc_opx[0];
	assign dma_buf = { mc_buf, sm_buf };
	assign mc_addr_inc = mc_inc;

	// request service from units;
	// request ops from aes crypto cores;
	// single-clock pulse when dma data is in sram;

	wire req_unit;			// request service;
	reg sha_req;			// request sha operation;
	reg me_req;				// request encryption;
	reg md_req;				// request decryption;
	wire [16:4] aes_ptr;	// aes buffer;
	wire [5:4] aes_size;	// aes op size;

	assign req_unit = sm[0] & sm_cont[0];
	assign aes_ptr = { mc_buf, sm_buf, 2'd0 };
	assign aes_size = mc_last? mc_size[5:4] : 2'd3;

	always @(posedge clk)
	begin
		sha_req <= mc_opx[0] & req_unit;
		me_req <= mc_opx[1] & req_unit;
		md_req <= |mc_opx[3:2] & req_unit;
	end

	assign me_chain = mc_chain;
	assign me_ptr = aes_ptr;
	assign me_size = aes_size;

	assign md_chain = mc_chain;
	assign md_ptr = aes_ptr;
	assign md_size = aes_size;
	assign md_dckey = mc_opx[3];

	assign sha_chain = mc_chain;
	assign sha_ptr = { mc_buf, sm_buf };
	assign sha_save = mc_last;

	// simulator init;
	// fill reg bits to inverse of reset defaults;
	// fill undefined bits with randoms;
	// needed, so no Xs move across PCI (parity);

`ifdef	SIM
	initial
	begin
		{ mc_busy, mc_intr_mask } = 2'b11;
		{ mc_op, mc_drop, mc_chain, mc_size, mc_err } = $random;
		mc_addr = $random;
		mc_buf = $random;
	end
`endif	// SIM
endmodule

