// gi_dc.v Frank Berndt
// DC (di crypto) unit;
// :set tabstop=4

module gi_dc (
	clk, reset, secure,
	reg_sel, reg_addr, reg_we, reg_wdata, reg_rdata,
	sram_req, sram_gnt, sram_addr, sram_we, sram_wdata, sram_rdata,
	dc_busy,
	dma_req, dma_wr, dma_mem, dma_buf, dma_act, dma_done, dma_abort, dma_err,
	di_load, gc_di_size, gc_di_out, gc_di_ptr,
	sha_req, sha_chain, sha_ptr, sha_cmp, sha_buf, sha_ack, sha_busy, sha_fail,
	dc_intr
);
	input clk;					// core clock;
	input reset;				// system reset;
	input secure;				// secure mode;

	// register interface;

	input reg_sel;				// register select;
	input [7:0] reg_addr;		// register address decodes;
	input reg_we;				// register write enable;
	input [31:0] reg_wdata;		// register write data;
	output [31:0] reg_rdata;	// register read data;

	// sram interface;

	output sram_req;			// sram request;
	input sram_gnt;				// sram grant;
	output [16:2] sram_addr;	// sram address;
	output sram_we;				// sram write enable;
	output [31:0] sram_wdata;	// sram write data;
	input [31:0] sram_rdata;	// sram read data;

	// dma interface to bi;

	output dc_busy;				// dc is busy;
	output dma_req;				// dc dma request;
	output dma_wr;				// dc dma write to memory;
	output [27:7] dma_mem;		// dc dma address;
	output [16:7] dma_buf;		// dma buffer in sram;
	input dma_act;				// dc dma in progress;
	input dma_done;				// dc dma done;
	input dma_abort;			// dc dma aborted;
	input [2:0] dma_err;		// dc dma error;

	// interface to di;

	output di_load;				// load di request;
	output [11:0] gc_di_size;	// gc di request size;
	output gc_di_out;			// gc di data direction;
	output [16:2] gc_di_ptr;	// di_ptr value;

	// interface to sha core;

	output sha_req;				// request hash;
	output sha_chain;			// chain sha request;
	output [16:6] sha_ptr;		// ptr to sram data;
	output sha_cmp;				// compare sha result;
	output [16:2] sha_buf;		// ptr to expected hash;
	input sha_ack;				// sha consumed data;
	input sha_busy;				// sha busy with dc;
	input sha_fail;				// hash comparison failed;

	// interrupt to bi;

	output dc_intr;				// interrupt;

	// register interface;

	// decode register addresses;

	wire sec_we;			// secure reg write;
	wire ra_ctrl;			// DC_CTRL register;
	wire ra_buf;			// DC_BUF register;
	wire ra_haddr;			// DC_HADDR register;
	wire ra_hidx;			// DC_HIDX register;
	wire ra_dioff;			// DC_DIOFF register;
	wire ra_iv;				// DC_IV register;
	wire ra_h0;				// DC_H0 register;
	wire ra_h1;				// DC_H1 register;

	assign sec_we = reg_we & secure;
	assign ra_ctrl = reg_sel & reg_addr[0];
	assign ra_buf = reg_sel & reg_addr[1];
	assign ra_haddr = reg_sel & reg_addr[2];
	assign ra_hidx = reg_sel & reg_addr[3];
	assign ra_dioff = reg_sel & reg_addr[4];
	assign ra_iv = reg_sel & reg_addr[5];
	assign ra_h0 = reg_sel & reg_addr[6];
	assign ra_h1 = reg_sel & reg_addr[7];

	// dc control register;
	// usable from secure and non-secure mode;

	wire dc_ctrl_wr;		// write DC_CTRL;
	wire dc_ctrl_start;		// start by write to DC_CTRL;
	wire dc_start;			// start new dc operation;
	reg dc_kill;			// kill dc operation;
	wire dc_nokill;			// do not allow kills;
	reg dc_done;			// dc is done;
	wire dc_stop;			// stop dc operation;
	reg dc_busy;			// dc is busy;
	reg dc_intr_mask;		// interrupt mask;
	reg [29:28] dc_op;		// operations code;
	reg [16:2] dc_sg;		// ptr to sg list in sram;
	reg sg_first;			// first in sg list;
	wire sg_inc;			// increment sg list ptr;
	reg [2:0] dc_err;		// dc dma error;
	wire [31:0] dc_ctrl;	// sum of all DC_CTRL bits;

	assign dc_ctrl_wr = reg_we & ra_ctrl;
	assign dc_ctrl_start = dc_ctrl_wr & reg_wdata[31];
	assign dc_start = ~dc_busy & dc_ctrl_start;
	assign dc_stop = reset | dc_kill | dc_done;

	always @(posedge clk)
	begin
		dc_kill <= ~dc_nokill & dc_ctrl_wr & ~reg_wdata[31];
		dc_busy <= ~dc_stop & (dc_busy | dc_start);
		if(dc_start) begin
			dc_op <= reg_wdata[29:28];
			dc_sg <= { reg_wdata[16:7], 5'd0 };
			sg_first <= 1'b1;
		end else if(sg_inc) begin
			dc_sg <= dc_sg + 1;
			sg_first <= 1'b0;
		end
		if(dc_ctrl_wr)
			dc_err <= 3'd0;
		else if(dma_abort)
			dc_err <= dma_err;
	end

	assign dc_ctrl = { dc_busy, dc_intr_mask,
		dc_op, 3'd0, dc_sg[6:2], 3'd0, dc_sg[16:7], 4'd0, dc_err };

	// DC_BUF register;
	// points to two 128-byte PCI buffers in sram;
	// only writable in secure mode, to not allow sram attacks;
	// SHA, AES and DI run over these buffers;
	// locate in secure sram space if decrypted content
	// of last block is considered sensitive;
	// only writable in secure mode, to prevent sram attacks;

	wire dc_buf_wr;			// write to DC_BUF;
	reg [16:8] dc_buf;		// dma buffers;

	assign dc_buf_wr = sec_we & ra_buf & ~dc_busy;

	always @(posedge clk)
	begin
		if(dc_buf_wr)
			dc_buf <= reg_wdata[16:8];
	end

	// DC_HADDR register;
	// pointer to H0 block in main memory;
	// used when H1 hashing over H0 block is required;
	// must be aligned to 4kB boundary;
	// H0 block contains 200 H0 hashes and IV;
	// writes are dropped if dc is busy;

	wire dc_haddr_wr;		// write to DC_HADDR;
	reg [27:12] dc_haddr;	// memory pointer;

	assign dc_haddr_wr = reg_we & ra_haddr & ~dc_busy;

	always @(posedge clk)
	begin
		if(dc_haddr_wr)
			dc_haddr <= reg_wdata[27:12];
	end

	// DC_HIDX;
	// offset in H0 block for H0 cache fill;
	// ptr to H1 for H0 block in sram;
	// if any are misconfigured, the hash will fail
	// and the secure kernel trap will handle the error;
	// writes are dropped if dc is busy;

	wire dc_hidx_wr;		// write DC_HIDX;
	reg [22:20] h0_idx;		// H0 index for cache fill;
	reg [16:2] h1_ptr;		// pointer to H1 for H0 block;

	assign dc_hidx_wr = reg_we & ra_hidx & ~dc_busy;

	always @(posedge clk)
	begin
		if(dc_hidx_wr) begin
			h0_idx <= reg_wdata[22:20];
			h1_ptr <= reg_wdata[16:2];
		end
	end

	// DC_DIOFF;
	// offsets into first and last data block
	// to start/stop DI byte stream;
	// hashing must be done over entire data block;
	// crypto must start at 16-byte aligned blocks;
	// the data block size is defined by the sg list;
	// H0 block contains dulpicate for IV;
	// size allow data block sizes of 16kB;
	// writes are dropped if dc is busy;

	wire dc_dioff_wr;		// write DC_DIOFF;
	reg [11:0] di_last;	// last di byte in last block;
	reg [11:0] di_first;	// first di byte in first block;

	assign dc_dioff_wr = reg_we & ra_dioff & ~dc_busy;

	always @(posedge clk)
	begin
		if(dc_dioff_wr) begin
			di_last <= reg_wdata[27:16];
			di_first <= reg_wdata[11:0];
		end
	end

	// hash region registers;
	// only writable in secure mode;
	// DC_H1 ... DC_H0 spans buffer for H1s;
	// DC_H0 ... DC_H0+640 spans H0 cache, 32 entries;
	// registers are readable in non-secure mode,
	// because no secrets are exposed;
	// registers keep state across resets;
	// writes are dropped if dc is busy;

	reg [16:4] dc_iv;		// end of hash space;
	reg [16:7] dc_h0;		// begin of H0 cache;
	reg [16:4] dc_h1;		// begin of H1 space;
	wire dc_iv_wr;			// write DC_IV;
	wire dc_h0_wr;			// write DC_H0;
	wire dc_h1_wr;			// write DC_H1;

	assign dc_iv_wr = sec_we & ra_iv & ~dc_busy;
	assign dc_h0_wr = sec_we & ra_h0 & ~dc_busy;
	assign dc_h1_wr = sec_we & ra_h1 & ~dc_busy;

	always @(posedge clk)
	begin
		if(dc_iv_wr)
			dc_iv <= reg_wdata[16:4];
		if(dc_h0_wr)
			dc_h0 <= reg_wdata[16:7];
		if(dc_h1_wr)
			dc_h1 <= reg_wdata[16:4];
	end

	// register read data mux;
	// output 0 when not selected;
	// next stage will OR data from all the units;

	reg [31:0] reg_rdata;		// read data flops;

	always @(posedge clk)
	begin
		reg_rdata <= ({32{ra_ctrl}} & dc_ctrl)
			| ({32{ra_buf}} & { 15'd0, dc_buf, 8'd0 })
			| ({32{ra_haddr}} & { 4'd0, dc_haddr, 12'd0 })
			| ({32{ra_hidx}} & { 9'd0, h0_idx, 3'd0, h1_ptr, 2'd0 })
			| ({32{ra_dioff}} & { 4'd0, di_last, 4'd0, di_first })
			| ({32{ra_iv}} & { 15'd0, dc_iv, 4'd0 })
			| ({32{ra_h0}} & { 15'd0, dc_h0, 7'd0 })
			| ({32{ra_h1}} & { 15'd0, dc_h1, 4'd0 });
	end

	// check for h1 ptr miss;
	// valid one clock after DC_H0, DC_H1 or DC_HIDX change;
	// secure trap is triggered on use of a miss;

	wire [16:2] h1_p5;		// h1_ptr + 5;
	reg [1:0] h1_miss;		// h1 ptr miss;

	assign h1_p5 = h1_ptr + 5;

	always @(posedge clk)
	begin
		h1_miss[0] <= (h1_ptr[16:4] < dc_h1);
		h1_miss[1] <= (h1_p5[16:4] >= dc_h0);
	end

	// operations sequencer;
	//	op	sequence
	//	00	read from DI;
	//	01	hash extraction;
	//	10	write to DI;
	//	11	hash extraction, write to DI;

	reg op_start;		// delayed dc_start;
	wire start_he;		// start hash extraction;
	reg op_he;			// run hash extraction first;
	wire done_he;		// done with hash extraction;
	wire start_di;		// start di operation;
	reg op_di;			// run di operation;
	wire done_di;		// done with di operation;
	wire op_done;		// operation done;

	assign start_he = op_start & dc_op[28];
	assign start_di = (op_start & ~dc_op[28]) | (done_he & dc_op[29]);
	assign done_di = 1'b0; //XXX

	always @(posedge clk)
	begin
		op_start <= dc_start;
		op_he <= dc_busy & ~done_he & (op_he | start_he);
		op_di <= dc_busy & ~done_di & (op_di | start_di);
		dc_done <= dc_busy & op_done;
	end

	assign op_done =
		((dc_op == 2'b01) & done_he)	// h0-only done;
		;

	// H0 hash extraction;
	// read entire H0 block from memory;
	// compute hash over entire block;
	// extract the indicated H0 entries into H0 cache;
	// compare H1 hash from sram with result of H0 block;
	// stop if hashes do not match;
	// aligning H0 cache avoids copying by using the
	// H0 cache as the dma buffer;
	// copying would have taken at least 640b/4*20ns=3.2usec;
	// computing hash over H0 block should be around 65usecs;

	reg [4:0] h0_val;	// h0 valid flags;

	//XXX


	// sg list logic;
	// dc only requests sram for reading of sg list;
	// end of list is automatic hash boundary;

	wire sg_req;			// request a sg entry;
	wire sg_end;			// at end of sg list;
	reg [1:0] sg_pipe;		// read data valid pipe;
	reg [27:12] sg_addr;	// ptr to block in memory;
	reg sg_hash;			// hash boundary after this block;
	reg sg_last;			// last entry in list;
	reg [4:0] sg_hidx;		// H0 cache index;
	wire [7:0] sg_him5;		// h0 idx * 5;
	reg [16:2] sg_ptr;		// pointer to h0 cache entry;

	assign sg_req = 1'b0; //XXX

	assign sram_req = dc_busy & sg_req & ~sram_gnt;
	assign sram_addr = dc_sg;
	assign sram_we = 1'b0;
	assign sram_wdata = 32'd0;

	assign sg_end = (dc_sg[6:2] == 5'd31);
	assign sg_inc = sg_pipe[1];

	always @(posedge clk)
	begin
		sg_pipe <= { sg_pipe[0], sram_gnt };
		if(dc_busy == 1'b0)
			sg_last <= 1'b0;
		else if(sg_pipe[1]) begin
			sg_addr <= sram_rdata[27:12];
			sg_hash <= sram_rdata[9] | sg_end;
			sg_last <= sram_rdata[8] | sg_end;
			sg_hidx <= sram_rdata[4:0];
		end
		sg_ptr <= { dc_h0, 2'd0 } + sg_him5;
	end

	assign sg_him5 = { sg_hidx, 2'd0 } + sg_hidx;

	// often used op decodes;

	wire op_write;		// ops writing memory;
	wire op_sha;		// ops requiring sha;
	wire op_aes;		// ops requiring aes;

	assign op_write = (dc_op == 2'b00);
	assign op_sha = op_he; //XXX sg secure ops;
	assign op_aes = 1'b0; //XXX sg secure ops;

	// dma engine;
	// prefetch reads to keep dma buffer full;
	// avoid copying of H0 cache by aligning and
	// using the cache block as the dma buffer;
	// XXX

	wire [27:12] dma_page;	// dma page;
	wire [11:7] dma_cur;	// current buffer offset;
	wire dma_pref;			// prefetch next buffer;
	reg [11:7] dma_addr;	// dma address;
	wire dma_load;			// load dma request;

	always @(posedge clk)
	begin
		if(dma_load)
			dma_addr <= dma_cur + dma_pref;
	end

	assign dma_page = op_he? dc_haddr : sg_addr;
	assign dma_mem[27:12] = dma_page;
	assign dma_mem[11:7] = dma_addr;

	// determine dma buffer to use;
	// hash extraction uses h0 cache as dma buffer to avoid copies;
	// buffer tracks dma address bit 7;
	// h0 cache is 640 bytes, or 5 128-byte dma buffers;
	// there are 4096/128=32 dma buffer per h0 block;
	// determine h0 hit for fill from memory;

	wire h0_fill;			// fill into h0 cache;
	wire h0_init;			// init h0 cache ptr;
	wire h0_binc;			// increment h0 cache ptr;
	reg [16:7] dma_buf;		// dma buffer in sram;
	reg [16:7] h0_buf;		// h0 cache dma buffer;
	wire [16:7] nd_buf;		// normal cmd buffer;

	assign nd_buf = { dc_buf, dma_cur[7] ^ dma_pref };

	always @(posedge clk)
	begin
		if(h0_init)
			h0_buf <= dc_h0;
		else if(h0_binc)
			h0_buf <= h0_buf + 1;
		if(dma_load)
			dma_buf <= h0_fill? h0_buf : nd_buf;
	end

	// issue dma request;
	// kicked off by read or write request;
	// cleared when dc is not busy, or killed;

	wire dma_rreq;			// dma read request;
	wire dma_wreq;			// dma write request;
	reg dma_req;			// dma request pending;

	always @(posedge clk)
	begin
		dma_req <= dc_busy & ~dma_done & (dma_req | dma_rreq | dma_wreq);
	end

	assign dma_wr = op_write;

	// sequencer;
	// operates on one 4kB page, for all operations;
	// each bit encodes one state;
	// w=di->mem h=hash r=di<-mem s=secure read
	//	    whr	description
	//	[0] -**	set dma buffer;
	//	[0] -** request read dma;
	//	[1] -**	wait for dma done;
	// >[2] ***	set dma buffer;
	//	[2]	-** request prefetch dma, do not block;
	//	[3] -*s	start first sha, block until sha_ack;
	//	[4] --s start first aes, do not block;
	//	[5] *** wait for sha/aes/di completion;
	//	[6] *-* start first di, do not block;
	//	[7] -*s start second sha, block until sha_ack;
	//	[8] --s start second aes, do not block;
	//	[9] *** wait for sha/aes/di completion;
	// [10] *-* start second di, do not block;
	// [11] *-- wait for di completion;
	// [12] *-- request write dma;
	// [13] *** wait for dma completion;
	// [14] *** check end, if not goto 2;

	wire sm_sec;			// secure ops over full 4k page;
	wire sm_clr;			// clear sm state;
	reg [1:0] sm_start;		// start sm pipe;
	reg [11:7] sm_cur;		// dma buffer start index;
	reg [11:7] sm_last;		// dma buffer last index;
	wire sm_end;			// end sequence after this block;
	reg sm_done;			// done with page;
	wire sm_inc;			// increment dma buffer index;
	reg [14:0] sm;			// read state machine;
	wire [14:0] sm_new;		// new state;
	wire sm_next;			// advance sm state;
	wire sm_cont;			// continue with next block;
	reg [16:7] sm_buf;		// buffer to work on;

	assign sm_sec = op_sha | op_aes;
	assign sm_clr = reset | ~dc_busy; //XXX
	assign sm_end = (sm_cur == sm_last);

	always @(posedge clk)
	begin
		if(sm_clr)
			sm_start <= 2'd0;
		else
			sm_start <= { sm_start[0], start_he }; //XXX sg start
		if(sm_start[0]) begin
			sm_cur <= (sm_sec | ~sg_first)? 5'd0 : di_first[11:7];
			sm_last <= (sm_sec | ~sg_last)? 5'd31 : di_last[11:7];
		end else if(sm_inc)
			sm_cur <= sm_cur + 1;
		if(sm_clr)
			sm <= 15'd0;
		else if(sm_next)
			sm <= sm_new; 
		if(sm[2])
			sm_buf <= dma_buf;
		sm_done <= sm[6] & sm_end;
	end

	// dma request;
	// load dma with current block offset;
	// request read dma, prefetch dma or write dma;

	wire dma_rdone;			// first read dma done;
	wire dma_xdone;			// prefetch/write dma done;

	assign dma_cur = sm_cur;
	assign dma_load = sm[0] | sm[2];
	assign dma_pref = sm[2] & ~op_write & ~sm_end;
	assign dma_rreq = sm[0] | dma_pref;
	assign dma_wreq = sm[12];
	assign dma_rdone = sm[1] & dma_done;
	assign dma_xdone = sm[13] & ~dma_req;

	// determine dma buffer fill for hash extraction;
	// fill directly into h0 cache;
	// insted of dividing by 5, count mode 5;

	reg [2:0] h0_cnt;		// mod 5 counter;
	reg [2:0] h0_cur;		// current h0 index;
	wire h0_cinc;			// increment h0_cur;

	always @(posedge clk)
	begin
		if(sm_start[0]) begin
			h0_cnt <= 3'd0;
			h0_cur <= 3'd0;
		end else if(h0_cinc) begin
			h0_cnt <= h0_cnt[2]? 3'd0 : h0_cnt + 1;
			h0_cur <= h0_cnt[2]? h0_cur + 1 : h0_cur;
		end
	end

	assign h0_fill = op_he & (h0_idx == h0_cur);
	assign h0_init = sm_start;
	assign h0_cinc = ~op_write & dma_done;
	assign h0_binc = h0_cinc & h0_fill;
	assign done_he = op_he & sm_done;

	// send hash request to sha unit;
	// two hash requests per 128-byte dma block;
	// always compare on last hash request of 4kB page;
	// compare target comes from h1_ptr ot sg list h0 index;

	assign sha_req = sm[3] | sm[7];
	assign sha_chain = 'b0; //XXX
	assign sha_ptr = { sm_buf, sm[4] };
	assign sha_cmp = sm_end & sm[4] & (op_he | 1'b0); //XXX op_di
	assign sha_buf = op_he? h1_ptr : sg_ptr;

	// issue di requests;
	// max of two di bursts per 128-byte dma block;
	// XXX

	wire di_xdone;			// XXX

	assign di_load = op_di & (sm[6] | sm[10]);
	assign gc_di_size = 'bx; //XXX
	assign gc_di_out = 'bx; //XXX
	assign gc_di_ptr = 'bx; //XXX

	wire di_busy = 1'b0; //XXX
	assign di_xdone = sm[11] & ~di_busy;
	wire aes_busy = 1'b0; //XXX


	// set next sm state;
	// advance sm state;

	wire done_sad;			// done with sha, aes & di;

	assign sm_new[0] = ~op_write & sm_start[1];
	assign sm_new[1] = sm[0];
	assign sm_new[2] = sm_cont | sm[1] | (op_write & sm_start[1]);
	assign sm_new[3] = op_sha & sm[2];
	assign sm_new[4] = op_aes & sm[3];
	assign sm_new[5] = (~op_sha & sm[2]) | (op_aes? sm[4] : sm[3]);
	assign sm_new[6] = sm[5];
	assign sm_new[7] = op_sha & sm[6];
	assign sm_new[8] = op_aes & sm[7];
	assign sm_new[9] = (~op_sha & sm[6]) | (op_aes? sm[8] : sm[7]);
	assign sm_new[10] = sm[9];
	assign sm_new[11] = op_write & sm[10];
	assign sm_new[12] = sm[11];
	assign sm_new[13] = (~op_write & sm[10]) | sm[12];
	assign sm_new[14] = sm[13];

	assign done_sad = (sm[5] | sm[9]) & ~(sha_busy | aes_busy | di_busy);
	assign sm_cont = sm[14] & ~sm_end;
	assign sm_inc = sm[14];

	assign sm_next = sm_start[1]	// start;
		| sm[0]						// read dma issued;
		| dma_rdone					// read dma done, [1];
		| sm[2]						// prefetch dma issued;
		| sha_ack					// sha acked requests, [3] [7];
		| sm[4] | sm[8]				// first/second aes issued;
		| done_sad					// sha/aes/di done, [5] [9];
		| sm[6] | sm[10]			// first/second di issued;
		| di_xdone					// second di->mem done, [11];
		| sm[12]					// write dma issued;
		| dma_xdone					// prefetch/write dma done, [13];
		| sm_inc;					// [14];

	// dc interrupt handling;
	// stopping clears the mask and interrupt;

	reg dc_intr;			// intr flag;
	wire dc_intr_clr;		// clear interrupt;
	wire dc_intr_set;		// issue interrupt;

	assign dc_intr_clr = reset | dc_kill;
	assign dc_intr_set = dc_intr_mask & dc_done;

	always @(posedge clk)
	begin
		if(dc_intr_clr)  
			dc_intr_mask <= 1'b0;
		else if(dc_start)
			dc_intr_mask <= reg_wdata[30];
		dc_intr <= ~dc_intr_clr & (dc_intr | dc_intr_set);
	end

	// cannot allow hash extraction to be killed,
	// as an attacker could start the op to fill the cache,
	// then kill it, just before the check is done;
	// hash extraction has to complete up to the cmp;
	// however, allow all kills from secure mode;

	assign dc_nokill = ~secure & op_he;

	// dc secure kernel trap;
	// XXX

	// simulator init;
	// fill reg bits to inverse of reset defaults;
	// fill undefined bits with randoms;
	// needed, so no Xs move across PCI (parity);

`ifdef	SIM
	initial
	begin
		{ dc_op, dc_sg, dc_err } = $random;
		dc_buf = $random;
		dc_haddr = $random;
		{ h0_idx, h1_ptr } = $random;
		{ di_last, di_first } = $random;
		dc_iv = $random;
		dc_h0 = $random;
		dc_h1 = $random;
	end
`endif	// SIM

endmodule

