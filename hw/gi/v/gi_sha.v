// gi_sha.v Frank Berndt
// secure hash unit;
// :set tabstop=4

module gi_sha (
	clk, reset, secure,
	reg_sel, reg_addr, reg_we, reg_wdata, reg_rdata,
	sram_req, sram_gnt, sram_addr, sram_we, sram_wdata, sram_rdata,
	mc_req, mc_chain, mc_ptr, mc_save, mc_done,
	dc_req, dc_chain, dc_ptr, dc_cmp, dc_buf, dc_ack, dc_busy, dc_fail,
	sha_intr
);
	input clk;					// core clock;
	input reset;				// system reset;
	input secure;				// secure mode;

	// register interface;

	input reg_sel;				// register select;
	input [7:0] reg_addr;		// register address decodes;
	input reg_we;				// register write enable;
	input [31:0] reg_wdata;		// register write data;
	output [31:0] reg_rdata;	// register read data;

	// sram interface;

	output sram_req;			// sram request;
	input sram_gnt;				// sram grant;
	output [16:2] sram_addr;	// sram address;
	output sram_we;				// sram write enable;
	output [31:0] sram_wdata;	// sram write data;
	input [31:0] sram_rdata;	// sram read data;

	// mc sha interface;
	// one request for each block;

	input mc_req;				// request from mc;
	input mc_chain;				// chain mc request;
	input [16:6] mc_ptr;		// ptr to mc data;
	input mc_save;				// save mc hash result to sram;
	output mc_done;				// done with mc block;

	// dc sha interface;

	input dc_req;				// request from dc;
	input dc_chain;				// chain dc request;
	input [16:6] dc_ptr;		// ptr to dc data;
	input dc_cmp;				// compare hash result;
	input [16:2] dc_buf;		// ptr to dc data;
	output dc_ack;				// sha consumed data;
	output dc_busy;				// busy with dc block;
	output dc_fail;				// hash comparison failed;

	// interrupt to bi;

	output sha_intr;			// interrupt;

	// register interface;

	// decode sha register addresses;

	wire sec_we;			// secure reg write;
	wire ra_ctrl;			// ctrl register;
	wire ra_buf;			// result sram location;

	assign sec_we = reg_we & secure;
	assign ra_ctrl = reg_sel & reg_addr[0];
	assign ra_buf = reg_sel & reg_addr[1];

	// sha control register;
	// software can only use sha unit directly in secure mode;
	// interface is also used by mc for sha ops;

	wire sha_ctrl_wr;			// write SHA_CTRL;
	wire sha_ctrl_start;		// start by write to SHA_CTRL;
	wire sha_start;				// start operation;
	reg sha_kill;				// kill operation;
	reg sha_busy;				// sha busy with operation;
	wire sha_req;				// request to core;
	wire sha_stop;				// stop operation;
	wire sha_done;				// done with operation;
	reg sha_intr_mask;			// interrupt mask;
	reg sha_mc;					// sha op for MC;
	reg [29:20] sha_size;		// size in 64-byte blocks - 1;
	reg sha_save;				// write hash to sram;
	reg [16:6] sha_ptr;			// ptr to sram;
	reg sha_chain;				// chain hash state;
	wire sha_inc;				// advance to next block;
	reg sha_last;				// sha_size = 0;
	wire [31:0] sha_ctrl;		// SHA_CTRL bits;

	assign sha_ctrl_wr = sec_we & ra_ctrl;
	assign sha_ctrl_start = sha_ctrl_wr & reg_wdata[31];
	assign sha_start = ~sha_busy & (mc_req | sha_ctrl_start);
	assign sha_stop = reset | sha_kill | sha_done;
	assign sha_req = sha_busy;

	always @(posedge clk)
	begin
		sha_kill <= sha_ctrl_wr & ~reg_wdata[31];
		sha_busy <= ~sha_stop & (sha_busy | sha_start);
		if(sha_start) begin
			sha_mc <= mc_req;
			sha_size <= mc_req? 10'd0 : reg_wdata[29:20];
			sha_save <= mc_req? mc_save : reg_wdata[19];
			sha_ptr <= mc_req? mc_ptr : reg_wdata[16:6];
			sha_chain <= mc_req? mc_chain : reg_wdata[0];
		end else if(sha_inc) begin
			sha_size <= sha_size - 1;
			sha_ptr <= sha_ptr + 1;
			sha_chain <= 1'b1;
		end
		sha_last <= (sha_size == 10'd0);
	end

	assign sha_ctrl = { sha_busy, sha_intr_mask,
		sha_size, sha_save, 2'd0, sha_ptr, 5'd0, sha_chain };

	// sha hash result buffer ptr;
	// MC write offset of second PCI buffer;
	// register keeps state across resets;
	// writes are dropped if sha is busy;

	reg [16:2] sha_buf;		// result buffer ptr;
	wire sha_buf_mc;		// MC writes SHA_BUF;
	wire sha_buf_wr;		// write SHA_BUF;

	assign sha_buf_mc = ~sha_busy & mc_req;
	assign sha_buf_wr = ~sha_busy & reg_we & ra_buf;

	always @(posedge clk)
	begin
		if(sha_buf_mc)
			sha_buf <= { mc_ptr[16:7], 5'b10000 };
		else if(sha_buf_wr)
			sha_buf <= reg_wdata[16:2];
	end

	// register read data mux;
	// output 0 when not selected;
	// next stage will OR data from all the units;

	reg [31:0] reg_rdata;		// read data flops;

	always @(posedge clk)
	begin
		reg_rdata <= ({32{ra_ctrl}} & sha_ctrl)
			| ({32{ra_buf}} & { 15'd0, sha_buf, 2'd0 });
	end

	// sha round logic;
	// computes one round of SHA1;

	reg [1:0] sr_rndec;		// round decodes;
	wire [31:0] sr_w;			// W term;
	reg [31:0] sr_k;			// round k;

	always @(sr_rndec)
	begin
		case(sr_rndec)
			2'd0: sr_k <= 32'h5a827999;
			2'd1: sr_k <= 32'h6ed9eba1;
			2'd2: sr_k <= 32'h8f1bbcdc;
			2'd3: sr_k <= 32'hca62c1d6;
		endcase
	end

	// round terms a, b, c, d, e;

	wire [159:0] sr_in;			// input to sha round;
	wire [31:0] sr_a;			// term a;
	wire [31:0] sr_b;			// term b;
	wire [31:0] sr_c;			// term c;
	wire [31:0] sr_d;			// term d;
	wire [31:0] sr_e;			// term e;
	wire [31:0] sr_ash;			// a shifted;
	wire [31:0] sr_bsh;			// b shifted;

	assign { sr_a, sr_b, sr_c, sr_d, sr_e } = sr_in;
	assign sr_ash = { sr_a[26:0], sr_a[31:27] };
	assign sr_bsh = { sr_b[1:0], sr_b[31:2] };

	// compute round f term;
	// extract common terms;

	wire [31:0] sr_bc;			// b & c;
	wire [31:0] sr_bcd;			// b ^ c ^ d;
	reg [31:0] sr_f;			// f term;

	assign sr_bc = sr_b & sr_c;
	assign sr_bcd = sr_b ^ sr_c ^ sr_d;

	always @(sr_rndec or sr_b or sr_c or sr_d or sr_bc or sr_bcd)
	begin
		case(sr_rndec)
			2'd0: sr_f <= sr_bc | (~sr_b & sr_d);
			2'd1: sr_f <= sr_bcd;
			2'd2: sr_f <= sr_bc | (sr_b & sr_d) | (sr_c & sr_d);
			2'd3: sr_f <= sr_bcd;
		endcase
	end

	// final round result;
	// f and k take the longest to compute;
	// hold round state;
	// put most critical path first;

	wire [31:0] sr_sum;			// round sum of terms;
	wire [159:0] sr_out;		// result of sha round;
	reg sr_calc;				// calculate hash;
	reg sr_init;				// init round state;
	wire [159:0] hash_in;		// state to continue from;
	reg [159:0] sr_hash;		// round hash result;

	assign sr_sum = sr_ash + sr_e + sr_w + sr_f + sr_k;
	assign sr_out = { sr_sum, sr_a, sr_bsh, sr_c, sr_d };
	assign sr_in = sr_hash;

	always @(posedge clk)
	begin
		if(sr_calc)
			sr_hash <= sr_out;
		else if(sr_init)
			sr_hash <= hash_in;
	end

	// message input register;
	// need only one, independent of winner;
	// 64 bytes for message block;
	// produces W[0..79] terms in msg[511:480];
	// simple rotate left with W generation after 16 words;
	// msg_wt: -16 -14 -8 -3;
	// loading of next message cannot overlap calculation;

	wire [31:0] msg_in;			// word from sram;
	reg [511:0] msg;			// message buffer;
	reg msg_shift;				// shift;
	reg msg_load;				// load word & shift;
	reg msg_gen;				// compute W[t];
	wire [31:0] msg_wx;			// xor tersm of W[t];
	wire [31:0] msg_wt;			// generated W[t];

	assign msg_in = sram_rdata;
	assign msg_wx = msg[479:448] ^ msg[415:384] ^ msg[223:192] ^ msg[63:32];
	assign msg_wt = { msg_wx[30:0], msg_wx[31] };

	always @(posedge clk)
	begin
		if(msg_shift) begin
			msg[511:480] <= msg_gen? msg_wt : msg[479:448];
			msg[479:32] <= msg[447:0];
			msg[31:0] <= msg_load? msg_in : msg[511:480];
		end
	end

	assign sr_w = msg[511:480];

	// arbitrate sha core;
	// two requestors: DC, (MC | SHA) in order of priority;
	// arbitrate for one block of 64 bytes;
	// sm_buf needs full increment as if must cover 5 words;
	// sha_kill can stop MC or SHA requests, but not DC;

	wire arb_kill;			// kill arbiter;
	wire arb_idle;			// arbiter idle;
	reg [1:0] arb_win;		// arbitration winner;
	wire sm_busy;			// sha core busy;
	reg sm_done;			// sha core done;
	reg sm_save;			// save hash result to sram;
	reg sm_cmp;				// compare hash result with sram;
	reg [16:6] sm_ptr;		// ptr to data;
	reg sm_chain;			// chain hash operation;
	reg [16:2] sm_buf;		// winners result/cmp buffer;
	wire sm_inc;			// increment sm_buf;
	reg hash_sel;			// hash to operate on;

	assign arb_kill = sha_kill & arb_win[1];
	assign arb_idle = reset | sm_done | arb_kill;

	always @(posedge clk)
	begin
		if(arb_idle)
			arb_win <= 2'b00;
		else if(~sm_busy) begin
			arb_win[0] <= dc_req;
			arb_win[1] <= ~dc_req & sha_req;
			sm_save <= dc_req? 1'b0 : sha_save;
			sm_cmp <= dc_req? dc_cmp : 1'b0;
			sm_ptr <= dc_req? dc_ptr : sha_ptr;
			sm_chain <= dc_req? dc_chain : sha_chain;
			sm_buf <= dc_req? dc_buf : sha_buf;
		end else if(sm_inc)
			sm_buf <= sm_buf + 1;
		hash_sel <= arb_win[0];
	end

	assign sm_busy = |arb_win;

	// sha core state machine;
	// operates on one block of 64 bytes;
	// load 16 words from sram, hash, write-back or compare;
	// read latency is part of state count;
	//	0..15		load data words;
	//	16..19		read data latency;
	//	20..99		80 rounds of hash calculations;
	//	100			final round add, done if not save/cmp;
	//	101..105	save result, read for compare;
	//	112			done if save/cmp;

	reg [6:0] sm;			// state count;
	wire sm_next;			// advance sm state;
	reg sm_calc;			// calulate hash, one clk ahead;
	wire sm_init;			// init hash state, then start;
	reg sm_cstart;			// begin hash calculation;
	wire sm_cend;			// end hash calculation;
	reg sm_gen;				// generate W[t] after 16 words;
	wire sm_gstart;			// first generated W[t];
	reg sm_add;				// do final sha add in next clock;
	wire sm_end;			// done for save/cmp;
	wire sm_sc;				// save/compare to do;
	reg dc_ack;				// sha has consumed dc data;

	assign sm_sc = sm_save | sm_cmp;

	always @(posedge clk)
	begin
		if(~sm_busy)
			sm <= 7'd0;
		else if(sm_next)
			sm <= sm + 1;
		sm_cstart <= sm_init;
		sm_calc <= sm_busy & ~sm_cend & (sm_calc | sm_cstart);
		sm_gen <= sm_busy & ~sm_cend & (sm_gen | sm_gstart);
		sm_add <= sm_cend;
		sm_done <= sm_sc? sm_end : sm_add;
		dc_ack <= arb_win[0] & sm_cstart;
	end

	assign sm_init = (sm == 7'd17);
	assign sm_gstart = (sm == 7'd33);
	assign sm_cend = (sm == 7'd98);
	assign sm_end = (sm[6:4] == 3'b111);

	// read sram for sha input;
	// read 16 words from aligned 64-byte buffer;
	// sm states 0..15;
	// lower 4 bits make offset in sram buffer;

	wire sm_read;			// read words;

	assign sm_read = sm_busy & (sm[6:4] == 3'd0);

	// sram signals used by sm;

	wire sram_next;			// got work from sram;
	wire sram_ack;			// sram ack;
	wire sram_rack;			// read data ack;
	wire sram_wack;			// write data ack;

	// message/state controls;
	// initialize round state for requestor;
	// shift in 16 words;
	// generate W[t] for words >= 16;

	reg msg_rack;			// ack one msg word;

	always @(posedge clk)
	begin
		sr_calc <= sm_calc;
		sr_init <= sm_cstart;
		msg_shift <= msg_rack | sm_calc;
		msg_load <= msg_rack;
		msg_gen <= sm_gen;
	end

	// round decodes;
	// 0=0..19, 1=20..39, 2=40..59, 3=60..79;
	// +20 for sm states;
	// implemented as counter that increments every 20;

	wire sr_rndinc;			// next block of rounds;

	assign sr_rndinc = (sm == 7'd39) | (sm == 7'd59) | (sm == 7'd79);

	always @(posedge clk)
	begin
		if(sm_busy == 1'b0)
			sr_rndec <= 2'b00;
		else if(sr_rndinc)
			sr_rndec <= sr_rndec + 1;
	end

	// hash buffers per requestor;
	// keep in flops as saving to sram is too expensive;
	// work with hash state of arb winner;
	// final round is 5 individual 32-bit adds;

	reg [159:0] hash_dc;	// dc hash;
	reg [159:0] hash_sha;	// sha/mc hash;
	reg [1:0] hash_init;	// init chain for new op;
	reg [1:0] hash_add;		// final add to new state;
	wire [159:0] hash_sum;	// final round sum;

	always @(posedge clk)
	begin
		hash_init[0] <= sm_init & ~sm_chain & arb_win[0];
		hash_init[1] <= sm_init & ~sm_chain & arb_win[1];
		hash_add[0] <= sm_add & arb_win[0];
		hash_add[1] <= sm_add & arb_win[1];
		if(hash_init[0])
			hash_dc <= 160'h67452301efcdab8998badcfe10325476c3d2e1f0;
		else if(hash_add[0])
			hash_dc <= hash_sum;
		if(hash_init[1])
			hash_sha <= 160'h67452301efcdab8998badcfe10325476c3d2e1f0;
		else if(hash_add[1])
			hash_sha <= hash_sum;
	end

	assign hash_in = hash_sel? hash_dc : hash_sha;

	assign hash_sum[159:128] = hash_in[159:128] + sr_hash[159:128];
	assign hash_sum[127:96] = hash_in[127:96] + sr_hash[127:96];
	assign hash_sum[95:64] = hash_in[95:64] + sr_hash[95:64];
	assign hash_sum[63:32] = hash_in[63:32] + sr_hash[63:32];
	assign hash_sum[31:0] = hash_in[31:0] + sr_hash[31:0];

	// hash word mux;
	// 5->1 mux selects one word of hash result;
	// split as one full 4->1 mux and 2->1 mux;
	// used by both the write-back and compare logic;
	// must buffer hash_word in flop for loading;

	wire [2:0] hash_wsel;	// hash word selects;
	reg [31:0] hash_mux;	// muxed lower 4 words;
	reg [31:0] hash_word;	// selected word;

	always @(hash_wsel[1:0] or hash_in[127:0])
	begin
		case(hash_wsel)
			2'd0: hash_mux <= hash_in[127:96];
			2'd1: hash_mux <= hash_in[95:64];
			2'd2: hash_mux <= hash_in[63:32];
			2'd3: hash_mux <= hash_in[31:0];
		endcase
	end

	always @(posedge clk)
	begin
		if(sram_next)
			hash_word <= hash_wsel[2]? hash_in[159:128] : hash_mux;
	end

	assign sram_wdata = hash_word;

	// write/read sram for save/cmp operation;
	// always 5 words for 20 byte hash result;
	// word selects for cmps lags write by 2 clocks;

	wire sm_scx;			// save/cmp states;
	wire sm_screq;			// sram request for save/cmp;
	reg [2:0] sc_wsel;		// hash word select for writes;
	reg [2:0] sc_wdel;		// delayed sc_wsel;
	reg [2:0] sc_rsel;		// hash word select for reads;

	assign sm_scx = (sm == 7'd101) | (sm == 7'd102)
		| (sm == 7'd103) | (sm == 7'd104) | (sm == 7'd105);
	assign sm_screq = sm_sc & sm_scx;

	always @(posedge clk)
	begin
		if(sm_scx == 1'b0)
			sc_wsel <= 3'b111;
		else if(sram_next)
			sc_wsel <= sc_wsel + 1;
		sc_wdel <= sc_wsel;
		sc_rsel <= sc_wdel;
	end

	assign hash_wsel = sm_save? sc_wsel : sc_rsel;

	// compare hash result with words from sram;
	// only for dc as triggered by dc_cmp;

	reg hash_weq;			// hash word equal sram word;
	reg [1:0] hash_rack;	// read ack for cmp data;

	always @(posedge clk)
	begin
		if(sm_busy == 1'b0)
			hash_weq <= 1'b1;
		else if(hash_rack[1])
			hash_weq <= hash_weq & (sram_rdata == hash_word);
	end

	assign dc_fail = ~hash_weq;

	// advance sha block state machine;
	// the only stall source is the sram;

	assign sm_next = sram_next;

	// done signals to units;
	// depend on arb winner;

	assign dc_busy = arb_win[0];
	assign sha_inc = sm_done & arb_win[1];
	assign sha_done = sha_inc & sha_last;
	assign mc_done = sha_done & sha_mc;

	// sram access controller;
	// keep request pending until ack;
	// advance address on acks;

	wire sm_req;			// sm sram request;
	reg sram_pend;			// pending sram request;
	reg [16:2] sram_addr;	// sram access address;
	reg sram_we;			// sram write-enable;
	reg sram_rmsg;			// read message;
	reg sram_sc;			// save/cmp hash;

	assign sm_req = sm_read | sm_screq;
	assign sram_req = sm_req | (sram_pend & ~sram_gnt);
	assign sram_next = ~sram_pend | sram_gnt;

	always @(posedge clk)
	begin
		sram_pend <= sm_busy & sram_req;
		if(sram_next) begin
			sram_addr <= sm_read? { sm_ptr, sm[3:0] } : sm_buf;
			sram_we <= sm_save & sm_screq;
			sram_rmsg <= sm_read;
			sram_sc <= sm_screq;
		end
		msg_rack <= sram_rack & sram_rmsg;
		hash_rack <= { hash_rack[0], sram_rack & sram_sc };
	end

	assign sram_ack = sram_gnt & sram_pend;
	assign sram_rack = sram_ack & ~sram_we;
	assign sram_wack = sram_ack & sram_we;
	assign sm_inc = sm_screq & sram_next;

	// sha interrupt handling;
	// stopping clears the mask and interrupt;

	reg sha_intr;			// intr flag;
	wire sha_intr_clr;		// clear interrupt;
	wire sha_intr_set;		// issue interrupt;

	assign sha_intr_clr = reset | sha_kill;
	assign sha_intr_set = sha_intr_mask & sha_done;

	always @(posedge clk) 
	begin
		if(sha_intr_clr) 
			sha_intr_mask <= 1'b0;
		else if(sha_start)
			sha_intr_mask <= reg_wdata[30];
		sha_intr <= ~sha_intr_clr & (sha_intr | sha_intr_set);
	end

	// simulator init;
	// fill reg bits to inverse of reset defaults;
	// fill undefined bits with randoms;
	// needed, so no Xs move across PCI (parity);

`ifdef	SIM
	initial
	begin
		{ sha_size, sha_save, sha_ptr, sha_chain } = $random;
		sha_buf = $random;
	end
`endif	// SIM

endmodule

