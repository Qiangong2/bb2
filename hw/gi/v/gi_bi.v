// gi_bi.v Frank Berndt
// bus interface unit;
// :set tabstop=4

// address space;
// gi needs 18 bits (256kB) of space to fit:
//	00000 ... 17fff    SRAM, 96kB;
//	18000 ... 1bfff    ROM, up to 16kB;
//	1c000 ... 1dfff    virage, up to 8kB;
//	1e000 ... 1ffff    registers;
//	..0..              BI;
//	..1..              DI;
//	..2..              AES;
//	..3..              SHA1;
//	..4..              GI;
//	..5..              AIS;

// dma protocol;
// gi asserts dma_req and keeps it asserted;
// bus takes dma_addr and dma_wr to request dma service;
// bus interface generates sys_* cycles with sys_cmd[2]=1;
// bus interface asserts dma_done for one or more clocks;
// bus interface asserts dma_retry for one or more clocks;
// bus interface asserts dma_err for one or more clocks;
// gi deasserts dma_req;
// if gi deasserts dma_req before dma_done, then the request
// is considered cancelled; the bus interface asserts dma_done
// in next clock until all ongoing sys_* cycles are flushed;
// dma_done asserted delays issuance of next dma request;

module gi_bi (
	clk, reset, alt_boot, soft_reset, test_in, test_ena,
	sys_addr, sys_cmd, sys_in, sys_out, sys_intr,
	dma_req, dma_bx, dma_addr, dma_wr, dma_done, dma_retry, dma_err,
	reg_sel, reg_addr, reg_we, reg_wdata, reg_rdata,
	sram_req, sram_gnt, sram_addr, sram_we, sram_wdata, sram_rdata,
	rom_addr, rom_rdata,
	v_addr, v_me, v_we, v_wdata, v_rdata,
	secure, trap_gi,
	ais_req, ais_mem, ais_buf, ais_dma, ais_done, ais_abort, ais_err,
	mc_req, mc_wr, mc_mem, mc_buf, mc_dma, mc_done, mc_abort, mc_err,
	dc_req, dc_wr, dc_mem, dc_buf, dc_dma, dc_done, dc_abort, dc_err,
	di_intr, ais_intr, aes_intr, mc_intr, sha_intr, dc_intr
);
	input clk;					// core clock;
	input reset;				// system reset;
	input alt_boot;				// alternative boot;
	output soft_reset;			// soft reset, single clock;
	input test_in;				// from test enable pin on engr chips;
	output test_ena;			// enable test/debug logic;

	// interface to system;

	input [17:2] sys_addr;		// cpu address;
	input [2:0] sys_cmd;		// bus command;
	input [31:0] sys_in;		// write data, cpu and dma;
	output [31:0] sys_out;		// read data. cpu and dma;
	output sys_intr;			// interrupt to system;
	output dma_req;				// request a dma transfer;
	output [1:0] dma_bx;		// burst control, [1]=idx, [0] 0=64 1=128;
	output [27:6] dma_addr;		// dma physical address;
	output dma_wr;				// dma write req to memory;
	input dma_done;				// dma done;
	input dma_retry;			// retry dma;
	input [2:0] dma_err;		// dma errors;

	// register interface;

	output [6:0] reg_sel;		// unit selects;
	output [7:0] reg_addr;		// register addr decodes;
	output reg_we;				// register write enable;
	output [31:0] reg_wdata;	// register write data;
	input [31:0] reg_rdata;		// register read data;

	// interface to sram arbiter;

	output sram_req;			// sram request;
	input sram_gnt;				// sram grant;
	output [16:2] sram_addr;	// sram address;
	output sram_we;				// sram write enable;
	output [31:0] sram_wdata;	// sram write data;
	input [31:0] sram_rdata;	// sram read data;

	// rom interface;

	output [12:2] rom_addr;		// rom address;
	input [31:0] rom_rdata;		// rom read data;

	// virage interface;

	output [9:2] v_addr;		// virage address;
	output v_me;				// virage access enable;
	output v_we;				// virage write enable;
	output [31:0] v_wdata;		// virage write data;
	input [31:0] v_rdata;		// virage read data;

	// secure environment;

	output secure;				// in secure mode;
	input trap_gi;				// secure trap from gi;

	// ais dma interface;
	// ais never writes;

	input ais_req;				// ais dma request;
	input [27:6] ais_mem;		// ais dma address;
	input [16:6] ais_buf;		// dma buffer in sram;
	output ais_dma;				// dma busy for ais;
	output ais_done;			// ais dma done;
	output ais_abort;			// ais dma aborted;
	output [2:0] ais_err;		// ais dma error;

	// mc dma interface;

	input mc_req;				// mc dma request;
	input mc_wr;				// mc dma write;
	input [27:6] mc_mem;		// mc dma address;
	input [16:6] mc_buf;		// dma buffer in sram;
	output mc_dma;				// dma busy for mc;
	output mc_done;				// mc dma done;
	output mc_abort;			// mc dma aborted;
	output [2:0] mc_err;		// mc dma error;

	// dc dma interface, only reads;

	input dc_req;				// dc dma request;
	input dc_wr;				// dc dma write;
	input [27:7] dc_mem;		// gc dma address;
	input [16:7] dc_buf;		// dma buffer in sram;
	output dc_dma;				// dma busy for dc;
	output dc_done;				// dc dma done;
	output dc_abort;			// mc dma aborted;
	output [2:0] dc_err;		// mc dma error;

	// misc;

	input di_intr;				// di interrupt;
	input ais_intr;				// ais interrupt;
	input [1:0] aes_intr;		// aes en-/decrypt interrupts;
	input mc_intr;				// mc interrupt;
	input sha_intr;				// sha interrupt;
	input dc_intr;				// dc interrupt;

	// important bits;

	reg [6:0] sec_mode;			// secure mode register;

	assign secure = sec_mode[0];
	assign test_ena = sec_mode[5];

	// bus interface;
	// assume that bi inputs have to go into flops
	// and that outputs must come from flops;

	reg [17:2] r_addr;			// address register;
	reg [2:0] r_cmd;			// cmd register;
	reg [31:0] r_in;			// data input register;
	reg [31:0] sys_out;			// data ouptut register;
	wire cmd_read;				// a read command;
	wire cmd_write;				// a write command;
	wire cmd_rw;				// read or write command;
	wire [31:0] rdata;			// read response data;

	always @(posedge clk)
	begin
		r_addr <= sys_addr;
		r_cmd <= sys_cmd;
		r_in <= sys_in;
		sys_out <= rdata;
	end

	assign cmd_read = (r_cmd[1:0] == 2'b01);
	assign cmd_write = (r_cmd[1:0] == 2'b10);
	assign cmd_rw = |r_cmd[1:0];

	// decode major address spaces;
	// let smaller memories be mirrored;
	// make regs h10 apart to avoid prefetch issues
	// on reg reads to regs with side effects;

	wire spc_flip;				// flip spaces;
	wire spc_rom;				// rom space;
	wire spc_sram;				// sram space;
	wire spc_v;					// virage space;
	wire spc_reg;				// register space addressed;

	assign spc_flip = ~r_addr[16] | (r_addr[16:15] == 2'b10);
	assign spc_rom = (sec_mode[1] ^ r_addr[17]) & spc_flip;
	assign spc_sram = (sec_mode[1] ^ ~r_addr[17]) & spc_flip;
	assign spc_v = (r_addr[16:14] == 3'b110);
	assign spc_reg = (r_addr[16:14] == 3'b111) & (r_addr[3:2] == 2'd0);

	// decode fetch from secure entry;
	// both reset and debug excp vector;

	wire fetch_addr;			// address decode for fetch;
	wire sec_fetch;				// fetch from exception vector;

	assign fetch_addr = (r_addr == 16'h0) | (r_addr == 16'h0120); //XXX single
	assign sec_fetch = ~secure & cmd_read & fetch_addr;

	// decode register space;
	// registers are aligned to 32-bit addresses;
	// three address bits are given to units for sub-decoding;
	// delay write to line up with write data;

	wire reg_io;				// register io;
	wire [7:0] reg_adec;		// reg address decodes;
	reg [6:0] reg_sel;			// per-unit selects;
	reg [7:0] reg_addr;			// sub-address;
	reg reg_we;					// write enable;

	assign reg_io = spc_reg & ~r_cmd[2] & cmd_rw;
	assign reg_wdata = r_in;

	assign reg_adec[0] = (r_addr[6:4] == 3'd0);
	assign reg_adec[1] = (r_addr[6:4] == 3'd1);
	assign reg_adec[2] = (r_addr[6:4] == 3'd2);
	assign reg_adec[3] = (r_addr[6:4] == 3'd3);
	assign reg_adec[4] = (r_addr[6:4] == 3'd4);
	assign reg_adec[5] = (r_addr[6:4] == 3'd5);
	assign reg_adec[6] = (r_addr[6:4] == 3'd6);
	assign reg_adec[7] = (r_addr[6:4] == 3'd7);

	always @(posedge clk)
	begin
		reg_sel[0] <= reg_io & (r_addr[11:8] == 3'd0);
		reg_sel[1] <= reg_io & (r_addr[11:8] == 3'd1);
		reg_sel[2] <= reg_io & (r_addr[11:8] == 3'd2);
		reg_sel[3] <= reg_io & (r_addr[11:8] == 3'd3);
		reg_sel[4] <= reg_io & (r_addr[11:8] == 3'd4);
		reg_sel[5] <= reg_io & (r_addr[11:8] == 3'd5);
		reg_sel[6] <= reg_io & (r_addr[11:8] == 3'd6);
		reg_addr <= reg_adec;
		reg_we <= cmd_write;
	end

	// buffer address for sram;
	// due to load and distance;
	// rom and virage get address one clock earlier;

	reg [16:2] mem_addr;		// memory address;
	reg mem_we;					// memory write enable;
	wire [16:2] dma_buf;		// dma sram pointer;

	always @(posedge clk)
	begin
		mem_addr <= r_cmd[2]? dma_buf : r_addr;
		mem_we <= cmd_write;
	end

	// requests to sram must go through the sram arbiter;
	// fast sram access is the performance target;
	// then use the same timing for ROM and virage;
	// cpu and dma data movements go through the same door;

	assign sram_req = cmd_rw & (r_cmd[2]? 1'b1 : spc_sram);
	assign sram_addr = mem_addr;
	assign sram_we = mem_we;
	assign sram_wdata = r_in;

	// rom and virage are faster by one clock,
	// because they don't go through sram arbiter;
	// the extra clock is consumed in the read data path;
	// for alternative boot, read the rom backwards;
	// only allow this when test_in pin is active;
	// on production chips test_in is inactive,
	// the value of the alt_boot pin can be read from
	// SEC_MODE[6], so software can do a late branch
	// to the ALI code in external flash;

	wire alt;					// boot rom backwards;

	assign alt = &sec_mode[6:5];
	assign rom_addr = alt? ~r_addr[12:2] : r_addr[12:2];

	// virage interface;
	// allow access only in secure mode;
	// and only for pio or cacheline requests, not dma;

	assign v_addr = r_addr[9:2];
	assign v_me = secure & ~r_cmd[2] & cmd_rw & spc_v;
	assign v_we = cmd_write;
	assign v_wdata = r_in;

	// system read data select;
	// cpu read response data or dma->mem write data;
	// highest priority is sram, then rom, virage and reg data;
	// rom and virage are only accessible in secure mode;
	// let dma only access the sram;

	reg [1:0] sel_sram;			// select sram read data;
	reg sel_rom;				// select rom read data;
	reg [31:0] r_rdata;			// rom registered read data;
	reg [31:0] bi_rdata;		// bi reg read data;
	wire [31:0] o_rdata;		// rom/v/reg read data;
	wire sec_rom;				// select rom in secure mode;

	always @(posedge clk)
	begin
		sel_sram[0] <= sram_req;
		sel_sram[1] <= sel_sram[0];
		sel_rom <= ~r_cmd[2] & spc_rom;
		r_rdata <= sec_rom? rom_rdata : 32'd0;
	end

	assign sec_rom = secure & sel_rom;
	assign o_rdata = r_rdata | reg_rdata | bi_rdata | v_rdata;
	assign rdata = sel_sram[1]? sram_rdata : o_rdata;

	// decode bi register spaces;
	// bi uses register select 0;

	wire ra_bi_ctrl;
	wire ra_bi_mem;
	wire ra_bi_intr;
	wire ra_sec_mode;
	wire ra_sec_timer;
	wire ra_sec_sram;
	wire ra_sec_enter;

	assign ra_bi_ctrl = reg_sel[0] & reg_addr[0];
	assign ra_bi_mem = reg_sel[0] & reg_addr[1];
	assign ra_bi_intr = reg_sel[0] & reg_addr[2];
	assign ra_sec_mode = reg_sel[0] & reg_addr[4];
	assign ra_sec_timer = reg_sel[0] & reg_addr[5];
	assign ra_sec_sram = reg_sel[0] & reg_addr[6];
	assign ra_sec_enter = reg_sel[0] & reg_addr[7];

	// bi control register;
	// drop cpu writes when gc is busy;

	wire bi_ctrl_sel;			// read BI_CTRL;
	wire bi_ctrl_wr;			// write BI_CTRL;
	reg bi_kill;				// cpu kills bi request;
	wire bi_finish;				// bi finished without errors;
	wire bi_stop;				// stop bi dma;
	reg bi_start;				// start bi dma burst;
	wire bi_busy;				// bi busy with request/dma;
	reg bi_req;					// bi requests system interface;
	reg bi_done;				// dma burst done;
	reg bi_abort;				// dma aborted due to error;
	reg bi_intr_mask;			// interrupt mask;
	reg bi_intr;				// interrupt flag;
	reg [29:20] bi_size;		// size in blocks - 1;
	reg [16:7] bi_ptr;			// bi dma sram pointer;
	reg bi_last;				// size is 0;
	reg bi_wr;					// dma to memory;
	reg [2:0] bi_err;			// bi dma error;
	wire [31:0] bi_ctrl;		// all bits;
	wire bi_dma;				// dma busy for bi;

	assign bi_ctrl_sel = ra_bi_ctrl;
	assign bi_ctrl_wr = bi_ctrl_sel & reg_we & ~bi_busy;
	assign bi_finish = bi_done & bi_last;
	assign bi_stop = reset | bi_finish | bi_kill | bi_abort;
	assign bi_busy = bi_req | bi_dma;

	always @(posedge clk)
	begin
		bi_start <= bi_ctrl_wr & reg_wdata[31];
		bi_kill <= bi_ctrl_sel & reg_we & ~reg_wdata[31];
		bi_req <= ~bi_stop & (bi_req | bi_start);
		if(bi_ctrl_wr) begin
			bi_size <= reg_wdata[29:20];
			bi_wr <= reg_wdata[19];
		end else if(bi_done)
			bi_size <= bi_size - 1;
		bi_last <= (bi_size == 8'd0);
		if(bi_start)
			bi_err <= 3'd0;
		else if(bi_abort)
			bi_err <= dma_err;
	end

	assign bi_ctrl = { bi_busy, bi_intr_mask,
		bi_size, bi_wr, 2'd0, bi_ptr, 4'd0, bi_err };

	// dma sram pointer handling;
	// loaded by cpu write and for every gc request;

	wire bi_ptr_inc;			// increment sram pointer;

	always @(posedge clk)
	begin
		if(bi_ctrl_wr)
			bi_ptr <= reg_wdata[16:7];
		else if(bi_ptr_inc)
			bi_ptr <= bi_ptr + 1;
	end

	// bi interrupt handling;
	// clear mask on reset or when killed;
	// load from cpu write data;
	// clear for gc requests;

	wire bi_intr_clr;		// clear bi interrupt mask;
	wire bi_intr_set;		// raise bi interrupt;

	assign bi_intr_clr = reset | bi_kill;
	assign bi_intr_set = bi_intr_mask & (bi_finish | bi_abort);

	always @(posedge clk)
	begin
		if(bi_intr_clr)
			bi_intr_mask <= 1'b0;
		else if(bi_ctrl_wr)
			bi_intr_mask <= reg_wdata[30];
		if(bi_intr_clr)
			bi_intr <= 1'b0;
		else if(bi_intr_set)
			bi_intr <= 1'b1;
	end

	// bi memory address register;
	// the dma pointer in physical main memory;
	// the two sources are cpu and gi;
	// cpu writes are dropped when unit is busy;
	// gi only needs to load on new sq boundary;
	// increments for next request;

	wire bi_mem_sel;			// cpu read from BI_MEM;
	wire bi_mem_wr;				// cpu write to BI_MEM;
	wire bi_mem_inc;			// increment block address;
	reg [27:7] bi_mem;			// BI_MEM;

	assign bi_mem_sel = ra_bi_mem;
	assign bi_mem_wr = bi_mem_sel & reg_we & ~bi_busy;

	always @(posedge clk)
	begin
		if(bi_mem_wr)
			bi_mem <= reg_wdata[27:7];
		else if(bi_mem_inc)
			bi_mem <= bi_mem + 1;
	end

	// dma arbiter;
	// requestors: ais, dc, bi, mc; in order of priority;
	// ais is low-frequency event, so priority can be highest;
	// assertion of dma_done or dma_retry resets the arbiter;
	// dma_retry will cause rearbitration;
	// look at top of file for bus dma details;
	// units kill request by deasserting their *_req before done;

	wire dma_abort;				// dma aborted with error;
	wire dma_stop;				// stop dma due to done or error;
	wire dma_clr;				// clear the dma arbiter;
	reg dma_da;					// delay dma arbitration;
	wire [1:0] dma_arb;			// arbitration winner;
	wire dma_pend;				// dma request pending;
	reg [1:0] dma_win;			// dma winner;
	reg dma_busy;				// dma is busy;
	reg ais_done;				// ais dma done;
	reg ais_abort;				// abort ais dma;
	reg dc_done;				// dc dma done;
	reg dc_abort;				// abort dc dma;
	reg mc_done;				// mc dma done;
	reg mc_abort;				// abort mc dma;

	assign dma_abort = |dma_err;
	assign dma_stop = dma_done | dma_abort;
	assign dma_clr = reset | dma_stop | dma_retry;
	assign dma_arb[0] = ~ais_req & (dc_req | (~bi_req & mc_req));
	assign dma_arb[1] = ~ais_req & ~dc_req & (bi_req | mc_req);
	assign dma_pend = ais_req | dc_req | bi_req | mc_req;

	assign ais_dma = dma_busy & (dma_win == 2'd0);
	assign dc_dma = dma_busy & (dma_win == 2'd1);
	assign bi_dma = dma_busy & (dma_win == 2'd2);
	assign mc_dma = dma_busy & (dma_win == 2'd3);

	always @(posedge clk)
	begin
		dma_da <= dma_clr;
		if(dma_clr | dma_da) begin
			dma_busy <= 1'b0;
			dma_win <= 2'd0;
		end else if(~dma_busy) begin
			dma_busy <= dma_pend;
			dma_win <= dma_arb;
		end
		ais_done <= ais_dma & dma_done;
		ais_abort <= ais_dma & dma_abort;
		dc_done <= dc_dma & dma_done;
		dc_abort <= dc_dma & dma_abort;
		bi_done <= bi_dma & dma_done;
		bi_abort <= bi_dma & dma_abort;
		mc_done <= mc_dma & dma_done;
		mc_abort <= mc_dma & dma_abort;
	end

	assign dma_req = dma_busy;
	assign bi_mem_inc = bi_done;
	assign bi_ptr_inc = bi_done;
	assign ais_err = dma_err;
	assign mc_err = dma_err;
	assign dc_err = dma_err;

	// dma request mux;
	// sends winner on to bus interface;
	// ais always issues 128-byte bursts;
	// mc always issues 64-byte bursts;

	reg [27:6] dma_addr;	// dma memory address;
	reg dma_wr;				// dma write to memory;
	reg [16:6] dma_ptr;		// dma index in sram;

	always @(dma_win or ais_mem or bi_mem or dc_mem or mc_mem)
	begin
		case(dma_win)
			2'b00: dma_addr <= ais_mem;
			2'b01: dma_addr <= { dc_mem, 1'b0 };
			2'b10: dma_addr <= { bi_mem, 1'b0 };
			2'b11: dma_addr <= mc_mem;
		endcase
	end

	always @(dma_win or dc_wr or bi_wr or mc_wr)
	begin
		case(dma_win)
			2'b00: dma_wr <= 1'b0;
			2'b01: dma_wr <= dc_wr;
			2'b10: dma_wr <= bi_wr;
			2'b11: dma_wr <= mc_wr;
		endcase
	end

	assign dma_bx[0] = (dma_win != 2'b11);

	always @(dma_win or ais_buf or dc_buf or bi_ptr or mc_buf)
	begin
		case(dma_win)
			2'b00: dma_ptr <= ais_buf;
			2'b01: dma_ptr <= { dc_buf, 1'b0 };
			2'b10: dma_ptr <= { bi_ptr, 1'b0 };
			2'b11: dma_ptr <= mc_buf;
		endcase
	end

	// dma buffer address;
	// buffer index is loaded when winner is known;
	// have to buffer in flops due to latency of dma writes;
	// upper layer supplies word index for dma cycles;

	reg [16:6] dma_idx;			// dma buffer index;
	reg dma_iload;				// load buffer index of dma winner;

	always @(posedge clk)
	begin
		dma_iload <= ~dma_busy & dma_pend;
		if(dma_iload)
			dma_idx <= dma_ptr;
	end

	assign dma_bx[1] = dma_idx[6];
	assign dma_buf[16:7] = dma_idx[16:7];
	assign dma_buf[6:2] = r_addr[6:2];

	// secure mode register;
	// accesible only in secure mode;
	// force secure mode on at risign edge of reset,
	// when test_in or alt_boot is active;
	// alt_boot automatically drops into secure mode;

	wire sec_mode_sel;			// read SEC_MODE;
	wire sec_mode_wr;			// write SEC_MODE;
	wire [4:2] sec_trig;		// secure mode triggers;
	wire sec_trap;				// secure trap pending;
	wire trap_timer;			// secure timer trap;
	wire trap_app;				// application trap;
	wire sec_mode_enter;		// enter secure mode;
	wire sec_mode_force;		// force secure mode;

	assign sec_mode_sel = secure & ra_sec_mode;
	assign sec_mode_wr = sec_mode_sel & reg_we;
	assign trap_app = ~secure & ~reg_we & ra_sec_enter;
	assign soft_reset = reg_we & ra_sec_enter;
	assign sec_trig[4] = ~secure & trap_gi;
	assign sec_trig[3] = ~secure & trap_timer;
	assign sec_trig[2] = ~secure & trap_app;
	assign sec_trap = |sec_mode[4:2];
	assign sec_mode_enter = (sec_mode[1] | sec_trap) & sec_fetch;
	assign sec_mode_force = test_in;

	always @(posedge clk or posedge reset)
	begin
		if(reset) begin
			sec_mode <= { alt_boot, test_in, 4'b0001, sec_mode_force };
		end else if(sec_mode_wr) begin
			sec_mode[6] <= sec_mode[6] ^ reg_wdata[6];
			sec_mode[5] <= sec_mode[5] ^ reg_wdata[5];
			sec_mode[4] <= reg_wdata[4]? sec_mode[4] : sec_trig[4];
			sec_mode[3] <= reg_wdata[3]? sec_mode[3] : sec_trig[3];
			sec_mode[2] <= reg_wdata[2]? sec_mode[2] : sec_trig[2];
			sec_mode[1:0] <= reg_wdata[1:0];
		end else begin
			sec_mode[4:2] <= sec_mode[4:2] | sec_trig[4:2];
			sec_mode[0] <= sec_mode[0] | sec_mode_enter;
		end
	end

	// secure timer;
	// sec_div divides clk by 2^20+1, then decrments timer;
	// both divider and timer are cleared on cpu writes;
	// trigger timer when it rolls fom 1 to 0,
	// but not when 0 is written by cpu;

	wire sec_timer_sel;			// select SEC_TIMER;
	wire sec_timer_wr;			// write SEC_TIMER reg;
	wire sec_timer_dec;			// decrment timer;
	wire sec_div_clr;			// clear pre-scaler;
	reg [20:0] sec_div;			// pre-scaler;
	reg sec_timer_rst;			// reset on secure timer event;
	reg [23:0] sec_timer_val;	// secure timer counter;
	reg sec_timer_nz;			// upper bits are non-0;

	assign sec_timer_sel = secure & ra_sec_timer;
	assign sec_timer_wr = sec_timer_sel & reg_we;
	assign sec_div_clr = reset | sec_timer_wr | sec_div[20];
	assign sec_timer_dec = sec_div[20] & (sec_timer_nz | sec_timer_val[0]);
	assign trap_timer = sec_div[20] & ~sec_timer_nz & sec_timer_val[0];

	always @(posedge clk)
	begin
		if(sec_div_clr)
			sec_div <= 'd0;
		else
			sec_div <= sec_div + 1;
		if(reset) begin
			sec_timer_rst <= 1'b0;
			sec_timer_val <= 'd0;
		end else if(sec_timer_wr) begin
			sec_timer_rst <= reg_wdata[24];
			sec_timer_val <= reg_wdata[23:0];
		end else if(sec_timer_dec)
			sec_timer_val <= sec_timer_val - 1;
		sec_timer_nz <= |sec_timer_val[23:1];
	end
	//XXX timer triggers reset;

	// secure sram end;
	// only accessible in secure mode;
	// configures a region from SEC_SRAM to upper end
	// to be accessible in non-secure mode;
	// it is up to software to put sensitive buffers below SEC_SRAM;
	// force alignment to 16 bytes;
	// cleared to 0 during reset;

	wire sec_sram_z;			// zero SEC_SRAM;
	wire sec_sram_sel;			// read SEC_SRAM;
	wire sec_sram_wr;			// write SEC_SRAM;
	reg [16:4] sec_sram;		// SEC_SRAM;

	assign sec_sram_z = reset; // XXX debug mode;
	assign sec_sram_sel = secure & ra_sec_sram;
	assign sec_sram_wr = sec_sram_sel & reg_we;

	always @(posedge clk)
	begin
		if(sec_sram_z)
			sec_sram <= 'd0;
		else if(sec_sram_wr)
			sec_sram <= reg_wdata[16:4];
	end

	// sum up all interrupt source;

	wire [6:0] intr;

	assign intr = { dc_intr, sha_intr, mc_intr, aes_intr,
		ais_intr, di_intr, bi_intr };
	assign sys_intr = |intr;

	// bi register read mux;
	// only muxes make it hard to include secure gating;
	// output 0 when not selected;
	// next stage will OR data from all the units;

	always @(posedge clk)
	begin
		bi_rdata <= ({32{bi_ctrl_sel}} & bi_ctrl)
			| ({32{bi_mem_sel}} & { 4'd0, bi_mem, 7'd0 })
			| ({32{ra_bi_intr}} & { 26'd0, intr })
			| ({32{sec_mode_sel}} & { 25'd0, sec_mode })
			| ({32{sec_timer_sel}} & { 7'd0, sec_timer_rst, sec_timer_val })
			| ({32{sec_sram_sel}} & { 15'd0, sec_sram, 4'd0 });
	end

	// simulator init;
	// fill reg bits to inverse of reset defaults;
	// fill undefined bits with randoms;
	// needed, so no Xs move across PCI (parity);

`ifdef	SIM
	initial
	begin
		bi_req = 1'b1;
		bi_intr_mask = 1'b1;
		{ bi_size, bi_wr, bi_ptr, bi_err } = $random;
		bi_mem = $random;
		bi_intr = {32{1'b1}};
		bi_abort = 1'b0;
		sec_mode = { $random, 5'b11100 };
		sec_timer_rst = 1;
		sec_timer_val = {24{1'b1}};
		sec_sram = { $random };
	end
`endif	// SIM

endmodule

