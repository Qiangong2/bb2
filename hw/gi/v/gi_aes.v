// gi_aes.v Frank Berndt
// aes en-/decryption unit;
// :set tabstop=4

module gi_aes (
	clk, reset, secure,
	reg_sel, reg_addr, reg_we, reg_wdata, reg_rdata,
	sram_req, sram_gnt, sram_addr, sram_we, sram_wdata, sram_rdata,
	dc_req, dc_chain, dc_iv, dc_ptr, dc_done,
	md_req, md_chain, md_size, md_ptr, md_done, md_dckey,
	me_req, me_chain, me_size, me_ptr, me_done,
	aes_intr
);
	input clk;					// core clock;
	input reset;				// system reset;
	input secure;				// secure mode;

	// register interface;

	input reg_sel;				// register select;
	input [7:0] reg_addr;		// register address decodes;
	input reg_we;				// register write enable;
	input [31:0] reg_wdata;		// register write data;
	output [31:0] reg_rdata;	// register read data;

	// sram interface;

	output sram_req;			// sram request;
	input sram_gnt;				// sram grant;
	output [16:2] sram_addr;	// sram address;
	output sram_we;				// sram write enable;
	output [31:0] sram_wdata;	// sram write data;
	input [31:0] sram_rdata;	// sram read data;

	input dc_req;				// decrypt request from dc;
	input dc_chain;				// load new iv;
	input [16:4] dc_iv;			// dc iv buffer offset;
	input [16:4] dc_ptr;		// dc pointer;
	output dc_done;				// dc done;

	input md_req;				// decrypt request from mc;
	input md_chain;				// load new iv;
	input [5:4] md_size;		// decrypt size;
	input [16:4] md_ptr;		// decrypt pointer;
	output md_done;				// decrypt done;
	input md_dckey;				// decrypt with dc key;

	input me_req;				// encrypt request from mc;
	input me_chain;				// load new iv;
	input [5:4] me_size;		// encrypt size;
	input [16:4] me_ptr;		// encrypt pointer;
	output me_done;				// encrypt done;

	// interrupts to bi;

	output [1:0] aes_intr;		// en-/decrypt interrupts;

	// register interface;

	// decode aes registers addresses;

	wire sec_we;				// secure reg writes;
	wire ra_aesd_ctrl;
	wire ra_aesd_iv;
	wire ra_aese_ctrl;
	wire ra_aese_iv;
	wire ra_aes_kexp;
	wire ra_aes_key;

	assign sec_we = reg_we & secure;
	assign ra_aesd_ctrl = reg_sel & reg_addr[0];
	assign ra_aesd_iv = reg_sel & reg_addr[1];
	assign ra_aese_ctrl = reg_sel & reg_addr[4];
	assign ra_aese_iv = reg_sel & reg_addr[5];
	assign ra_aes_kexp = reg_sel & reg_addr[6];
	assign ra_aes_key = reg_sel & reg_addr[7];

	// decryption control registers;
	// only writable in secure mode;
	// no software use from non-secure mode;

	wire aesd_ctrl_wr;			// write AESD_CTRL;
	wire aesd_ctrl_start;		// start by write to AESD_CTRL;
	wire aesd_start;			// start aes decryption;
	reg aesd_kill;				// kill aes decryption;
	wire aesd_stop;				// stop decryption;
	reg aesd_busy;				// aesd unit busy with sw/mc request;
	wire dsm_busy;				// dsm is busy;
	wire aesd_ubusy;			// decryption unit still busy;
	wire aesd_done;				// done with request;
	reg aesd_intr_mask;			// intr mask;
	reg [29:20] aesd_size;		// size in 16-byte bursts - 1;
	reg [16:4] aesd_ptr;		// sram pointer;
	reg aesd_ksel;				// decryption key to use;
	reg aesd_chain;				// chain iv;
	reg aesd_inc;				// advance to next 16-byte block;
	wire [31:0] aesd_ctrl;		// AESD_CTRL bits;

	assign aesd_ctrl_wr = sec_we & ra_aesd_ctrl;
	assign aesd_ctrl_start = aesd_ctrl_wr & reg_wdata[31];
	assign aesd_ubusy = aesd_busy | dsm_busy;
	assign aesd_start = ~aesd_ubusy & (md_req | aesd_ctrl_start);
	assign aesd_stop = reset | aesd_kill | aesd_done;

	always @(posedge clk)
	begin
		aesd_kill <= aesd_ctrl_wr & ~reg_wdata[31];
		aesd_busy <= ~aesd_stop & (aesd_busy | aesd_start);
		if(aesd_start) begin
			aesd_size <= md_req? { 8'd0, md_size } : reg_wdata[29:20];
			aesd_ptr <= md_req? md_ptr : reg_wdata[16:4];
			aesd_ksel <= md_req? md_dckey : reg_wdata[1];
			aesd_chain <= md_req? md_chain : reg_wdata[0];
		end else if(aesd_inc) begin
			aesd_size <= aesd_size - 1;
			aesd_ptr <= aesd_ptr + 1;
		end
	end

	assign aesd_ctrl = { aesd_ubusy, aesd_intr_mask,
		aesd_size, 3'd0, aesd_ptr, 2'd0, aesd_ksel, aesd_chain };

	// encryption control registers;
	// only writable in secure mode;
	// no software use from non-secure mode;

	wire aese_ctrl_wr;			// write AESE_CTRL;
	wire aese_ctrl_start;		// start by write to AESE_CTRL;
	wire aese_start;			// start aes encryption;
	reg aese_kill;				// kill aes encryption;
	wire aese_stop;				// stop encryption;
	reg aese_busy;				// aese unit busy with sw/mc request;
	wire aese_done;				// done with request;
	reg aese_intr_mask;			// intr mask;
	reg [29:20] aese_size;		// size in 16-byte bursts - 1;
	reg [16:4] aese_ptr;		// sram pointer;
	reg aese_chain;				// chain iv;
	reg aese_last;				// last aese block;
	wire aese_inc;				// advance to next 16-byte block;
	wire [31:0] aese_ctrl;		// AESE_CTRL bits;

	assign aese_ctrl_wr = sec_we & ra_aese_ctrl;
	assign aese_ctrl_start = aese_ctrl_wr & reg_wdata[31];
	assign aese_start = ~aese_busy & (me_req | aese_ctrl_start);
	assign aese_stop = reset | aese_kill | aese_done;

	always @(posedge clk)
	begin
		aese_kill <= aese_ctrl_wr & ~reg_wdata[31];
		aese_busy <= ~aese_stop & (aese_busy | aese_start);
		if(aese_start) begin
			aese_size <= me_req? { 8'd0, me_size } : reg_wdata[29:20];
			aese_ptr <= me_req? me_ptr : reg_wdata[16:4];
			aese_chain <= me_req? me_chain : reg_wdata[0];
		end else if(aese_inc) begin
			aese_size <= aese_size - 1;
			aese_ptr <= aese_ptr + 1;
			aese_chain <= 1'b1;
		end
		aese_last <= (aese_size == 10'd0);
	end

	assign aese_ctrl = { aese_busy, aese_intr_mask,
		aese_size, 3'd0, aese_ptr, 3'd0, aese_chain };

	// iv offsets;
	// only writable in secure mode;
	// readable in both secure and non-secure mode;
	// no need to clear IVs on reset;

	wire [1:0] ivoff_sel;		// iv read selects;
	wire [1:0] ivoff_wr;		// iv write enables;
	reg [16:4] aesd_iv;			// decrypt IV;
	reg [16:4] aese_iv;			// encrypt IV;

	assign ivoff_sel = { ra_aese_iv, ra_aesd_iv };
	assign ivoff_wr = {2{sec_we}} & ivoff_sel;

	always @(posedge clk)
	begin
		if(ivoff_wr[0])
			aesd_iv <= reg_wdata[16:4];
		if(ivoff_wr[1])
			aese_iv <= reg_wdata[16:4];
	end

	// register read data mux;
	// the keys cannot be read read back;
	// output 0 when not selected;
	// next stage will OR data from all the units;

	wire [31:0] aes_kexp;		// AES_KEXP bits;
	wire [31:0] aes_key;		// AES_KEY word;
	reg [31:0] reg_rdata;		// read data flops;

	always @(posedge clk)
	begin
		reg_rdata <= ({32{ra_aesd_ctrl}} & aesd_ctrl)
			| ({32{ra_aese_ctrl}} & aese_ctrl)
			| ({32{ivoff_sel[0]}} & { 15'd0, aesd_iv, 4'd0 })
			| ({32{ivoff_sel[1]}} & { 15'd0, aese_iv, 4'd0 })
			| ({32{ra_aes_kexp}} & aes_kexp )
			| ({32{ra_aes_key}} & aes_key);
	end

	// aes-128 key expander;
	// dc key changes are quite rare;
	// however, mc key changes are frequent, per packet/connection;
	// one key-expander for all three aes keys;
	// decryption requires two expanded key buffers;
	// encryption does not need buffer, does live key expansion;
	// the idea is to expand the two decryption keys
	// while the encryption core is not busy, then use
	// the key expander for the single encryption stream;
	// logic reduced at expense of key expansion time;

	// the encryption key needs storage,
	// because key expansion is destructive;
	// buffer is also used for expansion of decryption keys;
	// flush out key during reset;
	// writes are ignored ready is not 1;
	// fully accessible in non-secure mode;
	// expansion of dc key only from secure mode;
	// any read/write will shift the key;
	// reading is needed for kexp state saving in secure kernel;

	wire k_load;				// key load enable;
	wire [31:0] k_in;			// one of four key words;
	reg [127:0] k_buf;			// key buffer;

	assign k_in = reg_wdata;
	assign aes_key = k_buf[127:96];

	always @(posedge clk)
	begin
		if(k_load)
			k_buf <= { k_buf[95:0], k_in };
	end

	// expansion of decryption keys;
	// started by write to AES_KEXP for IDX 0 or 1;
	// nothing to expand for encryption key;
	// ignore request if kexp is not ready;

	wire ke_wr;					// write to start key expansion;
	reg ke_val;					// kidx valid after reg write;
	wire ke_ready;				// kexp is ready;
	reg ke_stall;				// stall encryption core for kexp;
	wire ke_start;				// start key expansion;
	reg ke_idx;					// decryption key index;
	wire ke_stop;				// stop key expansion;
	reg ke_busy;				// busy with key expansion;
	reg ke_go;					// delayed ke_busy;
	wire ke_end;				// key expansion done;

	assign k_load = reset | ra_aes_key;
	assign ke_stop = reset | ke_end;
	assign ke_wr = reg_we & ra_aes_kexp;
	assign ke_start = ke_val & ke_stall & ke_ready & (secure | ~ke_idx);

	always @(posedge clk)
	begin
		if(reset)
			ke_stall <= 1'b0;
		else if(ke_wr) begin
			ke_stall <= reg_wdata[4];
			ke_idx <= reg_wdata[0];
		end
		ke_val <= ke_wr & reg_wdata[2];
		ke_busy <= ~ke_stop & (ke_busy | ke_start);
		ke_go <= ke_busy;
	end

	assign aes_kexp = { 27'd0, ke_stall, ke_ready, ke_busy, 1'b0, ke_idx };

	// actual key expander;
	// expands key into 11x128 bits;
	// use aes_sbox module from opencores verilog;
	// encryption key has to be reloaded for each data item;
	// rcon logic has been written as shift register,
	// from which key expansion state is decoded;
	// assert ke_set when idle to reduce toggling;

	wire ke_en;					// kexp for encryption;
	wire ke_set;				// load key from k_buf;
	reg [31:0] kw0, kw1, kw2, kw3;
	wire [31:0] kn0, kn1, kn2, kn3;
	wire [127:0] kexp;			// expanded key;
	wire [31:0] ksr;			// sub ^ rcon;
	reg [31:24] krc;			// rcon;
	wire [31:0] ksub;			// sbox substitutions;

	assign ke_set = ~(ke_go | ke_en);
	assign ke_end = (krc[29:28] == 2'b11);	// 'h36;

	always @(posedge clk)
	begin
		kw3 <= ke_set? k_buf[127:96] : kn3;
		kw2 <= ke_set? k_buf[95:64]  : kn2;
		kw1 <= ke_set? k_buf[63:32]  : kn1;
		kw0 <= ke_set? k_buf[31:0]   : kn0;
		if(ke_set)
			krc <= 8'h01;		// rounds 0..7, 01..80;
		else if(krc[31])
			krc <= 8'h1b;		// rounds 8..9, 1b..36;
		else if(ke_end)
			krc <= 8'h00;		// round 10, 00;
		else
			krc <= { krc[30:24], 1'b0 };
	end

	assign kexp = { kw3, kw2, kw1, kw0 };
	assign ksr = ksub ^ { krc, 24'd0 };
	assign kn3 = ksr ^ kw3;
	assign kn2 = kn3 ^ kw2;
	assign kn1 = kn2 ^ kw1;
	assign kn0 = kn1 ^ kw0;

	aes_sbox ksbox3 ( .a(kw0[23:16]), .d(ksub[31:24]) );
	aes_sbox ksbox2 ( .a(kw0[15:8]),  .d(ksub[23:16]) );
	aes_sbox ksbox1 ( .a(kw0[7:0]),   .d(ksub[15:8])  );
	aes_sbox ksbox0 ( .a(kw0[31:24]), .d(ksub[7:0])   );

	// buffers for expanded decryption keys;
	// one for MC, one for DC, each is 11x128 bits;
	// implemented as circular shift register to reduce logic;
	// which means that abort is only possible after an entire
	// data block (16 bytes) has been decrypted; aborting in
	// the middle would leave expanded key messed up;

	// MC key buffer;

	wire kmc_load;				// load kdc;
	wire kmc_shift;				// shift kdc;
	wire [127:0] kmc;			// expanded MC key word;

	assign kmc_load = ke_busy & (ke_idx == 1'b0);

	gi_kbuf kmc_buf (
		.clk(clk),
		.load(kmc_load),
		.shift(kmc_shift),
		.kin(kexp),
		.kout(kmc)
	);

	// DC key buffer;

	wire kdc_load;				// load kdc;
	wire kdc_shift;				// shift kdc;
	wire [127:0] kdc;			// expanded DC key word;

	assign kdc_load = ke_busy & (ke_idx == 1'b1);

	gi_kbuf kdc_buf (
		.clk(clk),
		.load(kdc_load),
		.shift(kdc_shift),
		.kin(kexp),
		.kout(kdc)
	);

	// aes decryption core;
	// single issue sequential operation;
	// 4 clocks to load data from sram;
	// 11 clocks to decrypt, leaves data in output reg;
	// 4 clocks to write back to sram;
	// 100Mhz * 16bytes / 24 clocks = 66MBytes/sec;
	// sufficient bandwidth for both DC and MC;

	// input data/IV registers;
	// 4 loads from sram to pull 128 bits of data;
	// implemented as shift reg to reduce loading of sram_rdata;
	// 4 words of IV must be loaded before decryption start;
	// the old encrypted data become the new IV,
	// shift IVs into buffers for MC or DC;
	// saving and reloading in sram is too expensive;

	wire de_load;				// load word for decryption;
	reg [127:0] de_in;			// data to decrypt;
	wire iv_md_load;			// load IV for MD;
	reg [127:0] iv_md;			// MC decryption IV;
	wire iv_dc_load;			// load IV for DC;
	reg [127:0] iv_dc;			// DC decryption IV;

	always @(posedge clk)
	begin
		if(de_load)
			de_in <= { de_in[95:0], sram_rdata };
		if(iv_md_load)
			iv_md <= { iv_md[95:0], de_in[127:96] };
		if(iv_dc_load)
			iv_dc <= { iv_dc[95:0], de_in[127:96] };
	end

	// expanded key & iv mux;
	// select MC or DC values;
	// allow dc key being used for decryption;

	reg de_ksel;				// decryption key to use;
	reg de_sel;					// iv select 0=dc 1=md;
	wire [127:0] de_ks;			// selected decryption key stream;
	wire [127:0] de_iv;			// selected decryption iv;

	assign de_ks = de_ksel? kdc : kmc;
	assign de_iv = de_sel? iv_md : iv_dc;

	// decryption round state;
	// do initial permutation one clock after start;
	// followed by 10 rounds;
	// de_work stops aes core (and sim) from toggling;

	reg de_start;				// start decryption;
	reg de_work;				// work on decryption;
	reg [127:0] de_x;			// decryption state;
	wire [127:0] de_sr;			// scrambled round;
	wire [127:0] de_sub;		// inverse sbox substitutions;
	wire [127:0] de_rk;			// sub round key;
	wire [127:0] de_next;		// next state;

	always @(posedge clk)
	begin
		if(de_start)
			de_x <= de_in ^ de_ks;
		else if(de_work)
			de_x <= de_next;
	end

	// decryption result handling;
	// XOR result with iv;
	// shift out as 4 words for sram write-back;

	reg de_cbc;					// do cbc on result;
	wire de_sho;				// shift out result;
	reg [127:0] de_out;			// decrypted data;

	always @(posedge clk)
	begin
		if(de_cbc)
			de_out <= de_rk ^ de_iv;
		else if(de_sho)
			de_out[127:32] <= de_out[95:0];
	end

	// scramble rounds;
	// basically reorder the bytes;
	//				in	ik	sa	sr		rk	out
	//							sub
	//	[127:120]	15	15	00	00=15	15	15
	//	[119:112]	14	14	10	11=10	10
	//	[111:104]	13	13	20	22=5	5
	//	[103:96]	12	12	30	33=0	0
	//	[95:88]		11	11	01	01=11	11
	//	[87:80]		10	10	11	12=6	6
	//	[79:72]		9	9	21	23=1	1
	//	[71:64]		8	8	31	30=12	12
	//	[63:56]		7	7	02	02=7	7
	//	[55:48]		6	6	12	13=2	2
	//	[47:40]		5	5	22	20=13	13
	//	[39:32]		4	4	32	31=8	8
	//	[31:24]		3	3	03	03=3	3
	//	[23:16]		2	2	13	10=14	14
	//	[15:8]		1	1	23	21=9	9
	//	[7:0]		0	0	33	32=4	4

	assign de_sr = { 
		de_x[127:120], de_x[23:16], de_x[47:40], de_x[71:64],
		de_x[95:88], de_x[119:112], de_x[15:8], de_x[39:32],
		de_x[63:56], de_x[87:80], de_x[111:104], de_x[7:0],
		de_x[31:24], de_x[55:48], de_x[79:72], de_x[103:96] };

	// inverse cipher sboxes;
	// basically 8->8 recoders;

	aes_inv_sbox isbox15 ( .a(de_sr[127:120]), .d(de_sub[127:120]) );
	aes_inv_sbox isbox14 ( .a(de_sr[119:112]), .d(de_sub[119:112]) );
	aes_inv_sbox isbox13 ( .a(de_sr[111:104]), .d(de_sub[111:104]) );
	aes_inv_sbox isbox12 ( .a(de_sr[103:96]),  .d(de_sub[103:96])  );
	aes_inv_sbox isbox11 ( .a(de_sr[95:88]),   .d(de_sub[95:88])   );
	aes_inv_sbox isbox10 ( .a(de_sr[87:80]),   .d(de_sub[87:80])   );
	aes_inv_sbox isbox09 ( .a(de_sr[79:72]),   .d(de_sub[79:72])   );
	aes_inv_sbox isbox08 ( .a(de_sr[71:64]),   .d(de_sub[71:64])   );
	aes_inv_sbox isbox07 ( .a(de_sr[63:56]),   .d(de_sub[63:56])   );
	aes_inv_sbox isbox06 ( .a(de_sr[55:48]),   .d(de_sub[55:48])   );
	aes_inv_sbox isbox05 ( .a(de_sr[47:40]),   .d(de_sub[47:40])   );
	aes_inv_sbox isbox04 ( .a(de_sr[39:32]),   .d(de_sub[39:32])   );
	aes_inv_sbox isbox03 ( .a(de_sr[31:24]),   .d(de_sub[31:24])   );
	aes_inv_sbox isbox02 ( .a(de_sr[23:16]),   .d(de_sub[23:16])   );
	aes_inv_sbox isbox01 ( .a(de_sr[15:8]),    .d(de_sub[15:8])    );
	aes_inv_sbox isbox00 ( .a(de_sr[7:0]),     .d(de_sub[7:0])     );

	// xor substitutions with key stream;
	// instantiate inverse column mixers;

	assign de_rk = de_sub ^ de_ks;

	gi_mixd mixd3 ( .i(de_rk[127:96]), .o(de_next[127:96]) );
	gi_mixd mixd2 ( .i(de_rk[95:64]), .o(de_next[95:64]) );
	gi_mixd mixd1 ( .i(de_rk[63:32]), .o(de_next[63:32]) );
	gi_mixd mixd0 ( .i(de_rk[31:0]), .o(de_next[31:0]) );

	// aes encryption core;
	// single issue sequential operation;
	// 4 clocks to load data from sram;
	// 11 clocks to encrypt, leaves data in output reg;
	// 4 clocks to write back to sram;
	// 100Mhz * 16bytes / 24 clocks = 66MBytes/sec;
	// sufficient bandwidth for MC;

	// input data/IV registers;
	// 4 loads from sram to pull 128 bits of data;
	// implemented as shift reg to reduce loading of sram_rdata;
	// 4 words of IV must be loaded before encryption start;
	// no IV buffer needed, load en_in with IV, then XOR data into;
	// shift in the encrypted data, which is the new iv;
	// expanded key stream comes live from key expander;

	wire [2:0] en_load;			// load ct, data (cbc), iv;
	reg [127:0] en_in;			// data to decrypt;
	wire [127:0] en_ks;			// encryption key stream;
	reg [127:0] en_out;			// encrypted data;

	assign en_ks = kexp;

	always @(posedge clk)
	begin
		if(en_load[0])
			en_in[31:0] <= sram_rdata;
		else if(en_load[1])
			en_in[31:0] <= en_in[127:96] ^ sram_rdata;
		else if(en_load[2])
			en_in[31:0] <= en_out[127:96];
		if(|en_load)
			en_in[127:32] <= en_in[95:0];
	end

	// encryption round state;
	// do initial permutation one clock after start;
	// followed by 10 rounds;
	// en_work stops aes core (and sim) from toggling;

	reg en_start;				// start encryption;
	reg en_work;				// work on encryption;
	reg [127:0] en_x;			// encryption state;
	wire [127:0] en_sub;		// sbox substitutions;
	wire [127:0] en_sr;			// scrambled round;
	wire [127:0] en_mix;		// mixed columns;
	wire [127:0] en_next;		// next state;

	always @(posedge clk)
	begin
		if(en_start)
			en_x <= en_in ^ en_ks;
		else if(en_work)
			en_x <= en_next;
	end

	// forward cipher sboxes;
	// basically 8->8 recoders;

	aes_sbox fsbox15 ( .a(en_x[127:120]), .d(en_sub[127:120]) );
	aes_sbox fsbox14 ( .a(en_x[119:112]), .d(en_sub[119:112]) );
	aes_sbox fsbox13 ( .a(en_x[111:104]), .d(en_sub[111:104]) );
	aes_sbox fsbox12 ( .a(en_x[103:96]),  .d(en_sub[103:96])  );
	aes_sbox fsbox11 ( .a(en_x[95:88]),   .d(en_sub[95:88])   );
	aes_sbox fsbox10 ( .a(en_x[87:80]),   .d(en_sub[87:80])   );
	aes_sbox fsbox09 ( .a(en_x[79:72]),   .d(en_sub[79:72])   );
	aes_sbox fsbox08 ( .a(en_x[71:64]),   .d(en_sub[71:64])   );
	aes_sbox fsbox07 ( .a(en_x[63:56]),   .d(en_sub[63:56])   );
	aes_sbox fsbox06 ( .a(en_x[55:48]),   .d(en_sub[55:48])   );
	aes_sbox fsbox05 ( .a(en_x[47:40]),   .d(en_sub[47:40])   );
	aes_sbox fsbox04 ( .a(en_x[39:32]),   .d(en_sub[39:32])   );
	aes_sbox fsbox03 ( .a(en_x[31:24]),   .d(en_sub[31:24])   );
	aes_sbox fsbox02 ( .a(en_x[23:16]),   .d(en_sub[23:16])   );
	aes_sbox fsbox01 ( .a(en_x[15:8]),    .d(en_sub[15:8])    );
	aes_sbox fsbox00 ( .a(en_x[7:0]),     .d(en_sub[7:0])     );

	// scramble rounds;
	// basically reorder the bytes;
	//				in	ik	sub	sr		rk	out
	//							mix
	//	[127:120]	15	15	00	00=15	15
	//	[119:112]	14	14	10	13=2
	//	[111:104]	13	13	20	22=5
	//	[103:96]	12	12	30	31=8
	//	[95:88]		11	11	01	01=11
	//	[87:80]		10	10	11	10=14
	//	[79:72]		9	9	21	23=1
	//	[71:64]		8	8	31	32=4
	//	[63:56]		7	7	02	02=7
	//	[55:48]		6	6	12	11=10
	//	[47:40]		5	5	22	20=13
	//	[39:32]		4	4	32	33=0
	//	[31:24]		3	3	03	03=3
	//	[23:16]		2	2	13	12=6
	//	[15:8]		1	1	23	21=9
	//	[7:0]		0	0	33	30=12

	assign en_sr = { 
		en_sub[127:120], en_sub[87:80], en_sub[47:40], en_sub[7:0],
		en_sub[95:88], en_sub[55:48], en_sub[15:8], en_sub[103:96],
		en_sub[63:56], en_sub[23:16], en_sub[111:104], en_sub[71:64],
		en_sub[31:24], en_sub[119:112], en_sub[79:72], en_sub[39:32] };

	// instantiate column mixers;
	// xor scrambled with key stream;

	gi_mixe mixe3 ( .i(en_sr[127:96]), .o(en_mix[127:96]) );
	gi_mixe mixe2 ( .i(en_sr[95:64]), .o(en_mix[95:64]) );
	gi_mixe mixe1 ( .i(en_sr[63:32]), .o(en_mix[63:32]) );
	gi_mixe mixe0 ( .i(en_sr[31:0]), .o(en_mix[31:0]) );

	assign en_next = en_mix ^ en_ks;

	// encryption result handling;
	// shift out as 4 words for sram write-back;

	reg en_done;				// done with encryption;
	wire en_sho;				// shift out result;

	always @(posedge clk)
	begin
		if(en_done)
			en_out <= en_sr ^ en_ks;
		else if(en_sho)
			en_out[127:32] <= en_out[95:0];
	end

	// sram access acks;
	// needed to latch/supply sram data;
	// sram_win[1] 0=aese 1=aesd;
	// sram_win[0] 0=dc 1=aesd;
	// sram_*ack [0]=dsm [1]=esm;

	reg [1:0] sram_win;			// sram winner;
	wire [1:0] sram_rack;		// sram read ack;
	wire [1:0] sram_wack;		// sram write ack;
	wire [1:0] sram_ack;		// sram ack per unit;

	// arbitrate decryption engine;
	// requestors are DC and MC, with DC having higher priority;
	// size of DC and MC requests is 64 bytes, same as hash size;
	// software use of AESD_CTRL allows variable size;

	wire dsm_idle;				// set dsm arbiter idle;
	reg [1:0] dsm_win;			// decryption winner;
	reg dsm_start;				// start dsm;
	wire dsm_inc;				// advance to next 16-byte block;
	reg [29:20] dsm_size;		// size in 16-byte blocks - 1;
	reg [16:4] dsm_ptr;			// ptr in sram;
	reg [16:4] dsm_iv;			// iv in sram;
	reg dsm_chain;				// winner needs iv chain;
	reg dsm_last;				// last block to decrypt;
	wire dsm_done;				// done, rearbitrate;

	assign dsm_idle = reset | dsm_done;

	always @(posedge clk)
	begin
		if(dsm_idle)
			dsm_win <= 2'b00;
		else if(~dsm_busy) begin
			dsm_win[0] <= dc_req;
			dsm_win[1] <= ~dc_req & aesd_busy;
			dsm_size <= dc_req? 10'd3 : aesd_size;
			dsm_ptr <= dc_req? dc_ptr : aesd_ptr;
			dsm_iv <= dc_req? dc_iv : aesd_iv;
			dsm_chain <= dc_req? dc_chain : aesd_chain;
			de_ksel <= dc_req? 1'b1 : aesd_ksel;
		end else if(dsm_inc) begin
			dsm_size <= dsm_size - 1;
			dsm_ptr <= dsm_ptr + 1;
		end
		dsm_start <= ~dsm_busy;
		dsm_last <= dsm_busy & ((dsm_size == 10'd0) | dsm_last | aesd_kill);
		de_sel <= dsm_win[1];
	end

	assign dsm_busy = |dsm_win;

	// decryption state machine;
	// operates on dsm_size 16-byte packets;
	// load 4 words, decrypt, write-back;
	// optionally, load IV from sram;
	// read latency is part of state count;
	//	0		idle;
	//	4..7	load iv;
	//	8..11	read data;
	//	12..15	read data latency;
	//	16..27	decrypt;
	//	28..31	write data;

	reg [4:0] dsm;				// decryption state machine;
	reg dsm_end;				// check for end of request;
	wire dsm_next;				// advance to next state;
	wire dsm_new;				// next 16-byte block;
	wire dsm_s_iv;				// state 4..7, iv load;
	wire dsm_s_r;				// state 8..11, read;
	wire dsm_s_w;				// state 24..27, write;
	reg md_done;				// done to mc;

	always @(posedge clk)
	begin
		if(~dsm_busy) begin
			dsm <= 5'd0;
			dsm_end <= 1'b0;
		end else if(dsm_start)
			dsm[3:2] <= { dsm_chain, ~dsm_chain };
		else if(dsm_next) begin
			dsm <= dsm_new? 5'd8 : (dsm + 1);
			dsm_end <= dsm_new & dsm_last;
		end
		aesd_inc <= dsm_win[1] & dsm_inc;
		md_done <= aesd_done;
	end

	assign dsm_new = (dsm == 5'd31);
	assign dsm_s_iv = (dsm[4:2] == 3'b001);
	assign dsm_s_r = (dsm[4:2] == 3'b010);
	assign dsm_s_w = (dsm[4:2] == 3'b111);
	assign dsm_inc = dsm_next & dsm_new;
	assign dsm_done = dsm_next & dsm_end;
	assign aesd_done = dsm_done & dsm_win[1];

	// aes decryption core control;
	// controls loading/saving of data;
	// need key shift control one clock earlier due to loading;

	reg [1:0] dsm_load;			// load enable pipe;
	reg [1:0] dsm_dc;			// requestor pipe;
	reg de_run;					// run decryption rounds;
	wire de_last;				// last decryption round;

	assign de_last = ~dsm_busy | (dsm == 5'd26);

	always @(posedge clk)
	begin
		dsm_load <= { dsm_load[0], sram_rack[0] };
		dsm_dc <= { dsm_dc[0], sram_rack[0] & sram_win[0] };
		de_run <= (dsm == 5'd15) | (~de_last & de_run);
		de_start <= dsm[4] & de_run & ~de_work;
		de_work <= dsm[4] & de_run;
		de_cbc <= de_last;
	end

	assign de_load = dsm_load[1];
	assign iv_md_load = de_load & ~dsm_dc[1];
	assign iv_dc_load = de_load & dsm_dc[1];
	assign de_sho = sram_wack[0];

	assign kmc_shift = de_run & ~de_ksel;
	assign kdc_shift = de_run & de_ksel;

	// request sram access;
	// states: 4..7=iv, 8..11=in, 28..31=write;
	// address sources are iv and ptr;

	wire dsm_rw;				// dsm sram r/w;
	reg dsm_pend;				// dsm req pending;
	wire dsm_req;				// dsm sram request;
	reg [16:2] dsm_addr;		// dsm sram address;
	reg dsm_we;					// write to sram;
	wire [31:0] dsm_wdata;		// decrypted data;

	assign dsm_rw = dsm_s_iv | dsm_s_r | dsm_s_w;
	assign dsm_req = dsm_rw | (dsm_pend & ~sram_ack[0]);

	always @(posedge clk)
	begin
		dsm_pend <= dsm_busy & dsm_req;
		if( ~dsm_pend | sram_ack[0]) begin
			dsm_addr[16:4] <= dsm_s_iv? dsm_iv : dsm_ptr;
			dsm_addr[3:2] <= dsm[1:0];
			dsm_we <= dsm_s_w;
		end
	end

	assign dsm_wdata = de_out[127:96];

	// encryption has single user, AESE or MC;
	// no arbitration needed;
	// size of MC requests is 64 bytes, same as hash size;
	// software use of AESE_CTRL allows variable size;
	// stall next 16-byte block for pending key expansion;

	wire esm_chain;				// chain iv for esm;
	wire esm_busy;				// esm busy;
	wire esm_last;				// last aese block;

	assign esm_chain = aese_chain;
	assign esm_last = aese_last;
	assign ke_ready = ~esm_busy;

	// encryption state machine;
	// operates on aese_size 16-byte packets;
	// load 4 words, encrypt, write-back;
	// optionally, load IV from sram;
	// read latency is part of state count;
	//	0		idle;
	//	4..7	load iv;
	//	8..11	read data;
	//	12..15	read data latency;
	//	16..27	encrypt;
	//	28..31	write data;

	wire esm_ena;				// enable esm;
	wire esm_clr;				// clear esm;
	wire esm_start;				// start encryption;
	reg [4:0] esm;				// encryption state machine;
	reg esm_end;				// check for end of request;
	wire esm_next;				// advance to next state;
	wire esm_done;				// done with 16-byte block;
	wire esm_s_iv;				// state 4..7, iv load;
	wire esm_s_r;				// state 8..11, read;
	wire esm_s_w;				// state 24..27, write;
	reg me_done;				// done to mc;

	assign esm_ena = aese_busy;
	assign esm_busy = |esm[4:2];
	assign esm_clr = ~esm_ena | (~esm_busy & ke_stall);
	assign esm_start = esm_ena & ~esm_busy & ~esm_end & esm_next;

	always @(posedge clk)
	begin
		if(esm_clr) begin
			esm <= 5'd0;
			esm_end <= 1'b0;
		end else if(esm_start)
			esm[3:2] <= { esm_chain, ~esm_chain };
		else if(esm_next) begin
			esm <= esm_done? 5'd0 : (esm + 1);
			esm_end <= esm_done & esm_last;
		end
		me_done <= aese_done;
	end

	assign esm_done = (esm == 5'd31);
	assign esm_s_iv = (esm[4:2] == 3'b001);
	assign esm_s_r = (esm[4:2] == 3'b010);
	assign esm_s_w = (esm[4:2] == 3'b111);
	assign aese_inc = esm_next & esm_done;
	assign aese_done = esm_next & esm_end;

	// request sram access;
	// states: 4..7=iv, 8..11=in, 28..31=write;
	// address sources are iv and ptr;

	wire esm_rw;				// esm sram r/w;
	reg esm_pend;				// esm req pending;
	wire esm_req;				// esm sram request;
	reg [16:2] esm_addr;		// esm sram address;
	reg esm_we;					// write to sram;
	reg esm_p_iv;				// piped iv load state;
	wire [31:0] esm_wdata;		// encrypted data;

	assign esm_rw = esm_s_iv | esm_s_r | esm_s_w;
	assign esm_req = esm_rw | (esm_pend & ~sram_ack[1]);

	always @(posedge clk)
	begin
		esm_pend <= esm_ena & esm_req;
		if( ~esm_pend | sram_ack[1]) begin
			esm_addr[16:4] <= esm_s_iv? aese_iv : aese_ptr;
			esm_addr[3:2] <= esm[1:0];
			esm_we <= esm_s_w;
			esm_p_iv <= esm_s_iv;
		end
	end

	assign esm_wdata = en_out[127:96];

	// aes encryption core control;
	// controls loading/saving of data;
	// need piped state for load controls;

	reg [1:0] esm_load;			// load enable pipe;
	reg [1:0] esm_ldiv;			// iv load pipe;
	reg en_run;					// run encryption rounds;
	wire en_last;				// last encryption round;

	assign en_last = esm_clr | (esm == 5'd26);

	always @(posedge clk)
	begin
		esm_load <= { esm_load[0], sram_rack[1] };
		esm_ldiv <= { esm_ldiv[0], esm_p_iv };
		en_run <= (esm == 5'd15) | (~en_last & en_run);
		en_start <= esm[4] & en_run & ~en_work;
		en_work <= esm[4] & en_run;
		en_done <= en_last;
	end

	assign en_load[0] = esm_load[1] & esm_ldiv[1];
	assign en_load[1] = esm_load[1] & ~esm_ldiv[1];
	assign en_sho = sram_wack[1];
	assign en_load[2] = en_sho;
	assign ke_en = en_work;

	// sram access controller;
	// two sources: decryption & encryption;
	// both decryption and encryption take the same time,
	// ie. 4 clks load, 11 op, 4 clks writeback;
	// round-robin arbitration would stall both,
	// so give decryption core higher priority,
	// which is what we want for the DC anyway;
	// sram_end cancels prefetch by taking away data ack;

	wire sram_pend;				// sram request pending;
	wire [16:2] sram_addr;		// sram address;
	wire sram_we;				// sram write;
	wire [31:0] sram_wdata;		// sram write data;
	reg sram_end;				// valid sram request;

	assign sram_req = dsm_req | esm_req;
	assign sram_pend = dsm_pend | esm_pend;

	always @(posedge clk)
	begin
		if(~sram_pend | sram_gnt) begin
			sram_win <= { dsm_req, dsm_win[0] };
			sram_end <= dsm_req? dsm_end : esm_end; //XXX piped ?
		end
	end

	assign sram_wdata = sram_win[1]? dsm_wdata : esm_wdata;
	assign sram_addr = sram_win[1]? dsm_addr : esm_addr;
	assign sram_we = sram_win[1]? dsm_we : esm_we;

	assign sram_ack[0] = sram_gnt & sram_win[1];
	assign sram_ack[1] = sram_gnt & ~sram_win[1];
	assign sram_rack[0] = sram_ack[0] & ~sram_end & sram_pend & ~sram_we;
	assign sram_rack[1] = sram_ack[1] & ~sram_end & sram_pend & ~sram_we;
	assign sram_wack[0] = sram_ack[0] & ~sram_end & sram_pend & sram_we;
	assign sram_wack[1] = sram_ack[1] & ~sram_end & sram_pend & sram_we;

	// advance dsm/esm state machines;
	// for every granted sram access;
	// for duration of cipher operation;

	assign dsm_next = ~dsm_pend | sram_ack[0];
	assign esm_next = ~esm_pend | sram_ack[1];

	// aes interrupt handling;

	reg [1:0] aes_intr;			// intr flags;
	wire [1:0] aes_intr_clr;	// clear interrupts;
	wire [1:0] aes_intr_set;	// issue interrupts;

	assign aes_intr_clr[0] = reset | aesd_kill;
	assign aes_intr_clr[1] = reset | aese_kill;
	assign aes_intr_set[0] = aesd_intr_mask & aesd_done;
	assign aes_intr_set[1] = aese_intr_mask & aese_done;

	always @(posedge clk)
	begin
		if(aes_intr_clr[0])
			aesd_intr_mask <= 1'b0;
		else if(aesd_start)
			aesd_intr_mask <= md_req? 1'b0 : reg_wdata[30];
		if(aes_intr_clr[1])
			aese_intr_mask <= 1'b0;
		else if(aese_start)
			aese_intr_mask <= me_req? 1'b0 : reg_wdata[30];
		aes_intr <= ~aes_intr_clr & (aes_intr | aes_intr_set);
	end

	// simulator init;
	// fill reg bits to inverse of reset defaults;
	// fill undefined bits with randoms;
	// needed, so no Xs move across PCI (parity);

`ifdef	SIM
	initial
	begin
		{ aesd_busy, aesd_intr_mask } = 2'b11;
		{ aesd_size, aesd_ptr, aesd_ksel, aesd_chain } = $random;
		aesd_iv = $random;
		{ aese_busy, aese_intr_mask } = 2'b11;
		{ aese_size, aese_ptr, aese_chain } = $random;
		aese_iv = $random;
		ke_idx = $random;
	end

`endif	// SIM

endmodule

