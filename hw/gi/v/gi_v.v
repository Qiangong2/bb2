// gi_v.v Frank Berndt
// virage wrapper, novea, nms and charge pump;
// :set tabstop=4

module gi_v (
	clk, reset,
	v_addr, v_me, v_we, v_in, v_out,
	v_sclk, v_sme, v_si, v_so,
	v_porst, v_vpp
);
	input clk;				// system clock;
	input reset;			// system reset;

	// parallel interface;

	input [9:2] v_addr;		// address;
	input v_me;				// enable;
	input v_we;				// write enable;
	input [31:0] v_in;		// write data;
	output [31:0] v_out;	// read data;

	// serial interface;

	input v_sclk;			// serial clock;
	input v_sme;			// serial mode control;
	input v_si;				// serial input;
	output v_so;			// serial output;

	// power;

	output v_porst;			// power-on reset;
	output v_vpp;			// charge pump monitor pad;

`ifdef FPGA

   // Stub out Virage RTL/Model for FPGA implementation
   
   assign v_porst = 1'b0;
   assign v_vpp = 1'b0;
   assign v_so = 1'b0;
   assign v_out = 32'b0;

`else

	// need a negative reset;

	wire reset_l;			// active 0 reset;

	assign reset_l = ~reset;

	// do writes one clock later,
	// because of delayed write data;

	reg [9:2] w_addr;		// write address;
	reg w_we;				// do a write;
	wire [9:2] x_addr;		// r/w access address;
	wire x_me;				// access enable;

	always @(posedge clk)
	begin
		w_addr <= v_addr;
		w_we <= v_me & v_we;
	end

	assign x_addr = w_we? w_addr : v_addr;
	assign x_me = v_me | w_we;

	// decode novea & nms register spaces;
	// hold on to address for register accesses;

	wire [2:0] v_sel;		// individual selects;
	reg [2:0] v_rsel;		// read data selects;
	wire nms_wr;			// write to V_CMD;
	reg [1:0] nms_pipe;		// cmd pipe;
	reg [2:0] nms_cmd;		// ctrl command;
	wire nms_exec;			// execute command;

	assign v_sel[0] = x_me & ~x_addr[9];				// sram;
	assign v_sel[1] = x_me & (x_addr[9:8] == 2'b10);	// regs;
	assign v_sel[2] = v_me & (v_addr[9:8] == 2'b11);	// cmds;
	assign nms_wr = w_we & v_rsel[2];					// cmd write;

	always @(posedge clk)
	begin
		v_rsel <= v_sel;
		nms_pipe <= { nms_pipe[0], nms_wr };
		if(nms_wr)
			nms_cmd <= v_in[10:8];
	end

	assign nms_exec = |nms_pipe;

	// virage controller time base;
	// needs 1usec reference tick;

	reg [6:0] v_div;		// clk divider value;
	wire v_div_wr;			// write divider value;
	reg [6:0] v_cnt;		// clk counter;
	reg v_cntz;				// set divider to 0;
	reg v_tick;				// virage time base;

	assign v_div_wr = nms_wr & v_in[7];

	always @(posedge clk)
	begin
		if(reset)
			v_div <= 7'd100 - 2;		// reset default for 100MHz;
		else if(v_div_wr)
			v_div <= v_in[6:0];
		if(v_cntz)
			v_cnt <= 7'd0;
		else
			v_cnt <= v_cnt + 1;
		v_cntz <= reset | (v_cnt == v_div);
		v_tick <= (v_cnt[6:2] == 5'd1);
	end

	// decode novea accesses;
	// nv_we comes from store controller, which blocks writes
	// if the array is in a state that cannot do sram writes;
	// magic jtag cookie is at end of array;

	wire [8:2] nv_addr;		// novea sram address;
	wire nv_me;				// novea memory enable;
	wire nv_we;				// novea write enable;

	assign nv_addr = x_addr[8:2];
	assign nv_me = v_sel[0];

	// cannot instantiate the virage supplied top level,
	// because it uses tri-states for muxing the novea
	// sram read data and the controller read data;
	// also, we need to issue a recall for the jtag cookie;

	// instantiate charge pump;
	// this is a hard core;

	wire nv_rst;				// charge-pump reset;
	wire nv_pe;					// pump enable;
	wire nv_clke;				// pump clock enable;
	wire nv_data;				// magic data;
	wire nv_store;				// store signal;
	wire [3:0] nv_vppsel;		// vpp select;
	wire nv_vpprange;			// vpp range;
	wire nv_unlock;				// pump unlock;
	wire [3:1] nv_pu;			// XXX

	nvcp_um18gfs cp (
		.CLK(clk),
		.RST(nv_rst),
		.PE(nv_pe),
		.CLKE(nv_clke),
		.D(nv_data),
		.STORE(nv_store),
		.VPPSEL(nv_vppsel),
		.VPPRANGE(nv_vpprange),
		.PORST(v_porst),
		.UNLOCK(nv_unlock),
		.PU1(nv_pu[1]),
		.PU2(nv_pu[2]),
		.PU3(nv_pu[3]),
		.VPP(v_vpp)
	);

	// instantiate novea array;
	// this is a hard core;
	// the novea array is the memory region;
	// do not use ou

	wire nv_recall;				// novea recall;
	wire [1:0] nv_mrcl;			// margin recall controls;
	wire [1:0] nv_tecc;			// cell selects;
	wire [1:0] nv_bias;			// bias controls;
	wire [31:0] nv_out;			// novea read data;

	wire nv_sme;				// serial mode enable;
	wire nv_si;					// serial in;
	wire nv_so;					// serial out;

	nvrm_um18ugs_128x32 novea (
		.CLK(clk),
		.ADR6(nv_addr[8]),
		.ADR5(nv_addr[7]),
		.ADR4(nv_addr[6]),
		.ADR3(nv_addr[5]),
		.ADR2(nv_addr[4]),
		.ADR1(nv_addr[3]),
		.ADR0(nv_addr[2]),
		.ME(nv_me),
		.WE(nv_we),
		.OE(1'b1),
		.DIN31(v_in[31]),
		.DIN30(v_in[30]),
		.DIN29(v_in[29]),
		.DIN28(v_in[28]),
		.DIN27(v_in[27]),
		.DIN26(v_in[26]),
		.DIN25(v_in[25]),
		.DIN24(v_in[24]),
		.DIN23(v_in[23]),
		.DIN22(v_in[22]),
		.DIN21(v_in[21]),
		.DIN20(v_in[20]),
		.DIN19(v_in[19]),
		.DIN18(v_in[18]),
		.DIN17(v_in[17]),
		.DIN16(v_in[16]),
		.DIN15(v_in[15]),
		.DIN14(v_in[14]),
		.DIN13(v_in[13]),
		.DIN12(v_in[12]),
		.DIN11(v_in[11]),
		.DIN10(v_in[10]),
		.DIN9(v_in[9]),
		.DIN8(v_in[8]),
		.DIN7(v_in[7]),
		.DIN6(v_in[6]),
		.DIN5(v_in[5]),
		.DIN4(v_in[4]),
		.DIN3(v_in[3]),
		.DIN2(v_in[2]),
		.DIN1(v_in[1]),
		.DIN0(v_in[0]),
		.Q31(nv_out[31]),
		.Q30(nv_out[30]),
		.Q29(nv_out[29]),
		.Q28(nv_out[28]),
		.Q27(nv_out[27]),
		.Q26(nv_out[26]),
		.Q25(nv_out[25]),
		.Q24(nv_out[24]),
		.Q23(nv_out[23]),
		.Q22(nv_out[22]),
		.Q21(nv_out[21]),
		.Q20(nv_out[20]),
		.Q19(nv_out[19]),
		.Q18(nv_out[18]),
		.Q17(nv_out[17]),
		.Q16(nv_out[16]),
		.Q15(nv_out[15]),
		.Q14(nv_out[14]),
		.Q13(nv_out[13]),
		.Q12(nv_out[12]),
		.Q11(nv_out[11]),
		.Q10(nv_out[10]),
		.Q9(nv_out[9]),
		.Q8(nv_out[8]),
		.Q7(nv_out[7]),
		.Q6(nv_out[6]),
		.Q5(nv_out[5]),
		.Q4(nv_out[4]),
		.Q3(nv_out[3]),
		.Q2(nv_out[2]),
		.Q1(nv_out[1]),
		.Q0(nv_out[0]),
		.COMP(nv_comp),
		.RECALL(nv_recall),
		.STORE(nv_store),
		.MRCL0(nv_mrcl[0]),
		.MRCL1(nv_mrcl[1]),
		.TECC0(nv_tecc[0]),
		.TECC1(nv_tecc[1]),
		.BIAS0(nv_bias[0]),
		.BIAS1(nv_bias[1]),
		.MATCH(nv_match),
		.RCREADY(nv_rcready),
		.SCLK(v_sclk),
		.SME(nv_sme),
		.SI(nv_si),
		.SO(nv_so),
		.PU1(nv_pu[1]),
		.PU2(nv_pu[2]),
		.PU3(nv_pu[3]),
		.VPP(v_vpp)
	);

	// instantiate store controller;
	// this is synthesized with the gi;
	// the controller has all the registers;

	wire nv_updatewr;
	wire nv_shiftwr;
	wire nv_selectwir;
	wire [31:0] ctrl_out;		// store ctrl read data;

	assign nv_updatewr = 1'b0;
	assign nv_shiftwr = 1'b0;
	assign nv_selectwir = 1'b0;

	// nms status signals;

	wire nms_ready;				// ctrl ready;
	wire nms_pass;				// operation passed;
	wire nms_keep;				// keep mode is on;

	nms128x32_ctr ctr (
		.SYS_CK(clk),
		.TIME_B(v_tick),
		.WRSTN(reset_l),
		.WRCK(v_sclk),
		.WSI(v_si),
		.WSO(v_so),
		.UpdateWR(nv_updatewr),
		.ShiftWR(nv_shiftwr),
		.SelectWIR(nv_selectwir),
		.PAE_p(nms_exec),
		.PA_p(nms_cmd),
		.RCREADY(nv_rcready),
		.MATCH(nv_match),
		.UNLOCK(nv_unlock),
		.PORST(v_porst),
		.ADR_p(x_addr[8:2]),
		.D_p(v_in),
		.RA_p(v_sel[1]),
		.WE_p(w_we),
		.ME_p(x_me),
		.OE_p(1'b1),
		.NMS_READY(nms_ready),
		.NMS_PASS(nms_pass),
		.KEEP_ON(nms_keep),
		.RST_i(nv_rst),
		.PE(nv_pe),
		.VPPRANGE(nv_vpprange),
		.VPPSEL(nv_vppsel),
		.UDATA(nv_data),
		.PCLKE(nv_clke),
		.COMP(nv_comp),
		.STORE(nv_store),
		.RECALL(nv_recall),
		.MRCL0(nv_mrcl[0]),
		.MRCL1(nv_mrcl[1]),
		.TECC0(nv_tecc[0]),
		.TECC1(nv_tecc[1]),
		.BIAS0(nv_bias[0]),
		.BIAS1(nv_bias[1]),
		.Q(ctrl_out),
		.WE_NV(nv_we),
		.OE_NV(),
		.ME_NV(),
		.ADR_NV(),
		.D_NV(),
		.SME_NV(nv_sme),
		.SI_NV(nv_si),
		.SO_NV(nv_so)
	);

	// read data mux;
	// select data from array or regs;
	// zero output if not selected;

	wire [31:0] nms_out;		// ctrl read data;
	reg [31:0] v_out;			// sum of all read data;

	assign nms_out = { 16'd0,
		nms_ready, nms_pass, nms_keep, 2'd0, nms_cmd,
		1'b0, v_div };

	always @(posedge clk)
	begin
		v_out <= ({32{v_rsel[0]}} & nv_out)
			| ({32{v_rsel[1]}} & ctrl_out)
			| ({32{v_rsel[2]}} & nms_out);
	end
`endif // !`ifdef FPGA


endmodule

