// gi_di.v Frank Berndt
// GC disc interface DI;
// :set tabstop=4

module gi_di (
	clk, reset,
	di_in, di_out, di_oe,
	di_brk_in, di_brk_out, di_brk_oe,
	di_dir, di_hstrb, di_dstrb, di_err, di_cover, di_reset,
	reg_sel, reg_addr, reg_we, reg_wdata, reg_rdata,
	sram_req, sram_gnt, sram_addr, sram_we, sram_wdata, sram_rdata,
	secure, sec_rsp,
	gc_busy, dc_load, gc_di_size, gc_di_out, gc_di_ptr,
	di_busy, di_intr
);
	input clk;					// core clock;
	input reset;				// system reset;

	// di bus interface;

	input [7:0] di_in;			// DI data from input cell;
	output [7:0] di_out;		// DI data to output cell;
	output di_oe;				// DI output enable;
	input di_brk_in;			// DI break from input cell;
	output di_brk_out;			// DI break to ouput cell;
	output di_brk_oe;			// DI break output enable;
	input di_dir;				// DI direction;
	input di_hstrb;				// DI host strobe;
	output di_dstrb;			// DI drive strobe;
	output di_err;				// DI error;
	output di_cover;			// DI cover;
	input di_reset;				// DI reset;

	// register interface;

	input reg_sel;				// register select;
	input [7:0] reg_addr;		// register address decodes;
	input reg_we;				// register write enable;
	input [31:0] reg_wdata;		// register write data;
	output [31:0] reg_rdata;	// register read data;

	// sram interface;

	output sram_req;			// sram request;
	input sram_gnt;				// sram grant;
	output [16:2] sram_addr;	// sram address;
	output sram_we;				// sram write enable;
	output [31:0] sram_wdata;	// sram write data;
	input [31:0] sram_rdata;	// sram read data;

	// security environment;

	input secure;				// in secure mode;
	output sec_rsp;				// response must go through crypto;

	// gc interface;

	input gc_busy;				// gc is busy;
	input dc_load;				// load di from gc;
	input [11:0] gc_di_size;	// gc di request size;
	input gc_di_out;			// gc di data direction;
	input [16:2] gc_di_ptr;		// di_ptr load value;

	// misc;

	output di_busy;				// di is busy;
	output di_intr;				// di interrupt;

	// important signals;

	reg di_rst;					// di interface reset;
	wire di_busy;				// di is busy;
	reg dir_change;				// change in xfer direction;
	reg sec_rsp;				// require secure response;

	// decode bi register spaces;

	wire ra_conf;				// access DI_CONF;
	wire ra_ctrl;				// access DI_CTRL;
	wire ra_be;					// access DI_BE;

	assign ra_conf = reg_sel & reg_addr[0];
	assign ra_ctrl = reg_sel & reg_addr[1];
	assign ra_be = reg_sel & reg_addr[2];

	// DI_CONF register;
	// only accessible by cpu in secure mode;

	wire di_conf_wr;			// write to DI_CONF;
	reg [31:24] cmd0;			// first secure command;
	reg [23:16] cmd1;			// second secure command;
	reg [6:4] tim_end;			// timing config, 1..7;
	reg [2:0] tim_start;		// timing config, 0..6;
	wire [31:0] di_conf;		// all bits of register;

	assign di_conf_wr = ra_conf & reg_we & secure;

	always @(posedge clk)
	begin
		if(reset) begin
			cmd0 <= 8'ha8;				// READ command;
			cmd1 <= 8'h00;				// not assigned yet;
			tim_end <= 3'd7;			// slowest;
			tim_start <= 3'd0;			// slowest;
		end else if(di_conf_wr) begin
			cmd0 <= reg_wdata[31:24];
			cmd1 <= reg_wdata[23:16];
			tim_end <= reg_wdata[6:4];
			tim_start <= reg_wdata[2:0];
		end
	end

	assign di_conf = { cmd0, cmd1, 9'd0, tim_end, 1'b0, tim_start };

	// DI_CTRL register;
	// can be written while BE is busy;
	// several bits have enables for writes to take effect;
	// nothing in DI_CTRL requires secure mode;

	wire di_ctrl_wr;			// write to DI_CTRL;
	reg [27:24] intr_mask;		// interrupt masks;
	wire intr_mask_wr;			// write interrupt mask;
	wire [31:0] di_ctrl;		// all bits of register;

	assign di_ctrl_wr = ra_ctrl & reg_we;
	assign intr_mask_wr = di_ctrl_wr & reg_wdata[31];

	always @(posedge clk)
	begin
		if(reset)
			intr_mask <= 4'd0;
		else if(intr_mask_wr)
			intr_mask <= reg_wdata[27:24];
	end

	// di interface reset flag;
	// RST bit is set while di_reset is active;
	// software cannot clear RST bit while gc drives di_reset active;

	reg rst_flag;				// di interface reset flag;
	wire rst_clr;				// clear DI_CTRL RST bit;
	wire rst_intr;				// issue reset interrupt;

	assign rst_clr = reset | (di_ctrl_wr & reg_wdata[16]);

	always @(posedge clk)
	begin
		if(di_rst)
			rst_flag <= 1'b1;
		else if(rst_clr)
			rst_flag <= 1'b0;
	end

	assign rst_intr = rst_flag & intr_mask[24];

	// DI_BE register;
	// loaded by cpu for stand-alone operation;
	// loaded by ctrl for complex operation;
	// be_ptr and be_size change per byte;
	// stop on di_rst, direction change;
	// drop OUT requests when a secure response is required;
	// even though gc is required to start a sec_rsp, the cpu
	// is allowed to kill the be, and has to clean up gc state;

	wire di_be_wr;				// write to DI_CTRL;
	reg [12:0] be_size;			// size in bytes - 1;
	reg be_out;					// 0 bu<-di, 1 buf->di;
	reg [16:2] be_ptr;			// pointer bits;
	reg [1:0] be_bidx;			// be byte index;
	wire be_inc;				// increment be_ptr, dec be_size;
	wire be_sec;				// OUT request for secure response;
	wire be_sa;					// valid start by cpu;
	reg be_start;				// start be transfer;
	wire be_done;				// be is done;
	wire be_stop;				// stop be;
	reg be_kill;				// kill be;
	reg be_busy;				// be is busy;
	reg be_w0;					// last word in burst;
	wire be_ptr_inc;			// increment be_ptr;
	wire [31:0] di_be;			// all bits of register;

	assign di_be_wr = ra_be & reg_we & ~di_busy;
	assign be_sec = sec_rsp & reg_wdata[19];
	assign be_sa = di_be_wr & reg_wdata[18] & ~be_sec;
	assign be_done = be_busy & be_size[12];
	assign be_stop = di_rst | be_done | dir_change | be_kill;

	always @(posedge clk)
	begin
		be_start <= be_sa | dc_load;
		be_kill <= ra_be & reg_we & ~reg_wdata[18];
		be_busy <= ~be_stop & (be_busy | be_start);
		if(dc_load) begin
			be_size <= { 1'b0, gc_di_size };
			be_out <= gc_di_out;
			be_bidx <= 2'b0;
		end else if(di_be_wr) begin
			be_size <= { 1'b0, reg_wdata[31:20] };
			be_out <= reg_wdata[19];
			be_bidx <= 2'b0;
		end else if(be_inc) begin
			be_size <= be_size - 1;
			be_bidx <= be_bidx + 1;
		end
		be_w0 <= (be_size[11:2] == 10'd0);
		if(dc_load)
			be_ptr <= gc_di_ptr;
		else if(di_be_wr)
			be_ptr <= reg_wdata[16:2];
		else if(be_ptr_inc)
			 be_ptr <= be_ptr + 1;
	end

	assign di_busy = gc_busy | be_busy;
	assign di_be = { be_size[11:0], be_out, di_busy, 1'd0, be_ptr, 2'd0 };

	// BE states;
	//	IDLE
	//		state after di interface reset;
	//		state between in and out phases;
	//		dir=0/1, hstrb=1, dstrb=1, brk=0, err=1;
	//		cmd reception is stalled until dstrb=0;
	//		reply transmission is stalled until hstrb=0;
	//	RUN
	//		receiver asserts ready, captures bytes on strobes;
	//		transmitter drives data and strobes;
	//		advances to DONE when be_size is done;
	//	DONE
	//		receiver has taken away ready;
	//		transmitter stops driving strobes;
	//		wait for direction change and raise DIR interrupt;

	// done interrupt;
	// raise when mask is enabled and BE is done;
	// raising the intr has higher priority than clearing;

	reg done_intr;				// done interrupt;
	wire done_set;				// raise done interrupt;
	wire done_clr;				// clear done interrupt;

	assign done_set = be_done & intr_mask[25];
	assign done_clr = reset | (di_ctrl_wr & reg_wdata[17]);

	always @(posedge clk)
	begin
		if(done_set)
			done_intr <= 1'b1;
		else if(done_clr)
			done_intr <= 1'b0;
	end

	// di register read mux;
	// output 0 when not selected;
	// next stage will OR data from all the units;

	reg [31:0] reg_rdata;

	always @(posedge clk)
	begin
		reg_rdata <= ({32{ra_conf}} & di_conf)
			| ({32{ra_ctrl}} & di_ctrl)
			| ({32{ra_be}} & di_be);
	end

	// di interface reset;
	// synchronize in flops;
	// set DI_CTRL_RST bit;

	reg di_rst_sync;			// sync flop;

	always @(posedge clk)
	begin
		di_rst_sync <= reset | ~di_reset;
		di_rst <= reset | di_rst_sync;
	end

	// di direction signal;
	// goes directly into oe term for output data;
	// signals that host is changing flow;

	reg [1:0] dir_sync;			// sync flops;
	reg dir_intr;				// dir change interrupt;
	wire dir_set;				// set dir_intr;
	wire dir_clr;				// clear dir_intr;
	wire dir_state;				// current state of di_dir;

	assign di_oe = di_dir;
	assign dir_set = dir_change & intr_mask[26];
	assign dir_clr = reset | (di_ctrl_wr & reg_wdata[18]);

	always @(posedge clk)
	begin
		dir_sync <= { dir_sync[0], di_dir };
		dir_change <= ^dir_sync;
		if(dir_set)
			dir_intr <= 1'b1;
		else if(dir_clr)
			dir_intr <= 1'b0;
	end

	assign dir_state = dir_sync[1];

	// track cmd phase in stream of IN phases;
	// needed to snoop command byte for secure replies;
	// set by di_rst which defaults to IN phase;
	// set by falling edge of di_dir;
	// cleared after first cmd byte has been received from host;

	reg cmd_in;					// cmd IN phase;
	wire set_cmd_in;			// set cmd flag of IN phases;
	wire clr_cmd_in;			// clear cmd flag;

	assign set_cmd_in = di_rst | (dir_change & ~dir_sync[1]);

	always @(posedge clk)
	begin
		if(set_cmd_in)
			cmd_in <= 1'b1;
		else if(clr_cmd_in)
			cmd_in <= 1'b0;
	end

	// latch data from host;
	// on the rising edge of di_hstrb;
	// transfer di_hstrb and di_hin into clk domain;
	// drop strobes if not busy with in burst;

	reg [7:0] di_hin;			// captured host data;
	reg [1:0] hstrb_sync;		// di_hstrb sync pipe;
	reg hstrb;					// strobe in clk domain;

	always @(posedge di_hstrb)
		di_hin <= di_in;

	always @(posedge clk)
	begin
		hstrb_sync <= { hstrb_sync[0], di_hstrb };
		hstrb <= be_busy & ~be_out & (hstrb_sync == 2'b01);
	end

	// device ready signal;
	// important observations:
	// - ready only matters for in phase;
	// - di always gets to sram within 3 clks,
	//   so there is no need for device stalls;
	// - using size-1 reduces end detection to upper bits;
	// use device stall to delay begin of in phase;
	// stalls are pushed between BE bursts;

	wire drdy;					// device ready;
	reg drdy_end;				// end burst;
	wire drdy_stop;				// reached last word of burst;

	assign drdy_stop = hstrb & be_w0;

	always @(posedge clk)
		drdy_end <= be_busy & (drdy_end | drdy_stop);

	assign drdy = be_busy & ~be_out & ~drdy_end & ~drdy_stop;

	// host stalling device;
	// wait for ready (hstrb=0) before sending strobes;
	// stop sampling ready once stalled;
	// minumal delay to dstrb_zero is critical;

	reg [1:0] hstall;			// host stall pipe;
	reg dstrb;					// device strobe;
	wire hstall_sample;			// sample host ready;

	assign hstall_sample = ~be_bidx[0];

	always @(posedge clk)
	begin
		if(be_busy == 1'b0)
			hstall <= 2'b11;
		else if(hstrb_sync[1] == 1'b0)
			hstall <= 2'b00;
		else if(dstrb) begin
			if(hstall_sample)
				hstall[0] <= hstrb_sync[0];
			hstall[1] <= hstall[0];
		end
	end

	// di strobe generator;
	// divides clk to make strobe timing programmable;
	// strobe counter has an initial and end, centered around 0;
	// bit 3 becomes the inverted strobe signal;
	// counts: 0 1 D E F 0 1 D E F ...;
	// sample host ready every other byte;
	// stop strobes when byte count is reached;
	// must wait for first word from sram before starting;

	reg [3:0] dstrb_cnt;		// strobe counter;
	wire dstrb_zero;			// zero dstrb_cnt and hold dstrb 1;
	wire dstrb_jump;			// jump counter for strobe timing;
	reg out_ready;				// got word from sram;
	reg brk_stall;				// stall on break;
	wire dstrb_stop;			// when it is save to stop strobes;

	assign dstrb_zero = ~be_busy | ~be_out
		| ~out_ready | be_size[12] | hstall[1] | brk_stall;

	always @(posedge clk)
	begin
		if(dstrb_zero)
			dstrb_cnt <= 4'd0;
		else if(dstrb_jump)
			dstrb_cnt <= { 1'b1, tim_start };
		else
			dstrb_cnt <= dstrb_cnt + 1;
		dstrb <= (dstrb_cnt == 4'hf);
	end

	assign dstrb_jump = (dstrb_cnt == { 1'b0, tim_end });
	assign dstrb_stop = (dstrb_cnt == 4'h0);

	// drive strobe/ready;
	// drive from flop in clk domain;
	// must be driven high during host reset;

	reg di_dstrb;				// drive strobe/ready;

	always @(posedge clk)
	begin
		di_dstrb <= di_rst | ~(drdy | dstrb_cnt[3]);
	end

	// break logic;
	// host drives high and releases brk;
	// resistor pulls brk signal high while released;
	// device stops strobes and drives brk low (ack);
	// device terminates drive operations;
	// device drives brk high (terminate) and releases it;
	// host takes over, drives dir low to issue next cmd;
	// break intr is set on break, cleared on ack;

	reg [1:0] brk_sync;			// sync flops;
	wire brk_state;				// current state of break signal;
	reg brk_act;				// doing break;
	reg brk_out;				// device break;
	reg brk_oe;					// device driving break signal;
	wire brk_edge;				// rising edge on brk;
	wire brk_ack;				// break ack by sw;
	wire brk_term;				// terminate break protocol;
	wire brk_rel;				// release break signal;
	reg brk_intr;				// break interrupt;

	assign brk_edge = (brk_sync == 2'b01);
	assign brk_ack = di_ctrl_wr & reg_wdata[19];
	assign brk_term = di_ctrl_wr & reg_wdata[15];
	assign brk_rel = ~brk_act | brk_term;

	always @(posedge clk)
	begin
		brk_sync <= { brk_sync[0], di_brk_in };
		brk_act <= ~di_rst & (brk_edge ^ brk_act);
		brk_stall <= brk_act & (brk_stall | dstrb_stop);
		if(brk_rel)
			brk_out <= 1'b1;
		else if(brk_ack)
			brk_out <= 1'b0;
		brk_oe <= ~di_rst & ~brk_out;
		if(reset | brk_ack)
			brk_intr <= 1'b0;
		else if(~brk_act & brk_edge)
			brk_intr <= intr_mask[27];
	end

	assign brk_state = brk_sync[1];
	assign di_brk_oe = brk_oe;
	assign di_brk_out = brk_out;

	// drive error signal, active 0;
	// drive from flop in clk domain;
	// must be driven high during host reset;

	reg error;					// error bit;
	wire error_wr;				// write error flag;
	reg di_err;					// output flop;

	assign error_wr = di_ctrl_wr & reg_wdata[9];

	always @(posedge clk)
	begin
		if(di_rst)
			error <= 1'b0;
		else if(error_wr)
			error <= reg_wdata[8];
		di_err <= ~error;
	end

	// cover signal;
	// uninitialized after reset;

	reg cover;					// cover bit;
	wire cover_wr;				// write cover bit;
	reg di_cover;				// output flop;

	assign cover_wr =  di_ctrl_wr & reg_wdata[11];

	always @(posedge clk)
	begin
		if(cover_wr)
			cover <= reg_wdata[10];
		di_cover <= cover;
	end

	// advance be to next byte;

	assign be_inc = hstrb | dstrb;

	// sum up all interrupt sources;

	assign di_intr = rst_intr | done_intr | dir_intr | brk_intr;

	// collect all di_ctrl bits;

	assign di_ctrl = { 4'd0, intr_mask,
		4'd0, brk_intr, dir_intr, done_intr, rst_flag,
		4'd0, 1'b0, cover, 1'b0, error,
		4'd0, brk_state, sec_rsp, cmd_in, dir_state };

	//XXX proto errors;
	// on too many hstrb;

	// di data shift register;
	// di xfers are always multiples of 4 bytes;
	// shift in 4 byte before xfer t odi_buf for IN;
	// shift out 4 bytes before reloading from di_buf for OUT;

	reg [31:0] di_sr;			// shift register;
	reg [31:0] di_buf;			// data buffer register;
	wire di_snext;				// next byte for out phase;
	wire di_sload;				// parallel load for out data;
	wire di_shift;				// byte shift for in & out;
	reg [7:0] di_out;			// flops close to output cell;

	assign di_snext = dstrb_jump & ~hstall[1];
	assign di_sload = di_snext & (be_bidx == 2'd0);
	assign di_shift = hstrb | di_snext;

	always @(posedge clk)
	begin
		if(di_sload)
			di_sr <= di_buf;
		else if(di_shift)
			di_sr <= { di_sr[23:0], di_hin };
		di_out <= di_sr[31:24];
	end

	// di sram buffer register;
	// holds read data from sram before shifting out;
	// buffers di in data before writing to sram;

	reg [1:0] di_bload;			// load di_buf with sram read data;
	reg di_bhold;				// load di_buf with di in word;
	reg di_bread;				// read word from di buffer;

	always @(posedge clk)
	begin
		di_bload <= { di_bload[0], sram_gnt & be_out };
		di_bhold <= hstrb & (be_bidx == 2'd3);
		if(di_bload[1])
			di_buf <= sram_rdata;
		else if(di_bhold)
			di_buf <= di_sr;
		if(be_busy == 1'b0)
			out_ready <= 1'b0;
		else if(di_bload[1])
			out_ready <= 1'b1;
		di_bread <= (be_start & be_out) | di_sload;
	end

	assign sram_wdata = di_buf;

	// compare cmd byte[0] with secure commands;
	// set flag to mark that OUT has to go through crypto;
	// clear cmd_in flag when cmd byte has been sampled;
	// clear sec_rsp on di interface reset;

	wire [1:0] sec_cmp;			// secure cmd compares;
	reg sec_sample;				// sample cmd byte;

	assign sec_cmp[0] = (di_sr[7:0] == cmd0);
	assign sec_cmp[1] = (di_sr[7:0] == cmd1);

	always @(posedge clk)
	begin
		sec_sample <= hstrb & cmd_in;
		if(di_rst)
			sec_rsp <= 1'b0;
		else if(sec_sample)
			sec_rsp <= |sec_cmp;
	end

	assign clr_cmd_in = sec_sample;

	// sram request engine;
	// write di IN data, read di OUT data;

	reg sram_pend;				// sram request pending;
	wire sram_done;				// done with sram request;
	reg sram_we;				// write IN data;

	assign sram_done = reset | sram_gnt;

	always @(posedge clk)
	begin
		sram_we <= ~be_out;
		if(sram_done)
			sram_pend <= 1'b0;
		else if(di_bhold | di_bread)
			sram_pend <= 1'b1;
	end

	assign sram_req = sram_pend & ~sram_gnt;
	assign sram_addr = be_ptr;
	assign be_ptr_inc = sram_gnt;

	// simulator init;
	// fill reg bits to inverse of reset defaults;
	// fill undefined bits with randoms;
	// needed, so no Xs move across PCI (parity);

`ifdef	SIM
	initial
	begin
		{ cmd0, cmd1, tim_end, tim_start } = { 8'h55, 8'hff, 3'd0, 3'd7 };
		intr_mask = {4{1'b1}};
		brk_intr = 1'b1;
		dir_intr = 1'b1;
		done_intr = 1'b1;
		rst_flag = 1'b1;
		{ cover, error, sec_rsp } = $random;
		brk_sync = {2{1'b1}};
		cmd_in = 1'b1;
		dir_sync = {2{1'b1}};
		{ be_size, be_out, be_ptr } = $random;
	end
`endif	// SIM

endmodule

