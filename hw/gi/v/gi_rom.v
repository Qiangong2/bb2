// gi_sram.v Frank Berndt
// GI ROM wrapper, single-port 2kx32, synchronous;
// :set tabstop=4

module gi_rom (
	clk, addr, rdata
);
	input clk;				// clock;
	input [10:0] addr;		// address;
	output [31:0] rdata;	// read data;

`ifdef	GI_ROM_MODEL

   // synopsys translate_off
   
	// sync ROM behavioral model;

	reg [14:0] r_addr;		// address reg;

	reg [31:0] rom [2*1024-1:0];

	// write at rising edge;

	always @(posedge clk)
	begin
		r_addr <= addr;
	end

	// read at falling edge;

	reg [31:0] rdata;		// read data;

	always @(negedge clk)
	begin
		rdata <= rom[r_addr];
	end

	// rom backdoor access;

	// fill word at address;

	task put;
		input [10:0] addr;
		input [31:0] data;
		begin
			rom[addr] = data;
		end
	endtask

	// get word of address;

	function [31:0] get;
		input [10:0] addr;
		begin
			get = rom[addr];
		end
	endfunction

	// fill rom with value;

	task fill;
		input [31:0] val;
		integer n;
		begin
			$display("%t: %M val=%h", $time, val);
			for(n = 0; n < (`GI_ROM_SIZE / 4); n = n + 1)
				rom[n] = val;
		end
	endtask

	// fill rom with randoms;

	task random;
		integer n;
		begin
			$display("%t: %M", $time);
			for(n = 0; n < (`GI_ROM_SIZE / 4); n = n + 1)
				rom[n] = $random;
		end
	endtask

	// init rom;

	initial
		fill(32'd0);

   // synopsys translate_on

`else // !`ifdef GI_ROM_MODEL

 `ifdef FPGA

   // Altera ROM Model
   
	rom2kx32 u_rom2kx32(
		.address(addr),
		.clock(clk),
		.q(rdata)
		);
   
   // synopsys translate_off
   
	// rom backdoor access;
   
  `define rom_data_array u_rom2kx32.inst.mem
   
	// fill word at address;
   
	task put;
		input [10:0] addr;
		input [31:0] data;
		begin
			`rom_data_array[addr] = data;
		end
	endtask
   
	// get word of address;

	function [31:0] get;
		input [10:0] addr;
		begin
			get = `rom_data_array[addr];
		end
	endfunction

	// fill rom with randoms;

	task random;
		integer n;
		begin
			$display("%t: %M", $time);
			for(n = 0; n < (`GI_ROM_SIZE / 4); n = n + 1)
			  `rom_data_array[n] = $random;
		end
	endtask
   
	// fill rom with value;

	task fill;
		input [31:0] val;
		integer n;
		begin
			$display("%t: %M val=%h", $time, val);
			for(n = 0; n < (`GI_ROM_SIZE / 4); n = n + 1)
				`rom_data_array[n] = val;
		end
	endtask

	// init rom;

	initial
		fill(32'd0);

   // synopsys translate_on
   
 `else // !`ifdef FPGA
   
	// rtl model for rom;
	// XXX

 `endif // !`ifdef FPGA
   
`endif // !`ifdef GI_ROM_MODEL


endmodule

