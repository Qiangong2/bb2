// gi_pci.v Frank Berndt;
// gi pci top level;
// :set tabstop=4

// io cells are outside this module;
// clk and pciclk must be be rising edge aligned;
// clk must be an integer multiple of pciclk;
// ALI's pci implementation is muxed on the output
// path, thus requiring loopback through the pins;
// however, to shield GI traffic an internal loopback
// has to be added;
// PCI_LOCK is not implemented in ALI chip;

// all x_*** signals are important controls that
// cross the pci/clk domain boundaries;

// synthesis note:
// make sure no signal is driven from gi clk domain
// onto the pci bus, to avoid timing paths to be considered
// by synthesis tools;

// XXX move virage to PCI clock domain;

`timescale 1ns/1ns

`define	x_del	#1				// delay for clk crossing;

module gi_pci (
	clk, clk_lock, pin_rst, sys_rst, cpu_rst,
	alt_boot, test_in, test_ena,
	gc_rst, sec_intr, sec_ack,
	i2c_clk, i2c_din, i2c_dout, i2c_doe,
	pci_clk, pci_rst, pci_req, pci_gnt, pci_ognt, pci_intr,
	ad_in, ad_out, ad_oe, cbe_in, cbe_out, cbe_oe,
	par_in, par_out, par_oe, frame_in, frame_out, frame_oe,
	devsel_in, devsel_out, devsel_oe,
	irdy_in, irdy_out, irdy_oe, trdy_in, trdy_out, trdy_oe,
	stop_in, stop_out, stop_oe,
	di_in, di_out, di_oe,
	di_brk_in, di_brk_out, di_brk_oe,
	di_dir, di_hstrb, di_dstrb, di_err, di_cover, di_reset,
	ais_clk, ais_lr, ais_d
);
	input clk;					// gi core clock;
	input clk_lock;				// clk is stable, pll locked;
	input pin_rst;				// reset from pin;
	output sys_rst;				// system reset;
	input cpu_rst;				// cpu reset, active 0;
	input alt_boot;				// alternative boot;
	input test_in;				// test enable pin on engr chips;
	output test_ena;			// enable test/debug logic;

	// MCM i2c serial flash;

	output i2c_clk;				// i2c clock;
	input i2c_din;				// i2c data from io cell;
	output i2c_dout;			// i2c data to io cell;
	output i2c_doe;				// i2c data output enable;

	// flipper control;

	output gc_rst;				// reset to flipper;
	output sec_intr;			// cause secure exception;
	input sec_ack;				// secure exception fetch;

	// pci interface;

	input pci_clk;				// gi pci clock;
	input pci_rst;				// pci bus reset;
	input [31:0] ad_in;			// addr/data inputs;
	output [31:0] ad_out;		// addr/data outputs;
	output ad_oe;				// addr/data output enable;
	input [3:0] cbe_in;			// byte enable inputs;
	output [3:0] cbe_out;		// byte enable outputs;
	output cbe_oe;				// byte enable output enable;
	input par_in;				// parity from master;
	output par_out;				// parity when master;
	output par_oe;				// parity output enable;
	input frame_in;				// frame when target;
	output frame_out;			// frame when master;
	output frame_oe;			// frame output enable;
	input devsel_in;			// device selected;
	output devsel_out;			// device select;
	output devsel_oe;			// device select output enable;
	input irdy_in;				// initiator ready;
	output irdy_out;			// ready when master;
	output irdy_oe;				// irdy output enable;
	input trdy_in;				// target ready;
	output trdy_out;			// when target;
	output trdy_oe;				// trdy output enable;
	input stop_in;				// initiator stops transfer;
	output stop_out;			// target stops;
	output stop_oe;				// stop output enable;
	output pci_req;				// pci bus request;
	input pci_gnt;				// bus granted;
	input pci_ognt;				// OR of other grants, not host;
	output pci_intr;			// interrupt to processor;

	// disk interface;

	input [7:0] di_in;			// DI data from input cell;
	output [7:0] di_out;		// DI data to output cell;
	output di_oe;				// DI output enable;
	input di_brk_in;			// DI break from input cell;
	output di_brk_out;			// DI break to output cell;
	output di_brk_oe;			// DI break output enable;
	input di_dir;				// DI direction;
	input di_hstrb;				// DI host strobe;
	output di_dstrb;			// DI drive strobe;
	output di_err;				// DI error;
	output di_cover;			// DI cover;
	input di_reset;				// DI reset;

	// ais interface;

	input ais_clk;				// AIS clock from GC;
	input ais_lr;				// AIS left/right signal;
	output ais_d;				// AIS data out;

	// gi interface;

	wire [17:2] sys_addr;		// cpu address;
	wire [2:0] sys_cmd;			// bus command;
	wire [31:0] sys_in;			// write data, cpu and dma;
	wire [31:0] sys_out;		// read data. cpu and dma;
	wire dma_req;				// request a dma transfer;
	wire [1:0] dma_bx;			// burst control, [1]=idx, [0] 0=64, 1=218;
	wire [31:0] dma_addr;		// dma physical address;
	wire dma_wr;				// dma write req to memory;
	wire dma_done;				// dma done without errors;
	wire dma_retry;				// retry dma;
	wire [2:0] dma_err;			// dma master/target abort;

	assign dma_addr[31:28] = 4'd0;
	assign dma_addr[5:0] = 6'd0;

	// reset control;
	// pin_rst comes from pads and is combined with
	// the gi internal power-on reset;
	// the resulting sys_rst resets the chip;
	// pci_rst and cpu_rst come back in, make gi_rst;
	// pci_rst resets pci logic and gi;
	// gi_rst resets secure environment;

	wire gi_rst;		// gi internal reset, active 0;

	// XXX instantiate pon reset cell;

`ifdef FPGA
	wire pon_rst;		// power-on reset, active 0;
   assign pon_rst = 1'b1;    // power-on reset not supported on FPGA
`else
	reg pon_rst;		// power-on reset, active 0;
   // synopsys translate_off
   initial
	begin
		pon_rst = 0;
		#100;
		pon_rst = 1;
	end     
   // synopsys translate_on
`endif
     
   assign sys_rst = pin_rst & pon_rst;
	assign gi_rst = ~pci_rst | ~cpu_rst;

	// instantiate gi with system interface;

	gi gi (
		.clk(clk),
		.clk_lock(clk_lock),
		.reset(gi_rst),
		.alt_boot(alt_boot),
		.test_in(test_in),
		.test_ena(test_ena),
		.gc_rst(gc_rst),
		.sec_intr(sec_intr),
		.sec_ack(sec_ack),
		.i2c_clk(i2c_clk),
		.i2c_din(i2c_din),
		.i2c_dout(i2c_dout),
		.i2c_doe(i2c_doe),
		.sys_addr(sys_addr),
		.sys_cmd(sys_cmd),
		.sys_in(sys_in),
		.sys_out(sys_out),
		.sys_intr(pci_intr),
		.dma_req(dma_req),
		.dma_addr(dma_addr[27:6]),
		.dma_bx(dma_bx),
		.dma_wr(dma_wr),
		.dma_done(dma_done),
		.dma_retry(dma_retry),
		.dma_err(dma_err),
		.di_in(di_in),
		.di_out(di_out),
		.di_oe(di_oe),
		.di_brk_in(di_brk_in),
		.di_brk_out(di_brk_out),
		.di_brk_oe(di_brk_oe),
		.di_dir(di_dir),
		.di_hstrb(di_hstrb),
		.di_dstrb(di_dstrb),
		.di_err(di_err),
		.di_cover(di_cover),
		.di_reset(di_reset),
		.ais_clk(ais_clk),
		.ais_lr(ais_lr),
		.ais_d(ais_d)
	);

	// put pci input addr/data/cbe/par into flops;
	// write data to gi come directly from ad_ir;

	reg [31:0] ad_ir;			// addr/data input flops;
	reg [3:0] cbe_ir;			// cbe input flops;
	reg par_ir;					// parity input flop;
	reg irdy_ir;				// irdy input flop;
	reg gnt_ir;					// pci_gnt input flop;

	always @(posedge pci_clk)
	begin
		ad_ir <= ad_in;
		cbe_ir <= cbe_in;
		par_ir <= par_in;
		irdy_ir <= ~irdy_in;
		gnt_ir <= ~pci_gnt;
	end

	assign sys_in = ad_ir;

	// parity generation;
	// agent that drives ad must drive par one clock later;

	reg par_oe;					// parity output enable;

	always @(posedge pci_clk)
	begin
		par_oe <= ad_oe;
	end

	assign par_out = ^{ ad_ir, cbe_ir };

	// XXX parity checking;

	// detect access to gi space;
	// accesses to 'h1fcX_XXXX are recognized;
	// however, addr[19:18] must be 2'b00 to be forwarded to gi;

	wire t_spc;

	assign t_spc = (ad_ir[31:20] == (`GI_BASE >> 20));

	// decode target commands;
	// the ALI host only issues;
	//		Configuration Read/Write;
	//		IO Read/Write;
	//		Memory Read/Write;
	// however, if simple memory commands are implemented,
	// then a target must respond to all memory commands;
	// all others are not supported;
	// gi does not respond to config space requests,
	// because everything must be setup at boot time;

	wire tcmd_read;				// target read;
	wire tcmd_write;			// target write;
	wire tcmd_rw;				// legal read/write command;
	wire tcmd_err;				// illegal target command;

	assign tcmd_read = (cbe_ir == 4'b0110)		// memory read;
		| (cbe_ir == 4'b1100)					// memory read multiple;
		| (cbe_ir == 4'b1110);					// memory read line;
	assign tcmd_write = (cbe_ir == 4'b0111)		// memory write;
		| (cbe_ir == 4'b1111);					// memory write & inval;
	assign tcmd_rw = tcmd_read | tcmd_write;
	assign tcmd_err = ~tcmd_rw;

	// data valid when irdy=0 and trdy=0;
	// separate target and master for *rdy loading;
	// target done when also frame=1;

	wire data_xfer;				// bus data transfer;
	reg data_val;				// data was transferred;
	wire t_last;				// last target transfer;

	assign data_xfer = ~irdy_in & ~trdy_in;
	assign t_last = frame_in & data_xfer;

	always @(posedge pci_clk)
	begin
		data_val <= data_xfer;
	end

	// gi only supports full 32-bit word accesses;
	// all byte enables must be active in data phases;
	// do target abort is not so;

	wire t_be_err;				// byte-enable error;
	wire t_abort_be;			// abort due to BE error;

	assign t_be_err = |cbe_ir;
	assign t_abort_be = t_be_err & irdy_ir;

	// detect assertion of frame;
	// bus being requires at least one deasserted frame;
	// this works even for fast back-to-back transactions;

	reg [1:0] frame_ir;			// frame input flop;
	wire frame_start;			// start of transaction;
	wire t_start;				// gi claims target;

	always @(posedge pci_clk)
	begin
		if(pci_rst == 1'b0)
			frame_ir <= 2'b11;
		else
			frame_ir <= { frame_ir[0], frame_in };
	end

	assign frame_start = (frame_ir == 2'b10);
	assign t_start = frame_start & t_spc;

	// hold on to target access type;

	reg t_read;					// target read;
	reg t_abort_cmd;			// target cmd abort;

	always @(posedge pci_clk)
	begin
		if(t_start) begin
			t_read <= tcmd_read;
			t_abort_cmd <= tcmd_err;
		end
	end

	// drive device select;
	// devsel is always driven by target;
	// implement medium device speed;
	// claim access if gi space is selected
	// and if access did came from host bridge;
	// devsel needs to be driven high, then released;

	reg pci_ognt_ir;			// grants other than host;
	reg devsel_out;				// devsel output flop;
	wire devsel_clr;			// deassert devsel;
	wire devsel_set;			// assert devsel;
	reg devsel_del;				// delayed devsel for oe;
	wire t_abort;				// target abort;
	wire t_ad_oe;				// target ad oe;
	wire t_dphase;				// target data phase;

	assign devsel_clr = ~pci_rst | t_abort | t_last;
	assign devsel_set = ~pci_ognt_ir & t_start;

	always @(posedge pci_clk)
	begin
		pci_ognt_ir <= pci_ognt;
		if(devsel_clr)
			devsel_out <= 1'b1;
		else if(devsel_set)
			devsel_out <= 1'b0;
		devsel_del <= ~devsel_out;
	end

	assign t_dphase = ~devsel_out;
	assign devsel_oe = t_dphase | devsel_del;
	assign t_ad_oe = (t_dphase & t_read);

	// target ready;
	// once ready, the gi never stalls as a target;

	reg trdy_out;				// target ready;
	wire trdy_clr;				// deassert trdy;
	wire trdy_set;				// assert trdy;
	wire trdy_write;			// trdy for writes;
	reg trdy_read;				// trdy for reads;

	assign trdy_clr = ~pci_rst | t_last;
	assign trdy_set = trdy_read | trdy_write;
	assign trdy_write = t_start & tcmd_write;

	always @(posedge pci_clk)
	begin
		if(trdy_clr)
			trdy_out <= 1'b1;
		else if(trdy_set)
			 trdy_out <= 1'b0;
		trdy_read <= t_start & tcmd_read;
	end

	assign trdy_oe = devsel_oe;

	// target termination;
	// gi never issues target retry or disconnect;
	// unsupported commands and BE errors cause target aborts;
	// stop must deassert after frame deasserted;

	reg stop_out;				// stop for target abort;
	wire stop_clr;				// deassert stop;

	assign stop_clr = ~pci_rst | frame_in;
	assign t_abort = t_dphase & (t_abort_cmd | t_abort_be);

	always @(posedge pci_clk)
	begin
		if(stop_clr)
			stop_out <= 1'b1;
		else if(t_abort)
			stop_out <= 1'b0;
	end

	assign stop_oe = devsel_oe;

	// master logic;
	// dma is fixed at 64 or 128 bytes (16/32 words);
	// ALI bridge being target will never retry
	// or disconnect, but can issue target abort;

	// transfer dma_req to pci domain;
	// request bus on rising edge of dma_req;
	// release request on grant;

	wire `x_del x_dma_req;		// clock crossing;
	wire `x_del x_dma_b128;		// burst 128 bytes;
	reg dma_req_del;			// delayed dma_req in pci_clk;
	reg pci_req;				// pci bus request;
	reg pci_b128;				// burst 64=0 or 128=1 bytes;
	wire pci_req_clr;			// clear pci request;
	wire pci_req_set;			// issue pci request;
	wire req_ack;				// ack request;

	assign x_dma_req = dma_req;
	assign x_dma_b128 = dma_bx[0];
	assign pci_req_clr = ~pci_rst | req_ack;
	assign pci_req_set = x_dma_req & ~dma_req_del;

	always @(posedge pci_clk)
	begin
		dma_req_del <= x_dma_req;
		if(pci_req_clr) begin
			pci_req <= 1'b1;
			pci_b128 <= 1'b0;
		end else if(pci_req_set) begin
			pci_req <= 1'b0;
			pci_b128 <= x_dma_b128;
		end
	end

	// start master cycle;
	// pipe start for master-abort;

	wire m_idle;				// bus idle & grant;
	reg m_start;				// start master cycle;

	assign m_idle = frame_in & irdy_in & ~pci_gnt;

	always @(posedge pci_clk)
	begin
		m_start <= m_idle & ~pci_req;
	end

	assign req_ack = m_start;

	// drive frame when master;
	// assert coincident with address phase;
	// deassert on master-abort and target-aborts;

	reg m_frame;				// frame when master;
	reg m_dphase;				// master data phase;
	wire frame_clr;				// clear master frame;
	wire frame_set;				// set master frame;
	wire frame_act;				// frame active;
	wire mabort_frame;			// master-abort frame;
	wire mterm_frame;			// terminate frame;
	wire mstop_frame;			// stop frame for abort/retry;

	assign frame_clr = ~pci_rst | mabort_frame | mterm_frame | mstop_frame;
	assign frame_set = m_start;

	always @(posedge pci_clk)
	begin
		if(frame_clr)
			m_frame <= 1'b0;
		else if(frame_set)
			m_frame <= 1'b1;
	end

	assign frame_act = m_start | m_frame;
	assign frame_out = ~frame_act | mabort_frame;
	assign frame_oe = frame_act | m_dphase;

	// master irdy;
	// gi never stalls irdy;
	// assert two clocks after frame;
	// driven high after last data item and released;

	reg irdy_out;				// master irdy;
	wire irdy_clr;				// deassert irdy;
	wire mabort_irdy;			// master-abort irdy;
	wire mterm_irdy;			// terminate irdy;
	wire mstop_irdy;			// stop irdy for abort/retry;
	reg irdy_oe;				// master drives irdy;
	wire m_act;					// master active;

	assign irdy_clr = ~pci_rst | mabort_irdy | mterm_irdy | mstop_irdy;

	always @(posedge pci_clk)
	begin
		if(irdy_clr)
			m_dphase <= 1'b0;
		else if(m_start)
			m_dphase <= 1'b1;
		irdy_out <= irdy_clr | (irdy_out & ~m_dphase);
		irdy_oe <= m_act;
	end

	assign m_act = m_start | m_dphase;

	// wait for devsel from target;
	// master abort after 4 clocks;

	reg [2:0] macnt;			// master abort counter;
	wire macnt_z;				// zero macnt;
	wire macnt_inc;				// increment abort counter;
	wire `x_del x_mabort;		// dma error master abort;

	assign macnt_z = ~pci_rst | ~m_frame;
	assign macnt_inc = devsel_in;

	always @(posedge pci_clk)
	begin
		if(macnt_z)
			macnt <= 3'd0;
		else if(macnt_inc)
			macnt <= macnt + 1;
	end

	assign mabort_frame = macnt[2];
	assign mabort_irdy = macnt[2];
	assign x_mabort = mabort_irdy;
	assign dma_err[0] = x_mabort;

	// detect target aborts;
	// mtabort is set on target abort;
	// mretry is set for retry/disconnect, dma will be retried;

	reg mtabort;				// target signalled abort;
	reg mretry;					// target signalled retry;
	wire `x_del x_mtabort;		// xfer to clk domain;
	wire `x_del x_mretry;		// xfer to clk domain;

	assign mstop_frame = ~frame_out & ~stop_in;

	always @(posedge pci_clk)
	begin
		mtabort <= mstop_frame & devsel_in;
		mretry <= mstop_frame & ~devsel_in;
	end

	assign mstop_irdy = (mtabort | mretry);
	assign x_mtabort = mtabort;
	assign dma_err[1] = x_mtabort;
	assign x_mretry = mretry;
	assign dma_retry = x_mretry;

	// word burst counter;
	// zero'ed at start of transaction;
	// increments after each data transaction,
	// independent of target aborts or retries;
	// for 16 word burst start burst_cnt at 16;

	reg [4:0] burst_cnt;		// burst counter;
	wire burst_inc;				// increment burst_cnt;

	always @(posedge pci_clk)
	begin
		if(~m_act)
			burst_cnt <= { ~pci_b128, 4'd0 };
		else if(burst_inc)
			burst_cnt <= burst_cnt + 1;
	end

	assign burst_inc = ~irdy_out & ~trdy_in;
	assign mterm_frame = (burst_cnt == 5'd30) & burst_inc;
	assign mterm_irdy = (burst_cnt == 5'd31) & burst_inc;

	// dma is done when no target abort/retry;
	// dma_retry can be ignored when dma_done is active,
	// because all 16/32 words have been moved already;
	// do not send done if gi took its request away;

	reg burst_done;				// burst done;
	wire `x_del x_dma_done;		// xfer to clk domain;

	always @(posedge pci_clk)
	begin
		burst_done <= dma_req_del & mterm_irdy;
	end

	assign x_dma_done = burst_done;
	assign dma_done = x_dma_done;

	// access to gi system interface;
	// create access pulse for gi clk domain;
	// active for single clock every three clocks;

	wire `x_del x_gi_acc;		// starts from pci_clk domain;
	reg gi_acc_del;				// delayed gi_acc in clk domain;
	wire gi_acc;				// gi access;
	reg gi_dma;					// master dma;
	reg gi_write;				// write to gi;
	reg [2:0] gi_pipe;			// gi cycle pipe;
	wire gi_more;				// more gi cycles;

	assign x_gi_acc = t_start | m_start;
	assign gi_acc = ~gi_acc_del & x_gi_acc;

	always @(posedge clk)
	begin
		gi_acc_del <= x_gi_acc;
		if(gi_rst)
			gi_dma <= 1'b0;
		else if(gi_acc) begin
			gi_dma <= m_start;
			gi_write <= m_start? ~dma_wr : tcmd_write;
		end
		if(gi_rst)
			gi_pipe <= 3'd0;
		else if(gi_acc)
			gi_pipe <= 3'd1;
		else
			gi_pipe <= { gi_pipe[1:0], gi_more };
	end

	assign gi_more = gi_pipe[2] & (t_dphase | m_dphase);

	// gi system command;
	// same for target or master,
	// except for the meaning of direction;

	assign sys_cmd[0] = ~gi_write & gi_pipe[0];
	assign sys_cmd[1] = gi_write & gi_pipe[2] & data_val;
	assign sys_cmd[2] = gi_dma & |sys_cmd[1:0];

	// latch target address and cmd error flag;
	// transfer into clk domain is not critical;
	// only bits [6:2] matter for dma cycles;

	reg [19:0] addr;			// gi access address;
	wire [19:0] addr_bst;		// dma burst address;
	wire addr_load;				// load address;
	wire addr_rinc;				// increment for reads;
	wire addr_winc;				// increment for writes;
	wire addr_inc;				// increment address;

	assign addr_bst = { 13'd0, dma_bx[1], 6'd0 };
	assign addr_load = gi_acc;
	assign addr_winc = sys_cmd[1];
	assign addr_inc = addr_winc | addr_rinc;

	always @(posedge clk)
	begin
		if(addr_load)
			addr <= m_start? addr_bst : ad_ir[19:0];
		else if(addr_inc)
			addr[19:2] <= addr[19:2] + 1;
	end

	assign sys_addr = addr[17:2];

	// encode dma command;
	// dma_wr=0 -> MEM_READ;
	// dma_wr=1 -> MEM_WRITE;
	// can always be driven, because cbe_oe really enables it;
	// isolate with flops in pci clock domain;

	reg [3:0] cbe_out;			// synthesis isolation flops;
	wire cbe_all;				// all byte enables for master data;
	wire m_ad_oe;				// master ad oe;

	always @(posedge pci_clk)
	begin
		cbe_out <= cbe_all? 4'd0 : dma_wr? 4'b0111 : 4'b0110;
	end

	assign cbe_all = m_act;
	assign cbe_oe = m_act;

	// drive data bus;
	// only for writes to pci bus;
	// basically a copy of irdy_out that is direction dependent;

	reg m_ad_we;				// enable write data;

	always @(posedge pci_clk)
	begin
		if(irdy_clr)
			m_ad_we <= 1'b0;
		else if(m_start)
			m_ad_we <= dma_wr;
	end

	assign m_ad_oe = m_start | m_ad_we;

	// data path to pci;
	// need two bufers due to 4 clk latency of gi;
	// read data when target;
	// dma address or dma write data when master;

	wire [31:0] `x_del x_out;	// clk domain crossing;
	reg [31:0] ad_buf;			// flops to pci bus;
	reg [31:0] ad_del;			// delayed ad_out for irdy stall;
	wire sel_start;				// master/target start;
	reg sel_pref;				// first two prefetch;
	wire sel_addr;				// select address;
	reg sel_data;				// ad data select;

	assign x_out = sys_out;
	assign sel_addr = ~t_dphase & ~m_dphase;
	assign sel_start = t_start | m_start;

	always @(posedge pci_clk)
	begin
		sel_pref <= sel_start;
		sel_data <= sel_start | sel_addr | sel_pref | data_xfer;
		if(sel_addr)
			ad_buf <= dma_addr;
		else if(sel_data)
			ad_buf <= x_out;
		if(sel_data)
			ad_del <= ad_buf;
	end

	assign addr_rinc = ~gi_write & gi_pipe[2] & sel_data;
	assign ad_out = sel_data? ad_buf : ad_del;
	assign ad_oe = t_ad_oe | m_ad_oe;

	assign dma_err[2] = 1'b0; //XXX

endmodule
