// gi.vh Frank Berndt;
// gi definitions;

// GI base address;
// XXX until ALI boot space is fixed;

`undef XXX

`ifdef	ALI_GI
`define	GI_PCI			// on PCI in ALI;
`define	GI_BASE			32'h0800_0000
`else	// ALI_GI
`ifdef	GI_PCI
`define	PCI_HOST		// use pci_host model if not on ALI;
`endif	// GI_PCI
`define	GI_BASE			32'h1fc0_0000
`endif	// ALI_GI

`ifdef FPGA
`undef GI_BASE
`define	GI_BASE			32'h3000_0000
`endif	// FPGA

// maximum available memory space;

`ifdef	ALI_GI
`define	MEM_MAX			32'h0800_0000
`else	// ALI_GI
`define	MEM_MAX			32'h1000_0000
`endif	// ALI_GI
`define	MEM_BASE		32'h0000_0000
`define	MEM_MASK		(`MEM_MAX - 1)

// bread/bwrite sizes;
// ALI can issue 1, 2, and 4 words only;

`define	BSIZE_4			1
`define	BSIZE_8			2
`define	BSIZE_16		4
`define	BSIZE_MAX		4
`define	BSIZE_BITS		(`BSIZE_MAX*32)

// major GI address spaces;

`define	GI_ROM_RST		`GI_BASE + 0
`define	GI_SRAM_RST		`GI_BASE + 'h2_0000
`define	GI_ROM_SWP		`GI_SRAM_RST
`define	GI_SRAM_SWP		`GI_ROM_RST
`define	GI_V			`GI_BASE + 'h1_8000
`define	GI_REGS			`GI_BASE + 'h1_c000

`define	GI_ROM_SIZE		8*1024		// size of rom in bytes;
`define	GI_SRAM_SIZE	96*1024		// size of sram in bytes;
`define	GI_V_SIZE		512			// size of virage in bytes;

// register bases;

`define	BI_BASE			`GI_REGS + 'h000
`define	DI_BASE			`GI_REGS + 'h100
`define	AES_BASE		`GI_REGS + 'h200
`define	SHA_BASE		`GI_REGS + 'h300
`define	DC_BASE			`GI_REGS + 'h400
`define	AIS_BASE		`GI_REGS + 'h500
`define	MC_BASE			`GI_REGS + 'h600

// BI registers;

`define	BI_CTRL			`BI_BASE + 'h00
`define	BI_CTRL_EXEC		32'h8000_0000
`define	BI_CTRL_MASK		32'h4000_0000
`define	BI_CTRL_SIZE		32'h3ff0_0000
`define	BI_CTRL_WRITE		32'h0008_0000
`define	BI_CTRL_PTR			32'h0001_ff80
`define	BI_CTRL_ERRS		32'h0000_0007
`define	BI_CTRL_PERR		32'h0000_0004
`define	BI_CTRL_MERR		32'h0000_0002
`define	BI_CTRL_TERR		32'h0000_0001

`define	BI_MEM			`BI_BASE + 'h10
`define	BI_MEM_PTR			32'h0fff_ff80

`define	BI_INTR			`BI_BASE + 'h20
`define	BI_INTR_DC			32'h0000_0080
`define	BI_INTR_SHA			32'h0000_0040
`define	BI_INTR_MC			32'h0000_0020
`define	BI_INTR_AESE		32'h0000_0010
`define	BI_INTR_AESD		32'h0000_0008
`define	BI_INTR_AIS			32'h0000_0004
`define	BI_INTR_DI			32'h0000_0002
`define	BI_INTR_BI			32'h0000_0001

`define	SEC_MODE		`BI_BASE + 'h40
`define	SEC_MODE_ALT_BOOT	32'h40
`define	SEC_MODE_TEST_IN	32'h20
`define	SEC_MODE_SGI		32'h10
`define	SEC_MODE_STIMER		32'h08
`define	SEC_MODE_SAPP		32'h04
`define	SEC_MODE_RESET		32'h02
`define	SEC_MODE_ON			32'h01

`define	SEC_TIMER		`BI_BASE + 'h50
`define	SEC_TIMER_RST		32'h0100_0000
`define	SEC_TIMER_VAL		32'h00ff_ffff

`define	SEC_SRAM		`BI_BASE + 'h60
`define	SEC_SRAM_PTR		32'h0001_fff0

`define	SEC_ENTER		`BI_BASE + 'h70

// virage spaces;;

`define	V_MEM			`GI_V + 'h000	// memory array;
`define	V_CRSTO_0		`GI_V + 'h200	// ctrl registers;
`define	V_CRSTO_1		`GI_V + 'h204
`define	V_CRM_0			`GI_V + 'h208
`define	V_CRM_1			`GI_V + 'h20c
`define	V_CRM_2			`GI_V + 'h210
`define	V_CRM_3			`GI_V + 'h214
`define	V_REG_6			`GI_V + 'h218	// fixed 8'b01001111;
`define	V_REG_7			`GI_V + 'h21c	// fixed 8'b00000001;
`define	V_CMD			`GI_V + 'h300	// cmd register;

// virage ctrl regster defaults;

`define	V_CRSTO_0_DEF		'h01
`define	V_CRSTO_1_DEF		'h12
`define	V_CRM_0_DEF			'h90
`define	V_CRM_1_DEF			'h96
`define	V_CRM_2_DEF			'h59
`define	V_CRM_3_DEF			'h15
`define	V_REG_6_DEF			'h4f
`define	V_REG_7_DEF			'h01

// DI registers;

`define	DI_CONF			`DI_BASE + 'h00
`define	DI_CONF_CMD0		32'hff00_0000
`define	DI_CONF_CMD1		32'h00ff_0000
`define	DI_CONF_TIM_END		32'h0000_0070
`define	DI_CONF_TIM_START	32'h0000_0007
`define	DI_TIM_GC162		32'h0000_0015

`define	DI_CTRL			`DI_BASE + 'h10
`define	DI_CTRL_MASK_WE		32'h8000_0000
`define	DI_CTRL_BREAK_MASK	32'h0800_0000
`define	DI_CTRL_DIR_MASK	32'h0400_0000
`define	DI_CTRL_DONE_MASK	32'h0200_0000
`define	DI_CTRL_RST_MASK	32'h0100_0000
`define	DI_CTRL_ALL_MASK	32'h0f00_0000
`define	DI_CTRL_BREAK_INTR	32'h0008_0000
`define	DI_CTRL_DIR_INTR	32'h0004_0000
`define	DI_CTRL_DONE_INTR	32'h0002_0000
`define	DI_CTRL_RST_INTR	32'h0001_0000
`define	DI_CTRL_BREAK_TERM	32'h0000_8000
`define	DI_CTRL_COVER_WE	32'h0000_0800
`define	DI_CTRL_COVER		32'h0000_0400
`define	DI_CTRL_ERROR_WE	32'h0000_0200
`define	DI_CTRL_ERROR		32'h0000_0100
`define	DI_CTRL_BREAK_STATE	32'h0000_0008
`define	DI_CTRL_SEC_RSP		32'h0000_0004
`define	DI_CTRL_CMD_IN		32'h0000_0002
`define	DI_CTRL_DIR_STATE	32'h0000_0001

`define	DI_BE			`DI_BASE + 'h20
`define	DI_BE_SIZE			32'hfff0_0000
`define	DI_BE_WRITE			32'h0008_0000
`define	DI_BE_EXEC			32'h0004_0000
`define	DI_BE_PTR			32'h0001_fffc

// AES registers;

`define	AESD_CTRL		`AES_BASE + 'h00
`define	AESE_CTRL		`AES_BASE + 'h40
`define	AES_CTRL_EXEC		32'h8000_0000
`define	AES_CTRL_MASK		32'h4000_0000
`define	AES_CTRL_SIZE		32'h3ff0_0000
`define	AES_CTRL_PTR		32'h0001_fff0
`define	AES_CTRL_DCKEY		32'h0000_0002
`define	AES_CTRL_CHAIN		32'h0000_0001

`define	AESD_IV			`AES_BASE + 'h10
`define	AESE_IV			`AES_BASE + 'h50
`define	AES_IV_OFF			32'h0001_fff0

`define	AES_KEXP		`AES_BASE + 'h60
`define	AES_KEXP_STALL		32'h0000_0010
`define	AES_KEXP_READY		32'h0000_0008
`define	AES_KEXP_BUSY		32'h0000_0004
`define	AES_KEXP_UNIT		32'h0000_0003
`define	AESE_KEXP_MC		32'h0000_0002
`define	AESD_KEXP_DC		32'h0000_0001
`define	AESD_KEXP_MC		32'h0000_0000

`define	AES_KEY			`AES_BASE + 'h70

// SHA1 registers;

`define	SHA_CTRL		`SHA_BASE + 'h00
`define	SHA_CTRL_EXEC		32'h8000_0000
`define	SHA_CTRL_MASK		32'h4000_0000
`define	SHA_CTRL_SIZE		32'h0ff0_0000
`define	SHA_CTRL_WRITE		32'h0008_0000
`define	SHA_CTRL_PTR		32'h0001_ffc0
`define	SHA_CTRL_CHAIN		32'h0000_0001

`define	SHA_BUF			`SHA_BASE + 'h10
`define	SHA_BUF_OFF			32'h0001_fffc

// DC registers;

`define	DC_CTRL			`DC_BASE + 'h00
`define	DC_CTRL_EXEC		32'h8000_0000
`define	DC_CTRL_MASK		32'h4000_0000
`define	DC_CTRL_OP			32'h3000_0000
`define	DC_CTRL_CUR			32'h01f0_0000
`define	DC_CTRL_SG			32'h0001_ff80
`define	DC_CTRL_ERRS		32'h0000_0007
`define	DC_CTRL_PERR		32'h0000_0004
`define	DC_CTRL_MERR		32'h0000_0002
`define	DC_CTRL_TERR		32'h0000_0001

`define	DC_BUF			`DC_BASE + 'h10
`define	DC_BUF_OFF			32'h0001_ff00

`define	DC_HADDR		`DC_BASE + 'h20
`define	DC_HADDR_MEM		32'h0fff_f000

`define	DC_HIDX			`DC_BASE + 'h30
`define	DC_HIDX_H0			32'h0070_0000
`define	DC_HIDX_H1			32'h0001_fffc

`define	DC_DIOFF		`DC_BASE + 'h40
`define	DC_DI_LAST			32'h0fff_0000
`define	DC_DI_FIRST			32'h0000_0fff

`define	DC_IV			`DC_BASE + 'h50
`define	DC_IV_OFF			32'h0001_fff0
`define	DC_H0			`DC_BASE + 'h60
`define	DC_H0_OFF			32'h0001_ff80
`define	DC_H1			`DC_BASE + 'h70
`define	DC_H1_OFF			32'h0001_fff0

// AIS registers;

`define	AIS_CTRL		`AIS_BASE + 'h00
`define	AIS_CTRL_EXEC		32'h8000_0000
`define	AIS_CTRL_MASK		32'h4000_0000
`define	AIS_CTRL_ERRS		32'h0000_0007
`define	AIS_CTRL_PERR		32'h0000_0004
`define	AIS_CTRL_MERR		32'h0000_0002
`define	AIS_CTRL_TERR		32'h0000_0001

`define	AIS_PTR0		`AIS_BASE + 'h10
`define	AIS_PTR1		`AIS_BASE + 'h20
`define	AIS_PTR_ADDR		32'h0fff_f000
`define	AIS_PTR_EMPTY		32'h0000_0001
`define	AIS_PTR_FULL		32'h0000_0000

`define	AIS_BUF			`AIS_BASE + 'h30
`define	AIS_BUF_OFF			32'h0001_ff00

// MC registers;

`define	MC_CTRL			`MC_BASE + 'h00
`define	MC_CTRL_EXEC		32'h8000_0000
`define	MC_CTRL_MASK		32'h4000_0000
`define	MC_CTRL_OP			32'h3000_0000
`define	MC_CTRL_OP_SHA		32'h0000_0000
`define	MC_CTRL_OP_AESE		32'h1000_0000
`define	MC_CTRL_OP_AESD		32'h2000_0000
`define	MC_CTRL_OP_AESDC	32'h3000_0000
`define	MC_CTRL_DROP		32'h0c00_0000
`define	MC_CTRL_DROP0		32'h0800_0000
`define	MC_CTRL_DROP01		32'h0c00_0000
`define	MC_CTRL_CHAIN		32'h0200_0000
`define	MC_CTRL_DCKEY		32'h0100_0000
`define	MC_CTRL_SIZE		32'h0000_fff0
`define	MC_CTRL_ERRS		32'h0000_000f
`define	MC_CTRL_CONFLICT	32'h0000_0008
`define	MC_CTRL_PERR		32'h0000_0004
`define	MC_CTRL_MERR		32'h0000_0002
`define	MC_CTRL_TERR		32'h0000_0001

`define	MC_ADDR			`MC_BASE + 'h10
`define MC_ADDR_MEM			32'h0fff_ffc0

`define	MC_BUF			`MC_BASE + 'h20
`define	MC_BUF_OFF			32'h0001_ff80

