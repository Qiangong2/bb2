// gi_ais.v Frank Berndt
// bus interface unit;
// :set tabstop=4

module gi_ais (
	clk, reset, secure,
	ais_clk, ais_lr, ais_d,
	reg_sel, reg_addr, reg_we, reg_wdata, reg_rdata,
	sram_req, sram_gnt, sram_addr, sram_rdata,
	ais_req, ais_mem, ais_buf, ais_done, ais_abort, ais_err,
	ais_intr
);
	input clk;					// go core clock;
	input reset;				// system reset;
	input secure;				// secure mode;

	input ais_clk;				// AIS clock from GC;
	input ais_lr;				// AIS left/right signal;
	output ais_d;				// AIS data out;

	// register interface;

	input reg_sel;				// register select;
	input [7:0] reg_addr;		// register address decodes;
	input reg_we;				// register write enable;
	input [31:0] reg_wdata;		// register write data;
	output [31:0] reg_rdata;	// register read data;

	// sram interface;

	output sram_req;			// sram request;
	input sram_gnt;				// sram grant;
	output [16:2] sram_addr;	// sram address;
	input [31:0] sram_rdata;	// sram read data;

	// dma interface to bi;

	output ais_req;				// ais dma request;
	output [27:6] ais_mem;		// ais dma address;
	output [16:6] ais_buf;		// dma buffer in sram;
	input ais_done;				// ais dma done;
	input ais_abort;			// ais dma aborted;
	input [2:0] ais_err;		// ais dma error code;

	// misc;

	output ais_intr;			// ais interrupt;

	// decode register writes;
	// secure mode has no impact on AIS registers;

	wire ra_ctrl;			// select AIS_CTRL;
	wire [1:0] ra_ptr;		// select AIS_PTR0/1;
	wire ra_buf;			// select AIS_BUF;
	wire ctrl_wr;			// write AIS_CTRL;
	wire [1:0] ptr_wr;		// write AIS_PTR0/1;
	wire buf_wr;			// write AIS_BUF;

	assign ra_ctrl = reg_sel & reg_addr[0];
	assign ra_ptr[0] = reg_sel & reg_addr[1];
	assign ra_ptr[1] = reg_sel & reg_addr[2];
	assign ra_buf = reg_sel & reg_addr[3];

	assign ctrl_wr = reg_we & ra_ctrl;
	assign ptr_wr[0] = reg_we & ra_ptr[0];
	assign ptr_wr[1] = reg_we & ra_ptr[1];
	assign buf_wr = secure & reg_we & ra_buf;

	// control register;
	// EXEC flag;
	// cleared by reset, reg writes, or when done (same as underrun);
	// set by register write to start new operation;
	// ignore starts when busy, PTRx would cause conflicts;

	wire start;				// start ais stream;
	wire kill;				// kill ais stream;
	wire done;				// done with stream;
	wire stop;				// stop everything;
	reg exec;				// start/stop/busy bit;

	assign start = ~exec & (ctrl_wr & reg_wdata[31]);
	assign kill = ctrl_wr & ~reg_wdata[31];
	assign stop = reset | kill | done;

	always @(posedge clk)
	begin
		exec <= ~stop & (exec | start);
	end

	// buffer offset;
	// base address of the two ping-pong buffers in sram;
	// two buffers, 128-byte each;
	// buffers are forced to 256-byte alignment;
	// one address bit changes when buffers are flipped;
	// always start with sram buffer 0;
	// only writable in secure mode;

	reg [16:8] buf_off;		// buffer offset;
	reg buf_idx;			// which buffer to use;
	wire buf_flip;			// flip sram buffers;

	always @(posedge clk)
	begin
		if(buf_wr)
			buf_off <= reg_wdata[16:8];
		buf_idx <= exec & (buf_idx ^ buf_flip);
	end

	assign ais_buf = { buf_off, buf_idx, 1'b0 };

	// dma pointers;
	// must be aligned on 4kB boundary;
	// only lower 12 bits increment;
	// always start with PTR0;

	reg [27:12] ptr0;		// AIS_PTR0;
	reg [27:12] ptr1;		// AIS_PTR1;
	reg ptr_idx;			// which ptr to use;
	wire ptr_flip;			// flip pointers;

	always @(posedge clk)
	begin
		if(ptr_wr[0])
			ptr0 <= reg_wdata[27:12];
		if(ptr_wr[1])
			ptr1 <= reg_wdata[27:12];
		ptr_idx <= exec & (ptr_idx ^ ptr_flip);
	end

	// interrupt handling;
	// interrupt mask is cleared on reset;
	// setting has higher priority than clearing;
	// clear interrupts when AIS is not busy;

	reg intr_mask;			// interrupt mask;
	reg [1:0] intr;			// interrupt flags per ptr;
	wire [1:0] intr_set;	// raise interrupt;
	wire [1:0] intr_clr;	// clear interrupt;
	wire [1:0] set_empty;	// buffer has been emptied;

	assign intr_clr = {2{~exec}} | ptr_wr;
	assign intr_set = set_empty;

	always @(posedge clk)
	begin
		if(reset | kill)
			intr_mask <= 1'b0;
		else if(ctrl_wr & ~exec)
			intr_mask <= reg_wdata[30];
		intr <= intr_set | (intr & ~intr_clr);
	end

	assign ais_intr = intr_mask & |intr;

	// handle empty flags;
	// writing bit[0] = 1 declares buffer empty;
	// writing bit[0] = 0 declares buffer full;
	// set to 1 when buffer has been processed;

	reg [1:0] empty;		// empty flags;

	always @(posedge clk)
	begin
		if(ptr_wr[0])
			empty[0] <= reg_wdata[0];
		else if(set_empty[0])
			empty[0] <= 1'b1;
		if(ptr_wr[1])
			empty[1] <= reg_wdata[0];
		else if(set_empty[1])
			empty[1] <= 1'b1;
	end

	// ais register read mux;
	// output 0 when not selected;
	// next stage will OR data from all the units;

	reg [2:0] dma_err;		// dma error code;
	wire [31:0] ais_ctrl;	// all bits of AIS_CTRL;
	reg [31:0] reg_rdata;

	assign ais_ctrl = { exec, intr_mask, 27'd0, dma_err };

	always @(posedge clk)
	begin
		reg_rdata <= ({32{ra_ctrl}} & ais_ctrl)
			| ({32{ra_ptr[0]}} & { 4'd0, ptr0, 11'd0, empty[0] })
			| ({32{ra_ptr[1]}} & { 4'd0, ptr1, 11'd0, empty[1] })
			| ({32{ra_buf}} & { 15'd0, buf_off, 8'd0 });
	end

	// dma address counter;
	// upper bits of address are fixed due to 4kB alignment;
	// only lower bits increment;
	// burst size is fixed at 128 bytes;

	reg [11:7] mem_off;		// offset in 4kb buffer;
	wire mem_clr;			// zero offset;
	wire mem_inc;			// increment offset;
	wire mem_last;			// last dma in 4kB buffer;

	always @(posedge clk)
	begin
		if(mem_clr)
			mem_off <= 5'd0;
		else if(mem_inc)
			mem_off <= mem_off + 1;
	end

	assign mem_last = &mem_off;
	assign ais_mem = { ptr_idx? ptr1 : ptr0, mem_off, 1'b0 };

	// request dma read;
	// flip sram buffer after dma burst is done;
	// empty interrupts are raised when the dma has moved
	// the last 128-byte block from main memory to sram,
	// not when the last sample is out the ais interface;
	// hold on to dma error code;

	wire dma_empty;			// this buffer is empty;
	reg dma_req;			// ais dma request;
	wire dma_stop;			// done with dma request;
	reg dma_start;			// issue dma request;
	reg dma_next;			// request next dma burst;
	reg dma_done;			// done with ais dma burst;

	assign dma_empty = ptr_idx? empty[1] : empty[0];
	assign dma_stop = reset | ~exec | ais_done | ais_abort;

	always @(posedge clk)
	begin
		dma_start <= ~dma_empty & ((~exec & start) | dma_next);
		dma_req <= ~dma_stop & (dma_req | dma_start);
		dma_done <= exec & ais_done;
		if(dma_start)
			dma_err <= 3'd0;
		else if(ais_abort)
			dma_err <= ais_err;
	end

	assign ais_req = dma_req;
	assign buf_flip = dma_done;
	assign ptr_flip = dma_done & mem_last;
	assign mem_clr = ~exec | ptr_flip;
	assign mem_inc = dma_done;
	assign set_empty[0] = ptr_flip & ~ptr_idx;
	assign set_empty[1] = ptr_flip & ptr_idx;

	// buffer sram address;
	// counts from 0 to 256/4, then loops around;
	// clear to 0 when not busy with stream;

	wire buf_init;			// zero buf_addr;
	reg [7:2] buf_addr;		// sram buffer index;
	wire buf_last;			// last word of 128-byte buffer;
	wire buf_inc;			// increment buf_addr;
	reg [1:0] buf_val;		// buffer contains samples;
	wire [1:0] buf_full;	// set buffer full;
	wire [1:0] buf_empty;	// set buffer empty;
	wire buf_ready;			// buffer ready with samples;
	reg buf_del;			// delayed buf_inc for done test;

	assign buf_init = ~exec;
	assign buf_full[0] = buf_flip & ~buf_idx;
	assign buf_full[1] = buf_flip & buf_idx;
	assign buf_last = buf_inc & (buf_addr[6:2] == 5'd31);
	assign buf_empty[0] = buf_init | (buf_last & ~buf_addr[7]);
	assign buf_empty[1] = buf_init | (buf_last & buf_addr[7]);
	assign buf_ready = |buf_val;

	always @(posedge clk)
	begin
		if(buf_init)
			buf_addr <= 6'd0;
		else if(buf_inc)
			buf_addr <= buf_addr + 1;
		buf_val <= ~buf_empty & (buf_val | buf_full);
		buf_del <= buf_inc;
	end

	assign sram_addr = { buf_off, buf_addr };
	assign done = buf_del & ~buf_ready;

	// audio stream serializer;
	// audio clock and left/right signal come from flipper;

	// synchronize audio clock;
	// synchronize left/right signal;

	reg [2:0] aclk;			// audio clock;
	reg [2:0] alr;			// audio clock;
	wire aclk_fall;			// falling edge of audio clock;

	always @(posedge clk)
	begin
		aclk <= { aclk[1:0], ais_clk };
		alr <= { alr[1:0], ais_lr };
	end

	assign aclk_fall = (aclk[2:1] == 2'b10);

	// state machine;
	// the host tells us when to start based on ais_lr;

	reg alr_del;			// delayed alr;
	wire alr_rise;			// ais_lr going high;

	always @(posedge clk)
	begin
		if(reset)
			alr_del <= 1'b0;
		else if(aclk_fall)
			alr_del <= alr[2];
	end

	assign alr_rise = ~alr_del & alr[2];

	// request sample from sram;
	// alr_rise indicates start of sample;
	// ais always gets to sram within 2 clocks (if be prempts);
	// with all the delays the first bit is on the pin in 11 clks;
	// ash outputs 0, until next sample is loaded, if any;

	reg sram_pend;			// sram request pending;
	wire sram_get;			// get word from sram;
	wire sram_done;			// done with sram request;
	reg [2:0] sram_pipe;	// read pipe;

	assign sram_get = aclk_fall & alr_rise & buf_ready;
	assign sram_done = reset | sram_gnt;

	always @(posedge clk)
	begin
		if(reset) begin
			sram_pend <= 1'b0;
			sram_pipe <= 3'd0;
		end else begin
			sram_pend <= ~sram_gnt & (sram_pend | sram_get);
			sram_pipe <= { sram_pipe[1:0], sram_gnt };
		end
	end

	assign sram_req = sram_pend & ~sram_gnt; 
	assign buf_inc = sram_pipe[2];

	// request next dma burst;
	// after we pulled sample 7 out of the sram;
	// want to spread out the initial back-to-back dmas
	// a little, but not more than 25% of request rate;

	always @(posedge clk)
	begin
		dma_next <= buf_inc & (buf_addr[6:2] == 5'd7);
	end

	// audio data shift register;
	// upper 16 bits are left channel, lower 16 are right channel;
	// MSB is shifted out first;
	// shift in 0, which is required after end of stream;
	// pass audio data through output flop;

	reg [31:0] ash;			// audio sample shift register;
	wire ash_load;			// load sample;
	wire ash_shift;			// shift out one bit;
	reg ais_d;				// output data flop;

	assign ash_load = sram_pipe[1];
	assign ash_shift = reset | aclk_fall;

	always @(posedge clk)
	begin
		if(ash_load)
			ash <= sram_rdata;
		else if(ash_shift)
			ash <= { ash[30:0], 1'b0 };
		ais_d <= ash[31];
	end

	// simulator init;
	// fill reg bits to inverse of reset defaults;
	// fill undefined bits with randoms;
	// needed, so no Xs move across PCI (parity);

`ifdef	SIM
	initial
	begin
		{ exec, intr_mask } = 2'b11;
		{ dma_err } = $random;
		{ ptr0, empty[0] } = $random;
		{ ptr1, empty[1] } = $random;
		{ buf_off } = $random;
	end
`endif	// SIM

endmodule

