// ali.vh v1 Frank Berndt
// hack for  ALI build method;

// define INC_GI for building with gi_pci;
// if undefined use gi_dummy;

`define	INC_GI

// define ALI_GI if you want GI at pci 0x0800_0000;
// should go away when boot space switched to 0x1fc0_0000;

`define	ALI_GI

// do not want fpga stuff in here;

`undef FPGA

// defines to use faster gi ROM/SRAM models;

`define	GI_ROM_MODEL
`define	GI_SRAM_MODEL

`define SIM

