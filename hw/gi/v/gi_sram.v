// gi_sram.v Frank Berndt
// GI SRAM wrapper, single-port 24kx32, word write-enable;
// :set tabstop=4

module gi_sram (
	clk, addr, we, wdata, rdata
);
	input clk;				// clock;
	input [16:2] addr;		// address;
	input we;				// word write enable;
	input [31:0] wdata;		// write data;
	output [31:0] rdata;	// read data;

`ifdef	GI_SRAM_MODEL

   // synopsys translate_off

	// sync SRAM behavioral model;

	reg [16:2] r_addr;		// address reg;
	reg r_we;				// we reg;
	reg [31:0] r_wdata;		// write data reg;

	reg [31:0] ram [24*1024-1:0];

	// write at rising edge;

	always @(posedge clk)
	begin
		r_addr <= addr;
		r_we <= we;
		r_wdata <= wdata;
		if(r_we === 1'b1)
			ram[r_addr] <= r_wdata;
	end

	// read at falling edge;
	// X output data if cycle was a write;
	// prevents us from relying on write-through sram;

	reg [31:0] rdata;		// read data;

	always @(negedge clk)
	begin
		if(r_we === 1'b0)
			rdata <= ram[r_addr];
		else
			rdata <= 32'bx;
	end

	// assertions;

	wire wdata_x;
	reg got_clk;

	initial
		got_clk = 0;

	assign wdata_x = ^wdata;

	always @(posedge clk)
	begin
		got_clk <= 1;
		if(got_clk) begin
			if(addr >= 24*1024)
				$display("ERROR: %t: %M: addr=0x%h", $time, addr);
			if((we !== 1'b0) & (we !== 1'b1))
				$display("ERROR: %t: %M: we=%b", $time, we);
			if((we === 1'b1) & (wdata_x === 1'bx))
				$display("ERROR: %t: %M: wdata=%b", $time, wdata);
		end
	end

	// sram backdoor access;
	// use the backdoor only when absolutely neccessary,
	// because it cannot be used for code running on cpu;

	// fill word at address;

	task put;
		input [14:0] addr;
		input [31:0] data;
		begin
			ram[addr] = data;
		end
	endtask

	// get word of address;

	function [31:0] get;
		input [14:0] addr;
		begin
			get = ram[addr];
		end
	endfunction

	// fill ram with value;

	task fill;
		input [31:0] val;
		integer n;
		begin
			$display("%t: %M val=%h", $time, val);
			for(n = 0; n < (`GI_SRAM_SIZE / 4); n = n + 1)
				ram[n] = val;
		end
	endtask

	// init sram;

	initial
		fill(32'd0);

   // synopsys translate_on

`else // !`ifdef GI_SRAM_MODEL
   
 `ifdef FPGA // !`ifdef GI_SRAM_MODEL
   
	// Altera memory model
   
   sram24kx16 u0_sram24kx16 (
                            .address(addr),
                            .clock(clk),
                            .data(wdata[15:0]),
                            .wren(we),
                            .q(rdata[15:0])
                            );
   sram24kx16 u1_sram24kx16 (
                            .address(addr),
                            .clock(clk),
                            .data(wdata[31:16]),
                            .wren(we),
                            .q(rdata[31:16])
                            );
   
   // synopsys translate_off
   
	// sram backdoor access;
	// use the backdoor only when absolutely neccessary,
	// because it cannot be used for code running on cpu;
   
  `define ram_data_array_0 u0_sram24kx16.inst.mem
  `define ram_data_array_1 u1_sram24kx16.inst.mem
   
	// fill word at address;
   
	task put;
		input [14:0] addr;
		input [31:0] data;
		begin
			`ram_data_array_0[addr] = data[15:0];
			`ram_data_array_1[addr] = data[31:16];
		end
	endtask
   
	// get word of address;
   
	function [31:0] get;
		input [14:0] addr;
		begin
			get = {`ram_data_array_1[addr],`ram_data_array_0[addr]};
		end
	endfunction
   
	// fill ram with value;

	task fill;
		input [31:0] val;
		integer n;
		begin
			$display("%t: %M val=%h", $time, val);
			for(n = 0; n < (`GI_SRAM_SIZE / 4); n = n + 1) begin
				`ram_data_array_0[n] = val[15:0];
				`ram_data_array_1[n] = val[31:16];
         end
		end
	endtask // fill
   
	// init sram;

	initial
		fill(32'd0);

   // synopsys translate_on
   
 `else // !`ifdef FPGA

	// rtl model for sram;
	// XXX
   
 `endif // !`ifdef FPGA

`endif // !`ifdef GI_SRAM_MODEL
   
endmodule

