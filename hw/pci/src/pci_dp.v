//**************************************************************************************
//
//	Model Name	: pci_dp.v
//	Revision	: $Revision: 1.1 $
//	Date		: $Date: 2004/02/26 22:16:57 $
//	Author		: Bill Saperstein
//	Description	: PCI interface data path
//
//**************************************************************************************


//**************************************************************************************
// Module Definition
//**************************************************************************************
module pci_dp
		(
	// Inputs
		pcirst_i_ ,		// pci system reset
		ad_i ,			// pci input address/data bus
		cbe_i ,			// pci command/byte enable encoding
		sys_out ,		// data bus from bi_unit
		dma_addr ,		// dma request address from bi_unit
		dma_req ,		// dma request from bi_unit
		dma_wr ,		// dma direction from bi_unit
		frame_i_ ,		// pci frame signal
		addr_sel ,		// address/data select control to ad_o
		addr_inc ,		// address word increment enable
		data1_hld ,		// first data out hold reg. enable
		data2_hld ,		// second data out hold reg. enable
		pciclk_i ,		// pci system clock
	// Outputs
		hit ,			// io/memory space address hit
		pci_rd ,		// read/write_ pci command decode
		cmd_err ,		// unsupported pci command indication
		byte_err ,		// unsupported byte enable indication
		framein_ ,		// latched pci frame signal
		ad_o ,			// pci output data bus
		cbe_o ,			// pci command/byte enable encoding
		sys_addr ,		// target address to bi_unit
		sys_in 			// data bus to bi_unit
		) ;

//**************************************************************************************
// Define Parameters (optional)
//**************************************************************************************

`define GI_BASE			15'h0fe0	// base address of gi_unit

//**************************************************************************************
// Port Declarations
//**************************************************************************************

	input
		pcirst_i_ ,		// pci system reset
		dma_req ,		// dma request from bi_unit
		dma_wr ,		// dma direction from bi_unit
		frame_i_ ,		// pci frame signal
		addr_sel ,		// address/data select control to ad_o
		addr_inc ,		// address word increment enable
		data1_hld ,		// first data out hold reg. enable
		data2_hld ,		// second data out hold reg. enable
		pciclk_i ;		// pci system clock
	input [3:0]
		cbe_i ;			// pci command/byte enable encoding
	input [27:7] 
		dma_addr ;		// dma address from bi_unit
	input [31:0]
		ad_i ,			// pci input address/data bus
		sys_out ;		// data bus from bi_unit
	output
		hit ,			// io/memory space address hit
		pci_rd ,		// read/write_ pci command decode
		cmd_err ,		// unsupported pci command indication
		byte_err ,		// unsupported byte enable indication
		framein_ ;		// latched pci frame signal
	output [3:0]
		cbe_o ;			// pci command/byte enable encoding
	output [17:2]
		sys_addr ;		// target address to bi_unit
	output [31:0]
		ad_o ,			// pci output data bus
		sys_in ;		// data bus to bi_unit

//**************************************************************************************
// Net Assignments and Declarations
//**************************************************************************************

	wire	framestrt ;

//**************************************************************************************
// Pre-Defined Module Instantiations
//**************************************************************************************

// Not applicable

//**************************************************************************************
// Gate and Structural Declarations
//**************************************************************************************

// *** Define the AD bus input path
//	there are two paths to de-multiplex the address and data
//	the address path also includes the increment logic to increment
//	the sys_addr to the bi_unit for target requests

//	input register to AD is clocked on posedge pciclk followed
//	by registers for sys_in and sys_addr clocked on negedge of pciclk

	reg[31:0] adi_reg, sysin_reg ;
	reg[17:2] sysaddr_reg ;
	always @(posedge pciclk_i or negedge pcirst_i_)
	   begin
	// handle pci reset
		if (!pcirst_i_)
		   adi_reg <= 32'h0 ;
		else
		   adi_reg <= ad_i ;
	   end

	always @(negedge pciclk_i or negedge pcirst_i_)
	   begin
	// handle pci reset
		if (!pcirst_i_)
		   begin
			sysin_reg <= 32'h0 ;
		  	sysaddr_reg <= 16'h0 ;
		   end
		else
		   begin
			sysin_reg <= adi_reg ;
			sysaddr_reg <= framestrt ? adi_reg[17:2] : 
				addr_inc ? (sysaddr_reg + 1'b0) : sysaddr_reg ;
		   end
	   end

	assign
		sys_addr = sysaddr_reg ,
		sys_in = sysin_reg ;

// 	define logic to generate framestrt pulse to latch address into sysaddr_reg

	reg framei_reg1, framei_reg2 ;
	always @(posedge pciclk_i or negedge pcirst_i_)  
	   begin
	// handle pci reset
		if (!pcirst_i_)
		   begin
			framei_reg1 <= 1'b0 ;
			framei_reg2 <= 1'b0 ;
		   end
		else
		   begin
			framei_reg1 <= frame_i_ ;
			framei_reg2 <= framei_reg1 ;
		   end
	   end

	assign
		framestrt = ~framei_reg1 & framei_reg2 ,
		framein_ = framei_reg1 ;

//	decoder compares incoming pci address with hardcoded gi base address
//	to generate hit signal to slave state machine

	assign
		hit = (adi_reg[31:18] == `GI_BASE) ;

// ***	Define CBE input path
//	latch the CBE lines on posedge pciclk then decode the command

	reg[3:0] cbe_reg ;
	always @(posedge pciclk_i or negedge pcirst_i_)
	   begin
	// handle pci reset
		if (!pcirst_i_)
		   cbe_reg <= 4'h0 ;
		else
		   cbe_reg <= cbe_i ;
	   end

//	decode the cbe_reg to determine memory/io read or write
//	all other commands generate a cmd_err to the state machine

	assign
		{pci_rd,cmd_err} = cmdecode(cbe_reg) ;

//	the byte enables should always signal full 32bit transactions
//	for reads/writes otherwise a byte_err is generated to the
//	state machine

	assign
		byte_err = |cbe_reg ;

// *** Define the AD output path
//	dma_addr is latched with posedge pciclk and then piped thru a negedge
//	register to allow setup/hold to rising pciclk on the pci bus
//	sys_out data is latched with posedge pciclk and then piped thru a
//	negedge register to allow setup/hold to rising pciclk on the pci bus
//	the sys_out registers also provide hold capability when the pipeline
//	is staled due to wait inserted by the target(bridge)

	reg[31:0] sysout1_reg, sysout2_reg ;
	reg[27:7] dmaddr1_reg, dmaddr2_reg ; 
	always @(posedge pciclk_i or negedge pcirst_i_)
	   begin
		if (!pcirst_i_)
		   begin
			sysout1_reg <= 32'h0 ;
			dmaddr1_reg <= 16'h0 ;
		   end
		else
		   begin
			sysout1_reg <= data1_hld ? sysout1_reg : sys_out ;
			dmaddr1_reg <= dma_addr ;
		   end
	   end

	always @(negedge pciclk_i or negedge pcirst_i_)
	   begin
		if (!pcirst_i_)
		   begin
			sysout2_reg <= 32'h0 ;
			dmaddr2_reg <= 16'h0 ;
		   end
		else
		   begin
			sysout2_reg <= data2_hld ? sysout2_reg : sysout1_reg ;
			dmaddr2_reg <= dmaddr1_reg ;
		   end
	   end

//	output mux either address or data onto ad_o
//	control signal valid at negedge of pciclk from master state machine
	assign
		ad_o = addr_sel ? {4'h0,dmaddr2_reg,7'h00} : sysout2_reg ;

// *** Define CB output path
//	select between encoded command and byte enables with addr_sel
//	control signal valid on negedge of pciclk from master state machine
//	dma transfers are always 128byte bursts 

//	read ->	memory read multiple cbe = 4'b1100
//	write -> memory write and invalidate cbe = 4'b1111

	assign
		cbe_o = addr_sel ? cmdencode(dma_req,dma_wr) : 4'h0 ;
		

//**************************************************************************************
// Procedural Assignments
//**************************************************************************************

// Not applicable

//**************************************************************************************
// Task and Function Definitions
//**************************************************************************************

// *** pci command decode
//	only memory/io read/writes are valid - other commands generate
//	cmd_err

	function[1:0] cmdecode ;
	   input[3:0] cmd ;
	   begin
		casez (cmd)
		4'b1100, 4'b1110,
		4'b0010, 4'b0110 : cmdecode = 2'b10 ; // io/memory read
		4'b1111,
		4'b0011, 4'b0111 : cmdecode = 2'b00 ; // io/memory write
		default          : cmdecode = 2'b01 ; // invalid command
	        endcase
	   end
	endfunction

// *** pci command encode
//	only support memory read multiple and memory write invalidate commands
//	as master

	function[3:0] cmdencode ;
	   input dma_req, dma_wr ;
	   begin
		casez ({dma_req, dma_wr})
		2'b10 : cmdencode = 4'b1100 ;
		2'b11 : cmdencode = 4'b1111 ;
		default : cmdencode = 4'b0000 ;
		endcase
	   end
	endfunction


//**************************************************************************************
// End of Model
//**************************************************************************************
endmodule // pci_dp.v 
