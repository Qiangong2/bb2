//**************************************************************************************
//
//	Model Name	: pci_blk.v
//	Revision	: $Revision: 1.2 $
//	Date		: $Date: 2004/03/15 01:34:26 $
//	Author		: Bill Saperstein
//	Description	: top level for pci interface
//
//**************************************************************************************

`timescale 1ns/10ps

//**************************************************************************************
// Module Definition
//**************************************************************************************
module pci_blk
		(
	// Inputs
                pcirst_i_ ,             // pci system reset
                ad_i ,                  // pci input address/data bus
                cbe_i ,                 // pci command/byte enable encoding
                sys_out ,               // data bus from bi_unit
                dma_addr ,              // dma request address from bi_unit
                dma_req ,               // dma request from bi_unit
                dma_wr ,                // dma direction from bi_unit
                frame_i_ ,              // pci frame signal
		devsel_i_ ,             // pci devsel input signal
                irdy_i_ ,               // pci irdy input signal
                trdy_i_ ,               // pci trdy input signal
                stop_i_ ,               // pci stop input signal
                gnt_i_ ,                // pci grant input signal  
                pciclk_i ,              // pci system clock    
		pci3xclk ,		// 3X clock from bi_unit
	// Outputs
		dma_done ,		// dma done indication to bi_unit
                dma_abort ,             // dma abort indication to bi_unit     
                ad_o ,                  // pci output data bus
		ad_oe ,			// address/data bus output select
                cbe_o ,                 // pci command/byte enable encoding
		cbe_oe ,		// command/byte enable output select
	        frame_o_ ,              // pci frame output signal
		frame_oe ,		// frame output select
                devsel_o_ ,             // pci devsel output signal
                irdy_o_ ,               // pci irdy output signal
		irdy_oe ,		// frame output select
                trdy_o_ ,               // pci trdy output signal
                stop_o_ ,               // pci stop output signal
		tds_oe ,		// trdy_,devsel_,stop_ output select
                req_o_ ,                // pci request output signal
		sys_cmd ,		// bus command to bi_unit
                sys_addr ,              // target address to bi_unit
                sys_in                  // data bus to bi_unit    
		) ;

//**************************************************************************************
// Define Parameters (optional)
//**************************************************************************************

// Not Applicable

//**************************************************************************************
// Port Declarations
//**************************************************************************************

	input 
		pcirst_i_, dma_req, dma_wr, frame_i_, devsel_i_,
		irdy_i_, trdy_i_, stop_i_, gnt_i_, pciclk_i, pci3xclk ;
	input [3:0]
		cbe_i ;
	input [27:7]
		dma_addr ;
	input [31:0]
		ad_i, sys_out ;

	output
		dma_done, dma_abort, ad_oe, cbe_oe, frame_o_, frame_oe,
		devsel_o_, irdy_o_, irdy_oe,
		trdy_o_, stop_o_, tds_oe, req_o_ ;
	output [2:0]
		sys_cmd ;
	output [3:0]
		cbe_o ;
	output [17:2]
		sys_addr ;
	output [31:0]
		ad_o, sys_in ;
		
//**************************************************************************************
// Net Assignments and Declarations
//**************************************************************************************

// Not Applicable

//**************************************************************************************
// Pre-Defined Module Instantiations
//**************************************************************************************

	pci_dp		datapath
			(	
		// Inputs
                	.pcirst_i_(pcirst_i_) ,     // pci system reset
                	.ad_i(ad_i) ,               // pci input address/data bus
                	.cbe_i(cbe_i) ,             // pci command/byte enable encoding
                	.sys_out(sys_out) ,         // data bus from bi_unit
                	.dma_addr(dma_addr) ,       // dma request address from bi_unit
                	.dma_req(dma_req) ,         // dma request from bi_unit
                	.dma_wr(dma_wr) ,           // dma direction from bi_unit
                	.frame_i_(frame_i_) ,       // pci frame signal
                	.addr_sel(addr_sel) ,       // address/data select control to ad_o
                	.addr_inc(addr_inc) ,       // address word increment enable
                	.data1_hld(data1_hld) ,     // first data out hold reg. enable
                	.data2_hld(data2_hld) ,     // second data out hold reg. enable
                	.pciclk_i(pciclk_i) ,       // pci system clock
        	// Outputs
                	.hit(hit) ,                 // io/memory space address hit
                	.pci_rd(pci_rd) ,           // read/write_ pci command decode
                	.cmd_err(cmd_err) ,         // unsupported pci command indication
                	.byte_err(byte_err) ,       // unsupported byte enable indication
                	.framein_(framein_) ,       // latched pci frame signal
                	.ad_o(ad_o) ,               // pci output data bus
                	.cbe_o(cbe_o) ,             // pci command/byte enable encoding
                	.sys_addr(sys_addr) ,       // target address to bi_unit
                	.sys_in(sys_in)             // data bus to bi_unit      
			) ;

	pci_ctrl	controller
			(
		// Inputs
                	.pcirst_i_(pcirst_i_) ,     // pci system reset
                	.framein_(framein_) ,       // latched pci frame signal
                	.devsel_i_(devsel_i_) ,     // pci devsel input signal
                	.irdy_i_(irdy_i_) ,         // pci irdy input signal
                	.trdy_i_(trdy_i_) ,         // pci trdy input signal
                	.stop_i_(stop_i) ,          // pci stop input signal
                	.gnt_i_(gnt_i_) ,           // pci grant input signal
                	.hit(hit) ,                 // io/memory space address hit
                	.pci_rd(pci_rd) ,           // read/write_ pci command indication
                	.cmd_err(cmd_err) ,         // unsupported pci command error
                	.byte_err(byte_err) ,       // unsupported byte enable error
                	.dma_req(dma_req) ,         // dma request from bi_unit
                	.dma_wr(dma_wr) ,           // dma write indication
                	.pciclk_i(pciclk_i) ,       // pci system clock
                	.pci3xclk(pci3xclk) ,       // 3X clock from bi_unit      

		// Outputs
                	.frame_o_(frame_o_) ,       // pci frame output signal
                	.devsel_o_(devsel_o_) ,     // pci devsel output signal
                	.irdy_o_(irdy_o_) ,         // pci irdy output signal
                	.trdy_o_(trdy_o_) ,         // pci trdy output signal
                	.stop_o_(stop_o_) ,         // pci stop output signal
                	.req_o_(req_o_) ,           // pci request output signal
	           	.frame_oe(frame_oe) ,       // frame_ output select
                	.irdy_oe(irdy_oe) ,         // irdy_  output select
                	.tds_oe(tds_oe) ,           // trdy_, devsel_, stop_ output select
                	.cbe_oe(cbe_oe) ,           // command/byte enable output select
                	.ad_oe(ad_oe) ,             // address/data bus output select      
                	.dma_abort(dma_abort) ,     // dma abort indication to bi_unit
                	.data1_hld(data1_hld) ,     // first data out hold reg. enable
                	.data2_hld(data2_hld) ,     // second data out hold reg. enable
                	.dma_done(dma_done) ,       // dma done indication to bi_unit
                	.addr_sel(addr_sel) ,       // address/data select control to ad_o
                	.sys_cmd(sys_cmd)           // bus command to bi_unit
               		) ;              


//**************************************************************************************
// Gate and Structural Declarations
//**************************************************************************************

// Not Applicable

//**************************************************************************************
// Procedural Assignments
//**************************************************************************************

// Not Applicable

//**************************************************************************************
// Task and Function Definitions
//**************************************************************************************

// Not Applicable

//**************************************************************************************
// End of Model
//**************************************************************************************
endmodule // pci_blk 
