//**************************************************************************************
//
//	Model Name	: pci_ctrl.v
//	Revision	: $Revision: 1.2 $
//	Date		: $Date: 2004/03/15 01:34:52 $
//	Author		: Bill Saperstein
//	Description	: controller for pci interface to gi_unit
//
//**************************************************************************************


//**************************************************************************************
// Module Definition
//**************************************************************************************
module pci_ctrl
		(
	// Inputs
		pcirst_i_ ,		// pci system reset
		framein_ ,		// latched pci frame signal
		devsel_i_ ,		// pci devsel input signal
		irdy_i_ ,		// pci irdy input signal
		trdy_i_ ,		// pci trdy input signal
		stop_i_ ,		// pci stop input signal
		gnt_i_ ,		// pci grant input signal
		hit ,			// io/memory space address hit
		pci_rd ,		// read/write_ pci command indication
		cmd_err ,		// unsupported pci command error
		byte_err ,		// unsupported byte enable error
		dma_req ,		// dma request from bi_unit
		dma_wr ,		// dma write indication
		pciclk_i ,		// pci system clock
		pci3xclk ,		// 3X clock from bi_unit
	// Outputs
		frame_o_ ,		// pci frame output signal
		devsel_o_ ,		// pci devsel output signal
		irdy_o_ ,		// pci irdy output signal
		trdy_o_ ,		// pci trdy output signal
		stop_o_ ,		// pci stop output signal
		req_o_ ,		// pci request output signal
		frame_oe ,		// frame_ output select
		irdy_oe ,		// irdy_  output select
		tds_oe ,		// trdy_, devsel_, stop_ output select
		cbe_oe ,		// command/byte enable output select
		ad_oe ,			// address/data bus output select
		dma_abort ,		// dma abort indication to bi_unit
		data1_hld ,		// first data out hold reg. enable
		data2_hld ,		// second data out hold reg. enable
		dma_done ,		// dma done indication to bi_unit
		addr_sel ,		// address/data select control to ad_o
		addr_inc ,		// address word increment enable
		sys_cmd 		// bus command to bi_unit 
		) ;

//**************************************************************************************
// Define Parameters (optional)
//**************************************************************************************

// slave state register encodings - slv_sm
	parameter	slv_idle =	7'h00 ;
	parameter	slv_mem_rd =	7'h05 ;
	parameter	slv_rd_trdy =	7'h07 ;
	parameter	slv_rd_wait =	7'h01 ;
	parameter	slv_wr_wait =	7'h21 ;
	parameter	slv_mem_wr =	7'h2b ;
	parameter	slv_cmd_err =	7'h5d ;
	parameter	slv_byte_err =	7'h6d ;

// slave state register output encodings - slv_sm

	parameter	sidle =		5'h00 ;
	parameter	memrd =		5'h05 ;
	parameter	rdtrdy =	5'h07 ;
	parameter	rdwait =	5'h01 ;
	parameter	wrwait =	5'h09 ;
	parameter	memwr =		5'h0b ;
	parameter	cerr =		5'h15 ;
	parameter	berr =		5'h1d ;

// master state register encodings - mstr_sm
	parameter	mst_idle =	10'h000 ;
	parameter	mst_req =	10'h080 ;
	parameter	mst_rd_strt =	10'h043 ;
	parameter	mst_rd_wait =	10'h0e3 ;
	parameter	mst_read =	10'h1fb ;
	parameter	mst_rd_end =	10'h13b ;
	parameter	mst_lst_wait =	10'h223 ;
	parameter	mst_lst_rd =	10'h019 ;
	
	parameter	mst_wr_strt =	10'h2d7 ;
	parameter	mst_wr_wait1 =	10'h353 ;
	parameter	mst_write =	10'h3f7 ;
	parameter	mst_wr_wait2 =	10'h0c2 ;
	parameter	mst_wr_hld1 =	10'h162 ;
	parameter	mst_wr_hld2 =	10'h1e2 ;
	parameter	mst_lst_wr =	10'h222 ;
	parameter	mst_abort =	10'h2a2 ;

// master state register output encodings - mstr_sm

	parameter	midle =		5'h00 ;
	parameter	req =		5'h04 ;
	parameter	rdstrt =	5'h03 ;
	parameter	rwait =		5'h07 ;
	parameter	rd =		5'h0f ;
	parameter	rend =		5'h0b ;
	parameter	lwait =		5'h13 ;
	parameter	lrd =		5'h01 ;
	
	parameter	wrstrt =	5'h17 ;
	parameter	wwait1 =	5'h1b ;
	parameter	wr =		5'h1f ;
	parameter	wwait2 =	5'h06 ;
	parameter	hld1 =		5'h0a ;
	parameter	hld2 =		5'h0e ;
	parameter	lwr =		5'h12 ;
	parameter	abrt =		5'h16 ;





//**************************************************************************************
// Port Declarations
//**************************************************************************************

	input
		pcirst_i_ ,		// pci system reset
		framein_ ,		// latched pci frame signal
		devsel_i_ ,		// pci devsel input signal
		irdy_i_ ,		// pci irdy input signal
		trdy_i_ ,		// pci trdy input signal
		stop_i_ ,		// pci stop input signal
		gnt_i_ ,		// pci grant input signal
		hit ,			// io/memory space address hit
		pci_rd ,		// read/write_ pci command indication
		cmd_err ,		// unsupported pci command error
		byte_err ,		// unsupported byte enable error
		dma_req ,		// dma request from bi_unit
		dma_wr ,		// dma write indication
		pciclk_i ,		// pci system clock
		pci3xclk ;		// bi_unit 3X clock
	output
		frame_o_ ,		// pci frame output signal
		devsel_o_ ,		// pci devsel output signal
		irdy_o_ ,		// pci irdy output signal
		trdy_o_ ,		// pci trdy output signal
		stop_o_ ,		// pci stop output signal
		req_o_ ,		// pci request output signal
		frame_oe ,		// frame_ output select
		irdy_oe ,		// irdy_  output select
		tds_oe ,		// trdy_, devsel_, stop_ output select
		cbe_oe ,		// command/byte enable output select
		ad_oe ,			// address/data bus output select
		dma_abort ,		// dma abort indication to bi_unit
		data1_hld ,		// first data out hold reg. enable
		data2_hld ,		// second data out hold reg. enable
		addr_sel ,		// address/data select control to ad_o
		addr_inc ,		// address word increment enable
		dma_done ;		// dma done indication to bi_unit
	output [2:0]
		sys_cmd ; 		// bus command to bi_unit 

//**************************************************************************************
// Net Assignments and Declarations
//**************************************************************************************

	reg[9:0]
		mst_state ;
	reg[6:0]
		slv_state ;
	wire[2:0] 
		slvcmd, mstcmd ;
	wire
		master, cmd_sel, devsel_inc, wdcnt_inc ;

//**************************************************************************************
// Pre-Defined Module Instantiations
//**************************************************************************************

// Not Applicable

//**************************************************************************************
// Gate and Structural Declarations
//**************************************************************************************

// *** Define input registers for pci signals and signals from bi_unit
	reg devsel_reg, irdy_reg, trdy_reg, stop_reg, gnt_reg, dmareq_reg, dmawr_reg ;
	always @(posedge pciclk_i or negedge pcirst_i_)
	   begin
		if (!pcirst_i_)
		   begin
			devsel_reg <= 1'b1 ;
			irdy_reg <= 1'b1 ;
			trdy_reg <= 1'b1 ;
			stop_reg <= 1'b1 ;
			gnt_reg <= 1'b1 ;
			dmareq_reg <= 1'b0 ;
			dmawr_reg <= 1'b0 ;
		   end
		else
		   begin
			devsel_reg <= devsel_i_ ;
			irdy_reg <= irdy_i_ ;
			trdy_reg <= trdy_i_ ;
			stop_reg <= stop_i_ ;
			gnt_reg <= gnt_i_ ;
			dmareq_reg <= dma_req ;
			dmawr_reg <= dma_wr ;
		   end
	   end

// *** Define the immediate outputs of the slave state machine
//	the slave state register has six bits, the lower four(4)
//	bits are direct outputs, the upper bit is also a direct
//	output all are valid on the falling edge of pciclk

	assign
		devsel_o_ = ~slv_state[0] ,
		trdy_o_ = ~slv_state[1] ,
		slvcmd[0] = slv_state[2] ,
		slvcmd[1] = slv_state[3] ,
		slvcmd[2] = slv_state[4] ,
		stop_o_ = ~slv_state[6] ,
		tds_oe = slv_state[0] | slv_state[1] | slv_state[6] ;

// *** Define the decoded outputs from the slave state machine
//	the address incrementor to the bi_unit is also decoded from
//	five(5) bits of the state register and qualified with latched irdy_i_
	
	wire[4:0] slvdec ;
	assign
		slvdec = {slv_state[6:5],slv_state[2:0]} ,
		addr_inc = ((slvdec == rdwait) || (slvdec == rdtrdy) ||
			(slvdec == memwr)) && !irdy_reg ;

// *** Define the immediate outputs of the master state machine
//	the master state machine has eight(8) bit state register
//	the lower seven(7) bits are used for immediate outputs
//	valid on the falling edge of pciclk

	assign
		cmd_sel = mst_state[0] ,
		master = mst_state[1] ,
		mstcmd[0] = mst_state[2] ,
		mstcmd[1] = mst_state[3] ,
		mstcmd[2] = mst_state[4] ,
		irdy_o_ = ~mst_state[5] ,
		frame_o_ = ~mst_state[6] ,
		irdy_oe = mst_state[5] ,
		frame_oe = mst_state[6] ,
		cbe_oe = mst_state[1] ,
		ad_oe = mst_state[1] ;
		
// *** Define the decoded outputs of the master state machine
//	the addr_sel signal and counter increment signals for devsel_inc
//	and wdcnt_inc are decoded from bits of the master state register
//	five(5) bits total and qualified with the falling edge of pciclk
//	pci bus req_ is valid following the edge of pciclk to allow the
//	arbiter to latch it on the rising edge of pciclk

	wire[4:0] mstdec ;
	assign
		mstdec = {mst_state[9:7],mst_state[1:0]} ,
		req_o_ = ~(mstdec == mst_req) ,
		dma_done = (mstdec == lrd) || (mstdec == lwr) ,
		addr_sel = (mstdec == rdstrt) || (mstdec == wrstrt) ,
		devsel_inc = ((mstdec == rdwait) || (mstdec == hld1))
			&& devsel_reg  ,
		data2_hld = ((mstdec == wr) || (mstdec == hld1) ||
			(mstdec == hld2) || (mstdec == lwr)) && trdy_reg ,
		data1_hld = (mstdec == hld1) ,
		wdcnt_inc = (mstdec == rd) || (mstdec == wr) ;

// *** Define the counters for the master state machine
//	there are two(2) counters in the state machine
//	one is to count the devsel wait period (greater than six clocks)
//	the other is to count the number of words in a dma transactions (tot = 32words)

//	devsel latency counter
	reg[2:0] dselcnt_reg ;
	wire cnt_rst ;
	assign
	   cnt_rst = (mst_state == mst_idle) ;

	always @(negedge pciclk_i or negedge pcirst_i_)
	   begin
		if (!pcirst_i_)
		   dselcnt_reg <= 3'h0 ;
		else
		   dselcnt_reg <= devsel_inc ? (dselcnt_reg + 1'b1) :
			cnt_rst ? 3'h0 : dselcnt_reg ;
	   end

//	dma word counter
	reg[4:0] wdcnt_reg ;

	always @(negedge pciclk_i or negedge pcirst_i_)
	   begin
		if (!pcirst_i_)
		   wdcnt_reg <= 5'h00 ;
		else
		   wdcnt_reg <= wdcnt_inc ? (wdcnt_reg + 1'b1) :
			cnt_rst ? 5'h00 : wdcnt_reg ;
	   end

// *** Define logic to generate sys_cmd to bi_unit
//	the sys_cmd is valid for one pci3xclk

	wire[2:0] syscmd ;
	reg[2:0] syscmd_reg1, syscmd_reg2 ;

	assign
	   syscmd = cmd_sel ? mstcmd : slvcmd ;

	always @(posedge pci3xclk or negedge pcirst_i_)
	   begin
		if (!pcirst_i_)
		   syscmd_reg1 <= 3'h0 ;
		else
		   syscmd_reg1 <= syscmd ;
	   end
	always @(negedge pci3xclk or negedge pcirst_i_)
	   begin
		if (!pcirst_i_)
		   syscmd_reg2 <= 3'h0 ;
		else
		   syscmd_reg2 <= syscmd_reg1 ;
	   end

	assign
	   sys_cmd[2] = !syscmd_reg2[2] & syscmd[2] ,
	   sys_cmd[1] = !syscmd_reg2[1] & syscmd[1] ,
	   sys_cmd[0] = !syscmd_reg2[0] & syscmd[0] ;
	

//**************************************************************************************
// Procedural Assignments
//**************************************************************************************

// *** Define slave state machine
//	state assignments on lower four bits and upper one bit are direct outputs
//	and valid on falling edge of pciclk_i.
//	state assignments of bits [4:2] are the encoded sys_cmd to the bi_unit
//	state assignments on upper two bits and lower three bits re used for decoded outputs 

//	slv_state[6:0]		->	state register
//	slv_state[6]		->	~stop
//	slv_state[5]		->	state
//	slv_state[4]		->	slvcmd[2]
//	slv_state[3]		->	slvcmd[1]
//	slv_state[2]		->	slvcmd[0]
//	slv_state[1]		->	~trdy
//	slv_state[0]		->	~devsel

	always @(negedge pciclk_i or negedge pcirst_i_)
	   begin
		if (!pcirst_i_)
		   slv_state <= slv_idle ;
		else
		   begin :	slave_sm
			case (slv_state)
			   (slv_idle) :
				begin
				   if (!framein_ & !master & hit)
				      begin
					if (cmd_err)
					   slv_state <= slv_cmd_err ;
					else if (pci_rd)
					   slv_state <= slv_mem_rd ;
					else
					   slv_state <= slv_mem_wr ;
				      end
				end
			   (slv_mem_rd) :
				slv_state <= slv_rd_wait ;
			   (slv_rd_wait) :
				if (!irdy_reg)
				   slv_state <= slv_rd_trdy ;
			   (slv_rd_trdy) :
				if (framein_)
				   slv_state <= slv_idle ;
				else
				   slv_state <= slv_rd_wait ;
			   (slv_wr_wait) :
				if ( !irdy_reg & !byte_err)
				   slv_state <= slv_mem_wr ;
				else if ( !irdy_reg & byte_err)
				   slv_state <= slv_byte_err ;
			   (slv_mem_wr) :
				if (framein_)
				   slv_state <= slv_idle ;
				else
				   slv_state <= slv_wr_wait ;
			   (slv_cmd_err) :
				if (framein_)
				   slv_state <= slv_idle ;
			   (slv_byte_err) :
				if (framein_)
				   slv_state <= slv_idle ;
			endcase
		   end
	   end
				

// *** Define master state machine
//	state assignments on bits [6:5] and [1:0] are immediate outputs
//	and valid on the falling edge of pciclk
//	state assignments on bits [4:2] are encoding of sys_cmd for the bi_unit
//	decoded outputs are derived from the upper three bits and lower two bits
//	of the state register

//	mst_state[9:0]		->	master state register
//	mst_state[9]		->	state2
//	mst_state[8]		->	state1
//	mst_state[7]		->	state0
//	mst_state[6]		->	frame_o_
//	mst_state[5]		->	irdy_o_
//	mst_state[4]		->	mstcmd[2]
//	mst_state[3]		->	mstcmd[1]
//	mst_state[2]		->	mstcmd[0]
//	mst_state[1]		->	master
//	mst_state[0]		->	cmd_sel

	always @(negedge pciclk_i or negedge pcirst_i_)
	   begin
		if(!pcirst_i_)
		   mst_state <= 10'h000 ;
		else
		   begin : 	master_sm
			case (mst_state)
			   (mst_idle) :
				if (dmareq_reg)
				   mst_state <= mst_req ; 
			   (mst_req) :
				if (~gnt_reg & framein_ & irdy_reg)
				   begin
				      if (dmawr_reg)
				   	mst_state <= mst_wr_strt ;
				      else
					mst_state <= mst_rd_strt ;
				   end
	// dma_read
			   (mst_rd_strt) :	// output address and command
				mst_state <= mst_rd_wait ;
			   (mst_rd_wait) :
				if (!devsel_reg)
				  begin
				     if (~trdy_reg & stop_reg)
				        begin
				           if (gnt_reg)
					      mst_state <= mst_abort ;
				           else
					      mst_state <= mst_read ; 
				        end
				     else if (~trdy_reg & ~stop_reg)
					mst_state <= mst_abort ;
				  end
				else if (dselcnt_reg == 3'h7) 
				   mst_state <= mst_abort ;
			   (mst_read) :
				if (wdcnt_reg == 5'h1f)
				   begin
				      if (!trdy_reg)
					   mst_state <= mst_rd_end ;
				      else
				           mst_state <= mst_lst_wait ;
				   end
				else if (trdy_reg)
				   mst_state <= mst_rd_wait ;
			   (mst_rd_end) :
				if (!trdy_reg)
				   mst_state <= mst_lst_rd ;
				else
				   mst_state <= mst_lst_wait ;
			   (mst_lst_wait) :
				if (!trdy_reg)
				   mst_state <= mst_lst_rd ;
			   (mst_lst_rd) :
				mst_state <= mst_idle ;
			  
	// dma_write
			   (mst_wr_strt) :	// output address and command
				mst_state <= mst_wr_wait1 ;
			   (mst_wr_wait1) :
				mst_state <= mst_write ;
			   (mst_write) :
			 	if (~trdy_reg & stop_reg)
			  	   mst_state <= mst_wr_wait2 ;
			  	else if (~trdy_reg & ~stop_reg)
				   mst_state <= mst_abort ;
				else
				   mst_state <= mst_wr_hld2 ;
			   (mst_wr_wait2) :
				if (wdcnt_reg == 5'h1f)
				   mst_state <= mst_lst_wr ;
				else
				   mst_state <= mst_write ;
			   (mst_wr_hld2) :
				if (wdcnt_reg == 5'h1f)
				   begin
				      if (!trdy_reg)
				   	 mst_state <= mst_lst_wr ;
				   end
				else
				   begin
				      if (~trdy_reg & stop_reg)
					 mst_state <= mst_write ;
				      else if (~trdy_reg & ~stop_reg)
					 mst_state <= mst_abort ;
				      else
				   	 mst_state <= mst_wr_hld1 ;
				   end
			   (mst_wr_hld1) :
				if (!devsel_reg)
				   begin
				      if (wdcnt_reg == 5'h1f)
					begin
					   if (!trdy_reg)
					      mst_state <= mst_lst_wr ;
					end
				      else
					begin
					   if (!trdy_reg)
					      mst_state <= mst_write ;
					end
				   end
				else
				   if (dselcnt_reg == 3'h3)
					mst_state <= mst_abort ;
			   (mst_lst_wr) :
				if (!trdy_reg)
				   mst_state <= mst_idle ;
			   (mst_abort) :
				mst_state <= mst_idle ;
			endcase
		end
	   end


//**************************************************************************************
// Task and Function Definitions
//**************************************************************************************

// Not Applicable

//**************************************************************************************
// End of Model
//**************************************************************************************
endmodule // pci_ctrl 
