<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Virtual Memory Introduction</title>
</head>

<body>

<h1>Virtual Memory</h1>

<hr>

<h2>Introduction</h2>

<p>The virtual memory library provides the functionality to access ARAM using virtual memory addresses in the range of 0x7E000000 to 0x80000000. You must specify a swap space buffer in main memory (typically 1MB to 4MB) and a buffer in ARAM to act as the backing store (less than 16MB). When a page miss occurs, the page in ARAM is synchronously brought into main memory. If a page must be removed from main memory, it is copied back to ARAM only if it was modified. Both data and code can be placed in virtual memory.
</p>

<p>The virtual memory library has been divided into two libraries. The <code>VM</code> library is the high level library that your code will interface with. The <code>VMBASE</code> library is the core library that implements the page table and is responsible for intercepting page miss interrupts. You should not access functions within the <code>VMBASE</code> library unless you are rewriting or customizing the <code>VM</code> library. The source code is provided for the <code>VM</code> library, but is not provided for the <code>VMBASE</code> library.
</p>

<h3>Speed and Memory Footprint</h3>

<p>The speed of virtual memory is largely dependent on the ARAM transfer speed. When a page fault occurs, it takes roughly 65us to move a 4K page into main memory. If a modified page must be written back to ARAM, then the ARAM transfer time on a page fault takes double the time (130us). The overhead in servicing the page fault is roughly 3us. Worst case, a page fault takes roughly 133us and best case it takes 3us (when the source page in ARAM is uninitialized and doesn't need to be transferred into main memory). You can expect to be able to tolerate between 4 and 20 page misses per frame.
</p>

<p>The memory footprint of the virtual memory library is fairly small (excluding the main memory page buffer that you specify). The page table and other look-up tables in the <code>VMBASE</code> library require 84K. However, since the page table must be 64K aligned, it is possible for the <code>VMBASE</code> library to use up to 128K. The higher level <code>VM</code> library uses 48K for look-up tables that map virtual memory pages to ARAM pages (VMMapping.c). The result is that the <code>VMBASE</code> and <code>VM</code> libraries will use at most 176K (in addition to the main memory swap space).
</p>



<h2>Adding Virtual Memory to Your Project</h2>

<p>1. Add vmbaseD.a, vmbase.a, vmD.a, and vm.a libraries to your project. To access virtual memory functions, use:
</p>
<UL>
<code>#include &lt;dolphin/vm.h&gt;</code>
</UL>

<p>2. Initialize virtual memory with <a href="VM/VMInit.html"><code>VMInit()</code></a>. Ensure that <a href="VM/VMInit.html"><code>VMInit()</code></a> is called before your memory initialization code removes memory from the arena. Check the docs for other important caveats.
</p>
<UL>
<code>
//Example of initializing VM with 2MB of main memory
<br>//swap space and 8MB of ARAM backing store
<br>VMInit( 0x200000, ARGetBaseAddress(), 0x800000 );
</code>
</UL>

<p>3. Allocate the virtual address range you wish to use with <a href="VM/VMAlloc.html"><code>VMAlloc()</code></a>. It is recommended that you use <a href="VM/VMAlloc.html"><code>VMAlloc()</code></a> to grab a big chunk of virtual memory and then manage it yourself within your game.
</p>
<UL>
<code>
//Example of allocating 8MB starting at 0x7E000000
<br>VMAlloc( 0x7E000000, 0x800000 );
</code>
</UL>

<p>4. Access virtual memory addresses just like main memory addresses. Do not touch ARAM to access data stored in <code>VM</code>.
</p>
<UL>
<code>
//Example of a write to virtual memory address 0x7E000000
<br>*(u32*)0x7E000000 = myNum;
<br>
<br>//Example of a read from virtual memory address 0x7E000000
<br>u32 myNum = *(u32*)0x7E000000;
</code>
</UL>



<h2>Virtual Memory Demos</h2>

<p><strong>vmminimal</strong>
<br>This demo has the minimal implementation for using virtual memory. It shows how to initialize <code>VM</code>, allocate <code>VM</code> address space, and read/write to virtual memory addresses.
</p>

<p><strong>vmsample</strong>
<br>This demo has several unit tests that exercise virtual memory.
</p>

<p><strong>vmreldemo</strong>
<br>This demo shows how to place code in virtual memory (using relocatable modules). Please refer to the original reldemo documentation if you don't know how to work with relocatable modules.
</p>



<h2>What Should be put in Virtual Memory?</h2>

<p>Virtual memory gives you the ability to store game data in ARAM, while accessing it just like data held in main memory. Game data such as animations, scripts, and look-up tables work very well in virtual memory. Game data such as display lists, textures, and sound effects work poorly in virtual memory, since they can't be accessed by the GPU or DSP without first copying them to fixed memory addresses.
</p>

<p>Data that is accessed very frequently (like a run animation of the lead character) should not be put in virtual memory, since it always needs to be in the main memory swap space. Placing frequently used data in virtual memory will decrease the efficiency of the virtual memory system in two ways. First, the swap space in main memory is effectively reduced since the frequently accessed data will always take up space. Second, the default page replacement policy (random replacement) will occasionally throw out these frequently used pages, thus wasting cycles since they will have to be brought in from ARAM to main memory again.
</p>



<h2>Monitoring the Performance of Virtual Memory</h2>

<p>The <code>VM</code> library offers performance monitoring in the form of a callback. Use the function <a href="VM/VMSetLogStatsCallback.html"><code>VMSetLogStatsCallback()</code></a> to register a callback that gets called each time a page is swapped. The callback will be called with the following information:
</p>
<table border="0" width="100%">
  <tr>
    <td><em><strong><code>realVirtualAddress</code></strong></em></td>
    <td>The virtual address that caused the page fault.</td>
  </tr>
  <tr>
    <td><em><strong><code>physicalAddress</code></strong></em></td>
    <td>The main memory physical address where the ARAM page is being stored.</td>
  </tr>
  <tr>
    <td><em><strong><code>pageNumber</code></strong></em></td>
    <td>The page number where the ARAM page is being stored.</td>
  </tr>
  <tr>
    <td><em><strong><code>pageMissLatency</code></strong></em></td>
    <td>The time it took to replace the page (in microseconds).</td>
  </tr>
  <tr>
    <td><em><strong><code>pageSwappedOut</code></strong></em></td>
    <td>Whether the old page was written back to ARAM.</td>
  </tr>
</table>



<h2>Improving the Performance of Virtual Memory</h2>

<p>Typically, games can tolerate about 4 to 20 page misses per frame. However, the tolerable rate will be highly dependent on your particular game. Regardless, there are steps you can take to improve performance.
</p>
<p>1. If performance monitoring indicates thrashing (consistantly more than 20 pages misses a frame), increase the main memory swap space. A main memory swap space to ARAM ratio of 1:8 is probably as low as you want to go. Ratios of 1:4 typically give much better performance (for example 2MB of main memory and 8MB of ARAM).
</p>
<p>2. Once data has been loaded into virtual memory at the beginning of a level, use <a href="VM/VMStoreAllPages.html"><code>VMStoreAllPages()</code></a> to force all of the data to be copied out to ARAM. This avoids writing the page to ARAM later when the page is replaced, thus making the initial page swaps twice as fast.
</p>
<p>3. Test your game with several different page replacement policies. The default page replacement policy of random replacement (<code>VM_PRP_RANDOM</code>) generally works well and performs the best under thrashing. However, your particular access patterns might favor the least-recently used policy (<code>VM_PRP_LRU</code>). Use <a href="VM/VMSetPageReplacementPolicy.html"><code>VMSetPageReplacementPolicy()</code></a> to change policies.
</p>
<p>4. Try removing frequently used data from virtual memory and store it directly in main memory. If you have main memory to spare, this will always improve performance of the remaining data in virtual memory.
</p>
<p>5. For a slight performance gain, prefer using lower virtual memory addresses (starting at 0x7E000000). This is explained on the <a href="VM/VMAlloc.html"><code>VMAlloc()</code></a> page.
</p>
<p>6. At the end of your game loop, store modified pages back to ARAM if there is extra time. This can be done dynamically with the function <a href="VM/VMStoreOnePage.html"><code>VMStoreOnePage()</code></a>. Pseudo-code explaining the technique is on the <a href="VM/VMStoreOnePage.html"><code>VMStoreOnePage()</code></a> page.
</p>



<h2>Virtual Memory Hazards</h2>

<p>The virtual memory system is a very CPU-centric system. Because addresses are being remapped within the CPU, the virtual addresses don't make sense to systems outside the CPU. Therefore, the GPU, optical drive, and USB2EXI device can't access data stored at virtual memory addresses.
</p>

<p>For example, when data is loaded from the optical drive with <code>DVDRead</code>, it must be placed in standard main memory. Once in main memory, you can memcpy it into a virtual address. Similarly, if you have a texture in virtual memory that you want to display, you must first copy it out of virtual memory and into main memory for the GPU to be able to access it.
</p>



<h2>Virtual Memory Library Architecture</h2>

<p>As mentioned in the Introduction, the virtual memory system is broken into two libraries: <code>VM</code> and <code>VMBASE</code>. The <code>VM</code> library is shipped with source code and can be modified/recompiled/debugged. The <code>VM</code> library is built on top of the core <code>VMBASE</code> library. Functionality is divided as follows:
</p>

<p>The <code>VM</code> library:
<br><LI> Allocates and initializes the main memory swap space (VM.c).
<br><LI> Manages the virtual address to ARAM address mappings (VMMapping.c).
<br><LI> Swaps pages when there is a page fault (VM.c).
<br><LI> Implements the page replacement policy (VMPageReplacement.c).
</p>

<p>The <code>VMBASE</code> library:
<br><LI> Allocates and initializes the page table, reverse page table, locked page table, and TLB.
<br><LI> Patches the exception handlers to properly deal with DSI and ISI exceptions on virtual address reads/writes.
<br><LI> Contains functions to manage the page table.
<br><LI> Contains functions to store or invalidate pages.
</p>

<h3>Virtual Memory Functional Diagram</h3>

<p>Below is a functional diagram showing the relationships between the <code>VM</code> library, the <code>VMBASE</code> library, and the Gekko CPU. The blue groups are where most developers will interact with the virtual memory system. The blue groups also identify individual files within the <code>VM</code> library where functionality is located. The diagram is an abstraction and not every function is represented.
</p>


<table border="0" width="100%">
  <tr>
  <IMG SRC="vm.gif" WIDTH=670 HEIGHT=403 ALIGN=LEFT ALT="Functional Diagram">
  </tr>
</table>


<h3>Virtual Memory Customization</h3>

<p>The <code>VM</code> and <code>VMBASE</code> libraries were split in this manner in order to allow dramatic customization of the virtual memory system. The following ideas are possible extensions:
</p>

<p><strong>Compression:</strong> You can change the <code>VM</code> library to incorporate compression (so that all of the pages held in ARAM are compressed). When a page fault occurs, you can move the page into main memory and decompress the page on-the-fly, before returning control back to the CPU. 
</p>
<p><strong>Improved Memory Manager:</strong> A more rigorous memory manager for virtual memory can be implemented. The current recommendation is to manage the memory in your own game, but you could integrate your own management code into the <code>VM</code> library.
</p>
<p><strong>Customized Page Replacement Policy:</strong> New page replacement policies can be added or existing policies can be modified. The three most conventional page replacement policies are already provided, but you may have unique access patterns that warrant a particular algorithm.
</p>
<p><strong>Customized Monitoring:</strong> The performance monitoring functionality can be customized. You can enhance the data collecting code to provide more details on other aspects of the <code>VM</code> system.
</p>
<p><strong>Pre-paging:</strong> Pages can be pre-paged in anticipation of future access. Possible pitfalls: The pages will get thrown out if they are brought in too early. If they are locked down, someone has to remember to unlock them.
</p>
<p><strong>Locking Pages:</strong> There are hooks in the <code>VMBASE</code> library to lock and unlock pages. One use is to lock pages down for textures, but this would be better to implement without virtual memory (just using a system to transfer data from ARAM to main memory). The reason is that virtual memory excels when normal memory access causes pages to be swapped in as needed. Something like a texture needs to be present as a whole and systems like the GPU can't trigger the CPU to transfer the pages in the first place. Since it must be pre-paged in a block, you would be circumventing all of the benefits of virtual memory.
</p>

</body>
</html>