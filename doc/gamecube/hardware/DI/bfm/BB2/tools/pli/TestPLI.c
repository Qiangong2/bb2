/* 
 * PLI source for driving model from a external ascii text
 * that was not compiled into the simulation executable.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <strings.h>
#include "acc_user.h"
#include "vcsuser.h"

#define MAXKEY 20
#define MAXSTREAMS 32

int  numVecStreams = 0;
char InputLine[256];

struct {
   char key[MAXKEY];
   FILE *vecfp;
   int  more;
   int  fields;
   int  linenum;
} VecStreams[MAXSTREAMS];


/*
 * initialize a  vector stream
 */
int InitVec (int data, int reason)
{
  char *vecfn, *key;
  FILE *vecfp;
  
  if (numVecStreams == MAXSTREAMS) {
    printf("TestPLI WARNING: Maximum vector input streams exceeded.\n");
    printf("                 $InitVec call ignored.\n");
    return 1;
  }

  vecfn = mc_scan_plusargs("vecfn+");
  if (!vecfn) {
    printf("TestPLI WARNING: No \"+vecfn+<fn>\" argument was provided on command line.\n");
    printf("                 $InitVec call ignored.\n");
    return 1;
  }

  key = tf_getcstringp(1);  
  if (!key) {
    printf("TestPLI WARNING: A valid key was not provided (in the first argument).\n");
    printf("                 $InitVec call ignored.\n");
    return 1;
  }

  if (strlen(key) >= MAXKEY) {
    printf("TestPLI WARNING: Too long a key string was specified (in the first argument).\n");
    printf("                 $InitVec call ignored.\n");
    return 1;
  }

  vecfp = fopen(vecfn, "r");
  if (!vecfp) {
    printf("TestPLI: ERROR Unable to open vector input file \"%s\" for key \"%s\".\n", vecfn, key);
    printf("                 $InitVec call aborting.\n");
    exit(1);
  }
  printf("TestPLI: Opened vector file \"%s\" stream=%d key=\"%s\".\n", vecfn, numVecStreams, key);
  strcpy(VecStreams[numVecStreams].key, key);
  VecStreams[numVecStreams].vecfp = vecfp;
  VecStreams[numVecStreams].more = 1;
  VecStreams[numVecStreams].linenum = 0;
  VecStreams[numVecStreams].fields = -1;
  numVecStreams++;
  return 1;
}

/*
 * InitTaggedVec - initialize a vector stream from command-line argument +vecfn_%string%
 */
int InitTaggedVec (int data, int reason)
{
  char *vecfn, *name, *key;
  FILE *vecfp;
  char plusarg[256];
  
  if (numVecStreams == MAXSTREAMS) {
    fprintf(stderr,"TestPLI WARNING: Maximum vector input streams exceeded.\n");
    fprintf(stderr,"                 $InitTaggedVec call ignored.\n");
    tf_putp(0,0);
    return 1;
  }

  /* Get the name of the vecfn command-line argument. */
  name = tf_getcstringp(1);  
  if (!name) {
    fprintf(stderr,"TestPLI WARNING: A valid vecfn tag was not provided (in the first argument).\n");
    fprintf(stderr,"                 $InitTaggedVec call ignored.\n");
    tf_putp(0,0);
    return 1;
  }

  /* Create the plusarg and retrieve. */
  sprintf(plusarg, "vecfn_%s+", name);
  vecfn = mc_scan_plusargs(plusarg);
  /* Try again with the default argument name. */
  if (!vecfn) {
    vecfn = mc_scan_plusargs("vecfn+");
  }
  if (!vecfn) {
    fprintf(stderr,"TestPLI WARNING: No \"+%s<fn>\" or \"+vecfn+<fn>\" argument was provided on command line.\n", plusarg);
    fprintf(stderr,"                 $InitTaggedVec call ignored.\n");
    tf_putp(0,0);
    return 1;
  }

  /* Get the interface key. */
  key = tf_getcstringp(2);  
  if (!key) {
    fprintf(stderr,"TestPLI WARNING: A valid key was not provided (in the second argument).\n");
    fprintf(stderr,"                 $InitTaggedVec call ignored.\n");
    tf_putp(0,0);
    return 1;
  }

  /* Make sure key length isn't too long. */
  if (strlen(key) >= MAXKEY) {
    fprintf(stderr,"TestPLI WARNING: Too long a key string was specified (in the second argument).\n");
    fprintf(stderr,"                 $InitTaggedVec call ignored.\n");
    tf_putp(0,0);
    return 1;
  }

  /* Open the vector file. */
  vecfp = fopen(vecfn, "r");
  if (!vecfp) {
    fprintf(stderr,"TestPLI: WARNING Unable to open vector input file \"%s\" for key \"%s\".\n", vecfn, key);
    fprintf(stderr,"                 $InitTaggedVec call ignored.\n");
    tf_putp(0,0);
    return 1;
  }
  printf("TestPLI: Opened vector file \"%s\" stream=%d key=\"%s\".\n", vecfn, numVecStreams, key);

  /* Save away the details of the stream. */
  strcpy(VecStreams[numVecStreams].key, key);
  VecStreams[numVecStreams].vecfp = vecfp;
  VecStreams[numVecStreams].more = 1;
  VecStreams[numVecStreams].linenum = 0;
  VecStreams[numVecStreams].fields = -1;
  numVecStreams++;
  tf_putp(0,1);
  return 1;
}


/*
 * get some more values from the specified input vector stream
 * return flag indicating if there was anything left for that stream.
 */
int GetVec (int data, int reason)
{
  char *key, *s;  
  char tkey[MAXKEY];
  int stream, j, srch, fndline, cnt = 0, field[20];

  key = tf_getcstringp(1);  

  srch = 1;
  stream = 0;
  while ((stream < numVecStreams) && srch) {
     srch = (strcmp(key, VecStreams[stream].key) != 0);
     if (srch) stream++;
  }

  if (srch) {
    printf("TestPLI: WARNING could not find stream associated with key \"%s\".\n", key);
    printf("                 $GetVec call ignored.\n");
    return 1;
  }


  fndline = 0;
  while (VecStreams[stream].more && !fndline ) {
    s = fgets(InputLine, 256, VecStreams[stream].vecfp);
    VecStreams[stream].linenum++;
    if (s) {
      /* discard lines that don't start with my "key" */
      if (s[0] != '#') {                 /* leading '#' indicates comment */
	cnt = sscanf(s, " %s %x %x %x %x %x %x %x %x %x %x %x %x %x %x %x %x %x %x %x %x",
		     tkey, &field[0], &field[1], &field[2], &field[3], &field[4], &field[5], &field[6], 
		     &field[7], &field[8], &field[9], &field[10], &field[11], &field[12], &field[13], 
		     &field[14], &field[15], &field[16], &field[17], &field[18], &field[19]);
	fndline = (strcmp(tkey, key) == 0);
      }
    } else {
      VecStreams[stream].more = 0;
      fclose(VecStreams[stream].vecfp);
    }
  }
  if (fndline) {
    cnt--;
    if (cnt != VecStreams[stream].fields) {
       if (VecStreams[stream].fields < 0) VecStreams[stream].fields = cnt;
       else {
	 printf("TestPLI: WARNING Unexpected number of fields at line %d.\n", VecStreams[stream].linenum);
	 printf("                 Found %d fields and expected %d fields.\n",
		cnt, VecStreams[stream].fields );
       }
    }
    for(j = 0; j < cnt; j++) {
      tf_putp((j+2), field[j]);
    }
  }

  tf_putp(0, VecStreams[stream].more);
  return 1;
}

/*
 * GetAnyTaggedVec() -
 *   Get some more values from the specified input vector stream.
 *   The vector tag field is ignored when retrieving data, only the
 *   number of fields matters.
 *   Return flag indicating if there was anything left for that stream.
 */
int GetAnyTaggedVec (int data, int reason)
{
  char *key, *s;  
  char tkey[MAXKEY];
  int stream, j, srch, fndline, cnt = 0, field[20];

  key = tf_getcstringp(1);  

  srch = 1;
  stream = 0;
  while ((stream < numVecStreams) && srch) {
     srch = (strcmp(key, VecStreams[stream].key) != 0);
     if (srch) stream++;
  }

  if (srch) {
    fprintf(stderr,"TestPLI: WARNING could not find stream associated with key \"%s\".\n", key);
    fprintf(stderr,"                 $GetAnyTaggedVec call ignored.\n");
    return 1;
  }


  fndline = 0;
  while (VecStreams[stream].more && !fndline ) {
    s = fgets(InputLine, 256, VecStreams[stream].vecfp);
    VecStreams[stream].linenum++;
    if (s) {
      if (s[0] != '#') {                 /* leading '#' indicates comment */
	cnt = sscanf(s, " %s %x %x %x %x %x %x %x %x %x %x %x %x %x %x %x %x %x %x %x %x",
		     tkey,
		     &field[0], &field[1], &field[2], &field[3],
		     &field[4], &field[5], &field[6], &field[7],
		     &field[8], &field[9], &field[10], &field[11],
		     &field[12], &field[13], &field[14], &field[15],
		     &field[16], &field[17], &field[18], &field[19]);

	/* Always accept the line. */
	fndline = 1;
      }
    } else {
      VecStreams[stream].more = 0;
      fclose(VecStreams[stream].vecfp);
    }
  }
  if (fndline) {
    cnt--;
    if (cnt != VecStreams[stream].fields) {
       if (VecStreams[stream].fields < 0) VecStreams[stream].fields = cnt;
       else {
	 fprintf(stderr,"TestPLI: WARNING Unexpected number of fields at line %d.\n", VecStreams[stream].linenum);
	 fprintf(stderr,"                 Found %d fields and expected %d fields.\n",
		cnt, VecStreams[stream].fields );
       }
    }
    for(j = 0; j < cnt; j++) {
      tf_putp((j+2), field[j]);
    }
  }

  tf_putp(0, VecStreams[stream].more);
  return 1;
}

/*
 * GetUntaggedVec() -
 *   Get some more values from the specified input vector stream.
 *   The vector tag field is not present when retrieving data, only the
 *   number of fields matters.
 *   Argument identifies an open vector stream.
 *   Return flag indicating if there was anything left for that stream.
 */
int GetUntaggedVec (int data, int reason)
{
  char *key, *s;  
  int stream, j, srch, fndline, cnt = 0, field[20];

  key = tf_getcstringp(1);  

  srch = 1;
  stream = 0;
  while ((stream < numVecStreams) && srch) {
     srch = (strcmp(key, VecStreams[stream].key) != 0);
     if (srch) stream++;
  }

  if (srch) {
    fprintf(stderr,"TestPLI: WARNING could not find stream associated with key \"%s\".\n", key);
    fprintf(stderr,"                 $GetUntaggedVec call ignored.\n");
    return 1;
  }


  fndline = 0;
  while (VecStreams[stream].more && !fndline ) {
    s = fgets(InputLine, 256, VecStreams[stream].vecfp);
    VecStreams[stream].linenum++;
    if (s) {
      if (s[0] != '#') {                 /* leading '#' indicates comment */
	cnt = sscanf(s, " %x %x %x %x %x %x %x %x %x %x %x %x %x %x %x %x %x %x %x %x",
		     &field[0], &field[1], &field[2], &field[3],
		     &field[4], &field[5], &field[6], &field[7],
		     &field[8], &field[9], &field[10], &field[11],
		     &field[12], &field[13], &field[14], &field[15],
		     &field[16], &field[17], &field[18], &field[19]);

	/* Always accept the line. */
	fndline = 1;
      }
    } else {
      VecStreams[stream].more = 0;
      fclose(VecStreams[stream].vecfp);
    }
  }
  if (fndline) {
    if (cnt != VecStreams[stream].fields) {
       if (VecStreams[stream].fields < 0) VecStreams[stream].fields = cnt;
       else {
	 fprintf(stderr,"TestPLI: WARNING Unexpected number of fields at line %d.\n", VecStreams[stream].linenum);
	 fprintf(stderr,"                 Found %d fields and expected %d fields.\n",
		cnt, VecStreams[stream].fields );
       }
    }
    for(j = 0; j < cnt; j++) {
      tf_putp((j+2), field[j]);
    }
  }

  tf_putp(0, VecStreams[stream].more);
  return 1;
}

/*
 * get a command line value
 * the command line argument must be of form "+<string>+<decimal-string>"
 * verilog caller provides string in 1st agument;
 * GetCLVal returns the integer value to the verilog caller.
 * if the argument is not found -1 is returned to the verilog caller.
 */

int GetCLVal (int data, int reason)
{
   char *s, *clarg;
   int val;

   s = tf_getcstringp(1);
   clarg = mc_scan_plusargs(s);
   if (clarg) val = atoi(clarg);
   else val = -1;
   tf_putp(0, val);
   return 1;
}

/*
 * get a command line string
 */

void GetCLStr (int data, int reason)
{
  s_tfexprinfo ExprInfo;
  char *FirstArg, *Argument;
  int Continue, Group, i, ArgLength, Foo, j;
  int Status = -1;

  if ((tf_nump() != 2) || (tf_typep(1) != tf_string) || (tf_typep(2) != tf_readwrite))
    {
      tf_putp(0, -1);
      return;
    }

  FirstArg = (char *)tf_getp(1);
  if ((Argument = mc_scan_plusargs(FirstArg)) != 0)
    {
      tf_exprinfo(2, &ExprInfo);
      ArgLength = strlen(Argument);

      bzero((char *)ExprInfo.expr_value_p, ExprInfo.expr_ngroups * sizeof(struct t_vecval));
      if (Argument[0])
	{
	  Status   = 1;
	  Continue = 1;
	  Group    = 0;
	  i        = ArgLength - 1;
	  while (Group < ExprInfo.expr_ngroups)
	    {
	      Foo = 0;
	      j   = 0;
	      while ((i >= 0) && j++ < 4)
		{
		  ((char *) &Foo)[4-j] = Argument[i];
		  Continue             = (i-- >= 0);
		}

	      ExprInfo.expr_value_p[Group].avalbits = (Continue ? Foo : 0);
	      Group++;
	    }
	}
      else
	Status = 0;
      
      tf_propagatep(2);      
    }
  tf_putp(0, Status);
}  


int FileExist (int data, int reason)
{
   char *s;
   int val;

   s = tf_getcstringp(1);
   val = access(s,F_OK);
   if (val == 0x0) tf_putp(0, 1); /*  File found */
   else    tf_putp(0, 0);         /*  File not found */
   return(1);
}
