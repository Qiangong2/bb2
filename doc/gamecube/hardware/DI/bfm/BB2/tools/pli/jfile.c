/*  */
/*  These routines are meant to replace fopen/fdisplay/etc. */
/*  The allocate fd's above 32 thereby getting around the limit of */
/*  32 files imposed by verilog... */
/*   */
/*  int = jopen(char *path); */
/*  void jclose(int); */
/*  void jdisplay(int, char *format, ...) */
/*  void jflush(int); */
/*  */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/resource.h>
#include <fcntl.h>
#include <errno.h>
#include <ctype.h>

#include "vcsuser.h"

#define START_FD        40

static FILE *
hiOpen(const char *path)
{
    int fd;
    int f = -1;
    int i;
    int maxFd;
    struct rlimit rl;

    if (getrlimit(RLIMIT_NOFILE, &rl)) {
        io_printf("unable to get max fd\n");
        maxFd = 32;
    }
    else {
        maxFd = rl.rlim_cur;
    }

    /*  Find an unsed fd above START_FD */
    for( i = START_FD ; ; i++ ) {
        /*  run out of fd's??? */
        if ( i >= maxFd ) {
            io_printf("jopen: unable to open fd above 32.\n");
            goto fail;
        }

        if ( (fd = dup(i)) < 0 ) {
            if ( errno == EBADF ) {
                break;
            }
        }
        close(fd);
    }
    fd = i;

    f = open(path, O_WRONLY | O_CREAT, 0666);
    if ( f < 0 ) {
        perror("open");
        io_printf("jopen: unable to open %s\n", path);
        tf_dofinish();
        goto fail;
    }
    if ( f >= START_FD  ) {
/*         io_printf("jopen: path %s - fd %d\n", f); */
        fd = f;
    }
    else {
/*         io_printf("jopen: path %s duping %d:%d\n", path, f, fd); */
        if ( dup2(f, fd) < 0 ) {
            io_printf("jopen: unable to dup %d:%d\n", f, fd);
            goto fail;
        }
        /*  close the original */
        close(f);
    }

    return fdopen(fd, "w");

fail:
    if ( f >= 0 ) {
        close(f);
    }
    return fopen(path, "w");
}

int
check_jopen()
{
    if ( tf_nump() != 1 ) {
        tf_error("$jopen: parameter count mismatch %d, 1\n", tf_nump());
        tf_dofinish();
    }

    /*  check the output parm types */
    if ( tf_typep(1) != tf_string ) {
        tf_error("$jopen: arg not a string - %d\n", tf_typep(1));
        tf_dofinish();
    }

    return 0;
}

int
call_jopen()
{
    FILE *fp;

    /*  open the file */
    /*  XXX - format char is ignored for strs, but appears to be necessary */
    fp = hiOpen(tf_strgetp(1, 'b'));
    if ( fp == NULL ) {
        tf_error("jopen: unable to open file %s\n", tf_strgetp(1, 'b'));
        tf_dofinish();
    }

    tf_putp(0, (int)fp);

    return 0;
}

int
check_jclose()
{
    if ( tf_nump() != 1 ) {
        tf_error("$jclose: parameter count mismatch %d, 1\n", tf_nump());
        tf_dofinish();
    }

    /*  check the output parm types */
    if ( tf_typep(1) != tf_readwrite ) {
        tf_error("$jopen: arg not readwrite - %d\n", tf_typep(1));
        tf_dofinish();
    }

    return 0;
}

int
call_jclose()
{
    fclose((FILE *)tf_getp(1));

    return 0;
}

int
check_jflush()
{
    if ( tf_nump() != 1 ) {
        tf_error("$jflush: parameter count mismatch %d, 1\n", tf_nump());
        tf_dofinish();
    }

    /*  check the output parm types */
    if ( tf_typep(1) != tf_readwrite ) {
        tf_error("$jflush: arg not readwrite - %d\n", tf_typep(1));
        tf_dofinish();
    }

    return 0;
}

int
call_jflush()
{
    fflush((FILE *)tf_getp(1));

    return 0;
}

int
check_jdisplay()
{
    if ( tf_nump() < 2 ) {
        tf_error("$jdisplay: parameter count mismatch %d, >= 2 \n", tf_nump());
        tf_dofinish();
    }

    /*  check the output parm types */
    if ( tf_typep(1) != tf_readwrite ) {
        tf_error("$jdisplay: arg 1 not readwrite - %d\n", tf_typep(1));
        tf_dofinish();
    }

    if ( tf_typep(2) != tf_string ) {
        tf_error("$jdisplay: arg 2 not string - %d\n", tf_typep(2));
        tf_dofinish();
    }

    /*  The remainder don't really matter so much */

    return 0;
}

/* static char *baseStr = "0123456789ABCDEF"; */
#define FBLEN    128

static void
prtStr(FILE *fp, char *val, int digs, int ldZero)
{
    char *sp;
    int   len;

    if ( val == NULL ) {
        io_printf("prtStr: null value\n");
        return;
    }

    /*  skip past leading zero's */
    for( sp = val ; *sp == '0' ; sp++ );
    if ( (*sp == 0) && (sp > val) ) {
        sp--;
    }

    /*  Add leading zero's or blanks to digit */
    for ( len = strlen(sp) ; len < digs ; len++ ) {
        fprintf(fp, "%c", ldZero ? '0' : ' ');
    }

    fprintf(fp, "%s", sp);
}

int
call_jdisplay()
{
    FILE *fp;
    char *cp, *sp, *sv, *pp;
    int   ldZero, digs;
    int   base;
    int   cc;
    int   ai;

    fp = (FILE *)tf_getp(1);

    /*  get the format string */
    sv = sp = strdup(tf_strgetp(2, 'b'));
    
    /*  pull out the first string */
    cp = strchr(sp, '%');
    if ( cp == NULL ) {
        fprintf(fp, sp);
        goto done;
    }

    /*  Is the first char a %% */
    if ( cp != sp ) {
        *cp = 0;
        fprintf(fp, "%s", sp);
    }

    /*  now walk through the args */
    ai = 2;
    while ( cp ) {
        *cp = '%';
        sp = cp;
        ai++;

        /*  find the next % */
        /*  Check for the always annoying %% */
        if ( sp[1] == '%' ) {
            cp = strchr(sp+2, '%');
            fprintf(fp, sp);
            /*  cleanup */
            ai--;
            continue;
        }

        /*  find the next % */
        cp = strchr(sp+1, '%');

        /*  stop at the next format */
        if ( cp ) {
            *cp = 0;
        }

        /*  find the format character */
        for( pp = sp ; *pp && !isalpha((int)*pp) ; pp++ ); 

        /*  Have we run out of args? */
        if ( ai > tf_nump() ) {
            fprintf(fp, "%%(NO ARG)%s", sp+1);
            continue;
        }

        /*  formatter. */
        /*  cc2 = pp[1]; */
        /*  pp[1] = 0; */
        ldZero = (sp[1] == '0');
        /*  check for number of digits */
        digs = -1;
        if ( ldZero) {
            if ( isdigit((int)sp[2]) ) {
                digs = atoi(sp+2);
            }
        }
        else if ( isdigit((int)sp[1]) ) {
            digs = atoi(sp+1);
        }
        switch (*pp) {
        case 'h':
        case 'H':
        case 'x':
        case 'X':
            base = 'h';
            break;
        case 'D':
        case 'd':
            base = 'd';
            break;
        case 'O':
        case 'o':
            base = 'o';
            break;
        case 'b':
        case 'B':
            base = 'b';
            break;
        case 'f':
            fprintf(fp, "%f%s", tf_getrealp(ai), pp+1);
            continue;
        default:
            cc = (*pp) ? *pp : '?';
            fprintf(fp, "%%(%c NOT SUPPORTED)%s", cc, sp+1);
            continue;
        }

        if ( digs < 0 ) {
            if ( base == 'h' ) {
                ldZero = 1;
                digs = (tf_sizep(ai)+3) / 4;
                if ( digs <= 0 ) {
                    io_printf("jdisplay: bad size %d\n", tf_sizep(ai));
                    digs = 1;
                }
            }
            else if ( base == 'd' ) {
                digs = 11;
            }
        }

        /*  pp[1] = cc2; */
        prtStr(fp, tf_strgetp(ai, base), digs, ldZero);
        fprintf(fp, pp+1);
    }

done:
    /*  free the dup'd string */
    free(sv);

    /*  very annoying... there's an implied newline */
    fprintf(fp, "\n");
    fflush(fp);

    return 0;
}
