`define ECore system.flipper.flipper_core
`include "vi_state.v"
`include "vi_reg.v"

module monitor_vi_en ();
   
    integer clk_count;
    reg [8*256:1] vi_en_dump;
    reg [8*256:1] vi_intr_dump;
    integer 	vi_en_dump_fd;
    integer 	vi_trg_int_dump_fd;
	integer	dump_output;
reg		trigger, trigger_d1;
wire 		trigger_detected;
reg [9:0]       prb_pst, psb_pst;
wire 		vsync;
wire 		hsync;
wire [1:0]	trg1_md;
wire [1:0]	trg2_md;
wire		hsync_detected, vsync_detected;
reg 		start_chk;
wire [10:0]	hct0, hct1, hct2, hct3;
wire [10:0]	vct0, vct1, vct2, vct3;
wire 		int, int0, int1, int2, int3;
wire 		int_detected, int0_detected, int1_detected, int2_detected, int3_detected;
reg 		trg1_d1, trg2_d1;
reg 		vsync_d1, hsync_d1;
reg 		vsync_detected_d1;
reg 		int_d1, int0_d1, int1_d1, int2_d1, int3_d1;
reg 		trg1, trg2;
reg [9:0]	hlw;
reg [9:0]	hbe;
reg [9:0]	hbs;
reg [2:0]	equ;
reg [9:0]	acv;
reg 		read_int0, read_int1, read_int2, read_int3;
reg 		read_gun0, read_gun1;
reg 		read_pending;
reg [8:1]	read_addr;
reg [10:0]	x1_pos, x2_pos;
wire [10:0]	xpos;
reg [10:0]	xpos_d1;
reg [10:0]	x1_hcount, x2_hcount;
wire [10:0]	xhcount;
reg 		x1_state, x2_state;
wire		xstate;
reg [10:0]	y1_pos, y2_pos;
wire [10:0] 	 ypos;
reg [10:0] 	 ypos_d1;
reg [2:0] 	y1_state, y2_state;
wire [2:0]	ystate;
reg [10:0]	y1_hlcount, y2_hlcount;
wire [10:0] 	yhlcount;
wire [9:0]    	prb;
wire [9:0]    	psb;
reg [10:0]	act_hcount1, act_hcount2, act_hcount;
reg [10:0] 	act_vcount1, act_vcount2, act_vcount;
reg 		expecting_read;
reg [10:0]	hct_read;
reg [10:0]	vct_read;
wire 		trg1_detected, trg2_detected;
reg 		vstart;
wire [1:0]	fld_cnt;
reg 		fld_lsb_d1;
wire 		fld_low_detected;
   
parameter       idle = `IDLE,
                pre_eq = `PRE_EQ,  
                serr = `SERR,
                post_eq = `POST_EQ,
                pre_blnk = `PRE_BLNK,
                acv_state = `ACV,
                post_blnk = `POST_BLNK,
		second_hl = `SECOND_HL,
                first_hl = `FIRST_HL;

/* --------------------------------------------------------------------	*/
/* Open and close output file						*/
/* --------------------------------------------------------------------	*/

    initial
    begin
	clk_count = 0;
       
	if ($GetCLVal("vi_en_dump+") == 0)
	    dump_output = 0;
	else
	begin
	     dump_output = 1;	     
		vi_en_dump_fd = $fopen("diag/vi_en.tr");
	end 

	if ($GetCLVal("vi_gun_dump+") == 1)
	 begin
		vi_trg_int_dump_fd = $fopen("diag/vi_intr.dmp");
	    dump_output = 1;
	 end
	else
	     dump_output = 0;	     
    end

/* --------------------------------------------------------------------	*/
/* Monitor bus to ext encoder						*/
/* --------------------------------------------------------------------	*/

    always @(posedge `ECore.vi0.vClk)
    begin
//	    $fdisplay(vi_en_dump_fd,"tv_pe %h %h # %d",
//		`ECore.vicr_topad, `ECore.vid_topad, $time());
	    $fdisplay(vi_en_dump_fd,"%h %h",
		`ECore.vicr, `ECore.vid);

	if( (clk_count%1000) == 0 )
 	begin
	    $fflush(vi_en_dump_fd);
	   $display("display time = %d\n", $time());
	end
	clk_count = clk_count + 1;
    end


assign trigger_detected = trigger && ~trigger_d1;
assign prb = `ECore.vi0.vi_vercnt.prb_pst;
assign psb = `ECore.vi0.vi_vercnt.psb_pst;
assign vsync = `ECore.vi0.vi_vercnt.vsyncb;
assign hsync = `ECore.vi0.vi_horcnt.hsyncb;
assign trg1_md = `ECore.vi0.vi_pi.g0md_pst;
assign trg2_md = `ECore.vi0.vi_pi.g1md_pst;
assign hsync_detected = ~hsync && hsync_d1;
assign vsync_detected = ~vsync && vsync_d1;
assign hct0 = `ECore.vi0.vi_pi.hct0;
assign hct1 = `ECore.vi0.vi_pi.hct1;
assign hct2 = `ECore.vi0.vi_pi.hct2;
assign hct3 = `ECore.vi0.vi_pi.hct3;
assign vct0 = `ECore.vi0.vi_pi.vct0;
assign vct1 = `ECore.vi0.vi_pi.vct1;
assign vct2 = `ECore.vi0.vi_pi.vct2;
assign vct3 = `ECore.vi0.vi_pi.vct3;
assign int = `ECore.vi_piVrtIntr;
assign int_detected = int && ~int_d1;
assign int0 = `ECore.vi0.vi_pi.int0;
assign int1 = `ECore.vi0.vi_pi.int1;
assign int2 = `ECore.vi0.vi_pi.int2;
assign int3 = `ECore.vi0.vi_pi.int3;
assign int0_detected = int0 && ~int0_d1;
assign int1_detected = int1 && ~int1_d1;
assign int2_detected = int2 && ~int2_d1;
assign int3_detected = int3 && ~int3_d1;
assign trg1_detected = `ECore.vi0.vi_vercnt.gun0_trg_edge && ~`ECore.vi0.vi_vercnt.gun0_vtrg;
assign trg2_detected = `ECore.vi0.vi_vercnt.gun1_trg_edge && ~`ECore.vi0.vi_vercnt.gun1_vtrg;
`ifdef TESTER_MODE
`else
assign system.g_guntrg0 = trg1;
assign system.g_guntrg1 = trg2;
`endif
assign xpos = `ECore.vi0.vi_horcnt.hct_out;
assign xhcount = `ECore.vi0.vi_horcnt.hcount;
assign xstate = `ECore.vi0.vi_horcnt.current_hstate;
assign ypos = `ECore.vi0.vi_vercnt.vct;
assign ystate = `ECore.vi0.vi_vercnt.current_vstate;
assign yhlcount = `ECore.vi0.vi_vercnt.HL_count;
assign fld_cnt = `ECore.vi0.vi_vercnt.fld_cnt;


   always @(posedge `ECore.vi0.pi_viClk)
    begin

	
	if (`ECore.vi0.vi_pi.pi_write)
	 begin
		if (~`ECore.vi0.viResetb)
			trg1 <= 1'b0;
		else if (`ECore.vi0.vi_pi.pi_viAddr_d1 == 8'h50)
			trg1 <= `ECore.vi0.vi_pi.pi_viData_d1[0];
		
		if (~`ECore.vi0.viResetb)
			trg2 <= 1'b0;
		else if (`ECore.vi0.vi_pi.pi_viAddr_d1 == 8'h60)
			trg2 <= `ECore.vi0.vi_pi.pi_viData_d1[0];
	
		if (`ECore.vi0.vi_pi.pi_viAddr_d1 == `VI_HOR_TIM0_L)
			hlw <= `ECore.vi0.vi_pi.pi_viData_d1[`VI_HOR_TIM0_L_REG_HLW];
	
		if (`ECore.vi0.vi_pi.pi_viAddr_d1 == `VI_HOR_TIM1_L) 
	                hbe[8:0] <= `ECore.vi0.vi_pi.pi_viData_d1[`VI_HOR_TIM1_L_REG_HBE_L];
	
		if (`ECore.vi0.vi_pi.pi_viAddr_d1 == `VI_HOR_TIM1_U)
	                hbe[9] <= `ECore.vi0.vi_pi.pi_viData_d1[`VI_HOR_TIM1_U_REG_HBE_U];
		
		if (`ECore.vi0.vi_pi.pi_viAddr_d1 == `VI_HOR_TIM1_U)
	                hbs <= `ECore.vi0.vi_pi.pi_viData_d1[`VI_HOR_TIM1_U_REG_HBS];
	            
		if (`ECore.vi0.vi_pi.pi_viAddr_d1 == `VI_VER_TIM)
       	                equ <= `ECore.vi0.vi_pi.pi_viData_d1[`VI_VER_TIM_REG_EQU];

		if (`ECore.vi0.vi_pi.pi_viAddr_d1 == `VI_VER_TIM)
       	                acv <= `ECore.vi0.vi_pi.pi_viData_d1[`VI_VER_TIM_REG_ACV];
	
	end


// Writing to reg 0x70 bits [3:0] will tell the monitor to check the status of 
// the respective interrupts and make sure they are cleared 
		if ((`ECore.vi0.vi_pi.pi_viAddr_d1 == 8'h70) && (`ECore.vi0.vi_pi.pi_write))
                        read_int0 <= `ECore.vi0.vi_pi.pi_viData_d1[0];
		else 	
			read_int0 <= 1'b0;

		if ((`ECore.vi0.vi_pi.pi_viAddr_d1 == 8'h70) && (`ECore.vi0.vi_pi.pi_write))
                        read_int1 <= `ECore.vi0.vi_pi.pi_viData_d1[1];
		else 	
			read_int1 <= 1'b0;

		if ((`ECore.vi0.vi_pi.pi_viAddr_d1 == 8'h70) && (`ECore.vi0.vi_pi.pi_write))
                        read_int2 <= `ECore.vi0.vi_pi.pi_viData_d1[2];
		else 	
			read_int2 <= 1'b0;

		if ((`ECore.vi0.vi_pi.pi_viAddr_d1 == 8'h70) && (`ECore.vi0.vi_pi.pi_write))
                        read_int3 <= `ECore.vi0.vi_pi.pi_viData_d1[3];
		else 	
			read_int3 <= 1'b0;

// Writing to reg 0x80 bits [1:0] will tell the monitor to check the status of
// the respective gun triggers and make sure they are cleared

		if ((`ECore.vi0.vi_pi.pi_viAddr_d1 == 8'h80) && (`ECore.vi0.vi_pi.pi_write))
                        read_gun0 <= `ECore.vi0.vi_pi.pi_viData_d1[0];
		else 	
			read_gun0 <= 1'b0;

		if ((`ECore.vi0.vi_pi.pi_viAddr_d1 == 8'h80) && (`ECore.vi0.vi_pi.pi_write))
                        read_gun1 <= `ECore.vi0.vi_pi.pi_viData_d1[1];
		else 	
			read_gun1 <= 1'b0;

                        

	if (~`ECore.vi0.viResetb)
	 begin
		read_pending <= 1'b0;
                read_addr <= 8'h00;
	end
	else if (`ECore.vi0.vi_pi.pi_read)
	 begin
		read_pending <= 1'b1;
		read_addr <= `ECore.vi0.vi_pi.pi_viAddr_d1;
	 end
	else if (`ECore.vi0.vi_pi.vi_piAck && read_pending)
	 begin
		read_pending <= 1'b0;
		read_addr <= 8'h00;
	 end
	


	

// GUN testing
	if (read_pending && `ECore.vi0.vi_pi.vi_piAck)
	 begin
		if (read_addr == `VI_DSP_LATCH0_L)
			if (`ECore.vi_piData[`VI_DSP_LATCH0_L_REG_GUN0_HCT] != ((act_hcount1 +1) >>1))
                       		$fdisplay(vi_trg_int_dump_fd,"Error expected GUN0HCT :%h actual :%h time: %d",
                                	((act_hcount1 +1) >>1), `ECore.vi_piData[`VI_DSP_LATCH0_L_REG_GUN0_HCT], $time);
		if (read_addr == `VI_DSP_LATCH0_U)
			if (`ECore.vi_piData[`VI_DSP_LATCH0_U_REG_GUN0_VCT] != act_vcount1)
                       		$fdisplay(vi_trg_int_dump_fd,"Error expected GUN0VCT :%h actual :%h time: %d",
                                	act_vcount1, `ECore.vi_piData[`VI_DSP_LATCH0_U_REG_GUN0_VCT], $time);
		if (read_addr == `VI_DSP_LATCH1_L)
			if (`ECore.vi_piData[`VI_DSP_LATCH1_L_REG_GUN1_HCT] != ((act_hcount2 +1) >>1))
                       		$fdisplay(vi_trg_int_dump_fd,"Error expected GUN1HCT :%h actual :%h time: %d",
                                	((act_hcount2 +1) >>1), `ECore.vi_piData[`VI_DSP_LATCH1_L_REG_GUN1_HCT], $time);
		if (read_addr == `VI_DSP_LATCH1_U)
			if (`ECore.vi_piData[`VI_DSP_LATCH1_U_REG_GUN1_VCT] != act_vcount2)
                       		$fdisplay(vi_trg_int_dump_fd,"Error expected GUN1VCT :%h actual :%h time: %d",
                                	act_vcount2, `ECore.vi_piData[`VI_DSP_LATCH1_U_REG_GUN1_VCT], $time);
	 end

	if ((read_gun0) && (`ECore.vi0.vi_pi.gun0_trg != 1'b0))
		$fdisplay(vi_trg_int_dump_fd,"Error: Hey Jerk, you told me to check if GUN_TRG0 is clear, it aint GUN0HCT %h GUN0VCT :%h  time: %d",
                                      `ECore.vi0.vi_pi.gfx_g0hct  , `ECore.vi0.vi_pi.gfx_g0vct, $time);
	else if  ((read_gun0) && (`ECore.vi0.vi_pi.gun0_trg == 1'b0))
		 $fdisplay(vi_trg_int_dump_fd,"Hey Jerk, you told me to check if GUN_TRG0 is clear, it is time: %d", $time);
	
	if ((read_gun1) && (`ECore.vi0.vi_pi.gun1_trg != 1'b0))
		$fdisplay(vi_trg_int_dump_fd,"Error: Hey Jerk, you told me to check if GUN_TRG1 is clear, it aint GUN1HCT %h GUN1VCT :%h time: %d",
                                      `ECore.vi0.vi_pi.gfx_g1hct  , `ECore.vi0.vi_pi.gfx_g1vct, $time);
	else if ((read_gun1) && (`ECore.vi0.vi_pi.gun1_trg == 1'b0))
			$fdisplay(vi_trg_int_dump_fd,"Hey Jerk, you told me to check if GUN_TRG1 is clear, it is time: %d", $time);



// Status Reg reads


		
	if (read_pending && `ECore.vi0.vi_pi.vi_piAck)
	 begin
		if (read_addr == `VI_DSP_POS_L)
			if (`ECore.vi_piData[`VI_DSP_POS_L_REG_HCT] != hct_read)
                       		$fdisplay(vi_trg_int_dump_fd,"Error expected HCT :%h actual :%h time: %d",
                                	hct_read, `ECore.vi_piData[`VI_DSP_POS_L_REG_HCT], $time);
			else
				$fdisplay(vi_trg_int_dump_fd,"GOOD! expected HCT :%h actual :%h time: %d",
                                        hct_read, `ECore.vi_piData[`VI_DSP_POS_L_REG_HCT], $time);
		if (read_addr == `VI_DSP_POS_U)
			if (`ECore.vi_piData[`VI_DSP_POS_U_REG_VCT] != vct_read)
                       		$fdisplay(vi_trg_int_dump_fd,"Error expected VCT :%h actual :%h time: %d",
                                	vct_read, `ECore.vi_piData[`VI_DSP_POS_U_REG_VCT], $time);
			else
				$fdisplay(vi_trg_int_dump_fd,"GOOD! expected VCT :%h actual :%h time: %d",
                                        vct_read, `ECore.vi_piData[`VI_DSP_POS_U_REG_VCT], $time);
	
	 end





	if ((read_int0) && (`ECore.vi0.vi_pi.int0 != 1'b0))
		$fdisplay(vi_trg_int_dump_fd,"Error: Hey Jerk, you told me to check if INT0 is clear, it aint time: %d", $time);
	else if ((read_int0) && (`ECore.vi0.vi_pi.int0 == 1'b0))
		$fdisplay(vi_trg_int_dump_fd,"Hey Jerk, you told me to check if INT0 is clear, it is time: %d", $time);

	if ((read_int1) && (`ECore.vi0.vi_pi.int1 != 1'b0))
		$fdisplay(vi_trg_int_dump_fd,"Error: Hey Jerk, you told me to check if INT1 is clear, it aint time: %d", $time);
	else if ((read_int1) && (`ECore.vi0.vi_pi.int1 == 1'b0))
		$fdisplay(vi_trg_int_dump_fd,"Hey Jerk, you told me to check if INT1 is clear, it is time: %d", $time);

	if ((read_int2) && (`ECore.vi0.vi_pi.int2 != 1'b0))
		$fdisplay(vi_trg_int_dump_fd,"Error: Hey Jerk, you told me to check if INT2 is clear, it aint time: %d", $time);
	else if ((read_int2) && (`ECore.vi0.vi_pi.int2 == 1'b0))
		$fdisplay(vi_trg_int_dump_fd,"Hey Jerk, you told me to check if INT2 is clear, it is time: %d", $time);

	if ((read_int3) && (`ECore.vi0.vi_pi.int3 != 1'b0))
		$fdisplay(vi_trg_int_dump_fd,"Error: Hey Jerk, you told me to check if INT3 is clear, it aint time: %d", $time);
	else if ((read_int3) && (`ECore.vi0.vi_pi.int3 == 1'b0))
		$fdisplay(vi_trg_int_dump_fd,"Hey Jerk, you told me to check if INT3 is clear, it is time: %d", $time);


end

assign fld_low_detected = ~fld_cnt[0] && fld_lsb_d1;

always @ (posedge `ECore.vi0.vClk)
 begin
	xpos_d1 <= xpos;
	ypos_d1 <= ypos;
	trg1_d1 <= trg1;
	trg2_d1 <= trg2;
	vsync_d1 <= vsync;
	hsync_d1 <= hsync;
	vsync_detected_d1 <= vsync_detected;
	int_d1 <= int;	
	int0_d1 <= int0;
	int1_d1 <= int1;
	int2_d1 <= int2;
	int3_d1 <= int3;
	fld_lsb_d1 <= fld_cnt[0];
	
	if (hsync_detected && (ystate == pre_eq) && fld_low_detected)
		act_vcount <= 11'b01;
	else if (hsync_detected)
		act_vcount <= act_vcount + 1;
	
	if ((trg1_detected) && (`ECore.vi0.vi_pi.g0md_pst != 2'b00))
		act_vcount1 <= act_vcount;
	if ((trg2_detected) && (`ECore.vi0.vi_pi.g1md_pst != 2'b00))
		act_vcount2 <= act_vcount;

	if (hsync_detected)
		act_hcount <= 11'b01;
	else
		act_hcount <= act_hcount + 1;


	if (vsync_detected)
		start_chk <= 1'b1;
	if (`ECore.vi0.vi_vercnt.new_field_detected && ~fld_cnt[0])
		vstart <= 1'b1;


	if ((trg1_detected)  && (`ECore.vi0.vi_pi.g0md_pst != 2'b00))
	 begin
		x1_pos <= `ECore.vi0.vi_horcnt.hct_out;
		x1_hcount <= `ECore.vi0.vi_horcnt.hcount;
		x1_state <= `ECore.vi0.vi_horcnt.current_hstate;
		y1_pos <= `ECore.vi0.vi_vercnt.vct;
		y1_state <= `ECore.vi0.vi_vercnt.current_vstate;
		y1_hlcount <= `ECore.vi0.vi_vercnt.HL_count;
		act_hcount1 <= act_hcount;
	 end

	if ((trg2_detected) && (`ECore.vi0.vi_pi.g1md_pst != 2'b00))
	 begin
		x2_pos <= `ECore.vi0.vi_horcnt.hct_out;
		x2_hcount <= `ECore.vi0.vi_horcnt.hcount;
		x2_state <= `ECore.vi0.vi_horcnt.current_hstate;
		y2_pos <= `ECore.vi0.vi_vercnt.vct;
		y2_state <= `ECore.vi0.vi_vercnt.current_vstate;
		y2_hlcount <= `ECore.vi0.vi_vercnt.HL_count;
		act_hcount2 <= act_hcount;
	 end

		

	if (~`ECore.vi0.viResetb)
		expecting_read <= 1'b0;
	else if ((`ECore.vi0.vi_pi.vct_req_edge || `ECore.vi0.vi_pi.hct_req_edge) && ~expecting_read)
			expecting_read <= 1'b1; 
	else if ((`ECore.vi0.vi_pi.vct_req_edge || `ECore.vi0.vi_pi.hct_req_edge) && expecting_read)
			expecting_read <= 1'b0; 
			

/*	else if (`ECore.vi0.vi_pi.pi_read)
		if (((`ECore.vi0.vi_pi.pi_viAddr_d1 == `VI_DSP_POS_L) || (`ECore.vi0.vi_pi.pi_viAddr_d1 == `VI_DSP_POS_U)) && ~expecting_read)
			expecting_read <= 1'b1; 
		else if (((`ECore.vi0.vi_pi.pi_viAddr_d1 == `VI_DSP_POS_L) || (`ECore.vi0.vi_pi.pi_viAddr_d1 == `VI_DSP_POS_U)) && expecting_read)
			expecting_read <= 1'b0; */

	if ( ((`ECore.vi0.vi_pi.vct_req_edge) || (`ECore.vi0.vi_pi.hct_req_edge)) && ~expecting_read)
	 begin	
		hct_read <= xpos;
		vct_read <= ypos;
         end


// Interrupt Testing

	if (int0_detected)
	 begin
		$fdisplay(vi_trg_int_dump_fd,"INT0 detected!!! time: %d", $time);
		if (act_vcount != vct0)
     			$fdisplay(vi_trg_int_dump_fd,"Error expected VCT0 :%h actual :%h time: %d",
  		        	act_vcount, vct0, $time);
		if (act_hcount[10:1] != hct0)
     			$fdisplay(vi_trg_int_dump_fd,"Error expected HCT0 :%h actual :%h time: %d",
  		        	act_hcount[10:1], hct0, $time);
	  end


	if (int1_detected)
	 begin
		$fdisplay(vi_trg_int_dump_fd,"INT1 detected!!! time: %d", $time);
		if (act_vcount != vct1)
     			$fdisplay(vi_trg_int_dump_fd,"Error expected VCT1 :%h actual :%h time: %d",
  		        	act_vcount, vct1, $time);
		if (act_hcount[10:1] != hct1)
     			$fdisplay(vi_trg_int_dump_fd,"Error expected HCT1 :%h actual :%h time: %d",
  		        	 act_hcount[10:1], hct1, $time);
	  end

	if (int2_detected)
	 begin
		$fdisplay(vi_trg_int_dump_fd,"INT2 detected!!!");
		if (act_vcount != vct2)
     			$fdisplay(vi_trg_int_dump_fd,"Error expected VCT2 :%h actual :%h time: %d",
  		        	act_vcount, vct2, $time);
		if (act_hcount[10:1] != hct2)
     			$fdisplay(vi_trg_int_dump_fd,"Error expected HCT2 :%h actual :%h time: %d",
  		        	act_hcount[10:1], hct2, $time);
	  end


	if (int3_detected)
	 begin
		$fdisplay(vi_trg_int_dump_fd,"INT3 detected!!!");
		if (act_vcount != vct3)
     			$fdisplay(vi_trg_int_dump_fd,"Error expected VCT3 :%h actual :%h time: %d",
  		        	act_vcount, vct3, $time);
		if (act_hcount[10:1] != hct3)
     			$fdisplay(vi_trg_int_dump_fd,"Error expected HCT3 :%h actual :%h time: %d",
  		        	 act_hcount[10:1], hct3, $time);
	  end


// General Test
	if (start_chk && vstart)
	begin
	// there is a one clk diff between monitor and code
	// The vcount wont properly match until the 2nd frame because VI starts out on
	// Serration and not on Pre Equalization
		if (act_vcount != ypos_d1)
     			$fdisplay(vi_trg_int_dump_fd,"Error ACTUAL VCT :%h  CLAIMED VCT :%h time: %d",
  		        	act_vcount, ypos, $time);
		if (((act_hcount +1) >>1) != xpos_d1)
    			$fdisplay(vi_trg_int_dump_fd,"Error ACTUAL HCT :%h CLAIMED HCT :%h time: %d",
  			        ((act_hcount +1) >>1), xpos_d1, $time);
	end


end	

endmodule
