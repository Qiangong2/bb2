`define ECore system.flipper.flipper_core

`define DI system.flipper.flipper_core.io0.io_di0
`define DIDCTL `DI.iodidctl
`define	DIMCTL `DI.iodimctl
`define	DIPCTL `DI.iodipctl

`define CounterReset cntr_reset
`define DumpStats system.pending_finish
`define EndOfTest system.pending_finish
`define DumpFile "diag/iodi_stats.log"

`define MEMWR0 	  	4'b0100
`define MEMRDWAIT 	4'b1001

module monitor_di ();
   
    integer 
	clk_count,
	brk_count,
	err_count,
	rst_count,
	brkmembusy_count,
	errmembusy_count,
	rstmembusy_count,
     	iodi_dump_fd,
     	dump_output;

    integer
	number_mem_wait,
	max_mem_latency,
	total_mem_latency,
	curr_mem_latency;

    reg [8*256:1] 
	iodi_dump;

    reg
	memwait;	// Waiting for Mem ack

/* --------------------------------------------------------------------	*/
/* Open and close output file						*/
/* --------------------------------------------------------------------	*/

    initial
      begin
	clk_count = 0;
	brk_count = 0;
	err_count = 0;
	rst_count = 0;
	brkmembusy_count = 0;
	errmembusy_count = 0;
	rstmembusy_count = 0;
	number_mem_wait = 0;
	max_mem_latency   = 0;
	total_mem_latency = 0;
	curr_mem_latency  = 0;
	if ($test$plusargs("no_iodi_dump"))
	    dump_output = 0;
	else
	  begin
	     dump_output = 1;	     
	     if ($GetCLStr("iodi_dump=", iodi_dump) == 1)
		   iodi_dump_fd = $fopen(iodi_dump);
	     else
		   iodi_dump_fd = $fopen(`DumpFile);
	  end 
      end

   
   

/* --------------------------------------------------------------------	*/
/* Monitor Break, Error, Reset and Memory Busy Condition		*/
/* Monitor Max memory latency for DI					*/
/* --------------------------------------------------------------------	*/

    always @(posedge `DI.io_clk)
      begin
        if (dump_output)
          begin
            if (|`DIDCTL.p2d_break)
	      begin
	        // Memory Write busy and break
		if (`DIMCTL.cstate == `MEMWR0)
		  begin
              	    $fdisplay(iodi_dump_fd,"Mem WR Busy and break # %d", $time());
		    brkmembusy_count = brkmembusy_count + 1;
		  end
	        // Memory Read busy and break
		if (`DIMCTL.cstate == `MEMRDWAIT)
		  begin
              	    $fdisplay(iodi_dump_fd,"Mem RD Busy and break # %d", $time());
		    brkmembusy_count = brkmembusy_count + 1;
		  end
		brk_count = brk_count + 1;
	      end


	    if (|`DIDCTL.p2drstbfall)
	      begin
		// Memory Write and Software reset
		if (`DIMCTL.cstate == `MEMWR0)
		  begin
                    $fdisplay(iodi_dump_fd,"Mem WR Busy and swrst # %d", $time());
		    rstmembusy_count = rstmembusy_count + 1;
		  end
		// Memory Read and Software reset
		if (`DIMCTL.cstate == `MEMRDWAIT)
		  begin
              	    $fdisplay(iodi_dump_fd,"Mem RD Busy and swrst # %d", $time());
		    rstmembusy_count = rstmembusy_count + 1;
		  end
		rst_count = rst_count + 1;
	      end


	    if (|`DIDCTL.dierrfall)
	      begin
		// Memory Write and Error
		if (`DIMCTL.cstate == `MEMWR0)
		  begin
            	    $fdisplay(iodi_dump_fd,"Mem WR Busy and error # %d", $time());
		    errmembusy_count = errmembusy_count + 1;
		  end
		// Memory Read and Error
		if (`DIMCTL.cstate == `MEMRDWAIT)
		  begin
              	    $fdisplay(iodi_dump_fd,"Mem RD Busy and error # %d", $time());
		    errmembusy_count = errmembusy_count + 1;
		  end
		err_count = err_count + 1;
	      end

	    
	    curr_mem_latency = memwait ? curr_mem_latency + 1 : 0;
		
	    if (`DIMCTL.DiMemAck & memwait)
	      begin
		max_mem_latency = (max_mem_latency < curr_mem_latency) ?
			 	  curr_mem_latency : max_mem_latency;
	        total_mem_latency = total_mem_latency + curr_mem_latency;
		number_mem_wait   = number_mem_wait + 1;
	      end

          end

        if(((clk_count%1000)==0) && dump_output)
            $fflush(iodi_dump_fd);

        clk_count = clk_count + 1;
      end

    always @(posedge `DI.io_clk)
      if (~`DI.resetb)
	begin
	  memwait = 1'b0;
	end
      else
        begin
	  memwait = memwait ? ~`DIMCTL.DiMemAck : `DIMCTL.DiMemReq;
        end

    always @(posedge `DumpStats)
      begin
	$fdisplay(iodi_dump_fd, "%d no of break and mem busy, with total break %d ", brkmembusy_count, brk_count);
	$fdisplay(iodi_dump_fd, "%d no of swrst and mem busy, with total swrst %d ", rstmembusy_count, rst_count);
	$fdisplay(iodi_dump_fd, "%d no of error and mem busy, with total error %d ", errmembusy_count, err_count);

	$fdisplay(iodi_dump_fd, " Max Memory Latency %d ", max_mem_latency);
	$fdisplay(iodi_dump_fd, " Average Memory Latency %d with %d requests",
	    (total_mem_latency / number_mem_wait), number_mem_wait);
      end

endmodule // monitor_di

