/*****************************************************************************
 * 
 *   (C) 1999 ARTX INC..  ALL RIGHTS RESERVED.  UNPUBLISHED -- RIGHTS
 *   RESERVED UNDER THE COPYRIGHT LAWS OF THE UNITED STATES.  USE OF A
 *   COPYRIGHT NOTICE IS PRECAUTIONARY ONLY AND DOES NOT IMPLY PUBLICATION
 *   OR DISCLOSURE.
 *
 *   THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF
 *   ARTX INC..  USE, DISCLOSURE, OR REPRODUCTION IS PROHIBITED WITHOUT
 *   THE PRIOR EXPRESS WRITTEN PERMISSION OF ARTX INC..
 *
 *                   RESTRICTED RIGHTS LEGEND
 *
 *   Use, duplication, or disclosure by the Government is subject to
 *   restrictions as set forth in subparagraph (c)(1)(ii) of the Rights
 *   in Technical Data and Computer Software clause at DFARS 252.227-7013
 *   or subparagraphs (c)(1) and (2) of Commercial Computer Software --
 *   Restricted Rights at 48 CFR 52.227-19, as applicable. 
 *
 *   ArtX Inc.
 *   3400 Hillview Ave, Bldg 5
 *   Palo Alto, CA 94304
 *
 ****************************************************************************/

// syn myclk = io_clk

module di_mem (/*AUTOARG*/
    // Output to DI
    MemData, DiMemAck, MemFlushAck,

    // Input from DI
    DiMemData, DiMemAddr, DiMemReq, DiMemRd, DiMemFlush,

    // General Input
    io_clk, resetb

);

    /*
     * Input, Output, Inout Ports declaration
     */
    // Output to DI
    output [63:0]
	MemData;		// Data from Mem

    output
	DiMemAck;		// Ack from Mem

    output
	MemFlushAck;		// Flush Ack from Mem

    // Input from DI
    input [63:0]
	DiMemData;		// Data to Mem

    input [25:5]
	DiMemAddr;		// Address to Mem

    input
	DiMemReq;		// Request for 32B transfer

    input
	DiMemRd;		// Request for data read, 0 for write

    input
	DiMemFlush;		// Request write flush at the end
				// of the memory transfer

    // General inputs
    input
	io_clk;			// Clock

    input
	resetb;			// Reset, low active



   /*
    * Wires, Registers declaration
    */
    parameter
	noofentries = 8192,
	memwidth    = 64;

    reg [32*8:1] mem_initfile;
    reg [32*8:1] mem_dumpfile;

    reg [memwidth-1:0]
	memarray[0:noofentries-1];

    reg [63:0]
	MemData;

    reg
	MemFlushAck, flush;

    reg
	memwr1d, memwr2d, memwr3d, memwr4d,
	memrd1d, memrd2d, memrd3d, memrd4d;

    reg [22:0]
	memaddr;

    /*
     * Initialization
     */
    initial
      begin
	mem_initfile = "dimem.init";
	mem_dumpfile = "dimem.dump";
	$readmemh( mem_initfile, memarray );
	#24000
	$writememh( mem_dumpfile, memarray );
      end

    /*
     * Assignments
     */
    assign
	DiMemAck = memwr1d | memwr2d | memwr3d | memwr4d |
		   memrd1d | memrd2d | memrd3d | memrd4d;

    /*
     * Always Block
     */
    always @(posedge io_clk)
      if (~resetb)
        begin
	  memaddr <= 23'h0;
	  memwr1d <= 1'b0;
	  memwr2d <= 1'b0;
	  memwr3d <= 1'b0;
	  memwr4d <= 1'b0;
	  memrd1d <= 1'b0;
	  memrd2d <= 1'b0;
	  memrd3d <= 1'b0;
	  memrd4d <= 1'b0;
	  flush   <= 1'b0;
	  MemFlushAck <= 1'b0;
        end
      else
	begin
	  memaddr <= DiMemReq & ~DiMemAck ? (DiMemAddr[25:5] << 2) : memaddr;
	  memwr1d <= DiMemReq & ~DiMemRd & ~DiMemAck;
	  memwr2d <= memwr1d;
	  memwr3d <= memwr2d;
	  memwr4d <= memwr3d;
	  memrd1d <= DiMemReq & DiMemRd & ~DiMemAck;
	  memrd2d <= memrd1d;
	  memrd3d <= memrd2d;
	  memrd4d <= memrd3d;
	  flush   <= flush ? ~memwr4d : DiMemFlush;
	  MemFlushAck <= memwr4d & flush;
	end

    always @(posedge io_clk)
      begin
	if (memwr1d)
	  memarray[memaddr] = DiMemData;
	else if (memwr2d)
	  memarray[memaddr+1] = DiMemData;
	else if (memwr3d)
	  memarray[memaddr+2] = DiMemData;
	else if (memwr4d)
	  memarray[memaddr+3] = DiMemData;
      end

    always @(memrd1d or memrd2d or memrd3d or memrd4d)
      begin
	if (memrd1d)
	  MemData = memarray[memaddr];
	else if (memrd2d)
	  MemData = memarray[memaddr+1];
	else if (memrd3d)
	  MemData = memarray[memaddr+2];
	else if (memrd4d)
	  MemData = memarray[memaddr+3];
      end

endmodule // di_mem

