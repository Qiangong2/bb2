/* Copyright 1999 by Matsusita Electric  Industrial Co., Ltd.
 *                                                   M.Kawakubo
 * Module name: hstif_sndrsp
 * Function of this module: device of DIbus
 *
 */

module di_dvdmei( dd, dir, hstrb, dstrb, err, reset, brk, cover );

    inout [7:0] dd;
    input       dir;
    input       hstrb;
    output      dstrb;
    output      err;
    input       reset;
    inout       brk;
    output      cover;

    reg     err;
    reg     dstrb;

    reg [31:0]  w_time;
    reg     [7:0]   dd_reg;
    reg     brk_reg;
    reg     err_reserved;
    integer     count,capa;

    /*
     * wwy add begin
     */
    integer dvdlog;

    initial dvdlog = $fopen("diag/dvd.log");

    //`define `unittime 100
    //`define unittime 9
    `define unittime 1

    `define sim_log dvdlog

    `include "../sys/di/di_dvdcontrol.v"


    /*
     * wwy add end
     */
    assign      brk = brk_reg;
    assign      dd  = dir ? dd_reg : 8'hzz;

    initial     err_reserved = 0;

    always @( negedge reset ) begin

        err      = 1;
        dstrb        = 1;
        brk_reg      = 1'bz;
        dd_reg       = 8'bz;
    end

    task receive_command;

        input [32*8:1]  command_filename;
        reg   [7:0]     command_MEM[0:11];
    integer     i,diff,buff[11:0];

    begin
        $fdisplay( `sim_log,$stime," task receive_command  START" );

        wait( dir==0 );
        #(20 * `unittime);

                i     = 0;
        dstrb = 0;

        while( i<=11 ) begin

            @(posedge hstrb);
            buff[i] =dd;

            if( i==8 ) dstrb = 1;
            
            i = i+1;
        end

        if( err_reserved==0 ) #(40*`unittime) err = 1;
        else                  #(40*`unittime) err = 0;

        err_reserved = 0;
                    
        i    = 0;
        diff = 0;

        $readmemh( command_filename ,command_MEM );

        while( i<=11 ) begin

            diff = diff | !( buff[i] == command_MEM[i] );

            i = i+1;
        end

        if( diff==0 ) $fdisplay( `sim_log,$stime," task receive_command  OK" );
        else          $fdisplay( `sim_log,$stime," task receive_command  NG" );

        $fdisplay( `sim_log,$stime," task receive_command  END" );
    end
    
    endtask

    task send_response;

    input [32*8:1]  response_filename;
    input [31:0]    start_add;
    input [31:0]    end_add;
    reg   [7:0]     response_MEM[1024:0];
    integer     i,path;
    
    begin
        $fdisplay( `sim_log,$stime," task send_response    START" );

        $readmemh( response_filename ,response_MEM );
        capa = 1 + end_add - start_add;

        if( 0!=capa%32 ) begin

            if( 4!=capa ) begin

                $fdisplay( `sim_log,"byte err" );
            end
        end

        i      = start_add;
        path   = 0;
        w_time = 0;

        wait( dir==1 & hstrb==0 );
        #(20*`unittime);

        while( i<=end_add ) begin

            dstrb  = 0;
            dd_reg = response_MEM[i];       
            #(20*`unittime);
            dstrb  = 1;
                #(20*`unittime);

            i = i+1;

            if( w_time!=0 ) #w_time w_time = 0;

                        dstrb  = 0;
                        dd_reg = response_MEM[i];        
            #(20*`unittime);
                        dstrb  = 1;                       
            #(20*`unittime);

                    i = i+1;

            if( w_time!=0 ) #w_time w_time = 0;

            if( path==1 ) begin

                dstrb = 1;
                @( negedge hstrb ); 
                dstrb = 0;
                path  = 0;
            end

            if( hstrb==1 & i<=(end_add-3) ) path = 1;
        end

                dd_reg = 8'hzz;
                #(40*`unittime);

        if( err_reserved==1 ) err = 0;
        
        err_reserved = 0;
        
        $fdisplay( `sim_log,$stime," task send_response    END" );
    end
    endtask

    task send_wait;

    input   [31:0]  start_count;
    input   [31:0]  waiting_time;
    integer     count;

    begin
        $fdisplay( `sim_log,$stime," task send_wait        START" );

        /*wwy count = 0; */
        count = send_response.start_add;

        while( count<=send_response.end_add ) begin

            @( posedge dstrb );

            if( count==start_count ) w_time = waiting_time;

            count =count+1;
        end
        $fdisplay( `sim_log,$stime," task send_wait        END" );
    end
    endtask

    /*wwy always @( posedge brk ) begin
     */
    always @( posedge brk ) 
    if (reset)
    begin
	#100
        disable send_response;
 	/* wwy start */
        disable receive_command;
 	/* wwy end */
        dstrb   = 1;
        #(400*`unittime);
        brk_reg = 0;
        #(1000*`unittime);
        brk_reg = 1'bz;

        @( negedge brk );
    end

    task reserve_err;

    begin
        $fdisplay( `sim_log,$stime," task reserve_err      START" );
        err_reserved =1;
                $fdisplay( `sim_log,$stime," task reserve_err      OK" );   
    end
    endtask
endmodule

