/*******************************************************************************
 * 
 *   (C) 1999 ARTX INC..  ALL RIGHTS RESERVED.  UNPUBLISHED -- RIGHTS
 *   RESERVED UNDER THE COPYRIGHT LAWS OF THE UNITED STATES.  USE OF A
 *   COPYRIGHT NOTICE IS PRECAUTIONARY ONLY AND DOES NOT IMPLY PUBLICATION
 *   OR DISCLOSURE.
 *
 *   THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF
 *   ARTX INC..  USE, DISCLOSURE, OR REPRODUCTION IS PROHIBITED WITHOUT
 *   THE PRIOR EXPRESS WRITTEN PERMISSION OF ARTX INC..
 *
 *                   RESTRICTED RIGHTS LEGEND
 *
 *   Use, duplication, or disclosure by the Government is subject to
 *   restrictions as set forth in subparagraph (c)(1)(ii) of the Rights
 *   in Technical Data and Computer Software clause at DFARS 252.227-7013
 *   or subparagraphs (c)(1) and (2) of Commercial Computer Software --
 *   Restricted Rights at 48 CFR 52.227-19, as applicable. 
 *
 *   ArtX Inc.
 *   3400 Hillview Ave, Bldg 5
 *   Palo Alto, CA 94304
 *
 ******************************************************************************/


#ifndef __DIAG_DIDEV_H__
#define __DIAG_DIDEV_H__

#define PKT_LEN         12
#define DI_MIN_DMA	32

#define DI_SPD_27NS	1
#define DI_SPD_135NS	2

enum diCmds_e {
    di_read, di_write, di_irq, di_door, di_noop, di_brklen,
    di_speed, di_status, di_times, di_getDelays, di_setDelays, 
    di_doorClk, di_randDly
};

#define DI_STAT_NOERR		1
#define DI_STAT_WRTERR		2

struct diPkt_s {
    union {
        u8              buf[PKT_LEN];
        u32             b32[PKT_LEN/4];
    } u;
};
typedef struct diPkt_s DiPkt;
#define _cmd    u.buf[0]
#define _err    u.buf[1]
#define _speed  u.buf[2]
#define _rand   u.buf[2]
#define _mask   u.buf[2]
#define _ramp   u.buf[2]
#define _len    u.b32[1]
#define _open   u.b32[1]
#define _close  u.b32[2]
#define _b32    u.b32

/*  These are structures too large for the packet... */
struct devTimes_s {
    u32 xferTime;       /*  time of total xfer in ns */
    u32 pktTime;        /*  time of pkt xfer in ns */
    u32 brstTime;       /*  time of brst xfer in ns */
};

struct devDlys_s {
    u32 strbRatio;      /*  # of clocks per SEND strobe */
    u32 pktDelay;       /*  clocks between packets */
    u32 brstDelay;      /*  clocks between bursts */
    u32 brstDis;        /*  where to send disable */
    u32 brkDelay;       /*  How lown to hold brk low */
    u32 brkRespDelay;   /*  How long to wait before ack */
    u32 errDelay;
};

struct devBrst_s {
    union {
        struct devTimes_s times;
        struct devDlys_s  delays;
        u8                buf[DI_MIN_DMA];
        u32               b32[DI_MIN_DMA/4];
    }u;
};
typedef struct devBrst_s DevBrst;
#define _times  u.times
#define _delays u.delays

#endif /*  __DIAG_DIDEV_H__ */
