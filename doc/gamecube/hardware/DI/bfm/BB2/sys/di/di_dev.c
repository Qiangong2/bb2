/*  DI Device */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>

#include "types.h"
#include "diag/didev.h"

#include "acc_user.h"
#include "vcsuser.h"

/*  #define NSY             1 */
/*  #define EXNSY	   1 */

#define DIMON	1

#if DIMON
#include <sys/types.h>
#include <sys/resource.h>
#include <fcntl.h>
#include <errno.h>
#endif /*  DIMON */

#define RANDOM(x)       (random() % (x))
#define RAND(x)         (RANDOM(100) < (x))

#define DBRST_LEN       32

#define MAX_IMM_LEN	4

#define PKT_DLY         4       /*  number of internal clocks before reenable */
#define BRST_DLY        2       /*  number of internal clocks before reenable */
#define BRK_DLY         7       /*  delay to drive brk low */
#define BRK_RESP_DLY    7
#define ERR_DLY         2

static u32 strbRatio = DI_SPD_135NS;    /*  # of clocks per SEND strobe */
static u32 pktDelay  = PKT_DLY;         /*  clocks between packets */
static u32 brstDelay = BRST_DLY;        /*  clocks between bursts */
static u32 brkDelay  = BRK_DLY;         /*  How lown to hold brk low */
static u32 brkRespDelay = BRK_RESP_DLY; /*  How long to wait before ack */
static u32 errDelay  = ERR_DLY;

enum mode_e {
    m_getCmd, m_send, m_recv, m_brk0, m_brk1, m_err
};

struct dev_s {
    u32     state;
#define D_RESET         0x0001          /*  being reset */
#define D_WAITDIRLOW    0x0002          /*  Wait for Dir Low */
#define D_ERROR         0x0004          /*  Handling an error */
#define D_WAITHOSTLOW	0x0008		/*  Wait for host low */
#define D_STRBDLYNEG    0x0020          /*  wait between dev enable */
#define D_STRBDLYPOS    0x0040          /*  wait between dev enable */
#define D_IRQSMSKD      0x0080          /*  do not drive any irqs */
#define D_ERROR2        0x0100          /*  second stage of the error seq */
#define D_HSTPAUSE      0x0200          /*  Host requests a pause in xmit */
#define D_HSTPAUSED     0x0400          /*  We have paused */
#define D_GENERR        0x0800          /*  this command gens an error */
#define D_INTBUF        0x1000          /*  use internal buffer */
#define D_DOORCLK       0x2000          /*  door command in clocked vs. byte */
#define D_ERROR3        0x4000          /*  3rd state of error seq */
#define D_WAITDIRHI     0x8000          /*  Wait for Dir High */
#define D_RAND	        0x10000		/*  produce random delays */
#define D_BROKE	        0x20000		/*  Had a BRK */
#define D_RANDDLY       0x40000         /*  In the middle of a pause */
#define D_RECVPAUSE     0x80000
#define D_BRKSAVE       (D_RAND | D_IRQSMSKD | D_DOORCLK)    
#define D_CMDSAVE       (D_BRKSAVE | D_ERROR2 | D_ERROR3)
    u32     mode;
    u32     errors;
#define D_ERR_RECV      0x0001          /*  recv'd a bad bit */
    u32     clk;
    u32     strbDly;
    u32     pause;                      /*  byte delay of pause */
    u32     cmdCnt;

    /*  delays */
    u32     hNegCnt;
    u32     hPosCnt;
    u32     recvPause;

    /*  door control */
    u32     doorOpen;                   /*  when to open the door */
    u32     doorClose;                  /*  when to close the door */
    u32     closeCmd;

    /*  DMA info */
    u32     remainder;			/*  Amount not xfer'd on last transaction */
    u32     dmaLen;                     /*  DMA size */
    u32     ramp;                       /*  start value of the data ramp */
    u32     errDly;			/*  when and if to cause an error */

    /*  IMM buffer */
    u32	    immData;

    /*  time information */
    u32     pktStart;
    u32     pktEnd;
    u32     prevPktStart;
    u32     prevPktEnd;
    u32     prevBrstEnd;
    u32     brstStart;
    u32     hostNeg;
    u32     hostPos;
    float   goal;

    /*  packet/burst info */
    u32     curByte;
    DiPkt   pkt;
    DevBrst brst;
};
typedef struct dev_s Dev;

Dev dev;

#define POSEDGE(p)      (tf_getp(p))
#define NEGEDGE(p)      (tf_getp(p) == 0)

enum parms_e {
    p_ddout = 1, p_dstrb, p_cover, p_brkout, p_ddoe, p_brkoe, p_err,  /*  output */
    p_ddin, p_brkin, p_dir, p_hstrb, p_rst, p_clock                   /*  input */
};

#if NSY
static char *pStrs[] = {
    "", "ddout", "dstrb", "cover", "brkout", "ddoe", "brkoe", "err",
    "ddin", "brkin", "dir", "hstrb", "rst", "clock"
};

static char *cmdStrs[] = {
    "read", "write", "irq", "door", "noop", "brklen", "speed", 
    "status", "times", "getDelays", "setDelays", "doorClk"
};
#define MAX_CMD (sizeof(cmdStrs)/sizeof(cmdStrs[0]))
#endif /*  NSY */

#define P_MAX p_clock

static u32 pTypes[P_MAX+1] = {
    0, 
    tf_readwrite, tf_readwrite, tf_readwrite, tf_readwrite, tf_readwrite,
        tf_readwrite, tf_readwrite,
    tf_readonly, tf_readonly, tf_readonly, tf_readonly, tf_readonly, tf_readwrite
};

static int clkPer, startPer, clkCnt;

#if DIMON
enum sigs_e {
    bs_dir, bs_hstrb, bs_dstrb, bs_maxsig
};
enum sigvals_e {
    bs_lo, bs_hi, bs_xx, bs_maxval
};

static char *dirStr[2] = {
     "write", "read"
};

static char *sigStr[bs_maxsig] = {
    "DIR", "HSTRB", "DSTRB"
};

#define MAX_BRKSTATE    4

static u32 bsParms[bs_maxsig] = {
    p_dir, p_hstrb, p_dstrb
};

static u32 bsVals[2][MAX_BRKSTATE][bs_maxsig] = {
    {   /*  write */
        /*  dir, hstrb, dstrb */
        {bs_lo, bs_xx, bs_xx},  /*  har, har */
        {bs_lo, bs_hi, bs_hi},
        {bs_lo, bs_hi, bs_hi},
        {bs_lo, bs_hi, bs_hi}
    },
    {   /*  read */
        /*  dir, hstrb, dstrb */
        {bs_hi, bs_xx, bs_xx}, 
        {bs_hi, bs_xx, bs_hi}, /*  hstrb needs to be what it had been previously */
        {bs_hi, bs_hi, bs_hi},
        {bs_lo, bs_hi, bs_hi}
    }
};

struct diMon_s {
    FILE *fp;
    u32   reset;
    u32   brk;
    u32   brkDir;
    u32   brkState;
    u32   err;
    u32   hstrb;
    u32   dstrb;
    u32   change;
};
typedef struct diMon_s DiMon;

static DiMon mon;

static FILE *
hiOpen(const char *path)
{
    int fd;
    int f = -1;
    int i;
    int maxFd;
    struct rlimit rl;

    if (getrlimit(RLIMIT_NOFILE, &rl)) {
        io_printf("unable to get max fd\n");
        maxFd = 32;
    }
    else {
        maxFd = rl.rlim_cur;
    }
/*     io_printf("DI Monitor: max fd is %d\n", maxFd); */

    /*  Find an unsed fd above 32 */
    for( i = 32 ; ; i++ ) {
        /*  run out of fd's??? */
        if ( i >= maxFd ) {
            io_printf("DI monitor unable to open fd above 32.\n");
            goto fail;
        }

        if ( (fd = dup(i)) < 0 ) {
            if ( errno == EBADF ) {
                break;
            }
        }
        close(fd);
    }
    fd = i;

    f = open(path, O_WRONLY | O_CREAT, 0644);
    if ( f < 0 ) {
        io_printf("dimon unable to open %s\n", path);
        goto fail;
    }
/*     io_printf("dimon duping %d:%d\n", f, fd); */
    if ( dup2(f, fd) < 0 ) {
        io_printf("dimon unable to dup %d:%d\n", f, fd);
        goto fail;
    }

    /*  close the original */
    close(f);
    return fdopen(fd, "w");

fail:
    if ( f >= 0 ) {
        close(f);
    }
    return fopen(path, "w");
}
#endif /*  DIMON */

int
check_didev()
{
    int i;
    int err = 0;

    /*  allow access to the lines */
    acc_initialize();

    if ( tf_nump() != P_MAX ) {
        tf_error("$didev: parameter count mismatch %d,%d\n", tf_nump(),
                 P_MAX);
        tf_dofinish();
    }

    /*  check the output parm types */
    for( i = 1 ; i <= P_MAX ; i++ ) {
        if ( tf_typep(i) != pTypes[i] ) {
            tf_error("$didev: argument type mismatch arg %d: got %d,%d\n", i,
                    tf_typep(i), pTypes[i]);
	    err++;
        }
    }

    if (err) {
        tf_dofinish();
    }

    /*  sanity checks */
    assert(sizeof(DiPkt) == PKT_LEN);

#if  DIMON
    /*  open the log file */
    mon.fp = hiOpen("diag/di.tr");
    if ( mon.fp == NULL ) {
        tf_error("didev: unable to open monitor file\n");
        tf_dofinish();
    }
#endif /*  DIMON */

    return 0;
}

static void
devGetCmd(void)
{
    u32   prevLen;
    u32   dur;
    float spd;
    float goal;

    /*  performance information */
    prevLen = dev.dmaLen;

    /*  collect perf stats */
    dev.prevPktStart = dev.pktStart;
    dev.prevPktEnd   = dev.pktEnd;
    dev.prevBrstEnd  = tf_gettime();

    /*  check the numbers */
    if ( !(dev.state & (D_RANDDLY | D_BROKE)) && 
         (dev.dmaLen == dev.curByte) &&
	 (clkPer == 74) &&
         (prevLen >= 32) ) {
	dur = dev.prevBrstEnd - dev.brstStart;
	spd = (float)prevLen * (1000000000.0/1048576) / (float)dur;
#if NSY
	io_printf("DI spd = %.2f Mb/sec\n", spd);
#endif /*  NSY */
        goal = dev.goal;
	if ( (spd < (goal - (goal / 10.0))) || 
	     (spd > (goal + (goal / 10.0))) ) {
	    io_printf("SIM00: FAIL spd %.2f, goal %.2f @ %d\n", spd, goal,
		tf_gettime());
	}
    }

    /*  start ready for a command with the door closed */
    dev.state  &= D_CMDSAVE;
    dev.state  |= D_WAITDIRLOW | D_STRBDLYNEG;
    dev.mode    = m_getCmd;
    dev.strbDly = strbRatio;
    dev.remainder = dev.dmaLen - dev.curByte;
    dev.dmaLen  = 0;
    dev.curByte = 0;

    /*  turn on random delays for this transaction */
    if ( (dev.state & D_RAND) && (RANDOM(100) < 15) ) {
        dev.state |= D_RANDDLY;
    }
}


static void
devReset(int reset)
{
    if ( reset ) {
        if ( dev.state & D_RESET ) {
            return;
        }
#if NSY
        io_printf("didev: reset @ %d\n", tf_gettime());
#endif /*  NSY */
        tf_putp(p_dstrb, 1);
        tf_putp(p_cover, 0);
        tf_putp(p_ddoe, 0);
        tf_putp(p_brkoe, 0);
        tf_putp(p_err, 1);

        /*  clear all of memory */
        memset(&dev, 0, sizeof(dev));
        dev.state = D_RESET;

        return;
    }

#if NSY
    io_printf("didev: reset cleared @ %d\n", tf_gettime());
#endif /*  NSY */

    devGetCmd();
}

int
call_didev()
{
    /*  be advised when parameters change */
    tf_asynchon();

    devReset(1);

    return 0;
}

static void
setDelays(void)
{
    strbRatio    = dev.brst._delays.strbRatio;
    pktDelay     = dev.brst._delays.pktDelay;
    brstDelay    = dev.brst._delays.brstDelay;
    /*  brstDis      = dev.brst._delays.brstDis; */
    brkDelay     = dev.brst._delays.brkDelay;
    brkRespDelay = dev.brst._delays.brkRespDelay;
    errDelay     = dev.brst._delays.errDelay;
}

static void
getDelays(void)
{
    memset(&dev.brst, 0, DI_MIN_DMA);
    dev.brst._delays.strbRatio    = strbRatio;
    dev.brst._delays.pktDelay     = pktDelay;
    dev.brst._delays.brstDelay    = brstDelay;
    dev.brst._delays.brstDis      = 0;
    dev.brst._delays.brkDelay     = brkDelay;
    dev.brst._delays.brkRespDelay = brkRespDelay;
    dev.brst._delays.errDelay     = errDelay;
}

static void
getTimes(void)
{
    memset(&dev.brst, 0, DI_MIN_DMA);
    dev.brst._times.xferTime = dev.prevBrstEnd - dev.prevPktStart;
    dev.brst._times.pktTime  = dev.prevPktEnd  - dev.prevPktStart;
    dev.brst._times.brstTime = dev.prevBrstEnd - dev.prevPktEnd;
}

static void
forceErr(void)
{
#if NSY
    io_printf("didev: force err @ %d\n", tf_gettime());
#endif /*  NSY */
    dev.mode = m_err;
    dev.state |= D_STRBDLYNEG | D_ERROR;
    dev.state &= ~D_GENERR;
    dev.strbDly = errDelay;
}

static u32
testErr(void)
{
    if ( !(dev.state & D_GENERR) ) {
        return 0;
    }

    /*  generate the error at the end or in the middle */
    if ( dev.curByte < dev.dmaLen ) {
	dev.errDly--;
	if (dev.errDly ) {
	    return 0;
	}
    }

    forceErr();
    return 1;
}

static void
doDoor(void)
{
    if ( dev.doorOpen ) {
        dev.doorOpen--;
        if ( dev.doorOpen == 0 ) {
#if NSY
            io_printf("didev: door open @ %d\n", tf_gettime());
#endif /*  NSY */
            tf_putp(p_cover, 1);
            dev.closeCmd = dev.cmdCnt + 2;
        }
    }
    else if ( dev.doorClose && (dev.closeCmd <= dev.cmdCnt) ) {
        dev.doorClose--;
        if ( dev.doorClose == 0 ) {
#if NSY
            io_printf("didev: door close @ %d\n", tf_gettime());
#endif /*  NSY */
            tf_putp(p_cover, 0);
        }
    }
}

static void
doCover(void)
{
    dev.clk++;

    /*  Handle the door */
    /*  NOTE: The state of the door has no effect on the ability */
    /*  to send/recv data... */
    if ( !(dev.state & D_DOORCLK) ) {
        return;
    }

    doDoor();
}

static void
doDevPos(void)
{
    dev.state &= ~D_STRBDLYPOS;
    tf_putp(p_dstrb, 1);

    if ( dev.mode == m_send ) {
        /*  set up the next low strobe/data change */
        dev.state |= D_STRBDLYNEG;

        /*  check for a pause */
        if ( (dev.curByte & 1) && tf_getp(p_hstrb) ) {
            dev.state |= D_HSTPAUSE;
        }

        /*  Are we done? */
        if ( dev.curByte == dev.dmaLen ) {
	    if (testErr()) {
	        return;
            }

            /*  go back to packet command mode */
            devGetCmd();

            /*  XXX should track delay between hosts next packet */
            return;
        }

	if (dev.state & D_HSTPAUSED) {
            dev.state |= D_WAITHOSTLOW;
	}
        dev.strbDly = strbRatio;
        if ( (dev.state & D_RANDDLY) && (RANDOM(100) < 15) &&
             !(dev.state & D_GENERR) ) {
            dev.strbDly += (RANDOM(3) + 4) * strbRatio;
        }
        return;
    }

    /*  handle breaks */
    if ( dev.mode == m_brk0 ) {
        tf_putp(p_brkout, 0);
        tf_putp(p_brkoe, 1);
        dev.state |= D_STRBDLYNEG;
        dev.strbDly = brkDelay;
        return;
    }
}

static void
doDevNeg(void)
{
    static int dhCnt = 0;
    static int ddCnt = 0;
    static int ddhCnt = 0;

    if ( (dev.mode != m_send) && (tf_getp(p_ddoe)) ) {
        /*  stop driving data */
        tf_putp(p_ddoe, 0);
        tf_strdelputp(p_ddout, 8, 'b', "zzzzzzzz", 0, 0);
    }

    if ( dev.mode == m_send ) {
        /*  Must support errors on 32byte boundaries */
	if (testErr()) {
	    return;
        }
    }


    /*  sends have to wait for the host to go low */
    if ( dev.state & D_WAITHOSTLOW ) {
        if ( dev.state & D_HSTPAUSED ) {
            tf_putp(p_ddoe, 0);
            tf_strdelputp(p_ddout, 8, 'b', "zzzzzzzz", 0, 0);
        }
        /*  If not low, try again on the next edge */
        if ( tf_getp(p_hstrb) ) {
#if EXNSY
            io_printf("DI: doDevNeg: DSTRB Lo delayed by HSTRB\n");
#endif /*  EXNSY */
            dev.strbDly++;
            dhCnt++;
            if (dhCnt == 10) {
                io_printf("SIM00: FAIL HSTRB delay overrun @ %d\n", tf_gettime());
            }
            return;
        }
        dhCnt = 0;
        dev.state &= ~D_WAITHOSTLOW;
        /*  If we were paused, then we need to delay one more host strobe */
        if ( dev.state & D_HSTPAUSED ) {
            dev.state &= ~D_HSTPAUSED;
            dev.strbDly = DI_SPD_27NS * 2;
            return;
        }
    }
    /*  sends have to wait for the direction to go high */
    if ( dev.state & D_WAITDIRHI ) {
        /*  If not hi, try again on the next edge */
        if ( tf_getp(p_dir) != 1) {
#if NSY
            io_printf("DI: doDevNeg: DSTRB Lo delayed by DIR hi\n");
#endif /*  NSY */
            dev.strbDly++;
            ddhCnt++;
            if (ddhCnt == 10) {
                io_printf("SIM00: FAIL DIR HI delay overrun @ %d\n", tf_gettime());
            }
            return;
        }
        ddhCnt = 0;
        dev.state &= ~D_WAITDIRHI;
    }
    /*  Commands have to wait for the direction to go low */
    if ( dev.state & D_WAITDIRLOW ) {
        /*  If not low, try again on the next edge */
        if ( tf_getp(p_dir) ) {
#if NSY
            io_printf("DI: doDevNeg: DSTRB Lo delayed by DIR\n");
#endif /*  NSY */
            dev.strbDly++;
            ddCnt++;
            if (ddCnt == 10) {
                io_printf("SIM00: FAIL DIR delay overrun @ %d\n", tf_gettime());
            }
            return;
        }
        ddCnt = 0;
        dev.state &= ~D_WAITDIRLOW;
    }
    if ( dev.mode == m_err ) {
        if ( tf_getp(p_ddoe) ) {
            /*  stop driving data */
            tf_putp(p_ddoe, 0);
            tf_strdelputp(p_ddout, 8, 'b', "zzzzzzzz", 0, 0);
        }
        devGetCmd();
        dev.state |= D_ERROR2;
        tf_putp(p_err, 0);
#if NSY
        io_printf("didev: gen err @ %d\n", tf_gettime());
#endif /*  NSY */
        return;
    }

    dev.state &= ~D_STRBDLYNEG;

    if ( dev.mode != m_brk0 ) {
        tf_putp(p_dstrb, 0);
    }

    /*  set up pause at the end of the command */
    if ( dev.mode == m_getCmd ) {
        /*  Can safely start counting edges now. */
        dev.hNegCnt = dev.hPosCnt = 0;
        return;
    }

    /*  drive out data */
    if ( dev.mode == m_send ) {
        /*  See if it's time to generate an IRQ */
        if ( !(dev.state & D_DOORCLK) ) {
            doDoor();
        }

        /*  handle pauses from the host */
        if ( dev.state & D_HSTPAUSE ) {
            dev.state &= ~D_HSTPAUSE;
            dev.state |= D_HSTPAUSED;
        }

        /*  assert(tf_getp(p_dir) == 1); */

        /*  Set up the strobe to pos */
        dev.strbDly = strbRatio;
        dev.state |= D_STRBDLYPOS;

        /*  determine the start of the DMA */
        if ( dev.curByte == 0 ) {
            dev.brstStart = tf_gettime();
        }

        /*  clock out data */
	if (dev.dmaLen <= MAX_IMM_LEN) {
	    u8 *bp;

	    bp = (u8 *)&dev.immData;
	    tf_putp(p_ddout, bp[dev.curByte]);
	}
        else if (dev.state & D_INTBUF) {
	    tf_putp(p_ddout, dev.brst.u.buf[dev.curByte]);
        }
	else {
	    tf_putp(p_ddout, dev.ramp++ & 0xff);
	}
        tf_putp(p_ddoe, 1);
        dev.curByte++;

        return;
    }

    /*  finish off a break */
    if ( dev.mode == m_brk0 ) {
        dev.mode   = m_brk1;

        /*  stop driving brk */
        tf_putp(p_brkoe, 0);
        tf_strdelputp(p_brkout, 1, 'b', "z", 0, 0);
        return;
    }
}

static void
doHostNeg()
{
    dev.hostNeg = tf_gettime();
    dev.hNegCnt++;

    if ( dev.mode == m_getCmd ) {
        /*  See if this is a good one... */
        if ( tf_getp(p_dstrb) ) {
            return;
        }

        /*  cause a delay at the end of the command packet */
        /*  for errors */
        if ( dev.hNegCnt == 11 ) {
            tf_putp(p_dstrb, 1);
            return;
        }

        /*  cause a random delay? */
        if ( (dev.state & D_RANDDLY) && (dev.hNegCnt & 1) && RAND(15) ) {
            tf_putp(p_dstrb, 1);
            dev.state    |= D_RECVPAUSE;
            dev.recvPause = dev.hNegCnt + 1;
            return;
        }

        return;
    }

    if ( dev.mode == m_recv ) {
        /*  cause a delay at the end of the command packet */
        /*  for errors */
        if ( (dev.hNegCnt == (dev.dmaLen - 1)) && !(dev.state & D_GENERR) ) {
            tf_putp(p_dstrb, 1);
            return;
        }

        /*  cause a random delay? */
        if ( (dev.hNegCnt & 1) && 
              (((dev.state & D_RANDDLY) && RAND(15)) || 
               ((dev.state & D_GENERR) && (dev.errDly == dev.hNegCnt))) ) {
            tf_putp(p_dstrb, 1);
            dev.state    |= D_RECVPAUSE;
            dev.recvPause = dev.hNegCnt + 1;
            return;
        }

        return;
    }

    /*  can't start a send until the host goes low */
    if ( dev.mode == m_send ) {
        /*  direction had better be high unless we're being reset */
        /*  assert((tf_getp(p_rst) == 0) || (tf_getp(p_dir) == 1)); */
        if ((tf_getp(p_rst) != 0) && (tf_getp(p_dir) != 1)) {
            /*  shit. */
            io_printf("DI: assert 732 hit @ %d!\n", tf_gettime());
            /*  have to wait for DIR to go hi */
            dev.state |= D_WAITDIRHI;
            /*  tf_dofinish(); */
        }

        /*  only do this if we might have been paused... */
        /*  XXX Get's around hstrb blips */
        if ( tf_getp(p_dstrb) == 1 ) {
            dev.state |= D_STRBDLYNEG | D_WAITDIRHI;
            /*  XXX May want to add extra random delay here. */
            /*  XXX What's the min delay? */
            dev.strbDly = brstDelay;
        }
        else {
            io_printf("DI: probable HSTRB pulse @ %d\n", tf_gettime());
        }
        return;
    }
}

#if NSY
static void
dumpPkt(void)
{
    io_printf("pkt %s @ %d\n", (dev.pkt._cmd < MAX_CMD)? cmdStrs[dev.pkt._cmd]:
                "Unknown", tf_gettime());

    io_printf("    0x%x 0x%x 0x%x", dev.pkt._b32[0], dev.pkt._b32[1], 
            dev.pkt._b32[2]);

    if ( dev.pkt._err ) {
        io_printf(" Gen Err @ %d", dev.pkt._err);
    }

    io_printf("\n");
}
#endif /*  NSY */

static void
doCmd(void)
{
#if NSY
    dumpPkt();
#endif /*  NSY */

    /*  reset the clock for delays */
    dev.hNegCnt = dev.hPosCnt = 0;

    /*  Assume we'll stay in command mode. */
    dev.state |= D_STRBDLYNEG;
    dev.strbDly = pktDelay;
    if ( dev.state & D_RANDDLY ) {
        dev.strbDly += (RANDOM(3) + 4) * strbRatio;
    }
    dev.curByte = 0;

    /*  See if this packet will generate an error */
    dev.errDly = dev.pkt._err;
    if ( dev.errDly ) {
        dev.state |= D_GENERR;
        dev.errDly--;
        if ( dev.errDly == 0 ) {
            assert(!(dev.state & (D_ERROR3 | D_ERROR2 | D_ERROR)));
            forceErr();
            return;
        }
    }
    if ( dev.state & D_ERROR2 ) {
        dev.state &= ~D_ERROR2;
        dev.state |= D_ERROR3;
        dev.errDly = errDelay;
    }

    dev.cmdCnt++;

    switch (dev.pkt._cmd) {
    case di_randDly:
        if ( dev.pkt._rand ) {
            dev.state |= D_RANDDLY;
        }
        else {
            dev.state &= ~D_RANDDLY;
        }
        break;
    case di_times:
	dev.state |= D_WAITHOSTLOW | D_INTBUF;
        dev.mode   = m_send;
        dev.dmaLen = DI_MIN_DMA;
        getTimes();
        break;
    case di_getDelays:
	dev.state |= D_WAITHOSTLOW | D_INTBUF;
        dev.mode   = m_send;
        dev.dmaLen = DI_MIN_DMA;
        getDelays();
        break;
    case di_setDelays:
	dev.state |= D_WAITHOSTLOW | D_INTBUF;
        dev.mode   = m_recv;
        dev.dmaLen = DI_MIN_DMA;
        break;
    case di_status:
	/*  This is an immediate mode thingy */
	dev.immData = (dev.errors & D_ERR_RECV) ? DI_STAT_WRTERR : 0;
        dev.errors &= ~D_ERR_RECV;
	dev.dmaLen  = MAX_IMM_LEN;
	dev.state  |= D_WAITHOSTLOW;
	dev.mode    = m_send;
	break;
    case di_speed:
        strbRatio = dev.pkt._speed;
        break;
    case di_brklen:
	/*  This is an immediate mode thingy */
	dev.immData = dev.remainder;
	dev.dmaLen  = MAX_IMM_LEN;
	dev.state  |= D_WAITHOSTLOW;
	dev.mode    = m_send;
	break;
    case di_noop:
        break;
    case di_read:
        /*  Wait until the host tells us he's ready. */
        /*  XXX May want to vary the delays for when we're ready to */
        /*  start sending/receiving */
	dev.state |= D_WAITHOSTLOW;
        dev.mode   = m_send;
        dev.dmaLen = dev.pkt._len;
        dev.ramp   = dev.pkt._ramp;
        /*  if immediate mode, set up the ramp in the imm buffer */
        if ( dev.dmaLen == MAX_IMM_LEN ) {
            u32 i;
            u8 *bp = (u8 *)&dev.immData;

            for( i = 0 ; i < MAX_IMM_LEN ; i++ ) {
                *bp++ = dev.ramp + i;
            }
        }
        break;
    case di_write:
        /*  XXX May want to vary the delays for when we're ready to */
        /*  start sending/receiving. Perhaps increase the strbDly here... */
        dev.mode    = m_recv;
        dev.strbDly = pktDelay;
        dev.dmaLen  = dev.pkt._len;
        dev.ramp    = dev.pkt._ramp;
        break;
    case di_irq:
        if ( dev.pkt._mask ) {
            dev.state |= D_IRQSMSKD;
        }
        else {
            dev.state &= ~D_IRQSMSKD;
        }
        break;
    case di_door:
    case di_doorClk:
        dev.doorOpen  = dev.pkt._open;
        dev.doorClose = dev.pkt._close;
        if ( dev.pkt._cmd == di_doorClk ) {
            dev.state |= D_DOORCLK;
        }
        else {
            dev.state &= ~D_DOORCLK;
        }
        break;
    default:
        io_printf("SIM00: FAIL 999  didev: unexpected command %d @ %d\n", dev.pkt._cmd, 
                tf_gettime());
        return;
    }

    if (dev.mode == m_send ) {
        dev.goal = (strbRatio == DI_SPD_135NS) ? 13.5 : 27.0;
    }
    else {
        dev.goal = 25.0;
        if ( (dev.state & D_GENERR) && (dev.errDly > dev.dmaLen) ) {
            dev.errDly = dev.dmaLen;
        }
        /*  force it odd */
        if (!(dev.errDly & 1)) {
            dev.errDly--;
        }
    }

    if ( dev.mode == m_getCmd ) {
        dev.state |= D_WAITDIRLOW;
    }
}

static void
doHostPos(void)
{
    u8  data;
    u32 doErr = 0;

    dev.hostPos = tf_gettime();
    dev.hPosCnt++;

    /*  clear any pauses. */
    if ( (dev.state & D_RECVPAUSE) && (dev.recvPause == dev.hPosCnt) ) {
        dev.state &= ~D_RECVPAUSE;
        if ( (dev.state & D_GENERR) && (dev.errDly <= dev.hNegCnt) ) {
            doErr = 1;
        }
        else {
            tf_putp(p_dstrb, 0);
        }
    }

    data = tf_getp(p_ddin);
    if ( dev.mode == m_getCmd ) {
        /*  See if it's time to generate an IRQ */
        if ( !(dev.state & D_DOORCLK) ) {
            doDoor();
        }

        /*  See if this is a good one... */
        if ( dev.state & D_STRBDLYNEG ) {
            return;
        }

        if ( dev.curByte == 0 ) {
            dev.pktStart = tf_gettime();
        }

        assert(dev.curByte < PKT_LEN);
        dev.pkt.u.buf[dev.curByte++] = data;
        if ( dev.curByte == PKT_LEN ) {
            dev.pktEnd = tf_gettime();
            doCmd();
        }
        return;
    }

    /*  handle recv data */
    if ( dev.mode == m_recv ) {
        assert(tf_getp(p_dir) == 0);

        /*  see if this is the start of the DMA */
        if (dev.curByte == 0) {
            dev.brstStart = dev.hostNeg;
        }

        /*  See if it's time to generate an IRQ */
        if ( !(dev.state & D_DOORCLK) ) {
            doDoor();
        }

        /*  clock in data */
        if ( dev.state & D_INTBUF ) {
            dev.brst.u.buf[dev.curByte] = tf_getp(p_ddin);
        }
        else {
            if ( tf_getp(p_ddin) != (dev.ramp++ & 0xff) ) {
                io_printf("SIM00: FAIL recv , want:got 0x%x:0x%x @ %d\n",
                         dev.ramp - 1, tf_getp(p_ddin), tf_gettime());
                dev.errors |= D_ERR_RECV;
                dev.state |= D_ERROR;
            }
        }
        dev.curByte++;

        /*  check for errors */
        if (doErr) {
            forceErr();
	    return;
        }

        /*  Are we done */
        if ( dev.curByte == dev.dmaLen ) {
            /*  Go back to command mode after the appropriate delay */
            if ( dev.pkt._cmd == di_setDelays ) {
                setDelays();
            }

            devGetCmd();
            /*  XXX should track delay between hosts next packet */
            return;
        }

        return;
    }

    /*  Check for disables from the host */
    if ( dev.mode == m_send ) {
#if 0
        dev.state |= D_HSTPAUSE;
        dev.pause = (3 - (dev.curByte & 1)) + dev.curByte;
#endif /*  0 */
        return;
    }
}

static void
doBrk(void)
{
    if ( dev.mode == m_brk0 ) {
        return;
    }

    if ( dev.mode == m_brk1 ) {
        if ( POSEDGE(p_brkin) ) {
            return;
        }

	/*  Clear any gen'd errors */
        tf_putp(p_err, 1);

        /*  go back to packet command mode */
        dev.state |= D_BROKE;
        devGetCmd();
    }

    if ( NEGEDGE(p_brkin) ) {
        return;
    }

#if NSY
    io_printf("didev: p_brk %d at %d\n", tf_getp(p_brkin), tf_gettime());
#endif /*  NSY */
    dev.mode    = m_brk0;
    dev.state   = D_STRBDLYPOS | (dev.state & D_BRKSAVE);
    dev.strbDly = brkRespDelay;
}

#if DIMON
static void
brkVerify(void)
{
    u32 *bs;
    int  i; 

    mon.brkState++;
    if ( mon.brkState == 1 ) {
        mon.brkDir = tf_getp(p_dir);
    }

    bs = bsVals[mon.brkDir][mon.brkState - 1];

    for( i = 0 ; i < bs_maxsig ; i++ ) {
        /*  Check DIR */
        if ( (bs[i] != bs_xx) && (bs[i] !=  tf_getp(bsParms[i])) ) {
            io_printf("SIM00: FAIL brk%d on %s with %s %d @ %d\n",
                        mon.brkState, dirStr[mon.brkDir], sigStr[i], 
                        tf_getp(bsParms[i]), tf_gettime());
        }
    }

#if 0
    /*  XXX Timing is such that the monitor can see one thing while */
    /*      DI sees another. In this case, DI thinks it's the same */
    /*      betwee 1 and 2 while the monitor doesn't. Relaxing this condition. */
    /*      Left in in case we need to bring it back. */

#error  This should not be compiled.

    /*  special case for HSTRB, sigh -- Nasty spec */
    if ( (mon.brkState == 2) && (tf_getp(p_hstrb) != oHstrb) ) {
            io_printf("SIM00: FAIL brk2 on %s with HSTRB %d @ %d\n",
                        dirStr[mon.brkDir], tf_getp(p_hstrb), tf_gettime());
    }
    oHstrb = tf_getp(p_hstrb);
#endif /*  0 */
    
    if ( mon.brkState == 4 ) {
        mon.brkState = 0;
    }
}

static void
doMonitor(void)
{
    u32 val;

    if ( tf_getp(p_rst) == 0) {
        if ( mon.reset == 1 ) {
            fprintf(mon.fp, "%8d: reset low\n", tf_gettime());
            mon.change = tf_gettime();
        }
        mon.reset = 0;
        return;
    }
    else if (mon.reset == 0) {
        fprintf(mon.fp, "%8d: reset high\n", tf_gettime());
        mon.reset = 1;
        mon.change = tf_gettime();
        /*  get initial values */
        mon.brk = tf_getp(p_brkin);
        mon.err = tf_getp(p_err);
        mon.hstrb = tf_getp(p_hstrb);
        mon.dstrb = tf_getp(p_dstrb);
        return;
    }

    /*  brk */
    if ( (val = tf_getp(p_brkin)) != mon.brk ) {
        mon.brk = val;
        fprintf(mon.fp, "%8d: brk %s\n", tf_gettime(), (mon.brk)?"high":"low");
        brkVerify();
    }

    /*  err */
    if ( (val = tf_getp(p_err)) != mon.err ) {
        mon.err = val;
        fprintf(mon.fp, "%8d: err %s\n", tf_gettime(), (mon.err)?"high":"low");
    }

    /*  hstrb */
    if ( (val = tf_getp(p_hstrb)) != mon.hstrb ) {
        mon.hstrb = val;
#if 0
	if ( tf_getp(p_dir) == 1 ) {
            return;
        }
#endif /*  0 */
        if ( (tf_getp(p_dir) == 0) && mon.hstrb && (tf_getp(p_dstrb) == 0) ) {
            fprintf(mon.fp, "%8d: H %02x\n", tf_gettime(), tf_getp(p_ddin));
        }
    }

    /*  dstrb */
    if ( (val = tf_getp(p_dstrb)) != mon.dstrb ) {
        mon.dstrb = val;
        if ( (tf_getp(p_dir) == 1) && mon.dstrb ) {
            fprintf(mon.fp, "%8d: D %02x\n", tf_gettime(), tf_getp(p_ddout));
        }
    }
}
#endif /*  DIMON */

static void
clrErr(void)
{
    if ( !(dev.state & D_ERROR3) ) {
        return;
    }
    dev.errDly--;
    if ( dev.errDly ) {
        return;
    }

    /*  clear the error state. */
#if NSY
    io_printf("didev: clear error @ %d\n", tf_gettime());
#endif /*  NSY */
    assert(tf_getp(p_err) == 0);
    tf_putp(p_err, 1);
    dev.state &= ~D_ERROR3;
}

int
misc_didev(int data, int reason, int paramvc)
{
    /*  only interested in parameter changes */
    if ( reason != reason_paramvc ) {
        return 0;
    }

    /*  As long as we've got reset held, do nothing... */
    if ( paramvc == p_rst ) {
        if ( tf_getp(p_rst) == 0 ) {
            devReset(1);
        }
        else {
            devReset(0);
        }
    }

    /*  don't do anything while we're reset */
    if ( dev.state & D_RESET ) {
        goto doMon;
    }

    /*  p_clock drives the internal workings of the drive */
    if ( paramvc == p_clock ) {
        if ( NEGEDGE(p_clock) ) {
            goto doMon;
        }
	if (clkPer == 0) {
	    if (startPer == 0) {
		startPer = tf_gettime();
	    }
	    clkCnt++;
	    if (clkCnt == 5) {
		clkPer = tf_gettime() - startPer;
		if (clkPer != 74) {
		    io_printf("DVD PLI run with clk per %dns\n", clkPer);
		}
	    }
	}
        doCover();
        doBrk();
        clrErr();

        dev.strbDly--;
        if ( dev.strbDly ) {
            goto doMon;
        }

        if ( dev.state & D_STRBDLYNEG ) {
            doDevNeg();
        }
        else if ( dev.state & D_STRBDLYPOS ) {
            doDevPos();
        }

        goto doMon;
    }

    /*  host strobe drives our state */
    if ( paramvc == p_hstrb ) {
        if ( NEGEDGE(p_hstrb) ) {
            doHostNeg();
        }
        else {
            doHostPos();
        }
        goto doMon;
    }

#if NSY
    /*  io_printf("didev: param %s changed at %d!\n", pStrs[paramvc], tf_gettime()); */
#endif /*  NSY */

doMon:
#if DIMON
    doMonitor();
#endif /*  DIMON */

    return 0;
}
