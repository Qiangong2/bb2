/*****************************************************************************
 * 
 *   (C) 1999 ARTX INC..  ALL RIGHTS RESERVED.  UNPUBLISHED -- RIGHTS
 *   RESERVED UNDER THE COPYRIGHT LAWS OF THE UNITED STATES.  USE OF A
 *   COPYRIGHT NOTICE IS PRECAUTIONARY ONLY AND DOES NOT IMPLY PUBLICATION
 *   OR DISCLOSURE.
 *
 *   THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF
 *   ARTX INC..  USE, DISCLOSURE, OR REPRODUCTION IS PROHIBITED WITHOUT
 *   THE PRIOR EXPRESS WRITTEN PERMISSION OF ARTX INC..
 *
 *                   RESTRICTED RIGHTS LEGEND
 *
 *   Use, duplication, or disclosure by the Government is subject to
 *   restrictions as set forth in subparagraph (c)(1)(ii) of the Rights
 *   in Technical Data and Computer Software clause at DFARS 252.227-7013
 *   or subparagraphs (c)(1) and (2) of Commercial Computer Software --
 *   Restricted Rights at 48 CFR 52.227-19, as applicable. 
 *
 *   ArtX Inc.
 *   3400 Hillview Ave, Bldg 5
 *   Palo Alto, CA 94304
 *
 ****************************************************************************/

    integer dindex, dmalength;
    reg cover;
    event resp_event;

`ifdef CHIP_GATE_TILE
    wire[2:0] 	cbrkstate;
    wire	brkidle_mode;

    assign cbrkstate = {system.flipper.flipper_core.NB.io0.io_di0. \iodidctl/cbrkstate_reg[2] .Q , system.flipper.flipper_core.NB.io0.io_di0. \iodidctl/cbrkstate_reg[1] .Q , system.flipper.flipper_core.NB.io0.io_di0. \iodidctl/cbrkstate_reg[0] .Q };

    assign brkidle_mode = !(cbrkstate) ? 1 : 0;
`endif

    always @(reset or brk)
      begin
	dindex = 0;
    	dmalength = 0;
	cover = 1'b0;
      end

    always @(reset)
      if (reset) begin
	disable receive_command;
	disable send_response;
      end

    always begin
      wait(reset);
`ifdef CHIP_GATE_TILE
      wait(brkidle_mode);
`else
      wait(system.flipper.flipper_core.io0.io_di0.iodidctl.brkidle_mode);
`endif
      receive_command("../diag/di0/di_cmd.txt");
      if (err) begin 
	if(receive_command.buff[0] == 8'hf1 )
	  begin
	    if (receive_command.buff[3] == 8'h0e) cover = 1'b1;
	    else cover = 1'b0;
	    if (receive_command.buff[3] == 8'hff) reserve_err;
 	    -> resp_event;
	    send_response("../diag/di0/di_dvddata.txt",dindex,dindex+3);
	    dindex = dindex + 4;
	    if (receive_command.buff[3] == 8'hfe) reserve_err;
	    if (receive_command.buff[3] == 8'h0f) cover = 1'b1;
	    else cover = 1'b0;
	  end
	else if (receive_command.buff[0] == 8'ha8)
	  begin
	    if (receive_command.buff[3] == 8'h0e) cover = 1'b1;
	    else cover = 1'b0;
	    if (receive_command.buff[3] == 8'hff) reserve_err;
	    dmalength = (receive_command.buff[8] << 24) |
		        (receive_command.buff[9] << 16) |
		        (receive_command.buff[10] << 8) |
			(receive_command.buff[11]);
 	    -> resp_event;
	    send_response("../diag/di0/di_dvddata.txt",dindex,dindex+dmalength-1);
	    dindex = dindex + dmalength;
	    if (receive_command.buff[3] == 8'hfe) reserve_err;
	    if (receive_command.buff[3] == 8'h0f) cover = 1'b1;
	    else cover = 1'b0;
	  end
      end
    end

    always @(resp_event)
      begin
	if (receive_command.buff[3] == 8'h8f) send_wait(dindex+10, (40 * `unittime));
      end

