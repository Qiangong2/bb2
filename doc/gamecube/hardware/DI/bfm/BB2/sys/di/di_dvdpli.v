/*****************************************************************************
 * 
 *   (C) 1999 ARTX INC..  ALL RIGHTS RESERVED.  UNPUBLISHED -- RIGHTS
 *   RESERVED UNDER THE COPYRIGHT LAWS OF THE UNITED STATES.  USE OF A
 *   COPYRIGHT NOTICE IS PRECAUTIONARY ONLY AND DOES NOT IMPLY PUBLICATION
 *   OR DISCLOSURE.
 *
 *   THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF
 *   ARTX INC..  USE, DISCLOSURE, OR REPRODUCTION IS PROHIBITED WITHOUT
 *   THE PRIOR EXPRESS WRITTEN PERMISSION OF ARTX INC..
 *
 *                   RESTRICTED RIGHTS LEGEND
 *
 *   Use, duplication, or disclosure by the Government is subject to
 *   restrictions as set forth in subparagraph (c)(1)(ii) of the Rights
 *   in Technical Data and Computer Software clause at DFARS 252.227-7013
 *   or subparagraphs (c)(1) and (2) of Commercial Computer Software --
 *   Restricted Rights at 48 CFR 52.227-19, as applicable. 
 *
 *   ArtX Inc.
 *   3400 Hillview Ave, Bldg 5
 *   Palo Alto, CA 94304
 *
 ****************************************************************************/
module di_dvdtop (/*AUTOARG*/

    // Inout to for io_di in flipper
    did, dibrk,

    // Output to io_di in flipper
    didstrbb, dierrb, dicover, aisd,

    // Input from io_di in flipper
    didir, dihstrbb, dirstb, aisclk, aislr

);

    /*
     * Input, Output, Inout Ports declaration
     */
    // Inout
    inout [7:0]
	did;			// Data bus

    inout
	dibrk;			// Break

    // Output to io_di in flipper
    output
	didstrbb;		// Device (DVD) strobe, active low

    output
	dierrb;			// Error, active low

    output
	dicover;		// Cover, high for cover open

    output
	aisd;			// audio stream data

    // Input from io_di in flipper
    input
	didir;			// Data direction 1 for from DVD

    input
	dihstrbb;		// Host strobe, active low

    input
    	dirstb;			// Reset, active low

    input
	aisclk;			// audio bit clock

    input
	aislr;			// audio left/right clock

	  
   /*
    * Instantiations
    */

    // Outputs
    reg clk;
    reg didstrbb, dicover, aisd;
    reg [7:0] did_out;
    reg dibrk_out;
    reg did_oe, dibrk_oe, dierrb;

    integer clk_per;

assign did   = did_oe   ? did_out   : 8'bz;
assign dibrk = dibrk_oe ? dibrk_out : 1'bz;

    initial
      begin
`ifdef NO_PLI
`else
    $di_Dev(did_out, didstrbb, dicover, dibrk_out, did_oe, dibrk_oe, dierrb,
            did, dibrk, didir, dihstrbb, dirstb, clk);
    $di_stream(dirstb, aisclk, aislr, aisd);
`endif
      end // initial

    initial
      begin
	clk_per = $GetCLVal("DvdClkPer+");
        if (clk_per <= 0)
          begin
`ifdef TESTER_MODE
`ifdef SLOW_TESTER_MODE
          clk_per = 8000;
`else
          clk_per = 80;
`endif
`else
          clk_per = 74;
`endif
          end
	clk = 1'b0;
      end

    always
      begin
	#(clk_per / 8.0) clk = ~clk;
      end

endmodule // di_dvdtop
