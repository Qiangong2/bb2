11/19/03 1.1 Release of Flipper ChipC design and documents to RD3/BroadOn from ATI
  Only delta from original release included
  AI h/w design - see hw/ai/README - also includes AI test bench and C model
  Docs - see doc/spec/dspintf.doc, doc/spec/iodetail.doc, doc/spec/iotop.doc, doc/spec/soundhw_revb.doc
