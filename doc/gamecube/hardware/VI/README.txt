11/20/03 1.2 Release of Flipper ChipC design and documents to RD3/BroadOn from ATI
  Only delta from original release included
  VI h/w design - see hw/vi/README - also bfm fir mem i/f
  Docs - see doc/spec/vi.doc
