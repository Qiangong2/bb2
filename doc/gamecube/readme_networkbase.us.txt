***********************************************************************
*             Nintendo GameCube Network Base Package                  *
*                       August 5, 2003                                *
***********************************************************************

Table of Contents
  (1) Introduction
  (2) Copyright Notice
  (3) Revision History
  (4) File List

=======================================================================
(1) Introduction
=======================================================================
The Network Base Package is a set of low-level libraries for the 
Broadband Adapter and the Modem Adapter.

eth.a   The Ethernet library (non-debug version)
ethD.a  The Ethernet library (debug version)
mdm.a   The Modem library (non-debug version)
mdmD.a  The Modem library (debug version)

This package is required for both the Network SDK Package and the Socket
Library Package.

This package is guaranteed to work only if it is used with 
Nintendo GameCube SDK Version 5-MAY-2003 + Patch 1 or later. To install 
this package, copy the extracted files to the directory where the 
Nintendo GameCube SDK is installed.

For information on hardware installation, see HWInstallation_Guide.us.pdf.

=======================================================================
(2) Copyright Notice
=======================================================================

This library uses software derived from the MD5 Message-Digest Algorithm 
developed by RSA Security, Inc. When marketing games using the library 
included with this package, the following copyright statements must be 
placed in the operation manuals.

Japanese Display

This software uses MD5 Message-Digest Algorithm from RSA Security, Inc.
Copyright (C) 1991-2, RSA Security, Inc. Created 1991. All rights 
reserved.

English Display

This product includes software derived from the RSA Security Inc. MD5
Message-Digest Algorithm.
Copyright (C) 1991-2, RSA Security Inc. Created 1991. All rights reserved.

For additional details, see RFC 1321 "The MD5 Message-Digest Algorithm."

=======================================================================
(3) Revision History
=======================================================================

8/5/2003
- Revisions to the Modem Library
  Revised the Modem Library to support Nintendo GameCube SDK version
  8-MAY-2003 + Patch1.

- Revisions to the Ethernet Library
  Made changes so that settings could be made to refuse packets after
  disconnecting from the network.

- Fixed a problem that occasionally caused delays in receiving the final
  packet when a series of packets were received by the Nintendo GameCube 
  in a burst.

- Revised the back-off algorithm to use the Ethernet standard binary
  exponential back-off (BEB) algorithm. BBA has been set so that
  non-standard algorithms can be used in the future.

3/20/2003    
- Added the following files:
  md5.h (Header file for MD5. See (2) Copyright Notice.)
  md5.c (Source file for MD5. See (2) Copyright Notice.)
  mdm_country.h (Country code definition file for the Modem Adapter)

- Changes to the Ethernet library:
  Fixed a problem of internal processes.

1/9/2003
- Changes to the Ethernet library:
  Fixed a problem in which the interrupt for the second packet
  sometimes did not take place when another packet arrived during a
  receive interrupt process.

- Changes to the Modem library:
  Fixed a problem in which DELAYED did not occur, even when the
  program dialed the same number repeatedly.

=======================================================================
(4) File List
=======================================================================
./build/libraries/eth/src/md5.c
./HW2/lib/eth.a
./HW2/lib/ethD.a
./HW2/lib/mdm.a
./HW2/lib/mdmD.a
./include/dolphin/md5.h
./include/dolphin/mdm_country.h
./patch_docs/docs_us/HWInstallation_Guide.us.pdf
./readme_networkbase.us.txt

=======================================================================
If you have any technical questions concerning the Network SDK, post on
the SDSG news groups (news.sdsg.nintendo.com) or contact us directly at
support@noa.com.
=======================================================================


