***********************************************************************
*               Nintendo GameCube Network SDK                         *
*                    September 30, 2003                               *
***********************************************************************

* Table of Contents
(1) Introduction
(2) Security Considerations while Using the Broadband Adapter
(3) Installation
(4) Compatible Versions of the Nintendo GameCube SDK
(5) Revision History

=======================================================================
(1) Introduction
=======================================================================

The Nintendo GameCube Network SDK is made up of the following modules 
and files:

avetcp.zip       This module, developed by Access Co., Ltd., includes
                 a library that implements TCP/IP protocols, including 
                 TCP, UDP, IPv4, ICMPv4, ARP, PPP, PPPoE, DNS, and DHCP. 
                 Extract the Zip file outside of the directory where you 
                 have installed the Nintendo GameCube SDK.

h2h.zip          This module, developed by Dwango Co., Ltd., is a middle-
                 ware module for network-enabled Nintendo GameCube games.
                 It allows a game to be played using two Modem Adapters 
                 connected over a phone line (head-to-head). Be sure to
                 extract the Zip file outside of the directory where you 
                 have installed the Nintendo GameCube SDK.

readme_avetcp.us.txt
                 This is the readme file for the AVE-TCP module.

readme_h2h.us.txt
                 This is the readme file for the Head-to-Head module.

readme.us.txt    This file.

If you are using the AVE-TCP module, there is no need to install the Head-
to-Head module, and if you are using Head-to-Head, there is no need to 
install AVE-TCP. You cannot use both modules at the same time by linking 
to them from a single application.

The AVE-TCP and Head-to-Head modules are freeware. However, you do need
to display copyright information. More details will be provided in later
versions.

=======================================================================
(2) Security Considerations while Using the Broadband Adapter
=======================================================================

A Nintendo GameCube system and an NR Reader cannot communicate with other 
network devices (such as a PC) on the same LAN using the Broadband Adapter.
Due to this limitation, if you use a PC as a server (for example), the 
server needs to be on a different segment from the one to which the 
Nintendo GameCube system or the NR Reader belongs. This limitation does
not apply to development devices other than the NR Reader (such as NPDP-
GDEV, DDH, NPDP-GBOX, or the NPDP Console).

=======================================================================
(3) Installation
=======================================================================

The files were compressed using Zip format. Use software such as WinZip
or +Lhaca to extract them.

If you use the Network SDK Package, you need to install the Network Base
Package as well. For information on installing the Network Base Package,
see the readme file in that package.

Make sure that the AVE-TCP and Head-to-Head modules are installed in 
separate directories from the Nintendo GameCube SDK install folder. For 
example, you could install them in: C:/Access/avetcp and C:/Dwango/h2h.


After installation, define the directories where the AVE-TCP and Head-
to-Head modules were installed as the environment variables AVETCP_ROOT 
and H2H_ROOT, respectively.


=======================================================================
(4) Compatible Versions of the Nintendo GameCube SDK
=======================================================================

Proper functionality of the Network SDK can only be guaranteed with 
Nintendo GameCube SDK Version 8-May-2003 Patch 1 or later, and the 
Network Base Package Version 5-Aug-2003 or later.


=======================================================================
(5) Revision History
=======================================================================

09/30/2003     Updated AVE-TCP
09/09/2003     Updated AVE-TCP
08/27/2003     Updated AVE-TCP
04/01/2003     Updated AVE-TCP
11/14/2002     First release

=======================================================================
If you have any technical questions concerning the Network SDK, post on
the SDSG news groups (news.sdsg.nintendo.com) or contact us directly at
support@noa.com.
=======================================================================

