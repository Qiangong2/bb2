***********************************************************************
*                   Nintendo GameCube Network SDK                     *
*                          AVE-TCP Module                             *
*                        September 30, 2003                           *
***********************************************************************

* Table of Contents
(1) Introduction
(2) Directory Structure
(3) Revision History
(4) File List

=======================================================================
(1) Introduction
=======================================================================

The AVE-TCP module implements the TCP/IP protocol stack for 
Nintendo GameCube.

To use this module, you must install the Network Base Package.

To install this module, first create an appropriate directory, such as
C:/Access/avetcp, and then copy the extracted files to this directory. 
Also, define the directory (in the form of C:/Access/avetcp) 
as the environment variable AVETCP_ROOT.


=======================================================================
(2) Directory Structure
=======================================================================

include/
        common/  
                 API include files (the AVE-TCP Library API)
        ppp/     
                 PPP include files
        target/  
                 Include files for the Nintendo GameCube

lib/
        AVE-TCP debug-version and release-version library files


doc/
        Documents (refer to ./doc/avetcp/html/index.htm)
        
atdemo/
atdemo2/
atdemo3/
        Sample programs


=======================================================================
(3) Revision History
=======================================================================

09/24/2003

- Fixed the problem that terminated a PPPoE connection 30 seconds after
  being established when using the combination of FletsADSL and specific
  providers.
  This problem occurred when a PPPoE connection was used with providers
  (such as DION) that support only PAP as its authentication protocol.

- Fixed the problem that caused a Fault in the DNS reverse lookup process.
  Corrected the problem that caused a Fault when multiple names were
  stored in the response packet and compression of the domain name
  occurred repeatedly.

09/09/2003

- Fixed the problem that caused the internal timer process to stop.
  When the internal timer stops, the detection of ethernet cable 
  disconnection, DHCP lease extension, or the release process may not
  occur.

- Fixed the problem that caused omissions when the internal resources
  for ppp_term() and dix_term() are released.
  When ppp_term() or dix_term() were called repeatedly, internal resources
  would be used up causing abnormal operations unless the module was
  re-initialized.

- Fixed the problem that caused an incorrect address to be returned
  during DNS lookup.
  A problem existed when the name passed by the application was compared
  to the internal address cache. When the character string for the name
  passed by the applications was completely contained in the character
  string in the address cache, the cache would register a hit and no
  query would be issued.

  Example of the problem:
  If a lookup request for "www.aaaa.com" is received and the internal 
  cache has the address "www.aaaa.com.tw" stored, "www.aaaa.com.tw" will
  be returned.

- Fixed the problem that caused an error (AT_DNS_API_NO_CACHE) to be
  returned by dns_get_addr() and dns_get_name().
  When multiple DNS queries were issued simultaneously, dns_get_addr() and
  dns_get_name() would return errors even though valid responses were
  received from the server.

- Fixed the problem that caused the character string acquired by 
  dns_get_name and the string length to not match.
  The acquired character string included a period at the end of the string
  while the character string length did not. The character string was
  revised to eliminate the period at the end.

- Fixed the problem that caused an incorrect checksum value to be 
  acquired when data of an odd byte length was passed to ip4_chksum.
  This problem is related to application such as ping that call ip4_chksum
  to create ICMP data.
  This problem does not affect TCP/UDP communications.

- Fixed the argument names for the ASR functions (AT_ASR_FUNCTION)
  declared in at_api.h.

- Revised Description of AVE-TCP Sample Applications and AVE-TCP
  Instruction Manual.

08/27/2003

- Fixed the problem that prevented DHCP_release from returning.

- Made a revision that prevents ethernet reception interruptions after 
  ethernet use is completed.

- Added an example of IP address collision detection to atdemo sample.

- Revised documentation.

- Revised the sample applications and Description of AVE-TCP Sample 
  Applications manual.

04/01/2003

- Changed the default directory that is created when the 
  environment variable AVETCP_ROOT is not defined from 
  C:/SegaAccess/avetcp to C:/Access/avetcp.

- Converted AVE-TCP for Nintendo GameCube to HTML format.

- Added API for conflicting IP address detection processing
  (arp_probe(), arp_probe_cancel()).

- Added API for changing MTU (if_set_opt(), if_get_opt()).

- Added API for changing DHCP options (DHCP_add_option_list(), 
  DHCP_get_option()).

- Added API for obtaining PPP processing information (ppp_get_info()).
  This is used to obtain the CHAP/PAP message sent from the 
  destination PPP server.

- Added tcp_set_opt() and tcp_get_opt() options.

- Changed the resend interval for the DISCOVER packet during 
  SELECTING status, and the REQUEST packet during REQUESTING 
  status, so that it is based on a randomized exponential 
  backoff algorithm(RFC2131).

- Added the error value from the PPPoE discovery stage to 
  the PPP status structure (AT_PPP_STAT_ARG) error. 

- Changed the specifications for dns_set_server() so that
  an error does not occur even if you try to set up a DNS 
  server address that has been entered before.

- Changed the length of the character string that can be 
  set up with DHCP_hostname() from 31 to 255.

- During an AVE-TCP sub-module API call procedure problem, 
  output the error message to the serial line with OSReport(), 
  when using the debug version of the library.

- Moved the Ethernet internal buffer area to the work area 
  set aside by the user and modified so that the size can be 
  changed (ether_buffer_num of the AT_INIT_PARAM structure).
        
- Fixed the problem of the MAC address value obtained with 
  if_get() becoming indeterminable when connecting to PPPoE. 

- Fixed the problem of LCP TermReq being sent during reconnect 
  after abnormal PPP disconnection (telephone cable disconnection). 

11/14/2002     

- First release

=======================================================================
(4) File List
=======================================================================

./atdemo/atdemo.h
./atdemo/dns.c
./atdemo/http.c
./atdemo/main.c
./atdemo/makefile
./atdemo/ping.c
./atdemo/readme.us.txt
./atdemo/show_ifinfo.c
./atdemo/show_route.c
./atdemo/tcp_chargen.c
./atdemo/tcp_discard.c
./atdemo/tcp_discard_server.c
./atdemo/tcp_echo1.c
./atdemo/tcp_echo2.c
./atdemo/tcp_echo3.c
./atdemo/udp_echo.c
./atdemo2/atdemo.h
./atdemo2/dns.c
./atdemo2/http.c
./atdemo2/main.c
./atdemo2/makefile
./atdemo2/tcp_discard.c
./atdemo2/tcp_discard_server.c
./atdemo3/atdemo.h
./atdemo3/main.c
./atdemo3/makefile
./atdemo3/ping.c
./atdemo3/tcp_discard.c
./atdemo3/udp_discard.c
./doc/HTML/avetcp_instruction/00b-RevHistory.html
./doc/HTML/avetcp_instruction/01-Introduction.html
./doc/HTML/avetcp_instruction/02-Overview.html
./doc/HTML/avetcp_instruction/02-Overview2.html
./doc/HTML/avetcp_instruction/02-Overview3.html
./doc/HTML/avetcp_instruction/03-Specs.html
./doc/HTML/avetcp_instruction/03-Specs2.html
./doc/HTML/avetcp_instruction/03-Specs3.html
./doc/HTML/avetcp_instruction/03-Specs4.html
./doc/HTML/avetcp_instruction/03-Specs5.html
./doc/HTML/avetcp_instruction/03-Specs6.html
./doc/HTML/avetcp_instruction/03-Specs7.html
./doc/HTML/avetcp_instruction/03-Specs8.html
./doc/HTML/avetcp_instruction/03-Specs9.html
./doc/HTML/avetcp_instruction/04-API_Use_Proc.html
./doc/HTML/avetcp_instruction/04-API_Use_Proc10.html
./doc/HTML/avetcp_instruction/04-API_Use_Proc11.html
./doc/HTML/avetcp_instruction/04-API_Use_Proc12.html
./doc/HTML/avetcp_instruction/04-API_Use_Proc13.html
./doc/HTML/avetcp_instruction/04-API_Use_Proc14.html
./doc/HTML/avetcp_instruction/04-API_Use_Proc15.html
./doc/HTML/avetcp_instruction/04-API_Use_Proc16.html
./doc/HTML/avetcp_instruction/04-API_Use_Proc17.html
./doc/HTML/avetcp_instruction/04-API_Use_Proc18.html
./doc/HTML/avetcp_instruction/04-API_Use_Proc19.html
./doc/HTML/avetcp_instruction/04-API_Use_Proc2.html
./doc/HTML/avetcp_instruction/04-API_Use_Proc20.html
./doc/HTML/avetcp_instruction/04-API_Use_Proc21.html
./doc/HTML/avetcp_instruction/04-API_Use_Proc3.html
./doc/HTML/avetcp_instruction/04-API_Use_Proc4.html
./doc/HTML/avetcp_instruction/04-API_Use_Proc5.html
./doc/HTML/avetcp_instruction/04-API_Use_Proc6.html
./doc/HTML/avetcp_instruction/04-API_Use_Proc7.html
./doc/HTML/avetcp_instruction/04-API_Use_Proc8.html
./doc/HTML/avetcp_instruction/04-API_Use_Proc9.html
./doc/HTML/avetcp_instruction/05-API_Reference.html
./doc/HTML/avetcp_instruction/05-API_Reference100.html
./doc/HTML/avetcp_instruction/05-API_Reference2.html
./doc/HTML/avetcp_instruction/05-API_Reference20.html
./doc/HTML/avetcp_instruction/05-API_Reference21.html
./doc/HTML/avetcp_instruction/05-API_Reference22.html
./doc/HTML/avetcp_instruction/05-API_Reference23.html
./doc/HTML/avetcp_instruction/05-API_Reference24.html
./doc/HTML/avetcp_instruction/05-API_Reference25.html
./doc/HTML/avetcp_instruction/05-API_Reference26.html
./doc/HTML/avetcp_instruction/05-API_Reference27.html
./doc/HTML/avetcp_instruction/05-API_Reference28.html
./doc/HTML/avetcp_instruction/05-API_Reference29.html
./doc/HTML/avetcp_instruction/05-API_Reference3.html
./doc/HTML/avetcp_instruction/05-API_Reference30.html
./doc/HTML/avetcp_instruction/05-API_Reference31.html
./doc/HTML/avetcp_instruction/05-API_Reference32.html
./doc/HTML/avetcp_instruction/05-API_Reference33.html
./doc/HTML/avetcp_instruction/05-API_Reference34.html
./doc/HTML/avetcp_instruction/05-API_Reference35.html
./doc/HTML/avetcp_instruction/05-API_Reference36.html
./doc/HTML/avetcp_instruction/05-API_Reference37.html
./doc/HTML/avetcp_instruction/05-API_Reference38.html
./doc/HTML/avetcp_instruction/05-API_Reference39.html
./doc/HTML/avetcp_instruction/05-API_Reference4.html
./doc/HTML/avetcp_instruction/05-API_Reference40.html
./doc/HTML/avetcp_instruction/05-API_Reference41.html
./doc/HTML/avetcp_instruction/05-API_Reference42.html
./doc/HTML/avetcp_instruction/05-API_Reference43.html
./doc/HTML/avetcp_instruction/05-API_Reference44.html
./doc/HTML/avetcp_instruction/05-API_Reference45.html
./doc/HTML/avetcp_instruction/05-API_Reference46.html
./doc/HTML/avetcp_instruction/05-API_Reference47.html
./doc/HTML/avetcp_instruction/05-API_Reference48.html
./doc/HTML/avetcp_instruction/05-API_Reference49.html
./doc/HTML/avetcp_instruction/05-API_Reference5.html
./doc/HTML/avetcp_instruction/05-API_Reference50.html 
./doc/HTML/avetcp_instruction/05-API_Reference51.html
./doc/HTML/avetcp_instruction/05-API_Reference52.html
./doc/HTML/avetcp_instruction/05-API_Reference53.html
./doc/HTML/avetcp_instruction/05-API_Reference54.html
./doc/HTML/avetcp_instruction/05-API_Reference55.html
./doc/HTML/avetcp_instruction/05-API_Reference56.html
./doc/HTML/avetcp_instruction/05-API_Reference57.html
./doc/HTML/avetcp_instruction/05-API_Reference58.html
./doc/HTML/avetcp_instruction/05-API_Reference59.html
./doc/HTML/avetcp_instruction/05-API_Reference6.html
./doc/HTML/avetcp_instruction/05-API_Reference60.html
./doc/HTML/avetcp_instruction/05-API_Reference61.html
./doc/HTML/avetcp_instruction/05-API_Reference62.html
./doc/HTML/avetcp_instruction/05-API_Reference63.html
./doc/HTML/avetcp_instruction/05-API_Reference64.html
./doc/HTML/avetcp_instruction/05-API_Reference65.html
./doc/HTML/avetcp_instruction/05-API_Reference66.html
./doc/HTML/avetcp_instruction/05-API_Reference67.html
./doc/HTML/avetcp_instruction/05-API_Reference68.html
./doc/HTML/avetcp_instruction/05-API_Reference69.html
./doc/HTML/avetcp_instruction/05-API_Reference7.html
./doc/HTML/avetcp_instruction/05-API_Reference70.html
./doc/HTML/avetcp_instruction/05-API_Reference71.html
./doc/HTML/avetcp_instruction/05-API_Reference72.html
./doc/HTML/avetcp_instruction/05-API_Reference73.html
./doc/HTML/avetcp_instruction/05-API_Reference74.html
./doc/HTML/avetcp_instruction/05-API_Reference75.html
./doc/HTML/avetcp_instruction/05-API_Reference76.html
./doc/HTML/avetcp_instruction/05-API_Reference77.html
./doc/HTML/avetcp_instruction/05-API_Reference78.html
./doc/HTML/avetcp_instruction/05-API_Reference79.html
./doc/HTML/avetcp_instruction/05-API_Reference8.html
./doc/HTML/avetcp_instruction/05-API_Reference80.html
./doc/HTML/avetcp_instruction/05-API_Reference81.html
./doc/HTML/avetcp_instruction/05-API_Reference82.html
./doc/HTML/avetcp_instruction/05-API_Reference83.html
./doc/HTML/avetcp_instruction/05-API_Reference84.html
./doc/HTML/avetcp_instruction/05-API_Reference85.html
./doc/HTML/avetcp_instruction/05-API_Reference86.html
./doc/HTML/avetcp_instruction/05-API_Reference87.html
./doc/HTML/avetcp_instruction/05-API_Reference88.html
./doc/HTML/avetcp_instruction/05-API_Reference89.html
./doc/HTML/avetcp_instruction/05-API_Reference9.html
./doc/HTML/avetcp_instruction/05-API_Reference90.html
./doc/HTML/avetcp_instruction/05-API_Reference91.html
./doc/HTML/avetcp_instruction/05-API_Reference92.html
./doc/HTML/avetcp_instruction/05-API_Reference93.html
./doc/HTML/avetcp_instruction/05-API_Reference94.html
./doc/HTML/avetcp_instruction/05-API_Reference95.html
./doc/HTML/avetcp_instruction/05-API_Reference96.html
./doc/HTML/avetcp_instruction/05-API_Reference97.html
./doc/HTML/avetcp_instruction/05-API_Reference98.html
./doc/HTML/avetcp_instruction/05-API_Reference99.html
./doc/HTML/avetcp_instruction/AveTcpGC.usIX.xml
./doc/HTML/avetcp_instruction/AveTcpGC.usTOC.xml
./doc/HTML/avetcp_instruction/catalog.css
./doc/HTML/avetcp_instruction/document.css
./doc/HTML/avetcp_instruction/images/AT_PacketDumpStat_Packet_Capt_Data_Form.gif
./doc/HTML/avetcp_instruction/images/AT_SEND_BUFFS_Byte_Streams.gif
./doc/HTML/avetcp_instruction/images/AT_SEND_BUFFS_Datagrams.gif
./doc/HTML/avetcp_instruction/images/AT_SEND_BUFFS_Datagrams_UDP.gif
./doc/HTML/avetcp_instruction/images/AVE-TCP_Module_Struct.gif
./doc/HTML/avetcp_instruction/images/GCNLogoSmall.jpg
./doc/HTML/avetcp_instruction/images/ninlogo.gif
./doc/HTML/avetcp_instruction/images/PPP_Op_State_Tran_Schem.gif
./doc/HTML/avetcp_instruction/wwhdata/common/context.js
./doc/HTML/avetcp_instruction/wwhdata/common/files.js
./doc/HTML/avetcp_instruction/wwhdata/common/popups.js
./doc/HTML/avetcp_instruction/wwhdata/common/title.js
./doc/HTML/avetcp_instruction/wwhdata/common/topics.js
./doc/HTML/avetcp_instruction/wwhdata/common/towwhdir.js
./doc/HTML/avetcp_instruction/wwhdata/common/wwhpagef.js
./doc/HTML/avetcp_instruction/wwhdata/java/file.xml
./doc/HTML/avetcp_instruction/wwhdata/java/ix.xml
./doc/HTML/avetcp_instruction/wwhdata/java/search.xml
./doc/HTML/avetcp_instruction/wwhdata/java/toc.xml
./doc/HTML/avetcp_instruction/wwhdata/js/index.js
./doc/HTML/avetcp_instruction/wwhdata/js/search/search0.js
./doc/HTML/avetcp_instruction/wwhdata/js/search/search1.js
./doc/HTML/avetcp_instruction/wwhdata/js/search/search2.js
./doc/HTML/avetcp_instruction/wwhdata/js/search/search3.js
./doc/HTML/avetcp_instruction/wwhdata/js/search/search4.js
./doc/HTML/avetcp_instruction/wwhdata/js/search/search5.js
./doc/HTML/avetcp_instruction/wwhdata/js/search/search6.js
./doc/HTML/avetcp_instruction/wwhdata/js/search/search7.js
./doc/HTML/avetcp_instruction/wwhdata/js/search.js
./doc/HTML/avetcp_instruction/wwhdata/js/toc.js
./doc/HTML/avetcp_instruction/wwhelp/books.xml
./doc/HTML/avetcp_instruction/wwhelp/images/altclose.gif
./doc/HTML/avetcp_instruction/wwhelp/images/altopen.gif
./doc/HTML/avetcp_instruction/wwhelp/images/GCNLogo.jpg
./doc/HTML/avetcp_instruction/wwhelp/messages.xml
./doc/HTML/avetcp_instruction/wwhelp/settings.xml
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/html/blank.htm
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/html/bookmark.htm
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/html/content.htm
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/html/controll.htm
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/html/controlr.htm
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/html/default.htm
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/html/document.htm
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/html/init0.htm
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/html/init1.htm
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/html/init2.htm
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/html/init3.htm
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/html/pagenav.htm
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/html/switch.htm
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/html/title.htm
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/html/wwhelp.htm
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/images/bkmark.gif
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/images/bkmarkx.gif
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/images/close.gif
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/images/divider.gif
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/images/doc.gif
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/images/email.gif
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/images/emailx.gif
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/images/fc.gif
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/images/fo.gif
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/images/frameset.gif
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/images/next.gif
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/images/nextx.gif
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/images/prev.gif
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/images/prevx.gif
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/images/print.gif
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/images/printx.gif
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/images/related.gif
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/images/relatedi.gif
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/images/relatedx.gif
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/images/spacer4.gif
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/images/spc1w2h.gif
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/images/spc1w7h.gif
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/images/spc2w1h.gif
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/images/spc5w1h.gif
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/images/sync.gif
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/images/syncx.gif
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/private/books.js
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/private/locale.js
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/private/options.js
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/private/popupf.js
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/private/title.js
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/scripts/bklist1s.js
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/scripts/bookgrps.js
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/scripts/booklist.js
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/scripts/browseri.js
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/scripts/controls.js
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/scripts/documt1s.js
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/scripts/filelist.js
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/scripts/handler.js
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/scripts/help.js
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/scripts/highlt.js
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/scripts/pophelp.js
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/scripts/popus.js
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/scripts/related.js
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/scripts/strutils.js
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/common/scripts/switch.js
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/java/html/ie60win.htm
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/java/html/iemac.htm
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/java/html/iewindows.htm
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/java/html/netscape.htm
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/java/html/nosecie.htm
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/java/html/nosecie6.htm
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/java/html/nosecns.htm
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/java/html/wwhelp.htm
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/java/private/books.xml
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/java/private/locale.js
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/java/private/locale.xml
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/java/private/options.js
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/java/private/options.xml
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/java/scripts/handler.js
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/java/scripts/java.js
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/js/html/indexsel.htm
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/js/html/navigate.htm
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/js/html/panel.htm
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/js/html/panelini.htm
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/js/html/tabs.htm
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/js/html/wwhelp.htm
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/js/images/tabsbg.gif
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/js/private/locale.js
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/js/private/options.js
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/js/scripts/handler.js
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/js/scripts/index.js
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/js/scripts/index1s.js
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/js/scripts/javascpt.js
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/js/scripts/outlfast.js
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/js/scripts/outlin1s.js
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/js/scripts/outline.js
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/js/scripts/outlsafe.js
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/js/scripts/panels.js
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/js/scripts/search.js
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/js/scripts/search1s.js
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/js/scripts/search2s.js
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/js/scripts/search3s.js
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/js/scripts/search4s.js
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/js/scripts/tabs.js
./doc/HTML/avetcp_instruction/wwhelp/wwhimpl/version.htm
./doc/HTML/avetcp_instruction/wwhelp.htm
./doc/HTML/avetcp_instruction/wwhelp3.cab
./doc/HTML/avetcp_instruction/wwhelp3.jar
./doc/HTML/avetcp_sample/00b-RevHistory.html
./doc/HTML/avetcp_sample/01-Introduction.html
./doc/HTML/avetcp_sample/02-Overview.html
./doc/HTML/avetcp_sample/02-Overview2.html
./doc/HTML/avetcp_sample/02-Overview3.html
./doc/HTML/avetcp_sample/02-Overview4.html
./doc/HTML/avetcp_sample/03-Non-Blocking_Sample.html
./doc/HTML/avetcp_sample/03-Non-Blocking_Sample10.html
./doc/HTML/avetcp_sample/03-Non-Blocking_Sample11.html
./doc/HTML/avetcp_sample/03-Non-Blocking_Sample12.html
./doc/HTML/avetcp_sample/03-Non-Blocking_Sample13.html
./doc/HTML/avetcp_sample/03-Non-Blocking_Sample2.html
./doc/HTML/avetcp_sample/03-Non-Blocking_Sample3.html
./doc/HTML/avetcp_sample/03-Non-Blocking_Sample4.html
./doc/HTML/avetcp_sample/03-Non-Blocking_Sample5.html
./doc/HTML/avetcp_sample/03-Non-Blocking_Sample6.html
./doc/HTML/avetcp_sample/03-Non-Blocking_Sample7.html
./doc/HTML/avetcp_sample/03-Non-Blocking_Sample8.html
./doc/HTML/avetcp_sample/03-Non-Blocking_Sample9.html
./doc/HTML/avetcp_sample/04-Blocking_Sample.html
./doc/HTML/avetcp_sample/04-Blocking_Sample10.html
./doc/HTML/avetcp_sample/04-Blocking_Sample11.html
./doc/HTML/avetcp_sample/04-Blocking_Sample12.html
./doc/HTML/avetcp_sample/04-Blocking_Sample13.html
./doc/HTML/avetcp_sample/04-Blocking_Sample14.html
./doc/HTML/avetcp_sample/04-Blocking_Sample15.html
./doc/HTML/avetcp_sample/04-Blocking_Sample2.html
./doc/HTML/avetcp_sample/04-Blocking_Sample3.html
./doc/HTML/avetcp_sample/04-Blocking_Sample4.html
./doc/HTML/avetcp_sample/04-Blocking_Sample5.html
./doc/HTML/avetcp_sample/04-Blocking_Sample6.html
./doc/HTML/avetcp_sample/04-Blocking_Sample7.html
./doc/HTML/avetcp_sample/04-Blocking_Sample8.html
./doc/HTML/avetcp_sample/04-Blocking_Sample9.html
./doc/HTML/avetcp_sample/05-Loopback_Sample.html
./doc/HTML/avetcp_sample/05-Loopback_Sample10.html
./doc/HTML/avetcp_sample/05-Loopback_Sample2.html
./doc/HTML/avetcp_sample/05-Loopback_Sample3.html
./doc/HTML/avetcp_sample/05-Loopback_Sample4.html
./doc/HTML/avetcp_sample/05-Loopback_Sample5.html
./doc/HTML/avetcp_sample/05-Loopback_Sample6.html
./doc/HTML/avetcp_sample/05-Loopback_Sample7.html
./doc/HTML/avetcp_sample/05-Loopback_Sample8.html
./doc/HTML/avetcp_sample/05-Loopback_Sample9.html
./doc/HTML/avetcp_sample/ATGC_Sample.usIX.xml
./doc/HTML/avetcp_sample/ATGC_Sample.usTOC.xml
./doc/HTML/avetcp_sample/catalog.css
./doc/HTML/avetcp_sample/document.css
./doc/HTML/avetcp_sample/wwhelp.htm
./doc/HTML/avetcp_sample/wwhelp3.cab
./doc/HTML/avetcp_sample/wwhelp3.jar
./doc/HTML/avetcp_sample/images/Blocking_API_Sample_Flow.gif
./doc/HTML/avetcp_sample/images/Discard_Server_Screen-Non-Blocking.gif
./doc/HTML/avetcp_sample/images/DIX_Ethernet_Startup_Flow.gif
./doc/HTML/avetcp_sample/images/GCNLogoSmall.jpg
./doc/HTML/avetcp_sample/images/HTTP_GET_TEST_Screen_Blocking.gif
./doc/HTML/avetcp_sample/images/Interface_Info_Screen-Non-Blocking.gif
./doc/HTML/avetcp_sample/images/Interface_Shut-Down_Screen-Non-Blocking.gif
./doc/HTML/avetcp_sample/images/IP_Config_Method_Select_Screen-Blocking.gif
./doc/HTML/avetcp_sample/images/IP_Config_Method_Select_Screen-Non-Blocking.gif
./doc/HTML/avetcp_sample/images/Loopback_Sample_Flow.gif
./doc/HTML/avetcp_sample/images/Netw_Config_Start_Wait_Screen_Blocking.gif
./doc/HTML/avetcp_sample/images/Network_Config_Thread_Flow.gif
./doc/HTML/avetcp_sample/images/ninlogo.gif
./doc/HTML/avetcp_sample/images/Non-Blocking_API_Sample_Flow.gif
./doc/HTML/avetcp_sample/images/Notify_Func_Timing.gif
./doc/HTML/avetcp_sample/images/PING_TEST_Exec_Screen_Loopback.gif
./doc/HTML/avetcp_sample/images/PING_TEST_Stop_Screen_Loopback.gif
./doc/HTML/avetcp_sample/images/PPP_Startup_Process_Flow.gif
./doc/HTML/avetcp_sample/images/Route_Info_Screen-Non-Blocking.gif
./doc/HTML/avetcp_sample/images/TCP_DISCARD_SERVER_Screen_Blocking.gif
./doc/HTML/avetcp_sample/images/TCP_DISCARD_TEST_Screen_Loopback.gif
./doc/HTML/avetcp_sample/images/TCP_Echo_3_Test_Screen-Non-Blocking.gif
./doc/HTML/avetcp_sample/images/tcp_send_f_Notif_Proc_Completion_Time.gif
./doc/HTML/avetcp_sample/images/Test_Select_Screen-Non-Blocking.gif
./doc/HTML/avetcp_sample/images/UDP_DISCARD_TEST_Screen_Loopback.gif
./doc/HTML/avetcp_sample/wwhdata/common/context.js
./doc/HTML/avetcp_sample/wwhdata/common/files.js
./doc/HTML/avetcp_sample/wwhdata/common/popups.js
./doc/HTML/avetcp_sample/wwhdata/common/title.js
./doc/HTML/avetcp_sample/wwhdata/common/topics.js
./doc/HTML/avetcp_sample/wwhdata/common/towwhdir.js
./doc/HTML/avetcp_sample/wwhdata/common/wwhpagef.js
./doc/HTML/avetcp_sample/wwhdata/java/files.xml
./doc/HTML/avetcp_sample/wwhdata/java/ix.xml
./doc/HTML/avetcp_sample/wwhdata/java/search.xml
./doc/HTML/avetcp_sample/wwhdata/java/toc.xml
./doc/HTML/avetcp_sample/wwhdata/js/index.js
./doc/HTML/avetcp_sample/wwhdata/js/search/search0.js
./doc/HTML/avetcp_sample/wwhdata/js/search/search1.js
./doc/HTML/avetcp_sample/wwhdata/js/search/search2.js
./doc/HTML/avetcp_sample/wwhdata/js/search/search3.js
./doc/HTML/avetcp_sample/wwhdata/js/search/search4.js
./doc/HTML/avetcp_sample/wwhdata/js/search/search5.js
./doc/HTML/avetcp_sample/wwhdata/js/search/search6.js
./doc/HTML/avetcp_sample/wwhdata/js/search.js
./doc/HTML/avetcp_sample/wwhdata/js/toc.js
./doc/HTML/avetcp_sample/wwhelp/books.xml
./doc/HTML/avetcp_sample/wwhelp/images/altclose.gif
./doc/HTML/avetcp_sample/wwhelp/images/altopen.gif
./doc/HTML/avetcp_sample/wwhelp/images/GCNLogo.jpg
./doc/HTML/avetcp_sample/wwhelp/messages.xml
./doc/HTML/avetcp_sample/wwhelp/settings.xml
./doc/HTML/avetcp_sample/wwhelp/wwhimpl/
./doc/HTML/images/GCNLogo.jpg
./doc/HTML/images/getacro.gif
./doc/HTML/images/html.gif
./doc/HTML/images/ninlogo.gif
./doc/HTML/images/pdf.gif
./doc/HTML/index.html
./doc/index.html
./doc/PDF/ATGC_Sample.us.pdf
./doc/PDF/AveTcpGC.us.pdf
./include/avetcp.h
./include/common/at_api.h
./include/common/at_defs.h
./include/common/at_dlapi.h
./include/common/at_dns.h
./include/common/avedhcp.h
./include/ppp/common/ppp_api.h
./include/target/at_cfg.h
./include/target/at_debug.h
./include/target/at_end.h
./include/target/at_modul.h
./include/target/at_prm.h
./include/target/at_rtos.h
./include/target/at_stdlib.h
./include/target/at_types.h
./lib/avetcp.a
./lib/avetcpD.a

=======================================================================
If you have any technical questions concerning the Network SDK, post on
the SDSG news groups (news.sdsg.nintendo.com) or contact us directly at
support@noa.com.
=======================================================================

