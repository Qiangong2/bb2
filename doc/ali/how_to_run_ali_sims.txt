Get the ALi design files.  The ALI_3357_SIM_ENV_REL_1 label
corresponds to the first code release we received from ALi.

   % cvs checkout -r ALI_3357_SIM_ENV_REL_1 hw/lib/ali

To run a sim,

   % setenv design_root <path to ali directory, e.g., ~/bb2/hw/lib/ali>

   % cd $design_root/m3357/work/carcy/rsim

   % run n verilog/cpu/cpu_rw_mem001 s

VCS will give some warnings because I haven�t translated the NCVerilog
command-line arguments; however, the simulations still seem to work
fine.

Here are the other tests that I�m aware of --

   % run nu verilog/pci/pci_sdram_p7 s

   % run nu verilog/pci/pci_sdram_p1 s

   % run nu verilog/pci/cpu2tt_cfg s

   % run nu verilog/usb/test_mem s

   % run nu verilog/usb/ctrl_td s

If you�re interested in seeing the actual test code, take a look in
the subdirectories of $design_root/m3357/rsim/pattern/rtl/verilog.

By the way, none of these tests involve executing any MIPS code.  ALI
hasn�t given us any model of the MIPS yet.  The MIPS is stubbed out
and the tests simply generate read/write requests directly on the
cpu�s bus interface.


Phil Smith
psmith@broadon.com
14Jan04
