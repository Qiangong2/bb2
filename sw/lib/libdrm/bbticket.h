/**********************************************
 *
 * bbticket.h
 *
 * definition of a small ticket. The HMAC of this data structure is 
 * called the activation code. 
 *
 **********************************************/

#ifndef __BB_TICKET_H
#define __BB_TICKET_H

#include <bbtypes.h>


/************************************************
 * header file of the ticket, whose HMAC is the activation code. 
 * the ticket itself is not stored or transmitted through the system.
 * it is computed, and then its HMAC is transmitted, stored and verified.
 ***********************************************/ 
typedef struct {

    u32 bbId;			/* identity of player */
    u32 contentID;          	/* purchased content */	
    u32 tsCrlVersion;       	/* ticket server CRL number */
    
    u32 licenseTableIndex;  	/* index to allowed license type 
			     	 * (see bbmetadata.h)
				 */
    u32 ticketID;		/* serial number of ticket issued to
				 * the same player, used for 
				 * rentals
				 */
    BbServerName issuer; 	/* this name is used to look up the cert chain
				 */
} BbTicket;


#endif 
