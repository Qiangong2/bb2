#ifndef __BB_EMBFLASH_H__
#define __BB_EMBFLASH_H__


#include <bbtypes.h>
#include <bbsec.h>

/* 
 * This is the data structure written to embedded flash. We will assume a 
 * 2K Embedded Flash for now, change it later if necessary. The first part
 * is the Readonly portion, BbEmbFlashRO, the second data structure is the 
 * mirrored read-write(8 bytes), BbEmbFlashRW.
 * There will be two copies of the RW portion for redundancy 
 * so the size of the readonly portion is 256 - 16 bytes = 240 bytes.
 * The ROM PATCH is assigned the extra space for now, whatever is left
 * over after the keys/hash/id etc.
 */
#define READ_ONLY_BYTES 240

#define ROM_PATCH_AREA_BYTES \
         ( (READ_ONLY_BYTES) - \
           (sizeof(BbShaHash) + sizeof(BbId) + \
	    sizeof(BbEccPrivateKey) + sizeof(BbAesKey)*3 + \
            sizeof(u32)*2 ) )

typedef struct {
    BbShaHash skHash;		/* sha1 hash of secure kernel: fixed 
				 * 0xf0f0f0f0 for now
				 */
    u32 romPatch[ROM_PATCH_AREA_BYTES/4]; 
				/* patch code area for bootrom */
    BbId bbId;			/* player's identity: unique serial number */
    BbEccPrivateKey privateKey;	/* player's private key: from ECC key pair */
   
    BbAesKey bootAppKey;	/* decryption key for boot application 
				 * license: fixed
				 * 0x84983e36 0x4706816a 0xba3e2571 0x7850c26c
				 */

    /*
     * The following is extra random data from the server to aid RNG,
     * to keep any encrypted data in external flash etc. 
     * Or, the two together can be used as an application private key.
     */

    BbAesKey appStateKey;	/* key for state saving */
    BbAesKey selfMsgKey;	/* key for messages to self */
    u32      csumAdjust;        /* checksum adjustment. used to insure checksum
                                 * over BbEmbFlashRW  
				 * for now make it 0x66666666
				 */
    u32      jtagEnable;        /* controls jtag for now =0x00000000
				 * enable =0xbb2004fb 
				 */
} BbEmbFlashRO;  		/* read only portion */



/*
 * Structure containing portion of embedded flash data which is .
 * Data is ping-ponged between two copies using a version number
 * and checksum to indicate which one is current.
 */
typedef struct {
    u8 tsCrlVersion;	/* largest seen ticket server CRL version number */
    u8 caCrlVersion;	/* largest seen CA  CRL version number */
    u8 cpCrlVersion;    /* content publishing CRL version number */
    u8 seq;   		/* sequence number to know which of two copies is
			 * later 
			 */
    u16 versionExternal;/* version number of external data structure, 
			 * signed and encrypted
			 */
    u16 sum;		/* checksum of this structure */
#define BB_EMBFLASHRW_CKSUM       0x7ad /* value cksum should produce */
} BbEmbFlashRW;		/* read write portion */

#endif /*__BB_EMBFLASH_H__*/


