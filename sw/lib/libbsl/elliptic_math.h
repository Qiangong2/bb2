/* 
 * elliptic_math.h
 * elliptic curve operations
 * only deals with polynomial basis and support for 
 * DH and ECDSA
 * equation: y^2 + xy = x^3 + a x^2 + b
 * no point compression
*/


#include "binary_field.h"

#ifndef ELLIPTIC_MATH_H
#define ELLIPTIC_MATH_H

typedef struct {
	short int form;
	field_2n a2;
	field_2n a6;
}curve;


typedef struct {
	field_2n x;
	field_2n y;
}point;

void poly_elliptic_init_233_bit();
void copy_point(point *p1, point *p2);
void print_field(char *string, field_2n *field);
void print_point(char *string, point *point);

void poly_elliptic_sum(point *p1, point *p2, point *p3, curve *curv);
void poly_elliptic_double(point *p1, point *p3, curve *curv);
void poly_elliptic_sub(point *p1, point *p2, point *p3, curve *curv);
/* this is generic, uses window = 2, caches precomputed values */
void poly_elliptic_mul(field_2n *k, point *p, point *r, curve *curv);
/* fast version, use only if the point is base point. window = 4 */
void poly_elliptic_mul_four(field_2n *k, point *p, point *r, curve *curv);
/* generic version without precomputation, binary NAF */
void poly_elliptic_mul_slow(field_2n *k, point *p, point *r, curve *curv);
#endif
