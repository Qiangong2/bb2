/* 
 *
 * algorithms.h
 * algorithm prototypes.
 *
 */
#include <bbsec.h>
#include "bsl.h"
#include "binary_field.h"
#include "poly_math.h"
#include "elliptic_math.h"

#ifndef ALGORITHMS_H
#define ALGORITHMS_H


typedef struct {
  field_2n c;
  field_2n d;
}bsl_signature;


typedef struct {
  field_2n private_key;
  point public_key;
}ec_keypair;

typedef struct {
  curve par_curve;
  point par_point;
  field_2n point_order;
  field_2n cofactor;
}ec_parameter;
  

void _init_233_bit_ECDSA(ec_parameter *base, int num_bits);

BSL_error _poly_key_gen_primitive(ec_parameter *Base, ec_keypair *key, unsigned long *random_input);

BSL_error _poly_ECDSA_signature(char *message, unsigned long length, ec_parameter *public_curve, field_2n *private_key, bsl_signature *signature, unsigned long *rand_input, u32 identity);

BSL_error _poly_ECDSA_verify(char *message, unsigned long length, ec_parameter *public_curve, point *signer_point, bsl_signature *signature, field_boolean *result, u32 identity);


BSL_error _generate_public_key(point *base_point, curve *E, field_2n *my_private, point *my_public);


BSL_error _generate_shared_key(point *base_point, curve *E, point *recipient_public, field_2n *my_private, field_2n *shared_secret);
#endif
