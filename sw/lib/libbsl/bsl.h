/* 
 *
 * bsl.h
 * BB Security Library crypto algorithm prototypes.
 *
 */
#include <bbsec.h>
#include <binary_field.h>
#include <sha1.h>


#ifndef BSL_H
#define BSL_H

#define BSL_SHA1_BLOCKSIZE		64
#define BSL_SHA1_DIGESTSIZE		20

#define BSL_AES_BLOCKSIZE_BYTES         16
#define BSL_AES_KEYSIZE_BYTES		16
#define BSL_AES_IVSIZE_BYTES 		16

/*errors */

typedef enum { BSL_OK = 0, 
               BSL_OVERFLOW, 
               BSL_DIVIDE_BY_ZERO, 
               BSL_SHA_ERROR,
	       BSL_AES_ERROR,
               BSL_BAD_KEY,
               BSL_NULL_POINTER,
	       BSL_VERIFY_ERROR
} BSL_error;



typedef SHA1Context BbShaContext;

BSL_error bsl_sha_reset(BbShaContext* ctx);
BSL_error bsl_sha_input(BbShaContext* ctx, const u8 *buf, int len);
BSL_error bsl_sha_result(BbShaContext* ctx, u8 digest[BSL_SHA1_DIGESTSIZE]);


/*
 * AES calls 
 */


/* aes key expansion for use in setting expanded key for hardware decryption.
 * arguments
 *   key: input, 128 bit key
 *  expkey: output, 44*4 byte expanded key
 *  returns = 0 on success, BSL_AES_ERROR on error
*/
BSL_error bsl_aes_keyexpand(u8 *key,u8 *expkey);

/*
 * aes encryption given the 128 bit key (NOT the expanded key).

 *arguments
 *  key: input, 128 bit key
 *  initVector: input, 128 bit initialization vector
 *  dataIn: input, unencrypted data
 *  bytes: input, number of bytes to be encrypted, must be multiple of 16.
 *  dataOut: output, encrypted data

 * returns 0  on success, BSL_AES_ERROR on error
*/
BSL_error bsl_aes_sw_encrypt(u8 *key,u8 *initVector,u8 *dataIn,
			u32 bytes,u8 *dataOut);

/*
 * aes decryption given the 128 bit key (NOT the expanded key).

 * arguments
 *  key: input, 128 bit key
 *   initVector: input, 128 bit initialization vector
 *  dataIn: input, encrypted data
 *  bytes: input, number of bytes to be decrypted, must be multiple of 16.
 *  dataOut: output, decrypted data

 * returns 0 on success, returns BSL_AES_ERROR on error
*/
BSL_error bsl_aes_sw_decrypt(u8 *key,u8 *initVector,u8 *dataIn,
			u32 bytes,u8 *dataOut);


/* 
 * highest level calls for Elliptic curve operations
 */


BSL_error bsl_ecc_gen_shared_key(BbEccPublicKey publicKey, 
		BbEccPrivateKey privateKey, 
		BbAesKey sharedKey);

BSL_error bsl_ecc_gen_public_key(BbEccPublicKey publicKey, 
		BbEccPrivateKey privateKey);

BSL_error bsl_ecc_compute_sig(u8 *data, 
		u32 datasize, BbEccPrivateKey private_key, 
		u32 *random_data, BbEccSig sign, 
		u32 identity);

BSL_error bsl_ecc_verify_sig( u8 *data, u32 datasize, 
		BbEccPublicKey public_key, BbEccSig sign,
		u32 identity);

/*
 * highest level calls for RSA functions
 */

BSL_error bsl_rsa_verify(u8 *hash, u32 *certsign, 
		u32 *certpublickey, u32 *exponent,  
		int num_bits);

void bsl_rsa_sign(u8 *result, u32 *message, 
		u32 *certpublickey, u32 *secretexponent,  
		int num_bits);

void bsl_rsa_sign_fast(u8 *result, u32 *message, 
		u32 *certpublickey, u32 *certp, 
		u32 *certq, u32 *dmp, u32 *dmq, 
		u32 *qinv,  int num_bits);

void bsl_hmac_sha1(u8 *text, u32 text_length, 
		u8 *key, u32 key_length, 
		u8 *hmac);

#endif
