/*
 * highest level BB Security Library algorithms
 */


#include "algorithms.h"
#include "elliptic_math.h"
#include "algorithms.h"
#include "bsl.h"
#include "aes.h"
#include "sha1.h"

#include "integer_math.h"
#include <clibutil.h>

extern void bcopy();
extern void bzero();

/* set of AES functions, wrapped to BSL conventions 
 */


BSL_error bsl_aes_keyexpand(u8 *key,u8 *expkey){
	if(aes_HwKeyExpand(key, expkey) >=0 ){
		return BSL_OK;
	}
	else return BSL_AES_ERROR;
}

BSL_error bsl_aes_sw_encrypt(u8 *key,u8 *initVector,u8 *dataIn,
	u32 bytes,u8 *dataOut){
	if(aes_SwEncrypt(key, initVector, dataIn, bytes, dataOut) >= 0){
		return BSL_OK;
	}
	else return BSL_AES_ERROR;
}

BSL_error bsl_aes_sw_decrypt(u8 *key,u8 *initVector,u8 *dataIn,
			u32 bytes,u8 *dataOut){
	if(aes_SwDecrypt(key, initVector, dataIn, bytes, dataOut) >= 0){
		return BSL_OK;
  	}
	else return BSL_AES_ERROR;
}



/* 
 * set of calls to wrap around SHA calls: only return zeros
 */


BSL_error bsl_sha_reset(BbShaContext* ctx){
	return SHA1Reset((SHA1Context *) ctx);
}

BSL_error bsl_sha_input(BbShaContext* ctx, const u8 *buf, int len){
	return SHA1Input((SHA1Context * )ctx, buf, len);
}

BSL_error bsl_sha_result(BbShaContext* ctx, u8 digest[BSL_SHA1_DIGESTSIZE]){
	return SHA1Result((SHA1Context *)ctx, digest);
}


extern curve named_curve;
extern point named_point;

/* high level wrapper call to get the AES key from public and private keys
 */

BSL_error
bsl_ecc_gen_shared_key(BbEccPublicKey publicKey, BbEccPrivateKey privateKey, 
		BbAesKey sharedKey){
    field_2n pvtkey, sharedkey;
    point publickey;
    BSL_error error = BSL_OK;
    
    int i;
    
    poly_elliptic_init_233_bit();
    /* convert private key to field2n variable */
    for(i = 0; i < MAX_LONG; i++){
        pvtkey.e[i] = privateKey[i];
    }
    pvtkey.e[0] &= UPR_MASK;
    /* convert public key to point variable */
    for(i = 0; i < MAX_LONG; i++){
        publickey.x.e[i] = publicKey[i];
    }
    for(i = 0; i < MAX_LONG; i++){
        publickey.y.e[i] = publicKey[i + MAX_LONG];
    }
 
    /* call it !*/
    error = _generate_shared_key(&named_point, &named_curve, &publickey, &pvtkey, &sharedkey);

    /* write back to AES key variable : arbitrary: pick from {1..4}*/
    sharedKey[0] = sharedkey.e[1];
    sharedKey[1] = sharedkey.e[2];
    sharedKey[2] = sharedkey.e[3];
    sharedKey[3] = sharedkey.e[4];

    return BSL_OK;
}

    
/* high level wrapper call to get the AES key from public and private keys
 */

BSL_error
bsl_ecc_gen_public_key(BbEccPublicKey publicKey, BbEccPrivateKey privateKey){
    field_2n pvtkey;
    point publickey;
    BSL_error error = BSL_OK;
    
    int i;
    
    poly_elliptic_init_233_bit();
    /* convert private key to field2n variable */
    for(i = 0; i < MAX_LONG; i++){
        pvtkey.e[i] = privateKey[i];
    }
    pvtkey.e[0] &= UPR_MASK;
     
    /* call it !*/
    error = _generate_public_key(&named_point, &named_curve, &pvtkey, &publickey);

    /* write back to return array 
     */
    for(i = 0; i < MAX_LONG; i++){
        publicKey[i] = publickey.x.e[i];
    }
    for(i = 0; i < MAX_LONG; i++){
        publicKey[i + MAX_LONG] = publickey.y.e[i];
    }
    return error;
}
    



/* high level call for signing a blob using ECDSA
 * call with 256 bits of random data, the private key and the data blob
 */

BSL_error
bsl_ecc_compute_sig(u8 *data, u32 datasize, BbEccPrivateKey private_key, u32 *random_data, BbEccSig sign, u32 identity){
    ec_parameter base;
    bsl_signature signature;
    field_2n pvtkey;
    BSL_error ret;
    int i;
	

    _init_233_bit_ECDSA(&base, NUM_BITS);
    /* make sure private key is 233 bit */
    for(i = 0; i < MAX_LONG; i++){
      pvtkey.e[i] = private_key[i];
    }
    pvtkey.e[0] &= UPR_MASK;

    ret = _poly_ECDSA_signature(data, datasize, &base, &pvtkey, &signature, random_data, identity);
    if(ret == BSL_OK){
      copy(&(signature.c), (field_2n *)sign);
      copy(&(signature.d), (field_2n *)(sign + MAX_LONG));
      return BSL_OK;
    }
    
    return BSL_DIVIDE_BY_ZERO;
    
}


/* there is no error condition, result is in res, returns BSL_TRUE if
 * succeeds 
 */
BSL_error
bsl_ecc_verify_sig(u8 *data, u32 datasize, BbEccPublicKey public_key, BbEccSig sign, u32 identity){
    ec_parameter base;
    bsl_signature signature;
    field_boolean res;
    
    _init_233_bit_ECDSA(&base, NUM_BITS);
    
    copy((field_2n *)sign, &(signature.c));
    copy((field_2n *)(sign + MAX_LONG), &(signature.d));

    _poly_ECDSA_verify(data, datasize, &base, (point *) public_key, &signature, &res, identity);
    if(res == BSL_TRUE){
    	return BSL_OK;
    }
    else{
	return BSL_VERIFY_ERROR;
    }

}
    
    
/* high level RSA calls
 */

/* returns BSL_OK or BSL_VERIFY_ERROR 
*/

BSL_error
bsl_rsa_verify(u8 *hash, u32 *certsign, u32 *certpublickey, u32 *certexponent,  int num_bits){
 /* grab from cert */
    u32 eDigits, nDigits;
    u8 result[MAX_BIGINT_DIGITS * 4];
    bigint_digit bign[MAX_BIGINT_DIGITS];
    bigint_digit bigm[MAX_BIGINT_DIGITS];
    bigint_digit bige[MAX_BIGINT_DIGITS];
    bigint_digit  bigc[MAX_BIGINT_DIGITS];
    int outlen;
    int i;
    int num_words = num_bits/BIGINT_DIGIT_BITS;
	

    for(i=0; i< MAX_BIGINT_DIGITS; i++){
        bign[i] = 0;
        bigm[i] = 0;
        bige[i] = 0;
        bigc[i] = 0;
    }
    
    for(i =0; i < num_words; i++){
        bign[num_words -1 -i] = certpublickey[i];
        bigm[num_words -1 -i] = certsign[i];
    }
    bige[0] = *certexponent;
    nDigits = bigint_digits(bign, MAX_BIGINT_DIGITS);
    eDigits = 1;
    
                
    bigint_mod_exp(bigc, bigm, bige, eDigits, bign, nDigits);
    
    outlen = (num_bits + 7)/8;
    bigint_encode(result, outlen, bigc, nDigits);
	
    if(memcmp(result + (num_bits/8) - 20, hash, 20) == 0){
	return BSL_OK;
    }
    else{
	return BSL_VERIFY_ERROR;
    }

    
}


/* message should be hashed, padded as per PKCS-1 type 2
 * before this function is called 
 */

void
bsl_rsa_sign(u8 *result, u32 *message, u32 *certpublickey, u32 *secretexponent,  int num_bits){
 /* grab from cert */
    u32 dDigits, nDigits;
    bigint_digit bign[MAX_BIGINT_DIGITS];
    bigint_digit bigm[MAX_BIGINT_DIGITS];
    bigint_digit bigd[MAX_BIGINT_DIGITS]; /* secret exp */
    bigint_digit  bigc[MAX_BIGINT_DIGITS];
    int outlen;
    int i;
    int num_words = num_bits/BIGINT_DIGIT_BITS;

    for(i=0; i< MAX_BIGINT_DIGITS; i++){
        bign[i] = 0;
        bigm[i] = 0;
        bigd[i] = 0;
        bigc[i] = 0;
    }
    
    for(i =0; i < num_words; i++){
        bign[num_words - 1 - i] = certpublickey[i];
        bigm[num_words - 1 - i] = message[i];
	bigd[num_words - 1 - i] = secretexponent[i];
    }

    nDigits = bigint_digits(bign, MAX_BIGINT_DIGITS);
    dDigits = bigint_digits(bigd, MAX_BIGINT_DIGITS);
    
                
    bigint_mod_exp(bigc, bigm, bigd, dDigits, bign, nDigits);
    
    outlen = (num_bits + 7)/8;
    bigint_encode(result, outlen, bigc, nDigits);
    
}



/* using Garners algorithm and Chinese Remainder theorem. 
 * see the derivation on page 613 in handbook of applied cryptography
 * use factors dp and dq of secret exponent d and p and q of public 
 * exponent n, and qinv
 */
void
bsl_rsa_sign_fast(u8 *result, u32 *message, u32 *certpublickey, u32 *certp, u32 *certq, u32 *dmp, u32 *dmq, u32 *qinv,  int num_bits){
    u32 pDigits, qDigits, dmpDigits, dmqDigits, cDigits, nDigits, qinvDigits;
    bigint_digit bigp[MAX_BIGINT_DIGITS];
    bigint_digit bigq[MAX_BIGINT_DIGITS];
    bigint_digit  bigc[MAX_BIGINT_DIGITS];
    bigint_digit bigdmp[MAX_BIGINT_DIGITS];
    bigint_digit bigdmq[MAX_BIGINT_DIGITS]; 
    bigint_digit bigqinv[MAX_BIGINT_DIGITS];
    bigint_digit bign[MAX_BIGINT_DIGITS];
    bigint_digit cP[MAX_BIGINT_DIGITS];
    bigint_digit cQ[MAX_BIGINT_DIGITS];
    bigint_digit mP[MAX_BIGINT_DIGITS];
    bigint_digit mQ[MAX_BIGINT_DIGITS];
    bigint_digit temp[MAX_BIGINT_DIGITS];

    int outlen;
    int i;
    int num_words = num_bits/BIGINT_DIGIT_BITS;

    for(i=0; i< MAX_BIGINT_DIGITS; i++){
        bigp[i] = 0;
        bigq[i] = 0;
        bigdmp[i] = 0;
        bigdmq[i] = 0;
	bigqinv[i] = 0;
	bigc[i] = 0;
    }
    
    for(i =0; i < num_words/2; i++){
        bigp[num_words/2 - 1 - i] = certp[i];
	bigq[num_words/2 - 1 - i] = certq[i];
	bigdmp[num_words/2 - 1 - i] = dmp[i];
	bigdmq[num_words/2 - 1 - i] = dmq[i];
	bigqinv[num_words/2 - 1 - i] = qinv[i];
    }
    for(i =0; i < num_words; i++){
	bigc[num_words - 1 - i] = message[i];
	bign[num_words - 1 - i] = certpublickey[i];
    }
    cDigits = bigint_digits(bigc, MAX_BIGINT_DIGITS);
    pDigits = bigint_digits(bigp, MAX_BIGINT_DIGITS);
    qDigits = bigint_digits(bigq, MAX_BIGINT_DIGITS);
    dmpDigits = bigint_digits(bigdmp, MAX_BIGINT_DIGITS);
    dmqDigits = bigint_digits(bigdmq, MAX_BIGINT_DIGITS);
    qinvDigits = bigint_digits(bigqinv, MAX_BIGINT_DIGITS);
    nDigits = bigint_digits(bign, MAX_BIGINT_DIGITS);
    
    /* compute cP and cQ 
     */
    
    bigint_mod(cP, bigc, cDigits, bigp, pDigits);
    bigint_mod(cQ, bigc, cDigits, bigq, qDigits);

    /*
     * Compute mP = cP^dP mod p  and  mQ = cQ^dQ mod q. 
     */
    bigint_mod_exp(mP, cP, bigdmp, pDigits, bigp, pDigits);
    bigint_zero(mQ, nDigits);
    bigint_mod_exp(mQ, cQ, bigdmq, pDigits, bigq, pDigits);

    /* do CRT 
     * m = ((((mP - mQ) mod p)*qinv) mod p) *q + mQ
     */
    if(bigint_cmp(mP, mQ, pDigits) >=0)
      bigint_sub(temp, mP, mQ, pDigits);

    else{
      bigint_sub(temp, mQ, mP, pDigits);
      bigint_sub(temp, bigp, temp, pDigits);
    }
    
    bigint_mod_mult(temp, temp, bigqinv, bigp, pDigits);
    bigint_mult(temp, temp, bigq, pDigits);
    bigint_add(temp, temp, mQ, nDigits);
      
    outlen = (num_bits + 7)/8;
    bigint_encode(result, outlen, temp, nDigits);
    
}


/* HMAC needs key material of length 20 bytes to support HMAC-SHA1-80
 * and the result is truncated outside
 */

void
bsl_hmac_sha1(u8 *text, u32 text_length, 
		u8 *key, u32 key_length, u8 *hmac){
	SHA1Context sha;
	u8 k_ipad[65]; /* inner padding */
	u8 k_opad[65]; /* outer padding */

	u8 tk[16];
	
	int i;
	
	/* if key is longer than 64 bytes set it to key = SHA1(key)
	*/
	if(key_length > 64){
		SHA1Reset(&sha);
    		SHA1Input(&sha, key, key_length);
    		SHA1Result(&sha, tk);
		key = tk;
		key_length = 20;
	}
	/* the HMAC = SHA1(K xor opad, SHA1(K xor ipad, text))
	*/
	bzero(k_ipad, sizeof(k_ipad));
	bzero(k_opad, sizeof(k_opad));
	bcopy(key, k_ipad, key_length);
	bcopy(key, k_opad, key_length);

	/* xor key with ipad and opad values */
	for(i=0; i < 64; i++){
		k_ipad[i] ^= 0x36;
		k_opad[i] ^= 0x5c;
	}
	
	/* perform inner SHA1 */
	SHA1Reset(&sha);
	SHA1Input(&sha, k_ipad, 64);
	SHA1Input(&sha, text, text_length);
	SHA1Result(&sha, hmac);
	
	/* perform outer SHA1 */
	SHA1Reset(&sha);
	SHA1Input(&sha, k_opad, 64);
	SHA1Input(&sha, hmac, 16);
	SHA1Result(&sha, hmac);

	/* truncate outside */

}

