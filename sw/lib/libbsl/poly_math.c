/* poly_math.c
 * implementation of GF 2^m arithmetic needed for elliptic math
 * field_2n poly_prime = {0x00080000,0x00000000,0x00000000,
 * 0x00000000,0x00000000,0x00000400, 0x00000000, 0x00000001};  
 * 233 bit u^233 + u^74 + 1
 * Reference:
 * all these routines are derived from the method in "fast key 
 * exchange with elliptic curve systems" by schroeppel, orman, malley
 * and the partial multiplication, is the LR window method surveyed 
 * in "software implementation of elliptic curve crypt.
 * over binary fields" hankerson, hernandez, menezes
 * 
 */

#include "integer_math.h"
#include "binary_field.h"
#include "poly_math.h"

const field_2n poly_prime = {{0x00000200,0x00000000,0x00000000,
			0x00000000,0x00000000,0x00000400, 
			0x00000000, 0x00000001}};  
/* needed for the shift */
const element rightmask[32] = {0x0, 0x1, 0x3, 0x7,
			 0xf, 0x1f, 0x3f, 0x7f,
			 0xff, 0x1ff, 0x3ff, 0x7ff,
			 0xfff, 0x1fff, 0x3fff, 0x7fff,
			 0xffff, 0x1ffff, 0x3ffff, 0x7ffff,
			 0xfffff, 0x1fffff, 0x3fffff, 0x7fffff,
			 0xffffff, 0x1ffffff, 0x3ffffff, 0x7ffffff,
			 0xfffffff, 0x1fffffff, 0x3fffffff, 0x7fffffff};

const element leftmask[32] = {0x00000000, 0x80000000, 0xc0000000, 0xe0000000, 
			0xf0000000, 0xf8000000, 0xfc000000, 0xfe000000, 
			0xff000000, 0xff800000, 0xffc00000, 0xffe00000, 
			0xfff00000, 0xfff80000, 0xfffc0000, 0xfffe0000, 
			0xffff0000, 0xffff8000, 0xffffc000, 0xffffe000, 
			0xfffff000, 0xfffff800, 0xfffffc00, 0xfffffe00, 
			0xffffff00, 0xffffff80, 0xffffffc0, 0xffffffe0, 
			0xfffffff0, 0xfffffff8, 0xfffffffc, 0xfffffffe};
const unsigned char shift_by[256] = {0x08,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x03,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x04,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x03,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x05,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x03,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x04,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x03,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x06,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x03,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x04,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x03,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x05,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x03,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x04,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x03,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x07,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x03,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x04,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x03,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x05,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x03,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x04,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x03,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x06,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x03,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x04,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x03,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x05,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x03,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x04,0x00,0x01,0x00,0x02,0x00,0x01,0x00, 
			       0x03,0x00,0x01,0x00,0x02,0x00,0x01,0x00};
 


/*
 * void 
 * null()
 * Purpose : null a field_2n variable
 * input: pointer to allocated field_2n, can do in place
 * 
 */

void 
null(field_2n *a)
{
  short int i;
  element *eptr;
  eptr = &(a->e[0]);
  for(i =0; i < MAX_LONG; i++){
    /*
      a->e[i] = 0L;
    */
    *eptr = 0L;
    eptr++;
  }
  return;
}


/*
 * void 
 * double_null()
 * Purpose : null a double variable
 * input: pointer to allocated double, can do in place
 * 
 */

void
double_null(field_double *a)
{
  short int i;
  element *eptr;
  eptr = &(a->e[0]);
  for(i =0; i< MAX_DOUBLE; i++){
    /*
      a->e[i] = 0L;
    */
    *eptr = 0L;
    eptr++;
  }
  return;
}



/*
 * void
 * double_add()
 * Purpose : add double variable
 * input: pointer to allocated doubles, cannot do in place
 * 
 */

void
double_add(field_double *a, field_double *b, field_double *c)
{
  short int i;
  element *aptr;
  element *bptr;
  element *cptr;
  aptr = &(a->e[0]);
  bptr = &(b->e[0]);
  cptr = &(c->e[0]);

  for(i =0; i< MAX_DOUBLE; i++){
    /*
      c->e[i] = a->e[i] ^ b->e[i];
    */
    *cptr = *aptr ^ *bptr;
    aptr++;
    bptr++;
    cptr++;
  }
  return;
}


/*
 * void 
 * copy()
 * Purpose : copy field_2n variables
 * input: pointer to allocated field_2n, cannot do in place
 * 
 */

void
copy(const field_2n *from, field_2n *to)
{
  short int i;
  const element *fromptr;
  element *toptr;
  fromptr = &(from->e[0]);
  toptr = &(to->e[0]);

  for(i =0 ; i < MAX_LONG; i++){
    /*
      to->e[i] = from->e[i];
    */
    *toptr = *fromptr;
    toptr++;
    fromptr++;
  }
  return;
}



/*
 * void 
 * double_copy()
 * Purpose : copy double variables
 * input: pointers to allocated double, cannot do in place
 * 
 */

void 
double_copy(field_double *from, field_double *to)
{
  short int i;
  element *fromptr;
  element *toptr;
  fromptr = &(from->e[0]);
  toptr = &(to->e[0]);

  for(i =0 ; i < MAX_DOUBLE; i++){
    /*
      to->e[i] = from->e[i];
    */
    *toptr = *fromptr;
    toptr++;
    fromptr++;
  }
  return;
}

/*
 * void 
 * single_to_double()
 * Purpose : copy single to double
 * input: pointers to allocated double, cannot do in place
 * 
 */

void 
single_to_double(field_2n *from, field_double *to)
{
  short int i;
    
  double_null(to);
  for(i =0 ; i < MAX_LONG; i++){
    to->e[DOUBLE_WORD - NUM_WORD + i] = from->e[i];
  }
  return;
}


/*
 * void 
 * double_to_single()
 * Purpose : copy bottom of double to single
 * input: pointers to allocated double, cannot do in place
 * 
 */
void 
double_to_single(field_double *from, field_2n *to)
{
  short int i;
    
  for(i =0; i <MAX_LONG; i++){
    to->e[i] = from->e[DOUBLE_WORD - NUM_WORD + i];
  }
  return;
}

/*
 * void
 * multiply_shift()
 * Purpose : shift one bit to do partial multiply: double field.
 * input: pointer to allocated field_double, can do in place
 * 
 */


void 
multiply_shift(field_double *a)
{
  element *eptr, temp, bit;
  short int i;
    
  /* this is bigendian representation
   */
  eptr = &a->e[DOUBLE_WORD];
  bit =0;
  
  for(i =0; i< MAX_DOUBLE; i++){
    temp = (*eptr << 1) | bit;
    bit = (*eptr & MSB) ? 1L: 0L;
    *eptr-- = temp;
  }
  return;
}



#ifdef OLD_IMP
/*
 * void 
 * poly_mul_partial()
 * Purpose : multiply two field_2n variables and obtain double
 * input: pointers to allocated field_2n and double
 * this is shift and add obsoleted by L-R with window since
 * that is faster
 * 
 */
void 
poly_mul_partial(field_2n *a, field_2n *b, field_double *c)
{
  short int i, bit_count, word;
  element mask;
  field_double local_b;
    
  /*clear bits in the result */
  double_null(c);
  /* initialize local b */
  single_to_double(b, &local_b);

  /*
   * for every bit in a that is set, add local_b to the
   * result 
   */
  mask = 1;
  for(bit_count =0; bit_count < NUM_BITS; bit_count++){
    word = NUM_WORD - bit_count / WORD_SIZE;
    if(mask & a->e[word]){
      for(i =0; i< MAX_DOUBLE; i++){
	c->e[i] ^= local_b.e[i];
      }
    }
    multiply_shift(&local_b);
    mask <<= 1;
    if (!mask) mask = 1; /*re-initialise if zero */
  }
  return;
}
#endif

/* this is optimised partial polynomial multiplication, 
 * L-R algorithm with precomputation of 4 bit wide multipliers 
 */

void 
poly_mul_partial(field_2n *a, field_2n *b, field_double *c)
{
  short int i, bit_count, word;
  element mask;
  field_double local_b;
  int k, num_shift;
  int b_start;
  element *eptr, temp, bit;
  element *local_bptr;
  element *cptr;
  field_double bprep[16]; /* precompute b multiples from 0-15 */
  element multiplier;
  
  /* precompute 2^window size -1 multiples*/
  
  /* hand code the 16 values, otherwise its too costly */
  for(i =0; i< MAX_DOUBLE; i++){
    bprep[0].e[i] =0;
  }
  single_to_double(b, &bprep[1]);
  for(i=0; i< MAX_DOUBLE; i++){
    local_b.e[i] = bprep[1].e[i];
  }
  multiply_shift(&local_b);
  for(i=0; i< MAX_DOUBLE; i++){
    bprep[2].e[i] = local_b.e[i];
  }
  multiply_shift(&local_b);
  for(i=0; i< MAX_DOUBLE; i++){
    bprep[4].e[i] = local_b.e[i];
  }
  multiply_shift(&local_b);
  for(i=0; i< MAX_DOUBLE; i++){
    bprep[8].e[i] = local_b.e[i];
  }

  /* 3 = 2+ 1 */
  for(i=0; i < MAX_DOUBLE; i++){
    bprep[3].e[i] = bprep[2].e[i] ^ bprep[1].e[i]; 
    /* 5 = 4+ 1 */  
    bprep[5].e[i] = bprep[4].e[i] ^ bprep[1].e[i];  
    /* 6 = 4 + 2 */ 
    bprep[6].e[i] = bprep[4].e[i] ^ bprep[2].e[i];  
    /* 7 = 6 + 1 */  
    bprep[7].e[i] = bprep[6].e[i] ^ bprep[1].e[i];  
    /* 9 = 8 + 1 */ 
    bprep[9].e[i] = bprep[8].e[i] ^ bprep[1].e[i];
    /* 10 = 8 + 2 */ 
    bprep[10].e[i] = bprep[8].e[i] ^ bprep[2].e[i];
    /* 11 = 10 + 1 */
    bprep[11].e[i] = bprep[10].e[i] ^ bprep[1].e[i];
    /* 12 = 8 + 4 */
    bprep[12].e[i] = bprep[8 ].e[i] ^ bprep[4].e[i];
    /* 13 = 12 + 1 */
    bprep[13].e[i] = bprep[12 ].e[i] ^ bprep[1].e[i];
    /* 14 = 12 + 2 */
    bprep[14].e[i] = bprep[12 ].e[i] ^ bprep[2].e[i];
    /* 15 = 14 + 1 */
    bprep[15].e[i] = bprep[14].e[i] ^ bprep[1].e[i];
  }
  /*clear bits in the result */
  double_null(c);

  mask = 0xf0000000;
  b_start = DOUBLE_WORD- NUM_WORD;
  for(bit_count = 7; bit_count >=0; bit_count--){
      
    for(word = MAX_LONG-1; word >=0; word--){
      multiplier = (mask & a->e[word]) >> (4*bit_count);
      num_shift = (MAX_LONG-1) - word;
      local_bptr = &(bprep[multiplier].e[b_start]);
      cptr = &(c->e[b_start-num_shift]);
      for(i = 0; i < MAX_LONG; i++){
	*cptr = *local_bptr ^ *cptr;
	cptr++;
	local_bptr++;
      }
    }
    if(bit_count !=0){
      /*
	for(i=0; i< 4; i++){
	multiply_shift(c);
	}
      */
      eptr = &(c->e[DOUBLE_WORD]);
      bit =0;
          
      for(k =0; k< MAX_DOUBLE; k++){
	temp = (*eptr << 4) | bit;
	bit = (*eptr & 0xf0000000)>>28;
	*eptr-- = temp;
      }
    }
    mask >>= 4;
  }
  
  return;
}
   

#ifdef OLD_IMP
/* shift right one bit 
   later use this for poly_mul too with prime polynomial 
   shifting is in place
*/


void 
divide_shift(field_2n *a){
  element *eptr, temp, bit;
  short int  i;
  eptr = (element *) &a->e[0];
  bit = 0;
  for(i =0; i< MAX_LONG; i++){
    temp = (*eptr >> 1) | bit;
    bit = (*eptr & 1) ? MSB : 0L;
    *eptr++ = temp;
  }
  return;
}
#endif


/* shift left several bits: for multiplication */

void 
multiply_shift_n(field_double *a, int n){
  element *eptr, temp, bit;
  short int  i;
  short int num_words_shift = 0;
  num_words_shift = n/WORD_SIZE;
  n = n % WORD_SIZE;

  /* shift left the words first */
  for(i = 0; i <=  MAX_DOUBLE - 1 - num_words_shift; i++){
    a->e[i] = a->e[i + num_words_shift];
  }
  for(i= MAX_DOUBLE -num_words_shift ; i< MAX_DOUBLE; i++){
    a->e[i] = 0x00000000;
  }
  /* shift two parts wise the remaining < WORD_SIZE bits */
  eptr = &a->e[DOUBLE_WORD];
  bit =0;
  for(i =0; i< MAX_DOUBLE; i++){
    temp = (*eptr << n) | bit;
    bit = (*eptr & leftmask[n])>>(32-n);
    *eptr-- = temp;
  }

  return;
}

/* shift right several bits for division */

void 
divide_shift_n(field_double *a, int n){
  element *eptr, temp, bit;
  short int  i;
  short int num_words_shift = 0;
  num_words_shift = n/WORD_SIZE;
  n = n % WORD_SIZE;

  /* shift right the words first */
  for(i = MAX_DOUBLE - 1 - num_words_shift; i >= 0; i--){
    a->e[i + num_words_shift] = a->e[i];
  }
  for(i=0; i< num_words_shift; i++){
    a->e[i] = 0x00000000;
  }
  /* shift two parts wise the remaining < WORD_SIZE bits */
  eptr = (element *) &a->e[0];
  bit =0;
  for(i=0; i< MAX_DOUBLE; i++){
    temp = (*eptr >> n) | bit;
    bit = (*eptr & rightmask[n])<<(32-n); /* left part */
    *eptr++ = temp;
  }

  return;
}

#ifdef OLD_IMP
void 
poly_shift_bit_n(field_2n *a, int n){
  element *eptr, temp, bit;
  short int  i,k;
  short int num_words_shift = 0;
  num_words_shift = n/WORD_SIZE;
  n = n % WORD_SIZE;

  /* shift right the words first */
  for(i = MAX_LONG - 1; i >= 0; i--){
    a->e[i + num_words_shift] = a->e[i];
  }
  for(i=0; i< num_words_shift; i++){
    a->e[i] = 0x00000000;
  }
  /* shift bitwise the remaining < WORD_SIZE bits */
	
    
  for(k=0; k<n;k++){
    eptr = (element *) &a->e[0];
    bit =0;
    for(i =0; i< MAX_LONG; i++){
      temp = (*eptr >> 1) | bit;
      bit = (*eptr & 1) ? MSB : 0L;
      *eptr++ = temp;
    }
  }
  return;
}

#endif

/* supporting functions for poly_mul later */
/* extract bits of the double field corresponding to mask
 */
void 
extract_masked_bits(field_double * a, field_double *mask, field_double *result){
  short int i;
  for(i =0; i< MAX_DOUBLE; i++){
    result->e[i] = a->e[i] & mask->e[i];
  }
  return;
}

/* put zeros in nonzero locations of mask in the input*/
void 
zero_masked_bits(field_double *a, field_double *mask){
  short int i;
  for(i =0; i< MAX_DOUBLE; i++){
    /* a - mask = a intersection (mask)complement */
    a->e[i] = a->e[i] & (~mask->e[i]);
  }
  return;
}

/* just utility function for the partial multiply 
 */

void
shift_and_add(field_double *temp, field_double *extract_mask){
  field_double temp1, temp_masked;
  /*first 159 bits */
  extract_masked_bits(temp, extract_mask, &temp_masked);
  zero_masked_bits(temp, extract_mask);
  divide_shift_n(&temp_masked, 159);
  double_add(temp, &temp_masked, &temp1); /* result in temp1*/
	
  /* then shift again by 74 to achieve shift of 233*/
  divide_shift_n(&temp_masked, 74);
  double_add(&temp1, &temp_masked, temp); /* result in temp*/
  return;
}

/* this is polynomial multiplication modulo a primitive
 * polynomial. The primitive polynomial is 233 bits
 * u^233 + u^74 + 1. defined in the header field
 * generally this will work for any trinomials changing the
 * shift numbers, check word
 * size. Need to generalise for pentanomials.
 * field_2n poly_prime = {0x00080000,0x00000000,0x00000000,
 * 0x00000000,0x0000000 * 0,0x00000400, 0x00000000, 0x00000001};  
 * 233 bit u^233 + u^74 + 1

 * this is derived from the method in "fast key exchange with 
 * elliptic curve systems"
 * by schroeppel, orman, malley
 */

void 
poly_mul( field_2n *a, field_2n *b, field_2n *c){
  field_double temp;
  field_double extract_mask; 
  /* check if this is 16*32=512 bits!*/
  /* bits 464-306 */
	
  double_null(&extract_mask);
  /*MSB is in 0*/
  extract_mask.e[0] = 0x000fffff;
  extract_mask.e[1] = 0xffffffff;
  extract_mask.e[2] = 0xffffffff;
  extract_mask.e[3] = 0xffffffff;
  extract_mask.e[4] = 0xffffffff;
  extract_mask.e[5] = 0xffe00000;
  poly_mul_partial(a,b, &temp);
	
  /* extract first 159 bits from degree= 468 and shift it by 159, 
     and 233 and add to temp.
  */
  shift_and_add(&temp, &extract_mask);
    	
  /* temp has first 159 bits reduced*/
  /* second iteration */
  /* repeat only for 74 points extract_mask shifted by 159 inclusive */
	
  /*MSB is in 0*/
  divide_shift_n(&extract_mask, 159);
  /* keep least significant 233 bits : so make them zero*/
  extract_mask.e[7] = 0xfffffe00;
  extract_mask.e[8] = 0x00000000;
  extract_mask.e[9] = 0x00000000;
  extract_mask.e[10] = 0x00000000;
  extract_mask.e[11] = 0x00000000;
  extract_mask.e[12] = 0x00000000;
  extract_mask.e[13] = 0x00000000;
  extract_mask.e[14] = 0x00000000;

  shift_and_add(&temp, &extract_mask);
  double_to_single(&temp, c);

  return;
}

void 
cus_times_u_to_n(field_2n *a, int n, field_2n *b)
{
  field_double extract_mask, temp1, temp2, temp3, temp_masked;
  field_double a_copy;
  unsigned int moving_one;
  int num_words_divide;
  int num_bits_divide;
  int i,j;
  element *fromptr;
  element *temp1ptr;
  element *temp2ptr;
  single_to_double(a, &a_copy);
  
  double_null(&extract_mask);

  num_words_divide = n/WORD_SIZE;
  num_bits_divide = n % WORD_SIZE;
  
  
  
  /* last word */
  extract_mask.e[DOUBLE_WORD] = 0xffffffff;
  for(j =0; j < num_words_divide; j++){
    extract_masked_bits(&a_copy, &extract_mask, &temp_masked);
    /*
      double_copy(&temp_masked, &temp1);
      double_copy(&temp_masked, &temp2);
    */
    fromptr = &(temp_masked.e[0]);
    temp1ptr = &(temp1.e[0]);
    temp2ptr = &(temp2.e[0]);	     
    for(i=0; i< MAX_DOUBLE; i++){
      *temp1ptr = *fromptr;
      *temp2ptr = *fromptr;
      fromptr++;
      temp1ptr++;
      temp2ptr++;
    }
        
    multiply_shift_n(&temp1, 233);
    multiply_shift_n(&temp2, 74);
           
    
    double_add(&a_copy, &temp1, &temp3);
    double_add(&temp3, &temp2, &temp1);
    /* last 32 bits must be zero anyway, check */
    zero_masked_bits(&temp1, &extract_mask);
    divide_shift_n(&temp1, 32);
    double_copy(&temp1, &a_copy);
  }
  
  /* shift last remaining bits */
  double_null(&extract_mask);
  moving_one = 0x0001;
  /* enter as many bits in the mask */
  for(i =0; i< num_bits_divide; i++){
    extract_mask.e[DOUBLE_WORD] |= moving_one;
    moving_one = moving_one << 1;
  }

  extract_masked_bits(&a_copy, &extract_mask, &temp_masked);
  double_copy(&temp_masked, &temp1);
  double_copy(&temp_masked, &temp2);
    
  multiply_shift_n(&temp1, 233);
  multiply_shift_n(&temp2, 74);

  double_add(&a_copy, &temp1, &temp3);
  double_add(&temp3, &temp2, &temp1);
    
  /* last num_bits_divide bits must be zero anyway, check */
  zero_masked_bits(&temp1, &extract_mask);
  divide_shift_n(&temp1, num_bits_divide);
  double_copy(&temp1, &a_copy);
  double_to_single(&a_copy, b);
}  


/* returns true if a < b */
void 
is_less_than(field_2n *a, field_2n *b, field_boolean *result){
  int i;
  for( i = 0; i < MAX_LONG; i++){
    if(a->e[i] == b->e[i]) continue;
    else{
      if (a->e[i] < b->e[i]){
	*result =  BSL_TRUE;
	return ;
      }
      else {
	*result = BSL_FALSE;
	return ;
      }
    }
  }
  /* beleive we wont enter this...
   *result = BSL_FALSE;
   return  ;
  */
}

/* poly_inv severely optimised, to see the reference implementation
 * check the OLD_IMP below
 */

void
poly_inv(field_2n *a, field_2n *dest)
{
  field_2n	f, b, c, g;
  short int	i, j, m, n, f_top, c_top;
  element bits;
  unsigned int longword = (NUM_BITS + 1)/WORD_SIZE;
	
  /* Set c to 0, b to 1, and n to 0 */
  null(&c);
  null(&b);
  copy(a, &f);
  copy(&poly_prime, &g);
  b.e[longword] = 1;
	
  n = 0;

	
  /* Now find a polynomial b, such that a*b = u^n */

  /* f and g decrease and b and c increase, 
     c_top and f_top point to the edges 
  */
  c_top = longword;
  f_top = 0;

  /* now c is zero, so no need to shift it left, implement
     that part first */
        
  do {
    i = shift_by[f.e[longword] & 0xff];
    n += i;
            
    if(i ==0 ) break;
    /* Shift f right i (divide by u^i) */
    m = 0;
    for ( j=f_top; j<=longword; j++ ) {
      bits = f.e[j];
      f.e[j] = (bits>>i) | ((element)m << (WORD_SIZE-i));
      m = bits;
    }
  } while ( i == 8 && (f.e[longword] & 1) == 0 );
  /* while you keep shifting whole words */
        
  /* find the first significant word */
  for ( j=0; j<longword; j++ )
    if ( f.e[j] ) break;
  if ( j<longword || f.e[longword] != 1 ) {
    /* implement two loops, to exchange variables go to the other
     * loop 
     */	  
    do {
      /* Shorten f and g when possible */
      while ( f.e[f_top] == 0 && g.e[f_top] == 0 ) f_top++;
      /* f needs to be bigger - if not, exchange f with g and b with c.
       * instead of exchanging jump to the other loop
       */	
      if ( f.e[f_top] < g.e[f_top] ) {
	goto loop2;
      }
		
    loop1:
      /* f = f+g */
      for ( i=f_top; i<=longword; i++ )
	f.e[i] ^= g.e[i];
		 
      /* b = b+c */
      for ( i=c_top; i<=longword; i++ )
	b.e[i] ^= c.e[i];
      do {
	i = shift_by[f.e[longword] & 0xff];
	n+=i;
	/* Shift c left i (multiply by u^i)*/
	m = 0;
	for ( j=longword; j>=c_top; j-- ) {
	  bits = c.e[j];
	  c.e[j] = (bits<<i) | m;
	  m = bits >> (WORD_SIZE-i);
	}
	if ( m ) c.e[c_top=j] = m;

	/* Shift f right i (divide by u^i) */
	m = 0;
	for ( j=f_top; j<=longword; j++ ) {
	  bits = f.e[j];
	  f.e[j] = (bits>>i) | ((element)m << (WORD_SIZE-i));
	  m = bits;
	}
      } while ( i == 8 && (f.e[longword] & 1) == 0 );
      /* Check if we are done (f=1) */
      for ( j=f_top; j<longword; j++ )
	if ( f.e[j] ) break;
    } while ( j<longword || f.e[longword] != 1 );
	   
    if ( j>0 ) 
      goto done;
    do { 
      /* do the same drill with variables reversed */
      /* Shorten f and g when possible */
      while ( g.e[f_top] == 0 && f.e[f_top] == 0 ) f_top++;

      if ( g.e[f_top] < f.e[f_top] ) goto loop1;
    loop2:

      /* g = f+g, making g divisible by u */
      for ( i=f_top; i<=longword; i++ )
	g.e[i] ^= f.e[i];
      /* c = b+c */
      for ( i=c_top; i<=longword; i++ )
	c.e[i] ^= b.e[i];
		
      do {
	i = shift_by[g.e[longword] & 0xff];
	n+=i;
	/* Shift b left i (multiply by u^i)*/
	m = 0;
	for ( j=longword; j>=c_top; j-- ) {
	  bits = b.e[j];
	  b.e[j] = (bits<<i) | m;
	  m = bits >> (WORD_SIZE-i);
	}
	if ( m ) b.e[c_top=j] = m;
		
	/* Shift g right i (divide by u^i) */
	m = 0;
	for ( j=f_top; j<=longword; j++ ) {
	  bits = g.e[j];
	  g.e[j] = (bits>>i) | ((element)m << (WORD_SIZE-i));
	  m = bits;
	}
      } while ( i == 8 && (g.e[longword] & 1) == 0 );
      /* Check if we are done (g=1) */
      for ( j=f_top; j<longword; j++ )
	if ( g.e[j] ) break;
    } while ( j<longword || g.e[longword] != 1 );
    copy(&c, &b);
  }

  /* Now b is a polynomial such that a*b = u^n, so multiply b by 
     u^(-n) */
 done: cus_times_u_to_n(&b, n, dest);    
  return;
} 

#ifdef OLD_IMP
/*
 * void 
 * field_swap()
 * Purpose : swap doubles
 * input: pointer to allocated doubles, cannot do in place
 * 
 */

void 
field_swap(field_2n *a, field_2n *b)
{
  field_2n temp;
    
  null(&temp);
  copy(a, &temp);
  copy(b, a);  
  copy(&temp, b);
  return;
}

#endif


#ifdef OLD_IMP
/* this is implementation of the modified almost inverse 
 * algorithm for inversion in F(2^m) in the paper: software 
 * implementation of elliptic curve cryptography over binary fields
 * by Hankerson, Hernandez, and Menezes

 *  uses multiply_shift and divide_shift above
 * b = inv (a) mod poly_prime
 */

void poly_inv(field_2n *a, field_2n *b){
  field_2n F, G;
  field_2n temp, temp1;
  int i;
  int k;
  field_boolean res;
  element *eptr, tempelement, bit;
   
  element *tempptr, *temp1ptr, *gptr, *fptr, *bptr, *cptr;
   
  field_2n B, C; /* later copy to single field b */
  null(&F);
  null(&G);
  null(&B);
  null(&C);
  k = 0;
  /* B = 1 */
  B.e[MAX_LONG -1] =1;
    
  null(&C);
  /* F = A*/
  copy(a, &F);

  /* G = M*/
  copy(&poly_prime, &G);
  
  for(;;){
    /* while F is even do */
    while((F.e[MAX_LONG - 1] & 1)== 0){
      /*
	divide_shift(&F);
      */
      eptr = (element *) &(F.e[0]);
      bit = 0;
      for(i =0; i< MAX_LONG; i++){
	tempelement = (*eptr >> 1) | bit;
	bit = (*eptr & 1) ? MSB : 0L;
	*eptr++ = tempelement;
      }
      /*
	poly_multiply_shift(&C);
      */
      eptr = (element *)&(C.e[NUM_WORD]);
      bit =0;

      for(i =0; i< MAX_LONG; i++){
	tempelement = (*eptr << 1) | bit;
	bit = (*eptr & MSB) ? 1L: 0L;
	*eptr-- = tempelement;
      }

      k++;
    }
    /* check if F is one */
    res = BSL_TRUE;
    for(i =0; i< MAX_LONG -1; i++){
      if(F.e[i] != 0x00){
	res = BSL_FALSE;
      }
    }
    if(res == BSL_TRUE){
      if(F.e[MAX_LONG -1] != 0x00000001){
	res = BSL_FALSE;
      }
    }
    
    /* if F is 1 return B, k */
    if(res == BSL_TRUE){
      cus_times_u_to_n(&B, k, b);
            
      return;
    }
    /*
      poly_degree_of((element *) &F, NUM_WORD, &degree_F);
      poly_degree_of((element *) &G, NUM_WORD, &degree_G);
    
      if(degree_F < degree_G){
    */
    is_less_than(&F, &G, &res);
    if(res == BSL_TRUE){
      /*
       * optimisation here: instead of swapping, change code 
       *
       */
      /*
	field_swap(&F, &G);
	field_swap(&B, &C);
      */
	  
      for(i=0; i < MAX_LONG; i++){
	temp.e[i] = F.e[i];
	temp1.e[i] = B.e[i];
           
	F.e[i] = F.e[i] ^ G.e[i];
	B.e[i] = B.e[i] ^ C.e[i];
            
	G.e[i] = temp.e[i];
	C.e[i] = temp1.e[i];
      }	    
    }
    /* add F= F+ G */
    else{
      for(i =0 ;i < MAX_LONG; i++){
	F.e[i] = F.e[i] ^ G.e[i];
	B.e[i] = B.e[i] ^ C.e[i];
      }
    }
  }
}
#endif

#ifdef OLD_IMP
/*
 * void 
 * poly_multiply_shift()
 * Purpose : shift one bit to do partial multiply: double field.
 * input: pointer to allocated field_double, can do in place
 * 
 */

void 
poly_multiply_shift(field_2n *a)
{
  element *eptr, temp, bit;
  short int i;
    
  /* this is bigendian representation
   */
  eptr = &a->e[NUM_WORD];
  bit =0;

  for(i =0; i< MAX_LONG; i++){
    temp = (*eptr << 1) | bit;
    bit = (*eptr & MSB) ? 1L: 0L;
    *eptr-- = temp;
  }
  return;
}
#endif 
#ifdef OLD_IMP

/*
 * void 
 * poly_div_shift()
 * Purpose : shift right once for polynomial division
 * input: pointers to allocated double, in place
 * 
 */

void 
poly_div_shift(field_double *a)
{
  element *eptr, temp, bit;
  short int i;
  /*
    if(a == NULL){
    return BSL_NULL_POINTER;
    }
  */

  eptr = (element *) &a->e[0];
  bit = 0;
  
  for(i =0; i < MAX_DOUBLE; i++){
    temp = (*eptr >> 1) | bit;
    bit = (*eptr & 1) ? MSB:0L;
    *eptr++ = temp;
  }
  return;
}

#endif

#ifdef OLD_IMP
/*
 * void 
 * poly_log2(short int *result)
 * Purpose : find first bit set
 * input: pointers to allocated double, in place, returns index
 * 
 */

void
poly_log2(element x, short int *result)
{
  element ebit, bitsave, bitmask;
  short int k, lg2;


  lg2 = 0;
  bitsave = x;

  k = WORD_SIZE/2;

  bitmask = -1L << k;

  while(k){
    ebit = bitsave & bitmask;
    if (ebit){
      lg2 += k;
      /* increment degree */
      bitsave = ebit;
    }
    /* binary searching */
    k /= 2; 
    bitmask ^= (bitmask >> k);
  }

  *result = lg2;
  return;
}

#endif

#ifdef OLD_IMP

/*
 * void 
 * poly_degree_of(element *t, short int *result)
 * Purpose : find first bit set in array of elements
 * input: pointers to allocated double, in place, returns index
 * 
 */
  
void
poly_degree_of(element *t, short int dim, short int *result)
{
  short int degree, k;
  short int temp;
    

  /* first non zero element */
  degree = dim * WORD_SIZE;
  for (k = 0; k < dim; k++){
    if( *t) break;
    degree -= WORD_SIZE;
    t++;
  }

  /* are all bits zero */
  if(!*t){
    *result = -1;
    return;
  }
  
  poly_log2(*t, &temp);
  degree += temp;
  *result = degree;
  return;
}

#endif
  
/*
 * void 
 * poly_rot_right(field_2n *a)
 * Purpose : polynomial rotate right once
 * input: pointers to field_2n variable
 * 
 */
    

void
poly_rot_right(field_2n *a)
{
  short int i;
  element bit, temp;
  bit = (a->e[NUM_WORD] & 1)? UPR_BIT : 0L;
  for(i = 0; i< MAX_LONG; i++){
    temp = (a->e[i] >> 1) | bit;
    bit = (a->e[i] & 1) ? MSB : 0L;
    a->e[i] = temp;
  }
  a->e[0] &= UPR_MASK;
  return;
}

#ifdef OLD_IMP
/*
 * void 
 * double_is_one()
 * Purpose : check if one
 * input: pointers to allocated double, result
 * 
 */
void 
double_is_one(field_double *a, field_boolean *result)
{
  short int i;
  field_double one;
    
  *result = BSL_TRUE;
  
  double_null(&one);
  /* big endian representation
   */
  one.e[DOUBLE_WORD] = 1;
  for(i =0; i < MAX_DOUBLE; i++){
    if(one.e[i] != a->e[i]){
      *result = BSL_FALSE;
    }
  }
  return;
}

#endif
#ifdef OLD_IMP
/*
 * void 
 * field_is_one()
 * Purpose : check if one
 * input: pointers to allocated double, result
 * 
 */
void 
field_is_one(field_2n *a, field_boolean *result)
{
  short int i;
  field_2n one;
    
  *result = BSL_TRUE;
  
  null(&one);
  /* big endian representation
   */
  one.e[NUM_WORD] = 1;
  for(i =0; i < MAX_LONG; i++){
    if(one.e[i] != a->e[i]){
      *result = BSL_FALSE;
    }
  }
  return;
}

#endif
