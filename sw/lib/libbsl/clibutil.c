
/*
 * clib string funcs
 */

#include <bbtypes.h>

#include "clibutil.h"


void* wmemcpy(void* s1, const void* s2, unsigned long n) {
    u32 *su1;
    const u32 *su2;
    for (su1 = s1, su2 = s2; 0 < n; ++su1, ++su2, --n)
        *su1 = *su2;
    return (s1);
}




int memcmp(const void *s1, const void *s2, unsigned long n)
{
    const u8* a = s1, * b = s2;
    u8 a1, b1;
    while (n-- > 0) {
	if ((a1 = *a++) == (b1 = *b++)) continue;
	return a1 - b1;
    }
    return 0;
}


/*
 * clib string funcs
 */

char *strchr(const char *s, int c)
{   /* find first occurrence of c in char s[] */
    const char ch = c;
    
    for (; *s != ch; ++s)
        if (*s == '\0')
            return (NULL);
    return ((char *) s);
}

size_t strlen(const char *s)
{   /* find length of s[] */
    const char *sc;
    
    for (sc = s; *sc != '\0'; ++sc);
    return (sc - s);
}

int strcmp(const char *s,const char *t)
{
    for(;*s==*t;s++,t++)
        if(*s=='\0')
            return 0;
    return *s - *t;
}


int strncmp(const char *s,const char *t, size_t bytes)
{
    int i;
    int result = 0;
    for(i =0; i< bytes; i++){
        if(s[i] != t[i]){
            result = -1;
        }
    }
    return result;
}


char *strstr(const char * s1,const char * s2)
{
    int l1, l2;
    
    l2 = strlen(s2);
    if (!l2)
        return (char *) s1;
    l1 = strlen(s1);
    while (l1 >= l2) {
        l1--;
        if (!memcmp(s1,s2,l2))
            return (char *) s1;
        s1++;
    }
    return NULL;
}

/* XXX: comment out occasionally to catch any hidden dependencies on memcpy,
 * such as copying large structs to functions on stack.
 */
void *memcpy(void *s1, const void *s2, size_t n)
{   /* copy char s2[n] to s1[n] in any order */
    char *su1;
    const char *su2;
    
    for (su1 = s1, su2 = s2; 0 < n; ++su1, ++su2, --n)
        *su1 = *su2;
    return (s1);
}


void *memset(void *s, int c, size_t n)
{
    u8* s1 = s;
    int i;
    for(i = 0; i < n; i++)
	s1[i] = c;
    return s;
}

void bzero(void* s, size_t n)
{
    memset(s, 0, n);
}

void bcopy(void *d, void *s, size_t n)
{
    memcpy(d, s, n);
}

