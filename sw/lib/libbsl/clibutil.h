#ifndef __CLIB_UTIL_HDR__
#define __CLIB_UTIL_HDR__


void* wmemcpy(void* s1, const void* s2, unsigned long n);
int memcmp(const void *s1, const void *s2, unsigned long n);

#ifndef X86_BUILD
#include <t2stdio.h>
#else
typedef unsigned int size_t;
#define NULL 0
#endif


char *strchr(const char *s, int c);
size_t strlen(const char *s);
int strcmp(const char *s,const char *t);
int strncmp(const char *s,const char *t, size_t bytes);
char *strstr(const char * s1,const char * s2);
void *memcpy(void *s1, const void *s2, size_t n);
void *memset(void *s, int c, size_t n);
void bzero(void* s, size_t n);
void bcopy(void *d, void *s, size_t n);



#endif

