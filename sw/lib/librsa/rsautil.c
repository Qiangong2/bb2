#include <openssl/rsa.h>
#include <openssl/bn.h>
#include <openssl/rand.h>
#include <bsl.h>
#include <sha1.h>
#include <bbsec.h>
#include "rsautil.h"

/* 
 * utilities for saving and retrieving openssl generated private key
 * data
 */

/* 
 * read key data from file and return RSA struct 
 */ 
void
readRsaData(FILE *readptr, RSA *prsa){

  readRsaDataNamed(readptr, prsa, 0);
}

/*
 * read key data from file, return RSA struct and name of entity with key
 */

void
readRsaDataNamed(FILE *readptr, RSA *prsa, BbServerName name){

    unsigned char e_string[MAX_KEY_SIZE], n_string[MAX_KEY_SIZE];
    int e_size, n_size, i;
    int d_size, p_size, q_size;
    unsigned char d_string[MAX_KEY_SIZE], p_string[MAX_KEY_SIZE], q_string[MAX_KEY_SIZE];
    unsigned char dmp1_string[MAX_KEY_SIZE];
    unsigned char dmq1_string[MAX_KEY_SIZE];
    unsigned char iqmp_string[MAX_KEY_SIZE];
    int dmp1_size, dmq1_size, iqmp_size;
#define read_byte(f, x)    { int tmp; fscanf(f, "%02x", &tmp); *(x) = tmp; }
    
    fscanf(readptr, "%d", &e_size);
    for( i =0 ; i< e_size; i++){
	read_byte(readptr, &e_string[i]);
    }
    prsa->e = BN_bin2bn(e_string, e_size, prsa->e);

    fscanf(readptr, "%d", &n_size);
    for( i =0 ; i< n_size; i++){
	read_byte(readptr, &n_string[i]);
    }
    prsa->n = BN_bin2bn(n_string, n_size, prsa->n);
        
    
    fscanf(readptr, "%d", &d_size);
    for( i =0 ; i< d_size; i++){
	read_byte(readptr, &d_string[i]);
    }
    prsa->d = BN_bin2bn(d_string, d_size, prsa->d);
    

    fscanf(readptr, "%d", &p_size);
    for( i =0 ; i< p_size; i++){
	read_byte(readptr, &p_string[i]);
    }
    prsa->p = BN_bin2bn(p_string, p_size, prsa->p);
    

    fscanf(readptr, "%d", &q_size);
    for( i =0 ; i< q_size; i++){
	read_byte(readptr, &q_string[i]);
    }
    prsa->q = BN_bin2bn(q_string, q_size, prsa->q);
    
    
    fscanf(readptr, "%d", &dmp1_size);
    for( i =0 ; i< dmp1_size; i++){
	read_byte(readptr, &dmp1_string[i]);
    }
    prsa->dmp1 = BN_bin2bn(dmp1_string, dmp1_size, prsa->dmp1);
    
    fscanf(readptr, "%d", &dmq1_size);
    for( i =0 ; i< dmq1_size; i++){
	read_byte(readptr, &dmq1_string[i]);
    }
    prsa->dmq1 = BN_bin2bn(dmq1_string, dmq1_size, prsa->dmq1);
    
    fscanf(readptr, "%d", &iqmp_size);
    for( i =0 ; i< iqmp_size; i++){
	read_byte(readptr, &iqmp_string[i]);
    }
    prsa->iqmp = BN_bin2bn(iqmp_string, iqmp_size, prsa->iqmp);

    if(name !=0){
      fscanf(readptr, "%s", name);
    }
}

/*
 * write RSA struct data into file 
 */

void saveRsaData(FILE *writeptr, RSA *prsa){
  saveRsaDataNamed(writeptr, prsa, 0);
}

/*
 * write rsa data to file and add name 
 */

void saveRsaDataNamed(FILE *writeptr, RSA *prsa, BbServerName name){ 
    unsigned char e_string[MAX_KEY_SIZE];
    int e_size, i;
    int n_size;
    unsigned char n_string[MAX_KEY_SIZE];
    unsigned char d_string[MAX_KEY_SIZE];
    unsigned char p_string[MAX_KEY_SIZE];
    unsigned char q_string[MAX_KEY_SIZE];
    unsigned char dmp1_string[MAX_KEY_SIZE];
    unsigned char dmq1_string[MAX_KEY_SIZE];
    unsigned char iqmp_string[MAX_KEY_SIZE];
    int d_size, p_size, q_size;
    int dmp1_size, dmq1_size, iqmp_size;

    e_size = BN_bn2bin(prsa->e, e_string);

    fprintf(writeptr, "\n%d\n", e_size);
    for( i =0 ; i< e_size; i++){
        fprintf(writeptr, "%02x ", e_string[i]);
    }
    n_size = BN_bn2bin(prsa->n, n_string);
 
    fprintf(writeptr, "\n%d\n", n_size);
    for( i =0 ; i< n_size; i++){
        fprintf(writeptr, "%02x ", n_string[i]);
    }
    d_size = BN_bn2bin(prsa->d, d_string);
    fprintf(writeptr, "\n%d\n", d_size);
    for( i =0 ; i< d_size; i++){
        fprintf(writeptr, "%02x ", d_string[i]);
    }
    p_size = BN_bn2bin(prsa->p, p_string);
    fprintf(writeptr, "\n%d\n", p_size);
    for( i =0 ; i< p_size; i++){
        fprintf(writeptr, "%02x ", p_string[i]);
    }
    q_size = BN_bn2bin(prsa->q, q_string);
    fprintf(writeptr, "\n%d\n", q_size);
    for( i =0 ; i< q_size; i++){
        fprintf(writeptr, "%02x ", q_string[i]);
    }
    dmp1_size = BN_bn2bin(prsa->dmp1, dmp1_string);
    fprintf(writeptr, "\n%d\n", dmp1_size);
    for( i =0 ; i< dmp1_size; i++){
        fprintf(writeptr, "%02x ", dmp1_string[i]);
    }
    dmq1_size = BN_bn2bin(prsa->dmq1, dmq1_string);
    fprintf(writeptr, "\n%d\n", dmq1_size);
    for( i =0 ; i< dmq1_size; i++){
        fprintf(writeptr, "%02x ", dmq1_string[i]);
    }
    iqmp_size = BN_bn2bin(prsa->iqmp, iqmp_string);
    fprintf(writeptr, "\n%d\n", iqmp_size);
    for( i =0 ; i< iqmp_size; i++){
        fprintf(writeptr, "%02x ", iqmp_string[i]);
    }
    
    if(name !=0){
      fprintf(writeptr, "\n%s", name);
    }
    
}



int validate(RSA *prsa, int num_bits){
    int size;
    unsigned char *data;
    unsigned char *padded_data;
    unsigned char *sign;
    unsigned char *verify;
    unsigned char hash_data[20];
    unsigned char e_string[512];
    unsigned char n_string[512];
    unsigned long certpublickey[128];
    unsigned long certsign[128];
    unsigned char customResult[512];
    unsigned long certexponent;
    int newsize, i, j, equal, e_size, n_size;
    int data_size;
    SHA1Context sha;
    BSL_error result;
    size = RSA_size(prsa);

    
    data_size = size;
    sign = (unsigned char *) malloc(512);
    verify = (unsigned char *) malloc(512);
    
    data = (unsigned char *)malloc(512);
    padded_data = (unsigned char *) malloc(512);

    
    for (i =0; i < data_size; i+=4){
      *((unsigned long *) (data + i)) = (unsigned long) rand();
    }

    /* compute SHA-1 */
    SHA1Reset(&sha);
    SHA1Input(&sha, data, data_size);
    SHA1Result(&sha, hash_data);
#ifdef DEBUG    
    for( i = 0; i < 20; i++){
      printf("hash = %02x\n", hash_data[i]);
    }
#endif
    RSA_padding_add_PKCS1_type_2(padded_data, data_size, hash_data, 20);
        
#ifdef DEBUG
    /*print padded data */
    for(i = 0; i < data_size; i++){
      printf("padded = %02x\n", padded_data[i]);
    } 
#endif
            
    /* zero the sign */
    for (i = 0; i <  size; i++){
      sign[i] = 0;
    }
    size = RSA_private_encrypt(data_size, padded_data, sign, prsa, RSA_NO_PADDING);
    /* zero the result */
    for(i = 0; i < size; i++){
      verify[i]  = 0;
    }
    newsize = RSA_public_decrypt(RSA_size(prsa), sign, verify, prsa, RSA_NO_PADDING);
#ifdef DEBUG
    printf("size of decrypted = %d\n", newsize);
    for(i =0; i< data_size; i++){
      printf("verify [%d] = %02x padded_data = %02x\n", i, verify[i], padded_data[i]);
    }
#endif
    /* call mod_exp from integer_math.h */
    
    /* initialise bigint */

    e_size = BN_bn2bin(prsa->e, e_string);
#ifdef DEBUG
    for( i =0 ; i< e_size; i++){
      printf("e[%d] = %02x\n", i, e_string[i]);
    }
#endif

    n_size = BN_bn2bin(prsa->n, n_string);
#ifdef DEBUG
    for( i =0 ; i< n_size; i++){
      printf("n[%d] = %02x\n", i, n_string[i]);
    }
#endif 
    convertToHost(e_string, n_string, sign, e_size, n_size, &certexponent, certpublickey, certsign);

    result = bsl_rsa_verify(hash_data, certsign, certpublickey, &certexponent, num_bits);

    free(sign);
    free(verify);
    free(data);
    free(padded_data);
    return result;

}

void
convertToHost(u8 *e_string, u8 *n_string, u8 *sign, 
	      int e_size, int n_size, 
	      u32 *certexponent, u32 *certpublickey, u32 *certsign){
  int i, count;
  u32 mask = 0x00;
  for(i =0 ; i < e_size; i++){
    mask = (mask << 8) | (0xff);
  }
	
  *certexponent = get_word(e_string) & mask;
  count = 0;
  for(i=0; i< n_size; i= i+4){
    certpublickey[count] = htonl(get_word(n_string + i));
    certsign[count] = htonl(get_word(sign + i));
    count++;
  }
}

#if 0

/*
 * for verifying certs use this call, pass in the cert as it is
 * read in from a file, and pass in public key and exponent
 * from the next cert with no conversion from a cert. if 
 * root public key and exponent are typecast from words, 
 * swap before passing in.
 * see tools/tests/certcheck.c for example to check a list of cert files.
 */

int verifyCertSign( void *subjectcert, int subjectsize, 
		    u32 signerexponent, 
		    u8 *signerpublic, int signerpublicsize){
  SHA1Context sha;
  unsigned char hash_data[20];
  u8 *sign;
  unsigned long certpublickey[512];
  unsigned long certsign[512];
  unsigned char customResult[512];
  int i, count, equal, j;
  u8 *name;
  SHA1Reset(&sha);
  if(subjectsize == sizeof(BbEccCert)){
      SHA1Input(&sha, (u8 *)subjectcert, BB_ECC_CERT_SIGNED_BYTES);
      name = ((BbEccCert *)subjectcert)->certId.name.bbid;
      sign = (u8 *)(((BbEccCert *)subjectcert)->signature.rsa2048);
  }
  else{
      SHA1Input(&sha, (u8 *)subjectcert, BB_RSA_CERT_SIGNED_BYTES);
      name = ((BbRsaCert *)subjectcert)->certId.name.server;
      if(signerpublicsize*8 == 2048){
	sign = (u8 *)(((BbRsaCert *)subjectcert)->signature.rsa2048);
      }
      else{
	sign = (u8 *)(((BbRsaCert *)subjectcert)->signature.rsa4096);
      }
  }
  SHA1Result(&sha, hash_data);
  signerexponent= htonl(signerexponent);
  count = 0;
  for(i=0; i< signerpublicsize; i= i+4){
    certpublickey[count] = htonl(get_word(signerpublic + i));
    certsign[count] = htonl(get_word(sign + i));
    count++;
  }
  bsl_rsa_verify(customResult, certsign, certpublickey, &signerexponent, signerpublicsize*8);
  
  equal = 0;
  j= 0;
  /* compare output*/
  for(i=signerpublicsize - sizeof(BbShaHash); i < signerpublicsize; i++){
    if(hash_data[j] != customResult[i]){
      equal = 1;
    }
    j++;
  }
  return equal;
}


/* sign a hash value (20 bytes)  and return sign in BB format */
void
signRsa(u8 *hash_data, RSA * prsa2, unsigned long *certsign){
  unsigned char e_string[512];
  unsigned char n_string[512];
  
  unsigned char signature[512];
  unsigned char *padded_data;
  unsigned char *verify;
  int num_bytes, size, newsize;
  int e_size, n_size, i, count;
    
  padded_data = (unsigned char *) malloc(512);
  verify = (unsigned char *) malloc(512);

  num_bytes = RSA_size(prsa2);
 /* compute hash */
      
  RSA_padding_add_PKCS1_type_2(padded_data, num_bytes, hash_data, 20);
#ifdef DEBUG
  for (i = 0; i < 20; i++){
    printf("hash[%d] of cert= %02x\n",i,hash_data[i]);
  }

  for (i = 0; i < num_bytes; i++){
    printf(" padded data = %02x\n", padded_data[i]);
  }
#endif
    
  
  /* take the signer data */
  size = RSA_private_encrypt(num_bytes, 
                      padded_data, signature, prsa2, RSA_NO_PADDING);

/* zero the result: just verifying for fun using RSA */

  for(i = 0; i < size; i++){
    verify[i]  = 0;
  }
  newsize = RSA_public_decrypt(RSA_size(prsa2), signature, verify, prsa2, RSA_NO_PADDING);

#ifdef DEBUG
  printf("size of decrypted = %d\n", newsize);
  for(i =0; i< newsize; i++){
    printf("cert signature = %02x verify [%d] = %02x padded_data = %02x\n", signature[i] , i, verify[i], padded_data[i]);
  }
#endif

  /* initialise bigint */

  e_size = BN_bn2bin(prsa2->e, e_string);
  n_size = BN_bn2bin(prsa2->n, n_string);
    
  count = 0;
  for(i=0; i< n_size; i= i+4){
    certsign[count] = htonl(get_word(signature + i));
    count++;
  }

  /* verify here using bsl_rsa_verify if in doubt: swap back to save */
  count = 0;
  for(i=0; i< n_size; i= i+4){
    certsign[count] = htonl(certsign[count]);
    count++;
  }
}



/*
 * this call takes two key files generated for certsgen tool
 * and creates a signed cert for the first key, signing by the second
 * key. the names are for the cert to be created
 */


     
void
generateCertFromKeyData(u8 *keydatafile, u8 *signerkeydatafile, u8 *subjectname, u8 *issuername, unsigned long *certdata){
  unsigned char e_string[512];
  unsigned char n_string[512];
  unsigned long certpublickey[128];
  unsigned long certsign[128];
  unsigned long certexponent;
  unsigned char hash_data[20];
  unsigned char *padded_data;
  unsigned char *verify;
  FILE *rsareadptr;
  int num_bytes;
  SHA1Context sha;
  int e_size, n_size, i, count;
  RSA *prsa1;
  RSA *prsa2;
  u32 sigtype;
  BbServerName embedsubjectname;
  BbServerName embedissuername;
  BbServerName tmpname1;
  BbServerName tmpname2;
  char *substr;
  
  padded_data = (unsigned char *) malloc(512);
  verify = (unsigned char *) malloc(512);

  /* read in key data to openssl  RSA pointers */
  rsareadptr = fopen(keydatafile, "r");
  prsa1 = RSA_new();
  if(subjectname !=0 ){
    readRsaData(rsareadptr, prsa1);
  }
  else {
    /* read in subject name */
    bzero(embedsubjectname, sizeof(BbServerName));
    readRsaDataNamed(rsareadptr, prsa1, embedsubjectname);
  }
  fclose(rsareadptr);
  prsa2 = RSA_new();
  
  rsareadptr = fopen(signerkeydatafile, "r");
  if(issuername !=0){
    readRsaData(rsareadptr, prsa2);
  }
  else{
    bzero(embedissuername, sizeof(BbServerName));
    readRsaDataNamed(rsareadptr, prsa2, embedissuername);
  }
  num_bytes = RSA_size(prsa2);
  fclose(rsareadptr);
  
  e_size = BN_bn2bin(prsa1->e, e_string);
#ifdef DEBUG
  for( i =0 ; i< e_size; i++){
    printf("e[%d] = %02x\n", i, e_string[i]);
  }

#endif
  n_size = BN_bn2bin(prsa1->n, n_string);
#ifdef DEBUG
  for( i =0 ; i< n_size; i++){
    printf("n[%d] = %02x\n", i, n_string[i]);
  }
#endif  

#define get_word(x)	*(u32*)(x)
  certexponent = get_word(e_string) & 0xff;
  count = 0;
  for(i=0; i< n_size; i= i+4){
    certpublickey[count] = htonl(get_word(n_string + i));
    count++;
  }
  if(num_bytes == 256){
    sigtype = BB_SIG_TYPE_RSA2048;
  }
  else{
    sigtype = BB_SIG_TYPE_RSA4096;
  }

  if((subjectname !=0) && (issuername !=0)){
    generateUnsignedRSACert(BB_CERT_TYPE_SERVER, sigtype, 0, subjectname, issuername,  certpublickey, certexponent, certdata, SIZE_RSA_CERTBLOB_WORDS);
  }
  else{
    bzero(tmpname1, sizeof(BbServerName));
    sprintf(tmpname1, "%s", embedissuername);
    if(strncmp(embedissuername, "Root", 4) !=0){
      /* its the CA : add the root */
      bzero(tmpname2, sizeof(BbServerName));
      sprintf(tmpname2, "Root-%s", tmpname1);
      strcpy(tmpname1, tmpname2);
    }
    substr=strrchr(embedsubjectname,'-');
    if(substr){
        bzero(tmpname2, sizeof(BbServerName));
        strcpy((char *)tmpname2,substr+1);
    }
    generateUnsignedRSACert(BB_CERT_TYPE_SERVER, sigtype, 0, substr?tmpname2:embedsubjectname, tmpname1,  certpublickey, certexponent, certdata, SIZE_RSA_CERTBLOB_WORDS);
      
  }


  /* compute hash */
  
  SHA1Reset(&sha);
  SHA1Input(&sha, (u8 *)&(certdata[0]),BB_RSA_CERT_SIGNED_BYTES);
  SHA1Result(&sha, hash_data);
  
  /* now go sign the cert */
  signRsa(hash_data, prsa2, certsign);

  /* copy the sign to its rightful place : already right endianness*/
  
  for (i = 0; i <  num_bytes/4; i++){
    certdata[(BB_RSA_CERT_SIGNED_BYTES/4) + i] = (certsign[i]);     
  }
        
  /*just return cert data */
  free(prsa1);
  free(prsa2);
  free(padded_data);
  free(verify);
}

int verifyTicketSig(BbTicket *ticket, BbRsaCert *signer)
{
    SHA1Context sha;
    unsigned char hashData[20];
    u32 signerexponent;
    u8 sigPaddedHash[512];
    u32 certPubKeyLE[512];
    u32 sigLE[512];
    int sigBytes = sizeof(BbRsaSig2048);
    int i,j,equal;
    BSL_error result;

    SHA1Reset(&sha);
    SHA1Input(&sha, (u8 *)ticket, sizeof(BbTicket) - sigBytes);
    SHA1Result(&sha, hashData);

    signerexponent= htonl(signer->exponent);
    for(i=0; i<sigBytes/4; i++){
        certPubKeyLE[i] = htonl(signer->publicKey[i]);
        sigLE[i] = htonl(ticket->head.ticketSign[i]);
    }
    result = bsl_rsa_verify(hashData, sigLE, certPubKeyLE, 
                   &signerexponent, sigBytes*8);
    return result;
}

/* XXX: initial version will only work with RL signed by cert.
 *      support for RL signed by root must be added.
 */
int verifyRlSig(BbCrlHead *head, u8 *crl, BbRsaCert *signer)
{
    SHA1Context sha;
    unsigned char hashData[20];
    u32 signerexponent;
    u8 sigPaddedHash[512];
    u32 certPubKeyLE[512];
    u32 sigLE[512];
    int sigBytes = sizeof(BbRsaSig2048);
    int i,j,equal;
    BSL_error result;

    SHA1Reset(&sha);
    SHA1Input(&sha, (u8 *)(&head->type), sizeof(BbCrlHead) - sizeof(BbGenericSig));
    SHA1Input(&sha, crl, htonl(head->numberRevoked) * sizeof(BbCrlEntry));
    SHA1Result(&sha, hashData);

    signerexponent= htonl(signer->exponent);
    for(i=0; i<sigBytes/4; i++){
        certPubKeyLE[i] = htonl(signer->publicKey[i]);
        sigLE[i] = htonl(head->signature.rsa2048[i]);
    }
    result = bsl_rsa_verify(hashData, sigLE, certPubKeyLE, 
                   &signerexponent, sigBytes*8);
  
    return result;
}

#endif
