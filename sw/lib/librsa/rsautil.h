#include <stdio.h>
#include <string.h>
#include <openssl/rsa.h>
#include <openssl/bn.h>
#include <openssl/rand.h>
#include <netinet/in.h>
#include <bbsec.h>


#define MAX_KEY_SIZE 4096

#define get_word(x)	*(u32*)(x)

void readRsaData(FILE *readptr, RSA *prsa);
void saveRsaData(FILE *writeptr, RSA *prsa);

void readRsaDataNamed(FILE *readptr, RSA *prsa, BbServerName name);
void saveRsaDataNamed(FILE *writeptr, RSA *prsa, BbServerName name);

/* takes in key material, signs random data and verifies using libcryptoX86
 */
int validate(RSA *prsa, int num_bits);

/* takes in byte strings from openssl
 * and converts to host word order
 */
void convertToHost(u8 *e_string, u8 *n_string, u8 *sign, 
	      int e_size, int n_size, 
	      u32 *certexponent, u32 *certpublickey, u32 *certsign);
#if 0
int verifyCertSign( void *subjectcert, int subjectsize, 
		    u32 signerexponent, 
		    u8 *signerpublic, int signerpublicsize);
void
generateCertFromKeyData(u8 *keydatafile, u8 *signerkeydatafile, u8 *subjectname, u8 *issuername, unsigned long *certdata);

void
signRsa(u8 *hash_data, RSA * prsa2, unsigned long *certsign);

int verifyTicketSig(BbTicket *ticket, BbRsaCert *signer);
int verifyRlSig(BbCrlHead *head, u8 *crl, BbRsaCert *signer);
#endif
