SDK sample code

===============================================================================

aac sample code by Chaojian

It shows you how to use the interface provided by soft_aacdec lib

===============================================================================

T2Video_Sample by Gauss

This sample is to demostrate the basic function call of t2Video library. 
This is one of the small projects which have been collected in SDK-SAMPLE-CODE 
        directory.

=================================================================================

devicewitch sample code by Wen

This is  a sample program to show you how to switch among exculsively accessible devices.


=================================================================================

disc usb test sample code by Kenneth


This is T6304 Disc library, usb library, harddisk lib test pattern.
 
(1) It play mp3 file in dvd loader, usb disk, harddisk. it will search all mp3 file 
 at the root directory.
 
(2) It play ccda in dvd loader.
 
(3) It display information about usb keyboard and usb mouse.

=================================================================================

dvdlib sample code by Wing

It shows you how to call the functions in DVD library.

(1) It can detect DVD/VCD/SVCD/CDDA disc.

(2) It can accept user input from IR, and control dvd playback, and show some
messages in OSD.

=================================================================================

mp3 sample code by Chaojian

It shows you how to use the interface provided by soft_mp3dec lib

=================================================================================

ogg sample code by Frank

It shows you how to calling corresponding functions to decode ogg file

=================================================================================

sample code by Michael, Germy, John.

(1) It shows you how to call the functions in OS,DE, network, etc. libraries.
(2) It shows you how to use mpeg1/2(including transport stream) decoder
(3) It shows you how to use mpeg4 decoder
(4) It shows you how to use MP3/WMA dsp-based decoders.

=================================================================================

socket sample code by Shipman
 
This is a small and simply sample to show the basic usage of TCP/IP stack. 
(1) It inclues the usage of DHCP Client API;
(2) It inclues the usage of DNS Client API;
(3) It inclues the basic usage of Socket API.

=================================================================================

wmadec sample code by John

It shows you how to use the API provided by soft_wmadec.a lib

=================================================================================

UDISK update sample code by Joe

This is a small to show how to create function for loading an application
 from UDISK to update FIRMWARE.
(1) It includes the basic usage of T2FS application interface.
(2) It includes the basic usage of T2OS application interface.
(3) It includes the USB/UDISK library function call usage also.
(4) And it calls some initializing BIOS functions.

=================================================================================

WAV sample code by Chaojian

This is a small to show how to play wav PCM file with DSP lib.
(1) It includes the basic usage of T2DSP application interface.
(2) It includes the basic usage of T2OS application interface.

=================================================================================

samplecodeforgsilibrary  By Victor

It shows you how to call the functions in gsi library.

It will show the key code of panel's key when you press it.
=================================================================================

samplecodeforI2Clibrary  By Victor

It shows you how to call the functions in I2c library.

It access eeprom by i2c port.

=================================================================================
FS_SAMPLE By Joe
	This is a small sample to demostrate the basic function of T2-FILESYSTEM
library.
	It includes a simple MP3 player for Ts087 platform to demostrate 
the media files playing back. Also a set of benchmarking function to profile
the filesystem and block devices(UDISK/Internal IDE HARDDISK) transferring
performance. T2-FILESYSTEM is designed as OS independ. but the HARDDISK driver 
and USB driver is designed base on T2OS. So, this sample comes together with the
OS startup code, works together with T2OS library.

===================================================================================
samplecodeforEEPROM&I2Clibrary By Victor

This sample is to access eeprom by i2c port. It will be write the data
which storage in address 0xa0100000 to eeprom,and then read data from eeprom storage
in address 0xa0200000 and compare them with source data, if there is some different 
with source data then will be show the error's message in the logfile.
show you how to call the interfaces of i2c library.

==================================================================================
samplecode_forwrite_93c46_rtl8139 By William

This sample demostrate how to modify the content of 93c46 which used for saving RTL8139 
LAN chip info.

==================================================================================

Image_sample By Chaojian

This is a sample program that show you how to use the interface provided by libimage.a
lib.

====================================================================================
flash_updated_sample by Wen

This is a standalone program that show you how to update the flash image by reading the flash image in  cd or usb disk. 