/*************************************************************
	OS ENTRY
	(C)T-SQUARE

	This file is a part of T2-RTOS version 2.9
		All right reserved by T-SQUARE
	
	PURPOSE:
	Jump to the rtos startup routine.

	History:
		March, 2003	Created by joe @ t2-design
*************************************************************/

	.text
	.align  4
	.globl  _osstart
	.ent    _osstart
_osstart:
	li		$4,		1				#1 for cache mode, 0 for uncache mode 
	la		$8,		_t2os_entry
	lui		$9,		0xa000
	or		$8,		$9
	jr		$8
	nop
	.end    _osstart
