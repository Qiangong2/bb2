/*---------------------------------------------------------------------------------------------
    
    Copyright (C) 2002 T2-Design Corp.  All rights reserved.
    
    File: t2typedef.h
    
    Content: 
    	This file defines all the general typedef used in M63XX FW
    
    History: 
    2002/9/24 by Michael Xie
    	Created
        
---------------------------------------------------------------------------------------------*/
#ifndef _T2TYPEDEF_H
#define _T2TYPEDEF_H

/* BOOL type */

#define TRUE    1
#define FALSE   0
#define BOOL int

/* Define NULL */
#define NULL ((void *)0)

#define C_OFF               0
#define C_ON                1

#define MIN_INDEX           1
#define C_INVAL_INDEX       0xFFFF
#define C_INVAL_NUM			0xFFFF

/* Define Integers */
typedef char            INT8;
typedef short           INT16;
typedef long            INT32;
typedef INT8*           LPINT8;
typedef INT16*          LPINT16;
typedef INT32*          LPINT32;

/* Define Unsigned Integers */
typedef unsigned char   UINT8;
typedef unsigned short  UINT16;
typedef unsigned long   UINT32;
typedef UINT8*       LPUINT8;
typedef UINT16*      LPUINT16;
typedef UINT32*      LPUINT32;

typedef short       WCHAR;
typedef short *     LPWCHAR;
typedef short *     LPWSTR;
typedef char        CHAR;
typedef char *      LPCHAR;
typedef char *      LPSTR;
typedef void *      PVOID;
typedef void *      LPVOID;

typedef int BOOLEAN; 

typedef unsigned long long UINT64;
typedef long long INT64;

/* Define BYTE, WORD, DWORD */
typedef unsigned char BYTE;
typedef unsigned short WORD;
typedef unsigned long DWORD;
typedef BYTE  *         LPBYTE;
typedef WORD  *         LPWORD;
typedef DWORD *         LPDWORD;

// Define FirmWare Status
typedef DWORD         FW_STATUS;

typedef enum    {
    OK=0, Not_Available=1, TimeOut=2, Fail=3, Finish = 4, NotSure = 5
} tEndCondition;

#endif
