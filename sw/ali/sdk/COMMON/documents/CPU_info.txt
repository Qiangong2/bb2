CPU information:

T2-RISC CPU is a 32bit RISC CPU that is binary compatible with the MIPS32 instruction set except the following:

-It does not support the LWL / LWR / SWL / SWR unaligned memory access instructions
-It does not have a floating point unit

It has 16 kbyte of 2 way instruction cache and 16 kbyte of 2 way write back data cache

The physical CPU memory map is as follows:

0x0000.0000 - 0x0800.0000 Up to 128 MByte of SDRAM space.Real available memory depends on board configuration
0x1000.0000 - 0x1800.8fff register and PCI I/O space
0x3000.0000 - 0x3fff.ffff PCI memory space
0x1fc00.0000 - 0x1fc40.0000 Up to 4 MByte of Flash.Real available flash depends on board configuration

Since the CPU follows the MIPS standard the CPU virtual memory map is as follows

0x0000.0000 - 0x7fff.ffff TLB mapped space
0x8000.0000 - 0x9fff.ffff Cached direct memory access space
0xa000.0000 - 0xbfff.ffff Uncached direct memory access space

Only SDRAM and PCI memory space can be accessed in cached mode.
Flash and register I/O space can only be accessed in uncached mode.

So if you want to access SDRAM cached just use 0x8000.0000 to 0x8800.0000
to access it uncached, use 0xa000.0000 to 0xa800.0000

Since our memory is UMA, and our write back cache does not snoop the address bus it is neccessary to 
flush the data cache after updating for example the frame buffer, so that the data will be properly written back 
to be read by the display engine. Reading from DMA enabled devices should also be done uncached or the cache should
be invalidated before accessing this memory area.

 


