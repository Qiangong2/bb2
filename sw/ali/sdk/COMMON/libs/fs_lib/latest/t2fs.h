/*----------------------------------------------------------------------------
        Copyright (C) 2002 T2-Design Corp.  All rights reserved.

        File: t2stdio.h

        Content:
                T2-Filesystem public interface definitions.

        History:
        2002/9/19 by Joe Zhou
                Created this file.
----------------------------------------------------------------------------*/

#ifndef __T2FS_H
#define __T2FS_H


/* !!! DO NOT TOUCH THIS FILE !!! */

#define MAX_FN_LEN  	256 /* Max filename length */

/* FS IDs */
#define FS_ISO          1	/* filesystem identifier definitions  */
#define FS_VFAT         2
#define FS_UDF			3
#define FS_NET			4
#define FS_SC 			5

/* DEV IDs */
#define DEV_CD0        	0x00000101
#define DEV_HD0		   	0x00000004
#define DEV_USB0       	0x00000002    // -- 0x0000FF02  (sub id is 0x00-0xFF)
#define DEV_USB1       	0x00000003    // -- 0x0000FF03  (sub id is 0x00-0xFF)
#define DEV_NET        	0x01000001    /* net fs */
#define DEV_SC         	0x01000002    /* internet radio */

/* SUB ID handling */
#define MAJOR_ID(x)		((x) & 0xf)
#define SUB_ID(x)		(((x) >> 8) & 0xffff)
#define DEV_ATR(x)		((x) >> 24)

/* BLK DEVICE I/O commands */
#define IOC_READ		0
#define IOC_WRITE 		1
#define IOC_SYNC		2

/* ERRs definition */
#define EOK             0
#define EOF				(-1)
#define EDEV            (-2)
#define ERDONLY         (-3)
#define EEJECTED        (-4)
#define ENOMEDIA        (-5)
#define EPROCESS        (-6)
#define ENOTMNT         (-7)
#define EINVAL          (-8)
#define EBADF			(-9)
#define EEXIST          (-10)
#define ENOSPC          (-11)
#define ENOMEM          (-12)
#define ENOENT          (-13)
#define EIO             (-14)
#define EBUSY           (-15)
#define ENOTDIR         (-16)
#define ENOTFILE        (-17)
#define ENOTEMPTYDIR    (-18)
#define ENOTFOUND		(-19)
#define EDESIGN			(-20)
#define EBADOP			(-21)

/*	Seeking modes  */
#define SEEK_SET    0
#define SEEK_CUR    1
#define SEEK_END    2

#define NULL		((void *)0)
typedef unsigned int FILE;
typedef unsigned int DIR;
typedef unsigned int size_t;
typedef long off_t;

/* Directory entry; all handlers must conform to this interface */
typedef struct {
	int		size;	/* if size < 0, than it is a directory */
	char	name[MAX_FN_LEN];
} dirent_t;


typedef struct {
	off_t	st_size;	/* total size */
} stat_t;

/* BLOCK DEVICE callback routine prototype */
typedef int (*devop_t)(unsigned int dev, unsigned int cmd, unsigned int blk_size,
                  unsigned int blk_nr, unsigned int scts, unsigned char *buf);

typedef struct {
	int (*u2l)(unsigned short find, unsigned short *local);
	int (*l2u)(unsigned short find, unsigned short *unicode);
} nls_translator_t;

typedef struct {
	void (*fs_lock)(void);
	void (*fs_unlock)(void);
} vfs_parameter_t;

struct mount_opt {
	unsigned char *devbuf;
	unsigned int devbufsz;
};

/* Mounting */
int t2mount(unsigned int blkdev_id, unsigned int fs_id, const char *dir, struct mount_opt *opt);
int t2umount(unsigned int blkdev_id);

/* File operation */
FILE *t2fopen(const char *path, const char *mode);
size_t t2fread(void *ptr, size_t size, size_t nmemb, FILE *stream);
int t2fseek(FILE *stream, long offset, int whence);
off_t t2ftell(FILE *stream);
size_t t2fwrite(const void *ptr, size_t size, size_t nmemb, FILE *stream);
int t2fclose(FILE *stream);
int t2fstat(FILE *stream, stat_t *buf);	/* Getting information */
int t2fsync(FILE *file);					/* Synchronizing */
int t2unlink(const char *path);			/* Deletion */
int t2feof(FILE *file);
int t2ferror(FILE *file);
void t2fclearerr(FILE *file);

/* Directory operation */
int t2mkdir(const char *path);
DIR *t2opendir(const char *path);
int t2readdir(DIR *dir, dirent_t *dirp);
int t2rewinddir(DIR *dir);
int t2closedir(DIR *file);
int t2rmdir(const char *path);			/* Deletion */

/* Device handling and initialization */
int vfs_init(vfs_parameter_t *para);
int nls_init(nls_translator_t *t);
int fs_vfat_init(unsigned int fs_id);
int fs_udf_init(unsigned int fs_id);
int fs_iso9660_init(unsigned int fs_id);
int fs_net_init(unsigned int fs_id);
int def_dev(unsigned int devid, devop_t op);

/* Formating */
int mk_fat(unsigned int device_id, unsigned int blk_nr, unsigned char *buffer, unsigned int bufsize);

#endif /* __T2FS_H */
