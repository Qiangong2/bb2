/*
 * Copyright (c) 2001-2003 Swedish Institute of Computer Science.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 *
 * This file is part of the lwIP TCP/IP stack.
 * 
 * Author: Adam Dunkels <adam@sics.se>
 *
 */

#ifndef __LWIP_SOCKETS_H__
#define __LWIP_SOCKETS_H__
/*
 *Basic system type definitions.
 */
typedef unsigned   char    u8_t;
typedef signed     char    s8_t;
typedef unsigned   short   u16_t;
typedef signed     short   s16_t;
typedef unsigned   long    u32_t;
typedef signed     long    s32_t;

/*
 *Definitions for error constants.
 */
#define ERR_OK    0      /* No error, everything OK. */
#define ERR_TMO  -1		 /* Function timeout.*/	      
#define ERR_ERR  -2		 /* Function error.*/

/*
 *Structure define for IP address.
 */
struct ip_addr {
  u32_t addr;
};
#define IP4_ADDR(ipaddr, a,b,c,d) (ipaddr)->addr = htonl(((u32_t)(a & 0xff) << 24) | ((u32_t)(b & 0xff) << 16) | \
                                                         ((u32_t)(c & 0xff) << 8) | (u32_t)(d & 0xff))

/*
 *Structure define Socket name.
 */
struct in_addr {
  u32_t s_addr;
};
struct sockaddr_in {
  u8_t sin_len;
  u8_t sin_family;
  u16_t sin_port;
  struct in_addr sin_addr;
  char sin_zero[8];
};
struct sockaddr {
  u8_t sa_len;
  u8_t sa_family;
  char sa_data[14];
};

#ifndef socklen_t
#  define socklen_t int
#endif


/*
 * Interface request structure used for socket
 * ioctl's.  All interface ioctl's must have parameter
 * definitions which begin with ifr_name.  The
 * remainder may be interface specific.
 */
#define	IFNAMSIZ	16
struct ifreq 
{
	union
	{
		char	ifrn_name[IFNAMSIZ];		/* if name, e.g. "en0" */
	} ifr_ifrn;
	
	union {
		struct	sockaddr ifru_addr;
		struct	sockaddr ifru_dstaddr;
		struct	sockaddr ifru_broadaddr;
		struct	sockaddr ifru_netmask;
		struct  sockaddr ifru_hwaddr;
		short	ifru_flags;
		int	ifru_ivalue;
		int	ifru_mtu;
//		struct  ifmap ifru_map;   //@@@@ ONLY support when needed.
		char	ifru_slave[IFNAMSIZ];	/* Just fits the size */
		char	ifru_newname[IFNAMSIZ];
		char *	ifru_data;
//		struct	if_settings ifru_settings; 	//@@@@ ONLY support when needed.
	} ifr_ifru;
};

#define ifr_name	ifr_ifrn.ifrn_name	/* interface name 	*/
#define ifr_hwaddr	ifr_ifru.ifru_hwaddr	/* MAC address 		*/
#define	ifr_addr	ifr_ifru.ifru_addr	/* address		*/
#define	ifr_dstaddr	ifr_ifru.ifru_dstaddr	/* other end of p-p lnk	*/
#define	ifr_broadaddr	ifr_ifru.ifru_broadaddr	/* broadcast address	*/
#define	ifr_netmask	ifr_ifru.ifru_netmask	/* interface net mask	*/
#define	ifr_flags	ifr_ifru.ifru_flags	/* flags		*/
#define	ifr_metric	ifr_ifru.ifru_ivalue	/* metric		*/
#define	ifr_mtu		ifr_ifru.ifru_mtu	/* mtu			*/
#define ifr_map		ifr_ifru.ifru_map	/* device map		*/
#define ifr_slave	ifr_ifru.ifru_slave	/* slave device		*/
#define	ifr_data	ifr_ifru.ifru_data	/* for use by interface	*/
#define ifr_ifindex	ifr_ifru.ifru_ivalue	/* interface index	*/
#define ifr_bandwidth	ifr_ifru.ifru_ivalue    /* link bandwidth	*/
#define ifr_qlen	ifr_ifru.ifru_ivalue	/* Queue length 	*/
#define ifr_newname	ifr_ifru.ifru_newname	/* New name		*/
#define ifr_settings	ifr_ifru.ifru_settings	/* Device/proto settings*/

/* Socket configuration controls. */
#define SIOCGIFADDR	0x8915		/* get PA address		*/
#define SIOCGIFHWADDR	0x8927		/* get hardware address		*/

/*
 *Type specification for socket.
 */
#define SOCK_STREAM     1
#define SOCK_DGRAM      2
#define SOCK_RAW        3

/*
 * Option flags per-socket.
 */
#define	SO_DEBUG		0x0001		/* turn on debugging info recording */
#define	SO_ACCEPTCONN	0x0002		/* socket has had listen() */
#define	SO_REUSEADDR	0x0004		/* allow local address reuse */
#define	SO_KEEPALIVE	0x0008		/* keep connections alive */
#define	SO_DONTROUTE	0x0010		/* just use interface addresses */
#define	SO_BROADCAST	0x0020		/* permit sending of broadcast msgs */
#define	SO_USELOOPBACK	0x0040		/* bypass hardware when possible */
#define	SO_LINGER		0x0080		/* linger on close if data present */
#define	SO_OOBINLINE	0x0100		/* leave received OOB data in line */

#define SO_DONTLINGER   (int)(~SO_LINGER)

/*
 * Additional options, not kept in so_options.
 */
#define SO_SNDBUF	0x1001		/* send buffer size */
#define SO_RCVBUF	0x1002		/* receive buffer size */
#define SO_SNDLOWAT	0x1003		/* send low-water mark */
#define SO_RCVLOWAT	0x1004		/* receive low-water mark */
#define SO_SNDTIMEO	0x1005		/* send timeout */
#define SO_RCVTIMEO	0x1006		/* receive timeout */
#define	SO_ERROR	0x1007		/* get error status and clear */
#define	SO_TYPE		0x1008		/* get socket type */

/*
 *To support loop back feature for multicast. 
 */
#define SO_MULTICAST_LOOPBACK_ON	 0xf001
#define SO_MULTICAST_LOOPBACK_OFF	 0xf000


/*
 * Structure used for manipulating linger option.
 */
struct linger {
       int l_onoff;                /* option on/off */
       int l_linger;               /* linger time */
};

/*
 * Level number for (get/set)sockopt() to apply to socket itself.
 */
#define	SOL_SOCKET	0xfff		/* options for socket level */

/*
 * Address families.
 */
#define AF_UNSPEC       0
#define AF_INET         2
#define PF_INET         AF_INET

/*
 * Protocols
 */
#define IPPROTO_IP                0               /* dummy for IP */
#define IPPROTO_ICMP            1               /* control message protocol */
#define IPPROTO_IGMP            2               /* internet group management protocol */
#define IPPROTO_TCP              6               /* tcp */
#define IPPROTO_UDP             17              /* user datagram protocol */

#define INADDR_ANY      0
#define INADDR_BROADCAST 0xffffffff

/* 
  *Flags we can use with send/ and recv 
  */
#define MSG_PEEK	       0x2              /* peek at incoming message */
#define MSG_DONTWAIT    0x40            /* Nonblocking i/o for this operation only */


/* 
  *IP options for use with WinSock 
  */
#define	IP_MULTICAST_IF 	9 /* old value 2 */
#define	IP_MULTICAST_TTL    10 /* old value 3 */
#define	IP_MULTICAST_LOOP   11 /* old value 4 */
#define	IP_ADD_MEMBERSHIP   12 /* old value 5 */
#define	IP_DROP_MEMBERSHIP  13 /* old value 6 */

/*
 * ip_mreq also in winsock.h for WinSock1.1,
 */ 
struct ip_mreq {
	struct in_addr imr_multiaddr;
	struct in_addr imr_interface;
};



/*
 * Commands for ioctlsocket(),  taken from the BSD file fcntl.h.
 *
 *
 * Ioctl's have the command encoded in the lower word,
 * and the size of any in or out parameters in the upper
 * word.  The high 2 bits of the upper word are used
 * to encode the in/out status of the parameter; for now
 * we restrict parameters to at most 128 bytes.
 */
#if !defined(FIONREAD) || !defined(FIONBIO)
#define IOCPARM_MASK    0x7f            /* parameters must be < 128 bytes */
#define IOC_VOID        0x20000000      /* no parameters */
#define IOC_OUT         0x40000000      /* copy out parameters */
#define IOC_IN          0x80000000      /* copy in parameters */
#define IOC_INOUT       (IOC_IN|IOC_OUT)
                                        /* 0x20000000 distinguishes new &
                                           old ioctl's */
#define _IO(x,y)        (IOC_VOID|((x)<<8)|(y))

#define _IOR(x,y,t)     (IOC_OUT|(((long)sizeof(t)&IOCPARM_MASK)<<16)|((x)<<8)|(y))

#define _IOW(x,y,t)     (IOC_IN|(((long)sizeof(t)&IOCPARM_MASK)<<16)|((x)<<8)|(y))
#endif

#ifndef FIONREAD
#define FIONREAD    _IOR('f', 127, unsigned long) /* get # bytes to read */
#endif
#ifndef FIONBIO
#define FIONBIO     _IOW('f', 126, unsigned long) /* set/clear non-blocking i/o */
#endif

/* Socket I/O Controls */
#ifndef SIOCSHIWAT
#define SIOCSHIWAT  _IOW('s',  0, unsigned long)  /* set high watermark */
#define SIOCGHIWAT  _IOR('s',  1, unsigned long)  /* get high watermark */
#define SIOCSLOWAT  _IOW('s',  2, unsigned long)  /* set low watermark */
#define SIOCGLOWAT  _IOR('s',  3, unsigned long)  /* get low watermark */
#define SIOCATMARK  _IOR('s',  7, unsigned long)  /* at oob mark? */
#endif

#ifndef O_NONBLOCK
#define O_NONBLOCK    04000
#endif

#ifndef FD_SET
  #undef  FD_SETSIZE
  #define FD_SETSIZE    64
  #define FD_SET(n, p)  ((p)->fd_bits[(n)/8] |=  (1 << ((n) & 7)))
  #define FD_CLR(n, p)  ((p)->fd_bits[(n)/8] &= ~(1 << ((n) & 7)))
  #define FD_ISSET(n,p) ((p)->fd_bits[(n)/8] &   (1 << ((n) & 7)))
  #define FD_ZERO(p)    memset((void*)(p),0,sizeof(*(p)))

  typedef struct fd_set {
          unsigned char fd_bits [(FD_SETSIZE+7)/8];
        } fd_set;
#endif

  struct timeval {
	  long    tv_sec;         /* seconds */
	  long    tv_usec;        /* and microseconds */
  };

  struct timezone {
  	  int tz_minuteswest;
  	  int tz_dsttime;
  };


/*
 *Error return value for socket APIs.
 */
#define INVALID_SOCKET  (-1)
#define SOCKET_ERROR    (-1)

/*
 *Stack init and netif config
 */
signed char  lwIPStackInit(struct ip_addr ipaddr, struct ip_addr gw,struct ip_addr mask,s16_t tskpri);
signed char  lwIPNetifInit(struct ip_addr ipaddr, struct ip_addr gw,struct ip_addr netmask,void *reserved);

/*
 *Information of library.
 */ 
const unsigned long GetIPStackLibVersion(void);
const unsigned long GetIPStackLibSubVersion(void);
const char *GetIPStackLibName(void);

/*
 *Definitions for Socket APIs.
 */
int lwip_accept(int s, struct sockaddr *addr, socklen_t *addrlen);
int lwip_bind(int s, struct sockaddr *name, socklen_t namelen);
int lwip_shutdown(int s, int how);
int lwip_getpeername (int s, struct sockaddr *name, socklen_t *namelen);
int lwip_getsockname (int s, struct sockaddr *name, socklen_t *namelen);
int lwip_getsockopt (int s, int level, int optname, void *optval, socklen_t *optlen);
int lwip_setsockopt (int s, int level, int optname, const void *optval, socklen_t optlen);
int lwip_close(int s);
int lwip_connect(int s, struct sockaddr *name, socklen_t namelen);
int lwip_listen(int s, int backlog);
int lwip_recv(int s, void *mem, int len, unsigned int flags);
int lwip_read(int s, void *mem, int len);
int lwip_recvfrom(int s, void *mem, int len, unsigned int flags,
		  struct sockaddr *from, socklen_t *fromlen);
int lwip_send(int s, void *dataptr, int size, unsigned int flags);
int lwip_sendto(int s, void *dataptr, int size, unsigned int flags,
		struct sockaddr *to, socklen_t tolen);
int lwip_socket(int domain, int type, int protocol);
int lwip_write(int s, void *dataptr, int size);
int lwip_select(int maxfdp1, fd_set *readset, fd_set *writeset, fd_set *exceptset,
                struct timeval *timeout);
int lwip_ioctl(int s, long cmd, void *argp);
u16_t htons(u16_t n);
u16_t ntohs(u16_t n);
u32_t htonl(u32_t n);
u32_t ntohl(u32_t n);

/*
 *Make user more convenient to get ERRNO.
 */
int lwip_getlasterror(void);
int lwip_getsockerror(int s);

/*
 * Make user more convenient to set IP address
 */
#define INADDR_NONE 0xFFFFFFFF
unsigned long inet_addr(const char *cp);
char *inet_ntoa(struct in_addr IP);

/*
 *@@@@
 *Because ONLY one netif is support now,
 *we will ignore parameter "netif"
 */
/*Multicast API. */
/**Host joins the host-group.*/
signed char  join_host_group(struct ip_addr groupaddr, void *netif, u32_t ttl);
/**Host leaves the host-group.*/
signed char  leave_host_group(struct ip_addr groupaddr, void *netif);

/*DHCP API.*/
/* Initialize netif for DHCP and get configuration from DHCP server.*/
signed char dhcp_request(unsigned long timeout_ms);
/* setup the requested DHCP options*/
void dhcp_setup_option(unsigned char option_code);
/*get DHCP information from lwIP*/
struct dhcp_option_info * dhcp_get_option(unsigned char option_code);	
/*stop dhcp client*/
void dhcp_shutdown(void);
/** DHCP options */
#define DHCP_OPTION_PAD 0
#define DHCP_OPTION_SUBNET_MASK 1 /* RFC 2132 3.3 */
#define DHCP_OPTION_TIME_OFFSET 2
#define DHCP_OPTION_ROUTER 3 
#define DHCP_OPTION_TIME_SERVER 4
#define DHCP_OPTION_NAME_SERVER 5
#define DHCP_OPTION_DNS_SERVER  6 
#define DHCP_OPTION_LOG_SERVER 7
#define DHCP_OPTION_COOKIE_SERVER 8
#define DHCP_OPTION_LPR_SERVER 9
#define DHCP_OPTION_IMPRESS_SERVER 10
#define DHCP_OPTION_RESOURCE_LOCATION_SERVER 11
#define DHCP_OPTION_HOSTNAME 12
#define DHCP_OPTION_BOOTFILE_SIZE 13
#define DHCP_OPTION_MERIT_DUMP_FILE 14
#define DHCP_OPTION_DOMAIN_NAME 15
#define DHCP_OPTION_SWAP_SERVER 16
#define DHCP_OPTION_ROOT_PATH 17
#define DHCP_OPTION_EXTENSIONS_PATH 18
#define DHCP_OPTION_IP_FORWARDING 19
#define DHCP_OPTION_NON_LOCAL_SOURCE_ROUTING 20
#define DHCP_OPTION_POLICY_FILTER 21
#define DHCP_OPTION_MAX_DATAGRAM_REASSEMBLY_SIZE 22
#define DHCP_OPTION_IP_TTL 23
#define DHCP_OPTION_PATH_MTU_AGING_TIMEOUT 24
#define DHCP_OPTION_PATH_MTU_PLATEAU_TABLE 25
#define DHCP_OPTION_MTU 26
#define DHCP_OPTION_ALL_SUBNETS_ARE_LOCAL 27
#define DHCP_OPTION_BROADCAST 28
#define DHCP_OPTION_PERFORM_MASK_DISCOVERY 29
#define DHCP_OPTION_MASK_SUPPLIER 30
#define DHCP_OPTION_PERFORM_ROUTER_DISCOVERY 31
#define DHCP_OPTION_ROUTER_SOLICITATION_ADDRESS 32
#define DHCP_OPTION_STATIC_ROUTE 33
#define DHCP_OPTION_TRAILER_ENCAPSULATION  34
#define DHCP_OPTION_ARP_CACHE_TIMEOUT 35
#define DHCP_OPTION_ETHERNET_ENCAPSULATION 36
#define DHCP_OPTION_TCP_TTL 37
#define DHCP_OPTION_TCP_KEEPALIVE_INTERVAL 38
#define DHCP_OPTION_TCP_KEEPALIVE_GARBAGE 39
#define DHCP_OPTION_NETWORK_INFORMATION_SERVICE_DOMAIN 40
#define DHCP_OPTION_NETWORK_INFORMATION_SERVER 41
#define DHCP_OPTION_NETWORK_TIME_PROTOCOL_SERVER 42
#define DHCP_OPTION_VENDOR_SPECIFIC_INFORMATION 43
#define DHCP_OPTION_NETBIOS_NAME_SERVER 44
#define DHCP_OPTION_NETBIOS_DATAGRAM_DISTRUBUTION_SERVER 45
#define DHCP_OPTION_NETBIOS_NODE_TYPE 46
#define DHCP_OPTION_NETBIOS_SCOPE 47
#define DHCP_OPTION_X_WINDOW_SYSTEM_FONT_SERVER 48
#define DHCP_OPTION_X_WINDOW_SYSTEM_DISPLAY_MANAGER 49
#define DHCP_OPTION_REQUESTED_IP 50 /* RFC 2132 9.1, requested IP address */
#define DHCP_OPTION_LEASE_TIME 51 /* RFC 2132 9.2, time in seconds, in 4 bytes */
#define DHCP_OPTION_OVERLOAD 52 /* RFC2132 9.3, use file and/or sname field for options */
#define DHCP_OPTION_MESSAGE_TYPE 53 /* RFC 2132 9.6, important for DHCP */
#define DHCP_OPTION_SERVER_ID 54 /* RFC 2131 9.7, server IP address */
#define DHCP_OPTION_PARAMETER_REQUEST_LIST 55 /* RFC 2131 9.8, requested option types */
#define DHCP_OPTION_MESSAGE 56
#define DHCP_OPTION_MAX_MSG_SIZE 57 /* RFC 2131 9.10, message size accepted >= 576 */
#define DHCP_OPTION_T1 58 /* T1 renewal time */
#define DHCP_OPTION_T2 59 /* T2 rebinding time */
#define DHCP_OPTION_VENDOR_CLASS_IDENTIFIER 60
#define DHCP_OPTION_CLIENT_ID 61
/*OPTION 62 IS NOT AVAILABLE IN DHCP SPEC. */
/*OPTION 63 IS NOT AVAILABLE IN DHCP SPEC. */
#define DHCP_OPTION_NETWORK_INFROMATION_SERVICE_DOMAIN  64 
#define DHCP_OPTION_NETWORK_INFROMATION_SERVICE_SERVER  65
#define DHCP_OPTION_TFTP_SERVERNAME 66
#define DHCP_OPTION_BOOTFILE 67
#define DHCP_OPTION_MOBILE_IP_HOME_AGENT 68 
#define DHCP_OPTION_SMTP_SERVER 69
#define DHCP_OPTION_POP3_SERVER 70
#define DHCP_OPTION_NNTP_SERVER 71 
#define DHCP_OPTION_DEFAULT_WWW_SERVER 72
#define DHCP_OPTION_DEFAULT_FINGER_SERVER 73
#define DHCP_OPTION_DEFAULT_IRC_SERVER 74
#define DHCP_OPTION_STREETTALK_SERVER 75
#define DHCP_OPTION_STDA_SERVER 76
#define DHCP_OPTION_IP_ADDR  77   
#define DHCP_OPTION_END 255
/*struct for option information*/
struct dhcp_option_info
{
	void * ptr;				// start address of the option
	unsigned short len;			// length of the option 	
	unsigned short remain;			// remain number of the same option
};
/*type for request IP actions.*/
typedef enum {
	DeviceAddressing_AllocateNewAddress = 0,
	DeviceAddressing_RenewAddress,
	DeviceAddressing_Max
} DeviceAddressing_t;
/*perform request IP action for device.*/
signed char device_requestIP(unsigned long timeout_ms, DeviceAddressing_t type);
/*return value special for device addressing.*/
#define ADDRESS_DHCP   		(0)
#define ADDRESS_LINKLOCAL    (-1)    
#define ADDRESS_ERROR           (-2)   
#define ADDRESS_RENEWOK	    (1)
#define ADDRESS_RENEWERROR   (-3) 
#define ADDRESS_RENEWFAIL     (-4)
#define ADDRESS_ERRORTYPE      (-5) 


/*DNS API*/
/**init lwIP DNS client */
signed char dns_init(struct ip_addr p_dns_server,struct ip_addr a_dns_addr);
/**resolve the requested domain name */
signed char dns_query(void *domain_name, struct ip_addr *result_IP,u16_t flags);

#define accept(a,b,c)         lwip_accept(a,b,c)
#define bind(a,b,c)           lwip_bind(a,b,c)
#define shutdown(a,b)         lwip_shutdown(a,b)
#define close(s)              lwip_close(s)
#define connect(a,b,c)        lwip_connect(a,b,c)
#define getsockname(a,b,c)    lwip_getsockname(a,b,c)
#define getpeername(a,b,c)    lwip_getpeername(a,b,c)
#define setsockopt(a,b,c,d,e) lwip_setsockopt(a,b,c,d,e)
#define getsockopt(a,b,c,d,e) lwip_getsockopt(a,b,c,d,e)
#define listen(a,b)           lwip_listen(a,b)
#define recv(a,b,c,d)         lwip_recv(a,b,c,d)
#define read(a,b,c)           lwip_read(a,b,c)
#define recvfrom(a,b,c,d,e,f) lwip_recvfrom(a,b,c,d,e,f)
#define send(a,b,c,d)         lwip_send(a,b,c,d)
#define sendto(a,b,c,d,e,f)   lwip_sendto(a,b,c,d,e,f)
#define socket(a,b,c)         lwip_socket(a,b,c)
#define write(a,b,c)          lwip_write(a,b,c)
#define select(a,b,c,d,e)     lwip_select(a,b,c,d,e)
#define ioctlsocket(a,b,c)    lwip_ioctl(a,b,c)
#define getsockerror(a)	      lwip_getsockerror(a)
#define getlasterror	      lwip_getlasterror	

/* error number which is provied by lwIP.*/
#define	EPERM		 1	/* Operation not permitted */
#define	ENOENT		 2	/* No such file or directory */
#define	ESRCH		 3	/* No such process */
#define	EINTR		 4	/* Interrupted system call */
#define	EIO		 5	/* I/O error */
#define	ENXIO		 6	/* No such device or address */
#define	E2BIG		 7	/* Arg list too long */
#define	ENOEXEC		 8	/* Exec format error */
#define	EBADF		 9	/* Bad file number */
#define	ECHILD		10	/* No child processes */
#define	EAGAIN		11	/* Try again */
#define	ENOMEM		12	/* Out of memory */
#define	EACCES		13	/* Permission denied */
#define	EFAULT		14	/* Bad address */
#define	ENOTBLK		15	/* Block device required */
#define	EBUSY		16	/* Device or resource busy */
#define	EEXIST		17	/* File exists */
#define	EXDEV		18	/* Cross-device link */
#define	ENODEV		19	/* No such device */
#define	ENOTDIR		20	/* Not a directory */
#define	EISDIR		21	/* Is a directory */
#define	EINVAL		22	/* Invalid argument */
#define	ENFILE		23	/* File table overflow */
#define	EMFILE		24	/* Too many open files */
#define	ENOTTY		25	/* Not a typewriter */
#define	ETXTBSY		26	/* Text file busy */
#define	EFBIG		27	/* File too large */
#define	ENOSPC		28	/* No space left on device */
#define	ESPIPE		29	/* Illegal seek */
#define	EROFS		30	/* Read-only file system */
#define	EMLINK		31	/* Too many links */
#define	EPIPE		32	/* Broken pipe */
#define	EDOM		33	/* Math argument out of domain of func */
#define	ERANGE		34	/* Math result not representable */
#define	EDEADLK		35	/* Resource deadlock would occur */
#define	ENAMETOOLONG	36	/* File name too long */
#define	ENOLCK		37	/* No record locks available */
#define	ENOSYS		38	/* Function not implemented */
#define	ENOTEMPTY	39	/* Directory not empty */
#define	ELOOP		40	/* Too many symbolic links encountered */
#define	EWOULDBLOCK	EAGAIN	/* Operation would block */
#define	ENOMSG		42	/* No message of desired type */
#define	EIDRM		43	/* Identifier removed */
#define	ECHRNG		44	/* Channel number out of range */
#define	EL2NSYNC	45	/* Level 2 not synchronized */
#define	EL3HLT		46	/* Level 3 halted */
#define	EL3RST		47	/* Level 3 reset */
#define	ELNRNG		48	/* Link number out of range */
#define	EUNATCH		49	/* Protocol driver not attached */
#define	ENOCSI		50	/* No CSI structure available */
#define	EL2HLT		51	/* Level 2 halted */
#define	EBADE		52	/* Invalid exchange */
#define	EBADR		53	/* Invalid request descriptor */
#define	EXFULL		54	/* Exchange full */
#define	ENOANO		55	/* No anode */
#define	EBADRQC		56	/* Invalid request code */
#define	EBADSLT		57	/* Invalid slot */

#define	EDEADLOCK	EDEADLK

#define	EBFONT		59	/* Bad font file format */
#define	ENOSTR		60	/* Device not a stream */
#define	ENODATA		61	/* No data available */
#define	ETIME		62	/* Timer expired */
#define	ENOSR		63	/* Out of streams resources */
#define	ENONET		64	/* Machine is not on the network */
#define	ENOPKG		65	/* Package not installed */
#define	EREMOTE		66	/* Object is remote */
#define	ENOLINK		67	/* Link has been severed */
#define	EADV		68	/* Advertise error */
#define	ESRMNT		69	/* Srmount error */
#define	ECOMM		70	/* Communication error on send */
#define	EPROTO		71	/* Protocol error */
#define	EMULTIHOP	72	/* Multihop attempted */
#define	EDOTDOT		73	/* RFS specific error */
#define	EBADMSG		74	/* Not a data message */
#define	EOVERFLOW	75	/* Value too large for defined data type */
#define	ENOTUNIQ	76	/* Name not unique on network */
#define	EBADFD		77	/* File descriptor in bad state */
#define	EREMCHG		78	/* Remote address changed */
#define	ELIBACC		79	/* Can not access a needed shared library */
#define	ELIBBAD		80	/* Accessing a corrupted shared library */
#define	ELIBSCN		81	/* .lib section in a.out corrupted */
#define	ELIBMAX		82	/* Attempting to link in too many shared libraries */
#define	ELIBEXEC	83	/* Cannot exec a shared library directly */
#define	EILSEQ		84	/* Illegal byte sequence */
#define	ERESTART	85	/* Interrupted system call should be restarted */
#define	ESTRPIPE	86	/* Streams pipe error */
#define	EUSERS		87	/* Too many users */
#define	ENOTSOCK	88	/* Socket operation on non-socket */
#define	EDESTADDRREQ	89	/* Destination address required */
#define	EMSGSIZE	90	/* Message too long */
#define	EPROTOTYPE	91	/* Protocol wrong type for socket */
#define	ENOPROTOOPT	92	/* Protocol not available */
#define	EPROTONOSUPPORT	93	/* Protocol not supported */
#define	ESOCKTNOSUPPORT	94	/* Socket type not supported */
#define	EOPNOTSUPP	95	/* Operation not supported on transport endpoint */
#define	EPFNOSUPPORT	96	/* Protocol family not supported */
#define	EAFNOSUPPORT	97	/* Address family not supported by protocol */
#define	EADDRINUSE	98	/* Address already in use */
#define	EADDRNOTAVAIL	99	/* Cannot assign requested address */
#define	ENETDOWN	100	/* Network is down */
#define	ENETUNREACH	101	/* Network is unreachable */
#define	ENETRESET	102	/* Network dropped connection because of reset */
#define	ECONNABORTED	103	/* Software caused connection abort */
#define	ECONNRESET	104	/* Connection reset by peer */
#define	ENOBUFS		105	/* No buffer space available */
#define	EISCONN		106	/* Transport endpoint is already connected */
#define	ENOTCONN	107	/* Transport endpoint is not connected */
#define	ESHUTDOWN	108	/* Cannot send after transport endpoint shutdown */
#define	ETOOMANYREFS	109	/* Too many references: cannot splice */
#define	ETIMEDOUT	110	/* Connection timed out */
#define	ECONNREFUSED	111	/* Connection refused */
#define	EHOSTDOWN	112	/* Host is down */
#define	EHOSTUNREACH	113	/* No route to host */
#define	EALREADY	114	/* Operation already in progress */
#define	EINPROGRESS	115	/* Operation now in progress */
#define	ESTALE		116	/* Stale NFS file handle */
#define	EUCLEAN		117	/* Structure needs cleaning */
#define	ENOTNAM		118	/* Not a XENIX named type file */
#define	ENAVAIL		119	/* No XENIX semaphores available */
#define	EISNAM		120	/* Is a named type file */
#define	EREMOTEIO	121	/* Remote I/O error */
#define	EDQUOT		122	/* Quota exceeded */
#define	ENOMEDIUM	123	/* No medium found */
#define	EMEDIUMTYPE	124	/* Wrong medium type */

#endif /* __LWIP_SOCKETS_H__ */

