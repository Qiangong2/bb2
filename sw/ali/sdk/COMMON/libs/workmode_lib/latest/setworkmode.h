#ifndef _SETWORKMODE_H_
#define _SETWORKMODE_H_

enum SYS_SETWORKMODES
{
	SWITCH_TO_PCI_MODE,
	SWITCH_TO_IDE_MODE,
	ENABLE_FLASH_MODE,
	DISABLE_FLASH_MODE
};
/*---------------------------------------------------------
Name: 
    SetWorkMode
Description: 
    Set working mode. For now, this function always set
    TS087 board into PCI mode.
Parameters: 
    [IN]
    WorkMode: SWITCH_TO_PCI_MODE: PCI mode 
              SWITCH_TO_IDE_MODE: IDE mode
              ENABLE_FLASH_MODE: Enable Flash
              DISABLE_FLASH_MODE: Disable Flash
    [OUT]
Return: 
---------------------------------------------------------*/
void SetWorkMode(unsigned char workmode);
#endif