/*---------------------------------------------------------------------------------------------
    
    Copyright (C) 2002 T2-Design Corp.  All rights reserved.
    
    File: nulldecoder.h
    
    Content: 
    	This file declares the export function of null decoder,
    	all the decoder should have the same export functions as 
    	this file
  	
    History: 
    2002/9/24 by Michael Xie
    	Create
        
---------------------------------------------------------------------------------------------*/
#ifndef _NULLDECODER_H
#define _NULLDECODER_H

/*----------------------------------------------------
    
Name: 
    NULLDecoderInit
Description: 
    A sample function, decoder should do initialization 
    in this function, this function is called whenever
    the decoder is first called
    
Parameters: 
    IN:
    
    OUT:
    
Return: 
	FALSE, decoder needs to read data by itself, i.e. call 
    DecoderReadData function in decoder, such as, AVI 
    decoder. For those decoders never call DecoderReadData 
    shold always return TRUE, such as VCD/CDDA etc.
------------------------------------------------------*/
BOOL NULLDecoderInit(DEC_INIT_PAR DecInitPar);

/*----------------------------------------------------
    
Name: 
    NULLDecoderReset
Description: 
    A sample function, decoder should do RESET in this 
    function, this function is called whenever the 
    decoder needs to be reset
    
Parameters: 
    IN:
    
    OUT:
    
Return: 
	TRUE, if success, otherwise FALSE    
------------------------------------------------------*/
BOOL NULLDecoderReset();

/*-------------------------------------------------------------------
Name: 
    NULLDecoderGetCap
Description: 
    A sample function, application can call this function to
    get the CAP of the decoder
Parameters: 
    [IN]
    [OUT]
    DecoderCap: the CAP of the decoder returned by the decoder
Return: 
    TRUE: if success, otherwise, FALSE. 
-------------------------------------------------------------------*/
BOOL NULLDecoderGetCap(t2DecoderCap *DecoderCap);

/*----------------------------------------------------
    
Name: 
    NULLDecoder
Description: 
    A sample function, decoder implements main decoding
    in this function, this function is called when the 
    new data are sent to decoder
    
Parameters: 
    IN:
	    DecoderBuffer: decoder buffer structure which 
	    points to the address of the new data
    OUT:
    
Return: 
    TRUE, if success, otherwise FALSE
------------------------------------------------------*/
BOOL NULLDecoder(t2DecoderBuffer *DecoderBuffer);

/*-------------------------------------------------------------------
Name: 
    NULLGetLastErrMsg
Description: 
    A sample function, application can call this function to get 
    decoder error message type.
Parameters: 
    [IN]
    [OUT]
Return: 
    Error message type.
-------------------------------------------------------------------*/
DWORD NULLGetLastErrMsg();

#endif
