/*---------------------------------------------------------------------------------------------
    
    Copyright (C) 2002 T2-Design Corp.  All rights reserved.
    
    File: cddadecoder.h
    
    Content: 
	    This file declares the export function of CDDA player
    
    History: 
    2002/09/7 by Michael Xie
        Created
---------------------------------------------------------------------------------------------*/
#ifndef _CDDADECODER_H
#define _CDDADECODER_H

/*----------------------------------------------------
    
Name: 
    CDDADecoderInit
Description: 
    Init CDDA playback, for now, nothing is done in this
    function
    
Parameters: 
    IN:
    
    OUT:
    
Return: 
	TRUE, if success, otherwise FALSE    
------------------------------------------------------*/
BOOL CDDADecoderInit(DEC_INIT_PAR DecInitPar);

/*----------------------------------------------------
    
Name: 
    CDDADecoderReset
Description: 
    Reset CDDA playback
    
Parameters: 
    IN:
    
    OUT:
    
Return: 
	TRUE, if success, otherwise FALSE    
------------------------------------------------------*/
BOOL CDDADecoderReset();

/*----------------------------------------------------
    
Name: 
    CDDADecoder
Description: 
    This function plays CDDA data
Parameters: 
    IN:
	    DecoderBuffer: decoder buffer structure which 
	    points to the address of the new data
    OUT:
    
Return: 
    TRUE, if success, otherwise FALSE
------------------------------------------------------*/
BOOL CDDADecoder(t2DecoderBuffer *DecoderBuffer);

#endif
