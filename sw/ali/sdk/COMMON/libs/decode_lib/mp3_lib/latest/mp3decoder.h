#ifndef __MP3DECODER_H
#define __MP3DECODER_H
BOOL MP3DecoderInit(DEC_INIT_PAR DecInitPar);
BOOL MP3DecoderReset();
BOOL MP3Decoder(t2DecoderBuffer *DecoderBuffer);
BOOL MP3DecoderGetCap(t2DecoderCap *DecoderCap);
#endif
