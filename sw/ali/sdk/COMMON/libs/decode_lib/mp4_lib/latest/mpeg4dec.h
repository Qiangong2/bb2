 #ifndef _MPEG4DEC_H
#define _MPEG4DEC_H

#include "t2mpgdec.h"

//video stream type definition
#define MS_MPEG4	1
#define OPEN_DIVX	2
#define DIVX50		3

#ifdef __cplusplus
extern "C"{
#endif

BOOL	Mpeg4DecoderGetCap(t2DecoderCap *DecoderCap);
BOOL	Mpeg4DecoderInit(DEC_INIT_PAR DecInitPar);
BOOL	Mpeg4DecoderReset();
BOOL	Mpeg4Decoder(t2DecoderBuffer *DecoderBuffer);
DWORD	Mpeg4DecoderGetLastErrMsg();
DWORD	MP4GetCurrentFrame();
DWORD	MP4GetTotalAudioTracks();
BOOL	Mpeg4SetFrameBufferNumber(int nFramebufferNumber);

#ifdef __cplusplus
}
#endif

#endif //_MPEG4DEC_H
