#ifndef __WMADECODER_H
#define __WMADECODER_H
BOOL WMADecoderInit(DEC_INIT_PAR DecInitPar);
BOOL WMADecoderReset();
BOOL WMADecoder(t2DecoderBuffer *DecoderBuffer);
BOOL WMADecoderGetCap(t2DecoderCap *DecoderCap);
#endif
