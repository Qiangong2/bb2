=================================
 MPg library (Ver 1.10) Change 
		--2004.04.21 Neo
=================================

Fixed the bug in reseting HW MC.

=================================
 MPg library (Ver 1.9) Change 
		--2004.04.13 Neo
=================================

Support search time function. This function will cause a call to function "DecoderSeekData". 
NOTE: Always set "SearchTime" to "INVALID_TIME(0xffffffff)", if not implementing search time 
function.

=================================
 MPg library (Ver 1.8) Change 
		--2004.04.06 Neo
=================================

Fixed "Pan&Scan position" bug in DVD

=================================
 MPg library (Ver 1.8) Change 
		--2004.03.31 Neo
=================================

Fixed "sync" problem when playing certain mpeg stream

=================================
 MPg library (Ver 1.7) Change 
		--2004.03.26 Neo
=================================

Updated mpeg lib which has fixed "green-block" problem

=================================
 MPg library (Ver 1.5) Change 
		--2004.03.17 Neo
=================================

Updated mpeg lib which improved "macro-blocking" artifacts problem. This version of mpeg decoder increase "SYS_VIDEO_BUFFER_SIZE" from 512K to 640K
=================================
 MPg library (Ver 1.3) Change 
		--2004.01.09 Neo
=================================

mpg_lib decreased "mosaic block" effect when playback is not smooth in last version.
=================================
 MPg library (Ver 1.2) Change 
		--2004.01.02 Neo
=================================
Optimized bitstream reading routine in mpeg file decoder

=================================
 MPg library (Ver 1.0) Change 
		--2003.10.09 Neo
=================================
Updated mpeg lib with fixing few small bugs
=================================
 MPg library (Ver 0.9) Change 
		--2003.9.22 Neo
=================================
Updated mpeg lib with removing internal "debug_printf"

=================================
 MPg library (Ver 0.8) Change 
		--2003.9.13 Neo
=================================
Update mpeg lib which doesnt call "DecoderSeekData" function any more, if dont do FF/FR.




