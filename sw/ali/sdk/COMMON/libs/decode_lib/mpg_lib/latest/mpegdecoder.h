#ifndef _MPEGDECODER_H
#define _MPEGDECODER_H

#ifdef __cplusplus
extern "C"{
#endif

BOOL MPEGDecoderGetCap(t2DecoderCap *DecoderCap);
BOOL MPEGDecoderInit(DEC_INIT_PAR DecInitPar);
BOOL MPEGDecoderReset();
BOOL MPEGDecoder(t2DecoderBuffer *DecoderBuffer);
unsigned char MPGGetCurPicType();

#ifdef __cplusplus
}
#endif

#endif //_MPEGDECODER_H

