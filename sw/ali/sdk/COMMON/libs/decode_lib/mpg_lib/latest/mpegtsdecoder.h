#ifndef _MPEGTSDECODER_H
#define _MPEGTSDECODER_H

#ifdef __cplusplus
extern "C"{
#endif

BOOL TSDecoderGetCap(t2DecoderCap *DecoderCap);
BOOL TSDecoderInit(DEC_INIT_PAR DecInitPar);
BOOL TSDecoderReset();
BOOL TSDecoder(t2DecoderBuffer *DecoderBuffer);

#ifdef __cplusplus
}
#endif

#endif //_MPEGTSDECODER_H

