
#ifndef _MPGFILEDECODER_H
#define _MPGFILEDECODER_H

#ifdef __cplusplus
extern "C"{
#endif

BOOL MPGFileDecoderGetCap(t2DecoderCap *DecoderCap);
BOOL MPGFileDecoderInit(DEC_INIT_PAR DecInitPar);
BOOL MPGFileDecoder(t2DecoderBuffer *DecoderBuffer); 
BOOL MPGFileDecoderReset(); 
DWORD MPGFileDecoderGetPlayTime();
DWORD MPGFileDecoderGetLastErrMsg();
int MPGGetTotalPlayTime();

#ifdef __cplusplus
}
#endif

#endif


