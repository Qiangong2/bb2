Apr 22, 2004	version 0.7.1.2
	Remove buggy profiling function temporarily.
Apr 21, 2004	version 0.7.1.1
	Fixed head file, removed debugging code and message.
Apr 2, 2004 	version 0.7.0.7
	Fixed sta_tsk() issue.
Apr 1, 2004	version 0.7.0.5
	Add ter_tsk()/ext_tsk() functions
Mar 26, 2004	version 0.7.0.1
	Added cre_tsk2()/fre_tsk()
Feb 20, 2004	version 0.6.2.5
	fixed the follow bug
	1)not Clear TLB entries when OS init
	2)schedual latency of API sta_tsk()
Feb 17, 2004		Version 0.6.2.0
	Fixed a bug related to scheduling control
Jan 8, 2004		version 0.6.1.1
	Cleaned code
Jan 2, 2004		Version 0.6.0.0
	Increase	bootup stack from 1K to 2K(512 words)
Dec 11, 2003	Version 0.5.9.2
	Removed	TLB initialized value setting, instead with clearing all TLB entries.
Nov 28, 2003	Version 0.5.9.0
	Added primitive ID allocating functions
Nov 21, 2003	Version 0.5.8.0
	Added Cache operation functions
Oct 28,	2003	Version 0.5.7.0 
	Added ref_sem() 
Oct 27, 2003	Version 0.5.6.0 
	Added alc_sem(), fre_sem()
Oct 9, 2003		version 0.5.5.0
	fixed scheduling bug.