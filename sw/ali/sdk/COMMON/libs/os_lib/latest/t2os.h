/*-----------------------------------------------------------------------------
    
    Copyright (C) 2002 T2-Design Corp.  All rights reserved.
    
    File: t2os.h
    
    Content:
    T2OS library declaration.

    History: 
    2004/03/31 by Joe Zhou
    	Add	xxx_xxx2 and related function declaration.
    2002/11/09 by Joe Zhou
        Created
        
-----------------------------------------------------------------------------*/

/* Please do NOT touch this file for any reason. */

#ifndef _T2OS_H_
#define _T2OS_H_

#define T2OS
#define _T2OS_
#define __T2OS__

#define TRUE 1
#define FALSE 0

#define NADR        ((VP)-1)

#define TA_ASM      0                   /* Assembler language program 	*/
#define TA_HLNG     1                   /* High-level language program 	*/

#define TA_TFIFO    0          			/* FIFO wait queue              */
#define TA_WMUL     0x00000008

#define TWF_ANDW    0x00000000
#define TWF_ORW     0x00000002
#define TWF_CLR     0x00000001


/* Timer interface */
#define TMO_POL     0
#define TMO_FEVR    (-1)

#define TCY_ON      0x00000001
#define TCY_OFF     0x00000000

#define TTM_ABS     0x00000000
#define TTM_REL     0x00000001


/* System call Error Code */
#define E_OK        0
#define E_NOMEM     (-10)
#define E_PAR       (-33)
#define E_PRI       (-34)
#define E_ID        (-35)
#define E_NOEXS     (-52)
#define E_OBJ       (-63)
#define E_CTX		(-69)
#define E_QOVR      (-73)
#define E_DLT       (-81)
#define E_TMOUT     (-85)

#define BOOL int

typedef char B;
typedef char VB;
typedef unsigned char UB;

typedef short BOOL_ID;
typedef short H;
typedef short ID;
typedef short PRI;

typedef unsigned int ATR;
typedef unsigned int TMO;
typedef unsigned int UINT;
typedef unsigned int UW;

typedef int INT;
typedef int ER;

typedef void *VP;
typedef void (*FP)(void);

typedef struct systime 
{
    H   utime;
    UW  ltime;
} SYSTIME;

typedef TMO     CYCTIME;
typedef SYSTIME ALMTIME;
typedef TMO     DLYTIME;

/* Task interface */
typedef struct t_ctsk {
    VP      exinf;
    ATR     tskatr; // dummy item
    FP      task;
    PRI     itskpri;
    UB      Reserved0;
    INT     stksz;
    TMO     quantum;
    UINT    stkbuf;
} T_CTSK;

typedef struct t_dcyc {
    VP      exinf;
    ATR     cycatr;
    FP      cychdr;
    UINT    cycact;
    CYCTIME cyctim;
    UINT    stksz;
    UINT    stkbuf;
	B		*name;
} T_DCYC;

typedef struct t_dalm {
    VP      exinf;
    ATR     almatr;
    FP      almhdr;
    UINT    tmmode;
    ALMTIME almtim;
    UINT    stksz;
    UINT    stkbuf;
	B		*name;
} T_DALM;

/* Semaphore interface */
typedef struct t_csem {
    VP      exinf;                      /* Not use, extention information */
    ATR     sematr;                     /* Not use, attribute: TA_TFIFO or TA_TPRI */
    INT     isemcnt;                    /* Initial semaphore count */
    INT     isemmax;                    /* Maximal semaphore count */
} T_CSEM;

typedef struct t_rsem {
	VP		exinf;						/* Reserved, extended information */
	BOOL_ID	wtsk;						/* indicates whether or not there is a
                                     		waiting task */
	INT		semcnt;						/* current semaphore count */
} T_RSEM;

/* Event flags interface */
typedef struct t_cflg {
    VP      exinf;
    ATR     flgatr;
    UINT    iflgptn;
} T_CFLG;

typedef struct t_rflg {
    VP      exinf;
    BOOL_ID wtsk;
    UINT    flgptn;
    ATR     flgatr;
} T_RFLG;

/* Message buffer interface */
typedef struct t_cmbf {
    VP      exinf;
    ATR     mbfatr;
    INT     bufsz;
    INT     maxmsz;
    UINT    reserved;                      /* Extension */
	VP		databuf;
} T_CMBF;

typedef struct 
{
	unsigned int timer_tick;
	unsigned int os_memory_ptr;
	unsigned int os_memory_size;
	unsigned int irq_stack_size;
	unsigned int sys_stack_size;
	unsigned int num_tsk;
	unsigned int num_cyc;
	unsigned int num_alm;
	unsigned int num_mbf;
	unsigned int num_sem;
	unsigned int num_flg;
	unsigned int ext_options;
}
oslib_parameter_t;

/* init and startup routines */
ER t2os_kernel_init(oslib_parameter_t *osp);
void t2os_kernel_start(void);

/* get version */
UINT t2os_getversion(void);

/* cache op */
void FlushDCache(unsigned int region, unsigned int size);
void InvalidateDCache(unsigned int region, unsigned int size);
void InvalidateICache(unsigned int region, unsigned int size);

/* task related */
ER alc_tsk(ID *p_id);		/* Allocate Task ID */
ER fre_tsk(ID tid);			/* Free Task ID */

ER cre_tsk2(ID, T_CTSK *);	/* Create Task */
ER del_tsk2(ID);			/* Delete Task */

ER sta_tsk(ID, INT);		/* Start Task */
ER ext_tsk(void);			/* Exit Issuing Task */
ER ter_tsk(ID tid);			/* Terminate Task */
ER dly_tsk(DLYTIME);		/* Sleep */


/* cyclic */
ER alc_cyc(ID *p_id);
ER fre_cyc(ID);

ER def_cyc2(ID, T_DCYC *);	/* define cyclic */
ER del_cyc2(ID);			/* Delete cyclic */

ER act_cyc(ID, UINT);		/* On/Off */


/* alarm */
ER alc_alm(ID *p_id);
ER fre_alm(ID);

ER def_alm2(ID, T_DALM *);
ER del_alm2(ID);



/* semaphore */
ER alc_sem(ID *p_id);
ER fre_sem(ID semid);

ER cre_sem(const ID, const T_CSEM *);
ER del_sem(ID);
ER wai_sem(ID);
ER sig_sem(ID);
ER preq_sem(ID);
ER twai_sem(ID, TMO);
ER ref_sem(T_RSEM *pk_rsem, ID semid);


/* message buffer */
ER alc_mbf(ID *p_id);
ER fre_mbf(ID semid);

ER cre_mbf(ID, T_CMBF *);
ER del_mbf(ID);
ER snd_mbf(ID, VP, INT);
ER psnd_mbf(ID, VP, INT);
ER tsnd_mbf(ID, VP, INT, TMO);
ER rcv_mbf(VP, INT *, ID);
ER prcv_mbf(VP, INT *, ID);
ER trcv_mbf(VP, INT *, ID, TMO);


/* flags */
ER alc_flg(ID *p_id);
ER fre_flg(ID semid);

ER cre_flg(const ID, const T_CFLG *);
ER del_flg(const ID flgid);
ER set_flg(ID, UINT);
ER clr_flg(ID, UINT);
ER wai_flg(UINT *, ID, UINT, UINT);
ER twai_flg(UINT *, ID, UINT, UINT, TMO);
ER pol_flg(UINT *, ID, UINT, UINT);
ER ref_flg(T_RFLG *, ID);


/* get os tick */
ER get_tim(SYSTIME *pk_tim);


/* dispatch controlling */
ER dis_dsp(void);
ER ena_dsp(void);	


/* interrupts */
ER def_int(UINT ino, FP irqno);		/* Install ISR */
ER del_int(UINT ino);				/* Delete ISR, (To intall another one) */
ER ena_int(UINT ino);			/* Enable Interrupt */
ER dis_int(UINT ino);			/* Disable Interrupt */


/* for backward compatible */
#define ALL_EXT					0x7FFC
void DisInt(void);				/* ena_int(ALL_EXT) */
void EnInt(void);				/* dis_int(ALL_EXT) */
ER def_alm(ID, T_DALM *);		/* !! there is no del_alm/del_cyc/del_tsk !! */
ER def_cyc(ID, T_DCYC *);
ER cre_tsk(ID, T_CTSK *);
ER ena_irq(ID ino);			/* work as ena_int */
ER def_irq(ID ino, FP irqno);	/* work as def_int */ 


#endif /* _T2OS_H_ */
