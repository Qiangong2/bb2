#ifndef _GSI_H
#define _GSI_H

typedef enum
{
	GSI_LSB=0,
	GSI_MSB
}GSI_LSBMSB;

typedef enum
{
	GSI_SDO_SCK_DEF_LOW=0,
	GSI_SDO_SCK_DEF_HIGH
}GSI_SDOSCKDEF;

void GSI_Init(unsigned int sb_clk,GSI_LSBMSB dir,GSI_SDOSCKDEF val);
int GSI_Transmit(void *sendbuf, void *recvbuf, int maxlength);

#endif
