ui lib version history:
[v0.98d, 2004-04-21]
1.Removed "SetWorkMode" function and call "SetWorkMode" in workmode lib

[v0.98, 2004-04-13]
1.Fixed some bugs

[v0.97c, 2004-04-06]
1.Clean up H file

[v0.97b, 2004-03-31]
1.Fixed some bugs.

[v0.97, 2004-03-26]
1.Fixed some bugs.

[v0.96b, 2004-03-23]
1.Fixed some bugs.

[v0.96a, 2004-03-19]
1.Fixed some bugs.

[v0.96, 2004-03-17]
1.Fixed some bugs.

[v0.95a, 2004-03-09]
1.Fixed some bugs.

[v0.92, 2004-01-13]
1.Created this library.



