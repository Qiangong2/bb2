/********************************************************************
// file TI2Cio.h													//
//	 TI2C IO read/write routine										//
// author:lby														//
//																	//
// date:2001-11-22													//
// 																	//
// 2001-11-24: the read/write cycle and data style is error,fix it	//
//	modify by Victor .	data 2001/12/11.															//
*********************************************************************/

#ifndef TI2CIO_H
#define TI2CIO_H

typedef unsigned char	UINT8;
typedef short		INT16;
typedef unsigned short	UINT16;
typedef int		INT32;
typedef unsigned int	UINT32;
typedef	INT32		STATUS;

#define SUCESS			0
#define ERRORMODE		-1
/******************************************************************
	Initialize I2C register.
Support:	M630x
Parameters:	clk - the transmitter baudrate number which to be setting.
Return:	none.

*************************************************************/
void	InitTI2C(int);
/******************************************************************
STATUS T2I2CReadFIFO(UINT8 slave_addr, UINT8 sub_slave_addr, UINT8 len, UINT8 *Targetaddr)

Description:	Read data from EEPROM through I2C (FIFO mode).
Support:	M630x
Parameters:	slave_addr - The device address.
		Sub_slave_addr -The read command after device address to 
         be sending.
		len - Byte count read from EEPROM, it's smaller than 16.
Targetaddr - pointer to the buffer in which you want to store the 
data which is read from EEPROM.

Return:	0 to success, otherwise to failure.
*************************************************************/
STATUS	T2I2CWriteFIFO(UINT8 slave_addr,UINT8 twlen,UINT8 *Sourceaddr);
/******************************************************************
STATUS	T2I2CWriteFIFO(UINT8 slave_addr, UINT8 sub_slave_addr, UINT8 len, UINT8 *Sourceaddr)

Description:	Write data to EEPROM through I2C (FIFO mode).
Support:	M630x
Parameters:	slave_addr - The device address.
		Sub_slave_addr -The write command after device address to 
         be sending.
Sourceaddr - pointer to the buffer in which store the source 
data which want to write to EEPROM. 

Return:	0 to success, otherwise to failure.
*************************************************************/
STATUS	T2I2CReadFIFO(UINT8 slave_addr,UINT8 sub_slave_addr,UINT8 trlen,UINT8 *Targetaddr);

#endif /* #ifudef TI2CIO_H */