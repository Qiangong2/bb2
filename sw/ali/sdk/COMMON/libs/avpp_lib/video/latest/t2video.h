/*-----------------------------------------------------------------------------
    
    Copyright (C) 2002 T2-Design Corp.  All rights reserved.
    
    File: t2video.h
    
    Content: 
        This file includes video post processing/OSD/SP API    
    History: 
    2004/3/22 by Michael Xie
        Created
-----------------------------------------------------------------------------*/
#ifndef _T2VIDEO_H
#define _T2VIDEO_H

#include <t2typedef.h>

#define EXPORT

#define MAX_VHWBUF_NO 10

typedef struct
{
    DWORD BufBase[MAX_VHWBUF_NO]; 
    DWORD BufSize[MAX_VHWBUF_NO]; 
}t2VHWBuf;

typedef struct 
{
    void (*SetString)(LPSTR lpString);
    LPBYTE (*GetCharMatrix)(int *pWidth, int *pHeight);
}t2SubTitleInterface;

typedef enum {Panscan, Letterbox, Normal}tDspMode; 

// TV Mode
typedef enum
{
    C_PAL = 0, 
    C_NTSC = 1, 
    C_AUTO = 2, 
    C_TVOUTMODE_MAX
}tNTSC_PAL;

typedef enum 
{
	C_VOM_SVideo = 0, // S-Video 
	C_VOM_CVBS   = 1, // Composite 
	C_VOM_RGB    = 2, // VGA
	C_VOM_YUV    = 3, // YUV (interlaced mode)
	C_VOM_YPbPr  = 4  // YPbPr (progressive mode)
}tVideoOut;

typedef enum
{
    C_ANTIFLICKER_NONE = 0,
    C_ANTIFLICKER_MIDDLE = 1,
    C_ANTIFLICKER_HIGH = 2,
}tAntiFlicker;

//Video interface
/*-------------------------------------------------------------------
Name: 
    VSyncCallBack
Description: 
    Vsync call back function. This function needs to be installed 
    to  vsync interrupt.
Parameters: 
    [IN]
    [OUT]
Return: 
-------------------------------------------------------------------*/
EXPORT void VSyncCallBack();

/*-------------------------------------------------------------------
Name: t2InitDispEngine
Description: 
      Init display engine. After calling this function, DE is initialized but 
      not turned on and DAC is not turned neither. To turn on DE and DAC, 
      pls call "t2TurnOnVideo()" to turn on DE and DAC or output one frame 
      by calling "OutputFrame".
Parameters: 
    [IN]
    TVMode: TV mode
    [OUT]
Return: 
    TRUE: succeed, FALSE: failed
-------------------------------------------------------------------*/
EXPORT BOOL t2InitDispEngine(tNTSC_PAL TVMode);

/*----------------------------------------------------
Name: 
    t2PowerTVE_DAC
Description: 
    Power on or down DACs of the internal TV encoder
Parameters: 
    IN:
    	power - 0: power down; 1: power on
    OUT:
    	None
Return: 
    None
------------------------------------------------------*/
EXPORT void t2PowerTVE_DAC(int power);

/*-------------------------------------------------------------------
Name: 
    t2SetTVType
Description: 
    Set TV type
Parameters: 
    [IN]
    tvtype: TV type
            C_PAL :PAL 
            C_NTSC: NTSC
            C_AUTO: Automatically select TV type accroding to movie
    [OUT]
Return: 
    0: succeed, non-zero: error
-------------------------------------------------------------------*/
EXPORT DWORD t2SetTVType(tNTSC_PAL tvtype);

/*-------------------------------------------------------------------
Name: 
    t2SetDispMode
Description: 
    Set display mode
Parameters: 
    [IN]
    width: width of the source
    height: height of the source
    dspmode: display mode
             Panscan: Pan&Scan(when play DVD) or wide screen(when play other video)
             Letterbox: letter box
             Normal: full screen display
    [OUT]
Return: 
    0: if succeed, non-zero: error
-------------------------------------------------------------------*/
EXPORT DWORD t2SetDispMode(DWORD width, DWORD height, tDspMode dspmode);

/*-------------------------------------------------------------------
Name: 
    t2SetVidOutput
Description: 
    Set video output
Parameters: 
    [IN]
    vidout: video output mode
    [OUT]
Return: 
    0: suceed, nonzer0: error
-------------------------------------------------------------------*/
EXPORT DWORD t2SetVidOutput(tVideoOut vidout);

/*-------------------------------------------------------------------
Name: 
    t2SetAntiFlicker
Description: 
    Set antiflicker mode
Parameters: 
    [IN]
    AntiflickerMode:
        C_ANTIFLICKER_NONE: no antiflicker
        C_ANTIFLICKER_MIDDLE: middle level antiflicker
        C_ANTIFLICKER_HIGH: high level antiflicker 
    [OUT]
Return: 
    0: succeed non-zero: error
-------------------------------------------------------------------*/
EXPORT DWORD t2SetAntiFlickerMode(tAntiFlicker AntiflickerMode);

/*---------------------------------------------------------
Name: 
    t2SetMacroVision
Description: 
    Set macro vision
Parameters: 
    [IN]
    MVType: Macrovision type
        0: disable
        1: type 1
        2: type 2
        3: type 3
    [OUT]
Return: 
    0: succeed, non-zero: error
---------------------------------------------------------*/
DWORD t2SetMacroVision(BYTE MVType);

/*-------------------------------------------------------------------
Name: 
    t2TurnOnVideo
Description: 
    Turn DE
Parameters: 
    [IN]
    [OUT]
Return: 
    0: succeed non-zero: error
-------------------------------------------------------------------*/
EXPORT DWORD t2TurnOnVideo();

/*----------------------------------------------------
Name: 
    OutputFrame
Description: 
    Display one frame
Parameters: 
    IN:
	    y_buf: Y buffer address
    	cb_buf: Cb buffer address
    	cr_buf: Cr buffer address
    	width: the width of the picture
    	height: the height of the picture
    	Stride: Y buffer stride, cb/cr buffer stride
    	is the half of the Y buffer stride
    	pic_type: 1: frame picture
    	          2: field picture
    OUT:
Return: 
------------------------------------------------------*/
EXPORT void OutputFrame(unsigned char *y_buf,
			  unsigned char *cb_buf,
			  unsigned char *cr_buf,
			  unsigned long width,
			  unsigned long height, 
			  unsigned long stride, 
			  unsigned char pic_type);

/*----------------------------------------------------
Name: 
    t2Zoom
Description: 
Parameters: 
    IN:
    ratio:
    0: normal
    1: 2X
    2: 3X
    3: 4X
    OUT:
Return: 
    TRUE, if success, otherwise FALSE;    
------------------------------------------------------*/
EXPORT BOOL t2Zoom(int ratio);

/*----------------------------------------------------
Name: 
    t2ZoomMove
Description: 
    Move picture to "direction" in zoom. 
Parameters: 
    IN:
    direction:
    1: left
    2: right
    3: up
    4: down
    OUT:
Return: 
    TRUE, if success, otherwise FALSE;    
------------------------------------------------------*/
EXPORT BOOL t2ZoomMove(int direction);

//OSD interface 
/*----------------------------------------------------
Name: 
    t2InitOSD
Description: 
    Init OSD
Parameters: 
    IN:
    base: the base address of OSD buffer. This address must be 16-byte align.
    x - horizontal offset of OSD
    y - vertical offset of OSD
    w - osd buffer width in pixel. 
    NOTE: osd buffer width shall be 32 bytes align. Therefore, in 2-bit OSD,
    width shall be 64 pixel align, in 4-bit OSD/4-bit alpha, width shall be 
    16 pixel align, in 8-bit OSD, width shall be 16 pixel align.
    
    h - osd buffer height
    bitdepth - 
        0: 2-bit OSD
        1: 4-bit OSD, 4-bit alpha
        2: 8-bit OSD
    OUT:
Return: 
    0: succeed, non-zero: error
------------------------------------------------------*/
EXPORT DWORD t2InitOSD(DWORD base, DWORD x, DWORD y, DWORD w, DWORD h, DWORD bitdepth);

/*-------------------------------------------------------------------
Name: 
    t2DEEnableOSD
Description: 
    Enable OSD
Parameters: 
    [IN]
    [OUT]
Return: 
    0: succeed non-zero: error
-------------------------------------------------------------------*/
EXPORT DWORD t2DEEnableOSD();

/*-------------------------------------------------------------------
Name: 
    t2DEDisableOSD
Description: 
    Disable OSD
Parameters: 
    [IN]
    [OUT]
Return: 
    0: succeed non-zero: error
-------------------------------------------------------------------*/
EXPORT DWORD t2DEDisableOSD();

/*---------------------------------------------------------
Name: 
    t2SetOSDAxis
Description: 
Parameters: 
    [IN]
        x - offset x of OSD
        y - offset y of OSD
    [OUT]
Return: 
    0, OK; otherwise error
---------------------------------------------------------*/
EXPORT DWORD t2SetOSDAxis(DWORD x, DWORD y);

/*-------------------------------------------------------------------
Name: 
    t2SetOsdPal
Description: 
    Set OSD palette.
Parameters: 
    [IN]
        pal - pointer to the palette
        entry_num - color numbers of the palette
        
    [OUT]
        NONE
Return: 
    0, OK; otherwise error

Notes: 
 	The format of each entry of the palette is 
    31			  24			  16	  		  8				  0
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   | 		       |		Y      |	   U       |		V      |
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
-------------------------------------------------------------------*/
EXPORT DWORD t2SetOsdPal(unsigned long *pal, int entry_num);

//SP interface
EXPORT BOOL t2InitSP(t2VHWBuf *pBuf);
EXPORT void t2EnableSP(BOOL EnableLater);
EXPORT void t2DisableSP();

//SubTitle callback interface.
EXPORT BOOL t2RegisterSubTitleInterface(t2SubTitleInterface *pInterface);

#endif //_T2VIDEO_H
