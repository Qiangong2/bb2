/*-----------------------------------------------------------------------------
    
    Copyright (C) 2002 T2-Design Corp.  All rights reserved.
    
    File: t2audio.h
    
    Content: 
        This file includes audio post processing API    
    
    History: 
    2004/3/22 by Michael Xie
        Created
-----------------------------------------------------------------------------*/
#ifndef _T2AUDIO_H
#define _T2AUDIO_H

#include <t2typedef.h>
#include <t2mpgdec.h>

#define EXPORT

#define STEREO							0
#define LEFT_CHANNEL					1
#define RIGHT_CHANNEL					2

#define MAX_AHWBUF_NO 10

typedef struct
{
    DWORD BufBase[MAX_AHWBUF_NO]; 
    DWORD BufSize[MAX_AHWBUF_NO]; 
}t2AHWBuf;

typedef struct
{
    t2AudioDecID AudioDec;
    BOOL (*AudioSelectSource)(DWORD id);
    void (*AudioSetTime)(DWORD time); 
    DWORD (*AudioGetTime)(void); 
    DWORD (*AudioBufferedRemain)(void); 
    int (*OutputAudio)(BYTE *buf, DWORD size);
    void (*PauseSound)(void);
    void (*StopAudio)();
    BOOL (*AudioIsCbr)();
}t2AudioDec;

//Audio interface
/*----------------------------------------------------
Name: 
    t2InitAudio
Description: 
    Init audio.
Parameters: 
    IN:
    pBuf: the pointer to the audio buffer 
        pBuf[0]: DSP buffer, this buffer shall be 16 bytes align
        pBuf[1]: Reverb buffer, this buffer shall be 16 bytes align
        pBuf[2]: Keyshift buffer, this buffer shall be 16 bytes align
        pBuf[3]: Delay buffer, this buffer shall be 16 bytes align
    OUT:
Return: 
    TRUE, if success, otherwise FALSE;
------------------------------------------------------*/
EXPORT BOOL t2InitAudio(t2AHWBuf *pBuf);

/*----------------------------------------------------
Name: 
    t2AudioSelectSource
Description: 
    Select audio source type.
Parameters: 
    IN:
        id: audio stream id.
        0xC0-0xDF: MPEG1 layer1/2/3 audio stream
        0x80-0x87: AC3 audio stream
        0x88-0x8F: DTS audio stream
        0x90-0x97: SDDS audio stream
        0xA0-0xA7: LPCM audio stream
        0xB0: CDDA(PCM)
        0x70-0x7f: WMA stream
        0x60-0x6f: DTSCD stream	special case, select and send PCM stream, but no annlog output
    OUT:
Return: 
    TRUE, if success, otherwise, FALSE 
------------------------------------------------------*/
EXPORT BOOL t2AudioSelectSource(DWORD id);

/*----------------------------------------------------
Name: 
    t2OutputAudio
Description: 
    Output audio stream.
Parameters: 
    IN:
        buf: the pointer to the audio stream to be output
        size: the audio stream size to be output
    OUT:
Return: 
	0	: buffer is full, pls re-send the data
	1	: Sent successful
	2	: Audio stream is not supported.
------------------------------------------------------*/
EXPORT int t2OutputAudio(BYTE *buf, DWORD size);

/*----------------------------------------------------
Name: 
    t2SoundOn
Description: 
    Turn on sound by turning on DAC
Parameters: 
    IN:
    OUT:
Return: 
    TRUE, if success, otherwise FALSE;
------------------------------------------------------*/
EXPORT BOOL t2SoundOn();

/*----------------------------------------------------
Name: 
    t2SoundOff
Description: 
    Turn off sound by turning off DAC
Parameters: 
    IN:
    OUT:
Return: 
	TRUE, if success, otherwise FALSE;    
------------------------------------------------------*/
EXPORT BOOL t2SoundOff();

/*-------------------------------------------------------------------
Name: 
    t2VolumCtrl
Description: 
    Set sound volum. This function control all the channel's volum.
    The range of the volume is from 0 to 10. The default vaule is 8.
Parameters: 
    [IN]
    [OUT]
Return: 
-------------------------------------------------------------------*/
EXPORT void t2VolumCtrl(BYTE volum);

/*----------------------------------------------------
Name: 
    t2SoundChannelSel
Description: 
    Switch channel among stereo, left and right
Parameters: 
    IN:
    	channel: 0,stereo 1,left 2,right
    OUT:
Return: 
------------------------------------------------------*/
EXPORT void t2SoundChannelSel(BYTE channel);

/*-------------------------------------------------------------------
Name: 
    t2SetChannelMode
Description: 
Parameters: 
    [IN]
    channelMode: the audio channel mode to be set
    	0: Stero
    	1: Vitual surround
    	2: 5.1 channels
    	3: Left and right channels
    	4: Only left channel
    	5: Only right channel
        
    [OUT]
Return: 
    0: succeed, non-zero: error
-------------------------------------------------------------------*/
EXPORT DWORD t2SetChannelMode(BYTE channelMode);

/*-------------------------------------------------------------------
Name: 
    t2SetEqualizer
Description: 
    Set equalizer.
Parameters: 
    [IN]
    equalizer: the equalizer mode to be set
    	0: No equalizer
    	1: Pop
    	2: Rock
    	3: Jazz
    	4: Classic
    	5: Bass
        
    [OUT]
Return: 
    0: succeed, non-zero: error
-------------------------------------------------------------------*/
EXPORT DWORD t2SetEqualizer(BYTE equalizer);

/*-------------------------------------------------------------------
Name: 
    t2SetReverbEffect
Description: 
    Set reverb effect
Parameters: 
    [IN]
    reverbEffect: the reverb effect to be set
    	0: No reverb effect
    	1: Church
    	2: Cinema
    	3: Concert
    	4: Hall
    	5: Live
    	6: Room
    	7: Stadium
    	8: Standard
    	       
    [OUT]
Return: 
    0: succeed, non-zero: error
-------------------------------------------------------------------*/
EXPORT DWORD t2SetReverbEffect(BYTE reverbEffect);

/*-------------------------------------------------------------------
Name: 
    t2SetSPDIFTyte
Description: 
    Set SPDIF type.
Parameters: 
    [IN]
    SPDIFType: the SPDIF type to be set
    	0: Raw
    	1: PCM
    	2: Disable SPDIF
        
    [OUT]
Return: 
    0: succeed, non-zero: error
-------------------------------------------------------------------*/
EXPORT DWORD t2SetSPDIFType(BYTE SPDIFType);

/*-------------------------------------------------------------------
Name: 
    t2SetSPCenter
Description: 
    Set volume of the center speaker.
Parameters: 
    [IN]
    spCenter: the volume of the center speaker to be set (0 ~ 20).
    	        
    [OUT]
Return: 
    0: succeed, non-zero: error
-------------------------------------------------------------------*/
EXPORT DWORD t2SetSPCenter(BYTE spCenter);

/*-------------------------------------------------------------------
Name: 
    t2SetSPFrontL
Description: 
    Set volume of the front left speaker.
Parameters: 
    [IN]
    spFrontL: the volume of the front left speaker to be set (0 ~ 20).
    	        
    [OUT]
Return: 
    0: succeed, non-zero: error
-------------------------------------------------------------------*/
EXPORT DWORD t2SetSPFrontL(BYTE spFrontL);

/*-------------------------------------------------------------------
Name: 
    t2SetSPFrontR
Description: 
    Set volume of the front right speaker.
Parameters: 
    [IN]
    spFrontR: the volume of the front right speaker to be set (0 ~ 20).
    	        
    [OUT]
Return: 
    0: succeed, non-zero: error
-------------------------------------------------------------------*/
EXPORT DWORD t2SetSPFrontR(BYTE spFrontR);

/*-------------------------------------------------------------------
Name: 
    t2SetSPRearL
Description: 
    Set volume of the rear left speaker.
Parameters: 
    [IN]
    spRearL: the volume of the rear left speaker to be set (0 ~ 20).
    	        
    [OUT]
Return: 
    0: succeed, non-zero: error
-------------------------------------------------------------------*/
EXPORT DWORD t2SetSPRearL(BYTE spRearL);

/*-------------------------------------------------------------------
Name: 
    t2SetSPRearR
Description: 
    Set volume of the rear right speaker.
Parameters: 
    [IN]
    spRearR: the volume of the rear right speaker to be set (0 ~ 20).
    	        
    [OUT]
Return: 
    0: succeed, non-zero: error
-------------------------------------------------------------------*/
EXPORT DWORD t2SetSPRearR(BYTE spRearR);

/*-------------------------------------------------------------------
Name: 
    t2SetSPSubwoofer
Description: 
    Set volume of the subwoofer.
Parameters: 
    [IN]
    spSubwoofer: the volume of the subwoofer to be set (0 ~ 20).
    	        
    [OUT]
Return: 
    0: succeed, non-zero: error
-------------------------------------------------------------------*/
EXPORT DWORD t2SetSPSubwoofer(BYTE spSubwoofer);

/*-------------------------------------------------------------------
Name: 
    t2SetSurroundDelay
Description: 
    Set surround delay.
Parameters: 
    [IN]
    srndDelay: delay for surround channel (0 ~ 15).
        
    [OUT]
Return: 
    0: succeed, non-zero: error
-------------------------------------------------------------------*/
EXPORT DWORD t2SetSurroundDelay(BYTE srndDelay);

/*-------------------------------------------------------------------
Name: 
    t2SetCenterDelay
Description: 
    Set center delay.
Parameters: 
    [IN]
    centerDelay: delay for surround channel (0 ~ 15).
        
    [OUT]
Return: 
    0: succeed, non-zero: error
-------------------------------------------------------------------*/
EXPORT DWORD t2SetCenterDelay(BYTE centerDelay);

/*-------------------------------------------------------------------
Name: 
    t2SetDynamic
Description: 
    Set dynamic level.
Parameters: 
    [IN]
    dynamic: the dynamic level to be set (0 ~ 8).
        
    [OUT]
Return: 
    0: succeed, non-zero: error
-------------------------------------------------------------------*/
EXPORT DWORD t2SetDynamic(BYTE dynamic);

/*-------------------------------------------------------------------
Name: 
    t2SetLPCM
Description: 
    Set LPCM mode.
Parameters: 
    [IN]
    lpcm: the LPCM mode to be set
    	0: 96k
    	1: 48k
        
    [OUT]
Return: 
    0: succeed, non-zero: error
-------------------------------------------------------------------*/
EXPORT DWORD t2SetLPCM(BYTE lpcm);

/*-------------------------------------------------------------------
Name: 
    t2SetKaraokePitch
Description: 
    Set Karaoke pitch
Parameters: 
    [IN]
    pitch: the Karaoke pitch to be set (-8 ~ +8)
        
    [OUT]
Return: 
    0: succeed, non-zero: error
-------------------------------------------------------------------*/
EXPORT DWORD t2SetKaraokePitch(int pitch);

/*-------------------------------------------------------------------
Name: 
    t2SetMicrophone
Description: 
    Set microphone on or off
Parameters: 
    [IN]
    microphoneOnOff: 
    	0: off
    	1: on
    	
    [OUT]
Return: 
    0: succeed, non-zero: error
-------------------------------------------------------------------*/
EXPORT DWORD t2SetMicrophone(int microphoneOnOff);

/*-------------------------------------------------------------------
Name: 
    t2SetDRCMode
Description: 
    Set DRC mode
Parameters: 
    [IN]
    drcMode: 
    	0: custom 0
    	1: custom 1
    	2: line out
    	3: RF remod
    	
    [OUT]
Return: 
    0: succeed, non-zero: error
-------------------------------------------------------------------*/
EXPORT DWORD t2SetDRCMode(BYTE drcMode);

/*-------------------------------------------------------------------
Name: 
    t2AudioSetLPCM
Description: 
    Configure PCM data format
Parameters: 
    [IN]
    BitPerSample: there are 16/20/24 bits sample
    SampleRate: sample rate 
    ChannelNum: the max channle number is 2 for now
    1: 1ch
    2: 2ch

    Therefore, for now, we only support the PCM format above
        
    [OUT]
Return: 
    0: if success, otherwise, 1
-------------------------------------------------------------------*/
BOOL t2AudioSetPCM(BYTE BitPerSample,DWORD SampleRate, BYTE ChannelNum);

/*----------------------------------------------------
Name: 
    t2StopAudio
Description: 
    Stop audio playback
Parameters: 
    IN:
    OUT:
Return: 
------------------------------------------------------*/
void t2StopAudio();

/*----------------------------------------------------
Name: 
    t2PauseSound
Description: 
    pause audio playback
Parameters: 
    IN:
    OUT:
Return: 
------------------------------------------------------*/
void t2PauseSound(void);

/*-------------------------------------------------------------------
Name: 
    t2AudioRegNewDEc
Description: 
    Register new external audio decoder into AVI decoder. 
Parameters: 
    [IN]
    audio_dec: new audio decoder
    [OUT]
Return: 
    0: succeed
    non-zero: failed
-------------------------------------------------------------------*/
int t2AudioRegNewDEc(t2AudioDec audio_dec);

/*-------------------------------------------------------------------
Name: 
    t2AudioSetExtDecCall
Description: 
    External audio decoder shall call this function(t2AudioSetExtDecCall(TRUE))
    to tell APP lib that this call is from external audio decoder whenever 
    external audio decoder call to APP lib and call this function 
    (t2AudioSetExtDecCall(FALSE)) to tell APP lib that external audio decoder 
    has finished call to APP lib.
Parameters: 
    [IN]
    [OUT]
Return: 
-------------------------------------------------------------------*/
void t2AudioSetExtDecCall(BOOL ext_dec_call);
    
#endif
