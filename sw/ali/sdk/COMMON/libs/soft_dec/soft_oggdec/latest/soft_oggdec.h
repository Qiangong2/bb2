/*****************************************************************************
          (c) copyrights 2003-. All rights reserved

           T-Square Design; Inc.

          Company confidential and Properietary information.
          This information may not be disclosed to unauthorized
          individual.
 *****************************************************************************/

/*****************************************************************************
 *
 *    PROJECT: audio decoder library
 *
 *    FILE NAME:  soft_oggdec.h
 *
 *    DESCRIPTION: This library can decode OGG.
 *
 *    AUTHOR:  Frank pan -- frank_pan@zh.t2-design.com
 *
 *****************************************************************************/

/* *******************!!!!!Note: very important !!!!!*************************
 * all the output data is linear PCM format.
 *****************************************************************************/


#ifndef __SOFT_OGGDEC_H__
#define __SOFT_OGGDEC_H__
#include "../../softdec_common_h/soft_common.h"
//#include "soft_common.h"


#ifndef DECODERINSTANCE_DEFINED
#define DECODERINSTANCE_DEFINED
typedef void* DecoderInstance;
#endif

/******* Hereinafter is for OGG decoder ************/

/* OGG audio info */
#ifndef _OGGAUDIOINFO_DEFINED
#define _OGGAUDIOINFO_DEFINED

#define OGGMAX_TITLE_NUM 256
#define OGGMAX_AUTHOR_NUM 256

typedef struct tagOGGFileHdrInfo
{
    unsigned char pTitle[OGGMAX_TITLE_NUM];      /* Title of the file */
    unsigned char pAuthor[OGGMAX_AUTHOR_NUM];     /* Author of the file */

	tTrack_Name track_name;
	tTrack_Duration track_duration;
	tArtist artist;
	tAlbum album;
	tOriginal_Track_Number original_track_num;
	tDate date;
	tTotal_file_size total_file_size;

	unsigned long bitrate;         /* bit-rate of the OGG vorbis bitstream */
    unsigned long sample_rate;  /* sampling rate in HZ*/
    unsigned long num_channels;   /* number of audio channels */
    tBitsPerSample bitspersample;  /* bits per sample */
	tSampleSigness sample_signed; /* are samples signed or unsigned quantities? */
	tSampleEndian sample_endina; /* are samples little- or big-endian? */
	tSampleDataOrder sample_data_order; /* how is multi-channel data is emitted, e.g. is stereo output LRLRLR or RLRLRL? */
} tOGGFileHdrInfo;
#endif

/* OGG Decoder input and output */
#ifndef _OGGDECODERINOUT_DEFINED
#define _OGGDECODERINOUT_DEFINED
typedef struct tagOGGDecoderIn
{
	const void *pvEncodedData; // pointer to the encoded OGG data stream.
	unsigned long uiTotalEncodedBytes; // total number of encoded OGG data in byte.

} tOGGDecoderIn;

typedef struct tagOGGDecoderOut
{
	void *pvDecodedData; // pointer to decoded PCM data stream.
	unsigned long uiMaxDecodedBytes; //PCM data buffer length in byte. It is used to put the decoded data.
	unsigned long uiBytesWritten; //real number copied PCM data to PCM data buffer.
	unsigned long uiUsed_EncodedData; /*used number of encoded OGG data stream. User should move the OGG
                                 encoded data pointer according this value. This value tell the user how
                                 many encoded data the decoder used. If this value is zero, means decoder
                                 did not use any encoded data, that's because the decoder will decode the
                                 encoded data page by page, if the encoded data is less than one page,
                                 the decoder will not decode it, it will wait the user send more data. 
	                           */
	bool bMoreOutputAvailable; //flag of PCM data transfer end.

} tOGGDecoderOut;

#endif /* _OGGDECODERINOUT_DEFINED */


/* ................................................................................. 
 *
 * Functions
 * =========
 */

#ifdef __cplusplus
extern "C"
{
#endif /* __cplusplus */

/*
 *       SoftOGGDecoder_BytesRequiredForExamination
 *       ==========================================
 *
 * Description
 * -----------
 * Retrieves the reqired bytes to examine whether the stream is OGG.
 *
 * Syntax
 * ------
 * unsigned long SoftOGGDecoder_BytesRequiredForExamination(void);
 * Returns the number of bytes required for SoftOGGDecoder_CanDecode() to be able to
 * identify and distinguish the different, supported audio stream types.
 *
 *
 * Return Value
 * ------------
 *   The unmber of bytes need to feed.
 *
 */

unsigned long SoftOGGDecoder_BytesRequiredForExamination(void);


/*
 *       SoftOGGDecoder_CanDecode
 *       ========================
 *
 * Description
 * -----------
 * Examine the stream type.
 *
 * Syntax
 * -----------
 * bool   SoftOGGDecoder_CanDecode(const void *pvStreamsInitialData,unsigned long uiTotalBytes, unsigned long *puihdrlen);
 *
 * When given at least the number of bytes indicated by SoftOGGDecoder_BytesRequiredForExamination()
 * from the start of a data stream it will determine whether or not that data stream is a 
 * supported audio format. If you supply less than the indicated number of bytes it is permitted
 * to always return 'false' (though it may return a valid audio type).
  
 * where:
 *
 *   pvStreamsInitialData    pointer to the stream data need to be examined.
 *   uiTotalBytes            total bytes that will be examined, it should not less than
 *                           the value return from SoftOGGDecoder_BytesRequiredForExamination().
 *   puihdrlen               the length of the header of the ogg vorbis file. If user want to get the
 *                           header information, he should send more byte than this value.
 *
 * Return Value
 * ------------
 *   true: OGG stream.
 *   false: not OGG stream.
 *
 */

bool SoftOGGDecoder_CanDecode(const void *pvStreamsInitialData,unsigned long uiTotalBytes, unsigned long *puihdrlen);



/*
 *        SoftOGGDecoder_Initialise
 *        =========================
 *
 * Description
 * -----------
 * Initializes the OGG decoder.
 *
 * Must be called before starting to decode OGG stream.
 *
 * Syntax
 * ------
 *   tAudioDecStatus SoftOGGDecoder_Initialise(DecoderInstance *phInstance);
 *
 * where:
 *
 *   *pInstance     pointer to the handle that holds the internal
 *                  state of the OGG decoder
 *                  User is not permit to change this area
 *
 * Return Value
 * ------------
 *   tAudioDecStatus
 *
 */
tAudioDecStatus SoftOGGDecoder_Initialise(DecoderInstance *phInstance);


/*
 *       SoftOGGDecoder_LocateNextPage
 *       =============================
 *
 * Description
 * -----------
 * Get the start position of next Page of OGG stream.

 * SO FOR, IT DOES NOT WORK, SO DO NOT CALL IT!!!

 *
 * Syntax
 * ------
 * bool SoftOGGDecoder_LocateNextPage(DecoderInstance hInstance, const void *pvEncodedData,unsigned long uiTotalEncodedBytes, \
 *								unsigned long *puiPageByteOffset);
 * Returns true if a valid OGG page header was found within the given buffer.  false is 
 * returned if there was none (or too little data was provided).On successful return 
 * 'puiPageByteOffset' is set to the index of the page's first byte within 'pvEncodedData'.
 *
 *
 * where:
 *
 *   hInstance      pointer to the structure that holds the internal
 *                  state of the OGG decoder
 *   pvEncodedData          pointer to the OGG stream data.
 *   uiTotalEncodedBytes    the total number of stream data.
 *   puiPageByteOffset     the offset from 'pvEncodedData' to the fist byte of next page.
 *
 * Return Value
 * ------------
 *   true: found the page header in the OGG stream.
 *   false: didn't find the page header in the OGG stream.
 *
 */
bool SoftOGGDecoder_LocateNextPage(DecoderInstance hInstance, const void *pvEncodedData,unsigned long uiTotalEncodedBytes, \
                                 unsigned long *puiPageByteOffset);

/*
 *       SoftOGGDecoder_DecodeOggHeader
 *       ==============================
 *
 * Description
 * -----------
 * Retrieves the OGG header information.
 *
 * Syntax
 * ------
 * bool SoftOGGDecoder_DecodeOggHeader(DecoderInstance hInstance, const void *pvOggHeader, unsigned long size, \
 *                                   tOGGFileHdrInfo *pOggInfo);
 *
 * Decodes the Ogg vorbis file header designated by 'pvOggHeader'.
 * Use SoftOGGDecoder_LocateNextPage() to locate that header.
 *
 * where:
 *
 *   hInstance      pointer to the structure that holds the internal
 *                  state of the OGG decoder
 *   pvOggHeader    pointer to the header of one Ogg file.
 *   size           size of the stream data.
 *   pOggInfo       pointer to the structure that holds the stream information. User should support it,
 *                  the size is sizeof(tOGGFileHdrInfo).
 *
 * Return Value
 * ------------
 *   true: get the Ogg vorbis file header information.
 *   false: didn't get the Ogg vorbis file header information.
 *
 */

bool SoftOGGDecoder_DecodeOggHeader(DecoderInstance hInstance, const void *pvOggHeader, unsigned long size, tOGGFileHdrInfo *pOggInfo);


/*
 *       SoftOGGDecoder_Decode
 *       =====================
 *
 * Description
 * -----------
 * It will decode the OGG data stream. The stream pointer should be put
 * to the beginning of the stream when you start decoding. The decoder 
 * will automatic skip the header information.
 *
 * If pvDecodedData was not big enough to contain all decoded samples
 * 'pbMoreOutputAvailable' will be set to true and the decoder must be
 * drained of decoded data before supplying any more encoded audio
 * data.  To retrieve these remaining samples, repeatedly call
 * Decoder_Decode() with uiTotalEncodedBytes == 0 (and pvEncodedData
 * == NULL) until pbMoreOutputAvailable is false or an error occurs
 * (Decoder_Decode() returned false).
 *
 * Syntax
 * ------
 * tAudioDecStatus SoftOGGDecoder_Decode(DecoderInstance hInstance, tOGGDecoderIn * OGGdecoder_in, \
 *                                   tOGGDecoderOut *OGGdecoder_out);
 *
 * where:
 *
 *   hInstance         pointer to the structure that holds the internal state of the OGG decoder.
 *   OGGdecoder_in     pointer to the structure tOGGDecoderIn.
 *   OGGdecoder_out    pointer to the structure tOGGDecoderOut.
 *
 * Return Value
 * ------------
 *   tAudioDecStatus
 *   
 */
tAudioDecStatus SoftOGGDecoder_Decode(DecoderInstance hInstance, tOGGDecoderIn * OGGdecoder_in, \
								  tOGGDecoderOut *OGGdecoder_out);


/*
 *       SoftOGGDecoder_SignalDiscontinuity
 *       ==================================
 *
 * Description
 * -----------
 * The next call to SoftOGGDecoder_Decode() will be with data from a
 * different position within the same audio stream.  The data may be
 * from before or after the decoder's current position within the stream.
 *

* SO FOR, IT DOES NOT WORK, SO DO NOT CALL IT!!!

 * Syntax
 * ------
 *   tAudioDecStatus SoftOGGDecoder_SignalDiscontinuity(DecoderInstance hInstance);
 *
 * where:
 *
 *   hInstance      pointer to the structure that holds the internal
 *                  state of the OGG decoder
 *
 * Return Value
 * ------------
 *   tAudioDecStatus
 *
 */

tAudioDecStatus SoftOGGDecoder_SignalDiscontinuity(DecoderInstance hInstance);

/*
 *        SoftOGGDecoder_Finalise
 *        =======================
 *
 * Description
 * -----------
 * Close the OGG decoder.
 *
 * Called after decoder finishing decode OGG stream.
 *
 * Syntax
 * ------
 *   tAudioDecStatus SoftOGGDecoder_Finalise(DecoderInstance *phInstance);
 *
 * where:
 *
 *   *phInstance    pointer to the handle that holds the internal
 *                  state of the OGG decoder
 *
 * Return Value
 * ------------
 *   tAudioDecStatus
 *
 */
tAudioDecStatus SoftOGGDecoder_Finalise(DecoderInstance *phInstance);

#ifdef __cplusplus
}
#endif /* __cplusplus */

/******* OGG decoder define end ************/
#endif /* _AUDIODEC_API_H_ */
