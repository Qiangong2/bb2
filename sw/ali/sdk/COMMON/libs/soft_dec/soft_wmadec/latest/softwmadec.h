/*****************************************************************************
          (c) copyrights 2003-. All rights reserved

           T-Square Design; Inc.

          Company confidential and Properietary information.
          This information may not be disclosed to unauthorized
          individual.
 *****************************************************************************/

/*****************************************************************************
 *
 *    PROJECT: wma decoder library
 *
 *    FILE NAME:  softwmadec.h
 *
 *    DESCRIPTION: This library can decode WMA
 *
 *    AUTHOR:  liyujiang-- john_li@zh.t2-design.com
 *
 *    HISTORY: 	19/12/03  first released by john li.
 *
 *****************************************************************************/

/* *******************!!!!!Note: very important !!!!!*************************
 * all the output data is linear PCM format.

 * WMA decoder only can decoder the whole file, if the stream data is not a complete
 * file, it can not decoder without the header information.
 *****************************************************************************/


#ifndef _SOFTWMADEC_API_H_
#define _SOFTWMADEC_API_H_

#include "../../softdec_common_h/soft_common.h"

/******* Hereinafter is for WMA decoder ************/

/* WMA audio info */
#ifndef _WMAAUDIOINFO_DEFINED
#define _WMAAUDIOINFO_DEFINED

#define WMAMAX_TITLE_NUM 256
#define WMAMAX_AUTHOR_NUM 256

typedef struct tagWMAFileHdrInfo
{
    unsigned char pTitle[WMAMAX_TITLE_NUM];      /* Title of the file */
    unsigned char pAuthor[WMAMAX_AUTHOR_NUM];     /* Author of the file */

	tTrack_Name track_name;
	tTrack_Duration track_duration;
	tArtist artist;
	tAlbum album;
	tOriginal_Track_Number original_track_num;
	tDate date;
	tTotal_file_size total_file_size;
	unsigned long first_packet_offset; // the offset of the first packet after header.
	unsigned long packet_size; //packet size for each packet.
	unsigned long total_packet_num; //total packet number in the file.

	unsigned long bitrate;         /* bit-rate of the WMA bitstream */
    unsigned long sample_rate;  /* sampling rate in HZ*/
    unsigned long num_channels;   /* number of audio channels */
    tBitsPerSample bitspersample;  /* bits per sample */
	tSampleSigness sample_signed; /* are samples signed or unsigned quantities? */
	tSampleEndian sample_endina; /* are samples little- or big-endian? */
	tSampleDataOrder sample_data_order; /* how is multi-channel data is emitted, e.g. is stereo output LRLRLR or RLRLRL? */
} tWMAFileHdrInfo;
#endif /* _WMAAUDIOINFO_DEFINED */

/* WMA Decoder input and output */
#ifndef _WMADECODERINOUT_DEFINED
#define _WMADECODERINOUT_DEFINED
typedef struct tagWMADecoderIn
{
	const void *pvEncodedData; // pointer to the encoded WMA data stream.
	unsigned long uiTotalEncodedBytes; // total number of encoded WMA data in byte.
	unsigned long uiPacketIndex; //Packet index, start from 0, the MAX value is (total_packet_num - 1)

} tWMADecoderIn;

typedef struct tagWMADecoderOut
{
	void *pvDecodedData; // pointer to decoded PCM data stream.
	unsigned long uiMaxDecodedBytes; //PCM data buffer length in byte. It is used to put the decoded data.
	unsigned long uiBytesWritten; //real number copied PCM data to PCM data buffer.
	unsigned long uiUsed_EncodedData; /*used number of encoded WMA data stream. User should move the WMA
                                 encoded data pointer according this value. This value tell the user how
                                 many encoded data the decoder used. If this value is zero, means decoder
                                 did not use any encoded data, that's because the decoder will decode the
                                 encoded data packet by packet, if the encoded data is less than one packet,
                                 the decoder will not decode it, it will wait the user send more data. 
	                           */
	bool bMoreOutputAvailable; //flag of PCM data transfer end.

} tWMADecoderOut;

#endif /* _WMADECODERINOUT_DEFINED */

/* ................................................................................. 
 *
 * Functions
 * =========
 */

#ifdef __cplusplus
extern "C"
{
#endif /* __cplusplus */

/*
 *       SoftWMADecoder_GetLibVersion
 *       ==========================================
 *
 * Description
 * -----------
 * Get Version Number of WMA Decoder library 
 *
 * Return Value
 * ------------
 *   Version Number
 *
 */
unsigned char *SoftWMADecoder_GetLibVersion(void);


/*
 *       SoftWMADecoder_GetLibName
 
 *       =========================================
 *
 * Description
 * -----------
 * Get Name of Wma Decoder library 
 *
 * Return Value
 * ------------
 *   Library Name.
 *
 */

unsigned char *SoftWMADecoder_GetLibName(void);



/*
 *       SoftWMADecoder_BytesRequiredForExamination
 *       ==========================================
 *
 * Description
 * -----------
 * Retrieves the reqired bytes to examine whether the stream is WMA.
 *
 * Syntax
 * ------
 * unsigned long SoftWMADecoder_BytesRequiredForExamination(void);
 * Returns the number of bytes required for SoftWMADecoder_CanDecode() to be able to
 * identify and distinguish the different, supported audio stream types.
 *
 *
 * Return Value
 * ------------
 *   The unmber of bytes need to feed.
 *
 */

unsigned long SoftWMADecoder_BytesRequiredForExamination(void);


/*
 *       SoftWMADecoder_CanDecode
 *       ========================
 *
 * Description
 * -----------
 * Examine the stream type.
 *
 * Syntax
 * ------
 * bool   SoftWMADecoder_CanDecode(const void *pvStreamsInitialData,unsigned long uiTotalBytes, unsigned long *puihdrlen);
 *
 * When given at least the number of bytes indicated by SoftWMADecoder_BytesRequiredForExamination()
 * from the start of a data stream it will determine whether or not that data stream is a 
 * supported audio format. If you supply less than the indicated number of bytes it is permitted
 * to always return 'false' (though it may return a valid audio type).
  
 * where:
 *
 *   pvStreamsInitialData    pointer to the stream data need to be examined.
 *   uiTotalBytes            total bytes that will be examined, it should not less than
 *                           the value return from SoftWMADecoder_BytesRequiredForExamination().
 *   puihdrlen               the length of the header of the WMA file. If user want to get the
 *                           header information, he should send more byte than this value.
 *
 * Return Value
 * ------------
 *   true: WMA stream.
 *   false: not WMA stream.
 *
 */

bool SoftWMADecoder_CanDecode(const void *pvStreamsInitialData,unsigned long uiTotalBytes, unsigned long *puihdrlen);


/*
 *        SoftWMADecoder_Initialise
 *        =========================
 *
 * Description
 * -----------
 * Initializes the WMAudio decoder.
 *
 * Must be called before starting to decode WMA stream.
 *
 * Syntax
 * ------
 *   tAudioDecStatus SoftWMADecoder_Initialise(DecoderInstance *phInstance);
 *
 * where:
 *
 *   *phInstance    pointer to the handle that holds the internal
 *                  state of the WMAudio decoder
 *                  User is not permit to change this area
 *
 * Return Value
 * ------------
 *   cAudio_NoErr                decoder initialised
 *   cAudio_BadArgument          bad argument passed in
 *   cAudio_BadAsfHeader         bad ASF header
 *   cAudio_BadSamplingRate      invalid or unsupported sampling rate
 *   cAudio_BadNumberOfChannels  invalid or unsupported number of channels
 *   cAudio_BadVersionNumber     invalid or unsupported version number
 *   cAudio_BadWeightingMode     invalid or unsupported weighting mode
 *   cAudio_BadPacketisation     invalid or unsupported packetisation
 *   cAudio_BadDRMType           unknown encryption type
 *   cAudio_DRMFailed            DRM failed
 *   cAudio_DRMUnsupported       DRM is not supported for this version
 *
 */
tAudioDecStatus SoftWMADecoder_Initialise(DecoderInstance *phInstance);


/*
 *       SoftWMADecoder_LocateNextPacket
 *       ===============================
 *
 * Description
 * -----------
 * Get the start position of next packet of WMA stream.
 *
 * Syntax
 * ------
 * bool SoftWMADecoder_LocateNextPacket(DecoderInstance hInstance, const void *pvEncodedData,unsigned long uiTotalEncodedBytes, \
 *								unsigned long *puiPacketByteOffset);
 * Returns true if a valid WMA packet header was found within the given buffer.  false is 
 * returned if there was none (or too little data was provided).On successful return 
 * 'puiPacketByteOffset' is set to the index of the packet's first byte within 'pvEncodedData'.
 *
 *
 * where:
 *
 *   hInstance              pointer to the structure that holds the internal
 *                          state of the WMAudio decoder
 *   pvEncodedData          pointer to the WMA stream data.
 *   uiTotalEncodedBytes    the total number of stream data.
 *   puiPacketByteOffset    the offset from 'pvEncodedData' to the fist byte of next packet.
 *
 * Return Value
 * ------------
 *   true: found the packet header in the WMA stream.
 *   false: didn't find the packet header in the WMA stream.
 *
 */
bool SoftWMADecoder_LocateNextPacket(DecoderInstance hInstance, const void *pvEncodedData,unsigned long uiTotalEncodedBytes, \
                                 unsigned long *puiPacketByteOffset);

/*
 *       SoftWMADecoder_DecodeASFHeader
 *       ==============================
 *
 * Description
 * -----------
 * Retrieves the ASF file header information.
 *
 * Syntax
 * ------
 * bool SoftWMADecoder_DecodeASFHeader(DecoderInstance hInstance, const void *pvASFHeader, unsigned long size, \
 *                                   tWMAFileHdrInfo *pASFInfo);
 *
 * Decodes the ASF file header designated by 'pvASFHeader'.
 * Use SoftWMADecoder_LocateNextPacket() to locate that header
 *
 * where:
 *
 *   hInstance      pointer to the structure that holds the internal
 *                  state of the WMAudio decoder
 *   pvASFHeader    pointer to the header of one ASF file header.
 *   size           size of the stream data.
 *   pASFInfo       pointer to the structure that holds the stream information. User should support it,
 *                  the size is sizeof(tWMAFileHdrInfo).
 *
 * Return Value
 * ------------
 *   true: get the ASF file header information.
 *   false: didn't get the ASF header information.
 *
 */

bool SoftWMADecoder_DecodeASFHeader(DecoderInstance hInstance, const void *pvASFHeader, unsigned long size,\
								  tWMAFileHdrInfo *pASFInfo);



/*
 *       SoftWMADecoder_Decode
 *       =====================
 *
 * Description
 * -----------
 * It will decode the WMA data stream. The stream pointer should be put
 * to the beginning of the stream when you start decoding. The decoder 
 * will automatic skip the header information.
 *
 * If pvDecodedData was not big enough to contain all decoded samples
 * 'pbMoreOutputAvailable' will be set to true and the decoder must be
 * drained of decoded data before supplying any more encoded audio
 * data.  To retrieve these remaining samples, repeatedly call
 * Decoder_Decode() with uiTotalEncodedBytes == 0 (and pvEncodedData
 * == NULL) until pbMoreOutputAvailable is false or an error occurs
 * (Decoder_Decode() returned false).
 *
 * Syntax
 * ------
 * tAudioDecStatus SoftWMADecoder_Decode(DecoderInstance hInstance, tWMADecoderIn *WMAdecoder_in, \
 *                                   tWMADecoderOut *WMAdecoder_out);
 *
 * where:
 *
 *   hInstance          pointer to the structure that holds the internal state of the WMA decoder.
 *   WMAdecoder_in      pointer to the structure tWMADecoderIn.
 *   WMAdecoder_out     pointer to the structure tWMADecoderOut.
 *
 * Return Value
 * ------------
 *   tAudioDecStatus
 *   
 */
tAudioDecStatus SoftWMADecoder_Decode(DecoderInstance hInstance, tWMADecoderIn *WMAdecoder_in, \
								  tWMADecoderOut *WMAdecoder_out);

/*
 *       SoftWMADecoder_SignalDiscontinuity
 *       ==================================
 *
 * Description
 * -----------
 * The next call to SoftWMADecoder_Decode() will be with data from a
 * different position within the same audio stream.  The data may be
 * from before or after the decoder's current position within the stream.
 *
 * Syntax
 * ------
 *   tAudioDecStatus SoftWMADecoder_SignalDiscontinuity(DecoderInstance hInstance);
 *
 * where:
 *
 *   hInstance      pointer to the structure that holds the internal
 *                  state of the WMAudio decoder
 *
 * Return Value
 * ------------
 *   tAudioDecStatus
 *
 */

tAudioDecStatus SoftWMADecoder_SignalDiscontinuity(DecoderInstance hInstance);

/*
 *        SoftWMADecoder_Finalise
 *        =======================
 *
 * Description
 * -----------
 * Close the WMAudio decoder.
 *
 * Called after decoder finishing decode WMA stream.
 *
 * Syntax
 * ------
 *   tAudioDecStatus SoftWMADecoder_Finalise(DecoderInstance *phInstance);
 *
 * where:
 *
 *   *phInstance    pointer to the handle that holds the internal
 *                  state of the WMAudio decoder
 *
 * Return Value
 * ------------
 *   tAudioDecStatus
 *
 */
tAudioDecStatus SoftWMADecoder_Finalise(DecoderInstance *hInstance);

#ifdef __cplusplus
}
#endif /* __cplusplus */

/******* WMA decoder define end ************/
#endif /* _AUDIODEC_API_H_ */
