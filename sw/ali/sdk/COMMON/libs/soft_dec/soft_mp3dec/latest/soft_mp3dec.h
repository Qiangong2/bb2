/*****************************************************************************
          (c) copyrights 2003-. All rights reserved

           T-Square Design; Inc.

          Company confidential and Properietary information.
          This information may not be disclosed to unauthorized
          individual.
 *****************************************************************************/

/*****************************************************************************
 *
 *    PROJECT: audio decoder library
 *
 *    FILE NAME:  audiodec.h
 *
 *    DESCRIPTION: This library can decode WMA, MP3, AAC, OGG.
 *
 *    AUTHOR:  davil ding -- davil_ding@zh.t2-design.com
 *
 *    HISTORY: 	
 * 			12/19/03  TangChaoJian Create this file, base on Davil softaudiodec.h.
 *
 *****************************************************************************/

/* *******************!!!!!Note: very important !!!!!*************************
 * all the output data is linear PCM format.

 * WMA decoder only can decoder the whole file, if the stream data is not a complete
 * file, it can not decoder without the header information.

 * MP3 decoder only can decoder any MP3 data, but it must find the begin of one frame.

 * AAC decoder specification is not ready, we don't know enough about it, we are gathering the information.

 * OGG decoder specification is not ready, we don't know enough about it, we are gathering the information.
 *****************************************************************************/

#include "../../softdec_common_h/soft_common.h"

/******* Hereinafter is for MP3 decoder ************/

#ifndef __SOFT_MP3DEC_H__
#define __SOFT_MP3DEC_H__
/* MP3 audio info */
#ifndef _MP3AUDIOINFO_DEFINED
#define _MP3AUDIOINFO_DEFINED

#define MP3MAX_TITLE_NUM 256
#define MP3MAX_AUTHOR_NUM 256
#define MP3MAX_ALBUM_NUM 31

typedef struct tagMP3FileHdrInfo
{
    unsigned char pTitle[MP3MAX_TITLE_NUM] __attribute__((packed));      /* Title of the file */
    unsigned char pAuthor[MP3MAX_AUTHOR_NUM] __attribute__((packed));     /* Author of the file */
	unsigned char  pAlbum[MP3MAX_ALBUM_NUM]__attribute__((packed));

	tTrack_Name track_name;
	tTrack_Duration track_duration;
	tArtist artist;
	tOriginal_Track_Number original_track_num;
	tDate date;
	tTotal_file_size total_file_size;

} tMP3FileHdrInfo;

typedef struct tagMP3FrameInfo
{
	unsigned long bitrate;         /* bit-rate of the MP3 bitstream */

    unsigned long sample_rate;  /* sampling rate in HZ*/
    unsigned long num_channels;   /* number of audio channels */
    tBitsPerSample bitspersample;  /* bits per sample */
	tSampleSigness sample_signed; /* are samples signed or unsigned quantities? */
	tSampleEndian sample_endina; /* are samples little- or big-endian? */
	tSampleDataOrder sample_data_order; /* how is multi-channel data is emitted, e.g. is stereo output LRLRLR or RLRLRL? */
} tMP3FrameInfo;

#endif /* _MP3AUDIOINFO_DEFINED */

/* MP3 Decoder input and output */
#ifndef _MP3DECODERINOUT_DEFINED
#define _MP3DECODERINOUT_DEFINED
typedef struct tagMP3DecoderIn
{
	const void *pvEncodedData; // pointer to the encoded MP3 data stream.
	unsigned long uiTotalEncodedBytes; // total number of encoded MP3 data in byte.

} tMP3DecoderIn;

typedef struct tagMP3DecoderOut
{
	void *pvDecodedData; // pointer to decoded PCM data stream.
	unsigned long uiMaxDecodedBytes; //PCM data buffer length in byte. It is used to put the decoded data.
	unsigned long uiBytesWritten; //real number copied PCM data to PCM data buffer.
	unsigned long uiUsed_EncodedData; /*used number of encoded MP3 data stream. User should move the MP3
                                 encoded data pointer according this value. This value tell the user how
                                 many encoded data the decoder used. If this value is zero, means decoder
                                 did not use any encoded data, that's because the decoder will decode the
                                 encoded data frame by frame, if the encoded data is less than one frame,
                                 the decoder will not decode it, it will wait the user send more data. 
	                           */
	bool bMoreOutputAvailable; //flag of PCM data transfer end.

} tMP3DecoderOut;

#endif /* _MP3DECODERINOUT_DEFINED */

/* ................................................................................. 
 *
 * Functions
 * =========
 */

#ifdef __cplusplus
extern "C"
{
#endif /* __cplusplus */


/*
 *       SoftWMADecoder_GetLibVersion
 *       ==========================================
 *
 * Description
 * -----------
 * Get Version Number of WMA Decoder library 
 *
 * Return Value
 * ------------
 *   Version Number
 *
 */
unsigned char *SoftMp3Decoder_GetLibVersion(void);


/*
 *       SoftWMADecoder_GetLibName *       ==========================================
 *
 * Description
 * -----------
 * Get Name of Wma Decoder library 
 *
 * Return Value
 * ------------
 *   Library Name.
 *
 */

unsigned char *SoftMp3Decoder_GetLibName(void);




/*
 *       SoftMP3Decoder_BytesRequiredForExamination
 *       ==========================================
 *
 * Description
 * -----------
 * Retrieves the reqired bytes to examine whether the stream is MP3.
 *
 * Syntax
 * ------
 * unsigned long SoftMP3Decoder_BytesRequiredForExamination(void);
 * Returns the number of bytes required for SoftMP3Decoder_CanDecode() to be able to
 * identify and distinguish the different, supported audio stream types.
 *
 *
 * Return Value
 * ------------
 *   The unmber of bytes need to feed.
 *
 */

unsigned long SoftMP3Decoder_BytesRequiredForExamination(void);


/*
 *       SoftMP3Decoder_CanDecode
 *       ========================
 *
 * Description
 * -----------
 * Examine the stream type.
 *
 * Syntax
 * ------
 * bool   SoftMP3Decoder_CanDecode(const void *pvStreamsInitialData,unsigned long uiTotalBytes, unsigned long *puihdrlen);
 *
 * When given at least the number of bytes indicated by SoftMP3Decoder_BytesRequiredForExamination()
 * from the start of a data stream it will determine whether or not that data stream is a 
 * supported audio format. If you supply less than the indicated number of bytes it is permitted
 * to always return 'false' (though it may return a valid audio type).
  
 * where:
 *
 *   pvStreamsInitialData    pointer to the stream data need to be examined.
 *   uiTotalBytes            total bytes that will be examined, it should not less than
 *                           the value return from SoftMP3Decoder_BytesRequiredForExamination().
 *   puihdrlen               the length of the header of the MP3 file. and mp3 stream data should
 * 							 follow at this position. If user want to get the header information,
 *                           he should send more byte than this value. and to decode mp3 frame header, it should
 *							 pass data at the position of stream data. If this value is 0, means 
 *                           this MP3 stream has no header.
 *
 * Return Value
 * ------------
 *   true: MP3 stream.
 *   false: not MP3 stream.
 *
 */

bool SoftMP3Decoder_CanDecode(const void *pvStreamsInitialData,unsigned long uiTotalBytes, unsigned long *puihdrlen);


/*
 *       SoftMP3Decoder_DecodeMP3Header
 *       ==============================
 *
 * Description
 * -----------
 * Retrieves the MP3 file header information.
 *
 * Syntax
 * ------
 * bool SoftMP3Decoder_DecodeMP3Header(DecoderInstance hInstance, const void *pvMP3Header, unsigned long size, \
 *                                   tMP3FileHdrInfo *pMP3HdrInfo);
 *
 * Decodes the MP3 file header designated by 'pvMP3Header'.
 *
 * where:
 *
 *   hInstance      pointer to the structure that holds the internal state of the MP3 decoder.
 *   pvMP3Header    pointer to the header of one MP3 file header.
 *   size           size of the stream data.
 *   pMP3HdrInfo    pointer to the structure that holds the stream information. User should support it,
 *                  the size is sizeof(tMP3FileHdrInfo).
 *
 * Return Value
 * ------------
 *   true: If there is a valid MP3 header.
 *   false: If there is no valid MP3 header.
 *
 */

bool SoftMP3Decoder_DecodeMP3FileHeader(DecoderInstance hInstance, const void *pvMP3Header, unsigned long size, \
									tMP3FileHdrInfo *pMP3HdrInfo);

/*
 *        SoftMP3Decoder_Initialise
 *        =========================
 *
 * Description
 * -----------
 * Initializes the MP3 decoder.
 *
 * Must be called before starting to decode MP3 stream.
 *
 * Syntax
 * ------
 *   tAudioDecStatus SoftMP3Decoder_Initialise(DecoderInstance *phInstance);
 *
 * where:
 *
 *   *pInstance     pointer to the handle that holds the internal
 *                  state of the MP3 decoder
 *                  User is not permit to change this area
 *
 * Return Value
 * ------------
 *   tAudioDecStatus
 *
 */
tAudioDecStatus SoftMP3Decoder_Initialise(DecoderInstance *phInstance);


/*
 *       SoftMP3Decoder_LocateNextFrame
 *       ==============================
 *
 * Description
 * -----------
 * Get the start position of next frame of MP3 stream.
 *
 * Syntax
 * ------
 * bool SoftMP3Decoder_LocateNextFrame(DecoderInstance hInstance, const void *pvEncodedData,unsigned long uiTotalEncodedBytes, \
 *								unsigned long *puiFrameByteOffset);
 * Returns true if a valid MP3 frame header was found within the given buffer.  false is 
 * returned if there was none (or too little data was provided).On successful return 
 * 'puiFrameByteOffset' is set to the index of the frame's first byte within 'pvEncodedData'.
 *
 *
 * where:
 *
 *   hInstance      pointer to the structure that holds the internal
 *                  state of the MP3 decoder
 *   pvEncodedData          pointer to the MP3 stream data.
 *   uiTotalEncodedBytes    the total number of stream data.
 *   puiFrameByteOffset     the offset from 'pvEncodedData' to the fist byte of next frame.
 *
 * Return Value
 * ------------
 *   true: found the frame header in the MP3 stream.
 *   false: didn't find the frame header in the MP3 stream.
 *
 */
bool SoftMP3Decoder_LocateNextFrame(DecoderInstance hInstance, const void *pvEncodedData,unsigned long uiTotalEncodedBytes, \
                                 unsigned long *puiFrameByteOffset);

/*
 *       SoftMP3Decoder_DecodeFrameHeader
 *       ================================
 *
 * Description
 * -----------
 * Retrieves the MP3 frame information.
 *
 * Syntax
 * ------
 * bool SoftMP3Decoder_DecodeFrameHeader(DecoderInstance hInstance, const void *pvFrameHeader, unsigned long size, \
 *                                   tMP3FrameInfo *pFrameInfo);
 *
 * Decodes the frame header designated by 'pvFrameHeader'.
 * Use SoftMP3Decoder_LocateNextFrame() to locate that header
 *
 * where:
 *
 *   hInstance      pointer to the structure that holds the internal
 *                  state of the MP3 decoder
 *   pvFrameHeader  pointer to the header of one frame of MP3 stream.
 *   size           size of the stream data.
 *   pFrameInfo     pointer to the structure that holds the stream information. User should support it,
 *                  the size is sizeof(tMP3FrameInfo).
 *
 * Return Value
 * ------------
 *   true: get the frame header information.
 *   false: didn't get the frame header information.
 *
 */

bool SoftMP3Decoder_DecodeFrameHeader(DecoderInstance hInstance, const void *pvFrameHeader, unsigned long size, \
								  tMP3FrameInfo *pFrameInfo);



/*
 *       SoftMP3Decoder_Decode
 *       =====================
 *
 * Description
 * -----------
 * It will decode the MP3 data stream. The stream pointer should be put
 * to the beginning of the stream when you start decoding. The decoder 
 * will automatic skip the header information.
 *
 * If pvDecodedData was not big enough to contain all decoded samples
 * 'pbMoreOutputAvailable' will be set to true and the decoder must be
 * drained of decoded data before supplying any more encoded audio
 * data.  To retrieve these remaining samples, repeatedly call
 * Decoder_Decode() with uiTotalEncodedBytes == 0 (and pvEncodedData
 * == NULL) until pbMoreOutputAvailable is false or an error occurs
 * (Decoder_Decode() returned false).
 *
 * Syntax
 * ------
 * tAudioDecStatus SoftMP3Decoder_Decode(DecoderInstance hInstance, tMP3DecoderIn *MP3decoder_in, \
 *                                   tMP3DecoderOut *MP3decoder_out);
 *
 * where:
 *
 *   hInstance          pointer to the structure that holds the internal state of the MP3 decoder.
 *   MP3decoder_in      pointer to the structure tMP3DecoderIn.
 *   MP3decoder_out     pointer to the structure tMP3DecoderOut.
 *
 * Return Value
 * ------------
 *   tAudioDecStatus
 *   
 */
tAudioDecStatus SoftMP3Decoder_Decode(DecoderInstance hInstance, tMP3DecoderIn *MP3decoder_in, \
								  tMP3DecoderOut *MP3decoder_out);


/*
 *       SoftMP3Decoder_SignalDiscontinuity
 *       ==================================
 *
 * Description
 * -----------
 * The next call to SoftMP3Decoder_Decode() will be with data from a
 * different position within the same audio stream.  The data may be
 * from before or after the decoder's current position within the stream.
 *
 * Syntax
 * ------
 *   tAudioDecStatus SoftMP3Decoder_SignalDiscontinuity(DecoderInstance hInstance);
 *
 * where:
 *
 *   hInstance      pointer to the structure that holds the internal
 *                  state of the MP3 decoder
 *
 * Return Value
 * ------------
 *   tAudioDecStatus
 *
 */

tAudioDecStatus SoftMP3Decoder_SignalDiscontinuity(DecoderInstance hInstance);

/*
 *        SoftMP3Decoder_Finalise
 *        =======================
 *
 * Description
 * -----------
 * Close the MP3 decoder.
 *
 * Called after decoder finishing decode MP3 stream.
 *
 * Syntax
 * ------
 *   tAudioDecStatus SoftMP3Decoder_Finalise(DecoderInstance *phInstance);
 *
 * where:
 *
 *   *phInstance    pointer to the handle that holds the internal
 *                  state of the MP3 decoder
 *
 * Return Value
 * ------------
 *   tAudioDecStatus
 *
 */
tAudioDecStatus SoftMP3Decoder_Finalise(DecoderInstance *phInstance);

#ifdef __cplusplus
}
#endif /* __cplusplus */

/******* MP3 decoder define end ************/
#endif // __SOFT_MP3DEC_H__
