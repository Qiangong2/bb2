/*****************************************************************************
          (c) copyrights 2003-. All rights reserved

           T-Square Design; Inc.

          Company confidential and Properietary information.
          This information may not be disclosed to unauthorized
          individual.
 *****************************************************************************/

/*****************************************************************************
 *
 *    PROJECT: audio decoder library
 *
 *    FILE NAME:  Soft_aacdec.h
 *
 *    DESCRIPTION: This library can decode WMA, MP3, AAC, OGG.
 *
 *    AUTHOR:  ChaoJian_Tang -- ZhaoJian_tang@zh.t2-design.com
 *
 *    HISTORY: 	
 * 			12/01/04  TangChaoJian Create this file, modify content base
 *          on Davil softaudiodec.h.
 *
 *****************************************************************************/

#include "../../softdec_common_h/soft_common.h"

/******* Hereinafter is for AAC decoder ************/

/* AAC audio info */
#ifndef _AACAUDIOINFO_DEFINED
#define _AACAUDIOINFO_DEFINED

#define IN_BUF_SIZE 1024*16

#define AACMAX_TITLE_NUM 256
#define AACMAX_AUTHOR_NUM 256

#define IN
#define OUT
#define OPTION

enum {
	AAC_ADIF_BitRate_Constant,
	AAC_ADIF_BitRate_Variable
} eEncodedBitRateType;

typedef enum {
	AAC_RAW,
	AAC_ADIF,
	AAC_ADTS
} eAACStreamType;

typedef struct tagPCMConfiguration
{
    int bitrate;
    int sample_rate;
    int bitspersample;
    int num_channels;
    int channel_order;
}tPCMConfiguration;

typedef struct tagADIFHdrInfo
{
    char adif_id[4] __attribute__ ((packed));
    bool copyright_id_present __attribute__ ((packed));
    char copyright_id[9]      __attribute__ ((packed));
    bool original_copy;
    bool home;
    int bitstream_type;
    long bitrate;
    int sample_rate;
    int bitspersample;
    int num_channels;
    
    int num_program_config_elements;      
} tADIFHdrInfo;

typedef struct {
	char syncword[2] __attribute__ ((packed));
    int ID;
    int layer;
    int protection_absent;
    int profile;
    int sample_rate;
    int bitrate;
    int channel_configuration;
    bool is_original;
    int home;
  } tAACADTSFrameInfo;

typedef struct {
    bool copyright_id_bit;
    int copyright_start_bit;
    int frame_length;
    int adfs_buffer_fullness;
    int no_raw_data_blocks_in_frame;
    int crc_check;
}tAACADTSVarHdr;

#endif /* _AACAUDIOINFO_DEFINED */


/* AAC Decoder input and output */
#ifndef _AACDECODERINOUT_DEFINED
#define _AACDECODERINOUT_DEFINED
typedef struct tagAACDecoderIn
{
	const void *pvEncodedData; // pointer to the encoded AAC data stream.
	unsigned long uiTotalEncodedBytes; // total number of encoded AAC data in byte.

} tAACDecoderIn;

typedef struct tagAACDecoderOut
{
	void *pvDecodedData; // pointer to decoded PCM data stream.
	unsigned long uiMaxDecodedBytes; //PCM data buffer length in byte. It is used to put the decoded data.
	unsigned long uiBytesWritten; //real number copied PCM data to PCM data buffer.
	unsigned long uiUsed_EncodedData; /*used number of encoded AAC data stream. User should move the AAC
                                 encoded data pointer according this value. This value tell the user how
                                 many encoded data the decoder used. If this value is zero, means decoder
                                 did not use any encoded data, that's because the decoder will decode the
                                 encoded data packet by packet, if the encoded data is less than one packet,
                                 the decoder will not decode it, it will wait the user send more data. 
	                           */
	bool bMoreOutputAvailable; //flag of PCM data transfer end.
	bool bConfigurationChanged;/*if the samplerate/channel mode/bit per sample have changed, this variable set true
	                                otherwise, this set false.  
	                               */

} tAACDecoderOut;

#endif /* _WMADECODERINOUT_DEFINED */

/* ................................................................................. 
 *
 * Functions
 * =========
 */

#ifdef __cplusplus
extern "C"
{
#endif /* __cplusplus */

/*
 *       SoftAACDecoder_BytesRequiredForExamination
 *       ==========================================
 *
 * Description
 * -----------
 * Retrieves the reqired bytes to examine whether the stream is AAC.
 *
 * Syntax
 * ------
 * unsigned long SoftAACDecoder_BytesRequiredForExamination(void);
 * Returns the number of bytes required for SoftAACDecoder_CanDecode() to be able to
 * identify and distinguish the different, supported audio stream types.
 *
 *
 * Return Value
 * ------------
 *   The unmber of bytes need to feed.
 *
 */

unsigned long SoftAACDecoder_BytesRequiredForExamination(void);


/*
 *       SoftAACDecoder_CanDecode
 *       ========================
 *
 * Description
 * -----------
 * Examine the stream type.
 *
 * Syntax
 * ------
 * bool SoftAACDecoder_CanDecode(const void *pvStreamsInitialData,unsigned long uiTotalBytes, unsigned long *puihdrlen);
 *
 * When given at least the number of bytes indicated by SoftAACDecoder_BytesRequiredForExamination()
 * from the start of a data stream it will determine whether or not that data stream is a 
 * supported audio format. If you supply less than the indicated number of bytes it is permitted
 * to always return 'false' (though it may return a valid audio type).
  
 * where:
 *
 *   pvStreamsInitialData    pointer to the stream data need to be examined.
 *   uiTotalBytes            total bytes that will be examined, it should not less than
 *                           the value return from SoftAACDecoder_BytesRequiredForExamination().
 *   puihdrlen               the length of the header of the AAC file. If user want to get the
 *                           header information, he should send more byte than this value.
 *
 * Return Value
 * ------------
 *   true: AAC stream.
 *   false: not AAC stream.
 *
 */

bool   SoftAACDecoder_CanDecode(const void *pvStreamsInitialData,unsigned long uiTotalBytes, unsigned long *puihdrlen);

/*
 *        SoftAACDecoder_Initialise
 *        =========================
 *
 * Description
 * -----------
 * Initializes the AAC decoder.
 *
 * Must be called before starting to decode AAC stream.
 *
 * Syntax
 * ------
 *   tAudioDecStatus SoftAACDecoder_Initialise(DecoderInstance *phInstance);
 *
 * where:
 *
 *   *pInstance     pointer to the handle that holds the internal
 *                  state of the AAC decoder
 *                  User is not permit to change this area
 *
 * Return Value
 * ------------
 *   tAudioDecStatus
 *
 */
tAudioDecStatus SoftAACDecoder_Initialise(DecoderInstance *phInstance);

/*
 *        SoftAACDecoder_GetStreamType
 *        ============================
 *
 * Description
 * -----------
 * Get AAC stream type.
 *
 * Syntax
 * ------
 *   eAACStreamType SoftAACDecoder_GetStreamType(const void *pvStreamsData,unsigned long uiTotalBytes);
 *
 * where:
 *
 *   *pvStreamsData     pointer to the stream data
 *    uiTotalBytes      steam data length
 *
 * Return Value
 * ------------
 *   eAACStreamType
 *      AAC_RAW: This don't present ADIF header,We just assume this stream is ADIF type and with default configuration
 *               Sample_rate:44100, Bitrate:128000,Channels:2 Profile: Mail_Profile bitpersample:16 
 *      AAC_ADIF:This is ADIF, we can use SoftAACDecoder_GetADIFHeader to get configuartion information
 *      AAC_ADTS:This is ADTS type, use SoftAACDecoder_GetADTSHeader to get ADTS information
 */
eAACStreamType SoftAACDecoder_GetStreamType(IN const void *pvStreamsData,IN unsigned long uiTotalBytes);

/*
 *        SoftAACDecoder_GetADIFHeader
 *        ============================
 *
 * Description
 * -----------
 * Get AAC ADIF header information.
 *
 * Syntax
 * ------
 *   eAACStreamType SoftAACDecoder_GetADIFHeader(IN const void *pvStreamsData,IN unsigned long uiTotalBytes,OUT tADIFHdrInfo *pADIFInfo)
 *
 * where:
 *
 *   *pvStreamsData     pointer to the ADIF data
 *    uiTotalBytes      data length
 *   *pADIFInfo     pointer to the structure that holds the ADIF header information. User should support it,
 *                  the size is sizeof(tADIFHdrInfo).
 * Return Value
 * ------------
 *   true: get the ADIF header information.
 *   false: didn't get the ADIF header information.
 */
bool SoftAACDecoder_GetADIFHeader(IN const void *pvStreamsData,IN unsigned long uiTotalBytes,OUT tADIFHdrInfo *pADIFInfo);


/*
 *        SoftAACDecoder_GetADTSHeader
 *        ============================
 *
 * Description
 * -----------
 * Get AAC ADTS frame header information.
 *
 * Syntax
 * ------
 *   bool SoftAACDecoder_GetADTSHeader(IN const void *pvStreamsData,IN unsigned long uiTotalBytes,\
                                    OUT tAACADTSFrameInfo *pADTSInfo,OUT tAACADTSVarHdr *pAACADTSVarHdr);
 *
 * where:
 *
 *   *pvStreamsData     pointer to the ADTS header data
 *    uiTotalBytes      data length
 *   *pADTSInfo         pointer to the structure that holds the ADTS fixed header information. User should support it,
 *                      the size is sizeof(tAACADTSFrameInfo).
 *   *pAACADTSVarHdr    pointer to the structure that holds the ADTS variable header information. User should support it,
 *                      the size is sizeof(tAACADTSFrameInfo).
 * Return Value
 * ------------
 *   true: get the ADIF header information.
 *   false: didn't get the ADIF header information.
 */
bool SoftAACDecoder_GetADTSHeader(IN const void *pvStreamsData,IN unsigned long uiTotalBytes,\
                                    OUT tAACADTSFrameInfo *pADTSInfo,OUT tAACADTSVarHdr *pAACADTSVarHdr);

/*
 *       SoftAACDecoder_LocateNextFrame
 *       ==============================
 *
 * Description
 * -----------
 * Get the start position of next frame of AAC stream.
 *
 * Syntax
 * ------
 * bool SoftAACDecoder_LocateNextFrame(DecoderInstance hInstance, const void *pvEncodedData,unsigned long uiTotalEncodedBytes, \
 *								unsigned long *puiFrameByteOffset);
 * Returns true if a valid AAC frame header was found within the given buffer.  false is 
 * returned if there was none (or too little data was provided).On successful return 
 * 'puiFrameByteOffset' is set to the index of the frame's first byte within 'pvEncodedData'.
 *
 *
 * where:
 *
 *   hInstance      pointer to the structure that holds the internal
 *                  state of the AAC decoder
 *   pvEncodedData          pointer to the AAC stream data.
 *   uiTotalEncodedBytes    the total number of stream data.
 *   puiFrameByteOffset     the offset from 'pvEncodedData' to the fist byte of next frame.
 *
 * Return Value
 * ------------
 *   true: found the frame header in the AAC stream.
 *   false: didn't find the frame header in the AAC stream.
 *
 */
bool SoftAACDecoder_LocateNextFrame(DecoderInstance hInstance, const void *pvEncodedData,unsigned long uiTotalEncodedBytes, \
                                 unsigned long *puiFrameByteOffset);


/*
 *       SoftAACDecoder_DecodeFrameHeader
 *       ================================
 *
 * Description
 * -----------
 * Retrieves the AAC frame information.
 *
 * Syntax
 * ------
 * bool SoftAACDecoder_DecodeFrameHeader(DecoderInstance hInstance, const void *pvFrameHeader, unsigned long size, \
 *                                   tADIFHdrInfo *pFrameInfo);
 *
 * Decodes the frame header designated by 'pvFrameHeader'.
 * Use SoftAACDecoder_LocateNextFrame() to locate that header
 *
 * where:
 *
 *   hInstance      pointer to the structure that holds the internal
 *                  state of the AAC decoder
 *   pvFrameHeader  pointer to the header of one frame of AAC stream.
 *   size           size of the stream data.
 *   pFrameInfo     pointer to the structure that holds the stream information. User should support it,
 *                  the size is sizeof(tADIFHdrInfo).
 *
 * Return Value
 * ------------
 *   true: get the frame header information.
 *   false: didn't get the frame header information.
 *
 */
bool SoftAACDecoder_DecodeFrameHeader(DecoderInstance hInstance, const void *pvFrameHeader, unsigned long size, \
								  tADIFHdrInfo *pFrameInfo);

/*
 *       SoftAACDecoder_GetLastestConfiguration
 *       ======================================
 *
 * Description
 * -----------
 * Retrieves the AAC decoded pcm data configuration.
 *
 * Syntax
 * ------
 * bool SoftAACDecoder_GetLastestConfiguration(DecoderInstance hInstance,tPCMConfiguration *pConfiguration);
 *                                   
 * Get lastest configuration of AAC stream
 * 
 *
 * where:
 *
 *   hInstance      pointer to the structure that holds the internal
 *                  state of the AAC decoder
 *   tPCMConfiguration     pointer to the structure that holds the configuration information. User should support it,
 *                  the size is sizeof(tPCMConfiguration).
 *
 * Return Value
 * ------------
 *   true: get the stream configuration information.
 *   false: didn't get the stream configuration information.
 *
 */
bool SoftAACDecoder_GetLastestConfiguration(DecoderInstance hInstance,tPCMConfiguration *pConfiguration);

/*
 *       SoftAACDecoder_Decode
 *       =====================
 *
 * Description
 * -----------
 * It will decode the AAC data stream. The stream pointer should be put
 * to the beginning of the stream when you start decoding. The decoder 
 * will automatic skip the header information.
 *
 * If pvDecodedData was not big enough to contain all decoded samples
 * 'pbMoreOutputAvailable' will be set to true and the decoder must be
 * drained of decoded data before supplying any more encoded audio
 * data.  To retrieve these remaining samples, repeatedly call
 * Decoder_Decode() with uiTotalEncodedBytes == 0 (and pvEncodedData
 * == NULL) until pbMoreOutputAvailable is false or an error occurs
 * (Decoder_Decode() returned false).
 *
 * Syntax
 * ------
 * tAudioDecStatus SoftAACDecoder_Decode(DecoderInstance hInstance, tAACDecoderIn *AACdecoder_in, \
 *                                   tAACDecoderOut *AACdecoder_out);
 *
 * where:
 *
 *   hInstance        pointer to the structure that holds the internal state of the AAC decoder. 
 *   AACdecoder_in    pointer to the structure tAACDecoderIn.
 *   AACdecoder_out   pointer to the structure tAACDecoderOut.
 *
 * Return Value
 * ------------
 *   tAudioDecStatus
 *   
 */
tAudioDecStatus SoftAACDecoder_Decode(DecoderInstance hInstance, tAACDecoderIn *AACdecoder_in, 
								  tAACDecoderOut *AACdecoder_out);


/*
 *       SoftAACDecoder_SignalDiscontinuity
 *       ==================================
 *
 * Description
 * -----------
 * The next call to SoftAACDecoder_Decode() will be with data from a
 * different position within the same audio stream.  The data may be
 * from before or after the decoder's current position within the stream.
 *
 * Syntax
 * ------
 *   tAudioDecStatus SoftAACDecoder_SignalDiscontinuity(DecoderInstance hInstance);
 *
 * where:
 *
 *   hInstance      pointer to the structure that holds the internal
 *                  state of the AAC decoder
 *
 * Return Value
 * ------------
 *   tAudioDecStatus
 *
 */

tAudioDecStatus SoftAACDecoder_SignalDiscontinuity(DecoderInstance hInstance);

/*
 *        SoftAACDecoder_Finalise
 *        =======================
 *
 * Description
 * -----------
 * Close the AAC decoder.
 *
 * Called after decoder finishing decode AAC stream.
 *
 * Syntax
 * ------
 *   tAudioDecStatus SoftAACDecoder_Finalise(DecoderInstance *phInstance);
 *
 * where:
 *
 *   *phInstance    pointer to the handle that holds the internal
 *                  state of the AAC decoder
 *
 * Return Value
 * ------------
 *   tAudioDecStatus
 *
 */
tAudioDecStatus SoftAACDecoder_Finalise(DecoderInstance *phInstance);

#ifdef __cplusplus
}
#endif /* __cplusplus */
/******* AAC decoder define end ************/



