/*****************************************************************************
          (c) copyrights 2003-. All rights reserved

           T-Square Design; Inc.

          Company confidential and Properietary information.
          This information may not be disclosed to unauthorized
          individual.
 *****************************************************************************/

/*****************************************************************************
 *
 *    PROJECT: audio decoder library
 *
 *    FILE NAME:  audiodec.h
 *
 *    DESCRIPTION: This library can decode WMA, MP3, AAC, OGG.
 *
 *    AUTHOR:  davil ding -- davil_ding@zh.t2-design.com
 *
 *    HISTORY: 	11/25/03  created  by davil ding
 *              12/05/03  davil updated it.
 *              12/09/03  davil updated the xxxDecoder_decode() function define. 
 *                        It will more flexible for updating later.
 *              12/10/03  davil changed the functions name with prefix soft.
 *
 *****************************************************************************/

/* *******************!!!!!Note: very important !!!!!*************************
 * all the output data is linear PCM format.

 * WMA decoder only can decoder the whole file, if the stream data is not a complete
 * file, it can not decoder without the header information.

 * MP3 decoder only can decoder any MP3 data, but it must find the begin of one frame.

 * AAC decoder specification is not ready, we don't know enough about it, we are gathering the information.

 * OGG decoder specification is not ready, we don't know enough about it, we are gathering the information.
 *****************************************************************************/


#ifndef _SOFT_DEC_COMMON_H_
#define _SOFT_DEC_COMMON_H_

typedef int bool;

#ifndef DECODERINSTANCE_DEFINED
#define DECODERINSTANCE_DEFINED
typedef void* DecoderInstance;
#endif

#ifndef AUDIODECSTATUS_DEFINED
#define AUDIODECSTATUS_DEFINED
typedef enum tagAudioDecStatus
{
    cAudio_NoErr,                 /* -> always first entry */
                                /* remaining entry order is not guaranteed */
    cAudio_Failed,
    cAudio_BadArgument,
    cAudio_BadAsfHeader,
    cAudio_BadPacketHeader,
    cAudio_BrokenFrame,
    cAudio_NoMoreFrames,
    cAudio_BadSamplingRate,
    cAudio_BadNumberOfChannels,
    cAudio_BadVersionNumber,
    cAudio_BadWeightingMode,
    cAudio_BadPacketization,

    cAudio_BadDRMType,
    cAudio_DRMFailed,
    cAudio_DRMUnsupported,

    cAudio_DemoExpired,

    cAudio_BadState,
    cAudio_Internal,               /* really bad */
    cAudio_NoMoreDataThisTime,
	cAudio_NoMoreBuffer
} tAudioDecStatus;
#endif /* AUDIODECSTATUS_DEFINED */


/* stream type */

/***** !!!!!!!very important !!!!!!!!!!! *************
*** DO NOT CHANGE THE ORDER, ONLY ADD AT THE END
*** (BEFORE cAudio_ST_UNKNOWN), when you add a new
*** type, please add the decoder functions as well,
*** and add the functions to AudioDecoder_Select. ***/

#ifndef _AUDIOSTREAMTYPE_DEFINED
#define _AUDIOSTREAMTYPE_DEFINED
typedef enum tagAudioStreamType
{
    cAudio_ST_WMA,
    cAudio_ST_MP3,
    cAudio_ST_AAC,
    cAudio_ST_OGG,
    cAudio_ST_UNKNOWN
} tAudioStreamType;

#define AUDIO_FORMAT_NUM	(cAudio_ST_UNKNOWN - cAudio_ST_WMA)
#endif /* _AUDIOSTREAMTYPE_DEFINED */

/* sample rates */
#ifndef _AUDIOSAMPLERATE_DEFINED
#define _AUDIOSAMPLERATE_DEFINED
typedef enum tagAudioSampleRate
{
    cAudio_SR_48kHz,
    cAudio_SR_44_1kHz,
    cAudio_SR_32kHz,
    cAudio_SR_22_05kHz,
    cAudio_SR_16kHz,
    cAudio_SR_11_025kHz,
    cAudio_SR_08kHz
} tAudioSampleRate;
#endif /* _AUDIOSAMPLERATE_DEFINED */

/* channels */
#ifndef _AUDIOCHANNELS_DEFINED
#define _AUDIOCHANNELS_DEFINED
typedef enum tagAudioChannels
{
    cAudio_C_Mono = 1,
    cAudio_C_Stereo = 2

} tAudioChannels;
#endif /* _AUDIOCHANNELS_DEFINED */

/* Bits per sample */
#ifndef _BITSPERSAMPLE_DEFINED
#define _BITSPERSAMPLE_DEFINED
typedef enum tagBitsPerSample
{
    cAudio_BPS_8 = 8,
    cAudio_BPS_16 = 16

} tBitsPerSample;
#endif /* _BITSPERSAMPLE_DEFINED */

/* sample data Signedness */
#ifndef _SAMPLESIGNEDNESS_DEFINED
#define _SAMPLESIGNEDNESS_DEFINED
typedef enum tagSampleSignedness
{
    cAudio_Unsigned,
    cAudio_Signed

} tSampleSigness;
#endif /* _SAMPLESIGNEDNESS_DEFINED */

/* sample data endianness */
#ifndef _SAMPLEENDIANNESS_DEFINED
#define _SAMPLEENDIANNESS_DEFINED
typedef enum tagSampleEndianness
{
    cAudio_BigEndian,
    cAudio_LittleEndian

} tSampleEndian;
#endif /* _SAMPLEENDIANNESS_DEFINED */

/* sample data order */
#ifndef _SAMPLEDATAORDER_DEFINED
#define _SAMPLEDATAORDER_DEFINED
typedef enum tagSampleDataOrder
{
    cAudio_LRLR,
    cAudio_RLRL

} tSampleDataOrder;
#endif /* _SAMPLEDATAORDER_DEFINED */

#ifndef _GENERALAUDIOINFO_DEFINED
#define _GENERALAUDIOINFO_DEFINED
typedef struct tagTrack_Name
{
	bool bPresent;
	unsigned char* puiTrackName; // Only valid if bPresent is true
} tTrack_Name;

typedef struct tagTrack_Duration
{
	bool bPresent;
	unsigned long uiTrackDuration; // of the file in milliseconds, Only valid if bPresent is true
} tTrack_Duration;

typedef struct tagArtist
{
	bool bPresent;
	unsigned long uiArtist; // Only valid if bPresent is true
} tArtist;

typedef struct tagAlbum
{
	bool bPresent;
	unsigned long uiArtist; // Only valid if bPresent is true
} tAlbum;

typedef struct tagOriginal_Track_Number
{
	bool bPresent;
	unsigned long uiOriginalTrackNumber; // Only valid if bPresent is true
} tOriginal_Track_Number;

typedef struct tagDate
{
	bool bPresent;
	unsigned long uiDate; // Only valid if bPresent is true
} tDate;

typedef struct tagTotal_file_size
{
	bool bPresent;
	unsigned long uiTotalfilesize; // Only valid if bPresent is true
} tTotal_file_size;

#endif /* _GENERALAUDIOINFO_DEFINED */

#endif
