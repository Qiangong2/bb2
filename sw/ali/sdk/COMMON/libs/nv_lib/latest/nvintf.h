/*-----------------------------------------------------------------------------
    
    Copyright (C) 2002 T2-Design Corp.  All rights reserved.
    
    File: nvintf.h
    
    Content: 
        This header file defined all the interface functions of navigator
    History: 
    2003/11/10 by michael
        Created
        
-----------------------------------------------------------------------------*/
#ifndef _NVINTF_H_
#define _NVINTF_H_

#include <t2typedef.h>
#include <t2mpgdec.h>
#include <t2video.h>
#include <t2audio.h>


#if (_PRODUCT_TARGET==T6305_DVD || _PRODUCT_TARGET==T6305_NETDVD)        
#define PE_STACK_SIZE       0x0000  //8k
#define DVDENG_STACK_SIZE   0x4000  //16k
#define VOBU_STACK_SIZE     0x0000  //4k
#define DEC_STACK_SIZE      0x8000  //32k
#else
#define PE_STACK_SIZE       0x0000  //6k
#define DVDENG_STACK_SIZE   0x1000  //2k
#define VOBU_STACK_SIZE     0x0000  //4k
#define DEC_STACK_SIZE      0x6000  //24k
#endif

#ifdef _USB_E_
#define RING_STACK_SIZE     0x4000
#else
#define RING_STACK_SIZE     0x0000
#endif


#ifdef _USB_E_
#define PEW_STACK_SIZE      0x1000  //4k
#define ENC_STACK_SIZE      0x1000  //4k
#else
#define PEW_STACK_SIZE      0x0  
#define ENC_STACK_SIZE      0x0  
#endif
#if (_PRODUCT_TARGET==T6305_DVD || _PRODUCT_TARGET==T6305_NETDVD)
#define ATA_STACK_SIZE      0x1000  //4k
#else
#define ATA_STACK_SIZE      0x1000  //4k
#endif

#define NVCALLBACK
#define EXPORT

#define NV_FORCE_TO_NEXT 0xffffffff

//NV error message
#define NVERR_OK        0
#define NVERR_REGION    1 //DVD region error
#define NVERR_BADDATA   2
#define NVERR_UNKNOWN   0xffffffff

/* for MSG2UI.Type is C_NV_NOTICE the value of bNV_EndMode*/
#define C_DISC_END                      0x01
#define C_TITLE_END                     0x02
#define C_CHAPTER_END                   0x04
#define C_POINT_END                     0x08
#define C_DVIEW_END                     0x10
#define C_NORMAL_END                    0x00

/*Display time type*/
//NV time format define
#define C_TT_TM                                         1
#define C_REMAIN_TT_TM                          2
#define C_CH_TM                                         3
#define C_REMAIN_CH_TM                          4

#define C_TRACK_TM                                      1
#define C_REMAIN_TRACK_TM                       2
#define C_DISC_TM                                       3
#define C_REMAIN_DISC_TM                        4

#define C_NO_OPERATION          0x0
#define C_STOP_SKIPPG           0x2

#define C_NV_Invalid            0xFFFF
#define C_NV_Play               0
#define C_NV_Fast               1
#define C_NV_Slow               2
#define C_NV_Reverse            3       /* for backward play */
#define C_NV_Pause              4
#define C_NV_Step               5
#define C_NV_Stop               6
#define C_NV_Flush              7


#define KEY_NULL        0
#define KEY_TIMER       1
#define KEY_TITLE       2
#define KEY_PTT         3
#define KEY_PROGRAM     4

//HL macro for NVHLKey()'s HL_KEY
#define HL_UP                   0
#define HL_DOWN                 1
#define HL_LEFT                 2
#define HL_RIGHT                3
#define HL_ENTER                4

#define   FP_DOM   0
#define VMGM_DOM   1
#define VTSM_DOM   2
#define   TT_DOM   3

/******* VOBU Flag Event *********/
#define VOBU_STL_OK_EVENT               0x00010000
#define VOBU_STL_SKIP_EVENT             0x00020000
#define VOBU_WAIT_EVENT                 0x00010000

#define C_VOBU_END              0x00    /*VOBU (cell) end msg from VOBU*/
#define C_VOBU_STILL            0x01    /*VOBU still msg from VOBU*/

//This language code is followed ISO639-1
#define LC_ENGLISH          0x656e  /*** "en" ***/
#define LC_GERMAN           0x6465  /*** "de" ***/
#define LC_GREEK            0x656c  /*** "el" ***/
#define LC_ARABIC           0x6172  /*** "ar" ***/
#define LC_SPANISH          0x6573  /*** "es" ***/
#define LC_FRENCH           0x6672  /*** "fr" ***/
#define LC_ITALIN           0x6974  /*** "it" ***/
#define LC_JANANESE         0x6a61  /*** "ja" ***/
#define LC_KOREAN           0x6b6f  /*** "ko" ***/
#define LC_DUTCH            0x6e6c  /*** "nl" ***/
#define LC_RUSSIAN          0x7275  /*** "ru" ***/
#define LC_CHINESE          0x7a68  /*** "zh  ***/
#define LC_THAI             0x7468  /*** "th" ***/
#define LC_POLISH           0x706c  /*** "pl" ***/
#define LC_ICELANDIC        0x6973  /*** "is" ***/
#define LC_INDONESIAN       0x696e  /*** "in" ***/

//The order is according to setup menu
//NV country define
#define L_CHINESE         0
#define L_ENGLISH         1
#define L_SPANISH         2
#define L_FRENCH          3
#define L_JANANESE        4
#define L_KOREAN          5
#define L_RUSSIAN         6
#define L_THAI            7
#define L_GERMAN          8
#define L_OTHER           9
    
// Disc Type
typedef enum
{
    C_UNKNOWN_DISC = 0,
    C_DVD = 1,
    C_VCD10 = 2,
    C_VCD20 = 3,
    C_CDDA = 4,
    C_SVCD = 5,
    C_FILEOPENDISC = 6,
    C_CDI = 7,
    C_DVD_AUDIO = 8,
    C_CDG = 9,
    C_DTSCD = 10,
    C_NO_DISC = 11, 
    C_MINIDVD = 12, 
    C_DISC_MAX
}tDiscType;

// NV Paly Info
typedef struct tagPLAYPOINT
{
    WORD    wTTN;
    WORD    wPTTN;
    DWORD   dwLBAAddr;
    BYTE    bDomain;
    BYTE    bMenuID;
    WORD    FileTp;
} PLAYPOINT, *LPPLAYPOINT;

typedef struct 
{
    struct tagPLAYPOINT;
    WORD GPRM[16]; //Only available for DVD
    WORD SPRM[26]; //Only available for DVD
    BYTE bAudioLang;
    BYTE bSubtitleLang;
}RESUMEPOINT, *LPRESUMEPOINT;

// Drive Type
typedef enum
{
    DT_ROOT = 0, 

    DT_CdRom, 
    DT_Usb1, 
    DT_Usb2, 
    DT_HardDisk, 
    DT_Net, 
    DT_Image, 
    DT_InetRadio, 

    DT_MAX
}DRIVE_TYPE;

typedef struct tagSubTTInfo
{
        BYTE SubTTTotalNo;              /* Subtitle ID Number.*/
        BYTE SubTTNo;                   /* Current Subtitle ID Number.*/
        BYTE SubTTOnOff;                /* Current Subtitle On/Off.*/
} SUBTTINFO, *LPSUBTTINFO;

typedef struct tagAudioStream
{
        int AStreamTotalNo;             /* Audio Stream ID total Number.*/
        int AStreamNo;                          /* Current Audio Stream ID Number.*/
} AUDIOSTREAM, *LPAUDIOSTREAM;

typedef struct tagAngleInfo
{
        BYTE AngleTotalNo;              /* Angle total ID Number.*/
        BYTE AngleNo;                   /* Current Angle ID Number.*/
} ANGLEINFO, *LPANGLEINFO;

typedef enum    {
                ALL_MENU                =                               0,//==>MenuID =0 it is mean all kind of menu
        TITLE_MENU              =               2,
        ROOT_MENU               =               3,
        SUBPIC_MENU             =               4,
                AUDIO_MENU              =                               5,
                ANGLE_MENU              =                               6,
                PTT_MENU                =                               7
} tDVDMenuID;

typedef enum {
        OP_ZOOM = 0,
        OP_MENU = 1,
        OP_SUB = 2,
        OP_AUDIO = 3,
        OP_TITLESEARCH = 4,
        OP_SKIPPG = 5
        
}tOPType;

// Media Type
typedef enum
{
    MT_MIN = 0, 
    MT_Video = MT_MIN, 
    MT_Audio, 
    MT_Picture, 
    MT_Stream, 
    MT_Other, 
    MT_AllMedia, 
    MT_MAX
}tMediaType;

// File Type
typedef enum
{
    C_UNKNOWN_FILE = 0,
    C_MP3FILE = 1,
    C_JPGFILE = 2,
    C_MP4FILE = 3, 
    C_MPGFILE = 4,
    C_DTSFILE = 5, 
    C_WMVFILE = 6, 
	C_WMAFILE = 7, 
    C_AC3FILE = 8,
    C_IMGFILE = 9,

    C_MP3IRD = 10, 

    C_FILE_MAX
}tFileType;

// Audio Type
typedef enum
{/* See DVD Spec Part 3 p4-50 */
    C_AC3 = 0,
    C_MPEG = 1,
    C_MPEGI = 2,
    C_MPEGII = 3,
    C_LPCM = 4,
    C_CDDA_AUDIO = 5,
    C_DTS = 6,
    C_SDDS = 7,
    C_MP3_AUDIO = 8,
    C_MLP = 9,
    C_MPEG_MCH = 10,
    C_SONYSACD = 11,
    C_CDG_AUDIO    = 12,
    C_DTS_CD_AUDIO = 13,
    C_INVALID_TYPE = 14, 

    C_AUDIOTYPE_MAX
}tAudType;

typedef enum {
  psStop      = 0,
  psPlay      = 1,
  psPauseStep = 2,
  psFFx2      = 3,
  psFFx4      = 4,
  psFFx8x16x32 = 5,
  psSlow      = 6,
  psRev       = 7
} tPlayState;

typedef struct _DISC_INFO
{
    tDiscType eDiscType;
    WORD wTotalTitle;
}DISC_INFO, *PDISC_INFO;

typedef struct _NV_AC3TEST_PARAM
{
    WORD wDaulMode;
    WORD wDRCMode;
    WORD wLFEChn;
    WORD wKaraokeMode;
    WORD wVocal;
    WORD wOutputMode;
    WORD wStereoMode;
    WORD wSubWoofer;
    WORD wBass;
    WORD wIEC;
    DWORD dwPCMScale;
    DWORD dwDRCLowScale;
    DWORD dwDRCHighScale;
}NV_AC3TEST_PARAM, *PNV_AC3TEST_PARAM;

// VOBU Message
typedef struct tagACELL
{
    DWORD  StartLBA;
    DWORD  EndLBA;
    DWORD  OffsetSec;                /* Offset of Start LBA     */
    DWORD  OffSrchLBA;               /* the LBA for time search */
    WORD   ID;
    short  Speed;                    /* for scan speed */
    DWORD  Time;                     /* for search time */
    int    PackType;
    WORD   wPGN;
}ACELL, *LPACELL;
typedef struct tagMSG2AOBU
{
    BYTE    Cmd;
    ACELL    Cell;                      /* DVD cell info  */
}MSG2AOBU, *LPMSG2AOBU;

typedef BYTE t_Domain;

// CHECK interface
EXPORT BOOL NVChkOPPermit(tOPType Operation, tDVDMenuID MenuID);
EXPORT BOOL NVRegionChk();
EXPORT BOOL NVChkVCDPSDExist();
EXPORT BOOL NVTestDiscType(tDiscType eDisc, PDISC_INFO pDiscInfo);
EXPORT BOOL NVCheckVirDiscType(tDiscType eDisc);
EXPORT BOOL NVCheckDiscType(tDiscType eDisc);
EXPORT BOOL NVIsInPBCMenu();
EXPORT BOOL NVChkIsMenu(void);
EXPORT BOOL NVChkIfChapterEnd(void);
EXPORT BOOL NVChkIfTitleEnd(void);
EXPORT BOOL NVChkIfDiscEnd(void);
EXPORT BOOL NVChkIfSecEnd(void);
EXPORT BOOL NVCheckFileType(tFileType eFile);
EXPORT BOOL NVCheckMediaType(tMediaType eMedia);
EXPORT BOOL NVChkLockTimeUpd();
EXPORT BOOL NVInfiniteStill();

// GET interface
EXPORT WORD NVGetTTNsOrTrks();
EXPORT BOOL NVGetCurInfo(LPPLAYPOINT lpPlayPoint);
EXPORT BOOL NVGetCurPoint(LPPLAYPOINT lpPlayPoint);
EXPORT BOOL NVGetResumePoint(LPRESUMEPOINT lpResumePoint);
EXPORT DWORD NVGetVideoTime(BYTE bMode);
EXPORT DWORD NVGetTotalTime();
EXPORT WORD NVGetTTPTTNs(WORD ttn);
EXPORT DWORD NVGetTrackTime(WORD wTrack, WORD FileType, tDiscType eDiscType);
EXPORT WORD NVGetLanguageID(BYTE bType, BYTE bStreamNum);
EXPORT BOOL NVGetSPInfo(LPSUBTTINFO lpSubTTInfo);
EXPORT BOOL NVGetAudioInfo (LPAUDIOSTREAM lpAudioStreamInfo);
EXPORT BOOL NVGetAGLInfo(LPANGLEINFO lpAngleInfo);
EXPORT BYTE NVChkIsHighLight();
EXPORT WORD NVGetCrnTrackNo(void);
EXPORT DWORD NVGetLastError();
EXPORT WORD NVGetPBCState();
EXPORT tAudType NVGetAudType();
EXPORT tDiscType NV_GET_DT();
EXPORT tFileType NV_GET_FT();
EXPORT tMediaType NV_GET_MT();
EXPORT BYTE NVGetEndMode();
EXPORT WORD NVGetPlayMode();
EXPORT WORD NVGetVOBUID();
EXPORT void NVGetAC3TestParam(PNV_AC3TEST_PARAM pAC3Param);
EXPORT t_Domain NVGetCurDomain();

// SET interface
EXPORT void NV_SET_FT(tFileType eFile);
EXPORT void NV_SET_DT(tDiscType eDisc);
EXPORT void NV_SET_MT(tMediaType eMedia);
EXPORT void NVSetTVOutputMode(tNTSC_PAL tv_type);
EXPORT void NVSetEndMode(BYTE bEndMode);
EXPORT void NVSetPoint(LPPLAYPOINT lpPlayPointA, LPPLAYPOINT lpPlayPointB);
EXPORT BOOL NVSPIdSet(WORD SPId, WORD Mode);
EXPORT BOOL NVSetSpSW(BOOL bOnOff);
EXPORT BOOL NVAudstreamChange(WORD wAudId);
EXPORT BOOL NVAngleChange(WORD wAngled);
EXPORT void NVSetRegion(BYTE bRegion);
EXPORT void NVSetParentLevel(BYTE bLevel);
EXPORT void NVSetPWRes(BOOL PWChkResult);
EXPORT void NVSetMenuLanguage(BYTE bMlanguage);
EXPORT void NVSetDialogue(BYTE bDialogue);
EXPORT void NVSetCaption(BYTE bCaption);
EXPORT void NVSetTVScreen(BYTE bScreen);
EXPORT void NVChangeDisc(PDISC_INFO pDiscInfo);
EXPORT void NVSetMacroVision(BYTE bSel);
EXPORT void NVSetAudType(tAudType audiotype);
EXPORT void NVPBCChg(BYTE PBCflg);
EXPORT BOOL NVHLKey(BYTE bHL_KEY);
EXPORT void NVSetProgEndABRepeate(BOOL InProgEndABRepeat);
EXPORT void NVSetVOBUEvent(DWORD VOBUEvent);
EXPORT void NVSetAC3TestParam(PNV_AC3TEST_PARAM pAC3Param);

// PLAYBACK control interface
EXPORT BOOL NVFirstPlay();
EXPORT BOOL NVDefaultPlay();
EXPORT void NVPlayNextProgram(DWORD PlayResult);
EXPORT BOOL NVPTTPlay(LPPLAYPOINT lpPlayPoint, BYTE key, BOOL IsPreStop);
EXPORT BOOL NVNumPlay(WORD wNum);
EXPORT BOOL NVSkipPG(int ofst);
EXPORT BOOL NVTimeSearch(DWORD sec);
EXPORT BOOL NVPlayModeChg(WORD mode, BYTE bRatio);
EXPORT BOOL NVMenuCall(tDVDMenuID MenuID);
EXPORT void NVTimeOut(void);
EXPORT BOOL NVReturn(void);
EXPORT void NVStop();
EXPORT void NVEnd(void);
EXPORT void NVInit();
EXPORT void NVReset();
EXPORT void NVForceStop();
EXPORT void NVRelease();

// MISC interface
EXPORT BOOL NVCmdVOBUMsg(BYTE wCmd, BYTE bRatio);
EXPORT BOOL NVCreateResources();
EXPORT BOOL NVRun();

// NV Import Interface
typedef struct _FILE_NODE
{
    tFileType eType;    // file type
    DWORD dwSize;       // file size

    DWORD dwPathSize; // path buffer size 
    LPCHAR Path;  // pointer to path buffer
}FILE_NODE, *PFILE_NODE;
NVCALLBACK BOOL GetFileNode(tMediaType eMediaType, WORD eMediaIndex, PFILE_NODE pFileNode);

NVCALLBACK void NVNoteUI_NormalPlay();              // for state changed to normal play
NVCALLBACK void NVNoteUI_Stop();                    // for state changed to stop
NVCALLBACK void NVNoteUI_SegEnd(DWORD Msg_Input);                   // for UI-named segment end happen, note ui to send next segment to nv to play
NVCALLBACK void NVNoteUI_PlayFailure();
NVCALLBACK void NVNoteUI_TitleChange();     // for DVD Title change
NVCALLBACK void NVNoteUI_ChapterChange();   // for DVD chapter change
NVCALLBACK void NVNoteUI_DomainChange();    // for DVD domain change
NVCALLBACK void NVNoteUI_AudioChange();     // for DVD audio change
NVCALLBACK void NVNoteUI_AngleChange();     // for DVD angle change
NVCALLBACK void NVNoteUI_SubPicChange();    // for DVD subpic change
NVCALLBACK void NVNoteUI_VCDDiscErr();              // for vcd disc err
NVCALLBACK void NVNoteUI_VCDNoData();               // for vcd no data
NVCALLBACK void NVNoteUI_TrackChange();     // for none-dvd track change
NVCALLBACK void NVNoteUI_EntryChange();     // for VCD2.0 and SVCD entry change
NVCALLBACK void NVNoteUI_VCDPBCOff();               // for VCD2.0 and SVCD PBC Off
NVCALLBACK void NVUIPWChk(BYTE bTmp); //NV note UI to process parental password check.
NVCALLBACK void NVNoteUI_Transferring();
NVCALLBACK void NVNoteUI_TransferEnd();
NVCALLBACK void NVNoteUI_DVDHLTimer();
NVCALLBACK void NVNoteUI_EnterKey();
NVCALLBACK void NVNoteUI_DVDReday();
NVCALLBACK void NVNoteUI_VCDTimeOut();
//NVCALLBACK void NVNoteUI_SlideShowTimer();
NVCALLBACK void NVNoteUI_CloseZoom();
NVCALLBACK void NVNoteUI_CloseDVDConsoleKey();
NVCALLBACK void NVNoteUI_VolumeMute(BOOL Mute);
NVCALLBACK void Send_UI_Message(WORD MsgUI_Type, DWORD Msg_Input, DWORD Msg_Flag);
NVCALLBACK void NVNoteUI_InitDecoderBuffer(t2DecoderCap *PDecoderCap, BYTE StreamType, 
    t2FrameBuffer *FrameBuffer, t2DecOtherBuf *OtherBuffer);
NVCALLBACK void NVNoteUI_SetMP4FBNumber();

#endif

