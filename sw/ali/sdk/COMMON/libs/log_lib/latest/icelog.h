#ifndef	_ICE_LOG_H_
#define _ICE_LOG_H_

/*
 *	Library		:	liblog.a
 *
 *	Last update	:	2003-12-26 14:39 Kenneth
 */

/*
 *	printf a string to ice.
 */
void soc_ice_printf(const char *str);


/*
 *	Print formatted output to ice.
 *
 *	this function doest not support all the format flag,it only support the most common used format flag.
 *
 *	the stack request by this function is small,so it useful at some situation.
 *
 *	if you don't like this function, you can write soc_printf yourself. user's soc_printf will override
 *	this one, for how to implement a function like this, please reference follow code.
 */

void soc_printf(const char *fmt,...);

/*--------------------------------------------------------
#include <stdio.h>
#include <string.h>
#include <stdarg.h>

char m_str[512];

//
//	this soc_printf request more stack than the default soc_printf, but it also 
//	powerful than the default one.
//
 	
void soc_printf(const char *fmt,...)
{
	va_list ap;
	va_start(ap,fmt);
	vsprintf(m_str,fmt,ap);
	soc_ice_printf(m_str);
	va_end(ap);
}

----------------------------------------------------------*/
#endif