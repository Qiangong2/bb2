/******************************************************************************
 * File name: imagedec.h
 * Author   : frank pan
 * Description:
 *			The image decoder can support JPEG, BMP, PNG, GIF and simple TIFF image decoding, 
 *			the called sequence of interface funciton is
 *		   (1) SetMpgFunctions(), set some functions' pointer for the decoder, those 
				functions are define outside the image decoder,
 *		   (2) PicDec_GetInfo(), get the information of the image, you must call it first, that you can know
						the image type, size and the decoded needed buffer size.
 *		   (3) SetTempBufPoint(), specify the temporary buffer that can be used for decoding, the buffer size can
						be set according to the information that get by PicDec_GetInofo() function.
 * 		   (4) SetTVWHRatio(), set the TV width/height ratio and output width and 
			   height of output frame buffer.
 *         (5) ImageDecode(), decode all supported image to specify buffer.
 *         (6) JPGDecode(), decode a JPEG image to specify buffer.
 *         (7) BMPDecode(), decode a BMP image to specify buffer.
 *         (8) PNGDecode(), decode a PNG image to specify buffer.
 *         (9) GIFDecode(), decode a GIF image to specify buffer.
 *         (10) TIFFDecode(), decode a TIFF image to specify buffer.
 *		   (11) PicDec_Rotate(), rotate current decoded image to specify buffer.
 *         (12) ImageAbortDecoding(), if you want to stop decoding, call this.
				replace the original JPGDecoderReset()

		NOTE:
		So far, the image library only support YUV420 output, so the ouput buffer size ratio
		Y:U:V = 4:1:1. And now it only can decode JPEG format image, the maximal resolution of 
		JPEG image is 8000x8000 pixels. 
		
		Function pointers:
	2 important function pointers unsigned long (*readfunc)() and void 
	(*seekfunc) are used for reading data from data stream (DVD ROM 
	or even memory) into a memory, many following interface functions must 
	use the 2 function pointers. The user must offer the function pointers, the 
	2 functions just like fread() and fseek().

	(1)	unsigned long (*readfunc)(unsigned char *buffer, int size) 
      	Description: read data from loader into the buffer.
        	Parameters:
			buffer: the reading data will be stored into the buffer.
			size: the data byte size that user want to read.
	     	Return: the actual data byte size that reading from the data stream, 
				if it read nothing or some error happens, it will return 0.

	(2) void (*seekfunc)(long offset, int origin)
		Description: moves the data pointer (if any) associated with data 
			stream to a new location that is offset bytes from origin. The 
			next operation on the data stream takes place at the new location 
		Parameters:
			Offset: number of bytes from origin
			Origin: 0 - data pointer is at beginning of data stream.
			 		1 - current position of data pointer.
			 		2 - data pointer is at end of data stream.
	   		Note: In the image library, it only use seekfunc (0, 0) to locate 
				to the beginning of data stream, other parameter values is not used.
	
 *****************************************************************************/

#ifndef	__IMAGEDEC_H
#define __IMAGEDEC_H


#define BOOL int

//Define the key values of remote control
//it can get by GetRemotedCmd() function.
//none: can not get the key.
//normal: normal play mode
//skip_b: only skip b frame in FF mode
//skip_b_p: skip both b and p in FF mode
//step: step or pause mode
//stop: stop mode
#define IMAGE_PLAY_MODE_NONE			-1
#define IMAGE_PLAY_MODE_NORMAL			0
#define IMAGE_PLAY_MODE_SKIP_B			1
#define IMAGE_PLAY_MODE_SKIP_B_P		2
#define IMAGE_PLAY_MODE_STEP			3
#define IMAGE_PLAY_MODE_STOP         	4

typedef enum PICDEC_OUTPUT_MODE_t
{
	YUV = 0,
	RGB565,
	RGB888
}PICDEC_OUTPUT_MODE;

typedef enum PICDEC_IMAGE_TYPE_t
{
	SEQUENTIAL_JPEG,	// 0
	PROGRESSIVE_JPEG,	// 1
	JPEG_2000,			// 2
	PNG,				// 3
	BMP,				// 4
	GIF,				// 5
	TIFF,				// 6
	UNKNOWN				// 7
}PICDEC_IMAGE_TYPE;

typedef struct picdec_info_t
{
    int type; // type can be one of IMAGE_TYPE
    int xsize; // x real size of the Image.
    int ysize; // y real size of the Image.
	int frames; // If the image is an animation, this holds the total number of frames
} picdec_info;


typedef struct
{
	unsigned long (*TellPos)(); // get the position of the data stream.
	int (*GetRemotedCmd)(void);	//Get the key of remote controler, please set NULL
								//if not use it.
	void (*Sleep)(int n);		//Sleep for n milliseconds, please set NULL if 
								//not use it.
	void (*ShowFrame)
		(		//Callback function, the decoder would call it to notice user that
				// an GIF frame is decoded, so that the user can process the frame,
				// show it of other process.
			unsigned char *y_buf,	//the y component of the decoded GIF frame
			unsigned char *cb_buf,  //the cb component of the decoded GIF frame
			unsigned char *cr_buf	//the cr component of the decoded GIF frame
		);
}MPG_FUNCTIONS, *PMPG_FUNCTIONS;


/*----------------------------------------------------
Name: 
    ImageAbortDecoding
Description: 
	Abort(Stop) decoding the current image.
    
Parameters: 
    IN:
    OUT:
    
Return: 
	None
------------------------------------------------------*/
void ImageAbortDecoding();


/*----------------------------------------------------
Name: 
    SetMpgFunctions()
Description: 
	Set function pointer for decoder to get the remoted control commond and 
	show output image. If the user does not call the functions, the decoder
	will be used as for middleware.

Parameters: 
	IN:
		PMPG_FUNCTIONS:
			typedef struct
			{
				int (*GetRemotedCmd)(void);	//Get the key of remote controler, please set NULL
														//if not use it.
				void (*Sleep)(int n);					//Sleep for n milliseconds, please set NULL if 
														//not use it.
				void (*ShowFrame)
					(		//Callback function, the decoder would call it to notice user that
							// an GIF frame is decoded, so that the user can process the frame,
							// show it of other process.
						unsigned char *y_buf,	//the y component of the decoded GIF frame
						unsigned char *cb_buf,  //the cb component of the decoded GIF frame
						unsigned char *cr_buf	//the cr component of the decoded GIF frame
					);
			}MPG_FUNCTIONS, *PMPG_FUNCTIONS;
			the structure includes mpg functions pointers and function parameters' value:

			(1)When the image decoder is decoding an image, if the user wants to pause,
			he can press the key PAUSE of the remote controler, and then press the key
			PLAY (or CONTINUE) to continue decoding the image. The image decoder uses 
			following 2 funcitons to do the job, it gets the key of controler loop when
			decoding an image, if the key is PAUSE, decoder will pause and waitting to 
			continue; if the key is PLAY(or CONTINUE), decoder will continue decoding 
			from former	pause point; if the key is STOP, decoder will exit decoding.
			int (*GetRemotedCmd)(void):
				get the key of remote controler, the key will store to *CtrlCmd.
			void (*Sleep)(int n)
			    sleep n millisecond.
			if the user does not need the PAUSE or other control's function during decoding 
			iamge, set the 2 function to (void*)0. In general, the 2 function pointer must 
			be set in firmware. 
			(2) void (*ShowFrame)() 
				The is a callback function, and it is only valid in GIF decoder, because the
				decoder must show (or other process) circularly when deocding a GIF image, 
				the decoder would call this function to notice the user that an GIF frame
				decoding is done, then the user can process the GIF frame in this callback
				function, showes it or copies it to other buffer, etc.
    OUT: 		
		None
Return: 
	None
------------------------------------------------------*/
void SetMpgFunctions(PMPG_FUNCTIONS mpg_functions);

/*----------------------------------------------------
Name: 
    JPGGetName
Description: 
	interface: This function tells the name information 
			about JPG decoding library.
    
Parameters: 
    IN:

    OUT: 
    
Return: 
    a string of characters contain the information.
------------------------------------------------------*/
char* JPGGetName(void);

/*----------------------------------------------------
Name: 
    JPGGetName
Description: 
	interface: This function tells the version information 
			about JPG decoding library.

Parameters: 
    IN:

    OUT: 
    
Return: 
    an unsigned long WORD contain the information:
	low 16bits represents the revision number and the high 16bits
    represents the version number
------------------------------------------------------*/
unsigned long JPGGetVersion(void);

////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------

	New picture decoding interface

------------------------------------------------------*/

/*----------------------------------------------------
Name: 
    SetTempBufPoint
Description: 
	We must use some temporary memory(buffer) for decoding an image, the function
	specify the buffer that can be used for decoding, the buffer size can be set 
	according to the information that get by PicDec_GetInofo() function. And the
	buffer size must larger than the size get by PicDec_GetInofo(), otherwise it
	can not decode image.

Parameters: 
    IN: 
		buf_point: the temory memory(buffer)
		int size:  the size of temory memory
    OUT: 
    
Return: void
------------------------------------------------------*/
void SetTempBufPoint(unsigned char *buf_point, int size);

/*----------------------------------------------------
Name: 
    SetTVWHRatio(int TV_ratio, int FB_width, int FB_height)
Description: 
	For avoiding the distortion of displaging image on TV, we 
	should set the width/height ratio of TV. In general, the ratio
	is 4:3.

Parameters: 
	IN:
		TV_ratio: width/height ratio of TV.
			0, not set the radio, NO scaling output, the decoded image will keep 
			   original size to output, so also the next two parameters (FB_width 
			   and FB_height) is no use.
			1, width/height is 4:3
			2, width/height is 3:2
			3, width/height is 16:9
		FB_width: the width of output frame buffer, use for get width/height of FB
		FB_height: the height of output frame buffer, use for get width/height of FB
	OUT:
Return: void
------------------------------------------------------*/
void SetTVWHRatio(int TV_ratio, int FB_width, int FB_height);

/*----------------------------------------------------
Name: 
    PicDec_GetInfo
Description: 
	This function will get the information of the image, it will fill the structure picinfo.

Parameters: 
    IN: 
		readfunc: a pointer to the read function
		seekfunc: a pointer to the seek function
		buf: the buffer is for analyst the image type and get the decoded buffer size, 
			 the size at least 150*1024(150k) bytes, it only need when the image format is JPEG. you can
			 free the buffer after calling the function.
		buf_size: the byte size of buffer, at least 150*1024(150K).
		output_width: the image output width, the decoder can calculate the needed buffer size by the parameter.
    OUT: 
		info:the pointer to the picinfo structure.
    
Return: 
		0: failed.
		>0: the necessary buffer size when decoding an image.
------------------------------------------------------*/
//int PicDec_GetInfo((void *)(readfunc(char *buffer, int size)), struct picdec_info *info);
unsigned long PicDec_GetInfo(unsigned long (*readfunc)(unsigned char *buffer, int size), void (*seekfunc)(long offset, int origin), \
				   unsigned char *buf, int buf_size, int output_width, picdec_info *info);


/*----------------------------------------------------
Name: 
    PicDec_Rotate
Description: 
	This function will rotate the certain image by given angle.

Parameters: 
    IN: angle: only 90, 180, 270 is supported.
		mode: one of the PICDEC_OUTPUT_MODE macros. So far, the image library only support YUV411 output
		srcy: if mode is YUV, means the source of y, else means the base address of RGB.
		srccb: if mode is YUV, the source of the cb, else it's ignored.
		srccr: if mode is YUV, the source of the cr, else it's ignored.
		desty: if mode is YUV, the destination of the y, else means the destination address of RGB.
		destcb: if mode is YUV, the destination of the cb, else it's ignored.
		destcr: if mode is YUV, the destination of the cr, else it's ignored.
    OUT: 
    
Return: 
		0: success.
		-1: failed.
------------------------------------------------------*/
int PicDec_Rotate(int angle, PICDEC_OUTPUT_MODE mode, unsigned char *srcy, unsigned char *srccb, unsigned char *srccr, \
				  unsigned char *desty, unsigned char *destcb, unsigned char *destcr);


/*----------------------------------------------------

Name: 
    ImageDecode()
Description: 
	This function will call responding image decoder.

Parameters: 
	IN:
		readfunc: a pointer to the read function
		seekfunc: a pointer to the seek function, only used by PROGRESSIVE_JPEG image type
		outputmode: one of the PICDEC_OUTPUT_MODE macros.
		TVmode:		the TV mode, 0: PAL, 1: NTSC
		desty: if mode is YUV, means the destination of the y, else means the base address of RGB.
		destcb: if mode is YUV, means the destination of the cb, else it's ignored.
		destcr: if mode is YUV, means the destination of the cr, else it's ignored.
		xsize: the destination output size of x. If it is different from the actual image x size, scaling is done
		ysize: the destination output size of y. If it is different from the actual image y size, scaling is done
		stride: the output stride in PIXELS.
    OUT: 
    
Return: 
		0: success.
		-1: failed.
------------------------------------------------------*/
int ImageDecode(unsigned long (*readfunc)(unsigned char *buffer, int size), void (*seekfunc)(long offset, int origin), PICDEC_OUTPUT_MODE mode, \
			  int TVmode, unsigned char *desty, unsigned char *destcb, unsigned char *destcr, int xsize, int ysize, int stride);


/*----------------------------------------------------
Name: 
    JPGDecode()
Description: 
	This function will decode SEQUENTIAL_JPEG and PROGRESSIVE_JPEG image types

Parameters: 
	IN:
		readfunc: a pointer to the read function
		seekfunc: a pointer to the seek function, only used by PROGRESSIVE_JPEG image type
		outputmode: one of the PICDEC_OUTPUT_MODE macros. So far we only support YUV format output
		TVmode:		the TV mode, 0: PAL, 1: NTSC
		desty: if mode is YUV, means the destination of the y, else means the base address of RGB.
		destcb: if mode is YUV, means the destination of the cb, else it's ignored.
		destcr: if mode is YUV, means the destination of the cr, else it's ignored.
		xsize: the destination output size of x. If it is different from the actual image x size, scaling is done
		ysize: the destination output size of y. If it is different from the actual image y size, scaling is done
		stride: the output stride in PIXELS.
    OUT: 
    
Return: 
		0: success.
		-1: failed.
------------------------------------------------------*/
//int JPGDecode((void *)(readfunc(char *buffer, int size)), (void *)(seekfunc(int offset)), int mode, char *desty, char *destcb, char *destcr, int xsize, int ysize, int stride);
int JPGDecode(unsigned long (*readfunc)(unsigned char *buffer, int size), void (*seekfunc)(long offset, int origin), PICDEC_OUTPUT_MODE mode, \
			  int TVmode, unsigned char *desty, unsigned char *destcb, unsigned char *destcr, int xsize, int ysize, int stride);

/*----------------------------------------------------
Name: 
    BMPDecode()
Description: 
	This function will decode BMP image types

Parameters: 
	IN:
		readfunc: a pointer to the read function
		seekfunc: a pointer to the seek function, only used by PROGRESSIVE_JPEG image type
		mode: one of the PICDEC_OUTPUT_MODE macros.
		TVmode:		the TV mode, 0: PAL, 1: NTSC
		desty: if mode is YUV, means the destination of the y, else means the base address of RGB.
		destcb: if mode is YUV, means the destination of the cb, else it's ignored.
		destcr: if mode is YUV, means the destination of the cr, else it's ignored.
		xsize: the destination output size of x. If it is different from the actual image x size, scaling is done
		ysize: the destination output size of y. If it is different from the actual image y size, scaling is done
		stride: the output stride in PIXELS.
    OUT: 
    
Return: 
		0: success.
		-1: failed.
------------------------------------------------------*/
int BMPDecode(unsigned long (*readfunc)(unsigned char *buffer, int size), void (*seekfunc)(long offset, int origin), PICDEC_OUTPUT_MODE mode, \
			  int TVmode, unsigned char *desty, unsigned char *destcb, unsigned char *destcr, int xsize, int ysize, int stride);

/*----------------------------------------------------
Name: 
    PNGDecode()
Description: 
	This function will decode PNG image types

Parameters: 
	IN:
		readfunc: a pointer to the read function
		seekfunc: a pointer to the seek function, only used by PROGRESSIVE_JPEG image type
		mode: one of the PICDEC_OUTPUT_MODE macros.
		TVmode:		the TV mode, 0: PAL, 1: NTSC
		desty: if mode is YUV, means the destination of the y, else means the base address of RGB.
		destcb: if mode is YUV, means the destination of the cb, else it's ignored.
		destcr: if mode is YUV, means the destination of the cr, else it's ignored.
		xsize: the destination output size of x. If it is different from the actual image x size, scaling is done
		ysize: the destination output size of y. If it is different from the actual image y size, scaling is done
		stride: the output stride in PIXELS.
    OUT: 
    
Return: 
		0: success.
		-1: failed.
------------------------------------------------------*/
//int PNGDecode((void *)(readfunc(char *buffer, int size)), int mode, char *desty, char *destcb, char *destcr, int frame, int xsize, int ysize, int stride);
int PNGDecode(unsigned long (*readfunc)(unsigned char *buffer, int size), void (*seekfunc)(long offset, int origin), PICDEC_OUTPUT_MODE mode, \
			  int TVmode, unsigned char *desty, unsigned char *destcb, unsigned char *destcr, int xsize, int ysize, int stride);



/*----------------------------------------------------
Name: 
    SetGifOutputBuf()
Description: 
	Because the decoder must show the GIF animation circularly, we want to show 
	the frame smoothly, so we can show the current frame in one buffer,	and decode 
	next frame to the other buffer, then show it. Then the two buffer can be exchanged
	for decoding or showing, the animation looks smooth. 
	The function set the output decoded buffer for the next frame of GIF, then the
	decoder would decode the next frame the these buffer, so we can change the output
	buffer by this function.
	The function should be called when the decoder call the show iamge function to show
	or process the frame, so that the next frame will decode to the other buffer.

	The function mainly use for firmware.
Parameters: 
	IN:
		y_buf: the buffer for decoded y component
		cb_buf: the buffer for decoded cr component
		cr_buf: the buffer for decoded cb component
    OUT: 
    
Return: 
	None
------------------------------------------------------*/
void SetGifOutputBuf(unsigned char *y_buf, unsigned char *cb_buf, unsigned char *cr_buf);


/*----------------------------------------------------
Name: 
    GIFDecode()
Description: 
	This function will decode GIF image types

Parameters: 
	IN:
		readfunc: a pointer to the read function
		seekfunc: a pointer to the seek function
		mode: one of the PICDEC_OUTPUT_MODE macros.
		TVmode:		the TV mode, 0: PAL, 1: NTSC
		desty: if mode is YUV, means the destination of the y, else means the base address of RGB.
		destcb: if mode is YUV, means the destination of the cb, else it's ignored.
		destcr: if mode is YUV, means the destination of the cr, else it's ignored.
		xsize: the destination output size of x. If it is different from the actual image x size, scaling is done
		ysize: the destination output size of y. If it is different from the actual image y size, scaling is done
		stride: the output stride in PIXELS.
    OUT: 
    
Return: 
		0: success.
		-1: failed.
------------------------------------------------------*/
int GIFDecode(unsigned long (*readfunc)(unsigned char *buffer, int size), void (*seekfunc)(long offset, int origin), PICDEC_OUTPUT_MODE mode, \
			  int TVmode, unsigned char *desty, unsigned char *destcb, unsigned char *destcr, int xsize, int ysize, int stride);


/*----------------------------------------------------
Name: 
    TIFFDecode()
Description: 
	This function will decode Tiff image types

Parameters: 
	IN:
		readfunc: a pointer to the read function
		seekfunc: a pointer to the seek function
		mode: one of the PICDEC_OUTPUT_MODE macros.
		TVmode:		the TV mode, 0: PAL, 1: NTSC
		desty: if mode is YUV, means the destination of the y, else means the base address of RGB.
		destcb: if mode is YUV, means the destination of the cb, else it's ignored.
		destcr: if mode is YUV, means the destination of the cr, else it's ignored.
		xsize: the destination output size of x. If it is different from the actual image x size, scaling is done
		ysize: the destination output size of y. If it is different from the actual image y size, scaling is done
		stride: the output stride in PIXELS.
    OUT: 
    
Return: 
		0: success.
		-1: failed.
------------------------------------------------------*/
int TIFFDecode(unsigned long (*readfunc)(unsigned char *buffer, int size), void (*seekfunc)(long offset, int origin), PICDEC_OUTPUT_MODE mode, \
			   int TVmode, unsigned char *desty, unsigned char *destcb, unsigned char *destcr, int xsize, int ysize, int stride);

#endif