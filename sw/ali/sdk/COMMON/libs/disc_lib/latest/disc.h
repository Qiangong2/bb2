/*--------------------------------------------------------------------------------
    
    Copyright (C) 2003 T2-Design Corp.  All rights reserved.
    
    Content: 
		this file document the interface provide by DISC library. The DISC library is
	based on T2 rtos and BIOS library,and Log library.It also assume there have C Run 
	Time Library.
    
    History: 
    2003/11/05	by Kenneth Wang.
----------------------------------------------------------------------------------*/    
#ifndef _T2_DISC_H

typedef struct _MSF
{
	BYTE	mm;
	BYTE	ss;
	BYTE	ff;
}MSF;

typedef enum {TRK_CDDA,TRK_DATA} TRK_TYPE;

typedef struct _DISC_TRACKINFO
{
	TRK_TYPE	TrkType;
	MSF			StartAddr;
	MSF			EndAddr;	
}DISC_TRACKINFO;	

#define MAX_NUM_TRACKS	100

typedef struct _DISC_TRACK_TABLE
{
	BYTE TotalTrack;
	DISC_TRACKINFO TrkDscp[MAX_NUM_TRACKS];	
}DISC_TRACK_TABLE,*PDISC_TRACK_TABLE;

#define LOADER_TRAY_OPEN	0
#define LOADER_TRAY_CLOSE	1
#define LOADER_TRAY_OPENING	2
#define LOADER_TRAY_CLOSING	3
#define LOADER_TRAY_LOADING	4
#define LOADER_TRAY_POWERON	5
#define LOADER_ERROR		6

#define C_LBA				1
#define C_MSF				2

void Disc_Init(ID SemdID,BYTE *DriverBuffer,int DriverBufferSize);

void Disc_Start(void);

void Disc_Stop(void);

void Disc_Tray_Open(void);

void Disc_Tray_Close(void);

int	 Disc_GetStatus(void);

PDISC_TRACK_TABLE Disc_GetToc(void);

int Disc_ReadSector(char *buffer, int Startsector, int numsectors, int type,int sectorsize);

#endif
