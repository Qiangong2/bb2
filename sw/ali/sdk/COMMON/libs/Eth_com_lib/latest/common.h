#ifndef _COMMON_H
#define _COMMON_H

typedef unsigned char  u8;
typedef unsigned short u16;
typedef unsigned long  u32;
typedef signed long    s32;
typedef signed short   s16;
typedef signed char    s8;

typedef u32 dma_addr_t;

#ifndef NULL
# define NULL 0
#endif

#ifndef HZ
# define HZ 100
#endif

#define virt_to_bus(addr)       ((unsigned long) addr & 0x0fffffff)

typedef struct {
	volatile unsigned int lock;
} spinlock_t;

#if 0
/* for I/O input and output functions */
#define inw(port) __inw(port)
#define outw(val, port) __outw(val, port)		\
do {							\
	*(volatile u16 *)((port)) = (val);		\
} while(0)
#endif

/* for IO read and write macros */
#define inw(port) (u16)(*(volatile u16 *)(port))
#define outw(val, port) ((*(volatile u16 *)(port)) = (val))

#define insw(port, addr, count) __insw(port, addr, count)
#define outsw(port, addr, count) __outsw(port, addr, count)

/* for memory read and write macros */
#define readb(addr)             (unsigned char)(*(volatile unsigned char *)(addr))
#define readw(addr)             (unsigned short)(*(volatile unsigned short *)(addr))
#define readl(addr)             (unsigned int)(*(volatile unsigned long *)(addr))

#define writeb(b,addr)          ((*(volatile unsigned char *)(addr)) = (b))
#define writew(w,addr)          ((*(volatile unsigned short *)(addr)) = (w))
#define writel(l,addr)          ((*(volatile unsigned long *)(addr)) = (l))

static inline unsigned short __inw(unsigned long port)
{
	return *(volatile u16 *)(port);
}

static inline void __outw(unsigned short val, unsigned long port)
{
	*(volatile u16 *)((port)) = (val);
}

static inline void __insw(unsigned long port, void *addr, unsigned int count)
{
	while (count--) {
		*(u16 *)addr = inw(port);
		addr += 2;
	}
}

static inline void __outsw(unsigned long port, const void *addr, unsigned int count)
{
	while (count--) {
		outw(*(u16 *)addr, port);
		addr += 2;
	}
}

#define le16_to_cpu(x)  ((unsigned short)(x))
#define le32_to_cpu(x)  ((unsigned long)(x))
#define cpu_to_le16(x)  ((unsigned short)(x))
#define cpu_to_le32(x)  ((unsigned long)(x))

#define GET_C0_COUNT(val) asm volatile("mfc0 %0, $9\n"		\
						"nop"		\
						:"=r"(val))

#define WRITE_C0_COMP(val) asm volatile("mtc0 %0, $11\n"	\
						"nop"		\
						: :"r"(val))

#define CPU_FRE (200 * 1000 * 1000)
#define ONEms   (CPU_FRE / (2 * 1000))
#define ONEus   (CPU_FRE / (2 * 1000 * 1000))

static inline void mdelay(volatile unsigned int ms)  // delay n ms
{
	volatile unsigned int t0 = 0, t1 = 0;
	volatile unsigned long temp = 0;
	
	if(!ms) return;

	temp=ONEms*ms;
	GET_C0_COUNT(t0);
	
	do{
		GET_C0_COUNT(t1);
	}while((t1 - t0) < temp);
}

static inline void udelay(volatile unsigned int us)  // delay n us
{
	volatile unsigned int t0 = 0, t1 = 0;
	volatile unsigned long temp = 0;
	
	if(!us) return;

	temp = ONEus * us;
	GET_C0_COUNT(t0);

	do{
		GET_C0_COUNT(t1);
	}while((t1 - t0) < temp);
}

#define spin_lock_irqsave(lock, flags)	do { local_irq_save(flags);	spin_lock(lock);} while (0)
#define spin_lock_irq(lock)		do { local_irq_disable();	spin_lock(lock);} while (0)
#define spin_unlock_irqrestore(lock, flags)do { spin_unlock(lock);	local_irq_restore(flags);} while (0)
#define spin_unlock_irq(lock)		do { spin_unlock(lock);		local_irq_enable();} while (0)

#define spin_lock(lock)            (void)(lock) /* Not "unused variable". */
#define spin_unlock(lock)          do { } while(0)
#define spin_lock_init(lock)    do { } while(0)

/* For spinlocks etc */
#define local_irq_save(x)       __save_and_cli(x)
#define local_irq_restore(x)    __restore_flags(x)
#define local_irq_disable()     __cli()
#define local_irq_enable()      __sti()

__asm__ (
	".macro\t__save_and_cli result\n\t"
	".set\tpush\n\t"
	".set\treorder\n\t"
	".set\tnoat\n\t"
	"mfc0\t\\result, $12\n\t"
	"ori\t$1, \\result, 1\n\t"
	"xori\t$1, 1\n\t"
	".set\tnoreorder\n\t"
	"mtc0\t$1, $12\n\t"
	"sll\t$0, $0, 1\t\t\t# nop\n\t"
	"sll\t$0, $0, 1\t\t\t# nop\n\t"
	"sll\t$0, $0, 1\t\t\t# nop\n\t"
	".set\tpop\n\t"
	".endm");

#define __save_and_cli(x)                                               \
__asm__ __volatile__(                                                   \
	"__save_and_cli\t%0"                                            \
	: "=r" (x)                                                      \
	: /* no inputs */                                               \
	: "memory")

__asm__(".macro\t__restore_flags flags\n\t"
	".set\tnoreorder\n\t"
	".set\tnoat\n\t"
	"mfc0\t$1, $12\n\t"
	"andi\t\\flags, 1\n\t"
	"ori\t$1, 1\n\t"
	"xori\t$1, 1\n\t"
	"or\t\\flags, $1\n\t"
	"mtc0\t\\flags, $12\n\t"
	"sll\t$0, $0, 1\t\t\t# nop\n\t"
	"sll\t$0, $0, 1\t\t\t# nop\n\t"
	"sll\t$0, $0, 1\t\t\t# nop\n\t"
	".set\tat\n\t"
	".set\treorder\n\t"
	".endm");

#define __restore_flags(flags)                                          \
do {                                                                    \
	unsigned long __tmp1;                                           \
	                                                                \
	__asm__ __volatile__(                                           \
		"__restore_flags\t%0"                                   \
		: "=r" (__tmp1)                                         \
		: "0" (flags)                                           \
		: "memory");                                            \
} while(0)

__asm__ (
	".macro\t__sti\n\t"
	".set\tpush\n\t"
	".set\treorder\n\t"
	".set\tnoat\n\t"
	"mfc0\t$1,$12\n\t"
	"ori\t$1,0x1f\n\t"
	"xori\t$1,0x1e\n\t"
	"mtc0\t$1,$12\n\t"
	".set\tpop\n\t"
	".endm");

extern __inline__ void
__sti(void)
{
	__asm__ __volatile__(
		"__sti"
		: /* no outputs */
		: /* no inputs */
		: "memory");
}

/*
 * For cli() we have to insert nops to make sure that the new value
 * has actually arrived in the status register before the end of this
 * macro.
 * R4000/R4400 need three nops, the R4600 two nops and the R10000 needs
 * no nops at all.
 */
__asm__ (
	".macro\t__cli\n\t"
	".set\tpush\n\t"
	".set\tnoat\n\t"
	"mfc0\t$1,$12\n\t"
	"ori\t$1,1\n\t"
	"xori\t$1,1\n\t"
	".set\tnoreorder\n\t"
	"mtc0\t$1,$12\n\t"
	"sll\t$0, $0, 1\t\t\t# nop\n\t"
	"sll\t$0, $0, 1\t\t\t# nop\n\t"
	"sll\t$0, $0, 1\t\t\t# nop\n\t"
	".set\tpop\n\t"
	".endm");

extern __inline__ void
__cli(void)
{
	__asm__ __volatile__(
		"__cli"
		: /* no outputs */
		: /* no inputs */
		: "memory");
}
#undef min
#undef max
#define min(x,y) (((x)<(y)) ? (x) : (y))
#define max(x,y) (((x)>(y)) ? (x) : (y))

#undef min_t
#undef max_t
#define min_t(type,x,y) \
	({ type __x = (x), __y = (y); __x < __y ? __x: __y; })
#define max_t(type,x,y) \
	({ type __x = (x), __y = (y); __x > __y ? __x: __y; })

#define swab16(x) \
({ \
	((unsigned short)( \
		(((unsigned short)(x) & (unsigned short)0x00ffU) << 8) | \
		(((unsigned short)(x) & (unsigned short)0xff00U) >> 8) )); \
})

#define swab32(x) \
({ \
	((unsigned long)( \
		(((unsigned long)(x) & (unsigned long)0x000000ffUL) << 24) | \
		(((unsigned long)(x) & (unsigned long)0x0000ff00UL) <<  8) | \
		(((unsigned long)(x) & (unsigned long)0x00ff0000UL) >>  8) | \
		(((unsigned long)(x) & (unsigned long)0xff000000UL) >> 24) )); \
})

#define cpu_to_be32(x)  swab32((x))
#define cpu_to_be16(x)  swab16((x))
#define be32_to_cpu(x)  swab32((x))
#define be16_to_cpu(x)  swab16((x))

#define htonl(x) cpu_to_be32(x)
#define htons(x) cpu_to_be16(x)
#define ntohl(x) be32_to_cpu(x)
#define ntohs(x) be16_to_cpu(x)

#endif /* _COMMON_H */

