/*
 * Copyright (c) 2003, T-Square ZH.
 * All rights reserved.
 * 
 * This file declares a middle layer between lwIP stack and
 * LAN/WLAN network card layer.
 * 
 * 2004-03-17
 *
 */

#ifndef	__IP_ETH_H__ 

#define __IP_ETH_H__ 

#include "pci_init.h"

/*the eth mac addr structure. */
struct eth_addr {
	unsigned char addr[6];
};

/* the structure for card register info. */
struct pci_card_info {
	unsigned int v_id;	/* the vendor ID of the PCI network card. */
	unsigned int d_id;	/* the device ID of the PCI network card. */
	int card_type;		/* the exclusive type ID of the card. define in file net_dri_api.c. */
	int net_type;		/* the net type, define in file net_dri_api.c. */
	struct netcard_com_driver *pcard_com_drv;	/* the card driver interface functions map table. */
	const char *desc;	/* the description of the card. */
};

/* the net type your card supported. */
enum net_type {
	LAN_NET = 0,
	WLAN_NET,
	UNKNOWN_NET
};

/* PCI slot type define. */
enum pci_slot_type {
	NORMAL_SLOT = 0,
	MINI_SLOT,
	USB_SLOT,
	UNKNOWN_SLOT
};

/* the structure for return from the card scanning function, contains all the available card infos. */
struct net_card_list {
	int nettype;
	int slottype;
	char desc[32];
	void *private;		/* usually the pointer to corresponding pci_slot structure. */
};

#if 0
/* for common configure structure */
struct common_conf {
	unsigned char card_type;
	unsigned char main_ver;
	unsigned char slave_ver;
	void *priv_conf;
};
#endif

/* the link status of the current card. For LAN card we just concern the 'linkSpeed' part.*/
struct net_link_status{ 
	unsigned char linkSpeed;		// link speed in 500Kbps units 
	unsigned char curBSSID[6];		// current BSSID, 6 octets IEEE MAC address. 
	unsigned char curNetworkMode;		// Current Network Mode, 1- Infrastructure or 2 - AdHoc 
	char curSSID[33];			// Current SSID, it's null terminated string 
	unsigned short curChannel;		// Current Channel, Radio channel number. 
	unsigned char curRssi;			// current BSS RSSI indication in dBm. 
}; 

/* for WLAN card network mode. */
enum network_mode {
	DOT11_BSSTYPE_NONE = 0,
	DOT11_BSSTYPE_INFRA,	// infrastructure mode, default using
	DOT11_BSSTYPE_IBSS,	// Ad-hoc mode
	DOT11_BSSTYPE_ANY	// any available BSS
};

/* for WLAN card RF profile. */
enum rf_profile {
	DOT11_PROFILE_B_ONLY = 0,
	DOT11_PROFILE_MIXED,	//default using
	DOT11_PROFILE_MIXED_LONG,
	DOT11_PROFILE_G_ONLY,
	DOT11_PROFILE_TEST,
	DOT11_PROFILE_B_WIFI,
	DOT11_PROFILE_A_ONLY,
	DOT11_PROFILE_MIXED_SHORT
};

/* for WLAN card authenticate algorithm. */
enum authen_alg {
	DOT11_AUTH_NONE = 0,
	DOT11_AUTH_OS,		// open system, default using
	DOT11_AUTH_SK,		// shared keys
	DOT11_AUTH_BOTH		// both above two
};

/* for the every configure parameter. */
enum cnfg_para {
	CNFG_NET_MODE = 1,
	CNFG_CHANNEL,
	CNFG_SSID,
	CNFG_PROFILE,
	CNFG_FRAG,
	CNFG_RTS,
	CNFG_AUTH_ALG,
	CNFG_SECU_MODE,
	CNFG_DEF_INDEX,
	CNFG_WEP1,
	CNFG_WEP2,
	CNFG_WEP3,
	CNFG_WEP4,
	CNFG_WPA_PSK
};

/* for WLAN card security mode. */
enum secur_mode {
	PRIVACY_DISABLE = 0,	// default using
	WEP_64,
	WEP_128,
	WPA_PSK
};

/* for WLAN BSS */
struct wlan_obj_bss
{
	u8 BSSID[6];		// BSSID
	u8 networkMode;		// Infrastructure or AdHoc
	char SSID[33];		// null terminated SSID
	u8 securityMode;	// WEP status
	u8 rssi;		// BSS RSSI indication
	u16 channel;		// Radio channel in number
	u16 rates;		// bitmap of supported rates
	u16 basic_rates;	// bitmap of basic rates
};

struct wlan_obj_bsslist
{
	u32 nr;				// number of BSS in the array bsslist
	struct wlan_obj_bss bsslist[1];	// array of BSSs
};

/* the register functions(API) for lower card driver. */
struct netcard_com_driver {

	/* first for the all the LAN and WLAN card common API functions */
	int (*net_card_configure) (unsigned long *, void (*cnfg_clbk) (unsigned long));
	int (*net_setting_load) (void *);
	int (*net_card_init) (pci_dev_t *);
	int (*net_card_open) (void);

	/* add a card device task to handle some special work */	
	void (*net_card_task) (void);
	
	struct eth_addr *(*net_get_card_mac) (void);
	int (*net_get_link_status) (int (*link_clbk) (struct net_link_status *));
	int (*net_disable_multicast) (void);
	int (*net_enable_multicast) (void);
	int (*net_card_stop) (void);
	void (*net_interrupt_isr) (unsigned int (*do_fetch_data) (unsigned char *, unsigned int));
	int (*net_packet_send) (unsigned char *, unsigned int);

	/* special for WLAN API functions */
	int (*wlan_set_network_mode) (int);
	int (*wlan_get_network_mode) (void);
	int (*wlan_set_channel) (int);
	int (*wlan_get_channel) (void);
	int (*wlan_set_ssid) (char *);
	int (*wlan_get_ssid) (char *);
	int (*wlan_set_rf_profile) (int);
	int (*wlan_get_rf_profile) (void);
	int (*wlan_set_fragmentation) (int);
	int (*wlan_get_fragmentation) (void);
	int (*wlan_set_rts) (int);
	int (*wlan_get_rts) (void);
	int (*wlan_set_auth_alg) (int);
	int (*wlan_get_auth_alg) (void);
	int (*wlan_set_sec_mode) (int);
	int (*wlan_get_sec_mode) (void);
	int (*wlan_set_def_wep_index) (int);
	int (*wlan_get_def_wep_index) (void);
	int (*wlan_set_wep_keys) (int, unsigned char *, int);
	int (*wlan_get_wep_keys) (int, unsigned char *,int *);
	int (*wlan_set_wpa_psk) (char *);
	int (*wlan_get_wpa_psk) (char *);
	int (*wlan_trigger_site_survey) (void);
	int (*wlan_get_site_survey_result) (struct wlan_obj_bsslist *, unsigned int);
};

/* the extern interface functions(API) for the upper layers, eg: OS, UI etc. */
extern struct net_card_list *net_get_card(int);
extern int net_set_card(int);
extern int net_get_cur_card(void);
extern int net_setting_load(void *);
extern int net_card_configure(unsigned long *err_oid, void (*cnfg_clbk) (unsigned long));
extern int net_card_init(void);
extern int net_card_open(void);
extern void net_card_task(void);
extern struct eth_addr *net_get_card_mac(void);
extern int net_get_link_status (int (*link_clbk) (struct net_link_status *));
extern int net_disable_multicast(void);
extern int net_enable_multicast(void);
extern int net_card_stop(void);
extern void net_interrupt_isr(void);
extern int net_packet_send(unsigned char *, unsigned int);
extern int net_packet_recv_callback(unsigned int (*lwip_rec_fun)(unsigned char *, unsigned int));
extern int wlan_set_network_mode(int);
extern int wlan_get_network_mode(void);
extern int wlan_set_channel(int);
extern int wlan_get_channel(void);
extern int wlan_set_ssid(char *);
extern int wlan_get_ssid(char *);
extern int wlan_set_rf_profile(int);
extern int wlan_get_rf_profile(void);
extern int wlan_set_fragmentation(int);
extern int wlan_get_fragmentation(void);
extern int wlan_set_rts(int);
extern int wlan_get_rts(void);
extern int wlan_set_auth_alg(int);
extern int wlan_get_auth_alg(void);
extern int wlan_set_sec_mode(int);
extern int wlan_get_sec_mode(void);
extern int wlan_set_def_wep_index(int);
extern int wlan_get_def_wep_index(void);
extern int wlan_set_wep_keys(int, unsigned char *, int);
extern int wlan_get_wep_keys(int, unsigned char *, int *);
extern int wlan_set_wpa_psk(char *);
extern int wlan_get_wpa_psk(char *);
extern int wlan_trigger_site_survey(void);
extern int wlan_get_site_survey_result(struct wlan_obj_bsslist *, unsigned int);

#endif	/* __IP_ETH_H__ */

