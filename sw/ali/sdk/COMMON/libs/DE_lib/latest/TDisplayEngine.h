/***************************************************************************
	T630x Display Engine Library
	T-Square

This library is used to drive the Display Engine within the T630x.
It provides a minimum abstraction over the hardware: its intent is just
to let application control the display engine without being forced
to access the hardware registers directly.

 ***************************************************************************/

#ifndef __DISPLAYENGINELIB_H
#define __DISPLAYENGINELIB_H

#ifdef __cplusplus
extern "C" {
#endif

/********************************************************************
	Macros
 ********************************************************************/

enum TDE_CHIPTYPE
{
	DE_ID_6303 = 0x6303,
	DE_ID_6304 = 0x6304
};

enum TDE_TVMODES
{
	DE_TV_PAL = 0,
	DE_TV_NTSC = 1
};

enum TDE_VIDEOOUT_MODES
{
	DE_VOM_SVideo = 0, 
	DE_VOM_CVBS   = 1, 
	DE_VOM_RGB    = 2,
	DE_VOM_YUV    = 3
};

enum TDE_VIDEOSCAN_MODES
{
	DE_VSM_Interlace   = 0,
	DE_VSM_Progressive = 1, 
};

enum TDE_PICTUREMODE
{
	DE_PM_RGB  = 0,
	DE_PM_VCD  = 1,

	// 6304 ONLY picture modes
	DE_PM_SVCD = 2,
	DE_PM_DVD  = 3,
	DE_PM_DIVXVCD  = 5,
	DE_PM_DIVXSVCD = 6

	// Others are set as DVD Mode
};


enum TDE_PIXELFORMATS
{
	DE_PF_ARGB1555 = 0,
	DE_PF_ABGR1555 = 1,
	DE_PF_RGB565   = 2,
	DE_PF_BGR565   = 3,
	DE_PF_RGB888   = 4,
	DE_PF_BGR888   = 5,
	DE_PF_ARGB8888  = 6,
	DE_PF_ABGR8888  = 7,
	DE_PF_YUV       = 8 // Only for T6304
};

enum TDE_FETCHMODE
{
	DE_FM_FIELDBASED = 0,
	DE_FM_FRAMEBASED = 1
};

enum TDE_FILTERMODE
{
	DE_FM_CALC = 0,
	DE_FM_DUP  = 1
};

enum TDE_OSDPIXELFORMATS
{
	DE_OPF_2BIT = 0,
	DE_OPF_4BIT = 1,
	DE_OPF_8BIT   = 2
};

enum TDE_SPPIXELFORMATS
{
	DE_SPPF_2BIT = 0,
	DE_SPPF_8BIT = 1
};

enum TDE_ANTIFLICKERMODE
{
	DE_ATM_LOW = 0,
	DE_ATM_MID = 1,
	DE_ATM_HIGH = 2
};

enum TDE_MACROVISIONTYPES
{
    DE_MVT_TYPE0 = 0, /* disable macrovision */
    DE_MVT_TYPE1 = 1,
    DE_MVT_TYPE2 = 2,
    DE_MVT_TYPE3 = 3
};

// Display engine callback type
typedef void (*DE_CB)(void);

/********************************************************************
Display Engine Functions
********************************************************************/

/********************************************************************
 TDE_Reset - Reset the display engine and the library
 
 Supports: T630X
 
 Notes:
	After TDE_Reset(), the TV mode of the Display Engine is NTSC, the TV
	encoder is set to NTSC too. it is configured ready for playing DVD 
	in NTSC mode.
 ********************************************************************/
void TDE_Reset(void);

/*****************************************************************************
 TDE_HWReset - Reset the display engine but not reset the TV encoder
 
 Supports: T630X
 Params: 
    tvMode           - Requested TV mode (use the DE_TV_* macros)
    
 Notes:
	After TDE_HWReset(), the TV mode of the Display Engine is NTSC.
	it is configured ready for playing DVD in NTSC mode. Before changing the 
	pixel format for RGB mode to YUV mode, you'd better call this function.
 *****************************************************************************/
void TDE_HWReset(void);

/*******************************************************************************
 TDE_SetTVMode - Change the current TV mode
 
 Supports: T630x

 Params: 
    tvMode           - Requested TV mode (use the DE_TV_* macros)

 Returns:
    0 if ok, 1 if TV mode is not correct

  Notes:
	In TDE_SetTVMode(), the TV encoder is set to NTSC or PAL according to 
	the display engine. 
	
	If the current vertical size is less the standard size of the TV mode, 
	it should not be changed. Otherwise it will be change to the standard 
	size. The current vertical size is recorded by a gobal named curVSize.

*******************************************************************************/
char TDE_SetTVMode(int tvMode);


/********************************************************************
 TDE_SetPcitureMode - Change the picture mode

 Supports: T630x

 Params:
    pictureMode      - Picture mode (use the DE_PM_* macros. Others should be
					   set as DVD mode)

 Returns:
    0 if ok, 1 if picture mode is not supported
********************************************************************/
char TDE_SetPictureMode(int pictureMode);

/********************************************************************
 TDE_SetFilterMode- Set the filter mode for T6304

 Supports: T6304

 Params:
    fetchMode        - DMA fetch mode. 0: field based; 1: frame based
	filterMode       - filter calculate mode. 0: calculate; 1: duplicate
	fyBypass         - filter Y bypass control. 0: not bypass; 1: bypass
 
 Returns:
    NULL
 Note:  
    For VCD, the filter should be always working and it should be frame based. 
	For RGB, the filter should be always frame based, too. 
	For DVD, normally the filter should be frame based (for example, "Weave").
	But if good visual effect can not be gotten, users can switch to field based
	(for example, "Bob"). If the filter is in duplicate mode, pixels will be 
	repeated or copied. There is no filter effect in vertical direction. In 
	horizontal direction, although filter is working, only one group coefficients 
	are always used . 
	
********************************************************************/
void TDE_SetFilterMode(int fetchMode, int filterMode, int fyBypass);

/******************************************************************************
 TDE_SetGammaCorrection - Adjust the brightness of the display by set the Gamma
							factor 

 Supports: T6304
 
 Params:
   gammaFactor      - Gamma factor grade, which is from 0 to 11 

 Returns:
   0 ok, 1 if gammaFactor is not supported.
      
 Notes:
   The transfer function of most displays produces an intensity that is 
   proportional to some power (referred to as gamma) of the signal amplitude.
   As a result, high-intensity ranges are expanded and low-intensity ranges are
   compressed. By ��gamma correcting�� the video signals before display, the 
   intensity output of the display is roughly linear. You can use this function 
   to set gamma factor. 
   0 - No gamma correction
   1 - 11, gamma factor becomes bigger and bigger, and the brightness becomes 
   lower and lower.
  ******************************************************************************/
char TDE_SetGammaCorrection(unsigned int gammaFactor);

/********************************************************************
 TDE_SetOutputResolution - Change the output resolution

 Supports: T630x
 
 Params:
   width            - output width 
   height           - output height

 Returns:
   0 ok, 1 if width is not supported,  2 if height is not supported.
   3 if the width or height is not even.
      
 Notes:
   In T6303, the output resolution must always be no less than the
   framebuffer resolution. In YUV mode, the output resolution shall 
   be the same as framebuffer resolution. And only twice scaling up 
   is supported in both horizonatal and vertical direction.
   In T6304, TDE_UpdateSize() should be called at least once after this
   function is called.
 ********************************************************************/
char TDE_SetOutputResolution(unsigned int width, unsigned int height);

/********************************************************************
 TDE_SetAxis - Change the offset on the screen

 Supports: T630x
 
 Params:
   offsetX            - X offset on the screen
   offsetY            - Y offset on the screen

 Returns:
   0 ok, 1 if X offset plus output width extends TV width,  2 if Y offset plus 
   output height extends TV height, 3 if Y offset is not even in T6304
 
 Notes: 
     In M6303, the offsetY should be no less than 1. In M6304, the offsetY 
	 should be even, otherwise the odd field and even field will be flipped. 
	 You had better call this function after calling TDE_SetOutputResolution().
	 In T6304, TDE_UpdateSize() should be called at least once after this
	 funciton is called.
 **************************************************************************/
char TDE_SetAxis(unsigned int offsetX, unsigned int offsetY);


/**************************************************************************
 TDE_SetFrameBufferResolution - Change the frame buffer resolution

 Supports: T630x

 Params:
    width            - Frame buffer width 
    height           - Frame buffer height
    rgbPixelFormat   - RGB Pixel format (use the DE_PF_* macros)

 Returns:
    0 ok, 1 if width is not supported,  2 if height is not supported.

 Notes:
    The rgbPixelFormat parameter is valid only when slecting DE_PM_RGB.
	In other picture mode, the value is ignored.
	In T6303, the output resolution must always be bigger than the
	framebuffer resolution. In YUV mode, only 2 frame buffer resolution 
	is supported 352*288(PAL)/352*240(NTSC).
	In T6304, TDE_UpdateSize() should be called at least once after this
	setting after this function is called.
 ********************************************************************/
char TDE_SetFrameBufferResolution(unsigned int width, unsigned int height, int rgbPixelFormat);


/********************************************************************
 TDE_SetFrameBufferStride - Sets a new frame buffer stride

 Supports: T630x

 Params:
    byteStride       - Frame buffer strided (in bytes)

 Returns:
    0 if ok, 1 if the stride is not DWORD aligned, 2 if the stride
	is not supported

 Notes:
    If the new byteStride is 0 (which is default when the library is
	reset), the library will automatically calculate the byte stride
	from the current frame buffer width and pixelformat settings.
	The stride should always be a multiple of 4 (DWORD-aligned).
	In YUV mode, the u/v stride is always the half of the y stride.
	In T6304, TDE_UpdateSize() should be called at least once after this
	function is called.
 ********************************************************************/
char TDE_SetFrameBufferStride(unsigned int byteStride);

/********************************************************************
 TDE_UpdateSize - Update those registers relative to size.

 Supports: T6304

 Params:
    None

 Returns:
    None

 Notes:
    This function is to update frame buffer resolution, output resolution,
	stride, axis, line size and the filter coefficient fit for these size. 
	Adding this function is to fix the bug that T6304 latch filter coeffients
	at vertical blanking but latch size at frame end. This bug will cause 
	underrun. -- Gauss, 030512
 ********************************************************************/
void TDE_UpdateSize();


/********************************************************************
 TDE_SetFrameBufferPointer - Sets a new frame buffer pointer

 Supports: T630x

 Params:
    baseAddress      - Address of the new frame in UMA (Y buffer in VCD)
	cbAddress        - Address of the Cb buffer in UMA (ignored in RGB mode)
	crAddress        - Address of the Cr buffer in UMA (ignored in RGB mode)

 Returns:
    0 if ok, 1 if the address is not dword-aligned in T6303 or 16-byte-aligned 
	in T6304.

 Notes:
    On the T6303, due to the way the hardware works, the new address 
	will be active only at the start of the next frame (not at the 
	start of the next field). In other words, it is possible to change 
	the address  only 30 times per second, not 60.
	cbAddress and crAddress are ignored in RGB mode. In VCD mode, if 
	they are set to 0, they will be automatically calculated by the
	library, and put consecutive to the base (Y) buffer.
	The address must be DWORD-aligned (multiple of 4) and should not exceed
	the	bound of the memory assigned to frame buffer. And more, they should
	not overlap.

 ********************************************************************/
char TDE_SetFrameBufferPointer(unsigned long baseAddress, unsigned long cbAddress, 
							   unsigned long crAddress);


/********************************************************************
 TDE_GetCurrentField - Returns the field currently being displayed

 Supports: T630x

 Returns:
    0 for even field (or v-blank after even field), 1 for odd field
	(or v-blank after odd field).

 Notes;
    There is no reason to return a different value for the v-blank
	periods, since we have another function, TDE_IsInVBlank, to 
	check for that.
 ********************************************************************/
char TDE_GetCurrentField(void);

/********************************************************************
 TDE_GetLineCounter - Returns current line counter

 Supports: T630x

 Returns:
    current line counter.
 ********************************************************************/
unsigned int TDE_GetLineCounter(void);


/********************************************************************
 TDE_SetDisplay - Enable displaying or disable displaying

  Supports: T630x

  Params:
     displayOn    - Flag for set display. 1 - display; 0 - not display
 ********************************************************************/
void TDE_SetDisplay(char displayOn);


/********************************************************************
 TDE_IsInVBlank - Check if the display engine is in the v-blank period

 Supports: T630x

 Returns:
    1 if within vertical blank, 0 if outside (displaying)
 ********************************************************************/
char TDE_IsInVBlank(void);


/********************************************************************
 TDE_IsFrameEnd - Check if at least one frame has finished the 
   display period, from last time you called this function

 Supports: T630x

 Returns:
    1 if a frame has finished the display period, 0 otherways.

 Notes:
    This function is useful on 6303 to know when a new frame buffer
	pointer has been effectively fetched and it's being displayed.
    You can call this function to clear the frameend status just before
	setting a new pointer, and then keep calling it until it returns
	1, which means that the new pointer has been effectively fetched,
	and it's currently being displayed on the screen
 ********************************************************************/
char TDE_IsFrameEnd(void);

/********************************************************************
 TDE_EnableVBInt - Enable Vertical blanking interruption function

 Supports: T630x

 Params:
		enable_vb_int - 0, disable VB int; 1, enable VB int
 Returns:

 Notes:
    This is VSyncCallBack function which is to enable vertical 
 blanking interruption. 
 ********************************************************************/
void TDE_EnableVBInt(unsigned char enable_vb_int);

/********************************************************************
 TDE_VSyncCallBack - VSync callback function

 Supports: T630x

 Params:

 Returns:

 Notes:
    This is VSyncCallBack function which call vsync handler. There is 
    bug in T6303, therefore a 10 ms timer interrupt is used here 
    to emulate VSync
 ********************************************************************/
void TDE_VSyncCallBack();

/********************************************************************
 TDE_SetVSyncCallback - Set a callback for the vsync

 Supports: T630x

 Params:
    callback         - Pointer to the callback

 Returns:
    The previously set callback (or NULL if none)

 Notes:
    This callback is granted to be called some time before the hardware
	fetches the new display position. The user should not assume that
	the callback will be called exactly at VSync time.

    On the T6303, we have to program our timer to simulate the VSync 
	callback some lines before it really happens, since the register is 
	fetched exactly at VSync time. On the T6304, we can use the real
	VSync interrupt, since the hardware fetches the register much later.
	Anyone who called this function should call "TDE_ResetVSyncCallback"
	to release vsync callback function as well.
 ********************************************************************/
DE_CB TDE_SetVSyncCallback(DE_CB cb);

/********************************************************************
 TDE_ResetVSyncCallback - Reset a vsync callback function

 Supports: T630x

 Params:
    callback         - Pointer to the callback

 Returns:
    The previously set callback (or NULL if none)

 Notes:
    Reset the vsync callback function when dont need it anymore.
 ********************************************************************/
void TDE_ResetVSyncCallback();

/********************************************************************
OSD FUNCTIONS
*********************************************************************/
/********************************************************************
 TDE_EnableOSD - Enable/Disable OSD

 Supports: T630x

 Params:
	enable_osd		-non zero: enable OSD, 0: disable OSD
 Returns:
    
 Notes:
    Enable/Disable OSD
 ********************************************************************/
void TDE_EnableOSD(unsigned char enable_osd);

/********************************************************************
 TDE_SetOSDBaseAddr - Set OSD base address

 Supports: T630x

 Params:
	OSDBase			-Base address of OSD buffer
 Returns:
    0 if ok, 1 if the address is not DWORD-aligned.

 Notes:
    Set OSD base address.
 ********************************************************************/
char TDE_SetOSDBaseAddr(unsigned long OSDBase);


/********************************************************************
 TDE_SetOSDResolution - Set the OSD reolustion

 Supports: T630x

 Params:
	OSDWidth		-OSD buffer width in pixel
	OSDHeight		-OSD buffer height in pixel
	osdPixelFormat   - OSD Pixel format (use the DE_OPF_* macros)

 Returns:
   0 ok, 1 if width is not supported,  2 if height is not supported,
   3 if the osdPixelFormat is not supported

 Notes:
    In T6303, MAX OSD resolution is 704*288 in PAL and 704*240 in NTSC. 
    HW scales it up twice in vertical direction. And only 2 bits OSD is
    supported and entry 0 is ALWAYS transparent.

	In T6304, the OSD stride should be set correctly before playback.
	You can set it with TDE_SetOSDStride(). If you never set OSD stride,
	TDE_SetOSDResolution will set the stride according to the width of 
	the OSD (in byte), but you should ensure that your OSD width (in byte)
    is 16-byte aligned.
 ********************************************************************/
char TDE_SetOSDResolution(unsigned long OSDWidth, unsigned long OSDHeight, int osdPixelFormat);

/********************************************************************
 TDE_SetOSDStride - Set the OSD frame buffer stride

 Supports: T6304

 Params:
	OSDByteStride       -OSD buffer stride in byte

 Returns:
   0 ok, 1 if stride is not 16-byte aligned.

 Notes:
    the byte stride should be 16-byte aligned.
 ********************************************************************/
char TDE_SetOSDStride(unsigned long OSDByteStride);

/********************************************************************
 TDE_SetOSDAxis - Set the OSD offset on the screen

 Supports: T6303/T6304
 
 Params:
   offsetX            - X offset on the screen
   offsetY            - Y offset on the screen

 Returns:
   0 ok, 1 if X offset is not supported,  2 if Y offset is not supported.
   3 if Y offset is not even in T6304
 
 Notes: 
 	The offset shall not out of the range of the screen. The Y offset should 
	be even in T6304.
     
 ********************************************************************/
char TDE_SetOSDAxis(unsigned int offsetX, unsigned int offsetY);

/********************************************************************
 TDE_UploadOSDPal - Upload the OSD palette 

 Supports: T6303/T6304
 
 Params:
   pal		         - The pointer of the palette
   entry_num         - The numebr of the palette entries

 Returns:
   0 ok, 1 if the entry number is not supported. In T6303, the entry 
   numebr shall only be 4 and entry 0 is transparent. In T6304, the 
   entry number should be no more than 256.
 
 Notes: 
 	The format of each entry of the palette is 
    31			  24			  16	  		  8				  0
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   | 		       |		Y      |	   U       |		V      |
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

 	In T6303, only 2 bits OSD is available and entry 0 is always 
 	transparent. 
     
 ********************************************************************/
char TDE_UploadOSDPal(unsigned long *pal, int entry_num);


/********************************************************************
						SP FUNCTIONS
*********************************************************************/
/********************************************************************
 TDE_EnableSP - Enable/Disable sub-picture

 Supports: T6304

 Params:
	enable_sp		-non zero: enable subpicture, 0: disable subpicture
 Returns:
    NULL
 Notes:
    Enable/Dsiable sub-picture
 ********************************************************************/
void TDE_EnableSP(unsigned char enable_sp);

/********************************************************************
 TDE_SetSPBaseAddr - Set SP base address

 Supports: T6304

 Params:
	SPBase			-Base address of SP buffer
 Returns:
    0 if ok, 1 if the address is not 16 byte-aligned.

 Notes:
    Set SP base address.
 ********************************************************************/
char TDE_SetSPBaseAddr(unsigned long SPBase);


/********************************************************************
 TDE_SetSPResolution - Set the SP reolustion

 Supports: T6304

 Params:
	SPWidth		   -SP buffer width in pixel
	SPHeight	   -SP buffer height in pixel
	SPByetStride   -sp buffer stride
 Returns:
   0 ok, 1 if width is not supported,  2 if height is not supported,
   3 if SPByteStride is not 16-byte alignment.
 Notes:
    Set SP resolution which shall not be bigger than the TV screen
 ********************************************************************/
char TDE_SetSPResolution(unsigned long SPWidth, unsigned long SPHeight, 
						 unsigned long SPByteStride);

/********************************************************************
 TDE_SetSPPixelFormat - Set the SP pixel format

 Supports: T6304

 Params:
	SPPixelFormat  -SP pixel format, 0 - 2bit; 1 - 8bit. See enum 
					TDE_SPPIXELFORMATS.

 ********************************************************************/
void TDE_SetSPPixelFormat(int SPPixelFormat);

/********************************************************************
 TDE_SetSPAxis - Set the SP offset on the screen

 Supports: T6304
 
 Params:
   offsetX            - X offset on the screen
   offsetY            - Y offset on the screen

 Returns:
   0 ok, 1 if X offset is not supported,  2 if Y offset is not supported.
 
 Notes: 
 	The offset shall not out of the range of the screen
     
 ********************************************************************/
char TDE_SetSPAxis(unsigned int offsetX, unsigned int offsetY);

/********************************************************************
 TDE_UploadSPPal - Upload the SP palette 

 Supports: T6304
 
 Params:
   pal		         - The pointer of the palette

 Returns:
 
 Notes: 
 	The format of each entry of the palette is 
   31		27	   24			  16	  		   8			   0
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   |        | Index |	    Y      |	   U       |		V      |
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    Index is the entry number 
   The length of the pallette array should be 16.  
 ********************************************************************/
void TDE_UploadSPPal(unsigned long *pal);

/********************************************************************
 TDE_SetSPColorCode - Set the SP color code

 Supports: T6304
 
 Params:
   	colorcode:
    	bit 0-3: Background pixel color code
    	bit 4-7: Pattern pixel color code
    	bit 8-11: Emphasis pixel-1 color code
    	bit 12-15: Emphasis pixel-2 color code

 Returns:
 
 Notes: 
 	Set SP color code
 ********************************************************************/
void TDE_SetSPColorCode(unsigned short color_code);

/********************************************************************
 TDE_SetSPContrast - Set the SP mixture ratio

 Supports: T6304
 
 Params:
	contrast:
    	bit 0-3: Background pixel contrast
    	bit 4-7: Pattern pixel contrast
    	bit 8-11: Emphasis pixel-1 contrast
    	bit 12-15: Emphasis pixel-2 contrast
 Returns:
 
 Notes: 
 	Set SP color mixture ratio
 ********************************************************************/
void TDE_SetSPContrast(unsigned short contrast);

////////////////////Functions for set output/////////////////////////

/********************************************************************
 TDE_VideoOutputMode - Set video output mode

 Supports: T6304
 
 Params:
	videoOutputMode:
    	See the macros: DE_VOM_XXX
 Returns:
	0, OK; 1, the video output mode is not supportted.
 Notes: 
 	Set video output mode
 ********************************************************************/
int TDE_SetVideoOutputMode(int videoOutputMode);

/********************************************************************
 TDE_VideoScanMode - Set video scan mode

 Supports: T6304
 
 Params:
	videoScanMode:
    	See the macros: DE_VSM_XXX
 Returns:
	0, OK; 1, the video scan mode is not supportted
 Notes: 
 	Set video scan mode
 ********************************************************************/
int TDE_SetVideoScanMode(int videoScanMode);

/********************************************************************

  Name: 
  TDE_PowerTVE_DAC
  Description: 
  Power up or down DACs of the  internal TV encoder
  Parameters: 
  IN:
  power - 0: power down; 1: power up
  
    OUT:
	None
	Return: 
    None
********************************************************************/
void TDE_PowerTVE_DAC(int power);

//////////////////////Functions for retrieving infomation/////////////

/********************************************************************
 TDE_GetFrameBufferPointerY - Get the current display memory base address of Y (YUV mode) 
							 or RGB data (RGB mode)

 Supports: T6303/T6304

 Params:		
	NULL

 Returns:
   the current display memory base address of Y (YUV mode) or RGB data (RGB mode)

 ********************************************************************/
unsigned long TDE_GetFrameBufferPointerY();

/********************************************************************
 TDE_GetFrameBufferPointerCb - Get the current display memory base address of Y (YUV mode) 
							 or RGB

 Supports: T6303/T6304

 Params:		
	NULL

 Returns:
   the current display memory base address of Y (YUV mode) or RGB

 ********************************************************************/
unsigned long TDE_GetFrameBufferPointerCb();

/********************************************************************
 TDE_GetFrameBufferPointerCr - Get the current display memory base address of Cr

 Supports: T6303/T6304

 Params:		
	NULL

 Returns:
   The current display memory base address of Cr. It's meaningless in RGB mode

 ********************************************************************/
unsigned long TDE_GetFrameBufferPointerCr();

/********************************************************************
 TDE_GetFrameBufferWidth - Get the current width of the frame buffer

 Supports: T6303/T6304

 Params:		
	NULL

 Returns:
   current width of the frame buffer

 ********************************************************************/
unsigned int TDE_GetFrameBufferWidth();

/********************************************************************
 TDE_GetFrameBufferHeight - Get the current Height of the frame buffer

 Supports: T6303/T6304

 Params:		
	NULL

 Returns:
   current Height of the frame buffer

 ********************************************************************/
unsigned int TDE_GetFrameBufferHeight();

/********************************************************************
 TDE_GetOutputWidth - Get the current output width 

 Supports: T6303/T6304

 Params:		
	NULL

 Returns:
	the current output width 

 ********************************************************************/
unsigned int TDE_GetOutputWidth();

/********************************************************************
 TDE_GetOutputHeight - Get the current output Height 

 Supports: T6303/T6304

 Params:		
	NULL

 Returns:
	the current output width 

 ********************************************************************/
unsigned int TDE_GetOutputHeight();

/********************************************************************
 TDE_GetFrameBufferStride - Get the current stride of the frame buffer (in bytes)

 Supports: T6303/T6304

 Params:		
	NULL

 Returns:
   The stride of the frame buffer (in bytes)

 Notes:
    You should know the stride when you set it. DE will never change the stride automatically. 
    But you can also get the current stride by this function.
 ********************************************************************/
unsigned int TDE_GetFrameBufferStride();

/********************************************************************
 TDE_GetTVMode - Get the current TV mode of DE

 Supports: T6303/T6304

 Params:		
	NULL

 Returns:
   0 if PAL, 1 if NTSC

 Notes:
    You should know the TV mode when you set it. DE will never change the TV mode 
    automatically. But you can also get the current TV mode by this function.
 ********************************************************************/
char TDE_GetTVMode();

/********************************************************************
 TDE_GetPictureMode - Get the current picture mode of DE

 Supports: T630x

 Params:		
	NULL

 Returns:
   0 if RGB, 1 if VCD, 2 if SVCD, 3 if DVD, 5 if DIVXVCD, 6 if DIVXSVCD (refer to the enum.)

 Notes:
    You should know the TV mode when you set it. DE will never change the picture mode 
    automatically. But you can also get the current picture mode by this function.
 ********************************************************************/
char TDE_GetPictureMode();

/********************************************************************
 TDE_GetAxisX - Get the horizontal axis that the main-picture will be display on the screen

 Supports: T6303/T6304

 Params:		
	NULL

 Returns:
   the horizontal axis value ( in pixels) that the main-picture will be display on the screen

 Note:
   It means the offset to the left up corner on the screen 

 ********************************************************************/
unsigned int TDE_GetAxisX();

/********************************************************************
 TDE_GetAxisY - Get the vertical axis that the main-picture will be display on the screen

 Supports: T6303/T6304

 Params:		
	NULL

 Returns:
   the vertical axis value (in lines) that the main-picture will be display on the screen

 Note:
   It means the offset to the left up corner on the screen 

 ********************************************************************/
unsigned int TDE_GetAxisY();

/********************************************************************
 TDE_GetPixelFormat - Get the current pixel format of the frame buffer 

 Supports: T6303/T6304

 Params:		
	NULL

 Returns:
   An integer value indicating the current pixel format of the frame buffer,
   Pls reference to the enumeration at top.

 Notes:
    You should know the pixel format when you set it. DE will never change it 
    automatically. But you can also get it by this function.
 ********************************************************************/
int TDE_GetPixelFormat();

/*******************************************************************************
 TDE_FixBufferUnderrun - Fix underrun

 Supports: T6303

 Params:
    NULL
				
 Returns:	 
    0 if nothing  to be done. 1 if underrun happened and has been fixed.
 
 Notes:	     
	This function is used to check whether underrun is happened. And if	that's
	ture, this function will disable DE playback , then enable it and clear the
	underrun flag in register. This will help in improve the performance when 
	underrun occurs, but can't solve it. Note that here "disable or enable DE 
	playback" is a little different with the function named DisablePlayBack or
	EnablePlayBack. Please check the code if you want to know more. --Jacka
	
 ******************************************************************************/
char TDE_FixBufferUnderrun(void);

/********************************************************************
 TDE_ClearVBInt - Clear Vertical blank interruption 

 Supports: T6304

 Params:		
	NULL

 Returns:
   None.

 Notes:
    This function is used to clear vertical blank interruption.
 ********************************************************************/
void TDE_ClearVBInt();

/*******************************************************************************
 TDE_SetAntiflickerMode - Change the anti-flicker mode
 
 Params: 
    antiflickerMode - anti-flicker mode (use the DE_ATM_* macros)

 Returns:
    0 if ok, 1 if the anti-flicker mode is not correct

 Notes:
	The less flickering, the more blurring. So the default anti-flicker mode is low.
	You can call this function to change it if you want less flicker.

*******************************************************************************/
char TDE_SetAntiflickerMode(int antiflickerMode);

/******************************************************************************
 TDE_SetMacrovision - Set Macrovision type

 Supports: T6304
 
 Params:
   MvType      - Macrovision type (refer to macros DE_MVT_*)

 Returns:
   0 ok, 1 the type is not correct.
      
  ******************************************************************************/
char TDE_SetMacrovision(int MvType);

/********************************************************************
 TDE_GetVersion - Get the current version of the library 

 Params:		
	NULL

 Returns:
   Current version of the library.
 ********************************************************************/
unsigned int TDE_GetVersion();

/********************************************************************
 TDE_GetName - Get the name of the library 

 Params:		
	NULL

 Returns:
   name of the library.
 ********************************************************************/
char * TDE_GetName();

#ifdef __cplusplus
}
#endif

#endif


