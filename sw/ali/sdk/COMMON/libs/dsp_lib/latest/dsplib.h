/* release-1_0_1_7, 2004/04/14
$Id: dsplib.h,v 1.1.1.1 2004/05/04 23:26:17 paulm Exp $
*/

#ifndef DSPLIB_H
#define DSPLIB_H

//{{{{{{{ general configurations
//#define DSPLIB_DEBUG
//#define DSP_BIST_TEST
//#define LOAD_DSP_FROM_EDGE

//#define PCM_INCLUDED
//#define DIGIOUT_INCLUDED
//#define MP2DEC_INCLUDED
//#define MP3DEC_INCLUDED
//#define AC3DEC_INCLUDED
//#define WMADEC_INCLUDED
//}}}}}}}

#ifndef TDSP_LIB_COMMON
#define TDSP_LIB_COMMON

#ifdef DSPLIB_DEBUG
#define dbg_printf		soc_printf
#else
#define dbg_printf(...)
#endif

#ifndef TRUE
#define TRUE            1
#endif

#ifndef FALSE
#define FALSE           0
#endif

#ifndef NULL
#define NULL            0
#endif

typedef unsigned long   TDSP_UINT32;
typedef unsigned short  TDSP_UINT16;
typedef unsigned char   TDSP_UINT8;

#endif /* TDSP_LIB_COMMON */

/* output type for TDSP_AuioOutput() */
enum TDSP_OUT_TYPE
{
	TDSP_OUT_BOTH,
	TDSP_OUT_DIGITAL,
	TDSP_OUT_ANALOG,
};

/* digital output type for TDSP_SelectSpdifType() */
enum TDSP_SPDIF_TYPE
{
	TDSP_SPDIF_RAWDATA,
	TDSP_SPDIF_PCM,
	TDSP_SPDIF_DISABLE
};

/* channel num for TDSP_SetSoundVolumn(), SetSoundDelay() */
enum TDSP_CHANNEL
{
	TDSP_CH_CEN,
	TDSP_CH_FL,
	TDSP_CH_FR,
	TDSP_CH_SL,
	TDSP_CH_SR,
	TDSP_CH_SUBWOOFER,
	TDSP_CH_ALL,
	TDSP_CH_MIC_MAIN,
	TDSP_CH_MICL,
	TDSP_CH_MICR
};

/* channel type for TDSP_TurnOnAudioChn() */
enum TDSP_AUDIO_CHANNEL
{
//    Value     Downmix Mode
//      0       1+1 (L,R) Dolby Surround compatible
//      1       1/0 (C)
//      2       2/0 (L,R)
//      3       3/0 (L,R,C)
//      4       2/1 (L,R,l)
//      5       3/1 (L,R,l,C)
//      6       2/2 (L,R,l,r)
//      7       3/2 (L,R,l,r,C)
	TDSP_STEREO,	//0x02
	TDSP_VIRTUAL_SURROUND,	//0x08
	TDSP_5DOT1,	//0x07
	TDSP_LTRT,	//0x00
	TDSP_LT,	//0x09
	TDSP_RT		//0x0A
};

/* effect for TDSP_TurnOnEffect() */
enum TDSP_AUDIO_EFFECT
{
	TDSP_EFF_NO_EFFECT,
	TDSP_EFF_EQUALIZER,
	TDSP_EFF_REVERB,
	TDSP_EFF_KEY_SHIFT,
	TDSP_EFF_VOCAL_CANCEL,
	TDSP_EFF_ECHO,
	TDSP_EFF_VIRTUAL_SURROUND,
};

/* sub-effect for TDSP_EFF_EQUALIZER in TDSP_TurnOnEffect() */
enum TDSP_EQUALIZER
{
	TDSP_EQ_DISABLE,
	TDSP_EQ_POP,
	TDSP_EQ_ROCK,
	TDSP_EQ_JAZZ,
	TDSP_EQ_CLASSIC,
	TDSP_EQ_BASS,
};

/* sub-effect for TDSP_EFF_CINEMA in TDSP_TurnOnEffect() */
enum TDSP_REVERBERATION
{
	TDSP_REV_DISABLE,
	TDSP_REV_CHURCH,
	TDSP_REV_CINEMA,
	TDSP_REV_CONCERT,
	TDSP_REV_HALL,
	TDSP_REV_LIVE,
	TDSP_REV_ROOM,
	TDSP_REV_STADIUM,
	TDSP_REV_STANDARD,
};

enum TDSP_VOCAL_CANCEL_CMD {
	TDSP_VC_DISABLE,
	TDSP_VC_ENABLE
};

enum TDSP_ECHO_CMD {
	TDSP_ECHO_DISABLE,
	TDSP_ECHO_ENABLE
};

enum TDSP_VIRTUAL_SURROUND_CMD {
	TDSP_VS_DISABLE,
	TDSP_VS_ENABLE
};

enum TDSP_SUBWOOFER_SWITCHER {
	TDSP_SUBWOOFER_ON,
	TDSP_SUBWOOFER_OFF,
};

enum TDSP_CHANNEL_SWITCH {
	TDSP_CHN_SW_STEREO,
	TDSP_CHN_SW_LEFT,
	TDSP_CHN_SW_RIGHT
};

/* sample rate for TDSP_SelectLPCMSampleRate() */
#define TDSP_LPCM_48K                   0
#define TDSP_LPCM_96K                   1

/* source type for TDSP_SelectSourceType() */
//#define TDSP_PS_AUDIO                   1
//#define TDSP_SEGA_AUDIO                 2
#define TDSP_MP3_DECODE                 3
#define TDSP_MP2_DECODE                 4
//#define TDSP_MP3_ENCODE                 5
//#define TDSP_MIDI                       7
#define TDSP_AC3                        8
#define TDSP_LPCM                       9
#define TDSP_DTS                        10
#define TDSP_AC3DEC                     11
#define TDSP_MPA_DECODE                 12
#define TDSP_PCM                        65536
#define TDSP_WMADEC						13
#define TDSP_DTSCD						14

/* error code for TDSP_GetLastError() */
#define TDSP_ERR_NO_ERROR               0x0
#define TDSP_ERR_INVALID_SOURCE_TYPE    0x10
#define TDSP_ERR_INVALID_SAMPLE_RATE    0x20
#define TDSP_ERR_INVALID_BIT_RESOLUTION 0x21
#define TDSP_ERR_INVALID_NUM_OF_CHN     0x22
#define TDSP_ERR_INVALID_OUTPUT_TYPE    0x30
#define TDSP_ERR_INVALID_SPDIF_TYPE     0x40
/*#define TDSP_ERR_AUDIO_QUEUE_FULL       0x50 */
#define TDSP_ERR_INVALID_AUDIO_STREAM   0x51
#define TDSP_ERR_NOT_ENOUGH_SPACE       0x52
#define TDSP_ERR_BUF_UNDERRUN           0x53
#define TDSP_ERR_TOO_MANY_TO_COPY       0x54 /* nbyte_to_copy > nbyte_of_buf */
#define TDSP_ERR_INCOMPLETE_HEADER      0x55
#define TDSP_ERR_NOT_SUPPORTED          0x56
#define TDSP_ERR_TIME_OUT               0x100

typedef void (*TDSP_CB)(void);

int TDSP_Init(TDSP_UINT8 *buf, TDSP_UINT32 nbyte);
int TDSP_InitRevWorkingBuf(TDSP_UINT8 *buf, TDSP_UINT32 nbyte);
int TDSP_InitKeyshiftWorkingBuf(TDSP_UINT8 *buf, TDSP_UINT32 nbyte);
int TDSP_InitDelayWorkingBuf(TDSP_UINT8 *buf, TDSP_UINT32 nbyte);
int TDSP_SelectSourceType(TDSP_UINT32 type);
int TDSP_ConfigureDAC(TDSP_UINT32 fs, TDSP_UINT16 bitres, TDSP_UINT16 nch);
void TDSP_TurnOnDAC();
void TDSP_TurnOffDAC();
int TDSP_ConfigureADC(TDSP_UINT32 fs, TDSP_UINT16 res, TDSP_UINT16 nch);
void TDSP_TurnOnADC();
void TDSP_TurnOffADC();
int TDSP_AudioOutput(TDSP_UINT32 type);
int TDSP_SelectSpdifType(TDSP_UINT32 type);
int TDSP_SetSoundVolume(TDSP_UINT32 vol, TDSP_UINT32 ch);
int TDSP_TurnOnAudioChn(TDSP_UINT32 type);
int TDSP_TurnOnEffect(TDSP_UINT32 effect, int sub);
int TDSP_SetReverbVolume(TDSP_UINT32 sub);
int TDSP_SetSoundDelay(TDSP_UINT32 delay, TDSP_UINT16 ch);
int TDSP_SetDynScale(TDSP_UINT16 dynscale);
int TDSP_SetDRCMode(TDSP_UINT16 drcmode);
int TDSP_SelectLPCMSampleRate(TDSP_UINT32 fs);
int TDSP_TurnOnVoice();
int TDSP_TurnOffVoice();
int TDSP_CopyAudioStream(TDSP_UINT8 *buf, TDSP_UINT32 nbyte);
int TDSP_EndOfAudioStream();
int TDSP_PlaySound();
int TDSP_StopSound();
int TDSP_PauseSound();
long TDSP_GetTime();
long TDSP_GetRemainTime(void);
int TDSP_SetTime(long ms);
TDSP_UINT32 TDSP_GetLastError();
const char *TDSP_GetVersion();
const char *TDSP_GetName();
int TDSP_SwitchChannel(int chn);
TDSP_UINT32 TDSP_GetFffbOffset(int fffb, int fffbtime);
int TDSP_SetCount(TDSP_UINT32 count);
TDSP_UINT32 TDSP_GetCount();
TDSP_UINT32 TDSP_GetCountFrequency();

int TDSP_IsStreamCbr();
int TDSP_SetDecodedFrame(long nframe);
long TDSP_GetDecodedFrame();
int TDSP_SetMpeg4Bitrate(int bitrate);
	
TDSP_CB TDSP_SetDSPCallback(TDSP_CB newhandler);
void TDSP_ResetDSPCallback();

void TDSP_Isr();

/* the following function are for debug purpose */
int TDSP_QueryBufStat();
//int TDSP_LoadDSPProgram(TDSP_UINT32 *dspinst, TDSP_UINT32 len);
//void TDSP_SetLastError(TDSP_UINT32 error_code);

int TDSP_TimerInit(void);
int TDSP_Delay(unsigned long ms);

typedef struct _DSP_DESC_
{
	unsigned long	*DspCode;
	unsigned long	DspCodeSize;
	unsigned short	*DspData1;
	unsigned long	DspData1Size;
	unsigned short	*DspData2;
	unsigned long	DspData2Size;
}DSP_DESC, *PDSP_DESC;

typedef struct SPU_PARA_
{
	volatile struct SPU_PARA_	*pspu_para;
	volatile unsigned char	*in_buf;
	volatile unsigned char	*out_buf;
	volatile unsigned short	length;
	volatile unsigned short	top;
	volatile unsigned short	bottom;
  union
  {
	volatile unsigned short	width;
	volatile unsigned short	busy;
  };
	volatile unsigned short	height;
} SPU_PARA, *PSPU_PARA;
/* removed from dsp
void TDSP_SPUFIX(PSPU_PARA pSPU);
*/

extern const DSP_DESC DSP_PCM_Tab;
extern const DSP_DESC DSP_DIGIOUT_Tab;
extern const DSP_DESC DSP_MP3DEC_Tab;
extern const DSP_DESC DSP_MP2DEC_Tab;
extern const DSP_DESC DSP_AC3DEC_Tab;
extern const DSP_DESC DSP_WMADEC_Tab;

int TDSP_InitDspMem(int audio_src_type, int pm_divide_mode);

void TDSP_SacrificePlay();

/* ac3 certification function declaration */
int M6304AC3BitstreamInfo(unsigned short *Info);
int M6304SetAC3DualMode(unsigned short DualMode);
int M6304SetAC3DRCMode(unsigned short DRCMode);
int M6304SetAC3LFEChn(unsigned short Enable);
int M6304SetAC3Karaoke(unsigned short KaraMode , unsigned short VocalCofig);
int M6304SetAC3Outmode(unsigned short OutMode);
int M6304SetAC3StereoMode(unsigned short StereoMode);
int M6304SetAC3PCMScale(int PCMScale);
int M6304SetAC3DRCLowScale(int LowScale);
int M6304SetAC3DRCHighScale(int HighScale);
int M6304SetAC3SubWOOFER(unsigned short Enable);
int M6304SetAC3BassRD(unsigned short Enable, unsigned short config);
int M6304SetAC3IECOut(unsigned short OutMode);

#ifndef _BIOS_AUDIO_H	//#include "BIOS_Audio.h"
#define _BIOS_AUDIO_H

enum BIOS_DAC_I2SCTRL
{
	//DAC_AC97,
	DAC_ALC650 = 0x0000,
	DAC_ALC658 = 0x0000,
	
	//DAC_I2S
	DAC_CS4340 = 0x0003,	//I2S mode, 16-bit
	DAC_1602 = 0x0053,		//24-bit right adjusted format
	DAC_PCM1738 = 0x0003	//I2S mode, 16-bit
};

typedef struct _BIOS_AUDIO_INTERFACE
{
	unsigned short fs;
	unsigned short i2s_ctrl;
} BIOS_AUDIO_INTERFACE, *PBIOS_AUDIO_INTERFACE;

int BIOS_GetAudioInterface(BIOS_AUDIO_INTERFACE *interface);

//#define DAC_DYNSWITCH_ENABLE
//int GetDacMode(int *mode);
#endif //#ifndef _BIOS_AUDIO_H

#endif /* DSPLIB_H */
#define PCM_INCLUDED
#define DIGIOUT_INCLUDED
#define MP2DEC_INCLUDED
#define MP3DEC_INCLUDED
#define AC3DEC_INCLUDED
#define WMADEC_INCLUDED
