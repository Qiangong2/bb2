/***********************************************/
/* headfile for the eeprom.c                   */
/* frist write by Victor                       */
/* data: 2003/10/21                            */
/***********************************************/

#ifndef _EEPROM_H
#define _EEPROM_H

/********************************************************************
 EEPROM_read - Read data from EEPROM.

 Supports: T630x
 
 Parms:
 	TargetDataPtr - Pointer to buffer where will be store data which 
 					was read from EEPROM.
 	rlen		  -	number of data.
 	
 Returns:
	SUCCESS:	1
	ERROR:		0
	ERROR_MODE:	-1
********************************************************************/
int EEPROM_read(unsigned char *TargetDataPtr,unsigned int rlen);

/********************************************************************
 EEPROM_write - Write data to EEPROM.

 Supports: T630x
 
 Parms:
 	SourceDataPtr - Pointer to the buffer in which store the source 
					data which want to write to EEPROM. 

 	wlen		  -	number of data.
 	
 Returns:
	SUCCESS:	1
	ERROR:		0
	ERROR_MODE:	-1
********************************************************************/
int EEPROM_write(unsigned char *SourceDataPtr,unsigned int wlen);

/********************************************************************
 EEPROM_getSize - Get size of EEPROM.

 Supports: T630x
 
 Parms:
 	none.
 	 	
 Returns:
	EEPROM_SIZE: Size of EEPROM by bytes.
********************************************************************/
int EEPROM_getSize(void);

#endif 
