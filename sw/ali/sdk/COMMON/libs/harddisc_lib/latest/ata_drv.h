#ifndef __ATA_DRV_H
#define __ATA_DRV_H
/*
typedef	unsigned char	BYTE;
typedef unsigned short	WORD;
typedef unsigned long	DWORD;
*/
//master or slave
#define DEVICE0				0x0//master
#define DEVICE1				0x1//slave

//Partition type
#define PARTITION_END			0
#define PARTITION_PRIMARY		1
#define PARTITION_LOGICAL		2

#define MAX_PARTITION_COUNT		10 //Max partition count in a disk

#define MAX_ALL_LOGIC_DRIVERS_COUNT	20 //Max logic drivers in all disks

//format type of logic dirver
#define  EMPTY			0x00	 
#define	 FAT12			0x01
#define	 XENIX1			0x02
#define	 XENIX2			0x03
#define	 FAT16S32MEG	        0x04
#define	 EXTENDED		0x05
#define	 FAT16L32MEG	        0x06
#define	 NTFS			0x07
#define	 BOOT			0x0A
#define	 FAT32			0x0B
#define	 LINUX			0x53
#define	 NOVELL			0x64
#define	 PCIX			0x75
#define	 CPM			0xDB
#define	 BBT			0xFF

#define T2_IDE  0
#define PCI_IDE 1
//Device type
#define  IDE_CONFIG_TYPE_NONE    0
#define  IDE_CONFIG_TYPE_UNKN	 1
#define  IDE_CONFIG_TYPE_HD	 2//Hard disk driver
#define  IDE_CONFIG_TYPE_LD      3//CD/DVD  loader     

typedef struct{
	BYTE	Interface;	
	BYTE	DevType;
	BYTE	PIOM;
	BYTE	ULTRADMA;
	BYTE	DMAM;
/*	WORD    Cyls;
	WORD	Heads;	
	WORD	Sectors;*/
	BYTE	Partition;
	BYTE	Ena48bit;
	DWORD	Capacity;
	DWORD	lba48bit;			
}Ide_Info_Struct;

typedef struct{
	BYTE	PartitionType;		//Primary or Logical
	BYTE	FormatType;		//File System Type
	DWORD	SectorCount;		//Size of the partition
}PartitionStruct;

/*-------------------------------------------------------------------------------
description:
Detect devices and set the configuration information Then it call hd_init() to 
initialise all the detected hard disks. 
input:		None
return:		
		if return = -1 ,error happen
		if return = 0, it means there is no device detected.
		if return  = 1, OK
-------------------------------------------------------------------------------*/

extern int ide_all_init();


/*-------------------------------------------------------------------------------
description: initialize ide harddisk, get the information of ata/ide device connected to the IDE interface,
			 soft reset the ata/ide device, then choose and set transfer mode.
input:		0 primary/master, 1 primary/slave 
		2 secondary/master, 3 secondary/slave
	
return:		
		if return = -1 ,error happen
		if return = 0, it means there is no harddisk connected.
		if retur  = 1, OK
-------------------------------------------------------------------------------*/
extern int hd_init(int dev);


/*-------------------------------------------------------------------------------
description: Read the special sector data from ata/IDE device
input:
			int device - 0 primary/master, 1 primary/slave 
			 	     2 secondary/master, 3 secondary/slave
			int partitionNum - 0, 1, 2, ... corresponding to C:, D:, E:... partition
			int sectCount - the count of sectors(1 - 256 sectors) that read data from harddisk
			long startLba - the logicl block address that begin to read data from harddisk
			unsigned char* buffer -  the data buffer for read, at least 512 * sectCount size
return:		0 - OK
		1 - error happen
note:	This read is blocking, and the fast PIO mode is selected.	
-------------------------------------------------------------------------------*/
extern int hd_read_sectors( int device, 
					  int partitionNum,
					  int sectCount, 
					  long startLba,
					  unsigned char* buffer
				     );

/*-------------------------------------------------------------------------------
description: write the special sector data to ata/IDE device
input:
			int device - 0 primary/master, 1 primary/slave 
			 	2 secondary/master, 3 secondary/slave
			int partitionNum - 0, 1, 2, ... corresponding to C:, D:, E:... partition
			int sectCount - the count of sectors(1 - 256 sectors) that write data to harddisk
			long startLba - the logicl block address that begin to write data to harddisk
			unsigned char* buffer -  the data buffer for write, at least 512 * sectCount size
return:		0 - OK
		1 - error happen
note:		This write is blocking, and the fast PIO mode is assumed.
-------------------------------------------------------------------------------*/
extern int hd_write_sectors( int device, 
			int partitionNum,
                       int sectCount, 
                       long startLba,
                       unsigned char* buffer
				      );


/*-------------------------------------------------------------------------------
description: get partition information of logic drivers 
input:		int device - 0 primary/master, 1 primary/slave 
			 2 secondary/master, 3 secondary/slave
		PartitionStruct *partitionInfo
			   - Simple partition information required by customers
return:		-1 - error happen
			> 0 - the count of harddisk patritions(c:, d:, e:, ...)
-------------------------------------------------------------------------------*/
extern int hd_get_driver_info(int device, PartitionStruct *partitionInfo);

/*-------------------------------------------------------------------------------
description: Set partition into hard disk 
input:		int device - 0 primary/master, 1 primary/slave 
			     2 secondary/master, 3 secondary/slave
		PartitionStruct *partitionInfo
			   - Simple partition table provided by customers
return:		-1 - error happen
		> 0 - the count of harddisk patritions(c:, d:, e:, ...)
-------------------------------------------------------------------------------*/
extern int hd_set_driver_info(int device, PartitionStruct *partitionInfo);
/*-------------------------------------------------------------------------------
description: Start reading sector data from ata/IDE device
input:
			int device - 0 primary/master,   1 primary/slave 
			 	     2 secondary/master, 3 secondary/slave
			int partitionNum - 0, 1, 2, ... corresponding to C:, D:, E:... partition
			int sectCount - the count of sectors(1 - 256 sectors) that read data from harddisk
			long startLba - the logicl block address that begin to read data from harddisk
			unsigned char* buffer -  the data buffer for read, at least 512 * sectCount size
return:			0 - OK
			1 - error happen
note:	This read is non-blocking, and the fast DMA mode is selected.	
-------------------------------------------------------------------------------*/

extern int hd_read_start( int device, 
					  int partitionNum,
					  int sectCount, 
					  long startLba,
					  unsigned char* buffer
				     );
				     
/*-------------------------------------------------------------------------------
description: Start to write sector data to ata/IDE device
input:
			int device - 0 primary/master, 1 primary/slave 
			             2 secondary/master, 3 secondary/slave
			int partitionNum - 0, 1, 2, ... corresponding to C:, D:, E:... partition
			int sectCount - the count of sectors(1 - 256 sectors) that write data to harddisk
			long startLba - the logicl block address that begin to write data to harddisk
			unsigned char* buffer -  the data buffer for write, at least 512 * sectCount size
return:		0 - OK
		1 - error happen
note:		This write is non-blocking, and the fast DMA mode is assumed.
-------------------------------------------------------------------------------*/				     
extern int hd_write_start( int device, 
					  int partitionNum,
					  int sectCount, 
					  long startLba,
					  unsigned char* buffer
				     );

/*-------------------------------------------------------------------------------
description: Return the  current state of the last read/write task.
input:		int dev - 0 primary/master, 1 primary/slave 
			  2 secondary/master, 3 secondary/slave
return:		0 - Not Over
		1 - over
		-1 - Error and aborted
note:		This funtion will poll the state of the last read/write task.
-------------------------------------------------------------------------------*/				     

extern int hd_dma_over();

/*-------------------------------------------------------------------------------
description: Interrupt handler for dma read/write.
input:		None
return:		None

-------------------------------------------------------------------------------*/				     

extern void hd_interrupt_handler(void);

/*-------------------------------------------------------------------------------
description: Stop harddisk activities.
input:		None
return:		None
-------------------------------------------------------------------------------*/
extern void hd_stop(int dev);

/*-------------------------------------------------------------------------------
description: Stop harddisk activities and make it enter into sleep mode.
input:		None
return:		None
-------------------------------------------------------------------------------*/
extern void hd_sleep(int dev);

/*-------------------------------------------------------------------------------
description: Enter into normal work mode.
input:		None
return:		None
-------------------------------------------------------------------------------*/
extern void hd_continue(int dev);

/*-------------------------------------------------------------------------------
description: Give user customized configuration information of the ide system.This table 
contains other information such as capacity, ide transfer mode after you call the function 
hd_init(dev).
input:		Ide_Info_Struct * p_config_info
			   - Configuration information table 
return:		None
-------------------------------------------------------------------------------*/
extern void hd_set_interface(Ide_Info_Struct * p_config_info);

/*-------------------------------------------------------------------------------
description: Return internal configuration information table. 
input:		None
return:		Ide_Info_Struct * p_config_info
			   - Configuration information table
-------------------------------------------------------------------------------*/
extern Ide_Info_Struct *hd_get_interface(void);

/*-------------------------------------------------------------------------------
description: Reset configuration information table and some global vairable. It is 
of use if you want to restart the whole initialization process. But in most cases 
it is not necessary even in shared pins cases. 
input:		None
return:		None
-------------------------------------------------------------------------------*/
extern void hd_reset_interface(void);
#endif
