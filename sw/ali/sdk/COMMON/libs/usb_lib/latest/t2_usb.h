/***************************************************************************
	Application library - USB library
	T-Square, Zhuhai
	Started by Victor, 2003-07-29
 ***************************************************************************/

#ifndef __T2_USB_H
#define __T2_USB_H

#ifdef __cplusplus
extern "C" {
#endif
/*
CALLBACK wait_for_int()
{
#if DVD	
	dly_tsk();
#else
	BIOS_USB_ISR_Handle();
#endif		
}
*/
/********************************************************************
USB_Init - Initialize USB Host control.

 Supports: T630x
 
 Parms:
    void*virtual_function:  - virtual usb operate function entry.
    unsigned int mode: - indicate initialize device using blocking or nonblocking mode.
 
 Returns: 
    0: Init USB PCI head register and host control not finish.
    1: Init USB PCI head register and host control finish.
 ********************************************************************/
int USB_Initialize(void*virtual_function,unsigned int mode);


/********************************************************************
USB_CheckHubPort - Check USB device whether connect with the port.

 Supports: T630x
 
 Parms:
 	port  - number of USB port.
 
 Returns:
	DEVICE_NOEXIST(-1): device not exist.
	DEVICE_EXIST(1):    device connect on the port.
 ********************************************************************/
int USB_CheckHubPort(unsigned char port);


/********************************************************************
USB_HubThread - Initialize USB device.

 Supports: T630x
 
 Parms:
 	port  - number of USB port.
 
 Returns:
	0: usb device init success.
	1: usb device init failure.
 ********************************************************************/
int USB_HubThread(unsigned char port);

/********************************************************************
USB_CheckUsbDevType - Check USB device whether it is UDISK.

 Supports: T630x
 
 Parms:
 	port  - number of USB port.
 
 Returns:
	0: 	usb device is a UDISK.
	1: 	usb device is a JOYSTICK.
	2:	usb device is a MOUSE.
	3:	usb device is a KEYBOARD.
	4:	usb device is a Hardware disk.
	-1:	unknown usb device.
 ********************************************************************/
int USB_CheckDevType(unsigned char port);

/********************************************************************
 USB_GetKeyCode - Get the usb keyboard scancode.

 Supports: T630x
 
 Parms: 	none.
 
 Returns: pointer to the data buffer.
	
 ********************************************************************/
unsigned char* USB_GetKeyCode();

/********************************************************************
 USB_GetMouseData - Get the usb mouse data.

 Supports: T630x
 
 Parms: 	none.
 
 Returns: pointer to the data buffer.
	
 ********************************************************************/
unsigned char* USB_GetMouseData();

/********************************************************************
USB_ISR_Handle - Operate USB interrupt.

 Supports: T630x
 
 Parms:
 	None.
 
 Returns:
	None.
 ********************************************************************/
void USB_ISR_Handle();

enum USB_COMMAND_STATUS
{
	USB_CMD_SUCCESS=0,
	USB_CMD_FAILEd
};


/********************************************************************
USB_GetMaxLun - Check maximal sub-deivce number and 
 			initialize sub-device.

 Supports: T630x
 
 Parms:
 	port  - number of USB port.
 
 Returns:
 	success return number of the device's LUN.
 	failure 0.
 ********************************************************************/
int USB_GetMaxLun(unsigned char port);


/********************************************************************
USB_CheckSubDeviceExist - Check sub-device exist or not.

 Supports: T630x
 
 Parms:
 	port  - number of USB port.
 	lun   - sub-device port number.
 
 Returns:
 	exist 	 1
 	no exist 0.
 ********************************************************************/
int USB_CheckSubDeviceExist(unsigned char port,unsigned char lun);


/********************************************************************
USB_GetPartitionDiskSize - Get the size of partition.

 Supports: T630x
 
 Parms:
 	port  - number of USB port.
 	lun   - sub-device port number.
 
 Returns:
 	return size of partition.
 ********************************************************************/
int USB_GetPartitionDiskSize(unsigned char port, unsigned char lun);

/********************************************************************
USB_SetDiskPartitionInfo - Set partition's information.

 Supports: T630x
 
 Parms:
 	port  - number of USB port.
 	lun   - sub-device port number.
 	parInfo - pointer to array of structure which store information about per partition table.
 
 Returns:
 	1 failure. 0 success.
 ********************************************************************/
int USB_SetDiskPartitionInfo(unsigned char port,unsigned char lun, struct partition_struct *parInfo);

/********************************************************************
USB_DiskPartition - Build partition's table

 Supports: T630x
 
 Parms:
 	port  - number of USB port.
 	lun   - sub-device port number.
 
 Returns:
 	1 failure. 0 success.
 ********************************************************************/
int USB_DiskPartition(unsigned char port, unsigned char lun);

/********************************************************************
SCSI_Read_Lba - 
 Description: Send command to USB disk through SCSI interface.

 Supports: T630x
 
 Parms:
 	port  - port number which connect with UDISK device.
 	lun	- subport number of the UDISK device.
 	lbn  - number of sector in which will be start to read.
 	buff - address which point data received buf.
 	cbwcb_length - size of the data.(size by sector, 1 sector = 512byte)
 	
 Returns:
	0: success.
	1: failure.
 ********************************************************************/
int USB_Read_Lba(unsigned char port, unsigned char lun, unsigned long lbn, void *buff,unsigned long cbwcb_length);

/********************************************************************
SCSI_Write_Lba - 
 Description: Send command to USB disk through SCSI interface.

 Supports: T630x
 
 Parms:
 	port  - port number which connect with UDISK device.
 	lun	- subport number of the UDISK device.
 	lbn  - number of sector in which will be start to read.
 	buff - address which point data sended buf.
 	cbwcb_length - size of the data.(size by sector, 1 sector = 512byte)
 	
 Returns:
	0: success.
	1: failure.
 ********************************************************************/
int USB_Write_Lba(unsigned char port, unsigned char lun, unsigned long lbn, void *buff,unsigned long cbwcb_length);

/********************************************************************
 USB_Disable - disable USB operation.

 Supports: T630x
 
 Parms:
 	none
 
 Returns: 
 	none
 ********************************************************************/
void USB_Operation_Disable();

#ifdef __cplusplus
}
#endif
#endif
