/*====================================================================
*
*  Copyright (C) T2 Inc., Inc.  All Rights Reserved. *
*  File:   mp3enc.h
*
*  This is the interface of MP3encoder for CDDA bitstream.
*
*  History:
*     Date       By          Reason           Ver.
*   ========  =========    ================= ======
*   04/15     Wesharp       Create it         0.1 
*********************************************************************/
#ifndef	MP3_ENC
#define	MP3_ENC
//--------------------------------------------------//
#ifndef BYTE
#define BYTE unsigned char	// 8-bits
#define WORD unsigned short	//16-bits
#define DWORD unsigned long	//32-bits
#endif

#define SWORD short		// signed 16-bits
#define SDWORD long		// signed 32-bits

#define NCHANS       2            // max 2 channels
#define NBLOCK       1152         // MP3 one frame max 1152 sample
#define PCMBUFSIZE   (NBLOCK*NCHANS) // input pcm buffer size (int),  must >= NBLOCK*NCHANS

#define	MONO	0 
#define	STEREO	1

enum F_SRATE
{ 
	FS48=0, 
	FS44, 
	FS32
};
enum A_BITRATE
{ 
	BRFREE=0, 
	BR32, 
	BR40, 
	BR48, 
	BR56, 
	BR64, 
	BR80, 
	BR96, 
	BR112, 
	BR128, 
	BR160, 
	BR192, 
	BR224, 
	BR256, 
	BR320 
};
enum E_STATUS
{ 
	WARNING=0, 
	FATAL 
};				/* Error message types */

/*------------------------------------------------*/
/*              MP3 Encode                        */
/*------------------------------------------------*/
typedef struct _ENCINSIP_MP3
{	SDWORD *PCM_ptr;
	WORD PCM_length;
	BYTE quanword;
	BYTE bitrate;
	BYTE samprate;
	BYTE stereo_mode;
} ENCINSIP_MP3;

typedef struct _ENCOUTSIP_MP3
{	BYTE *bs_ptr;
	WORD bs_length;
	BYTE status;
} ENCOUTSIP_MP3;

void MP3Encoder_Init();
ENCOUTSIP_MP3  MP3Encoder(ENCINSIP_MP3 mp3_parameter);
#endif
