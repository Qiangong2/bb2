/***************************************************************************
	BIOS - Application library - System library defines
	T-Square, Zhuhai
	Started by Bagio, 2002-10-28
 ***************************************************************************/

#ifndef __BIOS_SYS_DEFINES_H
#define __BIOS_SYS_DEFINES_H

#ifdef __cplusplus
extern "C" {
#endif

// Model of the chip
enum BIOS_MODEL
{
	BIOS_MODEL_6301 = 0x6301,
	BIOS_MODEL_6303 = 0x6303,
	BIOS_MODEL_6304 = 0x6304,
	BIOS_MODEL_6302 = 0x6302,
	BIOS_MODEL_6305 = 0x6305,
	BIOS_MODEL_6306 = 0x6306
};

// Audio DAC type (only use on the DSP Audio library)
// DAC_AC97
#define _DAC_ALC650		0x01
#define _DAC_ALC658		0x02
	
// DAC_I2S
#define _DAC_CS4340		0x11
#define _DAC_1602		0x12
#define _DAC_PCM1738	0x13


// The struct for Audio interface
typedef struct _BIOS_AUDIO_INTERFACE
{
	unsigned short fs;
	unsigned short i2s_ctrl;
} BIOS_AUDIO_INTERFACE, *PBIOS_AUDIO_INTERFACE;

#ifdef __cplusplus
}
#endif

#endif
