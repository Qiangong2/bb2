/*****************************************************************************
	TV Encoder library
 *****************************************************************************/ 
#ifndef _BIOS_TVE_H
#define _BIOS_TVE_H
 
enum TV_MODE
{
	PAL = 0,
	NTSC = 1
};

enum TV_VIDEODATA_MODES
{
	VDM_ITU_BT656 = 0,
	VDM_ITU_BT601 = 1,
	VDM_ITU_BT1358= 2
};

enum TV_VIDEODATA_FORMAT
{
	VDF_CbYCrY = 0,
	VDF_YCrYCb = 1,
	VDF_CrYCbY = 2,
	VDF_YCbYCr = 3
};

enum TV_VIDEOOUT_MODES
{
	VOM_SVideo = 0,
	VOM_CVBS   = 1, 
	VOM_RGB    = 2,
	VOM_YUV    = 3
};

enum TV_VIDEOSCAN_MODES
{
	VSM_Interlace   = 0,
	VSM_Progressive = 1, 
};


enum TV_MACROVISIONTYPES
{
    MVT_TYPE0 = 0,
    MVT_TYPE1 = 1,
    MVT_TYPE2 = 2,
    MVT_TYPE3 = 3
};

/*****************************************************************************
 BIOS_TVE_Init - Reset the TV encoder
 
 Supports: T6305/6
 
 Notes:
   This function is designed for being called by TS630x but should be 
   implemented with a certain TV encoder such as  M3325 and Winbond. After
   reset, the TV mode of the encoder should be NTSC.
 *****************************************************************************/
void BIOS_TVE_Init(void);

/*****************************************************************************
 BIOS_TVE_SetTVMode - Reset the TV encoder
 
 Supports: T6305/6
 
 Params: 
    tvMode - TV mode. 

 Notes:
	This function must do its work in a very short period of time (because it 
    will be called within vblank).
 *****************************************************************************/
void BIOS_TVE_SetTVMode(char tvMode); 

/*****************************************************************************
 BIOS_TVE_GetVideoDataMode - Get video data mode
 
 Supports: T6305/6
 
 Params: 
    None

 Return:
	see enum TV_VIDEODATA_MODES 
 *****************************************************************************/
char BIOS_TVE_GetVideoDataMode(void);

/*****************************************************************************
 BIOS_TVE_GetVideoDataFormat - Return the required video data format
 
 Supports: T6305/6
 
 Params: 
    None

 Return:
	see enum TV_VIDEODATA_FORMAT 
 *****************************************************************************/
char BIOS_TVE_GetVideoDataFormat(void);

/*****************************************************************************
 BIOS_TVE_GetVideoSyncMode - Return the required video sync mode
 
 Supports: T6305/6
 
 Params: 
    None

 Return:
	1, if TVE works in master mode; 0, if TVE works in slave mode 
 *****************************************************************************/
char BIOS_TVE_GetVideoSyncMode();

/*****************************************************************************
 BIOS_TVE_SetVideoOutput - Set video output mode

 Supports: T6305/6 Internal TVE

 Params:
    VideoOutpMode  - Video output mode (use VOM_* macros)

 Returns:
    0 if ok, 1 if video output mode is not supported
******************************************************************************/
int BIOS_TVE_SetVideoOutput(int VideoOutpMode);

/*****************************************************************************
 BIOS_TVE_SetVideoScanMode - Set scan mode

 Supports: T6305/6 Internal TVE

 Params:
    VideoScanMode  - Video scan mode (use VSM_* macros)

 Returns:
    0 if ok, 1 if video scan mode is not supported
******************************************************************************/
int BIOS_TVE_SetVideoScanMode (int VideoScanMode);

/*****************************************************************************
 BIOS_TVE_GetAxisAdjustment - Get Axis Adjustment

 Supports: T6305/6

 Params:
    tvMode - TV mode, PAL or NTSC, refer the macros.

 Returns:
    None.
******************************************************************************/
void BIOS_TVE_GetAxisAdjustment(int tvMode, int *offsetX, int *offsetY);

/*****************************************************************************
 BIOS_TVE_OutputBlkPic - Make the TVE output black picture or not

 Supports: T6305/6 Internal TVE

 Params:
    IsBlack - 1, output black picture; 0, do not output black picture.

 Returns:
    None.
******************************************************************************/
void BIOS_TVE_OutputBlkPic(int IsBlack);

/*****************************************************************************
 BIOS_TVE_PowerDAC - Power up or down DACs of the  internal TV encoder

 Supports: T6305/6 Internal TVE

 Params:
    power - 0: power down; 1: power up

 Returns:
    None.
******************************************************************************/
void BIOS_TVE_PowerDAC(int power);

/*****************************************************************************
 BIOS_TVE_SetMacrovision - Set Macrovision type

 Supports: T6305/6 Internal TVE

 Params:
    MvType - Macrovision type, see macros MVT_TYPE*

 Returns:
    None.
******************************************************************************/
void BIOS_TVE_SetMacrovision(int MvType);

#endif