/***************************************************************************
	BIOS - Application library
	T-Square, Zhuhai
	Started by Bagio, 2002-10-28
 ***************************************************************************

This library provides the API used by the application to access the 
BIOS. It mainly consists of an installation function that must be called
at startup, and the thunkers to call the functions with a normal C 
interface.

 ***************************************************************************/

#ifndef __BIOS_H
#define __BIOS_H

/********************************************************************
	Library includes
 ********************************************************************/

// Include sub-library header files, with thunkers
#include "BIOS_sys.h"
#include "BIOS_tve.h"
#include "BIOS_panel.h"
#include "BIOS_flash.h"
#include "BIOS_cdr.h"
#include "BIOS_irc.h"
#include "BIOS_pm.h"
//#include "BIOS_commonhead.h"

#ifdef __cplusplus
extern "C" {
#endif

/********************************************************************
	Functions
 ********************************************************************/

/********************************************************************
 BIOS_Install - Install the BIOS, at a given free memory 
    position

 Supports: T6305/6

 Params:
    address           - Physical address within RAM where the BIOS can
	                    be installed, the size is 128K or 256K bytes
    tlbIndex          - Index of the TLB to use
    addressTmpMem     - Address of a temporary area of RAM used
                        during the installation, and the size must 
						at least 130K bytes.


 Returns:
    1 if ok, 0 if the BIOS is not present in the flash

 Notes:
	there are different sizes for BIOS, 128Kbyte and 256Kbyte, and we
	can get the BIOS size information from every BIOS release name. For
	example name like: bios_xxx_128kb.obj or bios_xxx_256kb.obj. 
	so we have 2 bios.a files. One for 128kb (called bios_128kb.a) and 
	one for 256kb (called bios_256kb.a). the application should link 
	correct bios_xxxkb.a file based on the size on the 
	bios_..._xxxkb.obj file.

    The application must ensure that at least 130K bytes of free memory
	are available at the given address. This function must be called
	before calling any other BIOS function.
    BIOS requires the use of a TLB entry, so you have to specify
	an index which is not used by your application.
	The temporary area of RAM is used only during the installation
	procedure, and can be reused by the application after it. The 
	area must be at least 130K big. After BIOS_Install(), the area is 
	always clear (0 fill).
 ********************************************************************/
int BIOS_Install(unsigned long address, int tlbIndex, unsigned long addressTmpMem);


#ifdef __cplusplus
}
#endif

#endif

