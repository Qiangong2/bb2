/***************************************************************************
	BIOS - Application library - CD Library defines
	T-Square, Zhuhai
	Started by Bagio, 2002-10-28
 ***************************************************************************/

#ifndef __BIOS_CDR_DEFINES_H
#define __BIOS_CDR_DEFINES_H

#ifdef __cplusplus
extern "C" {
#endif

/***************************************************************************
	Enums and structures
 ***************************************************************************/ 

// Table of contents
typedef struct {
	unsigned char ADRCTL,TrkNo;
	unsigned char mm,ss,ff;
} TRK_DSCP;

typedef struct _CDR_TOC{
	unsigned char DiskType;
	unsigned char FirstTrk;
	unsigned char LastTrk;
	unsigned char LeadOut[3];
	TRK_DSCP TrkDscp[100];
} BIOS_CDR_TOC;



// Macro-function to conver MSF format into LBA
#define MSF2LBA(m,s,f)	 (((m)*60L + (s))*75L + (f) - 150L)

// Current status of the tray
enum BIOS_CDR_TRAY_STATUS
{
	BIOS_CDR_IS_OPEN = 1,
	BIOS_CDR_IS_CLOSED,
	BIOS_CDR_IS_OPENING,
	BIOS_CDR_IS_CLOSING,
	BIOS_CDR_IS_PUSHED
};

// Action that can be performed on the tray
enum BIOS_CDR_TRAY_ACTION
{
	BIOS_CDR_TRAY_OPEN = 1,
	BIOS_CDR_TRAY_CLOSE = 0
};

// CD reading speed
enum BIOS_CDR_SPEED
{
	BIOS_CDR_SPEED_1X = 1,
	BIOS_CDR_SPEED_2X = 2,

	// Maximum speed available for the given hardware
	BIOS_CDR_SPEED_MAX = 0xFFFFFFFF
};

// Reading modes
enum BIOS_CDR_DATAMODE
{
	BIOS_CDR_MODE_2048 = 1,
	BIOS_CDR_MODE_2324 = 2,
	BIOS_CDR_MODE_2352 = 3,
	BIOS_CDR_MODE_EMU32 = 4,

	// Added for M6304 VCD/DVD decoder
	BIOS_CDR_MODE_2048DC,
	BIOS_CDR_MODE_2352DC
};

// Reading results
enum BIOS_CDR_READ_RESULT
{
	BIOS_CDR_READ_FAILURE = 0,
	BIOS_CDR_READ_SUCCESS = 1,
	BIOS_CDR_READ_CRC_ERROR = 2
};

/***************************************************************************
   Compatible defined names
 **************************************************************************/

// Current status of the tray
#define CDR_IS_OPENED           BIOS_CDR_IS_OPEN
#define CDR_IS_CLOSED           BIOS_CDR_IS_CLOSED
#define CDR_IS_OPENING          BIOS_CDR_IS_OPENING
#define CDR_IS_CLOSING          BIOS_CDR_IS_CLOSING
#define CDR_IS_PUSHED           BIOS_CDR_IS_PUSHED

// Action that can be performed on the tray
#define CDR_TRAY_OPEN           BIOS_CDR_TRAY_OPEN
#define CDR_TRAY_CLOSE          BIOS_CDR_TRAY_CLOSE

// CD reading speed
#define CDR_SPEED_1X            BIOS_CDR_SPEED_1X
#define CDR_SPEED_2X            BIOS_CDR_SPEED_2X
#define	CDR_SPEED_MAX			BIOS_CDR_SPEED_MAX

// Reading modes
#define CDR_MODE_2048           BIOS_CDR_MODE_2048
#define CDR_MODE_2324           BIOS_CDR_MODE_2324
#define CDR_MODE_2352           BIOS_CDR_MODE_2352
#define CDR_MODE_EMU32          BIOS_CDR_MODE_EMU32
#define CDR_MODE_2048DC         BIOS_CDR_MODE_2048DC
#define CDR_MODE_2352DC         BIOS_CDR_MODE_2352DC

// Reading results
#define CDR_READ_FAILURE        BIOS_CDR_READ_FAILURE
#define CDR_READ_SUCCESS        BIOS_CDR_READ_SUCCESS
#define CDR_READ_CRC_ERROR      BIOS_CDR_READ_CRC_ERROR

#define CDR_TOC					BIOS_CDR_TOC

/***************************************************************************
   Added for DVD play
 **************************************************************************/

#define DVD_Physical      0x00
#define DVD_Copyright     0x01
#define DVD_DiskKey       0x02
#define DVD_BCA           0x03
#define DVD_Manufacturer  0x04
#define DVD_DDS           0x08

// define report key format field
#define KEY_AGID          0x00
#define KEY_ChallengeKey  0x01
#define KEY_Key1          0x02
#define KEY_Key2          0x03
#define KEY_TitleKey      0x04
#define KEY_ASF           0x05
#define KEY_RPCStruct     0x06
#define KEY_RPCState      0x08
#define KEY_None          0x3F

typedef enum
{
	CD_DataOnly = 0,
	CD_CDDA = 1,
	CD_DataAudioMixed = 2,
	CD_Photo = 3,
	DVD_Medium = 4,
	NoDiskPresent = 5,
	DiscTrayOpen = 6,
	MediumFormatError = 7,
	MediumTypeUnknown = 8,
	MediumTypeNotReady = 9, 
	MediumTypeMax
} MediumType;

typedef struct _FULLTOC_TRK_DSCP
{
	unsigned char SessionNo;
	unsigned char ADRCTL;
	unsigned char TNO;
	unsigned char POINT;
	unsigned char Min;
	unsigned char Sec;
	unsigned char Frame;
	unsigned char Zero;
	unsigned char PMin;
	unsigned char PSec;
	unsigned char PFrame;
}FULLTOC_TRK_DSCP;

typedef struct _FULL_TOC
{
	unsigned short DataLen;
	unsigned char FirstSession;
	unsigned char LastSession;
	FULLTOC_TRK_DSCP TrakDscp[150];
}FULL_TOC, *LPFULL_TOC;

typedef union _OPTIC_DISC_TOC
{
    struct _CDR_TOC;
    struct _FULL_TOC;
}OPTIC_DISC_TOC, *LPOPTIC_DISC_TOC;

typedef struct _CDTXT_DSCP
{
    unsigned char PackType;
    unsigned char TrackNumber:7;
    unsigned char EF:1;
    unsigned char SequenceNumber;
    unsigned char CharacterPosition:4;
    unsigned char BlockNumber:3;
    unsigned char DBCC:1;
    unsigned char TextData[12];
    unsigned char CRC0;
    unsigned char CRC1;
} CDTXT_DSCP;

typedef struct _CD_TEXT
{
	unsigned short DataLen;
	unsigned short Reserved;
	CDTXT_DSCP TextDscp[2048];
} CD_TEXT;


#ifdef __cplusplus
}
#endif

#endif
