/***************************************************************************
	BIOS - Application library - MemCard library
	T-Square, Zhuhai
	Started by Bagio, 2002-10-28
 ***************************************************************************/

#ifndef __BIOS_FLASH_H
#define __BIOS_FLASH_H

#ifdef __cplusplus
extern "C" {
#endif

/********************************************************************
	Functions
 ********************************************************************/

/********************************************************************
BIOS_Flash_Read --
Description: Read data from the Flash chip.
Supports: T6305/6.
Params:
     buff:              	Buffer that points to data source.
     flashaddress: Reading address.
     size:               Reading data size (in bytes).
Returns:
 	1: Success.
	0: Fail. 
    	-1: It can't switch on the flash enable mode. (I think it unhappen.)
Notes:
      Before accessing Flash, the application needs to wait all of the share pin device in idle.
********************************************************************/
int  BIOS_Flash_Read (void* buff, void *flashaddress, unsigned int size);

/********************************************************************
BIOS_Flash_Write --
Description: Write data to the Flash chip.
Supports: T6305/6.
Params:
     buff:              	Buffer that points to data source.
     flashaddress: Writing address.
     size:               Writing data size (in bytes).
Returns:
	 1: Success.
	 0: Fail.
    	 -1: It can't switch on the flash enable mode.
Notes:
      Before accessing Flash, the application needs to wait for all of the share pin device to be in idle.
********************************************************************/
int  BIOS_Flash_Write (void* buff, void *flashaddress, unsigned int size);

#ifdef __cplusplus
}
#endif

#endif
