#ifndef __BIOS_VKEYS_H_
#define __BIOS_VKEYS_H_

#define BIOS_VKEY_NULL              0xFF
#define BIOS_VKEY_POWER             0
#define BIOS_VKEY_OPEN              1
#define BIOS_VKEY_PLAY              2
#define BIOS_VKEY_STOP              3
#define BIOS_VKEY_STEP              4
#define BIOS_VKEY_LAST_PLAY         5
#define BIOS_VKEY_SKIP_L            6
#define BIOS_VKEY_SKIP_R            7
#define BIOS_VKEY_FF                8
#define BIOS_VKEY_REV               9
#define BIOS_VKEY_UP                10
#define BIOS_VKEY_DOWN              11
#define BIOS_VKEY_LEFT              12
#define BIOS_VKEY_RIGHT             13
#define BIOS_VKEY_MENU              14
#define BIOS_VKEY_TITLE             15
#define BIOS_VKEY_ANGLE             16
#define BIOS_VKEY_WARM_START        17
#define BIOS_VKEY_COLD_START        18
#define BIOS_VKEY_DIAGNOSTIC        19
#define BIOS_VKEY_DISPLAY           20  //OSD
#define BIOS_VKEY_ENTER             21
#define BIOS_VKEY_DVIEW             22
#define BIOS_VKEY_SETUP             23
#define BIOS_VKEY_RESUME            24
#define BIOS_VKEY_RANDOM            25
#define BIOS_VKEY_ZOOM              26
#define BIOS_VKEY_SEARCH            27
#define BIOS_VKEY_SLOW              28
#define BIOS_VKEY_SUBTITLE          29
#define BIOS_VKEY_SUBP_ON_OFF       30
#define BIOS_VKEY_VOICE             31   //switch language when playing dvd
#define BIOS_VKEY_REPEAT            32
#define BIOS_VKEY_AB_REPEAT         33
#define BIOS_VKEY_KARAOKE_ONOFF     34
#define BIOS_VKEY_MODE              35
#define BIOS_VKEY_KEYUP             36
#define BIOS_VKEY_KEYDOWN           37
#define BIOS_VKEY_AUDIO             38   // audio output mode ,ch51,ch2,stereo
#define BIOS_VKEY_VOCAL             39   // switch voice channel when playing dvd
#define BIOS_VKEY_CLEAR             40
#define BIOS_VKEY_0                 41
#define BIOS_VKEY_1                 42
#define BIOS_VKEY_2                 43
#define BIOS_VKEY_3                 44
#define BIOS_VKEY_4                 45
#define BIOS_VKEY_5                 46
#define BIOS_VKEY_6                 47
#define BIOS_VKEY_7                 48
#define BIOS_VKEY_8                 49
#define BIOS_VKEY_9                 50
#define BIOS_VKEY_10                51
#define BIOS_VKEY_PROGRAM           52
#define BIOS_VKEY_EQ                53
#define BIOS_VKEY_MICVALUP          54
#define BIOS_VKEY_MICVALDOWN        55
#define BIOS_VKEY_MUTE              56
#define BIOS_VKEY_LANGU             57  //to be canceled 
#define BIOS_VKEY_KARAECHO_ONOFF    58
#define BIOS_VKEY_ECHOUP            59
#define BIOS_VKEY_ECHODOWN          60
#define BIOS_VKEY_ASSIST            61
#define BIOS_VKEY_VOLUP             62
#define BIOS_VKEY_VOLDOWN           63
#define BIOS_VKEY_PBC_ON            64
#define BIOS_VKEY_PBC_OFF           65
#define BIOS_VKEY_PBC_ONOFF         66
#define BIOS_VKEY_GAME              68
#define BIOS_VKEY_VENDER_SETUP      69
#define BIOS_VKEY_SHOWTIME          70
#define BIOS_VKEY_VOLUM             71
#define BIOS_VKEY_DEBUG_M3323REG    72
#define BIOS_VKEY_PAUSE             73

#define BIOS_VKEY_CLOSE             75
#define BIOS_VKEY_LOAD              76
#define BIOS_VKEY_CONNECT           77
#define BIOS_VKEY_SWITCH	        78
#define BIOS_VKEY_RETURN            79
#define BIOS_VKEY_TV_SYS            80  //PAL,NTSC ,etc
#define BIOS_VKEY_SETUPDEFAULT      81
#define BIOS_VKEY_DOLBY_SETUP       82
#define BIOS_VKEY_MACROVISION       83
#define BIOS_VKEY_AUDIOEFFECT       84
#define BIOS_VKEY_TITLE_PBCONOFF    85
#define BIOS_VKEY_ENTERPLAY         86
#define BIOS_VKEY_SCANMODE          87
#define BIOS_VKEY_VIDEO             88  //video output ,such as yuv,rgb,s-video,etc
#define BIOS_VKEY_NETWORK	        89

#define BIOS_VKEY_RECMODE	        91  //added at dong guan


#define BIOS_VKEY_VIEWAUDIO		    90 //HOTKEY
#define BIOS_VKEY_VIEWVIDEO		    92 //HOTKEY
#define BIOS_VKEY_VIEWPICTURE		93 //HOTKEY
#define BIOS_VKEY_VIEWDISC          106//HOTKEY
#define BIOS_VKEY_PAGEUP		    94
#define BIOS_VKEY_PAGEDOWN		    95

#define BIOS_VKEY_WEBHOME		    96
#define BIOS_VKEY_WEBBACK		    97
#define BIOS_VKEY_WEBNEXT		    98
#define BIOS_VKEY_WEBREFRESH		99	
#define BIOS_VKEY_WEBSTOP		    100

#define BIOS_VKEY_DEVICE_MODE	  101
#define BIOS_VKEY_KEY_BOARD		  102
#define BIOS_VKEY_INPUT_MODE      103
#define BIOS_VKEY_ZOOM_IN         104
#define BIOS_VKEY_ZOOM_OUT        105

#define BIOS_VKEY_FAST_TEXT_R     107
#define BIOS_VKEY_FAST_TEXT_G     108
#define BIOS_VKEY_FAST_TEXT_B     109
#define BIOS_VKEY_FAST_TEXT_Y     110
#define BIOS_VKEY_SCREEN_SCALE    111   //4:3,16:9,etc

/*------c---------------bt------a----------------------n*/
#define BIOS_VKEY_SYSTEM_RESET    112
#define BIOS_VKEY_FUNCTION	      113
#define BIOS_VKEY_MEDIABAR	      114
#define BIOS_VKEY_DASH		      115
#define BIOS_VKEY_SLASH		      116
#define BIOS_VKEY_MOUSE		      117
#define BIOS_VKEY_UNDERLINE	      118
#define BIOS_VKEY_DOT		      119
//----
#define BIOS_VKEY_A		    121
#define BIOS_VKEY_B		    122
#define BIOS_VKEY_C		    123

#define BIOS_VKEY_D		    124
#define BIOS_VKEY_E		    125
#define BIOS_VKEY_F		    126

#define BIOS_VKEY_G		    127
#define BIOS_VKEY_H		    128
#define BIOS_VKEY_I		    129

#define BIOS_VKEY_J		    130
#define BIOS_VKEY_K		    131
#define BIOS_VKEY_L		    132

#define BIOS_VKEY_M		    133
#define BIOS_VKEY_N		    134
#define BIOS_VKEY_O		    135

#define BIOS_VKEY_P		    136
#define BIOS_VKEY_Q		    137
#define BIOS_VKEY_R		    138
#define BIOS_VKEY_S		    139

#define BIOS_VKEY_T		    140
#define BIOS_VKEY_U		    141
#define BIOS_VKEY_V		    142

#define BIOS_VKEY_W		    143
#define BIOS_VKEY_X		    144
#define BIOS_VKEY_Y		    145
#define BIOS_VKEY_Z		    146

//-------------------extend vkey---------------------------------------//
#define BIOS_VKEY_EXTENDED0       150
#define BIOS_VKEY_EXTENDED1       151
#define BIOS_VKEY_EXTENDED2		  152
#define BIOS_VKEY_EXTENDED3		  153
#define BIOS_VKEY_EXTENDED4       154
#define BIOS_VKEY_EXTENDED5		  155
#define BIOS_VKEY_EXTENDED6		  156
#define BIOS_VKEY_EXTENDED7       157
#define BIOS_VKEY_EXTENDED8 	  158
#define BIOS_VKEY_EXTENDED9		  159

#define BIOS_VKEY_EXTENDED10      160
#define BIOS_VKEY_EXTENDED11	  161
#define BIOS_VKEY_EXTENDED12	  162
#define BIOS_VKEY_EXTENDED13      163
#define BIOS_VKEY_EXTENDED14	  164
#define BIOS_VKEY_EXTENDED15	  165
#define BIOS_VKEY_EXTENDED16      166
#define BIOS_VKEY_EXTENDED17	  167
#define BIOS_VKEY_EXTENDED18	  168
#define BIOS_VKEY_EXTENDED19      169

#define BIOS_VKEY_EXTENDED20	  170
#define BIOS_VKEY_EXTENDED21	  171
#define BIOS_VKEY_EXTENDED22	  172
#define BIOS_VKEY_EXTENDED23	  173
#define BIOS_VKEY_EXTENDED24      174
#define BIOS_VKEY_EXTENDED25	  175
#define BIOS_VKEY_EXTENDED26	  176
#define BIOS_VKEY_EXTENDED27      177
#define BIOS_VKEY_EXTENDED28	  178
#define BIOS_VKEY_EXTENDED29	  179

#define BIOS_VKEY_EXTENDED30      180
#define BIOS_VKEY_EXTENDED31      181
#define BIOS_VKEY_EXTENDED32      182
#define BIOS_VKEY_EXTENDED33	  183
#define BIOS_VKEY_EXTENDED34      184
#define BIOS_VKEY_EXTENDED35	  185
#define BIOS_VKEY_EXTENDED36	  186
#define BIOS_VKEY_EXTENDED37      187
#define BIOS_VKEY_EXTENDED38 	  188
#define BIOS_VKEY_EXTENDED39	  189

#define BIOS_VKEY_EXTENDED40      190
#define BIOS_VKEY_EXTENDED41	  191
#define BIOS_VKEY_EXTENDED42	  192
#define BIOS_VKEY_EXTENDED43      193
#define BIOS_VKEY_EXTENDED44	  194
#define BIOS_VKEY_EXTENDED45	  195
#define BIOS_VKEY_EXTENDED46      196
#define BIOS_VKEY_EXTENDED47	  197
#define BIOS_VKEY_EXTENDED48	  198
#define BIOS_VKEY_EXTENDED49      199

#define BIOS_VKEY_EXTENDED50	  200
#define BIOS_VKEY_EXTENDED51	  201
#define BIOS_VKEY_EXTENDED52	  202
#define BIOS_VKEY_EXTENDED53	  203
#define BIOS_VKEY_EXTENDED54      204
#define BIOS_VKEY_EXTENDED55	  205
#define BIOS_VKEY_EXTENDED56	  206
#define BIOS_VKEY_EXTENDED57      207
#define BIOS_VKEY_EXTENDED58	  208
#define BIOS_VKEY_EXTENDED59	  209

#define BIOS_VKEY_EXTENDED60      210
#define BIOS_VKEY_EXTENDED61	  211
#define BIOS_VKEY_EXTENDED62	  212
#define BIOS_VKEY_EXTENDED63	  213
#define BIOS_VKEY_EXTENDED64      214
#define BIOS_VKEY_EXTENDED65	  215
#define BIOS_VKEY_EXTENDED66	  216
#define BIOS_VKEY_EXTENDED67      217
#define BIOS_VKEY_EXTENDED68	  218
#define BIOS_VKEY_EXTENDED69	  219

#define BIOS_VKEY_EXTENDED70      220
#define BIOS_VKEY_EXTENDED71      221
#define BIOS_VKEY_EXTENDED72      222
#define BIOS_VKEY_EXTENDED73	  223
#define BIOS_VKEY_EXTENDED74      224
#define BIOS_VKEY_EXTENDED75	  225
#define BIOS_VKEY_EXTENDED76	  226
#define BIOS_VKEY_EXTENDED77      227
#define BIOS_VKEY_EXTENDED78 	  228
#define BIOS_VKEY_EXTENDED79	  229

#define BIOS_VKEY_EXTENDED80      230
#define BIOS_VKEY_EXTENDED81	  231
#define BIOS_VKEY_EXTENDED82	  232
#define BIOS_VKEY_EXTENDED83      233
#define BIOS_VKEY_EXTENDED84	  234
#define BIOS_VKEY_EXTENDED85	  235
#define BIOS_VKEY_EXTENDED86      236
#define BIOS_VKEY_EXTENDED87	  237
#define BIOS_VKEY_EXTENDED88	  238
#define BIOS_VKEY_EXTENDED89      239

#define BIOS_VKEY_EXTENDED90	  240
#define BIOS_VKEY_EXTENDED91	  241
#define BIOS_VKEY_EXTENDED92	  242
#define BIOS_VKEY_EXTENDED93	  243
#define BIOS_VKEY_EXTENDED94      244
#define BIOS_VKEY_EXTENDED95	  245
#define BIOS_VKEY_EXTENDED96	  246
#define BIOS_VKEY_EXTENDED97      247
#define BIOS_VKEY_EXTENDED98	  248
#define BIOS_VKEY_EXTENDED99	  249

#define BIOS_VKEY_EXTENDED100     250

#define BIOS_VKEY_EJECT				BIOS_VKEY_OPEN
#define BIOS_INTERNAL_VKEY_OPEN     0x1000

#endif

