#ifndef IRC_INIT_H
#define IRC_INIT_H
/********************************************************************
BIOS_IRC_Init --
Description: Initialize IRC input.
Supports: T6305/6.
       Params: None.
 Returns:
 1: Success.
 0: Fail.
 ********************************************************************/
int BIOS_IRC_Init(void);

/********************************************************************
BIOS_IRC_ISR --
Description: Receive the remote controller signal in an Interrupt Requested. 
Supports: T6305/6.
Params: None.
Returns:
	 (-1): no IR key pressed.
 other value: an IR key value.
 ********************************************************************/
void BIOS_IRC_ISR (void);
 
/********************************************************************
BIOS_IRC_GetKeyCode --
Description: Get the pressed key on IRC.
Supports: T6305/6.
Params: None.
Returns:
     0: No key has been pressed, 
 other value: the virtual code of the key (use BIOS_VKEY_* macros).
 ********************************************************************/
unsigned int  BIOS_IRC_GetKeyCode (void);

#endif //IRC_INIT_H
