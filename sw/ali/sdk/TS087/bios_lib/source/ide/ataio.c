/*************************************************************************
*
*  Copyright (C) 2001-2003 T2-Design Corp.  All rights reserved.
*  File:   ataio.c
*
*  Contents: ATA IO base control process.
*
*************************************************************************/ 

#define LOG_ENABLE		(1)		// if 0 disable all logs of this module

#include "common.h"
#include "ataio.h"


#if (IDE_TYPE == M3325_IDE)

/********************************************************************
				UP Interface: Communication With M3325
				=======================================
********************************************************************/
#define REG_CTL			0xb8001c00
#define REG_STAT		0xb8001c01
#define REG_INIMSK		0xb8001c02
#define REG_INISTA		0xb8001c03
#define REG_DATA		0xb8001c04
#define REG_ADDR		0xb8001c06
#define REG_ADD			0xb8001c08
#define REG_LEN			0xb8001c0c
#define REG_ABT			0xb8001c10
#define REG_CST			0xb8001c12

#define MEM_CLOCK_NUM	((SOC_MEM_FREQUENCY-1)*50/1000+1)

static void SetChipBYTE( WORD index, BYTE data )
{
	WRITE_WORD( REG_ADDR, index );
	WRITE_BYTE( REG_DATA, data );
	WRITE_BYTE( REG_CTL, 0xc0 );
	while( READ_BYTE( REG_STAT ) & 0x80 );
}

static BYTE GetChipBYTE(WORD index)
{
	BYTE bTemp;

	WRITE_WORD( REG_ADDR, index );
	WRITE_BYTE( REG_CTL, 0x80 );
	while( READ_BYTE( REG_STAT ) & 0x80 );
	bTemp = READ_BYTE( REG_DATA );

	return bTemp;
}

static void SetChipWORD( WORD index, WORD data )
{
	SetChipBYTE(index, data & 0xff);
	SetChipBYTE(index + 1, (data >> 8) & 0xff);
}

static WORD GetChipWORD(WORD index)
{
	WORD wTemp;

	wTemp = GetChipBYTE(index);
	wTemp |= (WORD)GetChipBYTE(index + 1) << 8;

	return wTemp;
}

static WORD AIO_ReadReg(BYTE regAddr)
{
	BYTE ans = 0;

	SetChipBYTE(0x42, regAddr);
//	delay(10);
    ans = GetChipBYTE(0x40);
//	delay(10);
	return ans;
}

static void AIO_WriteReg(BYTE regAddr, WORD data)
{
	SetChipBYTE(0x42, regAddr);
	SetChipBYTE(0x41, data>>8); 
	SetChipBYTE(0x40, data); 
//	delay(1);
}

void AIO_Init(void)
{
	DWORD tmp_reg;

    *( WORD *) 0xb8001c10 = 0x0500;
    *( BYTE *) 0xb8001c12 = MEM_CLOCK_NUM;

	SetChipBYTE(0x20, 0x08);		//Set to Little Endian,up Port,ATA interface
	tmp_reg=GetChipBYTE(0x20);
	//add according ALI M3323 test	
	SetChipBYTE(0x17,(GetChipBYTE(0x17) & 0xfc) | 0x01); // set DMCKL delay 
	tmp_reg=GetChipBYTE(0x17);
	SetChipWORD(0x70, 0x00c0); 							// set REN delay 2 degree 
	tmp_reg=GetChipWORD(0x70);
	SetChipBYTE(0x72, 0x00);
	tmp_reg=GetChipBYTE(0x72);
	SetChipBYTE(0x73, 0x00);
	tmp_reg=GetChipBYTE(0x73);
	SetChipBYTE(0x74, 0x50); 
	tmp_reg=GetChipBYTE(0x74);
	SetChipBYTE(0x76, 0x00);
	tmp_reg=GetChipBYTE(0x76);
    SetChipBYTE(0x75,0x38);
	tmp_reg=GetChipBYTE(0x75);
	SetChipBYTE(0x75,(GetChipBYTE(0x75) | 0x80));  // initial sdram
    while((GetChipBYTE(0x75) & 0x80) != 0);
    
    SetChipBYTE(0x24, 0xFF);                //Set ATA Write wait count Origin is 0x20
	tmp_reg=GetChipBYTE(0x24);
    SetChipBYTE(0x2F, 0xFF);                //Set ATA Write wait count Origin is 0x20
    tmp_reg=GetChipBYTE(0x2F);

	//dvd-rom setting
	//Default PIO MODE0, SetFeature will change it 
	AIO_SetTiming(PIO_MODE0);

    SetChipBYTE(0x43, 0x00);                //Set to up Host and Normal mode,NOSURE
	SetChipBYTE(0x26,0x40);

}

void AIO_PIORead(WORD RecBytes, BYTE *buff)
{
	WORD i;
	int odd;

	odd = RecBytes % 2;
	if(odd)
		RecBytes--;

	SetChipBYTE(0x26, 0x40); /* PIO Mode, SIFIFO enable */
	SetChipBYTE(0x42, ATAREG_Data);
	for (i=0; i<RecBytes; i+=2)
	{
		buff[i] = GetChipBYTE(0x40);
		buff[i+1] = GetChipBYTE(0x41);
	}
	if(odd)
	{
		buff[i] = GetChipBYTE(0x40);
		GetChipBYTE(0x41);
	}
}

void AIO_PIOWrite(WORD SendBytes, BYTE *buff)
{
	WORD i;
	int odd;

	odd = SendBytes % 2;
	if(odd)
		SendBytes--;

	SetChipBYTE(0x26, 0x40); /* PIO Mode, SIFIFO enable */
	SetChipBYTE(0x42, ATAREG_Data);
	for (i=0; i<SendBytes; i+=2)
	{
        SetChipBYTE(0x41, buff[i+1]);
	    SetChipBYTE(0x40, buff[i]);
	}
	if(odd)
	{
        SetChipBYTE(0x41, 0);
	    SetChipBYTE(0x40, buff[i]);
	}
}

void AIO_DmaStart(WORD RecBytes, DWORD Addr, int SectorSize)
{
	SetChipBYTE(0x26,0x70);
	WRITE_BYTE(REG_INIMSK,0x07);
	WRITE_BYTE(REG_INISTA,0x07);
	*(volatile DWORD *)REG_ADD = Addr;
	*(volatile WORD *)REG_LEN = RecBytes;

	SetChipBYTE(0x43,0x05);
	SetChipBYTE(0x42,ATAREG_Data);
	GetChipBYTE(0x40);
	WRITE_BYTE(REG_ADDR,0x2a);
	WRITE_BYTE(REG_CTL,0xa0);
}

BOOL AIO_IsDmaOver(void)
{
	if ((READ_BYTE(REG_INISTA) & 0x02) == 0x02)
	{
		WRITE_BYTE(REG_INISTA, 0x02);
		return TRUE;
	}
	return FALSE;
}

void AIO_HWReset(void)
{
	SetChipBYTE(0x43, 0x02);		// Reset ATAPI
	delay_ms(2);
	SetChipBYTE(0x43, 0x00);
	delay_ms(10);
}

void AIO_SetDefault(void)
{
	SetChipBYTE(0x43, 0x00);
	// SetChipBYTE(0x26,0x40);	???
}

void AIO_SetTiming(int PIO_Mode)
{
	switch(PIO_Mode)
	{
	case PIO_MODE0:
		SetChipBYTE( 0x45, 0xBA );
		SetChipBYTE( 0x46, 0x0B );
		break;
	case PIO_MODE1:
		SetChipBYTE( 0x45, 0x90 );
		SetChipBYTE( 0x46, 0x08 );
		break;
	case PIO_MODE2:
		SetChipBYTE( 0x45, 0x69 );
		SetChipBYTE( 0x46, 0x07 );
		break;
	case PIO_MODE3:
		SetChipBYTE( 0x45, 0x66 );
		SetChipBYTE( 0x46, 0x06 );
		break;
	case PIO_MODE4:
		SetChipBYTE( 0x45, 0x64 );
		SetChipBYTE( 0x46, 0x05 );
		break;
	default:
		SetChipBYTE( 0x45, 0x69 );
		SetChipBYTE( 0x46, 0x07 );
		break;
	}
	SetChipBYTE(0x24, 0x10);
}

BOOL AIO_CheckInterrupt()
{
    return ((GetChipBYTE(0x23) & 0x01) == 0x01);
}

void AIO_ClearInterrupt()
{
	SetChipBYTE(0x23, 0x01); // Clear M3323 interrupt bit
}

#else

/********************************************************************
						T2-IDE ATA Interface
						====================
********************************************************************/

#define IDE_IOBASE			0xb8008000

// ATA Setting
#define ATA_HWCFG			(IDE_IOBASE + 0x00)		// [2:0]
#define ATA_ASTTIME			(IDE_IOBASE + 0x00)		// [6:4]
#define ATA_ACTTIME			(IDE_IOBASE + 0x01)
#define ATA_RECTIME			(IDE_IOBASE + 0x02)
#define ATA_PAR				(IDE_IOBASE + 0x03)

// Select ATA register
#define ATA_ADDR			(IDE_IOBASE + 0x04)
// Write data
#define ATA_WDATA			(IDE_IOBASE + 0x06)

// DMA Setting 
#define DMA_SET				(IDE_IOBASE + 0x08)
#define DMA_SCNT			(IDE_IOBASE + 0x09)
#define DMA_WBS				(IDE_IOBASE + 0x0a)

#define ATA_CTRL			(IDE_IOBASE + 0x0c)
#define BCNT_LO				(IDE_IOBASE + 0x0e)		// [23:16]
#define BCNT_HI				(IDE_IOBASE + 0x0f)		// [31:24]

#define DMA_WSA				(IDE_IOBASE + 0x10)

#define ATA_STATUS_1		(IDE_IOBASE + 0x14)
#define ATA_STATUS_2		(IDE_IOBASE + 0x18)
#define TRI_COUNT			(IDE_IOBASE + 0X18)
#define WORD_VAL			(IDE_IOBASE + 0X1a)

#define MODE_FORM			(IDE_IOBASE + 0x1c)
#define ATA_RDATA			(IDE_IOBASE + 0x1e)


// for ATA_PAR Register setting

#define ATA_FIFO			0x01
#define ATA_RESET			0x02
#define ATA_BURST			0x04
#define PARSING_TRICK		0x08
#define TM_2328				0x00
#define TM_2336				0x10
#define TM_2340				0x20
#define TM_2352				0x40
#define TM_2048				0x30
#define TM_2064				0x50
#define TM_AUTO				0x60

// for DMA_SET Register setting

#define DMA_EN				0x01
#define DMA_IOEN			0x02
#define DMA_RDSTB			0x04

// for ATA Control Register

#define ATA_STOP			0x0001
#define STOP_WAT			0x0002
#define PAU_DMAEN			0x0004
#define TRICN_RST			0x0008
#define FIFO_FLUSH			0x0010

#define ERR_TRIG			0x0100
#define ERR_FORM			0x0200
#define BCNT_STB			0x0400

// for ATA Status Register 1

#define ATASTA				//	0-4
#define ATA_ACT				(1 << 5)
#define ATATSND				(1 << 6)
#define ATABKRDY			(1 << 7)
#define ATAINTRQ			(1 << 8)
#define AU_PAUSE			(1 << 9)
#define CMD_ACT				(1 << 10)
#define CMD_AST				(1 << 11)
#define CMD_REC				(1 << 12)
#define DMA_OVER			(1 << 13)
#define INT_FLAG			(1 << 14)
#define DMARECNT			//	16-23
#define DMASTA				//	24-27
#define DMAREAD				(1 << 28)
#define P_DMARQ				(1 << 29)
#define FIFOEMPTY			(1 << 30)
#define FIFOFULL			(1 << 31)

// for Mode & Form register

#define CD_MODE				//	0-1
#define CD_FORM				//	4-5
#define TRIGDIF				0x0100
#define FORMDIF				0x0200


static WORD AIO_ReadReg(BYTE regAddr)
{
	WRITE_BYTE(ATA_ADDR, regAddr);
	// CLEAR_B_BIT(ATA_PAR, ATA_FIFO);

	return READ_WORD(ATA_RDATA);
}

static void AIO_WriteReg(BYTE regAddr, WORD data)
{
	WRITE_BYTE(ATA_ADDR, regAddr);
	WRITE_WORD(ATA_WDATA, data);
}

void AIO_Init(void)
{
	// Reset IDE device
	HW_RESET_IDE();
	
	AIO_HWReset();

#if (0)
	// Read all registers values, to ensure the default values are correct.

	TRACE("\nATA Registers default:\n");
	TRACE1("ATA set        Register(0x0b040537) = 0x%08x\n", READ_DWORD(ATA_HWCFG));
	TRACE1("ATA write&addr Register(0x0000010f) = 0x%08x\n", READ_DWORD(ATA_ADDR));
	TRACE1("DMA set        Register(0xffff0602) = 0x%08x\n", READ_DWORD(DMA_SET));
	TRACE1("ATA ctrl       Register(0x12600004) = 0x%08x\n", READ_DWORD(ATA_CTRL));
	TRACE1("DMA write      Register(0x00000000) = 0x%08x\n", READ_DWORD(DMA_WSA));
	TRACE1("ATA status1    Register(0x40000080) = 0x%08x\n", READ_DWORD(ATA_STATUS_1));
	TRACE1("ATA status2    Register(0x00000000) = 0x%08x\n", READ_DWORD(ATA_STATUS_2));
	TRACE1("ATA read&mode  Register(0x00000000) = 0x%08x\n", READ_DWORD(MODE_FORM));

#endif

	// set HWCFG to "111" for 16 bit bus access

	WRITE_BYTE(ATA_HWCFG, (READ_BYTE(ATA_HWCFG) & 0xf0) | 0x07);

	// Set ACTTIME[4:0], ASTTIME[2:0], RECTIME[4:0] to ATAINTF 
	// to match ATA Read/Write timing

	WRITE_BYTE(ATA_ASTTIME, (READ_BYTE(ATA_ASTTIME) & 0x0f) | 0x30);
	WRITE_BYTE(ATA_ACTTIME, 0x05);
	WRITE_BYTE(ATA_RECTIME, 0x04);

	AIO_SetDefault();
}

void AIO_PIORead(WORD RecBytes, BYTE *buff, WORD BuffLen)
{
	WORD value;

	while (RecBytes >= 2)
	{
		WRITE_BYTE(ATA_ADDR, ATAREG_Data);
		value = READ_WORD(ATA_RDATA);
		if (BuffLen >= 2)
		{
			*(WORD *)buff = value;
			buff += 2;
			BuffLen -= 2;
		}
		else if (BuffLen)
		{
			*buff = value & 0xff;
			BuffLen -= 1;
		}
		RecBytes -= 2;
	}

	if (RecBytes)
	{
		WRITE_BYTE(ATA_ADDR, ATAREG_Data);
		value = READ_WORD(ATA_RDATA);
		if (BuffLen)
			*buff = value & 0xff;
	}
}

void AIO_PIOWrite(WORD SendBytes, BYTE *buff)
{
	WORD i;
	int odd;

	odd = SendBytes % 2;
	if(odd)
		SendBytes--;

	for(i = 0; i < SendBytes; i += 2)
	{
		WRITE_BYTE(ATA_ADDR, ATAREG_Data);
		WRITE_WORD(ATA_WDATA, *(WORD *)(buff + i));
	}
	if(odd)
	{
		WRITE_BYTE(ATA_ADDR, ATAREG_Data);
		WRITE_WORD(ATA_WDATA, buff[i]);
	}
}

void AIO_DmaStart(WORD RecBytes, DWORD Addr, int SectorSize)
{
/*
	T2 IDE DMA mode transfer sequence

  ---------------------------------
	-- Step 1: parameter setting
	---------------------------------
	1. Set ATAPAR [6:4] for sector size
	2. Set ATAPAR [3] =
	'1' for enable parsing trick mode bit,
	'0' for disable parsing trick mode bit.
	3. Set ATAPAR [2] = '1' for enable burst mode.
	4. Set ATAPAR [1] = '0' for not to do reset.
	5. Set ATAPAR [0] = '1' for ATA data to channel FIFO.
	6. Set DMAIODEN = '1' for enable Device Ready Signal.
	7. Set PAUDMAEN = '1' for Pause DMA transfer when FIFO Full.
*/

	SET_W_BIT(ATA_CTRL, TRICN_RST);
	WRITE_BYTE(ATA_PAR, TM_2352 | PARSING_TRICK | ATA_BURST | ATA_FIFO);

	SET_B_BIT(DMA_SET, DMA_IOEN | DMA_EN);
	SET_B_BIT(ATA_CTRL, PAU_DMAEN);
	SET_B_BIT(ATA_CTRL, FIFO_FLUSH);

/*
	--------------------------------------
	-- Step 2: set DMA request sectors
	--------------------------------------
	Device DMA mode: Set DMASCNT, unit: sector.
	PIO burst mode: Set BCNT_LO, unit: bytes.
*/

#ifdef DEVICE_USE_DMA
	// The DMA_SCNT use 2048 bytes as sector unit.
	if (RecBytes % 2048)
		WRITE_BYTE(DMA_SCNT, RecBytes / 2048 + 1);
	else
		WRITE_BYTE(DMA_SCNT, RecBytes / 2048);

#else	// PIO burst mode
	WRITE_WORD(BCNT_LO, RecBytes);
	SET_W_BIT(ATA_CTRL, BCNT_STB);

#endif

	WRITE_DWORD(DMA_WSA, Addr);

/*
	--------------------------------------
	-- Step 3: start DMA transfer
	--------------------------------------
*/
#ifdef DEVICE_USE_DMA

	// Write to DMARDSTB register to generate a read strobe
	// to start device DMA transfer.

	SET_B_BIT(DMA_SET, DMA_RDSTB);

#else
	// start PIO burst transfer:

	WRITE_BYTE(ATA_ADDR, ATAREG_Data);
	READ_WORD(ATA_RDATA);

#endif
}

BOOL AIO_IsDmaOver(void)
{
	DWORD status = READ_DWORD(ATA_STATUS_1);

	/*
	   DMA_OVER will occur at two situations, one is when ATATSND come 
	   and transfer end, the other is memory bank size is full. So you
	   must check if memory bank is full or really data transfer end.
	   ATATSND will occur at two situation, one is when auto pause occur,
	   the other is when data transfer end. So, when ATATSND is coming,
	   you must check if auto pause occur or really data transfer end.
	 */
	if (status & DMA_OVER)
	{
		if (!(status & ATATSND))
		{
			TRACE("ATA DMA memory bank size is full!!!\n");
			ASSERT(0);
		}
		else if (status & AU_PAUSE)
		{
			TRACE("Auto pause occur in burst read!!!\n");			
			ASSERT(0);
		}
		CLEAR_B_BIT(DMA_SET, DMA_IOEN | DMA_EN);
		return TRUE;
	}
	return FALSE;
}

void AIO_HWReset(void)
{
	SET_B_BIT(ATA_PAR, ATA_RESET);
	delay_ms(2);

	CLEAR_B_BIT(ATA_PAR, ATA_RESET);
	delay_ms(10);
}

void AIO_SetDefault(void)
{
	WRITE_BYTE(ATA_PAR, 0x00);
}

void AIO_SetTiming(int PIO_Mode)
{
	#define SET_TIMING(ast, act, rec)				\
	{												\
		WRITE_BYTE(ATA_ASTTIME, (READ_BYTE(ATA_ASTTIME)&0x0f) | ((ast)<<4));	\
		WRITE_BYTE(ATA_ACTTIME, (act));				\
		WRITE_BYTE(ATA_RECTIME, (rec));				\
	}

	switch(PIO_Mode)
	{
	case PIO_MODE0:
		SET_TIMING(0x03, 0x05, 0x04);
		break;

	case PIO_MODE1:
		SET_TIMING(0x03, 0x05, 0x04);
		break;

	case PIO_MODE2:
		SET_TIMING(0x03, 0x05, 0x04);
		break;

	case PIO_MODE3:
		SET_TIMING(0x03, 0x05, 0x04);
		break;

	case PIO_MODE4:
		SET_TIMING(0x02, 0x04, 0x02);
		break;

	default:
		SET_TIMING(0x03, 0x05, 0x04);
		break;
	}
}

#define ATA_STATUS_INTRQ    (IDE_IOBASE + 0x15)

BOOL AIO_CheckInterrupt()
{
    return ((READ_BYTE(ATA_STATUS_INTRQ) & 0x01) == 0x01);
}

void AIO_ClearInterrupt()
{
	inportbi(ATAREG_AlternateStatus);
	inportbi(ATAREG_Status);    // Clear ATA device interrupt
}

#endif


void outporti(BYTE ind, WORD data)
{	
	AIO_WriteReg(ind, data);
}

void outportbi(BYTE ind, BYTE data)
{	
	AIO_WriteReg(ind, data);
}

BYTE inportbi(BYTE Index)
{	
	return (AIO_ReadReg(Index) & 0xff);
}
