/********************************************************************
  Copyright (C) 2002 T2-Design Corp.  All rights reserved.
    
  File: dvdloader.c
    
  Content: Implement the interface defined in t2cdr library. 
           Additionally, add some functions to suuport dvd, 
    
  History: 
    2002/12/7 by cwh  Created this file.
********************************************************************/

#define LOG_ENABLE		(1)		// if 0 disable all logs of this module
#define READ_LOG		(0)

#include <common.h>
#include <cdfmt.h>
#include <cdbuf.h>
#include <ecc.h>
#include <toc.h>
#include "ataio.h"
#include "atacmd.h"
#include "BIOS_cdr.h"


#define LOADER_READ_MAX			10
#define ATAPI_RETRY_COUNT       3
#define WAIT_READY_COUNT		25000	// Disc start waitting time(ms)

#define TRACK_LEADOUT			0xFF
#define TRACK_MAX				99
#define REVERSE_WORD(val)		((((val)>>8)&0xff) | (((val)<<8) & 0xff00))

static CDR_TOC cdrToc;
static FULL_TOC m_Toc;
static DWORD LeadOutLba = 0;
static int CurTrackType = TRACK_CDROM;
static int cdrStatus = CDR_IS_CLOSED;
static MediumType m_eMediumType = MediumTypeUnknown;

static BOOL BufferUpdate = FALSE;
static BOOL ReadStop = FALSE;
static DWORD StartLBA = -1;
static int ReadMode = CDR_MODE_2352;
static int LockNum = 0;
static int dvdNRReadCount = 0;
static int DiscReady = 0;

/********************************************************************
				FULL TOC Information Transform
				==============================
********************************************************************/

static BYTE GetTrkDscpNum(void)
{
	return ((m_Toc.DataLen - 2)/11);
}

static BYTE GetFirstTrack(void)
{
	BYTE i, DscpNum, FirstTrk = TRACK_MAX;

	DscpNum = GetTrkDscpNum();
	for(i=0; i<DscpNum; i++)
	{
		if(0xA0 == m_Toc.TrakDscp[i].POINT && m_Toc.TrakDscp[i].PMin < FirstTrk)
			FirstTrk = m_Toc.TrakDscp[i].PMin;
	}
	return FirstTrk;
}

static BYTE GetLastTrack(void)
{
	BYTE i, DscpNum, LastTrk = 0;

    DscpNum = GetTrkDscpNum();
	for(i=0; i<DscpNum; i++)
	{
		if(0xA1 == m_Toc.TrakDscp[i].POINT && m_Toc.TrakDscp[i].PMin > LastTrk)
			LastTrk = m_Toc.TrakDscp[i].PMin;
	}
	return LastTrk;
}

static BOOL FT_GetTrackMsf(BYTE Track, BYTE msf[3])
{
    BOOL fSuccess = FALSE;
	BYTE i, DscpNum, TrkIdx;
	DWORD LastLba, CurLba;

	if(TRACK_LEADOUT == Track)
	    Track = 0xA2;
	else if(Track > 99 )
		return FALSE;

	DscpNum = GetTrkDscpNum();

	for(i=0, TrkIdx=0, LastLba=0; i<DscpNum; i++)
	{
	    if(m_Toc.TrakDscp[i].POINT == Track)
	    {
            fSuccess = TRUE;
	        if(0xA2 != Track)
	        {
	            TrkIdx = i;
	            break;
	        }
			// ???
	        CurLba = MSF2LBA(m_Toc.TrakDscp[i].PMin, 
	                            m_Toc.TrakDscp[i].PSec, 
	                            m_Toc.TrakDscp[i].PFrame);
	        ASSERT(CurLba > 0);
	        if(CurLba >= LastLba)
	        {
	            LastLba = CurLba;
	            TrkIdx = i;
				// break; ??? 
	        }
	    }
	}
	if(fSuccess)
	{
	    msf[0] = m_Toc.TrakDscp[TrkIdx].PMin;
	    msf[1] = m_Toc.TrakDscp[TrkIdx].PSec;
	    msf[2] = m_Toc.TrakDscp[TrkIdx].PFrame;
	}
	return fSuccess;
}

/********************************************************************
 FT_GetTrackLba - Get track lba
 Parameters: 
    [IN]
    Track - track number. If it is 0xFF, leadout lba will 
            be returned.
    [OUT]
    Lba - pointer to a variable to receive the track lba.
********************************************************************/
static BOOL FT_GetTrackLba(BYTE Track, DWORD *Lba)
{
	BYTE msf[3];

    if(!FT_GetTrackMsf(Track, msf))
        return FALSE;

    *Lba = MSF2LBA(msf[0], msf[1], msf[2]);

    return TRUE;
}


#if (LOG_ENABLE)

void DbgPrintFullToc()
{
	LPFULL_TOC s = (LPFULL_TOC)&m_Toc;
	BYTE LeadOut[3];
	int j;

    TRACE("\n-------------------- FULL TOC ---------------------\n");
	TRACE3(" DataLen: %04d FirstSession: %02d LastSession: %02d\n", 
				s->DataLen, s->FirstSession, s->LastSession);
	TRACE("---------------------------------------------------\n");
	TRACE(" SessionNo   ADRCTL   POINT    mm     ss     ff\n");
	TRACE("---------------------------------------------------\n");
	for(j=0; j<(s->DataLen-2)/11; j++)
	{
		if(s->TrakDscp[j].POINT == 0xA2)
		{
			LeadOut[0] = s->TrakDscp[j].PMin;
			LeadOut[1] = s->TrakDscp[j].PSec;
			LeadOut[2] = s->TrakDscp[j].PFrame;
		}
		TRACE3(" %02x          %02x       %02x", 
				s->TrakDscp[j].SessionNo, 
				s->TrakDscp[j].ADRCTL, 
				s->TrakDscp[j].POINT 
				);
		TRACE3("       %02d     %02d     %02d\n", 
				s->TrakDscp[j].PMin, 
				s->TrakDscp[j].PSec, 
				s->TrakDscp[j].PFrame
				);
	}
	TRACE("---------------------------------------------------\n");
}
#endif


/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////

/********************************************************************
				Auto Non-Brocking Reading Process
				=================================
********************************************************************/

#define READ_IDLE		0
#define READ_FULL		1
#define READ_INTRQ		2
#define READ_DMA		3

static int ReadStatus = READ_IDLE;

static WORD ReadCount = 0;
// static int IntrqMiss = 0;

void CDLReadEnable(BOOL Enable)
{
}

void EnableCDRint(void)
{
	CDLReadEnable(TRUE);
}

void DisableCDRint(void)
{
	CDLReadEnable(FALSE);
}

static int GetReadNum(void)
{
	int num, free;
	BYTE track;
	DWORD lba;

	num = CDLBufGetFreeNum();
	free = CDLBufGetTailFree();
	if(num > free)
		num = free;

	if(DVD_Medium != m_eMediumType)
	{
		lba = CDLBufGetTailLba();
		if(lba >= LeadOutLba)
			return 0;

		track = GetSectorTrack(lba);
		free = GetTrackMaxLba(track) - lba + 1;

		if(num > free)
			num = free;
	}

	if(num > LOADER_READ_MAX)
		num = LOADER_READ_MAX;

#if (LOG_ENABLE)
	if(num < LOADER_READ_MAX)
		TRACE1("read num = %d\n",num);
#endif

	return num;
}

static BOOL IsReadFull(void)
{
	return (CDLBufGetFreeNum() - LockNum - dvdNRReadCount < LOADER_READ_MAX);
}

static BOOL IsReadIdel(void)
{
	return (ReadStatus == READ_IDLE || ReadStatus == READ_FULL);
}


/*********************************************************************
 Read_10 - Reading sectors data step 1. 
           1) Checking reading status to decide if continue.
           2) Getting reading ATAPI parameters.
           3) Sending ATAPI command.
*********************************************************************/
static void Read_10(void)
{
	int num, type;
	DWORD start;

    if(ReadStop)
    {
		ReadStatus = READ_IDLE;
		return;
    }

	if(BufferUpdate)
	{
		// TRACE("Buffer updata.\n");
		BufferUpdate = FALSE;
	}
	else if(IsReadFull())
	{
	#if (0)
		TRACE("Buffer full.\n");
	#endif
		ReadStatus = READ_FULL;
		return;
	}

	if((num = GetReadNum()) == 0)
	{
		TRACE("Read idel.\n");
		ReadStatus = READ_IDLE;
		return;
	}

	if(DVD_Medium == m_eMediumType)
		type = 0;	// Read 12 for DVD 
	else if(ReadMode == CDR_MODE_2048 || ReadMode == CDR_MODE_2048DC || ReadMode == CDR_MODE_2324)
		type = 1;	// Read CD User Data only
	else 
		type = 2;	// Read CD total 2352 bytes

	start = CDLBufGetTailLba();

	ReadStatus = READ_INTRQ;

	if(!ATA_StartReading(start, num, type))
	{
		ReadStatus = READ_IDLE;
		return;
	}
}


/*********************************************************************
 Read_20 - Reading sectors data step 2. 
           When ATA INTRQ occur, called this function to get data.
*********************************************************************/
static void Read_20(void)
{
	int Status = ATA_GetReadingStatus();

	if(Status == 0x01)
	{
		// Reading data ready
		ReadCount = ATA_GetByteCount();
		if (ReadCount == 0 || ReadCount == 0xffff)
		{
			// TRACE1("Read_20 error: ReadCount = 0x%x\n",ReadCount);
		}
		else
		{
		#ifdef DMA_MODE
			AIO_DmaStart(ReadCount, CDLBufGetTailAddr() & 0xffffff, 
				CDLBufGetSectorSize());
			ReadStatus = READ_DMA;

		#else
			AIO_PIORead(ReadCount, (BYTE *)CDLBufGetTailAddr());
			if(!BufferUpdate)
				CDLBufInputEx(ReadCount);
			Read_20();

		#endif
		}
	}
	else if(Status == 0x02)
	{
		// Reading finish
		Read_10();
	}
	else if(Status == 0x03)
	{
		// Reading error
		TRACE("Reading error!\n");
		if (!BufferUpdate)
			CDLBufInputNull();
		Read_10();
	}
}

/*********************************************************************
 Read_30 - Reading sectors data step 3. 
           Called it when DMA transfer over.
*********************************************************************/
#ifdef DMA_MODE
static void Read_30(void)
{
#if (READ_LOG)
	TRACE("DMA over.\n");
#endif

	AIO_SetDefault();
	ReadStatus = READ_INTRQ;

	if(!BufferUpdate)
	{
	#if (0)		// Reading checking when mode is 2352 bytes
		if(DVD_Medium != m_eMediumType && CurTrackType != TRACK_CDDA)
		{
			BYTE msf[3];
			BYTE * buff = (BYTE *)CDLBufGetTailAddr();
			DWORD lba = BCD_MSF2LBA(buff[12], buff[13], buff[14]);
			
			if(lba != CDLBufGetTailLba())
			{
				LBAToMSF(CDLBufGetTailLba(), msf, BCD_CODE);
				TRACE3("Read error: %02x:%02x:%02x,", msf[0], msf[1], msf[2]);
				TRACE3("return %02x:%02x:%02x\n", buff[12], buff[13], buff[14]);
				return;
			}
		}
	#endif

		CDLBufInputEx(ReadCount);
	}

/*
	if(IntrqMiss)
	{
		IntrqMiss = 0;
		Read_20();
	}
*/
	Read_20();
}
#endif

void BIOS_CDR_InterruptHandler(void)
{
	if(AIO_CheckInterrupt())
	{
		// AIO_ClearInterrupt();

		if(ReadStatus == READ_INTRQ)
		{
		#if (READ_LOG)
			TRACE("INTRQ occur.\n");
		#endif
			Read_20();
		}
		else if(ReadStatus == READ_DMA)
		{
			AIO_ClearInterrupt();
			// IntrqMiss = 1;
		}
		else
			AIO_ClearInterrupt();
	}
	else if(AIO_IsDmaOver())
	{
	#ifdef DMA_MODE
		ASSERT(ReadStatus == READ_DMA);
		Read_30();
	#endif
	}
	else
	{
		TRACE("IDE Interrupt error\n");
	}
}

static BOOL WaitDataReady(void)
{
	int timeout = 15000;	// reading timeout max is 15s

	while(timeout--)	
	{
		if(CDLBufGetCount())
			return TRUE;
		delay_ms(1);

		if(ReadMode == CDR_MODE_EMU32)
			BIOS_CDR_TimerHandler();
	}

	return FALSE;
}

static void EndRead(void)
{
	int timeout = 10000;

	ReadStop = TRUE;
	while(!IsReadIdel())
	{
		if(timeout-- == 0)
		{
			TRACE("End reading timeout!!!\n");
			ATA_SoftReset();
			break;
		}
		delay_ms(1);

		if(ReadMode == CDR_MODE_EMU32)
			BIOS_CDR_TimerHandler();
	}

//	ReadStop = FALSE;
	ReadStatus = READ_IDLE;
}

/********************************************************************
 TocTransform - Get CDR TOC from Full TOC
********************************************************************/
static void TocTransform(void)
{
    BYTE i, j, DscpNum;
	BYTE AdrCtl;
	CDR_TOC * toc = &cdrToc;

    toc->DiskType = (BYTE)m_eMediumType;
    toc->FirstTrk = GetFirstTrack();
    toc->LastTrk = GetLastTrack();
    FT_GetTrackMsf(TRACK_LEADOUT, toc->LeadOut);

    DscpNum = GetTrkDscpNum();
    for(i=toc->FirstTrk; i<=toc->LastTrk; i++)
    {
        for(j=0; j<DscpNum; j++)
        {
            if(i == m_Toc.TrakDscp[j].POINT)
            {
				AdrCtl = m_Toc.TrakDscp[j].ADRCTL;
                toc->TrkDscp[i].ADRCTL = (AdrCtl >> 4) | (AdrCtl << 4);
                toc->TrkDscp[i].TrkNo = m_Toc.TrakDscp[j].POINT;
                toc->TrkDscp[i].mm = m_Toc.TrakDscp[j].PMin;
                toc->TrkDscp[i].ss = m_Toc.TrakDscp[j].PSec;
                toc->TrkDscp[i].ff = m_Toc.TrakDscp[j].PFrame;
            }
        }
    }

	TocReset(&cdrToc);
}

static int CheckDiscReady(void)
{
#if 1
    ModeSenseData Data;
    DWORD Returned;
	BYTE type;

	if (DiscReady)
	{
		if (ATA_TestUnitReady(0))
		{
			TRACE("Unit ready\n");
			return 1;
		}
	}
	else if (ATA_ModeSense(0, sizeof(Data), (BYTE *)&Data, &Returned))
	{
		type = Data.medium_type_code;
		if (type > 0 && type <= 0x41)
		{
			if (!(type == 0x10 || type == 0x20 || type == 0x30))
			{
				TRACE1("ModeSense: disk ready, type = 0x%x.\n", type);
				DiscReady = 1;
			}
		}
		else if (type == 0x70)
		{
			TRACE("ModeSense: no disk.\n");
			return 0;
		}	
/*
		else if (type == 0x71)
		{
			return 2;
		}
*/
	}
	return 2;

#else

    RequestSenseData Data;
    DWORD Returned;
	BYTE * Buff = (BYTE *)&Data;

	if(ATA_TestUnitReady(0))
	{
		TRACE("Disk ready\n");
		return 1;
	}
	else if(ATA_RequestSense(0, sizeof(Data), (BYTE *)&Data, &Returned))
	{
//		if ((Data.sense_key == 0x02) && (Data.asc == 0x3A))
		if ((Buff[2] == 0x02) && (Buff[12] == 0x3A))
		{
			return 0;
		}
	}
	return 2;

#endif
}

static BOOL WaitDiscReady(void)
{
    int i;
	int status;
	int ms = 200;

    for(i = 0; i < WAIT_READY_COUNT; i += ms)
    {
		status = CheckDiscReady();
		if(status == 1)
			return TRUE;
		else if(status == 0)
			return FALSE;

		delay_ms(ms);
    }
	return FALSE;
}


/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////

/********************************************************************
						T2CDR Interface
						===============
********************************************************************/

int BIOS_CDR_Init(void* cdBuff, int numSectors)
{
	DWORD addr;

	InitChip();
	if(!InitATA(0))
	{
	    ASSERT(0);
	    return FALSE;
	}

	addr = (DWORD)cdBuff;
	addr |= 0xa0000000;

	// TRACE1("CD buffer = 0x%x\n", addr);

	ASSERT((addr & 0x0f) == 0x00);
	ASSERT(numSectors >= 20 && numSectors <= 1000);

	CDLBufSetup((BYTE *)addr, numSectors);

	memset(&m_Toc, 0, sizeof(m_Toc));

#if (IDE_TYPE == M3325_IDE)
	if(!ATA_OpenTray(0, FALSE))
	{
	    ASSERT(0);
	    return FALSE;
	}

#endif
#if (defined(_DEBUG) && 0)
	ATA_OpenTray(0, TRUE);
#endif

	return TRUE;
}

int BIOS_CDR_Start(void)
{
    DWORD TocSize;

	if(!WaitDiscReady())
		return FALSE;

    m_eMediumType = BIOS_DVD_GetMediumType();

	CDLBufReset(-1, 0);

    if(!ATA_ReadToc(0, TOC_Full, sizeof(m_Toc), (BYTE *)&m_Toc, &TocSize))
    {
        ASSERT(0);
        return FALSE;
    }

    m_Toc.DataLen = REVERSE_WORD(m_Toc.DataLen);

#if (LOG_ENABLE)
    DbgPrintFullToc();
#endif

    if(!FT_GetTrackLba(TRACK_LEADOUT, &LeadOutLba))
    {
        ASSERT(0);
        LeadOutLba = 0xFFFFFFFF;
    }

    if(DVD_Medium != m_eMediumType)
		TocTransform();

	ReadStatus = READ_IDLE;
    BufferUpdate = FALSE;
    ReadStop = FALSE;
	dvdNRReadCount = 0;
	StartLBA = -1;

/*
#if (defined(DMA_MODE) && (IDE_TYPE == M3325_IDE))

	CDLReadSectors(16, LOADER_READ_MAX/2, m_HwBuffer);
	if(!Ide_Init()) 
		return FALSE;
#endif
*/

    return TRUE;
}

int BIOS_CDR_StartNonBlocking(void)
{
	int result = CheckDiscReady();

	if(result == 1)
		BIOS_CDR_Start();

	return result;
}

void BIOS_CDR_Stop(void)
{
	EndRead();
}

int BIOS_CDR_TimerHandler(void)
{
	if(ReadMode == CDR_MODE_EMU32)
	{
		if(ReadStatus == READ_INTRQ)
		{
			Read_20();
		}
		else if(ReadStatus == READ_DMA && AIO_IsDmaOver())
		{
			Read_30();
		}
	}

	return 1;
}

void BIOS_CDR_GetToc(BIOS_CDR_TOC *toc)
{
	CDLGetToc(toc);
}

void BIOS_CDR_GetFullToc(FULL_TOC *toc)
{
	// Return full toc to support multi-section disc.
	memcpy(toc, &m_Toc, sizeof(m_Toc));
}

/********************************************************************
 BIOS_CDR_GetStatus - Get the current tray status
Return: 
    Tray status (macros CDR_IS_*):
	CDR_IS_OPEN    : tray is open
	CDR_IS_CLOSED  : tray is closed
	CDR_IS_OPENING : tray is opening
	CDR_IS_CLOSING : tray is closing
	CDR_IS_PUSHED  : tray is pushed (??)
********************************************************************/
unsigned int BIOS_CDR_GetStatus(void)
{
    ModeSenseData Data;
    DWORD Returned;

	if (cdrStatus == CDR_IS_CLOSED)
		return CDR_IS_CLOSED;

	EndRead();
    if(ATA_ModeSense(0, sizeof(Data), (BYTE *)&Data, &Returned))
	{
		if(Data.medium_type_code == 0x71)
			cdrStatus = CDR_IS_OPENED;
		else
			cdrStatus = CDR_IS_CLOSED;
	}

    return cdrStatus;
}

/********************************************************************
 BIOS_CDR_OpenTray - Open or close the tray
 Notes:
    This function is non-blocking. After called, the application can
	check the status of the requested action through the 
	CDR_GetStatus() function.
********************************************************************/
void BIOS_CDR_OpenTray(int action)
{
    if(CDR_TRAY_OPEN == action)
    {
		EndRead();
		CDLBufReset(-1, 0);

        ATA_OpenTray(0, TRUE);
		cdrStatus = CDR_IS_OPENED;
		DiscReady = 0;
    }
    else if(CDR_TRAY_CLOSE == action)
    {
        ATA_OpenTray(0, FALSE);
		cdrStatus = CDR_IS_CLOSED;
    }

	TRACE1("Do tray %s...\n", action ? "open" : "close");
}


/********************************************************************
 BIOS_CDR_SetSpeed - Set the cdloader speed (single or double)
 Notes: Does nothing with dvd loader.
********************************************************************/
int BIOS_CDR_SetSpeed(int speed)
{
	return 1;
}

/********************************************************************
 BIOS_CDR_StartReading - Start reading sectors in non-blocking mode, 
 from the given absolute address.
********************************************************************/
void BIOS_CDR_StartReading(unsigned long lba, int mode)
{
	int size;
	BOOL seek = TRUE;

	if(cdrStatus != CDR_IS_CLOSED)
	{
		ASSERT(0);
		return;
	}
	
	if(DVD_Medium == m_eMediumType)
	{
		ASSERT(mode == CDR_MODE_2048 || mode == CDR_MODE_2048DC);
	}
	else if(GetSectorType(lba) == TRACK_ERROR)
	{
		ASSERT(0);
		return;
	}
	
	if(mode == CDR_MODE_2048)
		size = 2048;
	else if(mode == CDR_MODE_2048DC)
	{
		size = 2048;
		seek = FALSE;
	}
	else if(mode == CDR_MODE_2352DC)
	{
		size = 2352;
		seek = FALSE;
	}
 	else if(mode == CDR_MODE_EMU32 || mode == CDR_MODE_2352)
		size = 2352;
	else // if(mode == CDR_MODE_2324)
		size = 2328;

	// TRACE1("start reading = %d\n", lba);

	StartLBA = lba;
	ReadMode = mode;
    ReadStop = FALSE;

	if(!CDLBufSeekSector(lba, size, seek))
	{
		BufferUpdate = TRUE;
		CDLBufReset(lba, size);

		if(DVD_Medium != m_eMediumType)
			CurTrackType = GetSectorType(lba);
	}

	if(IsReadIdel())
		Read_10();
}

/********************************************************************
 BIOS_CDR_GetNumSectorsInBuffer - Check if at least one sector of data is 
 ready, when reading in non-blocking mode
********************************************************************/
int BIOS_CDR_GetNumSectorsInBuffer(void)
{
    return CDLBufGetCount();
}

void BIOS_CDR_ResetBuffer(void)
{
#if (READ_LOG)
	TRACE("BIOS_CDR_ResetBuffer enter.\n");
#endif

	BufferUpdate = TRUE;
	ReadStop = TRUE;
	dvdNRReadCount = 0;

	CDLBufReset(-1, 0);
}

void BIOS_CDR_LockBuffer(int numSectors)
{
	int Output = numSectors - dvdNRReadCount;

	ASSERT(LockNum == 0);
	ASSERT(Output >= 0);
#if (READ_LOG)
	TRACE1("Lock buffer = %d.\n", numSectors);
#endif

	LockNum = numSectors;
	StartLBA += Output;
	dvdNRReadCount = 0;

	CDLBufOutputEx(Output);
}

void BIOS_CDR_ReleaseBuffer(void)
{
#if (READ_LOG)
	TRACE("BIOS_CDR_ReleaseBuffer.\n");
#endif
	LockNum = 0;

	if(IsReadIdel())
		Read_10();
}

int BIOS_CDR_ReadSectorNoRelease(unsigned char* buff)
{
	BYTE * sector = CDLBufOutputNoRelease();

	if(sector == NULL)
		return FALSE;

	memcpy(buff, sector, CDLBufGetSectorSize());
	dvdNRReadCount++;
	StartLBA ++;

	return TRUE;
}

/********************************************************************
 BIOS_CDR_GetSector - Get one sector of data, in non-blocking reading mode.
 Notes: Application can check if a sector is ready through 
 BIOS_CDR_GetNumSectorsInBuffer().
********************************************************************/
int BIOS_CDR_GetSector(unsigned char **buff)
{
	DWORD lba;
	int type = TRACK_CDROM;
#if (LOG_ENABLE)
	BYTE time[3];
#endif

	if(cdrStatus != CDR_IS_CLOSED || StartLBA == -1 || ReadStop)
	{
		ASSERT(0);
		return CDR_READ_FAILURE;
	}

	lba = StartLBA++;				// reading one by one

#if (LOG_ENABLE)
	LBAToMSF(lba, time, BCD_CODE);
#endif

	// TRACE3("CDR reading: %x:%x:%x\n",time[0],time[1],time[2]);

	if(DVD_Medium != m_eMediumType)
	{
		if((type = GetSectorType(lba)) == TRACK_ERROR)
			return CDR_READ_FAILURE;
	}
	
	if(!WaitDataReady())
	{
		TRACE3("CDR read timeout: %x:%x:%x\n",time[0],time[1],time[2]);
		return CDR_READ_FAILURE;
	}

	if((*buff = CDLBufOutput()) == NULL)
	{
		TRACE3("CDR read fail: %x:%x:%x\n",time[0],time[1],time[2]);
		return CDR_READ_FAILURE;
	}

	if(IsReadIdel())
		Read_10();

#ifdef _DEBUG
	if(ReadMode == CDR_MODE_2352 && type != TRACK_CDDA)
	{
		ASSERT(DVD_Medium != m_eMediumType);
		CDLSectorECC(*buff, time);
	}
#endif

	return CDR_READ_SUCCESS;
}

/********************************************************************
 BIOS_CDR_StopReading - Stop reading in non-blocking mode
********************************************************************/
void BIOS_CDR_StopReading(void)
{
    ReadStop = TRUE;
//	StartLBA = -1;
}

/********************************************************************
 BIOS_CDR_ReadSectors - Read sectors in blocking mode
********************************************************************/
int BIOS_CDR_ReadSectors(unsigned char* buff, unsigned long lba, int num, int mode)
{
	int i, len;
	BYTE * sector;
	
	if(cdrStatus != CDR_IS_CLOSED)
	{
		ASSERT(0);
		return 0;
	}

	if(lba != StartLBA || mode != ReadMode)
		BIOS_CDR_StartReading(lba, mode);
	
	len = CDLBufGetSectorSize();

	for(i = 0; i < num; i++, lba++)
	{
		if(!WaitDataReady())
			return i;
		if(BIOS_CDR_GetSector(&sector) != CDR_READ_SUCCESS)
			return i;
		
		CDLMemCopy((DWORD)sector, (DWORD)(buff + i * len), len);
	}
	
	return i;

}

int BIOS_CDR_ReadSectorsEx(unsigned char* buff, unsigned long lba, int num, int mode)
{
	int result;
	ReadStop = FALSE;
	result = BIOS_CDR_ReadSectors(buff, lba, num, mode);
	EndRead();
	return result;
}

/********************************************************************
				Optic Disc Enhanced Interface
				=============================
********************************************************************/


MediumType BIOS_DVD_GetMediumType()
{
    ModeSenseData SenseData;
    DWORD BytesReturned;
	MediumType type;

	EndRead();
    if(ATA_ModeSense(0, sizeof(SenseData), (BYTE *)&SenseData, &BytesReturned))
	{
		switch(SenseData.medium_type_code)
		{
		case 0x00:
		case 0x10:
		case 0x20:
		case 0x30:
			type = MediumTypeUnknown;
			break;
		case 0x01:
		case 0x05:
		case 0x11:
		case 0x15:
		case 0x21:
		case 0x25:
			type = CD_DataOnly;
			break;
		case 0x02:
		case 0x06:
		case 0x12:
		case 0x16:
		case 0x22:
		case 0x26:
			type = CD_CDDA;
			break;
		case 0x03:
		case 0x07:
		case 0x13:
		case 0x17:
		case 0x23:
		case 0x27:
			type = CD_DataAudioMixed;
			break;
		case 0x04:
		case 0x08:
		case 0x14:
		case 0x18:
		case 0x24:
		case 0x28:
			type = CD_Photo;
			break;
		case 0x41:
			type = DVD_Medium;
			break;
		case 0x70:
			type = NoDiskPresent;
			break;
		case 0x71:
			type = DiscTrayOpen;
			break;
		case 0x72:
			type = MediumFormatError;
			break;
		default:
			if (SenseData.medium_type_code < 41)
				type = CD_DataOnly;
			else
				type = MediumTypeUnknown;
			break;
		}
	}
	else
		type = MediumTypeNotReady;
	return type;
}

int BIOS_DVD_ReadDVDStructure(BYTE Layer, BYTE AGID, BYTE Format, DWORD ByteCount, BYTE *Buffer, DWORD *ByteReturned)
{
    int i;

	EndRead();
    for(i=0; i<ATAPI_RETRY_COUNT; i++)
    {
        if(ATA_ReadDVDStructure(0, Layer, AGID, Format, ByteCount, Buffer, ByteReturned))
            return 1;
    }
    return 0;
}

int BIOS_DVD_ReportKey(BYTE AGID, BYTE Format, DWORD Lba, DWORD ByteCount, BYTE *Buffer, DWORD *ByteReturned)
{
	BYTE KeyBuffer[16];
	BYTE *KeyData = &KeyBuffer[4];
	int i;

	ASSERT(sizeof(KeyBuffer) >= ByteCount);
								   
	EndRead();
	for(i=0; i<3; i++)
	{
	    if(ATA_ReportKey(0, AGID, Format, Lba, ByteCount, KeyBuffer, ByteReturned))
			break;
	}
	if(i >= 3)
	    return FALSE;

	if((Format == KEY_TitleKey) && ((KeyBuffer[4] & 0xC0) == 0x00))
		return FALSE;

	switch(Format)
	{
	case KEY_AGID:
		Buffer[0] = KeyData[3] & 0xC0;
		break;

	case KEY_ChallengeKey:
		Buffer[0] = KeyData[9];   // LSB
		Buffer[1] = KeyData[8];
		Buffer[2] = KeyData[7];
		Buffer[3] = KeyData[6];
		Buffer[4] = KeyData[5];
		Buffer[5] = KeyData[4];
		Buffer[6] = KeyData[3];
		Buffer[7] = KeyData[2];
		Buffer[8] = KeyData[1];
		Buffer[9] = KeyData[0];    // MSB
		break;

	case KEY_Key1:
		Buffer[0] = KeyData[4];    // LSB
		Buffer[1] = KeyData[3];
		Buffer[2] = KeyData[2];
		Buffer[3] = KeyData[1];
		Buffer[4] = KeyData[0];    // MSB
		break;

	case KEY_TitleKey:
		Buffer[0] = KeyBuffer[5];    // LSB
		Buffer[1] = KeyBuffer[6];
		Buffer[2] = KeyBuffer[7];
		Buffer[3] = KeyBuffer[8];
		Buffer[4] = KeyBuffer[9];    // MSB
		break;

	default:									   
		return FALSE;
	}
	return TRUE;
}

int BIOS_DVD_SendKey(BYTE AGID, BYTE Format, DWORD ByteCount, BYTE *Buffer, DWORD *ByteReturned)
{
	BYTE KeyBuffer[16];
	int i;

	switch(Format)
	{
	case KEY_ChallengeKey:
		KeyBuffer[0] = 0x00;
		KeyBuffer[1] = 0x0E;
		KeyBuffer[2] = 0x00;
		KeyBuffer[3] = 0x00;
		KeyBuffer[4] = Buffer[9];
		KeyBuffer[5] = Buffer[8];
		KeyBuffer[6] = Buffer[7];
		KeyBuffer[7] = Buffer[6];
		KeyBuffer[8] = Buffer[5];
		KeyBuffer[9] = Buffer[4];
		KeyBuffer[10] = Buffer[3];
		KeyBuffer[11] = Buffer[2];
		KeyBuffer[12] = Buffer[1];
		KeyBuffer[13] = Buffer[0];
		KeyBuffer[14] = 0x00;
		KeyBuffer[15] = 0x00;
		ByteCount = 16;
		break;

	case KEY_Key2:
		KeyBuffer[ 0 ] = 0x00;
		KeyBuffer[ 1 ] = 0x0A;
		KeyBuffer[ 2 ] = 0x00;
		KeyBuffer[ 3 ] = 0x00;
		KeyBuffer[ 4 ] = Buffer[ 4 ];
		KeyBuffer[ 5 ] = Buffer[ 3 ];
		KeyBuffer[ 6 ] = Buffer[ 2 ];
		KeyBuffer[ 7 ] = Buffer[ 1 ];
		KeyBuffer[ 8 ] = Buffer[ 0 ];
		KeyBuffer[ 9 ] = 0x00;
		KeyBuffer[ 10 ] = 0x00;
		KeyBuffer[ 11 ] = 0x00;
		KeyBuffer[ 12 ] = 0x00;
		KeyBuffer[ 13 ] = 0x00;
		KeyBuffer[ 14 ] = 0x00;
		KeyBuffer[ 15 ] = 0x00;
		ByteCount = 12;
		break;

	case KEY_RPCStruct:
		KeyBuffer[ 0 ] = 0x00;
		KeyBuffer[ 1 ] = 0x06;
		KeyBuffer[ 2 ] = 0x00;
		KeyBuffer[ 3 ] = 0x00;
		KeyBuffer[ 4 ] = Buffer[ 0 ];
		KeyBuffer[ 5 ] = 0x00;
		KeyBuffer[ 6 ] = 0x00;
		KeyBuffer[ 7 ] = 0x00;
		KeyBuffer[ 8 ] = 0x00;
		KeyBuffer[ 9 ] = 0x00;
		KeyBuffer[ 10 ] = 0x00;
		KeyBuffer[ 11 ] = 0x00;
		KeyBuffer[ 12 ] = 0x00;
		KeyBuffer[ 13 ] = 0x00;
		KeyBuffer[ 14 ] = 0x00;
		KeyBuffer[ 15 ] = 0x00;
		ByteCount = 8;
		break;

	default:
		return FALSE;
	}
	
	EndRead();
	for(i=0; i<3; i++)
	{	
		if(ATA_SendKey(0, AGID, Format, ByteCount, KeyBuffer, ByteReturned))
		    return TRUE;
	}

    return FALSE;
}

int BIOS_CDR_GetCdText(CD_TEXT *pCDText)
{
	DWORD Size;

    if(!ATA_ReadToc(0, TOC_CDTXT, sizeof(CD_TEXT), (BYTE *)pCDText, &Size))
        return FALSE;

    pCDText->DataLen = REVERSE_WORD(pCDText->DataLen);

	return TRUE;
}
