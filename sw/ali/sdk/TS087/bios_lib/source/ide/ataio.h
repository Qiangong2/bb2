#ifndef __ATAIO_H
#define __ATAIO_H

// ATA interface data transfer mode( DMA or PIO )
#define DMA_MODE

// IDE Device data transfer mode( DMA or PIO )
// #define DEVICE_USE_DMA

// Type of IDE systems
#define M3325_IDE		0
#define T2_IDE			1

#ifndef IDE_TYPE
#define IDE_TYPE		T2_IDE
#endif

#if (IDE_TYPE == T2_IDE)

#define HW_RESET_IDE()												\
	{																\
	WRITE_DWORD(0xb8000054, READ_DWORD(0xb8000054) & ~(1 << 28));	\
	delay_ms(100);													\
	WRITE_DWORD(0xb8000054, READ_DWORD(0xb8000054) | (1 << 28));	\
	delay_ms(300);													\
	}

#endif

// for ATA_ADDR to select ATA register

#define ATAREG_AlternateStatus	0x16	// R  - alternate status register 
#define ATAREG_DeviceControl	0x16	// W  - device control register 
#define ATAREG_Data				0x08	// RW - data register (16 bits)
#define ATAREG_Error			0x09	// R  - error register
#define ATAREG_Features			0x09	// W  - features
#define ATAREG_InterruptReason	0x0A	// RW - interrupt reason
#define ATAREG_TAG				0x0B	//    - reserved for SAM TAG byte
#define ATAREG_SectorCount		0x0A
#define ATAREG_SectorNumber		0x0B
#define ATAREG_ByteCountLow		0x0C	// RW - byte count, low byte
#define ATAREG_ByteCountHigh	0x0D	// RW - byte count, high byte
#define ATAREG_DriveSelect		0x0E	// RW - drive select
#define ATAREG_Status			0x0F	// R  - immediate status
#define ATAREG_Command			0x0F	// W  - command register
#define CSMask					0x18

typedef enum
{
    PIO_MODE0 = 0, 
    PIO_MODE1, 
    PIO_MODE2, 
    PIO_MODE3, 
    PIO_MODE4, 
    PIO_MODE5, 
    PIO_MODE6, 

    PIOModeMax
}ATAPIOMode;


void AIO_Init(void);
void AIO_HWReset(void);
void AIO_SetTiming(int PIO_Mode);
void AIO_SetDefault(void);
BOOL AIO_IsDmaOver(void);
void AIO_PIORead(WORD RecBytes, BYTE *buff, WORD BuffLen);
void AIO_PIOWrite(WORD SendBytes, BYTE *buff);
void AIO_DmaStart(WORD RecBytes, DWORD Addr, int SectorSize);

void outporti(BYTE ind, WORD data);
void outportbi(BYTE ind, BYTE data);
BYTE inportbi(BYTE Index);

BOOL AIO_CheckInterrupt();
void AIO_ClearInterrupt();


#endif
