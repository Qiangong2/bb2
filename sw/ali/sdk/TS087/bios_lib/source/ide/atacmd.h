/********************************************************************
 Copyright (C) 2002 T2-Design Corp.  All rights reserved.

 File: atacmd.h

 Content: Delcarations of ata command task structure and function.

 History: 2002/12/6 by cwh   Created this file.
********************************************************************/
#ifndef _ATACMD_H
#define _ATACMD_H


// ATA Status Register bits [Ref:SFF-8020i, page 55]

#define ATASR_CHECK              0x01  // error occured, see sense key/code
#define ATASR_ERR                0x01  // bit 0x02 reserved
#define ATASR_CORR               0x04  // correctable error occured
#define ATASR_DRQ                0x08  // data request / interrupt reason valid
#define ATASR_DSC                0x10  // immediate operation completed
#define ATASR_SERV               0x10  // serivce
#define ATASR_DF                 0x20  // drive fault
#define ATASR_DMARDY             0x20  // DMA ready
#define ATASR_DRDY               0x40  // ready to get command
#define ATASR_BSY                0x80  // registers busy
// for overlap mode only:
#define ATASR_SERVICE            0x10  // service is requested
#define ATASR_DMARDY             0x20  // ready to start a DMA transfer


// ATA Error Register bits [Ref:SFF-8020i, page 55]

#define ATAER_ILI                0x01  // illegal length indication
#define ATAER_EOM                0x02  // end of media detected
#define ATAER_ABRT               0x04  // command aborted
#define ATAER_MCR                0x08  // media change requested
#define ATAER_SKEY               0xf0  // sense key mask
#define ATAER_SK_NO_SENSE        0x00  // no specific sense key info
#define ATAER_SK_RECOVERED_ERROR 0x10  // command succeeded, data recovered
#define ATAER_SK_NOT_READY       0x20  // no access to drive
#define ATAER_SK_MEDIUM_ERROR    0x30  // non-recovered data error
#define ATAER_SK_HARDWARE_ERROR  0x40  // non-recoverable hardware failure
#define ATAER_SK_ILLEGAL_REQUEST 0x50  // invalid command parameter(s)
#define ATAER_SK_UNIT_ATTENTION  0x60  // media changed
#define ATAER_SK_DATA_PROTECT    0x70  // reading read-protected sector
#define ATAER_SK_BLANK_CHECK     0x80  // blank check
#define ATAER_SK_VENDOR_SPECIFIC 0x90  // vendor specific skey
#define ATAER_SK_COPY_ABORTED    0xa0  // copy aborted
#define ATAER_SK_ABORTED_COMMAND 0xb0  // command aborted, try again
#define ATAER_SK_EQUAL           0xc0  // equal
#define ATAER_SK_VOLUME_OVERFLOW 0xd0  // volume overflow
#define ATAER_SK_MISCOMPARE      0xe0  // data did not match the medium
#define ATAER_SK_RESERVED        0xf0


// ATA Feature Register bits [Ref:SFF-8020i, page 56]

#define ATAFR_DMA                0x01  // transfer data via DMA
#define ATAFR_OVERLAP            0x02  // release the bus until completion


// ATA Interrupt Reason Register bits [Ref:SFF-8020i, page 57]

#define ATAIRR_CoD               0x01  // command(1) or data(0)
#define ATAIRR_IN                0x02  // transfer to(1) or from(0) the host
#define ATAIRR_RELEASE           0x04  // bus released until completion


// ATA Drive Select Register values [Ref:SFF-8020i, page 57]

#define ATADSR_DRIVE0            0xa0  // drive 0 selected
#define ATADSR_DRIVE1            0xb0  // drive 1 selected
#define ATADSR_MasterDrive       0xa0
#define ATADSR_SlaveDrive        0xb0


// ATA Device Control Register bits [Ref:SFF-8020i, page 57]

#define ATADCR_nIEN              0x02  // disable/enable interrupt
#define ATADCR_SRST              0x04  // ATA Software reset


// define read TOC command packet format field

#define TOC_TOC           0x00
#define TOC_SessionInfo   0x01
#define TOC_Full          0x02
#define TOC_PMA           0x03
#define TOC_ATP           0x04
#define TOC_CDTXT         0x05


// ATA Task file

typedef struct
{
    BYTE DeviceControl; // Device control register
    BYTE Feature;       // Feature register
    BYTE SectorCount;   // Sector count register
    BYTE SectorNumber;  // Sector number register
    BYTE CylinderLow;   // Byte count low register
    BYTE CylinderHigh;  // Byte count high register
    BYTE DeviceHead;    // Device/Head register
    BYTE Command;       // Command register
}ATATaskFile, *LPATATaskFile;

// ATAPI command packet for most commands [Ref:SFF-8020i,page 86]

typedef WORD ATAPICmdPkt[6];
typedef ATAPICmdPkt *LPATAPICmdPkt;


// ATAPI Packet Command Return Data Struct

typedef struct
{
	WORD mode_data_length;
	BYTE medium_type_code;
	BYTE reserved[3];
	WORD block_descriptor_length;
	WORD dummy[8];
}ModeSenseData, *LPModeSenseData;

typedef struct
{
	BYTE error_code:7;
	BYTE valid:1;
	BYTE segment_number;
	BYTE sense_key:4;
	BYTE reserved1:1;
	BYTE ili:1;
	BYTE reserved2:2;
	BYTE information[4];
	BYTE add_sense_len;
	BYTE command_info[4];
	BYTE asc;
	BYTE ascq;
	BYTE fruc;
	BYTE sks[3];
	BYTE asb[46];
}RequestSenseData, *LPRequestSenseData;


#define MODE_NONBLOCK   0
#define MODE_BLOCK      1

void InitChip(void);
BOOL InitATA(int Drive);

// ATA Synchronize Functions

BOOL AcquireATALock(DWORD BlockMode);
void ReleaseATALock();
void ReleaseATALockInISR();

// ATAPI Packet Command Functions

BOOL ATA_TestUnitReady(int Drive);
BOOL ATA_OpenTray(int Drive, BOOL fOpen);
BOOL ATA_ReadToc(int Drive, BYTE TocFormat, DWORD ByteCount, BYTE *Buffer, DWORD *ByteReturned);
BOOL ATA_RequestSense(int Drive, DWORD ByteCount, BYTE *Buffer, DWORD *ByteReturned);
BOOL ATA_ModeSense(int Drive, DWORD ByteCount, BYTE *Buffer, DWORD *ByteReturned);
BOOL ATA_ReadDVDStructure(int Drive, BYTE Layer, BYTE AGID, BYTE Format, DWORD ByteCount, BYTE *Buffer, DWORD *ByteReturned);
BOOL ATA_ReportKey(int Drive, BYTE AGID, BYTE Format, DWORD Lba, DWORD ByteCount, BYTE *Buffer, DWORD *ByteReturned);
BOOL ATA_SendKey(int Drive, BYTE AGID, BYTE Format, DWORD ByteCount, BYTE *Buffer, DWORD *ByteReturned);
BOOL ATA_ReadCD(int Drive, DWORD StartLba, DWORD SectorCount, DWORD ByteCount, BYTE *Buffer, DWORD *ByteReturned);
BOOL ATA_Read12(int Drive, DWORD StartLba, DWORD SectorCount, DWORD ByteCount, BYTE *Buffer, DWORD *ByteReturned);

BOOL ATA_StartReading(int StartLba, int numSectors, int Type);
int ATA_GetReadingStatus(void);
WORD ATA_GetByteCount(void);
BOOL ATA_SoftReset(void);

#endif  // _ATACMD_H

