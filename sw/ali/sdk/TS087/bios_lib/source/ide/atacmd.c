/********************************************************************
 Copyright (C) 2002 T2-Design Corp.  All rights reserved.
    
 File: atacmd.c
    
 Content: 
    
 History: 2002/12/6 by cwh   Created this file.
********************************************************************/

#define LOG_ENABLE		(1)		// if 0 disable all logs of this module

#include <common.h>
#include "ataio.h"
#include "atacmd.h"
#include "BIOS_cdr.h"


#define NEW_ATA		0


extern void CDLReadEnable(BOOL enable);

#define BUSY_WAIT_CNT			6000	// Wait 6 Sec for Status of ATA Not Be Busy
#define DEFAULT_DELAY_TIME      3000    // 3000 ms
#define REQUIRE_DELAY_TIME      100     // 100 ms

static const BYTE DrivePort[2] = {0xA0, 0xB0};

#define SET_READCD_PARAM(Command, Lba, Num) \
    do \
    { \
    	Command[1] = (((Lba) & 0xFF0000)>>8) | (((Lba) & 0xFF000000)>>24); \
    	Command[2] = (((Lba) & 0xFF)<<8) | (((Lba) & 0xFF00)>>8); \
    	Command[3] = ((Num) & 0xFF00) | (((Num) & 0xFF0000)>>16); \
    	Command[4] |= ((Num) & 0xFF); \
    }while(0)

#define SET_READ12_PARAM(Command, Lba, Num) \
    do \
    { \
    	Command[1] = (((Lba) & 0xFF0000)>>8) | (((Lba) & 0xFF000000)>>24); \
    	Command[2] = (((Lba) & 0xFF)<<8) | (((Lba) & 0xFF00)>>8); \
		Command[3] = (((Num) & 0xFF0000)>>8) | (((Num) & 0xFF000000)>>24); \
		Command[4] = (((Num) & 0xFF)<<8) | (((Num) & 0xFF00) >> 8); \
    }while(0)
        
static int m_AtapiPacketSize = 12;

ATATaskFile ATACmdTaskFile[] = {
//Device |       |Sector|Sector|Bytecount|Bytecount|Device|
//Control|Feature| Count|Number| Low  | High | Head |Command
//-------+-------+------+------+------+------+------+--------
   {0x00,   0x03,  0x08,  0x00,  0x00,  0x00,  0x00, 0xEF},  // 00: ATACMD_PIOMode0
   {0x00,   0x03,  0x09,  0x00,  0x00,  0x00,  0x00, 0xEF},  // 01: ATACMD_PIOMode1
   {0x00,   0x03,  0x0A,  0x00,  0x00,  0x00,  0x00, 0xEF},  // 02: ATACMD_PIOMode2
   {0x00,   0x03,  0x0B,  0x00,  0x00,  0x00,  0x00, 0xEF},  // 03: ATACMD_PIOMode3
   {0x00,   0x03,  0x0C,  0x00,  0x00,  0x00,  0x00, 0xEF},  // 04: ATACMD_PIOMode4
   {0x00,   0x03,  0x0D,  0x00,  0x00,  0x00,  0x00, 0xEF},  // 05: ATACMD_PIOMode5
   {0x00,   0x03,  0x0E,  0x00,  0x00,  0x00,  0x00, 0xEF},  // 06: ATACMD_PIOMode6
   {0x00,   0x5D,  0x00,  0x00,  0x00,  0x00,  0x00, 0xEF},  // 07: ATACMD_EnableReleaseInterrupt
   {0x00,   0x5E,  0x00,  0x00,  0x00,  0x00,  0x00, 0xEF},  // 08: ATACMD_EnableServiceInterrupt
   {0x00,   0x00,  0x00,  0x00,  0x00,  0x00,  0x00, 0x08},  // 09: ATACMD_DeviceReset
   {0x00,   0x00,  0x00,  0x00,  0x00,  0x00,  0x00, 0xE5},  // 10: ATACMD_CheckPowerMode
   {0x00,   0x00,  0x00,  0x00,  0x00,  0x00,  0x00, 0x90},  // 11: ATACMD_DeviceDiagnostic
   {0x00,   0x00,  0x00,  0x00,  0x00,  0x00,  0x00, 0xEC},  // 12: ATACMD_IdentifyDevice
   {0x00,   0x00,  0x00,  0x00,  0x00,  0x00,  0x00, 0xA1},  // 13: ATACMD_IdentifyPacket
   {0x00,   0x00,  0x00,  0x00,  0x00,  0x00,  0x00, 0xA0},  // 14: ATACMD_NonDataPacket
   {0x00,   0x00,  0x00,  0x00,  0xFF,  0xFF,  0x00, 0xA0},  // 15: ATACMD_PacketCommand
   {0x00,   0xED,  0x00,  0x00,  0x00,  0x00,  0x00, 0xEF},  // 16: ATACMD_MediaEject
};

typedef enum {
  ATACMD_PIOMode0 = 0,
  ATACMD_PIOMode1 = 1,
  ATACMD_PIOMode2 = 2,
  ATACMD_PIOMode3 = 3,
  ATACMD_PIOMode4 = 4,
  ATACMD_PIOMode5 = 5,
  ATACMD_PIOMode6 = 6,
  ATACMD_EnableReleaseInterrupt = 7,
  ATACMD_EnableServiceInterrupt = 8,
  ATACMD_DeviceReset = 9,
  ATACMD_CheckPowerMode = 10,
  ATACMD_DeviceDiagnostic = 11,
  ATACMD_IdentifyDevice = 12,
  ATACMD_IdentifyPacket = 13,
  ATACMD_NondataPacket = 14,
  ATACMD_PacketCommand = 15,
  ATACMD_MediaEject = 16,

  ATACMD_MAX
} ATACommand;


ATAPICmdPkt ATAPICmdPacket[] = {
//     B1B0 |  B3B2 |  B5B4 |  B7B6 |  B9B8 | B11B10
//----------+-------+-------+-------+-------+----------
    {0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000},  // 00: ATAPICMD_TestUnitReady
    {0x011B, 0x0000, 0x0002, 0x0000, 0x0000, 0x0000},  // 01: ATAPICMD_OpenTray
    {0x011B, 0x0000, 0x0003, 0x0000, 0x0000, 0x0000},  // 02: ATAPICMD_CloseTray
    {0x001E, 0x0000, 0x0003, 0x0000, 0x0000, 0x0000},  // 03: ATAPICMD_PreventRemoval
    {0x001E, 0x0000, 0x0002, 0x0000, 0x0000, 0x0000},  // 04: ATAPICMD_AllowRemoval
    {0x0003, 0x0000, 0x0012, 0x0000, 0x0000, 0x0000},  // 05: ATAPICMD_RequestSense
    {0x00BD, 0x0000, 0x0000, 0x0000, 0xFF7F, 0x0000},  // 06: ATAPICMD_MechanismStatus
    {0x005A, 0x000E, 0x0000, 0x0000, 0x0018, 0x0000},  // 07: ATAPICMD_ModeSense,
    {0x1055, 0x000E, 0x0000, 0x0000, 0x0018, 0x0000},  // 08: ATAPICMD_ModeSelect,
    {0x0028, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000},  // 09: ATAPICMD_Read_10,
    {0x00A8, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000},  // 10: ATAPICMD_Read_12,
    {0x001B, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000},  // 11: ATAPICMD_StartStopUnit,
    {0x00BE, 0x0000, 0x0000, 0x0000, 0xF800, 0x0000},  // 12: ATAPICMD_ReadCD,
    {0x00B9, 0x0000, 0x1002, 0x0200, 0x1011, 0x0000},  // 13: ATAPICMD_ReadCD_MSF,
    {0x0044, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000},  // 14: ATAPICMD_ReadHeader,
    {0x0042, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000},  // 15: ATAPICMD_ReadSubChannel,
    {0x0043, 0x0000, 0x0000, 0x7F00, 0x00FF, 0x0000},  // 16: ATAPICMD_ReadTOC
    {0x00AD, 0x0000, 0x0000, 0x0000, 0xFF7F, 0x0000},  // 17: ATAPICMD_ReadDVDStruct,
    {0x00A4, 0x0000, 0x0000, 0x0000, 0xFF7F, 0x0000},  // 18: ATAPICMD_ReportKey,
    {0x00A3, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000},  // 19: ATAPICMD_SendKey,
    {0x0012, 0x0000, 0x0024, 0x0000, 0x0000, 0x0000},  // 20: ATAPICMD_Inquiry
};

typedef enum {
  ATAPICMD_TestUnitReady = 0,
  ATAPICMD_OpenTray = 1,
  ATAPICMD_CloseTray = 2,
  ATAPICMD_PreventRemoval = 3,
  ATAPICMD_AllowRemoval = 4,
  ATAPICMD_RequestSense = 5,
  ATAPICMD_MechanismStatus = 6,
  ATAPICMD_ModeSense = 7,
  ATAPICMD_ModeSelect = 8,
  ATAPICMD_Read_10 = 9,
  ATAPICMD_Read_12 = 10,
  ATAPICMD_StartStopUnit = 11,
  ATAPICMD_ReadCD = 12,
  ATAPICMD_ReadCD_MSF = 13,
  ATAPICMD_ReadHeader = 14,
  ATAPICMD_ReadSubChannel = 15,
  ATAPICMD_ReadTOC = 16,
  ATAPICMD_ReadDVDStruct = 17,
  ATAPICMD_ReportKey = 18,
  ATAPICMD_SendKey = 19,
  ATAPICMD_Inquiry = 20,

  ATAPICMD_MAX
} ATAPICommand;

typedef BOOL (*ATA_TRANSFER_DATA)(BYTE *Buffer, DWORD ByteCount, 
                                  DWORD *ByteReturned);

void DbgPrintTaskFile(LPATATaskFile Task)
{
    TRACE4("TaskFile={0x%02x, 0x%02x, 0x%02x, 0x%02x, ",
        Task->DeviceControl, Task->Feature, 
        Task->SectorCount, Task->SectorNumber);
    TRACE4("0x%02x, 0x%02x, 0x%02x, 0x%02x}\n", 
        Task->CylinderLow, Task->CylinderHigh, 
        Task->DeviceHead, Task->Command);
}

/********************************************************************
					ATA Register Operations
					=======================
********************************************************************/

#define CheckStatus(and, cmp, wait) \
    CheckRegister(ATAREG_Status, (and), (cmp), (wait))
#define CheckBusyStatusClear(wait) \
    CheckRegister(ATAREG_Status, ATASR_BSY, 0x00, (wait))
#define CheckAlternateStatus(and, cmp, wait) \
    CheckRegister(ATAREG_AlternateStatus, (and), (cmp), (wait))
#define CheckIntStatus(and, cmp, wait) \
    CheckRegister(ATAREG_InterruptReason, (and), (cmp), (wait))

/********************************************************************
 CheckRegister - Check register status until TimeOut or 
    (AndPattern == CmpPattern)
 Parameters: 
    [IN]
    ataRegister - ATAPI register to check
    AndPattern - pattern to and with status of register
    CmpPattern - compare result pattern
    WaitTime - time out limit
    [OUT]
 Return: 
    If (status & and_pattern) == copmare_pattern, return 
    TRUE; otherwise, return FALSE if time out.
********************************************************************/
static BOOL CheckRegister(BYTE ataRegister, BYTE AndPattern, BYTE CmpPattern, int WaitTime)
{
	int retries = 0;
	
	// check each REQUIRE_DELAY_TIME
	while(1)
	{
		if((inportbi(ataRegister) & AndPattern) == CmpPattern)
		    return TRUE;
		if(++retries > (WaitTime/REQUIRE_DELAY_TIME))
		    break;
		delay_ms(REQUIRE_DELAY_TIME);
	}
	return FALSE;
}

static BOOL CheckErrorStatus()
{
    BYTE Temp;

    Temp = inportbi(ATAREG_Status);
	if(Temp & ATASR_ERR)
	{
	    TRACE1("ErrorStatus: SenseKey=0x%x\n", Temp>>4);
		return FALSE;
	}
	return TRUE;
}

#if 0
static BOOL WaitDeviceINTRQ(int WaitTime)
{
	int retries;

	if(!WaitTime)
	{
		if(AIO_CheckInterrupt())
		{
			inportbi(ATAREG_AlternateStatus);
			inportbi(ATAREG_Status);    // Clear ATA device interrupt
			AIO_ClearInterrupt();
			return TRUE;
		}
		else
			return FALSE;
	}
	for(retries=0; retries<WaitTime/REQUIRE_DELAY_TIME; retries++)
	{
		if(AIO_CheckInterrupt())
		{
		    inportbi(ATAREG_AlternateStatus);
		    inportbi(ATAREG_Status);    // Clear ATA device interrupt
		    AIO_ClearInterrupt();
			return TRUE;
		}
		delay_ms(REQUIRE_DELAY_TIME);
	}
	return FALSE;
}
#endif

/********************************************************************
 SoftReset - reset atapi device
 Parameters: 
    [IN]
    Drive - Drive select
 Return: 
********************************************************************/
static BOOL SoftReset(int Drive)
{
	int i, ResetTimes = 5;

    ASSERT(0 == Drive || 1 == Drive);

    for(i=0; i<ResetTimes; i++)
    {
        outportbi(ATAREG_DriveSelect, DrivePort[Drive]);
	#if (NEW_ATA)
        delay_ms(5);
	#endif

        outportbi(ATAREG_Error, 0);
		outportbi(ATAREG_InterruptReason, 0);
        outportbi(ATAREG_TAG, 0);
        outportbi(ATAREG_ByteCountLow, 0);
        outportbi(ATAREG_ByteCountHigh, 0);
        outportbi(ATAREG_Command, 0x08);
	#if (NEW_ATA)
        delay_ms(5);
	#endif

        if(CheckBusyStatusClear(DEFAULT_DELAY_TIME))
        {
			TRACE("Soft reset ok.\n");
			return TRUE;
        }
    }

	TRACE("Soft reset fail!\n");
	// ASSERT(0);
	return FALSE;
}

BOOL ATA_SoftReset(void)
{
	BOOL result = SoftReset(0);
	ReleaseATALockInISR();

	return result;
}

/********************************************************************
 PollDevice - polling device
 Parameters: 
    [IN]
    PollingMode - 0: polling interrupt flag
                  1: polling busy flag
    [OUT]
 Return: 
********************************************************************/
#if 0
static BOOL PollDevice(int PollingMode, int WaitTime)
{
	if(!PollingMode)
	{
		return WaitDeviceINTRQ(WaitTime);
	}
	else
	{
		return CheckBusyStatusClear(WaitTime);
	}
}
#endif

/********************************************************************
 SelectDrive - Select master/slave drive protocol
 Parameters: 
    [IN]
    Drive - 0: select master drive;
            1: select slave drive.
    [OUT]
 Return: TRUE if success, otherwise FALSE.
********************************************************************/
static BOOL SelectDrive(int Drive)
{
    ASSERT(0==Drive || 1==Drive);

	// Wait device not busy
	if(!CheckBusyStatusClear(DEFAULT_DELAY_TIME))
	{
        TRACE("SelectDrive error: Device is BUSY!\n");
        return FALSE;
	}
	
	// Select the drive we really want to work with by
	// putting 0xA0 or 0xB0 in the Drive/Head register
	outportbi(ATAREG_DriveSelect, DrivePort[Drive]);
#if (NEW_ATA)
	delay_ns(400);
#endif
	
	if(!CheckBusyStatusClear(DEFAULT_DELAY_TIME))
	{
        TRACE("SelectDrive error: Device keeps BUSY!\n");
	    return FALSE;
	}

	return TRUE;
}

static BOOL WaitLoaderReadyAfterReset(int WaitMode)
{
	BYTE status;
	int timeout = 3000;
	
	while (timeout --)
	{
		status = inportbi(ATAREG_Status);
		if(WaitMode == 0) // Hardware reset
		{
			if(!status || (status == (ATASR_DRDY|ATASR_DSC)))
			    return TRUE;
		}
		else // Software reset
		{
			if ((status & (ATASR_BSY | ATASR_DRDY)) == ATASR_DRDY)
			    return TRUE;
		}
		delay_ms(1);
	}
	return FALSE;
}

/********************************************************************

					ATA Synchronize Functions 
					=========================
********************************************************************/

#define ATALOCK_RELEASED        0
#define ATALOCK_ACQUIRED        1
volatile static DWORD g_AtaLock = ATALOCK_RELEASED;
/********************************************************************
 AcquireATALock - Acquire permission to access ata device.
 Note:
	if it is called in isr, only non-blocking mode can be
	used, otherwise, it will cause dead-lock.
********************************************************************/
BOOL AcquireATALock(DWORD BlockMode)
{
    BOOL fAcquired = FALSE;
    if(MODE_BLOCK == BlockMode)
    {
        while (1)
        {
			CDLReadEnable(FALSE);	// ???
            if(ATALOCK_RELEASED == g_AtaLock)
            {
                g_AtaLock = ATALOCK_ACQUIRED;
				CDLReadEnable(TRUE);
                fAcquired = TRUE;
                break;
            }
			CDLReadEnable(TRUE);
			// delay_ms(1);
        }
    }
    else if(MODE_NONBLOCK == BlockMode)
    {
        if(ATALOCK_RELEASED == g_AtaLock)
        {
            g_AtaLock = ATALOCK_ACQUIRED;
            fAcquired = TRUE;
        }
    }
    return fAcquired;
}

void ReleaseATALock()
{
	CDLReadEnable(FALSE);	// ???
    g_AtaLock = ATALOCK_RELEASED;
	CDLReadEnable(FALSE);
}

void ReleaseATALockInISR()
{
    g_AtaLock = ATALOCK_RELEASED;
}


/********************************************************************

				ATA Command Common Operations
				=============================
********************************************************************/

static int TestIdeBusy(int Times, char *status, char *intrdata)
{
	int	cntr;
	char status1 = 0;

	for (cntr=Times; cntr>0; cntr--)
	{
		status1 = inportbi(ATAREG_Status);
		if (!(status1 & ATASR_BSY))
			break;
		delay_ms(1);
	}
	if(status != NULL)
	{
		*status = status1;
		*intrdata = inportbi(ATAREG_InterruptReason);
	}
	
	return cntr;
}

/********************************************************************
 WriteTaskFile - Write ATA task file to device
 Note:
********************************************************************/
static void WriteTaskFile(BYTE Drive, LPATATaskFile TaskFile)
{
    outportbi(ATAREG_DeviceControl,	TaskFile->DeviceControl);
	outportbi(ATAREG_Features,		TaskFile->Feature);
	outportbi(ATAREG_SectorCount,	TaskFile->SectorCount);
	outportbi(ATAREG_SectorNumber,	TaskFile->SectorNumber);
	outportbi(ATAREG_ByteCountLow,	TaskFile->CylinderLow);
	outportbi(ATAREG_ByteCountHigh,	TaskFile->CylinderHigh);
	outportbi(ATAREG_Command,		TaskFile->Command);
#if (NEW_ATA)
	delay_ns(400);
#endif
}

/********************************************************************
  Write ATAPI command packet to device
  Note:
********************************************************************/
static void WriteCommandPacket(ATAPICmdPkt Command)
{
	int i;

	for(i=0; i<6; i++)
	{
	    outporti(ATAREG_Data, Command[i]);
	}

	if(m_AtapiPacketSize != 12)
	{
	    outporti(ATAREG_Data, 0);
	    outporti(ATAREG_Data, 0);
	}
}

static WORD GetByteCount(void)
{
	WORD cRecByte;
 
	cRecByte = inportbi(ATAREG_ByteCountLow);
	cRecByte |= inportbi(ATAREG_ByteCountHigh) << 8;

	return cRecByte;
}

WORD ATA_GetByteCount(void)
{
	return GetByteCount();
}

/********************************************************************
 Read ATA data port with PIO mode
 Note:
********************************************************************/
static BOOL PIOReadData(BYTE *Buffer, DWORD ByteCount, DWORD *ByteRead)
{
	WORD Count = GetByteCount();

	if(Count && (Count != 0xFFFF))
	{
		AIO_PIORead(Count, Buffer, ByteCount);

	    if(Count > ByteCount)
	        Count = ByteCount;
		if (NULL != ByteRead)
			*ByteRead = Count;

	    AIO_SetDefault();
		return TRUE;
	}
	else
	{
		TRACE1("GetByteCount error = %x\n", Count);
		return FALSE;
	}
}

/********************************************************************
 PIOWriteData - Write data to ata data port.
 Note:
********************************************************************/
static BOOL PIOWriteData(BYTE *Buffer, DWORD ByteCount, DWORD *ByteWritten)
{
	WORD Count = GetByteCount();

	if(Count && 0xFFFF != Count)
	{
		AIO_PIOWrite(Count, Buffer);

		if (Count > ByteCount)
			Count = ByteCount;
		if (NULL != ByteWritten)
			*ByteWritten = Count;

		AIO_SetDefault();
		return TRUE;
	}
	else
	{
		TRACE1("GetByteCount error = %x\n", Count);
		return FALSE;
	}
}

/********************************************************************
 InCommand - ATA PIO data in command
 Note:
	if fails, and atapi is soft reset.
********************************************************************/
static BOOL InCommand(int Drive, LPATATaskFile Task, DWORD ByteCount, 
					  BYTE *Buffer, DWORD *ByteRead)
{
	BOOL fSuccess = FALSE;
	DWORD Temp = 0;
	BYTE status, intrdata;

	ASSERT(NULL != Buffer && NULL != ByteRead);

    do
    {
		if(!SelectDrive(Drive))
		{
			ASSERT(0);
    	    break;
		}

    	WriteTaskFile(Drive, Task);

	#if 0
        // HPIOI0: INTRQ_Wait State
	    if(!PollDevice(0, DEFAULT_DELAY_TIME))
	    {
            ASSERT(0);
	        break;
	    }
        // HPIOI1: Check_Status State
        delay_ns(400);
        if(!CheckStatus(ATASR_BSY|ATASR_DRQ, ATASR_DRQ, DEFAULT_DELAY_TIME))
        {
	        ASSERT(0);
            break;
        }
	#endif

        *ByteRead = 0;

    	while (1)
	    {
			if(!TestIdeBusy(BUSY_WAIT_CNT, &status, &intrdata))
				break;

			intrdata &= (ATAIRR_IN | ATAIRR_CoD);
			if ((status & ATASR_DRQ) && (intrdata == ATAIRR_IN))
			{
				if (!PIOReadData(&Buffer[*ByteRead], ByteCount, &Temp))
					break;
				ByteCount -= Temp;
				*ByteRead += Temp;
			}

		    if(!ByteCount)
		    {
		        fSuccess = TRUE;
		        break;
		    }
	    }
    }while(0);

	if(fSuccess && !CheckErrorStatus())
	    fSuccess = FALSE;

    if(!fSuccess)
	{
		TRACE("InCommand() fail!\n");
        SoftReset(Drive);
	}

	return fSuccess;
}

/********************************************************************
 NondataCommand - ATA no-data command
 (*) -> SelectDrive -> WriteCommand -> CheckStatus -> HostIdle -> (o)
********************************************************************/
static BOOL NondataCommand(int Drive, LPATATaskFile Task)
{
    BOOL status = FALSE;

    do
    {
	    // Step 1: Select drive
	    if(!SelectDrive(Drive))
	    {
	        ASSERT(0);
	        break;
	    }

	    // Step 2: Write task file
	    WriteTaskFile(Drive, Task);

	#if (0)
		// Step 3: Wait INTRQ
	    if(!PollDevice(0, DEFAULT_DELAY_TIME))
	    {
	        ASSERT(0);	// ??? NO disc fail
	        break;
	    }
	    // Step 4: Final step, check status
	    if(!CheckStatus(ATASR_BSY|ATASR_DRQ, 0x00, DEFAULT_DELAY_TIME))
	    {
	        ASSERT(0);
	        break;
	    }
	#endif

		if(!TestIdeBusy(DEFAULT_DELAY_TIME, NULL, NULL))
			break;

	    if(!CheckErrorStatus())
	        break;

	    status = TRUE;
    }while(0);

    if(!status)
	{
		TRACE("NondataCommand() fail!\n");
        SoftReset(Drive);
	}

	return status;
}

static BOOL PacketCommand(int Drive, LPATATaskFile Task, ATAPICmdPkt Command,
    DWORD ByteCount, BYTE *Buffer, DWORD *ByteReturned, ATA_TRANSFER_DATA fnTransferData)
{
    BOOL fSuccess = FALSE;
    DWORD Temp;
	BYTE status, intrdata, direction;

    ASSERT(NULL != Task);
    ASSERT((NULL != Buffer && NULL != ByteReturned && NULL != fnTransferData) ||
			(NULL == Buffer && NULL == ByteReturned && NULL == fnTransferData));

    // DbgPrintTaskFile(Task);
    AIO_SetDefault();

	do 
	{
		if(!SelectDrive(Drive))
		{
			ASSERT(0);
    		break;
		}

		WriteTaskFile(Drive, Task);

	#if (0)
		// HP0:Check_Status_A State
		//delay_ns(400);
		if(!CheckStatus(ATASR_BSY|ATASR_DRQ, ATASR_DRQ, DEFAULT_DELAY_TIME))
		{
    		ASSERT(0);
			break;
		}
	#endif

		if (!TestIdeBusy(DEFAULT_DELAY_TIME, NULL, NULL))
			break;

		// HP1:Send_Packet State
		WriteCommandPacket(Command);
		if(NULL != ByteReturned)
			*ByteReturned = 0;

		while (1)
		{
			if(!TestIdeBusy(BUSY_WAIT_CNT, &status, &intrdata))
				break;

			if(NULL == Buffer)
			{
				fSuccess = TRUE;
				break;
			}

			direction = intrdata & (ATAIRR_IN | ATAIRR_CoD);
			if((status & ATASR_DRQ))
			{
				if((direction == ATAIRR_IN) || !direction)
				{
					(*fnTransferData)(&Buffer[*ByteReturned], ByteCount, &Temp);
					ByteCount -= Temp;
					*ByteReturned += Temp;
				}
			}
			else if((status & ATASR_DRDY))
			{
				if(direction == (ATAIRR_IN | ATAIRR_CoD))
				{
					fSuccess = TRUE;
					break;
				}
			}
		}
	} while(0);

    if(fSuccess && !CheckErrorStatus())
    	fSuccess = FALSE;

    if(!fSuccess)
	{
		// TRACE("PacketCommand() fail!\n");
        // SoftReset(Drive);
	}

	return fSuccess;
}       

/********************************************************************

					ATA Command Task Functions
					==========================
********************************************************************/

/********************************************************************
 Pkt_Nondata - Execute an ata packet non-data command.
 Note:
	Ref to T13 1410D ATA/ATAPI-7 V3, 6.8, Page 115.
	if fails, and atapi is soft reset.
********************************************************************/
static BOOL Pkt_Nondata(int Drive, ATAPICmdPkt Command)
{
    return PacketCommand(Drive, &ATACmdTaskFile[ATACMD_NondataPacket], 
                         Command, 0, NULL, NULL, NULL);
}

/********************************************************************
 Pkt_PIODataOut - Execute an ata packet PIO data-out command.
 Note:
	Ref to T13 1410D ATA/ATAPI-7 V3, 6.8, Page 115.
	if fails, and atapi is soft reset.
********************************************************************/
static BOOL Pkt_PIODataOut(int Drive, ATAPICmdPkt Command, DWORD ByteCount,
						   BYTE *Buffer,DWORD *ByteWritten)
{
    ASSERT(NULL != Buffer && NULL != ByteWritten);
    return PacketCommand(Drive, &ATACmdTaskFile[ATACMD_PacketCommand], 
                         Command, ByteCount, Buffer, ByteWritten, 
                         PIOWriteData);
}

/********************************************************************
 Pkt_PIODataIn - Execute an ata packet PIO data-in command.
 Note: 
	Ref to T13 1410D ATA/ATAPI-7 V3, 6.8, Page 115.
    if fails, and atapi is soft reset.
********************************************************************/
static BOOL Pkt_PIODataIn(int Drive, ATAPICmdPkt Command, DWORD ByteCount,
						  BYTE *Buffer, DWORD *ByteRead)
{
    ASSERT(NULL != Buffer && NULL != ByteRead);
    return PacketCommand(Drive, &ATACmdTaskFile[ATACMD_PacketCommand], 
                         Command, ByteCount, Buffer, ByteRead, 
                         PIOReadData);
}

/********************************************************************
 Identify - identify packet device.
 Note:
********************************************************************/
static BOOL Identify(int Drive, DWORD ByteCount, BYTE *Buffer, DWORD *ByteReturned)
{
    BOOL fSuccess = FALSE;
	int times = 3;

    ASSERT(NULL != Buffer && NULL != ByteReturned);

    AcquireATALock(MODE_BLOCK);

	while(times--)
	{
		fSuccess = InCommand(Drive, &ATACmdTaskFile[ATACMD_IdentifyPacket], 
							 ByteCount, Buffer, ByteReturned);
		if(fSuccess)
			break;
	}
    ReleaseATALock();
    return fSuccess;
}

static BOOL SetPIOMode(int Drive, ATAPIOMode ePIOMode)
{	
    BOOL fSuccess = FALSE;
    LPATATaskFile Task;

    AIO_SetTiming(ePIOMode);

    switch(ePIOMode)
    {
        case PIO_MODE0:
            Task = &ATACmdTaskFile[ATACMD_PIOMode0];
            break;
            
        case PIO_MODE1:
            Task = &ATACmdTaskFile[ATACMD_PIOMode1];
            break;
            
        case PIO_MODE2:
            Task = &ATACmdTaskFile[ATACMD_PIOMode2];
            break;
            
        case PIO_MODE3:
            Task = &ATACmdTaskFile[ATACMD_PIOMode3];
            break;
            
        case PIO_MODE4:
            Task = &ATACmdTaskFile[ATACMD_PIOMode4];
            break;
            
        case PIO_MODE5:
            Task = &ATACmdTaskFile[ATACMD_PIOMode5];
            break;
            
        case PIO_MODE6:
            Task = &ATACmdTaskFile[ATACMD_PIOMode6];
            break;

        default:
            return fSuccess;
    }
    AcquireATALock(MODE_BLOCK);
    fSuccess = NondataCommand(Drive, &ATACmdTaskFile[ePIOMode]);
    if(!fSuccess)
        ASSERT(0);
    ReleaseATALock();
    return fSuccess;
}


/********************************************************************

				ATAPI Command Packet Functions
				==============================
********************************************************************/


/********************************************************************
 ATA_TestUnitReady - Implement atapi command TestUnitReady.
 Note: If the disc is ready return TRUE, otherwise FALSE.
********************************************************************/
BOOL ATA_TestUnitReady(int Drive)
{
    BOOL fSuccess;

    AcquireATALock(MODE_BLOCK);
    fSuccess = Pkt_Nondata(Drive, ATAPICmdPacket[ATAPICMD_TestUnitReady]);
    ReleaseATALock();

    return fSuccess;
}

/********************************************************************
 ATA_OpenTray - Open/Close tray
 Note: 
********************************************************************/
BOOL ATA_OpenTray(int Drive, BOOL fOpen)
{
    BOOL fSuccess = FALSE;
    int i, j;
    LPATAPICmdPkt Command;

    if(fOpen)
        Command = &ATAPICmdPacket[ATAPICMD_OpenTray];
    else
        Command = &ATAPICmdPacket[ATAPICMD_CloseTray];
    AcquireATALock(MODE_BLOCK);
    for(i=0; i<10; i++)
    {
        for(j=0; j<3; j++)
        {
            if((fSuccess = Pkt_Nondata(Drive, *Command)))
                break;
            delay_ms(500);
        }
        if(fSuccess)
            break;
        SoftReset(Drive);
    }
    ReleaseATALock();
    return fSuccess;
}


/********************************************************************
 ATA_ReadToc - Read TOC
 Note: 
********************************************************************/
BOOL ATA_ReadToc(int Drive, BYTE TocFormat, DWORD ByteCount, BYTE *Buffer, 
				DWORD *ByteReturned)
{
	int i;
    BOOL fSuccess;
    ATAPICmdPkt Command;

    ASSERT(NULL != Buffer);

	memcpy(&Command, &ATAPICmdPacket[ATAPICMD_ReadTOC], 
	        sizeof(ATAPICmdPkt));
	Command[1] = (Command[1] & 0xFFF0) | ((WORD)TocFormat & 0xF);
	AcquireATALock(MODE_BLOCK);
	for(i=0; i<3; i++)
	{
		fSuccess = Pkt_PIODataIn(
					Drive, Command, ByteCount, Buffer, ByteReturned);
		if(fSuccess)
			break;
	}
	ReleaseATALock();
	return fSuccess;
}


/********************************************************************
 ATA_RequestSense - Execut atatpi RequestSense command
 Note: 
********************************************************************/
BOOL ATA_RequestSense(int Drive, DWORD ByteCount, BYTE *Buffer, DWORD *ByteReturned)
{
    BOOL fSuccess;

    AcquireATALock(MODE_BLOCK);
    fSuccess = Pkt_PIODataIn(
                Drive, ATAPICmdPacket[ATAPICMD_RequestSense], 
                ByteCount, Buffer, ByteReturned);
    ReleaseATALock();

    return fSuccess;
}

/********************************************************************
 ATA_ModeSense - Execute atapi ModeSense command.
 Note: 
********************************************************************/
BOOL ATA_ModeSense(int Drive, DWORD ByteCount, BYTE *Buffer, DWORD *ByteReturned)
{
    BOOL fSuccess;

    AcquireATALock(MODE_BLOCK);
    fSuccess = Pkt_PIODataIn(
                Drive, ATAPICmdPacket[ATAPICMD_ModeSense], 
                ByteCount, Buffer, ByteReturned);
    ReleaseATALock();

    return fSuccess;
}

/********************************************************************
 ATA_ReadDVDStructure - Execute atapi ReadDVDStructrue command.
 Note: 
********************************************************************/
BOOL ATA_ReadDVDStructure(int Drive, BYTE Layer, BYTE AGID, BYTE Format, 
					DWORD ByteCount, BYTE *Buffer, DWORD *ByteReturned)
{
    BOOL fSuccess;
    ATAPICmdPkt Command;
    BYTE *cptr = (BYTE *)&Command;

    ASSERT(NULL != Buffer && NULL != ByteReturned);

	memcpy(&Command, &ATAPICmdPacket[ATAPICMD_ReadDVDStruct], 
	        sizeof(ATAPICmdPkt));
	cptr[6] = Layer;
	cptr[7] = Format;
	cptr[10] = AGID & 0xC0;
	cptr[8] = (BYTE)(ByteCount >> 8);
	cptr[9] = (BYTE)ByteCount;

    AcquireATALock(MODE_BLOCK);
    fSuccess = Pkt_PIODataIn(
                Drive, Command, ByteCount, Buffer, ByteReturned);
    ReleaseATALock();

    return fSuccess;
}

/********************************************************************
 ATA_ReportKey - Execute atapi ReportKey command.
 Note: 
********************************************************************/
BOOL ATA_ReportKey(int Drive, BYTE AGID, BYTE Format, DWORD Lba, 
				   DWORD ByteCount, BYTE *Buffer, DWORD *ByteReturned)
{
    BOOL fSuccess;
    ATAPICmdPkt Command;
    BYTE *cptr = (BYTE *)&Command;

    ASSERT(NULL != Buffer && NULL != ByteReturned);

	memcpy(&Command, &ATAPICmdPacket[ATAPICMD_ReportKey], sizeof(ATAPICmdPkt));
	cptr[10] = (AGID & 0xC0) | (Format & 0x3F);
	cptr[5] = (BYTE)(Lba & 0x000000FF);   // LSB
	cptr[4] = (BYTE)((Lba >> 8) & 0x000000FF);
	cptr[3] = (BYTE)((Lba >> 16) & 0x000000FF);
	cptr[2] = (BYTE)((Lba >> 24) & 0x000000FF);   // MSB

	switch(Format)
	{
	case KEY_AGID:
		cptr[ 9 ] = 8;
		cptr[ 8 ] = 0;
		break;

	case KEY_ChallengeKey:
		cptr[ 9 ] = 16;
		cptr[ 8 ] = 0;
		break;

	case KEY_Key1:
		cptr[ 9 ] = 12;
		cptr[ 8 ] = 0;
		break;

	case KEY_TitleKey:
		cptr[ 9 ] = 12;
		cptr[ 8 ] = 0;
		break;

    default:
        break;
	}

    AcquireATALock(MODE_BLOCK);
    fSuccess = Pkt_PIODataIn(
                Drive, Command, ByteCount, Buffer, ByteReturned);
    ReleaseATALock();

    return fSuccess;
}

/********************************************************************
 ATA_SendKey - Execute atapi SendKey command.
 Note: 
********************************************************************/
BOOL ATA_SendKey(int Drive, BYTE AGID, BYTE Format, DWORD ByteCount, 
				 BYTE *Buffer, DWORD *ByteReturned)
{
    BOOL fSuccess;
    ATAPICmdPkt Command;
    BYTE *cptr = (BYTE *)&Command;

    ASSERT(NULL != Buffer && NULL != ByteReturned);

	memcpy(&Command, &ATAPICmdPacket[ATAPICMD_SendKey], sizeof(ATAPICmdPkt));
	cptr[10] = (AGID & 0xC0) | (Format & 0x3F);
	cptr[8] = (BYTE)((ByteCount >> 8) & 0x000000FF); // MSB
	cptr[9] = (BYTE)((ByteCount & 0x000000FF));      // LSB

    AcquireATALock(MODE_BLOCK);
    fSuccess = Pkt_PIODataOut(
                Drive, Command, ByteCount, Buffer, ByteReturned);
    ReleaseATALock();

    return fSuccess;
}

/********************************************************************
 ATA_ReadCD - Execute atapi command READ_CD.
 Note: 
********************************************************************/
BOOL ATA_ReadCD(int Drive, DWORD StartLba, DWORD SectorCount, DWORD ByteCount, 
				BYTE *Buffer, DWORD *ByteReturned)
{
    BOOL fSuccess;
    ATAPICmdPkt Command;

    memcpy(&Command, &ATAPICmdPacket[ATAPICMD_ReadCD], sizeof(ATAPICmdPkt));
    SET_READCD_PARAM(Command, StartLba, SectorCount);

    AcquireATALock(MODE_BLOCK);
    fSuccess = Pkt_PIODataIn(Drive, Command, ByteCount, Buffer, ByteReturned);
    ReleaseATALock();

    return fSuccess;
}

/********************************************************************
 ATA_Read12 - Execute atapi command READ_12.
 Note: 
********************************************************************/
BOOL ATA_Read12(int Drive, DWORD StartLba, DWORD SectorCount, 
				DWORD ByteCount, BYTE *Buffer, DWORD *ByteReturned)
{
    BOOL fSuccess;
    ATAPICmdPkt Command;

    memcpy(&Command, &ATAPICmdPacket[ATAPICMD_Read_12], sizeof(ATAPICmdPkt));
    SET_READ12_PARAM(Command, StartLba, SectorCount);
    AcquireATALock(MODE_BLOCK);
    fSuccess = Pkt_PIODataIn(
            Drive, Command, 
            ByteCount, Buffer, ByteReturned);
    ReleaseATALock();

    return fSuccess;
}

static void SetATAReadCmd(DWORD start, WORD num, WORD * Command, int Type)
{
	if(Type != 0)
	{
		// Read CD 2352 bytes
		Command[0] = 0xbe;
		Command[1] = ((start & 0xff0000) >> 8) | ((start & 0xff000000) >>24);
		Command[2] = ((start & 0xff) << 8) | ((start & 0xff00) >> 8);
		Command[3] = (num & 0xff00)  | ((num & 0xff0000)>>16);//0xff00;
		if(Type == 1)
			Command[4] = 0x1000 | (num & 0xff);
		else
			Command[4] = 0xf800 | (num & 0xff);
	}
	else
	{
		memcpy(Command, &ATAPICmdPacket[ATAPICMD_Read_12], sizeof(ATAPICmdPkt));
		SET_READ12_PARAM(Command, start, num);
	}
}

void InitChip(void)
{
    AIO_Init();
}

BOOL InitATA(int Drive)
{
	static WORD IdentifyData[256];
	DWORD DataLength;
	ATAPIOMode eMode = PIO_MODE0;

    if(!SoftReset(Drive))
    {
		TRACE("Soft reset fail!!!\n");
        ASSERT(0);
        return FALSE;
    }
    if(!WaitLoaderReadyAfterReset(0))
    {
        ASSERT(0);
        return FALSE;
    }

    if(!Identify(Drive, sizeof(IdentifyData), (BYTE *)IdentifyData, &DataLength))
    {
        ASSERT(0);
        return FALSE;
    }

//	TRACE2("Identify: Word[51]=0x%x, Word[64]=0x%x\n", IdentifyData[51], IdentifyData[64]);

    if(!(IdentifyData[0] & 0x03))
        m_AtapiPacketSize = 12;
    else
        m_AtapiPacketSize = 16;

#if 0
//	eMode = (IdentifyData[51] >> 8) & 0x0F;
    eMode = IdentifyData[64] & 0xFF;

#else
    if(IdentifyData[64] & 0x01)
        eMode = PIO_MODE3;
    if(IdentifyData[64] & 0x02)
        eMode = PIO_MODE4;
#endif

	TRACE1("Identify: PIO mode%d found.\n", eMode);

    if(!SetPIOMode(Drive, eMode))
        ASSERT(0);

    return TRUE;
}

BOOL ATA_StartReading(int StartLba, int numSectors, int Type)
{
	static WORD ReadCmdPkt[6];

    if(!AcquireATALock(MODE_NONBLOCK))
	{
		TRACE("Acquire ATA Lock fail!\n");
		ASSERT(0);
		return FALSE;
	}

	SetATAReadCmd(StartLba, numSectors, ReadCmdPkt, Type);

	AIO_SetDefault();

	if(!SelectDrive(0))
	{
		TRACE("Select drive error!\n");
		ReleaseATALockInISR();
		return FALSE;
	}

	outportbi(ATAREG_DeviceControl,	0);
#ifdef DEVICE_USE_DMA
	outportbi(ATAREG_Features, 1);
#else
	outportbi(ATAREG_Features, 0);
#endif
	outportbi(ATAREG_InterruptReason, 0);
	outportbi(ATAREG_TAG, 0);
	outportbi(ATAREG_ByteCountLow, 0xFF);	/* Setup transfer data quanity to max */ 
	outportbi(ATAREG_ByteCountHigh, 0xFF);
	outportbi(ATAREG_Command, 0xA0);		/* Issue packet Command */

	if(!CheckBusyStatusClear(DEFAULT_DELAY_TIME))
	{
		TRACE("Ide busy!\n");
		ReleaseATALockInISR();
		return FALSE;
	}

	WriteCommandPacket(ReadCmdPkt);

	return TRUE;
}

int ATA_GetReadingStatus(void)
{
	BYTE status, intrdata;

	status = inportbi(ATAREG_Status);
	if (status & ATASR_BSY)
		return 0;

	intrdata = inportbi(ATAREG_InterruptReason);
	intrdata &= (ATAIRR_IN | ATAIRR_CoD);

	if (status & ATASR_DRQ)
	{
		if(intrdata == ATAIRR_IN)
		{	
			/* Input, Data */
			return 0x01;
		}
		else if(intrdata == 0)
		{
			/* Output, Data */
			TRACE1("DRQ: status = %02x, intrdata = 0 !!!\n", status);
			// ASSERT(0);
		}
		else
		{
			// TRACE2("DRQ: status=%02x, intrdata=%02x !!!\n", status, intrdata);
			// ASSERT(0);
		}
	}	
	else if (status & ATASR_DRDY)
	{	
		if (intrdata == (ATAIRR_IN | ATAIRR_CoD))
		{
			ReleaseATALockInISR();
			if(!CheckErrorStatus())
				return 0x03;
			else
				return 0x02;
		}
		else
		{
			TRACE1("DRDY: intrdata = %02x !!!\n", intrdata);
			// ASSERT(0);
		}
	}

	return 0;
}
