
how to build the BIOS
=====================

1. if need be, build your IR and VFD library according to the document 
   "\IR_VFD_custom_v3.1\readme.txt".

2. if need be, copy your IR library to "bios_custom_t6305(6)_v3.36\BIOS\Libs\irc\"
   if need be, copy your VFD library to "bios_custom_t6305(6)_v3.36\BIOS\Libs\panel\"

3. modify the file "bios_custom_t6305(6)_v3.36\BIOS\makefile" to custom your BIOS:
   1) change audio chip type, the default is "DAC_ALC650";
   2) change the BIOS name;
   3) change the ir and vfd library name according to your copy on item 2;
   4) if need be, change the common library "common_log_disable.a" to "common_log_enable.a",
      the "common_log_enable.a" support the print message to the WinICE output window by 
      calling the function "soc_printf". but it only for debug, NOT for release.

4. enter the directory "BIOS\" in Cygwin, run "make" to build the BIOS, you will get the 
   BIOS in "bios_custom_t6305(6)_v3.36\".
