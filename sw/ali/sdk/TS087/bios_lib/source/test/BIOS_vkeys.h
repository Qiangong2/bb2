#ifndef __BIOS_VKEYS_H_
#define __BIOS_VKEYS_H_
/////ITEM is just used to select which light pixel according vfd spec

enum BIOS_VITUAL_KEYS
{
	BIOS_VKEY_NULL=0,
	BIOS_VKEY_POWER,
	BIOS_VKEY_EJECT,		//          1
	BIOS_VKEY_PLAY,		//          2
	BIOS_VKEY_STOP, 	 	//          3
	BIOS_VKEY_STEP,        	//          4
	BIOS_VKEY_LASTPLAY,    	//          5
	BIOS_VKEY_PREV,         	//          6
	BIOS_VKEY_NEXT,        	//          7
	BIOS_VKEY_FF,          	//          8
	BIOS_VKEY_REV,          	//          9
	BIOS_VKEY_UP,           	//          10               
	BIOS_VKEY_DOWN,         	//  	11               
	BIOS_VKEY_LEFT,         	//  	12               
	BIOS_VKEY_RIGHT,        	//         13               
	BIOS_VKEY_MENU,         	//         14               
	BIOS_VKEY_TITLE,       	//          15               
	BIOS_VKEY_ANGLE,       	//          16               
	BIOS_VKEY_WARM_START,	//            17	/* ERRUIMSG */            
	BIOS_VKEY_COLD_START,   	//         18	/* ERRUIMSG */               
	BIOS_VKEY_FILEOPEN,	//		19	/* Call file open Menu */        
 	BIOS_VKEY_CONSOLE,      //        20               
	BIOS_VKEY_ENTER,        //         21               
	BIOS_VKEY_DVIEW,        //         22                                
	BIOS_VKEY_SETUP,       //          23                                
	BIOS_VKEY_RESUME,      //          24                                
	BIOS_VKEY_RDM_SHFL,   //           25
	BIOS_VKEY_ZOOM,        //          26                                
	BIOS_VKEY_SEARCH,     //           27                                
	BIOS_VKEY_SLOW,              //    28                                
	BIOS_VKEY_SUBP,             //     29                                
	BIOS_VKEY_SUBP_ONOFF,      //      30                                
	BIOS_VKEY_VOICE,           //      31                                
	BIOS_VKEY_REPEAT,          //      32                                
	BIOS_VKEY_REPEAT_AB,       //      33                                
	BIOS_VKEY_KARA_ONOFF,       //     34                                
	BIOS_VKEY_MODE,            //      35  /* ERRUIMSG */  
        BIOS_VKEY_KEYUP,           //      36	/* ERRUIMSG */
	BIOS_VKEY_KEYDOWN,         //      37	/* ERRUIMSG */                
	BIOS_VKEY_AUDIO,           //      38                                
	BIOS_VKEY_VOCAL,           //      39                                
	BIOS_VKEY_CLEAR,	//	    40                               
	BIOS_VKEY_0,          //           41                                
	BIOS_VKEY_1,            //         42                                
	BIOS_VKEY_2,             //        43                                
	BIOS_VKEY_3,             //        44                                
	BIOS_VKEY_4,      //               45                                
	BIOS_VKEY_5,      //               46                                
	BIOS_VKEY_6,      //               47                                
	BIOS_VKEY_7,     //                48                                
	BIOS_VKEY_8,     //                49                                
	BIOS_VKEY_9,     //                50
	BIOS_VKEY_10,	//	    51
	BIOS_VKEY_PROGRAM,	//	    52
	BIOS_VKEY_EQ,         //           53  /*claire--8/18*/
	BIOS_VKEY_MICVALUP,   //           54  /*claire--8/18*/
	BIOS_VKEY_MICVALDOWN, //           55  /*claire--8/18*/
	BIOS_VKEY_MUTE,       //           56  /*claire---8/29*/ 
	BIOS_VKEY_LANGU,      //           57   /*claire---8/31*/      
	BIOS_VKEY_KARAECHO_ONOFF,  //      58  /*Mickey add 8/25*/
	BIOS_VKEY_ECHOUP,          //      59
	BIOS_VKEY_ECHODOWN,        //      60
	BIOS_VKEY_ASSIST,          //      61
	BIOS_VKEY_VOLUP,          //       62
	BIOS_VKEY_VOLDOWN,        //       63
	BIOS_VKEY_PBC_ON,        //        64
	BIOS_VKEY_PBC_OFF,       //        65 
	BIOS_VKEY_PBC_ONOFF,     //        66
	BIOS_VKEY_NOUSE,         //        67	/* ERRUIMSG */
	BIOS_VKEY_GAME,          //        68  
	BIOS_VKEY_CONFIRM,		//    72
	BIOS_VKEY_PAUSE,		//    73
	BIOS_VKEY_BOOKMARK,          //    74
	BIOS_VKEY_CLOSE,	//	    75	/* 101  */

	// Internal keys NOT used by applications but only within
	// BIOS
	BIOS_INTERNAL_VKEY_OPEN = 0x1000       
};

#endif
