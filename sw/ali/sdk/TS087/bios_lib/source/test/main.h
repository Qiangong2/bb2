#ifndef __MAIN_H
#define __MAIN_H
//#define DEBUG

#define CDBUFF_ADDRESS		0xa0200000
#define CDBUFF_SIZE			120			// number of sectors

#define IO_BASE				0xb8000000
#define INTR_STATUS			(IO_BASE + 0x0038)
#define INTR_CTRL			(IO_BASE + 0x003c)
#define INTR_CTRL1			(IO_BASE + 0x0040)

#define IDE_INTR			(1 << 19)

#define SET_PCI_MODE()		{ \
	WRITE_DWORD(0xb8000058, READ_DWORD(0xb8000058) | (1 << 14)); \
	WRITE_DWORD(0xb8000054, READ_DWORD(0xb8000054) | (1 << 14)); \
	WRITE_DWORD(0xb8000074, (READ_DWORD(0xb8000074) | (1 << 26)) | (1 << 25)); \
			}

#define SET_IDE_MODE()		{ \
	WRITE_DWORD(0xb8000074, (READ_DWORD(0xb8000074) | (1 << 26)) & ~(1 << 25)); \
	WRITE_DWORD(0xb8000058, READ_DWORD(0xb8000058) | (1 << 14)); \
	WRITE_DWORD(0xb8000054, READ_DWORD(0xb8000054) & ~(1 << 14)); \
			}

#if (_P_TYPE == SONY_T2GVCD)
	#define	GFS				(1<<8)
	#define	SENSE			(1<<9)
	#define	XLAT			(1<<10)
	#define	XRST			(1<<11)
	#define	SPEED_1X_2X		(1<<17)
	#define	AGC				(1<<18)
	#define	LDON			(1<<19)
	#define	FOK				(1<<14)
	#define	CLSW			(1<<15)
	#define	OPSW			(1<<16)
	#define	OPEN			(1<<20)
	#define	CLOSE			(1<<21)
	#define GSI_CLK			(1<<7)
	#define GSI_DATA		(1<<6)

#elif (_P_TYPE == PANASONIC_GODAS)
    #define XRST	(1<<22)       
    #define LIMIT	(1<<20)        
    #define OPSW    (1<<17)
    #define CLSW	(1<<16)
    #define CLOSE	(1<<19)        
    #define OPEN	(1<<18)       
    #define MCLK	(1<<27)        
    #define MLAT	(1<<25)        
    #define MDATA	(1<<26)            
    #define SENSE	(1<<24)    
    #define STAT	(1<<23)

#endif

extern void EnableTimerIntr(void);
extern void EnableIP4(void);
extern void DisableIP4(void);

#endif

