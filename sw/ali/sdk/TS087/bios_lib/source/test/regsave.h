
/* define the memory location for saveing registers when our cop2 exception occurs*/
/*
	.bss
	.align	4
	.globl  _exp_reg_save
	.lcomm	_exp_reg_save,64<<4
*/

#define EXP_STACKSIZE	64

#define _exp_save_r0 0

#define _exp_save_at 1

#define _exp_save_v0 2

#define _exp_save_v1 3

#define _exp_save_a0 4

#define _exp_save_a1 5

#define _exp_save_a2 6

#define _exp_save_a3 7

#define _exp_save_t0 8

#define _exp_save_t1 9

#define _exp_save_t2 10

#define _exp_save_t3 11

#define _exp_save_t4 12

#define _exp_save_t5 13

#define _exp_save_t6 14

#define _exp_save_t7 15

#define _exp_save_s0 16

#define _exp_save_s1 17

#define _exp_save_s2 18

#define _exp_save_s3 19

#define _exp_save_s4 20

#define _exp_save_s5 21

#define _exp_save_s6 22

#define _exp_save_s7 23

#define _exp_save_t8 24

#define _exp_save_t9 25

#define _exp_save_k0 26

#define _exp_save_k1 27

#define _exp_save_gp 28

#define _exp_save_sp 29

#define _exp_save_fp 30

#define _exp_save_ra 31


/* COP0 */
#define _exp_save_sr 32

#define _exp_save_cause 33
#84
#define _exp_save_epc 34
#88
#define _exp_save_hi 35

#define _exp_save_lo 36

#define _exp_save_BadVaddr	37
#94
#define BP_NO_STICKY     0xb0d
#define BP_NO_STICKY     0xb0d
#define BP_NO_STICKY     0xb0d


