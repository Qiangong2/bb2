/***************************************************************************
	BIOS - Application library - Panel library
	T-Square, Zhuhai
	Started by Bagio, 2002-10-28
 ***************************************************************************/

#ifndef __BIOS_PANEL_H
#define __BIOS_PANEL_H

#include "BIOS_vkeys.h"
#include "BIOS_panel_defines.h"

#ifdef __cplusplus
extern "C" {
#endif

/********************************************************************
	Functions
 ********************************************************************/

/********************************************************************
 BIOS_PANEL_Init - Initialize the panel library

 Supports: T630x
 ********************************************************************/
void BIOS_PANEL_Init(void);


/********************************************************************
 BIOS_PANEL_Update - Update the front panel

 Supports: T630x

 Notes:
   The application must be called this function every frame
 ********************************************************************/
void BIOS_PANEL_Update(void);


/********************************************************************
 BIOS_PANEL_SetItem - Set the status of an item on the panel

 Supports: T630x

 Params:
    item              - Item code (use BIOS_ITEM_* macros)
	status            - Status (use BIOS_ITEM_STATUS_* macros)
	customParm        - Custom parameter for the item

 Notes:
    The requested item change may not be completed until next 
	BIOS_PANEL_Update() call
 ********************************************************************/
void BIOS_PANEL_SetItem(int item, int onoff, void* customParm);


/********************************************************************
 BIOS_PANEL_CheckKey - Check if requested ley is pressed 

 Supports: T630x

 Params:
    key               - Key that we want to test

 Returns:
    0 if the key is pressed, 1 if the key is pressed
 ********************************************************************/
unsigned int BIOS_PANEL_CheckKey(unsigned long key);


#ifdef __cplusplus
}
#endif

#endif
