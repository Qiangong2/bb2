#include <common.h>
#include <BIOS_cdr.h>
#include "main.h"
#include "BIOS_panel.h"
#include "toc.h"

#define READ_TRACK1_ONLY	0
#define READ_FORWARD		0
#define JUMP_ONLY			0
#define TEST_WITH_PANEL		1
#define VIRTUAL_PANEL		1

int BIOS_GSI_Locked = 0;
DWORD BIOS_PHYSICAL_ADDRESS = 0;	// for test

static BYTE cdbuff[2352*1];
static CDR_TOC Toc;
static DWORD LeadoutLBA;

extern void CDLServoJump(BYTE track,BYTE * msf);

void SectorPrint(BYTE * buf)
{
	int i;
	BYTE mm,ss,ff;

	mm = buf[12]; ss = buf[13]; ff = buf[14];
	soc_printf("Sector(%x:%x:%x) data :\n",mm,ss,ff);

	for(i=0; i<2352; i++)
	{
		if(i>0 && (i%30)==0) soc_printf("\n");
		soc_printf("%02X ",buf[i]);
	}
	soc_printf("\n");
}

void TimerHandler(void)
{
	BIOS_CDR_TimerHandler();
}

void process_external_interrupt(void)
{
#ifdef _IDE
	if((READ_DWORD(INTR_STATUS) >> 8) & IDE_INTR)
	{
//		soc_printf("IDE interrupt\n");
		BIOS_CDR_InterruptHandler();
	}
	else
	{
		soc_printf("error extern interrupt !!!\n");
	}
#endif
}

void Test_GetToc(void)
{
	int i;
	BYTE mm, ss, ff;

#ifdef _IDE
	int MediumType = BIOS_DVD_GetMediumType();
	if(DVD_Medium == MediumType)
	{
		LeadoutLBA = 4500 * 60;
		return;
	}
	else if(CD_CDDA == MediumType)
	{
		static CD_TEXT CDText;
		int PackCount;

		if(BIOS_CDR_GetCdText(&CDText))
		{
			soc_printf("\nCD_Text pack list\n");
			soc_printf("-----------------\n");
			soc_printf("DataLen = %d\n", CDText.DataLen);
			soc_printf("\n No   PType  TrackNo EF    SN   CP    BN    DBCC  Text\n");
			soc_printf("-------------------------------------------------------\n");
			
			PackCount = (CDText.DataLen - 2)/18;
			for(i = 0; i < PackCount; i++)
			{
				CDText.TextDscp[i].CRC0 = 0;
				CDText.TextDscp[i].CRC1 = 0;
				soc_printf(" %02d   %02x     %02d      %d    %03d", i,
					CDText.TextDscp[i].PackType,
					CDText.TextDscp[i].TrackNumber,
					CDText.TextDscp[i].EF,
					CDText.TextDscp[i].SequenceNumber);
				soc_printf("   %02d    %d     %d     %s\n\n", 
					CDText.TextDscp[i].CharacterPosition,
					CDText.TextDscp[i].BlockNumber,
					CDText.TextDscp[i].DBCC,
					CDText.TextDscp[i].TextData);
			}
			soc_printf("-------------------------------------------------------\n");
		}
	}
#endif

	CDLGetToc(&Toc);

	soc_printf("Toc information:\n");
	soc_printf("first track = %d, last track = %d\n", Toc.FirstTrk, Toc.LastTrk);

	for(i = Toc.FirstTrk; i <= Toc.LastTrk; i++)
	{
		mm = Toc.TrkDscp[i].mm;
		ss = Toc.TrkDscp[i].ss;
		ff = Toc.TrkDscp[i].ff;
		soc_printf("track %02d : %02d:%02d:%02d\n", i, mm, ss, ff);
	}
	
	mm = Toc.LeadOut[0];
	ss = Toc.LeadOut[1];
	ff = Toc.LeadOut[2];
	soc_printf("lead out : %02d:%02d:%02d\n\n", mm, ss, ff);

	LeadoutLBA = MSF2LBA(mm, ss, ff);
}

#define PRINT_POSITION(p) soc_printf("reading pos: %02x:%02x:%02x\n", p[0], p[1], p[2])

void Test_ReadSector(BOOL Forward, BOOL Brocking, BOOL FirstTrackOnly)
{
	DWORD i;
	DWORD StartLBA, EndLBA;
	DWORD diff = 10*75;
	BYTE * Buff;
	BYTE msf[3];
	int mode = CDR_MODE_2352;

READ_BEGIN:
	StartLBA = 16;
#ifdef _IDE
	if(DVD_Medium == BIOS_DVD_GetMediumType())
		mode = CDR_MODE_2048;
#endif

	if(FirstTrackOnly && (Toc.FirstTrk != Toc.LastTrk))
		EndLBA = MSF2LBA(Toc.TrkDscp[2].mm, Toc.TrkDscp[2].ss, Toc.TrkDscp[2].ff) - 150;
	else
		EndLBA = LeadoutLBA;

	soc_printf("Reading data...\n");

	if(Forward && !Brocking)
	{
		BIOS_CDR_StartReading(StartLBA, mode);
		for( i = StartLBA; i < EndLBA; i++)
		{
			if(i % 750 == 0)
			{
				LBAToMSF(i, msf, BCD_CODE);
				PRINT_POSITION(msf);
			}

			BIOS_CDR_GetSector(&Buff);
			if (BIOS_PANEL_CheckKey(-1))
				return;
		}
	}
	else if	(Forward && Brocking)
	{
		for(i = StartLBA; i < EndLBA; i++)
		{
			if(i % 750 == 0)
			{
				LBAToMSF(i, msf, BCD_CODE);
				PRINT_POSITION(msf);
			}
			if(!BIOS_CDR_ReadSectors(cdbuff, i, 1, CDR_MODE_2048))
				soc_printf("read fail at lba = %d\n",i);
			if (BIOS_PANEL_CheckKey(-1))
				return;
		}
	}
	else
	{
		if(LeadoutLBA < diff)
			diff = 1*75;
		EndLBA = LeadoutLBA - diff;

		while(StartLBA < EndLBA)
		{
			BIOS_CDR_StartReading(StartLBA, mode);
			for( i = StartLBA; i < StartLBA + diff; i++)
			{
				if(i % 750 == 0)
				{
					LBAToMSF(i, msf, BCD_CODE);
					PRINT_POSITION(msf);
				}

				BIOS_CDR_GetSector(&Buff);
				if (BIOS_PANEL_CheckKey(-1))
					return;
			}

			BIOS_CDR_StartReading(EndLBA, mode);
			for( i = EndLBA; i < EndLBA + diff; i++)
			{
				if(i % 750 == 0)
				{
					LBAToMSF(i, msf, BCD_CODE);
					PRINT_POSITION(msf);
				}

				BIOS_CDR_GetSector(&Buff);
				if (BIOS_PANEL_CheckKey(-1))
					return;
			}

			StartLBA += diff;
			EndLBA -= diff;
		}
	}

	goto READ_BEGIN;
	soc_printf("read finish!\n");
}

#if(JUMP_ONLY && !defined(_IDE))
void Test_Jump(void)
{
	DWORD StartLBA, EndLBA;
	DWORD diff = 10*75;
	BYTE msf[3];

	StartLBA = 16;

	soc_printf("Jump begin...\n");

	if(LeadoutLBA < diff)
		diff = 1*75;
	EndLBA = LeadoutLBA - diff;

	while(StartLBA < EndLBA)
	{
		LBAToMSF(StartLBA, msf, BCD_CODE);
		CDLServoJump(0,msf);

		soc_printf("jumping pos: %02x:%02x:%02x\n", msf[0], msf[1], msf[2]);

		delay_ms(3000);

		LBAToMSF(EndLBA, msf, BCD_CODE);
		CDLServoJump(0,msf);
		soc_printf("jumping pos: %02x:%02x:%02x\n", msf[0], msf[1], msf[2]);

		StartLBA += diff;
		EndLBA -= diff;
	}

	soc_printf("Jump finish!\n");
}
#endif

#if VIRTUAL_PANEL

#define PANEL_ADDR		0x807ffff0
#define EJECT			(1<<0)
#define PLAY			(1<<1)
#define STOP			(1<<2)
#define PREV			(1<<3)
#define NEXT			(1<<4)
#define TOP_OPEN		(1<<5)

void BIOS_PANEL_Init(void) 
{
	WRITE_DWORD(PANEL_ADDR, 0);
}
void BIOS_PANEL_SetItem(int item, int onoff, void* customParm) {}
void BIOS_PANEL_Update(void) {}

unsigned int BIOS_PANEL_CheckKey(unsigned long key)
{
	unsigned int result = 0;

	switch (key) 
	{
	case BIOS_VKEY_EJECT:
		result = (READ_DWORD(PANEL_ADDR) & EJECT);
		break;
	case BIOS_VKEY_PLAY:
		result = (READ_DWORD(PANEL_ADDR) & PLAY);
		break;
	case BIOS_VKEY_STOP:
		result = (READ_DWORD(PANEL_ADDR) & STOP);
		break;
	case BIOS_VKEY_PREV:
		result = (READ_DWORD(PANEL_ADDR) & PREV);
		break;
	case BIOS_VKEY_NEXT:
		result = (READ_DWORD(PANEL_ADDR) & NEXT);
		break;
	case BIOS_INTERNAL_VKEY_OPEN:
		result = (READ_DWORD(PANEL_ADDR) & TOP_OPEN);
		break;
	default:
		result = (READ_DWORD(PANEL_ADDR));
		return result;
	}
	if (result)
		WRITE_DWORD(PANEL_ADDR, 0);
	return result;
}

#endif

void Test_WithPanel(void)
{
	BOOL TrayOpen = FALSE;
	BOOL IsStop = FALSE;
	BOOL IsNewStart = FALSE;
	BOOL IsStarting = FALSE;
	BOOL StartOK = FALSE;
	int TrackNo = 1;
	int TrackTotal = 15;

	BIOS_PANEL_Init();
	BIOS_PANEL_SetItem(BIOS_ITEM_GAME, 1, 0);

	while(1)
	{
		if(BIOS_PANEL_CheckKey(BIOS_VKEY_EJECT))
		{
			TrayOpen = !TrayOpen;
			if(TrayOpen)
			{
				StartOK = FALSE;
				IsNewStart = FALSE;
				BIOS_CDR_OpenTray(CDR_TRAY_OPEN);
				soc_printf("tray open...\n");
			}
			else
			{
				BIOS_CDR_OpenTray(CDR_TRAY_CLOSE);
				soc_printf("tray close...\n");
			}
		}
		else if(BIOS_PANEL_CheckKey(BIOS_VKEY_STOP))
		{
			extern void CDLServoStop();
			if(StartOK && !IsStop)
			{
				soc_printf("cd stop...\n");
				IsStop = TRUE;
				#if 1
				#ifndef _IDE
					CDLServoStop();
				#endif
				#else
					BIOS_CDR_Stop();
				#endif
			}
		}
		else if(BIOS_PANEL_CheckKey(BIOS_VKEY_PLAY))
		{
			if(StartOK && IsStop)
			{
				soc_printf("cd play...\n");
				#if 1
					//BIOS_CDR_StartReading((LeadoutLBA / TrackTotal) * (TrackNo-1), CDR_MODE_2352);
					Test_ReadSector(READ_FORWARD, FALSE, READ_TRACK1_ONLY);
				#else
					BIOS_CDR_Init((BYTE *)CDBUFF_ADDRESS, CDBUFF_SIZE);
					if(!BIOS_CDR_Start())
						soc_printf("no disc !!!\n");
					Test_GetToc();
					BIOS_CDR_SetSpeed(CDR_SPEED_2X);
					BIOS_CDR_StartReading(16);
				#endif
				IsStop = FALSE;
			}
		}
		else if(BIOS_PANEL_CheckKey(BIOS_VKEY_PREV))
		{
			if(StartOK && !IsStop)
			{
				if(TrackNo == 1)
					TrackNo = TrackTotal;
				else
					TrackNo--;
				BIOS_CDR_StartReading((LeadoutLBA / TrackTotal) * (TrackNo-1), CDR_MODE_2352);
				soc_printf("cd prev: track no=%d\n", TrackNo);
			}
		}
		else if(BIOS_PANEL_CheckKey(BIOS_VKEY_NEXT))
		{
			if(StartOK && !IsStop)
			{
				if(TrackNo == TrackTotal)
					TrackNo = 1;
				else
					TrackNo++;
				BIOS_CDR_StartReading((LeadoutLBA / TrackTotal) * (TrackNo-1), CDR_MODE_2352);
				soc_printf("cd next: track no=%d\n", TrackNo);
			}
		}
		else if(BIOS_PANEL_CheckKey(BIOS_INTERNAL_VKEY_OPEN))
		{
			StartOK = FALSE;
			IsNewStart = FALSE;
//			soc_printf("cd top open...\n");
		}
		
		switch(BIOS_CDR_GetStatus())
		{
		case CDR_IS_OPENED:
			StartOK = FALSE;
			IsNewStart = FALSE;
//			soc_printf("cd open!\n");
			break;
		case CDR_IS_CLOSED:
			if(!IsNewStart)
			{
				soc_printf("tray closed.\n");
				IsNewStart = TRUE;
				IsStarting = TRUE;
			}
			break;
		case CDR_IS_OPENING:
			StartOK = FALSE;
			IsNewStart = FALSE;
			break;
		case CDR_IS_CLOSING:
			break;
		case CDR_IS_PUSHED:
			BIOS_CDR_OpenTray(CDR_TRAY_CLOSE);
			TrayOpen = !TrayOpen;
			soc_printf("tray close...\n");
			break;
		}

		if(IsNewStart && IsStarting)
		{
			/*
			if(!BIOS_CDR_Start())
				soc_printf("no disc !!!\n");
			else
			{
				TrackNo = 1;
				StartOK = TRUE;
				Test_GetToc();
				BIOS_CDR_SetSpeed(CDR_SPEED_2X);
				soc_printf("cd start ok!\n");
				BIOS_CDR_StartReading(16);
			}
			*/
			int StartResult = BIOS_CDR_StartNonBlocking();

			if(StartResult == 0)
			{
				IsStarting = FALSE;
				soc_printf("no disc !!!\n");
			}
			else if(StartResult == 1)
			{
				IsStarting = FALSE;
				TrackNo = 1;
				StartOK = TRUE;
				Test_GetToc();
				BIOS_CDR_SetSpeed(CDR_SPEED_2X);
				soc_printf("cd start ok.\n");
			#if VIRTUAL_PANEL
				WRITE_DWORD(PANEL_ADDR, PLAY);
				IsStop = TRUE;
			#else
				BIOS_CDR_StartReading(16, CDR_MODE_2352);
			#endif
			}
		}

		BIOS_PANEL_Update();
		delay_ms(150);
	}
}

void Test_Normal(void) 
{
	if(!BIOS_CDR_Start())
	{
		LEDOUT(0x99);
		soc_printf("no disc!!!\n");
		while(1);
	}
	soc_printf("cd start ok.\n");
	
	Test_GetToc();
	
	soc_printf("set double speed.\n");
	BIOS_CDR_SetSpeed(CDR_SPEED_2X);
	
	// Test_Library();
	
#if(JUMP_ONLY && !defined(_IDE))
	// Test_Jump();
#endif
	Test_ReadSector(READ_FORWARD, FALSE, READ_TRACK1_ONLY);
}

int mips_main(void)
{
#if defined(SERVO_PHILIPS)
	soc_printf("\n----CD Loader(philips) test begin----\n\n");
#elif defined(SERVO_SONY)
	soc_printf("\n----CD Loader(sony) test begin----\n\n");
#elif defined(SERVO_SAMSUNG)
	soc_printf("\n----CD Loader(samsung) test begin----\n\n");
#elif defined(SERVO_PANASONIC)
	soc_printf("\n----CD Loader(panasonic) test begin----\n\n");
#elif defined(_IDE)
	soc_printf("\n----Ide test begin----\n\n");
#endif
	LEDOUT(0x01);
	EnableTimerIntr();

#ifdef _M6304
#ifdef _IDE
	EnableIP4();
	CLEAR_D_BIT(INTR_CTRL1, IDE_INTR);
	SET_D_BIT(INTR_CTRL, IDE_INTR);
	SET_IDE_MODE();
#endif

	// Flash memory access disable
	WRITE_DWORD(0xb8000090, 0x1f0002);
#endif

	BIOS_CDR_Init((BYTE *)CDBUFF_ADDRESS, CDBUFF_SIZE);
	soc_printf("cd init ok!\n");

	LEDOUT(0x02);
#if(TEST_WITH_PANEL)
	Test_WithPanel();
#else
	Test_Normal();
#endif

	soc_printf("\n--CD Loader test end--\n");
	while(1);

	return 0;
}

int bios_cpu_freq = 200000000;

#if (_P_TYPE == SONY_T2GVCD)
static int BIOS_gpio_maptable[] = {
	GFS, SENSE,  XLAT, XRST, SPEED_1X_2X, 
	AGC, LDON, FOK, CLSW, OPSW,
	OPEN, CLOSE, GSI_CLK, GSI_DATA
};

#elif (_P_TYPE == PANASONIC_GODAS)
static int BIOS_gpio_maptable[] = {
	XRST,  LIMIT,  OPSW, CLSW, CLOSE, 
	OPEN, MCLK, MLAT, MDATA, SENSE, STAT
};

#else
static int BIOS_gpio_maptable[] = {
	0
};

#endif

void BIOS_soc_printf(const char * fmt,...)
{
}

int BIOS_get_gpio_pin(int pin)
{
	return BIOS_gpio_maptable[pin];
}

void BIOS_delayclock(unsigned long clk)
{
	DWORD start = ReadCountReg();
	while (ReadCountReg() - start >= clk);
}
