/***************************************************************************
	BIOS - Application library - Panel library defines
	T-Square, Zhuhai
	Started by Bagio, 2002-10-28
 ***************************************************************************/

#ifndef __BIOS_PANEL_DEFINES_H
#define __BIOS_PANEL_DEFINES_H

#ifdef __cplusplus
extern "C" {
#endif

// Items that can be controlled with the panel library
enum BIOS_PANEL_ITEMS
{
	BIOS_ITEM_CD=1,
	BIOS_ITEM_VCD,
	BIOS_ITEM_DTSCD,
	BIOS_ITEM_MP3,
	BIOS_ITEM_SVCD,
	BIOS_ITEM_DVD,	
	BIOS_ITEM_GAME,    //wch: during game, display Rotate circle, play item and "play".
	BIOS_ITEM_DATA,
	BIOS_ITEM_READ,    //wch: display "load"
	BIOS_ITEM_NEXT,
	BIOS_ITEM_FORWARD,
	BIOS_ITEM_BACKWARD,
	BIOS_ITEM_MP4,
	BIOS_ITEM_A,
	BIOS_ITEM_B,
	BIOS_ITEM_REC,     //wch: display "open"
	BIOS_ITEM_MIC,
	BIOS_ITEM_NODISC,  //wch: display "no disc"
	BIOS_ITEM_REPEAT,
	BIOS_ITEM_OPEN,
	BIOS_ITEM_CLOSE
};

// Status of each item
enum BIOS_PANEL_ITEM_STATUS
{
	BIOS_ITEM_STATUS_OFF = 0,
	BIOS_ITEM_STATUS_ON
};

#ifdef __cplusplus
}
#endif

#endif
