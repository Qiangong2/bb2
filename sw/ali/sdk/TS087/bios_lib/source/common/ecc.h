#ifndef __ECC_H
#define __ECC_H

#define ECC_MaxCount		6

#define ConstGFieldLen		255
#define ConstRedundentBytes	2

#define MaxP_Parity			85
#define MaxQ_Parity			51
#define P_ParityLen			24		// (26, 24)RS
#define Q_ParityLen			43		// (45, 43)RS

BOOL CheckSync(BYTE sync[]);
BOOL CheckSync_1(BYTE sync[]);
BOOL CDLSectorECC(BYTE* sector, BYTE * msf);

#endif		// __ECC_H
