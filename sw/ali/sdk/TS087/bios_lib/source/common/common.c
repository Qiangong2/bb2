#include "common.h"

DWORD ReadCountReg(void)
{
#ifndef	WIN32
	unsigned long v;
	
	__asm volatile(
		"mfc0 %0,$9"
		: "=r"(v)
		);
	return v;
#endif
}

void delay_ms(int ms)
{
#ifndef WIN32
	__asm ("				 \
	.set noreorder			;\
	li	 $9,%0				;\
	mult $9,%1				;\
	mfc0 $8,$9				;\
	mflo $9					;\
	1:						;\
	mfc0 $10,$9				;\
	subu $10,$10,$8			;\
	sltu $11,$9,$10			;\
	beqz $11,1b				;\
	nop						;\
	.set reorder			;\
	"
	:: "i"((SOC_CPU_FREQUENCY/2000)), "r"(ms)
	);
#endif
}

void delay_ns(int ns)
{
	DWORD start = ReadCountReg();
	DWORD total = (SOC_CPU_FREQUENCY/2000) * ns / 1000000;
	while((ReadCountReg() - start) < total) ;
}

void LBAToMSF(DWORD Lba, BYTE * Msf, int Code)
{
	DWORD tmp;
	BYTE mm,ss,ff;

	tmp = Lba + 150;
	ff = (BYTE)(tmp % 75);
	mm = (BYTE)(tmp / (60 * 75));
	ss = (BYTE)(((tmp - ff) / 75) % 60);

	if(Code == HEX_CODE)
	{
		Msf[0] = mm;
		Msf[1] = ss;
		Msf[2] = ff;
	}
	else
	{
		Msf[0] = HEX2BCD(mm);
		Msf[1] = HEX2BCD(ss);
		Msf[2] = HEX2BCD(ff);
	}
}

void *memcpy(void *dest, const void *src, int count)
{
	char * d = (char *)dest;
	char * s = (char *)src;

	if(!dest || !src || count <= 0)
		return dest;

	if(((DWORD)d & 0x03) == ((DWORD)s & 0x03))
	{
		while(((DWORD)d & 0x03) && count > 0)
		{
			*d++ = *s++;
			count--;
		}
		while(count >= 4)
		{
			*(DWORD *)d = *(DWORD *)s;
			d += 4;
			s += 4;
			count -= 4;
		}
	}
	while(count > 0)
	{
		*d++ = *s++;
		count--;
	}

	return dest;
}

void *memset(void *dest, int c, int count)
{
	char * d = (char *)dest;
	BYTE s = (BYTE)c;
	DWORD v = s | (s << 8) | (s << 16) | (s << 24);

	if(!dest || count <= 0)
		return dest;

	while(((DWORD)d & 0x03) && count > 0)
	{
		*d++ = s;
		count--;
	}
	while(count >= 4)
	{
		*(DWORD *)d = v;
		d += 4;
		count -= 4;
	}
	while(count > 0)
	{
		*d++ = s;
		count--;
	}

	return dest;
}
