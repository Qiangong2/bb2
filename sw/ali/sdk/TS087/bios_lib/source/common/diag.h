#ifndef __DIAG_H
#define __DIAG_H

//#define _DEBUG

#define soc_printf	cd_printf

void soc_printf(const char *,...);

/*
 *	TRACE macro defined, "LOG_ENABLE" should define in every .c file,
 *  if LOG_ENABLE = 0 , the file's TRACE will be ((void)0).
 */
#if (!defined(_DEBUG) || !LOG_ENABLE)

#define TRACE(s)					((void)0)
#define TRACE1(s, p1)				((void)0)
#define TRACE2(s, p1, p2)			((void)0)
#define TRACE3(s, p1, p2, p3)		((void)0)
#define TRACE4(s, p1, p2, p3, p4)	((void)0)

#else

#define TRACE(s)					soc_printf(s)
#define TRACE1(s, p1)				soc_printf(s, p1)
#define TRACE2(s, p1, p2)			soc_printf(s, p1, p2)
#define TRACE3(s, p1, p2, p3)		soc_printf(s, p1, p2, p3)
#define TRACE4(s, p1, p2, p3, p4)	soc_printf(s, p1, p2, p3, p4)

#endif

/*
 *	ASSERT macro defined
 */
#ifndef _DEBUG

#define ASSERT(e)					((void)0)

#else	// _DEBUG

#ifdef  __cplusplus
extern "C" {
#endif

void soc_assert(int type, const char * file, int line, const char * exp);

#ifdef  __cplusplus
}
#endif

#define ASSERT(e)       ((e) ? (void)0 : soc_assert(1, __FILE__, __LINE__, #e))

#endif	// !_DEBUG


#endif	// __DIAG_H
