/*========================================================================
//
//      diag.c
//
//      Infrastructure diagnostic output code
//
//========================================================================
//####COPYRIGHTBEGIN####
//                                                                          
// -------------------------------------------                              
// The contents of this file are subject to the Red Hat eCos Public License 
// Version 1.1 (the "License"); you may not use this file except in         
// compliance with the License.  You may obtain a copy of the License at    
// http://www.redhat.com/                                                   
//                                                                          
// Software distributed under the License is distributed on an "AS IS"      
// basis, WITHOUT WARRANTY OF ANY KIND, either express or implied.  See the 
// License for the specific language governing rights and limitations under 
// the License.                                                             
//                                                                          
// The Original Code is eCos - Embedded Configurable Operating System,      
// released September 30, 1998.                                             
//                                                                          
// The Initial Developer of the Original Code is Red Hat.                   
// Portions created by Red Hat are                                          
// Copyright (C) 1998, 1999, 2000 Red Hat, Inc.                             
// All Rights Reserved.                                                     
// -------------------------------------------                              
========================================================================*/

#include "stdarg.h"
#include "diag.h"

#define NULL 0
#define true 1
#define false 0
#define HAL_soc_WRITE_CHAR(c) hal_soc_write_char(c);


/*----------------------------------------------------------------------*/
/* Write single char to output                                          */

static void hal_soc_write_char(char c)
{
	#define ICE_PRINT_PORT	0xff20003c
	*(volatile unsigned long *)(ICE_PRINT_PORT) = (unsigned long)((c&0xff)|0xde000000);
}


static void soc_write_char(char c)
{    
    /* Translate LF into CRLF */
    
    if( c == '\n' )
    {
        HAL_soc_WRITE_CHAR('\r');        
    }

    HAL_soc_WRITE_CHAR(c);
}

/*----------------------------------------------------------------------*/
/* Write zero terminated string                                         */
  
static void soc_write_string(const char *psz)
{
    while( *psz ) soc_write_char( *psz++ );
}

/*----------------------------------------------------------------------*/
/* Generic number writing function                                      */
/* The parameters determine what radix is used, the signed-ness of the  */
/* number, its minimum width and whether it is zero or space filled on  */
/* the left.                                                            */

static void soc_write_num(
    unsigned long  n,             /* number to write              */
    unsigned char base,           /* radix to write to            */
    unsigned char sign,           /* sign, '-' if -ve, '+' if +ve */
    int    pfzero,                /* prefix with zero ?           */
    unsigned char width           /* min width of number          */
    )
{
    char buf[16];
    char bpos;
    char bufinit = pfzero?'0':' ';
    char *digits = "0123456789ABCDEF";

    /* init buffer to padding char: space or zero */
    for( bpos = 0; bpos < (char)sizeof(buf); bpos++ ) buf[(unsigned char)bpos] = bufinit;

    /* Set pos to start */
    bpos = 0;

    /* construct digits into buffer in reverse order */
    if( n == 0 ) buf[(unsigned char)bpos++] = '0';
    else while( n != 0 )
    {
        unsigned char d = n % base;
        buf[(unsigned char)bpos++] = digits[d];
        n /= base;
    }

    /* set pos to width if less. */
    if( (char)width > bpos ) bpos = width;

    /* set sign if negative. */
    if( sign == '-' )
    {
        if( buf[bpos-1] == bufinit ) bpos--;
        buf[(unsigned char)bpos] = sign;
    }
    else bpos--;

    /* Now write it out in correct order. */
    while( bpos >= 0 )
        soc_write_char(buf[(unsigned char)bpos--]);
}

/*----------------------------------------------------------------------*/
/* Write decimal value                                                  */

#if 0
static void soc_write_dec( long n)
{
    unsigned char sign;

    if( n < 0 ) n = -n, sign = '-';
    else sign = '+';
    
    soc_write_num( n, 10, sign, false, 0);
}
#endif

/*----------------------------------------------------------------------*/
/* Write hexadecimal value                                              */

static void soc_write_hex( unsigned long n)
{
    soc_write_num( n, 16, '+', false, 0);
}    

/*----------------------------------------------------------------------*/
/* perform some simple sanity checks on a string to ensure that it      */
/* consists of printable characters and is of reasonable length.        */

static int soc_check_string( const char *str )
{
    int result = true;
    const char *s;

    if( str == NULL ) return false;
    
    for( s = str ; result && *s ; s++ )
    {
        char c = *s;

        /* Check for a reasonable length string. */
        
        if( s-str > 256 ) result = false;

        /* We only really support CR and NL at present. If we want to
         * use tabs or other special chars, this test will have to be
         * expanded.
         */
        
        if( c == '\n' || c == '\r' )
            continue;

        /* Check for printable chars. This assumes ASCII */
        
//        if( c < ' ' || c > '~' )
//            result = false;
    }

    return result;
}

static void soc_vprintf( const char *fmt, va_list args)
{
#ifndef WIN32
    if( !soc_check_string(fmt) )
    {
		#if 0
			int i;
		#endif
        soc_write_string("<Bad format string: ");
        soc_write_hex((unsigned long)fmt);
        soc_write_string(" :");
		#if 0	// args typs is void * , args[i] ???
			for( i = 0; i < 8; i++ )
			{
				soc_write_char(' ');
				soc_write_hex(args[i]);
			}
		#endif
        soc_write_string(">\n");
        return;
    }
    
    while( *fmt != '\0' )
    {
        char c = *fmt;

        if( c != '%' ) soc_write_char( c );
        else
        {
            int pfzero = false;
            char width = 0;
            char sign = '+';
                        
            c = *++fmt;
                        
            if( c == '0' ) pfzero = true;

            while( '0' <= c && c <= '9' )
            {
                width = width*10 + c - '0';
                c = *++fmt;
            }

            switch( c )
            {
            case 'd':
            case 'D':
            {
                long val = va_arg(args, long);
                if( val < 0 ) val = -val, sign = '-';
                soc_write_num(val, 10, sign, pfzero, width);
                break;
            }

            case 'x':
            case 'X':
            {
                unsigned long val = va_arg(args, long);
                soc_write_num(val, 16, sign, pfzero, width);
                break;
            }

            case 'c':
            case 'C':
            {
                char ch = (char)(*args++);
                soc_write_char(ch);
                break;
            }

            case 's':
            case 'S':
            {
//              char *s = (char *)(*args++);
                char *s = (char *)(*((unsigned long*)(args++)));
                long len = 0;
                long pre = 0, post = 0;

                if( s == NULL ) s = "<null>";
                else if( !soc_check_string(s) )
                {
                    soc_write_string("<Not a string: 0x");
                    soc_write_hex((unsigned long)s);
                    s = ">";
                    if( width > 25 ) width -= 25;
                    pfzero = false;
                    /* Drop through to print the closing ">" */
                    /* and pad to the required length.       */
                }
                
                while( s[len] != 0 ) len++;
                if( width && len > width ) len = width;

                if( pfzero ) pre = width-len;
                else post = width-len;

                while( pre-- > 0 ) soc_write_char(' ');

                while( *s != '\0' && len-- != 0)
                    soc_write_char(*s++);

                while( post-- > 0 ) soc_write_char(' ');
                                
                break;
            }

            case 'b':
            case 'B':
            {
                unsigned long val = (unsigned long)(*args++);
                unsigned long i;
                if( width == 0 ) width = 32;

                for( i = width-1; i >= 0; i-- )
                    soc_write_char( (val&(1<<i))?'1':'.' );
                                
                break;
            }
            case '%':
                soc_write_char('%');
                break;

            default:
                soc_write_char('%');
                soc_write_char(c);
                break;
            }
        }

        fmt++;
    }   
	soc_write_char('\0');

#endif

}

/*-----------------------------------------------------------------------*/
/* Formatted diagnostic output.                                          */

void soc_printf(const char *fmt, ... )
{
    va_list a;

    va_start(a, fmt);
    soc_vprintf( fmt, a);
    va_end(a);

}

void exception_log(unsigned long cause,unsigned long epc,unsigned long badaddr)
{
	soc_printf("\nexception: cause = %08x ", cause); 
	soc_printf("epc = %08x ", epc);
	soc_printf("bad address = %08x\n", badaddr);
}

void soc_assert(int type, const char * file, int line, const char * exp)
{
		soc_printf("assertion %s ", exp);
		soc_printf("failed: file %s, ", file);
		soc_printf("line %d\n", line);
		while(1);
}
