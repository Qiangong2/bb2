#ifndef	_TYPES_H_
#define	_TYPES_H_

typedef unsigned char	BYTE;
typedef unsigned int	UINT;
typedef unsigned long	DWORD;
typedef unsigned short  WORD;
typedef	int				BOOL;
typedef	int				BOOLEAN;

#define	NULL	0
#define FALSE	0
#define	TRUE	1

#define STATIC	static

#endif	// _TYPES_H_
