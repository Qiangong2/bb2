#ifndef	__TOC_H
#define	__TOC_H


#define	TRACK_ERROR			-1
#define	TRACK_CDROM			1
#define	TRACK_CDDA			2

void TocReset(void * pToc);
DWORD GetTrackLba(BYTE track);
BOOL GetTrackMsf(BYTE track, BYTE *buffer);
int GetSectorType(DWORD lba);
BYTE GetSectorTrack(DWORD lba);
DWORD GetTrackMaxLba(BYTE track);
BOOL CDLGetToc(void * toc);

#endif	// __TOC_H
