/*************************************************************************
*
*  Copyright (C) 2001 T2-Design Corp.  All rights reserved.
*  File:   toc.c
*
*  Contents: CD Table of contents manage routine.
*
*************************************************************************/ 
#define LOG_ENABLE	(0)		// if 0 disable all logs of this module

#include "common.h"
#include "BIOS_cdr.h"
#include "toc.h"

static CDR_TOC *Toc = NULL;

void TocReset(void * pToc)
{
	Toc = (CDR_TOC *)pToc;
}

BOOL GetTrackMsf(BYTE track, BYTE *buffer)
{
	if(Toc == NULL)
		return FALSE;

	if( track >= Toc->FirstTrk && track <= Toc->LastTrk)
	{
		buffer[0]=HEX2BCD(Toc->TrkDscp[track].mm);
		buffer[1]=HEX2BCD(Toc->TrkDscp[track].ss);
		buffer[2]=HEX2BCD(Toc->TrkDscp[track].ff);
		return TRUE;
	}
	else if(track == 0xaa)
	{
		buffer[0]=HEX2BCD(Toc->LeadOut[0]);
		buffer[1]=HEX2BCD(Toc->LeadOut[1]);
		buffer[2]=HEX2BCD(Toc->LeadOut[2]);
		return TRUE;
	}
	else
		return FALSE;
}

DWORD GetTrackLba(BYTE track)
{
	if(Toc == NULL)
		return 0;

	if(track == 0xaa)
		return MSF2LBA(Toc->LeadOut[0],Toc->LeadOut[1],Toc->LeadOut[2]);
	else
		return MSF2LBA(Toc->TrkDscp[track].mm,Toc->TrkDscp[track].ss,
			Toc->TrkDscp[track].ff);
}

static int GetTrackType(BYTE track)
{
	if(Toc->TrkDscp[track].ADRCTL & 0x40)
		return TRACK_CDROM;
	else
		return TRACK_CDDA;
}

BYTE GetSectorTrack(DWORD lba)
{
	int i;
	DWORD lba1, LeadOutLba;

	if(Toc == NULL)
		return 0;

	LeadOutLba = GetTrackLba(0xaa);
	if(lba < 0 || lba >= LeadOutLba)
	{
		TRACE1("Sector error Lba = %x\n",lba);
		return 0;
	}

	for(i = Toc->FirstTrk; i <= Toc->LastTrk; i++)
	{
		lba1 = (i == Toc->LastTrk)?LeadOutLba:(GetTrackLba(i+1) - 150);
		if(lba1 > lba)
			break;
	}
	
	if(i > Toc->LastTrk) 
	{
		TRACE1("Can't found track Lba = %x\n",lba);
		return 0;
	}
	
	return i;
}

int GetSectorType(DWORD lba)
{
	BYTE track = GetSectorTrack(lba);

	if(track != 0)
		return GetTrackType(track);
	else
		return TRACK_ERROR;
}

DWORD GetTrackMaxLba(BYTE track)
{
	BYTE msf[3];
	DWORD lba;

	if(Toc == NULL)
		return -1;

	if(track == Toc->LastTrk)
		track = 0xaa;
	else
		track++;

	if(GetTrackMsf(track,msf))
	{
		lba = BCD_MSF2LBA(msf[0],msf[1],msf[2]);
		if(track != 0xaa) lba -= 150;
		lba --;
		return lba;
	}
	else
		return -1;
}

BOOL CDLGetToc(void * toc)
{
	int i;
	for(i = 0; i < sizeof(CDR_TOC); i++)
		*((BYTE *)toc + i) = *((BYTE *)Toc + i);

	return TRUE;
}
