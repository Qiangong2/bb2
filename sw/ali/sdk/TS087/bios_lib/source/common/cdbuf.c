/*************************************************************************
*
*  Copyright (C) 2001 T2-Design Corp.  All rights reserved.
*  File:   cdbuf.c
*
*  Contents: cd buffer management, read data with non brocking mode.
*
*************************************************************************/ 

#define LOG_ENABLE	(1)		// if 0 disable all logs of this module

#include "common.h"
#include "cdbuf.h"
#include "cdfmt.h"
#include "ecc.h"


static int HeadPos = 0;
static int TailPos = 0;
static DWORD HeadLba = 0;
static DWORD TailLba = 0;

static BYTE *DataBuff = NULL;
static int BuffLen = 0;
static int SectorSize = SECTOR_SIZE;
static int UnitSize = SECTOR_SIZE;
static BYTE UnitResult[1000];


void CDLBufSetup(BYTE *buffer, int numSectors)
{
	ASSERT(buffer != NULL);

	DataBuff = buffer;
	BuffLen = numSectors;
	UnitSize = SectorSize = SECTOR_SIZE;
}

void CDLBufReset(DWORD StartLba, int Sector_Size)
{
	if(StartLba == -1)
	{	// init all buffer
		HeadPos = 0;
		TailPos = 0;
	}
	else
	{
		if(SectorSize != Sector_Size)
		{
			TRACE("SectorSize changed!!!\n");
			UnitSize = SectorSize = Sector_Size;
			HeadPos = 0;
			TailPos = 0;
		}
		else
		{
			TailPos = HeadPos;
		}
	}
	HeadLba = StartLba;
	TailLba = StartLba;
}

int CDLBufGetCount(void)
{
	if(TailPos >= HeadPos)
		return (TailPos - HeadPos);
	else
		return (TailPos + BuffLen - HeadPos);
}

DWORD CDLBufGetTailLba(void)
{
	// Add variable TailLba to avoid sometimes gets error tail 
	// LBA value when reading sector and IDE interrupt occur.
	return TailLba;
}

int CDLBufGetFreeNum(void)
{
	int count = CDLBufGetCount();

	return (BuffLen - count - 1);
}

BOOL CDLBufIsFull(void)
{
	return ((HeadPos + BuffLen - TailPos) % BuffLen == 1);
}

#if 0
static BOOL IsSectorExist(DWORD lba)
{
	int count;
	if((count = CDLBufGetCount()) == 0) 
		return FALSE;

	return (lba >= HeadLba && lba < (HeadLba + count));
}
#endif

BOOL CDLBufSeekSector(DWORD lba, int Sector_Size, BOOL Seek)
{
	int offset, count;

	if(Sector_Size != SectorSize)
		return FALSE;

	if(lba == HeadLba)
		return TRUE;

	if((count = CDLBufGetCount()) == 0)
		return FALSE;

	if(Seek)
	{
		if(lba >= HeadLba && lba < (HeadLba + count))
		{
			offset = lba - HeadLba;
			HeadPos = (HeadPos + offset) % BuffLen;
			HeadLba += offset;
			return TRUE;
		}
	}

	return FALSE;
}

BYTE * CDLBufOutput(void)
{
	BYTE * buff;

    if(!CDLBufGetCount())
        return NULL;

	if(UnitResult[HeadPos])
		buff = &DataBuff[HeadPos * UnitSize];
	else
		buff = NULL;

	HeadPos = (HeadPos + 1) % BuffLen;
	HeadLba ++;

	return buff;
}

BYTE *CDLBufOutputNoRelease(void)
{
	BYTE *buff;

    if (!CDLBufGetCount())
        return NULL;
	if (UnitResult[HeadPos])
	{
		buff = &DataBuff[HeadPos * UnitSize];
		HeadPos = (HeadPos + 1) % BuffLen;
		HeadLba++;
	}
	else
	{
		buff = NULL;
		TailPos = HeadPos;
	}
	return buff;
}

void CDLBufInputNull(void)
{
	UnitResult[TailPos] = 0;
	TailPos = (TailPos + 1) % BuffLen;
	TailLba ++;
}

#ifdef _IDE
void CDLBufInput(BYTE *buff, int ByteCount)
{
	int len = ByteCount/SectorSize;
	BYTE * dest = &DataBuff[TailPos * UnitSize];

	while(len-- > 0)
	{
		if(CDLBufIsFull())
		{
			TRACE("input: buffer is full !!!\n");
			return;
		}

		memcpy(dest, buff, SectorSize);

		UnitResult[TailPos] = 1;
		TailPos = (TailPos + 1) % BuffLen;
		TailLba ++;

		buff += SectorSize;
		if(TailPos)
			dest += UnitSize;
		else
			dest = DataBuff;
	}
}

#else

static BYTE TempSector[SECTOR_SIZE];
static void InputSector(DWORD src, DWORD dest)
{
	int offset;
	BYTE time[3];
	BYTE result = 1;

	if(SectorSize == 2048)
	{
		LBAToMSF(TailLba, time, BCD_CODE);
		result = CDLSectorECC((BYTE *)src, time);

		if(((CDXA_FMT21 *)src)->Header[3] == 1)
			offset = 16;
		else
			offset = 24;
	}
	else if(SectorSize == 2328)
		offset = 24;
	else
		offset = 0;

	CDLMemCopy((src + offset), dest, SectorSize);

	UnitResult[TailPos] = result;
	TailPos = (TailPos + 1) % BuffLen;
	TailLba ++;
}

void CDLBufInput(DWORD src, int len, int offset, BOOL first)
{
	int i;
	DWORD dest;

	dest = (DWORD)DataBuff + TailPos * UnitSize;
	if(!first && offset > 0)
	{
		CDLMemCopy(src, ((DWORD)TempSector + SECTOR_SIZE - offset), offset);
		InputSector((DWORD)TempSector, dest);
		if(TailPos)
			dest += UnitSize;
		else
			dest = (DWORD)DataBuff;
	}

	src += offset;

	for(i = 0; i < len-1; i++)
	{
		if(CDLBufIsFull())
			return;
		InputSector(src, dest);

		src += SECTOR_SIZE;
		if(TailPos)
			dest += UnitSize;
		else
			dest = (DWORD)DataBuff;
	}

	if(CDLBufIsFull()) 
		return;

	if(offset == 0 )
		InputSector(src, dest);
	else
		CDLMemCopy(src, (DWORD)TempSector, SECTOR_SIZE - offset);
}

#endif

void CDLMemCopy(DWORD SrcBase, DWORD DestBase, DWORD Size)
{
	memcpy((BYTE *)DestBase, (BYTE *)SrcBase, Size);
}


/////////////////////////////////////////////////////////////////////
// Added for DVD Loader

void CDLBufInputEx(int ByteCount)
{
	int i;
	int sectors = ByteCount/SectorSize;

	// TRACE1("input = %d\n", sectors);

	for(i = 0; i < sectors; i++)
	{
		UnitResult[TailPos + i] = 1;
	}

	TailPos = (TailPos + sectors) % BuffLen;
	TailLba += sectors;
}

void CDLBufOutputEx(int numSectors)
{
	ASSERT(CDLBufGetCount() >= numSectors);

	HeadPos = (HeadPos + numSectors) % BuffLen;
	HeadLba += numSectors;

	// TRACE2("CD buff: head=%d, tail=%d\n", HeadPos, TailPos);
}

DWORD CDLBufGetTailAddr(void)
{
	return (DWORD)(DataBuff + TailPos * UnitSize);
}

int CDLBufGetTailFree(void)
{
	return (BuffLen - TailPos);
}

int CDLBufGetSectorSize(void)
{
	return SectorSize;
}
