#ifndef __CDFMT_H
#define __CDFMT_H

#define SECTOR_SIZE			2352
#define DVD_SECTOR_SIZE		2048

#define CD_FMT_UNKNOWN		-1
#define CDDA_FMT_FLAG		0
#define CDROM_FMT_FLAG		1
#define CDROM_FMT_M1_FLAG	2
#define CDXA_FMT21_FLAG		3
#define CDXA_FMT22_FLAG		4
#define DVD_FMT_FLAG		5

#define FIELD_OFFSET(type, field)    ((DWORD)(DWORD)&(((type *)0)->field))
#define FIELD_LENGTH(type, field)    sizeof(((type *)0)->field)

#define CDXA_SUBFLAG2		0x20	// form2

typedef struct
{
	unsigned char P_parity[172];
	unsigned char Q_parity[104];
}STRUCT_ECC;

typedef struct
{
	unsigned char UserData[2352];
}CDDA_FMT;			// CD-DA

typedef struct
{
	unsigned char Sync[12];
	unsigned char Header[4];	// Min. Sec. Frame. Mode.
	unsigned char UserData[2336];
}CDROM_FMT;		// CD-ROM Mode0, Mode1

typedef struct
{
	unsigned char Sync[12];
	unsigned char Header[4];
	unsigned char UserData[2048];
	unsigned char EDC[4];		// Error Detection Code
	unsigned char Space[8];
	STRUCT_ECC	ECC;			// Error Correction Code
}CDROM_FMT_M1;		// CD-ROM Mode1

typedef struct
{
	unsigned char Sync[12];
	unsigned char Header[4];
	unsigned char SubHeader[8];
	unsigned char UserData[2048];
	unsigned char EDC[4];
	STRUCT_ECC ECC;
}CDXA_FMT21;		// CD-ROM/XA Mode2 form1

typedef struct
{
	unsigned char Sync[12];
	unsigned char Header[4];
	unsigned char SubHeader[8];
	unsigned char UserData[2324];
	unsigned char EDC[4];
}CDXA_FMT22;		// CD-ROM/XA Mode2 form2

typedef struct
{
	unsigned char UserData[2048];
}DVD_FMT;

typedef struct
{
	unsigned long Flag; 
	unsigned long DataOffset;
	unsigned long DataLength;
}CD_FORMAT;

#endif
