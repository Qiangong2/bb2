#ifndef	__COMMON_H
#define	__COMMON_H

#include "types.h"
#include "diag.h"

#define SOC_MEM_FREQUENCY	120
#define SOC_CPU_FREQUENCY	192000000

#define CDL_TIMER			2		// ms

#define LEDOUT(x)			// *(unsigned char *)0xb8000055 = (x)

#define BCD2HEX(x) 			(((x)>>4)*10 + ((x)&0x0f))
#define HEX2BCD(x)			((((x)/10)<<4) + ((x)%10))
#define MSF2LBA(m,s,f)		(((m)*60L + (s))*75L + (f) - 150L)
#define BCD_MSF2LBA(m,s,f)	(((BCD2HEX(m))*60L + (BCD2HEX(s)))*75L + \
							(BCD2HEX(f)) - 150L)

#define	BCD_CODE			0
#define	HEX_CODE			1


#define READ_BYTE(addr)				(*(volatile BYTE *)(addr))
#define WRITE_BYTE(addr, data)		(*(volatile BYTE *)(addr) = (data))
#define READ_WORD(addr)				(*(volatile WORD *)(addr))
#define WRITE_WORD(addr, data)		(*(volatile WORD *)(addr) = (data))
#define READ_DWORD(addr)			(*(volatile DWORD *)(addr))
#define WRITE_DWORD(addr, data)		(*(volatile DWORD *)(addr) = (data))

#define SET_B_BIT(addr, data)		(WRITE_BYTE((addr), READ_BYTE(addr) | (data)))
#define SET_W_BIT(addr, data)		(WRITE_WORD((addr), READ_WORD(addr) | (data)))
#define SET_D_BIT(addr, data)		(WRITE_DWORD((addr), READ_DWORD(addr) | (data)))
#define CLEAR_B_BIT(addr, data)		(WRITE_BYTE((addr), READ_BYTE(addr) & ~(data)))
#define CLEAR_W_BIT(addr, data)		(WRITE_WORD((addr), READ_WORD(addr) & ~(data)))
#define CLEAR_D_BIT(addr, data)		(WRITE_DWORD((addr), READ_DWORD(addr) & ~(data)))


void delay_ms(int ms);
void delay_ns(int ns);
DWORD ReadCountReg(void);
void LBAToMSF(DWORD Lba, BYTE * Msf, int Code);

#define memset		cd_memset
#define memcpy		cd_memcpy
void *memset(void *dest, int c, int count);
void *memcpy(void *dest, const void *src, int count);

#endif	// __COMMON_H
