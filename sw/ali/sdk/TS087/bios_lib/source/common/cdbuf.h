#ifndef	__CDBUF_H
#define	__CDBUF_H

void CDLBufSetup(BYTE *buffer, int numSectors);
void CDLBufReset(DWORD StartLba, int Sector_Size);
int CDLBufGetCount(void);
int CDLBufGetFreeNum(void);
DWORD CDLBufGetTailLba(void);
BOOL CDLBufIsFull(void);
BYTE * CDLBufOutput(void);
void CDLBufInputNull(void);
#ifdef _IDE
void CDLBufInput(BYTE *buff, int ByteCount);
#else
void CDLBufInput(DWORD src, int len, int offset, BOOL first);
#endif
void CDLMemCopy(DWORD SrcBase, DWORD DestBase, DWORD Size);
BOOL CDLBufSeekSector(DWORD lba, int Sector_Size, BOOL Seek);


/////////////////////////////////////////////////////////////////////
// Added for DVD Loader

void CDLBufInputEx(int ByteCount);
void CDLBufOutputEx(int numSectors);
BYTE *CDLBufOutputNoRelease(void);
DWORD CDLBufGetTailAddr(void);
int CDLBufGetTailFree(void);
int CDLBufGetSectorSize(void);


#endif	// __CDBUF_H
