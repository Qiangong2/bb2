#Makefile for t2bios

CC	= mips-t2-elf-gcc
OBJDUMP	= mips-t2-elf-objdump
OBJCOPY	= mips-t2-elf-objcopy
LD     = mips-t2-elf-ld
AR 	= mips-t2-elf-ar -rc

bios_OBJECTS = bios_main.o

ifeq ($(AUDIO_DAC), $(DAC_ALC650))
	bios_OBJECTS1 =  bios_main_DAC_ALC650.o
else
ifeq ($(AUDIO_DAC), $(DAC_ALC658))
	bios_OBJECTS1 =  bios_main_DAC_ALC658.o
else
ifeq ($(AUDIO_DAC), $(DAC_CS4340))
	bios_OBJECTS1 =  bios_main_DAC_CS4340.o
else
ifeq ($(AUDIO_DAC), $(DAC_1602))
	bios_OBJECTS1 =  bios_main_DAC_1602.o
else
ifeq ($(AUDIO_DAC), $(DAC_PCM1738))
	bios_OBJECTS1 =  bios_main_DAC_PCM1738.o
endif
endif
endif
endif
endif


DEFS += -DR4K

CFLAGS = -EL -nostdinc -O2 -mips1 -G 0 -fno-builtin -I../Includes

COMPILE = $(CC) $(DEFS) $(CFLAGS)
LINK = $(CC) $(CFLAGS) -o $(BIOS_NAME).elf

all: $(BIOS_NAME).abs

.c.o:
	$(COMPILE) -c $<

.s.o:
	$(COMPILE) -c $<

.S.o:
	$(COMPILE) -c $<


$(BIOS_NAME).abs: $(bios_OBJECTS) $(LIBS) Makefile
	cp $(bios_OBJECTS1) $(bios_OBJECTS)
	$(LINK) -mips1 -O2 -G 0 -nostdlib -Wl,-Map,bios.map,-Tbios.lnk $(bios_OBJECTS) $(LIBS)
	mips-t2-elf-objcopy -O binary $(BIOS_NAME).elf $(BIOS_NAME).abs
	./t2zip.exe $(BIOS_NAME).abs $(BIOS_NAME).zip
	$(OBJCOPY) -I binary -O elf32-littlemips $(BIOS_NAME).zip bios_ram.o
	mv bios_ram.o $(BIOS_NAME).obj
	mv $(BIOS_NAME).obj ../


$(BIOS_NAME).a: $(bios_OBJECTS) $(LIBS)
	$(AR) $(bios_OBJECTS) $(LIBS)

disasm:
	mips-t2-elf-objdump $(BIOS_NAME).elf --disassemble >disasm.txt	


clean:
	rm -f *.zip
	rm -f bios.map
	rm -f *.elf
	rm -f *.abs
