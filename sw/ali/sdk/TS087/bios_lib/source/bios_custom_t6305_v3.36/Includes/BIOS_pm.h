/***************************************************************************
                   M6304 System Management Macros and Functions  
    By Wen Liu 
    January 2 2003 
    History:
    8.6.2003:Modify the function SetSysState().Now state BIOS_PM_EXIT is added up
    for warm reset.   
    8.11.2003:Modify the function BIOS_PM_SetSysState().Now state BIOS_PM_EXIT is removed.
    The state BIOS_PM_STANDBY is defined as all local devices are powered down, and cpu suspends
    until Ir interrupt is detected.The frequency is not changed for the sake of safety. 
 ***************************************************************************/
#ifndef __PM_H
#define __PM_H


//System and Device clock control macros
#define	CLOCK_SET_CPU_FREQUENCY(x)		pm_set_pll((((x)/1500)&0xff)|0x100) //KHz
#define	CLOCK_SET_MEM_RATIO(x)	  		pm_set_pll((((x)&0x3)<<16)|(0x1<<18))
#define CLOCK_SET_PCI_RATIO(x)			pm_set_pll((((x)&0x3)<<24)|(0x1<<26))
#define CLOCK_SET_VE_RATIO(x)			{*(volatile unsigned long *)(0xb8000060) = (*(volatile unsigned long *)(0xb8000060))&(~(0x1<<18));\
						 *(volatile unsigned long *)(0xb8000078) = (*(volatile unsigned long *)(0xb8000078))&(~(0x3<<16))|(((x)&0x3)<<16);\
						 time_delay_us(100);										\
						 *(volatile unsigned long *)(0xb8000060) |= (0x1<<18);}	  
#define	CLOCK_SET_GPU_RATIO(x)			{*(volatile unsigned long *)(0xb8000060) = (*(volatile unsigned long *)(0xb8000060))&(~(0x1<<17));\
						 *(volatile unsigned long *)(0xb8000078) = (*(volatile unsigned long *)(0xb8000078))&(~(0x3<<8))|(((x)&0x3)<<8);\
						 time_delay_us(100);										\
						 *(volatile unsigned long *)(0xb8000060) |= (0x1<<17);}
#define	CLOCK_SET_DSP_SRC_FS(x,y)	  	{*(volatile unsigned long *)(0xb8000060) = (*(volatile unsigned long *)(0xb8000060))&(~(0x1<<16));\
						 *(volatile unsigned long *)(0xb8000078) = (*(volatile unsigned long *)(0xb8000078))&(~0x1f)|(((x)&0x1)<<4)|((y-1)&0xf);\
						 time_delay_us(100);										\
						 *(volatile unsigned long *)(0xb8000060) |= (0x1<<16);}
#define	CLOCK_SET_SPDIF_SRC(x)	 		{*(volatile unsigned long *)(0xb8000094) = (*(volatile unsigned long *)(0xb8000094))&(~(0x3<<8))|(((x)&0x3)<<8);\
						 time_delay_us(100);}
#define	CLOCK_SET_I2S_MCLK_SRC(x)	  	{*(volatile unsigned long *)(0xb8000094) = (*(volatile unsigned long *)(0xb8000094))&(~(0x3<<5))|(((x)&0x3)<<5);\
						 time_delay_us(100);}
#define CLOCK_ENABLE_I2S_MCLK_OUTPUT	        {*(volatile unsigned long *)(0xb8000094) = (*(volatile unsigned long *)(0xb8000094))&(~(0x1<<7));		\
						 time_delay_us(100);}
#define CLOCK_DISABLE_I2S_MCLK_OUTPUT		{*(volatile unsigned long *)(0xb8000094) = (*(volatile unsigned long *)(0xb8000094))|(0x1<<7);			\
						 time_delay_us(100);}
//Mem clock phases control macros
#define CLOCK_SET_MEM_READ_CLOCK_DELAY(x)	{*(volatile unsigned long *)(0xb8000064) = (*(volatile unsigned long *)(0xb8000064))&(~(0xf<<8))|(((x)&0xf)<<8);\
						 time_delay_us(100);}
#define CLOCK_SET_MEM_CLOCK_TREE_DELAY(x)	{*(volatile unsigned long *)(0xb8000094) = (*(volatile unsigned long *)(0xb8000064))&(~(0xf))|((x)&0xf);	\
						 time_delay_us(100);}
//Regard the following macros as rvalues
//Macros to get modules' clock status (all are ratios to cpu clock except for cpu and dsp frequency)
#define CLOCK_GET_CPU_PLLM	  ((*(volatile unsigned long *)(0xb8000070))&0xff)
#define	CLOCK_GET_CPU_FREQUENCY	  (((*(volatile unsigned long *)(0xb8000070))&0xff)*1500)//KHz
#define	CLOCK_GET_MEM_RATIO	  (((*(volatile unsigned long *)(0xb8000070))>>8)&0x3)
#define CLOCK_GET_PCI_RATIO	  (((*(volatile unsigned long *)(0xb8000070))>>10)&0x1)
#define CLOCK_GET_VE_RATIO        (((*(volatile unsigned long *)(0xb8000078))>>16)&0x3)
#define	CLOCK_GET_GPU_RATIO	  (((*(volatile unsigned long *)(0xb8000078))>>8)&0x3)
#define	CLOCK_GET_DSP_SRC	  (((*(volatile unsigned long *)(0xb8000078))>>4)&0x1)
#define	CLOCK_GET_DSP_FS	  (((*(volatile unsigned long *)(0xb8000078))&0xf)+1)
#define	CLOCK_GET_DSP_FREQUENCY	  (CLOCK_GET_DSP_SRC?(2250*CLOCK_GET_CPU_PLLM*(CLOCK_GET_DSP_FS+1)/16):(3000*CLOCK_GET_CPU_PLLM*(CLOCK_GET_DSP_FS+1)/16))//KHz
#define	CLOCK_GET_SPDIF_SRC	  (((*(volatile unsigned long *)(0xb8000094))>>8)&0x3)
#define	CLOCK_GET_I2S_MCLK_SRC	  (((*(volatile unsigned long *)(0xb8000094))>>5)&0x3)
#define CLOCK_GET_I2S_MCLK_OUTPUT_STATE	(((*(volatile unsigned long *)(0xb8000094))>>7)&0x1)
#define CLOCK_GET_MEM_READ_CLOCK_DELAY	(((*(volatile unsigned long *)(0xb8000064))>>8)&0xf)	
#define CLOCK_GET_MEM_CLOCK_TREE_DELAY	((*(volatile unsigned long *)(0xb8000064))&0xf)
//clock values
#define CLOCK_PCI_1_4		  0x00
#define CLOCK_PCI_1_8		  0x01
#define CLOCK_MEM_1_1             0x00
#define CLOCK_MEM_2_3             0x01
#define CLOCK_MEM_1_2             0x02
#define CLOCK_MEM_1_3             0x03
#define CLOCK_VE_1_2		  0x00
#define CLOCK_VE_2_3		  0x01
#define CLOCK_VE_1_3		  0x02
#define CLOCK_VE_1_4		  0x03
#define CLOCK_GPU_1_2		  0x00
#define CLOCK_GPU_1_3		  0x01
#define CLOCK_GPU_1_4		  0x02
#define CLOCK_GPU_2_3		  0x03
#define CLOCK_DSP_SRC_3_2         0x1
#define	CLOCK_DSP_SRC_2_1         0x0
#define CLOCK_SPDIF_SRC_12_288    0x00
#define CLOCK_SPDIF_SRC_16_934    0x01   
#define CLOCK_SPDIF_SRC_I2S_MCLK_IN 0x02
#define CLOCK_SPDIF_SRC_AC_BCK    0x03
#define CLOCK_I2S_MCLK_OUTPUT_ENABLE 0x0
#define CLOCK_I2S_MCLK_OUTPUT_DISABLE 0x1
#define CLOCK_I2S_MCLK_SRC_24_576 0x00
#define CLOCK_I2S_MCLK_SRC_12_288 0x01
#define CLOCK_I2S_MCLK_SRC_16_934 0x02
#define CLOCK_I2S_MCLK_SRC_EXTERNAL 0x03

//Share pin switch macros 
#define	SHAREPIN_SWITCH_I2S_DATAIN {*(volatile unsigned long *)(0xb8000094) = (*(volatile unsigned long *)(0xb8000094))&(~(0x1<<1));\
				    time_delay_us(10);}
#define	SHAREPIN_SWITCH_AC_DIN1    {*(volatile unsigned long *)(0xb8000094) = (*(volatile unsigned long *)(0xb8000094))|(0x1<<1);\
				    time_delay_us(10);}
#define	SHAREPIN_SWITCH_GPIO       {*(volatile unsigned long *)(0xb8000094) = (*(volatile unsigned long *)(0xb8000094))|(0x1<<3);\
				    time_delay_us(10);}
#define	SHAREPIN_SWITCH_CDDV	   {*(volatile unsigned long *)(0xb8000094) = (*(volatile unsigned long *)(0xb8000094))&(~(0x1<<3));\
				    time_delay_us(10);}
//All PCI devices should be initialized after 
#define	SHAREPIN_SWITCH_DISABLE_FLASH  {unsigned long temp;\
					temp = *(volatile unsigned long *)(0xbfc00000);\
				        *(volatile unsigned long *)(0xb8000090) = (*(volatile unsigned long *)(0xb8000090))|(0x1<<1);\
				        time_delay_us(10);}
#define	SHAREPIN_SWITCH_ENABLE_FLASH   {*(volatile unsigned long *)(0xb8000060) = (*(volatile unsigned long *)(0xb8000054))|(~(1<<28));\
				        *(volatile unsigned long *)(0xb8000090) = (*(volatile unsigned long *)(0xb8000090))&(~(0x1<<1));\
				        time_delay_us(10);}
//Share pin state(boolean)
#define	SHAREPIN_STATE_I2S_DATAIN  (((*(volatile unsigned long *)(0xb8000094))&(0x1<<1)) == 0x0)
#define SHAREPIN_STATE_AC_DIN1	   (((*(volatile unsigned long *)(0xb8000094))&(0x1<<1)) == (0x1<<1))
#define	SHAREPIN_STATE_GPIO        (((*(volatile unsigned long *)(0xb8000094))&(0x1<<3)) == (0x1<<3))
#define	SHAREPIN_STATE_CDDV        (((*(volatile unsigned long *)(0xb8000094))&(0x1<<3)) == 0x0)
#define	SHAREPIN_STATE_FLASH_DISABLED	   (((*(volatile unsigned long *)(0xb8000090))&(0x1<<1)) == (0x1<<1))
#define	SHAREPIN_STATE_FLASH_ENABLED	   (((*(volatile unsigned long *)(0xb8000090))&(0x1<<1)) == 0x0)
//power on/off macros and values
#define PM_SET_POWER_STATE(x,y)	   {*(volatile unsigned long *)(0xb8000060) = (*(volatile unsigned long *)(0xb8000060))&(~(0x1<<x))|(y<<x);\
				    time_delay_us(10);}
#define PM_GET_POWER_STATE(x)	   (((*(volatile unsigned long *)(0xb8000060))>>x)&0x1)

//RESET MACROS
//Internal devices
#define RESET_GPU		   {*(volatile unsigned long *)(0xb8000060) = ((*(volatile unsigned long *)(0xb8000060))|(1<<1));\
				    time_delay_us(10);\
				    *(volatile unsigned long *)(0xb8000060) = ((*(volatile unsigned long *)(0xb8000060))&(~(1<<1)));}
#define RESET_VE		   {*(volatile unsigned long *)(0xb8000060) = ((*(volatile unsigned long *)(0xb8000060))|(1<<2));\
				    time_delay_us(10);\
				    *(volatile unsigned long *)(0xb8000060) = ((*(volatile unsigned long *)(0xb8000060))&(~(1<<2)));}				    
#define RESET_DE		   {*(volatile unsigned long *)(0xb8000060) = ((*(volatile unsigned long *)(0xb8000060))|1);\
				    time_delay_us(10);\
				    *(volatile unsigned long *)(0xb8000060) = ((*(volatile unsigned long *)(0xb8000060))&(~1));}
#define RESET_AUDIO_CORE	   {*(volatile unsigned long *)(0xb8000060) = ((*(volatile unsigned long *)(0xb8000060))|(1<<4));\
				    time_delay_us(10);\
				    *(volatile unsigned long *)(0xb8000060) = ((*(volatile unsigned long *)(0xb8000060))&(~(1<<4)));}
#define RESET_EMBEDDED_TV_ENCODER  {*(volatile unsigned long *)(0xb8000060) = ((*(volatile unsigned long *)(0xb8000060))|(1<<3));\
				    time_delay_us(10);\
				    *(volatile unsigned long *)(0xb8000060) = ((*(volatile unsigned long *)(0xb8000060))&(~(1<<3)));}
//some enternal device,not include cd-dsp 
#define RESET_IDE          	   {time_delay_us(10);\
				    *(volatile unsigned long *)(0xb8000060) = ((*(volatile unsigned long *)(0xb8000054))|(1<<28));}

#define	POWER_ON		   1
#define	POWER_OFF		   0
//Internal device
#define DEV_VSB 		   31
#define DEV_AC_LINK 	   	   29
#define DEV_SPDIF 		   28
#define DEV_I2S   		   27
#define DEV_TVE			   26
#define DEV_IDE			   25
#define DEV_UP			   24
#define DEV_CDI			   23
#define DEV_IRC			   22
#define DEV_GSI  		   21
#define DEV_SCB                    20
#define DEV_USB 		   19
#define DEV_VE   		   18
#define DEV_GPU 		   17
#define DEV_DSP 		   16

#define PM_DEV_OFF POWER_OFF
#define PM_DEV_ON  POWER_ON

#define PM_MODE_DVD		0
#define PM_MODE_VCD_SVCD	1
#define PM_MODE_MPEG12		2
#define PM_MODE_MPEG4		3
#define PM_MODE_MP3_WMA		4
#define PM_MODE_JPEG		5
#define PM_MODE_CDDA		6

#define PM_NORMAL  	 0
#define PM_STANDBY 	 1

#define PM_DVDSYS  	 0
#define PM_GAMESYS 	 1

#define PM_IDE		0
#define PM_PCI		1
#define PM_CD		2
#define PM_UP		3
/***********************************************************************
 *Function Name: BIOS_PM_SetWorkMode (int Workmode)
 *Description: Be used by some typical applications to shut down unused 
 *hardware function blocks.
 *Supports: T6305/6.
 *Params:
 *Workmode: Typical application Value: PM_MODE_DVD,PM_MODE_VCD_SVCD,PM_MODE_MPEG12 , PM_MODE_MPEG4,PM_MODE_MP3_WMA,PM_MODE_JPEG,PM_MODE_CDDA
 *Returns:
 *None.
 ***********************************************************************/
void 	BIOS_PM_SetWorkMode(int Workmode);	
/***********************************************************************
 *Function Name :PM_SetDevState ( int DeviceID, int DevState)
 *Description: 
 *	Power on or power off local devices.
 *Supports: T6305/6.
 *Params: 
 *DeviceID: local devices ID number.
 *Device State Value: PM_DEV_OFF = 0, PM_DEV_ON = 1.
 *Returns:
 *0: Success.
 *-1: Fail to set device state.
 ***********************************************************************/
int  	BIOS_PM_SetDevState(int DeviceID, int DevState);
/***********************************************************************
 *Function Name :PM_GetDevState (int DeviceID)
 *Description: 
 *	Power on or power off local devices.
 *Supports: T6305/6.
 *Params: 
 *DeviceID: local devices ID number.
 *Returns:
 *PM_DEV_OFF, PM_DEV_ON.
 ***********************************************************************/
int  	BIOS_PM_GetDevState(int deviceID);

/***********************************************************************
 *Function Name: int BIOS_PM_GetDEVRatio ( int *VEratio, int *GPUratio, int *DSPsource, int *DSPfreqratio)
 *Description: Lock some local devices frequencies in a ratio to the CPU clock.
 *Supports: T6305/6.
 *Params:
 *VEratio: Only 1:2, 1:1.5, 1:3 and 1:4 can be selected.
 *GPUratio: Only 1:2, 1:3, 1:4 and 1:1.5 can be selected.
 *DSPsource: Only 1:2 and 2:3 can be selected.
 *DSPfreqratio: Only 4:16 to 16:16 can be selected (DSP: DSP_SRC).
 *Returns:
 *None.
 ************************************************************************/
void 	BIOS_PM_GetDEVRatio( int *VEratio, int *GPUratio, int *DSPsource, int *DSPfreqratio);
/*************************************************************************
 *Function Name: int BIOS_PM_GetPCIdivider (void)
 *Description: Read the PCI clock that is set by strap pin.
 *Supports: T6305/6.
 *Params: None.
 *Returns:
 *The value of range from 0 to 3, representing 1:1, 1:2, 1:4 and 1:8 respectively.
 ************************************************************************/
 int  	BIOS_PM_GetPCIdivider(void);
/************************************************************************
 *Function Name: int BIOS_PM_GetMEMdivider (void)
 *Description: Read the MEM clock that is set by strap pin.
 *Supports: T6305/6.
 *Params: None.
 *Returns:
 *The value of range from 0 to 3, representing 1:1, 2:3, 1:2 and 1:3 respectively.
 *************************************************************************/
int  	BIOS_PM_GetMEMdivider(void);
/***********************************************************************
 *Function Name: int BIOS_PM_SetDEVRatio ( int VEratio, int GPUratio, int DSPsource, int DSPfreqratio)
 *Description: Lock some local devices frequencies in a ratio to the CPU clock.
 *Supports: T6305/6.
 *Params:
 *VEratio: Only 1:2, 1:1.5, 1:3 and 1:4 can be selected.
 *GPUratio: Only 1:2, 1:3, 1:4 and 1:1.5 can be selected.
 *DSPsource: Only 1:2 and 2:3 can be selected.
 *DSPfreqratio: Only 4:16 to 16:16 can be selected (DSP: DSP_SRC).
 *Returns:
 *0: Success.
 *-1:Failure.
  ***********************************************************************/
int  	BIOS_PM_SetDEVRatio( int VEratio, int GPUratio, int DSPsource, int DSPfreqratio);
/************************************************************************
 *Function Name:BIOS_PM_SetSysState (int State)
 *Description: Control the power state of the whole system.
 *Supports: T6305/6.
 *Params:
 *State: General system power states: PM_STANDBY
 *PM_STANDBY:all local devices are powered down, and cpu suspends until Ir 
 *interrupt is detected.
 *Returns:
 *None.
 *Note:Most of local devices should be power off accroding to the definition of 
 *standby state.But you have to assure that all local devices are in idle states.  
 *Now you can only use the parameter PM_STANDBY and the system will come back to the PM_NORMAL
 *state after it returns.
 ***********************************************************************/
void 	BIOS_PM_SetSysState( int State);
/************************************************************************
 *Function Name:BIOS_SYS_EXIT (int State)
 *Description:Enter into Game or dvd application system, only use on T2's application.
 *Supports: T6302/4.
 *Params:
 *State: General system power states: BIOS_PM_DVDSYS,BIOS_PM_GAMESYS
 *Returns:
 *None.
 ***********************************************************************/
void 	BIOS_PM_SYSEXIT( int State);

#endif
//__PM_H
