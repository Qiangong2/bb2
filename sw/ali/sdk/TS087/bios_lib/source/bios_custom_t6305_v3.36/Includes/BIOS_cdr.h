/***************************************************************************
	BIOS - Application library - CD Library
	T-Square, Zhuhai
	Started by Bagio, 2002-10-28
 ***************************************************************************/

#ifndef __BIOS_CDR_H
#define __BIOS_CDR_H

#include "BIOS_CDR_defines.h"

#ifdef __cplusplus
extern "C" {
#endif

/***************************************************************************
	Functions
 ***************************************************************************/ 
	
/********************************************************************
 BIOS_CDR_Init - Initialise the CD loader, must be called once at
   startup
 
 Supports: T630x

 Params:
    cdBuff             - CDROM buffer used for data caching
	numSectors         - Size of the buffer in number of sectors

 Return:
    1 for success, 0 for failure

 Notes:
    Each sector is 2352 bytes long
 ********************************************************************/
int BIOS_CDR_Init(void* cdBuff, int numSectors);


/********************************************************************
 BIOS_CDR_Start - Start spinning the CD
 
 Supports: T630x

 Return:
    1 for success, 0 if there is no disc
 ********************************************************************/
int BIOS_CDR_Start(void);


/********************************************************************
 BIOS_CDR_StartNonBlocking - Start spinning the CD, in non-blocking
    mode
 
 Supports: T630x

 Return:
    1 for success, 0 if there is no disc, 2 if the function has not
    yet finished its execution.

 Notes:
    Application can use this function to start spinning the CD without
    blocking for long time. Then, it can poll this function: if the
    value is 2, it needs to wait. When the result is either 0 or 1,
    the function has finished execution.
 ********************************************************************/
int BIOS_CDR_StartNonBlocking(void);


/********************************************************************
 BIOS_CDR_Stop - Stop spinning the CD
 
 Supports: T630x
 ********************************************************************/
void BIOS_CDR_Stop(void);


/********************************************************************
 BIOS_CDR_TimerHandler - Polling function for the library

 Supports: T630x, BIOS 2.0, CD-Loader only

 Returns:
    1 for success, 0 for error, 2 if the function could not operate
    because of some hardware lock.

 Notes:
    The application must call this function every 2 milliseconds. If
	this function is not called, the library will not operate
	correctly
	If the return value is 2, it is strongly suggested to call again
    the function after a smaller interval, instead of waiting other
    2 ms.
 ********************************************************************/
int BIOS_CDR_TimerHandler(void);


/********************************************************************
 BIOS_CDR_GetToc - Get the TOC (table of contents)
 
 Supports: T630x

 Params:
    toc               - Pointer to the structure that will be filled
                        with TOC informations

 Notes:
    This function must called after BIOS_CDR_Start()
 ********************************************************************/
void BIOS_CDR_GetToc(BIOS_CDR_TOC *toc);


/********************************************************************
 BIOS_CDR_GetFullToc - Get the FULL TOC (table of contents)
 
 Supports: T630x

 Params:
    toc               - Pointer to the structure that will be filled
                        with TOC informations

 Notes:
    This function must called after BIOS_CDR_Start()
 ********************************************************************/
void BIOS_CDR_GetFullToc(FULL_TOC *toc);


/********************************************************************
 BIOS_CDR_GetStatus - Get the current tray status
 
 Supports: T630x

 Returns:
    Tray status (macros BIOS_CDR_IS_*):
	BIOS_CDR_IS_OPEN    : tray is open
	BIOS_CDR_IS_CLOSED  : tray is closed
	BIOS_CDR_IS_OPENING : tray is opening
	BIOS_CDR_IS_CLOSING : tray is closing
	BIOS_CDR_IS_PUSHED  : tray is pushed (??)
 ********************************************************************/
unsigned int BIOS_CDR_GetStatus(void);


/********************************************************************
 BIOS_CDR_OpenTray - Open or close the tray
 
 Supports: T630x

 Params:
    action            - Action to perform on the tray (use 
	                    BIOS_CDR_TRAY_* macros)

 Notes:
    This function is non-blocking. After called, the application can
	check the status of the requested action through the 
	BIOS_CDR_GetStatus() function.
 ********************************************************************/
void BIOS_CDR_OpenTray(int action);


/********************************************************************
 BIOS_CDR_SetSpeed - Set the cdloader speed (single or double)
 
 Supports: T630x, BIOS 2.0

 Params:
    speed             - Loader speed (use BIOS_CDR_SPEED_* macros)

 Returns:
    1 if the speed is supported, 0 if the speed is not supported.
    If BIOS_CDR_SPEED_MAX is specified, the return value is the
    maximum loader speed (BIOS_CDR_SPEED_*).

 Notes:
    If the specified speed is not supported, SetSpeed() returns 0
    (error), and does NOT change the current speed. If 
	BIOS_CDR_SPEED_MAX is specified, the maximum speed will be set
    and the function return value can be used to see which is the
    supported speed.
 ********************************************************************/
int BIOS_CDR_SetSpeed(int speed);


/********************************************************************
 BIOS_CDR_SetLoc - Start reading sectors in non-blocking mode, from
   the given absolute address
 
 Supports: T630x

 Params:
    lba               - LBA (absolute) position within the CD
	mode              - Requested data mode (use BIOS_CDR_MODE_*)

 Notes:
    Application can use MSF2LBA() to convert a mm:ss:ff position 
    into LBA.
	This function begins reading in non-blocking mode. The 
	application can use BIOS_CDR_GetNumSectorsInBuffer() to check 
	when a sector is available, and retrieve it with BIOS_CDR_GetSector(). 
	The library will keep reading sectors sequentially on the disc
	until BIOS_CDR_StopReading() is called. Example code:

    BIOS_CDR_StartReading(MSF2LBA(0,2,16), BIOS_CDR_MODE_2048);

    // Wait and get sector at 0:2:16
    while (!BIOS_CDR_GetNumSectorsInBuffer());
    BIOS_CDR_GetSector(&buff);

    // Wait and get sector at 0:2:17
    while (!BIOS_CDR_GetNumSectorsInBuffer());
    BIOS_CDR_GetSector(&buff);

    BIOS_CDR_StopReading();

    Notice that the above code assumes that BIOS_CDR_TimerHandler()
    is being called through an interrupt(for CD-Loader only).
 ********************************************************************/
void BIOS_CDR_StartReading(unsigned long lba, int mode);



/********************************************************************
 BIOS_CDR_GetNumSectorsInBuffer - Check if at least one sector of data is
   ready, when reading in non-blocking mode

 Supports: T630x

 Returns:
   0 if data is not ready, non-zero if data is ready
 ********************************************************************/
int BIOS_CDR_GetNumSectorsInBuffer(void);
    


/********************************************************************
 BIOS_CDR_GetSector - Get one sector of data, in non-blocking reading
   mode.

 Supports: T630x

 Params:
    buff              - Pointer to a pointer that will contain the
	                    address of the buffer with the sector data

 Returns:
   Read result (macros BIOS_CDR_READ_*):
	BIOS_CDR_READ_FAILURE	: reading fail
	BIOS_CDR_READ_SUCCESS	: reading finish and data is no error.
	BIOS_CDR_READ_CRC_ERROR	: reading finish but data contain error

 Notes:
   Application can check if a sector is ready through 
   BIOS_CDR_GetNumSectorsInBuffer().
 ********************************************************************/
int BIOS_CDR_GetSector(unsigned char **buff);



/********************************************************************
 BIOS_CDR_StopReading - Stop reading in non-blocking mode

 Supports: T603x
 ********************************************************************/
void BIOS_CDR_StopReading(void);


/********************************************************************
 BIOS_CDR_ReadSectors - Read sectors in blocking mode

 Supports: T630x

 Params:
    buff              - Buffer that will be filled with the requeste
                        sectors
    lba               - Address of the first sector to read
    num               - Number of sectors that must be read
    mode              - Data mode (use BIOS_CDR_MODE_*)

 Returns:
    The number of sectors actually read, or 0 in case of error

 Notes:
    Application can use MSF2LBA() to convert a mm:ss:ff position 
    into LBA.
********************************************************************/
int BIOS_CDR_ReadSectors(unsigned char* buff, unsigned long lba, int num, int mode);


/********************************************************************
 BIOS_DVD_GetMediumType - Check the current medium disc type. 
 Supports: T6305/6
 Params: None.
 Returns:
	 The medium disc type value (as fellow).
		BIOS_CD_DataOnly = 0,
		BIOS_CD_CDDA = 1,
		BIOS_CD_DataAudioMixed = 2,
		BIOS_CD_Photo = 3,
		BIOS_DVD_Medium = 4,
		BIOS_NoDiskPresent = 5,
		BIOS_MediumFormatError = 7,
		BIOS_MediumTypeUnknown = 8,
		BIOS_MediumTypeNotReady = 9, 
		BIOS_MediumTypeMax.
 ********************************************************************/
MediumType BIOS_DVD_GetMediumType(void);


/********************************************************************
 BIOS_DVD_ReportKey - Get the DVD report key.
 Supports: T6305/6;
 Params:
	AGID:		Authentication generating ID.
	Format:		Type of information needed to be sent
	Lba:		DVD Disc address.
	ByteCount:	Total bytes
	Butter:		Data buffer pointer
	ByteReturned: Returning total bytes.

  Returns:
	 1: Success.
	 0: Fail.
 ********************************************************************/
int BIOS_DVD_ReportKey(unsigned char AGID, unsigned char Format, unsigned long Lba, 
		unsigned long ByteCount, unsigned char *Buffer, unsigned long *ByteReturned);


/********************************************************************
 BIOS_DVD_ReadDVDStructure - Read the DVD Logical Unit transfered data.
 Supports: T6305/6.
 Params:
	Layer:		Specifies the layer number for which the DVD STRUCTURE 
				data will be returned.
	AGID:		Authentication generating ID.
	Format:		Type of information need to be sent
	ByteCount:	Total bytes.
	Buffer:		Data buffer pointer.
	ByteReturned: 	Returning total bytes.
 Returns:
	1: Success.
	0: Fail.
 Notes:
    DVD Structure type value:
		BIOS_DVD_Physical = 0x00.
		BIOS_DVD_Copyright = 0x01,
		BIOS_DVD_DiskKey = 0x02,
		BIOS_DVD_BCA = 0x03,
		BIOS_DVD_Manufacturer = 0x04,
		BIOS_DVD_DDS = 0x08.
 ********************************************************************/
int BIOS_DVD_ReadDVDStructure(unsigned char Layer, unsigned char AGID, unsigned char Format, 
			unsigned long ByteCount, unsigned char *Buffer, unsigned long *ByteReturned);


/********************************************************************
 BIOS_DVD_SendKey - Perform authentication for DVD Logical Units which 
	is conformed to DVD Copy Protection Scheme and to generate a Bus 
	Key as the result of authentication.
 Support: T6305/6.
 Params:
	AGID:		Authentication generating ID.
	Format:		Type of information need to be sent
	ByteCount:	Totle bytes.
	Buffer:		Data buffer pointer.
	ByteReturned: Returning total bytes.
 Returns:
	1: Success
	0: Fail.
 Notes:
	Key Structure type value:
	BIOS_KEY_AGID = 0x00,
	BIOS_KEY_ChallengeKey	 = 0x01,
	BIOS_KEY_Key1 = 0x02,
	BIOS_KEY_Key2 = 0x03,
	BIOS_KEY_TitleKey = 0x04,
	BIOS_KEY_ASF = 0x05,
	BIOS_KEY_RPCStruct = 0x06,
	BIOS_KEY_RPCState = 0x08,
	BIOS_KEY_None = 0x3F. 
 ********************************************************************/
int BIOS_DVD_SendKey(unsigned char AGID, unsigned char Format, unsigned long ByteCount, 
					 unsigned char *Buffer, unsigned long *ByteReturned);


/********************************************************************
 BIOS_CDR_GetCdText - Get the CD text buffer pointer from loader library.
 Supports: T6305/6.
 Params:
	pCDText:	OUT, pointer to a CD_TEXT pointer that contains the 
				adress of the buffer with the CD-Text information.
 Returns:
	1: Success. 
	0: Fail.
 Notes:
	In a CD disc, CD-Text information is recorded in the Lead-In area and 
	is called the Text Group. Each Text Group consists of up to 8 types 
	of language Blocks. Each Block represents one language and consists of
	a maximum of 256 sets of Pack Data. Each Pack Data is an 18-byte data 
	block.
 ********************************************************************/
int BIOS_CDR_GetCdText(CD_TEXT *pCDText);


/********************************************************************
 BIOS_CDR_InterruptHandler - ATA Device and ATA Interface interrupt handler

 Supports: T630x, BIOS 2.0

 Notes:
    The application must enable the IDE interrupt at startup
 ********************************************************************/
void BIOS_CDR_InterruptHandler(void);


/********************************************************************
 BIOS_CDR_ResetBuffer - When the decoder reset the buffer.

 Supports: T630x, BIOS 2.0

 Notes:
	Call this function to ensure the CD library's buffers pointers are 
	same step with the decoder.
 ********************************************************************/
void BIOS_CDR_ResetBuffer(void);


/********************************************************************
 BIOS_CDR_LockBuffer - Lock the buffers for decoder in DVD play

 Supports: T630x, BIOS 2.0

 Params:
  	numSectors         - Size of the buffer in number of sectors

 Notes:
    Before call this function, The application must call 
	BIOS_CDR_GetNumSectorsInBuffer to check if the sectors are ready.
	After decoding, the application must call dvdReleaseBuffer 
	to release these buffers.
 ********************************************************************/
void BIOS_CDR_LockBuffer(int numSectors);


/********************************************************************
 BIOS_CDR_ReleaseBuffer - Release the buffers locked by dvdLockBuffer

 Supports: T630x, BIOS 2.0

 ********************************************************************/
void BIOS_CDR_ReleaseBuffer(void);


/********************************************************************
 BIOS_CDR_ReadSectorNoRelease - Copy the first sector in buffers and the
	buffers's pointer don't be changed.

 Supports: T630x, BIOS 2.0

 Params:
    buff              - Pointer to a pointer that will contain the
	                    address of the buffer with the sector data
 Returns:
    1 for success, 0 for fail.
 ********************************************************************/
int BIOS_CDR_ReadSectorNoRelease(unsigned char* buff);


#ifdef __cplusplus
}
#endif

#endif
