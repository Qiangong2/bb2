/***************************************************************************
	BIOS - Application library - System library
	T-Square, Zhuhai
	Started by Bagio, 2002-10-28
 ***************************************************************************/

#ifndef __BIOS_SYS_H
#define __BIOS_SYS_H

#include "BIOS_sys_defines.h"

#ifdef __cplusplus
extern "C" {
#endif

/********************************************************************
	Functions
 ********************************************************************/

/********************************************************************
 BIOS_SYS_GetVersion - Get YOSHI BIOS version
 
 Supports: T6305/6
 
 Returns:
   Version number in high 8 bits, revision number in low 8 bits
 ********************************************************************/
unsigned int BIOS_SYS_GetVersion(void);


/********************************************************************
 BIOS_SYS_GetModel - Get the chip model
 
 Supports: T6305/6
 
 Returns:
   One of the BIOS_MODEL_* macros
 ********************************************************************/
unsigned int BIOS_SYS_GetModel(void);


/********************************************************************
 BIOS_SYS_GetCPUFrequency - Get the CPU frequency
 
 Supports: T6305/6
 
 Returns:
   The frequency in hertz
 ********************************************************************/
unsigned long BIOS_SYS_GetCPUFrequency(void);


/********************************************************************
 BIOS_SYS_GetMemorySize - Get the memory size
 
 Supports: T6305/6
 
 Returns:
   Memory size in kilobytes
 ********************************************************************/
unsigned long BIOS_SYS_GetMemorySize(void);


/********************************************************************
 BIOS_GetAudioInterface - Get the Audio interface and DAC type,
                          only called by the DSP audio library.
 Supports: T6305/6
 
 Returns:
    1 - success; 2 - fail.
 ********************************************************************/
int BIOS_GetAudioInterface(BIOS_AUDIO_INTERFACE *interface);


#ifdef __cplusplus
}
#endif

#endif
