/***************************************************************************
	BIOS - Application library - Panel library
	T-Square, Zhuhai
	Started by Bagio, 2002-10-28
 ***************************************************************************/

#ifndef __BIOS_PANEL_H
#define __BIOS_PANEL_H

#include "BIOS_vkeys.h"
#include "BIOS_panel_defines.h"

#ifdef __cplusplus
extern "C" {
#endif

/********************************************************************
	Functions
 ********************************************************************/

/********************************************************************
 BIOS_PANEL_Init - Initialize the panel library

 Supports: T6305/6
 ********************************************************************/
void BIOS_PANEL_Init(void);


/********************************************************************
 BIOS_PANEL_Update - Update the front panel

 Supports: T6305/6

 Notes:
   The application call this function about 10 times every second
 ********************************************************************/
void BIOS_PANEL_Update(void);


/********************************************************************
 BIOS_PANEL_SetItem - Set the status of an item on the panel

 Supports: T6305/6

 Params:
    item              - Item code (use BIOS_ITEM_* macros)
	status            - Status (use BIOS_ITEM_STATUS_* macros)
	customParm        - Custom parameter for the item

 Notes:
    The requested item change may not be completed until next 
	BIOS_PANEL_Update() call
 ********************************************************************/
void BIOS_PANEL_SetItem(int item, int onoff, unsigned long customParm);

/********************************************************************
 BIOS_PANEL_CheckKey - Check if requested ley is pressed 

 Supports: T6305/6

 Params:
    key               - Key that we want to test

 Returns:
    0 if the key is pressed, 1 if the key is pressed
 ********************************************************************/
unsigned int BIOS_PANEL_CheckKey(unsigned long key);

/********************************************************************
 BIOS_PANEL_GetKey - Get panel key value 

 Supports: T6305/6

 Returns: Key value defined in pnl_*.h (customers).
 ********************************************************************/
unsigned long BIOS_PANEL_GetKey(void);

#ifdef __cplusplus
}
#endif

#endif
