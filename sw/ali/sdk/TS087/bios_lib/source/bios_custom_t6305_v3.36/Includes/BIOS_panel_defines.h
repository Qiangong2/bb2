/***************************************************************************
	BIOS - Application library - Panel library defines
	T-Square, Zhuhai
	Started by Bagio, 2002-10-28
 ***************************************************************************/

#ifndef __BIOS_PANEL_DEFINES_H
#define __BIOS_PANEL_DEFINES_H

#ifdef __cplusplus
extern "C" {
#endif

// Items that can be controlled with the panel library
enum BIOS_PANEL_ITEMS
{
	BIOS_ITEM_CD=1,
	BIOS_ITEM_VCD,
	BIOS_ITEM_DTSCD,
	BIOS_ITEM_MP3,
	BIOS_ITEM_SVCD,
	BIOS_ITEM_DVD,	
	BIOS_ITEM_GAME,    //wch: during game, display Rotate circle, play item and "play".
	BIOS_ITEM_DATA,
	BIOS_ITEM_READ,    //wch: display "load"
	BIOS_ITEM_NEXT,
	BIOS_ITEM_FORWARD,
	BIOS_ITEM_BACKWARD,
	BIOS_ITEM_MP4,
	BIOS_ITEM_A,
	BIOS_ITEM_B,
	BIOS_ITEM_REC,     //wch: display "open"
	BIOS_ITEM_MIC,
	BIOS_ITEM_NODISC,  //wch: display "no disc"
	BIOS_ITEM_REPEAT,
	BIOS_ITEM_PLAY,
	BIOS_ITEM_OPEN,
	BIOS_ITEM_CLOSE,
	BIOS_ITEM_TV_NTSC,
	BIOS_ITEM_TV_PAL,
	BIOS_ITEM_TV_AUTO,
	BIOS_ITEM_FF,
	BIOS_ITEM_FR,
	BIOS_ITEM_SF,
	BIOS_ITEM_SR,
	BIOS_ITEM_AUDIO_LEFT, 
	BIOS_ITEM_AUDIO_RIGHT,
	BIOS_ITEM_AUDIO_ALL,  
	BIOS_ITEM_AUDIO_MUTE,
	BIOS_ITEM_PBC,
	BIOS_ITEM_ANGLE,
	BIOS_ITEM_AC3,
	BIOS_ITEM_DTS,
	BIOS_ITEM_CIRCLE,
	BIOS_ITEM_RANDOM,
	BIOS_ITEM_STOP,
	BIOS_ITEM_PAUSE,
	BIOS_ITEM_STEP,
	BIOS_ITEM_PROGRAM,
	BIOS_ITEM_TITLE,
	BIOS_ITEM_TITLE_NUM,
	BIOS_ITEM_CHAPTER,
	BIOS_ITEM_CHAPTER_NUM,
	BIOS_ITEM_TIME,
	BIOS_ITEM_RESET,
	BIOS_ITEM_TRACK,
	BIOS_ITEM_SPEED,
	BIOS_ITEM_LOAD,
	BIOS_ITEM_REPEAT_ALL,
	BIOS_ITEM_REPEAT_1,
	BIOS_ITEM_INVALID_DISC,
	BIOS_ITEM_REMAIN,
	BIOS_ITEM_KARAOKE,
	BIOS_ITEM_SHOWLOADING,
	BIOS_ITEM_TVSYSTEM,
	BIOS_ITEM_AUDIOTYPECHG, 
	BIOS_ITEM_TRACK_X, 
	BIOS_ITEM_TIME_X,
	BIOS_ITEM_STANDBY,
	BIOS_ITEM_DVDTITLE
};

// Status of each item
enum BIOS_PANEL_ITEM_STATUS
{
	BIOS_ITEM_STATUS_OFF = 0,
	BIOS_ITEM_STATUS_ON
};

#define BIOS_ITEM_BLINK_ON                0x0004
#define BIOS_ITEM_BLINK_OFF               0x0008

#define BIOS_ITEM_SPEED_OFF               0x0010
#define BIOS_ITEM_SPEED_SLOW              0x0020
#define BIOS_ITEM_SPEED_MIDDLE            0x0040
#define BIOS_ITEM_SPEED_FAST              0x0080

#define BIOS_ITEM_DIRECT_FORWARD          0x0100
#define BIOS_ITEM_DIRECT_BACKWARD         0x0200

#ifdef __cplusplus
}
#endif

#endif
