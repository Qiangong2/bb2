/********************************************************************************************
 *
 *		pci_init.h: definitions of PCI driver.
 *		
 *		Maintained by Germy Luo. Copyright (C) 2003 T2 Design.
 *
 *******************************************************************************************/

/* version: 0.8.0
 * For more portable and compatible, our PCI driver must support more 
 * than one address map device, such as TI8212 card. So we adjust the 
 * 'struct pci_dev_str' for all kinds PCI device.
 *
 * ---2003-11-26---
 * 
 * By Germy Luo.
 */

#ifndef _PCI_INIT_H_
#define _PCI_INIT_H_

#define M6303_PCICONFDREG	0xB8000028
#define M6303_PCICONFAREG	0xB800002C
#define M6303_INTCS		0xB8000038
#define M6303_EXTINTCS		0xB800003C

#define PCI_VENDOR_ID		0x00    /* 16 bits */
#define PCI_DEVICE_ID		0x02    /* 16 bits */
#define PCI_COMMAND		0x04    /* 16 bits */
#define PCI_STATUS		0x06    /* 16 bits */
#define PCI_CLASS_REVISION	0x08    /* High 24 bits are class, low 8 revision */
#define PCI_REVISION_ID		0x08    /* Revision ID */
#define PCI_CLASS_PROG		0x09    /* Reg. Level Programming Interface */
#define PCI_CLASS_DEVICE	0x0a    /* Device class */
#define PCI_CACHE_LINE_SIZE	0x0c    /* 8 bits */
#define PCI_LATENCY_TIMER	0x0d    /* 8 bits */
#define PCI_HEADER_TYPE		0x0e    /* 8 bits */
#define PCI_BIST		0x0f    /* 8 bits */
#define PCI_BASE_ADDRESS_0	0x10    /* 32 bits */
#define PCI_BASE_ADDRESS_1	0x14    /* 32 bits [htype 0,1 only] */
#define PCI_BASE_ADDRESS_2	0x18    /* 32 bits [htype 0 only] */
#define PCI_BASE_ADDRESS_3	0x1c    /* 32 bits */
#define PCI_BASE_ADDRESS_4	0x20    /* 32 bits */
#define PCI_BASE_ADDRESS_5	0x24    /* 32 bits */
#define PCI_CARDBUS_CIS		0x28
#define PCI_SUBSYSTEM_VENDOR_ID	0x2c
#define PCI_SUBSYSTEM_ID	0x2e
#define PCI_ROM_ADDRESS		0x30    /* Bits 31..11 are address, 10..1 reserved */
#define PCI_CAPABILITY_LIST	0x34    /* Offset of first capability list entry */
/* 0x35-0x3b are reserved */
#define PCI_INTERRUPT_LINE	0x3c    /* 8 bits */
#define PCI_INTERRUPT_PIN	0x3d    /* 8 bits */
#define PCI_MIN_GNT		0x3e    /* 8 bits */
#define PCI_MAX_LAT		0x3f    /* 8 bits */

#define  PCI_COMMAND_IO         0x1     /* Enable response in I/O space */
#define  PCI_COMMAND_MEMORY     0x2     /* Enable response in Memory space */
#define  PCI_COMMAND_MASTER     0x4     /* Enable bus mastering */
#define  PCI_COMMAND_SPECIAL    0x8     /* Enable response to special cycles */
#define  PCI_COMMAND_INVALIDATE 0x10    /* Use memory write and invalidate */
#define  PCI_COMMAND_VGA_PALETTE 0x20   /* Enable palette snooping */
#define  PCI_COMMAND_PARITY     0x40    /* Enable parity checking */
#define  PCI_COMMAND_WAIT       0x80    /* Enable address/data stepping */
#define  PCI_COMMAND_SERR       0x100   /* Enable SERR */
#define  PCI_COMMAND_FAST_BACK  0x200   /* Enable back-to-back writes */

/* Header type 1 (PCI-to-PCI bridges) */
#define PCI_PRIMARY_BUS         0x18    /* Primary bus number */
#define PCI_SECONDARY_BUS       0x19    /* Secondary bus number */
#define PCI_SUBORDINATE_BUS     0x1a    /* Highest bus number behind the bridge */
#define PCI_SEC_LATENCY_TIMER   0x1b    /* Latency timer for secondary interface */
#define PCI_IO_BASE             0x1c    /* I/O range behind the bridge */
#define PCI_IO_LIMIT            0x1d
#define  PCI_IO_RANGE_TYPE_MASK 0x0fUL  /* I/O bridging type */
#define  PCI_IO_RANGE_TYPE_16   0x00
#define  PCI_IO_RANGE_TYPE_32   0x01
#define  PCI_IO_RANGE_MASK      (~0x0fUL)
#define PCI_SEC_STATUS          0x1e    /* Secondary status register, only bit 14 used */
#define PCI_MEMORY_BASE         0x20    /* Memory range behind */
#define PCI_MEMORY_LIMIT        0x22
#define  PCI_MEMORY_RANGE_TYPE_MASK 0x0fUL
#define  PCI_MEMORY_RANGE_MASK  (~0x0fUL)
#define PCI_PREF_MEMORY_BASE    0x24    /* Prefetchable memory range behind */
#define PCI_PREF_MEMORY_LIMIT   0x26
#define  PCI_PREF_RANGE_TYPE_MASK 0x0fUL
#define  PCI_PREF_RANGE_TYPE_32 0x00
#define  PCI_PREF_RANGE_TYPE_64 0x01
#define  PCI_PREF_RANGE_MASK    (~0x0fUL)
#define PCI_PREF_BASE_UPPER32   0x28    /* Upper half of prefetchable memory range */
#define PCI_PREF_LIMIT_UPPER32  0x2c
#define PCI_IO_BASE_UPPER16     0x30    /* Upper half of I/O addresses */
#define PCI_IO_LIMIT_UPPER16    0x32
/* 0x34 same as for htype 0 */
/* 0x35-0x3b is reserved */
#define PCI_ROM_ADDRESS1        0x38    /* Same as PCI_ROM_ADDRESS, but for htype 1 */
/* 0x3c-0x3d are same as for htype 0 */
#define PCI_BRIDGE_CONTROL      0x3e
#define  PCI_BRIDGE_CTL_PARITY  0x01    /* Enable parity detection on secondary interface */
#define  PCI_BRIDGE_CTL_SERR    0x02    /* The same for SERR forwarding */
#define  PCI_BRIDGE_CTL_NO_ISA  0x04    /* Disable bridging of ISA ports */
#define  PCI_BRIDGE_CTL_VGA     0x08    /* Forward VGA addresses */
#define  PCI_BRIDGE_CTL_MASTER_ABORT 0x20  /* Report master aborts */
#define  PCI_BRIDGE_CTL_BUS_RESET 0x40  /* Secondary bus reset */
#define  PCI_BRIDGE_CTL_FAST_BACK 0x80  /* Fast Back2Back enabled on secondary interface */

/* Header type 2 (CardBus bridges) */
#define PCI_CB_CAPABILITY_LIST  0x14
/* 0x15 reserved */
#define PCI_CB_SEC_STATUS       0x16    /* Secondary status */
#define PCI_CB_PRIMARY_BUS      0x18    /* PCI bus number */
#define PCI_CB_CARD_BUS         0x19    /* CardBus bus number */
#define PCI_CB_SUBORDINATE_BUS  0x1a    /* Subordinate bus number */
#define PCI_CB_LATENCY_TIMER    0x1b    /* CardBus latency timer */
#define PCI_CB_MEMORY_BASE_0    0x1c
#define PCI_CB_MEMORY_LIMIT_0   0x20
#define PCI_CB_MEMORY_BASE_1    0x24
#define PCI_CB_MEMORY_LIMIT_1   0x28
#define PCI_CB_IO_BASE_0        0x2c
#define PCI_CB_IO_BASE_0_HI     0x2e
#define PCI_CB_IO_LIMIT_0       0x30
#define PCI_CB_IO_LIMIT_0_HI    0x32
#define PCI_CB_IO_BASE_1        0x34
#define PCI_CB_IO_BASE_1_HI     0x36
#define PCI_CB_IO_LIMIT_1       0x38
#define PCI_CB_IO_LIMIT_1_HI    0x3a
#define  PCI_CB_IO_RANGE_MASK   (~0x03UL)
/* 0x3c-0x3d are same as for htype 0 */
#define PCI_CB_BRIDGE_CONTROL   0x3e
#define  PCI_CB_BRIDGE_CTL_PARITY       0x01    /* Similar to standard bridge control register */
#define  PCI_CB_BRIDGE_CTL_SERR         0x02
#define  PCI_CB_BRIDGE_CTL_ISA          0x04
#define  PCI_CB_BRIDGE_CTL_VGA          0x08
#define  PCI_CB_BRIDGE_CTL_MASTER_ABORT 0x20
#define  PCI_CB_BRIDGE_CTL_CB_RESET     0x40    /* CardBus reset */
#define  PCI_CB_BRIDGE_CTL_16BIT_INT    0x80    /* Enable interrupt for 16-bit cards */
#define  PCI_CB_BRIDGE_CTL_PREFETCH_MEM0 0x100  /* Prefetch enable for both memory regions */
#define  PCI_CB_BRIDGE_CTL_PREFETCH_MEM1 0x200
#define  PCI_CB_BRIDGE_CTL_POST_WRITES  0x400
#define PCI_CB_SUBSYSTEM_VENDOR_ID 0x40
#define PCI_CB_SUBSYSTEM_ID     0x42
#define PCI_CB_LEGACY_MODE_BASE 0x44    /* 16-bit PC Card legacy mode base address (ExCa) */
/* 0x48-0x7f reserved */

#define MAX_PCI_DEV     64

enum base_type_t {
	MEMBASE = 0,
	IOBASE
};

typedef struct pci_dev_str {
	unsigned int	busnum;
	unsigned int	devnum;
	unsigned int	funcnum;
	unsigned int	vendor;
	unsigned int	device;
	unsigned int	irq;
	struct addr_map_str {
		unsigned int	addr_type;
		unsigned long	addr_start;
		unsigned long	addr_end;
		unsigned long	addr_len;
	} map_addr[6];
}pci_dev_t;

extern unsigned char pci_config_readb(pci_dev_t *, unsigned int);
extern unsigned short pci_config_readw(pci_dev_t *, unsigned int);
extern unsigned long pci_config_readl(pci_dev_t *, unsigned int);
extern void pci_config_writeb(pci_dev_t *, unsigned int, unsigned char);
extern void pci_config_writew(pci_dev_t *, unsigned int, unsigned short);
extern void pci_config_writel(pci_dev_t *, unsigned int, unsigned long);
extern void superconsole_pcibus_init(int);
extern void pci_irq_disable(void);
extern void pci_irq_enable(void);

#endif	/* _PCI_INIT_H_ */

