/********************************************************************************************
 *
 *		pci_init.c: the PCI slot scan and initialize engine.
 *		
 *		Maintained by Germy Luo.
 *		
 *		Copyright (C) 2003 T2 Design.
 *
 *******************************************************************************************/
/* version: 0.8.5
 * Added a pair of functins: pci_switch_to_cb() and cb_switch_to_pci() to fixed the bug about 
 * initializing the PCI Slot more than one time.
 * Initialize the memstart and iostart to the system's default value at the begining of the pci 
 * initialization.
 *
 * ---2004-04-23---
 *
 * By Germy Luo.
 */

/* version: 0.8.4
 * Remove the functions: set_pci/ide_mode(). These functions should be called outside the driver.
 *
 * ---2004-04-21---
 *
 * By Germy Luo.
 */

/* version: 0.8.3
 * Updated the 'set_pci_mode' and 'set_ide_mode' functions to remain the previous
 * PCI clock.
 * Added the pci_irq_enable() in the superconsole_pcibus_init that was done in every card's
 * driver previously.
 *
 * ---2004-03-17---
 *
 * By Germy Luo.
 */

/* version: 0.8.2
 * Added a pci_dev_t type object 'pci_dev_it8212' specially for IT8212 chipset,
 * which use PCI dev NO 5.
 * Fixed a bug of resource mapping (I/O and memory map).
 *
 * ---2004-02-25---
 *
 * By Germy Luo
 */

/*
 * version: 0.8.1
 * For more compatible and portable with our new version CPU and T2OS, we add 
 * the TLB map for PCI memory region here that was done in the T2OS before.
 *
 * ---2003-12-11---
 *
 * By Germy Luo.
 */

/*
 * version: 0.8.0
 * For more portable and compatible, our PCI driver must support the device
 * which has more than one address map, such as TI8212 card. So we adjust the
 * 'struct pci_dev_str' for all kinds PCI device.
 *
 * ---2003-11-26---
 *
 * By Germy Luo.
 */

#include "pci_init.h"
#include "common.h"

#define PCI_VER	"PCI driver version: 0.8.5"

static unsigned long memstart = 0x30000000;
static unsigned long memlimit = 0x3fffffff;
/* because the IO base is often 16-bits, so we don't use 0x10000000 and 0x103fffff */
static unsigned long iostart =  0xb0000000;
static unsigned long iolimit =  0xb03fffff;

pci_dev_t pci_dev_hb, pci_dev_usb, pci_dev_slot, pci_dev_mini, pci_dev_it8212;

static unsigned char ReadConfig8(int bus, int dev, int func, int reg)
{
	unsigned char config;

	if (bus == 0) {
		/* type 0 configuration */
		if (dev >= 8 || func >= 8 || reg >= 256)
			return 0xff;
		writel(0x80000000 | (reg & 0xfc) | (func << 8) | (dev <<11), M6303_PCICONFAREG);
	}
	
	config = readb(M6303_PCICONFDREG|(reg&0x03));
	
	return config;
}

static void WriteConfig8(int bus, int dev, int func, int reg, unsigned char data)
{
	if (bus == 0) {
		/* type 0 configuration */
		if (dev>=8 || func>=8 || reg>=256)
			return;
		writel(0x80000000 | (reg & 0xfc) | (func << 8) | (dev << 11),M6303_PCICONFAREG);
	}
	
	writeb(data, M6303_PCICONFDREG | (reg & 0x03));
}

static unsigned short ReadConfig16(int bus, int dev, int func, int reg)
{
	unsigned short config;

	if (reg & 0x01) {
		soc_printf("in PCI ReadConfig16, wrong register: 0x%x!\n", reg);
		return 0xffff;
	}
	
	if (bus == 0) {
		/* type 0 configuration */
		if (dev >= 8 || func >= 8 || reg >= 256)
			return 0xffff;
		writel(0x80000000 | (reg & 0xfc) | (func << 8) | (dev <<11), M6303_PCICONFAREG);
	}
	
	config = readw(M6303_PCICONFDREG | (reg & 0x03));
	
	return config;
}

static void WriteConfig16(int bus, int dev, int func, int reg, unsigned short data)
{
	if (reg & 0x01) {
		soc_printf("in PCI WriteConfig16, wrong register: 0x%x!\n", reg);
		return;
	}
	
	if (bus == 0) {
		/* type 0 configuration */
		if (dev>=8 || func>=8 || reg>=256)
			return;
		writel(0x80000000 | (reg & 0xfc) | (func << 8) | (dev << 11),M6303_PCICONFAREG);
	}
	
	writew(data, M6303_PCICONFDREG | (reg & 0x03));
}

static unsigned long ReadConfig32(int bus, int dev, int func, int reg)
{
	unsigned long config;
	
	if (reg & 0x03) {
		soc_printf("in PCI ReadConfig32, wrong register: 0x%x!\n", reg);
		return 0xffffffff;
	}
	
	if (bus == 0) {
		/* type 0 configuration */
		if (dev >= 8 || func >= 8 || reg >= 256)
			return 0xffffffff;
		writel(0x80000000 | (reg & 0xfc) | (func << 8) | (dev <<11), M6303_PCICONFAREG);
	}
	
	config = readl(M6303_PCICONFDREG);
	
	return config;
}

static void WriteConfig32(int bus, int dev, int func, int reg, unsigned long data)
{
	if (reg & 0x03) {
		soc_printf("in PCI WriteConfig32, wrong register: 0x%x!\n", reg);
		return;
	}
	
	if (bus == 0) {
		/* type 0 configuration */
		if (dev>=8 || func>=8 || reg>=256)
			return;
		writel(0x80000000 | (reg & 0xfc) | (func << 8) | (dev << 11),M6303_PCICONFAREG);
	}
	
	writel(data, M6303_PCICONFDREG);
}

unsigned char pci_config_readb(pci_dev_t *dev, unsigned int offset)
{
	unsigned char val;

	val = ReadConfig8(dev->busnum, dev->devnum, dev->funcnum, offset);

	return val;
}

unsigned short pci_config_readw(pci_dev_t *dev, unsigned int offset)
{
	unsigned short val;

	val = ReadConfig16(dev->busnum, dev->devnum, dev->funcnum, offset);

	return val;
}

unsigned long pci_config_readl(pci_dev_t *dev, unsigned int offset)
{
	unsigned long val;

	val = ReadConfig32(dev->busnum, dev->devnum, dev->funcnum, offset);

	return val;
}

void pci_config_writeb(pci_dev_t *dev, unsigned int offset, unsigned char val)
{
	WriteConfig8(dev->busnum, dev->devnum, dev->funcnum, offset, val);
}

void pci_config_writew(pci_dev_t *dev, unsigned int offset, unsigned short val)
{
	WriteConfig16(dev->busnum, dev->devnum, dev->funcnum, offset, val);
}

void pci_config_writel(pci_dev_t *dev, unsigned int offset, unsigned long val)
{
	WriteConfig32(dev->busnum, dev->devnum, dev->funcnum, offset, val);
}

#if 0
static inline void set_pci_mode(void)
{
	unsigned long tmp_val;

	/* bit10  for PLL bit24 */
	tmp_val = (readl(0xb8000070) & 0x400) << 14;

	local_irq_disable();
	asm volatile(".word 0x0f");
	writel(readl(0xb8000060) | (1 << 25), 0xb8000060);
	writel(readl(0xb8000054) | (1 << 14), 0xb8000054);
	writel(readl(0xb8000058) | (1 << 14), 0xb8000058);
	writel((readl(0xb8000074) | (1 << 26)) | (1 << 25) | tmp_val, 0xb8000074);
	asm volatile(".word 0x0f");
	local_irq_enable();
}

static inline void set_ide_mode(void)
{
	unsigned long tmp_val;

	/* bit10 for PLL bit24 */
	tmp_val = (readl(0xb8000070) & 0x400) << 14;
	
	local_irq_disable();
	asm volatile(".word 0x0f");
	writel((readl(0xb8000060) & ~(1 << 25)), 0xb8000060);
	writel((readl(0xb8000074) | (1 << 26) | tmp_val) & ~(1 << 25), 0xb8000074);
	writel(readl(0xb8000054) & ~(1 << 14), 0xb8000054);
	writel(readl(0xb8000058) | (1 << 14), 0xb8000058);
	asm volatile(".word 0x0f");
	local_irq_enable();
}
#endif

static inline void pci_switch_to_cb(void)
{
//	local_irq_disable();
	asm volatile(".word 0x0f");
	writel(readl(0xb8000058) | (1 << 31), 0xb8000058);
	writel(readl(0xb8000054) | (1 << 31), 0xb8000054);
	asm volatile(".word 0x0f");
//	local_irq_enable();
}

static inline void cb_switch_to_pci(void)
{
//	local_irq_disable();
	asm volatile(".word 0x0f");
	writel(readl(0xb8000058) | (1 << 31), 0xb8000058);
	writel(readl(0xb8000054) & ~(1 << 31), 0xb8000054);
	asm volatile(".word 0x0f");
//	local_irq_enable();
}

static void map_resource(int bus, int dev, int func, int nbase, unsigned long id, pci_dev_t *pci_dev_p)
{
	unsigned long val, reg, next, req, base, size;
	unsigned int addr_type;
	unsigned char irq;
	volatile int tmp_count = 0;

	/* set the related value */
	pci_dev_p->busnum = bus;
	pci_dev_p->devnum = dev;
	pci_dev_p->funcnum = func;
	pci_dev_p->vendor = id & 0xffff;
	pci_dev_p->device = (id & 0xffff0000) >> 16;

	soc_printf("busnum=%d, devnum=%d, funcnum=%d. vendor ID=0x%04x, device ID=0x%04x.\n", \
		pci_dev_p->busnum, pci_dev_p->devnum, pci_dev_p->funcnum, \
		pci_dev_p->vendor, pci_dev_p->device);
	
	/* scan all base registers */
	next = PCI_BASE_ADDRESS_0;
	
	while (next < PCI_BASE_ADDRESS_0 + nbase * 4) {
		reg = next;
		next = reg + 4;
	
		WriteConfig32(bus, dev, func, reg, 0xFFFFFFFF);
		val = ReadConfig32(bus, dev, func, reg);
		
		if (val & 1) {
			req = ~(val & 0xFFFFFFFC)+1;
		
			if (req & (req - 1))
				continue;
			if (req == 0)
				continue;
		
			size = req > 0x1000 ? req : 0x1000;
			base = (iostart + size - 1) & ~(size - 1);
		
			if ((base + size - 1) > iolimit)
				continue;
			
			addr_type = IOBASE;
			iostart = base + size;
		} else {
			req = ~(val & 0xFFFFFFF0) + 1;
		
			if (req & (req - 1))
				continue;
			if (req == 0)
				continue;
		
			switch (val & 0x00000006) {
				case 0:
					break;
				case 2:
					continue;
				case 4:
					WriteConfig32(bus, dev, func, reg + 4, 0);
					next += 4;
					break;
				case 6:
					continue;
			}
			
			size = req > 0x1000 ? req : 0x1000;
			base = (memstart + size - 1) & ~(size - 1);
		
			if ((base + size - 1) > memlimit)
				continue;
		
			addr_type = MEMBASE;
			memstart = base + size;
		}
		
		/* to determine whether the device has MEM or IO map */
		pci_dev_p->map_addr[tmp_count].addr_type = addr_type;
		pci_dev_p->map_addr[tmp_count].addr_start = base;
		pci_dev_p->map_addr[tmp_count].addr_end = base + size - 1;
		pci_dev_p->map_addr[tmp_count].addr_len = size;
		
		if (id == 0x63048866) {
			pci_dev_p->irq = 0;
#if 0			
			/* Set master latency timer and cache line size */
			WriteConfig32(bus, dev, func, PCI_CACHE_LINE_SIZE, 0x8004);
			/* Enable device and clear all status */
			WriteConfig32(bus, dev, func, PCI_COMMAND,
				ReadConfig32(bus, dev, func, PCI_COMMAND) | 0xffff0000 | PCI_COMMAND_IO
				| PCI_COMMAND_MEMORY | PCI_COMMAND_MASTER | PCI_COMMAND_SERR);
#endif
		}
		else if (id == 0x523710b9) {
			pci_dev_p->irq = 8;

			/* Configure the device */
			WriteConfig32(bus, dev, func, PCI_CACHE_LINE_SIZE, 0xf100);
			WriteConfig32(bus, dev, func, PCI_COMMAND, 0xffffffff);
			WriteConfig32(bus, dev, func, PCI_COMMAND_PARITY, 0x00000000);
#if 0
			/* Set master latency timer and cache line size */
			WriteConfig32(bus, dev, func, PCI_CACHE_LINE_SIZE, 0x8004);
			/* Enable device and clear all status */
			WriteConfig32(bus, dev, func, PCI_COMMAND,
				ReadConfig32(bus, dev, func, PCI_COMMAND) | 0xffff0000 | PCI_COMMAND_IO
				| PCI_COMMAND_MEMORY | PCI_COMMAND_MASTER | PCI_COMMAND_SERR);
#endif
		}
		else {
			pci_dev_p->irq = 0;
#if 0			
			/* Set master latency timer and cache line size */
			WriteConfig32(bus, dev, func, PCI_CACHE_LINE_SIZE, 0x8004);
			/* Enable device and clear all status */
			WriteConfig32(bus, dev, func, PCI_COMMAND,
				ReadConfig32(bus, dev, func, PCI_COMMAND) | 0xffff0000 | PCI_COMMAND_IO
				| PCI_COMMAND_MEMORY | PCI_COMMAND_MASTER | PCI_COMMAND_SERR);
#endif
		}
/*		
		soc_printf("	irq=%d, addr_type=%s.\n \
		addr_start=0x%08x, addr_end=0x%08x, addr_len=0x%08x.\n\n", \
			pci_dev_p->irq, (pci_dev_p->addr_type == 0) ? "MEMBASE" : "IOBASE", \
			base, pci_dev_p->addr_end, size);
*/		
		soc_printf("	irq=%d, addr_type=%s.\n \
		addr_start=0x%08x, addr_end=0x%08x, addr_len=0x%08x.\n\n",
			pci_dev_p->irq, (pci_dev_p->map_addr[tmp_count].addr_type == MEMBASE) ?
			"MEMBASE" : "IOBASE",
			pci_dev_p->map_addr[tmp_count].addr_start,
			pci_dev_p->map_addr[tmp_count].addr_end,
			pci_dev_p->map_addr[tmp_count].addr_len);
		
		/* set the base address to the device */
		if ((base & 0xf0000000) == 0xb0000000)
			base &= 0xffff;
		WriteConfig32(bus, dev, func, reg, base);

		tmp_count ++;
	}

	if (tmp_count == 0)
		soc_printf("	The device hasn't address map.\n\n");
}

static int pcidevs = 0;
static int superconsole_pcidev_init(int bus, int dev, int func)
{
	unsigned long id, bhlc;
	unsigned char header_type;
	static unsigned long first_fun_id;
	int nbase = 0;
	pci_dev_t *pci_dev_p;
	id = ReadConfig32(bus, dev, func, PCI_VENDOR_ID);
	if (func == 0)
		first_fun_id = id;
	
	if ((func > 0) && (id == first_fun_id))
		return -1;
	
	if ((id & 0xffff) == 0xffff)
		return -1;
	
	if (pcidevs >= MAX_PCI_DEV)
		return -1;

	pcidevs++;
	header_type = ReadConfig8(bus, dev, func, PCI_HEADER_TYPE);
//	bhlc = ReadConfig32(bus, dev, func, PCI_CACHE_LINE_SIZE);
	
	soc_printf("PCI: bus=%d, dev=%d, func=%d,vendor id=0x%04x, device id=0x%04x, BHLC=0x%02x.\n", \
		bus, dev, func, id & 0xffff, (id & 0xffff0000) >> 16, header_type);

	switch (header_type & 0x7f) {
//	switch ((bhlc >> 16) & 0x7f) {
		case 0:
			nbase = 6;
			break;
		case 1:
			nbase = 2;
			break;
		case 2:
			nbase = 1;
			break;
		default:
			break;
	}
	
	if (id == 0x63048866) {
		pci_dev_p = &pci_dev_hb;
		soc_printf("PCI DEVICE -- Host Bridge: ");
	}
	else if (id == 0x523710b9) {
		pci_dev_p = &pci_dev_usb;
		soc_printf("PCI DEVICE -- USB: ");
	}
	else {
		if (dev == 5) {
			pci_dev_p = &pci_dev_it8212;
			soc_printf("PCI DEVICE -- Device in IT8212-Slot: ");
		}
		else if (dev == 6) {
			pci_dev_p = &pci_dev_mini;
			soc_printf("PCI DEVICE -- Device in Mini-Slot: ");
		} else {
			pci_dev_p = &pci_dev_slot;
			soc_printf("PCI DEVICE -- Device in Normal-Slot: ");
		}
	}
	
	map_resource(bus, dev, func, nbase, id, pci_dev_p);
	
	/* Set master latency timer and cache line size */
	WriteConfig16(bus, dev, func, PCI_CACHE_LINE_SIZE, 0x8004);
	
	/* Enable device and clear all status */
	WriteConfig32(bus, dev, func, PCI_COMMAND,
		ReadConfig32(bus, dev, func, PCI_COMMAND) | 0xffff0000 | PCI_COMMAND_IO
		| PCI_COMMAND_MEMORY | PCI_COMMAND_MASTER | PCI_COMMAND_SERR);
	
	return 0;
}

static int counter_ver = 0;
void superconsole_pcibus_init(int bus)
{
	int dev, func;
	unsigned long type;

	if (counter_ver == 0) {
		soc_printf("%s\n", PCI_VER);
		counter_ver ++;
	}

	/* first set PCI mode instead of IDE mode, Now we do it in firmware. */
	/* don't do it any more, it is done outside the driver. */
//	set_pci_mode();
//	mdelay(50);
	
	soc_printf("before t2_pci_tlb..\n");
//	asm volatile (".word 0x7000003f");
	
	/* PCI Memory region TLB map. */
	t2_pci_tlb();
	soc_printf("after t2_pci_tlb..\n");
//	asm volatile (".word 0x7000003f");
	
	/* initialize the memstart and iostart value to the system value */
#ifdef BB2_FPGA
	memstart = 0x30020000;
#else
	memstart = 0x30000000;
#endif
	iostart = 0xb0000000;

	/* initialize the global pci_dev structure for PCI slot */
	memset(&pci_dev_hb, 0, sizeof(pci_dev_t));
	memset(&pci_dev_usb, 0, sizeof(pci_dev_t));
	memset(&pci_dev_it8212, 0, sizeof(pci_dev_t));
	memset(&pci_dev_slot, 0, sizeof(pci_dev_t));
	memset(&pci_dev_mini, 0, sizeof(pci_dev_t));
	
	soc_printf("begin to detect PCI device\n");
	cb_switch_to_pci();
	mdelay(100);
	
	for (dev = 0; dev < 8; dev++) {
		for (func = 0; func < 8; func++) {
			if (superconsole_pcidev_init(bus, dev, func)) {
				type = ReadConfig32(bus, dev, func, PCI_CACHE_LINE_SIZE);
				if ((type & 0x00800000) == 0)
					break;
			}
		}
	}

	/* to detect Card Bus device */
	if ((pci_dev_mini.vendor == 0) && (pci_dev_mini.device == 0)) {
	
		soc_printf("begin to detect Card Bus device\n");
		/* set to Card Bus mode to detect whether has cardbus device */
		pci_switch_to_cb();
		mdelay(100);
		
		for (func = 0; func < 8; func++) {
			if (superconsole_pcidev_init(bus, 6, func)) {
				type = ReadConfig32(bus, 6, func, PCI_CACHE_LINE_SIZE);
				if ((type & 0x00800000) == 0)
					break;
			}
		}
	}

	/* enable PCI irq */
	pci_irq_enable();
}

/* These two funcitons must be declared as global, for OS will call them */
void pci_irq_disable(void)
{
	*(unsigned int *)0xb800003c &= ~((unsigned int)(1 << 0));
}

void pci_irq_enable(void)
{
	*(unsigned int *)0xb800003c |= (unsigned int)(1 << 0);
}

