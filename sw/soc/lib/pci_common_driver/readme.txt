
/* version: 0.8.5
 * Added a pair of functins: pci_switch_to_cb() and cb_switch_to_pci() to
 * fixed the bug about
 * initializing the PCI Slot more than one time.
 * Initialize the memstart and iostart to the system's default value at the
 * begining of the pci
 * initialization.
 *
 * ---2004-04-23---
 *
 * By Germy Luo.
 */

/* version: 0.8.4
 * Remove the functions: set_pci/ide_mode(). These functions should be called
 * outside the driver.
 *
 * ---2004-04-21---
 *
 * By Germy Luo.
 */

/* version: 0.8.3
 * Updated the 'set_pci_mode' and 'set_ide_mode' functions to remain the previous
 * PCI clock.
 * Added the pci_irq_enable() in the superconsole_pcibus_init that was done in every card's
 * driver previously.
 *
 * ---2004-03-17---
 *
 * By Germy Luo.
 */

/* version: 0.8.2
 * Added a pci_dev_t type object 'pci_dev_it8212' specially for IT8212 chipset,
 * which use PCI dev NO 5.
 * Fixed a bug of resource mapping (I/O and memory map).
 *
 * ---2004-02-25---
 *
 * By Germy Luo
 */

/*
 * version: 0.8.1
 * For more compatible and portable with our new version CPU and T2OS, we add
 * the TLB map for PCI memory region here that was done in the T2OS before.
 *
 * ---2003-12-11---
 *
 * By Germy Luo.
 */

/*
 * version: 0.8.0
 * For more portable and compatible, our PCI driver must support the device
 * which has more than one address map, such as TI8212 card. So we adjust the
 * 'struct pci_dev_str' for all kinds PCI device.
 *
 * ---2003-11-26---
 *
 * By Germy Luo.
 */

