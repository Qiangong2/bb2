/*
 * Regular lowlevel cardbus driver ("yenta")
 *
 * (C) Copyright 1999, 2000 Linus Torvalds
 */

#include "common.h"
#include "pci_init.h"
#include "pci_pcmcia.h"
#include "i82365.h"

/* mapping IO base of WLan Card, 256 bytes */
unsigned long ricoh_iobase = 0xb000fe00;

/* the common CIS information firt 8 bytes */
static u8 cis_magic[] = {
	0x01, 0x03, 0x00, 0x00, 0xff, 0x17, 0x04, 0x67
};

static struct cis_info pccard_cis_info[] = {
	/* the index Num is according to enum network_card_type in ip_eth.h */
	{0, { 0x4e, 0x45, 0x43, 0x00, 0x57, 0x69, 0x72, 0x65, 0x6c}}, /* Num:0 is "NEC Wirel" chipset */
	{0, { 0x54, 0x68, 0x65, 0x20, 0x4c, 0x69, 0x6e, 0x6b, 0x73}}, /* Num:0 is "The Links" chipset */
	{0, { 0x20, 0x00, 0x49, 0x45, 0x45, 0x45, 0x20, 0x38, 0x30}}, /* Num:0 is "ASCII: I" chipset */
	{1, { 0x41, 0x6c, 0x69, 0x00, 0x4d, 0x34, 0x33, 0x30, 0x31}}   /* Num:1 is "Ali M4301" chipset */
};

extern pci_dev_t pci_dev_slot;

/*
 * Generate easy-to-use ways of reading a cardbus sockets
 * regular memory space ("cb_xxx"), configuration space
 * ("config_xxx") and compatibility space ("exca_xxxx")
 */

static inline u32 cb_readl(pci_dev_t *dev, unsigned int reg)
{
	u32 val = readl(dev->map_addr[0].addr_start + reg);
	
	return val;
}

static inline void cb_writel(pci_dev_t *dev, unsigned int reg, u32 val)
{
	writel(val, dev->map_addr[0].addr_start + reg);
}

static inline u8 exca_readb(pci_dev_t *dev, unsigned int reg)
{
	u8 val = readb(dev->map_addr[0].addr_start + 0x800 + reg);
	
	return val;
}

static inline u8 exca_readw(pci_dev_t *dev, unsigned int reg)
{
	u16 val;
	val = readb(dev->map_addr[0].addr_start + 0x800 + reg);
	val |= readb(dev->map_addr[0].addr_start + 0x800 + reg + 1) << 8;
	
	return val;
}

static inline void exca_writeb(pci_dev_t *dev, unsigned int reg, u8 val)
{
	writeb(val, dev->map_addr[0].addr_start + 0x800 + reg);
}

static void exca_writew(pci_dev_t *dev, unsigned int reg, u16 val)
{
	writeb(val, dev->map_addr[0].addr_start + 0x800 + reg);
	
	writeb(val >> 8, dev->map_addr[0].addr_start + 0x800 + reg + 1);
}

static void set_ricoh_pci(pci_dev_t *dev)
{
	soc_printf("config PCMCIA<->PCI adapter resource initialize. RICOH Card Control Register Base Address:0x%x.\n",
			dev->map_addr[0].addr_start);
	
	/* add by germy according to Linux */
	pci_config_writel(dev, CB_LEGACY_MODE_BASE, 0);
	
	/* set latency timer, 168 */
	pci_config_writeb(dev, PCI_LATENCY_TIMER, 0xA8);
	
	pci_config_writel(dev, PCI_PRIMARY_BUS,
			(0xB0 << 24) |			/* sec. latency timer: 176 */
			(0x04 << 16) |			/* subordinate bus */
			(0x01 << 8) |			/* secondary bus */
			0x00);				/* primary bus */
	
	/* PCI use INTA# interrupt pin, memory windown #0 prefetchable, write post enable. */
	pci_config_writew(dev, CB_BRIDGE_CONTROL, 0x0500);
	
#if 0	/* To set 160bit Card interface timing */
	/* enable read prefetch from CardBus to PCI bus */
	pci_config_writew(dev, CB_BRIDGE_CONFIGUE, pci_config_readw(dev, CB_BRIDGE_CONFIGUE) | 0x0001);

	/* 2:3:1 */
	pci_config_writew(dev, CB_16BIT_IO_CONL, 0x0432);
	
	pci_config_writew(dev, CB_16BIT_MEM_CONL, 0x431);

	/* set interface control register, 16-bit memory and I/O enhance timing */
	pci_config_writew(dev, CB_16BIT_INTERFACE_CONL, 0x0300);
	
	soc_printf("84:0x%04x, 88:0x%04x, 8a:0x%04x.\n",
		pci_config_readw(dev, CB_16BIT_INTERFACE_CONL), pci_config_readw(dev, CB_16BIT_IO_CONL),
		pci_config_readw(dev, CB_16BIT_MEM_CONL));
#endif
	/* end by germy */
}

static void set_card_control(pci_dev_t *dev)
{
	unsigned long cis_base;
	unsigned short word;

	cis_base = dev->map_addr[0].addr_start + 0x1000;

//	exca_writeb(dev, I365_STATUS, 0x6f);
//	mdelay(1);

	/* Power Control register, control the socket power: enable VCC5EN and VPPEN0 */
	exca_writeb(dev, I365_POWER, 0xb1);

	/* Misc Control 1 register, set bit0 to 1 according to the Power Control Register setting */
	exca_writeb(dev, 0x2f, 0x01);
	
	/* Interrupt and General Control register, set I/O card,not reset,no interrupt */		
	exca_writeb(dev, I365_INTCTL, 0x60);
	
	/* Address Window Enable register, memwin0 and I/Owin0 enable */
	exca_writeb(dev, I365_ADDRWIN, 0x41);
	
	/* I/O Window Control register, set I/Owin0autosize */
	exca_writeb(dev, I365_IOCTL, 0x02);

	/* memory window 0, set the CIS memory region, 16-bit data path is selected */
	word = (cis_base >> 12) & 0xffff;
	
	exca_writew(dev, I365_MEM(0) + I365_W_START, word);
	
//	word = (((cis_base + 0x1000 - 1) >> 12) & 0xffff) | 0xc000;
	word = ((cis_base + 0x1000 - 1) >> 12) & 0xffff;
	
	exca_writew(dev, I365_MEM(0) + I365_W_STOP, word);
	
	exca_writew(dev, I365_MEM(0) + I365_W_OFF, 0x4000);
	
	exca_writeb(dev, 0x40, (unsigned char)((cis_base >> 24) & 0xff));

	/* I/O window 0, set Hermes Control register's iobase */
	exca_writew(dev, I365_IO(0) + I365_W_START, (unsigned short)(ricoh_iobase & 0xffff));
	
	exca_writew(dev, I365_IO(0) + I365_W_STOP, (unsigned short)((ricoh_iobase & 0xffff) | 0x00ff));
	
//	exca_writew(dev, 0x36, 0x0200);
	exca_writew(dev, 0x36, 0x0000);

//	exca_writew(dev, 0x38, 0x0000);
	
	/* add by germy according to Linux */
	exca_writeb(dev, I365_GBLCTL, 0x00);
	
	exca_writeb(dev, I365_GENCTL, 0x00);
	/* end by germy */
}

int init_ricoh_resource(void)
{
	int tmp, rev = -1;
	unsigned char reg;
	u8 magic[48];
	pci_dev_t *ricoh;
	unsigned long cis_base;
	
	ricoh = &pci_dev_slot;
	
	cis_base = ricoh->map_addr[0].addr_start + 0x1000;
	
	/* to further setup the RICOH chipset */
	set_ricoh_pci(ricoh);
	
	/* set Card Control Registers */
	set_card_control(ricoh);
	
	/* may be teh delay time should more */
	mdelay(500);

	soc_printf("Fetched WaveLan chip ID: ");
	for (tmp = 0; tmp < 48; tmp ++) {
		magic[tmp] = readb(cis_base + (tmp << 1));
		if (tmp == 22)
			soc_printf("\nVer1:---");
		soc_printf("%02x ", magic[tmp]);
	}
	soc_printf("\n");

	soc_printf("Version1 ASCII:");
	for (tmp = 22; tmp < 48; tmp ++) {
		soc_printf("%c", magic[tmp]);
	}
	soc_printf("\n");

	/* Verify whether PC card is present */
	/* FIXME: we probably need to be smarted about this */
	if (memcmp(magic, cis_magic, sizeof(cis_magic)) != 0) {
		soc_printf("in init_ricoh_resource: The CIS value of PC card is invalid.\n");
		rev = -1;
		goto go_out;
	}
	
	for (tmp = 0; tmp < (sizeof(pccard_cis_info) / sizeof(pccard_cis_info[0])); tmp ++) {
		if (memcmp(&magic[22], pccard_cis_info[tmp].cis_version1, VER_NUM) == 0) {
			rev = pccard_cis_info[tmp].index;
			break;
		}
	}

	if (rev == -1) {
		soc_printf("in init_ricoh_resource: The CIS value of PC card is invalid.\n");
		goto go_out;
	}
	
	/* Ha, ha, ha .... germy */
	/* enable to access the mapped I/O space */
	writeb(0x41, cis_base + 0x3e0);
	mdelay(1);
	
	reg = readb(cis_base + 0x3e0);
//	soc_printf("0x300013e0: 0x%x.addr: 0x%x.\n", readb(orinoco_dev.mmio_start + 0x3e0), orinoco_dev.mmio_start + 0x3e0);
	
	if (reg != 0x41) {
		soc_printf("Error: setting COR value:0x%02x. (!= 0x41).\n", reg);
		rev = -1;
		
		goto go_out;
	}
	
	soc_printf("config PCMCIA<->PCI adapter resource initialize --- ok!\n");

go_out:
	return rev;
}

