#include <t2os.h>
#include <sockets.h>
#include <common.h>
#include <ip_eth.h>

#define PLATFORM_ASSERT(x)  \
        do{soc_printf x;__asm(".word 0x7000003f;nop");} while(0);
extern void soc_printf(char *text,...);
extern char lwip_packet_recv(unsigned char * buf, unsigned short len);


static volatile int link_flag=0;
static int first_link_clbk(struct net_link_status *plink_status)
{
    soc_printf("in fisrt_link_clbk-------------------------------------\n");
    soc_printf("the link speed is: %d (*500kbps).\n", plink_status->linkSpeed);

    if (plink_status->linkSpeed != 0)
        link_flag = 1;
    else
        link_flag = 0;
    return 0;
}

#define CLIENT_IP       "10.0.0.116"
#define SERVER_IP       "10.0.0.66"
#define GATEWAY         "10.0.0.20"
#define NETMASK         "255.255.255.0"
#define IRQ_NET 8

static int conn;

static void do_dhcp()
{
    struct dhcp_option_info *my_dhcp_info;
    int ret;

    soc_printf("Begin to do DHCP\n");
    /*setup requested DHCP options
      note: options DHCP_OPTION_SUBNET_MASK,DHCP_OPTION_ROUTER
            and DHCP_OPTION_BROADCAST should be always setup. 
            lwIP will bind above three IP address to stack 
            automaticlly if DHCP ok.*/
    dhcp_setup_option(DHCP_OPTION_SUBNET_MASK);
    dhcp_setup_option(DHCP_OPTION_ROUTER);
    dhcp_setup_option(DHCP_OPTION_BROADCAST);
		
    /*send request to DHCP server and get information from DHCP server
       note : input parameter will determin how long this funcion will
              block for DHCP message.(unit : ms) */
    ret=dhcp_request(15000);
            
    //check whether success to get DHCP message from DHCP server
    if(ERR_ERR==ret) {
        PLATFORM_ASSERT(("@@@ DHCP error!\n" ));
    } else if(ERR_TMO==ret) {
        PLATFORM_ASSERT(("@@@ DHCP timeout!\n")); 
    } else {
        soc_printf("DHCP ok !\n"); 
        
        //get the config information after DHCP ok
        //IP address from DHCP server
        my_dhcp_info=dhcp_get_option(DHCP_OPTION_IP_ADDR);
        if(my_dhcp_info) {
            /*any units left?*/
            if(!(my_dhcp_info->ptr)) {	
                PLATFORM_ASSERT(("@@@ no option unit can be fetched!\n"));
            } else {
                unsigned char *dataPtr=(char *)(my_dhcp_info->ptr);
                if(my_dhcp_info->len!=4) {
                    PLATFORM_ASSERT(("@@@ error option length!\n"));
                } else {
                    soc_printf("DHCP offer IP: %d.%d.%d.%d\n",
                                dataPtr[0],dataPtr[1],dataPtr[2],dataPtr[3]);
                }		
            }				
        }
			
        //GW from DHCP server
        my_dhcp_info=dhcp_get_option(DHCP_OPTION_ROUTER);
        if(my_dhcp_info) {
            /*any units left?*/
            if(!(my_dhcp_info->ptr)) {	
                PLATFORM_ASSERT(("@@@ no option unit can be fetched!\n"));
            } else {
                unsigned char *dataPtr=(char *)(my_dhcp_info->ptr);
                if(my_dhcp_info->len!=4) {
                    PLATFORM_ASSERT(("@@@ error option length!\n"));
                } else {
                    soc_printf("DHCP offer GW: %d.%d.%d.%d\n",
                                dataPtr[0],dataPtr[1],dataPtr[2],dataPtr[3]);
                }		
            }				
        }			
		
        //Net mask from DHCP server	
        my_dhcp_info=dhcp_get_option(DHCP_OPTION_SUBNET_MASK);
        if(my_dhcp_info) {
            /*any units left?*/
            if(!(my_dhcp_info->ptr)) {	
                PLATFORM_ASSERT(("@@@ no option unit can be fetched!\n"));
            } else {
                unsigned char *dataPtr=(char *)(my_dhcp_info->ptr);
                if(my_dhcp_info->len!=4) {
                    PLATFORM_ASSERT(("@@@ error option length!\n"));
                } else {
                    soc_printf("DHCP offer Mask: %d.%d.%d.%d\n",
                                dataPtr[0],dataPtr[1],dataPtr[2],dataPtr[3]);
                }		
            }				
        }

        //Broadcast address from DHCP server
        my_dhcp_info=dhcp_get_option(DHCP_OPTION_BROADCAST);
        if(my_dhcp_info) {
            /*any units left?*/
            if(!(my_dhcp_info->ptr)) {	
                PLATFORM_ASSERT(("@@@ no option unit can be fetched!\n"));
            } else {
                unsigned char *dataPtr=(char *)(my_dhcp_info->ptr);
                if(my_dhcp_info->len!=4) {
                    PLATFORM_ASSERT(("@@@ error option length!\n"));
                } else {
                    soc_printf("DHCP offer Broadcast: %d.%d.%d.%d\n",
                                dataPtr[0],dataPtr[1],dataPtr[2],dataPtr[3]);
                }		
            }				
        }

        //Lease time from DHCP server
        my_dhcp_info=dhcp_get_option(DHCP_OPTION_LEASE_TIME);
        if(my_dhcp_info) {
            /*any units left?*/
            if(!(my_dhcp_info->ptr)) {	
                PLATFORM_ASSERT(("@@@ no option unit can be fetched!\n"));
            } else {
                char *dataPtr=(char *)(my_dhcp_info->ptr);
                if(my_dhcp_info->len!=4) {
                    PLATFORM_ASSERT(("@@@ error option length!\n"));
                } else {
                    soc_printf("DHCP offer lease tiem: %d\n",
                               (dataPtr[0]<<24)|(dataPtr[1]<<16)|
                               (dataPtr[2]<< 8)|(dataPtr[3]));
                }
            }				
        }
    }
}

void network_init(void)
{
        int index;
        int cur_type;
        int pci_slot_num = 3;
        struct net_card_list *card_list;
        volatile int link_counter = 0;

        unsigned long  lwIPLibVersion=0;
        unsigned long  lwIPLibSubVersion=0;
        const char    *lwIPLibName=NULL;
int ret;
struct sockaddr_in in_name;
        struct ip_addr ipaddr, gw, netmask;


        //to get each card info
        for (index = 0; index < pci_slot_num; index ++) {
            //soc_printf("netowrk device index : %d  ", index);
            card_list = net_get_card(index);
            if (card_list == NULL)
              //do the special thing for no net card.
              ;
        }

        //set network mode to wire LAN
        if (net_set_card(0) != 0) {
            PLATFORM_ASSERT(("@@@ set card failed\n"));
        }
        else
            soc_printf("set netcard to LAN \n");

        //get current network type
        cur_type = net_get_cur_card();
        soc_printf("current network type is: %s\n", (cur_type == 0) ? "LAN_NET" : "WLAN_NET");

        //initialize the related card
        /* use the driver default configure structure. */
        if (cur_type == WLAN_NET)
                net_setting_load(NULL);

        soc_printf("begin to init\n");
        //initialize the current active card
        if (net_card_init() != 0) {
            PLATFORM_ASSERT(("@@@ init netcard error\n"));
        }
        else
            soc_printf("init netcard ok\n");

        //open the current active card
        if (net_card_open() != 0) {
            PLATFORM_ASSERT(("@@@ open netcard error.\n"));
        }
        else
            soc_printf("open netcard ok\n");

        /* check whether the network is linked */
        while (link_flag == 0) {
            soc_printf("send net_get_link_status command\n");
            net_get_link_status(first_link_clbk);

            dly_tsk(500);
            if (link_counter >= 20) {
                PLATFORM_ASSERT(("@@@ timeout waiting for link over(10s), link isn't OK!\n"));
            }
            link_counter ++;
         }
        /**************************************/
        //@@@@ Get TCP/IP Stack library info
        /**************************************/
        lwIPLibName=GetIPStackLibName();
        lwIPLibVersion=GetIPStackLibVersion();
        lwIPLibSubVersion=GetIPStackLibSubVersion();
        soc_printf("stack lib info: %s(%d.%d.%d.%d)\n",lwIPLibName,(lwIPLibVersion>>8)&0xff,
                                (lwIPLibVersion>>4)&0xf,(lwIPLibVersion)&0xf,lwIPLibSubVersion);

        /************************/
        //@@@@ Init TCP/IP Stack
        /************************/
        //set addresses to init network interface
        //note: if use DHCP,all the IP addresses will be ignored.
        ipaddr.addr=inet_addr(CLIENT_IP);       //IP address of interface
        netmask.addr=inet_addr(NETMASK);        //Net mask of interface
        gw.addr=inet_addr(GATEWAY);             //Gateway of interface
        //init TCP/IP Stack
        //note: the last argument always be 20 now
        if(ERR_OK!=lwIPStackInit(ipaddr,gw,netmask,20))
        {
                PLATFORM_ASSERT(("@@@ init TCP/IP stack error\n" ));
        }

        /****************************************************************/
        //@@@@ register the TCP/IP stack recv callback function to driver
        /****************************************************************/
        if (net_packet_recv_callback(lwip_packet_recv) != 0) {
                PLATFORM_ASSERT(("@@@ cann't register callback function to driver\n"));
        }

ena_irq(IRQ_NET);
    do_dhcp();

    conn = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if(conn==-1) {
        ret = getlasterror();
        PLATFORM_ASSERT(("@@@ create socket failed!(err:%d)\n",ret));
    }
    else

    soc_printf("create TCP socket ok!\n");
    in_name.sin_family = AF_INET;
    in_name.sin_port = htons(1114);
    in_name.sin_addr.s_addr = inet_addr(SERVER_IP);
    soc_printf("Attempt connection to %s\n", SERVER_IP);
    ret = connect(conn, (struct sockaddr *)(&in_name), sizeof(struct sockaddr) );
    if (ret == -1)
    {
            ret = getsockerror(conn);
            PLATFORM_ASSERT(("@@@ connect server failed!(err: %d)\n",ret));
    }
    else
            soc_printf("connect to server ok\n");


   
}

void netget(int offset, int size, u8 *buf) {
    u32 request[2];
    int ret, total;

    request[0] = htonl(offset);
    request[1] = htonl(size);
    
    if ((ret = write(conn, request, sizeof(request))) == -1) {
        ret = getsockerror(conn);
        PLATFORM_ASSERT(("@@@ send request failed(err: %d)\n",ret));
    }

    total = 0;
    while (total < size) {
        ret = recv(conn, buf+total, size-total, 0);

        if (ret > 0) {
            total += ret;
        } else if (ret == 0) {
            soc_printf("short read: %d bytes\n", total);
            break;
        } else {
            ret = getsockerror(conn);
            if (ret!= EWOULDBLOCK) { // NON-Blocking mode ? 
                PLATFORM_ASSERT(("@@@ recv data failed!(err: %d)\n",ret));
            }
        }
    }  
}

/* the ID of your net card. So it must be exclusive in the enum. */
enum net_card_type {
	/* first is PCMCIA wireless card */
	HERMES = 0,
	M4301,

	/* USB interface network card */
	M4301A,

	/* then is PCI network card */
	RTL8139,
	M4303,
	ISLPCI,
	
	/* if your card isn't in the items above, please add it here.
	 * Note: the value of your new card should be no more than 255.
	 */
	
	
	UNKNOWN_TYPE = 255
};

extern struct netcard_com_driver rtl8139_pci_driver;
extern struct netcard_com_driver islpci_pci_driver;

struct pci_card_info card_info_table[] = {
	{0x10ec, 0x8139, RTL8139, LAN_NET, &rtl8139_pci_driver, "LAN: RTL8139 card"},
	/* for ricoh PCI<-->PCMCIA card */
/*	{0x1180, 0x0475, UNKNOWN_TYPE, UNKNOWN_NET, NULL, "PCI<-->PCMCIA card"}, */
	{0x10b9, 0x4303, M4303, WLAN_NET, NULL, "WLAN: M4303.11b card"},
	{0x390c, 0x8000, M4303, WLAN_NET, NULL, "WLAN: M4303.11b card"},
	{0x1260, 0x3890, ISLPCI, WLAN_NET,/* NULL*/ &islpci_pci_driver, "WLAN: INTERSIL.11g card"},
	/* for USB card */
/*	{0x10b9, 0x5237, UNKNOWN_TYPE, UNKNOWN_NET, NULL, "USB card"}, */

	/* if your card isn't in the items above, please add it here. */
	
	
	{0, 0, -1, -1, NULL, "No card"}
};

#define CARD_INFO_SIZE (sizeof(card_info_table) / sizeof(card_info_table[0]))

int get_card_info_size(void)
{
	return CARD_INFO_SIZE;
}

