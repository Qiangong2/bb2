#include <t2os.h>
#include <pci_init.h>
#include <setworkmode.h>
#include <common.h>
#include "alires.h"

#include <gi.h>

void network_init(void);

void main_task()
{
    DPRINTF("Set PCI mode\n");
    SetWorkMode(SWITCH_TO_PCI_MODE);

    DPRINTF("configure pci..\n");
    superconsole_pcibus_init(0);
    def_irq(PCI_IRQ, pci_irq);

    DPRINTF("Initializing network...\n");
    IO_WRITE(GI_DI_CTRL_REG,  GI_DI_CTRL_MASK_WE_MASK);
    network_init();
    DPRINTF("Initializing gi...\n");
    gi_init();
    DPRINTF("Initialization is finished.\n");

    while(1) {
        gi_step();
    }
}
