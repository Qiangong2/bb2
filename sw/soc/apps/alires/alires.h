#include <common.h>
#include <gi.h>

typedef volatile unsigned long vu32;
#define IO_READ(addr)           (*(vu32 *)(addr))
#define IO_WRITE(addr,data)     (*(vu32 *)(addr)=(u32)(data))

#if 1
void soc_printf(const char *fmt, ...);
#define DPRINTF(...) soc_printf(__VA_ARGS__)
#else
#define DPRINTF(...) /*soc_printf(__VA_ARGS__)*/
#endif

void gi_init(void);
void gi_step(void);
void pci_irq(void);
#define PCI_IRQ 8

/* BI unit: bi.c */
void bi_init(void);
void bi_isr(void);
void bi_step(void);

/* DI unit: di.c */
void di_init(void);
void di_isr(void);
void di_step(void);

/* AES unit: aes.c */
void aes_init(void);
void aese_isr(void);
void aesd_isr(void);
void aes_step(void);

/* SHA unit: sha.c */
void sha_init(void);
void sha_isr(void);
void sha_step(void);

/* MC unit: mc.c */
void mc_init(void);
void mc_isr(void);
void mc_step(void);

/* DC unit: dc.c */
void dc_init(void);
void dc_isr(void);
void dc_step(void);

/* AIS unit: ais.c */
void ais_init(void);
void ais_isr(void);
void ais_step(void);
