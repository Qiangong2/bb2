#!/usr/bin/env python

import sys, socket, struct, array

def main(argv):
  if len(argv) != 2:
    sys.stderr.write("Usage: %s <disk file>\n" % argv[0])
    return -1

  f = open(argv[1])

  HOST = ''
  PORT = 1114
  s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
  s.bind((HOST, PORT))
  s.listen(1)
  while 1:
    conn, addr = s.accept()
    print 'Connected by', addr
    while 1:
      data = conn.recv(8)
      if not data: break
      (offset, length) = struct.unpack(">ll", data)
      print "Read %d bytes from 0x%08x" % (length, offset)
      f.seek(0x8000+offset)
      while length > 0:
        if length > 4096:
          chunk = 4096
        else:
          chunk = length
        c = f.read(chunk)
        a = array.array('l', c)
        a.byteswap()
        conn.send(a.tostring())
        length = length - chunk
    conn.close()


if __name__ == "__main__":
  main(sys.argv)
