#include "alires.h"

#define GI_SRAM_DI_REQUEST_BUF (GI_BASE + 0x100)
#define GI_SRAM_DI_RESPONSE_BUF (GI_BASE + 0x110)

typedef enum {
    GET_REQUEST,
    WAIT_REQUEST,
    PROCESS_REQUEST,
    SEND_RESPONSE,
    WAIT_RESPONSE,
    RESPONSE_DONE,
} DI_state;

static DI_state CurrentDIState;

void di_init(void)
{
    /* clear 0xa8 secure command and change timing */
    IO_WRITE(GI_DI_CONF_REG,  0x25/*val & ~GI_DI_CONF_SEC_CMD0_MASK*/);

    IO_WRITE(GI_DI_CTRL_REG,  GI_DI_CTRL_MASK_WE_MASK|GI_DI_CTRL_RST_MASK_MASK|GI_DI_CTRL_DONE_MASK_MASK);
}

#define DI_COMMAND(a) (((a) & GI_DI_CMD_BYTE_MASK) >> GI_DI_CMD_BYTE_SHIFT)
#define DI_CMD_INQUIRY 0x12
#define DI_CMD_READ 0xa8
#define DI_CMD_SEEK 0xab
#define DI_CMD_REQUEST_ERROR 0xe0
#define DI_CMD_AUDIO_STREAMING 0xe1
#define DI_CMD_REQUEST_AUDIO_STATUS 0xe2
#define DI_CMD_STOP_MOTOR 0xe3
#define DI_CMD_AUDIO_BUFFER_CONFIGURATION 0xe4
 
static u32 req[3];
static u32 resp[1024];
static u32 response_size;
static u32 read_size;
static u32 read_off;

void netget(int, int, u32 *);

DI_state di_continue_response(void)
{
    DI_state next;

    if (read_size) {
        if (read_size > 4096) {
            response_size = 4096;
        } else {
            response_size = read_size;
        }
        netget(read_off, response_size, resp);
        read_off += response_size;
        read_size -= response_size;
        next = SEND_RESPONSE;
    } else {
        next = GET_REQUEST;
    }

    return next;
}

DI_state di_process_request(void)
{
    int i;
    DI_state next = GET_REQUEST;

    for (i = 0; i < 3; ++i) {
       req[i] = IO_READ(GI_SRAM_DI_REQUEST_BUF+(i << 2));
    }

    switch (DI_COMMAND(req[0])) {
    case DI_CMD_INQUIRY:
        IO_WRITE(GI_SRAM_DI_RESPONSE_BUF+0, 0);
        IO_WRITE(GI_SRAM_DI_RESPONSE_BUF+4, 0x19991006);
        IO_WRITE(GI_SRAM_DI_RESPONSE_BUF+8, 0);
        IO_WRITE(GI_SRAM_DI_RESPONSE_BUF+12, 0);
        IO_WRITE(GI_SRAM_DI_RESPONSE_BUF+16, 0);
        IO_WRITE(GI_SRAM_DI_RESPONSE_BUF+20, 0);
        IO_WRITE(GI_SRAM_DI_RESPONSE_BUF+24, 0);
        IO_WRITE(GI_SRAM_DI_RESPONSE_BUF+28, 0);
        response_size = 32;
        next = SEND_RESPONSE;
        break;
    case DI_CMD_READ:
        read_size = req[2];
        read_off = req[1] << 2;
        next = di_continue_response();
        break;
    case DI_CMD_REQUEST_ERROR:
        IO_WRITE(GI_SRAM_DI_RESPONSE_BUF, 0);
        response_size = 4;
        next = SEND_RESPONSE;
        break;
    case DI_CMD_AUDIO_STREAMING:
        DPRINTF("AUDIO_STREAMING: unimplemented\n");
        IO_WRITE(GI_SRAM_DI_RESPONSE_BUF, 0);
        response_size = 4;
        next = SEND_RESPONSE;
        break;
    case DI_CMD_SEEK:
        DPRINTF("SEEK: unimplemented\n");
        IO_WRITE(GI_SRAM_DI_RESPONSE_BUF, 0);
        response_size = 4;
        next = SEND_RESPONSE;
        break;
    case DI_CMD_REQUEST_AUDIO_STATUS:
        DPRINTF("REQUEST_AUDIO_STATUS: unimplemented\n");
        IO_WRITE(GI_SRAM_DI_RESPONSE_BUF, 0);
        response_size = 4;
        next = SEND_RESPONSE;
        break;
    case DI_CMD_STOP_MOTOR:
        DPRINTF("Ack!!! Stop motor\n");
        IO_WRITE(GI_SRAM_DI_RESPONSE_BUF, 0);
        response_size = 4;
        next = SEND_RESPONSE;
        break;
    case DI_CMD_AUDIO_BUFFER_CONFIGURATION:
        DPRINTF("AUDIO_BUFFER_CONFIGURATION: unimplemented\n");
        IO_WRITE(GI_SRAM_DI_RESPONSE_BUF, 0);
        response_size = 4;
        next = SEND_RESPONSE;
        break;
    default:
        DPRINTF("Unknown DI command (%02x)\n", DI_COMMAND(req[0]));
        break;
    }

    return next;
}

void di_step(void)
{
    int i;
    u32 val;

    switch (CurrentDIState) {
    case GET_REQUEST:
        //DPRINTF("DI_BE reading 12 bytes\n");
        val = (11 << GI_DI_BE_SIZE_SHIFT) | (GI_DI_BE_EXEC_MASK) | (GI_SRAM_DI_REQUEST_BUF & GI_DI_BE_PTR_MASK);
        IO_WRITE(GI_DI_BE_REG, val);
        CurrentDIState = WAIT_REQUEST;
        break;
    case PROCESS_REQUEST:
        CurrentDIState = di_process_request();
        break;
    case SEND_RESPONSE:
        //DPRINTF("DI_BE writing %d bytes\n", response_size);
        for (i = 0; i < (response_size >> 2); ++i) {
            IO_WRITE(GI_SRAM_DI_RESPONSE_BUF + (i << 2), resp[i]);
        }
        val = ((response_size-1) << GI_DI_BE_SIZE_SHIFT) | (GI_DI_BE_EXEC_MASK) | (GI_DI_BE_OUT_MASK) | (GI_SRAM_DI_RESPONSE_BUF & GI_DI_BE_PTR_MASK);
        IO_WRITE(GI_DI_BE_REG, val);
        CurrentDIState = WAIT_RESPONSE;
        break;
    case RESPONSE_DONE:
        CurrentDIState = di_continue_response();
        break;
    }

}

void di_isr(void)
{
    u32 intr = IO_READ(GI_DI_CTRL_REG);

    //DPRINTF("di_ctrl = %08x\n", intr);
    if (intr & GI_DI_CTRL_RST_MASK) {
        /* halt di_be */
        IO_WRITE(GI_DI_BE_REG, 0);
        CurrentDIState = GET_REQUEST;
    } else {
        if (intr & GI_DI_CTRL_DONE_INTR_MASK) {
            switch (CurrentDIState) {
            case WAIT_REQUEST:
                CurrentDIState = PROCESS_REQUEST;
                break;
            case WAIT_RESPONSE:
                CurrentDIState = RESPONSE_DONE;
                break;
            }        
        }
    }

    /* clear intr */
    IO_WRITE(GI_DI_CTRL_REG, intr);
}
