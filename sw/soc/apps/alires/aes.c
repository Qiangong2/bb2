#include "alires.h"

void aes_init(void)
{
}

void aes_step(void)
{
}

void aese_isr(void)
{
    /* clear intr */
    IO_WRITE(GI_AESE_CTRL_REG, 0);
}

void aesd_isr(void)
{
    /* clear intr */
    IO_WRITE(GI_AESD_CTRL_REG, 0);
}
