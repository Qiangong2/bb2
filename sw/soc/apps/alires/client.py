#!/usr/bin/env python

import sys, socket, struct

def main(argv):
  if len(argv) != 3:
    sys.stderr.write("Usage: %s <offset> <length>\n" % argv[0])
    return -1

  HOST = ''
  PORT = 1114
  s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  s.connect((HOST, PORT))
  
  offset = int(argv[1])
  length = int(argv[2])
  s.send(struct.pack(">ll", offset, length))
  while length > 0:
    d = s.recv(4096)
    sys.stdout.write(d)
    length = length - len(d)
  s.close()
 

if __name__ == "__main__":
  main(sys.argv)
