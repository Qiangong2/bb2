#include <pci_init.h>
#include "alires.h"
#include "gi.h"

#ifdef ALIDEV
#include <ip_eth.h>
#endif

static int gi_initialized = 0;

void pci_irq(void)
{
    u32 val;

    pci_irq_disable();

#ifdef ALIDEV
    net_interrupt_isr();
#endif

    if (gi_initialized) {
        val = IO_READ(GI_BI_INTR_REG);
        if (val) {
            //DPRINTF("bi_intr = %08x\n", val);
            if (val & GI_BI_INTR_DC_INTR_MASK) {
                dc_isr();
            }
            if (val & GI_BI_INTR_SHA_INTR_MASK) {
                sha_isr();
            }
            if (val & GI_BI_INTR_MC_INTR_MASK) {
                mc_isr();
            }
            if (val & GI_BI_INTR_AESE_INTR_MASK) {
                aese_isr();
            }
            if (val & GI_BI_INTR_AESD_INTR_MASK) {
                aesd_isr();
            }
            if (val & GI_BI_INTR_AIS_INTR_MASK) {
                ais_isr();
            }
            if (val & GI_BI_INTR_DI_INTR_MASK) {
                di_isr();
            }
            if (val & GI_BI_INTR_BI_INTR_MASK) {
                bi_isr();
            }
        }
    }
    
    pci_irq_enable();
}

void gi_step(void)
{
    bi_step();
    di_step();
    aes_step();
    sha_step();
    mc_step();
    dc_step();
    ais_step();
}

void gi_init(void)
{
#ifdef ALIDEV
    /* Hack to enter secure mode */
    IO_READ(GI_ROM_START);
#endif

    /* Swap address space of SRAM & ROM */
    IO_WRITE(GI_SEC_MODE_REG, 1);

    bi_init();
    di_init();
    aes_init();
    sha_init();
    mc_init();
    dc_init();
    ais_init();

    gi_initialized = 1;
}
