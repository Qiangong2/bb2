#include <t2typedef.h>
#include <t2os.h>
#include <t2fs.h>
#include <BIOS.h>
#include <BIOS_cdr_defines.h>
#include <disc.h>
#include <osdefine.h>
#include <icelog.h>

extern int bios_start_var[];
extern int bios_end_var[];

//YOXI BIOS base address must be 512k-aligned
#define YOXI_BIOS_BASE 0xa0300000
#define YOXI_BIOS_SIZE 0x40000 //256k

// These are used by the bios library
unsigned long *bios_obj_start;
unsigned long *bios_obj_end;

#define CD_BUFFER_SECTORS 256
long long cd_buffer[2352*CD_BUFFER_SECTORS / sizeof(long long)];
#define DISC_SEM 8

static void InstallBios()
{
  //check wether the address is suitable.
  // NOTE: _end is the end of BSS from the linker script
  extern unsigned long _end;
  if( (DWORD)(&_end) > (YOXI_BIOS_BASE&0x8FFFFFFF) || YOXI_BIOS_BASE == 0 ) {
    soc_printf("mem map error\n");
    asm volatile("sdbbp");
  }

  memset((unsigned char *)YOXI_BIOS_BASE, 0, YOXI_BIOS_SIZE);
  BIOS_Install(YOXI_BIOS_BASE, (0x11|0x7e000), (YOXI_BIOS_BASE+YOXI_BIOS_SIZE));
}

static DWORD GetInputKey()
{
  DWORD KeyCode;
  
  KeyCode = BIOS_IRC_GetKeyCode();
  if(BIOS_VKEY_NULL == KeyCode)
    KeyCode = BIOS_PANEL_GetKey();
  return KeyCode;
}

static void EnableInterrupts()
{
  def_irq(IRQ_IRC, BIOS_IRC_ISR);
  ena_irq(IRQ_IRC);
}

static unsigned int get_count(void) 
{
    unsigned int count;

    __asm__ volatile (
        "mfc0\t%0,$9\n\t"
        : "=r" (count));

    return count;
}


// Count register should be working at 1/2 CPU clock
#define COUNT_MSEC (200000000L / 2L / 1000L)
#define NUM_TESTS 10

void accessTest(int nSeeks, int sector0, int sector1)
{
  int times[nSeeks];
  int buffer[2048 / sizeof(int)];
  int i;
  unsigned int start;
  unsigned int end;
  int max, min, ave;

  // 1/3rd stroke access time tests
  if (Disc_ReadSector((char *)buffer, sector0, 1, C_LBA, 2048) != TRUE) {
    soc_printf("Error reading disc\n");
    goto error;
  }
  start = get_count();
  for (i=0; i<nSeeks; i++) {
    if (Disc_ReadSector((char *)buffer, (i&1) ? sector0 : sector1, 1, C_LBA, 2048) != TRUE) {
      soc_printf("Error reading disc\n");
      goto error;
    }
    end = get_count();
    times[i] = (end > start) ? (end - start)/COUNT_MSEC : (start + (0xffffffff - end))/COUNT_MSEC;
    start = end;
  }

  max = times[0];
  min = max;
  ave = max;
  for (i=1; i<nSeeks; i++) {
    max = (max > times[i]) ? max : times[i];
    min = (min < times[i]) ? min : times[i];
    ave += times[i];
  }
  ave = ave / nSeeks;

  soc_printf("Times (ms), min: %d, max: %d, average: %d\n", min, max, ave);
 error:
  return;
}

#define NUM_BLOCKS 1000
#define BLOCK_SIZE 16
static int txBuffer[BLOCK_SIZE * 2048 / sizeof(int)];
static void transferTests()
{
  int i;
  unsigned int start;
  unsigned int end;
  unsigned int time;

  if (Disc_ReadSector((char *)txBuffer, 50000, 1, C_LBA, 2048) != TRUE) {
    soc_printf("Error reading disc\n");
    goto error;
  }

  start = get_count();
  for (i=0; i<NUM_BLOCKS; i++) {
    if (Disc_ReadSector((char *)txBuffer, 50000 + i*BLOCK_SIZE, 16, C_LBA, 2048) != TRUE) {
      soc_printf("Error reading disc\n");
      goto error;
    }
  }
  end = get_count();
  time = (end > start) ? (end - start)/COUNT_MSEC : (start + (0xffffffff - end))/COUNT_MSEC;
  soc_printf("Transfer rate (kbytes/sec) %d\n", (NUM_BLOCKS * BLOCK_SIZE * 2048) / time);

 error:
  return;
}

void main_task()
{
    
  DWORD key;
  int trayState;
  int mediaType;
  int stateChange = 0;

  // Install YOXI bios	
  bios_obj_start = (unsigned long *)bios_start_var;
  bios_obj_end = (unsigned long *)bios_end_var;
  InstallBios();

  // Init IR and Panel
  if(!BIOS_IRC_Init()) {
    soc_printf("Init IR failed\n");
  }

  BIOS_PANEL_Init();

  // Start the Disc 
  Disc_Init(DISC_SEM, (char *) cd_buffer, CD_BUFFER_SECTORS);
  Disc_Start();

  EnableInterrupts();

  // Loop on input events
  while (1) {
    key = GetInputKey();
    switch (key) {
    case BIOS_VKEY_OPEN:
      soc_printf("open\n");
      stateChange = 1;
      trayState = Disc_GetStatus();
      if (trayState == LOADER_TRAY_OPEN) {
	Disc_Tray_Close();
	while (trayState != LOADER_TRAY_CLOSE) {
	  dly_tsk(1000);
	  trayState = Disc_GetStatus();
	}
	// Delay is necessary to allow disc to start up
	dly_tsk(10000);
	mediaType = BIOS_DVD_GetMediumType();
	soc_printf("Mediatype = %d\n", mediaType);
      } else {
	Disc_Tray_Open();
	while (trayState != LOADER_TRAY_OPEN) {
	  // Delay is necessary to allow disc state to settle
	  dly_tsk(5000);
	  trayState = Disc_GetStatus();
	}
      }
      break;
    default:
      stateChange = 0;
      break;
    }

    if ((trayState == LOADER_TRAY_CLOSE) && (stateChange != 0)) {
      if (mediaType != NoDiskPresent) {

	soc_printf("1/3rd stroke (60040 sectors)\n");
	accessTest(NUM_TESTS, 50000, 110040);
	soc_printf("Short seek, inside (400 sectors)\n");
	accessTest(NUM_TESTS, 50000, 50400);
	soc_printf("Short seek, outside (400 sectors)\n");
	accessTest(NUM_TESTS, 2000000, 2000400);

	soc_printf("Really short seek (10 sectors)\n");
	accessTest(NUM_TESTS, 50000, 50010);

	transferTests();
	transferTests();
	transferTests();
	
      } else {
	soc_printf("No disk present %d\n", mediaType);
      }
    }

  }
}

