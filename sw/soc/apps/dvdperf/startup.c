#include <t2typedef.h>
#include <t2os.h>
#include <icelog.h>

#define CPU_FREQUENCY 200000000

/* ############################# OS BOOTUP PARAMETERS ############################### */

/* IRQ stack size */
#define SIZE_IRQSTACK	0x8000
/* SYS stack size, shares by tasks (also cyclic and alarm) */
#define SIZE_SYSSTACK	(0x20000+0x8000+0x10000)
/* SYS data structure size, share by semaphores, flags, message buffer controller */
#define SIZE_DATA		0x4000
/* total size */
#define SIZE_OSMEMORY (SIZE_IRQSTACK + SIZE_SYSSTACK + SIZE_DATA)


/* os memory block, deservedly put in data segment, not bss */
static char os_memory[SIZE_OSMEMORY] = {0};
/* operation system parameters */
static oslib_parameter_t osp = 
{
	CPU_FREQUENCY / 2 / 1000, 	/* timer_tick default to 1ms (MHz / 2 / 1000 = 1ms) */
	(unsigned long)os_memory, 			/* pointer to os reserved memory */
	SIZE_OSMEMORY,			/* os reserved memory size, it must be bigger than the sum of stacks plus 16K for other DS(flags, cyclics, tasks...) */
	SIZE_IRQSTACK, 			/* stack size reserved by interrupt service routines */
	SIZE_SYSSTACK,			/* system stack size, the system stack must be bigger than the sum of all task's stack */
	20, 				/* the number of tasks, so the taskid could be 1 to 20, */
	20, 				/* cyclic number (5-24)*/
	20, 				/* alarm number  (5-24)*/
	20, 				/* message buffer number (5-24)*/
	40, 				/* semaphore number (5-24)*/
	20, 				/* define flag number (5-24)*/
	0       			/* PLEASE SET THIS ITEM TO ZERO IF YOU ARE NOT THE OS DEVELOPER */
};

/* ############################# OS BOOTUP PARAMETERS  END ############################### */

#define TASK_ID_MAIN 1

extern void main_task();

void t2os_main(void)					/* this is the new os start point symbol */
{
  ER ercd;
  T_CTSK t_ctsk;
  ER ret;
	
  ret = t2os_kernel_init(&osp);	
  if (ret != E_OK) {
    soc_printf("os init fail\n");
    while(1);				/* died */
  }
	
  // Create main task
  soc_printf("Create main task\n");
  t_ctsk.task = (FP)main_task;
  t_ctsk.stksz = 0x10000; //64k
  t_ctsk.quantum = 10;
  t_ctsk.itskpri = 20;
  ercd = cre_tsk(TASK_ID_MAIN, &t_ctsk);

  // Start main task
  if((ercd = sta_tsk(TASK_ID_MAIN, 0)) != E_OK) {
    soc_printf("Start main task failed\n");
  }

  t2os_kernel_start();
}
