
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	  BB2 GI header file
  
     Original in sw/soc/include, 
     Installed at $(ROOT)/sw/soc/include
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef _BB2_GI_HEADER
#define _BB2_GI_HEADER

/* * * * * * * * * *  General GI definitions  * * * * * * * * */

#ifndef ALI_GI
	#define GI_BASE                     0x1fc00000 
#else
	#define GI_BASE                     0x08000000 
#endif

#ifdef BB2_FPGA
	#undef  GI_BASE
	#define GI_BASE                     0x30000000
#endif

#define GI_SRAM_START               (GI_BASE + 0x20000)
#define GI_SRAM_SIZE                0x18000            
#define GI_SRAM_MASK                0x1FFFF            
#define GI_ROM_START                (GI_BASE)

/* * * * * * * * * *  GI BI definitions * * * * * * * * */

#define GI_BI_REG_BASE              (GI_BASE + 0x1c000)

#define GI_BI_CTRL_REG              (GI_BI_REG_BASE)
#define GI_BI_CTRL_EXEC_MASK        0x80000000
#define GI_BI_CTRL_EXEC_SHIFT       31
#define GI_BI_CTRL_INTR_MASK        0x40000000
#define GI_BI_CTRL_INTR_SHIFT       30
#define GI_BI_CTRL_SIZE_MASK        0x3FF00000
#define GI_BI_CTRL_SIZE_SHIFT       20
#define GI_BI_CTRL_WRITE_MASK       0x00080000
#define GI_BI_CTRL_WRITE_SHIFT      19
#define GI_BI_CTRL_PTR_MASK         0x0001FF80
#define GI_BI_CTRL_PTR_SHIFT        7
#define GI_BI_CTRL_ERR_MASK         0x7
#define GI_BI_CTRL_ERR_SHIFT        0
#define GI_BI_CTRL_ERR_PAR_MASK     0x4
#define GI_BI_CTRL_ERR_PAR_SHIFT    2
#define GI_BI_CTRL_ERR_NR_MASK      0x2
#define GI_BI_CTRL_ERR_NR_SHIFT     1
#define GI_BI_CTRL_ERR_TA_MASK      1
#define GI_BI_CTRL_ERR_TA_SHIFT     0

#define GI_BI_MEM_REG               (GI_BI_REG_BASE+0x10)
#define GI_BI_MEM_MEM_MASK          0x0FFFFF80
#define GI_BI_MEM_MEM_SHIFT         7

#define GI_BI_INTR_REG              (GI_BI_REG_BASE+0x20)
#define GI_BI_INTR_DC_INTR_MASK     0x80
#define GI_BI_INTR_DC_INTR_SHIFT    7
#define GI_BI_INTR_SHA_INTR_MASK    0x40
#define GI_BI_INTR_SHA_INTR_SHIFT   6
#define GI_BI_INTR_MC_INTR_MASK     0x20
#define GI_BI_INTR_MC_INTR_SHIFT    5
#define GI_BI_INTR_AESE_INTR_MASK   0x10
#define GI_BI_INTR_AESE_INTR_SHIFT  4 
#define GI_BI_INTR_AESD_INTR_MASK   0x8
#define GI_BI_INTR_AESD_INTR_SHIFT  3 
#define GI_BI_INTR_AIS_INTR_MASK    0x4
#define GI_BI_INTR_AIS_INTR_SHIFT   2
#define GI_BI_INTR_DI_INTR_MASK     0x2
#define GI_BI_INTR_DI_INTR_SHIFT    1
#define GI_BI_INTR_BI_INTR_MASK     0x1
#define GI_BI_INTR_BI_INTR_SHIFT    0

#define GI_BI_BLK_SZ                128
#define GI_BI_BLK_MASK              0xFFFFFF80
#define GI_BI_BLK_TO                250    /* 2.5 msec */

/* * * * * * * * * *  GI DI definitions * * * * * * * * */

#define GI_DI_REG_BASE              (GI_BASE + 0x1c100)

#define GI_DI_CONF_REG              (GI_DI_REG_BASE)
#define GI_DI_CONF_SEC_CMD0_MASK    0xFF000000
#define GI_DI_CONF_SEC_CMD0_SHIFT   24
#define GI_DI_CONF_SEC_CMD1_MASK    0x00FF0000
#define GI_DI_CONF_SEC_CMD1_SHIFT   16
#define GI_DI_CONF_TIM_END_MASK     0x00000070
#define GI_DI_CONF_TIM_END_SHIFT    4
#define GI_DI_CONF_TIM_START_MASK   0x7
#define GI_DI_CONF_TIM_START_SHIFT  0

#define GI_DI_CTRL_REG              (GI_DI_REG_BASE + 0x10)
#define GI_DI_CTRL_MASK_WE_MASK     0x80000000
#define GI_DI_CTRL_MASK_WE_SHIFT    31
#define GI_DI_CTRL_BREAK_MASK_MASK  0x08000000
#define GI_DI_CTRL_BREAK_MASK_SHIFT 27
#define GI_DI_CTRL_DIR_MASK_MASK    0x04000000
#define GI_DI_CTRL_DIR_MASK_SHIFT   26
#define GI_DI_CTRL_DONE_MASK_MASK   0x02000000
#define GI_DI_CTRL_DONE_MASK_SHIFT  25
#define GI_DI_CTRL_RST_MASK_MASK    0x01000000
#define GI_DI_CTRL_RST_MASK_SHIFT   24
#define GI_DI_CTRL_BREAK_INTR_MASK  0x00080000
#define GI_DI_CTRL_BREAK_INTR_SHIFT 19
#define GI_DI_CTRL_DIR_INTR_MASK    0x00040000       
#define GI_DI_CTRL_DIR_INTR_SHIFT   18
#define GI_DI_CTRL_DONE_INTR_MASK   0x00020000
#define GI_DI_CTRL_DONE_INTR_SHIFT  17
#define GI_DI_CTRL_RST_MASK         0x00010000
#define GI_DI_CTRL_RST_SHIFT        16
#define GI_DI_CTRL_BREAK_TERM_MASK  0x00008000
#define GI_DI_CTRL_BREAK_TERM_SHIFT 15
#define GI_DI_CTRL_COVER_WE_MASK    0x00000800
#define GI_DI_CTRL_COVER_WE_SHIFT   11
#define GI_DI_CTRL_COVER_MASK       0x00000400
#define GI_DI_CTRL_COVER_SHIFT      10
#define GI_DI_CTRL_ERROR_WE_MASK    0x00000200
#define GI_DI_CTRL_ERROR_WE_SHIFT   9
#define GI_DI_CTRL_ERROR_MASK       0x00000100
#define GI_DI_CTRL_ERROR_SHIFT      8
#define GI_DI_CTRL_BREAK_STATE_MASK 0x00000008
#define GI_DI_CTRL_BREAK_STATE_SHIFT 3
#define GI_DI_CTRL_SEC_RSP_MASK     0x00000004
#define GI_DI_CTRL_SEC_RSP_SHIFT    2
#define GI_DI_CTRL_CMD_IN_MASK      0x00000002
#define GI_DI_CTRL_CMD_IN_SHIFT     1
#define GI_DI_CTRL_DIR_STATE_MASK   0x00000001
#define GI_DI_CTRL_DIR_STATE_SHIFT  0

#define ENABLE_DI_INTR              IO_WRITE(GI_DI_CTRL_REG,                    \
						GI_DI_CTRL_MASK_WE_MASK |       \
						GI_DI_CTRL_BREAK_MASK_MASK |    \
						GI_DI_CTRL_DIR_MASK_MASK |      \
						GI_DI_CTRL_DONE_MASK_MASK |     \
						GI_DI_CTRL_RST_MASK_MASK)

#define CLEAR_DI_INTR               IO_WRITE(GI_DI_CTRL_REG,                    \
						GI_DI_CTRL_BREAK_INTR_MASK |    \
						GI_DI_CTRL_DIR_INTR_MASK |      \
						GI_DI_CTRL_DONE_INTR_MASK |     \
						GI_DI_CTRL_RST_MASK)

#define GI_DI_BE_REG                (GI_DI_REG_BASE + 0x20)
#define GI_DI_BE_SIZE_MASK          0xfff00000
#define GI_DI_BE_SIZE_SHIFT         20
#define GI_DI_BE_OUT_MASK           0x00080000
#define GI_DI_BE_OUT_SHIFT          19
#define GI_DI_BE_EXEC_MASK          0x00040000
#define GI_DI_BE_EXEC_SHIFT         18
#define GI_DI_BE_PTR_MASK           0x0001fffc
#define GI_DI_BE_PTR_SHIFT          2

#define DI_SIMPLE_CMD_SIZE          12
#define DI_SIMPLE_RESPOND_SIZE      4
#define DIR_GI_TO_FLIPPER           1
#define DIR_FLIPPER_TO_GI           0

#define GI_DI_CMD_BYTE_MASK         0xFF000000
#define GI_DI_CMD_BYTE_SHIFT        24

/* * * * * * * * * *  GI AES definitions * * * * * * * * */
#define GI_AES_REG_BASE             (GI_BASE + 0x1c200)

#define GI_AESD_CTRL_REG            (GI_AES_REG_BASE)
#define GI_AESD_CTRL_EXEC_MASK      0x80000000
#define GI_AESD_CTRL_EXEC_SHIFT     31
#define GI_AESD_CTRL_MASK_MASK      0x40000000
#define GI_AESD_CTRL_MASK_SHIFT     30
#define GI_AESD_CTRL_SIZE_MASK      0x3FF00000
#define GI_AESD_CTRL_SIZE_SHIFT     20
#define GI_AESD_CTRL_PTR_MASK       0x0001FFF0
#define GI_AESD_CTRL_PTR_SHIFT      4
#define GI_AESD_CTRL_CHAIN_MASK     1
#define GI_AESD_CTRL_CHAIN_SHIFT    0

#define GI_AESD_IV_REG              (GI_AES_REG_BASE + 0x10)
#define GI_AESD_IV_IV_OFF_MASK      0x0001FFF0
#define GI_AESD_IV_IV_OFF_SHIFT     4

#define GI_AESE_CTRL_REG             (GI_AES_REG_BASE + 0x40)
#define GI_AESE_CTRL_EXEC_MASK       0x80000000
#define GI_AESE_CTRL_EXEC_SHIFT      31
#define GI_AESE_CTRL_MASK_MASK       0x40000000
#define GI_AESE_CTRL_MASK_SHIFT      30
#define GI_AESE_CTRL_SIZE_MASK       0x3FF00000
#define GI_AESE_CTRL_SIZE_SHIFT      20
#define GI_AESE_CTRL_PTR_MASK        0x0001FFF0
#define GI_AESE_CTRL_PTR_SHIFT       4
#define GI_AESE_CTRL_CHAIN_MASK      1
#define GI_AESE_CTRL_CHAIN_SHIFT     0

#define GI_AESE_IV_REG               (GI_AES_REG_BASE + 0x50)
#define GI_AESE_IV_IV_OFF_MASK       0x0001FFF0
#define GI_AESE_IV_IV_OFF_SHIFT      4

#define GI_AES_KEXP_REG             (GI_AES_REG_BASE + 0x60)
#define GI_AES_KEXP_STALL_MASK      0x10
#define GI_AES_KEXP_STALL_SHIFT     4
#define GI_AES_KEXP_READY_MASK      0x8
#define GI_AES_KEXP_READY_SHIFT     3
#define GI_AES_KEXP_EXEC_MASK       0x4
#define GI_AES_KEXP_EXEC_SHIFT      2
#define GI_AES_KEXP_IDX_MASK        0x1
#define GI_AES_KEXP_IDX_SHIFT       0

#define GI_AES_KEY_REG              (GI_AES_REG_BASE + 0x70)
#define GI_AES_KEY_KEY_MASK         0xffffffff
#define GI_AES_KEY_KEY_SHIFT        0

/* * * * * * * * * *  GI MC definitions * * * * * * * * */
#define GI_MC_REG_BASE              (GI_BASE + 0x1c600)

#define GI_MC_CTRL_REG              (GI_MC_REG_BASE)
#define GI_MC_CTRL_EXEC_MASK        0x80000000
#define GI_MC_CTRL_EXEC_SHIFT       31
#define GI_MC_CTRL_MASK_MASK        0x40000000
#define GI_MC_CTRL_MASK_SHIFT       30
#define GI_MC_CTRL_OP_MASK          0x30000000
#define GI_MC_CTRL_OP_SHIFT         28
#define GI_MC_CTRL_DROP_MASK        0x0C000000
#define GI_MC_CTRL_DROP_SHIFT       26
#define GI_MC_CTRL_CHAIN_MASK       0x02000000
#define GI_MC_CTRL_CHAIN_SHIFT      25
#define GI_MC_CTRL_SIZE_MASK        0x0000FFF0
#define GI_MC_CTRL_SIZE_SHIFT       4
#define GI_MC_CTRL_CONFLICT_MAS     0x8
#define GI_MC_CTRL_CONFLICT_SHIFT   3
#define GI_MC_CTRL_ERR_MASK         0x7
#define GI_MC_CTRL_ERR_SHIFT        0
#define GI_MC_CTRL_ERR_PAR_MASK     0x4
#define GI_MC_CTRL_ERR_PAR_SHIFT    2
#define GI_MC_CTRL_ERR_NR_MASK      0x2
#define GI_MC_CTRL_ERR_NR_SHIFT     1
#define GI_MC_CTRL_ERR_TA_MASK      0x1
#define GI_MC_CTRL_ERR_TA_SHIFT     0

#define GI_MC_ADDR_REG              (GI_MC_REG_BASE + 0x10)
#define GI_MC_ADDR_MEM_MASK         0x0FFFFFC0
#define GI_MC_ADDR_MEM_SHIFT        6

#define GI_MC_BUF_REG               (GI_MC_REG_BASE + 0x20)
#define GI_MC_BUF_OFF_MASK          0x0001FF80
#define GI_MC_BUF_OFF_SHIFT         7

/* * * * * * * * * *  GI SHA1 definitions * * * * * * * * */
#define GI_SHA_REG_BASE              (GI_BASE + 0x1c300)

#define GI_SHA_CTRL_REG              (GI_SHA_REG_BASE)
#define GI_SHA_CTRL_EXEC_MASK        0x80000000
#define GI_SHA_CTRL_EXEC_SHIFT       31
#define GI_SHA_CTRL_INTR_MASK        0x40000000
#define GI_SHA_CTRL_INTR_SHIFT       30
#define GI_SHA_CTRL_SIZE_MASK        0x3FF00000
#define GI_SHA_CTRL_SIZE_SHIFT       20
#define GI_SHA_CTRL_SAVE_MASK        0x00080000
#define GI_SHA_CTRL_SAVE_SHIFT       19
#define GI_SHA_CTRL_PTR_MASK         0x0001FFC0
#define GI_SHA_CTRL_PTR_SHIFT        6
#define GI_SHA_CTRL_CHAIN_MASK       0x00000001
#define GI_SHA_CTRL_CHAIN_SHIFT      0

#define GI_SHA_BUF_REG               (GI_SHA_REG_BASE+0x10)
#define GI_SHA_BUF_OFF_MASK          0x0001FFFC
#define GI_SHA_BUF_OFF_SHIFT         2


/* * * * * * * * * *  DC register definitions * * * * * * * * */
#define GI_DC_REG_BASE                (GI_BASE + 0x1c400)

#define GI_DC_CTRL_REG                (GI_DC_REG_BASE)
#define GI_DC_CTRL_EXEC_MASK          0x80000000
#define GI_DC_CTRL_EXEC_SHIFT         31
#define GI_DC_CTRL_MASK_MASK          0x40000000
#define GI_DC_CTRL_MASK_SHIFT         30
#define GI_DC_CTRL_OP_MASK            0x30000000
#define GI_DC_CTRL_OP_SHIFT           28  
#define GI_DC_CTRL_SG_CUR_MASK        0x01F00000
#define GI_DC_CTRL_SG_CUR_SHIFT       20
#define GI_DC_CTRL_SG_OFF_MASK        0x0001FF80
#define GI_DC_CTRL_SG_OFF_SHIFT       7
#define GI_DC_CTRL_ERR_MASK           0x7
#define GI_DC_CTRL_ERR_SHIFT          0

#define GI_DC_BUF_REG                 (GI_DC_REG_BASE + 0x10)
#define GI_DC_BUF_BUF_OFF_MASK        0x1FF00
#define GI_DC_BUF_BUF_OFF_SHIFT       8 

#define GI_DC_HADDR_REG               (GI_DC_REG_BASE + 0x20)
#define GI_DC_HADDR_H0_ADDR_MASK      0x0FFFF000
#define GI_DC_HADDR_H0_SHIFT          12

#define GI_DC_HIDX_REG                (GI_DC_REG_BASE + 0x30)
#define GI_DC_HIDX_H0_IDX_MASK        0x00700000 
#define GI_DC_HIDX_H0_IDX_SHIFT       20
#define GI_DC_HIDX_H1_PTR_MASK        0x0001FFFC
#define GI_DC_HIDX_H1_PTR_SHIFT       2

#define GI_DC_DIOFF_REG               (GI_DC_REG_BASE + 0x40)
#define GI_DC_DIOFF_DI_LAST_MASK      0x0FFF0000
#define GI_DC_DIOFF_DI_LAST_SHIFT     16
#define GI_DC_DIOFF_DI_FIRST_MASK     0xFFF
#define GI_DC_DIOFF_DI_FIRST_SHIFT    0

#define GI_DC_IV_REG                  (GI_DC_REG_BASE + 0x50)
#define GI_DC_IV_IV_OFF_MASK          0x0001FFF0
#define GI_DC_IV_IV_OFF_SHIFT         4

#define GI_DC_H0_REG                  (GI_DC_REG_BASE + 0x60)
#define GI_DC_H0_H0_BASE_MASK         0x0001FF80
#define GI_DC_H0_H0_BASE_SHIFT        7

#define GI_DC_H1_REG                  (GI_DC_REG_BASE + 0x70)
#define GI_DC_H1_H1_BASE_MASK         0x0001FFF0
#define GI_DC_H1_H1_BASE_SHIFT        4

/* * * * * * * * * *  GI sec definitions * * * * * * * * */
#define GI_SEC_REG_BASE             (GI_BASE + 0x1c000)

#define GI_SEC_MODE_REG             (GI_SEC_REG_BASE + 0x40)
#define GI_SEC_MODE_ALT_BOOT_MASK   0x40
#define GI_SEC_MODE_ALT_BOOT_SHIFT  6
#define GI_SEC_MODE_TEST_ENA_MASK   0x20
#define GI_SEC_MODE_TEST_ENA_SHIFT  5
#define GI_SEC_MODE_SGI_MASK        0x10
#define GI_SEC_MODE_SGI_SHIFT       4
#define GI_SEC_MODE_STIMER_MASK     0x8
#define GI_SEC_MODE_STIMER_SHIFT    3
#define GI_SEC_MODE_SAPP_MASK       0x4
#define GI_SEC_MODE_SAPP_SHIFT      2
#define GI_SEC_MODE_RESET_MASK      0x2
#define GI_SEC_MODE_RESET_SHIFT     1
#define GI_SEC_MODE_SECURE_MASK     0x1
#define GI_SEC_MODE_SECURE_SHIFT    0

#define GI_SEC_TIMER_REG            (GI_SEC_REG_BASE + 0x50)
#define GI_SEC_TIMER_TRESET_MASK    0x01000000
#define GI_SEC_TIMER_TRESET_SHIFT   24
#define GI_SEC_TIMER_VAL_MASK       0x00FFFFFF
#define GI_SEC_TIMER_VAL_SHIFT      0

#define GI_SEC_SRAM_REG             (GI_SEC_REG_BASE + 0x60)
#define GI_SEC_SRAM_PTR_MASK        0x0001FFF0 
#define GI_SEC_SRAM_PTR_SHIFT       4

#define GI_SEC_ENTER_REG            (GI_SEC_REG_BASE + 0x70)

/* * * * * * * * * *  GI ais definitions * * * * * * * * */
#define GI_AIS_REG_BASE             (GI_BASE + 0x1c500)

#define GI_AIS_CTRL_REG             (GI_AIS_REG_BASE)
#define GI_AIS_CTRL_EXEC_MASK       0x80000000
#define GI_AIS_CTRL_EXEC_SHIFT      31
#define GI_AIS_CTRL_MASK_MASK       0x40000000
#define GI_AIS_CTRL_MASK_SHIFT      30

#define GI_AIS_CTRL_ERR_MASK        7
#define GI_AIS_CTRL_ERR_SHIFT       0
#define GI_AIS_CTRL_ERR_PAR_MASK    0x4
#define GI_AIS_CTRL_ERR_PAR_SHIFT   2
#define GI_AIS_CTRL_ERR_NR_MASK     0x2
#define GI_AIS_CTRL_ERR_NR_SHIFT    1
#define GI_AIS_CTRL_ERR_TA_MASK     1
#define GI_AIS_CTRL_ERR_TA_SHIFT    0

#define GI_AIS_PTR0_REG             (GI_AIS_REG_BASE + 0x10)
#define GI_AIS_PTR0_MEM_PTR0_MASK   0x0FFFF000
#define GI_AIS_PTR0_MEM_PTR0_SHIFT  12
#define GI_AIS_PTR0_EMPTY_MASK      1
#define GI_AIS_PTR0_EMPTY_SHIFT     0

#define GI_AIS_PTR1_REG             (GI_AIS_REG_BASE + 0x20)
#define GI_AIS_PTR1_MEM_PTR1_MASK   0x0FFFF000
#define GI_AIS_PTR1_MEM_PTR1_SHIFT  12
#define GI_AIS_PTR1_EMPTY_MASK      1
#define GI_AIS_PTR1_EMPTY_SHIFT     0

#define GI_AIS_BUF_REG              (GI_AIS_REG_BASE + 0x30)
#define GI_AIS_BUF_BUF_OFF_MASK     0x0001FF00
#define GI_AIS_BUF_BUF_OFF_SHIFT    8         

#endif
