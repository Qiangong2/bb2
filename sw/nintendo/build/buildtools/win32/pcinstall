#!bash

########################################################
# This script will create the install image for the PC Emulator
# into INSTALL_ROOT from TREE_ROOT. This script should be called
# from TREE_ROOT. It will build all the libraries and demos into
# the TREE_ROOT then clean object files. Then all included files
# and directories are copied from the TREE_ROOT to the INSTALL_ROOT
# Notice that special documentation, exported data, and the PC
# specific exporter are copied from the 'Pcemu_Specific' directory
########################################################

# Store current directory
MAKEDIR=`pwd`

# ROOT Directories
TREE_ROOT='//c/rabin/dolphin/'
INSTALL_ROOT='//c/pcemu/'

# Tar File ROOT
TARFILE_ROOT='//c/Pcemu_Specific'

# Directories to move from TREE_ROOT to INSTALL_ROOT
INSTALL_DIRECTORIES='
build/buildtools
build/charPipeline
build/demos/charPipeline
build/demos/gxdemo
build/libraries/demo
build/libraries/G2D
build/libraries/mtx
build/libraries/win32
build/samplebuild
build/graphicTools
dvddata/gxDemos
dvddata/gxTests
include/charPipeline
include/dolphin
include/Mac
include/win32
man
X86
win32
docs/Developer/PDF/SDK1.0
'

# Directories containing files to move from TREE_ROOT
# to INSTALL_ROOT
INSTALL_FILES='
build
include
dvddata
'

# Directories containing executables that should
# be included
DEMO_DIRECTORIES='
build/demos/gxdemo/bin/win32
build/demos/charPipeline/bin/win32
'


TARFILE='pcemu.tar'

########################################################

# Destroy Root
echo --- Deleting Install Root Dir: $INSTALL_ROOT
rm -rf $INSTALL_ROOT/*

# Build Libraries
echo --- Building Libraries
makelibs.bat

# Build Demos
echo --- Building Demos
source makedemos

# Create Demos Directories
echo --- Creating Demo Directories
for d in $DEMO_DIRECTORIES; do \
    if [ ! -d $INSTALL_ROOT/$d ] ; then \
        mkdir -p $INSTALL_ROOT/$d ;\
    fi \
done \

# Copy Demo Recursively
echo --- Copying Demos
for d in $DEMO_DIRECTORIES; do \
    cp -R $TREE_ROOT/$d/* $INSTALL_ROOT/$d ;\
done \

# Make Clean
echo --- Cleaning
cd $MAKEDIR
source makeclean

# Create Install Directories
echo --- Creating Directories
for d in $INSTALL_DIRECTORIES; do \
    if [ ! -d $INSTALL_ROOT/$d ] ; then \
        mkdir -p $INSTALL_ROOT/$d ;\
    fi \
done \

# Copy Directories Recursively
echo --- Copying Directories
for d in $INSTALL_DIRECTORIES; do \
    cp -R $TREE_ROOT/$d/* $INSTALL_ROOT/$d ;\
done \

# Copy Files 
echo --- Copying Files
for d in $INSTALL_FILES; do \
    cp $TREE_ROOT/$d/* $INSTALL_ROOT/$d ;\
done \

# Untar PC Specific files
echo --- Extracting PC Specific tar file
cd $INSTALL_ROOT
cd ..
tar -xvf $TARFILE_ROOT/$TARFILE

# Delete vssver files
echo --- Deleting Visual Source Save Files
cd $INSTALL_ROOT
rm -rf `find | grep vssver.scc`

# Change Permision
echo --- Changing File Permissions
chmod -R 777 *

cd $MAKEDIR

echo --- Done.
