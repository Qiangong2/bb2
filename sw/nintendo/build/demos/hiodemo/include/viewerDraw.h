//----------------------------------------------------------------------
//	(C)2000-2001 Nintendo
//----------------------------------------------------------------------
#ifndef	_VIEWERDRAW_H_
#define	_VIEWERDRAW_H_

void viewerDrawInit( void );
void viewerDrawMain( void );

#endif	//_VIEWERDRAW_H_
