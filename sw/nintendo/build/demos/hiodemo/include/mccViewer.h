/*---------------------------------------------------------------------*

Project:  mcc bitmap viewer - viewer module
File:     mccViewer.h

(C) 2000 Nintendo

-----------------------------------------------------------------------*/

#ifndef	__MCC_BITMAP_VIEWER__VIEWER__
#define	__MCC_BITMAP_VIEWER__VIEWER__

// FUNCTION PROTOTYPES
// ==============================
void viewerInitialize();
void viewerDrawBitmap( void *buffer, u32 bmpWidth, u32 bmpHeight );

#endif 