//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by viewer_server.rc
//
#define IDS_STRING_START                1
#define IDS_STRING_STOP                 2
#define IDS_STRING_ERROR_CANT_DETECT_ADAPTER 3
#define IDS_STRING_ERROR_NO_ADAPTER     4
#define IDS_STRING_ERROR_CANT_INIT_MCC  5
#define IDS_STRING_ERROR_CANT_START_TTY_SERVER 6
#define IDS_STRING_ERROR_CANT_START_FIO_SERVER 7
#define IDS_STRING_ERROR_CANT_OPEN_CONSOLE 8
#define IDD_VIEWER_SERVER_DIALOG        102
#define IDR_MAINFRAME                   128
#define IDD_DIALOG_NUM                  129
#define IDC_BUTTON_START                1000
#define IDC_LIST_CONFIG                 1001
#define IDC_EDIT_NUM                    1002

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1003
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
