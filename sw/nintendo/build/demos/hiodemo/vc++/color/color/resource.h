//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by color.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_COLOR_DIALOG                102
#define IDS_MESSAGE_NONE                102
#define IDS_STRING_TITLE                103
#define IDS_ERROR_INITIALIZE            104
#define IDS_ERROR_CHANGEMODE            105
#define IDR_MAINFRAME                   128
#define IDR_ICON_RED                    129
#define IDR_ICON_GREEN                  130
#define IDR_ICON_BLUE                   131
#define IDC_SLIDER_RED                  1000
#define IDC_SLIDER_GREEN                1001
#define IDC_SLIDER_BLUE                 1002
#define IDC_BUTTON_MESSAGE              1004

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1006
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
