// FIOView.cpp : implementation file
//

#include "stdafx.h"
#include "usbsample.h"
#include "FIOView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFIOView

CFIOView::CFIOView()
{
}

CFIOView::~CFIOView()
{
}


BEGIN_MESSAGE_MAP(CFIOView, CChildView)
	//{{AFX_MSG_MAP(CFIOView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CFIOView message handlers
