// TTYView.cpp : implementation file
//

#include "stdafx.h"
#include "usbsample.h"
#include "TTYView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTTYView

CTTYView::CTTYView()
{
}

CTTYView::~CTTYView()
{
}


BEGIN_MESSAGE_MAP(CTTYView, CChildView)
	//{{AFX_MSG_MAP(CTTYView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CTTYView message handlers
