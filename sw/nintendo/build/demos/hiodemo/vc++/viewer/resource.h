//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by usbsample.rc
//
#define IDD_ABOUTBOX                    100
#define IDR_MAINFRAME                   128
#define IDR_USBSAMTYPE                  129
#define ID_MCCSTATUS                    130
#define IDD_DIALOG_FILER                130
#define IDC_LIST1                       1001
#define IDC_EDIT_ROOT                   1002
#define IDC_EDIT_PATH                   1003
#define IDC_BUTTON_REFRESH              1004
#define IDC_CHECK_ENABLESRV             1006
#define IDC_BUTTON_CHOOSEDIR            1007
#define IDC_EDIT_MASK                   1008
#define ID_APP_STARTSERVER              32771
#define ID_APP_FIOROOTDIR               32772

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32773
#define _APS_NEXT_CONTROL_VALUE         1009
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
