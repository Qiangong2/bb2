//  SP Demo Application
//  (c) Copyright 2001 Nintendo Technology Development, Inc.
//  
#define SFX_MENU    	    0
#define SFX_DRUM    	    1
#define SFX_GUNSHOT         2
#define VOICE_NGC_MAN       3
#define VOICE_NGC_WOMAN     4
#define SFX_HONK_LOOPED     5
