/*---------------------------------------------------------------------------*
  Project:  Dolphin
  File:     gd-indtex-gc.c

  Copyright 2003 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: gd-indtex-gc.c,v $
  Revision 1.1.1.1  2004/06/09 17:36:08  paulm
  GC demos from Nintendo SDK

    
    3     03/03/05 14:29 Hirose
    More updates.
    
    2     03/02/28 17:42 Hirose
    Updates.
    
    1     03/02/21 12:12 Hirose
    Initial ver.
    
  $NoKeywords: $
 *---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*
   gd-indtex
     Displaylist demo with indirect texture commands
     [Main source code for GAMECUBE exectable]
 *---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*
   Header files
 *---------------------------------------------------------------------------*/
#include <demo.h>
#include <math.h>
#include <dolphin/gd.h>

#include "gd-indtex.h"

/*---------------------------------------------------------------------------*
   Macro definitions
 *---------------------------------------------------------------------------*/
#define PI          3.14159265F
#define MAX_Z       0x00ffffff // max value of Z buffer

#define NUM_CUBES       4
#define NUM_TORI        3
#define NUM_SPHERES     1
#define NUM_GOBJS       (NUM_CUBES+NUM_TORI+NUM_SPHERES)
#define BOUNDARY        400

#define Clamp(val,min,max) \
    ((val) = (((val) < (min)) ? (min) : ((val) > (max)) ? (max) : (val)))

/*---------------------------------------------------------------------------*
   Structure definitions
 *---------------------------------------------------------------------------*/
// for Effect/Display List Manager
typedef struct
{
    u16     baseViewWd;
    u16     baseViewHt;
    u16     copyTexWd;
    u16     copyTexHt;
    void*   dlPtr;
    u32     dlSize;
} EffectManager;

// for camera
typedef struct
{
    Vec    location;
    Vec    up;
    Vec    target;
    f32    left;
    f32    top;
    f32    znear;
    f32    zfar;
} CameraConfig;

typedef struct
{
    CameraConfig  cfg; 
    Mtx           view;
    Mtx44         proj;
} MyCameraObj;

// for cube objects
typedef struct
{
    Vec         pos;
    Vec         vel;
    Vec         axis;
    f32         deg;
} MyCubeObj;

// for entire scene control
typedef struct
{
    MyCameraObj cam;
    MyCubeObj   cube[NUM_CUBES];
    s32         torusAngle[3];
    u32         effectNo;
} MySceneCtrlObj;

/*---------------------------------------------------------------------------*
   Forward references
 *---------------------------------------------------------------------------*/
void        main                ( void );
static void DrawInit            ( MySceneCtrlObj* sc );
static void DrawTick            ( MySceneCtrlObj* sc );
static void AnimTick            ( MySceneCtrlObj* sc );
static void DrawModels          ( MySceneCtrlObj* sc );
static void DrawFullScrQuad     ( void );
static void CopyTextureFromFB   ( EffectManager* em );
static void SetTextures         ( EffectManager* em );
static void SetCamera           ( MyCameraObj* cam );
static void SetScreenSpaceMode  ( void );
static void SetLight            ( void );
static void PrepareDL           ( void );
static void PrintIntro          ( void );

/*---------------------------------------------------------------------------*
  Model and texture data
 *---------------------------------------------------------------------------*/
// Texture Data
u16 EffectTex0TLUT[FXTEX0_TL_N];
u32 EffectTexture0[];
u8  EffectTexture1[];
u32 EffectTexture2A[];
u32 EffectTexture2B[];
u32 EffectTexture3[];


// for cube models
#define REG_AMBIENT  ColorArray[NUM_GOBJS]
#define LIGHT_COLOR  ColorArray[NUM_GOBJS+1]
#define BG_COLOR     ColorArray[NUM_GOBJS+2]

static GXColor ColorArray[NUM_GOBJS+3] ATTRIBUTE_ALIGN(32) = 
{
    { 0x60, 0x60, 0xFF, 0xFF },     // CUBES
    { 0x40, 0xC0, 0x80, 0xE0 },
    { 0xE0, 0xA0, 0x80, 0xC0 },
    { 0xFF, 0x60, 0xC0, 0xA0 },
    { 0xFF, 0xA0, 0xFF, 0x80 },     // TORUS
    { 0xA0, 0xFF, 0xFF, 0x60 },
    { 0xFF, 0xFF, 0xA0, 0x40 },
    { 0xE0, 0xE0, 0xE0, 0x20 },     // SPHERE
    { 0x40, 0x40, 0x40, 0xFF },     // AMBIENT
    { 0xFF, 0xFF, 0xFF, 0x00 },     // LIGHT
    { 0x00, 0x00, 0x00, 0x00 }      // BACKGROUND
};

static Vec CubeIniData[NUM_CUBES*3] =
{
    // Position          Velocity                 Rotation axis
    { -300, -150,  30 }, {  5.0F,  2.5F,  5.0F }, {  0.0F,  1.0F,  1.0F },
    {  300, -150,  30 }, {  5.0F, -7.5F, -2.5F }, {  0.5F,  0.0F, -1.0F },
    { -200,  250,   0 }, {  2.5F, -2.5F, -7.5F }, { -0.5F, -1.0F,  0.0F },
    {  200,  250,   0 }, { -5.0F, -5.0F, -5.0F }, {  1.0F,  1.0F,  1.0F },
};

// Effect Informations
static EffectManager Effects[NUM_EFFECTS] =
{
    { BASEVIEW0_WD, BASEVIEW0_HT, COPYTEX0_WD, COPYTEX0_HT, 0, 0 },
    { BASEVIEW1_WD, BASEVIEW1_HT, COPYTEX1_WD, COPYTEX1_HT, 0, 0 },
    { BASEVIEW2_WD, BASEVIEW2_HT, COPYTEX2_WD, COPYTEX2_HT, 0, 0 },
    { BASEVIEW3_WD, BASEVIEW3_HT, COPYTEX3_WD, COPYTEX3_HT, 0, 0 },
};

/*---------------------------------------------------------------------------*
   Camera configuration
 *---------------------------------------------------------------------------*/
static CameraConfig DefaultCamera =
{
    {   0.0F, 0.0F, 800.0F }, // location
    {   0.0F, 1.0F, 0.0F }, // up
    {   0.0F, 0.0F, 0.0F }, // target
    -160.0F,  // left
     120.0F,  // top
     200.0F,  // near
    2000.0F   // far
};

/*---------------------------------------------------------------------------*
   Global variables
 *---------------------------------------------------------------------------*/
static MySceneCtrlObj   SceneCtrl;                // scene control parameters
static u8*              TexBuffer;

/*---------------------------------------------------------------------------*
   Application main loop
 *---------------------------------------------------------------------------*/
void main ( void )
{
    DEMOInit(&GXNtsc480IntDf);  // Init the OS, game pad, graphics and video.

    DrawInit(&SceneCtrl); // Initialize vertex formats, array pointers
                          // and default scene settings.

    PrintIntro();    // Print demo directions

    while(!(DEMOPadGetButton(0) & PAD_BUTTON_MENU))
    {   
		DEMOBeforeRender();
        DrawTick(&SceneCtrl);    // Draw the model.
        DEMODoneRender();
        DEMOPadRead();           // Read controller
        AnimTick(&SceneCtrl);    // Do animation
    }

    OSHalt("End of test");
}

/*---------------------------------------------------------------------------*
   Functions
 *---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*
    Name:           DrawInit
    
    Description:    Initializes the vertex attribute format and sets up
                    the array pointer for the indexed data.
                    This function also initializes scene control parameters.
                    
    Arguments:      sc : pointer to the structure of scene control parameters
    
    Returns:        none
 *---------------------------------------------------------------------------*/
static void DrawInit( MySceneCtrlObj* sc )
{
    u32             i;

    // Texture Buffer
    TexBuffer = (u8*)OSAlloc(GXGetTexBufferSize(SCREEN_WD, SCREEN_HT, GX_TF_RGB5A3, GX_FALSE, 0));

    // Vertex Attribute
    GXSetVtxAttrFmt(GX_VTXFMT0, GX_VA_POS, GX_POS_XYZ, GX_S16, 0);
    GXSetVtxAttrFmt(GX_VTXFMT0, GX_VA_TEX0, GX_TEX_ST, GX_S16, 8);
    GXSetVtxAttrFmt(GX_VTXFMT0, GX_VA_CLR0, GX_CLR_RGBA, GX_RGBA8, 0);

    GXSetVtxAttrFmt(GX_VTXFMT1, GX_VA_POS, GX_POS_XYZ, GX_S16, 0);
    GXSetVtxAttrFmt(GX_VTXFMT1, GX_VA_TEX0, GX_TEX_ST, GX_S16, 8);
    GXSetVtxAttrFmt(GX_VTXFMT1, GX_VA_CLR0, GX_CLR_RGBA, GX_RGBA8, 0);

    // Array Pointers and Strides
    GXSetArray(GX_VA_CLR0, ColorArray, 4 * sizeof(u8));

    // Disable auto texcoord scale because they are set by display lists.
    GXSetTexCoordScaleManually(GX_TEXCOORD0, GX_TRUE, 0, 0);
    GXSetTexCoordScaleManually(GX_TEXCOORD1, GX_TRUE, 0, 0);
    GXSetTexCoordScaleManually(GX_TEXCOORD2, GX_TRUE, 0, 0);
    
    // Pixel format and background color
    GXSetPixelFmt(GX_PF_RGBA6_Z24, GX_ZC_LINEAR);
    GXSetCopyClear(BG_COLOR, MAX_Z);
    GXSetBlendMode(GX_BM_NONE, GX_BL_ONE, GX_BL_ZERO, GX_LO_SET);

    // Prepare effect display lists
    PrepareDL();


    // Default scene control parameter settings

    sc->effectNo = 0;

    // camera
    sc->cam.cfg   = DefaultCamera;

    // cube objects
    for ( i = 0 ; i < NUM_CUBES ; ++i )
    {
        sc->cube[i].pos  = CubeIniData[i*3];
        sc->cube[i].vel  = CubeIniData[i*3+1];
        sc->cube[i].axis = CubeIniData[i*3+2];
        sc->cube[i].deg  = 0.0F;
    }

    for ( i = 0 ; i < NUM_TORI ; ++i )
    {
        sc->torusAngle[i] = 0;
    }
}

/*---------------------------------------------------------------------------*
    Name:           DrawTick
    
    Description:    Draw the model by using given scene parameters 
                    
    Arguments:      sc : pointer to the structure of scene control parameters
    
    Returns:        none
 *---------------------------------------------------------------------------*/
static void DrawTick( MySceneCtrlObj* sc )
{
    EffectManager* ef = &Effects[sc->effectNo];

    // Set up camera / viewport for the 1st pass.
    SetCamera(&sc->cam);
    GXSetViewport(0.0F, 0.0F, ef->baseViewWd, ef->baseViewHt, 0.0F, 1.0F);
    GXSetScissor(0, 0, ef->baseViewWd, ef->baseViewHt);

    // Default matrix
    GXSetCurrentMtx(GX_PNMTX0);
    
    // Draw models
    DrawModels(sc);
    
    // Make sure every cubes are drawn
    GXDrawDone();
    
    // Copyout
    CopyTextureFromFB(ef);
    
    // Set up camera / viewport for the 2nd pass.
    SetScreenSpaceMode();
    GXSetViewport(0.0F, 0.0F, SCREEN_WD, SCREEN_HT, 0.0F, 1.0F);
    GXSetScissor(0, 0, SCREEN_WD, SCREEN_HT);

    // Draw effect by using a GD display list.
    SetTextures(ef);
    GXCallDisplayList(ef->dlPtr, ef->dlSize);

    DrawFullScrQuad();
}

/*---------------------------------------------------------------------------*
    Name:           AnimTick
    
    Description:    Changes scene parameters according to the pad status.
                    
    Arguments:      sc  : pointer to the structure of scene control parameters
    
    Returns:        none
 *---------------------------------------------------------------------------*/
#define VelReflect(cmp) \
    if ( sc->cube[i].pos.cmp < - BOUNDARY )                 \
        sc->cube[i].vel.cmp = fabsf(sc->cube[i].vel.cmp);   \
    if ( sc->cube[i].pos.cmp > BOUNDARY )                   \
        sc->cube[i].vel.cmp = - fabsf(sc->cube[i].vel.cmp);

static void AnimTick( MySceneCtrlObj* sc )
{
    u32  i;

    // Effect mode
    if ( DEMOPadGetButtonDown(0) & PAD_BUTTON_B )
    {
        sc->effectNo = ( sc->effectNo + 1 ) % NUM_EFFECTS;
    }

    if ( DEMOPadGetButtonDown(0) & PAD_BUTTON_X )
    {
        sc->effectNo = ( sc->effectNo + NUM_EFFECTS - 1 ) % NUM_EFFECTS;
    }

    if ( !(DEMOPadGetButton(0) & PAD_BUTTON_A) )
    {
        // Animate cubes
        for ( i = 0 ; i < NUM_CUBES ; ++i )
        {
            // rotation
            sc->cube[i].deg += 5;
            if ( sc->cube[i].deg > 720 )
            {
                sc->cube[i].deg -= 720;
            }
            
            // position
            VECAdd(&sc->cube[i].pos, &sc->cube[i].vel, &sc->cube[i].pos);
            
            // velocity
            VelReflect(x);
            VelReflect(y);
            VelReflect(z);
        }

        // Animate tori
        for ( i = 0 ; i < NUM_TORI ; ++i )
        {
            sc->torusAngle[i] += (i+1);
            if ( sc->torusAngle[i] > 360 )
            {
                sc->torusAngle[i] -= 360;
            }
        }
    }
}

/*---------------------------------------------------------------------------*
    Name:           DrawModels
    
    Description:    Draw models
                    
    Arguments:      sc  : pointer to the structure of scene control parameters
 
    Returns:        none
 *---------------------------------------------------------------------------*/
static void DrawModels( MySceneCtrlObj* sc )
{
    u32     i;
    Mtx     mt, mr, ms, mv, mvi;

    // enable lighting
    SetLight();

	GXSetNumChans(1);
    GXSetNumTexGens(0);
    GXSetTevOrder(GX_TEVSTAGE0, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR0A0);
    GXSetTevOp(GX_TEVSTAGE0, GX_PASSCLR);
    GXSetAlphaCompare(GX_ALWAYS, 0, GX_AOP_OR, GX_ALWAYS, 0);
	GXSetTevDirect(GX_TEVSTAGE0);
	GXSetTevDirect(GX_TEVSTAGE1);
	GXSetTevDirect(GX_TEVSTAGE2);
	GXSetTevDirect(GX_TEVSTAGE3);
	GXSetNumTevStages(1);
	GXSetNumIndStages(0);
    
    MTXScale(ms, 100.0F, 100.0F, 100.0F);
    MTXConcat(sc->cam.view, ms, mv);
    MTXInvXpose(mv, mvi);
    GXLoadPosMtxImm(mv, GX_PNMTX0);
    GXLoadNrmMtxImm(mvi, GX_PNMTX0);
    GXSetChanMatColor(GX_COLOR0A0, ColorArray[NUM_CUBES+NUM_TORI]);
    GXDrawSphere1(2);

    MTXScale(ms, 220.0F, 220.0F, 220.0F);
    MTXRotDeg(mr, 'Y', (s32)(sc->torusAngle[0]/2));
    MTXConcat(ms, mr, ms);
    MTXConcat(sc->cam.view, ms, mv);
    MTXInvXpose(mv, mvi);
    GXLoadPosMtxImm(mv, GX_PNMTX0);
    GXLoadNrmMtxImm(mvi, GX_PNMTX0);
    GXSetChanMatColor(GX_COLOR0A0, ColorArray[NUM_CUBES]);
    GXDrawTorus(0.24F, 8, 16);

    MTXScale(ms, 330.0F, 330.0F, 330.0F);
    MTXRotDeg(mr, 'X', (s32)(sc->torusAngle[1]/2));
    MTXConcat(ms, mr, ms);
    MTXConcat(sc->cam.view, ms, mv);
    MTXInvXpose(mv, mvi);
    GXLoadPosMtxImm(mv, GX_PNMTX0);
    GXLoadNrmMtxImm(mvi, GX_PNMTX0);
    GXSetChanMatColor(GX_COLOR0A0, ColorArray[NUM_CUBES+1]);
    GXDrawTorus(0.15F, 8, 24);

    MTXScale(ms, 440.0F, 440.0F, 440.0F);
    MTXRotDeg(mr, 'Y', (s32)(-sc->torusAngle[2]/2));
    MTXConcat(ms, mr, ms);
    MTXConcat(sc->cam.view, ms, mv);
    MTXInvXpose(mv, mvi);
    GXLoadPosMtxImm(mv, GX_PNMTX0);
    GXLoadNrmMtxImm(mvi, GX_PNMTX0);
    GXSetChanMatColor(GX_COLOR0A0, ColorArray[NUM_CUBES+2]);
    GXDrawTorus(0.10F, 8, 24);

    for ( i = 0 ; i < NUM_CUBES ; ++i )
    {
        MTXTrans(mt, sc->cube[i].pos.x, sc->cube[i].pos.y, sc->cube[i].pos.z);
        MTXConcat(sc->cam.view, mt, mv);
        MTXScale(ms, 100.0F, 100.0F, 100.0F);
        MTXConcat(mv, ms, mv);
        MTXRotAxisDeg(mr, &sc->cube[i].axis, sc->cube[i].deg);
        MTXConcat(mv, mr, mv);
        GXLoadPosMtxImm(mv, GX_PNMTX0);
        MTXInverse(mv, mvi); 
        MTXTranspose(mvi, mv);
        GXLoadNrmMtxImm(mv, GX_PNMTX0);
        
        GXSetChanMatColor(GX_COLOR0A0, ColorArray[i]);
        
        GXDrawCube();
    }
}

/*---------------------------------------------------------------------------*
    Name:           DrawFullScrQuad
    
    Description:    Draw a full-screen sized quad
                    
    Arguments:      none
 
    Returns:        none
 *---------------------------------------------------------------------------*/
static void DrawFullScrQuad( void )
{
    // Vertex descriptor
    GXClearVtxDesc();
    GXSetVtxDesc(GX_VA_POS, GX_DIRECT);
    GXSetVtxDesc(GX_VA_TEX0, GX_DIRECT);

    // Draw a large quad
    GXBegin(GX_QUADS, GX_VTXFMT1, 4);
        GXPosition3s16(        0,         0, 0);
        GXTexCoord2s16(0x0000, 0x0000);
        GXPosition3s16(SCREEN_WD,         0, 0);
        GXTexCoord2s16(0x0100, 0x0000);
        GXPosition3s16(SCREEN_WD, SCREEN_HT, 0);
        GXTexCoord2s16(0x0100, 0x0100);
        GXPosition3s16(        0, SCREEN_HT, 0);
        GXTexCoord2s16(0x0000, 0x0100);
    GXEnd();
}

/*---------------------------------------------------------------------------*
    Name:           CopyTextureFromFB
    
    Description:    Copies texture from the frame buffer image
                    
    Arguments:      em : a pointer to EffectManager structure
 
    Returns:        none
 *---------------------------------------------------------------------------*/
static void CopyTextureFromFB( EffectManager* em )
{
    GXBool  filterMode;

    if ( em->baseViewWd == em->copyTexWd &&
         em->baseViewHt == em->copyTexHt )
    {
        filterMode = GX_FALSE;
    }
    else
    {
        ASSERT( (em->baseViewWd == em->copyTexWd * 2) &&
                (em->baseViewHt == em->copyTexHt * 2) );

        filterMode = GX_TRUE;
    }

    // Copy image
    GXSetTexCopySrc(0, 0, em->baseViewWd, em->baseViewHt);
    GXSetTexCopyDst(em->copyTexWd, em->copyTexHt, GX_TF_RGB5A3, filterMode);
    GXCopyTex(TexBuffer, GX_TRUE);
}

/*---------------------------------------------------------------------------*
    Name:           SetTextures
    
    Description:    Set up textures
                    
    Arguments:      em : a pointer to EffectManager structure
     
    Returns:        none
 *---------------------------------------------------------------------------*/
static void SetTextures( EffectManager* em )
{
    GXTexObj    txb, tx0, tx1, tx2a, tx2b, tx3;
    GXTlutObj   tl0;

    // Base texture (copied from the EFB.)
    GXInitTexObj(&txb, TexBuffer, em->copyTexWd, em->copyTexHt,
                 GX_TF_RGB5A3, GX_CLAMP, GX_CLAMP, GX_FALSE);
    GXInitTexObjFilter(&txb, GX_NEAR, GX_NEAR);
    GXLoadTexObj(&txb, GX_TEXMAP0);

    // Effect texture 0
    GXInitTlutObj(&tl0, EffectTex0TLUT, FXTEX0_TL_FMT, FXTEX0_TL_N);
    GXLoadTlut(&tl0, GX_TLUT0);
    GXInitTexObjCI(&tx0, EffectTexture0, FXTEX0_WD, FXTEX0_HT,
                   FXTEX0_FMT, GX_REPEAT, GX_REPEAT, GX_FALSE, GX_TLUT0);
    GXInitTexObjFilter(&tx0, GX_NEAR, GX_NEAR);
    GXLoadTexObj(&tx0, GX_TEXMAP1);

    // Effect texture 1
    GXInitTexObj(&tx1, EffectTexture1, FXTEX1_WD, FXTEX1_HT,
                 FXTEX1_FMT, GX_CLAMP, GX_CLAMP, GX_FALSE);
    GXInitTexObjFilter(&tx1, GX_NEAR, GX_NEAR);
    GXLoadTexObj(&tx1, GX_TEXMAP2);

    // Effect texture 2-A
    GXInitTexObj(&tx2a, EffectTexture2A, FXTEX2A_WD, FXTEX2A_HT,
                 FXTEX2A_FMT, GX_MIRROR, GX_MIRROR, GX_FALSE);
    GXInitTexObjFilter(&tx2a, GX_NEAR, GX_NEAR);
    GXLoadTexObj(&tx2a, GX_TEXMAP3);

    // Effect texture 2-B
    GXInitTexObj(&tx2b, EffectTexture2B, FXTEX2B_WD, FXTEX2B_HT,
                 FXTEX2B_FMT, GX_REPEAT, GX_REPEAT, GX_FALSE);
    GXInitTexObjFilter(&tx2b, GX_NEAR, GX_NEAR);
    GXLoadTexObj(&tx2b, GX_TEXMAP4);

    // Effect texture 3
    GXInitTexObj(&tx3, EffectTexture3, FXTEX3_WD, FXTEX3_HT,
                 FXTEX3_FMT, GX_REPEAT, GX_REPEAT, GX_FALSE);
    GXInitTexObjFilter(&tx3, GX_LINEAR, GX_LINEAR);
    GXLoadTexObj(&tx3, GX_TEXMAP5);
}

/*---------------------------------------------------------------------------*
    Name:           SetCamera
    
    Description:    set view matrix and load projection matrix into hardware
                    
    Arguments:      cam : pointer to the MyCameraObj structure
                    
    Returns:        none
 *---------------------------------------------------------------------------*/
static void SetCamera( MyCameraObj* cam )
{
    MTXLookAt(
        cam->view,
        &cam->cfg.location,
        &cam->cfg.up,
        &cam->cfg.target );

    MTXFrustum(
        cam->proj,
        cam->cfg.top,
        - (cam->cfg.top),
        cam->cfg.left,
        - (cam->cfg.left),
        cam->cfg.znear,
        cam->cfg.zfar );
    GXSetProjection(cam->proj, GX_PERSPECTIVE);
}

/*---------------------------------------------------------------------------*
    Name:           SetScreenSpaceMode
    
    Description:    set projection matrix up to screen coordinate system
                    
    Arguments:      none
                    
    Returns:        none
 *---------------------------------------------------------------------------*/
static void SetScreenSpaceMode( void )
{
    Mtx44  proj;
    Mtx    mv;

    MTXOrtho( proj, 0.0F, SCREEN_HT, 0.0F, SCREEN_WD, 0.0F, - MAX_Z );
    GXSetProjection( proj, GX_ORTHOGRAPHIC );
    MTXIdentity(mv);
    GXLoadPosMtxImm(mv, GX_PNMTX0);
}

/*---------------------------------------------------------------------------*
    Name:           SetLight
    
    Description:    Sets light objects and color channels
                    
    Arguments:      none
    
    Returns:        none
 *---------------------------------------------------------------------------*/
static void SetLight( void )
{
    GXLightObj  lobj;

    // set up light position and color
    GXInitLightPos(&lobj, 0.0F, 5000.0F, 10000.0F); // almost parallel
    GXInitLightColor(&lobj, LIGHT_COLOR);
    GXLoadLightObjImm(&lobj, GX_LIGHT0);

    // channel setting
    GXSetChanCtrl(
        GX_COLOR0A0,
        GX_ENABLE,        // enable channel
        GX_SRC_REG,       // amb source
        GX_SRC_REG,       // mat source
        GX_LIGHT0,        // light mask
        GX_DF_CLAMP,      // diffuse function
        GX_AF_NONE);      // attenuation function
    // channel ambient
    GXSetChanAmbColor(GX_COLOR0A0, REG_AMBIENT);
}

/*---------------------------------------------------------------------------*
    Name:           PrepareDL
    
    Description:    [create mode] Allocate memory for display list and call
                    the function CreateModelDL to create the display list.
                    [load mode] Load GDL file from the disc.

    Arguments:      none
    
    Returns:        none
 *---------------------------------------------------------------------------*/
static void PrepareDL ( void )
{
#ifdef  LOAD_DL_FROM_FILE
    //---------------------------------------------------------
    // File mode : Read pre-generated GDL file from file
    //---------------------------------------------------------
    s32         err;
    GDGList*    dlArray;
    GDGList*    plArray;
    u32         nDls, nPls;
    u32         i;
    
    err = GDReadDLFile("gddemo/gdIndTex.gdl", &nDls, &nPls, &dlArray, &plArray);

    ASSERTMSG( err == 0, "GD file read error.\n" );
    
    // Check number of lists.
    ASSERTMSG( ( nDls == NUM_DLS && nPls == NUM_PLS ),
               "This data doesn't match requirement of this demo.\n" );

    // Obtain effect display lists.
    for ( i = 0 ; i < NUM_DLS ; ++i )
    {
        Effects[i].dlPtr  = dlArray[i].ptr;
        Effects[i].dlSize = dlArray[i].byteLength;
        // No patch list in this demo.
    }
    
#else
    //---------------------------------------------------------
    // Create mode : Create display list in this application
    //---------------------------------------------------------
    u32  i;
    
    // Allocate memory for DLs.
    for ( i = 0 ; i < NUM_EFFECTS ; ++i )
    {
        Effects[i].dlPtr = OSAlloc(EFFECTDL_SIZE_MAX);
        ASSERTMSG(Effects[i].dlPtr, "Memory allocation failed.\n");
    }
    
    Effects[0].dlSize = CreateEffectDL0(Effects[0].dlPtr);
    OSReport("DL0 Size = %d\n", Effects[0].dlSize);

    Effects[1].dlSize = CreateEffectDL1(Effects[1].dlPtr);
    OSReport("DL1 Size = %d\n", Effects[1].dlSize);

    Effects[2].dlSize = CreateEffectDL2(Effects[2].dlPtr);
    OSReport("DL2 Size = %d\n", Effects[2].dlSize);

    Effects[3].dlSize = CreateEffectDL3(Effects[3].dlPtr);
    OSReport("DL3 Size = %d\n", Effects[3].dlSize);

#endif
}

/*---------------------------------------------------------------------------*
    Name:           PrintIntro
    
    Description:    Prints the directions on how to use this demo.
                    
    Arguments:      none
    
    Returns:        none
 *---------------------------------------------------------------------------*/
static void PrintIntro( void )
{
    OSReport("\n\n");
    OSReport("********************************\n");
    OSReport("   GD Indirect Texture demo\n");
    OSReport("********************************\n");
    OSReport("START  : Quit.\n");
    OSReport("A      : Pause while pressed.\n");
    OSReport("B/Y    : Change effect mode.\n");
    OSReport("********************************\n");
}

/*============================================================================*/

/*---------------------------------------------------------------------------*
    Texture Data
 *---------------------------------------------------------------------------*/
// Effect Texture 0 (CI TEXTURE + TLUT)
u16 EffectTex0TLUT[FXTEX0_TL_N] ATTRIBUTE_ALIGN(32) =
{
    0x8080, 0x807F, 0x8081,
    0x8180, 0x817F, 0x8181,
    0x8080, 0x8080
};

u32 EffectTexture0[] ATTRIBUTE_ALIGN(32) =
{
    // Line 0-7
    0x00001111,
    0x00000111,
    0x00000001,
    0x00000000,
    0x00000000,
    0x00000000,
    0x00000000,
    0x00000000,
    
    0x11113333,
    0x11133333,
    0x13333333,
    0x33333333,
    0x33333333,
    0x33333333,
    0x33333333,
    0x33333333,

    // Line 8-15
    0x00000000,
    0x00000000,
    0x00000000,
    0x00000000,
    0x00000000,
    0x00000002,
    0x00000222,
    0x00002222,

    0x33333333,
    0x33333333,
    0x33333333,
    0x33333333,
    0x33333333,    
    0x23333333,    
    0x22233333,    
    0x22223333,    

    // Line 16-23
    0x11110000,
    0x11100000,
    0x10000000,
    0x00000000,
    0x00000000,
    0x00000000,
    0x00000000,
    0x00000000,
    
    0x00004444,
    0x00000444,
    0x00000004,
    0x00000000,    
    0x00000000,
    0x00000000,
    0x00000000,
    0x00000000,

    // Line 24-31
    0x00000000,
    0x00000000,
    0x00000000,
    0x00000000,
    0x00000000,
    0x20000000,
    0x22200000,
    0x22220000,

    0x00000000,
    0x00000000,
    0x00000000,
    0x00000000,
    0x00000000,
    0x00000005,
    0x00000555,
    0x00005555,
};

u8 EffectTexture1[] ATTRIBUTE_ALIGN(32) =
{
    // intensity level 0
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,

    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,

    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,

    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,

    // intensity level 1
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0xFF, 0xFF,
    0x00, 0x00, 0x00, 0x0F,
    0x00, 0x00, 0x00, 0x0F,
    0x00, 0x00, 0x00, 0x0F,
    0x00, 0x00, 0x00, 0x0F,
    0x00, 0x00, 0x00, 0x0F,
    0x00, 0x00, 0x00, 0x0F,

    0x00, 0x00, 0x00, 0x00,
    0xFF, 0xF0, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,

    0x00, 0x00, 0x00, 0x0F,
    0x00, 0x00, 0x00, 0x0F,
    0x00, 0x00, 0x00, 0x0F,
    0x00, 0x00, 0x00, 0x0F,
    0x00, 0x00, 0x00, 0x0F,
    0x00, 0x00, 0x00, 0x0F,
    0x00, 0x00, 0xFF, 0xFF,
    0x00, 0x00, 0x00, 0x00,

    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0xFF, 0xF0, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,

    // intensity level 2
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0xFF, 0xFF,
    0x00, 0x0F, 0x00, 0x00,
    0x00, 0xF0, 0x00, 0x00,
    0x0F, 0x00, 0x00, 0x00,
    0x0F, 0x00, 0x00, 0x00,
    0x0F, 0x00, 0x00, 0x00,
    0x0F, 0x00, 0x00, 0x00,

    0x00, 0x00, 0x00, 0x00,
    0xFF, 0xFF, 0x00, 0x00,
    0x00, 0x00, 0xF0, 0x00,
    0x00, 0x00, 0x0F, 0x00,
    0x00, 0x00, 0x00, 0xF0,
    0x00, 0x00, 0x00, 0xF0,
    0x00, 0x00, 0x00, 0xF0,
    0x00, 0x00, 0x00, 0xF0,

    0x0F, 0x00, 0x00, 0x00,
    0x0F, 0x00, 0x00, 0x00,
    0x0F, 0x00, 0x00, 0x00,
    0x0F, 0x00, 0x00, 0x00,
    0x00, 0xF0, 0x00, 0x00,
    0x00, 0x0F, 0x00, 0x00,
    0x00, 0x00, 0xFF, 0xFF,
    0x00, 0x00, 0x00, 0x00,

    0x00, 0x00, 0x00, 0xF0,
    0x00, 0x00, 0x00, 0xF0,
    0x00, 0x00, 0x00, 0xF0,
    0x00, 0x00, 0x00, 0xF0,
    0x00, 0x00, 0x0F, 0x00,
    0x00, 0x00, 0xF0, 0x00,
    0xFF, 0xFF, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,

    // intensity level 3
    0x00, 0x00, 0x00, 0x00,
    0x0F, 0xFF, 0xFF, 0xFF,
    0x0F, 0xF0, 0x00, 0x0F,
    0x00, 0x00, 0x00, 0x0F,
    0x00, 0x00, 0x00, 0x0F,
    0x00, 0x00, 0x00, 0x0F,
    0x00, 0x00, 0x00, 0x0F,
    0x00, 0x00, 0x00, 0x0F,

    0x00, 0x00, 0x00, 0x00,
    0xFF, 0xFF, 0xFF, 0xF0,
    0xF0, 0x00, 0x0F, 0xF0,
    0xF0, 0x00, 0x00, 0x00,
    0xF0, 0x00, 0x00, 0x00,
    0xF0, 0x00, 0x00, 0x00,
    0xF0, 0x00, 0x00, 0x00,
    0xF0, 0x00, 0x00, 0x00,

    0x00, 0x00, 0x00, 0x0F,
    0x00, 0x00, 0x00, 0x0F,
    0x00, 0x00, 0x00, 0x0F,
    0x00, 0x00, 0x00, 0x0F,
    0x00, 0x00, 0x00, 0x0F,
    0x00, 0x00, 0x00, 0x0F,
    0x00, 0x00, 0x00, 0x0F,
    0x00, 0x00, 0x00, 0x00,

    0xF0, 0x00, 0x00, 0x00,
    0xF0, 0x00, 0x00, 0x00,
    0xF0, 0x00, 0x00, 0x00,
    0xF0, 0x00, 0x00, 0x00,
    0xF0, 0x00, 0x00, 0x00,
    0xF0, 0x00, 0x00, 0x00,
    0xF0, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,

    // intensity level 4
    0x00, 0x00, 0x00, 0x00,
    0x0F, 0xFF, 0xFF, 0xFF,
    0x00, 0xFF, 0x00, 0x0F,
    0x00, 0xFF, 0x00, 0x00,
    0x00, 0xFF, 0x00, 0x00,
    0x00, 0xFF, 0x00, 0x00,
    0x00, 0xFF, 0x00, 0x00,
    0x00, 0xFF, 0x00, 0x00,

    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0xFF, 0xF0, 0x00, 0x00,
    0x00, 0xFF, 0xF0, 0x00,
    0x00, 0x00, 0xFF, 0x00,
    0x00, 0x00, 0xFF, 0x00,
    0x00, 0x00, 0xFF, 0x00,
    0x00, 0x00, 0xFF, 0x00,

    0x00, 0xFF, 0x00, 0x00,
    0x00, 0xFF, 0x00, 0x00,
    0x00, 0xFF, 0x00, 0x00,
    0x00, 0xFF, 0x00, 0x00,
    0x00, 0xFF, 0x00, 0x00,
    0x00, 0xFF, 0x00, 0x0F,
    0x0F, 0xFF, 0xFF, 0xFF,
    0x00, 0x00, 0x00, 0x00,

    0x00, 0x00, 0xFF, 0x00,
    0x00, 0x00, 0xFF, 0x00,
    0x00, 0x00, 0xFF, 0x00,
    0x00, 0x00, 0xFF, 0x00,
    0x00, 0xFF, 0xF0, 0x00,
    0xFF, 0xF0, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,

    // intensity level 5
    0x00, 0x00, 0x00, 0x00,
    0x0F, 0xFF, 0xFF, 0xFF,
    0x0F, 0xFF, 0xFF, 0xFF,
    0x0F, 0xF0, 0x00, 0x00,
    0x0F, 0xF0, 0x00, 0x00,
    0x0F, 0xF0, 0x00, 0x00,
    0x0F, 0xF0, 0x00, 0x00,
    0x0F, 0xFF, 0xFF, 0xFF,

    0x00, 0x00, 0x00, 0x00,
    0xFF, 0xFF, 0xFF, 0xF0,
    0xFF, 0xFF, 0xFF, 0xF0,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0xFF, 0xFF, 0xFF, 0x00,

    0x0F, 0xFF, 0xFF, 0xFF,
    0x0F, 0xF0, 0x00, 0x00,
    0x0F, 0xF0, 0x00, 0x00,
    0x0F, 0xF0, 0x00, 0x00,
    0x0F, 0xF0, 0x00, 0x00,
    0x0F, 0xFF, 0xFF, 0xFF,
    0x0F, 0xFF, 0xFF, 0xFF,
    0x00, 0x00, 0x00, 0x00,

    0xFF, 0xFF, 0xFF, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0xFF, 0xFF, 0xFF, 0xF0,
    0xFF, 0xFF, 0xFF, 0xF0,
    0x00, 0x00, 0x00, 0x00,

    // intensity level 6
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x0F,
    0x00, 0x00, 0x00, 0x0F,
    0x00, 0x00, 0x00, 0x0F,
    0x00, 0x00, 0x00, 0xFF,
    0x0F, 0xFF, 0xFF, 0xFF,
    0x00, 0xFF, 0xFF, 0xFF,

    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0xF0, 0x00, 0x00, 0x00,
    0xF0, 0x00, 0x00, 0x00,
    0xF0, 0x00, 0x00, 0x00,
    0xFF, 0x00, 0x00, 0x00,
    0xFF, 0xFF, 0xFF, 0xF0,
    0xFF, 0xFF, 0xFF, 0x00,

    0x00, 0x0F, 0xFF, 0xFF,
    0x00, 0x00, 0x00, 0xFF,
    0x00, 0x00, 0x0F, 0xFF,
    0x00, 0x00, 0xFF, 0xFF,
    0x00, 0x0F, 0xFF, 0xF0,
    0x00, 0x0F, 0xF0, 0x00,
    0x00, 0xFF, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,

    0xFF, 0xFF, 0xF0, 0x00,
    0xFF, 0x00, 0x00, 0x00,
    0xFF, 0xF0, 0x00, 0x00,
    0xFF, 0xFF, 0x00, 0x00,
    0x0F, 0xFF, 0xF0, 0x00,
    0x00, 0x0F, 0xF0, 0x00,
    0x00, 0x00, 0xFF, 0x00,
    0x00, 0x00, 0x00, 0x00,

    // intensity level 7
    0x00, 0x00, 0x00, 0x00,
    0x0F, 0xFF, 0xFF, 0x00,
    0x0F, 0xFF, 0xFF, 0x00,
    0x0F, 0xFF, 0xFF, 0xF0,
    0x0F, 0xFF, 0xFF, 0xF0,
    0x0F, 0xFF, 0xFF, 0xFF,
    0x0F, 0xFF, 0xF0, 0xFF,
    0x0F, 0xFF, 0xF0, 0xFF,

    0x00, 0x00, 0x00, 0x00,
    0x00, 0x0F, 0xFF, 0xF0,
    0x00, 0x0F, 0xFF, 0xF0,
    0x00, 0x0F, 0xFF, 0xF0,
    0x00, 0x0F, 0xFF, 0xF0,
    0xF0, 0x0F, 0xFF, 0xF0,
    0xF0, 0x0F, 0xFF, 0xF0,
    0xF0, 0x0F, 0xFF, 0xF0,

    0x0F, 0xFF, 0xF0, 0x0F,
    0x0F, 0xFF, 0xF0, 0x0F,
    0x0F, 0xFF, 0xF0, 0x0F,
    0x0F, 0xFF, 0xF0, 0x00,
    0x0F, 0xFF, 0xF0, 0x00,
    0x0F, 0xFF, 0xF0, 0x00,
    0x0F, 0xFF, 0xF0, 0x00,
    0x00, 0x00, 0x00, 0x00,

    0xFF, 0x0F, 0xFF, 0xF0,
    0xFF, 0x0F, 0xFF, 0xF0,
    0xFF, 0xFF, 0xFF, 0xF0,
    0x0F, 0xFF, 0xFF, 0xF0,
    0x0F, 0xFF, 0xFF, 0xF0,
    0x00, 0xFF, 0xFF, 0xF0,
    0x00, 0xFF, 0xFF, 0xF0,
    0x00, 0x00, 0x00, 0x00,

};

u32 EffectTexture2A[] ATTRIBUTE_ALIGN(32) =
{
    0xF0000000,
    0xF0000000,
    0xF0000000,
    0xF0000000,
    0xE0000000,
    0x80000000,
    0x80000000,
    0x80000000,

    0x60000000,
    0x50000000,
    0x40000000,
    0x30000000,
    0x20000000,
    0x10000000,
    0x00000000,
    0x00000000
};

u32 EffectTexture2B[] ATTRIBUTE_ALIGN(32) =
{
    // Tile pattern 0
    0x9AACCEFF,
    0x89AACCEF,
    0x889AACCE,
    0x7889AACC,
    0x77889AAC,
    0x677889AA,
    0x1677889A,
    0x11677889,

    0x9AACCEFF,
    0x89AACCEF,
    0x889AACCE,
    0x7889AACC,
    0x77889AAC,
    0x677889AA,
    0x1677889A,
    0x11677889,

    0x9AACCEFF,
    0x89AACCEF,
    0x889AACCE,
    0x7889AACC,
    0x77889AAC,
    0x677889AA,
    0x1677889A,
    0x11677889,

    0x9AACCEFF,
    0x89AACCEF,
    0x889AACCE,
    0x7889AACC,
    0x77889AAC,
    0x677889AA,
    0x1677889A,
    0x11677889,

    // Tile pattern 1
    0xFFFF268A,
    0xFFF468AF,
    0xFF268AFF,
    0xF468AFFF,
    0xF268AFFF,
    0xF468AFFF,
    0xF268AFFF,
    0xF468AFFF,

    0xFFFF1579,
    0xFFF3579F,
    0xFF1579FF,
    0xF3579FFF,
    0xF1579FFF,
    0xF3579FFF,
    0xF1579FFF,
    0xF3579FFF,

    0xFF268AFF,
    0xFFF468AF,
    0xFFFF268A,
    0xAFFFF468,
    0xAFFFF268,
    0xAFFFF468,
    0xAFFFF268,
    0xAFFFF468,

    0xFF1579FF,
    0xFFF3579F,
    0xFFFF1579,
    0x9FFFF357,
    0x9FFFF157,
    0x9FFFF357,
    0x9FFFF157,
    0x9FFFF357,

    // Tile pattern 2
    0xFFFFFFFF,
    0xFFFFFFFF,
    0xEEEEEEEE,
    0x88888888,
    0xFFFFFFFF,
    0xFFFFFFFF,
    0xCCCCCCCC,
    0x11111111,

    0xFFFFFFFF,
    0xFFFFFFFF,
    0xEEEEEEEE,
    0x88888888,
    0xFFFFFFFF,
    0xFFFFFFFF,
    0xCCCCCCCC,
    0x11111111,

    0xFFFFFFFF,
    0xFFFFFFFF,
    0xEEEEEEEE,
    0x88888888,
    0xFFFFFFFF,
    0xFFFFFFFF,
    0xCCCCCCCC,
    0x33333333,

    0xFFFFFFFF,
    0xFFFFFFFF,
    0xEEEEEEEE,
    0x88888888,
    0xFFFFFFFF,
    0xFFFFFFFF,
    0xCCCCCCCC,
    0x33333333,

    // Tile pattern 3
    0xFFFFFFFF,
    0xFFFFFFFF,
    0xFFFFFFFF,
    0xFFFFFFFF,
    0xFFFFFFFF,
    0xFFFFFFFF,
    0xFFFFFFFF,
    0xFFFFFFFF,

    0xFEDBDEFF,
    0xECA8ACEF,
    0xDA555ADF,
    0xB85158BF,
    0xDA555ADF,
    0xECA8ACEF,
    0xFEDBDEFF,
    0xFFFFFFFF,

    0xFEDBDEFF,
    0xECA8ACEF,
    0xDA555ADF,
    0xB85158BF,
    0xDA555ADF,
    0xECA8ACEF,
    0xFEDBDEFF,
    0xFFFFFFFF,

    0xFFFFFFFF,
    0xFFFFFFFF,
    0xFFFFFFFF,
    0xFFFFFFFF,
    0xFFFFFFFF,
    0xFFFFFFFF,
    0xFFFFFFFF,
    0xFFFFFFFF,

};

u32 EffectTexture3[] ATTRIBUTE_ALIGN(32) =
{
    0x8C8C8C8C,
    0xC0000000,
    0x80000000,
    0xC0000000,
    0x80000000,
    0xC0000000,
    0x80000000,
    0xC0000000,
    
    0x8C8C8C8C,
    0x00000000,
    0x00000000,
    0x00000000,
    0x00000000,
    0x00000000,
    0x00000000,
    0x00000000,

    0x80000000,
    0xC0000000,
    0x80000000,
    0xC0000000,
    0x80000000,
    0xC0000000,
    0x80000000,
    0xC0000000,

    0x00000000,
    0x00000000,
    0x00000000,
    0x00000000,
    0x00000000,
    0x00000000,
    0x00000000,
    0x00000000,
};

/*============================================================================*/


