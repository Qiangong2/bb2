/*---------------------------------------------------------------------------*
  Project:  Dolphin
  File:     gd-indtex-create.c

  Copyright 2003 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: gd-indtex-create.c,v $
  Revision 1.1.1.1  2004/06/09 17:36:08  paulm
  GC demos from Nintendo SDK

    
    3     03/03/05 14:29 Hirose
    More updates.
    
    2     03/02/28 17:42 Hirose
    Updates.
    
    1     03/02/21 12:12 Hirose
    Initial ver.
    
  $NoKeywords: $
 *---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*
   gd-indtex
     Displaylist demo with indirect texture commands
     [Displaylist creation function for both off-line/runtime]
 *---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*
   Header files
 *---------------------------------------------------------------------------*/
#include "gd-indtex.h"

/*---------------------------------------------------------------------------*
   Forward references
 *---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*
   Data
 *---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*
   Functions
 *---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*
    Name:           CreateEffectDL0
    
    Description:    Creates the display list used by the program.
                    GD library is used for creating the display list.
                    
    Arguments:      dlPtr : pointer to memory for the display list
    
    Returns:        actual size of the display list
 *---------------------------------------------------------------------------*/
// Texcoord transform matrix data
static f32 EffDL0TgMtx[2][4] =
{
    { (f32)(SCREEN_WD/FXTEX0_WD), 0.00F, 0.00F, 0.00F },
    { 0.00F, (f32)(SCREEN_HT/FXTEX0_HT), 0.00F, 0.00F }
};

// Indirect matrix data
static f32 EffDL0IndMtx[2][3] =
{
    { 0.50F, 0.00F, 0.00F },
    { 0.00F, 0.50F, 0.00F }
};

/*---------------------------------------------------------------------------*/
u32 CreateEffectDL0 ( void* dlPtr )
{
    GDLObj  dlObj;
    u32     size;

    GDInitGDLObj(&dlObj, dlPtr, EFFECTDL_SIZE_MAX);
    GDSetCurrent(&dlObj);

    // General mode (TexGens,Chans,TevStages,IndStages,CullMode)
    GDSetGenMode2(2, 0, 1, 1, GX_CULL_BACK);
    
    // Texture coordinate
    GDSetTexCoordGen(GX_TEXCOORD0, GX_TG_MTX2x4, GX_TG_TEX0, GX_FALSE, GX_PTIDENTITY);
    GDSetTexCoordGen(GX_TEXCOORD1, GX_TG_MTX2x4, GX_TG_TEX0, GX_FALSE, GX_PTIDENTITY);
    GDSetTexCoordScale(GX_TEXCOORD0, COPYTEX0_WD, COPYTEX0_HT);
    GDSetTexCoordScale(GX_TEXCOORD1, FXTEX0_WD,  FXTEX0_HT);

    GDLoadTexMtxImm(EffDL0TgMtx, GX_TEXMTX0, GX_MTX2x4);
    GDSetCurrentMtx(GX_PNMTX0, GX_IDENTITY, GX_TEXMTX0,  GX_IDENTITY, GX_IDENTITY,
                               GX_IDENTITY, GX_IDENTITY, GX_IDENTITY, GX_IDENTITY);

    // Tev
    GDSetTevOp(GX_TEVSTAGE0, GX_REPLACE);
    GDSetTevOrder(GX_TEVSTAGE0,
                  GX_TEXCOORD0,     GX_TEXMAP0,     GX_COLOR_NULL,  // STAGE0
                  GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL); // STAGE1

    // Indirect texture
    GDSetIndTexOrder(GX_TEXCOORD1,     GX_TEXMAP1,      // STAGE0
                     GX_TEXCOORD_NULL, GX_TEXMAP_NULL,  // STAGE1
                     GX_TEXCOORD_NULL, GX_TEXMAP_NULL,  // STAGE2
                     GX_TEXCOORD_NULL, GX_TEXMAP_NULL); // STAGE3
    GDSetIndTexMtx(GX_ITM_0, EffDL0IndMtx, 1);
    GDSetIndTexCoordScale(GX_INDTEXSTAGE0, GX_ITS_1, GX_ITS_1,  // STAGE0
                                           GX_ITS_1, GX_ITS_1); // STAGE1
    /*
    GDSetTevIndirect(GX_TEVSTAGE0, GX_INDTEXSTAGE0, GX_ITF_8, GX_ITB_ST, GX_ITM_0,
                     GX_ITW_OFF, GX_ITW_OFF, GX_FALSE, GX_FALSE, GX_ITBA_OFF);
    */
    GDSetTevIndWarp(GX_TEVSTAGE0, GX_INDTEXSTAGE0, GX_TRUE, GX_FALSE, GX_ITM_0);

    // close the DL (padding + flush)
    GDPadCurr32();
    GDFlushCurrToMem();
    
    // get actual DL size
    size = GDGetCurrOffset();

    // release GD object
    GDSetCurrent(NULL);
    
    return size;
}

/*---------------------------------------------------------------------------*
    Name:           CreateEffectDL1
    
    Description:    Creates the display list used by the program.
                    GD library is used for creating the display list.
                    
    Arguments:      dlPtr : pointer to memory for the display list
    
    Returns:        actual size of the display list
 *---------------------------------------------------------------------------*/

// Indirect matrix data
static f32 EffDL1IndMtx[2][3] =
{
    { 0.00F, 0.00F, 0.00F },
    { 0.00F, 0.00F, 0.50F }
};

/*---------------------------------------------------------------------------*/
u32 CreateEffectDL1( void* dlPtr )
{
    GDLObj  dlObj;
    u32     size;

    GDInitGDLObj(&dlObj, dlPtr, EFFECTDL_SIZE_MAX);
    GDSetCurrent(&dlObj);

    // General mode (TexGens,Chans,TevStages,IndStages,CullMode)
    GDSetGenMode2(1, 0, 1, 1, GX_CULL_BACK);

    // Texture coordinate
    GDSetTexCoordGen(GX_TEXCOORD0, GX_TG_MTX2x4, GX_TG_TEX0, GX_FALSE, GX_PTIDENTITY);
    GDSetTexCoordScale(GX_TEXCOORD0, SCREEN_WD, SCREEN_HT);
    GDSetCurrentMtx(GX_PNMTX0, GX_IDENTITY, GX_IDENTITY, GX_IDENTITY, GX_IDENTITY,
                               GX_IDENTITY, GX_IDENTITY, GX_IDENTITY, GX_IDENTITY);

    // Tev
    GDSetTevOp(GX_TEVSTAGE0, GX_REPLACE);
    GDSetTevOrder(GX_TEVSTAGE0,
                  GX_TEXCOORD0,     GX_TEXMAP2,     GX_COLOR_NULL,  // STAGE0
                  GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL); // STAGE1

    // Indirect texture
    GDSetIndTexOrder(GX_TEXCOORD0,     GX_TEXMAP0,      // STAGE0
                     GX_TEXCOORD_NULL, GX_TEXMAP_NULL,  // STAGE1
                     GX_TEXCOORD_NULL, GX_TEXMAP_NULL,  // STAGE2
                     GX_TEXCOORD_NULL, GX_TEXMAP_NULL); // STAGE3
    GDSetIndTexCoordScale(GX_INDTEXSTAGE0, GX_ITS_16, GX_ITS_16,    // STAGE0
                                           GX_ITS_1,  GX_ITS_1);    // STAGE1

    GDSetIndTexMtx(GX_ITM_1, EffDL1IndMtx, 5);
    GDSetTevIndirect(GX_TEVSTAGE0, GX_INDTEXSTAGE0, GX_ITF_3, GX_ITB_NONE, GX_ITM_1,
                     GX_ITW_16, GX_ITW_16, GX_FALSE, GX_FALSE, GX_ITBA_OFF);

    // close the DL (padding + flush)
    GDPadCurr32();
    GDFlushCurrToMem();
    
    // get actual DL size
    size = GDGetCurrOffset();

    // release GD object
    GDSetCurrent(NULL);
    
    return size;
}

/*---------------------------------------------------------------------------*
    Name:           CreateEffectDL2
    
    Description:    Creates the display list used by the program.
                    GD library is used for creating the display list.
                    
    Arguments:      dlPtr : pointer to memory for the display list
    
    Returns:        actual size of the display list
 *---------------------------------------------------------------------------*/
// Texcoord transform matrix data
static f32 EffDL2TgMtx0[2][4] =
{
    { 1.00F, 0.00F, 0.00F, 1.0F/(f32)SCREEN_WD },
    { 0.00F, 1.00F, 0.00F, 1.0F/(f32)SCREEN_HT }
};

// Indirect matrix data
static f32 EffDL2IndMtx0[2][3] =
{
    { 0.00F, 0.00F, 0.00F },
    { 0.50F, 0.00F, 0.00F }
};

static f32 EffDL2IndMtx1[2][3] =
{
    { 0.50F, 0.00F, 0.00F },
    { 0.00F, 0.50F, 0.50F }
};

static f32 EffDL2IndMtx2[2][3] =
{
    { -0.50F,  0.00F,  0.00F },
    {  0.00F, -0.50F, -0.50F }
};

/*---------------------------------------------------------------------------*/
u32 CreateEffectDL2( void* dlPtr )
{
    GDLObj  dlObj;
    u32     size;

    GDInitGDLObj(&dlObj, dlPtr, EFFECTDL_SIZE_MAX);
    GDSetCurrent(&dlObj);

    // General mode (TexGens,Chans,TevStages,IndStages,CullMode)
    GDSetGenMode2(3, 0, 4, 2, GX_CULL_BACK);

    // Texture coordinate
    GDSetTexCoordGen(GX_TEXCOORD0, GX_TG_MTX2x4, GX_TG_TEX0, GX_FALSE, GX_PTIDENTITY);
    GDSetTexCoordGen(GX_TEXCOORD1, GX_TG_MTX2x4, GX_TG_TEX0, GX_FALSE, GX_PTIDENTITY);
    GDSetTexCoordGen(GX_TEXCOORD2, GX_TG_MTX2x4, GX_TG_TEX0, GX_FALSE, GX_PTIDENTITY);
    GDSetTexCoordScale(GX_TEXCOORD0, COPYTEX2_WD, COPYTEX2_HT);
    GDSetTexCoordScale(GX_TEXCOORD1, COPYTEX2_WD, COPYTEX2_HT);
    GDSetTexCoordScale(GX_TEXCOORD2, SCREEN_WD, SCREEN_HT);

    GDLoadTexMtxImm(EffDL2TgMtx0, GX_TEXMTX0, GX_MTX2x4);
    GDSetCurrentMtx(GX_PNMTX0, GX_IDENTITY, GX_TEXMTX0,  GX_IDENTITY, GX_IDENTITY,
                               GX_IDENTITY, GX_IDENTITY, GX_IDENTITY, GX_IDENTITY);

    // Tev
    GDSetTevColorCalc(GX_TEVSTAGE0, GX_CC_ZERO, GX_CC_ZERO, GX_CC_ZERO, GX_CC_TEXC,
                      GX_TEV_ADD, GX_TB_ZERO, GX_CS_SCALE_1, GX_ENABLE, GX_TEVREG2);
    GDSetTevColorCalc(GX_TEVSTAGE1, GX_CC_C2, GX_CC_TEXC, GX_CC_ONE, GX_CC_ZERO,
                      GX_TEV_COMP_GR16_GT, GX_TB_ZERO, GX_CS_SCALE_1, GX_ENABLE, GX_TEVREG1);
    GDSetTevColorCalc(GX_TEVSTAGE2, GX_CC_ONE, GX_CC_C2, GX_CC_C1, GX_CC_ZERO,
                      GX_TEV_ADD, GX_TB_ZERO, GX_CS_SCALE_1, GX_ENABLE, GX_TEVPREV);
    GDSetTevColorCalc(GX_TEVSTAGE3, GX_CC_ZERO, GX_CC_CPREV, GX_CC_TEXC, GX_CC_ZERO,
                      GX_TEV_ADD, GX_TB_ZERO, GX_CS_SCALE_1, GX_ENABLE, GX_TEVPREV);
    GDSetTevOrder(GX_TEVSTAGE0,
                  GX_TEXCOORD0, GX_TEXMAP0, GX_COLOR_NULL,                              // STAGE0
                  GX_TEXCOORD2, GX_TEXMAP4, GX_COLOR_NULL);                             // STAGE1
    GDSetTevOrder(GX_TEVSTAGE2,
                  GX_TEXCOORD0, (GXTexMapID)(GX_TEXMAP3|GX_TEX_DISABLE), GX_COLOR_NULL, // STAGE2
                  GX_TEXCOORD0, GX_TEXMAP3, GX_COLOR_NULL);                             // STAGE3

    // Indirect texture
    GDSetIndTexOrder(GX_TEXCOORD0,     GX_TEXMAP0,      // STAGE0
                     GX_TEXCOORD1,     GX_TEXMAP0,      // STAGE1
                     GX_TEXCOORD_NULL, GX_TEXMAP_NULL,  // STAGE2
                     GX_TEXCOORD_NULL, GX_TEXMAP_NULL); // STAGE3
    GDSetIndTexCoordScale(GX_INDTEXSTAGE0, GX_ITS_1, GX_ITS_1,      // STAGE0
                                           GX_ITS_1, GX_ITS_1);     // STAGE1
    GDSetIndTexMtx(GX_ITM_0, EffDL2IndMtx0, 5);
    GDSetIndTexMtx(GX_ITM_1, EffDL2IndMtx1, 2);
    GDSetIndTexMtx(GX_ITM_2, EffDL2IndMtx2, 2);

    GDSetTevDirect(GX_TEVSTAGE0);
    GDSetTevIndirect(GX_TEVSTAGE1, GX_INDTEXSTAGE0, GX_ITF_3, GX_ITB_NONE, GX_ITM_0,
                     GX_ITW_16, GX_ITW_16, GX_FALSE, GX_FALSE, GX_ITBA_OFF);
    GDSetTevIndirect(GX_TEVSTAGE2, GX_INDTEXSTAGE0, GX_ITF_3, GX_ITB_NONE, GX_ITM_1,
                     GX_ITW_0, GX_ITW_0, GX_FALSE, GX_FALSE, GX_ITBA_OFF);
    GDSetTevIndirect(GX_TEVSTAGE3, GX_INDTEXSTAGE1, GX_ITF_3, GX_ITB_NONE, GX_ITM_2,
                     GX_ITW_0, GX_ITW_0, GX_TRUE,  GX_FALSE, GX_ITBA_OFF);

    // close the DL (padding + flush)
    GDPadCurr32();
    GDFlushCurrToMem();
    
    // get actual DL size
    size = GDGetCurrOffset();

    // release GD object
    GDSetCurrent(NULL);
    
    return size;
}

/*---------------------------------------------------------------------------*
    Name:           CreateEffectDL3
    
    Description:    Creates the display list used by the program.
                    GD library is used for creating the display list.
                    
    Arguments:      dlPtr : pointer to memory for the display list
    
    Returns:        actual size of the display list
 *---------------------------------------------------------------------------*/
// Indirect matrix data
static f32 EffDL3IndMtx0[2][3] =
{
    { 0.00F, -0.80F, -0.20F },
    { 0.00F, -0.20F, -0.80F }
};

static f32 EffDL3IndMtx1[2][3] =
{
    { 0.00F,  0.60F,  0.15F },
    { 0.00F,  0.15F,  0.60F }
};

// Color data
static GXColor EffDL3RegColor = { 0x60, 0xFF, 0x40, 0xFF };

/*---------------------------------------------------------------------------*/
u32 CreateEffectDL3( void* dlPtr )
{
    GDLObj  dlObj;
    u32     size;

    GDInitGDLObj(&dlObj, dlPtr, EFFECTDL_SIZE_MAX);
    GDSetCurrent(&dlObj);

    // General mode (TexGens,Chans,TevStages,IndStages,CullMode)
    GDSetGenMode2(1, 0, 2, 1, GX_CULL_BACK);

    // Texture coordinate
    GDSetTexCoordGen(GX_TEXCOORD0, GX_TG_MTX2x4, GX_TG_TEX0, GX_FALSE, GX_PTIDENTITY);
    GDSetTexCoordScale(GX_TEXCOORD0, SCREEN_WD, SCREEN_HT);
    GDSetCurrentMtx(GX_PNMTX0, GX_IDENTITY, GX_IDENTITY, GX_IDENTITY, GX_IDENTITY,
                               GX_IDENTITY, GX_IDENTITY, GX_IDENTITY, GX_IDENTITY);

    // Tev
    GDSetTevColorCalc(GX_TEVSTAGE0, GX_CC_ZERO, GX_CC_C2, GX_CC_TEXC, GX_CC_ZERO,
                      GX_TEV_ADD, GX_TB_ZERO, GX_CS_SCALE_1, GX_ENABLE, GX_TEVPREV);
    GDSetTevColorCalc(GX_TEVSTAGE1, GX_CC_ZERO, GX_CC_TEXC, GX_CC_ONE, GX_CC_CPREV,
                      GX_TEV_ADD, GX_TB_ZERO, GX_CS_SCALE_1, GX_ENABLE, GX_TEVPREV);
    GDSetTevAlphaCalcAndSwap(GX_TEVSTAGE1,
                             GX_CA_ZERO, GX_CA_ZERO, GX_CA_ZERO, GX_CA_RASA,
                             GX_TEV_ADD, GX_TB_ZERO, GX_CS_SCALE_1, GX_ENABLE, GX_TEVPREV,
                             GX_TEV_SWAP0, GX_TEV_SWAP0);
    GDSetTevColor(GX_TEVREG2, EffDL3RegColor);
    GDSetAlphaCompare(GX_GREATER, 0, GX_AOP_AND, GX_GREATER, 0);

    GDSetTevOrder(GX_TEVSTAGE0,
                  GX_TEXCOORD0, GX_TEXMAP5, GX_COLOR_NULL,      // STAGE0
                  GX_TEXCOORD0, GX_TEXMAP5, GX_ALPHA_BUMP);     // STAGE1

    // Indirect texture
    GDSetIndTexOrder(GX_TEXCOORD0,     GX_TEXMAP0,      // STAGE0
                     GX_TEXCOORD_NULL, GX_TEXMAP_NULL,  // STAGE1
                     GX_TEXCOORD_NULL, GX_TEXMAP_NULL,  // STAGE2
                     GX_TEXCOORD_NULL, GX_TEXMAP_NULL); // STAGE3
    GDSetIndTexCoordScale(GX_INDTEXSTAGE0, GX_ITS_2, GX_ITS_2,      // STAGE0
                                           GX_ITS_1, GX_ITS_1);     // STAGE1
    GDSetIndTexMtx(GX_ITM_0, EffDL3IndMtx0, -4);
    GDSetIndTexMtx(GX_ITM_1, EffDL3IndMtx1, -4);

    GDSetTevIndWarp(GX_TEVSTAGE0, GX_INDTEXSTAGE0, GX_FALSE, GX_FALSE, GX_ITM_0);
    GDSetTevIndirect(GX_TEVSTAGE1, GX_INDTEXSTAGE0, GX_ITF_8, GX_ITB_NONE, GX_ITM_1,
                     GX_ITW_OFF, GX_ITW_OFF, GX_FALSE, GX_FALSE, GX_ITBA_S);

    // close the DL (padding + flush)
    GDPadCurr32();
    GDFlushCurrToMem();
    
    // get actual DL size
    size = GDGetCurrOffset();

    // release GD object
    GDSetCurrent(NULL);
    
    return size;
}

/*============================================================================*/
