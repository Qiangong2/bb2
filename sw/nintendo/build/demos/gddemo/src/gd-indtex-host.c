/*---------------------------------------------------------------------------*
  Project:  Dolphin GD demo
  File:     gd-indtex-host.c

  Copyright 2003 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: gd-indtex-host.c,v $
  Revision 1.1.1.1  2004/06/09 17:36:08  paulm
  GC demos from Nintendo SDK

    
    2     03/03/05 14:29 Hirose
    More updates.
    
    1     03/02/21 12:12 Hirose
    Initial ver.
    
  $NoKeywords: $
 *---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*
   gd-indtex
     Displaylist demo with indirect texture commands
     [Host program for creating off-line display list file]
 *---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*
   Header file includes
 *---------------------------------------------------------------------------*/
#include <windows.h>

#include <dolphin/gd.h>

#include <assert.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#include "gd-indtex.h"

/*---------------------------------------------------------------------------*
   Macro definitions
 *---------------------------------------------------------------------------*/
#define RoundUp32B(x)  (((u32)(x) + 31) & ~31)


/*---------------------------------------------------------------------------*
   Forward references
 *---------------------------------------------------------------------------*/
// defined in gd-win32-ui.c
extern void GDReport (char* msg, ...);

void        main     ( void );

/*---------------------------------------------------------------------------*
   Application name
 *---------------------------------------------------------------------------*/
char* AppName = "gd-indtex-host";

/*---------------------------------------------------------------------------*
   Global variables
 *---------------------------------------------------------------------------*/
static u32(*CreateEffectDLFunc[NUM_EFFECTS])() =
{
    CreateEffectDL0, CreateEffectDL1, CreateEffectDL2, CreateEffectDL3
};

/*---------------------------------------------------------------------------*
   Application main loop
 *---------------------------------------------------------------------------*/
// Function WinMain() is defined in gd-win32-ui.c
void main ( void )
{
    GDGList dlists[NUM_DLS];    // keeps track of all display lists
	u32     size;
	void*   buffer;
	u8*     ptr;
	u32     i;
	u32     index;

    // Allocate entire work memory first
    size   = EFFECTDL_SIZE_MAX * NUM_DLS + 32;
    buffer = malloc(size);
	assert(buffer);
	
	index  = 0;

    // Make sure 32Bytes alignment
    ptr = (u8*)RoundUp32B(buffer);


    // Create multitexturing shader display lists
    for ( i = 0 ; i < NUM_DLS ; ++i )
    {
       	dlists[index].ptr = (void*)ptr;
       	ptr += EFFECTDL_SIZE_MAX;
        
        dlists[index].byteLength = (*CreateEffectDLFunc[i])(dlists[index].ptr);
        
        index++;
    }

    GDWriteDLFile("gdIndTex.gdl", NUM_DLS, NUM_PLS, dlists, NULL /* No PLs*/);

	free(buffer);

	GDReport("Created file \"gdIndTex.gdl\" successfully.");
}

/*============================================================================*/
