/*---------------------------------------------------------------------------*
  Project:  Dolphin
  File:     gd-indtex.h

  Copyright 2003 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: gd-indtex.h,v $
  Revision 1.1.1.1  2004/06/09 17:36:08  paulm
  GC demos from Nintendo SDK

    
    3     03/03/05 14:29 Hirose
    More updates.
    
    2     03/02/28 17:42 Hirose
    Updates.
    
    1     03/02/21 12:13 Hirose
    Initial ver.
    
  $NoKeywords: $
 *---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*
   gd-indtex
     Displaylist demo with lighting commands
     [Header file]
 *---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*
   Header files
 *---------------------------------------------------------------------------*/
#include <dolphin/gd.h>

/*---------------------------------------------------------------------------*
   Macro definitions
 *---------------------------------------------------------------------------*/

#define SCREEN_WD       640
#define SCREEN_HT       480


#define NUM_EFFECTS     4

// Effect 0
#define COPYTEX0_WD     (SCREEN_WD/16)
#define COPYTEX0_HT     (SCREEN_HT/16)
#define BASEVIEW0_WD    (COPYTEX0_WD*2)
#define BASEVIEW0_HT    (COPYTEX0_HT*2)
#define FXTEX0_WD       16
#define FXTEX0_HT       32
#define FXTEX0_FMT      GX_TF_C4
#define FXTEX0_TL_N     8
#define FXTEX0_TL_FMT   GX_TL_IA8

// Effect 1
#define COPYTEX1_WD     (SCREEN_WD/16)
#define COPYTEX1_HT     (SCREEN_HT/16)
#define BASEVIEW1_WD    COPYTEX1_WD
#define BASEVIEW1_HT    COPYTEX1_HT
#define FXTEX1_WD       16
#define FXTEX1_HT       128
#define FXTEX1_FMT      GX_TF_I4

// Effect 2
#define COPYTEX2_WD     (SCREEN_WD/2)
#define COPYTEX2_HT     (SCREEN_HT/2)
#define BASEVIEW2_WD    COPYTEX2_WD
#define BASEVIEW2_HT    COPYTEX2_HT
#define FXTEX2A_WD      8
#define FXTEX2A_HT      16
#define FXTEX2A_FMT     GX_TF_I4
#define FXTEX2B_WD      16
#define FXTEX2B_HT      64
#define FXTEX2B_FMT     GX_TF_I4

// Effect 3
#define COPYTEX3_WD     (SCREEN_WD/2)
#define COPYTEX3_HT     (SCREEN_HT/2)
#define BASEVIEW3_WD    (COPYTEX3_WD*2)
#define BASEVIEW3_HT    (COPYTEX3_HT*2)
#define FXTEX3_WD       16
#define FXTEX3_HT       16
#define FXTEX3_FMT      GX_TF_I4


// Display list properties
#define EFFECTDL_SIZE_MAX   1024
#define NUM_DLS             NUM_EFFECTS
#define NUM_PLS             0               // No Patch List

/*---------------------------------------------------------------------------*
   External function definitions
 *---------------------------------------------------------------------------*/
extern u32  CreateEffectDL0 ( void* dlPtr );
extern u32  CreateEffectDL1 ( void* dlPtr );
extern u32  CreateEffectDL2 ( void* dlPtr );
extern u32  CreateEffectDL3 ( void* dlPtr );


/*============================================================================*/
