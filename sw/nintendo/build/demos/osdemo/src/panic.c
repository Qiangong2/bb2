/*---------------------------------------------------------------------------*
  Project: panic demo
  File:    panic.c

  Copyright 2002 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: panic.c,v $
  Revision 1.1.1.1  2004/06/09 17:36:09  paulm
  GC demos from Nintendo SDK

    
    4     02/09/05 9:50 Hashida
    Deleted #include <alloca.h> since SN ProDG doesn't have the header
    file.
    
    3     8/26/02 13:57 Shiki
    Clean up.

    2     8/22/02 10:49 Shiki
    Added another const keyword to the OSPanic() function prototype.

    1     8/20/02 21:55 Shiki
    Initial check-in.
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#include <stdarg.h>
#include <stdio.h>
#include <dolphin/os.h>

// Define program-specific OSPanic() function for the assertion message.
void OSPanic(const char* file, int line, const char* msg, ...)
{
    va_list marker;
    u32     i;
    u32*    p;
    int     len;
    GXColor bg = {   0,   0, 255, 0 };
    GXColor fg = { 255, 255, 255, 0 };
    char    message[4096];

    va_start(marker, msg);
    len = vsprintf(message, msg, marker);
    va_end(marker);
    len += sprintf(message + len, " in \"%s\" on line %d.\n", file, line);

    // Stack crawl
    len += sprintf(message + len, "\nAddress:    Back Chain LR Save\n");
    for (i = 0, p = (u32*) OSGetStackPointer(); // get current sp
         p && (u32) p != 0xffffffff && i++ < 16;
         p = (u32*) *p)                         // get caller sp
    {
        len += sprintf(message + len, "0x%08x: 0x%08x 0x%08x\n", p, p[0], p[1]);
    }

    OSFatal(fg, bg, message);
    // NOT REACHED HERE
}

void main(void)
{
    OSReport("This program illustrates how to capture OS assertion message\n"
             "at the user-level.\n");
    OSReport("Note: This program simply crashes with NDEBUG build.\n");

    // Do something really bad to raise an assertion failure. (DEBUG build only)
    OSFree(NULL);
    OSReport("Done.\n");
}
