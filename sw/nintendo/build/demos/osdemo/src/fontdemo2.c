/*---------------------------------------------------------------------------*
  Project:  Dolphin OS Overview - OSLoatFont(), OSGetFontTexel()
  File:     fontdemo2.c

  Copyright 2002 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: fontdemo2.c,v $
  Revision 1.1.1.1  2004/06/09 17:36:09  paulm
  GC demos from Nintendo SDK

    
    2     3/07/02 18:37 Shiki
    Fixed to use correct size of fontData.

    1     1/09/02 15:26 Shiki
    Initial check-in.
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#include <stdlib.h>
#include <string.h>
#include <dolphin.h>

void main(int argc, char* argv[])
{
    s32           code;
    OSFontHeader* fontData;
    u32           image[24 / 8 * 24];   // 24 x 24
    char          buffer[3];
    char*         string;
    int           i, j;
    s32           width;

    if (argc != 2)
    {
        OSHalt("usage: fontdemo2 char_code\n");
    }

    code = strtol(argv[1], NULL, 0);
    if (code < 256)
    {
        buffer[0] = (char) code;
        buffer[1] = '\0';
    }
    else
    {
        buffer[0] = (char) ((code >> 8) & 0xff);
        buffer[1] = (char) (code & 0xff);
        buffer[2] = '\0';
    }
    string = buffer;

    if (OSGetFontEncode() == OS_FONT_ENCODE_SJIS)
    {
        fontData = OSAllocFromArenaLo(OS_FONT_DATA_SIZE_SJIS, 32);
    }
    else
    {
        fontData = OSAllocFromArenaLo(OS_FONT_DATA_SIZE_ANSI, 32);
    }
    OSLoadFont(fontData, OSGetArenaLo());

    // Clear image buffer by zero since OSGetFontTexel() copies out
    // font texels using logical OR.
    memset(image, 0x00, sizeof(image));

    OSReport("code %x\n\n", code);

    // OSGetFontTexel() only works with the compressed font data
    // read by OSLoadFont().
    string = OSGetFontTexel(string, image, 0, 24 / 4, &width);
    for (i = 0; i < 24; i++)
    {
        j = 24 * (i / 8) + (i % 8);
        OSReport("%08x%08x%08x\n",
                 image[j],
                 image[j + 32 / sizeof(u32)],
                 image[j + 64 / sizeof(u32)]);
    }

    OSReport("\nwidth %d\n", width);
}
