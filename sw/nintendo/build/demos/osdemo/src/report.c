/*---------------------------------------------------------------------------*
  Project:  Dolphin OS screen print demo
  File:     report.c

  Copyright 2002 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: report.c,v $
  Revision 1.1.1.1  2004/06/09 17:36:09  paulm
  GC demos from Nintendo SDK

    
    2     8/26/02 13:57 Shiki
    Clean up.

    1     8/22/02 20:37 Shiki
    Initial check-in.
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#include <string.h>
#include <demo.h>
#include <dolphin.h>

#define LINES           24

GXColor Black = {   0,   0,   0, 0 };
GXColor Blue  = {   0,   0, 192, 0 };
GXColor Red   = { 255,   0,   0, 0 };
GXColor Green = {   0, 224,   0, 0 };
GXColor White = { 255, 255, 255, 0 };

static char ReportBuffer[4096];

static void DoReport(void)
{
    GXRenderModeObj* rmp;

    rmp = DEMOGetRenderModeObj();
    DEMOInitCaption(DM_FT_XLU, (s16) rmp->fbWidth, (s16) rmp->efbHeight);
    DEMOSetROMFontSize(16, -1);
    DEMORFPuts(25, 25, 0, ReportBuffer);
}

// Define program-specific OSReport() function.
void OSReport(const char* msg, ...)
{
    BOOL    enabled;
    va_list marker;
    u32     len;
    int     c;
    char*   p;
    char*   q;

    enabled = OSDisableInterrupts();

    len = strlen(ReportBuffer);
    va_start(marker, msg);
    len += vsprintf(ReportBuffer + len, msg, marker);
    va_end(marker);

    c = 0;
    q = ReportBuffer;
    for (p = ReportBuffer; p < ReportBuffer + len; ++p)
    {
        if (*p == '\n')
        {
            ++c;
            if (LINES <= c)
            {
                q = strchr(q, '\n');
            }
        }
    }
    if (LINES <= c)
    {
        memmove(ReportBuffer, q + 1, len - (q - ReportBuffer));
    }

    OSRestoreInterrupts(enabled);
}

void main(void)
{
    OSTime         t1, t2;
    OSCalendarTime ct;

    OSReport("This program illustrates how to implement program-specific\n"
             "OSReport() function at the user-level.\n");

    DEMOInit(NULL);
    DEMOInitROMFont();

    // Clear EFB
    GXSetCopyClear(Blue, 0x00ffffff);
    GXCopyDisp(DEMOGetCurrentBuffer(), GX_TRUE);

    t1 = OSGetTime();
    for (;;)
    {
        DEMOBeforeRender();
        DoReport();
        DEMODoneRender();

        t2 = OSGetTime();
        if (OSSecondsToTicks(1) < t2 - t1)
        {
            t1 = t2;
            OSTicksToCalendarTime(t1, &ct);
            OSReport("%02d:%02d %02d\n", ct.hour, ct.min, ct.sec);
        }
    }

    OSHalt("End of demo");
}
