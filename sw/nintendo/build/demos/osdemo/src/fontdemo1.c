/*---------------------------------------------------------------------------*
  Project:  Dolphin OS Overview - OSInitFont(), OSGetFontTexture()
  File:     fontdemo1.c

  Copyright 2002 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: fontdemo1.c,v $
  Revision 1.1.1.1  2004/06/09 17:36:09  paulm
  GC demos from Nintendo SDK

    
    3     1/11/02 9:05 Shiki
    Minor change

    2     1/09/02 16:14 Shiki
    Minor fix.

    1     1/09/02 15:26 Shiki
    Initial check-in.
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#include <stdlib.h>
#include <string.h>
#include <demo.h>

const GXColor Bkcolor = { 0, 0, 192, 0 };

static OSFontHeader* FontData;

static void DrawCell(int x, int y, int xChar, int yChar)
{
    s16 posLeft   = (s16) x;
    s16 posRight  = (s16) (posLeft + FontData->cellWidth);
    s16 posTop    = (s16) y;
    s16 posBottom = (s16) (posTop  + FontData->cellHeight);

    s16 texLeft   = (s16) xChar;
    s16 texRight  = (s16) (xChar + FontData->cellWidth);
    s16 texTop    = (s16) yChar;
    s16 texBottom = (s16) (yChar + FontData->cellHeight);

    GXBegin(GX_QUADS, GX_VTXFMT0, 4);
        GXPosition3s16(posLeft , posTop   , 0);
        GXTexCoord2s16(texLeft , texTop);

        GXPosition3s16(posRight, posTop   , 0);
        GXTexCoord2s16(texRight, texTop);

        GXPosition3s16(posRight, posBottom, 0);
        GXTexCoord2s16(texRight, texBottom);

        GXPosition3s16(posLeft , posBottom, 0);
        GXTexCoord2s16(texLeft , texBottom);
    GXEnd();
}

static void LoadSheet(void* image)
{
    Mtx      mtx;
    GXTexObj texObj;

    // Set up and load texture object
    GXInitTexObj(&texObj,                           // obj
                 image,                             // image_ptr
                 FontData->sheetWidth,              // wight
                 FontData->sheetHeight,             // height
                 (GXTexFmt) FontData->sheetFormat,  // format
                 GX_CLAMP,                          // wrap_s (don't care)
                 GX_CLAMP,                          // wrap_t (don't care)
                 GX_FALSE);                         // mipmap (don't care)

    GXInitTexObjLOD(&texObj,
                    GX_LINEAR,     // min_filt
                    GX_LINEAR,     // max_filt
                    0.0f,          // min_lod     (don't care)
                    0.0f,          // max_lod     (don't care)
                    0.0f,          // lod_bias    (don't care)
                    GX_DISABLE,    // bias_clamp  (don't care)
                    GX_FALSE,      // do_edge_lod (don't care)
                    GX_ANISO_1);   // max_aniso   (don't care)

    GXLoadTexObj(&texObj, GX_TEXMAP0);
    MTXScale(mtx, 1.0f / FontData->sheetWidth,
                  1.0f / FontData->sheetHeight,
                  1.0f);
    GXLoadTexMtxImm(mtx, GX_TEXMTX0, GX_MTX2x4);
    GXSetNumTexGens(1);
    GXSetTexCoordGen(GX_TEXCOORD0, GX_TG_MTX2x4, GX_TG_TEX0, GX_TEXMTX0);
}

static s32 DrawChar(int x, int y, u16 code)
{
    char  buffer[3];
    s32   width;
    void* image;
    s32   xChar;
    s32   yChar;

    if (code == 0)
    {
        return 0;
    }

    if (code < 256)
    {
        buffer[0] = (char) code;
        buffer[1] = '\0';
    }
    else
    {
        buffer[0] = (char) (code >> 8);
        buffer[1] = (char) (code & 0xff);
        buffer[2] = '\0';
    }

    OSGetFontTexture(buffer, &image, &xChar, &yChar, &width);
    LoadSheet(image);
    DrawCell(x, y, xChar, yChar);

    return width;
}

static s32 DrawString(int x, int y, char* string)
{
    s32 cx = 0;

    while (*string)
    {
        void* image;
        s32   xChar;
        s32   yChar;
        s32   width;

        string = OSGetFontTexture(string, &image, &xChar, &yChar, &width);
        LoadSheet(image);
        DrawCell(x + cx, y, xChar, yChar);
        cx += width;
    }

    return cx;
}

static void PrintIntro(void)
{
    OSReport("\n\n");
    OSReport("*******************************************************\n");
    OSReport("fontdemo: IPL font list\n");
    OSReport("*******************************************************\n");
    OSReport("to quit hit the start button\n");

    if (OSGetFontEncode() == OS_FONT_ENCODE_SJIS)
    {
        OSReport("\n");
        OSReport("  Up   button: page up\n");
        OSReport("  Down button: page down\n");
    }

    OSReport("*******************************************************\n\n");
}

void main(void)
{
    Mtx44  projection;
    Mtx    modelview;
    GXRenderModeObj* rmp;
    u16    top = 0x00;
    u16    code;
    u32    count;
    s32    repeat;

    DEMOInit(NULL);

    PrintIntro();

    if (OSGetFontEncode() == OS_FONT_ENCODE_SJIS)
    {
        FontData = OSAlloc(OS_FONT_SIZE_SJIS);
    }
    else
    {
        FontData = OSAlloc(OS_FONT_SIZE_ANSI);
    }
    OSInitFont(FontData);

    // Clear EFB
    GXSetCopyClear(Bkcolor, 0x00ffffff);
    GXCopyDisp(DEMOGetCurrentBuffer(), GX_TRUE);

    // Set matrices up to screen coordinates system
    rmp = DEMOGetRenderModeObj();
    MTXOrtho(projection, 0.0f, (f32) rmp->efbHeight, 0.0f, (f32) rmp->fbWidth, 0.0f, -100.0f);
    GXSetProjection(projection, GX_ORTHOGRAPHIC);
    MTXIdentity(modelview);
    GXLoadPosMtxImm(modelview, GX_PNMTX0);
    GXSetCurrentMtx(GX_PNMTX0);

    // Set pixel processing mode
    GXSetZMode(GX_ENABLE, GX_ALWAYS, GX_ENABLE);

    // Set TEV parameters to "REPLACE COLOR"
    GXSetNumChans(0);
    GXSetNumTevStages(1);
    GXSetTevOp(GX_TEVSTAGE0, GX_REPLACE);
    GXSetTevOrder(GX_TEVSTAGE0, GX_TEXCOORD0, GX_TEXMAP0, GX_COLOR_NULL);

    // Translucent mode
    GXSetBlendMode(GX_BM_BLEND, GX_BL_ONE, GX_BL_ONE, GX_LO_CLEAR);

    // Set up vertex descriptors
    GXClearVtxDesc();
    GXSetVtxDesc(GX_VA_POS,  GX_DIRECT);
    GXSetVtxDesc(GX_VA_TEX0, GX_DIRECT);
    GXSetVtxAttrFmt(GX_VTXFMT0, GX_VA_POS,  GX_POS_XYZ, GX_S16, 0);
    GXSetVtxAttrFmt(GX_VTXFMT0, GX_VA_TEX0, GX_TEX_ST,  GX_S16, 0);

    while (!(DEMOPadGetButton(0) & PAD_BUTTON_START))
    {
        int x;
        int y;

        DEMOPadRead();
        repeat = (s32) (VIGetRetraceCount() - count);
        repeat = (repeat < 20) ? 0 : (repeat % 3 == 0);
        if (DEMOPadGetButtonDown(0) & PAD_BUTTON_DOWN)
        {
            count = VIGetRetraceCount();
            repeat = 1;
        }
        if ((DEMOPadGetButton(0) & PAD_BUTTON_DOWN) && repeat)
        {
            if (top < 0x10000 - 256)
            {
                top += 256;
            }
            if (256 <= top && top < 0x8100)
            {
                top = 0x8100;
            }
            else if (0x9800 < top)
            {
                top = 0x9800;
            }
        }
        if (DEMOPadGetButtonDown(0) & PAD_BUTTON_UP)
        {
            count = VIGetRetraceCount();
            repeat = 1;
        }
        if ((DEMOPadGetButton(0) & PAD_BUTTON_UP) && repeat)
        {
            if (0 < top)
            {
                top -= 256;
            }
            if (256 <= top && top < 0x8100)
            {
                top = 0;
            }
        }
        if (OSGetFontEncode() != OS_FONT_ENCODE_SJIS)
        {
            top = 0;
        }

        DEMOBeforeRender();

        code = top;
        for (y = 25;
             y < 25 + FontData->cellHeight * 16;
             y += FontData->cellHeight)
        {
            char addr[5];

            sprintf(addr, "%04x", code);
            DrawString(50, y, addr);

            for (x = 150;
                 x < 150 + FontData->cellHeight * 16;
                 x += FontData->cellWidth)
            {
                DrawChar(x, y, code);
                ++code;
            }
        }

        DEMODoneRender();
    }

    OSHalt("End of demo");
}
