/*---------------------------------------------------------------------------*
  Project:  vmminimal
  File:     main.c
 
  Copyright 2002 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: main.c,v $
  Revision 1.1.1.1  2004/04/20 05:29:07  paulm
  Virtual Memory library from Nintendo

 * 
 * 2     11/08/02 3:12p Stevera
 * 
 * 1     11/01/02 5:39p Stevera
  
  $NoKeywords: $

 *---------------------------------------------------------------------------*/

#include <demo.h>
#include <dolphin/vm.h>


void main()
{	
	u32 answer;
	s32 success;
	u32 swapSize = 0x200000;	//2MB of main memory swap space
	u32 ARAMSize = 0x800000;	//8MB of backing store
	
	//Initialize virtual memory
	VMInit( swapSize,				//size of main memory swap space
            ARGetBaseAddress(),		//base address of ARAM backing store
            ARAMSize );				//size of ARAM backing store


	//Initialize everything else
	DEMOInit(NULL);
	
	
	//Allocate memory in the virtual address space
	success = VMAlloc( 0x7E000000, ARAMSize );


	if( success )
	{
		//Write to virtual address 0x7E000000
		*(u32*)0x7E000000 = 42;


		//Read from virtual address 0x7E000000
		answer = *(u32*)0x7E000000;
		
		
		//Output answer
		OSReport( "\nThe answer to the Universe is %d.\n", answer );
	}

	while(1)
	{
		//spin
	}
	
}



