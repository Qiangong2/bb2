/*---------------------------------------------------------------------------*
  Project:  Dolphin DVD fatal error demo
  File:     dvdfatal.c

  Copyright 2002 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: dvdfatal.c,v $
  Revision 1.1.1.1  2004/06/09 17:36:08  paulm
  GC demos from Nintendo SDK

    
    1     8/20/02 21:52 Shiki
    Initial check-in.

  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#include <dolphin.h>

// The name of the file we are going to read.
const char* Filename = "texts/test1.txt";

void main(void)
{
    DVDFileInfo fileInfo;
    u32         fileSize;

    VIInit();
    DVDInit();

    OSReport("Activate the disc fatal error to see the default disc fatal screen.\n");

    DVDSetAutoFatalMessaging(TRUE);

    for (;;)
    {
        // We MUST open the file before accessing the file
        if (FALSE == DVDOpen(Filename, &fileInfo))
        {
            OSHalt("Cannot open file");
        }

        // Get the size of the file
        fileSize = DVDGetLength(&fileInfo);

        // read the entire file here
        if (0 > DVDRead(&fileInfo, OSGetArenaLo(), (s32)OSRoundUp32B(fileSize), 0))
        {
            OSHalt("Error occurred when reading file");
        }

        // Close the file
        DVDClose(&fileInfo);

        VIWaitForRetrace();
    }
    // NOT REACHED HERE
}
