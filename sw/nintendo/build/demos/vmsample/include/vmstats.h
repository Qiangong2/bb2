/*---------------------------------------------------------------------------*
  Project:  vmsample
  File:     vmstats.h
 
  Copyright 2002 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: vmstats.h,v $
  Revision 1.1.1.1  2004/04/20 05:29:07  paulm
  Virtual Memory library from Nintendo

 * 
 * 1     11/01/02 5:02p Stevera
  
  $NoKeywords: $

 *---------------------------------------------------------------------------*/

#ifndef __VMSTATS_H__
#define __VMSTATS_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <dolphin/types.h>


/*---------------------------------------------------------------------------*
    function prototypes
 *---------------------------------------------------------------------------*/

void VMInitStats( void );
void VMDumpLog( void );
void VMClearLog( void );
void VMLogPageMiss( u32 exactVirtualAddress, 
                    u32 physicalAddress, 
                    u32 pageNumber,
                    u32 pageMissLatency,
                    BOOL pageSwappedOut );




#ifdef __cplusplus
}
#endif

#endif // __VMSTATS_H__ 
