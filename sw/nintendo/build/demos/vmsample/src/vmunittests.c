/*---------------------------------------------------------------------------*
  Project:  vmsample
  File:     vmunittests.c
 
  Copyright 2002 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: vmunittests.c,v $
  Revision 1.1.1.1  2004/04/20 05:29:07  paulm
  Virtual Memory library from Nintendo

 * 
 * 1     11/01/02 5:02p Stevera
  
  $NoKeywords: $

 *---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*
  Includes
 *---------------------------------------------------------------------------*/
#include "vmunittests.h"
#include "vmstats.h"
#include <dolphin/vm.h>
#include <dolphin/vmbase.h>
#include <demo.h>



/*---------------------------------------------------------------------------*
  Local definitions & declarations
 *---------------------------------------------------------------------------*/



/*---------------------------------------------------------------------------*
  Prototypes of Private Functions
 *---------------------------------------------------------------------------*/
void __VMUnitTest1( void );
void __VMUnitTest2( void );
void __VMUnitTest3( void );
void __VMUnitTest4( void );
void __VMUnitTest5( void );
void __VMUnitTest6( void );
void __VMUnitTest7( void );
BOOL __VMWriteAndReadU32Test( u32 address, u32 value );



/*---------------------------------------------------------------------------*
  Private functions
 *---------------------------------------------------------------------------*/

void VMExecuteUnitTests( void )
{
	__VMUnitTest1();
	VMDumpLog();
	VMClearLog();
	
	__VMUnitTest2();
	VMDumpLog();
	VMClearLog();
	
	__VMUnitTest3();
	VMDumpLog();
	VMClearLog();

	__VMUnitTest4();
	VMDumpLog();
	VMClearLog();
	
	__VMUnitTest5();
	VMDumpLog();
	VMClearLog();
	
	__VMUnitTest6();
	VMDumpLog();
	VMClearLog();
	
	__VMUnitTest7();
	VMDumpLog();
	VMClearLog();
	

	{	//Spin
		s64 time = OSTicksToMilliseconds( OSGetTime() );
		while( time + 1500 > OSTicksToMilliseconds( OSGetTime() ) ) {}
	}

	DEMOBeforeRender();
	DEMOPrintf( 70, 100, 0, "Passed all VM unit tests.\n" );
	DEMODoneRender();
	OSReport( "Passed all VM unit tests.\n" );

}


void __VMUnitTest1( void )
{
	u32 i;
	u32 value = 0;
	u32 subtest = 0;
	u32 vmStartAddr = 0;
	u32 testsize = VMGetARAMSize();
	
	for( subtest=0; subtest<2; subtest++ )
	{
		if( subtest == 0 ) vmStartAddr = 0x7E000000;
		else			   vmStartAddr = 0x7F000000;
	
		VMAlloc( vmStartAddr, testsize );
		
		value = 0;
		for( i=vmStartAddr; i<vmStartAddr+testsize; i=i+4 )
		{
			if( __VMWriteAndReadU32Test( i, value++ ) == FALSE ) {
				DEMOBeforeRender();
				DEMOPrintf( 70, 100, 0, "Failed VM Unit Test #1\n" );
				DEMODoneRender();
				
				OSReport( "Failed VM Unit Test #1: Simple write/read to each virtual address.\n" );
				PPCHalt();
			}
		}
		
		VMFree( vmStartAddr, testsize );
	}
		
	DEMOBeforeRender();
	DEMOPrintf( 70, 100, 0, "Passed VM Unit Test #1\n" );
	DEMODoneRender();
	
	OSReport( "Passed VM Unit Test #1\n" );

}

void __VMUnitTest2( void )
{
	u32 i;
	u32 value = 0;
	u32 subtest = 0;
	u32 vmStartAddr = 0;
	u32 testsize = VMGetARAMSize();
	
	for( subtest=0; subtest<2; subtest++ )
	{
		if( subtest == 0 ) vmStartAddr = 0x7E000000;
		else			   vmStartAddr = 0x7F000000;
	
		VMAlloc( vmStartAddr, testsize );
		
		value = 0;
		for( i=vmStartAddr; i<vmStartAddr+testsize; i=i+4 ) {
			*(u32*)i = value++;
		}
					
		value = 0;
		for( i=vmStartAddr; i<vmStartAddr+testsize; i=i+4 )
		{
			u32 storedValue = *(u32*)i;
			if( storedValue != value++ )
			{	//Failed
				DEMOBeforeRender();
				DEMOPrintf( 70, 100, 0, "Failed VM Unit Test #2\n" );
				DEMODoneRender();
				
				OSReport( "Failed VM Unit Test #2: Write to all virtual addresses, then read from all virtual addresses.\n" );
				PPCHalt();
			}
		}
		
		VMFree( vmStartAddr, testsize );
	}
	
	DEMOBeforeRender();
	DEMOPrintf( 70, 100, 0, "Passed VM Unit Test #2\n" );
	DEMODoneRender();

	OSReport( "Passed VM Unit Test #2\n" );

}


void __VMUnitTest3( void )
{
	u32 i;
	u32 value = 0;
	u32 subtest = 0;
	u32 vmStartAddr = 0;
	u32 testsize = VMGetARAMSize();


	for( subtest=0; subtest<2; subtest++ )
	{
		if( subtest == 0 ) vmStartAddr = 0x7E000000;
		else			   vmStartAddr = 0x7F000000;
	
		VMAlloc( vmStartAddr, testsize );
		
		value = 0;
		for( i=vmStartAddr; i<vmStartAddr+testsize; i=i+4 )
		{
			*(u32*)i = value++;
		}
		
		VMStoreAllPages();
		VMInvalidateAllPages();
		
		value = 0;
		for( i=vmStartAddr; i<vmStartAddr+testsize; i=i+4 )
		{
			if( *(u32*)i != value++ )
			{	//Failed
				DEMOBeforeRender();
				DEMOPrintf( 70, 100, 0, "Failed VM Unit Test #3\n" );
				DEMODoneRender();
				
				OSReport( "Failed VM Unit Test #3: Write to all virtual addresses, flush all pages, then read from all virtual addresses.\n" );
				PPCHalt();
			}
		}
		
		VMFree( vmStartAddr, testsize );
	}
	
	DEMOBeforeRender();
	DEMOPrintf( 70, 100, 0, "Passed VM Unit Test #3\n" );
	DEMODoneRender();

	OSReport( "Passed VM Unit Test #3\n" );

}


void __VMUnitTest4( void )
{
	u32 i;
	u32 value = 0;
	u32 subtest = 0;
	u32 vmStartAddr = 0;
	u32 testsize = VMGetARAMSize();
	
	for( subtest=0; subtest<2; subtest++ )
	{
		if( subtest == 0 ) vmStartAddr = 0x7E000000;
		else			   vmStartAddr = 0x7F000000;
	
		VMAlloc( vmStartAddr, testsize );
		
		value = 0;
		for( i=vmStartAddr; i<vmStartAddr+testsize; i=i+4 )
		{
			if( i%10000 == 0 )
			{	//write to address every 10000 bytes
				*(u32*)i = i;
			}
			else {
				value = *(u32*)i;
			}
		}
			
		value = 0;
		for( i=vmStartAddr; i<vmStartAddr+testsize; i=i+4 )
		{
			if( i%10000 == 0 )
			{
				if( *(u32*)i != i )
				{	//Failed
					DEMOBeforeRender();
					DEMOPrintf( 70, 100, 0, "Failed VM Unit Test #4\n" );
					DEMODoneRender();
				
					OSReport( "Failed VM Unit Test #4: Write to every 10000th byte of virtual address, then read.\n" );
					PPCHalt();
				}
			}
		}
		VMFree( vmStartAddr, testsize );
	}
	
	DEMOBeforeRender();
	DEMOPrintf( 70, 100, 0, "Passed VM Unit Test #4\n" );
	DEMODoneRender();

	OSReport( "Passed VM Unit Test #4\n" );

}


void __VMUnitTest5( void )
{
	u32 i;
	u32 value = 0;
	u32 vmStartAddr = 0x7E000000;
	u32 testsize = 0x100000;
	
	//Writes to pages without causing any to be paged in or out.
	VMAlloc( vmStartAddr, testsize );
	for( i=vmStartAddr; i<vmStartAddr+testsize; i=i+4 )
	{
		*(u32*)i = value++;
	}
	VMFree( vmStartAddr, testsize );

	DEMOBeforeRender();
	DEMOPrintf( 70, 100, 0, "Passed VM Unit Test #5\n" );
	DEMODoneRender();

	OSReport( "Passed VM Unit Test #5\n" );

}


void __VMUnitTest6( void )
{
	u32 i;
	u32 value = 0;
	u32 vmStartAddr = 0x7E000000;
	u32 testsize = 0xF00000;
	
	//Creates extensive thrashing at low MRAM sizes
	VMAlloc( vmStartAddr, testsize );
	for( i=vmStartAddr; i<vmStartAddr+testsize; i=i+4 )
	{
		*(u32*)i = value++;
	}
		
	for( i=vmStartAddr; i<vmStartAddr+testsize; i=i+4 )
	{		
		value = *(u32*)i;
		*(u32*)i = value + 5;
		if( OSGetTime()%50 == 0 )
		{
			*(u32*)(vmStartAddr+0x000000) = 1;
			*(u32*)(vmStartAddr+0x100000) = 1;
			*(u32*)(vmStartAddr+0x200000) = 1;
			*(u32*)(vmStartAddr+0x300000) = 1;
			*(u32*)(vmStartAddr+0x400000) = 1;
			*(u32*)(vmStartAddr+0x500000) = 1;
			*(u32*)(vmStartAddr+0x600000) = 1;
			*(u32*)(vmStartAddr+0x700000) = 1;
			*(u32*)(vmStartAddr+0x800000) = 1;
			*(u32*)(vmStartAddr+0x000000) = 1;
			*(u32*)(vmStartAddr+0x100000) = 1;
			*(u32*)(vmStartAddr+0x200000) = 1;
			*(u32*)(vmStartAddr+0x300000) = 1;
			*(u32*)(vmStartAddr+0x400000) = 1;
			*(u32*)(vmStartAddr+0x080000) = 1;
			*(u32*)(vmStartAddr+0x180000) = 1;
			*(u32*)(vmStartAddr+0x280000) = 1;
			*(u32*)(vmStartAddr+0x380000) = 1;
			*(u32*)(vmStartAddr+0x480000) = 1;
			*(u32*)(vmStartAddr+0x580000) = 1;
			*(u32*)(vmStartAddr+0x680000) = 1;
			*(u32*)(vmStartAddr+0x780000) = 1;
			*(u32*)(vmStartAddr+0x880000) = 1;
			*(u32*)(vmStartAddr+0x080000) = 1;
			*(u32*)(vmStartAddr+0x180000) = 1;
			*(u32*)(vmStartAddr+0x280000) = 1;
			*(u32*)(vmStartAddr+0x380000) = 1;
			*(u32*)(vmStartAddr+0x480000) = 1;
		}
	}
	
	VMFree( vmStartAddr, testsize );

	DEMOBeforeRender();
	DEMOPrintf( 70, 100, 0, "Passed VM Unit Test #6\n" );
	DEMODoneRender();

	OSReport( "Passed VM Unit Test #6\n" );

}


void __VMUnitTest7( void )
{
	u32 i;
	u32 value = 0;
	u32 subtest = 0;
	u32 vmStartAddr = 0;
	u32 testsize = VMGetARAMSize();
	
	for( subtest=0; subtest<2; subtest++ )
	{
		if( subtest == 0 ) vmStartAddr = 0x7E000000;
		else			   vmStartAddr = 0x7F000000;
	
		VMAlloc( vmStartAddr, testsize );
		
		value = 0;
		for( i=vmStartAddr; i<vmStartAddr+testsize; i=i+4 ) {
			*(u32*)i = value++;
		}
		
		value = 0;
		while( VMStoreOnePage() ) {
			value++;
		}
		if( value == 0 )
		{	//Failed
			DEMOBeforeRender();
			DEMOPrintf( 70, 100, 0, "Failed VM Unit Test #7\n" );
			DEMODoneRender();
			
			OSReport( "Failed VM Unit Test #7: VMStoreOnePage didn't write back all of the dirty pages.\n" );
			PPCHalt();
		}
					
		value = 0;
		for( i=vmStartAddr; i<vmStartAddr+testsize; i=i+4 )
		{
			u32 storedValue = *(u32*)i;
			if( storedValue != value++ )
			{	//Failed
				DEMOBeforeRender();
				DEMOPrintf( 70, 100, 0, "Failed VM Unit Test #7\n" );
				DEMODoneRender();
				
				OSReport( "Failed VM Unit Test #7: Write to all virtual addresses, then read from all virtual addresses.\n" );
				PPCHalt();
			}
		}
		
		VMFree( vmStartAddr, testsize );
	}
	
	DEMOBeforeRender();
	DEMOPrintf( 70, 100, 0, "Passed VM Unit Test #7\n" );
	DEMODoneRender();

	OSReport( "Passed VM Unit Test #7\n" );

}

BOOL __VMWriteAndReadU32Test( u32 address, u32 value )
{
	*(u32*)address = value;
	if( *(u32*)address == value ) {
		return( TRUE );
	}
	else {
		return( FALSE );
	}
}



