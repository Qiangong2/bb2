/*---------------------------------------------------------------------------*
  Project:  vmsample
  File:     main.c
 
  Copyright 2002 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: main.c,v $
  Revision 1.1.1.1  2004/04/20 05:29:07  paulm
  Virtual Memory library from Nintendo

 * 
 * 1     11/01/02 5:02p Stevera
  
  $NoKeywords: $

 *---------------------------------------------------------------------------*/

#include <demo.h>
#include <dolphin/vm.h>
#include "vmunittests.h"
#include "vmstats.h"


void main()
{	
	u32 mainMemorySwapSpaceSize = 2*1024*1024;
	u32 maxARAMSize = (16*1024*1024) - ARGetBaseAddress();
	
	VMInit( mainMemorySwapSpaceSize,	//size of main memory VM
            ARGetBaseAddress(),			//base address of ARAM VM
            maxARAMSize );				//size of ARAM VM

	DEMOInit(NULL);
	
	DEMOInitCaption( DM_FT_OPQ, 320, 224 );
	
	//Unit Testing
	VMInitStats();
	VMExecuteUnitTests();
	
	
	while(1)
	{
	
	}
	
}



