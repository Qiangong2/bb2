/*---------------------------------------------------------------------------*
  Project:  vmsample
  File:     vmstats.c
 
  Copyright 2002 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: vmstats.c,v $
  Revision 1.1.1.1  2004/04/20 05:29:07  paulm
  Virtual Memory library from Nintendo

 * 
 * 1     11/01/02 5:02p Stevera
  
  $NoKeywords: $

 *---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*
  Includes
 *---------------------------------------------------------------------------*/
#include "vmstats.h"
#include <dolphin/vm.h>
#include <stdio.h>
#include <dolphin.h>


/*---------------------------------------------------------------------------*
  Local definitions & declarations
 *---------------------------------------------------------------------------*/
typedef struct
{
	u32 exactVirtualAddress;
	u32 physicalAddress;
	u32 pageNumber;
	u32 pageMissLatency;
	BOOL pageSwappedOut;
} PageMissLogEntry;

#define VM_STATS_VIRTUAL_ADDRESS_NUM_PAGES 0x2000	//8K
#define PAGE_MISS_LOG_SIZE 100

PageMissLogEntry g_pageMissLog[PAGE_MISS_LOG_SIZE];
u32 g_vmPageMissLogCurIndex = 0;
u32 g_vmPagesReplaced = 0;
u32 g_vmPagesReplacedChanged = 0;
u32 g_vmPageReplacementLatencyCum = 0;



/*---------------------------------------------------------------------------*
  Prototypes of Private Functions
 *---------------------------------------------------------------------------*/



/*---------------------------------------------------------------------------*
  Private functions
 *---------------------------------------------------------------------------*/

void VMInitStats( void )
{
	VMSetLogStatsCallback( VMLogPageMiss );
	VMClearLog();
}


void VMLogPageMiss( u32 exactVirtualAddress, 
                    u32 physicalAddress, 
                    u32 pageNumber,
                    u32 pageMissLatency,
                    BOOL pageSwappedOut )
{
	//Log individual event
	g_pageMissLog[g_vmPageMissLogCurIndex].exactVirtualAddress = exactVirtualAddress;
	g_pageMissLog[g_vmPageMissLogCurIndex].physicalAddress = physicalAddress;
	g_pageMissLog[g_vmPageMissLogCurIndex].pageNumber = pageNumber;
	g_pageMissLog[g_vmPageMissLogCurIndex].pageMissLatency = pageMissLatency;
	g_pageMissLog[g_vmPageMissLogCurIndex].pageSwappedOut = pageSwappedOut;
	
	g_vmPageMissLogCurIndex++;
	if( g_vmPageMissLogCurIndex >= PAGE_MISS_LOG_SIZE ) {
		g_vmPageMissLogCurIndex = 0;
	}
	
	
	//Accumulate data
	g_vmPagesReplaced++;
	if( pageSwappedOut ) {
		g_vmPagesReplacedChanged++;
	}
	g_vmPageReplacementLatencyCum += pageMissLatency;
	

}


void VMClearLog( void )
{
	u32 i;
	for( i=0; i<PAGE_MISS_LOG_SIZE; i++ ) {
		g_pageMissLog[i].exactVirtualAddress = 0;
		g_pageMissLog[i].physicalAddress = 0;
		g_pageMissLog[i].pageNumber = 0;
	}
	g_vmPageMissLogCurIndex = 0;
	g_vmPagesReplaced = 0;
	g_vmPagesReplacedChanged = 0;
	g_vmPageReplacementLatencyCum = 0;
}


void VMDumpLog( void )
{
	char output[1024];
	sprintf( output, "Pages swapped in: %d\n", g_vmPagesReplaced );
	sprintf( output, "%sPages swapped out: %d\n", output, g_vmPagesReplacedChanged );
	sprintf( output, "%sPage miss latency: %fus\n\n", output, (f32)g_vmPageReplacementLatencyCum/(f32)g_vmPagesReplaced );
	OSReport( output );
}

