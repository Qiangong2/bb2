/*---------------------------------------------------------------------------*
  Project:  DPL2 and Reverb Interaction Test
  File:     dpl2reverbo.c

  Copyright 2002 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: dpl2reverb.c,v $
  Revision 1.1.1.1  2004/06/09 17:36:08  paulm
  GC demos from Nintendo SDK

    
    2     4/30/02 4:43p Eugene
    Added DPL2-compatible reverb effect to demo. 
    
    1     4/29/02 5:54p Eugene
    DPL2 reverb test.
    
   
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*
 * Includes
 *---------------------------------------------------------------------------*/

#include <demo.h>
#include <demo/DEMOWin.h>
#include <dolphin.h>
#include <dolphin/mix.h>
#include <dolphin/sp.h>
#include <dolphin/am.h>
#include <dolphin/axfx.h>

#include "dpl2reverb.h"


/*---------------------------------------------------------------------------*
 * SP data 
 *---------------------------------------------------------------------------*/

#define SPT_FILE "/axdemo/dpl2/dpl2reverb.spt"
#define SPD_FILE "/axdemo/dpl2/dpl2reverb.spd"

// use only a single SP sound table
static SPSoundTable *sp_table;


/*---------------------------------------------------------------------------*
* ARAM initialization
*---------------------------------------------------------------------------*/

// Use AR allocator to divide ARAM into 3 blocks
#define MAX_ARAM_BLOCKS  3

// Give a whopping 8MB of ARAM to audio!
#define AUDIO_BLOCK_SIZE_BYTES (8*1024*1024)


static u32  aramZeroBase;
static u32  aramUserBase;
static u32  aramMemArray[MAX_ARAM_BLOCKS];


// transfer buffer for ARAM audio manager (AM)
#define XFER_BUFFER_SIZE_BYTES (16*1024)

u8 xfer_buffer[XFER_BUFFER_SIZE_BYTES] ATTRIBUTE_ALIGN(32);

/*---------------------------------------------------------------------------*
 * AX and Application-layer voice abstraction
 *---------------------------------------------------------------------------*/

// Max number of voices we will support in the abstraction layer
#define MAX_DEMO_VOICES 64


// Checks SP entry 'type' to see if the voice is looped or not
#define mISLOOPED(x) ((x->type)&0x1)

int panX;   // pan value for left/right
int panY;   // pan value for front/back

// Each voice has an associated AX parameter block and an SP entry
typedef struct 
{
    AXVPB *ax_voice;
    SPSoundEntry *sp_entry;

} DEMO_VOICE;

DEMO_VOICE demo_voices[MAX_DEMO_VOICES];


// Constructs for Aux-bus effects
static AXFX_REVERBSTD     reverbStd;
static AXFX_REVERBHI      reverbHi;
static AXFX_CHORUS        chorus;
static AXFX_DELAY         delay;
static AXFX_REVERBHI_DPL2 reverbDPL2;


// AX profiling structures

// store up to 8 frames, just to be safe
#define NUM_AX_PROFILE_FRAMES 8

static AXPROFILE        ax_profile[NUM_AX_PROFILE_FRAMES]; 


/*---------------------------------------------------------------------------*
 * Prototypes
 *---------------------------------------------------------------------------*/

// for AX and voice abstraction layer
static void         ax_demo_callback        (void);
static void         ax_drop_voice_callback  (void *p);
static void         stop_all_voices         (void);
static void         init_effects            (void);
static void         stop_voice              (DEMO_VOICE *v);
static DEMO_VOICE  *play_sfx                (u32 sfx);

// for UI menus
static void         MNU_sound               (DEMOWinMenuInfo *menu, u32 item, u32 *result);
static void         MNU_position            (DEMOWinMenuInfo *menu, u32 item, u32 *result);
static void         MNU_stop_sfx            (DEMOWinMenuInfo *menu, u32 item, u32 *result);
static void         MNU_auxa                (DEMOWinMenuInfo *menu, u32 item, u32 *result);
static void         MNU_compressor          (DEMOWinMenuInfo *menu, u32 item, u32 *result);
static void         MNU_kill_all            (DEMOWinMenuInfo *menu, u32 item, u32 *result);

/*---------------------------------------------------------------------------*
 * User Interface stuff
 *---------------------------------------------------------------------------*/

static u32 soundIndex    = 0;   // current sound effect to play
static u32 positionIndex = 0;   // current panning
static u32 auxaIndex     = 0;   // current effect to apply to AuxA bus
static u32 compressFlag  = 0;   // current compressor state


DEMOWinInfo *DebugWin;
DEMOWinInfo *PositionWin;

DEMOWinMenuItem MenuItem[] = 
{
    { "Sound   : (none)",            DEMOWIN_ITM_NONE,       MNU_sound,        NULL },
    { "AuxA    : (none)",            DEMOWIN_ITM_NONE,       MNU_auxa,         NULL },
    { "",                            DEMOWIN_ITM_SEPARATOR,  NULL,             NULL },
    { "Position: C Stick",           DEMOWIN_ITM_NONE,       MNU_position,     NULL },
    { "",                            DEMOWIN_ITM_SEPARATOR,  NULL,             NULL },
    { "Compress: No",                DEMOWIN_ITM_NONE,       MNU_compressor,   NULL },
    { "",                            DEMOWIN_ITM_SEPARATOR,  NULL,             NULL },
    { "Kill All Voices",             DEMOWIN_ITM_NONE,       MNU_kill_all,     NULL },
    { "",                            DEMOWIN_ITM_TERMINATOR, NULL,             NULL }

};

DEMOWinMenuInfo Menu = 
{
    "AX DPL2-Reverb Test",      // title
    NULL,                       // window handle
    MenuItem,                   // list of menu items
    9,                          // max num of items to display at a time
    DEMOWIN_MNU_NONE,           // attribute flags

    // user callbacks
    NULL,                       // callback for menu open event 
    NULL,                       // callback for cursor move event
    NULL,                       // callback for item select event
    NULL,                       // callback for cancel event
    
    // private members
    0, 0, 0, 0, 0
};

DEMOWinMenuInfo *MenuPtr;

/*===========================================================================*
 *                   F U N C T I O N    D E F I N I T I O N S
 *===========================================================================*/


/*---------------------------------------------------------------------------*
 * VOICE ABSTRACTION LAYER STUFF
/*---------------------------------------------------------------------------*


/*---------------------------------------------------------------------------*
 * Name        : 
 * Description : 
 * Arguments   : 
 * Returns     : 
 *---------------------------------------------------------------------------*/


static void stop_voice(DEMO_VOICE *v)
{

    u32  i;
    BOOL old;

        old = OSDisableInterrupts();

        for (i=0; i<MAX_DEMO_VOICES; i++)
        {
            if (v == &demo_voices[i])
            {
                if (demo_voices[i].ax_voice) 
                {
                    if (mISLOOPED(demo_voices[i].sp_entry))
                    {
                        SPPrepareEnd(demo_voices[i].sp_entry, demo_voices[i].ax_voice);
                    }
                    else
                    {
                        AXSetVoiceState(demo_voices[i].ax_voice, AX_PB_STATE_STOP);
                    }

                    break;
                }

            }
        }

        OSRestoreInterrupts(old);
                

} // end stop_voice() 

/*---------------------------------------------------------------------------*
 * Name        : 
 * Description : 
 * Arguments   : 
 * Returns     : 
 *---------------------------------------------------------------------------*/

static void stop_all_voices(void)
{

    u32  i;
    BOOL old;



        old = OSDisableInterrupts();

        for (i=0; i<MAX_DEMO_VOICES; i++)
        {
            if (demo_voices[i].ax_voice) 
            {
                if (mISLOOPED(demo_voices[i].sp_entry))
                {
                    SPPrepareEnd(demo_voices[i].sp_entry, demo_voices[i].ax_voice);
                }
                else
                {
                    AXSetVoiceState(demo_voices[i].ax_voice, AX_PB_STATE_STOP);
                }

            }

        }

        OSRestoreInterrupts(old);
                

} // end stop_voice() 
 
/*---------------------------------------------------------------------------*
 * Name        : 
 * Description : 
 * Arguments   : 
 * Returns     : 
 *---------------------------------------------------------------------------*/


static DEMO_VOICE *play_sfx(u32 sfx)
{

    AXVPB      *ax_v;
    DEMO_VOICE *v;

    BOOL        old;


        old = OSDisableInterrupts();

        ax_v = AXAcquireVoice(15, ax_drop_voice_callback, 0);

        if (ax_v)
        {
            // use AX voice index to reference a voice in the demo abstraction layer
            demo_voices[ax_v->index].ax_voice = ax_v;

            // assign a pointer for convenience
            v = &demo_voices[ax_v->index];

            // grab the requested sound from the SP table
            v->sp_entry = SPGetSoundEntry(sp_table, sfx);

            SPPrepareSound(v->sp_entry, v->ax_voice, (v->sp_entry)->sampleRate);

            MIXInitChannel(v->ax_voice, 0, 0, 0, -960, panX, 127-panY, 0);
            AXSetVoiceState(v->ax_voice, AX_PB_STATE_RUN);

            OSRestoreInterrupts(old);
        }
        else
        {
            v = NULL;
            OSRestoreInterrupts(old);
            DEMOWinLogPrintf(DebugWin, "ERROR: AX voice allocation failed.\n");
            
        }

        return(v);

} // end play_sfx() 


/*---------------------------------------------------------------------------*
 * Name        : 
 * Description : 
 * Arguments   : None.
 * Returns     : None.
 *---------------------------------------------------------------------------*/
static void ax_demo_callback(void)
{

    u32 i;

        // check for voices which have stopped and remove them from the 
        // abstraction layer
        for (i=0; i<MAX_DEMO_VOICES; i++)
        {
            if (demo_voices[i].ax_voice)
            {
                if ( AX_PB_STATE_STOP == ((demo_voices[i].ax_voice)->pb.state))
                {
                    // if the voice has stopped, clear it from the abstraction layer
                    MIXReleaseChannel(demo_voices[i].ax_voice);
                    AXFreeVoice(demo_voices[i].ax_voice);
                    demo_voices[i].ax_voice = NULL;
                }
                else
                {
                    // otherwise, update the panning
                    MIXSetPan(demo_voices[i].ax_voice, panX);
                    MIXSetSPan(demo_voices[i].ax_voice, 127 - panY);
                    MIXUpdateSettings();
                }
            }
        }

} // end ax_demo_callback()


/*---------------------------------------------------------------------------*
 * Name        : ax_drop_voice_callback()
 * Description : Invoked by AX when a voice has been forciby dropped.
 *               Must delete references to the voice from our abstraction layer
 *               and release the associated MIXer channel. 
 * Arguments   : None.
 * Returns     : None.
 *---------------------------------------------------------------------------*/
static void ax_drop_voice_callback(void *p)
{

    AXVPB *v;

        v = (AXVPB *)p;

        MIXReleaseChannel(demo_voices[v->index].ax_voice);
        demo_voices[v->index].ax_voice = NULL;
        demo_voices[v->index].sp_entry = NULL;

} // end ax_demo_callback()



/*---------------------------------------------------------------------------*
 * MENU FUNCTIONS
/*---------------------------------------------------------------------------*

/*---------------------------------------------------------------------------*
 * Name        : 
 * Description : 
 * Arguments   : 
 * Returns     : 
 *---------------------------------------------------------------------------*/

static void MNU_compressor(DEMOWinMenuInfo *menu, u32 item, u32 *result)
{
#pragma unused(menu, item, result)

    BOOL old;

        old = OSDisableInterrupts();

        compressFlag ^= 1;    
        AXSetCompressor(compressFlag);

        OSRestoreInterrupts(old);

        if (compressFlag)
        {
            menu->items[item].name = "Compress: YES";
        }
        else
        {
            menu->items[item].name = "Compress: NO ";
        }


} // end MNU_compressor()

/*---------------------------------------------------------------------------*
 * Name        : 
 * Description : 
 * Arguments   : 
 * Returns     : 
 *---------------------------------------------------------------------------*/

static void MNU_kill_all(DEMOWinMenuInfo *menu, u32 item, u32 *result)
{
#pragma unused(menu, item, result)


    u32  i;
    BOOL old;

        old = OSDisableInterrupts();

        for (i=0; i<MAX_DEMO_VOICES; i++)
        {
            if (demo_voices[i].ax_voice) 
            {
                AXSetVoiceState(demo_voices[i].ax_voice, AX_PB_STATE_STOP);
            }
        }

        OSRestoreInterrupts(old);


} // end MNU_kill_all()

/*---------------------------------------------------------------------------*
 * Name        : 
 * Description : 
 * Arguments   : 
 * Returns     : 
 *---------------------------------------------------------------------------*/

static void MNU_auxa(DEMOWinMenuInfo *menu, u32 item, u32 *result)
{
#pragma unused(menu, item, result)

    auxaIndex++;

    auxaIndex %= 6;

    switch (auxaIndex)
    {
        case 0:
            AXRegisterAuxACallback(NULL, NULL);
            menu->items[item].name = "AuxA    : (none)";
            break;
        case 1:
            AXRegisterAuxACallback((void*)&AXFXReverbStdCallback, (void*)&reverbStd);    
            menu->items[item].name = "AuxA    : ReverbStd";
            break;
        case 2:
            AXRegisterAuxACallback((void*)&AXFXReverbHiCallback, (void*)&reverbHi);    
            menu->items[item].name = "AuxA    : ReverbHi";
            break;

        case 3:
            AXRegisterAuxACallback((void*)&AXFXReverbHiCallbackDpl2, (void*)&reverbDPL2);    
            menu->items[item].name = "AuxA    : ReverbDPL2";
            break;


        case 4:
            AXRegisterAuxACallback((void*)&AXFXChorusCallback, (void*)&chorus);    
            menu->items[item].name = "AuxA    : Chorus";
            break;
        case 5:
            AXRegisterAuxACallback((void*)&AXFXDelayCallback, (void*)&delay);    
            menu->items[item].name = "AuxA    : Delay";
            break;

    }

    
} // end MNU_reverb()


/*---------------------------------------------------------------------------*
 * Name        : 
 * Description : 
 * Arguments   : 
 * Returns     : 
 *---------------------------------------------------------------------------*/

static void MNU_sound(DEMOWinMenuInfo *menu, u32 item, u32 *result)
{
#pragma unused(result)

        soundIndex++;
        soundIndex %= 5;


        stop_all_voices();

        switch (soundIndex)
        {
            case 0: // None

                menu->items[item].name = "Sound   : None";            
                break;

            case 1:

                play_sfx(SFX_TICK);        
                menu->items[item].name = "Sound   : TICK";

                break;

            case 2:

                play_sfx(SFX_EXPLODE);
                menu->items[item].name = "Sound   : EXPLOSION";

                break;

            case 3:

                play_sfx(SFX_MACHINEGUN);
                menu->items[item].name = "Sound   : MACHINEGUN";

                break;

            case 4:

                play_sfx(SFX_SYNTH);
                menu->items[item].name = "Sound   : SYNTH";

                break;

        } // end switch()
    
} // end MNU_sound





/*---------------------------------------------------------------------------*
 * Name        : 
 * Description : 
 * Arguments   : 
 * Returns     : 
 *---------------------------------------------------------------------------*/

static void MNU_position(DEMOWinMenuInfo *menu, u32 item, u32 *result)
{
#pragma unused(result)

        positionIndex++;
        positionIndex %= 7;

        switch (positionIndex)
        {
            case 0:

                menu->items[item].name = "Position: C Stick";            

                break;

            case 1:

                panX = 0;
                panY = 0;
                menu->items[item].name = "Position: L";            

                break;

            case 2:

                panX = 63;
                panY = 0;
                menu->items[item].name = "Position: C";            

                break;

            case 3:

                panX = 127;
                panY = 0;
                menu->items[item].name = "Position: R";            

                break;

            case 4:

                panX = 0;
                panY = 127;
                menu->items[item].name = "Position: Ls";            

                break;

            case 5:

                panX = 127;
                panY = 127;
                menu->items[item].name = "Position: Rs";            

                break;

            case 6:

                panX = 63;
                panY = 127;
                menu->items[item].name = "Position: Bs";            

                break;

            }

} // end MNU_position()


/*---------------------------------------------------------------------------*
 * Name        : position_win_update()
 * Description : refresh callback for position window
 * Arguments   : 
 * Returns     : 
 *---------------------------------------------------------------------------*/

static void position_win_update(DEMOWinInfo *window)
{


    int substickX;
    int substickY;

    BOOL old;

    u32 i;
    
    u32 cpuCycles;
    u32 userCycles;
    u32 axCycles;
    u32 voices;   

    u32 maxCpuCycles =0;
    u32 maxUserCycles=0;
    u32 maxAxCycles  =0;
    u32 maxVoices    =0;   


        substickX = (MenuPtr->handle)->pad.pads[0].substickX;
        substickY = (MenuPtr->handle)->pad.pads[0].substickY;

        // Update panning position
        if (positionIndex == 0)
        {

            // if positionIndex is zero, then get panning information from 
            // the C-stick
            panX = substickX + 63;
            panY = (substickY - 63) * -1;

        }

        DEMOWinPrintfXY(window, 0, 1, "Pan       : %1.2f ", (f32)panX/127);
        DEMOWinPrintfXY(window, 0, 2, "SPan      : %1.2f ", (f32)panY/127);

        old = OSDisableInterrupts();

        i = AXGetProfile();

        if (i)
        {
            // up to 4 audio frames can complete within a 60Hz video frame
            // so spin thru the accumulated audio frame profiles and find the peak values
            while (i)
            {
                i--;

                cpuCycles   = (u32)(ax_profile[i].axFrameEnd      - ax_profile[i].axFrameStart);
                userCycles  = (u32)(ax_profile[i].userCallbackEnd - ax_profile[i].userCallbackStart);
                axCycles    = cpuCycles - userCycles;
                voices      = ax_profile[i].axNumVoices;

                // find peak values over the last i audio frames
                if (cpuCycles > maxCpuCycles)     maxCpuCycles    = cpuCycles;
                if (userCycles > maxUserCycles)   maxUserCycles   = userCycles;
                if (axCycles > maxAxCycles)       maxAxCycles     = axCycles;
                if (voices > maxVoices)           maxVoices       = voices;

            }
            OSRestoreInterrupts(old);

            DEMOWinPrintfXY(window, 0, 4, "Total CPU : %5.2f\%", (f32)OSTicksToNanoseconds(maxCpuCycles) / 50000);
            DEMOWinPrintfXY(window, 0, 6, "User      : %5.2f\%", (f32)OSTicksToNanoseconds(maxUserCycles) / 50000);
            DEMOWinPrintfXY(window, 0, 7, "AX        : %5.2f\%", (f32)OSTicksToNanoseconds(maxAxCycles) / 50000);
            DEMOWinPrintfXY(window, 0, 9, "Voices    : %5d",    maxVoices);
           
        }

        OSRestoreInterrupts(old);

} 

/*---------------------------------------------------------------------------*
 * Name        : 
 * Description : 
 * Arguments   : 
 * Returns     : 
 *---------------------------------------------------------------------------*/

void init_effects(void)
{

    reverbStd.tempDisableFX     = FALSE;
    reverbStd.time              = 3.0f;
    reverbStd.preDelay          = 0.1f;
    reverbStd.damping           = 0.5f;
    reverbStd.coloration        = 0.5f;
    reverbStd.mix               = 0.5f;

    reverbHi.tempDisableFX      = FALSE;
    reverbHi.time               = 3.0f;
    reverbHi.preDelay           = 0.1f;
    reverbHi.damping            = 0.5f;
    reverbHi.coloration         = 0.5f;
    reverbHi.crosstalk          = 0.3f;
    reverbHi.mix                = 0.5f;

    reverbDPL2.tempDisableFX    = FALSE;
    reverbDPL2.time             = 3.0f;
    reverbDPL2.preDelay         = 0.1f;
    reverbDPL2.damping          = 0.5f;
    reverbDPL2.coloration       = 0.5f;
    //reverbDPL2.crosstalk        = 0.3f;
    reverbDPL2.mix              = 0.5f;

    chorus.baseDelay            = 15;
    chorus.variation            = 0;
    chorus.period               = 500;

    delay.delay[0]              = 500;
    delay.delay[1]              = 500;
    delay.delay[2]              = 500;
    delay.feedback[0]           = 50;
    delay.feedback[1]           = 50;
    delay.feedback[2]           = 50;
    delay.output[0]             = 100;
    delay.output[1]             = 100;
    delay.output[2]             = 100;
    
    AXFXReverbStdInit(&reverbStd);      // initialize reverb
    AXFXReverbHiInit(&reverbHi);        // initialize reverb
    AXFXReverbHiInitDpl2(&reverbDPL2);  // initialize DPL2-compatible reverb
    AXFXChorusInit(&chorus);            // initialize chorus
    AXFXDelayInit(&delay);              // initialize delay


} // end init_effects()


/*---------------------------------------------------------------------------*
 * Name        : main()
 * Description : Hold on to your seatbelts!
 * Arguments   : None.
 * Returns     : None.
 *---------------------------------------------------------------------------*/
void main(void)
{
        // initialize system
        DEMOInit(NULL);
        DEMOWinInit();


        SISetSamplingRate(5);

        // initialize ARAM w/ stack allocator
        ARInit(aramMemArray, MAX_ARAM_BLOCKS);
        ARQInit();

        // initialize AI subsystem
        AIInit(NULL);

        // initialize AX audio system and MIXer application
        AXInit();
        MIXInit();

        AXSetMode(AX_MODE_DPL2);
        MIXSetSoundMode(MIX_SOUND_MODE_DPL2);

        // -----------------------------------------------------------
        // Initialize ARAM audio manager (AM)
        // -----------------------------------------------------------

        // get a block from the AR ARAM allocator
        aramUserBase = ARAlloc(AUDIO_BLOCK_SIZE_BYTES);

        // initialize AM with the block
        AMInit(aramUserBase, AUDIO_BLOCK_SIZE_BYTES);

        // retrieve start of zero buffer, as created by AM
        aramZeroBase = AMGetZeroBuffer();

        // -----------------------------------------------------------
        // Load SP data!
        // -----------------------------------------------------------

        // Retrieve sound table 
        sp_table = (SPSoundTable *)AMLoadFile(SPT_FILE, NULL);

        // Load sound effects into ARAM
        aramUserBase = AMPushBuffered(SPD_FILE, (void *)xfer_buffer, XFER_BUFFER_SIZE_BYTES);

        // -----------------------------------------------------------
        // initialize sound table! 
        // -----------------------------------------------------------
        SPInitSoundTable(sp_table, aramUserBase, aramZeroBase);

        // -----------------------------------------------------------
        // Initialize demo voice abstraction layer
        // -----------------------------------------------------------
        AXRegisterCallback(ax_demo_callback);


        // -----------------------------------------------------------
        // Initialize AUX-bus effects
        // -----------------------------------------------------------
        init_effects();


        // -----------------------------------------------------------
        // initialize profiling for AX
        // -----------------------------------------------------------
        AXInitProfile(ax_profile, NUM_AX_PROFILE_FRAMES); 

        
        // -----------------------------------------------------------
        // Invoke menu system!
        // -----------------------------------------------------------
        MenuPtr     = DEMOWinCreateMenuWindow(
                            &Menu,
                            20,
                            100
                            ); 

        DebugWin    = DEMOWinCreateWindow(
                            (u16)(MenuPtr->handle->x2+10),
                            20,
                            620,
                            440,
                            "Debug",
                            1024,
                            NULL
                            );

        PositionWin = DEMOWinCreateWindow(
                            (u16)(MenuPtr->handle->x1),
                            (u16)(MenuPtr->handle->y2+10),
                            (u16)(MenuPtr->handle->x2),
                            (u16)(MenuPtr->handle->y2+120),
                            "Position",
                            0,
                            position_win_update
                            );

        DEMOWinOpenWindow(DebugWin);
        DEMOWinOpenWindow(PositionWin);

        DEMOWinLogPrintf(DebugWin, "-------------------------------\n");
        DEMOWinLogPrintf(DebugWin, "AX DPL2-Reverb Interaction Test\n");
        DEMOWinLogPrintf(DebugWin, "-------------------------------\n");

        DEMOWinLogPrintf(DebugWin, "\n");

        DEMOWinLogPrintf(DebugWin, "Mode is AX_MODE_DPL2.\n");



        while (1)
        {

            DEMOWinMenu(MenuPtr);

        }
    

} // end main()

