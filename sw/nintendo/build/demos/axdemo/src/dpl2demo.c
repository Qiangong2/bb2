/*---------------------------------------------------------------------------*
  Project:  DPL2 Demo application
  File:     dpl2demo.c

  Copyright 2001 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: dpl2demo.c,v $
  Revision 1.1.1.1  2004/06/09 17:36:08  paulm
  GC demos from Nintendo SDK

    
    2     4/30/02 4:43p Eugene
    Fixed pad read contention between UI and application. Requires latest
    DEMO library! 
    
    1     1/15/02 3:19p Billyjack
    created
    
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*
 * Includes
 *---------------------------------------------------------------------------*/

#include <demo.h>
#include <demo/DEMOWin.h>
#include <dolphin.h>
#include <dolphin/mix.h>
#include <dolphin/sp.h>
#include <dolphin/am.h>

#include "noise.h"


/*---------------------------------------------------------------------------*
 * SP data 
 *---------------------------------------------------------------------------*/

#define SPT_FILE "/axdemo/dpl2/noise.spt"
#define SPD_FILE "/axdemo/dpl2/noise.spd"

static SPSoundTable *sp_table;


/*---------------------------------------------------------------------------*
* ARAM initialization
*---------------------------------------------------------------------------*/

// Use AR allocator to divide ARAM into 3 blocks
#define MAX_ARAM_BLOCKS  3

// Give a whopping 8MB of ARAM to audio!
#define AUDIO_BLOCK_SIZE_BYTES (8*1024*1024)


static u32  aramZeroBase;
static u32  aramUserBase;
static u32  aramMemArray[MAX_ARAM_BLOCKS];


// transfer buffer for ARAM audio manager (AM)
#define XFER_BUFFER_SIZE_BYTES (16*1024)

u8 xfer_buffer[XFER_BUFFER_SIZE_BYTES] ATTRIBUTE_ALIGN(32);

/*---------------------------------------------------------------------------*
 * Application-layer voice abstraction
 *---------------------------------------------------------------------------*/
AXVPB           *voice;
SPSoundEntry    *sound;
int             panX, panY;

/*---------------------------------------------------------------------------*
 * Prototypes
 *---------------------------------------------------------------------------*/

static void         ax_demo_callback        (void);
static void         ax_drop_voice_callback  (void *p);

// for UI menus
static void         MNU_sound               (DEMOWinMenuInfo *menu, u32 item, u32 *result);
static void         MNU_position            (DEMOWinMenuInfo *menu, u32 item, u32 *result);


/*---------------------------------------------------------------------------*
 * UI Stuff
 *---------------------------------------------------------------------------*/

DEMOWinInfo *DebugWin;
DEMOWinInfo *PositionWin;

DEMOWinMenuItem MenuItem[] = 
{
    { "",                       DEMOWIN_ITM_SEPARATOR,  NULL,             NULL },
    { "Sound: None",            DEMOWIN_ITM_NONE,       MNU_sound,        NULL },
    { "Position: C Stick",      DEMOWIN_ITM_NONE,       MNU_position,     NULL },
    { "",                       DEMOWIN_ITM_SEPARATOR,  NULL,             NULL },
    { "",                       DEMOWIN_ITM_TERMINATOR, NULL,             NULL }
};

DEMOWinMenuInfo Menu = 
{
    "AX DPL2 Demo",             // title
    NULL,                       // window handle
    MenuItem,                   // list of menu items
    5,                          // max num of items to display at a time
    DEMOWIN_MNU_NONE,           // attribute flags

    // user callbacks
    NULL,                       // callback for menu open event 
    NULL,                       // callback for cursor move event
    NULL,                       // callback for item select event
    NULL,                       // callback for cancel event
    
    // private members
    0, 0, 0, 0, 0
};

DEMOWinMenuInfo *MenuPtr;

/*===========================================================================*
 *                   F U N C T I O N    D E F I N I T I O N S
 *===========================================================================*/
static void stop_voice(void)
{
    if (voice)
    {
        DEMOWinLogPrintf(DebugWin, "Free voice.\n");
        MIXReleaseChannel(voice);
        AXFreeVoice(voice);
        voice = NULL;
    }
}
 
static void play_sound(u32 sfx)
{
    DEMOWinLogPrintf(DebugWin, "Acquire voice.\n");
    voice = AXAcquireVoice(15, ax_drop_voice_callback, 0);
    
    if (voice)
    {
        sound = SPGetSoundEntry(sp_table, sfx);

        SPPrepareSound(sound, voice, sound->sampleRate);

        MIXInitChannel(voice, 0, 0, -960, -960, panX, 127 - panY, 0);
        DEMOWinLogPrintf(DebugWin, "Start voice.\n");
        AXSetVoiceState(voice, AX_PB_STATE_RUN);
    }
    else
    {
        DEMOWinLogPrintf(DebugWin, "AX Voice allocation failed!\n");
    }
}


/*---------------------------------------------------------------------------*
 *---------------------------------------------------------------------------*/
static u32 soundIndex = 0;
static void MNU_sound(DEMOWinMenuInfo *menu, u32 item, u32 *result)
{
#pragma unused(result)

    soundIndex++;
    soundIndex %= 3;

    switch (soundIndex)
    {
    case 0: // None

        stop_voice();
        menu->items[item].name = "Sound: None";            

        break;

    case 1: // White Noise

        stop_voice();
        play_sound(SOUND_WHITE_NOISE);        
        menu->items[item].name = "Sound: White Noise";            

        break;

    case 2: // Pink Noise

        stop_voice();
        play_sound(SOUND_PINK_NOISE);
        menu->items[item].name = "Sound: Pink Noise";            

        break;
    }
    
} 


/*---------------------------------------------------------------------------*
 *---------------------------------------------------------------------------*/
static u32 positionIndex = 0;
static void MNU_position(DEMOWinMenuInfo *menu, u32 item, u32 *result)
{
#pragma unused(result)

    positionIndex++;
    positionIndex %= 7;

    switch (positionIndex)
    {
    case 0:

        menu->items[item].name = "Position: C Stick";            

        break;

    case 1:

        panX = 0;
        panY = 0;
        menu->items[item].name = "Position: L";            

        break;

    case 2:

        panX = 63;
        panY = 0;
        menu->items[item].name = "Position: C";            

        break;

    case 3:

        panX = 127;
        panY = 0;
        menu->items[item].name = "Position: R";            

        break;

    case 4:

        panX = 0;
        panY = 127;
        menu->items[item].name = "Position: Ls";            

        break;

    case 5:

        panX = 127;
        panY = 127;
        menu->items[item].name = "Position: Rs";            

        break;

    case 6:

        panX = 63;
        panY = 127;
        menu->items[item].name = "Position: Bs";            

        break;

    }

}


/*---------------------------------------------------------------------------*
 * Name        : position_win_update()
 * Description : refresh callback for position window
 * Arguments   : 
 * Returns     : 
 *---------------------------------------------------------------------------*/
static void position_win_update(DEMOWinInfo *window)
{
//    DEMOWinPrintfXY(window, 0, 2, "X: %1.2f Y: %1.2f\%", (f32)(panX + 1) / 128, (f32)(panY + 1) / 128);
    DEMOWinPrintfXY(window, 0, 2, "X: %1.2f Y: %1.2f\%", (f32)panX / 127, (f32)panY / 127);
} 

/*---------------------------------------------------------------------------*
 * Name        : 
 * Description : 
 * Arguments   : None.
 * Returns     : None.
 *---------------------------------------------------------------------------*/
static void ax_demo_callback(void)
{

    int substickX;
    int substickY;

        substickX = (MenuPtr->handle)->pad.pads[0].substickX;
        substickY = (MenuPtr->handle)->pad.pads[0].substickY;

        if (positionIndex == 0)
        {
            //DEMOPadRead();
            //panX = (int)DEMOPadGetSubStickX(0) + 63;
            //panY = ((int)DEMOPadGetSubStickY(0) - 63) * -1;

            panX = substickX + 63;
            panY = (substickY - 63) * -1;

        }

        if (voice)
        {
            MIXSetPan(voice, panX);
            MIXSetSPan(voice, 127 - panY);
            MIXUpdateSettings();
        }


} // end ax_demo_callback()


/*---------------------------------------------------------------------------*
 * Name        : ax_drop_voice_callback()
 * Description : Invoked by AX when a voice has been forciby dropped.
 *               Must delete references to the voice from our abstraction layer
 *               and release the associated MIXer channel. 
 * Arguments   : None.
 * Returns     : None.
 *---------------------------------------------------------------------------*/
static void ax_drop_voice_callback(void *p)
{
    if (p == voice)
    {
        MIXReleaseChannel(voice);
        voice = NULL;
    }
} // end ax_demo_callback()


/*---------------------------------------------------------------------------*
 * Name        : main()
 * Description : Hold on to your seatbelts!
 * Arguments   : None.
 * Returns     : None.
 *---------------------------------------------------------------------------*/
void main(void)
{
    // initialize system
    DEMOInit(NULL);
    DEMOWinInit();

    // initialize ARAM w/ stack allocator
    ARInit(aramMemArray, MAX_ARAM_BLOCKS);
    ARQInit();

    // initialize AI subsystem
    AIInit(NULL);

    // initialize AX audio system and MIXer application
    AXInit();
    MIXInit();

    AXSetMode(AX_MODE_DPL2);
    MIXSetSoundMode(MIX_SOUND_MODE_DPL2);

    // -----------------------------------------------------------
    // Initialize ARAM audio manager (AM)
    // -----------------------------------------------------------

    // get a block from the AR ARAM allocator
    aramUserBase = ARAlloc(AUDIO_BLOCK_SIZE_BYTES);

    // initialize AM with the block
    AMInit(aramUserBase, AUDIO_BLOCK_SIZE_BYTES);

    // retrieve start of zero buffer, as created by AM
    aramZeroBase = AMGetZeroBuffer();

    // -----------------------------------------------------------
    // Load SP data!
    // -----------------------------------------------------------

    // Retrieve sound table 
    sp_table = (SPSoundTable *)AMLoadFile(SPT_FILE, NULL);

    // Load sound effects into ARAM
    aramUserBase = AMPushBuffered(SPD_FILE, (void *)xfer_buffer, XFER_BUFFER_SIZE_BYTES);

    // -----------------------------------------------------------
    // initialize sound table! 
    // -----------------------------------------------------------
    SPInitSoundTable(sp_table, aramUserBase, aramZeroBase);

    // -----------------------------------------------------------
    // Initialize demo voice abstraction layer
    // -----------------------------------------------------------
    AXRegisterCallback(ax_demo_callback);

    // -----------------------------------------------------------
    // Invoke menu system!
    // -----------------------------------------------------------
    MenuPtr     = DEMOWinCreateMenuWindow(
                        &Menu,
                        20,
                        100
                        ); 
    
    DebugWin    = DEMOWinCreateWindow(
                        (u16)(MenuPtr->handle->x2+10),
                        20,
                        620,
                        440,
                        "Debug",
                        1024,
                        NULL
                        );

    PositionWin = DEMOWinCreateWindow(
                        (u16)(MenuPtr->handle->x1),
                        (u16)(MenuPtr->handle->y2+10),
                        (u16)(MenuPtr->handle->x2),
                        (u16)(MenuPtr->handle->y2+60),
                        "Position",
                        0,
                        position_win_update
                        );
                                         
    DEMOWinOpenWindow(DebugWin);
    DEMOWinOpenWindow(PositionWin);

    DEMOWinLogPrintf(DebugWin, "-------------------------------\n");
    DEMOWinLogPrintf(DebugWin, "AX DPL2 Demo!\n");
    DEMOWinLogPrintf(DebugWin, "-------------------------------\n");

    while (1)
    {

        DEMOWinMenu(MenuPtr);

    }
    

} // end main()

