//  Low-Pass Filter Demo Application
//  (c) Copyright 2002 Nintendo Technology Development, Inc.
//  


// -----------------------------------------------------------------
// SP table entry indices for lpfdemo.spd
// -----------------------------------------------------------------

#define MSX_GS_LEFT			0
#define MSX_GS_RIGHT        1
#define MSX_SPL_LEFT        2
#define MSX_SPL_RIGHT       3
#define MSX_CAR_LEFT        4
#define MSX_CAR_RIGHT       5
#define SFX_GUNSHOT			6
#define SFX_PINK			7
#define SFX_WHITE			8
#define SFX_MACHINE_GUN     9
#define SFX_EXPLOSION       10
#define SFX_FOOTSTEPS       11
#define SFX_NGC_MAN			12



// -----------------------------------------------------------------
// Coefficient table for LPF
// -----------------------------------------------------------------
// The filter is a simple one-pole function: 
// 
//     y(n) = a0*x(n) - b0*y(n-1)
//
// Where x(n) is the current input sample, and y(n-1) is the previous 
// output sample. The coefficients a0 and b0 can be calculated thusly: 
// 
//     c  = 2.0 - cos(2*pi * freq)    
//     b0 = sqrt(c^2 - 1) - c    
//     a0 = 1 + b0
//
// Where freq is in HZ and must be within the range:
// 
//     0 > freq < 16,000
//
// The coefficients must be unsigned, 16-bit words in fixed-point 
// format, having 1 bit of integer and 15 bits of fraction. 
// Furthermore, the b0 term must be (arithmetically) negated before 
// being sent to the DSP. This is because of an optimization in the 
// DSP filter calculation that is afforded by the absence of a sign 
// bit in the coefficients. 
// 

#define NUM_FREQ_CUTOFF 24  // we're using 24 steps in the freq range

typedef struct
{
    u16 a0;
    u16 b0; 
    char *text;

} __LPF_COEF;

static __LPF_COEF __coefs[] = 
{
    {  0x6a09, 0x15f6, "16000 Hz" },
    {  0x6871, 0x178e, "12800 Hz" },
    {  0x6463, 0x1b9c, "10240 Hz" },
    {  0x5db3, 0x224c, "8000 Hz " },
    {  0x5618, 0x29e7, "6400 Hz " },
    {  0x4d7a, 0x3285, "5120 Hz " },
    {  0x4367, 0x3c98, "4000 Hz " },
    {  0x3a5a, 0x45a5, "3200 Hz " },
    {  0x31c5, 0x4e3a, "2560 Hz " },
    {  0x2924, 0x56db, "2000 Hz " },
    {  0x2244, 0x5dbb, "1600 Hz " },
    {  0x1c50, 0x63af, "1280 Hz " },
    {  0x16c0, 0x693f, "1000 Hz " },
    {  0x1292, 0x6d6d, "800 Hz  " },
    {  0x0f18, 0x70e7, "640 Hz  " },
    {  0x0bf5, 0x740a, "500 Hz  " },
    {  0x09a9, 0x7656, "400 Hz  " },
    {  0x07ca, 0x7835, "320 Hz  " },
    {  0x0646, 0x79b9, "256 Hz  " },
    {  0x04ed, 0x7b12, "200 Hz  " },
    {  0x03f5, 0x7c0a, "160 Hz  " },
    {  0x032d, 0x7cd2, "128 Hz  " },
    {  0x027d, 0x7d82, "100 Hz  " },
    {  0x01fe, 0x7e01, "80 Hz   " }
};
