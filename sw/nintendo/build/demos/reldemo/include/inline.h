/*---------------------------------------------------------------------------*
  Project:  Dolphin OS relocatable module demo
  File:     inline.h

  Copyright 2002 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: inline.h,v $
  Revision 1.1.1.1  2004/06/09 17:36:09  paulm
  GC demos from Nintendo SDK

    
    1     8/23/02 17:43 Shiki
    Initial check-in.
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#ifndef __INLINE_H__
#define __INLINE_H__

#ifdef __cplusplus
extern "C" {
#endif

// If InlineFunc() exists in the static module and also in relocatable
// modules, InlineFunc() in relocatable modules shall be stripped out.
inline void InlineFunc(int a, int b)
{
    OSReport("%d + %d = %d\n", a, b, a + b);
}

#ifdef __cplusplus
}
#endif

#endif  // __INLINE_H__
