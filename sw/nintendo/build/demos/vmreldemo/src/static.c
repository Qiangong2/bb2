/*---------------------------------------------------------------------------*
  Project:  Relocatable module test
  File:     static.c

  Copyright 2000-2001 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: static.c,v $
  Revision 1.1.1.1  2004/04/20 05:29:07  paulm
  Virtual Memory library from Nintendo

 * 
 * 4     3/26/03 10:18a Stevera
 * Added VM flushing to better exercise the system.
 * 
 * 3     5/13/02 5:39p Stevera
 * Altered to test on a range of virtual addresses.
 * 
 * 2     5/09/02 4:21p Stevera
 * Added comments bracketing virtual memory modifications.
 * 
 * 1     5/09/02 4:09p Stevera
    
    8     11/27/01 21:49 Shiki
    Modified to include <dolphin.h> only.

    7     6/09/01 5:25p Shiki
    Fixed to load .rel files from lower arena to support 48MB ORCA.

    6     01/04/02 13:44 Shiki
    Separated OSModuleInfo into OSModuleInfo and OSModuleHeader.

    5     01/03/01 12:55 Shiki
    Clean up.

    4     01/02/27 13:11 Shiki
    Modified to load string table with non-debug build as well.

    3     10/31/00 3:51p Shiki
    Modified to call OSUnlink().

    2     4/19/00 12:47a Shiki
    Added a call to unlinked function to test.

    1     4/14/00 11:37p Shiki
    Initial check-in.
  $NoKeywords: $
 *---------------------------------------------------------------------------*/


#include <dolphin.h>
#include <demo.h>
#include <string.h>
//////////////////////////////////////////////////////////////
// Virtual Memory modification
#include <dolphin/vm.h>
// End Virtual Memory modification
//////////////////////////////////////////////////////////////

#ifdef _DEBUG
#define MODULE_A     "/aD.rel"
#define MODULE_B     "/bD.rel"
#define STRING_TABLE "/staticD.str"
#else
#define MODULE_A     "/a.rel"
#define MODULE_B     "/b.rel"
#define STRING_TABLE "/static.str"
#endif

static void DumpModuleInfo(OSModuleInfo* moduleInfo)
{
    OSReport("id                %d\n",         moduleInfo->id);
    OSReport("numSections       %d\n",         moduleInfo->numSections);
    OSReport("sectionInfoOffset %08xh\n",      moduleInfo->sectionInfoOffset);
    OSReport("nameOffset        %08xh [%s]\n", moduleInfo->nameOffset, moduleInfo->nameOffset);
    OSReport("nameSize          %d\n",         moduleInfo->nameSize);
    OSReport("\n");
}

static void DumpModuleHeader(OSModuleHeader* module)
{
    DumpModuleInfo(&module->info);
    OSReport("bssSize           %d\n",         module->bssSize);
    OSReport("relOffset         %08xh\n",      module->relOffset);
    OSReport("impOffset         %08xh\n",      module->impOffset);
    OSReport("impSize           %08xh\n",      module->impSize);
    OSReport("prolog            %08xh\n",      module->prolog);
    OSReport("epilog            %08xh\n",      module->epilog);
    OSReport("unresolved        %08xh\n",      module->unresolved);
    OSReport("\n");
}

void RunModules( OSModuleHeader* moduleA, OSModuleHeader* moduleB, void* ptrA, void* ptrB );
void FlushVM( void );

int main(void)
{
	u32				i;
    BOOL            result;
    DVDFileInfo     fileInfo;
    u32             length;
    OSModuleHeader* moduleAtemp;
    OSModuleHeader* moduleBtemp;
    void*			ptr;


	VMInit( 2*1024*1024,				//size of main memory VM
            ARGetBaseAddress(),			//base address of ARAM VM
            (16*1024*1024)-(16*1024) );	//size of ARAM VM

	    
    //DEMOInit( NULL );
    OSInit();
    DVDInit();
        
        
        
    // Load string table to use debugger
    result = DVDOpen(STRING_TABLE, &fileInfo);
    if (!result)
        return 1;
    length = OSRoundUp32B(DVDGetLength(&fileInfo));
    ptr = OSAllocFromArenaLo(length, 32);
    result = DVDRead(&fileInfo, ptr, (s32) length, 0);
    if (!result)
        return 1;
    OSSetStringTable(ptr);


    // Load and link module A
    result = DVDOpen(MODULE_A, &fileInfo);
    if (!result)
        return 1;
    length = OSRoundUp32B(DVDGetLength(&fileInfo));
    moduleAtemp = OSAllocFromArenaLo(length, 32);
	result = DVDRead(&fileInfo, moduleAtemp, (s32) length, 0);
    if (!result)
        return 1;


    // Load and link module B
    result = DVDOpen(MODULE_B, &fileInfo);
    if (!result)
        return 1;
    length = OSRoundUp32B(DVDGetLength(&fileInfo));
    moduleBtemp = OSAllocFromArenaLo(length, 32);
    result = DVDRead(&fileInfo, moduleBtemp, (s32) length, 0);
    if (!result)
        return 1;


    
    for( i=0; i<28; i++ )
    {
    	OSModuleHeader* moduleA;
    	OSModuleHeader* moduleB;
    	void*           ptrA;
    	void*           ptrB;
	    u32				addrSpaceModuleA = 0x7F000000 + (0x80000 * i);
	    u32				addrSpaceModuleB = 0x7F100000 + (0x80000 * i);
	    u32				addrSpaceModuleAbss = 0x7E000000 + (0x80000 * i);
	    u32				addrSpaceModuleBbss = 0x7E100000 + (0x80000 * i);
		

	    //Virtual Memory for code
	    VMAlloc( addrSpaceModuleA, 1*1024*1024 );
	    VMAlloc( addrSpaceModuleB, 1*1024*1024 );

	    //Virtual Memory for data (bss)
	    VMAlloc( addrSpaceModuleAbss, 1*1024*1024 );
	    VMAlloc( addrSpaceModuleBbss, 1*1024*1024 );
    	
	    moduleA = (OSModuleHeader*)(addrSpaceModuleA);
	    memcpy( (void*)moduleA, (void*)moduleAtemp, length );

	    moduleB = (OSModuleHeader*)(addrSpaceModuleB);
	    memcpy( (void*)moduleB, (void*)moduleBtemp, length );

	    ptrA = (void*)(addrSpaceModuleAbss); 	
	    ptrB = (void*)(addrSpaceModuleBbss);

		FlushVM();

    	RunModules( moduleA, moduleB, ptrA, ptrB );
    	
    	VMFreeAll();
    }
    
    
    OSReport( "\n\nAll instruction VM tests passed.\n\n" );
    
    while(1)
    {
    	VIWaitForRetrace();
    }

    return 0;
}



void RunModules( OSModuleHeader* moduleA, OSModuleHeader* moduleB, void* ptrA, void* ptrB )
{
    OSLink(&moduleA->info, ptrA);
	FlushVM();  
	DumpModuleHeader(moduleA);
	FlushVM();
    ((u32 (*)(void)) moduleA->prolog)();
	FlushVM();
    
    OSLink(&moduleB->info, ptrB);
	FlushVM();
    DumpModuleHeader(moduleB);
	FlushVM();
    ((u32 (*)(void)) moduleB->prolog)();
	FlushVM();

    // Unlink module B
    ((u32 (*)(void)) moduleB->epilog)();
 	FlushVM();
	OSUnlink(&moduleB->info);
	FlushVM();

    // Unlink module A
    ((u32 (*)(void)) moduleA->epilog)();
 	FlushVM();
	OSUnlink(&moduleA->info);
	FlushVM();

}

void FlushVM( void )
{
	VMStoreAllPages();
	VMInvalidateAllPages();
}
