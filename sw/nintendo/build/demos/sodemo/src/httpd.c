/*---------------------------------------------------------------------------*
  Project:  Socket demo -- tiny HTTP server
  File:     httpd.c

  Copyright 2002 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: httpd.c,v $
  Revision 1.1.1.1  2004/03/02 05:31:13  paulm
  Socket Library 24-Sep-2003

    
    4     12/12/02 8:56 Shiki
    Clean up.

    3     10/25/02 17:05 Shiki
    Added CAUTION.

    2     10/25/02 9:05 Shiki
    Added ATTRIBUTE_ALIGN(32) to Buffer[32 * 1024].

    1     10/17/02 14:09 Shiki
    Initial check-in.
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*

                                CAUTION

   This program is provided to show the concept of the WWW server
   running on the GameCube WITHOUT ANY WARRANTY. The program has
   several known problems, which are marked with "XXX" inside the
   program comments. They are very common errors to many network
   software including "buffer overrun" for an unchecked buffer,
   and "privacy compromise" in which a user can access to the
   confidential files with "..", etc. These errors must be fixed
   in production quality software.

 *---------------------------------------------------------------------------*/

#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <demo.h>
#include <dolphin/socket.h>

#define HTTP_PORT       80
#define CR              13  // '\r' carriage return (may be omitted)
#define LF              10  // '\n' line feed

#define METHOD_OPTIONS  1
#define METHOD_GET      2
#define METHOD_HEAD     3
#define METHOD_POST     4
#define METHOD_PUT      5
#define METHOD_DELETE   6
#define METHOD_TRACE    7

char* MethodStrings[] =
{
    "(NULL)",
    "OPTIONS",
    "GET",
    "HEAD",
    "POST",
    "PUT",
    "DELETE",
    "TRACE"
};

static char          Request[1024 * 1024];
static char          Path[1024];
static char          Buffer[32 * 1024] ATTRIBUTE_ALIGN(32);

static OSThread      Thread;
static u8            Stack[16*1024];
static OSThreadQueue Event;

// XXX check length
// Returns method number
static int GetRequest(int fd, char** path, char** body)
{
    int   method;
    char* request;
    char* end;
    s32   len;
    s32   contentLen;
    char* version;
    int   major = 0;
    int   minor = 9;
    char* entity = NULL;
    char* p;

    if (path)
        *path = NULL;
    if (body)
        *body = NULL;

    // Retreive just one line in case HTTP 0.9
    request = Request;
    for (;;)
    {
        len = SORead(fd, request, (s32) sizeof(Request) - (request - Request));
        if (len < 0)
        {
            return 0;
        }
        request += len;
        *request = '\0';
        if (strchr(Request, LF))
            break;
    }

    //
    // Parse request-line (Method SP Request-URI SP HTTP-Version CRLF)
    //

    // Check method
    if (strncmp("OPTIONS ", Request, strlen("OPTIONS ")) == 0)
    {
        method = METHOD_OPTIONS;
    }
    else if (strncmp("GET ", Request, strlen("GET ")) == 0)
    {
        method = METHOD_GET;
    }
    else if (strncmp("HEAD ", Request, strlen("HEAD ")) == 0)
    {
        method = METHOD_HEAD;
    }
    else if (strncmp("POST ", Request, strlen("POST ")) == 0)
    {
        method = METHOD_POST;
    }
    else if (strncmp("PUT ", Request, strlen("PUT ")) == 0)
    {
        method = METHOD_PUT;
    }
    else if (strncmp("DELETE ", Request, strlen("DELETE ")) == 0)
    {
        method = METHOD_DELETE;
    }
    else if (strncmp("TRACE ", Request, strlen("TRACE ")) == 0)
    {
        method = METHOD_TRACE;
    }
    else
    {
        return 0;
    }
    version = strchr(Request, ' ');
    ASSERT(version != NULL);
    ++version;

    // Check URI
    while (isspace(*version) && *version != LF)
    {
        ++version;
    }
    if (path)
    {
        *path = version;
    }
    end = version;
    while (!isspace(*end) && *end != LF)
    {
        ++end;
    }
    version = end;
    version = strchr(version, ' ');
    if (version)
    {
        ++version;
        // Now version should points to "HTTP" "/" 1*DIGIT "." 1*DIGIT
        if (sscanf(version, "HTTP/%d.%d", &major, &minor) != 2)
        {
            return 0;
        }
    }

    // Retrive rest of the request
    entity = NULL;
    if (0 < major)
    {
        while ((entity = strstr(Request, "\r\n\r\n")) == NULL)
        {
            len = SORead(fd, request, (s32) sizeof(Request) - (request - Request));
            if (len < 0)
            {
                return 0;
            }
            request += len;
            *request = '\0';
        }
        ASSERT(entity);
        entity += 4;
        if (body)
        {
            *body = entity;
        }
    }

    // Check Content-length
    contentLen = 0;
    p = strstr(Request, "Content-Length");
    if (p)
    {
        if (sscanf(p, "Content-Length: %d", &contentLen) != 1)
        {
            contentLen = 0;
        }
    }
    if (0 < contentLen)
    {
        // Read body
        while (request - entity < contentLen)
        {
            len = SORead(fd, request, (s32) sizeof(Request) - (request - Request));
            if (len < 0)
            {
                return 0;
            }
            request += len;
            *request = '\0';
        }
    }

    *end = '\0';
    return method;
}

// XXX check path not accessing out of /www directory
static BOOL DoGet(int fd, char* path)
{
    DVDFileInfo fileInfo;
    u32         length;
    u32         len;
    s32         offset;
    BOOL        result;

    // Convert path to DVD filename space
    strcpy(Path, "/www");
    strcat(Path, path);
    if (*(path + strlen(path) - 1) == '/')
    {
        strcat(Path, "index.html");
    }

    result = DVDOpen(Path, &fileInfo);
    if (!result)
    {
        return FALSE;
    }

    result = TRUE;
    length = DVDGetLength(&fileInfo);
    offset = 0;
    while (0 < length)
    {
        len = (sizeof(Buffer) < length) ? sizeof(Buffer) : length;
        length -= len;
        if (DVDRead(&fileInfo, Buffer, (s32) OSRoundUp32B(len), offset) != (s32) OSRoundUp32B(len))
        {
            result = FALSE;
            break;
        }
        if (SOWrite(fd, Buffer, (s32) len) != len)
        {
            result = FALSE;
            break;
        }
        offset += len;
    }
    DVDClose(&fileInfo);
    return result;
}

static int HttpdLoop(int listening)
{
    SOSockAddrIn socket;
    char*        path;
    char*        body;
    BOOL         reuse;
    int          fd;
    int          rc;

    reuse = TRUE;
    SOSetSockOpt(listening, SO_SOL_SOCKET, SO_SO_REUSEADDR, &reuse, sizeof reuse);

    socket.addr.addr = SOHtoNl(SO_INADDR_ANY);
    socket.len    = sizeof(SOSockAddrIn);
    socket.family = IP_INET;
    socket.port   = HTTP_PORT;
    rc = SOBind(listening, &socket);
    if (rc < 0)
    {
        OSReport("SOBind failed. (%d)\n", rc);
        return rc;
    }

    rc = SOListen(listening, 5);
    if (rc < 0)
    {
        OSReport("SOListen failed. (%d)\n", rc);
        return rc;
    }

    for (;;)
    {
        int  method;
        char dotted[SO_INET_ADDRSTRLEN];

        memset(&socket, 0, sizeof socket);
        socket.len = sizeof(IPSocket);
        fd = SOAccept(listening, &socket);
        if (fd < 0)
        {
            OSReport("SOAccept failed. (%d)\n", rc);
            return fd;
        }

        method = GetRequest(fd, &path, &body);
        OSReport("%s %s (%s)\n",
                 MethodStrings[method],
                 SOInetNtoP(SO_PF_INET, &socket.addr, dotted, SO_INET_ADDRSTRLEN),
                 path);
        switch (method)
        {
          case METHOD_GET:
            DoGet(fd, path);
            break;
          case METHOD_OPTIONS:
          case METHOD_HEAD:
          case METHOD_PUT:
          case METHOD_DELETE:
          case METHOD_TRACE:
          default:
            break;
        }

        SOClose(fd);
    }

    return 0;   // lint
}

static void* Httpd(void* param)
{
    #pragma unused(param)
    int listening;

    for (;;)
    {
        // Sleep while host is not configured
        while (SOGetHostID() == SOHtoNl(SO_INADDR_ANY))
        {
            OSSleepThread(&Event);
        }

        listening = SOSocket(SO_PF_INET, SO_SOCK_STREAM, 0);
        if (listening < 0)
        {
            OSReport("SOSocket failed. (%d)\n", listening);
            continue;
        }
        HttpdLoop(listening);
        SOClose(listening);
    }
}

void StartHttpd(void);
void WakeupHttpd(void);

void StartHttpd(void)
{
    OSInitThreadQueue(&Event);
    OSCreateThread(
        &Thread,                        // pointer to the thread to initialize
        Httpd,                          // pointer to the start routine
        NULL,                           // parameter passed to the start routine
        Stack + sizeof(Stack),          // initial stack address
        sizeof(Stack),
        31,                             // scheduling priority
        0);                             // joinable by default
    OSResumeThread(&Thread);
}

void WakeupHttpd(void)
{
    OSWakeupThread(&Event);
}
