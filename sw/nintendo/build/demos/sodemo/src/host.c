/*---------------------------------------------------------------------------*
  Project:  Socket demo -- Using DNS server
  File:     host.c

  Copyright 2002, 2003 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: host.c,v $
  Revision 1.1.1.1  2004/03/02 05:31:13  paulm
  Socket Library 24-Sep-2003

    
    5     3/19/03 9:20 Shiki
    Fixed main() so that State goes back to STATE_OFFLINE after GetHost()
    completes.

    4     3/07/03 15:45 Shiki
    Revised configuration steps.

    3     10/25/02 17:06 Shiki
    Modified to show error messages in two lines to fit in the screen.

    2     10/25/02 14:21 Shiki
    Modified Alloc() and Free() using mutex for mutual exclusion.

    1     10/17/02 14:09 Shiki
    Initial check-in.
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#include <demo.h>
#include <string.h>
#include <dolphin/socket.h>

const GXColor Black = {   0,   0,   0, 0 };
const GXColor Blue  = {   0,   0, 127, 0 };
const GXColor Red   = { 127,   0,   0, 0 };
const GXColor Green = {   0, 127,   0, 0 };
const GXColor White = { 255, 255, 255, 0 };

char* Anim[] =
{
    "",
    ".",
    "..",
    "...",
};

#define CYLINE  20

#define STATE_OFFLINE       0
#define STATE_LINK_DOWN     1
#define STATE_CONNECTING    2
#define STATE_ONLINE        3

static int     State;

static OSMutex AllocLock;

static void* Alloc(u32 name, s32 size)
{
    #pragma unused( name )
    void* ptr;

    if (0 < size)
    {
        OSLockMutex(&AllocLock);
        ptr = OSAlloc((u32) size);
        OSUnlockMutex(&AllocLock);
        return ptr;
    }
    else
    {
        return NULL;
    }
}

static void Free(u32 name, void* ptr, s32 size)
{
    #pragma unused( name )

    if (ptr && 0 < size)
    {
        OSLockMutex(&AllocLock);
        OSFree(ptr);
        OSUnlockMutex(&AllocLock);
    }
}

SOConfig Config =
{
    SO_VENDOR_NINTENDO,     // vendor
    SO_VERSION,             // version

    Alloc,                  // alloc
    Free,                   // free

    SO_FLAG_DHCP,           // flag
    SOHtoNl(SO_INADDR_ANY), // addr
    SOHtoNl(SO_INADDR_ANY), // netmask
    SOHtoNl(SO_INADDR_ANY), // router
    SOHtoNl(SO_INADDR_ANY), // dns1
    SOHtoNl(SO_INADDR_ANY), // dns1

    4096,                   // timeWaitBuffer
    4096                    // reassemblyBuffer
};

static SOHostEnt* GetHost(const char* host)
{
    SOHostEnt* ent;
    SOInAddr   addr;

    if (SOInetAtoN(host, &addr))
    {
        ent = SOGetHostByAddr((u8*) &addr, sizeof(SOInAddr), SO_PF_INET);
    }
    else
    {
        ent = SOGetHostByName(host);
    }
    return ent;
}

int main(int argc, char* argv[])
{
    GXRenderModeObj* rmp;
    SOInAddr         addr;
    SOInAddr         dns1;
    SOInAddr         dns2;
    u8               mac[ETH_ALEN];
    s32              mtu;
    s16              x, y;
    BOOL             linkUp;
    s32              err = 0;
    u32              type;
    SOHostEnt*       ent = NULL;
    char             dotted[SO_INET_ADDRSTRLEN];

    if (argc != 2)
    {
        OSHalt("usage: host host");
    }

    //
    // Check the serial port 1
    //
    EXIGetType(0, 2, &type);

    //
    // GX related initialization
    //
    DEMOInit(NULL);
    if (DEMOInitROMFont() == 0)
    {
        OSHalt("This program requires boot ROM ver 0.8 or later\n");
        // NOT REACHED HERE
    }

    // Clear EFB
    GXSetCopyClear(Blue, 0x00ffffff);
    GXCopyDisp(DEMOGetCurrentBuffer(), GX_TRUE);
    DEMOSetROMFontSize(CYLINE - 2, -1);

    //
    // Initialize mutex for Alloc() and Free()
    //
    OSInitMutex(&AllocLock);

    //
    // Initialize the socket library
    //
    SOInit();

    //
    // Main loop
    //
    while (!(DEMOPadGetButton(0) & PAD_BUTTON_START))
    {
        DEMOPadRead();
        if (DEMOPadGetButtonDown(0) & PAD_BUTTON_A)
        {
            if (STATE_CONNECTING <= State)
            {
                State = STATE_OFFLINE;
                SOCleanup();
            }
            else if (type == EXI_ETHER)
            {
                State = STATE_LINK_DOWN;
            }
        }

        // GX frame initialization
        DEMOBeforeRender();
        rmp = DEMOGetRenderModeObj();
        DEMOInitCaption(DM_FT_XLU, (s16) rmp->fbWidth, (s16) rmp->efbHeight);
        x = 30;
        y = 50;

        addr.addr = (u32) SOGetHostID();
        if (addr.addr != SOHtoNl(SO_INADDR_ANY))
        {
            State = STATE_ONLINE;
        }

        switch (State)
        {
          case STATE_ONLINE:
            DEMORFPrintf(x, y, 0, "Online. Push <A> to disconnect.");
            break;
          case STATE_LINK_DOWN:
            // Check LAN cable
            IPGetLinkState(NULL, &linkUp);
            if (!linkUp)
            {
                DEMORFPrintf(x, y, 0, "Detecting link pulses%s", Anim[VIGetRetraceCount() / 15 % 4]);
                break;
            }
            // SOStartup() should not be called if link pulses are not detected.
            if (SOStartup(&Config) < 0)
            {
                State = STATE_OFFLINE;
                break;
            }
            State = STATE_CONNECTING;
            // FALL THROUGH
          case STATE_CONNECTING:
            DEMORFPrintf(x, y, 0, "Connecting to the network%s", Anim[VIGetRetraceCount() / 15 % 4]);
            break;
          default:
            // Check broadband adapter
            if (type != EXI_ETHER)
            {
                DEMORFPrintf(x, y, 0, "Broadband adapter is not attached.");
            }
            else
            {
                DEMORFPrintf(x, y, 0, "Offline. Push <A> to connect.");
            }
            break;
        }
        y += CYLINE;
        y += CYLINE;

        DEMORFPrintf(x, y, 0, "IP address:      %s",
                     SOInetNtoP(SO_PF_INET, &addr, dotted, SO_INET_ADDRSTRLEN));
        y += CYLINE;
        IPGetNetmask(NULL, (u8*) &addr);
        DEMORFPrintf(x, y, 0, "Subnet mask:     %s",
                     SOInetNtoP(SO_PF_INET, &addr, dotted, SO_INET_ADDRSTRLEN));
        y += CYLINE;
        IPGetGateway(NULL, (u8*) &addr);
        DEMORFPrintf(x, y, 0, "Default gateway: %s",
                     SOInetNtoP(SO_PF_INET, &addr, dotted, SO_INET_ADDRSTRLEN));
        y += CYLINE;

        y += CYLINE;

        IPGetMacAddr(NULL, mac);
        DEMORFPrintf(x, y, 0, "MAC address:     %02x:%02x:%02x:%02x:%02x:%02x",
                     mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
        y += CYLINE;
        IPGetMtu(NULL, &mtu);
        DEMORFPrintf(x, y, 0, "MTU:             %d", mtu);
        y += CYLINE;

        y += CYLINE;

        if (0 <= SOGetResolver(&dns1, &dns2))
        {
            DEMORFPrintf(x, y, 0, "Preferred DNS server: %s",
                         SOInetNtoP(SO_PF_INET, &dns1.addr, dotted, SO_INET_ADDRSTRLEN));
            y += CYLINE;
            DEMORFPrintf(x, y, 0, "Alternate DNS server: %s",
                         SOInetNtoP(SO_PF_INET, &dns2.addr, dotted, SO_INET_ADDRSTRLEN));
            y += CYLINE;

            y += CYLINE;

            if (State == STATE_ONLINE)
            {
                ent = GetHost(argv[1]);
                State = STATE_OFFLINE;
                SOCleanup();
            }
        }

        if (ent)
        {
            u8** ptr;

            DEMORFPrintf(x, y, 0, "Name: %s", ent->name);
            y += CYLINE;
            for (ptr = ent->addrList; *ptr != NULL; ++ptr)
            {
                DEMORFPrintf(x, y, 0, "Addr: %s",
                             SOInetNtoP(SO_PF_INET, *ptr, dotted, SO_INET_ADDRSTRLEN));
                y += CYLINE;
            }
        }
        y += CYLINE;

        // Host configuration error check
        err = IPGetConfigError(NULL);
        if (err < 0)
        {
            y += CYLINE;
            switch (err)
            {
              case IP_ERR_DHCP_TIMEOUT:
                DEMORFPrintf(x, y, 0, "Connection Failed (%d):", err);      y += CYLINE;
                DEMORFPrintf(x, y, 0, "Could not find any DHCP server.");
                break;
              case IP_ERR_DHCP_EXPIRED:
                DEMORFPrintf(x, y, 0, "Connection Terminated (%d):", err);  y += CYLINE;
                DEMORFPrintf(x, y, 0, "DHCP Expired.");
                break;
              case IP_ERR_DHCP_NAK:
                DEMORFPrintf(x, y, 0, "Connection Failed (%d):", err);      y += CYLINE;
                DEMORFPrintf(x, y, 0, "DHCP Nak.");
                break;
              case IP_ERR_PPPoE_TIMEOUT:
                DEMORFPrintf(x, y, 0, "Connection Failed (%d):", err);      y += CYLINE;
                DEMORFPrintf(x, y, 0, "Could not find any access concentrator.");
                break;
              case IP_ERR_PPPoE_SERVICE_NAME:
                DEMORFPrintf(x, y, 0, "Connection Failed (%d):", err);      y += CYLINE;
                DEMORFPrintf(x, y, 0, "The requested service-name request could not be honored.");
                break;
              case IP_ERR_PPPoE_AC_SYSTEM:
                DEMORFPrintf(x, y, 0, "Connection Failed (%d):", err);      y += CYLINE;
                DEMORFPrintf(x, y, 0, "The access concentrator experienced some error.");
                break;
              case IP_ERR_PPPoE_GENERIC:
                DEMORFPrintf(x, y, 0, "Connection Failed (%d):", err);      y += CYLINE;
                DEMORFPrintf(x, y, 0, "Genric PPPoE error.");
                break;
              case IP_ERR_LCP:
                DEMORFPrintf(x, y, 0, "Connection Failed (%d):", err);      y += CYLINE;
                DEMORFPrintf(x, y, 0, "LCP negotiation error.");
                break;
              case IP_ERR_AUTH:
                DEMORFPrintf(x, y, 0, "Connection Failed (%d):", err);      y += CYLINE;
                DEMORFPrintf(x, y, 0, "Authentication error.");
                break;
              case IP_ERR_IPCP:
                DEMORFPrintf(x, y, 0, "Connection Failed (%d):", err);      y += CYLINE;
                DEMORFPrintf(x, y, 0, "IPCP negotiation error.");
                break;
              case IP_ERR_PPP_TERMINATED:
                DEMORFPrintf(x, y, 0, "Connection Terminated (%d):", err);  y += CYLINE;
                DEMORFPrintf(x, y, 0, "The session is terminated by the access concentrator.");
                break;
              case IP_ERR_ADDR_COLLISION:
                DEMORFPrintf(x, y, 0, "Detected duplicate IP address (%d):", err);  y += CYLINE;
                DEMORFPrintf(x, y, 0, "Please check the network configuration.");
                break;
              case IP_ERR_LINK_DOWN:
                DEMORFPrintf(x, y, 0, "Network cable is not connected (%d):", err);  y += CYLINE;
                DEMORFPrintf(x, y, 0, "Please check the network cable connection.");
                break;
              default:
                DEMORFPrintf(x, y, 0, "Connection Failed (%d):", err);      y += CYLINE;
                break;
            }
            y += CYLINE;

            if (STATE_CONNECTING <= State)
            {
                State = STATE_OFFLINE;
                SOCleanup();
            }
        }

        // GX frame completion
        DEMODoneRender();
    }

    OSHalt("End of test");
    return 0;   // lint
}
