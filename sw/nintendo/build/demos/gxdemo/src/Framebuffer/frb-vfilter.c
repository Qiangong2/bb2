/*---------------------------------------------------------------------------*
  Project:  Dolphin
  File:     frb-vfilter.c

  Copyright 1998 - 2003 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: frb-vfilter.c,v $
  Revision 1.1.1.1  2004/06/09 17:36:08  paulm
  GC demos from Nintendo SDK

    
    4     03/07/18 10:06 Tamaki
    fixed minor error.
    
    2     03/04/08 21:18 Hirose
    Removed DEMOLoadFont() call.
    
    1     03/04/03 10:49 Hirose
    Merged into the main tree. Supported various video format.
  
    1     03/04/02 13:40 Tamaki
    initial check-in.
  
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#include <demo.h>

#define MAX_NUM_NTSC_RM             6
#define MAX_NUM_NTSC_RM_NONPROG     4
#define MAX_NUM_PAL_RM              4
#define MAX_NUM_PAL_RM_NONPROG      4
#define MAX_NUM_MPAL_RM             4
#define MAX_NUM_MPAL_RM_NONPROG     4
#define MAX_NUM_EURGB_RM            4
#define MAX_NUM_EURGB_RM_NONPROG    4


enum{
    CTRL_BRIGHTNESS  = 0,
    CTRL_RATIO       = 1,
    CTRL_RENDERMODE  = 2,
    CTRL_PROGRESSIVE = 3
};


typedef struct _GXRMObjPtrStr
{
    GXRenderModeObj*    GXRMObjPtr;
    char*               GXRMObjString;  
}GXRMObjPtrStr;

GXRMObjPtrStr GXRMObjPtrStrTblNtsc[MAX_NUM_NTSC_RM] =
{
    {&GXNtsc240Ds,      "GXNtsc240Ds\n"},
//  {&GXNtsc240DsAa,    "GXNtsc240DsAa\n"},
    {&GXNtsc240Int,     "GXNtsc240Int\n"},
//  {&GXNtsc240IntAa,   "GXNtsc240IntAa\n"},
    {&GXNtsc480IntDf,   "GXNtsc480IntDf\n"},
    {&GXNtsc480Int,     "GXNtsc480Int\n"},
//  {&GXNtsc480IntAa,   "GXNtsc480IntAa\n"},
    {&GXNtsc480Prog,    "GXNtsc480Prog\n"},
//  {&GXNtsc480ProgAa,  "GXNtsc480ProgAa\n"},
    {&GXNtsc480ProgSoft,"GXNtsc480ProgSoft\n"},
};

GXRMObjPtrStr GXRMObjPtrStrTblPal[MAX_NUM_PAL_RM] =
{
    {&GXPal264Ds,       "GXPal264Ds\n"},
//  {&GXPal264DsAa,     "GXPal264DsAa\n"},
    {&GXPal264Int,      "GXPal264Int\n"},
//  {&GXPal264IntAa,    "GXPal264IntAa\n"},
    {&GXPal528IntDf,    "GXPal528IntDf\n"},
    {&GXPal528Int,      "GXPal528Int\n"},
//  {&GXPal524IntAa,    "GXPal524IntAa\n"},
};

GXRMObjPtrStr GXRMObjPtrStrTblMPal[MAX_NUM_MPAL_RM] =
{
    {&GXMpal240Ds,          "GXMpal240Ds\n"},
//  {&GXMpal240DsAa,        "GXMpal240DsAa\n"},
    {&GXMpal240Int,         "GXMpal240Int\n"},
//  {&GXMpal240IntAa,       "GXMpal240IntAa\n"},
    {&GXMpal480IntDf,       "GXMpal480IntDf\n"},
    {&GXMpal480Int,         "GXMpal480Int\n"},
//  {&GXMpal480IntAa,       "GXMpal480IntAa\n"},
};

GXRMObjPtrStr GXRMObjPtrStrTblEURGB[MAX_NUM_EURGB_RM] =
{
    {&GXEurgb60Hz240Ds,     "GXEurgb60Hz240Ds\n"},
//  {&GXEurgb60Hz240DsAa,   "GXEurgb60Hz240DsAa\n"},
    {&GXEurgb60Hz240Int,    "GXEurgb60Hz240Int\n"},
//  {&GXEurgb60Hz240IntAa,  "GXEurgb60Hz240IntAa\n"},
    {&GXEurgb60Hz480IntDf,  "GXEurgb60Hz480IntDf\n"},
    {&GXEurgb60Hz480Int,    "GXEurgb60Hz480Int\n"},
//  {&GXEurgb60Hz480IntAa,  "GXEurgb60Hz480IntAa\n"}
};

/*---------------------------------------------------------------------------*
   The macro ATTRIBUTE_ALIGN provides a convenient way to align initialized 
   arrays.  Alignment of vertex arrays to 32B IS NOT required, but may result 
   in a slight performance improvement.
 *---------------------------------------------------------------------------*/
f32 Verts_f32[] ATTRIBUTE_ALIGN(32) = 
{
//      x, y, z       
    0.0f, 0.0f, 1.0f,    // 0:0
    0.0f, 1.0f, 0.0f,    // 0:1
    1.0f, 0.0f, 0.0f,    // 0:2
    0.0f, 0.0f, 1.0f,    // 1:0
    -1.0f, 0.0f, 0.0f,   // 1:1
    0.0f, 1.0f, 0.0f,    // 1:2
    0.0f, 0.0f, 1.0f,    // 2:0
    0.0f, -1.0f, 0.0f,   // 2:1
    -1.0f, 0.0f, 0.0f,   // 2:2
    0.0f, 0.0f, 1.0f,    // 3:0
    1.0f, 0.0f, 0.0f,    // 3:1
    0.0f, -1.0f, 0.0f,   // 3:2
    0.0f, 0.0f, -1.0f,   // 4:0
    1.0f, 0.0f, 0.0f,    // 4:1
    0.0f, 1.0f, 0.0f,    // 4:2
    0.0f, 0.0f, -1.0f,   // 5:0
    0.0f, 1.0f, 0.0f,    // 5:1
    -1.0f, 0.0f, 0.0f,   // 5:2
    0.0f, 0.0f, -1.0f,   // 6:0
    -1.0f, 0.0f, 0.0f,   // 6:1
    0.0f, -1.0f, 0.0f,   // 6:2
    0.0f, 0.0f, -1.0f,   // 7:0
    0.0f, -1.0f, 0.0f,   // 7:1
    1.0f, 0.0f, 0.0f     // 7:2
};

f32 Tex_f32[] ATTRIBUTE_ALIGN(32) =
{
    1.0f, 1.0f, 0.5f, 0.0f, 0.0f, 1.0f, 
    0.0f, 1.0f, 1.0f, 1.0f, 0.5f, 0.0f,  
    1.0f, 1.0f, 0.5f, 0.0f, 0.0f, 1.0f, 
    0.0f, 1.0f, 1.0f, 1.0f, 0.5f, 0.0f,
    0.0f, 1.0f, 1.0f, 1.0f, 0.5f, 0.0f,
    1.0f, 1.0f, 0.5f, 0.0f, 0.0f, 1.0f, 
    0.0f, 1.0f, 1.0f, 1.0f, 0.5f, 0.0f,
    1.0f, 1.0f, 0.5f, 0.0f, 0.0f, 1.0f, 
    
};

u32    ticks = 0;     // time counter

typedef struct{
    s32    control;   // mode
    BOOL   flug_prog; // progressive ON/OFF  
} myControlParam;

typedef struct{
    u8 newFilter[7];
    f32 brightness;
    f32 ratio;
    GXBool flug;
    u16 df_a, df_b;
    u32 RMIndex;
} myCopyFilter;

static u32  MaxNumRM;
static u32  MaxNumRMNonProg;

static GXRMObjPtrStr* GXRMObjPtrStrTbl;

/*---------------------------------------------------------------------------*
   Forward references
 *---------------------------------------------------------------------------*/
 
void        main                ( void );
static void myDrawCaptionTick   ( myCopyFilter, myControlParam * );
static void CameraInit          ( Mtx );
static void DrawInit            ( void );
static void DrawTick            ( Mtx );
static void DrawBackgroundTick  ( Mtx, GXTexObj * );
static void AnimTick            ( void );
static void PrintIntro          ( void );
static void myInitCopyFilter    ( myCopyFilter *);
static void myCopyFilterTick    ( myCopyFilter *, myControlParam *);

/*---------------------------------------------------------------------------*
   Application main loop
 *---------------------------------------------------------------------------*/

void main ( void )
{
    Mtx         v;
    GXTexObj    texObj,texObj_zebra_1, texObj_zebra_2, texObj_zebra_4;
    TEXPalettePtr tpl = (TEXPalettePtr)NULL;
    myCopyFilter cf;
    myControlParam cp;
    GXRenderModeObj * rmode;
    
    VIInit();
    switch( VIGetTvFormat() )
    {
      case VI_NTSC:
        MaxNumRM         = MAX_NUM_NTSC_RM;
        MaxNumRMNonProg  = MAX_NUM_NTSC_RM_NONPROG;
        GXRMObjPtrStrTbl = GXRMObjPtrStrTblNtsc;
        OSReport("NTSC\n");
        break;
      case VI_PAL:
        MaxNumRM         = MAX_NUM_PAL_RM;
        MaxNumRMNonProg  = MAX_NUM_PAL_RM_NONPROG;
        GXRMObjPtrStrTbl = GXRMObjPtrStrTblPal;
        OSReport("PAL\n");
        break;
      case VI_MPAL:
        MaxNumRM         = MAX_NUM_MPAL_RM;
        MaxNumRMNonProg  = MAX_NUM_MPAL_RM_NONPROG;
        GXRMObjPtrStrTbl = GXRMObjPtrStrTblMPal;
        OSReport("M/PAL\n");
        break;
      case VI_EURGB60:
        MaxNumRM         = MAX_NUM_EURGB_RM;
        MaxNumRMNonProg  = MAX_NUM_EURGB_RM_NONPROG;
        GXRMObjPtrStrTbl = GXRMObjPtrStrTblEURGB;
        OSReport("EU RGB 60\n");
        break;
      default:
        OSHalt("Unknown TV Format\n");
    }
    
    cf.RMIndex = 3; // GXNtsc480Int
    
    myInitCopyFilter( &cf );

    cp.control = 0;       // BRIGHTNESS
    cp.flug_prog = FALSE; // PROGRESSIVE OFF
    
    rmode = GXRMObjPtrStrTbl[cf.RMIndex].GXRMObjPtr;

    DEMOInit(rmode);
    
    TEXGetPalette(&tpl, "gxTests/vfilter01.tpl");
    TEXGetGXTexObjFromPalette(tpl, &texObj, 0);
    TEXGetGXTexObjFromPalette(tpl, &texObj_zebra_1, 1);
    TEXGetGXTexObjFromPalette(tpl, &texObj_zebra_2, 2);
    TEXGetGXTexObjFromPalette(tpl, &texObj_zebra_4, 3);

    PrintIntro();
    
    while(!(DEMOPadGetButton(0) & PAD_BUTTON_MENU))
    {   

        DEMOPadRead();
        myCopyFilterTick( &cf, &cp );       
        
        rmode = GXRMObjPtrStrTbl[cf.RMIndex].GXRMObjPtr;
        GXSetCopyFilter( rmode->aa, rmode->sample_pattern, GX_TRUE, cf.newFilter );

        DEMOBeforeRender();
        
        myDrawCaptionTick(cf, &cp);
        CameraInit(v);
        DrawBackgroundTick(v, &texObj);
        
        GXLoadTexObj(&texObj_zebra_1, GX_TEXMAP1); // zebra : width 1
        GXLoadTexObj(&texObj_zebra_2, GX_TEXMAP2); // zebra : width 2
        GXLoadTexObj(&texObj_zebra_4, GX_TEXMAP3); // zebra : width 4

        DrawInit();
        DrawTick(v);
        
        DEMODoneRender();
                
                
        AnimTick();        // Update animation.
   
    }

    OSHalt("End of demo.");
    
}

/*---------------------------------------------------------------------------*
   Functions
 *---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*
    Name:           myDrawCaptionTick
    
    Description:    Indicate current status on a screen
                    
    Arguments:      cf          pointer of myCopyFilter
                    cp          pointer of myControlparam
    
    Returns:        none
 *---------------------------------------------------------------------------*/

static void myDrawCaptionTick( myCopyFilter cf, myControlParam * cp ){

    s16 i;
    s16 pos_x = 15;
    s16 pos_y = 30;
    s16 pos_z = 1;
    char * indent[4];
    
    DEMOInitCaption(DM_FT_XLU, 320, 240);

    DEMOPrintf(pos_x, 20, pos_z, "frb-vfilter");
    DEMOPrintf(pos_x, 25, pos_z, "-----------");
    
    for (i = 0; i < 7; ++i){
        DEMOPrintf((s16)(pos_x+10), (s16)(i*10+10+pos_y), pos_z, "%d", cf.newFilter[i]);
    }
    
    for (i = 0;i < 4;i++){
        if(i == cp->control)
            indent[i]=">";
        else
            indent[i]=" ";
    }
    
    DEMOPrintf(pos_x, (s16)(90  + pos_y), pos_z, "%sBRIGHTNESS",indent[0]);
    DEMOPrintf(pos_x, (s16)(100 + pos_y), pos_z, " %f", cf.brightness/64.0F);
    
    DEMOPrintf(pos_x, (s16)(115 + pos_y), pos_z, "%sRATIO",indent[1]);
    DEMOPrintf(pos_x, (s16)(125 + pos_y), pos_z, " %f\%", cf.ratio*100.0F);
        
    DEMOPrintf(pos_x, (s16)(140 + pos_y), pos_z, "%sRENDER MODE",indent[2]);
    DEMOPrintf(pos_x, (s16)(150 + pos_y), pos_z, " %s", GXRMObjPtrStrTbl[cf.RMIndex].GXRMObjString);
    
    DEMOPrintf(pos_x, (s16)(165 + pos_y), pos_z, "%sPROGRESSIVE",indent[3]);
    if(cp->flug_prog)
        DEMOPrintf(pos_x, (s16)(175 + pos_y), pos_z, " ON");
    else
        DEMOPrintf(pos_x, (s16)(175 + pos_y), pos_z, " OFF");
    
}


/*---------------------------------------------------------------------------*
    Name:           myInitCopyFilter
    
    Description:    Initialize the struct " myCopyFilter ".
                    
    Arguments:      cf          pointer of myCopyFilter
    
    Returns:        none
 *---------------------------------------------------------------------------*/

static void myInitCopyFilter( myCopyFilter * cf ){

    u32 i;
    GXRenderModeObj * rmode;
    f32 tmp_center = 0.0f;

    rmode = GXRMObjPtrStrTbl[cf->RMIndex].GXRMObjPtr;

    cf->brightness = 0;
        
    for(i = 0;i < 7; i++){
        cf->brightness += rmode->vfilter[i];
        cf->newFilter[i] = rmode->vfilter[i];
        if(i > 1 && i < 5)
            tmp_center += rmode->vfilter[i]; 
    }
    cf->ratio = tmp_center / cf->brightness;
    cf->df_b = (u16)(cf->brightness * cf->ratio);
    cf->df_a = (u16)(cf->brightness - cf->df_b);

}
 
/*---------------------------------------------------------------------------*
    Name:           myCopyFilterTick
    
    Description:    Calculates the parameter of myCopyFilter once
                        with input from PAD.
    
    Arguments:      cf          pointer of myCopyFilter
                    cp          pointer of myControlParam 
    
    Returns:        none
 *---------------------------------------------------------------------------*/
 
static void myCopyFilterTick( myCopyFilter * cf, myControlParam * cp){
        s16 i;
        u8 temp_a[4], temp_b[3];
    
        u16 button;
        u16 buttonDown;
        GXRenderModeObj * rmode;
        u32 max_rm;
        BOOL pad_r = FALSE;
        BOOL pad_l = FALSE;
        BOOL flug_change_rm = FALSE;
        BOOL flug_change_param = FALSE;
        BOOL flug_bad_param = FALSE;

        button = DEMOPadGetButton(PAD_CHAN0);
        buttonDown = DEMOPadGetButtonDown(PAD_CHAN0);
        
        if( buttonDown & ( PAD_BUTTON_RIGHT | PAD_BUTTON_A )){
            pad_r = TRUE;
        }
        else if( buttonDown & ( PAD_BUTTON_LEFT | PAD_BUTTON_B )){
            pad_l = TRUE;
        }
        else{
            if(buttonDown & PAD_BUTTON_DOWN){
                cp->control = (cp->control + 1) % 4;
            }
            else if(buttonDown & PAD_BUTTON_UP){
                cp->control = (cp->control + 3) % 4;
            }
        }

        if(cp->control == CTRL_BRIGHTNESS){
            // increase brightness
            if((button & (PAD_BUTTON_RIGHT | PAD_BUTTON_A))&&
                cf->df_b < 64*3 && cf->df_a < 64*4){
                    cf->brightness++;
                    cf->df_b = (u16)(cf->brightness * cf->ratio);
                    cf->df_a = (u16)(cf->brightness - cf->df_b);
                    flug_change_param = TRUE;
    
            }
            // decrease brightness
            else if(button & (PAD_BUTTON_LEFT | PAD_BUTTON_B)){
                if(cf->brightness > 0){
                    cf->brightness--;
                    cf->df_b = (u16)(cf->brightness * cf->ratio);
                    cf->df_a = (u16)(cf->brightness - cf->df_b);
                    flug_change_param = TRUE;
                }
            }
        }
        else if(cp->control == CTRL_RATIO){
            // increase a ratio of center line
            if(button & (PAD_BUTTON_RIGHT | PAD_BUTTON_A)){
                if(cf->df_a > 0 && cf->df_b < 63*3){
                    
                    cf->df_b++;
                    cf->df_a--;
                    
                    cf->ratio = (f32)cf->df_b / cf->brightness;
                    flug_change_param = TRUE;
                }
            }
            // decrease a ratio of center line
            else if(button & (PAD_BUTTON_LEFT | PAD_BUTTON_B)){
                if(cf->df_b > 0 && cf->df_a < 63*4){
                
                    cf->df_b--;
                    cf->df_a++;
                    
                    cf->ratio = (f32)cf->df_b / cf->brightness;
                    flug_change_param = TRUE;
                }
            }           
        }
        else if(cp->control == CTRL_RENDERMODE){
            // increase RenderModeIndex
            if(pad_l){
            
                if(cp->flug_prog)
                    max_rm = MaxNumRM;
                else
                    max_rm = MaxNumRMNonProg;
            
                cf->RMIndex++;
                cf->RMIndex %= max_rm;
    
                rmode = GXRMObjPtrStrTbl[cf->RMIndex].GXRMObjPtr;
                DEMOReInit(rmode);
                
                flug_change_rm = TRUE;
                
            }
            // decrease RenderModeIndex
            else if(pad_r){
            
                if(cp->flug_prog)
                    max_rm = MaxNumRM;
                else
                    max_rm = MaxNumRMNonProg;
            
                if(cf->RMIndex==0){
                    cf->RMIndex = max_rm;
                }
                cf->RMIndex--;
                
                rmode = GXRMObjPtrStrTbl[cf->RMIndex].GXRMObjPtr;
                DEMOReInit(rmode);
                
                flug_change_rm = TRUE;
                
            }
        }
        else if(cp->control == CTRL_PROGRESSIVE){
        
            if(pad_r || pad_l)
                cp->flug_prog = !cp->flug_prog;
        
            if((cp->flug_prog == FALSE) && cf->RMIndex >= MaxNumRMNonProg){
                cf->RMIndex = 0;
                rmode = GXRMObjPtrStrTbl[cf->RMIndex].GXRMObjPtr;
                DEMOReInit(rmode);
                
                flug_change_rm = TRUE;
                
            }
        
        }
            
        // get back to initial condition
        if((button & PAD_TRIGGER_Z) || flug_change_rm){
            myInitCopyFilter( cf );     
        }
        
        // calculate BRIGHTNESS and RATIO
        else if(flug_change_param){
        
            flug_bad_param = FALSE;
        
            // calculate each 7 points' value
            for(i=0; i<4; i++){
                if(i < cf->df_a % 4)
                    temp_a[i]=(u8)((f32)cf->df_a / 4.0F + 1);
                else
                    temp_a[i]=(u8)((f32)cf->df_a / 4.0F);
                if(temp_a[i]>63)
                    flug_bad_param = TRUE;
            }
            for(i=0; i<3; i++){
                if(i < cf->df_b % 3)
                    temp_b[i]=(u8)((f32)cf->df_b / 3.0F + 1);
                else
                    temp_b[i]=(u8)((f32)cf->df_b / 3.0F);
                if(temp_b[i]>63)
                    flug_bad_param = TRUE;
            }
        
            if(!flug_bad_param){
                cf->newFilter[0] = temp_a[0];
                cf->newFilter[1] = temp_a[2];
                cf->newFilter[2] = temp_b[0];
                cf->newFilter[3] = temp_b[1];
                cf->newFilter[4] = temp_b[2];
                cf->newFilter[5] = temp_a[1];
                cf->newFilter[6] = temp_a[3];
            }
        }
}
 
 
/*---------------------------------------------------------------------------*
    Name:           CameraInit
    
    Description:    Initialize the projection matrix and load into hardware.
                    Initialize the view matrix.
                    
    Arguments:      v      view matrix
    
    Returns:        none
 *---------------------------------------------------------------------------*/
 
static void CameraInit ( Mtx v )
{
    Mtx44   p;      // projection matrix
    Vec     up      = {0.0F, 1.0F, 0.0F};
    Vec     camLoc  = {0.5F, 1.0F, -3.0F};
    Vec     objPt   = {0.5F, 0.0F, 0.0F};
    f32     top     = 0.0375F;
    f32     left    = -0.050F;
    f32     znear   = 0.1F;
    f32     zfar    = 10.0F;
    
    MTXFrustum(p, top, -top, left, -left, znear, zfar);
    GXSetProjection(p, GX_PERSPECTIVE);
    
    MTXLookAt(v, &camLoc, &up, &objPt);    
}

/*---------------------------------------------------------------------------*
    Name:           DrawBackTick
    
    Description:    Draw the background once.
                    
    Arguments:      v           view matrix
                    texObj      pointer of GXTexObj
    
    Returns:        none
 *---------------------------------------------------------------------------*/

static void DrawBackgroundTick( Mtx v , GXTexObj * texObj){

    Mtx    m;    // Model matrix.
    Mtx    mv;   // Modelview matrix.
    
    GXSetNumTexGens(1);
    GXSetTexCoordGen(GX_TEXCOORD0, GX_TG_MTX2x4, GX_TG_TEX0, GX_IDENTITY);
    GXSetTevOp(GX_TEVSTAGE0, GX_REPLACE);
    GXSetTevOrder(GX_TEVSTAGE0, GX_TEXCOORD0, GX_TEXMAP0, GX_COLOR_NULL);
    
    MTXIdentity( m );
    MTXConcat(v, m, mv);
    GXLoadPosMtxImm(mv, GX_PNMTX0);
    
    GXClearVtxDesc();
    GXSetVtxDesc(GX_VA_POS,  GX_DIRECT);
    GXSetVtxDesc(GX_VA_TEX0, GX_DIRECT);

    GXSetVtxAttrFmt(GX_VTXFMT0, GX_VA_POS,  GX_POS_XYZ,  GX_F32,    0);
    GXSetVtxAttrFmt(GX_VTXFMT0, GX_VA_TEX0, GX_TEX_ST,   GX_F32,    0);

    GXLoadTexObj(texObj, GX_TEXMAP0);

    GXBegin(GX_QUADS, GX_VTXFMT0, 4);
    
        GXPosition3f32( 3.0f, 1.0f, 1.0f );
        GXTexCoord2f32( 0.0f, 0.0f );
        GXPosition3f32( -1.5f, 1.0f, 1.0f );
        GXTexCoord2f32( 1.0f, 0.0f );
        GXPosition3f32( -1.7f, -2.2f, 1.0f );
        GXTexCoord2f32( 1.0f, 1.0f );
        GXPosition3f32( 3.1f, -2.2f, 1.0f );
        GXTexCoord2f32( 0.0f, 1.0f );
        
    GXEnd();

}

/*---------------------------------------------------------------------------*
    Name:           DrawInit
    
    Description:    Initializes the vertex attribute format 0, and sets
                    the array pointers and strides for the indexed data.
                    
    Arguments:      none
    
    Returns:        none
 *---------------------------------------------------------------------------*/
 
static void DrawInit( void )
{ 
    GXColor black = {0, 0, 0, 0};

    GXSetCopyClear(black, 0x00ffffff);

    GXSetNumTexGens(1);
    GXSetTexCoordGen(GX_TEXCOORD0, GX_TG_MTX2x4, GX_TG_TEX0, GX_IDENTITY);
    GXSetTevOp(GX_TEVSTAGE0, GX_REPLACE);

    // change zebra texture at regular time intervals 
    switch ((u32)(ticks / 180) % 3)
    {
        case 0:
            GXSetTevOrder(GX_TEVSTAGE0, GX_TEXCOORD0, GX_TEXMAP1, GX_COLOR_NULL);
            break;
        case 1:
            GXSetTevOrder(GX_TEVSTAGE0, GX_TEXCOORD0, GX_TEXMAP2, GX_COLOR_NULL);
            break;
        case 2:
            GXSetTevOrder(GX_TEVSTAGE0, GX_TEXCOORD0, GX_TEXMAP3, GX_COLOR_NULL);
            break;
        default:
            GXSetTevOrder(GX_TEVSTAGE0, GX_TEXCOORD0, GX_TEXMAP1, GX_COLOR_NULL);
            break;
    }

    // Set current vertex descriptor to enable position and color0.
    // Both use 8b index to access their data arrays.
    GXClearVtxDesc();
    GXSetVtxDesc(GX_VA_POS, GX_INDEX8);
    GXSetVtxDesc(GX_VA_TEX0, GX_INDEX8);
            
    // Position has 3 elements (x,y,z), each of type f32.
    GXSetVtxAttrFmt(GX_VTXFMT0, GX_VA_POS, GX_POS_XYZ, GX_F32, 0);
    
    // Texture Chood 0 has 2 elements (s,t), each of type f32.
    GXSetVtxAttrFmt(GX_VTXFMT0, GX_VA_TEX0, GX_TEX_ST, GX_F32, 0);
    
    // stride = 3 elements (x,y,z) each of type f32
    GXSetArray(GX_VA_POS, Verts_f32, 3*sizeof(f32));
    // stride = 2 elements (s,t) each of type f32
    GXSetArray(GX_VA_TEX0, Tex_f32, 2*sizeof(f32));

    GXSetNumChans(0);

}

/*---------------------------------------------------------------------------*
    Name:           Vertex
    
    Description:    Create my vertex format
                    
    Arguments:      t        8-bit triangle index
                    v        8-bit vertex index
    
    Returns:        none
 *---------------------------------------------------------------------------*/
 
static inline void Vertex( u8 t, u8 v )
{
    u8 tv3 = (u8) (3 * t + v);
    GXPosition1x8(tv3);
    GXTexCoord1x8(tv3);    
}

/*---------------------------------------------------------------------------*
    Name:           DrawTick
    
    Description:    Draw the model once.
    
    Arguments:      v        view matrix
    
    Returns:        none
 *---------------------------------------------------------------------------*/
 
static void DrawTick( Mtx v )
{
    Mtx    m;        // Model matrix.
    Mtx    mv;       // Modelview matrix.
    u8     iTri;     // index of triangle
    u8     iVert;    // index of vertex
    
    // model has a rotation about z axis
    MTXRotDeg(m, 'y', 1 * ticks);
    MTXConcat(v, m, mv);
    GXLoadPosMtxImm(mv, GX_PNMTX0);

    GXBegin(GX_TRIANGLES, GX_VTXFMT0, 24);
    
    // for all triangles of octahedron, ...
    for (iTri = 0; iTri < 8; ++iTri)
    {
        // for all vertices of triangle, ...
        for (iVert = 0; iVert < 3; ++iVert)
        {
            Vertex(iTri, iVert);
        }
    }

    GXEnd();
}

/*---------------------------------------------------------------------------*
    Name:           AnimTick
    
    Description:    Computes next time step.
                    
    Arguments:      none
    
    Returns:        none
 *---------------------------------------------------------------------------*/
 
static void AnimTick( void )
{
    ticks++;
}

/*---------------------------------------------------------------------------*
    Name:            PrintIntro
    
    Description:     Prints the directions on how to use this demo.
                    
    Arguments:       none
    
    Returns:         none
 *---------------------------------------------------------------------------*/
 
static void PrintIntro( void )
{
    OSReport("\n\n********************************\n");
    OSReport("to change item:\n");
    OSReport("     + UP/DOWN\n");
    OSReport("to change BRIGHTNESS/RATIO\n");
    OSReport("     + RIGHT or Button A : increase parameter\n");
    OSReport("     + LEFT  or Button B : decrease parameter\n");
    OSReport("to change RENDERMODE/PROGRESSIVE\n");
    OSReport("     + RIGHT, + LEFT, Button A and B : change parameter\n");
    OSReport("     (if you choose PROGRESSIVE OFF,\n");
    OSReport("      progressive mode are excluded from RENDERMODE)\n");
    OSReport("to return default parameters\n");
    OSReport("     Button Z\n");
    OSReport("to quit:\n");
    OSReport("     START : quit the program.\n");
#ifdef MACOS
    OSReport("     click on the text output window\n");
    OSReport("     select quit from the menu or hit 'command q'\n");
    OSReport("     select 'don't save'\n");
#endif
    OSReport("********************************\n");
}

/*============================================================================*/
