/*---------------------------------------------------------------------------*
  Project:  GAMECUBE DVDETH Demo5
  File:     osreportdemo1.c

  Copyright 2002 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: osreportdemo1.c,v $
  Revision 1.1.1.1  2004/03/02 05:31:13  paulm
  Socket Library 24-Sep-2003

    
    11    03/03/20 4:29p Ooshima
    Modified to move ip address setting to ipaddress.h
    
    10    03/03/13 8:17p Ooshima
    Deleted DVDETH definition
    
    9     03/02/28 5:26p Ooshima
    2nd release version
    
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*
  This program shows OSReport function
 *---------------------------------------------------------------------------*/
#include <demo.h>
#include "osreport.h"
#include "ipaddress.h"

#define    NumOf(x)    (sizeof(x)/sizeof((x)[0]))

void        main            ( void );

// The name of the file we are going to read.
static const char *pMessage[] =
{
    "\n",
    "Hello World!\n",
    "abcdefghijklmnopqrstuvwxyz\n",
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ\n",
    "1234567890\n",
    "GAMEBOY PLAYER\n",
    "GAMECUBE\n",
    "WAVEBIRD\n",
    "Broad Band Adapter\n",
    "Modem\n",
    "Network\n",
    "Nintendo\n",
    "Online\n",
    "NTSC\n",
    "The socket library provides the access to the internet via the Nintendo GameCube broadband adapter. \n",
    "The socket library API resembles Berkeley sockets \nwhich are the de facto standard programming interface for networking.\n",
};

void main(void)
{
    u32         i;

    DEMOInit(NULL);

    // Initialize OSReport();
    OSReportInit(ReportServerAddr, ReportServerPort);

    while(!(DEMOPadGetButton(0) & PAD_BUTTON_MENU))
    {
        u16 down;

        DEMOPadRead();           // Read controller
        down  = DEMOPadGetButtonDown(0);

        if ( down & PAD_BUTTON_A )
        {
            int j;
            for( j = 0; j < 10 ; j++)
            {
                OSReport("%3d\n", j);
                for( i = 0; i < NumOf(pMessage); i++)
                {
                    OSReport(pMessage[i]);
                }
            }
        }

        VIWaitForRetrace();
    }

    OSHalt("End of demo");
}

