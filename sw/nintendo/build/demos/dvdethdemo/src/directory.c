/*---------------------------------------------------------------------------*
  Project: GAMECUBE DVDETH Directory Demo
  File:    directory.c

  Copyright 2002 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: directory.c,v $
  Revision 1.1.1.1  2004/03/02 05:31:13  paulm
  Socket Library 24-Sep-2003


    5     03/09/08 16:26 Ooshima
    Modified to insert VIWaitForRetrace() in printOneLevel for fast FST
    access. 
    
    3     03/09/02 4:22p Ueno_kyu
    3rd release version 03/09/02 3:00p
    
    2     03/03/20 11:03a Ooshima
    Modified to move ip address setting to ipaddress.h
    
    1     03/02/28 5:26p Ooshima
    2nd release version
    
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*
  This program shows how to use DVDOpenDir, DVDReadDir, and DVDCloseDir.
 *---------------------------------------------------------------------------*/
#include <dolphin/os.h>
#include <dolphin/dvd.h>
#include <demo.h>
#include "osreport.h"
#include "ipaddress.h"

#ifdef DVDETH
#include <dolphin/dvdeth.h>
#endif

#include <string.h>

#ifdef DVDETH
#define FST_MAX_SIZE 1024 * 1024
#endif

static void PrintIntro( void )
{
    OSReport("\n\n");
    OSReport("************************************************\n");
    OSReport("directory: DVDOpenDir, DVDReadDir, DVDCloseDir test\n");
    OSReport("************************************************\n");
    OSReport("to quit hit the start button\n");
    OSReport("\n");
    OSReport("A Button    : Read directroy\n"); 
    OSReport("************************************************\n\n");
}

static void printOneLevel(char* pathName);

struct dirs_t
{
    struct dirs_t*  next;       // must be first
    DVDDirEntry     dirEntry;
};

typedef struct dirs_t       dirs;

static void printOneLevel(char* pathName)
{
    DVDFileInfo     finfo;
    DVDDir          dir;
    DVDDirEntry     dirent;
    dirs*           start = (dirs*)NULL;
    dirs*           curr;
    dirs            *p, *q;
    char            path[256];

    curr = (dirs*)&start;
    
    if (FALSE == DVDOpenDir(".", &dir))
    {
        OSReport("Can't open dir %s\n", path);
        return;
    }
    
    while(1)
    {
        if (FALSE == DVDReadDir(&dir, &dirent))
        {
            for(p = start; p != (dirs*)NULL; )
            {
                strcpy(path, pathName);
                strcat(path, "/");
                strcat(path, p->dirEntry.name);
                OSReport("\n%s:\n", path);
                
                if (FALSE == DVDChangeDir(p->dirEntry.name))
                {
                    OSReport("Can't change dir to %s\n", path);
                    return;
                }
                printOneLevel(path);
                if (FALSE == DVDChangeDir(".."))
                {
                    OSReport("Can't change dir to %s/..\n", path);
                    return;
                }

                q = p;
                p = p->next;
                OSFree((void*)q);
            }
            return;
        }
        
        if (dirent.isDir)
        {
            OSReport("D %9d %s\n", 0, dirent.name);

            if (NULL == ( curr->next = (dirs*)OSAlloc(sizeof(dirs)) ) )
            {
                OSReport("Can't allocate memory\n");
                return;
            }
            curr = curr->next;
            curr->next = (dirs*)NULL;
            memcpy((void*)&(curr->dirEntry), (void*)&dirent,
                   sizeof(DVDDirEntry));
            // we open directories later
        }
        else
        {
            if (FALSE == DVDOpen(dirent.name, &finfo))
            {
                OSReport("Can't open file %s/%s\n", path, dirent.name);
                return;
            }
            
            OSReport("F %9d %s\n", DVDGetLength(&finfo), dirent.name);

            DVDClose(&finfo);
        }

        VIWaitForRetrace();
        
    } // while(1)
    
} // void printOneLevel(char*)

void main(void)
{
    BOOL        reset = FALSE;  // TRUE if reset is requested

#ifdef DVDETH
    char*       fstBuffer;         // pointer to the FST buffer
#endif

    DEMOInit(NULL);

#ifdef DVDETH
    // Initialize Network
    DVDEthInit(GCClientAddr, Netmask, Gateway);

    // Initialize DVD
    DVDLowInit(DvdServerAddr, DvdServerPort);
#endif
    // Initialize OSReport();
    OSReportInit(ReportServerAddr, ReportServerPort);

#ifdef DVDETH
    // Allocate a buffer to load the fst
    fstBuffer = OSAlloc(FST_MAX_SIZE);

    // Initialize FST
    if (!DVDFstInit(fstBuffer, FST_MAX_SIZE))
    {
        OSHalt("Cannot initialize FST");
    }
#endif

    DVDSetAutoFatalMessaging(TRUE);

    PrintIntro();

       while(!(DEMOPadGetButton(0) & PAD_BUTTON_MENU))
    {
        u16 down;

        DEMOPadRead();           // Read controller
        down  = DEMOPadGetButtonDown(0);

        if ( down & PAD_BUTTON_A )
        {
            printOneLevel(".");
            PrintIntro();
        }

        // Handle reset button
        if (OSGetResetButtonState())
        {
            reset = TRUE;
        }
        else if (reset)
        {
            // Restart
            OSResetSystem(OS_RESET_RESTART, 0x00, FALSE);
        }
    }

#ifdef DVDETH
    DVDEthShutdown();
#endif   

    OSHalt("End of program");

    // NOT REACHED HERE
}
