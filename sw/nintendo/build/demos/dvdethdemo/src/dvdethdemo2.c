/*---------------------------------------------------------------------------*
  Project:  Dolphin DVD Demo2
  File:     dvddemo2.c

  Copyright 2002 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: dvdethdemo2.c,v $
  Revision 1.1.1.1  2004/03/02 05:31:13  paulm
  Socket Library 24-Sep-2003


    12    03/09/05 8:55a Ueno_kyu
    
    11    03/09/02 4:23p Ueno_kyu
    
    3rd release version 03/09/02 3:00p
    
    10    03/03/20 11:03a Ooshima
    Modified to move ip address setting to ipaddress.h
    
    9     03/02/28 5:26p Ooshima
    2nd release version
    
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*
  This program shows how to use asynchronous read function
 *---------------------------------------------------------------------------*/

#include <demo.h>
#include "osreport.h"
#include "ipaddress.h"
#ifdef DVDETH
#include <dolphin/dvdeth.h>
#endif

#define    NumOf(x)    (sizeof(x)/sizeof((x)[0]))

#ifdef DVDETH
#define FST_MAX_SIZE 1024 * 1024
#endif

void        main    ( void );
static void callback(s32 result, DVDFileInfo* fileInfo);

// The name of the file we are going to read.
static const char *pFileName[] =
{
    "gxTests/dnt-02.tpl",
    "gxTests/dnt-03.tpl",
    "gxTests/frb-00.tpl",
    "gxTests/g2d-00.tpl",
    "gxTests/geo-00.tpl",
    "gxTests/lit-06.tpl",
    "gxTests/lit-10.tpl",
    "gxTests/pseudo.tpl",
    "gxTests/tev-00.tpl",
    "gxTests/tev-01.tpl",
    "gxTests/tev-02.tpl",
    "gxTests/tev-03.tpl",
    "gxTests/tev-04.tpl",
    "gxTests/tex-01.tpl",
    "gxTests/tex-03.tpl",
    "gxTests/tex-05.tpl",
    "gxTests/tex-06.tpl",
    "gxTests/tex-07.tpl",
    "gxTests/tex-08.tpl",
    "gxTests/tf-02.tpl",
    "gxTests/tg-01.tpl",
    "gxTests/tg-02.tpl",
    "gxTests/tg-cube.tpl",
    "gxTests/tg-cube1.tpl",
    "gxTests/tg-dual.tpl",
    "gxTests/tg-pc.tpl"
};

// This variable should be declared "volatile" because this is shared
// by two contexts.
volatile s32     readDone[NumOf(pFileName)] = {0};

DVDFileInfo fileInfo[NumOf(pFileName)];

 /*---------------------------------------------------------------------------*
    Name:               callback
    
    Description:        callback function for DVDReadAsync
                                
    Arguments:          result     -1 if read fails, file size if succeeded.
                        fileInfo   fileInfo for the file transferred.
    
    Returns:            none
 *---------------------------------------------------------------------------*/
static void callback(s32 result, DVDFileInfo* pfileInfo)
{
    u32 j = 0;

    for(j = 0; j < NumOf(pFileName); j++)
    {
        if(pfileInfo == &fileInfo[j])
            break;
    }

    if (result == -1)
    {
        readDone[j] = -1;
    }
    else
    {
        readDone[j] = result;
    }
    return;
}

void main(void)
{
    u32         fileSize[NumOf(pFileName)];
    char*       buffer[NumOf(pFileName)];         // pointer to the buffer
    u8*         ptr;
    u32         i, j;
    u32         Sum = 0;

#ifdef DVDETH
    char*       fstBuffer;         // pointer to the FST buffer
#endif

    DEMOInit(NULL);

#ifdef DVDETH
    // Initialize Network
    DVDEthInit(GCClientAddr, Netmask, Gateway);

    // Initialize DVD
    DVDLowInit(DvdServerAddr, DvdServerPort);
#endif

    // Initialize OSReport();
    OSReportInit(ReportServerAddr, ReportServerPort);

#ifdef DVDETH
    // Allocate a buffer to load the fst
    fstBuffer = OSAlloc(FST_MAX_SIZE);

    // Initialize FST
    if (!DVDFstInit(fstBuffer, FST_MAX_SIZE))
    {
        OSHalt("Cannot initialize FST");
    }
#endif

    DVDSetAutoFatalMessaging(TRUE);

    for( j = 0 ; j < NumOf(pFileName) ; j++)
    {
        // We MUST open the file before accessing the file
        if (FALSE == DVDOpen(pFileName[j], &fileInfo[j]))
        {
            OSHalt("Cannot open file");
        }

        // Get the size of the file
        fileSize[j] = DVDGetLength(&fileInfo[j]);

        // Allocate a buffer to read the file.
        // Note that pointers returned by OSAlloc are always 32byte aligned.
        buffer[j] = OSAlloc(OSRoundUp32B(fileSize[j]));
    }

    OSReport("All files were openend.\n");

    for( j = 0 ; j < NumOf(pFileName) ; j++)
    {
        // This function only start reading the file.
        // The read keeps going in the background after the function returns
 
        if (FALSE == DVDReadAsync(&fileInfo[j], (void*)buffer[j],
                      (s32)OSRoundUp32B(fileSize[j]), 0, callback))
        {
            OSHalt("Error occurred when issuing read");
        }
    }

    OSReport("All Read Command Issued.\n");

    for( j = 0 ; j < NumOf(pFileName) ; j++)
    {
        while (1) 
        {    
            if(readDone[j] > 0)
            {    
                break;
            }
            else if(readDone[j] < 0)
            {
                OSHalt("Error occurred when reading file");
            }
        }
    }

    OSReport("All Read commands were completed.\n");

    // Check File Sum
    for( j = 0 ; j < NumOf(pFileName) ; j++)
    {
        ptr = (u8*)buffer[j];
        for (i = 0; i < fileSize[j]; i++)
        {
            Sum += *ptr++;
        }
        OSReport("file : %s,Size :%d, Ret: %d, Sum:%X\n", pFileName[j], fileSize[j] , readDone[j], Sum);

        Sum = 0;
        DVDClose(&fileInfo[j]);
        OSFree(buffer[j]);
    }
    
#ifdef DVDETH
    DVDEthShutdown();
#endif

    OSHalt("End of demo");
}
