/*---------------------------------------------------------------------------*
  Project:  GAMECUBE DVDETH Demo1
  File:     dvddemo1.c

  Copyright 2002 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: dvdethdemo1.c,v $
  Revision 1.1.1.1  2004/03/02 05:31:13  paulm
  Socket Library 24-Sep-2003


    17    03/09/05 8:55a Ueno_kyu
    
    16    03/09/02 4:22p Ueno_kyu
    
    3rd release version 03/09/02 3:00p
    
    15    03/03/20 11:03a Ooshima
    Modified to move ip address setting to ipaddress.h
    
    14    03/02/28 5:26p Ooshima
    2nd release version
    
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*
  This program shows how to use synchronous read function
 *---------------------------------------------------------------------------*/
#include <demo.h>
#include <stdlib.h>
#include "osreport.h"
#include "ipaddress.h"
#ifdef DVDETH
#include <dolphin/dvdeth.h>
#endif

#define    NumOf(x)    (sizeof(x)/sizeof((x)[0]))

#ifdef DVDETH
#define FST_MAX_SIZE 1024 * 1024
#endif

void main(void);

// The name of the file we are going to read.
static const char *pFileName[] =
{
    "texts/test1.txt",
    "texts/test2.txt"
};

static void PrintIntro( void )
{
    OSReport("\n\n");
    OSReport("************************************************\n");
    OSReport("dvdethdemo1: DVDRead test\n");
    OSReport("************************************************\n");
    OSReport("to quit hit the start button\n");
    OSReport("\n");
    OSReport("A Button    : Read files\n"); 
    OSReport("************************************************\n\n");
}

void main(void)
{
    DVDFileInfo fileInfo;
    u32         fileSize;
    char*       buffer;         // pointer to the buffer
    u32         j;
    s32            retVal;
    BOOL        reset = FALSE;  // TRUE if reset is requested

#ifdef DVDETH
    char*       fstBuffer;         // pointer to the FST buffer
#endif

    DEMOInit(NULL);

#ifdef DVDETH
    // Initialize Network
    DVDEthInit(GCClientAddr, Netmask, Gateway);

    // Initialize DVD
    DVDLowInit(DvdServerAddr, DvdServerPort);
#endif

    // Initialize OSReport();
    OSReportInit(ReportServerAddr, ReportServerPort);

#ifdef DVDETH
    // Allocate a buffer to load the fst
    fstBuffer = OSAlloc(FST_MAX_SIZE);

    // Initialize FST
    if (!DVDFstInit(fstBuffer, FST_MAX_SIZE))
    {
        OSHalt("Cannot initialize FST");
    }
#endif

    DVDSetAutoFatalMessaging(TRUE);

    PrintIntro();

    while(!(DEMOPadGetButton(0) & PAD_BUTTON_MENU))
    {
        u16 down;

        DEMOPadRead();           // Read controller
        down  = DEMOPadGetButtonDown(0);

        if ( down & PAD_BUTTON_A )
        {
            for( j = 0 ; j < NumOf(pFileName) ; j++)
            {
                // We MUST open the file before accessing the file
                if (FALSE == DVDOpen(pFileName[j], &fileInfo))
                {
                    OSHalt("Cannot open file");
                }

                // Get the size of the file
                fileSize = DVDGetLength(&fileInfo);

                // Allocate a buffer to read the file.
                // Note that pointers returned by OSAlloc are always 32byte aligned.
                buffer = OSAlloc(OSRoundUp32B(fileSize));
                // read the entire file here

                if (0 > (retVal = DVDRead(&fileInfo, (void*)buffer, (s32)OSRoundUp32B(fileSize), 0)))
                {
                    OSHalt("Error occurred when reading file");
                }

                // Close the file
                DVDClose(&fileInfo);

                // Show the files
                OSReport("file:%s\n", pFileName[j]);
                OSReport("%s\n", buffer);

                OSFree(buffer);
            }            
        } // if ( down & PAD_BUTTON_A )
        
        // Handle reset button
        if (OSGetResetButtonState())
        {
            reset = TRUE;
        }
        else if (reset)
        {
            // Restart
            OSResetSystem(OS_RESET_RESTART, 0x00, FALSE);
        }

        VIWaitForRetrace();
    } // while

#ifdef DVDETH
    DVDEthShutdown();
#endif

    OSHalt("End of demo");
}

