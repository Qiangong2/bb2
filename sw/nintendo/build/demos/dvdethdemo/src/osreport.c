/*---------------------------------------------------------------------------*
  Project:  Socket UDP OSReport demo.
  File:     OSReport.c

  Copyright 2002 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: osreport.c,v $
  Revision 1.1.1.1  2004/03/02 05:31:13  paulm
  Socket Library 24-Sep-2003

    
    4     03/03/20 11:03a Ooshima
    Modified to move ip address setting to ipaddress.h
    
    3     03/03/13 8:16p Ooshima
    Added SOInit in OSReportInit
    
    2     03/03/11 4:29p Ooshima
    Modified to prevent calling SOWrite from callback.
    
    1     03/02/28 5:26p Ooshima
    2nd release version
    
  $NoKeywords: $
 *---------------------------------------------------------------------------*/
#include <dolphin.h>
#include <dolphin/socket.h>
#include <string.h>
#include <stdarg.h>
#include <stdio.h>
#include "OSReport.h"
#include "ipaddress.h"

// mtu of Either - header size
#define MAX_TRANSFER_SIZE 1500 - ETH_HLEN - IP_MIN_HLEN - UDP_HLEN 
static char Buffer[MAX_TRANSFER_SIZE];

static int     SocketDesc;
static BOOL       OSReportInitialized = FALSE;

static void* Alloc(u32 name, s32 size)
{
    #pragma unused( name )
    BOOL  enabled;
    void* ptr;

    if (0 < size)
    {
        enabled = OSDisableInterrupts();
        ptr = OSAlloc((u32) size);
        OSRestoreInterrupts(enabled);
    }
    else
    {
        ptr = NULL;
    }
    return ptr;
}

static void Free(u32 name, void* ptr, s32 size)
{
    #pragma unused( name, size )
    BOOL enabled;

    enabled = OSDisableInterrupts();
    if (ptr && 0 < size)
    {
        OSFree(ptr);
    }
    OSRestoreInterrupts(enabled);
}

SOConfig Config =
{
    SO_VENDOR_NINTENDO,
    SO_VERSION,

    Alloc,
    Free,

    0,
    SO_INADDR_ANY,
    SO_INADDR_ANY,
    SO_INADDR_ANY,
    SO_INADDR_ANY,
    SO_INADDR_ANY,

    4096,
    4096
};

/*---------------------------------------------------------------------------*
  Name:         OSReport2()

  Description:  original OSReport

  Arguments:    msg         pointer to a null-terminated string that contains
                            the format specifications
                ...         optional arguments

  Returns:      None.
 *---------------------------------------------------------------------------*/
void OSReport2(const char* msg, ...)
{
    va_list marker;

    va_start(marker, msg);
    vprintf(msg, marker);
    va_end(marker);
}

/*---------------------------------------------------------------------------*
  Name:         OSReport()

  Description:  Outputs a formatted message into the output port

  Arguments:    msg         pointer to a null-terminated string that contains
                            the format specifications
                ...         optional arguments

  Returns:      None.
 *---------------------------------------------------------------------------*/
// Do not call this OSReport from a callback function,
// for SOWrite does not work in a callback function.
// If you want to call OSReport from a callback function,
// create a new thread to call SOWrite.
void OSReport(const char* msg, ...)
{
    va_list marker;

    BOOL        enabled;
    int            rc;
    int         len;

    if(OSReportInitialized && OSGetCurrentThread() &&
        &OSGetCurrentThread()->context == OSGetCurrentContext())
    {
        enabled = OSDisableInterrupts();

        // vsnprintf return size of extended string.
        va_start(marker, msg);
        rc = vsnprintf(Buffer, MAX_TRANSFER_SIZE, msg, marker);
        va_end(marker);

        // size of string is 0.
        if(!rc)
        {
            OSRestoreInterrupts(enabled);
            return;
        }

        // Though size to write is "rc",
        // only "count" bytes were written.
        if(MAX_TRANSFER_SIZE < rc)
        {
            OSReport2("OSReport Error: Unable to write last %d bytes.\n", 
                        rc - MAX_TRANSFER_SIZE);
            len = MAX_TRANSFER_SIZE;
        }
        else
        {
            len = rc + 1;// + NULL byte
        }

        rc = SOWrite(SocketDesc, Buffer, len);
        if(rc < 0 )
        {
            OSReport2("OSReport Error: Write failed. Error No.= %d\n",rc);
        }

        OSRestoreInterrupts(enabled);
        return;
    }
    else
    {
        va_start(marker, msg);
        vprintf(msg, marker);
        va_end(marker);
    }
}

void OSVReport(const char* msg, va_list marker)
{
    BOOL        enabled;
    int            rc;
    int         len;

    // OSGetCurrentThread() == NULL means that the callback is called 
    // while the thread is not running like VIWaitForRetrace.
    // OSGetCurrentThread()->context != OSGetCurrentContext() means an
    // interrupt occured while the thread is running.
    
    if(OSReportInitialized && OSGetCurrentThread() &&
        &OSGetCurrentThread()->context == OSGetCurrentContext())
    {
        enabled = OSDisableInterrupts();
        
        // vsnprintf return size of extended string.
        rc = vsnprintf(Buffer, MAX_TRANSFER_SIZE, msg, marker);

        // size of string is 0.
        if(!rc)
        {
            OSRestoreInterrupts(enabled);
            return;
        }

        // Though size to write is "rc",
        // only "count" bytes were written.
        if(MAX_TRANSFER_SIZE < rc)
        {
            OSReport2("OSReport Error: Unable to write last %d bytes.\n", 
                        rc - MAX_TRANSFER_SIZE);
            len = MAX_TRANSFER_SIZE;
        }
        else
        {
            len = rc + 1;// + NULL byte
        }

        rc = SOWrite(SocketDesc, Buffer, len);
        if(rc < 0 )
        {
            OSReport2("OSReport Error: Write failed. Error No.= %d\n",rc);
        }

        OSRestoreInterrupts(enabled);
        return;
    }
    else
    {
        vprintf(msg, marker);
    }
}

BOOL OSReportInit(const u8* pServerAddr, u16 ServerPort)
{
    IPSocket     socket;
    OSCalendarTime td;
    char   buffer[24];
    int    nLength;
    int    rc;
    BOOL   state;

    // if you do not use either DVDETH or CW Debugger, 
    // you must initialize the IP routing table. 

    memcpy(&Config.addr,    GCClientAddr, IP_ALEN);
    memcpy(&Config.netmask, Netmask,      IP_ALEN);
    memcpy(&Config.router,  Gateway,      IP_ALEN);

    SOInit();

    state = FALSE;
    while(!state)
    {
        IPGetLinkState(NULL, &state);
    }
    
    // Before SOStartup, initialize heap.
    SOStartup(&Config);

    while (SOGetHostID() == 0)
      ;

    SocketDesc = SOSocket(SO_PF_INET, SO_SOCK_DGRAM, 0);

    socket.family    = SO_PF_INET;
    socket.len       = sizeof(IPSocket);
    memcpy(socket.addr, pServerAddr, IP_ALEN);
    socket.port      = ServerPort;

    rc = SOConnect(SocketDesc, &socket);
    if(rc != 0)
    {
        OSReport2("OSReport Error: Connect failed. Error No.= %d\n", rc);
        return FALSE;
    }

    OSTicksToCalendarTime(OSGetTime(), &td);
    sprintf(buffer, "GC connect %2d:%2d:%2d\n", td.hour, td.min, td.sec);
    nLength = (int)strlen(buffer)+1;
    // It takes 2 or 3 seconds to send first data successfully.
    // After the first send, next send will finish immediately.

    // Even if server does not exit, error will not be returned immediately.
    rc = SOWrite(SocketDesc, buffer, nLength);// +NULL
    if(rc < 0)
    {
        OSReport2("OSReport Error : Write failed. Error No.= %d\n", rc);
        return FALSE;
    }

    OSReportInitialized = TRUE;

    return TRUE;
}

void OSReportShutdown(void)
{
    if (!OSReportInitialized)
       return;
    
    OSReportInitialized = FALSE;

    SOClose(SocketDesc);
    
    SOCleanup();
    return;
}

/*---------------------------------------------------------------------------*
  Name:         OSPanic()

  Description:  Sends a formatted message to the output port with the
                file name and the line number. And then halts.

  Arguments:    file        should be __FILE__
                line        should be __LINE__
                msg         pointer to a null-terminated string that contains
                            the format specifications
                ...         optional arguments

  Returns:      None.
 *---------------------------------------------------------------------------*/
void OSPanic(const char* file, int line, const char* msg, ...)
{
    va_list marker;
    u32     i;
    u32*    p;
    BOOL    enabled;

    if(OSReportInitialized && OSGetCurrentThread() &&
        &OSGetCurrentThread()->context == OSGetCurrentContext())
    {    
        // If Socket Error occurred,
        // this function does not work.

        va_start(marker, msg);
        vprintf(msg, marker);
        va_end(marker);

        va_start(marker, msg);
        OSVReport(msg, marker);
        va_end(marker);
        
        OSReport(" in \"%s\" on line %d.\n", file, line);
        OSReport2(" in \"%s\" on line %d.\n", file, line);

        OSReport("\nAddress:      Back Chain    LR Save\n");
        OSReport2("\nAddress:      Back Chain    LR Save\n");
        // Stack crawl
        for (i = 0, p = (u32*) OSGetStackPointer(); // get current sp
             p && (u32) p != 0xffffffff && i++ < 16;
             p = (u32*) *p)                     // get caller sp
        {
            OSReport("0x%08x:   0x%08x    0x%08x\n", p, p[0], p[1]);    
            OSReport2("0x%08x:   0x%08x    0x%08x\n", p, p[0], p[1]);
        }
        PPCHalt();
    }
    else
    {
        enabled = OSDisableInterrupts();
        va_start(marker, msg);
        vprintf(msg, marker);
        va_end(marker);

        OSReport2(" in \"%s\" on line %d.\n", file, line);
        // Stack crawl
        OSReport2("\nAddress:      Back Chain    LR Save\n");
        for (i = 0, p = (u32*) OSGetStackPointer(); // get current sp
             p && (u32) p != 0xffffffff && i++ < 16;
             p = (u32*) *p)                     // get caller sp
        {
            OSReport2("0x%08x:   0x%08x    0x%08x\n", p, p[0], p[1]);
        }
        OSRestoreInterrupts(enabled);

        PPCHalt();
    }
}