/*---------------------------------------------------------------------------*
  Project:  Dolphin DVD error handling demo
  File:     errorhandling2.c

  Copyright 2003 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: errorhandling2.c,v $
  Revision 1.1.1.1  2004/03/02 05:31:13  paulm
  Socket Library 24-Sep-2003

    
    4     03/09/05 8:56a Ueno_kyu
    
    3     03/09/02 4:23p Ueno_kyu
    
    3rd release version 03/09/02 3:00p

    2     03/03/20 11:03a Ooshima
    Modified to move ip address setting to ipaddress.h.
    
    1     03/02/28 5:26p Ooshima
    2nd release version.
    
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*
 * This demo illustrates how to handle errors. See __refresh_status() for
 * the actual error handling code!
 *---------------------------------------------------------------------------*/

#include <demo.h>
#include <dolphin.h>
#include <demo/DEMOWin.h>
#include <string.h>
#include <stdio.h>
#include "selectfile2.h"
#include "osreport.h"
#include "ipaddress.h"

#ifdef DVDETH
#include <dolphin/dvdeth.h>
#endif

#ifdef DVDETH
#define FST_MAX_SIZE 1024 * 1024
#endif

/*---------------------------------------------------------------------------*
 * DVD definitions
 *---------------------------------------------------------------------------*/

//static DVDFileInfo                  FileInfo;
//static volatile BOOL                FileInfoIsInUse = FALSE;

static DVDDiskID                    DiskID;

/*---------------------------------------------------------------------------*
 * Misc
 *---------------------------------------------------------------------------*/
#define MIN(x, y)                   ((x) < (y)? (x):(y))

BOOL  DemoPauseFlag = FALSE;

/*---------------------------------------------------------------------------*
 * Data to pass from command issuing routines to status printing routine.
 *---------------------------------------------------------------------------*/
enum{
    STATE_START,   // Initial State
    STATE_END,     // read command is not issued or finished.
    STATE_WAITING  // waiting to be queued 
};

enum{
    COMMAND_READ,  // Read only
    COMMAND_COPY,  // Read for copy
    COMMAND_WRITE  // Write 
};

typedef struct
{
    u8*    buffer; 
    s32    command;
    s32    state;
    s32    Length;
    char   filename[128];
}   MyCBData;

#define MAX_READ_ISSUE 8

static DVDFileInfo  FileInfo[MAX_READ_ISSUE];
static MyCBData     CBData[MAX_READ_ISSUE];

/*---------------------------------------------------------------------------*
 * Messages for errors
 *---------------------------------------------------------------------------*/
typedef struct
{
    char*           line[6];
} Message;

// Error messages. XXX Caution: Subject to change.
Message ErrorMessages[] = {
    {"Please close the NINTENDO GAMECUBE cover.", "", "", "", "", ""},
    {"Please insert the disc for xxx.", "", "", "", "", ""},
    {"This disc is not for xxx.", "Please insert the disc for xxx.", "", "", "", ""},
    {"The disc could not be read.", "", "Please read the NINTENDO GAMECUBE",
     "Instruction Booklet for more information", "", ""},
    {"An error has occurred.", "", "Turn the power OFF and check", "the NINTENDO GAMECUBE",
     "Instruction Booklet for", "further instructions."},
    {"Please insert the next disc.", "", "", "", "", ""},
};

enum{
    MESSAGE_COVER_OPEN = 0,
    MESSAGE_NO_DISK,
    MESSAGE_WRONG_DISK,
    MESSAGE_RETRY_ERROR,
    MESSAGE_FATAL_ERROR,
    MESSAGE_CHANGE_DISK
};

/*---------------------------------------------------------------------------*
 * Prototypes
 *---------------------------------------------------------------------------*/
static void InitWindows(void);

static void __status_refresh(DEMOWinInfo *handle);

static void Run_Demo(void);

static void MNU_open(DEMOWinMenuInfo *menu, u32 item, u32 *result);
static void MNU_read_start(DEMOWinMenuInfo *menu, u32 item, u32 *result);
static void MNU_cancel(DEMOWinMenuInfo *menu, u32 item, u32 *result);
static void MNU_cancel_all(DEMOWinMenuInfo *menu, u32 item, u32 *result);
static void MNU_pause(DEMOWinMenuInfo *menu, u32 item, u32 *result);
static void MNU_resume(DEMOWinMenuInfo *menu, u32 item, u32 *result);
static void MNU_dump_waiting_queue(DEMOWinMenuInfo *menu, u32 item, u32 *result);
static void MNU_pick_file(DEMOWinMenuInfo *menu, u32 item, u32 *result);
static void MNU_pick_cancel(DEMOWinMenuInfo *menu, u32 item, u32 *result);

/*---------------------------------------------------------------------------*
 * Main Control Menu Stuff!
 *---------------------------------------------------------------------------*/
DEMOWinMenuItem control_menu_items[] = 
{
    { "ISSUE COMMAND",              DEMOWIN_ITM_SEPARATOR,  NULL,                   NULL },
    { "  Open",                     DEMOWIN_ITM_NONE,       MNU_open,               NULL },
#ifndef DVDETH
    { "  Read Start",               DEMOWIN_ITM_NONE,       MNU_read_start,         NULL },
#else
    { "  Read Write Start",         DEMOWIN_ITM_NONE,       MNU_read_start,         NULL },
#endif
    { "  DVDCancel",                DEMOWIN_ITM_NONE,       MNU_cancel,             NULL },
    { "  DVDCancelAll",             DEMOWIN_ITM_NONE,       MNU_cancel_all,         NULL },
    { "  DVDPause",                 DEMOWIN_ITM_NONE,       MNU_pause,              NULL },
    { "  DVDResume",                DEMOWIN_ITM_NONE,       MNU_resume,             NULL },
    { "  DVDDumpWaitingQueue",      DEMOWIN_ITM_NONE,       MNU_dump_waiting_queue, NULL },
    { "",                           DEMOWIN_ITM_TERMINATOR, NULL,                   NULL }
};

DEMOWinMenuInfo control_menu = 
{
   "DVD Priority Control Demo",     // title
    NULL,                   // window handle
    control_menu_items,     // list of menu items
    50,                     // max num of items to display at a time
    DEMOWIN_MNU_NONE,       // attribute flags?

    // user callbacks for misc menu operations
    NULL, NULL, NULL, NULL, 
    // private menu info members; do not touch
    0, 0, 0, 0, 0
}; 

DEMOWinMenuItem number_menu_items[] = 
{
    { "1",                          DEMOWIN_ITM_EOF,        MNU_pick_file,        NULL },
    { "2",                          DEMOWIN_ITM_EOF,        MNU_pick_file,        NULL },
    { "3",                          DEMOWIN_ITM_EOF,        MNU_pick_file,        NULL },
    { "4",                          DEMOWIN_ITM_EOF,        MNU_pick_file,        NULL },
    { "5",                          DEMOWIN_ITM_EOF,        MNU_pick_file,        NULL },
    { "6",                          DEMOWIN_ITM_EOF,        MNU_pick_file,        NULL },
    { "7",                          DEMOWIN_ITM_EOF,        MNU_pick_file,        NULL },
    { "8",                          DEMOWIN_ITM_EOF,        MNU_pick_file,        NULL },
    { "Cancel Menu",                DEMOWIN_ITM_EOF,        MNU_pick_cancel,      NULL },
    { "",                           DEMOWIN_ITM_TERMINATOR, NULL,                 NULL }
};

DEMOWinMenuInfo number_menu = 
{
   "Pick cancel number", // title
    NULL,                   // window handle
    number_menu_items,     // list of menu items
    MAX_READ_ISSUE+1,        // max num of items to display at a time
    DEMOWIN_MNU_NONE,       // attribute flags?

    // user callbacks for misc menu operations
    NULL, NULL, NULL, NULL, 
    // private menu info members; do not touch
    0, 0, 0, 0, 0
}; 

DEMOWinMenuInfo *control_menu_ptr;

/*---------------------------------------------------------------------------*
 * Debug and Status window stuff!
 *---------------------------------------------------------------------------*/
DEMOWinInfo *debug_win;     // debug window
DEMOWinInfo *status_win;    // status window
DEMOWinInfo *errmsg_win;    // error message window

 /*---------------------------------------------------------------------------*
    Name:               InitWindows
    
    Description:        Initialize windows
                                
    Arguments:          none
    
    Returns:            none
 *---------------------------------------------------------------------------*/
static void InitWindows(void)
{
    DEMOWinInfo *p;

    control_menu_ptr = DEMOWinCreateMenuWindow(&control_menu, 20, 20);
    p = control_menu_ptr->handle;

    // Intialize a window for showing status of DVD commands
    // By passing "__status_refresh" as the last argument, the window system 
    // calls "__status_refresh" once every frame. We use this function to
    // handle errors in this demo.
    status_win = DEMOWinCreateWindow((u16)(p->x2+5), p->y1, 620, (u16)(p->y1+100),
                                     "Status", 0, __status_refresh);

    // Initialize a window for debug output
    debug_win  = DEMOWinCreateWindow((u16)(p->x2+5), (u16)(p->y1+105), 620, 434,
                                     "Debug", 1024, NULL);

    // Initialize a window for showing error message
    errmsg_win = DEMOWinCreateWindow(120, 150, 520, 250,
                                     "Error message", 0, NULL);

    // Open status and debug windows. We don't open error message window until
    // we hit an error.
    DEMOWinOpenWindow(debug_win);
    DEMOWinOpenWindow(status_win);

    // Tell the pointer to the debug window to the "file select" system 
    InitSelectFile(debug_win);
}

 /*---------------------------------------------------------------------------*
    Name:               __status_refresh
    
    Description:        This is the error handling part. This function is called
                        once every frame.
                                
    Arguments:          handle              Window handle for the status window
    
    Returns:            none
 *---------------------------------------------------------------------------*/
static void __status_refresh(DEMOWinInfo *handle)
{
    static u32          counter;
    s32                 message;
    u16                 i;  
    MyCBData            *data;
    s32                 state;

    DEMOWinClearRow(handle, MAX_READ_ISSUE);
    // We use "user data" as the information of the command that was issued
    // last.
    for(i = 0; i < MAX_READ_ISSUE; i++)
    {
        // Clear status window 
        DEMOWinClearRow(handle, i);
        data = DVDGetUserData((DVDCommandBlock*)&FileInfo[i]);
        if(data->state != STATE_START)
        {
            state = DVDGetCommandBlockStatus((DVDCommandBlock*)&FileInfo[i]); 
            
            if(state == DVD_STATE_END)
            {
               DEMOWinPrintfXY(handle, 0,  i, "%d:%s %s:%s", i+1, 
                                    data->command != COMMAND_WRITE ? "Read ":"Write", 
                                    data->state   == STATE_WAITING ? "waiting":"finished",
                                    data->filename);
            }
            else if(state == DVD_STATE_BUSY)
            {
                DEMOWinPrintfXY(handle, 0,  i, "%d:%s:%3d%%(%d/%d)",i+1,  
                                data->command != COMMAND_WRITE ? "Read ":"Write",
                                100*DVDGetTransferredSize(&FileInfo[i])/data->Length,
                                DVDGetTransferredSize(&FileInfo[i]), data->Length);
            }
            else if(state == DVD_STATE_CANCELED)
            {
                DEMOWinPrintfXY(handle, 0,  i, "%d:%s Request canceled:%s", i+1, 
                                data->command != COMMAND_WRITE ? "Read ":"Write",
                                data->filename);
            }
            else if(state == DVD_STATE_WAITING)
            {
                DEMOWinPrintfXY(handle, 0,  i, "%d:%s Request queued:%s", i+1, 
                                data->command != COMMAND_WRITE ? "Read ":"Write",
                                data->filename);
            }
        }
    }
      
    message = -1;

    // This part is for debugging purpose. Show the drive's status on status
    // window.
    switch(DVDGetDriveStatus())
    {
      case DVD_STATE_END:
        //FileInfoIsInUse = FALSE;
        DEMOWinPrintfXY(handle, 0,  MAX_READ_ISSUE, "DVDGetDriveStatus:Command finished");
        break;
      case DVD_STATE_FATAL_ERROR:
        DEMOWinPrintfXY(handle, 0,  MAX_READ_ISSUE, "DVDGetDriveStatus:Fatal error occurred");
        message = MESSAGE_FATAL_ERROR;
        break;
      case DVD_STATE_BUSY:
        DEMOWinPrintfXY(handle, 0,  MAX_READ_ISSUE, "DVDGetDriveStatus:Command busy");
        break;
      case DVD_STATE_PAUSING:
        DEMOWinPrintfXY(handle, 0,  MAX_READ_ISSUE, "DVDGetDriveStatus:Pausing");
        break;
      case DVD_STATE_NO_DISK:
        DEMOWinPrintfXY(handle, 0,  MAX_READ_ISSUE, "DVDGetDriveStatus:No disk");
        message = MESSAGE_NO_DISK;
        break;
      case DVD_STATE_COVER_OPEN:
        DEMOWinPrintfXY(handle, 0,  MAX_READ_ISSUE, "DVDGetDriveStatus:Cover open");
        message = MESSAGE_COVER_OPEN;
        break;
      case DVD_STATE_WRONG_DISK:
        DEMOWinPrintfXY(handle, 0,  MAX_READ_ISSUE, "DVDGetDriveStatus:Wrong disk");
        message = MESSAGE_WRONG_DISK;
        break;
      case DVD_STATE_RETRY:
        DEMOWinPrintfXY(handle, 0,  MAX_READ_ISSUE, "DVDGetDriveStatus:Please retry");
        message = MESSAGE_RETRY_ERROR;
        break;
      case DVD_STATE_MOTOR_STOPPED:
        DEMOWinPrintfXY(handle, 0,  MAX_READ_ISSUE, "DVDGetDriveStatus:Ready to replace");
        message = MESSAGE_CHANGE_DISK;
        break;
    }

    // If necessary, launch error message window to show the error message to
    // the user.
    if (message >= 0)
    {
        DEMOWinOpenWindow(errmsg_win);
        counter = (counter+1) % 60;
        for (i = 0; i < 6; i++)
        {
            if (counter < 45)
            {
                DEMOWinPrintfXY(errmsg_win, 0, (u16)i, ErrorMessages[message].line[i]);
            }
            else
            {
                DEMOWinClearRow(errmsg_win, (u16)i);
            }
        }
    }
    else
    {
        DEMOWinCloseWindow(errmsg_win);
    }
    
    if (OSGetResetButtonState())
    {
        OSReport("reset is pressed\n");

        while (OSGetResetButtonState())
            ;
        
        OSReport("reset is released\n");

        OSResetSystem(OS_RESET_RESTART, 0, FALSE);
    }


} // end __status_refresh()

 /*---------------------------------------------------------------------------*
    Name:               MNU_pick_cancel
    
    Description:        Issue DVDCancel. This function is called when DVDCancel
                        is selected in the control window.
                                
    Arguments:          menu, item, result      unused
    
    Returns:            none
 *---------------------------------------------------------------------------*/
static void MNU_pick_cancel(DEMOWinMenuInfo *menu, u32 item, u32 *result)
{
#pragma unused(menu, item)
   *result = 0;
}

static void MNU_pick_file(DEMOWinMenuInfo *menu, u32 item, u32 *result)
{
#pragma unused (menu)
    *result = item + 1;
} // end __select_file

static u32 PickFile(DEMOWinInfo *handle)
{
    u32             val = 0;

    DEMOWinSetWindowColor(handle, DEMOWIN_COLOR_CAPTION,
                          50, 50, 50, 255);

    DEMOWinCreateMenuWindow(&number_menu, (u16)(handle->x2-10), (u16)(handle->y1+24));
    val = DEMOWinMenu(&number_menu);
    DEMOWinDestroyMenuWindow(&number_menu);

    DEMOWinSetWindowColor(handle, DEMOWIN_COLOR_RESET, 0, 0, 0, 0);
    
    return val;
}

 /*---------------------------------------------------------------------------*
    Name:               MNU_cancel
    
    Description:        Issue DVDCancel. This function is called when DVDCancel
                        is selected in the control window.
                                
    Arguments:          menu, item, result      unused
    
    Returns:            none
 *---------------------------------------------------------------------------*/
static void MNU_cancel(DEMOWinMenuInfo *menu, u32 item, u32 *result)
{
#pragma unused(menu, item, result)
    u32 n;
    
    n = PickFile(menu->handle);
    if(n)
    {
        DVDCancelAsync((DVDCommandBlock*)&FileInfo[n-1], NULL);
        DEMOWinLogPrintf(debug_win, "\nRead [%d] is canceled.\n", n);
    }
}

/*---------------------------------------------------------------------------*
    Name:               MNU_cancel_all
    
    Description:        Issue DVDCancel. This function is called when DVDCancel
                        is selected in the control window.
                                
    Arguments:          menu, item, result      unused
    
    Returns:            none
 *---------------------------------------------------------------------------*/
static void MNU_cancel_all(DEMOWinMenuInfo *menu, u32 item, u32 *result)
{
#pragma unused(menu, item, result)
    DVDCancelAllAsync(NULL);
    DEMOWinLogPrintf(debug_win, "\nAll read commands are canceled.\n");
}

/*---------------------------------------------------------------------------*
    Name:               MNU_pause
    
    Description:        Issue DVDCancel. This function is called when DVDCancel
                        is selected in the control window.
                                
    Arguments:          menu, item, result      unused
    
    Returns:            none
 *---------------------------------------------------------------------------*/
static void MNU_pause(DEMOWinMenuInfo *menu, u32 item, u32 *result)
{
#pragma unused(menu, item, result)
    DVDPause();
    DemoPauseFlag = TRUE;
    DEMOWinLogPrintf(debug_win, "\nRead commands are paused.\n");
}

/*---------------------------------------------------------------------------*
    Name:               MNU_resume
    
    Description:        Issue DVDCancel. This function is called when DVDCancel
                        is selected in the control window.
                                
    Arguments:          menu, item, result      unused
    
    Returns:            none
 *---------------------------------------------------------------------------*/
static void MNU_resume(DEMOWinMenuInfo *menu, u32 item, u32 *result)
{
#pragma unused(menu, item, result)
    DVDResume();
    DemoPauseFlag = FALSE;
    DEMOWinLogPrintf(debug_win, "\nRead commands are resumed.\n");
}

/*---------------------------------------------------------------------------*
    Name:               MNU_dump_waiting_queue
    
    Description:        Issue DVDCancel. This function is called when DVDCancel
                        is selected in the control window.
                                
    Arguments:          menu, item, result      unused
    
    Returns:            none
 *---------------------------------------------------------------------------*/
static void MNU_dump_waiting_queue(DEMOWinMenuInfo *menu, u32 item, u32 *result)
{
#pragma unused(menu, item, result)
    DVDDumpWaitingQueue();
    DEMOWinLogPrintf(debug_win, "\nDumped the status of waiting queue.\n");
}

static void readCallback(s32 result, DVDFileInfo* fileInfo)
{
#pragma unused(fileInfo, result)
    MyCBData*   data;

    data = DVDGetUserData((DVDCommandBlock*)fileInfo);
    data->state = STATE_END;
    if(data->command == COMMAND_READ)
    {
        OSFree(data->buffer);
    }
}

#ifdef DVDETH
static void writeCallback(s32 result, DVDFileInfo* fileInfo)
{
#pragma unused(fileInfo, result)
    MyCBData*   data;

    data = DVDGetUserData((DVDCommandBlock*)fileInfo);
    data->state = STATE_END;
    OSFree(data->buffer);
}
#endif

 /*---------------------------------------------------------------------------*
    Name:               MNU_read
    
    Description:        Issue DVDRead, after letting the user to select a file
                        to read.
                                
    Arguments:          menu                Winmenuinfo for control window
                        item, result        unused
    
    Returns:            none
 *---------------------------------------------------------------------------*/
void MNU_read(DEMOWinMenuInfo *menu, u32 item, u32 *result)
{
#pragma unused(item)
    u32                 i;
    MyCBData*           data;
    
    *result = MENU_OPEN_ALL;
    for(i = 0; i < MAX_READ_ISSUE; i++)
    {            
        data = DVDGetUserData((DVDCommandBlock*)&FileInfo[i]);
        if (data->state != STATE_WAITING)
        {        
            DVDOpen(menu->title, &FileInfo[i]);
            
            data->Length  = (s32)OSRoundUp32B(DVDGetLength(&FileInfo[i]));

            if (data->Length  > 0)
            {
                if((data->buffer = OSAlloc((u32)data->Length)) == NULL)
                {
                    DEMOWinLogPrintf(debug_win, "\nUnable to allocate\n");
                    return;
                }
            }
            else
            {
                if( (data->buffer = OSAlloc( OSRoundUp32B(1) ) ) == NULL)
                {
                    DEMOWinLogPrintf(debug_win, "\nUnable to allocate\n");
                    return;
                }
            }
            data->command = COMMAND_READ;
            strcpy(data->filename, menu->title);
            data->state   = STATE_WAITING;
            return;               
        }
    }

    DEMOWinLogPrintf(debug_win, "\nRead queue is full\n");
} // MNU_read

#ifdef DVDETH
/*---------------------------------------------------------------------------*
    Name:               MNU_copy
    
    Description:        Issue DVDRead, after letting the user to select a file
                        to read.
                                
    Arguments:          menu                Winmenuinfo for control window
                        item, result        unused
    
    Returns:            none
 *---------------------------------------------------------------------------*/
void MNU_copy(DEMOWinMenuInfo *menu, u32 item, u32 *result)
{
#pragma unused (item)

    char        filename[256];
    u32         i;
    MyCBData*   dataR;
    MyCBData*   dataW;

    if (DemoPauseFlag)
    {
        DEMOWinLogPrintf(debug_win, "\nDVDPause is selected, unable to issue DVDCreate.\nIssue DVDResume .\n");
        return;
    }

    sprintf(filename, "copy_%s", menu->title);
    *result = MENU_OPEN_ALL;

//    DEMOWinLogPrintf(debug_win, "\nDVDCreate:%s is created\n", 
//                         filename);
    for(i = 0; i < MAX_READ_ISSUE - 1; i++)
    {            
        dataR = DVDGetUserData((DVDCommandBlock*)&FileInfo[i]);
        dataW = DVDGetUserData((DVDCommandBlock*)&FileInfo[i+1]);
        if (dataR->state != STATE_WAITING  &&
            dataW->state != STATE_WAITING)
        {
            DVDOpen(menu->title, &FileInfo[i]);
            if (DVDGetLength(&FileInfo[i]) == 0)
            {
//                DEMOWinLogPrintf(debug_win, "\nDVDCreate:Unable to create %s, the file size is zero, \n", 
//                     filename);
                DEMOWinLogPrintf(debug_win, "\nUnable to copy zero-size file. \n", 
                     filename);
                return;
            }

            if(!DVDCreate(filename, &FileInfo[i+1]))
            {
                DEMOWinLogPrintf(debug_win, "\nDVDCreate:Unable to create %s\n", 
                     filename);
                return;
            }
            
            DEMOWinLogPrintf(debug_win, "\nDVDCreate:%s is created\n", 
                                     filename);

            dataR->Length  = (s32)OSRoundUp32B(DVDGetLength(&FileInfo[i]));
            dataW->Length  = (s32)DVDGetLength(&FileInfo[i]);
            if((dataR->buffer = OSAlloc((u32)dataR->Length)) == NULL)
            {
                dataR->state = STATE_END;
                DEMOWinLogPrintf(debug_win, "\nUnable to allocate\n");
                return;
            }
            strcpy(dataR->filename, menu->title);
            strcpy(dataW->filename, filename);
            dataR->state   = STATE_WAITING;
            dataW->state   = STATE_WAITING;
            dataR->command = COMMAND_COPY;
            dataW->command = COMMAND_WRITE;
            dataW->buffer = dataR->buffer;
            return;
        }
    }

    DEMOWinLogPrintf(debug_win, "\nRead queue is full\n");
} // MNU_create

void MNU_remove(DEMOWinMenuInfo *menu, u32 item, u32 *result)
{
#pragma unused (item)

    BOOL      ret;
    u32       i;
    MyCBData* data;

    if (DemoPauseFlag)
    {
        DEMOWinLogPrintf(debug_win, "\nDVDPause is selected, unable to issue DVDRemove.\nIssue DVDResume .\n");
        return;
    }

    *result = MENU_OPEN_ALL;
    for(i = 0; i < MAX_READ_ISSUE; i++)
    {            
        data = DVDGetUserData((DVDCommandBlock*)&FileInfo[i]);
        if (data->state == STATE_WAITING)
        {
            if(!strcmp(data->filename, menu->title))
            {
                DEMOWinLogPrintf(debug_win, "\n%s command is not issued yet.\n", 
                    data->command == COMMAND_WRITE ? "Write": "Read");
                return;
            }
        }
    }

    ret = DVDRemove(menu->title, NULL);
    if(ret)
    {
        DEMOWinLogPrintf(debug_win, "\nDVDRemove:%s is removed\n", 
                         menu->title);
    }
    else
    {
        DEMOWinLogPrintf(debug_win, "\nDVDRemove:Unable to remove %s\n", 
                         menu->title);
    }
    

} // MNU_remove
#endif

 /*---------------------------------------------------------------------------*
    Name:               MNU_open
    
    Description:        Issue DVDRead, after letting the user to select a file
                        to read.
                                
    Arguments:          menu                Winmenuinfo for control window
                        item, result        unused
    
    Returns:            none
 *---------------------------------------------------------------------------*/
static void MNU_open(DEMOWinMenuInfo *menu, u32 item, u32 *result)
{
#pragma unused(item, result)

    u32                 i;
    MyCBData*           data;

    // Avoid using the same FileInfo/CommandBlock at the same time. 
    for(i = 0; i < MAX_READ_ISSUE; i++)
    {
        data = DVDGetUserData((DVDCommandBlock*)&FileInfo[i]);
        if(data->state != STATE_START)
        {
            if(DVD_STATE_BUSY == DVDGetCommandBlockStatus((DVDCommandBlock*)&FileInfo[i]))
            {
                DEMOWinLogPrintf(debug_win, "\nAll read commands are not finished yet.\n");
                return;  
            }
        }
    }

    DEMOWinLogPrintf(debug_win, "\nPress A to choose a file to read or change directory.\nPress B to exit.\n");

    // Launch a window to let the user to select a file. 
    // The return value is the pointer to the selected file.
    SelectFile(menu->handle);
}

 /*---------------------------------------------------------------------------*
    Name:               MNU_read_start
    
    Description:        Issue DVDRead, after letting the user to select a file
                        to read.
                                
    Arguments:          menu                Winmenuinfo for control window
                        item, result        unused
    
    Returns:            none
 *---------------------------------------------------------------------------*/
static void MNU_read_start(DEMOWinMenuInfo *menu, u32 item, u32 *result)
{
#pragma unused(menu, item, result)

    u32                 i;
    MyCBData*           data;

    // Avoid using the same FileInfo/CommandBlock at the same time. 
    for(i = 0; i < MAX_READ_ISSUE; i++)
    {
        data = DVDGetUserData((DVDCommandBlock*)&FileInfo[i]);
        if(data->state != STATE_START)
        {
            if(DVD_STATE_BUSY == DVDGetCommandBlockStatus((DVDCommandBlock*)&FileInfo[i]))
            {
                DEMOWinLogPrintf(debug_win, "\nAll read commands are not finished yet.\n");
                return;  
            }
        }
    }

    for(i = 0; i < MAX_READ_ISSUE; i++)
    {            
        data = DVDGetUserData((DVDCommandBlock*)&FileInfo[i]);
        if(data->state == STATE_WAITING)
        {
            if(data->command != COMMAND_WRITE)
            {
                DVDReadAsync(&FileInfo[i], data->buffer, data->Length, 0, readCallback);
            }
#ifdef DVDETH
            else
            {
                DVDWriteAsync(&FileInfo[i], data->buffer, data->Length, 0, writeCallback);
            }
#endif
        }
    }
}

 /*---------------------------------------------------------------------------*
    Name:               Run_Demo
    
    Description:        Main loop of the demo.
                                
    Arguments:          none
    
    Returns:            none
 *---------------------------------------------------------------------------*/
static void Run_Demo(void)
{
    DEMOWinPadInfo pad;

    DEMOWinLogPrintf(debug_win, "-------------------------------\n");
    DEMOWinLogPrintf(debug_win, "DVD error handling sample\n");
    DEMOWinLogPrintf(debug_win, "-------------------------------\n");
    DEMOWinLogPrintf(debug_win, "- Stick Up/Down to move cursor\n");
    DEMOWinLogPrintf(debug_win, "- Button A to select an item\n");
    DEMOWinLogPrintf(debug_win, "- Button B to exit a menu\n");
    DEMOWinLogPrintf(debug_win, "While you are executing a command, open cover, make no disk,\n");
    DEMOWinLogPrintf(debug_win, "put other disks to see how to handle errors\n");
    
    while(1)
    {
        // Launch control window. This funcion returns when B is pressed.
        DEMOWinMenu(control_menu_ptr);

        DEMOWinLogPrintf(debug_win, "-------------------------------------------\n");
        DEMOWinLogPrintf(debug_win, "\nUse Stick Up/Down to scroll debug buffer.\n");

        DEMOBeforeRender();
        DEMOWinRefresh();
        DEMODoneRender();
        DEMOWinPadRead(&pad);

        // Let the user to scroll the debug window. Press start button to 
        // go to the top of the outer loop and open the control window again.
        while(1)
        {
            
            DEMOWinPadRead(&pad);

            if (pad.pads[0].stickY < -50)
            {
                DEMOWinScrollWindow(debug_win, DEMOWIN_SCROLL_DOWN);
                if (pad.pads[0].triggerLeft > 150)
                {
                    DEMOWinScrollWindow(debug_win, DEMOWIN_SCROLL_DOWN);
                    DEMOWinScrollWindow(debug_win, DEMOWIN_SCROLL_DOWN);
                }
            }
            else if (pad.pads[0].stickY > 50)
            {
                DEMOWinScrollWindow(debug_win, DEMOWIN_SCROLL_UP);
                if (pad.pads[0].triggerLeft > 150)
                {
                    DEMOWinScrollWindow(debug_win, DEMOWIN_SCROLL_UP);
                    DEMOWinScrollWindow(debug_win, DEMOWIN_SCROLL_UP);
                }
                
            }
            else if (pad.changed_button[0] & PAD_BUTTON_START)
            {
                DEMOWinScrollWindow(debug_win, DEMOWIN_SCROLL_HOME);
                // get out of the inner loop
                break;
            }

            DEMOBeforeRender();
            DEMOWinRefresh();
            DEMODoneRender();


        } // debug buffer scroll loop

    } // forever loop -> break if B button is pressed.

EXIT:
    return;


} // end Init_Player_Windows()



void main(void)
{
    u32             i;
    
#ifdef DVDETH
    char*       fstBuffer;         // pointer to the FST buffer
#endif

    for(i = 0; i < MAX_READ_ISSUE; i++)
    {
        CBData[i].state = STATE_START;
        DVDSetUserData((DVDCommandBlock*)&FileInfo[i], (void*)&CBData[i]);
    }

    DEMOInit(NULL);

#ifdef DVDETH
    // Initialize GC IP address
    DVDEthInit(GCClientAddr, Netmask, Gateway);

    // Initialize DVD
    DVDLowInit(DvdServerAddr, DvdServerPort);
#endif
     // Initialize OSReport();
    OSReportInit(ReportServerAddr, ReportServerPort);

#ifdef DVDETH
    // Allocate a buffer to load the fst
    fstBuffer = OSAlloc(FST_MAX_SIZE);

    // Initialize FST
    if (!DVDFstInit(fstBuffer, FST_MAX_SIZE))
    {
        OSHalt("Cannot initialize FST");
    }
#endif

    DVDSetAutoFatalMessaging(TRUE);

    OSReport("Disk number is %x\n", DVDGetCurrentDiskID()->diskNumber);

    AIInit(NULL);

    OSReport("AIInit done\n");

    DEMOWinInit();
    
    OSReport("DEMOWinInit done\n");

    InitWindows();

    OSReport("InitWindows done\n");

    Run_Demo();

#ifdef DVDETH
    DVDEthShutdown();
#endif

} // end main