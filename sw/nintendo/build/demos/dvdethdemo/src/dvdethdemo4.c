/*---------------------------------------------------------------------------*
  Project:  Dolphin DVD Demo4
  File:     dvddemo4.c

  Copyright 2002 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: dvdethdemo4.c,v $
  Revision 1.1.1.1  2004/03/02 05:31:13  paulm
  Socket Library 24-Sep-2003

    
    13    03/09/05 8:56a Ueno_kyu
    
    12    03/09/02 4:23p Ueno_kyu
    
    3rd release version 03/09/02 3:00p

    11    03/03/24 3:57p Ooshima
    Fixed a spelling mistake

    10    03/03/20 11:03a Ooshima
    Modified to move ip address setting to ipaddress.h
    
    9     03/02/28 5:26p Ooshima
    2nd release version
    
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*
  This program shows how to use asynchronous write function
 *---------------------------------------------------------------------------*/

#include <demo.h>
#include "osreport.h"
#include "ipaddress.h"
#ifdef DVDETH
#include <dolphin/dvdeth.h>
#endif

#define    NumOf(x)    (sizeof(x)/sizeof((x)[0]))

#ifdef DVDETH
#define FST_MAX_SIZE 1024 * 1024
#endif

void        main    ( void );
static void readcallback(s32 result, DVDFileInfo* fileInfo);
static void writecallback(s32 result, DVDFileInfo* fileInfo);

// The name of the file we are going to read.
static const char *pFileName[] =
{
    "gxTests/dnt-02.tpl",
    "gxTests/dnt-03.tpl",
    "gxTests/frb-00.tpl",
    "gxTests/g2d-00.tpl",
    "gxTests/geo-00.tpl",
    "gxTests/lit-06.tpl",
    "gxTests/lit-10.tpl",
    "gxTests/pseudo.tpl",
    "gxTests/tev-00.tpl",
    "gxTests/tev-01.tpl",
    "gxTests/tev-02.tpl",
    "gxTests/tev-03.tpl",
    "gxTests/tev-04.tpl",
    "gxTests/tex-01.tpl",
    "gxTests/tex-03.tpl",
    "gxTests/tex-05.tpl",
    "gxTests/tex-06.tpl",
    "gxTests/tex-07.tpl",
    "gxTests/tex-08.tpl",
    "gxTests/tf-02.tpl",
    "gxTests/tg-01.tpl",
    "gxTests/tg-02.tpl",
    "gxTests/tg-cube.tpl",
    "gxTests/tg-cube1.tpl",
    "gxTests/tg-dual.tpl",
    "gxTests/tg-pc.tpl"
};

// This variable should be declared "volatile" because this is shared
// by two contexts.
volatile s32     readDone[NumOf(pFileName)] = {0};
volatile s32     writeDone[NumOf(pFileName)] = {0};

DVDFileInfo fileInfo[NumOf(pFileName)];

 /*---------------------------------------------------------------------------*
    Name:               writecallback
    
    Description:        callback function for DVDWriteAsync
                                
    Arguments:          result     -1 if read fails, file size if succeeded.
                        fileInfo   fileInfo for the file transferred.
    
    Returns:            none
 *---------------------------------------------------------------------------*/
static void writecallback(s32 result, DVDFileInfo* pfileInfo)
{
    u32 j = 0;

    for(j = 0; j < NumOf(pFileName); j++)
    {
        if(pfileInfo == &fileInfo[j])
            break;
    }

    if (result == -1)
    {
        writeDone[j] = -1;
    }
    else
    {
        writeDone[j] = result;
    }
    return;
}


 /*---------------------------------------------------------------------------*
    Name:               readcallback
    
    Description:        callback function for DVDReadAsync
                                
    Arguments:          result     -1 if read fails, file size if succeeded.
                        fileInfo   fileInfo for the file transferred.
    
    Returns:            none
 *---------------------------------------------------------------------------*/
static void readcallback(s32 result, DVDFileInfo* pfileInfo)
{
    u32 j = 0;

    for(j = 0; j < NumOf(pFileName); j++)
    {
        if(pfileInfo == &fileInfo[j])
            break;
    }

    if (result == -1)
    {
        readDone[j] = -1;
    }
    else
    {
        readDone[j] = result;
    }
    return;
}



void main(void)
{
    u32     fileSize[NumOf(pFileName)];
    char*   buffer[NumOf(pFileName)];         // pointer to the buffer
    u32     j;

#ifdef DVDETH
    char*       fstBuffer;         // pointer to the FST buffer
#endif

    DEMOInit(NULL);

#ifdef DVDETH
        // Initialize Network
    DVDEthInit(GCClientAddr, Netmask, Gateway);

    // Initialize DVD
    DVDLowInit(DvdServerAddr, DvdServerPort);
#endif

    // Initialize OSReport();
    OSReportInit(ReportServerAddr, ReportServerPort);

#ifdef DVDETH
    // Allocate a buffer to load the fst
    fstBuffer = OSAlloc(FST_MAX_SIZE);

    // Initialize FST
    if (!DVDFstInit(fstBuffer, FST_MAX_SIZE))
    {
        OSHalt("Cannot initialize FST");
    }
#endif

    DVDSetAutoFatalMessaging(TRUE);

    // Open file
    for( j = 0 ; j < NumOf(pFileName) ; j++)
    {
        // We MUST open the file before accessing the file
        if (FALSE == DVDOpen(pFileName[j], &fileInfo[j]))
        {
            OSHalt("Cannot open file");
        }

        // Get the size of the file
        fileSize[j] = DVDGetLength(&fileInfo[j]);

        // Allocate a buffer to read the file.
        // Note that pointers returned by OSAlloc are always 32byte aligned.
        buffer[j] = OSAlloc(OSRoundUp32B(fileSize[j]));
    }

    OSReport("All files were openend.\n");

    // Start Reading 
    for( j = 0 ; j < NumOf(pFileName) ; j++)
    {
        // This function only start reading the file.
        // The read keeps going in the background after the function returns
 
        if (FALSE == DVDReadAsync(&fileInfo[j], (void*)buffer[j],
                      (s32)OSRoundUp32B(fileSize[j]), 0, readcallback))
        {
            OSHalt("Error occurred when issuing read");
        }
    }

    OSReport("All Read commands were issued.\n");  

    // Waiting for Read done.
    for( j = 0 ; j < NumOf(pFileName) ; j++)
    {
        while (1) 
        {    
            if(readDone[j] > 0)
            {    
                break;
            }
            else if(readDone[j] < 0)
            {
                OSHalt("Error occurred when reading file");
            }
        }
        DVDClose(&fileInfo[j]);
    }

    OSReport("All Read commands were completed.\n");

#ifdef DVDETH
    // Create File
    for( j = 0 ; j < NumOf(pFileName) ; j++)
    {
        char filename[256];
        sprintf(filename, "copy/%s", pFileName[j]);
        if (FALSE == DVDCreate(filename, &fileInfo[j]))
        {
            OSHalt("Cannot create file");
        }
    }

    OSReport("New Files were created.\n");

    // Start writing 
    for( j = 0 ; j < NumOf(pFileName) ; j++)
    {
        if (FALSE == DVDWriteAsync(&fileInfo[j], (void*)buffer[j],
                      (s32)fileSize[j], 0, writecallback))
        {
            OSHalt("Error occurred when issuing read");
        }
    } 
    
    OSReport("All Write commands were issued.\n");
    // Waiting for Write done.
    for( j = 0 ; j < NumOf(pFileName) ; j++)
    {
        while (1) 
        {    
            if(writeDone[j] > 0)
            {    
                break;
            }
            else if(writeDone[j] < 0)
            {
                OSHalt("Error occurred when writing file");
            }
        }
        DVDClose(&fileInfo[j]);
        OSFree(buffer[j]);
    }
    OSReport("All Write commands were completed.\n");
#endif

#ifdef DVDETH
    DVDEthShutdown();
#endif

    OSHalt("End of demo");


}
