/*---------------------------------------------------------------------------*
  Project:  File selection window system for Dolphin DVD error handling demo
  File:     selectfile2.c

  Copyright 2003 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: selectfile2.c,v $
  Revision 1.1.1.1  2004/03/02 05:31:13  paulm
  Socket Library 24-Sep-2003

    
    2     03/09/02 4:23p Ueno_kyu
    
    1     03/02/28 5:26p Ooshima
    2nd release version
    
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#include <demo.h>
#include <dolphin.h>
#include <demo/DEMOWin.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "selectfile2.h"
#ifdef DVDETH
#include <dolphin/dvdeth.h>
#endif
/*---------------------------------------------------------------------------*
 * Directory listing stuff!
 *---------------------------------------------------------------------------*/
#define MAX_DIR_ENTRIES     512
#define MAX_FILENAME_LENGTH 128

static DEMOWinMenuItem dir_entry[MAX_DIR_ENTRIES+1];
static DEMOWinMenuInfo dir_list;

static char dir_entry_name[MAX_DIR_ENTRIES][MAX_FILENAME_LENGTH];

static void MNU_select_file(DEMOWinMenuInfo *menu, u32 item, u32 *result);

static void MNU_change_dir_up(DEMOWinMenuInfo *menu, u32 item, u32 *result);
static void MNU_select_dir_menu(DEMOWinMenuInfo *menu, u32 item, u32 *result);

static void MNU_open_dir(DEMOWinMenuInfo *menu, u32 item, u32 *result);
static void MNU_read_dir(DEMOWinMenuInfo *menu, u32 item, u32 *result);
static void MNU_tell_dir(DEMOWinMenuInfo *menu, u32 item, u32 *result);
static void MNU_seek_dir(DEMOWinMenuInfo *menu, u32 item, u32 *result);
static void MNU_rewind_dir(DEMOWinMenuInfo *menu, u32 item, u32 *result);
static void MNU_get_current_dir(DEMOWinMenuInfo *menu, u32 item, u32 *result);
static void MNU_cancel(DEMOWinMenuInfo *menu, u32 item, u32 *result);
static void MNU_convert(DEMOWinMenuInfo *menu, u32 item, u32 *result);
static void MNU_file_size(DEMOWinMenuInfo *menu, u32 item, u32 *result);

#ifdef DVDETH
static void MNU_fst_refresh(DEMOWinMenuInfo *menu, u32 item, u32 *result);
#endif

static u32 PickLocation(DEMOWinInfo *handle);
static u32 SelectDirMenu(DEMOWinInfo *handle, char* name);

static DEMOWinMenuInfo *__create_dir_menu(void);
static DEMOWinMenuInfo *__create_location_menu(void);

static char toupper(char c);
static BOOL compare_strings(char *s1, char *s2);

static DEMOWinInfo          *Debug_win;

#define MAX_LOCATION_LENGTH      16
#define MAX_LOCATION_ENTRIES     512

static DEMOWinMenuItem location_entry[MAX_LOCATION_ENTRIES+1];
static DEMOWinMenuInfo location_list;

static DVDDir      menu_dir;
static char location_entry_name[MAX_LOCATION_ENTRIES][MAX_LOCATION_LENGTH];

#define    NumOf(x)    (sizeof(x)/sizeof((x)[0]))

static DEMOWinMenuItem dir_menu_items[] = 
{
    { "Open",                       DEMOWIN_ITM_EOF,        MNU_open_dir,           NULL },
    { "DVDReadDir",                 DEMOWIN_ITM_EOF,        MNU_read_dir,           NULL },
    { "DVDTellDir",                 DEMOWIN_ITM_EOF,        MNU_tell_dir,           NULL },
    { "DVDSeekDir",                 DEMOWIN_ITM_EOF,        MNU_seek_dir,           NULL },
    { "DVDRewindDir",               DEMOWIN_ITM_EOF,        MNU_rewind_dir,         NULL },
    { "DVDGetCurrentDir",           DEMOWIN_ITM_EOF,        MNU_get_current_dir,    NULL },
    { "DVDConvertPath",             DEMOWIN_ITM_EOF,        MNU_convert,            NULL },
#ifdef DVDETH
    { "DVDFstRefresh",              DEMOWIN_ITM_EOF,        MNU_fst_refresh,        NULL },
#endif
    { "Cancel Menu",                DEMOWIN_ITM_EOF,        MNU_cancel,             NULL },
    { "",                           DEMOWIN_ITM_TERMINATOR, NULL,                   NULL }
};

static DEMOWinMenuItem file_menu_items[] = 
{
    { "DVDRead",                    DEMOWIN_ITM_EOF,        MNU_read,               NULL },
    { "File Size",                  DEMOWIN_ITM_EOF,        MNU_file_size,          NULL },
    { "DVDConvertPath",             DEMOWIN_ITM_EOF,        MNU_convert,            NULL },
    { "DVDGetCurrentDir",           DEMOWIN_ITM_EOF,        MNU_get_current_dir,    NULL },
#ifdef DVDETH
    { "Copy",                       DEMOWIN_ITM_EOF,        MNU_copy,               NULL },
    { "DVDRemove",                  DEMOWIN_ITM_EOF,        MNU_remove,             NULL },
    { "DVDFstRefresh",              DEMOWIN_ITM_EOF,        MNU_fst_refresh,        NULL },
#endif
    { "Cancel Menu",                DEMOWIN_ITM_EOF,        MNU_cancel,             NULL },
    { "",                           DEMOWIN_ITM_TERMINATOR, NULL,                   NULL }
};

static DEMOWinMenuInfo dir_menu = 
{
    NULL,                   // title
    NULL,                   // window handle
    dir_menu_items,         // list of menu items
    NumOf(dir_menu_items),  // max num of items to display at a time
    DEMOWIN_MNU_NONE,       // attribute flags?

    // user callbacks for misc menu operations
    NULL, NULL, NULL, NULL, 
    // private menu info members; do not touch
    0, 0, 0, 0, 0
}; 

static DEMOWinMenuInfo file_menu = 
{
    NULL,                   // title
    NULL,                   // window handle
    file_menu_items,        // list of menu items
    NumOf(file_menu_items), // max num of items to display at a time
    DEMOWIN_MNU_NONE,       // attribute flags?

    // user callbacks for misc menu operations
    NULL, NULL, NULL, NULL, 
    // private menu info members; do not touch
    0, 0, 0, 0, 0
}; 

void InitSelectFile(DEMOWinInfo *debug_win)
{
    Debug_win = debug_win;
}


static char toupper(char c)
{
    if ((c >= 'a') && (c <= 'z'))
        return((char)(c-32));
    else
        return(c);
}


static BOOL compare_strings(char *s1, char *s2)
{
    u32 i;

    for (i=0; i<strlen(s1); i++)
    {
        if (toupper(s1[i]) != toupper(s2[i]))
            return(FALSE);
    }
    return(TRUE);
}

static u32 SelectFileMenu(DEMOWinInfo *handle, char *name)
{
    u32             val = 0;

    DEMOWinSetWindowColor(handle, DEMOWIN_COLOR_CAPTION,
                          50, 50, 50, 255);

    file_menu.title = name;
    do
    {
        DEMOWinCreateMenuWindow(&file_menu, (u16)(handle->x2-30), (u16)(handle->y1+24));
        val = DEMOWinMenu(&file_menu);
        DEMOWinDestroyMenuWindow(&file_menu);
    } while (val == MENU_SUSPEND );

    DEMOWinSetWindowColor(handle, DEMOWIN_COLOR_RESET, 0, 0, 0, 0);
    
    return val;
}

static void MNU_select_file(DEMOWinMenuInfo *menu, u32 item, u32 *result)
{
#pragma unused(menu, item)
    u32 menu_result;
    
    menu_result = SelectFileMenu(menu->handle, menu->items[item].name);
    *result = MENU_OPEN_ALL;
    
} // end __select_file

static void MNU_change_dir_up(DEMOWinMenuInfo *menu, u32 item, u32 *result)
{
#pragma unused(menu, item)

    menu->items[item].flags |= DEMOWIN_ITM_EOF;

    DEMOWinLogPrintf(Debug_win, "\nChanging to parent directory...\n");

    DVDChangeDir("..");

    *result = MENU_OPEN_ALL;

} // MNU_change_dir_up

static DEMOWinMenuInfo *__create_file_list(void)
{
    DVDDir      dir;
    DVDDirEntry dirent;

    u32         index;
    u32         len;

    index = 0;

    // first entry is always parent directory
    strcpy(dir_entry_name[index], "../");

    dir_entry[index].name     = &dir_entry_name[index][0];
    dir_entry[index].flags    = DEMOWIN_ITM_EOF;
    dir_entry[index].function = MNU_change_dir_up;
    dir_entry[index].link     = NULL;

    index++;

    DVDOpenDir(".", &dir);

    while ( (DVDReadDir(&dir, &dirent)) && (index < MAX_DIR_ENTRIES))
    {
        // get name, append '/' if it's a directory
        strcpy(dir_entry_name[index], dirent.name);
        if (dirent.isDir)
        {
            len = strlen(dirent.name);
            dir_entry_name[index][len]   = '/';
            dir_entry_name[index][len+1] = 0;

            dir_entry[index].function = MNU_select_dir_menu;
            dir_entry[index].flags    = DEMOWIN_ITM_EOF;
        }
        else
        {
            dir_entry[index].function = MNU_select_file;
            dir_entry[index].flags    = DEMOWIN_ITM_EOF;
        }

        dir_entry[index].name  = &dir_entry_name[index][0];
        dir_entry[index].link  = NULL;
        index++;
    }

    // terminate list
    dir_entry[index].flags    = DEMOWIN_ITM_TERMINATOR;
    dir_entry[index].function = NULL;
    dir_entry[index].link     = NULL;


    // Initialize Menu info structure

    dir_list.title  = "Directory Listing";  // later, init with directory name
    dir_list.handle = NULL;                 // placeholder for window handle
    dir_list.items  = &dir_entry[0];        // list of items

    dir_list.max_display_items = 24;        // display 22 files at a time
    dir_list.flags             = 0;         // no flags
    
    dir_list.cb_open   = NULL;              // no callbacks
    dir_list.cb_move   = NULL;
    dir_list.cb_select = NULL;
    dir_list.cb_cancel = NULL;

    // private members; initialize to zero, they will be calculated at runtime
    dir_list.num_display_items = 0;
    dir_list.num_items         = 0;  
    dir_list.max_str_len       = 0;
    dir_list.curr_pos          = 0;   
    dir_list.display_pos       = 0;

    DVDCloseDir(&dir);

    if (index)
    {
        return(&dir_list);
    }
    else
    {
        return(NULL);
    }


} // end __create_file_list()

char* SelectFile(DEMOWinInfo *handle)
{
    DEMOWinMenuInfo *m; // file list
    u32             val = 0;

    DEMOWinSetWindowColor(handle, DEMOWIN_COLOR_CAPTION,
                          50, 50, 50, 255);
    do 
    {
        m = __create_file_list();
        if (m)
        {      
            DEMOWinCreateMenuWindow(m, (u16)(handle->x1+30), (u16)(handle->y1+24));
            val = DEMOWinMenu(m);
            DEMOWinDestroyMenuWindow(m);
        }
        else
        {
            DEMOWinLogPrintf(Debug_win, "Failed to create directory list!\n");
        }
    } while (val != MENU_CANCEL);
    // validate B Button cancel.

    DEMOWinSetWindowColor(handle, DEMOWIN_COLOR_RESET, 0, 0, 0, 0);
    
    return (char*)val;
}

static u32 SelectDirMenu(DEMOWinInfo *handle, char *name)
{
    u32             val = 0;

    DEMOWinSetWindowColor(handle, DEMOWIN_COLOR_CAPTION,
                          50, 50, 50, 255);

    dir_menu.title = name;
    do
    {
        DEMOWinCreateMenuWindow(&dir_menu, (u16)(handle->x2-30), (u16)(handle->y1+24));
        val = DEMOWinMenu(&dir_menu);
        DEMOWinDestroyMenuWindow(&dir_menu);
    } while (val == MENU_SUSPEND);

    DEMOWinSetWindowColor(handle, DEMOWIN_COLOR_RESET, 0, 0, 0, 0);
    
    return val;
}

static void MNU_select_dir_menu(DEMOWinMenuInfo *menu, u32 item, u32 *result)
{
#pragma unused(result, item)

    u32 menu_result;

    DVDOpenDir(menu->items[item].name, &menu_dir);
 
    menu_result = SelectDirMenu(menu->handle, menu->items[item].name);
    if(MENU_OPEN_SUBDIR== menu_result)
    {
        DVDDir      dir;
        DVDDirEntry dirent;
        u32 i;

        DEMOWinLogPrintf(Debug_win, "\nChanging directory to: '%s'...\n", menu->items[item].name);

        DVDOpenDir(".", &dir);
        for (i=1; i<=item; i++)
        {
            DVDReadDir(&dir, &dirent);
        }
        if (dirent.isDir)
        {
            DVDChangeDir(dirent.name);
            menu->items[item].flags |= DEMOWIN_ITM_EOF;
        }
        else
        {
            DEMOWinLogPrintf(Debug_win, "\nUnable to change directory...??\n");
            menu->items[item].flags &= ~DEMOWIN_ITM_EOF;
        }

        DVDCloseDir(&dir);
    }
    
    *result = MENU_OPEN_ALL;
        
} // MNU_select_dir_menu

static void MNU_open_dir(DEMOWinMenuInfo *menu, u32 item, u32 *result)
{
#pragma unused (menu)
#pragma unused (item)

    *result = MENU_OPEN_SUBDIR;

} // MNU_open_dir

static void MNU_read_dir(DEMOWinMenuInfo *menu, u32 item, u32 *result)
{
#pragma unused (menu)
#pragma unused (item)
    DVDDirEntry dirent;

    if(DVDReadDir(&menu_dir, &dirent))
    {
       DEMOWinLogPrintf(Debug_win, "\nDVDReadDir:%s%4d:%s\n", 
                     DVDDirEntryIsDir(&dirent) ? "D:":"F:",
                     dirent.entryNum,
                     DVDGetDirEntryName(&dirent));
    }
    else
    {
        DEMOWinLogPrintf(Debug_win, "\nReached the end of the directory\n");
    }
    *result = MENU_SUSPEND;

} // MNU_read_dir

static void MNU_tell_dir(DEMOWinMenuInfo *menu, u32 item, u32 *result)
{
#pragma unused (menu)
#pragma unused (item)
    DEMOWinLogPrintf(Debug_win, "\nDVDTellDir:Current location:%d\n", 
                     DVDTellDir(&menu_dir));
    *result = MENU_SUSPEND;

} // MNU_tell_dir

static void MNU_select_location(DEMOWinMenuInfo *menu, u32 item, u32 *result)
{
    *result = (u32)atoi(menu->items[item].name);
} // end __select_file

static DEMOWinMenuInfo *__create_location_menu(void)
{
    DVDDir      dir;
    DVDDirEntry dirent;

    u32         index;

    index = 0;

    memcpy(&dir, &menu_dir, sizeof(DVDDir));
    DVDRewindDir(&dir);

    while ( (DVDReadDir(&dir, &dirent)) && (index < MAX_DIR_ENTRIES))
    {
        // get name, append '/' if it's a directory
        sprintf(location_entry_name[index], "%d", DVDTellDir(&dir));
        location_entry[index].function = MNU_select_location;
        location_entry[index].flags    = DEMOWIN_ITM_EOF;
        location_entry[index].name  = &location_entry_name[index][0];
        location_entry[index].link  = NULL;
        index++;
    }

    // terminate list
    location_entry[index].flags    = DEMOWIN_ITM_TERMINATOR;
    location_entry[index].function = NULL;
    location_entry[index].link     = NULL;

    // Initialize Menu info structure

    location_list.title  = "Location Listing";  // later, init with directory name
    location_list.handle = NULL;                 // placeholder for window handle
    location_list.items  = &location_entry[0];        // list of items

    location_list.max_display_items = 24;        // display 22 files at a time
    location_list.flags             = 0;         // no flags
    
    location_list.cb_open   = NULL;              // no callbacks
    location_list.cb_move   = NULL;
    location_list.cb_select = NULL;
    location_list.cb_cancel = NULL;

    // private members; initialize to zero, they will be calculated at runtime
    location_list.num_display_items = 0;
    location_list.num_items         = 0;  
    location_list.max_str_len       = 0;
    location_list.curr_pos          = 0;   
    location_list.display_pos       = 0;

    DVDCloseDir(&dir);

    if (index)
    {
        return(&location_list);
    }
    else
    {
        return(NULL);
    }
} // end __create_location_menu()

static u32 PickLocation(DEMOWinInfo *handle)
{
    DEMOWinMenuInfo *m; // dir menu
    u32             val = 0;

    DEMOWinSetWindowColor(handle, DEMOWIN_COLOR_CAPTION,
                          50, 50, 50, 255);

    m = __create_location_menu();
    DEMOWinCreateMenuWindow(m, (u16)(handle->x1-170), (u16)(handle->y1+24));
    val = DEMOWinMenu(m);
    DEMOWinDestroyMenuWindow(m);

    DEMOWinSetWindowColor(handle, DEMOWIN_COLOR_RESET, 0, 0, 0, 0);
    
    return val;
}

static void MNU_seek_dir(DEMOWinMenuInfo *menu, u32 item, u32 *result)
{
#pragma unused (menu)
#pragma unused (item)
    u32 loc;

    loc = PickLocation(menu->handle);
    DVDSeekDir(&menu_dir, loc);
    DEMOWinLogPrintf(Debug_win, "\nDVDSeekDir:Set the location:%d\n", loc);
    *result = MENU_SUSPEND;

} // MNU_seek_dir

static void MNU_rewind_dir(DEMOWinMenuInfo *menu, u32 item, u32 *result)
{
#pragma unused (menu)
#pragma unused (item)
    DVDRewindDir(&menu_dir);
    DEMOWinLogPrintf(Debug_win, "\nDVDRewindDir:Current location:%d\n", 
                     DVDTellDir(&menu_dir));

    *result = MENU_SUSPEND;

} // MNU_rewind_dir

static void MNU_get_current_dir(DEMOWinMenuInfo *menu, u32 item, u32 *result)
{
#pragma unused (menu, item)

    char path[256];

    DVDGetCurrentDir( path, sizeof(path));
    DEMOWinLogPrintf(Debug_win, "\nDVDGetCurrentDir:%s\n", path);
    *result = MENU_SUSPEND;

} // MNU_get_current_dir

#ifdef DVDETH
static void MNU_fst_refresh(DEMOWinMenuInfo *menu, u32 item, u32 *result)

{
#pragma unused (menu, item)

    *result = MENU_OPEN_ALL;

    if (DemoPauseFlag)
    {
        DEMOWinLogPrintf(Debug_win, "\nDVDPause is selected, unable to issue DVDFstRefresh.\nIssue DVDResume .\n");
        return;
    }
    
    if (DVDFstRefresh())
    {
        DEMOWinLogPrintf(Debug_win, "\nDVDFstRefresh:Succeeded\n");
    }
    else
    {
        DEMOWinLogPrintf(Debug_win, "\nDVDFstRefresh:Faileded\n");
    }

} // MNU_cancel
#endif

static void MNU_cancel(DEMOWinMenuInfo *menu, u32 item, u32 *result)
{
#pragma unused (menu, item)

    *result = MENU_OPEN_ALL;

} // MNU_cancel

static void MNU_convert(DEMOWinMenuInfo *menu, u32 item, u32 *result)
{
#pragma unused (menu, item)
    
    s32 entry;

    entry = DVDConvertPathToEntrynum(menu->title);
    DEMOWinLogPrintf(Debug_win, "\nDVDConvertPath:%4d:%s\n", entry, menu->title);
    *result = MENU_SUSPEND;

} // MNU_convert

static void MNU_file_size(DEMOWinMenuInfo *menu, u32 item, u32 *result)
{
#pragma unused (item)

    DVDFileInfo  fileInfo;

    if (DVDOpen(menu->title, &fileInfo))
    {
        DEMOWinLogPrintf(Debug_win, "\nFile Size:%4d:%s\n", DVDGetLength(&fileInfo), menu->title);
    }
    else
    {
        DEMOWinLogPrintf(Debug_win, "\nDVDOpen:Failed:%s\n", menu->title);
    }
    
    *result = MENU_OPEN_ALL;

} // MNU_open_dir
