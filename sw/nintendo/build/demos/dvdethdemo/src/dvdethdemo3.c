/*---------------------------------------------------------------------------*
  Project:  GAMECUBE DVDETH Demo3
  File:     dvddemo3.c

  Copyright 2002 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: dvdethdemo3.c,v $
  Revision 1.1.1.1  2004/03/02 05:31:13  paulm
  Socket Library 24-Sep-2003

    
    13    03/09/05 8:55a Ueno_kyu
    
    12    03/09/02 4:23p Ueno_kyu
    
    3rd release version 03/09/02 3:00p

    11    03/03/20 11:03a Ooshima
    Modified to move ip address setting to ipaddress.h.
    
    10    03/02/28 5:26p Ooshima
    2nd release version
    
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*
  This program shows how to use synchronous write function
 *---------------------------------------------------------------------------*/
#include <demo.h>
#include "osreport.h"
#include "ipaddress.h"
#ifdef DVDETH
#include <dolphin/dvdeth.h>
#endif

#define    NumOf(x)    (sizeof(x)/sizeof((x)[0]))

#ifdef DVDETH
#define FST_MAX_SIZE 1024 * 1024
#endif

void        main            ( void );

// The name of the file we are going to read.
static const char *pFileName[] =
{
    "texts/test1.txt",
    "texts/test2.txt"
};

void main(void)
{
    DVDFileInfo fileInfo;
    u32         fileSize;
    char*       buffer;         // pointer to the buffer
    u32         j;
    s32            retVal;
#ifdef DVDETH
    char        filename[256];
#endif

#ifdef DVDETH
    char*       fstBuffer;         // pointer to the FST buffer
#endif

    DEMOInit(NULL);

#ifdef DVDETH
    // Initialize Network
    DVDEthInit(GCClientAddr, Netmask, Gateway);

    // Initialize DVD
    DVDLowInit(DvdServerAddr, DvdServerPort);
#endif

    // Initialize OSReport();
    OSReportInit(ReportServerAddr, ReportServerPort);

#ifdef DVDETH
    // Allocate a buffer to load the fst
    fstBuffer = OSAlloc(FST_MAX_SIZE);

    // Initialize FST
    if (!DVDFstInit(fstBuffer, FST_MAX_SIZE))
    {
        OSHalt("Cannot initialize FST");
    }
#endif

    DVDSetAutoFatalMessaging(TRUE);

    for( j = 0 ; j < NumOf(pFileName) ; j++)
    {
        // We MUST open the file before accessing the file
        if (FALSE == DVDOpen(pFileName[j], &fileInfo))
        {
            OSHalt("Cannot open file");
        }

        // Open the files
        OSReport("open file:%s\n", pFileName[j]);

        // Get the size of the file
        fileSize = DVDGetLength(&fileInfo);

        // Allocate a buffer to read the file.
        // Note that pointers returned by OSAlloc are always 32byte aligned.
        buffer = OSAlloc(OSRoundUp32B(fileSize));
        // read the entire file here

        if (0 > (retVal = DVDRead(&fileInfo, (void*)buffer, (s32)OSRoundUp32B(fileSize), 0)))
        {
            OSHalt("Error occurred when reading file");
        }

        // Close the file
        DVDClose(&fileInfo);

        // Show the files
        OSReport("read file:%s\n", pFileName[j]);

#ifdef DVDETH
        // Create same file as opened file. 
        sprintf(filename, "copy/%s", pFileName[j]);
    
        // Remove file to create.
        // if specified file does not exists, DVDRemove fail.
        if(DVDRemove(filename, NULL))
        {
            OSReport("remove file:%s\n", filename);
        }

        if (FALSE == DVDCreate(filename, &fileInfo))
        {
            OSHalt("Cannot create file");
        }

        OSReport("create file:%s\n", filename);

        // No need to align address, length or offset like DVDRead.
        if (0 > (retVal = DVDWrite(&fileInfo, (void*)buffer, (s32)fileSize, 0)))
        {
            OSHalt("Error occurred when writing file");
        }

        OSReport("write file:%s\n", filename);
#endif
        OSFree(buffer);
    } // for( j = 0 ; j < NumOf(pFileName) ; j++)

#ifdef DVDETH
    DVDEthShutdown();
#endif

    OSHalt("End of demo");


}
