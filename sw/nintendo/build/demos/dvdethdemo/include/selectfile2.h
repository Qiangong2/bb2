/*---------------------------------------------------------------------------*
  Project:  header file for file selection window system
  File:     errorhandling.h

  Copyright 2003 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: selectfile2.h,v $
  Revision 1.1.1.1  2004/03/02 05:31:13  paulm
  Socket Library 24-Sep-2003

    
    2     03/09/02 4:22p Ueno_kyu
    
    1     03/02/28 5:26p Ooshima
    2nd release version
    
    3     03/02/14 2:02p Ooshima
    
    2     03/01/31 1:57p Ooshima
    
    1     03/01/29 6:16p Ooshima
    
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#include <demo.h>
#include <demo/DEMOWin.h>

void InitSelectFile(DEMOWinInfo *debug_win);
char* SelectFile(DEMOWinInfo *handle);
void MNU_read(DEMOWinMenuInfo *menu, u32 item, u32 *result);

#ifdef DVDETH
void MNU_copy(DEMOWinMenuInfo *menu, u32 item, u32 *result);
void MNU_remove(DEMOWinMenuInfo *menu, u32 item, u32 *result);
#endif

extern BOOL DemoPauseFlag;

enum
{
    MENU_CANCEL   = 0,
    MENU_OPEN_ALL,
    MENU_OPEN_SUBDIR,
    MENU_SUSPEND
};
