/*---------------------------------------------------------------------------*
  Project:  IP address setting file.
  File:     ipaddress.h

  Copyright 2003 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: ipaddress.h,v $
  Revision 1.1.1.1  2004/03/02 05:31:13  paulm
  Socket Library 24-Sep-2003

    
    1     03/03/20 11:10a Ooshima
    Initial Version

  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#ifndef __IPADDRESS_H__
#define __IPADDRESS_H__

static const u8   DvdServerAddr[4]        = { 192, 168,   0,   2 };// PC IP Address
static const u16  DvdServerPort           = 9001;                  // PC TCP PORT

static const u8   GCClientAddr[4]         = { 192, 168,   0,   3 };// GC IP Address
static const u8   Netmask[4]              = { 255, 255, 255,   0 };// Netmask
static const u8   Gateway[4]              = { 192, 168,   0, 254 };// Gateway

static const u8   ReportServerAddr[4]     = { 192, 168,   0,   2 };// PC IP Address
static const u16  ReportServerPort        = 9002;                  // PC TCP PORT

#endif  // __IPADDRESS_H__
