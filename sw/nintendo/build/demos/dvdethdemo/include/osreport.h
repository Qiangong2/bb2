/*---------------------------------------------------------------------------*
  Project:  Socket UDP OSReport demo.
  File:     OSReport.h

  Copyright 2002 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: osreport.h,v $
  Revision 1.1.1.1  2004/03/02 05:31:13  paulm
  Socket Library 24-Sep-2003

    
    1     03/02/28 5:26p Ooshima
    2nd release version
    
    1     03/02/25 4:07p Ooshima
    
    1     02/12/18 3:39p Ooshima
    Initial Version
    
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#ifndef __OSREPORT_H__
#define __OSREPORT_H__


BOOL OSReportInit(const u8* pServerAddr, u16 ServerPort);
void OSReportShutdown(void);
void OSReport2(const char* msg, ...);

#endif  // __OSREPORT_H__
