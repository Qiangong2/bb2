/*---------------------------------------------------------------------------*
  Project:  Audio visualization tools
  File:     DEMOAVX.c

  Copyright 2001 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: DEMOAVX.c,v $
  Revision 1.1.1.1  2004/06/09 17:39:20  paulm
  GC library source from Nintendo SDK

    
    1     1/11/02 4:55p Eugene
    Cheesy method for snooping output from DSP/input to AI-FIFO. It is
    audio-system agnostic, however. 
    
    
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*
 * Includes
 *---------------------------------------------------------------------------*/

#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <math.h>

#include <dolphin.h>
#include <demo.h>
#include <demo/DEMOWin.h>
#include <demo/DEMOAVX.h>



#define AVX_INTERNAL_NUM_FRAMES  10     // buffer for up to 10 frames (in case we dip to oh, say 20fps)

static s16 __AVX_internal_buffer[AVX_FRAME_SIZE_WORDS*AVX_INTERNAL_NUM_FRAMES] ATTRIBUTE_ALIGN(32);


static void (*__AVX_save_isr)(void);    // AVX callback for AI-FIFO DMA interrupt

static u32  __AVX_num_frames;           // number of audio frames to buffer
static u32  __AVX_num_filled;           // number of frames filled since last user refresh
static u32  __AVX_curr_frame;

static u16 *__AVX_buffer;               // internal AI-FIFO buffer 
static s16 *__AVX_left_buffer;          // pointer to caller's left-channel sample buffer
static s16 *__AVX_right_buffer;         // pointer to caller's right-channel sample buffer

static u32  __AVX_write_ptr    = 0;
static u32  __AVX_buffer_size  = 0;


/*---------------------------------------------------------------------------*
 * Name        : 
 * Description : 
 * Arguments   : None.
 * Returns     : None.
 *---------------------------------------------------------------------------*/


static BOOL flag = FALSE;


static void __DEMOAVX_isr(void)
{

    u32 frame_address;

        if (__AVX_save_isr)
        {
            (*__AVX_save_isr)();


            // get current address of current AI-FIFO DMA transaction
            frame_address = 0x80000000 | AIGetDMAStartAddr();

            // freak out if the address is NULL
            ASSERTMSG(frame_address, "AVX: frame address is NULL!\n");


            // invalidate source (DSP output/AI-FIFO input)
            DCInvalidateRange((void *)(frame_address), AVX_FRAME_SIZE_BYTES);

            // copy output from DSP into our AVX buffer
            memcpy((void *)(&__AVX_buffer[__AVX_curr_frame * AVX_FRAME_SIZE_WORDS]), (void *)(frame_address), AVX_FRAME_SIZE_BYTES);

            // Flush newly copied data from cache
            DCFlushRange((void *)(&__AVX_buffer[__AVX_curr_frame * AVX_FRAME_SIZE_WORDS]), AVX_FRAME_SIZE_BYTES);


            // increment frame pointer
            __AVX_curr_frame = (__AVX_curr_frame + 1) % __AVX_num_frames;

            // increment frame counter
            __AVX_num_filled = (__AVX_num_filled + 1) % AVX_INTERNAL_NUM_FRAMES;

            if (__AVX_curr_frame > 4)
            {
                flag = TRUE;
            }



        }

} // end __DEMOAVX_isr

/*---------------------------------------------------------------------------*
 * Name        : 
 * Description : 
 * Arguments   : None.
 * Returns     : None.
 *---------------------------------------------------------------------------*/


u32 DEMOAVXGetNumFilled(void)
{

    u32  tmp;
    BOOL old;

        old = OSDisableInterrupts();

        tmp = __AVX_num_filled;
        __AVX_num_filled = 0;

        OSRestoreInterrupts(old);

        return(tmp);

} // end DEMOAVXGetNumFilled()

/*---------------------------------------------------------------------------*
 * Name        : 
 * Description : 
 * Arguments   : None.
 * Returns     : None.
 *---------------------------------------------------------------------------*/

u32 DEMOAVXGetFrameCounter(void)
{

    return(__AVX_curr_frame);

} // end DEMOAVXGetFrameCounter()

/*---------------------------------------------------------------------------*
 * Name        : 
 * Description : 
 * Arguments   : None.
 * Returns     : None.
 *---------------------------------------------------------------------------*/

u32 DEMOAVXRefreshBuffer(u32 *start_index, u32 *end_index)
{

    u32  num_filled;
    u32  curr_frame;

    u32 i;
    u32 j;

        if (flag)
        {

            // 
            num_filled = DEMOAVXGetNumFilled();

            // rewind back through the AI buffer
            curr_frame = (__AVX_num_frames + DEMOAVXGetFrameCounter() - num_filled) % __AVX_num_frames;

            // save starting position in final sample buffer
            *start_index = __AVX_write_ptr;


            for (i=0; i<num_filled; i++)
            {

                // invalidate the AI buffer range 
                DCInvalidateRange( (void *)(&__AVX_buffer[curr_frame * AVX_FRAME_SIZE_WORDS]), AVX_FRAME_SIZE_BYTES);

                for (j=0; j<AVX_FRAME_SIZE_WORDS; j+=2)
                {
                    __AVX_left_buffer [__AVX_write_ptr] = (s16)(__AVX_buffer[curr_frame * AVX_FRAME_SIZE_WORDS + j]);
                    __AVX_right_buffer[__AVX_write_ptr] = (s16)(__AVX_buffer[curr_frame * AVX_FRAME_SIZE_WORDS + j + 1]);

                    __AVX_write_ptr = (__AVX_write_ptr + 1) % __AVX_buffer_size;
                }

                curr_frame = (curr_frame + 1) % __AVX_num_frames;

            }

            *end_index = __AVX_write_ptr;



            return(num_filled*AVX_FRAME_SIZE_SAMPLES);
        }

        else
        {
            return(0);
        }


} // end DEMOAudioRefreshBuffer()

/*---------------------------------------------------------------------------*
 * Name        : 
 * Description : 
 * Arguments   : None.
 * Returns     : None.
 *---------------------------------------------------------------------------*/

void DEMOAVXInit(s16 *left, s16 *right, u32 size)
{

    __AVX_left_buffer  = left;
    __AVX_right_buffer = right;

    __AVX_write_ptr = 0;

    __AVX_buffer_size = size;


    DEMOAVXAttach((void *)__AVX_internal_buffer, AVX_INTERNAL_NUM_FRAMES);

} // end DEMOAVXInit()

/*---------------------------------------------------------------------------*
 * Name        : 
 * Description : 
 * Arguments   : None.
 * Returns     : None.
 *---------------------------------------------------------------------------*/


void DEMOAVXAttach(void *buffer, u32 num_frames)
{

    BOOL old;
    u32  i;


        __AVX_buffer     = (u16 *)buffer;
        __AVX_num_frames = num_frames;
        __AVX_num_filled = 0;
        __AVX_curr_frame = 0;
        

        // clear buffer
        for (i=0; i<(num_frames*AVX_FRAME_SIZE_WORDS); i++)
        {
            *(__AVX_buffer + i) = 0;
        }
        DCFlushRange(__AVX_buffer, num_frames*AVX_FRAME_SIZE_WORDS);


        // attach AVX interrupt handler
        old = OSDisableInterrupts();

        __AVX_save_isr = AIRegisterDMACallback(__DEMOAVX_isr);

        OSRestoreInterrupts(old);


} // end DEMOAVXAttach()