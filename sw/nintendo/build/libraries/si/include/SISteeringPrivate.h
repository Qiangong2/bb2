/*---------------------------------------------------------------------------*
  Project:  Dolphin OS Steering API
  File:     SISteeringPrivate.h

  Copyright 2001 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: SISteeringPrivate.h,v $
  Revision 1.1.1.1  2004/01/03 01:02:28  paulm
  Gamecube library source from Nintendo

    
    2     8/22/02 11:48 Shiki
    Set #pragma warn_padding off.

    1     4/01/02 16:29 Shiki
    Initial NTD check-in from IRD.
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#ifndef __SISTEERINGPRIVATE_H__
#define __SISTEERINGPRIVATE_H__

#include <private/SISteering.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef void (* SISteeringProc )(s32 chan);

#define SI_STEERING_SIPOLL_NOTINITIALIZE_VAL    0

#define SI_STEERING_CMD_TYPESTATUS              0x00
#define SI_STEERING_CMD_RESET                   0xff
#define SI_STEERING_CMD_READ                    0x30
#define SI_STEERING_CMD_ORIGIN                  0x31
#define SI_STEERING_CMD_CALIBRATE               0x32

#define __SISteeringIsBusy(sc)                  (sc->callback)

#ifdef  __MWERKS__
#pragma warn_padding off
#endif

typedef struct SISteeringControl
{
    u8                 output[3];
    u8                 input[8];
    u32                outputBytes;
    u32                inputBytes;
    SISteeringCallback callback;
    s32                ret;
    OSThreadQueue      threadQueue;
    SISteeringProc     proc;
} SISteeringControl;

#ifdef  __MWERKS__
#pragma warn_padding reset
#endif

void   __SISteeringSyncCallback(s32 chan, s32 ret);
s32    __SISteeringSync        (s32 chan);
s32    __SISteeringTransfer    (s32 chan, u32 outputBytes, u32 inputBytes, SISteeringProc proc);

void   __SISteeringEnable (s32 chan);
void   __SISteeringDisable(s32 chan);

extern SISteeringControl __SISteering[SI_MAX_CHAN];
extern volatile BOOL     __SIResetSteering;           // TRUE if reset has been requested
extern u32               __SISteeringEnableBits;

#ifdef __cplusplus
}
#endif

#endif  // __SISTEERINGPRIVATE_H__
