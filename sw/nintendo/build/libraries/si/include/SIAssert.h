/*---------------------------------------------------------------------------*
  Project:  Dolphin OS Error Messages
  File:     SIAssert.h

  Copyright 1998-2001 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: SIAssert.h,v $
  Revision 1.1.1.1  2004/01/03 01:02:28  paulm
  Gamecube library source from Nintendo

    
    15    11/22/01 10:28 Shiki
    Removed non-SI specific messages.

    13    10/02/01 9:17 Shiki
    Fixed SI_ERR_SETXY_INVX message.

    12    01/04/06 16:41 Shiki
    Fixed SI_ERR_SETXY_INVX message.

    11    01/03/22 9:18 Shiki
    Added SI_ERR_GETCOMMAND_INVCHAN.

    10    01/03/08 15:14 Shiki
    Removed OS_ERR_INITAUDIOSYSTEM_CHKSUMERR.

    9     01/03/02 14:36 Shiki
    Added OSAudioSystem messages.

    8     01/01/30 10:14 Shiki
    Added OS_ERR_SETABSALARM_HANDLER.

    7     5/17/00 5:35p Shiki
    Added more error messages for OSMutex.c

    6     3/21/00 5:01p Shiki
    Added two more SI error messages.

    5     3/16/00 4:31p Shiki
    Added mutex API assert messages.

    4     3/15/00 6:26p Shiki
    Added thread assert messages.

    3     1/21/00 11:04a Shiki
    Fixed OS_ERR_GETARENAHI_INVALID message.

    2     12/02/99 7:53p Tian
    Locked cache asserts added

    12    9/15/99 4:56p Shiki
    Added OSSerial.c assert messages.

    11    8/02/99 2:35p Shiki
    Added more OSException.c assert messages.

    10    8/02/99 2:17p Shiki
    Added more OSInterrupt.c assert messages.

    9     8/02/99 2:10p Shiki
    Added OSInterrupt.c assert messages.

    8     99/07/28 9:21p Shiki
    Added OSSetPeriodicAlarm() assert messages.

    7     99/07/28 5:56p Shiki
    Added OSAlarm.c assert messages.

    6     99/07/27 4:46p Shiki
    Added OSAddress.c assert messages.

    5     99/07/22 7:31p Shiki
    Added OSArena.c assert messages.

    4     99/07/21 2:54p Shiki
    Added OS.c assert messages.

    3     99/07/16 4:29p Shiki
    Added OSTimer.c assert messages.

    2     99/07/12 9:04p Shiki
    Added OS_ERR_SETERRORHANDLER_INVERR.

    1     6/03/99 6:36p Shiki

    2     5/11/99 10:18a Shiki
    Refreshed include tree.

    1     4/30/99 12:47p Tianli01

    1     4/12/99 7:47p Tianli01
    Changed from OSInit.h, added some private utility functions.

    1     4/06/99 3:07p Tianli01
    Initial checkin.  Private header file with init routines for OS
    modules.
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#ifndef __SIASSERT_H__
#define __SIASSERT_H__

#ifdef __cplusplus
extern "C" {
#endif

/*---------------------------------------------------------------------------*
    Error messages of SIBios.c
 *---------------------------------------------------------------------------*/
#define SI_ERR_TRANSFER_INVCHAN             "SITransfer(): invalid channel."
#define SI_ERR_TRANSFER_INVOUTBYTES         "SITransfer(): output size is out of range (must be 1 to 128)."
#define SI_ERR_TRANSFER_INVINBYTES          "SITransfer(): input size is out of range (must be 1 to 128)."
#define SI_ERR_SYNC_CALLBACK                "SISync(): asynchronous SITransfer() is in progress."
#define SI_ERR_SYNC_NOTRANSFER              "SISync(): no SITransfer() is in progress."
#define SI_ERR_SETCOMMAND_INVCHAN           "SISetCommand(): invalid channel."
#define SI_ERR_GETCOMMAND_INVCHAN           "SIGetCommand(): invalid channel."
#define SI_ERR_SETXY_INVX                   "SISetXY(): x is out of range (8 <= x <= 1023)."
#define SI_ERR_SETXY_INVY                   "SISetXY(): y is out of range (0 <= y <= 255)."
#define SI_ERR_GETRESPONSE_INVCHAN          "SIGetResponse(): invalid channel."
#define SI_ERR_ENABLEPOLLING_INVCHAN        "SIEnablePolling(): invalid chan bit(s)."
#define SI_ERR_DISABLEPOLLING_INVCHAN       "SIDisablePolling(): invalid chan bit(s)."

/*---------------------------------------------------------------------------*
    Error messages of SISamplingRate.c
 *---------------------------------------------------------------------------*/
#define SI_ERR_SETSAMPLINGRATE_INVARG       "SISetSamplingRate(): out of rage (0 <= msec <= 11)"

#ifdef __cplusplus
}
#endif

#endif  // __OSPRIVATE_H__
