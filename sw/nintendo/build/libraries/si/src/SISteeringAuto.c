/*---------------------------------------------------------------------------*
  Project:  Dolphin OS Steering API
  File:     SISteeringAuto.c

  Copyright 2001 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: SISteeringAuto.c,v $
  Revision 1.1.1.1  2004/01/03 01:02:28  paulm
  Gamecube library source from Nintendo

    
    1     4/01/02 16:26 Shiki
    Initial NTD check-in from IRD.
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#include <stdlib.h>
#include <string.h>
#include <dolphin/os.h>
#include <private/si.h>
#include <private/SISteering.h>
#include <private/flipper.h>
#include "SISteeringPrivate.h"

u32 __SISteeringEnableBits;

static void (*SamplingCallback)(void);

/*---------------------------------------------------------------------------*
  Name:         __SISteeringEnable

  Description:  Start auto read mode

  Arguments:    None.

  Returns:      None.
 *---------------------------------------------------------------------------*/
void __SISteeringEnable(s32 chan)
{
    u32 cmd;
    u32 chanBit;
    u32 data[2];

    chanBit = SI_CHAN0_BIT >> chan;
    if (!(__SISteeringEnableBits & chanBit))
    {
        __SISteeringEnableBits |= chanBit;
        SIGetResponse(chan, data);  // clear RDST bit in SISR
        cmd = (SI_STEERING_CMD_READ << 16) | 0x680;
        SISetCommand(chan, cmd);
        SIEnablePolling(__SISteeringEnableBits);
    }
}

/*---------------------------------------------------------------------------*
  Name:         __SISteeringDisable

  Description:  Stop auto read mode

  Arguments:    None.

  Returns:      None.
 *---------------------------------------------------------------------------*/
void __SISteeringDisable(s32 chan)
{
    u32          chanBit;

    chanBit = SI_CHAN0_BIT >> chan;
    SIDisablePolling(chanBit);
    __SISteeringEnableBits &= ~chanBit;
}

/*---------------------------------------------------------------------------*
  Name:         SIReadSteering

  Description:  Gets steering status.

  Arguments:    chan        One of SI_CHAN*
                status      Pointer to SISteeringStatus

  Returns:      SI_STEERING_ERR_READY
                SI_STEERING_ERR_BUSY
                SI_STEERING_ERR_NO_CONTROLLER
                SI_STEERING_ERR_TRANSFER
                SI_STEERING_ERR_OVERFLOW
 *---------------------------------------------------------------------------*/
s32 SIReadSteering(s32 chan, SISteeringStatus* status)
{
    BOOL                enabled;
    SISteeringControl*  sc;
    u32                 data[2];
    u32                 chanBit;
    u32                 sr;
    s32                 ret;

    enabled = OSDisableInterrupts();

    sc = &__SISteering[chan];
    chanBit = SI_CHAN0_BIT >> chan;

    if (SIIsChanBusy(chan))
    {
        // SISteering still performs a commutation transfer for something.
        // This makes sure SIReadSteering() does not call SISteeringDisable()
        // until the commutation transfer completes.
        sc->ret = SI_STEERING_ERR_BUSY;
    }
    else if (!(__SISteeringEnableBits & chanBit))
    {
        sc->ret = SI_STEERING_ERR_NO_CONTROLLER;
    }
    else
    {
        sr = SIGetStatus(chan);
        if (sr & SI_ERROR_NO_RESPONSE)
        {
            SIGetResponse(chan, data);  // clear RDST bit in SISR
            __SISteeringDisable(chan);
            sc->ret = SI_STEERING_ERR_NO_CONTROLLER;
        }
        else if (!SIGetResponse(chan, data) || data[0] & SI_0INBUFH_ERRSTAT_MASK)
        {
            sc->ret = SI_STEERING_ERR_TRANSFER;
        }
        else
        {
            sc->ret = SI_STEERING_ERR_READY;

            if (status)
            {
                status->button   = (u16) (data[0] >> 16);
                status->misc     = (u8)  (data[0] >> 8);
                status->steering = (s8) ((data[0] & 0xff) - 0x80);
                status->gas   = (u8) (data[1] >> 24);
                status->brake = (u8) (data[1] >> 16);
                status->left  = (u8) (data[1] >> 8);
                status->right = (u8) data[1];
            }
        }
    }

    if (status)
    {
        status->err = (s8) sc->ret;
    }
    ret = sc->ret;

    OSRestoreInterrupts(enabled);

    return ret;
}

static void SamplingHandler(__OSInterrupt interrupt, OSContext* context)
{
    #pragma unused(interrupt)
    OSContext exceptionContext;

    if (SamplingCallback)
    {
        // Create new context on stack
        OSClearContext(&exceptionContext);
        OSSetCurrentContext(&exceptionContext);

        SamplingCallback();

        OSClearContext(&exceptionContext);
        OSSetCurrentContext(context);
    }
}

/*---------------------------------------------------------------------------*
  Name:         SISetSteeringSamplingCallback

  Description:  Installs sampling callback.

  Arguments:    callback    Pointer to the callback to be installed.
                            Set NULL to cancel the callback.

  Returns:      previous callback
 *---------------------------------------------------------------------------*/
SISteeringSamplingCallback SISetSteeringSamplingCallback(SISteeringSamplingCallback callback)
{
    SISteeringSamplingCallback prev;

    prev = SamplingCallback;
    SamplingCallback = callback;
    if (callback)
    {
        SIRegisterPollingHandler(SamplingHandler);
    }
    else
    {
        SIUnregisterPollingHandler(SamplingHandler);
    }
    return prev;
}

/*---------------------------------------------------------------------------*
  Name:         SIControlSteering

  Description:  Controls rumble motor

  Arguments:    chan        One of SI_CHANn
                control     One of SI_STEERING_CONTROL_*
                level       Clamped to [-128, 128]

  Returns:      None.
 *---------------------------------------------------------------------------*/
void SIControlSteering( s32 chan, u32 control, s32 level )
{
    BOOL enabled;
    u32  chanBit;
    u32  command;

    control &= SI_STEERING_CONTROL_MASK;
    if (level <= -128)
    {
        level = -128;
        command = 0x000;
    }
    else if (128 <= level)
    {
        level = 128;
        command = 0x100;
    }
    else
    {
        command = (u32) (0x80 + level);
    }
    command |= control;
    command &= 0x7ff;

    enabled = OSDisableInterrupts();
    chanBit = SI_CHAN0_BIT >> chan;
    if ((__SISteeringEnableBits & chanBit))
    {
        command = (SI_STEERING_CMD_READ << 16) | command;
        SISetCommand(chan, command);
        SITransferCommands();
    }
    OSRestoreInterrupts(enabled);
}
