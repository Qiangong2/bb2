/*---------------------------------------------------------------------------*
  Project:  Dolphin OS Steering API
  File:     SISteeringXfer.c

  Copyright 2000-2001 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: SISteeringXfer.c,v $
  Revision 1.1.1.1  2004/01/03 01:02:28  paulm
  Gamecube library source from Nintendo

    
    1     4/01/02 16:26 Shiki
    Initial NTD check-in from IRD.
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#include <stdlib.h>
#include <dolphin/os.h>
#include <private/SISteering.h>
#include <private/si.h>
#include "SISteeringPrivate.h"

extern void __OSReschedule(void);

/*---------------------------------------------------------------------------*
  Name:         __SISteeringHandler

  Description:  Steering SI callback

  Arguments:    chan
                error
                context

  Returns:      None.
 *---------------------------------------------------------------------------*/
static void __SISteeringHandler(s32 chan, u32 error, OSContext* context)
{
    #pragma unused( context )

    SISteeringControl* sc = &__SISteering[chan];
    SISteeringProc     proc;
    SISteeringCallback callback;

    // If reset has been requested, do not call callback functions.
    if (__SIResetSteering)
    {
        return;
    }

    if (error & SI_ERROR_NO_RESPONSE)
    {
        sc->ret = SI_STEERING_ERR_NO_CONTROLLER;
    }
    else if (error & (SI_ERROR_UNDER_RUN | SI_ERROR_OVER_RUN | SI_ERROR_COLLISION))
    {
        sc->ret = SI_STEERING_ERR_TRANSFER;
    }
    else
    {
        sc->ret = SI_STEERING_ERR_READY;
    }

    if (sc->proc)
    {
        proc = sc->proc;
        sc->proc = 0;
        proc(chan);
    }

    if (sc->callback)
    {
        OSContext exceptionContext;

        // Create new context on stack so that callback can touch FPU registers
        OSClearContext(&exceptionContext);
        OSSetCurrentContext(&exceptionContext);

        callback = sc->callback;
        sc->callback = 0;
        callback(chan, sc->ret);

        OSClearContext(&exceptionContext);
        OSSetCurrentContext(context);
    }
}

void __SISteeringSyncCallback(s32 chan, s32 ret)
{
    #pragma unused( ret )
    SISteeringControl* sc = &__SISteering[chan];

    OSWakeupThread(&sc->threadQueue);
}

s32 __SISteeringSync(s32 chan)
{
    SISteeringControl* sc = &__SISteering[chan];
    s32                ret;
    BOOL               enabled;

    enabled = OSDisableInterrupts();
    while ((volatile SISteeringCallback) sc->callback)
    {
        OSSleepThread(&sc->threadQueue);
    }
    ret = sc->ret;
    OSRestoreInterrupts(enabled);
    return ret;
}

static void TypeAndStatusCallback(s32 chan, u32 type)
{
    #pragma unused( chan )
    SISteeringControl* sc = &__SISteering[chan];
    SISteeringProc     proc;
    SISteeringCallback callback;

    // If reset has been requested, do not call callback functions.
    if (__SIResetSteering)
    {
        return;
    }

    ASSERT(!(type & SI_ERROR_BUSY));

    if ((type & 0xffff0000) == SI_GC_STEERING)
    {
        if (SITransfer(chan,
                       sc->output, sc->outputBytes,
                       sc->input,  sc->inputBytes,
                       __SISteeringHandler,
                       0))
        {
            return;
        }
        else
        {
            // should not occur
            sc->ret = SI_STEERING_ERR_BUSY;
        }
    }
    else
    {
        sc->ret = SI_STEERING_ERR_NO_CONTROLLER;
    }

    if (sc->proc)
    {
        proc = sc->proc;
        sc->proc = 0;
        proc(chan);
    }

    if (sc->callback)
    {
        OSContext  exceptionContext;
        OSContext* context = OSGetCurrentContext();

        // Create new context on stack so that callback can touch FPU registers
        OSClearContext(&exceptionContext);

        // Thread scheduler is automatically disabled
        // after the following OSSetCurrentContext().
        OSSetCurrentContext(&exceptionContext);

        callback = sc->callback;
        sc->callback = 0;
        callback(chan, sc->ret);

        OSClearContext(&exceptionContext);
        // Thread scheduler will be re-enabled if TypeAndStatusCallback() is
        // directly called from SIGetTypeAsync().
        OSSetCurrentContext(context);

        // In case thread functions are called inside the callback.
        __OSReschedule();
    }
}

s32 __SISteeringTransfer(s32 chan, u32 outputBytes, u32 inputBytes, SISteeringProc proc)
{
    BOOL               enabled;
    SISteeringControl* sc = &__SISteering[chan];

    enabled = OSDisableInterrupts();
    sc->proc = proc;
    sc->outputBytes = outputBytes;
    sc->inputBytes  = inputBytes;
    SIGetTypeAsync(chan, TypeAndStatusCallback);
    OSRestoreInterrupts(enabled);

    return SI_STEERING_ERR_READY;
}
