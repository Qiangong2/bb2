/*---------------------------------------------------------------------------*
  Project:  Dolphin OS Steering API
  File:     SISteering.c

  Copyright 2001 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: SISteering.c,v $
  Revision 1.1.1.1  2004/01/03 01:02:28  paulm
  Gamecube library source from Nintendo

    
    1     4/01/02 16:26 Shiki
    Initial NTD check-in from IRD.
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#include <stdlib.h>
#include <stddef.h>
#include <string.h>

#include <dolphin/os.h>
#include <dolphin/pad.h>
#include <private/SISteering.h>
#include <private/flipper.h>
#include <dolphin/vi.h>
#include <private/si.h>
#include "SISteeringPrivate.h"

extern void __OSReschedule(void);

SISteeringControl __SISteering[SI_MAX_CHAN];
volatile BOOL     __SIResetSteering;           // TRUE if reset has been requested

static BOOL OnReset( BOOL final );

static OSResetFunctionInfo ResetFunctionInfo =
{
    OnReset,
    OS_RESET_PRIO_PAD
};

/*---------------------------------------------------------------------------*
  Name:         SIInitSteering

  Description:  Initializes SISteering library

  Arguments:    None.

  Returns:      None.
 *---------------------------------------------------------------------------*/
void SIInitSteering(void)
{
    static BOOL        initialized = FALSE;
    SISteeringControl* sc;
    s32                chan;

    if (initialized)
    {
        return;
    }
    initialized = TRUE;

    for (chan = 0; chan < SI_MAX_CHAN; ++chan)
    {
        sc = &__SISteering[chan];
        sc->ret = SI_STEERING_ERR_READY;
        OSInitThreadQueue(&sc->threadQueue);
    }

    SIRefreshSamplingRate();

    __SIResetSteering = FALSE;
    OSRegisterResetFunction(&ResetFunctionInfo);
}

/*---------------------------------------------------------------------------*
  Name:         DefaultCallback

  Description:  Default callback for synchronous functions
 *---------------------------------------------------------------------------*/
static void DefaultCallback(s32 chan, s32 ret)
{
    #pragma unused(chan, ret)
    return;
}

/*---------------------------------------------------------------------------*
  Name:         SISteeringBegin

  Description:  Start the next SISteering command if not busy
 *---------------------------------------------------------------------------*/
static s32 SISteeringBegin(SISteeringControl* sc, SISteeringCallback callback)
{
    BOOL enabled;
    s32  ret;

    callback = callback ? callback : DefaultCallback;
    enabled = OSDisableInterrupts();
    if (__SISteeringIsBusy(sc))
    {
        ret = SI_STEERING_ERR_BUSY;
    }
    else
    {
        sc->callback  = callback;
        ret = SI_STEERING_ERR_READY;
    }
    OSRestoreInterrupts(enabled);
    return ret;
}

/*---------------------------------------------------------------------------*
  Name:         ResetProc

  Description:  Handles reset command

  Arguments:    chan        SI channel number

  Returns:      None.
 *---------------------------------------------------------------------------*/
static void ResetProc(s32 chan)
{
    SISteeringControl* sc = &__SISteering[chan];

    if (sc->ret == SI_STEERING_ERR_READY && !__SIResetSteering)
    {
        __SISteeringEnable(chan);
    }
}

/*---------------------------------------------------------------------------*
  Name:         SIResetSteeringAsync

  Description:  Resets the steering connected to the specified channel.

  Arguments:    chan        One of SI_CHAN*

  Returns:      SI_STEERING_ERR_READY         succeeded
 *---------------------------------------------------------------------------*/
s32 SIResetSteeringAsync(s32 chan, SISteeringCallback callback)
{
    SISteeringControl* sc = &__SISteering[chan];
    s32         ret;

    ret = SISteeringBegin(sc, callback);
    if (ret != SI_STEERING_ERR_READY)
    {
        return ret;
    }
    sc->output[0] = SI_STEERING_CMD_RESET;
    return __SISteeringTransfer(chan, 1, 3, ResetProc);
}

/*---------------------------------------------------------------------------*
  Name:         SIResetSteering

  Description:  Resets the steering connected to the specified channel.

  Arguments:    chan        One of SI_CHAN*

  Returns:      SI_STEERING_ERR_READY         succeeded
                SI_STEERING_ERR_NO_CONTROLLER steering is not attached
                SI_STEERING_ERR_BUSY          busy
                SI_STEERING_ERR_TRANSFER      data transfer error
 *---------------------------------------------------------------------------*/
s32 SIResetSteering(s32 chan)
{
    SISteeringControl* sc = &__SISteering[chan];
    s32                ret;

    ret = SIResetSteeringAsync(chan, __SISteeringSyncCallback);
    if (ret)
    {
        return ret;
    }
    return __SISteeringSync(chan);
}

/*---------------------------------------------------------------------------*
  Name:         OnReset

  Description:  Reset function for OSResetSystem()

  Arguments:    final       TRUE if OSResetSystem() is about to reset
                            the system

  Returns:      TRUE if GBA subsystem is ready to reset.
 *---------------------------------------------------------------------------*/
static BOOL OnReset(BOOL final)
{
    #pragma unused( final )
    static s32 count;
    s32 chan;

    if (__SIResetSteering == FALSE)
    {
        __SIResetSteering = TRUE;
        for (chan = 0; chan < SI_MAX_CHAN; ++chan)
        {
            if ((SI_CHAN0_BIT >> chan) & __SISteeringEnableBits)
            {
                SIControlSteering(chan, SI_STEERING_CONTROL_STANDBY, 0);
            }
        }
        count = (s32) VIGetRetraceCount();
    }

    if (2 < (s32) VIGetRetraceCount() - count)
    {
        while (__SISteeringEnableBits)
        {
            __SISteeringDisable(__cntlzw(__SISteeringEnableBits));
        }
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}
