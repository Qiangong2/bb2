/*---------------------------------------------------------------------------*
  Project:  Serial Interface API
  File:     SISamplingRate.c

  Copyright 1998-2001 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: SISamplingRate.c,v $
  Revision 1.1.1.1  2004/01/03 01:02:28  paulm
  Gamecube library source from Nintendo

    
    86    11/22/01 16:01 Shiki
    Modified XY table to minimize counts.

    85    11/22/01 11:35 Shiki
    Clean up.

    84    11/22/01 10:29 Shiki
    Clean up.

    82    11/12/01 10:47 Shiki
    Fixed PADSetSamplingRate() for VI_EURGB60 to make it use XYNTSC.

    81    11/02/01 5:31p Hashida
    Renamed eu60 -> eurgb60

    80    11/01/01 3:36p Hashida
    Added EURGB60 mode support.

    79    10/29/01 11:56 Shiki
    Modified __PADDisableRecalibration() to make it return the previous
    state.

    78    10/25/01 18:25 Shiki
    To support multiple auto mode serial devices, modified SIGetStatus() to
    check just a single channel at a time.

    77    10/25/01 17:12 Shiki
    Revised to support multiple auto mode SI devices.

    76    10/18/01 12:09p Hashida
    Removed EU60

    74    10/12/01 16:30 Shiki
    Modified PADSetSamplingCallback() to make it return the previous
    callback pointer.

    73    10/02/01 10:52 Shiki
    Simplified PADSetSamplingRate() and make it disable interrupts
    internally so that it can cooperate with __VIRetraceHandler().

    72    10/01/01 21:30 Shiki
    Fixed PADSetSamplingRate() to set correct SIPOLL value for progressive
    mode video.

    71    9/27/01 11:35 Shiki
    Fixed PADInit() not to call __PADDisableXPatch() in IPL initialization.

    70    9/27/01 11:08 Shiki
    Fixed PADInit() to force controllers to recalibrate manually at
    power-on reset.

    69    9/08/01 12:50 Shiki
    Fixed typo in comments.

    68    9/08/01 12:46 Shiki
    Added more comments.

    67    9/07/01 23:20 Shiki
    Fixed PADRead() using SIIsChanBusy().

    66    9/06/01 17:48 Shiki
    Refined SamplingHandler().

    65    9/05/01 16:34 Shiki
    Added __PADDisableRecalibration().

    64    9/03/01 14:55 Shiki
    Revised using new SI functions.

    63    8/11/01 10:22 Shiki
    Modified to perform control stick X drift workaround whenever updating
    origin.

    62    8/10/01 16:59 Shiki
    Implemented control stick X drift workaround.

    61    7/30/01 10:54 Shiki
    Modified PADRead() to return bit mask of controllers that support
    rumble motors.

    60    01/06/29 16:28 Shiki
    Fixed to keep output bytes until SITransfer completes. Also fixed
    PADGetType() not to return garbage data due to SI error.

    59    6/08/01 10:58a Shiki
    Fixed PADRead() to check PAD_ORIGIN correctly.

    58    5/21/01 1:22p Shiki
    Modified PADSync() to check SIBusy().

    57    5/21/01 12:50p Shiki
    Revised OnReset().

    56    5/21/01 10:50a Shiki
    Modified OnReset() to call PADRecalibrate() to recalibrate controllers
    and to stop motors.

    55    5/17/01 8:34p Shiki
    Revised PADInit() to register the reset function OnReset().

    54    5/11/01 5:34p Shiki
    Revised for better WaveBird support. (Test by Fukuda, IRD).

    53    01/03/23 15:09 Shiki
    Changed error code:
    1. from PAD_ERR_NOT_READY to PAD_ERR_NONE while probing WaveBird.
    2. from PAD_ERR_NOT_READY to PAD_ERR_TRANSFER if no data.

    52    01/03/22 11:03 Shiki
    Fixed PADSetAnalogMode() to stop the polling.

    51    01/03/21 18:56 Shiki
    Added PADGetSpec().

    50    01/03/21 17:39 Shiki
    Added PADSetAnalogMode().
    Changed the default spec to 5.
    Added __OSWirelessPadFixMode handling code.

    49    01/03/14 9:57 Shiki
    Fixed PADResetCallback() to get origin no matter what.

    48    01/03/13 21:07 Shiki
    Modified not to clamp analog A and B.

    47    01/03/12 9:45 Shiki
    Fixed SPEC2_MakeStatus().

    46    01/03/08 15:33 Shiki
    Modified PADRead() to check SI_SR_RDSTn_MASK bit all the time.

    45    01/03/06 14:13 Shiki
    Fixed PADEnable() to clear RDST bit in SISR.

    44    01/03/06 11:28 Shiki
    Revised XY table for longer latency (8).

    43    01/03/06 10:30 Shiki
    Fixed PADRead() to check RDST bit for the 1st time.

    42    01/03/05 14:30 Shiki
    Fixed DoReset() so that it can recover SITransfer() error.

    41    01/02/08 16:33 Shiki
    Revised PAD_ORIGIN bit handling code.

    40    01/02/05 11:18 Shiki
    Revised Wavebird handling code.

    39    01/01/31 18:39 Shiki
    Fixed PADSetSpec().

    38    01/01/31 17:18 Shiki
    Modified non-standard wireless controllers code.

    37    01/01/30 14:45 Shiki
    Removed non-standard wavebird controller support.

    36    01/01/30 14:23 Shiki
    Revised Wavebird handling code.

    35    12/19/00 1:11p Shiki
    Modified to use OSSramEx functions.

    34    12/01/00 6:38p Shiki
    Added __PADSpec.

    33    11/20/00 6:05p Shiki
    Added DS4 support code.

    32    10/17/00 10:14a Hashida
    From IRD: Added PADRecalibrate and PADSync

    31    10/03/00 9:43a Hashida
    From Shiki: Crash bug fix for spec 0 and 1 controllers that connected
    to other than port zero.

    30    10/02/00 10:16a Hashida
    From Shiki: Fixed PADRead() to return PAD_ERR_NOT_READY while wireless
    units do not receive any data.

    29    9/11/00 5:45p Shiki
    Fixed to initialized MakeStatus when PAD_USESPEC is not defined.

    28    9/05/00 9:07p Shiki
    Added PADGetType().

    27    8/29/00 10:18p Shiki
    Added __PADWirelessIDCheck().

    26    8/29/00 10:09p Shiki
    Added wireless controller support.

    25    8/28/00 8:16p Shiki
    Implemented clamp routines for origin adjustment, etc.

    24    8/28/00 6:56p Shiki
    Checked using the final controller.

    23    8/24/00 7:33p Shiki
    Modified to support final controller (not tested).

    22    5/10/00 10:25a Shiki
    Modified PADResetCallback().

    21    5/09/00 6:47p Shiki
    Updated for prototype controllers

    20    3/23/00 4:33p Shiki
    Added PADSetSpec().

    19    3/21/00 5:43p Shiki
    PADInit() shows an error message if it is called twice (_DEBUG only).

    18    3/21/00 5:06p Shiki
    Fixed the wrong calls to SIEnablePolling() and SIDisablePolling().

    17    3/17/00 4:01p Shiki
    Fixed PADInit() so that it can be called even if there is an on-going
    PADReset().

    16    3/10/00 4:00p Shiki
    Implemented ASSERTMSG()s.

    15    3/10/00 3:39p Shiki
    Made x-y tables be static.

    14    3/10/00 3:35p Shiki
    Modified to use Commands()..

    13    3/10/00 3:25p Shiki
    Fixed PADInit() to initially disable data sampling.

    12    3/10/00 2:57p Shiki
    Added PADControlMotor().

    11    3/09/00 8:29p Shiki
    Added PADControlMotor().

    10    2/24/00 8:25p Shiki
    Revised to use standard <dolphin/pad.h>

    9     2/22/00 6:22p Shiki
    Fixed x-y table.

    8     2/18/00 6:03p Shiki
    Revised PADSetSamplingRate().

    7     2/17/00 6:56p Shiki
    Revised PADSetSamplingRate().

    6     2/17/00 3:21p Shiki
    Revised not to use P_ONset bit on the controller bread board.

    5     2/16/00 4:52p Shiki
    Revised PAD_CHANn_BIT management.

    4     2/15/00 4:11p Shiki
    Implemented PADSetSamplingRate(). Added function descriptions.

    3     2/14/00 7:33p Shiki
    Implemented PADReset(). Revised other functions accordingly.

    2     2/09/00 7:24p Shiki
    Updated.

    8     10/14/99 3:30p Tian
    Added stddef.h

    7     10/08/99 12:54p Shiki
    Removed Mac related comment.

    6     9/23/99 4:59p Shiki
    Renamed 'errno' of PADStatus to 'err'.
    Changed type of chan from u32 to s32.

    5     9/15/99 6:31p Shiki
    Clean up.

    4     9/15/99 4:20p Shiki
    Bug fix.

    3     9/14/99 7:05p Shiki
    Revised.

    2     9/08/99 5:05p Shiki
    Fixed PADInit() to set en bits correctly.

    1     9/08/99 4:06p Shiki
    Initial check in.
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#include <dolphin/os.h>
#include <dolphin/vi.h>
#include <dolphin/pad.h>
#include <private/flipper.h>
#include <private/si.h>
#include "SIAssert.h"

static u32 SamplingRate;

/*---------------------------------------------------------------------------*
  Name:         SISetSamplingRate

  Description:  Sets the sampling rate in milliseconds.

  Arguments:    msec    Sampling rate in milliseconds. If 0 is specified,
                        the sampling rate is set so that SIRead() can get
                        the latest controller status if it is called just
                        after each vertical retrace interupt.

  Returns:      None.
 *---------------------------------------------------------------------------*/

typedef struct XY
{
    u16 line;
    u8  count;
} XY;

static struct XY XYNTSC[12] =
{
    263 - 17,   2,
    15,         18,
    30,         9,
    44,         6,
    52,         5,
    65,         4,
    87,         3,
    87,         3,
    87,         3,
    131,        2,
    131,        2,
    131,        2,
};

static struct XY XYPAL[12] =
{
    313 - 17,   2,
    15,         21,
    29,         11,
    45,         7,
    52,         6,
    63,         5,
    78,         4,
    104,        3,
    104,        3,
    104,        3,
    104,        3,
    156,        2,
};

void SISetSamplingRate(u32 msec)
{
    XY*  xy;
    BOOL progressive;
    BOOL enabled;

    ASSERTMSG(msec <= 11, SI_ERR_SETSAMPLINGRATE_INVARG);
    if (11 < msec)
    {
        msec = 11;
    }

    enabled = OSDisableInterrupts();

    SamplingRate = msec;

    switch (VIGetTvFormat())
    {
      case VI_NTSC:
      case VI_MPAL:
      case VI_EURGB60:
        xy = XYNTSC;
        break;
      case VI_PAL:
        xy = XYPAL;
        break;
      default:
        OSReport("SISetSamplingRate: unknown TV format. Use default.");
        msec = 0;
        xy = XYNTSC;
        break;
    }

    progressive = __VIRegs[VI_CLKSEL] & VI_CLKSEL_REG_VICLKSEL_MASK;
    SISetXY((progressive ? 2u : 1u) * xy[msec].line, xy[msec].count);
    OSRestoreInterrupts(enabled);
}

void SIRefreshSamplingRate(void)
{
    SISetSamplingRate(SamplingRate);
}

#ifdef _DEBUG

#define LATENCY 8

void __SITestSamplingRate(u32 tvmode);

void __SITestSamplingRate(u32 tvmode)
{
    u32 msec;
    u32 line;
    u32 count;
    XY* xy;

    switch (tvmode)
    {
      case VI_NTSC:
      case VI_MPAL:
        xy = XYNTSC;
        for (msec = 0; msec <= 11; msec++)
        {
            line  = xy[msec].line;
            count = xy[msec].count;

            // (1 / 13.5) * 429 = 63.6 [usec]
            // line * (count - 1) + LATENCY < 263

            OSReport("%2d[msec]: count %3d, line %3d, last %3d, diff0 %2d.%03d, diff1 %2d.%03d\n",
                     msec, count, line, line * (count - 1) + LATENCY,
                     636 * line / 10000,
                     636 * line % 10000,
                     636 * (263 - line * (count - 1)) / 10000,
                     636 * (263 - line * (count - 1)) % 10000);
            ASSERT(line * (count - 1) + LATENCY < 263);
            if (msec != 0)
            {
                ASSERT(636 * line < msec * 10000);
                ASSERT(636 * (263 - line * (count - 1)) < msec * 10000);
            }
        }
        break;
      case VI_PAL:
        xy = XYPAL;
        for (msec = 0; msec <= 11; msec++)
        {
            line  = xy[msec].line;
            count = xy[msec].count;

            // (1 / 13.5) * 432 = 64.0 [usec]
            // line * (count - 1) + LATENCY < 313

            OSReport("%2d[msec]: count %3d, line %3d, last %3d, diff0 %2d.%03d, diff1 %2d.%03d\n",
                     msec, count, line, line * (count - 1) + LATENCY,
                     640 * line / 10000,
                     640 * line % 10000,
                     640 * (313 - line * (count - 1)) / 10000,
                     640 * (313 - line * (count - 1)) % 10000);
            ASSERT(line * (count - 1) + LATENCY < 313);
            if (msec != 0)
            {
                ASSERT(640 * line < msec * 10000);
                ASSERT(640 * (313 - line * (count - 1)) < msec * 10000);
            }
        }
        break;
    }
}
#endif
