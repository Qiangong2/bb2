/*---------------------------------------------------------------------------*
  Project:  Serial Interface API
  File:     SIBios.c

  Copyright 1998-2002 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: SIBios.c,v $
  Revision 1.1.1.1  2004/01/03 01:02:28  paulm
  Gamecube library source from Nintendo

    
    27    8/07/02 21:45 Shiki
    Defined DOLPHIN_LIB_VERSION() and registered the library with
    OSRegisterVersion().

    26    12/26/01 9:05 Shiki
    Fixed typos in comments.

    25    11/22/01 16:02 Shiki
    Modified SISetXY() to set SI_POLL_IDX register immediately.

    24    11/22/01 10:29 Shiki
    Clean up.

    22    11/16/01 17:04 Shiki
    Added support for steering wheel.

    21    10/29/01 16:47 Shiki
    Fixed typo in SIGetTypeString().

    20    10/25/01 18:24 Shiki
    To support multiple auto mode serial devices, modified SIGetStatus() to
    check just a single channel at a time.

    19    10/19/01 13:49 Shiki
    Modified SIDecodeType() to make it decode GameCube keyboards.

    18    10/02/01 10:48 Shiki
    Fixed SIInterruptHandler() to eliminate waiting cycles for slow
    controllers.
    Fixed SISetXY() to accept ten bit /x/.

    17    9/08/01 14:37 Shiki
    Fixed SIInterruptHandler() to recover failed SITransfer() for SIGetType
    ().

    16    9/07/01 23:18 Shiki
    Added SIIsChanBusy().

    15    9/07/01 15:46 Shiki
    Fixed not to reset Type[] while probing the port.

    14    9/06/01 19:40 Shiki
    Fixed interrupt handling code.

    13    9/06/01 15:30 Shiki
    Fixed SI xfer delay for SIGetType().

    12    9/03/01 14:24 Shiki
    Added SIProbe(), etc.

    11    7/12/01 11:22 Shiki
    Modified SIEnablePolling() not to disable polling temporarily.

    10    5/21/01 1:14p Shiki
    Added SIBusy().

    9     01/03/22 11:38 Shiki
    Added SIGetCommand().

    8     01/03/06 11:39 Shiki
    Modified SISetXY() for latency change (from 7 to 8).

    7     01/01/30 14:14 Shiki
    Changed SITransfer() interface.

    6     10/23/00 3:16p Shiki
    Fixed SIInit() to wait for transfer to complete for safety.

    5     3/21/00 5:03p Shiki
    Revised SIEnableablePolling() and SIDisablePolling().

    4     3/10/00 3:34p Shiki
    Added SITransferCommands().

    3     3/10/00 2:27p Shiki
    Modified SIEnablePolling().

    2     2/09/00 7:23p Shiki
    Changed SITransfer param order.

    8     9/23/99 4:59p Shiki
    Changed type of chan from u32 to s32.

    7     9/21/99 6:30p Shiki
    Fixed SIInterruptHandler() to uninstall callback before calling it.

    6     9/15/99 4:56p Shiki
    Added assert messages.

    5     9/14/99 7:04p Shiki
    Revised command mode stuff.

    4     9/08/99 4:52p Shiki
    Fixed SIInit() to disable TCINT by default.

    3     9/08/99 4:06p Shiki
    Revised SISetCommand() interface.

    2     9/07/99 8:00p Shiki
    Revised.

    1     9/03/99 7:08p Shiki
    Initial check-in.
 *---------------------------------------------------------------------------*/

#include <dolphin/doldefs.h>
DOLPHIN_LIB_VERSION(SI);
/* $NoKeywords: $ */

#include <dolphin/os.h>
#include <dolphin/vi.h>
#include <private/flipper.h>
#include <private/si.h>
#include <private/OSRtc.h>
#include "SIAssert.h"

// Expected max # of controller types (pad, kbd, gba, xxx)
#define SI_MAX_TYPE             4

#define CMD_TYPE_AND_STATUS     0x00u       // 1-3
#define CMD_SYSTEM_RESET        0xFFu       // 1-3

// Wireless specific commands
#define CMD_FIX_DEVICE          0x4Eu       // 3-3
#define CMD_UNFIX_DEVICE        0x4Fu

#define CHAN_NONE       -1

#define OFFSET(n, a)    (((u32) (n)) & ((a) - 1))
#define TRUNC(n, a)     (((u32) (n)) & ~((a) - 1))
#define ROUND(n, a)     (((u32) (n) + (a) - 1) & ~((a) - 1))

typedef struct SIControl
{
    s32         chan;
    u32         poll;
    u32         inputBytes;
    void*       input;
    SICallback  callback;
} SIControl;

static SIControl Si =
{
    CHAN_NONE,  // chan
};

static SIPacket  Packet[SI_MAX_CHAN];
static OSAlarm   Alarm[SI_MAX_CHAN];
static u32       Type[SI_MAX_CHAN] =
{
    SI_ERROR_NO_RESPONSE,
    SI_ERROR_NO_RESPONSE,
    SI_ERROR_NO_RESPONSE,
    SI_ERROR_NO_RESPONSE
};
static OSTime    TypeTime[SI_MAX_CHAN];
static OSTime    XferTime[SI_MAX_CHAN];

static SITypeAndStatusCallback TypeCallback[SI_MAX_CHAN][SI_MAX_TYPE];
static __OSInterruptHandler    RDSTHandler[SI_MAX_TYPE];

u32 __PADFixBits;   // Set to ~0 in the boot ROM

static
BOOL __SITransfer(s32        chan,
                  void*      output,
                  u32        outputBytes,
                  void*      input,
                  u32        inputBytes,
                  SICallback callback);

static BOOL InputBufferValid[SI_MAX_CHAN];
static u32  InputBuffer[SI_MAX_CHAN][2];
static vu32 InputBufferVcount[SI_MAX_CHAN];

static BOOL SIGetResponseRaw(s32 chan);
static void GetTypeCallback(s32 chan, u32 error, OSContext* context);

/*---------------------------------------------------------------------------*
  Name:         SIBusy

  Description:  Checks if SI xfer is busy

  Arguments:    None.

  Returns:      TRUE if SI has an on-going xfer or waiting xfer requests.
 *---------------------------------------------------------------------------*/
BOOL SIBusy(void)
{
    return (Si.chan != CHAN_NONE) ? TRUE : FALSE;
}

/*---------------------------------------------------------------------------*
  Name:         SIIsChanBusy

  Description:  Checks if there is an on-going xfer on the specified channel
                Should be called after interrupts are disabled.

  Arguments:    chan        channel to inspect

  Returns:      TRUE if SI has an on-going xfer or waiting xfer requests.
 *---------------------------------------------------------------------------*/
BOOL SIIsChanBusy(s32 chan)
{
    return Packet[chan].chan != CHAN_NONE || Si.chan == chan;
}

/*---------------------------------------------------------------------------*
  Name:         SIClearTCInterrupt

  Description:  Clear TC interrupt

  Arguments:    None.

  Returns:      None.
 *---------------------------------------------------------------------------*/
static void SIClearTCInterrupt(void)
{
    u32 reg;

    reg = __SIRegs[SI_COMCSR_IDX];
    reg |= SI_COMCSR_TCINT_MASK;
    reg &= ~SI_COMCSR_TSTART_MASK;
    __SIRegs[SI_COMCSR_IDX] = reg;
}

/*---------------------------------------------------------------------------*
  Name:         CompleteTransfer

  Description:  Copies data read through SI to the user specified input
                buffer.

  Arguments:    None.

  Returns:      OR-ed SI_ERROR_*
 *---------------------------------------------------------------------------*/
static u32 CompleteTransfer(void)
{
    u32 sr;
    u32 i;
    u32 rLen;
    u8* input;

    // Get error status
    sr = __SIRegs[SI_SR_IDX];

    // Clear the irq
    SIClearTCInterrupt();

    if (Si.chan != CHAN_NONE)
    {
        XferTime[Si.chan] = __OSGetSystemTime();

        input = Si.input;

        // Copy in the data by multiple of 4 bytes
        rLen = Si.inputBytes / 4;
        for (i = 0 ; i < rLen ; i++)
        {
            *(u32*) input = __SIRegs[SI_RAM_IDX + i];
            input += 4;
        }

        // Copy in the data by multiple of a single byte
        rLen = OFFSET(Si.inputBytes, 4);
        if (rLen)
        {
            u32 temp = __SIRegs[SI_RAM_IDX + i];
            for (i = 0 ; i < rLen ; i++)
            {
                *input++ = (u8) ((temp >> ((3 - i) * 8)) & 0xff);
            }
        }

        if (__SIRegs[SI_COMCSR_IDX] & SI_COMCSR_COMERR_MASK)
        {
            // Error on the last communication transfer
            sr >>= SI_SR_UNRUN2_SHIFT * (3 - Si.chan);
            sr &= (SI_SR_UNRUN3_MASK | SI_SR_OVRUN3_MASK |
                   SI_SR_COLL3_MASK  | SI_SR_NOREP3_MASK);

            if ((sr & SI_ERROR_NO_RESPONSE) && !(Type[Si.chan] & SI_ERROR_BUSY))
            {
                Type[Si.chan] = SI_ERROR_NO_RESPONSE;
            }
            if (sr == 0)
            {
                sr = SI_ERROR_COLLISION;    // Anyways...
            }
        }
        else
        {
            // No error
            TypeTime[Si.chan] = __OSGetSystemTime();
            sr = 0;
        }

        Si.chan = CHAN_NONE;
    }

    // Return error states.
    return sr;
}

/*---------------------------------------------------------------------------*
  Name:         SITransferNext

  Description:  Sets up the next DMA transfer if any

  Arguments:    chan    Previously active channel number

  Returns:      None.
 *---------------------------------------------------------------------------*/
static void SITransferNext(s32 chan)
{
    int       i;
    SIPacket* packet;

    for (i = 0; i < SI_MAX_CHAN; ++i)
    {
        ++chan;
        chan %= SI_MAX_CHAN;
        packet = &Packet[chan];
        if (packet->chan != CHAN_NONE && packet->fire <= __OSGetSystemTime())
        {
            if (__SITransfer(packet->chan,
                             packet->output, packet->outputBytes,
                             packet->input,  packet->inputBytes,
                             packet->callback))
            {
                OSCancelAlarm(&Alarm[chan]);
                packet->chan = CHAN_NONE;
            }
            break;
        }
    }
}

/*---------------------------------------------------------------------------*
  Name:         SIInterruptHandler

  Description:  Interrupt handler for serial interface

  Arguments:    interrupt   interrupt number
                context     current context

  Returns:      None.
 *---------------------------------------------------------------------------*/
static void SIInterruptHandler(__OSInterrupt interrupt, OSContext* context)
{
    u32    reg;

    reg = __SIRegs[SI_COMCSR_IDX];

    if ((reg & (SI_COMCSR_TCINT_MASK | SI_COMCSR_TCINTMSK_MASK)) ==
        (SI_COMCSR_TCINT_MASK | SI_COMCSR_TCINTMSK_MASK))
    {
        s32        chan;
        u32        sr;
        SICallback callback;

        ASSERT(Si.chan != CHAN_NONE);
        chan = Si.chan;
        sr = CompleteTransfer();
        callback = Si.callback;
        Si.callback = 0;

        SITransferNext(chan);

        if (callback)
        {
            callback(chan, sr, context);
        }

        // Clear error status bits of the specified channel since
        // auto mode transfer has been canceled until the next
        // vsync.
        sr = __SIRegs[SI_SR_IDX];
        sr &= (SI_SR_NOREP0_MASK | SI_SR_COLL0_MASK  |
               SI_SR_OVRUN0_MASK | SI_SR_UNRUN0_MASK) >>
              (SI_SR_UNRUN2_SHIFT * chan);
        __SIRegs[SI_SR_IDX] = sr;

        // If not all the entries in TypeCallback[chan][] are NULL and
        // there is no on-going SI transfer in the current channel,
        // it is likely SIGetType() failed to initiate SITransfer().
        // So restart it on behalf of SIGetType().
        if (Type[chan] == SI_ERROR_BUSY && !SIIsChanBusy(chan))
        {
            static u32 cmdTypeAndStatus = CMD_TYPE_AND_STATUS << 24; // 1 - 3

            SITransfer(chan, &cmdTypeAndStatus, 1, &Type[chan], 3, GetTypeCallback,
                       OSMicrosecondsToTicks(65));
        }
    }

    if ((reg & (SI_COMCSR_RDSTINT_MASK | SI_COMCSR_RDSTINTMSK_MASK)) ==
        (SI_COMCSR_RDSTINT_MASK | SI_COMCSR_RDSTINTMSK_MASK))
    {
        // RDSTINT is raised whenever a controller port captures new
        // data. For example, WaveBird receiver responds about 25 usec
        // later than the standard controllers.
        // We should call RDSTHandler() once just after every controller
        // port has captured new data.

        int i;
        u32 vcount;
        u32 x;

        vcount = VIGetCurrentLine() + 1;
        x = (Si.poll & SI_POLL_X_MASK) >> SI_POLL_X_SHIFT;

        for (i = 0; i < SI_MAX_CHAN; ++i)
        {
            if (SIGetResponseRaw(i))
            {
                InputBufferVcount[i] = vcount;
            }
        }

        for (i = 0; i < SI_MAX_CHAN; ++i)
        {
            if (!(Si.poll & (SI_CHAN0_BIT >> (31 - SI_POLL_EN0_SHIFT + i))))
            {
                continue;
            }
            if (InputBufferVcount[i] == 0 ||
                InputBufferVcount[i] + (x / 2) < vcount)
            {
                return;
                // NOT REACHED HERE
            }
        }

        for (i = 0; i < SI_MAX_CHAN; ++i)
        {
            InputBufferVcount[i] = 0;
        }

        for (i = 0; i < SI_MAX_TYPE; ++i)
        {
            if (RDSTHandler[i])
            {
                RDSTHandler[i](interrupt, context);
            }
        }
    }
}

/*---------------------------------------------------------------------------*
  Name:         SIEnablePollingInterrupt

  Description:  Enables/disables polling interrupts

  Arguments:    enable  Set TRUE to enable interrupts.
                        Set FALSE to disable interrupts.

  Returns:      previous state
 *---------------------------------------------------------------------------*/
static BOOL SIEnablePollingInterrupt(BOOL enable)
{
    BOOL enabled;
    BOOL rc;
    u32  reg;
    int  i;

    enabled = OSDisableInterrupts();
    reg = __SIRegs[SI_COMCSR_IDX];
    rc = (reg & SI_COMCSR_RDSTINTMSK_MASK) ? TRUE : FALSE;
    if (enable)
    {
        reg |= SI_COMCSR_RDSTINTMSK_MASK;
        for (i = 0; i < SI_MAX_CHAN; ++i)
        {
            InputBufferVcount[i] = 0;
        }
    }
    else
    {
        reg &= ~SI_COMCSR_RDSTINTMSK_MASK;
    }
    reg &= ~(SI_COMCSR_TSTART_MASK | SI_COMCSR_TCINT_MASK);
    __SIRegs[SI_COMCSR_IDX] = reg;
    OSRestoreInterrupts(enabled);
    return rc;
}

BOOL SIRegisterPollingHandler(__OSInterruptHandler handler)
{
    BOOL enabled;
    int  i;

    enabled = OSDisableInterrupts();
    for (i = 0; i < SI_MAX_TYPE; ++i)
    {
        if (RDSTHandler[i] == handler)
        {
            OSRestoreInterrupts(enabled);
            return TRUE;
        }
    }
    for (i = 0; i < SI_MAX_TYPE; ++i)
    {
        if (RDSTHandler[i] == 0)
        {
            RDSTHandler[i] = handler;
            SIEnablePollingInterrupt(TRUE);
            OSRestoreInterrupts(enabled);
            return TRUE;
        }
    }
    OSRestoreInterrupts(enabled);
    return FALSE;
}

BOOL SIUnregisterPollingHandler(__OSInterruptHandler handler)
{
    BOOL enabled;
    int  i;

    enabled = OSDisableInterrupts();
    for (i = 0; i < SI_MAX_TYPE; ++i)
    {
        if (RDSTHandler[i] == handler)
        {
            RDSTHandler[i] = 0;
            for (i = 0; i < SI_MAX_TYPE; ++i)
            {
                if (RDSTHandler[i])
                {
                    break;
                }
            }
            if (i == SI_MAX_TYPE)
            {
                SIEnablePollingInterrupt(FALSE);
            }
            OSRestoreInterrupts(enabled);
            return TRUE;
            break;
        }
    }
    OSRestoreInterrupts(enabled);
    return FALSE;
}

/*---------------------------------------------------------------------------*
  Name:         SIInit

  Description:  Initializes serial interface. Called by OSInit().

  Arguments:    None.

  Returns:      None.
 *---------------------------------------------------------------------------*/
void SIInit(void)
{
    OSRegisterVersion(__SIVersion);

    Packet[0].chan = Packet[1].chan = Packet[2].chan = Packet[3].chan = CHAN_NONE;

    // Disable polling and initializes sampling rate to default value.
    Si.poll = 0;
    SISetSamplingRate(0);

    // Wait for transfer to complete
    while (__SIRegs[SI_COMCSR_IDX] & SI_COMCSR_TSTART_MASK)
        ;

    // Disable TCINT and clear pending interrupt if any
    __SIRegs[SI_COMCSR_IDX] = SI_COMCSR_TCINT_MASK;

    __OSSetInterruptHandler(__OS_INTERRUPT_PI_SI, SIInterruptHandler);
    __OSUnmaskInterrupts(OS_INTERRUPTMASK_PI_SI);

    SIGetType(0);
    SIGetType(1);
    SIGetType(2);
    SIGetType(3);
}

/*---------------------------------------------------------------------------*
  Name:         __SITransfer

  Description:  Sets up a DMA transfer between main memory and SI device.

  Arguments:    chan        channel to communicate
                output      pointer to write buffer (byte aligned)
                outputBytes Number of bytes to be write. Max 128 bytes.
                input       pointer to read buffer (byte aligned)
                inputBytes  Number of bytes to be read. Max 128 bytes.
                callback    function to be called back after transfer
                            completion. 0 specifies SISync() will be
                            called afterwards.

  Returns:      TRUE if transfer is successfully initiated. Otherwise,
                FALSE.
 *---------------------------------------------------------------------------*/
static
BOOL __SITransfer(s32        chan,
                  void*      output,
                  u32        outputBytes,
                  void*      input,
                  u32        inputBytes,
                  SICallback callback)
{
    BOOL enabled;
    u32  rLen;
    u32  i;
    u32  sr;
    si_comcsr_u comcsr;

    ASSERTMSG(0 <= chan && chan < SI_MAX_CHAN,  SI_ERR_TRANSFER_INVCHAN);
    ASSERTMSG(0 < outputBytes && outputBytes <= SI_MAX_COMCSR_OUTLNGTH,
                                                SI_ERR_TRANSFER_INVOUTBYTES);
    ASSERTMSG(0 < inputBytes  && inputBytes  <= SI_MAX_COMCSR_INLNGTH,
                                                SI_ERR_TRANSFER_INVINBYTES);

    enabled = OSDisableInterrupts();
    if (Si.chan != CHAN_NONE)
    {
        OSRestoreInterrupts(enabled);
        return FALSE;
    }

    ASSERT((__SIRegs[SI_COMCSR_IDX] &
           (SI_COMCSR_TSTART_MASK | SI_COMCSR_TCINT_MASK)) == 0);

    // Clear error status bits of the specified channel
    sr = __SIRegs[SI_SR_IDX];
    sr &= (SI_SR_NOREP0_MASK | SI_SR_COLL0_MASK  |
           SI_SR_OVRUN0_MASK | SI_SR_UNRUN0_MASK) >>
          (SI_SR_UNRUN2_SHIFT * chan);
    __SIRegs[SI_SR_IDX] = sr;

    Si.chan       = chan;
    Si.callback   = callback;
    Si.inputBytes = inputBytes;
    Si.input      = input;

    // Copy out the data
    rLen = ROUND(outputBytes, 4) / 4;
    for (i = 0 ; i < rLen ; i++)
    {
        __SIRegs[SI_RAM_IDX + i] = ((u32*) output)[i];
    }

    // Set up COMmunication Control Status Register
    comcsr.val = __SIRegs[SI_COMCSR_IDX];
    comcsr.f.tcint    = 1;  // clear irq
    comcsr.f.tcintmsk = callback ? 1 : 0;
    comcsr.f.outlngth = (outputBytes == SI_MAX_COMCSR_OUTLNGTH) ? 0 : outputBytes;
    comcsr.f.inlngth  = (inputBytes  == SI_MAX_COMCSR_INLNGTH)  ? 0 : inputBytes;
    comcsr.f.channel  = chan;
    comcsr.f.tstart   = 1;
    __SIRegs[SI_COMCSR_IDX] = comcsr.val;

    OSRestoreInterrupts(enabled);

    return TRUE;
}

/*---------------------------------------------------------------------------*
  Name:         SISync

  Description:  Synchronizes with the previous call to SITransfer(). The
                callback parameter for that SITransfer() must have been 0.

  Arguments:    None.

  Returns:      OR-ed SI_ERROR_*
 *---------------------------------------------------------------------------*/
u32 SISync(void)
{
    BOOL enabled;
    u32  sr;

    // Wait for transfer to complete
    while (__SIRegs[SI_COMCSR_IDX] & SI_COMCSR_TSTART_MASK)
        ;

    enabled = OSDisableInterrupts();
    sr = CompleteTransfer();

    SITransferNext(SI_MAX_CHAN);

    OSRestoreInterrupts(enabled);

    // Return status
    return sr;
}

/*---------------------------------------------------------------------------*
  Name:         SIGetStatus

  Description:  Gets current SI status for auto mode driver

  Arguments:    chan        channel to get status

  Returns:      current SI status (SI_ERROR_*)
 *---------------------------------------------------------------------------*/
u32 SIGetStatus(s32 chan)
{
    BOOL enabled;
    u32  sr;
    int  chanShift;

    enabled = OSDisableInterrupts();
    sr = __SIRegs[SI_SR_IDX];
    chanShift = 8 * (SI_MAX_CHAN - 1 - chan);
    sr >>= chanShift;
    if (sr & SI_ERROR_NO_RESPONSE)
    {
        if (!(Type[chan] & SI_ERROR_BUSY))
        {
            Type[chan] = SI_ERROR_NO_RESPONSE;
        }
    }
    OSRestoreInterrupts(enabled);
    return sr;
}

/*---------------------------------------------------------------------------*
  Name:         SISetCommand

  Description:  Sets polling command for the specified channel

  Arguments:    chan        channel to set command
                command     command bytes as below:
                                23:16   cmd
                                15: 8   output0
                                 7: 0   output1

  Returns:      None.
 *---------------------------------------------------------------------------*/
void SISetCommand(s32 chan, u32 command)
{
    ASSERTMSG(0 <= chan && chan < SI_MAX_CHAN,  SI_ERR_SETCOMMAND_INVCHAN);
    __SIRegs[SI_1OUTBUF_IDX * chan] = command;
}

/*---------------------------------------------------------------------------*
  Name:         SIGetCommand

  Description:  Gets polling command set for the specified channel

  Arguments:    chan        channel to set command

  Returns:      command     command bytes as below:
                                23:16   cmd
                                15: 8   output0
                                 7: 0   output1
 *---------------------------------------------------------------------------*/
u32 SIGetCommand(s32 chan)
{
    ASSERTMSG(0 <= chan && chan < SI_MAX_CHAN,  SI_ERR_GETCOMMAND_INVCHAN);
    return __SIRegs[SI_1OUTBUF_IDX * chan];
}

/*---------------------------------------------------------------------------*
  Name:         SITransferCommands

  Description:  Transfers output buffers out

  Arguments:    None.

  Returns:      None.
 *---------------------------------------------------------------------------*/
void SITransferCommands(void)
{
    __SIRegs[SI_SR_IDX] = SI_SR_WR_MASK;
}

/*---------------------------------------------------------------------------*
  Name:         SISetXY

  Description:  Sets X-Y polling timing.

  Arguments:    x           per /x/ line (8 <= x <= 1023)
                y           /y/ times    (0 <= y <= 255)

  Returns:      updated poll register value
 *---------------------------------------------------------------------------*/
u32 SISetXY(u32 x, u32 y)
{
    u32  poll;
    BOOL enabled;

    ASSERTMSG(8 <= x,                           SI_ERR_SETXY_INVX);
    ASSERTMSG(x <= 1023,                        SI_ERR_SETXY_INVX);
    ASSERTMSG(y <= 255,                         SI_ERR_SETXY_INVY);
    poll =  x << SI_POLL_X_SHIFT;
    poll |= y << SI_POLL_Y_SHIFT;

    enabled = OSDisableInterrupts();
    Si.poll &= ~(SI_POLL_X_MASK | SI_POLL_Y_MASK);
    Si.poll |= poll;
    poll = Si.poll;
    __SIRegs[SI_POLL_IDX] = poll;
    OSRestoreInterrupts(enabled);
    return poll;
}

/*---------------------------------------------------------------------------*
  Name:         SIEnablePolling

  Description:  Enable polling for the channels which SI_POLL_ENn_MASK
                bit is set. SI_POLL_VBCPYn_MASK bits are also set.

  Arguments:    poll        OR-ed SI_CHANn_BITs. (1 = Enable)

  Returns:      updated poll register value
 *---------------------------------------------------------------------------*/
u32 SIEnablePolling(u32 poll)
{
    BOOL enabled;
    u32  en;

    ASSERTMSG(!(poll & ~(SI_CHAN0_BIT|SI_CHAN1_BIT|SI_CHAN2_BIT|SI_CHAN3_BIT)),
              SI_ERR_ENABLEPOLLING_INVCHAN);

    if (poll == 0)
    {
        return Si.poll;
        // NOT REACHED HERE
    }

    enabled = OSDisableInterrupts();

#if 0
    // 7/12/2001 -- Nagaoka-san, IRD, confirmed we don't have to disable
    //              polling temporarily.

    // Disable polling temporarily
    __SIRegs[SI_POLL_IDX] = 0;
#endif

#if 0
    // 3/9/2000 -- Nagaoka-san, RD3, confirmed we don't have to wait
    //             for outbuf buffers to be copied. Also it will take
    //             at most 106 microseconds.

    // Wait for outbuf buffers to be copied
    while (__SIRegs[SI_SR_IDX] & SI_SR_WR_MASK)
        ;
#endif

    poll >>= (31 - SI_POLL_EN0_SHIFT);
    en = poll & (SI_POLL_EN0_MASK | SI_POLL_EN1_MASK |
                 SI_POLL_EN2_MASK | SI_POLL_EN3_MASK);
    ASSERT(en);

    // Clear VBCPYn bits where ENn bits are not set
    poll &= (en >> SI_POLL_EN3_SHIFT) | (SI_POLL_EN0_MASK | SI_POLL_EN1_MASK |
                                         SI_POLL_EN2_MASK | SI_POLL_EN3_MASK |
                                         SI_POLL_X_MASK | SI_POLL_Y_MASK);

    // Clear X and Y field
    poll &= ~(SI_POLL_X_MASK | SI_POLL_Y_MASK);

    // Clear VBCPYn bits where ENn bits are set
    Si.poll &= ~(en >> SI_POLL_EN3_SHIFT);

    Si.poll |= poll;

    poll = Si.poll;

    // Transfer output buffers out
    SITransferCommands();

    // Enable polling
    __SIRegs[SI_POLL_IDX] = poll;

    OSRestoreInterrupts(enabled);

    return poll;
}

/*---------------------------------------------------------------------------*
  Name:         SIDisablePolling

  Description:  Disables polling for the channels which SI_POLL_ENn_MASK
                bits is set.

  Arguments:    poll        OR-ed SI_CHANn_BITs. (1 = Disable)

  Returns:      updated poll register value
 *---------------------------------------------------------------------------*/
u32 SIDisablePolling(u32 poll)
{
    BOOL enabled;

    ASSERTMSG(!(poll & ~(SI_CHAN0_BIT|SI_CHAN1_BIT|SI_CHAN2_BIT|SI_CHAN3_BIT)),
              SI_ERR_DISABLEPOLLING_INVCHAN);

    if (poll == 0)
    {
        return Si.poll;
        // NOT REACHED HERE
    }

    enabled = OSDisableInterrupts();

    poll >>= (31 - SI_POLL_EN0_SHIFT);
    poll &= (SI_POLL_EN0_MASK | SI_POLL_EN1_MASK |
             SI_POLL_EN2_MASK | SI_POLL_EN3_MASK);
    ASSERT(poll);
    poll = Si.poll & ~poll;

    __SIRegs[SI_POLL_IDX] = poll;
    Si.poll = poll;

    OSRestoreInterrupts(enabled);
    return poll;
}

/*---------------------------------------------------------------------------*
  Name:         SIGetResponseRaw

  Description:  Updates InputBuffer[chan] and InputBufferValid[chan]

  Arguments:    chan        channel to check status

  Returns:      TRUE if captured new data
 *---------------------------------------------------------------------------*/
static BOOL SIGetResponseRaw(s32 chan)
{
    u32 sr;

    sr = SIGetStatus(chan);
    if (sr & SI_ERROR_RDST)
    {
        InputBuffer[chan][0] = __SIRegs[SI_1OUTBUF_IDX * chan + SI_0INBUFH_IDX];
        InputBuffer[chan][1] = __SIRegs[SI_1OUTBUF_IDX * chan + SI_0INBUFL_IDX];
        InputBufferValid[chan] = TRUE;
        return TRUE;
    }
    return FALSE;
}

/*---------------------------------------------------------------------------*
  Name:         SIGetResponse

  Description:  Read polling commands results of all the channels

  Arguments:    chan        channel to check status
                data        8 byte buffer to receive command response

  Returns:      TRUE if data has been captured.
                FALSE if data has been read by the CPU.
 *---------------------------------------------------------------------------*/
BOOL SIGetResponse(s32 chan, void* data)
{
    BOOL rc;
    BOOL enabled;

    ASSERTMSG(0 <= chan && chan < SI_MAX_CHAN,  SI_ERR_GETRESPONSE_INVCHAN);
    enabled = OSDisableInterrupts();
    SIGetResponseRaw(chan);
    rc = InputBufferValid[chan];
    InputBufferValid[chan] = FALSE;
    if (rc)
    {
        ((u32*) data)[0] = InputBuffer[chan][0];
        ((u32*) data)[1] = InputBuffer[chan][1];
    }
    OSRestoreInterrupts(enabled);
    return rc;
}

/*---------------------------------------------------------------------------*
  Name:         AlarmHandler

  Description:  Processes deferred SI xfer

  Arguments:    alarm       pointer to expired alarm
                context     not used

  Returns:      None.
 *---------------------------------------------------------------------------*/
static void AlarmHandler(OSAlarm* alarm, OSContext* context)
{
    #pragma unused (context)
    s32       chan;
    SIPacket* packet;

    chan = alarm - Alarm;
    ASSERT(0 <= chan && chan < SI_MAX_CHAN);
    packet = &Packet[chan];
    if (packet->chan != CHAN_NONE)
    {
        ASSERT(packet->fire <= __OSGetSystemTime());
        if (__SITransfer(packet->chan,
                         packet->output, packet->outputBytes,
                         packet->input,  packet->inputBytes,
                         packet->callback))
        {
            packet->chan = CHAN_NONE;
        }
    }
}

/*---------------------------------------------------------------------------*
  Name:         SITransfer

  Description:  Sets up a DMA transfer between main memory and SI device.

  Arguments:    chan        channel to communicate
                output      pointer to write buffer (byte aligned)
                outputBytes Number of bytes to be write. Max 128 bytes.
                input       pointer to read buffer (byte aligned)
                inputBytes  Number of bytes to be read. Max 128 bytes.
                callback    function to be called back after transfer
                            completion. 0 specifies SISync() will be
                            called afterwards.
                delay       minimum delay since the previous communication
                            transfer. Set to zero to start immediately.

  Note:         outputBytes must be kept by the caller since it is not
                copied to the OS internal buffer.

  Returns:      TRUE if transfer is successfully initiated. Otherwise,
                FALSE.
 *---------------------------------------------------------------------------*/
BOOL SITransfer(s32        chan,
                void*      output,
                u32        outputBytes,
                void*      input,
                u32        inputBytes,
                SICallback callback,
                OSTime     delay)
{
    BOOL      enabled;
    SIPacket* packet = &Packet[chan];
    OSTime    now;
    OSTime    fire;

    enabled = OSDisableInterrupts();
    if (packet->chan != CHAN_NONE || Si.chan == chan)
    {
        OSRestoreInterrupts(enabled);
        return FALSE;
    }

    now = __OSGetSystemTime();
    if (delay == 0)
    {
        fire = now;
    }
    else
    {
        fire = XferTime[chan] + delay;
    }
    if (now < fire)
    {
        delay = fire - now;
        OSSetAlarm(&Alarm[chan], delay, AlarmHandler);
    }
    else if (__SITransfer(chan, output, outputBytes, input, inputBytes, callback))
    {
        OSRestoreInterrupts(enabled);
        return TRUE;
    }

    packet->chan        = chan;
    packet->output      = output;
    packet->outputBytes = outputBytes;
    packet->input       = input;
    packet->inputBytes  = inputBytes;
    packet->callback    = callback;
    packet->fire        = fire;

    OSRestoreInterrupts(enabled);
    return TRUE;
}

/*---------------------------------------------------------------------------*
  Name:         CallTypeAndStatusCallback

  Description:  Calls user specified type and status callback functions

  Arguments:    chan        SI channel
                type        controller type

  Returns:      None.
 *---------------------------------------------------------------------------*/
static void CallTypeAndStatusCallback(s32 chan, u32 type)
{
    SITypeAndStatusCallback callback;
    int                     i;

    for (i = 0; i < SI_MAX_TYPE; ++i)
    {
        callback = TypeCallback[chan][i];
        if (callback)
        {
            TypeCallback[chan][i] = 0;
            callback(chan, type);
        }
    }
}

/*---------------------------------------------------------------------------*
  Name:         GetTypeCallback

  Description:  The callback function for SITransfer() used by SIGetType()

  Arguments:    chan        SI channel
                error       SI error code
                context     interrupt context

  Returns:      None.
 *---------------------------------------------------------------------------*/
static void GetTypeCallback(s32 chan, u32 error, OSContext* context)
{
    #pragma unused( context )
    static u32 cmdFixDevice[SI_MAX_CHAN];                           // 3
    u32  type;
    u32  chanBit;
    BOOL fix;
    u32  id;

    ASSERT(0 <= chan && chan < SI_MAX_CHAN);

    ASSERT((Type[chan] & 0xff) == SI_ERROR_BUSY);
    Type[chan] &= ~SI_ERROR_BUSY;
    Type[chan] |= error;
    TypeTime[chan] = __OSGetSystemTime();

    type = Type[chan];

    chanBit = SI_CHAN0_BIT >> chan;
    fix = (BOOL) (__PADFixBits & chanBit);
    __PADFixBits &= ~chanBit;

    //
    // Error check, etc.
    //
    if ((error & (SI_ERROR_UNDER_RUN | SI_ERROR_OVER_RUN | SI_ERROR_NO_RESPONSE | SI_ERROR_COLLISION)) ||
        (type & SI_TYPE_MASK) != SI_TYPE_DOLPHIN ||
        !(type & SI_GC_WIRELESS) || (type & SI_WIRELESS_IR))
    {
        // Clear registered ID
        OSSetWirelessID(chan, 0);
        CallTypeAndStatusCallback(chan, Type[chan]);
        return;
        // NOT REACHED HERE
    }

    //
    // Wireless controller
    //
    id = (u32) (OSGetWirelessID(chan) << 8);

    // Boot ROM version
    if (fix && (id & SI_WIRELESS_FIX_ID))
    {
        // Issue fix command
        cmdFixDevice[chan] = CMD_FIX_DEVICE << 24 | (id & SI_WIRELESS_TYPE_ID) | SI_WIRELESS_FIX_ID;
        Type[chan] = SI_ERROR_BUSY;
        SITransfer(chan, &cmdFixDevice[chan], 3, &Type[chan], 3, GetTypeCallback, 0);
        return;
        // NOT REACHED HERE
    }

    if (type & SI_WIRELESS_FIX_ID)
    {
        // Check ID
        if ((id & SI_WIRELESS_TYPE_ID) != (type & SI_WIRELESS_TYPE_ID))
        {
            if (!(id & SI_WIRELESS_FIX_ID))    // Sram broken?
            {
                id = type & SI_WIRELESS_TYPE_ID;
                id |= SI_WIRELESS_FIX_ID;
                // Register ID
                OSSetWirelessID(chan, (u16) ((id >> 8) & 0xffff));
            }

            // Issue fix command (retry)
            cmdFixDevice[chan] = CMD_FIX_DEVICE << 24 | id;
            Type[chan] = SI_ERROR_BUSY;
            SITransfer(chan, &cmdFixDevice[chan], 3, &Type[chan], 3, GetTypeCallback, 0);
            return;
            // NOT REACHED HERE
        }
    }
    else if (type & SI_WIRELESS_RECEIVED)
    {
        id = type & SI_WIRELESS_TYPE_ID;
        id |= SI_WIRELESS_FIX_ID;

        // Register ID
        OSSetWirelessID(chan, (u16) ((id >> 8) & 0xffff));

        // Issue fix command
        cmdFixDevice[chan] = CMD_FIX_DEVICE << 24 | id;
        Type[chan] = SI_ERROR_BUSY;
        SITransfer(chan, &cmdFixDevice[chan], 3, &Type[chan], 3, GetTypeCallback, 0);
        return;
        // NOT REACHED HERE
    }
    else
    {
        // Clear registered ID
        OSSetWirelessID(chan, 0);
    }

    CallTypeAndStatusCallback(chan, Type[chan]);
}

/*---------------------------------------------------------------------------*
  Name:         SIGetType

  Description:  Queries controller type and status

  Arguments:    chan        Channel to query type and status

  Returns:      type & status : error
 *---------------------------------------------------------------------------*/
u32 SIGetType(s32 chan)
{
    static u32 cmdTypeAndStatus = CMD_TYPE_AND_STATUS << 24; // 1 - 3
    BOOL   enabled;
    u32    type;
    OSTime diff;

    enabled = OSDisableInterrupts();

    ASSERT(0 <= chan && chan < SI_MAX_CHAN);
    type = Type[chan];
    diff = __OSGetSystemTime() - TypeTime[chan];
    if (Si.poll & (SI_POLL_EN0_MASK >> chan))
    {
        if (type != SI_ERROR_NO_RESPONSE)
        {
            // As long as auto mode is set, SIGetType() assumes the
            // current controller is still attached. However, issuing
            // a communication command could change Type[chan] to
            // SI_ERROR_NO_RESPONSE though.
            TypeTime[chan] = __OSGetSystemTime();
            OSRestoreInterrupts(enabled);
            return type;
            // NOT REACHED HERE
        }
        else
        {
            type = Type[chan] = SI_ERROR_BUSY;
        }
    }
    else if (diff <= OSMillisecondsToTicks(50) && type != SI_ERROR_NO_RESPONSE)
    {
        OSRestoreInterrupts(enabled);
        return type;
        // NOT REACHED HERE
    }
    else if (diff <= OSMillisecondsToTicks(75))
    {
        Type[chan] = SI_ERROR_BUSY;
    }
    else
    {
        type = Type[chan] = SI_ERROR_BUSY;
    }
    TypeTime[chan] = __OSGetSystemTime();

    // The following SITransfer() can fail due to another on-going SI transfer,
    // but SIInterruptHandler() will recover this if it happens.
    SITransfer(chan, &cmdTypeAndStatus, 1, &Type[chan], 3, GetTypeCallback,
               OSMicrosecondsToTicks(65));

    OSRestoreInterrupts(enabled);
    return type;
}

/*---------------------------------------------------------------------------*
  Name:         SIGetTypeAsync

  Description:  Queries controller type and status

  Arguments:    chan        Channel to query type and status
                callback    Callback to be called. Only a single instance of
                            the same callback can be registered per channel

  Returns:      type & status : error
 *---------------------------------------------------------------------------*/
u32 SIGetTypeAsync(s32 chan, SITypeAndStatusCallback callback)
{
    BOOL enabled;
    u32  type;

    enabled = OSDisableInterrupts();
    type = SIGetType(chan);
    if (Type[chan] & SI_ERROR_BUSY)
    {
        // register callback
        int i;

        for (i = 0; i < SI_MAX_TYPE; ++i)
        {
            if (TypeCallback[chan][i] == callback)
            {
                break;
            }
            if (TypeCallback[chan][i] == 0)
            {
                TypeCallback[chan][i] = callback;
                break;
            }
        }
        ASSERT(i < SI_MAX_TYPE);
    }
    else
    {
        callback(chan, type);
    }
    OSRestoreInterrupts(enabled);
    return type;
}

/*---------------------------------------------------------------------------*
  Name:         SIDecodeType

  Description:  Decodes controller type and status to generic controller type

  Arguments:    type        type and status returned by SIGetTypeAsync()

  Returns:      SI_ERROR_NO_RESPONSE    no controller is attached
                SI_ERROR_UNKNOWN        unknown type of controller is attached
                SI_ERROR_BUSY           detecting controller

                SI_N64_CONTROLLER       N64 standard controller
                SI_N64_KEYBOARD         N64 keyboard
                SI_N64_MOUSE            N64 mouse
                SI_GBA                  GameBoy Advance
                SI_GC_CONTROLLER        GC standard controller
                SI_GC_RECEIVER          GC wireless receiver
                SI_GC_WAVEBIRD          GC wavebird controller

 *---------------------------------------------------------------------------*/
u32 SIDecodeType(u32 type)
{
    u32 error;

    error = type & 0xff;
    type &= ~0xff;

    if (error & SI_ERROR_NO_RESPONSE)
    {
        return SI_ERROR_NO_RESPONSE;
    }
    if (error & (SI_ERROR_UNDER_RUN | SI_ERROR_OVER_RUN | SI_ERROR_COLLISION | SI_ERROR_UNKNOWN))
    {
        return SI_ERROR_UNKNOWN;
    }
    if (error)
    {
        ASSERT(error == SI_ERROR_BUSY);
        return SI_ERROR_BUSY;
    }

    if ((type & SI_TYPE_MASK) == SI_TYPE_N64)
    {
        //
        // N64 controllers
        //
        switch (type & 0xffff0000)
        {
          case SI_N64_CONTROLLER:
          case SI_N64_MIC:
          case SI_N64_KEYBOARD:
          case SI_N64_MOUSE:
          case SI_GBA:
            return type & 0xffff0000;
            break;
        }
        return SI_ERROR_UNKNOWN;
    }

    if ((type & SI_TYPE_MASK) != SI_TYPE_GC)
    {
        //
        // non-N64/non-GC controllers
        //
        return SI_ERROR_UNKNOWN;
    }


    //
    // GameCube controllers
    //
    switch (type & 0xffff0000)
    {
      case SI_GC_CONTROLLER:
      case SI_GC_STEERING:
        return type & 0xffff0000;
        break;
    }

    //
    // GameCube keyboards
    //
    if ((type & 0xffe00000) == SI_GC_KEYBOARD)
    {
        return SI_GC_KEYBOARD;
    }

    if ((type & SI_GC_WIRELESS) && !(type & SI_WIRELESS_IR))
    {
        //
        // Wireless controller
        //
        if ((type & SI_GC_WAVEBIRD) == SI_GC_WAVEBIRD)
        {
            return SI_GC_WAVEBIRD;
        }
        else if (!(type & SI_WIRELESS_STATE))
        {
            return SI_GC_RECEIVER;
        }
    }

    //
    // Unknown GC controllers
    //
    if ((type & SI_GC_CONTROLLER) == SI_GC_CONTROLLER)
    {
        //
        // Controller that has the standard feature set
        //
        return SI_GC_CONTROLLER;
    }
    return SI_ERROR_UNKNOWN;
}

/*---------------------------------------------------------------------------*
  Name:         SIProbe

  Description:  Queries the type of controller attached to the specified
                channel

  Arguments:    chan        Channel to query type and status

  Returns:      SI_ERROR_NO_RESPONSE    no controller is attached
                SI_ERROR_UNKNOWN        unknown type of controller is attached
                SI_ERROR_BUSY           detecting controller

                SI_GC_CONTROLLER        GC standard controller
                SI_GC_RECEIVER          GC wireless receiver
                SI_GC_WAVEBIRD          GC WaveBird controller
                SI_GC_KEYBOARD          GC keyboard
                SI_GC_STEERING          GC steering
                SI_GBA                  GameBoy Advance
                SI_N64_CONTROLLER       N64 standard controller
                SI_N64_KEYBOARD         N64 keyboard
                SI_N64_MOUSE            N64 mouse
 *---------------------------------------------------------------------------*/
u32 SIProbe(s32 chan)
{
    return SIDecodeType(SIGetType(chan));
}

/*---------------------------------------------------------------------------*
  Name:         SIGetTypeString

  Description:  Gets description of the specified controller type

  Arguments:    type        type and status returned by SIProbe()

  Returns:      character string that describes the specified controller
 *---------------------------------------------------------------------------*/
char* SIGetTypeString(u32 type)
{
    switch (SIDecodeType(type))
    {
      case SI_ERROR_NO_RESPONSE:
        return "No response";
      case SI_ERROR_BUSY:
        return "Busy";
      case SI_N64_CONTROLLER:
        return "N64 controller";
      case SI_N64_MIC:
        return "N64 microphone";
      case SI_N64_KEYBOARD:
        return "N64 keyboard";
      case SI_N64_MOUSE:
        return "N64 mouse";
      case SI_GBA:
        return "GameBoy Advance";
      case SI_GC_CONTROLLER:
        return "Standard controller";
      case SI_GC_RECEIVER:
        return "Wireless receiver";
      case SI_GC_WAVEBIRD:
        return "WaveBird controller";
      case SI_GC_KEYBOARD:
        return "Keyboard";
      case SI_GC_STEERING:
        return "Steering";
      case SI_ERROR_UNKNOWN:
      default:
        return "Unknown";
    }
}
