/*---------------------------------------------------------------------------*
  Project:  base 64 encode/decode functions
  File:     base64.c

  Copyright 2002 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: base64.c,v $
  Revision 1.1.1.1  2004/06/08 23:11:08  paulm
  Source for Network Base Package 05-Aug-2003

    
    2     10/17/02 14:56 Shiki
    Renamed base64 function names.

    1     9/19/02 10:55 Shiki
    Initial check-in.
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#include <string.h>
#include <dolphin/types.h>

char* __IPEncodeToBase64  ( const void* src, s32 len, char* dst );
void* __IPDecodeFromBase64( const char* src, s32 len, void* dst );

// 0xfc = 11111100
// 0x03 = 00000011
// 0xf0 = 11110000
// 0x0f = 00001111
// 0xc0 = 11000000
// 0x3f = 00111111
// 0x30 = 00110000
// 0x3c = 00111100

// The Base64 alphabet
static const char Base64[] =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";

__declspec(weak) char* __IPEncodeToBase64(const void* _src, s32 len, char* dst)
{
    const u8* src = _src;
    int   i;
    char* p;

    p = dst;
    for (i = 0; i < (len - (len % 3)); i += 3)  // Encode 3 bytes at a time
    {
        *p++ = Base64[(src[i] & 0xfc) >> 2];
        *p++ = Base64[((src[i] & 0x03) << 4)   | ((src[i+1] & 0xf0) >> 4)];
        *p++ = Base64[((src[i+1] & 0x0f) << 2) | ((src[i+2] & 0xc0) >> 6)];
        *p++ = Base64[(src[i+2] & 0x3f)];
    }

    i = len - (len % 3);
    switch (len % 3)
    {
      case 2:  // One character padding needed
        *p++ = Base64[(src[i] & 0xfc) >> 2];
        *p++ = Base64[((src[i] & 0x03) << 4) | ((src[i+1] & 0xf0) >> 4)];
        *p++ = Base64[(src[i+1] & 0x0f) << 2];
        *p++ = '=';
        break;
      case 1:  // Two character padding needed
        *p++ = Base64[(src[i] & 0xfc) >> 2];
        *p++ = Base64[(src[i] & 0x03) << 4];
        *p++ = '=';
        *p++ = '=';
        break;
    }
    return (char*) p;
}

__declspec(weak) void* __IPDecodeFromBase64(const char* src, s32 len, void* _dst)
{
    u8* dst = _dst;
    int i;
    u8* p;
    int n0, n1, n2, n3;

    p = dst;
    for (i = 0; i < len; i += 4) // Work on 4 bytes at a time
    {
        n0 = strchr(Base64, src[i]) - Base64;
        n1 = strchr(Base64, src[i + 1]) - Base64;
        *p++ = (u8) ((n0 << 2) | ((n1 & 0x30) >> 4));
        if (src[i + 2] == '=')  // Two character padded
        {
            break;
        }
        n2 = strchr(Base64, src[i + 2]) - Base64;
        *p++ = (u8) (((n1 & 0x0f) << 4) | ((n2 & 0x3c) >> 2));
        if (src[i + 3] == '=')  // One character padded
        {
            break;
        }
        n3 = strchr(Base64, src[i + 3]) - Base64;
        *p++ = (u8) (((n2 & 0x03) << 6) | (n3 & 0x3f));
    }
    return p;
}
