/*---------------------------------------------------------------------------*
  Project:  ETH API
  File:     ethsec.c

  Copyright 2002, 2003 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: ethsec.c,v $
  Revision 1.1.1.1  2004/06/08 23:11:08  paulm
  Source for Network Base Package 05-Aug-2003

    
    11    5/14/03 9:34 Shiki
    Modified to allow dst port 1900 for UPnP.

    10    5/08/03 18:44 Shiki
    Modified to allow port 1900 for UPnP.

    9     3/07/03 13:29 Shiki
    Revised the TDEV detect method in CheckConsoleType() to support
    production mode.

    8     03/02/10 14:32 Ikedae
    Fixed security check miss behavior (which blocks all TCP packets)

    7     10/17/02 14:56 Shiki
    Renamed base64 function names.

    6     10/01/02 9:42 Shiki
    Defined AT_IP4_IsMyAddrMas() weakly.

    5     9/24/02 9:34 Shiki
    Modified CheckConsoleType() on ORCA board to enable TCP connection
    restriction in production mode.

    4     9/20/02 10:05 Shiki
    Added support for TDEV.

    3     9/20/02 9:35 Shiki
    Modified __ETHFilter() not to reject DNS connections.

    2     9/19/02 15:43 Shiki
    Fixed __ETHPostSend() to generate incremental IP id as intended.

    1     9/19/02 10:55 Shiki
    Initial check-in.
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#include <dolphin.h>
#include <dolphin/ip.h>
#include <dolphin/md5.h>
#include <private/OSTdev.h>
#include "ethsec.h"

#include <private/OSLoMem.h>
#include <secure/dvdcb.h>

char* __IPEncodeToBase64  ( const void* src, s32 len, char* dst );
void* __IPDecodeFromBase64( const char* src, s32 len, void* dst );

//
// OS API section
//
u32 __OSGetDIConfig(void);

vu16 __OSDeviceCode : (OS_BASE_CACHED | OS_DVD_DEVICECODE);

//
// AVE-TCP section
//

#pragma dont_inline on

extern s16 AT_IP4_IsMyAddrMask(u32 addr);  // Check if the network is my own.

__declspec(weak) s16 AT_IP4_IsMyAddrMask(u32 addr)
{
    #pragma unused(addr)
    return 0;
}

//
// Socket API section
//
extern BOOL IPIsLocalAddr(IPInterface* interface, const u8* addr);

__declspec(weak) BOOL IPIsLocalAddr(IPInterface* interface, const u8* addr)
{
    #pragma unused(interface)
    return (AT_IP4_IsMyAddrMask(*(u32*) addr) != -1) ? TRUE : FALSE;
}

#pragma dont_inline reset

//
// ethsec Implementation
//

#define RST_TEXT_LEN        24

static const u8 NintendoAddr[ETH_ALEN] = {0x00, 0x09, 0xbf, 0x00, 0x00, 0x00 };

typedef struct TCPResetInfo
{
    u8  addr[ETH_ALEN];
    u8  dstAddr[IP_ALEN];
    u8  srcAddr[IP_ALEN];
    u16 dstPort;
    u16 srcPort;
    s32 seq;
    s32 ack;
    u16 flag;
    u16 id;
} TCPResetInfo;

static TCPResetInfo Ri;
static u8           SendBuf[ETH_HLEN + IP_MIN_HLEN + TCP_MIN_HLEN + RST_TEXT_LEN + 4 /* crc */] ATTRIBUTE_ALIGN(32);

/*---------------------------------------------------------------------------*
  Name:         CheckConsoleType

  Description:  Checks if the current console should restrict the TCP
                connections as below:

                Console                 Restriction
                -----------------------------------
                Production Board
                    * Retail            Yes
                    * NR-Reader         Yes
                    * NPDP-Reader       No
                    * TDEV              No
                Development Board
                    * Production mode   Yes
                    * Development mode  No

  Arguments:

  Returns:      TRUE if no ETH.a restriction is required.
                FALSE if ETH.a must restrict TCP connection from non-GC
                in the same network.
 *---------------------------------------------------------------------------*/
static inline BOOL CheckConsoleType(void)
{
    if (__OSGetDIConfig() == 0xff)
    {
        //
        // Production board
        //

        // Check drive type
        switch (__OSDeviceCode)
        {
          case DVD_DEVICECODE_PRESENT | DVD_DEVICECODE_NPDP:
            return TRUE;
          case DVD_DEVICECODE_PRESENT | DVD_DEVICECODE_NRDRIVE:
            if (OSGetPhysicalMemSize() == 48 * 1024 * 1024)
            {
                // TDEV production mode
                return TRUE;
            }
            return FALSE;
            break;
          default:
            return FALSE;
            break;
        }
    }
    else
    {
        //
        // ORCA board
        //

        if ((OSGetConsoleType() & 0xf0000000) == 0) // OS_CONSOLE_RETAIL*
        {
            return FALSE;
        }

        return TRUE;
    }
}

// Compute IP checksum
static u16 CalcIpCheckSum(IPHeader* ip)
{
    int  len;
    u32  sum = 0;
    u16* p;

    len = IP_HLEN(ip);
    p = (u16*) ip;
    while (0 < len)
    {
        sum += *p++;
        len -= 2;
    }
    sum = (sum & 0xffff) + (sum >> 16);
    sum = (sum & 0xffff) + (sum >> 16);
    return (u16) (sum ^ 0xffff);
}

// Compute TCP checksum
static u16 CalcTcpCheckSum(IPHeader* ip, s32 len)
{
    s32        hlen;
    u16*       p;
    u32        sum = 0;

    ASSERT(IP_MIN_HLEN + TCP_MIN_HLEN <= len);

    // Add pseudo header
    ASSERT(ip->proto == IP_PROTO_TCP);
    hlen = IP_HLEN(ip);
    sum += *(u16*) &ip->src[0];
    sum += *(u16*) &ip->src[2];
    sum += *(u16*) &ip->dst[0];
    sum += *(u16*) &ip->dst[2];
    sum += IP_PROTO_TCP;
    sum += len - hlen;

    // Add TCP datagram
    p = (u16*) ((u8*) ip + hlen);
    len = len - hlen;
    while (1 < len)
    {
        sum += *p++;
        len -= 2;
    }
    if (len == 1)
    {
        sum += ((u32) (*(u8*) p)) << 8;
    }
    sum = (sum & 0xffff) + (sum >> 16);
    sum = (sum & 0xffff) + (sum >> 16);
    return (u16) (sum ^ 0xffff);
}

// Compute TCP segment length in sequence count
static s32 GetTcpSegLen(IPHeader* ip, TCPHeader* tcp)
{
    s32 len;

    len = ip->len - IP_HLEN(ip) - TCP_HLEN(tcp);

    // SYN is considered to occur before the first actual data
    if (tcp->flag & TCP_FLAG_SYN)
    {
        ++len;
    }
    // FIN is considered to occur after the last actual data
    if (tcp->flag & TCP_FLAG_FIN)
    {
        ++len;
    }

    return len;
}

static BOOL VerifyPacket(IPHeader* ip)
{
    // Check IP version, etc.
    if ((ip->verlen >> 4) != IP_VER || ip->len < IP_HLEN(ip))
    {
        return TRUE;
    }

    // Verify checksum
    if (CalcIpCheckSum(ip) != 0)
    {
        return TRUE;
    }

    // Class E/D
    if (IP_CLASSD(ip->dst) || IP_CLASSD(ip->src) ||
        IP_CLASSE(ip->dst) || IP_CLASSE(ip->src))
    {
        return TRUE;
    }

    // Verify checksum
    ASSERT(ip->proto == IP_PROTO_TCP);
    if (CalcTcpCheckSum(ip, ip->len) != 0)
    {
        return TRUE;
    }

    return FALSE;
}

#define A 0x0005deece66dLL
#define C 0xb
#define M (1LL << 48)

static void CalcDigest(u8* digest, TCPHeader* tcp)
{
    MD5Context context;
    u64        secret;

    MD5Init(&context);
    MD5Update(&context, (u8*) &tcp->seq, 4);
    MD5Update(&context, (u8*) &tcp->ack, 4);
    secret = (u64) ((A * (tcp->seq ^ tcp->ack) + C) % M);   // rand 48
    MD5Update(&context, (u8*) &secret + 2, 6);
    MD5Final(digest, &context);
}

/*---------------------------------------------------------------------------*
  Name:         __ETHFilter

  Description:  Intercepts TCP packet sent from NICs other than Nintendo.

                Firstly, __ETHFilter() tries to send a SYN|FIN packet to
                the receiver window of the remote peer after the TCP
                connection is established, which cause the remote peer to
                send a RST packet back to the local peer by illegal SYN.
                Note SYN|FIN segment transmission is differed until the node
                sent the next TCP packet to the remote peer. This is a
                compromise without any serious lose of security property
                (GC can send at most one ACK or RST for syn and then reset
                 the connection).

                Secondly, __ETHFilter() filters out SYN|FIN packet sent
                from remote GameCube nodes by checking digest values contained
                in the SYN|FIN packet data portion so as not to disconnect
                the TCP connection between two GameCube nodes.

  Arguments:

  Returns:
 *---------------------------------------------------------------------------*/
BOOL __ETHFilter(u8* buf, s32 len)
{
    ETHHeader*    eh;
    IPHeader*     ip;
    TCPHeader*    tcp;
    TCPResetInfo* ri = &Ri;
    u8            digest1[16];
    u8            digest2[16];

    // Check console type
    if (CheckConsoleType())
    {
        // Devkit or NPDP-Reader
        return TRUE;
    }

    // Ethernet filter
    eh = (ETHHeader*) buf;
    if (eh->type != ETH_IP)
    {
        return TRUE;
    }
    if (memcmp(eh->src, NintendoAddr, 3) == 0)
    {
        return TRUE;
    }

    // IP packet filter
    len -= ETH_HLEN;
    ip = (IPHeader*) ((u8*) eh + ETH_HLEN);
    if (len < IP_MIN_HLEN || len < ip->len)
    {
        return TRUE;
    }
    if (ip->proto != IP_PROTO_TCP)
    {
        return TRUE;
    }

    // TCP segment filter
    tcp = (TCPHeader*) ((u8*) ip + IP_HLEN(ip));
    if (TCP_HLEN(tcp) < TCP_MIN_HLEN)
    {
        return TRUE;
    }
    if (ip->len < IP_HLEN(ip) + TCP_HLEN(tcp))
    {
        return TRUE;
    }

    // TCP connection reject test suites
    if ((tcp->flag & (TCP_FLAG_SYN | TCP_FLAG_FIN)) == TCP_FLAG_SYN)
    {
        // Test granted ports
        switch (tcp->src)
        {
          case DNS_PORT:    // DNS(53)
          case 1900:        // UPnP
            return TRUE;
            break;
        }

        switch (tcp->dst)
        {
          case 1900:        // UPnP
            return TRUE;
            break;
        }

        if (!IPIsLocalAddr(NULL, ip->src))    // Not from the same segment?
        {
            return TRUE;
        }

        if (tcp->flag & TCP_FLAG_RST)
        {
            return TRUE;
        }

        if (VerifyPacket(ip))
        {
            return TRUE;
        }

        // Request connection reset
        if (ri->flag == 0)
        {
            memmove(ri->addr,    eh->src, ETH_ALEN);
            memmove(ri->dstAddr, ip->dst, IP_ALEN);
            memmove(ri->srcAddr, ip->src, IP_ALEN);
            ri->dstPort = tcp->dst;
            ri->srcPort = tcp->src;
            if ((tcp->flag & TCP_FLAG_ACK) == 0)
            {
                ri->seq = 0;
                ri->ack = tcp->seq + GetTcpSegLen(ip, tcp);
                ri->flag = TCP_FLAG_SYN | TCP_FLAG_FIN | TCP_FLAG_ACK;
            }
            else
            {
                ri->seq = tcp->ack;
                ri->ack = 0;
                ri->flag = TCP_FLAG_SYN | TCP_FLAG_FIN;
            }
            ri->id = 0;
            return TRUE;
        }
        else
        {
            // Discard SYN
            ip->len = 0;
            return FALSE;
        }
        // NOT REACHED HERE
    }

    if ((tcp->flag & (TCP_FLAG_SYN | TCP_FLAG_FIN)) == (TCP_FLAG_SYN | TCP_FLAG_FIN))
    {
        if (TCP_HLEN(tcp) != TCP_MIN_HLEN)
        {
            return TRUE;
        }

        if (ip->len != IP_MIN_HLEN + TCP_MIN_HLEN + RST_TEXT_LEN)
        {
            return TRUE;
        }

        if (!IPIsLocalAddr(NULL, ip->src))    // Not from the same segment?
        {
            return TRUE;
        }

        if (VerifyPacket(ip))
        {
            return TRUE;
        }

        // Check digest text to prevent connection reset
        CalcDigest(digest1, tcp);
        __IPDecodeFromBase64((char*) tcp + TCP_MIN_HLEN, RST_TEXT_LEN, digest2);
        if (memcmp(digest1, digest2, 16) != 0)
        {
            return TRUE;
        }

        // Discard SYN | FIN
        ip->len = 0;
        return FALSE;
        // NOT REACHED HERE
    }

    return TRUE;
}

/*---------------------------------------------------------------------------*
  Name:         __ETHPostSend

  Description:  Sends a SYN|FIN segment requested by __ETHFilter to let the
                remote peer abort the TCP connection.

  Arguments:

  Returns:
 *---------------------------------------------------------------------------*/
BOOL __ETHPostSend(u8 ltps, ETHCallback2 callback, void* prev)
{
    ETHHeader*    eh;
    IPHeader*     ip;
    TCPHeader*    tcp;
    TCPResetInfo* ri;
    u8            digest[16];

    ri = &Ri;
    if (ri->flag == 0)
    {
        ri = NULL;
    }
    else if (prev != SendBuf)
    {
        eh = (ETHHeader*) prev;
        ip = (IPHeader*) ((u8*) eh + ETH_HLEN);
        tcp = (TCPHeader*) ((u8*) ip + IP_MIN_HLEN);

        if (IP_ADDR_EQ(ip->dst, ri->srcAddr) && IP_ADDR_EQ(ip->src, ri->dstAddr) &&
            tcp->src == ri->dstPort && tcp->dst == ri->srcPort)
        {
            if (tcp->flag & TCP_FLAG_RST)
            {
                ri->flag = 0;
                ri = NULL;
            }
            else
            {
                ri->seq = tcp->seq + GetTcpSegLen(ip, tcp);
                ri->ack = tcp->ack;
                ri->flag |= (tcp->flag & TCP_FLAG_ACK);
                // Fake none zero IP ID based on the last ID the TCP/IP stack
                // is used minus (2^16)/4 or something like that.
                ri->id = (u16) (ip->id - (1u << 14));
                if (ri->id == 0)
                {
                    ri->id = 1;
                }
            }
        }
    }

    if (ri && ri->id)
    {
        eh = (ETHHeader*) SendBuf;
        ip = (IPHeader*) ((u8*) eh + ETH_HLEN);
        tcp = (TCPHeader*) ((u8*) ip + IP_MIN_HLEN);

        eh->type = ETH_IP;
        memmove(eh->dst, ri->addr, ETH_ALEN);
        ETHGetMACAddr(eh->src);

        ip->verlen = IP_VERHLEN;
        ip->tos    = 0;     // normal
        ip->len    = (u16) (IP_MIN_HLEN + TCP_MIN_HLEN + RST_TEXT_LEN);
        ip->id     = ri->id;
        ip->frag   = IP_DF; // Every internet module must be able to forward a datagram of 68 octets
        ip->ttl    = 255;   // normal
        ip->proto  = IP_PROTO_TCP;
        ip->sum    = 0;
        memmove(ip->dst, ri->srcAddr, IP_ALEN);
        memmove(ip->src, ri->dstAddr, IP_ALEN);

        tcp = (TCPHeader*) ((u8*) ip + IP_HLEN(ip));
        tcp->src = ri->dstPort;
        tcp->dst = ri->srcPort;
        tcp->seq = ri->seq;
        tcp->ack = ri->ack;
        tcp->flag = (u16) ((TCP_MIN_HLEN << 10) | (ri->flag & TCP_FLAG_793));
        tcp->win = 0;
        tcp->sum = 0;
        tcp->urg = 0;

        // Make digest text
        CalcDigest(digest, tcp);
        __IPEncodeToBase64(digest, 16, (char*) tcp + TCP_MIN_HLEN);

        ip->sum  = CalcIpCheckSum(ip);
        tcp->sum = CalcTcpCheckSum(ip, ip->len);

        ri->flag = 0;

        ETHSendAsync(SendBuf, ETH_HLEN + IP_MIN_HLEN + TCP_MIN_HLEN + RST_TEXT_LEN, callback);
    }
    else if (callback)
    {
        callback(ltps);
    }
    return TRUE;
}
