/*---------------------------------------------------------------------------*
  Project:  ETH API
  File:     eth.c

  Copyright 2002, 2003 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: eth.c,v $
  Revision 1.1.1.1  2004/06/08 23:11:08  paulm
  Source for Network Base Package 05-Aug-2003

    
    66    1/28/04 9:01 Shiki
    Fixed the wrong definition of __ETHInterruptTime to a declaration in
    <private/eth.h>..

    65    12/17/03 9:39 Shiki
    Lowered the automatic gain control from the default 0x5f to 0x30
    according to mega-chips.

    64    11/28/03 19:16 Shiki
    Removed support for ethsec.c

    63    8/05/03 16:02 Shiki
    Fixed ETHSetRecvCallback() not to disable TX interrupts.

    62    7/11/03 9:49 Shiki
    Modified *Update variables to volatile.

    61    7/09/03 13:55 Shiki
    Refined the ETHAddMulticastAddress() declaration using const.

    60    7/01/03 20:39 Shiki
    Fixed the lockup bug due to multiple EXILock().

    59    7/01/03 16:55 Shiki
    Clean up "etherdef.h".

    58    7/01/03 16:07 Shiki
    Clean up.

    57    6/04/03 10:59 Shiki
    Cleanup. Declared __ETHInterruptTime to measure interrupt process
    overhead.

    56    03/06/03 16:15 Ikedae
    Added ETHClearMulticastAddresses()

    55    03/06/03 13:36 Ikedae
    Disabled MAC receiving when ETHSetRecvCallback(NULL NULL) is set

    54    5/30/03 13:24 Shiki
    Modified ME_TRB to 0x2e according to the response from MXIC.

    53    03/05/29 8:57 Ikedae
    set TRB=0x0e to reduce collision. removed unused #if blocks.

    51    03/05/02 19:10 Ikedae
    turned off NCRB_CA bit

    50    03/04/30 14:19 Ikedae
    modified receive int handling,removed receive polling

    49    02/12/05 18:48 Ikedae
    added alarm check to fix lost packet issue

    48    02/11/18 18:05 Ikedae
    Fixed receive delay problem

    46    10/17/02 14:08 Shiki
    Modified not to display ETHGetLibraryVersion() number.

    45    10/03/02 10:43 Shiki
    Fixed readCID() to use EXI_FREQ_1M.

    44    9/19/02 10:56 Shiki
    Merged support for ethsec.c

    41    02/09/09 9:53 Ikedae
    Added OSRegisterVersion support

    37    02/07/11 9:01 Ikedae
    modified MPBBA PHY init code

    16    02/04/22 17:24 Ikedae
    Added ETHGetLinkStateAsync

    10    02/04/03 18:07 Ikedae
    Addes ETHAddMulticastAddress

    8     02/04/02 10:01 Ikedae
    restore priority TI > RI

    5     02/03/28 13:33 Ikedae
    modified ETHSEC

    4     02/03/28 13:30 Ikedae
    modified ETHSend

    3     02/03/27 11:26 Ikedae
    Initial check-in.

  $NoKeywords: $

  Implementation Note:

    Before the version 60, EXILock() for sending a packet can fail
    if DMA is running to receive packet from the ring buffer. In
    the mean time, if further EXILock is called, it completely
    fails and locks up.

    1. List of functions that call EXILock()

        waitexilock() -- can lock up
        sendsub0() from ETHSendAsync() -- can lock up
        exiinthandler() -- shouldn't fail
        linkStateAsyncMain()    -- can fail

    2. List of functions that call waitexilock()

        ETHInit
        ETHSetRecvCallback
        ETHAddMulticastAddress
        ETHClearMulticastAddresses

        ETHGetLinkState -- obsolete

        ETHGetNWAYMode -- not used
        ETHSetMACAddr -- not used
        ETHGetREVID -- not used

 *---------------------------------------------------------------------------*/

#include <dolphin/doldefs.h>
DOLPHIN_LIB_VERSION(ETH);

#define ETHVERSION  0x00000506

#include <dolphin.h>
#include <private/exi.h>
#include <private/eth.h>
#include <private/io_reg.h>
#include <private/flipper.h>
#include <string.h>
#include "etherdef.h"
#include "ethsec.h"

volatile OSTime __ETHInterruptTime;

static u8 broadcastheader[6] = { 0xff, 0xff, 0xff, 0xff, 0xff, 0xff };

#define ET4BYTEBIT      0x80000000

#define EXIMASK         (ME_EXI_IMR_MACE | ME_EXI_IMR_TIMEOUTE | ME_EXI_IMR_CMDERRE | \
                         ME_EXI_IMR_CERTIFYE | ME_EXI_IMR_HASHCOMPLETEE)

#define TYPEOFFSET      12      // packet type offset

#define ET_CHAN         0
#define ET_DEV          2
#define ET_INT_CHAN     2

#define ET_FREQ         EXI_FREQ_32M

static BOOL lock(void);
static void unlock(void);

static u8 private_macaddr[6];

    static u8   __revid;
    static u16  __devid   = 0xd107;    // nintendo
    static u8   __acstart = 0x4e;

static OSThreadQueue    threadQ;
static volatile BOOL    lockUpdate;

static volatile BOOL    __sendbusy = FALSE;
static volatile BOOL    sendUpdate;
static ETHStat          ethstat;
static u16*             protoarray = NULL;
static s32              protonum = 0;
static ETHCallback0     recvcallback0 = NULL;
static ETHCallback1     recvcallback1 = NULL;
static volatile BOOL    recvcallbackUpdate;
static ETHCallback2     __sendcallback = NULL;
static BOOL             __recvpriorityflag = FALSE;

static void sendsub0(void);
static void sendsub1(s32 chan, OSContext* context);

static u8*  __sendadr;
static s32  __sendlen;
static s32  __senddmalen;
static s32  __sendimmlen;

// 10/23/2001
#define RXSTART 0x01
#define RXEND   0x0f            // 4KB

#define INT_RBFI                24
#define INT_BUSEI               25
#define INT_FIFOEI              26
#define INT_TEI                 27
#define INT_REI                 28
#define INT_TI                  29
#define INT_RI                  30
#define INT_CNTOFI              31

#define INT_EXI_MAC             24
#define INT_EXI_TIMEOUT         25
#define INT_EXI_CMDERR          26
#define INT_EXI_CERTIFY         27
#define INT_EXI_HASHCOMPLETE    28
#define INT_EXI_RESERVE1        29
#define INT_EXI_RESERVE2        30
#define INT_EXI_RESERVE3        31

// recv procedures
static void recvsub0(void);
static void recvsub1(void);
static void readbuffer0(u16 cmd);
static void readbuffer1(s32 chan, OSContext* context);
static void readbuffersub(s32 chan, OSContext* context);

static u8 tmppool0[1536] ATTRIBUTE_ALIGN(32);
static u8 tmppool1[1536] ATTRIBUTE_ALIGN(32);
static u32 recvlen0 = 0;
static u32 recvlen1 = 0;

static Descriptor2 d2;
static u8*         recvaddr;
static u16         rwp;
static u16         rrp;

// mcast map
static u8               mcastmap[8];
static volatile BOOL    mcastUpdate;

static void waitexilock(void)
{
    BOOL enabled;

    enabled = OSDisableInterrupts();
    for (;;)
    {
        if (lock())
        {
            OSRestoreInterrupts(enabled);
            return;
        }
        lockUpdate = TRUE;
        OSSleepThread(&threadQ);
        OSRestoreInterrupts(enabled);
    }
}

static u16 swap16(u16 val)
{
    return (u16) ((val << 8) | ((val >> 8) & 0xff));
}

static void waitmicro(s32 usec)
{
    OSTick start;
    OSTick interval;

    start = OSGetTick();
    interval = OSMicrosecondsToTicks(usec);
    while (((s32) OSGetTick() - (s32) start) < interval)
        ;
}

static u8 readEXIcmd(u8 cmd)
{
    u16 ethcmd;
    u8  rdata;

    ethcmd = (u16) (cmd << 8);
    EXISelect(ET_CHAN, ET_DEV, ET_FREQ);
    EXIImm(ET_CHAN, (u8*) &ethcmd, sizeof(ethcmd), EXI_WRITE, NULL);
    EXISync(ET_CHAN);
    EXIImm(ET_CHAN, (u8*) &rdata, sizeof(rdata), EXI_READ, NULL);
    EXISync(ET_CHAN);
    EXIDeselect(ET_CHAN);

    return rdata;
}

static u8 readEXIcmdWait200Micro(u8 cmd)
{
    u16 ethcmd;
    u8  rdata;

    ethcmd = (u16) (cmd << 8);
    EXISelect(ET_CHAN, ET_DEV, ET_FREQ);
    EXIImm(ET_CHAN, (u8*) &ethcmd, sizeof(ethcmd), EXI_WRITE, NULL);
    EXISync(ET_CHAN);
    EXIImm(ET_CHAN, &rdata, sizeof(rdata), EXI_READ, NULL);
    EXISync(ET_CHAN);
    waitmicro(200);
    EXIDeselect(ET_CHAN);

    return rdata;
}

static void writeEXIcmd(u8 cmd, u8 imr)
{
    BOOL ret;
    u16  ethcmd;

    ethcmd = (u16) (cmd << 8 | 0x4000);

    ret = EXISelect(ET_CHAN, ET_DEV, ET_FREQ);

    EXIImm(ET_CHAN, (u8*) &ethcmd, sizeof(ethcmd), EXI_WRITE, NULL);
    EXISync(ET_CHAN);
    EXIImm(ET_CHAN, (u8*) &imr, sizeof(imr), EXI_WRITE, NULL);
    EXISync(ET_CHAN);
    EXIDeselect(ET_CHAN);
}

static void readEXIcmdLong(u8 cmd, void* address, s32 length)
{
    u16 ethcmd;

    ethcmd = (u16) (cmd << 8);
    EXISelect(ET_CHAN, ET_DEV, ET_FREQ);
    EXIImm(ET_CHAN, (u8*) &ethcmd, sizeof(ethcmd), EXI_WRITE, NULL);
    EXISync(ET_CHAN);
    EXIImmEx(ET_CHAN, address, length, EXI_READ);
    EXIDeselect(ET_CHAN);
}

static void writeEXIcmdLong(u8 cmd, void* address, s32 length)
{
    BOOL ret;
    u16  ethcmd;

    ethcmd = (u16) (cmd << 8 | 0x4000);
    ret = EXISelect(ET_CHAN, ET_DEV, ET_FREQ);
    EXIImm(ET_CHAN, (u8*) &ethcmd, sizeof(ethcmd), EXI_WRITE, NULL);
    EXISync(ET_CHAN);
    EXIImmEx(ET_CHAN, address, length, EXI_WRITE);
    EXIDeselect(ET_CHAN);
}

static void readcmdLong(u16 cmd, void* addr, s32 length)
{
    u32 etcmd;

    etcmd = ET4BYTEBIT | cmd << 8;
    EXISelect(ET_CHAN, ET_DEV, ET_FREQ);
    EXIImm(ET_CHAN, (u8*) &etcmd, sizeof(etcmd), EXI_WRITE, NULL);
    EXISync(ET_CHAN);
    EXIImmEx(ET_CHAN, addr, length, EXI_READ);
    EXIDeselect(ET_CHAN);
    return;
}

static void writecmdLong(u16 cmd, void* addr, s32 length)
{
    u32 etcmd;

    etcmd = ET4BYTEBIT | 0x40000000 | cmd << 8;
    EXISelect(ET_CHAN, ET_DEV, ET_FREQ);
    EXIImm(ET_CHAN, (u8*) &etcmd, sizeof(etcmd), EXI_WRITE, NULL);
    EXISync(ET_CHAN);
    EXIImmEx(ET_CHAN, addr, length, EXI_WRITE);
    EXIDeselect(ET_CHAN);
    return;
}

static u8 readcmd(u16 cmd)
{
    u8 val;

    readcmdLong(cmd, &val, sizeof(val));
    return val;
}

static void writecmd(u16 cmd, u8 val)
{
    writecmdLong(cmd, &val, sizeof(val));
}

static void StaOutput(unsigned bit)
{
    writecmd(ME_MIIMR, (u8) (0x00 + bit * 2));
    writecmd(ME_MIIMR, (u8) (0x08 + bit * 2));
    writecmd(ME_MIIMR, (u8) (0x08 + bit * 2));
    writecmd(ME_MIIMR, (u8) (0x00 + bit * 2));
}

static u8 StaInput(void)
{
    u8 bit;

    writecmd(ME_MIIMR, 0x0c);
    bit = readcmd(ME_MIIMR);
    bit &= 0x01;
    writecmd(ME_MIIMR, 0x04);
    writecmd(ME_MIIMR, 0x04);
    return bit;
}

static u16 PhyRead(unsigned regNo)
{
    int i;
    u16 value;

    // Idle
    for (i = 0; i < 32; ++i)
    {
        StaOutput(1);
    }

    // Start
    StaOutput(0);
    StaOutput(1);
    // Opcode(Read)
    StaOutput(1);
    StaOutput(0);
    // PHY Address
    StaOutput(0);
    StaOutput(0);
    StaOutput(0);
    StaOutput(0);
    StaOutput(0);
    // Register Address
    StaOutput((regNo >> 4) & 1);
    StaOutput((regNo >> 3) & 1);
    StaOutput((regNo >> 2) & 1);
    StaOutput((regNo >> 1) & 1);
    StaOutput((regNo >> 0) & 1);
    // Turn around
    writecmd(ME_MIIMR, 0x04);   // Z
    value = StaInput();         // 0
    value = StaInput();

    // Register Data
    value = 0x0000;
    for (i = 0; i < 16; ++i)
    {
        value += (StaInput() << (15 - i));
    }

    // Idle
    for (i = 0; i < 32; ++i)
    {
        StaOutput(1);
    }

    return value;
}

static void PhyWrite(unsigned regNo, u16 value)
{
    int i;

    // Idle
    for (i = 0; i < 32; ++i)
    {
        StaOutput(1);
    }

    // Start
    StaOutput(0);
    StaOutput(1);
    // Opcode(Write)
    StaOutput(0);
    StaOutput(1);
    // PHY Address
    StaOutput(0);
    StaOutput(0);
    StaOutput(0);
    StaOutput(0);
    StaOutput(0);
    // Register Address
    StaOutput((regNo >> 4) & 1);
    StaOutput((regNo >> 3) & 1);
    StaOutput((regNo >> 2) & 1);
    StaOutput((regNo >> 1) & 1);
    StaOutput((regNo >> 0) & 1);
    // Turn around
    StaOutput(1);
    StaOutput(0);
    // Register Data
    //
    for (i = 0; i < 16; ++i)
    {
        StaOutput((u8) ((value >> (15 - i)) & 1));
    }

    // Idle
    for (i = 0; i < 32; ++i)
    {
        StaOutput(1);
    }
}

static void reset(void)
{
    writecmd(ME_MIIMR, 0);      // Disable MII Auto-Polling
    waitmicro(10 * 1000);       // wait 10millisec

    readEXIcmdWait200Micro(ME_EXI_SWR);
    waitmicro(10 * 1000);       // wait 10miillisec

    // Reset 98730
    writecmd(ME_NCRA, ME_NCRA_RESET);
    writecmd(ME_NCRA, 0);

    // Set Automatic Gain Control
    //
    // PHY Register Access Flow
    //
    // 1. Write 0x80 to PHY register 31. (Offset 0x1f)
    // 2. Write 0x30 (AGC, Automatic Gain Control) to PHY register 17.
    //    (Offset 0x11)
    // 3. Write 0x00 to PHY Register 31. (Offset 0x1f)
    PhyWrite(0x1f, 0x80);
    PhyWrite(0x11, 0x30);
    PhyWrite(0x1f, 0x00);
    writecmd(ME_MIIMR, 0x10);   // Change to MII auto-polling mode
}

static void recvinit(void)
{
    u8  ncrb;
    u16 swapped = swap16(RXSTART);

    writecmdLong(ME_BPL, &swapped, sizeof(swapped));
    writecmdLong(ME_RWPL, &swapped, sizeof(swapped));
    writecmdLong(ME_RRPL, &swapped, sizeof(swapped));

    swapped = swap16(RXEND);

    writecmdLong(ME_RHBPL, &swapped, sizeof(swapped));
    ncrb = readcmd(ME_NCRB);
    ncrb &= ~(ME_NCRB_PR | ME_NCRB_CA);     // Promiscuous, Capture Effect
    ncrb |= ME_NCRB_AB;                     // accept broadcast
    writecmd(ME_NCRB, ncrb);
    writecmd(ME_NCRA, ME_NCRA_SR);      // start receive
    writecmd(ME_GCA, ME_GCA_ARXERRB);
}

static void getdescriptor(Descriptor2* d2, u16 address)
{
    Descriptor d;

    readcmdLong(address, &d, sizeof(d));
    d2->nextpacket = (u16) (d.nextpacketH << 8 | d.nextpacketL);
    d2->packetlength = (u16) (d.packetlengthH << 4 | d.packetlengthL);
    d2->status = d.status;
}

// recv procedures

 static u16 readcmd16(u8 cmd)
{
    u16 tmp16;

    readcmdLong(cmd, &tmp16, sizeof(tmp16));
    return swap16(tmp16);
}

 static void writecmd16(u8 cmd, u16 val)
{
    u16 tmp16 = swap16(val);

    writecmdLong(cmd, &tmp16, sizeof(tmp16));
}

static void recvsub0(void)
{
    rwp = readcmd16(ME_RWPL);
    rrp = readcmd16(ME_RRPL);
    recvsub1();
}

static void recvsub1(void)
{
    u16     packettype;
    s32     i;
    u8      lrps;

    recvlen0 = recvlen1 = 0;

    do {
        while (rrp != rwp)
        {
            u16 address = (u16) (rrp << 8);

            getdescriptor(&d2, address);

            readcmdLong((u16) (address + sizeof(Descriptor) + TYPEOFFSET),
                        &packettype, sizeof(packettype));
            if (protonum > 0)
            {
                for (i = 0; i < protonum; i++)
                {
                    if (protoarray[i] == packettype)
                    {
                        break;
                    }
                }
                if (i == protonum)      // not found
                {
                    goto NEXTPACKET;
                }
            }

            if (recvcallback0)
            {
                lrps = d2.status;
                recvaddr = recvcallback0(packettype, d2.packetlength, lrps);
                if (recvaddr == NULL)       // reject packet
                {
                    goto NEXTPACKET;
                }
    // packet is divided by ring buffer boundary.
                if ((address + sizeof(Descriptor) + d2.packetlength) > ((RXEND + 1) << 8))
                {
                    recvlen1 = ((address + sizeof(Descriptor) + d2.packetlength) - ((RXEND + 1) << 8));
                    recvlen0 = d2.packetlength - recvlen1;
                }
                else
                {
                    recvlen0 = d2.packetlength;
                    recvlen1 = 0;
                }
                readbuffer0((u16) (address + sizeof(Descriptor)));
                return;     // leave without EXIUnlock
            }
    NEXTPACKET:
            rrp = d2.nextpacket;

            //  02.11.18
            rwp = readcmd16(ME_RWPL);
            //
        }
        writecmd16(ME_RRPL, rwp);   // Update read pointer

    // clear interrupt bit
        writecmd(ME_IR, ME_IR_RI);

// 03.4.30 check again
        rwp = readcmd16(ME_RWPL);
        rrp = readcmd16(ME_RRPL);

    } while (rwp != rrp);

    writeEXIcmd(ME_EXI_IMR, EXIMASK);
    unlock();
}

static void readbuffer0(u16 cmd)
{
    u32 etcmd;
    u32 length = OSRoundUp32B(recvlen0);

    etcmd = ET4BYTEBIT | cmd << 8;
    EXISelect(ET_CHAN, ET_DEV, ET_FREQ);
    EXIImm(ET_CHAN, (u8*) &etcmd, sizeof(etcmd), EXI_WRITE, NULL);
    EXISync(ET_CHAN);
    EXIDma(ET_CHAN, tmppool0, (s32) length, EXI_READ,
           (recvlen1) ? readbuffersub : readbuffer1);
}

// this is called when packet is divided by ring buffer boundary.
static void readbuffersub(s32 chan, OSContext* context)
{
    #pragma unused(chan)
    #pragma unused(context)
    u32 etcmd;
    u32 length = OSRoundUp32B(recvlen1);

    EXIDeselect(ET_CHAN);

    etcmd = ET4BYTEBIT | (RXSTART << 8) << 8;

    EXISelect(ET_CHAN, ET_DEV, ET_FREQ);
    EXIImm(ET_CHAN, (u8*) &etcmd, sizeof(etcmd), EXI_WRITE, NULL);
    EXISync(ET_CHAN);
    EXIDma(ET_CHAN, tmppool1, (s32) length, EXI_READ, readbuffer1);

    __ETHInterruptTime = OSGetTime() - __OSLastInterruptTime;
}

static void readbuffer1(s32 chan, OSContext* context)
{
    #pragma unused(chan, context)

    EXIDeselect(ET_CHAN);
    DCInvalidateRange(tmppool0, OSRoundUp32B(recvlen0));
    if (recvlen1)
    {
        DCInvalidateRange(tmppool1, OSRoundUp32B(recvlen1));
    }

    if (recvcallback1)
    {
        memcpy(recvaddr, tmppool0, recvlen0);
        if (recvlen1)
        {
            memcpy(recvaddr + recvlen0, tmppool1, recvlen1);
        }
        recvcallback1(recvaddr, d2.packetlength);
    }

    rrp = d2.nextpacket;

    // 02.11.18
    rwp = readcmd16(ME_RWPL);
    //
    recvsub1();         // once again.

    __ETHInterruptTime = OSGetTime() - __OSLastInterruptTime;
}

#include "hashfunc.c"

static void patchthru(void)
{
    u32     rnda, fa;

    writeEXIcmd(ME_EXI_AC_START, __acstart);
    readEXIcmdLong(ME_EXI_RNDA, &rnda, sizeof(rnda));
    fa = hashfunc(rnda);
    writeEXIcmdLong(ME_EXI_FA, &fa, sizeof(fa));
}

static void exiinthandler(s32 chan, OSContext* context)
{
    #pragma unused(chan, context)
    u8      ir;
    u8      exiisr;
    BOOL    ret;
    BOOL    sendcbflag = FALSE;
    u8      ltps;
    u8      lrps;
    u8      result;

    ret = EXILock(ET_CHAN, ET_DEV, exiinthandler);  // Shouldn't fail.
    if (ret == FALSE)
    {
        return;
    }
    exiisr = readEXIcmd(ME_EXI_ISR);
    writeEXIcmd(ME_EXI_IMR, 0);
    switch (__cntlzw(exiisr))
    {
      case INT_EXI_MAC:
        writeEXIcmd(ME_EXI_ISR, ME_EXI_ISR_MAC);
        break;
      case INT_EXI_CMDERR:
        writeEXIcmd(ME_EXI_ISR, ME_EXI_ISR_CMDERR);
        writeEXIcmd(ME_EXI_IMR, EXIMASK);
        unlock();
        ASSERTMSG(0, "exi cmderr\n");
        return;
      case INT_EXI_TIMEOUT:
        writeEXIcmd(ME_EXI_ISR, ME_EXI_ISR_TIMEOUT);
        writeEXIcmd(ME_EXI_IMR, EXIMASK);
        unlock();
        return;
      case INT_EXI_CERTIFY:
        patchthru();
        writeEXIcmd(ME_EXI_ISR, ME_EXI_ISR_CERTIFY);
        writeEXIcmd(ME_EXI_IMR, EXIMASK);
        unlock();
        return;
      case INT_EXI_HASHCOMPLETE:
        result = readEXIcmd(ME_EXI_RESULT);
        writeEXIcmd(ME_EXI_ISR, ME_EXI_ISR_HASHCOMPLETE);
        writeEXIcmd(ME_EXI_IMR, EXIMASK);
        unlock();
        if (result != ME_EXI_RESULT_OK)
        {
            ASSERTMSG(0, "ETHLIB: hash func error\n");
        }
        return;
    }

    ir = readcmd(ME_IR);

    if (__recvpriorityflag == TRUE && (ir & ME_IR_RI) != 0)
    {
        ethstat.rcvcount++;
        recvsub0();

        __ETHInterruptTime = OSGetTime() - __OSLastInterruptTime;
        return;         // recv process is done outside of exiinthandler
    }

    switch (__cntlzw(ir))
    {
      case INT_RBFI:
        ethstat.rbf++;
        writecmd(ME_IR, ME_IR_RBFI);
        break;
      case INT_BUSEI:
        writecmd(ME_IR, ME_IR_BUSEI);
        break;
      case INT_FIFOEI:
        ethstat.fifoe++;
        writecmd(ME_IR, ME_IR_FIFOEI);
        break;
      case INT_TEI:
        ethstat.te++;
        ASSERT(__sendbusy);
        __sendbusy = FALSE;
        ltps = readcmd(ME_LTPS);

        if (ltps & ME_LTPS_CRSLOST)
        {
            ethstat.tx_crs++;
        }
        if (ltps & ME_LTPS_UF)
        {
            ethstat.tx_uf++;
        }
        if (ltps & ME_LTPS_OWC)
        {
            ethstat.tx_owc++;
        }

        sendcbflag = TRUE;
        writecmd(ME_IR, ME_IR_TEI);
        break;
      case INT_REI:
        ethstat.re++;

        lrps = readcmd(ME_LRPS);
        if (lrps & ME_LRPS_BF)
        {
            ethstat.rx_bf++;
        }
        if (lrps & ME_LRPS_CRC)
        {
            ethstat.rx_crc++;
        }
        if (lrps & ME_LRPS_FAE)
        {
            ethstat.rx_fae++;
        }
        if (lrps & ME_LRPS_FO)
        {
            ethstat.rx_fo++;
        }
        if (lrps & ME_LRPS_RW)
        {
            ethstat.rx_rw++;
        }
        if (lrps & ME_LRPS_RF)
        {
            ethstat.rx_rf++;
        }

        writecmd(ME_IR, ME_IR_REI);
        break;
      case INT_TI:
        ASSERT(__sendbusy);
        ethstat.sendcount++;
        __sendbusy = FALSE;
        sendcbflag = TRUE;
        ltps = readcmd(ME_LTPS);
        writecmd(ME_IR, ME_IR_TI);
        break;
      case INT_RI:
        ethstat.rcvcount++;
        recvsub0();
        return;         // recv process is done outside of exiinthandler
      case INT_CNTOFI:
        ethstat.cntof++;
        writecmd(ME_IR, ME_IR_CNTOFI);
        break;
      default:
        break;
    }
    writeEXIcmd(ME_EXI_IMR, EXIMASK);
    unlock();

    if (sendcbflag)
    {
        ETHCallback2 callback;

        callback = __sendcallback;
        __sendcallback = NULL;
        callback(ltps);
    }

    __ETHInterruptTime = OSGetTime() - __OSLastInterruptTime;
}

s32 ETHInit(s32 mode)
{
    #pragma unused (mode)
    static BOOL initialized;
    u32 cid;

    if (!initialized)
    {
        initialized = TRUE;
        OSRegisterVersion(__ETHVersion);
    }

    OSInitThreadQueue(&threadQ);

    // Enable EXI32M
    SI_EXILK_SET_LOCK(__SIRegs[SI_EXILK_IDX], SI_EXILK_UNLOCKED);

    waitexilock();

    __sendbusy = FALSE;
    memset(&ethstat, 0x00, sizeof(ethstat));
    protoarray = NULL;
    protonum = 0;
    recvcallback0 = NULL;
    recvcallback1 = NULL;

    cid = 0;
    EXIGetID(ET_CHAN, ET_DEV, &cid);
    if (cid != EXI_ETHER)
    {
        unlock();
        OSReport("cid = %08x\n", cid);
        return ETH_ERROR_NODEVICE;
    }

    reset();

    readcmdLong(ME_PAR0, private_macaddr, sizeof(private_macaddr));

    __revid = readEXIcmd(ME_EXI_REVID);
    writeEXIcmdLong(ME_EXI_DEVID, &__devid, sizeof(__devid));

    writeEXIcmd(ME_EXI_AC_START, __acstart);

    {   // only effective for MPBBA
        u8  fcsr, phy0r;

        fcsr = readcmd(ME_FCSR);
        fcsr &= ~ME_FCSR_BACKEN;    // turn off BACKPRESSURE !!
        writecmd (ME_FCSR, fcsr);

        writecmd(ME_PHY4R, ME_PHY4R_PHY_ANAR_10HD);
        phy0r = readcmd(ME_PHY0R);
        phy0r |= ME_PHY0R_PHY_ANRESTART;
        writecmd (ME_PHY0R, phy0r);
    }

    writecmd(ME_NCRB, 0x00);

    // ME_TRB: Keep 0x2E all the time. If there is a software reset, set 0x2E again.
    //   3.0 No connect
    //   3.1 RDNCNTCB - random counter reset bar
    //   3.2 RDNCNTSB - random conter set bar
    //   3.3 COLCNTCB - collision counter reset bar
    //   3.4 No connect
    //   3.5 BKCNTLB     - Backoff Counter (decided by collision counter and random counter)
    //   3.6 No connect
    //   3.7 No connect
    writecmd(ME_TRB, 0x2e);     // to reduce collision

    writecmd(ME_MISC2, ME_MISC2_AUTORCVR);
    writecmd(ME_IMR, 0x00);
    writeEXIcmd(ME_EXI_IMR, EXIMASK);
    EXISetExiCallback(ET_INT_CHAN, exiinthandler);

    unlock();

    return ETH_OK;
}

void ETHSendAsync(void* addr, s32 length, ETHCallback2 callback2)
{
    BOOL disabled;

    ASSERT(!__sendbusy);
    ASSERT(((u32) addr % 32) == 0);

    DCStoreRange(addr, (u32) length);

    disabled = OSDisableInterrupts();

    __sendbusy = TRUE;
    __sendadr = addr;
    __sendlen = (length < 60) ? 60 : length;
    __sendcallback = callback2;

    sendUpdate = TRUE;
    if (lock())
    {
        unlock();
    }

    OSRestoreInterrupts(disabled);
}

static void sendsub0(void)
{
    u32 etcmd;

    __senddmalen = (s32) OSRoundDown32B(__sendlen);
    __sendimmlen = __sendlen - __senddmalen;
    etcmd = ET4BYTEBIT | 0x40000000 | ME_WRTXFIFOD << 8;
    EXISelect(ET_CHAN, ET_DEV, ET_FREQ);
    EXIImm(ET_CHAN, (u8*) &etcmd, sizeof(etcmd), EXI_WRITE, NULL);
    EXISync(ET_CHAN);
    EXIDma(ET_CHAN, __sendadr, __senddmalen, EXI_WRITE, sendsub1);
}

static void sendsub1(s32 chan, OSContext* context)
{
    #pragma unused(chan, context)
    u8 ncra;

    if (__sendimmlen)
    {
        EXIImmEx(ET_CHAN, __sendadr + __senddmalen, __sendimmlen,  EXI_WRITE);
    }

    EXIDeselect(ET_CHAN);

    ncra = readcmd(ME_NCRA);

    ASSERT((ncra & ME_NCRA_TXFIFO) == 0);

    ncra |= ME_NCRA_TXFIFO;
    writecmd(ME_NCRA, ncra);
    unlock();
}

void ETHGetMACAddr(u8* macaddr)
{
    memcpy(macaddr, private_macaddr, sizeof(private_macaddr));
}

void ETHSetMACAddr(u8* macaddr)
{
    memcpy(private_macaddr, macaddr, sizeof(private_macaddr));

    waitexilock();
    writecmdLong(ME_PAR0, private_macaddr, sizeof(private_macaddr));
    unlock();
}

void ETHSetRecvCallback(ETHCallback0 cb0, ETHCallback1 cb1)
{
    BOOL disabled;

    disabled = OSDisableInterrupts();
    recvcallback0 = cb0;
    recvcallback1 = cb1;
    recvcallbackUpdate = TRUE;
    if (lock())
    {
        unlock();
    }
    OSRestoreInterrupts(disabled);
}

static  BOOL linkState(void)
{
    u8 nways;

    nways = readcmd(ME_NWAYS);
    if ((nways & ME_NWAYS_LS10) != 0 || (nways & ME_NWAYS_LS100) != 0)
    {
        return TRUE;
    }
    return FALSE;
}

BOOL ETHGetLinkState(void)          // nwayc-->nways�@01.23.2002
{
    BOOL    ret;

    waitexilock();
    ret = linkState();
    unlock();
    return ret;
}

static  BOOL linkStateAsyncMain(BOOL *linkstateptr)
{
    if (EXILock(ET_CHAN, ET_DEV, NULL))
    {
        *linkstateptr = linkState();
        unlock();
        return TRUE;
    }
    return FALSE;
}

BOOL ETHGetLinkStateAsync(BOOL *status)
{
    ASSERT(status);
    return linkStateAsyncMain(status);
}

s32 ETHGetNWAYMode(void)
{
    u8 nways;

    waitexilock();
    nways = readcmd(ME_NWAYS);
    unlock();

    if (nways & ME_NWAYS_100TXF)
    {
        return ETH_MODE_100FULL;
    }
    if (nways & ME_NWAYS_100TXH)
    {
        return ETH_MODE_100HALF;
    }
    if (nways & ME_NWAYS_10TXF)
    {
        return ETH_MODE_10FULL;
    }
    if (nways & ME_NWAYS_10TXH)
    {
        return ETH_MODE_10HALF;
    }
    return ETH_MODE_UNKNOWN;
}

void ETHSetProtoType(u16 *array, s32 num)
{
    BOOL flag;

    ASSERT((num != 0 && array != NULL) || (num == 0 && array == NULL));
    flag = OSDisableInterrupts();
    protoarray = array;
    protonum = num;
    OSRestoreInterrupts(flag);
}

void ETHGetStatus(ETHStat *stat)
{
    BOOL flag;

    ASSERT(stat);
    flag = OSDisableInterrupts();
    *stat = ethstat;
    OSRestoreInterrupts(flag);
}


s32 ETHGetLibraryVersion(void)
{
    return ETHVERSION;
}

s32 ETHGetBBAType(void)
{
    return ETH_BBA_MP;
}

#define POLY32 0x04c11db6

static u16 HashIndex(const u8* address)
{
    u32 crc = 0xffffffff;
    u16 index = 0;
    u32 msb; // mdy - 07/13/99
    u8  byte;
    s32 byteLen, bit, shift;

    for (byteLen = 0; byteLen < 6; byteLen++, address++)
    {
        byte = *address;

        for (bit=0; bit < 8 ; bit++)
        {
            msb = crc >> 31;
            crc <<= 1;

            if (msb ^ (byte & 1))
            {
                crc ^= POLY32;
                crc |= 1;
            }

            byte >>= 1;
        }
    }

    for (bit=31, shift=5; shift >= 0; shift--, bit--)
        index |= (((crc>>bit) & 1) << shift);

    /* MX98715BEC
    for (bit=26, shift=5; shift >= 0; shift--, bit++)
        index |= (((crc>>bit) & 1) << shift);
    //*/
    return index;
}

void ETHAddMulticastAddress(const u8* macaddr)
{
    BOOL disabled;
    u16  index;
    u8   marNo;
    u8   bitNo;
    u8   ncrb;

    // Set to promiscuous mode (not used by ip.a)
    if (memcmp(macaddr, broadcastheader, 6) == 0)
    {
        waitexilock();
        ncrb = readcmd(ME_NCRB);
        ncrb |= ME_NCRB_PM;
        writecmd(ME_NCRB, ncrb);
        unlock();
        return;
        // NOT REACHED HERE
    }

    index = HashIndex(macaddr);
    marNo = (u8) (index / 8);
    bitNo = (u8) (index % 8);

    disabled = OSDisableInterrupts();
    mcastmap[marNo] |= (1u << bitNo);
    mcastUpdate = TRUE;
    if (lock())
    {
        unlock();
    }
    OSRestoreInterrupts(disabled);
}

void ETHChangeIntPriority(BOOL flag)
{
    __recvpriorityflag = flag;
}

u32 ETHGetREVID(void)
{
    u32 revid;

    waitexilock();
    revid = (u32) readEXIcmd(ME_EXI_REVID);
    unlock();
    return revid;
}

void ETHClearMulticastAddresses(void)
{
    BOOL disabled;

    disabled = OSDisableInterrupts();
    memset(mcastmap, 0x00, sizeof(mcastmap));
    mcastUpdate = TRUE;
    if (lock())
    {
        unlock();
    }
    OSRestoreInterrupts(disabled);
}

static void unlockcallback(s32 chan, OSContext* context)
{
    #pragma unused(chan, context)
    if (lock())
    {
        unlock();
    }
}

static BOOL lock(void)
{
    return EXILock(ET_CHAN, ET_DEV, unlockcallback);
}

static void unlock(void)
{
    BOOL disabled;

    disabled = OSDisableInterrupts();

    // Deferred process for ETHAddMulticastAddress()
    // and ETHClearMulticastAddresses()
    if (mcastUpdate)
    {
        mcastUpdate = FALSE;
        writecmdLong(ME_MAR0, mcastmap, sizeof(mcastmap));
    }

    // Deferred process for ETHSetRecvCallback()
    if (recvcallbackUpdate)
    {
        recvcallbackUpdate = FALSE;
        if (recvcallback0 == NULL)
        {
            writecmd(ME_NCRA, 0);       // stop receive (turn off NCRA_SR bit)
            writecmd(ME_IMR, ME_IMR_TIM | ME_IMR_TXEIM);
        }
        else
        {
            ASSERT(recvcallback0 != NULL && recvcallback1 != NULL);
            writecmd(ME_IMR, 0xff);
            recvinit();                 // start receive in recvinit()
        }
    }

    // Deferred process for ETHSendAsync()
    if (sendUpdate)
    {
        sendUpdate = FALSE;
        sendsub0();
        // unlock() is called by sendsub1().
    }
    else
    {
        EXIUnlock(ET_CHAN);

        if (lockUpdate)
        {
            lockUpdate = FALSE;
            OSWakeupThread(&threadQ);
        }
    }

    OSRestoreInterrupts(disabled);
}
