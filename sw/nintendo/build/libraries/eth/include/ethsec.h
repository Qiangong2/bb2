/*---------------------------------------------------------------------------*
  Project:  ETH API
  File:     ethsec.h

  Copyright 2002 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: ethsec.h,v $
  Revision 1.1.1.1  2004/06/08 23:11:08  paulm
  Source for Network Base Package 05-Aug-2003

    
    1     9/19/02 10:56 Shiki
    Initial check-in.

 *---------------------------------------------------------------------------*/

#ifndef __ETHSEC_H__
#define __ETHSEC_H__

#include <private/eth.h>

#ifdef __cplusplus
extern "C" {
#endif

BOOL __ETHFilter  ( u8* buf, s32 len );
BOOL __ETHPostSend( u8 ltps, ETHCallback2 callback, void* prev );

#ifdef __cplusplus
}
#endif

#endif  // __ETHSEC_H__
