static u32 hashfunc(u32 seccode)
{
	u8 p,q,r,s;
	u8 a[4];
	u8 dev[2];
	memcpy(a, &seccode, sizeof(seccode));
	memcpy(dev, &__devid, sizeof(__devid));	
	p = (u8)((a[0] + a[1] * 0xc1 + 0x18 + __revid) ^ (a[3] * a[2] + 0x90));
	q = (u8)((a[1] + a[2] + 0x90) ^ (p - 0xc1 + a[0]));
	r = (u8)(0xc8 + a[2] ^(p + ((dev[0] + __revid * 0x23) ^ 0x19)));
	s = (u8)((a[3] + (0x90 ^ (0xc8 + dev[1]))) ^ (0xc1 + a[0]));
	return (u32)((p << 24) | (q << 16) | (r << 8) | s);
}

