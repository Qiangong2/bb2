/*---------------------------------------------------------------------------*
  Project:  Definitions for ether EXI device
  File:

  Copyright 2002, 2003 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: etherdef.h,v $
  Revision 1.1.1.1  2004/06/08 23:11:08  paulm
  Source for Network Base Package 05-Aug-2003

    
    5     7/01/03 16:55 Shiki
    Clean up "etherdef.h".
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#ifndef __ETHERDEF_H__
#define __ETHERDEF_H__

#ifdef __cplusplus
extern "C" {
#endif

// EXI registers

#define ME_EXI_CID  0x00        // customer ID

#ifdef VIEWERBBA
#define ME_EXI_AC_START 0x02    // access start code
#define ME_EXI_IMR  0x03        // interrupt mask register
#define ME_EXI_ISR  0x04        // interrupt status register
#define ME_EXI_SWR  0x01        // software reset
#elif defined(PROTOBBA)
#define ME_EXI_AC_START 0x01    // access start code
#define ME_EXI_IMR  0x02        // interrupt mask register
#define ME_EXI_ISR  0x03        // interrupt status register
#define ME_EXI_MAC  0x04        // MAC address
#define ME_EXI_SWR  0x0f        // software reset
#elif defined(MPBBA)
#define ME_EXI_REVID    0x01
#define ME_EXI_IMR      0x02        // interrupt mask register
#define ME_EXI_ISR      0x03        // interrupt status register
#define ME_EXI_DEVID    0x04
#define ME_EXI_AC_START 0x05    // access start code
#define ME_EXI_RNDA     0x08
#define ME_EXI_FA       0x09
#define ME_EXI_ANSWER   0x0a
#define ME_EXI_RESULT   0x0b
#define ME_EXI_REMAIN   0x0c
#define ME_EXI_REQUEST  0x0d
#define ME_EXI_VALID    0x0e
#define ME_EXI_SWR      0x0f        // software reset


#define ME_EXI_RESULT_DEFAULT   0x00
#define ME_EXI_RESULT_OK        0x01
#define ME_EXI_RESULT_NG        0x02


#endif

#define ME_EXI_IMR_MACE     0x80
#define ME_EXI_IMR_TIMEOUTE 0x40
#define ME_EXI_IMR_CMDERRE  0x20
#define ME_EXI_IMR_CERTIFYE 0x10
#define ME_EXI_IMR_HASHCOMPLETEE    0x08

#define ME_EXI_ISR_MAC      0x80
#define ME_EXI_ISR_TIMEOUT  0x40
#define ME_EXI_ISR_CMDERR   0x20
#define ME_EXI_ISR_CERTIFY  0x10
#define ME_EXI_ISR_HASHCOMPLETE 0x08

// 98726 registers

#define ME_NCRA     0x00        // network control register A
#define ME_NCRB     0x01        // network control register B
#define ME_TRA      0x02        // GMAC test register A
#define ME_TRB      0x03        // GMAC test register B
#define ME_LTPS     0x04        // last transmitted packet status
#define ME_LRPS     0x05        // last received packet status
#define ME_MPCL     0x06        // missed packet counter
#define ME_MPCH     0x07
#define ME_IMR      0x08        // interrupt mask register
#define ME_IR       0x09        // interrupt register
#define ME_BPL      0x0a        // boundary page pointer register
#define ME_BPH      0x0b
#define ME_TLBPL    0x0c        // tx low boundary page pointer register
#define ME_TLBPH    0x0d
#define ME_TWPL     0x0e        // transmit buffer write page pointer register
#define ME_TWPH     0x0f
#define ME_IOBL     0x10        // IO base page register
#define ME_IOBH     0x11
#define ME_TRPL     0x12        // transmit buffer read page pointer register
#define ME_TRPH     0x13
#define ME_RXINTTL  0x14        // receive interrupt timer
#define ME_RXINTTH  0x15
#define ME_RWPL     0x16        // receive buffer write page pointer register
#define ME_RWPH     0x17
#define ME_RRPL     0x18        // receive buffer read page pointer register
#define ME_RRPH     0x19        // 64K memory bank address (upper 4 bits)
#define ME_RHBPL    0x1a        // RX high boundary page pointer register
#define ME_RHBPH    0x1b
#define ME_EIR      0x1c        // EEPROM interface register

#define ME_PAR0     0x20        // Physical Address
#define ME_PAR1     0x21        // Physical Address
#define ME_PAR2     0x22        // Physical Address
#define ME_PAR3     0x23        // Physical Address
#define ME_PAR4     0x24        // Physical Address
#define ME_PAR5     0x25        // Physical Address

#define ME_MAR0     0x26        // Hash Table Register
#define ME_MAR1     0x27
#define ME_MAR2     0x28
#define ME_MAR3     0x29
#define ME_MAR4     0x2a
#define ME_MAR5     0x2b
#define ME_MAR6     0x2c
#define ME_MAR7     0x2d

#define ME_NWAYC    0x30        // NWAY configuration register
#define ME_NWAYS    0x31        // NWAY status register
#define ME_GCA      0x32        // GMAC configuration A register
#define ME_GCB      0x33        // GMAC configuration B register


#define ME_LPC      0x3b        // link partner link code register
#define ME_DSR      0x3c        // TX/RX DMA status register

// not present on MX98730
//#define ME_MISC1  0x3d        // MISC control register

#define ME_TXFIFOCNTL   0x3e    // TX FIFO byte counter
#define ME_TXFIFOCNTH   0x3f

#define ME_ID1L     0x44
#define ME_ID1H     0x45
#define ME_ID2L     0x46
#define ME_ID2H     0x47
#define ME_WRTXFIFOD    0x48        // write TX FIFO data port
#define ME_MISC2        0x50        // MISC control register 2

// new from 98730
#define ME_FCR      0x58
#define ME_FLTR     0x59
#define ME_FHTR     0x5a
#define ME_FCSR     0x5b
#define ME_PHY0R    0x5c
#define ME_PHY1R    0x5d
#define ME_PHY4R    0x5e
#define ME_PHY5R    0x5f
#define ME_MIIMR    0x60

#define ME_PACKET   0x100       // packet memory start point

#define ME_NCRA_RESET      0x01
#define ME_NCRA_ST0        0x02
#define ME_NCRA_ST1        0x04
#define ME_NCRA_SR         0x08
#define ME_NCRA_LB0        0x10
#define ME_NCRA_LB1        0x20
#define ME_NCRA_INTMODE    0x40
#define ME_NCRA_CLKSEL     0x80
#define ME_NCRA_TXFIFO  0x04
#define ME_NCRA_TXDMAPOLL   0x02
#define ME_NCRA_TXCMD   0x06

// Network Control Register B
#define ME_NCRB_PR         0x01
#define ME_NCRB_CA         0x02
#define ME_NCRB_PM         0x04
#define ME_NCRB_PB         0x08
#define ME_NCRB_AB         0x10     // accept the broadcast
#define ME_NCRB_HBD        0x20


// Interrupt Mask Register

#define ME_IMR_CNTOFIM     0x01
#define ME_IMR_RIM         0x02
#define ME_IMR_TIM         0x04
#define ME_IMR_RXEIM       0x08
#define ME_IMR_TXEIM       0x10
#define ME_IMR_FIFOEIM     0x20
#define ME_IMR_BUSEIM      0x40
#define ME_IMR_RBFIM       0x80

// Interrupt Register
#define ME_IR_CNTOFI       0x01
#define ME_IR_RI           0x02
#define ME_IR_TI           0x04
#define ME_IR_REI          0x08
#define ME_IR_TEI          0x10
#define ME_IR_FIFOEI       0x20
#define ME_IR_BUSEI        0x40
#define ME_IR_RBFI         0x80

// NWAY Configuration Register
#define ME_NWAYC_FD         0x01
#define ME_NWAYC_PS100      0x02
#define ME_NWAYC_ANE        0x04

/****   NOT PRESENT on 98730
#define ME_NWAYC_ANS_MASK   0x38
#define ME_NWAYC_ANS_RESTART    0x08
#define ME_NWAYC_ANS_LINKGOOD   0x28
#define ME_NWAYC_LTE        0x80
****/

// NWAY Status Register
#define ME_NWAYS_LS10       0x01
#define ME_NWAYS_LS100      0x02
#define ME_NWAYS_100TXF     0x10
#define ME_NWAYS_100TXH     0x20
#define ME_NWAYS_10TXF      0x40
#define ME_NWAYS_10TXH      0x80

//GMAC Configuration A Register
#define ME_GCA_BPSCRM       0x01
#define ME_GCA_PBW          0x02
#define ME_GCA_SLOWSRAM     0x04
#define ME_GCA_ARXERRB      0x08
#define ME_GCA_MISEL        0x10
#define ME_GCA_AUTOPUB      0x20
#define ME_GCA_TXFIFOCNTEN  0x40
#define ME_GCA_RESERVED     0x80


//GMAC Configuration B Register

#define ME_GCB_TTHD        0xFC
#define ME_GCB_RTHD        0xF3
#define ME_GCB_TBLEN       0xCF
#define ME_GCB_RBLEN       0x3F

#define ME_GCB_TH_1_4          0x01
#define ME_GCB_TH_3_4          0x02
#define ME_GCB_TH_16           0x03
#define ME_GCB_RH_1_4          0x04
#define ME_GCB_RH_3_4          0x08
#define ME_GCB_RH_16           0x0c


/****   not present on MX98730
 MISC Control Register 1
#define ME_MISC1_BURSTDMA   0x01
#define ME_MISC1_DISLDMA    0x02
#define ME_MISC1_TPF        0x04
#define ME_MISC1_TPH        0x08
#define ME_MISC1_TXF        0x10
#define ME_MISC1_TXH        0x20
#define ME_MISC1_TXFIFORST  0x40
#define ME_MISC1_RXFIFORST  0x80
***/

// MISC Control Register 2
#define ME_MISC2_AUTORCVR   0x80

// new from 98730
#define ME_FCSR_BACKEN          0x80
#define ME_PHY0R_PHY_ANRESTART  0x04
#define ME_PHY4R_PHY_ANAR_10HD  0x01

//GMAC TX/RX DMA Status Register
#define TXRXDMA     (XBYTE[0x003C])
#define TXDMA       (XBYTE[0x003C]>>4&0x0f) // TXDMA[7:4]...[7654 3210]
#define RXDMA       (XBYTE[0x003C]&0x0f)    // RXDMA[3:0]

/*
#define TXDMA       (XBYTE[0x003C]&0x0F) // TXDMA[7:4]...[7654 3210]
#define RXDMA       (XBYTE[0x003C]>>4)   // RXDMA[3:0]----> Derek */


typedef struct Descriptor
{
    u8  nextpacketL;
    u8  packetlengthL:4;
    u8  nextpacketH:4;
    u8  packetlengthH;
    u8  status;
} Descriptor;

typedef struct Descriptor2
{
    u16 nextpacket;
    u16 packetlength;
    u8  status;
} Descriptor2;

#ifdef __cplusplus
}
#endif

#endif  // __ETHERDEF_H__
