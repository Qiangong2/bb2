/*---------------------------------------------------------------------------*
  Project:  GAMECUBE Audio sound file parsor win32 DLL
  File:     chunkname.h

  Copyright 1998, 1999, 2000, 2001 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.
 *---------------------------------------------------------------------------*/

#ifndef __CHUNKNAME_H__
#define __CHUNKNAME_H__


/*--------------------------------------------------------------------------*
    chunk_name macro
 *--------------------------------------------------------------------------*/
#define chunk_name(a,b,c,d)(	    \
				(a & 0xFF)	        \
			+	((b & 0xFF) << 8)	\
			+	((c & 0xFF) << 16)	\
			+	((d & 0xFF) << 24))


#endif  // __CHUNKNAME_H_
