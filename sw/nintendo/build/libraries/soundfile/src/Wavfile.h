/*--------------------------------------------------------------------------*
  Project: Dolphin soundfile DLL
  File:    wavfile.h

  Copyright 2001 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.
 *--------------------------------------------------------------------------*/
#include "types.h"
#include "chunkname.h"


/*--------------------------------------------------------------------------*
    RIFF chunk names
 *--------------------------------------------------------------------------*/
#define CHUNK_RIFF  chunk_name('R','I','F','F')
#define CHUNK_WAVE  chunk_name('W','A','V','E')


typedef struct
{
  char      chunkId[4];
  u32       chunkSize;
  u16       waveFormatType;
  u16       channel;
  u32       samplesPerSec;
  u32       bytesPerSec;
  u16       blockSize;
  u16       bitsPerSample;
} FMTCHUNK;

typedef struct
{
  char      chunkId[4];
  u32       chunkSize;
} DATACHUNK;

typedef struct
{
  char      chunkId[4];
  u32       chunkSize;
  char      formType[4];
  FMTCHUNK  fmt;
  DATACHUNK data;
} WAVECHUNK;


void wavCreateHeader(WAVECHUNK *wc,
                                int       numOfSamples,
                                int       channel,
                                int       bitsPerSample,
                                int       frequency
                            );
void wavWriteHeader(WAVECHUNK *wc, FILE *outfile);
int  wavReadHeader(WAVECHUNK *wc, FILE *infile);
u32  wavGetSamples(WAVECHUNK *wc);
u32  wavGetChannels(WAVECHUNK *wc); 
u32  wavGetBitsPerSample(WAVECHUNK *wc);  
u32  wavGetSampleRate(WAVECHUNK *wc);


  