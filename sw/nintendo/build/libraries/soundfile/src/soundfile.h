/*---------------------------------------------------------------------------*
  Project:  GAMECUBE Audio sound file parsor win32 DLL
  File:     soundfile.h

  Copyright 1998, 1999, 2000, 2001 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.
 *---------------------------------------------------------------------------*/

#ifndef __SOUNDFILE_H__
#define __SOUNDFILE_H__

#define MAKE_A_DLL

#ifdef MAKE_A_DLL
#define LINKDLL __declspec(dllexport)
#else
#define LINKDLL __declspec(dllimport)
#endif

/*--------------------------------------------------------------------------*
    Status
 *--------------------------------------------------------------------------*/
#define SOUND_FILE_SUCCESS      0
#define SOUND_FILE_FORMAT_ERROR 1
#define SOUND_FILE_FOPEN_ERROR  2


/*--------------------------------------------------------------------------*
    SOUNDINFO struct
 *--------------------------------------------------------------------------*/
typedef struct
{

    int     channels;           // Number of channels
    int     bitsPerSample;      // Number of bits per sample
    int     sampleRate;         // Sample rate in Hz
    int     samples;            // Number for samples
    int     loopStart;          // 1 based sample index for loop start
    int     loopEnd;            // 1 based sample count for loop samples
    int     bufferLength;       // buffer length in bytes

} SOUNDINFO;


/*--------------------------------------------------------------------------*
    Function prototypes
 *--------------------------------------------------------------------------*/
LINKDLL int getSoundInfo        (char *path, SOUNDINFO *info);
LINKDLL int getSoundSamples     (char *path, SOUNDINFO *info, void *dest);

LINKDLL int writeWaveFile       (char *path, SOUNDINFO *info, void *samples);
LINKDLL int writeAiffFile       (char *path, SOUNDINFO *info, void *samples);

#endif // __SOUNDFILE_H__