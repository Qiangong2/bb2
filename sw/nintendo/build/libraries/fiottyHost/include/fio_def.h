/*---------------------------------------------------------------------------*
  Project:  FIO secure define for MCC 
  File:     fio_def.h

  Copyright 2000 - 2001 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: fio_def.h,v $
  Revision 1.1.1.1  2004/06/09 17:39:20  paulm
  GC library source from Nintendo SDK

    
    3     6/18/01 4:32p Hashida
    Changed copyright informaiton.
    
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#ifndef __MCC_FIO_SECURE_DEFINE_H__
#define __MCC_FIO_SECURE_DEFINE_H__

// HEADER INCLUDE
// ================
#include <dolphin/fio.h>

#ifdef __cplusplus
extern "C" {
#endif

	// DEFINES
	// ================
	// ------------------------------------------------------------
	// MESSAGE FORMAT (FIO)
	// ------------------------------------------------------------
	// 2.MESSAGE FORMAT(FIO HEADER)
	typedef struct {
		u32 code;
		u32 number;
	} FIOHeader;

	typedef enum{
		FIO_CODE_OPEN,
		FIO_CODE_OPENR,
		FIO_CODE_CLOSE,
		FIO_CODE_CLOSER,
		FIO_CODE_READ,
		FIO_CODE_READR,
		FIO_CODE_WRITE,
		FIO_CODE_WRITER,
		FIO_CODE_SEEK,
		FIO_CODE_SEEKR,
		
		FIO_CODE_FLUSH,
		FIO_CODE_FLUSHR,
		FIO_CODE_STAT,
		FIO_CODE_STATR,
		FIO_CODE_ERROR,
		FIO_CODE_ERRORR,
		FIO_CODE_FFIRST,
		FIO_CODE_FFIRSTR,
		FIO_CODE_FNEXT,
		FIO_CODE_FNEXTR,

		FIO_CODE_OPENDIR,
		FIO_CODE_OPENDIRR,
		FIO_CODE_CLOSEDIR,
		FIO_CODE_CLOSEDIRR,
		FIO_CODE_DIRSIZE,
		FIO_CODE_DIRSIZER,
		FIO_CODE_READDIR,
		FIO_CODE_READDIRR,

		FIO_CODE_MAX
	} FIO_CODE;

	// 3.MESSAGE
	// 3.1 FIO_CODE_OPEN
	typedef struct {
		u32 flag;
		s8 filename;
	} FIOFormatCodeOpen;
	/*
	#define	FIO_RDONLY	0x0001
	#define	FIO_WRONLY	0x0002
	#define	FIO_RDWR	0x0003
	#define	FIO_CREAT	0x0200
	#define	FIO_TRUNC	0x0400
	#define	FIO_EXCL	0x0800
	*/
	// 3.2 FIO_CODE_OPENR
	typedef struct {
		u32 result;
		u32 descriptor;
	} FIOFormatCodeOpenR;
	/*
	#ifndef FIO_EACCES
	#define	FIO_EACCES	(13)
	#define	FIO_EDQUOT	(122)
	#define	FIO_EEXIST	(17)
	#define	FIO_ENOENT	(2)
	#define	FIO_ENOSPC	(28)
	#define	FIO_ENFILE	(23)
	#define	FIO_EROFS	(30)
	#define	FIO_EBADF	(9)
	#define	FIO_EIO	(5)
	#define	FIO_EISDIR	(21)
	#endif
	*/
	// 3.3 FIO_CODE_CLOSE
	typedef struct {
		u32 descriptor;
	} FIOFormatCodeClose;
	// 3.4 FIO_CODE_CLOSER
	typedef struct {
		u32 result;
	} FIOFormatCodeCloseR;
	// 3.5 FIO_CODE_READ
	typedef struct {
		u32 descriptor;
		u32 nbytes;
	} FIOFormatCodeRead;
	// 3.6 FIO_CODE_READR
	typedef struct {
		u32 result;
		u32 nbytes;
		char data;
	} FIOFormatCodeReadR;
	// 3.7 FIO_CODE_WRITE
	typedef struct {
		u32 descriptor;
		u32 nbytes;
		char data;
	} FIOFormatCodeWrite;
	// 3.8 FIO_CODE_WRITER
	typedef struct {
		u32 result;
		u32 nbytes;
	} FIOFormatCodeWriteR;
	// 3.9 FIO_CODE_SEEK
	typedef struct {
		u32 descriptor;
		u32 offset;
		u32 base;
	} FIOFormatCodeSeek;
	// 3.10 FIO_CODE_SEEKR
	typedef struct {
		u32 result;
		u32 pos;
	} FIOFormatCodeSeekR;

	// 3.11 FIO_CODE_FLUSH
	typedef struct {
		u32 descriptor;
	} FIOFormatCodeFlush;
	// 3.12 FIO_CODE_FLUSHR
	typedef struct {
		u32 result;
	} FIOFormatCodeFlushR;
	// 3.13 FIO_CODE_STAT
	typedef struct {
		u32 descriptor;
	} FIOFormatCodeStat;
	// 3.14 FIO_CODE_STATR
	typedef struct {
		u32 result;
		FIOStat	stat;
	} FIOFormatCodeStatR;
	// 3.15 FIO_CODE_ERROR
	typedef struct {
		u32 descriptor;
	} FIOFormatCodeError;
	// 3.16 FIO_CODE_ERRORR
	typedef struct {
		u32 result;
	} FIOFormatCodeErrorR;

	// 3.17 FIO_CODE_FFIRST
	typedef struct {
		char filename;
	} FIOFormatCodeFFirst;
	// 3.18 FIO_CODE_FFIRSTR
	typedef struct {
		u32 result;
		FIOFindData	findData;
	} FIOFormatCodeFFirstR;
	// 3.19 FIO_CODE_FNEXT
	typedef struct {
		u32 reserved;
	} FIOFormatCodeFNext;
	// 3.20 FIO_CODE_FNEXTR
	typedef struct {
		u32 result;
		FIOFindData	findData;
	} FIOFormatCodeFNextR;


	// FIO_CODE_OPENDIR
	typedef struct {
		s8 filename;
	} FIOFormatCodeOpenDir;
	// FIO_CODE_OPENDIRR
	typedef struct {
		u32 result;
		u32 descriptor;
	} FIOFormatCodeOpenDirR;

	// FIO_CODE_CLOSEDIR
	typedef struct {
		u32 descriptor;
	} FIOFormatCodeCloseDir;
	// 3.4 FIO_CODE_CLOSEDIRR
	typedef struct {
		u32 result;
	} FIOFormatCodeCloseDirR;

	// FIO_CODE_DIRSIZE
	typedef struct {
		u32 descriptor;
	} FIOFormatCodeGetDirSize;
	// FIO_CODE_DIRSIZER
	typedef struct {
		u32 result;
		u32 size;
	} FIOFormatCodeGetDirSizeR;

	// FIO_CODE_READDIR
	typedef struct {
		u32 descriptor;
		u32 size;
	} FIOFormatCodeReadDir;
	// FIO_CODE_READDIRR
	typedef struct {
		u32 result;
		u32 readsize;
	} FIOFormatCodeReadDirR;


	// INTERNAL DEFINES
	// --------------------
	#define FIO_PROTOCOL_ID     (0x0120)
	#define FIO_BUFFER_SIZE     (MCC_BLOCK_SIZE) // must be multiple of 32 for cache
	#define FIO_QUERY_TIMEOUT   (10) //[sec]
	// Notify
	// --------------------
	#define FIO_NOTIFY_QUERY	(0x00100000)	// QUERY : **10iiii iiii...TTYP_ID @@@
	#define FIO_NOTIFY_QUERY_R	(0x00200000)	// QUERY : **10iiii iiii...TTYP_ID @@@
	#define FIO_NOTIFY_DONE		(0x00F00000)
	// Notify Mask
	// --------------------
	#define FIO_MASK_NOTIFY		(0x00F00000)
	// Async State
	// --------------------
	typedef enum{
		FIO_ASYNC_STATE_IDOL=0,
		FIO_ASYNC_STATE_BUSY,
		FIO_ASYNC_STATE_DONE
	} FIOAsyncState;

#ifdef __cplusplus
}
#endif

#endif __MCC_FIO_SECURE_DEFINE_H__
