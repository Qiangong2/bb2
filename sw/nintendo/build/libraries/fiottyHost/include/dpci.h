/*---------------------------------------------------------------------------*
  Project:  Dolphin Protocol Communication Interface API for MCC
  File:     dpci.h

  Copyright 2000 - 2001 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: dpci.h,v $
  Revision 1.1.1.1  2004/06/09 17:39:20  paulm
  GC library source from Nintendo SDK

    
    2     6/18/01 4:32p Hashida
    Changed copyright informaiton.
    
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#ifndef __MCC_DPCI_H__
#define __MCC_DPCI_H__

// HEADER INCLUDE
// ================
#ifdef WIN32//host incluide here
#	include <win32/dolphin.types.h>
#else
#	include <dolphin/types.h>
#	define MCC_TARGET
#endif


#ifdef __cplusplus
extern "C" {
#endif

	// DEFINES
	// ================
	#define DPCI_BASE_SIZE	(8192)
	typedef u8 DPCISocket;

	// ERROR CODES
	#define DPCI_ERR_NOERR		0
	#define DPCI_ERR_INVALID	-1 	// invalid argument
	#define DPCI_ERR_INVALSOCK	-2	// invalid socket descriptor
	#define DPCI_ERR_ALREADYUSE	-3	// protocol number already used
	#define DPCI_ERR_MFILE		-4	// too many open protocols
	#define DPCI_ERR_INVALADDR	-5	// invalid address for buffer
	#define DPCI_ERR_PKTSIZE	-6	// buffer is too small
	#define DPCI_ERR_WOULDBLOCK	-7	// blocks inspite of asynchronous
	#define DPCI_ERR_ALREADYLOCK	-8	// already lock
	#define DPCI_ERR_NOTLOCKED	-9	// not locked
	#define DPCI_ERR_NOROUTE	-10	// no route to host
	#define DPCI_ERR_NOSPACE	-11	// no room left on manager
	#define DPCI_ERR_INVALHEAD	-12	// invalid deci2 header
	#define DPCI_ERR_MCCERR     -15
	#define DPCI_ERR_NOINITIALIZED -16

	// DPCI PACKET HEADER
	typedef struct {
		u32 length;
		u16 rsvd;
		u16 protocol;
	} DPCIHeader;

	// FUNCTION PROTOTYPES
	// ================
	u16 EndianConvert16( u16 n );
	u32 EndianConvert32( u32 n );
	BOOL MPDWaiting( int timeout, volatile BOOL* flag, BOOL value );

#ifdef __cplusplus
}
#endif

#endif  // __DPCI_H__
