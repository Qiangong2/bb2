/*---------------------------------------------------------------------------*
  Project:  TTY server API for MCC
  File:     ttyserver.cpp

  Copyright 2000 - 2001 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: ttyserver.c,v $
  Revision 1.1.1.1  2004/06/09 17:39:20  paulm
  GC library source from Nintendo SDK

    
    6     02/09/30 15:16 Hashida
    Source sync with Hudson (modified to use include/win32/ttyserver.h
    instead of using the local ttyserver.h).
    
    
    5     02/09/18 23:01 Hashida
    Added static version of fio/tty by Hudson.
    
    4     6/18/01 4:34p Hashida
    Changed copyright informaiton.
    
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

// HEADER INCLUDE
// ====================
#include <windows.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>

#include "../include/dpci.h"
#include <dolphin/tty.h>
#include <win32/ttyserver.h>

// INTERNAL DEFINES
// ====================
#define TTY_PROTOCOL_ID		(0x0210)
#define TTY_BLOCK_SIZE		(1)
#define TTY_BUFFER_SIZE		(MCC_BLOCK_SIZE*TTY_BLOCK_SIZE) // must be multiple of 32 for cache
#define TTY_QUERY_TIMEOUT	(5) //[sec]
// NOTIFY
// --------------------
#define	TTY_NOTIFY_QUERY	0x00100000	// QUERY : **10iiii iiii...TTY_ID @@@
#define	TTY_NOTIFY_QUERY_R	0x00200000	// QUERY : **10iiii iiii...TTY_ID @@@
#define	TTY_NOTIFY_FLUSH	0x00300000	// FLUSH : **2000mm mm...msgID @@@
#define	TTY_NOTIFY_FLUSH_R	0x00400000	// DONE  : **30ssmm ss...size/32 mm...msgID
// NOTIFY MASK
// --------------------
#define TTY_MASK_NOTIFY        (0x00F00000)
#define TTY_MASK_QUERY_PID     (0x0000FFFF)
#define TTY_MASK_FLUSH_MSGID   (0x000000FF)
#define TTY_MASK_FLUSH_SIZE    (0x0000FF00)
// ERRORS
// --------------------
#define TTY_ERR_NOERR			(0)
#define TTY_ERR_MCC_LEVEL		(1)
#define TTY_ERR_BAD_PARAMETER	(2)

// INTERNAL VALIABLES
// ====================
volatile static MCCChannel	gChID		= MCC_CHANNEL_SYSTEM;//OK
static u8					gLastErr	= TTY_ERR_NOERR;

static u8					gBuf[TTY_BUFFER_SIZE] ATTRIBUTE_ALIGN(32); // must be 32 for cache
volatile static u32			gBufHead	= 0;
volatile static u32			gBufTail	= 0;

volatile static BOOL		gThreadBreak = FALSE;//OK
volatile static HANDLE		hThreadTtyServer = INVALID_HANDLE_VALUE;//OK
volatile static TTYServerCallback fTTYServerCallback = NULL;

// INTERNAL CALLBACK PROTOTYPES
// ====================
static void  WINAPI ttyServerMccSysEvent( MCCSysEvent e );
static void  WINAPI ttyServerMccChannelEvent( MCCChannel chID, MCCEvent event, u32 value );
static DWORD WINAPI ttyServerThread( LPVOID param );

// INTERNAL FUNCTIONS
// ====================
static u32	ttyServerReceivePacket( MCCChannel chID, u32 msgID );
static BOOL	ttyServerIsInitialized(){ return (gChID!=MCC_CHANNEL_SYSTEM); }


// ================================================
// APIs
// ================================================

// Starts TTY server.
// ====================
TTY_SERVER_API BOOL TTYServerStart( MCCChannel chID, TTYServerCallback callback )
{	OSReport("TTY server.Start.\n");

	//	Checks parameters.
	if(!callback){
		//	Parameter error
		gLastErr = TTY_ERR_BAD_PARAMETER;
	}else{
		//	Channel open error
		if(!MCCOpen( chID, TTY_BLOCK_SIZE, ttyServerMccChannelEvent )){
			//	Channel open error
			gLastErr = TTY_ERR_MCC_LEVEL;
		}else{
			long lpThreadId;
			//	Masks unnecessary events.
			MCCSetChannelEventMask( chID, MCC_EVENT_READ_DONE|MCC_EVENT_READ_DONE_INSIDE|MCC_EVENT_WRITE_DONE|MCC_EVENT_WRITE_DONE_INSIDE );
			//	Sets defaults for internal information.
			gChID		= chID;
			gLastErr	= TTY_ERR_NOERR;
			gBufHead	= 0;
			gBufTail	= 0;

			// Starts server thread.
			hThreadTtyServer = CreateThread( 
				NULL,0,ttyServerThread,NULL,0,&lpThreadId );
			if(hThreadTtyServer == INVALID_HANDLE_VALUE){
				//"Failed to create thread."
				TTYServerStop();
			}else{
				OSReport("TTY server.READY.\n");
				fTTYServerCallback = callback;
				return TRUE;
			}
		}
	}
	OSReport("TTY server.NG.\n");
	return FALSE;
}

// Stops TTY server.
// ====================
TTY_SERVER_API void TTYServerStop( void )
{	OSReport("Halt TTY server.\n");

	//	Closes MCC channel.
	MCCClose( gChID );
	//	Stops server thread and waits for its termination.
	if(hThreadTtyServer != INVALID_HANDLE_VALUE){
		gThreadBreak = TRUE;
		WaitForSingleObject( hThreadTtyServer,INFINITE );
	}
	//	Resets the internal information.
	gChID		 = (MCCChannel)0;
	gLastErr	 = TTY_ERR_NOERR;
	gBufHead	 = 0;
	gBufTail	 = 0;
	fTTYServerCallback = NULL;
	OSReport("Halt TTY server.DONE\n");
}


// ================================================
//	EVENTs
// ================================================

volatile static BOOL gHostBusy=FALSE;
volatile static u32	 gMsgID=0x00000000;
static void WINAPI ttyServerMccChannelEvent( MCCChannel chID, MCCEvent event, u32 value )
{	gChID = chID;

	switch(event){
	case MCC_EVENT_USER:{
		u32	notify = value & TTY_MASK_NOTIFY;
		switch(notify){
		case TTY_NOTIFY_QUERY:{//COMPLETED
			if((value&0x0000FFFF)==TTY_PROTOCOL_ID){
				if(!MCCNotify(chID, TTY_NOTIFY_QUERY_R | TTY_PROTOCOL_ID)){
					// notify error
				}else{
				}
			}
		} break;
		case TTY_NOTIFY_FLUSH:{
			while(gHostBusy){
				gHostBusy=gHostBusy;
				OSReport(" *** Server busy.\n");
				Sleep(1);
			};
			gHostBusy	= TRUE;
			gMsgID		= (value&TTY_MASK_FLUSH_MSGID);
		} break;
		}//switch(notify)
	}	break;
	}//switch(event)
}


// ================================================
//	THREADs
// ================================================

static DWORD WINAPI ttyServerThread( LPVOID param )
{	gThreadBreak=FALSE;
	while(!gThreadBreak){
		//	packet received
		if(gHostBusy){
			//	Call receive packet proc.
			ttyServerReceivePacket( gChID,gMsgID );
			// procDone
			gHostBusy=FALSE;
		}
		Sleep(1);
	}
	return 0;
}


// ================================================
//	HOST SIDE FUNCTIONS
// ================================================

static u32 ttyServerReceivePacket( MCCChannel chID, u32 msgID )
{
	u32	resultSizeAvg=0;

	//	Read the received packet.
	if(!MCCRead( chID,0,gBuf,TTY_BUFFER_SIZE,MCC_SYNC )){
	}else{
		while(1){
			DPCIHeader	*hdr	= (DPCIHeader *)(gBuf+gBufHead);
			u32			*id		= (u32*)((DPCIHeader *)(hdr + 1));
			char		*str	= (char *)((u32 *)(id + 1));
			u32			dataSize,dataSize32; 

			//	Converts the endian.
			hdr->length		= EndianConvert32(hdr->length);
			hdr->protocol	= EndianConvert16(hdr->protocol);
			*id = EndianConvert32(*id);

			if(hdr->protocol!=TTY_PROTOCOL_ID){
				break;
			}else{
				if(*id>255){
					break;
				}else{
					if(hdr->length>TTY_BUFFER_SIZE){
						break;
					}else{
						//	Returns the received data.
						if(fTTYServerCallback)
							fTTYServerCallback(str);

						//	Creates reply messages.
						dataSize32	=	(hdr->length+31)/32;
						dataSize	=	dataSize32*32;
						gBufHead	+=	dataSize;
						gBufHead	%=	TTY_BUFFER_SIZE;

						resultSizeAvg+=dataSize32;

						if((*id)==msgID){
							//	Returns reply messages.
							if(!MCCNotify(chID, 
								TTY_NOTIFY_FLUSH_R | 
								((resultSizeAvg<<8)&TTY_MASK_FLUSH_SIZE) | 
								((*id)&TTY_MASK_FLUSH_MSGID) )){
								//	Failed to notify.
								return (u32)-1;
							}else{
								return (u32)0;
							}
						}
					}
				}
			}
			Sleep(1);
		}
	}
	//	Returns reply messages.
	if(!MCCNotify(chID, 
		TTY_NOTIFY_FLUSH_R | 
		((0<<8)&TTY_MASK_FLUSH_SIZE) | 
		((0)&TTY_MASK_FLUSH_MSGID) )){
		//	Failed to notify.
		return (u32)-1;
	}else{
		return (u32)-1;
	}
}
