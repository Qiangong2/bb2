/*---------------------------------------------------------------------------*
  Project:  Dolphin Protocol Communication Interface API for MCC
  File:     dpci.c

  Copyright 2000 - 2001 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: dpci.c,v $
  Revision 1.1.1.1  2004/06/09 17:39:20  paulm
  GC library source from Nintendo SDK

    
    3     02/09/18 23:01 Hashida
    Added static version of fio/tty by Hudson.
    
    2     6/18/01 4:33p Hashida
    Changed copyright informaiton.
    
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

// HEADER INCLUDE
// ================
#ifdef WIN32
#	include <windows.h>
#else
#	include <dolphin.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>

#include "../include/dpci.h"


//	Converts the endian of 2 bytes data.
u16 EndianConvert16( u16 n ){
	return (u16)(((n&0x00FF)<<8) | ((n&0xFF00)>>8)); 
}

//	Converts the endian of 4 bytes data.
u32 EndianConvert32( u32 n ){
	return (u32)(
		(u32)EndianConvert16( (u16)((n>>16)&0x0000FFFF) ) | 
		((u32)EndianConvert16( (u16)(n&0x0000FFFF) )<<16) );
}

//	Waits for the time specified by the value of timeout, or the valuable of flag turns to the value specified by VALUE.
BOOL MPDWaiting( int timeout, volatile BOOL* flag, BOOL value )
{	OSTick tickStart = OSGetTick(),tickDist;
	while((*flag)!=value){
		// Check count for timeout.
		tickDist = OSGetTick()-tickStart;
		tickDist = (tickDist&0x80000000)?(0x80000000-tickStart+OSGetTick()):tickDist;
		if(OSTicksToSeconds(tickDist) >= timeout){
			// timeout error
			OSReport("Error:Time is over.\n");
			return FALSE;
		}
	};
	return TRUE;
}
