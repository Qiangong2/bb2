/*---------------------------------------------------------------------------*
  Project:  FIO server API for MCC
  File:     fioserver.cpp

  Copyright 2000 - 2001 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: fioserver.cpp,v $
  Revision 1.1.1.1  2004/06/09 17:39:20  paulm
  GC library source from Nintendo SDK

    
    6     03/04/16 21:37 Hashida
    Modified FIOServerStop so that it closes all the opened files.
    
    5     02/09/30 15:16 Hashida
    Source sync with Hudson (modified to use include/win32/fioserver.h
    instead of using the local fioserver.h).
    
    
    4     02/09/18 23:00 Hashida
    Added static version of fio/tty by Hudson.
    
    3     6/18/01 4:31p Hashida
    Changed copyright informaiton.
    
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

// HEADER INCLUDE
// ====================
#include <windows.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>

#include "../include/dpci.h"
#include <dolphin/mcc.h>
#include <dolphin/fio.h>
#include "../include/fio_def.h"
#include <win32/fioserver.h>

// INTERNAL DEFINES
// ====================

// INTERNAL VALIABLES
// ====================
static MCCChannel	gChID=(MCCChannel)0;
static u8			gSizeOfBlocks=1;
static u8			gLastErr=FIO_ERR_NOERR;
static u32			gRequestSequenceNumber=0;
static char*		gResultBuf=NULL;
static BOOL			gThreadBreak=FALSE;
volatile static HANDLE hThreadFioServer = INVALID_HANDLE_VALUE;//USED
static char			gRootPath[MAX_PATH];

// INTERNAL CALLBACK PROTOTYPES
// ====================
static void WINAPI fioServerChannelEvent( MCCChannel chID, MCCEvent event, u32 value );
static DWORD WINAPI fioServerThread( LPVOID param );

// INTERNAL FUNCTION PROTOTYPES
// ====================
static void fioProcReceivePacket( void );
static void* fioPacketMakeHeader( u32 fioCode, u32 dataSize, BOOL bEndianConvert );

//static void fioSetRootPath( LPCSTR rootPath );
//static void fioOpenRootPath();
//static void fioCloseRootPath();
//static void fioSetRootPath( LPCSTR rootPath );
//static void fioGetRealPath( LPCSTR fileName, char* realPath );

static void fioProcOpen( FIOFormatCodeOpen* formatOpen );
static void fioProcClose( FIOFormatCodeClose* formatClose );
static void fioProcRead( FIOFormatCodeRead* formatRead );
static void fioProcWrite( FIOFormatCodeWrite* formatWrite );
static void fioProcSeek( FIOFormatCodeSeek* formatSeek );
static void fioProcFlush( FIOFormatCodeFlush* formatSeek );
static void fioProcStat( FIOFormatCodeStat* formatStat );
static void fioProcError( FIOFormatCodeError* formatError );
static void fioProcFFirst( FIOFormatCodeFFirst* formatFFirst );
static void fioProcFNext( FIOFormatCodeFNext* formatFNext );

static void fioProcOpenDir( FIOFormatCodeOpenDir* format );
static void fioProcCloseDir( FIOFormatCodeCloseDir* format );
static void fioProcGetDirSize( FIOFormatCodeGetDirSize* format );
static void fioProcReadDir( FIOFormatCodeReadDir* format );

static void _InitializeServerRootPath();
static void _SetServerRootPath( LPCSTR rootPath );
static BOOL _OpenRootPath( void );
static BOOL _CloseRootPath( void );
static BOOL _CheckServerRootSecurity( LPCSTR checkPath );
static BOOL _GetServerRealPath( LPCSTR fileName, char* serverPath );
static void _AdjustFindPath( char* findPath );
static void _AttributeConvertW2F( DWORD dwFileAttributes, u32 *fileAttributes );
static BOOL _FileTimeConvertW2F( CONST FILETIME *lpFileTime, FIOFileTime *fioFileTime );
static BOOL _FindDataConvertW2F( WIN32_FIND_DATA* winFind, FIOFindData* fioFind );
static HANDLE _CreateTempFile( void );
static BOOL	_WriteToTempFile( HANDLE hTmpFile, WIN32_FIND_DATA* findData );
static int	_GetTempFileSize( HANDLE hTmpFile );

static BOOL	_FIOOpenDir( char* dirName, HANDLE* dir );
static BOOL	_FIOCloseDir( HANDLE dir );
static int	_FIOGetDirSize( HANDLE dir );
static int	_FIOReadDir( HANDLE dir, FIOFindData* data, int numOfData );


#ifdef __cplusplus
extern "C" {
#endif

static void GetHFIOPacket(LPCVOID pPacket,DWORD* rr,DWORD *rs);
static DWORD HFIOSeek(DWORD Desc,DWORD Offset,DWORD Fromwhere);
static DWORD HFIOWrite(DWORD Desc,LPCVOID pBuffer,DWORD Length);
static DWORD HFIORead(DWORD Desc,LPVOID pBuffer,DWORD Length);
static HANDLE HFIOOpen(LPCTSTR lpFileName,DWORD Option);
static INT HFIOClose(DWORD Desc);
static INT HFIOGetLastError();

static DWORD HFIOFlush(DWORD Desc);
static DWORD HFIOStat(DWORD Desc,FIOStat* stat);
static DWORD HFIOError(DWORD Desc);

static DWORD HFIOFindFirst(char* filename,FIOFindData* findData);
static DWORD HFIOFindNext(FIOFindData* findData);

#ifdef __cplusplus
}
#endif

/*===================================*
	Old Style API - Open & Close
 *===================================*/
#include <list>
#include <string>
using namespace std;
typedef struct _HFIO_FILE_INFO {
    DWORD Id;		// 	Sequence number when opening.
    HANDLE Handle;
    string Name;
    _HFIO_FILE_INFO(){;}
    _HFIO_FILE_INFO(const _HFIO_FILE_INFO& a)
	{
	    Id = a.Id;
	    Handle = a.Handle;
	    Name = a.Name;
	}
    bool operator==(const _HFIO_FILE_INFO& a)
	{
	    return a.Id == Id;
	}
} HFIO_FILE_INFO;
typedef list<HFIO_FILE_INFO> HFIOList;
HFIOList s_HfioList;


// ================================================
//	APIs
// ================================================

// Starts FIO server.
// ====================
FIO_SERVER_API BOOL FIOServerStart( MCCChannel chID, u8 blockSize, char* rootPath )
{	OSReport("Start FIO server.");
	//	Opens MCC channel.
	if(!MCCOpen( chID, blockSize, fioServerChannelEvent )){
		//	Channel open error.
		gLastErr = FIO_ERR_MCC_LEVEL;
	}else{
		DWORD lpThreadId;
		//	Masks unnecessary events.
		MCCSetChannelEventMask( chID, MCC_EVENT_READ_DONE|MCC_EVENT_READ_DONE_INSIDE|MCC_EVENT_WRITE_DONE|MCC_EVENT_WRITE_DONE_INSIDE );
		//	Initializes internal information.
		gChID			= chID;
		gSizeOfBlocks	= blockSize;
		gLastErr		= FIO_ERR_NOERR;
		gRequestSequenceNumber = 0;
		gResultBuf		= (char*)malloc( FIO_BUFFER_SIZE*gSizeOfBlocks );
		gThreadBreak	= FALSE;
		//	Sets server root path.
		if( rootPath ){
			_SetServerRootPath( rootPath );
		}else{
			_InitializeServerRootPath();
		}
//		if(rootPath)
//			fioSetRootPath( rootPath );

		// 	Starts server thread.
		hThreadFioServer = CreateThread( 
			NULL,0,fioServerThread,NULL,0,&lpThreadId );
		if(hThreadFioServer == INVALID_HANDLE_VALUE){
			//	== Failed to create thread. ==
			FIOServerStop();
		}else{
			OSReport("READY.\n");
			return TRUE;
		}
	}
	OSReport("NG.\n");
	return FALSE;
}//FIXED

// Stops FIO server.
// ====================
FIO_SERVER_API void FIOServerStop( void )
{
    HFIOList::iterator it;
 
	OSReport("Halt FIO server.\n");

    for(it=s_HfioList.begin(); it != s_HfioList.end(); it=s_HfioList.begin()){
        CloseHandle((*it).Handle);
        s_HfioList.remove(*it);
    }

	//	Closes MCC channel.
	MCCClose( gChID );
	//	Stops server thread and waits for its termination.
	if( hThreadFioServer != INVALID_HANDLE_VALUE ){
		gThreadBreak=TRUE;
		WaitForSingleObject( hThreadFioServer,INFINITE );
	}
	//	Resets internal information.
	gChID			= (MCCChannel)0;
	gSizeOfBlocks	= 1;
	gLastErr		= FIO_ERR_NOERR;
	gRequestSequenceNumber = 0;
	if(gResultBuf)
		free(gResultBuf),gResultBuf=NULL;
	gThreadBreak	= FALSE;
	OSReport("Halt FIO server.DONE\n");
}//FIXED


// ================================================
//	EVENTs
// ================================================

volatile static BOOL gHostBusy=FALSE;
volatile static BOOL gReadDone,gWriteDone;
static void WINAPI fioServerChannelEvent( MCCChannel chID, MCCEvent event, u32 value )
{	gChID = chID;

	switch(event){
	case MCC_EVENT_USER:{
		u32	notify = value & FIO_MASK_NOTIFY;
		switch(notify){
		case FIO_NOTIFY_QUERY:{//host
			if((value&0x0000FFFF)==FIO_PROTOCOL_ID){
				if(!MCCNotify(chID, FIO_NOTIFY_QUERY_R | FIO_PROTOCOL_ID)){
					// notify error
				}else{
				}
			}
		} break;
		case FIO_NOTIFY_DONE:{
			while(gHostBusy){
				gHostBusy=gHostBusy;
				OSReport("server busy.\n");
				Sleep(1);
			};
			gHostBusy=TRUE;
		} break;
		}//switch(notify)
	} break;
	case MCC_EVENT_READ_DONE:{
		gReadDone=TRUE;
	} break;
	case MCC_EVENT_WRITE_DONE:{
		gWriteDone=TRUE;
	} break;
	}//switch(event)
}//FIXED


// ================================================
//	THREADs
// ================================================

DWORD WINAPI fioServerThread( LPVOID param )
{	gThreadBreak=FALSE;
	while(!gThreadBreak){
		//	packet received
		if(gHostBusy){
			//	Call receive packet proc.
			fioProcReceivePacket();
			// procDone
			gHostBusy=FALSE;
		}
		Sleep(1);
	}
	return 0;
}


// ================================================
//	HOST SIDE FUNCTIONS
// ================================================

#define FIO_FIRST_READ_DATA_SIZE 128
static void fioProcReceivePacket( void )
{
	//	 Allocates working memory.
	char* pPacketData = (char*)malloc(FIO_FIRST_READ_DATA_SIZE);
	if(!pPacketData){
		// 	== Insufficient memory ==
	}else{
		// 	Reads received packet.
		if(!MCCRead( gChID,0,pPacketData,FIO_FIRST_READ_DATA_SIZE,MCC_SYNC )){
			// 	== Failed to read packet. ==
		}else{
			DPCIHeader	*hdrDpci = (DPCIHeader *)(pPacketData);
			u32			nPacketSize = 0;
			nPacketSize = EndianConvert32( hdrDpci->length );
			nPacketSize = ((nPacketSize+31)/32)*32;
			nPacketSize = (nPacketSize < (u32)(FIO_BUFFER_SIZE*gSizeOfBlocks))?
							nPacketSize : (u32)(FIO_BUFFER_SIZE*gSizeOfBlocks);
			// 	Prepares packet data.
			if(nPacketSize < FIO_FIRST_READ_DATA_SIZE){
			}else{
				OSReport("::: PACKET_RELOADING\n");
				// 	Rereads packet data.
				if(pPacketData) free(pPacketData),pPacketData=NULL;
				pPacketData = (char*)malloc(nPacketSize);
				if(!pPacketData){
					// 	== Insufficient memory ==
				}else{
					// 	Reads packet data.
					if(!MCCRead( gChID,0,pPacketData,nPacketSize,MCC_SYNC )){
					}else{
					}
				}
			}
			{	// 	Processes packet data.
				DPCIHeader	*hdrDpci = (DPCIHeader *)(pPacketData);
				FIOHeader	*hdrFio = (FIOHeader*)((DPCIHeader *)(hdrDpci + 1));
				char		*format	= (char *)((FIOHeader *)(hdrFio + 1));

				// 	Converts endian.
				hdrDpci->length		= EndianConvert32( hdrDpci->length );
				hdrDpci->protocol	= EndianConvert16( hdrDpci->protocol );
				hdrFio->code		= EndianConvert32( hdrFio->code );
				hdrFio->number		= EndianConvert32( hdrFio->number );
				gRequestSequenceNumber = hdrFio->number-1;

				// 	Checks  header contents.
				if(hdrDpci->protocol != FIO_PROTOCOL_ID){
				}else{
					switch( hdrFio->code ){
					case FIO_CODE_OPEN:		//	OPEN proc.
						fioProcOpen( (FIOFormatCodeOpen*)format );		break;
					case FIO_CODE_CLOSE:	//	CLOSE proc.
						fioProcClose( (FIOFormatCodeClose*)format );	break;
					case FIO_CODE_READ:		//	READ proc.
						fioProcRead( (FIOFormatCodeRead*)format );		break;
					case FIO_CODE_WRITE:	//	WRITE proc.
						fioProcWrite( (FIOFormatCodeWrite*)format );	break;
					case FIO_CODE_SEEK:		//	SEEK proc.
						fioProcSeek( (FIOFormatCodeSeek*)format );		break;

					case FIO_CODE_FLUSH:	// FLUSH proc.
						fioProcFlush( (FIOFormatCodeFlush*)format );	break;
					case FIO_CODE_STAT:		// STAT proc.
						fioProcStat( (FIOFormatCodeStat*)format );		break;
					case FIO_CODE_ERROR:	// ERROR proc.
						fioProcError( (FIOFormatCodeError*)format );	break;

					case FIO_CODE_FFIRST:	// FFIRST proc.
						fioProcFFirst( (FIOFormatCodeFFirst*)format );	break;
					case FIO_CODE_FNEXT:	// FNEXT proc.
						fioProcFNext( (FIOFormatCodeFNext*)format );	break;


					case FIO_CODE_OPENDIR:
						fioProcOpenDir( (FIOFormatCodeOpenDir*)format );	break;
					case FIO_CODE_CLOSEDIR:
						fioProcCloseDir( (FIOFormatCodeCloseDir*)format );	break;
					case FIO_CODE_DIRSIZE:
						fioProcGetDirSize( (FIOFormatCodeGetDirSize*)format );	break;
					case FIO_CODE_READDIR:
						fioProcReadDir( (FIOFormatCodeReadDir*)format );	break;


					default:
						break;
					}
				}
				{	// 	Returns response packet.
					DPCIHeader	*hdrDpci = (DPCIHeader *)gResultBuf;
					u32			resultSize = EndianConvert32( hdrDpci->length );
					resultSize = ((resultSize+31)/32)*32;
					resultSize = (resultSize<(u32)(FIO_BUFFER_SIZE*gSizeOfBlocks))?
									resultSize : (u32)(FIO_BUFFER_SIZE*gSizeOfBlocks);

					if(!MCCWrite( gChID,0,gResultBuf,resultSize,MCC_SYNC )){
						OSReport("Result write.NG(Ch:%d)\n",gChID);
					}else{
						// 	Notifies the opponent that the writing to response packet is completed.
						if(!MCCNotify( gChID,FIO_NOTIFY_DONE )){
							OSReport("MCCNotify.NG(Ch:%d)\n",gChID);
						}
					}
				}
			}

		}
		// 	Releases working memory.
		if(pPacketData) free(pPacketData),pPacketData=NULL;
	}
}


// ================================================
//	Packet processing
// ================================================

//	Creates packet header that corresponds to the request packet.
// ====================
//	fioCode : FIO request code (FIO_CODE_*)
//	dataSize : ((AllPacket size)-(DPCIHeader size)-(FIOHeader size))
//	bEndianConvert : Sets TRUE when the argument is used in host.
static void* fioPacketMakeHeader( u32 fioCode, u32 dataSize, BOOL bEndianConvert )
{
	DPCIHeader *hdrDpci = (DPCIHeader *)(gResultBuf);
	FIOHeader *hdrFio   = (FIOHeader *)((DPCIHeader*)(hdrDpci + 1));
	char *data          = (char *)((FIOHeader*)(hdrFio + 1));

	// 	Increments the number of packet sequence.
	gRequestSequenceNumber++;
	// 	Creates FIO header.
	hdrFio->code	= fioCode;
	hdrFio->number	= gRequestSequenceNumber;
	// 	Creates DPCI header.
	hdrDpci->length		= sizeof(DPCIHeader)+sizeof(FIOHeader)+dataSize;
	hdrDpci->rsvd		= 0;
	hdrDpci->protocol	= FIO_PROTOCOL_ID;

	// 	Converts endian.
	if( bEndianConvert ){
		// 	For FIO header
		hdrFio->code	= EndianConvert32( hdrFio->code );
		hdrFio->number	= EndianConvert32( hdrFio->number );
		// 	For DPCI header
		hdrDpci->length		= EndianConvert32( hdrDpci->length );
		hdrDpci->protocol	= EndianConvert16( hdrDpci->protocol );
	}
	return (void*)data;
}//FIXED


// ================================================
//	FIO COMMANDS
// ================================================
/*
static char gCurrentPath[MAX_PATH];

static void fioSetRootPath( LPCSTR rootPath )
{
	if( rootPath )
		strcpy( gRootPath, rootPath );
}

static void fioOpenRootPath()
{
	if( GetCurrentDirectory( MAX_PATH, gCurrentPath ) == 0 ){
	}else{
		if( !SetCurrentDirectory( gRootPath ) ){
		}else{
		}
	}
}

static void fioCloseRootPath()
{
	if( !SetCurrentDirectory( gCurrentPath ) ){
	}else{
	}
}

static void fioGetRealPath( LPCSTR fileName, char* realPath )
{
	if(!realPath ){
	}else{
		if(fileName[0]!='\\'){
			strcpy( realPath, fileName );
		}else{
			char tmp[MAX_PATH];
			strcpy( tmp, gRootPath );
			strcat( tmp, (gRootPath[strlen(gRootPath)-1]!='\\')?"\\":"" );
			strcat( tmp, &fileName[1] );
			strcpy( realPath, tmp );
		}
	}
}
*/


//	OPEN
// ====================
static void fioProcOpen( FIOFormatCodeOpen* formatOpen )
{
	formatOpen->flag = EndianConvert32( formatOpen->flag );
	OSReport("OPEN. Flag:%08X Str:%s\n",formatOpen->flag,&formatOpen->filename);

	{	// 	Creates OPENR response packet.
		char filePath[MAX_PATH];
		u32 descriptor,result;
		FIOFormatCodeOpenR* formatOpenR =
			(FIOFormatCodeOpenR*)fioPacketMakeHeader( 
				FIO_CODE_OPENR, 
				sizeof(DPCIHeader)+sizeof(FIOHeader)+sizeof(FIOFormatCodeOpenR),
				TRUE );
		// 	Opens host file.
//		fioOpenRootPath();
		_OpenRootPath();
		_GetServerRealPath( (const char *)&formatOpen->filename, filePath );
		//fioGetRealPath( (const char *)&formatOpen->filename, filePath );
		// 	Creates OPENR response packet.
		descriptor = (u32)HFIOOpen( filePath, formatOpen->flag );
		result = HFIOGetLastError();
//		fioCloseRootPath();
		_CloseRootPath();
		// 	Creates OPENR response packet.
		formatOpenR->descriptor	= EndianConvert32(descriptor);
		formatOpenR->result		= EndianConvert32(result);
	}
}

//	CLOSE
// ====================
static void fioProcClose( FIOFormatCodeClose* formatClose )
{
	formatClose->descriptor = EndianConvert32( formatClose->descriptor );
	OSReport("CLOSE. Descriptor:%d\n",formatClose->descriptor);

	{	// 	Creates CLOSER response packet.
		u32 result;
		FIOFormatCodeCloseR* formatCloseR =
			(FIOFormatCodeCloseR*)fioPacketMakeHeader( 
				FIO_CODE_CLOSER, 
				sizeof(DPCIHeader)+sizeof(FIOHeader)+sizeof(FIOFormatCodeCloseR),
				TRUE );
		// 	Closes host file.
		HFIOClose(formatClose->descriptor);
		result = HFIOGetLastError();
		// 	Creates CLOSER response packet.
		formatCloseR->result = EndianConvert32(result);
	}
}


//	READ
// ====================
static void fioProcRead( FIOFormatCodeRead* formatRead )
{	u32 result=0, nbytes=0;

	// 	Reads parameters from READ packet.
	// ==============================
	formatRead->descriptor = EndianConvert32( formatRead->descriptor );
	formatRead->nbytes = EndianConvert32( formatRead->nbytes );
	//OSReport("READ. Descriptor:%d, nBytes:%d\n",formatRead->descriptor,formatRead->nbytes);
	{	int fd = (int)formatRead->descriptor;
		int nSizeOfData = (int)formatRead->nbytes;

		// 	Allocates the working buffer.
		// ==============================
		char* readBuffer = (char*)malloc( (nSizeOfData+MCC_BLOCK_SIZE-1)/MCC_BLOCK_SIZE*MCC_BLOCK_SIZE );
		if(!readBuffer){
			//	ERROR: NO_MEMORY
		}else{
			BOOL		bResult=TRUE;
			MCCChannel	nChID = gChID;
			u8			nChannelBlocks = gSizeOfBlocks;
			char*		readBufferAddress = readBuffer;
			// 	Reads file from host.
			// ==============================
			nbytes = HFIORead( fd, readBuffer, nSizeOfData );
			result = HFIOGetLastError();
			{
				MCCConnectState state;
				//BOOL			bNeedWaitDisconnect=FALSE;
				MCCEvent		oldMaskWrite=MCCSetChannelEventMask( gChID, 0 );

				u32	nPacketSize		= (MCC_BLOCK_SIZE * nChannelBlocks),
					nDataPacketSize	= (nSizeOfData / nPacketSize),
					nFraction		= (nSizeOfData - (nDataPacketSize * nPacketSize));
				nFraction = (nFraction + 32 - 1)/32*32;

				// ===============
				if( nFraction ){
					gReadDone = FALSE;
					if(!MCCWrite( nChID, 0, readBuffer, nFraction, MCC_SYNC )){
					}else{
						readBufferAddress += nFraction;
					}
				}
				// Restore eventmask
				MCCSetChannelEventMask( gChID, oldMaskWrite );

				////////////////////////////////////////////////////////////
				//	Processing for stream.
				if( nDataPacketSize ){
					MCCEvent oldMaskWrite=MCCSetChannelEventMask( gChID, 0 );
					MCCSetChannelEventMask( gChID, oldMaskWrite );
					//	For stream transfer
					while( TRUE ){
						MCCGetConnectionStatus( nChID, &state );
						if( state == MCC_CONNECT_CONNECTED ){
						}else{
							//	Close
							if(!MCCClose(nChID)){
								OSReport("MCCStream:MCCClose.NG\n");
								bResult=FALSE;
							}else{
								// Close
								while( TRUE ){
									MCCGetConnectionStatus( nChID, &state );
									if( state == MCC_CONNECT_CONNECTED ){
									}else{
										//	Stream OPEN
										if(!MCCStreamOpen( nChID, nChannelBlocks )){
											OSReport("MCCStream:MCCStreamOpen.NG\n");
											bResult=FALSE;
										}else{
											//	Stream WRITE
											if(!MCCStreamWrite( nChID, readBufferAddress, (nDataPacketSize * nChannelBlocks) )){
												OSReport("MCCStream:MCCStreamWrite.NG\n");
												bResult=FALSE;
											}else{}
											//	Stream CLOSE
											{	MCCConnectState state;
												while( TRUE ){
//OutputDebugString("while-2.\n");
													MCCGetConnectionStatus( nChID,&state );
													if( state == MCC_CONNECT_CONNECTED ){
													}else{
														if(!MCCStreamClose( nChID )){
															OSReport("MCCStream:MCCStreamClose.NG\n");
															bResult=FALSE;
														}else{
															while( TRUE ){
//OutputDebugString("while-3.\n");
																MCCGetConnectionStatus( nChID,&state );
																if( state == MCC_CONNECT_DISCONNECT ){
																}else{

																	if(!MCCOpen( nChID, nChannelBlocks, fioServerChannelEvent )){
																		OSReport("MCCStream:MCCOpen.NG\n");
																		bResult=FALSE;
																	}else{
																	}
																	break;
																}
															}
														}
														break;
													}
												}
											};
											//	Stream CLOSE
										}
										//	Stream OPEN
										break;
									}
									Sleep(1);
								};
							}
							break;
						}
						Sleep(1);
					};//while
					// Restore mask
					MCCSetChannelEventMask( gChID, oldMaskWrite );
				}
				//	Stream
				////////////////////////////////////////////////////////////

				if(!bResult)
					result = FIO_ERR_PACKET_WRITE;
			}

			// 	Releases working memory.
			free( readBuffer );
		}
	}

	// 	Creates READR response packet.
	// ==============================
	{	FIOFormatCodeReadR* formatReadR =
			(FIOFormatCodeReadR*)fioPacketMakeHeader( 
				FIO_CODE_READR,sizeof(DPCIHeader)+sizeof(FIOHeader)+sizeof(u32)*2,TRUE );
		//	format READR
		OSReport("::: READR.(Result:%d,Size:%d).\n",result,nbytes);
		formatReadR->result = EndianConvert32(result);
		formatReadR->nbytes = EndianConvert32(nbytes);
	}
}

//	WRITE
// ====================
static void fioProcWrite( FIOFormatCodeWrite* formatWrite )
{	u32 result=0, nbytes=0;

	// 	Reads parameters from WRITE packet.
	// ==============================
	formatWrite->descriptor = EndianConvert32( formatWrite->descriptor );
	formatWrite->nbytes = EndianConvert32( formatWrite->nbytes );
	//OSReport("WRITE. Descriptor:%d, nBytes:%d\n",formatWrite->descriptor,formatWrite->nbytes);
	{	int fd = (int)formatWrite->descriptor;
		int nSizeOfData = (int)formatWrite->nbytes;

		// 	Allocates the working buffer.
		// ==============================
		char* writeBuffer = (char*)malloc( (nSizeOfData + MCC_BLOCK_SIZE-1) / MCC_BLOCK_SIZE * MCC_BLOCK_SIZE );
		if(!writeBuffer){
			//	ERROR: NO_MEMORY
		}else{
			BOOL		bResult	=TRUE;
			MCCChannel	nChID	= gChID;
			u8			nChannelBlocks = gSizeOfBlocks;
			{	MCCConnectState state;
				BOOL			bNeedWaitDisconnect=FALSE;
				MCCEvent oldMaskWrite=MCCSetChannelEventMask( gChID, 0 );
				MCCSetChannelEventMask( gChID, oldMaskWrite );

				////////////////////////////////////////////////////////////
				//	Stream
				while( TRUE ){
					MCCGetConnectionStatus( nChID, &state );
					if( state == MCC_CONNECT_CONNECTED ){
					}else{
						//	Close
						//					Close
						//	Wait
						if(!MCCClose(nChID)){
							OSReport("MCCStream:MCCClose.NG\n");
							bResult=FALSE;
						}else{
							//		*** Closed ***
							//					Wait
							while( TRUE ){
								MCCGetConnectionStatus( nChID, &state );
								if( state == MCC_CONNECT_CONNECTED ){
								}else{
									//	Stream open
									//					Stream open
									//	Wait

									//	================
									//	Stream OPEN
									if(!MCCStreamOpen( nChID, nChannelBlocks )){
										OSReport("MCCStream:MCCStreamOpen.NG\n");
										bResult=FALSE;
									}else{
										//		*** Closed ***
										//	ReadOpen		WriteOpen
										u32 nDataBlockSize = (nSizeOfData+MCC_BLOCK_SIZE-1)/MCC_BLOCK_SIZE;
										//	Stream READ
										if(MCCStreamRead( nChID, writeBuffer ) != nDataBlockSize){
											OSReport("MCCStream:MCCStreamWrite.NG\n");
											bResult=FALSE;
										}else{
											// 	Writes data to host file.
											nbytes = HFIOWrite( fd,writeBuffer,nSizeOfData );
											if((u32)nSizeOfData!=nbytes)
													result = HFIOGetLastError();
											else	result = 0;
										}

										//	Stream CLOSE
										//	================
										{	MCCConnectState state;
											//	(T)Wait for close
											//				(H)close
											if(!MCCStreamClose( nChID )){
												OSReport("MCCStream:MCCStreamClose.NG\n");
												bResult=FALSE;
											}else{
												//				(H)wait for close
												while( TRUE ){
													MCCGetConnectionStatus( nChID,&state );
													if( state != MCC_CONNECT_DISCONNECT ){
													}else{
														//	(T)close
														//	**** Closed ****
														//	(T)Wait for open
														//				(H)open
														if(!MCCOpen( nChID, nChannelBlocks, fioServerChannelEvent )){
															OSReport("MCCStream:MCCOpen.NG\n");
															bResult=FALSE;
														}else{
															//				(H)Wait for open
															while( TRUE ){
																MCCGetConnectionStatus( nChID,&state );
																if( state != MCC_CONNECT_CONNECTED ){
																}else{
																	//	(T)open
																	break;
																}
															}									
														}
														break;
													}
												}
											}
										}//	Stream CLOSE

									}//if
									break;
								}//if
								Sleep(1);
							};//while
						}//if
						break;
					}//if
					Sleep(1);
				};//while

				// Restore mask
				MCCSetChannelEventMask( gChID, oldMaskWrite );

				//	Stream
				////////////////////////////////////////////////////////////
				if(!bResult)
					result = FIO_ERR_PACKET_WRITE;
			}

			// 	Releases working memory.
			// ==============================
			free( writeBuffer );
		}
	}

	// Creates WRITER response packet.
	// ==============================
	{	FIOFormatCodeWriteR* formatWriteR =
			(FIOFormatCodeWriteR*)fioPacketMakeHeader( 
				FIO_CODE_WRITER, 
				sizeof(DPCIHeader)+sizeof(FIOHeader)+sizeof(FIOFormatCodeWriteR), 
				TRUE );
		//	format WRITER
		formatWriteR->result = EndianConvert32(result);
		formatWriteR->nbytes = EndianConvert32(nbytes);
	}
}


//	SEEK
// ====================
static void fioProcSeek( FIOFormatCodeSeek*	formatSeek )
{
	formatSeek->descriptor = EndianConvert32( formatSeek->descriptor );
	formatSeek->offset = EndianConvert32( formatSeek->offset );
	formatSeek->base = EndianConvert32( formatSeek->base );
	OSReport("SEEK. Descriptor:%d, Offset:%d, Base:%d\n",formatSeek->descriptor,formatSeek->offset,formatSeek->base);

	{	// 	Creates SEEKR response packet.
		u32	pos,result;
		FIOFormatCodeSeekR* formatSeekR =
			(FIOFormatCodeSeekR*)fioPacketMakeHeader( 
				FIO_CODE_SEEKR, sizeof(DPCIHeader)+sizeof(FIOHeader)+sizeof(FIOFormatCodeSeekR),
				TRUE );
		// 	Seeks host file.
		pos = HFIOSeek(formatSeek->descriptor,formatSeek->offset,formatSeek->base);
		if(pos==0xFFFFFFFF){
			pos=0,result = FIO_ERR_EBADF;
		}else{
			result = 0;
		}
		// 	Creates SEEKR response packet.
		formatSeekR->pos = EndianConvert32(pos);
		formatSeekR->result = EndianConvert32(result);
	}
}

//	FLUSH
// ====================
static void fioProcFlush( FIOFormatCodeFlush*	formatSeek )
{
	formatSeek->descriptor = EndianConvert32( formatSeek->descriptor );
	OSReport("FLUSH. Descriptor:%d\n",formatSeek->descriptor);

	{	// 	Creates FLUSHR response packet.
		u32	result;
		FIOFormatCodeFlushR* formatFlushR =
			(FIOFormatCodeFlushR*)fioPacketMakeHeader( 
				FIO_CODE_FLUSHR, sizeof(DPCIHeader)+sizeof(FIOHeader)+sizeof(FIOFormatCodeFlushR),
				TRUE );
		// 	Flushes host file.
		if(HFIOFlush(formatSeek->descriptor)){
			result = HFIOGetLastError();
		}else{
			result = 0;
		}
		// 	Creates SEEKR response packet.
		formatFlushR->result = EndianConvert32(result);
	}
}

//	STAT
// ====================
static void fioProcStat( FIOFormatCodeStat* formatStat )
{
	formatStat->descriptor = EndianConvert32( formatStat->descriptor );
	OSReport("STAT. Descriptor:%d\n",formatStat->descriptor);

	{	// 	Creates STATR response packet.
		FIOStat stat;
		u32	result;
		FIOFormatCodeStatR* formatStatR =
			(FIOFormatCodeStatR*)fioPacketMakeHeader( 
				FIO_CODE_STATR, 
				sizeof(DPCIHeader)+sizeof(FIOHeader)+sizeof(FIOFormatCodeStatR),
				TRUE );
		// 	Obtains host file information.
		memset(&stat,0,sizeof(FIOStat));
		if(HFIOStat( formatStat->descriptor, &stat )){
			result = HFIOGetLastError();
		}else{
			//	ENDIAN CONVERT
			/*
			stat.fileAttributes	= EndianConvert32( stat.fileAttributes );
			stat.fileSizeHigh	= EndianConvert32( stat.fileSizeHigh );
			stat.fileSizeLow	= EndianConvert32( stat.fileSizeLow );
			stat.creationTime.date.year		= EndianConvert16( stat.creationTime.date.year );
			stat.lastAccessTime.date.year	= EndianConvert16( stat.lastAccessTime.date.year );
			stat.lastWriteTime.date.year	= EndianConvert16( stat.lastWriteTime.date.year );
			*/
			result = 0;
		}
		// 	Creates SEEKR response packet.
		formatStatR->stat	= stat;
		formatStatR->result = EndianConvert32(result);
	}
}

//	ERROR
// ====================
static void fioProcError( FIOFormatCodeError*	formatError )
{
	formatError->descriptor = EndianConvert32( formatError->descriptor );
	OSReport("ERROR. Descriptor:%d\n",formatError->descriptor);

	{	// 	Creates ERRORR response packet. 
		u32	result;
		FIOFormatCodeErrorR* formatErrorR =
			(FIOFormatCodeErrorR*)fioPacketMakeHeader( 
				FIO_CODE_ERRORR, 
				sizeof(DPCIHeader)+sizeof(FIOHeader)+sizeof(FIOFormatCodeErrorR),
				TRUE );
		// 	Obtains host file error.
		result = HFIOError(formatError->descriptor);
		// 	Creates ERRORR response packet.
		formatErrorR->result = EndianConvert32(result);
	}
}

//	FIND_FIRST
// ====================
static void fioProcFFirst( FIOFormatCodeFFirst* formatFFirst )
{
	OSReport("FFIRST. Filename:%s\n",&formatFFirst->filename);
//	fioOpenRootPath();
	{	// 	Creates FFIRSTR response packet.
		char filePath[MAX_PATH];
		FIOFindData findData;
		u32	result;
		FIOFormatCodeFFirstR* formatFFirstR =
			(FIOFormatCodeFFirstR*)fioPacketMakeHeader( 
				FIO_CODE_FFIRSTR, 
				sizeof(DPCIHeader)+sizeof(FIOHeader)+sizeof(FIOFormatCodeFFirstR),
				TRUE );
		// 	Starts FindFirst in host.
		memset(&findData,0,sizeof(FIOFindData));
//		fioOpenRootPath();
		_OpenRootPath();
		if( _GetServerRealPath( &formatFFirst->filename, filePath ) ){
			if( HFIOFindFirst( filePath, &findData )==0xFFFFFFFF ){
				result = HFIOGetLastError();
				if(result==0)
					result=-1;//END_OF_FIND;
			}else{
				result = 0;
			}
		}else{
			result = FIO_ERR_UNKNOWN;
		}
		//fioGetRealPath( &formatFFirst->filename, filePath );
//		fioCloseRootPath();
		_CloseRootPath();
		// 	Creates FFIRSTR response packet.
		formatFFirstR->findData	= findData;
		formatFFirstR->result = EndianConvert32(result);
	}
//	fioCloseRootPath();
}

//	FIND_NEXT
// ====================
static void fioProcFNext( FIOFormatCodeFNext* formatFNext )
{	formatFNext=formatFNext;
	OSReport("FNEXT. ");

	{	// 	Creates FNEXT response packet.
		FIOFindData findData;
		u32	result;
		FIOFormatCodeFNextR* formatFNextR =
			(FIOFormatCodeFNextR*)fioPacketMakeHeader( 
				FIO_CODE_FNEXTR, 
				sizeof(DPCIHeader)+sizeof(FIOHeader)+sizeof(FIOFormatCodeFNextR),
				TRUE );
		// 	Starts FindNext in host.
		memset(&findData,0,sizeof(FIOFindData));
		if( HFIOFindNext(&findData)==0xFFFFFFFF ){
			result = HFIOGetLastError();
			if(result==0)
				result=-1;//END_OF_FIND;
		}else{
			result = 0;
			/*
			//	ENDIAN CONVERT
			findData.stat.fileAttributes	= EndianConvert32( findData.stat.fileAttributes );
			findData.stat.fileSizeHigh		= EndianConvert32( findData.stat.fileSizeHigh );
			findData.stat.fileSizeLow		= EndianConvert32( findData.stat.fileSizeLow );
			findData.stat.creationTime.date.year	= EndianConvert16( findData.stat.creationTime.date.year );
			findData.stat.lastAccessTime.date.year	= EndianConvert16( findData.stat.lastAccessTime.date.year );
			findData.stat.lastWriteTime.date.year	= EndianConvert16( findData.stat.lastWriteTime.time.hour );
			*/
		}
		// 	Creates FNEXTR response packet.
		formatFNextR->findData	= findData;
		formatFNextR->result = EndianConvert32(result);
	}
}


static void fioProcOpenDir( FIOFormatCodeOpenDir* format )
{
	// Create OpenDirR response packet.
	// --------------------------------
	FIOFormatCodeOpenDirR* formatR =
		(FIOFormatCodeOpenDirR*)fioPacketMakeHeader( 
			FIO_CODE_OPENDIRR, sizeof(DPCIHeader)+sizeof(FIOHeader)+sizeof(FIOFormatCodeOpenDirR), TRUE );
	
	// Creates OPENR response packet.
	// ------------------------------
	// Open Directory
	HANDLE dir = NULL;
	u32 descriptor,result;
//	fioOpenRootPath();
	_OpenRootPath();
	if(_FIOOpenDir( (char *)&format->filename, &dir )){
		descriptor = (u32)dir;	// OK.
	}else{
		descriptor = (u32)-1;	// ERROR.
	}//if(_FIOOpenDir( char* dirName, HANDLE* dir ))
//	fioCloseRootPath();
	_CloseRootPath();

	// Set result code.
	// ----------------
	result = HFIOGetLastError();
	formatR->descriptor	= EndianConvert32(descriptor);
	formatR->result		= EndianConvert32(result);
}

static void fioProcCloseDir( FIOFormatCodeCloseDir* format )
{
	// Create CloseDirR response packet.
	// --------------------------------
	FIOFormatCodeCloseDirR* formatR =
		(FIOFormatCodeCloseDirR*)fioPacketMakeHeader( 
			FIO_CODE_CLOSEDIRR, sizeof(DPCIHeader)+sizeof(FIOHeader)+sizeof(FIOFormatCodeCloseDirR), TRUE );
	
	// Creates CloseDirR response packet.
	// ------------------------------
	// Close Directory
	HANDLE dir = (HANDLE)EndianConvert32( format->descriptor );
	if(_FIOCloseDir( dir )){
	}else{
		// ERROR:
	}

	// Set result code.
	// ----------------
	u32 result = HFIOGetLastError();
	formatR->result = EndianConvert32(result);
}

static void fioProcGetDirSize( FIOFormatCodeGetDirSize* format )
{
	// Create DirSizeR response packet.
	// --------------------------------
	FIOFormatCodeGetDirSizeR* formatR =
		(FIOFormatCodeGetDirSizeR*)fioPacketMakeHeader( 
			FIO_CODE_DIRSIZER, sizeof(DPCIHeader)+sizeof(FIOHeader)+sizeof(FIOFormatCodeGetDirSizeR), TRUE );
	
	// Creates DirSizeR response packet.
	// ------------------------------
	// Get Size Directory
	HANDLE dir = (HANDLE)EndianConvert32( format->descriptor );
	int size = _FIOGetDirSize( dir );
	u32 result = HFIOGetLastError();

	// Set result code.
	// ----------------
	formatR->size	= EndianConvert32(size);
	formatR->result = EndianConvert32(result);
}

static void fioProcReadDir( FIOFormatCodeReadDir* format )
{
	// Create ReadDirR response packet.
	// --------------------------------
	FIOFormatCodeReadDirR* formatR =
		(FIOFormatCodeReadDirR*)fioPacketMakeHeader( 
			FIO_CODE_READDIRR, sizeof(DPCIHeader)+sizeof(FIOHeader)+sizeof(FIOFormatCodeReadDirR), TRUE );
	
	// Creates ReadDirR response packet.
	// ------------------------------
	int readSize;
	u32 result;
	// Get Size Directory
	HANDLE dir		= (HANDLE)EndianConvert32( format->descriptor );
	u32 size		= EndianConvert32( format->size );
	FIOFindData* data=NULL;
	data = new FIOFindData[size];
	if( data != NULL ){
		readSize = _FIOReadDir( dir, data, size );
	}else{
		// エラー
		readSize = -1;
	}	
	result = HFIOGetLastError();

	// Set result code.
	// ----------------
	formatR->readsize	= EndianConvert32(readSize);
	formatR->result = EndianConvert32(result);

	if(data) delete data,data=NULL;
}


/*===========================================================================*
	Tools for FIO functions
 *===========================================================================*/

/*===================================*
	Server root path managing
 *===================================*/
static char _gCurrentPath[MAX_PATH], _gRootPath[MAX_PATH];

void _InitializeServerRootPath()
{	::GetCurrentDirectory( MAX_PATH, _gRootPath );
}

void _SetServerRootPath( LPCSTR rootPath )
{
	if( rootPath ){
		char curDir[MAX_PATH],srvPath[MAX_PATH];
		::GetCurrentDirectory( MAX_PATH, curDir );
		::SetCurrentDirectory( rootPath );
		::GetCurrentDirectory( MAX_PATH, srvPath );
		::SetCurrentDirectory( curDir );
		strcpy( _gRootPath, srvPath );
	}
}

BOOL _OpenRootPath( void )
{
	if( ::GetCurrentDirectory( MAX_PATH, _gCurrentPath ) != 0 ){
		if( ::SetCurrentDirectory( _gRootPath ) ){
			return TRUE;
		}else{}
	}else{}
	return FALSE;
}

BOOL _CloseRootPath( void )
{	return ::SetCurrentDirectory( _gCurrentPath );
}

BOOL _CheckServerRootSecurity( LPCSTR checkPath )
{
	BOOL result = FALSE;
	char oldPath[MAX_PATH],tmpPath[MAX_PATH],drive[MAX_PATH],dir[MAX_PATH],*address;

	::GetCurrentDirectory( MAX_PATH, oldPath );
	::SetCurrentDirectory( _gRootPath );
	if( ::GetFullPathName(checkPath,MAX_PATH,tmpPath,&address) != 0 ){
		// パスがディレクトリでなければファイル名を削る
		DWORD attr = ::GetFileAttributes(tmpPath);
		if((!(attr & FILE_ATTRIBUTE_DIRECTORY))||(attr == 0xFFFFFFFF)){
			_splitpath(tmpPath,drive,dir,NULL,NULL);
			strcpy( tmpPath, drive );
			strcat( tmpPath, dir );
		}
		::SetCurrentDirectory( tmpPath );
		::GetCurrentDirectory( MAX_PATH, tmpPath );
		result = (strncmp( _gRootPath,tmpPath, strlen(_gRootPath) )==0);
	}else{
	}
	::SetCurrentDirectory( oldPath );
	return result;
}

BOOL _GetServerRealPath( LPCSTR fileName, char* serverPath )
{
	if( serverPath ){
		if(fileName[0]!='\\'){
			// 相対パス
			strcpy( serverPath, fileName );
			// ファイル名が空であれば
			if(strlen(fileName)==0)
				strcpy( serverPath, "*" );
		}else{
			// 絶対パス
			char tmp[MAX_PATH];
			strcpy( tmp, _gRootPath );
			strcat( tmp, (_gRootPath[strlen(_gRootPath)-1]!='\\')?"\\":"" );
			strcat( tmp, &fileName[1] );
			if(strlen(&fileName[1])==0)
				strcpy( tmp, "*" );
			strcpy( serverPath, tmp );
		}
		return _CheckServerRootSecurity( serverPath );
	}else{}
	return FALSE;
}

void _AdjustFindPath( char* findPath )
{
	int length = strlen(findPath);
	if(length > 0){
		if( findPath[length-1] == '\\' )
			strcat(findPath,"*");
	}else{
		strcat(findPath,"*");
	}
}

/*===================================*
	Data format convert for Win32 to FIO
 *===================================*/
void _AttributeConvertW2F( DWORD dwFileAttributes, u32 *fileAttributes )
{
	*fileAttributes = 0x00000000;
	if((dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)!=0x00000000)
		*fileAttributes |= FIO_ATTRIBUTE_DIRECTORY;
	if((dwFileAttributes & FILE_ATTRIBUTE_HIDDEN)!=0x00000000)
		*fileAttributes |= FIO_ATTRIBUTE_HIDDEN;
	if((dwFileAttributes & FILE_ATTRIBUTE_NORMAL)!=0x00000000)
		*fileAttributes |= FIO_ATTRIBUTE_NORMAL;
	if((dwFileAttributes & FILE_ATTRIBUTE_READONLY)!=0x00000000)
		*fileAttributes |= FIO_ATTRIBUTE_READONLY;
	if((dwFileAttributes & FILE_ATTRIBUTE_SYSTEM)!=0x00000000)
		*fileAttributes |= FIO_ATTRIBUTE_SYSTEM;
}

BOOL _FileTimeConvertW2F( CONST FILETIME *lpFileTime, FIOFileTime *fioFileTime )
{
	SYSTEMTIME	systemTime;
	if(FileTimeToSystemTime( lpFileTime, &systemTime )){
		fioFileTime->date.year	= (u16)systemTime.wYear;
		fioFileTime->date.month	= (u8)systemTime.wMonth;
		fioFileTime->date.day	= (u8)systemTime.wDay;
		fioFileTime->time.hour	= (u8)systemTime.wHour;
		fioFileTime->time.minute= (u8)systemTime.wMinute;
		fioFileTime->time.second= (u8)systemTime.wSecond;
	}else{
		return FALSE;
	}//FileTimeToSystemTime

	// Endian Convert
	fioFileTime->date.year = EndianConvert16( fioFileTime->date.year );

	return TRUE;
}

BOOL _FindDataConvertW2F( WIN32_FIND_DATA* winFind, FIOFindData* fioFind )
{
	// メモリクリア
	::ZeroMemory( fioFind, sizeof(FIOFindData) );

	// ファイル名
	strncpy( (char*)fioFind->filename, winFind->cFileName, FIO_MAX_PATH );
	// ファイル属性
	_AttributeConvertW2F( winFind->dwFileAttributes, &fioFind->stat.fileAttributes );
	fioFind->stat.fileAttributes = EndianConvert32( fioFind->stat.fileAttributes );
	// ファイルサイズ
	fioFind->stat.fileSizeHigh	= EndianConvert32( winFind->nFileSizeHigh );
	fioFind->stat.fileSizeLow	= EndianConvert32( winFind->nFileSizeLow );

	// ファイル時間
	BOOL result = TRUE;
	result &= _FileTimeConvertW2F( &winFind->ftCreationTime,	&fioFind->stat.creationTime );
	result &= _FileTimeConvertW2F( &winFind->ftLastAccessTime,	&fioFind->stat.lastAccessTime );
	result &= _FileTimeConvertW2F( &winFind->ftLastWriteTime,	&fioFind->stat.lastWriteTime );
	return result;
}

/*===================================*
	Temp file managiment for DIR Technology
 *===================================*/
HANDLE _CreateTempFile( void )
{
	HANDLE hFile = INVALID_HANDLE_VALUE;
	// 一時ファイルのパス
	char tmpPath[MAX_PATH];
	DWORD pathSize = ::GetTempPath( MAX_PATH, tmpPath );
	if(pathSize < MAX_PATH)	tmpPath[pathSize] = '\0';
	else					tmpPath[0] = '\0';
	// 一時ファイルの名前
	char tmpPathName[MAX_PATH];
	UINT tmpID = ::GetTempFileName( tmpPath, "fio", 0, tmpPathName );
	if(tmpID == 0){
	}else{
		//puts( tmpPath );
		//puts( tmpPathName );
		// 一時ファイルの作成
		hFile = ::CreateFile( 
			tmpPathName, GENERIC_READ|GENERIC_WRITE, FILE_SHARE_READ, 0, 
			OPEN_EXISTING, FILE_ATTRIBUTE_TEMPORARY|FILE_FLAG_DELETE_ON_CLOSE, 0 );
	}
	return hFile;
}

BOOL _WriteToTempFile( HANDLE hTmpFile, WIN32_FIND_DATA* findData )
{
	FIOFindData data;
	DWORD numWrite;
	//PrintFindData( findData );
	// WIN32 to FIO
	_FindDataConvertW2F( findData, &data );
	return ::WriteFile( hTmpFile, &data, sizeof(data), &numWrite, 0 );
}

int _GetTempFileSize( HANDLE hTmpFile )
{
	DWORD loSize,hiSize;
	loSize = ::GetFileSize( hTmpFile, &hiSize );
	int result = loSize/sizeof(FIOFindData);
	return result;
}


/*===========================================================================*
	FIO functions for WIN32
 *===========================================================================*/

/*===================================*
	Old Style API - Last Error
 *===================================*/
/*---------------------------------------------------------------------------*
  Name:         HFIOGetLastError

  Description:  FIO サーバ内で発生した直前のエラーコードを返す

  Arguments:    なし

  Returns:      エラーコード
 *---------------------------------------------------------------------------*/
static int HFIOLastErrorCode = FIO_ERR_NOERR;
INT HFIOGetLastError()
{    return HFIOLastErrorCode;
}


/*---------------------------------------------------------------------------*
  Name:         HFIOOpen

  Description:  ファイルを閉じる

  Arguments:    lpFileName		ファイルハンドル
				Option			オプション

  Returns:      成功したら ファイルハンドル, 失敗したら INVALID_HANDLE_VALUE.
 *---------------------------------------------------------------------------*/
HANDLE HFIOOpen( LPCTSTR lpFileName, DWORD Option )
{
    HANDLE Handle;
    HFIO_FILE_INFO fi;
    DWORD OpenFlag = 0,Creation = 0;

    if( Option & FIO_OPEN_RDONLY ){
		OpenFlag |= GENERIC_READ;
		Creation = OPEN_EXISTING;
	}
    if( Option & FIO_OPEN_WRONLY ){
		OpenFlag |= GENERIC_WRITE;
		Creation = OPEN_EXISTING;
	}
    if( Option & FIO_OPEN_CREAT )
	Creation = CREATE_ALWAYS;
    if( Option & FIO_OPEN_TRUNC ){
		Creation = TRUNCATE_EXISTING;
	}
    if( (Option & FIO_OPEN_EXCL) && (Option & FIO_OPEN_CREAT) ){
	// 	Generates an error if the specified file name already exists when creating a file.
	Creation = CREATE_NEW;
    }
    Handle = CreateFile(lpFileName,OpenFlag,0,NULL,Creation,0,NULL);
    if( Handle == INVALID_HANDLE_VALUE ){
	DWORD WinLastError = GetLastError();
	switch( WinLastError ){
	 case ERROR_FILE_EXISTS:
	     HFIOLastErrorCode = FIO_ERR_EEXIST; break;
	 default:
	     HFIOLastErrorCode = FIO_ERR_ENOENT; break;
	}
	return Handle;
    }
    HFIOLastErrorCode = FIO_ERR_NOERR;
    fi.Handle = Handle;
    fi.Name = lpFileName;
    fi.Id = DWORD(Handle);
    s_HfioList.push_back(fi);
    return Handle;
}

/*---------------------------------------------------------------------------*
  Name:         HFIOClose

  Description:  ファイルを閉じる

  Arguments:    Desc			ファイルハンドル

  Returns:      成功したら 0,失敗したら 1.
 *---------------------------------------------------------------------------*/
INT HFIOClose( DWORD Desc )
{
    HFIOList::iterator it;

    for(it = s_HfioList.begin();it != s_HfioList.end();it++){
	if( (*it).Id == Desc ){
	    CloseHandle((*it).Handle);
	    s_HfioList.remove(*it);
	    HFIOLastErrorCode = FIO_ERR_NOERR;
	    return 0;
	}
    }
    HFIOLastErrorCode = FIO_ERR_EBADF;
    return 1;
}


/*===================================*
	Old Style API - File Access
 *===================================*/
/*---------------------------------------------------------------------------*
  Name:         HFIORead

  Description:  ファイルのに書き込む

  Arguments:    Desc			ファイルハンドル
                pBuffer			バッファ
				Length			データ長

  Returns:      実際に書き込んだデータのサイズ
 *---------------------------------------------------------------------------*/
DWORD HFIORead( DWORD Desc,LPVOID pBuffer,DWORD Length )
{
    DWORD Result;
    if( ::ReadFile(HANDLE(Desc),pBuffer,Length,&Result,NULL) ){
		return Result;
    }
	HFIOLastErrorCode = FIO_ERR_EACCES;
    return 0;
}

/*---------------------------------------------------------------------------*
  Name:         HFIOWrite

  Description:  ファイルのに書き込む

  Arguments:    Desc			ファイルハンドル
                pBuffer			バッファ
				Length			データ長

  Returns:      実際に書き込んだデータのサイズ
 *---------------------------------------------------------------------------*/
DWORD HFIOWrite( DWORD Desc, LPCVOID pBuffer, DWORD Length )
{
    DWORD Result=0;
    if( ::WriteFile(HANDLE(Desc),pBuffer,Length,&Result,NULL) ){
		return Result;
    }
	HFIOLastErrorCode = FIO_ERR_EACCES;
    return 0;
}

/*---------------------------------------------------------------------------*
  Name:         HFIOSeek

  Description:  ファイルのポインタを移動する

  Arguments:    Desc			ファイルハンドル
                Offset			オフセット
				Fromwhere		開始位置

  Returns:      移動後のファイルポインタの位置
 *---------------------------------------------------------------------------*/
DWORD HFIOSeek( DWORD Desc, DWORD Offset, DWORD Fromwhere )
{
	DWORD NewPos;
	DWORD MoveMethod = 0;
	switch( Fromwhere ){
	case 0:
		MoveMethod = FILE_BEGIN;
		break;
	case 1:
		MoveMethod = FILE_CURRENT;
		break;
	case 2:
		MoveMethod = FILE_END;
		break;
	}
	NewPos = ::SetFilePointer(HANDLE(Desc),Offset,NULL,MoveMethod);
	return NewPos;
}

/*---------------------------------------------------------------------------*
  Name:         HFIOFlush

  Description:  指定したファイルをフラッシュする

  Arguments:    Desc			ファイルハンドル

  Returns:      ファイルのフラッシュに成功したら 0.そうでなければ -1.
 *---------------------------------------------------------------------------*/
DWORD HFIOFlush( DWORD Desc )
{
	if(::FlushFileBuffers( HANDLE(Desc) )){
		HFIOLastErrorCode = FIO_ERR_NOERR;
		return 0;
	}else{
		HFIOLastErrorCode = FIO_ERR_EBADF;
	}
	return -1;
}

/*---------------------------------------------------------------------------*
  Name:         HFIOStat

  Description:  FIO サーバ内で最後に発生したエラーコードを取得

  Arguments:    Desc			ファイルハンドル
                stat			ファイル情報

  Returns:      ファイル情報の取得に成功したら 0.そうでなければ -1.
 *---------------------------------------------------------------------------*/
DWORD HFIOStat( DWORD Desc, FIOStat* stat )
{
	BY_HANDLE_FILE_INFORMATION	fileInfo;

	//	read file status
	if( ::GetFileInformationByHandle( HANDLE(Desc), &fileInfo) ){
		memset( stat, 0, sizeof(FIOStat) );
		// ファイル属性
		_AttributeConvertW2F( fileInfo.dwFileAttributes, &stat->fileAttributes );
		stat->fileAttributes = EndianConvert32( stat->fileAttributes );
		// ファイルサイズ
		stat->fileSizeHigh	= EndianConvert32( fileInfo.nFileSizeHigh );
		stat->fileSizeLow	= EndianConvert32( fileInfo.nFileSizeLow );
		// ファイル時間
		BOOL result = TRUE;
		result &= _FileTimeConvertW2F( &fileInfo.ftCreationTime,	&stat->creationTime );
		result &= _FileTimeConvertW2F( &fileInfo.ftLastAccessTime,	&stat->lastAccessTime );
		result &= _FileTimeConvertW2F( &fileInfo.ftLastWriteTime,	&stat->lastWriteTime );

		HFIOLastErrorCode = FIO_ERR_NOERR;
		return 0;
	}else{
		HFIOLastErrorCode = FIO_ERR_EBADF;
	}
	return -1;
}

/*---------------------------------------------------------------------------*
  Name:         HFIOError

  Description:  FIO サーバ内で最後に発生したエラーコードを取得

  Arguments:    Desc			未使用

  Returns:      エラーコード
 *---------------------------------------------------------------------------*/
DWORD HFIOError(DWORD Desc)
{	return HFIOLastErrorCode;
}

static HANDLE gFindHandle=NULL;
/*---------------------------------------------------------------------------*
  Name:         HFIOFindFirst

  Description:  続きのファイル検索を行う

  Arguments:    filename		検索するファイルの条件
				findData        検索したファイルの情報

  Returns:      検索に成功したら 0.そうでなければ -1.
 *---------------------------------------------------------------------------*/
DWORD HFIOFindFirst( char* filename, FIOFindData* findData )
{
	WIN32_FIND_DATA fdata;

	// 検索ハンドルがすでに開いていたら閉じる
	if(gFindHandle)
		::FindClose(gFindHandle);
	// 最初に条件に合うファイルを検索する
	gFindHandle = ::FindFirstFile( filename,&fdata );
	if(gFindHandle != INVALID_HANDLE_VALUE ){
		// convert FindData type 
		HFIOLastErrorCode = FIO_ERR_NOERR;
		return (_FindDataConvertW2F( &fdata, findData )!=TRUE) ? -1 : 0;
	}else{
		gFindHandle = NULL;
		HFIOLastErrorCode = FIO_ERR_NOT_FOUND;
	}
	return -1;
}

/*---------------------------------------------------------------------------*
  Name:         HFIOFindNext

  Description:  続きのファイル検索を行う

  Arguments:    findData        検索したファイルの情報

  Returns:      検索に成功したら 0.そうでなければ -1.
 *---------------------------------------------------------------------------*/
DWORD HFIOFindNext( FIOFindData* findData )
{
	// Is handle open?
	if(gFindHandle){
		WIN32_FIND_DATA fdata;
		if(::FindNextFile( gFindHandle,&fdata )){
			// convert FindData type 
			HFIOLastErrorCode = FIO_ERR_NOERR;
			return (_FindDataConvertW2F( &fdata, findData )!=TRUE) ? -1 : 0;
		}else{
			HFIOLastErrorCode = FIO_ERR_NO_MORE;
		}
	}else{}
	return -1;
}


/*===================================*
	Find file in directory	(FIO DIR)
 *===================================*/
/*---------------------------------------------------------------------------*
  Name:         _FIOOpenDir

  Description:  ファイル検索条件を指定して検索情報を開く

  Arguments:    dirName         ファイル検索条件
                dir             検索情報のハンドル

  Returns:      検索情報を開くのに成功したら TRUE. そうでなければ FALSE.
 *---------------------------------------------------------------------------*/
BOOL _FIOOpenDir( char* dirName, HANDLE* dir )
{
	WIN32_FIND_DATA findData;

	// 一時ファイルを作成
	HANDLE hTmpFile = _CreateTempFile();
	if( hTmpFile != INVALID_HANDLE_VALUE ){
		char findPath[MAX_PATH];
		if(_GetServerRealPath( dirName, findPath )){
			// 検索パス名の調整
			_AdjustFindPath( findPath );
			//puts( findPath );

			// ファイルの検索
			HANDLE hFind = ::FindFirstFile( findPath, &findData );
			if( hFind != INVALID_HANDLE_VALUE ){
				// 検索したファイルの情報を書き出す
				_WriteToTempFile( hTmpFile, &findData );
				// 次を検索
				while( ::FindNextFile( hFind, &findData ) )
					_WriteToTempFile( hTmpFile, &findData );
				// 検索を閉じる
				*dir = hTmpFile;
				// 一覧情報ファイルのポインタを先頭へ
				::SetFilePointer( hTmpFile,0,NULL,FILE_BEGIN );
				HFIOLastErrorCode = FIO_ERR_NOERR;
				return ::FindClose( hFind );
			}else{
				//puts("ERROR: FindFirstFile.NG");
				HFIOLastErrorCode = FIO_ERR_NOT_FOUND;
			}//if( hFind != INVALID_HANDLE_VALUE )
		}else{
			//puts("ERROR: _GetServerRealPath.NG");
			HFIOLastErrorCode = FIO_ERR_UNKNOWN;
		}//_GetServerRealPath
	}else{
		//puts("ERROR: _FIOCreateTempFile.NG");
		HFIOLastErrorCode = FIO_ERR_UNKNOWN;
	}//if( hTmpFile != INVALID_HANDLE_VALUE )
	*dir = NULL;
	return FALSE;
}

/*---------------------------------------------------------------------------*
  Name:         _FIOCloseDir

  Description:  検索情報を閉じる

  Arguments:    dir          検索情報のハンドル

  Returns:      検索情報を閉じるのに成功したら TRUE. そうでなければ FALSE.
 *---------------------------------------------------------------------------*/
BOOL _FIOCloseDir( HANDLE dir )
{
	BOOL result = FALSE;
	HFIOLastErrorCode = FIO_ERR_UNKNOWN;
	__try{
		result = ::CloseHandle( dir );
		if( result ){
			HFIOLastErrorCode = FIO_ERR_NOERR;
		}else{
		}
	}
	__except( EXCEPTION_EXECUTE_HANDLER ){
	}
	return 	result;
}

/*---------------------------------------------------------------------------*
  Name:         _FIOGetDirSize

  Description:  検索情報に含まれる情報の数を取得する

  Arguments:    dir          検索情報のハンドル

  Returns:      検索情報に含まれる情報の数
 *---------------------------------------------------------------------------*/
int _FIOGetDirSize( HANDLE dir )
{	return _GetTempFileSize( dir );
}

/*---------------------------------------------------------------------------*
  Name:         _FIOReadDir

  Description:  検索情報ファイルを読む

  Arguments:    dir          検索情報のハンドル
                data         検索ファイル情報
                numOfData    読み込む検索情報の数    

  Returns:      実際に読むことの出来た検索情報の数
 *---------------------------------------------------------------------------*/
int _FIOReadDir( HANDLE dir, FIOFindData* data, int numOfData )
{
	DWORD readSize;
	HFIOLastErrorCode = FIO_ERR_UNKNOWN;
	DWORD filePos = ::SetFilePointer( dir,0,NULL,FILE_CURRENT );
	for(int i=0;i<numOfData; i++){
		if(::ReadFile( dir,&data[i],sizeof(*data),&readSize,0 )){
			if(readSize > 0){
				// ディレクトリ情報の個数を計算
				HFIOLastErrorCode = FIO_ERR_NOERR;
			}else{
				HFIOLastErrorCode = FIO_ERR_NO_MORE;
				break;
			}
		}else{
			break;
		}
	}
	// 一覧情報ファイルのポインタを元の位置に
	::SetFilePointer( dir,filePos,NULL,FILE_BEGIN );
	return i;
}


#if 0
/*
// ================================================
//	HOST API ACCESS INTERFACE(HFIO)
// ================================================

#include <windows.h>
#include <list>
#include <string>
#include <time.h>
#include <sys\types.h>
#include <sys\stat.h>

using namespace std;

typedef struct _HFIO_FILE_INFO {
    DWORD Id;		// 	Sequence number when opening.
    HANDLE Handle;
    string Name;
    _HFIO_FILE_INFO(){;}
    _HFIO_FILE_INFO(const _HFIO_FILE_INFO& a)
	{
	    Id = a.Id;
	    Handle = a.Handle;
	    Name = a.Name;
	}
    bool operator==(const _HFIO_FILE_INFO& a)
	{
	    return a.Id == Id;
	}
} HFIO_FILE_INFO;

typedef list<HFIO_FILE_INFO> HFIOList;

HFIOList s_HfioList;


static INT HFIOLastErrorCode = 0;
static HANDLE gFindHandle=NULL;

void GetHFIOPacket(LPCVOID pPacket,DWORD* rr,DWORD* rs)
{
    LPBYTE p = (LPBYTE)pPacket;
    *rr = MAKELONG(MAKEWORD(p[0],p[1]),MAKEWORD(p[2],p[3]));
    *rs = MAKELONG(MAKEWORD(p[4],p[5]),MAKEWORD(p[6],p[7]));
}

HANDLE HFIOOpen(LPCTSTR lpFileName,DWORD Option)
{
    HANDLE Handle;
    HFIO_FILE_INFO fi;
    DWORD OpenFlag = 0,Creation = 0;

    if( Option & FIO_OPEN_RDONLY ){
		OpenFlag |= GENERIC_READ;
		Creation = OPEN_EXISTING;
	}
    if( Option & FIO_OPEN_WRONLY ){
		OpenFlag |= GENERIC_WRITE;
		Creation = OPEN_EXISTING;
	}
    if( Option & FIO_OPEN_CREAT )
	Creation = CREATE_ALWAYS;
    if( Option & FIO_OPEN_TRUNC ){
		Creation = TRUNCATE_EXISTING;
	}
    if( (Option & FIO_OPEN_EXCL) && (Option & FIO_OPEN_CREAT) ){
	// 	Generates an error if the specified file name already exists when creating a file.
	Creation = CREATE_NEW;
    }
    Handle = CreateFile(lpFileName,OpenFlag,0,NULL,Creation,0,NULL);
    if( Handle == INVALID_HANDLE_VALUE ){
	DWORD WinLastError = GetLastError();
	switch( WinLastError ){
	 case ERROR_FILE_EXISTS:
	     HFIOLastErrorCode = FIO_ERR_EEXIST; break;
	 default:
	     HFIOLastErrorCode = FIO_ERR_ENOENT; break;
	}
	return Handle;
    }
    HFIOLastErrorCode = 0;
    fi.Handle = Handle;
    fi.Name = lpFileName;
    fi.Id = DWORD(Handle);
    s_HfioList.push_back(fi);
    return Handle;
}

INT HFIOClose(DWORD Desc)
{
    HFIOList::iterator it;

    for(it = s_HfioList.begin();it != s_HfioList.end();it++){
	if( (*it).Id == Desc ){
	    CloseHandle((*it).Handle);
	    s_HfioList.remove(*it);
	    HFIOLastErrorCode = 0;
	    return 0;
	}
    }
    HFIOLastErrorCode = FIO_ERR_EBADF;
    return 1;
}

DWORD HFIORead(DWORD Desc,LPVOID pBuffer,DWORD Length)
{
    DWORD Result;

    if( ReadFile(HANDLE(Desc),pBuffer,Length,&Result,NULL) ){
	return Result;
    }
	HFIOLastErrorCode = FIO_ERR_EACCES;
    return 0;
}

DWORD HFIOWrite(DWORD Desc,LPCVOID pBuffer,DWORD Length)
{
    DWORD Result=0;

    if( WriteFile(HANDLE(Desc),pBuffer,Length,&Result,NULL) ){
	return Result;
    }
	HFIOLastErrorCode = FIO_ERR_EACCES;
    return 0;
}

DWORD HFIOSeek(DWORD Desc,DWORD Offset,DWORD Fromwhere)
{
    DWORD NewPos;
    DWORD MoveMethod = 0;
    switch( Fromwhere ){
     case 0:
	 MoveMethod = FILE_BEGIN;
	 break;
     case 1:
	 MoveMethod = FILE_CURRENT;
	 break;
     case 2:
	 MoveMethod = FILE_END;
	 break;
    }
    NewPos = SetFilePointer(HANDLE(Desc),Offset,NULL,MoveMethod);
    return NewPos;
}

DWORD HFIOFlush(DWORD Desc)
{	if(!FlushFileBuffers(HANDLE(Desc))){
		HFIOLastErrorCode = FIO_ERR_EBADF;
	}else{
		HFIOLastErrorCode = 0;
		return 0;
	}
	return -1;
}

void hfioAttributesWin32ToFio(DWORD dwFileAttributes,u32 *fileAttributes )
{
	*fileAttributes = 0x00000000;
	if((dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)!=0x00000000)
		*fileAttributes |= FIO_ATTRIBUTE_DIRECTORY;
	if((dwFileAttributes & FILE_ATTRIBUTE_HIDDEN)!=0x00000000)
		*fileAttributes |= FIO_ATTRIBUTE_HIDDEN;
	if((dwFileAttributes & FILE_ATTRIBUTE_NORMAL)!=0x00000000)
		*fileAttributes |= FIO_ATTRIBUTE_NORMAL;
	if((dwFileAttributes & FILE_ATTRIBUTE_READONLY)!=0x00000000)
		*fileAttributes |= FIO_ATTRIBUTE_READONLY;
	if((dwFileAttributes & FILE_ATTRIBUTE_SYSTEM)!=0x00000000)
		*fileAttributes |= FIO_ATTRIBUTE_SYSTEM;
}

DWORD HFIOStat(DWORD Desc,FIOStat* stat)
{	BY_HANDLE_FILE_INFORMATION	fileInfo;

	//	read file status
	memset(stat,0,sizeof(FIOStat));
	if(!GetFileInformationByHandle(HANDLE(Desc),&fileInfo)){
		HFIOLastErrorCode = FIO_ERR_EBADF;
		return -1;
	}else{
		SYSTEMTIME	systemTime;

		//stat->fileAttributes = fileInfo.dwFileAttributes;
		hfioAttributesWin32ToFio( fileInfo.dwFileAttributes, &stat->fileAttributes );
		stat->fileSizeHigh = fileInfo.nFileSizeHigh;
		stat->fileSizeLow = fileInfo.nFileSizeLow;
		//
		if(!FileTimeToSystemTime(&fileInfo.ftCreationTime,&systemTime)){
			HFIOLastErrorCode = FIO_ERR_UNKNOWN;
			return -1;
		}else{
			stat->creationTime.date.year = systemTime.wYear;
			stat->creationTime.date.month = systemTime.wMonth;
			stat->creationTime.date.day = systemTime.wDay;
			stat->creationTime.time.hour = systemTime.wHour;
			stat->creationTime.time.minute = systemTime.wMinute;
			stat->creationTime.time.second = systemTime.wSecond;
		}
		//
		if(!FileTimeToSystemTime(&fileInfo.ftLastAccessTime,&systemTime)){
			HFIOLastErrorCode = FIO_ERR_UNKNOWN;
			return -1;
		}else{
			stat->lastAccessTime.date.year = systemTime.wYear;
			stat->lastAccessTime.date.month = systemTime.wMonth;
			stat->lastAccessTime.date.day = systemTime.wDay;
			stat->lastAccessTime.time.hour = systemTime.wHour;
			stat->lastAccessTime.time.minute = systemTime.wMinute;
			stat->lastAccessTime.time.second = systemTime.wSecond;
		}
		//
		if(!FileTimeToSystemTime(&fileInfo.ftLastWriteTime,&systemTime)){
			HFIOLastErrorCode = FIO_ERR_UNKNOWN;
			return -1;
		}else{
			stat->lastWriteTime.date.year = systemTime.wYear;
			stat->lastWriteTime.date.month = systemTime.wMonth;
			stat->lastWriteTime.date.day = systemTime.wDay;
			stat->lastWriteTime.time.hour = systemTime.wHour;
			stat->lastWriteTime.time.minute = systemTime.wMinute;
			stat->lastWriteTime.time.second = systemTime.wSecond;
		}
	}
	HFIOLastErrorCode = 0;
	return 0;
}

DWORD HFIOError(DWORD Desc)
{	return HFIOLastErrorCode;
}

DWORD HFIOFindFirst(char* filename,FIOFindData* findData)
{	WIN32_FIND_DATA fdata;

	if(gFindHandle)
		FindClose(gFindHandle);	
	gFindHandle = FindFirstFile( filename,&fdata );
	if(gFindHandle==INVALID_HANDLE_VALUE){
		gFindHandle=NULL;
		return -1;
	}else{
		SYSTEMTIME	systemTime;

		memset(findData,0,sizeof(FIOFindData));

		findData->stat.fileSizeHigh = fdata.nFileSizeHigh;
		findData->stat.fileSizeLow = fdata.nFileSizeLow;
		//findData->stat.fileAttributes = fdata.dwFileAttributes;
		hfioAttributesWin32ToFio( fdata.dwFileAttributes, &findData->stat.fileAttributes );
		//
		if(!FileTimeToSystemTime(&fdata.ftCreationTime,&systemTime)){
			HFIOLastErrorCode = FIO_ERR_UNKNOWN;
			return -1;
		}else{
			findData->stat.creationTime.date.year = systemTime.wYear;
			findData->stat.creationTime.date.month = systemTime.wMonth;
			findData->stat.creationTime.date.day = systemTime.wDay;
			findData->stat.creationTime.time.hour = systemTime.wHour;
			findData->stat.creationTime.time.minute = systemTime.wMinute;
			findData->stat.creationTime.time.second = systemTime.wSecond;
		}
		//
		if(!FileTimeToSystemTime(&fdata.ftLastAccessTime,&systemTime)){
			HFIOLastErrorCode = FIO_ERR_UNKNOWN;
			return -1;
		}else{
			findData->stat.lastAccessTime.date.year = systemTime.wYear;
			findData->stat.lastAccessTime.date.month = systemTime.wMonth;
			findData->stat.lastAccessTime.date.day = systemTime.wDay;
			findData->stat.lastAccessTime.time.hour = systemTime.wHour;
			findData->stat.lastAccessTime.time.minute = systemTime.wMinute;
			findData->stat.lastAccessTime.time.second = systemTime.wSecond;
		}
		//
		if(!FileTimeToSystemTime(&fdata.ftLastWriteTime,&systemTime)){
			HFIOLastErrorCode = FIO_ERR_UNKNOWN;
			return -1;
		}else{
			findData->stat.lastWriteTime.date.year = systemTime.wYear;
			findData->stat.lastWriteTime.date.month = systemTime.wMonth;
			findData->stat.lastWriteTime.date.day = systemTime.wDay;
			findData->stat.lastWriteTime.time.hour = systemTime.wHour;
			findData->stat.lastWriteTime.time.minute = systemTime.wMinute;
			findData->stat.lastWriteTime.time.second = systemTime.wSecond;
		}
		strncpy( (char*)findData->filename,fdata.cFileName,FIO_MAX_PATH );
	}
	return 0;
}

DWORD HFIOFindNext( FIOFindData* findData )
{
	if(!gFindHandle){
		return -1;
	}else{
		WIN32_FIND_DATA fdata;
		if(!FindNextFile( gFindHandle,&fdata )){
			return -1;
		}else{
			SYSTEMTIME	systemTime;

			memset(findData,0,sizeof(FIOFindData));

			//findData->stat.fileAttributes = fdata.dwFileAttributes;
			hfioAttributesWin32ToFio( fdata.dwFileAttributes, &findData->stat.fileAttributes );
			findData->stat.fileSizeHigh = fdata.nFileSizeHigh;
			findData->stat.fileSizeLow = fdata.nFileSizeLow;
			//
			if(!FileTimeToSystemTime(&fdata.ftCreationTime,&systemTime)){
				HFIOLastErrorCode = FIO_ERR_UNKNOWN;
				return -1;
			}else{
				findData->stat.creationTime.date.year = systemTime.wYear;
				findData->stat.creationTime.date.month = systemTime.wMonth;
				findData->stat.creationTime.date.day = systemTime.wDay;
				findData->stat.creationTime.time.hour = systemTime.wHour;
				findData->stat.creationTime.time.minute = systemTime.wMinute;
				findData->stat.creationTime.time.second = systemTime.wSecond;
			}
			//
			if(!FileTimeToSystemTime(&fdata.ftLastAccessTime,&systemTime)){
				HFIOLastErrorCode = FIO_ERR_UNKNOWN;
				return -1;
			}else{
				findData->stat.lastAccessTime.date.year = systemTime.wYear;
				findData->stat.lastAccessTime.date.month = systemTime.wMonth;
				findData->stat.lastAccessTime.date.day = systemTime.wDay;
				findData->stat.lastAccessTime.time.hour = systemTime.wHour;
				findData->stat.lastAccessTime.time.minute = systemTime.wMinute;
				findData->stat.lastAccessTime.time.second = systemTime.wSecond;
			}
			//
			if(!FileTimeToSystemTime(&fdata.ftLastWriteTime,&systemTime)){
				HFIOLastErrorCode = FIO_ERR_UNKNOWN;
				return -1;
			}else{
				findData->stat.lastWriteTime.date.year = systemTime.wYear;
				findData->stat.lastWriteTime.date.month = systemTime.wMonth;
				findData->stat.lastWriteTime.date.day = systemTime.wDay;
				findData->stat.lastWriteTime.time.hour = systemTime.wHour;
				findData->stat.lastWriteTime.time.minute = systemTime.wMinute;
				findData->stat.lastWriteTime.time.second = systemTime.wSecond;
			}
			strncpy( (char*)findData->filename,fdata.cFileName,FIO_MAX_PATH );
		}
	}
	return 0;
}
*/
#endif
