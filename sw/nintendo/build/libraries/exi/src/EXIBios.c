/*---------------------------------------------------------------------------*
  Project:  Dolphin Expansion Interface API
  File:     EXIBios.c

  Copyright 1998-2003 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: EXIBios.c,v $
  Revision 1.1.1.1  2004/01/03 01:02:28  paulm
  Gamecube library source from Nintendo

    
    52    3/10/03 13:54 Shiki
    Added support for EXI_BARNACLE_ENABLER.

    51    11/08/02 17:33 Shiki
    Fixed EXIGetTypeString() string for IS-DOL-VIEWER.

    50    11/07/02 11:37 Shiki
    Fixed EXIInit() to call OSRegisterVersion() after the initialization,
    etc.

    49    10/28/02 9:45 Shiki
    Modified EXIGetID() to disable interrupts while locking the EXI
    channel.

    48    10/25/02 16:35 Shiki
    Modified EXIGetID() to retrieve the EXI device ID for the serial port 1
    only once at EXIInit().

    47    10/25/02 14:34 Shiki
    Modified EXIGetTypeString() to use "Broadband Adapter" instead of
    "Ether".

    46    10/25/02 14:26 Shiki
    Added support for Memory Card 1019 and 2043.

    45    9/20/02 10:22 Shiki
    Added support for TDEV.

    44    8/08/02 12:41 Shiki
    Turned off the CW scheduling optimizer.

    43    8/07/02 21:45 Shiki
    Defined DOLPHIN_LIB_VERSION() and registered the library with
    OSRegisterVersion().

    42    6/11/02 12:49 Shiki
    Fixed not to mask TCINT when an EXI card is detached/removed from Slot
    A.

    41    3/26/02 9:14 Shiki
    Added support for USB-ADAPTER, IS-VIEWER and Artist Ether with
    NPDP-Reader.

    40    11/22/01 16:45 Shiki
    Moved from OS.

    38    10/18/01 13:46 Shiki
    1. Added EXIGetType().
    2. Modified EXIProbe() to make it detect device it.
    3. Modified interrupt handlers to make it create an exception context
    before invoking callback functions so that callbacks can touch FPU.

    37    01/06/29 19:57 Shiki
    Fixed EXIDeselect() to call EXIProbe() for both slot A and B.

    36    01/06/25 14:27 Shiki
    Modified EXISync() for HIO check.

    35    01/04/27 16:33 Shiki
    Fixed EXI_PROBE_DELAY handling code.

    34    01/03/28 14:54 Shiki
    Revised EXIGetID().

    33    01/03/28 13:28 Shiki
    Fixed EXIGetID() to support dev 1 and 2.

    32    01/03/26 15:54 Shiki
    Fixed EXI_PROBE_DELAY.

    31    01/03/26 15:33 Shiki
    Fixed EXIProbe() and EXIAttach() to check exi state correctly.

    30    01/03/05 10:35 Shiki
    Modified EXIGetID() to support EXI2 too.

    29    01/01/25 10:12 Shiki
    Added EXIGetState() and EXIGetID().

    28    12/07/00 9:19p Shiki
    Fixed EXIInit().

    27    11/29/00 2:10p Shiki
    Modified EXIProbeReset() to call EXIProbe() after resetting the timer.

    26    11/21/00 3:56p Shiki
    Modified to call EXIProbeReset() from EXIInit() to deal with older boot
    ROM.

    25    11/08/00 5:08p Shiki
    Removed proprietary functions.

    24    10/03/00 9:41a Hashida
    From Shiki: Added EXIProbeEx().

    23    9/28/00 2:37p Tian
    From Shiki : Fixed EXIAttach() not to reset __EXIProbeStartTime.

    22    9/18/00 11:56p Shiki
    Modified the type of __EXIProbeStartTime from u32 to s32.

    21    9/12/00 10:47p Shiki
    Revised EXIProbe() to use lomem.

    20    9/06/00 8:23p Shiki
    Modified EXIAttach() to clear potential EXT interrupt.

    19    8/31/00 8:16p Shiki
    Modified SetExiInterruptMask() to support EXI2.

    18    8/24/00 3:40p Shiki
    Revised EXIProbe() to handle debouncing delay (Not enabled).

    17    8/24/00 3:20p Shiki
    Fixed NDEBUG build error.

    16    8/23/00 7:19p Shiki
    Modified EXILock() so that only a single callback can be installed per
    device (Rather than an ASSERT).

    15    8/11/00 3:58p Shiki
    Revised to check CPR_EXT_MASK bit more strictly.

    14    8/08/00 5:50p Shiki
    Fixed EXIAttach().

    13    7/17/00 6:03p Shiki
    Fixed a warning in EXILock() for NDEBUG build.

    12    7/14/00 3:51p Shiki
    Many fixes. Added EXIDetach().

    11    6/20/00 12:48p Shiki
    Fixed SetExiInterruptMask().

    10    4/27/00 3:21p Shiki
    Added SetExiInterruptMask().

    9     4/26/00 8:49p Shiki
    Revised EXILock and EXIUnlock.

    8     4/21/00 6:45p Shiki
    Fixed comment.

    7     4/21/00 5:37p Shiki
    Added EXILock/EXIUnlock.

    6     4/18/00 9:35p Shiki
    Fixed wrong interrupt mask shift operations.

    5     2/02/00 2:41p Shiki
    Added ROM disable code in EXIInit().

    4     1/24/00 2:12p Shiki
    Minor revision.

    3     12/07/99 8:52p Shiki
    Fixed not to check STATE_ATTACHED bit except for dev0, and also not to
    clear interrupts by accident.

    2     12/03/99 2:52p Shiki
    Temporarily removed EXIProve() from EXIAttach().

    4     10/25/99 2:03p Shiki
    Enabled 32 byte alignment check for EXIDma().

    3     10/01/99 3:14p Shiki
    Revised EXISetExiCallback() not to check STATE_ATTACHED bit. See
    description for detail.

    2     9/30/99 5:08p Shiki
    STATE_ATTACHED is implemented for safety.
    EXISync() returns a BOOL value to indicate success.

    1     9/23/99 3:57p Shiki
    Initial check-in (not tested).
 *---------------------------------------------------------------------------*/

#include <dolphin/doldefs.h>
DOLPHIN_LIB_VERSION(EXI);
/* $NoKeywords: $ */

#include <string.h>
#include <dolphin/os/OSExpansion.h>
#include <private/flipper.h>
#include <private/exi.h>
#include <private/OSLoMem.h>
#include <private/OSTdev.h>

#include <dolphin/dvd.h>
#include <secure/dvdcb.h>

extern BOOL __OSInIPL;      // TRUE only in IPL

void __OSEnableBarnacle(s32 chan, u32 dev);

#define EXI_PROBE_DELAY     300   // in msec
#define EXI_PROBE_NULL      0

#define OFFSET(n, a)        (((u32) (n)) & ((a) - 1))
#define TRUNC(n, a)         (((u32) (n)) & ~((a) - 1))
#define ROUND(n, a)         (((u32) (n) + (a) - 1) & ~((a) - 1))

#define REG_MAX             5   // # of registers per channel
#define REG(chan, idx)      (__EXIRegs[((chan) * REG_MAX) + (idx)])

#define MAX_CHAN            3
#define MAX_DEV             3   // chip select
#define MAX_FREQ            6   // clock frequency
#define MAX_TYPE            3   // xfer type RD/WR/RDWR
#define MAX_IMM             4   // max # of bytes via imm mode xfer

#define CPR_EXIINTMSK_MASK  EXI_0CPR_EXIINTMSK_MASK
#define CPR_EXIINT_MASK     EXI_0CPR_EXIINT_MASK
#define CPR_TCINTMSK_MASK   EXI_0CPR_TCINTMSK_MASK
#define CPR_TCINT_MASK      EXI_0CPR_TCINT_MASK
#define CPR_EXTINTMSK_MASK  EXI_0CPR_EXTINTMSK_MASK
#define CPR_EXTINT_MASK     EXI_0CPR_EXTINT_MASK

#define CPR_EXT_MASK        EXI_0CPR_EXT_MASK

#define CPR_ALLINTMSK_MASK  (CPR_EXTINTMSK_MASK | CPR_EXIINTMSK_MASK | CPR_TCINTMSK_MASK)
#define CPR_ALLINT_MASK     (CPR_EXTINT_MASK | CPR_EXIINT_MASK | CPR_TCINT_MASK)
#define CPR_CLK_MASK        EXI_0CPR_CLK_MASK
#define CPR_CS_MASK         (EXI_0CPR_CS0B_MASK | EXI_0CPR_CS1B_MASK | EXI_0CPR_CS2B_MASK)

#define CPR_CS(x)           ((1u << (x)) << EXI_0CPR_CS0B_SHIFT)
#define CPR_CLK(x)          ((x) << EXI_0CPR_CLK_SHIFT)

#define STATE_IDLE          0x00
#define STATE_DMA           0x01
#define STATE_IMM           0x02
#define STATE_BUSY          (STATE_DMA | STATE_IMM)
#define STATE_SELECTED      0x04
#define STATE_ATTACHED      0x08    // for dev 0 (CS0B == 1)
#define STATE_LOCKED        0x10

typedef struct EXIControl
{
    EXICallback exiCallback;
    EXICallback tcCallback;
    EXICallback extCallback;
    vu32        state;
    int         immLen;
    u8*         immBuf;
    u32         dev;            // the device number that locked the channel

    u32         id;
    s32         idTime;

    // unlock callback queue
    int         items;          // # of items in the queue
    struct
    {
        u32         dev;
        EXICallback callback;
    }           queue[MAX_DEV];
} EXIControl;

static EXIControl Ecb[MAX_CHAN];    // EXI Control Block

s32 __EXIProbeStartTime[2] : (OS_BASE_CACHED | OS_EXI0_PROBE_START_TIME);

static u32        IDSerialPort1;    // EXI device ID for serial port 1

static BOOL __EXIProbe(s32 chan);
static BOOL __EXIAttach(s32 chan, EXICallback extCallback);

// CW 1.2.5 scheduling optimizer may swap the register-read order
// even if they are declared as volatile. Therefore turned off.
#ifndef _DEBUG
#pragma scheduling off
#endif

/*---------------------------------------------------------------------------*
  Name:         SetExiInterruptMask

  Description:  Controls EXI interrupt mask bits

  Arguments:    chan        channel to control interrupt mask bits
                exi         pointer to the control block

  Returns:      None.
 *---------------------------------------------------------------------------*/
static void SetExiInterruptMask(s32 chan, EXIControl* exi)
{
    EXIControl* exi2;

    exi2 = &Ecb[2];
    switch (chan)
    {
      case 0:
        if ((exi->exiCallback == 0 && exi2->exiCallback == 0) || (exi->state & STATE_LOCKED))
        {
            __OSMaskInterrupts(OS_INTERRUPTMASK_EXI_0_EXI |
                               OS_INTERRUPTMASK_EXI_2_EXI);
        }
        else
        {
            __OSUnmaskInterrupts(OS_INTERRUPTMASK_EXI_0_EXI |
                                 OS_INTERRUPTMASK_EXI_2_EXI);
        }
        break;
      case 1:
        if (exi->exiCallback == 0 || (exi->state & STATE_LOCKED))
        {
            __OSMaskInterrupts(OS_INTERRUPTMASK_EXI_1_EXI);
        }
        else
        {
            __OSUnmaskInterrupts(OS_INTERRUPTMASK_EXI_1_EXI);
        }
        break;
      case 2:
        if (__OSGetInterruptHandler(__OS_INTERRUPT_PI_DEBUG) == 0 || (exi->state & STATE_LOCKED))
        {
            __OSMaskInterrupts(OS_INTERRUPTMASK_PI_DEBUG);
        }
        else
        {
            __OSUnmaskInterrupts(OS_INTERRUPTMASK_PI_DEBUG);
        }
        break;
    }
}

/*---------------------------------------------------------------------------*
  Name:         CompleteTransfer

  Description:  Copies data read through EXI imm xfer to the user specified
                input buffer. Negates STATE_BUSY just once, since
                CompleteTransfer() might be invoked multiple times for a
                single xfer request.

  Arguments:    chan        channel to complete transfer

  Returns:      None.
 *---------------------------------------------------------------------------*/
static void CompleteTransfer(s32 chan)
{
    EXIControl* exi = &Ecb[chan];
    u8* buf;
    u32 data;
    int i;
    int len;

    ASSERT(0 <= chan && chan < MAX_CHAN);

    // Complete the xfer for immediate mode
    if (exi->state & STATE_BUSY)
    {
        if ((exi->state & STATE_IMM) && (len = exi->immLen))
        {
            buf = exi->immBuf;
            data = REG(chan, EXI_0DATA_IDX);
            for (i = 0 ; i < len ; i++)
            {
                *buf++ = (u8) ((data >> ((3 - i) * 8)) & 0xff);
            }
        }
        exi->state &= ~STATE_BUSY;
    }
}

/*---------------------------------------------------------------------------*
  Name:         EXIImm

  Description:  Sets up an immediate mode data transfer between main memory
                and EXI device.

  Arguments:    chan        channel to communicate
                buf         pointer to buffer
                len         Number of bytes to be read/write. Max 4 bytes.
                type        transfer type (EXI_READ, EXI_WRITE, or EXI_RDWR)
                callback    TC interrupt callback function (can be NULL)

  Returns:      TRUE if transfer is successfully initiated. Otherwise,
                FALSE.
 *---------------------------------------------------------------------------*/
BOOL EXIImm(s32 chan, void* buf, s32 len, u32 type, EXICallback callback)
{
    EXIControl* exi = &Ecb[chan];
    BOOL  enabled;

    ASSERT(exi->state & STATE_SELECTED);
    ASSERT(0 <= chan && chan < MAX_CHAN);
    ASSERT(0 < len && len <= MAX_IMM);
    ASSERT(type < MAX_TYPE);

    enabled = OSDisableInterrupts();
    if ((exi->state & STATE_BUSY) || !(exi->state & STATE_SELECTED))
    {
        OSRestoreInterrupts(enabled);
        return FALSE;
    }

    exi->tcCallback = callback;
    if (exi->tcCallback)
    {
        EXIClearInterrupts(chan, FALSE, TRUE, FALSE);
        __OSUnmaskInterrupts(OS_INTERRUPTMASK_EXI_0_TC >> (3 * chan));
    }

    exi->state |= STATE_IMM;

    // Copy down imm data for writes
    if (type != EXI_READ)
    {
        u32 data;
        int i;

        data = 0;
        for (i = 0 ; i < len ; i++)
        {
            data |= ((u8*) buf)[i] << ((3 - i) * 8);
        }
        REG(chan, EXI_0DATA_IDX) = data;
    }

    // See if data must be xfer'd at completion
    exi->immBuf = buf;
    exi->immLen = (type != EXI_WRITE) ? len : 0;

    // len - 1 to encode 1-4 in 2 bits.
    REG(chan, EXI_0CR_IDX) = EXI_0CR(IO_TSTART_START, IO_DMA_IMM, type, len-1);

    OSRestoreInterrupts(enabled);

    return TRUE;
}

/*---------------------------------------------------------------------------*
  Name:         EXIImmEx

  Description:  Sets up and synchronize immediate mode data transfers of
                the specified length between main memory and EXI device.

  Arguments:    chan        channel to communicate
                buf         pointer to buffer
                len         Number of bytes to be read/write. Max 4 bytes.
                type        transfer type (EXI_READ, EXI_WRITE, or EXI_RDWR)
                callback    TC interrupt callback function (can be NULL)

  Returns:      TRUE if transfer is successfully initiated. Otherwise,
                FALSE.
 *---------------------------------------------------------------------------*/
BOOL EXIImmEx(s32 chan, void* buf, s32 len, u32 mode)
{
    s32 xLen;

    while (len)
    {
        xLen = (len < 4) ? len : 4;
        if (!EXIImm(chan, buf, xLen, mode, NULL))
        {
            return FALSE;
        }

        if (!EXISync(chan))
        {
            return FALSE;
        }

        (u8*) buf += xLen;
        len -= xLen;
    }
    return TRUE;
}

/*---------------------------------------------------------------------------*
  Name:         EXIDma

  Description:  Sets up a DMA transfer between main memory and EXI device.

  Arguments:    chan        channel to communicate
                buf         pointer to buffer (32 byte aligned)
                len         Number of bytes to be read/write. (in multiple
                            of 32 bytes)
                type        transfer type (EXI_READ or EXI_WRITE)
                callback    TC interrupt callback function (can be NULL)

  Returns:      TRUE if transfer is successfully initiated. Otherwise,
                FALSE.
 *---------------------------------------------------------------------------*/
BOOL EXIDma(s32 chan, void* buf, s32 len, u32 type, EXICallback callback)
{
    EXIControl* exi = &Ecb[chan];
    BOOL        enabled;

    ASSERT(exi->state & STATE_SELECTED);
    ASSERT(OFFSET(buf, 32) == 0);
    ASSERT(0 < len && OFFSET(len, 32) == 0);
    // XXX check latest io_reg.h if EXI_0LENGTH_EXILENGTH_MASK disables leftmost 6 bits
    ASSERT(((u32) len & ~EXI_0LENGTH_EXILENGTH_MASK) == 0);
    // DMA doesn't support rdwr
    ASSERT(type == EXI_READ || type == EXI_WRITE);

    enabled = OSDisableInterrupts();
    if ((exi->state & STATE_BUSY) || !(exi->state & STATE_SELECTED))
    {
        OSRestoreInterrupts(enabled);
        return FALSE;
    }

    exi->tcCallback = callback;
    if (exi->tcCallback)
    {
        EXIClearInterrupts(chan, FALSE, TRUE, FALSE);
        __OSUnmaskInterrupts(OS_INTERRUPTMASK_EXI_0_TC >> (3 * chan));
    }

    exi->state |= STATE_DMA;

    // XXX check latest io_reg.h if EXI_0MAR_EXIMAR_MASK disables leftmost 6 bits
    REG(chan, EXI_0MAR_IDX)    = (u32) buf & EXI_0MAR_EXIMAR_MASK;
    REG(chan, EXI_0LENGTH_IDX) = (u32) len;
    REG(chan, EXI_0CR_IDX)     = EXI_0CR(IO_TSTART_START, IO_DMA_DMA, type, 0);

    OSRestoreInterrupts(enabled);

    return TRUE;
}

/*---------------------------------------------------------------------------*
  Name:         EXISync

  Description:  Synchronizes with the previous call to EXIDma() or EXIImm()
                for the specified channel.

  Arguments:    chan        channel to synchronizes with

  Returns:      FALSE if device was unplugged during the call. Otherwise,
                TRUE.
 *---------------------------------------------------------------------------*/

extern u32 __OSGetDIConfig(void);

vu16 __OSDeviceCode : (OS_BASE_CACHED | OS_DVD_DEVICECODE);

BOOL EXISync(s32 chan)
{
    EXIControl* exi = &Ecb[chan];
    BOOL        rc = FALSE;
    BOOL        enabled;

    ASSERT(0 <= chan && chan < MAX_CHAN);

    // Wait for transfer to complete
    while (exi->state & STATE_SELECTED)
    {
        if (EXI_0CR_GET_TSTART(REG(chan, EXI_0CR_IDX)) == IO_TSTART_DONE)
        {
            enabled = OSDisableInterrupts();
            if (exi->state & STATE_SELECTED)
            {
                CompleteTransfer(chan);
                if (__OSGetDIConfig() != 0xff ||    // !production board
                    ((OSGetConsoleType() & 0xf0000000) == OS_CONSOLE_TDEV) ||   // TDEV
                    exi->immLen != 4 ||             // !4 byte read
                    (REG(chan, EXI_0CPR_IDX) & CPR_CLK_MASK) != CPR_CLK(EXI_FREQ_1M) || // !1M
                    (REG(chan, EXI_0DATA_IDX) != EXI_USB_ADAPTER &&
                     REG(chan, EXI_0DATA_IDX) != EXI_IS_VIEWER &&
                     REG(chan, EXI_0DATA_IDX) != EXI_ETHER_VIEWER) ||
                    __OSDeviceCode == (DVD_DEVICECODE_PRESENT | DVD_DEVICECODE_NPDP))
                {
                    rc = TRUE;
                }
            }
            OSRestoreInterrupts(enabled);
            break;
        }
    }

    ASSERT(!(exi->state & STATE_BUSY));
    return rc;
}

/*---------------------------------------------------------------------------*
  Name:         EXIClearInterrupts

  Description:  Clears specified interrupt cause bits

  Arguments:    chan        channel to clear interrupt cause bits
                exi         TRUE to clear EXI interrupt bit
                tc          TRUE to clear TC interrupt bit
                ext         TRUE to clear EXT interrupt bit

  Returns:      previous CPR register bits
 *---------------------------------------------------------------------------*/
u32 EXIClearInterrupts(s32 chan, BOOL exi, BOOL tc, BOOL ext)
{
    u32 cpr;
    u32 prev;

    ASSERT(0 <= chan && chan < MAX_CHAN);
    prev = cpr = REG(chan, EXI_0CPR_IDX);
    cpr &= CPR_CLK_MASK | CPR_CS_MASK | CPR_ALLINTMSK_MASK;
    if (exi)
        cpr |= CPR_EXIINT_MASK;
    if (tc)
        cpr |= CPR_TCINT_MASK;
    if (ext)
        cpr |= CPR_EXTINT_MASK;
    REG(chan, EXI_0CPR_IDX) = cpr;
    return prev;
}

/*---------------------------------------------------------------------------*
  Name:         EXISetExiCallback

  Description:  Installs EXI interrupt callback function for the specified
                channel. To uninstall, specify a NULL pointer as a callback.

  Note:         EXI_2_EXI will be used for EXI_1 devices, etc. So, this
                function doesn't check if a device is attached to the
                specified channel or not.

  Arguments:    chan        channel to install EXI callback
                exiCallback EXI interrupt callback function

  Returns:      previous EXI callback function
 *---------------------------------------------------------------------------*/
EXICallback EXISetExiCallback(s32 chan, EXICallback exiCallback)
{
    EXIControl* exi = &Ecb[chan];
    EXICallback prev;
    BOOL        enabled;

    ASSERT(0 <= chan && chan < MAX_CHAN);
    enabled = OSDisableInterrupts();
    prev = exi->exiCallback;
    exi->exiCallback = exiCallback;

    if (chan != 2)
    {
        SetExiInterruptMask(chan, exi);
    }
    else
    {
        SetExiInterruptMask(0, &Ecb[0]);
    }

    OSRestoreInterrupts(enabled);
    return prev;
}

/*---------------------------------------------------------------------------*
  Name:         EXIProbeReset

  Description:  Resets probe timer. Should be called whenever Gekko time base
                is modified.

  Arguments:    None

  Returns:      None
 *---------------------------------------------------------------------------*/
void EXIProbeReset(void)
{
    __EXIProbeStartTime[0] = __EXIProbeStartTime[1] = EXI_PROBE_NULL;
    Ecb[0].idTime = Ecb[1].idTime = EXI_PROBE_NULL;

    // To minimize wait cycles for games
    __EXIProbe(0);
    __EXIProbe(1);
}

/*---------------------------------------------------------------------------*
  Name:         EXIProbe

  Description:  Verifies if a device is present at the specified channel.

  Arguments:    chan        channel to probe

  Returns:      TRUE if a device presents. Otherwise, FALSE.
 *---------------------------------------------------------------------------*/
static BOOL __EXIProbe(s32 chan)
{
    EXIControl* exi = &Ecb[chan];
    BOOL        enabled;
    BOOL        rc;
    u32         cpr;
    s32         t;

    ASSERT(0 <= chan && chan < MAX_CHAN);
    if (chan == 2)
    {
        // EXI2 doesn't have an EXT line
        return TRUE;
    }

    rc = TRUE;
    enabled = OSDisableInterrupts();
    cpr = REG(chan, EXI_0CPR_IDX);
    if (!(exi->state & EXI_STATE_ATTACHED))
    {
        if (cpr & CPR_EXTINT_MASK)
        {
            EXIClearInterrupts(chan, FALSE, FALSE, TRUE);
            __EXIProbeStartTime[chan] = exi->idTime = EXI_PROBE_NULL;
        }

        if (cpr & CPR_EXT_MASK)
        {
            t = (s32) (OSTicksToMilliseconds(OSGetTime()) / 100) + 1;   // in 0.1 sec (+1 to avoid reset)
            if (__EXIProbeStartTime[chan] == EXI_PROBE_NULL)
            {
                __EXIProbeStartTime[chan] = t;
            }
            if (t - __EXIProbeStartTime[chan] < EXI_PROBE_DELAY / 100)
            {
                rc = FALSE;
            }
        }
        else
        {
            __EXIProbeStartTime[chan] = exi->idTime = EXI_PROBE_NULL;
            rc = FALSE;
        }
    }
    else if (!(cpr & CPR_EXT_MASK) || (cpr & CPR_EXTINT_MASK))
    {
        __EXIProbeStartTime[chan] = exi->idTime = EXI_PROBE_NULL;
        rc = FALSE;
    }
    OSRestoreInterrupts(enabled);

    return rc;
}

BOOL EXIProbe(s32 chan)
{
    EXIControl* exi = &Ecb[chan];
    BOOL        rc;
    u32         id;

    rc = __EXIProbe(chan);
    if (rc && exi->idTime == EXI_PROBE_NULL)
    {
        // The inserted device type has not been identified.
        // Get the device ID first
        rc = EXIGetID(chan, 0, &id) ? TRUE : FALSE;
    }
    return rc;
}

/*---------------------------------------------------------------------------*
  Name:         EXIProbeEx

  Description:  Verifies if a device is present at the specified channel.

  Arguments:    chan        channel to probe

  Returns:      -1 if no device presents
                0  if still probing
                1  if device presents
 *---------------------------------------------------------------------------*/
s32 EXIProbeEx(s32 chan)
{
    if (EXIProbe(chan))
    {
        return 1;
    }
    else if (__EXIProbeStartTime[chan] != EXI_PROBE_NULL)
    {
        return 0;
    }
    else
    {
        return -1;
    }
}

/*---------------------------------------------------------------------------*
  Name:         EXIAttach

  Description:  Enables detach interrupt. EXIAttach() must be invoked
                before using devices connected to connector 1 or 2

  Arguments:    chan        channel to attach (0 or 1)
                extCallback callback invoked when device is unplugged

  Returns:      TRUE if a device is present. Otherwise, FALSE.
 *---------------------------------------------------------------------------*/
static BOOL __EXIAttach(s32 chan, EXICallback extCallback)
{
    EXIControl* exi = &Ecb[chan];
    BOOL        enabled;

    ASSERT(0 <= chan && chan < 2);  // EXI2 doesn't have an EXT interrupt line

    enabled = OSDisableInterrupts();
    if ((exi->state & EXI_STATE_ATTACHED) ||    // Already attached?
        __EXIProbe(chan) == FALSE)              // Nothing inserted?
    {
        OSRestoreInterrupts(enabled);
        return FALSE;
    }

    EXIClearInterrupts(chan, TRUE, FALSE, FALSE);

    exi->extCallback = extCallback;
    __OSUnmaskInterrupts(OS_INTERRUPTMASK_EXI_0_EXT >> (3 * chan));
    exi->state |= STATE_ATTACHED;
    OSRestoreInterrupts(enabled);

    return TRUE;
}

BOOL EXIAttach(s32 chan, EXICallback extCallback)
{
    EXIControl* exi = &Ecb[chan];
    BOOL        enabled;
    BOOL        rc;

    ASSERT(0 <= chan && chan < 2);  // EXI2 doesn't have an EXT interrupt line

    EXIProbe(chan); // To update exi->idTime

    enabled = OSDisableInterrupts();
    if (exi->idTime == EXI_PROBE_NULL)  // Device type unknown?
    {
        OSRestoreInterrupts(enabled);
        return FALSE;
    }
    rc = __EXIAttach(chan, extCallback);
    OSRestoreInterrupts(enabled);
    return rc;
}

/*---------------------------------------------------------------------------*
  Name:         EXIDetach

  Description:  Detach the device at the specified channel. EXIDetach()
                should be called if the device is no longer necessary.
                However, when the device is removed from the slot by the
                user, the detach operation is automatically performed
                by the EXT interrupt handler.

  Arguments:    chan        channel to attach (0 or 1)

  Returns:      TRUE if a device is detached. Otherwise (eg. the device
                is locked and busy), FALSE.
 *---------------------------------------------------------------------------*/
BOOL EXIDetach(s32 chan)
{
    EXIControl* exi = &Ecb[chan];
    BOOL        enabled;

    ASSERT(0 <= chan && chan < 2);  // EXI2 doesn't have an EXT interrupt line
    enabled = OSDisableInterrupts();
    if (!(exi->state & STATE_ATTACHED))
    {
        OSRestoreInterrupts(enabled);
        return TRUE;
    }
    if ((exi->state & STATE_LOCKED) && exi->dev == 0)
    {
        OSRestoreInterrupts(enabled);
        return FALSE;
    }

    // Note TC is not used by dev0 here
    exi->state &= ~STATE_ATTACHED;
    __OSMaskInterrupts((OS_INTERRUPTMASK_EXI_0_EXT |
                        OS_INTERRUPTMASK_EXI_0_EXI) >> (3 * chan));
    OSRestoreInterrupts(enabled);
    return TRUE;
}

/*---------------------------------------------------------------------------*
  Name:         EXISelect

  Description:  Asserts a specified chip select line and sets clock
                frequency.

  Arguments:    chan        channel to select
                dev         device number to assert access
                freq        clock frequency (one of EXI_FREQ_*)

  Returns:      TRUE if succeeded. FALSE if the specified channel is busy, or
                it isn't attached.
 *---------------------------------------------------------------------------*/
BOOL EXISelect(s32 chan, u32 dev, u32 freq)
{
    EXIControl* exi = &Ecb[chan];
    u32         cpr;
    BOOL        enabled;

    ASSERT(0 <= chan && chan < MAX_CHAN);
    ASSERT(chan == 0 && dev < MAX_DEV || dev == 0);
    ASSERT(freq < MAX_FREQ);
    ASSERT(!(exi->state & STATE_SELECTED));

    enabled = OSDisableInterrupts();
    if ((exi->state & STATE_SELECTED) ||
        chan != 2 && (dev == 0 && !(exi->state & STATE_ATTACHED) && !__EXIProbe(chan) ||
                      !(exi->state & STATE_LOCKED) ||
                      (exi->dev != dev)))
    {
        OSRestoreInterrupts(enabled);
        return FALSE;
    }

    exi->state |= STATE_SELECTED;
    cpr = REG(chan, EXI_0CPR_IDX);
    cpr &= CPR_ALLINTMSK_MASK;
    cpr |= CPR_CS(dev) | CPR_CLK(freq);
    REG(chan, EXI_0CPR_IDX) = cpr;

    if (exi->state & STATE_ATTACHED)
    {
        switch (chan)
        {
          case 0:
            __OSMaskInterrupts(OS_INTERRUPTMASK_EXI_0_EXT);
            break;
          case 1:
            __OSMaskInterrupts(OS_INTERRUPTMASK_EXI_1_EXT);
            break;
        }
    }

    OSRestoreInterrupts(enabled);
    return TRUE;
}

/*---------------------------------------------------------------------------*
  Name:         EXIDeselect

  Description:  Negates the current chip select line

  Arguments:    chan        channel to deselect

  Returns:      TRUE if succeeded. FALSE if the specified channel was not
                busy.
 *---------------------------------------------------------------------------*/
BOOL EXIDeselect(s32 chan)
{
    EXIControl* exi = &Ecb[chan];
    u32         cpr;
    BOOL        enabled;

    ASSERT(0 <= chan && chan < MAX_CHAN);
    enabled = OSDisableInterrupts();
    if (!(exi->state & STATE_SELECTED))
    {
        OSRestoreInterrupts(enabled);
        return FALSE;
    }
    exi->state &= ~STATE_SELECTED;
    cpr = REG(chan, EXI_0CPR_IDX);
    REG(chan, EXI_0CPR_IDX) = cpr & CPR_ALLINTMSK_MASK;

    if (exi->state & STATE_ATTACHED)
    {
        switch (chan)
        {
          case 0:
            __OSUnmaskInterrupts(OS_INTERRUPTMASK_EXI_0_EXT);
            break;
          case 1:
            __OSUnmaskInterrupts(OS_INTERRUPTMASK_EXI_1_EXT);
            break;
        }
    }

    OSRestoreInterrupts(enabled);

    if (chan != 2 && (cpr & CPR_CS(0)))
    {
        return __EXIProbe(chan) ? TRUE : FALSE;
    }

    return TRUE;
}

/*---------------------------------------------------------------------------*
  Name:         EXIIntrruptHandler

  Description:  Interrupt handler for EXI interrupt

  Arguments:    interrupt   interrupt number
                context     current context

  Returns:      None.
 *---------------------------------------------------------------------------*/
static void EXIIntrruptHandler(__OSInterrupt interrupt, OSContext* context)
{
    s32         chan;
    EXIControl* exi;
    EXICallback callback;

    chan = (interrupt - __OS_INTERRUPT_EXI_0_EXI) / 3;
    ASSERT(0 <= chan && chan < MAX_CHAN);
    exi = &Ecb[chan];
    EXIClearInterrupts(chan, TRUE, FALSE, FALSE);
    callback = exi->exiCallback;
    if (callback)
    {
        OSContext exceptionContext;

        // Create new context on stack so that callback can touch FPU registers
        OSClearContext(&exceptionContext);
        OSSetCurrentContext(&exceptionContext);

        callback(chan, context);

        OSClearContext(&exceptionContext);
        OSSetCurrentContext(context);
    }
}

/*---------------------------------------------------------------------------*
  Name:         TCIntrruptHandler

  Description:  Interrupt handler for TC interrupt

  Arguments:    interrupt   interrupt number
                context     current context

  Returns:      None.
 *---------------------------------------------------------------------------*/
static void TCIntrruptHandler(__OSInterrupt interrupt, OSContext* context)
{
    s32         chan;
    EXIControl* exi;
    EXICallback callback;

    chan = (interrupt - __OS_INTERRUPT_EXI_0_TC) / 3;
    ASSERT(0 <= chan && chan < MAX_CHAN);
    exi = &Ecb[chan];
    __OSMaskInterrupts(OS_INTERRUPTMASK(interrupt));
    EXIClearInterrupts(chan, FALSE, TRUE, FALSE);
    callback = exi->tcCallback;
    if (callback)
    {
        OSContext exceptionContext;

        exi->tcCallback = 0;
        CompleteTransfer(chan);

        // Create new context on stack so that callback can touch FPU registers
        OSClearContext(&exceptionContext);
        OSSetCurrentContext(&exceptionContext);

        callback(chan, context);

        OSClearContext(&exceptionContext);
        OSSetCurrentContext(context);
    }
}
/*---------------------------------------------------------------------------*
  Name:         EXTIntrruptHandler

  Description:  Interrupt handler for EXT interrupt. (Invoked when a device
                is unplugged.)

  Arguments:    interrupt   interrupt number
                context     current context

  Returns:      None.
 *---------------------------------------------------------------------------*/
static void EXTIntrruptHandler(__OSInterrupt interrupt, OSContext* context)
{
    s32         chan;
    EXIControl* exi;
    EXICallback callback;

    chan = (interrupt - __OS_INTERRUPT_EXI_0_EXT) / 3;
    ASSERT(0 <= chan && chan < 2);
    // Note TC is not used by dev0 here
    __OSMaskInterrupts((OS_INTERRUPTMASK_EXI_0_EXT |
                        OS_INTERRUPTMASK_EXI_0_EXI) >> (3 * chan));
    exi = &Ecb[chan];
    callback = exi->extCallback;
    exi->state &= ~STATE_ATTACHED;
    if (callback)
    {
        OSContext exceptionContext;

        // Create new context on stack so that callback can touch FPU registers
        OSClearContext(&exceptionContext);
        OSSetCurrentContext(&exceptionContext);

        exi->extCallback = 0;
        callback(chan, context);

        OSClearContext(&exceptionContext);
        OSSetCurrentContext(context);
    }
}

/*---------------------------------------------------------------------------*
  Name:         EXIInit

  Description:  Initializes expansion interface.

  Arguments:    None.

  Returns:      None.
 *---------------------------------------------------------------------------*/
void EXIInit(void)
{
    u32 id;

    while (EXI_0CR_GET_TSTART(REG(0, EXI_0CR_IDX)) == IO_TSTART_START ||
           EXI_0CR_GET_TSTART(REG(1, EXI_0CR_IDX)) == IO_TSTART_START ||
           EXI_0CR_GET_TSTART(REG(2, EXI_0CR_IDX)) == IO_TSTART_START)
    {
        ;
    }

    __OSMaskInterrupts(OS_INTERRUPTMASK_EXI_0_EXI   |
                       OS_INTERRUPTMASK_EXI_0_TC    |
                       OS_INTERRUPTMASK_EXI_0_EXT   |
                       OS_INTERRUPTMASK_EXI_1_EXI   |
                       OS_INTERRUPTMASK_EXI_1_TC    |
                       OS_INTERRUPTMASK_EXI_1_EXT   |
                       OS_INTERRUPTMASK_EXI_2_EXI   |
                       OS_INTERRUPTMASK_EXI_2_TC);

    REG(0, EXI_0CPR_IDX) = 0;
    REG(1, EXI_0CPR_IDX) = 0;
    REG(2, EXI_0CPR_IDX) = 0;

    // Set ROM disable, so EXI0 not affected by scramble
    REG(0, EXI_0CPR_IDX) = EXI_0CPR_ROMDIS_MASK;

    __OSSetInterruptHandler(__OS_INTERRUPT_EXI_0_EXI, EXIIntrruptHandler);
    __OSSetInterruptHandler(__OS_INTERRUPT_EXI_0_TC,  TCIntrruptHandler);
    __OSSetInterruptHandler(__OS_INTERRUPT_EXI_0_EXT, EXTIntrruptHandler);
    __OSSetInterruptHandler(__OS_INTERRUPT_EXI_1_EXI, EXIIntrruptHandler);
    __OSSetInterruptHandler(__OS_INTERRUPT_EXI_1_TC,  TCIntrruptHandler);
    __OSSetInterruptHandler(__OS_INTERRUPT_EXI_1_EXT, EXTIntrruptHandler);
    __OSSetInterruptHandler(__OS_INTERRUPT_EXI_2_EXI, EXIIntrruptHandler);
    __OSSetInterruptHandler(__OS_INTERRUPT_EXI_2_TC,  TCIntrruptHandler);

    EXIGetID(0, 2, &IDSerialPort1);

    if (__OSInIPL)
    {
        EXIProbeReset();
    }
    else
    {
        if (EXIGetID(0, 0, &id) && id == EXI_BARNACLE_ENABLER)
        {
            __OSEnableBarnacle(1, 0);
        }
        else if (EXIGetID(1, 0, &id) && id == EXI_BARNACLE_ENABLER)
        {
            __OSEnableBarnacle(0, 2);
        }
    }

    OSRegisterVersion(__EXIVersion);
}

/*---------------------------------------------------------------------------*
  Name:         EXILock

  Description:  Locks the specified EXI channel for the specified device.
                If the channel is already locked, the function returns
                immediately with an error

  Note:         Only a single callback can be installed per device.

  Arguments:    chan        channel to lock
                dev         device number to activate
                callback    callback to be called when the channel is
                            unlocked in the case EXILock failed.

  Returns:      TRUE if the function locked the channel successfully.
                Otherwise FALSE.
 *---------------------------------------------------------------------------*/
BOOL EXILock(s32 chan, u32 dev, EXICallback unlockedCallback)
{
    EXIControl* exi = &Ecb[chan];
    BOOL        enabled;
    int         i;

    ASSERT(0 <= chan && chan < MAX_CHAN);
    ASSERT(chan == 0 && dev < MAX_DEV || dev == 0);
    enabled = OSDisableInterrupts();
    if (exi->state & STATE_LOCKED)
    {
        if (unlockedCallback)
        {
            ASSERT(chan == 0 && exi->items < (MAX_DEV - 1) || exi->items == 0);
            for (i = 0; i < exi->items; i++)
            {
                if (exi->queue[i].dev == dev)
                {
                    OSRestoreInterrupts(enabled);
                    return FALSE;
                }
            }
            exi->queue[exi->items].callback = unlockedCallback;
            exi->queue[exi->items].dev      = dev;
            exi->items++;
        }
        OSRestoreInterrupts(enabled);
        return FALSE;
    }
    ASSERT(exi->items == 0);
    exi->state |= STATE_LOCKED;
    exi->dev = dev;
    SetExiInterruptMask(chan, exi);

    OSRestoreInterrupts(enabled);
    return TRUE;
}

/*---------------------------------------------------------------------------*
  Name:         EXIUnlock

  Description:  Unlocks the specified EXI channel.

  Arguments:    chan        channel to unlock

  Returns:      TRUE if succeeded. Otherwise FALSE.
 *---------------------------------------------------------------------------*/
BOOL EXIUnlock(s32 chan)
{
    EXIControl* exi = &Ecb[chan];
    BOOL        enabled;
    EXICallback unlockedCallback;

    ASSERT(0 <= chan && chan < MAX_CHAN);
    enabled = OSDisableInterrupts();
    if (!(exi->state & STATE_LOCKED))
    {
        OSRestoreInterrupts(enabled);
        return FALSE;
    }
    exi->state &= ~STATE_LOCKED;
    SetExiInterruptMask(chan, exi);

    if (0 < exi->items)
    {
        unlockedCallback = exi->queue[0].callback;
        if (0 < --exi->items)
        {
            memmove(&exi->queue[0], &exi->queue[1], sizeof(exi->queue[0]) * exi->items);
        }
        unlockedCallback(chan, 0);
    }

    OSRestoreInterrupts(enabled);
    return TRUE;
}

/*---------------------------------------------------------------------------*
  Name:         EXIGetState

  Description:  Returns current EXI control block state

  Arguments:    chan        channel to inspect state

  Returns:      EXI control block state
 *---------------------------------------------------------------------------*/
u32 EXIGetState(s32 chan)
{
    EXIControl* exi = &Ecb[chan];

    ASSERT(0 <= chan && chan < MAX_CHAN);
    return (u32) exi->state;
}

/*---------------------------------------------------------------------------*
  Name:         EXIGetID

  Description:  Returns current EXI device id

  Arguments:    chan        channel number
                dev         device number
                id          EXI device id to receive

  Returns:      Zero if device id is not yet available (i.e., no device is
                inserted into the slot, or the OS is still probing the
                device).

                A non-zero value if device id is available. The returned
                value will change every time the device is swapped.
 *---------------------------------------------------------------------------*/
static void UnlockedHandler(s32 chan, OSContext* context)
{
    #pragma unused(context)
    u32 id;

    // EXIGetID() takes about 71 usec.
    EXIGetID(chan, 0, &id);
}

s32 EXIGetID(s32 chan, u32 dev, u32* id)
{
    EXIControl* exi = &Ecb[chan];
    BOOL err;
    u32  cmd;
    s32  startTime;
    BOOL enabled;

    ASSERT(0 <= chan && chan < MAX_CHAN);

    if (chan == 0 && dev == 2 && IDSerialPort1 != 0)
    {
        *id = IDSerialPort1;
        return !EXI_PROBE_NULL;
    }

    if (chan < 2 && dev == 0)
    {
        // Update __EXIProbeStartTime[]
        if (!__EXIProbe(chan))
        {
            return EXI_PROBE_NULL;
        }

        if (exi->idTime == __EXIProbeStartTime[chan])
        {
            *id = exi->id;
            return exi->idTime;
        }

        if (!__EXIAttach(chan, NULL))
        {
            return EXI_PROBE_NULL;
        }

        startTime = __EXIProbeStartTime[chan];
    }

    // Note EXILock() can fail if modem and other device is busy
    enabled = OSDisableInterrupts();
    err = !EXILock(chan, dev, (chan < 2 && dev == 0) ? UnlockedHandler : NULL);
    if (!err)
    {
        err = !EXISelect(chan, dev, EXI_FREQ_1M);
        if (!err)
        {
            // ID command
            cmd = 0;
            err |= !EXIImm(chan, &cmd, 2, EXI_WRITE, NULL);
            err |= !EXISync(chan);
            err |= !EXIImm(chan, id, 4, EXI_READ, NULL);
            err |= !EXISync(chan);
            err |= !EXIDeselect(chan);
        }
        EXIUnlock(chan);
    }
    OSRestoreInterrupts(enabled);

    if (chan < 2 && dev == 0)
    {
        EXIDetach(chan);

        // Updates exi->id if no error and the same card is still inserted
        enabled = OSDisableInterrupts();
        err |= (startTime != __EXIProbeStartTime[chan]);
        if (!err)
        {
            exi->id = *id;
            exi->idTime = startTime;
        }
        OSRestoreInterrupts(enabled);

        return err ? EXI_PROBE_NULL : exi->idTime;
    }

    return err ? EXI_PROBE_NULL : !EXI_PROBE_NULL;
}

/*---------------------------------------------------------------------------*
  Name:         EXIGetType

  Description:  Returns current EXI device type

  Arguments:    chan        channel number
                dev         device number
                type        EXI device type to receive

  Returns:      Zero if device id is not yet available (i.e., no device is
                inserted into the slot, or the OS is still probing the
                device).

                A non-zero value if device type is available. The returned
                value will change every time the device is swapped.
 *---------------------------------------------------------------------------*/

#define CARD_CUSTOM_ID          0x00000000  // Nintendo code for flash (0x0000xxxx)

#define CARD_ID_SIZE            0x000000fc  // 4, 8, 16, 32, 64, 128
#define CARD_ID_CHIPS           0x00000003  // 0: 1 chip - 3: 4 chip
#define CARD_ID_LATENCY         0x00000700  // 2 * 2 ^ (1 + latency) bytes
#define CARD_ID_LATENCY_SHIFT   8
#define CARD_ID_SECTOR          0x00003800  // 0: 8K,   1: 16K,  2: 32K,      3: 64K,
                                            // 4: 128K, 5: 256K, 6: reserved, 7: reserved
#define CARD_ID_SECTOR_SHIFT    11

s32 EXIGetType(s32 chan, u32 dev, u32* type)
{
    u32 _type;
    s32 probe;

    probe = EXIGetID(chan, dev, &_type);
    if (probe == EXI_PROBE_NULL)
    {
        return probe;
    }

    // Ether (prototypes) or Mic
    switch (_type & 0xffffff00)
    {
      case EXI_ETHER_PROTO1:
      case EXI_ETHER_PROTO2:
      case EXI_ETHER_PROTO3:
      case EXI_MIC:
        *type = _type & 0xffffff00;
        return probe;
    }

    switch (_type & 0xffff0000)
    {
      case CARD_CUSTOM_ID:
        if (_type & (CARD_ID_CHIPS | CARD_ID_SECTOR))
        {
            // 1 chip, 8KB sector version only
            break;
        }
        switch (_type & CARD_ID_SIZE)
        {
          case  4:  case 8:  case  16:
          case 32:  case 64: case 128:
            *type = _type & (CARD_ID_SIZE);
            return probe;
        }
        break;
      case EXI_IS_VIEWER:
        *type = EXI_IS_VIEWER;
        return probe;
    }

    *type = _type;
    return probe;
}

/*---------------------------------------------------------------------------*
  Name:         EXIGetTypeString

  Description:  Gets description of the specified EXI device type

  Arguments:    type        type retrieved by EXIGetType()

  Returns:      character string that describes the specified EXI device
 *---------------------------------------------------------------------------*/
char* EXIGetTypeString(u32 type)
{
    switch (type)
    {
      case EXI_MEMORY_CARD_59:
        return "Memory Card 59";
      case EXI_MEMORY_CARD_123:
        return "Memory Card 123";
      case EXI_MEMORY_CARD_251:
        return "Memory Card 251";
      case EXI_MEMORY_CARD_507:
        return "Memory Card 507";
      case EXI_MEMORY_CARD_1019:
        return "Memory Card 1019";
      case EXI_MEMORY_CARD_2043:
        return "Memory Card 2043";
      case EXI_USB_ADAPTER:
        return "USB Adapter";
      case EXI_NPDP_GDEV:
        return "GDEV";
      case EXI_MODEM:
        return "Modem";
      case EXI_MARLIN:
        return "Marlin";
      case EXI_AD16:
        return "AD16";
      case EXI_RS232C:
        return "RS232C";
      case EXI_NET_CARD_59:
      case EXI_NET_CARD_123:
      case EXI_NET_CARD_251:
      case EXI_NET_CARD_507:
      case EXI_NET_CARD_1019:
      case EXI_NET_CARD_2043:
        return "Net Card";
      case EXI_ETHER_VIEWER:
        return "Artist Ether";
      case EXI_ETHER_PROTO0:
      case EXI_ETHER_PROTO1:
      case EXI_ETHER_PROTO2:
      case EXI_ETHER_PROTO3:
        return "Broadband Adapter";
      case EXI_MIC:
        return "Mic";
      case EXI_STREAM_HANGER:
        return "Stream Hanger";
      case EXI_IS_VIEWER:
        return "IS-DOL-VIEWER";
      default:
        return "Unknown";
    }
}

#ifndef _DEBUG
#pragma scheduling reset
#endif
