/*---------------------------------------------------------------------------*
  Project:  Dolphin OS EXI UART for Minnow
  File:     EXIUart.c

  Copyright 1998-2001 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: EXIUart.c,v $
  Revision 1.1.1.1  2004/01/03 01:02:28  paulm
  Gamecube library source from Nintendo

    
    16    10/28/02 9:52 Shiki
    Modified WriteUARTN() to disable interrupts while locking the EXI
    channel.

    15    11/27/01 17:12 Shiki
    Fixed not to use kUARTDataError to support CW GC 1.3 beta.

    14    11/22/01 16:45 Shiki
    Moved from OS.

    12    11/21/01 20:58 Shiki
    Added __OSEnableBarnacle().

    11    5/11/01 10:21a Shiki
    Modified InitializeUAR() to always initialize serEnabled.

    10    8/16/00 7:44p Shiki
    Added lost console type check code in InitializeUART.

    9     8/11/00 4:27p Shiki
    Fixed WriteUARTN to return NoError even if EXI lock failed to let MSL
    does not freeze stdout.

    8     5/12/00 10:10a Shiki
    Temporarily disabled ORCA settings.

    7     5/11/00 11:47a Shiki
    Changed EXI_FREQ to 8M for DRIP.

    6     5/10/00 11:02a Shiki
    Modified to support both MINNOW and ORCA.

    5     4/26/00 8:49p Shiki
    Modified WriteUARTN to use new EXILock interface.

    4     4/21/00 5:37p Shiki
    Revised to use EXILock/EXIUnlock.

    3     12/07/99 8:53p Shiki
    EXIAttach() should not be called except for EXI dev 0.

    2     12/01/99 6:57p Shiki
    Fixed bugs.

    1     11/11/99 6:33p Shiki
    Revised from the ArtX code.
  $NoKeywords: $
 *---------------------------------------------------------------------------*/
/****************************************************************************
 *
 *   (C) 1999 ARTX INC..  ALL RIGHTS RESERVED.  UNPUBLISHED -- RIGHTS
 *   RESERVED UNDER THE COPYRIGHT LAWS OF THE UNITED STATES.  USE OF A
 *   COPYRIGHT NOTICE IS PRECAUTIONARY ONLY AND DOES NOT IMPLY PUBLICATION
 *   OR DISCLOSURE.
 *
 *   THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF
 *   ARTX INC..  USE, DISCLOSURE, OR REPRODUCTION IS PROHIBITED WITHOUT
 *   THE PRIOR EXPRESS WRITTEN PERMISSION OF ARTX INC..
 *
 *                   RESTRICTED RIGHTS LEGEND
 *
 *   Use, duplication, or disclosure by the Government is subject to
 *   restrictions as set forth in subparagraph (c)(1)(ii) of the Rights
 *   in Technical Data and Computer Software clause at DFARS 252.227-7013
 *   or subparagraphs (c)(1) and (2) of Commercial Computer Software --
 *   Restricted Rights at 48 CFR 52.227-19, as applicable.
 *
 *   ArtX Inc.
 *   3400 Hillview Ave, Bldg 5
 *   Palo Alto, CA 94304
 *
 ***************************************************************************/

#if defined(MINNOW) || defined(ORCA)

// Define USE_SW to check WriteUARTN() execution time
#undef USE_SW
// #define USE_SW

#define SER_CHAN        0
#define SER_DEV         1
#define SER_FIFO        16

#include <string.h>
#include <dolphin/os.h>
#include <private/flipper.h>
#include <private/exi.h>
#include <UART.h>

extern void __OSEnableBarnacle(s32 chan, u32 dev);

#define SERIAL_TX       0x800400u
#define SER_MAGIC       0xa5ff005a

static s32 Chan;
static u32 Dev;
static u32 Enabled = 0;
static u32 BarnacleEnabled = 0;

#ifdef USE_SW
static OSStopwatch  StopWatch;
#endif

// Probe EXI2 Barnacle
static BOOL ProbeBarnacle(s32 chan, u32 dev, u32* revision)
{
    BOOL err;
    u32  cmd;

    if (chan != 2 && dev == 0 && !EXIAttach(chan, NULL))
    {
        return FALSE;
    }

    err = !EXILock(chan, dev, NULL);
    if (!err)
    {
        err = !EXISelect(chan, dev, EXI_FREQ_1M);
        if (!err)
        {
            // revision code command
            cmd = 0x20011300;
            err = FALSE;
            err |= !EXIImm(chan, &cmd, 4, EXI_WRITE, NULL);
            err |= !EXISync(chan);
            err |= !EXIImm(chan, revision, 4, EXI_READ, NULL);
            err |= !EXISync(chan);
            err |= !EXIDeselect(chan);
        }
        EXIUnlock(chan);
    }

    if (chan != 2 && dev == 0)
    {
        EXIDetach(chan);
    }

    if (err)
    {
        return FALSE;
    }

    return (*revision != 0xFFFFFFFF) ? TRUE : FALSE;
}

void __OSEnableBarnacle(s32 chan, u32 dev)
{
    u32 id;

    // Check EXI2 UART
    if (EXIGetID(chan, dev, &id))
    {
        switch (id)
        {
          case 0xffffffff:  // Nothing attached
          case EXI_MEMORY_CARD_59:
          case EXI_MEMORY_CARD_123:
          case EXI_MEMORY_CARD_251:
          case EXI_MEMORY_CARD_507:
          case EXI_USB_ADAPTER:
          case EXI_NPDP_GDEV:
          case EXI_MODEM:
          case EXI_MARLIN:
          case EXI_ETHER_PROTO0:
          case EXI_ETHER_PROTO1:
          case EXI_ETHER_PROTO2:
          case EXI_ETHER_PROTO3:
          case EXI_RS232C:
          case EXI_MIC:
          case EXI_AD16:
          case EXI_STREAM_HANGER:
          case EXI_NET_CARD_59:
          case EXI_NET_CARD_123:
          case EXI_NET_CARD_251:
          case EXI_NET_CARD_507:
            break;
          default:
            if (ProbeBarnacle(chan, dev, &id))
            {
                Chan = chan;
                Dev  = dev;
                Enabled = BarnacleEnabled = SER_MAGIC;
            }
            break;
        }
    }
}

UARTError InitializeUART(UARTBaudRate baudRate)
{
    #pragma unused(baudRate)

    if (BarnacleEnabled == SER_MAGIC)
    {
        return kUARTNoError;
    }

#ifdef USE_SW
    OSInitStopwatch(&StopWatch, "");
#endif

    if (!(OSGetConsoleType() & OS_CONSOLE_DEVELOPMENT))
    {
        Enabled = 0;
        return kUARTConfigurationError;
    }
    else
    {
        Chan = SER_CHAN;
        Dev  = SER_DEV;
        Enabled = SER_MAGIC;
        return kUARTNoError;
    }
}

UARTError ReadUARTN(void* bytes, unsigned long length)
{
#pragma unused(bytes)
#pragma unused(length)
    return kUARTNoData;
}

static int QueueLength(void)
{
    u32 cmd;

    // Can't share the bus
    if (!EXISelect(Chan, Dev, EXI_FREQ_8M))
        return -1;

    // send a read command
    // Reads require a dead cycle...
    cmd = SERIAL_TX << 6;
    EXIImm(Chan, &cmd, 4, EXI_WRITE, NULL);
    EXISync(Chan);

    EXIImm(Chan, &cmd, 1, EXI_READ, NULL);
    EXISync(Chan);
    EXIDeselect(Chan);

    return SER_FIFO - (int) ((cmd >> 24) & 0xff);
}

UARTError WriteUARTN(const void* buf, unsigned long len)
{
    BOOL      enabled;
    u32       cmd;
    int       qLen;
    long      xLen;
    char*     ptr;
    BOOL      locked;
    UARTError error;

    // use a magic in case BSS is fried.
    if (Enabled != SER_MAGIC)
        return kUARTConfigurationError;

    enabled = OSDisableInterrupts();

    locked = EXILock(Chan, Dev, 0);
    if (!locked)
    {
        OSRestoreInterrupts(enabled);
        return kUARTNoError;
    }

#ifdef USE_SW
    OSStartStopwatch(&StopWatch);
#endif

    for (ptr = (char*) buf; ptr - buf < len ; ptr++)
    {
        if (*ptr == '\n')
            *ptr = '\r';
    }

    error = kUARTNoError;
    cmd = (SERIAL_TX | 0x2000000) << 6;        // write bit
    while (len)
    {
        qLen = QueueLength();
        if (qLen < 0)
        {
            // EXI interface is busy. Oh well.
            error = kUARTBufferOverflow;
            break;
        }

        // Wait until we can send at least 12 bytes or the whole thing
        if (qLen < 12 && qLen < len)
            continue;

        // Can't share the bus
        if (!EXISelect(Chan, Dev, EXI_FREQ_8M))
        {
            error = kUARTBufferOverflow;
            break;
        }

        // send out the printf header
        EXIImm(Chan, &cmd, 4, EXI_WRITE, NULL);
        EXISync(Chan);

        // Do immediate mode xfers only at this time.
        while (qLen && len)
        {
            if (qLen < 4 && qLen < len)
                break;
            xLen = (len < 4) ? (long) len : 4;
            EXIImm(Chan, (void*) buf, xLen, EXI_WRITE, NULL);
            (u8*) buf += xLen;
            len  -= xLen;
            qLen -= xLen;
            EXISync(Chan);
        }
        EXIDeselect(Chan);
    }

    EXIUnlock(Chan);

#ifdef USE_SW
    OSStopStopwatch(&StopWatch);
#endif

    OSRestoreInterrupts(enabled);

    return error;
}

#ifdef USE_SW
void ReportUART(void);
void ReportUART(void)
{
    OSDumpStopwatch(&StopWatch);
}
#endif

#endif  // defined(MINNOW) || defined(ORCA)
