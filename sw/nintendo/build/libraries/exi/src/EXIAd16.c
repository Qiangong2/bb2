/*---------------------------------------------------------------------------*
  Project:  Dolphin OS EXI_AD16 Interface API
  File:     EXIAd16.c

  Copyright 2000, 2001  Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: EXIAd16.c,v $
  Revision 1.1.1.1  2004/01/03 01:02:28  paulm
  Gamecube library source from Nintendo

    
    7     11/22/01 16:45 Shiki
    Moved from OS.

    5     9/21/01 17:51 Shiki
    Added AD16Probe().

    4     10/26/00 5:00p Shiki
    Added initialization check code.

    3     10/26/00 4:49p Shiki
    Fixed copyright.

    2     10/26/00 4:46p Shiki
    Removed debug printf.

    1     10/26/00 4:41p Shiki
    Initial check-in.
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#include <private/ad16.h>

#define AD16_ID             0x04120000
#define AD16_CHAN           2
#define AD16_DEV            0
#define AD16_FREQ           EXI_FREQ_8M

#define AD16_CMD_ID         0x0000u     // W2-R4
#define AD16_CMD_WRITE_REG  0xA0u       // W5 (4 byte data)
#define AD16_CMD_READ_REG   0xA2u       // W1-R4
#define AD16_CMD_WRITE_SRAM 0xB00000u   // W67 (2 byte addr, 32 byte data, 32 byte dummy)
#define AD16_CMD_READ_SRAM  0xB2000000u // W4-Rx (2 byte addr, 1 byte dummy - any # of data)
#define AD16_CMD_GET        0xC000u     // W2
#define AD16_CMD_RELEASE    0xC200u     // W2

static BOOL Initialized;

BOOL AD16Init(void)
{
    BOOL err;
    u32  cmd;
    u32  id;

    if (Initialized)
    {
        return TRUE;
    }

    if (!EXILock(AD16_CHAN, AD16_DEV, 0))
    {
        return FALSE;
    }

    if (!EXISelect(AD16_CHAN, AD16_DEV, EXI_FREQ_1M))
    {
        EXIUnlock(AD16_CHAN);
        return FALSE;
    }

    cmd = AD16_CMD_ID << 16;
    err = FALSE;
    err |= !EXIImm(AD16_CHAN, &cmd, 2, EXI_WRITE, NULL);
    err |= !EXISync(AD16_CHAN);
    err |= !EXIImm(AD16_CHAN, &id, 4, EXI_READ, NULL);
    err |= !EXISync(AD16_CHAN);
    err |= !EXIDeselect(AD16_CHAN);

    EXIUnlock(AD16_CHAN);
    if (err || id != AD16_ID)
    {
        return FALSE;
    }
    Initialized = TRUE;
    return TRUE;
}

BOOL AD16WriteReg(u32 word)
{
    BOOL err;
    u32  cmd;

    if (!Initialized || !EXILock(AD16_CHAN, AD16_DEV, 0))
    {
        return FALSE;
    }

    if (!EXISelect(AD16_CHAN, AD16_DEV, AD16_FREQ))
    {
        EXIUnlock(AD16_CHAN);
        return FALSE;
    }

    cmd = AD16_CMD_WRITE_REG << 24;
    err = FALSE;
    err |= !EXIImm(AD16_CHAN, &cmd, 1, EXI_WRITE, NULL);
    err |= !EXISync(AD16_CHAN);
    err |= !EXIImm(AD16_CHAN, &word, 4, EXI_WRITE, NULL);
    err |= !EXISync(AD16_CHAN);
    err |= !EXIDeselect(AD16_CHAN);

    EXIUnlock(AD16_CHAN);
    return err ? FALSE : TRUE;
}

BOOL AD16ReadReg(u32* word)
{
    BOOL err;
    u32  cmd;

    if (!Initialized || !EXILock(AD16_CHAN, AD16_DEV, 0))
    {
        return FALSE;
    }

    if (!EXISelect(AD16_CHAN, AD16_DEV, AD16_FREQ))
    {
        EXIUnlock(AD16_CHAN);
        return FALSE;
    }

    cmd = AD16_CMD_READ_REG << 24;
    err = FALSE;
    err |= !EXIImm(AD16_CHAN, &cmd, 1, EXI_WRITE, NULL);
    err |= !EXISync(AD16_CHAN);
    err |= !EXIImm(AD16_CHAN, word, 4, EXI_READ, NULL);
    err |= !EXISync(AD16_CHAN);
    err |= !EXIDeselect(AD16_CHAN);

    EXIUnlock(AD16_CHAN);
    return err ? FALSE : TRUE;
}

BOOL AD16Probe(void)
{
    return Initialized;
}
