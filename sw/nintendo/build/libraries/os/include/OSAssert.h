/*---------------------------------------------------------------------------*
  Project:  Dolphin OS Error Messages
  File:     OSAssert.h

  Copyright 1998-2001 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: OSAssert.h,v $
  Revision 1.1.1.1  2004/06/09 17:39:20  paulm
  GC library source from Nintendo SDK

    
    14    8/26/02 11:58 Shiki
    Added OS_ERR_CANCELALARMS_INVTAG.

    13    10/02/01 9:17 Shiki
    Fixed SI_ERR_SETXY_INVX message.

    12    01/04/06 16:41 Shiki
    Fixed SI_ERR_SETXY_INVX message.

    11    01/03/22 9:18 Shiki
    Added SI_ERR_GETCOMMAND_INVCHAN.

    10    01/03/08 15:14 Shiki
    Removed OS_ERR_INITAUDIOSYSTEM_CHKSUMERR.

    9     01/03/02 14:36 Shiki
    Added OSAudioSystem messages.

    8     01/01/30 10:14 Shiki
    Added OS_ERR_SETABSALARM_HANDLER.

    7     5/17/00 5:35p Shiki
    Added more error messages for OSMutex.c

    6     3/21/00 5:01p Shiki
    Added two more SI error messages.

    5     3/16/00 4:31p Shiki
    Added mutex API assert messages.

    4     3/15/00 6:26p Shiki
    Added thread assert messages.

    3     1/21/00 11:04a Shiki
    Fixed OS_ERR_GETARENAHI_INVALID message.

    2     12/02/99 7:53p Tian
    Locked cache asserts added

    12    9/15/99 4:56p Shiki
    Added OSSerial.c assert messages.

    11    8/02/99 2:35p Shiki
    Added more OSException.c assert messages.

    10    8/02/99 2:17p Shiki
    Added more OSInterrupt.c assert messages.

    9     8/02/99 2:10p Shiki
    Added OSInterrupt.c assert messages.

    8     99/07/28 9:21p Shiki
    Added OSSetPeriodicAlarm() assert messages.

    7     99/07/28 5:56p Shiki
    Added OSAlarm.c assert messages.

    6     99/07/27 4:46p Shiki
    Added OSAddress.c assert messages.

    5     99/07/22 7:31p Shiki
    Added OSArena.c assert messages.

    4     99/07/21 2:54p Shiki
    Added OS.c assert messages.

    3     99/07/16 4:29p Shiki
    Added OSTimer.c assert messages.

    2     99/07/12 9:04p Shiki
    Added OS_ERR_SETERRORHANDLER_INVERR.

    1     6/03/99 6:36p Shiki

    2     5/11/99 10:18a Shiki
    Refreshed include tree.

    1     4/30/99 12:47p Tianli01

    1     4/12/99 7:47p Tianli01
    Changed from OSInit.h, added some private utility functions.

    1     4/06/99 3:07p Tianli01
    Initial checkin.  Private header file with init routines for OS
    modules.
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#ifndef __OSASSERT_H__
#define __OSASSERT_H__

#ifdef __cplusplus
extern "C" {
#endif

/*---------------------------------------------------------------------------*
    Error messages of OS.c
 *---------------------------------------------------------------------------*/
#define OS_ERR_EXCEPTIONINIT_TOOBIG         "OSExceptionInit(): too big exception vector code."
#define OS_ERR_SETEXCEPTIONHANDLER_UNKNOWN  "__OSSetExceptionHandler(): unknown exception."
#define OS_ERR_GETEXCEPTIONHANDLER_UNKNOWN  "__OSGetExceptionHandler(): unknown exception."

/*---------------------------------------------------------------------------*
    Error messages of OSAddress.c
 *---------------------------------------------------------------------------*/
#define OS_ERR_PHYSICALTOCACHED_INVADDR     "OSPhysicalToCached(): illegal address."
#define OS_ERR_PHYSICALTOUNCACHED_INVADDR   "OSPhysicalToUncached(): illegal address."
#define OS_ERR_CACHEDTOPHYSICAL_INVADDR     "OSCachedToPhysical(): illegal address."
#define OS_ERR_UNCACHEDTOPHYSICAL_INVADDR   "OSUncachedToPhysical(): illegal address."
#define OS_ERR_CACHEDTOUNCACHED_INVADDR     "OSCachedToUncached(): illegal address."
#define OS_ERR_UNCACHEDTOCACHED_INVADDR     "OSUncachedToCached(): illegal address."

/*---------------------------------------------------------------------------*
    Error messages of OSAlarm.c
 *---------------------------------------------------------------------------*/
#define OS_ERR_SETALARM_TICK                "OSSetAlarm(): tick was less than zero."
#define OS_ERR_SETALARM_HANDLER             "OSSetAlarm(): null handler was specified."
#define OS_ERR_SETABSALARM_HANDLER          "OSSetAbsAlarm(): null handler was specified."
#define OS_ERR_SETPERIODICALARM_PERIOD      "OSSetPeriodicAlarm(): period was less than zero."
#define OS_ERR_SETPERIODICALARM_HANDLER     "OSSetPeriodicAlarm(): null handler was specified."
#define OS_ERR_CANCELALARMS_INVTAG          "OSCancelAlarms(): invalid tag. (tag zero is used by the operating system.)"

/*---------------------------------------------------------------------------*
    Error messages of OSAlloc.c
 *---------------------------------------------------------------------------*/
#define OS_ERR_ALLOCFROMHEAP_NOHEAP         "OSAllocFromHeap(): heap is not initialized."
#define OS_ERR_ALLOCFROMHEAP_INVSIZE        "OSAllocFromHeap(): invalid size."
#define OS_ERR_ALLOCFROMHEAP_INVHEAP        "OSAllocFromHeap(): invalid heap handle."
#define OS_ERR_ALLOCFROMHEAP_BROKENHEAP     "OSAllocFromHeap(): heap is broken."
#define OS_ERR_ALLOCFIXED_NOHEAP            "OSAllocFixed(): heap is not initialized."
#define OS_ERR_ALLOCFIXED_INVRANGE          "OSAllocFixed(): invalid range."
#define OS_ERR_FREETOHEAP_NOHEAP            "OSFreeToHeap(): heap is not initialized."
#define OS_ERR_FREETOHEAP_INVPTR            "OSFreeToHeap(): invalid pointer."
#define OS_ERR_FREETOHEAP_INVHEAP           "OSFreeToHeap(): invalid heap handle."
#define OS_ERR_SETCURRENTHEAP_NOHEAP        "OSSetCurrentHeap(): heap is not initialized."
#define OS_ERR_SETCURRENTHEAP_INVHEAP       "OSSetCurrentHeap(): invalid heap handle."
#define OS_ERR_INITALLOC_INVNUMHEAPS        "OSInitAlloc(): invalid number of heaps."
#define OS_ERR_INITALLOC_INVRANGE           "OSInitAlloc(): invalid range."
#define OS_ERR_INITALLOC_INSRANGE           "OSInitAlloc(): too small range."
#define OS_ERR_CREATEHEAP_NOHEAP            "OSCreateHeap(): heap is not initialized."
#define OS_ERR_CREATEHEAP_INVRANGE          "OSCreateHeap(): invalid range."
#define OS_ERR_CREATEHEAP_INSRANGE          "OSCreateHeap(): too small range."
#define OS_ERR_DESTROYHEAP_NOHEAP           "OSDestroyHeap(): heap is not initialized."
#define OS_ERR_DESTROYHEAP_INVHEAP          "OSDestroyHeap(): invalid heap handle."
#define OS_ERR_ADDTOHEAP_NOHEAP             "OSAddToHeap(): heap is not initialized."
#define OS_ERR_ADDTOHEAP_INVHEAP            "OSAddToHeap(): invalid heap handle."
#define OS_ERR_ADDTOHEAP_INVRANGE           "OSAddToHeap(): invalid range."
#define OS_ERR_ADDTOHEAP_INSRANGE           "OSAddToHeap(): too small range."
#define OS_ERR_REFERENT_NOHEAP              "OSReferentSize(): heap is not initialized."
#define OS_ERR_REFERENT_INVPTR              "OSReferentSize(): invalid pointer."
#define OS_ERR_DUMPHEAP_NOHEAP              "OSDumpHeap(): heap is not initialized."
#define OS_ERR_DUMPHEAP_INVHEAP             "OSDumpHeap(): invalid heap handle."
#define OS_ERR_DUMPHEAP_BROKENHEAP          "OSDumpHeap(): heap is broken."

/*---------------------------------------------------------------------------*
    Error messages of OSArena.c
 *---------------------------------------------------------------------------*/
#define OS_ERR_GETARENAHI_INIT              "OSGetArenaHi(): OSInit() must be called in advance."
#define OS_ERR_GETARENAHI_INVALID           "OSGetArenaHi(): invalid arena (hi < lo)."
#define OS_ERR_GETARENALO_INIT              "OSGetArenaLo(): OSInit() must be called in advance."
#define OS_ERR_GETARENALO_INVALID           "OSGetArenaLo(): invalid arena (hi < lo)."

/*---------------------------------------------------------------------------*
    Error messages of OSAudioSystem.c
 *---------------------------------------------------------------------------*/
#define OS_ERR_INITAUDIOSYSTEM_ARAMDMAPROGRESS  "__OSInitAudioSystem(): ARAM DMA already in progress"
#define OS_ERR_INITAUDIOSYSTEM_DSPDMAPROGRESS   "__OSInitAudioSystem(): DSP DMA already in progress"
#define OS_ERR_INITAUDIOSYSTEM_DSPWORKING       "__OSInitAudioSystem(): DSP already working"
#define OS_ERR_INITAUDIOSYSTEM_INVALIDMSG       "__OSInitAudioSystem(): DSP returns invalid message"

/*---------------------------------------------------------------------------*
    Error messages of OSError.c
 *---------------------------------------------------------------------------*/
#define OS_ERR_SETERRORHANDLER_INVERR       "OSSetErrorHandler(): unknown error."

/*---------------------------------------------------------------------------*
    Error messages of OSInterrupt.c
 *---------------------------------------------------------------------------*/
#define OS_ERR_SETINTERRUPTHANDLER_INIT     "__OSSetInterruptHandler(): OSInit() must be called in advance."
#define OS_ERR_SETINTERRUPTHANDLER_UNKNOWN  "__OSSetInterruptHandler(): unknown interrupt."
#define OS_ERR_GETINTERRUPTHANDLER_INIT     "__OSGetInterruptHandler(): OSInit() must be called in advance."
#define OS_ERR_GETINTERRUPTHANDLER_UNKNOWN  "__OSGetInterruptHandler(): unknown interrupt."

/*---------------------------------------------------------------------------*
    Error messages of OSSerial.c
 *---------------------------------------------------------------------------*/
#define SI_ERR_TRANSFER_INVCHAN             "SITransfer(): invalid channel."
#define SI_ERR_TRANSFER_INVOUTBYTES         "SITransfer(): output size is out of range (must be 1 to 128)."
#define SI_ERR_TRANSFER_INVINBYTES          "SITransfer(): input size is out of range (must be 1 to 128)."
#define SI_ERR_SYNC_CALLBACK                "SISync(): asynchronous SITransfer() is in progress."
#define SI_ERR_SYNC_NOTRANSFER              "SISync(): no SITransfer() is in progress."
#define SI_ERR_SETCOMMAND_INVCHAN           "SISetCommand(): invalid channel."
#define SI_ERR_GETCOMMAND_INVCHAN           "SIGetCommand(): invalid channel."
#define SI_ERR_SETXY_INVX                   "SISetXY(): x is out of range (8 <= x <= 1023)."
#define SI_ERR_SETXY_INVY                   "SISetXY(): y is out of range (0 <= y <= 255)."
#define SI_ERR_GETRESPONSE_INVCHAN          "SIGetResponse(): invalid channel."
#define SI_ERR_ENABLEPOLLING_INVCHAN        "SIEnablePolling(): invalid chan bit(s)."
#define SI_ERR_DISABLEPOLLING_INVCHAN       "SIDisablePolling(): invalid chan bit(s)."

/*---------------------------------------------------------------------------*
    Error messages of OSThread.c
 *---------------------------------------------------------------------------*/
#define OS_ERR_CREATETHREAD_BADPRIO         "OSCreateThread(): priority out of range (0 <= priority <= 31)."
#define OS_ERR_CREATETHREAD_STILLACTIVE     "OSCreateThread(): thread %p is still active."
#define OS_ERR_EXITTHREAD_NOCURRENT         "OSExitThread(): current thread does not exist."
#define OS_ERR_EXITTHREAD_NOTRUNNING        "OSExitThread(): current thread is not running."
#define OS_ERR_EXITTHREAD_NOTACTIVE         "OSExitThread(): current thread is not active."
#define OS_ERR_CANCELTHREAD_NOTACTIVE       "OSExitThread(): thread %p is not active."
#define OS_ERR_JOINTHREAD_NOTACTIVE         "OSJoinThread(): thread %p is not active."
#define OS_ERR_DETACHTHREAD_NOTACTIVE       "OSDetachThread(): thread %p is not active."
#define OS_ERR_RESUMETHREAD_NOTACTIVE       "OSResumeThread(): thread %p is not active."
#define OS_ERR_RESUMETHREAD_MORIBUND        "OSResumeThread(): thread %p is terminated."
#define OS_ERR_SUSPENDTHREAD_NOTACTIVE      "OSSuspendThread(): thread %p is not active."
#define OS_ERR_SUSPENDTHREAD_MORIBUND       "OSSuspendThread(): thread %p is terminated."
#define OS_ERR_SLEEPTHREAD_NOCURRENT        "OSSleepThread(): current thread does not exist."
#define OS_ERR_SLEEPTHREAD_NOTACTIVE        "OSSleepThread(): current thread %p is not active."
#define OS_ERR_SLEEPTHREAD_NOTRUNNING       "OSSleepThread(): current thread %p is not running."
#define OS_ERR_SLEEPTHREAD_SUSPENDED        "OSSleepThread(): current thread %p is suspended."
#define OS_ERR_SETTHREADPRIORITY_BADPRIO    "OSSetThreadPriority(): priority out of range (0 <= priority <= 31)."
#define OS_ERR_SETTHREADPRIORITY_NOTACTIVE  "OSSetThreadPriority(): thread %p is not active."
#define OS_ERR_SETTHREADPRIORITY_MORIBUND   "OSSetThreadPriority(): thread %p is terminated."

/*---------------------------------------------------------------------------*
    Error messages of OSTimer.c
 *---------------------------------------------------------------------------*/
#define OS_ERR_SETTIMERCALLBACK_INIT        "OSSetTimerCallback(): timer is not initialized."
#define OS_ERR_INITTIMER_INVTIME            "OSInitTimer(): time param must be less than 0x80000000."
#define OS_ERR_STARTTIMER_INIT              "OSStartTimer(): timer is not initialized."
#define OS_ERR_STOPTIMER_INIT               "OSStopTimer(): timer is not initialized."

/*---------------------------------------------------------------------------*
    Error messages of OSMutex.c
 *---------------------------------------------------------------------------*/
#define OS_ERR_LOCKMUTEX_NOCURRENT          "OSLockMutex(): current thread does not exist."
#define OS_ERR_LOCKMUTEX_NOTRUNNING         "OSLockMutex(): current thread is not running."
#define OS_ERR_LOCKMUTEX_DEADLOCK           "OSLockMutex(): detected deadlock: current thread %p, mutex %p."
#define OS_ERR_UNLOCKMUTEX_NOCURRENT        "OSUnlockMutex(): current thread does not exist."
#define OS_ERR_UNLOCKMUTEX_NOTRUNNING       "OSUnlockMutex(): current thread is not running."
#define OS_ERR_UNLOCKMUTEX_NOTOWNER         "OSUnlockMutex(): current thread %p is not the owner of mutex %p."
#define OS_ERR_TRYLOCKMUTEX_NOCURRENT       "OSTryLockMutex(): current thread does not exist."
#define OS_ERR_TRYLOCKMUTEX_NOTRUNNING      "OSTryLockMutex(): current thread is not running."
#define OS_ERR_WAITCOND_NOCURRENT           "OSWaitCond(): current thread does not exist."
#define OS_ERR_WAITCOND_NOTRUNNING          "OSWaitCond(): current thread is not running."
#define OS_ERR_WAITCOND_NOTOWNER            "OSWaitCond(): current thread %p is not the owner of mutex %p."

/*---------------------------------------------------------------------------*
    Locked cache error messages
 *---------------------------------------------------------------------------*/
#define LC_ERR_ALLOC_ADDR                   "LCAlloc(): addr must be 32 byte aligned"
#define LC_ERR_ALLOC_NBYTES                 "LCAlloc(): nBytes must be 32 byte aligned"
#define LC_ERR_ALLOCNF_ADDR                 "LCAllocNoFlush(): addr must be 32 byte aligned"
#define LC_ERR_ALLOCNF_NBYTES               "LCAllocNoFlush(): nBytes must be 32 byte aligned"

#define LC_ERR_FREE_ADDR                    "LCFree(): addr must be 32 byte aligned"
#define LC_ERR_FREE_NBYTES                  "LCFree(): nBytes must be 32 byte aligned"

#define LC_ERR_LOAD_SRCADDR                 "LCLoadData(): srcAddr not 32 byte aligned"
#define LC_ERR_LOAD_DESTADDR                "LCLoadData(): destAddr not 32 byte aligned"
#define LC_ERR_LOAD_NBYTES                  "LCLoadData(): nBytes not 32 byte aligned"

#define LC_ERR_STORE_SRCADDR                "LCStoreData(): srcAddr not 32 byte aligned"
#define LC_ERR_STORE_DESTADDR               "LCStoreData(): destAddr not 32 byte aligned"
#define LC_ERR_STORE_NBYTES                 "LCStoreData(): nBytes not 32 byte aligned"


#ifdef __cplusplus
}
#endif

#endif  // __OSPRIVATE_H__
