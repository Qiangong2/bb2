/*---------------------------------------------------------------------------*
  Project:  ARAM manager for AX Demos
  File:     am.c

  Copyright 1998, 1999 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: am.c,v $
  Revision 1.1.1.1  2004/06/09 17:39:20  paulm
  GC library source from Nintendo SDK

    
    5     02/08/26 10:00 Hashida
    Deleted __AM_arq_poll_flag that was not used.
    
    4     02/08/22 22:18 Hashida
    Corrected a comment (256 bytes -> /AM_ZEROBUFFER_BYTES/ bytes).
    
    3     8/19/02 7:14p Eugene
    Made polling flag volatile. Ooops. Moved ZERO buffer onto stack, ensure
    that it's 32-byte aligned. Ooops oops. 
    
    2     9/05/01 12:42a Eugene
    Added ARQ poll flags to each stack entry's "read info" structure.
    
    1     9/04/01 12:01a Eugene
    ARAM Manager for ARAM. Demo library.
    
    1     8/29/01 12:03p Billyjack
    created
    
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

 
 
/*---------------------------------------------------------------------------*
 * Includes
 *---------------------------------------------------------------------------*/

#include <demo.h>
#include <dolphin.h>
#include <dolphin/am.h>


/*---------------------------------------------------------------------------*
 * Local definitions
 *---------------------------------------------------------------------------*/

typedef struct 
{
    DVDFileInfo file_handle;   // file handle
    ARQRequest  arq_handle;    // ARQ request handle
    AMCallback  callback;      // callback for completion of data transfer

    char *path;                // pointer to string containing file path
    void *buffer;              // pointer to 'stream buffer'

    volatile u32   file_length;         // actual length of file, in bytes
    volatile u32   curr_read_offset;    // current read offset in file, in bytes

    volatile u32   read_length;         // size of each read from file (rounded down to 32bytes)

    volatile u32   aram_start_addr;     // start address in ARAM
    volatile u32   curr_aram_offset;    // current write offset in ARAM

    volatile BOOL  poll_flag;           // for synchronuous ARQ polling

} __AMREADINFO;

/*---------------------------------------------------------------------------*
 * Module Data
 *---------------------------------------------------------------------------*/


static u32              __AMStackPointer[AM_STACK_ENTRIES];
static u32              __AMStackLocation;
static u32              __AMFreeBytes;

volatile static u32     __AMPendingReads=0;

static __AMREADINFO     __AMReadInfo[AM_STACK_ENTRIES];


/*---------------------------------------------------------------------------*
 * Local macros
 *---------------------------------------------------------------------------*/

#define mROUNDUP32(x)      (((u32)(x) + 32 - 1) & ~(32 - 1))

/*---------------------------------------------------------------------------*
 * Local Prototypes
 *---------------------------------------------------------------------------*/

static void __AM_arq_callback       (u32 task);
static void __AM_arq_poll_callback  (u32 task);
static void __AM_dvd_callback       (s32 result, DVDFileInfo *handle);


/*===========================================================================*
 *                   F U N C T I O N    D E F I N I T I O N S
 *===========================================================================*/


/*---------------------------------------------------------------------------*
 * Name        : __AM_dvd_callback()
 *
 * Description : Callback for DVDReadAsync(). To be used by AMPushBufferedAsync() 
 *               only!!!
 *
 * Arguments   : 
 *
 * Returns     : None.
 *---------------------------------------------------------------------------*/

static void __AM_dvd_callback(s32 result, DVDFileInfo *handle)
{

    u32 i;
    __AMREADINFO *ptr;


        // find which read-info structure is associated with this handle
        for (i=0; i<AM_STACK_ENTRIES; i++)
        {
            if (handle == &(__AMReadInfo[i].file_handle))
            {
                ptr = &__AMReadInfo[i];
                break;
            }
        }

        // if we didn't find one, then freak out
        ASSERT(i != AM_STACK_ENTRIES);


        // is everything ok?
        if (DVD_STATE_END == DVDGetFileInfoStatus(&ptr->file_handle))
        {

            ARQPostRequest(
                &(ptr->arq_handle),
                i, 
                ARQ_TYPE_MRAM_TO_ARAM, 
                ARQ_PRIORITY_HIGH, 
                (u32)(ptr->buffer),
                ptr->curr_aram_offset, 
                (u32)result,
                __AM_arq_callback
                );

            ptr->curr_aram_offset += result;    // increment ARAM pointer
            ptr->curr_read_offset += result;    // increment file pointer

        }
        else
        {

            // LATER, add some nice error-handling or something.
        }


} // end __AM_dvd_callback()


/*---------------------------------------------------------------------------*
 * Name        : __AM_arq_callback()
 *
 * Description : ARQPostRequest() callback. To be used by AMPushBufferedAsync() 
 *               only.
 *
 * Arguments   : 
 *
 * Returns     : None.
 *---------------------------------------------------------------------------*/

static void __AM_arq_callback(u32 task)
{

    u32 i;
    __AMREADINFO *ptr;


        // find the parent read-info structure
        for (i=0; i<AM_STACK_ENTRIES; i++)
        {

            if ( (ARQRequest *)task == &(__AMReadInfo[i].arq_handle))
            {
                ptr = &__AMReadInfo[i];
                break;
            }
        }

        // if we didn't find one, then freak out
        ASSERT(i != AM_STACK_ENTRIES);

        if (ptr->curr_read_offset < ptr->file_length)
        {

            u32 remainder;
            u32 read_request_length;

                remainder = ptr->file_length - ptr->curr_read_offset;
                read_request_length = mROUNDUP32((ptr->read_length > remainder) ? remainder : ptr->read_length);

                // ok! start reading! 
                DVDReadAsync(&(ptr->file_handle),               // file handle
                               ptr->buffer,                     // buffer
                               (s32)(read_request_length),      // length of buffer, rounded down to 32-byte multiple
                               (s32)(ptr->curr_read_offset),    // offset (start at beginning of file
                               __AM_dvd_callback                // callback
                               );
        }
        else
        {
            // we're done! 
            ASSERTMSG(__AMPendingReads, "AMPushBuffered(): Dangling read-complete event!\n");
            
            __AMPendingReads--;
            
            
            if (ptr->callback)
            {
                (*(ptr->callback))(ptr->path);
            }

        }


} // end __AM_arq_callback()

/*---------------------------------------------------------------------------*
 * Name        : __AM_arq_poll_callback()
 *
 * Description : For synchronuous ARQ requests only.
 *
 * Arguments   : 
 *
 * Returns     : None.
 *---------------------------------------------------------------------------*/
static void __AM_arq_poll_callback(u32 task)
{
    u32 i;
    __AMREADINFO *ptr;


        // find the parent read-info structure
        for (i=0; i<AM_STACK_ENTRIES; i++)
        {

            if ( (ARQRequest *)task == &(__AMReadInfo[i].arq_handle))
            {
                ptr = &__AMReadInfo[i];
                break;
            }
        }

        // if we didn't find one, then freak out
        ASSERT(i != AM_STACK_ENTRIES);


    ptr->poll_flag = TRUE;

} // end __AM_arq_poll_callback()



/*---------------------------------------------------------------------------*
 * Name        : 
 * Description : 
 * Arguments   : 
 * Returns     : 
 *---------------------------------------------------------------------------*/
void *AMLoadFile(char *path, u32 *length)
{
    DVDFileInfo dvdFileInfo;
    u32         roundLength;
    s32         readLength;
    void        *buffer;
    int         old;

        if (!DVDOpen(path, &dvdFileInfo))
        {
            char ch[1024];

                sprintf(ch, "Cannot open %s", path);
                ASSERTMSG(0, ch);
        }

        // make sure file length is not 0
        ASSERTMSG(DVDGetLength(&dvdFileInfo), "File length is 0\n");

        roundLength= mROUNDUP32(DVDGetLength(&dvdFileInfo));

        old = OSDisableInterrupts();
        buffer = OSAlloc(roundLength);
        OSRestoreInterrupts(old);

        // make sure we got a buffer
        ASSERTMSG(buffer, "Unable to allocate buffer\n");

        readLength = DVDRead(&dvdFileInfo, buffer, (s32)(roundLength), 0);
        // make sure we read the file correctly
        ASSERTMSG(readLength > 0, "Read length <= 0\n");

        if (length) 
        {
            *length = roundLength;
        }

        return(buffer);

} // AMLoadFile()

/*---------------------------------------------------------------------------*
 * Name        : AMPush()
 *
 * Description : Loads data from specified file into main memory, then 
 *               DMAs the data into ARAM. Note that this function will 
 *               dynamically allocate main memory, then free it! 
 *
 * Arguments   : filepath
 *
 * Returns     : Address of data pushed into ARAM, zero if failure
 *---------------------------------------------------------------------------*/
u32 AMPush(char *path)
{

    DVDFileInfo handle;

    void *buffer;
    u32   buffer_length;
    u32   ret_val;
    BOOL  old;


        if (!DVDOpen(path, &handle))
        {
            OSReport("AMPushBuffered(): Unable to open file '%s'\n", path);
            OSHalt("AM: FATAL ERROR\n");
        }

        buffer_length = mROUNDUP32(DVDGetLength(&handle));
        if (buffer_length)
        {

            // allocate memory for the entire file
            old = OSDisableInterrupts();
            buffer = OSAlloc(buffer_length);
            OSRestoreInterrupts(old);

            // freak out if OSAlloc() fails
            ASSERTMSG(buffer, "AMPush(): Memory allocation failure.\n");

            // now invoke __AMPushBuffered() with a buffer large enough to transfer the data in just one pass
            ret_val = __AMPushBuffered(path, buffer, buffer_length, NULL, FALSE);

            // free the buffer
            OSFree(buffer);

            return(ret_val);

        }
        else
        {

#ifdef _DEBUG
            OSReport("AMPush(): WARNING: File has zero length.\n");
#endif
            return(0);
        }

} // AMPush()

/*---------------------------------------------------------------------------*
 * Name        : AMPushData()
 *
 * Description : Transfers data from main memory into ARAM stack. 
 *
 * Arguments   : pointer to buffer (must be 32-byte aligned)
 *               length (must be multiple of 32 bytes)
 *
 * Returns     : Address of data pushed into ARAM
 *---------------------------------------------------------------------------*/


u32 AMPushData(void *buffer, u32 length)
{

    BOOL  old;
    u32   round_length;

    __AMREADINFO *ptr;



        // ensure that buffer is 32-byte aligned
        ASSERTMSG( !(((u32)buffer) & 0x1F), "AMPushData(): buffer is not 32-byte aligned!\n");

        // ensure non-zero length
        ASSERT(length);

        round_length = mROUNDUP32(length);
        
        if ((__AMFreeBytes >= round_length) && (__AMStackLocation < (AM_STACK_ENTRIES - 1)))
        {

            // grab the READINFO structure associated with this stack entry
            ptr = &__AMReadInfo[__AMStackLocation];

            // push the stack
            old = OSDisableInterrupts();

            ptr->aram_start_addr = __AMStackPointer[__AMStackLocation];

       
            __AMStackLocation++;
            __AMStackPointer[__AMStackLocation] = ptr->aram_start_addr + round_length;
            __AMFreeBytes -= round_length;



            ptr->poll_flag = FALSE;

            // post a ARQ request
            ARQPostRequest(
                &(ptr->arq_handle),
                __AMStackLocation - 1,       
                ARQ_TYPE_MRAM_TO_ARAM,
                ARQ_PRIORITY_HIGH,
                (u32)buffer,
                ptr->aram_start_addr,
                round_length,
                &__AM_arq_poll_callback
                );

            OSRestoreInterrupts(old);        


            while(FALSE == ptr->poll_flag)
            {
                // do nothing
            }

            return(ptr->aram_start_addr);
        }

        return(0);


} // AMPushData()

/*---------------------------------------------------------------------------*
 * Name        : __AMPushBuffered()
 *
 * Description : Loads data from disc and DMAs it into ARAM, one chunk at a time.
 *               
 * Arguments   : None.
 *
 * Returns     : None.
 *---------------------------------------------------------------------------*/

u32 __AMPushBuffered(char *path, void *buffer, u32 buffer_size, AMCallback callback, BOOL async_flag)
{

    BOOL old;
    u32  round_file_length;
    u32  stack_index;

    __AMREADINFO *ptr;


        // ensure that buffer is 32-byte aligned
        ASSERTMSG( !(((u32)buffer) & 0x1F), "AMPushBuffered(): buffer is not 32-byte aligned!\n");

        // ensure that buffer size is at least 32-bytes in length (will be rounded down, BTW)
        ASSERTMSG( (buffer_size > 31), "AMPushBuffered(): buffer_size is less than 32 bytes!\n");


        // any slots left on the stack?
        if (__AMStackLocation < (AM_STACK_ENTRIES-1))
        {
            // grab an info entry
            ptr = &__AMReadInfo[__AMStackLocation];
            stack_index = __AMStackLocation;

            // can we open the file? 
            if (!DVDOpen(path, &(ptr->file_handle)))
            {
                OSReport("AMPushBuffered(): Unable to open file '%s'\n", path);
                OSHalt("AM: FATAL ERROR\n");
            }

            ptr->file_length  = DVDGetLength(&(ptr->file_handle));
            round_file_length = mROUNDUP32(ptr->file_length);


            // enough space left in ARAM? 
            if (__AMFreeBytes >= round_file_length)
            {

                ptr->aram_start_addr = __AMStackPointer[__AMStackLocation];

                old = OSDisableInterrupts();

                __AMStackLocation++;
                __AMStackPointer[__AMStackLocation] = ptr->aram_start_addr + round_file_length;
                __AMFreeBytes -= round_file_length;

                // initialize remainder of read-info structure
                ptr->curr_read_offset = 0;                      // read offset in file
                ptr->curr_aram_offset = ptr->aram_start_addr;   // curr location in ARAM
                
                ptr->read_length = buffer_size & (~0x1F);       // round buffer size down to multiple of 32 bytes
                ptr->callback    = callback;                    // callback when stream is complete
                ptr->path        = path;
                ptr->buffer      = buffer;


                OSRestoreInterrupts(old);


                if (TRUE == async_flag)
                {
                    // ok! start reading! 
                    DVDReadAsync(&(ptr->file_handle),               // file handle
                                   ptr->buffer,                     // buffer
                                   (s32)(ptr->read_length),         // length of buffer, rounded down to 32-byte multiple
                                   0,                               // offset (start at beginning of file
                                   __AM_dvd_callback                // callback
                                   );

                    __AMPendingReads++;
                }
                else
                {


                    while(ptr->curr_read_offset < ptr->file_length)
                    {
                        u32 remainder;
                        u32 read_request_length;
                        s32 actual_read_length;


                            remainder = ptr->file_length - ptr->curr_read_offset;
                            read_request_length = mROUNDUP32((ptr->read_length > remainder) ? remainder : ptr->read_length);

                            
                            actual_read_length = DVDRead(&(ptr->file_handle), 
                                                           ptr->buffer,  
                                                           (s32)read_request_length, 
                                                           (s32)ptr->curr_read_offset);

                            ASSERTMSG((actual_read_length > 0), "AMPushBuffered(): Fatal Error - synchronuous DVD read\n");

                            ptr->curr_read_offset += (u32)(actual_read_length);

                            ptr->poll_flag = FALSE;

                            // post a ARQ request
                            ARQPostRequest(
                                &(ptr->arq_handle),
                                stack_index, 
                                ARQ_TYPE_MRAM_TO_ARAM, 
                                ARQ_PRIORITY_HIGH, 
                                (u32)(ptr->buffer),
                                ptr->curr_aram_offset, 
                                (u32)actual_read_length,
                                __AM_arq_poll_callback
                                );

                            ptr->curr_aram_offset += (u32)(actual_read_length);

                            while (FALSE == (ptr->poll_flag))
                            {
                                // do nothing!
                            }


                    } // while....(for synchronuous mode)



                } // sync/async check

               

            } // enough space left?
            else
            {

#ifdef _DEBUG
                OSReport("AMPushBuffered(): WARNING: Not enough space in ARAM.\n");
#endif
                return(0);
            }

        } // if slots left on the stack
        else
        {
#ifdef _DEBUG
            OSReport("AMPushBuffered(): WARNING: Stack table is full.\n");
#endif
            return(0);
        }

        return(ptr->aram_start_addr);

} // end AMPushBuffered()


/*---------------------------------------------------------------------------*
 * Name        : AMPop()
 *
 * Description : Removes last entity pushed into ARAM audio block. 
 *
 * Arguments   : None.
 *
 * Returns     : None.
 *---------------------------------------------------------------------------*/

void AMPop(void)
{
    BOOL old;


        old = OSDisableInterrupts();


        if (0 == __AMPendingReads)
        {

            // do not pop the zero buffer off the stack, ever
            if (__AMStackLocation > 1)
            {
                __AMFreeBytes += __AMStackPointer[__AMStackLocation] -
                                    __AMStackPointer[__AMStackLocation - 1];

                __AMStackLocation--;
            }

        }

        OSRestoreInterrupts(old);

} // AMPop()


/*---------------------------------------------------------------------------*
 * Name        : AMGetZeroBuffer()
 *
 * Description : Return ARAM address of 'zero buffer'
 *
 * Arguments   : None.
 *
 * Returns     : Address.
 *---------------------------------------------------------------------------*/
u32 AMGetZeroBuffer(void)
{
    return(__AMStackPointer[0]);
}

/*---------------------------------------------------------------------------*
 * Name        : AMGetReadStatus()
 *
 * Description : Returns zero if no reads are pending, otherwise returns 
 *               number of pending reads.
 *
 * Arguments   : None.
 *
 * Returns     : See description.
 *---------------------------------------------------------------------------*/
u32 AMGetReadStatus(void)
{
    BOOL old;
    u32  tmp;

        old = OSDisableInterrupts();
        tmp = __AMPendingReads;
        OSRestoreInterrupts(old);

        return(tmp);

}

/*---------------------------------------------------------------------------*
 * Name        : AMGetFreeSize()
 *
 * Description : Returns remaining number of free bytes on stack.
 *
 * Arguments   : None.
 *
 * Returns     : See description.
 *---------------------------------------------------------------------------*/
u32 AMGetFreeSize(void)
{
    BOOL old;
    u32  tmp;

        old = OSDisableInterrupts();
        tmp = __AMFreeBytes;
        OSRestoreInterrupts(old);

        return(tmp);

} // end AMGetFreeSize()

/*---------------------------------------------------------------------------*
 * Name        : AMGetFreeSize()
 *
 * Description : Returns remaining number of free bytes on stack.
 *
 * Arguments   : None.
 *
 * Returns     : See description.
 *---------------------------------------------------------------------------*/
u32 AMGetStackPointer(void)
{
    BOOL old;
    u32  tmp;

        old = OSDisableInterrupts();
        tmp = __AMStackPointer[__AMStackLocation];
        OSRestoreInterrupts(old);

        return(tmp);

} // end AMGetFreeSize()

  /*---------------------------------------------------------------------------*
 * Name        : AMInit()
 *
 * Description : Initialize the given block of ARAM for use with audio.
 *
 * Arguments   : ARAM start address, size of block in bytes.
 *
 * Returns     : None.
 *
 * Initializes a block in ARAM for use with audio. Creates a 'zero buffer' at 
 * the bottom of the given block. 
 *
 *---------------------------------------------------------------------------*/

// static u8 __AMZeroBuffer[256] ATTRIBUTE_ALIGN(32);


void AMInit(u32 aramBase, u32 aramBytes)
{
    u32 i;

    // instantiate on stack
      u8 __AMZeroBuffer[AM_ZEROBUFFER_BYTES + 31];
      u8 *ptr;


        ASSERT(aramBytes);

        // ensure that buffer is on 32-byte boundary
        ptr = (u8 *)(mROUNDUP32((u32)(__AMZeroBuffer)));
    
        // initialize internal vars
        __AMStackLocation   = 0;
        __AMStackPointer[0] = aramBase;
        __AMFreeBytes       = aramBytes;
        __AMPendingReads    = 0;

        // put /AM_ZEROBUFFER_BYTES/ bytes of zeros at base
        for (i = 0; i < AM_ZEROBUFFER_BYTES; i++)
        {
            ptr[i] = 0;
        }

        AMPushData((void *)ptr, AM_ZEROBUFFER_BYTES);

}



