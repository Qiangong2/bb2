/*---------------------------------------------------------------------------*
 *       N I N T E N D O  C O N F I D E N T I A L  P R O P R I E T A R Y
 *---------------------------------------------------------------------------*

  Project: Dolphin OS - Audio Interface (AI) Device Driver
  File:    ai.c

  Copyright 1999-2002 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: ai.c,v $
  Revision 1.1.1.1  2004/01/03 01:02:28  paulm
  Gamecube library source from Nintendo

    
    29    02/08/09 11:00a Akagi
    Added __AID_Active, which checks previous __AID_Callback is still
    active or not.
    
    28    8/07/02 21:45 Shiki
    Defined DOLPHIN_LIB_VERSION() and registered the library with
    OSRegisterVersion().

    27    11/09/01 1:54p Eugene
    Corrected stack-alignment check for (optional) stack parameter of
    AIInit().

    26    5/06/01 11:07p Eugene

    25    5/04/01 1:36p Eugene
    AISetStreamSampleRate() revised to allow setting of 48KHz, but not
    32KHz.

    24    5/01/01 6:43p Eugene
    Added AIReset(), which clear init flag.
    AIInit() now returns if init flag is asserted.

    23    4/30/01 6:37p Eugene
    Added AICheckInit() function for determining whether or not AI has been
    initialized. Removed ability to change DVD stream sampling rate.
    Function remains for backwards compatibility, but prints message to
    terminal in debug builds. Note that an internal function,
    __AI_set_stream_sample_rate(), has been retained because we need it to
    synchronize AI clocks to fix the 32KHz  hardware SRC bug.

    22    3/25/01 9:37p Eugene
    Added AIGetDMAStartAddr() and AIGetDMALength() to retrieve most recent
    parameters programmed into AI-FIFO DMA engine.

    21    2/15/01 7:05p Tian
    Fixed bug in __AISHandler.  It was clearing the wrong context when
    returning to __OSDispatchInterrupt, thereby trashing the real register
    context.

    20    12/08/00 10:51p Eugene
    Callback registration functions now return previous callback (if any)

    19    12/06/00 9:18p Eugene
    Made AI MINNOW and SPRUCE platform friendly

    18    12/04/00 7:25p Eugene
    Optimizations for RevC and later.

    17    11/23/00 10:21p Eugene
    Added time logging of __AI_SRC_INIT() for debug builds. Revised
    AISetStreamSampleRate() so that play state is restored separately, via
    AISetStreamPlayState() --> SRC breaks for some reason others.
    Investigation is pending.

    16    11/23/00 8:07p Eugene
    Support for RevB changes:

      AIGetDSPSampleRate() & AISetDSPSampleRate()

    These functions manipulate the DSP's hardware SRC for
    AI_SAMPLERATE_32KHZ and AI_SAMPLERATE_48KHZ output.

    Also includes major revisions to work around hardware SRC bugs.

    15    6/12/00 1:54a Eugene
    Got rid of some warnings.

    14    6/12/00 1:36a Eugene
    Fixed interrupt-clear-clobber problem. Also made all other accesses to
    DSP_CDCR_IDX "Safe".

    13    5/31/00 4:20p Eugene
    Fixed AIGetSampleCount(). Was incorrectly using FDL-based macro
    to read the register.

    12    5/30/00 2:01p Eugene

    11    5/27/00 3:32a Eugene
    Verified that L/R mapping of AIS volume controls is correct.
    Don't muck with them!

    10    5/25/00 1:28a Eugene
    Added debug messages and ASSERTMSG() for testing.

    9     5/23/00 4:02p Eugene
    Fixed comments.

    8     5/23/00 2:27p Eugene
    AIStartDMA() and AIStopDMA() must enable DMA in AIBL register, NOT
    AIBLC register. Ooops.

    7     5/01/00 6:22p Eugene
    Added interrupt disable/restore around callback registration.
    Separated AIStartDMA() into AIInitDMA() and AIStartDMA().
    - AIInitDMA() programs control registers, does not enable DMA
    - AIStartDMA() now simply asserts the DMA enable bit
    ==> this allows the AI-FIFO callback to program the NEXT DMA
    transaction without restarting the DMA everytime (a bug!)


    6     4/11/00 5:32p Eugene
    Myriad fixes to API. Changed AI-FIFO DMA length parameter to accept
    bytes instead of blocks. Changed stream play state and rate parameters
    to u32 for convenience when manipulating the AI registers (which are 32
    bit).

    5     3/10/00 6:32p Eugene

    4     3/08/00 6:27p Tian
    Added stack alignment restriction in AIInit.  Also inserted stack frame
    before invoking AID callback.  Fixed interrupt handlers to not call
    OSLoadContext.

    3     3/08/00 2:55p Tian
    Added stack argument to AIInit.  If non-NULL, DMA callback will use
    that stack.

    2     3/07/00 8:02p Eugene

    1     3/07/00 6:52p Eugene
    Audio Interface (AI) driver.

    Includes API for:
    - DVD direct streaming sample counter
    - AI-FIFO DMA

    Note that DVD direct streaming and ARAM APIs are separate entities.
 *---------------------------------------------------------------------------*/

#include <dolphin/doldefs.h>
DOLPHIN_LIB_VERSION(AI);
/* $NoKeywords: $ */

/*---------------------------------------------------------------------------*
 * Includes
 *---------------------------------------------------------------------------*/

#include <stdlib.h>
#include <stddef.h>

#include <dolphin/types.h>
#include <private/flipper.h>
#include <dolphin/os.h>

#include <dolphin/ai.h>

#include "AIAssert.h"

/*---------------------------------------------------------------------------*
 * Local types
 *---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*
 * Local definitions/declarations
 *---------------------------------------------------------------------------*/

#define DSP_WORD_SHIFT      16
#define DSP_WORD_MASK       0xffff

#define DSP_AIMA_MASK_HI    0x03ff
#define DSP_AIMA_MASK_LO    0xffe0

static  AISCallback         __AIS_Callback; // AI Streaming callback
static  AIDCallback         __AID_Callback; // AI-FIFO DMA callback
static  u8*                 __CallbackStack;// stack for extended handlers
                                            // note that stacks grow DOWN,
                                            // so this must be the HIGH
                                            // address of the stack

static  u8*                 __OldStack;     // stack of context interrupted by
                                            // interrupt.
// For AICheckInit()
static volatile BOOL        __AI_init_flag = FALSE;

// a flag for checking previous __AID_Callback is still acitve or not.
static volatile BOOL        __AID_Active = FALSE;


// Private Functions
static void __AISHandler                (__OSInterrupt interrupt, OSContext *context);
static void __AIDHandler                (__OSInterrupt interrupt, OSContext *context);
static void __AICallbackStackSwitch     (register void *cb);
static void __AI_SRC_INIT               (void);
static void __AI_set_stream_sample_rate (u32 rate);


/*---------------------------------------------------------------------------*
 * Debugging tools for AI SRC hardware bug
 *---------------------------------------------------------------------------*/
typedef struct STRUCT_TIMELOG
{
    OSTime t_start;
    OSTime t1;
    OSTime t2;
    OSTime t3;
    OSTime t4;
    OSTime t_end;

} sTIMELOG;


OSTime __ai_src_time_start;
OSTime __ai_src_time_end;

// OSTime __ai_src_get_time();

sTIMELOG profile;

sTIMELOG *__ai_src_get_time();

/*---------------------------------------------------------------------------*
 * Globals
 *---------------------------------------------------------------------------*/

/*===========================================================================*
 *                   F U N C T I O N    D E F I N I T I O N S
 *===========================================================================*/


/*---------------------------------------------------------------------------*
 * AI-FIFO DMA Controls
 *---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*
 * Name        : AIRegisterDMACallback()
 *
 * Description : Registers the given function as the AIDINT interrupt callback.
 *
 * Arguments   : AIDCallback *callback
 *
 * Returns     : Returns the previously defined callback (NULL if there isn't
 *               one).
 *
 *---------------------------------------------------------------------------*/
AIDCallback AIRegisterDMACallback(AIDCallback callback)
{

    AIDCallback old_callback;
    BOOL        old;

        old_callback = __AID_Callback;

        old = OSDisableInterrupts();

        __AID_Callback = callback;

        OSRestoreInterrupts(old);

        return(old_callback);

} /* end of AIRegsiterDMACallback() */

/*---------------------------------------------------------------------------*
 * Name        : AIInitDMA()
 *
 * Description : Programs the AI-FIFO DMA controller with the parameters of
 *               the next DMA transaction.
 *
 * Arguments   : u32 start_addr.....main memory starting address
 *               u32 length.........number bytes to transfer
 *
 * Returns     : None.
 *
 * - Programs main memory start address and block length into AI FIFO DMA
 *   controller, but does NOT enable the DMA.
 *
 * - Note that the start address is 32 bits long. However, the top six bits
 *   and the bottom five bits will be ignored by the DMA controller.
 *
 * - The length parameter is in bytes, but must be a multiple of 32!!!!!
 *
 * - Note that this function does NOT check for DMA already in progress.
 *   The parameters for the current transaction (if any) are stored within
 *   internal registers. Writing to the (external) control registers effectively
 *   specifies the parameters for the NEXT transaction.
 *
 * - We are not using the ArtX macros because they are broken:
 *   1. The DSP registers are 16-bits wide, but the macros treat them as 32bit.
 *   2. The macros assume that addresses are in units of 32bytes, but most
 *      likely users will be handling byte-referencing addresses.
 *
 *---------------------------------------------------------------------------*/

void AIInitDMA(u32 start_addr, u32 length)
{

    BOOL old;


        old = OSDisableInterrupts();

        // Program start address into DMA controller
        __DSPRegs[DSP_AIMA_IDX]   = (u16)((__DSPRegs[DSP_AIMA_IDX] & ~DSP_AIMA_MASK_HI) | (start_addr >> DSP_WORD_SHIFT));
        __DSPRegs[DSP_AIMA_IDX+1] = (u16)((__DSPRegs[DSP_AIMA_IDX + 1] & ~DSP_AIMA_MASK_LO) | (DSP_WORD_MASK & start_addr));


        // length must be in bytes, and multiple of 32
        ASSERTMSG((u32)(length % 32) == 0, AI_ERR_AISTARTDMA_LENGTH);

        // Program block length into DMA controller - Note that this does NOT clear the ENABLEDMA bit
        __DSPRegs[DSP_AIBL_IDX]   = (u16)((__DSPRegs[DSP_AIBL_IDX] & ~DSP_AIBL_BLOCKLEN_MASK) | (u16)((length >> 5) & 0xffff));

        OSRestoreInterrupts(old);


} /* end of StartDMA() */
/*---------------------------------------------------------------------------*
 * Name        : AIGetDMAEnableFlag()
 *
 * Description : Retrieves current state of DMA Enable flag. In case you're curious.
 *
 * Arguments   : None.
 *
 * Returns     : 0x01 if DMA is enabled
 *               0x00 if DMA is differently-abled
 *
 *---------------------------------------------------------------------------*/
BOOL AIGetDMAEnableFlag(void)
{

    return((u8)((__DSPRegs[DSP_AIBL_IDX] & DSP_AIBL_ENABLEDMA_MASK) >> DSP_AIBL_ENABLEDMA_SHIFT));

} /* end of AIGetDMAEnableFlag() */


/*---------------------------------------------------------------------------*
 * Name        : AIStartDMA()
 *
 * Description : Enables the DMA from main memory to the AI FIFO.
 *
 * Arguments   : None.
 *
 * Returns     : None.
 *
 * Simply asserts the enable bit in the length register, thereby initiating
 * the DMA. Here's what happens:
 *
 * AI-FIFO loads mainmem address and length from DMA control registers and
 * stores them internally. AI-FIFO asserts AI-FIFO DMA interrupt.
 *
 * The application's callback for this interrupt must be ready to program
 * the control registers with the parameters of the next DMA.
 *
 *---------------------------------------------------------------------------*/

void AIStartDMA(void)
{

    // Enable DMA!
    __DSPRegs[DSP_AIBL_IDX]  = (u16)((__DSPRegs[DSP_AIBL_IDX] | (u16)(DSP_AIBL_ENABLEDMA_MASK)));


} /* end of StartDMA() */


  /*---------------------------------------------------------------------------*
 * Name        : AIStopDMA()
 *
 * Description : Clears AI-FIFO DMA enable flag (for you control-freaks)
 *
 * Arguments   : None.
 *
 * Returns     : None.
 *
 * Note that clearing the enable bit will also clear the block length counter
 * and effectively cancels the current transfer.
 *
 *---------------------------------------------------------------------------*/

void AIStopDMA(void)
{
    // Disable DMA!
    __DSPRegs[DSP_AIBL_IDX] = (u16)(__DSPRegs[DSP_AIBL_IDX] & ~DSP_AIBL_ENABLEDMA_MASK);

}

/*---------------------------------------------------------------------------*
 * Name        : AIGetDMABytesLeft()
 *
 * Description : Retrieves current value of DMA block counter
 *
 * Arguments   : None.
 *
 * Returns     : u32 bytes....number of bytes remaining in pending DMA, if
 *                            any.
 *
 *
 *---------------------------------------------------------------------------*/

u32  AIGetDMABytesLeft(void)
{
    // the AIBLC register stores the number in 32-byte blocks, so shift
    // value to get bytes
    return((u32)(__DSPRegs[DSP_AIBLC_IDX] & DSP_AIBLC_BLOCKLEN_MASK) << 5);

}


/*---------------------------------------------------------------------------*
 * Name        : AIGetDMAStartAddr()
 *
 * Description : Retrieves main memory start address of NEXT block to be DMA'd
 *
 * Arguments   : None.
 *
 * Returns     : u32 bytes....main mem address of next block to be DMA'd
 *
 *---------------------------------------------------------------------------*/

u32  AIGetDMAStartAddr(void)
{

    return( (u32) ( ((__DSPRegs[DSP_AIMA_IDX] & DSP_AIMA_MASK_HI) << 16) | (__DSPRegs[DSP_AIMA_IDX+1] & DSP_AIMA_MASK_LO) )  );
}

/*---------------------------------------------------------------------------*
 * Name        : AIGetDMALength()
 *
 * Description : Retrieves DMA length of NEXT block
 *
 * Arguments   : None.
 *
 * Returns     : u32 bytes....Length of NEXT block to be DMA'd
 *
 *---------------------------------------------------------------------------*/

u32  AIGetDMALength(void)
{
    // the AIBLC register stores the number in 32-byte blocks, so shift
    // value to get bytes
    return( (u32) ( (__DSPRegs[DSP_AIBL_IDX] & DSP_AIBL_BLOCKLEN_MASK) << 5) );

}


/*---------------------------------------------------------------------------*
 * Name        : AICheckInit()
 *
 * Description : Returns __AI_init_flag
 *
 * Arguments   : None.
 *
 * Returns     : TRUE if AIInit() has been called at some point.
 *               FALSE if AIInit() has never been called.
 *
 *---------------------------------------------------------------------------*/

BOOL AICheckInit(void)
{
    return(__AI_init_flag);

} // end AICheckInit()



/*---------------------------------------------------------------------------*
 * Streaming Audio Sample Counter Controls
 *---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*
 * Name        : AIRegisterStreamCallback()
 *
 * Description : Registers the given function as the AIINT interrupt callback.
 *
 * Arguments   : AISCallback *callback
 *
 * Returns     : Returns the previously defined callback (NULL if there isn't
 *               one).
 *
 *
 *---------------------------------------------------------------------------*/

AISCallback AIRegisterStreamCallback(AISCallback callback)
{

    AISCallback old_callback;
    BOOL        old;


        old_callback = __AIS_Callback;

        old = OSDisableInterrupts();

        __AIS_Callback = callback;

        OSRestoreInterrupts(old);

        return(old_callback);

} /* end AIRegisterStreamCallback() */


/*---------------------------------------------------------------------------*
 * Name        : AIGetStreamSampleCount()
 *
 * Description : Retrieves number of stereo samples counted since last reset
 *
 * Arguments   : None.
 *
 * Returns     : u32 count...number of streamed stereo samples counted thus far
 *                           (since last reset)
 *
 *---------------------------------------------------------------------------*/

u32 AIGetStreamSampleCount(void)
{

    return(AI_SCNT_GET_AISCNT(__AIRegs[AI_SCNT_IDX])); // macro defined in io_reg.h


} /* end AISetStreamTrigger() */


/*---------------------------------------------------------------------------*
 * Name        : AIResetStreamSampleCount()
 *
 * Description : Clears streaming audio sample counter
 *
 * Arguments   : None.
 *
 * Returns     : None.
 *
 *---------------------------------------------------------------------------*/

void AIResetStreamSampleCount(void)
{

    AI_CR_SET_SCRST(__AIRegs[AI_CR_IDX], AI_SCRST_RESET); // macro defined in io_reg.h

} /* end AIResetStreamSampleCounter() */



/*---------------------------------------------------------------------------*
 * Name        : AISetStreamTrigger()
 *
 * Description :
 *
 * Arguments   : u32 trigger...number stereo samples to count before triggering
 *
 * Returns     : None.
 *
 *
 *---------------------------------------------------------------------------*/

void AISetStreamTrigger(u32 trigger)
{

    AI_IT_SET_AIIT(__AIRegs[AI_IT_IDX], trigger); // macro defined in io_reg.h

} /* end AISetStreamTrigger() */

/*---------------------------------------------------------------------------*
 * Name        : AIGetStreamTrigger()
 *
 * Description :
 *
 * Arguments   : None.
 *
 * Returns     : Number stereo samples to count before triggering AIINT
 *
 *---------------------------------------------------------------------------*/

u32 AIGetStreamTrigger(void)
{

    // The following macro is defined in private/io_reg.h

    return(AI_IT_GET_AIIT(__AIRegs[AI_IT_IDX]));


} /* end AIGetStreamTrigger() */


/*---------------------------------------------------------------------------*
 * Streaming Audio Play State controls
 *---------------------------------------------------------------------------*/

 /*---------------------------------------------------------------------------*
 * Name        : AISetStreamPlayState()
 *
 * Description :
 *
 * Arguments   : u8 state
 *
 * Returns     : None.
 *---------------------------------------------------------------------------*/

void AISetStreamPlayState(u32 state)
{

#pragma unused(state)

#if defined(MINNOW) || defined(SPRUCE)

        AI_CR_SET_PSTAT(__AIRegs[AI_CR_IDX], AI_STREAM_START);

#else

    BOOL old;
    u8   vol_left;
    u8   vol_right;


        // don't do anything unless we're actually changing state
        if (state != AIGetStreamPlayState())
        {

            // we're starting a stream and we're running at 32KHz
            if ((AI_SAMPLERATE_32KHZ == AIGetStreamSampleRate()) && (AI_STREAM_START == state))
            {
                // zero stream volume to minimize noise
                vol_left  = AIGetStreamVolRight();
                vol_right = AIGetStreamVolLeft();
                AISetStreamVolRight(0);
                AISetStreamVolLeft(0);

                // disable interrupts and initialize SRC
                old = OSDisableInterrupts();

                __AI_SRC_INIT();

                AI_CR_SET_SCRST(__AIRegs[AI_CR_IDX], AI_SCRST_RESET);
                AI_CR_SET_PSTAT(__AIRegs[AI_CR_IDX], AI_STREAM_START);

                OSRestoreInterrupts(old);

                AISetStreamVolLeft(vol_left);
                AISetStreamVolRight(vol_right);

            }
            else
            {
                // we're at 48KHz or we're stopping the stream, so we can do as we please
                AI_CR_SET_PSTAT(__AIRegs[AI_CR_IDX], state);
            }
        }

#endif

} /* end AISetStreamPlayState() */

/*---------------------------------------------------------------------------*
 * Name        : AIGetStreamPlayState()
 *
 * Description :
 *
 * Arguments   : None.
 *
 * Returns     : Zero if not playing, non-zero if playing
 *---------------------------------------------------------------------------*/

u32 AIGetStreamPlayState(void)
{

    // The following macro is defined in private/io_reg.h

    return(AI_CR_GET_PSTAT(__AIRegs[AI_CR_IDX]));


} /* end AIGetStreamSampleRate() */



/*---------------------------------------------------------------------------*
 * Name        : AISetDSPSampleRate()
 *
 * Description : Sets sample rate of DSP audio output
 *
 *               For RevB (and later) Flipper only.
 *               Does nothing for RevA.
 *
 *               DVD streaming should be off when changing DSP sample rate.
 *               Clobbers AI sample counter.
 *
 *
 * Arguments   : u8 rate....AI_SAMPLERATE_48KHZ or AI_SAMPLERATE_32KHZ
 *
 * Returns     : None.
 *
 *
 *---------------------------------------------------------------------------*/

void AISetDSPSampleRate(u32 rate)
{

#pragma unused(rate)

#if GX_REV>=2

    BOOL old;
    u32  play_state;
    u32  afr_state;
    u8   vol_left;
    u8   vol_right;


        // perform if and only if sample rate has actually changed
        if (rate != AIGetDSPSampleRate())
        {
            // have to 'reset' DSP32 no matter what, so set 48KHz
            __AIRegs[AI_CR_IDX] &= ~(AI_CR_DSP32_MASK);

            // for AFR.....1 = 48KHz, 0 = 32KHz
            // for DSP32...1 = 32KHz, 0 = 48KHz

            if (AI_SAMPLERATE_32KHZ == rate)
            {

                vol_left   = AIGetStreamVolLeft();
                vol_right  = AIGetStreamVolRight();

                play_state = AIGetStreamPlayState();
                afr_state  = AIGetStreamSampleRate();

                // turn down stream volume, to minimize noise
                AISetStreamVolLeft(0);
                AISetStreamVolRight(0);

                old = OSDisableInterrupts();

                __AI_SRC_INIT();

                AI_CR_SET_SCRST(__AIRegs[AI_CR_IDX], AI_SCRST_RESET);
                AI_CR_SET_AFR(__AIRegs[AI_CR_IDX], afr_state);
                AI_CR_SET_PSTAT(__AIRegs[AI_CR_IDX], play_state);
                __AIRegs[AI_CR_IDX] |= AI_CR_DSP32_MASK;

                OSRestoreInterrupts(old);

                AISetStreamVolLeft(vol_left);
                AISetStreamVolRight(vol_right);
            }

        }

#endif

} /* end AISetDSPSampleRate() */

/*---------------------------------------------------------------------------*
 * Name        : AIGetDSPSampleRate()
 *
 * Description : Returns the *inverted* DSP32 bit in the AI control register.
 *               The bit is inverted so we can use the AI_SAMPLERATE_48KHZ and
 *               AI_SAMPLERATE_32KHZ definitions created for AFR.
 *
 *                 DSP32==1....DSP sample rate is 32KHz
 *                 DSP32==0....DSP sample rate is 48KHz
 *
 *               Note that this is the opposite of the AFR bit definition.
 *
 * Arguments   : None.
 *
 * Returns     : AI_SAMPLERATE_48KHZ or AI_SAMPLERATE_32KHZ.
 *
 *
 *---------------------------------------------------------------------------*/

u32 AIGetDSPSampleRate(void)
{

    return( ((__AIRegs[AI_CR_IDX] & AI_CR_DSP32_MASK) >> AI_CR_DSP32_SHIFT)^1);

} // end AIGetDSPSampleRAte()



/*---------------------------------------------------------------------------*
 * Name        : AISetStreamSampleRate()
 *
 * Description : Sets sample rate of streamed audio.
 *
 * Arguments   : u32 rate....AI_SAMPLERATE_48KHZ or AI_SAMPLERATE_32KHZ
 *
 * Returns     : None.
 *
 *
 * Writes AFR only if state has actually changed, to prevent goofy state
 * (due to clock drift problem).
 *
 * *WILL CAUSE CLICK/POP ARTIFACTS IN DSP OUTPUT*
 *
 *---------------------------------------------------------------------------*/

void AISetStreamSampleRate(u32 rate)
{
#pragma unused(rate)

    if (AI_SAMPLERATE_48KHZ == rate)
    {
        __AI_set_stream_sample_rate(rate);
    }
    else
    {

#ifdef _DEBUG
    OSReport("AISetStreamSampleRate(): OBSOLETED. Only 48KHz streaming from disk is supported!\n");
#endif

    }

} // end AISetStreamSampleRate();

void __AI_DEBUG_set_stream_sample_rate(u32 rate);


void __AI_DEBUG_set_stream_sample_rate(u32 rate)
{
    __AI_set_stream_sample_rate(rate);
}


static void __AI_set_stream_sample_rate(u32 rate)
{


#if defined(SPRUCE) || defined(MINNOW)

    AI_CR_SET_AFR(__AIRegs[AI_CR_IDX], rate);

#else



    BOOL old;
    u32  play_state;
    u8   vol_left;
    u8   vol_right;

#if GX_REV==2
    u32  dsp_src_state;

#endif

        // write state only if rate has changed
        if (rate != AIGetStreamSampleRate())
        {

            play_state = AIGetStreamPlayState();


            vol_left   = AIGetStreamVolLeft();
            vol_right  = AIGetStreamVolRight();

            AISetStreamVolRight(0);
            AISetStreamVolLeft(0);

#if GX_REV==2
            // get current state of DSP SRC sample rate
             dsp_src_state = __AIRegs[AI_CR_IDX] & AI_CR_DSP32_MASK; // REQUIRED FOR REVB

            // have to 'reset' DSP SRC, so set DSP to 48KHz REQUIRED FOR REVB
            __AIRegs[AI_CR_IDX] &= ~(AI_CR_DSP32_MASK);
#endif

            old = OSDisableInterrupts();

            __AI_SRC_INIT();

#if GX_REV==2
            __AIRegs[AI_CR_IDX] |= dsp_src_state; // REQUIRED FOR REVB
#else
            // DO NOT DO THIS FOR REVB!!! REVC AND LATER ONLY
            // (doesn't matter for RevA 'cuz it's unfixable anyway
            AI_CR_SET_PSTAT(__AIRegs[AI_CR_IDX], play_state);
#endif
            AI_CR_SET_SCRST(__AIRegs[AI_CR_IDX], AI_SCRST_RESET);
            AI_CR_SET_AFR(__AIRegs[AI_CR_IDX], rate);

            OSRestoreInterrupts(old);

#if GX_REV==2
            // restore play state separately - REVB ONLY
            AISetStreamPlayState(play_state);
#endif

            AISetStreamVolLeft(vol_left);
            AISetStreamVolRight(vol_right);

        }

#endif

} /* end __AI_set_stream_sample_rate() */

/*---------------------------------------------------------------------------*
 * Name        : AIGetStreamSampleRate()
 *
 * Description : Retrieves sample rate of current audio stream (from DVD)
 *
 * Arguments   : None.
 *
 * Returns     : AI_SAMPLERATE_48KHZ or AI_SAMPLERATE_32KHZ
 *
 *---------------------------------------------------------------------------*/

u32 AIGetStreamSampleRate(void)
{

    // The following macro is defined in private/io_reg.h

    return(AI_CR_GET_AFR(__AIRegs[AI_CR_IDX]));


} /* end AIGetStreamSampleRate() */

/*---------------------------------------------------------------------------*
 * Streaming Audio Volume controls
 *---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*
 * Name        : AISetStreamVolLeft()
 *
 * Description : Sets 8-bit volume level for DVD streamed audio left channel
 *
 * Arguments   : None.
 *
 * Returns     :
 *---------------------------------------------------------------------------*/

void AISetStreamVolLeft(u8 vol)
{

    // The following macro is defined in private/io_reg.h
    // L/R mapping is correct, don't muck with it (eug 27may00)
    AI_VR_SET_AVRL(__AIRegs[AI_VR_IDX], vol);


} /* end AISetStreamVolLeft() */

/*---------------------------------------------------------------------------*
 * Name        : AIGetStreamVolLeft()
 *
 * Description : Retrieves 8-bit volume level for DVD streamed audio left channel
 *
 * Arguments   : None.
 *
 * Returns     :
 *---------------------------------------------------------------------------*/

u8 AIGetStreamVolLeft(void)
{

    // The following macro is defined in private/io_reg.h
    // L/R mapping is correct, don't muck with it (eug 27may00)
    return((u8)(0xff & AI_VR_GET_AVRL(__AIRegs[AI_VR_IDX])));


} /* end AIGetStreamVolLeft() */


/*---------------------------------------------------------------------------*
 * Name        : AISetStreamVolRight()
 *
 * Description : Sets 8-bit volume level for DVD streamed audio right channel
 *
 * Arguments   : None.
 *
 * Returns     :
 *---------------------------------------------------------------------------*/

void AISetStreamVolRight(u8 vol)
{

    // The following macro is defined in private/io_reg.h
    // L/R mapping is correct, don't muck with it (eug 27may00)
    AI_VR_SET_AVRR(__AIRegs[AI_VR_IDX], vol);


} /* end AISetStreamVolRight() */

/*---------------------------------------------------------------------------*
 * Name        : AIGetStreamVolRight()
 *
 * Description : Retrieves 8-bit volume level for DVD streamed audio right channel
 *
 * Arguments   : None.
 *
 * Returns     :
 *---------------------------------------------------------------------------*/

u8 AIGetStreamVolRight(void)
{

    // The following macro is defined in private/io_reg.h
    // L/R mapping is correct, don't muck with it (eug 27may00)
    return((u8)(0xff & AI_VR_GET_AVRR(__AIRegs[AI_VR_IDX])));


} /* end AIGetStreamVolRight() */



/*---------------------------------------------------------------------------*
 * AI General functions
 *---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*
 * Name        : AIInit()
 *
 * Description : Initializes the Audio Interface hardware and driver subsystem.
 *
 *               For HW1/DRIP systems, defaults to:
 *               - 48KHz DSP interface
 *               - 48KHz DVD streaming interface
 *
 *               For HW2 systems, defaults to:
 *               - 32KHz DSP interface
 *               - 48KHz DVD streaming interface
 *
 * Arguments   : stack             high address of the stack to be used for
 *                                 extended callbacks. If NULL, just use
 *                                 current stack
 *
 * Returns     : None.
 *
 *---------------------------------------------------------------------------*/

#define BUFFER_NSEC          3000   // in nanoseconds
#define BOUND_32KHZ_NSEC    31524   // in nanoseconds
#define BOUND_48KHZ_NSEC    42024   // in nanoseconds
#define MIN_WAIT_NSEC       42000   // in nanoseconds
#define MAX_WAIT_NSEC       63000   // in nanoseconds

static OSTime  bound_32KHz;
static OSTime  bound_48KHz;
static OSTime  min_wait;
static OSTime  max_wait;
static OSTime  buffer;


void AIInit(u8 *stack)
{


        // if we've already initialized, don't bother!
        if (TRUE == __AI_init_flag)
        {
            return;
        }

        OSRegisterVersion(__AIVersion);

        // For AI SRC fixes - must live for the lifetime of the app
        // convert nanosecond times into ticks so we can use OSGetTime()
        // and not worry about funny Flipper clock speeds
        bound_32KHz = OSNanosecondsToTicks(BOUND_32KHZ_NSEC);
        bound_48KHz = OSNanosecondsToTicks(BOUND_48KHZ_NSEC);
        min_wait    = OSNanosecondsToTicks(MIN_WAIT_NSEC);
        max_wait    = OSNanosecondsToTicks(MAX_WAIT_NSEC);
        buffer      = OSNanosecondsToTicks(BUFFER_NSEC);

        // Set stream volume levels to zero
        AISetStreamVolRight(0);
        AISetStreamVolLeft(0);

        // No sample counter trigger
        AISetStreamTrigger(0);

        // Reset sample counter
        AIResetStreamSampleCount();

        // Set default streaming audio samplerate
        __AI_set_stream_sample_rate(AI_SAMPLERATE_48KHZ);


#if  GX_REV==1
        // for RevA systems ***************************************
        AISetDSPSampleRate(AI_SAMPLERATE_48KHZ);
#ifdef _DEBUG
        OSReport("AIInit(): DSP is 48KHz\n");
#endif

#else
        // for RevB and later systems *****************************
        AISetDSPSampleRate(AI_SAMPLERATE_32KHZ);
#ifdef _DEBUG
        OSReport("AIInit(): DSP is 32KHz\n");
#endif

#endif

        // Set callbacks to NULL
        __AIS_Callback = NULL;
        __AID_Callback = NULL;

        __CallbackStack = stack;
        ASSERTMSG((stack == NULL) || ( ((u32)stack % 8) == 0), AI_ERR_AIINIT_STACKALIGNMENT);

        // register the AI-FIFO DMA interrupt handler, and unmask its
        // interrupt
        __OSSetInterruptHandler(__OS_INTERRUPT_DSP_AI, __AIDHandler);
        __OSUnmaskInterrupts(OS_INTERRUPTMASK_DSP_AI);


        // register the AI-streaming audio interrupt handler and unmask
        // its interrupt
        __OSSetInterruptHandler(__OS_INTERRUPT_AI_AI, __AISHandler);
        __OSUnmaskInterrupts(OS_INTERRUPTMASK_AI_AI);

        // NOTE: You MUST use __OSUnmaskInterrupts() because it initializes
        //       a global variable which keeps track of the masks. The global
        //       variable is stored in low memory at OS_INTERRUPTMASK_ADDR


        __AI_init_flag = TRUE;


} /* end AIInit() */


/*---------------------------------------------------------------------------*
 * Name        : AIReset()
 *
 * Description : Clears __AI_init_flag, in case you want to call AIInit() again.
 *
 * Arguments   : None.
 *
 * Returns     : None.
 *
 *---------------------------------------------------------------------------*/
void AIReset(void)
{
    __AI_init_flag = FALSE;

} // end AIReset()




/*---------------------------------------------------------------------------*
 * Private AI functions (TBD move to different file)
 *---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*
 * Name        : __AISHandler()
 *
 * Description : Device driver handler for audio streaming interrupt
 *
 * Arguments   : None.
 *
 * Returns     : None.
 *
 *---------------------------------------------------------------------------*/
static void __AISHandler(__OSInterrupt interrupt, OSContext *context)
{

    OSContext   exceptionContext;

#pragma unused(interrupt)

        // clear interrupt
        __AIRegs[AI_CR_IDX] |= AI_CR_AIINT_MASK;

        // for re-entrancy
        OSClearContext(&exceptionContext);
        OSSetCurrentContext(&exceptionContext);

        if (__AIS_Callback)
        {
            // Invoke callback with number of samples counted so far
            (*__AIS_Callback)(AI_SCNT_GET_AISCNT(__AIRegs[AI_SCNT_IDX]));
        }

        // restore context
        OSClearContext(&exceptionContext);
        OSSetCurrentContext(context);

    // no need to load, as that will be handled by __OSDispatchInterrupt

} /* end AISHandler() */

/*---------------------------------------------------------------------------*
 * Name        : __AIDHandler()
 *
 * Description : Device driver handler for AI-FIFO DMA interrupt
 *
 * Arguments   :
 *
 * Returns     : None.
 *
 *---------------------------------------------------------------------------*/

static void __AIDHandler(__OSInterrupt interrupt, OSContext *context)
{

    OSContext   exceptionContext;
    u16         tmp;

#pragma unused(interrupt)


        // CLEAR INTERRUPT
        // Be careful not to clear the other interrupt-pending flags. If they
        // are asserted (meaning an interrupt is pending), writing a '1' back
        // back to the register will clear them!
        // Writing a zero has no effect, so always clear the other flags first

        // Note that explicitly setting DSP_CDCR_AIINT_MASK is unnecessary
        // because if we're here, then the AIINT flag is already asserted.
        // Leave it in for now for clarity...

        tmp = __DSPRegs[DSP_CDCR_IDX];
        tmp = (u16)((tmp & ~( DSP_CDCR_DSPINT_MASK | DSP_CDCR_ARAMINT_MASK)) | DSP_CDCR_AIINT_MASK);
        __DSPRegs[DSP_CDCR_IDX] = tmp;


        // for re-entrancy
        OSClearContext(&exceptionContext);
        OSSetCurrentContext(&exceptionContext);

        if (__AID_Callback)
        {
            // check previous __AID_Callback is still acitve or not.
            if(!__AID_Active)
            {
                __AID_Active = TRUE;
                
                // Invoke callback, no parameters
                if (__CallbackStack != NULL)
                {
                    // switch to callbackstack for the duration of this
                    // callback
                    __AICallbackStackSwitch(__AID_Callback);
                }
                else
                {
                    // use this stack for the callback
                    (*__AID_Callback)();
                }
                
                __AID_Active = FALSE;
            }
        }

        // restore context
        OSClearContext(&exceptionContext);
        OSSetCurrentContext(context);
        // no need to load, as that will be handled by __OSDispatchInterrupt

} /* end AIDHandler() */


/*---------------------------------------------------------------------------*
 * Name        : __AICallbackStackSwitch
 *
 * Description : Code to switch stacks (if one has been designated)
 *               This code should only be used if a stack has been designated,
 *               otherwise the stack will be trashed!
 *
 * Arguments   : cb         the callback to be invoked
 *
 * Returns     : None.
 *
 *---------------------------------------------------------------------------*/

asm static void __AICallbackStackSwitch(register void *cb)
{
    // need to save old LR
    fralloc

    // save ptr to old stack
    lis     r5, __OldStack@ha
    addi    r5, r5, __OldStack@l
    stw     r1, 0(r5)

    // load new stack
    lis     r5, __CallbackStack@ha
    addi    r5, r5, __CallbackStack@l
    lwz     r1, 0(r5)
    // push a frame
    subi    r1, r1, 8

    mtlr    cb
    blrl

    // restore stack
    lis     r5, __OldStack@ha
    addi    r5, r5, __OldStack@l
    lwz     r1, 0(r5)

    // function exit code automatically added by compiler
    frfree
    blr
}


/*---------------------------------------------------------------------------*
 * Name        : __AI_SRC_INIT()
 *
 * Description : For initializing AI sample rate clocks for hardware SRCs.
 *               Synchronizes f.s.a and sample clocks.
 *
 *               WARNING!!!!!!! THIS FUNCTION CLOBBERS THE FOLLOWING:
 *
 *               AI Streaming Sample Counter
 *               AI Streaming Frequency register (AFR)
 *               AI Streaming Play State bit     (PSTAT)
 *
 *               This function must be used whenever:
 *
 *                  1. DSP32 control bit is changed
 *                  2. PSTAT is *asserted* while AFR is 32KHz
 *                  3. AFR control bit is changed
 *
 *               Interrupts must be disabled during this function!!!!!
 *
 *               This function is necessary because of a bug in the SRC control
 *               logic. The logic makes faulty assumptions about the phase
 *               relationship of the 32KHz and 48KHz sample clocks when the SRCs
 *               are started. The result is that it is possible to put the
 *               SRCs into a bad state in which they do not work properly,
 *               causing many aliasing/imaging components in the audio output.
 *
 *               To prevent this "bad phase", this function must be called
 *               before:
 *
 *                  1. A change is made to the DSP32 bit of the AI control
 *                     register
 *                  2. A change is made to the AFR bit of the AI control register.
 *                  3. PSTAT is asserted while AFR is set to 32KHz.
 *
 *               Note if PSTAT is being cleared, then this function call is
 *               not needed. Also, if AFR is 48KHz, then PSTAT can be changed
 *               at will.
 *
 *               This function will watch the sample clock (AI sample count
 *               register) for a good state, then exit. The caller must then
 *               program AFR, PSTAT, and/or DSP32 within one 32KHz sample clock
 *               cycle (about 31,250 nsec).
 *
 *               This function will complete within 200usec, pending correction
 *               of the "drifting clocks problem" in hardware. Without the
 *               "drifting clocks" fix, this function could take as long as
 *               17msec!!!!
 *
 *               Upon termination, this function will leave the following state:
 *
 *               AFR   = 32KHZ streaming
 *               PSTAT = Stopped
 *               SCNT  = <undefined>
 *
 *
 * Arguments   : None.
 *
 * Returns     : None.
 *
 *---------------------------------------------------------------------------*/


void __AI_SRC_INIT(void)
{

    OSTime  rising_32khz=0;
    OSTime  rising_48khz=0;
    OSTime  diff=0;

    OSTime  t1 = 0;

    OSTime  temp;

    u32     temp0, temp1;
    u32     done=0;

    u32     volume=0;

    u32     Init_Cnt=0;
    u32     walking=0;

        walking=0;
        Init_Cnt=0;

        temp        = 0;


#if GX_REV==2
        //*************************************************************
        // Code specific to RevB, to accomodate drifting clocks problem
        //*************************************************************

#ifdef _DEBUG
        profile.t_start = OSGetTime();
#endif


        while(done==0)
        {
            AI_CR_SET_SCRST(__AIRegs[AI_CR_IDX], AI_SCRST_RESET);

            AI_CR_SET_AFR(__AIRegs[AI_CR_IDX], AI_SAMPLERATE_32KHZ);
            AI_CR_SET_PSTAT(__AIRegs[AI_CR_IDX], AI_STREAM_START);

            temp0 = AI_SCNT_GET_AISCNT(__AIRegs[AI_SCNT_IDX]);
            while(temp0 == AI_SCNT_GET_AISCNT(__AIRegs[AI_SCNT_IDX]))
            {
                // do nothing

            }

            //log the time
            rising_32khz = OSGetTime();

            //stop streaming and switch to 48khz
            AI_CR_SET_AFR(__AIRegs[AI_CR_IDX], AI_SAMPLERATE_48KHZ);
            AI_CR_SET_PSTAT(__AIRegs[AI_CR_IDX], AI_STREAM_START);


            temp1 = AI_SCNT_GET_AISCNT(__AIRegs[AI_SCNT_IDX]);
            while (temp1 == AI_SCNT_GET_AISCNT(__AIRegs[AI_SCNT_IDX]))
            {
                // do nothing
            }

            rising_48khz = OSGetTime();

            diff = rising_48khz - rising_32khz;

            // stop streaming
            AI_CR_SET_AFR(__AIRegs[AI_CR_IDX], AI_SAMPLERATE_32KHZ);
            AI_CR_SET_PSTAT(__AIRegs[AI_CR_IDX], AI_STREAM_STOP);

            if (diff < (bound_32KHz - buffer))
            {
                temp=min_wait;
                done=1;
                Init_Cnt++;
            }
            else
            {
                if ((diff >= (bound_32KHz+buffer)) && (diff < (bound_48KHz-buffer)))
                {
                    // 31524+10500
                    temp=max_wait;
                    done=1;
                    Init_Cnt++;
                }
                else
                {
                    done=0;
                    walking=1;
                    Init_Cnt++;
                }

            } // end if...else...



        } // end while()


        //wait for the proper amount of time specified above
        while ((rising_48khz + temp) > OSGetTime())
        {
            // do nothing
        }


#ifdef _DEBUG
        // for measuring length of time to execute SRC fix
        // for debug builds only!
        //__ai_src_time_end = OSGetTime();

        profile.t_end = OSGetTime();
#endif




#else
        // ********************************************************************************
        // for RevC or later --> note that this fix is also run for RevA, but it doesn't
        // matter because RevA is unfixable
        // ********************************************************************************


#ifdef _DEBUG

        profile.t_start = OSGetTime();

#endif

            // are we currently playing at 48KHz? for RevC only!
        if ((AI_STREAM_START == AIGetStreamPlayState()) && (AI_SAMPLERATE_48KHZ == AIGetStreamSampleRate()))
        {
            AI_CR_SET_PSTAT(__AIRegs[AI_CR_IDX], AI_STREAM_STOP);
            AI_CR_SET_AFR(__AIRegs[AI_CR_IDX], AI_SAMPLERATE_32KHZ); // added for RevC 32KHz rise time optimization

            t1 = OSGetTime();
            while ((OSGetTime() - t1) < OSNanosecondsToTicks(21000))
            {
                // do nothing
            }
        }
        else
        {
            AI_CR_SET_AFR(__AIRegs[AI_CR_IDX], AI_SAMPLERATE_32KHZ); // added for RevC 32KHz rise time optimization
        }

#ifdef _DEBUG
        profile.t1 = OSGetTime();
#endif

        AI_CR_SET_SCRST(__AIRegs[AI_CR_IDX], AI_SCRST_RESET);
        AI_CR_SET_PSTAT(__AIRegs[AI_CR_IDX], AI_STREAM_START);

        temp0 = AI_SCNT_GET_AISCNT(__AIRegs[AI_SCNT_IDX]);
        while(temp0 == AI_SCNT_GET_AISCNT(__AIRegs[AI_SCNT_IDX]))
        {
            // do nothing

        }
#ifdef _DEBUG
        profile.t2 = OSGetTime();
#endif
        //log the time
        rising_32khz = OSGetTime();

        //start streaming and switch to 48khz
        AI_CR_SET_AFR(__AIRegs[AI_CR_IDX], AI_SAMPLERATE_48KHZ);
        //AI_CR_SET_PSTAT(__AIRegs[AI_CR_IDX], AI_STREAM_START);


        temp1 = AI_SCNT_GET_AISCNT(__AIRegs[AI_SCNT_IDX]);
        while (temp1 == AI_SCNT_GET_AISCNT(__AIRegs[AI_SCNT_IDX]))
        {
            // do nothing
        }
#ifdef _DEBUG
        profile.t3 = OSGetTime();
#endif

        rising_48khz = OSGetTime();

        diff = rising_48khz - rising_32khz;

        // stop streaming
        AI_CR_SET_AFR(__AIRegs[AI_CR_IDX], AI_SAMPLERATE_32KHZ);
        AI_CR_SET_PSTAT(__AIRegs[AI_CR_IDX], AI_STREAM_STOP);


        if (diff < bound_32KHz)
        {
            temp=min_wait;
            done=1;
            Init_Cnt++;
        }
        else
        {

            temp=max_wait;
            done=1;
            Init_Cnt++;

        } // end if...else...

#ifdef _DEBUG
        profile.t4 = OSGetTime();
#endif

        //wait for the proper amount of time specified above
        while ((rising_48khz + temp) > OSGetTime())
        {
            // do nothing
        }

#ifdef _DEBUG
        profile.t_end = OSGetTime();
#endif


#endif // for RevA+RevC vs. RevB if...else clause

} // end __AI_SRC_INIT()


/*---------------------------------------------------------------------------*
 * Name        : __ai_src_get_time()
 *
 * Description : Returns pointer to internal profile of last execution of
 *               __AI_SRC_INIT() code.
 *
 * Arguments   : None.
 *
 * Returns     : Pointer to sTIMELOG
 *
 *---------------------------------------------------------------------------*/

sTIMELOG *__ai_src_get_time(void)
{
#ifdef _DEBUG
    return(&profile);
#else
    return(0);
#endif
} // end __ai_src_get_time()
