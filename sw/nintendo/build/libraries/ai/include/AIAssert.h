/*---------------------------------------------------------------------------*
  Project:  AI Error Messages
  File:     AIAssert.h

  Copyright 1998, 1999 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: AIAssert.h,v $
  Revision 1.1.1.1  2004/01/03 01:02:28  paulm
  Gamecube library source from Nintendo

    
    2     4/11/00 5:30p Eugene
    Added assert message for bad AI-FIFO DMA length.
    
    1     3/08/00 6:24p Tian
    Initial checkin
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#ifndef __AIASSERT_H__
#define __AIASSERT_H__

#define AI_ERR_AIINIT_STACKALIGNMENT "AIInit: stack must be 8-byte aligned"
#define AI_ERR_AISTARTDMA_LENGTH     "AIStartDMA: length must be multiple of 32 bytes"

#endif
