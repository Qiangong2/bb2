/*---------------------------------------------------------------------------*
  Project:  Sound Pipeline (SP) for AX
  File:     ax3d.c

  Copyright 1998, 1999, 2000, 2001 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: sp.c,v $
  Revision 1.1.1.1  2004/06/09 17:39:20  paulm
  GC library source from Nintendo SDK

    
    1     8/15/01 11:16a Billyjack
    created
   
  $NoKeywords: $
 *---------------------------------------------------------------------------*/
#include <dolphin.h>
#include <dolphin/sp.h>


/*---------------------------------------------------------------------------*
 *---------------------------------------------------------------------------*/
void SPInitSoundTable(SPSoundTable *table, u32 aramBase, u32 zeroBase)
{
    int             i;
    SPSoundEntry    *sound;
    SPAdpcmEntry    *adpcm;
    u32             aramBase4, aramBase8, aramBase16,
                    zeroBase4, zeroBase8, zeroBase16;

    ASSERT(table);

    // pre compute base addresses
    aramBase4   = aramBase << 1;
    zeroBase4   = (zeroBase << 1) + 2;
    aramBase8   = aramBase;
    zeroBase8   = zeroBase;
    aramBase16  = aramBase >> 1;
    zeroBase16  = zeroBase >> 1;

    // initialize pointers to objects 
    sound = &table->sound[0];
    adpcm = (SPAdpcmEntry*)&table->sound[table->entries];

    // intialize objects
    for (i = 0; i < table->entries; i++)
    {
        switch (sound->type)
        {
        case SP_TYPE_ADPCM_ONESHOT:

            sound->loopAddr     = zeroBase4;
            sound->loopEndAddr  = 0;
            sound->endAddr      = aramBase4 + sound->endAddr;
            sound->currentAddr  = aramBase4 + sound->currentAddr;
            sound->adpcm        = adpcm;

            adpcm++;

            break;

        case SP_TYPE_ADPCM_LOOPED:

            sound->loopAddr     = aramBase4 + sound->loopAddr;
            sound->loopEndAddr  = aramBase4 + sound->loopEndAddr;
            sound->endAddr      = aramBase4 + sound->endAddr;
            sound->currentAddr  = aramBase4 + sound->currentAddr;
            sound->adpcm        = adpcm;

            adpcm++;

            break;

        case SP_TYPE_PCM16_ONESHOT:

            sound->loopAddr     = zeroBase16;
            sound->loopEndAddr  = 0;
            sound->endAddr      = aramBase16 + sound->endAddr;
            sound->currentAddr  = aramBase16 + sound->currentAddr;

            break;

        case SP_TYPE_PCM16_LOOPED:

            sound->loopAddr     = aramBase16 + sound->loopAddr;
            sound->loopEndAddr  = aramBase16 + sound->loopEndAddr;
            sound->endAddr      = aramBase16 + sound->endAddr;
            sound->currentAddr  = aramBase16 + sound->currentAddr;

            break;

        case SP_TYPE_PCM8_ONESHOT:

            sound->loopAddr     = zeroBase8;
            sound->loopEndAddr  = 0;
            sound->endAddr      = aramBase8 + sound->endAddr;
            sound->currentAddr  = aramBase8 + sound->currentAddr;

            break;

        case SP_TYPE_PCM8_LOOPED:

            sound->loopAddr     = aramBase8 + sound->loopAddr;
            sound->loopEndAddr  = aramBase8 + sound->loopEndAddr;
            sound->endAddr      = aramBase8 + sound->endAddr;
            sound->currentAddr  = aramBase8 + sound->currentAddr;

            break;
        }

        sound++;
    }
}


/*---------------------------------------------------------------------------*
 *---------------------------------------------------------------------------*/
SPSoundEntry * SPGetSoundEntry(SPSoundTable *table, u32 index)
{
    ASSERT(table);

    if (table->entries > index)
        return &table->sound[index];    

    return NULL;
}


/*---------------------------------------------------------------------------*
 *---------------------------------------------------------------------------*/
void SPPrepareSound(SPSoundEntry *sound, AXVPB *axvpb, u32 sampleRate)
{
    int old;
    u32 srcBits, loopAddr, endAddr, currentAddr;
    u16 *p, *p1;

    ASSERT(sound);
    ASSERT(axvpb);

    srcBits = (u32)(0x00010000 * ((f32)sampleRate / AX_IN_SAMPLES_PER_SEC));

    switch (sound->type)
    {
    case SP_TYPE_ADPCM_ONESHOT:

        loopAddr    = sound->loopAddr;
        endAddr     = sound->endAddr;
        currentAddr = sound->currentAddr;

        p   = (u16*)&axvpb->pb.addr;
        p1  = (u16*)sound->adpcm;

        old = OSDisableInterrupts();

        // addr bits
        *p++    = AXPBADDR_LOOP_OFF;            
        *p++    = AX_PB_FORMAT_ADPCM;           
        *p++    = (u16)(loopAddr >> 16);        
        *p++    = (u16)(loopAddr & 0xffff);     
        *p++    = (u16)(endAddr >> 16);         
        *p++    = (u16)(endAddr & 0xffff);      
        *p++    = (u16)(currentAddr >> 16);     
        *p++    = (u16)(currentAddr & 0xffff);  
        // adpcm bits
        *p++    = *p1++;  
        *p++    = *p1++;  
        *p++    = *p1++;  
        *p++    = *p1++;  
        *p++    = *p1++;  
        *p++    = *p1++;  
        *p++    = *p1++;  
        *p++    = *p1++;  
        *p++    = *p1++;  
        *p++    = *p1++;  
        *p++    = *p1++;  
        *p++    = *p1++;  
        *p++    = *p1++;  
        *p++    = *p1++;  
        *p++    = *p1++;  
        *p++    = *p1++;  
        *p++    = *p1++;  
        *p++    = *p1++;  
        *p++    = *p1++;  
        *p++    = *p1++;
        // src bits
        *p++    = (u16)(srcBits >> 16);
        *p++    = (u16)(srcBits & 0xFFFF);
        *p++    = 0;
        *p++    = 0;
        *p++    = 0;
        *p++    = 0;
        *p++    = 0;
        // don't need ADPCM loop context

        axvpb->sync |= AX_SYNC_USER_ADDR | AX_SYNC_USER_ADPCM | AX_SYNC_USER_SRC;

        OSRestoreInterrupts(old);

        break;
        
    case SP_TYPE_ADPCM_LOOPED:

        loopAddr    = sound->loopAddr;
        endAddr     = sound->loopEndAddr;
        currentAddr = sound->currentAddr;

        p   = (u16*)&axvpb->pb.addr;
        p1  = (u16*)sound->adpcm;

        old = OSDisableInterrupts();

        // addr bits
        *p++    = AXPBADDR_LOOP_ON;            
        *p++    = AX_PB_FORMAT_ADPCM;           
        *p++    = (u16)(loopAddr >> 16);        
        *p++    = (u16)(loopAddr & 0xffff);     
        *p++    = (u16)(endAddr >> 16);         
        *p++    = (u16)(endAddr & 0xffff);      
        *p++    = (u16)(currentAddr >> 16);     
        *p++    = (u16)(currentAddr & 0xffff);  
        // adpcm bits
        *p++    = *p1++;  
        *p++    = *p1++;  
        *p++    = *p1++;  
        *p++    = *p1++;  
        *p++    = *p1++;  
        *p++    = *p1++;  
        *p++    = *p1++;  
        *p++    = *p1++;  
        *p++    = *p1++;  
        *p++    = *p1++;  
        *p++    = *p1++;  
        *p++    = *p1++;  
        *p++    = *p1++;  
        *p++    = *p1++;  
        *p++    = *p1++;  
        *p++    = *p1++;  
        *p++    = *p1++;  
        *p++    = *p1++;  
        *p++    = *p1++;  
        *p++    = *p1++;
        // src bits
        *p++    = (u16)(srcBits >> 16);
        *p++    = (u16)(srcBits & 0xFFFF);
        *p++    = 0;
        *p++    = 0;
        *p++    = 0;
        *p++    = 0;
        *p++    = 0;
        // ADPCM loop bits
        *p++    = *p1++;
        *p++    = *p1++;
        *p++    = *p1++;

        axvpb->sync |= AX_SYNC_USER_ADDR | AX_SYNC_USER_ADPCM | AX_SYNC_USER_SRC | AX_SYNC_USER_ADPCMLOOP;

        OSRestoreInterrupts(old);

        break;

    case SP_TYPE_PCM16_ONESHOT:

        loopAddr    = sound->loopAddr;
        endAddr     = sound->endAddr;
        currentAddr = sound->currentAddr;

        p   = (u16*)&axvpb->pb.addr;

        old = OSDisableInterrupts();

        // addr bits
        *p++    = AXPBADDR_LOOP_OFF;            
        *p++    = AX_PB_FORMAT_PCM16;           
        *p++    = (u16)(loopAddr >> 16);        
        *p++    = (u16)(loopAddr & 0xffff);     
        *p++    = (u16)(endAddr >> 16);         
        *p++    = (u16)(endAddr & 0xffff);      
        *p++    = (u16)(currentAddr >> 16);     
        *p++    = (u16)(currentAddr & 0xffff);  
        // adpcm bits
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0x0800;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;
        // src bits
        *p++    = (u16)(srcBits >> 16);
        *p++    = (u16)(srcBits & 0xFFFF);
        *p++    = 0;
        *p++    = 0;
        *p++    = 0;
        *p++    = 0;
        *p++    = 0;
        // don't need ADPCM loop context

        axvpb->sync |= AX_SYNC_USER_ADDR | AX_SYNC_USER_ADPCM | AX_SYNC_USER_SRC;

        OSRestoreInterrupts(old);

        break;

    case SP_TYPE_PCM16_LOOPED:

        loopAddr    = sound->loopAddr;
        endAddr     = sound->loopEndAddr;
        currentAddr = sound->currentAddr;

        p   = (u16*)&axvpb->pb.addr;

        old = OSDisableInterrupts();

        // addr bits
        *p++    = AXPBADDR_LOOP_ON;            
        *p++    = AX_PB_FORMAT_PCM16;           
        *p++    = (u16)(loopAddr >> 16);        
        *p++    = (u16)(loopAddr & 0xffff);     
        *p++    = (u16)(endAddr >> 16);         
        *p++    = (u16)(endAddr & 0xffff);      
        *p++    = (u16)(currentAddr >> 16);     
        *p++    = (u16)(currentAddr & 0xffff);  
        // adpcm bits
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0x0800;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;
        // src bits
        *p++    = (u16)(srcBits >> 16);
        *p++    = (u16)(srcBits & 0xFFFF);
        *p++    = 0;
        *p++    = 0;
        *p++    = 0;
        *p++    = 0;
        *p++    = 0;
        // don't need ADPCM loop context

        axvpb->sync |= AX_SYNC_USER_ADDR | AX_SYNC_USER_ADPCM | AX_SYNC_USER_SRC;

        OSRestoreInterrupts(old);

        break;

    case SP_TYPE_PCM8_ONESHOT:

        loopAddr    = sound->loopAddr;
        endAddr     = sound->endAddr;
        currentAddr = sound->currentAddr;

        p   = (u16*)&axvpb->pb.addr;

        old = OSDisableInterrupts();

        // addr bits
        *p++    = AXPBADDR_LOOP_OFF;            
        *p++    = AX_PB_FORMAT_PCM8;           
        *p++    = (u16)(loopAddr >> 16);        
        *p++    = (u16)(loopAddr & 0xffff);     
        *p++    = (u16)(endAddr >> 16);         
        *p++    = (u16)(endAddr & 0xffff);      
        *p++    = (u16)(currentAddr >> 16);     
        *p++    = (u16)(currentAddr & 0xffff);  
        // adpcm bits
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0x0100;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;
        // src bits
        *p++    = (u16)(srcBits >> 16);
        *p++    = (u16)(srcBits & 0xFFFF);
        *p++    = 0;
        *p++    = 0;
        *p++    = 0;
        *p++    = 0;
        *p++    = 0;
        // don't need ADPCM loop context

        axvpb->sync |= AX_SYNC_USER_ADDR | AX_SYNC_USER_ADPCM | AX_SYNC_USER_SRC;

        OSRestoreInterrupts(old);

        break;

    case SP_TYPE_PCM8_LOOPED:

        loopAddr    = sound->loopAddr;
        endAddr     = sound->loopEndAddr;
        currentAddr = sound->currentAddr;

        p   = (u16*)&axvpb->pb.addr;

        old = OSDisableInterrupts();

        // addr bits
        *p++    = AXPBADDR_LOOP_ON;            
        *p++    = AX_PB_FORMAT_PCM8;           
        *p++    = (u16)(loopAddr >> 16);        
        *p++    = (u16)(loopAddr & 0xffff);     
        *p++    = (u16)(endAddr >> 16);         
        *p++    = (u16)(endAddr & 0xffff);      
        *p++    = (u16)(currentAddr >> 16);     
        *p++    = (u16)(currentAddr & 0xffff);  
        // adpcm bits
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0x0100;  
        *p++    = 0;  
        *p++    = 0;  
        *p++    = 0;
        // src bits
        *p++    = (u16)(srcBits >> 16);
        *p++    = (u16)(srcBits & 0xFFFF);
        *p++    = 0;
        *p++    = 0;
        *p++    = 0;
        *p++    = 0;
        *p++    = 0;
        // don't need ADPCM loop context

        axvpb->sync |= AX_SYNC_USER_ADDR | AX_SYNC_USER_ADPCM | AX_SYNC_USER_SRC;

        OSRestoreInterrupts(old);

        break;
        }
}


/*---------------------------------------------------------------------------*
 *---------------------------------------------------------------------------*/
void SPPrepareEnd(SPSoundEntry *sound, AXVPB *axvpb)
{
    int old;

    ASSERT(sound);
    ASSERT(axvpb);

    old = OSDisableInterrupts();

    axvpb->pb.addr.loopFlag     = AXPBADDR_LOOP_OFF;
    axvpb->pb.addr.endAddressHi = (u16)(sound->endAddr >> 16);
    axvpb->pb.addr.endAddressLo = (u16)(sound->endAddr & 0xFFFF);

    axvpb->sync |= AX_SYNC_USER_LOOP | AX_SYNC_USER_ENDADDR;

    OSRestoreInterrupts(old);
}
