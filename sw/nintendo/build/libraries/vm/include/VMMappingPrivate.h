/*---------------------------------------------------------------------------*
  Project:  VM
  File:     VMMappingPrivate.h
 
  Copyright 2002 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: VMMappingPrivate.h,v $
  Revision 1.1.1.1  2004/04/20 05:29:07  paulm
  Virtual Memory library from Nintendo

 * 
 * 4     5/09/02 5:53p Stevera
 * Added __VMMappingErrorAlert.
 * 
 * 3     4/30/02 3:14p Stevera
 * Added concept of ARAM page being dirty (so that ARAM pages aren't
 * pulled into MRAM when they have garbage data).
 * 
 * 2     4/29/02 11:46a Stevera
 * RC2 for testing
 *     
  
  $NoKeywords: $

 *---------------------------------------------------------------------------*/

#ifndef __VMMAPPINGPRIVATE_H__
#define __VMMAPPINGPRIVATE_H__

#ifdef __cplusplus
extern "C" {
#endif


/*---------------------------------------------------------------------------*
  Includes
 *---------------------------------------------------------------------------*/
#include <dolphin/types.h>


/*---------------------------------------------------------------------------*
    function prototypes
 *---------------------------------------------------------------------------*/
void __VMAllocVirtualToARAMLUT( void );
void __VMAllocARAMToVirtualLUT( void );
u32  __VMTranslateVMPageToARAMPage( u32 virtualAddress );
BOOL __VMDoesMappingExist( u32 virtualAddress );
void __VMMappingErrorAlert( u32 virtualAddress );
void __VMSetARAMPageAsDirty( u32 virtualAddress );
BOOL __VMIsARAMPageDirty( u32 virtualAddress );



#ifdef __cplusplus
}
#endif

#endif // __VMMAPPINGPRIVATE_H__ 
