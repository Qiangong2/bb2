/*---------------------------------------------------------------------------*
  Project:  VM
  File:     VMPrivate.h
 
  Copyright 2002 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: VMPrivate.h,v $
  Revision 1.1.1.1  2004/04/20 05:29:07  paulm
  Virtual Memory library from Nintendo

 * 
 * 2     4/29/02 11:46a Stevera
 * RC2 for testing
 *     
  
  $NoKeywords: $

 *---------------------------------------------------------------------------*/

#ifndef __VMPRIVATE_H__
#define __VMPRIVATE_H__

#ifdef __cplusplus
extern "C" {
#endif


/*---------------------------------------------------------------------------*
  Includes
 *---------------------------------------------------------------------------*/
#include <dolphin/types.h>


/*---------------------------------------------------------------------------*
    function prototypes
 *---------------------------------------------------------------------------*/
u32  __VMGetMRAMBase( void );
u32  __VMGetNumPagesInMRAM( void );



#ifdef __cplusplus
}
#endif

#endif // __VMPRIVATE_H__ 
