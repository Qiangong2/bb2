/*---------------------------------------------------------------------------*
  Project:  VM
  File:     VMPageReplacement.c
 
  Copyright 2002 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: VMPageReplacement.c,v $
  Revision 1.1.1.1  2004/04/20 05:29:07  paulm
  Virtual Memory library from Nintendo

 * 
 * 14    9/10/02 6:29p Stevera
 * Changed function call from  VMBASEGetVirtualAddressFromPageInMRAM() to
 * VMBASEGetVirtualAddrFromPageInMRAM(). 
 * 
 * 13    9/05/02 1:12p Stevera
 * Changed all function calls into the VMBase library to start with the
 * prefix VMBASE.
 * 
 * 12    5/07/02 11:39a Stevera
 * Renamed variables, added more comments.
 * 
 * 11    5/03/02 2:11p Stevera
 * Function from VMBase changed (VMSetPageReferenced).
 * 
 * 10    5/02/02 5:07p Stevera
 * Changed the default policy to RANDOM.
 * 
 * 9     5/02/02 3:46p Stevera
 * Added a public header to expose the page replacement functions.
 * 
 * 8     5/01/02 2:59p Stevera
 * Removed stats tracking from VM library. Added optional stats logging
 * callback.
 * 
 * 7     4/29/02 11:46a Stevera
 * RC2 for testing
 *     
  
  $NoKeywords: $

 *---------------------------------------------------------------------------*/



/*---------------------------------------------------------------------------*
  Includes
 *---------------------------------------------------------------------------*/
#include "VMPageReplacementPrivate.h"
#include "VMPrivate.h"
#include <dolphin/vmbase.h>
#include <dolphin/vm.h>
#include <dolphin.h>



/*---------------------------------------------------------------------------*
  Definitions & declarations
 *---------------------------------------------------------------------------*/
static u32 g_vmNextPageToSwap = 0;		//The next page to swap given that free pages still exist
static BOOL g_vmFreePagesExist = TRUE;	//Flag for whether free page still exist
static VM_PageReplacementPolicy g_vmPageReplacementPolicy = VM_PRP_RANDOM;	//Default policy



/*---------------------------------------------------------------------------*
  Function prototypes
 *---------------------------------------------------------------------------*/
u32  __VMPageReplacementLRU( void );
u32  __VMPageReplacementRandom( void );
u32  __VMPageReplacementFIFO( void );



/*---------------------------------------------------------------------------*
  Private functions
 *---------------------------------------------------------------------------*/



/*---------------------------------------------------------------------------*
  Name:         VMSetPageReplacementPolicy

  Description:  Sets the page replacement policy. This can be changed at any time.

  Arguments:    VM_PageReplacementPolicy	policy

  Returns:      None.
 *---------------------------------------------------------------------------*/
void VMSetPageReplacementPolicy( VM_PageReplacementPolicy policy )
{
	g_vmPageReplacementPolicy = policy;

}


/*---------------------------------------------------------------------------*
  Name:         VMGetPageReplacementPolicy

  Description:  Gets the current page replacement policy.

  Arguments:    none.

  Returns:      VM_PageReplacementPolicy
 *---------------------------------------------------------------------------*/
VM_PageReplacementPolicy VMGetPageReplacementPolicy( void )
{
	return( g_vmPageReplacementPolicy );
}


/*---------------------------------------------------------------------------*
  Name:         __VMSetNextPageToSwap

  Description:  Set the next page to swap.

  Arguments:    u32    page

  Returns:      none.
 *---------------------------------------------------------------------------*/
void __VMSetNextPageToSwap( u32 page )
{
	g_vmNextPageToSwap = page;
}


/*---------------------------------------------------------------------------*
  Name:         __VMSetFreePagesExist

  Description:  Set whether free pages exist (like a free page list).

  Arguments:    BOOL    exist

  Returns:      none.
 *---------------------------------------------------------------------------*/
void __VMSetFreePagesExist( BOOL exist )
{
	g_vmFreePagesExist = exist;
}


/*---------------------------------------------------------------------------*
  Name:         __VMGetPageToReplace

  Description:  Figures out the best page to replace

  Arguments:    none.

  Returns:      u32     Page to replace.
 *---------------------------------------------------------------------------*/
u32  __VMGetPageToReplace( void )
{
	VM_PageReplacementPolicy policy = VMGetPageReplacementPolicy();
	
	if( policy == VM_PRP_LRU ) {
		return( __VMPageReplacementLRU() );
	}
	else if( policy == VM_PRP_RANDOM ) {	
		return( __VMPageReplacementRandom() );
	}
	else {
		return( __VMPageReplacementFIFO() );
	}

}


/*---------------------------------------------------------------------------*
  Name:         __VMPageReplacementLRU

  Description:  The LRU page replacement algorithm. Also known as the
                Enhanced Second-Chance (clock) page replacement policy.
                This variation only goes through the list once.

                1. Scan from current position for a 0,0 page (not referenced, not changed).
                2. Otherwise, record the first instance of (0,1), (1,0), and (1,1) pages
                   and reset the referenced bit in each considered page.
                3. If we make a cycle, choose the first page found in the lowest non-empty group.

  Arguments:    none.

  Returns:      u32     Page to replace.
 *---------------------------------------------------------------------------*/
u32 __VMPageReplacementLRU( void )
{
	u32 pageToReplace = 0;
	u32 startPage = g_vmNextPageToSwap;
	s32 firstPageInGroup1 = -1;
	s32 firstPageInGroup2 = -1;
	s32 firstPageInGroup3 = -1;

	if( !g_vmFreePagesExist )
	{
		while(1)
		{
			u32 vaddr = VMBASEGetVirtualAddrFromPageInMRAM( g_vmNextPageToSwap );
			
			if( vaddr != 0 && VMBASEIsPageValid( vaddr ) )
			{
				BOOL referenced = VMBASEIsPageReferenced( vaddr );
				BOOL changed = VMBASEIsPageDirty( vaddr );
				
				if( !referenced && !changed && !VMBASEIsPageLocked( g_vmNextPageToSwap ) )
				{	//Page not referenced or changed - use this page
					pageToReplace = (u32)g_vmNextPageToSwap;
					break;
				}
				else if( !referenced && changed ) {
					if( firstPageInGroup1 < 0 && !VMBASEIsPageLocked( g_vmNextPageToSwap ) ) {
						firstPageInGroup1 = (s32)g_vmNextPageToSwap;
					}
				}
				else if( referenced && !changed ) {
					if( firstPageInGroup2 < 0 && !VMBASEIsPageLocked( g_vmNextPageToSwap ) ) {
						firstPageInGroup2 = (s32)g_vmNextPageToSwap;
					}
				}
				else {
					if( firstPageInGroup3 < 0 && !VMBASEIsPageLocked( g_vmNextPageToSwap ) ) {
						firstPageInGroup3 = (s32)g_vmNextPageToSwap;
					}
				}
				
				if( referenced )
				{	//Clear referenced bit and pass on this page for now
					VMBASESetPageReferenced( vaddr, FALSE );
				}
				
				if( startPage == g_vmNextPageToSwap )
				{	//We've cycled - use first page in lowest non-empty group
					if( firstPageInGroup1 >= 0 ) {
						pageToReplace = (u32)firstPageInGroup1;
					}
					else if( firstPageInGroup2 >= 0 ) {
						pageToReplace = (u32)firstPageInGroup2;
					}
					else if( firstPageInGroup3 >= 0 ) {
						pageToReplace = (u32)firstPageInGroup3;
					}
					else {
						ASSERTMSG( 0, "All pages are locked! Can't find one to swap out." );
					}
					break;
				}

				g_vmNextPageToSwap++;
				if( g_vmNextPageToSwap >= __VMGetNumPagesInMRAM() ) {
					g_vmNextPageToSwap = 0;
				}
			}
			else
			{	//Page not in use
				pageToReplace = g_vmNextPageToSwap;
				break;
			}
		}
	}
	else {
		pageToReplace = g_vmNextPageToSwap;
	}

	g_vmNextPageToSwap++;
	if( g_vmNextPageToSwap >= __VMGetNumPagesInMRAM() ) {
		g_vmFreePagesExist = FALSE;
		g_vmNextPageToSwap = 0;
	}
	return( pageToReplace );
}


/*---------------------------------------------------------------------------*
  Name:         __VMPageReplacementRandom

  Description:  The Random page replacement algorithm.

  Arguments:    none.

  Returns:      u32     Page to replace.
 *---------------------------------------------------------------------------*/
u32 __VMPageReplacementRandom( void )
{
	while(1)
	{
		u32 pageToReplace;
		
		if( g_vmFreePagesExist )
		{	//Fill up free pages first
			pageToReplace = g_vmNextPageToSwap++;
			if( g_vmNextPageToSwap >= __VMGetNumPagesInMRAM() ) {
				g_vmFreePagesExist = FALSE;
				g_vmNextPageToSwap = 0;
			}
		}
		else
		{	//TODO: Perhaps use a better random number generator
			pageToReplace = OSGetTick()%__VMGetNumPagesInMRAM();
		}
		
		if( !VMBASEIsPageLocked( pageToReplace ) )
		{	//Page isn't locked - use this one
			return( pageToReplace );
		}
	}
}


/*---------------------------------------------------------------------------*
  Name:         __VMPageReplacementFIFO

  Description:  The FIFO page replacement algorithm. This is the simplest
                page replacement algorithm, but it sometimes performs badly
                and suffers from Belady's anomaly.

  Arguments:    none.

  Returns:      u32     Page to replace.
 *---------------------------------------------------------------------------*/
u32 __VMPageReplacementFIFO( void )
{
	while(1)
	{
		u32 pageToReplace = g_vmNextPageToSwap++;
		if( g_vmNextPageToSwap >= __VMGetNumPagesInMRAM() ) {
			g_vmFreePagesExist = FALSE;
			g_vmNextPageToSwap = 0;
		}
		
		if( !VMBASEIsPageLocked( pageToReplace ) )
		{	//Page isn't locked - use this one
			return( pageToReplace );
		}
	}
}

