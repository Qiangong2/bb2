/*---------------------------------------------------------------------------*
  Project:  VM
  File:     VMMapping.c
 
  Copyright 2002 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: VMMapping.c,v $
  Revision 1.1.1.1  2004/04/20 05:29:07  paulm
  Virtual Memory library from Nintendo

 * 
 * 9     10/02/02 4:19p Stevera
 * Added asserts to verify that the VM library is initialized.
 * 
 * 8     9/05/02 1:12p Stevera
 * Changed all function calls into the VMBase library to start with the
 * prefix VMBASE.
 * 
 * 7     9/04/02 5:51p Stevera
 * Added three more asserts to VMAlloc to check for invalid arguments.
 * 
 * 6     5/09/02 5:53p Stevera
 * Added __VMMappingErrorAlert.
 * 
 * 5     5/07/02 11:40a Stevera
 * Added more comments.
 * 
 * 4     4/30/02 3:14p Stevera
 * Added concept of ARAM page being dirty (so that ARAM pages aren't
 * pulled into MRAM when they have garbage data).
 * 
 * 3     4/29/02 11:46a Stevera
 * RC2 for testing
 *     
  
  $NoKeywords: $

 *---------------------------------------------------------------------------*/



/*---------------------------------------------------------------------------*
  Includes
 *---------------------------------------------------------------------------*/
#include "VMMappingPrivate.h"
#include "VMPrivate.h"
#include <dolphin/vm.h>
#include <dolphin/vmbase.h>
#include <dolphin.h>
#include <stdio.h>


/*---------------------------------------------------------------------------*
  Local definitions & declarations
 *---------------------------------------------------------------------------*/
#define VM_LUT_ARAM_TO_VM_SIZE 0x4000		//16K
#define VM_LUT_VM_TO_ARAM_SIZE 0x8000		//32K

//The dirty bit is superimposed onto the addresses in the g_baseVMtoARAM LUT
#define VM_ARAM_DIRTY_BIT_MASK 0x7FFFFFFF
#define VM_ARAM_DIRTY_BIT      0x80000000

static u32 g_baseARAMtoVM = 0;				//Base address of the ARAM to VM LUT
static u32 g_baseVMtoARAM = 0;				//Base address of the VM to ARAM LUT
static s32 g_totalAllocatedVM = 0;			//Total allocated memory in VM


/*---------------------------------------------------------------------------*
  Private functions
 *---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*
  Name:         VMAlloc

  Description:  Requests allocation of a range of virtual memory. The effect is
                to map VM pages to ARAM pages. Other than that, no bookkeeping 
                of allocated chunks is performed.

  Arguments:    u32    virtualAddr		Address in range of 0x7E000000 to 0x80000000.
                                        Must be 4K aligned.
                                        
                u32    size				Size in bytes.
                                        Must be multiple of 4K.

  Returns:      BOOL   Returns whether the function was successful.
 *---------------------------------------------------------------------------*/
BOOL VMAlloc( u32 virtualAddr, u32 size )
{
	static u32 g_nextARAMPageToCheck = 0;
	
	u32 i;
	u32 minARAMPage = VMGetARAMBase()/VM_PAGE_SIZE;
	u32 maxARAMPage = VMGetARAMSize()/VM_PAGE_SIZE + minARAMPage;
	
	if( g_nextARAMPageToCheck < minARAMPage ) {
		g_nextARAMPageToCheck = minARAMPage;
	}

	ASSERTMSG( VMIsInitialized() == TRUE, "VMAlloc - VMInit must first be called." );
	ASSERTMSG( (virtualAddr & 0xFFF) == 0, "VMAlloc - virtualAddr must be 4K aligned" );
	ASSERTMSG( (size & 0xFFF) == 0, "VMAlloc - size must be a multiple of 4K" );
	ASSERTMSG( VMGetARAMSize() >= g_totalAllocatedVM, "VMAlloc - Current allocated bytes exceeds physical size." );
	ASSERTMSG( virtualAddr >= 0x7E000000, "VMAlloc - virtualAddr must be between 0x7E000000 and 0x7FFFF000" );
	ASSERTMSG( virtualAddr <= 0x7FFFF000, "VMAlloc - virtualAddr must be between 0x7E000000 and 0x7FFFF000" );
	ASSERTMSG( virtualAddr + size <= 0x80000000, "VMAlloc - allocated range must not exceed 0x80000000" );

	if( g_totalAllocatedVM + size > VMGetARAMSize() )
	{	//Not enough space in ARAM
		return( FALSE );
	}

	//Step through pages that must be allocated
	for( i=0; i<size; i+=VM_PAGE_SIZE )
	{
		u32 curVirtualAddrToMap = virtualAddr+i;
		u32 infiniteLoopCheck = 0;
		
		ASSERTMSG( !__VMDoesMappingExist( curVirtualAddrToMap ), "VMAlloc - New range to allocate overlaps a previous range." );
		
		while(1)
		{	//Find a free ARAM page
			g_nextARAMPageToCheck++;
			if( g_nextARAMPageToCheck >= maxARAMPage ) {
				g_nextARAMPageToCheck = minARAMPage;
				infiniteLoopCheck++;
				ASSERTMSG( infiniteLoopCheck < 2, "VMAlloc - Infinite loop detected in trying to find a free ARAM page." );
			}
						
			if( *(u32*)(g_baseARAMtoVM+(g_nextARAMPageToCheck*4)) == 0 )
			{	//Map cur VM address to this free ARAM page
				*(u32*)(g_baseARAMtoVM+(g_nextARAMPageToCheck*4)) = curVirtualAddrToMap;
				*(u32*)(g_baseVMtoARAM+((curVirtualAddrToMap&0x01FFF000)/VM_PAGE_SIZE)*4) = (g_nextARAMPageToCheck*VM_PAGE_SIZE);
				break;
			}
		}
				
		g_totalAllocatedVM += 0x1000;
	}
	
	return( TRUE );
	
}


/*---------------------------------------------------------------------------*
  Name:         VMFree

  Description:  Requests deallocation of a range of virtual memory.

  Arguments:    u32    virtualAddr		Address in range of 0x7E000000 to 0x80000000.
                                        Must be 4K aligned.
                                        
                u32    size				Size in bytes.
                                        Must be 4K aligned.

  Returns:      BOOL   Returns whether the function was successful.
 *---------------------------------------------------------------------------*/
BOOL VMFree( u32 virtualAddr, u32 size )
{
	u32 addr, vaddrCur, vaddrEnd;
	u32 startLUT = g_baseVMtoARAM + (((virtualAddr&0x01FFF000)/VM_PAGE_SIZE)*4);
	u32 endLUT = startLUT + ((size/VM_PAGE_SIZE)*4);
	BOOL old;

	ASSERTMSG( VMIsInitialized() == TRUE, "VMFree - VMInit must first be called." );
	ASSERTMSG( (virtualAddr & 0xFFF) == 0, "VMFree - virtualAddr must be 4K aligned" );
	ASSERTMSG( (size & 0xFFF) == 0, "VMFree - size must be 4K aligned" );

	//Remove the range from VMtoARAM and ARAMtoVM look up tables
	for( addr=startLUT; addr<endLUT; addr+=4 )
	{
		if( *(u32*)addr == 0 ) {
			ASSERTMSG( 0, "VMFree - Trying to free unallocated virtual memory." );
			return( FALSE );
		}
		*(u32*)(g_baseARAMtoVM + (((*(u32*)addr & VM_ARAM_DIRTY_BIT_MASK)/VM_PAGE_SIZE)*4) ) = 0x00000000;
		*(u32*)addr = 0x00000000;
		g_totalAllocatedVM -= 0x1000;
	}
	
	old = OSDisableInterrupts();
	
	//Remove the range from the Page Table
	vaddrEnd = virtualAddr+size;
	for( vaddrCur=virtualAddr; vaddrCur<vaddrEnd; vaddrCur+=VM_PAGE_SIZE )
	{
		//Check if valid page entry
		if( VMBASEIsPageValid( vaddrCur ) )
		{
			//Remove from Page Table
			u32 pageNum = (VMBASEGetPhysicalAddrInMRAM( vaddrCur ) - __VMGetMRAMBase())/VM_PAGE_SIZE;
			VMBASEClearPageTableEntry( vaddrCur, pageNum );
		}
	}
	
	OSRestoreInterrupts(old);
	
	return( TRUE );

}


/*---------------------------------------------------------------------------*
  Name:         VMFreeAll

  Description:  Frees all of allocated VM. As a result, all of the pages in
  				memory are invalidated.

  Arguments:    none.

  Returns:      none.
 *---------------------------------------------------------------------------*/
void VMFreeAll( void )
{
	u32 i;
	
	ASSERTMSG( VMIsInitialized() == TRUE, "VMFreeAll - VMInit must first be called." );

	for( i=0; i<VM_LUT_VM_TO_ARAM_SIZE; i=i+4 ) {
		*(u32*)(g_baseVMtoARAM + i) = 0x00000000;
	}
	
	for( i=0; i<VM_LUT_ARAM_TO_VM_SIZE; i=i+4 ) {
		*(u32*)(g_baseARAMtoVM + i) = 0x00000000;
	}
	
	g_totalAllocatedVM = 0;
	
	VMInvalidateAllPages();

}


/*---------------------------------------------------------------------------*
  Name:         VMGetNumUnallocatedBytes

  Description:  Gets the number of unallocated bytes available in VM (ARAM)

  Arguments:    none.

  Returns:      u32   Returns number of free bytes
 *---------------------------------------------------------------------------*/
u32 VMGetNumUnallocatedBytes( void )
{
	ASSERTMSG( VMIsInitialized() == TRUE, "VMGetNumUnallocatedBytes - VMInit must first be called." );
	ASSERTMSG( VMGetARAMSize() >= g_totalAllocatedVM, "VMGetNumUnallocatedBytes - Allocated bytes exceeds physical size." );
	return( VMGetARAMSize() - g_totalAllocatedVM );
}


/*---------------------------------------------------------------------------*
  Name:         __VMTranslateVMPageToARAMPage

  Description:  Translates the VM address for a page to an ARAM address for a page.

  Arguments:    u32   virtualAddress.

  Returns:      u32   Returns ARAM page address
 *---------------------------------------------------------------------------*/
u32 __VMTranslateVMPageToARAMPage( u32 virtualAddress )
{
	u32 addrLUT = g_baseVMtoARAM + (((virtualAddress&0x01FFF000)/VM_PAGE_SIZE)*4);
	u32 addrARAM = *(u32*)addrLUT & VM_ARAM_DIRTY_BIT_MASK;	//Mask off upper bit
	if( addrARAM != 0 ) {
		return( addrARAM );
	}
	else {
		__VMMappingErrorAlert( virtualAddress );
		return(0);
	}

}


/*---------------------------------------------------------------------------*
  Name:         __VMDoesMappingExist

  Description:  Returns whether a mapping exists for this virtual address.

  Arguments:    u32   virtualAddress

  Returns:      BOOL   Returns whether a mapping exists for this virtual address
 *---------------------------------------------------------------------------*/
BOOL __VMDoesMappingExist( u32 virtualAddress )
{
	u32 addrLUT = g_baseVMtoARAM + (((virtualAddress&0x01FFF000)/VM_PAGE_SIZE)*4);
	u32 addrARAM = *(u32*)addrLUT & VM_ARAM_DIRTY_BIT_MASK;	//Mask off upper bit
	if( addrARAM != 0 ) {
		return( TRUE );
	}
	else {
		return( FALSE );
	}

}


/*---------------------------------------------------------------------------*
  Name:         __VMMappingErrorAlert

  Description:  OSReports a mapping error and halts processing

  Arguments:    u32   virtualAddress.

  Returns:      none.
 *---------------------------------------------------------------------------*/
void __VMMappingErrorAlert( u32 virtualAddress )
{
	char msg[1024];
	sprintf( msg, "Virtual address (%x) has not been allocated. "
				  "Call VMAlloc on virtual address ranges "
				  "before using them.", virtualAddress );
				  
	ASSERTMSG( 0, msg );
	
	PPCHalt();
}


/*---------------------------------------------------------------------------*
  Name:         __VMSetARAMPageAsDirty

  Description:  Marks an ARAM page as dirty.

  Arguments:    u32   virtualAddress

  Returns:      none.
 *---------------------------------------------------------------------------*/
void __VMSetARAMPageAsDirty( u32 virtualAddress )
{
	u32 addrLUT = g_baseVMtoARAM + (((virtualAddress&0x01FFF000)/VM_PAGE_SIZE)*4);
	*(u32*)addrLUT = *(u32*)addrLUT | VM_ARAM_DIRTY_BIT;
}


/*---------------------------------------------------------------------------*
  Name:         __VMIsARAMPageDirty

  Description:  Checks whether an ARAM page is dirty.

  Arguments:    u32   virtualAddress

  Returns:      BOOL  Returns whether the ARAM page is dirty.
 *---------------------------------------------------------------------------*/
BOOL __VMIsARAMPageDirty( u32 virtualAddress )
{
	u32 addrLUT = g_baseVMtoARAM + (((virtualAddress&0x01FFF000)/VM_PAGE_SIZE)*4);
	if( *(u32*)addrLUT & VM_ARAM_DIRTY_BIT ) {
		return( TRUE );
	}
	else {
		return( FALSE );
	}
}


/*---------------------------------------------------------------------------*
  Name:         __VMAllocVirtualToARAMLUT

  Description:  Allocates the LUT for Virtual -> ARAM mappings
                (32MB/4K pages = 8K pages)

  Arguments:    none.

  Returns:      none.
 *---------------------------------------------------------------------------*/
void __VMAllocVirtualToARAMLUT( void )
{
	u32 i;

	void*  arenaLo = OSGetArenaLo();
	g_baseVMtoARAM = (u32)arenaLo;
	OSSetArenaLo( (void*)((u32)arenaLo + VM_LUT_VM_TO_ARAM_SIZE) );	
	for( i=0; i<VM_LUT_VM_TO_ARAM_SIZE; i=i+4 ) {
		*(u32*)((u32)g_baseVMtoARAM + i) = 0x00000000;
	}
}


/*---------------------------------------------------------------------------*
  Name:         __VMAllocARAMToVirtualLUT

  Description:  Allocates the LUT for ARAM -> Virtual mappings
                (16MB/4K pages = 4K pages)

  Arguments:    none.

  Returns:      none.
 *---------------------------------------------------------------------------*/
void __VMAllocARAMToVirtualLUT( void )
{
	u32 i;

	void* arenaLo = OSGetArenaLo();
	g_baseARAMtoVM = (u32)arenaLo;
	OSSetArenaLo( (void*)((u32)arenaLo + VM_LUT_ARAM_TO_VM_SIZE) );	
	for( i=0; i<VM_LUT_ARAM_TO_VM_SIZE; i=i+4 ) {
		*(u32*)((u32)g_baseARAMtoVM + i) = 0x00000000;
	}
}
