/*---------------------------------------------------------------------------*
  Project:  VM
  File:     VM.c
 
  Copyright 2002 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: VM.c,v $
  Revision 1.1.1.1  2004/04/20 05:29:07  paulm
  Virtual Memory library from Nintendo

 * 
 * 26    3/26/03 10:39a Stevera
 * Added extra assert in VMInit.
 * 
 * 25    3/26/03 10:17a Stevera
 * Fixed bug with instruction cache not being invalidated.
 * 
 * 24    10/02/02 4:20p Stevera
 * Added function VMIsInitialized. Added asserts to verify that the VM
 * library is initialized.
 * 
 * 23    9/24/02 2:50p Stevera
 * Renamed function VMWriteBackOneDirtyPageSync to VMStoreOnePage.
 * 
 * 22    9/24/02 1:32p Stevera
 * Removed argument checking for Release builds.
 * 
 * 21    9/17/02 6:20p Stevera
 * Added function VMInvalidateRange. Fixed a bug in the expression of
 * several asserts.
 * 
 * 20    9/16/02 4:37p Stevera
 * Renamed VMFlushAllPages to VMStoreAllPages in order to use correct
 * terminology.
 * 
 * 19    9/10/02 6:29p Stevera
 * Changed function call from VMBASEInvalidatePageTable() to
 * VMBASEInvalidateAllPages(). Changed function call from
 * VMBASEGetVirtualAddressFromPageInMRAM() to
 * VMBASEGetVirtualAddrFromPageInMRAM(). 
 * 
 * 18    9/10/02 4:50p Stevera
 * Changed function call from VMBASEFlushAllPagesInternal() to
 * VMBASEFlushAllPages().
 * 
 * 17    9/05/02 1:29p Stevera
 * Fixed a typo in an assert message.
 * 
 * 16    9/05/02 1:12p Stevera
 * Changed all function calls into the VMBase library to start with the
 * prefix VMBASE.
 * 
 * 15    5/10/02 3:50p Stevera
 * Changed stats latency calculation from milliseconds to microseconds.
 * 
 * 14    5/09/02 5:52p Stevera
 * Changed verify mapping functions in VMSwapPageOut.
 * 
 * 13    5/07/02 3:11p Stevera
 * Fixed some function comments.
 * 
 * 12    5/07/02 11:48a Stevera
 * Renamed some variables, added more comments.
 * 
 * 11    5/06/02 4:07p Stevera
 * Added VMWriteBackOneDirtyPageSync function.
 * 
 * 10    5/02/02 3:46p Stevera
 * Added a public header to expose the page replacement functions.
 * 
 * 9     5/01/02 2:59p Stevera
 * Removed stats tracking from VM library. Added optional stats logging
 * callback.
 * 
 * 8     4/30/02 3:15p Stevera
 * Added concept of ARAM page being dirty (so that ARAM pages aren't
 * pulled into MRAM when they have garbage data).
 * 
 * 7     4/29/02 11:46a Stevera
 * RC2 for testing
 *     
  
  $NoKeywords: $

 *---------------------------------------------------------------------------*/



/*---------------------------------------------------------------------------*
  Includes
 *---------------------------------------------------------------------------*/
#include "VMPrivate.h"
#include "VMMappingPrivate.h"
#include "VMPageReplacementPrivate.h"
#include <dolphin/vm.h>
#include <dolphin/vmbase.h>
#include <dolphin.h>


/*---------------------------------------------------------------------------*
  Local definitions & declarations
 *---------------------------------------------------------------------------*/
#define VM_MAX_SIZE_MRAM 0x1000000	//16MB	(It doesn't make sense for this to be 
                                    //       bigger than ARAM. Also several LUTs are 
                                    //       dependant on 16MB being the maximum size.)

#define VM_MIN_BASE_ARAM 0x4000		//16K	(The first 16K of ARAM is reserved
                                    //       for audio use.)
                                            
#define VM_MAX_ARAM_SIZE 0xFFC000	//16MB-16K (Size of ARAM minus the min base)


static u32 g_vmSizeVMMainMemory = 0;			//size of page swapping space in MRAM
static u32 g_vmBaseVMMainMemory = 0;			//base address of page swapping space in MRAM
static u32 g_vmSizeVMARAM = 0;					//size of ARAM backing store for pages
static u32 g_vmBaseVMARAM = VM_MIN_BASE_ARAM;	//base address of ARAM backing store for pages
static u32 g_vmNumPagesInMRAM = 0;				//number of pages that MRAM can hold at one time
static VM_LogStats g_cbLogStats = 0;			//callback function for logging VM statistics
static BOOL g_vmInitialized = FALSE;			//initialized flag for the VM library


/*---------------------------------------------------------------------------*
  Prototypes of Private Functions
 *---------------------------------------------------------------------------*/
u32  __VMTranslateVMtoARAM( u32 addr );
u32  __VMTranslateARAMtoVM( u32 addr );
void __VMAllocMRAMSwapSpace( void );
void __VMSwapPageIn( u32 exactVirtualAddress );
void __VMSwapPageOut( u32 virtualAddress );
u32  __VMGetPhysicalAddrOfPageInMRAM( u32 pageNumber );


/*---------------------------------------------------------------------------*
  Private functions
 *---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*
  Name:         VMInit

  Description:  Initializes Virtual Memory. It will allocate the MRAM, but not
                the ARAM. Call this function before memory is removed from the
                arena. 

  Arguments:    u32     sizeVMMRAM      VMInit will allocate this amount of memory 
                                        from the arena for holding pages in MRAM.
  
                u32     baseVMARAM      Base of VM mapped memory in ARAM.
                
                u32     sizeVMARAM      Size of VM mapped memory in ARAM.
                                        
  Returns:      None.
 *---------------------------------------------------------------------------*/
void VMInit( u32 sizeVMMainMemory, u32 baseVMARAM, u32 sizeVMARAM )
{	
	if( !g_vmInitialized )
	{
		BOOL old = OSDisableInterrupts();
		
		g_vmInitialized = TRUE;
		g_vmBaseVMARAM = baseVMARAM;
		g_vmSizeVMARAM = sizeVMARAM;
		g_vmSizeVMMainMemory = sizeVMMainMemory;
		g_vmNumPagesInMRAM = g_vmSizeVMMainMemory/VM_PAGE_SIZE;

		ASSERTMSG( OSGetArenaLo() != OSGetArenaHi(), "VMInit - VMInit must allocate memory from the arena. The game's memory heap should be created after VMInit.\n" );
		ASSERTMSG( baseVMARAM + sizeVMARAM <= 0x1000000, "VMInit: baseVMARAM + sizeVMARAM must be <= 16MB" );
		ASSERTMSG( baseVMARAM >= VM_MIN_BASE_ARAM, "VMInit - baseVMARAM argument must be at or above 0x4000.\n" );
		ASSERTMSG( (baseVMARAM & 0xFFF) == 0, "VMInit - baseVMARAM argument must be 4K aligned.\n" );
		ASSERTMSG( sizeVMARAM <= VM_MAX_ARAM_SIZE, "VMInit - sizeVMARAM argument must be <= 0xFFC000 (16MB-16K).\n" );
		ASSERTMSG( sizeVMMainMemory <= VM_MAX_SIZE_MRAM, "VMInit - The allocated size of main memory for VM must be <= 16MB" );
		
		//Setup the page table and prepare the CPU for VM
		VMBASEInit( __VMSwapPageIn );
		
	    //Allocate and initialize the LUTs for Virtual <-> ARAM mappings
	    __VMAllocVirtualToARAMLUT();
	    __VMAllocARAMToVirtualLUT();
	    
	    //Allocate the physical swap space for pages in MRAM
		__VMAllocMRAMSwapSpace();
		
		OSRestoreInterrupts( old );
	}
	else {
		ASSERTMSG( 0, "VMInit - You may only initialize Virtual Memory once" );
	}
}


/*---------------------------------------------------------------------------*
  Name:         VMIsInitialized

  Description:  Checks whether the VM library has been initialized.

  Arguments:    None.
                                        
  Returns:      BOOL	TRUE if VM is initialized. 

 *---------------------------------------------------------------------------*/
BOOL VMIsInitialized( void )
{
	return( g_vmInitialized );
}


/*---------------------------------------------------------------------------*
  Name:         VMQuit

  Description:  Turns off VM and restores the original CPU state.
                This function does not try to reclaim allocated memory from
				either VMBase or VM.

  Arguments:    None.

  Returns:      None.
 *---------------------------------------------------------------------------*/
void VMQuit( void )
{
	if( g_vmInitialized == TRUE )
	{
		VMBASEQuit();

		//Reset initial conditions
		__VMSetFreePagesExist( TRUE );
		__VMSetNextPageToSwap(0);
		VMSetPageReplacementPolicy( VM_PRP_RANDOM );

		//Reset all global variables
		g_vmSizeVMMainMemory = 0;
		g_vmBaseVMMainMemory = 0;
		g_vmSizeVMARAM = 0;
		g_vmBaseVMARAM = VM_MIN_BASE_ARAM;
		g_vmNumPagesInMRAM = 0;
		g_cbLogStats = 0;
		g_vmInitialized = FALSE;
	}
	else {
		ASSERTMSG( 0, "VMQuit - VM must be initialized before calling this function." );
	}
}


/*---------------------------------------------------------------------------*
  Name:         VMResizeARAM

  Description:  Resizes the ARAM portion of VM. This will also free all allocated
                pages and invalidate all pages currently stored in MRAM. 

  Arguments:    u32     baseVMARAM      Base of VM mapped memory in ARAM.
                
                u32     sizeVMARAM      Size of VM mapped memory in ARAM.

  Returns:      None.
 *---------------------------------------------------------------------------*/
void VMResizeARAM( u32 baseVMARAM, u32 sizeVMARAM )
{
	ASSERTMSG( g_vmInitialized == TRUE, "VMResizeARAM - VMInit must first be called." );

	g_vmBaseVMARAM = baseVMARAM;
	g_vmSizeVMARAM = sizeVMARAM;

	ASSERTMSG( baseVMARAM >= VM_MIN_BASE_ARAM, "VMResizeARAM - baseVMARAM argument must be higher than 0x4000.\n" );
	ASSERTMSG( (baseVMARAM & 0xFFF) == 0, "VMResizeARAM - baseVMARAM argument must be 4K aligned.\n" );
	ASSERTMSG( sizeVMARAM <= VM_MAX_ARAM_SIZE, "VMResizeARAM - sizeVMARAM argument must be <= 0xFFC000 (16MB-16K).\n" );

	VMFreeAll();
}


/*---------------------------------------------------------------------------*
  Name:         VMSetLogStatsCallback

  Description:  Sets the log stats callback function

  Arguments:    VM_LogStats		cbLogStats

  Returns:      none.
 *---------------------------------------------------------------------------*/
void VMSetLogStatsCallback( VM_LogStats cbLogStats )
{
	ASSERTMSG( g_vmInitialized == TRUE, "VMSetLogStatsCallback - VMInit must first be called." );
	g_cbLogStats = cbLogStats;
}


/*---------------------------------------------------------------------------*
  Name:         __VMGetPhysicalAddrOfPageInMRAM

  Description:  Gets the physical address in MRAM of the page

  Arguments:    none.

  Returns:      u32   Returns physical address in MRAM of the page
 *---------------------------------------------------------------------------*/
u32 __VMGetPhysicalAddrOfPageInMRAM( u32 pageNumber )
{
	return( g_vmBaseVMMainMemory + (pageNumber*VM_PAGE_SIZE) );
}


/*---------------------------------------------------------------------------*
  Name:         __VMGetNumPagesInMRAM

  Description:  Gets the number of pages in main RAM.

  Arguments:    none.

  Returns:      u32    number of pages in MRAM
 *---------------------------------------------------------------------------*/
u32 __VMGetNumPagesInMRAM( void )
{
	return( g_vmNumPagesInMRAM );
}


/*---------------------------------------------------------------------------*
  Name:         VMGetARAMSize

  Description:  Gets the size of ARAM

  Arguments:    none.

  Returns:      u32    size of ARAM
 *---------------------------------------------------------------------------*/
u32 VMGetARAMSize( void )
{
	ASSERTMSG( g_vmInitialized == TRUE, "VMGetARAMSize - VMInit must first be called." );
	return( g_vmSizeVMARAM );
}


/*---------------------------------------------------------------------------*
  Name:         VMGetARAMBase

  Description:  Gets the base of ARAM

  Arguments:    none.

  Returns:      u32    base of ARAM
 *---------------------------------------------------------------------------*/
u32 VMGetARAMBase( void )
{
	ASSERTMSG( g_vmInitialized == TRUE, "VMGetARAMBase - VMInit must first be called." );
	return( g_vmBaseVMARAM );
}


/*---------------------------------------------------------------------------*
  Name:         __VMGetMRAMBase

  Description:  Gets the base of MRAM swap space

  Arguments:    none.

  Returns:      u32    base of MRAM
 *---------------------------------------------------------------------------*/
u32 __VMGetMRAMBase( void )
{
	return( g_vmBaseVMMainMemory );
}


/*---------------------------------------------------------------------------*
  Name:         __VMAllocMRAMSwapSpace

  Description:  Allocates the MRAM swap space for pages.

  Arguments:    none.

  Returns:      none.
 *---------------------------------------------------------------------------*/
void __VMAllocMRAMSwapSpace( void )
{
	void* arenaLo = OSGetArenaLo();
	g_vmBaseVMMainMemory = (u32)arenaLo;
	OSSetArenaLo( (void*)((u32)arenaLo + g_vmSizeVMMainMemory) );
}


/*---------------------------------------------------------------------------*
  Name:         __VMSwapPageIn

  Description:  This function will find a page to replace, save off the page if
                it was changed, load in the new page, and update the page table.
                It then returns and the original context is loaded so that 
                execution of the game can continue now that the page is in memory.

  Arguments:    u32    exactVirtualAddress	This is the exact address that caused
                                            the page fault (not the page aligned address).

  Returns:      none.
 *---------------------------------------------------------------------------*/
void __VMSwapPageIn( u32 exactVirtualAddress )
{
	u32 statsStartTime = (u32)OSTicksToMicroseconds( OSGetTime() );
	BOOL statsPageSwappedOut = FALSE;
	u32 virtualAddress = exactVirtualAddress & 0xFFFFF000; //Round down to 4K alignment
	BOOL interruptPending = FALSE;
	BOOL old = FALSE;
	u32 ptaddr = 0;
	

	//Find which page to replace
	u32 pageToReplace = __VMGetPageToReplace();
	u32 physicalAddress = __VMGetPhysicalAddrOfPageInMRAM( pageToReplace );
	u32 vaddrOldPage = VMBASEGetVirtualAddrFromPageInMRAM( pageToReplace );
	
	old = OSDisableInterrupts();	 
	while( ARGetDMAStatus() != 0 ) {} //Wait for DMA to finish
	interruptPending = __ARGetInterruptStatus();
	
	if( vaddrOldPage != 0 )
	{	//Page being replaced must be removed from the page table
		
		if( VMBASEIsPageDirty( vaddrOldPage ) )
		{	//Page was changed, copy back to ARAM
			__VMSetARAMPageAsDirty( vaddrOldPage );
			statsPageSwappedOut = TRUE;
			DCFlushRange( (void*)physicalAddress, VM_PAGE_SIZE );
			ARStartDMAWrite( physicalAddress, __VMTranslateVMPageToARAMPage( vaddrOldPage ), VM_PAGE_SIZE );
			while( ARGetDMAStatus() != 0 ) {} //Wait for DMA to finish
		}
		//Remove page from page table
		VMBASEClearPageTableEntry( vaddrOldPage, pageToReplace );
	}

	if( __VMIsARAMPageDirty( virtualAddress ) )
	{	//Transfer ARAM page to physical memory
		ARStartDMARead( physicalAddress, __VMTranslateVMPageToARAMPage( virtualAddress ), VM_PAGE_SIZE );	
		while( ARGetDMAStatus() != 0 ) {} //Wait for DMA to finish
		// Invalidate both data and instruction caches after the read.
		DCInvalidateRange( (void*)physicalAddress, VM_PAGE_SIZE );
		ICInvalidateRange( (void*)physicalAddress, VM_PAGE_SIZE );
	}
	else if( !__VMDoesMappingExist( virtualAddress ) ) {
		__VMMappingErrorAlert( virtualAddress );
	}
	
	if( !interruptPending )
	{	//interruptPending==FALSE, Clear ARAM DMA IRQ
		__ARClearInterrupt();
	}

	//Mark page table with new info
	VMBASESetPageTableEntry( virtualAddress, physicalAddress, pageToReplace );
		
	OSRestoreInterrupts(old);
	
	
	if( g_cbLogStats )
	{	//Keep some statistics on what has happened
		u32 statsPageMissLatency = (u32)OSTicksToMicroseconds( OSGetTime() ) - statsStartTime;
		g_cbLogStats( exactVirtualAddress, physicalAddress, pageToReplace, 
		              statsPageMissLatency, statsPageSwappedOut );
	}
	
}


/*---------------------------------------------------------------------------*
  Name:         __VMSwapPageOut

  Description:  Swaps a page out to ARAM

  Arguments:    u32     virtualAddress

  Returns:      None.
 *---------------------------------------------------------------------------*/
void __VMSwapPageOut( u32 virtualAddress )
{
	u32  physicalAddress = VMBASEGetPhysicalAddrInMRAM( virtualAddress );
	BOOL interruptPending = FALSE;
	BOOL old = OSDisableInterrupts();

	while( ARGetDMAStatus() != 0 ) {} //Wait for DMA to finish
	
	interruptPending = __ARGetInterruptStatus();
	
	DCFlushRange( (void*)physicalAddress, VM_PAGE_SIZE );
	ARStartDMAWrite( physicalAddress, __VMTranslateVMPageToARAMPage( virtualAddress ), VM_PAGE_SIZE );
	//Wait for DMA to finish
	while( ARGetDMAStatus() != 0 ) {}

	//if interruptPending==FALSE, Clear ARAM DMA IRQ
	if( !interruptPending ) {
		__ARClearInterrupt();
	}
		
	OSRestoreInterrupts(old);
	
	//Mark ARAM page as dirty
	__VMSetARAMPageAsDirty( virtualAddress );

}


/*---------------------------------------------------------------------------*
  Name:         VMStoreAllPages

  Description:  Stores all changed pages in MRAM to ARAM.

  Arguments:    None.

  Returns:      None.
 *---------------------------------------------------------------------------*/
void VMStoreAllPages( void )
{
	//The store needs to write back dirty pages to the proper spots in ARAM.
	//The callback function supplied will get called for each dirty page
	//that needs to be written back to ARAM.
	ASSERTMSG( g_vmInitialized == TRUE, "VMStoreAllPages - VMInit must first be called." );
	VMBASEStoreAllPages( __VMSwapPageOut );
}


/*---------------------------------------------------------------------------*
  Name:         VMInvalidateAllPages

  Description:  Invalidates all pages in MRAM.

  Arguments:    None.

  Returns:      None.
 *---------------------------------------------------------------------------*/
void VMInvalidateAllPages( void )
{
	ASSERTMSG( g_vmInitialized == TRUE, "VMInvalidateAllPages - VMInit must first be called." );
	VMBASEInvalidateAllPages();
	
	//All pages in MRAM are now free pages.
	//(this helps the page replacement policy during ramp-up)
	__VMSetFreePagesExist( TRUE );
	__VMSetNextPageToSwap(0);
}


/*---------------------------------------------------------------------------*
  Name:         VMInvalidateRange

  Description:  Invalidates a range of pages in MRAM.

  Arguments:    u32		virtualAddress
				u32		range

  Returns:      None.
 *---------------------------------------------------------------------------*/
void VMInvalidateRange( u32 virtualAddress, u32 range )
{
	u32 vaddr;
	u32 finalAddress = virtualAddress + range;

	ASSERTMSG( g_vmInitialized == TRUE, "VMInvalidateRange - VMInit must first be called." );
	ASSERTMSG( virtualAddress >= 0x7E000000, "VMInvalidateRange - virtualAddress argument must be >= 0x7E000000.\n" );
	ASSERTMSG( virtualAddress <= 0x7FFFF000, "VMInvalidateRange - virtualAddress must be <= 0x7FFFF000" );
	ASSERTMSG( (virtualAddress & 0xFFF) == 0, "VMInvalidateRange - virtualAddress argument must be 4K aligned.\n" );
	ASSERTMSG( (range & 0xFFF) == 0, "VMInvalidateRange - range argument must be multiple of 4K.\n" );
	ASSERTMSG( virtualAddress + range <= 0x80000000, "VMInvalidateRange - (virtualAddress + range) must not exceed 0x80000000" );

	for( vaddr=virtualAddress; vaddr<finalAddress; vaddr+=VM_PAGE_SIZE )
	{
		if( VMBASEIsPageValid( vaddr ) )
		{
			u32 physicalAddr = VMBASEGetPhysicalAddrInMRAM( vaddr );
			u32 pageNumber = (physicalAddr - g_vmBaseVMMainMemory) / VM_PAGE_SIZE;
			VMBASEClearPageTableEntry( vaddr, pageNumber );
		}
	}
}


/*---------------------------------------------------------------------------*
  Name:         VMStoreOnePage

  Description:  Finds a dirty page and writes it back synchonously. Call this 
                function when there is free time. This will speed up typical 
                VM access since fewer dirty pages will need to be written 
                back on-the-fly.

  Arguments:    None.

  Returns:      BOOL     Whether a page was written out to ARAM.
 *---------------------------------------------------------------------------*/
BOOL VMStoreOnePage( void )
{
	static u32 nextPageToCheck = 0;
	u32 count = 0;
	
	ASSERTMSG( g_vmInitialized == TRUE, "VMStoreOnePage - VMInit must first be called." );

	while( count++ <= g_vmNumPagesInMRAM )
	{
		u32 page = nextPageToCheck++;
		u32 vaddr = VMBASEGetVirtualAddrFromPageInMRAM( page );
		
		if( nextPageToCheck >= g_vmNumPagesInMRAM ) {
			nextPageToCheck = 0;
		}
				
		if( vaddr != 0 )
		{	//Virtual address valid
			if( VMBASEIsPageDirty( vaddr ) )
			{	//Found dirty page
				__VMSwapPageOut( vaddr );
				VMBASESetPageDirty( vaddr, FALSE );
				return( TRUE );
			}
		}
	}
	
	return( FALSE );
}




