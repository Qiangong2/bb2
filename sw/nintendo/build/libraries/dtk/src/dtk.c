#include <dolphin.h>
#include <dolphin/dtk.h>

/*---------------------------------------------------------------------------*
 * Globals
 *---------------------------------------------------------------------------*/
static DTKTrack            *__DTKCurrentTrack;
static DTKTrack            *__DTKPlayListHead;
static DTKTrack            *__DTKPlayListTail;

static vu32                __DTKState;
static vu32                __DTKTempState;
static vu32                __DTKRepeatMode;
static vu32                __DTKPosition;
static vu32                __DTKInterruptFrequency;

static vu8                 __DTKVolumeL;
static vu8                 __DTKVolumeR;                   

static vu32                __DTKShutdownFlag;
static vu32                __DTKTrackEnded;

static DTKFlushCallback    __DTKFlushCallback;

#ifdef _DEBUG
static BOOL                __busy_for_ais_address = FALSE;
#endif

/*---------------------------------------------------------------------------*
 * Function definitions
 *---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*
 * Name        : __DTKStatrAI
 *
 * Description : Start the playback 
 *               
 * Arguments   : None
 *
 * Returns     : None
 *---------------------------------------------------------------------------*/
static void __DTKStartAi(void)
{
    // set volume 
    AISetStreamVolLeft(__DTKVolumeL);
    AISetStreamVolRight(__DTKVolumeR);

    // start the DAC clock
   	AIResetStreamSampleCount();
   	AISetStreamTrigger(__DTKInterruptFrequency);
   	AISetStreamPlayState(AI_STREAM_START);
}


/*---------------------------------------------------------------------------*
 * Name        : __DTKStopAI
 *
 * Description : Stop the playback 
 *               
 * Arguments   : None
 *
 * Returns     : None
 *---------------------------------------------------------------------------*/
static void __DTKStopAi(void)
{
    // set volume to 0
    AISetStreamVolLeft(0);
    AISetStreamVolRight(0);

    // stop the DAC
	AISetStreamPlayState(AI_STREAM_STOP);
}


/*---------------------------------------------------------------------------*
 * Name        : __DTKCheckUserCallback 
 *
 * Description : Check user registered callback against event
 *               
 * Arguments   : u32 event
 *
 * Returns     : None.
 *---------------------------------------------------------------------------*/
static void __DTKCheckUserCallback(DTKTrack *track, u32 event)
{
    ASSERT(track);

    if (track)
        if (track->callback)
            if (track->eventMask & event)
                (*track->callback)(track->eventMask & event);
}


/*---------------------------------------------------------------------------*
 * Name        : __DTKForward 
 *
 * Description : Advances playlist by one track.
 *               
 * Arguments   : None
 *
 * Returns     : None.
 *---------------------------------------------------------------------------*/
static void __DTKForward(void)
{
    BOOL old = OSDisableInterrupts();
   
    if (__DTKCurrentTrack)
        if (__DTKCurrentTrack->next)
            __DTKCurrentTrack = __DTKCurrentTrack->next;

    OSRestoreInterrupts(old);
}


/*---------------------------------------------------------------------------*
 * Name        : __DTKBackward 
 *
 * Description : Rewinds playlist by one track.
 *               
 * Arguments   : None
 *
 * Returns     : None.
 *---------------------------------------------------------------------------*/
static void __DTKBackward(void)
{
    BOOL old = OSDisableInterrupts();

    if (__DTKCurrentTrack)
        if (__DTKCurrentTrack->prev)
            __DTKCurrentTrack = __DTKCurrentTrack->prev;
     
    OSRestoreInterrupts(old);
}


/*---------------------------------------------------------------------------*
 * Name        : __DTKCallbackForStreamStatus 
 *
 * Description : Callback function to see if the playback stream has stoppped.
 *               
 * Arguments   : result, per DVDCBCallback
 *                block, per DVDCBCallback
 *
 * Returns     : None.
 *---------------------------------------------------------------------------*/
static void __DTKCallbackForStreamStatus(s32 result, DVDCommandBlock* block)
{
#pragma unused(block)

    // if the last byte of result is 0 it means the stream has ended
    if ((result & 0x000000FF) == 0)
    {
        __DTKTrackEnded = TRUE;
        __DTKPosition   = 0;
    }
}


/*---------------------------------------------------------------------------*
 * Name        : __DTKCallbackForRun
 *
 * Description :  
 *               
 * Arguments   : 
 *
 * Returns     : 
 *---------------------------------------------------------------------------*/
static DVDCommandBlock __block_for_run_callback;
static void __DTKCallbackForRun(s32 result, DVDFileInfo* fileInfo)
{
#pragma unused (result)
#pragma unused (fileInfo)

    __DTKStartAi();

    DVDStopStreamAtEndAsync(
        &__block_for_run_callback,
        NULL
        );

    // mark the state as run
    __DTKState = DTK_STATE_RUN;

    __DTKCheckUserCallback(__DTKCurrentTrack, DTK_EVENT_PLAYBACK_STARTED);
}

/*---------------------------------------------------------------------------*
 * Name        : __DTKCallbackForPreparePaused
 *
 * Description :  
 *               
 * Arguments   : 
 *
 * Returns     : 
 *---------------------------------------------------------------------------*/
static DVDCommandBlock __block_for_prep_callback;
static void __DTKCallbackForPreparePaused(s32 result, DVDFileInfo* fileInfo)
{
#pragma unused (result)
#pragma unused (fileInfo)

    // make sure that AI is stopped
    __DTKStopAi();

    // make sure DVD hardware queue is cleared
    DVDStopStreamAtEndAsync(
        &__block_for_prep_callback,
        NULL
        );

    // mark the state as PAUSED
    __DTKState = DTK_STATE_PAUSE;

    __DTKCheckUserCallback(__DTKCurrentTrack, DTK_EVENT_TRACK_PREPARED);
}

/*---------------------------------------------------------------------------*
 * Name        : __DTKPrepareCurrentTrack
 *
 * Description :  
 *               
 * Arguments   : 
 *
 * Returns     : 
 *---------------------------------------------------------------------------*/
static void __DTKPrepareCurrentTrack(void)
{
    DVDPrepareStreamAsync(
	    &__DTKCurrentTrack->dvdFileInfo,
		0,
		0,
		&__DTKCallbackForRun
		);
}

/*---------------------------------------------------------------------------*
 * Name        : __DTKPrepareCurrentTrackPaused
 *
 * Description :  
 *               
 * Arguments   : 
 *
 * Returns     : 
 *---------------------------------------------------------------------------*/
static void __DTKPrepareCurrentTrackPaused(void)
{
    DVDPrepareStreamAsync(
	    &__DTKCurrentTrack->dvdFileInfo,
		0,
		0,
		&__DTKCallbackForPreparePaused
		);
}

/*---------------------------------------------------------------------------*
 * Name        : __DTKCallbackForPlayList 
 *
 * Description : Callback function used to maintain playlist, checks to see
 *               what is happening and queues tracks accordingly.
 *               
 * Arguments   : result, per DVDCBCallback
 *                block, per DVDCBCallback
 *
 * Returns     : None.
 *---------------------------------------------------------------------------*/
static DVDCommandBlock __block_for_stream_status;
static void __DTKCallbackForPlaylist(s32 result, DVDCommandBlock* cb)
{
#pragma unused (cb)

    // save position
    __DTKPosition = (u32)result;

#ifdef _DEBUG
    // clear flag
    __busy_for_ais_address = FALSE;
#endif

    // if the track has ended lets see if we can figure out what to do next
    if (__DTKTrackEnded)
    {
        __DTKTrackEnded = 0;

        // check user callback        
        __DTKCheckUserCallback(
            __DTKCurrentTrack,
            DTK_EVENT_TRACK_ENDED
            );

        // mark state as busy so no one can change it until the player is done
        // with maintaining the playlist
        __DTKState = DTK_STATE_BUSY;

        switch (__DTKRepeatMode)
        {
        case DTK_MODE_NOREPEAT:

            // no repeat mode means we play the list until it reaches the end
            // then stop
            if (__DTKCurrentTrack)
            {
                if (__DTKCurrentTrack->next)
                {
                    __DTKCurrentTrack = __DTKCurrentTrack->next;
                    __DTKStopAi();
                    __DTKPrepareCurrentTrack();
                }
                else
                {
                    __DTKCurrentTrack = __DTKPlayListHead;
                    __DTKStopAi();
                    __DTKState = DTK_STATE_STOP;
                }
            }

            break;

        case DTK_MODE_ALLREPEAT:

            // all repeat mode means we play the list until it reaches the end
            // then rewind and play the list again... and again... and again..
            if (__DTKCurrentTrack)
            {
                if (__DTKCurrentTrack->next)
                {
                    __DTKCurrentTrack = __DTKCurrentTrack->next;
                    __DTKStopAi();
                    __DTKPrepareCurrentTrack();
                }
                else
                {
                    __DTKCurrentTrack = __DTKPlayListHead;
                    __DTKStopAi();
                    __DTKPrepareCurrentTrack();
                }
            }

            break;

        case DTK_MODE_REPEAT1:

            // repeat 1 mode means we keep playing the same track
            if (__DTKCurrentTrack)
            {
                __DTKStopAi();
                __DTKPrepareCurrentTrack();
            }

            break;
        }
    }
    else
    {
        // calling this function will set the __DTKStreamEnded flag when the
        // playback has ended for the current song
        DVDGetStreamErrorStatusAsync(
            &__block_for_stream_status,
            &__DTKCallbackForStreamStatus
            );
    }
}


/*---------------------------------------------------------------------------*
 * Name        : __DTKCallbackForAIInterrupt
 *
 * Description : Callback function for AIS interrupts, call
 *               DVDGetStreamPlayAddr to maintain playlist
 *
 * Arguments   : count, per AISCallback
 *
 * Returns     : None.
 *---------------------------------------------------------------------------*/
static DVDCommandBlock __block_for_ais_isr;
static void __DTKCallbackForAIInterrupt(u32 count)
{
    AISetStreamTrigger(count + __DTKInterruptFrequency);

#ifdef _DEBUG

    if ((__DTKCurrentTrack) && (__busy_for_ais_address == FALSE))
    {
        __busy_for_ais_address = TRUE;

	    DVDGetStreamPlayAddrAsync(
            &__block_for_ais_isr,
            &__DTKCallbackForPlaylist
            );
    }

#else

    if (__DTKCurrentTrack)
    {
	    DVDGetStreamPlayAddrAsync(
            &__block_for_ais_isr,
            &__DTKCallbackForPlaylist
            );
    }

#endif


}


/*---------------------------------------------------------------------------*
 * Name        : __DTKCallbackForFlush 
 *
 * Description :  
 *               
 * Arguments   : 
 *
 * Returns     : 
 *---------------------------------------------------------------------------*/
static void __DTKCallbackForFlush(s32 result, DVDCommandBlock* block)
{
    DTKTrack *track;

#pragma unused (result)
#pragma unused (block)

    // stop the DAC
    AISetStreamPlayState(AI_STREAM_STOP);

    // close the DVD files
    track = __DTKPlayListHead;

    while (track)
    {
        DVDClose(&track->dvdFileInfo);    
        track = track->next;
    }

    // clear the playlist
    __DTKPlayListHead 		= NULL;
    __DTKPlayListTail 		= NULL;
    __DTKCurrentTrack	    = NULL;

    // mark the state as stopped
    __DTKState = DTK_STATE_STOP;

    // callback the user
    if (__DTKFlushCallback)
    {
        (*__DTKFlushCallback)();
        __DTKFlushCallback = NULL;
    }

    __DTKState = DTK_STATE_STOP;
    __DTKShutdownFlag = 0;
}


/*---------------------------------------------------------------------------*
 * Name        : __DTKCallbackForStop
 *
 * Description : Callback function used for stop.
 *               
 * Arguments   : result, per DVDCBCallback
 *               block, per DVDCBCallback
 *
 * Returns     : None.
 *---------------------------------------------------------------------------*/
static void __DTKCallbackForStop(s32 result, DVDCommandBlock* block)
{
#pragma unused (result)
#pragma unused (block)

    // perform callback
    __DTKCheckUserCallback(
        __DTKCurrentTrack,
        DTK_EVENT_PLAYBACK_STOPPED
        );

    __DTKState = DTK_STATE_STOP;
}


/*---------------------------------------------------------------------------*
 * Name        : __TRLCallbackForNextTrack
 *
 * Description :  
 *               
 * Arguments   : 
 *
 * Returns     : 
 *---------------------------------------------------------------------------*/
static void __DTKCallbackForNextTrack(s32 result, DVDCommandBlock* block)
{
#pragma unused (result)
#pragma unused (block)

    AISetStreamPlayState(AI_STREAM_STOP);

    __DTKForward();

    __DTKState = DTK_STATE_STOP;
    DTKSetState(__DTKTempState);			
}


/*---------------------------------------------------------------------------*
 * Name        : __DTKCallbackForPrevTrack
 *
 * Description :  
 *               
 * Arguments   : 
 *
 * Returns     : 
 *---------------------------------------------------------------------------*/
static void __DTKCallbackForPrevTrack(s32 result, DVDCommandBlock* block)
{
#pragma unused (result)
#pragma unused (block)

    AISetStreamPlayState(AI_STREAM_STOP);

    __DTKBackward();

    __DTKState = DTK_STATE_STOP;
    DTKSetState(__DTKTempState);			
}


/*---------------------------------------------------------------------------*
    Exposed API Functions
 *---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*
 * Name        : DTKInit
 *
 * Description : Initialization for the DVD track player.
 *               
 * Arguments   : None.
 *
 * Returns     : None.
 *---------------------------------------------------------------------------*/
void DTKInit(void)
{
    // note: the AI and DVD is assumed to be already initialized as those
    // are vital systems to any game
        
    // NULL all track pointers
    __DTKCurrentTrack       = NULL;
    __DTKPlayListHead   	= NULL;
    __DTKPlayListTail   	= NULL;

    // All settings to default
    __DTKState              = DTK_STATE_STOP;
    __DTKRepeatMode         = DTK_MODE_NOREPEAT;
    __DTKPosition       	= 0;
    __DTKInterruptFrequency = 48000;
	__DTKVolumeL			= 0xff;
	__DTKVolumeR			= 0xff;
    
    // stop the DAC clock, mask the AI stream interrupt, set to 48kHz
    AISetStreamVolLeft(0);
    AISetStreamVolRight(0);
    AIRegisterStreamCallback(&__DTKCallbackForAIInterrupt);
    AIResetStreamSampleCount();
    AISetStreamPlayState(AI_STREAM_STOP);
}


/*---------------------------------------------------------------------------*
 * Name        : DTKShutdown
 *
 * Description : Shutdown the track player.
 *
 * Arguments   : None.
 *
 * Returns     : None.
 *---------------------------------------------------------------------------*/
void DTKShutdown(void)
{
    // stop the DAC clock, mask the AI stream interrupt, set to 48kHz
    AISetStreamVolLeft(0);
    AISetStreamVolRight(0);
    AIRegisterStreamCallback(NULL);
    AIResetStreamSampleCount();
    AISetStreamPlayState(AI_STREAM_STOP);

    __DTKShutdownFlag   = 1;

    // flush the tracks
    DTKFlushTracks(NULL);

    __DTKState = DTK_STATE_STOP;

    while (__DTKShutdownFlag){};
}


/*---------------------------------------------------------------------------*
 * Name        : DTKQueueTrack
 *
 * Description : Put specified track into queue.
 *               
 * Arguments   : fileName   address location of string containing file name
 *               track      user allocated storage for track object
 *               eventMask  events to perform callback
 *               callback   address location of call back function 
 *
 * Returns     : DTK_QUEUE_SUCCESS or DTK_QUEUE_CANNOT_OPEN_FILE
 *---------------------------------------------------------------------------*/
u32 DTKQueueTrack(char* fileName, DTKTrack* track, u32 eventMask,
                    DTKCallback callback)
{
    u32 startTrack = 0;
    int old;

    // open the file on the DVD
    if (DVDOpen(fileName, &track->dvdFileInfo) == FALSE)
        return(DTK_QUEUE_CANNOT_OPEN_FILE);

    // mask interrupts while we are messing with the queue
    old = OSDisableInterrupts();

    // initialize the track object
    track->fileName     = fileName;
    track->eventMask    = eventMask;
    track->callback     = callback;
      
    // put this track in the play list
    // If there's nothing on the list this track becomes head and tail
    if (__DTKPlayListHead == NULL)
    {
        __DTKPlayListHead = track;
	    __DTKPlayListTail = track;
		    
	    track->prev = NULL;
	    track->next = NULL;

        // if the state of the player is run kick it off
        if (__DTKState == DTK_STATE_RUN)
            startTrack = 1;
    }
	// there's already track(s) in the list, attach this one to the tail
	else
	{
	    __DTKPlayListTail->next = track;
        track->prev	= __DTKPlayListTail;
	    __DTKPlayListTail = track;
        track->next = NULL;
	}

    // maintain DVD current and queued tracks
    if (__DTKCurrentTrack == NULL)
        __DTKCurrentTrack = track;

    // re-enable interrupts
    OSRestoreInterrupts(old);

    // check to see if we sould do a callback
    __DTKCheckUserCallback(track, DTK_EVENT_TRACK_QUEUED);

    if (startTrack)
    {
        __DTKState = DTK_STATE_BUSY;
        __DTKPrepareCurrentTrack();
    }

    return DTK_QUEUE_SUCCESS;
}


/*---------------------------------------------------------------------------*
 * Name        : DTKRemoveTrack
 *
 * Description : Remove specified track from playlist.
 *               
 * Arguments   : track pointer to specified track.
 *
 * Returns     : None.
 *---------------------------------------------------------------------------*/
u32 DTKRemoveTrack(DTKTrack* track)
{
    BOOL old;

    // see if it's the current track or queued track on the DVD drive, if so
	// we cannot remove it
	if (track == __DTKCurrentTrack)
        return(DTK_QUEUE_TRACK_IN_DVD);

    old = OSDisableInterrupts();

    // close the file on the DVD
    DVDClose(&track->dvdFileInfo);

    // see if it's the only track
    if ((track == __DTKPlayListHead) && (track == __DTKPlayListTail))
    {
	    __DTKPlayListHead = NULL;
	    __DTKPlayListTail = NULL;
		    
	    OSRestoreInterrupts(old);
	    return(DTK_QUEUE_SUCCESS);
    }

    // see if it's the head
    if (track == __DTKPlayListHead)
    {
	    __DTKPlayListHead		= track->next;
	    __DTKPlayListHead->prev = NULL;
		    
	    if (__DTKRepeatMode == DTK_MODE_ALLREPEAT)
		    __DTKPlayListTail->next = __DTKPlayListHead;
			    
	    OSRestoreInterrupts(old);
	    return DTK_QUEUE_SUCCESS;
    }
	    
    // see if it's the tail
    if (track == __DTKPlayListTail)
    {
	    __DTKPlayListTail		= track->prev;
	    __DTKPlayListTail->next	= NULL;
		    
	    if (__DTKRepeatMode == DTK_MODE_ALLREPEAT)
		    __DTKPlayListTail->next = __DTKPlayListHead;
			    
	    OSRestoreInterrupts(old);
	    return DTK_QUEUE_SUCCESS;
    } 

    // patch the playlist
    track->prev->next = track->next;
    track->next->prev = track->prev;
	    
    OSRestoreInterrupts(old);

    return(DTK_QUEUE_SUCCESS);
}



/*---------------------------------------------------------------------------*
 * Name        : DTKFlushTracks
 *
 * Description : Flushes all tracks from queue and stops DVD playback.
 *               
 * Arguments   : None.
 *
 * Returns     : None.
 *---------------------------------------------------------------------------*/
static DVDCommandBlock __block_for_flushtracks;
void DTKFlushTracks(DTKFlushCallback callback)
{
    u32 temp;

    if (__DTKState == DTK_STATE_BUSY)
        return;

	// mark the player state as busy
    temp       = __DTKState;         
    __DTKState = DTK_STATE_BUSY;

    // save the callback
    __DTKFlushCallback  = callback;

	// clear the queued track if any
    if (temp == DTK_STATE_RUN)
        DVDCancelStreamAsync(
    	    &__block_for_flushtracks,
            &__DTKCallbackForFlush
            );
    else
        __DTKCallbackForFlush(0, 0);
}   


/*---------------------------------------------------------------------------*
 * Name        : DTKSetSampleRate
 *
 * Description : Obsoleted. Drive only supports 48KHz now. 
 *
 * Arguments   : None.
 *
 * Returns     : None.
 *---------------------------------------------------------------------------*/
void DTKSetSampleRate(u32 samplerate)
{
#pragma unused(samplerate)

}



/*---------------------------------------------------------------------------*
 * Name        : DTKSetInterruptFrequency
 *
 * Description : Sets number of samples between interrupts
 *               
 * Arguments   : samples, number of samples to set
 *
 * Returns     : None.
 *---------------------------------------------------------------------------*/
void DTKSetInterruptFrequency(u32 samples)
{
    __DTKInterruptFrequency = samples;

    AIResetStreamSampleCount();
   	AISetStreamTrigger(__DTKInterruptFrequency);
}


/*---------------------------------------------------------------------------*
 * Name        : DTKSetRepeatMode
 *
 * Description : Set repeat mode for player
 *               
 * Arguments   : repeat, DTK_MODE_NOREPEAT, DTK_MODE_ALLREPEAT, DTK_MODE_REPEAT1
 *
 * Returns     : None.
 *---------------------------------------------------------------------------*/
static DVDCommandBlock __block_for_repeatmode;
void DTKSetRepeatMode (u32 repeat)
{
    __DTKRepeatMode = repeat;
}


/*---------------------------------------------------------------------------*
 * Name        : DTKSetState
 *
 * Description : Sets state for track player, prior to calling this function
 *               the user must call DTKGetState and make sure the player is
 *               not busy.
 *
 * Arguments   : state, DTK_STATE_STOP, DTK_STATE_RUN, DTK_STATE_PAUSE
 *
 * Returns     : none.
 *---------------------------------------------------------------------------*/
static DVDCommandBlock __block_for_set_state;
void DTKSetState(u32 state)
{
	// If the player is in the middle of waiting for stopping or flushing
	// routines and waiting for the DVD to complete commands return the 
	// proper state, also if the player is already in desired state we don't
	// have to do anything.
    if ((__DTKState == state) || (__DTKState == DTK_STATE_BUSY))
        return;
		
    switch (state)
    {
        case DTK_STATE_STOP:

            if (__DTKCurrentTrack)
            {
                // mark the player state as stopping
                __DTKState = DTK_STATE_BUSY;
        	    
                // set volume to 0
                AISetStreamVolLeft(0);
                AISetStreamVolRight(0);

        	    // stop the DAC
	            AISetStreamPlayState(AI_STREAM_STOP);
        	    
                // clear the queued track if any
                DVDCancelStreamAsync(
                    &__block_for_set_state,
                    &__DTKCallbackForStop
                    );
            }
		     
            break;

        case DTK_STATE_RUN:

            // see if we are running from pause
            if (__DTKState == DTK_STATE_PAUSE)
            {
                __DTKStartAi();
                __DTKState = DTK_STATE_RUN;

                if (__DTKCurrentTrack)
                    __DTKCheckUserCallback(
                        __DTKCurrentTrack,
                        DTK_EVENT_PLAYBACK_STARTED
                        );
            }
            // see if the DVD has a current track
            else if (__DTKCurrentTrack)
            {
                __DTKState = DTK_STATE_BUSY;
                __DTKPrepareCurrentTrack();
            }
            else
            {  
                __DTKState = DTK_STATE_RUN;
            }

            __DTKTrackEnded = 0;

            break;

        case DTK_STATE_PREPARE:
            // can be invoked if and only if the player is stopped!
            if (__DTKState == DTK_STATE_STOP)
            {
                if (__DTKCurrentTrack)
                {
                    __DTKState = DTK_STATE_BUSY;
                    __DTKPrepareCurrentTrackPaused();
                }
                __DTKTrackEnded = 0;
            }


            break;


        case DTK_STATE_PAUSE: 

            // stop the DAC clock
            AISetStreamPlayState(AI_STREAM_STOP);

            if (__DTKState == DTK_STATE_RUN)
                __DTKState = DTK_STATE_PAUSE;

            __DTKCheckUserCallback(
                __DTKCurrentTrack,
                DTK_EVENT_PLAYBACK_PAUSED
                );

            break;
    }

    return;
}


/*---------------------------------------------------------------------------*
 * Name        : DTKNextTrack
 *
 * Description : Advance to next track 
 *               
 * Arguments   : None 
 *
 * Returns     : None
 *---------------------------------------------------------------------------*/
static DVDCommandBlock __block_for_next_track;
void DTKNextTrack()
{
	if (__DTKState == DTK_STATE_BUSY)
		return;

    if (__DTKCurrentTrack)
    {
	    __DTKTempState	= __DTKState;
	    __DTKState 		= DTK_STATE_BUSY;

        // if the player is running, cancel the stream
        if (__DTKTempState == DTK_STATE_RUN)
        {
   	        // set volume to 0
            AISetStreamVolLeft(0);
	        AISetStreamVolRight(0);

            DVDCancelStreamAsync(
                &__block_for_next_track,
                &__DTKCallbackForNextTrack
                );
        }
        // if the player is not running just adjust track and restore state
        else
        {
            __DTKForward();
            __DTKState = __DTKTempState;
        }
    }
}


/*---------------------------------------------------------------------------*
 * Name        : DTKPrevTrack
 *
 * Description : Rewind to previous track 
 *
 * Arguments   : None 
 *
 * Returns     : None
 *---------------------------------------------------------------------------*/
static DVDCommandBlock __block_for_prev_track;
void DTKPrevTrack()
{
	if (__DTKState == DTK_STATE_BUSY)
		return;	

    if (__DTKCurrentTrack)
    {
	    __DTKTempState	= __DTKState;
	    __DTKState 		= DTK_STATE_BUSY;

        // if the player is running, cancel the stream
        if (__DTKTempState == DTK_STATE_RUN)
        {
   	        // set volume to 0
            AISetStreamVolLeft(0);
	        AISetStreamVolRight(0);

            DVDCancelStreamAsync(
                &__block_for_prev_track,
                &__DTKCallbackForPrevTrack
                );
        }
        // if the player is not running just adjust track and restore state
        else
        {
            __DTKBackward();
            __DTKState = __DTKTempState;
        }
    }
}


/*---------------------------------------------------------------------------*
 * Name        : DTKGetSampleRate
 *
 * Description : Returns sample rate of track player
 *
 * Arguments   : None.
 *
 * Returns     : DTK_SAMPLERATE_48KHZ or DTK_SAMPLERATE_32KHZ
 *---------------------------------------------------------------------------*/
u32 DTKGetSampleRate(void)
{
    return DTK_SAMPLERATE_48KHZ;
}


/*---------------------------------------------------------------------------*
 * Name        : DTKGetRepeatMode
 *
 * Description : Returns repeat mode of track player
 *               
 * Arguments   : None.
 *
 * Returns     : DTK_MODE_NOREPEAT, DTK_MODE_ALLREPEAT, DTK_MODE_REPEAT1
 *---------------------------------------------------------------------------*/
u32 DTKGetRepeatMode(void)
{
    return __DTKRepeatMode;
}


/*---------------------------------------------------------------------------*
 * Name        : DTKGetState
 *
 * Description : Returns state of track player
 *               
 * Arguments   : None.
 *
 * Returns     : DTK_STATE_STOP, DTK_STATE_RUN, DTK_STATE_PAUSE
 *---------------------------------------------------------------------------*/
u32 DTKGetState(void)
{
    return __DTKState;
}


/*---------------------------------------------------------------------------*
 * Name        : DTKGetPosition
 *
 * Description : Returns psyudo position of current track
 *               
 * Arguments   : None.
 *
 * Returns     : Current position
 *---------------------------------------------------------------------------*/
u32 DTKGetPosition(void)
{
    return __DTKPosition;
}


/*---------------------------------------------------------------------------*
 * Name        : DTKGetInterruptFrequency
 *
 * Description : Returns number of samples between interrupts
 *               
 * Arguments   : None.
 *
 * Returns     : Current position
 *---------------------------------------------------------------------------*/
u32 DTKGetInterruptFrequency(void)
{
    return __DTKInterruptFrequency;
}


/*---------------------------------------------------------------------------*
 * Name        : DTKGetCurrentTrack
 *
 * Description : Returns address location of current track 
 *               
 * Arguments   : None.
 *
 * Returns     : address location of current track
 *---------------------------------------------------------------------------*/
DTKTrack* DTKGetCurrentTrack(void)
{
    return(__DTKCurrentTrack);
}


/*---------------------------------------------------------------------------*
 * Name        : DTKSetVolume 
 *
 * Description : Sets playback volume for the player
 *
 * Arguments   : left   left volume 0 - 0xff
 *               right  right volume 0 - 0xff
 *
 * Returns     : None 
 *---------------------------------------------------------------------------*/
void DTKSetVolume(u8 left, u8 right)
{
	__DTKVolumeL	= left;
	__DTKVolumeR	= right;
	
	if (__DTKState == DTK_STATE_RUN)
	{
	    AISetStreamVolLeft(left);
    	AISetStreamVolRight(right);
	}
}


/*---------------------------------------------------------------------------*
 * Name        : DTKGetVolume
 *
 * Description : Returns volume for the player 
 *               
 * Arguments   : None
 *
 * Returns     : left and right volume
 *---------------------------------------------------------------------------*/
u16 DTKGetVolume(void)
{
	return (u16)((__DTKVolumeL << 8) | (__DTKVolumeR));
}


