/*---------------------------------------------------------------------------*
  Project:  Dolphin DI reg definitions
  File:     direg.h

  Copyright 1998, 1999 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: direg.h,v $
  Revision 1.1.1.1  2004/01/03 01:02:28  paulm
  Gamecube library source from Nintendo

    
    3     4/17/00 1:33p Hashida
    Added subcommands for audio buffer config command.
    
    2     4/07/00 6:51p Hashida
    Added streaming stuffs.
    
    11    11/04/99 2:06p Hashida
    Changed to use the real command codes
    
    10    8/25/99 3:03p Hashida
    Include Flipper.h instead of io_reg.h and pi_reg.h
    Removed unneeded definition (NUMREGS)
    Removed unneeded macros (DVDReadReg, DVDWriteReg, REG)
    
    9     8/20/99 9:58p Hashida
    Added volatile to read/write reg macro.
    
    8     8/19/99 12:37a Hashida
    Removed REALCODE definition
    
    7     8/17/99 1:57p Hashida
    Changed because we use ArtX's register definition.
    
    6     8/04/99 11:30a Hashida
    Added PI config register
    
    5     7/29/99 12:28p Hashida
    Fixed cache alignment bug for fake Flipper registers.
    
    4     7/27/99 11:14p Hashida
    Added complete command definition for CMDBUF0
    
    3     7/27/99 9:14p Hashida
    Added mask bit patterns.
    
    2     7/27/99 6:48p Hashida
    Added __DVDReadReg and __DVDWriteReg
    Removed two registers dedicated for write (use __DVDWriteReg to write
    inteligently, as real machine will do)
    
    1     7/27/99 2:44p Hashida
    initial revision for di register definitions
    
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#ifndef __DIREG_H__
#define __DIREG_H__

#include <private/flipper.h>

#ifdef __cplusplus
extern "C" {
#endif

#define DRIVE_CMD_MASK               0xff000000
#define DRIVE_CMD_INQUIRY            0x12000000
#define DRIVE_CMD_READ               0xa8000000
#define DRIVE_CMD_SEEK               0xab000000
#define DRIVE_CMD_REQUESTERROR       0xe0000000
#define DRIVE_CMD_AUDIOSTREAMING     0xe1000000
#define DRIVE_CMD_REQUESTAUDIOSTATUS 0xe2000000
#define DRIVE_CMD_STOPMOTOR          0xe3000000
#define DRIVE_CMD_AUDIOBUFFERCONFIG  0xe4000000

#define DRIVE_SUBCMD_MASK            0x000000c0
#define DRIVE_SUBCMD_ID              0x00000040

// audio streaming subcommands
#define DRIVE_SUBCMD_ENTRY           0x00000000
#define DRIVE_SUBCMD_CLEAR           0x00010000

// request audio status subcommands
#define DRIVE_SUBCMD_ERROR_STATUS    0x00000000
#define DRIVE_SUBCMD_PLAY_ADDR       0x00010000
#define DRIVE_SUBCMD_START_ADDR      0x00020000
#define DRIVE_SUBCMD_LENGTH          0x00030000

#define DRIVE_SUBCMD_ON              0x00010000
#define DRIVE_SUBCMD_OFF             0x00000000

#ifdef __cplusplus
}
#endif

#endif  // __DIREG_H__
