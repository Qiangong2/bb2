/*---------------------------------------------------------------------------*
  Project:  Dolphin OS Error Messages
  File:     DVDLowAssert.h

  Copyright 1998, 1999 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: DVDLowAssert.h,v $
  Revision 1.1.1.1  2004/01/03 01:02:28  paulm
  Gamecube library source from Nintendo

    
    3     7/27/99 11:13p Hashida
    Added assert message for seek
    
    2     7/10/99 3:02a Hashida
    added readid error
    
    1     7/08/99 5:06p Hashida
    initial revision
    
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#ifndef __DVDLOWASSERT_H__
#define __DVDLOWASSERT_H__

#ifdef __cplusplus
extern "C" {
#endif

/*---------------------------------------------------------------------------*
    Error messages of dvdlow.c
 *---------------------------------------------------------------------------*/

#define DVDLOW_ERR_READ_INVADDR     "DVDLowRead(): address must be aligned with 32 byte boundary."
#define DVDLOW_ERR_READ_INVSIZE     "DVDLowRead(): length must be a multiple of 32."
#define DVDLOW_ERR_READ_INVOFFSET   "DVDLowRead(): offset must be a multiple of 4."

#define DVDLOW_ERR_SEEK_INVOFFSET   "DVDLowSeek(): offset must be a multiple of 4."

#define DVDLOW_ERR_READID_INVADDR   "DVDLowReadID(): id must be aligned with 32 byte boundary."

#ifdef __cplusplus
}
#endif

#endif  // __DVDLOWASSERT_H__
