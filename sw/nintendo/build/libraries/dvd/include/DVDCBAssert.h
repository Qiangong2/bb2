/*---------------------------------------------------------------------------*
  Project:  Dolphin OS Error Messages
  File:     DVDEHAssert.h

  Copyright 1998, 1999 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: DVDCBAssert.h,v $
  Revision 1.1.1.1  2004/01/03 01:02:28  paulm
  Gamecube library source from Nintendo

    
    2     7/21/99 10:44p Hashida
    Added BUSY messages
    
    1     7/21/99 10:15a Hashida
    initial revision
    
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#ifndef __DVDCBASSERT_H__
#define __DVDCBASSERT_H__

#ifdef __cplusplus
extern "C" {
#endif

/*---------------------------------------------------------------------------*
    Error messages for dvdcb.c
 *---------------------------------------------------------------------------*/

#define DVD_ERR_READABS_NULLBLOCK   "DVDReadAbsAsync(): null pointer is specified to command block address."
#define DVD_ERR_READABS_NULLADDR    "DVDReadAbsAsync(): null pointer is specified to addr."
#define DVD_ERR_READABS_INVADDR     "DVDReadAbsAsync(): address must be aligned with 32 byte boundary."
#define DVD_ERR_READABS_INVSIZE     "DVDReadAbsAsync(): length must be a multiple of 32."
#define DVD_ERR_READABS_INVOFFSET   "DVDReadAbsAsync(): offset must be a multiple of 4."
#define DVD_ERR_READABS_BUSY        "DVDReadAbsAsync(): command block is used for processing previous request."

#define DVD_ERR_SEEKABS_NULLBLOCK   "DVDSeekAbs(): null pointer is specified to command block address."
#define DVD_ERR_SEEKABS_INVOFFSET   "DVDSeekAbs(): offset must be a multiple of 4."
#define DVD_ERR_SEEKABS_BUSY        "DVDSeekAbs(): command block is used for processing previous request."

#define DVD_ERR_READABSBS_NULLBLOCK "DVDReadAbsAsyncForBS(): null pointer is specified to command block address."
#define DVD_ERR_READABSBS_NULLADDR  "DVDReadAbsAsyncForBS(): null pointer is specified to addr."
#define DVD_ERR_READABSBS_INVADDR   "DVDReadAbsAsyncForBS(): address must be aligned with 32 byte boundary."
#define DVD_ERR_READABSBS_INVSIZE   "DVDReadAbsAsyncForBS(): length must be a multiple of 32."
#define DVD_ERR_READABSBS_INVOFFSET "DVDReadAbsAsyncForBS(): offset must be a multiple of 4."
#define DVD_ERR_READABSBS_BUSY      "DVDReadAbsAsyncForBS(): command block is used for processing previous request."

#define DVD_ERR_READID_NULLBLOCK    "DVDReadDiskID(): null pointer is specified to command block address."
#define DVD_ERR_READID_NULLID       "DVDReadDiskID(): null pointer is specified to id address."
#define DVD_ERR_READID_INVID        "DVDReadDiskID(): id must be aligned with 32 byte boundary."
#define DVD_ERR_READID_BUSY         "DVDReadDiskID(): command block is used for processing previous request."

#define DVD_ERR_GETCOMMANDBLOCKSTATUS_NULLBLOCK            \
                 "DVDGetCommandBlockStatus(): null pointer is specified to command block address."

#define DVD_ERR_CHANGEDISK_NULLBLOCK                      \
                 "DVDChangeDisk(): null pointer is specified to command block address."
#define DVD_ERR_CHANGEDISK_NULLID                         \
                 "DVDChangeDisk(): null pointer is specified to id address."
#define DVD_ERR_CHANGEDISK_BUSY     "DVDChangeDisk(): command block is used for processing previous request."

#ifdef __cplusplus
}
#endif

#endif  // __DVDCBASSERT_H__
