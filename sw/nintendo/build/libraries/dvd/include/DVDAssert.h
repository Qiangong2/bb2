/*---------------------------------------------------------------------------*
  Project:  Dolphin OS Error Messages
  File:     DVDAssert.h

  Copyright 1998, 1999 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: DVDAssert.h,v $
  Revision 1.1.1.1  2004/01/03 01:02:28  paulm
  Gamecube library source from Nintendo

    
    4     6/29/00 4:31p Hashida
    Removed DVDOPEN_ERR_NO_ENT because the message was moved to dvdfs.c
    
    3     5/14/00 5:55p Hashida
    Added assertion for DVDOpenDir.
    Minor changes.
    
    2     5/13/00 7:39p Shiki
    Revised *_NOENT messages.

    3     7/21/99 11:50p Hashida
    Added "out of range" messages

    2     7/21/99 11:34a Hashida
    Added assert messages

    1     7/20/99 11:56a Hashida
    Initial revision

    2     6/11/99 10:43p Hashida
    Added DVDSetRoot()'s error message

    1     6/11/99 2:53p Shiki

  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#ifndef __DVDASSERT_H__
#define __DVDASSERT_H__

#ifdef __cplusplus
extern "C" {
#endif

/*---------------------------------------------------------------------------*
    Error messages of dvdfs.c
 *---------------------------------------------------------------------------*/

#define DVD_ERR_OPEN_NULLPATHPTR    "DVDOpen(): null pointer is specified to file name  "
#define DVD_ERR_OPEN_NULLFILEINFO   "DVDOpen(): null pointer is specified to file info address  "
#define DVD_ERR_OPEN_ISDIR          "DVDOpen(): directory '%s' is specified as a filename  "

#define DVD_ERR_CLOSE_NULLFILEINFO  "DVDClose(): null pointer is specified to file info address  "
#define DVD_ERR_CLOSE_BUSY          "DVDClose(): fileInfo is busy for processing previous request  "

#define DVD_ERR_CHANGEDIR_NULLPATHPTR "DVDChangeDir(): null pointer is specified to directory name  "
#define DVD_ERR_CHANGEDIR_NOENT     "DVDChangeDir(): directory '%s' is not found under %s  "
#define DVD_ERR_CHANGEDIR_NOTDIR    "DVDChangeDir(): file '%s' is specified as a directory name  "

#define DVD_ERR_READASYNC_NULLFILEINFO "DVDReadAsync(): null pointer is specified to file info address  "
#define DVD_ERR_READASYNC_NULLADDR  "DVDReadAsync(): null pointer is specified to addr  "
#define DVD_ERR_READASYNC_INVADDR   "DVDReadAsync(): address must be aligned with 32 byte boundaries  "
#define DVD_ERR_READASYNC_INVSIZE   "DVDReadAsync(): length must be  multiple of 32 byte  "
#define DVD_ERR_READASYNC_INVOFFSET "DVDReadAsync(): offset must be multiple of 4 byte  "
#define DVD_ERR_READASYNC_BUSY      "DVDReadAsync(): fileInfo is used for processing previous request  "
#define DVD_ERR_READASYNC_OUTOFRANGE "DVDReadAsync(): specified area is out of the file  "

#define DVD_ERR_READ_NULLFILEINFO   "DVDRead(): null pointer is specified to file info address  "
#define DVD_ERR_READ_NULLADDR       "DVDRead(): null pointer is specified to addr  "
#define DVD_ERR_READ_INVADDR        "DVDRead(): address must be aligned with 32 byte boundaries  "
#define DVD_ERR_READ_INVSIZE        "DVDRead(): length must be  multiple of 32 byte  "
#define DVD_ERR_READ_INVOFFSET      "DVDRead(): offset must be multiple of 4 byte  "
#define DVD_ERR_READ_BUSY           "DVDRead(): fileInfo is used for processing previous request  "
#define DVD_ERR_READ_OUTOFRANGE     "DVDRead(): specified area is out of the file  "

#define DVD_ERR_SEEK_NULLFILEINFO   "DVDSeek(): null pointer is specified to file info address  "
#define DVD_ERR_SEEK_INVOFFSET      "DVDSeek(): offset must be multiple of 4 byte  "
#define DVD_ERR_SEEK_BUSY           "DVDSeek(): fileInfo is used for processing previous request  "
#define DVD_ERR_SEEK_OUTOFRANGE     "DVDSeek(): offset is out of the file  "

#define DVD_ERR_OPENDIR_NULLPATHPTR "DVDOpendir(): null pointer is specified to directory name  "
#define DVD_ERR_OPENDIR_NOENT       "DVDOpendir(): directory '%s' is not found under %s  "
#define DVD_ERR_OPENDIR_NOTDIR      "DVDOpendir(): file '%s' is specified as a directory name  "

#ifdef __cplusplus
}
#endif

#endif  // __DVDASSERT_H__
