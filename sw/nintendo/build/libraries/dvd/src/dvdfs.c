/*---------------------------------------------------------------------------*
  Project:  Dolphin DVD file system functions
  File:     dvdfs.c

  Copyright 1998-2001 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: dvdfs.c,v $
  Revision 1.1.1.1  2004/01/03 01:02:28  paulm
  Gamecube library source from Nintendo

    
    36    03/09/10 14:43 Ooshima
    Fixed 0 byte read of 0 byte file assertion and Inserted DVDRewindDir()
    for DVDETH  
    
    35    03/04/22 11:28 Hashida
    Modified DVDGetFileInfoStatus to refer fileinfo's member instead of
    just casting to DVDCommandBlock structure.
    
    34    8/05/02 16:19 Shiki
    Added const keywords to relevant function prototypes.

    33    8/10/01 3:02p Hashida
    Added DVDFastOpenDir

    32    7/06/01 1:21a Hashida
    Resurrected function version of DVDGetFileInfoStatus().

    31    6/15/01 9:01p Hashida
    Changed DVDGetFileInfoStatus to a macro.

    30    6/11/01 3:40p Hashida
    Fixed a bug that DVDGetTransferredSize crashes if "retry" error
    occurred.

    29    4/19/01 3:02p Hashida
    Fixed a bug that when DVDOpen couldn't find an entry for the specified
    file, it tried to access memory in arena.

    28    4/03/01 10:45p Hashida
    Added a code to return DVD_RESULT_CANCELED for synchronous functions if
    canceled.

    27    3/02/01 12:14a Hashida
    Added DVDCancel.
    Added DVDGetTransferredSize.

    26    1/16/01 4:09p Hashida
    Made area check in read/seek/streaming more strict and precise.

    25    11/15/00 11:37a Hashida
    Changed to perform 8.3 check depending on a BootInfo2 value.

    24    10/09/00 6:57p Hashida
    Added prioritized queuing.

    23    9/25/00 4:35p Hashida
    Added DVDPrepareStream (synchronous version).

    22    9/25/00 3:35p Hashida
    Changed API names that don't follow the convention (sync, async)

    21    7/27/00 3:02p Tian
    Fixed __DVDFSInit.  It was dereferencing NULL when DVDInit was called
    from the boot code.

    20    7/21/00 11:15a Hashida
    Changed DVDClose to call DVDCancel.

    19    7/20/00 12:30p Hashida
    Implemented DVDFSInit
    Changed DVDConvertEntrynumToPath to a static function because
    converting an entrynum of a file to path name is troublesome.

    18    7/20/00 9:49a Hashida
    Added DVDFastOpen(), DVDConvertPathToEntrynum and
    DVDConvertEntrynumToPath.

    17    6/29/00 4:33p Hashida
    Modified so that a warning message is printed when DVDOpen could not
    find the specified file (even in release library).

    16    6/20/00 12:50p Hashida
    Added assertion messages in DVDInitStream (32KB alignment assertion)

    15    5/14/00 8:04p Hashida
    Removed several functions.

    14    5/14/00 5:54p Hashida
    Added five functions: DVDGetCurrentDir, DVDPathToEntrynum,
    DVDEntrynumToPath, DVDEntrynumChangeDir, DVDEntrynumGetCurrentDir

    13    5/13/00 7:40p Shiki
    Revised to show more friendly assert messages.

    12    5/10/00 5:25p Hashida
    Removed relocate FST function.


    11    4/12/00 3:55p Hashida
    Moved audio streaming API definitions from dvdas.h to dvd.h (dvdas.h is
    removed).

    10    4/07/00 6:51p Hashida
    Added streaming APIs.

    9     3/13/00 2:46p Hashida
    Added a new API DVDGetFSTLocation.
    Added an assertion in DVDRelocateFST.

    8     3/10/00 2:38p Hashida
    Added DVDRelocateFST.

    7     3/03/00 1:21p Hashida
    Added 8.3 format check.

    6     3/01/00 1:13p Hashida
    Renamed dir->filename with dir->name.
    Added a member check in DVDReadDir.

    5     2/29/00 7:58p Hashida
    Added directory access support.

    4     2/11/00 3:23p Hashida
    Fixed a bug of thread support.

    3     2/09/00 6:23p Hashida
    make DVDRead thread-safe.

    2     1/13/00 12:16p Hashida
    Fixed a bug that DVDOpen didn't initialize the state of the file info.
    Added directory access functions for test purpose.

    11    9/09/99 2:15p Hashida
    Removed warnings.

    10    9/08/99 7:13p Tian
    Added includes to fill in missing prototypes.

    9     8/23/99 2:19p Tian
    Moved OSLoMem.h to private.

    8     8/02/99 12:06p Hashida
    Added DVDGetFileInfoStatus

    7     7/21/99 11:51p Hashida
    Added a check if specified area is in the file.
    Fixed a minor bug.

    6     7/21/99 11:14p Hashida
    Removed clearing the member callback before calling the callback.

    5     7/21/99 11:35a Hashida
    Added ASSERTMSG's

    4     7/20/99 10:31p Hashida
    Added DVDClose, DVDRead, DVDSeek

    3     7/20/99 11:56a Hashida
    Made DVDReadAsync

    2     7/20/99 10:30a Hashida
    Changed to use OS_BOOTINFO_ADDR instead of 0x80000000

    1     7/13/99 9:13p Hashida
    initial revision .. file system layer

  $NoKeywords: $
 *---------------------------------------------------------------------------*/

// Dolphin header files
#include <dolphin/os.h>
#include <dolphin/dvd.h>
#include <secure/dvdlow.h>
#include <secure/dvdcb.h>
#include <dolphin/os/osbootinfo.h>
#include <private/oslomem.h>
#include <private/flipper.h>
#include <ctype.h>
#include <string.h>     // for memcpy in DVDRelocateFST
#include "DVDAssert.h"

// Now whether to perform 8.3 check is decided based on __DVDLongFileNameFlag
// which is essentially OS_BI2_LONGFILENAME_OFFSET in BootInfo2

typedef struct FSTEntry FSTEntry;

struct FSTEntry
{
    unsigned int    isDirAndStringOff;    // the first byte is for isDir
                                          // the next 3bytes: name offset
    unsigned int    parentOrPosition;     // parent entry (dir entry)
                                          // position (file entry)
    unsigned int    nextEntryOrLength;    // next entry (dir entry)
                                          // length (file entry)
};

static OSBootInfo*      BootInfo;
static FSTEntry*        FstStart;
static char*            FstStringStart;
static u32              MaxEntryNum;

static u32              currentDirectory = 0;

OSThreadQueue           __DVDThreadQueue;   // list of threads waiting

// initialized in OSInit
u32                     __DVDLongFileNameFlag;

void                    __DVDFSInit(void);

#define entryIsDir(i)     \
    ( ( ( FstStart[i].isDirAndStringOff & 0xff000000 ) == 0 )? FALSE:TRUE )
#define stringOff(i)      \
        ( FstStart[i].isDirAndStringOff & 0x00ffffff )
#define parentDir(i)       \
        ( FstStart[i].parentOrPosition )
#define nextDir(i)        \
        ( FstStart[i].nextEntryOrLength )
#define filePosition(i)       \
        ( FstStart[i].parentOrPosition )
#define fileLength(i)         \
        ( FstStart[i].nextEntryOrLength )

static void cbForReadAsync(s32 result, DVDCommandBlock* block);
static void cbForReadSync(s32 result, DVDCommandBlock* block);
static void cbForSeekAsync(s32 result, DVDCommandBlock* block);
static void cbForSeekSync(s32 result, DVDCommandBlock* block);
static void cbForPrepareStreamAsync(s32 result, DVDCommandBlock* block);
static void cbForPrepareStreamSync(s32 result, DVDCommandBlock* block);

#ifndef offsetof
#define offsetof(type, memb)        ((u32)&((type *)0)->memb)
#endif

 /*---------------------------------------------------------------------------*
    Name:               __DVDFSInit

    Description:        Initialize file system. Only called from DVDInit

    Arguments:          None

    Returns:            None
 *---------------------------------------------------------------------------*/
void __DVDFSInit(void)
{
    BootInfo        = (OSBootInfo*)OSPhysicalToCached(OS_BOOTINFO_ADDR);
    FstStart        = (FSTEntry*)BootInfo->FSTLocation;

    if (FstStart)
    {
        // bootinfo has been initialized
        MaxEntryNum     = FstStart[0].nextEntryOrLength;
        FstStringStart  = (char*)&(FstStart[MaxEntryNum]);
    }
}


 /*---------------------------------------------------------------------------*
    Name:               isSame

    Description:        compare two strings up to the first string hits '/' or
                        '\0'

    Arguments:          path     path name
                        string   directory or file name

    Returns:            TRUE if same, FALSE if not
 *---------------------------------------------------------------------------*/
static BOOL isSame(const char* path, const char* string)
{
    while(*string != '\0')
    {
        // compare in case-insensitive
        if (tolower(*path++) != tolower(*string++))
        {
            return FALSE;
        }
    }

    if ( (*path == '/') || (*path == '\0') )
    {
        return TRUE;
    }

    return FALSE;
}


 /*---------------------------------------------------------------------------*
    Name:               DVDConvertPathToEntrynum

    Description:        convert path name (can be file or directory) to its
                        fst entry number

    Arguments:          pathPtr  path name (file or directory)

    Returns:            if found, its entry number is returned.
                        if not, -1 is returned.
 *---------------------------------------------------------------------------*/
s32 DVDConvertPathToEntrynum(const char* pathPtr)
{
    const char*  ptr;
    char*        stringPtr;
    BOOL         isDir;
    u32          length;
    u32          dirLookAt;
    u32          i;
    const char*  origPathPtr = pathPtr;
    const char*  extentionStart;
    BOOL         illegal;
    BOOL         extention;

    ASSERTMSG(pathPtr,
              "DVDConvertPathToEntrynum(): null pointer is specified  ");

    dirLookAt = currentDirectory;

    while (1)
    {

        // check /, ./, ../
        if (*pathPtr == '\0')
        {
            return (s32)dirLookAt;
        } else if (*pathPtr == '/')
        {
            dirLookAt = 0;
            pathPtr++;
            continue;
        }
        else if (*pathPtr == '.')
        {
            if (*(pathPtr + 1) == '.')
            {
                if (*(pathPtr + 2) == '/')
                {
                    dirLookAt = parentDir(dirLookAt);
                    pathPtr += 3;
                    continue;
                }
                else if (*(pathPtr + 2) == '\0')
                {
                    return (s32)parentDir(dirLookAt);
                }
            }
            else if (*(pathPtr + 1) == '/')
            {
                pathPtr += 2;
                continue;
            }
            else if (*(pathPtr + 1) == '\0')
            {
                return (s32)dirLookAt;
            }
        }

        if (__DVDLongFileNameFlag == 0)
        {
            extention = FALSE;
            illegal = FALSE;

            // directory or file? Can be checked by the endmark
            for(ptr = pathPtr; (*ptr != '\0') && (*ptr != '/'); ptr++)
            {
                if (*ptr == '.')
                {
                    if ( (ptr - pathPtr > 8) || (extention == TRUE) )
                    {
                        illegal = TRUE;
                        break;
                    }
                    extention = TRUE;
                    extentionStart = ptr + 1;

                } else if (*ptr == ' ')
                    illegal = TRUE;
            }

            if ( (extention == TRUE) && (ptr - extentionStart > 3) )
                illegal = TRUE;

            if (illegal)
                OSPanic(__FILE__, __LINE__,
                        "DVDConvertEntrynumToPath(possibly DVDOpen or DVDChangeDir or DVDOpenDir): specified directory or file (%s) doesn't match standard 8.3 format. This is a temporary restriction and will be removed soon\n",
                        origPathPtr);
        }
        else
        {
            // directory or file? Can be checked by the endmark
            for(ptr = pathPtr; (*ptr != '\0') && (*ptr != '/'); ptr++)
                ;
        }

        // it's true that one ends with '/' should be a directory name,
        // but one ends with '\0' is not always a file name.
        isDir = (*ptr == '\0')? FALSE : TRUE;
        length = (u32)(ptr - pathPtr);

        ptr = pathPtr;

        for(i = dirLookAt + 1; i < nextDir(dirLookAt);
            i = entryIsDir(i)? nextDir(i): (i+1) )
        {
            // isDir == FALSE doesn't mean it's a file.
            // so it's legal to compare it to a directory
            if ( ( entryIsDir(i) == FALSE ) &&
                 (isDir == TRUE) )
            {
                continue;
            }

            stringPtr = FstStringStart + stringOff(i);

            if ( isSame(ptr, stringPtr) == TRUE )
            {
                goto next_hier;
            }

        } // for()

        return -1;

      next_hier:
        if (! isDir)
        {
            return (s32)i;
        }

        dirLookAt = i;
        pathPtr += length + 1;

    } // while (1)

    // NOT REACHED
}


/*---------------------------------------------------------------------------*
  Name:         DVDFastOpen

  Description:  Opens a file using an entry number

  Arguments:    entrynum    entry number of the file
                fileInfo    pointer to file info to be used

  Returns:      TRUE if the function succeeds.
 *---------------------------------------------------------------------------*/
BOOL DVDFastOpen(s32 entrynum, DVDFileInfo* fileInfo)
{
    ASSERTMSG(fileInfo,
              "DVDFastOpen(): null pointer is specified to file info address  ");
    ASSERTMSG1((0 <= entrynum) && (entrynum < MaxEntryNum),
               "DVDFastOpen(): specified entry number '%d' is out of range  ",
               entrynum);
    ASSERTMSG1(!entryIsDir(entrynum),
               "DVDFastOpen(): entry number '%d' is assigned to a directory  ",
               entrynum);

    if ( (entrynum < 0) || (entrynum >= MaxEntryNum) || entryIsDir(entrynum) )
    {
        return FALSE;
    }

    fileInfo->startAddr = filePosition(entrynum);
    fileInfo->length    = fileLength(entrynum);
    fileInfo->callback  = (DVDCallback)NULL;
    fileInfo->cb.state  = DVD_STATE_END;

    return TRUE;
}

/*---------------------------------------------------------------------------*
  Name:         DVDOpen

  Description:  Opens a file.

  Arguments:    fileName    pointer to file name to be opened
                fileInfo    pointer to file info to be used

  Returns:      TRUE if the function succeeds.
 *---------------------------------------------------------------------------*/
BOOL DVDOpen(const char* fileName, DVDFileInfo* fileInfo)
{
    s32         entry;
    char        currentDir[128];

    ASSERTMSG( fileName, DVD_ERR_OPEN_NULLPATHPTR );
    ASSERTMSG( fileInfo, DVD_ERR_OPEN_NULLFILEINFO );

    entry = DVDConvertPathToEntrynum(fileName);

    if (0 > entry)
    {
        DVDGetCurrentDir(currentDir, 128);
        OSReport("Warning: DVDOpen(): file '%s' was not found under %s.\n",
                 fileName, currentDir);
        return FALSE;
    }

    if (entryIsDir(entry))
    {
        ASSERTMSG1( !entryIsDir(entry), DVD_ERR_OPEN_ISDIR, fileName );
        return FALSE;
    }

    fileInfo->startAddr = filePosition(entry);
    fileInfo->length    = fileLength(entry);
    fileInfo->callback  = (DVDCallback)NULL;
    fileInfo->cb.state  = DVD_STATE_END;

    return TRUE;
}


/*---------------------------------------------------------------------------*
  Name:         DVDClose

  Description:  Closes a file

  Arguments:    fileInfo    file info of the file to be closed

  Returns:      TRUE if the function succeeds.
 *---------------------------------------------------------------------------*/
BOOL DVDClose(DVDFileInfo* fileInfo)
{
    ASSERTMSG( fileInfo, DVD_ERR_CLOSE_NULLFILEINFO );

    DVDCancel(&(fileInfo->cb));


    return TRUE;
}

/*---------------------------------------------------------------------------*
  Name:         myStrncpy

  Description:  copy a string. Difference from standard strncpy is:
                    1. do not pad dest with null characters
                    2. do not terminate dest with a null
                    3. return the number of chars copied

  Arguments:    dest        destination
                src         source
                maxlen      maximum length of copy

  Returns:      Num of chars copied
 *---------------------------------------------------------------------------*/
static u32 myStrncpy(char* dest, char* src, u32 maxlen)
{
    u32         i = maxlen;

    while( (i > 0) && (*src != 0) )
    {
        *dest++ = *src++;
        i--;
    }

    return (maxlen - i);
}

/*---------------------------------------------------------------------------*
  Name:         entryToPath

  Description:  Convert from fst entry to path. The result is not null
                terminated.

  Arguments:    entry       FST entry
                path        pointer to store the result
                maxlen      size of the buffer start from path

  Returns:      Num of chars copied
 *---------------------------------------------------------------------------*/
static u32 entryToPath(u32 entry, char* path, u32 maxlen)
{
    char*       name;
    u32         loc;

    if (entry == 0)
    {
        return 0;
    }

    name = FstStringStart + stringOff(entry);

    loc = entryToPath(parentDir(entry), path, maxlen);

    if (loc == maxlen)
    {
        return loc;
    }

    *(path + loc++) = '/';

    loc += myStrncpy(path + loc, name, maxlen - loc);

    return loc;
}


/*---------------------------------------------------------------------------*
  Name:         DVDConvertEntrynumToPath

  Description:  Convert from fst entry to path.

  Arguments:    entrynum    FST entry
                path        pointer to store the result
                maxlen      size of the buffer start from path

  Returns:      TRUE if all the path fits in maxlen. FALSE if not (in this
                case, it's truncated to fit maxlen).
 *---------------------------------------------------------------------------*/
static BOOL DVDConvertEntrynumToPath(s32 entrynum, char* path, u32 maxlen)
{
    u32         loc;

    ASSERTMSG1((0 <= entrynum) && (entrynum < MaxEntryNum),
               "DVDConvertEntrynumToPath: specified entrynum(%d) is out of range  ",
                entrynum );
    ASSERTMSG1(1 < maxlen, "DVDConvertEntrynumToPath: maxlen should be more than 1 (%d is specified)",
               maxlen );
    // currently only directories can be converted to path
    // finding the directory where a file is located is not so easy
    // while finding the parent directory of a directory is a piece of cake.
    ASSERTMSG(entryIsDir(entrynum),
              "DVDConvertEntrynumToPath: cannot convert an entry num for a file to path  ");

    loc = entryToPath((u32)entrynum, path, maxlen);

    if (loc == maxlen)
    {
        // Overwrite the last char with NULL
        path[maxlen - 1] = '\0';
        return FALSE;
    }

    // For directories, put '/' at the last
    if (entryIsDir(entrynum))
    {
        if (loc == maxlen - 1)
        {
            // There's no room to put the last '/', so just put '\0' and return FALSE
            path[loc] = '\0';
            return FALSE;
        }

        path[loc++] = '/';
    }

    path[loc] = '\0';
    return TRUE;
}

/*---------------------------------------------------------------------------*
  Name:         DVDGetCurrentDir

  Description:  Get current directory

  Arguments:    path        pointer to store the result
                maxlen      size of the buffer start from path

  Returns:      TRUE if all the path fits in maxlen. FALSE if not (in this
                case, it's truncated to fit maxlen).
 *---------------------------------------------------------------------------*/
BOOL DVDGetCurrentDir(char* path, u32 maxlen)
{
    ASSERTMSG1( 1 < maxlen, "DVDGetCurrentDir: maxlen should be more than 1 (%d is specified)",
                maxlen );

    return DVDConvertEntrynumToPath((s32)currentDirectory, path, maxlen);
}

/*---------------------------------------------------------------------------*
  Name:         DVDChangeDir

  Description:  Change current directory

  Arguments:    dirName     directory name to change

  Returns:      TRUE if the function succeeds.
                FALSE if not (i.e. the directory doesn't exist)
 *---------------------------------------------------------------------------*/
BOOL DVDChangeDir(const char* dirName)
{
    s32         entry;
#ifdef _DEBUG
    char        currentDir[128];
#endif

    ASSERTMSG( dirName, DVD_ERR_CHANGEDIR_NULLPATHPTR );

    entry = DVDConvertPathToEntrynum(dirName);

#ifdef _DEBUG
    if (0 > entry)
    {
        DVDGetCurrentDir(currentDir, 128);
        OSPanic(__FILE__, __LINE__, DVD_ERR_CHANGEDIR_NOENT, dirName,
                currentDir);
    }
#endif
    ASSERTMSG1( entryIsDir(entry),  DVD_ERR_CHANGEDIR_NOTDIR, dirName );

    if ( (entry < 0) || (entryIsDir(entry) == FALSE) )
    {
        return FALSE;
    }

    currentDirectory = (u32)entry;

    return TRUE;
}

/*---------------------------------------------------------------------------*
  Name:         DVDReadAsyncPrio

  Description:  Reads data from a file asynchronously.

  Arguments:    fileInfo    file info of the file to be read
                addr        address of buffer
                offset      file position at which to start the read
                length      number of bytes to be read
                callback    address of callback function to notify the result
                            of the read
                prio        priority

  Returns:      TRUE if the function succeeds.
 *---------------------------------------------------------------------------*/
BOOL DVDReadAsyncPrio(DVDFileInfo* fileInfo, void* addr, s32 length, s32 offset,
                      DVDCallback callback, s32 prio)
{
    // check specified pointers are not nulls
    ASSERTMSG( fileInfo, DVD_ERR_READASYNC_NULLFILEINFO );
    ASSERTMSG( addr, DVD_ERR_READASYNC_NULLADDR );

    // check alignment is OK
    ASSERTMSG( OFFSET(addr,   DVD_ALIGN_ADDR) == 0,
               DVD_ERR_READASYNC_INVADDR );
    ASSERTMSG( OFFSET(length, DVD_ALIGN_SIZE) == 0,
               DVD_ERR_READASYNC_INVSIZE );
    ASSERTMSG( OFFSET(offset, DVD_ALIGN_OFFSET) == 0,
               DVD_ERR_READASYNC_INVOFFSET );

    // check specified area is in the file
    if (! ((0 <= offset) && (offset <= fileInfo->length)) )
    {
        OSPanic(__FILE__, __LINE__, DVD_ERR_READASYNC_OUTOFRANGE);
    }

    if (! ((0 <= offset + length) &&
           (offset + length < fileInfo->length + DVD_MIN_TRANSFER_SIZE)))
    {
        OSPanic(__FILE__, __LINE__, DVD_ERR_READASYNC_OUTOFRANGE);
    }

    fileInfo->callback = callback;
    DVDReadAbsAsyncPrio(&(fileInfo->cb), addr, length,
                        (s32)(fileInfo->startAddr + offset),
                        cbForReadAsync, prio);

    return TRUE;
}

static void cbForReadAsync(s32 result, DVDCommandBlock* block)
{
    DVDFileInfo* fileInfo;


    fileInfo = (DVDFileInfo*)
               ((char*)block - offsetof(DVDFileInfo, cb));
    ASSERT((void*) &fileInfo->cb == (void*) block);

    if (fileInfo->callback)
    {
        (fileInfo->callback)(result, fileInfo);
    }

}

/*---------------------------------------------------------------------------*
  Name:         DVDReadPrio

  Description:  Reads data from a file.

  Arguments:    fileInfo    file info of the file to be read
                addr        address of buffer
                offset      file position at which to start the read
                length      number of bytes to be read
                prio        Priority

  Returns:      If the function succeeds, it returns the number of bytes read.
                If the function fails, it returns -1.
 *---------------------------------------------------------------------------*/
s32 DVDReadPrio(DVDFileInfo* fileInfo, void* addr, s32 length, s32 offset, s32 prio)
{
    BOOL                result;
    DVDCommandBlock*    block;
    s32                 state;
    BOOL                enabled;
    s32                 retVal;

    // check specified pointers are not nulls
    ASSERTMSG( fileInfo, DVD_ERR_READ_NULLFILEINFO );
    ASSERTMSG( addr, DVD_ERR_READ_NULLADDR );

    // check alignment is OK
    ASSERTMSG(OFFSET(addr,   DVD_ALIGN_ADDR) == 0,
              DVD_ERR_READ_INVADDR);
    ASSERTMSG(OFFSET(length, DVD_ALIGN_SIZE) == 0,
              DVD_ERR_READ_INVSIZE);
    ASSERTMSG(OFFSET(offset, DVD_ALIGN_OFFSET) == 0,
              DVD_ERR_READ_INVOFFSET);

    // check specified area is in the file
    if (! ((0 <= offset) && (offset <= fileInfo->length)) )
    {
        OSPanic(__FILE__, __LINE__, DVD_ERR_READ_OUTOFRANGE);
    }

    if (! ((0 <= offset + length) &&
           (offset + length < fileInfo->length + DVD_MIN_TRANSFER_SIZE)))
    {
        OSPanic(__FILE__, __LINE__, DVD_ERR_READ_OUTOFRANGE);
    }

    block = &(fileInfo->cb);

    // should be changed to use polling the register of Flipper?
    // then, we can disable interrupts in the function.
    result = DVDReadAbsAsyncPrio(block, addr, length,
                                 (s32)(fileInfo->startAddr + offset),
                                 cbForReadSync, prio);

    // actually, result is always TRUE
    if (result == FALSE)
    {
        return -1;
    }

    enabled = OSDisableInterrupts();

    for (;;)
    {
        state = ( (volatile DVDCommandBlock*)block )->state;

        if (state == DVD_STATE_END)
        {
            retVal = (s32)block->transferredSize;
            break;
        }
        if (state == DVD_STATE_FATAL_ERROR)
        {
            retVal = DVD_RESULT_FATAL_ERROR;
            break;
        }
        if (state == DVD_STATE_CANCELED)
        {
            retVal = DVD_RESULT_CANCELED;
            break;
        }

        OSSleepThread(&__DVDThreadQueue);
    }

    OSRestoreInterrupts(enabled);
    return retVal;
}


static void cbForReadSync(s32 result, DVDCommandBlock* block)
{
#pragma unused(block)
#pragma unused(result)

    OSWakeupThread(&__DVDThreadQueue);
}

/*---------------------------------------------------------------------------*
  Name:         DVDSeekAsyncPrio

  Description:  Heads the drive pickup to the specified file asynchronously

  Arguments:    fileInfo    file info of the file to be closed
                offset      file position at which to head the pickup
                callback    address of callback function to notify the result
                            of the call
                prio        priority

  Returns:      TRUE if the function succeeds.
 *---------------------------------------------------------------------------*/
BOOL DVDSeekAsyncPrio(DVDFileInfo* fileInfo, s32 offset, DVDCallback callback,
                      s32 prio)
{
    // check specified pointer is not null
    ASSERTMSG( fileInfo, DVD_ERR_SEEK_NULLFILEINFO );

    // check alignment is OK
    ASSERTMSG( OFFSET(offset, DVD_ALIGN_OFFSET) == 0,
               DVD_ERR_SEEK_INVOFFSET);

    // check specified area is in the file
    if (!((0 <= offset) && (offset <= fileInfo->length)))
    {
        OSPanic(__FILE__, __LINE__, DVD_ERR_SEEK_OUTOFRANGE);
    }

    fileInfo->callback = callback;
    DVDSeekAbsAsyncPrio(&(fileInfo->cb), (s32)(fileInfo->startAddr + offset),
                        cbForSeekAsync, prio);

    return TRUE;
}


static void cbForSeekAsync(s32 result, DVDCommandBlock* block)
{
    DVDFileInfo* fileInfo;


    fileInfo = (DVDFileInfo*)
               ((char*)block - offsetof(DVDFileInfo, cb));
    ASSERT((void*) &fileInfo->cb == (void*) block);

    if (fileInfo->callback)
    {
        (fileInfo->callback)(result, fileInfo);
    }

}

/*---------------------------------------------------------------------------*
  Name:         DVDSeekPrio

  Description:  Heads the drive pickup to the specified file synchronously

  Arguments:    fileInfo    file info of the file to be closed
                offset      file position at which to head the pickup
                prio        priority

  Returns:      If the function succeeds, it returns 0.
                If the function fails, it returns -1.
 *---------------------------------------------------------------------------*/
s32 DVDSeekPrio(DVDFileInfo* fileInfo, s32 offset, s32 prio)
{
    BOOL                result;
    DVDCommandBlock*    block;
    s32                 state;
    BOOL                enabled;
    s32                 retVal;

    // check specified pointers are not nulls
    ASSERTMSG( fileInfo, DVD_ERR_SEEK_NULLFILEINFO );

    // check alignment is OK
    ASSERTMSG(OFFSET(offset, DVD_ALIGN_OFFSET) == 0,
              DVD_ERR_SEEK_INVOFFSET);

    // check specified area is in the file
    ASSERTMSG( (0 <= offset) && (offset <= fileInfo->length),
               DVD_ERR_SEEK_OUTOFRANGE );

    block = &(fileInfo->cb);

    // should be changed to use polling the register of Flipper?
    // then, we can disable interrupts in the function.
    result = DVDSeekAbsAsyncPrio(block, (s32)(fileInfo->startAddr + offset),
                                 cbForSeekSync, prio);

    // actually, result is always TRUE
    if (result == FALSE)
    {
        return -1;
    }

    enabled = OSDisableInterrupts();

    for (;;)
    {
        state = ( (volatile DVDCommandBlock*)block )->state;

        if (state == DVD_STATE_END)
        {
            retVal = 0;
            break;
        }
        if (state == DVD_STATE_FATAL_ERROR)
        {
            retVal = DVD_RESULT_FATAL_ERROR;
            break;
        }
        if (state == DVD_STATE_CANCELED)
        {
            retVal = DVD_RESULT_CANCELED;
            break;
        }

        OSSleepThread(&__DVDThreadQueue);
    }

    OSRestoreInterrupts(enabled);
    return retVal;
}


static void cbForSeekSync(s32 result, DVDCommandBlock* block)
{
#pragma unused(block)
#pragma unused(result)

    OSWakeupThread(&__DVDThreadQueue);
}


/*---------------------------------------------------------------------------*
  Name:         DVDGetFileInfoStatus

  Description:  Inspect file info status

  Arguments:    fileInfo    file info of the file to inspect
                            of the call

  Returns:
 *---------------------------------------------------------------------------*/
s32 (DVDGetFileInfoStatus)(const DVDFileInfo* fileInfo)
{
    return DVDGetCommandBlockStatus(&fileInfo->cb);
}


/* Directory functions */

/*---------------------------------------------------------------------------*
  Name:         DVDFastOpenDir

  Description:  Opens a dir using an entry number

  Arguments:    entrynum    entry number of the file
                dir         pointer to dir structure to open

  Returns:      TRUE if the function succeeds.
 *---------------------------------------------------------------------------*/
BOOL DVDFastOpenDir(s32 entrynum, DVDDir* dir)
{
    ASSERTMSG(dir,
              "DVDFastOpenDir(): null pointer is specified to dir structure address  ");
    ASSERTMSG1((0 <= entrynum) && (entrynum < MaxEntryNum),
               "DVDFastOpenDir(): specified entry number '%d' is out of range  ",
               entrynum);
    ASSERTMSG1(entryIsDir(entrynum),
               "DVDFastOpenDir(): entry number '%d' is assigned to a file  ",
               entrynum);

    if ( (entrynum < 0) || (entrynum >= MaxEntryNum) || !entryIsDir(entrynum) )
    {
        return FALSE;
    }

    dir->entryNum = (u32)entrynum;
    dir->location = (u32)entrynum + 1;
    dir->next = nextDir(entrynum);

    return TRUE;
}

/*---------------------------------------------------------------------------*
  Name:         DVDOpenDir

  Description:  Opens a directory and associates dir with it

  Arguments:    dirName     directory to open
                dir         DVDDir structure to associate

  Returns:      TRUE on succeed, FALSE if not.
 *---------------------------------------------------------------------------*/
BOOL DVDOpenDir(const char* dirName, DVDDir* dir)
{
    s32         entry;
    char        currentDir[128];

    ASSERTMSG(dirName, DVD_ERR_OPENDIR_NULLPATHPTR);
    ASSERTMSG(dir, "DVDOpenDir(): null pointer is specified to dir structure address  ");

    entry = DVDConvertPathToEntrynum(dirName);

    if (entry < 0)
    {
        DVDGetCurrentDir(currentDir, 128);
        OSReport("Warning: DVDOpenDir(): file '%s' was not found under %s.\n",
                dirName, currentDir);
        return FALSE;
    }

    if (!entryIsDir(entry))
    {
        ASSERTMSG1( entryIsDir(entry), DVD_ERR_OPENDIR_NOTDIR, dirName );
        return FALSE;
    }

    dir->entryNum = (u32)entry;
    dir->location = (u32)entry + 1;
    dir->next = nextDir(entry);

    return TRUE;
}

/*---------------------------------------------------------------------------*
  Name:         DVDReadDir

  Description:  Gets the next directory entry

  Arguments:    dir         DVDDir structure
                dirent      The next directory entry

  Returns:      TRUE if there's any directory entry. FALSE if there's none.
 *---------------------------------------------------------------------------*/
BOOL DVDReadDir(DVDDir* dir, DVDDirEntry* dirent)
{
    u32         loc;

    // Check the next location. loc == dir->next means it reached the
    // end of the directory. Other check is for illegal setting by DVDSeekDir.
    loc = dir->location;
    if ( (loc <= dir->entryNum) || (dir->next <= loc) )
        return FALSE;

    dirent->entryNum = loc;
    dirent->isDir = entryIsDir(loc);
    dirent->name = FstStringStart + stringOff(loc);

    dir->location = entryIsDir(loc)? nextDir(loc) : (loc+1);

    return TRUE;
}

/*---------------------------------------------------------------------------*
  Name:         DVDCloseDir

  Description:  Close the directory

  Arguments:    dir         DVDDir structure for the directory to close

  Returns:      TRUE on success, FALSE if not.
 *---------------------------------------------------------------------------*/
BOOL DVDCloseDir(DVDDir* dir)
{
#pragma unused (dir)

    return TRUE;
}

/*---------------------------------------------------------------------------*
  Name:         DVDRewindDir

  Description:  Resets the position of the directory to the beginning

  Arguments:    dir         Pre-opened DVDDir* structure

  Returns:      None
 *---------------------------------------------------------------------------*/
void DVDRewindDir(DVDDir* dir)
{
    dir->location = dir->entryNum + 1;
}

/*---------------------------------------------------------------------------*
  Name:         DVDGetFSTLocation

  Description:  Returns the current FST location

  Arguments:    None

  Returns:      current FST location
 *---------------------------------------------------------------------------*/
void* DVDGetFSTLocation(void)
{
    return BootInfo->FSTLocation;
}

/*---------------------------------------------------------------------------*
  Name:         DVDPrepareStreamAsync

  Description:  initialize a stream track

  Arguments:    fileInfo    file info of the file to inspect
                            of the call
                length      length of the track to play
                offset      start offset of the track to play
                callback    callback

  Returns:      TRUE if it successfully issued the command.
                FALSE if the command block is already used.
 *---------------------------------------------------------------------------*/
#define RoundUp32KB(x)      (((u32)(x) + 32*1024 - 1) & ~(32*1024 - 1))
#define Is32KBAligned(x)    (((u32)(x) & (32*1024 - 1)) == 0)

BOOL DVDPrepareStreamAsync(DVDFileInfo *fileInfo, u32 length, u32 offset,
                           DVDCallback callback)
{
    u32         start;

    ASSERTMSG(fileInfo,
              "DVDPrepareStreamAsync(): NULL file info was specified");

    start = fileInfo->startAddr + offset;

    if (!Is32KBAligned(start))
    {
        OSPanic(__FILE__, __LINE__,
               "DVDPrepareStreamAsync(): Specified start address (filestart(0x%x) + offset(0x%x)) is not 32KB aligned",
                fileInfo->startAddr, offset);
    }

    if (length == 0)
        length = fileInfo->length - offset;

    if (!Is32KBAligned(length))
    {
        OSPanic(__FILE__, __LINE__,
                "DVDPrepareStreamAsync(): Specified length (0x%x) is not a multiple of 32768(32*1024)",
                length);
    }

    // check specified area is in the file
    if (! ((offset <= fileInfo->length) && (offset + length <= fileInfo->length)) )
    {
        OSPanic(__FILE__, __LINE__,
                "DVDPrepareStreamAsync(): The area specified (offset(0x%x), length(0x%x)) is out of the file",
                offset, length);
    }


    fileInfo->callback = callback;
    return DVDPrepareStreamAbsAsync(&(fileInfo->cb), length,
                                    fileInfo->startAddr + offset,
                                    cbForPrepareStreamAsync);
}

static void cbForPrepareStreamAsync(s32 result, DVDCommandBlock* block)
{
    DVDFileInfo* fileInfo;


    fileInfo = (DVDFileInfo*)
               ((char*)block - offsetof(DVDFileInfo, cb));
    ASSERT((void*) &fileInfo->cb == (void*) block);

    if (fileInfo->callback)
    {
        (fileInfo->callback)(result, fileInfo);
    }

}

/*---------------------------------------------------------------------------*
  Name:         DVDPrepareStream

  Description:  initialize a stream track

  Arguments:    fileInfo    file info of the file to inspect
                            of the call
                length      length of the track to play
                offset      start offset of the track to play
                callback    callback

  Returns:      If the function succeeds, it returns 0.
                If the function fails, it returns -1.
 *---------------------------------------------------------------------------*/
s32 DVDPrepareStream(DVDFileInfo *fileInfo, u32 length, u32 offset)
{
    BOOL                result;
    DVDCommandBlock*    block;
    s32                 state;
    BOOL                enabled;
    s32                 retVal;
    u32                 start;

    // check specified pointers are not nulls
    ASSERTMSG(fileInfo,
              "DVDPrepareStream(): NULL file info was specified");

    start = fileInfo->startAddr + offset;

    if (!Is32KBAligned(start))
    {
        OSPanic(__FILE__, __LINE__,
                "DVDPrepareStream(): Specified start address (filestart(0x%x) + offset(0x%x)) is not 32KB aligned",
                fileInfo->startAddr, offset);
    }

    if (length == 0)
        length = fileInfo->length - offset;

    if (!Is32KBAligned(length))
    {
        OSPanic(__FILE__, __LINE__,
                "DVDPrepareStream(): Specified length (0x%x) is not a multiple of 32768(32*1024)",
                length);
    }

    // check specified area is in the file
    if (! ((offset <= fileInfo->length) && (offset + length <= fileInfo->length)) )
    {
        OSPanic(__FILE__, __LINE__,
                "DVDPrepareStream(): The area specified (offset(0x%x), length(0x%x)) is out of the file",
                offset, length);
    }

    block = &(fileInfo->cb);
    result = DVDPrepareStreamAbsAsync(block, length, start,
                                      cbForPrepareStreamSync);

    // actually, result is always TRUE
    if (result == FALSE)
    {
        return -1;
    }

    enabled = OSDisableInterrupts();

    for (;;)
    {
        state = ( (volatile DVDCommandBlock*)block )->state;

        if (state == DVD_STATE_END)
        {
            retVal = 0;
            break;
        }
        if (state == DVD_STATE_FATAL_ERROR)
        {
            retVal = DVD_RESULT_FATAL_ERROR;
            break;
        }
        if (state == DVD_STATE_CANCELED)
        {
            retVal = DVD_RESULT_CANCELED;
            break;
        }

        OSSleepThread(&__DVDThreadQueue);
    }

    OSRestoreInterrupts(enabled);
    return retVal;
}


static void cbForPrepareStreamSync(s32 result, DVDCommandBlock* block)
{
#pragma unused(block)
#pragma unused(result)

    OSWakeupThread(&__DVDThreadQueue);
}


s32 DVDGetTransferredSize(DVDFileInfo* fileinfo)
{
    s32                     bytes;
    DVDCommandBlock*        cb;

    cb = &(fileinfo->cb);

    switch (cb->state)
    {
      case DVD_STATE_END:
      case DVD_STATE_COVER_CLOSED:
      case DVD_STATE_NO_DISK:
      case DVD_STATE_COVER_OPEN:
      case DVD_STATE_WRONG_DISK:
      case DVD_STATE_FATAL_ERROR:
      case DVD_STATE_MOTOR_STOPPED:
      case DVD_STATE_CANCELED:
      case DVD_STATE_RETRY:
        bytes = (s32)cb->transferredSize;
        break;

      case DVD_STATE_WAITING:
        bytes = 0;
        break;

      case DVD_STATE_BUSY:
        bytes = (s32)(cb->transferredSize +
                      (cb->currTransferSize - __DIRegs[DI_LENGTH_IDX]));
        break;

      default:
        ASSERTMSG1(FALSE, "DVDGetTransferredSize(): Illegal state (%d)", cb->state );
        break;
    }

    return bytes;
}

