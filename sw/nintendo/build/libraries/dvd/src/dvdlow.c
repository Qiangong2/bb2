/*---------------------------------------------------------------------------*
  Project:  Dolphin DVD low-level functions
  File:     dvdlow.c

  Copyright 1998-2001 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: dvdlow.c,v $
  Revision 1.1.1.1  2004/01/03 01:02:28  paulm
  Gamecube library source from Nintendo

    
    33    03/07/23 10:53 Hashida
    Modification for 2003May SDK patch1.
    
    32    03/06/18 14:20 Hashida
    Added __DVDTestAlarm().
    
    31    03/03/28 22:59 Hashida
    Fixed a cover bug in IPL.
    
    30    02/10/21 10:14 Hashida
    Made symbols that are shared with Triforce lib (gcam.a) weak.
    
    29    02/02/27 16:54 Hashida
    Fixed a bug that clears alarm even when cover int occurs.
    
    28    1/05/02 5:37a Hashida
    Cleaned up.
    
    27    7/30/01 3:42p Hashida
    Fixed a bug in logging.
    Added to log DVDStreamGetErrorStatus.
    
    26    7/22/01 8:14a Hashida
    Changed to clear StopAtNextInt explicitly.

    25    7/22/01 4:51a Hashida
    Changed not to use break for DVDCancel.
    
    24    7/19/01 1:44a Hashida
    Changed not to expect the "safe" area right after read is issued.
    
    23    7/18/01 10:38p Hashida
    Added a logging feature.
    Added a workaround for break bug.
    
    22    6/18/01 2:13p Hashida
    Added timeout error.
    
    21    6/15/01 8:43p Hashida
    Deleted several workaround types. No workaround is needed anymore.
    
    20    6/11/01 3:39p Hashida
    Added workaround for MEI drive bug, but now it defaults to "do
    nothing".
    
    19    5/09/01 4:53p Hashida
    Added a check for 0 length read request.
    
    18    5/07/01 2:13p Tian
    Applied OSClearContext to callback exception contexts.
    
    17    4/16/01 6:42p Hashida
    Implemented an added feature for audio buffer config command (audio
    buffer trigger size).
    
    16    3/22/01 1:40p Hashida
    Added a debug code.
    
    15    3/07/01 6:51p Hashida
    Fixed a bug when break happens.
    
    14    3/02/01 12:07p Hashida
    Added DVDLowGetCoverStatus
    
    13    3/02/01 12:15a Hashida
    Added DVDCancel.
    
    12    2/23/01 4:26p Hashida
    Implemented reset cover interrupt callback so that cover close check
    can be done sooner.
    
    11    2/21/01 12:09p Hashida
    Added inquiry command.
    
    10    2/21/01 3:45a Hashida
    Changed to use __OSGetSystemTime instead of OSGetTime.
    
    9     11/14/00 4:32p Hashida
    The previous change is  not appropriate; the workaround should be done
    in ai.c.
    
    8     11/14/00 10:07a Hashida
    Added a workaround for Flipper Rev.B bug.
    
    7     10/03/00 10:07a Hashida
    Added support for late cover int after reset.
    
    6     9/20/00 3:31p Hashida
    Changed a bit for DVDLowWaitCoverClose so that it can be called from
    BS2 without interrupts disabled.
    
    5     4/17/00 1:32p Hashida
    Added DVDLowAudioBufferConfig.
    
    4     4/07/00 6:51p Hashida
    Added streaming APIs.
    
    3     1/17/00 6:45p Shiki
    Removed OSLoadContext() from the interrupt handler (for thread
    support).

    2     12/23/99 1:33a Hashida
    Fixed a part where happened to issue system reset.

    21    11/05/99 12:14p Hashida
    Changed the offset for read and seek to word address, not byte address.

    20    9/09/99 2:15p Hashida
    Removed warnings.

    19    8/27/99 5:04p Shiki
    Revised since OS no longer cares DI module level interrupt registers.

    18    8/25/99 3:05p Hashida
    Changed not to use DVDRead/WriteReg macros but to use array defined by
    OS.

    17    8/20/99 9:55p Hashida
    Fixed a bug that I misunderstood the meaning of RW bit of control
    register.

    16    8/19/99 6:18p Hashida
    Changed to use __OSUnmaskInterrupts.
    Changed so that DVDInterruptHandler is called from OS external
    interrupt handler.

    15    8/17/99 1:40p Hashida
    Changed to use ArtX's register definition.

    14    8/04/99 2:40p Hashida
    Added more comments.
    Moved the part to clear cover interrupt to dvd.c.
    Removed the part to disable interrupts because the functions are always
    called with interrupts disabled.

    13    8/04/99 11:31a Hashida
    Changed DVDLowReset() to a real code.

    12    7/29/99 6:16p Hashida
    Added temporary reset command

    11    7/29/99 12:20p Hashida
    Fixed a bug that I forgot to register callback in WaitCoverClose
    function

    10    7/27/99 11:12p Hashida
    changed seek, stop motor, request error to real codes.

    9     7/27/99 9:09p Hashida
    Moved the part that copies data from disk image to specified address to
    timer interrupt handler.

    8     7/27/99 6:46p Hashida
    Changed to use __DVDReadReg, __DVDWriteReg
    Fixed a bug that mask bits are not written.

    7     7/27/99 3:20p Hashida
    Removed TIMEREMU definition check since this file is now always
    compiled with TIMEREMU defined.

    6     7/27/99 2:49p Hashida
    Added some codes for level2 (timer-based) emulation

    5     7/23/99 5:01p Hashida
    Changed not to use __diskImage symbol but a fixed address for disk
    image.
    Changed to use uncached address instead of using cached address to copy
    disk data to ram.

    4     7/20/99 10:30p Hashida
    Added DVDLowSeek

    3     7/19/99 4:31p Hashida
    Changed low level callback

    2     7/10/99 3:02a Hashida
    added read disk id

    1     7/08/99 5:06p Hashida
    initial revision

  $NoKeywords: $
 *---------------------------------------------------------------------------*/

// Dolphin header files
#include <dolphin/os.h>
#include <dolphin/dvd.h>
#include <secure/dvdlow.h>
#include <secure/dvdlayout.h>
#include <private/OSRtc.h>
#include "DVDLowAssert.h"
#include "direg.h"


#define BREAK_WORKAROUND                    1
#define BREAK_WORKAROUND_POLLING_TIME       OSMillisecondsToTicks(20)
//#define BREAK_WORKAROUND_POLLING_TIME       OSMicrosecondsToTicks(15)
#define NO_BREAK

//#define LOGGING         1

#ifdef NO_BREAK
static volatile BOOL    StopAtNextInt = FALSE;
#endif

static volatile u32     LastLength;

static DVDLowCallback   Callback;
static DVDLowCallback   ResetCoverCallback = NULL;
static volatile OSTime  LastResetEnd;
static volatile u32     ResetOccurred = 0;
static volatile BOOL    WaitingCoverClose = FALSE;

static volatile BOOL    Breaking = FALSE;

static volatile u32     WorkAroundType;
static volatile s32     WorkAroundSeekLocation;

static volatile OSTime  LastReadFinished;
static volatile OSTime  LastReadIssued;
static volatile BOOL    LastCommandWasRead = FALSE;
static volatile BOOL    FirstRead = TRUE;

typedef struct
{
    s32         command;
    void*       address;
    u32         length;
    u32         offset;
    DVDLowCallback      callback;
} CommandInfo;

static CommandInfo CommandList[3];
static volatile s32     NextCommandNumber = 0;

// Prototypes
__declspec(weak) void __DVDInterruptHandler(__OSInterrupt interrupt, OSContext* context);
static void Read(void* addr, u32 length, u32 offset, DVDLowCallback callback);

// temporarily, we use the following macro. Probably, Flipper can accept
// cached or uncached address for the registers that holds RAM address.
#define toPhysical(vaddr)       (u32)((u32)(vaddr) & ~0xc0000000)

#define SR_ALLINT               (DI_SR_BRKINT_MASK |      \
                                 DI_SR_TCINT_MASK |       \
                                 DI_SR_DEINT_MASK)
#define SR_ALLINTMASK           (DI_SR_BRKINTMSK_MASK | \
                                 DI_SR_TCINTMSK_MASK |  \
                                 DI_SR_DEINTMSK_MASK)

#define RoundUp32KB(x)          (((u32)(x) + 32*1024 - 1) & ~(32*1024 - 1))
#define RoundDown32KB(x)        (((u32)(x)) & ~(32*1024 - 1))

static OSAlarm                  AlarmForWA;
static OSAlarm                  AlarmForTimeout;
static OSAlarm                  AlarmForBreak;

#define WORKAROUND_WAIT         OSMillisecondsToTicks(5)
#define WAIT_MARGIN             OSMicrosecondsToTicks(500)

typedef struct
{
    volatile void*       addr;
    volatile u32         length;
    volatile u32         offset;
} Packet;

static Packet           Prev;
static Packet           Curr;


__declspec(weak) void __DVDInitWA(void)
{
    NextCommandNumber = 0;
    CommandList[0].command = -1;
    
//    __DVDLowSetWAType(DVD_WATYPE_COMBO1, -8 * (2*1024));   // -8 sectors
    __DVDLowSetWAType(DVD_WATYPE_NONE, 0);

    OSInitAlarm();
}


/*---------------------------------------------------------------------------*
  Name:         ProcessNextCommand

  Description:  processes next command

  Arguments:

  Returns:      TRUE if next command is processed, false if there's no 
                command to process
 *---------------------------------------------------------------------------*/
static BOOL ProcessNextCommand(void)
{
    s32             n;

    n = NextCommandNumber;
    ASSERT(n < 3);
    if (CommandList[n].command == DVD_COMMAND_READ)
    {
        NextCommandNumber++;
        Read((void*)CommandList[n].address, CommandList[n].length,
             CommandList[n].offset, CommandList[n].callback);
        return TRUE;
    }
    else if (CommandList[n].command == DVD_COMMAND_SEEK)
    {
        NextCommandNumber++;
        DVDLowSeek(CommandList[n].offset, CommandList[n].callback);
        return TRUE;
    }

    // command = -1, no command to process
    return FALSE;
}

#ifdef LOGGING
typedef struct
{
    u32             event;
    u32             offset;
    u32             length;
    OSTime          start;
    OSTime          end;
    
} TimeStamp;


//static volatile TimeStamp   Stamp[10000];
//static volatile TimeStamp   BreakStamp[10000];
static volatile TimeStamp   EventStamp[100];
//static volatile u32         StampCounter = 0;
//static volatile u32         BreakStampCounter = 0;
static volatile u32         EventStampCounter = 0;
static volatile BOOL        MeasureStart = FALSE;
static volatile BOOL        Wrapped = FALSE;

#if 0
static void StampStart(u32 offset, u32 length)
{
    BOOL                enabled;

    enabled = OSDisableInterrupts();

    if (StampCounter >= 10000)
        StampCounter = 0;
    
//    ASSERT(!MeasureStart);
    ASSERT(StampCounter < 10000);

    MeasureStart = TRUE;

    Stamp[StampCounter].offset = offset;
    Stamp[StampCounter].length = length;
    Stamp[StampCounter].start  = __OSGetSystemTime();

    OSRestoreInterrupts(enabled);
}

static void StampBreak(void)
{
    BOOL                enabled;

    enabled = OSDisableInterrupts();

    if (BreakStampCounter >= 10000)
        BreakStampCounter = 0;

    ASSERT(BreakStampCounter < 10000);

    BreakStamp[BreakStampCounter++].start = __OSGetSystemTime();

    OSRestoreInterrupts(enabled);
}
#endif

__declspec(weak) void StampEvent(u32 event, u32 offset, u32 length)
{
    BOOL                enabled;

    enabled = OSDisableInterrupts();

    if (EventStampCounter >= 100)
    {
        Wrapped = TRUE;
        EventStampCounter = 0;
    }

    EventStamp[EventStampCounter].event = event;
    EventStamp[EventStampCounter].offset = offset;
    EventStamp[EventStampCounter].length = length;
    EventStamp[EventStampCounter].start = __OSGetSystemTime();

    EventStampCounter++;

    OSRestoreInterrupts(enabled);
}

__declspec(weak) void __FlushTimeStamp(void)
{
    u32                 i;
    OSTime              start;
    
    BOOL                enabled;

    enabled = OSDisableInterrupts();


    if (Wrapped)
        start = EventStamp[EventStampCounter].start;
    else
        start = EventStamp[0].start;

    OSReport("-----------------\n");
    if (Wrapped)
    {
        for (i = EventStampCounter; i < 100; i++)
        {
            OSReport("Event: %2d  start: %7llu, offset: 0x%08x, length: 0x%08x\n",
                     EventStamp[i].event,
                     OSTicksToMicroseconds(EventStamp[i].start - start),
                     EventStamp[i].offset, EventStamp[i].length);
        }
    }
    
    for (i = 0; i < EventStampCounter; i++)
    {
        OSReport("Event: %2d  start: %7llu, offset: 0x%08x, length: 0x%08x\n",
                 EventStamp[i].event,
                 OSTicksToMicroseconds(EventStamp[i].start - start),
                 EventStamp[i].offset, EventStamp[i].length);
    }
    OSReport("-----------------\n");

    EventStampCounter = 0;
    Wrapped = FALSE;
    OSRestoreInterrupts(enabled);
}
#endif

/*---------------------------------------------------------------------------*
  Name:         __DVDInterruptHandler

  Description:  interrupt handler for dvd interrupts

  Arguments:

  Returns:      None.
 *---------------------------------------------------------------------------*/
__declspec(weak) void __DVDInterruptHandler(__OSInterrupt interrupt, OSContext* context)
{
    DVDLowCallback  cb;
    OSContext       exceptionContext;
    u32             cause = 0;
    u32             reg;
    u32             intr;
    u32             mask;

#ifndef NO_BREAK
    OSCancelAlarm(&AlarmForBreak);
#endif

#if 0
#ifdef LOGGING
    StampEnd();
#endif
#endif

    if (LastCommandWasRead)
    {
        LastReadFinished = __OSGetSystemTime();
        FirstRead = FALSE;

        Prev.addr = Curr.addr;
        Prev.length = Curr.length;
        Prev.offset = Curr.offset;

#ifdef NO_BREAK
        if (StopAtNextInt == TRUE)
        {
            cause |= DVD_INTTYPE_BRK;
        }
#endif
    }
    LastCommandWasRead = FALSE;

#ifdef NO_BREAK
    StopAtNextInt = FALSE;
#endif

#pragma unused (interrupt)

    reg = __DIRegs[DI_SR_IDX];
    mask = reg & SR_ALLINTMASK;
    intr = (reg & SR_ALLINT) & (mask << 1);

    if ( intr & DI_SR_BRKINT_MASK )
        cause |= DVD_INTTYPE_BRK;

    if ( intr & DI_SR_TCINT_MASK )
        cause |= DVD_INTTYPE_TC;

    if ( intr & DI_SR_DEINT_MASK )
        cause |= DVD_INTTYPE_DE;

#if 0
    OSReport("[%s][%s][%s]0x%08x",
             (cause & DVD_INTTYPE_BRK)? "BRK" : "",
             (cause & DVD_INTTYPE_TC)?  "TC"  : "",
             (cause & DVD_INTTYPE_DE)?  "DE"  : "",
             __DIRegs[DI_CVR_IDX]);
#endif

#ifdef LOGGING
    {
        u32         event = 0;
        
        if (cause & DVD_INTTYPE_BRK)
            event += 1;
        if (cause & DVD_INTTYPE_TC)
            event += 2;
        if (cause & DVD_INTTYPE_DE)
            event += 4;

        StampEvent(event, __DIRegs[DI_SR_IDX], __DIRegs[DI_LENGTH_IDX]);
    }
#endif

    // if one of the following three int occurs, that means it's not "right-
    // after-reset" state anymore. 
    if (cause)
    {
        ResetOccurred = 0;
        OSCancelAlarm(&AlarmForTimeout);
    }
    
    // clear interrupt
    // intr holds interrupts that are already checked
    // mask should not be changed
    __DIRegs[DI_SR_IDX] = intr | mask;

    if (ResetOccurred &&
        ((__OSGetSystemTime() - LastResetEnd) < DVD_RESETCOVER_TIMELAG_TICKS))
    {
        // This interrupt is only be reported when "ResetCoverCallback" is
        // specified. This callback is used in bootrom when it wants to
        // know if cover is closed as soon as possible (if cover is closed,
        // cover interrupt occurs after reset
        reg = __DIRegs[DI_CVR_IDX];
        mask = reg & DI_CVR_CVRINTMSK_MASK;
        intr = (reg & DI_CVR_CVRINT_MASK) & (mask << 1);

        if ( intr & DI_CVR_CVRINT_MASK )
        {
            if (ResetCoverCallback)
            {
                (*ResetCoverCallback)(DVD_INTTYPE_CVR);
            }
            ResetCoverCallback = NULL;      // this callback is one-shot
        }

        // Clear interrupt
        __DIRegs[DI_CVR_IDX] = __DIRegs[DI_CVR_IDX];
    }
    else if (WaitingCoverClose)
    {
        // Waiting for cover close; process the interrupt
        reg = __DIRegs[DI_CVR_IDX];
        mask = reg & DI_CVR_CVRINTMSK_MASK;
        intr = (reg & DI_CVR_CVRINT_MASK) & (mask << 1);

        if ( intr & DI_CVR_CVRINT_MASK )
            cause |= DVD_INTTYPE_CVR;

        // clear interrupt
        __DIRegs[DI_CVR_IDX] = intr | mask;

        WaitingCoverClose = FALSE;
    }
    else
    {
        // Mask the interrupt; it'll be processed later
        __DIRegs[DI_CVR_IDX] = 0;
    }

    // if break occurred while not breaking (this can occur
    // when break asserted but command completed before 
    // break int occurred), ignore the interrupt.
    if ( (cause & DVD_INTTYPE_BRK) && (Breaking == FALSE) )
    {
        cause &= ~DVD_INTTYPE_BRK;
    }
    
    if (cause & DVD_INTTYPE_TC)
    {
        if (ProcessNextCommand())
        {
            // already processed next command
            return;
        }
        // Fall through when NextCommand == -1
    }
    else
    {
        // When other interrupt occurred
        CommandList[0].command = -1;
        NextCommandNumber = 0;
    }

    // Create new context on stack
    OSClearContext(&exceptionContext);
    OSSetCurrentContext(&exceptionContext);

    if (cause)
    {
        // We can't just clear callback after callback is called.
        // if a dvd function is called in the callback, clearing
        // callback after callback causes a problem.
        cb = Callback;
        Callback = NULL;
        
        if (cb)
            (*cb)(cause);

        // even when brk int doesn't occur, the first interrupt
        // after break is asserted should handle break int.
        Breaking = FALSE;
    }
    OSClearContext(&exceptionContext);    
    OSSetCurrentContext(context);
}

static void AlarmHandler(OSAlarm* alarm, OSContext* context)
{
#pragma unused(alarm, context)
    BOOL            processed;

    processed = ProcessNextCommand();
    
    ASSERT(processed);
}

static void AlarmHandlerForTimeout(OSAlarm* alarm, OSContext* context)
{
#pragma unused(alarm)
    DVDLowCallback  cb;
    OSContext       exceptionContext;

#if 0
#ifdef LOGGING
    StampEnd();
#endif
#endif

    __OSMaskInterrupts(OS_INTERRUPTMASK_PI_DI);

    // Create new context on stack
    OSClearContext(&exceptionContext);
    OSSetCurrentContext(&exceptionContext);

    // We can't just clear callback after callback is called.
    // if a dvd function is called in the callback, clearing
    // callback after callback causes a problem.
    cb = Callback;
    Callback = NULL;
    
    if (cb)
        (*cb)(DVD_INTTYPE_TIMEOUT);

    OSClearContext(&exceptionContext);    
    OSSetCurrentContext(context);
}

static void SetTimeoutAlarm(OSTime timeout)
{
    OSCreateAlarm(&AlarmForTimeout);
    OSSetAlarm(&AlarmForTimeout, timeout, AlarmHandlerForTimeout);
}

static void Read(void* addr, u32 length, u32 offset, DVDLowCallback callback)
{
    Callback = callback;

#ifdef LOGGING
//    StampStart(offset, length);
    StampEvent(8, offset, length);
#endif

#ifdef NO_BREAK
    StopAtNextInt = FALSE;
#endif

    LastCommandWasRead = TRUE;
    LastReadIssued = __OSGetSystemTime();
    
//    OSReport("read: addr 0x%08x, len %d, offset 0x%08x\n", addr, length,
//             offset);

    __DIRegs[DI_CMDBUF0_IDX] = DRIVE_CMD_READ;
    __DIRegs[DI_CMDBUF1_IDX] = offset >> 2;
    __DIRegs[DI_CMDBUF2_IDX] = length;
    __DIRegs[DI_MAR_IDX] =     (u32)addr;
    __DIRegs[DI_LENGTH_IDX] =  length;

    LastLength = length;

    // setting TSTART triggers DMA
    // DI_CR_RW_MASK: 0 means read, 1 means write
    __DIRegs[DI_CR_IDX] = DI_CR_DMA_MASK | DI_CR_TSTART_MASK;

    if (length > 0xa00000)
    {
        // can't occur; higher level always divide into 6Ms
        SetTimeoutAlarm(DVD_COMMAND_TIMEOUT_TICKS_FOR_LONGREAD);
    }
    else
    {
        SetTimeoutAlarm(DVD_COMMAND_TIMEOUT_TICKS);
    }
}

static BOOL AudioBufferOn(void)
{
    DVDDiskID*          id;
    
    id = DVDGetCurrentDiskID();

    if (id->streaming)
        return TRUE;
    else
        return FALSE;
}

//returns TRUE if cachehit, FALSE if cache miss
static BOOL HitCache(Packet* curr, Packet* prev)
{
    u32     blockNumOfPrevEnd;
    u32     blockNumOfCurrStart;
    u32     cacheBlockSize;
    
    blockNumOfPrevEnd = (prev->offset + prev->length - 1) / (32*1024);
    blockNumOfCurrStart = curr->offset / (32*1024);
    
//    OSReport("block num of prev end: 0x%08x, block num of curr start: 0x%08x\n", 
//             blockNumOfPrevEnd, blockNumOfCurrStart);

    cacheBlockSize = AudioBufferOn()? 5u : 15u;

//    OSReport("cacheblock size = %d\n", cacheBlockSize);

    if ( (blockNumOfCurrStart > blockNumOfPrevEnd - 2) ||
         (blockNumOfCurrStart < blockNumOfPrevEnd + cacheBlockSize + 3) )
        return TRUE;
    else
        return FALSE;
}


static void DoJustRead(void* addr, u32 length, u32 offset, DVDLowCallback callback)
{
    CommandList[0].command = -1;
    NextCommandNumber = 0;

    Read(addr, length, offset, callback);
}

static void SeekTwiceBeforeRead(void* addr, u32 length, u32 offset, DVDLowCallback callback)
{
    u32         offsetToSeek;

    if (RoundDown32KB(offset) == 0)
        offsetToSeek = 0;
    else
        offsetToSeek = RoundDown32KB(offset) + WorkAroundSeekLocation;

    CommandList[0].command = DVD_COMMAND_SEEK;
    CommandList[0].offset = offsetToSeek;
    CommandList[0].callback = callback;
    
    CommandList[1].command = DVD_COMMAND_READ;
    CommandList[1].address = addr;
    CommandList[1].length = length;
    CommandList[1].offset = offset;
    CommandList[1].callback = callback;

    CommandList[2].command = -1;
    NextCommandNumber = 0;

    DVDLowSeek(offsetToSeek, callback);
}

static void WaitBeforeRead(void* addr, u32 length, u32 offset,
                           DVDLowCallback callback, OSTime wait)
{
    CommandList[0].command = DVD_COMMAND_READ;
    CommandList[0].address = addr;
    CommandList[0].length = length;
    CommandList[0].offset = offset;
    CommandList[0].callback = callback;
            
    CommandList[1].command = -1;
    NextCommandNumber = 0;
                    
    OSCreateAlarm(&AlarmForWA);
    OSSetAlarm(&AlarmForWA, wait, AlarmHandler);
}

/*---------------------------------------------------------------------------*
  Name:         DVDLowRead

  Description:  low-level read function.

  Arguments:    addr        address to store the data
                length      transfer length
                offset      offset position from the beginning of the disk
                            drive
                callback    callback

  Returns:      always TRUE

  XXX Caller of this function must guarantee that interrupts are disabled.
 *---------------------------------------------------------------------------*/
__declspec(weak) BOOL DVDLowRead(void* addr, u32 length, u32 offset, DVDLowCallback callback)
{
    ASSERTMSG(OFFSET(addr,   DVD_ALIGN_ADDR) == 0,  DVDLOW_ERR_READ_INVADDR);
    ASSERTMSG(OFFSET(length, DVD_ALIGN_SIZE) == 0,  DVDLOW_ERR_READ_INVSIZE);
    ASSERTMSG(OFFSET(offset, DVD_ALIGN_OFFSET) == 0,DVDLOW_ERR_READ_INVOFFSET);

    ASSERTMSG(length != 0, "DVD read: 0 was specified to length of the read\n");

    // Need to initialize length first. If we don't do this here, a problem
    // can occur if seek before read hits an error. If seek before read hits
    // an error, the upper layer of device driver gets the length (which is 
    // 0 if there's any read command completed before) and thinks some amount
    // of data is already transferred
    __DIRegs[DI_LENGTH_IDX] = length;

    Curr.addr = addr;
    Curr.length = length;
    Curr.offset = offset;

    if (WorkAroundType == DVD_WATYPE_NONE)
    {
        DoJustRead(addr, length, offset, callback);
    }
    else if (WorkAroundType == DVD_WATYPE_COMBO1)
    {
        if (FirstRead)
            SeekTwiceBeforeRead(addr, length, offset, callback);
        else if (!HitCache(&Curr, &Prev))
        {
            // cache miss; safe
            DoJustRead(addr, length, offset, callback);
        }
        else
        {
            u32     blockNumOfPrevEnd;
            u32     blockNumOfCurrStart;
    
            blockNumOfPrevEnd = (Prev.offset + Prev.length - 1) / (32*1024);
            blockNumOfCurrStart = Curr.offset / (32*1024);

            if ( (blockNumOfPrevEnd == blockNumOfCurrStart) ||
                  (blockNumOfPrevEnd + 1 == blockNumOfCurrStart) )
            {
                OSTime diff = __OSGetSystemTime() - LastReadFinished;

                if (diff > WORKAROUND_WAIT)
                {
                    // we still may hit the bug but maybe ratio is low.
                    // we don't do anything
                    DoJustRead(addr, length, offset, callback);
                }
                else
                {
                    WaitBeforeRead(addr, length, offset, callback,
                                   WORKAROUND_WAIT - diff + WAIT_MARGIN);
                }
            }
            else
            {
                SeekTwiceBeforeRead(addr, length, offset, callback);
            }
        }
    }
    else
    {
        ASSERT(FALSE);
    }
    
    return TRUE;
}

/*---------------------------------------------------------------------------*
  Name:         DVDLowSeek

  Description:  low-level seek function.

  Arguments:    offset      offset addr in disk
                callback    callback

  Returns:      always TRUE

  XXX Caller of this function must guarantee that interrupts are disabled.
 *---------------------------------------------------------------------------*/
__declspec(weak) BOOL DVDLowSeek(u32 offset, DVDLowCallback callback)
{
    ASSERTMSG(OFFSET(offset, DVD_ALIGN_OFFSET) == 0,DVDLOW_ERR_SEEK_INVOFFSET);

    Callback = callback;

#ifdef LOGGING
    StampEvent(10, offset, 0);
#endif

#ifdef NO_BREAK
    StopAtNextInt = FALSE;
#endif
    __DIRegs[DI_CMDBUF0_IDX] = DRIVE_CMD_SEEK;
    __DIRegs[DI_CMDBUF1_IDX] = offset >> 2;
    // don't have to fill CMDBUF2, MAR, LENGTH

    // setting TSTART triggers the command
    // IMM mode, direction: READ
    __DIRegs[DI_CR_IDX] = DI_CR_TSTART_MASK;

    // prepare for timeout
    SetTimeoutAlarm(DVD_COMMAND_TIMEOUT_TICKS);

    return TRUE;
}

/*---------------------------------------------------------------------------*
  Name:         DVDLowWaitCoverClose

  Description:  wait for cover close interrupt

  Arguments:    callback    callback

  Returns:      always TRUE

  XXX Caller of this function must guarantee that interrupts are disabled.
 *---------------------------------------------------------------------------*/
__declspec(weak) BOOL DVDLowWaitCoverClose(DVDLowCallback callback)
{
    Callback = callback;

    WaitingCoverClose = TRUE;
#ifdef NO_BREAK
    StopAtNextInt = FALSE;
#endif

    // enable cover int
    // writing 0 to cover int bit does nothing
    __DIRegs[DI_CVR_IDX] = DI_CVR_CVRINTMSK_MASK;

    return TRUE;
}

/*---------------------------------------------------------------------------*
  Name:         DVDLowReadDiskID

  Description:  low-level read diskID function

  Arguments:    diskID      address to store ID
                callback    callback

  Returns:      always TRUE

  XXX Caller of this function must guarantee that interrupts are disabled.
 *---------------------------------------------------------------------------*/
__declspec(weak) BOOL DVDLowReadDiskID(DVDDiskID* diskID, DVDLowCallback callback)
{
    ASSERTMSG(OFFSET(diskID, DVD_ALIGN_ADDR) == 0,  DVDLOW_ERR_READID_INVADDR);

    Callback = callback;
#ifdef NO_BREAK
    StopAtNextInt = FALSE;
#endif

#ifdef LOGGING
    StampEvent(20, 0, 0);
#endif

    __DIRegs[DI_CMDBUF0_IDX] = DRIVE_CMD_READ | DRIVE_SUBCMD_ID;
    __DIRegs[DI_CMDBUF1_IDX] = DVDLAYOUT_ID_POSITION;
    __DIRegs[DI_CMDBUF2_IDX] = sizeof(DVDDiskID);
    __DIRegs[DI_MAR_IDX] =     (u32)diskID;
    __DIRegs[DI_LENGTH_IDX] =  sizeof(DVDDiskID);

    // setting TSTART triggers DMA
    // DI_CR_RW_MASK: 0 means read, 1 means write
    __DIRegs[DI_CR_IDX] = DI_CR_DMA_MASK | DI_CR_TSTART_MASK;

    // prepare for timeout
    SetTimeoutAlarm(DVD_COMMAND_TIMEOUT_TICKS);

    return TRUE;
}

/*---------------------------------------------------------------------------*
  Name:         DVDLowStopMotor

  Description:  low-level stop motor function

  Arguments:    callback    callback

  Returns:      always TRUE

  XXX Caller of this function must guarantee that interrupts are disabled.
 *---------------------------------------------------------------------------*/
__declspec(weak) BOOL DVDLowStopMotor(DVDLowCallback callback)
{
    Callback = callback;
#ifdef NO_BREAK
    StopAtNextInt = FALSE;
#endif

#ifdef LOGGING
    StampEvent(21, 0, 0);
#endif


    __DIRegs[DI_CMDBUF0_IDX] = DRIVE_CMD_STOPMOTOR;
    // don't have to fill CMDBUF1, CMDBUF2, MAR and LENGTH

    // setting TSTART triggers the command
    // IMM mode, direction: READ
    __DIRegs[DI_CR_IDX] = DI_CR_TSTART_MASK;

    // prepare for timeout
    SetTimeoutAlarm(DVD_COMMAND_TIMEOUT_TICKS);

    return TRUE;
}

/*---------------------------------------------------------------------------*
  Name:         DVDLowRequestError

  Description:  Request error code that drive holds

  Arguments:    callback    callback

  Returns:      always TRUE

  The result will be return to the DI immediate buffer register.
  XXX Caller of this function must guarantee that interrupts are disabled.
 *---------------------------------------------------------------------------*/
__declspec(weak) BOOL DVDLowRequestError(DVDLowCallback callback)
{
    Callback = callback;
#ifdef NO_BREAK
    StopAtNextInt = FALSE;
#endif

#ifdef LOGGING
    StampEvent(22, 0, 0);
#endif

    __DIRegs[DI_CMDBUF0_IDX] = DRIVE_CMD_REQUESTERROR;
    // don't have to fill CMDBUF1, CMDBUF2, MAR and LENGTH
    // specify IMMEDIATE mode

    // setting TSTART triggers the command
    // DI_CR_RW_MASK: 0 means read, 1 means write
    __DIRegs[DI_CR_IDX] = DI_CR_TSTART_MASK;

    // prepare for timeout
    SetTimeoutAlarm(DVD_COMMAND_TIMEOUT_TICKS);

    return TRUE;
}

/*---------------------------------------------------------------------------*
  Name:         DVDLowInquiry

  Description:  Issue inquiry command to the drive

  Arguments:    info        address to store drive info
                callback    callback

  Returns:      always TRUE

  XXX Caller of this function must guarantee that interrupts are disabled.
 *---------------------------------------------------------------------------*/
__declspec(weak) BOOL DVDLowInquiry(DVDDriveInfo* info, DVDLowCallback callback)
{
    Callback = callback;
#ifdef NO_BREAK
    StopAtNextInt = FALSE;
#endif

#ifdef LOGGING
    StampEvent(23, 0, 0);
#endif

    __DIRegs[DI_CMDBUF0_IDX] = DRIVE_CMD_INQUIRY;
    __DIRegs[DI_CMDBUF2_IDX] = sizeof(DVDDriveInfo);
    __DIRegs[DI_MAR_IDX] =     (u32)info;
    __DIRegs[DI_LENGTH_IDX] =  sizeof(DVDDriveInfo);
    
    // don't have to fill CMDBUF1
    // specify DMA mode

    // setting TSTART triggers the command
    // DI_CR_RW_MASK: 0 means read, 1 means write
    // DMA mode, direction: READ
    __DIRegs[DI_CR_IDX] = DI_CR_DMA_MASK | DI_CR_TSTART_MASK;

    // prepare for timeout
    SetTimeoutAlarm(DVD_COMMAND_TIMEOUT_TICKS);

    return TRUE;
}

/*---------------------------------------------------------------------------*
  Name:         DVDLowAudioStream

  Description:  Issue audio streaming command to the drive

  Arguments:    subcmd          specify 'entry' or 'cancel'
                callback        callback

  Returns:      always TRUE

  The result will be return to the DI immediate buffer register.(filled with
  0's, though)
  XXX Caller of this function must guarantee that interrupts are disabled.
 *---------------------------------------------------------------------------*/
__declspec(weak) BOOL DVDLowAudioStream(u32 subcmd, u32 length, u32 offset, DVDLowCallback callback)
{
    Callback = callback;
#ifdef NO_BREAK
    StopAtNextInt = FALSE;
#endif

#ifdef LOGGING
    StampEvent(24, offset, length);
#endif

    __DIRegs[DI_CMDBUF0_IDX] = DRIVE_CMD_AUDIOSTREAMING | subcmd;
    __DIRegs[DI_CMDBUF1_IDX] = (u32)offset >> 2;
    __DIRegs[DI_CMDBUF2_IDX] = length;

    // don't have to fill MAR and LENGTH
    // specify IMMEDIATE mode

    // setting TSTART triggers the command
    // DI_CR_RW_MASK: 0 means read, 1 means write
    // IMM mode, direction: READ
    __DIRegs[DI_CR_IDX] = DI_CR_TSTART_MASK;

    // prepare for timeout
    SetTimeoutAlarm(DVD_COMMAND_TIMEOUT_TICKS);

    return TRUE;
}

/*---------------------------------------------------------------------------*
  Name:         DVDLowRequestAudioStatus

  Description:  Issue Get Audio status command to the drive

  Arguments:    subcmd          subcommand (what kind of information to get)
                callback        callback

  Returns:      always TRUE

  The result will be return to the DI immediate buffer register.
  XXX Caller of this function must guarantee that interrupts are disabled.
 *---------------------------------------------------------------------------*/
__declspec(weak) BOOL DVDLowRequestAudioStatus ( u32 subcmd, DVDLowCallback callback )
{
    Callback = callback;
#ifdef NO_BREAK
    StopAtNextInt = FALSE;
#endif

#ifdef LOGGING
    StampEvent(12, 0, 0);
#endif

    __DIRegs[DI_CMDBUF0_IDX] = DRIVE_CMD_REQUESTAUDIOSTATUS | subcmd;
    // don't have to fill CMDBUF1, CMDBUF2, MAR and LENGTH
    // specify IMMEDIATE mode

    // setting TSTART triggers the command
    // DI_CR_RW_MASK: 0 means read, 1 means write
    // IMM mode, direction: READ
    __DIRegs[DI_CR_IDX] = DI_CR_TSTART_MASK;

    // prepare for timeout
    SetTimeoutAlarm(DVD_COMMAND_TIMEOUT_TICKS);

    return TRUE;
}

/*---------------------------------------------------------------------------*
  Name:         DVDLowAudioBufferConfig

  Description:  Issue Audio buffer config command to the drive

  Arguments:    enable          FALSE to disable, TRUE to enable
                size            buffer size (higher 4 bits are used as "how
                                many empty buffers trigger audio buffer fill?"
                                0/1 --- 1 block, 2 --- 2 blocks.
                callback        callback

  Returns:      always TRUE

  The result will be return to the DI immediate buffer register.
  XXX Caller of this function must guarantee that interrupts are disabled.
 *---------------------------------------------------------------------------*/
__declspec(weak) BOOL DVDLowAudioBufferConfig(BOOL enable, u32 size, DVDLowCallback callback)
{
#ifdef _DEBUG
    u32             bufSize;
    u32             trigger;
#endif

    Callback = callback;
#ifdef NO_BREAK
    StopAtNextInt = FALSE;
#endif

#ifdef _DEBUG
    bufSize = size & 0xf;
    trigger = (size & 0xf0) >> 4;
    
    ASSERT(bufSize < 16);
    ASSERT(trigger <= 2);
#endif

#ifdef LOGGING
    StampEvent(25, size, 0);
#endif

    __DIRegs[DI_CMDBUF0_IDX] = DRIVE_CMD_AUDIOBUFFERCONFIG | 
        (enable? DRIVE_SUBCMD_ON : DRIVE_SUBCMD_OFF) | size;
    // don't have to fill CMDBUF1, CMDBUF2, MAR and LENGTH
    // specify IMMEDIATE mode

    // setting TSTART triggers the command
    // DI_CR_RW_MASK: 0 means read, 1 means write
    // IMM mode, direction: READ
    __DIRegs[DI_CR_IDX] = DI_CR_TSTART_MASK;

    // prepare for timeout
    SetTimeoutAlarm(DVD_COMMAND_TIMEOUT_TICKS);

    return TRUE;
}


/*---------------------------------------------------------------------------*
  Name:         DVDLowReset

  Description:  Issue reset to the drive

  Arguments:    None

  Returns:      always TRUE

  XXX Caller of this function must guarantee that interrupts are disabled.
 *---------------------------------------------------------------------------*/
__declspec(weak) void DVDLowReset(void)
{
    u32      reg;
    OSTime   resetStart;

    // enable cover int to process a phony cover int
    __DIRegs[DI_CVR_IDX] = DI_CVR_CVRINTMSK_MASK;

    reg = __PIRegs[PI_REG_CONFIG/4];
    __PIRegs[PI_REG_CONFIG/4] =
        (reg & ~PI_CONFIG_REG_DIRSTB_MASK) | PI_CONFIG_REG_SYSRSTB_MASK;

    resetStart = __OSGetSystemTime();
    while(__OSGetSystemTime() - resetStart < DVD_RESET_TICKS)
        ;

    // the contents of the register should not be changed
    __PIRegs[PI_REG_CONFIG/4] =
        reg | PI_CONFIG_REG_DIRSTB_MASK | PI_CONFIG_REG_SYSRSTB_MASK;

    ResetOccurred = 1;
    LastResetEnd = __OSGetSystemTime();
}


/*---------------------------------------------------------------------------*
  Name:         DVDLowSetResetCoverCallback

  Description:  Set "reset cover callback", which fires when cover is closed
                after reset is deasserted.

  Arguments:    callback            callback to set

  Returns:      old callback
 *---------------------------------------------------------------------------*/
__declspec(weak) DVDLowCallback DVDLowSetResetCoverCallback(DVDLowCallback callback)
{
    DVDLowCallback          old;
    BOOL                    enabled;
    
    enabled = OSDisableInterrupts();

    old = ResetCoverCallback;
    ResetCoverCallback = callback;
    
    OSRestoreInterrupts(enabled);

    return old;
}

static void DoBreak(void)
{
    u32             statusReg;

#ifdef LOGGING
    StampEvent(9, 0, 0);
#endif

    statusReg = __DIRegs[DI_SR_IDX];
    statusReg |= DI_SR_BRK_MASK | DI_SR_BRKINT_MASK;
    __DIRegs[DI_SR_IDX] = statusReg;
        
    Breaking = TRUE;
}

static void SetBreakAlarm(OSTime timeout);

static void AlarmHandlerForBreak(OSAlarm* alarm, OSContext* context)
{
#pragma unused (alarm, context)
#ifdef LOGGING
//    StampBreak();
#endif

    // if data transfer has already begun, it's safe to break.
    if (__DIRegs[DI_LENGTH_IDX] < LastLength)
    {
        DoBreak();
    }
    else
    {
        // wait more
        SetBreakAlarm(BREAK_WORKAROUND_POLLING_TIME);
    }
}

static void SetBreakAlarm(OSTime timeout)
{
    OSCreateAlarm(&AlarmForBreak);
    OSSetAlarm(&AlarmForBreak, timeout, AlarmHandlerForBreak);
}

/*---------------------------------------------------------------------------*
  Name:         DVDLowBreak

  Description:  Issue break

  Arguments:    None

  Returns:      Always TRUE

  XXX Caller of this function must guarantee that interrupts are disabled.
 *---------------------------------------------------------------------------*/
__declspec(weak) BOOL DVDLowBreak(void)
{
#ifdef NO_BREAK
#ifdef LOGGING
    StampEvent(9, 0, 0);
#endif
    StopAtNextInt = TRUE;
    Breaking = TRUE;
#else

#ifdef BREAK_WORKAROUND
    // if the data transfer has begun, it's safe to break.
    if (__DIRegs[DI_LENGTH_IDX] < LastLength)
    {
#ifdef LOGGING
//        StampBreak();
#endif

        DoBreak();
    }
    else
    {
        SetBreakAlarm(BREAK_WORKAROUND_POLLING_TIME);
    }
#else   // BREAK_WORKAROUND
#ifdef LOGGING
//    StampBreak();
#endif
    DoBreak();
#endif
#endif  // NO_BREAK

    return TRUE;
}


/*---------------------------------------------------------------------------*
  Name:         DVDLowClearCallback

  Description:  Clear callback routine

  Arguments:    None

  Returns:      None

  XXX Caller of this function must guarantee that interrupts are disabled.
 *---------------------------------------------------------------------------*/
__declspec(weak) DVDLowCallback DVDLowClearCallback(void)
{
    DVDLowCallback          old;

    // mask cover int
    __DIRegs[DI_CVR_IDX] = 0;
    WaitingCoverClose = FALSE;

    old = Callback;
    Callback = NULL;

    return old;
}


/*---------------------------------------------------------------------------*
  Name:         DVDLowGetCoverStatus

  Description:  Report cover status

  Arguments:    None

  Returns:      Cover status

  XXX Caller of this function must guarantee that interrupts are disabled.
 *---------------------------------------------------------------------------*/
__declspec(weak) u32 DVDLowGetCoverStatus(void)
{
    if (__OSGetSystemTime() - LastResetEnd < DVD_RESETCOVER_TIMELAG_TICKS2)
    {
        return DVD_COVER_UNKNOWN;
    }
    else if (DI_CVR_GET_CVR(__DIRegs[DI_CVR_IDX]))
    {
        return DVD_COVER_OPEN;
    }
    else
    {
        return DVD_COVER_CLOSED;
    }
}


/*---------------------------------------------------------------------------*
  Name:         __DVDLowSetWAType

  Description:  Set work around type

  Arguments:    type        workaround type
                seekLoc     seek location relative to the block that was read
                            last if type == DVD_WATYPE_SEEK_AFTER_READ.
                            seek location relative to the block that is going
                            to be read if type == DVD_WATYPE_SEEK_BEFORE_READ.
                            this value means nothing if type == NONE.

  Returns:      None.
 *---------------------------------------------------------------------------*/
__declspec(weak) void __DVDLowSetWAType(u32 type, s32 seekLoc)
{
    BOOL                    enabled;

    enabled = OSDisableInterrupts();

    ASSERT(type < DVD_WATYPE_MAX);
    
    WorkAroundType = type;
    WorkAroundSeekLocation = seekLoc;

    OSRestoreInterrupts(enabled);
}


/*---------------------------------------------------------------------------*
  Name:         __DVDLowTestAlarm

  Description:  Check if the specified alarm is used in this file or not

  Arguments:    alarm               OSAlarm structure to test

  Returns:      TRUE if the alarm is used in this file
 *---------------------------------------------------------------------------*/
BOOL __DVDLowTestAlarm(const OSAlarm* alarm)
{
    if (alarm == &AlarmForBreak)
        return TRUE;
    
    if (alarm == &AlarmForTimeout)
        return TRUE;
    
#ifndef NO_BREAK
    if (alarm == &AlarmForWA)
        return TRUE;
#endif

    return FALSE;
}


