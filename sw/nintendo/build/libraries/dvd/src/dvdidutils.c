/*---------------------------------------------------------------------------*
  Project:  Dolphin DVD id utilities
  File:     dvdidutils.c

  Copyright 2001 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: dvdidutils.c,v $
  Revision 1.1.1.1  2004/01/03 01:02:28  paulm
  Gamecube library source from Nintendo

    
    4     8/05/02 16:19 Shiki
    Added const keywords to relevant function prototypes.

    3     02/02/07 16:02 Hashida
    Change the return value of DVDGenerateDiskID from void to DVDDiskID*

    2     02/02/07 15:56 Hashida
    fixed a bug that mistakenly asserts in debug lib.

    1     1/02/02 5:05p Hashida
    Multi disk support.

  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#include <dolphin/os.h>
#include <dolphin/dvd.h>

#include <string.h>

#define IS_BCD(x)               (((x) / 16 < 10) && ((x) % 16 < 10))

static u32 strnlen(const char *str, u32 maxlen)
{
    u32             i;

    for (i = 0; i < maxlen; i++)
    {
        if (*str++ == '\0')
        {
            return i;
        }
    }

    return maxlen;
}

BOOL DVDCompareDiskID(const DVDDiskID* id1, const DVDDiskID* id2)
{
#ifdef _DEBUG
    const char*     game1;
    const char*     game2;
    const char*     company1;
    const char*     company2;
    u8              diskNum1, diskNum2;
    u8              version1, version2;
    u32             length;
#endif

    ASSERTMSG(id1, "DVDCompareDiskID(): Specified id1 is NULL\n");
    ASSERTMSG(id2, "DVDCompareDiskID(): Specified id2 is NULL\n");

#ifdef _DEBUG
    game1 = &id1->gameName[0];
    game2 = &id2->gameName[0];
    company1 = &id1->company[0];
    company2 = &id2->company[0];
    diskNum1 = id1->diskNumber;
    diskNum2 = id2->diskNumber;
    version1 = id1->gameVersion;
    version2 = id2->gameVersion;

    length   = strnlen(game1, 4);
    ASSERTMSG((length == 0) || (length == 4), "DVDCompareDiskID(): Specified game name for id1 is neither NULL nor 4 character long\n");
    ASSERTMSG(company1, "DVDCompareDiskID(): Specified company name for id1 is NULL\n");
    ASSERTMSG(company1[1] != 0, "DVDCompareDiskID(): Specified company name for id1 is not 2 character long\n");
    ASSERTMSG((diskNum1 == 0xff) || IS_BCD(diskNum1), "DVDCompareDiskID(): Specified disk number for id1 is neither 0xff nor a BCD number");
    ASSERTMSG((version1 == 0xff) || IS_BCD(version1), "DVDCompareDiskID(): Specified version number for id1 is neither 0xff nor a BCD number");

    length   = strnlen(game2, 4);
    ASSERTMSG((length == 0) || (length == 4), "DVDCompareDiskID(): Specified game name for id2 is neither NULL nor 4 character long\n");
    ASSERTMSG(company2, "DVDCompareDiskID(): Specified company name for id2 is NULL\n");
    ASSERTMSG(company2[1] != 0, "DVDCompareDiskID(): Specified company name for id2 is not 2 character long\n");
    ASSERTMSG((diskNum2 == 0xff) || IS_BCD(diskNum2), "DVDCompareDiskID(): Specified disk number for id2 is neither 0xff nor a BCD number");
    ASSERTMSG((version2 == 0xff) || IS_BCD(version2), "DVDCompareDiskID(): Specified version number for id2 is neither 0xff nor a BCD number");
#endif

    if (id1->gameName[0] && id2->gameName[0] &&
        strncmp(&id1->gameName[0], &id2->gameName[0], 4))
    {
        return FALSE;
    }

    if (!id1->company[0] || !id2->company[0] ||
        strncmp(&id1->company[0], &id2->company[0], 2))
    {
        return FALSE;
    }

    if (id1->diskNumber != 0xff && id2->diskNumber != 0xff &&
        id1->diskNumber != id2->diskNumber)
    {
        return FALSE;
    }

    if (id1->gameVersion != 0xff && id2->gameVersion != 0xff &&
        id1->gameVersion != id2->gameVersion)
    {
        return FALSE;
    }

    return TRUE;
}


DVDDiskID* DVDGenerateDiskID(DVDDiskID* id, const char* game, const char* company,
                             u8 diskNum, u8 version)
{
    ASSERTMSG(id, "DVDGenerateDiskID(): Specified id is NULL\n");
    ASSERTMSG((game == NULL) || (strlen(game) == 4), "DVDGenerateDiskID(): Specified game name is neither NULL nor 4 character long\n");
    ASSERTMSG(company, "DVDGenerateDiskID(): Specified company name is NULL\n");
    ASSERTMSG(strlen(company) == 2, "DVDGenerateDiskID(): Specified company name is not 2 character long\n");
    ASSERTMSG((diskNum == 0xff) || IS_BCD(diskNum), "DVDGenerateDiskID(): Specified disk number is neither 0xff nor a BCD number");
    ASSERTMSG((version == 0xff) || IS_BCD(version), "DVDGenerateDiskID(): Specified version number is neither 0xff nor a BCD number");

    memset(id, 0, sizeof(DVDDiskID));

    if (game != NULL)
    {
        strncpy(&id->gameName[0], game, 4);
    }

    if (company != NULL)
    {
        strncpy(&id->company[0], company, 2);
    }

    id->diskNumber = diskNum;
    id->gameVersion = version;

    return id;
}

