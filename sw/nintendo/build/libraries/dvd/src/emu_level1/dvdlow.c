/*---------------------------------------------------------------------------*
  Project:  Dolphin DVD low-level functions
  File:     dvdlow.c

  Copyright 1998, 1999 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: dvdlow.c,v $
  Revision 1.1.1.1  2004/01/03 01:02:28  paulm
  Gamecube library source from Nintendo

    
    8     7/27/99 3:15p Hashida
    Removed level2 codes
    
    6     7/27/99 2:49p Hashida
    Added some codes for level2 (timer-based) emulation
    
    5     7/23/99 5:01p Hashida
    Changed not to use __diskImage symbol but a fixed address for disk
    image.
    Changed to use uncached address instead of using cached address to copy
    disk data to ram.
    
    4     7/20/99 10:30p Hashida
    Added DVDLowSeek
    
    3     7/19/99 4:31p Hashida
    Changed low level callback
    
    2     7/10/99 3:02a Hashida
    added read disk id
    
    1     7/08/99 5:06p Hashida
    initial revision

  $NoKeywords: $
 *---------------------------------------------------------------------------*/

// Dolphin header files
#include <dolphin/os.h>
#include <dolphin/dvd.h>
#include <secure/dvdlow.h>
#include <secure/dvdlayout.h>
#include "DVDLowAssert.h"


// temporarily, we use the following macro. Probably, Flipper can accept
// cached or uncached address for the registers that holds RAM address.
#define toPhysical(vaddr)       (u32)((u32)(vaddr) & ~0xc0000000)

/*---------------------------------------------------------------------------*
  Name:         DVDLowRead

  Description:  low-level read function.

  Arguments:    px          pointer to movement data in terms of x-axis
                py          pointer to movement data in terms of y-axis

  Returns:      None.
 *---------------------------------------------------------------------------*/
BOOL DVDLowRead(void* addr, u32 length, u32 offset, DVDLowCallback callback)
{
	u32*       src;
	u32*       dst;
	u32        i;
    BOOL       level;


    ASSERTMSG(OFFSET(addr,   DVD_ALIGN_ADDR) == 0,  DVDLOW_ERR_READ_INVADDR);
    ASSERTMSG(OFFSET(length, DVD_ALIGN_SIZE) == 0,  DVDLOW_ERR_READ_INVSIZE);
    ASSERTMSG(OFFSET(offset, DVD_ALIGN_OFFSET) == 0,DVDLOW_ERR_READ_INVOFFSET);


    // should be executed with interrupts disabled
    // disable int to allow reentrance
    level = OSDisableInterrupts();

    // use uncached addresses to copy the data so that it can emulate DMA
	src = (u32*)OSPhysicalToUncached((u32)DVDLOW_DISKIMAGE_START + offset);
	dst = (u32*)OSPhysicalToUncached(toPhysical(addr));
	
	for(i = 0; i < length; i += sizeof(u32))
	{
		*dst++ = *src++;
	}
	
	(*callback)(DVD_INTTYPE_TC);

    // restore int
    OSRestoreInterrupts(level);

	return TRUE;
}

/*---------------------------------------------------------------------------*
  Name:         DVDLowSeek

  Description:  low-level seek function.

  Arguments:    offset      offset addr in disk
                callback    callback

  Returns:      None.
 *---------------------------------------------------------------------------*/
BOOL DVDLowSeek(u32 offset, DVDLowCallback callback)
{
	(*callback)(DVD_INTTYPE_TC);

	return TRUE;
}

BOOL DVDLowWaitCoverClose(DVDLowCallback callback)
{
	(*callback)(DVD_INTTYPE_CVR);

	return TRUE;
}

BOOL DVDLowReadDiskID(DVDDiskID* diskID, DVDLowCallback callback)
{
	u32*       src;
	u32*       dst;
	u32        i;
    BOOL       level;


    ASSERTMSG(OFFSET(diskID, DVD_ALIGN_ADDR) == 0,  DVDLOW_ERR_READID_INVADDR);

    // should be executed with interrupts disabled
    // disable int to allow reentrance
    level = OSDisableInterrupts();

    // use uncached addresses to copy the data so that it can emulate DMA
	src = (u32*)OSPhysicalToUncached((u32)DVDLOW_DISKIMAGE_START
                                     + DVDLAYOUT_ID_POSITION);
	dst = (u32*)OSPhysicalToUncached(toPhysical(diskID));
	
	for(i = 0; i < OSRoundUp32B(sizeof(DVDDiskID)); i += sizeof(u32))
	{
		*dst++ = *src++;
	}
	
	(*callback)(DVD_INTTYPE_TC);

    // restore int
    OSRestoreInterrupts(level);

	return TRUE;
}

BOOL DVDLowStopMotor(DVDLowCallback callback)
{
	(*callback)(DVD_INTTYPE_TC);

	return TRUE;
}

BOOL DVDLowRequestError(DVDLowCallback callback)
{
	(*callback)(DVD_INTTYPE_TC);

	return TRUE;
}

BOOL DVDLowInquiry(DVDLowCallback callback)
{
	(*callback)(DVD_INTTYPE_TC);

	return TRUE;
}

void DVDLowReset(void)
{
}

