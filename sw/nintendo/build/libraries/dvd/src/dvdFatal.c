/*---------------------------------------------------------------------------*
  Project:  Dolphin DVD Fatal Error Message print routine
  File:     DVDFatal.c

  Copyright 2002 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: dvdFatal.c,v $
  Revision 1.1.1.1  2004/01/03 01:02:28  paulm
  Gamecube library source from Nintendo

    
    7     03/09/12 16:24 Hashida
    Modification for 2003 May SDK patch2.
    
    6     03/09/12 16:19 Hashida
    Modified (NINTENDO GAMECUBE -> Nintendo GameCube).
    Removed TM from European English sentense (US English has already done
    this).
    
    5     02/08/29 9:23 Hashida
    Removed TM from the English message.
    
    4     8/23/02 18:48 Shiki
    Modified message layouts to fit in the TV screen.

    3     8/22/02 18:35 Shiki
    Revised messages.

    2     8/21/02 13:48 Shiki
    Fixed a warning issued by CW 1.2.5.

    1     8/20/02 21:39 Shiki
    Initial check-in.

  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#include <dolphin/os.h>
#include <dolphin/gx.h>
#include <dolphin/vi.h>
#include <dolphin/dvd.h>

void __DVDPrintFatalMessage(void);

static void (*FatalFunc)(void) = NULL;

const char* Japanese = "\n\n\nエラーが発生しました。\n"
                 "\n"
                 "本体のパワーボタンを押して電源をOFFにし、\n"
                 "本体の取扱説明書の指示に従ってください。";

const char* English =  "\n\n\nAn error has occurred.\n"
                 "Turn the power off and refer to the\n"
                 "Nintendo GameCube Instruction Booklet\n"
                 "for further instructions.";

const char* const Europe[] =
{
    // English
    "\n\n\nAn error has occurred.\n"
    "Turn the power off and refer to the\n"
    "Nintendo GameCube Instruction Booklet\n"
    "for further instructions.",

    // German
    "\n\n\nEin Fehler ist aufgetreten.\n"
    "Bitte schalten Sie den Nintendo GameCube\n"
    "aus und lesen Sie die Bedienungsanleitung,\n"
    "um weitere Informationen zu erhalten.",

    // French
    "\n\n\nUne erreur est survenue.\n"
    "Eteignez la console et r馭駻ez-vous au\n"
    "manuel d'instructions Nintendo GameCube\n"
    "pour de plus amples informations.",

    // Spanish
    "\n\n\nSe ha producido un error.\n"
    "Apaga la consola y consulta el manual\n"
    "de instrucciones de Nintendo GameCube\n"
    "para obtener m疽 informaci�n.",

    // Italian
    "\n\n\nSi � verificato un errore.\n"
    "Spegni (OFF) e controlla il manuale\n"
    "d'istruzioni del Nintendo GameCube\n"
    "per ulteriori indicazioni.",

    // Dutch
    "\n\n\nEr is een fout opgetreden.\n"
    "Zet de Nintendo GameCube uit en\n"
    "raadpleeg de handleiding van de\n"
    "Nintendo GameCube voor nadere\n"
    "instructies."
};

static void ShowMessage(void)
{
    const char* message;
    GXColor bg = {   0,   0,   0,  0 };
    GXColor fg = { 255, 255, 255,  0 };

    if (VIGetTvFormat() == VI_NTSC)
    {
        if (OSGetFontEncode() == OS_FONT_ENCODE_SJIS)
        {
            message = Japanese;
        }
        else
        {
            message = English;
        }
    }
    else
    {
        message = Europe[OSGetLanguage()];
    }

    OSFatal(fg, bg, message);
    // NOT REACHED HERE
}

BOOL DVDSetAutoFatalMessaging(BOOL enable)
{
    BOOL enabled;
    BOOL prev;

    enabled = OSDisableInterrupts();
    prev = FatalFunc ? TRUE : FALSE;
    FatalFunc = enable ? ShowMessage : NULL;
    OSRestoreInterrupts(enabled);
    return prev;
}

void __DVDPrintFatalMessage(void)
{
    if (FatalFunc)
    {
        FatalFunc();
        // NOT REACHED HERE
    }
}
