/*---------------------------------------------------------------------------*
  Project:  Dolphin DVD functions for queuing
  File:     dvdqueue.c

  Copyright 2000 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: dvdqueue.c,v $
  Revision 1.1.1.1  2004/01/03 01:02:28  paulm
  Gamecube library source from Nintendo

    
    3     7/17/01 2:02a Hashida
    Fixed a bug.
    
    2     5/09/01 4:53p Hashida
    Added DVDDumpWaitingQueue
    
    1     10/09/00 6:54p Hashida
    Added prioritized queuing.
    
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

// Dolphin header files
#include <dolphin/os.h>
#include <dolphin/dvd.h>
#include <secure/dvdlow.h>
#include <secure/dvdcb.h>


#define NUM_QUEUES          4

typedef struct 
{
    DVDCommandBlock*        next;
    DVDCommandBlock*        prev;
} Queue;

static Queue                WaitingQueue[NUM_QUEUES];

void __DVDClearWaitingQueue(void)
{
    u32                 i;
    
    for (i = 0; i < NUM_QUEUES; i++)
    {
        DVDCommandBlock*        q;
        
        q = (DVDCommandBlock*)&(WaitingQueue[i]);
        q->next = q;
        q->prev = q;
    }
}

BOOL __DVDPushWaitingQueue(s32 prio, DVDCommandBlock* block)
{
    BOOL                enabled;
    DVDCommandBlock*    q;

    enabled = OSDisableInterrupts();

    q = (DVDCommandBlock*)&(WaitingQueue[prio]);

    q->prev->next = block;
    block->prev = q->prev;
    block->next = q;
    q->prev = block;

    OSRestoreInterrupts(enabled);

    return TRUE;
}

static DVDCommandBlock* PopWaitingQueuePrio(s32 prio)
{
    DVDCommandBlock*    tmp;
    BOOL                enabled;
    DVDCommandBlock*    q;
    
    enabled = OSDisableInterrupts();
    
    q = (DVDCommandBlock*)&(WaitingQueue[prio]);

    ASSERT(q->next != q);

    tmp = q->next;
    q->next = tmp->next;
    tmp->next->prev = q;

    OSRestoreInterrupts(enabled);

    tmp->next = (DVDCommandBlock*)NULL;
    tmp->prev = (DVDCommandBlock*)NULL;

    return tmp;
}

DVDCommandBlock* __DVDPopWaitingQueue(void)
{
    u32                 i;
    BOOL                enabled;
    DVDCommandBlock*    q;
    
    enabled = OSDisableInterrupts();

    for (i = 0; i < NUM_QUEUES; i++)
    {
        q = (DVDCommandBlock*)&(WaitingQueue[i]);
        if (q->next != q)
        {
            OSRestoreInterrupts(enabled);
            return PopWaitingQueuePrio((s32)i);
        }
    }

    OSRestoreInterrupts(enabled);

    return (DVDCommandBlock*)NULL;
}

/*
  returns TRUE if there's a command block in waiting queue
  FALSE if not
*/
BOOL __DVDCheckWaitingQueue(void)
{
    u32                 i;
    BOOL                enabled;
    DVDCommandBlock*    q;
    
    enabled = OSDisableInterrupts();

    for (i = 0; i < NUM_QUEUES; i++)
    {
        q = (DVDCommandBlock*)&(WaitingQueue[i]);
        if (q->next != q)
        {
            OSRestoreInterrupts(enabled);
            return TRUE;
        }
    }

    OSRestoreInterrupts(enabled);

    return FALSE;
}

BOOL __DVDDequeueWaitingQueue(DVDCommandBlock* block)
{
    BOOL                enabled;
    DVDCommandBlock*    prev;
    DVDCommandBlock*    next;
    
    enabled = OSDisableInterrupts();

    prev = block->prev;
    next = block->next;
    
    if ( (prev == (DVDCommandBlock*)NULL) ||
         (next == (DVDCommandBlock*)NULL) )
    {
        OSRestoreInterrupts(enabled);
        return FALSE;
    }

    prev->next = next;
    next->prev = prev;

    OSRestoreInterrupts(enabled);

    return TRUE;
}

/*---------------------------------------------------------------------------*
  Name:         __DVDIsBlockInWaitingQueue

  Description:  checks if the specified block is already used and in the queue.

  Arguments:    block       pointer to a command block to check

  Returns:      TRUE if the block is in the queue.
                FALSE if not.
 *---------------------------------------------------------------------------*/
BOOL __DVDIsBlockInWaitingQueue(DVDCommandBlock* block)
{
    u32                 i;
    DVDCommandBlock*    start;
    DVDCommandBlock*    q;
    
    for (i = 0; i < NUM_QUEUES; i++)
    {
        start = (DVDCommandBlock*)&(WaitingQueue[i]);

        if (start->next != start)
        {
            for (q = start->next; q != start; q = q->next)
            {
                if (q == block)
                    return TRUE;
            }
        }
    }

    return FALSE;
}

static char* CommandNames[] = {
    "",
    "READ",
    "SEEK",
    "CHANGE_DISK",
    "BSREAD",
    "READID",
    "INITSTREAM",
    "CANCELSTREAM",
    "STOP_STREAM_AT_END",
    "REQUEST_AUDIO_ERROR",
    "REQUEST_PLAY_ADDR",
    "REQUEST_START_ADDR",
    "REQUEST_LENGTH",
    "AUDIO_BUFFER_CONFIG",
    "INQUIRY",
    "BS_CHANGE_DISK",
};

/*---------------------------------------------------------------------------*
  Name:         DVDDumpWaitingQueue

  Description:  Dumps current waiting queue status

  Arguments:    None

  Returns:      None
 *---------------------------------------------------------------------------*/
void DVDDumpWaitingQueue(void)
{
    u32                 i;
    DVDCommandBlock*    start;
    DVDCommandBlock*    q;
    
    OSReport("==== DVD Waiting Queue Status ====\n");
    for (i = 0; i < NUM_QUEUES; i++)
    {
        OSReport("< Queue #%d > ", i);
        start = (DVDCommandBlock*)&(WaitingQueue[i]);

        if (start->next == start)
            OSReport("None\n");
        else
        {
            OSReport("\n");
            for (q = start->next; q != start; q = q->next)
            {
                OSReport("0x%08x: Command: %s ", q, CommandNames[q->command]);
                if (q->command == DVD_COMMAND_READ)
                {
                    OSReport("Disk offset: %d, Length: %d, Addr: 0x%08x\n",
                             q->offset, q->length, q->addr);
                }
                else
                {
                    OSReport("\n");
                }
            }
        }
    }
}
