/*---------------------------------------------------------------------------*
  Project:  DVD interrupt emulaiton
  File:     DVDFakeIntr.c

  Copyright 1998, 1999 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: DVDFakeIntr.c,v $
  Revision 1.1.1.1  2004/01/03 01:02:28  paulm
  Gamecube library source from Nintendo

    
    3     2/17/00 2:38p Shiki
    Fixed to work correctly with thread-ready OSAlarm API.

    2     2/16/00 7:19p Alligator
    TIAN: avoid calling OSLoadContext since someone might have disabled the
    scheduler.

    25    11/05/99 12:14p Hashida
    Changed offset for read as word address.

    24    9/10/99 4:59p Shiki
    Moved OSInitAlarm() from __DVDInitFakeInterrupt() to OSInit().

    23    9/09/99 2:15p Hashida
    Removed warnings.

    22    8/31/99 9:27p Hashida
    Used #define'd value for the start address of the Marlin's DMA buffer.

    21    8/31/99 10:54a Hashida
    Added Marlin support

    20    8/20/99 9:57p Hashida
    Added assert for check
    Fixed two bugs (missing parentheses, misuse of DI_CVR_CVR_MASK (should
    have been DI_CVR_CVRINT_MASK)).

    19    8/19/99 3:49p Shiki
    Modified to invoke __OSDispatchInterrupt() instead of DI interrupt
    handler directly.

    18    8/19/99 12:24a Hashida
    Changed to use real Flipper DI registers.
    Moved __DVD_reset_drive flag definition to os/src/PPCArthur/OSEmuDI.c

    17    8/17/99 5:15p Hashida
    Added assertion for the initial values for DI registers.

    16    8/17/99 1:56p Hashida
    Moved a code to initialize fake registers from DVDInit to
    DVDInitFakeInt.
    Changed to use ArtX's register definition.

    15    8/09/99 10:12a Hashida
    Added a feature to change disk.
    Added APIs for cover open/close and disk change.

    14    8/05/99 9:31p Hashida
    Changed to use boot.h

    13    8/05/99 1:11a Hashida
    Changed so that when request error is issued, the status number at that
    time, not at the time when error occurred, is returned.

    12    8/04/99 11:32a Hashida
    Changed a variable name (__reset_drive -> __DVD_reset_drive)

    11    7/29/99 6:17p Hashida
    Changed reset_issued variable to global

    10    7/29/99 4:42p Hashida
    Changed to use OSAlarm

    9     7/29/99 12:27p Hashida
    Added reset process.
    Fixed alignment bug for fake Flipper registers.
    Emulate drive's internal status.
    Request error, stop motor, seek and read check/change drive's status as
    expected and behave like the real drive.

    8     7/27/99 11:15p Hashida
    Added seek, stop motor and request error

    7     7/27/99 9:11p Hashida
    Moved the part that copies data from disk image to specified address
    from lowlevel read function.
    Fixed a bug that the size of Flipper register array was wrong.

    6     7/27/99 6:44p Hashida
    change to clear interrupts when written, not the next timer interrupt.

    5     7/27/99 2:50p Hashida
    Changed so that it works.
    First workable version

    4     99/07/19 5:06p Shiki
    Initial check-in.

  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#include <dolphin.h>
#include <dolphin/os/osalarm.h>
#include <private/flipper.h>
#include "secure/dvdlow.h"
#include "secure/boot.h"
#include "direg.h"


// this program assumes that the second disk is stored right after the
// first one and the size of the first one is 128K
#define ONE_DISK_IMAGE        (128 * 1024)

typedef struct
{
    volatile u32      inputStatusFlag;
    u32               statusFlag;
    u32               status;
    u32               error;
    u32               diskNumber;

} driveStatus_s;

static driveStatus_s driveStatus;

#define STATUS_COVER_OPEN        0x10
#define STATUS_DISK_CHANGED      0x08
#define STATUS_NO_DISK           0x04
#define STATUS_MOTOR_STOPPED     0x02
#define STATUS_ID_NOT_READ       0x01

#if 0
#define STATUS_INPUT_MASK        0x1c      // only cover open, disk change
                                           // and no disk can be specified
#else
#define STATUS_INPUT_MASK        0x14      // only cover open and no disk can be specified
#endif

// temporarily, we use the following macro. Probably, Flipper can accept
// cached or uncached address for the registers that holds RAM address.
#define toPhysical(vaddr)       (u32)((u32)(vaddr) & ~0xc0000000)

#define fallingE(old, new)      (((old) ^ (new)) & (old))

#define SR_ALLINT               (DI_SR_BRKINT_MASK |      \
                                 DI_SR_TCINT_MASK |       \
                                 DI_SR_DEINT_MASK)
#define SR_ALLINTMASK           (DI_SR_BRKINTMSK_MASK | \
                                 DI_SR_TCINTMSK_MASK |  \
                                 DI_SR_DEINTMSK_MASK)

// since first MARLIN has a bug, we have to divide read request to 32bytes,
// which causes the transfer very slow.
#define PERIOD  OSMillisecondsToTicks(10)

#ifdef MARLIN
extern u32   __DIEmuAddr;
extern u32   __DIEmuLength;
extern u32   __DIEmuCommandStart;
#endif

// prototypes
void __DVDDebugCover(BOOL open);
BOOL __DVDDebugPlaceDisk(s32 diskNumber);
void __DVDInitFakeInterrupt(void);


/*---------------------------------------------------------------------------*
  Name:         __DVDDebugCover

  Description:  close/open cover for debug purpose

  Arguments:    open         FALSE to close, TRUE to open

  Returns:      None.
 *---------------------------------------------------------------------------*/
void __DVDDebugCover(BOOL open)
{
    if (open)
    {
        driveStatus.inputStatusFlag |= STATUS_COVER_OPEN;
    }
    else
    {
        driveStatus.inputStatusFlag &= ~STATUS_COVER_OPEN;
    }
}

/*---------------------------------------------------------------------------*
  Name:         __DVDDebugPlaceDisk

  Description:  place a disk/remove a disk

  Arguments:    diskNumber      0 for disk0, 1 for disk1, ..., -1 for no disk

  Returns:      FALSE if cover is closed.
 *---------------------------------------------------------------------------*/
BOOL __DVDDebugPlaceDisk(s32 diskNumber)
{
    if (driveStatus.statusFlag & STATUS_COVER_OPEN)
    {
        if (diskNumber < 0)
        {
            driveStatus.inputStatusFlag |= STATUS_NO_DISK;
        }
        else
        {
            driveStatus.inputStatusFlag &= ~STATUS_NO_DISK;
            driveStatus.diskNumber = (u32)diskNumber;
        }

        return TRUE;
    }
    else
    {
        return FALSE;
    }
}


/*---------------------------------------------------------------------------*
  Name:         flag2mode

  Description:  change mode according as the flag in driveStatus

  Arguments:    None.

  Returns:      None.
 *---------------------------------------------------------------------------*/
static void flag2mode(void)
{
    if (driveStatus.statusFlag & STATUS_COVER_OPEN)
    {
        driveStatus.status = DVD_INTERNALERROR_STATUS_COVEROPEN;
    }
    else if (driveStatus.statusFlag & STATUS_DISK_CHANGED)
    {
        driveStatus.status = DVD_INTERNALERROR_STATUS_DISKCHANGE;
    }
    else if (driveStatus.statusFlag & STATUS_NO_DISK)
    {
        driveStatus.status = DVD_INTERNALERROR_STATUS_NODISK;
    }
    else if (driveStatus.statusFlag & STATUS_MOTOR_STOPPED)
    {
        driveStatus.status = DVD_INTERNALERROR_STATUS_MOTORSTOP;
    }
    else if (driveStatus.statusFlag & STATUS_ID_NOT_READ)
    {
        driveStatus.status = DVD_INTERNALERROR_STATUS_IDNOTREAD;
    }
    else
    {
        driveStatus.status = DVD_INTERNALERROR_STATUS_READY;
    }

}

/*---------------------------------------------------------------------------*
  Name:         reset

  Description:  do reset

  Arguments:    None.

  Returns:      None.
 *---------------------------------------------------------------------------*/
static void reset(void)
{
    // disk change, motor stop and no disk flags are reset here.
    // whether disk exists or not will be checked later in this function.
    // In reality, STATUS_ID_NOT_READ should be set, but we assume it's
    // cleared in bs...
    driveStatus.statusFlag &= ~(STATUS_DISK_CHANGED |
                                STATUS_MOTOR_STOPPED |
                                STATUS_NO_DISK);

    if (driveStatus.inputStatusFlag & STATUS_COVER_OPEN)
    {
        // stop motor if cover is opened
        // whether disk exists or not is not checked here.
        driveStatus.statusFlag |= STATUS_COVER_OPEN | STATUS_MOTOR_STOPPED;
        driveStatus.status = DVD_INTERNALERROR_STATUS_COVEROPEN;
    }
    else
    {
        // if cover is closed, drive check if disk exists
        if (driveStatus.inputStatusFlag & STATUS_NO_DISK)
        {
            // stop motor if no disk
            driveStatus.statusFlag |= STATUS_NO_DISK | STATUS_MOTOR_STOPPED;
            driveStatus.status = DVD_INTERNALERROR_STATUS_NODISK;
        }
    }

//    driveStatus.inputStatusFlag &= ~STATUS_DISK_CHANGED;
}

/*---------------------------------------------------------------------------*
  Name:         requestError

  Description:  do request error according as the contents of Flipper registers

  Arguments:    None.

  Returns:      None.
 *---------------------------------------------------------------------------*/
static void requestError(void)
{
    // This command should be IMM mode and the direction is READ
    ASSERT( (__DIEmuRegs[DI_CR_IDX] & DI_CR_DMA_MASK) == 0);
    ASSERT( (__DIEmuRegs[DI_CR_IDX] & DI_CR_RW_MASK) == 0);

    __DIEmuRegs[DI_IMMBUF_IDX] = driveStatus.status | driveStatus.error;

    driveStatus.error = DVD_INTERNALERROR_NO_ERROR;

    // the transfer succeeded
    __DIEmuRegs[DI_SR_IDX] |= DI_SR_TCINT_MASK;
}

/*---------------------------------------------------------------------------*
  Name:         stopMotor

  Description:  do stop motor according as the contents of Flipper registers

  Arguments:    None.

  Returns:      None.
 *---------------------------------------------------------------------------*/
static void stopMotor(void)
{
    // This command should be IMM mode and the direction is READ
    ASSERT( (__DIEmuRegs[DI_CR_IDX] & DI_CR_DMA_MASK) == 0);
    ASSERT( (__DIEmuRegs[DI_CR_IDX] & DI_CR_RW_MASK) == 0);

    // always succeeds
    __DIEmuRegs[DI_SR_IDX] |= DI_SR_TCINT_MASK;

    driveStatus.error = DVD_INTERNALERROR_NO_ERROR;

    // goto stop motor status
    driveStatus.statusFlag |= STATUS_MOTOR_STOPPED;
}

/*---------------------------------------------------------------------------*
  Name:         seek

  Description:  do seek according as the contents of Flipper registers

  Arguments:    None.

  Returns:      None.
 *---------------------------------------------------------------------------*/
static void seek(void)
{
    // This command should be IMM mode and the direction is READ
    ASSERT( (__DIEmuRegs[DI_CR_IDX] & DI_CR_DMA_MASK) == 0);
    ASSERT( (__DIEmuRegs[DI_CR_IDX] & DI_CR_RW_MASK) == 0);

    if (driveStatus.status == DVD_INTERNALERROR_STATUS_READY)
    {
        // succeeded
        __DIEmuRegs[DI_SR_IDX] |= DI_SR_TCINT_MASK;

        driveStatus.error = DVD_INTERNALERROR_NO_ERROR;
    }
    else
    {
        // failed
        __DIEmuRegs[DI_SR_IDX] |= DI_SR_DEINT_MASK;

        if (driveStatus.status == DVD_INTERNALERROR_STATUS_COVEROPEN)
        {
            driveStatus.error = DVD_INTERNALERROR_COVEROPEN_OR_NODISK;
        }
        else if (driveStatus.status == DVD_INTERNALERROR_STATUS_DISKCHANGE)
        {
            driveStatus.error = DVD_INTERNALERROR_COVER_CLOSED;
        }
        else if (driveStatus.status == DVD_INTERNALERROR_STATUS_NODISK)
        {
            driveStatus.error = DVD_INTERNALERROR_COVEROPEN_OR_NODISK;
        }
        else if (driveStatus.status == DVD_INTERNALERROR_STATUS_MOTORSTOP)
        {
            driveStatus.error = DVD_INTERNALERROR_MOTOR_STOPPED;
        }
        else if (driveStatus.status == DVD_INTERNALERROR_STATUS_IDNOTREAD)
        {
            driveStatus.error = DVD_INTERNALERROR_ID_NOT_READ;
        }

    }

}

/*---------------------------------------------------------------------------*
  Name:         read

  Description:  do read according as the contents of Flipper registers

  Arguments:    None.

  Returns:      None.
 *---------------------------------------------------------------------------*/
static void read(void)
{
    void*      addr;
    u32        length;
    u32        offset;
    u32*       src;
    u32*       dst;
    u32        i;

    // This command should be DMA mode and the direction is READ
    ASSERT( (__DIEmuRegs[DI_CR_IDX] & DI_CR_DMA_MASK) != 0);
    ASSERT( (__DIEmuRegs[DI_CR_IDX] & DI_CR_RW_MASK) == 0);

    if (driveStatus.status == DVD_INTERNALERROR_STATUS_READY)
    {
        addr = (void*)__DIEmuRegs[DI_MAR_IDX];
        length = __DIEmuRegs[DI_LENGTH_IDX];
        offset = __DIEmuRegs[DI_CMDBUF1_IDX] << 2; // word address is stored in
                                                   // the register

        // use uncached addresses to copy the data so that it can emulate DMA
        src = (u32*)OSPhysicalToUncached( toPhysical((void*)BOOT_DISK_IMAGE)
                                          + driveStatus.diskNumber * ONE_DISK_IMAGE
                                          + offset);
        dst = (u32*)OSPhysicalToUncached(toPhysical(addr));

        for(i = 0; i < length; i += sizeof(u32))
        {
            *dst++ = *src++;
        }

        // suppose the transfer succeeded
        __DIEmuRegs[DI_SR_IDX] |= DI_SR_TCINT_MASK;

        driveStatus.error = DVD_INTERNALERROR_NO_ERROR;
    }
    else
    {
        // transfer failed
        __DIEmuRegs[DI_SR_IDX] |= DI_SR_DEINT_MASK;

        if (driveStatus.status == DVD_INTERNALERROR_STATUS_COVEROPEN)
        {
            driveStatus.error = DVD_INTERNALERROR_COVEROPEN_OR_NODISK;
        }
        else if (driveStatus.status == DVD_INTERNALERROR_STATUS_DISKCHANGE)
        {
            driveStatus.error = DVD_INTERNALERROR_COVER_CLOSED;
        }
        else if (driveStatus.status == DVD_INTERNALERROR_STATUS_NODISK)
        {
            driveStatus.error = DVD_INTERNALERROR_COVEROPEN_OR_NODISK;
        }
        else if (driveStatus.status == DVD_INTERNALERROR_STATUS_MOTORSTOP)
        {
            driveStatus.error = DVD_INTERNALERROR_MOTOR_STOPPED;
        }
        else if (driveStatus.status == DVD_INTERNALERROR_STATUS_IDNOTREAD)
        {
            driveStatus.error = DVD_INTERNALERROR_ID_NOT_READ;
        }

    }
}


extern u32       __DVD_reset_issued;

/*---------------------------------------------------------------------------*
  Name:         DVDHardwareEmulator

  Description:  Emulator for DVD hardware registers

  Arguments:    context     interrupted context

  Returns:      None.
 *---------------------------------------------------------------------------*/
static void DVDHardwareEmulator(OSContext* context)
{
    u32       reg;
    u32       oldStatus;

    oldStatus = driveStatus.statusFlag;

    // cover status should be always updated first because in real drive, it's
    // updated in real time.
    // no disk won't updated until next reset
    driveStatus.statusFlag = (driveStatus.inputStatusFlag & STATUS_COVER_OPEN) |
                             (driveStatus.statusFlag & ~STATUS_COVER_OPEN);

    if (__DVD_reset_issued)
    {
        reset();
        __DVD_reset_issued = 0;
    }

    // reflect drive status to cover register
    if (driveStatus.statusFlag & STATUS_COVER_OPEN)
    {
        // cover open means motor is stopped
        driveStatus.statusFlag |= STATUS_MOTOR_STOPPED;
        __DIEmuRegs[DI_CVR_IDX] |= DI_CVR_CVR_MASK;
    }
    else
    {
        __DIEmuRegs[DI_CVR_IDX] &= ~DI_CVR_CVR_MASK;
    }

    if (fallingE(oldStatus, driveStatus.statusFlag) & STATUS_COVER_OPEN)
    {
        driveStatus.statusFlag |= STATUS_DISK_CHANGED;
    }

    // generate cover int if necessary
    if (driveStatus.statusFlag & STATUS_DISK_CHANGED)
    {
        // paranoia: probably, motor stop flag is already set when cover is open
        driveStatus.statusFlag |= STATUS_MOTOR_STOPPED;
        __DIEmuRegs[DI_CVR_IDX] |= DI_CVR_CVRINT_MASK;
    }

#ifndef MARLIN
    // generate "drive status" from flags.
    // drive status will be used in each functions (read, seek, ...)
    flag2mode();

    if (__DIEmuRegs[DI_CR_IDX] & DI_CR_TSTART_MASK)
    {
        switch(__DIEmuRegs[DI_CMDBUF0_IDX] & DRIVE_CMD_MASK )
        {
          case DRIVE_CMD_READ:
            read();
            break;

          case DRIVE_CMD_SEEK:
            seek();
            break;

          case DRIVE_CMD_STOPMOTOR:
            stopMotor();
            break;

          case DRIVE_CMD_REQUESTERROR:
            requestError();
            break;

          default:
            OSHalt("Undefined command\n");
            break;
        }

        // clear TSTART to indicate the transfer complete
        __DIEmuRegs[DI_CR_IDX] &= ~DI_CR_TSTART_MASK;
    }
#endif

#ifdef MARLIN
    if (__DIEmuCommandStart &&
        (! (__DIEmuRegs[DI_CR_IDX] & DI_CR_TSTART_MASK)) )
    {
        u32           i;
        u32*          src;
        u32*          dst;
        void*         addr;
        u32           length;

        addr = (void*)__DIEmuAddr;
        length = __DIEmuLength;

        src = (u32*)MARLIN_DVD_BUFFER;
        dst = (u32*)OSPhysicalToUncached(toPhysical(addr));

        for(i = 0; i < length; i += sizeof(u32))
        {
            *dst++ = *src++;
        }
        __DIEmuCommandStart = 0;
#endif

        // Set/clear PI_REG_INTSR[PI_INTSR_REG_DIINT_MASK]
        if ( (__DIEmuRegs[DI_SR_IDX]  & SR_ALLINT) ||
             (__DIEmuRegs[DI_CVR_IDX] & DI_CVR_CVRINT_MASK) )
        {
            __PIEmuRegs[PI_REG_INTSR / 4] |= PI_INTSR_REG_DIINT_MASK;
        }
        else
        {
            __PIEmuRegs[PI_REG_INTSR / 4] &= ~PI_INTSR_REG_DIINT_MASK;
        }

        reg = __DIEmuRegs[DI_SR_IDX];
        reg &= ( (reg << 1) & SR_ALLINT );
        if (reg)
        {
            OSEnableScheduler();    // DecrementerExceptionCallback() disabled scheduler
            __OSDispatchInterrupt(__OS_EXCEPTION_EXTERNAL_INTERRUPT, context);
        }
        else
        {
            reg = __DIEmuRegs[DI_CVR_IDX];
            reg &= ( (reg << 1) & DI_CVR_CVRINT_MASK );

            if (reg)
            {
                OSEnableScheduler();    // DecrementerExceptionCallback() disabled scheduler
                __OSDispatchInterrupt(__OS_EXCEPTION_EXTERNAL_INTERRUPT, context);
            }
        }
#ifdef MARLIN
    }
#endif

}


/*---------------------------------------------------------------------------*
  Name:         FakeInterruptHandler

  Description:  An alarm handler for external interrupt emulation.
                Invoked every 10 msec.

  Arguments:    alarm       expired alram
                context     interrupted context

  Returns:      None.
 *---------------------------------------------------------------------------*/
static void FakeInterruptHandler(OSAlarm* alarm, OSContext* context)
{
#pragma unused(alarm)

    DVDHardwareEmulator(context);
}

/*---------------------------------------------------------------------------*
  Name:         __DVDInitFakeInterrupt

  Description:  Install the fake interrupt handler

  Arguments:    None.

  Returns:      None.
 *---------------------------------------------------------------------------*/
void __DVDInitFakeInterrupt(void)
{
    static OSAlarm      alarm;
#ifdef MARLIN
    u8*                 ptr;
    u32                 i;
#endif

#ifdef MARLIN
    ptr = (u8*)__DIEmuRegs;
    for (i = 0; i < (DI_IMMBUF_IDX + 1) * 4; i++)
    {
        *ptr++ = 0;
    }
#endif

    // These are should be 0's
    ASSERT(__DIEmuRegs[DI_SR_IDX] == 0x0);
    ASSERT(__DIEmuRegs[DI_CVR_IDX] == 0x0);
    ASSERT(__DIEmuRegs[DI_CMDBUF0_IDX] == 0x0);
    ASSERT(__DIEmuRegs[DI_CMDBUF1_IDX] == 0x0);
    ASSERT(__DIEmuRegs[DI_CMDBUF2_IDX] == 0x0);
    ASSERT(__DIEmuRegs[DI_MAR_IDX] == 0x0);
    ASSERT(__DIEmuRegs[DI_LENGTH_IDX] == 0x0);
    ASSERT(__DIEmuRegs[DI_CR_IDX] == 0x0);
    ASSERT(__DIEmuRegs[DI_IMMBUF_IDX] == 0x0);

    OSSetPeriodicAlarm(&alarm, OSGetTime(), PERIOD, FakeInterruptHandler);
}
