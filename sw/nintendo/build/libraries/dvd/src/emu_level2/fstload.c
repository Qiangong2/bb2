/*---------------------------------------------------------------------------*
  Project: fst loader
  File:    fstload.c

  Copyright 1998, 1999 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: fstload.c,v $
  Revision 1.1.1.1  2004/01/03 01:02:28  paulm
  Gamecube library source from Nintendo

    
    3     4/12/00 7:34p Hashida
    Removed magic number printing.
    
    2     11/24/99 11:24a Hashida
    Added a code to print magic number in id.
    
    9     9/09/99 5:55p Hashida
    Removed warnings.
    
    8     8/26/99 10:32p Hashida
    Removed DCInvalidate because DVDRead* API does that.
    
    7     8/23/99 2:20p Tian
    Moved OSLoMem.h to private.
    
    6     8/05/99 11:32a Hashida
    Changed to fill FSTMaxLength in the Boot Info
    
    5     7/21/99 9:31p Hashida
    Removed FSTMaxSize
    
    4     7/20/99 2:36p Hashida
    Changed not to use DVD_RESULT_GOOD
    
    3     7/19/99 11:39p Hashida
    Added DVDReset()
    Changed from DVD_RESULT_DISK_CHANGED to DVD_RESULT_COVER_CLOSED
    
    2     7/19/99 4:28p Hashida
    Changed to use errorhandling layer.
    Changed to modify arenaHi appropriately.
    
    1     7/13/99 9:49p Hashida
    initial revision
    
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

/*
 * warning: ... this is a temporary file. FST should be loaded by apploader
 * eventually.
 */
#include <string.h>

#include <dolphin/os.h>
#include <dolphin/os/osbootinfo.h>
#include <private/oslomem.h>
#include <secure/dvdlow.h>
#include <secure/dvdlayout.h>
#include <secure/dvdcb.h>

// prototypes
void __fstLoad(void);


static u32  status;

enum
{
    STATUS_READING_ID,
    STATUS_READING_BB2,
    STATUS_READING_FST
};

static u8       bb2Buf[OSRoundUp32B(sizeof(DVDBB2)) + 31];
static DVDBB2*  bb2;
static DVDDiskID*
                idTmp;

static void cb(s32 result, DVDCommandBlock* block)
{
    if (result > 0)
    {
        switch(status)
        {
          case STATUS_READING_ID:
            // succeeded reading id
            status = STATUS_READING_BB2;
            DVDReadAbsAsyncForBS(block, bb2,
                                 (s32)OSRoundUp32B(sizeof(DVDBB2)),
                                 (s32)DVDLAYOUT_BB2_POSITION,
                                 cb);
            break;
          // case STATUS_READING_BB1: ...
          case STATUS_READING_BB2:
            status = STATUS_READING_FST;
            DVDReadAbsAsyncForBS(block, bb2->FSTAddress,
                                 (s32)OSRoundUp32B(bb2->FSTLength),
                                 (s32)bb2->FSTPosition,
                                 cb);
            break;
            
        }
        
    }
    else if (result == DVD_RESULT_FATAL_ERROR)
    {
    }
    else if (result == DVD_RESULT_COVER_CLOSED)
    {
        status = STATUS_READING_ID;
        DVDReset();
        DVDReadDiskID(block, idTmp, cb);
    }
}


void __fstLoad(void)
{
    OSBootInfo*    bootInfo;
    DVDDiskID*     id;
    u8             idTmpBuf[sizeof(DVDDiskID) + 31];
    s32            state;
    static DVDCommandBlock
                   block;
    void*          arenaHi;
    

    arenaHi = OSGetArenaHi();
    
    bootInfo = (OSBootInfo*)OSPhysicalToCached(OS_BOOTINFO_ADDR);

    // prepare the buffer to read id temporarily, because bootInfo->DVDDiskID
    // is not aligned by 32bytes.
    idTmp = (DVDDiskID*)OSRoundUp32B(idTmpBuf);
    bb2 = (DVDBB2*)OSRoundUp32B(bb2Buf);

    DVDReset();
    DVDReadDiskID(&block, idTmp, cb);

    while(1)
    {
        state = DVDGetDriveStatus();
        if (state == DVD_STATE_END)
        {
            break;
        }
        
        switch (state)
        {
          case DVD_STATE_BUSY:
            break;
          case DVD_STATE_WAITING:
            break;
          case DVD_STATE_COVER_CLOSED:
            break;
          case DVD_STATE_NO_DISK:
            break;
          case DVD_STATE_COVER_OPEN:
            break;
          case DVD_STATE_FATAL_ERROR:
            break;
          case DVD_STATE_MOTOR_STOPPED:
            break;
        }
    }

    // fill bootInfo
    bootInfo->FSTLocation  = bb2->FSTAddress;
    bootInfo->FSTMaxLength = bb2->FSTMaxLength;

    id = &(bootInfo->DVDDiskID);
    memcpy((void*)id, (void*)idTmp, sizeof(DVDDiskID));
    
    OSReport("\n");
    OSReport("  Game Name ... %c%c%c%c\n", id->gameName[0], id->gameName[1],
             id->gameName[2], id->gameName[3]);
    OSReport("  Company ..... %c%c\n", id->company[0], id->company[1]);
    OSReport("  Disk # ...... %d\n", id->diskNumber);
    OSReport("  Game ver .... %d\n", id->gameVersion);
    OSReport("  Streaming ... %s\n", (id->streaming == 0)? "OFF":"ON");
    
    OSReport("\n");
    
    OSSetArenaHi(bb2->FSTAddress);
}
