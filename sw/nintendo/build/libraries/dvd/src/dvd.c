/*---------------------------------------------------------------------------*
  Project:  Dolphin DVD middle-level functions
  File:     dvd.c

  Copyright 1998 - 2002 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: dvd.c,v $
  Revision 1.1.1.1  2004/01/03 01:02:28  paulm
  Gamecube library source from Nintendo

    
    54    03/07/23 10:53 Hashida
    Modification for 2003May SDK patch1.
    
    53    03/06/18 14:20 Hashida
    Added __DVDTestAlarm().
    
    52    02/10/18 23:01 Hashida
    Fixed a bug that DVDCheckDisk returns TRUE after canceling a transfer
    that is in an error state.
    
    51    02/10/18 21:31 Hashida
    Fixed a bug that the command block has not been freed by the time
    callback is called when the request in an error state is canceled.
    
    50    02/10/17 22:11 Hashida
    Fixed a bug that resuming after WRONGDISK cancel, the state of the
    command block became BUSY by mistake. This was the cause of the
    OSResetSystem(RESTART) hangup problem when it's called while
    WRONG_DISK. The bug is also fixed.
    
    49    8/20/02 21:40 Shiki
    Added support for DVDSetAutoFatalMessaging().

    48    02/08/19 21:06 Hashida
    Allowed 0 length read.
    Removed "app from bootrom" kind of message in DVDInit().

    47    8/07/02 21:45 Shiki
    Defined DOLPHIN_LIB_VERSION() and registered the library with
    OSRegisterVersion().

    46    8/05/02 16:19 Shiki
    Added const keywords to relevant function prototypes.

    45    02/02/28 15:41 Hashida
    Changed to support DVDCancel for ChangeDisk perfectly. Because of this
    change, the behavior of DVDRead -> wrong disk error -> cancel ->
    DVDRead is changed. The second read resets the drive and checks the id
    of the disk while the previous version didn't check but wait for cover
    close.

    44    1/02/02 5:03p Hashida
    Added gcam support.
    Fixed a bug that audio buf config was not issued when disk is changed.


    43    7/18/01 10:31p Hashida
    Fixed a bug that the driver didn't set fatal error when timeout error
    occurred.
    Fixed a bug in DVDCancelAsync that inproperly removed CancelCallback.

    42    7/13/01 4:09a Hashida
    Added __DVDPrepareResetAsync

    41    6/29/01 11:43a Hashida
    Added DVDCheckDisk().

    40    6/27/01 9:52p Hashida
    Fixed a problem that the error that should be treated as a retry will
    become fatal if it's occurred on readdisdid.

    39    6/18/01 2:16p Hashida
    Added timeout error.
    Changed so that when a fatal error occurred, stop motor.

    38    6/16/01 5:55a Hashida
    Fixed a DVDCancel bug.
    Changed to return audio streaming error status as it is.

    37    6/15/01 9:00p Hashida
    Fixed variety of bugs.

    36    6/12/01 3:55p Hashida
    Added reset workaround.

    35    6/11/01 3:40p Hashida
    Added MEI bug workaround.

    34    5/19/01 4:55p Hashida
    Fixed a bug that the check "if the command block is already in use"
    added in the previous change was too strict.

    33    5/09/01 4:54p Hashida
    Added a check for 0 or negative length read request.
    Added a check for command block that is already in use.

    32    4/19/01 5:40p Hashida
    Added DVDGetCurrentDiskID

    31    4/06/01 6:01p Hashida
    Turned off fake no disk workaround for a while.
    It'll be implemented next week.

    30    4/03/01 10:29p Hashida
    Modified spec for DVDCancel

    29    4/02/01 4:20p Hashida
    Changed to make the workaround for the fake no disk problem work for
    read disk id command.
    Added a RESET_ONCE_FOR_POWERON to easily select to/not to reset twice
    on power up.

    28    4/02/01 3:10p Hashida
    Added a workaround for the fake-nodisk problem.

    27    4/02/01 1:58p Hashida
    Changed DVDResume so that it can be resumed even when DVDPause is not
    fully finished.

    26    3/22/01 6:09p Hashida
    Changed to have callback for DVDCancelAsync return 0 for result when
    succeeded.
    Fixed a bug for DVDCancel.
    Added new functions DVDCancelAll and DVDCancelAllAsync.

    25    3/22/01 1:46p Hashida
    Fixed a problem that occurs when DVDCancel is issued to a command block
    that is already canceled.

    24    3/07/01 6:51p Hashida
    Added several functions for BS.

    23    3/02/01 5:48p Hashida
    Modified error code handling (fatal error, internal retry and retry)

    22    3/02/01 12:16a Hashida
    Added DVDCancel.

    21    2/21/01 12:10p Hashida
    Added inquiry command.

    20    11/03/00 11:09a Hashida
    Modified so that programmer can call dvdinit many times.

    19    10/27/00 2:27p Hashida
    Added a check code so that audio streaming command will not be issued
    while a track is auto finishing.

    18    10/09/00 6:56p Hashida
    Added prioritized queuing.
    Fixed a bug that DVDResume doesn't work when DVDPause is issued while
    there's no outstanding dvd command.

    17    10/03/00 10:05a Hashida
    Fixed the following bugs:
    - Coverclose isn't reported soon when read disk id failed with
    nodisk/coveropen in BS2
    - When no disk, the driver resets the drive infinitely.

    16    9/27/00 12:08p Hashida
    Changed to return 2-bit shifted values for DVDGetPlayAddr and
    DVDGetStartAddr.

    15    9/26/00 3:39p Hashida
    Added synchronous version of several functions.

    14    9/25/00 3:33p Hashida
    Changed API names that don't follow the convention (sync, async)

    13    7/21/00 11:14a Hashida
    Added DVDPause, DVDResume and DVDCancel

    12    7/20/00 12:30p Hashida
    Changed to call DVDFSInit from DVDInit.

    11    5/19/00 7:03p Hashida
    Fixed a bug when a fatal error happened.
    Fixed a typo that has been a bug (masked ERRORMASK instead of
    STATEMASK)

    10    4/17/00 4:21p Hashida
    Added audio buffer configuration when DVD is reset.

    9     4/17/00 1:33p Hashida
    Added DVDAudioBufferConfig.

    8     4/13/00 5:17p Hashida
    Added commands that use request audio status DVD command.
    Added error handling sequence for streaming commands.

    7     4/12/00 3:55p Hashida
    Moved audio streaming API definitions from dvdas.h to dvd.h (dvdas.h is
    removed).

    6     4/12/00 1:33p Hashida
    magic number is moved from dvd.h to dvdlayout.h to hide this from
    developers. DVDChangeDisk will not compare the magic number part any
    more when it compares ID.

    5     4/07/00 6:51p Hashida
    Added streaming APIs.

    4     3/13/00 2:45p Hashida
    Changed not to use FSTAddress in the disk for DVDChangeDisk. Use
    FSTLocation in bootInfo, which might have been changed by developers.

    3     2/09/00 6:23p Hashida
    make DVDRead thread-safe.

    2     1/31/00 7:26p Hashida
    Code cleanup (removed the variable "tmp").

    42    11/17/99 11:36a Hashida
    Preparing Minnow, not to load fst even when booted from JTAG since
    Minnow has no DVD support.

    41    11/17/99 11:06a Hashida
    Changed TIMEREMU to SPRUCE.

    40    11/17/99 10:11a Hashida
    Removed TIMEREMU and changed it to SPRUCE since only SPRUCE and
    SPRUCE_MARLIN needs TIMEREMU.

    39    11/16/99 9:47a Hashida
    Changed the behavior according as the boot style, whether bootrom, app
    booted from bootrom or app booted via JTAG.

    38    11/15/99 1:27p Hashida
    Fixed a problem for SPRUCE_MARLIN TEMPORARILY.

    37    10/01/99 10:17a Hashida
    Removed a warning.

    36    9/17/99 3:45p Tian
    Removed warnings.

    35    9/09/99 2:15p Hashida
    Removed warnings.

    34    9/08/99 7:15p Tian
    Added includes to fill in missing prototypes.

    33    8/27/99 5:04p Shiki
    Revised since OS no longer cares DI module level interrupt registers.

    32    8/26/99 5:37p Hashida
    Changed so that DCache invalidation is done in API as a default.
    Added DVDSetAutoInvalidation to change the default behavior.

    31    8/25/99 3:04p Hashida
    Changed not to use DVDRead/WriteReg macros but to use array defined by
    OS.

    30    8/23/99 2:19p Tian
    Moved OSLoMem.h to private.

    29    8/20/99 9:48p Hashida
    Added code to change OS's interrupt mask information directly because
    DVDLowReset resets all the DI registers without telling OS which mask
    is changed.

    28    8/19/99 6:15p Hashida
    Changed to use OSMask/UnmaskInterrupts()
    Changed so that DVDInterruptHandler is registerred using
    OSSetInterruptHandler.

    27    8/19/99 12:31a Hashida
    Removed REALCODE definition since all code is now REALCODE!

    26    8/17/99 1:53p Hashida
    Changed to use ArtX's register definition.
    Moved enabling interrupts in DVDInit to DVDReset().
    Added a code to enable interrupts after DVDLowReset().
    Moved a code to initialize fake registers (invalidate dcache) to
    DVDInitFakeInterrupts.
    Added DVDReset().

    25    8/13/99 11:18p Hashida
    Changed function names (cbForStateReady -> cbForStateBusy,
    processCommand -> stateBusy) to be more self-explanatory.

    24    8/10/99 10:10p Hashida
    Fixed a bug that bootInfo is accessed by physical address.
    Changed bootInfo to a static global variable.

    24    8/09/99 6:04p Hashida
    Fixed a bug that currID was using physical address.

    23    8/09/99 4:11p Hashida
    Fixed a bug that DVDReadDiskID cannot read id well once the cover is
    opened.
    Fixed a bug in stateError.

    22    8/05/99 11:33a Hashida
    Added invalidating BB2 and FST for changedisk

    21    8/05/99 1:14a Hashida
    Changed to clear queue in state error and state cover close (for bs
    read).
    Changed the way to check errors in cbForStateGettingError.

    20    8/04/99 2:41p Hashida
    Moved the part to clear cover int from dvdlow.c.

    19    8/04/99 1:29p Hashida
    Fixed a bug in DVDGetDriveStatus.

    18    8/04/99 11:32a Hashida
    Changed a position of processCommand() function and added a comment.

    17    8/03/99 2:38p Hashida
    Changed to use #define and function for some common parts.
    Changed variables waitingQueue and executing to file static.
    Removed an unused variable.
    Fixed minor bug.

    16    8/03/99 2:17p Hashida
    Use more status-oriented name for functions.
    Added more comments.

    15    7/29/99 6:17p Hashida
    Added wrong disk status.
    Fixed some status bug

    14    7/29/99 12:19p Hashida
    Added calling cover int clear after we detect cover close int.
    Added invalidate cache for initializing fake Flipper registers.

    13    7/28/99 4:19p Hashida
    Fixed a bug that the library cannot be compiled without TIMEREMU

    12    7/27/99 11:13p Hashida
    Used __DVDReadReg to get error code.

    11    7/27/99 2:48p Hashida
    added some code for level2 (timer-based) emulation

    10    7/23/99 3:56p Hashida
    Added some code to refresh FST when disk is successfully changed by
    using DVDChangeDisk()

    9     7/21/99 11:12p Hashida
    Fixed a bug that "next" member is not initialized when queued.
    Changed so that callback is not called if NULL is specified.

    8     7/21/99 10:45p Hashida
    Added busy check

    7     7/21/99 10:54a Hashida
    Added and modified checks whether proper interrupt comes in the low
    level callbacks.

    6     7/21/99 10:16a Hashida
    Inserted ASSERTMSG's

    5     7/20/99 10:30p Hashida
    Added DVDSeekAbs()

    4     7/20/99 6:54p Hashida
    Changed  to invalidate cache for tmpID buffer before DVDLowReadDiskID
    is internally called.
    Fixed some minor mistake

    3     7/20/99 2:39p Hashida
    Changed not to use DVD_RESULT_GOOD

    2     7/19/99 11:38p Hashida
    Changed from DVD_RESULT_DISK_CHANGED to DVD_RESULT_COVER_CLOSED

    1     7/19/99 4:31p Hashida
    Initial revision
 *---------------------------------------------------------------------------*/

#include <dolphin/doldefs.h>
DOLPHIN_LIB_VERSION(DVD);
/* $NoKeywords: $ */

// Dolphin header files
#include <dolphin/os.h>
#include <dolphin/dvd.h>
#include <secure/dvdlow.h>
#include <secure/dvdcb.h>
#include <secure/dvdlayout.h>
#include <dolphin/os/osbootinfo.h>
#include <private/oslomem.h>
#include <string.h>
#include "DVDCBAssert.h"
#include "direg.h"

//#define LOGGING             1

// cover closed state
static void stateCoverClosed(void);
static void cbForStateCoverClosed(u32 intType);

// The following three states are the same as motor stopped state
//   cover open state
//   no disk state
//   wrong disk state
typedef void (*stateFunc)(DVDCommandBlock* block);
stateFunc   LastState;

// for internal retry
static void stateCoverClosed_CMD(DVDCommandBlock* block);
static void stateCheckID2(DVDCommandBlock* block);
static void stateCheckID3(DVDCommandBlock* block);
static void stateCheckID2a(DVDCommandBlock* block);
static void cbForStateCheckID2a(u32 intType);

static void cbForUnrecoveredErrorRetry(u32 intType);
static void cbForUnrecoveredError(u32 intType);

// error state (no callback)
static void stateError(u32 error);
static void stateTimeout(void);

// getting error state
static void stateGettingError(void);
static void cbForStateGettingError(u32 intType);

// motor stopped state
static void stateMotorStopped(void);
static void cbForStateMotorStopped(u32 intType);

// reading FST state
static void stateReadingFST(void);
static void cbForStateReadingFST(u32 intType);

// check id state
static void stateCheckID(void);
static void cbForStateCheckID1(u32 intType);
static void cbForStateCheckID2(u32 intType);
static void cbForStateCheckID3(u32 intType);

// ready state
static void stateReady(void);
static void cbForStateBusy(u32 intType);

// retry state
static void stateGoToRetry(void);
static void cbForStateGoToRetry(u32 intType);


static void BreakCallback(u32 intType);

static BOOL issueCommand(s32 prio, DVDCommandBlock* block);
static void stateBusy(DVDCommandBlock* block);

static DVDCommandBlock*      executing;

static DVDBB2       BB2                     ATTRIBUTE_ALIGN(32);
static DVDDiskID    CurrDiskID              ATTRIBUTE_ALIGN(32);
static DVDDiskID*   IDShouldBe;
static OSBootInfo*  bootInfo;
static BOOL         autoInvalidation = TRUE;
static volatile BOOL PauseFlag = FALSE;
static volatile BOOL PausingFlag = FALSE;
static volatile BOOL AutoFinishing = FALSE;
static volatile BOOL FatalErrorFlag = FALSE;
static vu32         CurrCommand;            // Need to copy here for /canceling/ feature
static vu32         Canceling = FALSE;
static DVDCBCallback        CancelCallback;
static vu32         ResumeFromHere = 0;
static vu32         CancelLastError;
static vu32         LastError;
static vs32         NumInternalRetry = 0;
static volatile BOOL ResetRequired;
static volatile BOOL CancelAllSyncComplete = FALSE;
static vu32         ResetCount = 0;         // Workaround for HW bug.
static volatile BOOL FirstTimeInBootrom = FALSE;

static DVDCommandBlock      DummyCommandBlock;

// from dvdfs.c   XXX fix this in the future
extern OSThreadQueue    __DVDThreadQueue;   // list of threads waiting

extern void __DVDInterruptHandler(__OSInterrupt interrupt, OSContext* context);
extern void __DVDFSInit(void);

static void cbForChangeDiskSync(s32 result, DVDCommandBlock* block);
static void cbForCancelStreamSync(s32 result, DVDCommandBlock* block);
static void cbForStopStreamAtEndSync(s32 result, DVDCommandBlock* block);
static void cbForGetStreamErrorStatusSync(s32 result, DVDCommandBlock* block);
static void cbForGetStreamPlayAddrSync(s32 result, DVDCommandBlock* block);
static void cbForGetStreamStartAddrSync(s32 result, DVDCommandBlock* block);
static void cbForGetStreamLengthSync(s32 result, DVDCommandBlock* block);
static void cbForInquirySync(s32 result, DVDCommandBlock* block);
static void cbForCancelSync(s32 result, DVDCommandBlock* block);
static void cbForCancelAllSync(s32 result, DVDCommandBlock* block);

static OSAlarm ResetAlarm;


static void defaultOptionalCommandChecker(DVDCommandBlock* block,
                                          DVDLowCallback cb);

static DVDOptionalCommandChecker checkOptionalCommand = defaultOptionalCommandChecker;

static void defaultOptionalCommandChecker(DVDCommandBlock* block,
                                          DVDLowCallback cb)
{
#pragma unused(block, cb)
    return;
}


DVDOptionalCommandChecker __DVDSetOptionalCommandChecker(DVDOptionalCommandChecker func)
{
    DVDOptionalCommandChecker       old;

    old = checkOptionalCommand;
    checkOptionalCommand = func;

    return checkOptionalCommand;
}


enum
{
    INTERNALSTATE_WRONG_DISK = 1,
    INTERNALSTATE_RETRY,
    INTERNALSTATE_NODISK,
    INTERNALSTATE_COVEROPEN,
    INTERNALSTATE_ERROR,
    INTERNALSTATE_DISKCHANGE,
    INTERNALSTATE_MOTOR_STOPPED
};

#define MIN(a, b)        ( ((a) > (b))? (b) : (a) )

#ifdef SPRUCE
extern void __DVDInitFakeInterrupt(void);
#endif

#define USE_DMA(command)          ( (command == DVD_COMMAND_READ) ||       \
                                    (command == DVD_COMMAND_BSREAD) ||     \
                                    (command == DVD_COMMAND_READID) ||     \
                                    (command == DVD_COMMAND_INQUIRY) )
#define REQUEST_AUDIO_STATUS(command)       ( (command == DVD_COMMAND_REQUEST_AUDIO_ERROR)    ||  \
                                    (command == DVD_COMMAND_REQUEST_PLAY_ADDR)  ||  \
                                    (command == DVD_COMMAND_REQUEST_START_ADDR) ||  \
                                    (command == DVD_COMMAND_REQUEST_LENGTH) )

static BOOL DVDInitialized = FALSE;


void DVDInit(void)
{
    if (DVDInitialized)
        return;

    OSRegisterVersion(__DVDVersion);

    DVDInitialized = TRUE;

    __DVDFSInit();

    __DVDClearWaitingQueue();

    __DVDInitWA();

    // chain it to the waiting queue
    // store NULL to the executing

    bootInfo = (OSBootInfo*)OSPhysicalToCached(OS_BOOTINFO_ADDR);
    IDShouldBe = &(bootInfo->DVDDiskID);

    // Enable PI interrupt
    __OSSetInterruptHandler(__OS_INTERRUPT_PI_DI,  __DVDInterruptHandler);
    __OSUnmaskInterrupts(OS_INTERRUPTMASK_PI_DI);

#ifdef SPRUCE
    // XXX this function clears DI interrupt
    __DVDInitFakeInterrupt();
#endif

    // initialize the thread queue
    OSInitThreadQueue(&__DVDThreadQueue);

    // Enable DI interrupt
    __DIRegs[DI_SR_IDX] = DI_SR_DEINTMSK_MASK |
                          DI_SR_TCINTMSK_MASK |
                          DI_SR_BRKINTMSK_MASK;

    // Mask cover int
    __DIRegs[DI_CVR_IDX] = 0;

    // if it's not booted from bootrom, there's something
    // to be prepared
    if (bootInfo->magic == OS_BOOTINFO_MAGIC_JTAG)
    {
        extern void __fstLoad(void);

        // if not MINNOW, load fst here.
        // for MINNOW, DI is not supported.
#ifdef SPRUCE
        OSReport("load fst\n");
        __fstLoad();
#else
#ifdef MARLIN
        OSReport("load fst\n");
        __fstLoad();
#endif
#endif
    }
    else if (bootInfo->magic == OS_BOOTINFO_MAGIC)
    {

    }
    else
    {
        FirstTimeInBootrom = TRUE;
    }
}


/*---------------------------------------------------------------------------*
 *  ReadingFST state                                                         *
 *                                                                           *
 * Description: new (right) disk is inserted. Now need to read its FST.      *
 *                                                                           *
 * Action:      Issue Read() function to read FST.                           *
 *                                                                           *
 * IN:          From CheckID status.                                         *
 *                                                                           *
 * OUT:         If the result of Read() is good, goto Ready status.          *
 *              If not, goto GettingError status.                            *
 *                                                                           *
 *---------------------------------------------------------------------------*/
static void stateReadingFST(void)
{
    LastState = (stateFunc)stateReadingFST;

    // tmp should hold BB2.

    // check if FSTlocation is 32byte aligned
    ASSERT(((u32)(bootInfo->FSTLocation) & (32 - 1)) == 0);

    // XXX ignore FST address in the disk for 48MB development system
    // XXX use FST location in bootinfo now because this location
    // XXX might have moved to other location like the bottom of 48Mb.

    // check if there's room for the new FST in memory
    if (!(bootInfo->FSTMaxLength >= BB2.FSTLength))
    {
        OSPanic(__FILE__, __LINE__, "DVDChangeDisk(): FST in the new disc is too big.   ");
    }

    DVDLowRead( bootInfo->FSTLocation,
                OSRoundUp32B(BB2.FSTLength),
                BB2.FSTPosition,
                cbForStateReadingFST);
}

static void cbForStateReadingFST(u32 intType)
{
    DVDCommandBlock*            finished;

    if (intType == DVD_INTTYPE_TIMEOUT)
    {
        executing->state = DVD_STATE_FATAL_ERROR;
        stateTimeout();
        return;
    }

    ASSERT( (intType & DVD_INTTYPE_CVR) == 0 );

    if (intType & DVD_INTTYPE_TC)
    {
        // it's strange if TCINT and DEINT comes at the same time
        ASSERT( (intType & DVD_INTTYPE_DE) == 0 );

        // clear this counter because the previous command worked fine
        NumInternalRetry = 0;

        // Must initialize file system layer to propagate the change
        __DVDFSInit();

        // clear /executing/ before calling callback.
        // this is because of the check for seeing if command block is in use.
        // if executing is still there, it's hard to check if the dvd API
        // is called in callback.
        finished = executing;
        executing = &DummyCommandBlock;

        finished->state = DVD_STATE_END;
        if (finished->callback)
        {
            (finished->callback)(0, finished);
        }

        stateReady();

    }
    else // DEINT
    {
        // paranoia: BRKINT will be added in the future
        ASSERT( intType == DVD_INTTYPE_DE );

        stateGettingError();
    }
}


/*---------------------------------------------------------------------------*
 *  CoverOpen state                                                          *
 *                                                                           *
 * Description: Cover is open. Need to wait for the user to close the cover  *
 *                                                                           *
 * Action:      Issue WaitCoverClose() function.                             *
 *                                                                           *
 * IN:          From GettingError status.                                    *
 *                                                                           *
 * OUT:         Go to CoverClosed status.                                    *
 *                                                                           *
 *---------------------------------------------------------------------------*/
#define stateCoverOpen         stateMotorStopped

/*---------------------------------------------------------------------------*
 *  NoDisk state                                                             *
 *                                                                           *
 * Description: Disk doesn't exist. Need to wait for the user to insert a    *
 *              disk.                                                        *
 *                                                                           *
 * Action:      Issue WaitCoverClose() function.                             *
 *                                                                           *
 * IN:          From GettingError status.                                    *
 *                                                                           *
 * OUT:         Go to CoverClosed status.                                    *
 *                                                                           *
 *---------------------------------------------------------------------------*/
#define stateNoDisk            stateMotorStopped

/*---------------------------------------------------------------------------*
 *  Error state                                                              *
 *                                                                           *
 * Description: Fatal error occurred.                                        *
 *                                                                           *
 * Action:      Do nothing.                                                  *
 *                                                                           *
 * IN:          From GettingError status.                                    *
 *                                                                           *
 * OUT:         Go to nowhere.                                               *
 *                                                                           *
 *---------------------------------------------------------------------------*/

extern void __DVDPrintFatalMessage(void);

static void cbForStateError(u32 intType)
{
    DVDCommandBlock*            finished;
    // clear waiting queue

    if (intType == DVD_INTTYPE_TIMEOUT)
    {
        executing->state = DVD_STATE_FATAL_ERROR;
        stateTimeout();
        return;
    }

    // Only if developers want, show error message on screen here
    __DVDPrintFatalMessage();

    // clear /executing/ before calling callback.
    // this is because of the check for seeing if command block is in use.
    // if executing is still there, it's hard to check if the dvd API
    // is called in callback.
    FatalErrorFlag = TRUE;
    finished = executing;
    executing = &DummyCommandBlock;
    if (finished->callback)
    {
        (finished->callback)(DVD_RESULT_FATAL_ERROR, finished);
    }

    if (Canceling)
    {
        Canceling = FALSE;
        if (CancelCallback)
            (CancelCallback)(0, finished);
    }

    stateReady();

    return;
}

static void stateError(u32 error)
{
    __DVDStoreErrorCode(error);

    // stop motor to prepare cover open
    DVDLowStopMotor(cbForStateError);
}

extern void  __FlushTimeStamp(void);

static void stateTimeout(void)
{
    __DVDStoreErrorCode(DVD_TIMEOUT_ERROR_CODE);

    DVDReset();

#ifdef LOGGING
    {
        extern void __FlushTimeStamp(void);

        __FlushTimeStamp();
    }
#endif

    // do not stop motor: stop motor can cause another timeout error
    cbForStateError(0);     // argument"intType" will not be used anyway
}

/*---------------------------------------------------------------------------*
 *  GettingError state                                                       *
 *                                                                           *
 * Description: Some error occurred. Need to retrieve which error occurred.  *
 *                                                                           *
 * Action:      Issue RequestError() function.                               *
 *                                                                           *
 * IN:          From Ready or CoverClosed, CheckID, ReadingFST.              *
 *                                                                           *
 * OUT:         Go to NoDisk, CoverOpen, Error, CoverClosed according as the *
 *              error.                                                       *
 *                                                                           *
 *---------------------------------------------------------------------------*/
static void stateGettingError(void)
{
    DVDLowRequestError(cbForStateGettingError);
}

#define ERROR_RECOVERABLE           0
#define ERROR_FATAL                 1
#define ERROR_RETRY                 2
#define ERROR_RETRY_INTERNALLY      3

static u32 CategorizeError(u32 error)
{
    if ( error ==
         (DVD_INTERNALERROR_STATUS_READY | DVD_INTERNALERROR_MOTOR_STOPPED) )
    {
        // This error code is a "FATAL" error
        LastError = error;
        return ERROR_FATAL;
    }

    // From here, we are only interested in the "error" code part
    error &= DVD_INTERNALERROR_ERRORMASK;

    if ((error == DVD_INTERNALERROR_COVER_CLOSED) ||
        (error == DVD_INTERNALERROR_COVEROPEN_OR_NODISK) ||
        (error == DVD_INTERNALERROR_OP_DISK_RM_REQ))
    {
        return ERROR_RECOVERABLE;
    }

    NumInternalRetry++;
    if (NumInternalRetry == 2)
    {
        if (error == LastError)
        {
            LastError = error;
            return ERROR_FATAL;
        }
        else
        {
            LastError = error;
            return ERROR_RETRY;
        }
    }
    else
    {
        LastError = error;

        if ( (error == DVD_INTERNALERROR_UNRECOVERED_READ) ||
             (executing->command == DVD_COMMAND_READID) )
        {
            // if readid returns unrecovered read error,
            // do not retry internally
            return ERROR_RETRY;
        }
        else
        {
            return ERROR_RETRY_INTERNALLY;
        }
    }
}

static BOOL CheckCancel(u32 resume)
{
    DVDCommandBlock*            finished;

    if (Canceling)
    {
        ResumeFromHere = resume;
        Canceling = FALSE;

        // clear /executing/ before calling callback.
        // this is because of the check for seeing if command block is in use.
        // if executing is still there, it's hard to check if the dvd API
        // is called in callback.
        finished = executing;
        executing = &DummyCommandBlock;

        finished->state = DVD_STATE_CANCELED;
        if (finished->callback)
            (*finished->callback)(DVD_RESULT_CANCELED, finished);
        if (CancelCallback)
            (CancelCallback)(0, finished);
        stateReady();
        return TRUE;
    }
    else
        return FALSE;
}

static void cbForStateGettingError(u32 intType)
{
    u32                         error;
    u32                         status;
    u32                         errorCategory;
    u32                         resume;

    if (intType == DVD_INTTYPE_TIMEOUT)
    {
        executing->state = DVD_STATE_FATAL_ERROR;
        stateTimeout();
        return;
    }

    if ( intType & DVD_INTTYPE_DE )
    {
        // should go to fatal because request error should not
        // generate an error
        executing->state = DVD_STATE_FATAL_ERROR;
        stateError(DVD_DE_INT_ERROR_CODE);          // error code will not be used
        return;
    }

    // should be TCINT and no other int should not occur
    ASSERT( intType == DVD_INTTYPE_TC );

    // Don't clear error count because the previous command is RequestError

    error = __DIRegs[DI_IMMBUF_IDX];
    status = error & DVD_INTERNALERROR_STATUSMASK;

    errorCategory = CategorizeError(error);

    if (errorCategory == ERROR_FATAL)
    {
        // no need to check /Canceling/. fatal error should be treated as
        // higher priority
        executing->state = DVD_STATE_FATAL_ERROR;
        stateError(error);
        return;
    }

    if ( (errorCategory == ERROR_RETRY)
         || (errorCategory == ERROR_RETRY_INTERNALLY) )
    {
        resume = 0;
    }
    else
    {
        if (status == DVD_INTERNALERROR_STATUS_COVEROPEN)
            resume = INTERNALSTATE_COVEROPEN;
        else if (status == DVD_INTERNALERROR_STATUS_DISKCHANGE)
            resume = INTERNALSTATE_DISKCHANGE;
        else if (status == DVD_INTERNALERROR_STATUS_NODISK)
            resume = INTERNALSTATE_NODISK;
        else
            resume = INTERNALSTATE_ERROR;
    }

    if (CheckCancel(resume))
        return;

    if (errorCategory == ERROR_RETRY)
    {
        __DVDStoreErrorCode(error);
        stateGoToRetry();
        return;
    }

    if (errorCategory == ERROR_RETRY_INTERNALLY)
    {
        if ((error & DVD_INTERNALERROR_ERRORMASK) == DVD_INTERNALERROR_UNRECOVERED_READ)
        {
            DVDLowSeek(executing->offset, cbForUnrecoveredError);
        }
        else
        {
            LastState(executing);
        }
        return;
    }

    // errorCategory = ERROR_RECOVERABLE

    // Note: no disk error can happen when coming from checkid state
    if (status == DVD_INTERNALERROR_STATUS_COVEROPEN)
    {
        executing->state = DVD_STATE_COVER_OPEN;
        stateCoverOpen();
        return;
    }
    else if (status == DVD_INTERNALERROR_STATUS_DISKCHANGE)
    {
        executing->state = DVD_STATE_COVER_CLOSED;
        stateCoverClosed();
        return;
    }
    else if (status == DVD_INTERNALERROR_STATUS_NODISK)
    {
        executing->state = DVD_STATE_NO_DISK;
        stateNoDisk();
        return;
    }
    else
    {
        executing->state = DVD_STATE_FATAL_ERROR;
        stateError(DVD_DE_INT_ERROR_CODE);
        return;
    }
}

static void cbForUnrecoveredError(u32 intType)
{
    if (intType == DVD_INTTYPE_TIMEOUT)
    {
        executing->state = DVD_STATE_FATAL_ERROR;
        stateTimeout();
        return;
    }

    if (intType & DVD_INTTYPE_TC)
    {
        stateGoToRetry();
        return;
    }

    ASSERT( intType == DVD_INTTYPE_DE );

    DVDLowRequestError(cbForUnrecoveredErrorRetry);
}

static void cbForUnrecoveredErrorRetry(u32 intType)
{
    if (intType == DVD_INTTYPE_TIMEOUT)
    {
        executing->state = DVD_STATE_FATAL_ERROR;
        stateTimeout();
        return;
    }

    executing->state = DVD_STATE_FATAL_ERROR;

    if (intType & DVD_INTTYPE_DE)
    {
        stateError(DVD_DE_INT_ERROR_CODE);
    }
    else
    {
        stateError(__DIRegs[DI_IMMBUF_IDX]);
    }

    return;
}

#define stateRetry             stateMotorStopped

static void stateGoToRetry(void)
{
    DVDLowStopMotor(cbForStateGoToRetry);
}

static void cbForStateGoToRetry(u32 intType)
{
    if (intType == DVD_INTTYPE_TIMEOUT)
    {
        executing->state = DVD_STATE_FATAL_ERROR;
        stateTimeout();
        return;
    }

    if ( intType & DVD_INTTYPE_DE )
    {
        // should go to fatal because stop motor should not
        // generate an error
        executing->state = DVD_STATE_FATAL_ERROR;
        stateError(DVD_DE_INT_ERROR_CODE);          // error code will not be used
        return;
    }

    // should be TCINT and no other int should not occur
    ASSERT( intType == DVD_INTTYPE_TC );

    // clear this counter because the previous command worked fine
    NumInternalRetry = 0;

    if ( (CurrCommand == DVD_COMMAND_BSREAD) ||
         (CurrCommand == DVD_COMMAND_READID) ||
         (CurrCommand == DVD_COMMAND_AUDIO_BUFFER_CONFIG) ||
         (CurrCommand == DVD_COMMAND_BS_CHANGE_DISK) )
    {
        ResetRequired = TRUE;
    }

    if (!CheckCancel(INTERNALSTATE_RETRY))
    {
        executing->state = DVD_STATE_RETRY;
        stateRetry();
    }
}


/*---------------------------------------------------------------------------*
 *  WrongDisk state                                                          *
 *                                                                           *
 * Description: Motor is stopped. System decided that the current disk is    *
 *              a wrong disk.                                                *
 *                                                                           *
 * Action:      Issue WaitCoverClose() function                              *
 *                                                                           *
 * IN:          From CheckID state.                                          *
 *                                                                           *
 * OUT:         If the cover is closed, go to CoverClosed status.            *
 *                                                                           *
 *---------------------------------------------------------------------------*/
#define stateWrongDisk         stateMotorStopped

/*---------------------------------------------------------------------------*
 *  CheckID state                                                            *
 *                                                                           *
 * Description: ID is read. Now must check the ID whether it's a right disk  *
 *                                                                           *
 * Action:      Depends on commands.                                         *
 *              If read or seek: If it's the right disk, configure audio     *
 *                               buffer and then proceed processing original *
 *                               function. If not, issue stopMotor to wait   *
 *                               for the user to change disk.                *
 *              If change disk:  If it's a right disk, read bb2 to get info  *
 *                               for FST. If not, issue stopMotor to wait    *
 *                               for the user to change disk.                *
 *                                                                           *
 * IN:          From CoverClosed.                                            *
 *                                                                           *
 * OUT:         Depends on commands.                                         *
 *              If it's not a right disk, goto WrongDisk status after        *
 *              stopMotor is finished. If it's a right disk and if issued    *
 *              function failed, goto GettingError status. If issued         *
 *              function succeeded, goto ReadingFST status if change disk,   *
 *              goto ready status if other commands.                         *
 *                                                                           *
 *---------------------------------------------------------------------------*/
static void stateCheckID(void)
{
    switch (CurrCommand)
    {
      case DVD_COMMAND_CHANGE_DISK:
        if (DVDCompareDiskID(&CurrDiskID, executing->id))
        {
            // same
            memcpy(IDShouldBe, &CurrDiskID, sizeof(DVDDiskID));

            executing->state = DVD_STATE_BUSY;
            DCInvalidateRange(&BB2, sizeof(DVDBB2));
            LastState = stateCheckID2a;
            stateCheckID2a(executing);
            return;
        }
        else
        {
            // differs
            DVDLowStopMotor(cbForStateCheckID1);
        }
        break;

      default:
        if ( memcmp(&CurrDiskID, IDShouldBe, sizeof(DVDDiskID)) )
        {
            // differs
            DVDLowStopMotor(cbForStateCheckID1);
        }
        else
        {
            // same
            LastState = stateCheckID3;  // to prepare internal retry
            stateCheckID3(executing);
        }
        break;
    }
}

static void stateCheckID3(DVDCommandBlock* block)
{
#pragma unused(block)
    DVDLowAudioBufferConfig((BOOL)IDShouldBe->streaming,
                            DVD_AUDIO_BUFFER_SIZE,
                            cbForStateCheckID3);
}

static void stateCheckID2a(DVDCommandBlock* block)
{
#pragma unused(block)
    DVDLowAudioBufferConfig((BOOL)IDShouldBe->streaming,
                            DVD_AUDIO_BUFFER_SIZE,
                            cbForStateCheckID2a);
}

static void cbForStateCheckID2a(u32 intType)
{
    if (intType == DVD_INTTYPE_TIMEOUT)
    {
        executing->state = DVD_STATE_FATAL_ERROR;
        stateTimeout();
        return;
    }

    ASSERT( (intType & DVD_INTTYPE_CVR) == 0 );

    if (intType & DVD_INTTYPE_TC)
    {
        // it's strange if TCINT and DEINT comes at the same time
        ASSERT( (intType & DVD_INTTYPE_DE) == 0 );

        // clear this counter because the previous command worked fine
        NumInternalRetry = 0;

        // we should not check cancel here;
        stateCheckID2(executing);
    }
    else // DEINT
    {
        // paranoia: BRKINT will be added in the future
        ASSERT( intType == DVD_INTTYPE_DE );

        stateGettingError();
    }
}


static void stateCheckID2(DVDCommandBlock* block)
{
#pragma unused(block)
    DVDLowRead( &BB2,
                OSRoundUp32B(sizeof(DVDBB2)),
                DVDLAYOUT_BB2_POSITION,
                cbForStateCheckID2);
}

// When ID is wrong
static void cbForStateCheckID1(u32 intType)
{
    if (intType == DVD_INTTYPE_TIMEOUT)
    {
        executing->state = DVD_STATE_FATAL_ERROR;
        stateTimeout();
        return;
    }

    // XXX should check if it's canceling first?
    // if so, we can't use executing below...
    if ( intType & DVD_INTTYPE_DE )
    {
        // should go to fatal because stop motor should not
        // generate an error
        executing->state = DVD_STATE_FATAL_ERROR;
        stateError(DVD_DE_INT_ERROR_CODE);          // error code will not be used
        return;
    }

    // should be TCINT and no other int should not occur
    ASSERT( intType == DVD_INTTYPE_TC );

    // clear this counter because the previous command worked fine
    NumInternalRetry = 0;

    if (!CheckCancel(INTERNALSTATE_WRONG_DISK))
    {
        executing->state = DVD_STATE_WRONG_DISK;
        stateWrongDisk();
    }
}

// When change disk command and ID is good
static void cbForStateCheckID2(u32 intType)
{
    if (intType == DVD_INTTYPE_TIMEOUT)
    {
        executing->state = DVD_STATE_FATAL_ERROR;
        stateTimeout();
        return;
    }

    ASSERT( (intType & DVD_INTTYPE_CVR) == 0 );

    if (intType & DVD_INTTYPE_TC)
    {
        // it's strange if TCINT and DEINT comes at the same time
        ASSERT( (intType & DVD_INTTYPE_DE) == 0 );

        // XXX ignore FST address in the new disk but use FST location
        // XXX in boot info. This is for development system.
        // XXX Developer might have moved the FST location to the
        // XXX bottom of 48M.

        // clear this counter because the previous command worked fine
        NumInternalRetry = 0;

        stateReadingFST();

    }
    else // DEINT
    {
        // paranoia: BRKINT will be added in the future
        ASSERT( intType == DVD_INTTYPE_DE );

        stateGettingError();
    }
}

// When read or seek command and ID is good.
static void cbForStateCheckID3(u32 intType)
{
    if (intType == DVD_INTTYPE_TIMEOUT)
    {
        executing->state = DVD_STATE_FATAL_ERROR;
        stateTimeout();
        return;
    }

    ASSERT( (intType & DVD_INTTYPE_CVR) == 0 );

    if (intType & DVD_INTTYPE_TC)
    {
        // it's strange if TCINT and DEINT comes at the same time
        ASSERT( (intType & DVD_INTTYPE_DE) == 0 );

        // clear this counter because the previous command worked fine
        NumInternalRetry = 0;

        if (!CheckCancel(0))
        {
            executing->state = DVD_STATE_BUSY;
            stateBusy(executing);
        }
    }
    else // DEINT
    {
        // paranoia: BRKINT will be added in the future
        ASSERT( intType == DVD_INTTYPE_DE );

        stateGettingError();
    }
}

static void AlarmHandler(OSAlarm* alarm, OSContext* context)
{
#pragma unused(alarm, context)
    DVDReset();
    DCInvalidateRange(&CurrDiskID, sizeof(DVDDiskID));
    LastState = stateCoverClosed_CMD;
    stateCoverClosed_CMD(executing);
}

/*---------------------------------------------------------------------------*
 *  CoverClosed state                                                        *
 *                                                                           *
 * Description: Cover close is detected. System now needs to reset the drive *
 *              to do next action.                                           *
 *                                                                           *
 * Action:      Issue Reset() and ReadID() functions for non BS2 functions.  *
 *              Call callback for BS2 functions.                             *
 *                                                                           *
 * IN:          From MotorStopped or GettingError, NoDisk, CoverOpen,        *
 *              WrongDisk.                                                   *
 *                                                                           *
 * OUT:         Non BS2 functions:                                           *
 *                 If the result of ReadID() is good, goto CheckID status.   *
 *                 If not, goto GettingError status.                         *
 *              BS2 functions (bsread, readid and audio buf config)          *
 *                 Ends the error handling sequence and goto ready status.   *
 *                                                                           *
 *---------------------------------------------------------------------------*/
static void stateCoverClosed(void)
{
    DVDCommandBlock*            finished;

    switch(CurrCommand)
    {
      case DVD_COMMAND_READID:
      case DVD_COMMAND_BSREAD:
      case DVD_COMMAND_AUDIO_BUFFER_CONFIG:
      case DVD_COMMAND_BS_CHANGE_DISK:
        __DVDClearWaitingQueue();

        // clear /executing/ before calling callback.
        // this is because of the check for seeing if command block is in use.
        // if executing is still there, it's hard to check if the dvd API
        // is called in callback.
        finished = executing;
        executing = &DummyCommandBlock;
        if (finished->callback)
        {
            (finished->callback)(DVD_RESULT_COVER_CLOSED, finished);
        }
        stateReady();
        break;

      default:
        DVDReset();
        OSCreateAlarm(&ResetAlarm);
        OSSetAlarm(&ResetAlarm, DVD_2RESETS_INTERVAL, AlarmHandler);
        break;
    }
}

static void stateCoverClosed_CMD(DVDCommandBlock* block)
{
#pragma unused(block)
    DVDLowReadDiskID(&CurrDiskID, cbForStateCoverClosed);
}

static void cbForStateCoverClosed(u32 intType)
{
    if (intType == DVD_INTTYPE_TIMEOUT)
    {
        executing->state = DVD_STATE_FATAL_ERROR;
        stateTimeout();
        return;
    }

    ASSERT( (intType & DVD_INTTYPE_CVR) == 0 );

    if (intType & DVD_INTTYPE_TC)
    {
        // it's strange if TCINT and DEINT comes at the same time
        ASSERT( (intType & DVD_INTTYPE_DE) == 0 );

        // clear this counter because the previous command worked fine
        NumInternalRetry = 0;

        // we should not cancelled here (we should not check cancel here)
        // we should issue audio buf config command first
        stateCheckID();
    }
    else  // DEINT
    {
        // paranoia: BRKINT will be added in the future
        ASSERT( intType == DVD_INTTYPE_DE );

        stateGettingError();
    }

}

/*---------------------------------------------------------------------------*
 *  MotorStopped state                                                       *
 *                                                                           *
 * Description: Motor is stopped. System is waiting for user to exchange     *
 *              disk.                                                        *
 *                                                                           *
 * Action:      Issue WaitCoverClose() function                              *
 *                                                                           *
 * IN:          From ready state.                                            *
 *                                                                           *
 * OUT:         If the cover is closed, go to CoverClosed status.            *
 *                                                                           *
 *---------------------------------------------------------------------------*/
static void stateMotorStopped(void)
{
    DVDLowWaitCoverClose(cbForStateMotorStopped);
}

static void cbForStateMotorStopped(u32 intType)
{
#pragma unused (intType)
    // should be CVRINT and no other int should not occur
    ASSERT( intType == DVD_INTTYPE_CVR );

    // mask cvr int
    __DIRegs[DI_CVR_IDX] = 0;

    // no need to check cancel. cover is just closed, which means
    // cover was open. canceling flag should be false.
    executing->state = DVD_STATE_COVER_CLOSED;
    stateCoverClosed();
}

/*---------------------------------------------------------------------------*
 *  Ready state                                                              *
 *                                                                           *
 * Description: Be ready for any command                                     *
 *                                                                           *
 * Action:      Do next command if any.                                      *
 *                                                                           *
 * IN:          From busy.                                                   *
 *                                                                           *
 * OUT:         If there's any command to process next, goes to Busy state.  *
 *              If not, end.                                                 *
 *                                                                           *
 *---------------------------------------------------------------------------*/
static void stateReady(void)
{
    DVDCommandBlock*            finished;

    if(!__DVDCheckWaitingQueue())
    {
        executing = (DVDCommandBlock*)NULL;
        return;
        // NOT REACHED
    }

    if (PauseFlag)
    {
        PausingFlag = TRUE;
        executing = (DVDCommandBlock*)NULL;
        return;
    }

    // there's a command to be processed in the queue
    executing = __DVDPopWaitingQueue();

    if (FatalErrorFlag)
    {
        executing->state = DVD_STATE_FATAL_ERROR;
        finished = executing;
        executing = &DummyCommandBlock;
        if (finished->callback)
        {
            (finished->callback)(DVD_RESULT_FATAL_ERROR, finished);
        }
        stateReady();
        return;
    }

    // copy. /executing/ is on user memory, we need to
    // copy this to CurrCommand to prepare for user to
    // free the memory (this can happen when /canceling/)
    CurrCommand = executing->command;

    if (ResumeFromHere)
    {
        switch (ResumeFromHere)
        {
          case INTERNALSTATE_RETRY:
            executing->state = DVD_STATE_RETRY;
            stateRetry();
            break;

          case INTERNALSTATE_NODISK:
            executing->state = DVD_STATE_NO_DISK;
            stateNoDisk();
            break;

          case INTERNALSTATE_COVEROPEN:
            executing->state = DVD_STATE_COVER_OPEN;
            stateCoverOpen();
            break;

          case INTERNALSTATE_WRONG_DISK:
            // After cancel, "wrong disk" may not be true anymore
            // (e.g., when wrong disk error occurs for DVDChangeDisk, 
            // after cancel, the disk may be a "right one" when the
            // disk is the first disk
          case INTERNALSTATE_MOTOR_STOPPED:
          case INTERNALSTATE_DISKCHANGE:
            executing->state = DVD_STATE_COVER_CLOSED;
            stateCoverClosed();
            break;

          case INTERNALSTATE_ERROR:
            executing->state = DVD_STATE_FATAL_ERROR;
            stateError(CancelLastError);
            break;
        }

        ResumeFromHere = 0;
    }
    else
    {
        executing->state = DVD_STATE_BUSY;
        stateBusy(executing);
    }
}

/*---------------------------------------------------------------------------*
 *  Ready state                                                              *
 *                                                                           *
 * Description: Busy processing command                                      *
 *                                                                           *
 * Action:      Issue an appropriate command                                 *
 *                                                                           *
 * IN:          From command                                                 *
 *                                                                           *
 * OUT:         If the command is change disk, the function called is Motor  *
 *              Stop, so it always success and goes to MotorStopped state.   *
 *              If not, if the function succeeds, goes to Ready state.       *
 *              If fails, goes to GettingError state.                        *
 *                                                                           *
 *---------------------------------------------------------------------------*/
static void stateBusy(DVDCommandBlock* block)
{
    DVDCommandBlock*            finished;


    LastState = stateBusy;

    switch (block->command)
    {
      case DVD_COMMAND_READID:
        // clear cover int
        __DIRegs[DI_CVR_IDX] = __DIRegs[DI_CVR_IDX];

        block->currTransferSize = sizeof(DVDDiskID);
        DVDLowReadDiskID(block->addr, cbForStateBusy);
        break;

      case DVD_COMMAND_READ:
      case DVD_COMMAND_BSREAD:
        if (block->length == 0)
        {
            // since size is 0, we complete the command without an
            // error
            finished = executing;
            executing = &DummyCommandBlock;

            finished->state = DVD_STATE_END;
            if (finished->callback)
            {
                (finished->callback)(0, finished);
            }
            stateReady();
        }
        else
        {
            // clear cover int
            __DIRegs[DI_CVR_IDX] = __DIRegs[DI_CVR_IDX];

            // split into small sizes when exceeding the limit
            block->currTransferSize
                = MIN(block->length - block->transferredSize,
                      DVD_MAX_ONE_TRANSFER);
            DVDLowRead( (void*)( (u8*)block->addr
                                 + block->transferredSize ),
                        block->currTransferSize,
                        block->offset
                                 + block->transferredSize,
                        cbForStateBusy);
        }
        break;

      case DVD_COMMAND_SEEK:
        // clear cover int XXX needed?
        __DIRegs[DI_CVR_IDX] = __DIRegs[DI_CVR_IDX];

        DVDLowSeek(block->offset, cbForStateBusy);
        break;

      case DVD_COMMAND_CHANGE_DISK:
        // Do not clear cover int because stop motor doesn't report cover close
        DVDLowStopMotor(cbForStateBusy);
        break;

      case DVD_COMMAND_BS_CHANGE_DISK:
        // Do not clear cover int because stop motor doesn't report cover close
        DVDLowStopMotor(cbForStateBusy);
        break;

      case DVD_COMMAND_INITSTREAM:
        // clear cover int
        __DIRegs[DI_CVR_IDX] = __DIRegs[DI_CVR_IDX];

        if (AutoFinishing)
        {
            // Since auto finishing might not have been complete, we need to check
            // 0 for currtransfersize means checking
            executing->currTransferSize = 0;
            DVDLowRequestAudioStatus(DRIVE_SUBCMD_ERROR_STATUS,
                                     cbForStateBusy);
        }
        else
        {
            // 1 for currtransfersize means the real command has issued
            executing->currTransferSize = 1;
            DVDLowAudioStream(DRIVE_SUBCMD_ENTRY,
                              block->length,
                              block->offset,
                              cbForStateBusy);
        }
        break;

      case DVD_COMMAND_CANCELSTREAM:
        // clear cover int
        __DIRegs[DI_CVR_IDX] = __DIRegs[DI_CVR_IDX];

        DVDLowAudioStream(DRIVE_SUBCMD_CLEAR, 0, 0,
                          cbForStateBusy);
        break;

      case DVD_COMMAND_STOP_STREAM_AT_END:
        // clear cover int
        __DIRegs[DI_CVR_IDX] = __DIRegs[DI_CVR_IDX];

        AutoFinishing = TRUE;

        DVDLowAudioStream(DRIVE_SUBCMD_ENTRY, 0, 0,
                          cbForStateBusy);
        break;

      case DVD_COMMAND_REQUEST_AUDIO_ERROR:
        // clear cover int
        __DIRegs[DI_CVR_IDX] = __DIRegs[DI_CVR_IDX];

        DVDLowRequestAudioStatus(DRIVE_SUBCMD_ERROR_STATUS,
                                 cbForStateBusy);
        break;

      case DVD_COMMAND_REQUEST_PLAY_ADDR:
        // clear cover int
        __DIRegs[DI_CVR_IDX] = __DIRegs[DI_CVR_IDX];

        DVDLowRequestAudioStatus(DRIVE_SUBCMD_PLAY_ADDR,
                                 cbForStateBusy);
        break;

      case DVD_COMMAND_REQUEST_START_ADDR:
        // clear cover int
        __DIRegs[DI_CVR_IDX] = __DIRegs[DI_CVR_IDX];

        DVDLowRequestAudioStatus(DRIVE_SUBCMD_START_ADDR,
                                 cbForStateBusy);
        break;

      case DVD_COMMAND_REQUEST_LENGTH:
        // clear cover int
        __DIRegs[DI_CVR_IDX] = __DIRegs[DI_CVR_IDX];

        DVDLowRequestAudioStatus(DRIVE_SUBCMD_LENGTH,
                                 cbForStateBusy);
        break;

      case DVD_COMMAND_AUDIO_BUFFER_CONFIG:
        // clear cover int
        __DIRegs[DI_CVR_IDX] = __DIRegs[DI_CVR_IDX];

        DVDLowAudioBufferConfig((BOOL)block->offset, block->length,
                                cbForStateBusy);
        break;

      case DVD_COMMAND_INQUIRY:
        // clear cover int
        __DIRegs[DI_CVR_IDX] = __DIRegs[DI_CVR_IDX];

        block->currTransferSize = sizeof(DVDDriveInfo);
        DVDLowInquiry(block->addr, cbForStateBusy);
        break;

      default:
        checkOptionalCommand(block, cbForStateBusy);
        break;
    }
}

static u32 ImmCommand[] = {0xffffffff, 0xffffffff, 0xffffffff};
static u32 DmaCommand[] = {0xffffffff};

void __DVDSetImmCommand(u32 command)
{
    static u32 immCount = 0;

    ASSERT(immCount < sizeof(ImmCommand)/sizeof(ImmCommand[0]));
    ImmCommand[immCount++] = command;
}

void __DVDSetDmaCommand(u32 command)
{
    static u32 dmaCount = 0;

    ASSERT(dmaCount < sizeof(DmaCommand)/sizeof(DmaCommand[0]));
    DmaCommand[dmaCount++] = command;
}


static BOOL IsImmCommandWithResult(u32 command)
{
    u32                 i;

    if (REQUEST_AUDIO_STATUS(command))
        return TRUE;

    for (i = 0; i < sizeof(ImmCommand)/sizeof(ImmCommand[0]); i++)
    {
        if (command == ImmCommand[i])
            return TRUE;
    }

    return FALSE;
}

static BOOL IsDmaCommand(u32 command)
{
    u32                 i;

    if (USE_DMA(command))
        return TRUE;

    for (i = 0; i < sizeof(DmaCommand)/sizeof(DmaCommand[0]); i++)
    {
        if (command == DmaCommand[i])
            return TRUE;
    }

    return FALSE;
}

static void cbForStateBusy(u32 intType)
{
    DVDCommandBlock*            finished;

    if (intType == DVD_INTTYPE_TIMEOUT)
    {
        executing->state = DVD_STATE_FATAL_ERROR;
        stateTimeout();
        return;
    }

    if ( (CurrCommand == DVD_COMMAND_CHANGE_DISK) ||
         (CurrCommand == DVD_COMMAND_BS_CHANGE_DISK) )
    {
        if ( intType & DVD_INTTYPE_DE )
        {
            // should go to fatal since stop motor should not
            // generate an error
            executing->state = DVD_STATE_FATAL_ERROR;
            stateError(DVD_DE_INT_ERROR_CODE);          // error code will not be used
            return;
        }

        // should be TCINT and no other int should not occur
        ASSERT( intType == DVD_INTTYPE_TC );

        // clear this counter because the previous command worked fine
        NumInternalRetry = 0;

        if (CurrCommand == DVD_COMMAND_BS_CHANGE_DISK)
            ResetRequired = TRUE;

        if (CheckCancel(INTERNALSTATE_MOTOR_STOPPED))
        {
            return;
        }

        executing->state = DVD_STATE_MOTOR_STOPPED;
        stateMotorStopped();

        return;
    }
    else
    {
        ASSERT( (intType & DVD_INTTYPE_CVR) == 0 );

        if (IsDmaCommand(CurrCommand))
        {
            // currTransferSize and transferrredSize should be modified
            // at the same time; in this case, if the next state is
            // /busy/ mode, currTransferSize will be set properly later.
            // currTransferSize is only used when busy mode, so no
            // need to set currTransferSize here.
            executing->transferredSize +=
                executing->currTransferSize - __DIRegs[DI_LENGTH_IDX];
        }

        // Treat break interrupt at high priority
        if (intType & DVD_INTTYPE_BRK)
        {
            Canceling = FALSE;

            // clear /executing/ before calling callback.
            // this is because of the check for seeing if command block is in use.
            // if executing is still there, it's hard to check if the dvd API
            // is called in callback.
            finished = executing;
            executing = &DummyCommandBlock;

            finished->state = DVD_STATE_CANCELED;
            if (finished->callback)
                (*finished->callback)(DVD_RESULT_CANCELED, finished);
            if (CancelCallback)
                (CancelCallback)(0, finished);
            stateReady();

            return;
        }

        if (intType & DVD_INTTYPE_TC)
        {
            // it's strange if TCINT and DEINT comes at the same time
            ASSERT( (intType & DVD_INTTYPE_DE) == 0 );

            // clear this counter because the previous command worked fine
            NumInternalRetry = 0;

            if (CheckCancel(0))
                return;

            if (IsDmaCommand(CurrCommand))
            {
                if (executing->transferredSize != executing->length)
                {
                    // not end yet
                    stateBusy(executing);
                    return;
                }

                // clear /executing/ before calling callback.
                // this is because of the check for seeing if command block is in use.
                // if executing is still there, it's hard to check if the dvd API
                // is called in callback.
                finished = executing;
                executing = &DummyCommandBlock;

                finished->state = DVD_STATE_END;
                if (finished->callback)
                {
                    (finished->callback)((s32)finished->transferredSize, finished);
                }
                stateReady();
            }
            else if (IsImmCommandWithResult(CurrCommand))
            {
                s32     result;

                if ( (CurrCommand == DVD_COMMAND_REQUEST_START_ADDR) ||
                     (CurrCommand == DVD_COMMAND_REQUEST_PLAY_ADDR) )
                {
                    result = (s32)(__DIRegs[DI_IMMBUF_IDX] << 2);
                }
                else
                {
                    result = (s32)__DIRegs[DI_IMMBUF_IDX];
                }

                // clear /executing/ before calling callback.
                // this is because of the check for seeing if command block is in use.
                // if executing is still there, it's hard to check if the dvd API
                // is called in callback.
                finished = executing;
                executing = &DummyCommandBlock;

                finished->state = DVD_STATE_END;
                if (finished->callback)
                {
                    (finished->callback)(result, finished);
                }
                stateReady();
            }
            else if (CurrCommand == DVD_COMMAND_INITSTREAM)
            {
                // currTransferSize has some other meanings
                if (executing->currTransferSize == 0)
                {
                    if (__DIRegs[DI_IMMBUF_IDX] & 1)
                    {
                        // clear /executing/ before calling callback.
                        // this is because of the check for seeing if command block is in use.
                        // if executing is still there, it's hard to check if the dvd API
                        // is called in callback.
                        finished = executing;
                        executing = &DummyCommandBlock;

                        finished->state = DVD_STATE_IGNORED;
                        if (finished->callback)
                        {
                            (finished->callback)(DVD_RESULT_IGNORED, finished);
                        }
                        stateReady();
                    }
                    else
                    {
                        AutoFinishing = FALSE;
                        executing->currTransferSize = 1;
                        DVDLowAudioStream(DRIVE_SUBCMD_ENTRY,
                                          executing->length,
                                          executing->offset,
                                          cbForStateBusy);
                    }
                }
                else
                {
                    // clear /executing/ before calling callback.
                    // this is because of the check for seeing if command block is in use.
                    // if executing is still there, it's hard to check if the dvd API
                    // is called in callback.
                    finished = executing;
                    executing = &DummyCommandBlock;

                    finished->state = DVD_STATE_END;
                    if (finished->callback)
                    {
                        (finished->callback)(0, finished);
                    }
                    stateReady();
                }
            }
            else
            {
                // clear /executing/ before calling callback.
                // this is because of the check for seeing if command block is in use.
                // if executing is still there, it's hard to check if the dvd API
                // is called in callback.
                finished = executing;
                executing = &DummyCommandBlock;

                finished->state = DVD_STATE_END;
                if (finished->callback)
                {
                    (finished->callback)(0, finished);
                }
                stateReady();
            }
        }
        else // DEINT
        {
            // paranoia: BRKINT will be added in the future
            ASSERT( intType == DVD_INTTYPE_DE );

            if (CurrCommand == DVD_COMMAND_INQUIRY)
            {
                // should go to fatal since inquiry should not
                // generate an error
                executing->state = DVD_STATE_FATAL_ERROR;
                stateError(DVD_DE_INT_ERROR_CODE);
                return;
            }

            // Although error has occurred, all the transferred has finished
            // we treat this as successful transaction.
            if ( (USE_DMA(CurrCommand)) &&
                 (executing->transferredSize == executing->length) )
            {
                if (CheckCancel(0))
                {
                    return;
                }

                // clear /executing/ before calling callback.
                // this is because of the check for seeing if command block is in use.
                // if executing is still there, it's hard to check if the dvd API
                // is called in callback.
                finished = executing;
                executing = &DummyCommandBlock;

                finished->state = DVD_STATE_END;
                if (finished->callback)
                {
                    (finished->callback)((s32)finished->transferredSize, finished);
                }
                stateReady();
                return;
            }

            stateGettingError();
        }

    } // if (CurrCommand == DVD_COMMAND_CHANGE_DISK)

} // cbForStateBusy()


void* __DVDGetIssueCommandAddr(void)
{
    return (void*)issueCommand;
}

/*---------------------------------------------------------------------------*
  Name:         issueCommand

  Description:  If no command is being executed, process the command.
                If there is a command, put the command at the end of the
                waiting list.

  Arguments:    prio        priority for waiting queue
                block       pointer to a command block to issue.
                            should be filled in advance.

  Returns:      TRUE if it successfully issued the command.
                FALSE if the command block is already used.
 *---------------------------------------------------------------------------*/
static BOOL issueCommand(s32 prio, DVDCommandBlock* block)
{
    BOOL        level;
    BOOL        result;


    if ( autoInvalidation && USE_DMA(block->command) )
    {
        DCInvalidateRange(block->addr, block->length);
    }

    // disable int to allow reentrance
    level = OSDisableInterrupts();

    // check if the command block is in use
#ifdef _DEBUG
    // It might take time to follow the link in the waiting queue,
    // I check
    // - if the command block == current executing block
    // first. If not, the block's state should be WAITING if it's in
    // the waiting queue, so I check if the state is WAITING or not
    // second and then check if the block is actually in the queue.
    if ( (executing == block) ||
         (block->state == DVD_STATE_WAITING) && (__DVDIsBlockInWaitingQueue(block)) )
    {
        OSPanic(__FILE__, __LINE__, "DVD library: Specified command block (or file info) is already in use\n");
    }
#endif

    block->state = DVD_STATE_WAITING;
    result = __DVDPushWaitingQueue(prio, block);

    if ( (executing == (DVDCommandBlock*)NULL) &&
         (PauseFlag == FALSE) )
    {
        stateReady();
    }

    // restore int
    OSRestoreInterrupts(level);

    return result;
}

/*---------------------------------------------------------------------------*
  Name:         DVDReadAbsAsyncPrio

  Description:  error-handling-level read function.

  Arguments:    block       pointer to a command block for the read
                addr        address to store the data
                length      transfer length
                offset      offset position from the beginning of the disk
                            drive
                callback    callback
                prio        priority

  Returns:      TRUE if it successfully issued the command.
                FALSE if not (e.g. addr is not properly aligned).
 *---------------------------------------------------------------------------*/
BOOL DVDReadAbsAsyncPrio(DVDCommandBlock *block, void* addr, s32 length,
                         s32 offset, DVDCBCallback callback, s32 prio)
{
    BOOL        idle;

    ASSERTMSG(block, DVD_ERR_READABS_NULLBLOCK);
    ASSERTMSG(addr, DVD_ERR_READABS_NULLADDR);
    ASSERTMSG(OFFSET(addr, DVD_ALIGN_ADDR) == 0,
              DVD_ERR_READABS_INVADDR);
    ASSERTMSG(OFFSET(length, DVD_ALIGN_SIZE) == 0,
              DVD_ERR_READABS_INVSIZE);
    ASSERTMSG(OFFSET(offset, DVD_ALIGN_OFFSET) == 0,
              DVD_ERR_READABS_INVOFFSET);
    ASSERTMSG(length >= 0,
              "DVD read: negative value was specified to length of the read\n");

    block->command = DVD_COMMAND_READ;
    block->addr = addr;
    block->length = (u32)length;
    block->offset = (u32)offset;
    block->transferredSize = 0;
    block->callback = callback;

    idle = issueCommand(prio, block);
    ASSERTMSG( idle, DVD_ERR_READABS_BUSY );

    return idle;
}

/*---------------------------------------------------------------------------*
  Name:         DVDSeekAbsAsyncPrio

  Description:  error-handling-level seek function.

  Arguments:    block       pointer to a command block for the seek
                offset      offset position from the beginning of the disk
                            drive
                callback    callback

  Returns:      TRUE if it successfully issued the command.
                FALSE if not (e.g. offset is not four bytes aligned)
 *---------------------------------------------------------------------------*/
BOOL DVDSeekAbsAsyncPrio(DVDCommandBlock *block, s32 offset, DVDCBCallback callback,
                         s32 prio)
{
    BOOL        idle;

    ASSERTMSG(block, DVD_ERR_SEEKABS_NULLBLOCK);
    ASSERTMSG(OFFSET(offset, DVD_ALIGN_OFFSET) == 0,
              DVD_ERR_SEEKABS_INVOFFSET);

    block->command = DVD_COMMAND_SEEK;
    block->offset = (u32)offset;
    block->callback = callback;

    idle = issueCommand(prio, block);
    ASSERTMSG( idle, DVD_ERR_SEEKABS_BUSY );

    return idle;
}

/*---------------------------------------------------------------------------*
  Name:         DVDReadAbsAsyncForBS

  Description:  error-handling-level read function for boot strap

  Arguments:    block       pointer to a command block for the read
                addr        address to store the data
                length      transfer length
                offset      offset position from the beginning of the disk
                            drive
                callback    callback

  Returns:      TRUE if it successfully issued the command.
                FALSE if not (e.g. addr is not properly aligned).

  NOTE:         The difference against the normal read is when errors occur.
                Normal read keeps going to let users to change the disk when
                wrong disk is in, while BS read returns to notify BS that the
                disk is changed.
 *---------------------------------------------------------------------------*/
BOOL DVDReadAbsAsyncForBS(DVDCommandBlock *block, void* addr, s32 length,
                          s32 offset, DVDCBCallback callback)
{
    BOOL        idle;

    ASSERTMSG(block, DVD_ERR_READABSBS_NULLBLOCK);
    ASSERTMSG(addr, DVD_ERR_READABSBS_NULLADDR);
    ASSERTMSG(OFFSET(addr, DVD_ALIGN_ADDR) == 0,
              DVD_ERR_READABSBS_INVADDR);
    ASSERTMSG(OFFSET(length, DVD_ALIGN_SIZE) == 0,
              DVD_ERR_READABSBS_INVSIZE);
    ASSERTMSG(OFFSET(offset, DVD_ALIGN_OFFSET) == 0,
              DVD_ERR_READABSBS_INVOFFSET);

    block->command = DVD_COMMAND_BSREAD;
    block->addr = addr;
    block->length = (u32)length;
    block->offset = (u32)offset;
    block->transferredSize = 0;
    block->callback = callback;

    idle = issueCommand(2, block);
    ASSERTMSG( idle, DVD_ERR_READABSBS_BUSY );

    return idle;
}

/*---------------------------------------------------------------------------*
  Name:         DVDReadDiskID

  Description:  read disk id with error handling

  Arguments:    block       pointer to a command block for the read
                diskID      buffer to read disk id to. Need to be 32byte
                            aligned.
                callback    callback

  Returns:      TRUE if it successfully issued the command.
                FALSE if not (e.g. addr is not properly aligned).
 *---------------------------------------------------------------------------*/
BOOL DVDReadDiskID(DVDCommandBlock *block, DVDDiskID* diskID,
                   DVDCBCallback callback)
{
    BOOL        idle;

    ASSERTMSG(block, DVD_ERR_READID_NULLBLOCK);
    ASSERTMSG(diskID, DVD_ERR_READID_NULLID);
    ASSERTMSG(OFFSET(diskID, DVD_ALIGN_ADDR) == 0,
              DVD_ERR_READID_INVID);

    block->command = DVD_COMMAND_READID;
    block->addr = (void*)diskID;
    block->length = sizeof(DVDDiskID);
    block->offset = DVDLAYOUT_ID_POSITION;
    block->transferredSize = 0;
    block->callback = callback;

    idle = issueCommand(2, block);
    ASSERTMSG( idle, DVD_ERR_READID_BUSY );

    return idle;
}

/*---------------------------------------------------------------------------*
  Name:         DVDPrepareStreamAbsAsync

  Description:  Initialize streaming

  Arguments:    block       pointer to a command block for the read
                length      transfer length
                offset      offset position from the beginning of the disk
                            drive
                callback    callback

  Returns:      TRUE if it successfully issued the command.
                FALSE if not (e.g. addr is not properly aligned).

  Action:       cover open/disk not found ... keeps waiting
                disk change ... check id (if good, init stream, if not, wrong
                                          disk)
 *---------------------------------------------------------------------------*/
BOOL DVDPrepareStreamAbsAsync(DVDCommandBlock *block, u32 length, u32 offset,
                              DVDCBCallback callback)
{
    BOOL        idle;

    block->command = DVD_COMMAND_INITSTREAM;
    block->length = (u32)length;
    block->offset = (u32)offset;
    block->callback = callback;

    idle = issueCommand(1, block);

    return idle;
}

/*---------------------------------------------------------------------------*
  Name:         DVDCancelStreamAsync

  Description:  cancels the current stream track

  Arguments:    block           command block
                callback        callback function

  Returns:      TRUE if it successfully issued the command.
                FALSE if the command block is already used.
 *---------------------------------------------------------------------------*/
BOOL DVDCancelStreamAsync(DVDCommandBlock *block,
                          DVDCBCallback callback)
{
    BOOL        idle;

    block->command = DVD_COMMAND_CANCELSTREAM;
    block->callback = callback;

    idle = issueCommand(1, block);

    return idle;
}

/*---------------------------------------------------------------------------*
  Name:         DVDCancelStream

  Description:  cancels the current stream track synchronously

  Arguments:    block           command block

  Returns:      0 if it successfully issued the command.
                -1 if failed
 *---------------------------------------------------------------------------*/
s32 DVDCancelStream(DVDCommandBlock *block)
{
    BOOL                result;
    s32                 state;
    BOOL                enabled;
    s32                 retVal;

    result = DVDCancelStreamAsync(block, cbForCancelStreamSync);

    // actually, result is always TRUE
    if (result == FALSE)
    {
        return -1;
    }

    enabled = OSDisableInterrupts();

    for (;;)
    {
        state = ( (volatile DVDCommandBlock*)block )->state;

        if ( (state == DVD_STATE_END) || (state == DVD_STATE_FATAL_ERROR)
           || (state == DVD_STATE_CANCELED) )
        {
            retVal = (s32)block->transferredSize;
            break;
        }

        OSSleepThread(&__DVDThreadQueue);
    }

    OSRestoreInterrupts(enabled);
    return retVal;
}

static void cbForCancelStreamSync(s32 result, DVDCommandBlock* block)
{
#pragma unused(block)

    block->transferredSize = (u32)result;

    OSWakeupThread(&__DVDThreadQueue);
}

/*---------------------------------------------------------------------------*
  Name:         DVDStopStreamAtEndAsync

  Description:  Stop the track after it reaches its end

  Arguments:    block           command block
                callback        callback function

  Returns:      TRUE if it successfully issued the command.
                FALSE if the command block is already used.
 *---------------------------------------------------------------------------*/
BOOL DVDStopStreamAtEndAsync(DVDCommandBlock *block,
                             DVDCBCallback callback)
{
    BOOL        idle;

    block->command = DVD_COMMAND_STOP_STREAM_AT_END;
    block->callback = callback;

    idle = issueCommand(1, block);

    return idle;
}

/*---------------------------------------------------------------------------*
  Name:         DVDStopStreamAtEnd

  Description:  Stop the track after it reaches its end

  Arguments:    block           command block

  Returns:      0 if succeeded.
                A negative value if not.
 *---------------------------------------------------------------------------*/
s32 DVDStopStreamAtEnd(DVDCommandBlock *block)
{
    BOOL                result;
    s32                 state;
    BOOL                enabled;
    s32                 retVal;

    result = DVDStopStreamAtEndAsync(block, cbForStopStreamAtEndSync);

    if (result == FALSE)
    {
        return -1;
    }

    enabled = OSDisableInterrupts();

    for (;;)
    {
        state = ( (volatile DVDCommandBlock*)block )->state;

        if ( (state == DVD_STATE_END) || (state == DVD_STATE_FATAL_ERROR)
           || (state == DVD_STATE_CANCELED) )
        {
            retVal = (s32)block->transferredSize;
            break;
        }

        OSSleepThread(&__DVDThreadQueue);
    }

    OSRestoreInterrupts(enabled);
    return retVal;
}

static void cbForStopStreamAtEndSync(s32 result, DVDCommandBlock* block)
{
#pragma unused(block)

    block->transferredSize = (u32)result;

    OSWakeupThread(&__DVDThreadQueue);
}

/*---------------------------------------------------------------------------*
  Name:         DVDGetStreamErrorStatusAsync

  Description:  Get the current audio streaming error status

  Arguments:    block           command block
                callback        callback function

  Returns:      TRUE if it successfully issued the command.
                FALSE if the command block is already used.
 *---------------------------------------------------------------------------*/
BOOL DVDGetStreamErrorStatusAsync(DVDCommandBlock *block, DVDCBCallback callback)
{
    BOOL        idle;

    block->command = DVD_COMMAND_REQUEST_AUDIO_ERROR;
    block->callback = callback;

    idle = issueCommand(1, block);

    return idle;
}

/*---------------------------------------------------------------------------*
  Name:         DVDGetStreamErrorStatus

  Description:  Get the current audio streaming error status

  Arguments:    block           command block

  Returns:      0 if succeeded.
                A negative value if not.
 *---------------------------------------------------------------------------*/
s32 DVDGetStreamErrorStatus(DVDCommandBlock *block)
{
    BOOL                result;
    s32                 state;
    BOOL                enabled;
    s32                 retVal;

    result = DVDGetStreamErrorStatusAsync(block, cbForGetStreamErrorStatusSync);

    if (result == FALSE)
    {
        return -1;
    }

    enabled = OSDisableInterrupts();

    for (;;)
    {
        state = ( (volatile DVDCommandBlock*)block )->state;

        if ( (state == DVD_STATE_END) || (state == DVD_STATE_FATAL_ERROR)
           || (state == DVD_STATE_CANCELED) )
        {
            retVal = (s32)block->transferredSize;
            break;
        }

        OSSleepThread(&__DVDThreadQueue);
    }

    OSRestoreInterrupts(enabled);
    return retVal;
}

static void cbForGetStreamErrorStatusSync(s32 result, DVDCommandBlock* block)
{
#pragma unused(block)

    block->transferredSize = (u32)result;

    OSWakeupThread(&__DVDThreadQueue);
}

/*---------------------------------------------------------------------------*
  Name:         DVDGetStreamPlayAddrAsync

  Description:  Get the currently playing offset

  Arguments:    block           command block
                callback        callback function

  Returns:      TRUE if it successfully issued the command.
                FALSE if the command block is already used.
 *---------------------------------------------------------------------------*/
BOOL DVDGetStreamPlayAddrAsync(DVDCommandBlock *block, DVDCBCallback callback)
{
    BOOL        idle;

    block->command = DVD_COMMAND_REQUEST_PLAY_ADDR;
    block->callback = callback;

    idle = issueCommand(1, block);

    return idle;
}

/*---------------------------------------------------------------------------*
  Name:         DVDGetStreamPlayAddr

  Description:  Get the currently playing offset

  Arguments:    block           command block

  Returns:      0 if succeeded.
                A negative value if not.
 *---------------------------------------------------------------------------*/
s32 DVDGetStreamPlayAddr(DVDCommandBlock *block)
{
    BOOL                result;
    s32                 state;
    BOOL                enabled;
    s32                 retVal;

    result = DVDGetStreamPlayAddrAsync(block, cbForGetStreamPlayAddrSync);

    if (result == FALSE)
    {
        return -1;
    }

    enabled = OSDisableInterrupts();

    for (;;)
    {
        state = ( (volatile DVDCommandBlock*)block )->state;

        if ( (state == DVD_STATE_END) || (state == DVD_STATE_FATAL_ERROR)
           || (state == DVD_STATE_CANCELED) )
        {
            retVal = (s32)block->transferredSize;
            break;
        }

        OSSleepThread(&__DVDThreadQueue);
    }

    OSRestoreInterrupts(enabled);
    return retVal;
}

static void cbForGetStreamPlayAddrSync(s32 result, DVDCommandBlock* block)
{
#pragma unused(block)

    block->transferredSize = (u32)result;

    OSWakeupThread(&__DVDThreadQueue);
}

/*---------------------------------------------------------------------------*
  Name:         DVDGetStreamStartAddrAsync

  Description:  Get the starting address of the current track (if there's a
                queued track, it's of the next track)

  Arguments:    block           command block
                callback        callback function

  Returns:      TRUE if it successfully issued the command.
                FALSE if the command block is already used.
 *---------------------------------------------------------------------------*/
BOOL DVDGetStreamStartAddrAsync(DVDCommandBlock *block, DVDCBCallback callback)
{
    BOOL        idle;

    block->command = DVD_COMMAND_REQUEST_START_ADDR;
    block->callback = callback;

    idle = issueCommand(1, block);

    return idle;
}

/*---------------------------------------------------------------------------*
  Name:         DVDGetStreamStartAddr

  Description:  Get the starting address of the current track (if there's a
                queued track, it's of the next track)

  Arguments:    block           command block

  Returns:      0 if succeeded.
                A negative value if not.
 *---------------------------------------------------------------------------*/
s32 DVDGetStreamStartAddr(DVDCommandBlock *block)
{
    BOOL                result;
    s32                 state;
    BOOL                enabled;
    s32                 retVal;

    result = DVDGetStreamStartAddrAsync(block, cbForGetStreamStartAddrSync);

    if (result == FALSE)
    {
        return -1;
    }

    enabled = OSDisableInterrupts();

    for (;;)
    {
        state = ( (volatile DVDCommandBlock*)block )->state;

        if ( (state == DVD_STATE_END) || (state == DVD_STATE_FATAL_ERROR)
           || (state == DVD_STATE_CANCELED) )
        {
            retVal = (s32)block->transferredSize;
            break;
        }

        OSSleepThread(&__DVDThreadQueue);
    }

    OSRestoreInterrupts(enabled);
    return retVal;
}

static void cbForGetStreamStartAddrSync(s32 result, DVDCommandBlock* block)
{
#pragma unused(block)

    block->transferredSize = (u32)result;

    OSWakeupThread(&__DVDThreadQueue);
}

/*---------------------------------------------------------------------------*
  Name:         DVDGetStreamLengthAsync

  Description:  Get the stream length

  Arguments:    block           command block
                callback        callback function

  Returns:      TRUE if it successfully issued the command.
                FALSE if the command block is already used.
 *---------------------------------------------------------------------------*/
BOOL DVDGetStreamLengthAsync(DVDCommandBlock *block, DVDCBCallback callback)
{
    BOOL        idle;

    block->command = DVD_COMMAND_REQUEST_LENGTH;
    block->callback = callback;

    idle = issueCommand(1, block);

    return idle;
}

/*---------------------------------------------------------------------------*
  Name:         DVDGetStreamLength

  Description:  Get the stream length

  Arguments:    block           command block

  Returns:      0 if succeeded.
                A negative value if not.
 *---------------------------------------------------------------------------*/
s32 DVDGetStreamLength(DVDCommandBlock *block)
{
    BOOL                result;
    s32                 state;
    BOOL                enabled;
    s32                 retVal;

    result = DVDGetStreamLengthAsync(block, cbForGetStreamLengthSync);

    if (result == FALSE)
    {
        return -1;
    }

    enabled = OSDisableInterrupts();

    for (;;)
    {
        state = ( (volatile DVDCommandBlock*)block )->state;

        if ( (state == DVD_STATE_END) || (state == DVD_STATE_FATAL_ERROR)
           || (state == DVD_STATE_CANCELED) )
        {
            retVal = (s32)block->transferredSize;
            break;
        }

        OSSleepThread(&__DVDThreadQueue);
    }

    OSRestoreInterrupts(enabled);
    return retVal;
}

static void cbForGetStreamLengthSync(s32 result, DVDCommandBlock* block)
{
#pragma unused(block)

    block->transferredSize = (u32)result;

    OSWakeupThread(&__DVDThreadQueue);
}

/*---------------------------------------------------------------------------*
  Name:         __DVDAudioBufferConfig

  Description:  Configure audio buffer

  Arguments:    block
                enable          0 to disable, non-0 to enable
                size            size of config buffer. real drive ignores this
                callback

  Returns:      None
 *---------------------------------------------------------------------------*/
void __DVDAudioBufferConfig(DVDCommandBlock *block, u32 enable, u32 size,
                            DVDCBCallback callback)
{
    BOOL        idle;

    // using offset member as enable is a hack, but we don't want to add
    // more member for this only-used-in-bootrom API
    block->command = DVD_COMMAND_AUDIO_BUFFER_CONFIG;
    block->offset  = enable;
    block->length  = size;
    block->callback = callback;

    idle = issueCommand(2, block);

    return;
}

/*---------------------------------------------------------------------------*
  Name:         DVDChangeDiskAsyncForBS

  Description:  error-handling-level change disk function for BS.
                It returns when cover close is detected after motor stop.

  Arguments:    block       pointer to a command block for the function
                callback    callback

  Returns:      TRUE if it successfully issued the command.
                FALSE if not (e.g. addr is not properly aligned).
 *---------------------------------------------------------------------------*/
BOOL DVDChangeDiskAsyncForBS(DVDCommandBlock* block, DVDCBCallback callback)
{
    BOOL            idle;

    ASSERTMSG(block, "DVDChangeDiskAsyncForBS(): null pointer is specified to command block address.");

    block->command = DVD_COMMAND_BS_CHANGE_DISK;
    block->callback = callback;

    idle = issueCommand(2, block);
    ASSERTMSG(idle, "DVDChangeDiskAsyncForBS(): command block is used for processing previous request." );

    return idle;
}

/*---------------------------------------------------------------------------*
  Name:         DVDChangeDiskAsync

  Description:  error-handling-level change disk function.

  Arguments:    block       pointer to a command block for the function
                id          id of the next disk
                callback    callback

  Returns:      TRUE if it successfully issued the command.
                FALSE if not (e.g. addr is not properly aligned).
 *---------------------------------------------------------------------------*/
BOOL DVDChangeDiskAsync(DVDCommandBlock* block, DVDDiskID* id, DVDCBCallback callback)
{
    BOOL            idle;

    ASSERTMSG(block, DVD_ERR_CHANGEDISK_NULLBLOCK);
    ASSERTMSG(id, DVD_ERR_CHANGEDISK_NULLID);

    if (id->company[0] == '\0')
    {
        OSReport("DVDChangeDiskAsync(): You can't specify NULL to company name.  \n");
        OSHalt("");
        // NOT REACHED
    }

    block->command = DVD_COMMAND_CHANGE_DISK;
    block->id = id;
    block->callback = callback;

    DCInvalidateRange(bootInfo->FSTLocation, bootInfo->FSTMaxLength);

    idle = issueCommand(2, block);
    ASSERTMSG( idle, DVD_ERR_CHANGEDISK_BUSY );

    return idle;
}

/*---------------------------------------------------------------------------*
  Name:         DVDChangeDisk

  Description:  error-handling-level change disk function.

  Arguments:    block       pointer to a command block for the function
                id          id of the next disk
                callback    callback

  Returns:      If the function succeeds, it returns 0
                If the function fails, it returns a negative number
 *---------------------------------------------------------------------------*/
s32 DVDChangeDisk(DVDCommandBlock* block, DVDDiskID* id)
{
    BOOL                result;
    s32                 state;
    BOOL                enabled;
    s32                 retVal;

    result = DVDChangeDiskAsync(block, id, cbForChangeDiskSync);

    if (result == FALSE)
    {
        return -1;
    }

    enabled = OSDisableInterrupts();

    for (;;)
    {
        state = ( (volatile DVDCommandBlock*)block )->state;

        if (state == DVD_STATE_END)
        {
            retVal = 0;
            break;
        }
        if (state == DVD_STATE_FATAL_ERROR)
        {
            retVal = DVD_RESULT_FATAL_ERROR;
            break;
        }
        if (state == DVD_STATE_CANCELED)
        {
            retVal = DVD_RESULT_CANCELED;
            break;
        }

        OSSleepThread(&__DVDThreadQueue);
    }

    OSRestoreInterrupts(enabled);
    return retVal;
}

static void cbForChangeDiskSync(s32 result, DVDCommandBlock* block)
{
#pragma unused(block)
#pragma unused(result)

    OSWakeupThread(&__DVDThreadQueue);
}

/*---------------------------------------------------------------------------*
  Name:         DVDInquiryAsync

  Description:  Inquiry the drive (asynchronous version)

  Arguments:    block           command block
                info            address to read drive info
                callback        call back function

  Returns:      TRUE if it successfully issued the command.
                FALSE if the command block is already used.
 *---------------------------------------------------------------------------*/
BOOL DVDInquiryAsync(DVDCommandBlock *block, DVDDriveInfo* info,
                     DVDCBCallback callback)
{
    BOOL        idle;

    ASSERTMSG(block, "DVDInquiry(): Null address was specified for block");
    ASSERTMSG(info, "DVDInquiry(): Null address was specified for info");
    ASSERTMSG(OFFSET(info, DVD_ALIGN_ADDR) == 0,
              "DVDInquiry(): Address for info is not 32 bytes aligned");

    block->command = DVD_COMMAND_INQUIRY;
    block->addr = (void*)info;
    block->length = sizeof(DVDDriveInfo);
    block->transferredSize = 0;
    block->callback = callback;

    idle = issueCommand(2, block);

    return idle;
}

/*---------------------------------------------------------------------------*
  Name:         DVDInquiry

  Description:  Inquiry the drive (synchronous version)

  Arguments:    block           command block
                info            address to read drive info

  Returns:      0 if succeeded.
                A negative value if not.
 *---------------------------------------------------------------------------*/
s32 DVDInquiry(DVDCommandBlock *block, DVDDriveInfo* info)
{
    BOOL                result;
    s32                 state;
    BOOL                enabled;
    s32                 retVal;

    result = DVDInquiryAsync(block, info, cbForInquirySync);

    if (result == FALSE)
    {
        return -1;
    }

    enabled = OSDisableInterrupts();

    for (;;)
    {
        state = ( (volatile DVDCommandBlock*)block )->state;

        if (state == DVD_STATE_END)
        {
            retVal = (s32)block->transferredSize;
            break;
        }
        if (state == DVD_STATE_FATAL_ERROR)
        {
            retVal = DVD_RESULT_FATAL_ERROR;
            break;
        }
        if (state == DVD_STATE_CANCELED)
        {
            retVal = DVD_RESULT_CANCELED;
            break;
        }

        OSSleepThread(&__DVDThreadQueue);
    }

    OSRestoreInterrupts(enabled);
    return retVal;
}

static void cbForInquirySync(s32 result, DVDCommandBlock* block)
{
#pragma unused(result, block)

    OSWakeupThread(&__DVDThreadQueue);
}

/*---------------------------------------------------------------------------*
  Name:         DVDReset

  Description:  reset the drive and set interrupt masks properly

  Arguments:    None

  Returns:      None
 *---------------------------------------------------------------------------*/
void DVDReset(void)
{
    DVDLowReset();
    // enable interrupts except CVRINT
    __DIRegs[DI_SR_IDX] = DI_SR_DEINTMSK_MASK |
                          DI_SR_TCINTMSK_MASK |
                          DI_SR_BRKINTMSK_MASK;
    __DIRegs[DI_CVR_IDX] = __DIRegs[DI_CVR_IDX];

    // Since the drive is reset, no need to reset from outside
    ResetRequired = FALSE;

    ResumeFromHere = 0;
}

BOOL DVDResetRequired(void)
{
    return ResetRequired;
}

s32 DVDGetCommandBlockStatus(const DVDCommandBlock* block)
{
    BOOL                enabled;
    s32                 retVal;

    ASSERTMSG(block,
              DVD_ERR_GETCOMMANDBLOCKSTATUS_NULLBLOCK);

    enabled = OSDisableInterrupts();

    if (block->state == DVD_STATE_COVER_CLOSED)
    {
        retVal = DVD_STATE_BUSY;
    }
    else
    {
        retVal = block->state;
    }

    OSRestoreInterrupts(enabled);

    return retVal;
}

s32 DVDGetDriveStatus(void)
{
    BOOL                enabled;
    s32                 retVal;

    enabled = OSDisableInterrupts();

    // Check to see if pausing or not
    if (FatalErrorFlag)
    {
        retVal = DVD_STATE_FATAL_ERROR;
    }
    else if (PausingFlag)
    {
        retVal = DVD_STATE_PAUSING;
    }
    else
    {
        if (executing == (DVDCommandBlock*)NULL)
        {
            retVal = DVD_STATE_END;
        }
        else if (executing == &DummyCommandBlock)
        {
            // this function is called in a callback
            retVal = DVD_STATE_END;
        }
        else
        {
            retVal = DVDGetCommandBlockStatus(executing);
        }
    }

    OSRestoreInterrupts(enabled);

    return retVal;
}

/*---------------------------------------------------------------------------*
  Name:         DVDSetAutoInvalidation

  Description:  Set the auto invalidation mode

  Arguments:    auto     TRUE to set auto mode, FALSE to set manual mode

  Returns:      previous mode before calling is returned.
 *---------------------------------------------------------------------------*/
BOOL DVDSetAutoInvalidation(BOOL autoInval)
{
    BOOL       prev;

    prev = autoInvalidation;
    autoInvalidation = autoInval;

    return prev;
}

/*---------------------------------------------------------------------------*
  Name:         DVDPause

  Description:  Sets a pause flag. If this flag is set, no new request to
                DVD is made.

  Arguments:    None.

  Returns:      None.
 *---------------------------------------------------------------------------*/
void DVDPause(void)
{
    BOOL        level;

    level = OSDisableInterrupts();

    PauseFlag = TRUE;
    // if there's no outstanding dvd command, the state changes "pausing" here.
    // (if not, the state changes to "pausing" when the current command finishes)
    if (executing == (DVDCommandBlock*)NULL)
        PausingFlag = TRUE;

    OSRestoreInterrupts(level);
}

/*---------------------------------------------------------------------------*
  Name:         DVDResume

  Description:  Resume DVD processing.

  Arguments:    None.

  Returns:      None.
 *---------------------------------------------------------------------------*/
void DVDResume(void)
{
    BOOL        level;

    level = OSDisableInterrupts();

    PauseFlag = FALSE;

    if (PausingFlag)
    {
        PausingFlag = FALSE;
        stateReady();
    }

    OSRestoreInterrupts(level);
}

/*---------------------------------------------------------------------------*
  Name:         DVDCancelAsync

  Description:  Cancel specified command

  Arguments:    block               command block for the command to cancel
                callback            callback function when cancel completes

  Returns:      TRUE when successfully issued cancel. FALSE if not.
                callback for DVDCancelAsync returns 0 for result, while
                callback for canceled command returns DVD_RESULT_CANCELED.
 *---------------------------------------------------------------------------*/
BOOL DVDCancelAsync(DVDCommandBlock* block, DVDCBCallback callback)
{
    BOOL                enabled;
    DVDLowCallback      old;
    DVDCommandBlock*    finished;


    enabled = OSDisableInterrupts();

    switch(block->state)
    {
      case DVD_STATE_FATAL_ERROR:
      case DVD_STATE_END:
      case DVD_STATE_CANCELED:
        if (callback)
            (*callback)(0, block);
        // cancel sequence ends here
        break;

      case DVD_STATE_BUSY:
        if (Canceling)
        {
            OSRestoreInterrupts(enabled);
            return FALSE;
        }

        Canceling = TRUE;
        CancelCallback = callback;

        // If the command is read or bsread, we issue break.
        // Otherwise, just wait for the current command.
        if ( (block->command == DVD_COMMAND_BSREAD) ||
             (block->command == DVD_COMMAND_READ) )
        {
            DVDLowBreak();
        }
        // cancel sequence ends after break completed
        break;

      case DVD_STATE_WAITING:
        __DVDDequeueWaitingQueue(block);
        // it's in waiting state, so no need to back up /executing/
        block->state = DVD_STATE_CANCELED;
        if (block->callback)
            (block->callback)(DVD_RESULT_CANCELED, block);
        if (callback)
            (*callback)(0, block);
        // cancel sequence ends here
        break;

      case DVD_STATE_COVER_CLOSED:
        switch (block->command)
        {
          case DVD_COMMAND_READID:
          case DVD_COMMAND_BSREAD:
          case DVD_COMMAND_AUDIO_BUFFER_CONFIG:
          case DVD_COMMAND_BS_CHANGE_DISK:
            // it's already finished. no need to call callback for the
            // function (block->callback).
            if (callback)
                (*callback)(0, block);
            // cancel sequence ends here
            break;

          default:
            if (Canceling)
            {
                OSRestoreInterrupts(enabled);
                return FALSE;
            }
            Canceling = TRUE;
            CancelCallback = callback;
            break;
        }
        break;

      case DVD_STATE_NO_DISK:
      case DVD_STATE_COVER_OPEN:
      case DVD_STATE_WRONG_DISK:
      case DVD_STATE_MOTOR_STOPPED:
      case DVD_STATE_RETRY:
        // Do not need to wait for cover close int anymore.
        old = DVDLowClearCallback();    // this function masks cover int
        ASSERT(old == cbForStateMotorStopped);
        if (old != cbForStateMotorStopped)
        {
            // probably it's already canceled
            OSRestoreInterrupts(enabled);
            return FALSE;
        }

        if (block->state == DVD_STATE_NO_DISK)
            ResumeFromHere = INTERNALSTATE_NODISK;
        if (block->state == DVD_STATE_COVER_OPEN)
            ResumeFromHere = INTERNALSTATE_COVEROPEN;
        if (block->state == DVD_STATE_WRONG_DISK)
            ResumeFromHere = INTERNALSTATE_WRONG_DISK;
        if (block->state == DVD_STATE_RETRY)
            ResumeFromHere = INTERNALSTATE_RETRY;
        if (block->state == DVD_STATE_MOTOR_STOPPED)
            ResumeFromHere = INTERNALSTATE_MOTOR_STOPPED;

        // Note that /block/=/executing/ in this case.
        // We need to free /executing/ before calling callback
        // so that the /block/ can be used for other request
        // in the callback.
        finished = executing;
        executing = &DummyCommandBlock;

        block->state = DVD_STATE_CANCELED;
        if (block->callback)
        {
            (block->callback)(DVD_RESULT_CANCELED, block);
        }
        if (callback)
        {
            (callback)(0, block);
        }
        stateReady();
        // cancel sequence ends here
        break;
    }

    OSRestoreInterrupts(enabled);

    return TRUE;
}


/*---------------------------------------------------------------------------*
  Name:         DVDCancel

  Description:  Cancel specified command (synchronous version)

  Arguments:    block               command block for the command to cancel

  Returns:      0 if succeeded.
                A negative value if not.
 *---------------------------------------------------------------------------*/
s32 DVDCancel(DVDCommandBlock* block)
{
    BOOL                result;
    s32                 state;
    u32                 command;
    BOOL                enabled;

    result = DVDCancelAsync(block, cbForCancelSync);

    if (result == FALSE)
    {
        return -1;
    }

    enabled = OSDisableInterrupts();

    for (;;)
    {
        state = ( (volatile DVDCommandBlock*)block )->state;

        if ( (state == DVD_STATE_END) ||
             (state == DVD_STATE_FATAL_ERROR) ||
             (state == DVD_STATE_CANCELED) )
        {
            break;
        }

        if (state == DVD_STATE_COVER_CLOSED)
        {
            command = ( (volatile DVDCommandBlock*)block )->command;

            if ( (command == DVD_COMMAND_BSREAD) ||
                 (command == DVD_COMMAND_READID) ||
                 (command == DVD_COMMAND_AUDIO_BUFFER_CONFIG) ||
                 (command == DVD_COMMAND_BS_CHANGE_DISK) )
            {
                break;
            }
        }

        OSSleepThread(&__DVDThreadQueue);
    }

    OSRestoreInterrupts(enabled);
    return 0;
}

static void cbForCancelSync(s32 result, DVDCommandBlock* block)
{
#pragma unused(result, block)
    OSWakeupThread(&__DVDThreadQueue);
}


/*---------------------------------------------------------------------------*
  Name:         DVDCancelAllAsync

  Description:  Cancel all queued commands

  Arguments:    callback            callback function when cancel completes

  Returns:      TRUE when successfully issued cancel. FALSE if not.
                callback for DVDCAncelAllAsync returns 0 for /result/, and
                command block for the current executing (and just canceled)
                request for /block/. If there's no transaction when this
                function is called, NULL is returned for /block/.
 *---------------------------------------------------------------------------*/
BOOL DVDCancelAllAsync(DVDCBCallback callback)
{
    BOOL                enabled;
    DVDCommandBlock*    p;
    BOOL                retVal;

    enabled = OSDisableInterrupts();
    DVDPause();

    while ((p = __DVDPopWaitingQueue()) != 0)
    {
        // DVDCancelAsync should end soon because the states for all commands
        // are WAITING.
        DVDCancelAsync(p, NULL);
        // No need to call callback for each command block because it's
        // already called in DVDCancelAsync().
    }

    if (executing)
        retVal = DVDCancelAsync(executing, callback);
    else
    {
        retVal = TRUE;
        if (callback)
            (*callback)(0, NULL);
    }

    DVDResume();
    OSRestoreInterrupts(enabled);

    return retVal;
}


/*---------------------------------------------------------------------------*
  Name:         DVDCancelAll

  Description:  Cancel all queued commands (synchronous version)

  Arguments:    None.

  Returns:      0 if succeeded.
                A negative value if not.
 *---------------------------------------------------------------------------*/
s32 DVDCancelAll(void)
{
    BOOL                result;
    BOOL                enabled;

    enabled = OSDisableInterrupts();

    // Since there are only one pending DVDCancelAsync that is really
    // asynchronous, we can use the following flag safely to check.
    CancelAllSyncComplete = FALSE;

    result = DVDCancelAllAsync(cbForCancelAllSync);

    if (result == FALSE)
    {
        OSRestoreInterrupts(enabled);
        return -1;
    }

    for (;;)
    {
        if (CancelAllSyncComplete)
            break;

        OSSleepThread(&__DVDThreadQueue);
    }

    OSRestoreInterrupts(enabled);
    return 0;
}

static void cbForCancelAllSync(s32 result, DVDCommandBlock* block)
{
#pragma unused(result, block)
    CancelAllSyncComplete = TRUE;
    OSWakeupThread(&__DVDThreadQueue);
}


/*---------------------------------------------------------------------------*
  Name:         DVDGetCurrentDiskID

  Description:  Get the pointer for the current disk id

  Arguments:    None.

  Returns:      Current disk id address
 *---------------------------------------------------------------------------*/
DVDDiskID* DVDGetCurrentDiskID(void)
{
    return (DVDDiskID*)OSPhysicalToCached(OS_BOOTINFO_ADDR);
}


/*---------------------------------------------------------------------------*
  Name:         DVDCheckDisk

  Description:  Check if the disk has been identified

  Arguments:    None.

  Returns:      TRUE if identified (the disk is the correct disk), FALSE
                if not yet.
 *---------------------------------------------------------------------------*/
BOOL DVDCheckDisk(void)
{
    BOOL                enabled;
    s32                 retVal;
    s32                 state;
    u32                 coverReg;

    enabled = OSDisableInterrupts();

    // Check to see if pausing or not
    if (FatalErrorFlag)
    {
        state = DVD_STATE_FATAL_ERROR;
    }
    else if (PausingFlag)
    {
        state = DVD_STATE_PAUSING;
    }
    else
    {
        if (executing == (DVDCommandBlock*)NULL)
        {
            state = DVD_STATE_END;
        }
        else if (executing == &DummyCommandBlock)
        {
            // this function is called in a callback
            state = DVD_STATE_END;
        }
        else
        {
            // difference from DVDGetCommandBlockStatus()
            // report cover closed as cover closed
            state = executing->state;
        }
    }

    switch (state)
    {
      case DVD_STATE_BUSY:
      case DVD_STATE_IGNORED:   // not happen
      case DVD_STATE_CANCELED:  // not happen
      case DVD_STATE_WAITING:   // not happen
        retVal = TRUE;
        break;

      case DVD_STATE_FATAL_ERROR:
      case DVD_STATE_RETRY:
      case DVD_STATE_MOTOR_STOPPED:
      case DVD_STATE_COVER_CLOSED:
      case DVD_STATE_NO_DISK:
      case DVD_STATE_COVER_OPEN:
      case DVD_STATE_WRONG_DISK:
        retVal = FALSE;
        break;

      case DVD_STATE_END:
      case DVD_STATE_PAUSING:
        coverReg = __DIRegs[DI_CVR_IDX];
        if (DI_CVR_GET_CVRINT(coverReg) || DI_CVR_GET_CVR(coverReg))
        {
            // cover open or closed
            retVal = FALSE;
        }
        else
        {
            if (ResumeFromHere != 0)
            {
                // This means the previous command was canceled
                // after error occurred. The disk is not id'ed.
                retVal = FALSE;
            }
            else
            {
                retVal = TRUE;
            }
        }
    }

    OSRestoreInterrupts(enabled);

    return retVal;
}


void __DVDPrepareResetAsync(DVDCBCallback callback)
{
    BOOL                enabled;

    enabled = OSDisableInterrupts();

    __DVDClearWaitingQueue();

    if (Canceling)
    {
        CancelCallback = callback;
    }
    else
    {
        // so that application's callback doesn't get called
        if (executing)
        {
            executing->callback = NULL;
        }

        DVDCancelAllAsync(callback);
    }

    OSRestoreInterrupts(enabled);
}


/*---------------------------------------------------------------------------*
  Name:         __DVDTestAlarm

  Description:  Check if the specified alarm is used by DVD subsystem or not

  Arguments:    alarm               OSAlarm structure to test

  Returns:      TRUE if the alarm is used by DVD subsystem
 *---------------------------------------------------------------------------*/
BOOL __DVDTestAlarm(const OSAlarm* alarm)
{
    if (alarm == &ResetAlarm)
        return TRUE;
    
    return __DVDLowTestAlarm(alarm);
}
