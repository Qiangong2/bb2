/*---------------------------------------------------------------------------*
  Project:  Dolphin DVD error code store function
  File:     dvderror.c

  Copyright 2001 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: dvderror.c,v $
  Revision 1.1.1.1  2004/01/03 01:02:28  paulm
  Gamecube library source from Nintendo

    
    3     10/30/01 3:00p Hashida
    Added NPDP specific error codes.
    
    2     6/18/01 2:13p Hashida
    Added timeout error.
    
    1     6/15/01 8:51p Hashida
    Initial revision.
    
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#include <dolphin/os.h>
#include <secure/dvdcb.h>
#include <secure/dvdlow.h>
#include <dolphin/dvd.h>
#include <private/OSRtc.h>


static u32 ErrorTable[] = {
    DVD_INTERNALERROR_NO_ERROR,                     // 0
    DVD_INTERNALERROR_COVEROPEN_OR_NODISK,          // 1
    DVD_INTERNALERROR_COVER_CLOSED,                 // 2
    DVD_INTERNALERROR_NO_SEEK_COMPLETE,             // 3
    DVD_INTERNALERROR_UNRECOVERED_READ,             // 4
    DVD_INTERNALERROR_INVALID_COMMAND,              // 5
    DVD_INTERNALERROR_AUDIOBUF_NOT_SET,             // 6
    DVD_INTERNALERROR_LBA_OUT_OF_RANGE,             // 7
    DVD_INTERNALERROR_INVALID_FIELD,                // 8
    DVD_INTERNALERROR_INVALID_AUDIO_COMMAND,        // 9
    DVD_INTERNALERROR_AUDIOBUF_CONFIG_NOT_ALLOWED,  // 10
    DVD_INTERNALERROR_OP_DISK_RM_REQ,               // 11
    DVD_INTERNALERROR_END_OF_USER_AREA,             // 12
    DVD_INTERNALERROR_ID_NOT_READ,                  // 13
    DVD_INTERNALERROR_MOTOR_STOPPED,                // 14
    DVD_INTERNALERROR_PROTOCOL_ERROR,               // 15
    DVD_INTERNALERROR_NPDP_HARDDISKREADERROR,       // 16
    0,                                              // 17 (for other NPDP errors)

    // Max 28 (29 means "didn't match")
};

#define DE_INT_ERROR                255
#define TIMEOUT_ERROR               254

#define ERROR_CODE_MAX              30
#define OTHER_NPDP_ERRORS           17
#define DIDNT_MATCH                 (ERROR_CODE_MAX - 1)


static u8 ErrorCode2Num(u32 errorCode)
{
    u32                 i;
    
    for (i = 0; i < sizeof(ErrorTable)/sizeof(ErrorTable[0]); i++)
    {
        if (ErrorTable[i] == errorCode)
        {
            ASSERT(i < DIDNT_MATCH);
            return (u8)i;
        }
    }

    if ( (errorCode >= DVD_INTERNALERROR_NPDP_MIN) &&
         (errorCode <= DVD_INTERNALERROR_NPDP_MAX) )
    {
        // Shouldn't happen; NPDP cartridge errors other than 
        // HDD read error
        return OTHER_NPDP_ERRORS;
    }

    return DIDNT_MATCH;
}

static u8 Convert(u32 error)
{
    u32                 statusCode;
    u32                 errorCode;
    u8                  errorNum;

    if (error == DVD_DE_INT_ERROR_CODE)
        return DE_INT_ERROR;

    if (error == DVD_TIMEOUT_ERROR_CODE)
        return TIMEOUT_ERROR;

    statusCode = (error & DVD_INTERNALERROR_STATUSMASK) >> 24;
    errorCode = error & DVD_INTERNALERROR_ERRORMASK;

    errorNum = ErrorCode2Num(errorCode);
    if (statusCode >= 6)
        statusCode = 6;

    return (u8)(statusCode * ERROR_CODE_MAX + errorNum);
}

void __DVDStoreErrorCode(u32 error)
{
    OSSramEx*           sram;
    u8                  num;
    
    num = Convert(error);

    sram = __OSLockSramEx();
    sram->dvdErrorCode = num;
    __OSUnlockSramEx(TRUE);    
}
