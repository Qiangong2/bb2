/****************************************************************************
 * 
 *   (C) 2000 ARTX INC..  ALL RIGHTS RESERVED.  UNPUBLISHED -- RIGHTS
 *   RESERVED UNDER THE COPYRIGHT LAWS OF THE UNITED STATES.  USE OF A
 *   COPYRIGHT NOTICE IS PRECAUTIONARY ONLY AND DOES NOT IMPLY PUBLICATION
 *   OR DISCLOSURE.
 * 
 *   THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF
 *   ARTX INC..  USE, DISCLOSURE, OR REPRODUCTION IS PROHIBITED WITHOUT
 *   THE PRIOR EXPRESS WRITTEN PERMISSION OF ARTX INC..
 * 
 *                   RESTRICTED RIGHTS LEGEND
 * 
 *   Use, duplication, or disclosure by the Government is subject to
 *   restrictions as set forth in subparagraph (c)(1)(ii) of the Rights
 *   in Technical Data and Computer Software clause at DFARS 252.227-7013
 *   or subparagraphs (c)(1) and (2) of Commercial Computer Software --
 *   Restricted Rights at 48 CFR 52.227-19, as applicable. 
 * 
 *   ArtX Inc.
 *   3400 Hillview Ave, Bldg 5
 *   Palo Alto, CA 94304
 * 
 ***************************************************************************/

// For Orca and Drip systems
#if ORCA

#include <dolphin/os.h>
#include <stdarg.h>
#include <private/exi.h>
#include "i2c.h"

#define GPIO_PINS       0x800404
#define GPIO_OE         0x800408

//
// Curently there are 4 output lines defined for Barnacle on Orca/Drip
// 2 are for the I2C clock and data, 1 is to reset the optional philips
// AV card, and 1 is to enable the I2C lines by telling Marlin not to drive
// those lines.
// For Drip, another gpio output goes to the reset of the Drip memory
// controller.
//
#define GPIO_SCL_OUT	0x02
#define GPIO_SDA_OUT	0x01
#define GPIO_SCL_IN	0x02
#define GPIO_SDA_IN	0x01

#define GPIO_RESET_VID	0x04
#define GPIO_ENABLE_I2C 0x10

#if DRIP
#define GPIO_MEM_NRESET	0x40
#endif

static u8 shadowGPIOOE;
static u8 shadowGPIOData;

/* prototypes for this file */
static void initGpioExi();
static void setVideoReset(int value);
static void setI2CEnable(int value);
static int gpioOutput(u8);
static int gpioOutput(u8);
static int gpioOE(u8);
static int gpioOut(u32, u8);
static int gpioInput(u8 *);

//
// I2C operation:
// Reset the two I2C output bits
// Reset the two I2C output enable bits, should have pullups
// To set either the clock or data, you simply change the corresponding
// output enable bit.  Enabling the output will drive it low, turning the
// output enable bit off will allow the line to float high.  This is how
// I2C works.
// 
// On Drip, bit 6 is the reset for the memory controller, the
// prom has it high and the OE is on.  Don't mess with this bit on drips
//

void __VIInitI2C(void)
{
    OSTime          time;

    initGpioExi();
    // toggle the reset line
    setVideoReset(0);
    time = OSGetTime();
    while (OSGetTime() - time < OSMicrosecondsToTicks(100))
        ;

    setVideoReset(1);
    setI2CEnable(1);
}


static void initGpioExi()
{
    // EXIInit() is called in OSInit.

#ifdef DRIP
    shadowGPIOOE = GPIO_MEM_NRESET;
    shadowGPIOData = GPIO_MEM_NRESET; 
#else
    shadowGPIOOE = 0;
    shadowGPIOData = 0;
#endif
    gpioOutput(shadowGPIOData);
    gpioOE(shadowGPIOOE);
}

void __VISetSCL(int value)
{
    // if off, then enable the output of zero
    shadowGPIOOE &= ~GPIO_SCL_OUT;
    if (!value)
        shadowGPIOOE |= GPIO_SCL_OUT;

    // now program via Exi
    gpioOE(shadowGPIOOE);
}

int __VIGetSCL(void)
{
    u8 value;

    // get the data from Exi
    gpioInput(&value);

    if (value & GPIO_SCL_IN)
        return 1;
    else
        return 0;
}

void __VISetSDA(int value)
{
    // if off, then enable the output of zero
    shadowGPIOOE &= ~GPIO_SDA_OUT;
    if (!value)
        shadowGPIOOE |= GPIO_SDA_OUT;

    // now program via Exi
    gpioOE(shadowGPIOOE);
}

int __VIGetSDA(void)
{
    u8 value;

    // get the data from Exi
    gpioInput(&value);

    if (value & GPIO_SDA_IN)
        return 1;
    else
        return 0;
}

//
// 0 - reset
// 1 - enable
//
static void setVideoReset(int value)
{
    if (value) {
        shadowGPIOData |= GPIO_RESET_VID;
    } else {
        shadowGPIOData &= ~GPIO_RESET_VID;
    }
    shadowGPIOOE |= GPIO_RESET_VID;

    gpioOutput(shadowGPIOData);
    gpioOE(shadowGPIOOE);
}

static void setI2CEnable(int value)
{

    if (value) {
        shadowGPIOData &= ~GPIO_ENABLE_I2C;
    } else {
        shadowGPIOData |= GPIO_ENABLE_I2C;
    }
    shadowGPIOOE |= GPIO_ENABLE_I2C;

    gpioOutput(shadowGPIOData);
    gpioOE(shadowGPIOOE);
}


static int gpioOutput(u8 value)
{
    return gpioOut(GPIO_PINS, value);
}

static int gpioOE(u8 value)
{
    return gpioOut(GPIO_OE, value);
}

static int gpioOut(u32 addr, u8 value)
{
    u32  cmd;

    cmd = (addr | 0x2000000) << 6;        // write bit XXX put in fiddle

    // Can't share the bus
    if (!EXILock(0, 1, 0))
    {
        return I2C_FAILURE;
    }
    if ( EXISelect(0, 1, EXI_FREQ_16M) == FALSE ) {
        EXIUnlock(0);
        return I2C_FAILURE;
    }
    
    EXIImm(0, (u8 *)&cmd, 4, EXI_WRITE, NULL);
    EXISync(0);

    cmd = (u32)value << 24;
    EXIImm(0, (u8 *)&cmd, 1, EXI_WRITE, NULL);
    EXISync(0);

    EXIDeselect(0);
    EXIUnlock(0);

    return I2C_SUCCESS;
}

static int
gpioInput(u8 *p)
{
    u32 cmd;

    // Can't share the bus
    if (!EXILock(0, 1, 0))
    {
        return I2C_FAILURE;
    }
    if ( EXISelect(0, 1, EXI_FREQ_16M) == FALSE ) {
        EXIUnlock(0);
        return I2C_FAILURE;
    }

    // send a read command
    // Reads require a dead cycle...
    cmd = GPIO_PINS << 6;
    EXIImm(0, (u8 *)&cmd, 4, EXI_WRITE, NULL);
    EXISync(0);

    EXIImm(0, (u8 *)&cmd, 1, EXI_READ, NULL);
    EXISync(0);
    EXIDeselect(0);
    EXIUnlock(0);

    *p = (u8)((cmd >> 24) & 0xff);
    return I2C_SUCCESS;
}

#endif // ORCA
