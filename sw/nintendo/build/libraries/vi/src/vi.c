/*---------------------------------------------------------------------------*
  Project:  Dolphin OS
  File:     vi.c

  Copyright 1998, 1999, 2000 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: vi.c,v $
  Revision 1.1.1.1  2004/01/03 01:02:28  paulm
  Gamecube library source from Nintendo

    
    84    03/03/07 18:26 Yasu
    Added __VIGetLatchPosition
    
    83    03/03/04 9:56 Yasu
    Added ray-gun control functions
    
    82    03/02/27 16:33 Hashida
    Added hct/vct -> x/y conversion function.
    
    81    03/02/27 11:43 Hashida
    Added position interrupt functions.
    
    80    8/07/02 21:45 Shiki
    Defined DOLPHIN_LIB_VERSION() and registered the library with
    OSRegisterVersion().

    79    02/08/05 23:02 Hashida
    Modified so that people can call VIInit multiple times.

    78    8/05/02 15:53 Shiki
    Added const keyword to the VIConfigure() function prototype.

    77    02/07/02 21:44 Hashida
    Updated to support new video spec for TriForce.

    76    02/04/30 22:05 Hashida
    Fixed a bug that when restarted while the mode is EURGB60, it
    mistakenly returns NTSC when VIGetTvFormat is called before
    VIConfigure.
    Modified so that it checks unreasonable mode change between NTSC <->
    PAL.

    75    02/03/14 18:49 Hashida
    Added VIGetNextFrameBuffer and VIGetCurrentFrameBuffer.

    74    02/02/12 11:59 Hashida
    Fixed a bug.

    73    02/02/06 17:47 Hashida
    Added support for JAMMA video standard.

    72    11/26/01 9:43 Shiki
    Modified using SIRefreshSamplingRate() instead of
    __PADRefreshSamplingRate().

    71    01/11/16 13:11 Hashida
    Changed PAL noninterlace from 313 lines -> 312 lines.

    70    01/11/12 22:32 Hashida
    Fixed for MPAL register setting problem.

    69    01/11/10 12:44 Hashida
    3D mode.

    68    11/02/01 5:32p Hashida
    Renamed eu60 -> eurgb60

    67    11/01/01 3:35p Hashida
    Added EURGB60 mode support.

    66    10/18/01 6:38p Hashida
    Removed EU60

    65    10/12/01 2:48a Hashida
    Added EU60 mode.

    64    10/01/01 2:51p Hashida
    Support for sampling rate on progressive mode.

    63    9/19/01 12:47a Hashida
    Cleaned code.

    62    9/19/01 12:25a Hashida
    Added DEBUG_PAL (NTSC 50Hz).

    61    9/04/01 1:35p Hashida
    Fixed a bug that VIConfigure that changes VI mode after VIFlush can
    cause "color alternating".

    60    7/20/01 10:10a Hashida
    Lower the image by two lines in progressive mode to avoid using the
    line 41.

    59    7/05/01 11:19p Hashida
    Changed to use NIN=1 on progressive mode.

    58    6/25/01 3:27p Hashida
    Fixed a bug in black mode in progressive mode.

    57    6/25/01 1:16p Hashida
    Merged Urata@IRD's modification regarding progressive mode support for
    VIConfigure and adding VIGetDTVStatus.

    56    5/07/01 10:47p Hashida
    Changed so that NTSC games can be shown as MPAL on MPAL bootrom.

    55    1/31/01 12:41a Hashida
    Always set encoder type as "ROHM" if not on MINNOW.

    54    12/18/00 8:09p Shiki
    displayOffsetV is no longer saved in OSSram.

    53    10/31/00 2:51p Hashida
    Fixed code for 16MB over frame buffer address support.

    52    10/25/00 10:22a Hashida
    Changed so that if BUG_16MB_XFB is not defined, frb addr is not shifted
    if the address is lower than 16MB.

    51    10/23/00 3:46p Shiki
    Modified getEncoderType() to ignore DI_CONFIG if console is a retail
    version.

    50    10/18/00 5:37p Hashida
    Bug fix for the previous change; we need to shift 5 bits for all four
    frame buffer registers for HW2.

    49    10/18/00 5:29p Hashida
    Added higher frame buffer address support for HW2

    48    10/05/00 10:59p Hashida
    Changed so that adjusting works even when dispSizeY is 480.

    47    9/16/00 2:55a Hashida
    Added VI adjusting feature.

    46    9/11/00 5:47p Shiki
    Modified getEncoderType() to ignore DI_CONFIG if console is RETAIL1.

    45    8/11/00 6:54p Hashida
    Fixed a minor bug.

    44    8/11/00 6:25p Hashida
    Added 3D and progressive mode support

    43    8/03/00 4:13p Alligator
    update header files for Rev B hardware

    42    7/14/00 10:01a Hashida
    Fixed values for filter taps.

    41    6/30/00 7:17p Hashida
    Added philips 10bit encoder support for Orca
    Clean up.

    40    6/30/00 4:33p Carl
    Fixed retrace handler for SPRUCE.

    39    6/29/00 4:04p Hashida
    Added a friendly assertion message for VI 16MB framebuffer restriction.

    38    6/29/00 2:25p Hashida
    Added more friendly warning message for those who forgot to call
    VIFlush().

    37    6/16/00 6:21p Hashida
    Fixed a problem of reading two registers non-atomicly.

    36    5/18/00 10:57a Hashida
    Removed warnings.

    35    5/15/00 1:39p Hashida
    Changed maximum height from 482 to 480 for NTSC and MPAL.

    34    5/01/00 1:17p Carl
    Fixed omitted declaration.

    33    4/28/00 4:25p Carl
    Changed set callback routines to return old callback.

    32    4/12/00 5:09p Hashida
    Made __VIInit public so that BS2 can call this.

    31    2/29/00 6:34p Hashida
    Removed unnecessary call for getCurrentLine.
    Revised VIFlush so that it only updates the change so far.

    30    2/28/00 2:08p Carl
    TIAN/HASHIDA: changed VIRetraceHandler to always create new context
    before calling OSWakeupThread

    29    2/25/00 11:33a Hashida
    Added VISetPreRetraceCallback and VISetPostRetraceCallback.
    Added an assertion check in VIConfigure.

    28    2/17/00 4:17p Hashida
    Added VIGetTvFormat

    27    2/16/00 4:36p Hashida
    Fixed a bug that some APIs didn't disable interrupts when needed.

    26    2/14/00 4:53p Shiki
    Fixed __VIRetraceHandler() to clear the exception context before
    returning to the caller.

    25    2/14/00 12:28p Hashida
    Added assertions regarding dispPosX and dispPosX + dispSizeX because of
    hbe and hbs restriction (hbe should be <= hlw, hbs should be >= 0).

    24    2/11/00 7:04p Hashida
    Added several argument checks.

    23    2/09/00 6:23p Hashida
    Added initialization code for thread queue.

    22    2/03/00 5:11p Hashida
    Code cleanup.
    Fixed a minor bug.
    Changed because __VIInit() should not access HorVer (in other words,
    VIInit() should not rely on variables that __VIInit() sets).


    21    1/31/00 2:53p Hashida
    Changed so that register set is done in VI default callback.

    20    1/28/00 11:55p Hashida
    Bug fix (if application only calls VISetNextFrameBuffer and doesn't
    call VIConfigure at all to use the default settings, the previous
    version had trouble).

    19    1/28/00 11:01p Hashida
    Added VIFlush().

    18    1/28/00 4:18p Hashida
    Implemented a work around for mode change.

    17    1/28/00 1:19a Hashida
    Changed so that registers are updated in ISR.

    16    1/26/00 6:31p Hashida
    Changed VIGetNextField to return above/below field instead of even/odd.

    15    1/26/00 3:58p Hashida
    Merged VIConfigureTVScreen and VIConfigureXFrameBuffer to one API
    (VIConfigure).

    14    1/18/00 10:43a Shiki
    Implemented basic thread API.

    13    1/18/00 10:33a Hashida
    Fixed for non-interlace mode. (there was a bug when calculate DispPosY)

    12    1/17/00 6:53p Shiki
    Removed OSLoadContext() from the interrupt handler (for thread
    support).

    11    1/15/00 2:59a Hashida
    Fixed a bug that the image is not properly shown when it's placed with
    odd position Y.

    10    1/13/00 1:36a Hashida
    Changed VIGetCurrentLine so that it starts from 0.
    Code cleanup.

    9     1/12/00 9:02p Hashida
    Changed so that the interrupt occurs at the beginning of every frame,
    not at the end of every active video.

    8     1/12/00 6:26p Hashida
    #ifdef'd out I2C stuffs so that they are not compiled for SPRUCE build

    7     1/12/00 2:23p Hashida
    Made compliant with Rohm's encoder.

    6     1/04/00 5:54p Hashida
    Changed so that configuring i2c stuffs is only for minnow builds.

    5     12/24/99 12:33a Hashida
    Changed the initial state after VIInit() to enable black.
    Made black more stable.

    4     12/23/99 10:52p Hashida
    Fixed various problems.

    3     12/17/99 11:07a Hashida
    Fixed many bugs. Added i2c code from ArtX.

    2     12/03/99 2:46p Hashida
    Added CCIR656 support

    18    11/17/99 5:03p Hashida
    Fixed a minor mistake.

    17    11/17/99 3:32p Hashida
    Changed so that caller can specify DEBUG mode.
    When MINNOW, changed DEBUG mode as a default.

    16    11/12/99 5:20p Hashida
    Added VIGetCurrentLine().
    Coded real code for VIGetNextField().

    15    11/04/99 8:08p Tian
    Installed PI_VI interrupt handler BEFORE unmasking the interrupt.

    14    10/07/99 4:34p Shiki
    Revised VIWaitForRetrace() to call OSSched() to support background
    task.

    13    10/07/99 10:25a Hashida
    Moved the part that changes the registers from retrace handler to API.

    12    10/05/99 4:51p Hashida
    Added X scaling.

    11    10/04/99 6:28p Hashida
    Call __VIInit automatically.

    10    10/04/99 2:19p Hashida
    Minor changes and small bug fixes.

    9     10/04/99 10:21a Hashida
    Changed because OS interrupt handling is changed to handle VI as PI_VI

    8     10/04/99 9:49a Hashida
    Added many real code.

    7     9/29/99 6:14p Hashida
    Added some real code for VIGetRetraceCount, VISetNextFrameBuffer and
    VIConfigreTVScreen.

    6     9/29/99 12:02p Hashida
    Made VIWaitForRetrace work.
    Changed the name __VIInterruptHandler -> __VIRetraceHandler since this
    function will not support other VI line interrupts.

    5     9/28/99 2:02p Hashida
    Made VI interrupt handler.

    4     9/28/99 12:26p Hashida
    Added SOME real code to VIInit()

    3     9/16/99 11:08a Hashida
    Removed VISetRetraceCount stub.

    2     9/15/99 9:09p Hashida
    Changed a function name (VIGetCurrentFieldCount -> VIGetRetraceCount).
    Added VIGetNextField.

    1     9/14/99 6:01p Hashida
    Initial revision. Only stubs are here.
 *---------------------------------------------------------------------------*/

#include <dolphin/doldefs.h>
DOLPHIN_LIB_VERSION(VI);
/* $NoKeywords: $ */

// Dolphin header files
#include <dolphin/os.h>
#include <dolphin/vi.h>
#include <dolphin/gx.h>
#include <private/flipper.h>
#include <private/osLoMem.h>
#include <private/viprivate.h>
#include <private/vi3d.h>
#include <private/OSRtc.h>
#include <secure/gcam.h>
#include "vilocal.h"

void __VIInit(VITVMode mode);
static BOOL VISetRegs(void);

static BOOL         IsInitialized = FALSE;

static void __VIRetraceHandler(__OSInterrupt interrupt, OSContext* context);
static volatile u32     retraceCount;
static volatile u32     flushFlag;
static OSThreadQueue    retraceQueue;   // list of threads waiting for the next vsync
static VIRetraceCallback    PreCB;  // pre retrace call back
static VIRetraceCallback    PostCB;  // post retrace call back
static VIPositionCallback   PositionCallback = NULL;


static u32 getCurrentHalfLine(void);
static u32 getCurrentFieldEvenOdd(void);
static u32  getEncoderType(void);
static u32  encoderType;

static s16 displayOffsetH = 0;     // horizontal offset for VI.
static s16 displayOffsetV = 0;     // vertical offset for VI.

#ifndef SPRUCE
extern void __VIInitPhilips(void);
#endif

#define VI_NUMREGS      (sizeof(__VIRegs)/sizeof(__VIRegs[0]))
#define ToPhysical(fb)  (u32)(((u32)(fb)) & 0x3fffffff)

static vu32             changeMode = 0;
static vu64             changed = 0;
static u16              regs[VI_NUMREGS];

static vu32             shdwChangeMode = 0;
static vu64             shdwChanged = 0;
static u16              shdwRegs[VI_NUMREGS];

static HorVer_s         HorVer;
static timing_s*        CurrTiming;
static u32              CurrTvMode;

static u32              NextBufAddr;
static u32              CurrBufAddr;

static u32             FBSet = 0;

#define ONES(x)        ( (1 << (x)) - 1 )

#define INT_0          1
#define INT_1          2
#define INT_2          4
#define INT_3          8

#define ENABLE          1
#define DISABLE         0


#ifndef SPRUCE
static u32 getEncoderType(void)
{
#ifndef MINNOW
    return VIDEO_ENCODER_ROHM;
#else
    // MINNOW build only
    vu32    value;

    // If jumper J38 B on DI config then it is the ROHM encoder
    // bit is inverted, so value of 1 (2) is philips, value of 0 is rohm
    value = __DIRegs[DI_CONFIG_IDX];
    if (value & VIDEO_ENCODER_JUMPER)
        return VIDEO_ENCODER_PHILIPS;       // No jumper installed
    else
        return VIDEO_ENCODER_ROHM;      // Jumper installed
#endif

}
#endif

static timing_s timing[] = {

    // 0:VITimingNtscInt
{
    6,       //    equ
    240,     //    acv

    24,      //    prbOdd
    25,      //    prbEven

    3,       //    psbOdd
    2,       //    psbEven

    12,      //    bs1
    13,      //    bs2
    12,      //    bs3
    13,      //    bs4

    520,     //    be1
    519,     //    be2
    520,     //    be3
    519,     //    be4

    525,     //    nhlines

    429,     //    hlw
    64,      //    hsy
    71,      //    hcs
    105,     //    hce
    162,     //    hbe640
    373,     //    hbs640
    122,     //    hbeCCIR656
    412,     //    hbsCCIR656
},
    // 1:VITimingNtscDs
{
    6,       //    equ
    240,     //    acv

    24,      //    prbOdd
    24,      //    prbEven

    4,       //    psbOdd
    4,       //    psbEven

    12,      //    bs1
    12,      //    bs2
    12,      //    bs3
    12,      //    bs4

    520,     //    be1
    520,     //    be2
    520,     //    be3
    520,     //    be4

    526,     //    nhlines

    429,     //    hlw
    64,      //    hsy
    71,      //    hcs
    105,     //    hce
    162,     //    hbe640
    373,     //    hbs640
    122,     //    hbeCCIR656
    412,     //    hbsCCIR656
},
    // 2:VITimingPalInt
{
    5,       //    equ
    287,     //    acv

    35,      //    prbOdd
    36,      //    prbEven

    1,       //    psbOdd
    0,       //    psbEven

    13,      //    bs1
    12,      //    bs2
    11,      //    bs3
    10,      //    bs4

    619,     //    be1
    618,     //    be2
    617,     //    be3
    620,     //    be4

    625,     //    nhlines

    432,     //    hlw
    64,      //    hsy
#if 1
    75,      //    hcs
    106,     //    hce
#else
    71,      //    hcs
    105,     //    hce
#endif
#if 1
    172,     //    hbe640
    380,     //    hbs640
#else
    162,     //    hbe640
    373,     //    hbs640
#endif
    133,     //    hbeCCIR656
    420,     //    hbsCCIR656
},
    // 3:VITimingPalDs
{
    5,       //    equ
    287,     //    acv

    33,      //    prbOdd
    33,      //    prbEven

    2,       //    psbOdd
    2,       //    psbEven

    13,      //    bs1
    11,      //    bs2
    13,      //    bs3
    11,      //    bs4

    619,     //    be1
    621,     //    be2
    619,     //    be3
    621,     //    be4

    624,     //    nhlines

    432,     //    hlw
    64,      //    hsy
#if 1
    75,      //    hcs
    106,     //    hce
#else
    71,      //    hcs
    105,     //    hce
#endif
#if 1
    172,     //    hbe640
    380,     //    hbs640
#else
    162,     //    hbe640
    373,     //    hbs640
#endif
    133,     //    hbeCCIR656
    420,     //    hbsCCIR656
},
    // 4:VITimingMpalInt
{
    6,       //    equ
    240,     //    acv

    24,      //    prbOdd
    25,      //    prbEven

    3,       //    psbOdd
    2,       //    psbEven

    16,      //    bs1
    15,      //    bs2
    14,      //    bs3
    13,      //    bs4

    518,     //    be1
    517,     //    be2
    516,     //    be3
    519,     //    be4

    525,     //    nhlines

    429,     //    hlw
    64,      //    hsy
    78,      //    hcs
    112,     //    hce
    162,     //    hbe640
    373,     //    hbs640
    122,     //    hbeCCIR656
    412,     //    hbsCCIR656
},
    // 5:VITimingMpalDs
{
    6,       //    equ
    240,     //    acv

    24,      //    prbOdd
    24,      //    prbEven

    4,       //    psbOdd
    4,       //    psbEven

    16,      //    bs1
    14,      //    bs2
    16,      //    bs3
    14,      //    bs4

    518,     //    be1
    520,     //    be2
    518,     //    be3
    520,     //    be4

    526,     //    nhlines

    429,     //    hlw
    64,      //    hsy
    78,      //    hcs
    112,     //    hce
    162,     //    hbe640
    373,     //    hbs640
    122,     //    hbeCCIR656
    412,     //    hbsCCIR656
},
    // 6:VITimingNtscPro
{
    12,      //    equ
    480,     //    acv

    48,      //    prbOdd
    48,      //    prbEven

    6,       //    psbOdd
    6,       //    psbEven

    24,      //    bs1
    24,      //    bs2
    24,      //    bs3
    24,      //    bs4

    1038,    //    be1
    1038,    //    be2
    1038,    //    be3
    1038,    //    be4

    1050,    //    nhlines

    429,     //    hlw
    64,      //    hsy
    71,      //    hcs
    105,     //    hce
    162,     //    hbe640
    373,     //    hbs640
    122,     //    hbeCCIR656
    412,     //    hbsCCIR656
},
    // 7:VITimingNtsc3D
{
    12,      //    equ
    480,     //    acv

    44,      //    prbOdd
    44,      //    prbEven

    10,      //    psbOdd
    10,      //    psbEven

    24,      //    bs1
    24,      //    bs2
    24,      //    bs3
    24,      //    bs4

    1038,    //    be1
    1038,    //    be2
    1038,    //    be3
    1038,    //    be4

    1050,    //    nhlines

    429,     //    hlw
    64,      //    hsy
    71,      //    hcs
    105,     //    hce
    168,     //    hbe640
    379,     //    hbs640
    122,     //    hbeCCIR656
    412,     //    hbsCCIR656
},
    // 8:VITiming15k
{
    6,       //    equ
    241,     //    acv

    24,      //    prbOdd
    25,      //    prbEven

    1,       //    psbOdd
    0,       //    psbEven

    12,      //    bs1
    13,      //    bs2
    12,      //    bs3
    13,      //    bs4

    520,     //    be1
    519,     //    be2
    520,     //    be3
    519,     //    be4

    525,     //    nhlines

    429,     //    hlw
    64,      //    hsy
    71,      //    hcs
    105,     //    hce
    159,     //    hbe640
    370,     //    hbs640
    122,     //    hbeCCIR656
    412,     //    hbsCCIR656
},
    // 9:VITiming31k
{
    12,      //    equ
    480,     //    acv

    48,      //    prbOdd
    48,      //    prbEven

    6,       //    psbOdd
    6,       //    psbEven

    24,      //    bs1
    24,      //    bs2
    24,      //    bs3
    24,      //    bs4

    1038,    //    be1
    1038,    //    be2
    1038,    //    be3
    1038,    //    be4

    1050,    //    nhlines

    429,     //    hlw
    64,      //    hsy
    71,      //    hcs
    105,     //    hce
    180,     //    hbe640
    391,     //    hbs640
    122,     //    hbeCCIR656
    412,     //    hbsCCIR656
}
};

static u16 taps[] = {
    0x1f0,  /*  0: 0.96899223 in U1.9 */
    0x1dc,  /*  1: 0.93129772 in U1.9 */
    0x1ae,  /*  2: 0.84210527 in U1.9 */
    0x174,  /*  3: 0.72932333 in U1.9 */
    0x129,  /*  4: 0.58208954 in U1.9 */
    0x0db,  /*  5: 0.42857143 in U1.9 */
    0x08e,  /*  6: 0.27819550 in U1.9 */
    0x046,  /*  7: 0.13740458 in U1.9 */
    0x0c,   /*  8: 0.02325581 in S-2.9 */
    0xe2,   /*  9: -0.06106870 in S-2.9 */
    0xcb,   /* 10: -0.10526316 in S-2.9 */
    0xc0,   /* 11: -0.12781955 in S-2.9 */
    0xc4,   /* 12: -0.11940299 in S-2.9 */
    0xcf,   /* 13: -0.09774436 in S-2.9 */
    0xde,   /* 14: -0.06766917 in S-2.9 */
    0xec,   /* 15: -0.03816794 in S-2.9 */
    0xfc,   /* 16: -0.00775194 in S-2.9 */
    0x08,   /* 17: 0.01526718 in S-2.9 */
    0x0f,   /* 18: 0.03007519 in S-2.9 */
    0x13,   /* 19: 0.03759398 in S-2.9 */
    0x13,   /* 20: 0.03731343 in S-2.9 */
    0x0f,   /* 21: 0.03007519 in S-2.9 */
    0x0c,   /* 22: 0.02255639 in S-2.9 */
    0x08,   /* 23: 0.01526718 in S-2.9 */
    0x01,   /* 24: Hardwired to zero */
};

static s32 cntlzd(u64 bit)
{
    u32     hi;
    u32     lo;
    s32     value;

    hi = (u32)(bit >> 32);
    lo = (u32)(bit & 0xffffffff);

    value = __cntlzw(hi);
    if (value < 32)
        return value;

    return (32 + __cntlzw(lo));
}

static BOOL VISetRegs(void)
{
    s32         regIndex;

    // we don't change registers when we are going to change VI modes
    // and we are in even field now.(hw restriction)
    if (! ((shdwChangeMode == 1)
           && (getCurrentFieldEvenOdd() == 0)) )
    {
        while(shdwChanged)
        {
            regIndex = cntlzd(shdwChanged);
            __VIRegs[regIndex] = shdwRegs[regIndex];

            shdwChanged &= ~VI_BITMASK(regIndex);
        }
        shdwChangeMode = 0;
        CurrTiming = HorVer.timing;
        CurrTvMode = HorVer.tv;
        CurrBufAddr = NextBufAddr;

        return TRUE;
    }
    else
        return FALSE;
}

/*---------------------------------------------------------------------------*
  Name:         __VIRetraceHandler

  Description:  interrupt handler for vi interrupts

  Arguments:

  Returns:      None.
 *---------------------------------------------------------------------------*/
static void __VIRetraceHandler(__OSInterrupt interrupt, OSContext* context)
{
#pragma unused(interrupt)

    OSContext   exceptionContext;
    u16         reg;
    u32         inter = 0;
#ifdef _DEBUG
    static u32  dbgCount = 0;
#endif

    reg = __VIRegs[VI_DSP_INT0_U];
    if (reg & VI_DSP_INT0_U_REG_INT0_MASK)
    {
        __VIRegs[VI_DSP_INT0_U] = (u16)(reg & ~VI_DSP_INT0_U_REG_INT0_MASK);
        inter |= INT_0;
    }
    reg = __VIRegs[VI_DSP_INT1_U];
    if (reg & VI_DSP_INT1_U_REG_INT1_MASK)
    {
        __VIRegs[VI_DSP_INT1_U] = (u16)(reg & ~VI_DSP_INT1_U_REG_INT1_MASK);
        inter |= INT_1;
    }
    reg = __VIRegs[VI_DSP_INT2_U];
    if (reg & VI_DSP_INT2_U_REG_INT2_MASK)
    {
        __VIRegs[VI_DSP_INT2_U] = (u16)(reg & ~VI_DSP_INT2_U_REG_INT2_MASK);
        inter |= INT_2;
    }
    reg = __VIRegs[VI_DSP_INT3_U];
    if (reg & VI_DSP_INT3_U_REG_INT3_MASK)
    {
        __VIRegs[VI_DSP_INT3_U] = (u16)(reg & ~VI_DSP_INT3_U_REG_INT3_MASK);
        inter |= INT_3;
    }
#if 1 // XXX It takes some time until the interrupt actually gets cleared
    reg = __VIRegs[VI_DSP_INT3_U];
#endif

    if ( (inter & INT_2) || (inter & INT_3) )
    {
        OSClearContext(&exceptionContext);
        OSSetCurrentContext(&exceptionContext);

        if (PositionCallback)
        {
            s16         x, y;
            
            __VIGetCurrentPosition(&x, &y);
            (*PositionCallback)(x, y);
        }

        OSClearContext(&exceptionContext);
        OSSetCurrentContext(context);
        return;
    }
    else if (inter == 0)
    {
#ifndef SPRUCE
        ASSERT(FALSE);
#endif
    }

    retraceCount++;
    // Create new context on stack
    OSClearContext(&exceptionContext);
    OSSetCurrentContext(&exceptionContext);


    if (PreCB)
    {
        (*PreCB)(retraceCount);
    }

    if (flushFlag)
    {
#ifdef _DEBUG
        dbgCount = 0;
#endif
        if (VISetRegs())
        {
            flushFlag = 0;
            SIRefreshSamplingRate();
        }
    }
#ifdef _DEBUG
    else if (changed)
    {
        dbgCount++;
        if (dbgCount > 60)
        {
            OSReport("Warning: VIFlush() was not called for 60 frames although VI settings were changed\n");
            dbgCount = 0;
        }
    }
#endif

    if (PostCB)
    {
        OSClearContext(&exceptionContext);
        (*PostCB)(retraceCount);
    }

    OSWakeupThread(&retraceQueue);

    OSClearContext(&exceptionContext);
    OSSetCurrentContext(context);
}

/*---------------------------------------------------------------------------*
  Name:         VISetPreRetraceCallback

  Description:  set vi pre retrace callback

  Arguments:    cb           call back function

  Returns:      the old call back function
 *---------------------------------------------------------------------------*/
VIRetraceCallback VISetPreRetraceCallback(VIRetraceCallback cb)
{
    BOOL enabled;
    VIRetraceCallback oldcb = PreCB;

    enabled = OSDisableInterrupts();
    PreCB = cb;
    OSRestoreInterrupts(enabled);

    return oldcb;
}

/*---------------------------------------------------------------------------*
  Name:         VISetPostRetraceCallback

  Description:  set vi post retrace callback

  Arguments:    cb           call back function

  Returns:      the old call back function
 *---------------------------------------------------------------------------*/
VIRetraceCallback VISetPostRetraceCallback(VIRetraceCallback cb)
{
    BOOL enabled;
    VIRetraceCallback oldcb = PostCB;

    enabled = OSDisableInterrupts();
    PostCB = cb;
    OSRestoreInterrupts(enabled);

    return oldcb;
}

/*---------------------------------------------------------------------------*
  Name:         getTiming

  Description:  get timing values for the specified mode

  Arguments:    mode         TV mode (NTSC/PAL/MPAL, INT/DS/PROG)

  Returns:      None.
 *---------------------------------------------------------------------------*/
static timing_s *getTiming(VITVMode mode)
{
    switch (mode)
    {
      case VI_TVMODE_NTSC_INT:
        return &timing[0];
        break;
      case VI_TVMODE_NTSC_DS:
        return &timing[1];
        break;
      case VI_TVMODE_PAL_INT:
        return &timing[2];
        break;
      case VI_TVMODE_PAL_DS:
        return &timing[3];
        break;
      case VI_TVMODE_EURGB60_INT:
        return &timing[0];
        break;
      case VI_TVMODE_EURGB60_DS:
        return &timing[1];
        break;
      case VI_TVMODE_MPAL_INT:
        return &timing[4];
        break;
      case VI_TVMODE_MPAL_DS:
        return &timing[5];
        break;
      case VI_TVMODE_NTSC_PROG:
        return &timing[6];
        break;
      case VI_TVMODE_NTSC_3D:
        return &timing[7];
        break;
      case VI_TVMODE_DEBUG_PAL_INT:
        return &timing[2];
        break;
      case VI_TVMODE_DEBUG_PAL_DS:
        return &timing[3];
        break;
      case VI_TVMODE_GCA_INT:
        return &timing[8];
        break;
      case VI_TVMODE_GCA_PROG:
        return &timing[9];
        break;
    }
    // NOTREACHED
    return (timing_s*)NULL;
}

/*---------------------------------------------------------------------------*
  Name:         __VIInit

  Description:  Initialize VI. This function should only be called from boot,
                especially when power on reset.

  Arguments:    mode         TV mode (NTSC/PAL/MPAL, INT/DS)

  Returns:      None.
 *---------------------------------------------------------------------------*/
void __VIInit(VITVMode mode)
{
    timing_s*   tm;
    u32         nonInter;
    u32         tv;
    vu32        a;
    u16         hct;
    u16         vct;
#ifndef SPRUCE
    u32         encoderType;

    encoderType = getEncoderType();
    if (encoderType == VIDEO_ENCODER_PHILIPS) {
        __VIInitPhilips();
    }
#endif

    nonInter = (u32)mode & 2;
    tv = (u32)mode >> 2;

    // set TV format to lomem
    *(u32*)OSPhysicalToCached(OS_VITVTYPE_ADDR) = tv;

#ifndef SPRUCE
    // if the video encoder is Philips one, we need to use
    // debug mode
    if (encoderType == VIDEO_ENCODER_PHILIPS)
        tv = VI_DEBUG;
#endif

    tm = getTiming(mode);

    __VIRegs[VI_DSP_CFG] = VI_DSP_CFG_REG_RST_MASK;

    // wait for 10 cycles
    for(a = 0; a < 1000; a++)
        ;

    __VIRegs[VI_DSP_CFG] = 0;

    // Program the other registers
    __VIRegs[VI_HOR_TIM0_L] = (u16)VI_HOR_TIM0_L_REG(tm->hlw);
    __VIRegs[VI_HOR_TIM0_U] = (u16)VI_HOR_TIM0_U_REG(tm->hce, tm->hcs);
    __VIRegs[VI_HOR_TIM1_L] = (u16)VI_HOR_TIM1_L_REG(tm->hsy, tm->hbe640 & ONES(VI_HOR_TIM1_L_REG_HBE_L_SIZE));
    __VIRegs[VI_HOR_TIM1_U] = (u16)VI_HOR_TIM1_U_REG(tm->hbe640 >> VI_HOR_TIM1_L_REG_HBE_L_SIZE, tm->hbs640);

#ifndef SPRUCE
    if (encoderType == VIDEO_ENCODER_PHILIPS)
    {
        __VIRegs[VI_HBE656] = (u16)VI_HBE656_REG(tm->hbeCCIR656, 1);
        __VIRegs[VI_HBS656] = (u16)VI_HBS656_REG(tm->hbsCCIR656);
    }
#endif

    // to black out the screen, we set 0 to ACV, set prb as PRB+ACV
    // XXX Set prb as PRB+ACV-2, psb as PSB+2 here.
    // Setting prb as PRB+ACV doesn't work.
    __VIRegs[VI_VER_TIM] = (u16)VI_VER_TIM_REG(tm->equ, 0);
    __VIRegs[VI_VER_ODD_TIM_L] = (u16)VI_VER_ODD_TIM_L_REG(tm->prbOdd + tm->acv
                                                           * 2 - 2);
    __VIRegs[VI_VER_ODD_TIM_U] = (u16)VI_VER_ODD_TIM_U_REG(tm->psbOdd + 2);
    __VIRegs[VI_VER_EVN_TIM_L] = (u16)VI_VER_EVN_TIM_L_REG(tm->prbEven + tm->acv
                                                           * 2 - 2);
    __VIRegs[VI_VER_EVN_TIM_U] = (u16)VI_VER_EVN_TIM_U_REG(tm->psbEven + 2);

    __VIRegs[VI_ODD_BBLNK_INTVL_L] = (u16)VI_ODD_BBLNK_INTVL_L_REG(tm->bs1, tm->be1);
    __VIRegs[VI_ODD_BBLNK_INTVL_U] = (u16)VI_ODD_BBLNK_INTVL_U_REG(tm->bs3, tm->be3);
    __VIRegs[VI_EVN_BBLNK_INTVL_L] = (u16)VI_EVN_BBLNK_INTVL_L_REG(tm->bs2, tm->be2);
    __VIRegs[VI_EVN_BBLNK_INTVL_U] = (u16)VI_EVN_BBLNK_INTVL_U_REG(tm->bs4, tm->be4);

    // specify picture config in case application calls only
    // VISetNextFrameBuffer() (not calling VIConfigure)
    __VIRegs[VI_PIC_CFG] = (u16)VI_PIC_CFG_REG(40, 40);

    // don't specify picture base

    // Vertical int for odd field. Clear the interrupt if there's a pending int
    __VIRegs[VI_DSP_INT1_L] = (u16)VI_DSP_INT1_L_REG(1);
    __VIRegs[VI_DSP_INT1_U] = (u16)VI_DSP_INT1_U_REG(1, 1, 0);

    hct = (u16)(tm->hlw + 1);               // HCT starts from 1
    vct = (u16)(tm->nhlines / 2 + 1);       // VCT starts from 1
    // Vertical int for even field. Clear the interrupt if there's a pending int
    __VIRegs[VI_DSP_INT0_L] = (u16)VI_DSP_INT0_L_REG(hct);
    __VIRegs[VI_DSP_INT0_U] = (u16)VI_DSP_INT0_U_REG(vct, 1, 0);

    // VI_DSP_CFG_REG(enb, rst, int, dlr, le0, le1, mode)
    if ((mode != VI_TVMODE_NTSC_PROG) && (mode != VI_TVMODE_NTSC_3D) && (mode != VI_TVMODE_GCA_PROG))
    {
        __VIRegs[VI_DSP_CFG] = (u16)VI_DSP_CFG_REG(1, 0, nonInter, 0, 0, 0, tv);
        __VIRegs[VI_CLKSEL] = VI_CLKSEL_REG(0);
    }
    else
    {
        // when progressive or 3D mode, set 1 for "nin"
        __VIRegs[VI_DSP_CFG] = (u16)VI_DSP_CFG_REG(1, 0, 1, 0, 0, 0, tv);
        __VIRegs[VI_CLKSEL] = VI_CLKSEL_REG(1);
    }
}

/*---------------------------------------------------------------------------*
  Name:         AdjustPosition

  Description:  adjust position in a certain range

  Arguments:    origin          original data
                size            original width
                start           start of the range
                width           max width
                adjust          values to adjust

  Returns:      adjusted data
 *---------------------------------------------------------------------------*/
#define CLAMP(x,l,h)    (((x) > (h)) ? (h) : (((x) < (l)) ? (l) : (x)))
#define MIN(a,b)      (((a) < (b)) ? (a) : (b))
#define MAX(a,b)    (((a) > (b)) ? (a) : (b))

static void AdjustPosition(u16 acv)
{
    s32         coeff;
    s32         frac;

    HorVer.AdjustedDispPosX = (u16)CLAMP((s16)HorVer.DispPosX + displayOffsetH,
                                         0, 720 - HorVer.DispSizeX);

    coeff = (HorVer.FBMode == VI_XFBMODE_SF)? 2 : 1;
    frac = HorVer.DispPosY & 1;

    HorVer.AdjustedDispPosY = (u16)MAX((s16)HorVer.DispPosY + displayOffsetV, frac);

    HorVer.AdjustedDispSizeY = (u16)(HorVer.DispSizeY
                                     + MIN((s16)HorVer.DispPosY + displayOffsetV - frac, 0)
                                     - MAX((s16)HorVer.DispPosY + (s16)HorVer.DispSizeY +
                                           displayOffsetV - ((s16)acv*2 - frac), 0));

    HorVer.AdjustedPanPosY = (u16)(HorVer.PanPosY
                                   - MIN((s16)HorVer.DispPosY + displayOffsetV - frac, 0) / coeff);

    HorVer.AdjustedPanSizeY = (u16)(HorVer.PanSizeY
                                    + MIN((s16)HorVer.DispPosY + displayOffsetV - frac, 0) / coeff
                                    - MAX((s16)HorVer.DispPosY + (s16)HorVer.DispSizeY +
                                          displayOffsetV - ((s16)acv*2 - frac), 0) / coeff);
}

/*---------------------------------------------------------------------------*
  Name:         ImportAdjustValues

  Description:  Get adjust values from Sram

  Arguments:    None.

  Returns:      None.
 *---------------------------------------------------------------------------*/
static void ImportAdjustingValues(void)
{
    OSSram*         sram;

    sram = __OSLockSram();
    ASSERT(sram);
    displayOffsetH = sram->displayOffsetH;
    displayOffsetV = 0;
    __OSUnlockSram(FALSE);
}

/*---------------------------------------------------------------------------*
  Name:         VIInit

  Description:  Initialize VI

  Arguments:    None.

  Returns:      None.
 *---------------------------------------------------------------------------*/
void VIInit(void)
{
    u16     dspCfg;
    u32     value;
    u32     tv;
    u32     tvInBootrom;

    if (IsInitialized)
    {
        return;
    }

    OSRegisterVersion(__VIVersion);

    IsInitialized = TRUE;

#ifndef SPRUCE
    encoderType = getEncoderType();
#endif

    if ( !(__VIRegs[VI_DSP_CFG] & VI_DSP_CFG_REG_ENB_MASK) )
    {
        // do the very first initialization
        __VIInit(VI_TVMODE_NTSC_INT);
    }

    retraceCount = 0;
    changed = 0;
    shdwChanged = 0;
    changeMode = 0;
    shdwChangeMode = 0;
    flushFlag = 0;

    // set default scaling TAP.
    __VIRegs[VI_FLTR_COEFF0_L] = (u16)VI_FLTR_COEFF0_L_REG(taps[0], taps[1] & ONES(VI_FLTR_COEFF0_L_REG_T1_L_SIZE));
    __VIRegs[VI_FLTR_COEFF0_U] = (u16)VI_FLTR_COEFF0_U_REG(taps[1] >> VI_FLTR_COEFF0_L_REG_T1_L_SIZE, taps[2]);
    __VIRegs[VI_FLTR_COEFF1_L] = (u16)VI_FLTR_COEFF1_L_REG(taps[3], taps[4] & ONES(VI_FLTR_COEFF1_L_REG_T4_L_SIZE));
    __VIRegs[VI_FLTR_COEFF1_U] = (u16)VI_FLTR_COEFF1_U_REG(taps[4] >> VI_FLTR_COEFF1_L_REG_T4_L_SIZE, taps[5]);
    __VIRegs[VI_FLTR_COEFF2_L] = (u16)VI_FLTR_COEFF2_L_REG(taps[6], taps[7] & ONES(VI_FLTR_COEFF2_L_REG_T7_L_SIZE));
    __VIRegs[VI_FLTR_COEFF2_U] = (u16)VI_FLTR_COEFF2_U_REG(taps[7] >> VI_FLTR_COEFF2_L_REG_T7_L_SIZE, taps[8]);
    __VIRegs[VI_FLTR_COEFF3_L] = (u16)VI_FLTR_COEFF3_L_REG(taps[9], taps[10]);
    __VIRegs[VI_FLTR_COEFF3_U] = (u16)VI_FLTR_COEFF3_U_REG(taps[11], taps[12]);
    __VIRegs[VI_FLTR_COEFF4_L] = (u16)VI_FLTR_COEFF4_L_REG(taps[13], taps[14]);
    __VIRegs[VI_FLTR_COEFF4_U] = (u16)VI_FLTR_COEFF4_U_REG(taps[15], taps[16]);
    __VIRegs[VI_FLTR_COEFF5_L] = (u16)VI_FLTR_COEFF5_L_REG(taps[17], taps[18]);
    __VIRegs[VI_FLTR_COEFF5_U] = (u16)VI_FLTR_COEFF5_U_REG(taps[19], taps[20]);
    __VIRegs[VI_FLTR_COEFF6_L] = (u16)VI_FLTR_COEFF6_L_REG(taps[21], taps[22]);
    __VIRegs[VI_FLTR_COEFF6_U] = (u16)VI_FLTR_COEFF6_U_REG(taps[23], taps[24]);

    // default 640
    __VIRegs[VI_WIDTH] = (u16)VI_WIDTH_REG(640);

#if !defined(SPRUCE) && !defined(MINNOW)
    // Read Sram to get adjust values
    ImportAdjustingValues();
#endif


    tvInBootrom = *(u32*)OSPhysicalToCached(OS_VITVTYPE_ADDR);

    // should grab the mode from hardware
    // should not use __VIInit() definitions
    dspCfg = __VIRegs[VI_DSP_CFG];
    HorVer.nonInter = VI_DSP_CFG_REG_GET_INT(dspCfg);
    HorVer.tv = VI_DSP_CFG_REG_GET_MODE(dspCfg);
    if ( (tvInBootrom == VI_PAL) && (HorVer.tv == VI_NTSC) )
    {
        HorVer.tv = VI_EURGB60;
    }

    tv = (HorVer.tv == VI_DEBUG)? VI_NTSC : HorVer.tv;
    HorVer.timing = getTiming((VITVMode)VI_TVMODE(tv, HorVer.nonInter));

    // initialize shadow register for DSP_CFG.
    // From now on, this variable always contains the register value.
    regs[VI_DSP_CFG] = dspCfg;

    CurrTiming = HorVer.timing;
    CurrTvMode = HorVer.tv;

    // set some of the default values
    HorVer.DispSizeX = 640;
    HorVer.DispSizeY = (u16)(CurrTiming->acv * 2);
    HorVer.DispPosX = (u16)( (VI_MAX_WIDTH_NTSC - HorVer.DispSizeX) / 2 );
    HorVer.DispPosY = 0;

    AdjustPosition(CurrTiming->acv);

    HorVer.FBSizeX = 640;
    HorVer.FBSizeY = (u16)(CurrTiming->acv * 2);

    HorVer.PanPosX = 0;
    HorVer.PanPosY = 0;
    HorVer.PanSizeX = 640;
    HorVer.PanSizeY = (u16)(CurrTiming->acv * 2);

    HorVer.FBMode = VI_XFBMODE_SF;

    HorVer.wordPerLine = 40;
    HorVer.std = 40;
    HorVer.wpl = 40;

    HorVer.xof = 0;
    HorVer.black = TRUE;
    HorVer.threeD = FALSE;

    // initialize the thread queue
    OSInitThreadQueue(&retraceQueue);

    // Clear pending interrupts if any
    value = __VIRegs[VI_DSP_INT0_U];
    VI_DSP_INT0_U_REG_SET_INT0(value, 0);
    __VIRegs[VI_DSP_INT0_U] = (u16)value;

    value = __VIRegs[VI_DSP_INT1_U];
    VI_DSP_INT1_U_REG_SET_INT1(value, 0);
    __VIRegs[VI_DSP_INT1_U] = (u16)value;

    PreCB  = (VIRetraceCallback)NULL;
    PostCB = (VIRetraceCallback)NULL;

    // Enable VI interrupts
    __OSSetInterruptHandler(__OS_INTERRUPT_PI_VI,  __VIRetraceHandler);
    __OSUnmaskInterrupts(OS_INTERRUPTMASK_PI_VI);

}

/*---------------------------------------------------------------------------*
  Name:         VIWaitForRetrace

  Description:  Waits until the next vertical retrace interrupt occurs.

  Arguments:    None.

  Returns:      None.
 *---------------------------------------------------------------------------*/
void VIWaitForRetrace(void)
{
    BOOL enabled;
    u32  count;

    enabled = OSDisableInterrupts();
    count = retraceCount;
    do {
        OSSleepThread(&retraceQueue);
    } while (count == retraceCount);
    OSRestoreInterrupts(enabled);
}

/*---------------------------------------------------------------------------*
  Name:         setInterruptRegs

  Description:  set interrupt registers

  Arguments:    tm          timing of the mode (nhlines and hlw are used)

  Returns:      None.
 *---------------------------------------------------------------------------*/
static void setInterruptRegs(timing_s* tm)
{
    u16         vct;
    u16         hct;
    u16         borrow;

    // if tm->nhlines is odd, set hct at the half line
    vct = (u16)(tm->nhlines / 2);
    borrow = (u16)(tm->nhlines % 2);
    hct = (borrow)? tm->hlw : (u16)0;

    // both vct and hct start from 1
    vct++;
    hct++;

    regs[VI_DSP_INT0_L] = (u16)VI_DSP_INT0_L_REG(hct);
    changed |= VI_BITMASK_DSP_INT0_L;

    regs[VI_DSP_INT0_U] = (u16)VI_DSP_INT0_U_REG(vct, ENABLE, 0);
    changed |= VI_BITMASK_DSP_INT0_U;
}

/*---------------------------------------------------------------------------*
  Name:         setPicConfig

  Description:  set picture config register.
                calculate wordPerLine, std, wpl, xof for later use.

  Arguments:    fbSizeX     frame buffer width
                xfbMode     xfb mode (SF or DF)
                panPosX     pan position X
                panSizeX    pan width
                wordPerLine word_per_line described in vi.doc (output)
                std         stride (output)
                wpl         wpl (see vi.doc) (output)
                xof         xof (see vi.doc) (output)

  Returns:      None.
 *---------------------------------------------------------------------------*/
static void setPicConfig(u16 fbSizeX, VIXFBMode xfbMode, u16 panPosX,
                         u16 panSizeX, u8* wordPerLine, u8* std, u8* wpl,
                         u8* xof)
{
    *wordPerLine = (u8)( (fbSizeX + 15)/16 );
    *std = (xfbMode == VI_XFBMODE_SF)? *wordPerLine : (u8)( 2 * *wordPerLine );
    *xof = (u8)( panPosX % 16 );
    *wpl = (u8)( (*xof + panSizeX + 15) / 16 );

    regs[VI_PIC_CFG] = (u16)VI_PIC_CFG_REG(*std, *wpl);
    changed |= VI_BITMASK_PIC_CFG;
}

/*---------------------------------------------------------------------------*
  Name:         setBBIntervalRegs

  Description:  set burst blanking interval registers.

  Arguments:    tm          timing structure

  Returns:      None.
 *---------------------------------------------------------------------------*/
static void setBBIntervalRegs(timing_s* tm)
{
    u16         val;

    val = (u16)VI_ODD_BBLNK_INTVL_L_REG(tm->bs1, tm->be1);
    regs[VI_ODD_BBLNK_INTVL_L] = val;
    changed |= VI_BITMASK_ODD_BBLNK_INTVL_L;

    val = (u16)VI_ODD_BBLNK_INTVL_U_REG(tm->bs3, tm->be3);
    regs[VI_ODD_BBLNK_INTVL_U] = val;
    changed |= VI_BITMASK_ODD_BBLNK_INTVL_U;

    val = (u16)VI_EVN_BBLNK_INTVL_L_REG(tm->bs2, tm->be2);
    regs[VI_EVN_BBLNK_INTVL_L] = val;
    changed |= VI_BITMASK_EVN_BBLNK_INTVL_L;

    val = (u16)VI_EVN_BBLNK_INTVL_U_REG(tm->bs4, tm->be4);
    regs[VI_EVN_BBLNK_INTVL_U] = val;
    changed |= VI_BITMASK_EVN_BBLNK_INTVL_U;
}

/*---------------------------------------------------------------------------*
  Name:         setScalingRegs

  Description:  set scaling register if necessary

  Arguments:    panSizeX    pan width
                dispSizeX   display width

  Returns:      None.
 *---------------------------------------------------------------------------*/
static void setScalingRegs(u16 panSizeX, u16 dispSizeX, BOOL threeD)
{
    u32      scale;

    panSizeX = (u16)(threeD? panSizeX * 2 : panSizeX);

    if (panSizeX < dispSizeX)
    {
        // scaling on
        scale = (256 * (u32)panSizeX + (u32)dispSizeX - 1)/ (u32)dispSizeX;

        regs[VI_HSCALE] = (u16)VI_HSCALE_REG(scale, ENABLE);
        changed |= VI_BITMASK_HSCALE;

        regs[VI_WIDTH] = (u16)VI_WIDTH_REG(panSizeX);
        changed |= VI_BITMASK_WIDTH;
    }
    else
    {
        // scaling disable
        regs[VI_HSCALE] = (u16)VI_HSCALE_REG(256, DISABLE);
        changed |= VI_BITMASK_HSCALE;
    }
}

/*---------------------------------------------------------------------------*
  Name:         calcFbbs

  Description:  calculate frame buffer addresses (top/bottom)

  Arguments:    bufAddr     (specified) frame buffer start address
                panPosX     pan position X
                panPosY     pan position Y
                wordPerLine word_per_line described in vi.doc
                xfbMode     xfb mode (SF or DF)
                dispPosY    display position Y
                tfbb        top frame buffer physical address (output)
                bfbb        bottom frame buffer physical address (output)

  Returns:      None.
 *---------------------------------------------------------------------------*/
static void calcFbbs(u32 bufAddr, u16 panPosX, u16 panPosY, u8 wordPerLine,
                     VIXFBMode xfbMode, u16 dispPosY, u32* tfbb, u32* bfbb)
{
    u32     bytesPerLine;
    u32     xoffInWords;

    xoffInWords = (u32)panPosX / VI_PIXELS_PER_WORD;
    bytesPerLine = (u32)wordPerLine * VI_BYTES_PER_WORD;

    *tfbb = bufAddr + xoffInWords * VI_BYTES_PER_WORD + bytesPerLine * panPosY;
    *bfbb = (xfbMode == VI_XFBMODE_SF)? *tfbb : (*tfbb + bytesPerLine);

    if (dispPosY % 2 == 1)
    {
        u32     tmp;

        // if dispPosY is odd, first line is shown on even field
        tmp = *tfbb;
        *tfbb = *bfbb;
        *bfbb = tmp;
    }

    *tfbb = ToPhysical(*tfbb);
    *bfbb = ToPhysical(*bfbb);
}


/*---------------------------------------------------------------------------*
  Name:         setFbbRegs

  Description:  set (left/right) frame buffer registers

  Arguments:    HorVer      Horizontal/Vertical information
                tfbb        top left frame buffer address (output)
                bfbb        bottom left frame buffer address (output)
                rtfbb       top right frame buffer address (output)
                rbfbb       bottom right frame buffer address (output)

  Returns:      None.
 *---------------------------------------------------------------------------*/
#define     IS_LOWER_16MB(x)               ((x) < 16*1024*1024)

static void setFbbRegs(HorVer_s* HorVer, u32* tfbb, u32* bfbb, u32* rtfbb, u32* rbfbb)
{
    u32         shifted;

    calcFbbs(HorVer->bufAddr, HorVer->PanPosX, HorVer->AdjustedPanPosY,
             HorVer->wordPerLine, HorVer->FBMode, HorVer->AdjustedDispPosY,
             tfbb, bfbb);

    if (HorVer->threeD)
    {
        calcFbbs(HorVer->rbufAddr, HorVer->PanPosX, HorVer->AdjustedPanPosY,
                 HorVer->wordPerLine, HorVer->FBMode, HorVer->AdjustedDispPosY,
                 rtfbb, rbfbb);
    }

    if ( IS_LOWER_16MB(*tfbb) && IS_LOWER_16MB(*bfbb)
         && IS_LOWER_16MB(*rtfbb) && IS_LOWER_16MB(*rbfbb) )
    {
        shifted = 0;
    }
    else
    {
        shifted = 1;
    }

#ifdef BUG_16MB_XFB_LMT
    ASSERTMSG( shifted == 0,
              "Error: Frame buffer start address should be in lower 16MB. "
              "If the address you set is lower than 16MB and still see this message, "
              "try using MUCH lower address. We need some margin for some internal "
              "reason.(this is because of a hardware bug and should be fixed on "
              "the next chip)\n");
#else
    if (shifted)
    {
        *tfbb >>= 5;
        *bfbb >>= 5;
        *rtfbb >>= 5;
        *rbfbb >>= 5;
    }
#endif

    regs[VI_PIC_BASE_LFT_L] = (u16)VI_PIC_BASE_LFT_L_REG(*tfbb & 0xffff);
    changed |= VI_BITMASK_PIC_BASE_LFT_L;

    regs[VI_PIC_BASE_LFT_U] = (u16)VI_PIC_BASE_LFT_U_REG(*tfbb >> 16, HorVer->xof, shifted);
    changed |= VI_BITMASK_PIC_BASE_LFT_U;

    regs[VI_PIC_BASE_LFT_BOT_L] = (u16)VI_PIC_BASE_LFT_BOT_L_REG(*bfbb & 0xffff);
    changed |= VI_BITMASK_PIC_BASE_LFT_BOT_L;

    regs[VI_PIC_BASE_LFT_BOT_U] = (u16)VI_PIC_BASE_LFT_BOT_U_REG(*bfbb >> 16);
    changed |= VI_BITMASK_PIC_BASE_LFT_BOT_U;

    if (HorVer->threeD)
    {
        regs[VI_PIC_BASE_RGT_L] = (u16)VI_PIC_BASE_RGT_L_REG(*rtfbb & 0xffff);
        changed |= VI_BITMASK_PIC_BASE_RGT_L;

        regs[VI_PIC_BASE_RGT_U] = (u16)VI_PIC_BASE_RGT_U_REG(*rtfbb >> 16);
        changed |= VI_BITMASK_PIC_BASE_RGT_U;

        regs[VI_PIC_BASE_RGT_BOT_L] = (u16)VI_PIC_BASE_RGT_BOT_L_REG(*rbfbb & 0xffff);
        changed |= VI_BITMASK_PIC_BASE_RGT_BOT_L;

        regs[VI_PIC_BASE_RGT_BOT_U] = (u16)VI_PIC_BASE_RGT_BOT_U_REG(*rbfbb >> 16);
        changed |= VI_BITMASK_PIC_BASE_RGT_BOT_U;
    }
}

/*---------------------------------------------------------------------------*
  Name:         setHorizontalRegs

  Description:  set horizontal registers

  Arguments:    tm          timing structure
                dispPosX    display position X
                dispSizeX   display width

  Returns:      None.
 *---------------------------------------------------------------------------*/
static void setHorizontalRegs(timing_s* tm, u16 dispPosX, u16 dispSizeX)
{
    u32         hbe;
    u32         hbs;
    u32         hbeLo;
    u32         hbeHi;

    // set Horizontal Timing 0 Register
    regs[VI_HOR_TIM0_L] = (u16)VI_HOR_TIM0_L_REG(tm->hlw);
    changed |= VI_BITMASK_HOR_TIM0_L;

    regs[VI_HOR_TIM0_U] = (u16)VI_HOR_TIM0_U_REG(tm->hce, tm->hcs);
    changed |= VI_BITMASK_HOR_TIM0_U;

    // set Horizontal Timing 1 Register
    hbe = (u32)(tm->hbe640 - 40 + dispPosX);
    hbs = (u32)(tm->hbs640 + 40 + dispPosX - (720 - dispSizeX));
    hbeLo = hbe & ONES(VI_HOR_TIM1_L_REG_HBE_L_SIZE);
    hbeHi = hbe >> VI_HOR_TIM1_L_REG_HBE_L_SIZE;

    regs[VI_HOR_TIM1_L] = (u16)VI_HOR_TIM1_L_REG(tm->hsy, hbeLo);
    changed |= VI_BITMASK_HOR_TIM1_L;

    regs[VI_HOR_TIM1_U] = (u16)VI_HOR_TIM1_U_REG(hbeHi, hbs);
    changed |= VI_BITMASK_HOR_TIM1_U;
}

/*---------------------------------------------------------------------------*
  Name:         setVerticalRegs

  Description:  set vertical registers

  Arguments:    dispPosY    display position Y
                dispSizeY   display height
                equ         default EQU
                acv         default ACV
                prbOdd      default odd PRB
                prbEven     default even PRB
                psbOdd      default odd PSB
                psbEven     default even PSB
                black       1 ... black enable, 0 ... black diable

  Returns:      None.
 *---------------------------------------------------------------------------*/
static void setVerticalRegs(u16 dispPosY, u16 dispSizeY, u8 equ, u16 acv,
                            u16 prbOdd, u16 prbEven,
                            u16 psbOdd, u16 psbEven,
                            BOOL black)
{
    u16         actualPrbOdd;
    u16         actualPrbEven;
    u16         actualPsbOdd;
    u16         actualPsbEven;
    u16         actualAcv;
    u16         c, d;

    if (VI_CLKSEL_REG_GET_VICLKSEL(regs[VI_CLKSEL]))
    {
        // progressive/3D mode, acv lines are drawn in one frame
        c = 1;
        d = 2;
    }
    else
    {
        // other mode, 2*acv lines are drawn in one frame
        c = 2;
        d = 1;
    }
    // In progressive/3D mode,
    //   actualprb = prb + 2 * dispPosY
    //   actualpsb = psb + 2 * (acv - dispSizeY - dispPosY)
    // Note that one line difference in display position or size in Y axis
    // should be reflected as two lines and prb and psb (that's the meaning of
    // /d/)
    if (dispPosY % 2 == 0)
    {
        actualPrbOdd = (u16)(prbOdd + d * dispPosY);
        actualPsbOdd = (u16)(psbOdd + d * ((c * acv - dispSizeY) - dispPosY));
        actualPrbEven = (u16)(prbEven + d * dispPosY);
        actualPsbEven = (u16)(psbEven + d * ((c * acv - dispSizeY) - dispPosY));
    }
    else
    {
        // swap PrbOdd and PrbEven so that PrbOdd is always even
        actualPrbOdd = (u16)(prbEven + d * dispPosY);
        actualPsbOdd = (u16)(psbEven + d * ((c * acv - dispSizeY) - dispPosY));
        actualPrbEven = (u16)(prbOdd + d * dispPosY);
        actualPsbEven = (u16)(psbOdd + d * ((c * acv - dispSizeY) - dispPosY));
    }

    actualAcv = (u16)(dispSizeY / c);

    // In black mode, we move (acv) lines to prb. Again, note that
    // one acv line is worth two prb lines. +- 2 is for
    // workaround of ATI bug.
    if (black)
    {
        actualPrbOdd += 2 * actualAcv - 2;
        actualPsbOdd += 2;
        actualPrbEven += 2 * actualAcv - 2;
        actualPsbEven += 2;
        actualAcv = 0;
    }

    regs[VI_VER_TIM] = (u16)VI_VER_TIM_REG(equ, actualAcv);
    changed |= VI_BITMASK_VER_TIM;

    regs[VI_VER_ODD_TIM_L] = (u16)VI_VER_ODD_TIM_L_REG(actualPrbOdd);
    changed |= VI_BITMASK_VER_ODD_TIM_L;

    regs[VI_VER_ODD_TIM_U] = (u16)VI_VER_ODD_TIM_U_REG(actualPsbOdd);
    changed |= VI_BITMASK_VER_ODD_TIM_U;

    regs[VI_VER_EVN_TIM_L] = (u16)VI_VER_EVN_TIM_L_REG(actualPrbEven);
    changed |= VI_BITMASK_VER_EVN_TIM_L;

    regs[VI_VER_EVN_TIM_U] = (u16)VI_VER_EVN_TIM_U_REG(actualPsbEven);
    changed |= VI_BITMASK_VER_EVN_TIM_U;
}


static void PrintDebugPalCaution(void)
{
    static u32          message = 0;

    if (message == 0)
    {
        message = 1;

        // Print caution
        OSReport("***************************************\n");
        OSReport(" ! ! ! C A U T I O N ! ! !             \n");
        OSReport("This TV format \"DEBUG_PAL\" is only for \n");
        OSReport("temporary solution until PAL DAC board \n");
        OSReport("is available. Please do NOT use this   \n");
        OSReport("mode in real games!!!                  \n");
        OSReport("***************************************\n");
    }
}


/*---------------------------------------------------------------------------*
  Name:         VIConfigure

  Description:  Configure size, position and mode of TV screen
                Configure size and mode of x frame buffer

  Arguments:    rm      Render mode

  Returns:      None.
 *---------------------------------------------------------------------------*/
void VIConfigure(const GXRenderModeObj* rm)
{
    timing_s*       tm;
    u32             reg;
    BOOL            enabled;
    u32             newNonInter;
    u32             tvInBootrom;
    u32             tvInGame;

#ifdef _DEBUG
    if (rm->viHeight & 0x1)
        OSPanic(__FILE__, __LINE__,
                "VIConfigure(): Odd number(%d) is specified to viHeight\n",
                 rm->viHeight);

    if ( ((rm->xFBmode == VI_XFBMODE_DF) || (rm->viTVmode == VI_TVMODE_NTSC_PROG) || (rm->viTVmode == VI_TVMODE_NTSC_3D) || (rm->viTVmode == VI_TVMODE_GCA_PROG))
         && (rm->xfbHeight != rm->viHeight) )
        OSPanic(__FILE__, __LINE__,
                "VIConfigure(): xfbHeight(%d) is not equal to viHeight(%d) when DF XFB mode or "
                "progressive mode is specified\n",
                 rm->xfbHeight, rm->viHeight);

    if ( (rm->xFBmode == VI_XFBMODE_SF) && (rm->viTVmode != VI_TVMODE_NTSC_PROG) && (rm->viTVmode != VI_TVMODE_NTSC_3D) && (rm->viTVmode != VI_TVMODE_GCA_PROG)
         && (rm->xfbHeight * 2 != rm->viHeight) )
        OSPanic(__FILE__, __LINE__,
                "VIConfigure(): xfbHeight(%d) is not as twice as viHeight(%d) when SF XFB mode is specified\n",
                 rm->xfbHeight, rm->viHeight);
#endif

    enabled = OSDisableInterrupts();

    // Important: if we are going to change mode (int/ds) and
    // the current field is odd, we need to wait for one field
    // to make the change because changing mode in odd field
    // causes trouble
    newNonInter = (u32)rm->viTVmode & 3;
    if (HorVer.nonInter != newNonInter)
    {
        changeMode = 1;
        HorVer.nonInter = newNonInter;
    }

    tvInGame = (u32)rm->viTVmode >> 2;
    tvInBootrom = *(u32*)OSPhysicalToCached(OS_VITVTYPE_ADDR);

    if (tvInGame == VI_DEBUG_PAL)
    {
        PrintDebugPalCaution();
    }

    // Check unreasonable mode switch
    switch (tvInBootrom)
    {
      case VI_NTSC:
      case VI_MPAL:
      case VI_GCA:
        if ( (tvInGame == VI_NTSC) || (tvInGame == VI_MPAL)
             || (tvInGame == VI_GCA) )
        {
            goto GOOD_MODE_CHANGE;
        }
        break;

      case VI_PAL:
      case VI_EURGB60:      // should not occur
        if ( (tvInGame == VI_PAL) || (tvInGame == VI_EURGB60) )
        {
            goto GOOD_MODE_CHANGE;
        }
        break;

      default:
        break;
    }
    OSPanic(__FILE__, __LINE__,
            "VIConfigure(): Tried to change mode from (%d) to (%d), which is forbidden\n",
            tvInBootrom, tvInGame);

  GOOD_MODE_CHANGE:
    if ( (tvInGame == VI_NTSC) || (tvInGame == VI_MPAL) )
    {
        // Use the bootrom TVmode because of MPAL bootrom/NTSC game case
        HorVer.tv       = tvInBootrom;
    }
    else
    {
        HorVer.tv       = tvInGame;
    }

    HorVer.DispPosX     = rm->viXOrigin;
    HorVer.DispPosY     = (HorVer.nonInter == VI_NON_INTERLACE)?
                            (u16)(rm->viYOrigin * 2) : rm->viYOrigin;
    HorVer.DispSizeX    = rm->viWidth;

    HorVer.FBSizeX      = rm->fbWidth;
    HorVer.FBSizeY      = rm->xfbHeight;
    HorVer.FBMode       = rm->xFBmode;
    HorVer.PanSizeX     = HorVer.FBSizeX;
    HorVer.PanSizeY     = HorVer.FBSizeY;
    HorVer.PanPosX      = 0;
    HorVer.PanPosY      = 0;

    HorVer.DispSizeY    = (HorVer.nonInter == VI_PROGRESSIVE)? HorVer.PanSizeY :
                          (HorVer.nonInter == VI_3D)? HorVer.PanSizeY :
                          (HorVer.FBMode == VI_XFBMODE_SF)? (u16)(2 * HorVer.PanSizeY) : HorVer.PanSizeY;

    HorVer.threeD = (HorVer.nonInter == VI_3D)? TRUE : FALSE;

    tm = getTiming((VITVMode)VI_TVMODE(HorVer.tv, HorVer.nonInter));
    HorVer.timing = tm;

    // adjust
    AdjustPosition(tm->acv);

#ifdef _DEBUG
    // There's a hardware restriction that hbe <= hlw, hbs >= 0
    if (rm->viXOrigin > tm->hlw + 40 - tm->hbe640)
        OSPanic(__FILE__, __LINE__,
                "VIConfigure(): viXOrigin(%d) cannot be greater than %d in this TV mode\n",
                rm->viXOrigin, tm->hlw + 40 - tm->hbe640);

    if (rm->viXOrigin + rm->viWidth < 720 - 40 - tm->hbs640)
        OSPanic(__FILE__, __LINE__,
                "VIConfigure(): viXOrigin + viWidth (%d) cannot be less than %d in this TV mode\n",
                rm->viXOrigin + rm->viWidth, 720 - 40 - tm->hbs640);
#endif

    // assert dispsizeY == rm->viHeight

#ifndef SPRUCE
    // if the video encoder is Philips one, we need to use
    // debug mode
    if (encoderType == VIDEO_ENCODER_PHILIPS)
        HorVer.tv = VI_DEBUG;
#endif

    setInterruptRegs(tm);

    reg = regs[VI_DSP_CFG];
    if ((HorVer.nonInter == VI_PROGRESSIVE) || (HorVer.nonInter == VI_3D))
    {
        VI_DSP_CFG_REG_SET_INT (reg, 1);
    }
    else
    {
        VI_DSP_CFG_REG_SET_INT (reg, HorVer.nonInter & 1);
    }

    VI_DSP_CFG_REG_SET_DLR(reg, HorVer.threeD);

    if ((HorVer.tv == VI_DEBUG_PAL) || (HorVer.tv == VI_EURGB60) || (HorVer.tv == VI_GCA))
    {
        VI_DSP_CFG_REG_SET_MODE(reg, VI_NTSC);
    }
    else
    {
        VI_DSP_CFG_REG_SET_MODE(reg, HorVer.tv);
    }

    regs[VI_DSP_CFG] = (u16)reg;
    changed |= VI_BITMASK_DSP_CFG;

    // add by urata
    reg = regs[VI_CLKSEL];
    if ((rm->viTVmode == VI_TVMODE_NTSC_PROG) || (rm->viTVmode == VI_TVMODE_NTSC_3D)
        || (rm->viTVmode == VI_TVMODE_GCA_PROG))
    {
        VI_CLKSEL_REG_SET_VICLKSEL(reg, 1);
    }
    else
    {
        VI_CLKSEL_REG_SET_VICLKSEL(reg, 0);
    }
    regs[VI_CLKSEL] = (u16)reg;
    changed |= VI_BITMASK_CLKSEL;


    setScalingRegs(HorVer.PanSizeX, HorVer.DispSizeX, HorVer.threeD);

    setHorizontalRegs(tm, HorVer.AdjustedDispPosX, HorVer.DispSizeX);

    setBBIntervalRegs(tm);

    setPicConfig(HorVer.FBSizeX, HorVer.FBMode, HorVer.PanPosX, HorVer.PanSizeX,
                 &HorVer.wordPerLine, &HorVer.std, &HorVer.wpl, &HorVer.xof);

    if (FBSet)
    {
        setFbbRegs(&HorVer, &HorVer.tfbb, &HorVer.bfbb,
                   &HorVer.rtfbb, &HorVer.rbfbb);

    }

    setVerticalRegs(HorVer.AdjustedDispPosY, HorVer.AdjustedDispSizeY, tm->equ, tm->acv,
                    tm->prbOdd, tm->prbEven, tm->psbOdd, tm->psbEven,
                    HorVer.black);

    OSRestoreInterrupts(enabled);
}

/*---------------------------------------------------------------------------*
  Name:         VIConfigurePan

  Description:  Configure Pan in x frame buffer

  Arguments:    xOrg        x origin of pan in x frame buffer
                yOrg        y origin of pan in x frame buffer
                width       width of pan
                height      height of pan

  Returns:      None.
 *---------------------------------------------------------------------------*/
void VIConfigurePan(u16 xOrg, u16 yOrg, u16 width, u16 height)
{
    BOOL            enabled;
    timing_s*       tm;

    // assert xOrg: even, height: even
    // assert xorg < 720, yorg < 480 (240) for NTSC
    // assert width < 720 - xorg, height < 480 (240) - yorg
#ifdef _DEBUG
    if (xOrg & 0x1)
        OSPanic(__FILE__, __LINE__,
                "VIConfigurePan(): Odd number(%d) is specified to xOrg\n",
                xOrg);

    if ( (HorVer.FBMode == VI_XFBMODE_DF) && (height & 0x1) )
        OSPanic(__FILE__, __LINE__,
                "VIConfigurePan(): Odd number(%d) is specified to height when DF XFB mode\n",
                 height);
#endif

    enabled = OSDisableInterrupts();

    HorVer.PanPosX      = xOrg;
    HorVer.PanPosY      = yOrg;
    HorVer.PanSizeX     = width;
    HorVer.PanSizeY     = height;
    HorVer.DispSizeY    = (HorVer.nonInter == VI_PROGRESSIVE)? HorVer.PanSizeY :
                          (HorVer.nonInter == VI_3D)? HorVer.PanSizeY :
                          (HorVer.FBMode == VI_XFBMODE_SF)? (u16)(2 * HorVer.PanSizeY) : HorVer.PanSizeY;

    tm = HorVer.timing;

    AdjustPosition(tm->acv);

    setScalingRegs(HorVer.PanSizeX, HorVer.DispSizeX, HorVer.threeD);

    setPicConfig(HorVer.FBSizeX, HorVer.FBMode, HorVer.PanPosX, HorVer.PanSizeX,
                 &HorVer.wordPerLine, &HorVer.std, &HorVer.wpl, &HorVer.xof);

    if (FBSet)
    {
        setFbbRegs(&HorVer, &HorVer.tfbb, &HorVer.bfbb,
                   &HorVer.rtfbb, &HorVer.rbfbb);

    }

    setVerticalRegs(HorVer.AdjustedDispPosY, HorVer.DispSizeY, tm->equ, tm->acv,
                    tm->prbOdd, tm->prbEven, tm->psbOdd, tm->psbEven,
                    HorVer.black);

    OSRestoreInterrupts(enabled);
}

/*---------------------------------------------------------------------------*
  Name:         VIFlush

  Description:  Set a flag to tell ISR to flush pending register settings

  Arguments:    None.

  Returns:      None.
 *---------------------------------------------------------------------------*/
void VIFlush(void)
{
    BOOL            enabled;
    s32             regIndex;

    // assert yorigin + height?

    enabled = OSDisableInterrupts();

    // copy pending changes to shadow registers.
    shdwChangeMode |= changeMode;
    changeMode = 0;

    shdwChanged |= changed;

    while(changed)
    {
        regIndex = cntlzd(changed);
        shdwRegs[regIndex] = regs[regIndex];

        changed &= ~VI_BITMASK(regIndex);
    }

    flushFlag = 1;

    // prepare for VIGetNextFrameBuffer
    NextBufAddr = HorVer.bufAddr;

    OSRestoreInterrupts(enabled);
}

/*---------------------------------------------------------------------------*
  Name:         VISetNextFrameBuffer

  Description:  Set the base address of the next frame buffer

  Arguments:    fb          address of the next frame buffer

  Returns:      None.
 *---------------------------------------------------------------------------*/
void VISetNextFrameBuffer(void *fb)
{
    BOOL            enabled;

#ifdef _DEBUG
    if ( ((u32)fb & 0x1f) != 0 )
        OSPanic(__FILE__, __LINE__,
                "VISetNextFrameBuffer(): Frame buffer address(0x%08x) is not 32byte aligned\n",
                 fb);
#ifdef BUG_16MB_XFB_LMT
    ASSERTMSG1(ToPhysical(fb) < 0x1000000,
               "VISetNextFrameBuffer(): Frame buffer start address(0x%08x) should be "
               "in lower 16MB (this is because of a hardware bug and should "
               "be fixed on the next chip)\n", fb);
#endif

#endif

    enabled = OSDisableInterrupts();

    HorVer.bufAddr = (u32)fb;
    FBSet = 1;

    setFbbRegs(&HorVer, &HorVer.tfbb, &HorVer.bfbb,
               &HorVer.rtfbb, &HorVer.rbfbb);

    OSRestoreInterrupts(enabled);
}

/*---------------------------------------------------------------------------*
  Name:         VIGetNextFrameBuffer

  Description:  Get the base address of the next frame buffer

  Arguments:    fb          address of the next frame buffer

  Returns:      None.
 *---------------------------------------------------------------------------*/
void *VIGetNextFrameBuffer(void)
{
    return (void*)NextBufAddr;
}

/*---------------------------------------------------------------------------*
  Name:         VIGetCurrentFrameBuffer

  Description:  Get the base address of the current frame buffer

  Arguments:    fb          address of the current frame buffer

  Returns:      None.
 *---------------------------------------------------------------------------*/
void *VIGetCurrentFrameBuffer(void)
{
    return (void*)CurrBufAddr;
}

/*---------------------------------------------------------------------------*
  Name:         VISetNextRightFrameBuffer

  Description:  Set the base address of the next right frame buffer

  Arguments:    fb          address of the next right frame buffer

  Returns:      None.
 *---------------------------------------------------------------------------*/
void VISetNextRightFrameBuffer(void *fb);

void VISetNextRightFrameBuffer(void *fb)
{
    BOOL            enabled;

#ifdef _DEBUG
    if ( ((u32)fb & 0x1f) != 0 )
        OSPanic(__FILE__, __LINE__,
                "VISetNextFrameBuffer(): Frame buffer address(0x%08x) is not 32byte aligned\n",
                 fb);
#ifdef BUG_16MB_XFB_LMT
    ASSERTMSG1(ToPhysical(fb) < 0x1000000,
               "VISetNextFrameBuffer(): Frame buffer start address(0x%08x) should be "
               "in lower 16MB (this is because of a hardware bug and should "
               "be fixed on the next chip)\n", fb);
#endif

#endif

    enabled = OSDisableInterrupts();

    HorVer.rbufAddr = (u32)fb;
    FBSet = 1;

    setFbbRegs(&HorVer, &HorVer.tfbb, &HorVer.bfbb,
               &HorVer.rtfbb, &HorVer.rbfbb);

    OSRestoreInterrupts(enabled);
}

/*---------------------------------------------------------------------------*
  Name:         VISetBlack

  Description:  Turn video to black

  Arguments:    black       TRUE to turn black, FALSE to turn on

  Returns:      None.
 *---------------------------------------------------------------------------*/
void VISetBlack(BOOL black)
{
    BOOL            enabled;
    timing_s*       tm;

    enabled = OSDisableInterrupts();

    HorVer.black = black;

    tm = HorVer.timing;

    setVerticalRegs(HorVer.AdjustedDispPosY, HorVer.DispSizeY, tm->equ, tm->acv,
                    tm->prbOdd, tm->prbEven, tm->psbOdd, tm->psbEven,
                    HorVer.black);

    OSRestoreInterrupts(enabled);
}

/*---------------------------------------------------------------------------*
  Name:         VISet3D

  Description:  Turn on 3D mode

  Arguments:    threeD          TRUE to turn on 3D, FALSE to turn off

  Returns:      None.
 *---------------------------------------------------------------------------*/
void VISet3D(BOOL threeD);

void VISet3D(BOOL threeD)
{
    BOOL            enabled;
    u32             reg;

    enabled = OSDisableInterrupts();

    HorVer.threeD = threeD;

    reg = regs[VI_DSP_CFG];
    VI_DSP_CFG_REG_SET_DLR(reg, HorVer.threeD);
    regs[VI_DSP_CFG] = (u16)reg;
    changed |= VI_BITMASK_DSP_CFG;

    setScalingRegs(HorVer.PanSizeX, HorVer.DispSizeX, HorVer.threeD);

    OSRestoreInterrupts(enabled);
}


/*---------------------------------------------------------------------------*
  Name:         VIGetRetraceCount

  Description:  Returns retrace count

  Arguments:    None.

  Returns:      Current retrace count
 *---------------------------------------------------------------------------*/
u32 VIGetRetraceCount(void)
{
    return retraceCount;
}


static void GetCurrentDisplayPosition(u32 *hct, u32 *vct)
{
    u32             hcount;
    u32             vcount0;
    u32             vcount;

    // we can't get tm from HorVer anymore since NTSC can change into PAL
    // in the middle of games; we need to use the current settings instead
    // of future settings

    vcount = (u32)VI_DSP_POS_U_REG_GET_VCT(__VIRegs[VI_DSP_POS_U]);
    do
    {
        vcount0 = vcount;
        hcount = (u32)VI_DSP_POS_L_REG_GET_HCT(__VIRegs[VI_DSP_POS_L]);
        vcount = (u32)VI_DSP_POS_U_REG_GET_VCT(__VIRegs[VI_DSP_POS_U]);
    } while (vcount0 != vcount);

    *hct = hcount;
    *vct = vcount;
}


/*---------------------------------------------------------------------------*
  Name:         getCurrentHalfLine

  Description:  Returns the current half line

  Arguments:    None.

  Returns:      current half line (range 0-1049 for NTSC)
 *---------------------------------------------------------------------------*/
static u32 getCurrentHalfLine(void)
{
    u32             hcount;
    u32             vcount;

    GetCurrentDisplayPosition(&hcount, &vcount);

    // subtract 1 from hcount because hcount is 1-origin.
    return ((vcount - 1) << 1) + ( (hcount-1) / CurrTiming->hlw );
}

/*---------------------------------------------------------------------------*
  Name:         VIGetCurrentFieldEvenOdd

  Description:  Returns the current field in terms of even/odd

  Arguments:    None.

  Returns:      Current field (1:odd 0:even)
 *---------------------------------------------------------------------------*/
static u32 getCurrentFieldEvenOdd(void)
{
    return (getCurrentHalfLine() < CurrTiming->nhlines)? 1u : 0u;
}


/*---------------------------------------------------------------------------*
  Name:         VIGetNextField

  Description:  Returns the next field

  Arguments:    None.

  Returns:      Next field (1:above 0:below)
 *---------------------------------------------------------------------------*/
u32 VIGetNextField(void)
{
    u32             nextField;
    BOOL            enabled;

    enabled = OSDisableInterrupts();
    nextField = getCurrentFieldEvenOdd() ^ 1;
    OSRestoreInterrupts(enabled);

    // even(0)/odd(1) -> above(1)/below(0)
    return (nextField ^ (HorVer.AdjustedDispPosY & 1));
}


/*---------------------------------------------------------------------------*
  Name:         VIGetCurrentLine

  Description:  Returns the current line

  Arguments:    None.

  Returns:      Current line (start from 0)
 *---------------------------------------------------------------------------*/
u32 VIGetCurrentLine(void)
{
    u32             halfLine;
    timing_s*       tm;
    BOOL            enabled;

    tm = CurrTiming;

    enabled = OSDisableInterrupts();
    halfLine = getCurrentHalfLine();
    OSRestoreInterrupts(enabled);

    if (halfLine >= tm->nhlines)
    {
        halfLine -= tm->nhlines;
    }

    return (halfLine >> 1);
}


/*---------------------------------------------------------------------------*
  Name:         VIGetTvFormat

  Description:  Returns TV format

  Arguments:    None.

  Returns:      TV format (VI_NTSC or VI_PAL or VI_EURGB60 or VI_MPAL)
 *---------------------------------------------------------------------------*/
u32 VIGetTvFormat(void)
{
    u32             format;
    BOOL            enabled;

    enabled = OSDisableInterrupts();

    switch (CurrTvMode)
    {
      case VI_NTSC:
      case VI_DEBUG:
      case VI_GCA:
        format = VI_NTSC;
        break;

      case VI_PAL:
      case VI_DEBUG_PAL:
        format = VI_PAL;
        break;

      case VI_EURGB60:
      case VI_MPAL:
        format = CurrTvMode;
        break;

      default:
        ASSERT(FALSE);
        break;
    }

    OSRestoreInterrupts(enabled);

    return format;
}


/*---------------------------------------------------------------------------*
  Name:         VIGetDtvStatus

  Description:  Returns the digital AV connector status

  Arguments:    None.

  Returns:      Digital AV connector status (1:connected 0:not connected)
 *---------------------------------------------------------------------------*/
u32 VIGetDTVStatus(void)
{
    u32             dtvStatus;
    BOOL            enabled;

    enabled = OSDisableInterrupts();
    dtvStatus = VI_DTVSTATUS_REG_GET_VISEL(__VIRegs[VI_DTVSTATUS]);
    OSRestoreInterrupts(enabled);

    return (dtvStatus & 0x01);
}


/*---------------------------------------------------------------------------*
  Name:         __VISetAdjustingValues

  Description:  set adjusting values

  Arguments:    x           x coordinate of the adjusting values
                y           y coordinate of the adjusting values

  Returns:      None.
 *---------------------------------------------------------------------------*/
void __VISetAdjustingValues(s16 x, s16 y)
{
    BOOL            enabled;
    timing_s*       tm;

    ASSERTMSG( (y & 1) == 0, "__VISetAdjustValues(): y offset should be an even number");

    enabled = OSDisableInterrupts();

    displayOffsetH = x;
    displayOffsetV = y;

    tm = HorVer.timing;

    // adjust
    AdjustPosition(tm->acv);

    setHorizontalRegs(tm, HorVer.AdjustedDispPosX, HorVer.DispSizeX);

    if (FBSet)
    {
        setFbbRegs(&HorVer, &HorVer.tfbb, &HorVer.bfbb,
                   &HorVer.rtfbb, &HorVer.rbfbb);
    }

    setVerticalRegs(HorVer.AdjustedDispPosY, HorVer.AdjustedDispSizeY, tm->equ, tm->acv,
                    tm->prbOdd, tm->prbEven, tm->psbOdd, tm->psbEven,
                    HorVer.black);

    OSRestoreInterrupts(enabled);
}

/*---------------------------------------------------------------------------*
  Name:         __VIGetAdjustingValues

  Description:  get adjusting values

  Arguments:    x           x coordinate of the adjusting values
                y           y coordinate of the adjusting values

  Returns:      None.
 *---------------------------------------------------------------------------*/
void __VIGetAdjustingValues(s16* x, s16* y)
{
    BOOL            enabled;

    enabled = OSDisableInterrupts();

    *x = displayOffsetH;
    *y = displayOffsetV;

    OSRestoreInterrupts(enabled);
}


/*---------------------------------------------------------------------------*
  Name:         __VISetRawPositionInterrupt

  Description:  Set interrupt that fires at a certain position

  Arguments:    x           x coordinate of the position
                y           y coordinate of the position
                callback

  Returns:      None.
 *---------------------------------------------------------------------------*/
void __VIEnableRawPositionInterrupt(s16 x, s16 y, VIPositionCallback callback)
{
    BOOL        enabled;
    u32         halfLine;
    u32         halfLineOff;

    enabled = OSDisableInterrupts();
    

    __VIRegs[VI_DSP_INT2_L] = (u16)VI_DSP_INT2_L_REG(x+1);
    __VIRegs[VI_DSP_INT3_L] = (u16)VI_DSP_INT3_L_REG(x+1);

    if (HorVer.nonInter == VI_INTERLACE)
    {
        if (y & 1)
        {
            // odd number
            halfLineOff = (u32)(CurrTiming->nhlines + CurrTiming->equ*3 +
                                CurrTiming->prbEven);
            __VIRegs[VI_DSP_INT3_U] = (u16)VI_DSP_INT3_U_REG(halfLineOff/2 + y/2+1, ENABLE, 0);
        }
        else
        {
            // even number
            halfLineOff = (u32)(CurrTiming->equ*3 + CurrTiming->prbOdd);
            __VIRegs[VI_DSP_INT2_U] = (u16)VI_DSP_INT2_U_REG(halfLineOff/2+y/2+1, ENABLE, 0);
        }
    }
    else if (HorVer.nonInter == VI_NON_INTERLACE)
    {
        ASSERT((y & 1) == 0);
        halfLine = (u32)(CurrTiming->equ*3 + CurrTiming->prbOdd + y);        
        __VIRegs[VI_DSP_INT2_U] = (u16)VI_DSP_INT2_U_REG(halfLine/2+1, ENABLE, 0);
        __VIRegs[VI_DSP_INT3_U] = (u16)VI_DSP_INT3_U_REG((halfLine + CurrTiming->nhlines)/2+1, ENABLE, 0);
    }
    else if (HorVer.nonInter == VI_PROGRESSIVE)
    {
        halfLine = (u32)(CurrTiming->equ*3 + CurrTiming->prbOdd + y);
        __VIRegs[VI_DSP_INT2_U] = (u16)VI_DSP_INT2_U_REG(halfLine+1, ENABLE, 0);
        __VIRegs[VI_DSP_INT3_U] = (u16)VI_DSP_INT3_U_REG(0, DISABLE, 0);
    }
    
    PositionCallback = callback;

    OSRestoreInterrupts(enabled);
}


VIPositionCallback __VIDisableRawPositionInterrupt(void)
{
    BOOL            enabled;
    VIPositionCallback      old;
    
    enabled = OSDisableInterrupts();
    
    __VIRegs[VI_DSP_INT2_U] = (u16)VI_DSP_INT2_U_REG(0, DISABLE, 0);
    __VIRegs[VI_DSP_INT3_U] = (u16)VI_DSP_INT3_U_REG(0, DISABLE, 0);

    old = PositionCallback;
    PositionCallback = NULL;
    
    OSRestoreInterrupts(enabled);
    
    return old;
}


void __VIDisplayPositionToXY(u32 hct, u32 vct, s16 *x, s16 *y)
{
    u32             halfLine;


    // calculate halfline (in 0-origin)
    halfLine = ((vct - 1) << 1) + ( (hct-1) / CurrTiming->hlw );
    

    // XXX 
    if (HorVer.nonInter == VI_INTERLACE)
    {
        if (halfLine < CurrTiming->nhlines)
        {
            if (halfLine < CurrTiming->equ*3 + CurrTiming->prbOdd)
                *y = -1;
            else if (halfLine >= CurrTiming->nhlines - CurrTiming->psbOdd)
                *y = -1;
            else 
                *y = (s16)((halfLine - CurrTiming->equ*3 - CurrTiming->prbOdd) & ~1);
        }
        else
        {
            halfLine -= CurrTiming->nhlines;
            
            if (halfLine < CurrTiming->equ*3 + CurrTiming->prbEven)
                *y = -1;
            else if (halfLine >= CurrTiming->nhlines - CurrTiming->psbEven)
                *y = -1;
            else 
                *y = (s16)(( (halfLine - CurrTiming->equ*3 - CurrTiming->prbEven) & ~1)+1);
        }
    }
    else if (HorVer.nonInter == VI_NON_INTERLACE)
    {
        // ds
        if (halfLine >= CurrTiming->nhlines)
        {
            halfLine -= CurrTiming->nhlines;
        }
        
        if (halfLine < CurrTiming->equ*3 + CurrTiming->prbOdd)
            *y = -1;
        else if (halfLine >= CurrTiming->nhlines - CurrTiming->psbOdd)
            *y = -1;
        else 
            *y = (s16)((halfLine - CurrTiming->equ*3 - CurrTiming->prbOdd) & ~1);
    }
    else if (HorVer.nonInter == VI_PROGRESSIVE)
    {
        // prog
        if (halfLine < CurrTiming->nhlines)
        {
            if (halfLine < CurrTiming->equ*3 + CurrTiming->prbOdd)
                *y = -1;
            else if (halfLine >= CurrTiming->nhlines - CurrTiming->psbOdd)
                *y = -1;
            else 
                *y = (s16)(halfLine - CurrTiming->equ*3 - CurrTiming->prbOdd);
        }
        else
        {
          // not reached
            halfLine -= CurrTiming->nhlines;

            if (halfLine < CurrTiming->equ*3 + CurrTiming->prbEven)
                *y = -1;
            else if (halfLine >= CurrTiming->nhlines - CurrTiming->psbEven)
                *y = -1;
            else 
                *y = (s16)((halfLine - CurrTiming->equ*3 - CurrTiming->prbEven) & ~1);
        }
    }

    *x = (s16)(hct - 1);
}


void __VIGetCurrentPosition(s16 *x, s16 *y)
{
    u32             hcount;
    u32             vcount;

    GetCurrentDisplayPosition(&hcount, &vcount);

    // 1 <= hcount <= 858, 1 <= vcount <= 525  (NTSC)
    // 1 <= hcount <= 864, 1 <= vcount <= 625  (PAL)

    __VIDisplayPositionToXY(hcount, vcount, x, y);
}


void __VISetLatchMode( u32 mode )
{
    u32  reg = __VIRegs[VI_DSP_CFG];
    VI_DSP_CFG_REG_SET_LE0( reg, mode );
    VI_DSP_CFG_REG_SET_LE1( reg, mode );
    __VIRegs[VI_DSP_CFG] = (u16)reg;
}


BOOL __VIGetLatch0Position( s16 *px, s16 *py )
{
    u32		hcount;
    u32		vcount;
    
    if ( VI_DSP_LATCH0_U_REG_GET_GUN0_TRG(__VIRegs[VI_DSP_LATCH0_U]) )
    {
	vcount = (u32)VI_DSP_LATCH0_U_REG_GET_GUN0_VCT(__VIRegs[VI_DSP_LATCH0_U]);
	hcount = (u32)VI_DSP_LATCH0_L_REG_GET_GUN0_HCT(__VIRegs[VI_DSP_LATCH0_L]);
	__VIRegs[VI_DSP_LATCH0_U] = 0;
	__VIRegs[VI_DSP_LATCH0_L] = 0;
	__VIDisplayPositionToXY( hcount, vcount, px, py );
	return TRUE;
    }
    *px = *py = -1;
    return FALSE;
}


BOOL __VIGetLatch1Position( s16 *px, s16 *py )
{
    u32		hcount;
    u32		vcount;
    
    if ( VI_DSP_LATCH1_U_REG_GET_GUN1_TRG(__VIRegs[VI_DSP_LATCH1_U]) )
    {
	vcount = (u32)VI_DSP_LATCH1_U_REG_GET_GUN1_VCT(__VIRegs[VI_DSP_LATCH1_U]);
	hcount = (u32)VI_DSP_LATCH1_L_REG_GET_GUN1_HCT(__VIRegs[VI_DSP_LATCH1_L]);
	__VIRegs[VI_DSP_LATCH1_U] = 0;
	__VIRegs[VI_DSP_LATCH1_L] = 0;
	__VIDisplayPositionToXY( hcount, vcount, px, py );
	return TRUE;
    }
    *px = *py = -1;
    return FALSE;
}


BOOL __VIGetLatchPosition( u32 port, s16 *px, s16 *py )
{
    return ( port == 0 ) ?
	__VIGetLatch0Position( px, py ) : __VIGetLatch1Position( px, py );
}
