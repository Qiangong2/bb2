/****************************************************************************
 * 
 *   (C) 1999 ARTX INC..  ALL RIGHTS RESERVED.  UNPUBLISHED -- RIGHTS
 *   RESERVED UNDER THE COPYRIGHT LAWS OF THE UNITED STATES.  USE OF A
 *   COPYRIGHT NOTICE IS PRECAUTIONARY ONLY AND DOES NOT IMPLY PUBLICATION
 *   OR DISCLOSURE.
 * 
 *   THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF
 *   ARTX INC..  USE, DISCLOSURE, OR REPRODUCTION IS PROHIBITED WITHOUT
 *   THE PRIOR EXPRESS WRITTEN PERMISSION OF ARTX INC..
 * 
 *                   RESTRICTED RIGHTS LEGEND
 * 
 *   Use, duplication, or disclosure by the Government is subject to
 *   restrictions as set forth in subparagraph (c)(1)(ii) of the Rights
 *   in Technical Data and Computer Software clause at DFARS 252.227-7013
 *   or subparagraphs (c)(1) and (2) of Commercial Computer Software --
 *   Restricted Rights at 48 CFR 52.227-19, as applicable. 
 * 
 *   ArtX Inc.
 *   3400 Hillview Ave, Bldg 5
 *   Palo Alto, CA 94304
 * 
 ***************************************************************************/

#include <dolphin/os.h>
#include "i2c.h"

#define UC_BIT    OS_BASE_UNCACHED

#define busWrt16(address, data)		  *(u16 *)((address) | UC_BIT) = data
#define busRd16(address)		  (*(u16*)((address) | UC_BIT))
#define busWrt32(address, data)		  *(u32 *)((address) | UC_BIT) = data
#define busRd32(address)		  (*(u32*)((address) | UC_BIT))

// prototypes
static void udelay(u32 usec);

static void udelay(u32 usec)
{
    OSTime     time;
    
    time = OSGetTime();
    
    while(OSTicksToMicroseconds(OSGetTime() - time) < usec)
        ;
}

static u16 shadowI2C;

#define I2C_ADDR 0x0c004118
#define CHIP_ID  0x0c00302c

void
__VISetSCL(int value)
{
    shadowI2C &= ~1;
    if (value)
	shadowI2C |= 1;
    busWrt16(I2C_ADDR, shadowI2C);
    udelay(5);
}

int
__VIGetSCL()
{
    u32 v;

    v = busRd32(CHIP_ID);
    return (v & 0x10000) ? 1 : 0;
}

void
__VISetSDA(int value)
{
    shadowI2C &= ~2;
    if (value)
	shadowI2C |= 2;
    busWrt16(I2C_ADDR, shadowI2C);
    udelay(5);
}

int
__VIGetSDA()
{
    u32 v;

    v = busRd32(CHIP_ID);
    return (v & 0x20000) ? 1 : 0;
}

