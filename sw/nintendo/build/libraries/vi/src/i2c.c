/****************************************************************************
 * 
 *   (C) 1999 ARTX INC..  ALL RIGHTS RESERVED.  UNPUBLISHED -- RIGHTS
 *   RESERVED UNDER THE COPYRIGHT LAWS OF THE UNITED STATES.  USE OF A
 *   COPYRIGHT NOTICE IS PRECAUTIONARY ONLY AND DOES NOT IMPLY PUBLICATION
 *   OR DISCLOSURE.
 * 
 *   THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF
 *   ARTX INC..  USE, DISCLOSURE, OR REPRODUCTION IS PROHIBITED WITHOUT
 *   THE PRIOR EXPRESS WRITTEN PERMISSION OF ARTX INC..
 * 
 *                   RESTRICTED RIGHTS LEGEND
 * 
 *   Use, duplication, or disclosure by the Government is subject to
 *   restrictions as set forth in subparagraph (c)(1)(ii) of the Rights
 *   in Technical Data and Computer Software clause at DFARS 252.227-7013
 *   or subparagraphs (c)(1) and (2) of Commercial Computer Software --
 *   Restricted Rights at 48 CFR 52.227-19, as applicable. 
 * 
 *   ArtX Inc.
 *   3400 Hillview Ave, Bldg 5
 *   Palo Alto, CA 94304
 * 
 ***************************************************************************/

#include <dolphin/os.h>
#include "i2c.h"

// No delay needed, getting stuff out the EXI adds enough delay
#define DELAY_5US

static int lastError;

//
// This I2C code assumes that we are the only master on the bus
// No arbitration, 7 bit addresses only
// 

// Support routines (static)

static int
wait4ClkHigh()
{
    int n;

    for (n = 0; n < 1000; n++) {
        if (__VIGetSCL())
            return I2C_SUCCESS;
    }        
    lastError = I2C_ERROR_NO_CLOCK;
    return I2C_FAILURE;
}

//
// Do start sequence, send slave addr, get ack
// Assume 7 bit slave addr has been shifted up one bit and LSB is r/w flag
//
static int
sendSlaveAddr(u8 slaveAddr)
{
    int i;

    // Start sequence
    __VISetSDA(0);
    DELAY_5US;
    __VISetSCL(0);

    // now send slave address
    for (i = 0; i < 8; i++) {
        if (slaveAddr & 0x80)
            __VISetSDA(1);
        else
            __VISetSDA(0);

        DELAY_5US;
        __VISetSCL(1);
        if (!wait4ClkHigh()) {
            return I2C_FAILURE;
        }
        __VISetSCL(0);
        slaveAddr <<= 1;
    }

    // Master is xmitting, so rcvr should Ack
    __VISetSDA(1);
    DELAY_5US;
    __VISetSCL(1);
    if (!wait4ClkHigh()) {
        return I2C_FAILURE;
    }
    if (__VIGetSDA() != 0) {
        lastError = I2C_ERROR_NO_ACK;
        return I2C_FAILURE;
    }
    __VISetSCL(0);
    return I2C_SUCCESS;
}


// Public routines

//
// Send the data to the slave
//
int
__VISendI2CData(u8 slaveAddr, u8 *pData, int nBytes)
{
    int i;
    u8 data;

    if (!sendSlaveAddr(slaveAddr)) {
        return I2C_FAILURE;
    }

    // Now send all the bytes.  Look for an ACK from the slave after
    // each byte except for the last byte. (The last ACK is a don't care)
    while (nBytes) {
        data = *pData++;
        for (i = 0; i < 8; i++) {
            if (data & 0x80)
                __VISetSDA(1);
            else
                __VISetSDA(0);

            DELAY_5US;
            __VISetSCL(1);
            if (!wait4ClkHigh()) {
                return I2C_FAILURE;
            }
            __VISetSCL(0);
            data <<= 1;
        }

        // Master is xmitting, so rcvr should Ack (last byte is don't care)
        __VISetSDA(1);
        DELAY_5US;
        __VISetSCL(1);
        if (!wait4ClkHigh()) {
            return I2C_FAILURE;
        }
        if (nBytes != 1 && __VIGetSDA() != 0) {
            lastError = I2C_ERROR_NO_ACK;
            return I2C_FAILURE;
        }
        __VISetSCL(0);
        DELAY_5US;
        nBytes--;
    }

    // Now Stop condition
    __VISetSDA(0);
    DELAY_5US;
    __VISetSCL(1);
    DELAY_5US;
    __VISetSDA(1);

    return I2C_SUCCESS;
}

