=========================================================
        3D display user's manual
                                        2/6/02 IRD, NCL
=========================================================

===============
1. Overview
===============
This package is for those who are making applications for 
3D display unit.


===============
2. Environment
===============
To use this package, you need to prepare 3D display unit
connected to digital out connector of DDH/GDEV/NR reader,
etc.

You also need to use 12/12 SDK or later to use this device.


===============
3. Software
===============
You need to prepare two XFBs, one is for the left eye and
the other is for the right. Each pixel of the image is
mixtured and sent to the HW. HW then shows the two images
properly.

The following is the list of items different from NTSC:
- Need to include a special header file
- Need to use a different render mode
- Need to use a new API to specify the next "right" frame
buffer

Each item will be discussed in detail in the sections 
below.

---------------
3.1 Header file
---------------
You need to include vi3d.h in your application to use
the additional definitions/APIs.

You can use the same VI library included in SDK if you are
using 12/12 SDK or later. SDKs of 9/8 or earlier is not
supported.

---------------
3.2 Render Mode
---------------
You need to modify the GXRenderModeObj from the one for
NTSC "progressive" like following:

VITvMode -> VI_TVMODE_NTSC_3D
XfbWidth -> 320 (half of the resolution because this entry
   specifies the width of each XFB)

Here's the example of render mode for 3D.

================================================
GXRenderModeObj GXNtsc4803D = 
{
    VI_TVMODE_NTSC_3D, // viDisplayMode
    320,             // fbWidth
    480,             // efbHeight
    480,             // xfbHeight
    (VI_MAX_WIDTH_NTSC - 640)/2,        // viXOrigin
    (VI_MAX_HEIGHT_NTSC - 480)/2,       // viYOrigin
    640,             // viWidth
    480,             // viHeight
    VI_XFBMODE_SF,   // xFBmode
    GX_FALSE,        // field_rendering
    GX_FALSE,        // aa

    // sample points arranged in increasing Y order
     6,  6,  6,  6,  6,  6,  // pix 0, 3 sample points, 1/12 units, 4 bits each
     6,  6,  6,  6,  6,  6,  // pix 1
     6,  6,  6,  6,  6,  6,  // pix 2
     6,  6,  6,  6,  6,  6,  // pix 3

    // vertical filter[7], 1/64 units, 6 bits each
     0,         // line n-1
     0,         // line n-1
    21,         // line n
    22,         // line n
    21,         // line n
     0,         // line n+1
     0          // line n+1
};
================================================

---------------
3.3 New API
---------------
The following is the new API to specify the next "right"
frame buffer address:

void VISetNextRightFrameBuffer(void *fb);

It behaves just like VISetNextFrameBuffer.

Note that VISetNextFrameBuffer should also be used to
specify the address for the next "left" frame buffer.


===============
4. Caution
===============
- Please use offset 0 for position adjusting you can set
in IPL menu in production mode.

