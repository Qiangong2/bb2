/****************************************************************************
 * 
 *   (C) 1999 ARTX INC..  ALL RIGHTS RESERVED.  UNPUBLISHED -- RIGHTS
 *   RESERVED UNDER THE COPYRIGHT LAWS OF THE UNITED STATES.  USE OF A
 *   COPYRIGHT NOTICE IS PRECAUTIONARY ONLY AND DOES NOT IMPLY PUBLICATION
 *   OR DISCLOSURE.
 * 
 *   THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF
 *   ARTX INC..  USE, DISCLOSURE, OR REPRODUCTION IS PROHIBITED WITHOUT
 *   THE PRIOR EXPRESS WRITTEN PERMISSION OF ARTX INC..
 * 
 *                   RESTRICTED RIGHTS LEGEND
 * 
 *   Use, duplication, or disclosure by the Government is subject to
 *   restrictions as set forth in subparagraph (c)(1)(ii) of the Rights
 *   in Technical Data and Computer Software clause at DFARS 252.227-7013
 *   or subparagraphs (c)(1) and (2) of Commercial Computer Software --
 *   Restricted Rights at 48 CFR 52.227-19, as applicable. 
 * 
 *   ArtX Inc.
 *   3400 Hillview Ave, Bldg 5
 *   Palo Alto, CA 94304
 * 
 ***************************************************************************/

#ifndef _I2C_H_
#define _I2C_H_

// Defines
#define I2C_SUCCESS 1
#define I2C_FAILURE 0

// Error define
#define I2C_ERROR_NO_ACK	0x0001
#define I2C_ERROR_NO_CLOCK	0x0002


// Function prototypes
int __VISendI2CData(u8 slaveAddr, u8 *pData, int nBytes);
void __VIInitI2C();

void __VISetSCL(int);
void __VISetSDA(int);
int  __VIGetSCL(void);
int  __VIGetSDA(void);

#endif // _I2C_H_
