/*---------------------------------------------------------------------------*
  Project:  Dolphin OS
  File:     vilocal.h

  Copyright 1998, 1999 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: vilocal.h,v $
  Revision 1.1.1.1  2004/01/03 01:02:28  paulm
  Gamecube library source from Nintendo

    
    13    10/31/00 2:52p Hashida
    Fixed code for 16MB over frame buffer address support.
    
    12    10/05/00 10:59p Hashida
    Changed so that adjusting works even when dispSizeY is 480.
    
    11    9/16/00 2:54a Hashida
    Added VI adjusting feature.
    
    10    8/11/00 6:25p Hashida
    Added 3D and progressive mode support
    
    9     6/30/00 7:16p Hashida
    Added philips 10bit encoder support for Orca
    Clean up.
    
    8     5/15/00 1:39p Hashida
    Changed maximum height from 482 to 480 for NTSC and MPAL.
    
    7     1/31/00 9:57a Hashida
    bug fix
    
    6     1/28/00 4:16p Hashida
    Added definitions for change mode.
    
    5     1/12/00 4:21p Hashida
    added jumper switch definitions
    
    4     12/17/99 11:06a Hashida
    Added some definitions.
    Changed u8 -> u16 for prb and psb.
    
    3     12/03/99 2:44p Hashida
    Added hbe and hbs for CCIR656
    
    2     11/23/99 10:52a Hashida
    Added bitmask definitions for CLKSEL, DTVSTATUS and WIDTH registers.
    
    4     11/12/99 5:20p Hashida
    Added nhline in timing_s structure.
    
    3     10/07/99 10:25a Hashida
    Removed equ from HorVer structure.
    
    2     10/04/99 9:50a Hashida
    Changed timing_s and HorVer_s structures.
    
    1     9/29/99 6:13p Hashida
    Initial revision
    
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#ifndef __VILOCAL_H__
#define __VILOCAL_H__

#ifdef __cplusplus
extern "C" {
#endif

// Dolphin header files
#include <private/flipper.h>

// for cntlzw
#define VI_BITMASK(index)               (1ull << (63 - (index)))

#define VI_BITMASK_VER_TIM              VI_BITMASK(VI_VER_TIM)
#define VI_BITMASK_DSP_CFG              VI_BITMASK(VI_DSP_CFG)
#define VI_BITMASK_HOR_TIM0_U           VI_BITMASK(VI_HOR_TIM0_U)
#define VI_BITMASK_HOR_TIM0_L           VI_BITMASK(VI_HOR_TIM0_L)
#define VI_BITMASK_HOR_TIM1_U           VI_BITMASK(VI_HOR_TIM1_U)
#define VI_BITMASK_HOR_TIM1_L           VI_BITMASK(VI_HOR_TIM1_L)
#define VI_BITMASK_VER_ODD_TIM_U        VI_BITMASK(VI_VER_ODD_TIM_U)
#define VI_BITMASK_VER_ODD_TIM_L        VI_BITMASK(VI_VER_ODD_TIM_L)
#define VI_BITMASK_VER_EVN_TIM_U        VI_BITMASK(VI_VER_EVN_TIM_U)
#define VI_BITMASK_VER_EVN_TIM_L        VI_BITMASK(VI_VER_EVN_TIM_L)
#define VI_BITMASK_ODD_BBLNK_INTVL_U    VI_BITMASK(VI_ODD_BBLNK_INTVL_U)
#define VI_BITMASK_ODD_BBLNK_INTVL_L    VI_BITMASK(VI_ODD_BBLNK_INTVL_L)
#define VI_BITMASK_EVN_BBLNK_INTVL_U    VI_BITMASK(VI_EVN_BBLNK_INTVL_U)
#define VI_BITMASK_EVN_BBLNK_INTVL_L    VI_BITMASK(VI_EVN_BBLNK_INTVL_L)
#define VI_BITMASK_PIC_BASE_LFT_U       VI_BITMASK(VI_PIC_BASE_LFT_U)
#define VI_BITMASK_PIC_BASE_LFT_L       VI_BITMASK(VI_PIC_BASE_LFT_L)
#define VI_BITMASK_PIC_BASE_RGT_U       VI_BITMASK(VI_PIC_BASE_RGT_U)
#define VI_BITMASK_PIC_BASE_RGT_L       VI_BITMASK(VI_PIC_BASE_RGT_L)
#define VI_BITMASK_PIC_BASE_LFT_BOT_U   VI_BITMASK(VI_PIC_BASE_LFT_BOT_U)
#define VI_BITMASK_PIC_BASE_LFT_BOT_L   VI_BITMASK(VI_PIC_BASE_LFT_BOT_L)
#define VI_BITMASK_PIC_BASE_RGT_BOT_U   VI_BITMASK(VI_PIC_BASE_RGT_BOT_U)
#define VI_BITMASK_PIC_BASE_RGT_BOT_L   VI_BITMASK(VI_PIC_BASE_RGT_BOT_L)
#define VI_BITMASK_DSP_POS_U            VI_BITMASK(VI_DSP_POS_U)
#define VI_BITMASK_DSP_POS_L            VI_BITMASK(VI_DSP_POS_L)
#define VI_BITMASK_DSP_INT0_U           VI_BITMASK(VI_DSP_INT0_U)
#define VI_BITMASK_DSP_INT0_L           VI_BITMASK(VI_DSP_INT0_L)
#define VI_BITMASK_DSP_INT1_U           VI_BITMASK(VI_DSP_INT1_U)
#define VI_BITMASK_DSP_INT1_L           VI_BITMASK(VI_DSP_INT1_L)
#define VI_BITMASK_DSP_INT2_U           VI_BITMASK(VI_DSP_INT2_U)
#define VI_BITMASK_DSP_INT2_L           VI_BITMASK(VI_DSP_INT2_L)
#define VI_BITMASK_DSP_INT3_U           VI_BITMASK(VI_DSP_INT3_U)
#define VI_BITMASK_DSP_INT3_L           VI_BITMASK(VI_DSP_INT3_L)
#define VI_BITMASK_DSP_LATCH0_U         VI_BITMASK(VI_DSP_LATCH0_U)
#define VI_BITMASK_DSP_LATCH0_L         VI_BITMASK(VI_DSP_LATCH0_L)
#define VI_BITMASK_DSP_LATCH1_U         VI_BITMASK(VI_DSP_LATCH1_U)
#define VI_BITMASK_DSP_LATCH1_L         VI_BITMASK(VI_DSP_LATCH1_L)
#define VI_BITMASK_PIC_CFG              VI_BITMASK(VI_PIC_CFG)
#define VI_BITMASK_HSCALE               VI_BITMASK(VI_HSCALE)
#define VI_BITMASK_FLTR_COEFF0_U        VI_BITMASK(VI_FLTR_COEFF0_U)
#define VI_BITMASK_FLTR_COEFF0_L        VI_BITMASK(VI_FLTR_COEFF0_L)
#define VI_BITMASK_FLTR_COEFF1_U        VI_BITMASK(VI_FLTR_COEFF1_U)
#define VI_BITMASK_FLTR_COEFF1_L        VI_BITMASK(VI_FLTR_COEFF1_L)
#define VI_BITMASK_FLTR_COEFF2_U        VI_BITMASK(VI_FLTR_COEFF2_U)
#define VI_BITMASK_FLTR_COEFF2_L        VI_BITMASK(VI_FLTR_COEFF2_L)
#define VI_BITMASK_FLTR_COEFF3_U        VI_BITMASK(VI_FLTR_COEFF3_U)
#define VI_BITMASK_FLTR_COEFF3_L        VI_BITMASK(VI_FLTR_COEFF3_L)
#define VI_BITMASK_FLTR_COEFF4_U        VI_BITMASK(VI_FLTR_COEFF4_U)
#define VI_BITMASK_FLTR_COEFF4_L        VI_BITMASK(VI_FLTR_COEFF4_L)
#define VI_BITMASK_FLTR_COEFF5_U        VI_BITMASK(VI_FLTR_COEFF5_U)
#define VI_BITMASK_FLTR_COEFF5_L        VI_BITMASK(VI_FLTR_COEFF5_L)
#define VI_BITMASK_FLTR_COEFF6_U        VI_BITMASK(VI_FLTR_COEFF6_U)
#define VI_BITMASK_FLTR_COEFF6_L        VI_BITMASK(VI_FLTR_COEFF6_L)
#define VI_BITMASK_OUTPOL               VI_BITMASK(VI_OUTPOL)
#define VI_BITMASK_CLKSEL               VI_BITMASK(VI_CLKSEL)
#define VI_BITMASK_DTVSTATUS            VI_BITMASK(VI_DTVSTATUS)
#define VI_BITMASK_WIDTH                VI_BITMASK(VI_WIDTH)

#define VI_HLW_NTSC                 429
#define VI_HLW_PAL                  432
#define VI_HLW_MPAL                 429

#define VI_HSY_NTSC                 64
#define VI_HSY_PAL                  64
#define VI_HSY_MPAL                 64

#define VI_HCS_NTSC                 71
#define VI_HCS_PAL                  75
#define VI_HCS_MPAL                 78

#define VI_HCE_NTSC                 105
#define VI_HCE_PAL                  106
#define VI_HCE_MPAL                 112

#define VI_HBE640_NTSC              162
#define VI_HBE640_PAL               172
#define VI_HBE640_MPAL              162

#define VI_HBS640_NTSC              373
#define VI_HBS640_PAL               380
#define VI_HBS640_MPAL              373

#define VI_EQU_NTSC                 6
#define VI_ACV_NTSC                 240
#define VI_PRBODD_NTSC              24
#define VI_PRBEVN_NTSC              25
#define VI_PRBDS_NTSC               24
#define VI_PSBODD_NTSC              3
#define VI_PSBEVN_NTSC              2
#define VI_PSBDS_NTSC               4
#define VI_BS1_NTSC                 12
#define VI_BS2_NTSC                 13
#define VI_BS3_NTSC                 12
#define VI_BS4_NTSC                 13
#define VI_BSDS_NTSC                12
#define VI_BE1_NTSC                 520
#define VI_BE2_NTSC                 519
#define VI_BE3_NTSC                 520
#define VI_BE4_NTSC                 519
#define VI_BEDS_NTSC                520

#define VI_EQU_PAL                  5
#define VI_ACV_PAL                  287
#define VI_PRBODD_PAL               35
#define VI_PRBEVN_PAL               36
#define VI_PRBDS_PAL                35
#define VI_PSBODD_PAL               1
#define VI_PSBEVN_PAL               0
#define VI_PSBDS_PAL                2
#define VI_BS1_PAL                  12
#define VI_BS2_PAL                  13
#define VI_BS3_PAL                  12
#define VI_BS4_PAL                  13
#define VI_BSDS_PAL                 12
#define VI_BE1_PAL                  520
#define VI_BE2_PAL                  519
#define VI_BE3_PAL                  520
#define VI_BE4_PAL                  519
#define VI_BEDS_PAL                 520

#define VI_EQU_MPAL                 6

// 1 word = 16 pixels = 32 bytes
#define VI_PIXELS_PER_WORD          16
#define VI_BYTES_PER_PIXEL          2
#define VI_BYTES_PER_WORD           (VI_BYTES_PER_PIXEL * VI_PIXELS_PER_WORD)

typedef struct
{
    // Vertical timing
    u8      equ;
    u16     acv;

    u16     prbOdd;
    u16     prbEven;

    u16     psbOdd;
    u16     psbEven;

    u8      bs1;
    u8      bs2;
    u8      bs3;
    u8      bs4;

    u16     be1;
    u16     be2;
    u16     be3;
    u16     be4;

    u16     nhlines;    // # of half lines per field

    // Horizontal timing
    u16     hlw;
    u8      hsy;
    u8      hcs;
    u8      hce;
    u8      hbe640;             // for 640 pixel width
    u16     hbs640;             // for 640 pixel width
    u8      hbeCCIR656;         // for CCIR656
    u16     hbsCCIR656;         // for CCIR656
    
} timing_s;

typedef struct
{
    u16     DispPosX;
    u16     DispPosY;
    u16     DispSizeX;
    u16     DispSizeY;
    
    u16     AdjustedDispPosX;
    u16     AdjustedDispPosY;
    u16     AdjustedDispSizeY;
    u16     AdjustedPanPosY;
    u16     AdjustedPanSizeY;

    u16     FBSizeX;
    u16     FBSizeY;
    
    u16     PanPosX;
    u16     PanPosY;
    u16     PanSizeX;
    u16     PanSizeY;

    VIXFBMode     FBMode;

    u32     nonInter;
    u32     tv;

    u8      wordPerLine;
    u8      std;
    u8      wpl;

    u32     bufAddr;
    u32     tfbb;
    u32     bfbb;
    u8      xof;

    BOOL    black;

    BOOL    threeD;
    u32     rbufAddr;
    u32     rtfbb;
    u32     rbfbb;

    timing_s* timing;

} HorVer_s;
    
#ifndef SPRUCE
// to support both Rohm and Philips encodes
#define VIDEO_ENCODER_JUMPER    0x2 // Bit one in DI Config
#define VIDEO_ENCODER_PHILIPS   0
#define VIDEO_ENCODER_ROHM      1

#endif

#define CHANGE_MODE_NONE            0
#define CHANGE_MODE_START           1

#ifdef __cplusplus
}
#endif

#endif // __VILOCAL_H__
