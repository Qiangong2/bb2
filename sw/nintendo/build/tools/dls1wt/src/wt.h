/*---------------------------------------------------------------------------*
  Project:  Wavetable application for AX
  File:     wt.h

  Copyright 1998, 1999, 2000 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: wt.h,v $
  Revision 1.1.1.1  2004/06/09 17:41:00  paulm
  GC tools from Nintendo SDK

    
    1     5/20/01 11:48p Eugene
    DLS1.0 WaveTable converter. For use with AX/SEQ/SYN/MIX. 
    Source code provided to developers so they may create their own tool
    chain. MSVC++ project (bleah). 
    
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#ifndef __WT_H__
#define __WT_H__

#ifdef __cplusplus
extern "C" {
#endif

/*
#define WT_ART_FLAG_LFO     0x00000001
#define WT_ART_FLAG_EG1     0x00000002
#define WT_ART_FLAG_EG2     0x00000004    

#define WT_ADSR_ATTACK      0
#define WT_ADSR_DECAY       1
#define WT_ADSR_SUSTAIN     2    
#define WT_ADSR_RELEASE     3
*/
    
typedef struct WTART    // translated DLS articulartion block
{
    // LFO
    s32 lfoFreq;
    s32 lfoDelay;
    s32 lfoAtten;
    s32 lfoPitch;
    s32 lfoMod2Atten;
    s32 lfoMod2Pitch;

    // EG1
    s32 eg1Attack;
    s32 eg1Decay;
    s32 eg1Sustain;
    s32 eg1Release;
    s32 eg1Vel2Attack;
    s32 eg1Key2Decay;

    // EG2
    s32 eg2Attack;
    s32 eg2Decay;
    s32 eg2Sustain;
    s32 eg2Release;
    s32 eg2Vel2Attack;
    s32 eg2Key2Decay;
    s32 eg2Pitch;

    // pan
    s32 pan;

} WTART;

typedef struct WTREGION
{

//    u8  loKey;
//    u8  hiKey;
//    u8  loVel;
//    u8  hiVel;
    u8  unityNote;
    u8  keyGroup;
    
    s16 fineTune;
    s32 attn;
    
    u32 loopStart;
    u32 loopLength;

    u32 articulationIndex;  // articulation index to reference
    u32 sampleIndex;        // sample index to reference

} WTREGION;

typedef struct WTSAMPLE
{
    u16 format;     // ADPCM, PCM16, PCM8  
    u16 sampleRate; // Hz
    u32 offset;     // offset in bytes from beginning of PCM file
    u32 length;     // length of sample in bytes
    u16 adpcmIndex; // ADPCM index to reference if in ADPCM mode 

} WTSAMPLE;

#define WT_FORMAT_ADPCM 0 
#define WT_FORMAT_PCM16 1
#define WT_FORMAT_PCM8  2

typedef struct WTADPCM
{

    // values to program at start
    u16     a[8][2];            //  coef table a1[0],a2[0],a1[1],a2[1]....
    u16     gain;               //  gain to be applied (0 for ADPCM, 0x0800 for PCM8/16)
    u16     pred_scale;         //  predictor / scale combination (nibbles, as in hardware)
    u16     yn1;                //  y[n - 1]
    u16     yn2;                //  y[n - 2]

    // loop context
    u16     loop_pred_scale;    //  predictor / scale combination (nibbles, as in hardware)
    u16     loop_yn1;           //  y[n - 1]
    u16     loop_yn2;           //  y[n - 2]

} WTADPCM;

typedef struct WTINST   // insturment
{

    u16 keyRegion[128];

} WTINST;

typedef struct WTFILEHEADER   // FILE
{

    u32 offsetPercussiveInst;
    u32 offsetMelodicInst;
    u32 offsetRegions;
    u32 offsetArticulations;
    u32 offsetSamples;
    u32 offsetAdpcmContext;

    // data

} WTFILEHEADER;


#ifdef __cplusplus
}
#endif

#endif // __WT_H__ 
