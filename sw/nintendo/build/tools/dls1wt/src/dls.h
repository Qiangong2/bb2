/*--------------------------------------------------------------------------*
  Project: DLS converter for SYN
  File:    dls.h

  Copyright 1998, 1999 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.
 *--------------------------------------------------------------------------*/

#ifndef __DLS_H__
#define __DLS_H__

#ifdef __cplusplus
extern "C" {
#endif


/*--------------------------------------------------------------------------*/

#define ChunkName(a,b,c,d)(	    	\
				(a & 0xFF)	        \
			+	((b & 0xFF) << 8)	\
			+	((c & 0xFF) << 16)	\
			+	((d & 0xFF) << 24))


#define RIFF    ChunkName('R','I','F','F')
#define DLS     ChunkName('D','L','S',' ') 
#define LIST    ChunkName('L','I','S','T')
#define COLH    ChunkName('c','o','l','h')
#define VERS    ChunkName('v','e','r','s')
#define LINS    ChunkName('l','i','n','s')
#define INS     ChunkName('i','n','s',' ')
#define INSH    ChunkName('i','n','s','h')
#define LRGN    ChunkName('l','r','g','n')
#define RGN     ChunkName('r','g','n',' ')
#define RGNH    ChunkName('r','g','n','h')
#define WSMP    ChunkName('w','s','m','p')
#define WLNK    ChunkName('w','l','n','k')
#define LART    ChunkName('l','a','r','t')
#define LAR2    ChunkName('l','a','r','2')
#define ART1    ChunkName('a','r','t','1')
#define DATA    ChunkName('d','a','t','a')
#define PTBL    ChunkName('p','t','b','l')
#define DLID    ChunkName('d','l','i','d')
#define WVPL    ChunkName('w','v','p','l')
#define WAVE    ChunkName('w','a','v','e')
#define WAVU    ChunkName('w','a','v','u')
#define FMT     ChunkName('f','m','t',' ')
#define INFO    ChunkName('I','N','F','O')
#define IARL    ChunkName('I','A','R','L')
#define IART    ChunkName('I','A','R','T')
#define ICMS    ChunkName('I','C','M','S')
#define ICMT    ChunkName('I','C','M','T')
#define ICOP    ChunkName('I','C','O','P')
#define ICRD    ChunkName('I','C','R','D')
#define IENG    ChunkName('I','E','N','G')
#define IGNR    ChunkName('I','G','N','R')
#define IKEY    ChunkName('I','K','E','Y')
#define IMED    ChunkName('I','M','E','D')
#define INAM    ChunkName('I','N','A','M')
#define IPRD    ChunkName('I','P','R','D')
#define ISBJ    ChunkName('I','S','B','J')
#define ISFT    ChunkName('I','S','F','T')
#define ISRC    ChunkName('I','S','R','C')
#define ISRF    ChunkName('I','S','R','F')
#define ITCH    ChunkName('I','T','C','H')
#define SMPL	ChunkName('s','m','p','l')
#define ISTD    ChunkName('I','S','T','D')

// added these to support Microsoft's DLS spec *chuckle*
#define GUID	ChunkName('g','u','i','d')
#define WAVH	ChunkName('w','a','v','h')


/*--------------------------------------------------------------------------*/

//	standard DLS 1.0 sample loop descriptor
typedef struct _DLS_LOOP
{
	
	u32	size;
	u32	type;
	u32	start;
	u32	length;

}	DLS_LOOP, *PDLS_LOOP;


/*--------------------------------------------------------------------------*/

//	standard DLS 1.0 wsmp chunk
typedef struct _DLS_WSMP
{

	u32	size;
	u16	unityNote;
	s16	fineTune;
	s32	attenuation;
	u32	options;
	u32	sampleLoops;
	DLS_LOOP		loop[];

} DLS_WSMP, *PDLS_WSMP;


/*--------------------------------------------------------------------------*/

//	standard DLS 1.0 fmt chunk
typedef struct _DLS_FMT
{

	u16	formatTag;
	u16	channels;
	u32	samplesPerSec;
	u32	avgBytesPerSec;
	u16	blockAlign;
	u16	bitsPerSample;

} DLS_FMT, *PDLS_FMT;


/*--------------------------------------------------------------------------*/

//	standard DLS 1.0 rgnh chunk
typedef struct _DLS_RGNH
{

	u16	lowKey;
	u16	hiKey;
	u16	lowVelocity;
	u16	hiVelocity;
	u16	options;
	u16	keyGroup;

} DLS_RGNH, *PDLS_RGNH;


/*--------------------------------------------------------------------------*/

//	standard DLS 1.0 wlnk chunk
typedef struct _DLS_WLNK
{

	u16	options;
	u16	phase;
	u32	channel;
	u32	index;

} DLS_WLNK, *PDLS_WLNK;


/*--------------------------------------------------------------------------*/

//	standard DLS 1.0 connection block
typedef struct _DLS_CONNECTION
{
	
	u16	source;
	u16	control;
	u16	destination;
	u16	transform;
	s32	scale;

} DLS_CONNECTION, *PDLS_CONNECTION;


/*--------------------------------------------------------------------------*/

#define	CONN_SRC_NONE				0x0000	
#define CONN_SRC_LFO				0x0001
#define CONN_SRC_KEYONVELOCITY		0x0002
#define CONN_SRC_KEYNUMBER			0x0003
#define CONN_SRC_EG1				0x0004
#define CONN_SRC_EG2				0x0005
#define CONN_SRC_PITCHWHEEL			0x0006

#define CONN_SRC_CC1				0x0081	//	mod wheel
#define CONN_SRC_CC7				0x0087	//	channel volume
#define CONN_SRC_CC10				0x008A	//	pan
#define CONN_SRC_CC11				0x008B	//	expression
#define CONN_SRC_RPN0				//	???	//	pitch bend range
#define CONN_SRC_RPN1				//	???	//	fine tune
#define CONN_SRC_RPN2				//	???	//	coarse tune

#define CONN_DST_NONE				0x0000
#define CONN_DST_ATTENUATION		0x0001
#define CONN_DST_RESERVED			0x0002
#define CONN_DST_PITCH				0x0003
#define CONN_DST_PAN				0x0004

#define CONN_DST_LFO_FREQUENCY		0x0104
#define CONN_DST_LFO_STARTDELAY		0x0105

#define CONN_DST_EG1_ATTACKTIME		0x0206
#define CONN_DST_EG1_DECAYTIME		0x0207
#define CONN_DST_EG1_RESERVED		0x0208
#define CONN_DST_EG1_RELEASETIME	0x0209
#define CONN_DST_EG1_SUSTAINLEVEL	0x020A	

#define CONN_DST_EG2_ATTACKTIME		0x030A
#define CONN_DST_EG2_DECAYTIME		0x030B
#define CONN_DST_EG2_RESERVED		0x030C
#define CONN_DST_EG2_RELEASETIME	0x030D
#define CONN_DST_EG2_SUSTAINLEVEL	0x030E

#define CONN_TRN_NONE				0x0000
#define CONN_TRN_CONCAVE			0x0001

// compression mode
#define MODE_USE_FLAG       0
#define MODE_COMPRESS_ALL   1
#define MODE_COMPRESS_NONE  2

/*--------------------------------------------------------------------------*/
void	dls_read_file               (FILE *dlsFile, FILE *wtFile, FILE *pcmFile, int mode);

/*--------------------------------------------------------------------------*/

#ifdef __cplusplus
}
#endif

#endif  //__DLS_H__






































