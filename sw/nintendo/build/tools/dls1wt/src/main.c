#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include "types.h"
#include "wt.h"
#include "dls.h"
#include "dspadpcm.h"

/*--------------------------------------------------------------------------*/
static FILE *dlsFile;
static FILE *wtFile;
static FILE *pcmFile;
static HINSTANCE   hDll;

typedef u32 (*lpFunc1)(u32);
typedef u32 (*lpFunc2)(void);
typedef void (*lpFunc3)(s16*, u8*, ADPCMINFO*, u32);
typedef void (*lpFunc4)(u8*, s16*, ADPCMINFO*, u32);
typedef void (*lpFunc5)(u8*, ADPCMINFO*, u32);

lpFunc1 getBytesForAdpcmBuffer;
lpFunc1 getBytesForAdpcmSamples;
lpFunc1 getBytesForPcmBuffer;
lpFunc1 getNibbleAddress;
lpFunc2 getBytesForAdpcmInfo;
lpFunc3 encode;
lpFunc4 decode;
lpFunc5 getLoopContext;

/*--------------------------------------------------------------------------*/
void clean_up(void)
{
    // close files
    if (dlsFile)    fclose(dlsFile);
    if (wtFile)     fclose(wtFile);
    if (pcmFile)    fclose(pcmFile);
    if (hDll)       FreeLibrary(hDll);
}


/*--------------------------------------------------------------------------*/
int getDll(void)
{
    hDll = LoadLibrary("dsptool.dll");

    if (hDll)
    {
        if (!(getBytesForAdpcmBuffer =
                (lpFunc1)GetProcAddress(
                            hDll,
                            "getBytesForAdpcmBuffer"
                            ))) return 1;
        
        if (!(getBytesForAdpcmSamples =
                (lpFunc1)GetProcAddress(
                            hDll,
                            "getBytesForAdpcmSamples"
                            ))) return 1;

        if (!(getBytesForPcmBuffer =
                (lpFunc1)GetProcAddress(
                            hDll,
                            "getBytesForPcmBuffer"
                            ))) return 1;

        if (!(getBytesForAdpcmInfo =
                (lpFunc2)GetProcAddress(
                            hDll,
                            "getBytesForAdpcmInfo"
                            ))) return 1;

        if (!(encode =
                (lpFunc3)GetProcAddress(
                            hDll,
                            "encode"
                            ))) return 1;

        if (!(decode = 
                (lpFunc4)GetProcAddress(
                            hDll,
                            "decode"
                            ))) return 1;

        if (!(getLoopContext =
                (lpFunc5)GetProcAddress(
                            hDll,
                            "getLoopContext"
                            ))) return 1;

        return 0;
    }

    return 1;
}


/*--------------------------------------------------------------------------*/
int main(u32 argc, u8 *argv[])
{
    char        fileName[1024];
    char        fileName2[128] = {0};
    char        *p;
    int         mode;
    int         len;
    int         ii;
    char        token[]="/\\";
    char*       curr_ptr;
    char*       prev_ptr;

    // initialize vars
    dlsFile = NULL;
    wtFile  = NULL;
    pcmFile = NULL;
    mode    = MODE_USE_FLAG;

    // print banner
    printf("\nDLS1WT v1.5 - DLS Converter for SYN\n");
    printf("Copyright 2001 Nintendo.  All rights reserved.\n\n");

    // check args
    if (!((argc >= 2) && (argc <= 3)))
    {
        printf("\nUsage DLS1WT.exe <DLS file> -Options\n");
        printf("\n    -d   use DLS F_WSMP_NO_COMPRESSION flag (default)\n");
        printf("    -a   ADPCM compress all samples\n");
        printf("    -p   PCM encode all samples\n");
        clean_up();
        return 1;
    }

    // parse args
    if (argc == 3)
    {
        char *p;
        char ch[2];

        p = argv[2];

        ch[0] = *p;
        p++;
        ch[1] = *p;
        p++;

        if ((ch[0] != '-') || (*p != 0))
        {
            printf("\nInvalid option %s!\n", argv[2]);
            clean_up();
            return 1;
        }

        switch(ch[1])
        {
        case 'a':

            mode = MODE_COMPRESS_ALL;

            break;

        case 'd':

            mode = MODE_USE_FLAG;

            break;

        case 'p':

            mode = MODE_COMPRESS_NONE;
            
            break;

        default:

            printf("\nInvalid option %s!\n", argv[2]);
            clean_up();
            return 1;
        }
    }

    // load DLL
    if(getDll())
    {
        printf("\nError loading dsptool.dll!\n");
        clean_up();
        return 1;
    }

    // open DLS file
    strcpy(fileName, argv[1]);
    prev_ptr = fileName;
    curr_ptr = strtok(fileName, token);
    while (curr_ptr)
    {
        prev_ptr = curr_ptr;
        curr_ptr = strtok(NULL, token);
    }
    
    len = strlen(prev_ptr);
    strncpy(fileName2, prev_ptr, len);
    
    if (len < 5)
    {
        printf("\nInput file needs an extension \".dls\"!\n");
        clean_up();
        return 1;
    }

    for (ii = 0; ii < 3; ii++)
    {
        fileName2[len - ii - 1] = tolower(fileName2[len - ii - 1]);
    }
    
    if (strcmp(&fileName2[len - 4], ".dls"))
    {
        printf("\nInput file needs an extension \".dls\"!\n");
        clean_up();
        return 1;
    }
    
    if (!(dlsFile = fopen(argv[1], "rb")))
    {
        printf("\nCannot open %s for reading!\n", argv[1]);
        clean_up();
        return 1;
    }

    // open WT file for writing
    strcpy(fileName, fileName2);
    p = strrchr(fileName, '.');
    p++;
    *p = 'w';
    p++;
    *p = 't';
    p++;
    *p = 0;

    if (!(wtFile = fopen(fileName, "wb")))
    {
        printf("\nCannot open %s for writing!\n", fileName);
        clean_up();
        return 1;
    }

    // open PCM file for writing
    strcpy(fileName, fileName2);
    p = strrchr(fileName, '.');
    p++;
    *p = 'p';
    p++;
    *p = 'c';
    p++;
    *p = 'm';
    p++;
    *p = 0;

    if (!(pcmFile = fopen(fileName, "wb")))
    {
        printf("\nCannot open %s for writing!\n", fileName);
        clean_up();
        return 1;
    }

    // export objects from DLS file
    dls_read_file(dlsFile, wtFile, pcmFile, mode);

    clean_up();

    return 0;
}
