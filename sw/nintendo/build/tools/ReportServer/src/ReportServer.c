/*--------------------------------------------------------------------------*
  Project: GAMECUBE OSReport Server 
  File:    ReportServer.c

  Copyright 2003 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: ReportServer.c,v $
  Revision 1.1.1.1  2004/03/02 05:31:13  paulm
  Socket Library 24-Sep-2003

    
    1     03/03/04 10:21a Ooshima
    Initial version
    
  $NoKeywords: $

 *--------------------------------------------------------------------------*/

#include <stdio.h>
#include <winsock2.h>
#include <ws2tcpip.h>
    
static int srcSocket;  // TDEV Server (PC)
static int dstSocket;  // TDEV Client (GC)

/*---------------------------------------------------------------------------*
  Name:         NetUDPInit

  Description:  NetUDPInit

  Arguments:    None

  Returns:      TRUE if the function succeeds.

 *---------------------------------------------------------------------------*/
BOOL NetUDPInit( u_short UDP_Port )
{
    WSADATA wsaData;
    int err;
    struct sockaddr_in srcAddr;

    err = WSAStartup(MAKEWORD(2,0), &wsaData); 

    if(err != 0)
    {
        fprintf(stderr, "WSAStartup() failed.\n");
        return FALSE;
    }

    srcSocket = socket(AF_INET, SOCK_DGRAM, 0);

    if (srcSocket == INVALID_SOCKET)
    {
        fprintf(stderr, "socket() failed.\n");
        fprintf(stderr, "errno = %d\n", WSAGetLastError());
        WSACleanup();
        return FALSE;
    }

    memset(&srcAddr, 0, sizeof(srcAddr));
    srcAddr.sin_family = AF_INET;
    srcAddr.sin_port = htons(UDP_Port);
    srcAddr.sin_addr.s_addr = htonl(INADDR_ANY);

    if(SOCKET_ERROR == bind(srcSocket, (struct sockaddr *) &srcAddr, sizeof(srcAddr)))
    {
        fprintf(stderr, "bind() failed.\n");
        fprintf(stderr, "errno = %d\n", WSAGetLastError());
        closesocket(srcSocket);
        WSACleanup();
        return FALSE;
    }

    return TRUE;
}

#define BUFFER_SIZE   1500 // MTU

/*---------------------------------------------------------------------------*
  Name:         NetUDPRead

  Description:  Get message from OSReport() 

  Arguments:    None

  Returns:      NONE

 *---------------------------------------------------------------------------*/
void NetUDPRead(char* filename)
{
    int                    retval;
    int                    fromlen;
    struct sockaddr_in    from;
    char                MsgBuffer[BUFFER_SIZE];
    FILE                *fp;

    if(filename)
    {
        fp = fopen(filename, "wt");
    }

    while(1)
    {
        fromlen =sizeof(from);
        retval = recvfrom(srcSocket, MsgBuffer, sizeof(MsgBuffer), 0, (struct sockaddr *)&from,&fromlen);

#ifdef _DEBUG
        printf("Received datagram from %s, %d byte\n",inet_ntoa(from.sin_addr), retval);
#endif
        if (retval == SOCKET_ERROR)
        {
            fprintf(stderr,"recvfrom() failed!\n");
            fprintf(stderr, "errno = %d\n", WSAGetLastError());
            continue;
        }
        else if (retval == 0)
        {
            printf("Client closed connection\n");
            continue;
        }

        printf(MsgBuffer);
        if(filename)
        {
            fprintf(fp, MsgBuffer);
            fflush(fp);
        }
    }

    fclose(fp);

    closesocket(srcSocket);
    WSACleanup();
}

int main(int argc, void *argv[]) 
{
    SetConsoleTitle( "OSReport Server");

    // print banner
    printf("ReportServer v0.2\n");
    printf("Copyright 2003 Nintendo.  All rights reserved.\n");

    // check arguments
    if (argc != 2 && argc != 3)
    {
        printf("\nusage: ReportServer <UDP Port> [output file]\n");
        printf("\n");
        printf("UDP Port    : Choose between 5001 and 32767\n");
        printf("output file : Optional file to write\n");
        return 1;
    }

    // Initialize Socket
    if(!NetUDPInit((u_short)atoi(argv[1])))
    {
        printf("Failed to initialize.");
        return 1;
    }

    // Server Thread Start
    NetUDPRead(argv[2]);

    return 0;
}