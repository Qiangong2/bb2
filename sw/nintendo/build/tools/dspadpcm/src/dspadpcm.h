/*---------------------------------------------------------------------------*
  Project:  GAMECUBE DSP ADPCM win32 DLL
  File:     dspadpcm.h

  Copyright 1998, 1999, 2000, 2001 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

 *---------------------------------------------------------------------------*/

#ifndef __DSPADPCM_H__
#define __DSPADPCM_H__


typedef struct
{

    u32 channels;
    u32 bitsPerSample;
    u32 sampleRate;
    u32 samples;
    u32 loopStart;
    u32 loopEnd;
    u32 bufferLength;

} SOUNDINFO;

#define SOUND_FILE_SUCCESS      0
#define SOUND_FILE_FORMAT_ERROR 1
#define SOUND_FILE_FOPEN_ERROR  2

typedef struct
{
    // start context
    s16 coef[16];
    u16 gain;
    u16 pred_scale;
    s16 yn1;
    s16 yn2;

    // loop context
    u16 loop_pred_scale;
    s16 loop_yn1;
    s16 loop_yn2;

} ADPCMINFO;


#endif