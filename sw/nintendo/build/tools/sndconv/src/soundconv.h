#include "types.h"


/*--------------------------------------------------------------------------*
    soundfil.dll exports
 *--------------------------------------------------------------------------*/
typedef struct
{

    u32 channels;
    u32 bitsPerSample;
    u32 sampleRate;
    u32 samples;
    u32 loopStart;
    u32 loopEnd;
    u32 bufferLength;

} SOUNDINFO;


/*--------------------------------------------------------------------------*
    dsptool.dll exports
 *--------------------------------------------------------------------------*/
typedef struct
{
    // start context
    u16 coef[16];
    u16 gain;
    u16 pred_scale;
    u16 yn1;
    u16 yn2;

    // loop context
    u16 loop_pred_scale;
    u16 loop_yn1;
    u16 loop_yn2;

} ADPCMINFO; 


/*--------------------------------------------------------------------------*
    local defines
 *--------------------------------------------------------------------------*/
#define STATUS_SUCCESS          0
#define STATUS_EOF              1
#define STATUS_ERROR            2

#define STATE_BEGIN             0
#define STATE_END               1

#define SOUND_FORMAT_ADPCM      0
#define SOUND_FORMAT_PCM8       1
#define SOUND_FORMAT_PCM16      2

#define SOUND_STEREO_COMBINE    0
#define SOUND_STEREO_LEFT       1
#define SOUND_STEREO_RIGHT      2    


#define SOUND_DATA_NO_USER_INPUT 0xFFFFFFFF


/*--------------------------------------------------------------------------*
    player table format
 *--------------------------------------------------------------------------*/


typedef struct
{

    u32 type;
    u32 sampleRate;
    u32 loopAddr;
    u32 loopEndAddr;
    u32 endAddr;
    u32 currentAddr;
    u32 adpcm;

} SNDCONVDATA;

#define SP_TYPE_ADPCM_ONESHOT   0
#define SP_TYPE_ADPCM_LOOPED    1
#define SP_TYPE_PCM16_ONESHOT   2
#define SP_TYPE_PCM16_LOOPED    3
#define SP_TYPE_PCM8_ONESHOT    4
#define SP_TYPE_PCM8_LOOPED     5

/*--------------------------------------------------------------------------*
    function prototypes from sound.c
 *--------------------------------------------------------------------------*/
void soundSetDefaultFormat      (u32 format);
void soundInitParams            (void);
void soundSetSoundFile          (char *ch);
void soundSetIdString           (char *ch);
void soundSetSampleRate         (u32 i);
void soundSetFormat             (u32 i);
void soundSetLoopStart          (u32 i);
void soundSetLoopEnd            (u32 i);
void soundSetMix                (u32 i);
int  soundPrintSound            (void);


/*--------------------------------------------------------------------------*
    function prototypes from soundformat.c
 *--------------------------------------------------------------------------*/
void soundStereoCombine8Bit     (s8 *dest, s8 *source, u32 samples);
void soundStereoCombine16Bit    (s16 *dest, s16 *source, u32 samples);
void soundStereoLeft8Bit        (s8 *dest, s8 *source, u32 samples);
void soundStereoLeft16Bit       (s16 *dest, s16 *source, u32 samples);
void soundStereoRight8Bit       (s8 *dest, s8 *source, u32 samples);
void soundStereoRight16Bit      (s16 *dest, s16 *source, u32 samples);
void soundConvert8to16Bit       (s16 *dest, s8 *source, u32 samples);
void soundConvert16to8Bit       (s8 *dest, s16 *source, u32 samples);
void soundConvert16BitToAdpcm   (void *, s16 *, ADPCMINFO *, u32 samples);
void soundConvert16BitToAdpcmLoop(void *, s16 *, ADPCMINFO *, u32 samples, u32 loopStart);
void soundConvert8BitToAdpcm    (void *, s8 *, ADPCMINFO *, u32 samples);
void soundConvert8BitToAdpcmLoop(void *, s8 *, ADPCMINFO *, u32 samples, u32 loopStart);


/*--------------------------------------------------------------------------*
    function prototypes from output.c
 *--------------------------------------------------------------------------*/
int  soundOutputInit            (char *name);
void soundOutputQuit            (void);

void soundOutputAddEntry(
        u32         format,
        u32         dataBytes,
        void        *buffer,
        u32         samples,
        u32         sampleRate,
        u32         loopStart,
        u32         loopEnd,
        ADPCMINFO   *adpcminfo,
        char        *headerId
        );

void soundOutputComment         (char *ch);
