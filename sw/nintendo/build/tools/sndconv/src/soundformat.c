/*--------------------------------------------------------------------------*
  Project:  GAMECUBE Audio sound file converter
  File:     soundformat.c

  Copyright 1998, 1999, 2000, 2001 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.
 *--------------------------------------------------------------------------*/
#include <stdlib.h>
#include "soundconv.h"


/*--------------------------------------------------------------------------*
    export from dsptool.dll    
 *--------------------------------------------------------------------------*/
typedef void (*FUNCTION1)(s16*, s8*, ADPCMINFO*, u32);
typedef void (*FUNCTION2)(u8 *src, ADPCMINFO *cxt, u32 samples);

extern FUNCTION1 encode;
extern FUNCTION2 getLoopContext;


/*--------------------------------------------------------------------------*
    combine 8 bit stereo samples    
 *--------------------------------------------------------------------------*/
void soundStereoCombine8Bit(s8 *dest, s8 *source, u32 samples)
{
    u32 i;

    for (i = 0; i < samples; i++)
    {
        s16 sample;
        
        sample = *source;
        source++;
        sample += *source;
        source++;
        
        *dest = (u8)((sample >> 1) & 0xFF);
        dest++;
    }
}


/*--------------------------------------------------------------------------*
    combine 16 bit stereo samples    
 *--------------------------------------------------------------------------*/
void soundStereoCombine16Bit(s16 *dest, s16 *source, u32 samples)
{
    u32 i;

    for (i = 0; i < samples; i++)
    {
        *dest = *source >> 1;
        
        source++;
        
        *dest += *source >> 1;

        dest++;
        source++;
    }
}


/*--------------------------------------------------------------------------*
    left 8 bit stereo samples    
 *--------------------------------------------------------------------------*/
void soundStereoLeft8Bit(s8 *dest, s8 *source, u32 samples)
{
    u32 i;

    for (i = 0; i < samples; i++)
    {
        *dest = *source;
        
        source++;

        dest++;
        source++;
    }
}


/*--------------------------------------------------------------------------*
    left 16 bit stereo samples    
 *--------------------------------------------------------------------------*/
void soundStereoLeft16Bit(s16 *dest, s16 *source, u32 samples)
{
    u32 i;

    for (i = 0; i < samples; i++)
    {
        *dest = *source;
        
        source++;

        dest++;
        source++;
    }
}


/*--------------------------------------------------------------------------*
    right 8 bit stereo samples    
 *--------------------------------------------------------------------------*/
void soundStereoRight8Bit(s8 *dest, s8 *source, u32 samples)
{
    u32 i;

    for (i = 0; i < samples; i++)
    {
        source++;

        *dest = *source;

        dest++;
        source++;
    }
}


/*--------------------------------------------------------------------------*
    right 16 bit stereo samples    
 *--------------------------------------------------------------------------*/
void soundStereoRight16Bit(s16 *dest, s16 *source, u32 samples)
{
    u32 i;

    for (i = 0; i < samples; i++)
    {
        source++;

        *dest = *source;

        dest++;
        source++;
    }
}


/*--------------------------------------------------------------------------*
    convert 8 bit to 16 bit    
 *--------------------------------------------------------------------------*/
void soundConvert8to16Bit(s16 *dest, s8 *source, u32 samples)
{
    u32 i;

    for (i = 0; i < samples; i++)
    {
        *dest = (s16)(*source << 8);

        dest++;
        source++;
    }
}


/*--------------------------------------------------------------------------*
    convert 16 bit to 8 bit    
 *--------------------------------------------------------------------------*/
void soundConvert16to8Bit(s8 *dest, s16 *source, u32 samples)
{
    u32 i;

    for (i = 0; i < samples; i++)
    {
        *dest = (s8)((*source >> 8) & 0xFF);

        dest++;
        source++;
    }
}


/*--------------------------------------------------------------------------*
    convert 16 bit to ADPCM    
 *--------------------------------------------------------------------------*/
void soundConvert16BitToAdpcm(void *dest, s16 *source, ADPCMINFO *adpcminfo,
                              u32 samples)
{
    encode(source, dest, adpcminfo, samples);
}


/*--------------------------------------------------------------------------*
    convert 16 bit to ADPCM with loop context   
 *--------------------------------------------------------------------------*/
void soundConvert16BitToAdpcmLoop(void *dest, s16 *source, ADPCMINFO *adpcminfo,
                              u32 samples, u32 loopStart)
{
    encode(source, dest, adpcminfo, samples);
    getLoopContext(dest, adpcminfo, loopStart);
}


/*--------------------------------------------------------------------------*
    convert 8 bit to ADPCM    
 *--------------------------------------------------------------------------*/
void soundConvert8BitToAdpcm(void *dest, s8 *source, ADPCMINFO *adpcminfo,
                             u32 samples)
{
    void *p;

    if (p = malloc(samples * 2))
    {
        soundConvert8to16Bit(p, source, samples);
        soundConvert16BitToAdpcm(dest, p, adpcminfo, samples);    

        free(p);
    }
}


/*--------------------------------------------------------------------------*
    convert 8 bit to ADPCM with loop   
 *--------------------------------------------------------------------------*/
void soundConvert8BitToAdpcmLoop(void *dest, s8 *source, ADPCMINFO *adpcminfo,
                             u32 samples, u32 loopStart)
{
    void *p;

    if (p = malloc(samples * 2))
    {
        soundConvert8to16Bit(p, source, samples);
        soundConvert16BitToAdpcmLoop(dest, p, adpcminfo, samples, loopStart);    

        free(p);
    }
}
