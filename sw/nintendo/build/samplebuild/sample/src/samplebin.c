/*---------------------------------------------------------------------------*
  Project:  Sample binary template
  File:     Samplebin.c

  Copyright 1998, 1999 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: samplebin.c,v $
  Revision 1.1.1.1  2004/06/09 17:40:06  paulm
  GC samplebuild from Nintendo SDK

    
    2     6/14/99 12:35p Tianli01
    Demonstrated use of debug flags
    
    1     6/10/99 7:14p Tianli01
    Initial checkin - sample binary/library building
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*
  This sample template can be used as a tutorial to start building
  applications and libraries in a module with our build system.
 *---------------------------------------------------------------------------*/

#include <dolphin.h>

#include "sample.h"


void main (void)
{
    OSReport("Hello World!!!\n");
#ifdef _DEBUG
    OSReport("This is a DEBUG build.\n");
#else
    OSReport("This is a NON-DEBUG/optimized build.\n");
#endif // _DEBUG
    OSReport("Calling SampleLib->SampleFunction with 42.\n");
    OSReport("Result is %d\n",SampleFunction(42));
    
    OSHalt("Sample complete");
}
