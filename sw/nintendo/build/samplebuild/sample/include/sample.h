/*---------------------------------------------------------------------------*
  Project:  Sample binary template
  File:     Samplebin.h

  Copyright 1998, 1999 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: sample.h,v $
  Revision 1.1.1.1  2004/06/09 17:40:06  paulm
  GC samplebuild from Nintendo SDK

    
    1     10/12/99 5:35p Tian
    Updated return type to u32
    
    1     6/10/99 7:13p Tianli01
    Initial checkin
  $NoKeywords: $
 *---------------------------------------------------------------------------*/
#include <dolphin.h>

#ifndef __SAMPLE_H__
#define __SAMPLE_H__

u32 SampleFunction(u32 num);

#endif
