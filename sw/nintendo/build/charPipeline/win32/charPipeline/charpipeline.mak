# Microsoft Developer Studio Generated NMAKE File, Based on charpipeline.dsp
!IF "$(CFG)" == ""
CFG=charpipeline - Win32 Debug
!MESSAGE No configuration specified. Defaulting to charpipeline - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "charpipeline - Win32 Release" && "$(CFG)" != "charpipeline - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "charpipeline.mak" CFG="charpipeline - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "charpipeline - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "charpipeline - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

!IF  "$(CFG)" == "charpipeline - Win32 Release"

OUTDIR=.\../../../../win32/lib
INTDIR=.\Release
# Begin Custom Macros
OutDir=.\../../../../win32/lib
# End Custom Macros

ALL : "$(OUTDIR)\charpipeline.lib"


CLEAN :
	-@erase "$(INTDIR)\Actor.obj"
	-@erase "$(INTDIR)\actorAnim.obj"
	-@erase "$(INTDIR)\animBank.obj"
	-@erase "$(INTDIR)\animPipe.obj"
	-@erase "$(INTDIR)\boneAnim.obj"
	-@erase "$(INTDIR)\Control.obj"
	-@erase "$(INTDIR)\displayObject.obj"
	-@erase "$(INTDIR)\fileCache.obj"
	-@erase "$(INTDIR)\filelib.obj"
	-@erase "$(INTDIR)\geoPalette.obj"
	-@erase "$(INTDIR)\HTable.obj"
	-@erase "$(INTDIR)\Light.obj"
	-@erase "$(INTDIR)\List.obj"
	-@erase "$(INTDIR)\normalTable.obj"
	-@erase "$(INTDIR)\String.obj"
	-@erase "$(INTDIR)\texPalette.obj"
	-@erase "$(INTDIR)\Tree.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(OUTDIR)\charpipeline.lib"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /ML /W3 /GX /O2 /I "../../actor/include" /D "NDEBUG" /D "WIN32" /D "_MBCS" /D "_LIB" /D "EMU" /Fp"$(INTDIR)\charpipeline.pch" /YX /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

RSC=rc.exe
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\charpipeline.bsc" 
BSC32_SBRS= \
	
LIB32=link.exe -lib
LIB32_FLAGS=/nologo /out:"$(OUTDIR)\charpipeline.lib" 
LIB32_OBJS= \
	"$(INTDIR)\Actor.obj" \
	"$(INTDIR)\actorAnim.obj" \
	"$(INTDIR)\animBank.obj" \
	"$(INTDIR)\animPipe.obj" \
	"$(INTDIR)\boneAnim.obj" \
	"$(INTDIR)\Control.obj" \
	"$(INTDIR)\displayObject.obj" \
	"$(INTDIR)\fileCache.obj" \
	"$(INTDIR)\filelib.obj" \
	"$(INTDIR)\geoPalette.obj" \
	"$(INTDIR)\HTable.obj" \
	"$(INTDIR)\Light.obj" \
	"$(INTDIR)\List.obj" \
	"$(INTDIR)\normalTable.obj" \
	"$(INTDIR)\String.obj" \
	"$(INTDIR)\texPalette.obj" \
	"$(INTDIR)\Tree.obj"

"$(OUTDIR)\charpipeline.lib" : "$(OUTDIR)" $(DEF_FILE) $(LIB32_OBJS)
    $(LIB32) @<<
  $(LIB32_FLAGS) $(DEF_FLAGS) $(LIB32_OBJS)
<<

!ELSEIF  "$(CFG)" == "charpipeline - Win32 Debug"

OUTDIR=.\../../../../win32/lib
INTDIR=.\Debug
# Begin Custom Macros
OutDir=.\../../../../win32/lib
# End Custom Macros

ALL : "$(OUTDIR)\charpipelineD.lib"


CLEAN :
	-@erase "$(INTDIR)\Actor.obj"
	-@erase "$(INTDIR)\actorAnim.obj"
	-@erase "$(INTDIR)\animBank.obj"
	-@erase "$(INTDIR)\animPipe.obj"
	-@erase "$(INTDIR)\boneAnim.obj"
	-@erase "$(INTDIR)\Control.obj"
	-@erase "$(INTDIR)\displayObject.obj"
	-@erase "$(INTDIR)\fileCache.obj"
	-@erase "$(INTDIR)\filelib.obj"
	-@erase "$(INTDIR)\geoPalette.obj"
	-@erase "$(INTDIR)\HTable.obj"
	-@erase "$(INTDIR)\Light.obj"
	-@erase "$(INTDIR)\List.obj"
	-@erase "$(INTDIR)\normalTable.obj"
	-@erase "$(INTDIR)\String.obj"
	-@erase "$(INTDIR)\texPalette.obj"
	-@erase "$(INTDIR)\Tree.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\charpipelineD.lib"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MLd /W3 /Gm /GX /ZI /Od /I "../../actor/include" /D "_DEBUG" /D "WIN32" /D "_MBCS" /D "_LIB" /D "EMU" /Fp"$(INTDIR)\charpipeline.pch" /YX /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

RSC=rc.exe
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\charpipeline.bsc" 
BSC32_SBRS= \
	
LIB32=link.exe -lib
LIB32_FLAGS=/nologo /out:"$(OUTDIR)\charpipelineD.lib" 
LIB32_OBJS= \
	"$(INTDIR)\Actor.obj" \
	"$(INTDIR)\actorAnim.obj" \
	"$(INTDIR)\animBank.obj" \
	"$(INTDIR)\animPipe.obj" \
	"$(INTDIR)\boneAnim.obj" \
	"$(INTDIR)\Control.obj" \
	"$(INTDIR)\displayObject.obj" \
	"$(INTDIR)\fileCache.obj" \
	"$(INTDIR)\filelib.obj" \
	"$(INTDIR)\geoPalette.obj" \
	"$(INTDIR)\HTable.obj" \
	"$(INTDIR)\Light.obj" \
	"$(INTDIR)\List.obj" \
	"$(INTDIR)\normalTable.obj" \
	"$(INTDIR)\String.obj" \
	"$(INTDIR)\texPalette.obj" \
	"$(INTDIR)\Tree.obj"

"$(OUTDIR)\charpipelineD.lib" : "$(OUTDIR)" $(DEF_FILE) $(LIB32_OBJS)
    $(LIB32) @<<
  $(LIB32_FLAGS) $(DEF_FLAGS) $(LIB32_OBJS)
<<

!ENDIF 


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("charpipeline.dep")
!INCLUDE "charpipeline.dep"
!ELSE 
!MESSAGE Warning: cannot find "charpipeline.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "charpipeline - Win32 Release" || "$(CFG)" == "charpipeline - Win32 Debug"
SOURCE=..\..\Actor\Src\Actor.c

"$(INTDIR)\Actor.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\..\Actor\Src\actorAnim.c

"$(INTDIR)\actorAnim.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\..\Anim\Src\animBank.c

"$(INTDIR)\animBank.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\..\Anim\Src\animPipe.c

"$(INTDIR)\animPipe.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\..\Actor\Src\boneAnim.c

"$(INTDIR)\boneAnim.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\..\Control\Src\Control.c

"$(INTDIR)\Control.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\..\geoPalette\Src\displayObject.c

"$(INTDIR)\displayObject.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\..\fileCache\Src\fileCache.c

"$(INTDIR)\fileCache.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\src\filelib.c

"$(INTDIR)\filelib.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\..\geoPalette\Src\geoPalette.c

"$(INTDIR)\geoPalette.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\..\structures\Src\HTable.c

"$(INTDIR)\HTable.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\..\Lighting\Src\Light.c

"$(INTDIR)\Light.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\..\structures\Src\List.c

"$(INTDIR)\List.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\..\geoPalette\Src\normalTable.c

"$(INTDIR)\normalTable.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\..\structures\Src\String.c

"$(INTDIR)\String.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\..\texPalette\Src\texPalette.c

"$(INTDIR)\texPalette.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\..\structures\Src\Tree.c

"$(INTDIR)\Tree.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)



!ENDIF 

