# Microsoft Developer Studio Project File - Name="charpipeline" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Static Library" 0x0104

CFG=charpipeline - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "charpipeline.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "charpipeline.mak" CFG="charpipeline - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "charpipeline - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "charpipeline - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "charpipeline - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "../../../../win32/lib"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_MBCS" /D "_LIB" /YX /FD /c
# ADD CPP /nologo /W3 /GX /O2 /I "../../actor/include" /D "NDEBUG" /D "WIN32" /D "_MBCS" /D "_LIB" /D "EMU" /YX /FD /c
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ELSEIF  "$(CFG)" == "charpipeline - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "../../../../win32/lib"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_MBCS" /D "_LIB" /YX /FD /GZ /c
# ADD CPP /nologo /W3 /Gm /GX /ZI /Od /I "../../actor/include" /D "_DEBUG" /D "WIN32" /D "_MBCS" /D "_LIB" /D "EMU" /YX /FD /GZ /c
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo /out:"../../../../win32/lib\charpipelineD.lib"

!ENDIF 

# Begin Target

# Name "charpipeline - Win32 Release"
# Name "charpipeline - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=..\..\Actor\Src\Actor.c
# End Source File
# Begin Source File

SOURCE=..\..\Actor\Src\actorAnim.c
# End Source File
# Begin Source File

SOURCE=..\..\Anim\Src\animBank.c
# End Source File
# Begin Source File

SOURCE=..\..\Anim\Src\animPipe.c
# End Source File
# Begin Source File

SOURCE=..\..\Actor\Src\boneAnim.c
# End Source File
# Begin Source File

SOURCE=..\..\Control\Src\Control.c
# End Source File
# Begin Source File

SOURCE=..\..\geoPalette\Src\displayObject.c
# End Source File
# Begin Source File

SOURCE=..\..\fileCache\Src\fileCache.c
# End Source File
# Begin Source File

SOURCE=..\src\filelib.c
# End Source File
# Begin Source File

SOURCE=..\..\geoPalette\Src\geoPalette.c
# End Source File
# Begin Source File

SOURCE=..\..\structures\Src\HTable.c
# End Source File
# Begin Source File

SOURCE=..\..\Lighting\Src\Light.c
# End Source File
# Begin Source File

SOURCE=..\..\structures\Src\List.c
# End Source File
# Begin Source File

SOURCE=..\..\geoPalette\Src\normalTable.c
# End Source File
# Begin Source File

SOURCE=..\..\structures\Src\String.c
# End Source File
# Begin Source File

SOURCE=..\..\texPalette\Src\texPalette.c
# End Source File
# Begin Source File

SOURCE=..\..\structures\Src\Tree.c
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# End Group
# End Target
# End Project
