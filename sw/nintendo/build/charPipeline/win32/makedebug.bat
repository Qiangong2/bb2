@echo OFF

cd charpipeline
echo ----------------------------------------------------------
echo nmake /nologo /f charpipeline.mak CFG="charpipeline - Win32 Debug"
nmake /nologo /f charpipeline.mak CFG="charpipeline - Win32 Debug"
cd ..
cd shader
echo ----------------------------------------------------------
echo nmake /nologo /f shader.mak CFG="shader - Win32 Debug"
nmake /nologo /f shader.mak CFG="shader - Win32 Debug"
cd ..
