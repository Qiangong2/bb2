@echo OFF

cd charpipeline
echo ----------------------------------------------------------
echo nmake /nologo /f charpipeline.mak CFG="charpipeline - Win32 Release"
nmake /nologo /f charpipeline.mak CFG="charpipeline - Win32 Release"
cd ..
cd shader
echo ----------------------------------------------------------
echo nmake /nologo /f shader.mak CFG="shader - Win32 Release"
nmake /nologo /f shader.mak CFG="shader - Win32 Release"
cd ..
