/********************************************************************/
/*          define.h                                                */
/*            Global Macro Declaration                              */
/*                                                                  */
/*          Copyright (C) 1999-2001 NINTENDO Co.,Ltd.               */
/********************************************************************/
#ifndef _DEFINE_H
#define _DEFINE_H

#include <Agb.h>


//#define NDEBUG

#define INITIAL_CODE    (*(vu32 *)"AGBJ")   // Initial Code


#endif /* _DEFINE_H */
