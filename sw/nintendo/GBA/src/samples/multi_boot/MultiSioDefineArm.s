;********************************************************************
;*          MultiSioDefineArm.s                                     *
;*           Multi-play Communication Library Constant (ARMASM)     *
;*            (Timer interrupt send & not adjusted synchronous)     *
;*                                                                  *
;*          Copyright (C) 1999-2001 NINTENDO Co.,Ltd.               *
;********************************************************************


; Optimize the set values below according to the software specification.

MULTI_SIO_BLOCK_SIZE            *   16      ; Communication data block size (Max 24 Byte)

MULTI_SIO_PLAYERS_MAX           *   4       ; Maximum number of players

MULTI_SIO_SYNC_DATA             *   0xfefe  ; Synchronous data (0x0000/0xffff are prohibited.)


; Comment out if there is not enough space in CPU internal working RAM.
MULTI_SIO_DI_FUNC_FAST                      ; SIO interrupt prohibit function high speed flag (CPU internal RAM executed.)


MULTI_SIO_TIMER_NO              *   3       ; Timer No
MULTI_SIO_TIMER_INTR_FLAG       *   (TIMER0_INTR_FLAG << MULTI_SIO_TIMER_NO)
                                            ; Timer interrupt flag
REG_MULTI_SIO_TIMER             *   (REG_TM0CNT + MULTI_SIO_TIMER_NO * 4)
REG_MULTI_SIO_TIMER_L           *    REG_MULTI_SIO_TIMER
REG_MULTI_SIO_TIMER_H           *   (REG_MULTI_SIO_TIMER + 2)
                                            ; Timer register


; Multi-play communication packet structure offset

OFS_MSP_FRAME_COUNTER           *   0       ; Frame counter
OFS_MSP_RECV_ERROR_FLAGS        *   1       ; Receive error flag
OFS_MSP_CHECK_SUM               *   2       ; Check sum
OFS_MSP_DATA                    *   4       ; Communication data
OFS_MSP_OVERRUN_CATCH           *   (OFS_MSP_DATA + MULTI_SIO_BLOCK_SIZE)
                                            ; Overrun protected area

MULTI_SIO_PACKET_SIZE           *   (OFS_MSP_OVERRUN_CATCH + 4)
                                            ; Structure size


; Multi-play communication work area structure offset

OFS_MS_TYPE                     *   0       ; Connection (Master/Slave)
OFS_MS_STATE                    *   1       ; Status of communication function
OFS_MS_CONNECTED_FLAG           *   2       ; Connection history flag
OFS_MS_RECV_SUCCESS_FLAGS       *   3       ; Receiving success flag

OFS_MS_SYNC_RECV_FLAG           *   4       ; Send frame counter

OFS_MS_START_FLAG               *   8       ; Communication starting flag
OFS_MS_HARD_ERROR               *   9       ; Hard error

OFS_MS_SYNC_SEND_FRAME_COUNTER  *   11      ; Send frame counter
OFS_MS_SYNC_RECV_FRAME_COUNTER  *   12      ; Receive frame counter

OFS_MS_SEND_BUF_COUNTER         *   20      ; Send buffer counter
OFS_MS_RECV_BUF_COUNTER         *   24      ; Receive buffer counter

OFS_MS_NEXT_SEND_BUF_P          *   40      ; Send buffer pointer
OFS_MS_CURRENT_SEND_BUF_P       *   44
OFS_MS_CURRENT_RECV_BUF_P       *   48      ; Receive buffer pointer
OFS_MS_LAST_RECV_BUF_P          *   64
OFS_MS_RECV_CHECK_BUF_P         *   80

OFS_MS_SEND_BUF                 *   96      ; Send buffer (double buffer)
OFS_MS_RECV_BUF                 *   (OFS_MS_SEND_BUF + MULTI_SIO_PACKET_SIZE * 2)
                                            ; Receive buffer (triple buffer)

MULTI_SIO_AREA_SIZE             *   (OFS_MS_RECV_BUF + MULTI_SIO_PACKET_SIZE * 3 * MULTI_SIO_PLAYERS_MAX)
                                            ; Size of structure

    EXTERN RecvFuncBuf                      ; CPU internal RAM execution buffer
    EXTERN IntrFuncBuf



;--------------------------------------------------------------------
;-               Initialize Multi-play Communication		    -
;--------------------------------------------------------------------

;   EXTERN MultiSioInit

;--------------------------------------------------------------------
;-                Start Multi-play Communication		    -
;--------------------------------------------------------------------

    EXTERN MultiSioStart

;--------------------------------------------------------------------
;-                Stop Multi-play Communication			    -
;--------------------------------------------------------------------

    EXTERN MultiSioStop

;--------------------------------------------------------------------
;-                Multi-play Communication Main			    -
;--------------------------------------------------------------------

;   EXTERN MultiSioMain

;--------------------------------------------------------------------
;-                Multi-play Communication Interrupt		    -
;--------------------------------------------------------------------

;   EXTERN MultiSioIntr

;--------------------------------------------------------------------
;-                Set Sending Data                                  -
;--------------------------------------------------------------------

;   EXTERN MultiSioSendDataSet

;--------------------------------------------------------------------
;-                 Check Receiving Data				    -
;--------------------------------------------------------------------

;   EXTERN MultiSioRecvDataCheck


    END

