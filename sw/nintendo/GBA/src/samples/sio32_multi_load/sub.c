/********************************************************************/
/*          sub.c                                                   */
/*            General Subroutines                                   */
/*                                                                  */
/*          Copyright (C) 1999-2001 NINTENDO Co.,Ltd.               */
/********************************************************************/
//#define CODE32
#include "define.h"
#include "types.h"

#include "sub.h"
#include "intr_table.h"
#include "data.h"
#include "bss.h"


/*==================================================================*/
/*       Interrupt routine (Limit processing as much as possible)   */
/*==================================================================*/

/*------------------------------------------------------------------*/
/*                      V-blank processing                          */
/*------------------------------------------------------------------*/

void VBlankIntr(void)
{
}

/*------------------------------------------------------------------*/
/*                      Interrupt dummy routine                     */
/*------------------------------------------------------------------*/

void IntrDummy(void)
{
}

/*==================================================================*/
/*                      Sub-routine                                 */
/*==================================================================*/

/*------------------------------------------------------------------*/
/*                      Key read                                    */
/*------------------------------------------------------------------*/

void KeyRead(void)
{
    u16 ReadData = (*(vu16 *)REG_KEYINPUT ^ ALL_KEY_MASK);
    Trg  = ReadData & (ReadData ^ Cont);            // Trigger input
    Cont = ReadData;                                // Hold input
}

/*------------------------------------------------------------------*/
/*                      Set BG screen                               */
/*------------------------------------------------------------------*/

void BgScSet(u8 *Srcp, u16 *Destp, u8 PlttNo)
{
    while (*Srcp != '\0')
        *Destp++ = (u16 )PlttNo << BG_SC_PLTT_SHIFT | *Srcp++;
}

/*------------------------------------------------------------------*/
/*              Hexadecimal-->BG screen conversion routine          */
/*------------------------------------------------------------------*/

void Data2BgSc(u32 SrcData, u16 *Destp, u8 Num, u8 PlttNo)
{
    u32 CharNoTmp;
    u32 PlttNoTmp = PlttNo << BG_SC_PLTT_SHIFT;
    int i;

    SrcData <<= (8-Num)*4;
    for (i=0; i<Num; i++) {
        CharNoTmp = (SrcData >> 28);
        if (CharNoTmp < 0xA)    CharNoTmp += 0x30;
        else                    CharNoTmp += 0x41 - 0xa;
        Destp[i] = CharNoTmp | PlttNoTmp;
        SrcData <<= 4 ;
    }
}


