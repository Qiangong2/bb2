/********************************************************************/
/*          load_table_src.c                                        */
/*             Load source table                                    */
/*                                                                  */
/*          Copyright (C) 2001 NINTENDO Co.,Ltd.                    */
/********************************************************************/
#include "Sio32MultiLoad.h"
#include "data.h"


const u8 * const LoadTable[] = {
    (u8 *)_binary_demo_bin_start,
    (u8 *)_binary_demo_bin_start + SIO32ML_BLOCK_SIZE * 1,
    (u8 *)_binary_demo_bin_start + SIO32ML_BLOCK_SIZE * 2,
};


