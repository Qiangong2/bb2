/********************************************************************/
/*          intr_table.c                                            */
/*             Interrupt table                                      */
/*                                                                  */
/*          Copyright (C) 2001 NINTENDO Co.,Ltd.                    */
/********************************************************************/
#include "MultiSio.h"

#include "define.h"
#include "types.h"

#include "sub.h"


IntrFuncp IntrTableBuf[13];

const IntrFuncp IntrTable[13] = {
#ifdef MULTI_SIO_DI_FUNC_FAST
    (IntrFuncp )IntrFuncBuf,    // Serial communication interrupt 
#else                           // or multi-play communication timer interrupt
    MultiSioIntr,
#endif
    VBlankIntr,                 // V-blank interrupt 

    IntrDummy,                  // H-blank interrupt 
    IntrDummy,                  // V-counter match interrupt
    IntrDummy,                  // DMA0 interrupt 
    IntrDummy,                  // DMA1 interrupt
    IntrDummy,                  // DMA2 interrupt
    IntrDummy,                  // DMA3 interrupt
    IntrDummy,                  // Key interrupt
};


