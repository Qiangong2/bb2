@********************************************************************
@*          intr_main.s                                             *
@*            Interrupt branches routine (GAS)                      *
@*                                                                  *
@*          Copyright (C) 1999-2001 NINTENDO Co.,Ltd.               *
@********************************************************************
    .INCLUDE    "AgbDefine.s"
    .INCLUDE    "AgbMemoryMap.s"
    .INCLUDE    "AgbSyscallDefine.s"
    .INCLUDE    "AgbMacro.s"
    .TEXT

    .INCLUDE    "MultiSioDefine.s"

@--------------------------------------------------------------------
@-   Interrupt branches process (Jump table) 32Bit           28-63c -
@--------------------------------------------------------------------
    .EXTERN     IntrTableBuf
    .GLOBAL     intr_main
    .ALIGN
    .CODE 32
intr_main:
        mov     r12, #REG_BASE           @ r12: REG_BASE
        add     r3, r12, #OFFSET_REG_IE  @ r3:  REG_IE
        ldr     r2, [r3]
        and     r1, r2, r2, lsr #16      @ r1:  IE & IF
        mov     r2, #0                       @ Checks IE/IF
        ands    r0, r1, #CASSETTE_INTR_FLAG  @ Game Pak interrupt
        strneb  r0, [r3, #REG_SOUNDCNT_X - REG_IE]  @ Stop sound
loop:   bne     loop

        ands    r0, r1, #SIO_INTR_FLAG | MULTI_SIO_TIMER_INTR_FLAG
        bne     jump_intr                    @ Serial communication interrupt
        add     r2, r2, #4                   @ or Multi-play communication timer interrupt

        ands    r0, r1, #V_BLANK_INTR_FLAG   @ V-blank interrupt
        strneh  r0, [r12, #INTR_CHECK_BUF - WRAM_END] @ Sets interrupt check
        bne     jump_intr
        add     r2, r2, #4

        ands    r0, r1, #H_BLANK_INTR_FLAG   @ H-blank interrupt
        bne     jump_intr
        add     r2, r2, #4
        ands    r0, r1, #V_COUNT_INTR_FLAG   @ V-counter interrupt
        bne     jump_intr
        add     r2, r2, #4
        ands    r0, r1, #DMA0_INTR_FLAG      @ DMA0 interrupt
        bne     jump_intr
        add     r2, r2, #4
        ands    r0, r1, #DMA1_INTR_FLAG      @ DMA1 interrupt
        bne     jump_intr
        add     r2, r2, #4
        ands    r0, r1, #DMA2_INTR_FLAG      @ DMA2 interrupt
        bne     jump_intr
        add     r2, r2, #4
        ands    r0, r1, #DMA3_INTR_FLAG      @ DMA3 interrupt
        bne     jump_intr
        add     r2, r2, #4
        ands    r0, r1, #KEY_INTR_FLAG       @ Key interrupt
jump_intr:
        strh    r0, [r3, #2]            @ Clears IF          11c
        ldr     r1, =IntrTableBuf       @ Jumps to user IRQ process
        add     r1, r1, r2
        ldr     r0, [r1]
        bx      r0


    .END

