/********************************************************************/
/*          loader_main.c                                           */
/*            Loader Main                                           */
/*                                                                  */
/*          Copyright (C) 2001 NINTENDO Co.,Ltd.                    */
/********************************************************************/
//#define CODE32
#include "MultiSio.h"
#include "Sio32MultiLoad.h"

#include "define.h"
#include "types.h"

#include "sub.h"
#include "intr_table.h"
#include "data.h"
#include "bss.h"


//#define KEY_ON                              // Flag to enable OBJ key operation


/*-------------------- Global Variable ----------------------------*/

extern u8 *LoadTable[3];                        // Load table


/*==================================================================*/
/*                      Main Routine                                */
/*==================================================================*/
extern void intr_main(void);
u8     *LoadCounterp;
u8      RecvLoadCounter;
u8      BurstLoad;
u32     ProgressCounter;

void LoaderMain(void)
{
    u32    MultiSioId;
    u32    SioState = 0;
    u8    *Sio32Datap = 0;
    u8     LoadStartFlag;
    char  *StrTmp;
    s32    i;

    // Be cautious of clearing of wait control when register clear is specified
    RegisterRamReset(RESET_CPU_WRAM_FLAG);                 // Clear CPU internal work RAM

    DmaCopy(3, IntrTable, IntrTableBuf,sizeof(IntrTableBuf),32);// Set interrupt table
    DmaCopy(3, intr_main, IntrMainBuf, sizeof(IntrMainBuf), 32);// Set interrupt main routine
    *(vu32 *)INTR_VECTOR_BUF = (vu32 )IntrMainBuf;

    DmaArrayCopy(3, CharData_Sample, BG_VRAM+0x8000, 32);  // Set BG character
    DmaArrayCopy(3, CharData_Sample, OBJ_MODE0_VRAM, 32);  // Set OBJ character
    DmaArrayCopy(3, PlttData_Sample, BG_PLTT,        32);  // Set BG palette
    DmaArrayCopy(3, PlttData_Sample, OBJ_PLTT,       32);  // Set OBJ palette

    DmaArrayCopy(3, BgScData_Sample, BgBak,          32);  // Set BG screen
    DmaArrayCopy(3, BgBak,           BG_VRAM,        32);

    DmaArrayClear(3,160,             OamBak,         32);  // Undisplayed OBJ moves outside screen
    DmaClear(     3,160,             OAM, OAM_SIZE,  32);
    DmaArrayCopy(3, OamData_Sample,  OamBak,         32);  // Set OAM
//    DmaArrayCopy(3, OamBak,          OAM,            32);

    for (i=0; i<MULTI_SIO_PLAYERS_MAX; i++)     OamBak[i].VPos = 200;

    *(vu16 *)REG_BG0CNT =                                  // Set BG control
            BG_COLOR_16 | BG_SCREEN_SIZE_0 | BG_PRIORITY_0
            | 0 << BG_SCREEN_BASE_SHIFT | 2 << BG_CHAR_BASE_SHIFT ;

    // Select to permit interrupt by distinguishing Game Pak insertion
    *(vu16 *)REG_IE    = V_BLANK_INTR_FLAG;                // Enable V-blank interrupt
    if (*(vu8  *)(ROM_BANK0+0xb2) == 0x96                  // Check Game Pak inserted
     && *(vu32 *)(ROM_BANK0+0xac) == INITIAL_CODE)
        *(vu16 *)REG_IE |= CASSETTE_INTR_FLAG;             // Enable Game Pak interrupt
    *(vu16 *)REG_STAT  = STAT_V_BLANK_IF_ENABLE;
    *(vu16 *)REG_IME   = 1;                                // Set IME

    *(vu16 *)REG_DISPCNT = DISP_MODE_0 | DISP_BG0_ON | DISP_OBJ_ON; // LCDC ON

    LoadCounterp = &((CommData *)SendBuf)->LoadCounter;

    while(1) {
        VBlankIntrWait();                               // Wait V-blank interrupt to be end

        KeyRead();                                      // Key operation

        if (Trg) {   BurstLoad ^= 1;
//                  *LoadCounterp = 0;
        }

        switch (SioState) {
            case 0: SendBuf[0] = 200;        // Initialize OBJ display location
            case 1:
#ifdef MULTI_SIO_DI_FUNC_FAST
                    IntrTableBuf[0] = (IntrFuncp )IntrFuncBuf;      // Set interrupt address
#else
                    IntrTableBuf[0] = MultiSioIntr;
#endif
                    MultiSioInit((SioFlags & MULTI_SIO_CONNECTED_ID_MASK) >> 8);
                                                                    // Initialize multi-play communication
                    LoadStartFlag = 0;
                    SioFlags = 0;
                    if (SioState == 0)      SioState = 3;
                    else                    SioState = 2;
                    break;
            case 2: if (SioFlags & MULTI_SIO_LD_ENABLE) {           // Update load counter
                        if (SioFlags & MULTI_SIO_LD_SUCCESS) {
                            if (++(*LoadCounterp) >= sizeof(LoadTable)/4) {
//                             *LoadCounterp = 0;
//                              BurstLoad = 0;
                            }
                        }
                       SioState = 3;
                    }
                    MultiSioStart();                                // Multi-play communication start from 2nd time on
            case 3: if (SioFlags & MULTI_SIO_LD_REQUEST) {          // To be safe, resend after confirmation
                        if (!(SioFlags & MULTI_SIO_PARENT))
                                SioState = 4;
                        else if (RecvLoadCounter < sizeof(LoadTable)/4
                                || Trg)                             // Start button with master
                                SioState = 4;
                    }
                    if (BurstLoad)
                        if ((SioFlags & MULTI_SIO_LD_ENABLE))
                            LoadStartFlag = 1;
                    SioFlags = MultiSioMain(SendBuf, RecvBuf,       // Multi-play communication main
                                            LoadStartFlag);
                    if (Trg)   MultiSioStart();                     // Start multi-play communication

                    if (RecvLoadCounter != ((CommData *)RecvBuf[0])->LoadCounter) {
                        RecvLoadCounter =  ((CommData *)RecvBuf[0])->LoadCounter;
                        ProgressCounter = 0;
					}
                    Sio32Datap = LoadTable[RecvLoadCounter];
                    break;
            case 4: MultiSioStop();                                 // Stop multi-play communication
                    if (RecvLoadCounter == sizeof(LoadTable)/4) {
                        RegisterRamReset(RESET_CPU_WRAM_FLAG        // Initialization
                                       | RESET_ALL_REG_FLAG ^ RESET_REG_SIO_FLAG);
                        ((VoidFuncp )0x02000000)();                 // Execute download program
                    }

#ifdef SIO32ML_DI_FUNC_FAST                                         // Set interrupt address
                    IntrTableBuf[0] = (IntrFuncp )IntrFuncBuf + 1;
#else
                    IntrTableBuf[0] = Sio32MultiLoadIntr;
#endif
                    Sio32MultiLoadInit((SioFlags & MULTI_SIO_PARENT),// 32bit serial communication multi-load initialization
                                        Sio32Datap);
                    SioState = 5;
                    break;
            case 5: if (Sio32MultiLoadMain(&ProgressCounter))       // 32bit serial communication multi-load main
                        SioState = 1;
                    break;
        }


        BgScSet("0x", &BgBak[32*16+7], 3);              // Display communication end data size
        Data2BgSc(RecvLoadCounter * SIO32ML_BLOCK_SIZE + ProgressCounter * 4,
                  &BgBak[32*16+9], 8, 3);


        if (SioFlags & MULTI_SIO_PARENT) {
            if (*LoadCounterp == sizeof(LoadTable)/4)
                                  StrTmp = "PUSH ANY KEY  ";
            else if (BurstLoad)   StrTmp = "NOW LOADING...";
            else                  StrTmp = "PUSH ANY KEY  ";
        } else                    StrTmp = "              ";

        if (SioFlags & MULTI_SIO_HARD_ERROR)     // Display communication error
                StrTmp = "HARD ERROR    ";
        else if (SioFlags & MULTI_SIO_ID_OVER_ERROR)
                StrTmp = "ID OVER ERROR ";
        else if (SioFlags & MULTI_SIO_RECV_FLAGS_AVAILABLE
             && (((MultiSioReturn *)&SioFlags)->RecvSuccessFlags
               ^ ((MultiSioReturn *)&SioFlags)->ConnectedFlags))
                if (SioState >= 2 && SioState <= 3)
                    StrTmp = "RECV ERROR    ";

        BgScSet(StrTmp, &BgBak[32*18+7], 1);


        if (!SioStartFlag) {                            // After conection is verified, initialize OBJ location
            if (SioFlags & MULTI_SIO_CONNECTED_ID0) {
                MultiSioId = ((SioMultiCnt *)REG_SIOCNT)->ID;
                SendBuf[0] = ((OamData *)OamData_Sample)[MultiSioId].VPos;
                SendBuf[1] = ((OamData *)OamData_Sample)[MultiSioId].HPos;
                SioStartFlag = 1;
            }
        }

        for (i=0; i<4; i++)
            if (SioFlags & (1 << i)) {
                OamBak[i].VPos = RecvBuf[i][0];
                OamBak[i].HPos = RecvBuf[i][1];
            }


        // To enable SIO interrupt, do not use DMA
        CpuFastArrayCopy(BgBak,  BG_VRAM);              // Set BG screen
        CpuFastArrayCopy(OamBak, OAM    );              // Set OAM
    }
}


