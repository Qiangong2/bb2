/********************************************************************/
/*          types.h                                                 */
/*            Type Declaration                                      */
/*                                                                  */
/*          Copyright (C) 1999-2001 NINTENDO Co.,Ltd.               */
/********************************************************************/
#ifndef _TYPES_H
#define _TYPES_H

#include <Agb.h>


typedef void (*VoidFuncp)(void);
typedef void (*IntrFuncp)(void);


// Communication Data Structure
typedef struct {
    u16 VPos;               // Y Coordinate
    u16 HPos;               // X Coordinate
    u8  LoadDataId;         // Load Data ID
    u8  LoadCounter;        // Load Counter
} CommData;
typedef  vl CommData    vCommData;


#endif /* _TYPES_H */
