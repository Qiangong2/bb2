/********************************************************************/
/*          intr_table.c                                            */
/*             Interrupt table                                      */
/*                                                                  */
/*          Copyright (C) 2001 NINTENDO Co.,Ltd.                    */
/********************************************************************/
#ifndef _INTR_TABLE_H
#define _INTR_TABLE_H

#include "types.h"


extern IntrFuncp IntrTable[13];
extern IntrFuncp IntrTableBuf[13];


#endif /* _INTR_TABLE_H */
