/********************************************************************/
/*          data.h                                                  */
/*            Data External Declaration                             */
/*                                                                  */
/*          Copyright (C) 2001 NINTENDO Co.,Ltd.                    */
/********************************************************************/
#ifndef _DATA_H
#define _DATA_H

#include <Agb.h>


/*-------------------------- Data -------------------------------*/

extern u32 CharData_Sample[8*0x180];

extern u16 PlttData_Sample[16][16];

extern u16 BgScData_Sample[32*20];

extern u32 OamData_Sample[4][2];

extern u32 font_ex[15][8*0xE0];

extern u8 _binary_demo_bin_start[],  _binary_demo_bin_end[];

#endif /* _DATA_H */
