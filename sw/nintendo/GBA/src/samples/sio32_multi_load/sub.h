/********************************************************************/
/*          sub.h                                                   */
/*            General Subroutines                                    */
/*                                                                  */
/*          Copyright (C) 1999-2001 NINTENDO Co.,Ltd.               */
/********************************************************************/
#ifndef _SUB_H
#define _SUB_H

#include <Agb.h>


/*==================================================================*/
/*   Interrupt Routine(Limit processing as much as possible)        */
/*==================================================================*/

/*------------------------------------------------------------------*/
/*                      V-Blank Processing                          */
/*------------------------------------------------------------------*/

void VBlankIntr(void);

/*------------------------------------------------------------------*/
/*                      Interrupt Dummy Routine                     */
/*------------------------------------------------------------------*/

void IntrDummy(void);


/*==================================================================*/
/*                      Sub-routine                                 */
/*==================================================================*/

/*------------------------------------------------------------------*/
/*                      Key Read                                    */
/*------------------------------------------------------------------*/

void KeyRead(void);

/*------------------------------------------------------------------*/
/*                       Set BG Screen                              */
/*------------------------------------------------------------------*/

void BgScSet(u8 *Srcp, u16 *Destp, u8 PlttNo);

/*------------------------------------------------------------------*/
/*              Hexadecimal--> BG Screen Conversion Routine         */
/*------------------------------------------------------------------*/

void Data2BgSc(u32 SrcData, u16 *Destp, u8 Num, u8 PlttNo);


#endif /* _SUB_H */
