;********************************************************************
;*          crt0_arm.s                                              *
;*            Start-up Routine (ARMASM)                             *
;*                                                                  *
;*          Copyright (C) 1999-2001 NINTENDO Co.,Ltd.               *
;********************************************************************
    INCLUDE     AgbDefineArm.s
    INCLUDE     AgbMemoryMapArm.s
    INCLUDE     AgbSyscallDefineArm.s
    INCLUDE     AgbMacroArm.s
    AREA        Init, CODE, READONLY

    INCLUDE     MultiSioDefineArm.s

    ENTRY
start
    INCLUDE     rom_header_arm.s

;--------------------------------------------------------------------
;-                      Reset                                       -
;--------------------------------------------------------------------
    EXTERN      AgbMain
    CODE32
start_vector
        mov     r0, #PSR_IRQ_MODE       ; Switches to IRQ Mode
        msr     cpsr, r0
        ldr     sp, sp_irq              ; Sets SP_irq
        mov     r0, #PSR_SYS_MODE       ; Switches to System Mode
        msr     cpsr, r0
        ldr     sp, sp_usr              ; Sets SP_usr
        ldr     r1, =INTR_VECTOR_BUF    ; Sets interrupt address
        adr     r0, intr_main
        str     r0, [r1]
        ldr     r1, =AgbMain            ; Starts & switches to 16bit code
        mov     lr, pc
        bx      r1
        b       start_vector            ; Reset

    ALIGN
sp_usr  DCD     WRAM_END - 0x100
sp_irq  DCD     WRAM_END - 0x60


    LTORG                               ; Creates literal pool


;--------------------------------------------------------------------
;-Interrupt Branch Processing (Multiple Interrupts) (Look-up Table)50-88c -
;--------------------------------------------------------------------
    EXTERN      IntrTable
    GLOBAL      intr_main
    ALIGN
    CODE32
intr_main
        mov     r3, #REG_BASE           ; Reads in IE/IF
        add     r3, r3, #OFFSET_REG_IE  ; r3:  REG_IE
        ldr     r2, [r3]                ; r2:  IF|IE
        ldrh    r1, [r3, #REG_IME - REG_IE]
                                        ; r1:  IME
        mrs     r0, spsr                ; Saves register (IRQ Mode)
        stmfd   sp!, {r0-r3, lr}        ; {spsr, IME, IF|IE, REG_IE, lr}

        mov     r0, #1                  ; IME = 1 (To permit multiple interrupts
                                         ; when an interrupt occurs while executing IME = 0)
        strh    r0, [r3, #REG_IME - REG_IE]

        and     r1, r2, r2, lsr #16     ; Checks IE/IF
        mov     r12, #0
        ands    r0, r1, #SIO_INTR_FLAG | MULTI_SIO_TIMER_INTR_FLAG
        bne     jump_intr                    ; Serial communication interrupt
        add     r12,r12, #4                  ; or Multi-play communication timerinterrupt

        ands    r0, r1, #V_BLANK_INTR_FLAG   ; V-blank interrupt
        bne     jump_intr
        add     r12,r12, #4
        ands    r0, r1, #V_COUNT_INTR_FLAG   ; V-counter interrupt
        bne     jump_intr
        add     r12,r12, #4

        ands    r0, r1, #H_BLANK_INTR_FLAG   ; H-blank interrupt
        bne     jump_intr
        add     r12,r12, #4
        ands    r0, r1, #DMA0_INTR_FLAG      ; DMA0 interrupt
        bne     jump_intr
        add     r12,r12, #4
        ands    r0, r1, #DMA1_INTR_FLAG      ; DMA1 interrupt
        bne     jump_intr
        add     r12,r12, #4
        ands    r0, r1, #DMA2_INTR_FLAG      ; DMA2 interrupt
        bne     jump_intr
        add     r12,r12, #4
        ands    r0, r1, #DMA3_INTR_FLAG      ; DMA3 interrupt
        bne     jump_intr
        add     r12,r12, #4
        ands    r0, r1, #KEY_INTR_FLAG       ; Key interrupt
        bne     jump_intr
        add     r12,r12, #4
        ands    r0, r1, #CASSETTE_INTR_FLAG  ; Game Pak interrupt
        strneb  r0, [r3, #REG_SOUNDCNT_X - REG_IE]  ; Stop sound
loop    bne     loop
jump_intr
        strh    r0, [r3, #2]                    ; Clears IF         24c
                                                    ; Sets IE <- select multiple interrupts
        ldr     r1, =CASSETTE_INTR_FLAG | SIO_INTR_FLAG | MULTI_SIO_TIMER_INTR_FLAG
        bic     r2, r2, r0                          ; Prevent the same interrupt from occurring multiple times.
        and     r1, r1, r2                          ; Permit multiple interrupts of interrupts only that were set in IE.
        strh    r1, [r3]

        mrs     r3, cpsr                            ; Permit multiple interrupts & switch to system mode
        bic     r3, r3, #PSR_CPU_MODE_MASK | PSR_IRQ_DISABLE | PSR_FIQ_DISABLE
        orr     r3, r3, #PSR_SYS_MODE
        msr     cpsr, r3
;--------------------------------------------------------------------System Mode
        ldr     r1, =IntrTable                      ; Jump to User IRQ process
        add     r1, r1, r12
        ldr     r0, [r1]

        stmfd   sp!, {lr}                           ; Save register (System Mode)
        adr     lr, intr_return                     ; Set return address
        bx      r0
intr_return
        ldmfd   sp!, {lr}                           ; Return register (System Mode)

        mrs     r3, cpsr                            ; Disable multiple interrupts & switch to IRQ mode
        bic     r3, r3, #PSR_CPU_MODE_MASK | PSR_IRQ_DISABLE | PSR_FIQ_DISABLE
        orr     r3, r3, #PSR_IRQ_MODE      | PSR_IRQ_DISABLE
        msr     cpsr, r3
;---------------------------------------------------------------------IRQ Mode
        ldmfd   sp!, {r0-r3, lr}                    ; Return register (IRQ Mode)
        strh    r2,  [r3]                       ; {spsr, IME, IF|IE, REG_IE, lr}
        strh    r1,  [r3, #REG_IME - REG_IE]
        msr     spsr, r0
        bx      lr                              ; Return to prior to interrupt


    END

