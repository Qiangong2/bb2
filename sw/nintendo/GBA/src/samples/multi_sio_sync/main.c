/********************************************************************/
/*          main.c                                                  */
/*            multi_sio (Multi-play communication sample) main      */
/*                                                                  */
/*          Copyright (C) 1999-2001 NINTENDO Co.,Ltd.               */
/********************************************************************/
//#define CODE32
#include "MultiSio.h"
#include "sound/m4aLib.h"

#include "data.h"


//#define NDEBUG

#define SOUND_ON

//#define KEY_ON                              // Flag to enable key operation of OBJ


/*-------------------- Global variable  ----------------------------*/

    u16      Cont, Trg;                         // Key input

    u32      IntrMainBuf[0x140/4];                // Interrupt main routine buffer

    u16      BgBak[32*32];                      // BG backup
    OamData  OamBak[128];                       // OAM backup

    u16      SendBuf[MULTI_SIO_BLOCK_SIZE/2];   // User send buffer 
    u16      RecvBuf[MULTI_SIO_PLAYERS_MAX][MULTI_SIO_BLOCK_SIZE/2];
                                                // User receive buffer

    u32      SioFlags;                          // Communication status
    u8       SioStartFlag;                      // Communication start flag

/*---------------------- Sub routine  -----------------------------*/

void KeyRead(void);
void BgScSet(u8 *Srcp, u16 *Destp, u8 PlttNo);


/*------------------------------------------------------------------*/
/*                      Interrupt table                             */
/*------------------------------------------------------------------*/
typedef void (*IntrFuncp)(void);
void IntrDummy(void);
void VBlankIntr(void);
void VCountIntr(void);
void SioIntr(void);

const IntrFuncp IntrTable[13] = {
#ifdef NDEBUG
#ifdef MULTI_SIO_DI_FUNC_FAST
    (IntrFuncp )IntrFuncBuf + 1,// Serial communication interrupt
#else                           // or multi-play communication timer interrupt 
    MultiSioIntr,
#endif
#else
    SioIntr,
#endif
    VBlankIntr,                 // V-blank interrupt 
    VCountIntr,                 // V-counter match interrupt 

    IntrDummy,                  // H-blank interrupt 
    IntrDummy,                  // DMA0 interrupt 
    IntrDummy,                  // DMA1 interrupt 
    IntrDummy,                  // DMA2 interrupt 
    IntrDummy,                  // DMA3 interrupt 
    IntrDummy,                  // Key interrupt 
};

IntrFuncp IntrTableBuf[14];


/*==================================================================*/
/*                      Main routine                                */
/*==================================================================*/
extern void intr_main(void);

void AgbMain(void)
{
    u32    MultiSioId;
    char  *StrTmp;
    s32    i, ii;

    RegisterRamReset(RESET_EX_WRAM_FLAG                    // Clears CPU external work RAM
                   | RESET_CPU_WRAM_FLAG);                 // Clears CPU internal work RAM

    *(vu16 *)REG_WAITCNT = CST_ROM0_1ST_3WAIT | CST_ROM0_2ND_1WAIT
                         | CST_PREFETCH_ENABLE;            // Sets 3-1 wait access

    DmaCopy(3, IntrTable, IntrTableBuf,sizeof(IntrTableBuf),32);// Sets interrupt table
    DmaCopy(3, intr_main, IntrMainBuf, sizeof(IntrMainBuf), 32);// Sets interrupt main routine
    *(vu32 *)INTR_VECTOR_BUF = (vu32 )IntrMainBuf;

    DmaArrayCopy(3, CharData_Sample, BG_VRAM+0x8000, 32);  //  Sets BG character
    DmaArrayCopy(3, CharData_Sample, OBJ_MODE0_VRAM, 32);  // Sets OBJ character  
    DmaArrayCopy(3, PlttData_Sample, BG_PLTT,        32);  //  Sets BG palette 
    DmaArrayCopy(3, PlttData_Sample, OBJ_PLTT,       32);  // Sets OBJ palette 

    DmaArrayCopy(3, BgScData_Sample, BgBak,          32);  // Sets BG screen 
    DmaArrayCopy(3, BgBak,           BG_VRAM,        32);

    DmaArrayClear(3,160,             OamBak,         32);  // Moves OBJ that is not displayed to outside of the screen 
    DmaClear(     3,160,             OAM, OAM_SIZE,  32);
    DmaArrayCopy(3, OamData_Sample,  OamBak,         32);  // Sets OAM
//    DmaArrayCopy(3, OamBak,          OAM,            32);

    for (i=0; i<MULTI_SIO_PLAYERS_MAX; i++)     OamBak[i].VPos = 200;

    *(vu16 *)REG_BG0CNT =                              // Sets BG control
            BG_COLOR_16 | BG_SCREEN_SIZE_0 | BG_PRIORITY_0
            | 0 << BG_SCREEN_BASE_SHIFT | 2 << BG_CHAR_BASE_SHIFT ;

    *(vu16 *)REG_IE   = V_BLANK_INTR_FLAG              // Enable V-blank interrupt
                      | V_COUNT_INTR_FLAG              // Enable V-count interrupt
                      | CASSETTE_INTR_FLAG;            // Enable Game Pak interrupt
    *(vu16 *)REG_STAT = STAT_V_BLANK_IF_ENABLE
                      | STAT_V_COUNT_IF_ENABLE
                      | (32 << STAT_VCOUNT_CMP_SHIFT);
    *(vu16 *)REG_IME  = 1;                             // Sets IME 

    *(vu16 *)REG_DISPCNT = DISP_MODE_0 | DISP_BG0_ON | DISP_OBJ_ON; // LCDC ON

    MultiSioInit();                                    // Initialize multi-play communication
    SendBuf[0] = 200;

#ifdef SOUND_ON
    m4aSoundInit();                                    // Initialize sound 
    m4aSongNumStart(14);                               // Start BGM  
#endif

    while(1) {
        if (!(SioFlags & MULTI_SIO_CONNECTED_ID0)   // Master or before connection relation is decided
          || (SioFlags & MULTI_SIO_PARENT))
                VBlankIntrWait();                   // Wait for V-blank interrupt end
        else    IntrWait(1, SIO_INTR_FLAG);         // Wait for SIO interrupt end

        KeyRead();                                  // Key operation 

        SioFlags = MultiSioMain(RecvBuf);           // Multi-play communication  main
        if (Trg)   MultiSioStart();                 // Starts multi-play communication

        if (Trg & A_BUTTON)  m4aSongNumStart(14);   // Starts BGM 
        if (Trg & B_BUTTON)  m4aSongNumStop(14);    // Stops BGM 

        if (SioFlags & MULTI_SIO_PARENT) {
            if (!SioStartFlag)    StrTmp = "PUSH ANY KEY  ";
            else                  StrTmp = "              ";
            BgScSet(StrTmp, &BgBak[32*11+7], 1);
        }

        if (!SioStartFlag) {                        // Initialize OBJ position after connection is recognized 
            if (SioFlags & MULTI_SIO_CONNECTED_ID0) {
                MultiSioId = ((SioMultiCnt *)REG_SIOCNT)->ID;
                SendBuf[0] = ((OamData *)OamData_Sample)[MultiSioId].VPos;
                SendBuf[1] = ((OamData *)OamData_Sample)[MultiSioId].HPos;
                SioStartFlag = 1;
            }
        } else {
            for (i=0; i<4; i++)
                if (SioFlags & (1 << i)) {
                    OamBak[i].VPos = RecvBuf[i][0];
                    OamBak[i].HPos = RecvBuf[i][1];
                }

            if (SioFlags & 0xe
              && SioFlags & (1 << MultiSioId)) {
#ifndef KEY_ON
                SendBuf[1] -= 4;
#else
                if (Cont & U_KEY)    SendBuf[0] -= 4;
                if (Cont & D_KEY)    SendBuf[0] += 4;
                if (Cont & L_KEY)    SendBuf[1] -= 4;
                if (Cont & R_KEY)    SendBuf[1] += 4;
#endif
            }

            if (SioFlags & MULTI_SIO_HARD_ERROR)    // Display communication error
                    StrTmp = "HARD ERROR    ";
            else if (SioFlags & MULTI_SIO_ID_OVER_ERROR)
                    StrTmp = "ID OVER ERROR ";
            else if (SioFlags & MULTI_SIO_RECV_FLAGS_AVAILABLE
                 && (((MultiSioReturn *)&SioFlags)->RecvSuccessFlags
                   ^ ((MultiSioReturn *)&SioFlags)->ConnectedFlags))
                    StrTmp = "RECV ERROR    ";
            else    StrTmp = "              ";
            BgScSet(StrTmp, &BgBak[32*18+7], 1);
        }

        MultiSioSendDataSet(SendBuf);               // Multi-play communication sending data set
    }
}


/*==================================================================*/
/*                      Interrupt routine                           */
/*==================================================================*/

/*------------------------------------------------------------------*/
/*                      V-blank process                             */
/*------------------------------------------------------------------*/

void VBlankIntr(void)
{
    MultiSioVSync();        // Master: Start Multi-play communication (1 frame)
                            // Slave: Response to communication stop
#ifdef SOUND_ON
    m4aSoundVSync();                                // Re-set sound DMA 
#endif

    // DMA not used in order to enable SIO interrupt 
    CpuFastArrayCopy(BgBak,  BG_VRAM);              // Sets BG screen
    CpuFastArrayCopy(OamBak, OAM    );              // Sets OAM  

    *(vu16 *)REG_IME = 0;
    *(vu16 *)INTR_CHECK_BUF |= V_BLANK_INTR_FLAG;   // Sets V-blank interrupt c heck 
    *(vu16 *)REG_IME = 1;
}

/*------------------------------------------------------------------*/
/*                      V count process                             */
/*------------------------------------------------------------------*/

void VCountIntr(void)
{
#ifdef SOUND_ON
    *(vu16 *)BG_PLTT = 0xffe0;
    m4aSoundMain();                                 // Sound main (for interrupt load distribution)
    *(vu16 *)BG_PLTT = 0xffff;
#endif
}

/*------------------------------------------------------------------*/
/*                      Communication interrupt                     */
/*------------------------------------------------------------------*/

void SioIntr(void)
{
    vu16 BgPlttBak = *(vu16 *)BG_PLTT;

    *(vu16 *)BG_PLTT = 0x001f;

#ifdef MULTI_SIO_DI_FUNC_FAST
    ((IntrFuncp )IntrFuncBuf + 1)();                // Serial communication interrupt 
#else                                               // or multi-play communication timer interrupt  
    MultiSioIntr();
#endif

    *(vu16 *)BG_PLTT = BgPlttBak;
}

/*------------------------------------------------------------------*/
/*                     Interrupt dummy routine                      */
/*------------------------------------------------------------------*/

void IntrDummy(void)
{
}

/*==================================================================*/
/*                      Sub routine                                 */
/*==================================================================*/

/*------------------------------------------------------------------*/
/*                      Read key                                    */
/*------------------------------------------------------------------*/

void KeyRead(void)
{
    u16 ReadData = (*(vu16 *)REG_KEYINPUT ^ ALL_KEY_MASK);
    Trg  = ReadData & (ReadData ^ Cont);            // Trigger input
    Cont = ReadData;                                // Hold input
}

/*------------------------------------------------------------------*/
/*                      Set BG screen                               */
/*------------------------------------------------------------------*/

void BgScSet(u8 *Srcp, u16 *Destp, u8 PlttNo)
{
    while (*Srcp != '\0')
        *Destp++ = (u16 )PlttNo << BG_SC_PLTT_SHIFT | *Srcp++;
}


