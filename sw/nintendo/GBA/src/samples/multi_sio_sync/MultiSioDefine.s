@********************************************************************
@*          MultiSioDefine.s                                        *
@*            Multi-play communication library constant (for GAS)   *
@*            (Timer interrupt transmission & communication data synchronization)            *
@*            (exclusively for multiple interrupts)                 *
@*                                                                  *
@*          Copyright (C) 1999-2001 NINTENDO Co.,Ltd.               *
@********************************************************************


@ Optimize the following set values according to software specifications. 

MULTI_SIO_BLOCK_SIZE            =   16      @ Communication data block size (max 24Byte)

MULTI_SIO_PLAYERS_MAX           =   4       @ Maximum player number

MULTI_SIO_SYNC_DATA             =   0xfefe  @ Synchronous data (0x0000/0xffff are prohibited.)


@ Comment out if there is not enough space in CPU internal working RAM.
MULTI_SIO_DI_FUNC_FAST:                     @ SIO interrupt prohibit function high speed flag (CPU internal RAM executed.)


MULTI_SIO_TIMER_NO              =   3       @ Timer No
MULTI_SIO_TIMER_INTR_FLAG       =   (TIMER0_INTR_FLAG << MULTI_SIO_TIMER_NO)
                                            @ Timer interrupt flag
REG_MULTI_SIO_TIMER             =   (REG_TM0CNT + MULTI_SIO_TIMER_NO * 4)
REG_MULTI_SIO_TIMER_L           =    REG_MULTI_SIO_TIMER
REG_MULTI_SIO_TIMER_H           =   (REG_MULTI_SIO_TIMER + 2)
                                            @ Timer register


@ Multi-play communication packet structure/offset

OFS_MSP_FRAME_COUNTER           =   0       @ Frame counter
OFS_MSP_RECV_ERROR_FLAGS        =   1       @ Receive error flag
OFS_MSP_CHECK_SUM               =   2       @ Checksum
OFS_MSP_DATA                    =   4       @ Communication data
OFS_MSP_OVERRUN_CATCH           =   (OFS_MSP_DATA + MULTI_SIO_BLOCK_SIZE)
                                            @ Overrun protected area

MULTI_SIO_PACKET_SIZE           =   (OFS_MSP_OVERRUN_CATCH + 4)
                                            @ Structure size


@ Multi-play communication work area structure/offset 

OFS_MS_TYPE                     =   0       @ Connection relation (master/slave)
OFS_MS_STATE                    =   1       @ Statis communication function
OFS_MS_CONNECTED_FLAG           =   2       @ Connection history flag
OFS_MS_RECV_SUCCESS_FLAGS       =   3       @ Receiving success flag

OFS_MS_SYNC_SEND_FLAG           =   4       @ Sending verification flag
OFS_MS_SYNC_RECV_FLAG           =   5       @ Receiving verification flag 

OFS_MS_START_FLAG               =   6       @ Communication starting flag
OFS_MS_HARD_ERROR               =   7       @ Hard error

OFS_MS_SYNC_SEND_FRAME_COUNTER  =   11      @ Sending frame counter
OFS_MS_SYNC_RECV_FRAME_COUNTER  =   12      @ Receiving frame counter

OFS_MS_SEND_BUF_COUNTER         =   20      @ Sending buffer counter
OFS_MS_RECV_BUF_COUNTER         =   24      @ Receiving buffer counter

OFS_MS_NEXT_SEND_BUF_P          =   28      @ Sending buffer pointer
OFS_MS_CURRENT_SEND_BUF_P       =   32
OFS_MS_CURRENT_RECV_BUF_P       =   36      @ Receiving buffer pointer
OFS_MS_LAST_RECV_BUF_P          =   40
OFS_MS_RECV_CHECK_BUF_P         =   44

OFS_MS_SEND_BUF                 =   48      @ Sending buffer (double buffer)
OFS_MS_RECV_BUF                 =   (OFS_MS_SEND_BUF + MULTI_SIO_PACKET_SIZE * 2)
                                            @ Receive buffer (triple buffer) 

MULTI_SIO_AREA_SIZE             =   (OFS_MS_RECV_BUF + MULTI_SIO_PACKET_SIZE * 3 * MULTI_SIO_PLAYERS_MAX)
                                            @ Structure size


    .EXTERN RecvFuncBuf                     @ Buffer to execute CPU internal RAM 
    .EXTERN IntrFuncBuf


@--------------------------------------------------------------------
@-               Initialize Multi-play Communication		    -
@--------------------------------------------------------------------

    .EXTERN MultiSioInit

@--------------------------------------------------------------------
@-                Start Multi-play Communication		    -
@--------------------------------------------------------------------

    .EXTERN MultiSioStart

@--------------------------------------------------------------------
@-                 Stop Multi-play Communication		    -
@--------------------------------------------------------------------

    .EXTERN MultiSioStop

@--------------------------------------------------------------------
@-                 Multi-play Communication Main		    -
@--------------------------------------------------------------------

    .EXTERN MultiSioMain

@--------------------------------------------------------------------
@-                 Multi-play Communication V-blank Interrupt       -
@--------------------------------------------------------------------

    .EXTERN MultiSioVSync

@--------------------------------------------------------------------
@-                Multi-play Communication Interrupt		    -
@--------------------------------------------------------------------

    .EXTERN MultiSioIntr

@--------------------------------------------------------------------
@-                Check Receiving Data				    -
@--------------------------------------------------------------------

    .EXTERN MultiSioRecvDataCheck

@--------------------------------------------------------------------
@-                Set Sending Data				    -
@--------------------------------------------------------------------

    .EXTERN MultiSioSendDataSet


