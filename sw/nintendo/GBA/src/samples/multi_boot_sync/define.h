/********************************************************************/
/*          define.h                                                */
/*            Global Macro Definition                               */
/*                                                                  */
/*          Copyright (C) 1999-2001 NINTENDO Co.,Ltd.               */
/********************************************************************/
#ifndef _DEFINE_H
#define _DEFINE_H

#include <Agb.h>


//#define NDEBUG

#define SOUND_ON

#define INITIAL_CODE    (*(vu32 *)"AGBJ")   // Initial code

#endif /* _DEFINE_H */
