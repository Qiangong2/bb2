/********************************************************************/
/*          Sio32MultiLoad.h                                        */
/*      32bit serial communication multi-load/library external declaration*/
/*            (Timer interrupt send)                                */
/*                                                                  */
/*          Copyright (C) 2001 NINTENDO Co.,Ltd.                    */
/********************************************************************/
#ifndef _SIO32_MULTI_LOAD_H
#define _SIO32_MULTI_LOAD_H

#ifdef __cplusplus
extern "C" {
#endif

#include <Agb.h>
#include "MultiSio.h"


// Optimize the following settings based on the software specifications. 

#define SIO32ML_BLOCK_SIZE          0x8000  // Communication data block size

// Update if maximum delay for communication interrupt is larger than following.
#define SIO32ML_INTR_DELAY_MAX      1000    // Communication interrupt allowed delay clocks

#define SIO32ML_INTR_CLOCK_MAX      256     // Communication interrupt processing maximum clocks

#define SIO32ML_SYNC_DATA           0xfefefefe  // Synchronized data (0x00000000 and 0xffffffff prohibited)

#define SIO32ML_SYSTEM_CLOCK        (16  * 1024 * 1024)// System clock
#define SIO32ML_SHIFT_CLOCK         (256 * 1024)       // Shift clock
#define SIO32ML_LINE_CLOCKS         1232               // Line clocks

// Comment out if no space in CPU internal work RAM. 
#ifdef  MULTI_SIO_DI_FUNC_FAST
#define SIO32ML_DI_FUNC_FAST                // SIO interrupt prohibit function high speed flag (CPU internal RAM execution)
#endif

#define SIO32ML_TIMER_NO            3       // Timer No
#define SIO32ML_TIMER_INTR_FLAG     (TIMER0_INTR_FLAG << SIO32ML_TIMER_NO)
                                            // Timer interrupt flag
#define REG_SIO32ML_TIMER           (REG_TM0CNT + SIO32ML_TIMER_NO * 4)
#define REG_SIO32ML_TIMER_L          REG_SIO32ML_TIMER
#define REG_SIO32ML_TIMER_H         (REG_SIO32ML_TIMER + 2)
                                            // Timer register

// Timer count temporary value is calculated from communication data block size. 
#define SIO32ML_TIMER_COUNT_TMP     (SIO32ML_SYSTEM_CLOCK/SIO32ML_SHIFT_CLOCK*32 \
                                    + SIO32ML_INTR_DELAY_MAX + SIO32ML_INTR_CLOCK_MAX)
                                            // Timer count temporary value
#define SIO32ML_TIMER_COUNT_MAX     0x10000 // Timer count maximum value
#define SIO32ML_TIMER_COUNT         ((SIO32ML_TIMER_COUNT_TMP > SIO32ML_TIMER_COUNT_MAX)   \
                                    ? SIO32ML_TIMER_COUNT_MAX - SIO32ML_TIMER_COUNT_MAX    \
                                    : SIO32ML_TIMER_COUNT_MAX - SIO32ML_TIMER_COUNT_TMP)
                                            // Timer count

// Load timeout frames is calculated from above values.
#define SIO32ML_MODE_WAIT_FRAMES    6       // SIO mode switch wait frame
#define SIO32ML_LD_TIMEOUT_FRAMES   (((SIO32ML_TIMER_COUNT_MAX - SIO32ML_TIMER_COUNT        \
                                     + SIO32ML_INTR_DELAY_MAX  + SIO32ML_INTR_CLOCK_MAX)    \
                                    * (SIO32ML_BLOCK_SIZE/4)) / (SIO32ML_LINE_CLOCKS * 228) \
                                    +  SIO32ML_MODE_WAIT_FRAMES + 2)
                                            // Load timeout frames


// 32bit serial communication multi-load work area structure
typedef struct {
    u8  Type;                               // Connection (master/slave)
    u8  State;                              // Communication function state
    u8  FrameCounter;                       // Frame counter
    u8  DownloadSuccessFlag;                // Download success flag

    u32 *Datap;                             // Data pointer
    s32  DataCounter;                       // Data counter

    u32 CheckSum;                           // Checksum
    u32 CheckSumTmp;
    s32 CheckSumCounter;                    // Checksum counter
} Sio32MultiLoadArea;


extern Sio32MultiLoadArea  S32ml;           // 32bit serial communication multi-load work area 

/*------------------------------------------------------------------*/
/*                  32bit serial communication multi-load initialization             */
/*------------------------------------------------------------------*/

extern void Sio32MultiLoadInit(u32 Type, void *Datap);

//- Initialize register and buffer. 
//- To avoid problem with shift clock synchronization set master to 256Kbps 
//  32bit serial communication mode.  
//
//- 2Mbps communication with a cable is not guaranteed so do not set up.  
//- Set already connection already determined with multi-play communication to Type.
//- Do not pass VRAM address to Datap during display.  
//  May fail with checksum of stored data. 
//  Copy to VRAM after storing to work RAM once.  
//  After this, possible to overwrite program data to work RAM.
//
//Argument:
//    u32   Type    Connection (Master = Not 0/ Slave = 0) 
//    void *Datap   Data pointer (Master = Send data/ slave - Receive buffer)

/*------------------------------------------------------------------*/
/*                  32bit serial communication multi-load main             */
/*------------------------------------------------------------------*/

extern u32 Sio32MultiLoadMain(u32 *ProgressCounterp);

//- After 1 frame wait, set the slave to 256kbps 32 bit serial communication
//  Also, until the slave's SIO mode is determined the waits and then communication is started.
//- Slave successively calculates receive data checksum, and checks when reeive complete.
//- If receive is not completed at time when should be, end receive forcibly. 
//-After communication ends prohibit communication interrupts, and return to multi-play communication mode. 
//
//Arguments;
//    u32 *ProgressCounterp     Progress counter pointer
//                              (Disabled when 0 specified)
//Return value:
//    During communicatio = 0/end = not 0  

/*------------------------------------------------------------------*/
/*     32bit serial communication multi-load interrupt routine      */
/*------------------------------------------------------------------*/

extern void Sio32MultiLoadIntr(void);

//-Master does only send and slave only receive.
//-Master resets timer during send.
//-Slave stores receive data in order to receive buffer during communication interrupt.  
// Lastly it stores checksum to work area.  
//
//-Program so slave is called with communication interrupt and master 
//  is called with timer interrupt.
//-Data is passed with pipeline operation so delayed with slave with
//  large ID and then received. 
//  Therefore at a minimum need to send two (8Bytes) extra times.
//
//-Example
//  ID      0    1    2    3
//  Data    0 -> 0 -> 0 -> 0
//          1 -> 0 -> 0 -> 0
//          2 -> 1 -> 0 -> 0
//          3 -> 2 -> 1 -> 0
//          4 -> 3 -> 2 -> 1


#ifdef __cplusplus
}      /* extern "C" */
#endif

#endif /* _SIO32_MULTI_LOAD_H */
