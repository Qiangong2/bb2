/********************************************************************/
/*          sub.c                                                   */
/*            General subroutines                                   */
/*                                                                  */
/*          Copyright (C) 1999-2001 NINTENDO Co.,Ltd.               */
/********************************************************************/
//#define CODE32
#include "MultiSio.h"
#include "sound/m4aLib.h"

#include "define.h"
#include "types.h"

#include "sub.h"
#include "intr_table.h"
#include "data.h"
#include "bss.h"


/*==================================================================*/
/*                      Interrupt routine                           */
/*==================================================================*/

/*------------------------------------------------------------------*/
/*                      V-blank processing                          */
/*------------------------------------------------------------------*/

void VBlankIntr(void)
{
    if (SioState >= 2 && SioState <= 3)             // Master: Start Multi-play communication (1 frame)
        MultiSioVSync();                            // Slave: Response to communication stop

    // DMA unused to enable SIO interrupt 
    CpuFastArrayCopy(BgBak,  BG_VRAM);              // Set BG screen
    CpuFastArrayCopy(OamBak, OAM    );              // Set OAM  

    *(vu16 *)REG_IME = 0;
    *(vu16 *)INTR_CHECK_BUF |= V_BLANK_INTR_FLAG;   // Set V-blank interrupt check 
    *(vu16 *)REG_IME = 1;
}

/*------------------------------------------------------------------*/
/*                      V-count process                              */
/*------------------------------------------------------------------*/

void VCountIntr(void)
{
}

/*------------------------------------------------------------------*/
/*                      Communication interrupt (for debug)         */
/*------------------------------------------------------------------*/
#ifndef NDEBUG

void SioIntr(void)
{
    vu16 BgPlttBak = *(vu16 *)BG_PLTT;

    *(vu16 *)BG_PLTT = 0x001f;

    switch (SioState) {
        case 5:
#ifdef SIO32ML_DI_FUNC_FAST                         // Set interrupt address 
               ((IntrFuncp )IntrFuncBuf + 1)();
#else
                Sio32MultiLoadIntr();
#endif
                break;
        case 2:
        case 3:
#ifdef MULTI_SIO_DI_FUNC_FAST
                ((IntrFuncp )IntrFuncBuf + 1)();    // Serial communication interrupt 
#else                                               // or Multi-play communication timer interrupt 
                MultiSioIntr();
#endif
                break;
    }

    *(vu16 *)BG_PLTT = BgPlttBak;
}

#endif

/*------------------------------------------------------------------*/
/*                      Interrupt dummy routine                     */
/*------------------------------------------------------------------*/

void IntrDummy(void)
{
}

/*==================================================================*/
/*                      Subroutine                                 */
/*==================================================================*/

/*------------------------------------------------------------------*/
/*                      Key read                                    */
/*------------------------------------------------------------------*/

void KeyRead(void)
{
    u16 ReadData = (*(vu16 *)REG_KEYINPUT ^ ALL_KEY_MASK);
    Trg  = ReadData & (ReadData ^ Cont);            // Trigger input
    Cont = ReadData;                                //  Hold input
}

/*------------------------------------------------------------------*/
/*                      Set BG screen                               */
/*------------------------------------------------------------------*/

void BgScSet(u8 *Srcp, u16 *Destp, u8 PlttNo)
{
    while (*Srcp != '\0')
        *Destp++ = (u16 )PlttNo << BG_SC_PLTT_SHIFT | *Srcp++;
}

/*------------------------------------------------------------------*/
/*       Hexadecimal data -> BG screen conversion routine           */
/*------------------------------------------------------------------*/

void Data2BgSc(u32 SrcData, u16 *Destp, u8 Num, u8 PlttNo)
{
    u32 CharNoTmp;
    u32 PlttNoTmp = PlttNo << BG_SC_PLTT_SHIFT;
    int i;

    SrcData <<= (8-Num)*4;
    for (i=0; i<Num; i++) {
        CharNoTmp = (SrcData >> 28);
        if (CharNoTmp < 0xA)    CharNoTmp += 0x30;
        else                    CharNoTmp += 0x41 - 0xa;
        Destp[i] = CharNoTmp | PlttNoTmp;
        SrcData <<= 4 ;
    }
}


