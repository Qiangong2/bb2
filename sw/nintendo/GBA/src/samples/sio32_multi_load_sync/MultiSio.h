/********************************************************************/
/*     MultiSio.h                                                   */
/*      Multi-play communication library external declaration       */
/*      (Timer interrupt send & communication data synchronization) */
/*      (32bit serial communication multi-load compatible)          */
/*      (for multiple interrupts only)                              */
/*                                                                  */
/*      Copyright (C) 1999-2002 NINTENDO Co.,Ltd.                   */
/********************************************************************/
#ifndef _MULTI_SIO_H
#define _MULTI_SIO_H

#ifdef __cplusplus
extern "C" {
#endif

#include <Agb.h>


// Optimize the following settings based on the software specifications. 

#define MULTI_SIO_BLOCK_SIZE        16          // Communication data block size(max.24Byte)

#define MULTI_SIO_PLAYERS_MAX       4           // Maximum number of player 

#define MULTI_SIO_SYNC_DATA         0xfefe      // Synchronized data (0x0000/0xffff are prohibited)

#define MULTI_SIO_TIMEOUT_FRAMES    4           // Number of communication time-out frames (1-255)

// Comment out if no space in CPU internal work RAM.
#define MULTI_SIO_DI_FUNC_FAST                  // SIO interrupt disabling function speed-up flag (Executes in CPU internal RAM)


// Update if maximum delay for communication interrupt is larger than following. 
#define MULTI_SIO_INTR_DELAY_MAX    2000        // Communication interrupt allowed delay clocks

#ifdef  MULTI_SIO_DI_FUNC_FAST
#define MULTI_SIO_INTR_CLOCK_MAX    400         // Communication interrupt processing maximum clocks
#else
#define MULTI_SIO_INTR_CLOCK_MAX    1000
#endif

#define MULTI_SIO_1P_SEND_CLOCKS    3000        // Communication time per unit

#if     MULTI_SIO_PLAYERS_MAX == 4
#define MULTI_SIO_START_BIT_WAIT    0           // Start bit wait time
#else
#define MULTI_SIO_START_BIT_WAIT    512
#endif

// During development set NDEBUG to undefined and value below to 0, 
// define with last check and confirm operation with changed to 600.  
// (Even if increase setting the communication interval increases, but processing doesn't slow). 
//#define NDEBUG                                  // Can define with Makefile (MakefileDemo)
#ifdef  NDEBUG
#define MULTI_SIO_INTR_MARGIN       600         // Communication interrupt error guarantee value 
#else
#define MULTI_SIO_INTR_MARGIN       0
#endif


#define MULTI_SIO_BAUD_RATE         115200          // Baud rate
#define MULTI_SIO_BAUD_RATE_NO      SIO_115200_BPS  // Baud rate No


#define MULTI_SIO_TIMER_NO          3           // Timer No
#define MULTI_SIO_TIMER_INTR_FLAG   (TIMER0_INTR_FLAG << MULTI_SIO_TIMER_NO)
                                                // Timer interrupt flag
#define REG_MULTI_SIO_TIMER         (REG_TM0CNT + MULTI_SIO_TIMER_NO * 4)
#define REG_MULTI_SIO_TIMER_L        REG_MULTI_SIO_TIMER
#define REG_MULTI_SIO_TIMER_H       (REG_MULTI_SIO_TIMER + 2)
                                                // Timer register

// Timer count number is calculated from communication data block size. 
#define MULTI_SIO_TIMER_COUNT_TMP   ((SYSTEM_CLOCK/60) \
                                    /(1 + (4 + MULTI_SIO_BLOCK_SIZE + 4)/(16/8)))
                                                // Tentative value for timer count
#define MULTI_SIO_TIMER_COUNT_MIN   ( MULTI_SIO_1P_SEND_CLOCKS * MULTI_SIO_PLAYERS_MAX    \
                                    + MULTI_SIO_START_BIT_WAIT + MULTI_SIO_INTR_MARGIN    \
                                    + MULTI_SIO_INTR_DELAY_MAX + MULTI_SIO_INTR_CLOCK_MAX)
                                                // Timer count minimum value 
#define MULTI_SIO_TIMER_COUNT_MAX   0x10000     // Timer count maximum value
#define MULTI_SIO_TIMER_COUNT       (MULTI_SIO_TIMER_COUNT_MAX - MULTI_SIO_TIMER_COUNT_TMP)
                                                // Timer count number
// Timer count setup error
#if   MULTI_SIO_TIMER_COUNT_TMP < MULTI_SIO_TIMER_COUNT_MIN
    #error MULTI_SIO_TIMER_COUNT is too short,
    #error because MULTI_SIO_BLOCK_SIZE or MULTI_SIO_INTR_DELAY_MAX is too large.
#elif MULTI_SIO_TIMER_COUNT_TMP > MULTI_SIO_TIMER_COUNT_MAX
    #error MULTI_SIO_TIMER_COUNT is too long.
#endif


// Multi-play communication packet structure 
typedef struct {
    u8  FrameCounter;                       // Frame counter
    u8  RecvErrorFlags:4;                   // Receive error flag
    u8  LoadRequest:1;                      // Load request
    u8  DownloadSuccessFlag:1;              // Download success flag
    u8  LoadSuccessFlag:1;                  // Load success
    u8  Reserved_0:1;                       // Reserve
    u16 CheckSum;                           // Checksum
    u16 Data[MULTI_SIO_BLOCK_SIZE/2];       // Communication data
    u16 OverRunCatch[2];                    // Overrun protect area
} MultiSioPacket;


// Multi-play communication work area structure 
typedef struct {
    u8  Type;                               // Connection (master/slave)
    u8  State;                              // Communication function state
    u8  ConnectedFlags:4;                   // Connection history flag
    u8  TotalConnectedFlags:4;              // Previous Connection history flag
    u8  RecvSuccessFlags;                   // Receive success flag

    u8  SyncSendFlag;                       // Send verify flag
    u8  SyncRecvFlag;                       // Receive verify flag

    u8  DownloadSuccessFlags:4;             // Download success flag
    u8  LoadEnable:1;                       // Permit load
    u8  LoadRequest:1;                      // Load request
    u8  LoadSuccessFlag:1;                  // Load success
    u8  StartFlag:1;                        // Communication start flag

    u8  HardError;                          // Hard error

    u8  RecvFlagsAvailableCounter;          // Receiving success flag validation counter

    u8  TimeOutCounter;                     // Communication time-out counter 


    s8  Padding0[1];

    u8  SendFrameCounter;                   // Send frame counter
    u8  RecvFrameCounter[4][2];             // Receive frame counter

    s32 SendBufCounter;                     // Send buffer counter
    s32 RecvBufCounter;                     // Receive buffer counter

    u16 *NextSendBufp;                      // Send buffer pointer
    u16 *CurrentSendBufp;
    u16 *CurrentRecvBufp;                   // Receive buffer pointer
    u16 *LastRecvBufp;
    u16 *RecvCheckBufp;

    MultiSioPacket  SendBuf[2];             // Send buffer (double buffer)
    MultiSioPacket  RecvBuf[3][MULTI_SIO_PLAYERS_MAX];
                                            // Receive buffer (triple buffer)
} MultiSioArea;


extern u32 RecvFuncBuf[0x40/4];             // Program buffer for updating receive data buffer  
extern u32 IntrFuncBuf[0x200/4];            // Interrupt routine RAM execute buffer (When -O2 is specified, you can reduce up to 0x120 Bytes.)

extern MultiSioArea  Ms;                    // Clear Multi-play communication work area


/*------------------------------------------------------------------*/
/*                      Multi-play communication initialization     */
/*------------------------------------------------------------------*/

extern void MultiSioInit(u32 ConnectedFlags);

//- Set serial communication mode to multi-play mode. 
//- Initialize register and buffer. 
// - Argument:
//    u32 ConnectedFlags    Set applicable flag if a unit recognized as connected. 

/*------------------------------------------------------------------*/
/*                     Start multi-play communication               */
/*------------------------------------------------------------------*/

void MultiSioStart(void);

// - Set a flag to start send if after master is recognized. 
// - Do nothing if slave or prior to master recognition.

/*------------------------------------------------------------------*/
/*                      Stop multi-play communication               */
/*------------------------------------------------------------------*/

void MultiSioStop(void);

//- Stop communication.

/*------------------------------------------------------------------*/
/*                      Multi-play communication main               */
/*------------------------------------------------------------------*/

extern u32 MultiSioMain(void *Recvp);

//- First determine if master or slave.  If master recognized, set up timer.  
// - Call MultiSioRecvDataCheck(), check if data is received normally, and copy receive data to  
//  Recvp.
//
//- Set so it is called immediately after interrupt wait end. 
//- It is safe not to send data that matches with a flag data (SIO_SYNC_DATA) 
//  before connection is decided. 
//
//- Arguments:
//    void *Recvp           User receive buffer pointer 

//- Return value:

#define MULTI_SIO_RECV_ID_MASK          0x000f  // Receive success flag
#define MULTI_SIO_CONNECTED_ID_MASK     0x0f00  // Connection history flag

#define MULTI_SIO_RECV_ID_SHIFT         0
#define MULTI_SIO_CONNECTED_ID_SHIFT    8

#define MULTI_SIO_RECV_ID0              0x0001  // Receive success flag Master
#define MULTI_SIO_RECV_ID1              0x0002  // Slave 1
#define MULTI_SIO_RECV_ID2              0x0004  // Slave 2
#define MULTI_SIO_RECV_ID3              0x0008  // Slave 3
#define MULTI_SIO_LD_ENABLE             0x0010  // Enable load
#define MULTI_SIO_LD_REQUEST            0x0020  // Load request
#define MULTI_SIO_LD_SUCCESS            0x0040  // Load success
#define MULTI_SIO_TYPE                  0x0080  // Connection (Mater/Slave)
#define MULTI_SIO_PARENT                0x0080  // Connect Master
#define MULTI_SIO_CHILD                 0x0000  // Connect Slave
#define MULTI_SIO_CONNECTED_ID0         0x0100  // Connection history flag Master
#define MULTI_SIO_CONNECTED_ID1         0x0200  // Slave 1
#define MULTI_SIO_CONNECTED_ID2         0x0400  // Slave 2
#define MULTI_SIO_CONNECTED_ID3         0x0800  // Slave 3
#define MULTI_SIO_HARD_ERROR            0x1000  // Hard error
#define MULTI_SIO_ID_OVER_ERROR         0x2000  // ID over error
#define MULTI_SIO_RECV_FLAGS_AVAILABLE  0x8000  // Validate receive success flag

// Return value structure
typedef struct {
    u32 RecvSuccessFlags:4;                 // Receive success flag
    u32 LoadEnable:1;                       // Enable load
    u32 LoadRequest:1;                      // Load request
    u32 LoadSuccessFlag:1;                  // Load success
    u32 Type:1;                             // Connection (master/slave)
    u32 ConnectedFlags:4;                   // Connection history flag
    u32 HardError:1;                        // Hard error
    u32 ID_OverError:1;                     // ID over error
    u32 Reserved:1;                         // Reservation
    u32 RecvFlagsAvailable:1;               // Validate receive success flag

} MultiSioReturn;


/*------------------------------------------------------------------*/
/*     Multi-play communication V-blank interrupt processing        */
/*------------------------------------------------------------------*/

extern void MultiSioVSync(void);

// - If verified as master, start communication of 1 frame worth,
//   if verified as slave, set a dummy communication interrupt flag in
//   INTR_CHECK_BUF(0x3007ff8) when communication cannot be performed.
// - Call immediately after V-blank interrupt (e.g. right before SoundVSync()).
//

/*------------------------------------------------------------------*/
/*                     Multi-play communication interrupt           */
/*------------------------------------------------------------------*/

extern void MultiSioIntr(void);

//- During communication interrupt store receive data from each unit in each receive buffer and set   
//  the send buffer data to the register.
//- Store communication error flag also in the work area.
//- If master, reset timer and resume send. 
//- Set interrupt check flag when synchronous data is received. 
//- Notifiy occurrence of communication interrupt to MultiSioVSync().
//
//- Program so slave is called with communication interrupt and master is called with timer interrupt.  
//   
//- Adjust setting so 1 packet (except OverRunCatch[]) can be transferred with 1 frame.  
//   


/*------------------------------------------------------------------*/
/*                     Check receive data                           */
/*------------------------------------------------------------------*/

extern u32  MultiSioRecvDataCheck(void *Recvp);

//- Check if receive done normally.  If normal, copy the receive data to the 
//  user receive buffer.   
//
// - Called from MultiSioMain().
//- Not necessary to call directly. 
//
//-Argument:
//    void *Recvp      User receive buffer pointer 


/*------------------------------------------------------------------*/
/*                      Set send data                               */
/*------------------------------------------------------------------*/

extern void MultiSioSendDataSet(void *Sendp, u32 LoadRequest);

//- Set user send buffer data to send buffer. 
//
//- Call when 1 frame data is determined.   
//
//Argument:
//    void *Sendp      User send buffer pointer 
//    u32  LoadRequest Load request


#ifdef __cplusplus
}      /* extern "C" */
#endif

#endif /* _MULTI_SIO_H */
