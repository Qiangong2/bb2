/********************************************************************/
/*          bss.c                                                   */
/*            Global Variable                                       */
/*                                                                  */
/*          Copyright (C) 1999-2001 NINTENDO Co.,Ltd.               */
/********************************************************************/
//#define CODE32
#include "bss.h"


/*-------------------- Global variable  ----------------------------*/

u16      Cont, Trg;                             // Key input

u32      IntrMainBuf[0x140/4];        // Interrupt main routine buffer 

u16      BgBak[32*32];              // BG backup
OamData  OamBak[128];               // OAM backup

u16      SendBuf[MULTI_SIO_BLOCK_SIZE/2];       // User send buffer
u16      RecvBuf[MULTI_SIO_PLAYERS_MAX][MULTI_SIO_BLOCK_SIZE/2];
                                                // User receive buffer

u32      SioState;                              // SIO mode switch variable
u32      SioFlags;                              // Communication status
u32      SioFlagsBak;
u8       SioStartFlag;                          // Communication start flag


