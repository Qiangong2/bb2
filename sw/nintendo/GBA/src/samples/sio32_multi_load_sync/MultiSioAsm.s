@********************************************************************
@*          MultiSioAsm.s                                           *
@*            Multi-play Communication Library (GAS)                *
@*   (Timer interrupt send & Synchronous communication data)        *
@*            (Exclusive for multiple interrupts)                   *
@*                                                                  *
@*          Copyright (C) 1999-2001 NINTENDO Co.,Ltd.               *
@********************************************************************
    .INCLUDE    "AgbDefine.s"
    .INCLUDE    "AgbMemoryMap.s"
    .INCLUDE    "AgbSyscallDefine.s"
    .INCLUDE    "AgbMacro.s"
    .TEXT

    .INCLUDE    "MultiSioDefine.s"


    .ifdef MULTI_SIO_DI_FUNC_FAST

@--------------------------------------------------------------------
@-        Routine to Update Received Data Check Buffer              -
@--------------------------------------------------------------------

    .EXTERN     Ms
    .GLOBAL     MultiSioRecvBufChange
    .CODE 32
MultiSioRecvBufChange:
        stmfd   sp!, {r8-r11}

        mov     r12, #REG_BASE                      @ r12: REG_BASE
        ldr     r11, =Ms                            @ r11: &Ms
        add     r10, r11, #OFS_MS_LAST_RECV_BUF_P   @ r10: &Ms.LastRecvBufp[0]
        mov     r9,  #1                             @ r9:  1
        mov     r8,  #0                             @ r8:  0


        @ *(vu8 *)REG_IME = 0   Disables interrupt (15 clocks)

        strb    r8,  [r12, #OFFSET_REG_IME]

        @ Ms.RecvCheckBufp <--> Ms.LastRecvBufp

        ldmia   r10,  {r0-r1}
        stmia   r10!, {r1}
        stmia   r10!, {r0}

        @ SyncRecvFlagBak[i] = Ms.SyncRecvFlag[i]
        @ Ms.SyncRecvFlag[i] = 0

        ldrb    r0,  [r11, #OFS_MS_SYNC_RECV_FLAG]
        strb    r8,  [r11, #OFS_MS_SYNC_RECV_FLAG]

        @ *(vu8 *)REG_IME = 1   Enables interrupt

        strb    r9,  [r12, #OFFSET_REG_IME]

        @ return  Ms.SyncRecvFlag[i]

        ldmfd   sp!, {r8-r11}
        bx      lr

    .LTORG                                          @ Creates literal pool


    .endif


    .END

