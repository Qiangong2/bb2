;********************************************************************
;*          crt0_demo_arm.s                                         *
;*            Startup Routine (ARMASM)                              *
;*                                                                  *
;*          Copyright (C) 1999-2001 NINTENDO Co.,Ltd.               *
;********************************************************************
    INCLUDE     AgbDefineArm.s
    INCLUDE     AgbMemoryMapArm.s
    INCLUDE     AgbSyscallDefineArm.s
    INCLUDE     AgbMacroArm.s
    AREA        Init, CODE, READONLY

    ENTRY
start

;--------------------------------------------------------------------
;-                      Reset                                       -
;--------------------------------------------------------------------
    EXTERN      DemoMain
    EXTERN      intr_main
    CODE32
start_vector
        mov     r0, #PSR_IRQ_MODE       ; Switches to IRQ mode
        msr     cpsr, r0
        ldr     sp, sp_irq              ; Sets SP_irq
        mov     r0, #PSR_SYS_MODE       ; Switches to system mode
        msr     cpsr, r0
        ldr     sp, sp_usr              ; Sets SP_usr
        ldr     r1, =INTR_VECTOR_BUF    ; Sets interrupt address
        ldr     r0, =intr_main
        str     r0, [r1]
        ldr     r1, =DemoMain           ; Starts and switches to 16-bit code
        mov     lr, pc
        bx      r1
        b       start_vector            ; Reset

    ALIGN
sp_usr  DCD     WRAM_END - 0x100
sp_irq  DCD     WRAM_END - 0x60


    END

