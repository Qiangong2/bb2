/********************************************************************/
/*          bss.h                                                   */
/*            Global Variable External Declaration                  */
/*                                                                  */
/*          Copyright (C) 1999-2001 NINTENDO Co.,Ltd.               */
/********************************************************************/
#ifndef _BSS_H
#define _BSS_H

#include "MultiSio.h"
#include "types.h"


extern u16      Cont, Trg;                          // Key input

extern u32      IntrMainBuf[0x140/4];        // Interrupt main routine buffer 

extern u16      BgBak[32*32];              // BG backup
extern OamData  OamBak[128];               // OAM backup

extern u16      SendBuf[MULTI_SIO_BLOCK_SIZE/2];    // User send buffer
extern u16      RecvBuf[MULTI_SIO_PLAYERS_MAX][MULTI_SIO_BLOCK_SIZE/2];
                                                    // User receive buffer

extern u32      SioState;                           // SIO mode switch variable
extern u32      SioFlags;                           // Communication status
extern u32      SioFlagsBak;
extern u8       SioStartFlag;                       // Communication start flag


#endif /* _BSS_H */
