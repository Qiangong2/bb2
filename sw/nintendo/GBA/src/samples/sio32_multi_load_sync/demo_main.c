/********************************************************************/
/*          demo_main.c                                             */
/*            Download Program Main                                 */
/*                                                                  */
/*          Copyright (C) 1999-2001 NINTENDO Co.,Ltd.               */
/********************************************************************/
//#define CODE32
#include "MultiSio.h"
#include "sound/m4aLib.h"

#include "define.h"
#include "types.h"

#include "sub.h"
#include "intr_table.h"
#include "data.h"
#include "bss.h"


//#define KEY_ON                              // Flag to enable OBJ key operation 


/*==================================================================*/
/*                      Main Routine                                */
/*==================================================================*/
extern void intr_main(void);

void DemoMain(void)
{
    u32    MultiSioId;
    char  *StrTmp;
    s32    i;

    // Be cautious of clearing of wait control when register clear is specified 
    RegisterRamReset(RESET_CPU_WRAM_FLAG);                 // Clear CPU internal work RAM

    DmaCopy(3, IntrTable, IntrTableBuf,sizeof(IntrTableBuf),32);// Set interrupt table
    DmaCopy(3, intr_main, IntrMainBuf, sizeof(IntrMainBuf), 32);// Set interrupt main routine

    *(vu32 *)INTR_VECTOR_BUF = (vu32 )IntrMainBuf;

    DmaArrayCopy(3, CharData_Sample, BG_VRAM+0x8000, 32);  //  Set BG character
    DmaArrayCopy(3, CharData_Sample, OBJ_MODE0_VRAM, 32);  // Set OBJ character
    DmaArrayCopy(3, PlttData_Sample, BG_PLTT,        32);  //  Set BG palette
    DmaArrayCopy(3, PlttData_Sample, OBJ_PLTT,       32);  // Set OBJ palette

    DmaArrayCopy(3, BgScData_Sample, BgBak,          32);  // Set BG screen
    DmaArrayCopy(3, BgBak,           BG_VRAM,        32);

    DmaArrayClear(3,160,             OamBak,         32);  // Undisplayed OBJ moves outside screen
    DmaClear(     3,160,             OAM, OAM_SIZE,  32);
    DmaArrayCopy(3, OamData_Sample,  OamBak,         32);  // Set OAM 
//    DmaArrayCopy(3, OamBak,          OAM,            32);

    for (i=0; i<MULTI_SIO_PLAYERS_MAX; i++)     OamBak[i].VPos = 200;

    *(vu16 *)REG_BG0CNT =                              // Set BG control
            BG_COLOR_16 | BG_SCREEN_SIZE_0 | BG_PRIORITY_0
            | 0 << BG_SCREEN_BASE_SHIFT | 2 << BG_CHAR_BASE_SHIFT ;

    // Select to enable interrupt by distinguishing Game Pak insertion 
    *(vu16 *)REG_IE   = V_BLANK_INTR_FLAG              // Enable V-blank interrupt
                      | V_COUNT_INTR_FLAG;             // Enable V count interrupt
    if (*(vu8  *)(ROM_BANK0+0xb2) == 0x96              // Check Game Pak inserted
     && *(vu32 *)(ROM_BANK0+0xac) == INITIAL_CODE)
        *(vu16 *)REG_IE |= CASSETTE_INTR_FLAG;         // Enable Game Pak interrupt
    *(vu16 *)REG_STAT = STAT_V_BLANK_IF_ENABLE
                      | STAT_V_COUNT_IF_ENABLE
                      | (24 << STAT_VCOUNT_CMP_SHIFT);
    *(vu16 *)REG_IME  = 1;                             // Set IME

    MultiSioInit(0);                                   // Multi-play communication initialization
    SioState = 2;                                      // Because common VBlankIntr()
    SendBuf[0] = 200;

    *(vu16 *)REG_DISPCNT = DISP_MODE_0 | DISP_BG0_ON | DISP_OBJ_ON; // LCDC ON

#ifdef SOUND_ON
    m4aSoundInit();                                     // Initialize sound
    m4aSongNumStart(14);                                // Start BGM
#endif

    while(1) {
        if (!(SioFlags & MULTI_SIO_CONNECTED_ID0)   // Master or before connection is decided
          || (SioFlags & MULTI_SIO_PARENT))
                VBlankIntrWait();                   // Wait for V-blank interrupt end
        else    IntrWait(1, SIO_INTR_FLAG);         // Wait for SIO interrupt end

        KeyRead();                                  // Key operation

        SioFlags = MultiSioMain(RecvBuf);           // Multi-play communication main
        MultiSioStart();                            // Start Multi-play communication

        if (Trg & A_BUTTON)  m4aSongNumStart(14);   // Start BGM 
        if (Trg & B_BUTTON)  m4aSongNumStop(14);    // Stop BGM

        if (SioFlags & MULTI_SIO_PARENT) {
            if (!SioStartFlag)    StrTmp = "PUSH ANY KEY  ";
            else                  StrTmp = "              ";
            BgScSet(StrTmp, &BgBak[32*18+7], 1);
        }

        if (!SioStartFlag) {                        // After connection is recognized initialize OBJ position
            if (SioFlags & MULTI_SIO_CONNECTED_ID0) {
                MultiSioId = ((SioMultiCnt *)REG_SIOCNT)->ID;
                SendBuf[0] = ((OamData *)OamData_Sample)[MultiSioId].VPos;
                SendBuf[1] = ((OamData *)OamData_Sample)[MultiSioId].HPos;
                SioStartFlag = 1;
            }
        } else {
            for (i=0; i<4; i++)
                if (SioFlags & (1 << i)) {
                    OamBak[i].VPos = RecvBuf[i][0];
                    OamBak[i].HPos = RecvBuf[i][1];
                }

            if (SioFlags & 0xe
             && SioFlags & (1 << MultiSioId)) {
#ifndef KEY_ON
                SendBuf[1] -= 4;
#else
                if (Cont & U_KEY)    SendBuf[0] -= 4;
                if (Cont & D_KEY)    SendBuf[0] += 4;
                if (Cont & L_KEY)    SendBuf[1] -= 4;
                if (Cont & R_KEY)    SendBuf[1] += 4;
#endif
            }

            if (SioFlags & MULTI_SIO_HARD_ERROR)    // Display communication error
                    StrTmp = "HARD ERROR    ";
            else if (SioFlags & MULTI_SIO_ID_OVER_ERROR)
                    StrTmp = "ID OVER ERROR ";
            else if (SioFlags & MULTI_SIO_RECV_FLAGS_AVAILABLE
                 && (((MultiSioReturn *)&SioFlags)->RecvSuccessFlags
                   ^ ((MultiSioReturn *)&SioFlags)->ConnectedFlags))
                    StrTmp = "RECV ERROR    ";

            else    StrTmp = "              ";
            BgScSet(StrTmp, &BgBak[32*18+7], 1);
        }

        MultiSioSendDataSet(SendBuf, 0);             // Multi-play communication send data set
    }
}


