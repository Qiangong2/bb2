/********************************************************************/
/*          load_table_dest.c                                       */
/*             Load destination table                               */
/*                                                                  */
/*          Copyright (C) 2001 NINTENDO Co.,Ltd.                    */
/********************************************************************/
#include "Sio32MultiLoad.h"


const u8 * const LoadTable[] = {
    (u8 *)0x02000000,
    (u8 *)0x02000000 + SIO32ML_BLOCK_SIZE * 1,
    (u8 *)0x02000000 + SIO32ML_BLOCK_SIZE * 2,
};


