/********************************************************************/
/*          MultiSio.h                                              */
/*            Multi-play communication library external declaration */
/*            (Timer interrupt transmission & unadjusted synchronization)*/
/*                                                                  */
/*          Copyright (C) 1999-2001 NINTENDO Co.,Ltd.               */
/********************************************************************/
#ifndef _MULTI_SIO_H
#define _MULTI_SIO_H

#ifdef __cplusplus
extern "C" {
#endif

#include <Agb.h>


// Optimize the following set values according to software specifications. 

#define MULTI_SIO_BLOCK_SIZE        16      // Communication data block size (max 24Byte)

#define MULTI_SIO_PLAYERS_MAX       4       // Maximum player number

#define MULTI_SIO_SYNC_DATA         0xfefe  // Synchronous data (0x0000/0xffff are prohibited.)

// Comment out if there is not enough space in CPU internal working RAM. 
#define MULTI_SIO_DI_FUNC_FAST              // SIO interrupt prohibit function high speed flag (CPU internal RAM executed.)


// Update it when the maximum delay of communication interrupt is larger than the following value. 
#define MULTI_SIO_INTR_DELAY_MAX    2000    // Communication interrupt acceptable delay clock number

#ifdef  MULTI_SIO_DI_FUNC_FAST
#define MULTI_SIO_INTR_CLOCK_MAX    400     // Communication interrupt process maximum clock number
#else
#define MULTI_SIO_INTR_CLOCK_MAX    1000
#endif

#define MULTI_SIO_1P_SEND_CLOCKS    3000    // Communication time per unit 

#if     MULTI_SIO_PLAYERS_MAX == 4
#define MULTI_SIO_START_BIT_WAIT    0       // Start bit wait time 
#else
#define MULTI_SIO_START_BIT_WAIT    512
#endif

// During the development, undefine NDEBUG, and set the following value to 0.  
// At the final check stage, define it, and verify the operation while a communication interrupt margin is changed to 600. 
// (Even when the set value is increased, the speed will not be low.  Only communication interval will be longer.) 
//#define NDEBUG                              // Definition by Makefile (MakefileDemo) is also possible. 
#ifdef  NDEBUG
#define MULTI_SIO_INTR_MARGIN       600     // Communication interrupt margin
#else
#define MULTI_SIO_INTR_MARGIN       0
#endif


#define MULTI_SIO_BAUD_RATE         115200          // Baud rate
#define MULTI_SIO_BAUD_RATE_NO      SIO_115200_BPS  // Baud rate No


#define MULTI_SIO_TIMER_NO          3       // Timer No
#define MULTI_SIO_TIMER_INTR_FLAG   (TIMER0_INTR_FLAG << MULTI_SIO_TIMER_NO)
                                            // Timer interrupt flag
#define REG_MULTI_SIO_TIMER         (REG_TM0CNT + MULTI_SIO_TIMER_NO * 4)
#define REG_MULTI_SIO_TIMER_L        REG_MULTI_SIO_TIMER
#define REG_MULTI_SIO_TIMER_H       (REG_MULTI_SIO_TIMER + 2)
                                            // Timer register

// Timer count number is calculated from the communication data block size. 
#define MULTI_SIO_TIMER_COUNT_TMP   (SYSTEM_CLOCK/60/((2 + 4 + MULTI_SIO_BLOCK_SIZE + 6)/(16/8)))
                                            // Tentative value for timer count
#define MULTI_SIO_TIMER_COUNT_MIN   ( MULTI_SIO_1P_SEND_CLOCKS * MULTI_SIO_PLAYERS_MAX    \
                                    + MULTI_SIO_START_BIT_WAIT + MULTI_SIO_INTR_MARGIN    \
                                    + MULTI_SIO_INTR_DELAY_MAX + MULTI_SIO_INTR_CLOCK_MAX)
                                            // Timer count minimum value
#define MULTI_SIO_TIMER_COUNT_MAX   0x10000 // Timer count maximum value
#define MULTI_SIO_TIMER_COUNT       (MULTI_SIO_TIMER_COUNT_MAX - MULTI_SIO_TIMER_COUNT_TMP)
                                            // Timer count number
// Timer count setup error
#if   MULTI_SIO_TIMER_COUNT_TMP < MULTI_SIO_TIMER_COUNT_MIN
    #error MULTI_SIO_TIMER_COUNT is too short,
    #error because MULTI_SIO_BLOCK_SIZE or MULTI_SIO_INTR_DELAY_MAX is too large.
#elif MULTI_SIO_TIMER_COUNT_TMP > MULTI_SIO_TIMER_COUNT_MAX
    #error MULTI_SIO_TIMER_COUNT is too long.
#endif


// Multi-play communication packet structure
typedef struct {
    u8  FrameCounter;                       // Frame counter
    u8  RecvErrorFlags;                     // Receive error flag
    u16 CheckSum;                           // Checksum
    u16 Data[MULTI_SIO_BLOCK_SIZE/2];       // Communication data
    u16 OverRunCatch[2];                    // Overrun protected area
} MultiSioPacket;


// Multi-play communication work area structure
typedef struct {
    u8  Type;                               // Connection (master/slave)
    u8  State;                              // Status communication function
    u8  ConnectedFlags;                     // Connection history flag
    u8  RecvSuccessFlags;                   // Receiving success flag

    u8  SyncRecvFlag[4];                    // Receiving verification flag

    u8  StartFlag;                          // Communication start flag

    u8  HardError;                          // Hard error

    u8  RecvFlagsAvailableCounter;          // Receiving success flag validation counter

    u8  SendFrameCounter;                   // Send frame counter
    u8  RecvFrameCounter[4][2];             // Receive frame counter

    s32 SendBufCounter;                     // Send buffer counter
    s32 RecvBufCounter[4];                  // Receive buffer counter

    u16 *NextSendBufp;                      // Send buffer pointer
    u16 *CurrentSendBufp;
    u16 *CurrentRecvBufp[4];                // Receive buffer pointer
    u16 *LastRecvBufp[4];
    u16 *RecvCheckBufp[4];

    MultiSioPacket  SendBuf[2];             // Send buffer (double buffer) 
    MultiSioPacket  RecvBuf[MULTI_SIO_PLAYERS_MAX][3];
                                            // Receive buffer (triple buffer)
} MultiSioArea;


extern u32 RecvFuncBuf[];                   // Buffer to execute CPU internal RAM
extern u32 IntrFuncBuf[];

extern MultiSioArea     Ms;                 // Multi-play communication work area


/*------------------------------------------------------------------*/
/*                      Initialize multi-play communication         */
/*------------------------------------------------------------------*/

extern void MultiSioInit(void);

// - Set serial communication mode as multi-play mode. 
// - Initialize register and buffer. 

/*------------------------------------------------------------------*/
/*                      Start multi-play communication              */
/*------------------------------------------------------------------*/

void MultiSioStart(void);

// - Set a flag to start sending if after master is recognized. 
// - Do nothing if slave, or before master is recognized. 

/*------------------------------------------------------------------*/
/*                     Stop multi-play communcation                 */
/*------------------------------------------------------------------*/

void MultiSioStop(void);

// - Stop communication. 

/*------------------------------------------------------------------*/
/*                      Multi-play communication main               */
/*------------------------------------------------------------------*/

extern u32  MultiSioMain(void *Sendp, void *Recvp);

// - First, distinguish master from slave.  If recognized as master, start communication. 
// - Call MultiSioSendDataSet(), and set send data.  
// - Call MultiSioRecvDataCheck(), and check if data is received normally, and 
//  copy receive data to Recvp.
//
// - Set it so that it is called in the closest timing in one frame. 
// - It is safe not to send data that matches with the flag data 
//  (SIO_SYNC_DATA) before connection relation is decided. 
//
// - Argument:
//    void *Sendp  User send buffer pointer 
//    void *Recvp  User receive buffer pointer 

// - Return value:

#define MULTI_SIO_RECV_ID_MASK          0x000f  // Receiving success flag
#define MULTI_SIO_CONNECTED_ID_MASK     0x0f00  // Connection history flag

#define MULTI_SIO_RECV_ID_SHIFT         0
#define MULTI_SIO_CONNECTED_ID_SHIFT    8

#define MULTI_SIO_RECV_ID0              0x0001  // Receiving success flag Master
#define MULTI_SIO_RECV_ID1              0x0002  // Slave 1
#define MULTI_SIO_RECV_ID2              0x0004  // Slave 2
#define MULTI_SIO_RECV_ID3              0x0008  // Slave 3
#define MULTI_SIO_TYPE                  0x0080  // Relation (Master/Slave)
#define MULTI_SIO_PARENT                0x0080  // Connect Master
#define MULTI_SIO_CHILD                 0x0000  // Connect Slave
#define MULTI_SIO_CONNECTED_ID0         0x0100  // Connection history flag Master
#define MULTI_SIO_CONNECTED_ID1         0x0200  // Slave 1
#define MULTI_SIO_CONNECTED_ID2         0x0400  // Slave 2
#define MULTI_SIO_CONNECTED_ID3         0x0800  // Slave 3
#define MULTI_SIO_HARD_ERROR            0x1000  // Hard error
#define MULTI_SIO_ID_OVER_ERROR         0x2000  // ID over error
#define MULTI_SIO_RECV_FLAGS_AVAILABLE  0x8000  // Validate receiving success flag


// Return value structure
typedef struct {
    u32 RecvSuccessFlags:4;                 // Receiving success flag 
    u32 Reserved_0:3;                       // Reserve
    u32 Type:1;                             // Relation (master/slave) 
    u32 ConnectedFlags:4;                   // Connection history flag
    u32 HardError:1;                        // Hare error
    u32 ID_OverError:1;                     // ID over error
    u32 Reserved:1;                         // Reservation
    u32 RecvFlagsAvailable:1;               // Validate receiving success flag
} MultiSioReturn;



/*------------------------------------------------------------------*/
/*                      Multi-play communication interrupt          */
/*------------------------------------------------------------------*/

extern void MultiSioIntr(void);

// - Store the data received from each unit during communication interrupt in 
//   each receive buffer, and set the data in send buffer to register.  
// - Store communication error flag also in the work area.
// - If master, reset timer and resume sending. 
//
// - Program it so that slave is called by a communication interrupt, and  
//  master is called by a timer interrupt.  
// - Adjust the setting value so that 1 packet (other than OverRunCatch[]) is 
//  certainly transferred in 1 frame.  


/*------------------------------------------------------------------*/
/*                      Set send data                               */
/*------------------------------------------------------------------*/

extern void MultiSioSendDataSet(void *Sendp);

// - Set the data in user send buffer to send buffer. 
//
// - Called from MultiSioMain().
// - It is not necessary to call directly.
//
// - Argument:
//    void *Sendp  User send buffer pointer 

/*------------------------------------------------------------------*/
/*                      Check received data                         */
/*------------------------------------------------------------------*/

extern u32  MultiSioRecvDataCheck(void *Recvp);

// - Check if data is received normally, and if so, copy the received data to 
//  user receive buffer.  
//
// - Called from MultiSioMain().
// - It is not necessary to call direcly. 
//
// - Argument:
//    void *Recvp  User receive buffer pointer 


#ifdef __cplusplus
}      /* extern "C" */
#endif

#endif /* _MULTI_SIO_H */
