---------------------------------------------------------------------
Nintendo GameCube Virtual Memory Library
Software Development Support Group - Nintendo of America, Inc.
November 18, 2003
Author: Steve Rabin - stevera@noa.nintendo.com
---------------------------------------------------------------------


This is the Virtual Memory library package. To install this package, 
unzip the files (with paths enabled) into the DolphinSDK1.0 directory 
(or equivalent). The file list is below.

IMPORTANT: The manual pages of the VM library are inside the folder:

      $DOLPHIN_ROOT/man/vm/index.html


================== RELEASE NOTES for Nov 18th ========================

FIX: VMStoreAllPages and VMStoreOnePage failed to clear the dirty bits 
     properly resulting in pages still be written back to ARAM after 
     calling these functions. There was also an issue with clearing the
     affected TLB entries within these functions. Both issues have been 
     repaired.

FIX: Interrupts now disabled during changes to the page table.


============================== FILE LIST ==============================

Files included:

build/demos/vmminimal/makefile
build/demos/vmminimal/src/main.c
build/demos/vmreldemo/makefile
build/demos/vmreldemo/include/a.h
build/demos/vmreldemo/include/b.h
build/demos/vmreldemo/src/a.cpp
build/demos/vmreldemo/src/b.cpp
build/demos/vmreldemo/src/static.c
build/demos/vmsample/makefile
build/demos/vmsample/include/vmstats.h
build/demos/vmsample/include/vmunittests.h
build/demos/vmsample/src/main.c
build/demos/vmsample/src/vmstats.c
build/demos/vmsample/src/vmunittests.c
build/libraries/vm/makefile
build/libraries/vm/include/VMMappingPrivate.h
build/libraries/vm/include/VMPageReplacementPrivate.h
build/libraries/vm/include/VMPrivate.h
build/libraries/vm/src/VM.c
build/libraries/vm/src/VMMapping.c
build/libraries/vm/src/VMPageReplacement.c
HW2/lib/vm.a
HW2/lib/vmD.a
HW2/lib/vmbase.a
HW2/lib/vmbaseD.a
include/dolphin/vm.h
include/dolphin/vmbase.h
include/dolphin/vm/VMMapping.h
include/dolphin/vm/VMPageReplacement.h
man/vm/index.html
man/vm/intro.html
man/vm/toc.html
man/vm/vm.gif
man/vm/vm/VMAlloc.html
man/vm/vm/VMBASEClearPageTableEntry.html
man/vm/vm/VMBASEGetPhysicalAddrInMRAM.html
man/vm/vm/VMBASEGetVirtualAddrFromPageInMRAM.html
man/vm/vm/VMBASEInit.html
man/vm/vm/VMBASEInvalidateAllPages.html
man/vm/vm/VMBASEIsPageDirty.html
man/vm/vm/VMBASEIsPageLocked.html
man/vm/vm/VMBASEIsPageReferenced.html
man/vm/vm/VMBASEIsPageValid.html
man/vm/vm/VMBASEQuit.html
man/vm/vm/VMBASESetPageDirty.html
man/vm/vm/VMBASESetPageLocked.html
man/vm/vm/VMBASESetPageReferenced.html
man/vm/vm/VMBASESetPageTableEntry.html
man/vm/vm/VMBASEStoreAllPages.html
man/vm/vm/VMFree.html
man/vm/vm/VMFreeAll.html
man/vm/vm/VMGetARAMBase.html
man/vm/vm/VMGetARAMSize.html
man/vm/vm/VMGetNumUnallocatedBytes.html
man/vm/vm/VMGetPageReplacementPolicy.html
man/vm/vm/VMInit.html
man/vm/vm/VMInvalidateAllPages.html
man/vm/vm/VMInvalidateRange.html
man/vm/vm/VMIsInitialized.html
man/vm/vm/VMQuit.html
man/vm/vm/VMResizeARAM.html
man/vm/vm/VMSetLogStatsCallback.html
man/vm/vm/VMSetPageReplacementPolicy.html
man/vm/vm/VMStoreAllPages.html
man/vm/vm/VMStoreOnePage.html





