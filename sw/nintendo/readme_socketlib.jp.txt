■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
■ニンテンドーゲームキューブSocket Library Package(β版）   ■
■2003/09/24 版                                             ■
■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■

◆目次
(1) はじめに
(2) 知的所有権
(3) 更新履歴
(4) ファイルリスト

===========================================================================
(1)はじめに
===========================================================================
これは、ニンテンドーゲームキューブ Socket Library Package(β版) です。この
パッケージを使用するには、

- GAMECUBE SDK 2003/05/08版+Patch2 以降
- Network Base Package 2003/08/05 版以降

が必要です。

このパッケージをインストールするためには、パッケージ内のファイルを SDK のイ
ンストールされているディレクトリに上書きコピーしてください。

マニュアルは、以下のフォルダに入っています。

      $DOLPHIN_ROOT/man.jp/
      $DOLPHIN_ROOT/man/   （英語）

注意:このソフトウェアはβバージョンであり、ツールのみにご使用ください。ゲー
ムソフト開発には使用されないでください。

===========================================================================
(2) 知的所有権
===========================================================================
このライブラリには、RSA Data Security, Inc.のMD5 Message-Digest Algorithmを
ベースにしたソフトウェアを利用しています。
詳細については、RFC 1321 "The MD5 Message-Digest Algorithm"を参照してくださ
い。

===========================================================================
(3) 更新履歴
===========================================================================
■2003/09/24

[SO]
・TCPでRFC 2018で規定されているSelective Acknowledgement(SACK)、およびRFC
　2581で使用が認められているForward Acknowledgment(FACK)に対応しました。
・TCP がパーシスト状態にはいってから SOConfig.r2 時間経過後に接続がタイム
　アウトしてしまう問題を修正しました。ゼロ ウィンドウ プローブに対して ACK
　が返ってくる限りコネクションは維持されるようになりました。
・DHCP サーバーからアドレスを取得してくる際の手順を更新しました。従来のバ
　ージョンでは IP アドレスやルーター アドレスなどを取得してくることができ
　なかった DHCP サーバー製品からでもオプションを取得できるようになっている
　場合があります。
・DHCP で取得したアドレスが別のネットワーク機器と衝突していた場合に、関数
　IPGetConfigError で IP_ERR_ADDR_COLLISION エラーを取得できなかった問題を
　修正しました。
・UDP で未使用のポートに対してパケットが送信されてきた場合に、ICMP
　Unreachable エラー メッセージを返送するようにしました。
・関数 SOAccept で返された接続済みソケットが関数 SOFcntl でリスニング ソケ
　ットに設定されている属性(SO_O_*)を継承するようになりました。
・関数 SOListen を呼び出したあと、バックグラウンドで TCP のコネクションを
　確立しているときにエラーが発生すると、あとで関数 SOCleanup を呼び出した
　ときに関数が終了しなかったり、ハングアップしてしまうバグを修正しました。
・UDP のマルチキャストに対応するようになりました。SO_IP_MULTICAST_LOOP,
　SO_IP_MULTICAST_TTL, SO_IP_ADD_MEMBERSHIP, SO_IP_DROP_MEMBERSHIP ソケッ
　ト オプションを使用することができます。
・SOConfig 構造体に rdhcp メンバが追加され、DHCP のリトライ回数を設定でき
　るようになりました。
・SOPoll() が TCP ソケットに対しても機能するようになりました。
・ネットワークインターフェイスレベルで集計している統計情報を取得したりリセ
　ットするための関数 IPGetInterfaceStat と IPClearInterfaceStat を追加しま
　した。送受信したパケット数のほか、コリジョンにより送信をアボートしたパケ
　ット数などを取得することができます。詳細についてはマニュアル ページを参
　照してください。

[DVDETH]
・FSTをゲームキューブのメモリ上に実装しました。
  そのため、メモリ上のFST領域を、プログラマ側で用意する必要があります。
・新規作成したDVDFstInit()でFSTをDVDServerから取得して初期化、
  DVDFstReflesh()でFSTを再取得します。これによって、
  これまでのDVDETH関数でスレッドが切り替わっていたDVDFastOpen()や、
　ディレクトリ関数などがDVD関数と同じく切り替わらなくなり、コールバック関数
　からの呼び出しもできるようになりました。
・dvd.h内のDVDDirEntry構造体はDVD関数と同じになりました。DVDRewindDir()がDVD
　関数とDVDETH関数両方で、マクロから関数に置き換えられました。それによって、
　dvd.h内からDVDETHの定義はなくなりました。
・DVDServerは、FSTの作成中は「FST is not ready!!」、作成完了時に
 「FST is ready」というメッセージを表示するようにしました。 FST作成中に
  DVDFstInit()を実行するとFATALエラーになります。 



■2003/08/08

[DVDETH]
・正常なファイルをDVDReadで読み込んだ際に、稀にDVD_RESULT_FATAL_ERRORが返る
　不具合を修正しました。

■2003/06/02

[DVDETH]
・DVDFastOpen()を呼んだ後に、DVDFileInfoのstartAddrに入っていた、ファイルエ
　ントリが不正な値に書き換わってしまうという問題があり、DVDServer.exeを修正
　しました。

■2003/03/25

[SO]
・SOWrite(), SOSend(), SOSendTo() で非封鎖モードの処理を正しく実行していな
　い問題を修正しました。

■2003/03/20

[SO]
・SOInit() が追加されました。ソケット ライブラリを使用するときに SOInit()
　を呼び出しておけば、SOStartup() を呼び出す前に IPGetLinkState() を呼び出
　して BBA のリンク状態を確認することができます。
・IPGetLinkState() が BBA の初期化直後、リンクアップする前にリンクアップ状
　態を結果として返す問題を修正しました。
・IPGetConfigError() で取得できるエラー コードに IP_ERR_ADDR_COLLISION と
　IP_ERR_LINK_DOWN が追加されました。ADDR_COLLISION はネットワーク中で IP
　アドレスの競合が検出された場合に発生します。LINK_DOWN はそれまでつながっ
　ていた BBA からネットワークへのリンクが切れた場合に発生します。
・接続先を SOConnect() で指定している UDP ソケットに対してSOSend() を呼び出
　すとクラッシュするバグを修正しました。
・SOPoll() が追加されました。なお、現在のバージョンでは SOPoll() は UDP ソ
　ケットに対してのみ機能します。TCP ソケットには将来のバージョンで対応しま
　す。
・UDP ソケットに対する SOSendTo(), SOSend(), SOWrite() の動作に変更がありま
　す。これらの関数はライブラリの確保したメモリ領域にデータをコピーして直ち
　に完了するようになりました。従来は指定したデータが BBA から実際に送信され
　るまで関数がブロックしていました。
・SO_SO_REUSEADDR および SO_SO_SNDBUF オプションを UDP ソケットに対しても使
　用できるようになりました。通常はデフォルトの設定から変更する必要はありま
　せん。
・md5.h, md5.c を Network Base Package に移しました。

[DVDETH]
・DVDETH ライブラリにディレクトリ関数を追加しました。
・OSReport 関数、ReportSever ツールのソースを追加しました。
・2003/1/9 版に含まれていたdvd_replace.h は削除されました。
　GAMECUBE SDK 2002/09/05版+Patch3 以降に含まれているdvd.h をお使いください。

■2003/1/9
・新しく DVDETH ライブラリを追加しました。
・TCPの TIME_WAIT 状態にあるコネクションを SOConnect() で再生してしまう問題
　がありました。この不具合は修正されました。

■2002/11/14
・リリース開始

===========================================================================
(4) ファイルリスト
===========================================================================
./HW2/lib/dvdeth.a
./HW2/lib/dvdethD.a
./HW2/lib/ip.a
./HW2/lib/ipD.a
./X86/bin/DVDServer.exe
./X86/bin/ReportServer.exe
./build/demos/dvdethdemo/include/bmp.h
./build/demos/dvdethdemo/include/ipaddress.h
./build/demos/dvdethdemo/include/osreport.h
./build/demos/dvdethdemo/include/selectfile2.h
./build/demos/dvdethdemo/makefile
./build/demos/dvdethdemo/src/bmp.c
./build/demos/dvdethdemo/src/directory.c
./build/demos/dvdethdemo/src/dvdethdemo1.c
./build/demos/dvdethdemo/src/dvdethdemo2.c
./build/demos/dvdethdemo/src/dvdethdemo3.c
./build/demos/dvdethdemo/src/dvdethdemo4.c
./build/demos/dvdethdemo/src/errorhandling2.c
./build/demos/dvdethdemo/src/osreport.c
./build/demos/dvdethdemo/src/osreportdemo1.c
./build/demos/dvdethdemo/src/selectfile2.c
./build/demos/dvdethdemo/src/viewer.c
./build/demos/sodemo/makefile
./build/demos/sodemo/src/dhcp.c
./build/demos/sodemo/src/host.c
./build/demos/sodemo/src/httpd.c
./build/demos/sodemo/src/httpdmain.c
./build/demos/sodemo/src/pppoe.c
./build/tools/ReportServer/src/ReportServer.c
./build/tools/ReportServer/vc++/ReportServer.dsp
./build/tools/ReportServer/vc++/ReportServer.dsw
./dvddata/www/index.html
./include/dolphin/dvdeth.h
./include/dolphin/ip.h
./include/dolphin/ip/IFEther.h
./include/dolphin/ip/IFFifo.h
./include/dolphin/ip/IFQueue.h
./include/dolphin/ip/IP.h
./include/dolphin/ip/IPDhcp.h
./include/dolphin/ip/IPDns.h
./include/dolphin/ip/IPIcmp.h
./include/dolphin/ip/IPPpp.h
./include/dolphin/ip/IPTcp.h
./include/dolphin/ip/IPUdp.h
./include/dolphin/ip/IPIgmp.h
./include/dolphin/ip/IPUuid.h
./include/dolphin/socket.h
./man.jp/dvdeth/a-z.html
./man.jp/dvdeth/contents.html
./man.jp/dvdeth/css/dolphin.css
./man.jp/dvdeth/css/titles.css
./man.jp/dvdeth/css/titles2.css
./man.jp/dvdeth/demos/dvdethdemos.html
./man.jp/dvdeth/icons.html
./man.jp/dvdeth/index.html
./man.jp/dvdeth/index2.html
./man.jp/dvdeth/intro.html
./man.jp/dvdeth/list/list_dvdeth.html
./man.jp/dvdeth/list/list_tools.html
./man.jp/dvdeth/main.html
./man.jp/dvdeth/prog/dvdeth.html
./man.jp/dvdeth/prog/dvdeth/DVDCreate.html
./man.jp/dvdeth/prog/dvdeth/DVDEthInit.html
./man.jp/dvdeth/prog/dvdeth/DVDEthShutdown.html
./man.jp/dvdeth/prog/dvdeth/DVDLowInit.html
./man.jp/dvdeth/prog/dvdeth/DVDLowReport.html
./man.jp/dvdeth/prog/dvdeth/DVDLowReportInit.html
./man.jp/dvdeth/prog/dvdeth/DVDRemove.html
./man.jp/dvdeth/prog/dvdeth/DVDWrite.html
./man.jp/dvdeth/prog/dvdeth/DVDWriteAsync.html
./man.jp/dvdeth/prog/dvdeth/OSReportInit.html
./man.jp/dvdeth/prog/dvdeth/DVDFstInit.html
./man.jp/dvdeth/prog/dvdeth/DVDFstRefresh.html
./man.jp/dvdeth/tools/dvdserver.html
./man.jp/dvdeth/tools/reportserver.html
./man.jp/dvdeth/tools/tools.html
./man.jp/so/if/IPClearConfigError.html
./man.jp/so/if/IPGetAddr.html
./man.jp/so/if/IPGetAlias.html
./man.jp/so/if/IPGetBroadcastAddr.html
./man.jp/so/if/IPGetConfigError.html
./man.jp/so/if/IPGetGateway.html
./man.jp/so/if/IPGetLinkState.html
./man.jp/so/if/IPGetMacAddr.html
./man.jp/so/if/IPGetMtu.html
./man.jp/so/if/IPGetNetmask.html
./man.jp/so/if/IPSetConfigError.html
./man.jp/so/if/IPClearInterfaceStat.html
./man.jp/so/if/IPGetInterfaceStat.html
./man.jp/so/if/IPInterfaceStat.html
./man.jp/so/index.html
./man.jp/so/intro.html
./man.jp/so/ip/IPAddRoute.html
./man.jp/so/ip/IPInitRoute.html
./man.jp/so/ip/IPRefreshRoute.html
./man.jp/so/ip/IPRemoveRoute.html
./man.jp/so/so/Error.html
./man.jp/so/so/SOAccept.html
./man.jp/so/so/SOBind.html
./man.jp/so/so/SOCleanup.html
./man.jp/so/so/SOClose.html
./man.jp/so/so/SOConfig.html
./man.jp/so/so/SOConnect.html
./man.jp/so/so/SOFcntl.html
./man.jp/so/so/SOGetHostByAddr.html
./man.jp/so/so/SOGetHostByName.html
./man.jp/so/so/SOGetHostID.html
./man.jp/so/so/SOGetPeerName.html
./man.jp/so/so/SOGetResolver.html
./man.jp/so/so/SOGetSockName.html
./man.jp/so/so/SOGetSockOpt.html
./man.jp/so/so/SOHostEnt.html
./man.jp/so/so/SOInAddr.html
./man.jp/so/so/SOInetAtoN.html
./man.jp/so/so/SOInetNtoA.html
./man.jp/so/so/SOInetNtoP.html
./man.jp/so/so/SOInetPtoN.html
./man.jp/so/so/SOInit.html
./man.jp/so/so/SOIpMreq.html
./man.jp/so/so/SOLinger.html
./man.jp/so/so/SOListen.html
./man.jp/so/so/SONtoHl.html
./man.jp/so/so/SOPoll.html
./man.jp/so/so/SORecv.html
./man.jp/so/so/SOSend.html
./man.jp/so/so/SOSetResolver.html
./man.jp/so/so/SOSetSockOpt.html
./man.jp/so/so/SOShutdown.html
./man.jp/so/so/SOSockAddr.html
./man.jp/so/so/SOSockAddrIn.html
./man.jp/so/so/SOSockAtMark.html
./man.jp/so/so/SOSocket.html
./man.jp/so/so/SOStartup.html
./man.jp/so/toc.html
./man/so/if/IPClearConfigError.html
./man/so/if/IPGetAddr.html
./man/so/if/IPGetAlias.html
./man/so/if/IPGetBroadcastAddr.html
./man/so/if/IPGetConfigError.html
./man/so/if/IPGetGateway.html
./man/so/if/IPGetLinkState.html
./man/so/if/IPGetMacAddr.html
./man/so/if/IPGetMtu.html
./man/so/if/IPGetNetmask.html
./man/so/if/IPSetConfigError.html
./man/so/if/IPClearInterfaceStat.html
./man/so/if/IPGetInterfaceStat.html
./man/so/if/IPInterfaceStat.html
./man/so/index.html
./man/so/intro.html
./man/so/ip/IPAddRoute.html
./man/so/ip/IPInitRoute.html
./man/so/ip/IPRefreshRoute.html
./man/so/ip/IPRemoveRoute.html
./man/so/so/Error.html
./man/so/so/SOAccept.html
./man/so/so/SOBind.html
./man/so/so/SOCleanup.html
./man/so/so/SOClose.html
./man/so/so/SOConfig.html
./man/so/so/SOConnect.html
./man/so/so/SOFcntl.html
./man/so/so/SOGetHostByAddr.html
./man/so/so/SOGetHostByName.html
./man/so/so/SOGetHostID.html
./man/so/so/SOGetPeerName.html
./man/so/so/SOGetResolver.html
./man/so/so/SOGetSockName.html
./man/so/so/SOGetSockOpt.html
./man/so/so/SOHostEnt.html
./man/so/so/SOInAddr.html
./man/so/so/SOInetAtoN.html
./man/so/so/SOInetNtoA.html
./man/so/so/SOInetNtoP.html
./man/so/so/SOInetPtoN.html
./man/so/so/SOInit.html
./man/so/so/SOIpMreq.html
./man/so/so/SOLinger.html
./man/so/so/SOListen.html
./man/so/so/SONtoHl.html
./man/so/so/SOPoll.html
./man/so/so/SORecv.html
./man/so/so/SOSend.html
./man/so/so/SOSetResolver.html
./man/so/so/SOSetSockOpt.html
./man/so/so/SOShutdown.html
./man/so/so/SOSockAddr.html
./man/so/so/SOSockAddrIn.html
./man/so/so/SOSockAtMark.html
./man/so/so/SOSocket.html
./man/so/so/SOStartup.html
./man/so/toc.html
./readme_socketlib.jp.txt

===========================================================================
ニンテンドーゲームキューブ に関する技術的なご質問は、NTSC-ONLINE の質問部屋
もしくは gamecube-sup@lic.nintendo.co.jp までお問い合わせください。
===========================================================================
