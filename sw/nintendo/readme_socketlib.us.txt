--------------------------------------------------------------
Nintendo GameCube Socket Library Package (Beta Version)
September 24, 2003
--------------------------------------------------------------

Table of Contents
(1) About This Package
(2) Intellectual Property
(3) Revision History
(4) File List

==================================================================
(1) About This Package
==================================================================
This is the Nintendo GameCube Socket Library Package (Beta Version). 
To use this version you need the following:

- Nintendo GameCube SDK version 8-May-2003 + Patch2 or later
- Network Base Package version 5-Aug-2003 or later


To install this package, copy the files of this package to the directory 
where the SDK is installed, overwriting what is there.

The manual is under the following folders:

      $DOLPHIN_ROOT/man/
      $DOLPHIN_ROOT/man.jp/ (Japanese)

Note: This software is a beta version and should not be used for 
development of game software.

==================================================================
(2) Intellectual Property
==================================================================
The Nintendo GameCube Socket Library contains software derived from the 
RSA Data Security, Inc. MD5 Message-Digest Algorithm. Please refer to 
RFC 1321 "The MD5 Message-Digest Algorithm" for more detail.

==================================================================
(3) Revision History
==================================================================
09/24/2003

[SO]
- Ensured compatibility with Selective Acknowledgement (SACK) as prescribed 
  by RFC 2018 for TCP and Forward Acknowledgement (FACK), the use of which was 
  recognized in RFC 2581.
- Corrected a problem where a connection timeout occurs after the SOConfig.r2 
  time interval when TCP goes into persist state. For Zero Window Probe, as 
  long as ACK is returned, the connection is maintained.
- Revised the method for obtaining an address from a DHCP server. There are 
  now cases where past versions of DHCP server products that were unable to 
  obtain address and router addresses will be able to obtain options.
- Corrected a problem where in the event of a collision between the address 
  obtained by DHCP and another network device, the function IPGetConfigError 
  was unable to get an IP_ERR_ADDR_COLLISION error.
- In the event a packet is sent to an unused UDP port, an �ICMP Unreachable� 
  error message will now be returned.
- The connected socket returned by the SOAccept function will now inherit 
  the attribute SO_O_*  set by the listening socket of the SOFcntl function.
- Fixed a bug where, after calling the function SOListen and an error occurs 
  in the background while establishing a TCP connection and later when calling 
  the SOCleanup function, the function won�t shut down or a hangup occurs.
- Added support for UDP Multicast.  The socket options SO_IP_MULTICAST_LOOP, 
  SO_IP_MULTICAST_TTL, SO_IP_ADD_MEMBERSHIP, and SO_IP_DROP_MEMBERSHIP can now 
  be used.
- rdhcp has been added to the SOConfig structure and the number of retries 
  for DHCP can now be set.
- Added functionality for SOPoll() in the TCP socket.
- Added the functions IPGetInterfaceStat and IPClearInterfaceStat, which 
  reset and add up statistical information on a network interface level. The 
  number of sent/received packets as well as the number of packets aborted due 
  to collisions can be obtained.  Refer to the manual page for further details.

[DVDETH]
- Implemented FST into the Nintendo GameCube memory. Because of this implementation, 
  the FST region of the memory must be prepared by the programmer. The newly-created 
  DVDFstInit() obtains and initializes FST from DVDServer, then DVDFstRefresh() 
  re-obtains FST. Due to this implementation, DVDFastOpen(), in which the thread 
  was switched by DVDETH functions, and directory functions can no longer be 
  switched in the manner of DVD functions, and can be called from callback functions.
- Made the DVDDirEntry structure in dvd.h the same as the DVD function.  
  DVDRewindDir() has been changed from a macro to a function in both the DVD 
  and DVDETH functions.  Due to this replacement, the definition of DVDETH 
  has been removed from dvd.h. 
- DVDServer will now display the messages �FST is not ready!!� when creating FST, 
  and �FST is ready� when FST is complete. Calling DVDFstInit() while creating FST 
  will result in a fatal error.

08/08/2003

[DVDETH]
- Correct a problem that occasionally returned a DVD_RESULT_FATAL_ERROR 
  to be returned when a normal file was read with DVDRead.

06/02/2003

[DVDETH]
- Revised DVDServer.exe to correct the problem of the value in 
  startAddr in DVDFileInfo being changed to an invalid value after 
  DVDFastOpen() was called.

03/25/2003

[SO]
- Corrected the problem of process in Non-Blocking mode not executing 
  properly with SOWrite(), SOSend(), and SOSendTo().

03/20/2003

[SO]
- Added the function SOInit().  When using the Socket Library, if you 
  set a call to this function you can confirm the link state of the 
  Broadband Adapter by calling IPGetLinkState() before calling SOStartup().
- Fixed the problem that caused IPGetLinkState() to return "linked up" 
  as the state of the Broadband Adapter immediately after the device was 
  initialized and before it was linked up.
- Added IP_ERR_ADDR_COLLISION and IP_ERR_LINK_DOWN to the error codes 
  that IPGetConfigError() can get.  The ADDR_COLLISION error is generated 
  when an IP address collision is detected in the network.  The LINK_DOWN 
  error is generated when the existing link from the Broadband Adapter to 
  the network gets cut.
- Fixed the bug that caused the system to crash when SOSend() was called 
  for the UDP socket specified by the other party with SOConnect().
- Added the SOPoll() function.  In the present version, SOPoll() only 
  works with the UDP socket.   In a future version, the function will also 
  work with the TCP socket.
- Changed the way the SOSendTo(), SOSend() and SOWrite() functions operate 
  with regards to the UDP socket.  These functions now copy data to the 
  memory region secured for the library and then immediately complete, 
  whereas before they were blocked until the specified data was actually 
  being sent from the Broadband Adapter.
- Enabled use of both the SO_SO_REUSEADDR and the SO_SO_SNDBUF options 
  with the UDP socket.  It is not necessary to change the default settings.
- Moved md5.h and md5.c to the Network Base Package.

[DVDETH]
- Added directory functions to the DVDETH library.
- Added source for the OSReport function and the ReportSever tool.
- Deleted dvd_replace.h, which had been included in version 2003/1/9.  
  Instead, please use dvd.h, which is included in GAMECUBE SDK version 
  2002/09/05 + Patch3 and later.

01/09/2003
- Added the new DVDETH library.
- Fixed the problem that caused SOConnect() to refresh a connection when 
  it was in the TCP TIME_WAIT state.

11/14/2002
- First release

==================================================================

(4) File List
==================================================================
./HW2/lib/dvdeth.a
./HW2/lib/dvdethD.a
./HW2/lib/ip.a
./HW2/lib/ipD.a
./X86/bin/DVDServer.exe
./X86/bin/ReportServer.exe
./build/demos/dvdethdemo/include/bmp.h
./build/demos/dvdethdemo/include/ipaddress.h
./build/demos/dvdethdemo/include/osreport.h
./build/demos/dvdethdemo/include/selectfile2.h
./build/demos/dvdethdemo/makefile
./build/demos/dvdethdemo/src/bmp.c
./build/demos/dvdethdemo/src/directory.c
./build/demos/dvdethdemo/src/dvdethdemo1.c
./build/demos/dvdethdemo/src/dvdethdemo2.c
./build/demos/dvdethdemo/src/dvdethdemo3.c
./build/demos/dvdethdemo/src/dvdethdemo4.c
./build/demos/dvdethdemo/src/errorhandling2.c
./build/demos/dvdethdemo/src/osreport.c
./build/demos/dvdethdemo/src/osreportdemo1.c
./build/demos/dvdethdemo/src/selectfile2.c
./build/demos/dvdethdemo/src/viewer.c
./build/demos/sodemo/makefile
./build/demos/sodemo/src/dhcp.c
./build/demos/sodemo/src/host.c
./build/demos/sodemo/src/httpd.c
./build/demos/sodemo/src/httpdmain.c
./build/demos/sodemo/src/pppoe.c
./build/tools/ReportServer/src/ReportServer.c
./build/tools/ReportServer/vc++/ReportServer.dsp
./build/tools/ReportServer/vc++/ReportServer.dsw
./dvddata/www/index.html
./include/dolphin/dvdeth.h
./include/dolphin/ip.h
./include/dolphin/ip/IFEther.h
./include/dolphin/ip/IFFifo.h
./include/dolphin/ip/IFQueue.h
./include/dolphin/ip/IP.h
./include/dolphin/ip/IPDhcp.h
./include/dolphin/ip/IPDns.h
./include/dolphin/ip/IPIcmp.h
./include/dolphin/ip/IPPpp.h
./include/dolphin/ip/IPTcp.h
./include/dolphin/ip/IPUdp.h
./include/dolphin/ip/IPIgmp.h
./include/dolphin/ip/IPUuid.h
./include/dolphin/socket.h
./man.jp/dvdeth/a-z.html
./man.jp/dvdeth/contents.html
./man.jp/dvdeth/css/dolphin.css
./man.jp/dvdeth/css/titles.css
./man.jp/dvdeth/css/titles2.css
./man.jp/dvdeth/demos/dvdethdemos.html
./man.jp/dvdeth/icons.html
./man.jp/dvdeth/index.html
./man.jp/dvdeth/index2.html
./man.jp/dvdeth/intro.html
./man.jp/dvdeth/list/list_dvdeth.html
./man.jp/dvdeth/list/list_tools.html
./man.jp/dvdeth/main.html
./man.jp/dvdeth/prog/dvdeth.html
./man.jp/dvdeth/prog/dvdeth/DVDCreate.html
./man.jp/dvdeth/prog/dvdeth/DVDEthInit.html
./man.jp/dvdeth/prog/dvdeth/DVDEthShutdown.html
./man.jp/dvdeth/prog/dvdeth/DVDFstInit.html
./man.jp/dvdeth/prog/dvdeth/DVDFstRefresh.html
./man.jp/dvdeth/prog/dvdeth/DVDLowInit.html
./man.jp/dvdeth/prog/dvdeth/DVDLowReport.html
./man.jp/dvdeth/prog/dvdeth/DVDLowReportInit.html
./man.jp/dvdeth/prog/dvdeth/DVDRemove.html
./man.jp/dvdeth/prog/dvdeth/DVDWrite.html
./man.jp/dvdeth/prog/dvdeth/DVDWriteAsync.html
./man.jp/dvdeth/prog/dvdeth/OSReportInit.html
./man.jp/dvdeth/prog/dvdeth/DVDFstInit.html
./man.jp/dvdeth/prog/dvdeth/DVDFstRefresh.html
./man.jp/dvdeth/tools/dvdserver.html
./man.jp/dvdeth/tools/reportserver.html
./man.jp/dvdeth/tools/tools.html
./man.jp/so/if/IPClearConfigError.html
./man.jp/so/if/IPGetAddr.html
./man.jp/so/if/IPGetAlias.html
./man.jp/so/if/IPGetBroadcastAddr.html
./man.jp/so/if/IPGetConfigError.html
./man.jp/so/if/IPGetGateway.html
./man.jp/so/if/IPGetInterfaceStat.html
./man.jp/so/if/IPGetLinkState.html
./man.jp/so/if/IPGetMacAddr.html
./man.jp/so/if/IPGetMtu.html
./man.jp/so/if/IPGetNetmask.html
./man.jp/so/if/IPSetConfigError.html
./man.jp/so/if/IPClearInterfaceStat.html
./man.jp/so/if/IPGetInterfaceStat.html
./man.jp/so/if/IPInterfaceStat.html
./man.jp/so/index.html
./man.jp/so/intro.html
./man.jp/so/ip/IPAddRoute.html
./man.jp/so/ip/IPInitRoute.html
./man.jp/so/ip/IPRefreshRoute.html
./man.jp/so/ip/IPRemoveRoute.html
./man.jp/so/so/Error.html
./man.jp/so/so/SOAccept.html
./man.jp/so/so/SOBind.html
./man.jp/so/so/SOCleanup.html
./man.jp/so/so/SOClose.html
./man.jp/so/so/SOConfig.html
./man.jp/so/so/SOConnect.html
./man.jp/so/so/SOFcntl.html
./man.jp/so/so/SOGetHostByAddr.html
./man.jp/so/so/SOGetHostByName.html
./man.jp/so/so/SOGetHostID.html
./man.jp/so/so/SOGetPeerName.html
./man.jp/so/so/SOGetResolver.html
./man.jp/so/so/SOGetSockName.html
./man.jp/so/so/SOGetSockOpt.html
./man.jp/so/so/SOHostEnt.html
./man.jp/so/so/SOInAddr.html
./man.jp/so/so/SOInetAtoN.html
./man.jp/so/so/SOInetNtoA.html
./man.jp/so/so/SOInetNtoP.html
./man.jp/so/so/SOInetPtoN.html
./man.jp/so/so/SOInit.html
./man.jp/so/so/SOIpMreq.html
./man.jp/so/so/SOLinger.html
./man.jp/so/so/SOListen.html
./man.jp/so/so/SONtoHl.html
./man.jp/so/so/SOPoll.html
./man.jp/so/so/SORecv.html
./man.jp/so/so/SOSend.html
./man.jp/so/so/SOSetResolver.html
./man.jp/so/so/SOSetSockOpt.html
./man.jp/so/so/SOShutdown.html
./man.jp/so/so/SOSockAddr.html
./man.jp/so/so/SOSockAddrIn.html
./man.jp/so/so/SOSockAtMark.html
./man.jp/so/so/SOSocket.html
./man.jp/so/so/SOStartup.html
./man.jp/so/toc.html
./man/dvdeth/a-z.html
./man/dvdeth/contents.html
./man/dvdeth/css/dolphin.css
./man/dvdeth/css/titles.css
./man/dvdeth/css/titles2.css
./man/dvdeth/demos/dvdethdemos.html
./man/dvdeth/icons.html
./man/dvdeth/index.html
./man/dvdeth/index2.html
./man/dvdeth/intro.html
./man/dvdeth/list/list_dvdeth.html
./man/dvdeth/list/list_tools.html
./man/dvdeth/main.html
./man/dvdeth/prog/dvdeth.html
./man/dvdeth/prog/dvdeth/DVDCreate.html
./man/dvdeth/prog/dvdeth/DVDEthInit.html
./man/dvdeth/prog/dvdeth/DVDEthShutdown.html
./man/dvdeth/prog/dvdeth/DVDFstInit.html
./man/dvdeth/prog/dvdeth/DVDFstRefresh.html
./man/dvdeth/prog/dvdeth/DVDLowInit.html
./man/dvdeth/prog/dvdeth/DVDLowReport.html
./man/dvdeth/prog/dvdeth/DVDLowReportInit.html
./man/dvdeth/prog/dvdeth/DVDRemove.html
./man/dvdeth/prog/dvdeth/DVDWrite.html
./man/dvdeth/prog/dvdeth/DVDWriteAsync.html
./man/dvdeth/prog/dvdeth/OSReportInit.html
./man/dvdeth/prog/dvdeth/DVDFstInit.html
./man/dvdeth/prog/dvdeth/DVDFstRefresh.html
./man/dvdeth/tools/dvdserver.html
./man/dvdeth/tools/reportserver.html
./man/dvdeth/tools/tools.html
./man/so/if/IPClearConfigError.html
./man/so/if/IPGetAddr.html
./man/so/if/IPGetAlias.html
./man/so/if/IPGetBroadcastAddr.html
./man/so/if/IPGetConfigError.html
./man/so/if/IPGetGateway.html
./man/so/if/IPGetLinkState.html
./man/so/if/IPGetMacAddr.html
./man/so/if/IPGetMtu.html
./man/so/if/IPGetNetmask.html
./man/so/if/IPSetConfigError.html
./man/so/index.html
./man/so/intro.html
./man/so/ip/IPAddRoute.html
./man/so/ip/IPInitRoute.html
./man/so/ip/IPRefreshRoute.html
./man/so/ip/IPRemoveRoute.html
./man/so/so/Error.html
./man/so/so/SOAccept.html
./man/so/so/SOBind.html
./man/so/so/SOCleanup.html
./man/so/so/SOClose.html
./man/so/so/SOConfig.html
./man/so/so/SOConnect.html
./man/so/so/SOFcntl.html
./man/so/so/SOGetHostByAddr.html
./man/so/so/SOGetHostByName.html
./man/so/so/SOGetHostID.html
./man/so/so/SOGetPeerName.html
./man/so/so/SOGetResolver.html
./man/so/so/SOGetSockName.html
./man/so/so/SOGetSockOpt.html
./man/so/so/SOHostEnt.html
./man/so/so/SOInAddr.html
./man/so/so/SOInetAtoN.html
./man/so/so/SOInetNtoA.html
./man/so/so/SOInetNtoP.html
./man/so/so/SOInetPtoN.html
./man/so/so/SOInit.html
./man/so/so/SOLinger.html
./man/so/so/SOListen.html
./man/so/so/SONtoHl.html
./man/so/so/SOPoll.html
./man/so/so/SORecv.html
./man/so/so/SOSend.html
./man/so/so/SOSetResolver.html
./man/so/so/SOSetSockOpt.html
./man/so/so/SOShutdown.html
./man/so/so/SOSockAddr.html
./man/so/so/SOSockAddrIn.html
./man/so/so/SOSockAtMark.html
./man/so/so/SOSocket.html
./man/so/so/SOStartup.html
./man/so/toc.html
./readme_socketlib.jp.txt

==================================================================
If you have any technical questions concerning the Socket Library, 
post them on the SDSG news groups (news.sdsg.nintendo.com) or 
contact us directly at support@noa.com. 
==================================================================
