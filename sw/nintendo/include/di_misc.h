/*
 * This header file was synthesized from the "other DI defines.txt" file
 * supplied by Nintendo.  It is apparently a grab bag of definitions
 * gathered from several headers the full contents of which they do
 * not wish to reveal to BroadOn.
 *------------------------------------------------------------------------------------
 * from flipper.h
 *------------------------------------------------------------------------------------
 */
vu32 __DIRegs [DI_REG_UNUSED_15  + 1]   : OS_BASE_UNCACHED + IO_BASE_DI;
/*
 *------------------------------------------------------------------------------------
 * from io_reg.h
 *------------------------------------------------------------------------------------
 */
/*
 *  di_reg enum
 */
#define DI_SR_IDX	0x00000000
#define DI_CVR_IDX	0x00000001
#define DI_CMDBUF0_IDX	0x00000002
#define DI_CMDBUF1_IDX	0x00000003
#define DI_CMDBUF2_IDX	0x00000004
#define DI_MAR_IDX	0x00000005
#define DI_LENGTH_IDX	0x00000006
#define DI_CR_IDX	0x00000007
#define DI_IMMBUF_IDX	0x00000008
#define DI_CONFIG_IDX	0x00000009
#define DI_REG_UNUSED_10	0x0000000a
#define DI_REG_UNUSED_11	0x0000000b
#define DI_REG_UNUSED_12	0x0000000c
#define DI_REG_UNUSED_13	0x0000000d
#define DI_REG_UNUSED_14	0x0000000e
#define DI_REG_UNUSED_15	0x0000000f

/*
 *  di_brk enum
 */
#define DI_BRK_DONE	0x00000000
#define DI_BRK_REQUEST	0x00000001

/*
 *  di_sr struct
 */
#define DI_SR_BRK_SIZE	1
#define DI_SR_BRK_SHIFT	0
#define DI_SR_BRK_MASK	0x00000001
#define DI_SR_GET_BRK(di_sr) \
	((((unsigned long)(di_sr)) & DI_SR_BRK_MASK) >> DI_SR_BRK_SHIFT)
#define DI_SR_SET_BRK(di_sr, brk) { \
	FDL_ASSERT(!((brk) & ~((1 << DI_SR_BRK_SIZE)-1))); \
	di_sr = (((unsigned long)(di_sr)) & ~DI_SR_BRK_MASK) | (((unsigned long)(brk)) << DI_SR_BRK_SHIFT);\
}
#define DI_SR_DEINTMSK_SIZE	1
#define DI_SR_DEINTMSK_SHIFT	1
#define DI_SR_DEINTMSK_MASK	0x00000002
#define DI_SR_GET_DEINTMSK(di_sr) \
	((((unsigned long)(di_sr)) & DI_SR_DEINTMSK_MASK) >> DI_SR_DEINTMSK_SHIFT)
#define DI_SR_SET_DEINTMSK(di_sr, deintmsk) { \
	FDL_ASSERT(!((deintmsk) & ~((1 << DI_SR_DEINTMSK_SIZE)-1))); \
	di_sr = (((unsigned long)(di_sr)) & ~DI_SR_DEINTMSK_MASK) | (((unsigned long)(deintmsk)) << DI_SR_DEINTMSK_SHIFT);\
}
#define DI_SR_DEINT_SIZE	1
#define DI_SR_DEINT_SHIFT	2
#define DI_SR_DEINT_MASK	0x00000004
#define DI_SR_GET_DEINT(di_sr) \
	((((unsigned long)(di_sr)) & DI_SR_DEINT_MASK) >> DI_SR_DEINT_SHIFT)
#define DI_SR_SET_DEINT(di_sr, deint) { \
	FDL_ASSERT(!((deint) & ~((1 << DI_SR_DEINT_SIZE)-1))); \
	di_sr = (((unsigned long)(di_sr)) & ~DI_SR_DEINT_MASK) | (((unsigned long)(deint)) << DI_SR_DEINT_SHIFT);\
}
#define DI_SR_TCINTMSK_SIZE	1
#define DI_SR_TCINTMSK_SHIFT	3
#define DI_SR_TCINTMSK_MASK	0x00000008
#define DI_SR_GET_TCINTMSK(di_sr) \
	((((unsigned long)(di_sr)) & DI_SR_TCINTMSK_MASK) >> DI_SR_TCINTMSK_SHIFT)
#define DI_SR_SET_TCINTMSK(di_sr, tcintmsk) { \
	FDL_ASSERT(!((tcintmsk) & ~((1 << DI_SR_TCINTMSK_SIZE)-1))); \
	di_sr = (((unsigned long)(di_sr)) & ~DI_SR_TCINTMSK_MASK) | (((unsigned long)(tcintmsk)) << DI_SR_TCINTMSK_SHIFT);\
}
#define DI_SR_TCINT_SIZE	1
#define DI_SR_TCINT_SHIFT	4
#define DI_SR_TCINT_MASK	0x00000010
#define DI_SR_GET_TCINT(di_sr) \
	((((unsigned long)(di_sr)) & DI_SR_TCINT_MASK) >> DI_SR_TCINT_SHIFT)
#define DI_SR_SET_TCINT(di_sr, tcint) { \
	FDL_ASSERT(!((tcint) & ~((1 << DI_SR_TCINT_SIZE)-1))); \
	di_sr = (((unsigned long)(di_sr)) & ~DI_SR_TCINT_MASK) | (((unsigned long)(tcint)) << DI_SR_TCINT_SHIFT);\
}
#define DI_SR_BRKINTMSK_SIZE	1
#define DI_SR_BRKINTMSK_SHIFT	5
#define DI_SR_BRKINTMSK_MASK	0x00000020
#define DI_SR_GET_BRKINTMSK(di_sr) \
	((((unsigned long)(di_sr)) & DI_SR_BRKINTMSK_MASK) >> DI_SR_BRKINTMSK_SHIFT)
#define DI_SR_SET_BRKINTMSK(di_sr, brkintmsk) { \
	FDL_ASSERT(!((brkintmsk) & ~((1 << DI_SR_BRKINTMSK_SIZE)-1))); \
	di_sr = (((unsigned long)(di_sr)) & ~DI_SR_BRKINTMSK_MASK) | (((unsigned long)(brkintmsk)) << DI_SR_BRKINTMSK_SHIFT);\
}
#define DI_SR_BRKINT_SIZE	1
#define DI_SR_BRKINT_SHIFT	6
#define DI_SR_BRKINT_MASK	0x00000040
#define DI_SR_GET_BRKINT(di_sr) \
	((((unsigned long)(di_sr)) & DI_SR_BRKINT_MASK) >> DI_SR_BRKINT_SHIFT)
#define DI_SR_SET_BRKINT(di_sr, brkint) { \
	FDL_ASSERT(!((brkint) & ~((1 << DI_SR_BRKINT_SIZE)-1))); \
	di_sr = (((unsigned long)(di_sr)) & ~DI_SR_BRKINT_MASK) | (((unsigned long)(brkint)) << DI_SR_BRKINT_SHIFT);\
}
#define DI_SR_TOTAL_SIZE	7
#define DI_SR_UNUSED_SIZE	25

#define DI_SR(brk, deintmsk, deint, tcintmsk, tcint, brkintmsk, brkint) \
	((((unsigned long)(brk)) << DI_SR_BRK_SHIFT) | \
	 (((unsigned long)(deintmsk)) << DI_SR_DEINTMSK_SHIFT) | \
	 (((unsigned long)(deint)) << DI_SR_DEINT_SHIFT) | \
	 (((unsigned long)(tcintmsk)) << DI_SR_TCINTMSK_SHIFT) | \
	 (((unsigned long)(tcint)) << DI_SR_TCINT_SHIFT) | \
	 (((unsigned long)(brkintmsk)) << DI_SR_BRKINTMSK_SHIFT) | \
	 (((unsigned long)(brkint)) << DI_SR_BRKINT_SHIFT))

typedef struct {
    unsigned long unused:DI_SR_UNUSED_SIZE;
    unsigned long brkint:DI_SR_BRKINT_SIZE;
    unsigned long brkintmsk:DI_SR_BRKINTMSK_SIZE;
    unsigned long tcint:DI_SR_TCINT_SIZE;
    unsigned long tcintmsk:DI_SR_TCINTMSK_SIZE;
    unsigned long deint:DI_SR_DEINT_SIZE;
    unsigned long deintmsk:DI_SR_DEINTMSK_SIZE;
    unsigned long brk:DI_SR_BRK_SIZE;
} di_sr_t;

typedef union {
    unsigned long val;
    di_sr_t f;
} di_sr_u;

/*
 *  di_cvr enum
 */
#define DI_CVR_CLOSED	0x00000000
#define DI_CVR_OPEN	0x00000001

/*
 *  di_cvr struct
 */
#define DI_CVR_CVR_SIZE	1
#define DI_CVR_CVR_SHIFT	0
#define DI_CVR_CVR_MASK	0x00000001
#define DI_CVR_GET_CVR(di_cvr) \
	((((unsigned long)(di_cvr)) & DI_CVR_CVR_MASK) >> DI_CVR_CVR_SHIFT)
#define DI_CVR_SET_CVR(di_cvr, cvr) { \
	FDL_ASSERT(!((cvr) & ~((1 << DI_CVR_CVR_SIZE)-1))); \
	di_cvr = (((unsigned long)(di_cvr)) & ~DI_CVR_CVR_MASK) | (((unsigned long)(cvr)) << DI_CVR_CVR_SHIFT);\
}
#define DI_CVR_CVRINTMSK_SIZE	1
#define DI_CVR_CVRINTMSK_SHIFT	1
#define DI_CVR_CVRINTMSK_MASK	0x00000002
#define DI_CVR_GET_CVRINTMSK(di_cvr) \
	((((unsigned long)(di_cvr)) & DI_CVR_CVRINTMSK_MASK) >> DI_CVR_CVRINTMSK_SHIFT)
#define DI_CVR_SET_CVRINTMSK(di_cvr, cvrintmsk) { \
	FDL_ASSERT(!((cvrintmsk) & ~((1 << DI_CVR_CVRINTMSK_SIZE)-1))); \
	di_cvr = (((unsigned long)(di_cvr)) & ~DI_CVR_CVRINTMSK_MASK) | (((unsigned long)(cvrintmsk)) << DI_CVR_CVRINTMSK_SHIFT);\
}
#define DI_CVR_CVRINT_SIZE	1
#define DI_CVR_CVRINT_SHIFT	2
#define DI_CVR_CVRINT_MASK	0x00000004
#define DI_CVR_GET_CVRINT(di_cvr) \
	((((unsigned long)(di_cvr)) & DI_CVR_CVRINT_MASK) >> DI_CVR_CVRINT_SHIFT)
#define DI_CVR_SET_CVRINT(di_cvr, cvrint) { \
	FDL_ASSERT(!((cvrint) & ~((1 << DI_CVR_CVRINT_SIZE)-1))); \
	di_cvr = (((unsigned long)(di_cvr)) & ~DI_CVR_CVRINT_MASK) | (((unsigned long)(cvrint)) << DI_CVR_CVRINT_SHIFT);\
}
#define DI_CVR_TOTAL_SIZE	3
#define DI_CVR_UNUSED_SIZE	29

#define DI_CVR(cvr, cvrintmsk, cvrint) \
	((((unsigned long)(cvr)) << DI_CVR_CVR_SHIFT) | \
	 (((unsigned long)(cvrintmsk)) << DI_CVR_CVRINTMSK_SHIFT) | \
	 (((unsigned long)(cvrint)) << DI_CVR_CVRINT_SHIFT))

typedef struct {
    unsigned long unused:DI_CVR_UNUSED_SIZE;
    unsigned long cvrint:DI_CVR_CVRINT_SIZE;
    unsigned long cvrintmsk:DI_CVR_CVRINTMSK_SIZE;
    unsigned long cvr:DI_CVR_CVR_SIZE;
} di_cvr_t;

typedef union {
    unsigned long val;
    di_cvr_t f;
} di_cvr_u;

/*
 *  di_cmdbuf0 struct
 */
#define DI_CMDBUF0_CMDBYTE0_SIZE	8
#define DI_CMDBUF0_CMDBYTE0_SHIFT	0
#define DI_CMDBUF0_CMDBYTE0_MASK	0x000000ff
#define DI_CMDBUF0_GET_CMDBYTE0(di_cmdbuf0) \
	((((unsigned long)(di_cmdbuf0)) & DI_CMDBUF0_CMDBYTE0_MASK) >> DI_CMDBUF0_CMDBYTE0_SHIFT)
#define DI_CMDBUF0_SET_CMDBYTE0(di_cmdbuf0, cmdbyte0) { \
	FDL_ASSERT(!((cmdbyte0) & ~((1 << DI_CMDBUF0_CMDBYTE0_SIZE)-1))); \
	di_cmdbuf0 = (((unsigned long)(di_cmdbuf0)) & ~DI_CMDBUF0_CMDBYTE0_MASK) | (((unsigned long)(cmdbyte0)) << DI_CMDBUF0_CMDBYTE0_SHIFT);\
}
#define DI_CMDBUF0_CMDBYTE1_SIZE	8
#define DI_CMDBUF0_CMDBYTE1_SHIFT	8
#define DI_CMDBUF0_CMDBYTE1_MASK	0x0000ff00
#define DI_CMDBUF0_GET_CMDBYTE1(di_cmdbuf0) \
	((((unsigned long)(di_cmdbuf0)) & DI_CMDBUF0_CMDBYTE1_MASK) >> DI_CMDBUF0_CMDBYTE1_SHIFT)
#define DI_CMDBUF0_SET_CMDBYTE1(di_cmdbuf0, cmdbyte1) { \
	FDL_ASSERT(!((cmdbyte1) & ~((1 << DI_CMDBUF0_CMDBYTE1_SIZE)-1))); \
	di_cmdbuf0 = (((unsigned long)(di_cmdbuf0)) & ~DI_CMDBUF0_CMDBYTE1_MASK) | (((unsigned long)(cmdbyte1)) << DI_CMDBUF0_CMDBYTE1_SHIFT);\
}
#define DI_CMDBUF0_CMDBYTE2_SIZE	8
#define DI_CMDBUF0_CMDBYTE2_SHIFT	16
#define DI_CMDBUF0_CMDBYTE2_MASK	0x00ff0000
#define DI_CMDBUF0_GET_CMDBYTE2(di_cmdbuf0) \
	((((unsigned long)(di_cmdbuf0)) & DI_CMDBUF0_CMDBYTE2_MASK) >> DI_CMDBUF0_CMDBYTE2_SHIFT)
#define DI_CMDBUF0_SET_CMDBYTE2(di_cmdbuf0, cmdbyte2) { \
	FDL_ASSERT(!((cmdbyte2) & ~((1 << DI_CMDBUF0_CMDBYTE2_SIZE)-1))); \
	di_cmdbuf0 = (((unsigned long)(di_cmdbuf0)) & ~DI_CMDBUF0_CMDBYTE2_MASK) | (((unsigned long)(cmdbyte2)) << DI_CMDBUF0_CMDBYTE2_SHIFT);\
}
#define DI_CMDBUF0_CMDBYTE3_SIZE	8
#define DI_CMDBUF0_CMDBYTE3_SHIFT	24
#define DI_CMDBUF0_CMDBYTE3_MASK	0xff000000
#define DI_CMDBUF0_GET_CMDBYTE3(di_cmdbuf0) \
	((((unsigned long)(di_cmdbuf0)) & DI_CMDBUF0_CMDBYTE3_MASK) >> DI_CMDBUF0_CMDBYTE3_SHIFT)
#define DI_CMDBUF0_SET_CMDBYTE3(di_cmdbuf0, cmdbyte3) { \
	FDL_ASSERT(!((cmdbyte3) & ~((1 << DI_CMDBUF0_CMDBYTE3_SIZE)-1))); \
	di_cmdbuf0 = (((unsigned long)(di_cmdbuf0)) & ~DI_CMDBUF0_CMDBYTE3_MASK) | (((unsigned long)(cmdbyte3)) << DI_CMDBUF0_CMDBYTE3_SHIFT);\
}
#define DI_CMDBUF0_TOTAL_SIZE	32
#define DI_CMDBUF0(cmdbyte0, cmdbyte1, cmdbyte2, cmdbyte3) \
	((((unsigned long)(cmdbyte0)) << DI_CMDBUF0_CMDBYTE0_SHIFT) | \
	 (((unsigned long)(cmdbyte1)) << DI_CMDBUF0_CMDBYTE1_SHIFT) | \
	 (((unsigned long)(cmdbyte2)) << DI_CMDBUF0_CMDBYTE2_SHIFT) | \
	 (((unsigned long)(cmdbyte3)) << DI_CMDBUF0_CMDBYTE3_SHIFT))

typedef struct {
    unsigned long cmdbyte3:DI_CMDBUF0_CMDBYTE3_SIZE;
    unsigned long cmdbyte2:DI_CMDBUF0_CMDBYTE2_SIZE;
    unsigned long cmdbyte1:DI_CMDBUF0_CMDBYTE1_SIZE;
    unsigned long cmdbyte0:DI_CMDBUF0_CMDBYTE0_SIZE;
} di_cmdbuf0_t;

typedef union {
    unsigned long val;
    di_cmdbuf0_t f;
} di_cmdbuf0_u;

/*
 *  di_cmdbuf1 struct
 */
#define DI_CMDBUF1_CMDBYTE4_SIZE	8
#define DI_CMDBUF1_CMDBYTE4_SHIFT	0
#define DI_CMDBUF1_CMDBYTE4_MASK	0x000000ff
#define DI_CMDBUF1_GET_CMDBYTE4(di_cmdbuf1) \
	((((unsigned long)(di_cmdbuf1)) & DI_CMDBUF1_CMDBYTE4_MASK) >> DI_CMDBUF1_CMDBYTE4_SHIFT)
#define DI_CMDBUF1_SET_CMDBYTE4(di_cmdbuf1, cmdbyte4) { \
	FDL_ASSERT(!((cmdbyte4) & ~((1 << DI_CMDBUF1_CMDBYTE4_SIZE)-1))); \
	di_cmdbuf1 = (((unsigned long)(di_cmdbuf1)) & ~DI_CMDBUF1_CMDBYTE4_MASK) | (((unsigned long)(cmdbyte4)) << DI_CMDBUF1_CMDBYTE4_SHIFT);\
}
#define DI_CMDBUF1_CMDBYTE5_SIZE	8
#define DI_CMDBUF1_CMDBYTE5_SHIFT	8
#define DI_CMDBUF1_CMDBYTE5_MASK	0x0000ff00
#define DI_CMDBUF1_GET_CMDBYTE5(di_cmdbuf1) \
	((((unsigned long)(di_cmdbuf1)) & DI_CMDBUF1_CMDBYTE5_MASK) >> DI_CMDBUF1_CMDBYTE5_SHIFT)
#define DI_CMDBUF1_SET_CMDBYTE5(di_cmdbuf1, cmdbyte5) { \
	FDL_ASSERT(!((cmdbyte5) & ~((1 << DI_CMDBUF1_CMDBYTE5_SIZE)-1))); \
	di_cmdbuf1 = (((unsigned long)(di_cmdbuf1)) & ~DI_CMDBUF1_CMDBYTE5_MASK) | (((unsigned long)(cmdbyte5)) << DI_CMDBUF1_CMDBYTE5_SHIFT);\
}
#define DI_CMDBUF1_CMDBYTE6_SIZE	8
#define DI_CMDBUF1_CMDBYTE6_SHIFT	16
#define DI_CMDBUF1_CMDBYTE6_MASK	0x00ff0000
#define DI_CMDBUF1_GET_CMDBYTE6(di_cmdbuf1) \
	((((unsigned long)(di_cmdbuf1)) & DI_CMDBUF1_CMDBYTE6_MASK) >> DI_CMDBUF1_CMDBYTE6_SHIFT)
#define DI_CMDBUF1_SET_CMDBYTE6(di_cmdbuf1, cmdbyte6) { \
	FDL_ASSERT(!((cmdbyte6) & ~((1 << DI_CMDBUF1_CMDBYTE6_SIZE)-1))); \
	di_cmdbuf1 = (((unsigned long)(di_cmdbuf1)) & ~DI_CMDBUF1_CMDBYTE6_MASK) | (((unsigned long)(cmdbyte6)) << DI_CMDBUF1_CMDBYTE6_SHIFT);\
}
#define DI_CMDBUF1_CMDBYTE7_SIZE	8
#define DI_CMDBUF1_CMDBYTE7_SHIFT	24
#define DI_CMDBUF1_CMDBYTE7_MASK	0xff000000
#define DI_CMDBUF1_GET_CMDBYTE7(di_cmdbuf1) \
	((((unsigned long)(di_cmdbuf1)) & DI_CMDBUF1_CMDBYTE7_MASK) >> DI_CMDBUF1_CMDBYTE7_SHIFT)
#define DI_CMDBUF1_SET_CMDBYTE7(di_cmdbuf1, cmdbyte7) { \
	FDL_ASSERT(!((cmdbyte7) & ~((1 << DI_CMDBUF1_CMDBYTE7_SIZE)-1))); \
	di_cmdbuf1 = (((unsigned long)(di_cmdbuf1)) & ~DI_CMDBUF1_CMDBYTE7_MASK) | (((unsigned long)(cmdbyte7)) << DI_CMDBUF1_CMDBYTE7_SHIFT);\
}
#define DI_CMDBUF1_TOTAL_SIZE	32
#define DI_CMDBUF1(cmdbyte4, cmdbyte5, cmdbyte6, cmdbyte7) \
	((((unsigned long)(cmdbyte4)) << DI_CMDBUF1_CMDBYTE4_SHIFT) | \
	 (((unsigned long)(cmdbyte5)) << DI_CMDBUF1_CMDBYTE5_SHIFT) | \
	 (((unsigned long)(cmdbyte6)) << DI_CMDBUF1_CMDBYTE6_SHIFT) | \
	 (((unsigned long)(cmdbyte7)) << DI_CMDBUF1_CMDBYTE7_SHIFT))

typedef struct {
    unsigned long cmdbyte7:DI_CMDBUF1_CMDBYTE7_SIZE;
    unsigned long cmdbyte6:DI_CMDBUF1_CMDBYTE6_SIZE;
    unsigned long cmdbyte5:DI_CMDBUF1_CMDBYTE5_SIZE;
    unsigned long cmdbyte4:DI_CMDBUF1_CMDBYTE4_SIZE;
} di_cmdbuf1_t;

typedef union {
    unsigned long val;
    di_cmdbuf1_t f;
} di_cmdbuf1_u;

/*
 *  di_cmdbuf2 struct
 */
#define DI_CMDBUF2_CMDBYTE8_SIZE	8
#define DI_CMDBUF2_CMDBYTE8_SHIFT	0
#define DI_CMDBUF2_CMDBYTE8_MASK	0x000000ff
#define DI_CMDBUF2_GET_CMDBYTE8(di_cmdbuf2) \
	((((unsigned long)(di_cmdbuf2)) & DI_CMDBUF2_CMDBYTE8_MASK) >> DI_CMDBUF2_CMDBYTE8_SHIFT)
#define DI_CMDBUF2_SET_CMDBYTE8(di_cmdbuf2, cmdbyte8) { \
	FDL_ASSERT(!((cmdbyte8) & ~((1 << DI_CMDBUF2_CMDBYTE8_SIZE)-1))); \
	di_cmdbuf2 = (((unsigned long)(di_cmdbuf2)) & ~DI_CMDBUF2_CMDBYTE8_MASK) | (((unsigned long)(cmdbyte8)) << DI_CMDBUF2_CMDBYTE8_SHIFT);\
}
#define DI_CMDBUF2_CMDBYTE9_SIZE	8
#define DI_CMDBUF2_CMDBYTE9_SHIFT	8
#define DI_CMDBUF2_CMDBYTE9_MASK	0x0000ff00
#define DI_CMDBUF2_GET_CMDBYTE9(di_cmdbuf2) \
	((((unsigned long)(di_cmdbuf2)) & DI_CMDBUF2_CMDBYTE9_MASK) >> DI_CMDBUF2_CMDBYTE9_SHIFT)
#define DI_CMDBUF2_SET_CMDBYTE9(di_cmdbuf2, cmdbyte9) { \
	FDL_ASSERT(!((cmdbyte9) & ~((1 << DI_CMDBUF2_CMDBYTE9_SIZE)-1))); \
	di_cmdbuf2 = (((unsigned long)(di_cmdbuf2)) & ~DI_CMDBUF2_CMDBYTE9_MASK) | (((unsigned long)(cmdbyte9)) << DI_CMDBUF2_CMDBYTE9_SHIFT);\
}
#define DI_CMDBUF2_CMDBYTE10_SIZE	8
#define DI_CMDBUF2_CMDBYTE10_SHIFT	16
#define DI_CMDBUF2_CMDBYTE10_MASK	0x00ff0000
#define DI_CMDBUF2_GET_CMDBYTE10(di_cmdbuf2) \
	((((unsigned long)(di_cmdbuf2)) & DI_CMDBUF2_CMDBYTE10_MASK) >> DI_CMDBUF2_CMDBYTE10_SHIFT)
#define DI_CMDBUF2_SET_CMDBYTE10(di_cmdbuf2, cmdbyte10) { \
	FDL_ASSERT(!((cmdbyte10) & ~((1 << DI_CMDBUF2_CMDBYTE10_SIZE)-1))); \
	di_cmdbuf2 = (((unsigned long)(di_cmdbuf2)) & ~DI_CMDBUF2_CMDBYTE10_MASK) | (((unsigned long)(cmdbyte10)) << DI_CMDBUF2_CMDBYTE10_SHIFT);\
}
#define DI_CMDBUF2_CMDBYTE11_SIZE	8
#define DI_CMDBUF2_CMDBYTE11_SHIFT	24
#define DI_CMDBUF2_CMDBYTE11_MASK	0xff000000
#define DI_CMDBUF2_GET_CMDBYTE11(di_cmdbuf2) \
	((((unsigned long)(di_cmdbuf2)) & DI_CMDBUF2_CMDBYTE11_MASK) >> DI_CMDBUF2_CMDBYTE11_SHIFT)
#define DI_CMDBUF2_SET_CMDBYTE11(di_cmdbuf2, cmdbyte11) { \
	FDL_ASSERT(!((cmdbyte11) & ~((1 << DI_CMDBUF2_CMDBYTE11_SIZE)-1))); \
	di_cmdbuf2 = (((unsigned long)(di_cmdbuf2)) & ~DI_CMDBUF2_CMDBYTE11_MASK) | (((unsigned long)(cmdbyte11)) << DI_CMDBUF2_CMDBYTE11_SHIFT);\
}
#define DI_CMDBUF2_TOTAL_SIZE	32
#define DI_CMDBUF2(cmdbyte8, cmdbyte9, cmdbyte10, cmdbyte11) \
	((((unsigned long)(cmdbyte8)) << DI_CMDBUF2_CMDBYTE8_SHIFT) | \
	 (((unsigned long)(cmdbyte9)) << DI_CMDBUF2_CMDBYTE9_SHIFT) | \
	 (((unsigned long)(cmdbyte10)) << DI_CMDBUF2_CMDBYTE10_SHIFT) | \
	 (((unsigned long)(cmdbyte11)) << DI_CMDBUF2_CMDBYTE11_SHIFT))

typedef struct {
    unsigned long cmdbyte11:DI_CMDBUF2_CMDBYTE11_SIZE;
    unsigned long cmdbyte10:DI_CMDBUF2_CMDBYTE10_SIZE;
    unsigned long cmdbyte9:DI_CMDBUF2_CMDBYTE9_SIZE;
    unsigned long cmdbyte8:DI_CMDBUF2_CMDBYTE8_SIZE;
} di_cmdbuf2_t;

typedef union {
    unsigned long val;
    di_cmdbuf2_t f;
} di_cmdbuf2_u;

/*
 *  di_mar struct
 */
#define DI_MAR_PAD0_SIZE	5
#define DI_MAR_PAD0_SHIFT	0
#define DI_MAR_PAD0_MASK	0x0000001f
#define DI_MAR_GET_PAD0(di_mar) \
	((((unsigned long)(di_mar)) & DI_MAR_PAD0_MASK) >> DI_MAR_PAD0_SHIFT)
#define DI_MAR_SET_PAD0(di_mar, pad0) { \
	FDL_ASSERT(!((pad0) & ~((1 << DI_MAR_PAD0_SIZE)-1))); \
	di_mar = (((unsigned long)(di_mar)) & ~DI_MAR_PAD0_MASK) | (((unsigned long)(pad0)) << DI_MAR_PAD0_SHIFT);\
}
#define DI_MAR_DIMAR_SIZE	21
#define DI_MAR_DIMAR_SHIFT	5
#define DI_MAR_DIMAR_MASK	0x03ffffe0
#define DI_MAR_GET_DIMAR(di_mar) \
	((((unsigned long)(di_mar)) & DI_MAR_DIMAR_MASK) >> DI_MAR_DIMAR_SHIFT)
#define DI_MAR_SET_DIMAR(di_mar, dimar) { \
	FDL_ASSERT(!((dimar) & ~((1 << DI_MAR_DIMAR_SIZE)-1))); \
	di_mar = (((unsigned long)(di_mar)) & ~DI_MAR_DIMAR_MASK) | (((unsigned long)(dimar)) << DI_MAR_DIMAR_SHIFT);\
}
#define DI_MAR_PAD1_SIZE	6
#define DI_MAR_PAD1_SHIFT	26
#define DI_MAR_PAD1_MASK	0xfc000000
#define DI_MAR_GET_PAD1(di_mar) \
	((((unsigned long)(di_mar)) & DI_MAR_PAD1_MASK) >> DI_MAR_PAD1_SHIFT)
#define DI_MAR_SET_PAD1(di_mar, pad1) { \
	FDL_ASSERT(!((pad1) & ~((1 << DI_MAR_PAD1_SIZE)-1))); \
	di_mar = (((unsigned long)(di_mar)) & ~DI_MAR_PAD1_MASK) | (((unsigned long)(pad1)) << DI_MAR_PAD1_SHIFT);\
}
#define DI_MAR_TOTAL_SIZE	32
#define DI_MAR(dimar) \
	((((unsigned long)(dimar)) << DI_MAR_DIMAR_SHIFT))

typedef struct {
    unsigned long pad1:DI_MAR_PAD1_SIZE;
    unsigned long dimar:DI_MAR_DIMAR_SIZE;
    unsigned long pad0:DI_MAR_PAD0_SIZE;
} di_mar_t;

typedef union {
    unsigned long val;
    di_mar_t f;
} di_mar_u;

/*
 *  di_length struct
 */
#define DI_LENGTH_PAD0_SIZE	5
#define DI_LENGTH_PAD0_SHIFT	0
#define DI_LENGTH_PAD0_MASK	0x0000001f
#define DI_LENGTH_GET_PAD0(di_length) \
	((((unsigned long)(di_length)) & DI_LENGTH_PAD0_MASK) >> DI_LENGTH_PAD0_SHIFT)
#define DI_LENGTH_SET_PAD0(di_length, pad0) { \
	FDL_ASSERT(!((pad0) & ~((1 << DI_LENGTH_PAD0_SIZE)-1))); \
	di_length = (((unsigned long)(di_length)) & ~DI_LENGTH_PAD0_MASK) | (((unsigned long)(pad0)) << DI_LENGTH_PAD0_SHIFT);\
}
#define DI_LENGTH_DILENGTH_SIZE	21
#define DI_LENGTH_DILENGTH_SHIFT	5
#define DI_LENGTH_DILENGTH_MASK	0x03ffffe0
#define DI_LENGTH_GET_DILENGTH(di_length) \
	((((unsigned long)(di_length)) & DI_LENGTH_DILENGTH_MASK) >> DI_LENGTH_DILENGTH_SHIFT)
#define DI_LENGTH_SET_DILENGTH(di_length, dilength) { \
	FDL_ASSERT(!((dilength) & ~((1 << DI_LENGTH_DILENGTH_SIZE)-1))); \
	di_length = (((unsigned long)(di_length)) & ~DI_LENGTH_DILENGTH_MASK) | (((unsigned long)(dilength)) << DI_LENGTH_DILENGTH_SHIFT);\
}
#define DI_LENGTH_PAD1_SIZE	6
#define DI_LENGTH_PAD1_SHIFT	26
#define DI_LENGTH_PAD1_MASK	0xfc000000
#define DI_LENGTH_GET_PAD1(di_length) \
	((((unsigned long)(di_length)) & DI_LENGTH_PAD1_MASK) >> DI_LENGTH_PAD1_SHIFT)
#define DI_LENGTH_SET_PAD1(di_length, pad1) { \
	FDL_ASSERT(!((pad1) & ~((1 << DI_LENGTH_PAD1_SIZE)-1))); \
	di_length = (((unsigned long)(di_length)) & ~DI_LENGTH_PAD1_MASK) | (((unsigned long)(pad1)) << DI_LENGTH_PAD1_SHIFT);\
}
#define DI_LENGTH_TOTAL_SIZE	32
#define DI_LENGTH(dilength) \
	((((unsigned long)(dilength)) << DI_LENGTH_DILENGTH_SHIFT))

typedef struct {
    unsigned long pad1:DI_LENGTH_PAD1_SIZE;
    unsigned long dilength:DI_LENGTH_DILENGTH_SIZE;
    unsigned long pad0:DI_LENGTH_PAD0_SIZE;
} di_length_t;

typedef union {
    unsigned long val;
    di_length_t f;
} di_length_u;

/*
 *  di_cr struct
 */
#define DI_CR_TSTART_SIZE	1
#define DI_CR_TSTART_SHIFT	0
#define DI_CR_TSTART_MASK	0x00000001
#define DI_CR_GET_TSTART(di_cr) \
	((((unsigned long)(di_cr)) & DI_CR_TSTART_MASK) >> DI_CR_TSTART_SHIFT)
#define DI_CR_SET_TSTART(di_cr, tstart) { \
	FDL_ASSERT(!((tstart) & ~((1 << DI_CR_TSTART_SIZE)-1))); \
	di_cr = (((unsigned long)(di_cr)) & ~DI_CR_TSTART_MASK) | (((unsigned long)(tstart)) << DI_CR_TSTART_SHIFT);\
}
#define DI_CR_DMA_SIZE	1
#define DI_CR_DMA_SHIFT	1
#define DI_CR_DMA_MASK	0x00000002
#define DI_CR_GET_DMA(di_cr) \
	((((unsigned long)(di_cr)) & DI_CR_DMA_MASK) >> DI_CR_DMA_SHIFT)
#define DI_CR_SET_DMA(di_cr, dma) { \
	FDL_ASSERT(!((dma) & ~((1 << DI_CR_DMA_SIZE)-1))); \
	di_cr = (((unsigned long)(di_cr)) & ~DI_CR_DMA_MASK) | (((unsigned long)(dma)) << DI_CR_DMA_SHIFT);\
}
#define DI_CR_RW_SIZE	1
#define DI_CR_RW_SHIFT	2
#define DI_CR_RW_MASK	0x00000004
#define DI_CR_GET_RW(di_cr) \
	((((unsigned long)(di_cr)) & DI_CR_RW_MASK) >> DI_CR_RW_SHIFT)
#define DI_CR_SET_RW(di_cr, rw) { \
	FDL_ASSERT(!((rw) & ~((1 << DI_CR_RW_SIZE)-1))); \
	di_cr = (((unsigned long)(di_cr)) & ~DI_CR_RW_MASK) | (((unsigned long)(rw)) << DI_CR_RW_SHIFT);\
}
#define DI_CR_PAD0_SIZE	29
#define DI_CR_PAD0_SHIFT	3
#define DI_CR_PAD0_MASK	0xfffffff8
#define DI_CR_GET_PAD0(di_cr) \
	((((unsigned long)(di_cr)) & DI_CR_PAD0_MASK) >> DI_CR_PAD0_SHIFT)
#define DI_CR_SET_PAD0(di_cr, pad0) { \
	FDL_ASSERT(!((pad0) & ~((1 << DI_CR_PAD0_SIZE)-1))); \
	di_cr = (((unsigned long)(di_cr)) & ~DI_CR_PAD0_MASK) | (((unsigned long)(pad0)) << DI_CR_PAD0_SHIFT);\
}
#define DI_CR_TOTAL_SIZE	32
#define DI_CR(tstart, dma, rw) \
	((((unsigned long)(tstart)) << DI_CR_TSTART_SHIFT) | \
	 (((unsigned long)(dma)) << DI_CR_DMA_SHIFT) | \
	 (((unsigned long)(rw)) << DI_CR_RW_SHIFT))

typedef struct {
    unsigned long pad0:DI_CR_PAD0_SIZE;
    unsigned long rw:DI_CR_RW_SIZE;
    unsigned long dma:DI_CR_DMA_SIZE;
    unsigned long tstart:DI_CR_TSTART_SIZE;
} di_cr_t;

typedef union {
    unsigned long val;
    di_cr_t f;
} di_cr_u;

/*
 *  di_immbuf struct
 */
#define DI_IMMBUF_REGVAL3_SIZE	8
#define DI_IMMBUF_REGVAL3_SHIFT	0
#define DI_IMMBUF_REGVAL3_MASK	0x000000ff
#define DI_IMMBUF_GET_REGVAL3(di_immbuf) \
	((((unsigned long)(di_immbuf)) & DI_IMMBUF_REGVAL3_MASK) >> DI_IMMBUF_REGVAL3_SHIFT)
#define DI_IMMBUF_SET_REGVAL3(di_immbuf, regval3) { \
	FDL_ASSERT(!((regval3) & ~((1 << DI_IMMBUF_REGVAL3_SIZE)-1))); \
	di_immbuf = (((unsigned long)(di_immbuf)) & ~DI_IMMBUF_REGVAL3_MASK) | (((unsigned long)(regval3)) << DI_IMMBUF_REGVAL3_SHIFT);\
}
#define DI_IMMBUF_REGVAL2_SIZE	8
#define DI_IMMBUF_REGVAL2_SHIFT	8
#define DI_IMMBUF_REGVAL2_MASK	0x0000ff00
#define DI_IMMBUF_GET_REGVAL2(di_immbuf) \
	((((unsigned long)(di_immbuf)) & DI_IMMBUF_REGVAL2_MASK) >> DI_IMMBUF_REGVAL2_SHIFT)
#define DI_IMMBUF_SET_REGVAL2(di_immbuf, regval2) { \
	FDL_ASSERT(!((regval2) & ~((1 << DI_IMMBUF_REGVAL2_SIZE)-1))); \
	di_immbuf = (((unsigned long)(di_immbuf)) & ~DI_IMMBUF_REGVAL2_MASK) | (((unsigned long)(regval2)) << DI_IMMBUF_REGVAL2_SHIFT);\
}
#define DI_IMMBUF_REGVAL1_SIZE	8
#define DI_IMMBUF_REGVAL1_SHIFT	16
#define DI_IMMBUF_REGVAL1_MASK	0x00ff0000
#define DI_IMMBUF_GET_REGVAL1(di_immbuf) \
	((((unsigned long)(di_immbuf)) & DI_IMMBUF_REGVAL1_MASK) >> DI_IMMBUF_REGVAL1_SHIFT)
#define DI_IMMBUF_SET_REGVAL1(di_immbuf, regval1) { \
	FDL_ASSERT(!((regval1) & ~((1 << DI_IMMBUF_REGVAL1_SIZE)-1))); \
	di_immbuf = (((unsigned long)(di_immbuf)) & ~DI_IMMBUF_REGVAL1_MASK) | (((unsigned long)(regval1)) << DI_IMMBUF_REGVAL1_SHIFT);\
}
#define DI_IMMBUF_REGVAL0_SIZE	8
#define DI_IMMBUF_REGVAL0_SHIFT	24
#define DI_IMMBUF_REGVAL0_MASK	0xff000000
#define DI_IMMBUF_GET_REGVAL0(di_immbuf) \
	((((unsigned long)(di_immbuf)) & DI_IMMBUF_REGVAL0_MASK) >> DI_IMMBUF_REGVAL0_SHIFT)
#define DI_IMMBUF_SET_REGVAL0(di_immbuf, regval0) { \
	FDL_ASSERT(!((regval0) & ~((1 << DI_IMMBUF_REGVAL0_SIZE)-1))); \
	di_immbuf = (((unsigned long)(di_immbuf)) & ~DI_IMMBUF_REGVAL0_MASK) | (((unsigned long)(regval0)) << DI_IMMBUF_REGVAL0_SHIFT);\
}
#define DI_IMMBUF_TOTAL_SIZE	32
#define DI_IMMBUF(regval3, regval2, regval1, regval0) \
	((((unsigned long)(regval3)) << DI_IMMBUF_REGVAL3_SHIFT) | \
	 (((unsigned long)(regval2)) << DI_IMMBUF_REGVAL2_SHIFT) | \
	 (((unsigned long)(regval1)) << DI_IMMBUF_REGVAL1_SHIFT) | \
	 (((unsigned long)(regval0)) << DI_IMMBUF_REGVAL0_SHIFT))

typedef struct {
    unsigned long regval0:DI_IMMBUF_REGVAL0_SIZE;
    unsigned long regval1:DI_IMMBUF_REGVAL1_SIZE;
    unsigned long regval2:DI_IMMBUF_REGVAL2_SIZE;
    unsigned long regval3:DI_IMMBUF_REGVAL3_SIZE;
} di_immbuf_t;

typedef union {
    unsigned long val;
    di_immbuf_t f;
} di_immbuf_u;

/*
 *  di_config struct
 */
#define DI_CONFIG_CONFIG_SIZE	8
#define DI_CONFIG_CONFIG_SHIFT	0
#define DI_CONFIG_CONFIG_MASK	0x000000ff
#define DI_CONFIG_GET_CONFIG(di_config) \
	((((unsigned long)(di_config)) & DI_CONFIG_CONFIG_MASK) >> DI_CONFIG_CONFIG_SHIFT)
#define DI_CONFIG_SET_CONFIG(di_config, config) { \
	FDL_ASSERT(!((config) & ~((1 << DI_CONFIG_CONFIG_SIZE)-1))); \
	di_config = (((unsigned long)(di_config)) & ~DI_CONFIG_CONFIG_MASK) | (((unsigned long)(config)) << DI_CONFIG_CONFIG_SHIFT);\
}
#define DI_CONFIG_PAD0_SIZE	24
#define DI_CONFIG_PAD0_SHIFT	8
#define DI_CONFIG_PAD0_MASK	0xffffff00
#define DI_CONFIG_GET_PAD0(di_config) \
	((((unsigned long)(di_config)) & DI_CONFIG_PAD0_MASK) >> DI_CONFIG_PAD0_SHIFT)
#define DI_CONFIG_SET_PAD0(di_config, pad0) { \
	FDL_ASSERT(!((pad0) & ~((1 << DI_CONFIG_PAD0_SIZE)-1))); \
	di_config = (((unsigned long)(di_config)) & ~DI_CONFIG_PAD0_MASK) | (((unsigned long)(pad0)) << DI_CONFIG_PAD0_SHIFT);\
}
#define DI_CONFIG_TOTAL_SIZE	32
#define DI_CONFIG(config) \
	((((unsigned long)(config)) << DI_CONFIG_CONFIG_SHIFT))

typedef struct {
    unsigned long pad0:DI_CONFIG_PAD0_SIZE;
    unsigned long config:DI_CONFIG_CONFIG_SIZE;
} di_config_t;

typedef union {
    unsigned long val;
    di_config_t f;
} di_config_u;
