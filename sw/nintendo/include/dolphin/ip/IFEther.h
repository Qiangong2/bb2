/*---------------------------------------------------------------------------*
  Project:  Dolphin IF API
  File:     IFEther.h

  Copyright 2002 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: IFEther.h,v $
  Revision 1.1.1.1  2004/03/02 05:31:13  paulm
  Socket Library 24-Sep-2003

    
    3     7/29/02 8:56 Shiki
    Added ETH_PPPoE_*.

    2     3/13/02 9:13 Shiki
    Clean up with IP_ALEN and ETH_ALEN.

    1     3/11/02 19:35 Shiki
    Initial check-in.
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#ifndef __IF_ETHER_H__
#define __IF_ETHER_H__

#include <dolphin/ip.h>

#ifdef __cplusplus
extern "C" {
#endif

//
// Ethernet
//

// Ethernet type
#define ETH_IP                      0x0800  // DOD Internet Protocol (IP)
#define ETH_ARP                     0x0806  // Address Resolution Protocol (ARP)
#define ETH_RARP                    0x8035  // Reverse Address Resolution Protocol (RARP)
#define ETH_PAUSE                   0x8808  // IEEE 802.3x PAUSE Frame (01:80:C2:00:00:01)
#define ETH_PPPoE_DISCOVERY         0x8863  // PPPoE Discovery Stage
#define ETH_PPPoE_SESSION           0x8864  // PPPoE Session Stage

#ifndef ETH_ALEN
#define ETH_ALEN                    6   // Ethernet address length
#endif  // ETH_ALEN

#define ETH_HLEN                    14

typedef struct ETHHeader
{
    u8  dst[ETH_ALEN];
    u8  src[ETH_ALEN];
    u16 type;
} ETHHeader;

//
// ARP RFC 826, 920
//

#define ARP_HARDWARE_ETHERNET       1
#define ARP_PROTOCOL_IP             0x800   // IPv4

#define ARP_OPCODE_REQUEST          1
#define ARP_OPCODE_REPLY            2
#define ARP_OPCODE_REQUEST_REVERSE  3
#define ARP_OPCODE_REPLY_REVERSE    4

#define ARP_HLEN                    8

// ARP packet format
typedef struct ARPHeader
{
    u16 hwType;     // ARP_HARDWARE_ETHERNET for Ethernet
    u16 prType;     // ARP_PROTOCOL_IP for IPv4
    u8  hwAddrLen;
    u8  prAddrLen;
    u16 opCode;
    // u8 srcHwAddr[hwAddrLen];
    // u8 srcPrAddr[prAddrLen];
    // u8 dstHwAddr[hwAddrLen];
    // u8 dstPrAddr[prAddrLen];
} ARPHeader;

#define ARPGetSrcHwAddr(arp)    ((u8*) arp + ARP_HLEN)
#define ARPGetSrcPrAddr(arp)    ((u8*) arp + ARP_HLEN + (arp)->hwAddrLen)
#define ARPGetDstHwAddr(arp)    ((u8*) arp + ARP_HLEN + (arp)->hwAddrLen + (arp)->prAddrLen)
#define ARPGetDstPrAddr(arp)    ((u8*) arp + ARP_HLEN + 2 * (arp)->hwAddrLen + (arp)->prAddrLen)

void ARPDump( void );

#ifdef __cplusplus
}
#endif

#endif  /* __IF_ETHER_H__ */
