/*---------------------------------------------------------------------------*
  Project:  Dolphin IP API
  File:     IPDhcp.h

  Copyright 2002 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: IPDhcp.h,v $
  Revision 1.1.1.1  2004/03/02 05:31:13  paulm
  Socket Library 24-Sep-2003

    
    14    9/15/03 21:54 Shiki
    beta 3.7
    
    11    7/04/03 16:46 Shiki
    Declared DHCPProcessOptions().

    10    5/12/03 20:47 Shiki
    Defined DHCP_BOOTP_LEN.

    9     4/21/03 19:48 Shiki
    Defined DHCP_OPT_CLIENT_IDENTIFIER.

    8     4/18/03 21:58 Shiki
    Declared DHCPStartupEx().
    
    6     8/05/02 9:22 Shiki
    Added DHCP_OPT_MTU.

    5     4/26/02 15:18 Shiki
    Changed DHCPStartup() interface.

    4     4/26/02 13:36 Shiki
    Added DHCPAuto().

    3     4/23/02 21:16 Shiki
    Added DHCPReboot().

    2     3/13/02 9:13 Shiki
    Clean up with IP_ALEN and ETH_ALEN.

    1     3/11/02 19:35 Shiki
    Initial check-in.
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#ifndef __IP_DHCP_H__
#define __IP_DHCP_H__

#include <dolphin/ip.h>

#ifdef __cplusplus
extern "C" {
#endif

#define DHCP_SERVER_PORT        67
#define DHCP_CLIENT_PORT        68

typedef struct DHCPHeader
{
    u8  op;                 // Opcode (1=request, 2=reply)
    u8  htype;              // Hardware type (1=Ethernet)
    u8  hlen;               // Hardware address length (6 for Ethernet)
    u8  hops;               // Hop count
    u32 xid;                // Transaction ID
    u16 secs;               // Number of seconds
    u16 flags;              // DHCP_BROADCAST or zero
    u8  ciaddr[IP_ALEN];    // Client IP address
    u8  yiaddr[IP_ALEN];    // your IP address
    u8  siaddr[IP_ALEN];    // Server IP address
    u8  giaddr[IP_ALEN];    // Gateway IP address
    u8  chaddr[16];         // Client hardware address
    u8  sname[64];          // Server host name
    u8  file[128];          // Boot filename
    // Options...
} DHCPHeader;

#define DHCP_MIN_HLEN           236
#define DHCP_BOOTP_LEN          300     // BOOTP message length

// Opcode
#define DHCP_BOOTREQUEST        1
#define DHCP_BOOTREPLY          2

// Flags
#define DHCP_BROADCAST          0x8000

// Pad Option
//
//  Code
// +-----+
// |  0  |
// +-----+
#define DHCP_OPT_PAD            0

// End option - The last option must always be the 'end' option.
//
//  Code
// +-----+
// | 255 |
// +-----+
#define DHCP_OPT_END            255

// Subnet Mask
//
//  Code   Len        Subnet Mask
// +-----+-----+-----+-----+-----+-----+
// |  1  |  4  |  m1 |  m2 |  m3 |  m4 |
// +-----+-----+-----+-----+-----+-----+
#define DHCP_OPT_SUBNETMASK     1

// Time Offset
//
//  Code   Len        Time Offset
// +-----+-----+-----+-----+-----+-----+
// |  2  |  4  |  n1 |  n2 |  n3 |  n4 |
// +-----+-----+-----+-----+-----+-----+
#define DHCP_OPT_TIME_OFFSET    2

// Router Option
//
//  Code   Len         Address 1               Address 2
// +-----+-----+-----+-----+-----+-----+-----+-----+--
// |  3  |  n  |  a1 |  a2 |  a3 |  a4 |  a1 |  a2 |  ...
// +-----+-----+-----+-----+-----+-----+-----+-----+--
#define DHCP_OPT_ROUTER         3

// Domain Name Server Option
//
//  Code   Len         Address 1               Address 2
// +-----+-----+-----+-----+-----+-----+-----+-----+--
// |  6  |  n  |  a1 |  a2 |  a3 |  a4 |  a1 |  a2 |  ...
// +-----+-----+-----+-----+-----+-----+-----+-----+--
#define DHCP_OPT_DNS            6

// Host Name Option
//
//  Code   Len                 Host Name
// +-----+-----+-----+-----+-----+-----+-----+-----+--
// |  12 |  n  |  h1 |  h2 |  h3 |  h4 |  h5 |  h6 |  ...
// +-----+-----+-----+-----+-----+-----+-----+-----+--
#define DHCP_OPT_HOST_NAME      12

// Domain Name
//
//  Code   Len        Domain Name
// +-----+-----+-----+-----+-----+-----+--
// |  15 |  n  |  d1 |  d2 |  d3 |  d4 |  ...
// +-----+-----+-----+-----+-----+-----+--
#define DHCP_OPT_DOMAIN_NAME    15

// Interface MTU Option
//
//  Code   Len      MTU
// +-----+-----+-----+-----+
// |  26 |  2  |  m1 |  m2 |
// +-----+-----+-----+-----+
#define DHCP_OPT_MTU            26

// Broadcast Address Option
//
//  Code   Len     Broadcast Address
// +-----+-----+-----+-----+-----+-----+
// |  28 |  4  |  b1 |  b2 |  b3 |  b4 |
// +-----+-----+-----+-----+-----+-----+
#define DHCP_OPT_BROADCAST_ADDR 28

// Static Route Option
//
//  Code   Len         Destination 1           Router 1
// +-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
// |  33 |  n  |  d1 |  d2 |  d3 |  d4 |  r1 |  r2 |  r3 |  r4 |
// +-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
//         Destination 2           Router 2
// +-----+-----+-----+-----+-----+-----+-----+-----+---
// |  d1 |  d2 |  d3 |  d4 |  r1 |  r2 |  r3 |  r4 | ...
// +-----+-----+-----+-----+-----+-----+-----+-----+---
#define DHCP_OPT_STATIC_ROUTE   33

// ARP Cache Timeout Option
//
//  Code   Len           Time
// +-----+-----+-----+-----+-----+-----+
// |  35 |  4  |  t1 |  t2 |  t3 |  t4 |
// +-----+-----+-----+-----+-----+-----+
#define DHCP_OPT_ARP_TIMEOUT    35

//
// DHCP Extensions
//

// Requested IP Address
//
//  Code   Len          Address
// +-----+-----+-----+-----+-----+-----+
// |  50 |  4  |  a1 |  a2 |  a3 |  a4 |
// +-----+-----+-----+-----+-----+-----+
#define DHCP_OPT_REQUESTED_ADDR 50

// IP Address Lease Time
//
//  Code   Len         Lease Time
// +-----+-----+-----+-----+-----+-----+
// |  51 |  4  |  t1 |  t2 |  t3 |  t4 |
// +-----+-----+-----+-----+-----+-----+
#define DHCP_OPT_LEASE_TIME     51

// Option Overload
//
//  Code   Len  Value
// +-----+-----+-----+
// |  52 |  1  |1/2/3|
// +-----+-----+-----+
#define DHCP_OPT_OVERLOAD       52

#define DHCP_FILE               1
#define DHCP_SNAME              2
#define DHCP_FILE_AND_SNAME     3

// DHCP message type
//
//  Code   Len  Type
// +-----+-----+-----+
// |  53 |  1  | 1-7 |
// +-----+-----+-----+
#define DHCP_OPT_DHCP_TYPE      53

#define DHCP_DISCOVER           1
#define DHCP_OFFER              2
#define DHCP_REQUEST            3
#define DHCP_DECLINE            4
#define DHCP_ACK                5
#define DHCP_NAK                6
#define DHCP_RELEASE            7

// Server Identifier
//
// Code   Len            Address
// +-----+-----+-----+-----+-----+-----+
// |  54 |  4  |  a1 |  a2 |  a3 |  a4 |
// +-----+-----+-----+-----+-----+-----+
#define DHCP_OPT_SERVER_ID      54

// Parameter Request List
//
//  Code   Len   Option Codes
// +-----+-----+-----+-----+---
// |  55 |  n  |  c1 |  c2 | ...
// +-----+-----+-----+-----+---
#define DHCP_OPT_REQUEST_LIST   55

// Maximum DHCP Message Size
//
// Code   Len     Length
//+-----+-----+-----+-----+
//|  57 |  2  |  l1 |  l2 |
//+-----+-----+-----+-----+
#define DHCP_OPT_MAX_SIZE       57

// Renewal (T1) Time Value
//
//  Code   Len         T1 Interval
// +-----+-----+-----+-----+-----+-----+
// |  58 |  4  |  t1 |  t2 |  t3 |  t4 |
// +-----+-----+-----+-----+-----+-----+
#define DHCP_OPT_RENEWAL_TIME   58

// Rebinding (T2) Time Value
//
//  Code   Len         T2 Interval
// +-----+-----+-----+-----+-----+-----+
// |  59 |  4  |  t1 |  t2 |  t3 |  t4 |
// +-----+-----+-----+-----+-----+-----+
#define DHCP_OPT_REBINDING_TIME  59

// Client-identifier
//
// Code   Len   Type  Client-Identifier
// +-----+-----+-----+-----+-----+---
// |  61 |  n  |  t1 |  i1 |  i2 | ...
// +-----+-----+-----+-----+-----+---
#define DHCP_OPT_CLIENT_IDENTIFIER  61

typedef struct DHCPInfo
{
    u8  ipaddr[IP_ALEN];        // dhcp->yiaddr

    // BOOTP options
    u8  netmask[IP_ALEN];       // DHCP_OPT_SUBNETMASK
    u8  router[IP_ALEN];        // DHCP_OPT_ROUTER (1st)
    u8  dns1[IP_ALEN];          // DHCP_OPT_DNS (1st)
    u8  dns2[IP_ALEN];          // DHCP_OPT_DNS (2nd)
    u8  domain[256];            // DHCP_OPT_DOMAIN_NAME
    u16 mtu;                    // DHCP_OPT_MTU
    u8  broadcast[IP_ALEN];     // DHCP_OPT_BROADCAST_ADDR

    // DHCP Extensions
    u32 lease;                  // DHCP_OPT_LEASE_TIME [sec]
    u8  server[IP_ALEN];        // DHCP_OPT_SERVER_ID
    u32 renewal;                // DHCP_OPT_RENEWAL_TIME [sec] (t1)
    u32 rebinding;              // DHCP_OPT_REBINDING_TIME [sec] (t2)
} DHCPInfo;

#define DHCP_STATE_INIT         0
#define DHCP_STATE_SELECTING    1
#define DHCP_STATE_REQUESTING   2
#define DHCP_STATE_BOUND        3
#define DHCP_STATE_RENEWING     4
#define DHCP_STATE_REBINDING    5
#define DHCP_STATE_INIT_REBOOT  6
#define DHCP_STATE_REBOOTING    7

typedef void (* DHCPCallback )( int state );

void DHCPDump     ( DHCPHeader* dhcp, s32 optlen );
BOOL DHCPStartup  ( DHCPCallback callback );
BOOL DHCPStartupEx( DHCPCallback callback, int rxmitMax );
BOOL DHCPCleanup  ( void );
BOOL DHCPReboot   ( void );
int  DHCPGetStatus( DHCPInfo* info );
BOOL DHCPAuto     ( BOOL enable );

int
DHCPProcessOptions( DHCPHeader* dhcp, int mlen, DHCPInfo* info );

#ifdef __cplusplus
}
#endif

#endif  /* __IP_DHCP_H__ */
