/*---------------------------------------------------------------------------*
  Project:  Dolphin IP API
  File:     IPDns.h

  Copyright 2002 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: IPDns.h,v $
  Revision 1.1.1.1  2004/03/02 05:31:13  paulm
  Socket Library 24-Sep-2003

    
    7     6/18/02 11:55 Shiki
    Updated IP_ERR_DNS_* values in comments.

    6     4/17/02 10:01 Shiki
    Defined DNS_FLAG_* flags.

    5     4/15/02 13:07 Shiki
    Fixed DNSOpen() interface to be const safe.

    4     4/15/02 11:43 Shiki
    Added flag field to DNSInfo.

    3     3/25/02 16:43 Shiki
    Changed DNSGetNameAsync() interface.

    2     3/25/02 15:07 Shiki
    Revised.

    1     3/11/02 19:35 Shiki
    Initial check-in.
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#ifndef __IP_DNS_H__
#define __IP_DNS_H__

#include <dolphin/ip.h>

#ifdef __cplusplus
extern "C" {
#endif

//
// Domain Name [RFC 1035]
//

#define DNS_PORT            53
#define DNS_HLEN            12

#define DNS_LABEL_MAX       63      // labels are 63 octets or less
#define DNS_NAME_MAX        255     // names are 255 octets or less
#define DNS_UDP_MAX         512     // UDP messages are 512 octets or less

// Types
#define DNS_TYPE_A          1       // a host address
#define DNS_TYPE_NS         2       // an authoritative name server
#define DNS_TYPE_MD         3       // a mail destination (Obsolete - use MX)
#define DNS_TYPE_MF         4       // a mail forwarder (Obsolete - use MX)
#define DNS_TYPE_CNAME      5       // the canonical name for an alias
#define DNS_TYPE_SOA        6       // marks the start of a zone of authority
#define DNS_TYPE_MB         7       // a mailbox domain name (EXPERIMENTAL)
#define DNS_TYPE_MG         8       // a mail group member (EXPERIMENTAL)
#define DNS_TYPE_MR         9       // a mail rename domain name (EXPERIMENTAL)
#define DNS_TYPE_NULL       10      // a null RR (EXPERIMENTAL)
#define DNS_TYPE_WKS        11      // a well known service description
#define DNS_TYPE_PTR        12      // a domain name pointer
#define DNS_TYPE_HINFO      13      // host information
#define DNS_TYPE_MINFO      14      // mailbox or mail list information
#define DNS_TYPE_MX         15      // mail exchange
#define DNS_TYPE_TXT        16      // text strings

#define DNS_TYPE_RP         17      // responsible Person  [RFC 1183]
#define DNS_TYPE_AFSDB      18      // AFS Data Base location  [RFC 1183]
#define DNS_TYPE_X25        19      // X.25 PSDN address  [RFC 1183]
#define DNS_TYPE_ISDN       20      // ISDN address  [RFC 1183]
#define DNS_TYPE_RT         21      // route Through  [RFC 1183]
#define DNS_TYPE_NSAP       22      // NSAP address, NSAP style A record  [RFC 1706]
#define DNS_TYPE_NSAP_PTR   23      //
#define DNS_TYPE_SIG        24      // security signature  [RFC 2535], [RFC 2931]
#define DNS_TYPE_KEY        25      // security key  [RFC 2535]
#define DNS_TYPE_PX         26      // X.400 mail mapping information  [RFC 2163]
#define DNS_TYPE_GPOS       27      // geographical Position  [RFC 1712]
#define DNS_TYPE_AAAA       28      // IPv6 Address  [RFC 1886]
#define DNS_TYPE_LOC        29      // location Information  [RFC 1876]
#define DNS_TYPE_NXT        30      // next Domain  [RFC 2535]
#define DNS_TYPE_EID        31      // endpoint Identifier
#define DNS_TYPE_NIMLOC     32      // nimrod Locator
#define DNS_TYPE_NB         32      // NetBIOS general Name Service
#define DNS_TYPE_SRV        33      // server Selection
#define DNS_TYPE_NBSTAT     33      // NetBIOS NODE STATUS  [RFC 2052], [RFC 2782]
#define DNS_TYPE_ATMA       34      // ATM Address
#define DNS_TYPE_NAPTR      35      // naming authority pointer  [RFC 2168], [RFC 2915]
#define DNS_TYPE_KX         36      // key Exchanger  [RFC 2230]
#define DNS_TYPE_CERT       37      // [RFC 2538]
#define DNS_TYPE_A6         38      // [RFC 2874]
#define DNS_TYPE_DNAME      39      // [RFC 2672]
#define DNS_TYPE_SINK       40      //
#define DNS_TYPE_OPT        41      // [RFC 2671]
#define DNS_TYPE_APL        42      // [RFC 3123]

// Classes
#define DNS_CLASS_IN        1       // the Internet
#define DNS_CLASS_CS        2       // the CSNET class (Obsolete)
#define DNS_CLASS_CH        3       // the CHAOS class
#define DNS_CLASS_HS        4       // Hesiod [Dyer 87]

// Flags
#define DNS_FLAG_QR         0x8000
#define DNS_FLAG_Q          0x0000  // query
#define DNS_FLAG_R          0x8000  // response
#define DNS_OP_QUERY        0x0000  // standard query
#define DNS_OP_IQUERY       0x0800  // inverse query
#define DNS_OP_STATUS       0x1000  // server status request
#define DNS_OP_NOTIFY       0x2000  // [RFC 1996]
#define DNS_OP_UPDATE       0x2800  // [RFC 2136]
#define DNS_FLAG_AA         0x0400  // authoritative answer
#define DNS_FLAG_TC         0x0200  // truncation
#define DNS_FLAG_RD         0x0100  // recursion desired
#define DNS_FLAG_RA         0x0080  // recursion available
#define DNS_ERROR_MASK      0x000f
#define DNS_ERROR_NONE      0x0000  // No error
#define DNS_ERROR_FORMAT    0x0001  // Format error
#define DNS_ERROR_SERVER    0x0002  // Server failure
#define DNS_ERROR_NAME      0x0003  // Name Error
#define DNS_ERROR_NOTIMP    0x0004  // Not Implemented
#define DNS_ERROR_REFUSED   0x0005  // Refused

#define IP_ERR_DNS_FORMAT   (IP_ERR_DNS_MAX - DNS_ERROR_FORMAT )    // -201
#define IP_ERR_DNS_SERVER   (IP_ERR_DNS_MAX - DNS_ERROR_SERVER )    // -202
#define IP_ERR_DNS_NAME     (IP_ERR_DNS_MAX - DNS_ERROR_NAME   )    // -203
#define IP_ERR_DNS_NOTIMP   (IP_ERR_DNS_MAX - DNS_ERROR_NOTIMP )    // -204
#define IP_ERR_DNS_REFUSED  (IP_ERR_DNS_MAX - DNS_ERROR_REFUSED)    // -205

#define DNS_FUNC_GET_ADDR   1   // Host name to host address translation
#define DNS_FUNC_GET_NAME   2   // Host address to host name translation
#define DNS_FUNC_LOOKUP     3   // General lookup

#define DNS_FLAG_UNICAST    0x00
#define DNS_FLAG_BROADCAST  0x01
#define DNS_FLAG_MULTICAST  0x02
#define DNS_FLAG_SOCKET     0x04    // Accessed from the socket layer

typedef struct DNSHeader
{
    u16             id;         // identifier
    u16             flags;      // QR | Opcode:4 | AA | TC | RD | RA | Z | AD | CD | Rcode:4
    u16             qdcount;    // # of questions
    u16             ancount;    // # of answer RRs
    u16             nscount;    // # of authority RRs
    u16             arcount;    // # of additional RRs
} DNSHeader;

typedef struct DNSInfo DNSInfo;
typedef void (* DNSCallback )(DNSInfo* info, s32 result);

struct DNSInfo
{
    UDPInfo         udp;
    IPSocket        socket;
    DNSCallback     callback;
    OSTime          rxmit;
    OSAlarm         alarm;
    u32             flag;       // DNS_FLAG_*

    u8              query[DNS_UDP_MAX];
    s32             queryLen;

    u8              response[DNS_UDP_MAX];
    s32             responseLen;

    vs32*           result;
    u16             id;         // transaction id

    int             func;       // current function (DNS_FUNC_*)

    u8*             data;
    s32             datalen;

    int             ref;
    OSThreadQueue   queueThread;
};

void DNSDumpPacket  ( DNSHeader* dns );
s32  DNSOpen        ( DNSInfo* info, const u8* addr );
s32  DNSGetAddr     ( DNSInfo* info, const char* name, u8* addr, s32 addrlen );
s32  DNSGetAddrAsync( DNSInfo* info, const char* name, u8* addr, s32 addrlen,
                      DNSCallback callback, s32* result );
s32  DNSGetName     ( DNSInfo* info, const u8* addr, char* name );
s32  DNSGetNameAsync( DNSInfo* info, const u8* addr, char* name,
                      DNSCallback callback, s32* result );
s32  DNSLookup      ( DNSInfo* info,
                      const u8* query, s32 queryLen, u8* response, s32 responseLen );
s32  DNSLookupAsync ( DNSInfo* info,
                      const u8* query, s32 queryLen, u8* response, s32 responseLen,
                      DNSCallback callback, s32* result );
s32  DNSClose       ( DNSInfo* info );

#ifdef __cplusplus
}
#endif

#endif  /* __IP_DNS_H__ */
