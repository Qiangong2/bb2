/*---------------------------------------------------------------------------*
  Project:  Dolphin IF API
  File:     IFQueue.h

  Copyright 2002 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: IFQueue.h,v $
  Revision 1.1.1.1  2004/03/02 05:31:13  paulm
  Socket Library 24-Sep-2003

    
    2     10/15/02 21:24 Shiki
    Enclosed with extern "C" { }.

    1     3/11/02 19:35 Shiki
    Initial check-in.
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#ifndef __IF_QUEUE_H__
#define __IF_QUEUE_H__

#ifndef NULL
#ifdef  __cplusplus
#define NULL                0
#else   // __cplusplus
#define NULL                ((void *)0)
#endif  // __cplusplus
#endif  // NULL

#ifdef __cplusplus
extern "C" {
#endif

typedef struct IFQueue  IFQueue, IFLink;

struct IFQueue
{
    IFQueue* next;   // next (or head) item
    IFQueue* prev;   // previous (or tail) item
};

// InitQueue - make queue empty
#define IFInitQueue(queue)          ((queue)->next = (queue)->prev = NULL)

// QueryQueueFirst - returns first item in the queue
#define IFQueryQueueHead(queue)     ((queue)->next)

// QueryQueueLast - returns last item in the queue
#define IFQueryQueueTail(queue)     ((queue)->prev)

// QueryQueuePrev- returns previous item in the queue
#define IFQueryLinkPrev(item, link) ((item)->link.prev)

// QueryQueueNext- returns next item in the queue
#define IFQueryLinkNext(item, link) ((item)->link.next)

// IsEmtptyQueue - tests whether a queue is empty
#define IFIsEmptyQueue(queue)       ((queue)->next == NULL)

// IsQueueEnd - tests whether the item is the end of the queue
#define IFIsQueueEnd(queue, item)   ((item) == NULL)

// EnqueueAfter - insert the item after previtem in the queue
#define IFEnqueueAfter(type, queue, previtem, item, link)           \
do {                                                                \
    (item)->link.prev = (IFQueue*) (previtem);                      \
    (item)->link.next = (previtem)->link.next;                      \
    (previtem)->link.next = (IFQueue*) (item);                      \
    if (IFIsQueueEnd(queue, (item)->link.next))                     \
        (queue)->prev = (IFQueue*) (item);                          \
    else                                                            \
        ((type) (item)->link.next)->link.prev = (IFQueue*) (item);  \
} while (0)

// EnqueueBefore - insert the item before afteritem in the queue
#define IFEnqueueBefore(type, queue, item, afteritem, link)         \
do {                                                                \
    (item)->link.prev = (afteritem)->link.prev;                     \
    (item)->link.next = (IFQueue*) (afteritem);                     \
    (afteritem)->link.prev = (IFQueue*) (item);                     \
    if (IFIsQueueEnd(queue, (item)->link.prev))                     \
        (queue)->next = (IFQueue*) (item);                          \
    else                                                            \
        ((type) (item)->link.prev)->link.next = (IFQueue*) (item);  \
} while (0)

// EnqueueTail - insert the item at the tail of the queue
#define IFEnqueueTail(type, queue, item, link)                      \
do {                                                                \
    register IFQueue* ___prev;                                      \
                                                                    \
    ___prev = (queue)->prev;                                        \
    if (IFIsQueueEnd(queue, ___prev))                               \
        (queue)->next = (IFQueue*) (item);                          \
    else                                                            \
        ((type) ___prev)->link.next = (IFQueue*) (item);            \
    (item)->link.prev = ___prev;                                    \
    (item)->link.next = NULL;                                       \
    (queue)->prev = (IFQueue*) item;                                \
} while (0)

// EnqueueHead - insert the item at the head of the queue
#define IFEnqueueHead(type, queue, item, link)                      \
do {                                                                \
    register IFQueue* ___next;                                      \
                                                                    \
    ___next = (queue)->next;                                        \
    if (IFIsQueueEnd(queue, ___next))                               \
        (queue)->prev = (IFQueue*) (item);                          \
    else                                                            \
        ((type) ___next)->link.prev = (IFQueue*) (item);            \
    (item)->link.next = ___next;                                    \
    (item)->link.prev = NULL;                                       \
    (queue)->next = (IFQueue*) item;                                \
} while (0)

// DequeueItem - remove the item form the queue
#define IFDequeueItem(type, queue, item, link)                      \
do {                                                                \
    register IFQueue* ___next;                                      \
    register IFQueue* ___prev;                                      \
                                                                    \
    ___next = (item)->link.next;                                    \
    ___prev = (item)->link.prev;                                    \
                                                                    \
    if (IFIsQueueEnd(queue, ___next))                               \
        (queue)->prev = ___prev;                                    \
    else                                                            \
        ((type) ___next)->link.prev = ___prev;                      \
                                                                    \
    if (IFIsQueueEnd(queue, ___prev))                               \
        (queue)->next = ___next;                                    \
    else                                                            \
        ((type) ___prev)->link.next = ___next;                      \
} while (0)

// IFDequeueHead - remove and return the item at the head of the queue
// note: item is returned by reference
#define IFDequeueHead(type, queue, item, link)                      \
do {                                                                \
    register IFQueue* ___next;                                      \
                                                                    \
    (item) = (type) IFQueryQueueHead(queue);                        \
    ___next = (item)->link.next;                                    \
                                                                    \
    if (IFIsQueueEnd(queue, ___next))                               \
        (queue)->prev = NULL;                                       \
    else                                                            \
        ((type) ___next)->link.prev = NULL;                         \
    (queue)->next = ___next;                                        \
} while (0)

// IFDequeueTail - remove and return the item at the tail of the queue
// note: item is returned by reference
#define IFDequeueTail(type, queue, item, link)                      \
do {                                                                \
    register IFQueue* ___prev;                                      \
                                                                    \
    (item) = (type) IFQueryQueueTail(queue);                        \
    ___prev = (item)->link.prev;                                    \
                                                                    \
    if (IFIsQueueEnd(queue, ___prev))                               \
        (queue)->next = NULL;                                       \
    else                                                            \
        ((type) ___prev)->link.next = NULL;                         \
    (queue)->prev = ___prev;                                        \
} while (0)

#define IFIterateQueue(type, queue, item, next, link)               \
    for ((item) = (type) IFQueryQueueHead(queue),                   \
         (next) = IFIsQueueEnd(queue, item) ? NULL : (type) IFQueryLinkNext(item, link);    \
         !IFIsQueueEnd(queue, item);                                 \
         (item) = (next),                                           \
         (next) = IFIsQueueEnd(queue, item) ? NULL : (type) IFQueryLinkNext(item, link))

#define IFIterateQueueReverse(type, queue, item, prev, link)        \
    for ((item) = (type) IFQueryQueueTail(queue),                   \
         (prev) = IFIsQueueEnd(queue, item) ? NULL : (type) IFQueryLinkPrev(item, link);    \
         !IFIsQueueEnd(queue, item);                                 \
         (item) = (prev),                                           \
         (prev) = IFIsQueueEnd(queue, item) ? NULL : (type) IFQueryLinkPrev(item, link))

#ifdef __cplusplus
}
#endif

#endif  /* __IF_QUEUE_H__ */
