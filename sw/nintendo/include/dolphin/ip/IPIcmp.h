/*---------------------------------------------------------------------------*
  Project:  Dolphin IP API
  File:     IPIcmp.h

  Copyright 2002 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: IPIcmp.h,v $
  Revision 1.1.1.1  2004/03/02 05:31:13  paulm
  Socket Library 24-Sep-2003

    
    4     4/04/02 17:14 Shiki
    Removed ip field from ICMP header structures.

    3     3/27/02 16:44 Shiki
    Added ICMPSourceQuench, ICMPTimeExceeded and ICMPParameterProblem.

    2     3/13/02 9:13 Shiki
    Clean up with IP_ALEN and ETH_ALEN.

    1     3/11/02 19:35 Shiki
    Initial check-in.
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#ifndef __IP_ICMP_H__
#define __IP_ICMP_H__

#ifdef __cplusplus
extern "C" {
#endif

/*
    Every ICMP error message includes the Internet header and at
    least the first 8 data octets of the datagram that triggered
    the error. [RFC 1122]
 */

typedef struct ICMPHeader
{
    u8       type;      // ICMP_*
    u8       code;      // code
    u16      sum;
} ICMPHeader;

typedef struct ICMPEcho
{
    u8       type;      // ICMP_ECHO_REPLY / ICMP_ECHO_REQUEST
    u8       code;      // code
    u16      sum;
    u16      id;
    u16      seq;
} ICMPEcho;

typedef struct ICMPUnreachable
{
    u8       type;      // ICMP_UNREACHABLE
    u8       code;      // code
    u16      sum;
    u16      unused;    // must be zero
    u16      mtu;       // RFC 1191
} ICMPUnreachable;

typedef struct ICMPSourceQuench
{
    u8       type;      // ICMP_SOURCE_QUENCH
    u8       code;
    u16      sum;
    u32      unused;    // must be zero
} ICMPSourceQuench;

typedef struct ICMPRedirect
{
    u8       type;      // ICMP_REDIRECT
    u8       code;
    u16      sum;
    u8       gateway[IP_ALEN];
} ICMPRedirect;

typedef struct ICMPTimeExceeded
{
    u8       type;      // ICMP_TIME_EXCEEDED
    u8       code;      // code (0: ttl reaches zero. 1: fragment timeout)
    u16      sum;
    u32      unused;    // must be zero
} ICMPTimeExceeded;

typedef struct ICMPParameterProblem
{
    u8       type;      // ICMP_PARAMETER_PROBLEM
    u8       code;
    u16      sum;
    u8       ptr;
    u8       unused0;
    u8       unused1;
    u8       unused2;
} ICMPParameterProblem;

#define ICMP_MIN_HLEN           4
#define ICMP_HLEN               8   // Normal ICMP message length

// ICMP message type
#define ICMP_ECHO_REPLY         0   // [MUST}
#define ICMP_UNREACHABLE        3
#define ICMP_SOURCE_QUENCH      4   // [Host MAY send, MUST report to xport layer]
#define ICMP_REDIRECT           5   // [Host SHOULD NOT send, MUST update its routing info]
#define ICMP_ECHO_REQUEST       8
#define ICMP_TIME_EXCEEDED      11  // [MUST be passed to xport layer]
#define ICMP_PARAMETER_PROBLEM  12  // [MUST be passed to xport layer]
#define ICMP_TIMESTAMP_REQUEST  13  // A host MAY implement Timestamp [RFC1122]
#define ICMP_TIMESTAMP_REPLY    14  // A host MAY implement Timestamp Reply [RFC1122]

// ICMP_REDIRECT codes
#define ICMP_REDIRECT_NET                   0   // Redirect for network error
#define ICMP_REDIRECT_HOST                  1   // Redirect for host error
#define ICMP_REDIRECT_TOSNET                2   // Redirect for TOS and network error
#define ICMP_REDIRECT_TOSHOST               3   // Redirect for TOS and host error

// ICMP_UNREACHABLE codes
#define ICMP_UNREACHABLE_NET                0   // hint
#define ICMP_UNREACHABLE_HOST               1   // hint
#define ICMP_UNREACHABLE_PROTOCOL           2   // protocol [Host SHOULD generate]
#define ICMP_UNREACHABLE_PORT               3   // port     [Host SHOULD generate]
#define ICMP_UNREACHABLE_NEED_FRAGMENT      4   // fragmentation needed and DF set
#define ICMP_UNREACHABLE_SRC_FAIL           5   // hint Bad source route
#define ICMP_UNREACHABLE_NET_UNKNOWN        6
#define ICMP_UNREACHABLE_HOST_UNKNOWN       7
#define ICMP_UNREACHABLE_ISOLATED           8   // obsolete
#define ICMP_UNREACHABLE_NET_PROHIBITED     9   // intended for use by U.S military agencies
#define ICMP_UNREACHABLE_HOST_PROHIBITED    10  // intended for use by U.S military agencies
#define ICMP_UNREACHABLE_NET_TOS            11
#define ICMP_UNREACHABLE_HOST_TOS           12
#define ICMP_UNREACHABLE_PROHIBITED         13

#ifdef __cplusplus
}
#endif

#endif  /* __IP_ICMP_H__ */
