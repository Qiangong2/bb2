/*---------------------------------------------------------------------------*
  Project:  Dolphin IP API
  File:     IPIgmp.h

  Copyright 2003 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: IPIgmp.h,v $
  Revision 1.1.1.1  2004/03/02 05:31:13  paulm
  Socket Library 24-Sep-2003

    
    2     4/01/03 10:19 Shiki
    Modified IP multicast logic so that a socket receives multicast
    datagrams only from the joined group.

    1     3/27/03 17:25 Shiki
    Initial check-in.
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#ifndef __IP_IGMP_H__
#define __IP_IGMP_H__

#include <dolphin/ip.h>

#ifdef __cplusplus
extern "C" {
#endif

//
// RFC 1112
//

// IGMP message type
#define IGMP_QUERY          1
#define IGMP_REPORT         2

#define IGMP_TYPE(igmp)     ((igmp)->vertype & 0x0f)
#define IGMP_VER(igmp)      ((igmp)->vertype >> 4)
#define IGMP_VERTYPE(v, t)  ((u8) ((v << 4) | (t & 0x0f)))

#define IGMP_DELAY          10  // [sec]
#define IGMP_LEN            8

typedef struct IGMP
{
    u8   vertype;           // Version(4):Type(4)
    u8   unused;
    u16  sum;               // Checksum
    u8   addr[IP_ALEN];     // Group Address
} IGMP;

s32 IPMulticastLookup( const u8* groupAddr, const u8* interface );
s32 IPMulticastJoin  ( const u8* groupAddr, const u8* interface );
s32 IPMulticastLeave ( const u8* groupAddr, const u8* interface );

//
// Private part
//

#define IGMP_MAXGROUP       4

typedef struct IGMPInfo
{
    u8      addr[IP_ALEN];
    u8      interface[IP_ALEN];
    OSAlarm alarm;
    s16     ref;
} IGMPInfo;

BOOL IPJoinLocalGroup ( IPInterface* interface, const u8* groupAddr );
BOOL IPLeaveLocalGroup( IPInterface* interface, const u8* groupAddr );
u16  IGMPCheckSum     ( IGMP* igmp );
IGMPInfo*
     IGMPLookupInfo   ( u8* groupAddr );
void IGMPIn           ( IPInterface* interface, IPHeader* ip, u32 flag );
void IGMPInit         ( IPInterface* interface );
BOOL IGMPOnReset      ( BOOL final );

#ifdef __cplusplus
}
#endif

#endif  /* __IP_IGMP_H__ */
