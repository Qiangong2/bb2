/*---------------------------------------------------------------------------*
  Project:  Dolphin IP API
  File:     IPPpp.h

  Copyright 2002 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: IPPpp.h,v $
  Revision 1.1.1.1  2004/03/02 05:31:13  paulm
  Socket Library 24-Sep-2003

    
    8     9/15/03 21:54 Shiki
    beta 3.7
    
    5     7/01/03 14:03 Shiki
    Restructured the PPP layer structure.
    
    3     10/01/02 9:59 Shiki
    Added const keywords to relevant function prototypes.

    2     8/01/02 17:04 Shiki
    Added PPPoEGetACName().

    1     7/31/02 16:12 Shiki
    Initial check-in.
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#ifndef __IP_PPP_H__
#define __IP_PPP_H__

#include <dolphin/ip.h>

#ifdef __cplusplus
extern "C" {
#endif

//
// PPP [RFC 1661]
//

#define PPP_LCP     0xc021  // Link Control Protocol
#define PPP_PAP     0xc023  // Password Authentication Protocol
#define PPP_LQR     0xc025  // Link Quality Report
#define PPP_CHAP    0xc223  // Challenge Handshake Authentication Protocol
#define PPP_IPCP    0x8021  // PPP Network Control Protocol (NCP) for IP
#define PPP_IP      0x0021  // Internet Protocol

//
// PPPoE [RFC 2516]
//

#define PPPoE_HLEN          6
#define PPPoE_VERTYPE       0x11
#define PPPoE_TAGLEN(tag)   (4u + (tag)->len)

typedef struct PPPoEHeader
{
    u8  vertype;    // version (0x01) and type (0x01)
    u8  code;       // code
    u16 session;    // session ID
    u16 len;        // length (not including ETH_HLEN or PPPoE_HLEN)
} PPPoEHeader;

typedef struct PPPoETag
{
    u16 type;
    u16 len;        // length (not including type and len)
} PPPoETag;

// PPPoEHeader.code
#define PPPoE_PADI          0x09    // PPPoE Active Discovery Initiation
#define PPPoE_PADO          0x07    // PPPoE Active Discovery Offer
#define PPPoE_PADR          0x19    // PPPoE Active Discovery Request
#define PPPoE_PADS          0x65    // PPPoE Active Discovery Session-confirmation
#define PPPoE_PADT          0xa7    // PPPoE Active Discovery Terminate
#define PPPoE_SESS          0x00    // PPPoE SESSion stage

// TAG_TYPES and TAG_VALUES
enum
{
   PPPoE_EndOfList =        0x0000,
   PPPoE_ServiceName =      0x0101,
   PPPoE_ACName =           0x0102,
   PPPoE_HostUniq =         0x0103,
   PPPoE_ACCookie =         0x0104,
   PPPoE_VendorSpecific =   0x0105,
   PPPoE_RelaySessionId =   0x0110,
   PPPoE_ServiceNameError = 0x0201,
   PPPoE_ACSystemError =    0x0202,
   PPPoE_GenericError =     0x0203
};

void PPPoEDumpPacket( PPPoEHeader* pppoe );
void PPPoETerminate ( IPInterface* interface );
int  PPPoEGetACName ( IPInterface* interface, char* acname );

#ifdef __cplusplus
}
#endif

#endif  /* __IP_PPP_H__ */
