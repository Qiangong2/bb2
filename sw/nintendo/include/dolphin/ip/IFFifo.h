/*---------------------------------------------------------------------------*
  Project:  Dolphin IF API
  File:     IFFifo.h

  Copyright 2002 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: IFFifo.h,v $
  Revision 1.1.1.1  2004/03/02 05:31:13  paulm
  Socket Library 24-Sep-2003

    
    4     8/05/02 10:44 Shiki
    Added IFRingInEx().

    3     4/10/02 18:35 Shiki
    Revised IFRingGet() interface.

    2     3/18/02 14:10 Shiki
    Clean up.

    1     3/11/02 19:35 Shiki
    Initial check-in.
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#ifndef __IF_FIFO_H__
#define __IF_FIFO_H__

#include <dolphin/ip.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct IFFifo
{
    u8* buff;
    s32 size;
    u8* head;
    s32 used;
} IFFifo;

typedef struct IFBlock
{
    u8* ptr;
    s32 len;
} IFBlock;

void  IFFifoInit ( IFFifo* fifo, void* buff, s32 size );
void* IFFifoAlloc( IFFifo* fifo, s32 len );
BOOL  IFFifoFree ( IFFifo* fifo, void* ptr, s32 len );

void  IFDump( void* ptr, s32 len );

u8*   IFRingIn  ( u8* buf, s32 size, u8* head, s32 used,
                  const u8* data, s32 len );
u8*   IFRingOut ( u8* buf, s32 size, u8* head, s32 used,
                  u8* data, s32 len );
int   IFRingGet ( u8* buf, s32 size, u8* head, s32 used,
                  IFVec* vec, s32 len );
u8*   IFRingPut ( u8* buf, s32 size, u8* head, s32 used,
                  s32 len );

u8*   IFRingInEx( u8* buf, s32 size, u8* head, s32 used,
                  s32 offset, const u8* data, s32* plen,
                  IFBlock* blocks, s32 maxblock );

#ifdef __cplusplus
}
#endif

#endif  /* __IF_QUEUE_H__ */
