/*---------------------------------------------------------------------------*
  Project:  Dolphin IP API -- UUIDs and GUIDs
  File:     IPUuid.h

  Copyright 2003 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  software derived from Network Working Group INTERNET-DRAFT
  <draft-leach-uuids-guids-01.txt>.

  $Log: IPUuid.h,v $
  Revision 1.1.1.1  2004/03/02 05:31:13  paulm
  Socket Library 24-Sep-2003

    
    5     9/15/03 21:54 Shiki
    beta 3.7
    
    3     8/25/03 9:19 Shiki
    Clean up.
    
    2     4/18/03 22:02 Shiki
    Added utility functions.

    1     4/03/03 18:20 Shiki
    Initial check-in.
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

/*
** Copyright (c) 1990- 1993, 1996 Open Software Foundation, Inc.
** Copyright (c) 1989 by Hewlett-Packard Company, Palo Alto, Ca. &
** Digital Equipment Corporation, Maynard, Mass.
** Copyright (c) 1998 Microsoft.
** To anyone who acknowledges that this file is provided "AS IS"
** without any express or implied warranty: permission to use, copy,
** modify, and distribute this file for any purpose is hereby
** granted without fee, provided that the above copyright notices and
** this notice appears in all source code copies, and that none of
** the names of Open Software Foundation, Inc., Hewlett-Packard
** Company, or Digital Equipment Corporation be used in advertising
** or publicity pertaining to distribution of the software without
** specific, written prior permission.  Neither Open Software
** Foundation, Inc., Hewlett-Packard Company, Microsoft, nor Digital Equipment
** Corporation makes any representations about the suitability of
** this software for any purpose.
*/

#ifndef __IP_UUID_H__
#define __IP_UUID_H__

#include <dolphin/types.h>

#ifdef __cplusplus
extern "C" {
#endif

#define IP_UUID_STR_LEN    37      // including terminating zero

typedef struct IPUuid
{
    u32     timeLow;
    u16     timeMid;
    u16     timeHiAndVersion;
    u8      clockSeqHiAndReserved;
    u8      clockSeqLow;
    u8      node[6];
} IPUuid;

void IPPrintUuid(const IPUuid* u);
const char* IPGetUuidString(const IPUuid* u, char* str);
int IPScanUuid(const char* str, IPUuid* u);

// Generate a UUID
int IPCreateUuid(IPUuid* uuid);
int IPCreateUuid4(IPUuid* uuid);

// Compare two UUID's "lexically"
int IPCompareUuid(const IPUuid* u1, const IPUuid* u2);

#ifdef __cplusplus
}
#endif

#endif  /* __IP_UUID_H__ */
