/*---------------------------------------------------------------------------*
  Project:  Dolphin IP API
  File:     IP.h

  Copyright 2002 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: IP.h,v $
  Revision 1.1.1.1  2004/03/02 05:31:13  paulm
  Socket Library 24-Sep-2003

    
    6     3/26/03 17:16 Shiki
    Defined IP_PROTO_IGMP.
    
    4     4/04/02 17:13 Shiki
    Clean up.

    3     3/25/02 15:07 Shiki
    Added IP_OPT_*.

    2     3/13/02 9:13 Shiki
    Clean up with IP_ALEN and ETH_ALEN.

    1     3/11/02 19:35 Shiki
    Initial check-in.
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#ifndef __IP_IP_H__
#define __IP_IP_H__

#include <dolphin/ip.h>

#ifdef __cplusplus
extern "C" {
#endif

//
// IP RFC 791
//

#ifndef IP_ALEN
#define IP_ALEN             4       // IP address length
#endif  // IP_ALEN

#define IP_VER              0x4     // IP version 4
#define IP_VERHLEN          0x45    // Default 20 bytes header

// Fragment information
#define IP_DF               0x4000  // Don't fragment
#define IP_MF               0x2000  // More fragments
#define IP_FO               0x1fff  // Fragment offset

#define IP_FRAG(ip)         (((ip)->frag & IP_FO) << 3)

#define IP_HLEN(ip)         (((ip)->verlen & 0x0f) << 2)
#define IP_MIN_HLEN         20
#define IP_MAX_HLEN         60

// Options
#define IP_OPT_EOOL         0x00    // End of Option List
#define IP_OPT_NOP          0x01    // No Operation
#define IP_OPT_SEC          0x82    // Security
#define IP_OPT_LSRR         0x83    // Loose Source and Record Route
#define IP_OPT_SSRR         0x89    // Strict Source and Record Route
#define IP_OPT_RR           0x07    // Record Route
#define IP_OPT_TS           0x44    // Internet Timestamp

// Protocol numbers
#define IP_PROTO_ICMP       1
#define IP_PROTO_IGMP       2
#define IP_PROTO_TCP        6
#define IP_PROTO_UDP        17

#define IP_CLASSA(x)        (((x)[0] & 0x80) == 0x00)
#define IP_CLASSB(x)        (((x)[0] & 0xc0) == 0x80)
#define IP_CLASSC(x)        (((x)[0] & 0xe0) == 0xc0)
#define IP_CLASSD(x)        (((x)[0] & 0xf0) == 0xe0)
#define IP_CLASSE(x)        (((x)[0] & 0xf8) == 0xf0)

#define IP_ADDR_EQ(a, b)        (*(u32*) (a) == *(u32*) (b))
#define IP_ADDR_NE(a, b)        (*(u32*) (a) != *(u32*) (b))
#define IP_ADDR_IN_NET(a, n, m) ((*(u32*) (a) & *(u32*) (m)) == (*(u32*) (n) & *(u32*) (m)))

typedef struct IPHeader
{
    u8   verlen;        // Version and header length
    u8   tos;           // Type of service
    u16  len;           // packet length
    u16  id;            // Identification
    u16  frag;          // Fragment information
    u8   ttl;           // Time to live
    u8   proto;         // Protocol
    u16  sum;           // Header checksum
    u8   src[IP_ALEN];  // IP source
    u8   dst[IP_ALEN];  // IP destination
} IPHeader;

#ifdef __cplusplus
}
#endif

#endif  /* __IP_IP_H__ */
