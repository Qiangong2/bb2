/*---------------------------------------------------------------------------*
  Project:  Dolphin IP API
  File:     IPTcp.h

  Copyright 2002, 2003 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: IPTcp.h,v $
  Revision 1.1.1.1  2004/03/02 05:31:13  paulm
  Socket Library 24-Sep-2003

    
    25    9/15/03 21:54 Shiki
    beta 3.7
    
    23    8/21/03 21:01 Shiki
    Added support for SACK/FACK.

    22    5/28/03 15:06 Shiki
    Defined TCP_MAX_PERSIST.
    
    20    11/11/02 11:06 Shiki
    Added TCPReceiveEx[Async]() and TCPReceiveUrgEx[Async]().

    19    8/05/02 10:46 Shiki
    Added abs[] and removed sendMss and recvMss to/from TCPInfo{}.
    Defined TCP_PMTUD_BACKOFF and TCP_ASB.

    18    7/22/02 13:36 Shiki
    Added TCP[Get/Set][Send/Recv]Buff().

    17    7/19/02 16:12 Shiki
    Added support for non-blocking I/O.

    16    7/18/02 10:15 Shiki
    Added TCPStat.

    15    7/17/02 18:27 Shiki
    Fixed TCP_MIN_RTT value not to be too aggressive. (maybe still too
    short)

    14    7/15/02 17:01 Shiki
    Added node field to TCPInfo{}.

    13    7/10/02 17:18 Shiki
    Added TCP_SO_PERSIST flag.

    12    4/30/02 14:48 Shiki
    Added TCPEnumInfoQueue() and TCPStateNames[] (for debugging).

    11    4/25/02 21:37 Shiki
    Revised TCPListen() interface with TCPListenCallback.

    10    4/23/02 15:16 Shiki
    Modified TCPBind() not to take reuse argument.
    Implemented linger timer.

    9     4/15/02 11:43 Shiki
    Defined TCP_R1.

    8     4/08/02 16:36 Shiki
    Added TCPGetSockOpt() and TCPSetSockOpt(). Clean up.

    7     4/05/02 14:08 Shiki
    Refined using 'const'.

    6     4/02/02 13:22 Shiki
    Added support for half-close and urgent mode.

    5     3/18/02 14:10 Shiki
    Clean up.

    4     3/14/02 16:07 Shiki
    Renamed TCPInfo.template to TCPInfo.header.

    3     3/13/02 21:34 Shiki
    Added TCPPeek().

    2     3/13/02 18:25 Shiki
    Modified to accommodate to variable length IFDatagram.vec[].

    1     3/11/02 19:35 Shiki
    Initial check-in.
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#ifndef __IP_TCP_H__
#define __IP_TCP_H__

#include <dolphin/ip.h>

#ifdef __cplusplus
extern "C" {
#endif

// Define TCP_SACK to use TCP Selective Acknowledgement Options [RFC 2018)]
#define TCP_SACK
#define TCP_FACK

//
// TCP RFC 793
//

typedef struct TCPHeader
{
    u16 src;    // source port
    u16 dst;    // destination port

    s32 seq;    // sequence number
    s32 ack;    // acknowledgment number

    u16 flag;   // 4:data offset, 6:reserved, 1:URG, 1:ACK, 1:PSH, 1:RST, 1:SYN, 1:FIN
    u16 win;    // window
    u16 sum;    // checksum
    u16 urg;    // urgent pointer

    // option (if any)

    // data (if any)

} TCPHeader;

#define TCP_HLEN(tcp)       ((tcp->flag & 0xf000) >> 10)
#define TCP_MIN_HLEN        20
#define TCP_MAX_HLEN        60

#define TCP_IIS_CLOCK       (1000000 / 4)   // incremented every 4 usec following RFC793.

#define TCP_FLAG_FIN        0x0001  // No more data from sender
#define TCP_FLAG_SYN        0x0002  // Synchronize sequence numbers
#define TCP_FLAG_RST        0x0004  // Reset the connection
#define TCP_FLAG_PSH        0x0008  // Push function
#define TCP_FLAG_ACK        0x0010  // Acknowledgment field significant
#define TCP_FLAG_URG        0x0020  // Urgent pointer field significant
#define TCP_FLAG_793        0x003f

#define TCP_FLAG_ECE        0x0040  // [RFC 3168]
#define TCP_FLAG_CWR        0x0080  // [RFC 3168]
#define TCP_FLAG_3168       0x00ff

#define TCP_FLAG_DATAOFFSET 0xf000

// Option kind
#define TCP_OPT_EOL         0       // End of option list
#define TCP_OPT_NOP         1       // No-Operation.
#define TCP_OPT_MSS         2       // Maximum Segment Size (length = 4, can only apper in SYNs)

// RFC 1323 TCP Extensions for High Performance
#define TCP_OPT_WS          3       // Window scale (length = 3)
#define TCP_OPT_TS          8       // Timestamps (length = 10)

// RFC 2018 TCP Selective Acknowledgement Options
#define TCP_OPT_SACKP       4       // Sack-Permitted (length = 2)
#define TCP_OPT_SACK        5       // Sack (length = variable)

#define TCP_DEF_SSTHRESH    65535           // default slow start threshold value
#define TCP_MSL             120             // maximum segment lifetime in second
#define TCP_MIN_RTT         200             // minimum value of restransmission timer in millisecond
#define TCP_MAX_RTT         (2 * TCP_MSL)   // maximum value of restransmission timer in second
#define TCP_DEF_RTT         3               // default RTT in second
#define TCP_R1              3               // at least 3 retransmissions [RFC 1122]
#define TCP_PMTUD_BACKOFF   4               // Path MTU discovery blackhole detection
#define TCP_MAX_BACKOFF     16              // Maximum retransmission count allowed.
                                            // Must be more than R1, add less than 31.
#define TCP_MAX_PERSIST     (TCP_MAX_BACKOFF * TCP_MAX_RTT)
                                            // maximum idle time in persist state
#define TCP_RXMIT_THRESH    3               // Fast restransmission threshold

// XXX Lower bound must be larger than delayed ACK time + RTT

// a < b
#define TCP_SEQ_GT(a, b)    (0 < ((b) - (a)))

// a <= b
#define TCP_SEQ_GE(a, b)    (0 <= ((b) - (a)))

// max(a, b)
#define TCP_SEQ_MAX(a, b)   (TCP_SEQ_GT((a), (b)) ? (b) : (a))

// min(a, b)
#define TCP_SEQ_MIN(a, b)   (TCP_SEQ_GT((a), (b)) ? (a) : (b))
//
// Implementation specific define names
//

#define TCP_STATE_CLOSED        0
#define TCP_STATE_LISTEN        1
#define TCP_STATE_SYN_SENT      2
#define TCP_STATE_SYN_RECEIVED  3
#define TCP_STATE_ESTABLISHED   4
#define TCP_STATE_FINWAIT1      5
#define TCP_STATE_FINWAIT2      6
#define TCP_STATE_CLOSE_WAIT    7
#define TCP_STATE_CLOSING       8
#define TCP_STATE_LAST_ACK      9
#define TCP_STATE_TIME_WAIT     10

#define TCP_SHUTDOWN_RECEIVE    0   // Disallow further receives
#define TCP_SHUTDOWN_SEND       1   // Disallow further sends
#define TCP_SHUTDOWN_BOTH       2   // Disallow further receives and sends

// TCPInfo.flag
#define TCP_SO_CLOSED           0x00000001
#define TCP_SO_NAGLE            0x00000002
#define TCP_SO_ACK_DELAYED      0x00000004
#define TCP_SO_SH_SEND          0x00000008
#define TCP_SO_SH_RECEIVE       0x00000010
#define TCP_SO_HAVE_URG         0x00000020
#define TCP_SO_HAD_URG          0x00000040
#define TCP_SO_OOB              0x00000080
#define TCP_SO_PASSIVE          0x00000100
#define TCP_SO_PERSIST          0x00000200
#define TCP_SO_PEEK             0x00000400
#define TCP_SO_PEEK_URG         0x00000800
#define TCP_SO_FAST_RXMIT       0x00001000      // Set when fast retransmit algorithm is triggered
#define TCP_SO_SACK             0x00002000      // Set if SACK enabled
#define TCP_SO_REUSE            0x00010000
#define TCP_SO_LINGER           0x00020000

#define TCP_ASB                 4   // 8 * TCP_ASB + 2 < 40

typedef struct TCPInfo          TCPInfo;
typedef struct TCPSackHole      TCPSackHole;

typedef void (* TCPCallback )(TCPInfo* info, s32 result);
typedef BOOL (* TCPListenCallback )(TCPInfo* info, s32 result);

struct TCPSackHole
{
    s32             start;     // start seq no. of hole
    s32             end;       // end seq no.
    int             dupAcks;   // number of dup acks for this hole
    s32             rxmit;     // next seq. no in hole to be retransmitted
};

struct TCPInfo
{
    //
    // Note do not access members without API functions
    //

    IPInfo          pair;
    OSThreadQueue   queueThread;
    IPInterface*    interface;
    s32             err;

    // Send Sequence Variables
    s32             sendUna;    // send unacknowledged
    s32             sendNext;   // send next
    s32             sendWin;    // send window
    s32             sendUp;     // send urgent pointer
    s32             sendWL1;    // segment sequence number used for last window update
    s32             sendWL2;    // segment acknowledgment number used for a window update
    s32             iss;        // initial send sequence number
    s32             sendMaxWin; // the maximum send window size so far [Max(SND.WND) RFC1122]

    s32             sendMax;    // send max

    // Receive Sequence Variables
    s32             recvNext;   // receive next
    s32             recvWin;    // receive window
    s32             recvUp;     // receive urgent pointer
    s32             irs;        // initial receive sequence number

    // Current Segment Variables
    s32             segLen;     // segment length (counting SYN and FIN)

    u8*             segBegin;   // trimed

    // SACK/FACK [RFC 2018]
    IFBlock         asb[TCP_ASB];           // above sequence blocks received
    TCPSackHole     scoreboard[TCP_ASB];    // list of non-SACKed holes
    int             sendHoles;              // # of holes in scoreboard
    s32             sendFack;               // for FACK congestion control
    s32             sendAwin;               // sendNext - sendFack + rxmitData
    s32             rxmitData;              // amount of outstanding rxmit data
    s32             sendRecover;
    s32             lastSack;

    //
    // Implementations specific
    //

    s32             state;
    u32             flag;
    TCPCallback     closeCallback;
    s32*            closeResult;


    s32             mss;        // maximum segment size

    // Output
    vs32            sendBusy;
    u8              header[IP_MAX_HLEN + TCP_MAX_HLEN];   // 120 bytes
    u8*             sendData;
    s32             sendBuff;   // size of sendData in bytes
    u8*             sendPtr;
    s32             sendLen;
    IFDatagram      datagram;
    IFVec           vec[3];

    // Output (user)
    TCPCallback     sendCallback;
    s32*            sendResult;
    s32             userAcked;
    u8*             userSendData;
    s32             userSendLen;
    OSTime          lastSend;   // time when the last packet was sent used when
                                // re-starting the idle connection.

    // Input
    u8*             recvData;
    s32             recvBuff;   // size of recvData in bytes
    s32             recvUser;   // not yet consumed
    u8*             recvPtr;
    s32             recvAcked;
    s32             dupAcks;    // # of duplicated ACKs received.

    // Input (user)
    TCPCallback     recvCallback;
    s32*            recvResult;
    u8*             userData;
    s32             userBuff;   // size of userData in bytes
    s32             userLen;    // size of bytes filled in userData

    // Urgent mode
    u8              oob;
    s32             recvUrg;    // size of recvData before URG mark in bytes
    TCPCallback     urgCallback;
    s32*            urgResult;
    u8*             urgData;

    // Retransmission
    s32             rxmitCount; // Count of consecutive resransmissions
    OSTime          rto;        // Retransmission timeout value
    OSTime          r0;         //
    OSTime          r2;         // When transmissions reaches R2, close the connection. [RFC 1122 4.2.3.5]
    OSAlarm         rxmitAlarm; // restransmision timer

    // Slow start/Congestion avoidance
    s32             cWin;       // Congestion window size. The congestion
                                // window is a count of how many bytes will
                                // fit in the pipe.
    s32             ssThresh;   // Slow start threshold.

    // Round trip time
    BOOL            rttTiming;  // TRUE if rttSeq is valid to measure RTT value.
    s32             rttSeq;     //
    OSTime          rtt;
    OSTime          srtt;       // soothed round trip time estimate
    OSTime          rttDe;      // smoothed mean deviation estimator
    OSTime          rttMin;
    OSTime          rttMax;

    // Listen backlog
    TCPInfo*        listening;      // listening socket
    IPSocket*       local;
    IPSocket*       remote;
    IFQueue         queueListen;
    IFLink          linkListen;

    TCPCallback     openCallback;   // For connection establishment
    s32*            openResult;

    // Linger
    int             linger;         // linger time in seconds
    OSAlarm         lingerAlarm;

    //
    // Socket layer specific
    //

    // Listen backlog
    TCPInfo*        logging;
    IFQueue         queueBacklog;   // queue of incomplete connections
    IFQueue         queueCompleted; // queue of completed connections
    IFLink          linkLog;
    s32             accepting;      // # of accept calls
    void*           node;           // Pointer to SONode
};

// TCP statistics
typedef struct TCPStatistics
{
    u32             sendTotal;
    u32             recvTotal;
    u32             rxmitTimeout;
    u32             rxmitPackets;
    u32             rxmitBytes;
} TCPStatistics;

void TCPDumpHeader       ( const IPHeader* ip, const TCPHeader* tcp );
void TCPEnumInfoQueue    ( TCPCallback callback );

s32  TCPSetTimeWaitBuffer( void* buffer, s32 len );

s32  TCPBind             ( TCPInfo* info, const IPSocket* socket );

s32  TCPConnect          ( TCPInfo* info, const IPSocket* remote );
s32  TCPConnectAsync     ( TCPInfo* info, const IPSocket* remote,
                           TCPCallback callback, s32* result );

s32  TCPOpen             ( TCPInfo* info,
                           void* sendbuf, s32 sendbufLen,
                           void* recvbuf, s32 recvbufLen );

s32  TCPCancel           ( TCPInfo* info );

s32  TCPListen           ( TCPInfo* listening,
                           IPSocket* local, IPSocket* remote,
                           TCPListenCallback callback, s32* result );

s32  TCPAccept           ( TCPInfo* info, TCPInfo* listening );
s32  TCPAcceptAsync      ( TCPInfo* info, TCPInfo* listening,
                           TCPCallback callback, s32* result );

s32  TCPSend             ( TCPInfo* info, const void* data, s32 len );
s32  TCPSendAsync        ( TCPInfo* info, const void* data, s32 len,
                           TCPCallback callback, s32* result );
s32  TCPSendNonblock     ( TCPInfo* info, const void* data, s32 len );
s32  TCPSendUrg          ( TCPInfo* info, const void* data, s32 len );
s32  TCPSendUrgAsync     ( TCPInfo* info, const void* data, s32 len,
                           TCPCallback callback, s32* result );
s32  TCPSendUrgNonblock  ( TCPInfo* info, const void* data, s32 len );

s32  TCPReceiveExAsync   ( TCPInfo* info, void* data, s32 len, u32 flag,
                           TCPCallback callback, s32* result );
s32  TCPReceiveEx        ( TCPInfo* info, void* data, s32 len, u32 flag );

s32  TCPReceiveUrgExAsync( TCPInfo* info, void* data, s32 len, u32 flag,
                           TCPCallback callback, s32* result );
s32  TCPReceiveUrgEx     ( TCPInfo* info, void* data, s32 len, u32 flag );

s32  TCPReceive          ( TCPInfo* info, void* data, s32 len );
s32  TCPReceiveNonblock  ( TCPInfo* info, void* data, s32 len );
s32  TCPReceiveAsync     ( TCPInfo* info, void* data, s32 len,
                           TCPCallback callback, s32* result );
s32  TCPReceiveUrg       ( TCPInfo* info, void* data, s32 len );
s32  TCPReceiveUrgNonblock( TCPInfo* info, void* data, s32 len );
s32  TCPReceiveUrgAsync  ( TCPInfo* info, void* data, s32 len,
                           TCPCallback callback, s32* result );

s32  TCPPeek             ( TCPInfo* info, void* data, s32 len );
s32  TCPPeekUrg          ( TCPInfo* info, void* data, s32 len );

s32  TCPGetUrgOffset     ( TCPInfo* info );

s32  TCPClose            ( TCPInfo* info );
s32  TCPCloseAsync       ( TCPInfo* info,
                           TCPCallback callback, s32* result );

s32  TCPShutdown         ( TCPInfo* info, u32 flag );

s32  TCPGetStatus        ( TCPInfo* info );

s32  TCPGetRemoteSocket  ( TCPInfo* info, IPSocket* socket );
s32  TCPGetLocalSocket   ( TCPInfo* info, IPSocket* socket );

s32  TCPGetSockOpt       ( TCPInfo* info, int level, int optname,
                           void* optval, int* optlen );
s32  TCPSetSockOpt       ( TCPInfo* info, int level, int optname,
                           const void* optval, int optlen );

s32  TCPSetTimeout       ( TCPInfo* info, OSTime threshold );

s32  TCPGetSendBuff      ( TCPInfo* info, void** sendbuf, s32* sendbufLen );
s32  TCPSetSendBuff      ( TCPInfo* info, void* sendbuf, s32 sendbufLen );
s32  TCPGetRecvBuff      ( TCPInfo* info, void** recvbuf, s32* recvbufLen );
s32  TCPSetRecvBuff      ( TCPInfo* info, void* recvbuf, s32 recvbufLen );

//
// Obsolete functions...
//
s32  TCPSetOption        ( TCPInfo* info, u8 ttl, u8 tos );
s32  TCPSetUrgInLine     ( TCPInfo* info, BOOL inLine );
s32  TCPControlNagle     ( TCPInfo* info, BOOL enable );

extern char*         TCPStateNames[];
extern TCPStatistics TCPStat;

#ifdef __cplusplus
}
#endif

#endif  /* __IP_TCP_H__ */
