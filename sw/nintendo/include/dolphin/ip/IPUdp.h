/*---------------------------------------------------------------------------*
  Project:  Dolphin IP API
  File:     IPUdp.h

  Copyright 2002, 2003 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: IPUdp.h,v $
  Revision 1.1.1.1  2004/03/02 05:31:13  paulm
  Socket Library 24-Sep-2003

    
    14    1/30/03 18:33 Shiki
    Added support for UDP send buffering.

    13    1/23/03 17:23 Shiki
    Revised UDPInfo receive buffer structure using ring buffer.

    12    1/22/03 18:07 Shiki
    Added UDP_SO_REUSE.

    10    11/11/02 11:05 Shiki
    Added UDPReceiveEx[Async]().

    9     7/22/02 14:46 Shiki
    Added UDP[Get/Set]RecvBuff().

    8     7/19/02 16:12 Shiki
    Added support for non-blocking I/O.

    7     4/30/02 14:48 Shiki
    Added UDPPeek().

    6     4/08/02 14:02 Shiki
    Added UDPGetSockOpt() and UDPSetSockOpt().

    5     4/05/02 14:08 Shiki
    Refined using 'const'.

    4     3/22/02 9:21 Shiki
    Modified so UDPInfo can save multiple UDP datagrams in its receive
    buffer.

    3     3/14/02 16:15 Shiki
    Renamed UDPInfo.buffer to UDPInfo.header.

    2     3/13/02 18:25 Shiki
    Modified to accommodate to variable length IFDatagram.vec[].

    1     3/11/02 19:35 Shiki
    Initial check-in.
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#ifndef __IP_UDP_H__
#define __IP_UDP_H__

#include <dolphin/ip.h>

#ifdef __cplusplus
extern "C" {
#endif

//
// UDP RFC 768
//

#define UDP_HLEN            8

typedef struct UDPHeader
{
    u16 src;    // source port number
    u16 dst;    // destination port number
    u16 len;    // length
    u16 sum;    // check sum
} UDPHeader;

// UDPInfo.flag
#define UDP_SO_PEEK         0x00000400
#define UDP_SO_REUSE        0x00010000

typedef struct UDPInfo UDPInfo;

typedef void (* UDPCallback )(UDPInfo* info, s32 result);

struct UDPInfo
{
    //
    // Note do not access members without API functions
    //

    IPInfo          pair;
    OSThreadQueue   queueThread;
    u32             flag;

    // Send info
    UDPCallback     sendCallback;
    s32*            sendResult;
    IFDatagram      datagram;
    IFVec           vec[1];
    u8              header[IP_MAX_HLEN + UDP_HLEN];

    // Recv info
    UDPCallback     recvCallback;
    s32*            recvResult;
    void*           data;
    s32             len;
    IPSocket*       local;
    IPSocket*       remote;

    // Recv buffer
    u8*             recvRing;   // ring buffer
    s32             recvBuff;   // size of ring buffer
    u8*             recvPtr;    // head
    s32             recvUsed;   // used bytes

    // Send buffer (keeps just one datagram for ARP)
    u8*             sendData;
    s32             sendBuff;
    s32             sendUsed;
};

s32 UDPGetRemoteSocket( UDPInfo* info, IPSocket* socket );
s32 UDPGetLocalSocket ( UDPInfo* info, IPSocket* socket );

s32 UDPBind           ( UDPInfo* info, const IPSocket* local );
s32 UDPConnect        ( UDPInfo* info, const IPSocket* remote );
s32 UDPSendAsync      ( UDPInfo* info, const void* data, s32 len,
                        const IPSocket* remote,
                        UDPCallback callback, s32* result );
s32 UDPSend           ( UDPInfo* info, const void* data, s32 len,
                        const IPSocket* remote );
s32 UDPPeek           ( UDPInfo* info, void* data, s32 len,
                        IPSocket* local, IPSocket* remote );
s32 UDPReceiveAsync   ( UDPInfo* info, void* data, s32 len,
                        IPSocket* local, IPSocket* remote,
                        UDPCallback callback, s32* result );
s32 UDPReceiveNonblock( UDPInfo* info, void* data, s32 len,
                        IPSocket* local, IPSocket* remote );
s32 UDPReceive        ( UDPInfo* info, void* data, s32 len,
                        IPSocket* local, IPSocket* remote);
s32 UDPReceiveExAsync ( UDPInfo* info, void* data, s32 len,
                        IPSocket* local, IPSocket* remote, u32 flag,
                        UDPCallback callback, s32* result );
s32 UDPReceiveEx      ( UDPInfo* info, void* data, s32 len,
                        IPSocket* local, IPSocket* remote, u32 flag );
s32 UDPOpen           ( UDPInfo* info, void* heap, s32 heapLen );
s32 UDPClose          ( UDPInfo* info );
s32 UDPSetOption      ( UDPInfo* info, u8 ttl, u8 tos );
s32 UDPCancel         ( UDPInfo* info );

s32 UDPGetSockOpt     ( UDPInfo* info, int level, int optname,
                        void* optval, int* optlen );
s32 UDPSetSockOpt     ( UDPInfo* info, int level, int optname,
                        const void* optval, int optlen );

s32 UDPGetRecvBuff    ( UDPInfo* info, void** recvbuf, s32* recvbufLen );
s32 UDPSetRecvBuff    ( UDPInfo* info, void* recvbuf, s32 recvbufLen );

s32 UDPGetSendBuff    ( UDPInfo* info, void** sendbuf, s32* sendbufLen );
s32 UDPSetSendBuff    ( UDPInfo* info, void* sendbuf, s32 sendbufLen );


#ifdef __cplusplus
}
#endif

#endif  /* __IP_UDP_H__ */
