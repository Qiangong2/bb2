/*---------------------------------------------------------------------------*
  Project:  Dolphin OS IP API
  File:

  Copyright 2002, 2003 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: ip.h,v $
  Revision 1.1.1.1  2004/03/02 05:31:13  paulm
  Socket Library 24-Sep-2003

    
    36    9/05/03 14:32 Shiki
    beta 3.7
    
    34    7/01/03 14:03 Shiki
    Restructured the PPP layer structure.

    33    6/03/03 16:56 Shiki
    Declared IFMute().

    32    5/28/03 17:06 Shiki
    Added inFilter and outFilter to IPInterface{}.

    31    5/15/03 15:51 Shiki
    Added flag member to IFDatagram{} for IF_DATAGRAM_FLAG_DROP (test
    purpose only).

    30    5/03/03 12:57 Shiki
    Implemented IPInterfaceStat{} support functions.

    29    4/28/03 20:57 Shiki
    Declared IPAutoStop().

    28    4/18/03 21:56 Shiki
    Declared IPAutoConfig().

    27    4/03/03 18:24 Shiki
    Included <dolphin/ip/IPUuid.h>.

    26    4/01/03 10:19 Shiki
    Modified IP multicast logic so that a socket receives multicast
    datagrams only from the joined group.

    25    3/27/03 17:25 Shiki
    Added support for multicast.
    
    24    3/07/03 10:41 Shiki
    Defined IP_ERR_LINK_DOWN.

    23    1/22/03 18:08 Shiki
    Added SOPoll() interface.

    22    11/11/02 10:32 Shiki
    Renumbered IP_ERR_AGAIN error code.

    21    10/07/02 10:24 Shiki
    Added host configuration error definition.

    20    8/01/02 17:04 Shiki
    Fixed IFDatagram{} to make vec[1] be the last member.

    19    7/31/02 16:13 Shiki
    Added support for PPP.

    18    7/19/02 16:12 Shiki
    Added support for non-blocking I/O.

    17    02/07/01 13:20 Shiki
    Added type field to IPInterface{}.

    16    6/13/02 11:31 Shiki
    Added IPSetMtu().

    15    4/26/02 16:46 Shiki
    Added check sum error counters to IPInterface{}.

    14    4/25/02 9:39 Shiki
    Added tx and rx to IPInterface{}.

    13    4/24/02 13:40 Shiki
    Added IPGetLinkState().

    12    4/23/02 21:16 Shiki
    Defined IP_ERR_NETDOWN.
    Added 'up' field to IPInterface{}.

    11    4/18/02 18:16 Shiki
    Fixed typo in IP_ERR_INS_RESOURCES.

    10    4/18/02 9:06 Shiki
    Defined IP_ERR_INV_OPTION.

    9     4/17/02 20:15 Shiki
    Defined IP_ERR_SOURCE_QUENCH.

    8     4/15/02 11:41 Shiki
    Added IPRecoverGateway().

    7     4/08/02 13:28 Shiki
    Added 'len' field to IPSocket.

    6     4/05/02 14:02 Shiki
    Added 'family' field to IPSocket.

    5     4/04/02 17:13 Shiki
    Added IPSetReassemblyBuffer().

    4     3/25/02 15:07 Shiki
    Added IP_ERR_DNS_*.

    3     3/22/02 9:01 Shiki
    Included <dolphin/ip/IFFifo.h>.

    2     3/13/02 9:13 Shiki
    Added IP_ALEN and ETH_ALEN.

    1     3/11/02 19:35 Shiki
    Initial check-in.
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#ifndef __IP_H__
#define __IP_H__

#include <dolphin/types.h>
#include <dolphin/ip/IFQueue.h>
#include <string.h>

#ifdef __cplusplus
extern "C" {
#endif

#define IF_MAX_VEC                  4       // for header, data 1, data 2
#define IF_FLAG_UNICAST             0x00
#define IF_FLAG_BROADCAST           0x01
#define IF_FLAG_MULTICAST           0x02
#define IF_FLAG_LOOPBACK            0x04

#define IF_TYPE_AUTO                0
#define IF_TYPE_100FULL             1
#define IF_TYPE_100HALF             2
#define IF_TYPE_10FULL              3
#define IF_TYPE_10HALF              4
#define IF_TYPE_NONE                5       // loopback only

// Interface family
#define IP_INET                     2       // Internet protocol

#define IP_ERR_NONE                 0       // Ok
#define IP_ERR_BUSY                 -1      // Busy
#define IP_ERR_UNREACHABLE          -2      // Networks is unreachable
#define IP_ERR_RESET                -3      // Connection reset
#define IP_ERR_NOT_EXIST            -4      // Connection does not exist
#define IP_ERR_EXIST                -5      // Connection already exists
#define IP_ERR_SOCKET_UNSPECIFIED   -6      // Foreign socket unspecified
#define IP_ERR_INS_RESOURCES        -7      // Insufficient resources
#define IP_ERR_CLOSING              -8      // Connection closing
#define IP_ERR_AGAIN                -9      // Would block
#define IP_ERR_TIMEOUT              -10     // Timeout
#define IP_ERR_REFUSED              -11     // Connection refused
#define IP_ERR_INVALID              -12
#define IP_ERR_INV_SOCKET           -13
#define IP_ERR_INV_OPTION           -14
//
#define IP_ERR_CANCELED             -16
#define IP_ERR_DATASIZE             -17
#define IP_ERR_SOURCE_QUENCH        -18     // IPv4 only
#define IP_ERR_NETDOWN              -19     // local interface down

// IPGetConfigError()
#define IP_ERR_DHCP_TIMEOUT         -100    // Could not find any DHCP server.
#define IP_ERR_DHCP_EXPIRED         -101
#define IP_ERR_DHCP_NAK             -102
#define IP_ERR_PPPoE_TIMEOUT        -103    // Could not find any access concentrator.
#define IP_ERR_PPPoE_SERVICE_NAME   -104    // The requested Service-Name request could not be honored.
#define IP_ERR_PPPoE_AC_SYSTEM      -105    // The access concentrator experienced some error.
#define IP_ERR_PPPoE_GENERIC        -106    // An unrecoverable error occured.
#define IP_ERR_LCP                  -107    // LCP negotiation error
#define IP_ERR_AUTH                 -108    // Authentication error
#define IP_ERR_IPCP                 -109    // IPCP negotiation error
#define IP_ERR_PPP_TERMINATED       -110    // The access concentrator terminated the PPPoE session.
#define IP_ERR_ADDR_COLLISION       -111    // Duplicate IP address
#define IP_ERR_LINK_DOWN            -112    // Link down

#define IP_ERR_DNS_MAX              -200
#define IP_ERR_DNS_MIN              (IP_ERR_DNS_MAX - 15)

#ifndef ETH_ALEN
#define ETH_ALEN                    6       // Ethernet address length
#endif  // ETH_ALEN

#ifndef IP_ALEN
#define IP_ALEN                     4       // IP address length
#endif  // IP_ALEN

#define IP_SOCKLEN                  8

typedef void (* IFCallback )(void* info, s32 result);

typedef struct IPInterface      IPInterface;
typedef struct IFDatagram       IFDatagram;
typedef struct IFVec            IFVec;
typedef struct IPInterfaceConf  IPInterfaceConf;
typedef struct IPInterfaceStat  IPInterfaceStat;

struct IFVec
{
    void*           data;
    s32             len;
};

#define IF_DATAGRAM_FLAG_DROP   1       // Force drop

struct IFDatagram
{
    IPInterface*    interface;
    IFQueue*        queue;
    IFLink          link;

    // Ethernet specific
    u16             type;        // ETH_ARP, ETH_IP, etc.
    u8              hwAddr[ETH_ALEN];

    // Internet specific
    u8              dst[IP_ALEN];

    // PPP
    u8              prefix[8];
    u8              prefixLen;
    u8              flag;

    IFCallback      callback;
    void*           param;
    s32             nVec;

    // Note: 'vec' must be the last field of IFDatagram{} as vec[2, 3...] are also used.
    IFVec           vec[1];     // up to IF_MAX_VEC
};

struct IPInterfaceStat
{
    u32             inUcastPackets;     // Input unicast packets
    u32             inNonUcastPackets;  // Input broadcast or multicast packets
    u32             inDiscards;         // Input dropped due to ins. resources
    u32             inErrors;           // Packets dropped on input
    u32             outUcastPackets;    // Output unicast packets
    u32             outNonUcastPackets; // Output broadcast or multicast packets
    u32             outDiscards;        // Output dropped due to ins. resources
    u32             outErrors;          // Output errors
    u32             outCollisions;      // Collisions on CSMA
};

struct IPInterface
{
    s32             type;
    BOOL            up;
    s32             err;        // the last error code of the interface
    OSAlarm         gratuitousAlarm;

    u8              mac[ETH_ALEN];
    s32             mtu;        // 1500 bytes for Ether

    // IP
    u8              addr[IP_ALEN];
    u8              netmask[IP_ALEN];
    u8              broadcast[IP_ALEN];
    u8              gateway[IP_ALEN];
    u8              alias[IP_ALEN];

    // PPP
    IFQueue         ppp;        // LCP, { PAP , CHAP }, IPCP stack

    // Output support functions
    void         (* out    )(IPInterface* interface, IFDatagram* datagram);
    void         (* cancel )(IPInterface* interface, IFDatagram* datagram);
    void*        (* alloc  )(IPInterface* interface, s32 len);              // output FIFO allocator
    BOOL         (* free   )(IPInterface* interface, void* ptr, s32 len);   // output FIFO deallocator

    // Packet filter
    BOOL         (* inFilter  )(IPInterface* interface, void* frame, s32 len);
    BOOL         (* outFilter )(IPInterface* interface, void* frame, s32 len);

    // Address configuration request queue
    IFQueue         queue;

    // Statistics
    IPInterfaceStat stat;
};

struct IPInterfaceConf
{
    IPInterface*    interface;
    IFLink          link;

    u8              addr[IP_ALEN];
    OSAlarm         alarm;
    s32             count;
    IFCallback      callback;
};

typedef struct IPSocket
{
    u8              len;        // IP_SOCKLEN
    u8              family;     // IP_INET
    u16             port;
    u8              addr[IP_ALEN];
} IPSocket;

#define IP_INFO_FLAG_MCAST(n)   (0x0001 << (n))
#define IP_INFO_FLAG_MCASTLOOP  0x8000

typedef struct IPInfo
{
    u8              proto;      // Protocol
    u8              ttl;        // Time to live
    u8              tos;        // Type of service
    u8              mttl;       // Multicast time to live

    s16             poll;       // SOPoll() count
    u16             flag;

    IPSocket        local;      // Bind
    IPSocket        remote;     // Connect

    IFLink          link;       // Link of IPInfo
} IPInfo;

BOOL IFInit( s32 type );
BOOL IFMute( BOOL mute );
void IFDump( void* ptr, s32 len );      // Generic HEX dump function

s32  IPSetReassemblyBuffer( void* buffer, s32 len, s32 mtu );

BOOL IPAutoConfig      ( void );
void IPAutoStop        ( void );

void IPGetMacAddr      ( IPInterface* interface, u8* macAddr );
void IPGetLinkState    ( IPInterface* interface, BOOL* up );

void IPGetInterfaceStat( IPInterface* interface, IPInterfaceStat* stat);
void IPClearInterfaceStat
                       ( IPInterface* interface );

void IPSetMtu          ( IPInterface* interface, s32 mtu );
void IPGetMtu          ( IPInterface* interface, s32* mtu );

void IPSetBroadcastAddr( IPInterface* interface, const u8* broadcastAddr );
void IPGetBroadcastAddr( IPInterface* interface, u8* broadcastAddr );
void IPSetNetmask      ( IPInterface* interface, const u8* netmask );
void IPGetNetmask      ( IPInterface* interface, u8* netmask );
void IPSetAddr         ( IPInterface* interface, const u8* addr );
void IPGetAddr         ( IPInterface* interface, u8* addr );
void IPSetGateway      ( IPInterface* interface, const u8* gateway );
void IPGetGateway      ( IPInterface* interface, u8* addr );
void IPSetAlias        ( IPInterface* interface, const u8* alias );
void IPGetAlias        ( IPInterface* interface, u8* alias );

s32  IPGetConfigError  ( IPInterface* interface );
s32  IPSetConfigError  ( IPInterface* interface, s32 err );
s32  IPClearConfigError( IPInterface* interface );

BOOL  IPAtoN( const char* dottedDecimal, u8* addr );
char* IPNtoA( const u8* addr );

void  IPPrintAddr        ( u8* addr );
void  IPPrintRoutingTable( void );

BOOL  IPInitRoute     ( const u8* addr, const u8* netmask, const u8* gateway );
BOOL  IPAddRoute      ( const u8* dst,  const u8* netmask, const u8* gateway );
BOOL  IPRemoveRoute   ( const u8* dst,  const u8* netmask, const u8* gateway );
BOOL  IPRefreshRoute  ( void );
BOOL  IPRecoverGateway( const u8* dst );

#include <dolphin/ip/IFFifo.h>
#include <dolphin/ip/IFEther.h>
#include <dolphin/ip/IP.h>
#include <dolphin/ip/IPIcmp.h>
#include <dolphin/ip/IPIgmp.h>
#include <dolphin/ip/IPUdp.h>
#include <dolphin/ip/IPTcp.h>
#include <dolphin/ip/IPDns.h>
#include <dolphin/ip/IPDhcp.h>
#include <dolphin/ip/IPUuid.h>

extern const u8 IPAddrAny[IP_ALEN];           // 0.  0.  0.  0
extern const u8 IPLoopbackAddr[IP_ALEN];      // 127.0.  0.  1
extern const u8 IPLimited[IP_ALEN];           // 255.255.255.255

#ifdef __cplusplus
}
#endif

#endif // __IP_H__
