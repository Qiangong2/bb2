/*---------------------------------------------------------------------------*
  Project:  MDM API
  File:     mdm_country.h

  Copyright 2002 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law. They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: mdm_country.h,v $
  Revision 1.1.1.1  2004/03/02 05:30:12  paulm
  Network Base Package 05-Aug-2003

    
    4     03/03/03 9:31 Ikedae
    
    3     02/12/10 17:07 Ikedae
    deleted countries which are not in Europe
    
    2     02/12/10 16:43 Ikedae
    added 3 countries


  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#ifndef __MDM_CC_H__
#define __MDM_CC_H__

// country code
#define MDM_CC_JAPAN            "00"
#define MDM_CC_US               "B5"

#define MDM_CC_AUSTRIA          "0A"
#define MDM_CC_BELGIUM          "0F"
#define MDM_CC_DENMARK          "31"
#define MDM_CC_FINLAND          "3C"
#define MDM_CC_FRANCE           "3D"
#define MDM_CC_GERMANY          "42"
#define MDM_CC_GREECE           "46"
#define MDM_CC_ICELAND          "31"
#define MDM_CC_IRELAND          "57"
#define MDM_CC_ITALY            "59"
#define MDM_CC_LIECHTENSTEIN    "7B"
#define MDM_CC_LUXEMBOURG       "7B"
#define MDM_CC_NETHERLANDS      "7B"
#define MDM_CC_PORTUGAL         "8B"
#define MDM_CC_SPAIN            "A0"
#define MDM_CC_SWITZERLAND      "A6"
#define MDM_CC_UNITEDKINGDOM    "B4"

#endif // __MDM_CC_H__
