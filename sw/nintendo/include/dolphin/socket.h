/*---------------------------------------------------------------------------*
  Project: Dolphin OS Socket API
  File:    socket.h

  Copyright 2002, 2003 Nintendo. All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law. They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: socket.h,v $
  Revision 1.1.1.1  2004/03/02 05:31:13  paulm
  Socket Library 24-Sep-2003

    
    23    4/18/03 21:57 Shiki
    Added rdhcp member to SOConfig{}.

    22    3/27/03 17:25 Shiki
    Added support for multicast.

    21    3/07/03 10:42 Shiki
    Added SOInit().

    20    1/30/03 18:33 Shiki
    Added SO_MEM_UDP_SENDBUF.

    19    1/22/03 18:08 Shiki
    Added SOPoll() interface.

    18    11/11/02 21:57 Shiki
    Added support for SO_MSG_DONTWAIT.

    17    10/17/02 13:19 Shiki
    Fixed SOGetHostByAddr() function prototype.
    Added SOInetPtoN() and SOInetNtoP().

    16    10/07/02 10:25 Shiki
    Revised SOSetResolver()/SOGetResolver() prototypes.

    15    10/01/02 9:59 Shiki
    Added const keywords to relevant function prototypes.

    14    8/01/02 11:26 Shiki
    Integrated PPPoE. Added SOSetResolver() and SOGetResolver().

    13    7/22/02 13:36 Shiki
    Added SO_SO_SNDBUF and SO_SO_RCVBUF.

    12    7/19/02 16:12 Shiki
    Added support for non-blocking I/O.

    11    7/18/02 15:24 Shiki
    Added SOConfig.r2

    10    7/15/02 13:48 Shiki
    Modified to be SO_EAGAIN == SO_EWOULDBLOCK

    9     02/07/01 15:33 Shiki
    Added mtu and rwin field to SOConfig{}.

    8     4/30/02 14:49 Shiki
    Added SOGetHostID().

    7     4/26/02 15:49 Shiki
    Revised SOConfig{} structure.

    6     4/18/02 17:56 Shiki
    Defined SO_E*.

    5     4/17/02 11:02 Shiki
    Added dns1 and dns2 field to SOResolver{}.

    4     4/17/02 10:01 Shiki
    Added SOResolver{}.

    3     4/15/02 11:42 Shiki
    Disabled SONtoHl() and other macros for WIN32 build.

    2     4/12/02 18:48 Shiki
    Added SO_PF_UNSPEC.

    1     4/08/02 16:35 Shiki
    Initial check-in.
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#ifndef __DOL_SOCKET_H__
#define __DOL_SOCKET_H__

#include <dolphin/os.h>
#include <dolphin/ip.h>

#ifdef __cplusplus
extern "C" {
#endif

#define SO_VENDOR_NINTENDO  0x0000      // Nintendo TCP/IP stack
#define SO_VERSION          0x0100      // Version 1.0

#define SO_PF_UNSPEC        0           // Unspecified
#define SO_PF_INET          2           // ARPA Internet protocols

#define SO_INET_ADDRSTRLEN  16

#define SO_SOCK_STREAM      1           // stream socket
#define SO_SOCK_DGRAM       2           // datagram socket

#define SO_MSG_OOB          0x01        // send or receive out-of-band data
#define SO_MSG_PEEK         0x02        // take data but leave it
#define SO_MSG_DONTWAIT     0x04        // non-block operation

// Socket option levels
#define SO_SOL_SOCKET       0xffff      // options for socket level
#define SO_SOL_IP           0
#define SO_SOL_ICMP         1
#define SO_SOL_TCP          6
#define SO_SOL_UDP          17

#define SO_INADDR_ANY               ((u32) 0x00000000)  // 0.0.0.0
#define SO_INADDR_BROADCAST         ((u32) 0xffffffff)  // 255.255.255.255
#define SO_INADDR_LOOPBACK          ((u32) 0x7f000001)  // 127.0.0.1
#define SO_INADDR_UNSPEC_GROUP      ((u32) 0xe0000000)  // 224.0.0.0
#define SO_INADDR_ALLHOSTS_GROUP    ((u32) 0xe0000001)  // 224.0.0.1
#define SO_INADDR_MAX_LOCAL_GROUP   ((u32) 0xe00000ff)  // 224.0.0.255

// SOGetSockOpt() / SOSetSockOpt()
#define SO_IP_TOS               0x00000007  // int
#define SO_IP_TTL               0x00000008  // int
#define SO_IP_MULTICAST_LOOP    0x00000009  // unsigned char
#define SO_IP_MULTICAST_TTL     0x0000000a  // unsigned char
#define SO_IP_ADD_MEMBERSHIP    0x0000000b  // SOIpMreq
#define SO_IP_DROP_MEMBERSHIP   0x0000000c  // SOIpMreq
#define SO_SO_REUSEADDR         0x00000004  // BOOL
#define SO_SO_LINGER            0x00000080  // SOLinger
#define SO_SO_OOBINLINE         0x00000100  // BOOL
#define SO_SO_SNDBUF            0x00001001  // int
#define SO_SO_RCVBUF            0x00001002  // int
#define SO_SO_TYPE              0x00001008  // int
#define SO_TCP_NODELAY          0x00002001  // BOOL
#define SO_TCP_MAXSEG           0x00002002  // int

// SOShutdown()
#define SO_SHUT_RD          0
#define SO_SHUT_WR          1
#define SO_SHUT_RDWR        2

// SOFcntl()
#define SO_F_GETFL          3
#define SO_F_SETFL          4
#define SO_O_NONBLOCK       0x04

// SOPoll()
#define SO_POLLRDNORM       0x0001  // Normal data read
#define SO_POLLRDBAND       0x0002  // Priority data read
#define SO_POLLPRI          0x0004  // High priority data read
#define SO_POLLWRNORM       0x0008  // Normal data write
#define SO_POLLWRBAND       0x0010  // Priority data write
#define SO_POLLERR          0x0020  // Error (revents only)
#define SO_POLLHUP          0x0040  // Disconnected (revents only)
#define SO_POLLNVAL         0x0080  // Invalid fd (revents only)
#define SO_POLLIN           (SO_POLLRDNORM | SO_POLLRDBAND)
#define SO_POLLOUT          SO_POLLWRNORM

#define SO_INFTIM           -1

#define SO_E2BIG            -1
#define SO_EACCES           -2
#define SO_EADDRINUSE       -3      // Address is already in use
#define SO_EADDRNOTAVAIL    -4
#define SO_EAFNOSUPPORT     -5      // Non-supported address family
#define SO_EAGAIN           -6      // Posix.1
#define SO_EALREADY         -7      // Already in progress
#define SO_EBADF            -8      // Bad socket descriptor
#define SO_EBADMSG          -9
#define SO_EBUSY            -10
#define SO_ECANCELED        -11
#define SO_ECHILD           -12
#define SO_ECONNABORTED     -13     // Connection aborted
#define SO_ECONNREFUSED     -14     // Connection refused
#define SO_ECONNRESET       -15     // Connection reset
#define SO_EDEADLK          -16
#define SO_EDESTADDRREQ     -17     // Not bound to a local address
#define SO_EDOM             -18
#define SO_EDQUOT           -19
#define SO_EEXIST           -20
#define SO_EFAULT           -21
#define SO_EFBIG            -22
#define SO_EHOSTUNREACH     -23
#define SO_EIDRM            -24
#define SO_EILSEQ           -25
#define SO_EINPROGRESS      -26
#define SO_EINTR            -27     // Canceled
#define SO_EINVAL           -28     // Invalid operation
#define SO_EIO              -29     // I/O error
#define SO_EISCONN          -30     // Socket is already connected
#define SO_EISDIR           -31
#define SO_ELOOP            -32
#define SO_EMFILE           -33     // No more socket descriptors
#define SO_EMLINK           -34
#define SO_EMSGSIZE         -35     // Too large to be sent
#define SO_EMULTIHOP        -36
#define SO_ENAMETOOLONG     -37
#define SO_ENETDOWN         -38
#define SO_ENETRESET        -39
#define SO_ENETUNREACH      -40     // Unreachable
#define SO_ENFILE           -41
#define SO_ENOBUFS          -42     // Insufficient resources
#define SO_ENODATA          -43
#define SO_ENODEV           -44
#define SO_ENOENT           -45
#define SO_ENOEXEC          -46
#define SO_ENOLCK           -47
#define SO_ENOLINK          -48
#define SO_ENOMEM           -49     // Insufficient memory
#define SO_ENOMSG           -50
#define SO_ENOPROTOOPT      -51     // Non-supported option
#define SO_ENOSPC           -52
#define SO_ENOSR            -53
#define SO_ENOSTR           -54
#define SO_ENOSYS           -55
#define SO_ENOTCONN         -56     // Not connected
#define SO_ENOTDIR          -57
#define SO_ENOTEMPTY        -58
#define SO_ENOTSOCK         -59     // Not a socket
#define SO_ENOTSUP          -60
#define SO_ENOTTY           -61
#define SO_ENXIO            -62
#define SO_EOPNOTSUPP       -63     // Non-supported operation
#define SO_EOVERFLOW        -64
#define SO_EPERM            -65
#define SO_EPIPE            -66
#define SO_EPROTO           -67
#define SO_EPROTONOSUPPORT  -68     // Non-supported protocol
#define SO_EPROTOTYPE       -69     // Non-supported socket type
#define SO_ERANGE           -70
#define SO_EROFS            -71
#define SO_ESPIPE           -72
#define SO_ESRCH            -73
#define SO_ESTALE           -74
#define SO_ETIME            -75
#define SO_ETIMEDOUT        -76     // Timed out
#define SO_ETXTBSY          -77
#define SO_EWOULDBLOCK      SO_EAGAIN   // Posix.1g
#define SO_EXDEV            -78

// Name for SOConfig.alloc() and SOConfig.free()
#define SO_MEM_TCP_INFO         0
#define SO_MEM_TCP_SENDBUF      1
#define SO_MEM_TCP_RECVBUF      2
#define SO_MEM_UDP_INFO         3
#define SO_MEM_UDP_SENDBUF      4
#define SO_MEM_UDP_RECVBUF      5
#define SO_MEM_TIMEWAITBUF      6
#define SO_MEM_REASSEMBLYBUF    7

// Flags for SOConfig{}.flag
#define SO_FLAG_DHCP            0x0001  // Use DHCP
#define SO_FLAG_PPPoE           0x0002  // Use PPPoE
#ifndef SO_FLAG_ZEROCONF
#define SO_FLAG_ZEROCONF        0x8000  // Use AutoIP
#endif  // SO_FLAG_ZEROCONF

typedef struct SOSockAddr
{
    u8          len;
    u8          family;
    u8          data[6];
} SOSockAddr;

typedef struct SOInAddr
{
    u32         addr;
} SOInAddr;

typedef struct SOSockAddrIn
{
    u8          len;            // size of socket address structure
    u8          family;         // the address family
    u16         port;           // the port number
    SOInAddr    addr;           // the Internet address
} SOSockAddrIn;

typedef struct SOLinger
{
    int         onoff;          // zero=off, nonzero = on
    int         linger;         // linger time in seconds
} SOLinger;

typedef struct SOIpMreq
{
   SOInAddr     multiaddr;      // IP address of group
   SOInAddr     interface;      // IP address of interface
} SOIpMreq;

typedef struct SOPollFD
{
    int         fd;
    short       events;         // input event flags
    short       revents;        // output event flags
} SOPollFD;

typedef struct SOHostEnt
{
    char*       name;           // official name of host
    char**      aliases;        // alias list (zero-terminated)
    s16         addrType;       // always SO_PF_INET
    s16         length;         // length of address
    u8**        addrList;       // list of addresses
} SOHostEnt;

// 35 aliases, 35 addr, 8192 buffer
typedef struct SOResolver
{
    DNSInfo     info;
    SOHostEnt   ent;
    char        name[DNS_NAME_MAX];
    char*       zero;
    u8          addrList[IP_ALEN * 35];
    u8*         ptrList[36];

    u8          dns1[IP_ALEN];
    u8          dns2[IP_ALEN];
} SOResolver;

typedef struct SOConfig
{
    u16         vendor;             // SO_VENDOR_NINTENDO
    u16         version;            // SO_VERSION

    //
    // vendor specific section
    //
    void*    (* alloc )(u32 name, s32 size);
    void     (* free ) (u32 name, void* ptr, s32 size);

    u32         flag;               // ORed SO_FLAG_*
    SOInAddr    addr;
    SOInAddr    netmask;
    SOInAddr    router;
    SOInAddr    dns1;
    SOInAddr    dns2;

    s32         timeWaitBuffer;     // time wait buffer size
    s32         reassemblyBuffer;   // reassembly buffer size

    s32         mtu;                // maximum transmission unit size
    s32         rwin;               // default TCP receive window size
    OSTime      r2;                 // default TCP total retransmit timeout value

    // PPP
    const char* peerid;
    const char* passwd;
    const char* serviceName;        // currently not used
    const char* acName;             // currently not used

    // DHCP
    s32         rdhcp;              // TCP total retransmit times (default 4)
} SOConfig;

u32   (SONtoHl)    ( u32 netlong   );
u16   (SONtoHs)    ( u16 netshort  );
u32   (SOHtoNl)    ( u32 hostlong  );
u16   (SOHtoNs)    ( u16 hostshort );

#ifndef WIN32
#define SONtoHl(netlong)        ((u32) (netlong))
#define SONtoHs(netshort)       ((u16) (netshort))
#define SOHtoNl(hostlong)       ((u32) (hostlong))
#define SOHtoNs(hostshort)      ((u16) (hostshort))
#endif

int   SOInetAtoN   ( const char* cp, SOInAddr* inp );
char* SOInetNtoA   ( SOInAddr in );

int   SOInetPtoN   ( int af, const char* src, void* dst );
const
char* SOInetNtoP   ( int af, const void* src, char* dst, unsigned len );

int   SOSocket     ( int af, int type, int protocol );
int   SOClose      ( int s );
int   SOShutdown   ( int s, int how );

int   SOBind       ( int s, const void* sockAddr );
int   SOConnect    ( int s, const void* sockAddr );
int   SOGetPeerName( int s, void* sockAddr );
int   SOGetSockName( int s, void* sockAddr );

int   SORead       ( int s, void* buf, int len );
int   SORecv       ( int s, void* buf, int len, int flags );
int   SORecvFrom   ( int s, void* buf, int len, int flags,
                     void* sockFrom );

int   SOWrite      ( int s, const void* buf, int len );
int   SOSend       ( int s, const void* buf, int len, int flags );
int   SOSendTo     ( int s, const void* buf, int len, int flags,
                     const void* sockTo );

int   SOAccept     ( int s, void* sockAddr );
int   SOListen     ( int s, int backlog );

int   SOSockAtMark ( int s );

int   SOGetSockOpt ( int s, int level, int optname, void* optval, int* optlen );
int   SOSetSockOpt ( int s, int level, int optname,
                     const void* optval, int optlen );

int   SOFcntl      ( int s, int cmd, ... );

int   SOPoll       ( SOPollFD fds[], unsigned nfds, OSTime timeout );

SOHostEnt* SOGetHostByAddr( const void* addr, int len, int type );
SOHostEnt* SOGetHostByName( const char* name );

long  SOGetHostID  ( void );

void  SOInit       ( void );
int   SOStartup    ( const SOConfig* config );
int   SOCleanup    ( void );

int   SOSetResolver( const SOInAddr* dns1, const SOInAddr* dns2 );
int   SOGetResolver( SOInAddr* dns1, SOInAddr* dns2 );

#ifdef __cplusplus
}
#endif

#endif // __DOL_SOCKET_H__
