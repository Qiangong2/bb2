/*---------------------------------------------------------------------------*
  Project:  VM
  File:     VMMapping.h
 
  Copyright 2002 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: VMMapping.h,v $
  Revision 1.1.1.1  2004/04/20 05:29:07  paulm
  Virtual Memory library from Nintendo

 * 
 * 4     11/08/02 4:54p Stevera
 * 
 * 3     4/29/02 11:46a Stevera
 * RC2 for testing
 *     
  
  $NoKeywords: $

 *---------------------------------------------------------------------------*/

#ifndef __VMMAPPING_H__
#define __VMMAPPING_H__

#ifdef __cplusplus
extern "C" {
#endif


/*---------------------------------------------------------------------------*
  Includes
 *---------------------------------------------------------------------------*/

#include <dolphin/types.h>


/*---------------------------------------------------------------------------*
    function prototypes
 *---------------------------------------------------------------------------*/

// Allocation of virtual memory (must alloc ranges before using)
BOOL VMAlloc( u32 virtualAddr, u32 size );
BOOL VMFree( u32 virtualAddr, u32 size );
void VMFreeAll( void );
u32  VMGetNumUnallocatedBytes( void );


#ifdef __cplusplus
}
#endif

#endif // __VMMAPPING_H__ 
