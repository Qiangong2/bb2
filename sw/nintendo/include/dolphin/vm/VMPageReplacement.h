/*---------------------------------------------------------------------------*
  Project:  VM
  File:     VMPageReplacement.h
 
  Copyright 2002 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: VMPageReplacement.h,v $
  Revision 1.1.1.1  2004/04/20 05:29:07  paulm
  Virtual Memory library from Nintendo

 * 
 * 1     5/02/02 1:50p Stevera
 * 
  
  $NoKeywords: $

 *---------------------------------------------------------------------------*/

#ifndef __VMPAGEREPLACEMENT_H__
#define __VMPAGEREPLACEMENT_H__

#ifdef __cplusplus
extern "C" {
#endif

/*---------------------------------------------------------------------------*
  Includes
 *---------------------------------------------------------------------------*/
#include <dolphin/types.h>


/*---------------------------------------------------------------------------*
  Definitions & declarations
 *---------------------------------------------------------------------------*/
typedef enum {
	VM_PRP_LRU,
	VM_PRP_RANDOM,
	VM_PRP_FIFO
} VM_PageReplacementPolicy;



/*---------------------------------------------------------------------------*
    function prototypes
 *---------------------------------------------------------------------------*/
void VMSetPageReplacementPolicy( VM_PageReplacementPolicy policy );
VM_PageReplacementPolicy VMGetPageReplacementPolicy( void );


#ifdef __cplusplus
}
#endif

#endif // __VMPAGEREPLACEMENT_H__ 
