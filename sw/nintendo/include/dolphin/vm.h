/*---------------------------------------------------------------------------*
  Project:  VM
  File:     vm.h
 
  Copyright 2002 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: vm.h,v $
  Revision 1.1.1.1  2004/04/20 05:29:07  paulm
  Virtual Memory library from Nintendo

 * 
 * 13    10/02/02 4:18p Stevera
 * Added functions VMQuit and VMIsInitialized.
 * 
 * 12    9/24/02 2:49p Stevera
 * Renamed function VMWriteBackOneDirtyPageSync to VMStoreOnePage.
 * 
 * 11    9/17/02 6:19p Stevera
 * Added function VMInvalidateRange.
 * 
 * 10    9/16/02 4:36p Stevera
 * Renamed VMFlushAllPages to VMStoreAllPages in order to use correct
 * terminology.
 * 
 * 9     5/06/02 4:07p Stevera
 * Added VMWriteBackOneDirtyPageSync function.
 * 
 * 8     5/02/02 3:46p Stevera
 * Added a public header to expose the page replacement functions.
 * 
 * 7     5/01/02 2:58p Stevera
 * Removed stats tracking from VM library. Added optional stats logging
 * callback.
 * 
 * 6     4/29/02 11:46a Stevera
 * RC2 for testing
 * 
  
  $NoKeywords: $

 *---------------------------------------------------------------------------*/

#ifndef __VM_H__
#define __VM_H__

#ifdef __cplusplus
extern "C" {
#endif


/*---------------------------------------------------------------------------*
  Includes
 *---------------------------------------------------------------------------*/
#include <dolphin/types.h>
#include <dolphin/vm/VMMapping.h>
#include <dolphin/vm/VMPageReplacement.h>


/*---------------------------------------------------------------------------*
  Definitions & declarations
 *---------------------------------------------------------------------------*/

//This is a callback function definition to log stats
typedef void (*VM_LogStats)		(	u32 realVirtualAddress,
									u32 physicalAddress, 
									u32 pageNumber,
									u32 pageMissLatency,
									BOOL pageSwappedOut );


/*---------------------------------------------------------------------------*
    function prototypes
 *---------------------------------------------------------------------------*/

// VM initialization
void VMInit( u32 sizeVMMainMemory,		//size of main memory VM
             u32 baseVMARAM,			//base address of ARAM VM
             u32 sizeVMARAM );			//size of ARAM VM

BOOL VMIsInitialized( void );

void VMQuit( void );
          

void VMResizeARAM( u32 baseVMARAM,		//base address of ARAM VM
             	   u32 sizeVMARAM );	//size of ARAM VM


void VMSetLogStatsCallback( VM_LogStats cbLogStats );


// Query settings for ARAM
u32  VMGetARAMSize( void );
u32  VMGetARAMBase( void );

// Control over paged memory
void VMStoreAllPages( void );
BOOL VMStoreOnePage( void );
void VMInvalidateAllPages( void );
void VMInvalidateRange( u32 virtualAddress, u32 range );



#ifdef __cplusplus
}
#endif

#endif // __VM_H__ 
