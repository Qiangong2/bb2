/*---------------------------------------------------------------------------*
  Project:  VMBase
  File:     vmbase.h
 
  Copyright 2002 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: vmbase.h,v $
  Revision 1.1.1.1  2004/04/20 05:29:07  paulm
  Virtual Memory library from Nintendo

 * 
 * 13    10/02/02 4:18p Stevera
 * Added function VMBASEQuit.
 * 
 * 12    9/23/02 5:44p Stevera
 * Removed the functions VMBASEGetVirtualAddressFromPageTableSlot and
 * VMBASEGetNumSlotsInPageTable. Their functionality was redundant.
 * 
 * 11    9/16/02 4:36p Stevera
 * Renamed VMBASEFlushAllPages to VMBASEStoreAllPages in order to use
 * correct terminology.
 * 
 * 10    9/10/02 6:27p Stevera
 * Changed function name from VMBASEInvalidatePageTable() to
 * VMBASEInvalidateAllPages(). Renamed function
 * VMBASEGetVirtualAddressFromPageInMRAM() to
 * VMBASEGetVirtualAddrFromPageInMRAM().
 * 
 * 9     9/10/02 4:51p Stevera
 * Changed function call name from VMBASEFlushAllPagesInternal() to
 * VMBASEFlushAllPages().
 * 
 * 8     9/10/02 4:29p Stevera
 * Renamed the argument in VMBASEInit to match the name in vmbase.c.
 * 
 * 7     9/05/02 1:11p Stevera
 * Changed the prefix of all functions from VM to VMBASE.
 * 
 * 6     5/07/02 11:48a Stevera
 * Renamed a function argument.
 * 
 * 5     5/03/02 2:10p Stevera
 * Added functions to walk the page table and modify dirty/referenced
 * bits.
 * 
 * 4     4/30/02 3:13p Stevera
 * Moved functionality out of VMFlushAllPagesInternal.
 * 
 * 3     4/29/02 11:46a Stevera
 * RC2 for testing
 *     
  
  $NoKeywords: $

 *---------------------------------------------------------------------------*/

#ifndef __VMBASE_H__
#define __VMBASE_H__

#ifdef __cplusplus
extern "C" {
#endif


/*---------------------------------------------------------------------------*
  Includes
 *---------------------------------------------------------------------------*/
#include <dolphin/types.h>


/*---------------------------------------------------------------------------*
  Definitions & declarations
 *---------------------------------------------------------------------------*/
#define VM_PAGE_SIZE 0x1000					//4K

//This is a callback function definition to swap a page to MRAM
typedef void (*VM_SwapPageIn)    ( u32 exactVirtualAddress );

//This is a callback function definition to swap a page out to ARAM (when on a flush)
typedef void (*VM_SwapPageOut)    ( u32 virtualAddress );


/*---------------------------------------------------------------------------*
    function prototypes
 *---------------------------------------------------------------------------*/

//Init function
void VMBASEInit( VM_SwapPageIn cbSwapPageIn );

//Quit function
void VMBASEQuit( void );

//Global control over paged memory
void VMBASEStoreAllPages( VM_SwapPageOut cbSwapPageOut );
void VMBASEInvalidateAllPages( void );

//Page table translations
u32 VMBASEGetPhysicalAddrInMRAM( u32 virtualAddress );
u32 VMBASEGetVirtualAddrFromPageInMRAM( u32 pageNumber );

//Page table queries
BOOL VMBASEIsPageValid( u32 virtualAddress );
BOOL VMBASEIsPageDirty( u32 virtualAddress );
BOOL VMBASEIsPageReferenced( u32 virtualAddress );
BOOL VMBASEIsPageLocked( u32 pageNumber );

//Page table management
//(Note: These functions transparently take care of the 
//TLB and other data structures required for bookkeeping)
void VMBASEClearPageTableEntry( u32 virtualAddress, u32 pageNumber );
void VMBASESetPageTableEntry( u32 virtualAddress, u32 physicalAddress, u32 pageNumber );
void VMBASESetPageDirty( u32 virtualAddress, BOOL dirty );
void VMBASESetPageReferenced( u32 virtualAddress, BOOL referenced );
void VMBASESetPageLocked( u32 pageNumber, BOOL locked );


#ifdef __cplusplus
}
#endif

#endif // __VMBASE_H__ 
