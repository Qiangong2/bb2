/*---------------------------------------------------------------------------*
  Project: OS - Low Level L2 Cache Operations Library
  File:    OSL2.h

  Copyright 1998, 1999 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: OSL2.h,v $
  Revision 1.1.1.1  2004/06/09 17:51:00  paulm
  GC include from Nintendo SDK

    
    3     5/11/99 4:43p Shiki
    Refreshed include tree.

    1     4/30/99 12:49p Tianli01

    1     3/04/99 2:23p Tianli01
    Initial checkin to new tree
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

/*
    This header file defines the low level L2 cache operations.
    Most should be macros to the underlying assembly routines, but
    others will manipulate hardware setup registers.
 */

#ifndef __OSL2_H__
#define __OSL2_H__

#include <dolphin/types.h>

#ifdef __cplusplus
extern "C" {
#endif

void L2Enable           ( void );
void L2Disable          ( void );
void L2GlobalInvalidate ( void );
void L2SetDataOnly      ( BOOL dataOnly );
void L2SetWriteThrough  ( BOOL writeThrough );

#ifdef __cplusplus
}
#endif

#endif  // __OSL2_H__
