/*---------------------------------------------------------------------------*
  Project:  Dolphin OS Reset API
  File:     OSReset.h

  Copyright 2001-2003 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: OSReset.h,v $
  Revision 1.1.1.1  2004/06/09 17:51:00  paulm
  GC include from Nintendo SDK

    
    16    7/22/03 14:42 Shiki
    5/8/2003 SDK Patch 1
    
    15    7/10/03 14:38 Shiki
    Defined OS_RESET_PRIO_ALARM.

    14    03/04/16 22:32 Hashida
    Added OSSetBootDol.

    13    3/12/03 11:28 Shiki
    Defined OS_RESET_PRIO_SO and OS_RESET_PRIO_IP.

    12    2/13/03 14:24 Shiki
    Defined OS_RESET_TIMEOUT.

    11    02/11/19 14:55 Hirose
    Added OS_RESET_PRIO_GX.

    10    02/10/28 20:28 Hashida
    Added OSIsRestart().

    9     02/04/11 17:59 Hashida
    Added OSGetSavedRegion

    8     9/15/01 3:31a Hashida
    Added OS_RESET_SHUTDOWN

    7     9/07/01 5:30a Hashida
    Added OSSet/GetSaveRegion.

    6     7/18/01 10:55p Hashida
    Changed definitions.

    5     7/11/01 10:19p Hashida
    Added code definition for restarting.

    4     5/17/01 8:20p Shiki
    Modified priority from s32 to u32.

    3     5/17/01 7:58p Shiki
    Revised.

    2     01/04/23 16:31 Shiki
    Added menu param to OSResetSystem().

    1     01/04/09 13:52 Shiki
    Initial check-in.
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#ifndef __OSRESET_H__
#define __OSRESET_H__

#include <dolphin/types.h>

#ifdef __cplusplus
extern "C" {
#endif

#define OS_RESETCODE_RESTART    0x80000000      // not by cold/hot reset

#define OS_RESET_TIMEOUT        OSMillisecondsToTicks(1000)

#define OS_RESET_RESTART        0
#define OS_RESET_HOTRESET       1
#define OS_RESET_SHUTDOWN       2

#define OS_RESET_PRIO_SO        110     // SO API
#define OS_RESET_PRIO_IP        111     // IP API
#define OS_RESET_PRIO_CARD      127     // CARD API
#define OS_RESET_PRIO_PAD       127     // PAD API
#define OS_RESET_PRIO_GX        127     // GX API
#define OS_RESET_PRIO_ALARM     4294967295  // OSAlarm

typedef BOOL (* OSResetFunction )(BOOL final);
typedef struct OSResetFunctionInfo OSResetFunctionInfo;

struct OSResetFunctionInfo
{
    // public
    OSResetFunction      func;
    u32                  priority;

    // private
    OSResetFunctionInfo* next;
    OSResetFunctionInfo* prev;
};

void OSRegisterResetFunction  ( OSResetFunctionInfo* info );
void OSUnregisterResetFunction( OSResetFunctionInfo* info );
void OSResetSystem            ( int reset, u32 resetCode, BOOL forceMenu );
u32  OSGetResetCode           ( void );
void OSGetSaveRegion          ( void** start, void** end );
void OSGetSavedRegion         ( void** start, void** end );
void OSSetSaveRegion          ( void* start, void* end );
u32  OSSetBootDol             ( u32 dolOffset );

/*---------------------------------------------------------------------------*
  Name:         OSIsRestart

  Description:  Check to see if it's restarted

  Arguments:    None

  Returns:      True if restarted, False if cold-started
 *---------------------------------------------------------------------------*/
#define OSIsRestart()       ((OSGetResetCode() == OS_RESETCODE_RESTART)? \
                                         TRUE : FALSE)

#ifdef __cplusplus
}
#endif

#endif  // __OSRESET_H__
