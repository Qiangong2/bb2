/*---------------------------------------------------------------------------*
  Project: [structures]
  File:    [Tree.h]

  Copyright 1998-2001 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: Tree.h,v $
  Revision 1.1.1.1  2004/06/09 17:51:00  paulm
  GC include from Nintendo SDK

    
    3     3/21/01 2:44p John
    Updated copyright header.
    
    2     12/08/00 1:12p John
    Changed define from _TREE_H to _TREE_H_ for MSL C++ template
    compatibility.

  $NoKeywords: $

 *---------------------------------------------------------------------------*/

#ifndef _TREE_H_
#define _TREE_H_

#include <Dolphin/types.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct 
{
	Ptr	Prev;
	Ptr	Next;
	Ptr	Parent;
	Ptr	Children;

} DSBranch, *DSBranchPtr;

typedef struct 
{
	u32 Offset;
	Ptr Root;

} DSTree, *DSTreePtr;

void	DSExtractBranch		( DSTreePtr tree, Ptr obj );
void	DSInitTree			( DSTreePtr tree, Ptr obj, DSBranchPtr branch );
void	DSInsertBranchBelow	( DSTreePtr tree, Ptr cursor, Ptr obj );
void	DSInsertBranchBeside( DSTreePtr tree, Ptr cursor, Ptr obj );
void	DSRemoveBranch		( DSTreePtr tree, Ptr obj );

#ifdef __cplusplus
}
#endif

#endif // _TREE_H_
