/*---------------------------------------------------------------------------*
  Project: [structures]
  File:    [HTable.h]

  Copyright 1998-2001 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: HTable.h,v $
  Revision 1.1.1.1  2004/06/09 17:51:00  paulm
  GC include from Nintendo SDK

    
    3     3/21/01 2:43p John
    Updated copyright header.
    
    2     12/08/00 1:12p John
    Changed define from _TREE_H to _TREE_H_ for MSL C++ template
    compatibility.

  $NoKeywords: $

 *---------------------------------------------------------------------------*/

#ifndef _HTABLE_H_
#define _HTABLE_H_

#include <Dolphin/types.h>
#include <CharPipeline/structures/list.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef u16(DSHashFunc)( Ptr );

typedef struct 
{
  DSList*       table;
  u16		    tableSize;
  DSHashFunc*	hash;
} DSHashTable;

void    DSInitHTable        ( DSHashTable* hTable, 
                              u16          size, 
                              DSList*      listArray,
                              DSHashFunc*  hashFunc, 
                              Ptr          obj, 
                              DSLinkPtr    link );
void    DSInsertHTableObj   ( DSHashTable* hTable, Ptr obj );
void    DSHTableToList      ( DSHashTable* hTable, DSList* list );
void*   DSNextHTableObj     ( DSHashTable* hTable, Ptr obj );
s32     DSHTableIndex       ( DSHashTable* hTable, Ptr obj );
void*   DSHTableHead        ( DSHashTable* hTable, s32 index );

#ifdef __cplusplus
}
#endif

#endif // _HTABLE_H_
