/*---------------------------------------------------------------------------*
  Project: [structures]
  File:    [structures.h]

  Copyright 1998-2002 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: structures.h,v $
  Revision 1.1.1.1  2004/06/09 17:51:00  paulm
  GC include from Nintendo SDK

    
    3     8/22/02 11:10 Shiki
    Set #pragma warn_padding off.

    2     3/21/01 6:24p John
    Cleaned up code (copyrights).

  $NoKeywords: $

 *---------------------------------------------------------------------------*/

#ifndef __STRUCTURES_
#define __STRUCTURES_

#ifdef  __MWERKS__
#pragma warn_padding off
#endif

#include <charpipeline\structures\list.h>
#include <charpipeline\structures\tree.h>
#include <charpipeline\structures\htable.h>
#include <charpipeline\structures\dolphinString.h>

#ifdef  __MWERKS__
#pragma warn_padding reset
#endif

#endif
