/*---------------------------------------------------------------------------*
  Project:  Dolphin OS
  File:     dolphin.h

  Copyright 1998, 1999 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: dolphin.h,v $
  Revision 1.1.1.1  2004/06/09 17:51:00  paulm
  GC include from Nintendo SDK

    
    10    09/16/03 9:05 Shogo
    Corrected #endif comment.
    
    5     12/06/00 4:27p Billyjack
    added ax.h
    
    4     8/24/00 5:29p Shiki
    Included <dolphin/card.h>.

    3     6/06/00 9:25p Eugene
    Added header files for AI, AR, ARQ, and DSP drivers.

    2     12/23/99 4:05p Tian
    Added PPCArch.h

    5     7/23/99 12:11p Ryan
    removed demo.h

    4     7/20/99 6:03p Alligator
    demo library

    3     6/07/99 4:34p Alligator
    add vi.h

    2     6/03/99 7:43p Tianli01
    Added GX and MTX

    1     5/11/99 4:41p Shiki

  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#ifndef __DOLPHIN_H__
#define __DOLPHIN_H__

#include <dolphin/types.h>
#include <dolphin/base/PPCArch.h>
#include <dolphin/db.h>
#include <dolphin/os.h>
#include <dolphin/dvd.h>
#include <dolphin/pad.h>
#include <dolphin/mtx.h>
#include <dolphin/gx.h>
#include <dolphin/vi.h>
#include <dolphin/ai.h>
#include <dolphin/ar.h>
#include <dolphin/ax.h>
#include <dolphin/arq.h>
#include <dolphin/dsp.h>
#include <dolphin/card.h>

#endif // __DOLPHIN_H__

#ifndef __SDKVER_SYMBOL__
#define __SDKVER_SYMBOL__

#define __SDKVER__ "08May2003"
#define __SDKPATCH__ "Patch2"
#define __SDKVERPATCH__   __SDKVER__ __SDKPATCH__
#define __SDKVERNUMERIC__ 2003050802L

#endif // __SDKVER_SYMBOL__
