/*---------------------------------------------------------------------------*
  Project:  Dolphin DVD low-level functions
  File:     DVDLow.h

  Copyright 1998 - 2001 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: dvdlow.h,v $
  Revision 1.1.1.1  2004/03/18 19:02:52  paulm
  DI related headers from Nintendo

    
    20    03/07/23 10:53 Hashida
    Modification for 2003May SDK patch1.
    
    19    03/06/18 14:19 Hashida
    Added __DVDTestAlarm().
    
    18    02/12/26 5:25p Akagi
    Fixed to be C++ ready.
    
    17    10/30/01 2:59p Hashida
    Added NPDP specific error codes.
    
    16    6/18/01 2:17p Hashida
    Added timeout error.
    
    15    6/15/01 8:44p Hashida
    Added one more error code.
    Deleted several workaround.
    
    14    6/11/01 3:41p Hashida
    Added MEI bug workaround.
    
    13    3/29/01 6:38p Hashida
    Changed 20ms to 100ms to prevent both chattering and false cover open
    report.
    
    12    3/02/01 5:49p Hashida
    Modified error code handling (fatal error, internal retry and retry)
    
    11    3/02/01 12:06p Hashida
    Added DVDLowGetCoverStatus
    
    10    3/02/01 11:35a Hashida
    Added DVDCancel, DVDPause, DVDResume, DVDGetTransferredSize.
    
    9     2/23/01 4:27p Hashida
    Implemented reset cover interrupt callback so that cover close check
    can be done sooner.
    
    8     2/21/01 12:10p Hashida
    Added inquiry command.
    
    7     2/21/01 3:47a Hashida
    Changed POWERON_TICKS from 200ms to 80ms.
    
    6     10/03/00 10:07a Hashida
    Added DVD_RESETCOVER_TIMELAG_TICKS define.
    
    5     5/18/00 11:04a Hashida
    Changed DVD_RESET_TICKS from 10us to 12us.
    
    4     4/17/00 1:35p Hashida
    Added low audio buffer config command.
    
    3     4/07/00 6:54p Hashida
    Added streaming stuffs
    
    2     12/02/99 2:57p Hashida
    Added DVD_POWERON_TICKS.
    Changed DVD_RESET_TICKS to 10us.
    
    6     8/04/99 11:30a Hashida
    Added DVD_RESET_TICKS
    
    5     7/28/99 4:26p Hashida
    Changed error code to use the real one.
    
    4     7/23/99 4:59p Hashida
    Changed DVDLOW_DISKIMAGE_START from cached address to physical address
    
    3     7/20/99 10:31p Hashida
    Added DVDLowSeek
    
    2     7/19/99 4:33p Hashida
    Changed low callback function prototypes.
    Added some #defines.
    Fixed DVDLowRequestError().
    
    1     7/09/99 5:48p Hashida
    initial revision
    
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#ifndef __DVDLOW_H__
#define __DVDLOW_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <dolphin/types.h>
#include <dolphin/dvd.h>
#include <secure/dvdcb.h>

typedef void (*DVDLowCallback)(u32 intType);

#define OFFSET(n, a)    (((u32) (n)) & ((a) - 1))

#define DVDLOW_DISKIMAGE_START      0x00e00000

#define DVD_ALIGN_ADDR      32  // DMA transfer address alignment in bytes
#define DVD_ALIGN_SIZE      32  // DMA transfer size alignment in bytes
#define DVD_ALIGN_OFFSET    4   // DMA offset alignment in bytes

#define DVD_POWERON_TICKS   OSMillisecondsToTicks(200 - 120)
                                // 200 ms is needed on power on boot.
                                // However, 120ms has already passed when
                                // CPU starts executing BS1
#define DVD_RESET_TICKS     OSMicrosecondsToTicks(12)
                                // 12 us is needed to reset the drive.
#define DVD_RESETCOVER_TIMELAG_TICKS        \
                            OSMillisecondsToTicks(200)
                                // There's a time lag from reset until the
                                // drive can report the status of the cover 
#define DVD_RESETCOVER_TIMELAG_TICKS2       \
                            OSMillisecondsToTicks(100)
                                // This will eventually be merged to 
                                // TIMELAG_TICKS
                                // This can also prevent chattering

#define DVD_COMMAND_TIMEOUT_TICKS           \
                            OSSecondsToTicks(10)

#define DVD_COMMAND_TIMEOUT_TICKS_FOR_LONGREAD           \
                            OSSecondsToTicks(20)

#define DVD_INTERNALERROR_STATUSMASK        0xff000000

#define DVD_INTERNALERROR_STATUS_READY      0x00000000
#define DVD_INTERNALERROR_STATUS_COVEROPEN  0x01000000
#define DVD_INTERNALERROR_STATUS_DISKCHANGE 0x02000000
#define DVD_INTERNALERROR_STATUS_NODISK     0x03000000
#define DVD_INTERNALERROR_STATUS_MOTORSTOP  0x04000000
#define DVD_INTERNALERROR_STATUS_IDNOTREAD  0x05000000

#define DVD_INTERNALERROR_ERRORMASK         0x00ffffff

#define DVD_INTERNALERROR_NO_ERROR          0x00000000
#define DVD_INTERNALERROR_COVEROPEN_OR_NODISK 0x00023a00
#define DVD_INTERNALERROR_COVER_CLOSED      0x00062800
#define DVD_INTERNALERROR_NO_SEEK_COMPLETE  0x00030200
#define DVD_INTERNALERROR_UNRECOVERED_READ  0x00031100
#define DVD_INTERNALERROR_INVALID_COMMAND   0x00052000
#define DVD_INTERNALERROR_AUDIOBUF_NOT_SET  0x00052001
#define DVD_INTERNALERROR_LBA_OUT_OF_RANGE  0x00052100
#define DVD_INTERNALERROR_INVALID_FIELD     0x00052400
#define DVD_INTERNALERROR_INVALID_AUDIO_COMMAND 0x00052401
#define DVD_INTERNALERROR_AUDIOBUF_CONFIG_NOT_ALLOWED 0x00052402
#define DVD_INTERNALERROR_OP_DISK_RM_REQ    0x000b5a01
#define DVD_INTERNALERROR_END_OF_USER_AREA  0x00056300
#define DVD_INTERNALERROR_ID_NOT_READ       0x00020401
#define DVD_INTERNALERROR_MOTOR_STOPPED     0x00020400
#define DVD_INTERNALERROR_PROTOCOL_ERROR    0x00040800

// NPDP cartridge specific
#define DVD_INTERNALERROR_NPDP_UNKNOWNNPDPERROR         0x00100000
#define DVD_INTERNALERROR_NPDP_UNKNOWNFLASHROM          0x00100001
#define DVD_INTERNALERROR_NPDP_FLASHROMERASEERROR       0x00100002
#define DVD_INTERNALERROR_NPDP_FLASHROMWRITEERROR       0x00100003
#define DVD_INTERNALERROR_NPDP_FLASHROMVERIFYERROR      0x00100004
#define DVD_INTERNALERROR_NPDP_SECURITYUNLOCKERROR      0x00100005
#define DVD_INTERNALERROR_NPDP_SECURITYUPDATEERROR      0x00100006
#define DVD_INTERNALERROR_NPDP_HARDDISKREADERROR        0x00100007
#define DVD_INTERNALERROR_NPDP_HARDDISKWRITEERROR       0x00100008

#define DVD_INTERNALERROR_NPDP_MIN                      0x00100000
#define DVD_INTERNALERROR_NPDP_MAX                      0x00100008


#define DVD_INTTYPE_TC       1
#define DVD_INTTYPE_DE       2
#define DVD_INTTYPE_CVR      4
#define DVD_INTTYPE_BRK      8
#define DVD_INTTYPE_TIMEOUT  16

#define DVD_COVER_UNKNOWN    0
#define DVD_COVER_OPEN       1
#define DVD_COVER_CLOSED     2


enum
{
    DVD_WATYPE_NONE,
    DVD_WATYPE_COMBO1,
    DVD_WATYPE_MAX
};


BOOL DVDLowRead          ( void* addr, u32 length, u32 offset,
                           DVDLowCallback callback );

BOOL DVDLowSeek          ( u32 offset, DVDLowCallback callback );

BOOL DVDLowWaitCoverClose( DVDLowCallback callback );

BOOL DVDLowReadDiskID    ( DVDDiskID* diskID, DVDLowCallback callback );

BOOL DVDLowStopMotor     ( DVDLowCallback callback );

BOOL DVDLowRequestError  ( DVDLowCallback callback );

BOOL DVDLowInquiry       ( DVDDriveInfo* info, DVDLowCallback callback );

BOOL DVDLowAudioStream   ( u32 subcmd, u32 length, u32 offset, DVDLowCallback callback );

BOOL DVDLowRequestAudioStatus ( u32 subcmd, DVDLowCallback callback );

void DVDLowReset         ( void );

BOOL DVDLowAudioBufferConfig(BOOL enable, u32 size, DVDLowCallback callback);

DVDLowCallback DVDLowSetResetCoverCallback(DVDLowCallback callback);

BOOL DVDLowBreak(void);

DVDLowCallback DVDLowClearCallback(void);

u32 DVDLowGetCoverStatus(void);

void __DVDLowSetWAType(u32 type, s32 seekLoc);

void __DVDInitWA(void);

BOOL __DVDLowTestAlarm(const OSAlarm* alarm);

#ifdef __cplusplus
}
#endif

#endif  // __DVDLOW_H__
