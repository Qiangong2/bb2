/*---------------------------------------------------------------------------*
  Project:  Dolphin DVD middle-level functions
  File:     dvdcb.h

  Copyright 1998-2000  Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: dvdcb.h,v $
  Revision 1.1.1.1  2004/03/18 19:02:51  paulm
  DI related headers from Nintendo

    
    27    03/07/23 10:53 Hashida
    Modification for 2003May SDK patch1.
    
    26    03/06/18 14:19 Hashida
    Added __DVDTestAlarm().
    
    25    02/12/26 5:25p Akagi
    Fixed to be C++ ready.
    
    24    02/04/30 21:52 Hashida
    Added DVD_DEVICECODE_GCAM.
    
    23    1/22/02 18:25 Shiki
    Added DVD_DEVICECODE_FATAL.

    22    1/02/02 5:17p Hashida
    Initial revision for GC-AM support.

    21    8/21/01 3:43p Hashida
    Added DEVICECODE_PRESENT.

    20    8/21/01 12:01p Hashida
    Added definitions for various device codes.

    19    7/22/01 4:53a Hashida
    Changed the transfer size to 512K.

    18    7/17/01 1:08a Hashida
    Added __DVDPrepareResetAsync

    17    6/18/01 2:16p Hashida
    Added timeout error.

    16    6/18/01 11:04a Hashida
    Moved a result code (COVER_CLOSED) from dvd.h to dvdcb.h

    15    6/15/01 8:45p Hashida
    Added an error code for DEINT.
    Added __DVDStoreErrorCode.

    14    6/12/01 3:56p Hashida
    Added reset workaround.

    13    5/10/01 2:25p Hashida
    Added __DVDIsBlockInWaitingQueue.

    12    5/01/01 2:49p Hashida
    Changed DVDGetStreamAddr & DVDGetStreamLength to be internal APIs.

    11    3/07/01 6:50p Hashida
    Added several functions for BS.

    10    2/21/01 12:11p Hashida
    Added inquiry command.

    9     11/01/00 10:03a Hashida
    Changed default audio buffer size from 6 to 10

    8     10/09/00 6:54p Hashida
    Added prioritized queuing.

    7     9/25/00 3:33p Hashida
    Changed API names that don't follow the convention (sync, async)

    6     4/17/00 1:34p Hashida
    Added audio buffer config API.

    5     4/13/00 5:20p Hashida
    DVD_RESULT_COVER_CLOSED definition is moved to dvd.h because streaming
    commands will use that.

    4     4/07/00 6:54p Hashida
    Added streaming stuffs

    3     3/09/00 3:02p Hashida
    Changed #ifdef MARLIN ~ #ifdef SPRUCE into #ifdef SPRUCE_MARLIN to
    describe more specificly.

    2     12/23/99 1:20a Hashida
    Changed MAX_ONE_TRANSFER to be 0x1000 only for SPRUCE_MARLIN.

    10    9/09/99 2:15p Hashida
    Removed warnings.

    9     9/07/99 4:21p Hashida
    Changed the DMA size to 0x1000 because the bug is fixed.

    8     8/31/99 9:29p Hashida
    Changed the transfer size to 0x700

    7     8/31/99 11:01a Hashida
    Added Marlin support

    6     8/17/99 1:39p Hashida
    Changed DVDReset() to an independent function, which resets the drive
    and set interrupt masks properly.

    5     7/20/99 10:35p Hashida
    Added DVDSeekAbs()

    4     7/20/99 2:37p Hashida
    Changed the value of DVD_RESULT_COVER_CLOSED from 1 to -2

    3     7/19/99 11:45p Hashida
    changed the value for fatal error to -1

    2     7/19/99 11:39p Hashida
    Added DVDReset()
    Changed from DVD_RESULT_DISK_CHANGED to DVD_RESULT_COVER_CLOSED

    1     7/19/99 4:32p Hashida
    Initial revision

  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#ifndef __DVDCB_H__
#define __DVDCB_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <dolphin/types.h>
#include <dolphin/dvd.h>

typedef struct DVDDriveInfo DVDDriveInfo;

struct DVDDriveInfo
{
    u16         revisionLevel;
    u16         deviceCode;
    u32         releaseDate;        // in BCD
    u8          padding[24];
};

typedef void (*DVDOptionalCommandChecker)(DVDCommandBlock* block, void (*cb)(u32 intType));

// MEI devices, 0x00xx
#define DVD_DEVICECODE_REALDRIVE        0x0000
#define DVD_DEVICECODE_NRDRIVE          0x0001

// AMC device, 0x01xx
#define DVD_DEVICECODE_DDH              0x0100

// Hudson devices, 0x02xx
#define DVD_DEVICECODE_NPDP             0x0200
#define DVD_DEVICECODE_GDEV             0x0201

// Triforce; note this is not what triforce returns to "get device ID"
#define DVD_DEVICECODE_GCAM             0x1000

#define DVD_DEVICECODE_PRESENT          0x8000
#define DVD_DEVICECODE_FATAL            0x0001

#ifdef SPRUCE_MARLIN
// Marlin -2 needs packets to be divided into 4k chunks.
#define DVD_MAX_ONE_TRANSFER      0x1000
#else
//#define DVD_MAX_ONE_TRANSFER      0x00600000
#define DVD_MAX_ONE_TRANSFER      0x00080000
#endif

#define DVD_COMMAND_READ                        1
#define DVD_COMMAND_SEEK                        2
#define DVD_COMMAND_CHANGE_DISK                 3
#define DVD_COMMAND_BSREAD                      4
#define DVD_COMMAND_READID                      5
#define DVD_COMMAND_INITSTREAM                  6
#define DVD_COMMAND_CANCELSTREAM                7
#define DVD_COMMAND_STOP_STREAM_AT_END          8
#define DVD_COMMAND_REQUEST_AUDIO_ERROR         9
#define DVD_COMMAND_REQUEST_PLAY_ADDR           10
#define DVD_COMMAND_REQUEST_START_ADDR          11
#define DVD_COMMAND_REQUEST_LENGTH              12
#define DVD_COMMAND_AUDIO_BUFFER_CONFIG         13
#define DVD_COMMAND_INQUIRY                     14
#define DVD_COMMAND_BS_CHANGE_DISK              15


// Result that can only be returned for BS2 DVD APIs
#define DVD_RESULT_COVER_CLOSED                 -4      // make sure to check dvd.h

// XXX this value will be eventually decided and hardwired by HW
#define DVD_AUDIO_BUFFER_SIZE                   10

#define DVD_2RESETS_INTERVAL                    OSMillisecondsToTicks(1150)

#define DVD_DE_INT_ERROR_CODE                   0x01234567
#define DVD_TIMEOUT_ERROR_CODE                  0x01234568

extern BOOL DVDReadAbsAsyncPrio(DVDCommandBlock *block,
                                void* addr, s32 length, s32 offset,
                                DVDCBCallback callback, s32 prio);
extern BOOL DVDReadAbsAsyncForBS(DVDCommandBlock *block,
                                 void* addr, s32 length, s32 offset,
                                 DVDCBCallback callback);
extern BOOL DVDReadDiskID(DVDCommandBlock *block, DVDDiskID* diskID,
                          DVDCBCallback callback);
extern BOOL DVDSeekAbsAsyncPrio(DVDCommandBlock *block, s32 offset,
                                DVDCBCallback callback, s32 prio);
extern void DVDReset(void);

extern BOOL DVDPrepareStreamAbsAsync(DVDCommandBlock *block, u32 length, u32 offset,
                                     DVDCBCallback callback);

BOOL DVDGetStreamStartAddrAsync(DVDCommandBlock *block, DVDCBCallback callback);
s32  DVDGetStreamStartAddr(DVDCommandBlock *block);

BOOL DVDGetStreamLengthAsync(DVDCommandBlock *block, DVDCBCallback callback);
s32  DVDGetStreamLength(DVDCommandBlock *block);

extern BOOL DVDInquiryAsync(DVDCommandBlock *block, DVDDriveInfo* info,
                            DVDCBCallback callback);

extern s32 DVDInquiry(DVDCommandBlock *block, DVDDriveInfo* info);

// This API is for bootrom
extern void __DVDAudioBufferConfig(DVDCommandBlock *block, u32 enable, u32 size,
                                   DVDCBCallback callback);
extern BOOL DVDChangeDiskAsyncForBS(DVDCommandBlock* block, DVDCBCallback callback);

extern BOOL DVDResetRequired(void);

extern void __DVDPrepareResetAsync(DVDCBCallback callback);

// Exported from dvdqueue.c
extern void __DVDClearWaitingQueue(void);
extern BOOL __DVDPushWaitingQueue(s32 prio, DVDCommandBlock* block);
extern DVDCommandBlock* __DVDPopWaitingQueue(void);
extern BOOL __DVDCheckWaitingQueue(void);
extern BOOL __DVDDequeueWaitingQueue(DVDCommandBlock* block);
extern BOOL __DVDIsBlockInWaitingQueue(DVDCommandBlock* block);

// Exported from dvderror.c
extern void __DVDStoreErrorCode(u32 error);

// Exported from dvd.c
DVDOptionalCommandChecker __DVDSetOptionalCommandChecker(DVDOptionalCommandChecker func);
void __DVDSetImmCommand(u32 command);
void __DVDSetDmaCommand(u32 command);
void* __DVDGetIssueCommandAddr(void);
BOOL __DVDTestAlarm(const OSAlarm* alarm);

/*
 * Prototypes for the following functions should be in dvd.h, which
 * is exported to developers:
 *     DVDChangeDisk()
 *     DVDGetCommandBlockStatus()
 *     DVDGetDriveStatus()
 */

#ifdef __cplusplus
}
#endif

#endif  // __DVDCB_H__
