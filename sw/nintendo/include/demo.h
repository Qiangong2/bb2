/*---------------------------------------------------------------------------*
  Project:  Dolphin DEMO library
  File:     demo.h

  Copyright 1998-2001 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: demo.h,v $
  Revision 1.1.1.1  2004/06/09 17:51:00  paulm
  GC include from Nintendo SDK

    
    27    8/22/01 7:20p Carl
    BypassWorkaround is now GPHangWorkaround.
    Added DEMOSetGPHangMetric for hang diagnosis.
    
    26    5/09/01 10:10p Hirose
    moved DEMOPad/DEMOPuts/DEMOStat definitions into each individual header
    file.
    
    25    5/03/01 4:17p Tian
    Restored static inline changes
    
    24    01/04/25 14:28 Shiki
    Revised ROM font API interface. Added DEMOGetTextWidth().

    23    01/04/25 11:28 Shiki
    Added DEMOSetFontSize().

    22    01/04/19 13:37 Shiki
    Added ROM font functions.

    20    3/23/01 12:03p John
    Added stdio.h include to compensate for loss of geoPalette.h.

    19    3/22/01 2:38p John
    Removed geoPalette.h include (unnecessary).

    18    01/03/22 21:45 Shiki
    Removed DEMO_PAD_CHECK_INTERVAL.

    17    11/28/00 8:18p Hirose
    Enhancement of DEMOStat library
    Fixed DEMOSetTevOp definition for the emulator

    16    11/27/00 4:57p Carl
    Added DEMOSetTevColorIn and DEMOSetTevOp.

    15    10/26/00 10:31a Tian
    Added DEMOReInit and DEMOEnableBypassWorkaround.  Automatically repairs
    graphics pipe after a certain framecount based timeout.

    14    7/21/00 1:48p Carl
    Removed DEMODoneRenderBottom.
    Added DEMOSwapBuffers.

    13    6/20/00 10:37a Alligator
    added texture bandwidth, texture miss rate calculations

    12    6/19/00 3:23p Alligator
    added fill rate virtual counter

    11    6/12/00 4:32p Hirose
    updated DEMOPad library structures

    10    6/12/00 1:46p Alligator
    updated demo statistics to support new api

    9     6/06/00 12:02p Alligator
    made changes to perf counter api

    8     6/05/00 1:53p Carl
    Added DEMODoneRenderBottom

    7     5/18/00 2:56a Alligator
    added DEMOStats stuff

    6     5/02/00 3:28p Hirose
    added prototype of DEMOGetCurrentBuffer

    5     3/25/00 12:48a Hirose
    added DEMO_PAD_CHECK_INTERVAL macro

    4     3/23/00 1:20a Hirose
    added DEMOPad stuff

    3     1/19/00 3:43p Danm
    Added GXRenderModeObj *DEMOGetRenderModeObj(void) function.

    2     1/13/00 8:56p Danm
    Added GXRenderModeObj * parameter to DEMOInit()

    9     9/30/99 2:13p Ryan
    sweep to remove gxmodels libs

    8     9/28/99 6:56p Yasu
    Add defines of font type

    7     9/24/99 6:45p Yasu
    Change type of parameter of DEMOSetupScrnSpc().

    6     9/24/99 6:40p Yasu
    Change the number of parameter of DEMOSetupScrnSpc()

    5     9/24/99 6:35p Yasu
    Add DEMOSetupScrnSpc()

    4     9/23/99 5:35p Yasu
    Change function name cmMtxScreen to DEMOMtxScreen

    3     9/17/99 1:33p Ryan
    Added DEMOBeforeRender and DEMODoneRender

    2     9/14/99 5:08p Yasu
    Add some functions which contained in DEMOPut.c

    1     7/23/99 2:36p Ryan

    1     7/20/99 6:42p Alligator
    new demo lib
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#ifndef __DEMO_H__
#define __DEMO_H__

/*---------------------------------------------------------------------------*/
#include <dolphin.h>
#include <charPipeline/texPalette.h>
#include <stdio.h>

#include <demo/DEMOPad.h>
#include <demo/DEMOPuts.h>
#include <demo/DEMOStats.h>
/*---------------------------------------------------------------------------*/


#ifdef __cplusplus
extern "C" {
#endif

/*---------------------------------------------------------------------------*
    DEMOInit.c
 *---------------------------------------------------------------------------*/
extern void             DEMOInit             ( GXRenderModeObj* mode );
extern void             DEMOBeforeRender     ( void );
extern void             DEMODoneRender       ( void );
extern void             DEMOSwapBuffers      ( void );
extern GXRenderModeObj* DEMOGetRenderModeObj ( void );
extern void*            DEMOGetCurrentBuffer ( void );

extern void             DEMOEnableGPHangWorkaround ( u32 timeoutFrames );
extern void             DEMOReInit           ( GXRenderModeObj *mode );
extern void             DEMOSetGPHangMetric  ( GXBool enable );

/*---------------------------------------------------------------------------*
    DEMO misc
 *---------------------------------------------------------------------------*/

// The DEMO versions of SetTevColorIn and SetTevOp are backwards compatible
// with the Rev A versions in that the texture swap mode will be set
// appropriately if one of TEXC/TEXRRR/TEXGGG/TEXBBB is selected.

#if ( GX_REV == 1 || defined(EMU) )
static inline void DEMOSetTevColorIn(GXTevStageID stage,
                              GXTevColorArg a, GXTevColorArg b,
                              GXTevColorArg c, GXTevColorArg d )
    { GXSetTevColorIn(stage, a, b, c, d); }

static inline void DEMOSetTevOp(GXTevStageID stage, GXTevMode mode)
    { GXSetTevOp(stage, mode); }
#else
void DEMOSetTevColorIn(GXTevStageID stage,
                       GXTevColorArg a, GXTevColorArg b,
                       GXTevColorArg c, GXTevColorArg d );

void DEMOSetTevOp(GXTevStageID stage, GXTevMode mode);
#endif

/*---------------------------------------------------------------------------*/

#ifdef __cplusplus
}
#endif

#endif // __DEMO_H__

/*===========================================================================*/
