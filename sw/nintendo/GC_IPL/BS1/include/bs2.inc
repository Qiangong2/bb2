/*---------------------------------------------------------------------------*
  Project:  Boot Sequence Level 1
  File:     BS1.inc

  Copyright 1999 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: bs2.inc,v $
  Revision 1.1.1.1  2004/06/01 21:17:32  paulm
  GC IPLROM source from Nintendo


    7     5/12/00 11:58a Shiki
    Fixed BS2_ROMCMD for DRIP.

    6     5/11/00 4:37p Shiki
    Added config for DRIP system.

    5     5/10/00 12:09p Shiki
    Revised BS2_LEN to get info from <secure/boot.h>
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#include <secure/boot.h>

BS2_START       .set    0x00000800
BS2_ROMCMD      .set    0x00020000

BS2_LEN         .set    BS2_ROM_SIZE
BS2_DEST        .set    (BOOT_BS2_BASE-0x80000000-32)
BS2_ENTRY       .set    BOOT_BS2_BASE

