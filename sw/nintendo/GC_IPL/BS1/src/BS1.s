/*---------------------------------------------------------------------------*
  Project:  Boot Sequence Level 1
  File:     BS1.s

  Copyright 1999-2003 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: BS1.s,v $
  Revision 1.1.1.1  2004/06/01 21:17:32  paulm
  GC IPLROM source from Nintendo

    
    15    2/24/03 14:19 Shiki
    Updated to Revision 1.2.

    14    12/05/01 16:39 Shiki
    Updated to "PAL  Revision 1.0  ".

    13    11/15/01 12:59 Shiki
    Added more ROM information.

    12    01/04/25 18:32 Shiki
    Changed to set AMCT to 0x9C (=156).

    11    01/04/20 15:03 Shiki
    Changed to set AMCT to 0x9D (=157).
    Changed to set MEM_REFRESH to 0x40 (=64).

    10    01/03/16 22:17 Shiki
    Fixed not to clear PICONFIG PICFG bits.

    9     01/03/09 22:43 Shiki
    Modified to production ARAM version.

    8     01/03/09 15:14 Shiki
    Modified to set PI_REG_STRGTH and MEM_REFRESH.

    7     01/02/23 14:43 Shiki
    Added ARAM refresh setting.

    6     1/11/01 4:08p Shiki
    Fixed not to use #elif.

    5     1/10/01 5:38p Shiki
    Modified ARAM Configuration for HW2.

    4     10/24/00 5:08p Shiki
    Fixed r29 initial value to zero.

    3     10/23/00 7:02p Shiki
    Added postcode.

    2     9/11/00 3:36p Shiki
    Implemented SPLASH setting bypass code.

    1     9/08/00 4:07p Tian
    Initial checkin to production tree

    14    9/05/00 5:25p Shiki
    Fixed a harmless wrong EXI_CRIMMVAL setting.

    13    9/05/00 3:31p Hashida
    Added splash reset to handle bad splash.

    12    7/10/00 3:19p Tian
    Added changes by Scott Carr for Splash support

    11    5/11/00 4:37p Shiki
    Added config code for DRIP system. (ROM scrambling must be turned off.)

    10    5/11/00 10:24a Shiki
    Fixed a copyright line.

    9     5/10/00 5:55p Shiki
    Fixed copyright info.

    8     5/10/00 4:46p Shiki
    Detabbed.

    7     5/10/00 2:24p Shiki
    Fixed #ifdef ORCA.

    6     4/03/00 8:04p Hashida
    Clear BI2 address in lomem to prevent __start from parsing arguments.

    5     3/17/00 6:59p Hashida
    Added Splash settings.

    4     3/09/00 8:01p Shiki
    Implemented ARAM CAS latency program setting.
  $NoKeywords: $
 *---------------------------------------------------------------------------*/
;****************************************************************************
;
;   (C) 1999 ARTX INC..  ALL RIGHTS RESERVED.  UNPUBLISHED -- RIGHTS
;   RESERVED UNDER THE COPYRIGHT LAWS OF THE UNITED STATES.  USE OF A
;   COPYRIGHT NOTICE IS PRECAUTIONARY ONLY AND DOES NOT IMPLY PUBLICATION
;   OR DISCLOSURE.
;
;   THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF
;   ARTX INC..  USE, DISCLOSURE, OR REPRODUCTION IS PROHIBITED WITHOUT
;   THE PRIOR EXPRESS WRITTEN PERMISSION OF ARTX INC..
;
;                   RESTRICTED RIGHTS LEGEND
;
;   Use, duplication, or disclosure by the Government is subject to
;   restrictions as set forth in subparagraph (c)(1)(ii) of the Rights
;   in Technical Data and Computer Software clause at DFARS 252.227-7013
;   or subparagraphs (c)(1) and (2) of Commercial Computer Software --
;   Restricted Rights at 48 CFR 52.227-19, as applicable.
;
;   ArtX Inc.
;   3400 Hillview Ave, Bldg 5
;   Palo Alto, CA 94304
;
;****************************************************************************

#define         RETAIL                          ; skips DI_CONFIG check code

; The assembler can't handle bit defs so hardcode it.
; DPM | NHR | SGE | BTIC | BHT | ICFI | DCFI
HID0VAL         .set            0x00110C64
IC_ENABLE       .set            0x00008000
DC_ENABLE       .set            0x00004000
L1_ENABLE       .set            0x0000C000
MSRVAL          .set            0x00002000      ; FP

L2_INV          .set            0x19200000
L2_ENABLE       .set            0x99000000

; Dolphin OS BAT register settings
BAT0LVAL        .set            0x00000002      ; paddr 0x00000000, WIMG=,   RW
BAT0UVAL        .set            0x80001fff      ; vaddr 0x80000000, 256MB
BAT1LVAL        .set            0x0000002a      ; paddr 0x00000000, WIMG=IG, RW
BAT1UVAL        .set            0xC0001fff      ; vaddr 0xC0000000, 256MB
BAT3LVAL        .set            0xFFF00001      ; paddr 0xFFF00000, WIMG=,   R
BAT3UVAL        .set            0xFFF0001F      ; vaddr 0xFFF00000,   1MB

; Dolphin OS SR registers setting
SRVAL           .set            0x80000000      ; direct store mode to raise exceptions

; This defines BS2_START and BS2_LEN
#include "bs2.inc"

;-------------------------------------------------------------------------------
; SI values
;-------------------------------------------------------------------------------

SI_BASE         .set            0xcc006400

SI_EXILK        .set            0x0000003c
EXI_LOCK        .set            0x80000000

;-------------------------------------------------------------------------------
; PI values
;-------------------------------------------------------------------------------

PI_BASE         .set            0xCC003000

PI_PICFG_REG    .set            0x00000024
PI_PICFG_SYS    .set            0x0001          ; enable CPU
PI_PICFG_MEM    .set            0x0002          ; enable MEM
PI_PICFG_DI     .set            0x0004          ; enable DI
PI_PICFG_VAL    .set            0x0003          ; enable both

PI_REG_STRGTH   .set            0x00000030
STRGTHVAL       .set            0x0245248A      ; ARAM     010
                                                ; VI       010
                                                ; DI       001
                                                ; EXI0     010
                                                ; EXI1     010
                                                ; EXI2     010
                                                ; SI       010
                                                ; AIS      001
                                                ; AI       010

;-------------------------------------------------------------------------------
; EXI values
;-------------------------------------------------------------------------------

EXI_BASE        .set            0xcc006800
EXI_0CPR        .set            0x00000000
EXI_0MAR        .set            0x00000004
EXI_0LEN        .set            0x00000008
EXI_0CR         .set            0x0000000C
EXI_0DATA       .set            0x00000010
EXI_1CPR        .set            0x00000014
EXI_1MAR        .set            0x00000018
EXI_1LEN        .set            0x0000001C
EXI_1CR         .set            0x00000020
EXI_1DATA       .set            0x00000024
EXI_2CPR        .set            0x00000028
EXI_2MAR        .set            0x0000002C
EXI_2LEN        .set            0x00000030
EXI_2CR         .set            0x00000034
EXI_2DATA       .set            0x00000038

;-------------------------------------------------------------------------------
; RTC ROM values
;-------------------------------------------------------------------------------

EXI_MAXDMA      .set            0x00000400
RAMPCMD_LEN     .set            0x00010000      ; EXI_MAXDMA << 6

; register values
; The select
EXI_CPRVAL      .set            0x00000150      ; assume ROM is csel 1, freq 32Mhz, no irqs
EXI_CPRROMDIS   .set            0x00002000

; the IMM DMA start
EXI_CRIMMVAL    .set            0x00000035      ; len 4, write, IMM, TSTART, 110101

; the DMA
EXI_CRDMAVAL    .set            0x00000003      ; read, DMA, TSTART 000011

;-------------------------------------------------------------------------------
; Barnacle values
;-------------------------------------------------------------------------------

EXI_SER_CPRVAL  .set            0x0000013A      ; csel 1, freq 8Mhz, no irqs, 10:011:1010
EXI_SER_FIFO    .set            0x20010000      ; 0x0800400 << 6 (4:1)
EXI_SER_TX      .set            0xA0010000      ; 0x2800400 << 6 (4: up to 12)

;-------------------------------------------------------------------------------
; EXI_AD16 values
;-------------------------------------------------------------------------------

EXI_AD16_CPRVAL .set            0x000000BA      ; csel 0, freq 8MHz, no irqs, 1:011:1010
EXI_CRVAL_RD1   .set            0x00000001      ; len 1, read,  IMM, TSTART, 00 0001
EXI_CRVAL_RD4   .set            0x00000031      ; len 4, read,  IMM, TSTART, 11 0001
EXI_CRVAL_WR1   .set            0x00000005      ; len 1, write, IMM, TSTART, 00 0101
EXI_CRVAL_WR2   .set            0x00000015      ; len 2, write, IMM, TSTART, 01 0101
EXI_CRVAL_WR4   .set            0x00000035      ; len 4, write, IMM, TSTART, 11 0101
EXI_AD16_ID     .set            0x04120000

EXI_AD16_IDCMD  .set            0x00000000
EXI_AD16_WRCMD  .set            0xA0000000

;-------------------------------------------------------------------------------
; DSP values
;-------------------------------------------------------------------------------

#ifndef MINNOW

DSP_AMCR        .set            0x0C005012      ; in physical address

DSP_AMMA_ADDR   .set            0xCC005020
DSP_AMAA_ADDR   .set            0xCC005024
DSP_AMBL_ADDR   .set            0xCC005028

DSP_CDCR_ADDR   .set            0xCC00500A

#endif  // MINNOW

;-------------------------------------------------------------------------------
; MEM values
;-------------------------------------------------------------------------------

#ifndef MINNOW

MEM_BASE        .set            0xCC004000
MEM_REAL_BASE   .set            0x0C004000

MEM_REFRESH     .set            0x00000026
REFRESHVAL      .set            0x0040

MEM_CONFIG      .set            0x00000028
CFGVAL_1x16     .set            0x0000
CFGVAL_2x16     .set            0x0001
CFGVAL_1x24     .set            0x0002  ; default
CFGVAL_2x24     .set            0x0003
CFGVAL_1x32     .set            0x0004
CFGVAL_2x32     .set            0x0005

MEM_DRVSTR      .set            0x0000005A
DRVSTR_2        .set            0x0212  ; default
DRVSTR_4        .set            0x021A

#endif  // MINNOW

;-------------------------------------------------------------------------------
; Misc. values
;-------------------------------------------------------------------------------

#ifdef MINNOW
LEDLOC          .set            0xcc004100
#endif

#ifndef MINNOW
DI_CONFIG       .set            0xCC006024
NUM_CHIPS_4     .set            0x00000008
#endif

#ifdef DRIP
.extern _memReset
.extern ppcL2Enable
.extern ppcL2Disable
#endif

;-------------------------------------------------------------------------------
; We put some ROM information here (0x0000)
;-------------------------------------------------------------------------------
        .text
        .align 0x100
        .byte   "(C) 1999-2003 Nintendo.  All rights reserved."
        .byte   "(C) 1999 ArtX Inc.  All rights reserved."
#ifdef PAL
        .byte   "PAL  Revision 1.2  "
#endif
#ifdef NTSC
        .byte   "NTSC Revision 1.2  "
#endif
#ifdef MPAL
        .byte   "MPAL Revision 1.2  "
#endif

;-------------------------------------------------------------------------------
; Assume we won't hit any exceptions (0x0100)
;-------------------------------------------------------------------------------
        .align 0x100
        .globl powerUp
powerUp:
        addis   r4,r0,HID0VAL@ha        ; 0
        addi    r4,r4,HID0VAL@l         ; 1
        mtspr   hid0,r4                 ; 2: Set default HID0

        addis   r4,r0,MSRVAL@ha         ; 3
        addi    r4,r4,MSRVAL@l          ; 4
        mtmsr   r4                      ; 5: Set default MSR (Clear IP)

;-------------------------------------------------------------------------------
; Set ARAM Configuration
;-------------------------------------------------------------------------------

#ifdef MINNOW

        nop                             ; 6
        nop                             ; 7
;-------- 0x020
        nop                             ; 0
        nop                             ; 1
        nop                             ; 2
        nop                             ; 3

#else   // !MINNOW
#ifdef HW1

        addis   r4,r0,DSP_AMCR@ha       ; 6
        addi    r4,r4,DSP_AMCR@l        ; 7
;-------- 0x020
        li      r5,0x1                  ; 0: CAS latency (CAS=2)
        sth     r5,0(r4)                ; 1
        nop                             ; 2
        nop                             ; 3

#else   // !HW1

        addis   r4,r0,DSP_AMCR@ha       ; 6
        addi    r4,r4,DSP_AMCR@l        ; 7
;-------- 0x020
#if 1
        li      r5,0x43                 ; 0: ARAM Mode Register Setting (Disable)
                                        ;    & Internal ARAM Size (16MB)
#else
        li      r5,0x3                  ; 0: Internal ARAM Size (16MB)
#endif
        sth     r5,0(r4)                ; 1  Write into AMCR

        li      r5,0x9c                 ; 2: Refresh period
        sth     r5,8(r4)                ; 3: Write into AMCT

#endif  // HW1
#endif  // MINNOW

;-----------------------------------------------------------------------
; Set Splash refresh period/data retention time
;-----------------------------------------------------------------------
#if !defined(MINNOW) && !defined(DRIP)
        addis   r3,r0,MEM_REAL_BASE@h   ; 4
        ori     r3,r3,MEM_REAL_BASE@l   ; 5
        li      r4,REFRESHVAL           ; 6
        sth     r4,MEM_REFRESH(r3)      ; 7
#else   // MINNOW
        nop                             ; 4
        nop                             ; 5
        nop                             ; 6
        nop                             ; 7
#endif  // MINNOW

;-------- 0x040
        nop                             ; 0
        nop                             ; 1

;-----------------------------------------------------------------------
; Turn on the L1 cache, it's already been flushed and invalidated
;-----------------------------------------------------------------------
        mfspr   r3, hid0                ; 2
        ori     r4, r3, L1_ENABLE@l     ; 3
        mtspr   hid0, r4                ; 4
        nop                             ; 5
        nop                             ; 6
        nop                             ; 7: Gekko prefeches instructions
                                        ;    up to here before I-cache takes
                                        ;    effect.
;-------- 0x060

;-----------------------------------------------------------------------
; Clear upper DBAT and IBAT registers (rendering them invalid)
;-----------------------------------------------------------------------
        isync                           ; First I-cache line faulted in
        addi    r4,r0,0x0000
        mtspr   dbat0u,r4
        mtspr   dbat1u,r4
        mtspr   dbat2u,r4
        mtspr   dbat3u,r4
        mtspr   ibat0u,r4
        mtspr   ibat1u,r4
        mtspr   ibat2u,r4
        mtspr   ibat3u,r4
        isync

;-----------------------------------------------------------------------
; Initialize the segment registers to enable direct store mode
;-----------------------------------------------------------------------
        addis   r4,r0,SRVAL@ha
        addi    r4,r4,SRVAL@l
        mtsr    0,r4
        mtsr    1,r4
        mtsr    2,r4
        mtsr    3,r4
        mtsr    4,r4
        mtsr    5,r4
        mtsr    6,r4
        mtsr    7,r4
        mtsr    8,r4
        mtsr    9,r4
        mtsr    10,r4
        mtsr    11,r4
        mtsr    12,r4
        mtsr    13,r4
        mtsr    14,r4
        mtsr    15,r4

;-----------------------------------------------------------------------
; Set DBAT registers to establish cacheability for address regions
; on the board.
; 80000000:8FFFFFFF -> 00000000:0FFFFFFF,   cacheable, WB, IBAT0
; 80000000:8FFFFFFF -> 00000000:0FFFFFFF,   cacheable, WB, DBAT0
; C0000000:CFFFFFFF -> 00000000:0FFFFFFF, uncacheable,     DBAT1
; FFF00000:000FFFFF -> FFF00000:FFFFFFFF,   cacheable, WB, IBAT3
; FFF00000:000FFFFF -> FFF00000:FFFFFFFF,   cacheable, WB, DBAT3
;-----------------------------------------------------------------------
        ; Map in the WB spaces
        addis   r4,r0,BAT0LVAL@ha
        addi    r4,r4,BAT0LVAL@l
        addis   r3,r0,BAT0UVAL@ha
        addi    r3,r3,BAT0UVAL@l
        mtspr   dbat0l,r4
        mtspr   dbat0u,r3
        isync
        mtspr   ibat0l,r4
        mtspr   ibat0u,r3
        isync

        ; Map in the UC space.
        addis   r4,r0,BAT1LVAL@ha
        addi    r4,r4,BAT1LVAL@l
        addis   r3,r0,BAT1UVAL@ha
        addi    r3,r3,BAT1UVAL@l
        mtspr   dbat1l,r4
        mtspr   dbat1u,r3
        isync

        ; Map in the ROM
        addis   r4,r0,BAT3LVAL@ha
        addi    r4,r4,BAT3LVAL@l
        addis   r3,r0,BAT3UVAL@ha
        addi    r3,r3,BAT3UVAL@l
        mtspr   dbat3l,r4
        mtspr   dbat3u,r3
        isync
        mtspr   ibat3l,r4
        mtspr   ibat3u,r3
        isync

;-----------------------------------------------------------------------
; Turn on the MMU
;-----------------------------------------------------------------------
        mfmsr   r4
        ori     r4,r4,0x0030
        mtmsr   r4
        isync

#if !defined(MINNOW) && !defined(DRIP)
        addis   r3,r0,PI_BASE@h
        ori     r3,r3,PI_BASE@l

;-----------------------------------------------------------------------
; Set strength
;-----------------------------------------------------------------------
        addis   r4,r0,STRGTHVAL@h
        ori     r4,r4,STRGTHVAL@l
        stw     r4,PI_REG_STRGTH(r3)

;-----------------------------------------------------------------------
; Reset memory (to work around Splash problem. Requested by Dan Shimizu)
;-----------------------------------------------------------------------
        ; assert memrst

        lwz     r4,PI_PICFG_REG(r3)
        ori     r4,r4,PI_PICFG_SYS
        rlwinm  r4, r4, 0, 30+1, 29-1   ; clear PI_PICFG_MEM and PI_PICFG_DI
        stw     r4,PI_PICFG_REG(r3)

        ; wait for 100us
        mftb    r5                      ; load from TBL
_100usloop:
        mftb    r6                      ; load from TBL
        subf    r7, r5, r6
        cmplwi  r7, 0x1124              ; 0x1124 = 4388 = 100us at 175.5MHz
        blt+    _100usloop

        ; deassert memrst
        ori     r4,r4,PI_PICFG_VAL
        stw     r4,PI_PICFG_REG(r3)
#else
        nop
        nop

        nop
        nop
        nop

        nop
        nop
        nop
        nop

        nop
        nop
        nop
#ifdef MINNOW
;-----------------------------------------------------------------------
; Test LED
;-----------------------------------------------------------------------
; R15 points to the LED
        addis   r3,r0,LEDLOC@ha
        addi    r3,r3,LEDLOC@l
; R18 indicates the current step
        li      r4,4            ; clear LED value
        sth     r4,0(r3)        ; write to the LED
#else   // MINNOW
        nop
        nop
        nop
        nop
#endif  // MINNOW
#endif  // !defined(MINNOW) && !defined(DRIP)

;-----------------------------------------------------------------------
; DRIP settings
;-----------------------------------------------------------------------
#ifdef DRIP
        bl ppcL2Enable
; Set Mem regs for DRIP
        xor r3, r3, r3
        oris r3, r3, 0xCC00
        ori r3, r3, 0x4000
        li r4, 0x1
        sth r4, 0x2a(r3)
        sth r4, 0x2e(r3)
        sth r4, 0x30(r3)
; Assert MEM reset
        xor r3, r3, r3
        oris r3, r3, 0xCC00
        ori r3, r3, 0x3024
        xor r4, r4, r4
        ori r4, r4, 0x0005
        stw r4, 0(r3)
        bl _memReset
; Deassert MEM reset
        xor r3, r3, r3
        oris r3, r3, 0xCC00
        ori r3, r3, 0x3024
        xor r4, r4, r4
        ori r4, r4, 0x0007
        stw r4, 0(r3)
        bl ppcL2Disable
#endif // DRIP

#if 0
;-----------------------------------------------------------------------
; Enable L2
;-----------------------------------------------------------------------
        xor     r3, r3, r3
        oris    r3, r3, L2_INV@h
        ori     r3, r3, L2_INV@l
        mtspr   l2cr, r3
        isync
_l2_inv_loop:
        mfspr   r3, l2cr
        andi.   r3, r3, 0x1
        cmpi    cr0, 0, r3, 0x0
        bne     _l2_inv_loop
        isync
        xor     r3, r3, r3
        oris    r3, r3, L2_ENABLE@h
        ori     r3, r3, L2_ENABLE@l
        mtspr   l2cr, r3
        isync
#endif

;-----------------------------------------------------------------------
; Unlock 32Mhz mode on the EXI interface
;-----------------------------------------------------------------------
; R14 is the SI base
        addis   r14,r0,SI_BASE@h
        ori     r14,r14,SI_BASE@l
        li      r4, 0
        stw     r4, SI_EXILK(r14)

;-----------------------------------------------------------------------
; Init EXI
;-----------------------------------------------------------------------

; R2 is the EXI base
        addis   r2,r0,EXI_BASE@h
        ori     r2,r2,EXI_BASE@l

; R22 - select EXI_AD16
        addis   r22,r0,EXI_AD16_CPRVAL@h
        ori     r22,r22,EXI_AD16_CPRVAL@l

; R8 - TSTART test bit
        li      r8, 1

; R10 - for deselecting EXI
        addi    r10,r0,0x0000

; R21 - EXI_AD16 ID
        addis   r21,r0,EXI_AD16_ID@h
        ori     r21,r21,EXI_AD16_ID@l

;-----------------------------------------------------------------------
; Probe EXI_AD16
;-----------------------------------------------------------------------

; R3 - Read ID command (Rx)
        addis   r3,r0,EXI_AD16_IDCMD@h
        ori     r3,r3,EXI_AD16_IDCMD@l

; R7 - get it ready to go
        addis   r7,r0,EXI_CRVAL_WR2@h
        ori     r7,r7,EXI_CRVAL_WR2@l

        ; Set up read ID command
        stw     r3, EXI_2DATA(r2)

        ; Select the EXI_AD16
        stw     r22, EXI_2CPR(r2)

        ; Kill some time between select and command
        lwz     r16, EXI_2CPR(r2)

        ; Get it ready to go
        stw     r7, EXI_2CR(r2)

        ; Wait for it to finish
_immWait0:
        ; Get the start bit
        lwz     r16, EXI_2CR(r2)
        ; Is it set
        and.    r16, r16, r8
        ; Wait for TSTART to stop
        bgt     _immWait0

; R7 - get it ready to go
        addis   r7,r0,EXI_CRVAL_RD4@h
        ori     r7,r7,EXI_CRVAL_RD4@l

        ; Get it ready to go
        stw     r7, EXI_2CR(r2)

        ; Wait for it to finish
_immWait1:
        ; Get the start bit
        lwz     r16, EXI_2CR(r2)
        ; Is it set
        and.    r16, r16, r8
        ; Wait for TSTART to stop
        bgt     _immWait1

        ; Deselect EXI
        stw     r10, EXI_2CPR(r2)

        ; Kill time for the next select
        lwz     r16, EXI_2CPR(r2)
        lwz     r16, EXI_2CPR(r2)

; R20 - EXI2 ID
        ; Read ID
        lwz     r20, EXI_2DATA(r2)

;-----------------------------------------------------------------------
; Setup SetPostCode code in the I-cache (R15 - postcode)
;-----------------------------------------------------------------------
        b       _spc_cl0
        .align  0x20
;--------
_spc_cl0:
        b       _spc_cl1                ; 0
_spc_begin:
; R3 - Master Register WR command (Rx)
        addis   r3,r0,EXI_AD16_WRCMD@h  ; 1
        ori     r3,r3,EXI_AD16_WRCMD@l  ; 2

; R7 - get it ready to go
        addis   r7,r0,EXI_CRVAL_WR1@h   ; 3
        ori     r7,r7,EXI_CRVAL_WR1@l   ; 4

        ; Set up reg WR command
        stw     r3, EXI_2DATA(r2)       ; 5

        ; Select the EXI_AD16
        stw     r22, EXI_2CPR(r2)       ; 6

        ; Kill some time between select and command
        lwz     r16, EXI_2CPR(r2)       ; 7

        .align  0x20
;--------
        b       _spc_cl1x
_spc_cl1:
        b       _spc_cl2                ; 1
_spc_cl1x:
        ; Get it ready to go
        stw     r7, EXI_2CR(r2)         ; 2

        ; Wait for it to finish
_spc_immWait0:
        ; Get the start bit
        lwz     r16, EXI_2CR(r2)        ; 3
        ; Is it set
        and.    r16, r16, r8            ; 4
        ; Wait for TSTART to stop
        bgt     _spc_immWait0           ; 5
        nop                             ; 6
        nop                             ; 7

        .align  0x20
;--------
        b       _spc_cl2x
_spc_cl2:
        b       _spc_cl3                ; 1
_spc_cl2x:

; R7 - get it ready to go
        addis   r7,r0,EXI_CRVAL_WR4@h   ; 2
        ori     r7,r7,EXI_CRVAL_WR4@l   ; 3

        ; Set up post code
        stw     r15, EXI_2DATA(r2)      ; 4

        ; Get it ready to go
        stw     r7, EXI_2CR(r2)         ; 5

        ; Wait for it to finish
_spc_immWait1:
        ; Get the start bit
        lwz     r16, EXI_2CR(r2)        ; 6
        ; Is it set
        and.    r16, r16, r8            ; 7

        .align  0x20
;--------
        b       _spc_cl3x
_spc_cl3:
        b       _spc_cl4                ; 1
_spc_cl3x:
        ; Wait for TSTART to stop       ; 2
        bgt     _spc_immWait1           ; 3
        ; Deselect EXI
        stw     r10, EXI_2CPR(r2)       ; 4

        ; Kill time for the next select
        lwz     r16, EXI_2CPR(r2)       ; 5
        lwz     r16, EXI_2CPR(r2)       ; 6
        blr                             ; 7

        .align  0x20
;--------
_spc_cl4:
        b       _spc_end                ; 1

SetPostCode:
        ; Compare ID
        cmplw   r20, r21                ; 2
        beq     _spc_begin              ; 3
        blr                             ; 4
_spc_end:

;----------------------------------------------------------------------
; Post Code 1 (CPU Okay, EXI2 Okay)
;-----------------------------------------------------------------------

        lis     r15, 0x01000000@h
        bl      SetPostCode

;-----------------------------------------------------------------------
; SPLASH settings
;-----------------------------------------------------------------------
; We don't need to set physical memory size here.
; We can check num of chips in BS2 again.
; (we can assume we have 12M chips.)
#if !defined(MINNOW) && !defined(DRIP) && !defined(RETAIL)
        ; setting memrst was moved to the earlier part of BS1
;
; Check the dip switch for number of memory chips
; 0 - 4 chips; 1 - 2 chips
;
        addis   r15,r0,DI_CONFIG@ha
        addi    r15,r15,DI_CONFIG@l
        lwz     r15,0(r15)

        addis   r16,r0,MEM_BASE@ha
        addi    r16,r16,MEM_BASE@l

        addi    r17,r0,CFGVAL_2x24
        addi    r18,r0,DRVSTR_4

; preload the cachelines before jumping into memory
; NOTE: 8 instructions per cache line!!!
        b       memInit
        .align  0x20
;--------
memInit:
        andi.   r15,r15,NUM_CHIPS_4
        bne     memInitDone     ; jump if 2 chips
; set 2 x 24 to mem config
        sth     r17,MEM_CONFIG(r16)
; change address strength register for 4 chips
        sth     r18,MEM_DRVSTR(r16)
; read from mem module to make sure the register change
; went to the module (probably don't need this)
        lhz     r18,MEM_DRVSTR(r16)

#else
        nop
        nop
        nop

        nop
        nop

        nop
        nop

        b       memInit
        .align  0x20
;--------
memInit:
        nop
        nop
        nop
        nop
        nop
#endif

memInitDone:

;----------------------------------------------------------------------
; Post Code 2 (Initialized Splash)
;-----------------------------------------------------------------------

        lis     r15, 0x02000000@h
        bl      SetPostCode

;----------------------------------------------------------------------
; Install main memory test code into I-cache
;-----------------------------------------------------------------------
; R15 - temp
; R16 - temp
; R17 - bad memory #0 [  0M -  8M]
; R18 - bad memory #1 [  8M - 16M]
; R19 - bad memory #2 [ 16M - 24M]
; R23 - ptr
; R24 - len
; R25 - address
; R26 - pat
; R27 - bad memory Splash 0
; R28 - bad memory Splash 1
; R29 - the 1st bad word address

        b       _tmm_cl0
        .align  0x20
;--------
_tmm_cl0:
        b       _tmm_cl1                ; 0

;----------------------------------------------------------------------
; TestMemory: R25 - test address, R26 - test pattern
;----------------------------------------------------------------------
TestMemory:

_tmm_fill:
        nop                             ; 1
        nop                             ; 2
        nop                             ; 3
        nop                             ; 4
        nop                             ; 5

        ; load ptr
        mr      r23,r25                 ; 6

        ; load length
        lis     r24,0x1800000@h         ; 7

        .align  0x20
;--------
        b       _tmm_cl1x               ; 0
_tmm_cl1:
        b       _tmm_cl2                ; 1
_tmm_cl1x:

        srwi    r24,r24,5               ; 2

        mtctr   r24                     ; 3

_tmm_write:
        stw     r26,0(r23)              ; 4
        stw     r26,4(r23)              ; 5
        stw     r26,8(r23)              ; 6
        stw     r26,12(r23)             ; 7

        .align  0x20
;--------
        b       _tmm_cl2x               ; 0
_tmm_cl2:
        b       _tmm_cl3                ; 1
_tmm_cl2x:

        stw     r26,16(r23)             ; 2
        stw     r26,20(r23)             ; 3
        stw     r26,24(r23)             ; 4
        stw     r26,28(r23)             ; 5
        addi    r23,r23,32              ; 6
        bdnz    _tmm_write              ; 7

        .align  0x20
;--------
        b       _tmm_cl3x               ; 0
_tmm_cl3:
        b       _tmm_cl4                ; 1
_tmm_cl3x:

_tmm_verify:
        ; load ptr
        mr      r23,r25                 ; 2

        ; load length
        lis     r24,0x1800000@h         ; 3
        srwi    r24,r24,2               ; 4
        mtctr   r24                     ; 5

_tmm_read:
        lwz     r15,0(r23)              ; 6
        cmplw   r15,r26                 ; 7

        .align  0x20
;--------
        b       _tmm_cl4x               ; 0
_tmm_cl4:
        b       _tmm_cl5                ; 1
_tmm_cl4x:

        beq     _tmm_readnext           ; 2

        ; found a bad word

        ; 0 - 8 [M]
        srwi    r15,r23,18              ; 3     (per 256KB)
        andi.   r15,r15,31              ; 4
        li      r16,1                   ; 5
        slw     r16,r16,r15             ; 6
        or      r17,r17,r16             ; 7

        .align  0x20
;--------
        b       _tmm_cl5x               ; 0
_tmm_cl5:
        b       _tmm_cl6                ; 1
_tmm_cl5x:

        ; 8 - 16 [M]
        srwi    r15,r23,18              ; 2     (per 256KB)
        subi    r15,r15,32              ; 3
        andi.   r15,r15,31              ; 4
        li      r16,1                   ; 5
        slw     r16,r16,r15             ; 6
        or      r18,r18,r16             ; 7

        .align  0x20
;--------
        b       _tmm_cl6x               ; 0
_tmm_cl6:
        b       _tmm_cl7                ; 1
_tmm_cl6x:

        ; 16 - 24 [M]
        srwi    r15,r23,18              ; 2     (per 256KB)
        subi    r15,r15,64              ; 3
        andi.   r15,r15,31              ; 4
        li      r16,1                   ; 5
        slw     r16,r16,r15             ; 6
        or      r19,r19,r16             ; 7

        .align  0x20
;--------
        b       _tmm_cl7x               ; 0
_tmm_cl7:
        b       _tmm_cl8                ; 1
_tmm_cl7x:

        clrlwi  r15,r23,28              ; 2
        cmplwi  r15,0x0008              ; 3
        bge     _tmm_splash0            ; 4
        addi    r28,r28,1               ; 5
        b       _tmm_readnext           ; 6
_tmm_splash0:
        addi    r27,r27,1               ; 7

        .align  0x20
;--------
        b       _tmm_cl8x               ; 0
_tmm_cl8:
        b       _tmm_cl9                ; 1
_tmm_cl8x:

        cmplw   r29,r23                 ; 2
        bge     _tmm_readnext           ; 3
        mr      r29,r23                 ; 4

_tmm_readnext:
        addi    r23,r23,4               ; 5
        bdnz    _tmm_read               ; 6
        blr                             ; 7

        .align  0x20
;--------
_tmm_cl9:
        ; clear counters
        li      r17,0                   ; 0
        li      r18,0                   ; 1
        li      r19,0                   ; 2
        li      r27,0                   ; 3
        li      r28,0                   ; 4
        li      r29,0                   ; 5

;----------------------------------------------------------------------
; Post Code 3 (Installed memory test code)
;-----------------------------------------------------------------------

        lis     r15, 0x03000000@h
        bl      SetPostCode

;----------------------------------------------------------------------
; Test Splash
;----------------------------------------------------------------------
        b       _ts_cl0
        .align  0x20
;--------
_ts_cl0:
        b       _ts_cl1                 ; 0

TestSplash:
        lis     r25,0x80000000@h        ; 1
        addis   r26,0,0xAAAAAAAA@h      ; 2
        ori     r26,r26,0xAAAAAAAA@l    ; 3
        bl      TestMemory              ; 4
        not     r26,r26                 ; 5
        bl      TestMemory              ; 6
        nop                             ; 7

        .align  0x20
;--------
        b       _ts_cl1x                ; 0
_ts_cl1:
        b       _ts_cl2                 ; 1
_ts_cl1x:

;----------------------------------------------------------------------
; Post Code 4, 5, 6, 7
;-----------------------------------------------------------------------

        lis     r15,0x04000000@h        ; 2
        mr.     r16,r27                 ; 3
        beq     _pc2                    ; 4
        oris    r15,r15,0x02000000@h    ; 5
_pc2:   mr.     r16,r28                 ; 6
        beq     _pc1                    ; 7

        .align  0x20
;--------
        b       _ts_cl2x                ; 0
_ts_cl2:
        b       _ts_cl3                 ; 1
_ts_cl2x:

        oris    r15,r15,0x01000000@h    ; 2
_pc1:   srwi    r29,r29,2               ; 3
        or      r15,r15,r29             ; 4
        bl      SetPostCode             ; 5
        nop                             ; 6
        nop                             ; 7

        .align  0x20
;--------
        b       _ts_cl3x                ; 0
_ts_cl3:
        ; Compare ID
        cmplw   r20, r21                ; 1
        beq     TestSplash              ; 2

_ts_cl3x:

;----------------------------------------------------------------------
; Stop if splash is not good
;-----------------------------------------------------------------------
_stop:
        mr.     r16,r27
        bne     _stop
        mr.     r16,r28
        bne     _stop

;-----------------------------------------------------------------------
; Program an EXI DMA of BS2 out of the ROM and into memory.
; Then jump to it.
; start on a cacheline
; Perform as much as possible out of the loop
;-----------------------------------------------------------------------

        ; DMA the data down from the ROM

; R2 is the EXI base
        addis   r2,r0,EXI_BASE@h
        ori     r2,r2,EXI_BASE@l

; R6 - select the ROM
        addis   r6,r0,EXI_CPRVAL@h
        ori     r6,r6,EXI_CPRVAL@l

; R7 - get it ready to go
        addis   r7,r0,EXI_CRIMMVAL@h
        ori     r7,r7,EXI_CRIMMVAL@l

; R8 - test bit
        li      r8, 1

; R9 - program the DMA
        addis   r9,r0,EXI_CRDMAVAL@h
        ori     r9,r9,EXI_CRDMAVAL@l

; R10 - for deselecting EXI
        addi    r10,r0,0x0000

; R11, R12 - set up ROM CMD increment
        addis   r11,r0,EXI_MAXDMA@h
        ori     r11,r11,EXI_MAXDMA@l
        addis   r12,r0,RAMPCMD_LEN@h
        ori     r12,r12,RAMPCMD_LEN@l

; Load BS2
        addis   r3,r0,BS2_ROMCMD@h
        ori     r3,r3,BS2_ROMCMD@l
        addis   r4,r0,BS2_DEST@h
        ori     r4,r4,BS2_DEST@l
        addis   r13,r0,BS2_LEN@h
        ori     r13,r13,BS2_LEN@l

; exiDma: break request into multiple xfers of 1K or less
; ROM Src address must be 1K aligned!!!
; r3  ROM READ command
; r4  DEST addres
; r13 LEN

; preload the cachelines before jumping into memory
; NOTE: 8 instructions per cache line!!!
        b       _cl0
        .align  0x20
;--------
_cl0:   b       _cl1                    ; 1

testDone:
        ; see if we're done
        cmpwi   r13, 0
        beq     gotoBS2

getSz:
        ; compute size of next xfer
        mr      r5, r11
        cmplw   r13, r5
        bgt     loopDma

        ; DMA remainder less than 1K
        mr      r5, r13

loopDma:
        ; perform DMA

; EXI DMA from ROM

; r3 = ROM READ command
; r4 = DEST address
; r5 = LEN
; uses r16

        ; select the ROM
        stw     r6, EXI_0CPR(r2)
;--------
        b       _cl1x
_cl1:   b       _cl2
_cl1x:

        ; Set up the read command
        stw     r3, EXI_0DATA(r2)

        ; need to kill some time between select and command
        lwz     r16, EXI_0CPR(r2)

        ; get it ready to go
        stw     r7, EXI_0CR(r2)

        ; wait for it to finish
_immWait:
        ; get the start bit
        lwz     r16, EXI_0CR(r2)
        ; is it set
        and.    r16, r16, r8
        ; wait for TSTART to stop
        bgt     _immWait
;--------
        b       _cl2x
_cl2:   b       _cl3
_cl2x:

        stw     r4, EXI_0MAR(r2)
        lwz     r4, EXI_0MAR(r2)

        stw     r5, EXI_0LEN(r2)
        lwz     r5, EXI_0LEN(r2)

        ; program the DMA
        stw     r9, EXI_0CR(r2)

        ; wait for it to finish
_dmaWait:
        ; get the start bit
        lwz     r16, EXI_0CR(r2)
;--------
        b       _cl3x
_cl3:   b       _cl4
_cl3x:

        ; is it set
        and.    r16, r16, r8
        bgt     _dmaWait

        ; for deselecting EXI
        stw     r10, EXI_0CPR(r2)

        ; kill time for the next select
        lwz     r16, EXI_0CPR(r2)
        lwz     r16, EXI_0CPR(r2)

        ; bump the ROM CMD
        add     r3, r3, r12
;--------
        b       _cl4x
_cl4:   b       _cl5
_cl4x:

        ; bump the destination
        add     r4, r4, r11
        ; decrement remaining length
        sub     r13, r13, r5

        b       testDone

gotoBS2:
; jump to loaded memory
        addis   r4, r0, BS2_ENTRY@h
        ori     r4, r4, BS2_ENTRY@l
        mtlr    r4
;--------
        b       _cl5x
_cl5:   b       _cl6
_cl5x:

;-----------------------------------------------------------------------
; Disable IPL-ROM
;-----------------------------------------------------------------------
        addis   r6,r0,EXI_CPRROMDIS@h
        ori     r6,r6,EXI_CPRROMDIS@l
        stw     r6, EXI_0CPR(r2)

;-----------------------------------------------------------------------
; Lock 32Mhz mode on the EXI interface
;-----------------------------------------------------------------------
        li      r4, 1
        stw     r4, SI_EXILK(r14)
        lwz     r4, SI_EXILK(r14)
;--------
        b       _cl6x
_cl6:   b       _cl7
_cl6x:

;-----------------------------------------------------------------------
; Clear BI2 address in Lomem to tell __start that BI2 is not read yet
;-----------------------------------------------------------------------
        lis     r4,0x8000
        li      r3,0
        stw     r3,0xf4(r4)
        blr

        .align  0x20
;--------
_cl7:   b       _cl8
        .align  0x20
;--------
_cl8:   b       _cl9
        .align  0x20
;--------
_cl9:   b       _cl10
        .align  0x20
;--------
_cl10:   b      _cl11
        .align  0x20
;--------
_cl11:   b      _cl12
        .align  0x20
;--------
_cl12:   b      _cl13
        .align  0x20
;--------
_cl13:   b      testDone
        .align  0x20
;-------- The end address of the BS1 must be 1K aligned!!!
