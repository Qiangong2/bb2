/*---------------------------------------------------------------------------*
  Project:  Boot Sequence Level 1 (DRIP system memory reset code)
  File:     memrst.s

  Copyright 1999-2000 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: memrst.s,v $
  Revision 1.1.1.1  2004/06/01 21:17:32  paulm
  GC IPLROM source from Nintendo

    
    1     9/08/00 4:07p Tian
    Initial checkin to production tree
    
    2     5/11/00 4:37p Shiki
    Modified for NTD boot ROM.
  $NoKeywords: $
 *---------------------------------------------------------------------------*/
;****************************************************************************
;
;   (C) 1999 ARTX INC..  ALL RIGHTS RESERVED.  UNPUBLISHED -- RIGHTS
;   RESERVED UNDER THE COPYRIGHT LAWS OF THE UNITED STATES.  USE OF A
;   COPYRIGHT NOTICE IS PRECAUTIONARY ONLY AND DOES NOT IMPLY PUBLICATION
;   OR DISCLOSURE.
;
;   THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF
;   ARTX INC..  USE, DISCLOSURE, OR REPRODUCTION IS PROHIBITED WITHOUT
;   THE PRIOR EXPRESS WRITTEN PERMISSION OF ARTX INC..
;
;                   RESTRICTED RIGHTS LEGEND
;
;   Use, duplication, or disclosure by the Government is subject to
;   restrictions as set forth in subparagraph (c)(1)(ii) of the Rights
;   in Technical Data and Computer Software clause at DFARS 252.227-7013
;   or subparagraphs (c)(1) and (2) of Commercial Computer Software --
;   Restricted Rights at 48 CFR 52.227-19, as applicable.
;
;   ArtX Inc.
;   3400 Hillview Ave, Bldg 5
;   Palo Alto, CA 94304
;
;****************************************************************************

#ifdef DRIP

CPRCSEL1        .set    0x00000150      ; CSEL 1, 32Mhz
EXI_BASE        .set    0xCC006800
EXI_0CPR        .set    0x00000000
EXI_0LEN        .set    0x00000008
EXI_0CR         .set    0x0000000C
EXI_0DATA       .set    0x00000010
EXI_CR4B        .set    0x00000035      ; len 4, write, IMM, TSTART, 110101
EXI_CR1B        .set    0x00000005      ; len 1, write, IMM, TSTART, 000101

GPIO_BITS       .set    0xA0010100      ; 0x02800404 << 6
GPIO_ENB        .set    0xA0010200      ; 0x02800408 << 6

GPIO_B6ON       .set    0x40000000      ; 0x40 << 24
GPIO_B6OFF      .set    0x00000000      ; 0x00 << 24

CSEL_MASK       .set    0x00000380

BUS_SPEED       .set    40
USEC            .set    BUS_SPEED / 4
MSECS_500       .set    500 * 1000

// This can't be done with #define's as the name: is position dependent.
PROLOG:         .macro  name
                .text
                .align  4
                .globl  name
name:
                .endm


EPILOG:         .macro  name
                .function "name", name, .-name
                .endm

RLOAD:  .MACRO
    addis       \1,  r0,  \2@H
    ori         \1,  \1,  \2@L
        .ENDM

TBREAD:         .MACRO
; tbread
; r14 = tbL
; r15 = tbh
; r17 = tmp
@tbRdloop:
        mftbu   r15                     ; load from TBU
        mftb    r14                     ; Load from TBL
        mftbu   r17                     ; load from TBU
        cmpw    r17, r15
        bne     @tbRdloop
        .ENDM

; r3  tmp
EXISEL: .MACRO
    RLOAD       r3, CPRCSEL1
    stw         r3, EXI_0CPR(r16)
        .ENDM

; r3  tmp
EXIDESEL: .MACRO
    addi        r3,  r0,  0x0000
    stw         r3,  EXI_0CPR(r16)
        .ENDM

; in r3 - val
; in r4 - CRVAL
EXIIMM: .MACRO
    stw         r3, EXI_0DATA(r16)      ; load the IMM data
    stw         r4, EXI_0CR(r16)        ; load length and start
    li          r3, 1                   ; TSTART bit
@immWait:
    lwz         r4, EXI_0CR(r16)
    and.        r4, r4, r3              ; see if start TSTART is set
    bgt         @immWait
        .ENDM

; in r3 - start address
; in r4 - count
; modifies r6
MEMTOUCH: .MACRO
    RLOAD       r3, \1
    RLOAD       r4, \1End
@loop:
    lwz         r6,  0(r3)
    addi        r3, r3, 4
    cmplw       r3, r4
    blt         @loop
        .ENDM

; r3 - USECs to delay
PROLOG  __udel
; r20, r21 new time
; r14, r15 cur time

    ; get current time
    TBREAD

    ; compute time to stop
    li          r4, USEC
    mullw       r3, r4, r3
    add         r20, r14, r3
    mr          r21, r15
    cmplw       r20, r14
    bge         @loop1
    addi        r21, r21, 1

@loop1:
    ; get current time
    TBREAD

    cmplw       r14, r20
    blt         @loop1
    cmplw       r15, r21
    blt         @loop1

    blr
__udelEnd:
EPILOG __udel

; immediate mode xfer EXI0, CSEL 1
; output a value to an address
; in: r3 - address
; in: r4 - value
; modifies r6, r7
PROLOG RTCOutByte
    mr  r6, r3          ; save the args
    mr  r7, r4

    ; EXI0, CSEL 1, 32Mhz
    EXISEL

    ; IMM address
    mr          r3, r6  ; recover the address
    RLOAD       r4, EXI_CR4B
    EXIIMM

    ; IMM byte
    mr          r3, r7  ; recover the value
    RLOAD       r4, EXI_CR1B
    EXIIMM

    EXIDESEL

    blr
RTCOutByteEnd:
EPILOG RTCOutByte

; uses r3, r4
; r16 is the EXIBASE
; r19 is the saved lr
PROLOG _memReset
    ; save lr
    mflr        r19

    ; load memory with the necessary routines
    MEMTOUCH __udel
    MEMTOUCH RTCOutByte
    MEMTOUCH _memReset

    ; delay/spin while EXI finishes.
    ; check CSEL until it stops and/or delay
    RLOAD       r16, EXI_BASE
    RLOAD       r4,  CSEL_MASK
@cselWait:
    lwz         r3,  EXI_0CPR(r16)
    and.        r4,  r4,  r3              ; see if anything's selected
    bgt         @cselWait

    ; drive it low
    RLOAD       r3, GPIO_BITS
    RLOAD       r4, GPIO_B6OFF
    bl  RTCOutByte

    ; enable the output data
    RLOAD       r3, GPIO_ENB
    RLOAD       r4, GPIO_B6ON
    bl  RTCOutByte

    ; delay
    RLOAD       r3, MSECS_500
    bl          __udel

    ; drive it high
    RLOAD       r3, GPIO_BITS
    RLOAD       r4, GPIO_B6ON
    bl  RTCOutByte

    ; delay
    RLOAD       r3, MSECS_500
    bl          __udel
#if 0

    ; drive it low
    RLOAD       r3, GPIO_BITS
    RLOAD       r4, GPIO_B6OFF
    bl  RTCOutByte

    ; disable the output data
    RLOAD       r3, GPIO_ENB
    RLOAD       r4, GPIO_B6OFF
    bl  RTCOutByte
#endif // 0
    ; restore lr
    mtlr        r19
    blr
_memResetEnd:
EPILOG _memReset

; First, invalidate the L2 tags, then enable the cache.
; This sets some bits which aren't necessary in gecko. Those are used in minnow.
L2_INV          .set    0x19200000
L2_ENABLE       .set    0x99000000

    PROLOG ppcL2Enable
        xor r3, r3, r3
        oris r3, r3, L2_INV@h
        ori r3, r3, L2_INV@l
        mtspr   l2cr, r3
        isync
..l2_inv_loop:
        mfspr r3, l2cr
        andi. r3, r3, 0x1
        cmpi cr0, 0, r3, 0x0
        bne ..l2_inv_loop
        isync
        xor r3, r3, r3
        oris r3, r3, L2_ENABLE@h
        ori r3, r3, L2_ENABLE@l
        mtspr l2cr, r3
        isync
        blr
    EPILOG ppcL2Enable

    PROLOG ppcL2Disable
        RLOAD   r3, 0x00000000
        mtspr   l2cr, r3
        blr
    EPILOG ppcL2Disable

.align   0x20
        .long   0                       ; 0
        .long   1                       ; 1
        .long   2                       ; 2
        .long   3                       ; 3
        .long   4                       ; 4
        .long   5                       ; 5
        .long   6                       ; 6
        .long   7                       ; 7

#endif  // DRIP
