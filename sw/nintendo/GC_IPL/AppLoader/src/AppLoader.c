/*---------------------------------------------------------------------------*
  Project: AppLoader
  File:    AppLoader.c

  Copyright 1998 - 2002 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: AppLoader.c,v $
  Revision 1.1.1.1  2004/06/03 04:57:31  paulm
  GC IPLROM source from Nintendo

    
    38    04/02/13 13:10 Hashida
    Fixed a bug that NR disc checker didn't work.
    
    37    04/01/30 16:38 Hashida
    Added OSExec.
    Converted the reboot module to dol.
    
    36    03/11/27 10:21 Hashida
    Cleared lomem when in IPL for better coverage for the apploader
    problem.
    
    35    03/11/26 15:08 Hashida
    Modified so that the problem doesn't occur if the 2nd game uses patch3
    or later.
    
    34    03/11/25 11:58 Hashida
    Removed the part that shows the revision number (todo #252).
    
    
    33    03/11/17 18:21 Hashida
    Fixed todo #245 (replacing games in IPL causes hangup).
    
    32    03/04/16 22:37 Hashida
    Support dol loading.
    
    31    02/08/28 14:05 Hashida
    Workaround for an IPL bug that i cache in the apploader location is not
    invalidated.
    We workaround this bug by invalidating icache every time we return from
    apploader.
    
    30    02/08/09 13:08 Hashida
    Added const.
    
    29    02/02/05 17:23 Hashida
    Modified to set simulated mem size = physical mem size if it's ">".
    
    28    11/21/01 20:18 Shiki
    Fixed CheckDolLoadAddress() to make it test BOOT_DIAG_* region.

    27    10/12/01 12:50 Shiki
    Fixed GetResponse() to make it work correctly even for the 1st version
    of the IPL. (Note 1st IPL used controller analog mode zero.)

    26    9/04/01 10:05 Shiki
    Fixed GetConsoleType().

    25    8/20/01 11:31 Shiki
    Fixed SIGetResponse() to check standard controllers only (i.e., no
    keyboard, etc.)

    24    8/09/01 5:24p Hashida
    Added MEI bug workaround.

    23    01/07/09 16:33 Shiki
    Modified to sneak pad[3] button status.

    22    6/26/01 9:12a Hashida
    Reverted the last change.

    21    6/06/01 2:53p Hashida
    Added MEI bug workaround.

    20    6/04/01 6:54p Hashida
    Cleaned up.

    19    5/31/01 2:44p Shiki
    Added warning for 7M limit in development mode. (12M limit reverted).

    18    5/30/01 11:09a Hashida
    Changed so that we always do theck against 0x8070_0000.

    17    5/30/01 12:58a Hashida
    Added a check for 7M limit (12M limit when dol limit is off).

    16    2/20/01 10:23p Hashida
    Fixed a bug.

    15    2/05/01 3:01p Hashida
    Changed load order of FST and dol; it checks the position and read from
    whatever comes first (on DDH, FST comes first; on GDEV, dol comes
    first).

    14    1/31/01 12:12a Hashida
    Added check for limiting dol size.

    13    1/15/01 6:51p Hashida
    Added BS2GetBytesLeft function support.

    12    6/07/00 8:17p Hashida
    Changed so that apploader can show error message.

    11    5/10/00 5:16p Hashida
    Removed ORCA definitions (the ones used in this file were #ifdef
    SPLASH).

    10    4/07/00 12:30a Shiki
    Minor fix.

    9     4/07/00 12:22a Shiki
    Clean up.

    8     4/06/00 8:36p Shiki
    Revised not to use small data segments.

    7     4/04/00 11:28a Hashida
    Added BI2 support.

    6     3/10/00 6:43p Hashida
    Modified to set arenaHi properly.

    5     3/10/00 6:27p Hashida
    Added a check (BB2.FSTLength should be <= FSTMaxLength).
    For Orca, ignore FSTAddress and put FST at the bottom of 24MB.

    4     2/01/00 6:05p Tian
    Let BS2 initialize console type

    3     2/01/00 11:45a Tian
    Aligned static data to 32B with compiler directive.

    2     12/23/99 10:43a Hashida
    Removed warnings.

    6     11/15/99 2:24p Tian
    Added BOOT_VERBOSE flag to control Apploader/BS2 verbosity

    5     8/23/99 2:20p Tian
    Moved OSLoMem.h to private.

    4     8/18/99 1:55p Tian
    Cleanup, more useful output.  Fixed bug where BSS initialization was
    not flushed back to memory.

    3     8/17/99 2:03p Tian
    Tiny change.  Not worth commenting on. Ok I added a carriage return.

    2     8/11/99 11:16a Tian
    More output.  Correctly updates BootInfo structure (except for arenaHi,
    which we should not set until pre-proto board availability).

    1     8/10/99 1:05p Tian
    Moved to securebuild/boot/AppLoader.

    4     8/05/99 12:26p Tian
    Removed cache invalidation from init function - it serves no purpose.

    3     8/05/99 12:04p Tian
    Invalidate cache lines before a  load request is sent to BS2.  Fixes
    bug where BSS faulted in the cache lines again.

    2     7/28/99 6:17p Tian
    Cleanup, function comment blocks, made more robust.

    1     7/26/99 11:41a Tian
    Initial checkin
 *---------------------------------------------------------------------------*/

#include <string.h>

#include <dolphin/dolformat.h>
#include <dolphin/os.h>
#include <dolphin/os/OSBootInfo.h>

#include <private/OSLoMem.h>
#include <private/flipper.h>

#include <secure/Boot.h>
#include <secure/DVDLayout.h>

#include <secure/OSBootInfo2.h>
#include <secure/OSExec.h>

void* BI2Addr          : OS_BASE_CACHED + OS_BOOTINFO2_ADDR;
u32   DebugMonSize     : OS_BASE_CACHED + OS_DEBUGMONITOR_SIZE;
void* FreeDebugMonAddr : OS_BASE_CACHED + OS_FREEDEBUGMONITOR_ADDR;
u32   SimulatedMemSize : OS_BASE_CACHED + OS_SIMULATEDMEM_SIZE;

u32   TotalBytes       : OS_BASE_CACHED + OS_BOOT_TOTAL_BYTES;

u16   Pad3Button       : OS_BASE_CACHED + OS_PAD3_BUTTON;

u32   DolOffsetToBoot  : OS_BASE_CACHED + OS_DOL_OFFSET_TO_BOOT;

#include "Check.h"

/*
  This apploader uses a state machine to figure out what to load next.
 */
typedef enum
{
    START,              // Start
    LOAD_BB2,           // Loads bootblock 2 into static variable.
    LOAD_BI2_HEADER,    // Loads bootinfo 2 header (first 32bytes)
    LOAD_BI2,           // Loads entire bootinfo 2
    LOAD_FST_1,         // Loads the FST into the FST area in high memory
    LOAD_DOL_HEADER,    // Loads header of application into static variable
    INIT_BSS,           // bzeroes BSS area
    LOAD_TEXT,          // While in this state, text segments are loaded
    LOAD_DATA,          // While in this state, data segments are loaded
    LOAD_FST_2,         // Loads the FST into the FST area in high memory
    INIT_BOOTINFO,      // Prepares BootInfo structure for the application
    FINISHED,
    LOAD_FIRST_128K
} AppLoaderState;

// XXX reserved1 and 2 can hold some values like argument list
// XXX offset, but apploader only cares the first two values
// XXX in order to determine the memory location of boot info 2
typedef struct
{
    u32 debugMonitorSize;
    u32 consoleSimulatedMemSize;
    u32 reserved1;
    u32 reserved2;
} BI2Header;

// Note AppLoader cannot use small data segments:

// Note that the region address and size must both be 32 byte aligned.
__declspec(section ".data") DVDBB2          BB2         ATTRIBUTE_ALIGN(32);
__declspec(section ".data") DolImage        DolHeader   ATTRIBUTE_ALIGN(32);
__declspec(section ".data") BI2Header       Top32BofBI2 ATTRIBUTE_ALIGN(32);

__declspec(section ".data") AppLoaderState  CurrentState;   // state of statemachine
__declspec(section ".data") u32             LastIndex;      // index for iterations
__declspec(section ".data") u32             LastOffset;     // last disk offset
__declspec(section ".data") BOOL            DolFirst;       // if TRUE, read dol before FST
__declspec(section ".data") u32             BootDolOffset;  // dol file to boot
__declspec(section ".data") OSExecParams    ExecParams;

__declspec(section ".data") void          (*Report)(const char* msg, ...);

// Prototypes
void   AppLoaderInterface( AppInitFunc*     init,
                           AppGetNextFunc*  getnext,
                           AppGetEntryFunc* getentry );

static void  InitAppLoader( void (*report)(const char* msg, ...) );
static BOOL  GetNextPart  ( void**, u32*, u32* );
static void* GetEntryPoint( void );

#define DUMMY_READ_LOCATION             5

__declspec(section ".data")     static BOOL         SecondTimeForThePart = FALSE;
__declspec(section ".data")     static void*        SavedDest;
__declspec(section ".data")     static u32          SavedLength;
__declspec(section ".data")     static u32          SavedOffset;
__declspec(section ".data")     static AppLoaderState       SavedState;

#define ONE_READ_LIMIT                  (128 * 1024)

/*---------------------------------------------------------------------------*
  Name:         DivideRead

  Description:  Each read should be followed by a seek or a dummy read so
                that the read doesn't hit the cache while the drive is filling
                the prefetch buffer. DivideRead() is to prepare for issuing
                a dummy read

  Arguments:    dest    pointer to pointer to destination address
                length  pointer to variable containing length of transfer
                        (value is 32 byte aligned)
                offset  pointer to variable containing disk offset.
                        This offset is from BEGINNING of disk (not the user
                        area).  I.e. BS2 always uses DVDReadAbsAsyncForBS.
                state   pointer to the current state

  Returns:      None
 *---------------------------------------------------------------------------*/
static void DivideRead(void** dest, u32* length, u32* offset, AppLoaderState* state)
{
    if (SecondTimeForThePart)
    {
        SecondTimeForThePart = FALSE;

        *dest = SavedDest;
        *length = SavedLength;
        *offset = SavedOffset;
        *state = SavedState;

#if BOOT_VERBOSE
        Report("Issuing (Dest, length, offset) = (0x%08x, %d, %08x) second\n", *dest, *length,
               *offset);
#endif
    }
    else
    {
        if (*length <= ONE_READ_LIMIT)
        {
            // if read size is less than 4 blocks, no need to divide
            // Keep /secondTimForThePart/ FALSE
            return;
        }

        SecondTimeForThePart = TRUE;

        SavedDest = (void*)((u32)*dest + ONE_READ_LIMIT);
        SavedLength = *length - ONE_READ_LIMIT;
        SavedOffset = *offset + ONE_READ_LIMIT;
        SavedState = *state;

#if BOOT_VERBOSE
        Report("Instead of (Dest, length, offset) = (0x%08x, %d, %08x), ", *dest, *length,
               *offset);
#endif

        *length = ONE_READ_LIMIT;
        *state = LOAD_FIRST_128K;

#if BOOT_VERBOSE
        Report("Issuing (Dest, length, offset) = (0x%08x, %d, %08x) first\n", *dest, *length,
               *offset);
#endif
    }
}


/*---------------------------------------------------------------------------*
  Name:         Invalidate IC

 *---------------------------------------------------------------------------*/
#define STACKFRAME_SIZE     8
#define RETURNFUNC_MAXSIZE  0x80
#define RETURNFUNC_ENTRY    (BOOT_BS2_BASE - RETURNFUNC_MAXSIZE)

// dummy entry points
static void    __ReturnFromStubsStart(void);
static void    __ReturnFromStubsEnd(void);

// Stubs
asm void StubAppLoaderInterface
(
    AppInitFunc*     init,
    AppGetNextFunc*  getnext,
    AppGetEntryFunc* getentry
);
static asm void StubInitAppLoader(void (*report)(const char* msg, ...));
static asm BOOL StubGetNextPart(void**  dest,
                                u32*    length,
                                u32*    offset);
static asm void* StubGetEntryPoint(void);


static asm void ReturnFromStubs(void)
{
entry __ReturnFromStubsStart

    nofralloc

    // Leave r3 to keep return values

    // Invalidate ICache
    lis     r4,BOOT_APPLOADER_BASE@ha
    addi    r4,r4,BOOT_APPLOADER_BASE@l

    lis     r5,BS2_APPLOADER_MAX_SIZE@ha
    addi    r5,r5,BS2_APPLOADER_MAX_SIZE@l

    rlwinm  r6, r4, 0, 0, 26
    add     r7, r4, r5
    sub     r7, r7, r6   
    addi    r7, r7, 31
    srwi    r7, r7, 5
    mtctr   r7
_loop:
    icbi    0,  r6
    addi    r6, r6, 32
    bdnz    _loop
    sync
    isync
    
    // Return from stubs
    lwz       r0, STACKFRAME_SIZE+4(rsp)
    mtlr      r0
    addi      rsp, rsp, STACKFRAME_SIZE
    blr

entry __ReturnFromStubsEnd
    nop
}

asm void StubAppLoaderInterface
(
    AppInitFunc*     init,
    AppGetNextFunc*  getnext,
    AppGetEntryFunc* getentry
)
{
#pragma unused(init, getnext, getentry)
    // Allocate stack frame manually
    nofralloc

    mflr      r0
    stwu      rsp, -STACKFRAME_SIZE(rsp)
    stw       r0, STACKFRAME_SIZE+4(rsp)

    bl      AppLoaderInterface

    lis     r5, RETURNFUNC_ENTRY@ha
    addi    r5,r5,RETURNFUNC_ENTRY@l
    mtlr    r5
    blr
}

static asm void StubInitAppLoader(void (*report)(const char* msg, ...))
{
#pragma unused (report)
    // Allocate stack frame manually
    nofralloc

    mflr      r0
    stwu      rsp, -STACKFRAME_SIZE(rsp)
    stw       r0, STACKFRAME_SIZE+4(rsp)

    bl      InitAppLoader

    lis     r5, RETURNFUNC_ENTRY@ha
    addi    r5,r5,RETURNFUNC_ENTRY@l
    mtlr    r5
    blr
}


static asm BOOL StubGetNextPart(void**  dest,
                                u32*    length,
                                u32*    offset)
{
#pragma unused (dest, length, offset)
    // Allocate stack frame manually
    nofralloc

    mflr      r0
    stwu      rsp, -STACKFRAME_SIZE(rsp)
    stw       r0, STACKFRAME_SIZE+4(rsp)

    bl      GetNextPart

    lis     r5, RETURNFUNC_ENTRY@ha
    addi    r5,r5,RETURNFUNC_ENTRY@l
    mtlr    r5
    blr
}

static asm void* StubGetEntryPoint(void)
{
    // Allocate stack frame manually
    nofralloc

    mflr      r0
    stwu      rsp, -STACKFRAME_SIZE(rsp)
    stw       r0, STACKFRAME_SIZE+4(rsp)

    bl      GetEntryPoint

    lis     r5, RETURNFUNC_ENTRY@ha
    addi    r5,r5,RETURNFUNC_ENTRY@l
    mtlr    r5
    blr
}

/*---------------------------------------------------------------------------*
  Name:         SIGetResponse

  Description:  Read polling commands results of the specified channel

  Arguments:    chan        channel to check status
                data        8 byte buffer to receive command response

  Returns:      SI status
 *---------------------------------------------------------------------------*/

#define CMD_PORT_AND_DATA       0x40u
#define CMD_ANA_MD_MASK         0x00000700u

static void GetResponse(s32 chan, void* data)
{
    if ((__SIRegs[SI_POLL_IDX] & (SI_POLL_EN0_MASK >> chan)) &&
        (__SIRegs[SI_1OUTBUF_IDX * chan] & ~CMD_ANA_MD_MASK) == (CMD_PORT_AND_DATA << 16))
    {
        ((u32*) data)[0] = __SIRegs[SI_1OUTBUF_IDX * chan + SI_0INBUFH_IDX];
        ((u32*) data)[1] = __SIRegs[SI_1OUTBUF_IDX * chan + SI_0INBUFL_IDX];
    }
    else
    {
        ((u32*) data)[0] = ((u32*) data)[1] = 0;
    }
}

/*---------------------------------------------------------------------------*
  Name:         GetConsoleType

  Description:  Returns the console type

  Arguments:    None.

  Returns:      One of the following values:
                    OS_CONSOLE_RETAIL3
                    OS_CONSOLE_RETAIL2
                    OS_CONSOLE_RETAIL1
                    OS_CONSOLE_DEVHW3
                    OS_CONSOLE_DEVHW2
                    OS_CONSOLE_DEVHW1
 *---------------------------------------------------------------------------*/
static u32 GetConsoleType(void)
{
    OSBootInfo* bootInfo;

    bootInfo = (OSBootInfo*) OSPhysicalToCached(OS_BOOTINFO_ADDR);
    return bootInfo->consoleType;
}

/*---------------------------------------------------------------------------*
  Name:         AppLoaderInterface

  Description:  Returns the 3 key functions that BS2 will use to load in
                the application.  The types are determined in secure/Boot.h
                To be safe, this should be the FIRST function in the file!

  Arguments:    init        *OUT*   output ptr to init function
                getnext     *OUT*   output ptr to "GetNextPart" function
                getentry    *OUT*   output ptr to "GetEntryPoint" function

  Returns:      None.
 *---------------------------------------------------------------------------*/
void AppLoaderInterface
(
    AppInitFunc*     init,
    AppGetNextFunc*  getnext,
    AppGetEntryFunc* getentry
)
{
    void*       dst;
    void*       src;
    u32         len;

    *init     = StubInitAppLoader;
    *getnext  = StubGetNextPart;
    *getentry = StubGetEntryPoint;

    // Copy return func
    dst = (void*)RETURNFUNC_ENTRY;
    src = (void*)__ReturnFromStubsStart;
    len = (u32)((u8*)__ReturnFromStubsEnd - (u8*)__ReturnFromStubsStart);

    CHECK(len <= RETURNFUNC_MAXSIZE);

    memcpy(dst, src, len);
    DCStoreRange(dst, len);
    ICInvalidateRange(dst, len);
}


/*---------------------------------------------------------------------------*
  Name:         InitAppLoader

  Description:  Prepares AppLoader for loading the application.
                BS2 may call InitAppLoader many times (whenever the disk must
                be re-read).

  Arguments:    report      pointer to report function for debugging

  Returns:      None
 *---------------------------------------------------------------------------*/
void InitAppLoader(void (*report)(const char* msg, ...))
{
    // This is necessary because if BS2 resets, for whatever reason,
    // we want to be sure that the static structures have no residual
    // information.
    memset(&BB2,       0, sizeof(DVDBB2));
    memset(&DolHeader, 0, sizeof(DolImage));
    LastIndex    = 0;
    CurrentState = START;
    LastOffset   = 0;
    Report       = report;

    // Move execparams to a safe place
    __OSGetExecParams(&ExecParams);

#if BOOT_VERBOSE
    if (ExecParams.valid)
    {
        Report("ExecParams exists\n");
        Report("bootdol = 0x%08x\n", ExecParams.bootDol);
        Report("argsUseDefault = %d\n", ExecParams.argsUseDefault);
        Report("argsAddr = 0x%08x\n", ExecParams.argsAddr);
    }
#endif
    
//#if BOOT_VERBOSE
    Report("\nApploader Initialized.\n");
    Report("This Apploader built %s %s\n", __DATE__, __TIME__);
//#endif
}

/*---------------------------------------------------------------------------*
  Name:         GetDolSize

  Description:  Get total bytes of all sections in dol. Assuming we already
                read DOL header

  Arguments:    None

  Returns:      dol size
 *---------------------------------------------------------------------------*/
static u32 GetDolSize(void)
{
    u32             total;
    u32             i;

    total = 0;
    for (i = 0; i < DOL_MAX_TEXT; i++)
    {
        if (DolHeader.textData[i])
        {
            total += OSRoundUp32B(DolHeader.textLen[i]);
        }
    }

    for (i = 0; i < DOL_MAX_DATA; i++)
    {
        if (DolHeader.dataData[i])
        {
            total += OSRoundUp32B(DolHeader.dataLen[i]);
        }
    }

    return total;
}


/*---------------------------------------------------------------------------*
  Name:         CheckDolLoadAddress

  Description:  Check if all the sections in dol is under specified address

  Arguments:    border              address to check against

  Returns:      TRUE if good, FALSE otherwise
 *---------------------------------------------------------------------------*/
static BOOL CheckDolLoadAddress(u32 border)
{
    int i;
    u32 end;

    for (i = 0; i < DOL_MAX_TEXT; i++)
    {
        if (DolHeader.textData[i] == NULL)
        {
            continue;
        }
        end = DolHeader.text[i] + DolHeader.textLen[i];
        if (BOOT_DIAG_BASE <= DolHeader.text[i] && end <= BOOT_DIAG_END)
        {
            continue;
        }
        if (border < end)
        {
            return FALSE;
        }
    }

    for (i = 0; i < DOL_MAX_DATA; i++)
    {
        if (DolHeader.dataData[i] == NULL)
        {
            continue;
        }
        end = DolHeader.data[i] + DolHeader.dataLen[i];
        if (BOOT_DIAG_BASE <= DolHeader.data[i] && end <= BOOT_DIAG_END)
        {
            continue;
        }
        if (border < end)
        {
            return FALSE;
        }
    }

    end = DolHeader.bss + DolHeader.bssLen;
    if (BOOT_DIAG_BASE <= DolHeader.bss && end <= BOOT_DIAG_END)
    {
        return TRUE;
    }
    if (border < end)
    {
        return FALSE;
    }

    return TRUE;
}

/*---------------------------------------------------------------------------*
  Name:         CopyArgs

  Description:  Overwrites the argument portion of bi2

  Arguments:    src                     source of the BI2 image to overwrite
                                        with.
                dst                     destination of the BI2. Usually this
                                        is the copy of the disc's BI2.

  Returns:      None
 *---------------------------------------------------------------------------*/
static void CopyArgs(void* src, void* dst)
{
    u32         argoff;
    
    argoff = *(u32*)((u32)src + OS_BI2_ARGOFFSET_OFFSET);
    *(u32*)((u32)dst + OS_BI2_ARGOFFSET_OFFSET) = argoff;
    
    memcpy((void*)((u32)dst + (OS_BI2_SIZE - OS_BI2_ARGSIZE_MAX)), 
           (void*)((u32)src + (OS_BI2_SIZE - OS_BI2_ARGSIZE_MAX)),
           OS_BI2_ARGSIZE_MAX);
}


/*---------------------------------------------------------------------------*
  Name:         PrintBB2

  Description:  Dumps BB2 info

  Arguments:    None

  Returns:      None
 *---------------------------------------------------------------------------*/
static void PrintBB2(void)
{
#if BOOT_VERBOSE
    Report("  boot file position .... 0x%x\n", BB2.bootFilePosition);
    Report("  FST position .......... 0x%x\n", BB2.FSTPosition);
    Report("  FST length ............ 0x%x\n", BB2.FSTLength);
    Report("  FST max length ........ 0x%x\n", BB2.FSTMaxLength);
    Report("  FST address ........... 0x%x\n", (u32)BB2.FSTAddress);
    Report("  user position ......... 0x%x\n", BB2.userPosition);
    Report("  user length ........... 0x%x\n", BB2.userLength);
#endif
}


/*---------------------------------------------------------------------------*
  Name:         InitBootInfo

  Description:  Initializes OSBootInfo in lo-mem

  Arguments:    None

  Returns:      None
 *---------------------------------------------------------------------------*/
static void InitBootInfo(OSBootInfo* bi)
{
    bi->magic       = OS_BOOTINFO_MAGIC;
    bi->version     = 0x01;
    // memorySize is set by BS2
    bi->arenaLo     = NULL;
    bi->arenaHi     = BB2.FSTAddress;
    // consoleType is set by BS2
    bi->FSTLocation = BB2.FSTAddress;
    bi->FSTMaxLength= BB2.FSTMaxLength;
}


/*---------------------------------------------------------------------------*
  Name:         GetNextPart

  Description:  Primary entry to the apploader.  BS2 uses this routine
                to "query" the apploader as to what to load next.  All args
                are pointers to variables that GetNextPart can fill in,
                and BS2 will use those as parameters to DVDReadAbsAsyncForBS.

                Note that this routine can perform any other actions
                necessary for the game, such as initializing BSS, the
                BootInfo structure, etc.

  Arguments:    dest    pointer to pointer to destination address
                length  pointer to variable containing length of transfer
                        (value is 32 byte aligned)
                offset  pointer to variable containing disk offset.
                        This offset is from BEGINNING of disk (not the user
                        area).  I.e. BS2 always uses DVDReadAbsAsyncForBS.

  Returns:      TRUE    if more parts for BS2 to load
                FALSE   Nothing more for BS2 load
 *---------------------------------------------------------------------------*/
static BOOL GetNextPart(
    void**  dest,
    u32*    length,
    u32*    offset
)
{
    OSBootInfo* bi = (OSBootInfo*) OSPhysicalToCached(OS_BOOTINFO_ADDR);
    u32         dolLimit;
    u32         dolSize;
    u16         input[4];

#if BOOT_VERBOSE
    Report("Apploader GetNextPart\n");
#endif
    switch (CurrentState)
    {
      case START:
#if BOOT_VERBOSE
        Report("Apploader[START]\n");
#endif
        // FALL THROUGH

      case LOAD_BB2:
#if BOOT_VERBOSE
        Report("Apploader[LOAD_BB2]\n");
#endif
        *dest   = &BB2;
        *length = OSRoundUp32B(sizeof(DVDBB2));
        *offset = DVDLAYOUT_BB2_POSITION;
        CurrentState = LOAD_BI2_HEADER;
        DCInvalidateRange(*dest, *length);
        break;

      case LOAD_BI2_HEADER:
        PrintBB2();
#if BOOT_VERBOSE
        Report("Apploader[LOAD_BI2_HEADER]\n");
#endif
        CHECKMSG2(BB2.FSTLength <= BB2.FSTMaxLength,
                  "APPLOADER ERROR >>> FSTLength(%d) in BB2 is greater than FSTMaxLength(%d)\n",
                  BB2.FSTLength, BB2.FSTMaxLength);
        *dest   = &Top32BofBI2;
        *length = OSRoundUp32B(sizeof(Top32BofBI2));
        *offset = DVDLAYOUT_BI2_POSITION;
        CurrentState = LOAD_BI2;
        DCInvalidateRange(*dest, *length);
        break;

      case LOAD_BI2:
        DebugMonSize     = Top32BofBI2.debugMonitorSize;
        FreeDebugMonAddr = (void*) OSRoundDown32B(OS_BASE_CACHED + bi->memorySize - DebugMonSize);
        SimulatedMemSize = Top32BofBI2.consoleSimulatedMemSize;
        CHECKMSG1(DebugMonSize % 32 == 0,
                  "APPLOADER ERROR >>> Debug monitor size (%d) should be a multiple of 32\n",
                  DebugMonSize);
        CHECKMSG1(SimulatedMemSize % 32 == 0,
                  "APPLOADER ERROR >>> Simulated memory size (%d) should be a multiple of 32\n",
                  SimulatedMemSize);
#if BOOT_VERBOSE
        Report("DebugMonSize     = %d\n",     DebugMonSize);
        Report("FreeDebugMonAddr = 0x%08x\n", FreeDebugMonAddr);
        Report("SimulatedMemSize = 0x%08x\n", SimulatedMemSize);
#endif
        if (SimulatedMemSize == 0)
        {
            SimulatedMemSize = bi->memorySize;
        }

        // XXX We hack BB2 here.
        // XXX We set FST location at the bottom of 24MB and ignore what
        // XXX Marlin sets in BB2. This scheme might be changed in the future.
#if BOOT_VERBOSE
        Report("Changed FST address from 0x%08x ", BB2.FSTAddress);
#endif
        if (SimulatedMemSize < bi->memorySize)
        {
            CHECKMSG3(DebugMonSize < bi->memorySize - SimulatedMemSize,
                      "APPLOADER ERROR >>> [Physical memory size(0x%x)] - [Console simulated memory size(0x%x)]\n"
                      "APPLOADER ERROR >>> must be greater than debug monitor size(0x%x)\n",
                      bi->memorySize, SimulatedMemSize, DebugMonSize);

            // we put FST just below SimulatedMemSize
            BB2.FSTAddress = (void*) OSRoundDown32B(OS_BASE_CACHED + SimulatedMemSize - BB2.FSTMaxLength);
        }
        else
        {
            if (SimulatedMemSize > bi->memorySize)
            {
                Report("APPLOADER WARNING >>> Physical memory size is 0x%x bytes.\n"
                       "APPLOADER WARNING >>> Console simulated memory size (0x%x) must be smaller than or equal to the Physical memory size\n"
                       "APPLOADER WARNING >>> Use physical memory size as the console simulated memory size.....\n",
                       bi->memorySize, SimulatedMemSize);
                SimulatedMemSize = bi->memorySize;
            }
            // we put FST just below debug monitor
            BB2.FSTAddress = (void*) OSRoundDown32B((u32) FreeDebugMonAddr - BB2.FSTMaxLength);
        }
        
#if BOOT_VERBOSE
        Report("to 0x%08x\n", BB2.FSTAddress);
#endif

        BI2Addr = (void*) ((u32) BB2.FSTAddress - OS_BI2_SIZE);
        *dest   = BI2Addr;
        *length = OS_BI2_SIZE;
        *offset = DVDLAYOUT_BI2_POSITION;
        CurrentState = LOAD_FST_1;
        DCInvalidateRange(*dest, *length);
        break;

      case LOAD_FST_1:
        if (ExecParams.valid)
        {
            // Not in IPL
            // Boot the specified dol file
            BootDolOffset = ExecParams.bootDol;
            if (BootDolOffset == 0)
            {
                // If no dol file is specified, boot the default dol
                BootDolOffset = BB2.bootFilePosition;
            }

            // We use argsUseDefault flag to decide whether we use BI2
            // in the disc (default; used for OSResetSystem(RESTART) for
            // backward compatibility) or load arguments from the address
            // specified in ExecParams.
            if (!ExecParams.argsUseDefault && ExecParams.argsAddr)
            {
                CopyArgs(ExecParams.argsAddr, BI2Addr);
            }
        }
        else
        {
            // in IPL
            BootDolOffset = BB2.bootFilePosition;

            // Clear Lomem because it may already have been poluted.
            DolOffsetToBoot = 0;
        }

        DolFirst = (BootDolOffset < BB2.FSTPosition);
        if (DolFirst)
        {
            CurrentState = LOAD_DOL_HEADER;
            // FALLTHROUGH
        }
        else
        {
#if BOOT_VERBOSE
            Report("Apploader[LOAD_FST]\n");
#endif
            *dest   = BB2.FSTAddress;
            *length = OSRoundUp32B(BB2.FSTLength);
            *offset = BB2.FSTPosition;
            CurrentState = LOAD_DOL_HEADER;
            CHECKMSG1(BOOT_FST_BASE <= (u32) *dest,
                      "APPLOADER ERROR >>> Illegal FST destination address! (0x%x)\n",
                      (u32) *dest);
            DCInvalidateRange(*dest, *length);
            DivideRead(dest, length, offset, &CurrentState);
            break;
        }

      case LOAD_DOL_HEADER:
#if BOOT_VERBOSE
        Report("Apploader[LOAD_DOL_HEADER]\n");
#endif
        *dest   = &DolHeader;
        *length = OSRoundUp32B(sizeof(DolImage));
        *offset = BootDolOffset;
        CurrentState = INIT_BSS;
        DCInvalidateRange(*dest, *length);
        break;

      case INIT_BSS:
#if BOOT_VERBOSE
        Report("Apploader[INIT_BSS]\n");
        Report("<0x%x> - <0x%x>\n", DolHeader.bss, DolHeader.bssLen);
#endif
        dolSize = GetDolSize();
        dolLimit = *(u32*)((u32)BI2Addr + OS_BI2_DOLSIZE_LIMIT);
        if (dolLimit)
        {
            if (!ExecParams.valid)
            {
                // in IPL
                CHECKMSG4(dolSize <= dolLimit,
                          "APPLOADER ERROR >>> Total size of text/data sections of the dol file are too big (%d(0x%08x) bytes). Currently the limit is set as %d(0x%08x) bytes\n",
                          dolSize, dolSize, dolLimit, dolLimit);
            }
        }
        if (DolFirst)
            TotalBytes = dolSize + OSRoundUp32B(BB2.FSTLength);
        else
            // FST already read
            TotalBytes = dolSize;

        if (!ExecParams.valid)
        {
            // in IPL
            if (!(GetConsoleType() & OS_CONSOLE_DEVELOPMENT))
            {
                // production mode
                CHECKMSG1(CheckDolLoadAddress(BOOT_PRODBOOT_START_ADDR),
                          "APPLOADER ERROR >>> One of the sections in the dol file exceeded its boundary. "
                          "All the sections should not exceed 0x%08x (production mode)\n",
                          BOOT_PRODBOOT_START_ADDR);
            }
            else
            {
                // development mode
                if (!CheckDolLoadAddress(BOOT_PRODBOOT_START_ADDR))
                {
                    Report("\n\nAPPLOADER WARNING >>> One of the sections in the dol file exceeded its boundary. "
                           "All the sections should not exceed 0x%08x in production mode.\n\n",
                           BOOT_PRODBOOT_START_ADDR);
                }
                CHECKMSG1(CheckDolLoadAddress(BOOT_APPLOADER_BASE),
                          "APPLOADER ERROR >>> One of the sections in the dol file exceeded its boundary. "
                          "All the sections should not exceed 0x%08x (development mode).\n",
                          BOOT_APPLOADER_BASE);
            }
        }
        memset((void*) DolHeader.bss, 0, DolHeader.bssLen);
        DCFlushRange((void*) DolHeader.bss, DolHeader.bssLen);
        CurrentState = LOAD_TEXT;
        // FALL THROUGH

      case LOAD_TEXT: // use last offset to remember where to resume
#if BOOT_VERBOSE
        Report("Apploader[LOAD_TEXT]\n");
#endif

        // Find next non-NULL text segment
        while (DolHeader.textData[LastIndex] == NULL && LastIndex < DOL_MAX_TEXT)
        {
            LastIndex++;
        }
        if (LastIndex == DOL_MAX_TEXT)
        {
            // No more text segments
            CurrentState = LOAD_DATA;
            LastIndex    = 0;
            // FALL THROUGH
        }
        else
        {
            *dest   = (void*) DolHeader.text[LastIndex];
            *length = OSRoundUp32B(DolHeader.textLen[LastIndex]);
            *offset = (u32) DolHeader.textData[LastIndex] + BootDolOffset;
            if (!ExecParams.valid)
            {
                // in IPL
                CHECKMSG2((u32) *dest + *length <= BOOT_APPLOADER_BASE,
                          "APPLOADER ERROR >>> Too big text segment! (0x%x - 0x%x)\n",
                          (u32) *dest, (u32) *dest + *length);
            }
            LastIndex++;
            DCInvalidateRange(*dest, *length);
            break;
        }

      case LOAD_DATA:
#if BOOT_VERBOSE
        Report("Apploader[LOAD_DATA]\n");
#endif

        // Find next non-NULL text segment
        while (DolHeader.dataData[LastIndex] == NULL && LastIndex < DOL_MAX_DATA)
        {
            LastIndex++;
        }
        if (LastIndex == DOL_MAX_DATA)
        {
            // No more data segments
            CurrentState = LOAD_FST_2;
            LastIndex    = 0;
            // FALL THROUGH
        }
        else
        {
            *dest   = (void*) DolHeader.data[LastIndex];
            *length = OSRoundUp32B(DolHeader.dataLen[LastIndex]);
            *offset = (u32) DolHeader.dataData[LastIndex] + BootDolOffset;
            if (!ExecParams.valid)
            {
                // in IPL
                CHECKMSG2((u32) *dest + *length <= BOOT_APPLOADER_BASE,
                          "APPLOADER ERROR >>> Too big data segment! (0x%x - 0x%x)\n",
                          (u32) *dest, (u32) *dest + *length);
            }
            LastIndex++;
            DCInvalidateRange(*dest, *length);
            break;
        }

      case LOAD_FST_2:
        if (DolFirst)
        {
#if BOOT_VERBOSE
            Report("Apploader[LOAD_FST]\n");
#endif
            *dest   = BB2.FSTAddress;
            *length = OSRoundUp32B(BB2.FSTLength);
            *offset = BB2.FSTPosition;
            CurrentState = INIT_BOOTINFO;
            CHECKMSG1(BOOT_FST_BASE <= (u32) *dest,
                      "APPLOADER ERROR >>> Illegal FST destination address! (0x%x)\n",
                      (u32) *dest);
            DCInvalidateRange(*dest, *length);
            DivideRead(dest, length, offset, &CurrentState);
            break;
        }
        else
        {
            CurrentState = INIT_BOOTINFO;
            // FALLTHROUGH
        }

      case INIT_BOOTINFO:
#if BOOT_VERBOSE
        Report("Apploader[INIT_BOOTINFO]\n");
#endif
        InitBootInfo(bi);
        CurrentState = FINISHED;
        // FALL THROUGH

      case FINISHED:
#if BOOT_VERBOSE
        Report("Apploader[FINISHED]\n");
#endif

        if (!ExecParams.valid)
        {
            // in IPL
            GetResponse(3, input);
            Pad3Button = input[0];
        }
        return FALSE;
        // NOT REACHED HERE

      case LOAD_FIRST_128K:
#if BOOT_VERBOSE
        Report("Apploader[LOAD_FIRST_128K]\n");
#endif
        CHECK(SecondTimeForThePart == TRUE);
        DivideRead(dest, length, offset, &CurrentState);
        break;

      default:
#if BOOT_VERBOSE
        Report("Apploader[UNKNOWN]\n");
#endif
        return FALSE;
        // NOT REACHED HERE
    }
    return TRUE;
}


/*---------------------------------------------------------------------------*
  Name:         GetEntryPoint

  Description:  Returns the application entry point to start the program
                running

  Arguments:    None

  Returns:      ptr to entry point as specified in dol file.
 *---------------------------------------------------------------------------*/
static void* GetEntryPoint(void)
{
    return (void*) DolHeader.entry;
}
