/*---------------------------------------------------------------------------*
  Project:  AppLoader
  File:     Check.h

  Copyright 1998-2000 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: Check.h,v $
  Revision 1.1.1.1  2004/06/03 04:57:31  paulm
  GC IPLROM source from Nintendo

    
    1     9/08/00 4:06p Tian
    Initial checkin to production tree
    
    1     4/06/00 11:41p Shiki
    Initial check-in.
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#ifndef __CHECK_H__
#define __CHECK_H__

#include <dolphin/base/PPCArch.h>

#define CHECK(exp)                                                      \
    (void) ((exp) ||                                                    \
            (Report("Failed assertion " #exp), PPCHalt(), 0))

#define CHECKMSG(exp, msg)                                              \
    (void) ((exp) ||                                                    \
            (Report((msg)), PPCHalt(), 0))
#define CHECKMSG1(exp, msg, param1)                                     \
    (void) ((exp) ||                                                    \
            (Report((msg),                                              \
                   (param1)), PPCHalt(), 0))
#define CHECKMSG2(exp, msg, param1, param2)                             \
    (void) ((exp) ||                                                    \
            (Report((msg),                                              \
                   (param1), (param2)), PPCHalt(), 0))
#define CHECKMSG3(exp, msg, param1, param2, param3)                     \
    (void) ((exp) ||                                                    \
            (Report((msg),                                              \
                   (param1), (param2), (param3)), PPCHalt(), 0))
#define CHECKMSG4(exp, msg, param1, param2, param3, param4)             \
    (void) ((exp) ||                                                    \
            (Report((msg),                                              \
                   (param1), (param2), (param3), (param4)), PPCHalt(), 0))

#ifdef __cplusplus
}
#endif

#endif  // __CHECK_H__
