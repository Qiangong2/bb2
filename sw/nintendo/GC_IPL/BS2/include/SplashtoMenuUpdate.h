/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: Splashデモ Menu関連ルーチン
  -----------------------------------------------------------------------------*/
#ifndef SPLASHTOMENUUPDATE_H
#define SPLASHTOMENUUPDATE_H

/*-----------------------------------------------------------------------------
  enums
  -----------------------------------------------------------------------------*/
enum eMenu_FadeState
{
	cMFS_in_dec,
	cMFS_in_spring
};

/*-----------------------------------------------------------------------------
  外部参照可能な関数
  -----------------------------------------------------------------------------*/
u32 inSplashToMenuUpdate( void );
MtxPtr	getSplashToMenuMtx( void );

#endif //SPLASHTOMENUUPDATE_H
