/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: メニューモード時の各面のパラメータ
  -----------------------------------------------------------------------------*/
#ifndef OPTIONSTATE_H
#define OPTIONSTATE_H

#ifdef __cplusplus
extern "C" {
#endif

/*-----------------------------------------------------------------------------
  enums
  -----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
  OptionState構造体
  -----------------------------------------------------------------------------*/
typedef
struct OptionState
{
	s32 cube_r;
	s32 cube_g;
	s32	cube_b;
	s32	cube_a;

	s16	moji_r;
	s16	moji_g;
	s16	moji_b;
	
	f32	step;
	u16 dummy;

	s16 concent_dec_pos;
	s16 concent_dec_alpha;

	f32	sound_pos_y;
	f32 yoko_pos_y;
	f32 lang_y;
	f32 lang_sel_y;
} OptionState;

/*-----------------------------------------------------------------------------
  外部参照関数のプロトタイプ
  -----------------------------------------------------------------------------*/
void	initOption( BOOL );
void	initOptionState( OptionState* p );
OptionState*	getOptionState( void );

#ifdef __cplusplus
}
#endif

#endif // OPTIONSTATE_H
