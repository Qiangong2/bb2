/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: メモリーカードメニュー時の各面のパラメータ
  -----------------------------------------------------------------------------*/
#ifndef CARDUPDATE_H
#define CARDUPDATE_H

#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif

#define IPL_MAX_ICON_DISPLAY_WIDTH	(4)
#define IPL_MAX_ICON_DISPLAY_HEIGHT	(4)
#define IPL_MAX_ICON_DISPLAY_NUM	((IPL_MAX_ICON_DISPLAY_WIDTH * IPL_MAX_ICON_DISPLAY_HEIGHT))
#define IPL_CMDWIN_MERGIN_X	(40)

enum {
	IPL_CARD_FILE_FADEIN,
	IPL_CARD_FILE_SELECTING,
	IPL_CARD_FILE_WINDOW,
	IPL_CARD_FILE_VERIFY,
	IPL_CARD_FILE_FORMAT_VERIFY,
	IPL_CARD_FILE_WAIT,
	IPL_CARD_FILE_FADEOUT,
	IPL_CARD_FILE_COPYING,
	IPL_CARD_FILE_COPY_DONE,
	IPL_CARD_FILE_COPY_MISS,
	IPL_CARD_FILE_MOVING,
	IPL_CARD_FILE_MOVE_DONE,
	IPL_CARD_FILE_MOVE_MISS,
	IPL_CARD_FILE_DELETEING,
	IPL_CARD_FILE_DELETE_DONE,
	IPL_CARD_FILE_DELETE_MISS,
	IPL_CARD_FILE_RETURN,
	IPL_CARD_FILE_LAST
};

enum {
	IPL_CARD_WINDOW_MOVE,
	IPL_CARD_WINDOW_COPY,
	IPL_CARD_WINDOW_DELETE,
	IPL_CARD_WINDOW_LAST
//	IPL_CARD_WINDOW_FORMAT
};

enum {
	IPL_CARD_CMDWND_FADEIN,
	IPL_CARD_CMDWND_FADEOUT,
	IPL_CARD_CMDWND_VERIFY,
	IPL_CARD_CMDWND_LAST
};


typedef
struct CardMenuRunState
{
	s16		main;
	s16		win;
	s16		format;
} CardMenuRunState;

typedef
struct IconAnimeState
{
	s16		now_frame;
} IconAnimeState;

typedef
struct FileArray
{
	u32		fileNo;
	u32		time;
} FileArray;

typedef
struct CmdFade
{
	s16		now_fade_frame;		// 現在のフレーム。
	s16		end_fade_frame;		// fadein/outするフレーム数。( 0 - open_frameでfadein)
	s16		now_mini_frame;	// 選択時のフレーム。
	s16		end_mini_frame;	// 選択時のフレーム。
	s16		moji_frame;	// ベリファイの出るフレーム。
	s16		veri_frame;	//
	u16		anime_frame;	// ふちのアニメーション管理用フレーム
	
	u8		now_alpha;		// 現在のalpha
	u8		max_alpha;		// 表示されたときのalpha
	s16		slide_y;		// 出てくるときにスライドしてくる距離。( -方向なら下から）
	s16		veri_slide_y;
	
	u32		tag;			// 描画するタグ

	u16		cmd_height;		// コマンド時の高さ。
	u16		select_height;	// コマンド選択時の高さ
	u16		verify_height;	// ベリファイウィンドウの高さ
	u16		full_height;
	/*
	s16		txt0_frame;		// コマンド0のアニメフレーム
	s16		txt1_frame;		// コマンド1のアニメフレーム
	s16		txt2_frame;		// コマンド2のアニメフレーム
	  */
} CmdFade;

void	initCardMenu( BOOL );
s32		updateCardMenu( void );
void	drawCardMenu( u8 , u8 , u8 );
int		getCardRunstate( void );
s32		getCardSelectFileNo( void );
s32		getCardSelectChan( void );
s32		loadIconAnime( s32 no, s32 fileNo );
void	setCardIcon( void );
void	resetCardMessage( void );

void	initCardMenu_Lang( void );

/*-----------------------------------------------------------------------------
  stdlib.h
  -----------------------------------------------------------------------------*/
typedef int (*_compare_function)(void*, void*);                 /* mm 961031 */
void  qsort(void*, size_t, size_t, _compare_function);                      /* mm 961031 */

#ifdef __cplusplus
}
#endif

#endif //CARDUPDATE_H