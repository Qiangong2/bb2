/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: J3DBinary 1.5 Renderer
  
  構造体定義
  -----------------------------------------------------------------------------*/
#ifndef GMRSTRUCT_H
#define GMRSTRUCT_H

//#include <dolphin.h>

#ifdef __cplusplus
extern "C" {
#endif

/*-----------------------------------------------------------------------------
  モデルデータヘッダ
  -----------------------------------------------------------------------------*/
typedef
struct J3DGraphBinaryHeader
{
	u32				signature;					// シグネチャ。(J3DB)
	u32				dataType;					// バイナリデータの種類。
	u32				dataSize;					// バイナリデータの大きさ。
	u32				numBlocks;					// データブロックの数。
	u32				reserved[3];				// 予約。
	u32				userWork;					// ユーザ定義領域。
	char			dataArea[1];				// データ領域（可変長）。
} J3DGraphBinaryHeader;

/*-----------------------------------------------------------------------------
  モデルデータ用の構造体
  -----------------------------------------------------------------------------*/
typedef
struct J3DDataBlockHeader
{
	u32				kind;		// データブロックの種類
	u32				size;		// データブロックの大きさ
} J3DDataBlockHeader;

/*-----------------------------------------------------------------------------
  Hierarchy
  -----------------------------------------------------------------------------*/
enum
{
	cNodeExit		= 0,
	cNodeBegin		= 1,
	cNodeEnd		= 2,
	cNodeJoint		= 16,
	cNodeMaterial	= 17,
	cNodeShape		= 18
};

enum
{
	J3DMatRegMax	=	2,	// Ｊ３Ｄで扱うマテリアルレジスタの数
	J3DColorChanMax	=	4,	// Ｊ３Ｄで扱うチャンネルコントロールの数
	J3DTexCoordMax	=	8,	// Ｊ３Ｄで扱うテクスチャ座標設定の数
	J3DTexMtxMax	=	10,	// Ｊ３Ｄで扱うテクスチャ座標生成設定の数
	J3DTexNoMax		=	8,	// Ｊ３Ｄで扱うテクスチャの数
	J3DTevRegMax	=	4,	// Ｊ３Ｄで扱うＴＥＶレジスタの数
	J3DTevMax		=	16	// Ｊ３Ｄで扱うＴＥＶステージ設定の数
};


typedef
struct J3DModelHierarchy
{
	u16						kind;					// モデルノードの種類
	u16						index;					// ノード情報のインデックス
} J3DModelHierarchy;

typedef
struct J3DModelInfoBlock
{
	J3DDataBlockHeader		header;					// データブロックヘッダ。
	u16						modelDataFlag;			// モデルフラッグ（クラシックスケール等）。
    u16						reserved;				// 予約
    u32						polyNum;				// 総ポリゴン数
    u32						vtxNum;					// 総頂点数
	J3DModelHierarchy*		modelHierarchy;			// ノード階層データへのポインタ。
} J3DModelInfoBlock;

/*-----------------------------------------------------------------------------
  Vertex
  -----------------------------------------------------------------------------*/
typedef
struct J3DVertexBlock
{
	J3DDataBlockHeader		header;					// データブロックヘッダ。
	GXVtxAttrFmtList*		vtxAttr;				// 頂点属性データへのポインタ。
	void*					vtxPos;					// 頂点の座標データへのポインタ。
	void*					vtxNrm;					// 頂点の法線データへのポインタ。
    void*					vtxNbt;					// 頂点のＮＢＴデータへのポインタ。
	void*					vtxColor[2];			// 頂点のカラーデータへのポインタ。
	void*					vtxTexCoord[8];			// 頂点のテクスチャ座標データへのポインタ。
} J3DVertexBlock;

/*-----------------------------------------------------------------------------
  Envelop
  -----------------------------------------------------------------------------*/
typedef
struct J3DEnvelopBlock
{
	J3DDataBlockHeader		header;					// データブロックヘッダ。
	u16						wEvlpMtxNum;			// エンベロープ用マトリクスの総数
	u16						reserved;				// 予約

	u8*						wEvlpMixMtxNum;			// 混ぜ合わせるインデックスの個数列
	u16*					wEvlpMixMtxIndex;		// 混ぜ合わせるマトリクスインデックス列
	f32*					wEvlpMixWeight;			// 混ぜ合わせるマトリクスウエイト列
    Mtx*					invJointMtx;			// エンベロープ用インバースマトリクス列
} J3DEnvelopBlock;

/*-----------------------------------------------------------------------------
  Draw
  -----------------------------------------------------------------------------*/
typedef
struct J3DDrawBlock
{
	J3DDataBlockHeader		header;					// データブロックヘッダ。
	u16						drawMtxNum;				// 描画用行列の数。
	u16						reserved;				// 予約。

	u8*						drawMtxFlag;			// フルウエイトかウエイティッドかのフラグ列
	u16*					drawMtxIndex;			// フルウエイトかウエイティッドかそれぞれでのインデックス列
} J3DDrawBlock;

/*-----------------------------------------------------------------------------
  Joint
  -----------------------------------------------------------------------------*/

typedef
struct J3DTransformInfo
{
    f32 sx;
    f32 sy;
    f32 sz;
    s16 rx;
    s16 ry;
    s16 rz;
    s16 dummy;
    f32	tx;
    f32 ty;
    f32 tz;
} J3DTransformInfo;


typedef
struct J3DJointInitData
{
    u16					mKind;
    u16					dummy;
    J3DTransformInfo	mTransformInfo;
    f32					mRadius;
    Vec					mMin;
    Vec					mMax;
} J3DJointInitData;

typedef
struct J3DJointBlock
{
	J3DDataBlockHeader		header;					// データブロックヘッダ。
	u16						jointNum;				// ジョイントの個数。
	u16						reserved;				// 予約。

	J3DJointInitData*		jointInit;				// ジョイントデータへのポインタ。
    u16*					jointID;				// ジョイントID配列へのポインタ。
	void*					jointName;				// ジョイント名テーブルへのポインタ。
} J3DJointBlock;


/*-----------------------------------------------------------------------------
  Material
  -----------------------------------------------------------------------------*/

typedef
struct	J3DMaterialInitData
{
    u8           mMaterialMode;
    u8           mCullModeID;
    u8			 mColorChanNumID;
    u8			 mTexGenNumID;
    u8			 mTevStageNumID;
    u8	 	     mZCompLocID;
    u8			 mZModeID;
    u8			 mDitherID;							// 8

    u16          mColorID		[J3DMatRegMax];		// 12 
    u16          mColorChanID	[J3DColorChanMax];	// 20
    u16          mTexCoordID	[J3DTexCoordMax];	// 36
    u16          mTexMtxID		[J3DTexMtxMax];		// 56 
    u16          mTexNoID		[J3DTexNoMax];		// 72
    u16          mTevOrderID	[J3DTevMax];		// 104
    u16          mTevColorID	[J3DTevRegMax];		// 112
    u16          mTevStageID	[J3DTevMax];		// 144
    u16          mFogID;                            
    u16          mAlphaCompID;                       
    u16          mBlendID;
    u16          mNBTScaleID;                       // 152
} J3DMaterialInitData;

typedef
struct J3DColorChanInfo
{
    u8			enable;
    u8			matSrc;
    u8			lightMask;
    u8			diffuseFn;
    u8			attnFn;
    u8			dummy[3];
} J3DColorChanInfo;

typedef
struct J3DTexCoordInfo
{
    u8			texGenType;
    u8			texGenSrc;
    u8			texMtx;
    u8			dummy;
} J3DTexCoordInfo;

typedef
struct J3DTexMtxInfo
{
    Mtx			mtx;
    u8			texGenType;
    u8			dummy[3];
} J3DTexMtxInfo;

typedef
struct J3DTevOrderInfo
{
    u8			coord;
    u8			map;
    u8			color;
    u8			dummy;
} J3DTevOrderInfo;

typedef
struct J3DTevStageInfo
{
    u8			mode;

    u8			cA;
    u8			cB;
    u8			cC;
    u8			cD;

    u8			cOp;
    u8			cBias;
    u8			cScale;
    u8			cClamp;
    u8			cOutReg;

    u8			aA;
    u8			aB;
    u8			aC;
    u8			aD;

    u8			aOp;
    u8			aBias;
    u8			aScale;
    u8			aClamp;
    u8			aOutReg;

    u8			dummy;
} J3DTevStageInfo;

typedef
struct J3DFogInfo
{
    u8				type;
    u8				xfog;		// X方向フォグの設定
    u16				center;		// X方向フォグの設定
    f32				startz;
    f32				endz;
    f32				nearz;
    f32				farz;
    GXColor			color;
    GXFogAdjTable	table;
} J3DFogInfo;

typedef
struct J3DAlphaCompInfo
{
    u8				comp0;				// GXCompare
    u8				ref0;
    u8				op;					// GXAlphaOp
    u8				comp1;				// GXCompare
    u8				ref1;
    u8				dummy[3];
} J3DAlphaCompInfo;

typedef
struct J3DBlendInfo
{
    u8				type;
    u8				src_factor;
    u8				dst_factor;
    u8				op;
} J3DBlendInfo;

typedef
struct J3DZModeInfo
{
    u8				compare_enable;
    u8				func;
    u8				update_enable;
    u8				dummy;
} J3DZModeInfo;

typedef
struct J3DNBTScaleInfo
{
    u8				enable;
    u8				dummy[3];
    Vec				scale;
} J3DNBTScaleInfo;

typedef
struct J3DMaterialBlock
{
	J3DDataBlockHeader		header;					// データブロックヘッダ。
	u16						materialNum;			// マテリアルの個数。
	u16						reserved;				// 予約。

	J3DMaterialInitData*	materialInit;			// マテリアルデータへのポインタ。
	u16*					materialID;				// マテリアルＩＤテーブル。
	void*					materialName;			// マテリアル名テーブルへのポインタ。
	GXCullMode*				cullMode;			 	// カルモードデータへのポインタ。
	GXColor*				color;					// カラーデータへのポインタ。
    u8*						colorChanNum;			// カラーチャンネル数配列へのポインタ。
	J3DColorChanInfo*		colorChanInfo;			// カラーチャンネル情報配列へのポインタ。
    u8*						texGenNum;				// テクスチャ座標数配列へのポインタ。
	J3DTexCoordInfo*		texCoordInfo;			// テクスチャ座標情報配列へのポインタ。
	J3DTexMtxInfo*			texMtxInfo;				// テクスチャ行列情報配列へのポインタ。
    u16*					texNo;					// テクスチャ番号配列へのポインタ。
	J3DTevOrderInfo*		tevOrderInfo;			// TEVオーダー情報配列へのポインタ。
	GXColorS10*				tevColorInfo;			// TEVカラー情報配列へのポインタ。
    u8*						tevStageNum;			// TEVステージ数配列へのポインタ。
	J3DTevStageInfo*		tevStageInfo;			// TEVステージ情報配列へのポインタ。
	J3DFogInfo*				fogInfo;				// フォグ情報配列へのポインタ。
	J3DAlphaCompInfo*		alphaCompInfo;			// アルファコンペア情報配列へのポインタ。
	J3DBlendInfo*			blendInfo;				// ブレンド情報配列へのポインタ。
	J3DZModeInfo*			zmodeInfo;				// Zバッファ比較モード情報配列へのポインタ。
	GXBool*					zcompLoc;				// Z比較位置情報へのポインタ。
    GXBool*					dither;					// ディザ情報へのポインタ。
    J3DNBTScaleInfo*		nbtScaleInfo;			// NBTスケール情報へのポインタ。
} J3DMaterialBlock;

/*-----------------------------------------------------------------------------
  Shape
  -----------------------------------------------------------------------------*/

typedef
struct J3DShapeInitData {
    u8   	mMtxType;
    u8   	dummy;			 //u8   mDrawBaseType;
    u16  	mMtxGroupNum;
    u16  	mVtxDescOffset;
    u16 	mMtxID;
    u16  	mDrawID;
    u16  	dummy2;          /**/
    f32  	mRadius;
    Vec		mMin;
    Vec 	mMax;
} J3DShapeInitData;

typedef
struct J3DShapeMtxInitData
{
  u16  mBase;
  u16  mNum;
  u32  mOffset;
} J3DShapeMtxInitData;

typedef
struct J3DShapeDrawInitData {
  u32  mSize;
  u32  mOffset;
} J3DShapeDrawInitData;

typedef
struct J3DShapeBlock
{
	J3DDataBlockHeader		header;					// データブロックヘッダ。
	u16						shapeNum;				// シェープの個数（展開後の数）。
	u16						reserved;				// 予約。

	J3DShapeInitData*		shapeInit;				// シェープ情報配列へのポインタ。
	u16*					shapeID;				// シェープID配列へのポインタ。
	void*					shapeName;				// シェープ名テーブルへのポインタ。
	GXVtxDescList*			vtxDescList;			// 頂点ディスクリプタリストへのポインタ。
    u16*					mtxIndexList;			// 
	u8*						drawDisplayList;		// 
	J3DShapeMtxInitData*	mtxInit;				// 
    J3DShapeDrawInitData*	drawInit;				// 
} J3DShapeBlock;


/*-----------------------------------------------------------------------------
  Texture
  -----------------------------------------------------------------------------*/

typedef
struct ResTIMG
{
	u8					format;				//  1B: テクスチャのフォーマット
	u8					transparency;		//  1B: テクスチャの透明度
	u16					width;				//  2B: テクスチャの横幅
	u16					height;				//  2B: テクスチャの高さ
	u8					wrapS; 				//  1B: 横方向のラップモード（GXTexWrapMode）
    u8					wrapT;				//  1B: 縦方向ラップモード　（GXTexWrapMode）

	u8					indexTexture;		//  1B: インデックステクスチャ・フラッグ
	u8 					colorFormat;		//  1B: ＴＬＵＴの形式（GXTlutFmt）
	u16					numColors;			//  2B: 内部カラーパレットのエントリ数
	s32					paletteOffset;		//  4B: 内部カラーパレット

	u8					enableLOD;			//  1B: ＬＯＤ機能スイッチ
	u8					enableEdgeLOD;		//  1B:
	u8					enableBiasClamp;	//  1B:
	u8					enableMaxAniso;		//  1B:

	u8					minFilter;			//  1B: フィルタ（GXTexFilter）
	u8					magFilter;			//  1B: フィルタ（GXTexFilter）
	s8					minLOD;				//  1B: ＬＯＤ最小値　（固定少数点s4.3フォーマット）
	s8					maxLOD;				//  1B: ＬＯＤ最大値　（固定少数点s4.3フォーマット）
	u8					mipmapLevel;		//  1B: ミップマップに使用するイメージの枚数
	s8					reserved;			//  1B:
	s16		 			LODBias;			//  2B: ＬＯＤバイアス（100倍の値）

	s32					imageOffset;		//  4B: テクスチャイメージへのオフセット
//	u8					imageData[1];		//  nB: テクスチャのイメージ（可変長）
} ResTIMG;

typedef
struct J3DTextureBlock
{
	J3DDataBlockHeader		header;					// データブロックヘッダ。
	u16						textureNum;				// テクスチャの個数。
	u16						reserved;				// 予約。

	ResTIMG*				textureImage;			// テクスチャ情報配列へのポインタ。
	void*					textureName;			// テクスチャ名テーブルへのポインタ。
} J3DTextureBlock;

/*-----------------------------------------------------------------------------
  アニメーション用の構造体
  -----------------------------------------------------------------------------*/
typedef
struct J3DAnmKeyTableBase {
    u16		keyNum;
    u16		offset;
    u16		flag;
} J3DAnmKeyTableBase;

typedef
struct J3DAnmTransformKeyTable {
    J3DAnmKeyTableBase	scl;
    J3DAnmKeyTableBase	rot;
    J3DAnmKeyTableBase	trs;
} J3DAnmTransformKeyTable;

typedef
struct J3DAnmTransformKeyData {
	J3DDataBlockHeader			header;					// データブロックヘッダ。
	u8							attribute;
	u8							shift;
	s16							frameMax;
	u16							anmTableNum;
	u16							scalingNum;
	u16							rotationNum;
	u16							translationNum;

	J3DAnmTransformKeyTable*	anmTable;
	f32*						scaling;
	s16*						rotation;
	f32*						translation;
} J3DAnmTransformKeyData;

typedef
struct gMRPolyAnimeKeyTable
{
	J3DAnmKeyTableBase	r;
	J3DAnmKeyTableBase	g;
	J3DAnmKeyTableBase	b;
	J3DAnmKeyTableBase	a;
	
	// この情報はdpkの<KEY_POLY_RGBA>の[rgba]_paramで構成されます。
} gMRPolyAnimeKeyTable;

typedef
struct gMRPolyAnimeBlock
{
	u32	kind;		// バイナリフォーマットの種類。
	u32	size;		// このブロックのサイズ。
	
	u16	playmode;	// dpkのplaymodeの値。
	u16	max_frame;	// フレーム数。

	s16*	                poly_anime_index;	// Material ID -> gMRPolyAnimeKeyTableへの変換インデックス列。
												// 0xffffは、そのマテリアルにはアニメーションがついていない。
	gMRPolyAnimeKeyTable*	table;				// キーデータ管理行列へのポインタ（アニメーション付のマテリアルのみ）
	f32*	r;	// Red  の フレーム/値/左右スロープ　または　フレーム/値/右スロープ/左スロープ情報列
	f32*	g;	// Greenの フレーム/値/左右スロープ　または　フレーム/値/右スロープ/左スロープ情報列
	f32*	b;	// Blue の フレーム/値/左右スロープ　または　フレーム/値/右スロープ/左スロープ情報列
	f32*	a;	// Alphaの フレーム/値/左右スロープ　または　フレーム/値/右スロープ/左スロープ情報列
	
	// kind は、'IPK1'をいれます。
	// sizeは、ヘッダの構造体を含むバイナリ列全てのサイズです。
	// playmodeは、<KEY_POLY_RGBA_INFO>playmode を0:loop 1:onetimeとして入れます。
	// max_frameは、<KEY_POLY_RGBA>numframesを入れます。（Softimageの場合、全てのマテリアルで同じnumframesらしいです）

	// poly_anime_indexは、dpkの<JOINT>display-><MATERIAL>name-><KEY_POLY_RGBA>でどのマテリアルかを特定し、
	// bmdのMaterialBlockのindexを gMRPolyAnimeKeyTableへのインデックスに変換できるテーブルを作成します。

	// f32* [rgba]は、<KEY_POLY_[{RED}{GREEN}{BLUE}{ALPHA}]>をそのまま入れます。
} gMRPolyAnimeBlock;


#ifdef __cplusplus
}
#endif

#endif //GMRSTRUCT_H
