/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: Global Camera処理

  カメラ用の処理
  -----------------------------------------------------------------------------*/
#ifndef GCAMERA_H
#define GCAMERA_H

void buildCamera( void );//Mtx );
void loadLight( void );//Mtx );
MtxPtr	getCurrentCamera( void );
MtxPtr	gGetSmallCubeCamera( void );
MtxPtr	gGet2DCamera( void );
void	gSet2DCamera( void );

#endif // GCAMERA_H
