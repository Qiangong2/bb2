/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: 2D LayoutRenderer
  -----------------------------------------------------------------------------*/
#ifndef GLRSTRUCT_H
#define GLRSTRUCT_H

#ifdef __cplusplus
extern "C" {
#endif

/*-----------------------------------------------------------------------------
  enums
  -----------------------------------------------------------------------------*/
enum
{
	cMirror_lu_h	= ( 1 << 7 ),
	cMirror_lu_v	= ( 1 << 6 ),
	cMirror_ru_h	= ( 1 << 5 ),
	cMirror_ru_v	= ( 1 << 4 ),
	cMirror_ld_h	= ( 1 << 3 ),
	cMirror_ld_v	= ( 1 << 2 ),
	cMirror_rd_h	= ( 1 << 1 ),
	cMirror_rd_v	= ( 1 << 0 )
};

enum
{
	cBind_center	= 0,
	cBind_right		= 1,
	cBind_Bottom	= 1,
	cBind_left		= 2,
	cBind_Top		= 2
};

enum
{
	cPicBind_d		= ( 1 << 0 ),
	cPicBind_u		= ( 1 << 1 ),
	cPicBind_r		= ( 1 << 2 ),
	cPicBind_l		= ( 1 << 3 )
};

enum
{
	cPicMirror_ud	= ( 1 << 0 ),
	cPicMirror_lr	= ( 1 << 1 ),
	cPicMirror_rot	= ( 1 << 2 ),
	cPicMirror_lap	= ( 3 << 3 )
};

/*-----------------------------------------------------------------------------
【レイアウトデータ】
  ┏━━━━━━━━━━━━━━━━┓
  ┃  gLayoutHeader                 ┃
  ┣━━━━━━━━━━━━━━━━┫
  ┃  PictureBlock                  ┃
  ┃ [gLRPictureの配列]             ┃
         ...........
  ┃                                ┃
  ┣━━━━━━━━━━━━━━━━┫
  ┃  TextBoxBlock                  ┃
  ┃  [gLRTextBoxの配列]            ┃
         ...........
  ┃                                ┃
  ┣━━━━━━━━━━━━━━━━┫
  ┃  WindowBlock                   ┃
  ┃  [gLRWindowの配列]             ┃
         ...........
  ┃                                ┃
  ┗━━━━━━━━━━━━━━━━┛
  
  -----------------------------------------------------------------------------*/



/*-----------------------------------------------------------------------------
  PictureBlock
  -----------------------------------------------------------------------------*/
typedef
struct gLRPictureData
{
	u32		tag;		// タグ名。
	u16		x;			// 左上x座標。
	u16		y;			// 左上y座標。
	u16		dx;			// 横幅。
	u16		dy;			// 高さ。
	u16		textureID;	// テクスチャID。>> 外部のファイルになるので、注意。
	u8		bind;		// バインド情報。
	u8		mirror;		// ミラー情報。
} gLRPictureData;

/*-----------------------------------------------------------------------------
  TextBoxBlock
  -----------------------------------------------------------------------------*/
typedef
struct gLRTextBoxData
{
	u32		tag;		// タグ名。
	u16		x;			// 中央x座標。(	x - dx/2 , x + dx/2 )
	u16		y;			// 中央y座標。(	y - dy/2 , y + dy/2 )にウィンドウを書く。
	u16		dx;			// 横幅。
	u16		dy;			// 高さ。
	u32		color_top;		// 文字の上の色。

	u32		color_bottom;	// 文字の下の色。
	u8		h_bind;			// 文字の横バインド情報。
	u8		v_bind;			// 文字の縦バインド情報。
	s16		char_space;		// 字間。
	s16		line_space;		// 行間。
	u16		font_size;		// 横のフォントサイズ。
	u16		windowID;		// 下に書くとなるウィンドウへのID。(無いときは、0xffff)
	u16		string_length;	// 文字列の長さ。(ないときは、0)

	u32		string_offset;	// 文字列へのオフセット。(ないときは、NULL)	/* 36B */
} gLRTextBoxData;

/*-----------------------------------------------------------------------------
  WindowBlock
  -----------------------------------------------------------------------------*/
typedef
struct gLRWindowData
{
	u32		tag;		// タグ名。
	u16		x;			// 中央x座標。(	x - dx/2 , x + dx/2 )
	u16		y;			// 中央y座標。(	y - dy/2 , y + dy/2 )にウィンドウを書く。
	u16		dx;			// 横幅。
	u16		dy;			// 高さ。
	u16		contents_x;// コンテンツ領域の中央x座標。
	u16		contents_y;// コンテンツ領域の中央y座標。
	u16		contents_dx;// コンテンツ領域の横幅。
	u16		contents_dy;// コンテンツ領域の高さ。
	u16		id_texture_lu;	// テクスチャID。>> 外部のファイルになるので、注意。
	u16		id_texture_ru;	// テクスチャID。>> 外部のファイルになるので、注意。
	u16		id_texture_ld;	// テクスチャID。>> 外部のファイルになるので、注意。
	u16		id_texture_rd;	// テクスチャID。>> 外部のファイルになるので、注意。
	u8		texture_mirror;	// ミラー情報。
	u8		padding[3];
	u32		color_contents_lu;	// コンテンツ領域の左上の色。
	u32		color_contents_ru;	// コンテンツ領域の右上の色。
	u32		color_contents_ld;	// コンテンツ領域の左下の色。
	u32		color_contents_rd;	// コンテンツ領域の右下の色。
} gLRWindowData;


/*-----------------------------------------------------------------------------
  gLayoutHeader
  -----------------------------------------------------------------------------*/
typedef
struct gLayoutHeader
{
	u32			type;			// header情報。'GLH0'が入っている。
	u32			picture_offset;	// gLRPicture[]へのオフセット。
	u32			textbox_offset;	// gLRTextBox[]へのオフセット。
	u32			window_offset;	// gLRWindow[]へのオフセット。
	u16			picture_num;	// pictureの数。
	u16			textbox_num;	// textboxの数。
	u16			window_num;		// windowの数。
} gLayoutHeader;


/*-----------------------------------------------------------------------------
  StringBlock
  -----------------------------------------------------------------------------*/
typedef
struct gLRStringHeader
{
	u32		type;			// 'STH0'
	u16		string_num;		// 文字列の数
	u16		dummy;
} gLRStringHeader;

typedef
struct gLRStringData
{
	u16		textbox_id;		// attributeとして使用するgLRTextBoxのID。
	u16		string_length;	// 文字の長さ。
	u32		string_offset;	// 文字列へのオフセット。	/* 8B */
} gLRStringData;

/*-----------------------------------------------------------------------------
  【テクスチャイメージブロック】
・gLRPictureにはテクスチャイメージブロックへのオフセットが入る。
  テクスチャイメージブロックには、そのレイアウトデータで使用する
  テクスチャデータが全て含まれる。
┌──────────┐
│ ResTIMG            │
├──────────┤
        ......
├──────────┤
│ ResTIMG            │
├──────────┤
│                    │
│ TextureImage       │
│                    │
└──────────┘
（データの並び方は、J3DTextureBlockのResTIMG* textureImageと同じ。）
  -----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
  gTextureHeader
  -----------------------------------------------------------------------------*/
typedef
struct gTextureHeader
{
	u32		type;			// 'TXH0'
	u16		texture_num;	// テクスチャの数。
	u16		dummy[13];
} gTextureHeader;

#ifdef __cplusplus
}
#endif

#endif // GLRSTRUCT_H
