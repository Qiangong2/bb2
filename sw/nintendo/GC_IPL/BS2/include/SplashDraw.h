/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: Splashデモ メインルーチン

  シーケンスを含めたSplashデモのDRAW用ヘッダ
  -----------------------------------------------------------------------------*/
#ifndef SPLASHDRAW_H
#define SPLASHDRAW_H

#include "gModelRender.h"

void	inSplashDrawInit( void );	// 描画の初期化（モデルの初期化など）。

void	inSplashDraw( void );
void	inSplashDrawFirstBoot( void );
void	inSplashDrawSramCrash( void );
gmrModel* inSplashGetLargeCube( void );	// 大きいキューブのモデルインスタンスを返す。
gmrModel* inSplashGetLogo( void );	// ロゴのモデルインスタンスを返す。
gmrModel*	inSplashGetCoverCube( void );

void inMenuDraw( void );
gmrModel* inSplashGetMenuCube( void );	// メニューキューブのモデルインスタンスを返す。

void inSplashToMenuDraw( void );

void initSplashDraw_Lang( void );

//gmrModel* inSplashGetSmallLogo( void ); // 小さいロゴのモデルインスタンスを返す。
gmrModel* inSplashGetNinLogo( void );

#endif // SPLASHDRAW_H
