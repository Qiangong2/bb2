/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: 2D LayoutRenderer
  -----------------------------------------------------------------------------*/
#ifndef GLAYOUTRENDER_H
#define GLAYOUTRENDER_H

#include "gLRStruct.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef
struct gLRFade
{
	s16		now_frame;		// 現在のフレーム。
	s16		fade_frame;		// fadein/outするフレーム数。( 0 - open_frameでfadein)
	s16		wait_frame;	// 【使用してません】
	u16		anime_frame;	// ふちのアニメーション管理用フレーム
	
	u8		now_alpha;		// 現在のalpha
	u8		max_alpha;		// 表示されたときのalpha
	s16		slide_y;		// 出てくるときにスライドしてくる距離。( -方向なら下から）
	
	u32		tag;			// 描画するタグ
} gLRFade;

enum
{
	IPL_WINDOW_FADEIN,
	IPL_WINDOW_FADEOUT,
	IPL_WINDOW_FADEHALF
};

enum {
	IPL_LAYOUT_FADEIN,
	IPL_LAYOUT_FADEOUT,
	IPL_LAYOUT_LAST
};

void	setTexImage( ResTIMG*	image );
void	glrBuildTexImage( void* tex );
void	setGXforglrPicture( void );
void	setGXforglrText( void );
void	setGXforLine( void );

void	glrDrawTexture       ( s16 x, s16 y, ResTIMG* timg );
void	glrDrawPictureAll	 ( gLayoutHeader* header, GXColor* rasc );
void	glrDrawPictureTex	 ( u32 tag, gLayoutHeader* l_hdr, GXColor* rasc, ResTIMG* image_ptr );
void	glrDrawPicture		 ( u32 tag, gLayoutHeader* l_hdr, GXColor* rasc );
void	glrDrawPictureOffset ( u32 tag, gLayoutHeader* l_hdr, GXColor* rasc, s16 offset_x, s16 offset_y );
void	glrDrawTextBox		 ( u32 tag, gLayoutHeader* l_hdr, GXColor* rasc );
void	glrDrawTextBoxString ( u32 tag, gLayoutHeader* l_hdr, GXColor* rasc, char* );
void	glrDrawTextBoxNString( u32 tag, gLayoutHeader* l_hdr, GXColor* rasc, char*, int );
void	glrDrawTextBoxNStringOneLine( u32 tag, gLayoutHeader* l_hdr, GXColor* rasc, char* string, int string_num_max );
void	glrDrawTextBoxNStringTwoLine( u32 tag, gLayoutHeader* l_hdr, GXColor* rasc, char* string, int string_num_max );
void	glrDrawTextBoxID	 ( u16 id , gLayoutHeader* l_hdr, GXColor* rasc, char*, int, BOOL );
void	glrDrawString		 ( u16 id , gLRStringHeader* s_hdr, gLayoutHeader* l_hdr, GXColor* rasc );
void	glrDrawStringPos	 ( u16 id , gLRStringHeader* s_hdr, gLayoutHeader* l_hdr, GXColor* rasc, int x, int y );
void	glrDrawStringOffset  ( u16 id , gLRStringHeader* s_hdr, gLayoutHeader* l_hdr, GXColor* rasc, int offset_x, int offset_y );
void	glrDrawStringBright	 ( u16 id , gLRStringHeader* s_hdr, gLayoutHeader* l_hdr, GXColor* rasc, GXColor* back );
void	glrDrawWindow		 ( u32 tag, gLayoutHeader* l_hdr, GXColor* rasc );
void	glrDrawWindowOffset  ( u32 tag, gLayoutHeader* l_hdr, GXColor* rasc, int offset_x, int offset_y );
void	glrDrawWindowPos	 ( u32 tag, gLayoutHeader* l_hdr, GXColor* rasc, int x, int y );
void	glrDrawWindowPosID	 ( u16 id , gLayoutHeader* l_hdr, GXColor* rasc, int contents_x, int contents_y, int contents_dx, int contents_dy );
void	glrDrawWindowID		 ( u16 id , gLayoutHeader* l_hdr, GXColor* rasc, int contents_dx, int contents_dy );
void	glrDrawWindowOffsetID( u16 id , gLayoutHeader* l_hdr,GXColor* rasc, int offset_x, int offset_y, int contents_dx, int contents_dy );

void	glrUpdateFade( gLRFade* wp, u8 flag );
void	glrResetFade ( gLRFade* wp );
void	glrGetOffsetAlpha( gLRFade* wp, s16* alpha, int* offset_y);
u8		glrDrawFadeWindow( gLRFade* wp, gLayoutHeader* header, GXColor* rasc );
u8		glrDrawFadeString( gLRFade* wp, gLRStringHeader* s_hdr, gLayoutHeader* header, GXColor* rasc );
void	glrInitFadeState( gLRFade* wp );

void	glrDraw2dGrid(  Mtx, u8 alpha );
void	glrDraw2dGridCard( Mtx m,
						   u8 alpha,
						   VecPtr	slot_a_pos,
						   VecPtr	slot_b_pos,
						   u8 slot_a_alpha,
						   u8 slot_b_alpha,
						   u8 main_light_alpha );

#ifdef __cplusplus
}
#endif

#endif //GLAYOUTRENDER_H
