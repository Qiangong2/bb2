/*---------------------------------------------------------------------------*
  File:    smallCubeFader.h
  Copyright 2000 Nintendo.  All rights reserved.

  小キューブでの文字表示ルーチン。

 *---------------------------------------------------------------------------*/
#ifndef __SMALLCUBEFADER_H__
#define __SMALLCUBEFADER_H__


#include <dolphin.h>
#include "gMRStruct.h" // for J3DTransformInfo definition

#ifdef PAL
#define SCF_CUBEBASENUM 715
#else
#define SCF_CUBEBASENUM 500
#endif

#define SCF_CUBENUM (((SCF_CUBEBASENUM*3))) // スタート面のキューブ数＋エフェクト2枚ぶん

typedef struct smallCubeFaderState
{
    s16 state; // SCFS_MAIN, SCFS_OPTION, SCFS_TIME, SCFS_START
	s16 startrunstate;
    s16 startdvdstate; // 0:start 1:nodisk 2:other
	s16 optionrunstate; // IPL_OPTION_SELECTING,IPL_OPTION_SOUND,	IPL_OPTION_OFFSET
    s16 optionselect; // 
    s16 optionoffset; // オフセットの値そのまま
    s16 optionsound; // 0:モノラル　1:ステレオ
	s16 timerunstate; // IPL_TIME_SELECTING, IPL_TIME_DAY, IPL_TIME_CLOCK
    s16 timeselect; //   
    s16 timeday;    // 0:年 1:月 2:日
    s16 timeclock; // 0:時 1:分  2:秒
    s16 timeeditnum[3]; // timeselectの値により、年月日または時分秒
}smallCubeFaderState;


enum
{
    SCFS_MAIN,
    SCFS_OPTION,
    SCFS_TIME,
    SCFS_START,
    SCFS_NUM
};


void initSmallCubeFader( void* faderdata, void* animdata );
J3DTransformInfo* getSmallCubePos(void);
u8*               getSmallCubeAlpha(void);
u32               getSmallCubeNum(void);
GXColor*          getSmallCubeColor1(void);
GXColor*          getSmallCubeColor2(void);

u32  isCubeAnimFinished( u32 frame );


void updateMainSmallCube(void);
void updateOptionSmallCube( u32 state, s16 select_curstate,
							 s16 offset_curstate, s16 sound_curstate );
void updateTimeSmallCube( u32 state, s32 select_curstate,
							s32 day_curstate, s32 clock_curstate,
							int edit_num[] );
void updateStartSmallCube( u32 state, u32 start_nodisk_question );

u32 scfCheckChanged( s16 *prevvalue, s16 newvalue, s16 *oldvalue );
void scfRotateAround( f32 orgx, f32 orgy, f32 orgz, u16 rx, u16 ry, u16 rz,
		   J3DTransformInfo *pos, J3DTransformInfo *diff );
Vec scfGetCenterPos( J3DTransformInfo *pos, u16 *indices, u32 cubenum );
u16 scfDegToU16Rot( f32 deg );
void scfIntTo4Number(s16 *eachnum, int num);
void scfGetTimeNumberToDisplay( s16 numbers[], s16 timerunstate, s16 editnum[] );

smallCubeFaderState *scfGetState(void);
smallCubeFaderState *scfGetPrevState(void);

#endif // __SMALLCUBEFADER_H__
