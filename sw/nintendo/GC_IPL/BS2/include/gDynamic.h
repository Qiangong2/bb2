/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: Dynamics関連ルーチン群
  -----------------------------------------------------------------------------*/
#ifndef GDYNAMIC_H
#define GDYNAMIC_H

#ifdef __cplusplus
extern "C" {
#endif

typedef
struct gXY
{
	f32		x;
	f32		y;
} gXY;

typedef
struct gXYZ
{
	f32		x;
	f32		y;
	f32		z;
} gXYZ;

typedef
struct gSpringX
{
	f32		x;	// 原点からの変移
	f32		v;	// 現在の速度
	f32		prev_v;	// 前回の速度
	f32		k;	// ばね定数
//	f32		m;	// 質量
	f32		sf;	// 静止摩擦係数
	f32		mf;	// 動摩擦係数
	f32		amp; // 減衰時振幅
	s16		frame; // 最終減衰時フレーム
	s16		d_frame; // 減衰値
} gSpringX;

typedef
struct gSpringXY
{
	gXY		xy;	// 原点からのX方向の変移
	gXY		v;	// 現在の速度
	gXY		prev_v;	// 前回の速度
	f32		k;	// ばね定数
//	f32		m;	// 質量
	f32		sf;	// 摩擦係数
	f32		mf;	// 摩擦係数
} gSpringXY;


/*-----------------------------------------------------------------------------
  プロトタイプ
  -----------------------------------------------------------------------------*/
void gInitSpringX( gSpringX* s );
void gUpdateSpringX( gSpringX* s , f32 ef );

#ifdef __cplusplus
}
#endif

#endif // GDYNAMIC_H
