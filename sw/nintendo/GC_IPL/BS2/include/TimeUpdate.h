/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: カレンダーメニュー時の各面のパラメータ
  -----------------------------------------------------------------------------*/
#ifndef TIMEUPDATE_H
#define TIMEUPDATE_H

#ifdef __cplusplus
extern "C" {
#endif

enum {
	IPL_TIME_SELECTING,
	IPL_TIME_DAY,
	IPL_TIME_CLOCK,
	IPL_TIME_LAST
};

void	initTimeMenu( BOOL );
s32		updateTimeMenu( void );
void	drawTimeMenu( u8 ,u8 , u8 );
s32		getTimeRunstate( void );

void	initTimeMenu_Lang( void );

#ifdef __cplusplus
}
#endif

#endif //TIMEUPDATE_H
