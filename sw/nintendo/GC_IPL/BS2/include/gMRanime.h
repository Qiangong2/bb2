/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: J3DBinary 1.5 Renderer
  
  Key Frame Animation�v�Z�Ȃ�
  -----------------------------------------------------------------------------*/
#ifndef GMRANIME_H
#define GMRANIME_H

void setCurrentAnimeModel( gmrModel* model );
void getTransform( f32 frame , u16 joint_no , J3DTransformInfo* result );
void getMatColor( f32 frame, u16 matNo, GXColor* color );

#endif // GMRANIME_H
