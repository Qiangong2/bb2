/*---------------------------------------------------------------------------*
  Project:  Dolphin OS BS2 splash
  File:     BS2Draw.h

  Copyright 1998, 1999 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: BS2Draw.h,v $
  Revision 1.1.1.1  2004/06/01 21:17:32  paulm
  GC IPLROM source from Nintendo

    
    2     5/15/00 1:43p Hashida
    Changed maximum height from 482 to 480 for NTSC and MPAL.
    
    1     2/02/00 5:11p Tian
    Initial checkin - drawing component of splash app
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#ifndef __BS2DRAW_H__
#define __BS2DRAW_H__

#ifdef  __cplusplus
extern C {
#endif  // __cplusplus
// Framebuffer
#define SCREEN_WD       720
#define SCREEN_HT       480
#define PIXEL_SIZE      2
#define FB_START        ((u32)XFB)

typedef struct 
{
    u16         dispX;
    u16         dispY;
    u16         dispW;
    u8          tvFmt;
    u8          tvInt;
    u16         xfbW;
    u16         xfbH;
    VIXFBMode   mode;
    u16         panX;
    u16         panY;
    u16         panW;
    u16         panH;
    BOOL        black;
    
} VIData_s;

// External Framebuffer
extern u8*  XFB;
void        DrawNintendo    ( u8 c, u8 u, u8 v );
void        FillXFB         ( u8 c, u8 u, u8 v);
void        BS2VIInit       ( void );

#ifdef  __cplusplus
}
#endif  // __cplusplu
#endif
