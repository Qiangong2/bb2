/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: Splashデモ Menu関連ルーチン
  -----------------------------------------------------------------------------*/
#ifndef MENUUPDATE_H
#define MENUUPDATE_H

#ifdef __cplusplus
extern "C" {
#endif

/*-----------------------------------------------------------------------------
  enums
  -----------------------------------------------------------------------------*/
enum {
	IPL_MENU_CENTER,
	IPL_MENU_GAMESTART,
	IPL_MENU_GAMESTART_SELECT,
	IPL_MENU_MEMORYCARD,
	IPL_MENU_MEMORYCARD_SELECT,
	IPL_MENU_OPTION,
	IPL_MENU_OPTION_SELECT,
	IPL_MENU_CLOCK,
	IPL_MENU_CLOCK_SELECT,
	IPL_MENU_LAST
};

/*-----------------------------------------------------------------------------
  内部パラメータ
  -----------------------------------------------------------------------------*/
typedef
struct MenuState
{
	f32	y_sort_mergin;
	f32	y_sort_offset;

	f32	fadeout_z_max;

	s32	runstate;		
	s16	bound_frame;	//	ゆれている現在のフレーム。
	s16	menu_rot_x;		// メニューのX軸回転（縦）
	
	s16	menu_rot_y;		// メニューのY軸回転（横）
	s16	menu_rot_speed;	// メニューの回転速度。
	
	s16	fadeout_z_param;
	s16	menu_swing_speed_x;
	s16	menu_swing_speed_y;
	s16	menu_swing_speed_z;
	
	s16	menu_swing_amp_x;
	s16	menu_swing_amp_y;
	s16	menu_swing_amp_z;
	s16 menu_slide_speed_x;
	
	s16 menu_slide_speed_y;
	s16	menu_slide_amp_x;
	s16	menu_slide_amp_y;
	s16	menu_swing_offset_y;
	s16 menu_slide_offset_x;
	u16	enable_pre_key_input;

	/* smallcubefader setting start */
	f32 scf_optionsound_xoffset;
	f32 scf_optionsound_yoffset;
	f32 scf_optionsound_scale;
	f32 scf_optionsound_zoom;
	f32 scf_optionoffset_xoffset;
	f32 scf_optionoffset_yoffset;
	f32 scf_optionoffset_scale;
	f32 scf_optionoffset_zoom;

	f32 scf_optionlang_xoffset;
	f32 scf_optionlang_yoffset;
	f32 scf_optionlang_scale;
	f32 scf_optionlang_zoom;

	s32 scf_optionlang1;
	s32 scf_optionlang2;
	s32 scf_optionlang3;
	s32 scf_optionlang4;
	s32 scf_optionlang5;
	s32 scf_optionlang6;
	
	f32 scf_timedate_xoffset;
	f32 scf_timedate_yoffset;
	f32 scf_timedate_scale;
	f32 scf_timedate_zoom;
	f32 scf_timeclock_xoffset;
	f32 scf_timeclock_yoffset;
	f32 scf_timeclock_scale;
	f32 scf_timeclock_zoom;
	f32 scf_start_xoffset;
	f32 scf_start_yoffset;
	f32 scf_start_scale;
	s16 scf_alphafadeframenum;
	s16 scf_movefadeframenum;
	f32 scf_cubescale;
	s16 scf_darkalpha;
	f32 scf_dance_tx;
	f32 scf_dance_ty;
	f32 scf_dance_ry;
	f32 scf_dance_rz;
	f32 scf_dance_txturnpoint;
	s16 scf_dance_framenum;
	f32 scf_numeffect_width;
	f32 scf_numeffect_tenmag;
	s16 scf_numeffectframe;
	s16 scf_mainanimframenum;
	f32 scf_startdancetime;
	s16 scf_modelalpha;
	s16 scf_cubealpha;
	f32 scf_tx;
	f32 scf_ty;
	f32 scf_tz;
	s16 scf_arrowanimframe;
	f32 scf_arrowx;
	
	f32 scf_soundarrowl_x;
	f32 scf_soundarrowl_y;
	f32 scf_soundarrowr_x;
	f32 scf_soundarrowr_y;
	f32 scf_offsetarrowl_x;
	f32 scf_offsetarrowl_y;
	f32 scf_offsetarrowr_x;
	f32 scf_offsetarrowr_y;
	f32 scf_langarrowl_x;
	f32 scf_langarrowl_y;
	f32 scf_langarrowr_x;
	f32 scf_langarrowr_y;
	
	s16 scf_offsetfadeinframe;
	s16 scf_offsetfadeoutframe;
	s16 scf_handframe;
	/* smallcubefader setting end */
	s16 scf_cubecolor_start_r1;
	s16 scf_cubecolor_start_r2;
	s16 scf_cubecolor_start_g1;
	s16 scf_cubecolor_start_g2;
	s16 scf_cubecolor_start_b1;
	s16 scf_cubecolor_start_b2;
	s16 scf_cubecolor_option_r1;
	s16 scf_cubecolor_option_r2;
	s16 scf_cubecolor_option_g1;
	s16 scf_cubecolor_option_g2;
	s16 scf_cubecolor_option_b1;
	s16 scf_cubecolor_option_b2;
	s16 scf_darkcubecolor_option_r1;
	s16 scf_darkcubecolor_option_r2;
	s16 scf_darkcubecolor_option_g1;
	s16 scf_darkcubecolor_option_g2;
	s16 scf_darkcubecolor_option_b1;
	s16 scf_darkcubecolor_option_b2;
	s16 scf_darkercubecolor_option_r1;
	s16 scf_darkercubecolor_option_r2;
	s16 scf_darkercubecolor_option_g1;
	s16 scf_darkercubecolor_option_g2;
	s16 scf_darkercubecolor_option_b1;
	s16 scf_darkercubecolor_option_b2;
	s16 scf_cubecolor_calendar_r1;
	s16 scf_cubecolor_calendar_r2;
	s16 scf_cubecolor_calendar_g1;
	s16 scf_cubecolor_calendar_g2;
	s16 scf_cubecolor_calendar_b1;
	s16 scf_cubecolor_calendar_b2;
	s16 scf_darkcubecolor_calendar_r1;
	s16 scf_darkcubecolor_calendar_r2;
	s16 scf_darkcubecolor_calendar_g1;
	s16 scf_darkcubecolor_calendar_g2;
	s16 scf_darkcubecolor_calendar_b1;
	s16 scf_darkcubecolor_calendar_b2;
	s16 scf_darkercubecolor_calendar_r1;
	s16 scf_darkercubecolor_calendar_r2;
	s16 scf_darkercubecolor_calendar_g1;
	s16 scf_darkercubecolor_calendar_g2;
	s16 scf_darkercubecolor_calendar_b1;
	s16 scf_darkercubecolor_calendar_b2;
	u16 scf_dance_selected;


	s16 fadein_z_speed;
	s16	fadeout_z_speed;
	s16	fadeout_z_maxalpha;
	f32	fadeout_z_rotnum;

	f32 grid_light_da_dist;
	f32 grid_light_da_bright;
	f32	grid_light_pos_x;
	f32	grid_light_pos_y;
	f32 grid_light_pos_z;
	f32	grid_light_spot;

	s16	color_button_a_r;
	s16	color_button_a_g;
	s16	color_button_a_b;
	s16	color_button_b_r;
	s16	color_button_b_g;
	s16	color_button_b_b;

	s16	disp_wait_frame[3];

} MenuState;

/*-----------------------------------------------------------------------------
  外部参照可能な関数
  -----------------------------------------------------------------------------*/
void	 	MenuInit( BOOL );
u32 		inMenuUpdate( void );
MenuState*	getMenuState( void );
void 		inMenuStateInit( MenuState* p );
void		inMenuStateReset( MenuState* p );
MtxPtr		getMainMenuMtx( void );

// 描画関連。
void		drawMenuMain( u8 alpha );

// splash to menuから必要な、センターメニューの表示。
void		drawCenterSurface( Mtx, u8 alpha );

BOOL		isMenuDispMemoryCard( void );

void		initMenuDraw_Lang( void );
void		setOperationLanguage( int language );

#ifdef __cplusplus
}
#endif

#endif //MENUUPDATE_H
