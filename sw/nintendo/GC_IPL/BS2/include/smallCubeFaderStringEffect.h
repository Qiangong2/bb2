#ifndef __SMALLCUBEFADERSTRINGEFFECT_H__
#define __SMALLCUBEFADERSTRINGEFFECT_H__

#include <dolphin.h>
#include "gMRStruct.h" // for J3DTransformInfo definition

u32 scfGetStringEffectNum(void);
u8 *scfGetStringEffectAlpha(void);
J3DTransformInfo* scfGetStringEffectPos(void);

void scfUpdateOptionStringEffect( J3DTransformInfo *pos );
void scfUpdateStartStringEffect( J3DTransformInfo *pos );

void scfInvalidateStringEffect(void);

#endif // __SMALLCUBEFADERSTRINGEFFECT_H__
