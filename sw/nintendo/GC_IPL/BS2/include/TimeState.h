/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: メニューモード時の各面のパラメータ
  -----------------------------------------------------------------------------*/
#ifndef TIMESTATE_H
#define TIMESTATE_H

#ifdef __cplusplus
extern "C" {
#endif

/*-----------------------------------------------------------------------------
  enums
  -----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
  TimeState構造体
  -----------------------------------------------------------------------------*/
typedef
struct TimeState
{
	s16		cube_hari_r;
	s16		cube_hari_g;
	s16		cube_hari_b;
	s16		cube_hari_a;

	s16		cube_other_r;
	s16		cube_other_g;
	s16		cube_other_b;
	s16		cube_other_a;

	s16	moji_r;
	s16	moji_g;
	s16	moji_b;

	s16		year_x;
	s16		mon_x;
	s16		day_x;
	s16		hour_x;
	s16		min_x;
	s16		sec_x;

	s16 concent_dec_pos;
	s16 concent_dec_alpha;
} TimeState;

/*-----------------------------------------------------------------------------
  外部参照関数のプロトタイプ
  -----------------------------------------------------------------------------*/
void	initTime( BOOL );
void	initTimeState( TimeState* p );
TimeState*	getTimeState( void );

#ifdef __cplusplus
}
#endif

#endif // TIMESTATE_H
