/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: メモリーカードのアイコンの移動
  -----------------------------------------------------------------------------*/
#ifndef CARDICONMOVE_H
#define CARDICONMOVE_H

#ifdef __cplusplus
extern "C" {
#endif

enum
{
	IPL_FILE_SELECT,
	IPL_FILE_NOSELECT,
	IPL_NOFILE_SELECT,
	IPL_NOFILE_NOSELECT
};

typedef
struct IconMoveState
{
	s16		min_frame;		// 目的地が変わったときに更新。
	s16		min_alpha;		// 目的地が変わったときに更新。
	s16		min_scale_frame;//
	s16		now_frame;		// 基本的に、update時に更新。
	s16		now_alpha;		// 基本的に、update時に更新。
	s16		now_scale_frame;// 基本的に、update時に更新。
	s16		max_frame;		// 目的地が変わったときに更新。
	s16		max_alpha;		// 目的地が変わったときに更新。
	s16		max_scale_frame;//
	f32		now_scale;
	f32		min_scale;
	f32		max_scale;
	Vec		now_pos;		// 基本的に、update時に更新。
	Vec		now_slope;		// 基本的に、update時に更新。
	Vec		start_slope;	// 基本的に、update時に更新。
	Vec		start_pos;		// 目的地が変わったときに更新。
	Vec		end_pos;		// 目的地が変わったときに更新。
} IconMoveState;

typedef	IconMoveState	IconMoveStateArray[2][CARD_MAX_FILE];
typedef	IconMoveState	(*IconMoveStateArrayPtr)[CARD_MAX_FILE];


/*-----------------------------------------------------------------------------
  functions
  -----------------------------------------------------------------------------*/
void	initCardIconMove( BOOL first, s16 (*i_x)[IPL_MAX_ICON_DISPLAY_NUM], s16 (*i_y)[IPL_MAX_ICON_DISPLAY_NUM] );
void	updateCardIconMove( void );
void	setCardIconMove( FileArray (*fap)[CARD_MAX_FILE], s8* now_line );
void	drawCardIconMove( u8 chan, u8 alpha );
void	setCardIconPos(	u8 dstFileNo );
void	presetCardIconPos( u8 src_chan, u16 dispPos , u16 fileNo, u16 cmd );
void	cancelCardIconPos( void );
void	clearCardIconMoveSlot( s32 chan );
void	resetCardIconMove( void );
s16		getCardIconMovePosX( s32 chan, s32 fileNo );
s16		getCardIconMovePosY( s32 chan, s32 fileNo );
s16		getCardIconMoveAlpha( s32 chan, s32 fileNo );
BOOL	isCardIconAnimeDone( void );
BOOL	isCardIconFadeDone( void );


#ifdef __cplusplus
}
#endif

#endif //CARDICONMOVE_H
