#ifndef __SMALLCUBEMOVEMENT_H__
#define __SMALLCUBEMOVEMENT_H__
#include <dolphin.h>
#include "gMRStruct.h"

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

	void scfInitCubeMovement(void);
	void scfUpdateStartCubeMovement( J3DTransformInfo *cubepos, u32 cubenum );
	void scfUpdateOptionCubeMovement( J3DTransformInfo *cubepos );
	void scfUpdateTimeCubeMovement( J3DTransformInfo *cubepos );
	J3DTransformInfo* scfGetMovementOffsetPos(void);
	
#ifdef  __cplusplus
}
#endif  // __cplusplus


#endif // __SMALLCUBEMOVEMENT_H__
