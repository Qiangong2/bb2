/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: Menu CardAnim関連ルーチン
  -----------------------------------------------------------------------------*/
#ifndef MENUCARDANIM_H
#define MENUCARDANIM_H

#ifdef __cplusplus
extern "C" {
#endif

typedef
struct CCAnimState
{
	s16		now_x;	// s10.5
	s16		now_y;	// s10.5
	s16		now_z;	// s10.5
//	s16		now_frame;
	s16		start_x;// s10.5
	s16		start_y;// s10.5
	s16		start_z;// s10.5
	s16		start_frame;
	s16		end_x;	// s10.5
	s16		end_y;	// s10.5
	s16		end_z;	// s10.5
	s16		end_frame;

	s16		rot_xy;
	
	u8		alpha;
	u8		max_alpha;
	
} CCAnimState;

void	initCCAnim( gmrModelData* md , BOOL );
void	updateCCAnime( void );
void	updateCCAnimeMtx( void );
void	drawCCAnime( void );
void	clearCCAnime( void );

void	concentCCAnime( void );
BOOL	isConcentCCSmallCube( void );


#ifdef __cplusplus
}
#endif

#endif // MENUCARDANIM_H