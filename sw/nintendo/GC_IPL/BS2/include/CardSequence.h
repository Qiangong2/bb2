/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: メモリーカードの実際の処理
  -----------------------------------------------------------------------------*/
#ifndef CARDSEQUENCE_H
#define CARDSEQUENCE_H

#include <private/CARDDir.h>

#ifdef __cplusplus
extern "C" {
#endif

/*-----------------------------------------------------------------------------
  defines
  -----------------------------------------------------------------------------*/
//#define IPL_CARD_MAX_SLOT 2
#define IPL_CARD_THREAD_STACK		4096
#define IPL_CARD_THREAD_PRIORITY	17
#define IPL_CARD_MES_SIZE			16

#define	IPL_CARD_CMD_MASK		0xff
#define IPL_CARD_RESULT_MASK	0xff
#define	IPL_CARD_SLOT_MASK		1
#define IPL_CARD_FILENO_MASK	0xff
#define IPL_CARD_BROKEN_FILENAME	"Broken File"
#define IPL_CARD_BROKEN_FILENAMESIZE	11
#define IPL_CARD_COPY_BLOCK_SIZE		4
#define IPL_CARD_MAX_FILEBUFFER_SIZE	(256 * 1024)

#define	IPL_CARD_ICON_MAX_SIZE		(22528 + 512)
#define IPL_CARD_COMMENT_SIZE		64

#define getCmdMesCmd( cmd )						(s8)( (cmd) & IPL_CARD_CMD_MASK )
#define getCmdMesResult( cmd )					(s8)( ( (cmd) >> 8 ) & IPL_CARD_RESULT_MASK )
#define getCmdMesSlot( cmd )					(s16)( ( (cmd) >> 16 ) & IPL_CARD_SLOT_MASK )
#define getCmdMesFileno( cmd )					(u16)( ( (cmd) >> 24 ) & IPL_CARD_FILENO_MASK )
#define createCmdMes( slot , cmd )				(((slot) << 16 )|( (u32)(cmd) & IPL_CARD_CMD_MASK ))
#define createResultMes( slot , cmd , result, fileno )		( (((fileno)&IPL_CARD_FILENO_MASK) << 24 ) | (((slot)&IPL_CARD_SLOT_MASK) << 16) | (((result)&IPL_CARD_RESULT_MASK)<<8) | ((cmd)&IPL_CARD_CMD_MASK) )
#define createCmdMesFileNo( slot , cmd , fileno )	 ( (((fileno)&IPL_CARD_FILENO_MASK) << 24 ) | ( ((slot)&IPL_CARD_SLOT_MASK) << 16 ) | ( (u32)(cmd) & IPL_CARD_CMD_MASK ) )

enum {	// ICA 'Ipl Card Slot_runstate '
	SLOT_NOCARD			,
	SLOT_INVALID_ENTRY	,
	SLOT_CARD_INSERTED	,	// ただの識別子なので、この値を取ることはない。
	SLOT_GETTING_ENTRY	,
	SLOT_READY			,
	SLOT_WRONGDEVICE	,
	SLOT_BROKEN			,
	SLOT_BROKEN_NF		,
	SLOT_ENCODING		,
	SLOT_ENCODING_NF	,
	SLOT_CANNOT_USE		,
	SLOT_LAST
};

enum {	// Ipl Command
	IPL_CARD_CMD_MOUNT,
	IPL_CARD_CMD_FORMAT,
	IPL_CARD_CMD_FILECOPY,
	IPL_CARD_CMD_FILEMOVE,
	IPL_CARD_CMD_FILEDELETE,
	IPL_CARD_CMD_FILECOPY_WAIT,
	IPL_CARD_CMD_FILEMOVE_WAIT,
	IPL_CARD_CMD_FILEDELETE_WAIT,
	IPL_CARD_CMD_FORMAT_WAIT,
	IPL_CARD_CMD_UNMOUNT,
	IPL_CARD_CMD_START,
	IPL_CARD_CMD_STOP,
	IPL_CARD_CMD_GETCODESIZE,
	IPL_CARD_CMD_LAST
};

#define IPL_CARD_RESULT_READY           0
#define IPL_CARD_RESULT_BUSY            -21
#define IPL_CARD_RESULT_WRONGDEVICE     -22
#define IPL_CARD_RESULT_NOCARD          -23
#define IPL_CARD_RESULT_EXIST           -24
#define IPL_CARD_RESULT_INSSPACE        -25
#define IPL_CARD_RESULT_NOPERM          -26
#define IPL_CARD_RESULT_SECTOR			-27
#define IPL_CARD_RESULT_FATAL_ERROR     -28


typedef
struct SlotState
{
	vu8		cmd;		// 現在のスロットに対する発行コマンド。
	vu8		changed;	// スロットの状態が変更された。
	vs16		state;		// 現在のスロットの状況。CARD_RESULT_*が入る。
	
	vs32		result;		// 最後に返答のあったコマンドのリザルト。
	vu32		sector_size;
	vu16		full_blocks;
	vu16		free_fileentry;
	vu16		free_blocks;
	vs16		game_blocks;
} SlotState;

typedef
struct DirState
{
	vu16	validate;
	vu16	blocks;
	vu8		copy_enable;
	vu8		move_enable;
	vu16	same_file;
	vu32	time;
} DirState;

typedef
struct IconState
{
	vu8		banner_enable;
	vu8		icon_enable;
	vu8		icon_nums;
	vu8		banner_format;
	
	vs8		icon_bounds;
	vu8		icon_anim_style;
	vu8		icon_anim_first_frame;
	vu8		icon_anim_last_frame;

	vu8		icon_format[8];

	vu16	iconSpeed;
	vs16	icon_all_frame;

	vs32	banner_offset;
	vs32	banner_palette_offset;
	vs32	icon_offset[8];
	vs32	icon_palette_offset;
} IconState;


typedef DirState	DirStateArray[2][CARD_MAX_FILE];
typedef DirState	(*DirStateArrayPtr)[CARD_MAX_FILE];
typedef IconState	IconStateArray[2][CARD_MAX_FILE];
typedef IconState	(*IconStateArrayPtr)[CARD_MAX_FILE];

/*-----------------------------------------------------------------------------
  Function Prototypes
  -----------------------------------------------------------------------------*/
void				initCardThread( void ); 	// カードThreadの初期化などをします。
void				probeCard( void );			// カードの挿入状態をチェックします。

OSMessageQueue*		getCardSendMesQueue( void );
OSMessageQueue*		getCardRecvMesQueue( void );
DirStateArrayPtr	getCardDirState( void );
SlotState*			getCardSlotState( void );

BOOL				sendCardFormatCmd( u8 no );
BOOL				sendCardCopyCmd( u8 no , s16 fileNo);
BOOL				sendCardMoveCmd( u8 no , s16 fileNo );
BOOL				sendCardDeleteCmd( u8 no , s16 fileNo );
BOOL				sendCardThreadStartCmd( void );
BOOL				sendCardThreadStopCmd( void );

IconStateArrayPtr	getIconStateArray( void );
char*				getIconBuffer( u8 no, s16 fileNo );
char*				getIconComment( u8 no, s16 fileNo );

s32					getCardLastSrcSlot( void );
s32					getCardLastSendCmd( void );
s32					getCardLastCmdResult( void );
s32					getCardLastCMDFCmdResult( void );
u8					getCardLastCmdFileNo( void );
u8					getCardThreadState( void );

BOOL				sendCardGetGameCodeBlocks( void );
BOOL				isDoneGameCodeBlocks( void );
BOOL				isStateNotChanged( void );


// Most Dolphin-Card subsystems require 512 byte alignment
#define gRoundUp512B(x)       (((u32)(x) + 512 - 1) & ~(512 - 1))
#define gRoundDown512B(x)     (((u32)(x)) & ~(512 - 1))

#ifdef __cplusplus
}
#endif

#endif // CARDSEQUENCE_H
