#define MSG_D700    0 // [ftxa] Vrij
#define MSG_D701    1 // [ftxb] Vrij
#define MSG_D000    2 // [txt0] Verplaatsen
#define MSG_D001    3 // [txt0] Kopi�ren
#define MSG_D002    4 // [txt0] Wissen
#define MSG_D100    5 // [txt0] Ja
#define MSG_D101    6 // [txt0] Nee
#define MSG_D600    7 // [txt1] Verplaatsenvoltooid.
#define MSG_D601    8 // [txt1] Dezegegevenskunnennietwordenverplaatst.
#define MSG_D602    9 // [txt1] ErbevindtzichgeenMemoryCard(geheugenkaart)inslotA,hetslotwaarjedegegevensnaartoeprobeertteverplaatsen.
#define MSG_D603    10 // [txt1] ErbevindtzichgeenMemoryCard(geheugenkaart)inslotB,hetslotwaarjedegegevensnaartoeprobeertteverplaatsen.
#define MSG_D604    11 // [txt1] HetobjectdatzichinslotAbevindtkannietwordengebruikt.
#define MSG_D605    12 // [txt1] HetobjectdatzichinslotBbevindtkannietwordengebruikt.
#define MSG_D606    13 // [txt1] DeMemoryCard(geheugenkaart)dieinslotAisgestoken,kanbijditmenunietwordengebruikt.
#define MSG_D607    14 // [txt1] DeMemoryCard(geheugenkaart)dieinslotBisgestoken,kanbijditmenunietwordengebruikt.
#define MSG_D608    15 // [txt1] DespelgegevenskunnennietwordenuitgewisseldtussentweeverschillendesoortenMemoryCards(geheugenkaarten).
#define MSG_D609    16 // [txt1] DeMemoryCard(geheugenkaart)waarjedegegevensnaartoewiltverplaatsen,bevataleenkopievandezegegevens.
#define MSG_D610    17 // [txt1] OpdeMemoryCard(geheugenkaart)waarjedegegevensnaartoewiltverplaatsenisnietgenoegruimtebeschikbaar,oferzijnteveelgegevensopgeslagen.
#define MSG_D611    18 // [txt1] Hetkanzijndatdegegevensnietverplaatstzijn.
#define MSG_D200    19 // [txt1] Kopi�renvoltooid.
#define MSG_D201    20 // [txt1] Dezegegevenskunnennietwordengekopieerd.
#define MSG_D202    21 // [txt1] ErbevindtzichgeenMemoryCard(geheugenkaart)inslotA,hetslotwaarjedegegevensnaartoeprobeerttekopi�ren.
#define MSG_D203    22 // [txt1] ErbevindtzichgeenMemoryCard(geheugenkaart)inslotB,hetslotwaarjedegegevensnaartoeprobeerttekopi�ren.
#define MSG_D204    23 // [txt1] HetobjectdatzichinslotAbevindtkannietwordengebruikt.
#define MSG_D205    24 // [txt1] HetobjectdatzichinslotBbevindtkannietwordengebruikt.
#define MSG_D206    25 // [txt1] DeMemoryCard(geheugenkaart)dieinslotAisgestoken,kanbijditmenunietwordengebruikt.
#define MSG_D207    26 // [txt1] DeMemoryCard(geheugenkaart)dieinslotBisgestoken,kanbijditmenunietwordengebruikt.
#define MSG_D208    27 // [txt1] DespelgegevenskunnennietwordengekopieerdtussentweeverschillendesoortenMemoryCards(geheugenkaarten).
#define MSG_D209    28 // [txt1] DeMemoryCard(geheugenkaart)waarjedegegevensnaartoewiltkopi�ren,bevataleenkopievandezegegevens.
#define MSG_D210    29 // [txt1] OpdeMemoryCard(geheugenkaart)waarjedegegevensnaartoewiltkopi�renisnietgenoegruimtebeschikbaar,oferzijnteveelgegevensopgeslagen.
#define MSG_D211    30 // [txt1] Hetkanzijndatdegegevensnietgekopieerdzijn.
#define MSG_D300    31 // [txt1] Degegevenszijngewist.
#define MSG_D301    32 // [txt1] Hetkanzijndatdegegevensnietgewistzijn.
#define MSG_D400    33 // [txt4] SlotAisleeg.
#define MSG_D401    34 // [txt5] SlotBisleeg.
#define MSG_D402    35 // [txt2] HetobjectdatzichinslotAbevindtkannietwordengebruikt.
#define MSG_D403    36 // [txt3] HetobjectdatzichinslotBbevindtkannietwordengebruikt.
#define MSG_D404    37 // [txt2] DeMemoryCard(geheugenkaart)dieinslotAisgestoken,kanbijditmenunietwordengebruikt.
#define MSG_D405    38 // [txt3] DeMemoryCard(geheugenkaart)dieinslotBisgestoken,kanbijditmenunietwordengebruikt.
#define MSG_D406    39 // [txt2] DeMemoryCard(geheugenkaart)inslotAmoetwordengeformatteerd.Nuformatteren?
#define MSG_D407    40 // [txt3] DeMemoryCard(geheugenkaart)inslotBmoetwordengeformatteerd.Nuformatteren?
#define MSG_D408    41 // [txt2] WanneerdeMemoryCard(geheugenkaart)wordtgeformatteerd,gaanalleopgeslagengegevensverloren.Isditok�?
#define MSG_D409    42 // [txt3] WanneerdeMemoryCard(geheugenkaart)wordtgeformatteerd,gaanalleopgeslagengegevensverloren.Isditok�?
#define MSG_D410    43 // [txt2] DeMemoryCard(geheugenkaart)isgeformatteerd.
#define MSG_D411    44 // [txt3] DeMemoryCard(geheugenkaart)isgeformatteerd.
#define MSG_D412    45 // [txt2] HetkanzijndatdeMemoryCard(geheugenkaart)nietisgeformatteerd.
#define MSG_D413    46 // [txt3] HetkanzijndatdeMemoryCard(geheugenkaart)nietisgeformatteerd.
#define MSG_2D_MEMORYCARD_3_PAL_DUTCH_NUM    47
