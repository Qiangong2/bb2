#ifndef GMAINSTATE_H
#define GMAINSTATE_H

#ifdef __cplusplus
extern "C" {
#endif

typedef struct MainState
{
	f32		znear;
	f32		zfar;

	f32		fovy;

	f32		distCamera;
	f32		PosCamera;
	f32		logo_camera_height;
	f32		menu_camera_height;

	u8		update;
	u8		enableProcBar;
	u8		ortho;
	u8		dummy;

	f32		orthoScale;
	f32		orthoPos;

	u8		r;
	u8		g;
	u8		b;
	u8		window_mergin;

	s16		font_mergin;

	s16		key_repeat_threshold;
	s16		key_repeat_repeating;

	s16		bright_alpha_max;
	s16		bright_alpha_min;
	s16		bright_frame;
	s16		bright_frame_max;
	s16		bright_frame_bounds;

} MainState;

MainState* getMainState( void );
void initMainState( MainState* smsp );

#ifdef __cplusplus
}
#endif

#endif // GMAINSTATE_H
