/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: Splashデモ メインルーチン

  シーケンスを含めたSplashデモのメイン用ヘッダ
  -----------------------------------------------------------------------------*/
#ifndef SPLASHMAIN_H
#define SPLASHMAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/*-----------------------------------------------------------------------------
  enums
  -----------------------------------------------------------------------------*/
enum SplashRunState				// 実行ステート管理用
{
	cSpRun_Init		,
	cSpRun_Rotate	,
	cSpRun_Logo		,
	cSpRun_LogotoMenu,
	cMenu_Init		,
	cMenu_Select	,
	cMenu_Exit
};

enum
{
	IPL_SP_MEMCHECK_PRE,
	IPL_SP_MEMCHECK_GETSTATUS,
	IPL_SP_MEMCHECK_NOW,
	IPL_SP_MEMCHECK_GO,
	IPL_SP_MEMCHECK_DONE,
	IPL_SP_MEMCHECK_GOMENU,
	IPL_SP_MEMCHECK_LAST
};

enum
{
	IPL_SP_CLOCK_PRE,
	IPL_SP_CLOCK_NOW,
	IPL_SP_CLOCK_GO,
	IPL_SP_CLOCK_DONE,
	IPL_SP_CLOCK_GOMENU,
	IPL_SP_CLOCK_LAST
};

/*-----------------------------------------------------------------------------
  SplashState構造体
  -----------------------------------------------------------------------------*/
typedef
struct SplashState
{
	s16		pos;				// 現在の位置を角度で。
	s16		radius;				// 現在の半径。
	f32		scale;
	
	s16		rot_y;		// y軸回転。
	s16		start_y;	// Fadein時での回転開始位置。
	
	s32		max_v;		// 最高速。
	f32		rot_v;		// y軸回転速度。
	f32		rot_a;		// y軸回転加速度。
	f32		rot_aa;		// y軸回転加加速度。（定数）
	f32		rot_min_v;	// 原点に戻る回転数。

	s16		logo_rot;	// ロゴの回転角。
	s16		logo_amp;	// ロゴの振動振幅。

	f32		logo_rad_rot; // ロゴのばたつき

	s16		logo_change_menu_cube_frame; // メニューキューブに変わるフレーム
	s16		key_rot_num;	// 止まるまでの回転数。
	s16		key_rot_stop;	// 一旦止まったときの角度。（設定項目）
	s16		key_rot_start;	// 始めの角度。

	s32		key_start_amp_frame;// 減衰し始めるフレーム数。
	s32		key_amp_frame;		// 減衰するのに必要なフレーム数。
	s32		key_stop_frame;		// 止まるのに必要なフレーム数。
	s32		key_now_frame;		// 現在のフレーム
	s32		key_start_surface_frame;	// 表面が出始めるフレーム。
	f32		key_rot_start_v;	// 減衰し始めるときのスロープ（速度）

	f32		anim_min_scale;		// 一番小さい時の値。
	f32		anim_max_scale_y;	// 回転中のyの最大スケール。
	f32		key_scale_xz_slope; // xzスケールアニメのスロープ値。
	f32		key_now_scale_xz;	// xzスケールアニメの現在の値。

	u8		small_cube_alpha;	// 小さいキューブのα。
	u8		large_cube_alpha;	// 大きいキューブのα。
	u8		menu_cube_alpha;	// メニューキューブのα。
	u8		logo_mark_alpha;	// ロゴマークの現在のalpha
	
	s16		bound_z_amp;		// ロゴキューブのz軸周りの振動振幅。
	s16		bound_x_amp;		// ロゴキューブのx軸周りの振動振幅。
	s16		bound_z_amp_max;	// ロゴキューブのz軸周りの振動振幅。
	s16		bound_x_amp_max;	// ロゴキューブのx軸周りの振動振幅。

	s16		bound_rot;			// ロゴキューブの振動角度。
	s16		firstboot_fade_ninlogo;		//
	s16		firstboot_fade_max_ninlogo;	//
	s16		firstboot_fade_a;			//

	s16		firstboot_fade_b;			//
	s16		firstboot_fade_max_a;		//
	s16		firstboot_fade_max_b;		//
	s16		firstboot_state;			//

	s16		sramcrash_fade;				//
	s16		sramcrash_fade_max;			//
	s16		sramcrash_fade_alpha;		//
	s16		sramcrash_window_fade;		//

	s16		sramcrash_window_fade_max;	//
	s16		sramcrash_state;			//
	s16		sramcrash_cursor;			//
	s16		sramcrash_window_mergin;	// メッセージウィンドウとコマンドウィンドウの隙間の距離

	s16		boot_nodisk_fade;
	s16		boot_nodisk_fade_max;
	s16		boot_fatal_fade;
	s16		boot_fatal_fade_max;

	f32		bound_z_amp_atten;
	f32		bound_x_amp_atten;
	f32		bound_y_ave_z;
	f32		bound_y_ave_x;

	s16		bound_amp_rot_delta;
	s16		logo_angle_x;
	s16		logo_angle_y;
	s16		logo_angle_z;

	f32		logo_interact_start_slope;

	s16		spring_frame;
	s16		logo_interact_prev_y;
	s16		logo_interact_start_y;
	s16		key_first;

	s16		force_fatalerror;
	s16		force_cubemenu;

	s16		sramcrash_first_frame;
	s16		cover_open_wait_frame;

	f32		morphcube_sound_speed;
	f32		morphcube_sound_speed_end;

	s16		memorycard_check_state;
	s16		memorycard_check_cursor;
	s16		memorycard_check_frame;
	s16		memorycard_check_needblocks;

	s16		memorycard_check_slot;
	s16		clockadjust_check_cursor;
	s16		clockadjust_check_state;
	s16		clockadjust_check_frame;

	s16		pal_first_boot;
	s16		pal_language_cursor;
	s16		pal_first_boot_alpha;
	s16		pal_dummy;
} SplashState;

/*-----------------------------------------------------------------------------
  外部参照できる関数のプロトタイプ
  -----------------------------------------------------------------------------*/
SplashState* inSplashGetState( void );
void		 inSplashStateInit( SplashState* );
void		 inSplashStateReset( SplashState* p );
void		 setSplashRunstate( u32 state );
u32			 getSplashRunstate( void );

#ifdef __cplusplus
}
#endif

#endif // SPLASHMAIN_H
