/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: J3DBinary 1.5 Renderer
  
  Mtx����
  -----------------------------------------------------------------------------*/
#ifndef GTRANS_H
#define GTRANS_H

#include "gMRStruct.h"

#ifdef __cplusplus
extern "C" {
#endif

void gMTXRotS16(Mtx m, char axis, s16 rot);
void gGetTranslateRotateMtx(s16 rx, s16 ry, s16 rz, f32 tx, f32 ty, f32 tz, Mtx m);
void gGetRotateMtx(s16 rx, s16 ry, s16 rz, Mtx m);
void gGetTransformMtx( J3DTransformInfo* ti, Mtx mtx);
void gScaleNrmMtx(MtxPtr m, Vec* v);
u32  gGetInverseTranspose( Mtx src, Mtx inv );
void gGetVecInterp( f32 time, f32 f0, VecPtr v0, VecPtr s0, f32 f1, VecPtr v1, VecPtr s1, VecPtr result );

#ifdef __cplusplus
}
#endif

#endif // GTRANS_H