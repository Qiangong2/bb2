#define MSG_S000    0 // [txt0] Configuraçãodesistemaperdida.Calendárioeoutrosajustesresetados.Configurarsistemaagora?
#define MSG_S001    1 // [txt0] Sim
#define MSG_S002    2 // [txt0] Não
#define MSG_S100    3 // [txt1] Vocêdeveprimeiroajustarocalendárioeoutrasopções.Pressionequalquertecla.
#define MSG_S200    4 // [txt0] Umerroocorreu.DesligueosistemaeconsulteoManualdeInstruçõesdoNINTENDOGAMECUBE.
#define MSG_S201    5 // [txt0] Odisconãopodeserlido.FavorconsultaroManualdeInstruçõesdoNINTENDOGAMECUBE.
#define MSG_2D_DEMO_2_MPAL_PORTUGUESE_NUM    6
