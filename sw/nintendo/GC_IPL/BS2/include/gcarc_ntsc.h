#define     GC_2d_calendar_2_lay                              0
#define     GC_2d_calendar_2_English_str                      1
#define     GC_2d_calendar_2_Japanese_str                     2
#define     GC_2d_demo_2_lay                                  3
#define     GC_2d_demo_2_English_str                          4
#define     GC_2d_demo_2_Japanese_str                         5
#define     GC_2d_gameplay_2_lay                              6
#define     GC_2d_gameplay_2_English_str                      7
#define     GC_2d_gameplay_2_Japanese_str                     8
#define     GC_2d_memorycard_3_lay                            9
#define     GC_2d_memorycard_3_English_str                    10
#define     GC_2d_memorycard_3_Japanese_str                   11
#define     GC_2d_operation_1_lay                             12
#define     GC_2d_operation_1_Japanese_str                    13
#define     GC_2d_option_2_j_lay                              14
#define     GC_2d_option_2_j_English_str                      15
#define     GC_2d_option_2_j_Japanese_str                     16
#define     GC_3d_calendar_lay                                17
#define     GC_3d_calendar_Japanese_str                       18
#define     GC_3d_gameplay_lay                                19
#define     GC_3d_main_lay                                    20
#define     GC_3d_main_Japanese_str                           21
#define     GC_3d_memorycard_lay                              22
#define     GC_3d_option_lay                                  23
#define     GC_bana_mj_bnr                                    24
#define     GC_bana_pj_bnr                                    25
#define     GC_base_cube_szp                                  26
#define     GC_cover_cube_szp                                 27
#define     GC_cubeanim_szp                                   28
#define     GC_e_sound_mono_120x24_bti                        29
#define     GC_e_sound_stereo_120x24_bti                      30
#define     GC_e_week_0_48x24_bti                             31
#define     GC_e_week_1_48x24_bti                             32
#define     GC_e_week_2_48x24_bti                             33
#define     GC_e_week_3_48x24_bti                             34
#define     GC_e_week_4_48x24_bti                             35
#define     GC_e_week_5_48x24_bti                             36
#define     GC_e_week_6_48x24_bti                             37
#define     GC_english_2d_operation_1_lay                     38
#define     GC_english_2d_operation_1_English_str             39
#define     GC_english_3d_calendar_lay                        40
#define     GC_english_3d_calendar_English_str                41
#define     GC_english_3d_gameplay_lay                        42
#define     GC_english_3d_main_lay                            43
#define     GC_english_3d_main_English_str                    44
#define     GC_english_3d_memorycard_lay                      45
#define     GC_english_3d_option_lay                          46
#define     GC_icon_cube0_szp                                 47
#define     GC_icon_cube1_szp                                 48
#define     GC_ipl_2d_frame_bti                               49
#define     GC_j_blocknumber_110x14_bti                       50
#define     GC_j_num_0_16x22_bti                              51
#define     GC_j_num_1_16x22_bti                              52
#define     GC_j_num_2_16x22_bti                              53
#define     GC_j_num_3_16x22_bti                              54
#define     GC_j_num_4_16x22_bti                              55
#define     GC_j_num_5_16x22_bti                              56
#define     GC_j_num_6_16x22_bti                              57
#define     GC_j_num_7_16x22_bti                              58
#define     GC_j_num_8_16x22_bti                              59
#define     GC_j_num_9_16x22_bti                              60
#define     GC_j_sound_mono_120x24_bti                        61
#define     GC_j_sound_stereo_120x24_bti                      62
#define     GC_j_week_0_24x24_bti                             63
#define     GC_j_week_1_24x24_bti                             64
#define     GC_j_week_2_24x24_bti                             65
#define     GC_j_week_3_24x24_bti                             66
#define     GC_j_week_4_24x24_bti                             67
#define     GC_j_week_5_24x24_bti                             68
#define     GC_j_week_6_24x24_bti                             69
#define     GC_locus_szp                                      70
#define     GC_mark_szp                                       71
#define     GC_mcube_szp                                      72
#define     GC_moji_cube_bmd                                  73
#define     GC_nin_logo_bck                                   74
#define     GC_nin_logo_ipk                                   75
#define     GC_nin_logo_m_szp                                 76
#define     GC_ntsc_tex_szp                                   77
#define     GC_s_cube_szp                                     78
#define     GC_scm2_szp                                       79
#define     GC_smallcubefaderdata_eng_szp                     80
#define     GC_smallcubefaderdata_jpn_szp                     81
