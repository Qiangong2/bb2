/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: Menu OptionAnim関連ルーチン
  -----------------------------------------------------------------------------*/
#ifndef MENUOPTIONANIM_H
#define MENUOPTIONANIM_H

#ifdef __cplusplus
extern "C" {
#endif

typedef
struct OCAnimState
{
	f32		x;
	f32		y;
	f32		nx;
	f32		ny;
	s16		rot_x;
	s16		rot_y;
} OCAnimState;

void	initOCAnim( gmrModelData* md , BOOL );
void	initOCAnimState( void );
void	updateOCAnime( void );
void	updateOCAnimeMtx( void );
void	drawOCAnime( void );
void	clearOCAnime( void );

void	concentOCAnime( void );
BOOL	isConcentOCSmallCube( void );


#ifdef __cplusplus
}
#endif

#endif // MENUOPTIONANIM_H
