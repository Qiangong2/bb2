/*-----------------------------------------------------------------------------
  Tune用のヘッダファイル
  -----------------------------------------------------------------------------*/
#ifndef SMALLCUBETUNE_H
#define SMALLCUBETUNE_H

#ifdef __cplusplus
extern "C" {
#endif

#define		Y_SORT_OFFSET	(10.f)
#define		Y_SORT_MERGIN	(30.f)

typedef
struct sctSortInfo
{
	f32		y;
} sctSortInfo;


#ifdef __cplusplus
}
#endif

#endif	// SMALLCUBETUNE_H
