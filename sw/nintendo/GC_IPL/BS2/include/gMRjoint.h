/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: J3DBinary 1.5 Renderer
  
  JOINT処理
  -----------------------------------------------------------------------------*/
#ifndef GMRJOINT_H
#define GMRJOINT_H

#include "gMRStruct.h"

/*-----------------------------------------------------------------------------
  構造体宣言
  -----------------------------------------------------------------------------*/
typedef
struct gmrJoint
{
	u16		kind;	// ジョイントの種類
	u16		dummy;
	
	J3DTransformInfo	transformInfo;
} gmrJoint;


/*-----------------------------------------------------------------------------
  外部参照可能なプロトタイプ
  -----------------------------------------------------------------------------*/
void parseJoint( u16 joint );

void buildJoint( J3DJointBlock* jbPtr , gmrJoint* jArray );

#endif
