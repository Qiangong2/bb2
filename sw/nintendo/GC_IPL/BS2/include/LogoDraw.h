/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: ロゴを表示する部分のスプラッシュ
  
  GAMECUBEのロゴを表示するのに特化した管理
  描画関連
  -----------------------------------------------------------------------------*/
#ifndef LGDRAW_H
#define LGDRAW_H

void lgDraw( u8 alpha ); // Mtx v , Mtx camera );
void lgDrawInit( void );
gmrModel* lgGetSmallCubeModel( void );

#define cube_size (lgGetCubeState()->lg_cube_size)

#endif
