/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: J3DBinary 1.5 Renderer
  
  プロトタイプ
  -----------------------------------------------------------------------------*/
#ifndef GMODELRENDER_H
#define GMODELRENDER_H

#include "gMRStruct.h"

#include "gmrNode.h"

/*-----------------------------------------------------------------------------
  構造体
  -----------------------------------------------------------------------------*/
typedef
struct gmrModelData
{
	void*	modeldata;
	void*	animedata;
	
	gmrNode*	 root;		// ルートノード（ノードの配列）
	gmrJoint*	 joint;		// joint配列
	gmrMaterial* material;	// material配列
	gmrShape*	 shape;		// shape配列
	
	J3DModelInfoBlock*	ModelInfoP;
	J3DVertexBlock*		VertexP;
	J3DEnvelopBlock*	EnvelopP;
	J3DDrawBlock*		DrawP;
	J3DJointBlock*		JointP;
	J3DMaterialBlock*	MaterialP;
	J3DShapeBlock*		ShapeP;
	J3DTextureBlock*	TextureP;
} gmrModelData;

typedef
struct gmrAnime
{
	u8		attribute;		// 属性（？なんじゃ？）
	u8		shift;			// シフトデータ（Rotate情報）
	s16		frameMax;		// 最大フレーム数

	J3DAnmTransformKeyTable*	anmTable;	// アニメーションフレーム情報格納場所
	f32*						scale;		// scaleデータへのポインタ
	s16*						rotate;		// rotateデータへのポインタ
	f32*						translate;	// translateデータへのポインタ

	J3DAnmTransformKeyData*		animedata;
} gmrAnime;

typedef
struct gmrColorAnime
{
	u16		playmode;
	s16		max_frame;

	s16*	poly_anime_index;
	gMRPolyAnimeKeyTable*	table;
	f32*	r;
	f32*	g;
	f32*	b;
	f32*	a;

	gMRPolyAnimeBlock*		animedata;
} gmrColorAnime;

typedef
struct gmrModel
{
	Mtx*		animMtx;	// アニメーション用のMtxArrayへのポインタ(JOINT計算用)
//	Mtx*		drawMtx;	// 描画するモデル用のMtxArrayへのポインタ
//	Vec			baseScale;	// モデルの基本となるスケール
	Mtx			baseTRMtx;	// モデルの基本となるTRMtx
//	Mtx			viewMtx;	// viewMtx
	Mtx			currentMtx;
//	Vec			currentS;
	GXColor*	mat_color;	// マテリアルアニメ用のポインタ
	
	gmrModelData*	gmrmd;	// このモデル用のデータ
	gmrAnime*		anime;	// このモデルに関連付けられたJointアニメーション
	gmrColorAnime*  c_anime;// このモデルに関連付けられたColorアニメーション
	
	s16			modelAlpha;
	s16			enable_viewcalc;
} gmrModel;

/*-----------------------------------------------------------------------------
  プロトタイプ
  -----------------------------------------------------------------------------*/
void buildModelData( gmrModelData* data );
void buildModel( gmrModel* model , int buildModelData);
void buildAnime( gmrAnime* data , int buildFlag );
void copyModel( gmrModel* dst ,gmrModel* src );

void setAnime( gmrModel* model , gmrAnime* anime );
void setBaseMtx( gmrModel* d, MtxPtr base , Vec scale );
void setViewMtx( gmrModel* d, MtxPtr base );
void updateModel( gmrModel* data );
void updateAnime( gmrModel* data , f32 frame );
void drawModel( gmrModel* data );
void drawModelNode( gmrModel* d , gmrNode* node );
void drawModelNodeSmallCube( gmrModel* d , gmrNode* node );
void setMaterialModel( gmrModel* data );

void buildColorAnime( gmrModel* model, gmrColorAnime* data, int flag );
void drawInit( void );
//void setColorAnime( gmrModel* model, gmrColorAnime* c_anime );


MtxPtr gmrGetCurrentMtx( void );

#endif // JMODELRENDER_H
