#define MSG_S000    0 // [txt0] Systemsettingswerelost.Calendarandothersettingshavebeenreset.Choosesystemsettingsnow?
#define MSG_S001    1 // [txt0] Yes
#define MSG_S002    2 // [txt0] No
#define MSG_S100    3 // [txt1] Youmustfirstsetthecalendarandotheroptions.Pressanybutton.
#define MSG_S200    4 // [txt0] Anerrorhasoccurred.TurnthepowerOFFandchecktheNINTENDOGAMECUBEInstructionBookletforfurtherinstructions.
#define MSG_S201    5 // [txt0] Thedisccouldnotberead.PleasereadtheNINTENDOGAMECUBEInstructionBookletformoreinformation.
#define MSG_2D_DEMO_2_PAL_ENGLISH_NUM    6
