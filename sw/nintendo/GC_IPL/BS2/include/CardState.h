/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: メニューモード時の各面のパラメータ
  -----------------------------------------------------------------------------*/
#ifndef CARDSTATE_H
#define CARDSTATE_H

#ifdef __cplusplus
extern "C" {
#endif

/*-----------------------------------------------------------------------------
  enums
  -----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
  CardState構造体
  -----------------------------------------------------------------------------*/
typedef
struct CardState
{
	s32 cube_r;
	s32 cube_g;
	s32	cube_b;
	s32	cube_a;
	f32	scale;
	s16 cmdwnd_effect_speed;
	s16	fmtwnd_mergin;

	s16	icon_sel_file_r[2];
	s16	icon_sel_file_g[2];
	s16	icon_sel_file_b[2];

	s16	icon_nosel_file_r[2];
	s16	icon_nosel_file_g[2];
	s16	icon_nosel_file_b[2];

	s16	icon_sel_nofile_r[2];
	s16	icon_sel_nofile_g[2];
	s16	icon_sel_nofile_b[2];

	s16	icon_nosel_nofile_r[2];
	s16	icon_nosel_nofile_g[2];
	s16	icon_nosel_nofile_b[2];

	f32	icon_max_scale;
	f32	icon_min_scale;

	s16	moji_sel_r;
	s16	moji_sel_g;
	s16	moji_sel_b;
	s16	moji_back_r;
	s16	moji_back_g;
	s16	moji_back_b;
	s16	moji_half;

	s16 concent_dec_pos;
	s16 concent_dec_alpha;

	f32 swing_x;
	f32 swing_y;

	u16	enable_slot_effect;
	s16	slot_blink_base;

	f32	select_light_dist;
} CardState;

#define IPL_CARD_EFFECT_DEACTIVE_FADEOUT	(0x0001)
#define IPL_CARD_EFFECT_ACTIVE_BLINK		(0x0002)

/*-----------------------------------------------------------------------------
  外部参照関数のプロトタイプ
  -----------------------------------------------------------------------------*/
void	initCard( BOOL );
void	initCardState( CardState* p );
CardState*	getCardState( void );

#ifdef __cplusplus
}
#endif

#endif // CARDSTATE_H
