#define MSG_G000    0 // [mesg] START/PAUSEボタンで　ゲームスタート！
#define MSG_G100    1 // [txt1] エラーが発生しました。本体のパワーボタンを押して電源をOFFにし取扱説明書の指示に従ってください。
#define MSG_G101    2 // [txt1] ディスクを読めませんでした。くわしくは、本体の取扱説明書をお読みください。
#define MSG_G102    3 // [txt1] ゲームキューブディスクをセットしてください。
#define MSG_G103    4 // [txt1] ディスクを読み込んでいます。
#define MSG_G201    5 // PRESSSTART
#define MSG_G202    6 // NODISC
#define MSG_G203    7 // ?
#define MSG_G901    8 // [titl] ポケモンスタジアム金銀
#define MSG_G902    9 // [makr] 2000　NINTENDO
#define MSG_G903    10 // [info] 新しいポケモンスタジアムは、総勢９９９匹すべてのポケモンが登場！ゲームボーイをつなげてバトルだ！
#define MSG_2D_GAMEPLAY_2_JAPANESE_NUM    11
