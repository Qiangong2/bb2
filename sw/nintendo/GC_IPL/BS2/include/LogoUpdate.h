/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: ロゴを表示する部分のスプラッシュ
  
  GAMECUBEのロゴを表示するのに特化した管理
  -----------------------------------------------------------------------------*/
#ifndef LOGOUPDATE_H
#define LOGOUPDATE_H

#ifdef __cplusplus
extern "C" {
#endif

/*-----------------------------------------------------------------------------
  
  -----------------------------------------------------------------------------*/
enum ECubeState_dir
{
	CS_DIR_PLUS_X,
	CS_DIR_PLUS_Z,
	CS_DIR_MINUS_X,
	CS_DIR_MINUS_Z,
	CS_EDGE_TO_0,
	CS_EDGE_TO_1,
	CS_EDGE_TO_2,
	CS_ENLARGE,

	CS_DIR_LAST
};

enum ELogoState_Run
{
	LS_RUN_DROP,
	LS_RUN_ROTATE,
	LS_RUN_UP,
	LS_RUN_DOWN,
	LS_RUN_DONE,
	LS_RUN_LAST
};
/*-----------------------------------------------------------------------------
  1グリッドを構成するのに必要な情報
  -----------------------------------------------------------------------------*/
typedef
struct lgGridState
{
    u8	life;	// グリッド上の四角の生存期間
    u8	color;	// グリッド上の四角の色のインデックス
    u8	state;	// fade in中 / fade out中のフラグ
    u8	dummy;
	u32	real_color; // 実際に描画する色
} lgGridState;

/*-----------------------------------------------------------------------------
  キューブの転がっている状態
  -----------------------------------------------------------------------------*/
typedef struct LogoState
{
	s32	PosX;		// グリッド上のX座標。
	s32	PosZ;		// グリッド上のZ座標。

	s8	dir;		// グリッド上の向いている方向。
	s8	face;		// キューブの存在する面。
	s8	updateDir;	// 方向が更新されたかどうか。
	u8	cubeAlpha;	// キューブのα

	u32 cubeColor;	// キューブの色

	u16	stickChangeColor;	// キューブの色をスティックで変更できる機能。

	s16	nowFrame;	// 現在のフレーム数。
	s16	animFrame;	// 次のグリッドに移動するまでのフレーム数。
	s16	animEdgeFrame;	// エッジでの次のグリッドに移動するまでのフレーム数。

	s16 stopFrame;	// 転がり終わってから、次のフレームに移動するまでの止まっているフレーム数。
	s16 endFrame;	// 転がり終わったフレーム。
	
	u8	aliveTime;	// 軌跡の残っている期間。
	u8	nowSequence;// 現在のシーケンス番号。
	u8	gridMergin; // gridのmergin
	u8	demoDone;	// デモの終了フラグ

	u8	drop_dont_drop;		// そもそも落ちてこない。
	u8	drop_now_frame;		// 落ちてくるキューブを管理するための現在のフレーム。
	u8	drop_drop_frame;	// 落ちてくるキューブの落ちるのにかかるフレーム数。
	u8	drop_stop_frame;	// 転がり始めるまでのフレーム数。
	
	f32	drop_v;				// 落ちてくるキューブの速度。
	f32	drop_a;				// 落ちてくるキューブの加速度。
	f32	drop_y;				// 落ちてくるキューブの高さ。
	f32	drop_now_y;			// 落ちてくるキューブの高さ。

	u8	jump_now_frame;		// ジャンプ中の現在のフレーム。
	u8	jump_jump_frame;	// ジャンプするフレーム数。
	u8	jump_stop_frame;	// ジャンプした後止まるフレーム数。
	u8	jump_rot_num;		// 回転数（90度単位）
	f32	jump_max_height;	// 高さ。

	u8	down_now_frame;		// 落ちてくるキューブを管理するための現在のフレーム。
	u8	down_down_frame;	// 落ちてくるキューブの落ちるのにかかるフレーム数。
	u8	down_stop_frame;	// 転がり始めるまでのフレーム数。

	f32	down_v;				// 落ちてくるキューブの速度。
	f32	down_a;				// 落ちてくるキューブの加速度。
	f32	down_y;				// 落ちてくるキューブの高さ。
	f32	down_now_y;			// 落ちてくるキューブの高さ。
	f32	down_start_v;		// 落ちてくるキューブの初速度。

	u8	logo_mark_frame;	// ロゴマークの現れるまでのフレーム数。
	u8	logo_mark_now_frame;// ロゴマークの現在のフレーム数。

	u8	ninlogo_frame;		// 文字ロゴの現在フレーム。
	u8	ninlogo_max_frame;	// 文字ロゴの現れるまでのフレーム。
	u8	ninlogo_wait_frame;	// 文字ロゴの現れるまでのフレーム。

	u8	lg_cube_size;
	u8	grid_r;
	u8	grid_g;
	u8	grid_b;

	s16	sound_frame;
	s16	sound_start_frame1;
	s16	sound_start_frame2;
	s16	sound_start_frame3;

} LogoState;

/*-----------------------------------------------------------------------------
  転がるシーケンス情報
  -----------------------------------------------------------------------------*/
typedef
struct lgSequence
{
	s8 dir;
	u8 color;
	u8 life;
	u8 dummy;
} lgSequence;

/*-----------------------------------------------------------------------------
  定数・型宣言
  -----------------------------------------------------------------------------*/
#define LG_GRID_MAX_X	32
#define LG_GRID_MAX_Z	32
#define LG_GRID_CENTER_X	LG_GRID_MAX_X/2
#define LG_GRID_CENTER_Z	LG_GRID_MAX_Z/2

typedef lgGridState lgGridArray[3][LG_GRID_MAX_X][LG_GRID_MAX_Z];
typedef lgGridState (*lgGridArrayPtr)[LG_GRID_MAX_X][LG_GRID_MAX_Z];

/*-----------------------------------------------------------------------------
  外部参照可能な関数のプロトタイプ
  -----------------------------------------------------------------------------*/
lgGridArrayPtr  lgGetGridState( void );
LogoState*		lgGetCubeState( void );
void 	LogoInit( BOOL first );
u32 	lgUpdateCubePos( BOOL );
void 	lgUpdateCubeMtx( void );

void	lgInitCubeState( LogoState* cs );
void	lgInitPosState( LogoState* cs );
MtxPtr	lgGetSmallCubeMtx( void );
BOOL	gEnableInteractive( void );

//void	LogoUpdateToDoneState( void );

#ifdef __cplusplus
}
#endif

#endif // LOGOUPDATE_H
