#define MSG_O000    0 // [mesg] ChooseNINTENDOGAMECUBEsystemsettings.
#define MSG_O001    1 // [txt1] Sound
#define MSG_O002    2 // Stereo
#define MSG_O003    3 // Mono
#define MSG_O004    4 // [txt2] ScreenPosition
#define MSG_O005    5 // [txt3] Language
#define MSG_O100    6 // English
#define MSG_O101    7 // French
#define MSG_O102    8 // German
#define MSG_O103    9 // Italian
#define MSG_O104    10 // Dutch
#define MSG_O105    11 // Spanish
#define MSG_2D_OPTION_2_E_ENGLISH_NUM    12
