/*---------------------------------------------------------------------------*
  Project:  Dolphin Test IPLROM Menu
  File:     main.h

  Copyright 2000 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

 *---------------------------------------------------------------------------*/

#ifndef __MAIN_H__
#define __MAIN_H__

#ifdef __cplusplus
extern "C" {
#endif
	
enum MenuItem {
    LOGO_DISPLAY,
    TOP_MENU, DISK_MENU, CARD_MENU, CLOCK_MENU, SCREEN_MENU,
    SOUND_MENU, WPAD_MENU, MENU_INIT, LOADING, DISK_ERROR
};

#define BS2COUNTRY_JAPAN                2
#define BS2COUNTRY_US                   0
/*---------------------------------------------------------------------------*/
#ifdef __cplusplus
}
#endif

#endif  /* __MAIN_H__ */
