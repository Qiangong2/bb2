/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: NTSC/MPAL/PAL Rendermode ��`
  -----------------------------------------------------------------------------*/
#ifndef GRENDERMODE_H
#define GRENDERMODE_H

#ifdef __cplusplus
extern "C" {
#endif

	GXRenderModeObj GXNtsc592x448IntAa = 
	{
		VI_TVMODE_NTSC_INT,     // viDisplayMode
		592,             // fbWidth
		226,             // efbHeight
		448,             // xfbHeight
		(VI_MAX_WIDTH_NTSC - 640)/2,        // viXOrigin
		(VI_MAX_HEIGHT_NTSC - 448)/2,       // viYOrigin
		640,             // viWidth
		448,             // viHeight
		VI_XFBMODE_DF,   // xFBmode
		GX_FALSE,        // field_rendering
		GX_TRUE,         // aa
		
		// sample points arranged in increasing Y order
		3,  2,  9,  6,  3, 10,  // pix 0, 3 sample points, 1/12 units, 4 bits each
		3,  2,  9,  6,  3, 10,  // pix 1
		9,  2,  3,  6,  9, 10,  // pix 2
		9,  2,  3,  6,  9, 10,  // pix 3
		
		// vertical filter[7], 1/64 units, 6 bits each
		8,         // line n-1
		8,         // line n-1
		10,         // line n
		12,         // line n
		10,         // line n
		8,         // line n+1
		8          // line n+1
	};

#define SCREEN_HT   520
	GXRenderModeObj GXPal592x448IntAa = 
	{
		VI_TVMODE_PAL_INT,     // viDisplayMode
		592,             // fbWidth
		SCREEN_HT / 2 + 2,             // efbHeight
		SCREEN_HT,             // xfbHeight
		(VI_MAX_WIDTH_PAL - 640)/2,        // viXOrigin
		(VI_MAX_HEIGHT_PAL - SCREEN_HT)/2,       // viYOrigin
		640,             // viWidth
		SCREEN_HT,             // viHeight
		VI_XFBMODE_DF,   // xFBmode
		GX_FALSE,        // field_rendering
		GX_TRUE,         // aa
		
		// sample points arranged in increasing Y order
		3,  2,  9,  6,  3, 10,  // pix 0, 3 sample points, 1/12 units, 4 bits each
		3,  2,  9,  6,  3, 10,  // pix 1
		9,  2,  3,  6,  9, 10,  // pix 2
		9,  2,  3,  6,  9, 10,  // pix 3
		
		// vertical filter[7], 1/64 units, 6 bits each
		8,         // line n-1
		8,         // line n-1
		10,         // line n
		12,         // line n
		10,         // line n
		8,         // line n+1
		8          // line n+1
	};

	GXRenderModeObj GXDebugPal592x448IntAa = 
	{
		VI_TVMODE_DEBUG_PAL_INT,     // viDisplayMode
		592,             // fbWidth
		SCREEN_HT / 2 + 2,             // efbHeight
		SCREEN_HT,             // xfbHeight
		(VI_MAX_WIDTH_PAL - 640)/2,        // viXOrigin
		(VI_MAX_HEIGHT_PAL - SCREEN_HT)/2,       // viYOrigin
		640,             // viWidth
		SCREEN_HT,             // viHeight
		VI_XFBMODE_DF,   // xFBmode
		GX_FALSE,        // field_rendering
		GX_TRUE,         // aa
		
		// sample points arranged in increasing Y order
		3,  2,  9,  6,  3, 10,  // pix 0, 3 sample points, 1/12 units, 4 bits each
		3,  2,  9,  6,  3, 10,  // pix 1
		9,  2,  3,  6,  9, 10,  // pix 2
		9,  2,  3,  6,  9, 10,  // pix 3
		
		// vertical filter[7], 1/64 units, 6 bits each
		8,         // line n-1
		8,         // line n-1
		10,         // line n
		12,         // line n
		10,         // line n
		8,         // line n+1
		8          // line n+1
	};

	GXRenderModeObj GXMpal592x448IntAa = 
	{
		VI_TVMODE_MPAL_INT,     // viDisplayMode
		592,             // fbWidth
		226,             // efbHeight
		448,             // xfbHeight
		(VI_MAX_WIDTH_NTSC - 640)/2,        // viXOrigin
		(VI_MAX_HEIGHT_NTSC - 448)/2,       // viYOrigin
		640,             // viWidth
		448,             // viHeight
		VI_XFBMODE_DF,   // xFBmode
		GX_FALSE,        // field_rendering
		GX_TRUE,         // aa
		
		// sample points arranged in increasing Y order
		3,  2,  9,  6,  3, 10,  // pix 0, 3 sample points, 1/12 units, 4 bits each
		3,  2,  9,  6,  3, 10,  // pix 1
		9,  2,  3,  6,  9, 10,  // pix 2
		9,  2,  3,  6,  9, 10,  // pix 3
		
		// vertical filter[7], 1/64 units, 6 bits each
		8,         // line n-1
		8,         // line n-1
		10,         // line n
		12,         // line n
		10,         // line n
		8,         // line n+1
		8          // line n+1
	};



#ifdef __cplusplus
}
#endif

#endif GRENDERMODE_H
