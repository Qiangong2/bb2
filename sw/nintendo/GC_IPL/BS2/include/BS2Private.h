/*---------------------------------------------------------------------------*
  Project:  BS2
  File:     BS2Private.h

  Copyright 1998-2003 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: BS2Private.h,v $
  Revision 1.1.1.1  2004/06/01 21:17:32  paulm
  GC IPLROM source from Nintendo

    
    16    3/19/03 17:42 Shiki
    Declared __OSFPRInit().

    15    2/24/03 17:02 Shiki
    Declared BS2FPRInit().

    14    11/15/01 12:32 Shiki
    From DolphinIPL_09_Oct_2001.zip.
  Revision 1.6  2001/03/08 13:49:20  sasakit
  030901��


    13    3/07/01 6:49p Hashida
    Changed so that when wrong disk (country code check error) happens,
    wait for cover close and retry.

    12    3/02/01 6:07p Hashida
    Added retry

    11    3/02/01 12:02a Hashida
    Added restarting feature.

    10    2/23/01 4:21p Hashida
    Changed so that cover close check can be done sooner.

    9     2/21/01 3:41a Hashida
    Removed checking cover open after reset because it doesn't work.

    8     2/21/01 2:59a Hashida
    Changed to check cover status before issuing dvdreadid.
    Changed not to use OSGetTime but to use __OSGetSystemTime to check
    reset time.

    7     2/08/01 4:56p Hashida
    Added BS2GetBytesLeft, BS2SetBannerBuffer and BS2IsBannerAvailable.

    6     9/21/00 3:13p Hashida
    Added fatal error.

    5     4/17/00 1:31p Hashida
    Added audio buffer configuration.

    4     4/10/00 7:23p Hashida
    changed for the new apploader format.

    3     4/06/00 5:25p Shiki
    Clean up.

    2     3/10/00 1:24p Eugene

    1     12/15/99 5:00p Shiki
    Initial check-in.
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#ifndef __BS2PRIVATE_H__
#define __BS2PRIVATE_H__

#ifdef __cplusplus
extern "C" {
#endif

// Set this flag to non-zero to force memory to 0xFF's
#define DEBUG_BSS       0

// Define this flag to disable splash screen and all VI related things.
#define BRINGUP

typedef enum
{
    BS2_START,
    BS2_WAIT_DVD,
    BS2_WAKE_DVD,
    BS2_LOAD_DISKID,
    BS2_WAIT_DISKID,
    BS2_CONFIRMED_COVER_CLOSED,
    BS2_CONFIG_AUDIO_BUFFER,
    BS2_WAIT_AUDIO_BUFFER,
    BS2_LOAD_APPLOADER_HEADER,
    BS2_WAIT_APPLOADER_HEADER,
    BS2_LOAD_APPLOADER,
    BS2_WAIT_APPLOADER,
    BS2_DRIVE_APPLOADER,
    BS2_WAIT_APPLOADER_LOAD,
    BS2_LOAD_BANNER,
    BS2_WAIT_BANNER,
    BS2_RUN_APP,

    BS2_COVER_CLOSED,
    BS2_NO_DISK,
    BS2_COVER_OPEN,
    BS2_STOP_MOTOR,
    BS2_WAIT_STOP_MOTOR,
    BS2_WRONG_DISK,
    BS2_FATAL_ERROR,

    BS2_RETRY,

    BS2_CANCELING,

    BS2_MAX_STATE
} BS2State;

extern void     BS2Init(void);
extern BS2State BS2Tick(void);
extern void     BS2StartGame(void);
extern void     BS2Report(char* msg, ...);
extern s32      BS2GetBytesLeft(void);
extern void     BS2SetBannerBuffer(void* addr, u32 length);
extern BOOL     BS2IsBannerAvailable(void);
extern void     BS2RestartStateMachine(void);

// os[D].a
extern void     __OSFPRInit(void);

#ifdef __cplusplus
}
#endif

#endif  // __BS2PRIVATE_H__

