/*---------------------------------------------------------------------------*
  File:    smallCubeFaderData.h
  Copyright 2000 Nintendo.  All rights reserved.

  小さいキューブでの、電光掲示板的表示のデータを扱う。


 *---------------------------------------------------------------------------*/
#ifndef __SMALLCUBEFADERDATA_H__
#define __SMALLCUBEFADERDATA_H__

#include <dolphin/types.h>


// ファイルから読みこむ、ある集合をつくるキューブ数と
// インデクスのデータ。
typedef struct smallCubeFaderIndex
{
	u16 cubenum; // キューブ数
	u16 offset; // データ（u16の配列） へのオフセット
}smallCubeFaderIndex;


void initSmallCubeFaderData( void* data );

/****************************************
 * insert here
 ****************************************/
u16* scfGetCLOCKIndex(void);
u16  scfGetCLOCKNum(void);
u16* scfGetCOLONIndex(void);
u16  scfGetCOLONNum(void);
u16* scfGetCOLON1Index(void);
u16  scfGetCOLON1Num(void);
u16* scfGetCOLON2Index(void);
u16  scfGetCOLON2Num(void);
u16* scfGetCOLON3Index(void);
u16  scfGetCOLON3Num(void);
u16* scfGetCOLON4Index(void);
u16  scfGetCOLON4Num(void);
u16* scfGetDATEIndex(void);
u16  scfGetDATENum(void);
u16* scfGetDAYIndex(void);
u16  scfGetDAYNum(void);
u16* scfGetDAY1STIndex(void);
u16  scfGetDAY1STNum(void);
u16* scfGetDAY2NDIndex(void);
u16  scfGetDAY2NDNum(void);
u16* scfGetHOURIndex(void);
u16  scfGetHOURNum(void);
u16* scfGetHOUR1STIndex(void);
u16  scfGetHOUR1STNum(void);
u16* scfGetHOUR2NDIndex(void);
u16  scfGetHOUR2NDNum(void);
u16* scfGetMINUTEIndex(void);
u16  scfGetMINUTENum(void);
u16* scfGetMINUTE1STIndex(void);
u16  scfGetMINUTE1STNum(void);
u16* scfGetMINUTE2NDIndex(void);
u16  scfGetMINUTE2NDNum(void);
u16* scfGetMONTHIndex(void);
u16  scfGetMONTHNum(void);
u16* scfGetMONTH1STIndex(void);
u16  scfGetMONTH1STNum(void);
u16* scfGetMONTH2NDIndex(void);
u16  scfGetMONTH2NDNum(void);
u16* scfGetSECONDIndex(void);
u16  scfGetSECONDNum(void);
u16* scfGetSECOND1STIndex(void);
u16  scfGetSECOND1STNum(void);
u16* scfGetSECOND2NDIndex(void);
u16  scfGetSECOND2NDNum(void);
u16* scfGetYEARIndex(void);
u16  scfGetYEARNum(void);
u16* scfGetYEAR1STIndex(void);
u16  scfGetYEAR1STNum(void);
u16* scfGetYEAR2NDIndex(void);
u16  scfGetYEAR2NDNum(void);
u16* scfGetYEAR3RDIndex(void);
u16  scfGetYEAR3RDNum(void);
u16* scfGetYEAR4THIndex(void);
u16  scfGetYEAR4THNum(void);
u16* scfGetNODISCIndex(void);
u16  scfGetNODISCNum(void);
u16* scfGetPRESSSTARTIndex(void);
u16  scfGetPRESSSTARTNum(void);
u16* scfGetQUESIndex(void);
u16  scfGetQUESNum(void);
u16* scfGetNUMBER0Index(void);
u16  scfGetNUMBER0Num(void);
u16* scfGetNUMBER1Index(void);
u16  scfGetNUMBER1Num(void);
u16* scfGetNUMBER2Index(void);
u16  scfGetNUMBER2Num(void);
u16* scfGetNUMBER3Index(void);
u16  scfGetNUMBER3Num(void);
u16* scfGetNUMBER4Index(void);
u16  scfGetNUMBER4Num(void);
u16* scfGetNUMBER5Index(void);
u16  scfGetNUMBER5Num(void);
u16* scfGetNUMBER6Index(void);
u16  scfGetNUMBER6Num(void);
u16* scfGetNUMBER7Index(void);
u16  scfGetNUMBER7Num(void);
u16* scfGetNUMBER8Index(void);
u16  scfGetNUMBER8Num(void);
u16* scfGetNUMBER9Index(void);
u16  scfGetNUMBER9Num(void);
u16* scfGetARROWIndex(void);
u16  scfGetARROWNum(void);
u16* scfGetARROWLEFTIndex(void);
u16  scfGetARROWLEFTNum(void);
u16* scfGetARROWRIGHTIndex(void);
u16  scfGetARROWRIGHTNum(void);
u16* scfGetLANGUAGEIndex(void);
u16  scfGetLANGUAGENum(void);
u16* scfGetMONOIndex(void);
u16  scfGetMONONum(void);
u16* scfGetOFFSETIndex(void);
u16  scfGetOFFSETNum(void);
u16* scfGetOFFSET1STIndex(void);
u16  scfGetOFFSET1STNum(void);
u16* scfGetOFFSET2NDIndex(void);
u16  scfGetOFFSET2NDNum(void);
u16* scfGetSMALL_ARROW01Index(void);
u16  scfGetSMALL_ARROW01Num(void);
u16* scfGetSMALL_ARROW02Index(void);
u16  scfGetSMALL_ARROW02Num(void);
u16* scfGetSMALL_ARROW03Index(void);
u16  scfGetSMALL_ARROW03Num(void);
u16* scfGetSMALL_ARROW04Index(void);
u16  scfGetSMALL_ARROW04Num(void);
u16* scfGetSMALL_ARROW05Index(void);
u16  scfGetSMALL_ARROW05Num(void);
u16* scfGetSMALL_ARROW06Index(void);
u16  scfGetSMALL_ARROW06Num(void);
u16* scfGetSOUNDIndex(void);
u16  scfGetSOUNDNum(void);
u16* scfGetSTEREOIndex(void);
u16  scfGetSTEREONum(void);
/****************************************
 * insert here end
 ****************************************/


// 数字
u16* scfGetDIGITIndex(s16 num);

// 多言語用
u16* scfGetMONOIndex_pal( s32 lang );
u16  scfGetMONONum_pal( s32 lang );
u16* scfGetSTEREOIndex_pal( s32 lang );
u16  scfGetSTEREONum_pal( s32 lang );


#endif // __SMALLCUBEFADERDATA_H__
