/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: J3DBinary 1.5 Renderer
  
  Baseサービス
  -----------------------------------------------------------------------------*/
#ifndef GBASE_H
#define GBASE_H

#ifdef __cplusplus
extern "C" {
#endif

#define IPL_ADDHEAP_MEM_LOW		0x80700000
#define IPL_ADDHEAP_MEM_HIGH	0x81100000  // ATI diagを動かすため。BOOT_APPLOADER_BASE

#ifdef IPL
#define DIReport( a )					( (void) 0 )
#define DIReport1( a, b )				( (void) 0 )
#define DIReport2( a, b, c )			( (void) 0 )
#define DIReport3( a, b, c, d )			( (void) 0 )
#define DIReport4( a, b, c, d, e )		( (void) 0 )
#define DIReport5( a, b, c, d, e, f )	( (void) 0 )
#else
#define DIReport( a )						OSReport( (a) )
#define DIReport1( a, b )					OSReport( (a), (b) )
#define DIReport2( a, b, c )				OSReport( (a), (b), (c) )
#define DIReport3( a, b, c, d )				OSReport( (a), (b), (c), (d) )
#define DIReport4( a, b, c, d, e )			OSReport( (a), (b), (c), (d), (e) )
#define DIReport5( a, b, c, d, e, f )		OSReport( (a), (b), (c), (d), (e), (f) )
#endif

void*	gAlloc( u32   size);
void	gFree ( void* ptr );
void	gInitHeap( void );
void	gDumpHeap( void );

#ifdef __cplusplus
}
#endif

#endif // GBASE_H
