/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: オプションメニュー時の各面のパラメータ
  -----------------------------------------------------------------------------*/
#ifndef OPTIONUPDATE_H
#define OPTIONUPDATE_H

#ifdef __cplusplus
extern "C" {
#endif

enum {
	IPL_OPTION_SELECTING,
	IPL_OPTION_SOUND,
	IPL_OPTION_OFFSET,
	IPL_OPTION_LANGUAGE,
	IPL_OPTION_LAST
};

void	initOptionMenu( BOOL );
void	initOptionMenu_Lang( void );
s32		updateOptionMenu( void );
void	drawOptionMenu( u8 , u8 , u8 );
s32		getOptionRunstate( void );
s16		getOptionLanguagePosX_lang( s16 );
s16		getOptionLanguagePosX( void );
s16     getLanguageCurstate(void);

#ifdef __cplusplus
}
#endif

#endif //OPTIONUPDATE_H
