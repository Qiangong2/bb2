/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: J3DBinary 1.5 Renderer
  
  Shape処理
  -----------------------------------------------------------------------------*/
#ifndef GMRSHAPE_H
#define GMRSHAPE_H

#include "gMRStruct.h"

#ifdef __cplusplus
extern "C" {
#endif

/*-----------------------------------------------------------------------------
  構造体宣言
  -----------------------------------------------------------------------------*/
typedef
struct gmrShapeDraw
{
	u32		size;
	u8*		displayList;
} gmrShapeDraw;

typedef
struct gmrShapeMtx
{
	u16		baseId;		// Shapeの属するDrawMtxID
	u16		useMtxNum;	// 使用しているMtxの数
	u16*	useMtxIdP;	// index->DrawMtxIDへの変換テーブルへのポインタ
} gmrShapeMtx;

typedef
struct gmrShape
{
	u8			mtxType;
	u8			dummy;
	u16			mtxGroupNum;

	GXVtxDescList*	vtxDescList;

	gmrShapeMtx*	shapeMtx;
	gmrShapeDraw*	shapeDraw;
} gmrShape;

/*-----------------------------------------------------------------------------
  外部参照可能なプロトタイプ
  -----------------------------------------------------------------------------*/
//void drawShape( gmrShape* shape );

void buildShape( J3DShapeBlock* sbPtr , gmrShape* sArray );

#ifdef __cplusplus
}
#endif

#endif // GMRSHAPE_H
