#define MSG_S000    0 // [txt0] 本体設定が消えています。日付・時刻などをリセットしました。セットし直しますか？
#define MSG_S001    1 // [txt0] はい
#define MSG_S002    2 // [txt0] いいえ
#define MSG_S100    3 // [txt1] 最初にカレンダーとオプションをセットする必要があります。ボタンを押してください。
#define MSG_S200    4 // [txt0] エラーが発生しました。本体のパワーボタンを押して電源をOFFにし取扱説明書の指示に従ってください。
#define MSG_S201    5 // [txt0] ディスクを読めませんでした。くわしくは、本体の取扱説明書をお読みください。
#define MSG_2D_DEMO_2_JAPANESE_NUM    6
