#define MSG_S000    0 // [txt0] Sehadesconfiguradolaconsola.Elcalendarioyotrasopcioneshanvueltoalosvaloresiniciales.¿Quierescambiarlosahora?
#define MSG_S001    1 // [txt0] Sí
#define MSG_S002    2 // [txt0] No
#define MSG_S100    3 // [txt1] Enprimerlugardebesconfigurarelcalendarioyotrasopciones.Pulsacualquierbotón.
#define MSG_S200    4 // [txt0] Sehaproducidounerror.Apagalaconsola(OFF)yconsultaelmanualdeinstruccionesdeNINTENDOGAMECUBEparaobtenermásinformación.
#define MSG_S201    5 // [txt0] Nosepuedeleereldisco.ConsultaelmanualdeinstruccionesdeNINTENDOGAMECUBEparaobtenermásinformación.
#define MSG_2D_DEMO_2_PAL_SPANISH_NUM    6
