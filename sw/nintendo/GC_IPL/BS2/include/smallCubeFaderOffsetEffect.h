#ifndef __SMALLCUBEFADEROFFSETEFFECT_H__
#define __SMALLCUBEFADEROFFSETEFFECT_H__

#include <dolphin.h>
#include "gMRStruct.h" // for J3DTransformInfo definition

u32 getFaderEffectNum(void);
u8 getFaderEffectAlpha(void);
J3DTransformInfo* getFaderEffectPos(void);
void updateOptionOffsetEffect(void);
void initOptionOffsetEffect(void);

	
#endif // __SMALLCUBEFADEROFFSETEFFECT_H__
