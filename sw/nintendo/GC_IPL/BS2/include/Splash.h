/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: Splashデモ メインルーチン

  シーケンスを含めたSplashデモ用の外部参照用ヘッダ
  -----------------------------------------------------------------------------*/
#ifndef SPLASH_H
#define SPLASH_H

#ifdef __cplusplus
extern "C" {
#endif

void SplashUpdate( void );//Mtx camera );
void SplashDraw( void );
void SplashDrawDone( void );
void SplashInit( BOOL first );

#ifdef __cplusplus
}
#endif

#endif // SPLASH_H