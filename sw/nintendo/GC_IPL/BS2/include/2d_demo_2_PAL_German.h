#define MSG_S000    0 // [txt0] KeineSystemeinstellungenvorhanden!DatumundsämtlicheandereEinstellungenbefindensichinderWerkseinstellung.SollendieSystemeinstellungenjetztgeändertwerden?
#define MSG_S001    1 // [txt0] Ja
#define MSG_S002    2 // [txt0] Nein
#define MSG_S100    3 // [txt1] ZunächstmüssenderKalenderunddieweiterenOptionenfestgelegtwerden.(WeiterdurchKnopfdruck.)
#define MSG_S200    4 // [txt0] EinFehleristaufgetreten.BitteschaltenSiedenNINTENDOGAMECUBEausundlesenSiedieBedienungsanleitung,umweitereInformationenzuerhalten.
#define MSG_S201    5 // [txt0] DieseDisckannnichtgelesenwerden.BittelesenSiedieBedienungsanleitung,umweitereInformationenzuerhalten.
#define MSG_2D_DEMO_2_PAL_GERMAN_NUM    6
