#ifdef __cplusplus
extern "C" {
#endif

/*-----------------------------------------------------------------------------
  defines
  -----------------------------------------------------------------------------*/
#define DI_STICK_THRESHOLD      35
#define DI_STICK_UP             0x1000
#define DI_STICK_DOWN           0x2000
#define DI_STICK_LEFT           0x4000
#define DI_STICK_RIGHT          0x8000
#define DI_SUBSTICK_UP          0x0100
#define DI_SUBSTICK_DOWN        0x0200
#define DI_SUBSTICK_LEFT        0x0400
#define DI_SUBSTICK_RIGHT       0x0800
#define DI_PAD_CHECK_INTERVAL   30

/*-----------------------------------------------------------------------------
  PAD情報構造体
  -----------------------------------------------------------------------------*/
typedef struct DIPadStatus
{
    // contains PADStatus structure
    PADStatus   pst;
    
    // extended field
    u16         buttonDown;			//0
    u16         buttonUp;			//1
    u16         dirs;				//2
    u16         dirsPressed;		//3
    u16         dirsReleased;		//4
    s16         stickDeltaX;		//5
    s16         stickDeltaY;		//6
    s16         substickDeltaX;		//7
    s16         substickDeltaY;		//8

	s16			angle;				//9
//	u16			angle;				//9
	f32			value;				//10
} DIPadStatus;


/*-----------------------------------------------------------------------------
  Prototype
  -----------------------------------------------------------------------------*/
void	DIPadRead( void );
void	DIPadInit( void );
BOOL	gIsPadRepeating( void );
u32		gGetPremiumState( void );

#ifdef __cplusplus
}
#endif
