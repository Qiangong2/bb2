#ifndef __SMALLCUBEFADERNUMBEREFFECT_H__
#define __SMALLCUBEFADERNUMBEREFFECT_H__

#include <dolphin.h>
#include "gMRStruct.h" // for J3DTransformInfo definition

u32 scfGetNumberEffectNum(void);
u8 *scfGetNumberEffectAlpha(void);
J3DTransformInfo* scfGetNumberEffectPos(void);
void scfInvalidateOptionNumberEffect(void);
void scfUpdateOptionNumberEffect( J3DTransformInfo *pos );
void scfInvalidateTimeNumberEffect(void);
void scfUpdateTimeNumberEffect( J3DTransformInfo *pos );


#endif // __SMALLCUBEFADERNUMBEREFFECT_H__
