/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: Font関連ルーチン
  -----------------------------------------------------------------------------*/
#ifndef GFONT_H
#define GFONT_H

#include "gLRStruct.h"
#include "gInit.h"

#ifdef __cplusplus
extern "C" {
#endif

#define GF_MAX_LINES	64

// #define IPL_FONT_TEMP_HEAP_TOP	( IPL_ADDHEAP_MEM_LOW - 0x100000 )
#define IPL_JPN_FONT_SIZE	(0x4d000)
#define IPL_ENG_FONT_SIZE	(0x3000)
#define IPL_FONT_OFFSET		(2096896 - IPL_ENG_FONT_SIZE - IPL_JPN_FONT_SIZE)

enum {
	IPL_FONT_COLOR_TEXTURE,
	IPL_FONT_COLOR_REGISTER,
	IPL_FONT_COLOR_LAST
};
	
typedef
struct FontHeader
{
	u16					fontType;			//  2B: フォントタイプ
	u16					firstChar;			//  2B: キャラクタ番号の最初
	u16					lastChar;			//  2B: キャラクタ番号の最後
	u16					invalChar;			//  2B: 無効文字のフォントコード
	u16					ascent;				//  2B: 文字のベースラインから上の大きさ
	u16					descent;			//  2B: 文字のベースラインから下の大きさ
	u16					width;				//  2B: 文字の最大幅
	u16					leading;			//  2B: 行間隔

	u16					cellWidth;			//  2B: シート内のセル（１文字分）の横幅
	u16					cellHeight;			//  2B: シート内のセル（１文字分）の高さ
	u32					sheetSize;			//  4B: １シートのサイズ（バイト数）
	u16					sheetFormat;		//  2B: シートテクスチャのフォーマット
	u16					sheetRow;			//  2B: １シート内の文字の列数
	u16					sheetLine;			//  2B: １シート内の文字の行数
	u16					sheetWidth;			//  2B: シートの横幅（テクスチャの横幅）
	//32B

	u16					sheetHeight;		//  2B: シートの高さ（テクスチャの高さ）
	u16					widthTable;			//  2B: 文字幅表へのオフセット(固定値)

	u32					sheetImage;			//  4B: シートイメージへのオフセット
	u32					sheetFullSize;		//  4B: 全シートサイズ

	u8					c0;
	u8					c1;
	u8					c2;
	u8					c3;
	// 48B
} FontHeader;

/*-----------------------------------------------------------------------------
  function prototype
  -----------------------------------------------------------------------------*/
void	gfInitFontData( void * buf );
void	setGXforFont( void );
u16		drawFont( int x, int y , int size, int sjisCode );
int		print( int x, int y, int size, char* format, ... );
void	setFontRenderMode( int rm );
void	gfSetFontColor( u32 up, u32 down );
void	gfSetFontGXColor( GXColor* up, GXColor* down );
void	gfSetFontSetting( void );
void	gfSetBackFontColor( u32 up, u32 down );
void	gfSetBackFontGXColor( GXColor* up, GXColor* down );
void	gfPrint 	( gLRTextBoxData* tdp, char* string );
void	gfPrintN	( gLRTextBoxData* tdp, char* string, int max );
void	gfPrintPos	( gLRTextBoxData* tdp, char* string, int x, int y );
void	gfPrintNPos	( gLRTextBoxData* tdp, char* string, int x, int y, int max );
void	gfPrintNOneLine( gLRTextBoxData* tdp, char* string, int string_num_max );
void	gfPrintNTwoLine( gLRTextBoxData* tdp, char* string, int string_num_max );
void	gfBritePrint( gLRTextBoxData* tdp, char* string );
void	gfGetWidthHeight(  gLRTextBoxData* tdp, char* string, u16* max_width, u16* height );
void	gfGetWidthHeightLineFeed(  gLRTextBoxData* tdp, char* string, u16* max_width, u16* height );

void	gfUpdateAlphaAnime( void );

#ifdef __cplusplus
}
#endif

#endif // GFONT_H
