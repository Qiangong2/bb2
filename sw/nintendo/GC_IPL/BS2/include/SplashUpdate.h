/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: Splashデモ メインルーチン

  シーケンスを含めたSplashデモのupdate用ヘッダ
  -----------------------------------------------------------------------------*/
#ifndef SPLASHUPDATE_H
#define SPLASHUPDATE_H

/*-----------------------------------------------------------------------------
  外部参照できる関数のプロトタイプ
  -----------------------------------------------------------------------------*/
u32	updateSplashFirstBoot( void );
u32 updateSplashFadeinLogo( void );
u32	updateSplashLogoToMenu( void );
u32 inSplashUpdate( void );

void inSplashUpdateMtx( void );
MtxPtr inSplashGetMtxArray( void );

#endif // SPLASHUPDATE_H
