/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: J3DBinary 1.5 Renderer
  
  Material処理
  -----------------------------------------------------------------------------*/
#ifndef GMRMATERIAL_H
#define GMRMATERIAL_H

#include "gMRStruct.h"

/*-----------------------------------------------------------------------------
  構造体宣言
  -----------------------------------------------------------------------------*/
#define gmrZMI_setCompareEnable(zmi,value) 	(((zmi).ce_ue_func)&0x3f)|((value)<<6)
#define gmrZMI_setUpdateEnable(zmi,value)  	(((zmi).ce_ue_func)&0xcf)|((value)<<4)
#define gmrZMI_setFunc(zmi,value) 			(((zmi).ce_ue_func)&0xf0)|((value))
#define gmrZMI_getCompareEnable(zmi) 		((((zmi).ce_ue_func)>>6 ) & 3 )
#define gmrZMI_getUpdateEnable(zmi)  		((((zmi).ce_ue_func)>>4 ) & 3 )
#define gmrZMI_getFunc(zmi) 				(((zmi).ce_ue_func) & 0x0f )

typedef
struct gmrZModeInfo
{
	u8	ce_ue_func;	// 上位4bits ( 上位2bits Compare_enable 下位2bits update_enable )
					// 下位4bits func
} gmrZModeInfo;

typedef
struct gmrMaterial
{
	u8			MaterialMode;
	u8			CullMode;
	u8			colorChanNum;
	u8			texGenNum;
	u8			tevStageNum;
	u8			dither;
	u8			zCompLoc;
	gmrZModeInfo		zModeInfo;	// u8
	
	u16					texNo[8];

	GXColor*		 		colorPtr[2];
	J3DColorChanInfo*		colorChanPtr[4];
	GXColorS10*				tevColorPtr[4];
	J3DTexCoordInfo*		texCoordPtr[8];
	J3DTevOrderInfo*		tevOrderPtr[8];	// TeVは最大8ステージまで。
	J3DTevStageInfo*		tevStagePtr[8];
	J3DTexMtxInfo*		 	texMtxPtr[10];
	J3DFogInfo*				fogPtr;
	J3DAlphaCompInfo*		alphaCompPtr;
	J3DBlendInfo*			blendPtr;
	J3DNBTScaleInfo*		nbtScalePtr;
} gmrMaterial;

enum {
	cTexMapNormal,
	cTexMapEnv,
	cTexMapProj
};

enum  {
    gmrDrawMode_Opa			=	0x00000001,
    gmrDrawMode_TexEdge		=	0x00000002,
    gmrDrawMode_Xlu			=	0x00000004
};

/*-----------------------------------------------------------------------------
  外部参照可能なプロトタイプ
  -----------------------------------------------------------------------------*/
void parseMaterial( u16 material );

void buildMaterial( J3DMaterialBlock* mbPtr , gmrMaterial* mArray );
VecPtr getNBTScale(void);

void	loadMaterial( u16 index );
void	loadMaterialSmallCube( u16 i );
u8* getTexMtxInfo( void );

void loadTexture( u8 no , ResTIMG* ti );

#endif // GMRMATERIAL_H
