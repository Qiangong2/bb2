#ifndef __BAUDIO_H__
#define __BAUDIO_H__

#define BAUDIO_FORMAT_PCM       0
#define BAUDIO_FORMAT_ADPCM     1
#define BAUDIO_VOLUME_MIN       0
#define BAUDIO_VOLUME_MAX      63
#define BAUDIO_CHANNELS_STEREO  0
#define BAUDIO_CHANNELS_MONO    1

void AISrcInit(void);

void BAInit (s16 *bufferA, s16 *bufferB, s32 length);
void BAClose(void);

void BAPlay(void);
s32  BAStop(void);

s32 BAFull(void);

void BASetWave    (u8 *dataL, u8 *dataR, s32 length);
void BASetVolume  (u8  volume);
void BASetChannels(u8  channels);

#endif /* __BAUDIO_H__ */
