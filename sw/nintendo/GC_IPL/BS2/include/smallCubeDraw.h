#ifndef __SMALLCUBEDRAW_H__
#define __SMALLCUBEDRAW_H__

#include <dolphin.h>
#include "gMRStruct.h"
#include "gModelRender.h"

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

	void initSmallCubeDraw(void);
	void updateSmallCubeDraw(void);
	void drawSmallCubeDraw(u8 alpha );

#ifdef  __cplusplus
}
#endif  // __cplusplus


#endif // __SMALLCUBEDRAW_H__
