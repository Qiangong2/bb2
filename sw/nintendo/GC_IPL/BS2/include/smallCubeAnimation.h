#ifndef __SMALLCUBEANIMATION_H__
#define __SMALLCUBEANIMATION_H__

#include <dolphin.h>
#include "gMRStruct.h"

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

	void initCubeAnimation(void* data);
	void getAnimCubePos( u32 kind, f32 frame,
						 J3DTransformInfo *cubepos, u32 *cubenum );
	u32 getAnimCubeNum( u32 kind );

#ifdef  __cplusplus
}
#endif  // __cplusplus

#define SCA_ANIMKIND_CALENDAR 0
#define SCA_ANIMKIND_GAMEPLAY 1
#define SCA_ANIMKIND_OPTION   2
#define SCA_ANIMKIND_OPTIONLINEFADEIN 3
#define SCA_ANIMKIND_OPTIONLINEFADEOUT 4
#define SCA_ANIMKIND_GAMEPLAYFADEOUT 5

	
#endif // __SMALLCUBEANIMATION_H__
