/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: Menu StartAnim関連ルーチン
  -----------------------------------------------------------------------------*/
#ifndef MENUSTARTANIM_H
#define MENUSTARTANIM_H

#ifdef __cplusplus
extern "C" {
#endif

typedef
struct SCAnimState
{
	s16		start_frame;
	s16		end_frame;

	s16		base_rot_z;
	s16		radius;
	
	u8		alpha;
	u8		max_alpha;

	f32		x;
	f32		y;
} SCAnimState;

void	initSCAnim( gmrModelData* md , BOOL );
void	initSCAnimState( void );
void	updateSCAnime( void );
void	updateSCAnimeMtx( void );
void	drawSCAnime( void );
void	clearSCAnime( void );

void	concentSCAnime( void );
BOOL	isConcentSCSmallCube( void );

#ifdef __cplusplus
}
#endif

#endif // MENUSTARTANIM_H
