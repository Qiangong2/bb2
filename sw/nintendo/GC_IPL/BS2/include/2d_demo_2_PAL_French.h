#define MSG_S000    0 // [txt0] Lesparamètressystèmeontétéperdus.Lecalendrieretlesautresdonnéesontétéréinitialisés.Configurersystèmemaintenant?
#define MSG_S001    1 // [txt0] Oui
#define MSG_S002    2 // [txt0] Non
#define MSG_S100    3 // [txt1] Vousdevezd'abordparamétrerlecalendrieretlesautresfonctions.Appuyezsurunetouche.
#define MSG_S200    4 // [txt0] Uneerreurestsurvenue.EteignezvotreGAMECUBENINTENDOetconsultezlemanueld'utilisationpourdeplusamplesinformations.
#define MSG_S201    5 // [txt0] Lecturedudisqueimpossible.EteignezvotreGAMECUBENINTENDOetconsultezlemanueld'utilisationpourdeplusamplesinformations.
#define MSG_2D_DEMO_2_PAL_FRENCH_NUM    6
