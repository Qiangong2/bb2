#define MSG_O000    0 // [mesg] ゲームキューブ本体の設定を行います。
#define MSG_O001    1 // [txt1] サウンド
#define MSG_O002    2 // ステレオ
#define MSG_O003    3 // モノラル
#define MSG_O004    4 // [txt2] 画面ヨコ位置
#define MSG_O005    5 // [txt3] 言語
#define MSG_O100    6 // 英語
#define MSG_O101    7 // フランス語
#define MSG_O102    8 // ドイツ語
#define MSG_O103    9 // イタリア語
#define MSG_O104    10 // オランダ語
#define MSG_O105    11 // スペイン語
#define MSG_2D_OPTION_2_J_JAPANESE_NUM    12
