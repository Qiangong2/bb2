/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: System function
  
  初期化関連
  -----------------------------------------------------------------------------*/
#ifndef GINIT_H
#define GINIT_H

#include <secure/boot.h>
#include "BS2Private.h"

#ifdef __cplusplus
extern "C" {
#endif

#define COPY_OVERLAP 2

#define IPL_BANNER_SIZE			(8192)

enum
{
	cIPL_BannerType_OneLanguage,
	cIPL_BannerType_SixLanguage,
	cIPL_BannerType_Disable,
	cIPL_BannerType_Last
};

typedef
struct gBannerComment
{
 	u8	title_3d[32];	// one line
 	u8	maker_3d[32];	// one line
 	u8	title_2d[64];	// one line
 	u8	maker_2d[64];	// one line
 	u8	comment[128];	// two lines
} gBannerComment;	// 320 bytes

typedef
struct gBanner1
{
 	u32	id;				// 'BNR1'
 	u8	header[28];		// padding
 	u8	tex_img[6144];	// RGB5A3 96x32
	gBannerComment	com;
	/*
 	u8	title_3d[32];	// one line
 	u8	maker_3d[32];	// one line
 	u8	title_2d[64];	// one line
 	u8	maker_2d[64];	// one line
 	u8	comment[128];	// two lines
	  */
} gBanner1;

typedef
struct gBanner2
{
 	u32	id;				// 'BNR2'
 	u8	header[28];		// padding
 	u8	tex_img[6144];	// RGB5A3 96x32
	gBannerComment	com[6];	// six languages' comment
} gBanner2;


void gInitGX( u32 fifo_size , GXRenderModeObj* rmode );
void gBeginRender( void );
void gDrawProcMeter( void );
void gDoneRender( void );
void gPreBottomRender( void );
void gCopyDispTop( void );
void gCopyDispBottom( void );
void gSetGXforDirect( int enable_texture, int enable_vtxcolor, int enable_texture_color );

void gCheckSram( void );
BOOL gGetInvalidSram( void );
BOOL gGetInvalidCounterBias( void );
void gSetTimeBaseReg( void );
void gDrawFade( u8 alpha );

void gTickBs2( void );
BS2State gGetBs2State( void );
void	gRunAppUpdate( void );
void	gRunAppUpdateSlow( void );
void	gRunAppDraw( void );
void	gRunAppFadeinSetup( void );
void	gRunAppFadein( void );
u8		gGetRunAppFadeAlpha( void );
BOOL	gCheckReset( void );

void	gInitBannerBuffer( void );
BOOL	gIsBanner( void );
char*	gGetBannerBuffer( void );
u32		gGetBannerType( void );

BOOL	gGetBannerExInfo( u32* support_sector, u32* need_size );
BOOL	gGetBannerExInfoClockAdjust( void );

BOOL	gGetForceMenu( void );
void	gWaitForConfirmCoverState( void );
	
#ifdef __cplusplus
}
#endif

#endif // GINIT_H
