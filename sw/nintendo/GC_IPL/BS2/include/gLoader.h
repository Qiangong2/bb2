/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: アーカイバローダー
  -----------------------------------------------------------------------------*/
#ifndef GLOADER_H
#define GLOADER_H

#if defined(MPAL)
#include "gcarc_mpal.h"
#elif defined(PAL)
#include "gcarc_pal.h"
#else
#include "gcarc_ntsc.h"
#endif // MPAL/PAL/NTSC

#ifdef __cplusplus
extern "C" {
#endif

void	gLoadBinary( void );
void*	gGetBufferPtr( int id );
void	gResolveLanguage( void );
u16		gGetNowCountry( void );
s16		gGetPalCountry( void );
void	gSetPalCountry( int );

int 	getID_2D_memory_layout( void );
int 	getID_2D_memory_string( void );
int 	getID_2D_operation_layout( void );
int 	getID_2D_operation_string( void );
int 	getID_2D_option_layout( void );
int 	getID_2D_option_string( void );
int 	getID_2D_gamestart_layout( void );
int 	getID_2D_gamestart_string( void );
int 	getID_2D_calendar_layout( void );
int 	getID_2D_calendar_string( void );
int 	getID_2D_demo_layout( void );
int 	getID_2D_demo_string( void );

int 	getID_3D_memory_layout( void );
int 	getID_3D_option_layout( void );
int 	getID_3D_gamestart_layout( void );
int 	getID_3D_calendar_layout( void );
int 	getID_3D_main_layout( void );

int 	getID_texture_archive( void );
int 	getID_calendar_num_tex_base( void );
int 	getID_calendar_week_tex_base( void );
int 	getID_option_sound_mono_tex( void );
int 	getID_option_sound_stereo_tex( void );
int 	getID_blocknumber_tex( void );

int 	getID_2D_operation_string_pal( int );
int 	getID_2D_option_string_pal( int );

int 	getID_smallcubefaderdata( void );
 int 	getID_smallcubefaderdata_pal( int );

#define COUNTRY_JAPAN                2
#define COUNTRY_US                   0
#define COUNTRY_BRAZIL				 3

#define COUNTRY_PAL_OFFSET			( 4 )
#define COUNTRY_PAL_ENGLISH			( COUNTRY_PAL_OFFSET + 0 )
#define COUNTRY_PAL_GERMAN			( COUNTRY_PAL_OFFSET + 1 )
#define COUNTRY_PAL_FRENCH			( COUNTRY_PAL_OFFSET + 2 )
#define COUNTRY_PAL_SPANISH			( COUNTRY_PAL_OFFSET + 3 )
#define COUNTRY_PAL_ITALIAN			( COUNTRY_PAL_OFFSET + 4 )
#define COUNTRY_PAL_DUTCH			( COUNTRY_PAL_OFFSET + 5 )
#define COUNTRY_PAL_MAX_LANGUAGE	( 6 )

#ifdef __cplusplus
}
#endif

#endif // GLOADER_H
