/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: J3DBinary 1.5 Renderer
  
  Math�֘A
  -----------------------------------------------------------------------------*/
#ifndef __GMATH_H__
#define __GMATH_H__

#define gABS(a) ((a) >= 0 ? (a) : (-(a)))

#define gMAX(a, b) ((a) > (b) ? (a) : (b))

#define	gPI 	3.141592654f

/*
extern u16	gSinTableSize;
extern int	gSinShift;
extern f32*	gSinTable;
extern f32*	gCosTable;
  */

int gNewSinTable(u8 size);
void gDeleteSinTable(void);
f32 gSSin(s16 angle);
f32 gSCos(s16 angle);

void gEulerToQuat(s16 roll, s16 pitch, s16 yaw, Quaternion* quat);
float gHermiteInterpolation(float time, float f0, float v0, float s0, float f1, float v1, float s1);
float gLagrangeInterpolation(int n, float* xData, float* yData, float xVal);
void gQuatLerp(Quaternion* from, Quaternion* to, float t, Quaternion* res);
#endif  // __GMATH_H__
