/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: Menu ClockAnim関連ルーチン
  -----------------------------------------------------------------------------*/
#ifndef MENUCLOCKANIM_H
#define MENUCLOCKANIM_H

#ifdef __cplusplus
extern "C" {
#endif

typedef
struct OLAnimState
{
	f32		x;
	f32		y;
} OLAnimState;

void	initOLAnim( gmrModelData* md , BOOL );
void	initOLAnimState( void );
void	updateOLAnime( void );
void	updateOLAnimeMtx( void );
void	drawOLAnime( void );
void	clearOLAnime( void );

BOOL	isConcentOLSmallCube( void );
void	concentOLAnime( void );


#ifdef __cplusplus
}
#endif

#endif // MENUCLOCKANIM_H
