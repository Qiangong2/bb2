/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: メモリーカードメニュー時の各面のパラメータ
  -----------------------------------------------------------------------------*/
#ifndef STARTUPDATE_H
#define STARTUPDATE_H

#ifdef __cplusplus
extern "C" {
#endif
	
void	initStartMenu( BOOL );
s32		updateStartMenu( void );
void	resetStartMessage( void );
BOOL	getStartFatalState( void );

void	drawStartMenu( u8 , u8 , u8 );

void	initStartMenu_Lang( void );

#ifdef __cplusplus
}
#endif

#endif //STARTUPDATE_H
