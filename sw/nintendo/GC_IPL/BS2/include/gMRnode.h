/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: J3DBinary 1.5 Renderer
  
  nodeΗ
  -----------------------------------------------------------------------------*/
#ifndef GMRNODE_H
#define GMRNODE_H

#include "gmrJoint.h"
#include "gmrMaterial.h"
#include "gmrShape.h"

/*-----------------------------------------------------------------------------
  \’Μ
  -----------------------------------------------------------------------------*/
typedef struct gmrNode gmrNode;

/*
typedef
struct gmrNodeHeader
{
	u32		type;
	gmrNode*	parent;		// em[h
	gmrNode*	child;		// qm[h
	gmrNode*	prev;		// Zm[h
	gmrNode*	next;		// νm[h
} gmrNodeHeader;
*/

struct gmrNode
{
	u32		type;
	gmrNode*	parent;		// em[h
	gmrNode*	child;		// qm[h
	gmrNode*	prev;		// Zm[h
	gmrNode*	next;		// νm[h
//	gmrNodeHeader	node;
//	void*			nP;
	u16				id;
	u16				dummy;
};

/*
typedef
struct gmrMaterialNode
{
	u32		type;
	gmrNode*	parent;		// em[h
	gmrNode*	child;		// qm[h
	gmrNode*	prev;		// Zm[h
	gmrNode*	next;		// νm[h
//	gmrNodeHeader	node;	// nodeξρ
	gmrMaterial*	mP;		// Material id
} gmrMaterialNode;

typedef
struct gmrShapeNode
{
	u32		type;
	gmrNode*	parent;		// em[h
	gmrNode*	child;		// qm[h
	gmrNode*	prev;		// Zm[h
	gmrNode*	next;		// νm[h
//	gmrNodeHeader	node;
	gmrShape*		sP;		// Shape id
} gmrShapeNode;

typedef
struct gmrJointNode
{
	u32		type;
	gmrNode*	parent;		// em[h
	gmrNode*	child;		// qm[h
	gmrNode*	prev;		// Zm[h
	gmrNode*	next;		// νm[h
//	gmrNodeHeader	node;
	gmrJoint*		jP;		// Joint index
} gmrJointNode;
*/

/*-----------------------------------------------------------------------------
  vg^Cv
  -----------------------------------------------------------------------------*/
void initNode    ( gmrNode* ths );
void addChildNode( gmrNode* ths , gmrNode* node );
void addLastNode ( gmrNode* ths , gmrNode* node );
void insertPrevNode( gmrNode* ths , gmrNode* node );
void insertNextNode( gmrNode* ths , gmrNode* node );
void deleteNode  ( gmrNode* ths );
gmrNode* searchParent( gmrNode* ths );

void dumpTree( gmrNode* root );	// for debug

#endif // GMRNODE_H
