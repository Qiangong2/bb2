#ifdef _JAUDIO_H_
#else
#define _JAUDIO_H_


// ------------------------
//	APP.
// ------------------------


#ifdef __cplusplus 
extern "C" {
#endif

// --効果音-------------------------------------------------
extern void  Jac_PlaySe(u32 se);

#define JAC_SE_LOGO1	(0)		//	普通のロゴ音
#define JAC_SE_LOGO2	(1)		//	プレミアムロゴ音１
#define JAC_SE_LOGO3	(2)		//	プレミアムロゴ音２
// #define JAC_SE_TO_MENU	(2)			//	ロゴからメニューへモーフィング
#define JAC_SE_MAIN_TO_SUB	(3)		//　メインメニューから回転してサブメニューへ
#define JAC_SE_SUB_TO_MAIN	(4)		//　サブメニューへから回転してメインメニューへ　

#define JAC_SE_TO_2D	(5)			//	サブメニューから、ドットマトリクスのエディットモードへの移行
#define JAC_SE_TO_3D	(6)			//	ドットマトリクスから、サブメニューへもどる
#define JAC_SE_CANCEL	(7)			//	エディットモードにてBボタンでキャンセルしたとき
#define JAC_SE_DECIDE	(8)			//	エディットモードにてAボタンを押したとき
#define JAC_SE_VAL_MOVE (9)			//		〃				選んだ項目の数値変更
#define JAC_SE_LR_MOVE  (10)		//		〃				LRで項目を選択
#define JAC_SE_UD_MOVE  (11)		//		〃				UDで項目を選択

									//	メモリーカード 画面でも同じ音を使う
#define JAC_SE_MC_DECIDE (8)		//		項目選択  UD
#define JAC_SE_MC_SELECT (11)		//		項目選択  UD
#define JAC_SE_MC_FUNC  (10)  	    //		機能選択  LR
#define JAC_SE_MC_YESNO (9)			//		OK CANCEL VAL
#define JAC_SE_MC_CANCEL (7)		//		Bボタンキャンセル, あるいは最終確認での「いいえ」選択


#define JAC_SE_FINALDECIDE (12)		//	エディットモードにて最終決定を行ったとき

#define JAC_SE_VAL_LIMIT  (13)      // それ以上値を動かせません（画面位置調整など)
#define JAC_SE_ERROR      (13)		//　同じ音を、エラー・警告音全般に使用します
									// コピー失敗
									// コピー先に同じファイルがあります etc.

// #define JAC_SE_MC_MOVE	 (14)		//		(旧仕様)メモリーカード　ムーブ・コピー時の移動アニメ1回毎によぶ
#define JAC_SE_MC_DELETE (15)		//		メモリーカード　単独消去時
#define JAC_SE_MC_CREATE (16)		//		メモリーカード　コピー完了時

#define JAC_SE_MC_MOVE_LTOC	 (17)		//		メモカ  コピー・ムーブ開始時  (1→2)
#define JAC_SE_MC_MOVE_RTOC	 (18)		//		            〃                (2←1)

#define JAC_SE_MC_MOVE_CTOLR (19)		//		メモカ　 コピー完了時
#define JAC_SE_MC_MOVE_CTOR	 (20)		//		メモカ　 ムーブ完了時 (1→2)
#define JAC_SE_MC_MOVE_CTOL	 (21)		//		メモカ　    〃        (2→1)

#define JAC_SE_MC_FORMAT   (16)			//		メモリーカード　フォーマット完了時

#define JAC_SE_GAME_START   (22)		//		メニューからのゲーム起動


//	キューブモーフィング
//	基準面が正面にやってくる時の回転速度（絶対値)を与えて下さい
//	速度が0-1 に正規化されているとありがたいです。
//	メニューキューブに変化するまではmode=0,モーフィング後はmode=1でお願いします。


extern void Jac_PlayCubeMorphing(u8 mode ,f32 speed);		


//	音の中断　(今のところは、使う必要はありません)
							
extern void  Jac_StopSe(u32 se);



// --音楽--------------------------------------------------
extern void  Jac_PlayBgm(u32 bgm);
extern void  Jac_StopBgm(void);

#define JAC_BGM_MENU (0)		//	メニューキューブ中にバックでうっすら流れているBGM

// -------------------------------------------------------


// extern void  Jac_PlayInit(void);
// extern void	 Jac_Archiver_Init(void);

#ifdef __cplusplus 
}
#endif

// ------------------------
//	SYS.
// ------------------------

#ifdef __cplusplus 
extern "C" {
#endif

extern void Jac_Start(void *audiohptop,u32 audiohpsize,u32 aramsize);

// extern void StartAudioThread(void *audiohptop,u32 audiohpsize,u32 aramsize);
extern void Jac_Gsync(void);
extern void Jac_VframeWork(void);


// -------------------------------------------------------
extern void Jac_OutputMode(u32 mode);

#define JAC_MODE_MONO	(0)		//	モノラル出力
#define JAC_MODE_STEREO	(1)		//	ステレオ出力
// -------------------------------------------------------


// -------------------------------------------------------
extern void Jac_Freeze_Precall(void);
extern BOOL Jac_Silence_Check(void);
extern void Jac_Freeze(void);
extern void Jac_StopSoundAll(void);



extern void Probe_Start(s32 id,char *name);
extern void Probe_Finish(s32 id);

#ifdef __cplusplus 
}
#endif


#endif		// JAUDIO_H


