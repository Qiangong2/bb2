#include <dolphin.h>
#include <private/flipper.h>

#include "iplsec.h"

#ifdef __cplusplus 
extern "C" {
#endif

extern u32 Jac_IPLDspSec(SECParam *param);


#ifdef __cplusplus 
}
#endif


u32 IPLDspSec(SECParam *param)
{
 u32 st;
#ifdef _DEBUG
  OSReport(" (IPLSec by DSP) ");
#endif

  st=	Jac_IPLDspSec(param);

  return (st);
}

