/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: メモリーカードのアイコンの移動
  -----------------------------------------------------------------------------*/
#include <dolphin.h>

#include "gMath.h"
#include "gTrans.h"
#include "gInit.h"
#include "gLoader.h"
#include "gRand.h"

#include "gCamera.h"
#include "gModelRender.h"

#include "CardSequence.h"
#include "CardUpdate.h"
#include "CardIconMove.h"
#include "CardState.h"

#include "MenuCardAnim.h"
#include "MenuUpdate.h"

#if defined(MPAL)
#include "gcarc_mpal.h"
#elif defined(PAL)
#include "gcarc_pal.h"
#else
#include "gcarc_ntsc.h"
#endif // MPAL/PAL/NTSC

/*-----------------------------------------------------------------------------
  static funcions
  -----------------------------------------------------------------------------*/
static void		resetIconMoveState( IconMoveState* ims );
static void		setIconMoveStateToEnd( IconMoveState* ims );
static BOOL		checkImsPosframe( IconMoveState* ims );

/*-----------------------------------------------------------------------------
  statics
  -----------------------------------------------------------------------------*/
#define		PARTICLE_NUM	12
static IconMoveState	ims[2][CARD_MAX_FILE];
static s16		(*icon_x)[IPL_MAX_ICON_DISPLAY_NUM];
static s16		(*icon_y)[IPL_MAX_ICON_DISPLAY_NUM];

static gmrModelData	icon0;
static gmrModelData	icon1;
static gmrModel		icon_nofile[2][CARD_MAX_FILE];
static gmrModel		icon_file[2][CARD_MAX_FILE];
static gmrModel		delete_model;
static gmrModel		crash_model[PARTICLE_NUM];

static u8			sCmdSrcChan;
static u16			sCmdFromPos;
static u16			sCmdFromFileNo;
static u16			sLastCmd;
static s16			sBackup_x;
static s16			sBackup_y;

static IconMoveState	anim_ims;
static BOOL				animating;
static BOOL				sFading;
static BOOL				sDeleting;
static BOOL				sCrashAnime;
static s16				swing_frame;
static s16				crash_frame[PARTICLE_NUM];
static s16				crash_frame_max[PARTICLE_NUM];
static s16				crash_frame_min[PARTICLE_NUM];
static Mtx				crash_mtx;
static s16				crash_part[PARTICLE_NUM];

/*-----------------------------------------------------------------------------
  アイコン位置情報の初期化をします。
  -----------------------------------------------------------------------------*/
static void
initCardIconMoveSub( IconMoveState* ims )
{
	Vec zero = { 0.f, 0.f, 0.f };
	CardState	*csp = getCardState();
	
	resetIconMoveState( ims );	// フレームをリセット。
	ims->now_pos     = zero;
	ims->now_slope   = zero;
	ims->start_pos   = zero;
	ims->start_slope = zero;
	ims->end_pos	 = zero;
	ims->max_alpha	 = 0;
	ims->now_alpha   = 0;

	ims->now_scale_frame = 0;
	ims->max_scale_frame = 6;
	ims->max_scale = csp->icon_max_scale;
	ims->min_scale = csp->icon_min_scale;
	ims->now_scale = ims->min_scale;
}

void
initCardIconMove(
	BOOL	first,
	s16		(*i_x)[IPL_MAX_ICON_DISPLAY_NUM],
	s16		(*i_y)[IPL_MAX_ICON_DISPLAY_NUM]
	)
{
	int chan, fileNo, i;

	if ( first ) {
		//モデルデータの作成。
		icon0.modeldata = gGetBufferPtr( GC_icon_cube0_szp );
		icon1.modeldata = gGetBufferPtr( GC_icon_cube1_szp );
		buildModelData( &icon0 );
		buildModelData( &icon1 );
		for ( chan = 0 ; chan < 2 ; chan++ ) {
			for ( fileNo = 0 ; fileNo < CARD_MAX_FILE ; fileNo++ ) {
				icon_nofile[chan][fileNo].gmrmd = &icon0;
				icon_file[chan][fileNo].gmrmd   = &icon1;
				buildModel( &icon_nofile[chan][fileNo], FALSE );
				buildModel( &icon_file[chan][fileNo], FALSE );
			}
		}

		delete_model.gmrmd = &icon1;
		buildModel( &delete_model, FALSE );

		for ( i=0 ; i<PARTICLE_NUM ; i++ ) {
			crash_model[i].gmrmd = &icon1;
			buildModel( &crash_model[i], FALSE );
		}
	}
	
	icon_x = i_x;
	icon_y = i_y;
	
	for ( chan = 0 ; chan < 2 ; chan++ ) {
		for ( fileNo = 0 ; fileNo < CARD_MAX_FILE ; fileNo++ ) {
			initCardIconMoveSub( &ims[chan][fileNo] );
		}
	}
	animating = FALSE;
	sFading = TRUE;
	sDeleting = FALSE;
	sCrashAnime = FALSE;
	swing_frame = 0;
	initCardIconMoveSub( &anim_ims );
}

/*-----------------------------------------------------------------------------
	モデルの位置を更新します。
  -----------------------------------------------------------------------------*/
static void
updateModelPos( void )
{
	int chan, fileNo;
	Mtx	m, t, rot;
	MenuState*	mesp = getMenuState();
	CardState*	csp  = getCardState();
	J3DTransformInfo ti = (J3DTransformInfo){ 1.0f, 1.0f, 1.0f, 0, 0, 0, 0, -292, 224, 0 };
	DirStateArrayPtr tDir = getCardDirState();
	Vec		scale, pos;
	s16		frame = swing_frame;
	f32		scale_base;

	gGetTransformMtx( &ti, t );

	for ( chan = 0 ; chan < 2 ; chan++ ) {
		for ( fileNo = 0 ; fileNo < CARD_MAX_FILE ; fileNo++ ) {
			scale = (Vec){
				ims[chan][fileNo].now_scale,
				ims[chan][fileNo].now_scale,
				ims[chan][fileNo].now_scale };

			if ( chan == sCmdSrcChan &&
				 fileNo == sCmdFromFileNo &&
				 animating ) {
				if ( !sDeleting ) {
					scale_base = (f32)( ( anim_ims.now_scale - anim_ims.min_scale ) /
										( anim_ims.max_scale - anim_ims.min_scale ) );
					pos = (Vec){ anim_ims.now_pos.x/16,
						-anim_ims.now_pos.y/16,
						scale_base * 2 };
				}
			} else {
				scale_base = (f32)( ( ims[chan][fileNo].now_scale - ims[chan][fileNo].min_scale ) /
									( ims[chan][fileNo].max_scale - ims[chan][fileNo].min_scale ) );
				pos = (Vec) { ims[chan][fileNo].now_pos.x/16,
					-ims[chan][fileNo].now_pos.y/16,
					scale_base * 2 };
			}
			gGetRotateMtx(
				(s16)( scale_base * mesp->menu_swing_amp_x * gSCos( (s16)((70) * frame) )),
				(s16)( scale_base * mesp->menu_swing_amp_y * gSCos( (s16)((35) * frame + mesp->menu_swing_offset_y ) )),	// yオフセット追加。
				(s16)( scale_base * mesp->menu_swing_amp_z * gSCos( (s16)((35) * frame) )),
				rot );
			MTXTrans( m ,
					  pos.x + scale_base * csp->swing_x * gSSin( (s16)((35) * frame + mesp->menu_slide_offset_x ) ),	// xオフセット追加。
					  pos.y - scale_base * csp->swing_y * gSSin( (s16)((70) * frame) ),
					  pos.z );
			MTXConcat( m, t, m );
			MTXConcat( m, rot , m );

			if ( tDir[chan][fileNo].validate ) {
				setBaseMtx( &icon_file[chan][fileNo], m, scale );
				setViewMtx( &icon_file[chan][fileNo], gGetSmallCubeCamera() );
				updateModel( &icon_file[chan][fileNo] );
			} else {
				setBaseMtx( &icon_nofile[chan][fileNo], m, scale );
				setViewMtx( &icon_nofile[chan][fileNo], gGetSmallCubeCamera() );
				updateModel( &icon_nofile[chan][fileNo] );
			}
		}
	}

	if ( sDeleting ) {
		//削除アニメ。
		scale = (Vec){
			anim_ims.now_scale,
			anim_ims.now_scale,
			anim_ims.now_scale };
		scale_base = (f32)( ( anim_ims.now_scale - anim_ims.min_scale ) /
							( anim_ims.max_scale - anim_ims.min_scale ) );
		pos = (Vec){ anim_ims.now_pos.x/16,
			-anim_ims.now_pos.y/16,
			scale_base * 2 };
		gGetRotateMtx(
			(s16)( scale_base * mesp->menu_swing_amp_x * gSCos( (s16)((70) * frame) )),
			(s16)( scale_base * mesp->menu_swing_amp_y * gSCos( (s16)((35) * frame + mesp->menu_swing_offset_y ) )),	// yオフセット追加。
			(s16)( scale_base * mesp->menu_swing_amp_z * gSCos( (s16)((35) * frame) )),
			rot );
		MTXTrans( m ,
				  pos.x + scale_base * csp->swing_x * gSSin( (s16)((35) * frame + mesp->menu_slide_offset_x ) ),	// xオフセット追加。
				  pos.y - scale_base * csp->swing_y * gSSin( (s16)((70) * frame) ),
				  pos.z );
		MTXConcat( m, t, m );
		MTXConcat( m, rot , m );
		if ( !sCrashAnime ) {
			setBaseMtx( &delete_model, m, scale );
			setViewMtx( &delete_model, gGetSmallCubeCamera() );
			updateModel( &delete_model );
		} else {
//			MTXConcat( gGetSmallCubeCamera(), m, crash_mtx );
			MTXCopy( m, crash_mtx );
		}
	}

}

/*-----------------------------------------------------------------------------
  アイコンの位置を更新します。
  基本的に、すべてのアイコンの位置を更新します。
  -----------------------------------------------------------------------------*/
// アルファを更新します。
static void
updateImsAlpha( IconMoveState* ims )
{
	s16	val;

	if ( sFading ) {
		val = 255 / 10;
	} else {
		val = 255 / 30;
	}
	if ( ims->now_frame >= ims->min_frame ) {
		if ( ims->max_alpha == 255 ) {
			ims->now_alpha += val;
		} else {
			ims->now_alpha -= val;
		}
	}
	
	if ( ims->now_alpha > 255 ) ims->now_alpha = 255;
	if ( ims->now_alpha <   0 ) ims->now_alpha =   0;
}

// 位置を更新します。
static void
updateImsPos( IconMoveState* ims )
{
	Vec new_pos;
	Vec	zero = { 0.f, 0.f, 0.f };

	if ( ims->now_frame >= ims->min_frame ) {
		gGetVecInterp( ims->now_frame,
					   ims->min_frame, &ims->start_pos, &ims->start_slope,
					   ims->max_frame, &ims->end_pos  , &zero,
					   &new_pos );
	} else {
		new_pos = ims->start_pos;
	}
	VECSubtract( &new_pos, &ims->now_pos, &ims->now_slope );
	ims->now_pos = new_pos;

	ims->now_frame++;
	
	if ( ims->now_frame > ims->max_frame ) {
		ims->now_frame = ims->max_frame;
	}
}

static void
updateImsScale( IconMoveState* ims, s32 chan, s32 fileNo )
{
#ifndef IPL
	CardState	*csp = getCardState();
#endif
	ims->now_scale = gHermiteInterpolation( ims->now_scale_frame,
											0                   , ims->min_scale, 0,
											ims->max_scale_frame, ims->max_scale, 0 );
	
	if ( chan == getCardSelectChan() &&
		 fileNo == getCardSelectFileNo() &&
		 !sFading ) {
		ims->now_scale_frame++;
	} else {
		ims->now_scale_frame--;
	}
	if ( ims->now_scale_frame > ims->max_scale_frame ) ims->now_scale_frame = ims->max_scale_frame;
	if ( ims->now_scale_frame < 0 ) ims->now_scale_frame = 0;

#ifndef IPL
	ims->max_scale = csp->icon_max_scale;
	ims->min_scale = csp->icon_min_scale;
#endif
}

static BOOL
updateImsScaleForDelete( IconMoveState* ims )
{
	BOOL ret = FALSE;
	
	ims->now_scale = gHermiteInterpolation( ims->now_scale_frame,
											0                   , ims->min_scale, 0,
											ims->max_scale_frame, ims->max_scale, 0 );
	
	ims->now_scale_frame--;

	if ( ims->now_scale_frame > ims->max_scale_frame ) ims->now_scale_frame = ims->max_scale_frame;
	if ( ims->now_scale_frame < 0 ) {
		ims->now_scale_frame = 0;
		ret = TRUE;
	}

	return ret;
}

static void
updateAnimateImsPos( void )
{
	anim_ims.start_pos = anim_ims.now_pos;
	anim_ims.start_slope = anim_ims.now_slope;
	
	if ( anim_ims.end_pos.x >= 292 * 16 ) {
		 anim_ims.end_pos.x -= ( anim_ims.end_pos.x - 292 * 16 ) * 2;
	} else {
		 anim_ims.end_pos.x += ( 292 * 16 - anim_ims.end_pos.x ) * 2;
	}
	if ( anim_ims.end_pos.y >= 200 * 16 ) {
		 anim_ims.end_pos.y -= ( anim_ims.end_pos.y - 200 * 16 ) * 2;
	} else {
		 anim_ims.end_pos.y += ( 200 * 16 - anim_ims.end_pos.y ) * 2;
	}

	anim_ims.now_frame = 0;
	anim_ims.max_frame = 180;
}

static void
updateParticle( int i )
{
	Mtx m;
	Vec pos;
	f32	val;
	f32	s;

	if ( crash_frame[i] > crash_frame_min[i] ) {
		if ( crash_frame[i] - crash_frame_min[i] <= crash_frame_max[i] ) {
			val = (f32)( crash_frame[i] - crash_frame_min[i] ) / crash_frame_max[i];
		} else {
			val = 0.f;
		}
	} else {
		val = 0.f;
	}
	//s = gSSin( 16384* val);
	s = 0.25f + 0.75f * val;
	pos.x = 48* s  * gSSin( crash_part[i] );
	pos.y = 48* s  * gSCos( crash_part[i] );
	pos.z = 0;
	MTXTrans( m, pos.x, pos.y, pos.z );
	MTXConcat( crash_mtx, m, m );
	setBaseMtx( &crash_model[i], m, (Vec){0.3f, 0.3f, 0.3f} );
	setViewMtx( &crash_model[i], gGetSmallCubeCamera() );
	updateModel( &crash_model[i] );
	//////
}

static void
initCrashFileAnime( void )
{
	int i;

	for ( i=0 ; i<PARTICLE_NUM ; i++ ) {
		crash_frame[i] = 0;
		crash_frame_max[i] = 10;
		crash_frame_min[i] = (s16)( urand() & 3 );
		crash_part[i] = (s16)( urand() & 0xffff );
	}
}

static BOOL
updateCrashFileAnime( void )
{
	BOOL	ret = FALSE;
	int i, all = 0;

	for ( i=0 ; i<PARTICLE_NUM ; i++ ) {
		crash_frame[i]++;
		if ( crash_frame[i] > crash_frame_min [i] ) {
			if ( ( crash_frame[i] - crash_frame_min[i] ) > crash_frame_max[i] ) {
				crash_frame[i] = (s16)( crash_frame_max[i] + crash_frame_min[i] );
				all ++;
			}
		}
		updateParticle( i );
	}
	if ( all == PARTICLE_NUM ) ret = TRUE;

	return ret;
}

// カードアイコンの移動を更新するメインルーチン。
void
updateCardIconMove( void )
{
	s32	chan, fileNo;
	u32	stop_pos = 0;

	if ( !isConcentCCSmallCube() ) return;

	updateModelPos( );
	
	// fileNoは、表示上のファイル番号。
	if ( sDeleting ) {
		// 削除アニメーションの更新。
		if ( animating ) {
			// 消えるまで。
			updateImsScaleForDelete( &anim_ims );
		} else {
			// 消えた後。
			if ( updateImsScaleForDelete( &anim_ims ) ) {
				if ( !sCrashAnime ) initCrashFileAnime();
				sCrashAnime = TRUE;
				// 砕けるアニメーション。
				if ( updateCrashFileAnime() ) {
					sCrashAnime = FALSE;
					sDeleting = FALSE;
				}
			}
		}
	} else {
		for ( chan = 0 ; chan < 2 ; chan++ ){
			for ( fileNo = 0 ; fileNo < CARD_MAX_FILE ; fileNo++ ) { 
				updateImsAlpha( &ims[chan][fileNo] );
				updateImsPos  ( &ims[chan][fileNo] );
				updateImsScale( &ims[chan][fileNo] , chan , fileNo );
			}
		}
		if ( animating ) {
			updateImsPos( &anim_ims );
			if ( anim_ims.now_frame >= ( anim_ims.max_frame - 5 )  ) {
				// コピー／移動中アニメーションの位置更新。
				updateAnimateImsPos();
			}
		}
	}

	if ( sFading && isCardIconAnimeDone() ) {
		for ( chan = 0 ; chan < 2 ; chan++ ){
			for ( fileNo = 0 ; fileNo < CARD_MAX_FILE ; fileNo++ ) { 
				setIconMoveStateToEnd( &ims[chan][fileNo] );
			}
		}
		sFading = FALSE;
	}

	swing_frame+=7;
}

/*-----------------------------------------------------------------------------
  CardIconMove構造体のアプリループ中に行ってもいい初期化を行います。
  -----------------------------------------------------------------------------*/
static void
resetIconMoveState(
	IconMoveState*	ims
	)
{
	ims->min_frame = 0;
	ims->now_frame = ims->min_frame;
	ims->max_frame = 40;
}

static void
setIconMoveStateToEnd(
	IconMoveState*	ims
	)
{
	ims->min_frame = 0;
	ims->max_frame = 40;
	ims->now_frame = ims->max_frame;
}

void
presetCardIconPos(
	u8	src_chan,
	u16	dispPos,
	u16	fileNo,
	u16	cmd
	)
{
	s16 r;
	
	sCmdSrcChan = src_chan;
	sCmdFromPos = dispPos;
	sCmdFromFileNo = fileNo;
	sLastCmd = cmd;

	if ( !( sLastCmd == IPL_CARD_CMD_FILECOPY ||
			sLastCmd == IPL_CARD_CMD_FILEMOVE ||
			sLastCmd == IPL_CARD_CMD_FILEDELETE ) ) return;
	
	if ( sLastCmd != IPL_CARD_CMD_FILEDELETE ) {
		anim_ims = ims[sCmdSrcChan][sCmdFromFileNo];
		anim_ims.start_pos   = ims[sCmdSrcChan][sCmdFromFileNo].now_pos;
		anim_ims.start_slope = ims[sCmdSrcChan][sCmdFromFileNo].now_slope;
		anim_ims.now_pos	 = ims[sCmdSrcChan][sCmdFromFileNo].now_pos;
		anim_ims.now_slope	 = ims[sCmdSrcChan][sCmdFromFileNo].now_slope;

		// アニメ中のファイルの表示を消す。
		ims[sCmdSrcChan][sCmdFromFileNo].now_alpha = 0;
		ims[sCmdSrcChan][sCmdFromFileNo].max_alpha = 0;

		r = (s16)( urand() & 0xffff );
		anim_ims.end_pos = (Vec){
			(f32)( ( 292 + 40 * gSSin( r ) ) * 16),
			(f32)( ( 200 + 40 * gSCos( r ) ) * 16),
			0.f };
		anim_ims.now_frame = 0;
		anim_ims.max_frame = 40;
	} else {
		anim_ims = ims[sCmdSrcChan][sCmdFromFileNo];

		// アニメ中のファイルの表示を消す。
		ims[sCmdSrcChan][sCmdFromFileNo].now_alpha = 0;
		ims[sCmdSrcChan][sCmdFromFileNo].max_alpha = 0;

		anim_ims.now_scale_frame = 20;
		anim_ims.max_scale_frame = 20;
		anim_ims.min_scale = 0.0f;
		sDeleting = TRUE;
		sCrashAnime = FALSE;
//		sDeleting = FALSE;
	}
	
	animating = TRUE;
}

void
setCardIconPos(
	u8	dstFileNo
	)
{
	u32		fn = 0;

	if ( !( sLastCmd == IPL_CARD_CMD_FILECOPY ||
			sLastCmd == IPL_CARD_CMD_FILEMOVE ||
			sLastCmd == IPL_CARD_CMD_FILEDELETE ) ) return;
	
	animating = FALSE;

	if ( sLastCmd != IPL_CARD_CMD_FILEDELETE ) {
		ims[sCmdSrcChan^1][dstFileNo].now_pos = anim_ims.now_pos;
		if ( sLastCmd == IPL_CARD_CMD_FILECOPY ) {
			ims[sCmdSrcChan][sCmdFromFileNo].now_pos = anim_ims.now_pos;
		}

		if ( sCmdFromPos < 8 ) {
			ims[sCmdSrcChan^1][dstFileNo].now_slope = (Vec){ 0.f, 150.f, 0.f };
		} else {
			ims[sCmdSrcChan^1][dstFileNo].now_slope = (Vec){ 0.f, -150.f, 0.f };
		}

		// 最大の大きさにする。
		ims[sCmdSrcChan^1][dstFileNo].now_scale = ims[sCmdSrcChan^1][dstFileNo].max_scale;
		ims[sCmdSrcChan^1][dstFileNo].now_scale_frame = ims[sCmdSrcChan^1][dstFileNo].max_scale_frame;
		ims[sCmdSrcChan^1][dstFileNo].now_alpha = 255;
		ims[sCmdSrcChan^1][dstFileNo].max_alpha = 255;
		ims[sCmdSrcChan^1][dstFileNo].max_frame = 60;
	}

	if ( sLastCmd != IPL_CARD_CMD_FILECOPY ) {
		ims[sCmdSrcChan][sCmdFromFileNo].now_alpha = 0;
		ims[sCmdSrcChan][sCmdFromFileNo].max_alpha = 0;
	}
}

void
cancelCardIconPos( void )
{
	if ( animating ) {
		animating = FALSE;
		if ( sDeleting ) {
			sDeleting = FALSE;
			sCrashAnime = FALSE;
			// 削除中に変わったところをそのアイコンに反映。
		}
		ims[sCmdSrcChan][sCmdFromFileNo].now_pos = anim_ims.now_pos;
		ims[sCmdSrcChan][sCmdFromFileNo].start_pos = anim_ims.now_pos;
		ims[sCmdSrcChan][sCmdFromFileNo].now_frame = 0;
		ims[sCmdSrcChan][sCmdFromFileNo].max_frame = 40;
		ims[sCmdSrcChan][sCmdFromFileNo].now_alpha = 255;
		ims[sCmdSrcChan][sCmdFromFileNo].max_alpha = 255;
	}
}

/*-----------------------------------------------------------------------------
  アイコンの位置・アルファをセットします。
  -----------------------------------------------------------------------------*/
void
setCardIconMove(
	FileArray (*fap)[CARD_MAX_FILE],
	s8*		now_line
	)
{
	u32	chan, fileNo, dirNo;

	// fileNoは、表示上のファイル番号。
	// dirNoは、カード内のファイルの順番。
	for ( chan = 0 ; chan < 2 ; chan++ ){
		for ( fileNo = 0 ; fileNo < CARD_MAX_FILE ; fileNo++ ) { 
			dirNo = fap[chan][fileNo].fileNo;

			resetIconMoveState( &ims[chan][dirNo] );

			if ( fileNo >= now_line[chan]*4 &&
				 fileNo <= now_line[chan]*4 + 15
				 )
			{
				// 表示されるファイル。
				ims[chan][dirNo].max_alpha = 255;
				ims[chan][dirNo].start_pos   = ims[chan][dirNo].now_pos;
				ims[chan][dirNo].start_slope = ims[chan][dirNo].now_slope;
				
				ims[chan][dirNo].end_pos.x = (f32)icon_x[chan][fileNo - now_line[chan]*4];
				ims[chan][dirNo].end_pos.y = (f32)icon_y[chan][fileNo - now_line[chan]*4];
				ims[chan][dirNo].end_pos.z = 0.f;
			}
			else
			{
				// 非表示になるファイル。
				ims[chan][dirNo].max_alpha = 0;
				ims[chan][dirNo].start_pos   = ims[chan][dirNo].now_pos;
				ims[chan][dirNo].start_slope = ims[chan][dirNo].now_slope;
				if ( fileNo < now_line[chan]*4 ) { // 上。
					ims[chan][dirNo].end_pos.x = (f32)icon_x[chan][fileNo%4];
					ims[chan][dirNo].end_pos.y = (f32)icon_y[chan][fileNo%4] - 32 * 16;
				}
				if ( fileNo > now_line[chan]*4+15 ) { // 下。
					ims[chan][dirNo].end_pos.x = (f32)icon_x[chan][12 + fileNo%4];
					ims[chan][dirNo].end_pos.y = (f32)icon_y[chan][12 + fileNo%4] + 32 * 16;
				}
				ims[chan][dirNo].end_pos.z = 0.f;
			}
		}
	}
}

/*-----------------------------------------------------------------------------
  カードメニューに入るときに、中央に移動させる。
  -----------------------------------------------------------------------------*/
void
resetCardIconMove( void )
{
	int chan, fileNo;
	u32 r;
	
	for ( chan = 0 ; chan < 2 ; chan++ ){
		for ( fileNo = 0 ; fileNo < CARD_MAX_FILE ; fileNo++ ) { 
			ims[chan][fileNo].now_pos   = (Vec){ (f32)(292 * 16), (f32)(224 * 16), 0.f };

			r = urand() & 0xffff;
			ims[chan][fileNo].now_slope = (Vec){ 350.f * gSSin( (s16)r ), 350.f * gSCos( (s16)r ), 0.f};
			ims[chan][fileNo].start_pos   = ims[chan][fileNo].now_pos;
			ims[chan][fileNo].start_slope = ims[chan][fileNo].now_slope;
			ims[chan][fileNo].now_alpha = 0;
			ims[chan][fileNo].now_frame = 0;

			r = urand() & 0xf;
			ims[chan][fileNo].min_frame = (s16)(0 + r);
#ifdef PAL
			ims[chan][fileNo].max_frame = (s16)(33 + r);
#else
			ims[chan][fileNo].max_frame = (s16)(40 + r);
#endif
		}
	}

	sFading = TRUE;
}

/*-----------------------------------------------------------------------------
	スロットから抜かれたときに、スロットの中央に寄せる。
  -----------------------------------------------------------------------------*/
void
clearCardIconMoveSlot( s32 chan )
{
	u32	all_clear = 0;
	s32	fileNo;

	for ( fileNo = 0 ; fileNo < CARD_MAX_FILE ; fileNo++ ) { 
		ims[chan][fileNo].max_alpha = 0;
		if ( ims[chan][fileNo].now_alpha == 0 )  all_clear++;
	}

	if ( all_clear == CARD_MAX_FILE ) {
		// 全部消えている。
		for ( fileNo = 0 ; fileNo < CARD_MAX_FILE ; fileNo++ ) { 
			ims[chan][fileNo].now_pos   =
					(Vec){ 
						(icon_x[chan][ 5]+icon_x[chan][ 6] )/2,
						(icon_y[chan][ 5]+icon_y[chan][ 9] )/2,
						0.f };
			ims[chan][fileNo].now_slope = (Vec){ 0.f,  0.f, 0.f };
			ims[chan][fileNo].start_pos   = ims[chan][fileNo].now_pos;
			ims[chan][fileNo].start_slope = ims[chan][fileNo].now_slope;
			ims[chan][fileNo].now_alpha = 0;
			ims[chan][fileNo].now_frame = 0;
		}
	}
}

static BOOL
checkImsPosframe( IconMoveState* ims )
{
	if ( ims->now_frame == ims->max_frame ) return TRUE;

	return FALSE;
}

static void
fixAllIconNowPos( void )
{
	u32 chan, fileNo;
	SlotState* tSlot = getCardSlotState();
	
	for ( chan = 0 ; chan < 2 ; chan++ ){
		if ( tSlot[chan].state == SLOT_READY ) {
			for ( fileNo = 0 ; fileNo < CARD_MAX_FILE ; fileNo++ ) { 
				ims[chan][fileNo].start_pos   = ims[chan][fileNo].now_pos;
				ims[chan][fileNo].start_slope = ims[chan][fileNo].now_slope;
			}
		}
	}
}

BOOL
isCardIconAnimeDone( void )
{
	s32		all = 0;
	s32		chan, fileNo;
	SlotState* tSlot = getCardSlotState();

	if ( !isMenuDispMemoryCard() ) return FALSE;
	
	// fileNoは、表示上のファイル番号。
	for ( chan = 0 ; chan < 2 ; chan++ ){
		if ( tSlot[chan].state == SLOT_READY ) {
			for ( fileNo = 0 ; fileNo < CARD_MAX_FILE ; fileNo++ ) { 
				//dirNo = fap[chan][fileNo].fileNo;
				if ( checkImsPosframe( &ims[chan][fileNo] ) ) all++;
			}
		} else {
			all += CARD_MAX_FILE;
		}
	}

	if ( ( all == CARD_MAX_FILE * 2 ) && !sDeleting ) {
		fixAllIconNowPos();
		return TRUE;
	}

	return FALSE;
}

// 暫定
s16
getCardIconMovePosX( s32 chan, s32 fileNo )
{
	return (s16)ims[chan][fileNo].now_pos.x;
}
s16
getCardIconMovePosY( s32 chan, s32 fileNo )
{
	return (s16)ims[chan][fileNo].now_pos.y;
}
s16
getCardIconMoveAlpha( s32 chan, s32 fileNo )
{
	return (s16)ims[chan][fileNo].now_alpha;
}


















/*-----------------------------------------------------------------------------
  アイコンを描画します。
  -----------------------------------------------------------------------------*/
static GXColorS10 nofile_8k_noselect = {   0,  20,  60, 255};
static GXColorS10 nofile_8k_select 	 = {  30,  80, 100, 255};
static GXColorS10 file_8k_noselect 	 = {  20,  50, 200, 255};
static GXColorS10 file_8k_select	 = { 100, 150, 255, 255};

static GXColorS10 nofile_16k_noselect = {   0,  37,   0, 255};
static GXColorS10 nofile_16k_select   = {  50, 120,  80, 255};
static GXColorS10 file_16k_noselect   = {   0, 105,  70, 255};
static GXColorS10 file_16k_select	  = { 100, 255, 110, 255};

static GXColorS10 nofile_32k_noselect = {  28,  34,   0, 255};
static GXColorS10 nofile_32k_select   = {  90, 110,   0, 255};
static GXColorS10 file_32k_noselect   = {  80, 120,   0, 255};
static GXColorS10 file_32k_select	  = { 200, 255,  75, 255};

static GXColorS10 nofile_64k_noselect = {  50,  30,   0, 255};
static GXColorS10 nofile_64k_select   = { 100,  80,   0, 255};
static GXColorS10 file_64k_noselect   = { 160, 100,   0, 255};
static GXColorS10 file_64k_select	  = { 255, 180,  50, 255};

static GXColorS10 nofile_128k_noselect = {  50,  20,   0, 255};
static GXColorS10 nofile_128k_select   = { 150,  60,  50, 255};
static GXColorS10 file_128k_noselect   = { 160,  45,  50, 255};
static GXColorS10 file_128k_select	   = { 255, 110, 100, 255};

static GXColorS10 nofile_256k_noselect = {  45,   0,  60, 255};
static GXColorS10 nofile_256k_select   = { 100,  40,  90, 255};
static GXColorS10 file_256k_noselect   = {  90,  30, 170, 255};
static GXColorS10 file_256k_select	   = { 200,  80, 190, 255};

#ifndef IPL
static GXColorS10 hio_sel_file[2];
static GXColorS10 hio_nosel_file[2];
static GXColorS10 hio_sel_nofile[2];
static GXColorS10 hio_nosel_nofile[2];
#endif

static GXColorS10*
getSectorColor( u32	sector_size, u32 type, s32 chan )
{
#ifndef IPL
#pragma unused( chan )
	CardState	*csp = getCardState();
#else
#pragma unused ( chan )
#endif
	GXColorS10	*ret;
	
	switch ( sector_size )
	{
	case (1024 *   8):
		switch ( type )	{
		case IPL_FILE_SELECT:		ret = &file_8k_select;		break;
		case IPL_FILE_NOSELECT:		ret = &file_8k_noselect;	break;
		case IPL_NOFILE_SELECT:		ret = &nofile_8k_select;	break;
		case IPL_NOFILE_NOSELECT:	ret = &nofile_8k_noselect;	break;
		}
		break;
	case (1024 *  16):
		switch ( type )	{
		case IPL_FILE_SELECT:		ret = &file_16k_select;		break;
		case IPL_FILE_NOSELECT:		ret = &file_16k_noselect;	break;
		case IPL_NOFILE_SELECT:		ret = &nofile_16k_select;	break;
		case IPL_NOFILE_NOSELECT:	ret = &nofile_16k_noselect;	break;
		}
		break;
	case (1024 *  32):
		switch ( type )	{
		case IPL_FILE_SELECT:		ret = &file_32k_select;		break;
		case IPL_FILE_NOSELECT:		ret = &file_32k_noselect;	break;
		case IPL_NOFILE_SELECT:		ret = &nofile_32k_select;	break;
		case IPL_NOFILE_NOSELECT:	ret = &nofile_32k_noselect;	break;
		}
		break;
	case (1024 *  64):
		switch ( type )	{
		case IPL_FILE_SELECT:		ret = &file_64k_select;		break;
		case IPL_FILE_NOSELECT:		ret = &file_64k_noselect;	break;
		case IPL_NOFILE_SELECT:		ret = &nofile_64k_select;	break;
		case IPL_NOFILE_NOSELECT:	ret = &nofile_64k_noselect;	break;
		}
		break;
	case (1024 * 128):
		switch ( type )	{
		case IPL_FILE_SELECT:		ret = &file_128k_select;	break;
		case IPL_FILE_NOSELECT:		ret = &file_128k_noselect;	break;
		case IPL_NOFILE_SELECT:		ret = &nofile_128k_select;	break;
		case IPL_NOFILE_NOSELECT:	ret = &nofile_128k_noselect;break;
		}
		break;
	case (1024 * 256):
		switch ( type )	{
		case IPL_FILE_SELECT:		ret = &file_256k_select;	break;
		case IPL_FILE_NOSELECT:		ret = &file_256k_noselect;	break;
		case IPL_NOFILE_SELECT:		ret = &nofile_256k_select;	break;
		case IPL_NOFILE_NOSELECT:	ret = &nofile_256k_noselect;break;
		}
		break;
	}
	
	return ret;
}

static void
drawCubeModelIconSub( gmrModel*	model, u8 chan, int fileNo )
{
	u16  backup;
	SlotState* tSlot = getCardSlotState();
	GXColorS10 *a0, *a2;
	
	if ( model->modelAlpha > 0 ) {
		a0 = model->gmrmd->material[0].tevColorPtr[0];
		a2 = model->gmrmd->material[2].tevColorPtr[0];
		if ( chan == getCardSelectChan() &&
			 fileNo == getCardSelectFileNo() &&
			 !sFading ) {
			// tevの置き換え。
			model->gmrmd->material[0].tevColorPtr[0] = getSectorColor( tSlot[chan].sector_size, IPL_FILE_SELECT , chan );
			model->gmrmd->material[2].tevColorPtr[0] = getSectorColor( tSlot[chan].sector_size, IPL_FILE_SELECT , chan );
		} else {
			model->gmrmd->material[0].tevColorPtr[0] = getSectorColor( tSlot[chan].sector_size, IPL_FILE_NOSELECT , chan );
			model->gmrmd->material[2].tevColorPtr[0] = getSectorColor( tSlot[chan].sector_size, IPL_FILE_NOSELECT , chan );
		}
		drawModelNode( model , &model->gmrmd->root[2] );
		drawModelNode( model , &model->gmrmd->root[10] );

		// tevの置き換え。
		model->gmrmd->material[0].tevColorPtr[0] = a0;
		model->gmrmd->material[2].tevColorPtr[0] = a2;

		if ( loadIconAnime( chan, fileNo ) ) {
			backup = model->gmrmd->material[1].texNo[0];
			model->gmrmd->material[1].texNo[0] = 0xffff;
			drawModelNode( model , &model->gmrmd->root[6] );
			model->gmrmd->material[1].texNo[0] = backup;
		}
	}
}

static void
drawCubeModelNoIconSub( gmrModel* model, u8 chan, int fileNo )
{
	SlotState* tSlot = getCardSlotState();
	GXColorS10 *a0, *a1;
	
	if ( model->modelAlpha > 0 ) {
		a0 = model->gmrmd->material[0].tevColorPtr[0];
		a1 = model->gmrmd->material[1].tevColorPtr[0];
		if ( chan == getCardSelectChan() &&
			 fileNo == getCardSelectFileNo() &&
			 !sFading ) {
			model->gmrmd->material[0].tevColorPtr[0] = getSectorColor( tSlot[chan].sector_size, IPL_NOFILE_SELECT , chan );
			model->gmrmd->material[1].tevColorPtr[0] = getSectorColor( tSlot[chan].sector_size, IPL_NOFILE_SELECT , chan );
		} else {
			model->gmrmd->material[0].tevColorPtr[0] = getSectorColor( tSlot[chan].sector_size, IPL_NOFILE_NOSELECT , chan );
			model->gmrmd->material[1].tevColorPtr[0] = getSectorColor( tSlot[chan].sector_size, IPL_NOFILE_NOSELECT , chan );
		}

		drawModel( model );

		model->gmrmd->material[0].tevColorPtr[0] = a0;
		model->gmrmd->material[1].tevColorPtr[0] = a1;
	}
}

static void
drawParticleModel( gmrModel* model )
{
	SlotState* tSlot = getCardSlotState();
	GXColorS10 *a0, *a2;
	
	if ( model->modelAlpha > 0 ) {
		a0 = model->gmrmd->material[0].tevColorPtr[0];
		a2 = model->gmrmd->material[2].tevColorPtr[0];
		model->gmrmd->material[0].tevColorPtr[0] = getSectorColor( tSlot[sCmdSrcChan].sector_size, IPL_FILE_NOSELECT , sCmdSrcChan );
		model->gmrmd->material[2].tevColorPtr[0] = getSectorColor( tSlot[sCmdSrcChan].sector_size, IPL_FILE_NOSELECT , sCmdSrcChan );
		drawModelNode( model , &model->gmrmd->root[2] );
		drawModelNode( model , &model->gmrmd->root[10] );
		// tevの置き換え。
		model->gmrmd->material[0].tevColorPtr[0] = a0;
		model->gmrmd->material[2].tevColorPtr[0] = a2;
	}
}

static void
drawParticle( u8 alpha )
{
	int i;
	u8		model_alpha;
	
	for ( i=0 ; i < PARTICLE_NUM ; i++ ) {
		if ( crash_frame[i] > crash_frame_min[i] ) {
			if ( crash_frame[i] - crash_frame_min[i] <= crash_frame_max[i] ) {
				model_alpha = (u8)( alpha * ( crash_frame_max[i] - crash_frame[i] + crash_frame_min[i] )/ crash_frame_max[i] );
			} else {
				model_alpha = 0;
			}
		} else {
			model_alpha = 0;
		}
		if ( model_alpha > 0 ) {
			crash_model[i].modelAlpha = model_alpha;

			drawParticleModel( &crash_model[i] );
		}
	}
	
}

static void
drawCubeModelPos( u8 chan, u8 alpha )
{
	int fileNo;
	DirStateArrayPtr tDir = getCardDirState();

	for ( fileNo = 0 ; fileNo < CARD_MAX_FILE ; fileNo++ ) {
		if ( chan == sCmdSrcChan &&
			 fileNo == sCmdFromFileNo &&
			 animating ) {	// アニメ中。
			icon_file[chan][fileNo].modelAlpha   = 0;//anim_ims.now_alpha * alpha / 255;
		} else if ( chan == getCardSelectChan() &&
					fileNo == getCardSelectFileNo() &&
					!sFading ) {
			icon_file[chan][fileNo].modelAlpha   = 0;
		} else {
			icon_file[chan][fileNo].modelAlpha   = (u8)( ims[chan][fileNo].now_alpha * alpha / 255 );
		}
		icon_nofile[chan][fileNo].modelAlpha = (u8)( ims[chan][fileNo].now_alpha * alpha / 255 );

		if ( tDir[chan][fileNo].validate ) {
			drawCubeModelIconSub( &icon_file[chan][fileNo], chan, fileNo );
		} else {
			drawCubeModelNoIconSub( &icon_nofile[chan][fileNo], chan, fileNo );
		}
	}
	if ( animating ) {
		if ( !sDeleting ) {
			chan = sCmdSrcChan;
			fileNo = sCmdFromFileNo;
			if ( tDir[chan][fileNo].validate ) {
				icon_file[chan][fileNo].modelAlpha   = (u8)( anim_ims.now_alpha * alpha / 255 );
				drawCubeModelIconSub( &icon_file[chan][fileNo], chan, fileNo );
			}
		}
	} else if ( !sFading ) {
		chan = (u8)getCardSelectChan();
		fileNo = getCardSelectFileNo();
		if ( tDir[chan][fileNo].validate ) {
			icon_file[chan][fileNo].modelAlpha   = (u8)( ims[chan][fileNo].now_alpha * alpha / 255 );
			drawCubeModelIconSub( &icon_file[chan][fileNo], chan, fileNo );
		}
	}

	if ( sDeleting ) {
		if ( !sCrashAnime ) {
			// 削除中の表示。
			delete_model.modelAlpha = (u8)( anim_ims.now_alpha * alpha / 255 );
			drawCubeModelIconSub( &delete_model, sCmdSrcChan, -1 );
		} else {
			// 削除後の表示。
			drawParticle( alpha );
		}
	}
}

void
drawCardIconMove( u8 chan, u8 alpha )
{
	drawCubeModelPos( chan, alpha );
}

BOOL
isCardIconFadeDone( void )
{
	return sFading? FALSE: TRUE;
}
