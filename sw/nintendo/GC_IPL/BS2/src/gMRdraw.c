/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: J3DBinary 1.5 Renderer
  
  描画実装
  -----------------------------------------------------------------------------*/
#include <dolphin.h>
#include <math.h>

#include "gMath.h"
#include "gTrans.h"
#include "gCamera.h"
#include "gBase.h"

#include "gModelRender.h"
#include "gMRmaterial.h"
#include "gmrJoint.h"
#include "gmrShape.h"
#include "gmrMaterial.h"
#include "gmrAnime.h"

/*-----------------------------------------------------------------------------
  
  -----------------------------------------------------------------------------*/
static void calcTransformBasic( u16 no, J3DTransformInfo* ti );
//static void calcTransformSI( u16 no, J3DTransformInfo* ti );
static void setVtxFmt( void );
static void updateNode( gmrNode* node );
static void updateAnimeNode( gmrNode* node , f32 frame );
static void drawShape(	u16 shapeID , u16 preMatID );
static void drawNode( gmrNode* node );
static void viewCalc( gmrModel* d );
//static void drawInit( void );
static void updateToBillBoardMtx( MtxPtr src );
/*-----------------------------------------------------------------------------
  ファイルローカルもの
  -----------------------------------------------------------------------------*/
static gmrModelData* currentModelData;
static gmrModel* currentModel;
//static Mtx currentMtx;
//static Vec currentS;


/*-----------------------------------------------------------------------------
  anmMtxに代入
  -----------------------------------------------------------------------------*/
static void
setAnmMtx( u16 no , Mtx m )
{
//	DIReport("Buf:%d\n", no );
	MTXCopy( m , currentModel->animMtx[no] );
}

/*-----------------------------------------------------------------------------
  カレントマトリックスを取得。
MtxPtr
gmrGetCurrentMtx( void )
{
	return (MtxPtr)currentMtx;
}
  -----------------------------------------------------------------------------*/


/*-----------------------------------------------------------------------------
  クラシックスケールON
  -----------------------------------------------------------------------------*/
static void
calcTransformBasic( u16 no,J3DTransformInfo* ti )
{
    // トランスフォーム情報で計算します
    // スケール
    Mtx m;

	gGetTranslateRotateMtx(ti->rx, ti->ry, ti->rz,
						   ti->tx, ti->ty, ti->tz,
						   m);

    // カレントスケールが１でなかったら、スケール計算
    if (ti->sx != 1.f || ti->sy != 1.f || ti->sz != 1.f) {
        f32* mtxPtr = (f32*)m;
        *mtxPtr *= ti->sx;
        mtxPtr++;
        *mtxPtr *= ti->sy;
        mtxPtr++;
        *mtxPtr *= ti->sz;
        mtxPtr+=2;
        *mtxPtr *= ti->sx;
        mtxPtr++;
        *mtxPtr *= ti->sy;
        mtxPtr++;
        *mtxPtr *= ti->sz;
        mtxPtr+=2;
        *mtxPtr *= ti->sx;
        mtxPtr++;
        *mtxPtr *= ti->sy;
        mtxPtr++;
        *mtxPtr *= ti->sz;
    }
    
    // parentTRS * T*R*S

    MTXConcat(currentModel->currentMtx, m, currentModel->currentMtx);

	setAnmMtx(no, currentModel->currentMtx);
}

/*-----------------------------------------------------------------------------
  クラシックスケールOFF
static void
calcTransformSI(u16 no, J3DTransformInfo* ti)
{
    // トランスフォーム情報で計算します
    // スケール
    Mtx m;

    // parent TR * TR
    gGetTranslateRotateMtx(ti->rx, ti->ry, ti->rz,
						   ti->tx*currentModel->currentS.x, ti->ty*currentModel->currentS.y, ti->tz*currentModel->currentS.z,
						   m);

    MTXConcat(currentModel->currentMtx, m, currentModel->currentMtx);

    currentModel->currentS.x *= ti->sx;
    currentModel->currentS.y *= ti->sy;
    currentModel->currentS.z *= ti->sz;
    
    if (currentModel->currentS.x != 1.f || currentModel->currentS.y != 1.f || currentModel->currentS.z != 1.f) {
        f32* trsMtxPtr = (f32*)m;
        f32* trMtxPtr = (f32*)currentModel->currentMtx;
        *trsMtxPtr++ = (*trMtxPtr++)*currentModel->currentS.x;
        *trsMtxPtr++ = (*trMtxPtr++)*currentModel->currentS.y;
        *trsMtxPtr++ = (*trMtxPtr++)*currentModel->currentS.z;
        *trsMtxPtr++ = *trMtxPtr++;
        *trsMtxPtr++ = (*trMtxPtr++)*currentModel->currentS.x;
        *trsMtxPtr++ = (*trMtxPtr++)*currentModel->currentS.y;
        *trsMtxPtr++ = (*trMtxPtr++)*currentModel->currentS.z;
        *trsMtxPtr++ = *trMtxPtr++;
        *trsMtxPtr++ = (*trMtxPtr++)*currentModel->currentS.x;
        *trsMtxPtr++ = (*trMtxPtr++)*currentModel->currentS.y;
        *trsMtxPtr++ = (*trMtxPtr++)*currentModel->currentS.z;
        *trsMtxPtr++ = *trMtxPtr++;
        // マトリクスを保存
        setAnmMtx(no, m);
    } else {
        setAnmMtx(no, currentModel->currentMtx);
    }
}
  -----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
  VertexAttrFmtをセットする。
  -----------------------------------------------------------------------------*/
static void
setVtxFmt(void)
{
    GXVtxAttrFmtList*	vatPt = currentModelData->VertexP->vtxAttr;
    u8 unitSize;
	
    while(vatPt->attr != GX_VA_NULL) {
        switch(vatPt->attr) {
        case GX_VA_POS:

			switch(vatPt->cnt)
			{
            case GX_POS_XY:     unitSize = 2;  break;
            case GX_POS_XYZ:
            default:            unitSize = 3;  break;
            }

			switch(vatPt->type)
			{
            case GX_U8:  unitSize *= sizeof(u8);  break;
            case GX_S8:  unitSize *= sizeof(s8);  break;
            case GX_U16: unitSize *= sizeof(u16); break;
            case GX_S16: unitSize *= sizeof(s16); break;
            case GX_F32: unitSize *= sizeof(f32); break;
            }
            GXSetArray(GX_VA_POS, currentModelData->VertexP->vtxPos, unitSize);
            break;

		case GX_VA_NRM:
            unitSize = 3;
            switch(vatPt->type)
			{
            case GX_S8:  unitSize *= sizeof(s8);  break;
            case GX_S16: unitSize *= sizeof(s16); break;
            case GX_F32: unitSize *= sizeof(f32); break;
            }
            GXSetArray(GX_VA_NRM, currentModelData->VertexP->vtxNrm, unitSize);
            break;

		case GX_VA_CLR0:
        case GX_VA_CLR1:
			DIReport("draw:SRC_VTX\n");
            unitSize = 1;
            switch(vatPt->type)
			{
            case GX_RGB565:
            case GX_RGBA4:  unitSize *= 2;    break;

			case GX_RGB8:
            case GX_RGBA6:  unitSize *= 3;    break;

			case GX_RGBX8:
            case GX_RGBA8:
            default:        unitSize *= 4;    break;
            }
            GXSetArray(vatPt->attr, currentModelData->VertexP->vtxColor[(u8)(vatPt->attr-GX_VA_CLR0)], unitSize);
            break;

		case GX_VA_TEX0:
        case GX_VA_TEX1:
        case GX_VA_TEX2:
        case GX_VA_TEX3:
        case GX_VA_TEX4:
        case GX_VA_TEX5:
        case GX_VA_TEX6:
        case GX_VA_TEX7:
            switch(vatPt->cnt)
			{
            case GX_TEX_S:  unitSize = 1;  break;
            case GX_TEX_ST:
            default:        unitSize = 2;  break;
            }
            switch(vatPt->type)
			{
            case GX_U8:  unitSize *= sizeof(u8);  break;
            case GX_S8:  unitSize *= sizeof(s8);  break;
            case GX_U16: unitSize *= sizeof(u16); break;
            case GX_S16: unitSize *= sizeof(s16); break;
            case GX_F32: unitSize *= sizeof(f32); break;
            }
            GXSetArray(vatPt->attr, currentModelData->VertexP->vtxTexCoord[(u8)(vatPt->attr-GX_VA_TEX0)], unitSize);
            break;

		default:
            break;
        }
        vatPt++;
    }

    GXSetVtxAttrFmtv(GX_VTXFMT0, currentModelData->VertexP->vtxAttr);
}

/*-----------------------------------------------------------------------------
  再帰的にノードを更新します。
  -----------------------------------------------------------------------------*/
static void
updateNode( gmrNode* node )
{
	Mtx backupMtx;
	//Vec backupScale;

	MTXCopy( currentModel->currentMtx , backupMtx );
//	backupScale = currentModel->currentS;

	if ( node->type == cNodeJoint ) {
		calcTransformBasic( node->id, &( currentModelData->joint[ node->id ].transformInfo ) );
	}

	if ( node->child != NULL ) {
 		updateNode( node->child );
	}

//	currentModel->currentS = backupScale;
	MTXCopy( backupMtx , currentModel->currentMtx );

	if ( node->next != NULL ) {
		updateNode( node->next );
	}
}

/*-----------------------------------------------------------------------------
  モデルのアップデート
  各JOINTのMtxを計算する。
  -----------------------------------------------------------------------------*/
void
updateModel( gmrModel* d )
{
//	Mtx scale;
	currentModel = d;
	currentModelData = d->gmrmd;//sGMRD = d->gmrmd;

	MTXCopy( currentModel->baseTRMtx , currentModel->currentMtx );
//	currentModel->currentS = currentModel->baseScale;
	
//	gScaleNrmMtx( currentModel->currentMtx , &currentModel->currentS );
	updateNode( d->gmrmd->root );
}


/*-----------------------------------------------------------------------------
  再帰的にノードを更新します。
  -----------------------------------------------------------------------------*/
static void
updateAnimeNode( gmrNode* node , f32 frame )
{
	J3DTransformInfo ti;
	Mtx backupMtx;
	//Vec backupScale;

	MTXCopy( currentModel->currentMtx , backupMtx );
//	backupScale = currentModel->currentS;

	if ( node->type == cNodeJoint ) {
		getTransform( frame, node->id , &ti );
		calcTransformBasic( node->id, &ti );
	}

	if ( node->child != NULL ) {
		updateAnimeNode( node->child , frame );
	}

//	currentModel->currentS = backupScale;
	MTXCopy( backupMtx , currentModel->currentMtx );

	if ( node->next != NULL ) {
		updateAnimeNode( node->next , frame );
	}
}

/*-----------------------------------------------------------------------------
  マテリアルカラーアニメを更新します。
  -----------------------------------------------------------------------------*/
static void
updateMatAnime( f32 frame )
{
	u16 i;
//  DIReport("num:%d\n",currentModel->gmrmd->MaterialP->materialNum);
	for ( i = 0 ; i < currentModel->gmrmd->MaterialP->materialNum ; i++ ) {
		
		getMatColor( frame,
					 i,
					 &currentModel->mat_color[ i ]
					 );
		currentModelData->material[ i ].colorPtr[0]	= &currentModel->mat_color[ i ];// 更新すべき指定マテリアルの色。
	}
}


/*-----------------------------------------------------------------------------
  アニメーションフレームframeの状態にupdateするように
  各JOINTのMtxを計算します。
  -----------------------------------------------------------------------------*/
void
updateAnime( gmrModel* d , f32 frame )
{
//	Mtx scale;
	currentModel = d;
	currentModelData = d->gmrmd;//sGMRD = d->gmrmd;

	setCurrentAnimeModel( d );

	MTXCopy( currentModel->baseTRMtx , currentModel->currentMtx );
//	currentModel->currentS = currentModel->baseScale;
//	gScaleNrmMtx( currentModel->currentMtx , &currentModel->currentS );

	updateAnimeNode( d->gmrmd->root , frame );

	if ( currentModel->c_anime != NULL ) {
		updateMatAnime( frame );
	}
}


/*-----------------------------------------------------------------------------
  環境マップ用のマトリックスロード
  -----------------------------------------------------------------------------*/
static void loadEnvMtx(
     Mtx			 mtxPtr,
	 Mtx			 texMtx,
     u32             id, 
     GXTexMtxType    type )
{
//#pragma unused(mtxPtr)
/*	
	Mtx nrm , trans;
	J3DTransformInfo ti = { 0.5f, -0.5f, 0.0f , 0, 0, 0, 0, 1.0f, 1.0f, 0.0f }; // 環境マップの結果の補正用
	MTXConcat( getCurrentCamera() , mtxPtr , trans );
	gGetInverseTranspose( trans , nrm );
	gGetTransformMtx( &ti , trans );
	MTXConcat( nrm, trans, nrm );
	MTXConcat( texMtx , nrm ,  nrm );
	GXLoadTexMtxImm( nrm , id , type );
*/
	Mtx nrm;
//	MTXConcat( getCurrentCamera() , mtxPtr , nrm );
	MTXCopy( mtxPtr , nrm );
	nrm[0][3] = 0.f;
	nrm[1][3] = 0.f;
	nrm[2][3] = 0.f;

	MTXConcat( texMtx , nrm ,  nrm );
	GXLoadTexMtxImm( nrm , id , type );
	
}

/*-----------------------------------------------------------------------------
  
  -----------------------------------------------------------------------------*/
static void
checkTexMtx( u16 i , Mtx inv )
{
	u8 texMapFlag[10];
	gmrMaterial* mArray = currentModelData->material;
	int j;

	if ( i == 0xffff ) return;
	
	for ( j=0 ; j<10 ; j++ ){
		texMapFlag[j] = cTexMapNormal;
	}
	
	for ( j=0 ; j<mArray[i].texGenNum ; j++ ) {
		if ( mArray[i].texCoordPtr[j] != NULL ) {
			if ( mArray[i].texCoordPtr[j]->texGenSrc == GX_TG_NRM )	{
				texMapFlag[ (mArray[i].texCoordPtr[j]->texMtx - GX_TEXMTX0 ) / 3] = cTexMapEnv;
			}
		}
	}

	for ( j=0 ; j<10 ; j++ ){
		if ( mArray[i].texMtxPtr[j] != NULL ) {
			if ( texMapFlag[j] == cTexMapEnv ) {
				loadEnvMtx( inv ,
							mArray[i].texMtxPtr[j]->mtx,
							(u32)(GX_TEXMTX0+j*3),
							(GXTexMtxType)mArray[i].texMtxPtr[j]->texGenType);
			}
		}
	}
}
/*-----------------------------------------------------------------------------
  なんとなく、描画
  -----------------------------------------------------------------------------*/
static void
updateToBillBoardMtx( MtxPtr src )
{
    f32 lx = sqrtf(src[0][0]*src[0][0]+src[1][0]*src[1][0]+src[2][0]*src[2][0]);
    f32 ly = sqrtf(src[0][1]*src[0][1]+src[1][1]*src[1][1]+src[2][1]*src[2][1]);
    f32 lz = sqrtf(src[0][2]*src[0][2]+src[1][2]*src[1][2]+src[2][2]*src[2][2]);
    
    src[0][0] = lx;
    src[0][1] = 0.f;
    src[0][2] = 0.f;
    
    src[1][0] = 0.f;
    src[1][1] = ly;
    src[1][2] = 0.f;
    
    src[2][0] = 0.f;
    src[2][1] = 0.f;
    src[2][2] = lz;
}

static void
drawShape(
	u16 shapeID ,
	u16 materialID )
{
	Mtx			inv;
	MtxPtr		load;
	GXAttrType	attrType;
	gmrShape*	shp = &currentModelData->shape[ shapeID ];

//	if ( currentModel->enable_viewcalc ) {
//		load = currentModel->drawMtx[ shp->shapeMtx[0].baseId ];	// viewCalc後
//	} else {
		load = currentModel->animMtx[ currentModelData->DrawP->drawMtxIndex[ shp->shapeMtx[0].baseId ] ];	// viewCalc前
//	}

	GXClearVtxDesc();
	GXSetVtxDescv( shp->vtxDescList );

	if ( shp->mtxType == 1 ) { // ビルボード
		updateToBillBoardMtx( load );//currentModel->drawMtx[ shp->shapeMtx[0].baseId ] );
	}
	
//	gGetInverseTranspose ( currentModel->drawMtx[ shp->shapeMtx[0].baseId ] , inv );
	gGetInverseTranspose ( load , inv );

	GXGetVtxDesc( GX_VA_NBT , &attrType );
	if ( attrType != GX_NONE ) {
		gScaleNrmMtx( inv , getNBTScale() );
	}

//	GXLoadPosMtxImm( currentModel->drawMtx[ shp->shapeMtx[0].baseId ] , GX_PNMTX0);
	GXLoadPosMtxImm( load , GX_PNMTX0);
	GXLoadNrmMtxImm( inv , GX_PNMTX0 );

//	checkTexMtx( materialID , currentModel->drawMtx[ shp->shapeMtx[0].baseId ] );
	checkTexMtx( materialID , load );
//	checkTexMtx( materialID , inv ); 

	GXCallDisplayList( shp->shapeDraw[0].displayList , shp->shapeDraw[0].size );
}

/*-----------------------------------------------------------------------------
  再帰で描画
  -----------------------------------------------------------------------------*/
static void
drawNode( gmrNode* node )
{
	static u16	preMatID = 0xffff;
	
	if ( node->type == cNodeShape ) {
		drawShape( node->id , preMatID );	// drawin
	}
	else if ( node->type == cNodeMaterial ) {
		loadMaterial( node->id );
		preMatID = node->id;
	}

	if ( node->child != NULL ) {
		drawNode( node->child );
	}

	if (node->next != NULL ) {
		drawNode( node->next );
	}
}

/*-----------------------------------------------------------------------------
  View x AnimMtx => DrawMtxを実行。
static void
viewCalc( gmrModel* d )
{
	u16 mct;
	
	for ( mct = 0 ; mct < currentModelData->DrawP->drawMtxNum ; mct++ ) {
		if ( currentModelData->DrawP->drawMtxFlag[ mct ] == 0 ) { // Full Mtx
			MTXConcat( d->viewMtx ,
					   d->animMtx[ currentModelData->DrawP->drawMtxIndex[ mct ] ]  ,
					   d->drawMtx[ mct ] );
		}
		else {
			OSHalt("ERROR: This renderer doesn't support Weighted model\n");
		}
	}
}
  -----------------------------------------------------------------------------*/


/*-----------------------------------------------------------------------------
  GXの初期化
  -----------------------------------------------------------------------------*/
void
drawInit( void )
{
    u8 i;
    Mtx m;
	
    GXSetCullMode(GX_CULL_BACK);
    GXSetCoPlanar( GX_DISABLE );
    GXSetColorUpdate( GX_ENABLE );
	GXSetAlphaUpdate( GX_ENABLE );
    GXSetDither( GX_ENABLE );
	GXSetBlendMode( GX_BM_BLEND , GX_BL_SRCALPHA, GX_BL_INVSRCALPHA, GX_LO_CLEAR );
//    GXSetBlendMode(GX_BM_BLEND, GX_BL_SRCALPHA, GX_BL_INVSRCALPHA, GX_LO_NOOP);

    GXSetVtxAttrFmt(GX_VTXFMT0, GX_VA_POS, GX_POS_XYZ, GX_F32, 0);
    GXSetVtxAttrFmt(GX_VTXFMT0, GX_VA_NRM, GX_NRM_XYZ, GX_F32, 0);
    //GXSetVtxAttrFmt(GX_VTXFMT0, GX_VA_NBT, GX_NRM_NBT, GX_F32, 0);
    GXSetVtxAttrFmt(GX_VTXFMT0, GX_VA_CLR0, GX_CLR_RGBA, GX_RGBA8, 0);
    GXSetVtxAttrFmt(GX_VTXFMT0, GX_VA_CLR1, GX_CLR_RGBA, GX_RGBA8, 0);
    GXSetVtxAttrFmt(GX_VTXFMT0, GX_VA_TEX0, GX_TEX_ST, GX_S16, 7);	// これはS16かF32かデータフォーマットによる？
    GXSetVtxAttrFmt(GX_VTXFMT0, GX_VA_TEX1, GX_TEX_ST, GX_S16, 7);
    GXSetVtxAttrFmt(GX_VTXFMT0, GX_VA_TEX2, GX_TEX_ST, GX_S16, 7);
    GXSetVtxAttrFmt(GX_VTXFMT0, GX_VA_TEX3, GX_TEX_ST, GX_S16, 7);
    GXSetVtxAttrFmt(GX_VTXFMT0, GX_VA_TEX4, GX_TEX_ST, GX_S16, 7);
    GXSetVtxAttrFmt(GX_VTXFMT0, GX_VA_TEX5, GX_TEX_ST, GX_S16, 7);
    GXSetVtxAttrFmt(GX_VTXFMT0, GX_VA_TEX6, GX_TEX_ST, GX_S16, 7);
    GXSetVtxAttrFmt(GX_VTXFMT0, GX_VA_TEX7, GX_TEX_ST, GX_S16, 7);
    
    // TEV共通設定 もっと細かい設定を行いたくなったら新たにノードを作成する
    GXSetTevOrder(GX_TEVSTAGE0, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
    GXSetTevOrder(GX_TEVSTAGE1, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
    GXSetTevOrder(GX_TEVSTAGE2, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
    GXSetTevOrder(GX_TEVSTAGE3, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
    GXSetTevOrder(GX_TEVSTAGE4, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
    GXSetTevOrder(GX_TEVSTAGE5, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
    GXSetTevOrder(GX_TEVSTAGE6, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
    GXSetTevOrder(GX_TEVSTAGE7, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
    GXSetTevOrder(GX_TEVSTAGE8, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
    GXSetTevOrder(GX_TEVSTAGE9, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
    GXSetTevOrder(GX_TEVSTAGE10,GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
    GXSetTevOrder(GX_TEVSTAGE11,GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
    GXSetTevOrder(GX_TEVSTAGE12,GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
    GXSetTevOrder(GX_TEVSTAGE13,GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
    GXSetTevOrder(GX_TEVSTAGE14,GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
    GXSetTevOrder(GX_TEVSTAGE15,GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
	GXSetTevOp(GX_TEVSTAGE0, GX_REPLACE);

    MTXIdentity(m);
    for (i=0; i<10 ; i++) {
		GXLoadPosMtxImm( m , (u32)(GX_PNMTX0 + i*3 ) );
        GXLoadNrmMtxImm( m , (u32)(GX_PNMTX0 + i*3 ) );
		GXLoadTexMtxImm( m , (u32)(GX_TEXMTX0 + i*3 ), GX_MTX2x4);
    }

	GXSetChanMatColor( GX_COLOR0A0 , (GXColor){0xff,0xff,0xff,0xff} );
	GXSetChanMatColor( GX_COLOR1A1 , (GXColor){0xff,0xff,0xff,0xff} );
//	GXSetChanAmbColor( GX_COLOR0A0 , (GXColor){0xff,0xff,0xff,0xff} );
//	GXSetChanAmbColor( GX_COLOR1A1 , (GXColor){0xff,0xff,0xff,0xff} );
	GXSetNumChans ( 1 );

	GXSetChanCtrl( GX_COLOR0A0 , 
				   GX_DISABLE,
				   GX_SRC_REG,
				   GX_SRC_REG,
				   GX_LIGHT_NULL,
				   GX_DF_CLAMP,
				   GX_AF_NONE);
	GXSetChanCtrl( GX_COLOR1A1, 
				   GX_DISABLE,
				   GX_SRC_REG,
				   GX_SRC_REG,
				   GX_LIGHT_NULL,
				   GX_DF_CLAMP,
				   GX_AF_NONE);

	GXSetNumTexGens( 0 );

	GXSetTexCoordGen(GX_TEXCOORD0, GX_TG_MTX2x4, GX_TG_TEX0, GX_IDENTITY);
    GXSetTexCoordGen(GX_TEXCOORD1, GX_TG_MTX2x4, GX_TG_TEX1, GX_IDENTITY);
    GXSetTexCoordGen(GX_TEXCOORD2, GX_TG_MTX2x4, GX_TG_TEX2, GX_IDENTITY);
    GXSetTexCoordGen(GX_TEXCOORD3, GX_TG_MTX2x4, GX_TG_TEX3, GX_IDENTITY);
    GXSetTexCoordGen(GX_TEXCOORD4, GX_TG_MTX2x4, GX_TG_TEX4, GX_IDENTITY);
    GXSetTexCoordGen(GX_TEXCOORD5, GX_TG_MTX2x4, GX_TG_TEX5, GX_IDENTITY);
    GXSetTexCoordGen(GX_TEXCOORD6, GX_TG_MTX2x4, GX_TG_TEX6, GX_IDENTITY);
    GXSetTexCoordGen(GX_TEXCOORD7, GX_TG_MTX2x4, GX_TG_TEX7, GX_IDENTITY);
	
    GXClearVtxDesc();
    GXInvalidateVtxCache();
    GXInvalidateTexAll();

    GXSetAlphaCompare(GX_ALWAYS, 0, GX_AOP_OR, GX_ALWAYS, 0);
}
/*-----------------------------------------------------------------------------
  モデルを描画する。
  -----------------------------------------------------------------------------*/
void
drawModel( gmrModel* d )
{
	currentModel = d;
	currentModelData = d->gmrmd;

//	drawInit();
	setVtxFmt();
//	if ( d->enable_viewcalc ) viewCalc( d );
	setMaterialModel( d );
	drawNode( d->gmrmd->root );
}

/*-----------------------------------------------------------------------------
  モデルをノードから描画する。
  -----------------------------------------------------------------------------*/
void
drawModelNode( gmrModel* d , gmrNode* node )
{
	currentModel = d;
	currentModelData = d->gmrmd;

//	drawInit();
	setVtxFmt();
//	if ( d->enable_viewcalc ) viewCalc( d );
	setMaterialModel( d );
	drawNode( node );
}

/*-----------------------------------------------------------------------------
  モデルをノードから描画する。
  -----------------------------------------------------------------------------*/
void
drawModelNodeSmallCube( gmrModel* d , gmrNode* node )
{
	currentModel = d;
	currentModelData = d->gmrmd;

//	drawInit();
//	setVtxFmt();
//	if ( d->enable_viewcalc ) viewCalc( d );
	setMaterialModel( d );
	loadMaterialSmallCube( node[0].id );
	drawNode( &node[1] );
}

/*-----------------------------------------------------------------------------
  ベースとなるMtx/Scaleをセットする。
  -----------------------------------------------------------------------------*/
void
setBaseMtx( gmrModel* d , MtxPtr base , Vec scale )
{
	MTXCopy( base , d->baseTRMtx );
	gScaleNrmMtx( d->baseTRMtx, &scale );
}

/*-----------------------------------------------------------------------------
  ViewMtxをセットする。
  -----------------------------------------------------------------------------*/
void
setViewMtx( gmrModel* d , MtxPtr view )
{
	MTXConcat( view , d->baseTRMtx, d->baseTRMtx );
}
