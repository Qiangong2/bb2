/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: Splashデモ メインルーチン

  シーケンスを含めたSplashデモのメイン
  -----------------------------------------------------------------------------*/
#include <dolphin.h>

#include "BS2Private.h"

#include "Splash.h"

#include "gCamera.h"
#include "gDynamic.h"
#include "gInit.h"
#include "gMainState.h"
#include "gLoader.h"

#include "SplashMain.h"
#include "SplashUpdate.h"
#include "SplashDraw.h"

#include "SplashtoMenuUpdate.h"

#include "MenuUpdate.h"

#include "LogoUpdate.h"
#include "LogoDraw.h"

//デジカードの各面のパラメータ用。
#include "StartState.h"
#include "CardState.h"
#include "OptionState.h"
#include "TimeState.h"

#ifdef JHOSTIO
#include "IHISplashState.h"
#endif

#include "jaudio.h"

/*-----------------------------------------------------------------------------
  staticモノ
  -----------------------------------------------------------------------------*/
static u32	run_state;	// 実行ステート
static u32	pre_run_state;
#ifndef IPL
static u32	dbs2_run_state;
#endif

#ifndef JHOSTIO
static SplashState ss;
#endif

static SplashState* ssp;

static MtxPtr currentCamera;

static BOOL bgm_first = TRUE;

/*-----------------------------------------------------------------------------
  内部計算更新
  -----------------------------------------------------------------------------*/
void SplashUpdate( void )//Mtx camera )
{
	run_state = pre_run_state;
	
	switch ( run_state )
	{
	case cSpRun_Logo:	// デモがFadeinするとき。(sram crash時)
		pre_run_state = updateSplashFadeinLogo();
		inSplashUpdateMtx();
		break;
	case cSpRun_Init:	// 始めての起動。(invalid counterBias時)
		pre_run_state = updateSplashFirstBoot();
		inSplashUpdateMtx();
		break;
	case cSpRun_Rotate:	// 通常のロゴデモ。
		pre_run_state = inSplashUpdate();
		inSplashUpdateMtx();
		break;
	case cSpRun_LogotoMenu:	// 強制メニュー。
		pre_run_state = updateSplashLogoToMenu();
		inSplashUpdateMtx();
		break;
	case cMenu_Init:
		pre_run_state = inSplashToMenuUpdate();
		bgm_first = TRUE;
		break;
	case cMenu_Select:
		if ( bgm_first ) {
			Jac_PlayBgm( JAC_BGM_MENU );
			bgm_first = FALSE;
		}
		pre_run_state = inMenuUpdate();
		break;
	case cMenu_Exit:
		break;
	}
}

/*-----------------------------------------------------------------------------
  描画
  -----------------------------------------------------------------------------*/
void SplashDraw( void )
{
	switch ( run_state )
	{
	case cSpRun_Rotate:
	case cSpRun_LogotoMenu:
		inSplashDraw();
		break;
	case cSpRun_Init:
		inSplashDrawFirstBoot();
		break;
	case cSpRun_Logo:
		inSplashDrawSramCrash();
		break;

	case cMenu_Init:
		inSplashToMenuDraw();
		break;
	case cMenu_Select:
		inMenuDraw();
		break;
	case cMenu_Exit:
		break;
	}

}

void SplashDrawDone( void )
{
//	run_state = pre_run_state;
}

void
setSplashRunstate( u32 state )
{
#ifndef IPL
	dbs2_run_state = state;
#else
#pragma unused ( state )
#endif
}

u32
getSplashRunstate( void )
{
	return run_state;
}



/*-----------------------------------------------------------------------------
  初期化
  -----------------------------------------------------------------------------*/
static void
inSplashGetInitRunstate( void )
{
#ifdef IPL
	gTickBs2();
	
	if ( gGetInvalidSram() ) {
		run_state = cSpRun_Logo;
	}
	else if ( gGetInvalidCounterBias() ) {
		run_state = cSpRun_Init;
	}
	else if ( gGetBs2State() == BS2_COVER_OPEN ||
			  gGetForceMenu() ) {
		run_state = cSpRun_LogotoMenu;
	}
	else {
		run_state = cSpRun_Rotate;
	}
#else
	// DBS2.cで設定することに変更。
	run_state = dbs2_run_state;
#endif
}

void
SplashInit( BOOL first )
{
	MainState*	mesp = getMainState();
	inSplashGetInitRunstate();

	pre_run_state = run_state;

	mesp->ortho = 1;

	if ( first ) {
#ifndef JHOSTIO
		ssp = &ss;
		inSplashStateInit( ssp );
#else
		ssp = getIHISplashState();
#endif
	}
	
	inSplashStateReset( ssp );

	if ( first ) {
		inSplashDrawInit();
	}

	// 以下は、それぞれの初期化。
	LogoInit( first );
	MenuInit( first );

	// メニュー各面のパラメータ用の初期化。
	initStart( first );
	initCard( first );
	initOption( first );
	initTime( first );

//	DIReport("SplashInit Done\n");
}

/*-----------------------------------------------------------------------------
  SplashStateの初期化
  -----------------------------------------------------------------------------*/
void
inSplashStateInit( SplashState* p )
{
	// =====================================
	// JHostIOパラメータ
	p->pos 		= 0;
	p->radius 	= 100;
	p->max_v 	= 5000;
	p->logo_rad_rot 	= 0.7f;
	p->logo_change_menu_cube_frame = 30;
	p->anim_min_scale 	= 0.5f;
	p->anim_max_scale_y = 1.0f;
	p->rot_aa = 1.5f;
	p->rot_min_v = 100.0f;
	p->large_cube_alpha = 255;
	p->small_cube_alpha = 0xff;

	// logodemo
	p->bound_z_amp_atten = 0.93f;
	p->bound_y_ave_z = 0.015f;
	p->bound_z_amp_max = 4000;
	p->bound_x_amp_atten = 0.925f;
	p->bound_y_ave_x = 0.005f;
	p->bound_x_amp_max = 12000;
	p->bound_amp_rot_delta = 1000;

	// 一回目の起動
	p->firstboot_fade_max_ninlogo = 30;
	p->firstboot_fade_max_a = 30;
	p->firstboot_fade_max_b = 30;

	//sramcrash
	p->sramcrash_fade_max = 30;
	p->sramcrash_window_fade_max = 30;
	p->sramcrash_window_mergin = 8;
	p->sramcrash_fade_alpha = 150;

	// error表示。
	p->boot_nodisk_fade_max = 30;
	p->boot_fatal_fade_max = 30;
	// =====================================
	// キーフレーム補間 ( JHostIOパラメータ )
	p->key_start_amp_frame = 15;
	p->key_amp_frame = 80;
	p->key_stop_frame = 50;
	p->key_rot_stop = 5000;
	p->key_rot_num = 3;
	p->key_rot_start_v = p->max_v;
	p->key_start_surface_frame = 100;
	// =====================================

	p->logo_angle_x = 8192;
	p->logo_angle_y = -6420;
	p->logo_angle_z = -5461;
	p->force_fatalerror = FALSE;

	p->morphcube_sound_speed = 0.4f;
	p->morphcube_sound_speed_end = 0.5f;
}

void
inSplashStateReset( SplashState* p )
{
	// =====================================
	// セットしなおしても大丈夫なパラメータ。
	p->force_cubemenu = FALSE;
	p->scale = 1.0f;

	p->rot_y = 0;
	p->rot_v = 0.f;
	p->rot_a = 0.f;
	p->logo_interact_start_slope = 0.f;
	p->logo_interact_start_y = 0;
	p->logo_interact_prev_y = 0;
	
	ssp->key_now_frame = 1;
	ssp->key_first = TRUE;
	
	p->start_y = 0;

	p->spring_frame = 60;

	p->logo_rot = 2000;
	p->logo_amp = 0;
	p->logo_rad_rot = 0.f;
	
	p->key_scale_xz_slope = 0.1f;
	p->key_now_scale_xz = p->anim_min_scale;

	p->menu_cube_alpha = 0xff;

	p->logo_mark_alpha = 0;
	p->bound_z_amp = 0;
	p->bound_x_amp = 0;
	p->bound_rot = 0;
	// ================================
	// firstboot関連
	p->firstboot_fade_ninlogo = p->firstboot_fade_max_ninlogo;
	p->firstboot_fade_a = 0;
	p->firstboot_fade_b = 0;
	p->firstboot_state = 0;
	//=================================
	// sram crash関連
	p->sramcrash_fade = 0;
	p->sramcrash_state = 0;
	p->sramcrash_window_fade = 0;
	p->sramcrash_cursor = 1; // いいえ
	//=================================
	// ディスクが正しくありません。
	p->boot_nodisk_fade = 0;
	p->boot_fatal_fade = 0;

	p->cover_open_wait_frame = 120;

	p->memorycard_check_state = IPL_SP_MEMCHECK_PRE;
	p->memorycard_check_cursor = 1;	// いいえ。
	p->memorycard_check_frame = 0;

	p->memorycard_check_needblocks = 0;
	p->memorycard_check_slot = 0;

	p->clockadjust_check_cursor = 1; // いいえ。
	p->clockadjust_check_state  = IPL_SP_CLOCK_PRE;
	p->clockadjust_check_frame = 0;

	p->pal_first_boot = TRUE;
	p->pal_language_cursor = gGetPalCountry();
	p->pal_first_boot_alpha = 0;
}

/*-----------------------------------------------------------------------------
  SplashStateを返す
  -----------------------------------------------------------------------------*/
SplashState*
inSplashGetState( void )
{
	return ssp;
}
