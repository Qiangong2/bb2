#include <dolphin.h>
#include <string.h>
#include <math.h>
#include "gMath.h"
#include "smallCubeFader.h"
#include "smallCubeMovement.h"
#include "smallCubeFaderData.h"
#include "MenuUpdate.h"

/*---------------------------------------------------------------------------*
  もともとの位置との差分がここに入る。
 *---------------------------------------------------------------------------*/
J3DTransformInfo sMovementPos[SCF_CUBENUM];

static u32 sDancingFrame;
static u32 sSwingingFrame;

static f32 getRoundTripAnim( f32 turnframe, f32 framenow, f32 v0, f32 v1 );
static void getDancingCubeMovement( f32 frame, f32 *tx, f32 *ty, u16 *ry, u16 *rz );
static void getSwingingCubeMovement( f32 frame, f32 *tx, f32 *ty );

static void updateCubeMovement(void);


#define SWINGFRAMENUM 100


/*---------------------------------------------------------------------------*
  Name:
  void  scfInitCubeMovement(void)

  Description:
  面が変わるごとに呼ばれる、初期化関数

 *---------------------------------------------------------------------------*/
void
scfInitCubeMovement(void)
{
	sDancingFrame = 0;
	sSwingingFrame = 0;
	memset( sMovementPos, 0, sizeof(sMovementPos) );

}



/*---------------------------------------------------------------------------*
  Name:
  scfUpdateStartCubeMovement

  Description:
  スタート面のゆらゆらの動きを、sMovementPosに設定。


 *---------------------------------------------------------------------------*/
void
scfUpdateStartCubeMovement( J3DTransformInfo *cubepos, u32 cubenum )
{
	f32 tx, ty;
	u16 ry, rz;
	Vec center;
	J3DTransformInfo diff;
	int i;

	getDancingCubeMovement( (f32)sDancingFrame/getMenuState()->scf_dance_framenum,
				   &tx, &ty, &ry, &rz );

	center = scfGetCenterPos( cubepos, NULL, cubenum );
	for( i=0;i<cubenum;i++ ){
	cubepos[i].tx += tx;
	cubepos[i].ty += ty;
	scfRotateAround( center.x, center.y, center.z,
			 0, ry, rz,
			 &cubepos[i], &diff );
	sMovementPos[i].tx = diff.tx;
	sMovementPos[i].ty = diff.ty;
	sMovementPos[i].tz = diff.tz;
	sMovementPos[i].rx = diff.rx;
	sMovementPos[i].ry = diff.ry;
	sMovementPos[i].rz = diff.rz;

	}


	updateCubeMovement();
}


/*---------------------------------------------------------------------------*
  Name:
  scfUpdateOptionCubeMovement( J3DTransformInfo *cubepos )

  Description:
  オプション面の踊るような動きを作る

 *---------------------------------------------------------------------------*/
void
scfUpdateOptionCubeMovement( J3DTransformInfo *cubepos )
{
	f32 tx, ty;
	u16 ry, rz;
	u16* data;
	Vec center;
	int i;

	getDancingCubeMovement( (f32)sDancingFrame/getMenuState()->scf_dance_framenum,
							&tx, &ty, &ry, &rz );


	data = scfGetARROWIndex();
	center = scfGetCenterPos( cubepos, data, scfGetARROWNum() );
	for( i=0;i<scfGetARROWNum();i++ ){
		scfRotateAround( center.x, center.y, center.z,
						 0, ry, rz,
						 &cubepos[data[i]], &sMovementPos[data[i]] );
		sMovementPos[data[i]].tx += tx;
		sMovementPos[data[i]].ty += ty;
	}

	data = scfGetOFFSET1STIndex();
	center = scfGetCenterPos( cubepos, data, scfGetOFFSET1STNum() );
	for( i=0;i<scfGetOFFSET1STNum();i++ ){
		scfRotateAround( center.x, center.y, center.z,
						 0, ry, rz,
						 &cubepos[data[i]], &sMovementPos[data[i]] );
		sMovementPos[data[i]].tx += tx;
		sMovementPos[data[i]].ty += ty;
	}

	data = scfGetOFFSET2NDIndex();
	center = scfGetCenterPos( cubepos, data, scfGetOFFSET2NDNum() );
	for( i=0;i<scfGetOFFSET2NDNum();i++ ){
		scfRotateAround( center.x, center.y, center.z,
						 0, ry, rz,
						 &cubepos[data[i]], &sMovementPos[data[i]] );
		sMovementPos[data[i]].tx += tx;
		sMovementPos[data[i]].ty += ty;
	}

	data = scfGetSOUNDIndex();
	center = scfGetCenterPos( cubepos, data, scfGetSOUNDNum() );
	for( i=0;i<scfGetSOUNDNum();i++ ){
		scfRotateAround( center.x, center.y, center.z,
						 0, ry, rz,
						 &cubepos[data[i]], &sMovementPos[data[i]] );
		sMovementPos[data[i]].tx += tx;
		sMovementPos[data[i]].ty += ty;
	}

	data = scfGetLANGUAGEIndex();
	center = scfGetCenterPos( cubepos, data, scfGetLANGUAGENum() );
	for( i=0;i<scfGetLANGUAGENum();i++ ){
		scfRotateAround( center.x, center.y, center.z,
						 0, ry, rz,
						 &cubepos[data[i]], &sMovementPos[data[i]] );
		sMovementPos[data[i]].tx += tx;
		sMovementPos[data[i]].ty += ty;
	}

	updateCubeMovement();
}


/*---------------------------------------------------------------------------*
  Name:
  scfUpdateTimeCubeMovement( J3DTransformInfo *cubepos )

  Description:
  カレンダー面の踊る動きを設定

 *---------------------------------------------------------------------------*/
void
scfUpdateTimeCubeMovement( J3DTransformInfo *cubepos )
{
	f32 tx, ty;
	u16 ry, rz;
	u16* data;
	Vec center;
	int i;

	getDancingCubeMovement( (f32)sDancingFrame/getMenuState()->scf_dance_framenum,
				&tx, &ty, &ry, &rz );

	data = scfGetCOLON1Index();
	center = scfGetCenterPos( cubepos, data, scfGetCOLON1Num() );
	for( i=0;i<scfGetCOLON1Num();i++ ){
	scfRotateAround( center.x, center.y, center.z,
			 0, ry, rz,
			 &cubepos[data[i]], &sMovementPos[data[i]] );
	sMovementPos[data[i]].tx += tx;
	sMovementPos[data[i]].ty += ty;
	}
	data = scfGetCOLON2Index();
	center = scfGetCenterPos( cubepos, data, scfGetCOLON2Num() );
	for( i=0;i<scfGetCOLON2Num();i++ ){
	scfRotateAround( center.x, center.y, center.z,
			 0, ry, rz,
			 &cubepos[data[i]], &sMovementPos[data[i]] );
	sMovementPos[data[i]].tx += tx;
	sMovementPos[data[i]].ty += ty;
	}
	data = scfGetCOLON3Index();
	center = scfGetCenterPos( cubepos, data, scfGetCOLON3Num() );
	for( i=0;i<scfGetCOLON3Num();i++ ){
	scfRotateAround( center.x, center.y, center.z,
			 0, ry, rz,
			 &cubepos[data[i]], &sMovementPos[data[i]] );
	sMovementPos[data[i]].tx += tx;
	sMovementPos[data[i]].ty += ty;
	}
	data = scfGetCOLON4Index();
	center = scfGetCenterPos( cubepos, data, scfGetCOLON4Num() );
	for( i=0;i<scfGetCOLON4Num();i++ ){
	scfRotateAround( center.x, center.y, center.z,
			 0, ry, rz,
			 &cubepos[data[i]], &sMovementPos[data[i]] );
	sMovementPos[data[i]].tx += tx;
	sMovementPos[data[i]].ty += ty;
	}
	data = scfGetYEAR1STIndex();
	center = scfGetCenterPos( cubepos, data, scfGetYEAR1STNum() );
	for( i=0;i<scfGetYEAR1STNum();i++ ){
	scfRotateAround( center.x, center.y, center.z,
			 0, ry, rz,
			 &cubepos[data[i]], &sMovementPos[data[i]] );
	sMovementPos[data[i]].tx += tx;
	sMovementPos[data[i]].ty += ty;
	}
	data = scfGetYEAR2NDIndex();
	center = scfGetCenterPos( cubepos, data, scfGetYEAR2NDNum() );
	for( i=0;i<scfGetYEAR2NDNum();i++ ){
	scfRotateAround( center.x, center.y, center.z,
			 0, ry, rz,
			 &cubepos[data[i]], &sMovementPos[data[i]] );
	sMovementPos[data[i]].tx += tx;
	sMovementPos[data[i]].ty += ty;
	}
	data = scfGetYEAR3RDIndex();
	center = scfGetCenterPos( cubepos, data, scfGetYEAR3RDNum() );
	for( i=0;i<scfGetYEAR3RDNum();i++ ){
	scfRotateAround( center.x, center.y, center.z,
			 0, ry, rz,
			 &cubepos[data[i]], &sMovementPos[data[i]] );
	sMovementPos[data[i]].tx += tx;
	sMovementPos[data[i]].ty += ty;
	}
	data = scfGetYEAR4THIndex();
	center = scfGetCenterPos( cubepos, data, scfGetYEAR4THNum() );
	for( i=0;i<scfGetYEAR4THNum();i++ ){
	scfRotateAround( center.x, center.y, center.z,
			 0, ry, rz,
			 &cubepos[data[i]], &sMovementPos[data[i]] );
	sMovementPos[data[i]].tx += tx;
	sMovementPos[data[i]].ty += ty;
	}
	data = scfGetMONTH1STIndex();
	center = scfGetCenterPos( cubepos, data, scfGetMONTH1STNum() );
	for( i=0;i<scfGetMONTH1STNum();i++ ){
	scfRotateAround( center.x, center.y, center.z,
			 0, ry, rz,
			 &cubepos[data[i]], &sMovementPos[data[i]] );
	sMovementPos[data[i]].tx += tx;
	sMovementPos[data[i]].ty += ty;
	}
	data = scfGetMONTH2NDIndex();
	center = scfGetCenterPos( cubepos, data, scfGetMONTH2NDNum() );
	for( i=0;i<scfGetMONTH2NDNum();i++ ){
	scfRotateAround( center.x, center.y, center.z,
			 0, ry, rz,
			 &cubepos[data[i]], &sMovementPos[data[i]] );
	sMovementPos[data[i]].tx += tx;
	sMovementPos[data[i]].ty += ty;
	}
	data = scfGetDAY1STIndex();
	center = scfGetCenterPos( cubepos, data, scfGetDAY1STNum() );
	for( i=0;i<scfGetDAY1STNum();i++ ){
	scfRotateAround( center.x, center.y, center.z,
			 0, ry, rz,
			 &cubepos[data[i]], &sMovementPos[data[i]] );
	sMovementPos[data[i]].tx += tx;
	sMovementPos[data[i]].ty += ty;
	}
	data = scfGetDAY2NDIndex();
	center = scfGetCenterPos( cubepos, data, scfGetDAY2NDNum() );
	for( i=0;i<scfGetDAY2NDNum();i++ ){
	scfRotateAround( center.x, center.y, center.z,
			 0, ry, rz,
			 &cubepos[data[i]], &sMovementPos[data[i]] );
	sMovementPos[data[i]].tx += tx;
	sMovementPos[data[i]].ty += ty;
	}
	data = scfGetHOUR1STIndex();
	center = scfGetCenterPos( cubepos, data, scfGetHOUR1STNum() );
	for( i=0;i<scfGetHOUR1STNum();i++ ){
	scfRotateAround( center.x, center.y, center.z,
			 0, ry, rz,
			 &cubepos[data[i]], &sMovementPos[data[i]] );
	sMovementPos[data[i]].tx += tx;
	sMovementPos[data[i]].ty += ty;
	}
	data = scfGetHOUR2NDIndex();
	center = scfGetCenterPos( cubepos, data, scfGetHOUR2NDNum() );
	for( i=0;i<scfGetHOUR2NDNum();i++ ){
	scfRotateAround( center.x, center.y, center.z,
			 0, ry, rz,
			 &cubepos[data[i]], &sMovementPos[data[i]] );
	sMovementPos[data[i]].tx += tx;
	sMovementPos[data[i]].ty += ty;
	}
	data = scfGetMINUTE1STIndex();
	center = scfGetCenterPos( cubepos, data, scfGetMINUTE1STNum() );
	for( i=0;i<scfGetMINUTE1STNum();i++ ){
	scfRotateAround( center.x, center.y, center.z,
			 0, ry, rz,
			 &cubepos[data[i]], &sMovementPos[data[i]] );
	sMovementPos[data[i]].tx += tx;
	sMovementPos[data[i]].ty += ty;
	}
	data = scfGetMINUTE2NDIndex();
	center = scfGetCenterPos( cubepos, data, scfGetMINUTE2NDNum() );
	for( i=0;i<scfGetMINUTE2NDNum();i++ ){
	scfRotateAround( center.x, center.y, center.z,
			 0, ry, rz,
			 &cubepos[data[i]], &sMovementPos[data[i]] );
	sMovementPos[data[i]].tx += tx;
	sMovementPos[data[i]].ty += ty;
	}
	data = scfGetSECOND1STIndex();
	center = scfGetCenterPos( cubepos, data, scfGetSECOND1STNum() );
	for( i=0;i<scfGetSECOND1STNum();i++ ){
	scfRotateAround( center.x, center.y, center.z,
			 0, ry, rz,
			 &cubepos[data[i]], &sMovementPos[data[i]] );
	sMovementPos[data[i]].tx += tx;
	sMovementPos[data[i]].ty += ty;
	}
	data = scfGetSECOND2NDIndex();
	center = scfGetCenterPos( cubepos, data, scfGetSECOND2NDNum() );
	for( i=0;i<scfGetSECOND2NDNum();i++ ){
	scfRotateAround( center.x, center.y, center.z,
			 0, ry, rz,
			 &cubepos[data[i]], &sMovementPos[data[i]] );
	sMovementPos[data[i]].tx += tx;
	sMovementPos[data[i]].ty += ty;
	}

	updateCubeMovement();
}

J3DTransformInfo*
scfGetMovementOffsetPos(void)
{
	return sMovementPos;
}




static void
updateCubeMovement(void)
{
	sDancingFrame++;
	sSwingingFrame++;

	if( sDancingFrame >= getMenuState()->scf_dance_framenum ){
	sDancingFrame = 0;
	}
	if( sSwingingFrame >= SWINGFRAMENUM ){
	sSwingingFrame = 0;
	}

}


static void
getDancingCubeMovement( f32 frame, f32 *tx, f32 *ty, u16 *ry, u16 *rz )
{
	f32 txframe, tyframe, rframe;

	txframe = fmod( frame+getMenuState()->scf_dance_txturnpoint/2, 1.0f );
	tyframe = fmod( fmod( frame, 1.0f )*2+0.25f, 1.0f );
	rframe = fmod( frame + 0.25f, 1.0f );

	*tx = getRoundTripAnim( getMenuState()->scf_dance_txturnpoint,txframe,
				-getMenuState()->scf_dance_tx,
				getMenuState()->scf_dance_tx );
	*ty = getRoundTripAnim( 0.5f,tyframe,
				-getMenuState()->scf_dance_ty,
				getMenuState()->scf_dance_ty );
	*ry = scfDegToU16Rot( getRoundTripAnim( 0.5f, rframe,
						-getMenuState()->scf_dance_ry,
						getMenuState()->scf_dance_ry) );
	*rz = scfDegToU16Rot( getRoundTripAnim( 0.5f, rframe,
						-getMenuState()->scf_dance_rz,
						getMenuState()->scf_dance_rz ) );

}

static void
getSwingingCubeMovement( f32 frame, f32 *tx, f32 *ty )
{
#define SWINGWIDTH 7
#define SWINGHEIGHT 10

	f32 xframe = fmod( frame*2, 1.0f );

	*tx = getRoundTripAnim( 0.5f, xframe, 0, SWINGWIDTH );
	*ty = getRoundTripAnim( 0.5f, frame, 0, SWINGHEIGHT );
}



/*---------------------------------------------------------------------------*
  Name:
  getRoundTripAnim

  Description:
  二つの値の間をいったりきたりするアニメーションをつくる。
  スロープ値は０で。


 *---------------------------------------------------------------------------*/
static f32
getRoundTripAnim( f32 turnframe, f32 framenow, f32 v0, f32 v1 )
{
	if( framenow < turnframe ){
	return gHermiteInterpolation( framenow,
					 0, v0, 0,
					 turnframe, v1, 0 );
	}else{
	return gHermiteInterpolation( framenow,
					  turnframe, v1, 0,
					  1.0f, v0, 0 );
	}

}

