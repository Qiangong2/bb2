/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: J3DBinary 1.5 Renderer

  Key Frame Animation計算など
  -----------------------------------------------------------------------------*/
#include <dolphin.h>

#include "gMath.h"

#include "gModelRender.h"
#include "gMRanime.h"

/*-----------------------------------------------------------------------------
  staticモノ
  -----------------------------------------------------------------------------*/
static gmrModel* currentModel;
static gmrModelData* currentModelData;

/*-----------------------------------------------------------------------------
  static function
  -----------------------------------------------------------------------------*/
static float gGetKeyFrameInterpF32(float frame, J3DAnmKeyTableBase* keyInfo, float* param);
static float gGetKeyFrameInterpS16(float frame, J3DAnmKeyTableBase* keyInfo, s16* param);

/*-----------------------------------------------------------------------------
  指定フレームの値をキーフレーム情報から補完して返します。
  Cなので、float版。
  -----------------------------------------------------------------------------*/
static float
gGetKeyFrameInterpF32(float frame, J3DAnmKeyTableBase* keyInfo, float* param)
{
	int slope = 4;	// defaultでslopeが違うモード。
	float ret;

	if (keyInfo->flag == 0) {
		slope = 3;  // 左右のスロープ値が同じ
	}

	if (frame < *param) {
		ret = *(param + 1);   // スタート以下
	}
	else if ( frame >= *(param + slope * (keyInfo->keyNum - 1)) ) {
		ret = *(param + slope * (keyInfo->keyNum - 1) +1 );
	}
	else {
		// 間
		// 2分木探索形式
		int searchRange = keyInfo->keyNum;
		while (searchRange > 1) {
			int divideNum = searchRange / 2;
			if (frame >= *(param+divideNum*slope )) {
				param += divideNum*slope;
				searchRange = searchRange - divideNum;
			} else {
				searchRange = divideNum;
			}
		}
		// 発見
		ret = gHermiteInterpolation(frame,
									(float)*param,         (float)*(param+1),       (float)*(param+slope-1),
									(float)*(param+slope), (float)*(param+slope+1), (float)*(param+slope+2));
	}
	return ret;
}


/*-----------------------------------------------------------------------------
  指定フレームの値をキーフレーム情報から補完して返します。
  Cなので、float版。
  -----------------------------------------------------------------------------*/
static float
gGetColorKeyFrameInterpF32(float frame, J3DAnmKeyTableBase* keyInfo, float* param)
{
	int slope = 4;	// defaultでslopeが違うモード。
	float ret, f_base;

	if (keyInfo->flag == 0) {
		slope = 3;  // 左右のスロープ値が同じ
	}

	if (frame < *param) {
		ret = *(param + 1);   // スタート以下
	}
	else if ( frame >= *(param + slope * (keyInfo->keyNum - 1)) ) {
		ret = *(param + slope * (keyInfo->keyNum - 1) +1 );
	}
	else {
		// 間
		// 2分木探索形式
		int searchRange = keyInfo->keyNum;
		while (searchRange > 1) {
			int divideNum = searchRange / 2;
			if (frame >= *(param+divideNum*slope )) {
				param += divideNum*slope;
				searchRange = searchRange - divideNum;
			} else {
				searchRange = divideNum;
			}
		}
		// 発見

		f_base = 1.f / 30.f;
		ret = gHermiteInterpolation(frame * f_base,
									(float)*param * f_base , (float)*(param+1),       (float)*(param+slope-1),
									(float)*(param + slope) * f_base, (float)*(param+slope+1), (float)*(param+slope+2));
	}
	return ret;
}

/*-----------------------------------------------------------------------------
  指定フレームの値をキーフレーム情報から補完して返します。
  Cなので、s16版。
  -----------------------------------------------------------------------------*/
static float
gGetKeyFrameInterpS16(float frame, J3DAnmKeyTableBase* keyInfo, s16* param)
{
	int slope = 4;	// defaultでslopeが違うモード。
	float ret;

	if (keyInfo->flag == 0) {
		slope = 3;  // 左右のスロープ値が同じ
	}

	if (frame < *param) {
		ret =  *(param + 1);   // スタート以下
	}
	else if ( frame >= *(param + slope * (keyInfo->keyNum - 1)) ) {
		ret = *(param + slope * (keyInfo->keyNum - 1) +1 );
	}
	else {
		// 間
		// 2分木探索形式
		int searchRange = keyInfo->keyNum;
		while (searchRange > 1) {
			int divideNum = searchRange / 2;
			if (frame >= *(param+divideNum*slope )) {
				param += divideNum*slope;
				searchRange = searchRange-divideNum;
			} else {
				searchRange = divideNum;
			}
		}
		// 発見
		ret =  gHermiteInterpolation(frame,
									 (float)*param,         (float)*(param+1),       (float)*(param+slope-1),
									 (float)*(param+slope), (float)*(param+slope+1), (float)*(param+slope+2));
	}
	return ret;
}


/*-----------------------------------------------------------------------------
  カレントモデルをセットします。
  -----------------------------------------------------------------------------*/
void
setCurrentAnimeModel( gmrModel* data )
{
	currentModel = data;
	currentModelData = data->gmrmd;
}

/*-----------------------------------------------------------------------------
  指し示すフレームのtransformInfoを返します。
  -----------------------------------------------------------------------------*/
void
getTransform(
	f32 frame,
	u16 jntNo,
	J3DTransformInfo* result )
{
	int i;
	float st[3];
	s16   r[3];
	J3DAnmTransformKeyTable* mAnmTable = currentModel->anime->anmTable;

	u8   mShift       = currentModel->anime->shift;
	f32* mScaling     = currentModel->anime->scale;
	s16* mRotation    = currentModel->anime->rotate;
	f32* mTranslation = currentModel->anime->translate;

	// scale
	for ( i=0 ; i<3 ; i++ ) {
		switch(mAnmTable[ jntNo*3 + i ].scl.keyNum) {
		case 0:	st[i] = 1.f;	break;
		case 1:	st[i] = mScaling[mAnmTable[ jntNo * 3 + i ].scl.offset];	break;
		default:st[i] = gGetKeyFrameInterpF32(frame,
											  &mAnmTable[ jntNo * 3 + i ].scl,
											  &mScaling[ mAnmTable[ jntNo * 3 + i ].scl.offset ]);
			break;
		}
	}

	result->sx = st[0];
	result->sy = st[1];
	result->sz = st[2];

	// rotate
	for ( i=0 ; i<3 ; i++ ) {
		switch( mAnmTable[ jntNo*3 + i ].rot.keyNum ) {
		case  0: r[i] = 0;	break;
		case  1: r[i] = (s16)( (int)(mRotation[mAnmTable[ jntNo * 3 + i ].rot.offset]) << mShift );		break;
		default: r[i] = (s16)( (int)(gGetKeyFrameInterpS16(frame,
														   &mAnmTable[ jntNo * 3 + i ].rot ,
														   &mRotation[ mAnmTable[ jntNo * 3 + i ].rot.offset ])) << mShift );
			break;
		}
	}

	result->rx = r[0];
	result->ry = r[1];
	result->rz = r[2];

	// trans
	for ( i=0 ; i<3 ; i++ ) {
		switch(mAnmTable[ jntNo*3 + i ].trs.keyNum) {
		case 0:	st[i] = 0.f;	break;
		case 1:	st[i] = mTranslation[mAnmTable[ jntNo * 3 + i ].trs.offset];	break;
		default:st[i] = gGetKeyFrameInterpF32(frame,
											  &mAnmTable[ jntNo * 3 + i ].trs,
											  &mTranslation[ mAnmTable[ jntNo * 3 + i ].trs.offset ]);
			break;
		}
	}

	result->tx = st[0];
	result->ty = st[1];
	result->tz = st[2];

}

/*-----------------------------------------------------------------------------
  指し示すフレームのマテリアルカラーを返します。
  -----------------------------------------------------------------------------*/
void
getMatColor(
	f32		frame,
	u16		matNo,
	GXColor*	color )
{
	f32		r, g, b, a;
	gMRPolyAnimeKeyTable*	table = currentModel->c_anime->table;
	s16*		table_index = currentModel->c_anime->poly_anime_index;

	f32*	red   = currentModel->c_anime->r;
	f32*	green = currentModel->c_anime->g;
	f32*	blue  = currentModel->c_anime->b;
	f32*	alpha = currentModel->c_anime->a;

	frame /= 30.f;

	// [ R ]
	switch ( table[ table_index[ matNo ] ].r.keyNum ) {
	case 0:	 r = 0.f;	break;
	case 1:  r = red[ table[ table_index[ matNo ] ].r.offset ];	break;
	default: r = gGetKeyFrameInterpF32( frame,
										&table[ table_index[ matNo ] ].r ,
										&red[ table[ table_index[ matNo ] ].r.offset ] );
		break;
	}
	// [ G ]
	switch ( table[ table_index[ matNo ] ].g.keyNum ) {
	case 0:	 g = 0.f;	break;
	case 1:  g = green[ table[ table_index[ matNo ] ].g.offset ];	break;
	default: g = gGetKeyFrameInterpF32( frame,
										&table[ table_index[ matNo ] ].g ,
										&green[ table[ table_index[ matNo ] ].g.offset ] );
		break;
	}
	// [ B ]
	switch ( table[ table_index[ matNo ] ].b.keyNum ) {
	case 0:	 b = 0.f;	break;
	case 1:  b = blue[ table[ table_index[ matNo ] ].b.offset ];	break;
	default: b = gGetKeyFrameInterpF32( frame,
										&table[ table_index[ matNo ] ].b ,
										&blue[ table[ table_index[ matNo ] ].b.offset ] );
		break;
	}
	// [ A ]
	switch ( table[ table_index[ matNo ] ].a.keyNum ) {
	case 0:	 a = 0.f;	break;
	case 1:  a = alpha[ table[ table_index[ matNo ] ].a.offset ];	break;
	default: a = gGetKeyFrameInterpF32( frame,
										&table[ table_index[ matNo ] ].a ,
										&alpha[ table[ table_index[ matNo ] ].a.offset ] );
		break;
	}

	color->r = (u8)( ( r < 0 )? 0:( ( r > 255 )?255: (u8)r ) );
	color->g = (u8)( ( g < 0 )? 0:( ( g > 255 )?255: (u8)g ) );
	color->b = (u8)( ( b < 0 )? 0:( ( b > 255 )?255: (u8)b ) );
	color->a = (u8)( ( a < 0 )? 0:( ( a > 255 )?255: (u8)a ) );

	/*
	switch (matNo)
	{
	case 8:
	case 17:
	case 18:
	case 19:
	case 20:
		DIReport("r:%d\n", (u8)r );
		DIReport("g:%d\n", (u8)g );
		DIReport("b:%d\n", (u8)b );
		DIReport("%02d- a:%d:%d\n", matNo,(u8)a, color->a );
		if ( table[ table_index[ matNo ] ].a.flag == 0 ) {
			DIReport("[keyinfo]----------%f,%f,%f\n",
					 alpha[ table[ table_index[ matNo ] ].a.offset ],
					 alpha[ table[ table_index[ matNo ] ].a.offset+1 ],
					 alpha[ table[ table_index[ matNo ] ].a.offset+2 ]
					 );
		} else if ( table[ table_index[ matNo ] ].a.flag == 1 ) {
			DIReport("[keyinfo]----------%f,%f,%f,%f\n",
					 alpha[ table[ table_index[ matNo ] ].a.offset ],
					 alpha[ table[ table_index[ matNo ] ].a.offset+1 ],
					 alpha[ table[ table_index[ matNo ] ].a.offset+2 ],
					 alpha[ table[ table_index[ matNo ] ].a.offset+3 ]
					 );
		}
		DIReport("%02d- frame:%f:  a:%f a:%d\n",matNo,frame , a, (u8)a );
	}
	  */
}
