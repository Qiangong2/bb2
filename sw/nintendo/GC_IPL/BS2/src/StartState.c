/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: メニューモード時の各面のパラメータ
  -----------------------------------------------------------------------------*/
#include <dolphin.h>

#include "StartState.h"
#include "StartUpdate.h"

#ifdef JHOSTIO
#include "IHIStartState.h"
#endif

/*-----------------------------------------------------------------------------
  ファイルローカル変数
  -----------------------------------------------------------------------------*/
static StartState* stp;

#ifndef JHOSTIO
static StartState st;
#endif

/*-----------------------------------------------------------------------------
  初期化
  -----------------------------------------------------------------------------*/
void
initStart( BOOL first )
{
	if ( first ) {
#ifndef JHOSTIO
		stp = &st;
		initStartState( stp );
#else
		stp = getIHIStartState();
#endif
	}

	initStartMenu( first );
}

/*-----------------------------------------------------------------------------
  パラメータ初期化
  -----------------------------------------------------------------------------*/
void
initStartState( StartState* p )
{
	/*
	  p->param = 0;
	  */
//#ifdef PAL
	p->cube_r = 0;
	p->cube_g = -90;
	p->cube_b = 20;
//#else
//	p->cube_r = 45;
//	p->cube_g = -40;
//	p->cube_b = -25;
//#endif
	p->cube_a = 255;

	p->rad = 75;
	p->scale = 0.65f;

	p->concent_dec_alpha = 10;
	p->concent_dec_pos = 5;
}

/*-----------------------------------------------------------------------------
  パラメータポインタ取得
  -----------------------------------------------------------------------------*/
StartState*
getStartState( void )
{
	return stp;
}

