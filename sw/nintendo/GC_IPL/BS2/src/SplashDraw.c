/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: Splashデモ メインルーチン

  シーケンスを含めたSplashデモのdrawルーチン
  -----------------------------------------------------------------------------*/
#include <dolphin.h>
#include <private/OSRtc.h>
#include <string.h>

#include "gDynamic.h"
#include "gBase.h"
#include "gMath.h"
#include "gTrans.h"
#include "gRand.h"
#include "gCamera.h"
#include "gLoader.h"
#include "gInit.h"
#include "gFont.h"

#include "gMainState.h"
#include "gModelRender.h"
#include "gLayoutRender.h"

#include "SplashMain.h"
#include "SplashUpdate.h"
#include "SplashDraw.h"
#include "SplashtoMenuUpdate.h"
#include "LogoDraw.h"
#include "MenuUpdate.h"
#include "MenuCardAnim.h"
#include "MenuStartAnim.h"
#include "MenuOptionAnim.h"
#include "MenuClockAnim.h"
#include "CardUpdate.h"
#include "CardSequence.h"
#include "StartUpdate.h"
#include "TimeUpdate.h"
#include "OptionUpdate.h"

//#include "2d_demo_2_Japanese.h"
#if defined(MPAL)
#include "mpal_mesg.h"
#elif defined(PAL)
#include "pal_mesg.h"
#else // NTSC
#include "ntsc_mesg.h"
#endif

/*-----------------------------------------------------------------------------
  staticモノ
  -----------------------------------------------------------------------------*/

static gmrModelData cubeData;
static gmrModel		largeCube;
static gmrModelData menuCubeData;
static gmrModel		menuCube;
static gmrModelData logoData;
static gmrModel		logo;
static gmrModelData coverdata;
static gmrModel		cover;

//static gmrModelData	slogo_data;
//static gmrModel		slogo;

static gmrModel			nlogo;
static gmrModelData		nlogo_data;
static gmrAnime			nlogo_anime;
static gmrColorAnime	nlogo_canime;

static gLayoutHeader*	dem_lay;
static gLRStringHeader*	dem_str;

/*-----------------------------------------------------------------------------
  
  -----------------------------------------------------------------------------*/
static void inSplashDrawFade( u8 alpha );
static void	drawVerifyWindow( SplashState* ssp , u16 id , s16 cursor_state , u8 alpha );

/*-----------------------------------------------------------------------------
  描画初期化
  -----------------------------------------------------------------------------*/
void
initSplashDraw_Lang( void )
{
	dem_str = gGetBufferPtr( getID_2D_demo_string() );
}

void
inSplashDrawInit( void )
{
	cubeData.modeldata = gGetBufferPtr( GC_base_cube_szp );
	largeCube.gmrmd = &cubeData;
	buildModel( &largeCube , TRUE );

//	menuCubeData.modeldata = gGetBufferPtr( GC_menu_cube_szp );
	menuCubeData.modeldata = gGetBufferPtr( GC_mcube_szp );
	menuCube.gmrmd = &menuCubeData;
	buildModel( &menuCube , TRUE );
//	dumpTree( menu.gmrmd->root );

	// ロゴ
	logoData.modeldata = gGetBufferPtr( GC_mark_szp );
	logo.gmrmd = &logoData;
	buildModel( &logo , TRUE );

	// カバー
	coverdata.modeldata = gGetBufferPtr( GC_cover_cube_szp );
	cover.gmrmd = &coverdata;
	buildModel( &cover, TRUE );

//	dumpTree( cover.gmrmd->root );

	// 任天堂ロゴ。
	nlogo_data.modeldata = gGetBufferPtr( GC_nin_logo_m_szp );
	nlogo.gmrmd = &nlogo_data;
	buildModel( &nlogo , TRUE );
	nlogo_anime.animedata = gGetBufferPtr( GC_nin_logo_bck );
	buildAnime( &nlogo_anime, TRUE );
	nlogo_canime.animedata= gGetBufferPtr( GC_nin_logo_ipk );
	buildColorAnime( &nlogo, &nlogo_canime, TRUE );
	setAnime( &nlogo, &nlogo_anime );

	dem_lay = gGetBufferPtr( getID_2D_demo_layout() );
//	dem_str = gGetBufferPtr( getID_2D_demo_string() );

	initSplashDraw_Lang();
}

/*-----------------------------------------------------------------------------
  モデルインスタンスを返す。
  -----------------------------------------------------------------------------*/
gmrModel*
inSplashGetLargeCube( void )
{
	return &largeCube;
}

gmrModel*
inSplashGetMenuCube( void )
{
	return &menuCube;
}

gmrModel*
inSplashGetLogo( void )
{
	return &logo;
}

gmrModel*
inSplashGetNinLogo( void )
{
	return &nlogo;
}

gmrModel*
inSplashGetCoverCube( void )
{
	return &cover;
}

/*-----------------------------------------------------------------------------
  描画
  -----------------------------------------------------------------------------*/
void
inSplashDraw( void )
{
	SplashState* ssp = inSplashGetState();
	GXColor		white = {255,255,255,255};
	s32			amp;
	u8			warn_alpha0, warn_alpha1, w_alpha;

	warn_alpha0 = (u8)( 255 - 150 * ssp->memorycard_check_frame / 30 );
	warn_alpha1 = (u8)( 255 - 150 * ssp->clockadjust_check_frame / 30 );

	if ( warn_alpha0 > warn_alpha1 ) w_alpha = warn_alpha1;
	else w_alpha = warn_alpha0;

	amp = ssp->bound_z_amp + ssp->bound_x_amp;// + ssp->logo_amp;
	if ( amp > 3000 ) amp = 3000;

	// 任天堂ロゴ（を最初に書く）
	nlogo.modelAlpha = (u8)( 255 * w_alpha / 255 );
	drawModel( &nlogo );

	drawInit();
	largeCube.modelAlpha = (u8)( ( ssp->large_cube_alpha * amp / 3000 )* w_alpha / 255 );
	if ( largeCube.modelAlpha > 0 ) {
		// 大きいキューブの描画。
		drawModel( &largeCube );
	}
	
	amp = ssp->logo_amp;
	if ( amp > 3000 ) amp = 3000;
	cover.modelAlpha = (u8)( ( ssp->large_cube_alpha * amp / 3000 ) * w_alpha / 255 );
	if ( cover.modelAlpha > 0 ) {
		// 大きいキューブの描画。
		drawModelNode( &cover , &cover.gmrmd->root[2] );
	}
	
	// グリッドの表示。
	lgDraw( (u8)( ( 255 - ssp->logo_mark_alpha ) * w_alpha / 255 ) );

	logo.modelAlpha  = (u8)( ssp->logo_mark_alpha * w_alpha / 255 );
	if ( logo.modelAlpha > 0 ) {
		// ロゴの表示。
		drawModel( &logo );
	}
	
	if ( cover.modelAlpha > 0 ) {
		// 大きいキューブの描画。
		drawModelNode( &cover , &cover.gmrmd->root[5] );
	}

	white.a = (u8)( 255 * ssp->boot_fatal_fade / ssp->boot_fatal_fade_max );
	if ( white.a > 0 ) {
		gSet2DCamera();
		setGXforglrText();
		glrDrawString( MSG_S200, dem_str, dem_lay, &white );
	}
	white.a = (u8)( 255 * ssp->boot_nodisk_fade / ssp->boot_nodisk_fade_max );
	if ( white.a > 0 ) {
		gSet2DCamera();
		setGXforglrText();
		glrDrawString( MSG_S201, dem_str, dem_lay, &white );
	}
}








static void
getCmdWinWidthHeight(
	gLRTextBoxData*	tbp,
	gLRStringData*  sdp,
	u16		id,
	u16		num,
	u16*	width,
	u16*	height,
	u16*	one_height
	)
{
	u16		w, max_w, i; // id
	u8*		string;
	max_w = 0;
	w = 0;
	for ( i = 0 ; i < num ; i ++ ) {
		string = (u8*)&sdp[id+i] + sdp[id+i].string_offset;	// はい
		gfGetWidthHeight( &tbp[ sdp[id+i].textbox_id ], (char*)string, &w, one_height );	//[はい/いいえ]の縦横幅取得。
		if ( max_w < w ) max_w = w;
	}
	*height = (u16)( *one_height + tbp[ sdp[id].textbox_id ].line_space * 16 * ( num - 1 ) ); // 2行分
	*width = max_w;
}

static void
drawCmdWindowsString(
	gLRTextBoxData*	tbp,
	gLRStringData*  sdp,
	u16		id,
	u16		num,
	u16		cmd_y,
	u16		cmd_one_height,
	s16		cursor_state,
	u8		alpha
	)
{
	MainState*	    mesp = getMainState();
	gLRTextBoxData  temp_tbd;
	int i;
	GXColor		white  = {255,255,255,255};
	GXColor		yellow = {240,230,  0,255};

	white.a = alpha;
	yellow.a = alpha;
	
	temp_tbd = tbp[ sdp[ id ].textbox_id ];
	temp_tbd.y = (u16)( cmd_y + mesp->window_mergin * 16 + cmd_one_height / 2 );
	setGXforglrText();
	for ( i = 0 ; i < num ; i++ ) {
		gfSetBackFontGXColor( &yellow, &yellow );
		gfSetFontGXColor( &white, &white );
		if ( cursor_state == i ) {
			gfBritePrint( &temp_tbd, (char*)( (u8*)&sdp[id + i] + sdp[id + i].string_offset ) );
		} else {
			gfPrint( &temp_tbd, (char*)( (u8*)&sdp[id + i] + sdp[id + i].string_offset ) );
		}
		temp_tbd.y += temp_tbd.line_space * 16;
	}
}

#ifdef PAL
static void
drawSelectLanguageCmdWin( SplashState* ssp )
{
	MainState*	    mesp = getMainState();
	u16		cmd_width = 0, cmd_height = 0, cmd_one_height = 0;
	u16		cmd_x, cmd_y;
	gLRTextBoxData*	tbp = (gLRTextBoxData*)( (u8*)dem_lay + dem_lay->textbox_offset );
	gLRStringData*  sdp = (gLRStringData* )( (u8*)dem_str + sizeof(gLRStringHeader) );
	gLRWindowData*  wdp = (gLRWindowData*)( (u32)dem_lay + (u32)dem_lay->window_offset );
	u16		id = MSG_S306;
	GXColor		white = {255,255,255,255};

	white.a = (u8)ssp->pal_first_boot_alpha;

	getCmdWinWidthHeight( tbp, //dem_str,
						  sdp, // dem_lay,
						  id,
						  6,
						  &cmd_width,
						  &cmd_height,
						  &cmd_one_height );

	cmd_width  += mesp->window_mergin * 16 * 2;	// ウィンドウの隙間を取る。
	cmd_height += mesp->window_mergin * 16 * 2;	// ウィンドウの隙間を取る。
	
	cmd_x = (u16)( wdp[ tbp[ sdp[ id ].textbox_id ].windowID ].x - cmd_width / 2  );
	cmd_y = (u16)( wdp[ tbp[ sdp[ id ].textbox_id ].windowID ].y - cmd_height / 2 );
	glrDrawWindowPosID( tbp[ sdp[ id ].textbox_id ].windowID,
						dem_lay,
						&white,
						cmd_x,
						cmd_y,
						cmd_width,
						cmd_height );

	drawCmdWindowsString( tbp,
						  sdp,
						  id,
						  6,
						  cmd_y,
						  cmd_one_height,
						  ssp->pal_language_cursor,
						  (u8)ssp->pal_first_boot_alpha );
}
#endif


/*-----------------------------------------------------------------------------
  描画
  -----------------------------------------------------------------------------*/
void
inSplashDrawFirstBoot( void )
{
	SplashState* ssp = inSplashGetState();
	GXColor		white = {255,255,255,255};
	s32			amp;

#ifdef PAL
	if ( ssp->pal_first_boot || ssp->pal_first_boot_alpha > 0 ) {
		white.a = (u8)ssp->pal_first_boot_alpha;
		gSet2DCamera();
		setGXforglrText();
		glrDrawString( MSG_S300, dem_str, dem_lay, &white );
		glrDrawString( MSG_S301, dem_str, dem_lay, &white );
		glrDrawString( MSG_S302, dem_str, dem_lay, &white );
		glrDrawString( MSG_S303, dem_str, dem_lay, &white );
		glrDrawString( MSG_S304, dem_str, dem_lay, &white );
		glrDrawString( MSG_S305, dem_str, dem_lay, &white );

		drawSelectLanguageCmdWin( ssp );
		return;
	}
#endif

	nlogo.modelAlpha = (u8)( 255 * ssp->firstboot_fade_ninlogo / ssp->firstboot_fade_max_ninlogo );
	drawModel( &nlogo );
	
	amp = ssp->bound_z_amp + ssp->bound_x_amp;// +	ssp->logo_amp;
	if ( amp > 3000 ) amp = 3000;
	largeCube.modelAlpha = (u8)( ssp->large_cube_alpha * amp / 3000 );

	drawInit();
	if ( largeCube.modelAlpha > 0 ) {
		// 大きいキューブの描画。
		drawModel( &largeCube );
	}
	
	amp = ssp->logo_amp;
	if ( amp > 3000 ) amp = 3000;
	cover.modelAlpha = (u8)( ( ssp->large_cube_alpha * amp / 3000 )  );
	if ( cover.modelAlpha > 0 ) {
		// 大きいキューブの描画。
		drawModelNode( &cover , &cover.gmrmd->root[2] );
	}
	
	// グリッドの表示。
	lgDraw( (u8)( 255 - ssp->logo_mark_alpha ) );

	logo.modelAlpha = ssp->logo_mark_alpha;
	if ( logo.modelAlpha > 0 ) {
		// ロゴの表示。
		drawModel( &logo );
	}
	
	if ( cover.modelAlpha > 0 ) {
		// 大きいキューブの描画。
		drawModelNode( &cover , &cover.gmrmd->root[5] );
	}

	gSet2DCamera();
	setGXforglrText();
	white.a = (u8)( 255 * ssp->firstboot_fade_a / ssp->firstboot_fade_max_a );
	if ( white.a > 0 ) glrDrawString( MSG_S100, dem_str, dem_lay, &white );

	white.a = (u8)( 255 * ssp->boot_fatal_fade / ssp->boot_fatal_fade_max );
	if ( white.a > 0 ) glrDrawString( MSG_S200, dem_str, dem_lay, &white );
}


/*-----------------------------------------------------------------------------
  sramクラッシュ時の描画
  -----------------------------------------------------------------------------*/

static void
drawVerifyWindow( SplashState* ssp , u16 id , s16 cursor_state , u8 alpha )
{
	MainState*	    mesp = getMainState();
	u16	total_height;
	u16	mes_width = 0, mes_height = 0;
	u16 cmd_width = 0, cmd_height = 0, cmd_one_height = 0;
	u16	offset_y;
	u16	cmd_x, cmd_y;
	gLRTextBoxData*	tbp = (gLRTextBoxData*)( (u8*)dem_lay + dem_lay->textbox_offset );
	gLRStringData*  sdp = (gLRStringData* )( (u8*)dem_str + sizeof(gLRStringHeader) );
	GXColor		white  = {255,255,255,255};

	white.a = alpha;
	
	gSet2DCamera();
	setGXforglrText();
	
	gfGetWidthHeight( &tbp[ sdp[ id ].textbox_id ],
					  (char*)( (u8*)&sdp[ id ] + sdp[ id ].string_offset ),
					  &mes_width,
					  &mes_height );
	getCmdWinWidthHeight( tbp, //dem_str,
						  sdp, // dem_lay,
						  MSG_S001,
						  2,
						  &cmd_width,
						  &cmd_height,
						  &cmd_one_height );

	mes_height += mesp->window_mergin * 16 * 2;	// ウィンドウの隙間を取る。
	cmd_height += mesp->window_mergin * 16 * 2;	// ウィンドウの隙間を取る。
	total_height = (u16)( mes_height + cmd_height + ssp->sramcrash_window_mergin * 16 );

	offset_y = (u16)( -total_height / 2  + mes_height / 2 );
	// メッセージ文字の表示
	glrDrawStringOffset( id, dem_str, dem_lay, &white, 0, offset_y );

	// コマンドウィンドウの表示
	cmd_x = (u16)( tbp[ sdp[ id ].textbox_id ].x - ( cmd_width / 2 + mesp->window_mergin * 16 ) );
	cmd_y = (u16)( tbp[ sdp[ id ].textbox_id ].y + total_height / 2 - cmd_height );
	glrDrawWindowPosID( tbp[ sdp[ id ].textbox_id ].windowID,
						dem_lay,
						&white,
						cmd_x,
						cmd_y,
						cmd_width  + mesp->window_mergin * 32,
						cmd_height );

	drawCmdWindowsString( tbp,
						  sdp,
						  MSG_S001,
						  2,
						  cmd_y,
						  cmd_one_height,
						  cursor_state,
						  alpha );
}

void
inSplashDrawSramCrash( void )
{
	SplashState* ssp = inSplashGetState();
	GXColor		white = {255,255,255,255};
	s32			amp;

#ifdef PAL
	if ( ssp->pal_first_boot || ssp->pal_first_boot_alpha > 0 ) {
		white.a = (u8)ssp->pal_first_boot_alpha;
		gSet2DCamera();
		setGXforglrText();
		glrDrawString( MSG_S300, dem_str, dem_lay, &white );
		glrDrawString( MSG_S301, dem_str, dem_lay, &white );
		glrDrawString( MSG_S302, dem_str, dem_lay, &white );
		glrDrawString( MSG_S303, dem_str, dem_lay, &white );
		glrDrawString( MSG_S304, dem_str, dem_lay, &white );
		glrDrawString( MSG_S305, dem_str, dem_lay, &white );

		drawSelectLanguageCmdWin( ssp );
		return;
	}
#endif

	amp = ssp->logo_amp;
	if ( amp > 3000 ) amp = 3000;
	cover.modelAlpha = (u8)( ssp->large_cube_alpha * amp / 3000 );
	if ( cover.modelAlpha > 0 ) {
		// 大きいキューブの描画。
		drawModelNode( &cover , &cover.gmrmd->root[2] );
	}
	
	logo.modelAlpha = (s16)( ssp->logo_mark_alpha - ssp->sramcrash_fade_alpha * ssp->sramcrash_fade / ssp->sramcrash_fade_max );
	nlogo.modelAlpha = logo.modelAlpha;
	if ( logo.modelAlpha > 0 ) {
		// ロゴの表示。
		drawModel( &logo );
		drawModel( &nlogo );
	}

	amp = ssp->logo_amp;
	if ( amp > 3000 ) amp = 3000;
	cover.modelAlpha = (u8)( ssp->large_cube_alpha * amp / 3000 );
	if ( cover.modelAlpha > 0 ) {
		// 大きいキューブの描画。
		drawModelNode( &cover , &cover.gmrmd->root[5] );
	}



	drawVerifyWindow( ssp , MSG_S000 , ssp->sramcrash_cursor ,
					  (u8)( 255 * ssp->sramcrash_window_fade / ssp->sramcrash_window_fade_max ) );
	white.a = (u8)( 255 * ssp->boot_fatal_fade / ssp->boot_fatal_fade_max );
	if ( white.a > 0 ) {
		gSet2DCamera();
		setGXforglrText();
		glrDrawString( MSG_S200, dem_str, dem_lay, &white );
	}
}



/*-----------------------------------------------------------------------------
  スプラッシュ->メニューへの移行時の描画
  -----------------------------------------------------------------------------*/
void
inSplashToMenuDraw( void )
{
	SplashState* ssp = inSplashGetState();
	s32		key0, key1;

	if ( ssp->key_now_frame <= ssp->logo_change_menu_cube_frame ) {
		s32			amp;
		amp = ssp->bound_z_amp + ssp->bound_x_amp +	ssp->logo_amp;
		if ( amp > 3000 ) amp = 3000;
		
		largeCube.modelAlpha = (u8)( ssp->large_cube_alpha * amp / 3000 );
		menuCube.modelAlpha = (u8)(ssp->menu_cube_alpha * ssp->key_now_frame / ssp->logo_change_menu_cube_frame);
		nlogo.modelAlpha = (u8)(ssp->menu_cube_alpha * ssp->key_now_frame / ssp->logo_change_menu_cube_frame);
		largeCube.modelAlpha =
				(u8)(ssp->large_cube_alpha * amp
					 * ( ssp->logo_change_menu_cube_frame - ssp->key_now_frame ) / ( ssp->logo_change_menu_cube_frame * 3000) );
		nlogo.modelAlpha = (s16)( largeCube.modelAlpha * ssp->firstboot_fade_ninlogo / ssp->firstboot_fade_max_ninlogo );
		if ( nlogo.modelAlpha < 0 ) nlogo.modelAlpha = 0;
		
		drawModel( &largeCube );
		drawModel( &nlogo );
	}
	else {
		menuCube.modelAlpha = ssp->menu_cube_alpha;//
	}
	if ( menuCube.modelAlpha > 0 ) {
		GXSetChanAmbColor( GX_COLOR0A0, (GXColor){ 40, 40, 40, 255 } );
		drawModel( &menuCube );
		GXSetChanAmbColor( GX_COLOR0A0, (GXColor){ 255,255,255, 255 } );
	}

	key1 = ssp->key_amp_frame + ssp->key_stop_frame + ssp->key_start_amp_frame;
	key0 = key1 - ssp->key_start_surface_frame;
	
	if ( ( ssp->key_now_frame >= key0 ) &&
		 ( ssp->key_now_frame <= key1 )  ) {
		s16		alpha;
		alpha = (s16)( 255 * ( ssp->key_now_frame - key0 )/( key1 - key0 ) );
		drawCenterSurface( getSplashToMenuMtx(), (u8)alpha );
	} else if ( ssp->key_now_frame > ssp->key_amp_frame + ssp->key_stop_frame + ssp->key_start_amp_frame ) {
		drawCenterSurface( getSplashToMenuMtx(), 255 );
	}

}

/*-----------------------------------------------------------------------------
  メニューキューブの描画

  ゆらゆらとゆれているときから、
  各設定画面に入っているときも、このルーチンから処理が振り分けられます。
  -----------------------------------------------------------------------------*/
void
inMenuDraw( void )
{
	SplashState* ssp = inSplashGetState();
	MenuState*	mesp = getMenuState();
	u8			alpha;

	alpha =	(u8)( mesp->fadeout_z_maxalpha +
				  (255 - mesp->fadeout_z_maxalpha ) * (32767 - mesp->fadeout_z_param ) / 32767 );
	menuCube.modelAlpha = alpha;
	
	// draw menu inside
	if ( menuCube.modelAlpha > 0 ) {
		GXSetChanAmbColor( GX_COLOR0A0, (GXColor){ 40, 40, 40, 255 } );
		drawModelNode( &menuCube, &menuCube.gmrmd->root[5] );
		GXSetChanAmbColor( GX_COLOR0A0, (GXColor){ 255,255,255, 255 } );
	}
	// draw menu inside object
	drawCCAnime();
	drawSCAnime();
	drawOCAnime();
	drawOLAnime();
	// draw menu outside
	if ( menuCube.modelAlpha > 0 ) {
		GXSetChanAmbColor( GX_COLOR0A0, (GXColor){ 40, 40, 40, 255 } );
		drawModelNode( &menuCube, &menuCube.gmrmd->root[15] );
		GXSetChanAmbColor( GX_COLOR0A0, (GXColor){ 255,255,255, 255 } );
	}
	// 表面に書いたり、その他諸々。
	drawMenuMain( alpha );
}
