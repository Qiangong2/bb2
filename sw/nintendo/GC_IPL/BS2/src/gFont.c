/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: Font関連ルーチン
  -----------------------------------------------------------------------------*/
#include <dolphin.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <private/OSRtc.h>

#include "gBase.h"
#include "gInit.h"
#include "gMainState.h"

#include "gLoader.h"

#include "gLRStruct.h"
#include "gFont.h"

#ifndef IPL
#include "font.h"
#include "font_e.h"
#endif

extern int	vsnprintf(char* s, size_t n, char * format, va_list arg);

/*-----------------------------------------------------------------------------
  statics
  -----------------------------------------------------------------------------*/
static FontHeader sHeader;
static void*	sSheetImage = NULL;
static u8*		sSizeTable  = NULL;
static int 		sCharsInSheet, sLoadingSheet;
static GXTexObj sTexObj;
static u32		sColorU, sColorD;
static u32		sBackColorU, sBackColorD;
static int		sRenderMode;

/*-----------------------------------------------------------------------------
  static prototype
  -----------------------------------------------------------------------------*/
static void	expandFontSheet( u8* src, u8* dst );
static void drawFontSJIS( u16 x, u16 y,int size , int fontCode );
static int	getFontCode( int charCode );
static int	convertSjis( int sjisCode, u16* table );
static int	parse( int x, int y, int size ,u8* string );
static int	parseDraw( gLRTextBoxData* tdp, u8* string, u16* widths, int height, int string_num_max, int max_line , BOOL line_feed , BOOL oneline );

/*-----------------------------------------------------------------------------
  sliencのメイン展開ルーチン。by yoshimotoさん
  -----------------------------------------------------------------------------*/
static void
decode(u8 *s,u8 *d)
{
	int i,j,k,p,q,r7,r25,cnt,os;
	unsigned flag,code;
	os=(s[4]<<24)|(s[5]<<16)|(s[6]<<8)|s[7];
	r7=(s[8]<<24)|(s[9]<<16)|(s[10]<<8)|s[11];
	r25=(s[12]<<24)|(s[13]<<16)|(s[14]<<8)|s[15];
	q=0;flag=0;p=16;
	do{
		if(flag==0){
			code=(unsigned)(s[p]<<24)|(s[p+1]<<16)|(s[p+2]<<8)|s[p+3];p+=4;
			flag=32;
		}
		if(code&0x80000000)d[q++]=s[r25++];
		else{
			j=(s[r7]<<8)|s[r7+1];r7+=2;
			k=q-(j&0xFFF);cnt=j>>12;
			if(cnt==0)cnt=s[r25++]+18;
			else cnt+=2;
			for(i=0;i<cnt;i++,q++,k++)d[q]=d[k-1];
		}
		code<<=1;flag--;
	}while(q<os);
}

/*-----------------------------------------------------------------------------
  圧縮データかどうかのチェック
  戻り値：展開後のサイズ
  -----------------------------------------------------------------------------*/
static int
checkCompressed( u8* buf )
{
	if ( buf[0] == 'Y' &&
		 buf[1] == 'a' && 
		 buf[2] == 'y' ) {
		
		return ( (buf[4]<<24) |
				 (buf[5]<<16) |
				 (buf[6]<<8)  |
				 buf[7] );
	}

	return 0;
}

/*-----------------------------------------------------------------------------
  指定サイズのデータをromからロードします。
  -----------------------------------------------------------------------------*/
static void
readFromRom( void* buf, int size, int offset )
{
	int i, read_size;
	for ( i = 0 ; i < size ; i += 256 )
	{
		if ( size - i < 256 ) read_size = size - i;
		else read_size = 256;
		__OSReadROM( (u8*)buf + i, read_size, offset + i );
	}
}

/*-----------------------------------------------------------------------------
  
  -----------------------------------------------------------------------------*/
static u8*
getFontPtr( void )
{
	u8 *buf, *tExpandBuf;
	u32	align_size;
	int size;

#ifdef IPL
    if ( gGetNowCountry() == COUNTRY_JAPAN ) {
		buf = gAlloc( IPL_JPN_FONT_SIZE );
        readFromRom( buf, IPL_JPN_FONT_SIZE, IPL_FONT_OFFSET );
    } else {
		buf = gAlloc( IPL_ENG_FONT_SIZE );
        readFromRom( buf, IPL_ENG_FONT_SIZE, IPL_FONT_OFFSET + IPL_JPN_FONT_SIZE );
    }
#else
	if ( gGetNowCountry() == COUNTRY_JAPAN ) {
		buf = fontImage;
	} else {
		buf = font_eImage;
	}
#endif
	
	if ( ( size = checkCompressed( (u8*)( buf ) ) ) > 0 ) {
		align_size = OSRoundUp32B( size ) ;
		tExpandBuf = gAlloc( align_size );
		decode( buf, tExpandBuf );
		DCStoreRange( tExpandBuf, (u32)align_size );

#ifdef IPL
		gFree( buf );
#endif
	}
	
	return tExpandBuf;	// tExpandBuf needs to free.
}

/*-----------------------------------------------------------------------------
  フォントデータを初期化します。
  圧縮されたフォントシートをロードして、I4テクスチャにします。
  -----------------------------------------------------------------------------*/
void
gfInitFontData( void* buffer )
{
#pragma unused (buffer)
	u8 *buf;

    buf = getFontPtr();
	
	sHeader = *(FontHeader*)buf;

	if ( sSheetImage == NULL ) {
		sSheetImage = gAlloc( sHeader.sheetFullSize  );
		expandFontSheet( (u8*)buf + sHeader.sheetImage , (u8*)sSheetImage );
	}

	sCharsInSheet = sHeader.sheetRow * sHeader.sheetLine;

	if ( sSizeTable == NULL ) {
		u32		width_table_size = sHeader.sheetImage - sHeader.widthTable;
		sSizeTable = gAlloc( OSRoundUp32B(width_table_size) );
		memset( sSizeTable, 0, OSRoundUp32B(width_table_size) );
		memcpy( sSizeTable, (u8*)buf + sHeader.widthTable, width_table_size );
	}
	
	gFree( buf );
}

/*-----------------------------------------------------------------------------
  フォントテクスチャデータを展開
  -----------------------------------------------------------------------------*/
static void
expandFontSheet( u8* src, u8* dst )
{
	int i;
	u8* colorIndex = (u8*)&sHeader.c0;

	if ( sHeader.sheetFormat == GX_TF_I4 ) {
		// I4で展開
		for ( i = 0 ; i < sHeader.sheetFullSize / 2 ; i++ ) {
			dst[ i*2 + 0 ] = (u8)( ( colorIndex[ (src[i] >> 6) & 3 ] & 0xf0 ) |
								   ( colorIndex[ (src[i] >> 4) & 3 ] & 0x0f ) );
			dst[ i*2 + 1 ] = (u8)( ( colorIndex[ (src[i] >> 2) & 3 ] & 0xf0 ) |
								   ( colorIndex[ (src[i]     ) & 3 ] & 0x0f ) );
		}
	}
	else if ( sHeader.sheetFormat == GX_TF_IA4 ) {
		/* IA4で展開  */
		for ( i = 0 ; i < sHeader.sheetFullSize / 4 ; i++ ) {
			dst[ i*4 + 0 ] = colorIndex[ (src[i] >> 6) & 3 ];
			dst[ i*4 + 1 ] = colorIndex[ (src[i] >> 4) & 3 ];
			dst[ i*4 + 2 ] = colorIndex[ (src[i] >> 2) & 3 ];
			dst[ i*4 + 3 ] = colorIndex[ (src[i]     ) & 3 ];
		}
	}

	DCStoreRange( dst, sHeader.sheetFullSize );
}











/*-----------------------------------------------------------------------------
  文字を描画し、文字幅を返します。
  -----------------------------------------------------------------------------*/
u16
drawFont( int x, int y ,int size , int sjisCode )
{
	int code;// = getFontCode( sjisCode );

	code = getFontCode( sjisCode & 0xffff );
	drawFontSJIS( (u16)x, (u16)y, size, code );
	
	return (u16)( sSizeTable[code] * 16 * size / sHeader.cellWidth) ;
}

/*-----------------------------------------------------------------------------
  
  -----------------------------------------------------------------------------*/
static int
parse( int x, int y, int size , u8* string )
{
	u16 code;
	int	sx = 0, font_size;
	
	while ( (code = *string++) != '\0') {
		if ( gGetNowCountry() == COUNTRY_JAPAN ) {
			if ( ( ( code >= 0x80 && code <= 0x9f ) || ( code >= 0xe0 && code <= 0xff ) ) &&
				 *string != '\0') {
				code = (u16)( (code << 8) | *string++ );
			}
		}
		font_size = drawFont( x + sx , y, size, code);
		sx += font_size;
	}
	return sx;
}

/*-----------------------------------------------------------------------------
  
  -----------------------------------------------------------------------------*/
int
print( int x, int y, int size, char* format, ... )
{
	char string[256];
	int length;
	int width;
	
	va_list vargs;
	va_start(vargs, format);

	length = vsnprintf(string, 256, format, vargs);
	width = parse(x * 16, y * 16, size,(u8*)string);

	va_end(vargs);
	return width;
}

static BOOL
compareWidth( s32 width, s32 char_space, s32 max_width )
{
	if ( char_space > 0 ) {
		if ( width - char_space * 16 > max_width ) return TRUE;
	} else {
		if ( width > max_width ) return TRUE;
	}

	return FALSE;
}

/*-----------------------------------------------------------------------------
  文字列をtextboxの情報を元に描画します。
  -----------------------------------------------------------------------------*/
static u16
getStartPosition(
	gLRTextBoxData* tdp,
	u16* widths ,
	int line_num )
{
	u16 ret;
	switch ( tdp->h_bind & 0x7f )
	{
	case cBind_center:	ret = (u16)( tdp->x - widths[ line_num ]/2 );				break;
	case cBind_left:	ret = (u16)( tdp->x - tdp->dx / 2 );						break;
	case cBind_right:	ret = (u16)( tdp->x + tdp->dx / 2 - widths[line_num] );	break;
	}
	return ret;
}

static int
parseDraw(
	gLRTextBoxData* tdp,
	u8* string,
	u16* widths ,
	int height,
	int	string_num_max,
	int string_line_max,
	BOOL line_feed ,
	BOOL oneline )
{
	int	line_num = 0;
	int string_num = 0;
	u16 code;
	u16 max_width = (u16)( tdp->dx );
	u16	x, y, sx = 0, pre_sx;

	switch ( tdp->v_bind )
	{
	case cBind_center:	y = (u16)( tdp->y - height /2 );				break;
	case cBind_Top:		y = (u16)( tdp->y - tdp->dy / 2 );				break;
	case cBind_Bottom:	y = (u16)( tdp->y + tdp->dy / 2 - height );		break;
	}

	x = getStartPosition( tdp, widths, line_num );
	// 一行目。
	y += sHeader.ascent * tdp->font_size * 16 / sHeader.cellHeight;

	while ( (code = *string++) != '\0') {
		if ( gGetNowCountry() == COUNTRY_JAPAN ) {
			if ( ( ( code >= 0x80 && code <= 0x9f ) || ( code >= 0xe0 && code <= 0xff ) ) &&
				 *string != '\0') {
				code = (u16)( (code << 8) | *string++ );
				string_num++;
			}
		}

		if ( code == 0x0a ) {
			if ( line_feed ) {
				line_num++;
				x = getStartPosition( tdp, widths, line_num );
				y += ( tdp->line_space * 16 );
				sx = 0;
				if ( string_line_max !=0 && line_num >= string_line_max ) { // 行数制限。
					line_num--;
					height -= tdp->line_space * 16;
					break;
				}
			}
		}
		else {
			pre_sx = (u16)( sx + ( sSizeTable[ getFontCode( code )] * 16 * tdp->font_size / sHeader.cellWidth
							+ tdp->char_space * 16) );
			if ( compareWidth( pre_sx, tdp->char_space, max_width ) ) {
				if ( line_feed ) {
					line_num++;
					x = getStartPosition( tdp, widths, line_num );
					y += ( tdp->line_space * 16 );
					sx = 0;
					if ( *string == 0x0a ) string++;
				}
				if ( oneline ) break;
				if ( string_line_max !=0 && line_num >= string_line_max ) { // 行数制限。
					line_num--;
					height -= tdp->line_space * 16;
					break;
				}
			}
			drawFont( x + sx , y, tdp->font_size, code);
			sx += ( sSizeTable[ getFontCode( code )] * 16 * tdp->font_size / sHeader.cellWidth
					+ tdp->char_space * 16);
			string_num++;
		}

/*		if ( compareWidth( sx, tdp->char_space, max_width ) ) {
			if ( line_feed ) {
				line_num++;
				x = getStartPosition( tdp, widths, line_num );
				y += ( tdp->line_space * 16 );
				sx = 0;
				if ( *string == 0x0a ) string++;
			}
			if ( oneline ) break;
		}*/
		if ( string_num_max != 0 && string_num >= string_num_max ) break;	// ループから抜ける。
		if ( string_line_max !=0 && line_num >= string_line_max ) { // 行数制限。
			line_num--;
			height -= tdp->line_space * 16;
			break;
		}
	}
	return sx;
}


/*-----------------------------------------------------------------------------
  改行に必要な処理をします。
  -----------------------------------------------------------------------------*/
static void
gotoNextLine(
	gLRTextBoxData* tdp,
	u16		line_end_code,
	u16*	width,
	u16*	widths,
	int*	line_num,
	int*	height,
	BOOL	next_line )
{
	MainState* msp = getMainState();
	u16	temp_width = *width;
	
	if (temp_width != 0 ) {
		(temp_width) -= tdp->char_space * 16;
		if ( line_end_code == '。' || line_end_code == '、' || line_end_code == '．' || line_end_code == '，' ) {
			(temp_width) -= sSizeTable[ getFontCode( line_end_code )] * msp->font_mergin * tdp->font_size / sHeader.cellWidth; // 文字幅の半分を引く。
		}
	}
	widths[*line_num] = temp_width;
	temp_width = 0;
	if ( next_line ) {
		(*line_num)++;
		if ( *line_num >= GF_MAX_LINES ) {
			*height = 0;
		}
		else {
			(*height) += ( tdp->line_space * 16 );
		}
	}
	*width = temp_width;
}

/*-----------------------------------------------------------------------------
  描画するのに必要な、幅と高さと行数を取得します。
    widths:幅（安全にするなら、GF_MAX_LINESの配列）
    ln:行数。
    auto_line_feed: TRUEのとき、tdp->widthで指定される幅で折り返します。
    auto_line_feed: FALSEのとき、改行以外で行が変わることはありません。
  　line_feed: TRUEのとき、0x0aで改行します。
  　line_feed: FALSEのとき、0x0aで改行しません。
    oneline: TRUEのとき、一行終わったら、それ以上書きません。
    oneline: FALSEのとき、一行終わったら、それ以上書きません。

    戻り値：高さ(s11.4)
  -----------------------------------------------------------------------------*/
static int
getStringWidthHeight(
	gLRTextBoxData* tdp,
	u8* string,
	u16* widths ,
	int* ln,
	BOOL auto_line_feed,
	BOOL line_feed ,
	BOOL oneline,
	int	 string_num_max,
	int	 string_line_max )
{
	int height = ( sHeader.ascent * tdp->font_size * 16 / sHeader.cellHeight + 
				   sHeader.descent * tdp->font_size * 16 / sHeader.cellHeight );
	int line_num = 0;
	u16 code, pre_code = 0;
	u16	width = 0, pre_width;
	u16 max_width = (u16)( tdp->dx );
	int	string_num = 0;
	
	while ( ( code = *string++ ) != '\0' )
	{
		if ( gGetNowCountry() == COUNTRY_JAPAN ) {
			if ( ( ( code >= 0x80 && code <= 0x9f ) || ( code >= 0xe0 && code <= 0xff ) ) &&
				 *string != '\0') {
				code = (u16)( (code << 8) | *string++ );
				string_num++;
			}
		}
		
		if ( code == 0x0a ) { // 改行
			if ( line_feed ) {
				gotoNextLine( tdp, pre_code, &width, widths, &line_num, &height, TRUE );
				if ( height == 0 ) return 0;
				if ( string_line_max !=0 && line_num >= string_line_max ) { // 行数制限。
					break;
				}
			}
		}
		else {
			pre_code = code;
			pre_width = (u16)( width + ( sSizeTable[ getFontCode( code )] * 16 * tdp->font_size / sHeader.cellWidth
					   		   + tdp->char_space * 16) );
			if ( compareWidth( pre_width, tdp->char_space, max_width ) ) {
				if ( auto_line_feed ) {
					if ( *string == 0x0a ) string++;
					gotoNextLine( tdp, code, &width, widths, &line_num, &height, TRUE );
					if ( height == 0 ) return 0;
				}
				if ( oneline ) break;
				if ( string_line_max !=0 && line_num >= string_line_max ) { // 行数制限。
					break;
				}
			}
			width += ( sSizeTable[ getFontCode( code )] * 16 * tdp->font_size / sHeader.cellWidth
					   + tdp->char_space * 16);
			string_num++;
		}

/*		if ( compareWidth( width, tdp->char_space, max_width ) ) { // 改行
			if ( auto_line_feed ) {
				if ( *string == 0x0a ) string++;
				gotoNextLine( tdp, code, &width, widths, &line_num, &height, TRUE );
				if ( height == 0 ) return 0;
			}
			if ( oneline ) break;
		}*/

		pre_code = code;
		if ( string_num_max != 0 && string_num >= string_num_max ) break;	// ループから抜ける。
		if ( string_line_max !=0 && line_num >= string_line_max ) { // 行数制限。
			line_num--;
			height -= tdp->line_space * 16;
			break;
		}
	}
	if ( string_line_max !=0 && line_num >= string_line_max ) {
		line_num--;
		height -= tdp->line_space * 16;
	} else {
		gotoNextLine( tdp, pre_code, &width, widths, &line_num, &height, FALSE );	// 行末処理だけ。
	}

	if ( ln != NULL ) *ln = line_num;

	return height;
}

/*-----------------------------------------------------------------------------
  フォントを描画します。
  -----------------------------------------------------------------------------*/
void
gfPrint( gLRTextBoxData* tdp, char* string )
{
	u16 widths[GF_MAX_LINES];
	u16 height;
	int	line_num;
	
	if ( string == NULL ) {
		string = (char*)tdp + tdp->string_offset;
	}
	height = (u16)getStringWidthHeight( tdp, (u8*)string, widths, &line_num, TRUE, TRUE,FALSE, 0 ,0 );

	if ( height != 0 ) {
		parseDraw( tdp, (u8*)string, widths, height, 0,0, TRUE , FALSE );
	}
}

/*-----------------------------------------------------------------------------
  最大文字数指定で、フォントを描画します。
  -----------------------------------------------------------------------------*/
void
gfPrintN( gLRTextBoxData* tdp, char* string, int string_num_max )
{
	u16 widths[GF_MAX_LINES];
	u16 height;
	int	line_num;
	
	if ( string == NULL ) {
		string = (char*)tdp + tdp->string_offset;
	}
	height = (u16)getStringWidthHeight( tdp, (u8*)string, widths, &line_num, TRUE, TRUE, FALSE,string_num_max, 0);

	if ( height != 0 ) {
		parseDraw( tdp, (u8*)string, widths, height, string_num_max,0, TRUE, FALSE );
	}
}

/*-----------------------------------------------------------------------------
  最大文字数指定で、改行を無視してフォントを描画します。
  -----------------------------------------------------------------------------*/
void
gfPrintNOneLine( gLRTextBoxData* tdp, char* string, int string_num_max )
{
	u16 widths[GF_MAX_LINES];
	u16 height;
	int	line_num;
	
	if ( string == NULL ) {
		string = (char*)tdp + tdp->string_offset;
	}
	height = (u16)getStringWidthHeight( tdp, (u8*)string, widths, &line_num, TRUE, TRUE , FALSE, string_num_max,1 );

	if ( height != 0 ) {
		parseDraw( tdp, (u8*)string, widths, height, string_num_max, 1, TRUE, FALSE );
	}
}

void
gfPrintNTwoLine( gLRTextBoxData* tdp, char* string, int string_num_max )
{
	u16 widths[GF_MAX_LINES];
	u16 height;
	int	line_num;
	
	if ( string == NULL ) {
		string = (char*)tdp + tdp->string_offset;
	}
	height = (u16)getStringWidthHeight( tdp, (u8*)string, widths, &line_num, TRUE, TRUE , FALSE, string_num_max, 2 );

	if ( height != 0 ) {
		parseDraw( tdp, (u8*)string, widths, height, string_num_max,2, TRUE, FALSE );
	}
}


/*-----------------------------------------------------------------------------
  位置を指定して、フォントを描画します。
  -----------------------------------------------------------------------------*/
void
gfPrintPos( gLRTextBoxData* tdp, char* string, int x, int y )
{
	gLRTextBoxData	temp_tbd;

	temp_tbd = *tdp;
	temp_tbd.x = (u16)x;
	temp_tbd.y = (u16)y;

	gfPrint( &temp_tbd, string );
}

/*-----------------------------------------------------------------------------
  位置を指定して、フォントを描画します。
  -----------------------------------------------------------------------------*/
void
gfPrintNPos( gLRTextBoxData* tdp, char* string, int x, int y, int max)
{
	gLRTextBoxData	temp_tbd;

	temp_tbd = *tdp;
	temp_tbd.x = (u16)x;
	temp_tbd.y = (u16)y;

	gfPrintN( &temp_tbd, string, max );
}

/*-----------------------------------------------------------------------------
  バックカラーを描画して、派手な文字を書きます。
  -----------------------------------------------------------------------------*/
void
gfUpdateAlphaAnime( void )
{
	MainState*	msp = getMainState();

	msp->bright_frame += msp->bright_frame_bounds;

	if ( msp->bright_frame > msp->bright_frame_max ) {
		msp->bright_frame = msp->bright_frame_max;
		msp->bright_frame_bounds = -1;
	}
	if ( msp->bright_frame < 0 ) {
		msp->bright_frame = 0;
		msp->bright_frame_bounds = 1;
	}
}

static u8
gfGetNowAlpha( void )
{
	MainState*	msp = getMainState();
	s16		now_alpha;

	now_alpha = (s16)( msp->bright_alpha_min +
			( msp->bright_alpha_max - msp->bright_alpha_min ) * msp->bright_frame / msp->bright_frame_max );

	return (u8)now_alpha;
}

void
gfBritePrint( gLRTextBoxData* tdp, char* string )
{
	u32	backup_color_u, backup_color_d;
	u8	alpha_u, alpha_d;
	gLRTextBoxData	temp_tbd;
	u8	now_alpha;
	int i, j, x, y;

	temp_tbd = *tdp;

	backup_color_u = sColorU;
	backup_color_d = sColorD;

	now_alpha = gfGetNowAlpha();
	alpha_u = (u8)( ( sBackColorU & 0xff ) * now_alpha / 255 );
	alpha_d = (u8)( ( sBackColorD & 0xff ) * now_alpha / 255 );
	sColorU = ( sBackColorU & 0xffffff00 ) | ( alpha_u / 6 );
	sColorD = ( sBackColorD & 0xffffff00 ) | ( alpha_d / 6 );

	x = temp_tbd.x;
	y = temp_tbd.y;
	for ( i = 0 ; i < 5 ; i++ ) {
		for ( j = 0 ; j < 5 ; j++ ) {
			temp_tbd.x = (u16)( x - 64 + 32 * i );
			temp_tbd.y = (u16)( y - 64 + 32 * j );
			gfPrint( &temp_tbd, string );
		}
	}
	temp_tbd.x = (u16)x;
	temp_tbd.y = (u16)y;
	sColorU = backup_color_u;
	sColorD = backup_color_d;
	gfPrint( &temp_tbd, string );
}

/*-----------------------------------------------------------------------------
  textboxを勝手に改行されることなしに表示するのに必要な、幅と高さを取得します。
  -----------------------------------------------------------------------------*/
void
gfGetWidthHeight( gLRTextBoxData* tdp, char* string, u16* max_width, u16* height )
{
	u16 widths[GF_MAX_LINES];
	int	line_num;
	int i;

	*max_width = 0;
	
	if ( string == NULL ) {
		string = (char*)tdp + tdp->string_offset;
	}
	*height = (u16)getStringWidthHeight( tdp, (u8*)string, widths, &line_num, FALSE, TRUE, FALSE, 0 ,0);

	for ( i = 0 ; i <= line_num ; i++ ) {
		if ( *max_width < widths[i] ) {
			*max_width = widths[i];
		}
	}
}


/*-----------------------------------------------------------------------------
  textboxに幅が収まるように表示するのに必要な、幅と高さを取得します。
  -----------------------------------------------------------------------------*/
void
gfGetWidthHeightLineFeed(  gLRTextBoxData* tdp, char* string, u16* max_width, u16* height )
{
	u16 widths[GF_MAX_LINES];
	int	line_num;
	int i;

	*max_width = 0;
	
	if ( string == NULL ) {
		string = (char*)tdp + tdp->string_offset;
	}
	*height = (u16)getStringWidthHeight( tdp, (u8*)string, widths, &line_num, TRUE, TRUE, FALSE, 0 ,0);

	for ( i = 0 ; i <= line_num ; i++ ) {
		if ( *max_width < widths[i] ) {
			*max_width = widths[i];
		}
	}
}







/*-----------------------------------------------------------------------------
  テクスチャロード
  -----------------------------------------------------------------------------*/
static void
loadSheet( int sheet , GXTexMapID texMapID )
{
	if (sheet != sLoadingSheet)
	{
		u8* image = (u8*)sSheetImage + sHeader.sheetSize * sheet;

		GXInitTexObj(&sTexObj, image,
					 sHeader.sheetWidth,
					 sHeader.sheetHeight,
					 (GXTexFmt)sHeader.sheetFormat,
					 GX_CLAMP,
					 GX_CLAMP,
					 GX_FALSE
		);
		GXInitTexObjLOD(&sTexObj, GX_LINEAR, GX_LINEAR, 0.0f, 0.0f, 0.0f, 0, 0, GX_ANISO_1);
//		GXInitTexObjLOD(&sTexObj, GX_NEAR, GX_NEAR, 0.0f, 0.0f, 0.0f, 0, 0, GX_ANISO_1);
		sLoadingSheet = sheet;
	}
	GXLoadTexObj(&sTexObj, texMapID);
}

/*-----------------------------------------------------------------------------
  フォントを描画
  -----------------------------------------------------------------------------*/
static void
drawFontSJIS( u16 posh, u16 posv, int size , int fontCode )
{
	int sheet = fontCode / sCharsInSheet;
	int numChars = fontCode - sheet * sCharsInSheet;
	int line     = numChars / sHeader.sheetRow;
	int column   = numChars - line * sHeader.sheetRow;
	int tCellPosH = column * sHeader.cellWidth;
	int tCellPosV = line   * sHeader.cellHeight;
	
	int posLeft   = posh;
	int posRight  = posh + size * 16;//sHeader.cellWidth;
	int posTop	  = posv - sHeader.ascent * size * 16 / sHeader.cellHeight ;
	int posBottom = posv + sHeader.descent * size * 16 / sHeader.cellHeight ;
	
	int texLeft	  =  tCellPosH				  * 16384 / sHeader.sheetWidth;
	int texTop	  =  tCellPosV				  * 16384 / sHeader.sheetHeight;
	int texRight  = (tCellPosH + sHeader.cellWidth ) * 16384 / sHeader.sheetWidth;
	int texBottom = (tCellPosV + sHeader.cellHeight) * 16384 / sHeader.sheetHeight;

	loadSheet( sheet , GX_TEXMAP0 );

	GXBegin(GX_QUADS, GX_VTXFMT1, 4);
		GXPosition3s16((s16)posLeft , (s16)posTop	, 0);
		GXColor1u32(sColorU);
		GXTexCoord2u16((u16)texLeft , (u16)texTop		);

		GXPosition3s16((s16)posRight, (s16)posTop	, 0);
		GXColor1u32(sColorU);
		GXTexCoord2u16((u16)texRight, (u16)texTop		);

		GXPosition3s16((s16)posRight, (s16)posBottom, 0);
		GXColor1u32(sColorD);
		GXTexCoord2u16((u16)texRight, (u16)texBottom	);

		GXPosition3s16((s16)posLeft , (s16)posBottom, 0);
		GXColor1u32(sColorD);
		GXTexCoord2u16((u16)texLeft , (u16)texBottom	);
	GXEnd();
}

void
gfSetFontColor( u32 up, u32 down )
{
	sColorU = up;
	sColorD = down;
}
void
gfSetFontGXColor( GXColor* up, GXColor* down )
{
	sColorU = (u32)( (up->r<<24)|(up->g<<16)|(up->b<<8)|up->a );
	sColorD = (u32)( (down->r<<24)|(down->g<<16)|(down->b<<8)|down->a );
}

void
gfSetBackFontColor( u32 up, u32 down )
{
	sBackColorU = up;
	sBackColorD = down;
}
void
gfSetBackFontGXColor( GXColor* up, GXColor* down )
{
	sBackColorU = (u32)( (up->r<<24)|(up->g<<16)|(up->b<<8)|up->a );
	sBackColorD = (u32)( (down->r<<24)|(down->g<<16)|(down->b<<8)|down->a );
}

void
gfSetFontSetting( void )
{
	sLoadingSheet = -1;	// Font用のGXセットで、ロードフラグをクリアする。
	sColorU = 0xffffffff;
	sColorD = 0xffffffff;
}

/*-----------------------------------------------------------------------------
  フォント用のGXの設定
  -----------------------------------------------------------------------------*/
void
setGXforFont( void )
{
	Mtx viewMatrix;
	Mtx44 projMatrix;
	Vec camPos, camUp,target;

	gfSetFontSetting();
	
	MTXOrtho( projMatrix,
		0.0f,448.0f, 
		0.0f, 584.f,//640.0f,
		10.0f, 1000.0f	);
	GXSetProjection(projMatrix, GX_ORTHOGRAPHIC);
	camPos =  (Vec){0.0f,0.0f,100.0f};
	camUp  =  (Vec){0.0f,1.0f,0.0f};
	target =  (Vec){0.0f,0.0f,0.0f};

	MTXLookAt(viewMatrix ,
			  &camPos,
			  &camUp,
			  &target
			  );

	GXLoadPosMtxImm(viewMatrix, GX_PNMTX0);
	GXSetCurrentMtx(GX_PNMTX0);

	gSetGXforDirect( TRUE, TRUE, FALSE );

	if ( sRenderMode == IPL_FONT_COLOR_TEXTURE ) {
		//GXSetTevOp(GX_TEVSTAGE0, GX_MODULATE);
		GXSetTevColorIn( GX_TEVSTAGE0, GX_CC_ZERO, GX_CC_TEXC, GX_CC_RASC, GX_CC_ZERO );
		GXSetTevColorOp( GX_TEVSTAGE0, GX_TEV_ADD, GX_TB_ZERO, GX_CS_SCALE_1, GX_TRUE, GX_TEVPREV);
		GXSetTevAlphaIn( GX_TEVSTAGE0, GX_CA_ZERO, GX_CA_TEXA, GX_CA_RASA, GX_CA_ZERO );
		GXSetTevAlphaOp( GX_TEVSTAGE0, GX_TEV_ADD, GX_TB_ZERO, GX_CS_SCALE_1, GX_TRUE, GX_TEVPREV);
	}
}


/*-----------------------------------------------------------------------------
  SJISまたはASCIIコードから、フォントコードを取得します。
  -----------------------------------------------------------------------------*/
static int
getFontCode( int sjisCode )
{
#ifdef NTSC
	static unsigned short half2code[] = {	//半角文字をフォントコードに変換する表。
		0x020c, 0x020d, 0x020e, 0x020f, 0x0210, 0x0211, 0x0212, 0x0213,	// 20
		0x0214, 0x0215, 0x0216, 0x0217, 0x0218, 0x0219, 0x021a, 0x021b,
		0x021c, 0x021d, 0x021e, 0x021f, 0x0220, 0x0221, 0x0222, 0x0223,	// 30
		0x0224, 0x0225, 0x0226, 0x0227, 0x0228, 0x0229, 0x022a, 0x022b,
		0x022c, 0x022d, 0x022e, 0x022f, 0x0230, 0x0231, 0x0232, 0x0233,	// 40
		0x0234, 0x0235, 0x0236, 0x0237, 0x0238, 0x0239, 0x023a, 0x023b,
		0x023c, 0x023d, 0x023e, 0x023f, 0x0240, 0x0241, 0x0242, 0x0243,	// 50
		0x0244, 0x0245, 0x0246, 0x0247, 0x0248, 0x0249, 0x024a, 0x024b,
		0x024c, 0x024d, 0x024e, 0x024f, 0x0250, 0x0251, 0x0252, 0x0253,	// 60
		0x0254, 0x0255, 0x0256, 0x0257, 0x0258, 0x0259, 0x025a, 0x025b,
		0x025c, 0x025d, 0x025e, 0x025f, 0x0260, 0x0261, 0x0262, 0x0263,	// 70
		0x0264, 0x0265, 0x0266, 0x0267, 0x0268, 0x0269, 0x026a, 0x020c,
		0x020c, 0x020c, 0x020c, 0x020c, 0x020c, 0x020c, 0x020c, 0x020c,	// 80
		0x020c, 0x020c, 0x020c, 0x020c, 0x020c, 0x020c, 0x020c, 0x020c,
		0x020c, 0x020c, 0x020c, 0x020c, 0x020c, 0x020c, 0x020c, 0x020c,	// 90
		0x020c, 0x020c, 0x020c, 0x020c, 0x020c, 0x020c, 0x020c, 0x020c,
		0x020c, 0x026b, 0x026c, 0x026d, 0x026e, 0x026f, 0x0270, 0x0271,	// a0
		0x0272, 0x0273, 0x0274, 0x0275, 0x0276, 0x0277, 0x0278, 0x0279,
		0x027a, 0x027b, 0x027c, 0x027d, 0x027e, 0x027f, 0x0280, 0x0281,	// b0
		0x0282, 0x0283, 0x0284, 0x0285, 0x0286, 0x0287, 0x0288, 0x0289,
		0x028a, 0x028b, 0x028c, 0x028d, 0x028e, 0x028f, 0x0290, 0x0291,	// c0
		0x0292, 0x0293, 0x0294, 0x0295, 0x0296, 0x0297, 0x0298, 0x0299,
		0x029a, 0x029b, 0x029c, 0x029d, 0x029e, 0x029f, 0x02a0, 0x02a1,	// d0
		0x02a2, 0x02a3, 0x02a4, 0x02a5, 0x02a6, 0x02a7, 0x02a8, 0x02a9,
	};
	
	static unsigned short kanji2code[]={	//全角文字をフォントコードに変換する表。
		0x000, 0x001, 0x002, 0x003, 0x004, 0x005, 0x006, 0x007, 	/* 8140 */
		0x008, 0x009, 0x00A, 0x00B, 0x00C, 0x00D, 0x00E, 0x00F, 
		0x010, 0x011, 0x012, 0x013, 0x014, 0x015, 0x016, 0x017, 	/* 8150 */
		0x018, 0x019, 0x01A, 0x01B, 0x01C, 0x01D, 0x01E, 0x01F, 
		0x020, 0x021, 0x022, 0x023, 0x024, 0x025, 0x026, 0x027, 	/* 8160 */
		0x028, 0x029, 0x02A, 0x02B, 0x02C, 0x02D, 0x02E, 0x02F, 
		0x030, 0x031, 0x032, 0x033, 0x034, 0x035, 0x036, 0x037, 	/* 8170 */
		0x038, 0x039, 0x03A, 0x03B, 0x03C, 0x03D, 0x03E, 
		0x03F, 0x040, 0x041, 0x042, 0x043, 0x044, 0x045, 0x046, 	/* 8180 */
		0x047, 0x048, 0x049, 0x04A, 0x04B, 0x04C, 0x04D, 0x04E, 
		0x04F, 0x050, 0x051, 0x052, 0x053, 0x054, 0x055, 0x056, 	/* 8190 */
		0x057, 0x058, 0x059, 0x05A, 0x05B, 0x05C, 0x05D, 0x05E, 
		0x05F, 0x060, 0x061, 0x062, 0x063, 0x064, 0x065, 0x066, 	/* 81A0 */
		0x067, 0x068, 0x069, 0x06A, 0x06B, 0x000, 0x000, 0x000, 
		0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 	/* 81B0 */
		0x06C, 0x06D, 0x06E, 0x06F, 0x070, 0x071, 0x072, 0x073, 
		0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 	/* 81C0 */
		0x074, 0x075, 0x076, 0x077, 0x078, 0x079, 0x07A, 0x000, 
		0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 	/* 81D0 */
		0x000, 0x000, 0x07B, 0x07C, 0x07D, 0x07E, 0x07F, 0x080, 
		0x081, 0x082, 0x083, 0x084, 0x085, 0x086, 0x087, 0x088, 	/* 81E0 */
		0x089, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 
		0x08A, 0x08B, 0x08C, 0x08D, 0x08E, 0x08F, 0x090, 0x091, 	/* 81F0 */
		0x000, 0x000, 0x000, 0x000, 0x092, 
		0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 	/* 8240 */
		0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x093, 
		0x094, 0x095, 0x096, 0x097, 0x098, 0x099, 0x09A, 0x09B, 	/* 8250 */
		0x09C, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 
		0x09D, 0x09E, 0x09F, 0x0A0, 0x0A1, 0x0A2, 0x0A3, 0x0A4, 	/* 8260 */
		0x0A5, 0x0A6, 0x0A7, 0x0A8, 0x0A9, 0x0AA, 0x0AB, 0x0AC, 
		0x0AD, 0x0AE, 0x0AF, 0x0B0, 0x0B1, 0x0B2, 0x0B3, 0x0B4, 	/* 8270 */
		0x0B5, 0x0B6, 0x000, 0x000, 0x000, 0x000, 0x000, 
		0x000, 0x0B7, 0x0B8, 0x0B9, 0x0BA, 0x0BB, 0x0BC, 0x0BD, 	/* 8280 */
		0x0BE, 0x0BF, 0x0C0, 0x0C1, 0x0C2, 0x0C3, 0x0C4, 0x0C5, 
		0x0C6, 0x0C7, 0x0C8, 0x0C9, 0x0CA, 0x0CB, 0x0CC, 0x0CD, 	/* 8290 */
		0x0CE, 0x0CF, 0x0D0, 0x000, 0x000, 0x000, 0x000, 0x0D1, 
		0x0D2, 0x0D3, 0x0D4, 0x0D5, 0x0D6, 0x0D7, 0x0D8, 0x0D9, 	/* 82A0 */
		0x0DA, 0x0DB, 0x0DC, 0x0DD, 0x0DE, 0x0DF, 0x0E0, 0x0E1, 
		0x0E2, 0x0E3, 0x0E4, 0x0E5, 0x0E6, 0x0E7, 0x0E8, 0x0E9, 	/* 82B0 */
		0x0EA, 0x0EB, 0x0EC, 0x0ED, 0x0EE, 0x0EF, 0x0F0, 0x0F1, 
		0x0F2, 0x0F3, 0x0F4, 0x0F5, 0x0F6, 0x0F7, 0x0F8, 0x0F9, 	/* 82C0 */
		0x0FA, 0x0FB, 0x0FC, 0x0FD, 0x0FE, 0x0FF, 0x100, 0x101, 
		0x102, 0x103, 0x104, 0x105, 0x106, 0x107, 0x108, 0x109, 	/* 82D0 */
		0x10A, 0x10B, 0x10C, 0x10D, 0x10E, 0x10F, 0x110, 0x111, 
		0x112, 0x113, 0x114, 0x115, 0x116, 0x117, 0x118, 0x119, 	/* 82E0 */
		0x11A, 0x11B, 0x11C, 0x11D, 0x11E, 0x11F, 0x120, 0x121, 
		0x122, 0x123, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 	/* 82F0 */
		0x000, 0x000, 0x000, 0x000, 0x000, 
		0x124, 0x125, 0x126, 0x127, 0x128, 0x129, 0x12A, 0x12B, 	/* 8340 */
		0x12C, 0x12D, 0x12E, 0x12F, 0x130, 0x131, 0x132, 0x133, 
		0x134, 0x135, 0x136, 0x137, 0x138, 0x139, 0x13A, 0x13B, 	/* 8350 */
		0x13C, 0x13D, 0x13E, 0x13F, 0x140, 0x141, 0x142, 0x143, 
		0x144, 0x145, 0x146, 0x147, 0x148, 0x149, 0x14A, 0x14B, 	/* 8360 */
		0x14C, 0x14D, 0x14E, 0x14F, 0x150, 0x151, 0x152, 0x153, 
		0x154, 0x155, 0x156, 0x157, 0x158, 0x159, 0x15A, 0x15B, 	/* 8370 */
		0x15C, 0x15D, 0x15E, 0x15F, 0x160, 0x161, 0x162, 
		0x163, 0x164, 0x165, 0x166, 0x167, 0x168, 0x169, 0x16A, 	/* 8380 */
		0x16B, 0x16C, 0x16D, 0x16E, 0x16F, 0x170, 0x171, 0x172, 
		0x173, 0x174, 0x175, 0x176, 0x177, 0x178, 0x179, 0x000, 	/* 8390 */
		0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x17A, 
		0x17B, 0x17C, 0x17D, 0x17E, 0x17F, 0x180, 0x181, 0x182, 	/* 83A0 */
		0x183, 0x184, 0x185, 0x186, 0x187, 0x188, 0x189, 0x18A, 
		0x18B, 0x18C, 0x18D, 0x18E, 0x18F, 0x190, 0x191, 0x000, 	/* 83B0 */
		0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x192, 
		0x193, 0x194, 0x195, 0x196, 0x197, 0x198, 0x199, 0x19A, 	/* 83C0 */
		0x19B, 0x19C, 0x19D, 0x19E, 0x19F, 0x1A0, 0x1A1, 0x1A2, 
		0x1A3, 0x1A4, 0x1A5, 0x1A6, 0x1A7, 0x1A8, 0x1A9, 0x000, 	/* 83D0 */
		0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 
		0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 	/* 83E0 */
		0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 
		0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 	/* 83F0 */
		0x000, 0x000, 0x000, 0x000, 0x000, 
		0x1AA, 0x1AB, 0x1AC, 0x1AD, 0x1AE, 0x1AF, 0x1B0, 0x1B1, 	/* 8440 */
		0x1B2, 0x1B3, 0x1B4, 0x1B5, 0x1B6, 0x1B7, 0x1B8, 0x1B9, 
		0x1BA, 0x1BB, 0x1BC, 0x1BD, 0x1BE, 0x1BF, 0x1C0, 0x1C1, 	/* 8450 */
		0x1C2, 0x1C3, 0x1C4, 0x1C5, 0x1C6, 0x1C7, 0x1C8, 0x1C9, 
		0x1CA, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 	/* 8460 */
		0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 
		0x1CB, 0x1CC, 0x1CD, 0x1CE, 0x1CF, 0x1D0, 0x1D1, 0x1D2, 	/* 8470 */
		0x1D3, 0x1D4, 0x1D5, 0x1D6, 0x1D7, 0x1D8, 0x1D9, 
		0x1DA, 0x1DB, 0x1DC, 0x1DD, 0x1DE, 0x1DF, 0x1E0, 0x1E1, 	/* 8480 */
		0x1E2, 0x1E3, 0x1E4, 0x1E5, 0x1E6, 0x1E7, 0x1E8, 0x1E9, 
		0x1EA, 0x1EB, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 	/* 8490 */
		0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x1EC, 
		0x1ED, 0x1EE, 0x1EF, 0x1F0, 0x1F1, 0x1F2, 0x1F3, 0x1F4, 	/* 84A0 */
		0x1F5, 0x1F6, 0x1F7, 0x1F8, 0x1F9, 0x1FA, 0x1FB, 0x1FC, 
		0x1FD, 0x1FE, 0x1FF, 0x200, 0x201, 0x202, 0x203, 0x204, 	/* 84B0 */
		0x205, 0x206, 0x207, 0x208, 0x209, 0x20A, 0x20B, 0x000, 
		0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 	/* 84C0 */
		0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 
		0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 	/* 84D0 */
		0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 
		0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 	/* 84E0 */
		0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 
		0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 	/* 84F0 */
		0x000, 0x000, 0x000, 0x000, 0x20C, 
		0x20D, 0x20E, 0x20F, 0x210, 0x211, 0x212, 0x213, 0x214, 	/* 8540 */
		0x215, 0x216, 0x217, 0x218, 0x219, 0x21A, 0x21B, 0x21C, 
		0x21D, 0x21E, 0x21F, 0x220, 0x221, 0x222, 0x223, 0x224, 	/* 8550 */
		0x225, 0x226, 0x227, 0x228, 0x229, 0x22A, 0x22B, 0x22C, 
		0x22D, 0x22E, 0x22F, 0x230, 0x231, 0x232, 0x233, 0x234, 	/* 8560 */
		0x235, 0x236, 0x237, 0x238, 0x239, 0x23A, 0x23B, 0x23C, 
		0x23D, 0x23E, 0x23F, 0x240, 0x241, 0x242, 0x243, 0x244, 	/* 8570 */
		0x245, 0x246, 0x247, 0x248, 0x249, 0x24A, 0x24B, 
		0x24C, 0x24D, 0x24E, 0x24F, 0x250, 0x251, 0x252, 0x253, 	/* 8580 */
		0x254, 0x255, 0x256, 0x257, 0x258, 0x259, 0x25A, 0x25B, 
		0x25C, 0x25D, 0x25E, 0x25F, 0x260, 0x261, 0x262, 0x263, 	/* 8590 */
		0x264, 0x265, 0x266, 0x267, 0x268, 0x269, 0x26A, 0x26B, 
		0x26C, 0x26D, 0x26E, 0x26F, 0x270, 0x271, 0x272, 0x273, 	/* 85A0 */
		0x274, 0x275, 0x276, 0x277, 0x278, 0x279, 0x27A, 0x27B, 
		0x27C, 0x27D, 0x27E, 0x27F, 0x280, 0x281, 0x282, 0x283, 	/* 85B0 */
		0x284, 0x285, 0x286, 0x287, 0x288, 0x289, 0x28A, 0x28B, 
		0x28C, 0x28D, 0x28E, 0x28F, 0x290, 0x291, 0x292, 0x293, 	/* 85C0 */
		0x294, 0x295, 0x296, 0x297, 0x298, 0x299, 0x29A, 0x29B, 
		0x29C, 0x29D, 0x29E, 0x29F, 0x2A0, 0x2A1, 0x2A2, 0x2A3, 	/* 85D0 */
		0x2A4, 0x2A5, 0x2A6, 0x2A7, 0x2A8, 0x2A9, 0x2AA, 0x2AB, 
		0x2AC, 0x2AD, 0x2AE, 0x2AF, 0x2B0, 0x2B1, 0x2B2, 0x2B3, 	/* 85E0 */
		0x2B4, 0x2B5, 0x2B6, 0x2B7, 0x2B8, 0x2B9, 0x2BA, 0x2BB, 
		0x2BC, 0x2BD, 0x2BE, 0x2BF, 0x2C0, 0x2C1, 0x2C2, 0x2C3, 	/* 85F0 */
		0x2C4, 0x2C5, 0x2C6, 0x2C7, 0x2C8, 
		0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 	/* 8640 */
		0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 
		0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 	/* 8650 */
		0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 
		0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 	/* 8660 */
		0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 
		0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 	/* 8670 */
		0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 
		0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 	/* 8680 */
		0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 
		0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 	/* 8690 */
		0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 
		0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 	/* 86A0 */
		0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 
		0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 	/* 86B0 */
		0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 
		0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 	/* 86C0 */
		0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 
		0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 	/* 86D0 */
		0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 
		0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 	/* 86E0 */
		0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 
		0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 	/* 86F0 */
		0x000, 0x000, 0x000, 0x000, 0x000, 
		0x2C9, 0x2CA, 0x2CB, 0x2CC, 0x2CD, 0x2CE, 0x2CF, 0x2D0, 	/* 8740 */
		0x2D1, 0x2D2, 0x2D3, 0x2D4, 0x2D5, 0x2D6, 0x2D7, 0x2D8, 
		0x2D9, 0x2DA, 0x2DB, 0x2DC, 0x2DD, 0x2DE, 0x2DF, 0x2E0, 	/* 8750 */
		0x2E1, 0x2E2, 0x2E3, 0x2E4, 0x2E5, 0x2E6, 0x000, 0x2E7, 
		0x2E8, 0x2E9, 0x2EA, 0x2EB, 0x2EC, 0x2ED, 0x2EE, 0x2EF, 	/* 8760 */
		0x2F0, 0x2F1, 0x2F2, 0x2F3, 0x2F4, 0x2F5, 0x2F6, 0x2F7, 
		0x2F8, 0x2F9, 0x2FA, 0x2FB, 0x2FC, 0x2FD, 0x000, 0x000, 	/* 8770 */
		0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x2FE, 
		0x2FF, 0x300, 0x301, 0x302, 0x303, 0x304, 0x305, 0x306, 	/* 8780 */
		0x307, 0x308, 0x309, 0x30A, 0x30B, 0x30C, 0x30D, 0x30E, 
		0x30F, 0x310, 0x311, 0x312, 0x313, 0x314, 0x315, 0x316, 	/* 8790 */
		0x317, 0x318, 0x319, 0x31A, 0x31B, 0x000
	};
#else
	static unsigned short half2code[] = {	//半角文字をフォントコードに変換する表。
		0
	};
	static unsigned short kanji2code[]= {	//全角文字をフォントコードに変換する表。
		0
	};
#endif

#ifdef NTSC
	if ( gGetNowCountry() == COUNTRY_JAPAN ) {
		if (( sjisCode >= 0x20 ) && (sjisCode <= 0xdf )) {
			return (int)half2code[sjisCode - 0x20];
		}
		else if ( sjisCode > 0x889e ) {
			int i,j;
			i = ( ( sjisCode >> 8 ) - 0x88 ) * 188;
			j = ( sjisCode & 255 ) - 0x40;
			if ( j >= 0x40 ) j--;
			return (int)( i + j + 0x2BE );
		}
		else if ( sjisCode < 0x879E ) {
			int i,j;
			i = ( ( sjisCode >> 8 ) - 0x81 ) * 188;
			j = ( sjisCode & 255 ) - 0x40;
			if ( j >= 0x40 ) j--;
			return (int)kanji2code[ i+j ];
		}
	}
	else
#endif
	{	// winLatin1
		if (( sjisCode > 0x20 ) && ( sjisCode <= 0xff )) {
			return sjisCode - 0x20;
		} else {
			return 0;
		}
	}
	return 0;
}
