/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: Menu CardAnim関連ルーチン
  -----------------------------------------------------------------------------*/
#include <dolphin.h>

#include "gMath.h"
#include "gBase.h"
#include "gTrans.h"
#include "gCamera.h"
#include "gLoader.h"
#include "gRand.h"

#include "gModelRender.h"

#include "CardSequence.h"
#include "MenuUpdate.h"
#include "MenuCardAnim.h"
#include "CardState.h"

/*-----------------------------------------------------------------------------
  statics
  -----------------------------------------------------------------------------*/
static gmrModel		ccm[36];
static CCAnimState	cca[36];

static s32	now_frame;
static s16	card_rot;

static GXColorS10	cube_color;

static BOOL sConcent;

static void	initCCAnimState( void );

/*-----------------------------------------------------------------------------
  CardCubeがアニメーションするために必要な設定
  -----------------------------------------------------------------------------*/
void
initCCAnim( gmrModelData* md , BOOL first )
{
	s16 i;
	
	if ( first ) {
		for ( i = 0 ; i < 36 ; i++ ) {
			ccm[i].gmrmd = md;
			buildModel( &ccm[i] , FALSE );
		}
	}

	for ( i = 0 ; i < 36 ; i++ ) {
		ccm[i].modelAlpha = 0;
	}
	now_frame = 0;
	card_rot = 0;
	sConcent = FALSE;
	initCCAnimState();
}

/*-----------------------------------------------------------------------------
  CardCubeがアニメーションするための初期値の設定
  -----------------------------------------------------------------------------*/
static void
initCCAnimFrame( void )
{
	s16 i;
	s16	offset;
	
	for ( i = 0 ; i < 36 ; i++ ) {
		offset = (s16)( urand() & 0x1f );
		cca[i].start_frame = (s16)( offset * 1 );
		cca[i].end_frame = (s16)( offset * 1  + 20 );
	}
}
static void
initCCAnimCubeRot( void )
{
	s16 i;
	
	for ( i = 0 ; i < 36 ; i++ ) {
		cca[i].rot_xy = 0;
	}
}
static void
initCCAnimState( void )
{
	s16	i;
	s16	end_offset_x, end_offset_y;

	end_offset_x = -50;
	end_offset_y = 50;
	
	for ( i = 0 ; i < 36 ; i++ ) {
		cca[i].start_x = 0;
		cca[i].start_y = 0;
		cca[i].start_z = 0;

		cca[i].end_x = (s16)( end_offset_x + 20 * ( i % 6 ) );
		cca[i].end_y = (s16)( end_offset_y - 20 * ( i / 6 ) );
		cca[i].end_z = 0;

		cca[i].alpha = 0;
		cca[i].max_alpha = 128;

		cca[i].now_x = cca[i].start_x;
		cca[i].now_y = cca[i].start_y;
		cca[i].now_z = cca[i].start_z;

		now_frame = 0;
	}
	initCCAnimFrame();
	initCCAnimCubeRot();
}

static BOOL
isCCAnimeSmallCube( s16 i )
{
	if ( ( i >=  7 && i <= 10 ) ||
		 ( i >= 13 && i <= 16 ) ||
		 ( i >= 19 && i <= 22 ) ) {
			// 中の分。
		return TRUE;
	}

	return FALSE;
}
static s16
getCCAnimeRotNumber( s16 i )
{
	switch ( i )
	{
	case 0:
		return 0;
		break;
	case 1:
	case 6:
		return 1;
		break;
	case 2:
	case 12:
		return 2;
		break;
	case 3:
	case 18:
		return 3;
		break;
	case 4:
	case 24:
		return 4;
		break;
	case 5:
	case 25:
	case 30:
		return 5;
		break;
	case 11:
	case 26:
	case 31:
		return 6;
		break;
	case 17:
	case 27:
	case 32:
		return 7;
		break;
	case 23:
	case 28:
	case 33:
		return 8;
		break;
	case 29:
	case 34:
		return 9;
		break;
	case 35:
		return 10;
		break;
	}
	return 0;
}

static void
updateCCAnimeAppear( void )
{
	s16 i;
	
	for (i = 0 ; i < 36 ; i++ )	{
		if ( now_frame <= cca[i].start_frame ) {
			cca[i].now_x = cca[i].start_x;
			cca[i].now_y = cca[i].start_y;
			cca[i].now_z = cca[i].start_z;
		}
		if ( now_frame >= cca[i].end_frame ) {
			cca[i].now_x = cca[i].end_x;
			cca[i].now_y = cca[i].end_y;
			cca[i].now_z = cca[i].end_z;
		}

		if ( now_frame > cca[i].start_frame  &&
			 now_frame < cca[i].end_frame ) {
			f32	sx,sy,sf, ex,ey,ef;
			sx = cca[i].start_x;
			sy = cca[i].start_y;
			ex = cca[i].end_x;
			ey = cca[i].end_y;
			sf = cca[i].start_frame;
			ef = cca[i].end_frame;

			cca[i].now_x = (s16)gHermiteInterpolation( (f32)now_frame,
													   sf, sx,
													   (ex-sx)/(ef-sf),
													   ef, ex,
													   0.f );
			cca[i].now_y = (s16)gHermiteInterpolation( (f32)now_frame,
													   sf, sy,
													   (ey-sy)/(ef-sf),
													   ef, ey,
													   0.f );
			cca[i].now_z = 0;
		}
	}
	
	for (i = 0 ; i < 36 ; i++ )	{
		// Alphaの更新
		if ( isCCAnimeSmallCube( i ) ) {
			// 中の分。
		} else {
			if ( now_frame > cca[i].start_frame  &&
				 now_frame < cca[i].end_frame ) {
				ccm[i].modelAlpha = (s16)( 255 * ( now_frame - cca[i].start_frame )
										   / ( cca[i].end_frame - cca[i].start_frame ) );
			}
			else if ( now_frame <= cca[i].start_frame ) {
				ccm[i].modelAlpha = 0;
			}
			else if ( now_frame >= cca[i].end_frame ) {
				ccm[i].modelAlpha = 255;
			}
		}
	}
}

static void
updateCCAnimeCubeRot( s32 frame )
{
	s16 i;
	for (i = 0 ; i < 36 ; i++ )	{
		if ( isCCAnimeSmallCube( i ) ) {
			// 中の分。
		} else {
			if ( frame >= getCCAnimeRotNumber(i) * 3 &&
				 frame <= getCCAnimeRotNumber(i) * 3 + 15 ) {
				cca[i].rot_xy = (s16)( 16384 * ( frame - getCCAnimeRotNumber(i) * 3 ) / 15 );
			}
		}
	}
}

static void
checkOF( s16 *val )
{
	s16 a = *val;
	if ( a > 255 ) a = 255;
	if ( a < 0 ) a = 0;
	*val = a;
}

static void
updateCCAnimeInsideCube( BOOL fadein )
{
	s16	add, val;
	if ( fadein ) {
		add = 30;
		val = 100;
		ccm[7].modelAlpha += add;		checkOF( &ccm[7].modelAlpha );
		if ( ccm[7].modelAlpha > val ) {
			ccm[8].modelAlpha  += add;		checkOF( &ccm[8].modelAlpha );
			ccm[13].modelAlpha += add;		checkOF( &ccm[13].modelAlpha );
		}
		if ( ccm[8].modelAlpha > val ) {
			ccm[9].modelAlpha  += add;		checkOF( &ccm[9].modelAlpha );
			ccm[14].modelAlpha += add;		checkOF( &ccm[14].modelAlpha );
			ccm[19].modelAlpha += add;		checkOF( &ccm[19].modelAlpha );
		}
		if ( ccm[9].modelAlpha > val ) {
			ccm[10].modelAlpha  += add;		checkOF( &ccm[10].modelAlpha );
			ccm[15].modelAlpha += add;		checkOF( &ccm[15].modelAlpha );
			ccm[20].modelAlpha += add;		checkOF( &ccm[20].modelAlpha );
		}
		if ( ccm[10].modelAlpha > val ) {
			ccm[16].modelAlpha += add;		checkOF( &ccm[16].modelAlpha );
			ccm[21].modelAlpha += add;		checkOF( &ccm[21].modelAlpha );
		}
		if ( ccm[16].modelAlpha > val ) {
			ccm[22].modelAlpha += add;		checkOF( &ccm[22].modelAlpha );
		}
	} else {
		add = -15;
		val = 150;
		ccm[7].modelAlpha += add;		checkOF( &ccm[7].modelAlpha );
		ccm[8].modelAlpha  += add;		checkOF( &ccm[8].modelAlpha );
		ccm[13].modelAlpha += add;		checkOF( &ccm[13].modelAlpha );
		ccm[9].modelAlpha  += add;		checkOF( &ccm[9].modelAlpha );
		ccm[14].modelAlpha += add;		checkOF( &ccm[14].modelAlpha );
		ccm[19].modelAlpha += add;		checkOF( &ccm[19].modelAlpha );
		ccm[10].modelAlpha += add;		checkOF( &ccm[10].modelAlpha );
		ccm[15].modelAlpha += add;		checkOF( &ccm[15].modelAlpha );
		ccm[20].modelAlpha += add;		checkOF( &ccm[20].modelAlpha );
		ccm[16].modelAlpha += add;		checkOF( &ccm[16].modelAlpha );
		ccm[21].modelAlpha += add;		checkOF( &ccm[21].modelAlpha );
		ccm[22].modelAlpha += add;		checkOF( &ccm[22].modelAlpha );
		/*
		ccm[7].modelAlpha += add;		checkOF( &ccm[7].modelAlpha );
		if ( ccm[7].modelAlpha < val ) {
			ccm[8].modelAlpha  += add;		checkOF( &ccm[8].modelAlpha );
			ccm[13].modelAlpha += add;		checkOF( &ccm[13].modelAlpha );
		}
		if ( ccm[8].modelAlpha < val ) {
			ccm[9].modelAlpha  += add;		checkOF( &ccm[9].modelAlpha );
			ccm[14].modelAlpha += add;		checkOF( &ccm[14].modelAlpha );
			ccm[19].modelAlpha += add;		checkOF( &ccm[19].modelAlpha );
		}
		if ( ccm[9].modelAlpha < val ) {
			ccm[10].modelAlpha  += add;		checkOF( &ccm[10].modelAlpha );
			ccm[15].modelAlpha += add;		checkOF( &ccm[15].modelAlpha );
			ccm[20].modelAlpha += add;		checkOF( &ccm[20].modelAlpha );
		}
		if ( ccm[10].modelAlpha < val ) {
			ccm[16].modelAlpha += add;		checkOF( &ccm[16].modelAlpha );
			ccm[21].modelAlpha += add;		checkOF( &ccm[21].modelAlpha );
		}
		if ( ccm[16].modelAlpha < val ) {
			ccm[22].modelAlpha += add;		checkOF( &ccm[22].modelAlpha );
		}
		  */
	}

}

static void
updateCCAnimeCardRot( s32 frame )
{
	s16		a,b,c,d,e,f;
	a = 50;
	b = (s16)( a + 40 );
	c = (s16)( b + 50 );
	d = (s16)( c + 50 );
	e = (s16)( d + 40 );
	f = (s16)( e + 50 );
	// フレームは、0 - 200の範囲。
	if ( frame < a ) {
		card_rot = (s16)gHermiteInterpolation( frame,
											   0, 0, 0,
											   a, -8000, 0 );
	} else if ( frame < b ) {
		card_rot = (s16)gHermiteInterpolation( frame,
											   a, -8000, 0,
											   b, 32768, 0 );
	} else if ( frame < c ) {
		card_rot = (s16)32768;
	} else if ( frame < d ) {
		card_rot = (s16)gHermiteInterpolation( frame,
											   c,  32768, 0,
											   d, 32768 + 8000, 0 );
	} else if ( frame < e ) {
		card_rot = (s16)gHermiteInterpolation( frame,
											   d, 32768 + 8000, 0,
											   e, 0, 0 );
	} else if ( frame < f ) {
		card_rot = 0;
	}

}

void
updateCCAnime( void )
{
	CardState* csp = getCardState();
	//	SlotState*	slp = getCardSlotState();
	if ( now_frame == 0 ) {
		initCCAnimFrame();
	}

	if ( now_frame <= 31 * 3 + 20 ) updateCCAnimeAppear();

	if ( now_frame >= 150 && now_frame <= 150 + 10 * 3 + 15 ) {
			updateCCAnimeCubeRot( now_frame - 150 );
	} else if ( now_frame >= 210 + 90  && now_frame <= 210 + 90 + 10 * 3 + 15 ) {
		updateCCAnimeCubeRot( now_frame - 210 - 90 );
	} else 	if ( now_frame >= 210 + 230 && now_frame <= 210 + 230 + 10 * 3 + 15 ) {
		updateCCAnimeCubeRot( now_frame - 210 - 220 );
	} else {
		initCCAnimCubeRot();
	}

	if ( now_frame >= 340 ) {
		updateCCAnimeInsideCube( TRUE );
	} else {
		updateCCAnimeInsideCube( FALSE );
	}

	if ( now_frame >= 210 && now_frame <= 210 + 280 ) {
		updateCCAnimeCardRot( now_frame - 210 );
	}

	now_frame++;
	if ( now_frame > 210 + 280 ) now_frame = 210;
}

void
updateCCAnimeMtx( void )
{
	s16	i;
	Mtx currentMtx, rot_m, r;
	CardState*	csp = getCardState();
	gGetTranslateRotateMtx(	16384,0,0,0,0,0,r );

		for (i = 0 ; i < 36 ; i++ )	{
//			gGetRotateMtx( cca[i]
			gGetTranslateRotateMtx(cca[i].rot_xy,
								   cca[i].rot_xy,
								   0,
								   cca[i].now_x,
								   cca[i].now_y,
								   cca[i].now_z,
								   currentMtx );
			gGetRotateMtx( 0, card_rot, 0, rot_m );
			MTXConcat( rot_m, currentMtx, currentMtx);
			MTXConcat( r, currentMtx, currentMtx);
			MTXConcat(  getMainMenuMtx() ,currentMtx, currentMtx );
			if ( isCCAnimeSmallCube( i ) ) {
				// 中の分。
				setBaseMtx( &ccm[i] , currentMtx ,
							(Vec){
								csp->scale / 2,//.8f,
								csp->scale / 2,//.8f,
								csp->scale / 2//.8f,
									} );
			} else {
				setBaseMtx( &ccm[i] , currentMtx ,
							(Vec){
								csp->scale,//.8f,
								csp->scale,//.8f,
								csp->scale //.8f,
									} );
			}
			setViewMtx( &ccm[i] , getCurrentCamera() );
			updateModel( &ccm[i] );
		}
}

void
drawCCAnime( void )
{
	s16	i;
	CardState*	csp = getCardState();
	GXColorS10*	backup;

	cube_color.r = (s16)csp->cube_r;
	cube_color.g = (s16)csp->cube_g;
	cube_color.b = (s16)csp->cube_b;
	cube_color.a = (s16)csp->cube_a;

	backup = ccm[0].gmrmd->material[0].tevColorPtr[0];
	ccm[0].gmrmd->material[0].tevColorPtr[0] = &cube_color;


	for ( i = 0 ; i < 36 ; i++ ) {
		if ( ccm[i].modelAlpha > 0 ) {
			drawModel( &ccm[i] );
		}
	}

	ccm[0].gmrmd->material[0].tevColorPtr[0] = backup;
}

void
clearCCAnime( void )
{
	int i;
	int all_alpha = 0;
	
	for ( i = 0 ; i < 36 ; i++ ) {
		ccm[i].modelAlpha -= 10;
		if ( ccm[i].modelAlpha < 0 ) {
			ccm[i].modelAlpha = 0;
			all_alpha++;
		}
	}

//	if ( all_alpha == 36 ) {
		now_frame = 0;
		card_rot = 0;
//	}
}

static BOOL
decPosition( s16 *xp )
{
	s16	x = *xp;
	CardState*	csp = getCardState();

	if ( x > 0 ) {
		x -= csp->concent_dec_pos;
		if ( x < 0 ) x = 0;
	} else if ( x < 0 ) {
		x += csp->concent_dec_pos;
		if ( x > 0 ) x = 0;
	}

	*xp = x;
	if ( x == 0 ) return TRUE;
	return FALSE;
}

void
concentCCAnime( void )
{
	int i;
	int all_pos = 0;
	CardState*	csp = getCardState();

	for ( i = 0 ; i < 36 ; i++ ) {
		ccm[i].modelAlpha -= csp->concent_dec_alpha;
		if ( ccm[i].modelAlpha < 0 ) {
			ccm[i].modelAlpha = 0;
		}
		if ( decPosition( &cca[i].now_x ) && ccm[i].modelAlpha == 0 ) all_pos++;
		if ( decPosition( &cca[i].now_y ) && ccm[i].modelAlpha == 0 ) all_pos++;
		if ( decPosition( &cca[i].now_z ) && ccm[i].modelAlpha == 0 ) all_pos++;
	}

	if ( all_pos == 36*3 ) {
		sConcent = TRUE;
	} else {
		sConcent = FALSE;
	}
	
	now_frame = 0;
	card_rot = 0;
}

BOOL
isConcentCCSmallCube( void )
{
	return sConcent;
}
