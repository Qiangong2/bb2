#include <dolphin.h>
#include <string.h>
#include "smallCubeFaderOffsetEffect.h"
#include "OptionUpdate.h"
#include "smallCubeFader.h"
#include "smallCubeAnimation.h"
#include "MenuUpdate.h"

enum
{
    OFFSETSTATE_NONE,
    OFFSETSTATE_FADEIN,
    OFFSETSTATE_FADEOUT,
    OFFSETSTATE_NUM
};
    

typedef struct optionOffsetLineState
{
    s16 offsetstate;                // ここで扱う状態。OFFSETSTATE_*が入る
    u16 frame;                 // アニメーションフレーム
}optionOffsetLineState;
#define FADEINFRAMENUM getMenuState()->scf_offsetfadeinframe
#define FADEOUTFRAMENUM getMenuState()->scf_offsetfadeoutframe

static optionOffsetLineState sLineState;

#define EFFECTCUBENUM 100
J3DTransformInfo sEffectPos[EFFECTCUBENUM];

/*---------------------------------------------------------------------------*
  Name:
  initOptionOffsetEffect(void)
  
  Description:
  オプション面に入ったら毎回呼ばれる。
              
              
 *---------------------------------------------------------------------------*/
void
initOptionOffsetEffect(void)
{
    sLineState.offsetstate = OFFSETSTATE_NONE;
    sLineState.frame = 0;
    memset( sEffectPos, 0, sizeof(sEffectPos));
}

void
updateOptionOffsetEffect()
{
    sLineState.frame ++;

    switch( sLineState.offsetstate ){
    case OFFSETSTATE_FADEIN:
	if( sLineState.frame >= FADEINFRAMENUM ){
	    sLineState.frame = (u16)FADEINFRAMENUM;
	}
	break;
    case OFFSETSTATE_FADEOUT:
	if( sLineState.frame >= FADEOUTFRAMENUM ){
	    sLineState.frame = 0;
	    sLineState.offsetstate = OFFSETSTATE_NONE;
	}
	break;
    }

    if( scfGetState()->optionrunstate != scfGetPrevState()->optionrunstate ){
	switch( scfGetState()->optionrunstate ){
	case IPL_OPTION_OFFSET: // オフセットを選んだ
	    sLineState.offsetstate = OFFSETSTATE_FADEIN;
	    sLineState.frame = 0;
	    break;
	case IPL_OPTION_SELECTING:
	    if( scfGetPrevState()->optionrunstate == IPL_OPTION_OFFSET ){
		sLineState.offsetstate = OFFSETSTATE_FADEOUT;
		sLineState.frame = 0;
	    }else{
		sLineState.offsetstate = OFFSETSTATE_NONE;
	    }
	    break;
	default:
	    sLineState.offsetstate = OFFSETSTATE_NONE;
	    break;
	}
    }


}


J3DTransformInfo* getFaderEffectPos(void)
{
    u32 dummy;

    
    switch( sLineState.offsetstate ){
    case OFFSETSTATE_FADEIN:
	getAnimCubePos( SCA_ANIMKIND_OPTIONLINEFADEIN,
			(f32)sLineState.frame/FADEINFRAMENUM,
			sEffectPos, &dummy );
	break;
    case OFFSETSTATE_FADEOUT:
	getAnimCubePos( SCA_ANIMKIND_OPTIONLINEFADEOUT,
			1.0f-(f32)sLineState.frame/FADEOUTFRAMENUM,
			sEffectPos, &dummy );
	break;
    }
    
    return sEffectPos;
}

u32 getFaderEffectNum(void)
{
    switch( sLineState.offsetstate ){
    case OFFSETSTATE_FADEIN:
	return getAnimCubeNum( SCA_ANIMKIND_OPTIONLINEFADEIN );
    case OFFSETSTATE_FADEOUT:
	return getAnimCubeNum( SCA_ANIMKIND_OPTIONLINEFADEOUT );
    case OFFSETSTATE_NONE:
	return 0;
    }
    return 0;
}

u8 getFaderEffectAlpha(void)
{
    switch( sLineState.offsetstate ){
    case OFFSETSTATE_FADEIN:
	return (u8)(255*(f32)sLineState.frame/FADEINFRAMENUM );
    case OFFSETSTATE_FADEOUT:
	return (u8)(255 - 255*(f32)sLineState.frame/FADEOUTFRAMENUM );
    default:
	return 0;
    }
    return 0;
}
