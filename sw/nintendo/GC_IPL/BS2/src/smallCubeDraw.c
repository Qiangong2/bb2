/*---------------------------------------------------------------------------*
  File:    smallCubeDraw.c
  Copyright 2000 Nintendo.  All rights reserved.

  小さいキューブの描画とアップデート

 *---------------------------------------------------------------------------*/
#include "smallCubeFader.h"
#include "smallCubeDraw.h"
#include "gCamera.h"

#include "smallCubeTune.h"
#include "MenuUpdate.h"
#include "gTrans.h"
#include "gLoader.h"
#include "gCont.h"

static gmrModelData cubeModelData;
static gmrModel		cubeModels[SCF_CUBENUM];

static sctSortInfo	cube_before[SCF_CUBENUM];
static sctSortInfo	cube_after [SCF_CUBENUM];



void initSmallCubeDraw(void)
{
	u32 i;

	// モデルを作る
	cubeModelData.modeldata = gGetBufferPtr( GC_moji_cube_bmd );
	buildModelData( &cubeModelData );
//	dumpTree( cubeModelData.root );

	for ( i = 0 ; i < SCF_CUBENUM ; i++ ) {
		cubeModels[i].gmrmd = &cubeModelData;
		buildModel( &cubeModels[i], FALSE );
	}

}


/*---------------------------------------------------------------------------*
  Name:
  void drawSmallCubeDraw( u8 displayalpha )

  Description:
  １ループに２回呼ばれる描画関数。

 *---------------------------------------------------------------------------*/
void drawSmallCubeDraw( u8 displayalpha )
{
	u32 i;
	static u32	time = 0;
	f32	y_offset, y_mergin;
	GXColorS10 cs10_1, cs10_2, *backup1, *backup2;
	s16 num = 0;
	GXColor *cubecolor1, *cubecolor2;

	u8* cubealpha = getSmallCubeAlpha();
	u32 cubenum = getSmallCubeNum();

	cubecolor1 = getSmallCubeColor1();
	cubecolor2 = getSmallCubeColor2();

	y_mergin = getMenuState()->y_sort_mergin;
	y_offset = getMenuState()->y_sort_offset;

	num = 0;
	for( i=0;i<cubenum;i++ ){

	cs10_1.r = cubecolor1[i].r;
	cs10_1.g = cubecolor1[i].g;
	cs10_1.b = cubecolor1[i].b;
	cs10_1.a = 255;

	cs10_2.r = cubecolor2[i].r;
	cs10_2.g = cubecolor2[i].g;
	cs10_2.b = cubecolor2[i].b;
	cs10_2.a = 255;

	backup1 = cubeModels[i].gmrmd->material[0].tevColorPtr[0];
	backup2 = cubeModels[i].gmrmd->material[0].tevColorPtr[1];
	cubeModels[i].gmrmd->material[0].tevColorPtr[0] = &cs10_1;
	cubeModels[i].gmrmd->material[0].tevColorPtr[1] = &cs10_2;

	cubeModels[i].modelAlpha = (u8)(displayalpha * cubealpha[i] *cubecolor1[i].a
		/(255*255)); // cubecolor2は使わない.分子がでかすぎると計算結果が255にならない
	//	cubeModels[i].modelAlpha = 255;
	if( cubeModels[i].modelAlpha > 10 ){
		if ( (time == 0) && (cube_before[i].y > (y_mergin - y_offset) ) ) {
			if ( num == 0 ) {
					drawModelNode( &cubeModels[i] , &cubeModels[i].gmrmd->root[1] );
					num ++;
			}
			else {
					drawModelNodeSmallCube( &cubeModels[i] , &cubeModels[i].gmrmd->root[1] );
			}
		}
		if ( (time == 1) && (cube_before[i].y < (y_mergin + y_offset) ) ) {
			if ( num == 0 ) {
					drawModelNode( &cubeModels[i] , &cubeModels[i].gmrmd->root[1] );
					num ++;
			}
			else {
					drawModelNodeSmallCube( &cubeModels[i] , &cubeModels[i].gmrmd->root[1] );
			}
		}
	}

	cubeModels[i].gmrmd->material[0].tevColorPtr[0] = backup1;
	cubeModels[i].gmrmd->material[0].tevColorPtr[1] = backup2;


	}

//	if ( time == 0 ) DIReport("top   :%03d\n",num );
//	if ( time == 1 ) DIReport("bottom:%03d\n",num );

	time ^= 1;


}




void updateSmallCubeDraw(void)
{

	u32 i;
	Mtx m;

	u32 cubenum = getSmallCubeNum();
	J3DTransformInfo* cubepos = getSmallCubePos();

	for( i=0;i<cubenum;i++ ){
		Vec scale;

		gGetTranslateRotateMtx( cubepos[i].rx, cubepos[i].ry, cubepos[i].rz,
				   cubepos[i].tx, cubepos[i].ty, cubepos[i].tz,
					m);

		cube_before[i].y = cubepos[i].ty;

		scale.x = cubepos[i].sx;
		scale.y = cubepos[i].sy;
		scale.z = cubepos[i].sz;

		setBaseMtx( &cubeModels[i], m, scale );
		setViewMtx( &cubeModels[i], gGetSmallCubeCamera() );
		updateModel( &cubeModels[i] );
	}

}
