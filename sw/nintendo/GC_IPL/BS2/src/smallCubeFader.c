/*---------------------------------------------------------------------------*
  File:    smallCubeFader.c

  Copyright 2000 Nintendo.  All rights reserved.

  小さいキューブでの文字表記を作る。
  詳細はsmallCubeFader.h。

 *---------------------------------------------------------------------------*/

#include <dolphin.h>
#include <string.h>
#include <math.h>

#include "smallCubeAnimation.h"
#include "smallCubeFader.h"
#include "smallCubeFaderData.h"
#include "smallCubeFaderOffsetEffect.h"
#include "smallCubeFaderNumberEffect.h"
#include "smallCubeFaderStringEffect.h"
#include "smallCubeMovement.h"
#include "gMRStruct.h"
#include "MenuUpdate.h"
#include "TimeUpdate.h"
#include "OptionUpdate.h"
#include "gLayoutRender.h"
#include "gFont.h"
#include "gMath.h"
#include "gTrans.h"
#include "gRand.h"
#include "OptionUpdate.h"
#include "gLoader.h"


#define PAL_ACTIVE_CHANGE

/*---------------------------------------------------------------------------*
  状態が変わったとき、急に場所などが変わるのを防ぐのに
  使う変数をまとめる
 *---------------------------------------------------------------------------*/
typedef struct faderFrameCtrl
{
	s16 selectz[SCF_CUBENUM]; // 選ばれていることを示す、飛び出ぐあい
	s16 selectz_dest[SCF_CUBENUM];

	s16 charalpha[SCF_CUBENUM]; // 文字を作るためのアルファ。
	s16 charalpha_dest[SCF_CUBENUM];

	s16 selectalpha[SCF_CUBENUM]; // 選ばれているもの以外を暗くする
	s16 selectalpha_dest[SCF_CUBENUM];

	s16 selectcolor[SCF_CUBENUM]; // 選ばれてる色か、そうでない色か。
	s16 selectcolor_dest[SCF_CUBENUM]; // 選ばれてる色か、そうでない色か。

	s16 dance[SCF_CUBENUM]; // 踊るキューブ
	s16 dance_dest[SCF_CUBENUM];

	s16 mainanim; // メインの小キューブアニメーション
	s16 mainanim_dest;

}faderFrameCtrl;



static faderFrameCtrl       framectrl;
static smallCubeFaderState param;
static smallCubeFaderState prevparam;
static u16 sArrowAnim;
#define SCF_STARTFADEOUTANIMNUM 5
static u32  sStartFadeoutAnimKind;
Vec              selectPosOffset;
Vec              noSelectPosOffset;

#define LANGSELECTFRAME 10
struct LangSelectFade{
	s16 framenow;
	f32 old_x;
	f32 dest_x;
}sLangSelectFade;


static J3DTransformInfo cubePos[SCF_CUBENUM];
static u8               cubeAlpha[SCF_CUBENUM];
static GXColor          cubeColor1[SCF_CUBENUM];
static GXColor          cubeColor2[SCF_CUBENUM];
static GXColor          sMenuColor1;
static GXColor          sMenuColor2;
static GXColor          sDarkMenuColor1;
static GXColor          sDarkMenuColor2;
static GXColor          sDarkerMenuColor1;
static GXColor          sDarkerMenuColor2;
static u32 cubeNum;
// static 関数のプロトタイプ

static void applyFrameCtrl(void);
static void updateFader(void);
static void updateFaderValue( s16 *val, s16 dest );

static void setMainState(void);
static void setStartState( s16 runstate, s16 start_nodisk_question );
static void setOptionState( s16 runstate, s16 select_curstate, s16 offset_curstate,
							s16 sound_curstate );
static void setTimeState( s16 runstate, s16 select_curstate, s16 day_curstate, s16 clock_curstate, int edit_num[] );

static void setOptionSelectZFrameCtrl(void);
static void setOptionDanceFrameCtrl(void);
static void setOptionCharAlphaFrameCtrl(void);
static void setOptionSelectAlphaFrameCtrl(void);

static void setTimeCharAlphaFrameCtrl(void);
static void setTimeSelectAlphaFrameCtrl(void);

static void setStartAllFrameCtrl(void); // 単純なので一個の関数で。

static void scaleAround( f32 orgx, f32 orgy, f32 orgz,
						 f32 sx, f32 sy, f32 sz,   J3DTransformInfo *pos );

static void setSmallArrowFade( u32 id, u32 true_false );
static f32 getLangSelectXOffset(void);

#ifdef PAL
static void langCursorMove( s32 oldlang, s32 newlang );
static void langChanged( s32 oldlang, s32 newlang );
#endif

static f32 getSZFade( int i ); // get Select Z Fade
static f32 getSZDest( int i );
static f32 getCAFade( int i ); // get Char Alpha Fade
static f32 getSAFade( int i ); // get Select Alpha Fade
static f32 getDFade( int i );  // get Dance Fade
static f32 getSCFade( int i );
static f32 getMAFade(void);    // get Main Anim Fade
static f32 getTuneFade(void);

static void setSZFade( int i, int on_off );
static void setCAFade( int i, int on_off );
static void setSAFade( int i, int on_off );
static void setDFade( int i, int on_off );
static void setSCFade( int i, int on_off );

static void forceSZFade( int i, int on_off );
static void forceDFade( int i, int on_off );
static void forceSAFade( int i, int on_off );
static void forceCAFade( int i, int on_off );


static void tuneOptionCubePos(void);
static void tuneTimeCubePos(void);
static void tuneStartCubePos(void);

static void setSmallArrowPos( void );

static f32 getArrowFrame(void);

static void setEffectColor(void);

static void getStartFadeoutAnim( f32 frame, J3DTransformInfo *pos, u32 *cubenum );
static void blend3Color( GXColor *c1, GXColor *c2, GXColor *c3, f32 s, f32 t, GXColor *dest );

#ifdef PAL

#define SOUNDZONENUM 640
static struct LangSelectEffectParam
{
	s16 oldlang;
	s16 newlang;
	s16 frame;
	Vec dir;
	u8 randompos[SOUNDZONENUM];
}sLangEffect;

void initLangSelectEffect( s32 lang );
void startLangSelectEffect( s32 oldlang, s32 newlang );
void makeLangSelectEffect(void);
void makeLangSelectOffsetEffect(void);
#endif


/*
 *
 *
 * extern関数の定義
 *
 *
 */



J3DTransformInfo* getSmallCubePos(void)
{
	return cubePos;
}

u8*               getSmallCubeAlpha(void)
{
	return cubeAlpha;
}

u32               getSmallCubeNum(void)
{
	return cubeNum;
}

GXColor* getSmallCubeColor1(void)
{
	return cubeColor1;
}
GXColor* getSmallCubeColor2(void)
{
	return cubeColor2;
}

u32
isCubeAnimFinished( u32 frame )
{
	if( framectrl.mainanim < getMenuState()->scf_mainanimframenum - frame ){
		return 0;
	}else{
		return 1;
	}
}


/*---------------------------------------------------------------------------*
  Name:
  void initSmallCubeFader( void* animdata)

  Description:
  初期化。プログラム開始時に一回呼ばれる。
  リセットがかかったときも、これを呼べばOK。


 *---------------------------------------------------------------------------*/
void initSmallCubeFader( void* faderdata, void* animdata )
{

	memset( &param, 0, sizeof(param));
	memset( &framectrl, 0, sizeof(framectrl));
	memset( &sLangSelectFade, 0, sizeof(sLangSelectFade) );
	param.state = SCFS_MAIN;
	prevparam = param;
	initSmallCubeFaderData( faderdata );
	initCubeAnimation( animdata );

}




/*---------------------------------------------------------------------------*
  Name:
  void updateMainSmallCube(void)

  Description:
  どのメニューも選ばれていないときに呼ばれる。


 *---------------------------------------------------------------------------*/
void updateMainSmallCube(void)
{
	setMainState();
	memset( &framectrl,0,sizeof(framectrl));
	memset( &sLangSelectFade, 0, sizeof(sLangSelectFade) );
}



/*---------------------------------------------------------------------------*
  Name:
  void updateOptionSmallCube( u32 state, s16 select_curstate,
				 s16 offset_curstate, s16 sound_curstate )

  Description:
  オプションが選ばれているときに呼ばれる。

 *---------------------------------------------------------------------------*/
void updateOptionSmallCube( u32 state, s16 select_curstate,
							s16 offset_curstate, s16 sound_curstate )
{

	u32 i;

#ifdef PAL
	static s32 langcursor, langselect;
	if( langcursor != getLanguageCurstate() ) langCursorMove( langcursor, getLanguageCurstate() );
	langcursor = getLanguageCurstate();

	if( langselect != gGetPalCountry() ) langChanged( langselect, gGetPalCountry() );
	langselect = gGetPalCountry();

#endif // PAL

	setOptionState( (s16)state, select_curstate,
					offset_curstate, sound_curstate );

	if( prevparam.state != SCFS_OPTION ){
		initOptionOffsetEffect();
		scfInitCubeMovement();
		memset( &framectrl, 0, sizeof(faderFrameCtrl) );
		memset( &sLangSelectFade, 0, sizeof(sLangSelectFade) );
		framectrl.mainanim_dest = getMenuState()->scf_mainanimframenum;
		sArrowAnim = 0;
#ifdef PAL		
		langcursor = getLanguageCurstate();
		langCursorMove( langcursor, langcursor );
		initLangSelectEffect( getLanguageCurstate() );
#endif
	}

	
	sMenuColor1.r = (u8)getMenuState()->scf_cubecolor_option_r1;
	sMenuColor1.g = (u8)getMenuState()->scf_cubecolor_option_g1;
	sMenuColor1.b = (u8)getMenuState()->scf_cubecolor_option_b1;
	sMenuColor1.a = 255;
	sMenuColor2.r = (u8)getMenuState()->scf_cubecolor_option_r2;
	sMenuColor2.g = (u8)getMenuState()->scf_cubecolor_option_g2;
	sMenuColor2.b = (u8)getMenuState()->scf_cubecolor_option_b2;
	sMenuColor2.a = 255;
	sDarkMenuColor1.r = (u8)getMenuState()->scf_darkcubecolor_option_r1;
	sDarkMenuColor1.g = (u8)getMenuState()->scf_darkcubecolor_option_g1;
	sDarkMenuColor1.b = (u8)getMenuState()->scf_darkcubecolor_option_b1;
	sDarkMenuColor1.a = 255;
	sDarkMenuColor2.r = (u8)getMenuState()->scf_darkcubecolor_option_r2;
	sDarkMenuColor2.g = (u8)getMenuState()->scf_darkcubecolor_option_g2;
	sDarkMenuColor2.b = (u8)getMenuState()->scf_darkcubecolor_option_b2;
	sDarkMenuColor2.a = 255;
	sDarkerMenuColor1.r = (u8)getMenuState()->scf_darkercubecolor_option_r1;
	sDarkerMenuColor1.g = (u8)getMenuState()->scf_darkercubecolor_option_g1;
	sDarkerMenuColor1.b = (u8)getMenuState()->scf_darkercubecolor_option_b1;
	sDarkerMenuColor1.a = 255;
	sDarkerMenuColor2.r = (u8)getMenuState()->scf_darkercubecolor_option_r2;
	sDarkerMenuColor2.g = (u8)getMenuState()->scf_darkercubecolor_option_g2;
	sDarkerMenuColor2.b = (u8)getMenuState()->scf_darkercubecolor_option_b2;
	sDarkerMenuColor2.a = 255;


	// キューブの色を決める
	setEffectColor();
	setOptionCharAlphaFrameCtrl();
	setOptionSelectAlphaFrameCtrl();
	setOptionSelectZFrameCtrl();
	setOptionDanceFrameCtrl();

	// キューブの場所を決める
	getAnimCubePos( SCA_ANIMKIND_OPTION, getMAFade(), cubePos, &cubeNum );
	setSmallArrowPos(); // サウンドやオフセットの場所を見るので、こっちが先。
	tuneOptionCubePos();

	if( getMAFade() > getMenuState()->scf_startdancetime ) scfUpdateOptionCubeMovement( cubePos );

	applyFrameCtrl();

	// 画面位置のエフェクトを付加
	updateOptionOffsetEffect();

	ASSERT( 0<=cubeNum && cubeNum < sizeof(cubePos)/sizeof(cubePos[0]) );
	memcpy( &cubePos[cubeNum], getFaderEffectPos(),
			sizeof(J3DTransformInfo)*getFaderEffectNum() );
	for( i=cubeNum;i<(u32)(cubeNum+(u32)getFaderEffectNum());i++ ){
		ASSERT( 0<=i && i < sizeof(cubeAlpha)/sizeof(cubeAlpha[0]) );
		cubeAlpha[i] = getFaderEffectAlpha();
	}
	cubeNum += getFaderEffectNum();

	// 数字の変化のエフェクトを付加
	scfUpdateOptionNumberEffect( cubePos );

	ASSERT( 0<=cubeNum && cubeNum < sizeof(cubePos)/sizeof(cubePos[0]) );
	memcpy( &cubePos[cubeNum], scfGetNumberEffectPos(),
			sizeof(J3DTransformInfo)*scfGetNumberEffectNum() );
	memcpy( &cubeAlpha[cubeNum], scfGetNumberEffectAlpha(),
			sizeof(u8)*scfGetNumberEffectNum() );
	cubeNum += scfGetNumberEffectNum();
	if( param.optionrunstate == IPL_OPTION_OFFSET
			&& prevparam.optionrunstate != IPL_OPTION_OFFSET ){
		scfInvalidateOptionNumberEffect();
	}

	// ステレオモノラルのエフェクトを付加
	scfUpdateOptionStringEffect( cubePos );
	memcpy( &cubePos[cubeNum], scfGetStringEffectPos(),
			sizeof(J3DTransformInfo)*scfGetStringEffectNum() );
	memcpy( &cubeAlpha[cubeNum], scfGetStringEffectAlpha(),
			sizeof(u8)*scfGetStringEffectNum() );
	cubeNum += scfGetStringEffectNum();
	if( param.optionrunstate == IPL_OPTION_SOUND
			&& prevparam.optionrunstate != IPL_OPTION_SOUND ){
		scfInvalidateStringEffect();
	}

#ifdef PAL
	if( param.optionrunstate == IPL_OPTION_LANGUAGE )	makeLangSelectEffect();	
#endif

	updateFader();
	sArrowAnim ++;
	if( sArrowAnim >= getMenuState()->scf_arrowanimframe ) sArrowAnim = 0;

}






void updateTimeSmallCube( u32 state, s32 select_curstate,
						  s32 day_curstate, s32 clock_curstate,
						  int edit_num[] )
{

	setTimeState( (s16)state, (s16)select_curstate, (s16)day_curstate,
				  (s16)clock_curstate, edit_num  );

	if( prevparam.state != SCFS_TIME ){
		memset( &framectrl, 0, sizeof(faderFrameCtrl) );
		memset( &sLangSelectFade, 0, sizeof(sLangSelectFade) );
		framectrl.mainanim_dest = getMenuState()->scf_mainanimframenum;
		scfInitCubeMovement();
	}

	sMenuColor1.r = (u8)getMenuState()->scf_cubecolor_calendar_r1;
	sMenuColor1.g = (u8)getMenuState()->scf_cubecolor_calendar_g1;
	sMenuColor1.b = (u8)getMenuState()->scf_cubecolor_calendar_b1;
	sMenuColor1.a = 255;
	sMenuColor2.r = (u8)getMenuState()->scf_cubecolor_calendar_r2;
	sMenuColor2.g = (u8)getMenuState()->scf_cubecolor_calendar_g2;
	sMenuColor2.b = (u8)getMenuState()->scf_cubecolor_calendar_b2;
	sMenuColor2.a = 255;
	sDarkMenuColor1.r = (u8)getMenuState()->scf_darkcubecolor_calendar_r1;
	sDarkMenuColor1.g = (u8)getMenuState()->scf_darkcubecolor_calendar_g1;
	sDarkMenuColor1.b = (u8)getMenuState()->scf_darkcubecolor_calendar_b1;
	sDarkMenuColor1.a = 255;
	sDarkMenuColor2.r = (u8)getMenuState()->scf_darkcubecolor_calendar_r2;
	sDarkMenuColor2.g = (u8)getMenuState()->scf_darkcubecolor_calendar_g2;
	sDarkMenuColor2.b = (u8)getMenuState()->scf_darkcubecolor_calendar_b2;
	sDarkMenuColor2.a = 255;
	sDarkerMenuColor1.r = (u8)getMenuState()->scf_darkercubecolor_calendar_r1;
	sDarkerMenuColor1.g = (u8)getMenuState()->scf_darkercubecolor_calendar_g1;
	sDarkerMenuColor1.b = (u8)getMenuState()->scf_darkercubecolor_calendar_b1;
	sDarkerMenuColor1.a = 255;
	sDarkerMenuColor2.r = (u8)getMenuState()->scf_darkercubecolor_calendar_r2;
	sDarkerMenuColor2.g = (u8)getMenuState()->scf_darkercubecolor_calendar_g2;
	sDarkerMenuColor2.b = (u8)getMenuState()->scf_darkercubecolor_calendar_b2;
	sDarkerMenuColor2.a = 255;


	setEffectColor();

	setTimeCharAlphaFrameCtrl();
	setTimeSelectAlphaFrameCtrl();

	getAnimCubePos( SCA_ANIMKIND_CALENDAR,
					getMAFade(), cubePos, &cubeNum );

	tuneTimeCubePos();

	if( getMAFade() > getMenuState()->scf_startdancetime ) scfUpdateTimeCubeMovement( cubePos );

	applyFrameCtrl();

	// 数字の変化のエフェクトを付加
	scfUpdateTimeNumberEffect( cubePos );
	memcpy( &cubePos[cubeNum], scfGetNumberEffectPos(),
			sizeof(J3DTransformInfo)*scfGetNumberEffectNum() );
	memcpy( &cubeAlpha[cubeNum], scfGetNumberEffectAlpha(),
			sizeof(u8)*scfGetNumberEffectNum() );
	cubeNum += scfGetNumberEffectNum();


	if( param.timerunstate != IPL_TIME_SELECTING
			&& prevparam.timerunstate == IPL_TIME_SELECTING ){
		scfInvalidateOptionNumberEffect();
	}


	updateFader();

}

void updateStartSmallCube( u32 state, u32 start_nodisk_question )
{
	setStartState( (s16)state, (s16)start_nodisk_question );

	if( prevparam.state != SCFS_START ){
		int i;
		memset( &framectrl, 0, sizeof(faderFrameCtrl) );
		memset( &sLangSelectFade, 0, sizeof(sLangSelectFade) );
		

		for( i=0;i<SCF_CUBENUM;i++ ){
			cubeAlpha[i] = 255;
			setDFade( i, TRUE ); // ぜんぶ動いてる
			setSAFade( i, TRUE );
			forceCAFade( i, TRUE );
		}

		scfInitCubeMovement();

		framectrl.mainanim_dest = getMenuState()->scf_mainanimframenum;

	}

	sMenuColor1.r = sDarkMenuColor1.r = sDarkerMenuColor1.r = (u8)getMenuState()->scf_cubecolor_start_r1;
	sMenuColor1.g = sDarkMenuColor1.g = sDarkerMenuColor1.g = (u8)getMenuState()->scf_cubecolor_start_g1;
	sMenuColor1.b = sDarkMenuColor1.b = sDarkerMenuColor1.b = (u8)getMenuState()->scf_cubecolor_start_b1;
	sMenuColor1.a = 255;
	sMenuColor2.r = sDarkMenuColor2.r = sDarkerMenuColor2.r = (u8)getMenuState()->scf_cubecolor_start_r2;
	sMenuColor2.g = sDarkMenuColor2.g = sDarkerMenuColor2.g = (u8)getMenuState()->scf_cubecolor_start_g2;
	sMenuColor2.b = sDarkMenuColor2.b = sDarkerMenuColor2.b = (u8)getMenuState()->scf_cubecolor_start_b2;
	sMenuColor2.a = 255;

	if( getMenuState()->scf_handframe >= 0 ) framectrl.mainanim = getMenuState()->scf_handframe;


	if( param.startrunstate == FALSE ){
		getAnimCubePos( SCA_ANIMKIND_GAMEPLAY,getMAFade(), cubePos, &cubeNum );
		tuneStartCubePos();
	}else{
		if( prevparam.startrunstate == FALSE ){ //変わった瞬間だけ初期化
			framectrl.mainanim = 0;
			framectrl.mainanim_dest = getMenuState()->scf_mainanimframenum;
			sStartFadeoutAnimKind = urand()%SCF_STARTFADEOUTANIMNUM;
		}
		getStartFadeoutAnim( getMAFade(), cubePos, &cubeNum );
		tuneStartCubePos();
	}

	setEffectColor();

	setStartAllFrameCtrl();

	if( getMAFade() > getMenuState()->scf_startdancetime ){
		scfUpdateStartCubeMovement( cubePos, cubeNum );
	}

	applyFrameCtrl();

#if 1
	// エフェクトを付加
	scfUpdateStartStringEffect( cubePos );
	memcpy( &cubePos[cubeNum], scfGetStringEffectPos(),
			sizeof(J3DTransformInfo)*scfGetStringEffectNum() );
	memcpy( &cubeAlpha[cubeNum], scfGetStringEffectAlpha(),
			sizeof(u8)*scfGetStringEffectNum() );
	cubeNum += scfGetStringEffectNum();
#endif

	updateFader();

}













/*---------------------------------------------------------------------------*
  Name:
  void scfGetTimeNumberToDisplay( s16 numbers[] )

  Description:
  カレンダー面で、画面に表示すべき数字を、年月日時分秒の順番でnumbers[]に入れる。

 *---------------------------------------------------------------------------*/
void scfGetTimeNumberToDisplay( s16 numbers[], s16 timerunstate, s16 timeeditnum[] )
{
	OSCalendarTime	ostime;

	int	y, m, d, h, i, s;

	OSTicksToCalendarTime( OSGetTime(), &ostime );
	y = ostime.year;
	m = ostime.mon;
	d = ostime.mday;
	h = ostime.hour;
	i = ostime.min;
	s = ostime.sec;

	if ( timerunstate == IPL_TIME_SELECTING ) {
		// なにもしない
	}else if ( timerunstate == IPL_TIME_DAY ) {
		y = timeeditnum[0];
		m = timeeditnum[1];
		d = timeeditnum[2];
	}else if ( timerunstate == IPL_TIME_CLOCK ) {
		h = timeeditnum[0];
		i = timeeditnum[1];
		s = timeeditnum[2];
	}else{
		ASSERTMSG( 0, "invalid state\n" );
	}

	numbers[0] = (s16)y;
	numbers[1] = (s16)(m+1);
	numbers[2] = (s16)d;
	numbers[3] = (s16)h;
	numbers[4] = (s16)i;
	numbers[5] = (s16)s;

}


/*
 *
 *
 * static関数の定義
 *
 *
 */

static void setOptionSelectAlphaFrameCtrl()
{
	int i;
	u16 *offsetidx, *soundidx, *off1, *off2, *langidx;
	u16  offsetnum, soundnum, off1num, off2num, langnum;

	offsetidx = scfGetOFFSETIndex();
	offsetnum = scfGetOFFSETNum();
	soundidx = scfGetSOUNDIndex();
	soundnum = scfGetSOUNDNum();
	off1 = scfGetOFFSET1STIndex();
	off2 = scfGetOFFSET2NDIndex();
	off1num = scfGetOFFSET1STNum();
	off2num = scfGetOFFSET2NDNum();
	langidx = scfGetLANGUAGEIndex();
	langnum = scfGetLANGUAGENum();

	switch( param.optionselect ){
	case 0:	for( i=0;i<soundnum;i++ ) setSCFade( soundidx[i], TRUE );   break;
	case 1:	for( i=0;i<offsetnum;i++ ) setSCFade( offsetidx[i], TRUE );	break;
	case 2: for( i=0;i<langnum;i++ ) setSCFade( langidx[i], TRUE );	    break;
	}
		
	switch( param.optionrunstate ){
	case IPL_OPTION_SELECTING: // 選択中。
		for( i=0;i<soundnum;i++ )   setSAFade( soundidx[i], TRUE );
		for( i=0;i<offsetnum;i++ )  setSAFade( offsetidx[i], TRUE );
		for( i=0;i<langnum;i++ )    setSAFade( langidx[i], TRUE );
		
		setSmallArrowFade( 0, FALSE );
		setSmallArrowFade( 1, FALSE );
		setSmallArrowFade( 2, FALSE );
		setSmallArrowFade( 3, FALSE );
		setSmallArrowFade( 4, FALSE );
		setSmallArrowFade( 5, FALSE );
		break;
	case IPL_OPTION_SOUND: // サウンドを選択
		for( i=0;i<soundnum;i++ )   setSAFade( soundidx[i], TRUE );
		for( i=0;i<offsetnum;i++ )  setSAFade( offsetidx[i], FALSE );
		for( i=0;i<langnum;i++ )  setSAFade( langidx[i], FALSE );
		switch( param.optionsound ){
		case 0://mono
			setSmallArrowFade( 0, FALSE );
			setSmallArrowFade( 1, TRUE );
			setSmallArrowFade( 2, FALSE );
			setSmallArrowFade( 3, FALSE );
			setSmallArrowFade( 4, FALSE );
			setSmallArrowFade( 5, FALSE );
			break;
		case 1://stereo
			setSmallArrowFade( 0, TRUE );
			setSmallArrowFade( 1, FALSE );
			setSmallArrowFade( 2, FALSE );
			setSmallArrowFade( 3, FALSE );
			setSmallArrowFade( 4, FALSE );
			setSmallArrowFade( 5, FALSE );
			break;
		}

		if( prevparam.optionrunstate == IPL_OPTION_SOUND ){
			for( i=0;i<soundnum;i++ ) forceCAFade( soundidx[i], FALSE );
		}

		break;
	case IPL_OPTION_OFFSET: // 画面位置を選択
		for( i=0;i<soundnum;i++ )   setSAFade( soundidx[i], FALSE );
		for( i=0;i<offsetnum;i++ )  setSAFade( offsetidx[i], TRUE );
		for( i=0;i<langnum;i++ )    setSAFade( langidx[i], FALSE );
		
		if( prevparam.optionrunstate == IPL_OPTION_OFFSET ){ // prevparamとの差でエフェクトを作る関係から、１フレームお休み
			for( i=0;i<off1num;i++ ) forceCAFade( off1[i], FALSE );
			for( i=0;i<off2num;i++ ) forceCAFade( off2[i], FALSE );
		}
		setSmallArrowFade( 0, FALSE );
		setSmallArrowFade( 1, FALSE );
		if( param.optionoffset <= -32 ) setSmallArrowFade( 2, FALSE );
		else setSmallArrowFade( 2, TRUE );
		if( param.optionoffset >= 32 ) setSmallArrowFade( 3, FALSE );
		else setSmallArrowFade( 3, TRUE );
		setSmallArrowFade( 4, FALSE );
		setSmallArrowFade( 5, FALSE );
		break;
	case IPL_OPTION_LANGUAGE: // 言語選択
		for( i=0;i<soundnum;i++ )   setSAFade( soundidx[i], FALSE );
		for( i=0;i<offsetnum;i++ )  setSAFade( offsetidx[i], FALSE );
		for( i=0;i<langnum;i++ )    setSAFade( langidx[i], TRUE );
		setSmallArrowFade( 0, FALSE );
		setSmallArrowFade( 1, FALSE );
		setSmallArrowFade( 2, FALSE );
		setSmallArrowFade( 3, FALSE );
		if( getLanguageCurstate() == 0 ) setSmallArrowFade( 4, FALSE );
		else setSmallArrowFade( 4, TRUE );
		if( getLanguageCurstate() == 5 ) setSmallArrowFade( 5, FALSE );
		else setSmallArrowFade( 5, TRUE );
		
		break;
	}

}

static void setOptionCharAlphaFrameCtrl(void)
{
	int i;
	s16 e[4];
	u16 *ra, *la, *numdata, *data;

	u16 ranum, lanum;

	u16 *monoidx, *steidx;
	u16  mononum, stenum;

	// 最初に全部FALSEに設定しといて、あとはTRUEをいれてくだけにする
	data = scfGetOFFSETIndex();
	for( i=0;i<scfGetOFFSETNum();i++ ) forceCAFade( data[i], FALSE );
	data = scfGetSOUNDIndex();
	for( i=0;i<scfGetSOUNDNum();i++ ) forceCAFade( data[i], FALSE );

	ra = scfGetARROWRIGHTIndex();
	la = scfGetARROWLEFTIndex();
	ranum = scfGetARROWRIGHTNum();
	lanum = scfGetARROWLEFTNum();

	monoidx = scfGetMONOIndex();
	mononum = scfGetMONONum();
	steidx = scfGetSTEREOIndex();
	stenum = scfGetSTEREONum();

	// 言語選択の棒はいつでも全部見えている
	for( i=0;i<scfGetLANGUAGENum();i++ ) forceCAFade( scfGetLANGUAGEIndex()[i], TRUE );

	switch( param.optionsound ){
	case 0:// モノラル
		for( i=0;i<mononum;i++ ) forceCAFade( monoidx[i], TRUE );
		setSmallArrowFade( 0, FALSE );
		setSmallArrowFade( 1, TRUE );
		setSmallArrowFade( 2, FALSE );
		setSmallArrowFade( 3, FALSE );
		break;
	case 1: // ステレオ
		for( i=0;i<stenum;i++ ) forceCAFade( steidx[i], TRUE );
		setSmallArrowFade( 0, TRUE );
		setSmallArrowFade( 1, FALSE );
		setSmallArrowFade( 2, FALSE );
		setSmallArrowFade( 3, FALSE );
		break;
	}

	if( param.optionoffset < 0 ){
		for( i=0;i<lanum;i++ ) forceCAFade( la[i], TRUE );
	}else if( param.optionoffset > 0 ){
		for( i=0;i<ranum;i++ ) forceCAFade( ra[i], TRUE );
	}

	scfIntTo4Number( e, (s16)fabs(param.optionoffset) );

	data = scfGetOFFSET1STIndex();
	numdata = scfGetDIGITIndex( e[1] ); //１０の位
	for( i=0; i<scfGetOFFSET1STNum();i++ ) forceCAFade( data[numdata[i]], TRUE );
	data = scfGetOFFSET2NDIndex();
	numdata = scfGetDIGITIndex( e[0] );
	for( i=0; i<scfGetOFFSET2NDNum();i++ ) forceCAFade( data[numdata[i]], TRUE );

}


/*---------------------------------------------------------------------------*
  Name:
  static void setTimeAlphaModifyDest( int numbers[] )

  Description:
  カレンダー面の数字を作る

 *---------------------------------------------------------------------------*/
static void setTimeCharAlphaFrameCtrl()
{

	int i;
	s16 numbers[6],e[4];
	u16 *data, *numdata;

	scfGetTimeNumberToDisplay( numbers, param.timerunstate, param.timeeditnum );

	// まず全部０にする
	data = scfGetDATEIndex();
	for( i=0;i<scfGetDATENum();i++ ){
		forceCAFade( data[i], FALSE );
	}
	data = scfGetCLOCKIndex();
	for( i=0;i<scfGetCLOCKNum();i++ ){
		forceCAFade( data[i], FALSE );
	}

	// 年のところを作る
	scfIntTo4Number( e, numbers[0] );
	data = scfGetYEAR1STIndex();
	numdata = scfGetDIGITIndex( e[3] ); // 1000の位
	for( i=0;i<scfGetYEAR1STNum();i++ ){
		forceCAFade( data[numdata[i]], TRUE );
	}
	data = scfGetYEAR2NDIndex();
	numdata = scfGetDIGITIndex( e[2] );
	for( i=0;i<scfGetYEAR2NDNum();i++ ){
		forceCAFade( data[numdata[i]], TRUE );
	}
	data = scfGetYEAR3RDIndex();
	numdata = scfGetDIGITIndex( e[1] );
	for( i=0;i<scfGetYEAR3RDNum();i++ ){
		forceCAFade( data[numdata[i]], TRUE );
	}
	data = scfGetYEAR4THIndex();
	numdata = scfGetDIGITIndex( e[0] );
	for( i=0;i<scfGetYEAR4THNum();i++ ){
		forceCAFade( data[numdata[i]], TRUE );
	}

	// 月のところを作る
	scfIntTo4Number( e, numbers[1] );
	data = scfGetMONTH1STIndex();
	numdata = scfGetDIGITIndex( e[1] );
	for( i=0;i<scfGetMONTH1STNum();i++ ){
		forceCAFade( data[numdata[i]], TRUE );
	}
	data = scfGetMONTH2NDIndex();
	numdata = scfGetDIGITIndex( e[0] );
	for( i=0;i<scfGetMONTH2NDNum();i++ ){
		forceCAFade( data[numdata[i]], TRUE );
	}

	// 日のところを作る
	scfIntTo4Number( e, numbers[2] );
	data = scfGetDAY1STIndex();
	numdata = scfGetDIGITIndex( e[1] );
	for( i=0;i<scfGetDAY1STNum();i++ ){
		forceCAFade( data[numdata[i]], TRUE );
	}
	data = scfGetDAY2NDIndex();
	numdata = scfGetDIGITIndex( e[0] );
	for( i=0;i<scfGetDAY2NDNum();i++ ){
		forceCAFade( data[numdata[i]], TRUE );
	}

	// 時のところを作る
	scfIntTo4Number( e, numbers[3] );
	data = scfGetHOUR1STIndex();
	numdata = scfGetDIGITIndex( e[1] );
	for( i=0;i<scfGetHOUR1STNum();i++ ){
		forceCAFade( data[numdata[i]], TRUE );
	}
	data = scfGetHOUR2NDIndex();
	numdata = scfGetDIGITIndex( e[0] );
	for( i=0;i<scfGetHOUR2NDNum();i++ ){
		forceCAFade( data[numdata[i]], TRUE );
	}

	scfIntTo4Number( e, numbers[4] );
	data = scfGetMINUTE1STIndex();
	numdata = scfGetDIGITIndex( e[1] );
	for( i=0;i<scfGetMINUTE1STNum();i++ ){
		forceCAFade( data[numdata[i]], TRUE );
	}
	data = scfGetMINUTE2NDIndex();
	numdata = scfGetDIGITIndex( e[0] );
	for( i=0;i<scfGetMINUTE2NDNum();i++ ){
		forceCAFade( data[numdata[i]], TRUE );
	}

	scfIntTo4Number( e, numbers[5] );
	data = scfGetSECOND1STIndex();
	numdata = scfGetDIGITIndex( e[1] );
	for( i=0;i<scfGetSECOND1STNum();i++ ){
		forceCAFade( data[numdata[i]], TRUE );
	}
	data = scfGetSECOND2NDIndex();
	numdata = scfGetDIGITIndex( e[0] );
	for( i=0;i<scfGetSECOND2NDNum();i++ ){
		forceCAFade( data[numdata[i]], TRUE );
	}

	// コロンなどを２５５に。
	data = scfGetCOLONIndex();

	for( i=0;i<scfGetCOLONNum();i++ ){
		forceCAFade( data[i], TRUE );
	}


}

// 仕様変更によりズームもここでやることに。
static void setTimeSelectAlphaFrameCtrl()
{

	int i;
	u16 *data;


	// まず、どっちがえらばれてるかによって動かすものを決める
	if( param.timeselect ){ // CLOCK
		data = scfGetDATEIndex();
		for( i=0;i<scfGetDATENum();i++ ){
			setSZFade( data[i], FALSE );
			setDFade( data[i], FALSE );
		}
		data = scfGetCLOCKIndex();
		for( i=0;i<scfGetCLOCKNum();i++ ){
			setSZFade( data[i], FALSE );
			setDFade( data[i], TRUE );
			setSCFade( data[i], TRUE );
		}
	}else{ // DATE
		data = scfGetDATEIndex();
		for( i=0;i<scfGetDATENum();i++ ){
			setSZFade( data[i], FALSE );
			setDFade( data[i], TRUE  );
			setSCFade( data[i], TRUE );
		}
		data = scfGetCLOCKIndex();
		for( i=0;i<scfGetCLOCKNum();i++ ){
			setSZFade( data[i], FALSE );
			setDFade( data[i], FALSE );
		}
	}


	if( param.timerunstate == IPL_TIME_DAY ){ // 日付を変更


		// 選択時は、選ばれてない項目の動きを止めるかどうか。
		if( ! getMenuState()->scf_dance_selected ){
			data = scfGetDATEIndex();
			for( i=0;i<scfGetDATENum();i++ ) setDFade( data[i], FALSE );
		}

		switch( param.timeday ){
		case 0:
			data = scfGetYEAR1STIndex();
			for( i=0;i<scfGetYEAR1STNum();i++ ){
				setSAFade( data[i], TRUE );
				setSZFade( data[i], TRUE );
				setDFade( data[i], FALSE );
			}
			data = scfGetYEAR2NDIndex();
			for( i=0;i<scfGetYEAR2NDNum();i++ ){
				setSAFade( data[i], TRUE );
				setSZFade( data[i], TRUE );
				setDFade( data[i], FALSE );
			}
			data = scfGetYEAR3RDIndex();
			for( i=0;i<scfGetYEAR3RDNum();i++ ){
				// １フレーム待ってから消さないと、１フレーム真っ黒になる
				if( prevparam.timerunstate == IPL_TIME_DAY ) forceCAFade( data[i], FALSE );
				setSAFade( data[i], TRUE );
				setSZFade( data[i], TRUE );
				setDFade( data[i], FALSE );
			}
			data = scfGetYEAR4THIndex();
			for( i=0;i<scfGetYEAR4THNum();i++ ){
				if( prevparam.timerunstate == IPL_TIME_DAY )  forceCAFade( data[i], FALSE );
				setSAFade( data[i], TRUE );
				setSZFade( data[i], TRUE );
				setDFade( data[i], FALSE );
			}
			break;
		case 1:
			data = scfGetMONTHIndex();
			for( i=0;i<scfGetMONTHNum();i++ ){
				if( prevparam.timerunstate == IPL_TIME_DAY )	forceCAFade( data[i], FALSE );
				setSAFade( data[i], TRUE );
				setSZFade( data[i], TRUE );
				setDFade( data[i], FALSE );
			}
			break;
		case 2:
			data = scfGetDAYIndex();
			for( i=0;i<scfGetDAYNum();i++ ){
				if( prevparam.timerunstate == IPL_TIME_DAY )	forceCAFade( data[i], FALSE );
				setSAFade( data[i], TRUE );
				setSZFade( data[i], TRUE );
				setDFade( data[i], FALSE );
			}
			break;
		}

	}else if( param.timerunstate == IPL_TIME_CLOCK ){

		if( ! getMenuState()->scf_dance_selected ){
			data = scfGetCLOCKIndex();
			for( i=0;i<scfGetCLOCKNum();i++ ) setDFade( data[i], FALSE );
		}

		switch( param.timeclock ){
		case 0: // 時刻を変更
			data = scfGetHOURIndex();
			for( i=0;i<scfGetHOURNum();i++ ){
				if( prevparam.timerunstate == IPL_TIME_CLOCK ) forceCAFade( data[i], FALSE );
				setSAFade( data[i], TRUE );
				setSZFade( data[i], TRUE );
				setDFade( data[i], FALSE );
			}
			break;
		case 1:
			data = scfGetMINUTEIndex();
			for( i=0;i<scfGetMINUTENum();i++ ){
				if( prevparam.timerunstate == IPL_TIME_CLOCK ) forceCAFade( data[i], FALSE );
				setSAFade( data[i], TRUE );
				setSZFade( data[i], TRUE );
				setDFade( data[i], FALSE );
			}
			break;
		case 2:
			data = scfGetSECONDIndex();
			for( i=0;i<scfGetSECONDNum();i++ ){
				if( prevparam.timerunstate == IPL_TIME_CLOCK ) forceCAFade( data[i], FALSE );
				setSAFade( data[i], TRUE );
				setSZFade( data[i], TRUE );
				setDFade( data[i], FALSE );
			}
			break;
		}

	}else{
		data = scfGetDATEIndex();
		for( i=0;i<scfGetDATENum();i++ ) setSAFade( data[i], TRUE );
		data = scfGetCLOCKIndex();
		for( i=0;i<scfGetCLOCKNum();i++ ) setSAFade( data[i], TRUE );
	}
}




/*---------------------------------------------------------------------------*
  Name:
  void applyFrameCtrl

  Description:
  アニメーションデータから得られるキューブ位置に手を加えて、えらばれてるのを
  飛び出したりする。

 *---------------------------------------------------------------------------*/
static void applyFrameCtrl(void)
{
	int i;
	f32 mafade;
	J3DTransformInfo *movepos;

	movepos = scfGetMovementOffsetPos();
	mafade = getMAFade(); /* アニメの進み具合でアルファを入れる。
		キューブがたくさん重なっているのであまり
		効果はないが。*/


	for( i=0;i<cubeNum;i++ ){
		cubeAlpha[i] = (u8)(mafade*getCAFade(i)*255);
		cubePos[i].tx += movepos[i].tx*getDFade(i);
		cubePos[i].ty += movepos[i].ty*getDFade(i);
		cubePos[i].tz += movepos[i].tz*getDFade(i);
		cubePos[i].rx = (s16)(movepos[i].rx*getDFade(i));
		cubePos[i].ry = (s16)(movepos[i].ry*getDFade(i));
		cubePos[i].rz = (s16)(movepos[i].rz*getDFade(i));

		blend3Color( &sDarkMenuColor1, &sDarkerMenuColor1, &sMenuColor1,
					 getSCFade(i), getSZDest(i), &cubeColor1[i] );
		blend3Color( &sDarkMenuColor2, &sDarkerMenuColor2, &sMenuColor2,
					 getSCFade(i), getSZDest(i), &cubeColor2[i] );

	}

}


static void updateFader(void)
{
	int i;


	for( i=0;i<cubeNum;i++ ){

		updateFaderValue( &framectrl.selectz[i],
						  framectrl.selectz_dest[i] );
		updateFaderValue( &framectrl.charalpha[i],
						  framectrl.charalpha_dest[i] );
		updateFaderValue( &framectrl.selectalpha[i],
						  framectrl.selectalpha_dest[i] );
		updateFaderValue( &framectrl.dance[i],
						  framectrl.dance_dest[i] );
		updateFaderValue( &framectrl.selectcolor[i],
						  framectrl.selectcolor_dest[i] );
	}

	updateFaderValue( &framectrl.mainanim, framectrl.mainanim_dest );
	updateFaderValue( &sLangSelectFade.framenow, LANGSELECTFRAME );

#if PAL
	updateFaderValue( &sLangEffect.frame, LANGSELECTFRAME );
#endif

}



/*---------------------------------------------------------------------------*
  Name:
  void updateFaderValue( s16 *val, s16 dest )

  Description:
  フェーダーの値を更新する。destに1近づく。

  *---------------------------------------------------------------------------*/
static void updateFaderValue( s16 *val, s16 dest )
{
	if( *val > dest ){
		*val -= 1;
	}else if( *val< dest ){
		*val += 1;
	}else{
		ASSERT( *val == dest );
	}

}








static void setOptionSelectZFrameCtrl()
{
	int i;

	switch( param.optionrunstate ){
	case IPL_OPTION_SOUND:
		for( i=0;i<scfGetSOUNDNum();i++ )    setSZFade( scfGetSOUNDIndex()[i], TRUE );
		for( i=0;i<scfGetOFFSETNum();i++ )   setSZFade( scfGetOFFSETIndex()[i], FALSE );
		for( i=0;i<scfGetLANGUAGENum();i++ ) setSZFade( scfGetLANGUAGEIndex()[i], FALSE );
		break;
	case IPL_OPTION_OFFSET:
		for( i=0;i<scfGetSOUNDNum();i++ )  setSZFade( scfGetSOUNDIndex()[i], FALSE );
		for( i=0;i<scfGetOFFSETNum();i++ ) setSZFade( scfGetOFFSETIndex()[i], TRUE );
		for( i=0;i<scfGetLANGUAGENum();i++ ) setSZFade( scfGetLANGUAGEIndex()[i], FALSE );
		break;
	case IPL_OPTION_LANGUAGE:
		for( i=0;i<scfGetSOUNDNum();i++ )  setSZFade( scfGetSOUNDIndex()[i], FALSE );
		for( i=0;i<scfGetOFFSETNum();i++ ) setSZFade( scfGetOFFSETIndex()[i], FALSE );
		for( i=0;i<scfGetLANGUAGENum();i++ ) setSZFade( scfGetLANGUAGEIndex()[i], TRUE );
		break;
	default:
		for( i=0;i<scfGetSOUNDNum();i++ )  setSZFade( scfGetSOUNDIndex()[i], FALSE );
		for( i=0;i<scfGetOFFSETNum();i++ ) setSZFade( scfGetOFFSETIndex()[i], FALSE );
		for( i=0;i<scfGetLANGUAGENum();i++ ) setSZFade( scfGetLANGUAGEIndex()[i], FALSE );
		break;
	}
		
}


static void setOptionDanceFrameCtrl()
{
	int i;

	if( param.optionrunstate == IPL_OPTION_SELECTING ){
		switch( param.optionselect ){
		case 0:
			for( i=0;i<scfGetSOUNDNum();i++ )  setDFade( scfGetSOUNDIndex()[i], TRUE );
			for( i=0;i<scfGetOFFSETNum();i++ ) setDFade( scfGetOFFSETIndex()[i], FALSE );
			for( i=0;i<scfGetLANGUAGENum();i++ ) setDFade( scfGetLANGUAGEIndex()[i], FALSE );
			break;
		case 1:
			for( i=0;i<scfGetSOUNDNum();i++ )  setDFade( scfGetSOUNDIndex()[i], FALSE );
			for( i=0;i<scfGetOFFSETNum();i++ ) setDFade( scfGetOFFSETIndex()[i], TRUE );
			for( i=0;i<scfGetLANGUAGENum();i++ ) setDFade( scfGetLANGUAGEIndex()[i], FALSE );
			break;
		case 2:
			for( i=0;i<scfGetSOUNDNum();i++ )  setDFade( scfGetSOUNDIndex()[i], FALSE );
			for( i=0;i<scfGetOFFSETNum();i++ ) setDFade( scfGetOFFSETIndex()[i], FALSE );
			for( i=0;i<scfGetLANGUAGENum();i++ ) setDFade( scfGetLANGUAGEIndex()[i], TRUE );
			break;
		}
	}else{
		for( i=0;i<scfGetSOUNDNum();i++ )    setDFade( scfGetSOUNDIndex()[i], FALSE );
		for( i=0;i<scfGetOFFSETNum();i++ )    setDFade( scfGetOFFSETIndex()[i], FALSE );
		for( i=0;i<scfGetLANGUAGENum();i++ )    setDFade( scfGetLANGUAGEIndex()[i], FALSE );
	}

}




static void setStartAllFrameCtrl()
{
	int i;

	for( i=0;i<SCF_CUBENUM;i++ ){
		setDFade( i, TRUE );  // スタート面では一定。
		setSAFade( i, TRUE ); // スタート面では一定。
		setSZFade( i, FALSE );// スタート面では一定。
		forceCAFade( i, FALSE ); // FALSEで初期化しとく
	}


	for( i=0;i<SCF_CUBENUM;i++ ) forceCAFade( i, FALSE );

}



/*---------------------------------------------------------------------------*
  Name:
  static void scfIntTo4Number( s16 *eachnum, int num)

  Description:
  ０から9999までの範囲のintの値を、桁ごとの０から９までの整数に
  変換します。
  eachnumが指す場所には、int４個分のバッファが確保されている
  必要があります。
  eachnum[0] に1の位、eachnum[1]に10の位、というふうに値が入ります。

 *---------------------------------------------------------------------------*/
void scfIntTo4Number( s16 *eachnum, int num)
{
	if( num < 0 || num > 9999 ){// 範囲の制限
		eachnum[0]= 0;
		eachnum[1]= 0;
		eachnum[2]= 0;
		eachnum[3]= 0;
	}
	if( num > 999 ){
		eachnum[3] = (s16)(num/1000);
		num -= eachnum[3]*1000;
	}else{
		eachnum[3] = 0;
	}
	if( num > 99 ){
		eachnum[2] = (s16)(num/100);
		num -= eachnum[2]*100;
	}else{
		eachnum[2] = 0;
	}
	if( num > 9 ){
		eachnum[1] = (s16)(num/10);
		num -= eachnum[1]*10;
	}else{
		eachnum[1] = 0;
	}
	eachnum[0] = (s16)num;

}


/*---------------------------------------------------------------------------*
  Name:
  static u32 scfCheckChanged

  Description:
  prevvalueとnewvalueを比べて、違っていたら1、同じなら0を返します。
  同時に、prevvalueをnewvalueに書き換えます。状態が変わったかどうか
  調べつつ状態を表す変数を書きかえるのに使います。
  oldvalueがNULLでない場合、prevvalueのもともとの値が入ります。

 *---------------------------------------------------------------------------*/
u32 scfCheckChanged( s16 *prevvalue, s16 newvalue, s16 *oldvalue )
{
	if( oldvalue ){
		*oldvalue = *prevvalue;
	}
	if( *prevvalue != newvalue ){
		*prevvalue = newvalue;
		return 1;
	}else{
		return 0;
	}
}











/*---------------------------------------------------------------------------*
  Name:
  scfRotateAround( f32 orgx, f32 orgy, f32 orgz,
			  u16 rx, u16 ry, u16 rz,
			  J3DTransformInfo *pos, J3DTransformInfo *diff )

  Description:
  posを点（orgx,orgy,orgz)の周りで回転させたときの、座標の変化を
  diffに入れる。

 *---------------------------------------------------------------------------*/
void
scfRotateAround( f32 orgx, f32 orgy, f32 orgz,
				 u16 rx, u16 ry, u16 rz,
				 J3DTransformInfo *pos, J3DTransformInfo *diff )
{
	Mtx m;
	Vec src, dest;
	gGetRotateMtx( (s16)rx, (s16)ry, (s16)rz, m );
	src.x = pos->tx - orgx;
	src.y = pos->ty - orgy;
	src.z = pos->tz - orgz;
	MTXMultVec( m, &src, &dest );
	diff->tx = dest.x + orgx - pos->tx;
	diff->ty = dest.y + orgy - pos->ty;
	diff->tz = dest.z + orgz - pos->tz;
	diff->rx = (s16)rx;
	diff->ry = (s16)ry;
	diff->rz = (s16)rz;

}


static void
scaleAround( f32 orgx, f32 orgy, f32 orgz,
			 f32 sx, f32 sy, f32 sz,
			 J3DTransformInfo *pos )
{
	pos->tx = (pos->tx - orgx)*sx + orgx;
	pos->ty = (pos->ty - orgy)*sy + orgy;
	pos->tz = (pos->tz - orgz)*sz + orgz;
	pos->sx *= sx;
	pos->sy *= sy;
	pos->sz *= sz;

}


/*---------------------------------------------------------------------------*
  Name:
  scfGetCenterPos( J3DTransformInfo *pos, u16 *indices, u32 cubenum )

  Description:
  indicesで示されるposの重心の座標を求める。
  indicesがNULLの場合、インデクス参照せず、pos[0]~pos[cubenum-1]の中心を
  求める。

 *---------------------------------------------------------------------------*/
Vec
scfGetCenterPos( J3DTransformInfo *pos, u16 *indices, u32 cubenum )
{
	int i;
	f32 tx_sum, ty_sum, tz_sum;

	tx_sum = ty_sum = tz_sum = 0.0f;
	if( indices ){
		for( i=0;i<cubenum;i++ ){
			tx_sum += pos[indices[i]].tx;
			ty_sum += pos[indices[i]].ty;
			tz_sum += pos[indices[i]].tz;
		}
	}else{
		for( i=0;i<cubenum;i++ ){
			tx_sum += pos[i].tx;
			ty_sum += pos[i].ty;
			tz_sum += pos[i].tz;
		}
	}

	return (Vec){tx_sum/cubenum, ty_sum/cubenum, tz_sum/cubenum };

}




u16
scfDegToU16Rot( f32 deg )
{
	return (u16)(deg*65535/360);
}

static f32 getSZFade( int i )
{
	return (f32)framectrl.selectz[i]/getMenuState()->scf_movefadeframenum;
}

static f32 getSZDest( int i )
{
	return (f32)framectrl.selectz_dest[i]/getMenuState()->scf_movefadeframenum;
}

static f32 getCAFade( int i )
{
	return (f32)framectrl.charalpha[i]/getMenuState()->scf_alphafadeframenum;
}
static f32 getSAFade( int i )
{
	return (f32)framectrl.selectalpha[i]/getMenuState()->scf_alphafadeframenum;
}
static f32 getDFade( int i )
{
	return (f32)framectrl.dance[i]/getMenuState()->scf_movefadeframenum;
}
static f32 getMAFade(void)
{
	return (f32)framectrl.mainanim/getMenuState()->scf_mainanimframenum;
}
static f32 getTuneFade(void)
{
	f32 tunerate;
#define TUNESTART 0.9f
	tunerate = (getMAFade()-TUNESTART) / (1-TUNESTART);
	if( tunerate < 0 ) tunerate = 0;
	else if( tunerate > 1 ) tunerate = 1;

	return tunerate;
}


static f32 getSCFade( int i )
{
	return (f32)framectrl.selectcolor[i]/getMenuState()->scf_alphafadeframenum;
}

static void setSZFade( int i, int on_off )
{
	if( on_off ){
		framectrl.selectz_dest[i] = getMenuState()->scf_movefadeframenum;
	}else{
		framectrl.selectz_dest[i] = 0;
	}
}

static void setCAFade( int i, int on_off )
{
	if( on_off ){
		framectrl.charalpha_dest[i] = getMenuState()->scf_alphafadeframenum;
	}else{
		framectrl.charalpha_dest[i] = 0;
	}
}

static void setSAFade( int i, int on_off )
{
	if( on_off ){
		framectrl.selectalpha_dest[i] = getMenuState()->scf_alphafadeframenum;
	}else{
		framectrl.selectalpha_dest[i] = 0;
	}
}
static void setDFade( int i, int on_off )
{
	if( on_off ){
		framectrl.dance_dest[i] = getMenuState()->scf_movefadeframenum;
	}else{
		framectrl.dance_dest[i] = 0;
	}
}

static void setSCFade( int i, int on_off )
{
	if( on_off ){
		framectrl.selectcolor_dest[i] = getMenuState()->scf_alphafadeframenum;
	}else{
		framectrl.selectcolor_dest[i] = 0;
	}
}

static void forceSZFade( int i, int on_off )
{
	if( on_off ){
		framectrl.selectz[i]
		= framectrl.selectz_dest[i]
		  = getMenuState()->scf_movefadeframenum;
	}else{
		framectrl.selectz[i]
		= framectrl.selectz_dest[i]
		  = 0;
	}

}
static void forceCAFade( int i, int on_off )
{

	if( on_off ){
		framectrl.charalpha[i]
		= framectrl.charalpha_dest[i]
		  = getMenuState()->scf_alphafadeframenum;
	}else{
		framectrl.charalpha[i]
		= framectrl.charalpha_dest[i]
		  = 0;
	}
}

void forceSAFade( int i, int on_off )
{
	if( on_off ){
		framectrl.selectalpha[i]
		= framectrl.selectalpha_dest[i]
		  = getMenuState()->scf_alphafadeframenum;
	}else{
		framectrl.selectalpha[i]
		= framectrl.selectalpha_dest[i]
		  = 0;
	}

}

static void forceDFade( int i, int on_off )
{
	if( on_off ){
		framectrl.dance[i]
		= framectrl.dance_dest[i]
		  = getMenuState()->scf_movefadeframenum;
	}else{
		framectrl.dance[i]
		= framectrl.dance_dest[i]
		  = 0;
	}
}


smallCubeFaderState *scfGetState(void)
{
	return &param;

}

smallCubeFaderState *scfGetPrevState(void)
{
	return &prevparam;
}

static void setMainState(void)
{
	prevparam = param;
	param.state = SCFS_MAIN;
	param.startdvdstate = 0x7f; // ありえない値としてのマジックナンバー
	param.optionrunstate = 0x7f;
	param.optionselect = 0x7f;
	param.optionoffset = 0x7f;
	param.optionsound = 0x7f;
	param.timerunstate = 0x7f;
	param.timeselect = 0x7f;
	param.timeday = 0x7f;
	param.timeclock = 0x7f;
	param.timeeditnum[0] = 0x7f;
	param.timeeditnum[1] = 0x7f;
	param.timeeditnum[2] = 0x7f;

}

static void setStartState( s16 runstate, s16 start_nodisk_question )
{
	prevparam = param;
	param.state = SCFS_START;
	param.startrunstate = runstate;
	param.startdvdstate = start_nodisk_question;
	param.optionrunstate = 0x7f;
	param.optionselect = 0x7f;
	param.optionoffset = 0x7f;
	param.optionsound = 0x7f;
	param.timerunstate = 0x7f;
	param.timeselect = 0x7f;
	param.timeday = 0x7f;
	param.timeclock = 0x7f;
	param.timeeditnum[0] = 0x7f;
	param.timeeditnum[1] = 0x7f;
	param.timeeditnum[2] = 0x7f;
}

static void setOptionState( s16 runstate, s16 select_curstate,
							s16 offset_curstate, s16 sound_curstate )
{
	prevparam = param;
	param.state = SCFS_OPTION;
	param.startrunstate = 0x7f;
	param.startdvdstate = 0x7f;
	param.optionrunstate = runstate;
	param.optionselect = select_curstate;
	param.optionoffset = offset_curstate;
	param.optionsound = sound_curstate;
	param.timerunstate = 0x7f;
	param.timeselect = 0x7f;
	param.timeday = 0x7f;
	param.timeclock = 0x7f;
	param.timeeditnum[0] = 0x7f;
	param.timeeditnum[1] = 0x7f;
	param.timeeditnum[2] = 0x7f;
}

static void setTimeState( s16 runstate, s16 select_curstate, s16 day_curstate,
						  s16 clock_curstate, int edit_num[] )
{
	prevparam = param;
	param.state = SCFS_TIME;
	param.startrunstate = 0x7f;
	param.startdvdstate = 0x7f;
	param.optionrunstate = 0x7f;
	param.optionselect = 0x7f;
	param.optionoffset = 0x7f;
	param.optionsound = 0x7f;
	param.timerunstate = runstate;
	param.timeselect = select_curstate;
	param.timeday = day_curstate;
	param.timeclock = clock_curstate;
	param.timeeditnum[0] = (s16)edit_num[0];
	param.timeeditnum[1] = (s16)edit_num[1];
	param.timeeditnum[2] = (s16)edit_num[2];

}

static void
tuneOptionCubePos(void)
{
	int i;
	u16 *data;
	Vec center;


	center.x = center.y = center.z = 0;
	data = scfGetSOUNDIndex();
	center = scfGetCenterPos( cubePos, data, scfGetSOUNDNum() );
	for( i=0;i<scfGetSOUNDNum();i++ ){
		cubePos[data[i]].tx += getMenuState()->scf_optionsound_xoffset*getMAFade();
		cubePos[data[i]].ty += getMenuState()->scf_optionsound_yoffset*getMAFade();
		scaleAround( center.x, center.y, center.z,
					 getMenuState()->scf_optionsound_scale,
					 getMenuState()->scf_optionsound_scale,
					 getMenuState()->scf_optionsound_scale,
					 &cubePos[data[i]] );
	}
	data = scfGetOFFSETIndex();
	center = scfGetCenterPos( cubePos, data, scfGetOFFSETNum() );
	for( i=0;i<scfGetOFFSETNum();i++ ){
		cubePos[data[i]].tx += getMenuState()->scf_optionoffset_xoffset*getMAFade();
		cubePos[data[i]].ty += getMenuState()->scf_optionoffset_yoffset*getMAFade();
		scaleAround( center.x, center.y, center.z,
					 getMenuState()->scf_optionoffset_scale,
					 getMenuState()->scf_optionoffset_scale,
					 getMenuState()->scf_optionoffset_scale,
					 &cubePos[data[i]] );
	}

	data = scfGetLANGUAGEIndex();
	center = scfGetCenterPos( cubePos, data, scfGetLANGUAGENum() );
	center.x = 5; // 画面をはみ出させないための特殊処理
	center.y = -135; // 画面をはみ出させないための特殊処理
	for( i=0;i<scfGetLANGUAGENum();i++ ){
		cubePos[data[i]].tx += getMenuState()->scf_optionlang_xoffset*getMAFade();
		cubePos[data[i]].ty += getMenuState()->scf_optionlang_yoffset*getMAFade();
		scaleAround( center.x, center.y, center.z,
					 getMenuState()->scf_optionlang_scale,
					 getMenuState()->scf_optionlang_scale,
					 getMenuState()->scf_optionlang_scale,
					 &cubePos[data[i]] );
	}

	// 選択のズームもここでやってまう
	data = scfGetSOUNDIndex();
	center = scfGetCenterPos( cubePos, data, scfGetSOUNDNum() );
	for( i=0;i<scfGetSOUNDNum();i++ ){
		scaleAround( center.x, center.y, center.z,
					 1+getSZFade(data[i])*getMenuState()->scf_optionsound_zoom,
					 1+getSZFade(data[i])*getMenuState()->scf_optionsound_zoom,
					 1+getSZFade(data[i])*getMenuState()->scf_optionsound_zoom,
					 &cubePos[data[i]] );
	}
	data = scfGetOFFSETIndex();
	center = scfGetCenterPos( cubePos, data, scfGetOFFSETNum() );
	for( i=0;i<scfGetOFFSETNum();i++ ){
		scaleAround( center.x, center.y, center.z,
					 1+getSZFade(data[i])*getMenuState()->scf_optionoffset_zoom,
					 1+getSZFade(data[i])*getMenuState()->scf_optionoffset_zoom,
					 1+getSZFade(data[i])*getMenuState()->scf_optionoffset_zoom,
					 &cubePos[data[i]] );
	}
	data = scfGetLANGUAGEIndex();
	center = scfGetCenterPos( cubePos, data, scfGetLANGUAGENum() );
	for( i=0;i<scfGetLANGUAGENum();i++ ){
		scaleAround( center.x, center.y, center.z,
					 1+getSZFade(data[i])*getMenuState()->scf_optionlang_zoom,
					 1+getSZFade(data[i])*getMenuState()->scf_optionlang_zoom,
					 1+getSZFade(data[i])*getMenuState()->scf_optionlang_zoom,
					 &cubePos[data[i]] );
		// 言語選択の横移動もここでやる。  animeditの最終状態の座標系に変換
#define SCF_SCREENWIDTH 542 // 実測による値
		cubePos[data[i]].tx += (f32)(getTuneFade()*(- SCF_SCREENWIDTH/2 + SCF_SCREENWIDTH/592.0*getLangSelectXOffset()));
		
	}

}

#ifdef PAL
static void
langCursorMove( s32 oldlang, s32 newlang )
{
	s32 langoffset;
	ASSERT( 0<=oldlang && oldlang<=5 );
	ASSERT( 0<=newlang && newlang<=5 );

	// 言語による位置の微調整。
	switch( newlang ){
	case 0: langoffset = getMenuState()->scf_optionlang1; break;
	case 1: langoffset = getMenuState()->scf_optionlang2; break;
	case 2: langoffset = getMenuState()->scf_optionlang3; break;
	case 3: langoffset = getMenuState()->scf_optionlang4; break;
	case 4: langoffset = getMenuState()->scf_optionlang5; break;
	case 5: langoffset = getMenuState()->scf_optionlang6; break;
	}
	
	// まず古いほうの場所を現在のフレームで取得
	sLangSelectFade.old_x = getLangSelectXOffset();
	sLangSelectFade.dest_x = getOptionLanguagePosX() + langoffset;
	sLangSelectFade.framenow = 0;

#ifdef PAL_ACTIVE_CHANGE
	startLangSelectEffect( oldlang, newlang );
#endif
}
#endif


#ifdef PAL
static
void langChanged( s32 oldlang, s32 newlang )
{
	ASSERT( 0<=oldlang && oldlang<=5 );
	ASSERT( 0<=newlang && newlang<=5 );

#ifndef PAL_ACTIVE_CHANGE
	startLangSelectEffect( oldlang, newlang );
#else
#pragma unused ( oldlang )
#pragma unused ( newlang )
#endif
	
}
#endif

/*---------------------------------------------------------------------------*
  Name:
  
  Description:
  言語選択の棒をずらす量を返す。3D座標系で。
              
 *---------------------------------------------------------------------------*/
static f32
getLangSelectXOffset(void)
{

	return gHermiteInterpolation( sLangSelectFade.framenow,
											  0,               sLangSelectFade.old_x,  0,
											  LANGSELECTFRAME, sLangSelectFade.dest_x, 0 );
}

static void
tuneTimeCubePos(void)
{
	int i;
	u16 *data;
	Vec center;
	data = scfGetDATEIndex();
	center = scfGetCenterPos( cubePos, data, scfGetDATENum() );
	for( i=0;i<scfGetDATENum();i++ ){
		cubePos[data[i]].tx += getMenuState()->scf_timedate_xoffset;
		cubePos[data[i]].ty += getMenuState()->scf_timedate_yoffset;
		scaleAround( center.x, center.y, center.z,
					 getMenuState()->scf_timedate_scale,
					 getMenuState()->scf_timedate_scale,
					 getMenuState()->scf_timedate_scale,
					 &cubePos[data[i]] );
	}
	data = scfGetCLOCKIndex();
	center = scfGetCenterPos( cubePos, data, scfGetCLOCKNum() );
	for( i=0;i<scfGetCLOCKNum();i++ ){
		cubePos[data[i]].tx += getMenuState()->scf_timeclock_xoffset;
		cubePos[data[i]].ty += getMenuState()->scf_timeclock_yoffset;
		scaleAround( center.x, center.y, center.z,
					 getMenuState()->scf_timeclock_scale,
					 getMenuState()->scf_timeclock_scale,
					 getMenuState()->scf_timeclock_scale,
					 &cubePos[data[i]] );
	}

	// 選択のスケールもここで。

	if( param.timerunstate == IPL_TIME_DAY ){ // 日付を変更
		switch( param.timeday ){
		case 0:
			data = scfGetYEARIndex();
			center = scfGetCenterPos( cubePos, data, scfGetYEARNum() );
			for( i=0;i<scfGetYEARNum();i++ ){
				scaleAround( center.x, center.y, center.z,
							 1+getSZFade(data[i])*getMenuState()->scf_timedate_zoom,
							 1+getSZFade(data[i])*getMenuState()->scf_timedate_zoom,
							 1+getSZFade(data[i])*getMenuState()->scf_timedate_zoom,
							 &cubePos[data[i]] );
			}
			break;
		case 1:
			data = scfGetMONTHIndex();
			center = scfGetCenterPos( cubePos, data, scfGetMONTHNum() );
			for( i=0;i<scfGetMONTHNum();i++ ){
				scaleAround( center.x, center.y, center.z,
							 1+getSZFade(data[i])*getMenuState()->scf_timedate_zoom,
							 1+getSZFade(data[i])*getMenuState()->scf_timedate_zoom,
							 1+getSZFade(data[i])*getMenuState()->scf_timedate_zoom,
							 &cubePos[data[i]] );
			}
			break;
		case 2:
			data = scfGetDAYIndex();
			center = scfGetCenterPos( cubePos, data, scfGetDAYNum() );
			for( i=0;i<scfGetDAYNum();i++ ){
				scaleAround( center.x, center.y, center.z,
							 1+getSZFade(data[i])*getMenuState()->scf_timedate_zoom,
							 1+getSZFade(data[i])*getMenuState()->scf_timedate_zoom,
							 1+getSZFade(data[i])*getMenuState()->scf_timedate_zoom,
							 &cubePos[data[i]] );
			}
			break;
		}


	}else if( param.timerunstate == IPL_TIME_CLOCK ){
		switch( param.timeclock ){
		case 0: // 時刻を変更
			data = scfGetHOURIndex();
			center = scfGetCenterPos( cubePos, data, scfGetHOURNum() );
			for( i=0;i<scfGetHOURNum();i++ ){
				scaleAround( center.x, center.y, center.z,
							 1+getSZFade(data[i])*getMenuState()->scf_timeclock_zoom,
							 1+getSZFade(data[i])*getMenuState()->scf_timeclock_zoom,
							 1+getSZFade(data[i])*getMenuState()->scf_timeclock_zoom,
							 &cubePos[data[i]] );
			}
			break;
		case 1:
			data = scfGetMINUTEIndex();
			center = scfGetCenterPos( cubePos, data, scfGetMINUTENum() );
			for( i=0;i<scfGetMINUTENum();i++ ){
				scaleAround( center.x, center.y, center.z,
							 1+getSZFade(data[i])*getMenuState()->scf_timeclock_zoom,
							 1+getSZFade(data[i])*getMenuState()->scf_timeclock_zoom,
							 1+getSZFade(data[i])*getMenuState()->scf_timeclock_zoom,
							 &cubePos[data[i]] );
			}
			break;
		case 2:
			data = scfGetSECONDIndex();
			center = scfGetCenterPos( cubePos, data, scfGetSECONDNum() );
			for( i=0;i<scfGetSECONDNum();i++ ){
				scaleAround( center.x, center.y, center.z,
							 1+getSZFade(data[i])*getMenuState()->scf_timeclock_zoom,
							 1+getSZFade(data[i])*getMenuState()->scf_timeclock_zoom,
							 1+getSZFade(data[i])*getMenuState()->scf_timeclock_zoom,
							 &cubePos[data[i]] );
			}
			break;
		}
	}

}

static void
tuneStartCubePos(void)
{
	int i;
	Vec center;
	f32 xoff, yoff, scale;

	yoff = getMenuState()->scf_start_yoffset; // yは全言語共通
	
	switch( getLanguageCurstate() ){
	case 0: // eng
	case 3: // dut
	case 4: // spa
	case 5: // ita
		xoff = getMenuState()->scf_start_xoffset;
		scale = getMenuState()->scf_start_scale;
		break;
	case 1: // fra
		xoff = -36;
		scale = 0.65F;
		break;
	case 2: // ger
		xoff = -36;
		scale = 0.6F;
		break;
	}

	// 一枚しか設定がないので、全部に対して行う

	center = scfGetCenterPos( cubePos, NULL, cubeNum );

	for( i=0;i<cubeNum;i++ ){
		cubePos[i].tx += xoff;
		cubePos[i].ty += yoff;
		scaleAround( center.x, center.y, center.z,
					 scale, scale, scale, &cubePos[i] );
	}

}


static void
setSmallArrowFade( u32 id, u32 true_false )
{
	u16 *d[6];
	u16 num[6], i;

	ASSERT( 0<=id && id<6 );
	d[0] = scfGetSMALL_ARROW01Index();
	num[0] = scfGetSMALL_ARROW01Num();
	d[1] = scfGetSMALL_ARROW02Index();
	num[1] = scfGetSMALL_ARROW02Num();
	d[2] = scfGetSMALL_ARROW03Index();
	num[2] = scfGetSMALL_ARROW03Num();
	d[3] = scfGetSMALL_ARROW04Index();
	num[3] = scfGetSMALL_ARROW04Num();
	d[4] = scfGetSMALL_ARROW05Index();
	num[4] = scfGetSMALL_ARROW05Num();
	d[5] = scfGetSMALL_ARROW06Index();
	num[5] = scfGetSMALL_ARROW06Num();

	for( i=0;i<num[id];i++ ){
		setSAFade( d[id][i], (int)true_false );
		setCAFade( d[id][i], (int)true_false );
		setSCFade( d[id][i], TRUE ); // 選ばれてるほうの色しか使わん
		setSZFade( d[id][i], TRUE );
	}

}


static void
setSmallArrowPos( void )
{
	int i;
	u16 *centerdata, *arrowdata;
	Vec soundcenter, offsetcenter, langcenter;

	// 中心を計算
	centerdata = scfGetSOUNDIndex();
	soundcenter = scfGetCenterPos( cubePos, centerdata, scfGetSOUNDNum() );
	soundcenter.x += getMenuState()->scf_optionsound_xoffset;
	soundcenter.y += getMenuState()->scf_optionsound_yoffset;

	centerdata = scfGetOFFSETIndex();
	offsetcenter = scfGetCenterPos( cubePos, centerdata, scfGetOFFSETNum() );
	offsetcenter.x += getMenuState()->scf_optionoffset_xoffset;
	offsetcenter.y += getMenuState()->scf_optionoffset_yoffset;

	centerdata = scfGetLANGUAGEIndex();
	langcenter = scfGetCenterPos( cubePos, centerdata, scfGetLANGUAGENum() );
	langcenter.x += getMenuState()->scf_optionlang_xoffset;
	langcenter.y += getMenuState()->scf_optionlang_yoffset;

	arrowdata = scfGetSMALL_ARROW01Index();
	for( i=0;i<scfGetSMALL_ARROW01Num();i++ ){
		cubePos[arrowdata[i]].tx += getMenuState()->scf_optionsound_xoffset;
		cubePos[arrowdata[i]].ty += getMenuState()->scf_optionsound_yoffset;
		scaleAround( soundcenter.x, soundcenter.y, soundcenter.z,
					 getMenuState()->scf_optionsound_scale,
					 getMenuState()->scf_optionsound_scale,
					 getMenuState()->scf_optionsound_scale,
					 &cubePos[arrowdata[i]] );
	}
	for( i=0;i<scfGetSMALL_ARROW01Num();i++ ){
		scaleAround( soundcenter.x, soundcenter.y, soundcenter.z,
					 1+getMenuState()->scf_optionsound_zoom,
					 1+getMenuState()->scf_optionsound_zoom,
					 1+getMenuState()->scf_optionsound_zoom,
					 &cubePos[arrowdata[i]] );
		cubePos[arrowdata[i]].tx -= getMenuState()->scf_arrowx
									*gHermiteInterpolation( getArrowFrame(), 0, 0, 0,
															1,1,0 );
		cubePos[arrowdata[i]].tx += getMenuState()->scf_soundarrowl_x;
		cubePos[arrowdata[i]].ty += getMenuState()->scf_soundarrowl_y;
	}


	arrowdata = scfGetSMALL_ARROW02Index();
	for( i=0;i<scfGetSMALL_ARROW02Num();i++ ){
		cubePos[arrowdata[i]].tx += getMenuState()->scf_optionsound_xoffset;
		cubePos[arrowdata[i]].ty += getMenuState()->scf_optionsound_yoffset;
		scaleAround( soundcenter.x, soundcenter.y, soundcenter.z,
					 getMenuState()->scf_optionsound_scale,
					 getMenuState()->scf_optionsound_scale,
					 getMenuState()->scf_optionsound_scale,
					 &cubePos[arrowdata[i]] );
	}
	for( i=0;i<scfGetSMALL_ARROW02Num();i++ ){
		scaleAround( soundcenter.x, soundcenter.y, soundcenter.z,
					 1+getMenuState()->scf_optionsound_zoom,
					 1+getMenuState()->scf_optionsound_zoom,
					 1+getMenuState()->scf_optionsound_zoom,
					 &cubePos[arrowdata[i]] );

		cubePos[arrowdata[i]].tx += getMenuState()->scf_arrowx
									*gHermiteInterpolation( getArrowFrame(), 0,0,0,1,1,0 );

#ifdef PAL
		if( getLanguageCurstate() == 4 ){
			cubePos[arrowdata[i]].tx += -40;
		}else{
			cubePos[arrowdata[i]].tx += getMenuState()->scf_soundarrowr_x;
		}
#else
		cubePos[arrowdata[i]].tx += getMenuState()->scf_soundarrowr_x;
#endif
		cubePos[arrowdata[i]].ty += getMenuState()->scf_soundarrowr_y;
		
	}


	arrowdata = scfGetSMALL_ARROW03Index();
	for( i=0;i<scfGetSMALL_ARROW03Num();i++ ){
		cubePos[arrowdata[i]].tx += getMenuState()->scf_optionoffset_xoffset;
		cubePos[arrowdata[i]].ty += getMenuState()->scf_optionoffset_yoffset;
		scaleAround( offsetcenter.x, offsetcenter.y, offsetcenter.z,
					 getMenuState()->scf_optionoffset_scale,
					 getMenuState()->scf_optionoffset_scale,
					 getMenuState()->scf_optionoffset_scale,
					 &cubePos[arrowdata[i]] );
	}
	for( i=0;i<scfGetSMALL_ARROW03Num();i++ ){
		scaleAround( offsetcenter.x, offsetcenter.y, offsetcenter.z,
					 1+getMenuState()->scf_optionoffset_zoom,
					 1+getMenuState()->scf_optionoffset_zoom,
					 1+getMenuState()->scf_optionoffset_zoom,
					 &cubePos[arrowdata[i]] );
		cubePos[arrowdata[i]].tx -= getMenuState()->scf_arrowx
									*gHermiteInterpolation( getArrowFrame(), 0,0,0,1,1,0 );
		cubePos[arrowdata[i]].tx += getMenuState()->scf_offsetarrowl_x;
		cubePos[arrowdata[i]].ty += getMenuState()->scf_offsetarrowl_y;
	}


	arrowdata = scfGetSMALL_ARROW04Index();
	for( i=0;i<scfGetSMALL_ARROW04Num();i++ ){
		cubePos[arrowdata[i]].tx += getMenuState()->scf_optionoffset_xoffset;
		cubePos[arrowdata[i]].ty += getMenuState()->scf_optionoffset_yoffset;
		scaleAround( offsetcenter.x, offsetcenter.y, offsetcenter.z,
					 getMenuState()->scf_optionoffset_scale,
					 getMenuState()->scf_optionoffset_scale,
					 getMenuState()->scf_optionoffset_scale,
					 &cubePos[arrowdata[i]] );
	}
	for( i=0;i<scfGetSMALL_ARROW04Num();i++ ){
		scaleAround( offsetcenter.x, offsetcenter.y, offsetcenter.z,
					 1+getMenuState()->scf_optionoffset_zoom,
					 1+getMenuState()->scf_optionoffset_zoom,
					 1+getMenuState()->scf_optionoffset_zoom,
					 &cubePos[arrowdata[i]] );
		cubePos[arrowdata[i]].tx += getMenuState()->scf_arrowx
									*gHermiteInterpolation( getArrowFrame(), 0,0,0,1,1,0 );
		cubePos[arrowdata[i]].tx += getMenuState()->scf_offsetarrowr_x;
		cubePos[arrowdata[i]].ty += getMenuState()->scf_offsetarrowr_y;
	}

	arrowdata = scfGetSMALL_ARROW05Index();
	for( i=0;i<scfGetSMALL_ARROW05Num();i++ ){
		cubePos[arrowdata[i]].tx += getMenuState()->scf_optionlang_xoffset;
		cubePos[arrowdata[i]].ty += getMenuState()->scf_optionlang_yoffset;
		scaleAround( langcenter.x, langcenter.y, langcenter.z,
					 getMenuState()->scf_optionlang_scale,
					 getMenuState()->scf_optionlang_scale,
					 getMenuState()->scf_optionlang_scale,
					 &cubePos[arrowdata[i]] );
	}
	for( i=0;i<scfGetSMALL_ARROW05Num();i++ ){
		scaleAround( langcenter.x, langcenter.y, langcenter.z,
					 1+getMenuState()->scf_optionlang_zoom,
					 1+getMenuState()->scf_optionlang_zoom,
					 1+getMenuState()->scf_optionlang_zoom,
					 &cubePos[arrowdata[i]] );
		cubePos[arrowdata[i]].tx -= getMenuState()->scf_arrowx
									*gHermiteInterpolation( getArrowFrame(), 0,0,0,1,1,0 );
		cubePos[arrowdata[i]].tx += getMenuState()->scf_langarrowl_x;
		cubePos[arrowdata[i]].ty += getMenuState()->scf_langarrowl_y;
	}

	arrowdata = scfGetSMALL_ARROW06Index();
	for( i=0;i<scfGetSMALL_ARROW06Num();i++ ){
		cubePos[arrowdata[i]].tx += getMenuState()->scf_optionlang_xoffset;
		cubePos[arrowdata[i]].ty += getMenuState()->scf_optionlang_yoffset;
		scaleAround( langcenter.x, langcenter.y, langcenter.z,
					 getMenuState()->scf_optionlang_scale,
					 getMenuState()->scf_optionlang_scale,
					 getMenuState()->scf_optionlang_scale,
					 &cubePos[arrowdata[i]] );
	}
	for( i=0;i<scfGetSMALL_ARROW06Num();i++ ){
		scaleAround( langcenter.x, langcenter.y, langcenter.z,
					 1+getMenuState()->scf_optionlang_zoom,
					 1+getMenuState()->scf_optionlang_zoom,
					 1+getMenuState()->scf_optionlang_zoom,
					 &cubePos[arrowdata[i]] );
		cubePos[arrowdata[i]].tx += getMenuState()->scf_arrowx
									*gHermiteInterpolation( getArrowFrame(), 0,0,0,1,1,0 );
		cubePos[arrowdata[i]].tx += getMenuState()->scf_langarrowr_x;
		cubePos[arrowdata[i]].ty += getMenuState()->scf_langarrowr_y;
	}
	

}

static f32
getArrowFrame(void)
{
	f32 f;
	f = (f32)((f32)sArrowAnim/getMenuState()->scf_arrowanimframe);

	if( f < 0.5 ) return f*2.0F;
	else return (1.0f-f)*2;
}


static void
setEffectColor(void)
{
	int i;
	// エフェクト部分に色をつけるため、明るいほうの色を全部につける。
	for( i=0;i<SCF_CUBENUM;i++ ){
		cubeColor1[i].r = sMenuColor1.r;
		cubeColor1[i].g = sMenuColor1.g;
		cubeColor1[i].b = sMenuColor1.b;
		cubeColor1[i].a = sMenuColor1.a;
		cubeColor2[i].r = sMenuColor2.r;
		cubeColor2[i].g = sMenuColor2.g;
		cubeColor2[i].b = sMenuColor2.b;
		cubeColor2[i].a = sMenuColor2.a;
		setSCFade( i, FALSE );// ついでに色を暗いほうにしとく
		setSAFade( i, FALSE ); //さらについでに
	}
}

static void startFadeoutPushSection( f32 frame, J3DTransformInfo *pos, u32 *cubenum );
static void startFadeoutMoveSection( f32 frame, J3DTransformInfo *pos, u32 *cubenum );


static void
startFadeoutPushSection( f32 frame, J3DTransformInfo *pos, u32 *cubenum )
{
	int i;
	f32 scale;
	Vec center;

	center = scfGetCenterPos( pos, NULL, *cubenum );

#define SCF_DOWNTOUPFRAME 0.1f
#define SCF_PUSHSCALE 0.05f

	if( frame < SCF_DOWNTOUPFRAME ){
		scale = 1 - SCF_PUSHSCALE*gHermiteInterpolation( frame, 0, 0, 0,
				SCF_DOWNTOUPFRAME, 1, 0 );
	}else{
		scale = 1 - SCF_PUSHSCALE*gHermiteInterpolation( frame, SCF_DOWNTOUPFRAME, 1, 0,
				1, 0, 0 );
	}

	for( i=0;i<*cubenum;i++ ){
		scaleAround( center.x, center.y, center.z,
					 scale, scale, scale,
					 &pos[i] );

		pos[i].sx *= scale;
		pos[i].sy *= scale;
		pos[i].sz *= scale;
	}

}

static void
startFadeoutMoveSection( f32 frame, J3DTransformInfo *pos, u32 *cubenum )
{

#ifdef MPAL

    static u8 timingdata[] =
    {
		94,91,87,83,71,68,61,57,53,50,46,41,38,34,31,26,19,15,12,
		95,91,88,84,80,72,68,57,54,50,46,43,38,35,28,23,20,16,13,9,
		96,92,88,85,80,73,69,59,54,51,47,43,40,35,28,25,21,18,13,10,
		96,92,89,85,82,73,70,63,59,55,51,48,44,40,36,32,25,22,18,14,10,
		97,93,82,75,70,63,60,56,52,48,45,41,38,30,26,22,18,15,11,
		98,94,82,75,72,64,61,57,53,49,45,42,37,31,27,23,19,15,11,
		98,95,91,87,80,76,73,69,65,61,57,54,50,46,43,38,34,24,20,16,12,
		57,54,50,46,43,39,
		197,194,191,189,187,184,
		252,250,240,238,232,228,225,222,220,217,215,212,210,207,205,202,200,197,195,192,190,188,185,180,177,175,167,165,157,152,149,147,144,
		253,251,241,238,233,228,225,212,210,208,206,203,198,195,192,191,188,185,183,180,175,172,168,165,158,152,150,
		253,251,241,238,233,228,226,213,211,208,206,204,198,196,194,191,188,186,183,180,173,168,165,158,153,150,
		254,251,249,242,239,237,229,227,224,221,219,216,214,211,209,206,203,201,198,196,194,191,189,186,184,181,173,168,166,158,153,150,148,146,
		254,252,247,242,239,235,229,226,219,217,214,211,209,207,204,202,194,191,189,187,184,181,174,169,166,161,158,154,151,
		255,252,247,242,239,235,229,227,220,217,214,212,209,207,204,202,199,197,194,192,190,186,184,182,174,169,166,164,159,154,151,
		255,253,250,243,240,238,230,227,225,223,219,217,215,212,210,207,205,202,200,197,194,192,189,182,179,177,170,167,159,155,151,150,147,
    };

#elif PAL
	static u8 timingdata[] =
	{

255,255,255,255,255,255,255,253,250,247,244,241,237,233,230,225,221,217,212,207,203,197,193,188,182,176,171,166,160,154,149,138,132,126,121,115,104,99,83,72,68,
255,255,255,255,255,255,255,255,252,250,247,244,241,237,233,229,225,221,217,212,207,202,197,192,187,181,176,171,166,160,154,149,143,137,132,126,121,98,93,78,67,63,
255,255,255,255,255,255,255,253,250,247,244,240,237,234,229,225,221,217,212,207,202,197,193,187,182,176,171,166,160,154,149,143,137,132,127,121,115,93,88,73,
255,255,255,255,255,255,252,250,247,244,240,237,234,229,226,222,216,212,207,203,198,193,187,182,176,171,166,160,155,149,143,138,132,126,120,115,109,104,98,88,82,68,58,54,
255,255,255,255,255,255,252,250,247,244,241,237,233,230,225,221,217,212,207,202,197,193,187,182,176,171,165,160,155,149,143,138,132,126,121,115,110,104,83,78,68,63,54,50,
255,255,255,255,252,250,247,244,240,237,234,229,225,221,217,212,207,202,197,193,188,182,176,171,166,160,154,149,143,138,132,126,121,115,109,104,98,78,73,68,59,50,46,
255,255,255,255,253,250,247,244,241,237,234,230,225,221,216,212,207,203,198,193,187,181,176,171,165,160,154,149,143,137,132,126,120,115,110,104,98,94,88,83,73,68,54,46,42,
217,212,207,203,197,176,171,165,160,154,149,143,138,132,127,
202,198,192,187,166,160,154,149,144,138,132,126,120,115,110,88,82,
255,255,255,255,255,252,250,247,244,237,233,229,225,221,217,212,207,202,198,192,187,182,176,171,165,160,155,149,143,138,132,126,121,115,110,104,98,93,88,82,78,72,68,63,58,54,50,46,41,38,34,31,28,
255,255,255,255,252,250,247,244,241,234,229,225,221,216,212,207,202,197,193,187,182,176,171,166,160,154,149,143,138,132,126,120,115,109,104,98,93,88,83,78,72,68,63,59,54,50,46,42,38,35,31,28,
255,255,255,252,250,247,244,240,237,229,225,221,216,212,207,202,198,192,187,182,176,171,165,160,154,149,143,138,132,127,121,115,109,104,98,93,88,83,78,73,67,63,58,54,50,46,41,38,34,31,28,25,
255,255,252,250,247,244,240,237,233,229,225,221,217,212,207,202,197,193,187,182,177,171,166,160,154,149,143,138,132,126,121,115,110,103,99,93,88,82,77,73,67,63,59,54,50,46,42,38,34,31,28,25,22,20,
255,255,255,253,250,247,244,240,237,230,225,221,217,212,208,202,197,192,187,181,176,171,165,160,155,149,143,138,132,126,121,115,109,104,99,93,88,82,77,73,68,63,59,54,50,46,42,38,35,31,28,25,
255,255,255,255,253,250,247,244,241,234,230,226,221,217,212,207,202,197,192,187,182,176,171,166,160,155,148,143,138,132,126,120,115,109,104,99,93,88,83,77,73,68,63,59,54,50,45,41,38,34,31,28,
255,255,255,255,255,252,250,248,244,241,237,233,229,225,221,217,212,208,202,198,192,187,182,176,171,165,160,154,149,143,137,132,127,121,115,109,104,98,93,88,82,77,73,68,63,58,54,50,45,42,38,34,31,28,
237,233,229,225,160,155,
237,226,
	};

#else

	static u8 timingdata[] =
	{
		76,71,67,61,0,0,46,42,0,0,27,22,0,12,0,2,0,0,0,0,0,0,0,0,
		0,0,71,66,0,0,51,47,0,0,31,27,0,17,0,7,3,0,0,0,0,0,0,0,
		0,0,75,70,0,0,56,50,0,0,36,31,0,22,0,12,7,0,0,0,0,0,0,0,
		89,84,79,74,0,0,60,55,0,0,41,35,31,26,0,16,11,6,0,0,0,0,0,0,
		93,89,0,0,0,0,64,60,0,0,44,40,0,30,0,20,16,0,5,0,0,0,0,0,
		98,93,0,0,0,0,68,64,0,0,49,44,0,35,0,25,20,0,10,0,0,0,0,0,
		102,97,92,87,0,78,73,68,63,0,54,48,44,39,0,29,24,19,0,0,4,0,0,0,
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		240,236,0,0,0,216,211,0,202,0,191,187,181,176,0,166,161,156,152,0,142,137,132,127,
		243,239,0,0,0,218,213,0,204,0,193,188,0,0,0,0,0,159,154,0,0,0,134,129,
		246,240,0,0,0,221,216,0,206,0,197,191,0,0,0,0,0,161,157,0,0,0,136,132,
		248,243,238,0,0,223,219,213,0,0,198,193,188,183,0,174,168,164,159,0,149,144,139,134,
		250,245,0,235,0,225,220,0,211,0,201,196,0,0,0,176,171,0,0,0,151,146,0,0,
		252,247,0,238,0,228,223,0,213,0,203,198,0,0,0,179,174,0,0,0,153,148,0,0,
		255,250,245,0,0,231,225,220,0,0,205,201,196,190,0,181,176,171,166,0,156,151,146,141,
	};	
 
#endif
 
	
    
    int i;
	f32 starttime, localframe;
	J3DTransformInfo diff;
	s16 alpha;

	alpha = (s16)(255*(1.0F-frame*1.4f));
	if( alpha< 0 )alpha = 0;
	sMenuColor1.a = (u8)alpha;

	for( i=0;i<*cubenum;i++ ){
		starttime = 0.4f*(1.0f-(f32)timingdata[i]/255);
		localframe = (frame - starttime);
		if( localframe < 0 ) localframe = 0; // 1を超えるほうはクランプしない

		pos[i].tx += -585*localframe;
		pos[i].ty += 200*localframe;
		pos[i].tz -= 500*localframe;
		scfRotateAround( 0, 0, 0,
						 (u16)(65535*localframe), 0, (u16)(65535*localframe),
						 &pos[i], &diff );
		pos[i].tx += diff.tx;
		pos[i].ty += diff.ty;
		pos[i].tz += diff.tz;
		pos[i].rx += diff.rx;
		pos[i].ry += diff.ry;
		pos[i].rz += diff.rz;
	}

}


static void
getStartFadeoutAnim( f32 frame, J3DTransformInfo *pos, u32 *cubenum )
{
#define CHANGEFRAME 0.1F

	// 初期位置を取得しとく
	getAnimCubePos( SCA_ANIMKIND_GAMEPLAY, 1, pos, cubenum );

	if( frame < CHANGEFRAME ){
		startFadeoutPushSection( frame*(1.0F/CHANGEFRAME), pos, cubenum );
	}else{
		switch( sStartFadeoutAnimKind ){
		case 0:
		case 1:
		case 2:
		case 3:
		case 4:
		default:
			startFadeoutMoveSection( frame-CHANGEFRAME, pos, cubenum );
			break;
		}
	}

}


/*---------------------------------------------------------------------------*
  Name:
  blend3Color( GXColor *c1, GXColor *c2, GXColor *c3, f32 s, f32 t, GXColor *dest )

  Description:
  パラメータs[0.0,1.0]でc1とc2を混ぜ、その結果とc3をパラメータt[0.0,1.0]で
  混ぜた結果をdestに入れます。

			   s=0  s=1
	   t=0      c2  c1
	   t=1      c3  c3


 *---------------------------------------------------------------------------*/
static void
blend3Color( GXColor *c1, GXColor *c2, GXColor *c3, f32 s, f32 t, GXColor *dest )
{
	dest->r = (u8)((1-t)*(s*c1->r + (1-s)*c2->r) + t*c3->r);
	dest->g = (u8)((1-t)*(s*c1->g + (1-s)*c2->g) + t*c3->g);
	dest->b = (u8)((1-t)*(s*c1->b + (1-s)*c2->b) + t*c3->b);
	dest->a = (u8)((1-t)*(s*c1->a + (1-s)*c2->a) + t*c3->a);
}




// 言語切り替え時のエフェクト
#ifdef PAL

static f32 getSFFrame(void);
static f32 getSFFrameInv(void);

static f32
getSFFrame(void)
{
	return (f32)sLangEffect.frame/LANGSELECTFRAME;
}

static f32
getSFFrameInv(void)
{
	return 1.0F - (f32)sLangEffect.frame/LANGSELECTFRAME;
}




void initLangSelectEffect( s32 lang )
{
	sLangEffect.frame = LANGSELECTFRAME;
	sLangEffect.oldlang = sLangEffect.newlang = (s16)lang;
	sLangEffect.dir.x = sLangEffect.dir.y = sLangEffect.dir.z = 0;
}



void
startLangSelectEffect( s32 oldlang, s32 newlang )
{
	int i;

	if( oldlang < newlang ){
		sLangEffect.dir.x = -1;
	}else{
		sLangEffect.dir.x = 1;
	}
		  
	sLangEffect.frame = 0;
	sLangEffect.oldlang = (s16)oldlang;
	sLangEffect.newlang = (s16)newlang;
	for( i=0;i<SOUNDZONENUM;i++ ){
		sLangEffect.randompos[i] = (u8)(urand()%60 + 20); // 数値に根拠はない
	}
}


void
makeLangSelectEffect(void)
{
	
	int i;

	u16 oldnum, newnum;
	u16 *oldidx, *newidx;

	switch( param.optionsound ){
	case 0://mono
		oldidx = scfGetMONOIndex_pal( sLangEffect.oldlang );
		oldnum = scfGetMONONum_pal( sLangEffect.oldlang );
		newidx = scfGetMONOIndex_pal( sLangEffect.newlang );
		newnum = scfGetMONONum_pal( sLangEffect.newlang );
		break;
	case 1://stereo
		oldidx = scfGetSTEREOIndex_pal( sLangEffect.oldlang );
		oldnum = scfGetSTEREONum_pal( sLangEffect.oldlang );
		newidx = scfGetSTEREOIndex_pal( sLangEffect.newlang );
		newnum = scfGetSTEREONum_pal( sLangEffect.newlang );
		break;
	}

	// 本物は消す
	for( i=0;i<scfGetSOUNDNum();i++ ){
		cubeAlpha[scfGetSOUNDIndex()[i]] = 0;
	}

#define EFFECTSIZE 0.2
	// 古いほうを作る
	for( i=0;i<oldnum;i++ ){
		cubePos[cubeNum+i] = cubePos[oldidx[i]];
		cubePos[cubeNum+i].tx -= getSFFrame()*sLangEffect.randompos[i]*sLangEffect.dir.x*EFFECTSIZE;
		cubeAlpha[cubeNum+i] = (u8)(getSFFrameInv()*255);
		cubeColor1[cubeNum+i] = sDarkerMenuColor1;
		cubeColor2[cubeNum+i] = sDarkerMenuColor2;
	}

	// キューブ数をふやす
	cubeNum += oldnum;

	// 新しいほうを作る
	for( i=0;i<newnum;i++ ){
		cubePos[cubeNum+i] = cubePos[newidx[i]];
		cubePos[cubeNum+i].tx += getSFFrameInv()*sLangEffect.randompos[i+oldnum]*sLangEffect.dir.x*EFFECTSIZE;
		
		cubeAlpha[cubeNum+i] = (u8)(getSFFrame()*255);
		cubeColor1[cubeNum+i] = sDarkerMenuColor1;
		cubeColor2[cubeNum+i] = sDarkerMenuColor2;
	}
	
	cubeNum += newnum;

	makeLangSelectOffsetEffect(); // オフセットのエフェクトをつける
	

}



/*---------------------------------------------------------------------------*
  Name:
  
  Description:
  オフセット部の、言語選択時のエフェクトを作る。変化が無いのですごく簡単。
              
 *---------------------------------------------------------------------------*/
void
makeLangSelectOffsetEffect(void)
{
	int i;

	u16 num;
	u16 *idx;

	idx = scfGetOFFSETIndex();
	num = scfGetOFFSETNum();

	// 古いほうを作る（本物をコピーする）
	for( i=0;i<num;i++ ){
		cubePos[cubeNum+i] = cubePos[idx[i]];
		cubePos[cubeNum+i].tx -= getSFFrame()*sLangEffect.randompos[i]*sLangEffect.dir.x*EFFECTSIZE;
		cubeAlpha[cubeNum+i] = (u8)(cubeAlpha[idx[i]]*getSFFrameInv());
		cubeColor1[cubeNum+i] = cubeColor1[idx[i]];
		cubeColor2[cubeNum+i] = cubeColor2[idx[i]];
	}

	// キューブ数をふやす
	cubeNum += num;

	// 新しいほうを作る（座標変更とアルファ設定だけ）
	for( i=0;i<num;i++ ){
		cubePos[idx[i]].tx += getSFFrameInv()*sLangEffect.randompos[i]*sLangEffect.dir.x*EFFECTSIZE;
		cubeAlpha[idx[i]] = (u8)(getSFFrame()*cubeAlpha[idx[i]]);
	}

	// キューブ数はふやさなくてよい

}

#endif
