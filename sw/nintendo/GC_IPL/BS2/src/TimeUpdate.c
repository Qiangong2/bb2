/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: メニューモード時の各面のパラメータ
  -----------------------------------------------------------------------------*/
#include <dolphin.h>
#include <private/OSRtc.h>
#include <math.h>
#include <string.h>

#include "gCont.h"
#include "gBase.h"
#include "gFont.h"
#include "gCamera.h"
#include "gInit.h"
#include "gTrans.h"
#include "gLoader.h"

#include "gLayoutRender.h"
#include "gModelRender.h"

#include "MenuUpdate.h"	// メインメニューの動作を決定するEnumが入っている。
#include "MenuClockAnim.h"
#include "TimeState.h"
#include "TimeUpdate.h"
#include "gMainState.h"
#include "SplashMain.h"

//#include "2d_calendar_2_Japanese.h"
#if defined(MPAL)
#include "mpal_mesg.h"
#elif defined(PAL)
#include "pal_mesg.h"
#else // NTSC
#include "ntsc_mesg.h"
#endif
#include "jaudio.h"

// start nakamura's edit
#include "smallCubeFader.h"
#include "smallCubeDraw.h"
// end

extern int	snprintf(char* s, size_t n, char * format,...);

/*-----------------------------------------------------------------------------
  コントローラの入力を外部参照する。
  -----------------------------------------------------------------------------*/
extern DIPadStatus	DIPad;

static gLayoutHeader*	cal_lay;
static gLRStringHeader*	cal_str;

static u32	cal_runstate;
static u32	pre_cal_runstate;
static s32	select_curstate;
static s32	day_curstate;
static s32	clock_curstate;
static int	edit_num[3];

static OSCalendarTime	edit;

static s16 win_alpha;
static s8 a_bounds;


static int YearDays[] =
{ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

static int LeapYearDays[] =
{ 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

/*-----------------------------------------------------------------------------
  static function
  -----------------------------------------------------------------------------*/
static BOOL isLeapYear(int year)
{
    return (year % 4 == 0 && year % 100 != 0) || year % 400 == 0;
}

static void
setTime( OSTime tick )
{
	__OSSetTime( tick );
}

static void
commitBias( OSTime tick )
{
	u32		sec;
	OSSram* sram;
	
	__OSGetRTC(&sec);
	sram = __OSLockSram();
	sram->counterBias = (u32)OSTicksToSeconds(tick) - sec;
	sram->flags |= OS_SRAM_CLOCKADJUST_FLAG;
	__OSUnlockSram(TRUE);
	while (!__OSSyncSram()) ;
}

static void
adjustTime( void )
{
	OSTime	tick;
	
	switch ( cal_runstate )
	{
	case IPL_TIME_DAY:
		tick = OSGetTime();
		OSTicksToCalendarTime( tick, &edit );

		edit.year = edit_num[0];
		edit.mon = edit_num[1];
		edit.mday = edit_num[2];
		
		break;
	case IPL_TIME_CLOCK:
		tick = OSGetTime();
		OSTicksToCalendarTime( tick, &edit );
		
		edit.hour = edit_num[0];
		edit.min = edit_num[1];
		edit.sec = edit_num[2];
		if ( edit.year > 2099 ) {
			edit.year = 2000;
		}
		break;

	default:
		return;
	}
	
	tick = OSCalendarTimeToTicks( &edit );
	setTime( tick );
	commitBias( tick );
}

static int
getCalendarLRFlag( void )
{
	int ret;
	switch ( gGetNowCountry() )
	{
	case COUNTRY_JAPAN:
	case COUNTRY_US:
		ret = 1;
		break;
	case COUNTRY_BRAZIL:
		ret = -1;
		break;
	default:
		ret = -1;
		break;
	}

	return ret;
}

/*-----------------------------------------------------------------------------
  初期化
  -----------------------------------------------------------------------------*/
void
initTimeMenu_Lang( void )
{
	cal_str = gGetBufferPtr( getID_2D_calendar_string() );
}
void
initTimeMenu( BOOL first )
{
	if ( first ) {
		cal_lay = gGetBufferPtr( getID_2D_calendar_layout() );
//		cal_str = gGetBufferPtr( getID_2D_calendar_string() );
		initTimeMenu_Lang();
	}

	cal_runstate = IPL_TIME_SELECTING;
	pre_cal_runstate = cal_runstate;
	select_curstate = 0;

	day_curstate   = 0;
	clock_curstate = 0;
	
	win_alpha = 100;
	a_bounds = 2;
}

/*-----------------------------------------------------------------------------
  コントローラの入力から更新する
  -----------------------------------------------------------------------------*/
static s32
updateTimeMenuControl( void )
{
	s32	ret = IPL_MENU_CLOCK_SELECT;

	if ( !isCubeAnimFinished( 0 ) ) return ret;
	
	if ( DIPad.dirsPressed & DI_STICK_UP ) {
		if ( select_curstate != 0 ) Jac_PlaySe( JAC_SE_UD_MOVE );
		select_curstate = 0;
	}
	if ( DIPad.dirsPressed & DI_STICK_DOWN ) {	// 上
		if ( select_curstate != 1 ) Jac_PlaySe( JAC_SE_UD_MOVE );
		select_curstate = 1;
	}

	if ( DIPad.buttonDown & PAD_BUTTON_A ) {
		Jac_PlaySe( JAC_SE_DECIDE );
		switch ( select_curstate )
		{
		case 0:
			// 日付
			OSTicksToCalendarTime( OSGetTime(), &edit );
			edit_num[0] = edit.year;
			edit_num[1] = edit.mon;
			edit_num[2] = edit.mday;
			pre_cal_runstate = IPL_TIME_DAY;
			break;
		case 1:
			// 時間
			OSTicksToCalendarTime( OSGetTime(), &edit );
			edit_num[0] = edit.hour;
			edit_num[1] = edit.min;
			edit_num[2] = 0;//edit.sec;
			pre_cal_runstate = IPL_TIME_CLOCK;
			break;
		}
	}
	
	if ( DIPad.buttonDown & PAD_BUTTON_B ) {
		MenuState*	mesp = getMenuState();
		if ( mesp->fadeout_z_param == 32767 || 
			 mesp->enable_pre_key_input ) {
			Jac_PlaySe( JAC_SE_TO_3D );
			ret = IPL_MENU_CLOCK;
		}
	}
	
	return ret;
}


/*-----------------------------------------------------------------------------
  日付コントローラの入力から更新する
  -----------------------------------------------------------------------------*/
static void
checkDayOverFlow( void )
{
	int	max_day;
	
	if ( edit_num[ 0 ] < 2000 ) edit_num[ 0 ] = 2099;
	if ( edit_num[ 0 ] > 2099 ) edit_num[ 0 ] = 2000;
	
	if ( edit_num[ 1 ] < 0   )  edit_num[ 1 ] = 11;
	if ( edit_num[ 1 ] > 11   ) edit_num[ 1 ] = 0;
	
	if ( isLeapYear( edit_num[ 0 ] ) ) {
		max_day = LeapYearDays[ edit_num[ 1 ] ];
	} else {
		max_day = YearDays[ edit_num[ 1 ] ];
	}
	if ( edit_num[ 2 ] > max_day ) {
		if ( day_curstate == 2 ) edit_num[ 2 ] = 1;
		else edit_num[ 2 ] = max_day;
	}
	if ( edit_num[ 2 ] < 1 ) edit_num[ 2 ] = max_day;
}

static s32
updateDayMenuControl( void )
{
	s32	ret = IPL_MENU_CLOCK_SELECT;
	
	if ( ( DIPad.dirsPressed & DI_STICK_LEFT ) &&
		 ( ( DIPad.dirsPressed & ( DI_STICK_DOWN | DI_STICK_UP ) ) == 0 ) ) {	// 左
		Jac_PlaySe( JAC_SE_LR_MOVE );
		day_curstate -= getCalendarLRFlag();
		if ( day_curstate < 0 ) day_curstate = 2;
		if ( day_curstate > 2 ) day_curstate = 0;
	}
	if ( ( DIPad.dirsPressed & DI_STICK_RIGHT ) && 
		 ( ( DIPad.dirsPressed & ( DI_STICK_DOWN | DI_STICK_UP ) ) == 0 ) ) {	// 右
		Jac_PlaySe( JAC_SE_LR_MOVE );
		day_curstate += getCalendarLRFlag();
		if ( day_curstate > 2 ) day_curstate = 0;
		if ( day_curstate < 0 ) day_curstate = 2;
	}
	if ( DIPad.dirsPressed & DI_STICK_DOWN ) {	// 下
		edit_num[ day_curstate ]--;
		Jac_PlaySe( JAC_SE_VAL_MOVE );
		// フローチェック
		checkDayOverFlow();
	}
	if ( DIPad.dirsPressed & DI_STICK_UP ) {	// 上
		edit_num[ day_curstate ]++;
		Jac_PlaySe( JAC_SE_VAL_MOVE );
		// フローチェック
		checkDayOverFlow();
	}

	if ( DIPad.buttonDown & PAD_BUTTON_A ) {
		Jac_PlaySe( JAC_SE_FINALDECIDE );
		adjustTime();
		pre_cal_runstate = IPL_TIME_SELECTING;
	}
	
	if ( DIPad.buttonDown & PAD_BUTTON_B ) {
		Jac_PlaySe( JAC_SE_CANCEL );
		pre_cal_runstate = IPL_TIME_SELECTING;
	}
	
	return ret;
}

/*-----------------------------------------------------------------------------
  日付コントローラの入力から更新する
  -----------------------------------------------------------------------------*/
static void
checkClockOverFlow( void )
{
	// フローチェック
	switch ( clock_curstate )
	{
	case 0:	// 時間
		if ( edit_num[ clock_curstate ] < 0 ) edit_num[ clock_curstate ] = 23;
		if ( edit_num[ clock_curstate ] > 23 ) edit_num[ clock_curstate ] = 0;
		break;
	case 1: // 分
		if ( edit_num[ clock_curstate ] < 0 ) edit_num[ clock_curstate ] = 59;
		if ( edit_num[ clock_curstate ] > 59 ) edit_num[ clock_curstate ] = 0;
		break;
	case 2: // 秒
		if ( edit_num[ clock_curstate ] < 0 ) edit_num[ clock_curstate ] = 59;
		if ( edit_num[ clock_curstate ] > 59 ) edit_num[ clock_curstate ] = 0;
		break;
	}
}
static s32
updateClockMenuControl( void )
{
	s32	ret = IPL_MENU_CLOCK_SELECT;
	
	if ( ( DIPad.dirsPressed & DI_STICK_LEFT ) &&
		 ( ( DIPad.dirsPressed & ( DI_STICK_DOWN | DI_STICK_UP ) ) == 0 ) ) {	// 左
		Jac_PlaySe( JAC_SE_LR_MOVE );
		clock_curstate --;
		if ( clock_curstate < 0 ) clock_curstate = 2;
		if ( clock_curstate > 2 ) clock_curstate = 0;
	}
	if ( ( DIPad.dirsPressed & DI_STICK_RIGHT ) &&
		 ( ( DIPad.dirsPressed & ( DI_STICK_DOWN | DI_STICK_UP ) ) == 0 ) ) {	// 左
		Jac_PlaySe( JAC_SE_LR_MOVE );
		clock_curstate ++;
		if ( clock_curstate > 2 ) clock_curstate = 0;
		if ( clock_curstate < 0 ) clock_curstate = 2;
	}
	
	if ( DIPad.dirsPressed & DI_STICK_DOWN ) {	// 下
		Jac_PlaySe( JAC_SE_VAL_MOVE );
		edit_num[ clock_curstate ]--;
		checkClockOverFlow();
	}
	if ( DIPad.dirsPressed & DI_STICK_UP ) {	// 上
		Jac_PlaySe( JAC_SE_VAL_MOVE );
		edit_num[ clock_curstate ]++;
		checkClockOverFlow();
	}
	
	if ( DIPad.buttonDown & PAD_BUTTON_A ) {
		Jac_PlaySe( JAC_SE_FINALDECIDE );
		adjustTime();
		pre_cal_runstate = IPL_TIME_SELECTING;
	}
	
	if ( DIPad.buttonDown & PAD_BUTTON_B ) {
		Jac_PlaySe( JAC_SE_CANCEL );
		pre_cal_runstate = IPL_TIME_SELECTING;
	}
	
	return ret;
}

/*-----------------------------------------------------------------------------
  
  -----------------------------------------------------------------------------*/
s32
updateTimeMenu( void )
{
	s32	ret = IPL_MENU_CLOCK_SELECT;
	SplashState* ssp = inSplashGetState();

	cal_runstate = pre_cal_runstate;

	if ( !ssp->force_fatalerror ) {
		switch ( cal_runstate )
		{
		case IPL_TIME_SELECTING:
			ret = updateTimeMenuControl();
			break;
		case IPL_TIME_DAY:
			ret = updateDayMenuControl();
			break;
		case IPL_TIME_CLOCK:
			ret = updateClockMenuControl();
			break;
		}
	}
	
	if ( isConcentOLSmallCube() ) {
	// start nakamura's edit
	updateTimeSmallCube( cal_runstate,
			       select_curstate,
			       day_curstate,
			       clock_curstate,
			       edit_num );
	updateSmallCubeDraw();
	// end
	}

	win_alpha += a_bounds;

	if ( win_alpha > 120 ) a_bounds = -2;
	if ( win_alpha <  50 ) a_bounds = 2;
	
	return ret;
}

s32
getTimeRunstate( void )
{
	if ( cal_runstate == IPL_TIME_SELECTING ) {
		return TRUE;
	}

	return FALSE;
}


































/*-----------------------------------------------------------------------------
  draw section
  -----------------------------------------------------------------------------*/
static void
drawZanteiMenu( u8 alpha )
{
	TimeState* tsp = getTimeState();
	gLRTextBoxData	day_tb =
	{ 'aaaa',
		292 * 16,180 * 16,	// 位置
		400 * 16,40 * 16,		// paneサイズ
		0xffffffff,0xffffffff,	// 色
		0,0,
		0,32,
		32,0xffff,0 };
	gLRTextBoxData	clock_tb = day_tb;
	char day[32], clock[32], non[32];
	u32	select_top_color = 0xffff0000;
	u32	select_bottom_color = 0x70700000;
	u32	not_top_color = 0xffffff00;
	u32	not_bottom_color = 0x70707000;
	int	y, m, d, h, i, s;

	day_tb.x   = (u16)tsp->mon_x;
	clock_tb.x = (u16)tsp->min_x;

	clock_tb.y = 320 * 16;
	OSTicksToCalendarTime( OSGetTime(), &edit );
	y = edit.year;
	m = edit.mon;
	d = edit.mday;
	h = edit.hour;
	i = edit.min;
	s = edit.sec;
	
	if ( cal_runstate == IPL_TIME_SELECTING ) {
		snprintf( day, 32, "%04d/%02d/%02d", y, m+1, d );
		snprintf( clock, 32, "%02d:%02d:%02d", h, i, s );
		if ( select_curstate == 0 ) {
			gfSetFontColor( select_top_color + alpha, select_bottom_color + alpha );
			gfPrint( &day_tb, day );
			gfSetFontColor( not_top_color + alpha / 2, not_bottom_color + alpha / 2 );
			gfPrint( &clock_tb, clock );
		} else {
			gfSetFontColor( not_top_color + alpha / 2, not_bottom_color + alpha / 2 );
			gfPrint( &day_tb, day );
			gfSetFontColor( select_top_color + alpha, select_bottom_color + alpha );
			gfPrint( &clock_tb, clock );
		}
	}
	else if ( cal_runstate == IPL_TIME_DAY ) {
		y = edit_num[0];
		m = edit_num[1];
		d = edit_num[2];
		day_tb.font_size = 40;
		snprintf( clock, 32, "%02d:%02d:%02d", h, i, s );
		switch ( day_curstate )
		{
		case 0:
		snprintf( day  , 32, "%04d/  /  ", y);
		snprintf( non  , 32, "     %02d %02d", m+1, d );
			break;
		case 1:
		snprintf( day  , 32, "    /%02d/  ", m+1 );
		snprintf( non  , 32, "%04d    %02d", y, d );
			break;
		case 2:
		snprintf( day  , 32, "    /  /%02d",  d );
		snprintf( non  , 32, "%04d %02d   ", y, m+1 );
			break;
		}
		gfSetFontColor( select_top_color + alpha, select_bottom_color + alpha );
		gfPrint( &day_tb, day );
		gfSetFontColor( not_top_color + alpha / 2, not_bottom_color + alpha / 2 );
		gfPrint( &clock_tb, clock );
		gfPrint( &day_tb, non );
	}
	else if ( cal_runstate == IPL_TIME_CLOCK ) {
		h = edit_num[0];
		i = edit_num[1];
		s = edit_num[2];
		clock_tb.font_size = 40;
		snprintf( day, 32, "%04d/%02d/%02d", y, m+1, d );
		switch ( clock_curstate )
		{
		case 0:
		snprintf( clock, 32, "%02d:  :  ", h );
		snprintf( non  , 32, "   %02d %02d", i, s );
			break;
		case 1:
		snprintf( clock, 32, "  :%02d:  ", i );
		snprintf( non  , 32, "%02d    %02d", h, s );
			break;
		case 2:
		snprintf( clock, 32, "  :  :%02d", s );
		snprintf( non  , 32, "%02d %02d   ", h, i );
			break;
		}
		gfSetFontColor( select_top_color + alpha, select_bottom_color + alpha );
		gfPrint( &clock_tb, clock );
		gfSetFontColor( not_top_color + alpha / 2, not_bottom_color + alpha / 2 );
		gfPrint( &day_tb, day );
		gfPrint( &clock_tb, non );
	}
	


}
static void
drawLayout( u8 alpha )
{
	TimeState* tsp = getTimeState();
	GXColor white = { 255, 255, 255, 255 };
	GXColor bar1 = { 255, 255,100,150 };

	white.a = alpha;

	bar1.a = alpha;//(u8)( win_alpha * alpha /255 );
	
	setGXforglrText();
	glrDrawString( MSG_C000, cal_str, cal_lay, &white );

	bar1.r = (u8)tsp->moji_r;
	bar1.g = (u8)tsp->moji_g;
	bar1.b = (u8)tsp->moji_b;
	glrDrawString( MSG_C001, cal_str, cal_lay, &bar1 );
	glrDrawString( MSG_C002, cal_str, cal_lay, &bar1 );
}

/*-----------------------------------------------------------------------------
  アイコン選択時の描画。
  -----------------------------------------------------------------------------*/
void
drawTimeMenu( u8 all_alpha , u8 grid_alpha , u8 layout_alpha )
{
	MainState* msp  = getMainState();
	J3DTransformInfo ti = { 1.f, -1.f, 1.f, 0,0,0,0, -292, 224, 0 };
	Vec     up      = {   0.f,   1.f,   0.f };
	Vec     camLoc	= {   0.f,   0.f,   0.f };
	Vec		objPt	= {   0.f,   0.f,   0.f };
	Mtx m, cm, inv;

	camLoc.z = 224.0f / tanf( msp->fovy * 3.141592f / 360.f ); // ( fovy / 2 )  *  (3.14 / 180)
    MTXLookAt( cm, &camLoc, &up, &objPt);
	gGetTransformMtx( &ti , m );
	MTXConcat( cm, m , m );
	gGetInverseTranspose( m, inv );
	GXLoadPosMtxImm( m , GX_PNMTX0 );
	GXLoadNrmMtxImm( inv , GX_PNMTX0 );
	GXSetCurrentMtx( GX_PNMTX0 );

	glrDraw2dGrid( m, (u8)grid_alpha );
	
	drawLayout( (u8)layout_alpha );

	//	drawZanteiMenu( alpha );
	if ( isConcentOLSmallCube() ) {
	// start nakamura's edit
	drawSmallCubeDraw( (u8)all_alpha );
	// end
	}
}
