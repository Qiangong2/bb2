/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: Menu ClockAnim関連ルーチン
  -----------------------------------------------------------------------------*/
#include <dolphin.h>

#include "gMath.h"
#include "gBase.h"
#include "gTrans.h"
#include "gCamera.h"
#include "gLoader.h"
#include "gCont.h"

#include "gModelRender.h"

#include "MenuClockAnim.h"
#include "MenuUpdate.h"
#include "TimeState.h"

/*-----------------------------------------------------------------------------

  -----------------------------------------------------------------------------*/
extern DIPadStatus	DIPad;

/*-----------------------------------------------------------------------------
  statics
  -----------------------------------------------------------------------------*/
static gmrModel		other[5];
static gmrModel		m_hari[4];
static gmrModel		h_hari[3];
static gmrModel		s_hari;
static OLAnimState	as_o[5];
static OLAnimState	as_m[4];
static OLAnimState	as_h[3];
static OLAnimState	as_s;


static s16 now_frame;

static BOOL sConcent;


/*-----------------------------------------------------------------------------
  CardCubeがアニメーションするために必要な設定
  -----------------------------------------------------------------------------*/
void
initOLAnim( gmrModelData* md , BOOL first )
{
	s16 i;

	if ( first ) {
		s_hari.gmrmd = md;
		buildModel( &s_hari , FALSE );

		for ( i = 0 ; i < 5 ; i++ ) copyModel( &other[i], &s_hari );
		for ( i = 0 ; i < 4 ; i++ ) copyModel( &m_hari[i], &s_hari );
		for ( i = 0 ; i < 3 ; i++ ) copyModel( &h_hari[i], &s_hari );
	}

	now_frame = 0;
	sConcent = FALSE;

	s_hari.modelAlpha = 0;
	for ( i = 0 ; i < 5 ; i++ ) other[i].modelAlpha = 0;
	for ( i = 0 ; i < 4 ; i++ ) m_hari[i].modelAlpha = 0;
	for ( i = 0 ; i < 3 ; i++ ) h_hari[i].modelAlpha = 0;
	
	initOLAnimState();
}

/*-----------------------------------------------------------------------------
  CardCubeがアニメーションするための初期値の設定
  -----------------------------------------------------------------------------*/
void
initOLAnimState( void )
{
	s16 i;
	now_frame = 0;
	
	for (i = 0 ; i < 5 ; i++ )	{
		as_o[i].x = 0;
		as_o[i].y = 0;
	}
	for (i = 0 ; i < 4 ; i++ )	{
		as_m[i].x = 0;
		as_m[i].y = 0;
	}
	for (i = 0 ; i < 3 ; i++ )	{
		as_h[i].x = 0;
		as_h[i].y = 0;
	}
	as_s.x = 0;
	as_s.y = 0;
}

void
updateOLAnime( void )
{
	s16 i;
	OSCalendarTime	time;
	OSTime tick = OSGetTime();
	
	OSTicksToCalendarTime( tick , &time );

	now_frame++;
	if ( now_frame > 50 )	now_frame = 50;
	
	for (i = 0 ; i < 5 ; i++ )	{
		int base = 100;
		if ( i == 0 ) base = 0;
		other[i].modelAlpha = (u8)( 255 * (( now_frame>30 )?30:now_frame) / 30 );
		as_o[i].x = base * gSSin( (s16)( (i-1) * 16384 ) );
		as_o[i].y = base * gSCos( (s16)( (i-1) * 16384 ) );
	}
	for (i = 0 ; i < 4 ; i++ )	{
		m_hari[i].modelAlpha = (u8)( 255 *(( now_frame>40 )?now_frame-40:0) / 10 );
		as_m[i].x = (i+1) * 20 * (( now_frame>40 )?now_frame-40:0) * gSSin( (s16)( -time.min * 65536 / 60 - time.sec * 65536 / 3600 ) ) / 10;
		as_m[i].y = (i+1) * 20 * (( now_frame>40 )?now_frame-40:0) * gSCos( (s16)( -time.min * 65536 / 60 - time.sec * 65536 / 3600 ) ) / 10;
	}
	for (i = 0 ; i < 3 ; i++ )	{
		h_hari[i].modelAlpha = (u8)( 255 * (( now_frame>40 )?now_frame-40:0) / 10 );
		as_h[i].x = (i+1) * 20 * (( now_frame>40 )?now_frame-40:0) * gSSin( (s16)( -(time.hour%12) * 65536 / 12 - time.min * 65536 / 720 ) ) / 10;
		as_h[i].y = (i+1) * 20 * (( now_frame>40 )?now_frame-40:0) * gSCos( (s16)( -(time.hour%12) * 65536 / 12 - time.min * 65536 / 720 ) ) / 10;
	}
	s_hari.modelAlpha = (u8)( 255 *  (( now_frame>40 )?now_frame-40:0) / 10 );
	as_s.x = 100 * gSSin( (s16)( -time.sec * 65536 / 60 - i ) );
	as_s.y = 100 * gSCos( (s16)( -time.sec * 65536 / 60 - i ) );
}

void
updateOLAnimeMtx( void )
{
	s16	i;
	Mtx currentMtx, r, trans, view;
//	u32	millisec;

	gGetTranslateRotateMtx(	0, -16384,0,0,0,0,r );
	MTXConcat( getMainMenuMtx() ,r, view );

//	millisec = OSTicksToMilliseconds( tick ) % 1000;
	
	for (i = 0 ; i < 5 ; i++ )	{
		MTXTrans( trans ,
				  as_o[i].x,	//base * gSSin( (s16)( (i-1) * 16384 ) ) ,
				  as_o[i].y,	//base * gSCos( (s16)( (i-1) * 16384 ) ) ,
				  0 );
		MTXConcat( view, trans, currentMtx );

		setBaseMtx( &other[i] , currentMtx ,
					(Vec){
						.7f * (( now_frame>30 )?30:now_frame) / 30 ,
						.7f * (( now_frame>30 )?30:now_frame) / 30 ,
						.7f * (( now_frame>30 )?30:now_frame) / 30
							});
		setViewMtx( &other[i] , getCurrentCamera() );
		updateModel( &other[i] );
	}

	for ( i=0 ; i<4 ; i++ ) {
		MTXTrans( trans,
				  as_m[i].x,	//(i+1) * 20 * (( now_frame>40 )?now_frame-40:0) * gSSin( (s16)( -time.min * 65536 / 60 - time.sec * 65536 / 3600 ) ) / 10,
				  as_m[i].y,	//(i+1) * 20 * (( now_frame>40 )?now_frame-40:0) * gSCos( (s16)( -time.min * 65536 / 60 - time.sec * 65536 / 3600 ) ) / 10,
				  0 );
		MTXConcat( view, trans, currentMtx );

		setBaseMtx( &m_hari[i] , currentMtx ,
					(Vec){
						.55f,// * now_frame / 30 ,
						.55f,// * now_frame / 30 ,
						.55f,// * now_frame / 30
							});
		setViewMtx( &m_hari[i] , getCurrentCamera() );
		updateModel( &m_hari[i] );
	}
	for ( i=0 ; i<3 ; i++ ) {
		MTXTrans( trans,
				  as_h[i].x,	//(i+1) * 20 * (( now_frame>40 )?now_frame-40:0) * gSSin( (s16)( -(time.hour%12) * 65536 / 12 - time.min * 65536 / 720 ) ) / 10,
				  as_h[i].y,	//(i+1) * 20 * (( now_frame>40 )?now_frame-40:0) * gSCos( (s16)( -(time.hour%12) * 65536 / 12 - time.min * 65536 / 720 ) ) / 10,
				  0 );
		MTXConcat( view, trans, currentMtx );

		setBaseMtx( &h_hari[i] , currentMtx ,
					(Vec){
						.55f,// * now_frame / 30 ,
						.55f,// * now_frame / 30 ,
						.55f// * now_frame / 30
							});
		setViewMtx( &h_hari[i] , getCurrentCamera() );
		updateModel( &h_hari[i] );
	}
	i = 0;
	MTXTrans( trans,
			  as_s.x,	//100 * gSSin( (s16)( -time.sec * 65536 / 60 - i ) ) ,//* (( now_frame>40 )?now_frame-40:0) / 10,
			  as_s.y,	//100 * gSCos( (s16)( -time.sec * 65536 / 60 - i ) ) ,//* (( now_frame>40 )?now_frame-40:0) / 10,
			  0 );
	MTXConcat( view, trans, currentMtx );

	setBaseMtx( &s_hari , currentMtx ,
				(Vec){
					.55f * (( now_frame>40 )?now_frame-40:0) / 10 ,
					.55f * (( now_frame>40 )?now_frame-40:0) / 10 ,
					.55f * (( now_frame>40 )?now_frame-40:0) / 10
						});
	setViewMtx( &s_hari , getCurrentCamera() );
	updateModel( &s_hari );
}

void
drawOLAnime( void )
{
	s16	i;
	TimeState*	tsp = getTimeState();
	GXColorS10	other_c, hari_c;
	GXColorS10	*backup;

	other_c.r = tsp->cube_other_r;
	other_c.g = tsp->cube_other_g;
	other_c.b = tsp->cube_other_b;
	other_c.a = tsp->cube_other_a;

	hari_c.r = tsp->cube_hari_r;
	hari_c.g = tsp->cube_hari_g;
	hari_c.b = tsp->cube_hari_b;
	hari_c.a = tsp->cube_hari_a;

	backup = other[0].gmrmd->material[0].tevColorPtr[0];
	other[0].gmrmd->material[0].tevColorPtr[0] = &other_c;
	for (i = 0 ; i < 5 ; i++ )	{
		drawModel( &other[i] );
	}
	other[0].gmrmd->material[0].tevColorPtr[0] = &hari_c;

	for (i = 0 ; i < 4 ; i++ )	{
		drawModel( &m_hari[i] );
	}
	for (i = 0 ; i < 3 ; i++ )	{
		drawModel( &h_hari[i] );
	}
	drawModel( &s_hari );
	
	other[0].gmrmd->material[0].tevColorPtr[0] = backup;
}

void
clearOLAnime( void )
{
	int i;
	int	all_zero = 0;

	for ( i = 0 ; i < 5 ; i++ ) {
		other[i].modelAlpha -= 10;
		if ( other[i].modelAlpha < 0 ) {
			other[i].modelAlpha = 0;
			all_zero ++;
		}
	}

	for ( i = 0 ; i < 4 ; i++ ) {
		m_hari[i].modelAlpha -= 10;
		if ( m_hari[i].modelAlpha < 0 ) {
			m_hari[i].modelAlpha = 0;
			all_zero ++;
		}
	}
	
	for ( i = 0 ; i < 3 ; i++ ) {
		h_hari[i].modelAlpha -= 10;
		if ( h_hari[i].modelAlpha < 0 ) {
			h_hari[i].modelAlpha = 0;
			all_zero ++;
		}
	}
	
	s_hari.modelAlpha -= 10;
	if ( s_hari.modelAlpha < 0 ) {
		s_hari.modelAlpha = 0;
		all_zero ++;
	}
	
	if ( all_zero == 13 ) {
		now_frame = 0;
	}
}

static s16
decAlphaforConcent( s16* ap )
{
	s16	alpha = *ap;
	s16 ret = 0;
	TimeState*	tsp = getTimeState();
	
	alpha -= tsp->concent_dec_alpha;
	if ( alpha < 0 ) {
		alpha = 0;
		ret = 1;
	}

	*ap = alpha;
	return ret;
}

static s16
decPosition( f32 *xp )
{
	f32	x = *xp;
	TimeState*	tsp = getTimeState();

	if ( x > 0 ) {
		x -= tsp->concent_dec_pos;
		if ( x < 0 ) x = 0;
	} else if ( x < 0 ) {
		x += tsp->concent_dec_pos;
		if ( x > 0 ) x = 0;
	}

	*xp = x;
	if ( x == 0 ) return 1;
	return 0;
}

void
concentOLAnime( void )
{
	int i;
	int	all_zero = 0;

	for ( i = 0 ; i < 5 ; i++ ) {
		decAlphaforConcent( &other[i].modelAlpha );
		all_zero += decPosition( &as_o[i].x );
		all_zero += decPosition( &as_o[i].y );
	}	// 10

	for ( i = 0 ; i < 4 ; i++ ) {
		decAlphaforConcent( &m_hari[i].modelAlpha );
		all_zero += decPosition( &as_m[i].x );
		all_zero += decPosition( &as_m[i].y );
	}	// 8
	
	for ( i = 0 ; i < 3 ; i++ ) {
		decAlphaforConcent( &h_hari[i].modelAlpha );
		all_zero += decPosition( &as_h[i].x );
		all_zero += decPosition( &as_h[i].y );
	}	// 6
	
	decAlphaforConcent( &s_hari.modelAlpha );
	all_zero += decPosition( &as_s.x );
	all_zero += decPosition( &as_s.y ); // 2
	
	if ( all_zero == 26 ) {
		now_frame = 0;
		sConcent = TRUE;
	}
	else {
		sConcent = FALSE;
	}
}

BOOL
isConcentOLSmallCube( void )
{
	return sConcent;
}
