/*---------------------------------------------------------------------------*
  Project: BS2
  File:    BS2Init.c

  Copyright 1998-2003 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: BS2Init.c,v $
  Revision 1.1.1.1  2004/06/01 21:17:32  paulm
  GC IPLROM source from Nintendo

    
    11    3/19/03 17:39 Shiki
    Modified to use __OSFPRInit() instead of BS2FPRInit().
    Modified to override library default version strings to save space.

    10    2/26/03 9:33 Shiki
    Clean up. Moved BS2FPRInit() to BS2Mach.c.

    9     2/24/03 17:09 Shiki
    Revised BS2FPRInit() to initialize FPR as well as HPR.
    Merged dumb InitMetroTRK(),EnableMetroTRKInterrupts(),OSReport() and
    OSPanic() to save IPL image size.

    8     11/19/01 21:18 Shiki
    Modified BS2Init() to make it set __OSInIPL to TRUE.

    7     11/15/01 11:00 Shiki
    Modified SetConsoleInfo() to set default simulated memory size.

    6     01/03/15 12:13 Shiki
    BS2Init() Initializes FPRs to known values to avoid bug in Gekko.

    5     01/01/30 14:55 Shiki
    Modified BS2Init() to initialize __PADFixBits for Wavebirds.

    4     12/14/00 7:55p Shiki
    Fixed SetConsoleInfo to check Flipper's chipid.

    3     10/31/00 4:29p Shiki
    Fixed to clear extra lomem.

    2     9/11/00 3:37p Shiki
    Revised SetConsoleInfo() for production board.

    1     9/08/00 4:07p Tian
    Initial checkin to production tree

    11    9/07/00 6:39p Shiki
    Added HW2 setting code.

    10    7/27/00 3:03p Tian
    ORCA uses backwards logic on the DI dip switches.  Fixed it so it
    detected 2 or 4 chip SPLASH systems correctly.

    9     6/26/00 6:38p Eugene
    Removed references to ARAM size check (just a header file include).
    We've decided to remove the ARAM size check process from BS2 entirely.
    If RD3 needs to use ARAM, they'll have to link in the ARAM drivers
    anyway, so...

    8     5/18/00 9:23a Shiki
    Fixed BS2Init() not to call cache routine.

    7     5/10/00 4:41p Shiki
    Modified SetConsoleInfo() for DRIP system.

    6     5/10/00 2:27p Hashida
    Removed Orca definitions as possible.

    5     4/06/00 5:09p Shiki
    Separated ARAM code.

    4     3/13/00 11:25a Eugene
    pragma'd unused arguments for functions that are excluded for non-ORCA
    builds.

    3     3/10/00 5:34p Eugene
    Added ARAM size-check code. Did NOT add call to this code yet,
    however.

    2     1/06/00 11:01p Hashida
    Changed to initialize both cached and uncached area for 0x0 - 0xff for
    non-spruce.

    1     12/15/99 5:00p Shiki
    Initial check-in.
 *---------------------------------------------------------------------------*/

#include <dolphin/doldefs.h>

// Override library default version strings to save space.
#undef  DOLPHIN_LIB_VERSION
#define DOLPHIN_LIB_VERSION(lib)    \
    const char* __ ## lib ## Version = ""

DOLPHIN_LIB_VERSION(OS);
DOLPHIN_LIB_VERSION(EXI);
DOLPHIN_LIB_VERSION(SI);
DOLPHIN_LIB_VERSION(DVD);
DOLPHIN_LIB_VERSION(VI);
DOLPHIN_LIB_VERSION(PAD);
DOLPHIN_LIB_VERSION(AI);
DOLPHIN_LIB_VERSION(AR);
DOLPHIN_LIB_VERSION(GX);

//  $NoKeywords: $

#include <string.h>

#include <dolphin/os.h>
#include <dolphin/pad.h>
#include <dolphin/os/OSBootInfo.h>
#include <dolphin/base/PPCArch.h>
#include <private/OSLoMem.h>
#include <private/flipper.h>
#include <secure/boot.h>
#include "BS2Private.h"

void InitMetroTRK( void );
void EnableMetroTRKInterrupts( void );

extern BOOL __OSInIPL;  // TRUE only in IPL

/*---------------------------------------------------------------------------*
  Name:         ClearOtherBATs

  Description:  Deactivate BAT registers that are not used by default.
                (e.g. BS1 uses BAT3 to access BOOT ROM.)

  Arguments:    None.

  Returns:      None.
 *---------------------------------------------------------------------------*/
static asm void ClearOtherBATs(void)
{
    nofralloc
    // Deactivate rest of BATs
    isync
    li      r4, 0x0
#ifndef SPRUCE
    // Spruce needs to enable the PCI and I/O space starting at F8000000
    // for serial output mapped via BAT2
    mtspr   DBAT2L, r4
    mtspr   DBAT2U, r4
#endif  // SPRUCE
    mtspr   DBAT3L, r4
    mtspr   DBAT3U, r4
    mtspr   IBAT1L, r4
    mtspr   IBAT1U, r4
    mtspr   IBAT2L, r4
    mtspr   IBAT2U, r4
    mtspr   IBAT3L, r4
    mtspr   IBAT3U, r4
    isync
    blr
}

/*---------------------------------------------------------------------------*
  Name:         SetConsoleInfo

  Description:  Sets the memory size and console type in boot info

  Arguments:    None.

  Returns:      None.
 *---------------------------------------------------------------------------*/
static void SetConsoleInfo(void)
{
    OSBootInfo* bi;

    bi = (OSBootInfo*) OSPhysicalToCached(OS_BOOTINFO_ADDR);

    bi->memorySize = BOOT_SYSTEM_MEMORY;

#if defined(MINNOW)
    bi->consoleType = OS_CONSOLE_MINNOW;
#elif defined(SPRUCE)
    bi->consoleType = OS_CONSOLE_ARTHUR;
#else
    bi->consoleType = OS_CONSOLE_RETAIL1;
    bi->consoleType += (__PIRegs[PI_REG_CHIPID / 4] & 0xf0000000) >> 28;
#endif

    // Set simulated memory size to default so BAT resisters to be configured
    // properly in OSInit().
    *((u32*) (OS_BASE_CACHED + OS_SIMULATEDMEM_SIZE)) = bi->memorySize;
}

/*---------------------------------------------------------------------------*
  Name:         BS2Init

  Description:  Performs basic MPU, RAM initialization (not performed by BS1).
                This function must be called at the very beginning of BS2
                (before OSInit in particular).

  Arguments:    None.

  Returns:      None.
 *---------------------------------------------------------------------------*/
void BS2Init(void)
{
#if DEBUG_BSS
    // Force memory to 0xFF's (for testing purposes only)
    memset(OSPhysicalToCached(0x0), 0xff, OSCachedToPhysical(BOOT_BS2_BASE));
#endif

    // Low memory must be initialized before OSInit or DVDInit
    memset(OSPhysicalToCached(0x0000), 0, 0x0100);
    memset(OSPhysicalToCached(0x3000), 0, 0x0100);
    ClearOtherBATs();
    SetConsoleInfo();

    // For WaveBird IPL initialization
    __PADFixBits = (u32) ~0;

    // For OS initialization
    __OSInIPL = TRUE;

    __OSFPRInit();

    // Initializes performance monitor registers
    PPCMtmmcr0(MMCR0_PMC1_HOLD | MMCR0_PMC2_HOLD);
    PPCMtmmcr1(MMCR1_PMC3_HOLD | MMCR1_PMC4_HOLD);
    PPCMtpmc1(0);
    PPCMtpmc2(0);
    PPCMtpmc3(0);
    PPCMtpmc4(0);
}

/*---------------------------------------------------------------------------*
    The following functions are defined to save ROM image size.
 *---------------------------------------------------------------------------*/

#ifdef NDEBUG

void InitMetroTRK(void)
{

}

void EnableMetroTRKInterrupts(void)
{

}

void OSReport(const char* msg, ...)
{
    #pragma unused(msg)
}

void OSPanic(const char* file, int line, const char* msg, ...)
{
    #pragma unused(file, line, msg)
    PPCHalt();
}



#endif
