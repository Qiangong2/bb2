/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: J3DBinary 1.5 Renderer
  
  Math関連
  -----------------------------------------------------------------------------*/
#include <math.h>

#include <dolphin.h>

#include "gBase.h"
#include "gMath.h"

static u16	gSinTableSize = 0;
static int	gSinShift;

static f32*	gSinTable;
static f32*	gCosTable;

/*---------------------------------------------------------------------------*
gNewSinTable

	int	gNewSinTable(u8 size);

	size	作成したいサインテーブルのサイズ（２のべき乗の数0-15）

	size分のサインテーブルを作成します。
	（サイズ分のf32の領域を内部でヒープに確保します）
	0から2のsize乗で0度から360度までを表わします

  返り値
  	TRUE	作成できた
  	FALSE	作成できなかった

 *---------------------------------------------------------------------------*/
int
gNewSinTable(u8 size)
{
	u8 n;
	u16 ct;
	
    gSinShift = 16-size;

    gSinTableSize = 1;
    for (n=0; n<size; n++) {
        gSinTableSize*=2;
    }

//    gSinTable = new f32[gSinTableSize+gSinTableSize/4];
	gSinTable = (float *)gAlloc( (gSinTableSize+gSinTableSize/4)*sizeof(f32) );

	if (gSinTable == NULL)	return FALSE;

    for (ct=0; ct<gSinTableSize+gSinTableSize/4; ct++)
	{
        gSinTable[ct] = sinf( 2.0f * gPI/gSinTableSize * ct );
    }

    gCosTable = gSinTable+gSinTableSize/4;
    return TRUE;
}

/*---------------------------------------------------------------------------*
gDeleteSinTable

	void	gDeleteSinTable(void);

	gNewSinTableで確保したサインテーブル領域を開放します。

 *---------------------------------------------------------------------------*/
void
gDeleteSinTable(void)
{
    OSFree ( gSinTable );
    gSinTable = NULL;
    gCosTable = NULL;
}

/*---------------------------------------------------------------------------*
gSSin

	f32	gSSin(s16 angle);

	angle	角度

	サイン値を返します。
  	この関数を呼ぶ前に必ずgNewSinTableでサインテーブルが
        作成されていなければなりません。
  	angleが何度を意味するかは、サインテーブルのサイズによります。

 *---------------------------------------------------------------------------*/
f32
gSSin(s16 angle)
{
	return (gSinTable[(u16)angle>>gSinShift]);
}
// インライン化

/*---------------------------------------------------------------------------*
gSCos

	f32	gSCos(s16 angle)

	angle	角度

	コサイン値を返します。
  	この関数を呼ぶ前に必ずgNewSinTableでサインテーブルが
        作成されていなければなりません。
  	angleが何度を意味するかは、サインテーブルのサイズによります。

 *---------------------------------------------------------------------------*/
f32
gSCos(s16 angle)
{
	return (gCosTable[(u16)angle>>gSinShift]);
}
// インライン化

/*---------------------------------------------------------------------------*
gEulerToQuat

	void gEulerToQuat(s16 roll, s16 pitch, s16 yaw, Quaternion* quat);

	roll	Ｘ軸回転（？）

  	pitch	Ｙ軸回転（？）

  	yaw	Ｚ軸回転（？）

  	quat	クオータニオン（返り値）

  	オイラー角からクオータニオンを求めます。

 *---------------------------------------------------------------------------*/
void gEulerToQuat(s16 roll, s16 pitch, s16 yaw, Quaternion* quat)
{
    float cr, cp, cy, sr, sp, sy, cpcy, spsy;

    // calculate trig identities
    cr = gSCos((s16)(roll/2));
    cp = gSCos((s16)(pitch/2));
    cy = gSCos((s16)(yaw/2));

    sr = gSSin((s16)(roll/2));
    sp = gSSin((s16)(pitch/2));
    sy = gSSin((s16)(yaw/2));

    cpcy = cp*cy;
    spsy = sp*sy;

    quat->w = cr*cpcy+sr*spsy;
    quat->x = sr*cpcy-cr*spsy;
    quat->y = cr * sp * cy + sr * cp * sy;
    quat->z = cr * cp * sy - sr * sp * cy;
}

/*---------------------------------------------------------------------------*
gQuatLerp

	void gQuatLerp(Quaternion* from, Quaternion* to, float t, Quaternion* res);

	from	補完したいクオータニオン

  	to	補完したいクオータニオン

	t	補完値(0->from, 1->to)

  	res	求まるクオータニオン（返り値）

	クオータニオンの線形補完(Linear intERPoration)を行います。

 *---------------------------------------------------------------------------*/
void gQuatLerp(Quaternion* from, Quaternion* to, float t, Quaternion* res)
{
    float           to1[4];
    double          cosom;
    double          scale0, scale1;

    // calc cosine
    cosom = from->x * to->x + from->y * to->y + from->z * to->z
        + from->w * to->w;

    // adjust signs (if necessary)
    if ( cosom < 0.0 ) {
        to1[0] = - to->x;
        to1[1] = - to->y;
        to1[2] = - to->z;
        to1[3] = - to->w;
    } else  {
        to1[0] = to->x;
        to1[1] = to->y;
        to1[2] = to->z;
        to1[3] = to->w;
    }

    // interpolate linearly
    scale0 = 1.0 - t;
    scale1 = t;
 
    // calculate final values
    res->x = (float)(scale0 * from->x + scale1 * to1[0]);
    res->y = (float)(scale0 * from->y + scale1 * to1[1]);
    res->z = (float)(scale0 * from->z + scale1 * to1[2]);
    res->w = (float)(scale0 * from->w + scale1 * to1[3]);
}



/*---------------------------------------------------------------------------*
gHermiteInterpolation

	float gHermiteInterpolation(float time, float f0, float v0, float s0, float f1, float v1, float s1);

  	time	値を求めたいカレントフレーム

  	f0	timeを挟み込む前のキーフレームのフレーム値

	v0	timeを挟み込む前のキーフレームのデータ値

	s0	timeを挟み込む前のキーフレームの右スロープ値

  	f1	timeを挟み込む後のキーフレームのフレーム値

	v1	timeを挟み込む後のキーフレームのデータ値

	s1	timeを挟み込む後のキーフレームの左スロープ値

  	エルミート補完を行います。
  
	// 一般性には欠くが、Softimage固有の値1.f/30.fをtime,f0,f1かける様にしておく。

 *---------------------------------------------------------------------------*/
float gHermiteInterpolation(float time, float f0, float v0, float s0, float f1, float v1, float s1)
{
    float t1 = time-f0;
    float t2 = 1.f/(f1-f0);

    float result;
    
    float t1t1t2=t1*t1*t2;
    float t1t1t2t2=t1t1t2*t2;
    float t1t1t1t2t2=t1*t1t1t2t2;
    float t1t1t1t2t2t2=t1t1t1t2t2*t2;

    result = v0*(2*t1t1t1t2t2t2 - 3*t1t1t2t2 + 1)
          + v1*(-2*t1t1t1t2t2t2 + 3*t1t1t2t2)
          + s0*(t1t1t1t2t2 - 2*t1t1t2 + t1)
          + s1*(t1t1t1t2t2 - t1t1t2);

    return result;
}

/*---------------------------------------------------------------------------*
gLagrangeInterpolation

	float gLagrangeInterpolation(int n, float* xData, float* yData, float xVal);

  	n	キーの個数

  	xData	キーフレームのフレーム値列へのポインタ
  	
	yData	キーフレームのデータ値列へのポインタ

  	xVal	値を求めたいカレントフレーム

	ラグランジェ補完を行います。
	(３Ｄライブラリでは曲線補完はエルミート補完で処理されており、
         ラグランジェ補完は使用されていません)

 *---------------------------------------------------------------------------*/
float gLagrangeInterpolation(int n, float* xData, float* yData, float xVal)
{
    float* b = (float*)gAlloc( sizeof(float)*n );
    float yVal;
    float x,y,p;

    int	i,j;

    for (i=0; i<n; i++) {
        x = xData[i];
        y = yData[i];
        p = 1.f;

        for (j=0; j<n; j++) {
            if (j==i) { continue; }
            p *= (x - xData[j]);
        }
        b[i] = y/p;
    }

    yVal = 0.f;

    for (i=0; i<n; i++) {
        p = 1.f;
        for (j=0; j<n; j++) {
            if (j==i) { continue; }
            p *= (xVal - xData[j]);
        }
        yVal += (b[i]*p);
    }
    
    OSFree( b );

    return (yVal);
}
