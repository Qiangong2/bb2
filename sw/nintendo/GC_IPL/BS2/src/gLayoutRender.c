/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: 2D LayoutRenderer
  -----------------------------------------------------------------------------*/
#include <dolphin.h>

#include "gBase.h"
#include "gInit.h"
#include "gTrans.h"
#include "gFont.h"
#include "gCamera.h"
#include "gLoader.h"

#include "gMainState.h"

#include "gLayoutRender.h"
#include "gMRMaterial.h"

#include "MenuUpdate.h"


static ResTIMG*	sTexImage;

/*-----------------------------------------------------------------------------
  
  -----------------------------------------------------------------------------*/
static void	glrDrawPictureID( u16 i ,gLayoutHeader* header, GXColor* rasc, ResTIMG* image_ptr, s16 offset_x, s16 offset_y );
static void	setGXforglrWindowContents( void );
static void	setGXforglrWindowEdge( void );

/*-----------------------------------------------------------------------------
  テクスチャイメージをセットします。
  テクスチャイメージは、全レイアウトで共通です。
  -----------------------------------------------------------------------------*/
void
setTexImage( ResTIMG*	image )
{
	sTexImage = image;
}

/*-----------------------------------------------------------------------------
  テクスチャを登録します。（エラーチェック以外、特に何もしてません。）
  -----------------------------------------------------------------------------*/
void
glrBuildTexImage( void* tex )
{
	gTextureHeader*	thp	= (gTextureHeader*)tex;
	ResTIMG*	img	= (ResTIMG*)( (u8*)tex + sizeof(gTextureHeader) );

	if ( thp->type != 'TXH0' ) {
		DIReport("gLayoutRender : not texture\n");
		return;
	}

	setTexImage( img );
}

/*-----------------------------------------------------------------------------
  テクスチャ表示用にGXを初期化します。
  -----------------------------------------------------------------------------*/
void
setGXforglrPicture( void )
{
	gSetGXforDirect( TRUE, FALSE, FALSE );
}

/*-----------------------------------------------------------------------------
  文字表示用にGXを初期化します。(3D上にフォントを表示するときに使用します。)
  -----------------------------------------------------------------------------*/
void
setGXforglrText( void )
{
	gfSetFontSetting();
	gSetGXforDirect( TRUE, TRUE, FALSE );
}

/*-----------------------------------------------------------------------------
  ウィンドウコンテンツ領域用のGXの設定をします。
  -----------------------------------------------------------------------------*/
static void
setGXforglrWindowContents( void )
{
	gSetGXforDirect( FALSE, TRUE, FALSE );
}

/*-----------------------------------------------------------------------------
  ウィンドウの枠用のGXの設定をします。
  -----------------------------------------------------------------------------*/
static void
setGXforglrWindowEdge( void )
{
	gSetGXforDirect( TRUE, FALSE, FALSE );
}

/*-----------------------------------------------------------------------------
  ウィンドウの四隅のテクスチャを描画します。
  -----------------------------------------------------------------------------*/
static void
drawWindowCorner( s16 x, s16 y, s16 dx, s16 dy, u16 tex_id, u8 mirror )
{

	u16		texTop, texBottom, texLeft, texRight;

	if ( mirror & 1 ) {
		texTop = 16384;
		texBottom = 0;
	}
	else {
		texTop = 0;
		texBottom = 16384;
	}

	if ( mirror & 2 ) {
		texLeft = 16384;
		texRight = 0;
	}
	else {
		texLeft = 0;
		texRight = 16384;
	}

	loadTexture( 0, &sTexImage[ tex_id ] );

	GXBegin(GX_QUADS, GX_VTXFMT1, 4);
	GXPosition3s16((s16)x		, (s16)y		, 0 );	GXTexCoord2u16((u16)texLeft , (u16)texTop		);
	GXPosition3s16((s16)(x + dx), (s16)y		, 0 );	GXTexCoord2u16((u16)texRight, (u16)texTop		);
	GXPosition3s16((s16)(x + dx), (s16)(y + dy)	, 0 );	GXTexCoord2u16((u16)texRight, (u16)texBottom	);
	GXPosition3s16((s16)x		, (s16)(y + dy)	, 0 );	GXTexCoord2u16((u16)texLeft , (u16)texBottom	);
	GXEnd();
}
/*-----------------------------------------------------------------------------
  ウィンドウのエッジを描画します。
  -----------------------------------------------------------------------------*/
static void
drawWindowEdge( s16 x, s16 y, s16 dx, s16 dy, u16 tex_id, u8 mirror, u8 hori )
{

	u16		texTop, texBottom, texLeft, texRight;

	if ( mirror & 1 ) {
		texTop = 16384;
		texBottom = 0;
	}
	else {
		texTop = 0;
		texBottom = 16384;
	}

	if ( mirror & 2 ) {
		texLeft = 16384;
		texRight = 0;
	}
	else {
		texLeft = 0;
		texRight = 16384;
	}

	loadTexture( 0, &sTexImage[ tex_id ] );

	if ( hori ) {
		GXBegin(GX_QUADS, GX_VTXFMT1, 4);
		GXPosition3s16((s16)x		, (s16)y		, 0 );	GXTexCoord2u16((u16)texLeft , (u16)texTop		);
		GXPosition3s16((s16)(x + dx), (s16)y		, 0 );	GXTexCoord2u16((u16)texLeft , (u16)texTop		);
		GXPosition3s16((s16)(x + dx), (s16)(y + dy)	, 0 );	GXTexCoord2u16((u16)texLeft , (u16)texBottom	);
		GXPosition3s16((s16)x		, (s16)(y + dy)	, 0 );	GXTexCoord2u16((u16)texLeft , (u16)texBottom	);
		GXEnd();
	}
	else {
		GXBegin(GX_QUADS, GX_VTXFMT1, 4);
		GXPosition3s16((s16)x		, (s16)y		, 0 );	GXTexCoord2u16((u16)texLeft , (u16)texTop		);
		GXPosition3s16((s16)(x + dx), (s16)y		, 0 );	GXTexCoord2u16((u16)texRight, (u16)texTop		);
		GXPosition3s16((s16)(x + dx), (s16)(y + dy)	, 0 );	GXTexCoord2u16((u16)texRight, (u16)texTop		);
		GXPosition3s16((s16)x		, (s16)(y + dy)	, 0 );	GXTexCoord2u16((u16)texLeft , (u16)texTop		);
		GXEnd();
	}
}

/*-----------------------------------------------------------------------------
  テクスチャを描画します。
  -----------------------------------------------------------------------------*/
void
glrDrawTexture( s16 x, s16 y, ResTIMG* timg )
{
	s16		dx, dy;

	dx = (s16)( timg->width  * 8 );
	dy = (s16)( timg->height * 8 );
	
	setGXforglrPicture();
	loadTexture( 0, timg );
	GXBegin(GX_QUADS, GX_VTXFMT1, 4);
	GXPosition3s16((s16)(x - dx), (s16)(y - dy) , 0 );	GXTexCoord2u16((u16)0    , (u16)0		);
	GXPosition3s16((s16)(x + dx), (s16)(y - dy) , 0 );	GXTexCoord2u16((u16)16384, (u16)0		);
	GXPosition3s16((s16)(x + dx), (s16)(y + dy)	, 0 );	GXTexCoord2u16((u16)16384, (u16)16384	);
	GXPosition3s16((s16)(x - dx), (s16)(y + dy)	, 0 );	GXTexCoord2u16((u16)0    , (u16)16384	);
	GXEnd();
}

/*-----------------------------------------------------------------------------
  場所を指定して、ウィンドウを描画します。
  idで指定したスタイルで描画しますが、位置・サイズは無視されます。
  -----------------------------------------------------------------------------*/
void
glrDrawWindowPosID(
	u16 id ,
	gLayoutHeader*	header,
	GXColor* 		rasc,
	int contents_x,
	int contents_y,
	int contents_dx,
	int contents_dy
	)
{
	u32		color_lu, color_ru, color_ld, color_rd;
	u32		alpha_lu, alpha_ru, alpha_ld, alpha_rd;
	s16		tex_width, tex_height;
	gLRWindowData* wdp = (gLRWindowData*)( (u32)header + (u32)header->window_offset );

	alpha_lu = ( wdp[id].color_contents_lu & 0xff ) * rasc->a / 255;
	alpha_ru = ( wdp[id].color_contents_ru & 0xff ) * rasc->a / 255;
	alpha_ld = ( wdp[id].color_contents_ld & 0xff ) * rasc->a / 255;
	alpha_rd = ( wdp[id].color_contents_rd & 0xff ) * rasc->a / 255;
	if ( alpha_lu > 0xff ) alpha_lu = 0xff;
	if ( alpha_ru > 0xff ) alpha_ru = 0xff;
	if ( alpha_ld > 0xff ) alpha_ld = 0xff;
	if ( alpha_rd > 0xff ) alpha_rd = 0xff;

	color_lu = ( wdp[id].color_contents_lu & 0xffffff00 ) | alpha_lu;
	color_ru = ( wdp[id].color_contents_ru & 0xffffff00 ) | alpha_ru;
	color_ld = ( wdp[id].color_contents_ld & 0xffffff00 ) | alpha_ld;
	color_rd = ( wdp[id].color_contents_rd & 0xffffff00 ) | alpha_rd;
	
	// コンテンツ領域の描画。
	setGXforglrWindowContents();
	GXBegin(GX_QUADS, GX_VTXFMT1, 4);
		GXPosition3s16((s16)contents_x 				  , (s16)contents_y					, 0 );	GXColor1u32( (u32)color_lu );
		GXPosition3s16((s16)(contents_x + contents_dx), (s16)contents_y					, 0 );	GXColor1u32( (u32)color_ru );
		GXPosition3s16((s16)(contents_x + contents_dx), (s16)(contents_y + contents_dy)	, 0 );	GXColor1u32( (u32)color_rd );
		GXPosition3s16((s16)contents_x 				  , (s16)(contents_y + contents_dy)	, 0 );	GXColor1u32( (u32)color_ld );
	GXEnd();

	// ウィンドウエッジの描画。
	setGXforglrWindowEdge();
	GXSetChanMatColor( GX_COLOR0A0, *rasc );
	
	tex_width  = (s16)( sTexImage[ wdp[id].id_texture_lu ].width  * 16 );
	tex_height = (s16)( sTexImage[ wdp[id].id_texture_lu ].height * 16 );
	drawWindowCorner( (s16)( contents_x - tex_width   ), (s16)( contents_y - tex_height  ), tex_width, tex_height, wdp[id].id_texture_lu, (u8)( wdp[id].texture_mirror >> 6 ) );
	drawWindowCorner( (s16)( contents_x + contents_dx ), (s16)( contents_y - tex_height  ), tex_width, tex_height, wdp[id].id_texture_ru, (u8)( wdp[id].texture_mirror >> 4 ) );
	drawWindowCorner( (s16)( contents_x - tex_width   ), (s16)( contents_y + contents_dy ), tex_width, tex_height, wdp[id].id_texture_ld, (u8)( wdp[id].texture_mirror >> 2 ) );
	drawWindowCorner( (s16)( contents_x + contents_dx ), (s16)( contents_y + contents_dy ), tex_width, tex_height, wdp[id].id_texture_rd, (u8)( wdp[id].texture_mirror      ) );

	drawWindowEdge( (s16)contents_x, (s16)( contents_y - tex_height  ), (s16)contents_dx, (s16)tex_height  , wdp[id].id_texture_ru, (u8)( wdp[id].texture_mirror >> 4 ), (u8)TRUE );
	drawWindowEdge( (s16)contents_x, (s16)( contents_y + contents_dy ), (s16)contents_dx, (s16)tex_height  , wdp[id].id_texture_rd, (u8)( wdp[id].texture_mirror      ), (u8)TRUE );
	drawWindowEdge( (s16)( contents_x - tex_width )  , (s16)contents_y, (s16)tex_width  , (s16)contents_dy , wdp[id].id_texture_ld, (u8)( wdp[id].texture_mirror >> 2 ), (u8)FALSE );
	drawWindowEdge( (s16)( contents_x + contents_dx ), (s16)contents_y, (s16)tex_width  , (s16)contents_dy , wdp[id].id_texture_rd, (u8)( wdp[id].texture_mirror      ), (u8)FALSE );
}

/*-----------------------------------------------------------------------------
  IDで指定したアトリビュートのウィンドウをoffset付で描画します。
  -----------------------------------------------------------------------------*/
void
glrDrawWindowOffsetID(
	u16 id ,
	gLayoutHeader* header,
	GXColor* rasc,
	int	offset_x,
	int offset_y,
	int contents_dx,
	int contents_dy )
{
	gLRWindowData* wdp = (gLRWindowData*)( (u32)header + (u32)header->window_offset );
	int contents_x  = wdp[id].x - contents_dx / 2 + offset_x;
	int contents_y  = wdp[id].y - contents_dy / 2 + offset_y;

	glrDrawWindowPosID( id, header, rasc, contents_x, contents_y, contents_dx, contents_dy );
}

/*-----------------------------------------------------------------------------
  IDで指定したアトリビュートのウィンドウを描画します。
  -----------------------------------------------------------------------------*/
void
glrDrawWindowID(
	u16 id ,
	gLayoutHeader* header,
	GXColor* rasc,
	int contents_dx,
	int contents_dy )
{
	gLRWindowData* wdp = (gLRWindowData*)( (u32)header + (u32)header->window_offset );
	int contents_x  = wdp[id].x - contents_dx / 2;//wdp[id].contents_dx * 8; // *16/2
	int contents_y  = wdp[id].y - contents_dy / 2;//wdp[id].contents_dy * 8; // *16/2

	glrDrawWindowPosID( id, header, rasc, contents_x, contents_y, contents_dx, contents_dy );
}

/*-----------------------------------------------------------------------------
  タグをwindow blockから検索して、指定されたタグのウィンドウを描画します。
  -----------------------------------------------------------------------------*/
void
glrDrawWindow( u32 tag, gLayoutHeader* l_hdr, GXColor* rasc )
{
	u16 i;
	gLRWindowData* wdp = (gLRWindowData*)( (u32)l_hdr + (u32)l_hdr->window_offset );

	for ( i = 0 ; i < l_hdr->window_num ; i++ ) {
		if ( wdp[i].tag == tag ) glrDrawWindowID( i, l_hdr, rasc, wdp[i].contents_dx, wdp[i].contents_dy );
	}
}

/*-----------------------------------------------------------------------------
  タグをwindow blockから検索して、指定された位置(左上)にウィンドウを描画します。
  -----------------------------------------------------------------------------*/
void
glrDrawWindowPos( u32 tag, gLayoutHeader* l_hdr, GXColor* rasc, int x, int y )
{
	u16 i;
	gLRWindowData* wdp = (gLRWindowData*)( (u32)l_hdr + (u32)l_hdr->window_offset );

	for ( i = 0 ; i < l_hdr->window_num ; i++ ) {
		if ( wdp[i].tag == tag ) glrDrawWindowPosID( i, l_hdr, rasc, x, y, wdp[i].contents_dx, wdp[i].contents_dy );
	}
}

/*-----------------------------------------------------------------------------
  規定の位置のオフセットを指定して、ウィンドウを描画する。
  offset_x/y は 既にs11.4の値
  -----------------------------------------------------------------------------*/
void
glrDrawWindowOffset( u32 tag, gLayoutHeader* l_hdr, GXColor* rasc, int offset_x, int offset_y )
{
	u16 i;
	gLRWindowData* wdp = (gLRWindowData*)( (u32)l_hdr + (u32)l_hdr->window_offset );

	for ( i = 0 ; i < l_hdr->window_num ; i++ ) {
		if ( wdp[i].tag == tag ) {
			int contents_x  = wdp[i].x - wdp[i].contents_dx / 2;//wdp[id].contents_dx * 8; // *16/2
			int contents_y  = wdp[i].y - wdp[i].contents_dy / 2;//wdp[id].contents_dy * 8; // *16/2
			glrDrawWindowPosID( i, l_hdr, rasc,
								contents_x + offset_x,
								contents_y + offset_y,
								wdp[i].contents_dx,
								wdp[i].contents_dy );
		}
	}
}

/*-----------------------------------------------------------------------------
  バインド情報を解決します。
  -----------------------------------------------------------------------------*/
static void
resolveBinding(
	int bind,
	int mirror,
	int paneTop,
	int paneCenter,
	int paneBottom,
	int	texHeight,
	int topFlag,
	int bottomFlag,
	int* posTop,
	int* posBottom,
	int* texTop,
	int* texBottom )
{
	
	if ( ( bind & topFlag ) &&
		 ( bind & bottomFlag ) ) {
		*posTop = paneTop;
		*texTop = 0;
		*posBottom = paneBottom;
		*texBottom = 16384;
	}
	else {
		// top_b / bottom_b / no_b
		if ( bind & topFlag ) {
			*posTop = paneTop;
			*texTop = 0;
			if ( ( mirror & cPicMirror_lap ) == 0 &&
				 paneBottom - paneTop > texHeight ) {
				*posBottom = texHeight;
				*texBottom = 16384;
			}
			else {
				*posBottom = paneBottom;
				*texBottom = ( paneBottom - paneTop ) * 16384 / texHeight;
			}
		}
		else if ( bind & bottomFlag ) {
			*posBottom = paneBottom;
			*texBottom = 16384;
			if ( ( mirror & cPicMirror_lap ) == 0 &&
				 paneBottom - paneTop > texHeight ) {
				*posTop = paneBottom - texHeight;
				*texTop = 0;
			}
			else {
				*posTop = paneTop;
				*texTop = ( texHeight - paneBottom + paneTop ) * 16384 / texHeight;
			}
		}
		else {
			if ( ( mirror & cPicMirror_lap ) == 0 &&
				 paneBottom - paneTop > texHeight ) {
				*posTop = paneCenter - texHeight / 2;
				*texTop = 0;
				*posBottom = *posTop + texHeight;
				*texBottom = 16384;
			}
			else {
				*posTop = paneTop;
				*texTop = 8192 -  ( paneBottom - paneTop) * 8192 / texHeight;
				*posBottom = paneBottom;
				*texBottom = 8192 + ( paneBottom - paneTop ) * 8192 / texHeight;
			}
		}
	}
}

/*-----------------------------------------------------------------------------
  指定されたidのテクスチャを描画します。
  -----------------------------------------------------------------------------*/
static void
glrDrawPictureID(  u16 i, gLayoutHeader* header, GXColor* rasc, ResTIMG* image_ptr , s16 offset_x, s16 offset_y )
{
	int		paneLeft, paneTop, paneRight, paneBottom;
	int		texLeft, texTop, texRight, texBottom;
	int		posLeft, posTop, posRight, posBottom;
	int 	texWidth, texHeight;
	gLRPictureData*	pdp = (gLRPictureData*)( (u32)header + (u32)header->picture_offset );

	GXSetChanMatColor( GX_COLOR0A0, *rasc );
	
	paneLeft = ( pdp[i].x + offset_x ) - pdp[i].dx / 2;
	paneTop  = ( pdp[i].y + offset_y ) - pdp[i].dy / 2;
	paneRight   = paneLeft + pdp[i].dx;
	paneBottom  = paneTop  + pdp[i].dy;

	if ( image_ptr != NULL ) {
		if ( pdp[i].mirror & cPicMirror_rot ) {
			texWidth  = image_ptr->height;
			texHeight = image_ptr->width;
		}
		else {
			texWidth  = image_ptr->width;
			texHeight = image_ptr->height;
		}
	} else {
		// NULLはバナー専用。
		texWidth = 96;
		texHeight = 32;
	}
	resolveBinding(	pdp[i].bind,//int bind,
					pdp[i].mirror,//int mirror,
					paneTop,
					( pdp[i].y + offset_y ),
					paneBottom,
					texHeight,
					cPicBind_u,//int topFlag,
					cPicBind_d,//int bottomFlag,
					&posTop,
					&posBottom,
					&texTop,
					&texBottom );

	resolveBinding(	pdp[i].bind,//int bind,
					pdp[i].mirror,//int mirror,
					paneLeft,
					( pdp[i].x + offset_x ),
					paneRight,
					texWidth,
					cPicBind_l,//int topFlag,
					cPicBind_r,//int bottomFlag,
					&posLeft,
					&posRight,
					&texLeft,
					&texRight );

	if ( pdp[i].mirror & cPicMirror_ud ) {
		int t;
		t = texTop;
		texTop = texBottom;
		texBottom = t;//texTop;
	}
	if ( pdp[i].mirror & cPicMirror_lr ) {
		int t;
		t = texLeft;
		texLeft = texRight;
		texRight = t;//texLeft;
	}

	if ( image_ptr != NULL ) {
		loadTexture( 0, image_ptr );
	}
	GXSetChanMatColor( GX_COLOR0A0, *rasc );
	
	if ( pdp[i].mirror & cPicMirror_rot ) {
		GXBegin(GX_QUADS, GX_VTXFMT1, 4);
		GXPosition3s16((s16)posLeft , (s16)posTop	, 0 );
		GXTexCoord2u16((u16)texLeft , (u16)texBottom	);

		GXPosition3s16((s16)posRight, (s16)posTop	, 0 );
		GXTexCoord2u16((u16)texLeft , (u16)texTop		);

		GXPosition3s16((s16)posRight, (s16)posBottom, 0 );
		GXTexCoord2u16((u16)texRight, (u16)texTop		);

		GXPosition3s16((s16)posLeft , (s16)posBottom, 0 );
		GXTexCoord2u16((u16)texRight, (u16)texBottom	);
		GXEnd();
	}
	else {
		GXBegin(GX_QUADS, GX_VTXFMT1, 4);
		GXPosition3s16((s16)posLeft , (s16)posTop	, 0 );
		GXTexCoord2u16((u16)texLeft , (u16)texTop		);

		GXPosition3s16((s16)posRight, (s16)posTop	, 0 );
		GXTexCoord2u16((u16)texRight, (u16)texTop		);

		GXPosition3s16((s16)posRight, (s16)posBottom, 0 );
		GXTexCoord2u16((u16)texRight, (u16)texBottom	);

		GXPosition3s16((s16)posLeft , (s16)posBottom, 0 );
		GXTexCoord2u16((u16)texLeft , (u16)texBottom	);
		GXEnd();
	}
}

/*-----------------------------------------------------------------------------
  テクスチャを全て描画します。
  -----------------------------------------------------------------------------*/
void
glrDrawPictureAll( gLayoutHeader* header, GXColor* rasc )
{
	u16		i;
	gLRPictureData*	pdp = (gLRPictureData*)( (u8*)header + header->picture_offset );

	for ( i = 0 ; i < header->picture_num ; i++ ) {
		glrDrawPictureID( i, header, rasc, &sTexImage[ pdp[i].textureID ] ,0 ,0 );
	}
}

/*-----------------------------------------------------------------------------
  タグを検索して、テクスチャを描画します。
  -----------------------------------------------------------------------------*/
void
glrDrawPicture( u32 tag, gLayoutHeader* header, GXColor* rasc )
{
	u16 i;
	gLRPictureData*	pdp = (gLRPictureData*)( (u8*)header + header->picture_offset );

	for ( i = 0 ; i < header->picture_num ; i++ ) {
		if ( pdp[i].tag == tag ) glrDrawPictureID( i, header, rasc, &sTexImage[ pdp[i].textureID ], 0, 0 );
	}
}

/*-----------------------------------------------------------------------------
  タグを検索して、テクスチャをオフセット付きで描画します。
  -----------------------------------------------------------------------------*/
void
glrDrawPictureOffset( u32 tag, gLayoutHeader* header, GXColor* rasc, s16 offset_x, s16 offset_y )
{
	u16 i;
	gLRPictureData*	pdp = (gLRPictureData*)( (u8*)header + header->picture_offset );

	for ( i = 0 ; i < header->picture_num ; i++ ) {
		if ( pdp[i].tag == tag ) glrDrawPictureID( i, header, rasc, &sTexImage[ pdp[i].textureID ], offset_x, offset_y );
	}
}

/*-----------------------------------------------------------------------------
  タグを検索して、そのアトリビュートで指定テクスチャを描画します。
  -----------------------------------------------------------------------------*/
void
glrDrawPictureTex( u32 tag, gLayoutHeader* header, GXColor* rasc, ResTIMG* image_ptr )
{
	u16 i;
	gLRPictureData*	pdp = (gLRPictureData*)( (u8*)header + header->picture_offset );

	for ( i = 0 ; i < header->picture_num ; i++ ) {
		if ( pdp[i].tag == tag ) glrDrawPictureID( i, header, rasc, image_ptr, 0, 0 );
	}
}


static void
glrDrawStringOffsetInside(
	u16 id,
	gLRStringHeader* s_hdr,
	gLayoutHeader* l_hdr,
	GXColor* rasc,
	GXColor* back,
	int offset_x,
	int offset_y )
{
	u16		width, height;
	gLRTextBoxData*	tbp = (gLRTextBoxData*)( (u8*)l_hdr + l_hdr->textbox_offset );
	gLRStringData* sdp = (gLRStringData*)( (u8*)s_hdr + sizeof(gLRStringHeader) );
	MainState*	msp = getMainState();
	
	if ( s_hdr->type != 'STH0' ) {
		DIReport("gLayoutRender : not string\n");
		return;
	}
	
	gfGetWidthHeight( &tbp[ sdp[id].textbox_id ],
					  (char*)( (u8*)&sdp[id] + sdp[id].string_offset ),
					  &width,
					  &height );
	
	if ( tbp[ sdp[id].textbox_id ].windowID != 0xffff ) {
		glrDrawWindowOffsetID( tbp[ sdp[id].textbox_id ].windowID, l_hdr, rasc,
							   offset_x, offset_y,
							   width  + msp->window_mergin * 32,
							   height + msp->window_mergin * 32
							   );

	}
	
	tbp[ sdp[id].textbox_id ].x += offset_x;
	tbp[ sdp[id].textbox_id ].y += offset_y;

	if ( ( tbp[ sdp[id].textbox_id ].h_bind & 0x7f ) == cBind_center ) {
		// 
		tbp[ sdp[id].textbox_id ].dx = (u16)( width  + 256 );
		tbp[ sdp[id].textbox_id ].dy = (u16)( height + 256 );
	}

	setGXforglrText();
	gfSetFontGXColor( rasc, rasc );
	if ( back != NULL ) {
		gfSetBackFontGXColor( back, back );
		gfBritePrint( &tbp[ sdp[id].textbox_id ], (char*)( (u8*)&sdp[id] + sdp[id].string_offset ) );
	} else {
		gfPrint( &tbp[ sdp[id].textbox_id ], (char*)( (u8*)&sdp[id] + sdp[id].string_offset ) );
	}
	
	tbp[ sdp[id].textbox_id ].x -= offset_x;
	tbp[ sdp[id].textbox_id ].y -= offset_y;
}

/*-----------------------------------------------------------------------------
  stringからid指定で文字列を描画します。
  offset_x/y は 既にs11.4の値
  -----------------------------------------------------------------------------*/
void
glrDrawStringOffset( u16 id, gLRStringHeader* s_hdr, gLayoutHeader* l_hdr, GXColor* rasc, int offset_x, int offset_y )
{
	glrDrawStringOffsetInside( id, s_hdr, l_hdr, rasc, NULL, offset_x, offset_y );
}

/*-----------------------------------------------------------------------------
  バックに色付のものを書く。
  -----------------------------------------------------------------------------*/
void
glrDrawStringBright( u16 id, gLRStringHeader* s_hdr, gLayoutHeader* l_hdr, GXColor* rasc ,GXColor* back )
{
	glrDrawStringOffsetInside( id, s_hdr, l_hdr, rasc, back, 0, 0 );
}

/*-----------------------------------------------------------------------------
  
  -----------------------------------------------------------------------------*/
void
glrDrawString( u16 id, gLRStringHeader* s_hdr, gLayoutHeader* l_hdr, GXColor* rasc )
{
	glrDrawStringOffset( id, s_hdr, l_hdr, rasc, 0, 0 );
}

/*-----------------------------------------------------------------------------
  テキストボックスをid指定で描画します。
  -----------------------------------------------------------------------------*/
void
glrDrawTextBoxID(
	u16 id,
	gLayoutHeader* l_hdr,
	GXColor* rasc,
	char* string,
	int string_num_max,
	int num_line )
{
	u16		width, height;
	u32	color = (u32)( (rasc->r<<24)|(rasc->g<<16)|(rasc->b<<8)|rasc->a );
	MainState*	msp = getMainState();
	
	gLRTextBoxData*	tbp = (gLRTextBoxData*)( (u8*)l_hdr + l_hdr->textbox_offset );

	if ( tbp[id].windowID != 0xffff ) {
		gfGetWidthHeight( &tbp[id], NULL, &width, &height );
		glrDrawWindowID( tbp[id].windowID, l_hdr, rasc,
						 width  + msp->window_mergin * 32,
						 height + msp->window_mergin * 32
						 );
	}

	setGXforglrText();
	gfSetFontColor( color, color );

	if ( num_line == 0 ) {
		gfPrintN( &tbp[id], string , string_num_max );
	} else if ( num_line == 1 ) {
		gfPrintNOneLine( &tbp[id], string , string_num_max );
	} else if ( num_line == 2 ) {
		gfPrintNTwoLine( &tbp[id], string , string_num_max );
	}
}


/*-----------------------------------------------------------------------------
  タグをtextbox blockから検索して、指定されたタグのtextboxを描画します。
  -----------------------------------------------------------------------------*/
void
glrDrawTextBox( u32 tag, gLayoutHeader* l_hdr, GXColor* rasc )
{
	u16 i;
	gLRTextBoxData*	tbp = (gLRTextBoxData*)( (u8*)l_hdr + l_hdr->textbox_offset );

	for ( i = 0 ; i < l_hdr->textbox_num ; i++ ) {
		if ( tbp[i].tag == tag ) glrDrawTextBoxID( i, l_hdr, rasc, NULL, 0, 0 );
	}
}


/*-----------------------------------------------------------------------------
  タグをtextbox blockから検索して、指定されたタグのtextboxを描画します。
  -----------------------------------------------------------------------------*/
void
glrDrawTextBoxString( u32 tag, gLayoutHeader* l_hdr, GXColor* rasc, char* string )
{
	u16 i;
	gLRTextBoxData*	tbp = (gLRTextBoxData*)( (u8*)l_hdr + l_hdr->textbox_offset );

	for ( i = 0 ; i < l_hdr->textbox_num ; i++ ) {
		if ( tbp[i].tag == tag ) glrDrawTextBoxID( i, l_hdr, rasc, string, 0, 0 );
	}
}

/*-----------------------------------------------------------------------------
  タグをtextbox blockから検索して、指定されたタグのtextboxを指定された文字列
　で最大文字数を指定して描画します。
  -----------------------------------------------------------------------------*/
void
glrDrawTextBoxNString( u32 tag, gLayoutHeader* l_hdr, GXColor* rasc, char* string, int string_num_max )
{
	u16 i;
	gLRTextBoxData*	tbp = (gLRTextBoxData*)( (u8*)l_hdr + l_hdr->textbox_offset );

	for ( i = 0 ; i < l_hdr->textbox_num ; i++ ) {
		if ( tbp[i].tag == tag ) glrDrawTextBoxID( i, l_hdr, rasc, string, string_num_max, 0 );
	}
}

/*-----------------------------------------------------------------------------
  タグをtextbox blockから検索して、指定されたタグのtextboxを指定された文字列
　で最大文字数を指定して描画します。
  -----------------------------------------------------------------------------*/
void
glrDrawTextBoxNStringOneLine( u32 tag, gLayoutHeader* l_hdr, GXColor* rasc, char* string, int string_num_max )
{
	u16 i;
	gLRTextBoxData*	tbp = (gLRTextBoxData*)( (u8*)l_hdr + l_hdr->textbox_offset );

	for ( i = 0 ; i < l_hdr->textbox_num ; i++ ) {
		if ( tbp[i].tag == tag ) glrDrawTextBoxID( i, l_hdr, rasc, string, string_num_max, 1 );
	}
}

/*-----------------------------------------------------------------------------
  タグをtextbox blockから検索して、指定されたタグのtextboxを指定された文字列
　で最大文字数を指定して描画します。
  -----------------------------------------------------------------------------*/
void
glrDrawTextBoxNStringTwoLine( u32 tag, gLayoutHeader* l_hdr, GXColor* rasc, char* string, int string_num_max )
{
	u16 i;
	gLRTextBoxData*	tbp = (gLRTextBoxData*)( (u8*)l_hdr + l_hdr->textbox_offset );

	for ( i = 0 ; i < l_hdr->textbox_num ; i++ ) {
		if ( tbp[i].tag == tag ) glrDrawTextBoxID( i, l_hdr, rasc, string, string_num_max, 2 );
	}
}






























/*-----------------------------------------------------------------------------
  高機能なFadein Windowの描画
  -----------------------------------------------------------------------------*/
void
glrResetFade( gLRFade* wp )
{
	wp->now_frame = 0;
	wp->anime_frame = 0;
}

void
glrUpdateFade( gLRFade* wp, u8 flag )
{
	switch ( flag )
	{
	case IPL_WINDOW_FADEIN:
		wp->now_frame++;
		if ( wp->now_frame > ( wp->fade_frame + wp->wait_frame ) ) wp->now_frame = (s16)( wp->fade_frame + wp->wait_frame );
		break;
	case IPL_WINDOW_FADEOUT:
		wp->now_frame--;
		if ( wp->now_frame < wp->wait_frame ) wp->now_frame = 0;
		break;
	case IPL_WINDOW_FADEHALF:
		if ( wp->now_frame >= wp->wait_frame ) {
			if      ( ( wp->now_frame - wp->wait_frame ) > wp->fade_frame / 2 ) wp->now_frame--;
			else if ( ( wp->now_frame - wp->wait_frame ) < wp->fade_frame / 2 ) wp->now_frame++;
			else wp->now_frame = (s16)( wp->fade_frame / 2 + wp->wait_frame );
		} else {
			wp->now_frame++;
		}
		break;
	}
	wp->anime_frame++;

}

void
glrGetOffsetAlpha( gLRFade* wp, s16* alpha, int* offset_y)
{
	if ( wp->now_frame >= wp->wait_frame ) {
		*alpha = (s16)( wp->max_alpha * ( wp->now_frame - wp->wait_frame ) / wp->fade_frame );
	} else {
		*alpha = 0;
	}
	if ( offset_y != NULL ) {
		if ( wp->now_frame >= wp->wait_frame ) {
			*offset_y = wp->slide_y * 16 * ( wp->fade_frame - wp->now_frame + wp->wait_frame ) / wp->fade_frame;
		} else {
			*offset_y = wp->slide_y * 16;
		}
	}
}

u8
glrDrawFadeWindow( gLRFade* wp, gLayoutHeader* header, GXColor* rasc )
{
	s16	alpha;
	u8	backup_alpha;
	int	offset_y;
	gLRWindowData* wdp = (gLRWindowData*)( (u32)header + (u32)header->window_offset );

	glrGetOffsetAlpha( wp, &alpha, &offset_y );

	backup_alpha = rasc->a;
	rasc->a = (u8)( rasc->a * alpha / wp->max_alpha );
	if ( rasc->a != 0 )	glrDrawWindowOffset( wp->tag, header, rasc, 0, offset_y );
	rasc->a = backup_alpha;

	return (u8)alpha;
}

u8
glrDrawFadeString( gLRFade* wp, gLRStringHeader* s_hdr, gLayoutHeader* header, GXColor* rasc )
{
	s16	alpha;
	int	offset_y;
	u8	backup_alpha;
	gLRWindowData* wdp = (gLRWindowData*)( (u32)header + (u32)header->window_offset );

	glrGetOffsetAlpha( wp, &alpha, &offset_y );

	backup_alpha = rasc->a;
	rasc->a = (u8)( rasc->a * alpha / 255 );
	if ( rasc->a != 0 ) glrDrawStringOffset( (u16)wp->tag, s_hdr, header, rasc, 0, offset_y );
	rasc->a = backup_alpha;

	return (u8)alpha;
}

void
glrInitFadeState( gLRFade* wp )
{
	wp->wait_frame = 0;
	wp->now_frame = 0;
	wp->fade_frame = 20;
	wp->max_alpha = 255;

	wp->slide_y = 0;	// 10pixels
	wp->tag = 0;
}















/*-----------------------------------------------------------------------------
  テスト用
  -----------------------------------------------------------------------------*/


void
setGXforLine( void )
{
	GXClearVtxDesc();
	GXSetVtxDesc( GX_VA_POS		, GX_DIRECT );
	GXSetVtxDesc( GX_VA_CLR0	, GX_DIRECT );

	GXSetVtxAttrFmt(GX_VTXFMT1, GX_VA_POS , GX_POS_XYZ , GX_S16 , 4);
	GXSetVtxAttrFmt(GX_VTXFMT1, GX_VA_CLR0, GX_CLR_RGBA, GX_RGBA8,0);
	
	GXSetNumChans(1); // default, color = vertex color
	GXSetChanCtrl( GX_COLOR0A0,
				   GX_DISABLE,
				   GX_SRC_VTX,
				   GX_SRC_VTX,
				   GX_NONE,
				   GX_DF_CLAMP,
				   GX_AF_NONE);
	GXSetNumTexGens(1);
	GXSetTexCoordGen( GX_TEXCOORD0, GX_TG_MTX2x4, GX_TG_TEX0, GX_IDENTITY );
	GXSetNumTevStages(1);
	GXSetTevOrder(GX_TEVSTAGE0, GX_TEXCOORD0, GX_TEXMAP0, GX_COLOR0A0);
//    GXSetTevOrder(GX_TEVSTAGE0, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
    GXSetTevOrder(GX_TEVSTAGE1, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
    GXSetTevOrder(GX_TEVSTAGE2, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
    GXSetTevOrder(GX_TEVSTAGE3, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
    GXSetTevOrder(GX_TEVSTAGE4, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
    GXSetTevOrder(GX_TEVSTAGE5, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
    GXSetTevOrder(GX_TEVSTAGE6, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
    GXSetTevOrder(GX_TEVSTAGE7, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);

	GXSetTevColorIn( GX_TEVSTAGE0, GX_CC_ZERO, GX_CC_ONE, GX_CC_RASC, GX_CC_ZERO );
	GXSetTevColorOp( GX_TEVSTAGE0, GX_TEV_ADD, GX_TB_ZERO, GX_CS_SCALE_1, GX_TRUE, GX_TEVPREV);
	GXSetTevAlphaIn( GX_TEVSTAGE0, GX_CA_ZERO, GX_CA_ONE, GX_CA_RASA, GX_CA_ZERO );
	GXSetTevAlphaOp( GX_TEVSTAGE0, GX_TEV_ADD, GX_TB_ZERO, GX_CS_SCALE_1, GX_TRUE, GX_TEVPREV);

	GXSetColorUpdate(GX_TRUE);
	GXSetAlphaUpdate(GX_TRUE);
	GXSetBlendMode( GX_BM_BLEND , GX_BL_SRCALPHA, GX_BL_INVSRCALPHA, GX_LO_CLEAR );
	GXSetZMode( GX_FALSE, GX_LEQUAL ,GX_FALSE);
}
/*-----------------------------------------------------------------------------
  テスト用のグリッドを描画する
  -----------------------------------------------------------------------------*/
static void
setGXforLightGrid( GXLightID light_mask )
{
	GXClearVtxDesc();
	GXSetVtxDesc( GX_VA_POS		, GX_DIRECT );
	GXSetVtxDesc( GX_VA_NRM		, GX_DIRECT );
	GXSetVtxDesc( GX_VA_TEX0	, GX_DIRECT );

	GXSetVtxAttrFmt(GX_VTXFMT1, GX_VA_POS , GX_POS_XYZ , GX_S16 , 4);
	GXSetVtxAttrFmt(GX_VTXFMT1, GX_VA_NRM , GX_NRM_XYZ , GX_S16 , 4);
	GXSetVtxAttrFmt(GX_VTXFMT1, GX_VA_TEX0, GX_TEX_ST  , GX_S16 , 4);
	
	GXSetNumChans(1); // default, color = vertex color
	GXSetChanCtrl( GX_COLOR0,
				   GX_DISABLE,
				   GX_SRC_REG,
				   GX_SRC_REG,
				   GX_LIGHT_NULL ,//| GX_LIGHT1,
				   GX_DF_CLAMP,
				   GX_AF_NONE);
	GXSetChanCtrl( GX_ALPHA0,
				   GX_ENABLE,
				   GX_SRC_REG,
				   GX_SRC_REG,
				   //GX_LIGHT3 ,//| GX_LIGHT1,
				   light_mask,
				   GX_DF_CLAMP,
				   GX_AF_SPOT );
	GXSetNumTexGens(1);
	GXSetTexCoordGen( GX_TEXCOORD0, GX_TG_MTX2x4, GX_TG_TEX0, GX_IDENTITY );
	GXSetNumTevStages(1);
	GXSetTevOrder(GX_TEVSTAGE0, GX_TEXCOORD0, GX_TEXMAP0, GX_COLOR0A0);
    GXSetTevOrder(GX_TEVSTAGE1, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
    GXSetTevOrder(GX_TEVSTAGE2, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
    GXSetTevOrder(GX_TEVSTAGE3, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
    GXSetTevOrder(GX_TEVSTAGE4, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
    GXSetTevOrder(GX_TEVSTAGE5, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
    GXSetTevOrder(GX_TEVSTAGE6, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
    GXSetTevOrder(GX_TEVSTAGE7, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);

	GXSetTevColorIn( GX_TEVSTAGE0, GX_CC_ZERO, GX_CC_ONE , GX_CC_RASC, GX_CC_ZERO );
	GXSetTevColorOp( GX_TEVSTAGE0, GX_TEV_ADD, GX_TB_ZERO, GX_CS_SCALE_1, GX_TRUE, GX_TEVPREV);
	GXSetTevAlphaIn( GX_TEVSTAGE0, GX_CA_ZERO, GX_CA_TEXA, GX_CA_RASA, GX_CA_ZERO );
	GXSetTevAlphaOp( GX_TEVSTAGE0, GX_TEV_ADD, GX_TB_ZERO, GX_CS_SCALE_1, GX_TRUE, GX_TEVPREV);

	GXSetColorUpdate(GX_TRUE);
	GXSetAlphaUpdate(GX_TRUE);
	GXSetBlendMode( GX_BM_BLEND , GX_BL_SRCALPHA, GX_BL_INVSRCALPHA, GX_LO_CLEAR );
	GXSetZMode( GX_FALSE, GX_LEQUAL ,GX_FALSE);
	
}

static void
loadGridLight(
	Mtx m,
	VecPtr pos,
	GXLightID light_id,
	u8 alpha,
	f32 bright,
	f32 spot )
{
	MenuState*	mesp = getMenuState();
	/*
	Vec		pos = (Vec){
		mesp->grid_light_pos_x,
		mesp->grid_light_pos_y,
		mesp->grid_light_pos_z };
	  */
	Vec 	dir = { 0, 0.f , -1.f };
	GXLightObj	tObj;
	GXColor	white = { 255,255,255,255 };
	Mtx inv;

	white.a = alpha;
	
	MTXMultVec( m, pos, pos );
	gGetInverseTranspose( m, inv );
	MTXMultVec( inv , &dir, &dir );

	GXInitLightPosv( &tObj, pos );
	GXInitLightDirv( &tObj, &dir );
	GXInitLightSpot( &tObj, spot, GX_SP_COS2 );
	GXInitLightDistAttn( &tObj,
						 mesp->grid_light_da_dist,
						 bright,
						 GX_DA_GENTLE );

	GXInitLightColor( &tObj, white );
	GXLoadLightObjImm( &tObj, light_id );
}

static void
glrDraw2dGridSub(
	Mtx		m,
	u8		alpha,
	GXLightID	mask,
	u8		light_alpha
	)
{
	int i , j;
	MenuState*	mesp = getMenuState();
	Vec		pos = (Vec){
		mesp->grid_light_pos_x,
		mesp->grid_light_pos_y,
		mesp->grid_light_pos_z };

	GXColor	white = { 255,255,255,255 };
	GXColor	gray = {64, 64,64 , 64 };
	GXColor	black = { 0, 0, 0, 0 };
	ResTIMG*	img = gGetBufferPtr( GC_ipl_2d_frame_bti );

	loadGridLight( m, &pos, GX_LIGHT3, light_alpha,mesp->grid_light_da_bright, mesp->grid_light_spot );
	loadTexture( GX_TEXMAP0, img );
	setGXforLightGrid( (GXLightID)( GX_LIGHT3 | mask ) );
	white.a = alpha;
	gray.a  = alpha;
	GXSetChanMatColor( GX_COLOR0A0, white );
	GXSetChanAmbColor( GX_COLOR0A0, black );
	
	for ( i = 0 ; i < 584 ; i += 32 ) {
		for ( j = 0 ; j < 448 ; j += 32 ) {
			GXBegin( GX_QUADS, GX_VTXFMT1, 4);
			GXPosition3s16( (s16)(i*16)      ,(s16)(j*16)      , 0 );	GXNormal3s16  ( 0,0, (s16)( 1 * 16 ) );
			GXTexCoord2s16( (s16)(  0  )     ,(s16)( 0)         );
			GXPosition3s16( (s16)(i*16+32*16),(s16)(j*16)      , 0 );	GXNormal3s16  ( 0,0, (s16)( 1 * 16 ) );
			GXTexCoord2s16( (s16)(  32 )     ,(s16)( 0)         );
			GXPosition3s16( (s16)(i*16+32*16),(s16)(j*16+32*16), 0 );	GXNormal3s16  ( 0,0, (s16)( 1 * 16 ) );
			GXTexCoord2s16( (s16)(  32 )     ,(s16)( 32)      );
			GXPosition3s16( (s16)(i*16)      ,(s16)(j*16+32*16), 0 );	GXNormal3s16  ( 0,0, (s16)( 1 * 16 ) );
			GXTexCoord2s16( (s16)(  0  )     ,(s16)( 32	)      );
			GXEnd();
		}
	}
	GXSetChanAmbColor( GX_COLOR0A0, white );
	
	GXSetChanCtrl( GX_COLOR0A0,
				   GX_ENABLE,
				   GX_SRC_REG,
				   GX_SRC_REG,
				   GX_LIGHT_NULL ,//| GX_LIGHT1,
				   GX_DF_CLAMP,
				   GX_AF_SPOT );
}


void
glrDraw2dGrid( Mtx m, u8 alpha )
{
	glrDraw2dGridSub( m, alpha, GX_LIGHT3, 255 );
}


void
glrDraw2dGridCard(
	Mtx m,
	u8 alpha,
	VecPtr	slot_a_pos,
	VecPtr	slot_b_pos,
	u8 slot_a_alpha,
	u8 slot_b_alpha,
	u8 main_light_alpha )
{
	u8	alpha_;
	u32	mask = GX_LIGHT_NULL;
	if ( slot_a_alpha > 0 ) {
		loadGridLight( m, slot_a_pos, GX_LIGHT4, slot_a_alpha, .525f, 28.f );
		mask |= GX_LIGHT4;
	}
	if ( slot_b_alpha > 0 ) {
		loadGridLight( m, slot_b_pos, GX_LIGHT5, slot_b_alpha, .525f, 28.f );
		mask |= GX_LIGHT5;
	}

	alpha_ = (u8)( (255 + main_light_alpha) / 2 );
	glrDraw2dGridSub( m, alpha, (GXLightID)mask, main_light_alpha );
}
