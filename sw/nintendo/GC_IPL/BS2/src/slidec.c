#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<sys/time.h>
void decode(unsigned char *,unsigned char *);

void main(int argc,char *argv[])
{
	int j,osize;
struct timeval tv;
double x,y;
	char fn[64],fc[64];
	unsigned char *bz,*dt;
	FILE *fp;
	if(argc<2){
	    printf("slidec <FILE>\n");exit(1);
	}
	strcpy(fn,argv[1]);
	for(j=0;fn[j];j++)if(fn[j]=='.')break;
	if((fp=fopen(fn,"rb"))==0){
	    printf("FILE READ ERROR! [%s]\n",fn);exit(1);
	}
	fn[j]=0;
	fseek(fp,0L,2);j=(int)ftell(fp);fseek(fp,0L,0);
	dt=(unsigned char *)malloc(j);
	fread(dt,1,j,fp);fclose(fp);
printf("%X %X %X\n",dt[0],dt[1],dt[2]);
	if(dt[0]=='Y' && dt[1]=='a' && dt[2]=='y'){
		osize=(dt[4]<<24)|(dt[5]<<16)|(dt[6]<<8)|dt[7];
		bz=(unsigned char *)malloc(osize);
printf("START!");fflush(stdout);
(void)gettimeofday(&tv,(struct timezone *)NULL);
x=tv.tv_sec+tv.tv_usec/1000000.0;
		decode(dt,bz);
(void)gettimeofday(&tv,(struct timezone *)NULL);
y=tv.tv_sec+tv.tv_usec/1000000.0;
printf("END!");
printf("decode time=[%f]\n",y-x);fflush(stdout);
		strcpy(fc,fn);strcat(fc,".out");
		if((fp=fopen(fc,"wb"))==0){
		    printf("FILE READ ERROR! [%s]\n",fc);exit(1);
		}
		fwrite(bz,1,osize,fp);
		fclose(fp);
		free(bz);
	}
	free(dt);
}
void decode(unsigned char *s,unsigned char *d)
{
	int i,j,k,p,q,r7,r25,cnt,os;
	unsigned flag,code;
	os=(s[4]<<24)|(s[5]<<16)|(s[6]<<8)|s[7];
	r7=(s[8]<<24)|(s[9]<<16)|(s[10]<<8)|s[11];
	r25=(s[12]<<24)|(s[13]<<16)|(s[14]<<8)|s[15];
	q=0;flag=0;p=16;
	do{
		if(flag==0){
			code=(s[p]<<24)|(s[p+1]<<16)|(s[p+2]<<8)|s[p+3];p+=4;
			flag=32;
		}
		if(code&0x80000000)d[q++]=s[r25++];
		else{
			j=(s[r7]<<8)|s[r7+1];r7+=2;
			k=q-(j&0xFFF);cnt=j>>12;
			if(cnt==0)cnt=s[r25++]+18;
			else cnt+=2;
			for(i=0;i<cnt;i++,q++,k++)d[q]=d[k-1];
		}
		code<<=1;flag--;
	}while(q<os);
}
