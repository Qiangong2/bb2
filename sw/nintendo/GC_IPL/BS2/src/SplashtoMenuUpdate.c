/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: Splashデモ Menu関連ルーチン
  -----------------------------------------------------------------------------*/
#include <dolphin.h>
#include <math.h>

#include "gBase.h"
#include "gCont.h"
#include "gCamera.h"
#include "gDynamic.h"

//#include "gMRStruct.h"
#include "gModelRender.h"
#include "gMath.h"
#include "gTrans.h"

#include "SplashMain.h"
#include "SplashDraw.h"
#include "SplashUpdate.h"

#include "SplashtoMenuUpdate.h"

#include "MenuUpdate.h"
#include "LogoUpdate.h"

#include "gMainState.h"

#include "jaudio.h"

/*-----------------------------------------------------------------------------
  statics
  -----------------------------------------------------------------------------*/
static Mtx sSplashToMenuMtx;

/*-----------------------------------------------------------------------------
  メニューに入るときのアニメーション処理
  -----------------------------------------------------------------------------*/
static void
updateRotY( void )
{
	static s16 prev_y;
	SplashState* ssp = inSplashGetState();
	u8		playmode;
	f32		speed;

	prev_y = ssp->rot_y;

	if ( ( ssp->key_now_frame >= 0 ) &&
		 ( ssp->key_now_frame < ssp->key_start_amp_frame ) ) {
		ssp->rot_y += ssp->rot_v;
		ssp->key_rot_start = ssp->rot_y;
	}
	else if ( ( ssp->key_now_frame >= ssp->key_start_amp_frame ) &&
			  ( ssp->key_now_frame < ssp->key_amp_frame + ssp->key_start_amp_frame ) ) {
		// 止まれの指令が出てから、実際に止まるまでの間。
		ssp->rot_y = (s16) ( (s32)( gHermiteInterpolation( ssp->key_now_frame,
														   ssp->key_start_amp_frame, // 最初のフレーム
														   -1 * ( 0x10000 * ssp->key_rot_num + ssp->key_rot_start ),
														   ssp->key_rot_start_v,
														   ssp->key_amp_frame + ssp->key_start_amp_frame,
														   ssp->key_rot_stop,
														   0 ) ) );
		if ( ssp->key_now_frame > ssp->key_start_amp_frame ) {
			ssp->rot_v =  (s16) ( (u16)ssp->rot_y - (u16)prev_y) ;
		} else {
		}
	}
	else if ( ( ssp->key_now_frame >= ssp->key_amp_frame + ssp->key_start_amp_frame ) &&
			  ( ssp->key_now_frame < ssp->key_amp_frame + ssp->key_stop_frame + ssp->key_start_amp_frame )  ) {
		// 一旦止まってから、止まるまで。
		ssp->rot_y = (s16) ( (s32)gHermiteInterpolation( ssp->key_now_frame,
														 ssp->key_amp_frame + ssp->key_start_amp_frame, // 最初のフレーム
														 ssp->key_rot_stop,
														 0,
														 ssp->key_amp_frame + ssp->key_stop_frame + ssp->key_start_amp_frame,
														 0,
														 0 ) );
		ssp->rot_v = (s16) ( (u16)ssp->rot_y - (u16)prev_y) ;
	}
	else if ( ssp->key_now_frame == ssp->key_amp_frame + ssp->key_stop_frame + ssp->key_start_amp_frame ) {
		// とまり。
		ssp->rot_y = 0;
		ssp->rot_v = 0.f;
	}

//	DIReport("rot_v:%f\n", ssp->rot_v );

	// 回転音を鳴らす。
	if ( ssp->rot_v > 5000.f ) playmode = 0;
	else playmode = 1;

	speed = ssp->rot_v / 5000.f;
	if ( speed > 1.0f ) speed = 1.0f;
	if ( speed < 0.01f ) speed = 0.0f;

	if ( prev_y < 0 && ssp->rot_y >= 0 && speed > ssp->morphcube_sound_speed_end ) {
		Jac_PlayCubeMorphing( playmode, speed );
			DIReport2("%d,%f\n",playmode, speed );
	}/*
	else if ( prev_y < 8192 && ssp->rot_y >= 8192 && speed > ssp->morphcube_sound_speed ) {
		Jac_PlayCubeMorphing( playmode, speed );
	}*/
	else if ( prev_y > 0 && ssp->rot_y < 0 && speed > ssp->morphcube_sound_speed_end ) {
		Jac_PlayCubeMorphing( playmode, speed );
			DIReport2("%d,%f\n",playmode, speed );
	}/*
	else if ( prev_y < -8192 && ssp->rot_y >= -8192 && speed > ssp->morphcube_sound_speed ) {
		Jac_PlayCubeMorphing( playmode, speed );
	}*/
}

static u8
updateFadein( void )
{
	SplashState* ssp = inSplashGetState();

	if ( ssp->key_first ) {
		ssp->key_rot_start = ssp->rot_y;
		ssp->key_first = FALSE;
	}

	// アップデート処理
	updateRotY();

	ssp->key_now_frame ++;

	if ( ( ssp->rot_v <= 0.f ) && ( ssp->rot_y == (s16)0 ))	{
		// 原点で止まったら終了。
		ssp->key_first = TRUE;
	}
	return cMFS_in_dec;

}

static void
getFadeinMtx( Mtx v )
{
	SplashState* ssp = inSplashGetState();

	gGetTranslateRotateMtx( 0, ssp->rot_y ,0, 0,0,0, v);
}


static void
getChangingMenuCubeMtx ( Mtx v , u8 t )
{
	SplashState* ssp = inSplashGetState();
	MenuState* mesp = getMenuState();
	Mtx tr;
	s16 frame = mesp->bound_frame;
	f32 amp = ssp->logo_amp;
	s16 rot = ssp->logo_rot;
	f32 spd = ssp->rot_v/ssp->max_v;
	f32 rad_rot = 1.0f - ssp->logo_rad_rot;

	if ( ssp->key_now_frame == 1 ) frame = 0 ;

	MTXIdentity( tr );

	gGetTranslateRotateMtx(
		(s16)(
			(8191 + amp * gSSin( (s16)(90.f * rot )) ) * ( rad_rot * spd ) +
			( mesp->menu_swing_amp_x * gSCos( (s16)(mesp->menu_swing_speed_x * frame) ))
			* ( 1.0f - rad_rot * spd )
			),
		(s16)(
			(-6371+ amp * gSSin( (s16)(60.f * rot )) ) * ( rad_rot * spd ) +
			( mesp->menu_swing_amp_y * gSCos( (s16)(mesp->menu_swing_speed_y * frame + mesp->menu_swing_offset_y ) ))
			* ( 1.0f - rad_rot * spd )
			),
		(s16)(
			(-5461+ amp * gSSin( (s16)(70.f * rot )) ) * ( rad_rot * spd ) +
			( mesp->menu_swing_amp_z * gSCos( (s16)(mesp->menu_swing_speed_z * frame) ))
			* ( 1.0f - rad_rot * spd )
			),
		mesp->menu_slide_amp_x * gSSin( (s16)(mesp->menu_slide_speed_x * frame + mesp->menu_slide_offset_x ) ),
		mesp->menu_slide_amp_y * gSSin( (s16)(mesp->menu_slide_speed_y * frame) ),
		0,
		v );

	MTXConcat ( v, tr , v );
	if ( t ) frame += 7;

	mesp->bound_frame = frame;
}

/*-----------------------------------------------------------------------------
  スプラッシュデモからメニューへ
  -----------------------------------------------------------------------------*/
u32
inSplashToMenuUpdate( void )
{
	Mtx v , rotate;//, currentMtx;
	f32 cs , scale_xz;
	SplashState* ssp = inSplashGetState();
	MainState* msp = getMainState();
	u8	stat;

	stat = updateFadein();

	msp->ortho = 0;

	getFadeinMtx( v );
	getChangingMenuCubeMtx( rotate , TRUE );
	
	MTXConcat( v, rotate ,  sSplashToMenuMtx );

	cs = ssp->rot_v/ssp->max_v;
	scale_xz = 1.0f - ssp->anim_min_scale;

	setBaseMtx( inSplashGetMenuCube() ,
				sSplashToMenuMtx ,
				(Vec){
					ssp->scale * (( 1.0f - scale_xz ) * cs + 1.1f * ( 1.0f - cs )),
					ssp->scale * (( 1.f+ ssp->anim_max_scale_y ) * cs + 1.1f * ( 1.0f - cs )),
					ssp->scale * (( 1.0f - scale_xz ) * cs + 1.1f * ( 1.0f - cs ))
						} );
	setViewMtx( inSplashGetMenuCube() , getCurrentCamera() );
	updateModel( inSplashGetMenuCube() );
	if ( ssp->key_now_frame <= ssp->logo_change_menu_cube_frame ) {
		// largeCube用のアップデート処理
		inSplashUpdateMtx();
	}

	if ( ( ssp->rot_v <= 0.f ) && ( ssp->rot_y == (s16)0 ))	{
		return cMenu_Select;
	}
	return cMenu_Init;
}

MtxPtr
getSplashToMenuMtx( void )
{
	return sSplashToMenuMtx;
}
