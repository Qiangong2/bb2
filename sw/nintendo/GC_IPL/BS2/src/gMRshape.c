/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: J3DBinary 1.5 Renderer
  
  Shape����
  -----------------------------------------------------------------------------*/
#include <dolphin.h>

#include "gBase.h"

#include "gMRshape.h"

void
buildShape(
	J3DShapeBlock* sbPtr ,
	gmrShape* sArray )
{
	int i , j;

	for ( i=0 ; i < sbPtr->shapeNum ; i++ ) {
		sArray[i].mtxType     = sbPtr->shapeInit[ sbPtr->shapeID[ i ] ].mMtxType;
		sArray[i].mtxGroupNum = sbPtr->shapeInit[ sbPtr->shapeID[ i ] ].mMtxGroupNum;

		if ( sArray[ i ].mtxGroupNum > 2 ) {
			OSHalt("ERROR: This renderer doesn't support Enveloped Model.\n");
		}
		
		sArray[i].vtxDescList = (GXVtxDescList*)((s8*)sbPtr->vtxDescList + sbPtr->shapeInit[ sbPtr->shapeID[ i ] ].mVtxDescOffset);

		sArray[i].shapeMtx  = (gmrShapeMtx*)gAlloc( sArray[i].mtxGroupNum * sizeof(gmrShapeMtx)  );
		sArray[i].shapeDraw = (gmrShapeDraw*)gAlloc( sArray[i].mtxGroupNum * sizeof(gmrShapeDraw) );

		for ( j=0 ; j < sArray[i].mtxGroupNum ; j++ )
		{
			sArray[i].shapeMtx[j].baseId    = sbPtr->mtxInit[ sbPtr->shapeInit[ sbPtr->shapeID[ i ] ].mMtxID + j ].mBase;
			sArray[i].shapeMtx[j].useMtxNum = sbPtr->mtxInit[ sbPtr->shapeInit[ sbPtr->shapeID[ i ] ].mMtxID + j ].mNum;
			sArray[i].shapeMtx[j].useMtxIdP = sbPtr->mtxIndexList + sbPtr->mtxInit[ sbPtr->shapeInit[ sbPtr->shapeID[ i ] ].mMtxID + j ].mOffset;

			sArray[i].shapeDraw[j].size	       = sbPtr->drawInit[ sbPtr->shapeInit[ sbPtr->shapeID[ i ] ].mDrawID + j ].mSize;
			sArray[i].shapeDraw[j].displayList = sbPtr->drawDisplayList + sbPtr->drawInit[ sbPtr->shapeInit[ sbPtr->shapeID[ i ] ].mDrawID + j ].mOffset;
		}
	}
}

