/*---------------------------------------------------------------------------*
  File:    smallCubeFaderData.c
  Copyright 2000 Nintendo.  All rights reserved.

  電光掲示板表示のデータを扱う。各国語対応をここで全部
  吸収する。

 *---------------------------------------------------------------------------*/

#include <dolphin.h>
#include "smallCubeFaderData.h"
#include "smallCubeFaderDataDefine.h"
#include "gLoader.h"


// PAL とそれ以外の違いは、ここで吸収。

#ifdef PAL // for PAL

static smallCubeFaderIndex* filedata[COUNTRY_PAL_MAX_LANGUAGE];

static u16* getDataPtr( int index );
static u16* getDataPtr_pal( int index, int country );
static u16 getDataNum_pal( int index, int country );

void initSmallCubeFaderData( void* data )
{
#pragma unused(data) // ignore arg. get data directly.
	int i;
	for ( i = 0 ; i < COUNTRY_PAL_MAX_LANGUAGE ; i++ ) {
		filedata[i] =  gGetBufferPtr(getID_smallcubefaderdata_pal(i));
	}
}
static u16* getDataPtr( int index )
{
	return (u16*)((u32)&filedata[gGetPalCountry()][index] + filedata[gGetPalCountry()][index].offset);
}
static u16 getDataNum( int index )
{
	return filedata[gGetPalCountry()][index].cubenum;
}
static u16* getDataPtr_pal( int index, int country )
{
	return (u16*)((u32)&filedata[country][index] + filedata[country][index].offset);
}
static u16 getDataNum_pal( int index, int country )
{
	return filedata[country][index].cubenum;
}


u16* scfGetMONOIndex_pal( s32 lang )
{
	return getDataPtr_pal( SCFDATADEF_MONO, lang );
}

u16  scfGetMONONum_pal( s32 lang )
{
	return getDataNum_pal( SCFDATADEF_MONO, lang );
}

u16* scfGetSTEREOIndex_pal( s32 lang )
{
	return getDataPtr_pal( SCFDATADEF_STEREO, lang );
	
}

u16  scfGetSTEREONum_pal( s32 lang )
{
	return getDataNum_pal( SCFDATADEF_STEREO, lang );
}


#else // for NTSC, MPAL

static smallCubeFaderIndex* filedata;
static u16* getDataPtr( int index );
void initSmallCubeFaderData( void* data ){ filedata = data;}
static u16* getDataPtr( int index ){ return (u16*)((u32)&filedata[index] + filedata[index].offset);}
static u16 getDataNum( int index ){    return filedata[index].cubenum;}

#endif





#ifdef PAL
u16* scfGetLANGUAGEIndex(void){ return getDataPtr( SCFDATADEF_LANGUAGE );}
u16  scfGetLANGUAGENum(void){   return getDataNum( SCFDATADEF_LANGUAGE );}
u16* scfGetSMALL_ARROW05Index(void){ return getDataPtr( SCFDATADEF_SMALL_ARROW05 );}
u16  scfGetSMALL_ARROW05Num(void){   return getDataNum( SCFDATADEF_SMALL_ARROW05 );}
u16* scfGetSMALL_ARROW06Index(void){ return getDataPtr( SCFDATADEF_SMALL_ARROW06 );}
u16  scfGetSMALL_ARROW06Num(void){   return getDataNum( SCFDATADEF_SMALL_ARROW06 );}
#else
u16* scfGetLANGUAGEIndex(void){ return getDataPtr( 0 );}
u16  scfGetLANGUAGENum(void){   return 0; }
u16* scfGetSMALL_ARROW05Index(void){ return getDataPtr( 0 );}
u16  scfGetSMALL_ARROW05Num(void){   return 0; }
u16* scfGetSMALL_ARROW06Index(void){ return getDataPtr( 0 );}
u16  scfGetSMALL_ARROW06Num(void){   return 0; }
#endif



// end of PAL とそれ以外の違いは、ここで吸収。



/************************************************
 * insert here
 ************************************************/
u16* scfGetCLOCKIndex(void){ return getDataPtr( SCFDATADEF_CLOCK );}
u16  scfGetCLOCKNum(void){   return getDataNum( SCFDATADEF_CLOCK );}
u16* scfGetCOLONIndex(void){ return getDataPtr( SCFDATADEF_COLON );}
u16  scfGetCOLONNum(void){   return getDataNum( SCFDATADEF_COLON );}
u16* scfGetCOLON1Index(void){ return getDataPtr( SCFDATADEF_COLON1 );}
u16  scfGetCOLON1Num(void){   return getDataNum( SCFDATADEF_COLON1 );}
u16* scfGetCOLON2Index(void){ return getDataPtr( SCFDATADEF_COLON2 );}
u16  scfGetCOLON2Num(void){   return getDataNum( SCFDATADEF_COLON2 );}
u16* scfGetCOLON3Index(void){ return getDataPtr( SCFDATADEF_COLON3 );}
u16  scfGetCOLON3Num(void){   return getDataNum( SCFDATADEF_COLON3 );}
u16* scfGetCOLON4Index(void){ return getDataPtr( SCFDATADEF_COLON4 );}
u16  scfGetCOLON4Num(void){   return getDataNum( SCFDATADEF_COLON4 );}
u16* scfGetDATEIndex(void){ return getDataPtr( SCFDATADEF_DATE );}
u16  scfGetDATENum(void){   return getDataNum( SCFDATADEF_DATE );}
u16* scfGetDAYIndex(void){ return getDataPtr( SCFDATADEF_DAY );}
u16  scfGetDAYNum(void){   return getDataNum( SCFDATADEF_DAY );}
u16* scfGetDAY1STIndex(void){ return getDataPtr( SCFDATADEF_DAY1ST );}
u16  scfGetDAY1STNum(void){   return getDataNum( SCFDATADEF_DAY1ST );}
u16* scfGetDAY2NDIndex(void){ return getDataPtr( SCFDATADEF_DAY2ND );}
u16  scfGetDAY2NDNum(void){   return getDataNum( SCFDATADEF_DAY2ND );}
u16* scfGetHOURIndex(void){ return getDataPtr( SCFDATADEF_HOUR );}
u16  scfGetHOURNum(void){   return getDataNum( SCFDATADEF_HOUR );}
u16* scfGetHOUR1STIndex(void){ return getDataPtr( SCFDATADEF_HOUR1ST );}
u16  scfGetHOUR1STNum(void){   return getDataNum( SCFDATADEF_HOUR1ST );}
u16* scfGetHOUR2NDIndex(void){ return getDataPtr( SCFDATADEF_HOUR2ND );}
u16  scfGetHOUR2NDNum(void){   return getDataNum( SCFDATADEF_HOUR2ND );}
u16* scfGetMINUTEIndex(void){ return getDataPtr( SCFDATADEF_MINUTE );}
u16  scfGetMINUTENum(void){   return getDataNum( SCFDATADEF_MINUTE );}
u16* scfGetMINUTE1STIndex(void){ return getDataPtr( SCFDATADEF_MINUTE1ST );}
u16  scfGetMINUTE1STNum(void){   return getDataNum( SCFDATADEF_MINUTE1ST );}
u16* scfGetMINUTE2NDIndex(void){ return getDataPtr( SCFDATADEF_MINUTE2ND );}
u16  scfGetMINUTE2NDNum(void){   return getDataNum( SCFDATADEF_MINUTE2ND );}
u16* scfGetMONTHIndex(void){ return getDataPtr( SCFDATADEF_MONTH );}
u16  scfGetMONTHNum(void){   return getDataNum( SCFDATADEF_MONTH );}
u16* scfGetMONTH1STIndex(void){ return getDataPtr( SCFDATADEF_MONTH1ST );}
u16  scfGetMONTH1STNum(void){   return getDataNum( SCFDATADEF_MONTH1ST );}
u16* scfGetMONTH2NDIndex(void){ return getDataPtr( SCFDATADEF_MONTH2ND );}
u16  scfGetMONTH2NDNum(void){   return getDataNum( SCFDATADEF_MONTH2ND );}
u16* scfGetSECONDIndex(void){ return getDataPtr( SCFDATADEF_SECOND );}
u16  scfGetSECONDNum(void){   return getDataNum( SCFDATADEF_SECOND );}
u16* scfGetSECOND1STIndex(void){ return getDataPtr( SCFDATADEF_SECOND1ST );}
u16  scfGetSECOND1STNum(void){   return getDataNum( SCFDATADEF_SECOND1ST );}
u16* scfGetSECOND2NDIndex(void){ return getDataPtr( SCFDATADEF_SECOND2ND );}
u16  scfGetSECOND2NDNum(void){   return getDataNum( SCFDATADEF_SECOND2ND );}
u16* scfGetYEARIndex(void){ return getDataPtr( SCFDATADEF_YEAR );}
u16  scfGetYEARNum(void){   return getDataNum( SCFDATADEF_YEAR );}
u16* scfGetYEAR1STIndex(void){ return getDataPtr( SCFDATADEF_YEAR1ST );}
u16  scfGetYEAR1STNum(void){   return getDataNum( SCFDATADEF_YEAR1ST );}
u16* scfGetYEAR2NDIndex(void){ return getDataPtr( SCFDATADEF_YEAR2ND );}
u16  scfGetYEAR2NDNum(void){   return getDataNum( SCFDATADEF_YEAR2ND );}
u16* scfGetYEAR3RDIndex(void){ return getDataPtr( SCFDATADEF_YEAR3RD );}
u16  scfGetYEAR3RDNum(void){   return getDataNum( SCFDATADEF_YEAR3RD );}
u16* scfGetYEAR4THIndex(void){ return getDataPtr( SCFDATADEF_YEAR4TH );}
u16  scfGetYEAR4THNum(void){   return getDataNum( SCFDATADEF_YEAR4TH );}
u16* scfGetNODISCIndex(void){ return getDataPtr( SCFDATADEF_NODISC );}
u16  scfGetNODISCNum(void){   return getDataNum( SCFDATADEF_NODISC );}
u16* scfGetPRESSSTARTIndex(void){ return getDataPtr( SCFDATADEF_PRESSSTART );}
u16  scfGetPRESSSTARTNum(void){   return getDataNum( SCFDATADEF_PRESSSTART );}
u16* scfGetQUESIndex(void){ return getDataPtr( SCFDATADEF_QUES );}
u16  scfGetQUESNum(void){   return getDataNum( SCFDATADEF_QUES );}
u16* scfGetNUMBER0Index(void){ return getDataPtr( SCFDATADEF_NUMBER0 );}
u16  scfGetNUMBER0Num(void){   return getDataNum( SCFDATADEF_NUMBER0 );}
u16* scfGetNUMBER1Index(void){ return getDataPtr( SCFDATADEF_NUMBER1 );}
u16  scfGetNUMBER1Num(void){   return getDataNum( SCFDATADEF_NUMBER1 );}
u16* scfGetNUMBER2Index(void){ return getDataPtr( SCFDATADEF_NUMBER2 );}
u16  scfGetNUMBER2Num(void){   return getDataNum( SCFDATADEF_NUMBER2 );}
u16* scfGetNUMBER3Index(void){ return getDataPtr( SCFDATADEF_NUMBER3 );}
u16  scfGetNUMBER3Num(void){   return getDataNum( SCFDATADEF_NUMBER3 );}
u16* scfGetNUMBER4Index(void){ return getDataPtr( SCFDATADEF_NUMBER4 );}
u16  scfGetNUMBER4Num(void){   return getDataNum( SCFDATADEF_NUMBER4 );}
u16* scfGetNUMBER5Index(void){ return getDataPtr( SCFDATADEF_NUMBER5 );}
u16  scfGetNUMBER5Num(void){   return getDataNum( SCFDATADEF_NUMBER5 );}
u16* scfGetNUMBER6Index(void){ return getDataPtr( SCFDATADEF_NUMBER6 );}
u16  scfGetNUMBER6Num(void){   return getDataNum( SCFDATADEF_NUMBER6 );}
u16* scfGetNUMBER7Index(void){ return getDataPtr( SCFDATADEF_NUMBER7 );}
u16  scfGetNUMBER7Num(void){   return getDataNum( SCFDATADEF_NUMBER7 );}
u16* scfGetNUMBER8Index(void){ return getDataPtr( SCFDATADEF_NUMBER8 );}
u16  scfGetNUMBER8Num(void){   return getDataNum( SCFDATADEF_NUMBER8 );}
u16* scfGetNUMBER9Index(void){ return getDataPtr( SCFDATADEF_NUMBER9 );}
u16  scfGetNUMBER9Num(void){   return getDataNum( SCFDATADEF_NUMBER9 );}
u16* scfGetARROWIndex(void){ return getDataPtr( SCFDATADEF_ARROW );}
u16  scfGetARROWNum(void){   return getDataNum( SCFDATADEF_ARROW );}
u16* scfGetARROWLEFTIndex(void){ return getDataPtr( SCFDATADEF_ARROWLEFT );}
u16  scfGetARROWLEFTNum(void){   return getDataNum( SCFDATADEF_ARROWLEFT );}
u16* scfGetARROWRIGHTIndex(void){ return getDataPtr( SCFDATADEF_ARROWRIGHT );}
u16  scfGetARROWRIGHTNum(void){   return getDataNum( SCFDATADEF_ARROWRIGHT );}
u16* scfGetMONOIndex(void){ return getDataPtr( SCFDATADEF_MONO );}
u16  scfGetMONONum(void){   return getDataNum( SCFDATADEF_MONO );}
u16* scfGetOFFSETIndex(void){ return getDataPtr( SCFDATADEF_OFFSET );}
u16  scfGetOFFSETNum(void){   return getDataNum( SCFDATADEF_OFFSET );}
u16* scfGetOFFSET1STIndex(void){ return getDataPtr( SCFDATADEF_OFFSET1ST );}
u16  scfGetOFFSET1STNum(void){   return getDataNum( SCFDATADEF_OFFSET1ST );}
u16* scfGetOFFSET2NDIndex(void){ return getDataPtr( SCFDATADEF_OFFSET2ND );}
u16  scfGetOFFSET2NDNum(void){   return getDataNum( SCFDATADEF_OFFSET2ND );}
u16* scfGetSMALL_ARROW01Index(void){ return getDataPtr( SCFDATADEF_SMALL_ARROW01 );}
u16  scfGetSMALL_ARROW01Num(void){   return getDataNum( SCFDATADEF_SMALL_ARROW01 );}
u16* scfGetSMALL_ARROW02Index(void){ return getDataPtr( SCFDATADEF_SMALL_ARROW02 );}
u16  scfGetSMALL_ARROW02Num(void){   return getDataNum( SCFDATADEF_SMALL_ARROW02 );}
u16* scfGetSMALL_ARROW03Index(void){ return getDataPtr( SCFDATADEF_SMALL_ARROW03 );}
u16  scfGetSMALL_ARROW03Num(void){   return getDataNum( SCFDATADEF_SMALL_ARROW03 );}
u16* scfGetSMALL_ARROW04Index(void){ return getDataPtr( SCFDATADEF_SMALL_ARROW04 );}
u16  scfGetSMALL_ARROW04Num(void){   return getDataNum( SCFDATADEF_SMALL_ARROW04 );}

u16* scfGetSOUNDIndex(void){ return getDataPtr( SCFDATADEF_SOUND );}
u16  scfGetSOUNDNum(void){   return getDataNum( SCFDATADEF_SOUND );}
u16* scfGetSTEREOIndex(void){ return getDataPtr( SCFDATADEF_STEREO );}
u16  scfGetSTEREONum(void){   return getDataNum( SCFDATADEF_STEREO );}
/************************************************
 * insert here end
 ************************************************/





// 数字
u16* scfGetDIGITIndex(s16 num)
{
    switch( num ){
    case 0: return scfGetNUMBER0Index();
    case 1: return scfGetNUMBER1Index();
    case 2: return scfGetNUMBER2Index();
    case 3: return scfGetNUMBER3Index();
    case 4: return scfGetNUMBER4Index();
    case 5: return scfGetNUMBER5Index();
    case 6: return scfGetNUMBER6Index();
    case 7: return scfGetNUMBER7Index();
    case 8: return scfGetNUMBER8Index();
    case 9: return scfGetNUMBER9Index();
    default: ASSERTMSG( 0, "invalid digit value" ); return NULL;
    }
}

