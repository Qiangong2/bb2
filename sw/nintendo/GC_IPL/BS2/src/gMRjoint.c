/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: J3DBinary 1.5 Renderer
  
  JOINT処理
  -----------------------------------------------------------------------------*/
#include <dolphin.h>

#include "gModelRender.h"
#include "gMRjoint.h"

/*-----------------------------------------------------------------------------
  JOINT構造体にセットする。
  -----------------------------------------------------------------------------*/
void buildJoint(
	J3DJointBlock* jbPtr ,
	gmrJoint* jArray )
{
	int i;

	for ( i=0 ; i < jbPtr->jointNum ; i++ ) {
		jArray[i].kind = jbPtr->jointInit[ jbPtr->jointID[ i ] ].mKind;
		jArray[i].transformInfo = jbPtr->jointInit[ jbPtr->jointID[ i ] ].mTransformInfo;
	}
}

