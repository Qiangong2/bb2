/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: Splashデモ メインルーチン

  シーケンスを含めたSplashデモのupdateルーチン
  -----------------------------------------------------------------------------*/
#include <dolphin.h>
#include <private/OSRtc.h>

#include "gCont.h"
#include "gCamera.h"
#include "gDynamic.h"
#include "gLoader.h"

//#include "gMRStruct.h"
#include "gModelRender.h"
#include "gMath.h"
#include "gTrans.h"
#include "gInit.h"

#include "SplashMain.h"
#include "SplashUpdate.h"
#include "SplashDraw.h"

#include "CardSequence.h"

#include "LogoUpdate.h"

#include "jaudio.h"

/*-----------------------------------------------------------------------------
  コントローラの入力を外部参照する。
  -----------------------------------------------------------------------------*/
extern DIPadStatus	DIPad;

/*-----------------------------------------------------------------------------
  static モノ
  -----------------------------------------------------------------------------*/
static Mtx currentMtx;	// 現在の描画用マトリックスを入れる場所


static BOOL	checkCardFreesize( void );
static BOOL	checkClockAdjust( void );


/*-----------------------------------------------------------------------------
  コントローラの入力をキューブに伝える
  -----------------------------------------------------------------------------*/
static void
updateControl( BOOL turn )
{
	SplashState* ssp = inSplashGetState();
	s16	prev_y = ssp->logo_interact_prev_y;
	s16	start_y = ssp->logo_interact_start_y;
	f32	start_slope = ssp->logo_interact_start_slope;
	u8		playmode;
	f32		speed;

	prev_y = ssp->rot_y;
	
	if ( turn ) {
		ssp->rot_a += ssp->rot_aa;
		ssp->rot_v = ssp->rot_v + ssp->rot_a;
		ssp->rot_y += ssp->rot_v;
	}
	else if ( ( DIPad.pst.button & PAD_BUTTON_A ) &&
			  gEnableInteractive() ) {
		ssp->rot_a += ssp->rot_aa;
		ssp->rot_v = ssp->rot_v + ssp->rot_a;
		ssp->rot_y += ssp->rot_v;
		start_y = ssp->rot_y;
		start_slope = ssp->rot_v;
	
		ssp->spring_frame = 0;
	}
	else {
#ifdef PAL
		ssp->rot_a *= 0.76f;
		ssp->rot_v *= 0.95f;
#else
		ssp->rot_a *= 0.8f;
		ssp->rot_v *= 0.96f;
#endif
		if ( ssp->rot_a < 0.1 ) ssp->rot_a = 0;
		if ( ssp->rot_v < 0.01 ) ssp->rot_v = 0;
#ifdef PAL
		ssp->rot_y = (s16)gHermiteInterpolation( ssp->spring_frame,
												0, start_y , start_slope,
												50, 0, 0 );
		ssp->spring_frame++;
		if ( ssp->spring_frame > 50 ) ssp->spring_frame = 50;
#else
		ssp->rot_y = (s16)gHermiteInterpolation( ssp->spring_frame,
												0, start_y , start_slope,
												60, 0, 0 );
		ssp->spring_frame++;
		if ( ssp->spring_frame > 60 ) ssp->spring_frame = 60;
#endif
	}

	playmode = 0;
	speed = ssp->rot_v / 5000.f;
	if ( speed > 1.0f ) speed = 1.0f;
	if ( speed < 0.01f ) speed = 0.0f;

	if ( gEnableInteractive() || turn ) {
		if ( prev_y < 0 && ssp->rot_y >= 0 && speed > ssp->morphcube_sound_speed ) {
			Jac_PlayCubeMorphing( playmode, speed );
		}
		else if ( prev_y > 0 && ssp->rot_y < 0 && speed > ssp->morphcube_sound_speed ) {
			Jac_PlayCubeMorphing( playmode, speed );
		}
	}

	ssp->logo_interact_prev_y = prev_y;
	ssp->logo_interact_start_y = start_y;
	ssp->logo_interact_start_slope = start_slope;
}


/*-----------------------------------------------------------------------------
  適当に回転（自転）させる
  -----------------------------------------------------------------------------*/
static void
updateBoundParam( void )
{
	SplashState* ssp = inSplashGetState();
	f32	amp = ssp->bound_z_amp;
	f32	x_amp = ssp->bound_x_amp;
	s16	rot = ssp->bound_rot;

	amp   *= ssp->bound_z_amp_atten;
	x_amp *= ssp->bound_x_amp_atten;
	
	if ( amp < 10.f ) amp = 0.f;
	if ( x_amp < 80.f ) x_amp = 0.f;
	
	if ( amp > 0.f ||
		 x_amp > 0.f ) rot += ssp->bound_amp_rot_delta;

	ssp->bound_z_amp = (s16)amp;
	ssp->bound_x_amp = (s16)x_amp;
	ssp->bound_rot = rot;
}

static void
updateRotateParam( void )
{
	SplashState* ssp = inSplashGetState();
	f32 amp = ssp->logo_amp;
	s16 rot = ssp->logo_rot;
	f32 rad_rot = ssp->logo_rad_rot;
	f32 spd = rad_rot * ssp->rot_v/ssp->max_v;


#ifdef PAL
	if ( ssp->rot_a > 0.f ) amp = amp + 36.0f;
	else amp *= 0.945f;
#else
	if ( ssp->rot_a > 0.f ) amp = amp + 30.0f;
	else amp *= 0.96f;
#endif

	if ( amp < 1.f ) amp = 0.f;
	
	if ( amp > 0.f ) rot += 7;

	if ( amp > 5000.f ) amp = 5000.f;

	ssp->logo_amp = (s16)amp;
	ssp->logo_rot = rot;

	updateBoundParam();
}

static void
getBoundMtx( Mtx m )
{
	Mtx tr;
	SplashState* ssp = inSplashGetState();
	f32	amp = ssp->bound_z_amp;
	f32	x_amp = ssp->bound_x_amp;
	s16	rot = ssp->bound_rot;

	gGetTranslateRotateMtx( (s16)( -1 * x_amp * gSSin ( rot ) ),
							0,
							(s16)( amp * gSSin(  rot ) ) ,
							0,
							ssp->bound_y_ave_z * amp * gSSin( rot )  +
							ssp->bound_y_ave_x * x_amp * gSSin( rot ) ,
							0, tr );
	MTXConcat ( tr , m , m );
}

static void
getRotateMtx( Mtx m )
{
	Mtx tr;
	SplashState* ssp = inSplashGetState();
	f32 amp = ssp->logo_amp;
	s16 rot = ssp->logo_rot;
	f32 rad_rot = ssp->logo_rad_rot;
	f32 spd = rad_rot * ssp->rot_v/ssp->max_v;

	MTXIdentity( tr );
	tr[0][3] = 15.f;
	tr[1][3] = 15.f;
	tr[2][3] = 15.f;
	
	gGetTranslateRotateMtx( (s16)ssp->logo_angle_x,
							(s16)ssp->logo_angle_y,
							(s16)ssp->logo_angle_z,
							0,0,0, m );
	MTXConcat ( m , tr , tr );

	gGetTranslateRotateMtx( (s16)( ( amp * gSSin( (s16)(90 * rot )) )
								  * ( 1.0f - spd ) + 1000 * spd
								  ),
							(s16)( ( amp * gSSin( (s16)(60 * rot )) ) 
								  * ( 1.0f - spd ) + 1000 * spd
								  ),
							(s16)( ( amp * gSSin(  (s16)(70 * rot) ) )
								  * ( 1.0f - spd ) + 1000 * spd
								  ),
							0,
							0,//amp * gSSin( rot ) / 50.f + x_amp * gSSin( rot ) / 30.f ,
							0, m );
	MTXConcat ( m , tr , m );
	
	getBoundMtx( m );
	
	gGetTranslateRotateMtx( 0, ssp->rot_y ,0, 0,0,0, tr);
	MTXConcat ( tr , m , m );
}


/*-----------------------------------------------------------------------------
  Mtxを含む描画用アップデート処理
  -----------------------------------------------------------------------------*/
void
inSplashUpdateMtx( void )
{
	Mtx rotate , baseMtx;
	SplashState* ssp = inSplashGetState();
	f32 scale_xz;

	// LargeCubeを描画するための行列を生成します。
	// 最終的に、現在フレーム用の行列がcurrentMtxに入り、
	// 他の描画で参照できるようになります。
	getRotateMtx( rotate );

//	gGetTranslateRotateMtx( 0,0,0, ssp->trans,ssp->trans,ssp->trans, baseMtx);
	MTXIdentity( baseMtx );
	MTXConcat( rotate , baseMtx , currentMtx );

	scale_xz = 1.0f - ssp->anim_min_scale;
	
	gScaleNrmMtx( currentMtx ,
				  (&(Vec){
					  1.0f - scale_xz * ssp->rot_v/ssp->max_v ,
					  1.0f + ssp->anim_max_scale_y * ssp->rot_v/ssp->max_v,
					  1.0f - scale_xz * ssp->rot_v/ssp->max_v }) );

	// largeCube用のアップデート処理
	setBaseMtx( inSplashGetLargeCube() , currentMtx , (Vec){ssp->scale,ssp->scale,ssp->scale} );
	setViewMtx( inSplashGetLargeCube() , getCurrentCamera() );
	updateModel( inSplashGetLargeCube() );

	setBaseMtx( inSplashGetLogo() , currentMtx  , (Vec){1.0f, 1.0f, 1.0f});//ssp->scale,ssp->scale,ssp->scale} );
	setViewMtx( inSplashGetLogo() , getCurrentCamera() );
	updateModel( inSplashGetLogo() );

	setBaseMtx( inSplashGetCoverCube() , currentMtx  , (Vec){1.01f, 1.01f, 1.01f});//ssp->scale,ssp->scale,ssp->scale} );
	setViewMtx( inSplashGetCoverCube() , getCurrentCamera() );
	updateModel( inSplashGetCoverCube() );

	// 小さいキューブのマトリックスの更新
	lgUpdateCubeMtx();
}


/*-----------------------------------------------------------------------------
  ロゴデモパラメータのアップデート
  -----------------------------------------------------------------------------*/
u32
inSplashUpdate( void )
{
	SplashState* ssp = inSplashGetState();
	LogoState* lsp = lgGetCubeState();

	// コントローラからの入力のための更新。
	if ( !lsp->demoDone ) {
		updateControl( FALSE );
	}
	updateRotateParam();
	
	// 小さいキューブのアニメーションの更新。
	lgUpdateCubePos( TRUE );

	// メニューへの移行。
	if ( ssp->rot_v > ssp->max_v ) {
		ssp->rot_y = ssp->start_y;
		return cMenu_Init;
	}


	if ( ssp->force_fatalerror ) {
		ssp->boot_fatal_fade++;
		if ( ssp->boot_fatal_fade > ssp->boot_fatal_fade_max ) {
			ssp->boot_fatal_fade = ssp->boot_fatal_fade_max;
		}

		return cSpRun_Rotate;
	}

	if ( gGetBs2State() == BS2_WRONG_DISK ||
		 gGetBs2State() == BS2_RETRY ) {
		// error表示。
		ssp->boot_nodisk_fade++;
		if ( ssp->boot_nodisk_fade > ssp->boot_nodisk_fade_max ) {
			ssp->boot_nodisk_fade = ssp->boot_nodisk_fade_max;
		}
	} else {
		ssp->boot_nodisk_fade--;
		if ( ssp->boot_nodisk_fade < 0 ) {
			ssp->boot_nodisk_fade = 0;
		}
	}
	
	if ( ssp->force_cubemenu ) {
		if ( lsp->demoDone ) {
			gRunAppFadein();
			if ( gGetRunAppFadeAlpha() == 0 ) {
				updateControl( TRUE );
				updateRotateParam();
				if ( ssp->rot_v > ssp->max_v ) {
					ssp->rot_y = ssp->start_y;
					return cMenu_Init;
				}
			}
		}

		return cSpRun_Rotate;
	}

	/*
	if ( gGetBs2State() != BS2_RUN_APP ) {
		ssp->memorycard_check_state = IPL_SP_MEMCHECK_PRE;
		ssp->memorycard_check_frame--;
		if ( ssp->memorycard_check_frame < 0 ) {
			ssp->memorycard_check_frame = 0;
			ssp->memorycard_check_cursor = 1;
		}
	}
	  */
	
	switch ( gGetBs2State() )
	{
	case BS2_RUN_APP:
		// if ( !checkCardFreesize() ) {	// カードのフリーサイズチェック。
		// if ( !checkClockAdjust() ) {		// 時計合わせチェック。
		if ( lsp->demoDone ) {
#ifdef IPL
			gRunAppUpdate();
#endif
		}
		// }
		// }
		break;
	case BS2_WRONG_DISK:
	case BS2_RETRY:
		// ダイアログ表示。
		break;
	case BS2_FATAL_ERROR:
		ssp->force_fatalerror = TRUE;
		break;
	case BS2_NO_DISK:
	case BS2_COVER_OPEN:
		ssp->force_cubemenu = TRUE;
		break;
	}

	return cSpRun_Rotate;
}

/*-----------------------------------------------------------------------------
  [PAL] 言語選択メニュー
  -----------------------------------------------------------------------------*/

static void
updateSelectLanguage(  SplashState* ssp )
{
	if ( DIPad.dirsPressed & DI_STICK_UP ) { // 上
		ssp->pal_language_cursor--;
		if ( ssp->pal_language_cursor < 0 ) {
			ssp->pal_language_cursor = (s16)( COUNTRY_PAL_MAX_LANGUAGE - 1 );
		}
		Jac_PlaySe( JAC_SE_MC_SELECT );
	}
	if ( DIPad.dirsPressed & DI_STICK_DOWN ) {// 下
		ssp->pal_language_cursor++;
		if ( ssp->pal_language_cursor >= COUNTRY_PAL_MAX_LANGUAGE ){
			ssp->pal_language_cursor = 0;
		}
		Jac_PlaySe( JAC_SE_MC_SELECT );
	}
	
	if ( DIPad.buttonDown & PAD_BUTTON_A ) {
		Jac_PlaySe( JAC_SE_MC_DECIDE );
		gSetPalCountry( ssp->pal_language_cursor );
		ssp->pal_first_boot = FALSE;
	}
	if ( DIPad.buttonDown & PAD_BUTTON_B ) {
		Jac_PlaySe( JAC_SE_ERROR );
	}

}


/*-----------------------------------------------------------------------------
  SRAM設定後の一番最初の起動時には、コントローラからの入力は受け付けない。
  -----------------------------------------------------------------------------*/
u32
updateSplashFirstBoot( void )
{
	SplashState* ssp = inSplashGetState();
	LogoState* lsp = lgGetCubeState();
	// 小さいキューブのアニメーションの更新。
	if ( gGetBs2State() == BS2_FATAL_ERROR ) {
		ssp->force_fatalerror = TRUE;
	}

	if ( ssp->force_fatalerror ) {
		ssp->boot_fatal_fade++;
		if ( ssp->boot_fatal_fade > ssp->boot_fatal_fade_max ) {
			ssp->boot_fatal_fade = ssp->boot_fatal_fade_max;
		}
		return cSpRun_Init;
	}

#ifdef PAL
	if ( ssp->pal_first_boot ) {
		ssp->pal_first_boot_alpha += 10;
		if ( ssp->pal_first_boot_alpha > 255 ) {
			ssp->pal_first_boot_alpha = 255;
			updateSelectLanguage( ssp );
		}
		return cSpRun_Init;
	}

	if ( ssp->pal_first_boot_alpha > 0 ) {
		ssp->pal_first_boot_alpha-=10;
		if ( ssp->pal_first_boot_alpha <= 0 ) {
			ssp->pal_first_boot_alpha = 0;
		}
		return cSpRun_Init;
	}
#endif
	
	if ( !lsp->demoDone ) {
		updateRotateParam();
		lgUpdateCubePos( TRUE );
	}
	else {
		// メッセージfadein
		switch ( ssp->firstboot_state ) {
		case 0:
			ssp->firstboot_fade_ninlogo--;
			if ( ssp->firstboot_fade_ninlogo < 0 ) {
				ssp->firstboot_fade_ninlogo = 0;
				ssp->firstboot_state = 1;
			}
			break;
		case 1:
			// 「最初に設定を行ないましょう」メッセージ表示。
			ssp->firstboot_fade_a++;
			if ( ssp->firstboot_fade_a > ssp->firstboot_fade_max_a ) {
				ssp->firstboot_fade_a = ssp->firstboot_fade_max_a;
				ssp->firstboot_state = 3;
			}
			break;
		case 3:
			// キー入力待ち。
			if ( ( DIPad.buttonDown &
				   ( PAD_BUTTON_A | PAD_BUTTON_B | PAD_BUTTON_X | PAD_BUTTON_Y |
					 PAD_BUTTON_MENU | PAD_TRIGGER_Z | PAD_TRIGGER_L | PAD_TRIGGER_R ) )
				 != 0 ) {
				Jac_PlaySe( JAC_SE_MC_DECIDE );
				ssp->firstboot_state = 4;
			}
			break;
		case 4:
			// 「最初に設定を行ないましょう」メッセージ表示。
			ssp->firstboot_fade_a--;
			if ( ssp->firstboot_fade_a < 0 ) {
				ssp->firstboot_fade_a = 0;
				ssp->firstboot_state = 6;
			}
			break;
		case 6:
			// メニューキューブに移行。
			updateControl( TRUE );
			updateRotateParam();
			if ( ssp->rot_v > ssp->max_v ) {
				ssp->rot_y = ssp->start_y;
				return cMenu_Init;
			}
			break;
		}

	}

	return cSpRun_Init;
}

/*-----------------------------------------------------------------------------
  SRAMがつぶれいているときの起動は、Fadeinで行う。
  -----------------------------------------------------------------------------*/

static void
updateSramCrashRunApp( SplashState* ssp )
{
	if ( gGetBs2State() == BS2_COVER_OPEN ||
		 gGetBs2State() == BS2_NO_DISK ||
		 gGetBs2State() == BS2_RETRY ||
		 gGetBs2State() == BS2_WRONG_DISK 
		 ) {
		ssp->sramcrash_state = 3;
	} else {
		if ( gGetBs2State() == BS2_RUN_APP ) { 
			gRunAppUpdate();
		}
	}
}

static void
updateSramCrashCursor( SplashState* ssp )
{
	if ( DIPad.dirsPressed & DI_STICK_UP ) { // 上
		if ( ssp->sramcrash_cursor != 0 ) Jac_PlaySe( JAC_SE_MC_SELECT );
		ssp->sramcrash_cursor = 0;
	}
	if ( DIPad.dirsPressed & DI_STICK_DOWN ) {// 下
		if ( ssp->sramcrash_cursor != 1 ) Jac_PlaySe( JAC_SE_MC_SELECT );
		ssp->sramcrash_cursor = 1;
	}

	if ( DIPad.buttonDown & PAD_BUTTON_A ) {
		if ( ssp->sramcrash_cursor == 0 ) {
			Jac_PlaySe( JAC_SE_MC_DECIDE );
			ssp->sramcrash_state = 3; // メニューへ
		}
		else {
			Jac_PlaySe( JAC_SE_MC_CANCEL );
			ssp->sramcrash_state = 2; // ゲームスタート
		}
	}
	if ( DIPad.buttonDown & PAD_BUTTON_B ) {
		Jac_PlaySe( JAC_SE_MC_CANCEL );
		ssp->sramcrash_state = 2; // ゲームスタート
	}
}

u32
updateSplashFadeinLogo( void )
{
	LogoState* lsp = lgGetCubeState();
	SplashState* ssp = inSplashGetState();

	while ( !lsp->demoDone ) { // デモが終わってなかったら、終わるまで飛ばす。
		lgUpdateCubePos( FALSE );
		updateRotateParam();
#ifndef PAL
		gRunAppFadeinSetup();
#endif
	}

	if ( gGetBs2State() == BS2_FATAL_ERROR ) {
		ssp->force_fatalerror = TRUE;
	}

#ifdef PAL
	if ( ssp->pal_first_boot ) {
		ssp->pal_first_boot_alpha += 10;
		if ( ssp->pal_first_boot_alpha > 255 ) {
			ssp->pal_first_boot_alpha = 255;
			updateSelectLanguage( ssp );
		}
		return cSpRun_Logo;
	}

	if ( ssp->pal_first_boot_alpha > 0 ) {
		ssp->pal_first_boot_alpha-=10;
		if ( ssp->pal_first_boot_alpha <= 0 ) {
			ssp->pal_first_boot_alpha = 0;
			gRunAppFadeinSetup();
		}
		return cSpRun_Logo;
	}

#endif
	
	if ( gGetRunAppFadeAlpha() == 0 ) { // 画面が出てから。
		if ( ssp->force_fatalerror ) {
			ssp->sramcrash_window_fade--;
			if ( ssp->sramcrash_window_fade < 0 ) {
				ssp->sramcrash_window_fade = 0;
				ssp->boot_fatal_fade++;
				if ( ssp->boot_fatal_fade > ssp->boot_fatal_fade_max ) {
					ssp->boot_fatal_fade = ssp->boot_fatal_fade_max;
				}
			}
			return cSpRun_Logo;
		}
		 
		ssp->sramcrash_first_frame++;
		if ( ssp->sramcrash_first_frame > 20 ) {
			ssp->sramcrash_first_frame = 20;
			switch ( ssp->sramcrash_state )
			{
			case 0:// ロゴを暗くする。
				ssp->sramcrash_fade++;
				if ( ssp->sramcrash_fade > ssp->sramcrash_fade_max ) {
					ssp->sramcrash_fade = ssp->sramcrash_fade_max;
					ssp->sramcrash_state = 1;
				}
				break;
			case 1:	// 警告ウィンドウを表示する。
				ssp->sramcrash_window_fade++;
				if ( ssp->sramcrash_window_fade > ssp->sramcrash_window_fade_max ) {
					ssp->sramcrash_window_fade = ssp->sramcrash_window_fade_max;
				}
				updateSramCrashCursor( ssp );
				break;
			case 2: // ゲームスタート。
				ssp->sramcrash_window_fade--;
				if ( ssp->sramcrash_window_fade < 0 ) {
					ssp->sramcrash_window_fade = 0;
					ssp->sramcrash_fade--;
					if ( ssp->sramcrash_fade < 0 ) {
						ssp->sramcrash_fade = 0;
						ssp->sramcrash_state = 5;
					}
				}
				break;
			case 3: // メニューへ移行。
				// ウィンドウを消す。
				ssp->sramcrash_window_fade--;
				if ( ssp->sramcrash_window_fade < 0 ) {
					ssp->sramcrash_window_fade = 0;
					ssp->sramcrash_state = 4;
				}
				break;
			case 4: // ロゴを戻す。
				ssp->sramcrash_fade--;
				if ( ssp->sramcrash_fade < 0 ) {
					ssp->sramcrash_fade = 0;
					// メニューキューブに移行。
					updateControl( TRUE );
					updateRotateParam();
					if ( ssp->rot_v > ssp->max_v ) {
						ssp->rot_y = ssp->start_y;
						return cMenu_Init;
					}
				}
				break;
			case 5:
				updateSramCrashRunApp( ssp );
				break;
			}
		}
	}
	if ( ssp->sramcrash_state == 5 ) {
		updateSramCrashRunApp( ssp );
	}
	else {
		gRunAppFadein();	// 画面を出す。
	}

	return cSpRun_Logo;
}

/*-----------------------------------------------------------------------------
  CoverOpenの時のシーケンス。
  ロゴFadein -> メニューキューブへ。
  -----------------------------------------------------------------------------*/
u32
updateSplashLogoToMenu( void )
{
	LogoState* lsp = lgGetCubeState();
	SplashState* ssp = inSplashGetState();
	while ( !lsp->demoDone ) { // デモが終わってなかったら、終わるまで飛ばす。
		lgUpdateCubePos( FALSE );
		updateRotateParam();
		gRunAppFadeinSetup();
	}

	gRunAppFadein();

	ssp->cover_open_wait_frame--;
	
	if ( ssp->cover_open_wait_frame < 0 ) {
		ssp->cover_open_wait_frame = 0;
		updateControl( TRUE );
		updateRotateParam();
		if ( ssp->rot_v > ssp->max_v ) {
			ssp->rot_y = ssp->start_y;
			return cMenu_Init;
		}
	}

	return cSpRun_LogotoMenu;
}

/*-----------------------------------------------------------------------------
  現在の描画用行列を返す。
  -----------------------------------------------------------------------------*/
MtxPtr
inSplashGetMtxArray( void )
{
	return currentMtx;
}




















































/*-----------------------------------------------------------------------------
  メモリーカードの起動時のチェック。
  -----------------------------------------------------------------------------*/
static s16
updateMemDialogCursor( void )
{
	s16 ret = IPL_SP_MEMCHECK_NOW;
	SplashState* ssp = inSplashGetState();
	
	if ( DIPad.dirsPressed & DI_STICK_UP ) { // 上
		if ( ssp->memorycard_check_cursor != 0 ) Jac_PlaySe( JAC_SE_MC_SELECT );
		ssp->memorycard_check_cursor = 0;
	}
	if ( DIPad.dirsPressed & DI_STICK_DOWN ) {// 下
		if ( ssp->memorycard_check_cursor != 1 ) Jac_PlaySe( JAC_SE_MC_SELECT );
		ssp->memorycard_check_cursor = 1;
	}

	if ( DIPad.buttonDown & PAD_BUTTON_A ) {
		if ( ssp->memorycard_check_cursor == 0 ) {
			Jac_PlaySe( JAC_SE_MC_DECIDE );
			ret = IPL_SP_MEMCHECK_GOMENU; // メニューへ
		}
		else {
			Jac_PlaySe( JAC_SE_MC_CANCEL );
			ret = IPL_SP_MEMCHECK_GO; // ゲームスタート
		}
	}
	if ( DIPad.buttonDown & PAD_BUTTON_B ) {
		Jac_PlaySe( JAC_SE_MC_CANCEL );
		ret = IPL_SP_MEMCHECK_GO; // ゲームスタート
	}

	return ret;
}

static s16
checkCardFreesizeSub( void )
{
	SlotState*	tSlot = getCardSlotState();
	s16		chan, ret = IPL_SP_MEMCHECK_NOW;
	u32		support_sector, need_size;
	s16		need_blocks[2];
	s16		full_entry[2];
	SplashState* ssp = inSplashGetState();

	if ( gGetBannerExInfo( &support_sector, &need_size ) ) {
		for ( chan = 0 ; chan < 2 ; chan++ ) {
			full_entry[chan] = FALSE;
			if ( tSlot[chan].state == SLOT_READY &&
				 isStateNotChanged() &&
				 tSlot[chan].game_blocks >= 0 ) {
				if ( ( support_sector & tSlot[chan].sector_size ) != 0 ) {
					need_blocks[chan] = (s16)( need_size / tSlot[chan].sector_size );
					if ( ( need_size % tSlot[chan].sector_size ) != 0 ) {
						need_blocks[chan] += 1;
					}
					if ( need_blocks[chan] <= tSlot[chan].free_blocks + tSlot[chan].game_blocks ) {
						need_blocks[chan] = 0;
					}
					if ( need_blocks[chan] > tSlot[chan].full_blocks )  {
						need_blocks[chan] = -1;
					}
					if ( tSlot[chan].free_fileentry == 0 ) {
						full_entry[ chan ] = TRUE;
					}
				} else {
					need_blocks[chan] = -1;
				}
			} else {
				need_blocks[chan] = -1;
			}
		}

		if ( ( need_blocks[0] == 0 && !full_entry[0] ) ||
			 ( need_blocks[1] == 0 && !full_entry[1] ) ) {
			ret = IPL_SP_MEMCHECK_GO;
		} else if ( need_blocks[0] > 0 && need_blocks[1] > 0 ) {
			if ( need_blocks[0] < need_blocks [1] ) {
				ssp->memorycard_check_needblocks = need_blocks[0];
				ssp->memorycard_check_slot = 0;
			} else {
				ssp->memorycard_check_needblocks = need_blocks[1];
				ssp->memorycard_check_slot = 1;
			}
		} else if ( need_blocks[0] > 0 ) {
			ssp->memorycard_check_needblocks = need_blocks[0];
			ssp->memorycard_check_slot = 0;
		} else if ( need_blocks[1] > 0 ) {
			ssp->memorycard_check_needblocks = need_blocks[1];
			ssp->memorycard_check_slot = 1;
		}
		
	} else {
		ret = IPL_SP_MEMCHECK_GO;
	}

	if ( need_blocks[0] < 0 && need_blocks[1] < 0 ) {
		ret = IPL_SP_MEMCHECK_GO;
	}

	if ( ret != IPL_SP_MEMCHECK_GO ) {
		ssp->memorycard_check_frame++;
		if ( ssp->memorycard_check_frame > 30 ) ssp->memorycard_check_frame = 30;
		ret = updateMemDialogCursor();
	}

	return ret;
}

static BOOL
checkCardFreesize( void )
{
	SplashState* ssp = inSplashGetState();
	
	switch ( ssp->memorycard_check_state )
	{
	case IPL_SP_MEMCHECK_PRE:
		// カードスレッドにメモリチェックの指令を出す。
//		DIReport("pre\n");
		sendCardGetGameCodeBlocks();
		ssp->memorycard_check_state = IPL_SP_MEMCHECK_GETSTATUS;
	case IPL_SP_MEMCHECK_GETSTATUS:
//		DIReport("getstatus\n");
		if ( isDoneGameCodeBlocks() ) {
			ssp->memorycard_check_state = IPL_SP_MEMCHECK_NOW;
		}
		return TRUE;	// もう少し待つ。
		break;
	case IPL_SP_MEMCHECK_NOW:
//		DIReport("now\n");
		ssp->memorycard_check_state = checkCardFreesizeSub();
		return TRUE;
		break;
	case IPL_SP_MEMCHECK_GO:
//		DIReport("go\n");
		ssp->memorycard_check_frame--;
		if ( ssp->memorycard_check_frame < 0 ) {
			ssp->memorycard_check_frame = 0;
			ssp->memorycard_check_state = IPL_SP_MEMCHECK_DONE;
		}
		return TRUE;
		break;
	case IPL_SP_MEMCHECK_DONE:
//		DIReport("done\n");
		return FALSE;
		break;
	case IPL_SP_MEMCHECK_GOMENU:
//		DIReport("gomenu\n");
		ssp->memorycard_check_frame--;
		if ( ssp->memorycard_check_frame < 0 ) {
			ssp->memorycard_check_frame = 0;
			ssp->force_cubemenu = TRUE;
		}
		return TRUE;
		break;
	}
	return FALSE;
}

/*-----------------------------------------------------------------------------
  時計の起動チェック
  -----------------------------------------------------------------------------*/
static s16
checkClockAdjustSub( void )
{
	s16 ret = IPL_SP_CLOCK_NOW;
	SplashState* ssp = inSplashGetState();
	
	if ( DIPad.dirsPressed & DI_STICK_UP ) { // 上
		if ( ssp->clockadjust_check_cursor != 0 ) Jac_PlaySe( JAC_SE_MC_SELECT );
		ssp->clockadjust_check_cursor = 0;
	}
	if ( DIPad.dirsPressed & DI_STICK_DOWN ) {// 下
		if ( ssp->clockadjust_check_cursor != 1 ) Jac_PlaySe( JAC_SE_MC_SELECT );
		ssp->clockadjust_check_cursor = 1;
	}

	if ( DIPad.buttonDown & PAD_BUTTON_A ) {
		if ( ssp->clockadjust_check_cursor == 0 ) {
			Jac_PlaySe( JAC_SE_MC_DECIDE );
			ret = IPL_SP_CLOCK_GOMENU; // メニューへ
		}
		else {
			Jac_PlaySe( JAC_SE_MC_CANCEL );
			ret = IPL_SP_CLOCK_GO; // ゲームスタート
		}
	}
	if ( DIPad.buttonDown & PAD_BUTTON_B ) {
		Jac_PlaySe( JAC_SE_MC_CANCEL );
		ret = IPL_SP_CLOCK_GO; // ゲームスタート
	}

	if ( ret == IPL_SP_CLOCK_NOW ) {
		ssp->clockadjust_check_frame++;
		if ( ssp->clockadjust_check_frame > 30 ) {
			ssp->clockadjust_check_frame = 30;
		}
	}

	return ret;
}

static BOOL
checkClockAdjust( void )
{
	OSSram *sram;
	SplashState* ssp = inSplashGetState();

	switch ( ssp->clockadjust_check_state )
	{
	case IPL_SP_CLOCK_PRE:
//		DIReport("clock");
		sram = __OSLockSram();
		if ( gGetBannerExInfoClockAdjust() &&
			(sram->flags & OS_SRAM_CLOCKADJUST_FLAG) == 0 ) {
			ssp->clockadjust_check_state = IPL_SP_CLOCK_NOW;
		} else {
			ssp->clockadjust_check_state = IPL_SP_CLOCK_GO;
		}
		__OSUnlockSram(FALSE);
		return TRUE;
		break;
	case IPL_SP_CLOCK_GO:
//		DIReport("clock go");
		ssp->clockadjust_check_frame--;
		if ( ssp->clockadjust_check_frame < 0 ) {
			ssp->clockadjust_check_frame = 0;
			ssp->clockadjust_check_state = IPL_SP_CLOCK_DONE;
		}
		return TRUE;
		break;
	case IPL_SP_CLOCK_DONE:
//		DIReport("clock done");
		return FALSE;
		break;
	case IPL_SP_CLOCK_NOW:
//		DIReport("clock now");
		ssp->clockadjust_check_state = checkClockAdjustSub();
		return TRUE;
		break;
	case IPL_SP_CLOCK_GOMENU:
//		DIReport("clock gomenu");
		ssp->clockadjust_check_frame--;
		if ( ssp->clockadjust_check_frame < 0 ) {
			ssp->clockadjust_check_frame = 0;
			ssp->force_cubemenu = TRUE;
		}
		return TRUE;
		break;
	}
	
	return FALSE;

}
