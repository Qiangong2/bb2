/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: ��������
  -----------------------------------------------------------------------------*/
#include <dolphin.h>

#include "gRand.h"

static u32 urand1_seed1;
static u32 urand1_seed2;
static u32 urand2_seed1;
static u32 urand2_seed2;

void init_rand()
{
	urand1_seed1 = (u32)OSGetTick();
	urand1_seed2 = (u32)OSGetTick();
	urand2_seed1 = (u32)OSGetTick();
	urand2_seed2 = (u32)OSGetTick();

	urand();

}

static u32 urand1( void )
{
	urand1_seed1 = urand1_seed1 * 1103515245 + 12345 ;

	if ( (urand1_seed2 & 1) != 0 )  urand1_seed2 ^= 0x11020 ;
	urand1_seed2 >>= 1 ;

	return ( urand1_seed1 ^ urand1_seed2 ) ;
}

static u32 urand2( void )
{
	urand2_seed1 = urand2_seed1 * 1103515245 + 67890 ;

	if ( (urand2_seed2 & 1) != 0 )  urand2_seed2 ^= 0x11020 ;
	urand2_seed2 >>= 1 ;

	return ( urand2_seed1 + urand2_seed2 ) ;
}

u32 urand( void )
{
	if ( OSGetTick() & 0x100 ) {
		return urand1();
	}
	else {
		return urand2();
	}
}

/*
if ( all_cont.hold == 0 ) {
	(void)urand1() ;
} else {
	(void)urand2() ;
}
*/
