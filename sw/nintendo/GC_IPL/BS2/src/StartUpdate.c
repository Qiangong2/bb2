/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: メニューモード時の各面のパラメータ
  -----------------------------------------------------------------------------*/
#include <dolphin.h>
#include <math.h>

#include "BS2Private.h"

#include "gCont.h"
#include "gBase.h"
#include "gFont.h"
#include "gCamera.h"
#include "gInit.h"
#include "gTrans.h"
#include "gLoader.h"

#include "gLayoutRender.h"
#include "gModelRender.h"

#include "MenuUpdate.h"	// メインメニューの動作を決定するEnumが入っている。
#include "MenuStartAnim.h"
#include "SplashMain.h"
#include "StartState.h"
#include "StartUpdate.h"
#include "gMainState.h"
#include "SplashMain.h"

#if defined(MPAL)
#include "mpal_mesg.h"
#elif defined(PAL)
#include "pal_mesg.h"
#else // NTSC
#include "ntsc_mesg.h"
#endif
//#include "2d_gameplay_2_Japanese.h"
#include "jaudio.h"

// start nakamura's edit
#include "smallCubeFader.h"
#include "smallCubeDraw.h"
// end

/*-----------------------------------------------------------------------------
  コントローラの入力を外部参照する。
  -----------------------------------------------------------------------------*/
extern DIPadStatus	DIPad;

static gLayoutHeader*	gam_lay;
static gLRStringHeader*	gam_str;

static gLRFade			banner2d;

static gLRFade			run_app;
static gLRFade			no_disk;
static gLRFade			fatal;
static gLRFade			reading;
static gLRFade			retryable;

static BOOL				start_runstate;

static BOOL				fatal_occurred;
static BOOL				stop_bgm;

/*-----------------------------------------------------------------------------
  static function
  -----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
  初期化
  -----------------------------------------------------------------------------*/
void
initStartMenu_Lang( void )
{
	gam_str = gGetBufferPtr( getID_2D_gamestart_string() );
}
void
initStartMenu( BOOL first )
{
	if ( first ) {
		gam_lay = gGetBufferPtr( getID_2D_gamestart_layout() );
//		gam_str = gGetBufferPtr( getID_2D_gamestart_string() );
		initStartMenu_Lang();
	}

	glrInitFadeState( &banner2d );
	glrInitFadeState( &run_app );	run_app.tag   = MSG_G000;
	glrInitFadeState( &no_disk );	no_disk.tag   = MSG_G102;
	glrInitFadeState( &fatal );		fatal.tag     = MSG_G100;
	glrInitFadeState( &reading );	reading.tag   = MSG_G103;
	glrInitFadeState( &retryable );	retryable.tag = MSG_G101;

	start_runstate = FALSE;
	fatal_occurred = FALSE;
	stop_bgm = FALSE;
}

/*-----------------------------------------------------------------------------
  コントローラの入力から更新する
  -----------------------------------------------------------------------------*/
static s32
updateStartMenuControl( void )
{
	MenuState*	mesp = getMenuState();
	s32	ret = IPL_MENU_GAMESTART_SELECT;

	if ( !isConcentSCSmallCube() )  return ret;
	if ( !isCubeAnimFinished( 0 ) ) return ret;

	if ( fatal_occurred ) return ret;
	
	if ( DIPad.buttonDown & PAD_BUTTON_B ) {
		if ( mesp->fadeout_z_param == 32767 || 
			 mesp->enable_pre_key_input ) {
			if ( gGetRunAppFadeAlpha() == 0 ) {
				Jac_PlaySe( JAC_SE_TO_3D );
				ret = IPL_MENU_GAMESTART;
			}
		}
	}

#ifndef IPL
	if ( DIPad.buttonDown & PAD_BUTTON_MENU ) {
		if ( start_runstate ) start_runstate = FALSE;
		else {
			start_runstate = TRUE;
			stop_bgm = TRUE;
			Jac_StopBgm();
			Jac_PlaySe( JAC_SE_GAME_START );
		}
	}
#endif

#ifdef IPL
	if ( DIPad.buttonDown & PAD_BUTTON_MENU ) {
		if ( gGetBs2State() == BS2_RUN_APP ) {
			stop_bgm = TRUE;
			Jac_StopBgm();
			Jac_PlaySe( JAC_SE_GAME_START );
			start_runstate = TRUE;
		}
	}
#endif

	return ret;
}

static void
updateBannerChecker( void )
{
	if ( gIsBanner() ) {
		glrUpdateFade( &banner2d, IPL_WINDOW_FADEIN );
	}
	else {
		glrUpdateFade( &banner2d, IPL_WINDOW_FADEOUT );
	}
}

static void
updateBs2State( void )
{
	BS2State bs2state;
	SplashState* ssp = inSplashGetState();

	if ( !fatal_occurred ) {
		bs2state = gGetBs2State();
	} else {
		bs2state = BS2_FATAL_ERROR;
	}

	if ( bs2state == BS2_RUN_APP ) 	glrUpdateFade( &run_app, IPL_WINDOW_FADEIN  );
	else							glrUpdateFade( &run_app, IPL_WINDOW_FADEOUT );

	if ( bs2state == BS2_NO_DISK || bs2state == BS2_COVER_OPEN )
			glrUpdateFade( &no_disk, IPL_WINDOW_FADEIN  );
	else 	glrUpdateFade( &no_disk, IPL_WINDOW_FADEOUT );

	if ( bs2state == BS2_RETRY   || bs2state == BS2_WRONG_DISK )
			glrUpdateFade( &retryable, IPL_WINDOW_FADEIN  );
	else 	glrUpdateFade( &retryable, IPL_WINDOW_FADEOUT );

	if ( bs2state == BS2_FATAL_ERROR )	glrUpdateFade( &fatal, IPL_WINDOW_FADEIN  );
	else 								glrUpdateFade( &fatal, IPL_WINDOW_FADEOUT );

	switch ( bs2state ) {
	case BS2_RUN_APP:
		glrUpdateFade( &reading, IPL_WINDOW_FADEOUT );
		break;
	case BS2_FATAL_ERROR:
		start_runstate = FALSE;
		fatal_occurred = TRUE;
		ssp->force_fatalerror = TRUE;
		glrUpdateFade( &reading, IPL_WINDOW_FADEOUT );
		break;
	case BS2_NO_DISK:
	case BS2_RETRY:
	case BS2_WRONG_DISK:
	case BS2_COVER_OPEN:
		start_runstate = FALSE;
		glrUpdateFade( &reading, IPL_WINDOW_FADEOUT );
		break;
	default:
		glrUpdateFade( &reading, IPL_WINDOW_FADEIN  );
		break;
	}
}

/*-----------------------------------------------------------------------------
  
  -----------------------------------------------------------------------------*/
s32
updateStartMenu( void )
{
	s32	ret = IPL_MENU_GAMESTART_SELECT;
	// start nakamura's edit
	BS2State bs2state;
	u32      start_nodisk_question; // スタート面の表示の種類
	// end

#ifdef IPL
	if ( start_runstate ) {
		gRunAppUpdateSlow();
	}
	else {
		if ( stop_bgm ) {
			Jac_PlayBgm( JAC_BGM_MENU );
			stop_bgm = FALSE;
		}
		gRunAppFadein();
#endif
		
		ret = updateStartMenuControl();
		
#ifdef IPL
	}
#endif

	updateBs2State();

	if ( isConcentSCSmallCube() ) {
	// start nakamura's edit
	bs2state = gGetBs2State();
		
	switch( bs2state ){
	case BS2_RUN_APP:
	    start_nodisk_question = 0;
	    break;
	case BS2_NO_DISK:
	    start_nodisk_question = 1;
	    break;
	default:
	    start_nodisk_question = 2;
	    break;
	}
	updateStartSmallCube( (u32)start_runstate, start_nodisk_question );
	updateSmallCubeDraw();
	// end
	}

	updateBannerChecker();
	
	return ret;
}

void
resetStartMessage( void )
{
	// start nakamura's edit
	BS2State bs2state;
	u32      start_nodisk_question; // スタート面の表示の種類
	bs2state = gGetBs2State();
		
	switch( bs2state ){
	case BS2_RUN_APP:
	    start_nodisk_question = 0;
	    break;
	case BS2_NO_DISK:
	    start_nodisk_question = 1;
	    break;
	default:
	    start_nodisk_question = 2;
	    break;
	}
	updateStartSmallCube( (u32)start_runstate, start_nodisk_question );
	updateSmallCubeDraw();
	// end
	glrResetFade( &run_app		);
	glrResetFade( &no_disk		);
	glrResetFade( &fatal  		);
	glrResetFade( &reading		);
	glrResetFade( &retryable	);
	glrResetFade( &banner2d		);
}

BOOL
getStartFatalState( void )
{
	return fatal_occurred;
}





























/*-----------------------------------------------------------------------------
  draw section
  -----------------------------------------------------------------------------*/
static void
drawBanner2d( u8 alpha )
{
	ResTIMG	tex_image;
//	char*	banner_data;
	gBanner2*	banner_data;
	s16		b_alpha;
	GXColor white = { 255, 255, 255, 255 };

	glrGetOffsetAlpha( &banner2d, &b_alpha, NULL );
	if ( b_alpha == 0 ) return;

	banner_data = (gBanner2*)gGetBannerBuffer();

	white.a = (u8)( alpha * b_alpha / 255 );

	tex_image.enableLOD       = GX_DISABLE;
	tex_image.enableEdgeLOD   = GX_DISABLE;
	tex_image.enableBiasClamp = GX_DISABLE;
	tex_image.enableMaxAniso  = (u8)GX_ANISO_1;
	tex_image.width  = 96;
	tex_image.height = 32;
	tex_image.wrapS  = GX_CLAMP;
	tex_image.wrapT  = GX_CLAMP;
	tex_image.format = GX_TF_RGB5A3;
	tex_image.minFilter = GX_LINEAR;
	tex_image.magFilter = GX_LINEAR;
	tex_image.minLOD    = 0;
	tex_image.maxLOD    = 0;
	tex_image.LODBias   = 0;
	tex_image.indexTexture = FALSE;
	tex_image.imageOffset  = (s32)( (u32)banner_data + 32 - (u32)&tex_image );

	gSetGXforDirect( TRUE, FALSE, TRUE );
	glrDrawPictureTex( 'bana', gam_lay, &white, &tex_image );

	setGXforglrText();

	if ( gGetBannerType() == cIPL_BannerType_OneLanguage ) {
		glrDrawTextBoxNStringOneLine( 'titl', gam_lay, &white  , (char*)banner_data->com[0].title_2d , 64);
		glrDrawTextBoxNStringOneLine( 'makr', gam_lay, &white  , (char*)banner_data->com[0].maker_2d , 64);
		glrDrawTextBoxNStringTwoLine( 'info', gam_lay, &white  , (char*)banner_data->com[0].comment  , 128);
	}
	else if ( gGetBannerType() == cIPL_BannerType_SixLanguage ) {
		glrDrawTextBoxNStringOneLine( 'titl', gam_lay, &white  , (char*)banner_data->com[ gGetPalCountry() ].title_2d , 64);
		glrDrawTextBoxNStringOneLine( 'makr', gam_lay, &white  , (char*)banner_data->com[ gGetPalCountry() ].maker_2d , 64);
		glrDrawTextBoxNStringTwoLine( 'info', gam_lay, &white  , (char*)banner_data->com[ gGetPalCountry() ].comment  , 128);
	}
}

static void
drawLayout( u8 alpha )
{
	GXColor white = { 255, 255, 255, 255 };

	white.a = alpha;

	drawBanner2d( alpha );//gam_lay, &white );
//	glrDrawString( MSG_G000, gam_str, gam_lay, &white );
}

static void
drawMessage( u8 alpha )
{
	GXColor white = { 255,255,255,255};

	white.a = (u8)( 255 * alpha / 255 );

	setGXforglrText();
	glrDrawFadeString( &run_app,   gam_str, gam_lay, &white );
	glrDrawFadeString( &no_disk,   gam_str, gam_lay, &white );
//	glrDrawFadeString( &fatal  ,   gam_str, gam_lay, &white );
	glrDrawFadeString( &reading,   gam_str, gam_lay, &white );
	glrDrawFadeString( &retryable, gam_str, gam_lay, &white );
}

/*-----------------------------------------------------------------------------
  アイコン選択時の描画。
  -----------------------------------------------------------------------------*/
void
drawStartMenu( u8 all_alpha, u8 grid_alpha, u8 layout_alpha )
{
	MainState* msp  = getMainState();
	J3DTransformInfo ti = { 1.f, -1.f, 1.f, 0,0,0,0, -292, 224, 0 };
	Vec     up      = {   0.f,   1.f,   0.f };
	Vec     camLoc	= {   0.f,   0.f,   0.f };
	Vec		objPt	= {   0.f,   0.f,   0.f };
	Mtx m, cm, inv;

	camLoc.z = 224.0f / tanf( msp->fovy * 3.141592f / 360.f );
    MTXLookAt( cm, &camLoc, &up, &objPt);
	gGetTransformMtx( &ti , m );
	MTXConcat( cm, m , m );
	gGetInverseTranspose( m , inv );
	GXLoadPosMtxImm( m , GX_PNMTX0 );
	GXLoadNrmMtxImm( inv , GX_PNMTX0 );
	GXSetCurrentMtx( GX_PNMTX0 );

	// grid alpha
	glrDraw2dGrid( m, (u8)grid_alpha );
	
	drawLayout( (u8)layout_alpha );

	if ( isConcentSCSmallCube() ) {
	// start nakamura's edit
	drawSmallCubeDraw( all_alpha );
	// end
	}

	GXLoadPosMtxImm( m , GX_PNMTX0 );
	GXLoadNrmMtxImm( inv , GX_PNMTX0 );
	GXSetCurrentMtx( GX_PNMTX0 );

	drawMessage( (u8)layout_alpha );
}
