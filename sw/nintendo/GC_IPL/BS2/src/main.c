/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: メインルーチン
  -----------------------------------------------------------------------------*/
#include <dolphin.h>
#include <private/viprivate.h>
#include <dolphin/os/OSBootInfo.h>
#include <private/OSLoMem.h>
#include "BS2Private.h"
#include <string.h>
#include <stdlib.h>

#include "gCont.h"
#include "gInit.h"
#include "gCamera.h"
#include "gLoader.h"
#include "gBase.h"
#include "gMath.h"
#include "gTrans.h"
#include "gRand.h"
#include "gFont.h"

#include "gModelRender.h"
#include "gLayoutRender.h"
#include "gMRanime.h"

#ifdef JHOSTIO
#include "IHIroot.h"
#endif

#include "LogoUpdate.h"
#include "LogoDraw.h"
#include "Splash.h"
#include "SplashMain.h"
#include "CardSequence.h"
#include "gMainState.h"
#include "gRenderMode.h"

#include "jaudio.h"

/*---------------------------------------------------------------------------*
   Forward references
 *---------------------------------------------------------------------------*/
#define DEFAULT_FIFO_SIZE		(256 * 1024 * 2)

void        mainmenu        ( void );
static void ProjectionInit  ( void );
static void testRender( Mtx );

MainState* msp;

#ifndef JHOSTIO
MainState ms;
#endif

extern DIPadStatus	DIPad;

#define ARAM_MAX_NUM_BLOCK	256
u32	aram_block[ ARAM_MAX_NUM_BLOCK ];


/*-----------------------------------------------------------------------------
  
  -----------------------------------------------------------------------------*/
void
initMainState( MainState* smsp )
{
	smsp->znear = 50.0f;
	smsp->zfar = 10000.0f;
	smsp->fovy = 20;//8;

	smsp->distCamera = 2000.f;
	smsp->PosCamera = -55.f;
	smsp->logo_camera_height = -55.f;
	smsp->menu_camera_height = -10.f;
	
	smsp->update = 1;
#ifndef IPL
	smsp->enableProcBar = 0;
#else
	smsp->enableProcBar = 0;
#endif

	smsp->ortho = 1;
	smsp->orthoScale = 1.0f;
	smsp->orthoPos = 0.0f;

	smsp->r =0;
	smsp->g =0;
	smsp->b =0;

	smsp->window_mergin = 8;
	smsp->font_mergin	= 6;

	smsp->key_repeat_threshold = 35;
	smsp->key_repeat_repeating = 10;

	smsp->bright_alpha_max = 255;
	smsp->bright_alpha_min = 70;
	smsp->bright_frame = 0;
	smsp->bright_frame_max = 35;
	smsp->bright_frame_bounds = 1;
}

/*---------------------------------------------------------------------------*
   Application main loop
 *---------------------------------------------------------------------------*/
void mainmenu( void )
{
	
	GXRenderModeObj *rMode;     // pointer to the render mode struct
	void*	audio;

    ARInit( aram_block , ARAM_MAX_NUM_BLOCK );
    ARQInit();

#if defined(MPAL)
    rMode = &GXMpal592x448IntAa;
#elif defined(PAL)
    rMode = &GXPal592x448IntAa;
//    rMode = &GXDebugPal592x448IntAa;
//    rMode = &GXNtsc592x448IntAa;
#else
    rMode = &GXNtsc592x448IntAa;
#endif
	gInitHeap();
	gInitGX( DEFAULT_FIFO_SIZE , rMode );

#if 1
	// audio init
	audio = gAlloc( 1024*1024 );
	Jac_Start(audio, 1024*1024, 8*1024*1024);
	//=============
#endif
	
	GXSetVerifyLevel( GX_WARN_NONE );
	gResolveLanguage();
	gLoadBinary();	// データの読みこみ。
	gInitBannerBuffer();

#ifdef JHOSTIO
	IHIInitialize();
	msp = getIHIMainState();
#else
	msp = &ms;
	initMainState( msp );
#endif
	__CARDSetDiskID((DVDDiskID*)NULL);
	initCardThread();
	DIPadInit();
	gfInitFontData(NULL);
	gNewSinTable(12);
	init_rand();

#ifdef IPL
	gWaitForConfirmCoverState();
#endif
	
	SplashInit( TRUE );

#ifndef IPL
	__VISetAdjustingValues( 0, 0 );
#else
	
#endif

	Jac_OutputMode( OSGetSoundMode() );

    while(TRUE)
    {
		DIPadRead();
		gTickBs2();
		gBeginRender();

		// update section
		gfUpdateAlphaAnime();
		buildCamera ();
		loadLight   ();
		SplashUpdate();
		// update section

		// begin top draw section
		ProjectionInit();
		SplashDraw();
		gRunAppDraw();
		// end top draw section

		gCopyDispTop();
		gPreBottomRender();

		// begin bottom half draw section
		ProjectionInit();
		SplashDraw();
		gRunAppDraw();
#ifndef IPL
	    if ( msp->enableProcBar ) gDrawProcMeter();	// 現状では、下半分のみでOK。
#endif
		// end bottom half draw section
		gCopyDispBottom();

		GXFlush();

		// animation frame update
#ifdef JHOSTIO
		IHIEventLoop();
#endif
		gDoneRender();

		SplashDrawDone();

		probeCard();
		gCheckReset();
		
#ifndef IPL
		if ( DIPad.buttonDown & PAD_TRIGGER_Z ) {
//			gDumpHeap();
		}
#endif
	}
}


/*---------------------------------------------------------------------------*
   Functions
 *---------------------------------------------------------------------------*/
MainState* getMainState( void )
{
	return msp;
}
/*---------------------------------------------------------------------------*
  projection init
 *---------------------------------------------------------------------------*/
static void
ProjectionInit( void )
{
    Mtx44   p;      // projection matrix
	SplashState* ssp = inSplashGetState();

	if ( !msp->ortho ) {
		MTXPerspective( p, msp->fovy ,
						(f32)(592.0f/448.0f),
						msp->znear, msp->zfar );
		GXSetProjection(p, GX_PERSPECTIVE);
	}
	else {
		MTXOrtho( p,
				  224.f , -224.0f, 
				  -296.0f, 292.0f,
				  msp->znear, msp->zfar );
		GXSetProjection( p, GX_ORTHOGRAPHIC );
	}

	msp->update = 0;
}
