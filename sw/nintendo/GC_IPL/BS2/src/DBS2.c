#include <dolphin.h>
#include "SplashMain.h"
#include "gBase.h"

extern void mainmenu( void );

void main( int argc, char** argv )
{
	OSInit();
	DVDInit();
	CARDInit();

	VIInit();

	PADSetSpec( 5 );
	PADInit();

	DIReport("\n--- EAD BOOTROM DUMMY BS2 ---\n");

	if ( argc == 2 ) {
		switch ( *argv[1] )
		{
		case '0':
			setSplashRunstate( cSpRun_Rotate );
			break;
		case '1':
			setSplashRunstate( cSpRun_Init );
			break;
		case '2':
			setSplashRunstate( cSpRun_Logo );
			break;
		case '3':
			setSplashRunstate( cSpRun_LogotoMenu );
			break;

		case '4':
			setSplashRunstate( cMenu_Select );
			break;

		default :
			setSplashRunstate( cSpRun_Rotate );
			break;
		}
	}
	else {
		setSplashRunstate( cSpRun_Rotate );
	}
	mainmenu();

    OSHalt("BS2 ERROR >>> SHOULD NEVER REACH HERE");
}
