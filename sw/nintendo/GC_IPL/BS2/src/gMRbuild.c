/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: J3DBinary 1.5 Renderer
  
  build実装部
  -----------------------------------------------------------------------------*/
#include <dolphin.h>

#include "gBase.h"

#include "gModelRender.h"
#include "gMRnode.h"

/*-----------------------------------------------------------------------------
  static prototype
  -----------------------------------------------------------------------------*/
static void		buildModelInfoBlock	( J3DModelInfoBlock*	mdp );
static void		buildVertexBlock	( J3DVertexBlock*		mdp );
static void		buildEnvelopBlock	( J3DEnvelopBlock*		mdp );
static void		buildDrawBlock		( J3DDrawBlock*			mdp );
static void		buildJointBlock		( J3DJointBlock*		mdp );
static void		buildMaterialBlock	( J3DMaterialBlock*		mdp );
static void		buildShapeBlock		( J3DShapeBlock*		mdp );
static void		buildTextureBlock	( J3DTextureBlock*		mdp );

static void		parseHierarchy ( J3DModelHierarchy* hierarchy , gmrModelData* model );
static void		buildMtxBuffer ( gmrModel* d );
static void		buildNodeTree( gmrModelData* d );

/*-----------------------------------------------------------------------------
  各ブロックをbuildする。
  いや、別にオフセットをポインタにしてるだけっす。
  -----------------------------------------------------------------------------*/
static void
buildModelInfoBlock	(
	J3DModelInfoBlock*	mdp )
{
	if ( mdp->modelHierarchy ) mdp->modelHierarchy = (J3DModelHierarchy*)( (u32)mdp->modelHierarchy + (u32)mdp );
}

static void
buildVertexBlock	(
	J3DVertexBlock*		mdp )
{
	int i;
	
	if ( mdp->vtxAttr ) mdp->vtxAttr = (GXVtxAttrFmtList*)( (u32)mdp->vtxAttr + (u32)mdp );
	if ( mdp->vtxPos  ) mdp->vtxPos  = (void*)			   ( (u32)mdp->vtxPos  + (u32)mdp );
	if ( mdp->vtxNrm  ) mdp->vtxNrm  = (void*)            ( (u32)mdp->vtxNrm  + (u32)mdp );
	if ( mdp->vtxNbt  ) mdp->vtxNbt  = (void*)            ( (u32)mdp->vtxNbt  + (u32)mdp );
	
	if ( mdp->vtxColor[0] ) mdp->vtxColor[0] = (void*) ( (u32)mdp->vtxColor[0] + (u32)mdp );
	if ( mdp->vtxColor[1] ) mdp->vtxColor[1] = (void*) ( (u32)mdp->vtxColor[1] + (u32)mdp );

	for ( i = 0 ; i < 8 ; i++ )
	{
		if ( mdp->vtxTexCoord[i] ) mdp->vtxTexCoord[i] = (void*) ( (u32)mdp->vtxTexCoord[i] + (u32)mdp );
	}
}

static void
buildEnvelopBlock	(
	J3DEnvelopBlock*	mdp )
{
	if ( mdp->wEvlpMixMtxNum   ) mdp->wEvlpMixMtxNum   = (u8*)  ( (u32)mdp->wEvlpMixMtxNum   + (u32)mdp );
	if ( mdp->wEvlpMixMtxIndex ) mdp->wEvlpMixMtxIndex = (u16*) ( (u32)mdp->wEvlpMixMtxIndex + (u32)mdp );
	if ( mdp->wEvlpMixWeight   ) mdp->wEvlpMixWeight   = (f32*) ( (u32)mdp->wEvlpMixWeight   + (u32)mdp );
	if ( mdp->invJointMtx	    ) mdp->invJointMtx      = (Mtx*) ( (u32)mdp->invJointMtx      + (u32)mdp );
}

static void
buildDrawBlock		(
	J3DDrawBlock*		mdp )
{
	if ( mdp->drawMtxFlag  ) mdp->drawMtxFlag  = (u8*)  ( (u32)mdp->drawMtxFlag  + (u32)mdp );
	if ( mdp->drawMtxIndex ) mdp->drawMtxIndex = (u16*) ( (u32)mdp->drawMtxIndex + (u32)mdp );
}

static void
buildJointBlock		(
	J3DJointBlock*		mdp )
{
//	DIReport("mdp:%08x\n",mdp );
//	DIReport("jointInit:%08x\n",mdp->jointInit );
//	DIReport("jointID:%08x\n",mdp->jointID );
	if ( mdp->jointInit  ) mdp->jointInit = (J3DJointInitData*) ( (u32)mdp->jointInit + (u32)mdp );
	if ( mdp->jointID    ) mdp->jointID   = (u16*)              ( (u32)mdp->jointID   + (u32)mdp );
	// NameResは使用禁止
}

static void
buildMaterialBlock	(
	J3DMaterialBlock*	mdp )
{
	if ( mdp->materialInit  ) mdp->materialInit  = (J3DMaterialInitData*) ( (u32)mdp->materialInit  + (u32)mdp );
	if ( mdp->materialID    ) mdp->materialID    = (u16*)                 ( (u32)mdp->materialID    + (u32)mdp );
	if ( mdp->cullMode      ) mdp->cullMode      = (GXCullMode*)          ( (u32)mdp->cullMode      + (u32)mdp );
	if ( mdp->color         ) mdp->color         = (GXColor*)             ( (u32)mdp->color         + (u32)mdp );
	if ( mdp->colorChanNum  ) mdp->colorChanNum  = (u8*)                  ( (u32)mdp->colorChanNum  + (u32)mdp );
	if ( mdp->colorChanInfo ) mdp->colorChanInfo = (J3DColorChanInfo*)    ( (u32)mdp->colorChanInfo + (u32)mdp );
	if ( mdp->texGenNum     ) mdp->texGenNum     = (u8*)                  ( (u32)mdp->texGenNum     + (u32)mdp );
	if ( mdp->texCoordInfo  ) mdp->texCoordInfo  = (J3DTexCoordInfo*)     ( (u32)mdp->texCoordInfo  + (u32)mdp );
	if ( mdp->texMtxInfo    ) mdp->texMtxInfo    = (J3DTexMtxInfo*)       ( (u32)mdp->texMtxInfo    + (u32)mdp );
	if ( mdp->texNo         ) mdp->texNo         = (u16*)                 ( (u32)mdp->texNo         + (u32)mdp );
	if ( mdp->tevOrderInfo  ) mdp->tevOrderInfo  = (J3DTevOrderInfo*)     ( (u32)mdp->tevOrderInfo  + (u32)mdp );
	if ( mdp->tevColorInfo  ) mdp->tevColorInfo  = (GXColorS10*)          ( (u32)mdp->tevColorInfo  + (u32)mdp );
	if ( mdp->tevStageNum   ) mdp->tevStageNum   = (u8*)                  ( (u32)mdp->tevStageNum   + (u32)mdp );
	if ( mdp->tevStageInfo  ) mdp->tevStageInfo  = (J3DTevStageInfo*)     ( (u32)mdp->tevStageInfo  + (u32)mdp );
	if ( mdp->fogInfo       ) mdp->fogInfo       = (J3DFogInfo*)          ( (u32)mdp->fogInfo       + (u32)mdp );
	if ( mdp->alphaCompInfo ) mdp->alphaCompInfo = (J3DAlphaCompInfo*)    ( (u32)mdp->alphaCompInfo + (u32)mdp );
	if ( mdp->blendInfo     ) mdp->blendInfo     = (J3DBlendInfo*)        ( (u32)mdp->blendInfo     + (u32)mdp );
	if ( mdp->zmodeInfo     ) mdp->zmodeInfo     = (J3DZModeInfo*)        ( (u32)mdp->zmodeInfo     + (u32)mdp );
	if ( mdp->zcompLoc      ) mdp->zcompLoc      = (GXBool*)              ( (u32)mdp->zcompLoc      + (u32)mdp );
	if ( mdp->dither        ) mdp->dither        = (GXBool*)              ( (u32)mdp->dither        + (u32)mdp );
	if ( mdp->nbtScaleInfo  ) mdp->nbtScaleInfo  = (J3DNBTScaleInfo*)     ( (u32)mdp->nbtScaleInfo  + (u32)mdp );
}

static void
buildShapeBlock		(
	J3DShapeBlock*		mdp )
{
	if ( mdp->shapeInit       ) mdp->shapeInit       = (J3DShapeInitData*)     ( (u32)mdp->shapeInit       + (u32)mdp );
	if ( mdp->shapeID         ) mdp->shapeID         = (u16*)                  ( (u32)mdp->shapeID         + (u32)mdp );
//	if ( mdp->shapeName       ) mdp->shapeName       = (void*) ( (u32)mdp->shapeName + (u32)mdp );
	if ( mdp->vtxDescList     ) mdp->vtxDescList     = (GXVtxDescList*)        ( (u32)mdp->vtxDescList     + (u32)mdp );
	if ( mdp->mtxIndexList    ) mdp->mtxIndexList    = (u16*)                  ( (u32)mdp->mtxIndexList    + (u32)mdp );
	if ( mdp->drawDisplayList ) mdp->drawDisplayList = (u8*)                   ( (u32)mdp->drawDisplayList + (u32)mdp );
	if ( mdp->mtxInit         ) mdp->mtxInit         = (J3DShapeMtxInitData*)  ( (u32)mdp->mtxInit         + (u32)mdp );
	if ( mdp->drawInit        ) mdp->drawInit        = (J3DShapeDrawInitData*) ( (u32)mdp->drawInit        + (u32)mdp );
}

static void
buildTextureBlock	(
	J3DTextureBlock*	mdp )
{
	if ( mdp->textureImage ) mdp->textureImage = (ResTIMG*) ( (u32)mdp->textureImage + (u32)mdp );
//	if ( mdp->textureName  ) mdp->textureName  = (void*)    ( (u32)mdp->textureName  + (u32)mdp );
}


/*-----------------------------------------------------------------------------
  bmdのポインタ解決をする。ロード後に一度実行すれば、使用可能。
  gmrModelData構造体は、呼び出し側で作成し、ポインタを渡す。描画時に使用する。
  -----------------------------------------------------------------------------*/
void
buildModelData( gmrModelData* data )
{
	J3DGraphBinaryHeader* bhp = (J3DGraphBinaryHeader*)data->modeldata;
	u8*	mdp = (u8*)data->modeldata;
	s32	dataSize = (s32)bhp->dataSize - 32;
	
	if ( bhp->signature != 'J3D1' || bhp->dataType != 'bmd1' ) {
		data->modeldata = NULL;
		OSHalt("ERROR: This renderer ONLY supports J3DBinary1.5\n");
		return;
	}

	// J3DGraphBinaryHeaderを飛ばす。
	mdp += 32;

	while ( dataSize > 0 )
	{
		switch ( ( (J3DDataBlockHeader*)mdp )->kind )
		{
		case 'INF1': data->ModelInfoP = (J3DModelInfoBlock*)mdp;  buildModelInfoBlock( (J3DModelInfoBlock*)mdp ); break;
		case 'VTX1': data->VertexP 	  = (J3DVertexBlock*)mdp;  buildVertexBlock	( (J3DVertexBlock*)   mdp ); break;
		case 'EVP1': data->EnvelopP   = (J3DEnvelopBlock*)mdp;  buildEnvelopBlock	( (J3DEnvelopBlock*)  mdp ); break;
		case 'DRW1': data->DrawP 	  = (J3DDrawBlock*)mdp;  buildDrawBlock	    ( (J3DDrawBlock*)     mdp ); break;
		case 'JNT1': data->JointP 	  = (J3DJointBlock*)mdp;  buildJointBlock	( (J3DJointBlock*)    mdp ); break;
		case 'MAT1': data->MaterialP  = (J3DMaterialBlock*)mdp;  buildMaterialBlock ( (J3DMaterialBlock*) mdp ); break;
		case 'SHP1': data->ShapeP 	  = (J3DShapeBlock*)mdp;  buildShapeBlock	( (J3DShapeBlock*)    mdp ); break;
		case 'TEX1': data->TextureP   = (J3DTextureBlock*)mdp;  buildTextureBlock	( (J3DTextureBlock*)  mdp ); break;
		default:
			if ( dataSize > 0 ) dataSize = 0; DIReport("Illegal model data\n");break;
		}
		if ( dataSize > 0 ) {
			dataSize -= OSRoundUp32B( ( (J3DDataBlockHeader*)mdp )->size );
		}

		mdp = (u8*)( (u32)mdp + OSRoundUp32B( ( (J3DDataBlockHeader*)mdp )->size ) );
	}
	
	// ノードツリーを作成する。
	buildNodeTree( data );
}


/*-----------------------------------------------------------------------------
  ノードのツリーを作成する。
  -----------------------------------------------------------------------------*/
static void
parseHierarchy ( J3DModelHierarchy* hierarchy , gmrModelData* d )
{
	gmrNode*           nodeArray = d->root;
	J3DModelHierarchy* hrp       = hierarchy;

	u32      numNode = 0;
	gmrNode* parent  = NULL;
	gmrNode* node    = &nodeArray[ numNode ];

	while ( hrp->kind != cNodeExit )
	{
		switch ( hrp->kind )
		{
		case cNodeBegin:
			parent = node;
//			DIReport("child begin:%d\n", hrp->index);
			break;
			
		case cNodeEnd:
//			DIReport("child end:%d\n", hrp->index);
			parent = searchParent( parent );
			break;
			
		case cNodeJoint:
//			DIReport("joint:%d:%08x\n", hrp->index , &d->joint[ hrp->index ]);
			node	= &nodeArray[ numNode++ ];
			initNode( node );
			
			node->type = cNodeJoint;
//			node->nP   = &d->joint[ hrp->index ];
			node->id   = hrp->index;
			addChildNode( parent , node );
			
			break;
			
		case cNodeMaterial:
//			DIReport("material:%d:%08x\n", hrp->index, &d->material[ hrp->index ]);
			node	= &nodeArray[ numNode++ ];
			initNode( node );
			
			node->type = cNodeMaterial;
//			node->nP   = &d->material[ hrp->index ];
			node->id   = hrp->index;
			addChildNode( parent , node );
			
			break;
			
		case cNodeShape:
//			DIReport("shape:%d:%08x\n", hrp->index, &d->shape[ hrp->index ]);
			node	= &nodeArray[ numNode++ ];
			initNode( node );
			
			node->type = cNodeShape;
//			node->nP   = &d->shape[ hrp->index ];
			node->id   = hrp->index;
			addChildNode( parent , node );
			
			break;
		}
		++hrp;
	}

//	dumpTree( nodeArray );

}

/*-----------------------------------------------------------------------------
  ノードツリーを作成（必要なメモリも確保）
  -----------------------------------------------------------------------------*/
static void
buildNodeTree( gmrModelData* d )
{
//	gmrModelData* md = d->gmrmd;

	int allNodeNum = d->JointP->jointNum +
				     d->ShapeP->shapeNum +
					 d->MaterialP->materialNum;

	d->root = (gmrNode*)gAlloc( allNodeNum * sizeof(gmrNode) );

	d->joint    = (gmrJoint*)   gAlloc( d->JointP->jointNum       * sizeof( gmrJoint    ) );
	d->material = (gmrMaterial*)gAlloc( d->ShapeP->shapeNum       * sizeof( gmrMaterial ) );
	d->shape    = (gmrShape*)   gAlloc( d->MaterialP->materialNum * sizeof( gmrShape    ) );

	buildJoint   ( d->JointP    , d->joint    );
	buildMaterial( d->MaterialP , d->material );
	buildShape   ( d->ShapeP    , d->shape    );
	
	parseHierarchy ( d->ModelInfoP->modelHierarchy , d );
	
}

/*-----------------------------------------------------------------------------
  描画用マトリックスバッファの確保
  -----------------------------------------------------------------------------*/
static void
buildMtxBuffer( gmrModel* d )
{
	gmrModelData* data = d->gmrmd;

//	d->drawMtx = (Mtx*)gAlloc( data->DrawP->drawMtxNum * sizeof( Mtx ) ); // Weightも含む。
	d->animMtx = (Mtx*)gAlloc( data->JointP->jointNum  * sizeof( Mtx ) ); // JOINT用
}

/*-----------------------------------------------------------------------------
  モデル描画用のインスタンスを生成する。
  -----------------------------------------------------------------------------*/
void
buildModel( gmrModel* data , int buildMDFlag )
{
	// モデルデータも初期化する必要があれば、しておく。
	if ( buildMDFlag ) {
		buildModelData( data->gmrmd );
	}

	// 描画用マトリックスバッファを確保する。
	buildMtxBuffer( data );

	data->c_anime = NULL;
	data->anime = NULL;
	data->mat_color = NULL;
	data->enable_viewcalc = FALSE;
}

/*-----------------------------------------------------------------------------
  モデル描画用のインスタンスをコピーします。
  -----------------------------------------------------------------------------*/
void
copyModel( gmrModel* dst, gmrModel* src )
{
	*dst = *src;

	buildMtxBuffer( dst );
}

/*-----------------------------------------------------------------------------
  アニメーションデータをbuildします。
  -----------------------------------------------------------------------------*/
void
buildAnime( gmrAnime* data , int buildFlag )
{
	J3DAnmTransformKeyData* adp = (J3DAnmTransformKeyData*)( (u8*)data->animedata+32 );

	if ( buildFlag ) {
		if ( adp->anmTable    ) adp->anmTable    = (J3DAnmTransformKeyTable*)( (u32)adp->anmTable    + (u32)adp );
		if ( adp->scaling     ) adp->scaling     = (f32*)( (u32)adp->scaling     + (u32)adp );
		if ( adp->rotation    ) adp->rotation    = (s16*)( (u32)adp->rotation    + (u32)adp );
		if ( adp->translation ) adp->translation = (f32*)( (u32)adp->translation + (u32)adp );
	}

	data->attribute = adp->attribute;
	data->shift     = adp->shift;
	data->frameMax  = adp->frameMax;

	data->anmTable  = adp->anmTable;
	data->scale     = adp->scaling;
	data->rotate    = adp->rotation;
	data->translate = adp->translation;
/*
	DIReport("attr:%d\n", data->attribute );
	DIReport("shift:%d\n", data->shift );
	DIReport("frameMax:%d\n", data->frameMax );

	DIReport("animedata:%08x\n", adp );
	DIReport("anmtable:%08x\n", data->anmTable );
	DIReport("scale:%08x\n", data->scale );
	DIReport("rotate:%08x\n", data->rotate );
	DIReport("translate:%08x\n", data->translate );
  */
}

/*-----------------------------------------------------------------------------
  gmrAnimeをgmrModelに関連付けます。
  -----------------------------------------------------------------------------*/
void
setAnime( gmrModel* model , gmrAnime* anime )
{
	model->anime = anime;
}

void
buildColorAnime( gmrModel* model, gmrColorAnime* data, int flag )
{
	gMRPolyAnimeBlock*	adp = data->animedata;
	u16	mat_num = (u16)( ( (u32)adp->table - (u32)adp->poly_anime_index ) / 2 );
	
	if ( flag )  {
		if ( adp->poly_anime_index ) adp->poly_anime_index = (s16*)( (u32)adp->poly_anime_index + (u32)adp );
		if ( adp->r )          adp->r                      = (f32*)( (u32)adp->r     + (u32)adp );
		if ( adp->g )          adp->g                      = (f32*)( (u32)adp->g     + (u32)adp );
		if ( adp->b )          adp->b                      = (f32*)( (u32)adp->b     + (u32)adp );
		if ( adp->a )          adp->a                      = (f32*)( (u32)adp->a     + (u32)adp );
		if ( adp->table )      adp->table = (gMRPolyAnimeKeyTable*)( (u32)adp->table + (u32)adp );
	}

	model->c_anime = data;
	data->playmode  = adp->playmode;
	data->max_frame = (s16)adp->max_frame;

	data->poly_anime_index = adp->poly_anime_index;
	data->r = adp->r;
	data->g = adp->g;
	data->b = adp->b;
	data->a = adp->a;
	data->table = adp->table;

	model->mat_color = (GXColor*)gAlloc( mat_num * sizeof(GXColor) );
/*
	DIReport("adp: %08x\n", adp );
	DIReport("r: %08x\n", adp->r );
	DIReport("g: %08x\n", adp->g );
	DIReport("b: %08x\n", adp->b );
	DIReport("a: %08x\n", adp->a );
  */
}
/*
void
setColorAnime( gmrModel* model, gmrColorAnime* c_anime )
{
	model->c_anime = c_anime;
}
*/
