/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: J3DBinary 1.5 Renderer
  
  node管理
  -----------------------------------------------------------------------------*/
#include <dolphin.h>
#include "gBase.h"

#include "gMRnode.h"

/*-----------------------------------------------------------------------------
  Node管理情報をクリアします。
  -----------------------------------------------------------------------------*/
void
initNode(
	gmrNode*	ths)
{
	ths->parent = NULL;
	ths->child  = NULL;
	ths->prev   = NULL;
	ths->next   = NULL;
}

/*-----------------------------------------------------------------------------
  Nodeに子ノードを追加します。
  -----------------------------------------------------------------------------*/
void
addChildNode(
	gmrNode*	ths,
	gmrNode*	node)
{
	if ( ths == NULL ) {
		// ルートノード作成用
		return;
	}
	if ( ths->child == NULL ) {
		ths->child   = node;
		node->parent = ths;
		return;
	}
	else {
		addLastNode( ths->child , node );
	}
}

/*-----------------------------------------------------------------------------
  ノードの最後に兄弟ノードを追加します。
  -----------------------------------------------------------------------------*/
void
addLastNode(
	gmrNode*	ths,
	gmrNode*	node)
{
	if ( ths->next == NULL ) {
		ths->next  = node;
		node->prev = ths;
		return;
	}
	else {
		addLastNode( ths->next , node );
	}
}

/*-----------------------------------------------------------------------------
  あるノードを指定ノードの子供に挿入します。
  -----------------------------------------------------------------------------*/
void
insertNextNode(
	gmrNode*	ths,
	gmrNode*	node)
{
	node->child = ths->child;
	ths->child->parent = node;
	
	ths->child = node;
	node->parent = ths;
}


/*-----------------------------------------------------------------------------
  あるノードを指定ノードの親に挿入します。
  -----------------------------------------------------------------------------*/
void
insertPrevNode(
	gmrNode*	ths,
	gmrNode*	node)
{
	if ( ths->parent != NULL ) {
		ths->parent->child = node;
		node->parent = ths->parent;
		ths->parent = node;
		node->child = ths;
		return;
	}
	else if ( ths->prev != NULL ) {
		ths->prev->next = node;
		node->prev = ths->prev;
		ths->parent = node;
		node->next = ths->next;
		node->child = ths;
		ths->prev = NULL;
		ths->next = NULL;
	}
	else {
		OSHalt("ERROR: RootNodeの前には、ノードを追加できません。\n");
	}
}

/*-----------------------------------------------------------------------------
  あるノードを削除します
  -----------------------------------------------------------------------------*/
void
deleteNode(
	gmrNode* ths )
{
	if ( ths->parent != NULL ) {
		// 親ノードが直接つながっているとき
		if ( ths->child != NULL ) {
			// 削除ノードに子供があるとき。
			// とりあえず、子ノードと親ノードをつける。
			ths->parent->child = ths->child;
			ths->child->parent = ths->parent;

			if ( ths->next != NULL ) {
				// 削除ノードに子ノードがあり、弟ノードがあるとき
				// 元の子ノードの兄弟ノードの最後に元の弟ノードを追加する。
				addLastNode( ths->child , ths->next );
			}
		}
		else {
			// 削除ノードに子ノードがないとき。
			// 弟ノードと親ノードがつながる。
			ths->parent->child = ths->next;
			if ( ths->next != NULL ) {
				// 弟ノードがちゃんとあれば、双方向化
				ths->next->parent = ths->parent;
			}
		}
	}
	else if ( ths->prev != NULL ) {
		// 兄ノードにつながっているとき。
		if ( ths->child != NULL ) {
			// 削除ノードに子供があるとき、
			// 兄ノードと子供ノードをつける。
			ths->prev->next = ths->child;
			ths->child->prev = ths->prev;
			ths->child->parent = NULL; // 親はいなくなった。

			if ( ths->next != NULL ) {
				// 削除ノードに子ノードがあり、かつ弟ノードがあるとき、
				// 元の子ノードの兄弟ノードの最後に元の弟ノードを追加する。
				addLastNode( ths->child , ths->next );
			}
		}
		else {
			// 削除ノードに子ノードがないとき
			// 弟ノードと兄ノードがつながる。
			ths->prev->next = ths->next;
			if ( ths->next != NULL ) {
				// 弟ノードがちゃんとあれば、双方向化
				ths->next->prev = ths->prev;
			}
		}
	}
	else {
		OSHalt("ERROR: RootNodeを削除することはできません。\n");
	}

	// 消したノードは、接続情報をクリアします。
	initNode(ths);
}

/*-----------------------------------------------------------------------------
  親ノードを捜します。
  -----------------------------------------------------------------------------*/
gmrNode* searchParent( gmrNode* ths )
{
	gmrNode* ret;

	if ( ths->parent != NULL ) {
		ret = ths->parent;
	}
	else if ( ths->prev != NULL ) {
		ret = searchParent( ths->prev );
	}
	else {
//		DIReport("WARNING: RootNodeは親ノードを持っていません。\n");
	}

	return ret;
}

/*-----------------------------------------------------------------------------
  デバッグ用: dump tree
  -----------------------------------------------------------------------------*/
void
dumpTree( gmrNode* root )
{
	static int depth = 0;
	static gmrNode* base;
	int i;

	if ( depth == 0 ) {
		base = root;
		DIReport1("sizeof gmrNode %d\n", sizeof(gmrNode));
	}

	for ( i = 0 ; i < depth ; i++ ) {
		DIReport(" ");
	}

	switch ( root->type )
	{
	case cNodeJoint:
		DIReport2("%03d - JOINT:%d\n"   , ((u32)root-(u32)base)/sizeof(gmrNode), root->id );
//		DIReport("%08x:%08x - JOINT:%d\n"   , root, base , root->id );
		break;
	case cNodeMaterial:
		DIReport2("%03d - MATERIAL:%d\n", ((u32)root-(u32)base)/sizeof(gmrNode), root->id );
//		DIReport("%08x:%08x - MATER:%d\n"   , root, base , root->id );
		break;
	case cNodeShape:
		DIReport2("%03d - SHAPE:%d\n"   , ((u32)root-(u32)base)/sizeof(gmrNode), root->id );
//		DIReport("%08x:%08x - SHAPE:%d\n"   , root, base , root->id );
		break;
	}

	if ( root->child != NULL ) {
		depth++;
		dumpTree( root->child );
		depth--;
	}

	if ( root->next != NULL ) {
		dumpTree( root->next );
	}
}

