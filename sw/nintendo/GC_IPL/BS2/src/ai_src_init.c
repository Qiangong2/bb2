#include <dolphin/types.h>
#include <private/flipper.h>
#include <dolphin/os.h>
#include <dolphin/ai.h>

void AISrcInit(void);
static void __AISrcWorkaround(void);

#define BUFFER_NSEC          3000   // in nanoseconds
#define BOUND_32KHZ_NSEC    31524   // in nanoseconds
#define BOUND_48KHZ_NSEC    42024   // in nanoseconds
#define MIN_WAIT_NSEC       42000   // in nanoseconds
#define MAX_WAIT_NSEC       63000   // in nanoseconds

void AISrcInit(void)
{
  BOOL old;
  
  // stop stream
  AI_CR_SET_PSTAT(__AIRegs[AI_CR_IDX], AI_STREAM_STOP);
  
  // set AIIT register 0
  AI_IT_SET_AIIT(__AIRegs[AI_IT_IDX], 0);
  
  // set stream left volume 0
  AI_VR_SET_AVRL(__AIRegs[AI_VR_IDX], 0);
  // set stream right volume 0
  AI_VR_SET_AVRR(__AIRegs[AI_VR_IDX], 0);
  
  old = OSDisableInterrupts();
  
  __AISrcWorkaround();
  
  // dsp src is 32khz mode.
  __AIRegs[AI_CR_IDX] |= AI_CR_DSP32_MASK;
  
  // stream src is 48khz mode
  AI_CR_SET_AFR(__AIRegs[AI_CR_IDX], AI_SAMPLERATE_48KHZ);
  
  OSRestoreInterrupts(old);
  
  // reset AISCNT register
  AI_CR_SET_SCRST(__AIRegs[AI_CR_IDX], AI_SCRST_RESET);
  
}

static void __AISrcWorkaround(void)
{
  OSTime  bound_32KHz;
  OSTime  bound_48KHz;
  OSTime  min_wait;
  OSTime  max_wait;
  OSTime  buffer;

  OSTime  rising_32khz=0;
  OSTime  rising_48khz=0;
  OSTime  diff=0;
  OSTime  t1 = 0;
  OSTime  temp;
  u32     temp0, temp1;
  u32     done=0;
  u32     volume=0;
  u32     Init_Cnt=0;
  u32     walking=0;

  temp        = 0;

  bound_32KHz = OSNanosecondsToTicks(BOUND_32KHZ_NSEC);
  bound_48KHz = OSNanosecondsToTicks(BOUND_48KHZ_NSEC);
  min_wait    = OSNanosecondsToTicks(MIN_WAIT_NSEC);
  max_wait    = OSNanosecondsToTicks(MAX_WAIT_NSEC);
  buffer      = OSNanosecondsToTicks(BUFFER_NSEC);
  
  // This code is for RevB. But no problem will be for RevC(buffer is not necessary for RevC).
  while(done==0)
  {
    AI_CR_SET_SCRST(__AIRegs[AI_CR_IDX], AI_SCRST_RESET);
    
    AI_CR_SET_AFR(__AIRegs[AI_CR_IDX], AI_SAMPLERATE_32KHZ);
    AI_CR_SET_PSTAT(__AIRegs[AI_CR_IDX], AI_STREAM_START);

    temp0 = AI_SCNT_GET_AISCNT(__AIRegs[AI_SCNT_IDX]);
    while(temp0 == AI_SCNT_GET_AISCNT(__AIRegs[AI_SCNT_IDX]))
    {
      // do nothing         
    }

    // record 32khz rising edge
    rising_32khz = OSGetTime();
    
    // set stream src 48khz mode
    AI_CR_SET_AFR(__AIRegs[AI_CR_IDX], AI_SAMPLERATE_48KHZ);
    // AI_CR_SET_PSTAT(__AIRegs[AI_CR_IDX], AI_STREAM_START);

    temp1 = AI_SCNT_GET_AISCNT(__AIRegs[AI_SCNT_IDX]);
    while (temp1 == AI_SCNT_GET_AISCNT(__AIRegs[AI_SCNT_IDX]))
    {
      // do nothing
    }
     
    // record 48khz rising edge
    rising_48khz = OSGetTime();
    
    diff = rising_48khz - rising_32khz;
    
    // set stream src 32khz mode & stop stream
    AI_CR_SET_AFR(__AIRegs[AI_CR_IDX], AI_SAMPLERATE_32KHZ);
    AI_CR_SET_PSTAT(__AIRegs[AI_CR_IDX], AI_STREAM_STOP);
   
    // Is there a good timing for the src?
    if (diff < (bound_32KHz - buffer))
    {
      temp=min_wait;
      done=1;
      Init_Cnt++;
    } 
    else 
    {
      if ((diff >= (bound_32KHz+buffer)) && (diff < (bound_48KHz-buffer)))
      { 
        // 31524+10500
	      temp=max_wait;
	      done=1;
	      Init_Cnt++;
      } 
      else 
      {
	      done=0;      
	      walking=1;
	      Init_Cnt++;
      }

    } // end if...else...

  } // end while()
  
  // wait until appropriate timing 
  while ((rising_48khz + temp) > OSGetTime())
  {
    // do nothing
  }

}