/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: Menu OptionAnim関連ルーチン
  -----------------------------------------------------------------------------*/
#include <dolphin.h>

#include "gMath.h"
#include "gBase.h"
#include "gTrans.h"
#include "gCamera.h"
#include "gLoader.h"
#include "gCont.h"

#include "gModelRender.h"

#include "MenuOptionAnim.h"
#include "MenuUpdate.h"
#include "OptionState.h"

/*-----------------------------------------------------------------------------

  -----------------------------------------------------------------------------*/
extern DIPadStatus	DIPad;

/*-----------------------------------------------------------------------------
  statics
  -----------------------------------------------------------------------------*/
static gmrModel		ocm_w[30];	// _w は waku
static gmrModel		ocm_o[31];	// _o は onpu
static OCAnimState	oca_w[30];
static OCAnimState	oca_o[31];

static s16	now_frame;

static GXColorS10 cube_color;

static BOOL sConcent;

static s8 onpu[] =
{
	0,0,0,0,1,1,0,0,0,
	0,0,0,0,1,1,1,0,0,
	0,0,0,0,1,1,1,1,0,
	0,0,0,0,1,0,1,1,1,
	0,0,0,0,1,0,0,1,1,
	0,1,1,1,1,0,1,1,0,
	1,1,1,1,1,0,1,0,0,
	0,1,1,1,0,0,0,0,0,
};
/*-----------------------------------------------------------------------------
  CardCubeがアニメーションするために必要な設定
  -----------------------------------------------------------------------------*/
void
initOCAnim( gmrModelData* md , BOOL first)
{
	s16 i;

	if ( first ) {
		for ( i = 0 ; i < 30 ; i++ ) {
			ocm_w[i].gmrmd = md;
			buildModel( &ocm_w[i] , FALSE );
		}
		for ( i = 0 ; i < 31 ; i++ ) {
			ocm_o[i].gmrmd = md;
			buildModel( &ocm_o[i] , FALSE );
		}

	}

	for ( i = 0 ; i < 30 ; i++ ) {
		ocm_w[i].modelAlpha = 0;
	}
	for ( i = 0 ; i < 31 ; i++ ) {
		ocm_o[i].modelAlpha = 0;
	}
	now_frame = 0;
	sConcent = FALSE;
	initOCAnimState();
}

/*-----------------------------------------------------------------------------
  CardCubeがアニメーションするための初期値の設定
  -----------------------------------------------------------------------------*/
void
initOCAnimState( void )
{
	s16 i;
	s16	offset_x, offset_y;
	s16	point = 0;

	offset_x = -80;
	offset_y = 70;
	
	for ( i = 0 ; i < 30 ; i++ ) {
		if ( i >= 1 && i <= 6 ) {
			oca_w[i].x = offset_x + 20 * 8;
			oca_w[i].y = offset_y - 20 * i;
		}
		else if ( i >= 8 && i <= 14 ) {
			oca_w[i].x = offset_x + 20 * ( 15 - i );
			oca_w[i].y = offset_y - 20 * 7;
		}
		else if ( i >= 16 && i <= 21 ) {
			oca_w[i].x = offset_x;
			oca_w[i].y = offset_y - 20 * ( 22 - i );
		}
		else if ( i >= 23 && i <= 29 ) {
			oca_w[i].x = offset_x + 20 * ( i  - 22 );
			oca_w[i].y = offset_y;
		}
		else {
			// i=0,7,15,22
			oca_w[i].x = 0.f;
			oca_w[i].y = 0.f;
		}

		oca_w[i].rot_x = 0;
		oca_w[i].rot_y = 0;
		ocm_w[i].modelAlpha = 0;
	}
	for ( i = 0 ; i < 31 ; i++ ) {
		while ( onpu[ point++ ] == 0 && point < 72) {}
		oca_o[i].x = offset_x + 20 * ( (point-1) % 9 );
		oca_o[i].y = offset_y - 20 * ( (point-1) / 9 );
		ocm_o[i].modelAlpha = 0;
	}
}

static void
addModelAlpha( s16 *ap , s16 add )
{
	s16 alpha = *ap;
	
	alpha += add;
	if ( alpha > 255 ) alpha = 255;
	if ( alpha < 0   ) alpha = 0;
	
	*ap = alpha;
}

static void
updateOCAnimeDefault( void )
{
	s16 i;
	for ( i = 0 ; i < 30 ; i++ ) {
		oca_w[i].nx = oca_w[i].x;
		oca_w[i].ny = oca_w[i].y;
	}
	for ( i = 0 ; i < 31 ; i++ ) {
		oca_o[i].nx = oca_o[i].x;
		oca_o[i].ny = oca_o[i].y;
	}
}

static void
updateOCWakuCenterSub( s16 i )
{
	oca_w[i].x = 0.f;
	oca_w[i].y = 0.f;
	addModelAlpha( &ocm_w[i].modelAlpha, 255/40 );
}

static void
updateOCWakuCenter( void )
{
	updateOCWakuCenterSub( 0 );
	updateOCWakuCenterSub( 7 );
	updateOCWakuCenterSub( 15 );
	updateOCWakuCenterSub( 22 );

	updateOCAnimeDefault();
}

static void
updateOCWakuToEdge( s16 frame , s16 end_frame )
{
	s16	offset_x, offset_y, i;
	s16	ex,ey;
	u8	base_id[]   = { 0, 7, 22, 15 };

	offset_x = -80;
	offset_y = 70;

	for ( i = 0 ; i < 4 ; i ++ ) {
		ex = (s16)( offset_x + 20 * 8 * ( i % 2 ) );
		ey = (s16)( offset_y - 20 * 7 * ( i / 2 ) );
		oca_w[ base_id[i] ].x = gHermiteInterpolation( frame,
													   0,          0, ex / end_frame,
													   end_frame, ex, 0 );
		oca_w[ base_id[i] ].y = gHermiteInterpolation( frame,
													   0,          0, ey / end_frame,
													   end_frame, ey, 0 );
		addModelAlpha( &ocm_w[ base_id[i] ].modelAlpha, 255/40 );
	}

	updateOCAnimeDefault();
}

static void
updateOCWakuDraw( s16 frame , s16 end_frame )
{
	s16	offset_x, offset_y, i, mc;
	s16	sx, sy, ex, ey;
	u8	base_id[]   = { 22, 0, 15, 7 };
	u8	from[]      = {  2, 0,  3, 1 };

	offset_x = -80;
	offset_y = 70;

	for ( i = 0 ; i < 4 ; i ++ ) {
		sx = (s16)( offset_x + 20 * 8 * ( from[i] % 2 ) );
		sy = (s16)( offset_y - 20 * 7 * ( from[i] / 2 ) );
		ex = (s16)( offset_x + 20 * 8 * ( i % 2 ) );
		ey = (s16)( offset_y - 20 * 7 * ( i / 2 ) );
		oca_w[ base_id[i] ].x = gHermiteInterpolation( frame,
													   0,         sx, (ex - sx) / end_frame,
													   end_frame, ex, 0 );
		oca_w[ base_id[i] ].y = gHermiteInterpolation( frame,
													   0,         sy, (ey - sy) / end_frame,
													   end_frame, ey, 0 );
		addModelAlpha( &ocm_w[ base_id[i] ].modelAlpha, 255/40 );
		switch ( i )
		{
		case 0:	// left line;
			for ( mc = 7; mc > (s16)( ( offset_y - oca_w[ base_id[i] ].y ) / 20 ) ; mc-- ) {
				ocm_w[ 22 - mc ].modelAlpha = 255;
			}
			break;
		case 1:	// top line;
			for ( mc = 0; mc < (s16)( ( oca_w[ base_id[i] ].x - offset_x ) / 20 ) ; mc++ ) {
				ocm_w[ 23 + mc ].modelAlpha = 255;
			}
			break;
		case 2:	// bottom line;
			for ( mc = 8; mc > (s16)( ( oca_w[ base_id[i] ].x - offset_x ) / 20 ) ; mc-- ) {
				ocm_w[ 15 - mc ].modelAlpha = 255;
			}
			break;
		case 3:	// right line;
			for ( mc = 0; mc < (s16)( ( offset_y - oca_w[ base_id[i] ].y ) / 20 ) ; mc++ ) {
				ocm_w[ 1 + mc ].modelAlpha = 255;
			}
			break;
		}
	}
	updateOCAnimeDefault();
}

static void
updateOCWakuAnime( s16 frame )
{
	s16 i;
	for ( i = 0 ; i < 30 ; i++ ) {
		if ( frame >= i * 5 && frame <= i * 5 + 10 ) {
			if ( i < 7 ) {
				oca_w[i].rot_x = (s16)( 16384 * ( frame - i * 5 ) / 10 );
				oca_w[i].rot_y = 0;
			} else if ( i < 15 ) {
				oca_w[i].rot_x = 0;
				oca_w[i].rot_y = (s16)( -16384 * ( frame - i * 5 ) / 10 );
			} else if ( i < 22 ) {
				oca_w[i].rot_x = (s16)( -16384 * ( frame - i * 5 ) / 10 );
				oca_w[i].rot_y = 0;
			} else {
				oca_w[i].rot_x = 0;
				oca_w[i].rot_y = (s16)( 16384 * ( frame - i * 5 ) / 10 );
			}
		}
	}
	updateOCAnimeDefault();
}

static void
swapWakuOnpuAlpha( void )
{
	s16 i;
	for ( i = 0 ; i < 30 ; i++ ) {
		ocm_w[i].modelAlpha ^= 0xff;
	}
	for ( i = 0 ; i < 31 ; i++ ) {
		ocm_o[i].modelAlpha ^= 0xff;
	}
}

static void
updateOCWakuToOnpu( s16 frame , s16 f )
{
	s16 i;
	s16	now_line = (s16)gHermiteInterpolation( frame ,
											  0, 80, 0,
											  f,  0, 0 );

	for ( i = 0 ; i < 30 ; i++ ) {
		if ( oca_w[i].x > 0 && oca_w[i].x > now_line ) {
			oca_w[i].nx = now_line;
		} else if ( oca_w[i].x < 0 && oca_w[i].x < -now_line ) {
			oca_w[i].nx = -now_line;
		} else {
			oca_w[i].nx = oca_w[i].x;
		}
	}
}


static void
updateOCOnpuAppear( s16 frame , s16 f )
{
	s16 i;
	s16	now_line = (s16)gHermiteInterpolation( frame ,
											  0,  0, 0,
											  f, 80, 0 );

//	DIReport("%d\n", frame);
	if ( frame == 0 ) swapWakuOnpuAlpha();

	for ( i = 0 ; i < 31 ; i++ ) {
		if ( oca_o[i].x > 0 && oca_o[i].x > now_line ) {
			oca_o[i].nx = now_line;
		}
		else if ( oca_o[i].x < 0 && oca_o[i].x < -now_line ) {
			oca_o[i].nx = -now_line;
		}
		else {
			oca_o[i].nx = oca_o[i].x;
		}
	}
}

static void
updateOCOnpuWave( s16 frame )
{
	s16 i, offset_y, id_y;

	offset_y = 70;
	for ( i = 0 ; i < 31 ; i++ ) {
		id_y = 	(s16)( 7 - ( offset_y - oca_o[i].y ) / 20 );	//　top:7 bottom 0;
		if ( frame >= id_y * 4 && frame < id_y * 4 + 16 ) { // のぼり
			oca_o[i].ny = oca_o[i].y + gHermiteInterpolation( frame - id_y * 4,
															  0, 0, 0,
															  16, 10, 0 );
		} else if ( frame >= id_y * 4 + 16 && frame <= id_y * 4 + 32 ) { // くだり
			oca_o[i].ny = oca_o[i].y + gHermiteInterpolation( frame - id_y * 4 - 16,
															  0, 10, 0,
															  16, 0, 0 );
		}
		else if ( frame >= ( id_y + 8 ) * 4      && frame < ( id_y + 8 ) * 4 + 16 ) { //2回目のぼり
			oca_o[i].ny = oca_o[i].y + gHermiteInterpolation( frame - ( id_y + 8 ) * 4,
															  0,   0, 0,
															  16, 10, 0 );
		}
		else if ( frame >= ( id_y + 8 ) * 4 + 16 && frame < ( id_y + 8 ) * 4 + 32 ) { //2回目くだり。
			oca_o[i].ny = oca_o[i].y + gHermiteInterpolation( frame - ( id_y + 8 ) * 4 - 16,
															  0,  10, 0,
															  16,  0, 0 );
		}
		else {
			oca_o[i].ny = oca_o[i].y;
		}
	}
}

static void
updateOCOnpuToWaku( s16 frame , s16 f )
{
	s16 i;
	s16	now_line = (s16)gHermiteInterpolation( frame ,
											  0, 70, 0,
											  f,  0, 0 );

	for ( i = 0 ; i < 31 ; i++ ) {
		if ( oca_o[i].y > 0 && oca_o[i].y > now_line ) {
			oca_o[i].ny = now_line;
		} else if ( oca_o[i].y < 0 && oca_o[i].y < -now_line ) {
			oca_o[i].ny = -now_line;
		} else {
			oca_o[i].ny = oca_o[i].y;
		}
		oca_o[i].nx = oca_o[i].x;
	}
}

static void
updateOCWakuAppear( s16 frame , s16 f )
{
	s16 i;
	s16	now_line = (s16)gHermiteInterpolation( frame ,
											  0,  0, 0,
											  f, 80, 0 );

//	DIReport("%d\n", frame);
	if ( frame == 0 ) swapWakuOnpuAlpha();

	for ( i = 0 ; i < 30 ; i++ ) {
		if ( oca_w[i].y > 0 && oca_w[i].y > now_line ) {
			oca_w[i].ny = now_line;
		}
		else if ( oca_w[i].y < 0 && oca_w[i].y < -now_line ) {
			oca_w[i].ny = -now_line;
		}
		else {
			oca_w[i].ny = oca_w[i].y;
		}
		oca_w[i].nx = oca_w[i].x;
	}
}

void
updateOCAnime( void )
{
	OptionState*	osp = getOptionState();

	if ( now_frame == 0 ) initOCAnimState();

	if ( now_frame < 20 ) updateOCWakuCenter();
	else if ( now_frame < 40 ) updateOCWakuToEdge( (s16)( now_frame - 20 ), 20 );
	else if ( now_frame < 70 ) updateOCWakuDraw( (s16)( now_frame - 40 ), 30 );
	
	else if ( now_frame < 70 + 155 ) updateOCWakuAnime( (s16)( now_frame - 70 ) );
	else if ( now_frame < 70 + 155 + 40 ) updateOCWakuToOnpu( (s16)( now_frame -70 -155 ) , 40 );
	else if ( now_frame < 70 + 155 + 40 + 40 ) updateOCOnpuAppear( (s16)( now_frame - 70 -155 -40 ), 40 );
	else if ( now_frame < 70 + 155 + 40 + 40 + 92 ) updateOCOnpuWave( (s16)( now_frame- 70 -155 -40 -40 ) );
	else if ( now_frame < 70 + 155 + 40 + 40 + 92 + 122) updateOCOnpuWave( (s16)( now_frame- 70 -155 -40 -40 -92 ) );
	else if ( now_frame < 70 + 155 + 40 + 40 + 92 + 122 + 40 ) updateOCOnpuToWaku( (s16)( now_frame - (70 + 155 + 40 + 40 + 92 + 122) ) , 40 );
	else if ( now_frame < 70 + 155 + 40 + 40 + 92 + 122 + 40 + 40 ) updateOCWakuAppear( (s16)( now_frame - (70 + 155 + 40 + 40 + 92 + 122 + 40) ) , 40 );

	now_frame++;
	if ( now_frame > 70 + 155 + 40 + 40 + 92 + 122 + 40 + 40 ) now_frame = 70;
}

void
updateOCAnimeMtx( void )
{
	s16	i;
	Mtx currentMtx, r, trans, view;

	gGetTranslateRotateMtx(	0, -16384,0,0,0,0,r );
	MTXConcat(  getMainMenuMtx() ,r, view );
	
	for (i = 0 ; i < 30 ; i++ )	{
		gGetTranslateRotateMtx(	oca_w[i].rot_x,
								oca_w[i].rot_y,
								0 ,
								oca_w[i].nx,
								oca_w[i].ny,
								0,
								trans );
		MTXConcat( view, trans, currentMtx);
		setBaseMtx( &ocm_w[i] , currentMtx , (Vec){.67f,.67f,.67f } );
		setViewMtx( &ocm_w[i] , getCurrentCamera() );
		updateModel( &ocm_w[i] );
	}

	for (i = 0 ; i < 31 ; i++ )	{
		MTXTrans( trans , 
				  oca_o[i].nx,
				  oca_o[i].ny,
				  0 );
		MTXConcat( view, trans, currentMtx);
		setBaseMtx( &ocm_o[i] , currentMtx , (Vec){.67f,.67f,.67f } );
		setViewMtx( &ocm_o[i] , getCurrentCamera() );
		updateModel( &ocm_o[i] );
	}
}

void
drawOCAnime( void )
{
	s16	i;
	GXColorS10*	backup;

	OptionState *osp = getOptionState();

	cube_color.r = (s16)osp->cube_r;
	cube_color.g = (s16)osp->cube_g;
	cube_color.b = (s16)osp->cube_b;
	cube_color.a = (s16)osp->cube_a;

	backup = ocm_w[0].gmrmd->material[0].tevColorPtr[0];
	ocm_w[0].gmrmd->material[0].tevColorPtr[0] = &cube_color;
	// draw

	for (i = 0 ; i < 30 ; i++ )	{
		drawModel( &ocm_w[i] );
	}
	for (i = 0 ; i < 31 ; i++ )	{
		drawModel( &ocm_o[i] );
	}

	ocm_w[0].gmrmd->material[0].tevColorPtr[0] = backup;
}

void
clearOCAnime( void )
{
	int i;

	
	for ( i = 0 ; i < 30 ; i++ ) {
		addModelAlpha( &ocm_w[i].modelAlpha, -10 );
	}
	for ( i = 0 ; i < 31 ; i++ ) {
		addModelAlpha( &ocm_o[i].modelAlpha, -10 );
	}
	now_frame = 0;
}


static BOOL
decPosition( f32 *xp )
{
	f32	x = *xp;
	OptionState*	osp = getOptionState();

	if ( x > 0 ) {
		x -= osp->concent_dec_pos;
		if ( x < 0 ) x = 0;
	} else if ( x < 0 ) {
		x += osp->concent_dec_pos;
		if ( x > 0 ) x = 0;
	}

	*xp = x;
	if ( x == 0 ) return TRUE;
	return FALSE;
}

void
concentOCAnime( void )
{
	int i;
	int all_pos = 0;
	OptionState*	osp = getOptionState();

	for ( i = 0 ; i < 30 ; i++ ) {
		addModelAlpha( &ocm_w[i].modelAlpha, (s16)( -1 * osp->concent_dec_alpha ) );

		if ( decPosition( &oca_w[i].nx ) ) all_pos++;
		if ( decPosition( &oca_w[i].ny ) ) all_pos++;
	}

	for ( i = 0 ; i < 31 ; i++ ) {
		addModelAlpha( &ocm_o[i].modelAlpha, (s16)( -1 * osp->concent_dec_alpha ) );

		if ( decPosition( &oca_o[i].nx ) ) all_pos++;
		if ( decPosition( &oca_o[i].ny ) ) all_pos++;
	}

	if ( all_pos == 61 * 2 ) {
		sConcent = TRUE;
	} else {
		sConcent = FALSE;
	}

	now_frame = 0;
}

BOOL
isConcentOCSmallCube( void )
{
	return sConcent;
}
