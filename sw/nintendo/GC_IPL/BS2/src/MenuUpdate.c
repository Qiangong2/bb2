/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: Splashデモ Menu関連ルーチン
  -----------------------------------------------------------------------------*/
#include <dolphin.h>
#include <math.h>
#include <string.h>
#include <private/OSRtc.h>

#include "gCont.h"
#include "gCamera.h"
#include "gDynamic.h"
#include "gMath.h"
#include "gTrans.h"
#include "gLoader.h"
#include "gInit.h"

#include "gLayoutRender.h"
#include "gModelRender.h"

#include "gMainState.h"
#include "Splash.h"
#include "SplashMain.h"
#include "SplashDraw.h"
#include "SplashUpdate.h"
#include "MenuUpdate.h"
#include "MenuCardAnim.h"
#include "MenuStartAnim.h"
#include "MenuOptionAnim.h"
#include "MenuClockAnim.h"
#include "LogoUpdate.h"
#include "CardUpdate.h"
#include "CardSequence.h"
#include "CardIconMove.h"
#include "StartUpdate.h"
#include "OptionUpdate.h"
#include "TimeUpdate.h"

#ifdef JHOSTIO
#include "IHIMenuState.h"
#endif

#include "jaudio.h"

#if defined(MPAL)
#include "mpal_mesg.h"
#elif defined(PAL)
#include "pal_mesg.h"
#else // NTSC
#include "ntsc_mesg.h"
#endif
//#include "2d_operation_0_Japanese.h"
//#include "2d_demo_2_Japanese.h"

// start nakamura's edit
#include "smallCubeFader.h"
#include "smallCubeDraw.h"
// end

/*-----------------------------------------------------------------------------
  コントローラの入力を外部参照する。
  -----------------------------------------------------------------------------*/
extern DIPadStatus	DIPad;

/*-----------------------------------------------------------------------------
  staticモノ
  -----------------------------------------------------------------------------*/
static MenuState* mesp;

#ifndef JHOSTIO
static MenuState mes;
#endif

static s32 menu_runstate;
static s32 pre_menu_runstate;

static Mtx currentMtx;
static gmrModelData dummyMD;

static void*	lay_mai;
static void*	lay_gam;
static void*	lay_mem;
static void*	lay_opt;
static void*	lay_cal;
static void*	tex_ebt;

static void*	lay_ope;
static void*	str_ope[ COUNTRY_PAL_MAX_LANGUAGE ];

static void*	dem_lay;
static void*	dem_str;

static s16		mem_win_alpha[2];
static int		max_capa[2];

static ResTIMG*	num_timg[ 10 ];
static ResTIMG*	week_timg[ 7 ];
static ResTIMG* sound_timg[ 2 ];

static gLRFade	main[6];
static gLRFade	exmain[3];
static gLRFade	sentaku;
static gLRFade	tyousei;
static gLRFade	cancel;
static gLRFade	exit_str;
static gLRFade	kettei;

static gLRFade	fs_option[3];
static gLRFade	fs_memory[3];
static gLRFade	fs_time[3];
static gLRFade	fs_start[3];

static gLRFade	banner;
static gLRFade	nodisk, question;

static gLRFade	fatal_error;

static gLRFade	operation_language_string[ COUNTRY_PAL_MAX_LANGUAGE ];

static s16	fadein_alpha = 0;
static u16	anime_frame = 0;

static s16	operation_language;

/*-----------------------------------------------------------------------------
  Prototype
  -----------------------------------------------------------------------------*/
static u32	updateMenuControl( void );
static void	inMenuUpdateInsideAnime( void );

void
initMenuDraw_Lang( void )
{
	int i;

	lay_gam = gGetBufferPtr( getID_3D_gamestart_layout() );
	lay_mai = gGetBufferPtr( getID_3D_main_layout() 	 );
	lay_mem = gGetBufferPtr( getID_3D_memory_layout() 	 );
	lay_cal = gGetBufferPtr( getID_3D_calendar_layout()  );
	lay_opt = gGetBufferPtr( getID_3D_option_layout() 	 );

	dem_str = gGetBufferPtr( getID_2D_demo_string() 	 );

	for ( i = 0 ; i < 7 ; i++ ) {
		week_timg[i] = gGetBufferPtr( getID_calendar_week_tex_base() + i );	// 曜日。
	}
	sound_timg[0] = gGetBufferPtr( getID_option_sound_mono_tex() );
	sound_timg[1] = gGetBufferPtr( getID_option_sound_stereo_tex() );
}

static void
initMenuDraw( BOOL first )
{
	int i;

	if ( first ) {
		lay_ope = gGetBufferPtr( getID_2D_operation_layout() );
		tex_ebt = gGetBufferPtr( getID_texture_archive() );
		dem_lay = gGetBufferPtr( getID_2D_demo_layout() );
#ifdef PAL
		for ( i = 0 ; i < COUNTRY_PAL_MAX_LANGUAGE ; i++ ) {
			str_ope[i] = gGetBufferPtr( getID_2D_operation_string_pal( i ) );
		}
#else
			str_ope[0] = gGetBufferPtr( getID_2D_operation_string() );
#endif

		initMenuDraw_Lang();

		glrBuildTexImage( tex_ebt );

		// 3dレイアウト表示に使用するテクスチャイメージの取得。
		for ( i = 0 ; i < 10 ; i++ ) {
			num_timg[i] = gGetBufferPtr( getID_calendar_num_tex_base() + i );	// 数字。
		}
	}

	mem_win_alpha[0] = 0;
	mem_win_alpha[1] = 0;
	max_capa[0] = 0;
	max_capa[1] = 0;

	fadein_alpha = 0;
	anime_frame = 0;

	operation_language = gGetPalCountry();	// NTSC/MPALなら常に0。

	glrInitFadeState( &main[0] );	main[0].tag = 0;	// スティック
	glrInitFadeState( &main[1] );	main[1].tag = 0;	// bボタンフェーダ。
	glrInitFadeState( &main[2] );	main[2].tag = 0;	// aボタンフェーダ。
	glrInitFadeState( &main[3] );	main[3].tag = 0;	// 中心スティック
	glrInitFadeState( &main[4] );	main[4].tag = 0;	// 中心bボタンフェーダ。
	glrInitFadeState( &main[5] );	main[5].tag = 0;	// 中心aボタンフェーダ。
	glrInitFadeState( &exmain[0] );	exmain[0].tag = MSG_OP000;	// menucube時
	glrInitFadeState( &exmain[1] );	exmain[1].tag = MSG_OP001;	// menucube時
	glrInitFadeState( &exmain[2] );	exmain[2].tag = MSG_OP002;	// menucube時

	glrInitFadeState( &kettei	);	kettei.tag  = MSG_OP006;	// 決定。
	glrInitFadeState( &sentaku	);	sentaku.tag = MSG_OP004;	// 選択。
	glrInitFadeState( &tyousei	);	tyousei.tag = MSG_OP013;	// 調整。
	glrInitFadeState( &exit_str	);	exit_str.tag= MSG_OP011;	// 終了。
	glrInitFadeState( &cancel	);	cancel.tag	= MSG_OP008;	// 戻る。

	for ( i = 0 ; i < 3 ; i++ ) {
		glrInitFadeState( &fs_option[i] );
		glrInitFadeState( &fs_memory[i] );
		glrInitFadeState( &fs_time[i]   );
		glrInitFadeState( &fs_start[i]  );

		fs_option[i].wait_frame = mesp->disp_wait_frame[i];
		fs_memory[i].wait_frame = mesp->disp_wait_frame[i];
		fs_time[i].wait_frame = mesp->disp_wait_frame[i];
		fs_start[i].wait_frame = mesp->disp_wait_frame[i];
	}

	glrInitFadeState( &banner );
	glrInitFadeState( &nodisk );
	glrInitFadeState( &question );

	glrInitFadeState( &fatal_error );	fatal_error.tag = MSG_S200;

	for ( i = 0 ; i < COUNTRY_PAL_MAX_LANGUAGE ; i++ ) {
		glrInitFadeState( &operation_language_string[ i ] );
	}
}


/*-----------------------------------------------------------------------------
  初期化
  -----------------------------------------------------------------------------*/
void
MenuInit( BOOL first )
{
	if ( first ) {
#ifndef JHOSTIO
		mesp = &mes;
		inMenuStateInit( mesp );
#else
		mesp = getIHIMenuState();
#endif
	}

	inMenuStateReset( mesp );
	menu_runstate = IPL_MENU_CENTER;
	pre_menu_runstate = menu_runstate;
	initMenuDraw( first );

	if ( first ) {
		// dummy modelData & modelを使って、モデルデータのbuildをする。
		dummyMD.modeldata = gGetBufferPtr( GC_scm2_szp );
		buildModelData( &dummyMD );

		// start nakamura's edit
		initSmallCubeDraw();
		initSmallCubeFader(gGetBufferPtr( getID_smallcubefaderdata() ),
				   gGetBufferPtr(GC_cubeanim_szp) );
		// end
	}

	initCCAnim( &dummyMD ,first );
	initSCAnim( &dummyMD ,first );
	initOCAnim( &dummyMD ,first );
	initOLAnim( &dummyMD ,first );
}


static void setMpalSmallCubeParam( MenuState* p );
static void
setMpalSmallCubeParam( MenuState* p )
{
	p->scf_optionsound_xoffset = (f32)10;
	p->scf_optionsound_yoffset = (f32)-22;
	p->scf_optionsound_scale = (f32)0.6;
	p->scf_optionoffset_scale = (f32)0.7;
	p->scf_soundarrowl_x = (f32)-18;
	p->scf_soundarrowl_y = (f32)9;
	p->scf_soundarrowr_x = (f32)-47;
	p->scf_soundarrowr_y = (f32)9;
	p->scf_start_scale = (f32)0.66;
	p->scf_timedate_scale = (f32)0.74;
	p->scf_timeclock_scale = (f32)0.74;
}

static void setPalSmallCubeParam( MenuState* p );
static void
setPalSmallCubeParam( MenuState* p )
{
	p->scf_optionsound_yoffset = (f32)-84;
	p->scf_optionsound_scale = (f32)0.55;
	p->scf_optionoffset_yoffset = (f32)-35;
	p->scf_optionoffset_scale = (f32)0.64;
	p->scf_optionlang_yoffset = (f32)10.f;

	p->scf_soundarrowl_x = -10;
	p->scf_soundarrowl_y = 31;
	p->scf_soundarrowr_x = -30;
	p->scf_soundarrowr_y = 31;

	p->scf_offsetarrowl_x = (f32)-15;
	p->scf_offsetarrowl_y = (f32)13;
	p->scf_offsetarrowr_x = (f32)15;
	p->scf_offsetarrowr_y = (f32)13;
	
	p->scf_start_scale = (f32)0.66;
	p->scf_timedate_scale = (f32)0.74;
	p->scf_timeclock_scale = (f32)0.74;

	/*
	p->scf_cubecolor_start_r1 = 130;
	p->scf_cubecolor_start_g1 = 50;
	p->scf_cubecolor_start_b1 = 160;
	p->scf_cubecolor_start_r2 = 50;
	p->scf_cubecolor_start_g2 = 20;
	p->scf_cubecolor_start_b2 = 70;
	  */
}



/*-----------------------------------------------------------------------------
  パラメータの初期化
  -----------------------------------------------------------------------------*/
void
inMenuStateInit( MenuState* p )
{
	p->menu_rot_speed = 750;
	p->fadeout_z_max = -70.f;
	p->fadein_z_speed = 1840;
	p->fadeout_z_speed = 1400;
	p->fadeout_z_maxalpha = 0;
	p->fadeout_z_rotnum = 0.f;

	p->y_sort_mergin = 0;
	p->y_sort_offset = 5;

	p->menu_swing_speed_x	= 50;
	p->menu_swing_speed_y	= 25;
	p->menu_swing_speed_z	= 25;

	p->menu_swing_amp_x	= 350;
	p->menu_swing_amp_y	= 1000;
	p->menu_swing_amp_z	= 1000;
	p->menu_slide_speed_x	= 25;

	p->menu_slide_speed_y	= 50;
	p->menu_slide_amp_x	= 15;
	p->menu_slide_amp_y	= 6;
	p->menu_swing_offset_y = -15000;
	p->menu_slide_offset_x = -16384;

	p->enable_pre_key_input = 0;

	// オプション面
	p->scf_optionsound_xoffset = (f32)10;
	p->scf_optionsound_yoffset = (f32)-11;
	p->scf_optionsound_scale = (f32)0.82;
	p->scf_optionsound_zoom = (f32)0.2;

	p->scf_optionoffset_xoffset = (f32)0;
	p->scf_optionoffset_yoffset = (f32)13;
	p->scf_optionoffset_scale = (f32)0.82;
	p->scf_optionoffset_zoom = (f32)0.2;

	p->scf_optionlang_xoffset = (f32)0;
	p->scf_optionlang_yoffset = (f32)19;
	p->scf_optionlang_scale = (f32)0.66;
	p->scf_optionlang_zoom = (f32)0.2;

	// 言語選択微調整
	p->scf_optionlang1 = -2;
	p->scf_optionlang2 = -2;
	p->scf_optionlang3 = -2;
	p->scf_optionlang4 = -2;
	p->scf_optionlang5 = -1;
	p->scf_optionlang6 = -1;

	// 小さい矢印
	p->scf_soundarrowl_x = (f32)-20;
	p->scf_soundarrowl_y = (f32)1;
	p->scf_soundarrowr_x = (f32)0;
	p->scf_soundarrowr_y = (f32)1;
	
	p->scf_offsetarrowl_x = (f32)-15;
	p->scf_offsetarrowl_y = (f32)-3;
	p->scf_offsetarrowr_x = (f32)15;
	p->scf_offsetarrowr_y = (f32)-3;

	p->scf_langarrowl_x = (f32)-50;
	p->scf_langarrowl_y = (f32)13;
	p->scf_langarrowr_x = (f32)50;
	p->scf_langarrowr_y = (f32)13;

	p->scf_offsetfadeinframe = (s16)30;
	p->scf_offsetfadeoutframe = (s16)10;

	p->scf_arrowanimframe = (s16)25;
	p->scf_arrowx = (f32)10;

	// カレンダー面
	p->scf_timedate_xoffset = (f32)0;
	p->scf_timedate_yoffset = (f32)-46;
	p->scf_timedate_scale = (f32)0.82;
	p->scf_timedate_zoom = (f32)0.2;
	p->scf_timeclock_xoffset = (f32)0;
	p->scf_timeclock_yoffset = (f32)-20;
	p->scf_timeclock_scale = (f32)0.82;
	p->scf_timeclock_zoom = (f32)0.2;

	// スタート面
	p->scf_start_xoffset = (f32)-8;
	p->scf_start_yoffset = (f32)-8;
	p->scf_start_scale = (f32)0.8;

	p->scf_alphafadeframenum = (s16)10;
	p->scf_movefadeframenum = (s16)10;
	p->scf_cubescale = (f32)1.0;
	p->scf_darkalpha = (s16)100;
	p->scf_dance_tx = (f32)2;
	p->scf_dance_ty = (f32)5;
	p->scf_dance_ry = (f32)30;
	p->scf_dance_rz = (f32)3;
	p->scf_dance_txturnpoint = (f32)0.45;
	p->scf_dance_framenum = (s16)120;
	p->scf_numeffect_width = (f32)0.4;
	p->scf_numeffect_tenmag = (f32)2;
	p->scf_numeffectframe = (s16)10;
	p->scf_mainanimframenum = (s16)58;
	p->scf_startdancetime = (f32)0.9;
	p->scf_modelalpha = (s16)200;
	p->scf_cubealpha = (s16)200;
	p->scf_tx = (f32)200;
	p->scf_ty = (f32)200;
	p->scf_tz = (f32)200;
	p->scf_handframe = (s16)-1;

	/*
	p->scf_cubecolor_start_r1 = 255;
	p->scf_cubecolor_start_g1 = 130;
	p->scf_cubecolor_start_b1 = 150;
	p->scf_cubecolor_start_r2 = 120;
	p->scf_cubecolor_start_g2 = 10;
	p->scf_cubecolor_start_b2 = 60;
	*/
	p->scf_cubecolor_start_r1 = 130;
	p->scf_cubecolor_start_g1 = 50;
	p->scf_cubecolor_start_b1 = 160;
	p->scf_cubecolor_start_r2 = 50;
	p->scf_cubecolor_start_g2 = 20;
	p->scf_cubecolor_start_b2 = 70;
	
	p->scf_cubecolor_option_r1 = 180;
	p->scf_cubecolor_option_g1 = 255;
	p->scf_cubecolor_option_b1 = 0;
	p->scf_cubecolor_option_r2 = 20;
	p->scf_cubecolor_option_g2 = 100;
	p->scf_cubecolor_option_b2 = 50;

	p->scf_darkcubecolor_option_r1 = 120;
	p->scf_darkcubecolor_option_g1 = 220;
	p->scf_darkcubecolor_option_b1 = 35;
	p->scf_darkcubecolor_option_r2 = 0;
	p->scf_darkcubecolor_option_g2 = 80;
	p->scf_darkcubecolor_option_b2 = 25;

	p->scf_darkercubecolor_option_r1 = 40;
	p->scf_darkercubecolor_option_g1 = 120;
	p->scf_darkercubecolor_option_b1 = 0;
	p->scf_darkercubecolor_option_r2 = 0;
	p->scf_darkercubecolor_option_g2 = 40;
	p->scf_darkercubecolor_option_b2 = 10;

	p->scf_cubecolor_calendar_r1 = 240;
	p->scf_cubecolor_calendar_g1 = 255;
	p->scf_cubecolor_calendar_b1 = 0;
	p->scf_cubecolor_calendar_r2 = 100;
	p->scf_cubecolor_calendar_g2 = 50;
	p->scf_cubecolor_calendar_b2 = 20;

	p->scf_darkcubecolor_calendar_r1 = 235;
	p->scf_darkcubecolor_calendar_g1 = 185;
	p->scf_darkcubecolor_calendar_b1 = 0;
	p->scf_darkcubecolor_calendar_r2 = 70;
	p->scf_darkcubecolor_calendar_g2 = 40;
	p->scf_darkcubecolor_calendar_b2 = 0;

	p->scf_darkercubecolor_calendar_r1 = 160;
	p->scf_darkercubecolor_calendar_g1 = 90;
	p->scf_darkercubecolor_calendar_b1 = 0;
	p->scf_darkercubecolor_calendar_r2 = 50;
	p->scf_darkercubecolor_calendar_g2 = 40;
	p->scf_darkercubecolor_calendar_b2 = 0;


	p->scf_dance_selected = 1;

	p->grid_light_da_dist = 500.f;
	p->grid_light_da_bright = .375f;
	p->grid_light_pos_x = 296.f;
	p->grid_light_pos_y = 224.f;
	p->grid_light_pos_z = 474.f;
	p->grid_light_spot  = 36.f;

	p->color_button_a_r = 0;
	p->color_button_a_g = 200;
	p->color_button_a_b = 140;
	p->color_button_b_r = 210;
	p->color_button_b_g = 0;
	p->color_button_b_b = 70;

	p->disp_wait_frame[0] = 0;
	p->disp_wait_frame[1] = 20;
	p->disp_wait_frame[2] = 70;

	// MPAL版用に、設定を上書き
	if( gGetNowCountry() == COUNTRY_BRAZIL ) setMpalSmallCubeParam( p );
#ifdef PAL // PAL版用に、設定を上書き
	setPalSmallCubeParam( p );
#endif
	
}

void
inMenuStateReset( MenuState* p )
{
	p->bound_frame = 0;
	p->menu_rot_x = 0;
	p->menu_rot_y = 0;

	p->fadeout_z_param = 0;
}

/*-----------------------------------------------------------------------------
  内部処理用
  -----------------------------------------------------------------------------*/
static u32
updateMenuControl( void )
{
	// [暫定]ロゴキューブに戻る。

#ifndef IPL
	if ( DIPad.pst.button & PAD_BUTTON_MENU )
	{
		menu_runstate = IPL_MENU_CENTER;
		pre_menu_runstate = IPL_MENU_CENTER;
		mesp->menu_rot_x = 0;
		mesp->menu_rot_y = 0;
		SplashInit( FALSE );
		Jac_StopSoundAll();
		return TRUE;
	}
#endif

	if ( DIPad.dirsPressed & DI_STICK_UP ) {	// 上
//		Jac_PlaySe( 1 );
		switch ( menu_runstate ) {
		case IPL_MENU_CENTER:
			if ( mesp->menu_rot_x == 0 && mesp->menu_rot_y == 0 ||
				 mesp->enable_pre_key_input ) {
				Jac_PlaySe( JAC_SE_MAIN_TO_SUB );
				pre_menu_runstate = IPL_MENU_GAMESTART;
			}
			break;
		case IPL_MENU_MEMORYCARD:
			if ( mesp->menu_rot_x == -16384 && mesp->fadeout_z_param == 0  ||
				 mesp->enable_pre_key_input ) {
				Jac_PlaySe( JAC_SE_SUB_TO_MAIN );
				pre_menu_runstate = IPL_MENU_CENTER;
			}
			break;
		}
	}
	if ( DIPad.dirsPressed & DI_STICK_DOWN ) {// 下
		switch ( menu_runstate ) {
		case IPL_MENU_CENTER:
			if ( mesp->menu_rot_x == 0 && mesp->menu_rot_y == 0 ||
				 mesp->enable_pre_key_input ) {
				Jac_PlaySe( JAC_SE_MAIN_TO_SUB );
				pre_menu_runstate = IPL_MENU_MEMORYCARD;
			}
			break;
		case IPL_MENU_GAMESTART:
			if ( mesp->menu_rot_x == 16384 && mesp->fadeout_z_param == 0  ||
				 mesp->enable_pre_key_input ) {
				Jac_PlaySe( JAC_SE_SUB_TO_MAIN );
				pre_menu_runstate = IPL_MENU_CENTER;
			}
			break;
		}
	}
	if ( DIPad.dirsPressed & DI_STICK_LEFT ) {// 左
		switch ( menu_runstate ) {
		case IPL_MENU_CENTER:
			if ( mesp->menu_rot_x == 0 && mesp->menu_rot_y == 0 ||
				 mesp->enable_pre_key_input ) {
				Jac_PlaySe( JAC_SE_MAIN_TO_SUB );
				pre_menu_runstate = IPL_MENU_OPTION;
			}
			break;
		case IPL_MENU_CLOCK:
			if ( mesp->menu_rot_y == -16384 && mesp->fadeout_z_param == 0 ||
				 mesp->enable_pre_key_input ) {
				Jac_PlaySe( JAC_SE_SUB_TO_MAIN );
				pre_menu_runstate = IPL_MENU_CENTER;
			}
			break;
		}
	}
	if ( DIPad.dirsPressed & DI_STICK_RIGHT ) {// 右
		switch ( menu_runstate ) {
		case IPL_MENU_CENTER:
			if ( mesp->menu_rot_x == 0 && mesp->menu_rot_y == 0 ||
				 mesp->enable_pre_key_input ) {
				Jac_PlaySe( JAC_SE_MAIN_TO_SUB );
				pre_menu_runstate = IPL_MENU_CLOCK;
			}
			break;
		case IPL_MENU_OPTION:
			if ( mesp->menu_rot_y == 16384 && mesp->fadeout_z_param == 0 ||
				 mesp->enable_pre_key_input ) {
				Jac_PlaySe( JAC_SE_SUB_TO_MAIN );
				pre_menu_runstate = IPL_MENU_CENTER;
			}
			break;
		}
	}

	if ( DIPad.buttonDown & PAD_BUTTON_A ) {
		switch ( menu_runstate ) {
		case IPL_MENU_MEMORYCARD:
			if ( mesp->menu_rot_x == -16384 && mesp->fadeout_z_param == 0 ||
				 mesp->enable_pre_key_input ) {
				Jac_PlaySe( JAC_SE_TO_2D );
				pre_menu_runstate = IPL_MENU_MEMORYCARD_SELECT;
			}
			break;
		case IPL_MENU_GAMESTART:
			if ( mesp->menu_rot_x == 16384 && mesp->fadeout_z_param == 0 ||
				 mesp->enable_pre_key_input ) {
				Jac_PlaySe( JAC_SE_TO_2D );
				pre_menu_runstate = IPL_MENU_GAMESTART_SELECT;
			}
			break;
		case IPL_MENU_CLOCK:
			if ( mesp->menu_rot_y == -16384 && mesp->fadeout_z_param == 0 ||
				 mesp->enable_pre_key_input ) {
				Jac_PlaySe( JAC_SE_TO_2D );
				pre_menu_runstate = IPL_MENU_CLOCK_SELECT;
			}
			break;
		case IPL_MENU_OPTION:
			if ( mesp->menu_rot_y == 16384 && mesp->fadeout_z_param == 0 ||
				 mesp->enable_pre_key_input ) {
				Jac_PlaySe( JAC_SE_TO_2D );
				pre_menu_runstate = IPL_MENU_OPTION_SELECT;
			}
			break;
		}
	}
	if ( DIPad.buttonDown & PAD_BUTTON_B ) {
		switch ( menu_runstate ) {
		case IPL_MENU_MEMORYCARD:
			if ( mesp->menu_rot_x == -16384 && mesp->fadeout_z_param == 0 ||
				 mesp->enable_pre_key_input ) {
				Jac_PlaySe( JAC_SE_SUB_TO_MAIN );
				pre_menu_runstate = IPL_MENU_CENTER;
			}
			break;
		case IPL_MENU_GAMESTART:
			if ( mesp->menu_rot_x == 16384 && mesp->fadeout_z_param == 0 ||
				 mesp->enable_pre_key_input ) {
				Jac_PlaySe( JAC_SE_SUB_TO_MAIN );
				pre_menu_runstate = IPL_MENU_CENTER;
			}
			break;
		case IPL_MENU_CLOCK:
			if ( mesp->menu_rot_y == -16384 && mesp->fadeout_z_param == 0 ||
				 mesp->enable_pre_key_input ) {
				Jac_PlaySe( JAC_SE_SUB_TO_MAIN );
				pre_menu_runstate = IPL_MENU_CENTER;
			}
			break;
		case IPL_MENU_OPTION:
			if ( mesp->menu_rot_y == 16384 && mesp->fadeout_z_param == 0 ||
				 mesp->enable_pre_key_input ) {
				Jac_PlaySe( JAC_SE_SUB_TO_MAIN );
				pre_menu_runstate = IPL_MENU_CENTER;
			}
			break;
		}
	}

	return FALSE;
}

/*-----------------------------------------------------------------------------
  選択しているメニューに向けて回転する。
  -----------------------------------------------------------------------------*/
static void
getSelectMenuMtx( Mtx v )
{
	Mtx tr;
	gGetTranslateRotateMtx(
		mesp->menu_rot_x,
		mesp->menu_rot_y,
		0,
		0,0,0, tr );
	MTXConcat( tr , v, v );
}


/*-----------------------------------------------------------------------------
  ゆらゆらのパラメータの更新
  -----------------------------------------------------------------------------*/
static void
updateRotateMenuCubeParam( void )
{
	SplashState* ssp = inSplashGetState();
	s16 frame = mesp->bound_frame;

//	if ( ssp->key_now_frame == 1 ) frame = 0 ;

	frame += 7;
	mesp->bound_frame = frame;
}

/*-----------------------------------------------------------------------------
  ゆらゆらゆれる。
  -----------------------------------------------------------------------------*/
static void
getRotateMenuCubeMtx ( Mtx v )
{
	Mtx tr;
	s16 frame = mesp->bound_frame;

	MTXIdentity( tr );

	MTXConcat( tr, v, v );

	gGetTranslateRotateMtx(
		(s16)( mesp->menu_swing_amp_x * gSCos( (s16)(mesp->menu_swing_speed_x * frame) )),
		(s16)( mesp->menu_swing_amp_y * gSCos( (s16)(mesp->menu_swing_speed_y * frame + mesp->menu_swing_offset_y ) )),	// yオフセット追加。
		(s16)( mesp->menu_swing_amp_z * gSCos( (s16)(mesp->menu_swing_speed_z * frame) )),
		mesp->menu_slide_amp_x * gSSin( (s16)(mesp->menu_slide_speed_x * frame + mesp->menu_slide_offset_x ) ),	// xオフセット追加。
		mesp->menu_slide_amp_y * gSSin( (s16)(mesp->menu_slide_speed_y * frame) ),
		0,	tr );

	MTXConcat ( tr , v , v);
}

/*-----------------------------------------------------------------------------
  描画用の更新
  -----------------------------------------------------------------------------*/
static void
inMenuUpdateMtx( void )
{
	Mtx  rotate;//, fade;
	SplashState* ssp = inSplashGetState();

	// メニューキューブの状態を描画するためのMtxの生成（暫定）
	MTXIdentity( rotate );
	getSelectMenuMtx( rotate );
	getRotateMenuCubeMtx( rotate );
	MTXIdentity( currentMtx );

	MTXConcat( rotate , currentMtx , currentMtx );
	if ( mesp->fadeout_z_param != 0 ) {
		gGetTranslateRotateMtx( 0,0,
								(s16)( mesp->fadeout_z_param * mesp->fadeout_z_rotnum ),
								0,0,
								mesp->fadeout_z_max * ( 1 - gSCos( mesp->fadeout_z_param ) ),
								rotate );
		MTXConcat( rotate, currentMtx, currentMtx );
	}

	setBaseMtx( inSplashGetMenuCube() , currentMtx ,
				(Vec){
					ssp->scale * 1.1f,
					ssp->scale * 1.1f,
					ssp->scale * 1.1f,
				} );
	setViewMtx( inSplashGetMenuCube() , getCurrentCamera() );
}

static void
updateBannerChecker( void )
{
	if ( gIsBanner() ) {
		glrUpdateFade( &banner, IPL_WINDOW_FADEIN );
		glrUpdateFade( &nodisk, IPL_WINDOW_FADEOUT );
		glrUpdateFade( &question, IPL_WINDOW_FADEOUT );
	}
	else {
		glrUpdateFade( &banner, IPL_WINDOW_FADEOUT );
		
		if ( gGetBs2State() == BS2_NO_DISK ) {
			glrUpdateFade( &nodisk, IPL_WINDOW_FADEIN );
			glrUpdateFade( &question, IPL_WINDOW_FADEOUT );
		}
		else {
			glrUpdateFade( &nodisk, IPL_WINDOW_FADEOUT );
			glrUpdateFade( &question, IPL_WINDOW_FADEIN );
		}
	}
}

/*-----------------------------------------------------------------------------
  メニューのアップデート
  -----------------------------------------------------------------------------*/
static void
updateDispWait( void )
{
	int i;
	for ( i = 0 ; i < 3 ; i++ ) {
		fs_option[i].wait_frame = mesp->disp_wait_frame[i];
		fs_memory[i].wait_frame = mesp->disp_wait_frame[i];
		fs_time[i].wait_frame = mesp->disp_wait_frame[i];
		fs_start[i].wait_frame = mesp->disp_wait_frame[i];
	}
}

static u32
updateMainMenu( void )
{
	u32 ret = FALSE;
	SplashState* ssp = inSplashGetState();
	s16 delta = mesp->fadeout_z_speed;

	if ( mesp->fadeout_z_param > delta ) {
		mesp->fadeout_z_param -= delta;
	}
	if ( mesp->fadeout_z_param <= delta ) mesp->fadeout_z_param = 0;

	if ( !ssp->force_fatalerror ) {
		ret = updateMenuControl();	// カーソルの移動。
	}

	updateRotateMenuCubeParam();	// Mtxの更新。
	inMenuUpdateMtx();	//	Mtxの生成。

	// モデルのアップデート
	updateModel( inSplashGetMenuCube() );

	// bannerのチェック。
	updateBannerChecker();

#ifndef IPL
	updateDispWait();
#endif

	// start nakamura's edit
	updateMainSmallCube();
	// end

	return ret;
}

/*-----------------------------------------------------------------------------

  -----------------------------------------------------------------------------*/
static void
updateFadeMainMenu( void )
{
	s16 delta = mesp->fadein_z_speed;
	if ( mesp->fadeout_z_param < (32767 - delta) ) mesp->fadeout_z_param += delta;

	if ( mesp->fadeout_z_param >= (32767 - delta) ) mesp->fadeout_z_param = 32767;

	updateRotateMenuCubeParam();	// Mtxの更新。
	inMenuUpdateMtx();	//	Mtxの生成。

	// モデルのアップデート
	updateModel( inSplashGetMenuCube() );
//	updateModel( inSplashGetSmallLogo() );
}


/*-----------------------------------------------------------------------------
  メニューの回転を更新する。
  -----------------------------------------------------------------------------*/
static void
updateMenuRot( void )
{
	s16 spd = mesp->menu_rot_speed;

	switch ( pre_menu_runstate )
	{
	case IPL_MENU_MEMORYCARD:
	case IPL_MENU_MEMORYCARD_SELECT:
		mesp->menu_rot_x -= spd;
		if ( mesp->menu_rot_y > 0 ) mesp->menu_rot_y = (s16)(mesp->menu_rot_y / spd * spd - spd);
		if ( mesp->menu_rot_y < 0 ) mesp->menu_rot_y = (s16)(mesp->menu_rot_y / spd * spd + spd);

		if ( mesp->menu_rot_x < -16384 ) {
			mesp->menu_rot_x = -16384;
		}
		break;
	case IPL_MENU_GAMESTART:
	case IPL_MENU_GAMESTART_SELECT:
		mesp->menu_rot_x += spd;
		if ( mesp->menu_rot_y > 0 ) mesp->menu_rot_y = (s16)(mesp->menu_rot_y / spd * spd - spd);
		if ( mesp->menu_rot_y < 0 ) mesp->menu_rot_y = (s16)(mesp->menu_rot_y / spd * spd + spd);

		if ( mesp->menu_rot_x > 16384 ) {
			mesp->menu_rot_x = 16384;
		}
		break;
	case IPL_MENU_CLOCK:
	case IPL_MENU_CLOCK_SELECT:
		if ( mesp->menu_rot_x > 0 ) mesp->menu_rot_x = (s16)(mesp->menu_rot_x / spd * spd - spd);
		if ( mesp->menu_rot_x < 0 )	mesp->menu_rot_x = (s16)(mesp->menu_rot_x / spd * spd + spd);
		mesp->menu_rot_y -= spd;

		if ( mesp->menu_rot_y < -16384 ) {
			mesp->menu_rot_y = -16384;
		}
		break;
	case IPL_MENU_OPTION:
	case IPL_MENU_OPTION_SELECT:
		if ( mesp->menu_rot_x > 0 ) mesp->menu_rot_x = (s16)(mesp->menu_rot_x / spd * spd - spd);
		if ( mesp->menu_rot_x < 0 )	mesp->menu_rot_x = (s16)(mesp->menu_rot_x / spd * spd + spd);

		mesp->menu_rot_y += spd;
		if ( mesp->menu_rot_y > 16384 ) {
			mesp->menu_rot_y = 16384;
		}
		break;
	case IPL_MENU_CENTER:
		if ( mesp->menu_rot_x > 0 ) mesp->menu_rot_x = (s16)(mesp->menu_rot_x / spd * spd - spd);
		if ( mesp->menu_rot_x < 0 )	mesp->menu_rot_x = (s16)(mesp->menu_rot_x / spd * spd + spd);
		if ( mesp->menu_rot_y > 0 ) mesp->menu_rot_y = (s16)(mesp->menu_rot_y / spd * spd - spd);
		if ( mesp->menu_rot_y < 0 ) mesp->menu_rot_y = (s16)(mesp->menu_rot_y / spd * spd + spd);
		if ( mesp->menu_rot_y == 0 && mesp->menu_rot_x == 0 ) {
		}
		break;
	default:
		break;
	}
}

static void
inMenuUpdateInsideAnime( void )
{
	if ( menu_runstate == IPL_MENU_MEMORYCARD ) {
		updateCCAnime();
	}
	else if ( menu_runstate == IPL_MENU_MEMORYCARD_SELECT ) {
		concentCCAnime();
	}
	else {
		clearCCAnime();
	}

	if ( menu_runstate == IPL_MENU_GAMESTART ) {
		updateSCAnime();
	}
	else if ( menu_runstate == IPL_MENU_GAMESTART_SELECT ) {
		concentSCAnime();
	}
	else {
		clearSCAnime();
	}

	if ( menu_runstate == IPL_MENU_OPTION ) {
		updateOCAnime();
	}
	else if ( menu_runstate == IPL_MENU_OPTION_SELECT ) {
		concentOCAnime();
	}
	else {
		clearOCAnime();
	}


	if ( menu_runstate == IPL_MENU_CLOCK ) {
		updateOLAnime();
	}
	else if ( menu_runstate == IPL_MENU_CLOCK_SELECT ) {
		concentOLAnime();
	}
	else {
		clearOLAnime();
	}

	updateCCAnimeMtx();
	updateSCAnimeMtx();
	updateOCAnimeMtx();
	updateOLAnimeMtx();
}

static void
resetStartSurface( void )
{
	glrResetFade( &banner );
	glrResetFade( &nodisk );
	glrResetFade( &question );
}

static void
inMenuPreUpdate( void )
{
	if ( menu_runstate == IPL_MENU_MEMORYCARD ) {
		if ( pre_menu_runstate == IPL_MENU_MEMORYCARD_SELECT ) {
			resetCardMessage();
			setCardIcon();
			resetCardIconMove();
		}
	}
	if ( menu_runstate == IPL_MENU_GAMESTART ) {
		if ( pre_menu_runstate == IPL_MENU_GAMESTART_SELECT ) {
			resetStartMessage();
		}
	}
	if ( menu_runstate == IPL_MENU_GAMESTART_SELECT ) {
		if ( pre_menu_runstate == IPL_MENU_GAMESTART ) {
			resetStartSurface();
		}
	}
}

static void
updateMemSurface( void )
{
	s8		add_factor;
	s32		no;
	SlotState*	tSlot = getCardSlotState();

	for ( no = 0 ; no < 2 ; no++ ) {
		if ( tSlot[no].state == SLOT_READY ) {
			add_factor = 4;
			max_capa[no] = (int)( 255 - tSlot[no].free_blocks * 255 / tSlot[no].full_blocks );
		}
		else {
			add_factor = -4;
		}

		mem_win_alpha[ no ] += add_factor;

		if ( mem_win_alpha[ no ] > 255 ) mem_win_alpha[ no ] = 255;
		if ( mem_win_alpha[ no ] < 60 )   mem_win_alpha[ no ] = 60;
	}
}

static void
updateOperationLanguage( void )
{
	int i;

	for ( i = 0 ; i < COUNTRY_PAL_MAX_LANGUAGE ; i++ ) {
		u8 fade = ( i == operation_language ) ? IPL_WINDOW_FADEIN : IPL_WINDOW_FADEOUT;
		glrUpdateFade( &operation_language_string[i], fade );
	}
}

static void
updateOperation( void )
{

	// 選択時の操作説明表示。
	switch ( menu_runstate ) {
	case IPL_MENU_CENTER:
		glrUpdateFade( &exmain[0], IPL_WINDOW_FADEIN );
		glrUpdateFade( &exmain[1], IPL_WINDOW_FADEOUT );
		glrUpdateFade( &exmain[2], IPL_WINDOW_FADEOUT );
		glrUpdateFade( &sentaku  , IPL_WINDOW_FADEOUT );
		glrUpdateFade( &tyousei  , IPL_WINDOW_FADEOUT );
		glrUpdateFade( &cancel   , IPL_WINDOW_FADEOUT );
		glrUpdateFade( &kettei   , IPL_WINDOW_FADEOUT );
		glrUpdateFade( &exit_str , IPL_WINDOW_FADEOUT );
		break;
	case IPL_MENU_MEMORYCARD:
	case IPL_MENU_CLOCK:
	case IPL_MENU_OPTION:
	case IPL_MENU_GAMESTART:
		glrUpdateFade( &exmain[0], IPL_WINDOW_FADEOUT );
		glrUpdateFade( &exmain[1], IPL_WINDOW_FADEIN  );
		glrUpdateFade( &exmain[2], IPL_WINDOW_FADEIN  );
		glrUpdateFade( &sentaku  , IPL_WINDOW_FADEOUT );
		glrUpdateFade( &tyousei  , IPL_WINDOW_FADEOUT );
		glrUpdateFade( &cancel   , IPL_WINDOW_FADEOUT );
		glrUpdateFade( &exit_str , IPL_WINDOW_FADEOUT );
		glrUpdateFade( &kettei   , IPL_WINDOW_FADEOUT );
		break;
	case IPL_MENU_GAMESTART_SELECT:
		glrUpdateFade( &exmain[0], IPL_WINDOW_FADEOUT );
		glrUpdateFade( &exmain[1], IPL_WINDOW_FADEOUT );
		glrUpdateFade( &exmain[2], IPL_WINDOW_FADEOUT );
		glrUpdateFade( &sentaku  , IPL_WINDOW_FADEOUT );
		glrUpdateFade( &tyousei  , IPL_WINDOW_FADEOUT );
		if ( !getStartFatalState() ) {
			glrUpdateFade( &cancel   , IPL_WINDOW_FADEIN );
		} else {
			glrUpdateFade( &cancel   , IPL_WINDOW_FADEOUT );
		}
		glrUpdateFade( &exit_str , IPL_WINDOW_FADEOUT );
		glrUpdateFade( &kettei   , IPL_WINDOW_FADEOUT );
		break;
	case IPL_MENU_OPTION_SELECT:
		if ( getOptionRunstate() ) {
			glrUpdateFade( &sentaku , IPL_WINDOW_FADEIN );
			glrUpdateFade( &tyousei , IPL_WINDOW_FADEOUT );
			glrUpdateFade( &exit_str, IPL_WINDOW_FADEIN );
			glrUpdateFade( &cancel  , IPL_WINDOW_FADEOUT );
		} else {
			glrUpdateFade( &sentaku , IPL_WINDOW_FADEOUT );
			glrUpdateFade( &tyousei , IPL_WINDOW_FADEIN );
			glrUpdateFade( &exit_str, IPL_WINDOW_FADEOUT );
			glrUpdateFade( &cancel  , IPL_WINDOW_FADEIN );
		}
		glrUpdateFade( &exmain[0], IPL_WINDOW_FADEOUT );
		glrUpdateFade( &exmain[1], IPL_WINDOW_FADEOUT );
		glrUpdateFade( &exmain[2], IPL_WINDOW_FADEOUT );
		glrUpdateFade( &kettei , IPL_WINDOW_FADEIN );
		break;
	case IPL_MENU_CLOCK_SELECT:
		if ( getTimeRunstate() ) {
			glrUpdateFade( &sentaku , IPL_WINDOW_FADEIN );
			glrUpdateFade( &tyousei , IPL_WINDOW_FADEOUT );
			glrUpdateFade( &exit_str, IPL_WINDOW_FADEIN );
			glrUpdateFade( &cancel  , IPL_WINDOW_FADEOUT );
		} else {
			glrUpdateFade( &sentaku , IPL_WINDOW_FADEOUT );
			glrUpdateFade( &tyousei , IPL_WINDOW_FADEIN );
			glrUpdateFade( &exit_str, IPL_WINDOW_FADEOUT );
			glrUpdateFade( &cancel  , IPL_WINDOW_FADEIN );
		}
		glrUpdateFade( &kettei   , IPL_WINDOW_FADEIN );
		glrUpdateFade( &exmain[0], IPL_WINDOW_FADEOUT );
		glrUpdateFade( &exmain[1], IPL_WINDOW_FADEOUT );
		glrUpdateFade( &exmain[2], IPL_WINDOW_FADEOUT );
		break;
	case IPL_MENU_MEMORYCARD_SELECT:
		if ( getCardRunstate() ) {		// メモリカードファイル選択時。
			glrUpdateFade( &cancel  , IPL_WINDOW_FADEOUT );
			glrUpdateFade( &exit_str, IPL_WINDOW_FADEIN );
		} else {			// メモリカードメニューそれ以外。
			glrUpdateFade( &cancel  , IPL_WINDOW_FADEIN );
			glrUpdateFade( &exit_str, IPL_WINDOW_FADEOUT );
		}
		glrUpdateFade( &kettei   , IPL_WINDOW_FADEIN );
		glrUpdateFade( &exmain[0], IPL_WINDOW_FADEOUT );
		glrUpdateFade( &exmain[1], IPL_WINDOW_FADEOUT );
		glrUpdateFade( &exmain[2], IPL_WINDOW_FADEOUT );
		glrUpdateFade( &sentaku  , IPL_WINDOW_FADEIN );
		break;
	}

	// スティックおよびボタンの絵テクスチャ
	switch ( menu_runstate )
	{
	case IPL_MENU_CENTER:
		glrUpdateFade( &main[0], IPL_WINDOW_FADEOUT );
		glrUpdateFade( &main[1], IPL_WINDOW_FADEOUT );
		glrUpdateFade( &main[2], IPL_WINDOW_FADEOUT );
		glrUpdateFade( &main[3], IPL_WINDOW_FADEIN  );
		glrUpdateFade( &main[4], IPL_WINDOW_FADEOUT );
		glrUpdateFade( &main[5], IPL_WINDOW_FADEOUT );
		break;
	case IPL_MENU_GAMESTART_SELECT:
		glrUpdateFade( &main[0], IPL_WINDOW_FADEOUT );
		if ( !getStartFatalState() ) {
			glrUpdateFade( &main[1], IPL_WINDOW_FADEIN  );
		} else {
			glrUpdateFade( &main[1], IPL_WINDOW_FADEOUT  );
		}
		glrUpdateFade( &main[2], IPL_WINDOW_FADEOUT );
		glrUpdateFade( &main[3], IPL_WINDOW_FADEOUT );
		glrUpdateFade( &main[4], IPL_WINDOW_FADEOUT );
		glrUpdateFade( &main[5], IPL_WINDOW_FADEOUT );
		break;
	case IPL_MENU_GAMESTART:
	case IPL_MENU_OPTION:
	case IPL_MENU_CLOCK:
	case IPL_MENU_MEMORYCARD:
		glrUpdateFade( &main[0], IPL_WINDOW_FADEOUT );
		glrUpdateFade( &main[1], IPL_WINDOW_FADEOUT );
		glrUpdateFade( &main[2], IPL_WINDOW_FADEOUT );
		glrUpdateFade( &main[3], IPL_WINDOW_FADEOUT );
		glrUpdateFade( &main[4], IPL_WINDOW_FADEIN  );
		glrUpdateFade( &main[5], IPL_WINDOW_FADEIN  );
		break;
	default:
		glrUpdateFade( &main[0], IPL_WINDOW_FADEIN );
		glrUpdateFade( &main[1], IPL_WINDOW_FADEIN );
		glrUpdateFade( &main[2], IPL_WINDOW_FADEIN );
		glrUpdateFade( &main[3], IPL_WINDOW_FADEOUT );
		glrUpdateFade( &main[4], IPL_WINDOW_FADEOUT );
		glrUpdateFade( &main[5], IPL_WINDOW_FADEOUT );
		break;
	}

#ifdef PAL
	updateOperationLanguage();
#endif
}

static void
update2DSelect( void )
{
	int i;
	for ( i=0 ; i<3 ; i++ ) {
		if ( menu_runstate == IPL_MENU_MEMORYCARD_SELECT ) {
			glrUpdateFade( &fs_memory[i], IPL_WINDOW_FADEIN );
		}
		else {
			glrUpdateFade( &fs_memory[i], IPL_WINDOW_FADEOUT );
		}

		if ( menu_runstate == IPL_MENU_GAMESTART_SELECT ) {
			glrUpdateFade( &fs_start[i], IPL_WINDOW_FADEIN );
		}
		else {
			glrUpdateFade( &fs_start[i], IPL_WINDOW_FADEOUT );
		}

		if ( menu_runstate == IPL_MENU_OPTION_SELECT ) {
			glrUpdateFade( &fs_option[i], IPL_WINDOW_FADEIN );
		}
		else {
			glrUpdateFade( &fs_option[i], IPL_WINDOW_FADEOUT );
		}

		if ( menu_runstate == IPL_MENU_CLOCK_SELECT ) {
			glrUpdateFade( &fs_time[i], IPL_WINDOW_FADEIN );
		}
		else {
			glrUpdateFade( &fs_time[i], IPL_WINDOW_FADEOUT );
		}
	}

}

/*-----------------------------------------------------------------------------
  メニューの更新
  -----------------------------------------------------------------------------*/
static void
checkCounterBias( void )
{
	static BOOL first = TRUE;	// 絶対にFALSEからTRUEに戻すことはない。
	
	if ( first ) {
		if ( gGetInvalidCounterBias() ) {
			// セットしたことがない。
			OSSram* sram;
			sram = __OSLockSram();
			sram->flags |= OS_SRAM_BIAS_FLAG;
			__OSUnlockSram(TRUE);
			while (!__OSSyncSram()) ;
		}
		first = FALSE;
	}
}

u32
inMenuUpdate( void )
{
	u32	ret = FALSE;
	SplashState* ssp = inSplashGetState();
	MainState* msp = getMainState();

	checkCounterBias();
	
	if ( gGetBs2State() == BS2_FATAL_ERROR ) {
		ssp->force_fatalerror = TRUE;
	}
	
	msp->ortho = 0;

	updateMenuRot();	// メニューキューブの回転の更新。
	inMenuUpdateInsideAnime();	// キューブの中のアニメーションのupdate。
	updateMemSurface();	// メモリーカード面のウィンドウのアップデート。
	updateOperation();	// 操作説明の表示のアップデート
	update2DSelect();	// 2D面に入っているときの表示用のfaderアップデート。

	inMenuPreUpdate();

	menu_runstate = pre_menu_runstate;
	mesp->runstate = menu_runstate;	// 描画時に実行モードが取得できるようにする。

	switch ( menu_runstate )
	{
	case IPL_MENU_CENTER:
	case IPL_MENU_GAMESTART:
	case IPL_MENU_CLOCK:
	case IPL_MENU_OPTION:
	case IPL_MENU_MEMORYCARD:
		ret = updateMainMenu();
		break;
	case IPL_MENU_GAMESTART_SELECT:
		updateFadeMainMenu();
		pre_menu_runstate = updateStartMenu();
		break;
	case IPL_MENU_MEMORYCARD_SELECT:
		updateFadeMainMenu();
		pre_menu_runstate = updateCardMenu();
		break;
	case IPL_MENU_CLOCK_SELECT:
		updateFadeMainMenu();
		pre_menu_runstate = updateTimeMenu();
		break;
	case IPL_MENU_OPTION_SELECT:
		updateFadeMainMenu();
		pre_menu_runstate = updateOptionMenu();
		break;
	}
	
	if ( ssp->force_fatalerror ) {
		glrUpdateFade( &fatal_error , IPL_WINDOW_FADEIN );
	}

#ifndef IPL
	if ( !ret ) {
//		msp->ortho = 0;
		return cMenu_Select;
	}
	else {
		ssp->rot_y = 0;
		ssp->rot_v = 0;
		ssp->rot_a = 0;
		msp->ortho = 1;
		return cSpRun_Rotate;
	}
#endif
	return cMenu_Select;
}

MtxPtr
getMainMenuMtx( void )
{
	return currentMtx;
}

/*-----------------------------------------------------------------------------
  MenuStateの取得
  -----------------------------------------------------------------------------*/
MenuState*
getMenuState( void )
{
	return mesp;
}

void
setOperationLanguage( int language )
{
	operation_language = (s16)language;
}

















/*-----------------------------------------------------------------------------
  表面を描画する内部関数
  -----------------------------------------------------------------------------*/
void
drawCenterSurface( Mtx menu_mtx, u8 alpha )
{
	Mtx			mv, m;
	J3DTransformInfo ti = (J3DTransformInfo){ 1.f, -1.f, 1.f, 0,0,0,0, -144, 144, 144 };
	GXColor		white  = {0xff,0xff,0xff,0xff};
	MTXConcat( getCurrentCamera(), menu_mtx/*getMainMenuMtx()*/, mv );

	white.a = alpha;

	gGetTransformMtx( &ti , m );
	MTXConcat( mv, m, m );
	GXLoadPosMtxImm( m, GX_PNMTX0 );
	GXSetCurrentMtx( GX_PNMTX0 );

	white.a = (u8)( white.a * alpha /255 );
//	glrDrawWindow( 'fram', lay_mai , &white );

	setGXforglrPicture();
	glrDrawPictureAll( lay_mai, &white );
//	setGXforglrText();
//	glrDrawTextBox( 'mess', lay_mai, &white );
}

/*-----------------------------------------------------------------------------
  ゲームプレイ画面を描画
  -----------------------------------------------------------------------------*/
static void
drawBanner3d( gLayoutHeader* lhp, u8 alpha )
{
	ResTIMG	tex_image;
	gBanner2*	banner_data;
	s16			b_alpha;
	GXColor		white  = {0xff,0xff,0xff,0xff};
	GXColor		gray  = {40,40,40,0xff};

	glrGetOffsetAlpha( &banner, &b_alpha, NULL );
	if ( b_alpha == 0 ) return;

	banner_data = (gBanner2*)gGetBannerBuffer();

	white.a = (u8)( alpha * b_alpha /255 );
	gray.a  = (u8)( alpha  * b_alpha /255 );

	tex_image.enableLOD       = GX_DISABLE;
	tex_image.enableEdgeLOD   = GX_DISABLE;
	tex_image.enableBiasClamp = GX_DISABLE;
	tex_image.enableMaxAniso  = (u8)GX_ANISO_1;
	tex_image.width  = 96;
	tex_image.height = 32;
	tex_image.wrapS  = GX_CLAMP;
	tex_image.wrapT  = GX_CLAMP;
	tex_image.format = GX_TF_RGB5A3;
	tex_image.minFilter = GX_LINEAR;
	tex_image.magFilter = GX_LINEAR;
	tex_image.minLOD    = 0;
	tex_image.maxLOD    = 0;
	tex_image.LODBias   = 0;
	tex_image.indexTexture = FALSE;
	tex_image.imageOffset  = (s32)( (u32)banner_data + 32 - (u32)&tex_image );

	gSetGXforDirect( TRUE, FALSE, TRUE );
	glrDrawPictureTex( 'bana', lhp, &white, &tex_image );

	setGXforglrText();

	if ( gGetBannerType() == cIPL_BannerType_OneLanguage ) {
		glrDrawTextBoxNStringOneLine( 'zame', lhp, &gray  , (char*)banner_data->com[0].title_3d , 32);
		glrDrawTextBoxNStringOneLine( 'game', lhp, &white , (char*)banner_data->com[0].title_3d , 32);
		glrDrawTextBoxNStringOneLine( 'zakr', lhp, &gray  , (char*)banner_data->com[0].maker_3d , 32);
		glrDrawTextBoxNStringOneLine( 'makr', lhp, &white , (char*)banner_data->com[0].maker_3d , 32);
	}
	else if ( gGetBannerType() == cIPL_BannerType_SixLanguage ) {
		glrDrawTextBoxNStringOneLine( 'zame', lhp, &gray  , (char*)banner_data->com[ gGetPalCountry() ].title_3d , 32);
		glrDrawTextBoxNStringOneLine( 'game', lhp, &white , (char*)banner_data->com[ gGetPalCountry() ].title_3d , 32);
		glrDrawTextBoxNStringOneLine( 'zakr', lhp, &gray  , (char*)banner_data->com[ gGetPalCountry() ].maker_3d , 32);
		glrDrawTextBoxNStringOneLine( 'makr', lhp, &white , (char*)banner_data->com[ gGetPalCountry() ].maker_3d , 32);
	}

}

static void
drawGamSurface( gLayoutHeader* lhp, J3DTransformInfo* ti, u8 alpha )
{
	Mtx			mv, m;
	GXColor		white  = {0xff,0xff,0xff,0xff};
	GXColor		pic, pic_gray;//  = {0xff,0xff,0xff,0xff};
	GXColor		gray  = {40,40,40,0xff};
	s16			pic_alpha;

	MTXConcat( getCurrentCamera(), getMainMenuMtx(), mv );

	white.a = alpha;

	gGetTransformMtx( ti , m );
	MTXConcat( mv, m, m );
	GXLoadPosMtxImm( m, GX_PNMTX0 );
	GXSetCurrentMtx( GX_PNMTX0 );

	white.a = (u8)( white.a * alpha /255 );
	gray.a  = (u8)( gray.a * alpha /255 );
	glrDrawWindow( 'fram', lhp , &white );

	setGXforglrPicture();
	glrDrawPicture('titl', lhp, &white );

	glrGetOffsetAlpha( &nodisk, &pic_alpha, NULL );
	if ( pic_alpha > 0 ) {
		pic = white;
		pic_gray = gray;
		pic.a = (u8)( pic.a * pic_alpha / 255 );
		pic_gray.a = (u8)( pic_gray.a * pic_alpha / 255 );
		glrDrawPicture( 'zodi', lhp, &pic_gray );
		glrDrawPicture( 'nodi', lhp, &pic );
	}
	glrGetOffsetAlpha( &question, &pic_alpha, NULL );
	if ( pic_alpha > 0 ) {
		pic = white;
		pic_gray = gray;
		pic.a = (u8)( pic.a * pic_alpha / 255 );
		pic_gray.a = (u8)( pic_gray.a * pic_alpha / 255 );
		glrDrawPicture( 'zust', lhp, &pic_gray );
		glrDrawPicture( 'qust', lhp, &pic );
	}

	drawBanner3d( lhp, alpha );	// バナーの表示。

//	setGXforglrText();
//	glrDrawTextBox( 'mess', lhp, &white );
}

/*-----------------------------------------------------------------------------
  それぞれのスロットに関連するウィンドウを描画します。
  -----------------------------------------------------------------------------*/
static void
drawMemCapaWindow( u32 tag, gLayoutHeader* l_hdr, GXColor* rasc, u8 capa )
{
	u16 i;
	gLRWindowData* wdp = (gLRWindowData*)( (u32)l_hdr + (u32)l_hdr->window_offset );

	if ( capa == 0 ) return;

	for ( i = 0 ; i < l_hdr->window_num ; i++ ) {
		if ( wdp[i].tag == tag ) {
			int contents_x  = wdp[i].x - wdp[i].contents_dx / 2;
			int top_y       = wdp[i].y - wdp[i].contents_dy / 2;
			int bottom_y    = top_y + wdp[i].contents_dy;
			int	capa_y		= wdp[i].contents_dy  * capa / 255;

			top_y = bottom_y - capa_y;
			glrDrawWindowPosID( i, l_hdr, rasc, contents_x, top_y, wdp[i].contents_dx , capa_y );
		}
	}
}

/*-----------------------------------------------------------------------------
  スロットa/bのメモリカードが刺さっているかどうか、
  -----------------------------------------------------------------------------*/
static void
drawMemSlotWindow( u8 no, gLayoutHeader* lhp, u8 alpha )
{
	u32		tag;
	int		capa;
	GXColor		white  = {0xff,0xff,0xff,0xff};

	white.a = alpha;
	setGXforglrPicture();
	tag = 'texa' + no;
	glrDrawPicture( tag ,  lhp, &white );

	white.a = (u8)( mem_win_alpha[no] * alpha / 255 );
	tag = 'mca1' + no * 256;
	glrDrawWindow( tag, lay_mem, &white );
	tag = 'mca2' + no * 256;
	capa = (int)( max_capa[no] * (mem_win_alpha[no] - 60 ) / ( 255 - 60 ) );
	drawMemCapaWindow( tag, lay_mem, &white, (u8)capa );
}

/*-----------------------------------------------------------------------------
  3dメニューキューブのメモリーカード面を描画します。
  -----------------------------------------------------------------------------*/
static void
drawMemSurface( gLayoutHeader* lhp, J3DTransformInfo* ti, u8 alpha )
{
	Mtx			mv, m;
	GXColor		white  = {0xff,0xff,0xff,0xff};
	MTXConcat( getCurrentCamera(), getMainMenuMtx(), mv );

	white.a = alpha;

	gGetTransformMtx( ti , m );
	MTXConcat( mv, m, m );
	GXLoadPosMtxImm( m, GX_PNMTX0 );
	GXSetCurrentMtx( GX_PNMTX0 );

	white.a = (u8)( white.a * alpha /255 );
	glrDrawWindow( 'fram', lhp , &white );

	setGXforglrPicture();
	glrDrawPicture( 'titl', lhp, &white );
//	setGXforglrText();
//	glrDrawTextBox( 'mess', lhp, &white );

	drawMemSlotWindow( 0, lhp, alpha );
	drawMemSlotWindow( 1, lhp, alpha );
}



/*-----------------------------------------------------------------------------
  表面を描画する内部関数
  -----------------------------------------------------------------------------*/
static void
drawCalSurface( gLayoutHeader* lhp, J3DTransformInfo* ti, u8 alpha )
{
	Mtx			mv, m;
	GXColor		white  = {0xff,0xff,0xff,0xff};
	GXColor		gray   = {40, 40, 40, 0xff};
	OSCalendarTime 	time;

	MTXConcat( getCurrentCamera(), getMainMenuMtx(), mv );

	memset(&time, 0, sizeof(time));
	OSTicksToCalendarTime( OSGetTime() , &time);

	white.a = alpha;

	gGetTransformMtx( ti , m );
	MTXConcat( mv, m, m );
	GXLoadPosMtxImm( m, GX_PNMTX0 );
	GXSetCurrentMtx( GX_PNMTX0 );

	white.a = (u8)( white.a * alpha /255 );
	gray.a = (u8)( gray.a * alpha /255 );
	glrDrawWindow( 'fram', lhp , &white );

	setGXforglrPicture();
	//glrDrawPictureAll( lhp, &white );
	glrDrawPicture( 'zolo', lhp, &gray );
	glrDrawPicture( 'zlas', lhp, &gray );
	glrDrawPicture( 'zare', lhp, &gray );
	glrDrawPicture( 'zitl', lhp, &gray );
	glrDrawPictureTex( 'zal1', lhp, &gray, num_timg[2] );
	glrDrawPictureTex( 'zal2', lhp, &gray, num_timg[ time.year / 100 % 10 ] );
	glrDrawPictureTex( 'zal3', lhp, &gray, num_timg[ time.year / 10 % 10 ] );
	glrDrawPictureTex( 'zal4', lhp, &gray, num_timg[ time.year      % 10 ] );
	glrDrawPictureTex( 'zal5', lhp, &gray, num_timg[ ( time.mon + 1 ) /10 % 10 ] );
	glrDrawPictureTex( 'zal6', lhp, &gray, num_timg[ ( time.mon + 1 )     % 10 ] );
	glrDrawPictureTex( 'zal7', lhp, &gray, num_timg[ time.mday / 10 ] );
	glrDrawPictureTex( 'zal8', lhp, &gray, num_timg[ time.mday % 10 ] );
	glrDrawPictureTex( 'zeek', lhp, &gray, week_timg[ time.wday ] );
	glrDrawPictureTex( 'zim1', lhp, &gray, num_timg[ time.hour / 10 ] );
	glrDrawPictureTex( 'zim2', lhp, &gray, num_timg[ time.hour % 10 ] );
	glrDrawPictureTex( 'zim3', lhp, &gray, num_timg[ time.min  / 10 ] );
	glrDrawPictureTex( 'zim4', lhp, &gray, num_timg[ time.min  % 10 ] );
	glrDrawPictureTex( 'zim5', lhp, &gray, num_timg[ time.sec  / 10 ] );
	glrDrawPictureTex( 'zim6', lhp, &gray, num_timg[ time.sec  % 10 ] );

	glrDrawPicture( 'colo', lhp, &white );
	glrDrawPicture( 'slas', lhp, &white );
	glrDrawPicture( 'pare', lhp, &white );
	glrDrawPicture( 'titl', lhp, &white );
	glrDrawPictureTex( 'cal1', lhp, &white, num_timg[2] );
	glrDrawPictureTex( 'cal2', lhp, &white, num_timg[ time.year / 100 % 10 ] );
	glrDrawPictureTex( 'cal3', lhp, &white, num_timg[ time.year / 10 % 10 ] );
	glrDrawPictureTex( 'cal4', lhp, &white, num_timg[ time.year      % 10 ] );
	glrDrawPictureTex( 'cal5', lhp, &white, num_timg[ ( time.mon + 1 ) /10 % 10 ] );
	glrDrawPictureTex( 'cal6', lhp, &white, num_timg[ ( time.mon + 1 )     % 10 ] );
	glrDrawPictureTex( 'cal7', lhp, &white, num_timg[ time.mday / 10 ] );
	glrDrawPictureTex( 'cal8', lhp, &white, num_timg[ time.mday % 10 ] );
	glrDrawPictureTex( 'week', lhp, &white, week_timg[ time.wday ] );
	glrDrawPictureTex( 'tim1', lhp, &white, num_timg[ time.hour / 10 ] );
	glrDrawPictureTex( 'tim2', lhp, &white, num_timg[ time.hour % 10 ] );
	glrDrawPictureTex( 'tim3', lhp, &white, num_timg[ time.min  / 10 ] );
	glrDrawPictureTex( 'tim4', lhp, &white, num_timg[ time.min  % 10 ] );
	glrDrawPictureTex( 'tim5', lhp, &white, num_timg[ time.sec  / 10 ] );
	glrDrawPictureTex( 'tim6', lhp, &white, num_timg[ time.sec  % 10 ] );

	setGXforglrText();
//	glrDrawTextBox( 'mess', lhp, &white );
}

/*-----------------------------------------------------------------------------
  オプション面の表面を描画します。
  -----------------------------------------------------------------------------*/
static void
drawOptSurface( gLayoutHeader* lhp, J3DTransformInfo* ti, u8 alpha )
{
	Mtx			mv, m;
	GXColor		white  = {0xff,0xff,0xff,0xff};
	GXColor		gray   = {40, 40, 40, 0xff};
	u32			sound = OSGetSoundMode();
	s16			disp_offset_h;
	OSSram      *sram;

	sram = __OSLockSram();
	disp_offset_h = sram->displayOffsetH;
	__OSUnlockSram(FALSE);

	MTXConcat( getCurrentCamera(), getMainMenuMtx(), mv );

	gGetTransformMtx( ti , m );
	MTXConcat( mv, m, m );
	GXLoadPosMtxImm( m, GX_PNMTX0 );
	GXSetCurrentMtx( GX_PNMTX0 );

	white.a = (u8)( white.a * alpha /255 );
	gray.a = (u8)( gray.a * alpha / 255 );
	glrDrawWindow( 'fram', lhp , &white );

	setGXforglrPicture();
	//glrDrawPictureAll( lhp, &white );
	glrDrawPicture( 'zxt1', lhp, &gray );
	glrDrawPicture( 'zxt2', lhp, &gray );
	glrDrawPicture( 'zitl', lhp, &gray );
	glrDrawPicture( 'txt1', lhp, &white );
	glrDrawPicture( 'txt2', lhp, &white );
	glrDrawPicture( 'titl', lhp, &white );

	if ( disp_offset_h > 0 ) {
		// 右
		glrDrawPicture( 'zrwr', lhp, &gray );
		glrDrawPicture( 'arwr', lhp, &white );
	}
	else if ( disp_offset_h < 0 ) {
		// 左
		glrDrawPicture( 'zrwl', lhp, &gray );
		glrDrawPicture( 'arwl', lhp, &white );
	}
	else {
		// 中
	}
	if ( disp_offset_h < -32 ) disp_offset_h = -32;
	if ( disp_offset_h > 32 ) disp_offset_h = 32;

	glrDrawPictureTex( 'zum1', lhp, &gray, num_timg[ gABS(disp_offset_h) / 10 ] );
	glrDrawPictureTex( 'zum2', lhp, &gray, num_timg[ gABS(disp_offset_h) % 10 ] );
	glrDrawPictureTex( 'num1', lhp, &white, num_timg[ gABS(disp_offset_h) / 10 ] );
	glrDrawPictureTex( 'num2', lhp, &white, num_timg[ gABS(disp_offset_h) % 10 ] );

	if ( sound != 0 ) sound = 1;
	glrDrawPictureTex( 'zond', lhp, &gray, sound_timg[ sound ] );
	glrDrawPictureTex( 'sond', lhp, &white, sound_timg[ sound ] );

#ifdef PAL
	glrDrawPicture( 'zxt3', lhp, &gray );
	glrDrawPicture( 'zang', lhp, &gray );
	glrDrawPicture( 'txt3', lhp, &white );
	glrDrawPicture( 'lang', lhp, &white );
#endif

	setGXforglrText();
//	glrDrawTextBox( 'mess', lhp, &white );
}


/*-----------------------------------------------------------------------------
  表面に書く説明の描画。
  -----------------------------------------------------------------------------*/
static void
drawMenuSurface( u8 alpha )
{
	s16			rot_x,rot_y;
	f32			sc = 144.f;
	J3DTransformInfo ti;// = { 1.f, -1.f, 1.f, 0,0,0,0, -144, 144, sc };

	ti = (J3DTransformInfo){ 1.f, -1.f, 1.f, 0,0,0,0, -144, 144, sc };
	// 表面に書く説明の描画。
	if ( ( mesp->menu_rot_x >= 0 && mesp->menu_rot_x <  5000 ) ||
		 ( mesp->menu_rot_x <= 0 && mesp->menu_rot_x > -5000 ) ) {
		rot_x = (s16)gABS( mesp->menu_rot_x );
	} else {
		rot_x = 5000;
	}

	if ( ( mesp->menu_rot_y >= 0 && mesp->menu_rot_y <  5000 ) ||
		 ( mesp->menu_rot_y <= 0 && mesp->menu_rot_y > -5000 ) ) {
		rot_y = (s16)gABS( mesp->menu_rot_y );
	} else {
		rot_y = 5000;
	}

	if ( rot_x > rot_y )      drawCenterSurface( getMainMenuMtx(), (u8)( 255 - 255 * rot_x / 5000 ) );
	else if ( rot_x < rot_y ) drawCenterSurface( getMainMenuMtx(), (u8)( 255 - 255 * rot_y / 5000 ) );
	else if ( rot_x == 0 )    drawCenterSurface( getMainMenuMtx(), (u8)( 255 - 255 * rot_x / 5000 ) );

	if ( mesp->menu_rot_x <= -11834 ) {	// メモリーカード。
		ti = (J3DTransformInfo){ 1.f, -1.f, 1.f, 16384,0,0,0, -144, -sc, 144 };
		drawMemSurface( lay_mem, &ti, (u8)( ( 255 - 255 * ( 16384 - gABS(mesp->menu_rot_x) ) / 5000 ) * alpha / 255 ) );
	}

	if ( mesp->menu_rot_x >= 11834 ) {	// ゲームスタート。
		ti = (J3DTransformInfo){ 1.f, -1.f, 1.f, -16384,0,0,0, -144, sc, -144 };
		drawGamSurface( lay_gam, &ti, (u8)( ( 255 - 255 * ( 16384 - gABS(mesp->menu_rot_x) ) / 5000 ) * alpha / 255 ) );
	}

	if ( mesp->menu_rot_y <= -11834 ) {	// カレンダー。
		ti = (J3DTransformInfo){ 1.f, -1.f, 1.f, 0, 16384, 0,0, sc, 144, 144 };
		drawCalSurface ( lay_cal, &ti, (u8)( ( 255 - 255 * ( 16384 - gABS(mesp->menu_rot_y) ) / 5000 ) * alpha / 255 ) );
	}

	if ( mesp->menu_rot_y >= 11834 ) {	// オプション。
		ti = (J3DTransformInfo){ 1.f, -1.f, 1.f, 0, -16384, 0,0, -sc, 144, -144 };
		drawOptSurface( lay_opt, &ti, (u8)( ( 255 - 255 * ( 16384 - gABS(mesp->menu_rot_y) ) / 5000 ) * alpha / 255 ) );
	}

}

BOOL
isMenuDispMemoryCard( void )
{
	s16		alpha[3];
	int		i;
	for ( i=0 ; i<3 ; i++ ) {
		glrGetOffsetAlpha( &fs_memory[i], &alpha[i], NULL );
	}

	if ( alpha[0] > 172 && alpha[1] > 172 && alpha[2] > 172 ) {
		return TRUE;
	}

	return FALSE;
}

static void
drawMenuIn( void )
{
	s16		alpha[3];
	int		i;

	for ( i=0 ; i<3 ; i++ ) {
		glrGetOffsetAlpha( &fs_memory[i], &alpha[i], NULL );
	}
	if ( alpha[0] > 0 ) drawCardMenu( (u8)alpha[0], (u8)alpha[1], (u8)alpha[2] );

	for ( i=0 ; i<3 ; i++ ) {
		glrGetOffsetAlpha( &fs_start[i] , &alpha[i], NULL );
	}
	if ( alpha[0] > 0 ) drawStartMenu( (u8)alpha[0], (u8)alpha[1], (u8)alpha[2] );

	for ( i=0 ; i<3 ; i++ ) {
		glrGetOffsetAlpha( &fs_option[i], &alpha[i], NULL );
	}
	if ( alpha[0] > 0 ) drawOptionMenu( (u8)alpha[0], (u8)alpha[1], (u8)alpha[2] );

	for ( i=0 ; i<3 ; i++ ) {
		glrGetOffsetAlpha( &fs_time[i],   &alpha[i], NULL );
	}
	if ( alpha[0] > 0 ) drawTimeMenu( (u8)alpha[0], (u8)alpha[1], (u8)alpha[2] );
}

static void
drawOperationString_pal( u8 alpha )
{
	int i;
	GXColor white = { 255, 255, 255, 255 };
	s16	string_alpha;

	for ( i = 0 ; i < COUNTRY_PAL_MAX_LANGUAGE ; i++ ) {
		glrGetOffsetAlpha( &operation_language_string[ i ], &string_alpha, NULL );
		white.a = (u8)( alpha * string_alpha / 255 );
		glrDrawFadeString( &exmain[0], str_ope[i], lay_ope, &white );
		glrDrawFadeString( &exmain[1], str_ope[i], lay_ope, &white );
		glrDrawFadeString( &exmain[2], str_ope[i], lay_ope, &white );
		glrDrawFadeString( &sentaku,   str_ope[i], lay_ope, &white );
		glrDrawFadeString( &tyousei,   str_ope[i], lay_ope, &white );
		glrDrawFadeString( &cancel,    str_ope[i], lay_ope, &white );
		glrDrawFadeString( &exit_str,  str_ope[i], lay_ope, &white );
		glrDrawFadeString( &kettei,    str_ope[i], lay_ope, &white );
	}
}

static void
drawOperationString(  u8 alpha )
{
	GXColor white = { 255, 255, 255, 255 };

	setGXforglrText();

	white.a = alpha;

#ifdef PAL
	drawOperationString_pal( alpha );
#else
	glrDrawFadeString( &exmain[0], str_ope[operation_language], lay_ope, &white );
	glrDrawFadeString( &exmain[1], str_ope[operation_language], lay_ope, &white );
	glrDrawFadeString( &exmain[2], str_ope[operation_language], lay_ope, &white );
	glrDrawFadeString( &sentaku,   str_ope[operation_language], lay_ope, &white );
	glrDrawFadeString( &tyousei,   str_ope[operation_language], lay_ope, &white );
	glrDrawFadeString( &cancel,    str_ope[operation_language], lay_ope, &white );
	glrDrawFadeString( &exit_str,  str_ope[operation_language], lay_ope, &white );
	glrDrawFadeString( &kettei,    str_ope[operation_language], lay_ope, &white );
#endif
}

static void
drawOperation( u8 alpha )
{
#define LOOP 128
#define STATE_NUM 8
//  MenuState*	mesp = getMenuState;
	s16	stick_anim_alpha = 0;
	GXColor	white       = { 255, 255, 255, 255 };
	GXColor	stick_color = { 255, 255, 255, 255 };
	GXColor	but1_color  = { 255, 255, 255, 255 };
	GXColor	but2_color  = { 255, 255, 255, 255 };
	GXColor	but3_color  = { 255, 255, 255, 255 };

	GXColor bac1_color  = { 150, 150, 150, 255 };
	GXColor bac2_color;//  = { 255,   0, 150, 255 };
	GXColor bac3_color;//  = {   0, 220, 150, 255 };
/*  GXColor bac4_color  = { 150, 150, 150, 255 };
	GXColor bac5_color  = { 255,   0, 150, 255 };
	GXColor bac6_color  = {   0, 220, 150, 255 };*/
	u32		stick_id = 'stk0', i, id;
	s16		state;

	s16		button_base_alpha;
	s16		stick_alpha[2], button_b_alpha[2], button_a_alpha[2];

	bac3_color.r = (u8)mesp->color_button_a_r;
	bac3_color.g = (u8)mesp->color_button_a_g;
	bac3_color.b = (u8)mesp->color_button_a_b;
	bac3_color.a = 255;
	bac2_color.r = (u8)mesp->color_button_b_r;
	bac2_color.g = (u8)mesp->color_button_b_g;
	bac2_color.b = (u8)mesp->color_button_b_b;
	bac2_color.a = 255;
	
	gSet2DCamera();
	setGXforglrPicture();

	glrGetOffsetAlpha( &main[0], &stick_alpha[0]   , NULL );	// 操作時のスティックの描画
	glrGetOffsetAlpha( &main[1], &button_b_alpha[0], NULL );	// 操作時のボタンBの描画
	glrGetOffsetAlpha( &main[2], &button_a_alpha[0], NULL );	// 操作時のボタンAの描画用
	glrGetOffsetAlpha( &main[3], &stick_alpha[1]   , NULL );	// 操作時のスティックの描画
	glrGetOffsetAlpha( &main[4], &button_b_alpha[1], NULL );	// 操作時のボタンBの描画
	glrGetOffsetAlpha( &main[5], &button_a_alpha[1], NULL );	// 操作時のボタンAの描画用

	// ボタン／スティックのバックの描画。
	if ( (anime_frame % 256) > 127 ) button_base_alpha = (s16)( 100 + 155 * ( anime_frame % 128 ) / 127 );
	else 							 button_base_alpha = (s16)( 255 - 155 * ( anime_frame % 128 ) / 127 );
	button_base_alpha = (s16)( button_base_alpha * alpha / 255 );

	id = 'bac1';
	for ( i = 0 ; i < 2 ; i++ ) {
		bac1_color.a = (u8)( button_base_alpha * stick_alpha[i] / 255 );
		bac2_color.a = (u8)( button_base_alpha * button_b_alpha[i] / 255 );
		bac3_color.a = (u8)( button_base_alpha * button_a_alpha[i] / 255 );
		glrDrawPicture( id , lay_ope, &bac1_color );
		glrDrawPicture( id+1, lay_ope, &bac2_color );
		glrDrawPicture( id+2, lay_ope, &bac3_color );
		id += 3;
	}
	for ( i = 0 ; i < 2 ; i++ ) {
		// スティックのアニメーションの描画。
		state = (s16)( ( anime_frame /  LOOP ) % STATE_NUM );
		if ( ( state % 2 ) == 0 ) stick_anim_alpha = (u8)(       ( anime_frame % LOOP ) * 255 / ( LOOP - 1 ) );
		else 					  stick_anim_alpha = (u8)( 255 - ( anime_frame % LOOP ) * 255 / ( LOOP - 1 ) );

		stick_anim_alpha = (u8)( stick_anim_alpha * alpha / 255 );
		// 描画
		stick_color.a = (u8)( 255 - stick_anim_alpha );
		stick_color.a = (u8)( stick_color.a * stick_alpha[i] / 255 );
		glrDrawPicture( stick_id + ( ( (state+1) / 2 + 3 ) % 4 ) + 1, lay_ope, &stick_color );

		stick_color.a = (u8)stick_anim_alpha;
		stick_color.a = (u8)( stick_color.a * stick_alpha[i] / 255 );
		glrDrawPicture( stick_id , lay_ope, &stick_color );
		stick_id += 5;	// stk5にする。
	}

	// 最終の描画。
	white.a = (u8)( white.a * alpha / 255 );
	white.a = (u8)( white.a * fadein_alpha / 255 );

	but2_color.a = (u8)( white.a * button_b_alpha[0] / 255 );
	glrDrawPicture( 'butb', lay_ope, &but2_color );
	glrDrawPicture( 'dot2', lay_ope, &but2_color );
	but2_color.a = (u8)( white.a * button_b_alpha[1] / 255 );
	glrDrawPicture( 'butc', lay_ope, &but2_color );
	glrDrawPicture( 'dot5', lay_ope, &but2_color );
	but3_color.a = (u8)( white.a * button_a_alpha[0] / 255 );
	glrDrawPicture( 'buta', lay_ope, &but3_color );
	glrDrawPicture( 'dot3', lay_ope, &but3_color );
	but3_color.a = (u8)( white.a * button_a_alpha[1] / 255 );
	glrDrawPicture( 'butd', lay_ope, &but3_color );
	glrDrawPicture( 'dot6', lay_ope, &but3_color );

	but1_color.a = (u8)( white.a * stick_alpha[0] / 255 );
	glrDrawPicture( 'dot1', lay_ope, &but1_color );
	but1_color.a = (u8)( white.a * stick_alpha[1] / 255 );
	glrDrawPicture( 'dot4', lay_ope, &but1_color );

	drawOperationString( alpha );

	anime_frame++;
	fadein_alpha+=4;
	if ( fadein_alpha > 255 ) fadein_alpha = 255;

#undef LOOP
#undef STATE_NUM
}

/*-----------------------------------------------------------------------------
  SplashDrawのinMenuDraw()から呼ばれます。
  メニューに入っていたら、2d layoutを描画したり、
  3dキューブの時は、表面を描画したりします。
  -----------------------------------------------------------------------------*/
void
drawMenuMain( u8 alpha )
{
	GXColor white = { 255, 255, 255, 255 };
	SplashState* ssp = inSplashGetState();
	
	// 表面に書いてある説明を書きます。
	drawMenuSurface( alpha );

	// 各メニューに入っているときの描画。
	drawMenuIn();

	// 操作説明を描画します。
	drawOperation( (u8)255 );

	if ( ssp->force_fatalerror ) {
		s16	fade_alpha;
		glrGetOffsetAlpha( &fatal_error, &fade_alpha, NULL );
		gDrawFade( (u8)( fade_alpha / 2 ) );
		gSet2DCamera();
		setGXforglrText();
		glrDrawFadeString( &fatal_error, dem_str, dem_lay, &white );
	}
}
