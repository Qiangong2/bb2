/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: ロゴを表示する部分のスプラッシュ
  
  GAMECUBEのロゴを表示するのに特化した管理
  -----------------------------------------------------------------------------*/
#include <dolphin.h>

#include "gCont.h"
#include "gCamera.h"
#include "gDynamic.h"
#include "gTrans.h"

#include "gModelRender.h"

#include "LogoUpdate.h"
#include "LogoDraw.h"

#ifdef JHOSTIO
#include "IHILogoState.h"
#endif

#include "SplashMain.h"
#include "SplashUpdate.h"
#include "SplashDraw.h"

#include "gMainState.h"

#include "jaudio.h"

/*-----------------------------------------------------------------------------
  コントローラの入力を外部参照する。
  -----------------------------------------------------------------------------*/
extern DIPadStatus	DIPad;

/*-----------------------------------------------------------------------------
  staticモノ
  -----------------------------------------------------------------------------*/
static lgGridArray	sga;
static LogoState*	lsp;

#ifndef JHOSTIO
static LogoState	scs;
#endif

static lgSequence sequence[] =
{
//	{ CS_DIR_PLUS_X, 1,    5, 0 },
//	{ CS_DIR_PLUS_X, 1,    5, 0 },
//	{ CS_DIR_PLUS_X, 1,    5, 0 },
	{ CS_DIR_PLUS_X, 1,    5, 0 },
//	{ CS_DIR_PLUS_X, 1,    5, 0 },

//	{ CS_DIR_PLUS_X, 1, 0xff, 0 },
	{ CS_DIR_PLUS_X, 1, 0xff, 0 },

	{ CS_DIR_MINUS_Z, 1, 0xff, 0 },
	{ CS_DIR_MINUS_Z, 1, 0xff, 0 },
	{ CS_EDGE_TO_1  , 1, 0xff, 0 },
	
	{ CS_DIR_PLUS_X, 2, 0xff, 0 },
	{ CS_DIR_PLUS_X, 2, 0xff, 0 },
	{ CS_DIR_MINUS_Z, 2, 0xff, 0 },
	{ CS_DIR_MINUS_Z, 2, 0xff, 0 },
	{ CS_EDGE_TO_2  , 2, 0xff, 0 },
	
	{ CS_DIR_PLUS_X, 3, 0xff, 0 },
	{ CS_DIR_PLUS_X, 3, 0xff, 0 },
	{ CS_DIR_MINUS_Z, 3, 0xff, 0 },
	{ CS_DIR_MINUS_Z, 3, 0xff, 0 },
	{ CS_DIR_MINUS_X, 3, 0xff, 0 },
	{ CS_DIR_MINUS_X, 3, 0xff, 0 },
//	{ CS_EDGE_TO_0  , 1, 0xff, 0 },
//	{ CS_EDGE_TO_1  , 2, 0xff, 0 },
//	{ CS_DIR_MINUS_Z, 3, 0xff, 0 },
	{ CS_ENLARGE, 3, 0xff, 0 },

	{ CS_DIR_MINUS_Z, 1, 0xff, 0 },
	{ CS_DIR_MINUS_Z, 1,    5, 0 },
	{ CS_DIR_MINUS_Z, 1,    5, 0 },
	{ CS_DIR_MINUS_Z, 1,    5, 0 },
	{ CS_DIR_MINUS_Z, 1,    5, 0 },
	{ CS_DIR_MINUS_Z, 1,    5, 0 },
	{ CS_DIR_MINUS_Z, 1,    5, 0 },
	{ CS_DIR_MINUS_Z, 1,    5, 0 },
	{ CS_DIR_MINUS_Z, 1,    5, 0 },
	{ CS_DIR_MINUS_Z, 1,    5, 0 },
	{ CS_DIR_MINUS_Z, 1,    5, 0 },
	{ CS_DIR_MINUS_Z, 1,    5, 0 },
	{ CS_DIR_MINUS_Z, 1,    5, 0 },
	{ CS_DIR_MINUS_X, 1,    5, 0 },
	{ CS_DIR_MINUS_X, 1,    5, 0 },
	{ CS_DIR_MINUS_X, 1,    5, 0 },
//	{ CS_DIR_MINUS_X, 1,    5, 0 },
//	{ CS_DIR_MINUS_X, 1,    5, 0 },
//	{ CS_DIR_MINUS_X, 1,    5, 0 },
	{ CS_DIR_LAST, 0,    0, 0 },
};

static Mtx	smallCubeMtx;	// 小さいキューブのカレント行列。
static u32	logo_runstate;		// 実行ステート
static u32	pre_logo_runstate;	// 実行ステート

static BOOL sIsInteractive;
static u8 drop_first = TRUE;
static u8 down_first = TRUE;
static u8 sound_first = TRUE;

/*-----------------------------------------------------------------------------
  static function
  -----------------------------------------------------------------------------*/
static void lgEdgeFunction( void );
static void lgMovebySequence( void );
static int  lgCheckSpin( void );
static void lgMovetoNextGrid( void );
static void lgDecGridLife( void );
static void lgControlCubeColor( void );
static u32 HtoRGB( u16 H );

static void lgAnimateMtx( Mtx m );
static void lgTranslateMtx( Mtx m );

/*-----------------------------------------------------------------------------
  グリッドの状態を返します。
  -----------------------------------------------------------------------------*/
lgGridArrayPtr
lgGetGridState( void )
{
	return sga;
}

/*-----------------------------------------------------------------------------
  
  -----------------------------------------------------------------------------*/
LogoState*
lgGetCubeState( void )
{
	return lsp;
}

/*-----------------------------------------------------------------------------
  グリッドの状態を初期化します。
  -----------------------------------------------------------------------------*/
static void
lgInitGridState( void )
{
	int side,x,z;

	for ( side=0 ; side<3 ; side++ ) {
		for ( x=0; x<LG_GRID_MAX_X ; x++ ) {
			for ( z=0; z<LG_GRID_MAX_Z ; z++ ) {
				sga[side][x][z].life  = 0;
				sga[side][x][z].color = 0;
				sga[side][x][z].state = 0;
			}
		}
	}
}

/*-----------------------------------------------------------------------------
  CubeStateの初期化
  最初に戻ったときに、初期化されるものだけ。
  -----------------------------------------------------------------------------*/
void
lgInitPosState( LogoState* cs )
{
	cs->PosX = LG_GRID_CENTER_X ;
	cs->PosZ = LG_GRID_CENTER_Z + 2;
	cs->face =  0;
	cs->nowSequence = 0;
	cs->dir  =  sequence[ cs->nowSequence ].dir;
	cs->gridMergin = 0;

	cs->demoDone = FALSE;

	if ( ( cs->stickChangeColor & 1) == 0 ) {
		cs->cubeColor =
				(150 << 24 | 100 << 16 | 255 << 8 ) & 0xffffff00;
	}

	pre_logo_runstate = logo_runstate = LS_RUN_DROP;
	cs->drop_v = 0;
	cs->drop_a = 2 * cs->drop_y / ( cs->drop_drop_frame * cs->drop_drop_frame );
	cs->drop_now_y = cs->drop_y;

	cs->down_y = cs->jump_max_height;
	cs->down_v = cs->down_start_v;
	cs->down_a = 2 * cs->down_y / ( cs->down_down_frame * cs->down_down_frame ) - cs->down_v / cs->down_down_frame;
	if ( cs->down_a < 0.f ) cs->down_a = 0.f;
	cs->down_now_y = cs->down_y;

	// 現在のフレーム関連
	cs->drop_now_frame = 0;
	cs->down_now_frame = 0;
	cs->nowFrame = 0;
	cs->jump_now_frame = 0;
	cs->logo_mark_now_frame = 0;
	
	cs->ninlogo_frame = 0;

	cs->sound_frame = 0;
	sound_first = TRUE;
}
/*-----------------------------------------------------------------------------
  CubeStateの初期化
  起動一回目だけ、初期化されるもの。
  -----------------------------------------------------------------------------*/
void
lgInitCubeState( LogoState* cs )
{
	cs->endFrame = 0;

	cs->stickChangeColor = 0;

	cs->drop_dont_drop = 0;
	// ＋＋＋＋＋サウンド開始フレーム＋＋＋＋＋
#ifdef PAL
	cs->sound_start_frame1 = 0;
	cs->sound_start_frame2 = 6;
	cs->sound_start_frame3 = 5;
#else
	cs->sound_start_frame1 = 0;
	cs->sound_start_frame2 = 7;
	cs->sound_start_frame3 = 6;
#endif
	// 	＋＋＋＋＋SmallCube登場＋＋＋＋＋ 
	cs->drop_y = 100.f;
#ifdef PAL
	cs->drop_drop_frame = 4;
	cs->drop_stop_frame = 13;
#else
	cs->drop_drop_frame = 5;
	cs->drop_stop_frame = 16;
#endif

	// ＋＋＋＋＋SmallCube転がり＋＋＋＋＋
#ifdef PAL
	cs->animFrame = 8;
	cs->animEdgeFrame = 15;
#else
	cs->animFrame = 10;
	cs->animEdgeFrame = 16;
#endif
	cs->stopFrame = 0;
	cs->cubeAlpha = 255;
	cs->aliveTime = 5;

	//＋＋＋＋＋SmallCubeジャンプ＋＋＋＋＋
	cs->jump_rot_num = 6;
#ifdef PAL
	cs->jump_jump_frame = 15;
	cs->jump_stop_frame = 6;
#else
	cs->jump_jump_frame = 18;
	cs->jump_stop_frame = 7;
#endif
	cs->jump_max_height = 130.f;

	// ＋＋＋＋＋SmallCube落下＋＋＋＋＋
#ifdef PAL
	cs->down_down_frame = 8;
	cs->down_stop_frame = 17;
#else
	cs->down_down_frame = 10;
	cs->down_stop_frame = 20;
#endif
	cs->down_start_v	= 30.f;

	// ＋＋＋＋＋LOGO 出現＋＋＋＋＋
#ifdef PAL
	cs->logo_mark_frame = 33;

	cs->ninlogo_max_frame = 50;
	cs->ninlogo_wait_frame = 4;
#else
	cs->logo_mark_frame = 40;

	cs->ninlogo_max_frame = 60;
	cs->ninlogo_wait_frame = 5;
#endif

	// cubeの大きさ
	cs->lg_cube_size = 54;
	cs->grid_r = 100;//110;
	cs->grid_g = 80;
	cs->grid_b = 190;//255;
	
}

/*-----------------------------------------------------------------------------
  Logo表示の初期化
  -----------------------------------------------------------------------------*/
void
LogoInit( BOOL first )
{
	if ( first ) {
#ifndef JHOSTIO
		lsp = &scs;
		lgInitCubeState( &scs );
#else
		lsp = getIHILogoState();
#endif
	}

	lgInitGridState();
	lgInitPosState( lsp );

	if ( first ) lgDrawInit();

	sIsInteractive = TRUE;
	drop_first = TRUE;
	down_first = TRUE;
//	sound_first = TRUE;
}

/*-----------------------------------------------------------------------------
  違う面に移動するときのグリッドバッファ処理
  -----------------------------------------------------------------------------*/
static void
lgEdgeFunction( void )
{
	s8 next_face = (s8)(lsp->dir - CS_EDGE_TO_0);

	// 0->1 , 1->2 , 2->0方向への面の移動
	if ( ( ( ( next_face + 3 ) - lsp->face ) % 3 ) == 1 ) {
		if ( lsp->PosZ == LG_GRID_CENTER_Z ) {
			lsp->PosZ = lsp->PosX;
			lsp->PosX = LG_GRID_CENTER_X;
			lsp->face = next_face;
		}
	}
	
	// 0->2 , 2->1 , 1->0方向への面の移動
	if ( ( ( ( next_face + 3 ) - lsp->face ) % 3 ) == 2 ) {
		if ( lsp->PosX == LG_GRID_CENTER_X ) {
			lsp->PosX = lsp->PosZ;
			lsp->PosZ = LG_GRID_CENTER_Z;
			lsp->face = next_face;
		}
	}
}


/*-----------------------------------------------------------------------------
  軌跡の生存期間を更新する
  -----------------------------------------------------------------------------*/
static void
lgDecGridLife( void )
{
	s32 face , x, z;

	for ( face = 0 ; face < 3 ; face++ ) {
		for ( x = 0 ; x < LG_GRID_MAX_X ; x++ ) {
			for ( z = 0 ; z < LG_GRID_MAX_Z ; z++ ) {
				if ( sga[face][x][z].state == 2 ) {
					sga[face][x][z].state = 0;
					sga[face][x][z].color = 0;
					sga[face][x][z].life = 0;
				}
				else if ( sga[face][x][z].state == 1 ) {
					sga[face][x][z].state = 0;
				}
				else if ( sga[face][x][z].color != 0 ) {
					if ( sga[face][x][z].life != 0xff ){
						sga[face][x][z].life--;
						if ( sga[face][x][z].life <= 0 ) {
							sga[face][x][z].state = 2;
						}
					}
				}
			}
		}
	}
}

/*-----------------------------------------------------------------------------
  隣のグリッドにdirにあわせて移動する。
  -----------------------------------------------------------------------------*/
static void
lgMovetoNextGrid( void )
{
	switch ( lsp->dir )
	{
	case CS_DIR_PLUS_X:	 lsp->PosX = ( lsp->PosX < LG_GRID_MAX_X )? lsp->PosX+1 : LG_GRID_MAX_X;		break;
	case CS_DIR_PLUS_Z:	 lsp->PosZ = ( lsp->PosZ < LG_GRID_MAX_Z )? lsp->PosZ+1 : LG_GRID_MAX_Z;		break;
	case CS_DIR_MINUS_X: lsp->PosX = ( lsp->PosX > 0 )? lsp->PosX-1 : 0;		break;
	case CS_DIR_MINUS_Z: lsp->PosZ = ( lsp->PosZ > 0 )? lsp->PosZ-1 : 0;		break;
		
	case CS_EDGE_TO_0:
	case CS_EDGE_TO_1:
	case CS_EDGE_TO_2:
		lgEdgeFunction();
		break;
	}

	lgDecGridLife();

	sga[ lsp->face ][ lsp->PosX ][ lsp->PosZ ].life = sequence[ lsp->nowSequence ].life;
	sga[ lsp->face ][ lsp->PosX ][ lsp->PosZ ].color = sequence[ lsp->nowSequence ].color;
	sga[ lsp->face ][ lsp->PosX ][ lsp->PosZ ].state = 1; // fadein
	sga[ lsp->face ][ lsp->PosX ][ lsp->PosZ ].real_color = lsp->cubeColor; // fadein
}








/*-----------------------------------------------------------------------------
  キューブを描画するためのMtx生成処理
  -----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
  [ROTATE]回転アニメーションをつける。
  -----------------------------------------------------------------------------*/
static void
lgAnimateMtx( Mtx m )
{
	Mtx			rot, trans;

	int rtd;
	int rotMax = 16384;// == 90度( (s16)16384 )

	if ( lsp->dir >= CS_EDGE_TO_0 ) {
		// キューブのエッジなら、180度回転する。
		rotMax = 32768;
		rtd = rotMax * lsp->nowFrame / ( lsp->animEdgeFrame - lsp->stopFrame );
	}
	else {
		// 現状では、簡単に回転させてみる。
		// この計算式は、drawのときと共用できるようにしておく。
		rtd = rotMax * lsp->nowFrame / ( lsp->animFrame - lsp->stopFrame );
	}
		
	if ( rtd > rotMax ) rtd = rotMax;

	gGetTranslateRotateMtx( 0,0,0,
							-1 * cube_size / 2,
							cube_size / 2,
							-1 * cube_size / 2,
							trans );
	MTXConcat( m, trans, m );

	gMTXRotS16( rot, 'x', (s16)(rtd) );
	MTXConcat( rot, m , m );
}

/*-----------------------------------------------------------------------------
  [ROTATE]位置に移動させる。
  -----------------------------------------------------------------------------*/
static void
lgTranslateMtx( Mtx m )
{
//	LogoState*	lsp = lgGetCubeState();
	s16			rotS16 = 0;
	Mtx			rot, trans;
	f32			scale = 1.0f;

	//delta = 16384 * lsp->nowFrame / ( lsp->animFrame - lsp->stopFrame );

	switch ( lsp->dir )
	{

	case	CS_DIR_PLUS_X:		rotS16 = -16384;	break;
	case	CS_DIR_PLUS_Z:		rotS16 = 32767;		break;
	case	CS_DIR_MINUS_X:		rotS16 = 16384;		break;
	case	CS_DIR_MINUS_Z:		rotS16 = 0;			break;
	}
	// 単純に回転させる。
	MTXTrans( trans , cube_size / 2 , 0 , cube_size / 2 );
	MTXConcat( trans , m , m );
	gGetTranslateRotateMtx( 0, rotS16, 0,  -1 * cube_size / 2, 0 , -1 * cube_size / 2, trans);
	MTXConcat( trans , m , m );
	
	// それなりの位置に移動させる。
	MTXTrans( trans,
			  -1 * (lsp->PosX - LG_GRID_CENTER_X) * cube_size ,
			  0,
			  -1 * (lsp->PosZ - LG_GRID_CENTER_Z) * cube_size  );

	MTXConcat( trans, m , m );

	// 各faceに移動させる。（CS_ENLARGEはface2限定）
	if ( lsp->face == 1) {
		gGetTranslateRotateMtx( 16384, 0, 16384, 0.f,0.f,0.f ,rot );
		MTXConcat( rot , m , m );
	}
	if ( lsp->face == 2) {
		gGetTranslateRotateMtx( 0, -16384, -16384, 0.f,0.f,0.f ,rot );
		MTXConcat( rot , m , m );
	}
}

/*-----------------------------------------------------------------------------
  [ALL]更新されたSmallCube用のMtxを返す。
  -----------------------------------------------------------------------------*/
MtxPtr
lgGetSmallCubeMtx( void )
{
	return smallCubeMtx;
}

/*-----------------------------------------------------------------------------
  [ROTATE]隣のグリッドにシーケンスどおりに移動する。
  -----------------------------------------------------------------------------*/
static void
lgMovebySequence( void )
{
	lsp->nowSequence++;
	if ( sequence[ lsp->nowSequence ].dir == CS_DIR_LAST ) {
		lsp->demoDone = TRUE;
		lgDecGridLife();
		return;
	}

	lgMovetoNextGrid();

	lsp->dir = sequence[ lsp->nowSequence ].dir;
}

/*-----------------------------------------------------------------------------
  [ROTATE]隣のグリッドに移動するかどうかのチェック。
  -----------------------------------------------------------------------------*/
static int
lgCheckSpin( void )
{
	int round = 16384;	// 90度
	int delta;

	if ( lsp->dir >= CS_EDGE_TO_0 ) {
		// 違う面に映るとき。
		round = 32768;
		delta = round * lsp->nowFrame / ( lsp->animEdgeFrame - lsp->stopFrame );
	}
	else {
		// 同じ面のまま。
		delta = round * lsp->nowFrame / ( lsp->animFrame - lsp->stopFrame );
	}

	if ( delta > round ) {
		// 回転終了
		if ( lsp->endFrame == 0 ) {
			// 回転の終了を保存する。
			lsp->endFrame = lsp->nowFrame;
		}
	}
	
	if ( lsp->endFrame > 0  &&
		 lsp->nowFrame >= lsp->endFrame + lsp->stopFrame )	{
		// 次の面に移動。
		return TRUE;
	}
	
	return FALSE;

}

/*-----------------------------------------------------------------------------
  [ROTATE]転がりデモでのキューブの位置の更新
  -----------------------------------------------------------------------------*/
static u32
lgUpdateRotate( void )
{
	// アップデート。
	if ( lgCheckSpin() )
	{
		lsp->nowFrame = 0;
		lsp->endFrame = 0;
		lgMovebySequence();
	}

	if ( lsp->dir == CS_ENLARGE ) {
		return LS_RUN_UP;
	}

	lsp->nowFrame++;
	return LS_RUN_ROTATE;
}

static void
lgBuildRotateMtx( void )
{
	SplashState* ssp = inSplashGetState();

	Mtx trans;
	
	MTXIdentity( smallCubeMtx );	// カレントマトリックスを単位行列化。
	lgAnimateMtx( smallCubeMtx );	// （次のグリッドに向けて）回転させる。
	lgTranslateMtx( smallCubeMtx );	// （グリッドの指定の位置に移動させる）
	gGetTranslateRotateMtx( 0,0,0,
							cube_size * 3 / 2,cube_size * 3 / 2,cube_size * 3 / 2,
							trans );	// 原点の位置の調整。
	MTXConcat( trans, smallCubeMtx , smallCubeMtx );
	MTXConcat( inSplashGetMtxArray() , smallCubeMtx , smallCubeMtx );	// 土台のキューブの行列にかける。

	switch ( logo_runstate ) {
	case LS_RUN_DROP:
		MTXTrans( trans,
				  0,
				  lsp->drop_now_y,
				  0
				  );
		MTXConcat( trans, smallCubeMtx , smallCubeMtx );
		break;
	}
	
	// 小さいキューブのモデルのアップデート
	setBaseMtx( lgGetSmallCubeModel() , smallCubeMtx ,  (Vec){ssp->scale,ssp->scale,ssp->scale} );
	setViewMtx( lgGetSmallCubeModel() , getCurrentCamera() );
	updateModel( lgGetSmallCubeModel() );
}













/*-----------------------------------------------------------------------------
  [NIN_LOGO]文字ロゴのアップデート処理。
  -----------------------------------------------------------------------------*/
static void
updateNinLogoMtx( void )
{
	Mtx ident;
	f32 frame;
	
	if ( lsp->ninlogo_frame > lsp->ninlogo_wait_frame ) {
		frame = (f32)(lsp->ninlogo_frame - lsp->ninlogo_wait_frame) / lsp->ninlogo_max_frame * 60.f;
	}
	else {
		frame = 0.f;
	}

	MTXIdentity( ident );
	setBaseMtx(  inSplashGetNinLogo(), ident,  (Vec){1.0f, 1.0f, 1.0f } );
	setViewMtx(  inSplashGetNinLogo(), getCurrentCamera() );
	updateAnime( inSplashGetNinLogo(), frame );
}

static BOOL
updateNinLogo( void )
{
	BOOL done = FALSE;

	lsp->ninlogo_frame ++;

	if ( lsp->ninlogo_frame >= lsp->ninlogo_max_frame + lsp->ninlogo_wait_frame ) {
		lsp->ninlogo_frame = (u8)( lsp->ninlogo_max_frame + lsp->ninlogo_wait_frame );
		done = TRUE;
	}

	return done;
}

/*-----------------------------------------------------------------------------
  [DROP]最初に落ちてくるところの位置の更新
  -----------------------------------------------------------------------------*/
static u32
lgUpdateDrop( void )
{
	SplashState* ssp = inSplashGetState();

	if ( lsp->drop_dont_drop ) return LS_RUN_DROP;

	lsp->drop_now_y -= lsp->drop_v;
	if ( lsp->drop_now_y < 0.f ) {
		// 接地した。
		lsp->drop_now_y = 0.f;
		if ( drop_first ) {
			ssp->bound_z_amp = ssp->bound_z_amp_max;
			ssp->bound_rot = 32767;
			drop_first = FALSE;
		}
	}
	lsp->drop_v += lsp->drop_a;
	
	if ( lsp->drop_now_frame > lsp->drop_drop_frame + lsp->drop_stop_frame )
	{
		drop_first = TRUE;
		return LS_RUN_ROTATE;
	}
	
	lsp->drop_now_frame++;
	
	return LS_RUN_DROP;
}

/*-----------------------------------------------------------------------------
  [JUMP]ジャンプする。
  -----------------------------------------------------------------------------*/
static u32
lgUpdateJump( void )
{

	lsp->jump_now_frame++;

	if ( lsp->jump_now_frame > lsp->jump_jump_frame + lsp->jump_stop_frame ) {
		return LS_RUN_DOWN;
	}

	return LS_RUN_UP;
}

static f32
lgJumpGetScale( void )
{
	int	max_frame = lsp->jump_jump_frame;
	int now_frame = lsp->jump_now_frame;
	f32 ret;

	ret = ( 1.0f + 0.5f * now_frame / max_frame );
	if ( ret > 1.5f ) ret = 1.5f;
	return ret;
}

static void
lgJumpAnimMtx( Mtx m )
{
	Mtx			rot;
	f32			scale = lgJumpGetScale();
	int			max_frame = lsp->jump_jump_frame;
	int 		now_frame = lsp->jump_now_frame;
	f32			point;

	int rtd;
	int rotMax = lsp->jump_rot_num * 16384;// == 90度( (s16)16384 )

	rtd = rotMax * now_frame / max_frame;
		
	if ( rtd > rotMax ) rtd = rotMax;
	MTXScale( m , scale, scale, scale );
	point = cube_size / 2 - cube_size * (1.0f + scale ) * lsp->jump_now_frame / (lsp->jump_jump_frame) / 2;
	if ( point < 0 - cube_size * scale / 2 ) point = 0 - cube_size * scale / 2;
	
	gGetTranslateRotateMtx( (s16)rtd,0,0,
							-1 * cube_size * scale/ 2,
							point,
							-1 * cube_size * scale / 2,
							rot );
	MTXConcat( rot , m , m );

	// 各faceに移動させる。（CS_ENLARGEはface2限定）
	gGetTranslateRotateMtx( 0, -16384, -16384, 0.f,0.f,0.f ,rot );
	MTXConcat( rot , m , m );
}

/*-----------------------------------------------------------------------------
  [JUMP][DOWN]マトリックスの生成
  -----------------------------------------------------------------------------*/
static void
lgBuildJumpMtx( void )
{
	SplashState* ssp = inSplashGetState();
	Mtx	trans;
	int	max_frame = lsp->jump_jump_frame;
	int now_frame = lsp->jump_jump_frame - lsp->jump_now_frame;
	f32	height;
	
	MTXIdentity( smallCubeMtx );
	lgJumpAnimMtx( smallCubeMtx );
	
	gGetTranslateRotateMtx( 0,0,0,
							cube_size * 3 / 2,cube_size * 3 / 2,cube_size * 3 / 2,
							trans );	// 大きいキューブの原点に移動させる。
	MTXConcat( trans, smallCubeMtx , smallCubeMtx );
	MTXConcat( inSplashGetMtxArray() , smallCubeMtx , smallCubeMtx );	// 土台のキューブの行列にかける。

	switch ( logo_runstate )
	{
	case LS_RUN_UP:
		if ( now_frame < 0 ) now_frame = 0;
		height = lsp->jump_max_height
				- lsp->jump_max_height * now_frame * now_frame * now_frame * now_frame
						/ ( max_frame * max_frame * max_frame * max_frame );
		if ( height > lsp->jump_max_height ) height = lsp->jump_max_height;
		MTXTrans( trans,
				  0,
				  height,
				  0
				  );
		break;

	case LS_RUN_DOWN:
	case LS_RUN_DONE:
		MTXTrans( trans,
				  0,
				  lsp->down_now_y,
				  0
				  );
		break;
	}
	MTXConcat( trans, smallCubeMtx , smallCubeMtx );

	// 小さいキューブのモデルのアップデート
	setBaseMtx( lgGetSmallCubeModel() , smallCubeMtx ,
				(Vec){ ssp->scale ,ssp->scale ,ssp->scale } );
	setViewMtx( lgGetSmallCubeModel() , getCurrentCamera() );
	updateModel( lgGetSmallCubeModel() );
}

/*-----------------------------------------------------------------------------
  [DOWN]
  -----------------------------------------------------------------------------*/
static u32
lgUpdateDown( void )
{
	SplashState* ssp = inSplashGetState();

	lsp->down_now_y -= lsp->down_v;
	
	if ( lsp->down_now_y < 0.f ) {
		// 接地した。
		lsp->down_now_y = 0.f;
		if ( down_first ) {
			ssp->bound_x_amp = ssp->bound_x_amp_max;
			ssp->bound_rot = 32767;
			// ロゴ確定前の接地したときに振動を加えるときは、ここにパラメータを設定。
			down_first = FALSE;
		}
	}
	lsp->down_v += lsp->down_a;
	
	if ( lsp->down_now_frame > lsp->down_down_frame + lsp->down_stop_frame ) {
		down_first = TRUE;
		return LS_RUN_DONE;
	}
	
	lsp->down_now_frame++;
	
	updateNinLogo();
	
	return LS_RUN_DOWN;
}

static void
lgUpdateDone( void )
{
	SplashState* ssp = inSplashGetState();

	lsp->logo_mark_now_frame++;

	if ( lsp->logo_mark_now_frame > lsp->logo_mark_frame ) {
		lsp->logo_mark_now_frame = lsp->logo_mark_frame;
		lsp->demoDone = TRUE;
	}

	ssp->logo_mark_alpha = (u8)(0xff * lsp->logo_mark_now_frame / lsp->logo_mark_frame);

	if ( !updateNinLogo() ) {
		lsp->demoDone = FALSE;
	}

	if ( ssp->bound_x_amp != 0.f ) {
		lsp->demoDone = FALSE;
	}
}

/*-----------------------------------------------------------------------------
void
LogoUpdateToDoneState( void )
{
	while ( !lsp->demoDone )	{
		lgUpdateCubePos();
	}
	
	return;
}
  -----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
  [MAIN]Mtxの更新。
  -----------------------------------------------------------------------------*/
void
lgUpdateCubeMtx( void )
{
	
	switch ( logo_runstate )
	{
	case LS_RUN_DROP:
		lgBuildRotateMtx();
		break;
	case LS_RUN_ROTATE:
		lgBuildRotateMtx();
		break;
	case LS_RUN_UP:
		lgBuildJumpMtx();
		break;
	case LS_RUN_DOWN:
		lgBuildJumpMtx();
		break;
	case LS_RUN_DONE:
		lgBuildJumpMtx();
		break;
	}
	
	updateNinLogoMtx();
}

static void
playDemoSound( void )
{
	PADStatus	pad[PAD_MAX_CONTROLLERS];

	PADRead( pad );

//	DIReport("%d:%08x", pad[0].err, pad[0].button);
	
	if ( gGetPremiumState() == 2 ) {
		if ( sound_first && lsp->sound_frame > lsp->sound_start_frame2 ) {
			// 歌舞伎。
			Jac_PlaySe( JAC_SE_LOGO2 );
			sound_first = FALSE;
		}
	}
	else if ( gGetPremiumState() == 1 ) {
		if ( sound_first && lsp->sound_frame > lsp->sound_start_frame3 ) {
			// baby.
			Jac_PlaySe( JAC_SE_LOGO3 );
			sound_first = FALSE;
		}
	}
	else {
		if ( sound_first && lsp->sound_frame > lsp->sound_start_frame1 ) {
			Jac_PlaySe( JAC_SE_LOGO1 );
			sound_first = FALSE;
		}
	}

	if ( sound_first ) {
		lsp->sound_frame++;
	}
}

/*-----------------------------------------------------------------------------
  [MAIN]位置の更新
  -----------------------------------------------------------------------------*/
u32
lgUpdateCubePos( BOOL play )
{
	logo_runstate = pre_logo_runstate;	// 1フレーム前の更新をする。

	if ( play ) {
		playDemoSound();
	}
	
	switch ( logo_runstate )
	{
	case LS_RUN_DROP:
		pre_logo_runstate = lgUpdateDrop();
		break;
	case LS_RUN_ROTATE:
		pre_logo_runstate = lgUpdateRotate();
		break;
	case LS_RUN_UP:
		sIsInteractive = FALSE;
		pre_logo_runstate = lgUpdateJump();
		break;
	case LS_RUN_DOWN:
		pre_logo_runstate = lgUpdateDown();
		break;
	case LS_RUN_DONE:
		lgUpdateDone();
		break;
	}

	return cSpRun_Rotate;
}

BOOL
gEnableInteractive( void )
{
	return sIsInteractive;
}



/*-----------------------------------------------------------------------------
  キューブの色を更新
  -----------------------------------------------------------------------------*/
static void
lgControlCubeColor( void )
{
	if ( ( lsp->stickChangeColor & 1 ) == 1 ) {
		lsp->cubeColor = HtoRGB( (u16)DIPad.angle );
	}
}

/*-----------------------------------------------------------------------------
  コントローラで色を更新したいらしいので、その変更用。
  Hで角度を入れると、それなりの色になります。下が赤で、時計と逆周りに65535まで。
  -----------------------------------------------------------------------------*/
static u32
HtoRGB( u16 H )
{
	f32 Hue , f;
	f32 Sat = 1.0f;
	u8 i ,  p , q, t;
	u8 red , green , blue;
	
	Hue = (f32)H * 6.f / 65536.f;
	i = (u8)(H * 6 / 65536);
	f =  Hue -  (f32)i;
	p = (u8)(  255.f * (1.0 -  Sat )  );
	q = (u8)(  255.f * (1.0 -  Sat *  f )  );
	t = (u8)(  255.f * (1.0 -  Sat * ( 1.0 -  f ) )  );

	switch (i)
	{
	case 0:	red = (u8)  255;	green = (u8)  t;		blue = (u8)  p;		break;
	case 1:	red = (u8)  q;		green = (u8)  255;		blue = (u8)  p;		break;
	case 2:	red = (u8)  p;		green = (u8)  255;		blue = (u8)  t;		break;
	case 3:	red = (u8)  p;		green = (u8)  q;		blue = (u8)  255;	break;
	case 4:	red = (u8)  t;		green = (u8)  p;		blue = (u8)  255;	break;
	case 5:	red = (u8)  255;	green = (u8)  p;		blue = (u8)  q;		break;
	}
	
	return (u32)( red << 24 | green << 16 | blue << 8 );
}
