/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: J3DBinary 1.5 Renderer

  Baseサービス
  -----------------------------------------------------------------------------*/
#include <dolphin.h>

#include "gBase.h"
#include "gFont.h"

static OSHeapHandle     currentHeap;

/*-----------------------------------------------------------------------------
  メモリ関連の初期化
  -----------------------------------------------------------------------------*/
void gInitHeap( void )
{
    void    *arenaLoP, *arenaHiP;
#ifdef IPL
    void    *heapLoP;
#endif

    arenaLoP = (void*)OSRoundUp32B  ( OSGetArenaLo() );
    arenaHiP = (void*)OSRoundDown32B( OSGetArenaHi() );
#ifdef IPL
    DIReport2("InitAlloc  :%08x-%08x\n",(void*)OSRoundUp32B( IPL_FONT_TEMP_HEAP_TOP ), arenaHiP );
    heapLoP = OSInitAlloc( (void*)OSRoundUp32B( IPL_ADDHEAP_MEM_LOW ), arenaHiP, 2 ); // 2 heap
    OSSetArenaLo( arenaHiP );

    DIReport2("createHeap :%08x-%08x\n", arenaLoP, arenaHiP );
    OSSetCurrentHeap( currentHeap = OSCreateHeap( arenaLoP, arenaHiP ) );
    DIReport2("addto Heap :%08x-%08x\n", heapLoP, (void*)IPL_ADDHEAP_MEM_HIGH );
    OSAddToHeap( currentHeap, heapLoP, (void*)IPL_ADDHEAP_MEM_HIGH );

//  gDumpHeap();
#else
    arenaLoP = OSInitAlloc( arenaLoP, arenaHiP, 1 ); // 1 heap
    OSSetCurrentHeap( currentHeap = OSCreateHeap( arenaLoP, arenaHiP ) );
    OSSetArenaLo( arenaLoP = arenaHiP );
#endif
}

/*-----------------------------------------------------------------------------
  メモリの状況を確認する。
  -----------------------------------------------------------------------------*/
void
gDumpHeap( void )
{
    OSDumpHeap( currentHeap );
}

/*-----------------------------------------------------------------------------
  メモリアロケータ
  -----------------------------------------------------------------------------*/
void*
gAlloc(u32 size)
{
    void* alloc;

    alloc = OSAllocFromHeap( currentHeap, size );

    if ( alloc == NULL ) {
        DIReport1("Can't alloc memory : %d\n", size );
        OSHalt("");
    }

    return alloc;
}

/*-----------------------------------------------------------------------------
  メモリ開放
  -----------------------------------------------------------------------------*/
void
gFree( void* ptr )
{
    OSFreeToHeap( currentHeap, ptr );
}
