/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: Global Camera処理

  カメラ用の処理
  -----------------------------------------------------------------------------*/
#include <dolphin.h>
#include <math.h>

#include "gCont.h"
#include "gCamera.h"

//#include "gMRStruct.h"
#include "gModelRender.h"

#include "gBase.h"
#include "gMath.h"
#include "gTrans.h"
#include "gRand.h"

//#include "mainparam.h"
#include "gMainState.h"
#include "SplashMain.h"

/*-----------------------------------------------------------------------------
  Global参照
  -----------------------------------------------------------------------------*/
extern DIPadStatus	DIPad;

/*-----------------------------------------------------------------------------
  staticモノ
  -----------------------------------------------------------------------------*/
static Mtx cameraMtx;
static Mtx sc_cameraMtx;
static Mtx s2DCameraMtx;

/*-----------------------------------------------------------------------------
  Prototype
  -----------------------------------------------------------------------------*/
static void updateCameraPos( void );

/*-----------------------------------------------------------------------------
  コントローラの入力
static void
updateCameraPos( void )
{
	MainState* msp  = getMainState();
	// カメラの回転
	if ( DIPad.pst.triggerLeft > 128 ) {
		msp->round -= 10;
		if ( msp->round < -1000 ) msp->round = -1000;
	}

	if ( DIPad.pst.triggerRight > 128 ) {
		msp->round += 10;
		if ( msp->round > 1000 ) msp->round = 1000;
	}

	if ( ( DIPad.pst.triggerLeft  > 128 ) &&
		 ( DIPad.pst.triggerRight > 128 ) ) {
		msp->round = 0;
	}

	// カメラのズーム
	if (DIPad.pst.button & PAD_BUTTON_X ) {
		msp->distCamera += 5;
		if ( msp->distCamera > 5000 ) msp->distCamera = 5000;
	}
	if (DIPad.pst.button & PAD_BUTTON_Y ) {
		msp->distCamera -= 5;
		if ( msp->distCamera < 10 ) msp->distCamera = 10;
	}
}
  -----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
  カメラ行列を作成します。
  -----------------------------------------------------------------------------*/
void
buildCamera( void )
{
	MainState*   msp = getMainState();
	SplashState* ssp = inSplashGetState();
	f32			height;

	Vec     up      = {   0.f,   1.f,   0.f };
	Vec     camLoc	= { 1700.f, 1700.f, 1700.f };
	Vec		objPt	= {   0.f,   0.f,   0.f };

	camLoc.x = 	0.f;
	camLoc.y = 	0.f;
	camLoc.z = 224.0f / tanf( msp->fovy * 3.141592f / 360.f ); // ( fovy / 2 )  *  (3.14 / 180)

    MTXLookAt( sc_cameraMtx, &camLoc, &up, &objPt);
	// カメラの移動
	switch ( getSplashRunstate() )
	{
	case cMenu_Init:
		if ( ssp->key_now_frame <= ssp->logo_change_menu_cube_frame ) {
			height = msp->logo_camera_height -
					( msp->logo_camera_height - msp->menu_camera_height ) * ssp->key_now_frame / ssp->logo_change_menu_cube_frame ;
		} else {
			height = msp->menu_camera_height;
		}
		break;
	case cMenu_Select:
		height = msp->menu_camera_height;
		break;
	default:
		height = msp->logo_camera_height;
		break;
	}

	camLoc.y += height;
	objPt.y  += height;
	
    MTXLookAt( cameraMtx, &camLoc, &up, &objPt);
}

/*-----------------------------------------------------------------------------
  カレントのカメラ行列を返します。
  -----------------------------------------------------------------------------*/
MtxPtr
getCurrentCamera( void )
{
	return cameraMtx;
}
MtxPtr
gGetSmallCubeCamera( void )
{
	return sc_cameraMtx;
}
MtxPtr
gGet2DCamera( void )
{
	return s2DCameraMtx;
}

/*-----------------------------------------------------------------------------
  ライトを生成します。
  -----------------------------------------------------------------------------*/
void
loadLight( void )//Mtx camera )
{
	GXLightObj lo;
	MainState* msp = getMainState();
	Vec 	lp , dir;

	lp  = (Vec){ 400.f, 500.f , 200.f };
	dir = (Vec){ 0.f, 0.f, 0.f };

	MTXMultVec( cameraMtx , &lp , &lp );
	MTXMultVec( cameraMtx , &dir, &dir);
	
	GXInitLightPosv( &lo , &lp );
	GXInitLightDirv( &lo , &dir );
	
	GXInitLightColor( &lo , (GXColor){0xff,0xff,0xff,0xff} );
	GXLoadLightObjImm( &lo , GX_LIGHT0);
}


void
gSet2DCamera( void )
{
	MainState* msp  = getMainState();
	J3DTransformInfo ti = { 1.f, -1.f, 1.f, 0,0,0,0, -292, 224, 0 };
	Vec     up      = {   0.f,   1.f,   0.f };
	Vec     camLoc	= {   0.f,   0.f,   0.f };
	Vec		objPt	= {   0.f,   0.f,   0.f };
	Mtx		scmtx, inv;

	camLoc.z = 224.0f / tanf( msp->fovy * 3.141592f / 360.f ); // ( fovy / 2 )  *  (3.14 / 180)
	MTXLookAt( scmtx, &camLoc, &up, &objPt);
	gGetTransformMtx( &ti , s2DCameraMtx );
	MTXConcat( scmtx, s2DCameraMtx , s2DCameraMtx );
	GXLoadPosMtxImm( s2DCameraMtx , GX_PNMTX0 );
	MTXInverse( s2DCameraMtx, inv );
	MTXTranspose( inv , inv );
	GXLoadNrmMtxImm( inv , GX_PNMTX0 );
	GXSetCurrentMtx( GX_PNMTX0 );
}
