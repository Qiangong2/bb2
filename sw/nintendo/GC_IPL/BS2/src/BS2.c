/*---------------------------------------------------------------------------*
  Project: BS2 skeleton/test program
  File:    BS2.c

  Copyright 1998-2003 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: BS2.c,v $
  Revision 1.1.1.1  2004/06/01 21:17:32  paulm
  GC IPLROM source from Nintendo

    
    12    6/12/03 11:29 Shiki
    To save ROM size, bypass clock calibration in PAL IPL.

    11    2/24/03 17:32 Shiki
    Added log line.

    5     12/07/00 10:19p Shiki
    Fixed HW2 build to call CalibrateClockSpeed().

  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#include <string.h>
#include <time.h>
#include <dolphin.h>
#include <dolphin/os/OSBootInfo.h>
#include <private/ad16.h>
#include <private/flipper.h>
#include <private/OSLoMem.h>
#include <private/OSRtc.h>
#include "BS2Private.h"

#include "gInit.h"
#include "gBase.h"

extern void __VIInit(VITVMode mode);
extern void mainmenu( void );

//extern void AISrcInit(void);

#if defined(HW2) || defined(HW1) || defined(HW1_DRIP)

#ifdef PAL

// To save ROM size, bypass clock calibration in PAL IPL [6/12/2003]
static void CalibrateClockSpeed(void)
{
    __OSBusClock  = 162000000u;
    __OSCoreClock = 486000000u;
}

#else   // PAL

static void CalibrateClockSpeed(void)
{
    BOOL enabled;
    s32 t0;
    s32 t1;
    int i;
    s64 bus;
    s64 core;

    enabled = OSDisableInterrupts();

    // 1st -- just to fill the cache, then throw away
    while (VI_DSP_POS_U_REG_GET_VCT(__VIRegs[VI_DSP_POS_U]) != 1)
        ;
    while (VI_DSP_POS_U_REG_GET_VCT(__VIRegs[VI_DSP_POS_U]) == 1)
        ;
    t0 = (s32) OSGetTick();

    // 2nd
    while (VI_DSP_POS_U_REG_GET_VCT(__VIRegs[VI_DSP_POS_U]) != 1)
        ;
    while (VI_DSP_POS_U_REG_GET_VCT(__VIRegs[VI_DSP_POS_U]) == 1)
        ;
    t0 = (s32) OSGetTick();

    for (i = 0; i < 3; i++)
    {
        while (VI_DSP_POS_U_REG_GET_VCT(__VIRegs[VI_DSP_POS_U]) != 1)
            ;
        while (VI_DSP_POS_U_REG_GET_VCT(__VIRegs[VI_DSP_POS_U]) == 1)
            ;
        t1 = (s32) OSGetTick();
    }

    switch (VIGetTvFormat())
    {
      case VI_NTSC:     // Japan/US version
      case VI_MPAL:     // Brazil version
        bus = (s64) ((t1 - t0) * 4 * 10) * 1000 / 1001;
        break;
      case VI_PAL:      // Europe version
        bus = (s64) ((t1 - t0) * 100) / 3;
        break;
      default:          // Unknown
        bus = 162000000;
        break;
    }

    bus = (bus + 4000) / 10000 * 10000;

    PPCMtpmc1(0);
    PPCMtmmcr0(MMCR0_PMC1_CYCLE);
    t0 = (s32) OSGetTick();
    while ((s32) OSGetTick() - t0 < 1000)
    {
        ;
    }
    PPCMtmmcr0(0);
    core = PPCMfpmc1();
    PPCMtpmc1(0);

    // compute core at 10x, so we can deal with 1 decimal place multipliers
    core = (core + 400) / 1000 * 10000;
    core = (bus * (core / 4000)) / 10;

    OSRestoreInterrupts(enabled);

    __OSBusClock  = (u32) bus;
    __OSCoreClock = (u32) core;

    OSReport("Bus Clock = %u Hz\n", __OSBusClock);
    OSReport("Core Clock = %u Hz\n", __OSCoreClock);
}

#endif  // PAL

#endif // defined(HW2) || defined(HW1) || defined(HW1_DRIP)

void main(void)
{
    BS2Init();      // BS2Init() must be called before OSInit()

    OSInit();
    AD16Init();
    AD16WriteReg(0x08000000);

    DVDInit();
    AD16WriteReg(0x09000000);

    CARDInit();
    AD16WriteReg(0x0A000000);

    // XXX Check SRAM for video format before VIInit()
    gCheckSram();
    // XXX check video format and initialize appropriately.
    // Hardcode NTSC for now.
#if defined(MPAL)
    __VIInit(VI_TVMODE_MPAL_INT);
#elif defined(PAL)
    __VIInit(VI_TVMODE_PAL_INT);
//    __VIInit(VI_TVMODE_NTSC_INT);
#else
    __VIInit(VI_TVMODE_NTSC_INT);
#endif
    VIInit();
    AD16WriteReg(0x0B000000);

#if defined(HW2) || defined(HW1) || defined(HW1_DRIP)
    CalibrateClockSpeed();
#endif // defined(HW2) || defined(HW1) || defined(HW1_DRIP)

    // ここから、OSGetTime()/OSGetTick()が正常に動作するようになります。
    gSetTimeBaseReg();

    gTickBs2();

    // Call PADInit() to stop controller motors.
    PADSetSpec( 5 );
    PADInit();
    AD16WriteReg(0x0C000000);

    DIReport("\n--- EAD PRODUCTION BOOTROM ---\n");
    mainmenu();

    OSHalt("BS2 ERROR >>> SHOULD NEVER REACH HERE");
}
