/*---------------------------------------------------------------------------*
  Project: BS2 state machine
  File:    BS2Mach.c

  Copyright 1998 - 2003 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: BS2Mach.c,v $
  Revision 1.1.1.1  2004/06/01 21:17:32  paulm
  GC IPLROM source from Nintendo

    
    51    6/23/03 11:20 Shiki
    Clean up for non MPAL build.

    50    6/23/03 11:17 Shiki
    Modified BS2StartGame() to check NBA courtside 2002 (GNBE 0-01, MPAL)
    and set r30 to BOOT_GAME_STACK. Note GNBE 0-01, MPAL crashes if r30 is
    zero by a NULL pointer access.

    49    6/12/03 9:28 Shiki
    Modified Run() to set R1 to the fixed stack address (0x81600000). So
    every future IPL passes the same R1 value to the game no matter what.

    48    6/11/03 16:16 Shiki
    Revised Run() not to zero-clear R1 only.

    47    6/02/03 10:54 Shiki
    Revised Run() not to zero-clear R1, R2 and R13 for backward
    compatibility. Some games directly uses these registers before
    initializing it.

    46    3/19/03 17:40 Shiki
    Modified to use __OSFPRInit() instead of BS2FPRInit().

    45    2/26/03 9:33 Shiki
    Moved BS2FPRInit() from BS2Init.c.

    44    2/24/03 17:06 Shiki
    Revised Run() to initialize GRPs and FPRs before entering to the game
    code.

    43    03/02/20 15:30 Hashida
    Removed a warning.

    42    02/09/30 21:33 Hashida
    Added ICInvalidate for apploader.

    41    7/11/02 11:06 Shiki
    Revised to support TDEV dev mode.

    40    11/21/01 19:57 Shiki
    Moved  BOOT_DIAG_* to boot.h

    39    11/14/01 21:19 Shiki
    Modified not to perform country check in development mode.

    38    9/21/01 21:19 Shiki
    Revised CheckCountry() to support OS_BI2_COUNTRYCODE_ALL.

    37    9/14/01 1:29p Hashida
    Added country code check for PAL

    36    7/05/01 3:34p Hashida
    Changed not to use DVDGetCommandBlockStatus becuase it doesn't return
    STATE_COVER_CLOSE any more.

    35    5/31/01 2:34p Shiki
    Reverted BS2StartGame() not to zero-clear IPL heap in development mode.

    34    5/11/01 1:27p Shiki
    Modified BS2StartGame() to always zero-clear IPL heap.

    33    5/10/01 10:28a Shiki
    Modified CheckCountry() to check VIGetTvFormat() for MPAL.

    32    4/05/01 4:54p Hashida
    Added a workaround for MEI's fake-no-disk bug.

    31    3/13/01 3:52p Hashida
    Fixed a bug that audio buffer is not set correctly on a certain
    situation.

    30    01/03/09 15:07 Shiki
    Fixed BS2StartGame() not to clear diag memory space.

    29    01/03/08 15:20 Shiki
    Modified BS2StartGame() to zero-clear all the IPL regions.

    28    3/07/01 6:48p Hashida
    Changed so that when wrong disk (country code check error) happens,
    wait for cover close and retry.

    27    3/02/01 6:17p Hashida
    Added retry

    26    3/02/01 12:08p Hashida
    Changed so that it reports cover open sooner.
    Changed so that it goes to "confirmed cover close" status from "cover
    close" status.

    25    3/02/01 12:13a Hashida
    Implemented restarting statemachine.
    Changed to report cover close after reset immediately.

    24    01/02/27 15:21 Shiki
    Fixed to call PADSync() before calling loaded game application.

    23    2/23/01 4:25p Hashida
    Changed so that cover close check can be done sooner.

    22    2/21/01 3:41a Hashida
    Removed checking cover open after reset because it doesn't work.

    21    2/21/01 2:59a Hashida
    Changed to check cover status before issuing dvdreadid.
    Changed not to use OSGetTime but to use __OSGetSystemTime to check
    reset time.

    20    2/16/01 2:02p Hashida
    Ifdef'ed out country check so that it is compiled only for BOOT_IRD

    19    2/16/01 12:01p Hashida
    Integrated newest IRD bootrom.

    18    2/08/01 5:02p Hashida
    Added banner support

    17    1/15/01 6:51p Hashida
    Added BS2GetBytesLeft function support.

    16    11/10/00 3:21p Hashida
    Audio buffer size now defaults to 10, and size can be specified in disk
    ID.

    15    10/03/00 9:54a Hashida
    Fixed the following bugs:
    - Coverclose isn't reported soon when read disk id failed with
    nodisk/coveropen.
    - When no disk, the driver resets the drive infinitely.

    14    9/21/00 3:02p Hashida
    Fixed a bug that wrong disk was ignored.
    Fixed a bug in self-erase code.

    13    9/21/00 2:06p Hashida
    Added fatal error and country check support.

    12    9/20/00 3:14p Hashida
    Modified so that if disk is exchanged after the previous disk is read,
    it starts reading the new disk.

    11    9/19/00 4:24p Hashida
    Fixed a bug that it never recovered once cover is opened.

    10    9/13/00 3:48p Tian
    Now deletes all BS2 code from memory before running the application.

    9     6/07/00 8:16p Hashida
    Changed so that apploader can show error message.

    8     4/18/00 10:51a Hashida
    Supported SUPPORT_AUDIO_BUF_CFG definition to not issue audio buffer
    config command when it's 0.

    7     4/17/00 1:31p Hashida
    Added audio buffer configuration.
    Fixed a typo.
    DVDLowReset->DVDReset (DVDLowReset causes interrupt mask bits all
    masked).

    6     4/12/00 1:29p Hashida
    Changed caused by DVDDiskID change.

    5     4/10/00 7:23p Hashida
    changed for the new apploader format.

    4     4/07/00 4:13p Shiki
    Clean up.

    3     4/07/00 12:44a Shiki
    Modified BS2DVDCallback() to call BS2Tick() for max performance.

    2     4/06/00 8:35p Shiki
    Prototype of AppInit() is modified.

    1     4/05/00 9:56p Shiki
    Copied from BS2.c

    12    3/23/00 6:54p Hashida
    Added a memory size check for Orca.

    11    3/09/00 6:55p Shiki
    Set console type for ORCA as HW1.

    10    2/15/00 6:04p Shiki
    Revised console type setup.

    9     2/02/00 8:50p Tian
    Added BRINGUP flag.  If set, all VI/splash related things are disabled.

    8     2/02/00 5:10p Tian
    Restructured to draw a splash screen.

    7     2/01/00 6:05p Tian
    Set up bootinfo console type, use cached addresses for printing out
    disk ID (uncached byte-aligned accesses are illegal on minnow)

    6     12/24/99 2:40a Hashida
    Added VIInit.

    5     12/23/99 1:11a Hashida
    Fixed the part where happened to issue system reset.

    4     12/15/99 5:38p Shiki
    Revised to call BS2Init() from main().

    3     12/02/99 2:58p Hashida
    Changed the way to reset DVD. Now we wait for 200ms until DVD gets
    ready.

    2     11/24/99 11:26a Hashida
    Added a code to print magic number in id.
    Added a code to check the magic number (however, currently it's
    commented out because Marlin tools doesn't support the magic number
    yet).
    Removed warnings.

    8     11/15/99 2:24p Tian
    Added BOOT_VERBOSE flag to control Apploader/BS2 verbosity. Also added
    DEBUG_BSS flag.  If nonzero, BS2 will initialize memory to 0xFF.

    7     8/23/99 2:20p Tian
    Moved OSLoMem.h to private.

    6     8/20/99 6:05p Tian
    Fixed bugs - overwriting exception handlers prematurely, using physical
    address for bzero.

    5     8/18/99 1:57p Tian
    Cleanup.  Disabled interrupts prior to jumping to application.  Added
    InitializeMemory() to help locate BSS initialization problems.  Added
    sync/isync to RunApp.

    4     8/11/99 11:17a Tian
    Added bzero.  Initializes boot info, and DiskID is now loaded directly
    into the bootinfo structure.

    3     8/09/99 2:10p Tian
    Cleaned up, added comments.

    2     8/09/99 1:17p Tian
    Moved to boot/bs2.  Also correctly waits for the DVD transaction to
    complete before querying the AppLoader again.

    4     8/05/99 6:02p Tian
    Changed to use correct DVD read routine.

    3     8/05/99 2:27p Tian
    Corrected error message list.

    2     7/28/99 6:18p Tian
    Correctly loads AppLoader as separate entity and determines entry
    points.

    1     7/26/99 11:41a Tian
    Initial checkin
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#include <__ppc_eabi_linker.h>      /* linker-generated symbol declarations */

#include <stdarg.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <dolphin.h>
#include <dolphin/os/OSBootInfo.h>
#include <secure/OSBootInfo2.h>
#include <private/OSLoMem.h>
#include <secure/dvdlow.h>
#include <secure/dvdcb.h>
#include <secure/dvdlayout.h>
#include <secure/boot.h>
#include <private/flipper.h>
#include <private/OSRtc.h>
#include <private/ad16.h>
#include "BS2Private.h"

//#undef BOOT_VERBOSE
//#define BOOT_VERBOSE 1

/*
 * Full initialization
 *
 * Start Dolphin Opening
 *     check start time
 *     check NVRAM for optional start screen
 * Load diskID
 * Wait till ready
 * Configure audio buffer
 * Wait till ready
 * Load apploader header
 * Load apploader
 *     Load BB2
 *     Load application program
 *     Load FST
 *     Load boot structure, arena
 * [ratings check?]
 * Fade splash out
 * Jump to app
 *
 */

// Header structure for apploader's first 32bytes
typedef struct
{
    char        date[16];
    u32         entry;
    u32         size;
    u32         reserved1;
    u32         reserved2;
} ImgHeader;

static BS2State         CurrentState = BS2_START;

static DVDBB2           BB2 ATTRIBUTE_ALIGN(32);
static ImgHeader        ApL ATTRIBUTE_ALIGN(32);

static DVDCommandBlock  Block;

// AppLoader Interface
static AppInitFunc      AppInit;
static AppGetNextFunc   AppGetNext;
static AppGetEntryFunc  AppGetEntry;

static u32*             TotalBytesAddr;
static vu32             TransferredBytes;
static vu32             TransferringBytes;
static volatile BOOL    Transferring = FALSE;

// Banner support
static volatile void*   BannerBufferAddr = NULL;
static vu32             BannerBufferLength;
static volatile BOOL    BannerAvailable = FALSE;
#define BANNER_FILENAME         "/opening.bnr"

extern void __DVDFSInit(void);

// static functions
static void BS2DVDLowResetCoverCallback(u32 intType);
static void BS2DVDLowCallback(u32 intType);

#if BOOT_VERBOSE
static char* BS2StatusNames[] =
{
    "Start",
    "Wait DVD",
    "Wake DVD",
    "Load disk id",
    "Wait for disk id",
    "Confirmed cover closed",
    "Configure audio buffer",
    "Wait audio buffer",
    "Load Apploader Header",
    "Wait Apploader Header",
    "Load Apploader",
    "Wait Apploader",
    "Drive Apploader",
    "Wait Apploader current load",
    "Load Banner",
    "Wait Banner",
    "Run App",

    "Disk cover locked",
    "No disk",
    "Disk cover opened",
    "Stop motor",
    "Wait stop motor",
    "Not a game disk",
    "Fatal error occurred",

    "Retry",

    "Cancelling",
};
#endif  // BOOT_VERBOSE

#define CONSOLE_COUNTRY_JAPAN                2
#define CONSOLE_COUNTRY_US                   0

/*---------------------------------------------------------------------------*
  Name:         BS2Report()

  Description:  Outputs a formatted message into the output port

  Arguments:    msg         pointer to a null-terminated string that contains
                            the format specifications
                ...         optional arguments

  Returns:      None.
 *---------------------------------------------------------------------------*/
void BS2Report(char* msg, ...)
{
#if BOOT_VERBOSE
    va_list marker;

    va_start(marker, msg);
    vprintf(msg, marker);
    va_end(marker);
#else
    #pragma unused(msg)
#endif
}

/*---------------------------------------------------------------------------*
  Name:         BS2DVDLowCallback

  Description:  Low callback function used for DVDLowWaitCoverClose

  Arguments:    intType         interrupt type. passed by interrupt handler

  Returns:      None.
 *---------------------------------------------------------------------------*/
static void BS2DVDLowCallback(u32 intType)
{
#pragma unused (intType)
    // mask cvr int
    __DIRegs[DI_CVR_IDX] = 0;

    CurrentState = BS2_COVER_CLOSED;
    // Do not call DVDReset. If we call DVDReset here, it can happen that
    // application gets booted with "ID not read" as the drive's status,
    // which is unexpected.
}

/*---------------------------------------------------------------------------*
  Name:         BS2DVDLowResetCoverCallback

  Description:  Low callback function used for reset cover int

  Arguments:    intType         interrupt type. should be DVD_INTTYPE_CVR

  Returns:      None.
 *---------------------------------------------------------------------------*/
static void BS2DVDLowResetCoverCallback(u32 intType)
{
#pragma unused(intType)
    ASSERTMSG(intType == DVD_INTTYPE_CVR,
              "BS2: Reset cover callback didn't receive cover int");
    ASSERTMSG1((CurrentState == BS2_LOAD_DISKID) || (CurrentState == BS2_WAIT_DISKID),
               "BS2: reset cover int occurred while the state is not BS2_LOAD_DISKID or BS2_WAIT_DISKID(CurrState = %d)",
               CurrentState);

    CurrentState = BS2_CONFIRMED_COVER_CLOSED;
}

/*---------------------------------------------------------------------------*
  Name:         BS2DVDCallback

  Description:  Callback used for all calls to DVDReadAbsAsyncForBS.
                Sets the global "Done" flag if result is all right.


  Arguments:    result      same as DVDGetDriveStatus()
                block       irrelevant since we only have at most one
                            outstanding read at any time.

  Returns:      None.
 *---------------------------------------------------------------------------*/
static void BS2DVDCallback(s32 result, DVDCommandBlock* block)
{
#pragma unused (block)

    if (Transferring)
    {
        Transferring = FALSE;
        TransferredBytes += TransferringBytes;

        if (result < 0)
            *TotalBytesAddr = 0;
    }

    // No need to wait for reset cover int anymore.
    DVDLowSetResetCoverCallback(NULL);

    // For release version, we call BS2Tick in callback to gain some
    // performance. For debug version, don't call because BS2Tick calls
    // printf.
#ifndef _DEBUG
    BS2Tick();
#endif
}


/*---------------------------------------------------------------------------*
  Name:         BS2RestartStateMachine

  Description:  Restart the state machine

  Arguments:    None.

  Returns:      None.
 *---------------------------------------------------------------------------*/
void BS2RestartStateMachine(void)
{
    BOOL        enabled;

    if ( (CurrentState == BS2_START) ||
         (CurrentState == BS2_WAIT_DVD) ||
         (CurrentState == BS2_WAKE_DVD) ||
         (CurrentState == BS2_LOAD_DISKID) ||
         (CurrentState == BS2_WAIT_DISKID) ||
         (CurrentState == BS2_CONFIRMED_COVER_CLOSED) )
    {
        // no need to do anything
        return;
    }

    enabled = OSDisableInterrupts();

    *TotalBytesAddr = 0;
    TransferredBytes = 0;

    CurrentState = BS2_CANCELING;
    DVDCancelAsync(&Block, NULL);

    OSRestoreInterrupts(enabled);
}

/*---------------------------------------------------------------------------*
  Name:         BS2SetBannerBuffer

  Description:  Notify the location of the buffer for banner

  Arguments:    addr            start address of the buffer
                length          length of the buffer

  Returns:      None
 *---------------------------------------------------------------------------*/
void BS2SetBannerBuffer(void* addr, u32 length)
{
    ASSERTMSG(OFFSET(addr, DVD_ALIGN_ADDR) == 0,
              "BS2SetBannerBuffer(): address is not 32B aligned");
    BannerBufferAddr = addr;
    BannerBufferLength = length;
}

/*---------------------------------------------------------------------------*
  Name:         BS2IsBannerAvailable

  Description:  Tell if banner is available

  Arguments:    None

  Returns:      TRUE if banner is available, FALSE if not
 *---------------------------------------------------------------------------*/
BOOL BS2IsBannerAvailable(void)
{
    return BannerAvailable;
}

/*---------------------------------------------------------------------------*
  Name:         BS2GetBytesLeft

  Description:  Get how many bytes left

  Arguments:    None

  Returns:      bytes to read, -1 if there's no information
 *---------------------------------------------------------------------------*/
s32 BS2GetBytesLeft(void)
{
    u32         transferred;
    BOOL        enabled;

    enabled = OSDisableInterrupts();

    if (*TotalBytesAddr == 0)
    {
        OSRestoreInterrupts(enabled);
        return -1;
    }

    transferred = TransferredBytes +
        ((Transferring)? (TransferringBytes - __DIRegs[DI_LENGTH_IDX]) : 0);

//    ASSERTMSG2(*totalBytes >= transferred, "total, trans'ed %d, %d\n", *totalBytes,
//               transferred);

    OSRestoreInterrupts(enabled);

    return (s32)(*TotalBytesAddr - transferred);
}

/*---------------------------------------------------------------------------*
  Name:         CheckCountry

  Description:  Check if the country code in disk matches the one in console

  Arguments:    None

  Returns:      TRUE if it matches, FALSE if not
 *---------------------------------------------------------------------------*/
static BOOL CheckCountry(void)
{
    void** const BI2AddrHolder = (void**) (OS_BASE_CACHED + OS_BOOTINFO2_ADDR);
    u32 diskCountryCode;

#ifndef BOOT_IRD
    // Since boot-devkit is not ready to check this condition,
    // only for BOOT_IRD for now.
    return TRUE;
#endif  // BOOT_IRD

    if (OSGetConsoleType() & 0xffff0000 /* OS_CONSOLE_DEVELOPMENT */)
    {
        return TRUE;
    }

    diskCountryCode = *((u32*) ((u32) *BI2AddrHolder + OS_BI2_COUNTRYCODE_OFFSET));
    switch (VIGetTvFormat())
    {
      case VI_NTSC:     // Japan/US version
        if (__VIRegs[VI_DTVSTATUS] & 0x02)
        {
            // Japan
            if (diskCountryCode == OS_BI2_COUNTRYCODE_JP)
            {
                return TRUE;
            }
        }
        else
        {
            // US
            if (diskCountryCode == OS_BI2_COUNTRYCODE_US)
            {
                return TRUE;
            }
        }
        break;
      case VI_PAL:      // Europe version
        if (diskCountryCode == OS_BI2_COUNTRYCODE_EU)
        {
            return TRUE;
        }
        break;
      case VI_MPAL:     // Brazil version
        if (diskCountryCode == OS_BI2_COUNTRYCODE_US)
        {
            return TRUE;
        }
        break;
      default:          // Unknown
        return FALSE;
        break;
    }

    if (diskCountryCode == OS_BI2_COUNTRYCODE_ALL && AD16Probe())
    {
        return TRUE;
    }

    return FALSE;
}

/*---------------------------------------------------------------------------*
  Name:         Run

  Description:  Performs a far-jump to the specified location.
                To be safe, the interrupts are disabled and the I-Cache is
                invalidated one last time.

  Arguments:    entryPoint      destination address to start the application
                start           start address for deletion
                numblocks       number of 32B blocks to clear

  Returns:      DOES NOT RETURN
 *---------------------------------------------------------------------------*/

#define BOOT_GAME_STACK     0x81600000  // > BOOT_BS2_BASE + 1769216 - 2048 - 32 + 0x10abac

static asm void
Run
(
    register void*  entryPoint, // r3
    register u32    start,      // r4
    register u32    numblocks,  // r5
    register u32    patch       // r6
)
{
    nofralloc

    mtctr   numblocks
    mtlr    entryPoint
    li      r0, 0x0
    li      r2, 0x0
    li      r3, 0x0
    li      r5, 0x0

    li      r7, 0x0
    li      r8, 0x0
    li      r9, 0x0
    li      r10, 0x0
    li      r11, 0x0
    li      r12, 0x0
    li      r13, 0x0
    li      r14, 0x0
    li      r15, 0x0
    li      r16, 0x0
    li      r17, 0x0
    li      r18, 0x0
    li      r19, 0x0
    li      r20, 0x0
    li      r21, 0x0
    li      r22, 0x0
    li      r23, 0x0
    li      r24, 0x0
    li      r25, 0x0
    li      r26, 0x0
    li      r27, 0x0
    li      r28, 0x0
    li      r29, 0x0
    li      r30, 0x0
    li      r31, 0x0

    lis     r1, BOOT_GAME_STACK@h
    ori     r1, r1, BOOT_GAME_STACK@l

#ifdef MPAL
_patch1:    // GNBE 0-01 (MPAL) crashs if r30 is zero
    cmpwi   patch, 1
    bne     _patch2
    mr      r30, r1
_patch2:
#endif  // MPAL

    li      patch, 0x0          // set r6 to zero

    // MUST touch this code into cache
    b _touchme                  // 0
_loop:
    dcbz    start, r0           // 1
    dcbf    start, r0           // 2
    addi    start, start, 32    // 3
    bdnz    _loop               // 4
    b       _aftertouchme       // 5

_aftertouchme:
    li      r4, 0x0             // 6
    blr                         // 7

_touchme:
    b       _loop
    // NOT REACHED HERE
}

/*---------------------------------------------------------------------------*
  Name:         BS2StartGame

  Description:  Jump to the entry point of the loaded game application.

  Arguments:    None.

  Returns:      DOES NOT RETURN
 *---------------------------------------------------------------------------*/

#define BOOT_IPL_BASE   0x80700000  // IPL heap base address

extern void __OSStopAudioSystem(void);

#define PATCH_NONE          0
#define PATCH_GNBE_0_01     1

#ifdef  MPAL

static DVDDiskID DiskID_GNBE_0_01 =
{
    'G', 'N', 'B', 'E',
    '0', '1',
    0,
    1
};

#endif  // MPAL

void BS2StartGame(void)
{
    void* entry;
    u32   patch = PATCH_NONE;

    ASSERT(CurrentState == BS2_RUN_APP);

    while (!PADSync())
    {
        ;
    }

    OSDisableInterrupts();
    __OSStopAudioSystem();

#ifdef  MPAL
{
    DVDDiskID* diskID;

    diskID = DVDGetCurrentDiskID();
    if (memcmp(diskID, &DiskID_GNBE_0_01,
               offsetof(DVDDiskID, streaming)) == 0)
    {
        patch = PATCH_GNBE_0_01;
    }
#ifdef  _DEBUG
    OSReport("\ndiskID: %4.4s %2.2s %d %d\n",
             diskID->gameName,
             diskID->company,
             diskID->diskNumber,
             diskID->gameVersion);
#endif  // _DEBUG
}
#endif  // MPAL

    if (!(OSGetConsoleType() & 0xffff0000 /* OS_CONSOLE_DEVELOPMENT */))
    {
        memset((void*) BOOT_IPL_BASE, 0, BOOT_DIAG_BASE - BOOT_IPL_BASE);
        memset((void*) BOOT_DIAG_END, 0, BOOT_APPLOADER_BASE - BOOT_DIAG_END);
    }

    entry = AppGetEntry();

    __OSFPRInit();

    // The following code prevents the application sneaking IPL code
    // from the I-cache.
    ICFlashInvalidate();
    __sync();
    __isync();

    Run(entry, BOOT_BS2_BASE, BS2_MAX_SIZE >> 5, patch);
    // NOT REACHED HERE
}

/*---------------------------------------------------------------------------*
  Name:         WakeUpDVD

  Description:  Turn on DVD. On power on, DVD is in "reset" state so we need
                to wake up DVD. From HW restriction, we cannot wake it up
                until DVD_POWERON_TICKS cycles passes.

  Arguments:    None.

  Returns:      None.
 *---------------------------------------------------------------------------*/
static void WakeUpDVD(void)
{
    // Use DVDLowReset because it takes care of CVRINT well.
    DVDLowReset();
}

/*---------------------------------------------------------------------------*
  Name:         CheckDVDCommandStatus

  Description:  Checks DVD command status and set the CurrentState if necessary

  Arguments:    command block

  Returns:      TRUE if the previous transaction completed successfully
 *---------------------------------------------------------------------------*/
static BOOL CheckDVDCommandStatus(DVDCommandBlock* block)
{
    switch (block->state)
    {
      case DVD_STATE_END:
        return TRUE;
        // NOT REACHED HERE

      case DVD_STATE_COVER_CLOSED:
        CurrentState = BS2_COVER_CLOSED;
        break;
      case DVD_STATE_NO_DISK:
        CurrentState = BS2_NO_DISK;
        break;
      case DVD_STATE_COVER_OPEN:
        CurrentState = BS2_COVER_OPEN;
        break;
      case DVD_STATE_FATAL_ERROR:
        CurrentState = BS2_FATAL_ERROR;
        break;
      case DVD_STATE_RETRY:
        CurrentState = BS2_RETRY;
        break;
      case DVD_STATE_CANCELED:
        if (DVDResetRequired())
            CurrentState = BS2_WAKE_DVD;
        else
            CurrentState = BS2_LOAD_DISKID;
        break;
      case DVD_STATE_MOTOR_STOPPED:
        // this happens only when DVDChangeDiskForBS is called after
        // "wrong disk" is detected
        CurrentState = BS2_WRONG_DISK;
        break;
      default:
        // Keep the current state
        break;
    }
    return FALSE;
}

/*---------------------------------------------------------------------------*
  Name:         BS2Tick

  Description:  Execute the BS2 state machine.  Called from Splash screen main
                loop.

  Arguments:    None.

  Returns:      BS2 state
 *---------------------------------------------------------------------------*/
BS2State BS2Tick(void)
{
    DVDDiskID*             diskId;
    AppInterfaceFunc       appGetInterface;
    void*                  addr;
    u32                    length;
    u32                    offset;
    BOOL                   enabled;
    DVDFileInfo            fileInfo;
    s32                    entrynum;
#if 0

    static OSTime          startT, endT;
    static u32             firstTime;
#endif
    static u32             first;
    u32                    coverStatus;
    static BOOL            startLoadDiskId = FALSE;
    static OSTime          startLoadDiskIdTime;

    enabled = OSDisableInterrupts();

#if BOOT_VERBOSE
    ASSERT(CurrentState < BS2_MAX_STATE);
    if (CurrentState < BS2_MAX_STATE)
        BS2Report("[%s]\n", BS2StatusNames[CurrentState]);
#endif

    switch (CurrentState)
    {
      case BS2_START:
        // No need to get the start time of BS2. We check how long it passed
        // after CPU reset (tb starts from 0). We need to make sure

        // DVD_POWERON_TICKS passed before DVD reset register is turned off.
        TotalBytesAddr = OSPhysicalToCached(OS_BOOT_TOTAL_BYTES);

#if 0
        firstTime = 1;
        startT = __OSGetSystemTime();
#endif

        // XXX Check NVRAM for optional boot
        ;

        CurrentState = BS2_WAIT_DVD;
        // FALL THROUGH

      case BS2_WAIT_DVD:
        if (__OSGetSystemTime() < DVD_POWERON_TICKS)
        {
            break;  // Keep the current state

        }
        CurrentState = BS2_WAKE_DVD;
        // FALL THROUGH

      case BS2_WAKE_DVD:
        // Workaround for MEI drive bug
        if (startLoadDiskId == FALSE)
        {
            *TotalBytesAddr = 0;
            TransferredBytes = 0;
            first = 1;
            DVDLowSetResetCoverCallback(NULL);
            DVDReset();
            startLoadDiskId = TRUE;
            startLoadDiskIdTime = __OSGetSystemTime();
            break;
        }
        else if (__OSGetSystemTime() - startLoadDiskIdTime
                 < OSMillisecondsToTicks((OSTime) 1150))  // Wait for 1150 msec
        {
            break;
        }
        startLoadDiskId = FALSE;
        DVDLowSetResetCoverCallback(BS2DVDLowResetCoverCallback);
        DVDReset();

        CurrentState = BS2_LOAD_DISKID;
        // FALL THROUGH

      case BS2_LOAD_DISKID:

        DVDReadDiskID(&Block, OSPhysicalToCached(OS_BOOTINFO_ADDR), BS2DVDCallback);
        CurrentState = BS2_WAIT_DISKID;
        break;

      case BS2_WAIT_DISKID:
      case BS2_CONFIRMED_COVER_CLOSED:
#if 0
        if (CurrentState == BS2_CONFIRMED_COVER_CLOSED)
        {
            if (firstTime)
            {
                endT = __OSGetSystemTime();
                OSReport("It took %llu ticks(%llu us) to make sure cover is closed\n",
                         endT - startT, OSTicksToMicroseconds(endT - startT));
                firstTime = 0;
            }
        }
#endif
        if (!CheckDVDCommandStatus(&Block))
        {
            break;
        }


        diskId = (DVDDiskID*) OSPhysicalToCached(OS_BOOTINFO_ADDR);

        // Check disk format
        if (*(u32*)((u8*)diskId + DVD_MAGIC_OFF) != DVD_MAGIC)
        {
            CurrentState = BS2_STOP_MOTOR;
            break;
        }
#if SUPPORT_AUDIO_BUF_CFG
        CurrentState = BS2_CONFIG_AUDIO_BUFFER;
        // FALL THROUGH

      case BS2_CONFIG_AUDIO_BUFFER:
        diskId = (DVDDiskID*) OSPhysicalToCached(OS_BOOTINFO_ADDR);

        // Set DVD audio mode
        ASSERT((diskId->streaming == 0) || (diskId->streaming == 1));

        // if this is not the first time (i.e. restarted), do not
        // issue audio buffer config command because the drive cannot
        // accept this command more than once
        if (first == 0)
        {
            CurrentState = BS2_LOAD_APPLOADER_HEADER;
            break;
        }

        first = 0;
        if (diskId->streaming)
            __DVDAudioBufferConfig(&Block, (u32)1,
                                   (u32)( (diskId->streamingBufSize)? diskId->streamingBufSize : DVD_AUDIO_BUFFER_SIZE ),
                                   BS2DVDCallback);
        else
            __DVDAudioBufferConfig(&Block, (u32)0, (u32)0, BS2DVDCallback);

        CurrentState = BS2_WAIT_AUDIO_BUFFER;
        break;

      case BS2_WAIT_AUDIO_BUFFER:
        if (!CheckDVDCommandStatus(&Block))
        {
            break;
        }
#endif
        CurrentState = BS2_LOAD_APPLOADER_HEADER;
        // FALL THROUGH

      case BS2_LOAD_APPLOADER_HEADER:
        DVDReadAbsAsyncForBS(&Block, &ApL,
                             OSRoundUp32B(sizeof(ImgHeader)),
                             (s32) DVDLAYOUT_APPLOADER_POSITION,
                             BS2DVDCallback);
        CurrentState = BS2_WAIT_APPLOADER_HEADER;
        break;

      case BS2_WAIT_APPLOADER_HEADER:
        if (!CheckDVDCommandStatus(&Block))
        {
            break;
        }

        BS2Report("  appLoaderLength ...... 0x%x\n", ApL.size);
        BS2Report("  appLoaderFunc1  ...... 0x%x\n", ApL.entry);

        CurrentState = BS2_LOAD_APPLOADER;
        // FALL THROUGH

      case BS2_LOAD_APPLOADER:
        DVDReadAbsAsyncForBS(&Block, (void*) BOOT_APPLOADER_BASE,
                             (s32) OSRoundUp32B(ApL.size),
                             (s32) DVDLAYOUT_APPLOADER_POSITION + sizeof(ImgHeader),
                             BS2DVDCallback);
        CurrentState = BS2_WAIT_APPLOADER;
        break;

      case BS2_WAIT_APPLOADER:
        if (!CheckDVDCommandStatus(&Block))
        {
            break;
        }

        ICInvalidateRange((void*)BOOT_APPLOADER_BASE, OSRoundUp32B(ApL.size));

        appGetInterface = (AppInterfaceFunc) ApL.entry;
        // Obtain interface entry points to apploader.
        appGetInterface(&AppInit, &AppGetNext, &AppGetEntry);
        // Initialize the apploader
        // Pass OSReport instead of BS2Report so that Apploader can show message
        // even when BOOT_VERBOSE=0.
        AppInit(OSReport);
        BS2Report("\nApploader Initialized\n");

        CurrentState = BS2_DRIVE_APPLOADER;
        // FALL THROUGH

      case BS2_DRIVE_APPLOADER:
        // Drive the apploader until nothing left to be initialized
        // Currently the apploader is responsible for data cache management
        if (AppGetNext(&addr, &length, &offset))
        {
            BS2Report("Addr [0x%x] length [0x%x] offset [0x%x]\n",
                     addr, length, offset);

            {
                BOOL        enabled;

                enabled = OSDisableInterrupts();

                DVDReadAbsAsyncForBS(&Block, addr, (s32) length, (s32) offset,
                                     BS2DVDCallback);
                if (*TotalBytesAddr)
                {
                    Transferring = TRUE;
                    TransferringBytes = length;
                }

                OSRestoreInterrupts(enabled);
            }

            CurrentState = BS2_WAIT_APPLOADER_LOAD;
        }
        else
        {
            if (!CheckCountry())
            {
                // Failed country code check
                CurrentState = BS2_STOP_MOTOR;
                break;
            }

            CurrentState = BS2_LOAD_BANNER;
        }
        break;

      case BS2_WAIT_APPLOADER_LOAD:
        if (CheckDVDCommandStatus(&Block))
        {
            CurrentState = BS2_DRIVE_APPLOADER;
        }
        break;

      case BS2_LOAD_BANNER:
        __DVDFSInit();
        entrynum = DVDConvertPathToEntrynum(BANNER_FILENAME);
        if (entrynum >= 0)
        {
            // Found it!
            DVDFastOpen(entrynum, &fileInfo);
            if (BannerBufferAddr == NULL)
                OSPanic(__FILE__, __LINE__,
                        "BS2 ERROR >>> Banner buffer address is not set");
            if (BannerBufferLength < fileInfo.length)
                OSPanic(__FILE__, __LINE__,
                        "BS2 ERROR >>> Banner buffer length (0x%08x) is not enough",
                        BannerBufferLength);
            if (fileInfo.length)
            {
                DVDReadAbsAsyncForBS(&Block, (void*)BannerBufferAddr,
                                     (s32)OSRoundUp32B(fileInfo.length),
                                     (s32)fileInfo.startAddr, BS2DVDCallback);
                CurrentState = BS2_WAIT_BANNER;
                break;
            }
        }

        // Not found or length of the banner was 0
        BannerAvailable = FALSE;
        // Now ready to start the game!
        CurrentState = BS2_RUN_APP;
        DVDLowWaitCoverClose(BS2DVDLowCallback);

        break;

      case BS2_WAIT_BANNER:
        if (CheckDVDCommandStatus(&Block))
        {
            BannerAvailable = TRUE;
            // Now ready to start the game!
            CurrentState = BS2_RUN_APP;
            DVDLowWaitCoverClose(BS2DVDLowCallback);
        }
        break;

      case BS2_RUN_APP:
        // Keep the current state until BS2StartGame() is called
        break;

      case BS2_COVER_CLOSED:
        // All the DVD functions used in BS2 (read disk id, audio buffer
        // config and read for BS) complete their error handling sequence
        // when coverclose is detected. (so its safe to issue next command)
        BannerAvailable = FALSE;
        CurrentState = BS2_WAKE_DVD;
        break;

      case BS2_NO_DISK:
      case BS2_COVER_OPEN:
      case BS2_RETRY:
        // Call CheckDVDCommandStatus() to update status (the state machine
        // goes to coverclose state automatically since now we are checking
        // command status.
#if 0
        if (firstTime)
        {
            endT = __OSGetSystemTime();
            OSReport("It took %llu us to make sure %s\n",
                     OSTicksToMicroseconds(endT - startT),
                     (CurrentState == BS2_NO_DISK)? "no disk" : "cover open");
            firstTime = 0;
        }
#endif
        DVDLowSetResetCoverCallback(NULL);
        BannerAvailable = FALSE;
        CheckDVDCommandStatus(&Block);
        break;

      case BS2_STOP_MOTOR:
        BannerAvailable = FALSE;
        DVDChangeDiskAsyncForBS(&Block, BS2DVDCallback);
        CurrentState = BS2_WAIT_STOP_MOTOR;
        // FALLTHROUGH

      case BS2_WAIT_STOP_MOTOR:
      case BS2_WRONG_DISK:
        // When DVDChangeDiskAsyncForBS ends, state will become cover close.
        CheckDVDCommandStatus(&Block);
        break;

      case BS2_FATAL_ERROR:
        BannerAvailable = FALSE;
        break;

      case BS2_CANCELING:
        if(CheckDVDCommandStatus(&Block))
        {
            CurrentState = BS2_LOAD_DISKID;
            break;
        }
        break;

      default:
        OSHalt("BS2 ERROR >>> UNKNOWN STATE");
    }

    OSRestoreInterrupts(enabled);

    coverStatus = DVDLowGetCoverStatus();
    if (coverStatus == DVD_COVER_OPEN)
    {
#if BOOT_VERBOSE
        BS2Report(" ----> report as cover open\n");
#endif
        return BS2_COVER_OPEN;
    }

    return CurrentState;
}
