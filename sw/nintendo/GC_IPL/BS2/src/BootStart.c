/***************************************************************************/
/*

FILE
    __start.c

DESCRIPTION

    Metrowerks PowerPC EABI Runtime Initialization.  This file provides
    the first entry point, "__start" which is used by the linker as a
    program entry point.  It initializes the program state (registers,
    stack pointer, data pointers) and copies data images from ROM to RAM
    as necessary.


    void __start(void)

        Main entry point for the PPC EABI program.  It will call main().

    This file allows for board-level initialization.  Define a function
    called __init_hardware().

    This file allows for user-level initialization before main().  Define
    a function called __init_user().  If program is in C++, __init_user
    needs to initialize exceptions and static constructors.


REFERENCES

    [EABI95]    PowerPC Embedded Application Binary Interface,
                32-bit Implementation.  Version 1.0, Initial Release,
                1/10/95.  Stephen Sobek, Motorola, and Kevin Burke, IBM.

NOTES

    The .PPC.EMB.sdata0 and .PPC.EMB.sbss section pointers are not
    currently supported

    Defined, but unused linker-generated symbols:
        _stack_size     Defined but not used yet.  For bounds checking.
        _heap_size      Defined but not used yet.  For user heap.
        _ftext          Defined but not used.  Start of text section.
        _etext          Defined but not used.  Address after end of text.


COPYRIGHT
    (c) 1996-7 Metrowerks Corporation
    All rights reserved.

HISTORY
    96 OCT 20 LLY   Created __start.c
    96 NOV 08 LLY   Initialized .bss and .sbss
    97 FEB 19 LLY   Added __copy_rom function as a future placeholder for
                    copying ROM-based data into RAM.  __start changed to
                    __init_main for SDSMON07
    97 FEB 24 LLY   Test for zero length .bss and .sbss
    97 FEB 28 LLY   Converted to C code.  Added the rest of the EABI sections
                    (.sdata, .sbss, .sdata2)
    97 MAR 02 LLY   Added vector copy
    97 MAR 10 LLY   Converted runtime init for MPC821ADS stationery.
    97 APR 16 LLY   Converted runtime init for PPC EABI tools 4/1/97.
                    New linker-generated symbols; moved to
                    "__ppc_eabi_linker.h".  Register initialization was
                    moved into a function.
    97 APR 19 LLY   Added comments regarding safe usage of memory.
    97 MAY 01 LLY   Added GRC's initial stack frame.
    97 JUL 20 MEA   Removed __exit and replaced it with _ExitProcess.
    97 DEC 7  MEA   Changed linker generated symbols to C type variables.
                    Use _rom_copy_info and _bss_copy_info instead of
                    individual sections symbols.
    97 DEC 8  MEA   Added section pragma to put all code in this file into
                    ".init"

*/
/*
  Dolphin Modifications
  $Log: BootStart.c,v $
  Revision 1.1.1.1  2004/06/01 21:17:32  paulm
  GC IPLROM source from Nintendo

    
    2     2/24/03 14:21 Shiki
    Added GPR initialization code.

    1     10/02/00 7:48p Tian
    initial checkin - strongly linked __start

    1     10/02/00 7:45p Tian
    Initial checkin - strongly linked version of __start with no OSInit,
    and no TRK initialization.

    7     9/29/00 5:26p Tian
    Now calls OSInit and DBInit.  This allows us to use pre-main static
    global constructors that call new

    6     8/17/00 2:45p Shiki
    Modified to call InitMetroTRK() before __init_user().

    5     5/11/00 2:25p Tian
    ifdefed out all BI2 stuff if BI2_EXISTS is 0

    4     4/26/00 10:38p Tian
    Added log.  Added TRK integration code.
  $NoKeywords: $
*/
/***************************************************************************/

    /* main(), __init_user(), and exit() need to have far absolute  */
    /* addressing if you are flashing to ROM with the default       */
    /* address of 0xfe000000 on MPC860.  You can save a couple of   */
    /* instructions in __start() if you aren't using that address   */
    /* by commenting out the following define.                      */
// XXX do not use long jump
/* #define USE_FAR_ADDRESSING_TO_TEXT_SECTION */

#include <__mem.h>
#include <__ppc_eabi_linker.h>      /* linker-generated symbol declarations */
#include <__ppc_eabi_init.h>        /* board- and user-level initialization */


#include <secure/boot.h>
#include <secure/OSBootInfo2.h>

// For parsing BI2
#define BOOTINFO2_ADDR                  0x800000F4  // 4 bytes
#define BI2_ARGOFFSET_OFFSET            0x0008      // offset from base of BI2

#define ARENAHI_ADDR                    0x80000034

/***************************************************************************/
/*
 *  external declarations
 */
/***************************************************************************/

extern void main();

/***************************************************************************/
/*
 *  function declarations
 */
/***************************************************************************/

#pragma section code_type ".init"

#ifdef __cplusplus
extern "C" {
#endif

extern void __start(void);              /* primary entry point */
extern void exit(int);
extern void OSInit(void);
extern void DBInit(void);
#ifdef __cplusplus
}
#endif

static void __init_registers(void);     /* set up PPC regs (r1, r2, r13) */
static void __init_data(void);          /* private ROM-to-RAM copy routine */


/***************************************************************************/
/*
    Memory Map for Motorola MPC821/860ADS Evaluation Board

    0x00000000..0x00001FFF  Exception Vector Table
          0D00              Trace Exception
          1000              Breakpoint Exception
          1100              (end of Breakpoint Exception)
    0x00002000..0x0000FFFF  Unused RAM
    0x00010000..0x000FFFFF  Unused RAM (first MB)
         10000              Start PC (default for downloaded programs)
              ..0x001FFFFF  Unused RAM (second MB)
              ..0x002FFFFF  Unused RAM (third MB)
              ..0x003FFFFF  Unused RAM (fourth MB)
              ..
              ..0x01FFFFFF  Unused RAM (32nd MB, maximum for eval board)

*/
/***************************************************************************/

/***************************************************************************/
/*
 *  __start
 *
 *  PowerPC EABI Runtime Initialization.  Initialize pointers,
 *  initialize data, and call main().
 *
 *  This function is the very first location executed in a program, or
 *  the first location called by the board-level initialization.
 *  Memory access is not guaranteed to be safe until after __init_hardware.
 */
/***************************************************************************/
__declspec ( weak ) asm void __start(void)
{
    nofralloc                           /* MWERKS: explicitly no stack */
                                        /* frame allocation */

#if __option(little_endian)

    opword 0xA600007C   /* load the MSR */
    opword 0x01000060   /* set the LE mode bit */
    opword 0x2401007C   /* store the new MSR */

#endif


    /*
     *  PowerPC EABI init registers (stack, small data areas)
     */
    bl      __init_registers

    /*
     *  board-level initialization
     */
    bl      __init_hardware

    /*
     *  Memory access is safe now.
     */

    /*
     *  Prepare a terminating stack record.
     */
    li      r0, 0xFFFF              /* load up r0 with 0xFFFFFFFF */
    stwu    r1, -8(r1)              /* Decrement stack by 8 bytes, (write word)*/
    stw     r0, 4(r1)               /* Make an illegal return address of 0xFFFFFFFF */
    stw     r0, 0(r1)               /* Make an illegal back chain address of 0xFFFFFFFF */

    /*
     *  Data initialization: copy ROM data to RAM as necessary
     */

#if __dest_os != __eppc_vxworks
    bl      __init_data
#endif

#if BI2_EXISTS
#endif // BI2_EXISTS

    /*
     *  initialization before main
     */

#if BI2_EXISTS
    /*
     *  parse arguments
     *    r5: start address of BI2
     */
    lis     r6,BOOTINFO2_ADDR@ha
    addi    r6,r6,BOOTINFO2_ADDR@l
    lwz     r5,0(r6)            // BI2 base
    cmplwi  r5,0
    beq+    _no_args            // BI2 not loaded: treated as no arguments
                                // this is for boot
    addi    r6,r5,BI2_ARGOFFSET_OFFSET
    lwz     r6,0(r6)            // argument offset
    cmplwi  r6,0
    beq+    _no_args            // no arguments
    add     r6,r5,r6            // argument start

    lwz     r3,0(r6)            // argc

    cmplwi  r3,0
    beq-    _no_args            // shouldn't happen

    addi    r4,r6,4             // argv

    mtctr   r3
  _loop:
    addi    r6,r6,4
    lwz     r7,0(r6)
    add     r7,r7,r5
    stw     r7,0(r6)
    bdnz    _loop

    /*
     *  change arena hi
     */
    lis     r5,ARENAHI_ADDR@ha
    addi    r5,r5,ARENAHI_ADDR@l
    clrrwi  r7,r4,5             // align by 32bytes
    stw     r7,0(r5)
    b       _end_of_parseargs

  _no_args:
    li      r3,0
    li      r4,0
    // do not change arenaHi in this case

  _end_of_parseargs:
#endif // BI2_EXISTS

    bl  DBInit

#if defined(USE_FAR_ADDRESSING_TO_TEXT_SECTION)
    lis      r3,__init_user@ha
    addi     r3,r3,__init_user@l
    mtlr     r3
    blrl
#else
    bl      __init_user
#endif

    /*
     *  branch to main program
     */
#if defined(USE_FAR_ADDRESSING_TO_TEXT_SECTION)
    lis      r5,main@ha
    addi     r5,r5,main@l
    mtlr     r5
    blrl
#else
    bl      main
#endif

    /*
     *  exit program
     */

#if defined(USE_FAR_ADDRESSING_TO_TEXT_SECTION)
    lis      r3,exit@ha
    addi     r3,r3,exit@l
    mtlr     r3
    blrl
#else
    b       exit
#endif

}

/***************************************************************************/
/*
 *  __copy_rom_section
 *
 *  Copy the ROM section to RAM if dst and src are different and size
 *  is nonzero.
 *
 *  dst         destination RAM address
 *  src         source ROM address
 *  size        number of bytes to copy
 */
/***************************************************************************/
static void __copy_rom_section(void* dst, const void* src, unsigned long size)
{
    if (size && (dst != src)) {
        memcpy(dst, src, size);
        __flush_cache( dst, size );
    }
}


/***************************************************************************/
/*
 *  __init_bss_section
 *
 *  Initialize the RAM section to zeros if size is greater than zero.
 *
 *  dst         destination RAM address
 *  size        number of bytes to zero
 */
/***************************************************************************/
static void __init_bss_section(void* dst, unsigned long size)
{
    if (size) {
        memset(dst, 0, size);
    }
}

/***************************************************************************/
/*
 *  __init_registers
 *
 *  Initialize PowerPC EABI Registers
 *
 *  Note: this function is guaranteed to not reference any memory; the memory
 *  controller may not be initialized.
 *
 */
/***************************************************************************/
asm static void __init_registers(void)
{
    nofralloc                       /* see above on usage of nofralloc */

    /*
     *  initialize GPR for better HW/SW problem isolation
     */
    li      r0, 0x0
    li      r3, 0x0
    li      r4, 0x0
    li      r5, 0x0
    li      r6, 0x0
    li      r7, 0x0
    li      r8, 0x0
    li      r9, 0x0
    li      r10, 0x0
    li      r11, 0x0
    li      r12, 0x0
    li      r14, 0x0
    li      r15, 0x0
    li      r16, 0x0
    li      r17, 0x0
    li      r18, 0x0
    li      r19, 0x0
    li      r20, 0x0
    li      r21, 0x0
    li      r22, 0x0
    li      r23, 0x0
    li      r24, 0x0
    li      r25, 0x0
    li      r26, 0x0
    li      r27, 0x0
    li      r28, 0x0
    li      r29, 0x0
    li      r30, 0x0
    li      r31, 0x0

    /*
     *  initialize stack pointer
     */
    lis     r1, _stack_addr@h       /* _stack_addr is generated by linker */
    ori     r1, r1, _stack_addr@l

#if __dest_os != __eppc_vxworks
    /*
     *  initialize small data area pointers (EABI)
     */
    lis     r2, _SDA2_BASE_@h       /* __SDA2_BASE_ is generated by linker */
    ori     r2, r2, _SDA2_BASE_@l

    lis     r13, _SDA_BASE_@h       /* _SDA_BASE_ is generated by linker */
    ori     r13, r13, _SDA_BASE_@l
#endif

    blr
}

/***************************************************************************/
/*
 *  __init_data
 *
 *  Initialize all (RAM) data sections, copying ROM sections as necessary.
 *
 *  dst         destination RAM address
 *  size        number of bytes to zero
 */
/***************************************************************************/
static void __init_data(void)
{
    __rom_copy_info *dci;
    __bss_init_info *bii;

    /* Copy from ROM to RAM: */

    dci = _rom_copy_info;
    while (1) {
        if (dci->size == 0) break;
        __copy_rom_section(dci->addr, dci->rom, dci->size);
        dci++;
    }

    /* Initialize with zeros: */

    bii = _bss_init_info;
    while (1) {
        if (bii->size == 0) break;
        __init_bss_section(bii->addr, bii->size);
        bii++;
    }
}
