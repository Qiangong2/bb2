/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: メニューモード時の各面のパラメータ
  -----------------------------------------------------------------------------*/
#include <dolphin.h>

#include "OptionState.h"
#include "OptionUpdate.h"

#ifdef JHOSTIO
#include "IHIOptionState.h"
#endif

/*-----------------------------------------------------------------------------
  ファイルローカル変数
  -----------------------------------------------------------------------------*/
static OptionState* stp;

#ifndef JHOSTIO
static OptionState st;
#endif

/*-----------------------------------------------------------------------------
  初期化
  -----------------------------------------------------------------------------*/
void
initOption( BOOL first )
{
	if ( first ) {
#ifndef JHOSTIO
		stp = &st;
		initOptionState( stp );
#else
		stp = getIHIOptionState();
#endif
	}

	initOptionMenu( first );
}

/*-----------------------------------------------------------------------------
  パラメータ初期化
  -----------------------------------------------------------------------------*/
void
initOptionState( OptionState* p )
{
	p->cube_r = -30;
	p->cube_g = 20;
	p->cube_b = -50;
	p->cube_a = 255;

	p->moji_r = 0;
	p->moji_g = 255;
	p->moji_b = 120;
	p->step = 2.0f;

	p->concent_dec_alpha = 12;
	p->concent_dec_pos = 5;

	p->sound_pos_y = -15.f;
	p->yoko_pos_y = -4.f;
	p->lang_y = 6.f;
	p->lang_sel_y = 8.f;
}

/*-----------------------------------------------------------------------------
  パラメータポインタ取得
  -----------------------------------------------------------------------------*/
OptionState*
getOptionState( void )
{
	return stp;
}

