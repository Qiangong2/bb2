/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: メニューモード時の各面のパラメータ
  -----------------------------------------------------------------------------*/
#include <dolphin.h>
#include <math.h>

#include "gCont.h"
#include "gBase.h"
#include "gFont.h"
#include "gCamera.h"
#include "gInit.h"
#include "gTrans.h"
#include "gLoader.h"
#include "gMath.h"

#include "gLayoutRender.h"
#include "gMRmaterial.h"

#include "MenuUpdate.h"	// メインメニューの動作を決定するEnumが入っている。
#include "CardState.h"
#include "CardUpdate.h"
#include "CardSequence.h"
#include "CardIconMove.h"
#include "gMainState.h"
#include "SplashMain.h"

#if defined(MPAL)
#include "mpal_mesg.h"
#elif defined(PAL)
#include "pal_mesg.h"
#else // NTSC
#include "ntsc_mesg.h"
#endif
//#include "2d_memorycard_3_Japanese.h"
#include "jaudio.h"

/*-----------------------------------------------------------------------------
  コントローラの入力を外部参照する。
  -----------------------------------------------------------------------------*/
extern DIPadStatus	DIPad;

static u8	sel_slot;
static s8	sel_x[2];
static s8	sel_y[2];
static s8	now_line[2];
static s8	max_lines[2];

static s32	exec_chan;
static s32	exec_command;
static s32	exec_result;

static s16	card_runstate;
static s16	pre_card_runstate;
static s16	cardwin_runstate;
static s16	pre_cardwin_runstate;
static s16	formatwin_runstate[2];
static s16	pre_formatwin_runstate[2];
static s16	verify_command;
static s16	pre_verify_command;

static gLayoutHeader*	mem_lay;
static gLRStringHeader*	mem_str;
static s16		icon_x[2][IPL_MAX_ICON_DISPLAY_NUM];
static s16		icon_y[2][IPL_MAX_ICON_DISPLAY_NUM];
static IconAnimeState	sIconAnime[2][CARD_MAX_FILE];
static FileArray		sFileArray[2][CARD_MAX_FILE];	// ファイルを日付（以外でも）でソートする。

#if defined(MPAL)
static gLRFade	mes[ MSG_2D_MEMORYCARD_3_MPAL_PORTUGUESE_NUM ];
#elif defined(PAL)
static gLRFade	mes[ MSG_2D_MEMORYCARD_3_PAL_ENGLISH_NUM ];
#else // NTSC
static gLRFade	mes[ MSG_2D_MEMORYCARD_3_JAPANESE_NUM ];
#endif

static gLRFade  slot_icon[2];
static gLRFade	copy_mes[2];
static gLRFade	move_mes[2];
static CmdFade	cmdwnd[2];
static gLRFade	arrow_up[2];
static gLRFade	arrow_down[2];

static ResTIMG*	num_timg;		// 容量表示用。
static u16		num_timg_x[3];
static u16		num_timg_y[3];
static u32 copy_sound_first = 0;

static s16		arrow_slide_frame;
static s16		arrow_slide_bounds;
static s16		arrow_slide_frame_max;

static s16		slot_blink_frame;
static s16		slot_blink_bounds;
static s16		slot_blink_frame_max;
static gLRFade	active_slot_fade[2];
static gLRFade	main_light;

/*-----------------------------------------------------------------------------
  static function
  -----------------------------------------------------------------------------*/
static void		initCmdFade( CmdFade* wp );
static void		updateCmdFade( CmdFade* wp, u8 flag );
static u32		loadbannerTexture( void );
static void		drawFileInfo( u8 alpha );
static void		drawEdgeAnimeSub( int x, int y, int dx, int dy, int delta, GXColor* rasc );

/*-----------------------------------------------------------------------------
  アイコンを表示する場所を取得する。
  -----------------------------------------------------------------------------*/
static void
resolveIconPos( gLayoutHeader* header )
{
	u32	i, j, k, id;
	gLRPictureData*	pdp = (gLRPictureData*)( (u8*)header + header->picture_offset );

	i = 'ia00';
	for ( k = 0 ; k < IPL_MAX_ICON_DISPLAY_NUM * 2 ; k++ ) {
		u8 fileNo = (u8)( k % IPL_MAX_ICON_DISPLAY_NUM );
		u8 slot   = (u8)( k / IPL_MAX_ICON_DISPLAY_NUM );
		for ( j = 0 ; j < mem_lay->picture_num ; j++ ) {
			if ( pdp[j].tag == ( i + slot * 65536 + ( fileNo / 10 ) * 256 + ( fileNo % 10 ) ) ) {
				icon_x[slot][fileNo] = (s16)pdp[j].x;
				icon_y[slot][fileNo] = (s16)pdp[j].y;
			}
		}
	}

	for ( j = 0 ; j < 2 ; j ++ ) {
		id = 'fnma' + j;	// 空き容量
		for ( i = 0 ; i < ((gLayoutHeader*)mem_lay)->picture_num ; i++ ) {
			if ( pdp[i].tag == id )	{
				num_timg_x[0+j] = pdp[i].x;
				num_timg_y[0+j] = pdp[i].y;
			}
		}
	}
	for ( i = 0 ; i < ((gLayoutHeader*)mem_lay)->picture_num ; i++ ) {
		if ( pdp[i].tag == 'nmc1' )	{
			num_timg_x[2] = pdp[i].x;
			num_timg_y[2] = pdp[i].y;
		}
	}
}

/*-----------------------------------------------------------------------------
  アイコンアニメのフレーム初期化
  -----------------------------------------------------------------------------*/
static void
initIconAnime( u8 no )
{
	int i;

	for ( i = 0 ; i < CARD_MAX_FILE ; i++ ) {
		sIconAnime[no][i].now_frame = 0;
	}
}

/*-----------------------------------------------------------------------------
  初期化
  -----------------------------------------------------------------------------*/
void
initCardMenu_Lang( void )
{
	mem_str = gGetBufferPtr( getID_2D_memory_string() );
}

void
initCardMenu( BOOL first )
{
	u32 i;

	if ( first ) {
		mem_lay = gGetBufferPtr( getID_2D_memory_layout() );
//		mem_str = gGetBufferPtr( getID_2D_memory_string() );
		initCardMenu_Lang();

		resolveIconPos( mem_lay );

		num_timg = gGetBufferPtr( getID_blocknumber_tex() );	// 数字。
		
	}
	initCardIconMove( first, icon_x, icon_y );// アイコンの移動アニメの初期化。

	initIconAnime(0);
	initIconAnime(1);

	sel_slot = 0;
	sel_x[0] = 0;
	sel_x[1] = 0;
	sel_y[0] = 0;
	sel_y[1] = 0;

	now_line[0] = 0;
	now_line[1] = 0;

	card_runstate = IPL_CARD_FILE_SELECTING;
	cardwin_runstate = IPL_CARD_WINDOW_DELETE;

	pre_card_runstate = card_runstate;
	pre_cardwin_runstate = cardwin_runstate;

	exec_chan = 0;
	exec_command = IPL_CARD_CMD_MOUNT;
	exec_result  = IPL_CARD_RESULT_READY;

	verify_command = 1;

#if defined(MPAL)
	for ( i = 0 ; i < MSG_2D_MEMORYCARD_3_MPAL_PORTUGUESE_NUM ; i++ )	{
#elif defined(PAL)
	for ( i = 0 ; i < MSG_2D_MEMORYCARD_3_PAL_ENGLISH_NUM ; i++ )	{
#else // NTSC
	for ( i = 0 ; i < MSG_2D_MEMORYCARD_3_JAPANESE_NUM ; i++ )	{
#endif
		glrInitFadeState( &mes[i] );
		mes[i].tag = i;
	}

	for ( i = 0 ; i < 2 ; i++ ) {
		glrInitFadeState( &slot_icon[i] );
		glrInitFadeState( &copy_mes[i] );
		glrInitFadeState( &move_mes[i] );
		glrInitFadeState( &arrow_up[i] );	arrow_up[i].tag   = 'arau' + i * 256;
		glrInitFadeState( &arrow_down[i] );	arrow_down[i].tag = 'arad' + i * 256;
		initCmdFade( &cmdwnd[i] );

		formatwin_runstate[i] = 0b00000001;
		pre_formatwin_runstate[i] = formatwin_runstate[i];
	}

	copy_sound_first = 0;
	arrow_slide_frame  = 0;
	arrow_slide_bounds = 1;
	arrow_slide_frame_max = 15;

	slot_blink_frame  = 0;
	slot_blink_bounds = 1;
	slot_blink_frame_max = 30;

	glrInitFadeState( &active_slot_fade[0] );
	active_slot_fade[0].fade_frame = 10;
	glrInitFadeState( &active_slot_fade[1] );
	active_slot_fade[1].fade_frame = 10;
	glrInitFadeState( &main_light );
	main_light.fade_frame = 10;
}






















/*-----------------------------------------------------------------------------
  update section
  -----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
  コピー／移動の時に、相手側のスロットがつかえるかどうか。
  -----------------------------------------------------------------------------*/
static BOOL
isDistSlot( s32* ret )
{
	u8 no = (u8)( sel_slot ^ 1 );
	SlotState* tSlot = getCardSlotState();

	if ( ret != NULL ) *ret = tSlot[no].state;
	
	if ( tSlot[no].state == SLOT_READY ) {
		return TRUE;
	}

	return FALSE;
}

/*-----------------------------------------------------------------------------
  コピーできるかどうか
  -----------------------------------------------------------------------------*/
static BOOL
isCopyEnable( u8 chan, u32 fileNo, s32* result )
{
	s32	ret, tret;
	BOOL enable = FALSE;
	DirStateArrayPtr dirState = getCardDirState();
	SlotState* tSlot = getCardSlotState();
	
	if ( isDistSlot(NULL) &&
		 dirState[chan][fileNo].copy_enable &&
		 tSlot[chan].sector_size == tSlot[chan^1].sector_size &&
		 tSlot[chan^1].free_blocks >= dirState[chan][fileNo].blocks &&
		 tSlot[chan^1].free_fileentry > 0 &&
		 !dirState[chan][fileNo].same_file ) {
		ret = IPL_CARD_RESULT_BUSY;
		enable = TRUE;
	} else {
		if ( !isDistSlot(&tret) ) {
			if ( tret == SLOT_NOCARD ) {
				ret = IPL_CARD_RESULT_NOCARD;
			} else if ( tret == SLOT_WRONGDEVICE ||
						tret == SLOT_BROKEN_NF || 
						tret == SLOT_ENCODING_NF ) {
				ret = IPL_CARD_RESULT_WRONGDEVICE;
			} else {
				ret = IPL_CARD_RESULT_FATAL_ERROR;
			}
		}
		else if ( !dirState[chan][fileNo].copy_enable ) {
			ret = IPL_CARD_RESULT_NOPERM;
		}
		else if ( tSlot[chan].sector_size != tSlot[chan^1].sector_size ) {
			ret = IPL_CARD_RESULT_SECTOR;
		}
		else if ( tSlot[chan^1].free_blocks < dirState[chan][fileNo].blocks ||
				  tSlot[chan^1].free_fileentry == 0 ) {
			ret = IPL_CARD_RESULT_INSSPACE;
		}
		else if ( dirState[chan][fileNo].same_file ) {
			ret = IPL_CARD_RESULT_EXIST;
		}
	}
	if ( result != NULL ) *result = ret;

	return enable;
}


/*-----------------------------------------------------------------------------
  移動できるかどうか
  -----------------------------------------------------------------------------*/
static BOOL
isMoveEnable( u8 chan, u32 fileNo, s32* result )
{
	s32	ret, tret;
	BOOL enable = FALSE;
	DirStateArrayPtr dirState = getCardDirState();
	SlotState* tSlot = getCardSlotState();
	
	if ( isDistSlot(NULL) &&
		 dirState[chan][fileNo].move_enable &&
		 tSlot[chan].sector_size == tSlot[chan^1].sector_size &&
		 tSlot[chan^1].free_blocks >= dirState[chan][fileNo].blocks &&
		 tSlot[chan^1].free_fileentry > 0 &&
		 !dirState[chan][fileNo].same_file ) {
		ret = IPL_CARD_RESULT_BUSY;
		enable = TRUE;
	} else {
		if ( !isDistSlot(&tret) ) {
			if ( tret == SLOT_NOCARD ) {
				ret = IPL_CARD_RESULT_NOCARD;
			} else if ( tret == SLOT_WRONGDEVICE ||
						tret == SLOT_BROKEN_NF || 
						tret == SLOT_ENCODING_NF ) {
				ret = IPL_CARD_RESULT_WRONGDEVICE;
			} else {
				ret = IPL_CARD_RESULT_FATAL_ERROR;
			}
		}
		else if ( !dirState[chan][fileNo].move_enable ) {
			ret = IPL_CARD_RESULT_NOPERM;
		}
		else if ( tSlot[chan].sector_size != tSlot[chan^1].sector_size ) {
			ret = IPL_CARD_RESULT_SECTOR;
		}
		else if ( tSlot[chan^1].free_blocks < dirState[chan][fileNo].blocks ||
				  tSlot[chan^1].free_fileentry == 0 ) {
			ret = IPL_CARD_RESULT_INSSPACE;
		}
		else if ( dirState[chan][fileNo].same_file ) {
			ret = IPL_CARD_RESULT_EXIST;
		}
	}
	if ( result != NULL ) *result = ret;

	return enable;
}

/*-----------------------------------------------------------------------------
  ---- IPL_CARD_FILE_SELECTINGに変更するときの処理。
  -----------------------------------------------------------------------------*/
static void
setCardRunstateFileSelecting( BOOL force )
{
	SlotState* tSlot = getCardSlotState();

	if ( card_runstate == IPL_CARD_FILE_WAIT && !force ) return;
	
	if ( tSlot[sel_slot^1].state == SLOT_ENCODING ||
		 tSlot[sel_slot^1].state == SLOT_BROKEN ) {
		pre_card_runstate = IPL_CARD_FILE_FORMAT_VERIFY;
		card_runstate = pre_card_runstate;
		sel_slot ^= 1;
	}
	else {
		pre_card_runstate = IPL_CARD_FILE_SELECTING;
		card_runstate = pre_card_runstate;
		if ( tSlot[sel_slot].state != SLOT_READY ) {
			if ( isDistSlot(NULL) ) {
				sel_slot ^= 1;
			}
		}
//		DIReport("select\n");
	}
}

/*-----------------------------------------------------------------------------
  フォーマットをキャンセルしたときの処理。
  -----------------------------------------------------------------------------*/
static void
updateCancelFormat( void )
{
	SlotState* tSlot = getCardSlotState();

	if ( tSlot[sel_slot].state == SLOT_BROKEN ) {
		tSlot[sel_slot].state = SLOT_BROKEN_NF;
	} else {
		tSlot[sel_slot].state = SLOT_ENCODING_NF;
	}
	pre_formatwin_runstate[sel_slot] = 0b00000001;	// いいえにカーソルを移動。
	setCardRunstateFileSelecting( FALSE );
}

/*-----------------------------------------------------------------------------
  現在の表示可能最大行を取得する。
  -----------------------------------------------------------------------------*/
static BOOL
getMaxLines( s32 chan, s8* max )
{
	s8	m;
	SlotState* tSlot = getCardSlotState();

	if ( tSlot[chan].state != SLOT_READY ) return FALSE;

	m = (s8)( (CARD_MAX_FILE - tSlot[chan].free_fileentry ) / 4 - 3 );
	if ( ( (CARD_MAX_FILE - tSlot[chan].free_fileentry ) % 4 ) == 0 )  m--;
	if ( m < 0 ) m = 0;

	*max = m;

	return TRUE;
}

/*-----------------------------------------------------------------------------
  メニューから抜けたときにフォーマットしますかと聞いて欲しいんだって。
  -----------------------------------------------------------------------------*/
static void
resetFormatState( void )
{
	SlotState* tSlot = getCardSlotState();
	int	i;

	for ( i = 0 ; i < 2 ; i++ ) {
		if ( tSlot[i].changed == FALSE ) {
			if ( tSlot[i].state == SLOT_BROKEN_NF ) {
				tSlot[i].state = SLOT_BROKEN;
				tSlot[i].changed = TRUE;
			} else if ( tSlot[i].state == SLOT_ENCODING_NF ) {
				tSlot[i].state = SLOT_ENCODING;
				tSlot[i].changed = TRUE;
			}
		}
	}
}

/*-----------------------------------------------------------------------------
  ファイル選択状態のコントローラ入力
  -----------------------------------------------------------------------------*/
static s32
updateControlMain( void )
{
	s32	ret = IPL_MENU_MEMORYCARD_SELECT;
	SlotState* tSlot = getCardSlotState();
	DirStateArrayPtr dirState = getCardDirState();

	if ( !isCardIconFadeDone() ) return ret;
	
	// コントローラの入力
	if ( tSlot[sel_slot].state == SLOT_READY ) {
		if ( getMaxLines( sel_slot, &max_lines[ sel_slot ] ) ) {
			if ( DIPad.dirsPressed & DI_STICK_UP ) {	// 上
				sel_y[ sel_slot ] -= 1;
				if ( sel_y[ sel_slot ] < 0 ) {
					sel_y[ sel_slot ] = 0;
					if ( now_line[ sel_slot ] > 0 ) {
						now_line[ sel_slot ] --;
						setCardIconMove( sFileArray, now_line );
						Jac_PlaySe( JAC_SE_MC_SELECT );
					} else {
						if ( !gIsPadRepeating() ) Jac_PlaySe( JAC_SE_VAL_LIMIT );
					}
				} else {
					Jac_PlaySe( JAC_SE_MC_SELECT );
				}
			}
			if ( DIPad.dirsPressed & DI_STICK_DOWN ) {// 下
				sel_y[ sel_slot ] += 1;
				if ( (sel_x[ sel_slot ]+(sel_y[ sel_slot ]+now_line[sel_slot])*4) >= 127 ) {
					if ( !gIsPadRepeating() ) Jac_PlaySe( JAC_SE_VAL_LIMIT );
					sel_y[ sel_slot ] -= 1;
				} else if ( sel_y[ sel_slot ] > 3 ) {
					sel_y[ sel_slot ] = 3;
					if (  now_line[ sel_slot ] < max_lines[sel_slot] ) {
						now_line[ sel_slot ]++;
							setCardIconMove( sFileArray, now_line );
							Jac_PlaySe( JAC_SE_MC_SELECT );
					} else {
						if ( !gIsPadRepeating() ) Jac_PlaySe( JAC_SE_VAL_LIMIT );
					}
				} else {
					Jac_PlaySe( JAC_SE_MC_SELECT );
				}
			}

			if ( DIPad.dirsPressed & DI_STICK_LEFT ) {// 左。
				if ( sel_slot == 0 ) {
					if ( sel_x[ sel_slot ] > 0 ) {
						Jac_PlaySe( JAC_SE_MC_SELECT );
						sel_x[ sel_slot ] -= 1;
					} else {
						if ( !gIsPadRepeating() ) Jac_PlaySe( JAC_SE_VAL_LIMIT );
					}
				}
				else if ( sel_slot == 1 ) {
					sel_x[ sel_slot ] -= 1;
					if ( sel_x[ sel_slot ] < 0 ) { // スロットの移動。
						sel_x[ sel_slot ] = 0;
						if ( tSlot[ sel_slot^1 ].state == SLOT_READY ) {
							if ( (sel_x[ sel_slot^1 ]+(sel_y[ sel_slot ]+now_line[sel_slot^1])*4) >= 127 ) {
								if ( !gIsPadRepeating() ) Jac_PlaySe( JAC_SE_VAL_LIMIT );
							} else {
								sel_slot ^= 1;
								sel_y[ sel_slot ] = sel_y[ sel_slot^1 ];
								sel_x[ sel_slot ] = 3;
								Jac_PlaySe( JAC_SE_MC_SELECT );
							}
						} else {
						if ( !gIsPadRepeating() ) Jac_PlaySe( JAC_SE_VAL_LIMIT );
					}
					} else {
						Jac_PlaySe( JAC_SE_MC_SELECT );
					}
				}
			}
			if ( DIPad.dirsPressed & DI_STICK_RIGHT ) {// 右。
				if ( sel_slot == 1 ) {
					if ( sel_x[ sel_slot ] < 3 ) {
						sel_x[ sel_slot ] += 1;
						if ( (sel_x[ sel_slot ]+(sel_y[ sel_slot ]+now_line[sel_slot])*4) >= 127 ) {
							sel_x[ sel_slot ] -= 1;
							if ( !gIsPadRepeating() ) Jac_PlaySe( JAC_SE_VAL_LIMIT );
						} else {
							Jac_PlaySe( JAC_SE_MC_SELECT );
						}
					} else {
						if ( !gIsPadRepeating() ) Jac_PlaySe( JAC_SE_VAL_LIMIT );
					}
				}
				else if ( sel_slot == 0 ) {
					sel_x[ sel_slot ] += 1;
					if ( (sel_x[ sel_slot ]+(sel_y[ sel_slot ]+now_line[sel_slot])*4) >= 127 ) {
						sel_x[ sel_slot ] -= 1;
						if ( !gIsPadRepeating() ) Jac_PlaySe( JAC_SE_VAL_LIMIT );
					} else if ( sel_x[ sel_slot ] > 3 ) { // スロットの移動。
						sel_x[ sel_slot ] = 3;
						if ( tSlot[ sel_slot^1 ].state == SLOT_READY ) {
							sel_slot ^= 1;
							sel_y[ sel_slot ] = sel_y[ sel_slot^1 ];
							sel_x[ sel_slot ] = 0;
							Jac_PlaySe( JAC_SE_MC_SELECT );
						} else {
							if ( !gIsPadRepeating() ) Jac_PlaySe( JAC_SE_VAL_LIMIT );
						}
					} else {
						Jac_PlaySe( JAC_SE_MC_SELECT );
					}
				}
			}

			if ( isCardIconAnimeDone() ) {
				if ( DIPad.buttonDown & PAD_BUTTON_A ) {
					u32 fileNo = sFileArray[ sel_slot ][ sel_x[ sel_slot ] +
														 sel_y[ sel_slot ] * 4 +
														 now_line[ sel_slot ] * 4 ].fileNo;
					if ( dirState[sel_slot][fileNo].validate ) {
						pre_card_runstate = IPL_CARD_FILE_WINDOW;
						Jac_PlaySe( JAC_SE_MC_DECIDE );
					}
				}
			}
		}
	}

	if ( DIPad.buttonDown & PAD_BUTTON_B ) {
		MenuState*	mesp = getMenuState();
		if ( ( mesp->fadeout_z_param == 32767 || 
			   mesp->enable_pre_key_input ) &&
			 isCardIconAnimeDone() ) {
			Jac_PlaySe( JAC_SE_TO_3D );
			resetFormatState();
			ret = IPL_MENU_MEMORYCARD;
		}
	}

	return ret;
}

/*-----------------------------------------------------------------------------
  ウィンドウ表示時のコントローラ入力
  -----------------------------------------------------------------------------*/
static void
precopyFile( s16 fileNo )
{
	DirStateArrayPtr dirState = getCardDirState();
	s32	result;
	
	if ( isCopyEnable( sel_slot, fileNo, &result ) ) {
		Jac_PlaySe( JAC_SE_MC_DECIDE );
		pre_card_runstate = IPL_CARD_FILE_VERIFY;
	} else {
		Jac_PlaySe( JAC_SE_ERROR );
		exec_chan = sel_slot;
		exec_command = IPL_CARD_CMD_FILECOPY_WAIT;
		exec_result = result;
		pre_card_runstate = IPL_CARD_FILE_WAIT;
	}
}

static void
premoveFile( s16 fileNo )
{
	DirStateArrayPtr dirState = getCardDirState();
	s32	result;
	
	if ( isMoveEnable( sel_slot, fileNo , &result ) ) { //isDistSlot(NULL) && dirState[sel_slot][fileNo].move_enable ) {
		Jac_PlaySe( JAC_SE_MC_DECIDE );
		pre_card_runstate = IPL_CARD_FILE_VERIFY;
	} else {
		Jac_PlaySe( JAC_SE_ERROR );
		exec_chan = sel_slot;
		exec_command = IPL_CARD_CMD_FILEMOVE_WAIT;
		exec_result = result;
		pre_card_runstate = IPL_CARD_FILE_WAIT;
	}
}

static s32
updateControlWindow( void )
{
	s32	ret = IPL_MENU_MEMORYCARD_SELECT;
	DirStateArrayPtr dirState = getCardDirState();

	if ( DIPad.dirsPressed & DI_STICK_UP ) {	// 上
		Jac_PlaySe( JAC_SE_MC_FUNC );
		pre_cardwin_runstate--;// = cardwin_runstate - 1;
		if ( pre_cardwin_runstate < 0 ) pre_cardwin_runstate = IPL_CARD_WINDOW_LAST - 1;
	}
	if ( DIPad.dirsPressed & DI_STICK_DOWN ) {	// 上
		Jac_PlaySe( JAC_SE_MC_FUNC );
		pre_cardwin_runstate++;// = cardwin_runstate + 1;
		if ( pre_cardwin_runstate >= IPL_CARD_WINDOW_LAST ) pre_cardwin_runstate = 0;
	}
	if ( ( DIPad.buttonDown & PAD_BUTTON_A ) &&
		 pre_cardwin_runstate == cardwin_runstate ) {
		u32 fileNo = sFileArray[sel_slot][ sel_x[ sel_slot ] + ( sel_y[ sel_slot ] + now_line[ sel_slot ] ) * 4 ].fileNo;
		exec_chan = sel_slot;

		verify_command = 1;	// いいえにカーソルを移動。
		
		switch ( cardwin_runstate ) {
		case IPL_CARD_WINDOW_COPY:
			precopyFile( (s16)fileNo );
			break;
		case IPL_CARD_WINDOW_MOVE:
			premoveFile( (s16)fileNo );
			break;
		case IPL_CARD_WINDOW_DELETE:
			Jac_PlaySe( JAC_SE_MC_DECIDE );
			pre_card_runstate = IPL_CARD_FILE_VERIFY;
			break;
		}
	}
	if ( DIPad.buttonDown & PAD_BUTTON_B ) {
		Jac_PlaySe( JAC_SE_MC_CANCEL );
		setCardRunstateFileSelecting( FALSE );
	}

	return ret;
}
/*-----------------------------------------------------------------------------
  ベリファイ時のコントロール。
  -----------------------------------------------------------------------------*/
/*  コピーコマンド実行。  */
static void
sendCopyFile( u32 fileNo )
{
	exec_command = IPL_CARD_CMD_FILECOPY;
	exec_chan = sel_slot;
	
	if ( isDistSlot(NULL) ) {
		Jac_PlaySe( JAC_SE_MC_DECIDE );
//		if ( sel_slot == 0 ) Jac_PlaySe( JAC_SE_MC_MOVE_LTOC );
//		else Jac_PlaySe( JAC_SE_MC_MOVE_RTOC );
		presetCardIconPos( sel_slot, (u16)( sel_x[ sel_slot ] + sel_y[ sel_slot ] * 4 ) , (u16)fileNo, IPL_CARD_CMD_FILECOPY );
		sendCardCopyCmd( sel_slot, (s16)fileNo );
		exec_result = IPL_CARD_RESULT_BUSY;
		copy_sound_first = 0;
	} else {
		exec_result = IPL_CARD_RESULT_NOCARD;
	}
	pre_card_runstate = IPL_CARD_FILE_WAIT;
}

/*  移動コマンド実行。  */
static void
sendMoveFile( u32 fileNo )
{
	exec_command = IPL_CARD_CMD_FILEMOVE;
	exec_chan = sel_slot;
	
	if ( isDistSlot(NULL) ) {
		Jac_PlaySe( JAC_SE_MC_DECIDE );
//		if ( sel_slot == 0 ) Jac_PlaySe( JAC_SE_MC_MOVE_LTOC );
//		else Jac_PlaySe( JAC_SE_MC_MOVE_RTOC );
		presetCardIconPos( sel_slot, (u16)( sel_x[ sel_slot ] + sel_y[ sel_slot ] * 4 ) , (u16)fileNo, IPL_CARD_CMD_FILEMOVE );
		sendCardMoveCmd( sel_slot, (s16)fileNo );
		exec_result = IPL_CARD_RESULT_BUSY;
		copy_sound_first = 0;
	} else {
		exec_result = IPL_CARD_RESULT_NOCARD;
	}
	pre_card_runstate = IPL_CARD_FILE_WAIT;
}

/*  削除コマンド実行。  */
static void
sendDeleteFile( u32 fileNo )
{
	exec_command = IPL_CARD_CMD_FILEDELETE;
	exec_chan = sel_slot;
	
	Jac_PlaySe( JAC_SE_MC_DECIDE );
	presetCardIconPos( sel_slot, (u16)( sel_x[ sel_slot ] + sel_y[ sel_slot ] * 4 ) , (u16)fileNo ,IPL_CARD_CMD_FILEDELETE );
	sendCardDeleteCmd( sel_slot, (s16)fileNo );
	pre_card_runstate = IPL_CARD_FILE_WAIT;
	copy_sound_first = 0;
	
	exec_result  = IPL_CARD_RESULT_BUSY;
}

static s32
updateControlVerify( void )
{
	SlotState* tSlot = getCardSlotState();

	if ( DIPad.dirsPressed & DI_STICK_UP ) {	// 上
		if ( verify_command != 0 ) Jac_PlaySe( JAC_SE_MC_YESNO );
		verify_command = 0;
	}
	if ( DIPad.dirsPressed & DI_STICK_DOWN ) {	// 下
		if ( verify_command != 1 ) Jac_PlaySe( JAC_SE_MC_YESNO );
		verify_command = 1;
	}
	if ( DIPad.buttonDown & PAD_BUTTON_A ) {
		if ( verify_command == 0 ) {
			u32 fileNo = sFileArray[sel_slot][ sel_x[ sel_slot ] + ( sel_y[ sel_slot ] + now_line[ sel_slot ] ) * 4 ].fileNo;
			exec_chan = sel_slot;
			switch ( cardwin_runstate ) {
			case IPL_CARD_WINDOW_COPY:
				sendCopyFile( fileNo );
				break;
			case IPL_CARD_WINDOW_MOVE:
				sendMoveFile( fileNo );
				break;
			case IPL_CARD_WINDOW_DELETE:
				sendDeleteFile( fileNo );
				break;
			}
		}
		else {
			Jac_PlaySe( JAC_SE_MC_CANCEL );
			pre_card_runstate = IPL_CARD_FILE_WINDOW;
		}
	}
	if ( DIPad.buttonDown & PAD_BUTTON_B ) {
		Jac_PlaySe( JAC_SE_MC_CANCEL );
		pre_card_runstate = IPL_CARD_FILE_WINDOW;
	}

	if ( !isDistSlot(NULL) ) {
		switch ( cardwin_runstate ) {
		case IPL_CARD_WINDOW_COPY:
		case IPL_CARD_WINDOW_MOVE:
			pre_card_runstate = IPL_CARD_FILE_WINDOW;
			break;
		}
	}

	return IPL_MENU_MEMORYCARD_SELECT;
}

/*-----------------------------------------------------------------------------
  くどいフォーマットベリファイ時のコントロール
  -----------------------------------------------------------------------------*/
static BOOL
isFormatKeyEnable( void )
{
	s16	alpha0, alpha1;
	if ( isCardIconAnimeDone() ) {
		glrGetOffsetAlpha( &mes[ MSG_D406 + sel_slot ], &alpha0, NULL );
		glrGetOffsetAlpha( &mes[ MSG_D408 + sel_slot ], &alpha1, NULL );

		DIReport2( "%d:%d \n", alpha0, alpha1 );
		if ( alpha0 > 172 || alpha1 > 172 ) {
			return TRUE;
		}
	}

	return FALSE;
}

static s32
updateControlFmtVerify( void )
{
	SlotState* tSlot = getCardSlotState();
	s16	 	pre_param;

	if ( !isFormatKeyEnable() )	return IPL_MENU_MEMORYCARD_SELECT;

	if ( DIPad.dirsPressed & DI_STICK_UP ) {	// 上
		pre_param = (s16)( pre_formatwin_runstate[sel_slot] & 0b11111110 );
		if ( pre_param != pre_formatwin_runstate[sel_slot] ) Jac_PlaySe( JAC_SE_MC_SELECT );
		pre_formatwin_runstate[sel_slot] = pre_param;
	}
	if ( DIPad.dirsPressed & DI_STICK_DOWN ) {	// 下
		pre_param = (s16)( pre_formatwin_runstate[sel_slot] | 0b00000001 );
		if ( pre_param != pre_formatwin_runstate[sel_slot] ) Jac_PlaySe( JAC_SE_MC_SELECT );
		pre_formatwin_runstate[sel_slot] = pre_param;
	}
	if ( DIPad.buttonDown & PAD_BUTTON_A ) {
		switch ( formatwin_runstate[sel_slot] & 0b00000011 )
		{
		case 0b00000000: // yes
			pre_formatwin_runstate[sel_slot] = 0b00000011;
			Jac_PlaySe( JAC_SE_MC_DECIDE );
			break;
		case 0b00000010: // yes yes
			// 2度目のベリファイ。
			Jac_PlaySe( JAC_SE_MC_DECIDE );
			sendCardFormatCmd( sel_slot );
			pre_card_runstate = IPL_CARD_FILE_WAIT;
			exec_chan = sel_slot;
			exec_command = IPL_CARD_CMD_FORMAT;
			exec_result  = IPL_CARD_RESULT_BUSY;
			break;
		case 0b00000001: // no
		case 0b00000011: // yes no
			Jac_PlaySe( JAC_SE_MC_CANCEL );
			updateCancelFormat();
			break;
		}
	}
	if ( DIPad.buttonDown & PAD_BUTTON_B ) {
		Jac_PlaySe( JAC_SE_MC_CANCEL );
		updateCancelFormat();
	}

	return IPL_MENU_MEMORYCARD_SELECT;
}

/*-----------------------------------------------------------------------------
  確認メッセージ時のキー入力待ち時のコントロール。
  -----------------------------------------------------------------------------*/
static void
playDoneSound( void )
{
	if ( copy_sound_first > 0 ) {
		switch ( exec_command ) {
		case IPL_CARD_CMD_FILECOPY:
		case IPL_CARD_CMD_FILECOPY_WAIT:
			if ( exec_result == IPL_CARD_RESULT_READY ) Jac_PlaySe( JAC_SE_MC_CREATE );
			break;
		case IPL_CARD_CMD_FILEMOVE:
		case IPL_CARD_CMD_FILEMOVE_WAIT:
//			Jac_PlaySe( JAC_SE_MC_DELETE );
			break;
		case IPL_CARD_CMD_FILEDELETE:
		case IPL_CARD_CMD_FILEDELETE_WAIT:
			if ( exec_result == IPL_CARD_RESULT_READY ) Jac_PlaySe( JAC_SE_MC_DELETE );
			break;
		}
	}
	copy_sound_first = 0;
}

static s32
updateControlWait( void )
{
	SlotState* tSlot = getCardSlotState();

	if ( !isCardIconAnimeDone() ) return IPL_MENU_MEMORYCARD_SELECT;

	if ( getCardLastCmdResult() != IPL_CARD_RESULT_BUSY ) {
		playDoneSound();
		if ( DIPad.buttonDown & PAD_BUTTON_A ) {
			verify_command = 1;	// いいえにカーソルを移動。
			exec_command = IPL_CARD_CMD_MOUNT;
			exec_result  = IPL_CARD_RESULT_READY;
			setCardRunstateFileSelecting( TRUE );
			Jac_PlaySe( JAC_SE_MC_CANCEL );
		}
		if ( DIPad.buttonDown & PAD_BUTTON_B ) {
			verify_command = 1;	// いいえにカーソルを移動。
			exec_command = IPL_CARD_CMD_MOUNT;
			exec_result  = IPL_CARD_RESULT_READY;
			setCardRunstateFileSelecting( TRUE );
			Jac_PlaySe( JAC_SE_MC_CANCEL );
		}
	}

	return IPL_MENU_MEMORYCARD_SELECT;
}




/*-----------------------------------------------------------------------------
  日付でソートに使う比較ルーチン
  -----------------------------------------------------------------------------*/
static int
compareFileArray( void* arg_a, void* arg_b )
{
	FileArray	*a, *b;

	a = arg_a;
	b = arg_b;

	if ( a->time > b->time ) return 1;
	else if ( a->time < b->time ) return -1;

	if ( a->fileNo > b->fileNo ) return 1;
	else if ( a->fileNo < b->fileNo ) return -1;
	
	return 0;
}

/*-----------------------------------------------------------------------------
	スロットのファイルを日付でソートする。
  -----------------------------------------------------------------------------*/
static void
sortFileArraySub( u8 no )
{
	s32 i;
	DirStateArrayPtr dirState = getCardDirState();
	

	for ( i = 0 ; i < CARD_MAX_FILE ; i++ ) {
		if ( dirState[no][i].validate ) {
			sFileArray[no][i].fileNo = (u32)i;
			sFileArray[no][i].time = dirState[no][i].time;
		}
		else {
			sFileArray[no][i].fileNo = (u32)i;
			sFileArray[no][i].time = 0xffffff00 + (0xff - i);
		}
	}
	
	qsort( &sFileArray[no][0], CARD_MAX_FILE, sizeof(FileArray), compareFileArray );

	getMaxLines( no, &max_lines[no] );

	if ( max_lines[no] < now_line[no] ) now_line[no] = max_lines[no];
}

/*-----------------------------------------------------------------------------
	各スロットの表示用ファイルエントリの更新／ソート
  -----------------------------------------------------------------------------*/
static void
updateFileArraySub( u8 no )
{
	SlotState* tSlot = getCardSlotState();

	tSlot[no].changed = FALSE;

	if ( !getMaxLines( no, &max_lines[no] ) ) {
		return;
	}

	sortFileArraySub( no );

	// アイコンアニメのセット。
	if ( exec_command == IPL_CARD_CMD_FILEMOVE ||
		 exec_command == IPL_CARD_CMD_FILECOPY ||
		 exec_command == IPL_CARD_CMD_FILEDELETE )
	{
		if ( exec_result == IPL_CARD_RESULT_BUSY ) {
			exec_result = getCardLastCMDFCmdResult();
		} 
		if ( exec_result == IPL_CARD_RESULT_READY ) {
			setCardIconPos(	getCardLastCmdFileNo() );
			//copy_sound_first = 0;	// 音をリセット。
			switch ( exec_command )
			{
			case IPL_CARD_CMD_FILEMOVE:
				if ( exec_chan == 0 ) {
					Jac_PlaySe( JAC_SE_MC_MOVE_CTOR );
				}
				else {
					Jac_PlaySe( JAC_SE_MC_MOVE_CTOL );
				}
				exec_command = IPL_CARD_CMD_FILEMOVE_WAIT;
				break;
			case IPL_CARD_CMD_FILEDELETE:
				exec_command = IPL_CARD_CMD_FILEDELETE_WAIT;
				break;
			case IPL_CARD_CMD_FILECOPY:
				Jac_PlaySe( JAC_SE_MC_MOVE_CTOLR );
				exec_command = IPL_CARD_CMD_FILECOPY_WAIT;
				break;
			}
		} else {
			cancelCardIconPos();
		}
	}
	setCardIconMove( sFileArray, now_line );
}

void
setCardIcon( void )
{
	setCardIconMove( sFileArray, now_line );
}

/*-----------------------------------------------------------------------------
	フォーマット確認ステートに移行する。
  -----------------------------------------------------------------------------*/
static void
execFormatVerify( u8 no )
{
	SlotState* tSlot = getCardSlotState();
	// スロットに入っているカードが要フォーマット。
	sel_slot = no;
	pre_card_runstate = IPL_CARD_FILE_FORMAT_VERIFY;
	card_runstate = pre_card_runstate;
	pre_formatwin_runstate[no] = 0b00000001;
	tSlot[no].changed = FALSE;
}

static void
playFileOpSound( void )
{
	
	if ( exec_chan == 0 ) {
		switch ( exec_command ) {
		case IPL_CARD_CMD_FILEMOVE:
		case IPL_CARD_CMD_FILECOPY:
			if ( copy_sound_first == 10  ) {
				Jac_PlaySe( JAC_SE_MC_MOVE_LTOC );
			}
			break;
		}
	} else {
		switch ( exec_command ) {
		case IPL_CARD_CMD_FILEMOVE:		
		case IPL_CARD_CMD_FILECOPY:
			if ( copy_sound_first == 10 ) {
				Jac_PlaySe( JAC_SE_MC_MOVE_RTOC );
			}
			break;
		}
	}
	copy_sound_first++;
	if ( copy_sound_first > 20 ) copy_sound_first = 20;
}

/*-----------------------------------------------------------------------------
	各スロットの状況を見て、ステートを更新する。
  -----------------------------------------------------------------------------*/
static void
checkCommandDone( void )
{

	// コマンドが終わってないか、チェック。
	if ( exec_command == IPL_CARD_CMD_FILEMOVE ||
		 exec_command == IPL_CARD_CMD_FILECOPY ||
		 exec_command == IPL_CARD_CMD_FILEDELETE )
	{
		if ( exec_result == IPL_CARD_RESULT_BUSY ) {
			playFileOpSound();
			exec_result = getCardLastCMDFCmdResult();
		}
		if ( exec_result != IPL_CARD_RESULT_BUSY &&
			 exec_result != IPL_CARD_RESULT_READY) {
			switch ( exec_command ) {
			case IPL_CARD_CMD_FILEMOVE:		exec_command = IPL_CARD_CMD_FILEMOVE_WAIT;		break;
			case IPL_CARD_CMD_FILEDELETE:	exec_command = IPL_CARD_CMD_FILEDELETE_WAIT;	break;
			case IPL_CARD_CMD_FILECOPY:		exec_command = IPL_CARD_CMD_FILECOPY_WAIT;		break;
			}
			cancelCardIconPos();
			Jac_PlaySe( JAC_SE_ERROR );
		}
	} else if ( exec_command == IPL_CARD_CMD_FORMAT ) {
		if ( exec_result == IPL_CARD_RESULT_BUSY ) {
			exec_result = getCardLastCMDFCmdResult();
		}
		if ( exec_result != IPL_CARD_RESULT_BUSY ) {
			if ( exec_result == IPL_CARD_RESULT_READY ) {
				// 成功
				Jac_PlaySe( JAC_SE_MC_FORMAT );
			} else {
				Jac_PlaySe( JAC_SE_ERROR );
			}
			exec_command = IPL_CARD_CMD_FORMAT_WAIT;
		}
	}
}

static void
updateChangeCardStateSub( u8 no )
{
	SlotState* tSlot = getCardSlotState();

	if ( tSlot[no].changed == TRUE ) {
		// コマンドの実行が終わった。
		if ( tSlot[no].state == SLOT_READY ) {
			updateFileArraySub( no );
		}
		else if ( ( tSlot[no].state == SLOT_ENCODING ) ||
				  ( tSlot[no].state == SLOT_BROKEN ) )	{
			execFormatVerify( no );
		}

		if ( exec_result == IPL_CARD_RESULT_READY &&
//			 exec_chan != sel_slot &&
			 ( getCardLastSendCmd() == IPL_CARD_CMD_UNMOUNT ) &&
			 card_runstate == IPL_CARD_FILE_WAIT ) {
			setCardRunstateFileSelecting( TRUE );
		}
	}

	checkCommandDone();
	
	// スロットにカードがない時。
	if ( tSlot[ no ].state != SLOT_READY ) {
		clearCardIconMoveSlot( no );
	}

	// 選択スロットにカードが無い場合。
	if ( tSlot[ sel_slot ].state == SLOT_NOCARD ) {
		setCardRunstateFileSelecting( FALSE );
		if ( isDistSlot(NULL) ) {
			sel_slot ^= 1;
		}
	} else if ( tSlot[ sel_slot ].state != SLOT_READY &&
				tSlot[ sel_slot ].state != SLOT_BROKEN &&
				tSlot[ sel_slot ].state != SLOT_ENCODING ) {
		setCardRunstateFileSelecting( FALSE );
	}
}

/*-----------------------------------------------------------------------------
	ファイルの並びを更新します。
  -----------------------------------------------------------------------------*/
static void
updateChangeCardState( void )
{
	updateChangeCardStateSub( 0 );
	updateChangeCardStateSub( 1 );
}


/*-----------------------------------------------------------------------------
  全てのアップデートを統括する。
  コントローラの入力から更新する
  -----------------------------------------------------------------------------*/
static s32
updateCardMenuControl( void )
{
	s32 ret = IPL_MENU_MEMORYCARD_SELECT;
	SplashState* ssp = inSplashGetState();

	card_runstate = pre_card_runstate;
	cardwin_runstate = pre_cardwin_runstate;
	formatwin_runstate[0] = pre_formatwin_runstate[0];
	formatwin_runstate[1] = pre_formatwin_runstate[1];

	// カードが挿入されていて、壊れてたりするとマズイので、チェック。
	updateChangeCardState();
    // アイコンのアップデート。
    updateCardIconMove();

	if ( !ssp->force_fatalerror ) {
		switch ( card_runstate )
		{
		case IPL_CARD_FILE_SELECTING:// アイコン選択。
			ret = updateControlMain();
			break;
		case IPL_CARD_FILE_WINDOW:	// copy/move/delの返答待ち。
			ret = updateControlWindow();
			break;
		case IPL_CARD_FILE_VERIFY:	// y/nの返答待ち。
			ret = updateControlVerify();
			break;
		case IPL_CARD_FILE_FORMAT_VERIFY: // フォーマットして良いですか表示。
			ret = updateControlFmtVerify();
			break;
		case IPL_CARD_FILE_WAIT:	// キー入力待ち。
			ret = updateControlWait();
			break;
		}
	}

	return ret;
}













/*-----------------------------------------------------------------------------
  Message section
  -----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
  メッセージのフェーダアップデート。
  -----------------------------------------------------------------------------*/
static void
updateCmdFadeState( u8 no )
{
	DirStateArrayPtr dirState = getCardDirState();
	u32			fileNo;

	fileNo =  sFileArray[no][ sel_x[no] + ( sel_y[no] + now_line[no] ) * 4 ].fileNo;

	if ( isCopyEnable( no, fileNo, NULL ) ){//dirState[no][fileNo].copy_enable ) {
		glrUpdateFade( &copy_mes[no], IPL_WINDOW_FADEIN );
	} else {
		glrUpdateFade( &copy_mes[no], IPL_WINDOW_FADEOUT );
	}

	if ( isMoveEnable( no, fileNo, NULL ) ) {//dirState[no][fileNo].move_enable ) {
		glrUpdateFade( &move_mes[no], IPL_WINDOW_FADEIN );
	} else {
		glrUpdateFade( &move_mes[no], IPL_WINDOW_FADEOUT );
	}
}

/*-----------------------------------------------------------------------------
  各スロットの表示アップデート
  -----------------------------------------------------------------------------*/
static void
updateIconFadeState( SlotState* tSlot, u8 no )
{
	if ( tSlot[no].state == SLOT_READY ) {
		glrUpdateFade( &slot_icon[no], IPL_WINDOW_FADEIN );
		updateCmdFadeState( 0 );
		updateCmdFadeState( 1 );
	} else {
		glrUpdateFade( &slot_icon[no], IPL_WINDOW_FADEOUT );
	}
}
static void
updateCardSlotInfoFadeState( SlotState* tSlot, u8 no )
{
	if ( tSlot[no].state == SLOT_NOCARD ) {
		// スロット？には何もささっていません。
		glrUpdateFade( &mes[ MSG_D400 + no ], IPL_WINDOW_FADEIN );
		initIconAnime(no);
	}
	else {
		glrUpdateFade( &mes[ MSG_D400 + no ], IPL_WINDOW_FADEOUT );
	}
	if ( tSlot[no].state == SLOT_WRONGDEVICE ||
		 tSlot[no].state == SLOT_CANNOT_USE  ||
		 tSlot[no].state == SLOT_BROKEN_NF   ||
		 tSlot[no].state == SLOT_ENCODING_NF  ) {
		// スロット？にささっているものは使えません。
		glrUpdateFade( &mes[ MSG_D402 + no ], IPL_WINDOW_FADEIN );
		initIconAnime(no);
	}
	else  {
		glrUpdateFade( &mes[ MSG_D402 + no ], IPL_WINDOW_FADEOUT );
	}
}

static void
updateCardSlotFormatState( SlotState* tSlot, u8 no, BOOL always )
{
	// formatウィンドウ。
	if ( tSlot[no].state == SLOT_BROKEN ||
		 tSlot[no].state == SLOT_ENCODING ) {
		if ( card_runstate == IPL_CARD_FILE_FORMAT_VERIFY || always ) {
			if ( ( formatwin_runstate[ no ] & 0b00000010 ) == 0 ) {
				if ( no == sel_slot ) {
					glrUpdateFade( &mes[ MSG_D406 + no ], IPL_WINDOW_FADEIN );
				} else {
					glrUpdateFade( &mes[ MSG_D406 + no ], IPL_WINDOW_FADEHALF );
				}
				glrUpdateFade( &mes[ MSG_D408 + no ], IPL_WINDOW_FADEOUT );
			} else {
				glrUpdateFade( &mes[ MSG_D406 + no ], IPL_WINDOW_FADEOUT );
				if ( no == sel_slot ) {
					glrUpdateFade( &mes[ MSG_D408 + no ], IPL_WINDOW_FADEIN );
				} else {
					glrUpdateFade( &mes[ MSG_D408 + no ], IPL_WINDOW_FADEHALF );
				}
			}
		}
		else {
			glrUpdateFade( &mes[ MSG_D406 + no ], IPL_WINDOW_FADEOUT );
			glrUpdateFade( &mes[ MSG_D408 + no ], IPL_WINDOW_FADEOUT );
		}
	}
	else {
		glrUpdateFade( &mes[ MSG_D406 + no ], IPL_WINDOW_FADEOUT );
		glrUpdateFade( &mes[ MSG_D408 + no ], IPL_WINDOW_FADEOUT );
	}
}

static void
updateFadeState( void )
{
	SlotState* tSlot = getCardSlotState();
	u8	no;

	for ( no = 0 ; no < 2 ; no++ ) {
		updateIconFadeState( tSlot, no );	//ここにはアイコンのアップデートは含みません。
		
		if ( card_runstate != IPL_CARD_FILE_WAIT ) {
			updateCardSlotInfoFadeState( tSlot, no );
			updateCardSlotFormatState  ( tSlot, no, FALSE );
		} else {
			// IPL_CARD_FILE_WAIT中。
			if ( ( exec_command != IPL_CARD_CMD_FORMAT &&
				   exec_command != IPL_CARD_CMD_FORMAT_WAIT ) || 
				 exec_chan == no ) {
				glrUpdateFade( &mes[ MSG_D400 + no ], IPL_WINDOW_FADEOUT );
				glrUpdateFade( &mes[ MSG_D402 + no ], IPL_WINDOW_FADEOUT );
				glrUpdateFade( &mes[ MSG_D406 + no ], IPL_WINDOW_FADEOUT );
				glrUpdateFade( &mes[ MSG_D408 + no ], IPL_WINDOW_FADEOUT );
			}
			else if ( exec_chan != no ) {
				updateCardSlotInfoFadeState( tSlot, no );
				updateCardSlotFormatState  ( tSlot, no, TRUE );
			}
		}


		//上下の矢印。
		if ( card_runstate == IPL_CARD_FILE_SELECTING ) {
			if ( now_line[ no ] > 0 ) {
				glrUpdateFade( &arrow_up[no] , IPL_WINDOW_FADEIN  );
			} else {
				glrUpdateFade( &arrow_up[no] , IPL_WINDOW_FADEOUT );
			}
			if ( max_lines[ no ] > now_line[ no ] ) {
				glrUpdateFade( &arrow_down[no] , IPL_WINDOW_FADEIN  );
			} else {
				glrUpdateFade( &arrow_down[no] , IPL_WINDOW_FADEOUT );
			}
		} else {
				glrUpdateFade( &arrow_up[no] , IPL_WINDOW_FADEOUT );
				glrUpdateFade( &arrow_down[no] , IPL_WINDOW_FADEOUT );
		}
	}
}

/*-----------------------------------------------------------------------------
  アイコンアニメのアップデート
  -----------------------------------------------------------------------------*/
static void
updateIconAnime( void )
{
	u8		no;
	u32 	posNo, fileNo;
	IconStateArrayPtr 	icon = getIconStateArray();
	DirStateArrayPtr dirState = getCardDirState();

	for ( no = 0 ; no < 2 ; no++ ) {
		for ( posNo = 0 ; posNo < IPL_MAX_ICON_DISPLAY_NUM ; posNo++ ) {
			fileNo = sFileArray[no][ posNo + now_line[no] * 4 ].fileNo;

            if ( dirState[no][fileNo].validate ) {
				sIconAnime[no][fileNo].now_frame += icon[no][fileNo].icon_bounds;
                
				if ( sIconAnime[no][fileNo].now_frame >= icon[no][fileNo].icon_all_frame ) {
					if ( icon[no][fileNo].icon_anim_style == CARD_STAT_ANIM_BOUNCE ) {
						sIconAnime[no][fileNo].now_frame = (s16)( icon[no][fileNo].icon_all_frame - icon[no][fileNo].icon_anim_last_frame - 1 );
						icon[no][fileNo].icon_bounds = -1;
					} else {
						sIconAnime[no][fileNo].now_frame = 0;
						icon[no][fileNo].icon_bounds = 1;
					}
				}
                
				if ( sIconAnime[no][fileNo].now_frame < 0 ) {
					if ( icon[no][fileNo].icon_anim_style == CARD_STAT_ANIM_BOUNCE ) {
                        sIconAnime[no][fileNo].now_frame = icon[no][fileNo].icon_anim_first_frame;
                    } else {
                        sIconAnime[no][fileNo].now_frame = 0;
                    }
					icon[no][fileNo].icon_bounds = 1;
				}
                
			}
		}
	}

}

/*-----------------------------------------------------------------------------
  コマンドウィンドウの表示アップデート
  -----------------------------------------------------------------------------*/
static void
updateFadeCmdWindow( void )
{
	SlotState* tSlot = getCardSlotState();
	
	if ( tSlot[sel_slot].state == SLOT_READY &&
		 card_runstate == IPL_CARD_FILE_WINDOW ) {
		updateCmdFade( &cmdwnd[sel_slot], IPL_CARD_CMDWND_FADEIN );
		updateCmdFade( &cmdwnd[sel_slot^1], IPL_CARD_CMDWND_FADEOUT );
	}
	else if  ( tSlot[sel_slot].state == SLOT_READY &&
			   card_runstate == IPL_CARD_FILE_VERIFY ) {
		updateCmdFade( &cmdwnd[sel_slot], IPL_CARD_CMDWND_VERIFY );
		updateCmdFade( &cmdwnd[sel_slot^1], IPL_CARD_CMDWND_FADEOUT );
	}
	else {
		updateCmdFade( &cmdwnd[0], IPL_CARD_CMDWND_FADEOUT );
		updateCmdFade( &cmdwnd[1], IPL_CARD_CMDWND_FADEOUT );
	}
}



/*-----------------------------------------------------------------------------
  終了メッセージ表示アップデート
  -----------------------------------------------------------------------------*/
static void
updateFadeCopyMessage( BOOL draw )
{
	if ( draw ) {
		if ( exec_result == IPL_CARD_RESULT_READY )	glrUpdateFade( &mes[ MSG_D200 ], IPL_WINDOW_FADEIN );
		else										glrUpdateFade( &mes[ MSG_D200 ], IPL_WINDOW_FADEOUT );

		if ( exec_result == IPL_CARD_RESULT_NOPERM )	glrUpdateFade( &mes[ MSG_D201 ], IPL_WINDOW_FADEIN );
		else											glrUpdateFade( &mes[ MSG_D201 ], IPL_WINDOW_FADEOUT );

		if ( exec_result == IPL_CARD_RESULT_NOCARD )	glrUpdateFade( &mes[ MSG_D203 - exec_chan ], IPL_WINDOW_FADEIN );
		else 											glrUpdateFade( &mes[ MSG_D203 - exec_chan ], IPL_WINDOW_FADEOUT );

		if ( exec_result == IPL_CARD_RESULT_WRONGDEVICE )	glrUpdateFade( &mes[ MSG_D205 - exec_chan ], IPL_WINDOW_FADEIN );
		else												glrUpdateFade( &mes[ MSG_D205 - exec_chan ], IPL_WINDOW_FADEOUT );

		if ( exec_result == IPL_CARD_RESULT_SECTOR )		glrUpdateFade( &mes[ MSG_D208 ], IPL_WINDOW_FADEIN );
		else												glrUpdateFade( &mes[ MSG_D208 ], IPL_WINDOW_FADEOUT );

		if ( exec_result == IPL_CARD_RESULT_EXIST ) 		glrUpdateFade( &mes[ MSG_D209 ], IPL_WINDOW_FADEIN );
		else												glrUpdateFade( &mes[ MSG_D209 ], IPL_WINDOW_FADEOUT );

		if ( exec_result == IPL_CARD_RESULT_INSSPACE )		glrUpdateFade( &mes[ MSG_D210 ], IPL_WINDOW_FADEIN );
		else												glrUpdateFade( &mes[ MSG_D210 ], IPL_WINDOW_FADEOUT );

		if ( exec_result == IPL_CARD_RESULT_FATAL_ERROR )	glrUpdateFade( &mes[ MSG_D211 ], IPL_WINDOW_FADEIN );
		else												glrUpdateFade( &mes[ MSG_D211 ], IPL_WINDOW_FADEOUT );
	} else {
		// 全メッセージfadeout
		glrUpdateFade( &mes[ MSG_D200 ], IPL_WINDOW_FADEOUT );
		glrUpdateFade( &mes[ MSG_D201 ], IPL_WINDOW_FADEOUT );
		glrUpdateFade( &mes[ MSG_D202 ], IPL_WINDOW_FADEOUT );
		glrUpdateFade( &mes[ MSG_D203 ], IPL_WINDOW_FADEOUT );
		glrUpdateFade( &mes[ MSG_D204 ], IPL_WINDOW_FADEOUT );
		glrUpdateFade( &mes[ MSG_D205 ], IPL_WINDOW_FADEOUT );
		glrUpdateFade( &mes[ MSG_D208 ], IPL_WINDOW_FADEOUT );
		glrUpdateFade( &mes[ MSG_D209 ], IPL_WINDOW_FADEOUT );
		glrUpdateFade( &mes[ MSG_D210 ], IPL_WINDOW_FADEOUT );
		glrUpdateFade( &mes[ MSG_D211 ], IPL_WINDOW_FADEOUT );
	}
}

static void
updateFadeMoveMessage( BOOL draw )
{
	if ( draw ) {
		if ( exec_result == IPL_CARD_RESULT_READY )	glrUpdateFade( &mes[ MSG_D600 ], IPL_WINDOW_FADEIN );
		else										glrUpdateFade( &mes[ MSG_D600 ], IPL_WINDOW_FADEOUT );

		if ( exec_result == IPL_CARD_RESULT_NOPERM )	glrUpdateFade( &mes[ MSG_D601 ], IPL_WINDOW_FADEIN );
		else											glrUpdateFade( &mes[ MSG_D601 ], IPL_WINDOW_FADEOUT );

		if ( exec_result == IPL_CARD_RESULT_NOCARD )	glrUpdateFade( &mes[ MSG_D603 - exec_chan ], IPL_WINDOW_FADEIN );
		else 											glrUpdateFade( &mes[ MSG_D603 - exec_chan ], IPL_WINDOW_FADEOUT );

		if ( exec_result == IPL_CARD_RESULT_WRONGDEVICE )	glrUpdateFade( &mes[ MSG_D605 - exec_chan ], IPL_WINDOW_FADEIN );
		else												glrUpdateFade( &mes[ MSG_D605 - exec_chan ], IPL_WINDOW_FADEOUT );

		if ( exec_result == IPL_CARD_RESULT_SECTOR )	glrUpdateFade( &mes[ MSG_D608 ], IPL_WINDOW_FADEIN );
		else											glrUpdateFade( &mes[ MSG_D608 ], IPL_WINDOW_FADEOUT );

		if ( exec_result == IPL_CARD_RESULT_EXIST ) 	glrUpdateFade( &mes[ MSG_D609 ], IPL_WINDOW_FADEIN );
		else											glrUpdateFade( &mes[ MSG_D609 ], IPL_WINDOW_FADEOUT );

		if ( exec_result == IPL_CARD_RESULT_INSSPACE )	glrUpdateFade( &mes[ MSG_D610 ], IPL_WINDOW_FADEIN );
		else											glrUpdateFade( &mes[ MSG_D610 ], IPL_WINDOW_FADEOUT );

		if ( exec_result == IPL_CARD_RESULT_FATAL_ERROR )	glrUpdateFade( &mes[ MSG_D611 ], IPL_WINDOW_FADEIN );
		else												glrUpdateFade( &mes[ MSG_D611 ], IPL_WINDOW_FADEOUT );
	} else {
		// 全メッセージfadeout
		glrUpdateFade( &mes[ MSG_D600 ], IPL_WINDOW_FADEOUT );
		glrUpdateFade( &mes[ MSG_D601 ], IPL_WINDOW_FADEOUT );
		glrUpdateFade( &mes[ MSG_D602 ], IPL_WINDOW_FADEOUT );
		glrUpdateFade( &mes[ MSG_D603 ], IPL_WINDOW_FADEOUT );
		glrUpdateFade( &mes[ MSG_D604 ], IPL_WINDOW_FADEOUT );
		glrUpdateFade( &mes[ MSG_D605 ], IPL_WINDOW_FADEOUT );
		glrUpdateFade( &mes[ MSG_D608 ], IPL_WINDOW_FADEOUT );
		glrUpdateFade( &mes[ MSG_D609 ], IPL_WINDOW_FADEOUT );
		glrUpdateFade( &mes[ MSG_D610 ], IPL_WINDOW_FADEOUT );
		glrUpdateFade( &mes[ MSG_D611 ], IPL_WINDOW_FADEOUT );
	}
}

static void
updateFadeDeleteMessage( BOOL draw )
{
	if ( draw ) {
		if ( exec_result == IPL_CARD_RESULT_READY )	{
			glrUpdateFade( &mes[ MSG_D300 ], IPL_WINDOW_FADEIN );
			glrUpdateFade( &mes[ MSG_D301 ], IPL_WINDOW_FADEOUT );
		} else {
			glrUpdateFade( &mes[ MSG_D300 ], IPL_WINDOW_FADEOUT );
			glrUpdateFade( &mes[ MSG_D301 ], IPL_WINDOW_FADEIN );
		}
	} else {
		glrUpdateFade( &mes[ MSG_D300 ], IPL_WINDOW_FADEOUT );
		glrUpdateFade( &mes[ MSG_D301 ], IPL_WINDOW_FADEOUT );
	}
}

static void
updateFadeFormatMessage( BOOL draw )
{
	if ( draw ) {
		if ( exec_result == IPL_CARD_RESULT_READY )	{
			glrUpdateFade( &mes[ MSG_D410 + exec_chan ], IPL_WINDOW_FADEIN );
			glrUpdateFade( &mes[ MSG_D412 + exec_chan ], IPL_WINDOW_FADEOUT );
		} else {
			glrUpdateFade( &mes[ MSG_D410 + exec_chan ], IPL_WINDOW_FADEOUT );
			glrUpdateFade( &mes[ MSG_D412 + exec_chan ], IPL_WINDOW_FADEIN );
		}
	} else {
		glrUpdateFade( &mes[ MSG_D410 + exec_chan ], IPL_WINDOW_FADEOUT );
		glrUpdateFade( &mes[ MSG_D412 + exec_chan ], IPL_WINDOW_FADEOUT );
	}
}

static void
updateFadeMessage( void )
{
	if ( card_runstate == IPL_CARD_FILE_WAIT ) {
		if ( exec_result != IPL_CARD_RESULT_BUSY ) {
			// メッセージfadein
			if ( exec_command == IPL_CARD_CMD_FILECOPY ||
				 exec_command == IPL_CARD_CMD_FILECOPY_WAIT ) {
				updateFadeCopyMessage( TRUE );
			}
			if ( exec_command == IPL_CARD_CMD_FILEMOVE ||
				 exec_command == IPL_CARD_CMD_FILEMOVE_WAIT ) {
				updateFadeMoveMessage( TRUE );
			}
			if ( exec_command == IPL_CARD_CMD_FILEDELETE ||
				 exec_command == IPL_CARD_CMD_FILEDELETE_WAIT ) {
				updateFadeDeleteMessage( TRUE );
			}
			if ( exec_command == IPL_CARD_CMD_FORMAT ||
				 exec_command == IPL_CARD_CMD_FORMAT_WAIT ) {
				updateFadeFormatMessage( TRUE );
			}
		}
	}
	else {
		updateFadeCopyMessage( FALSE );
		updateFadeMoveMessage( FALSE );
		updateFadeDeleteMessage( FALSE );
		updateFadeFormatMessage( FALSE );
	}
}

static void
updateArrowAnime( void )
{
	arrow_slide_frame += arrow_slide_bounds;

	if ( arrow_slide_frame < 0 ) {
		arrow_slide_frame = 0;
		arrow_slide_bounds =  1;
	}
	if ( arrow_slide_frame > arrow_slide_frame_max ) {
		arrow_slide_frame = arrow_slide_frame_max;
		arrow_slide_bounds = -1;
	}
}

static void
updateSlotAnime( void )
{
	int i, light_flag_;
	SlotState* tSlot = getCardSlotState();
	
	slot_blink_frame += slot_blink_bounds;

	if ( slot_blink_frame < 0 ) {
		slot_blink_frame = 0;
		slot_blink_bounds = 1;
	}
	if ( slot_blink_frame > slot_blink_frame_max ) {
		slot_blink_frame = slot_blink_frame_max;
		slot_blink_bounds = -1;
	}

	light_flag_ = 0;
	for ( i = 0 ; i < 2 ; i++ ) {
		if ( ( i == sel_slot ) &&
			 ( tSlot[ i ].state == SLOT_READY ) ) {
			glrUpdateFade( &active_slot_fade[i], IPL_WINDOW_FADEIN );
			light_flag_++;
		} else {
			glrUpdateFade( &active_slot_fade[i], IPL_WINDOW_FADEOUT );
		}
	}

	if ( light_flag_ == 0 ) {
		glrUpdateFade( &main_light, IPL_WINDOW_FADEIN );
	} else {
//		glrUpdateFade( &main_light, IPL_WINDOW_FADEHALF );
		glrUpdateFade( &main_light, IPL_WINDOW_FADEOUT );
	}
}

/*-----------------------------------------------------------------------------
  処理はここから振り分けられます。
  -----------------------------------------------------------------------------*/
s32
updateCardMenu( void )
{
	s32	ret = IPL_MENU_MEMORYCARD_SELECT;

	updateFadeState();
	updateIconAnime();
	updateFadeCmdWindow();
	updateFadeMessage();
	updateArrowAnime();
	updateSlotAnime();

	ret = updateCardMenuControl();

	return ret;
}

s32
getCardSelectFileNo( void )
{
	return 	(s32)sFileArray[sel_slot][ sel_x[sel_slot] + ( sel_y[sel_slot] + now_line[sel_slot] ) * 4 ].fileNo;
}

s32
getCardSelectChan( void )
{
	return 	sel_slot;
}

void
resetCardMessage( void )
{
	glrResetFade( &mes[ MSG_D200 ] );
	glrResetFade( &mes[ MSG_D201 ] );
	glrResetFade( &mes[ MSG_D202 ] );
	glrResetFade( &mes[ MSG_D203 ] );
	glrResetFade( &mes[ MSG_D204 ] );
	glrResetFade( &mes[ MSG_D205 ] );
	glrResetFade( &mes[ MSG_D208 ] );
	glrResetFade( &mes[ MSG_D209 ] );
	glrResetFade( &mes[ MSG_D210 ] );
	glrResetFade( &mes[ MSG_D211 ] );
	// move
	glrResetFade( &mes[ MSG_D600 ] );
	glrResetFade( &mes[ MSG_D601 ] );
	glrResetFade( &mes[ MSG_D602 ] );
	glrResetFade( &mes[ MSG_D603 ] );
	glrResetFade( &mes[ MSG_D604 ] );
	glrResetFade( &mes[ MSG_D605 ] );
	glrResetFade( &mes[ MSG_D608 ] );
	glrResetFade( &mes[ MSG_D609 ] );
	glrResetFade( &mes[ MSG_D610 ] );
	glrResetFade( &mes[ MSG_D611 ] );
	// delete
	glrResetFade( &mes[ MSG_D300 ] );
	glrResetFade( &mes[ MSG_D301 ] );
	// format
	glrResetFade( &mes[ MSG_D410 ] );
	glrResetFade( &mes[ MSG_D411 ] );
	glrResetFade( &mes[ MSG_D412 ] );
	glrResetFade( &mes[ MSG_D413 ] );

	glrResetFade( &mes[ MSG_D400 ] );
	glrResetFade( &mes[ MSG_D401 ] );
	glrResetFade( &mes[ MSG_D402 ] );
	glrResetFade( &mes[ MSG_D403 ] );
}
































/*-----------------------------------------------------------------------------
  draw section
  -----------------------------------------------------------------------------*/
static void
setGXforfloatTexCoord( void )
{
	u16		tChanCtrl = GX_SRC_REG;

	GXClearVtxDesc();
	GXSetVtxDesc( GX_VA_POS		, GX_DIRECT );
	GXSetVtxAttrFmt(GX_VTXFMT1, GX_VA_POS , GX_POS_XYZ , GX_S16 , 4);
	GXSetVtxDesc( GX_VA_TEX0	, GX_DIRECT );
	GXSetVtxAttrFmt(GX_VTXFMT1, GX_VA_TEX0, GX_TEX_ST  , GX_F32	, 0);

	GXSetNumChans(1); // default, color = vertex color
	GXSetChanCtrl( GX_COLOR0A0,
				   GX_DISABLE,
				   (GXColorSrc)tChanCtrl,
				   (GXColorSrc)tChanCtrl,
				   GX_NONE,
				   GX_DF_CLAMP,
				   GX_AF_NONE);

	GXSetNumTexGens( 1 );
	GXSetTexCoordGen( GX_TEXCOORD0, GX_TG_MTX2x4, GX_TG_TEX0, GX_IDENTITY );

	GXSetNumTevStages(1);
	GXSetTevOrder(GX_TEVSTAGE0, GX_TEXCOORD0, GX_TEXMAP0, GX_COLOR0A0);
//    GXSetTevOrder(GX_TEVSTAGE0, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
    GXSetTevOrder(GX_TEVSTAGE1, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
    GXSetTevOrder(GX_TEVSTAGE2, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
    GXSetTevOrder(GX_TEVSTAGE3, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
    GXSetTevOrder(GX_TEVSTAGE4, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
    GXSetTevOrder(GX_TEVSTAGE5, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
    GXSetTevOrder(GX_TEVSTAGE6, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
    GXSetTevOrder(GX_TEVSTAGE7, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
	
	GXSetTevColorIn( GX_TEVSTAGE0, GX_CC_ZERO, GX_CC_ONE,  GX_CC_RASC, GX_CC_ZERO );
	GXSetTevColorOp( GX_TEVSTAGE0, GX_TEV_ADD, GX_TB_ZERO, GX_CS_SCALE_1, GX_TRUE, GX_TEVPREV);
	GXSetTevAlphaIn( GX_TEVSTAGE0, GX_CA_ZERO, GX_CA_TEXA, GX_CA_RASA, GX_CA_ZERO );
	GXSetTevAlphaOp( GX_TEVSTAGE0, GX_TEV_ADD, GX_TB_ZERO, GX_CS_SCALE_1, GX_TRUE, GX_TEVPREV);

	GXSetColorUpdate(GX_TRUE);
	GXSetAlphaUpdate(GX_TRUE);
	GXSetBlendMode( GX_BM_BLEND , GX_BL_SRCALPHA, GX_BL_INVSRCALPHA, GX_LO_CLEAR );
	GXSetZMode( GX_FALSE, GX_LEQUAL ,GX_FALSE);

	GXSetCullMode( GX_CULL_BACK );
}
/*-----------------------------------------------------------------------------
  blocknumテクスチャ内の決まったテクスチャをx,yを中心に描画します。
  -----------------------------------------------------------------------------*/
static void
drawNumString( int x, int y, int tex_num )
{
	s16 tex_x = (s16)( num_timg->width * 16 / 11 ); // '0123456789/'で11文字。
	s16 tex_y = (s16)( num_timg->height * 16 );
	f32	texCoord_x, texCoord_y, texCoord_delta;

	texCoord_x = 1.0f * tex_num / 11.f;
	texCoord_delta = 1.0f / 11.f;
	texCoord_y = 1.0f;

	setGXforfloatTexCoord();

	x -= tex_x / 2;
	y -= tex_y / 2;

	loadTexture( 0, num_timg );

	GXBegin(GX_QUADS, GX_VTXFMT1, 4);
	GXPosition3s16((s16)( x ),
				   (s16)( y ),
				   0 );
	GXTexCoord2f32( texCoord_x, 0.f );
	GXPosition3s16((s16)( x + tex_x ),
				   (s16)( y ),
				   0 );
	GXTexCoord2f32( texCoord_x + texCoord_delta, 0.f );
	GXPosition3s16((s16)( x + tex_x ),
				   (s16)( y + tex_y ),
				   0 );
	GXTexCoord2f32( texCoord_x + texCoord_delta, 1.f );
	GXPosition3s16((s16)( x ),
				   (s16)( y + tex_y ),
				   0 );
	GXTexCoord2f32( texCoord_x, 1.f );
	GXEnd();
}

static void
drawTextureNum( int x, int y, int num )
{
	int tex_x = num_timg->width *16 / 11; // '0123456789/'で11文字。
	int tex_y = num_timg->height * 16;
	tex_x += 32;

	if ( num < 10 ) {
		// 1桁
		drawNumString( x, y, num%10 );
	}
	else if ( num < 100 ) {
		// 2桁
		x -= tex_x / 2;
		drawNumString( x, y, (num/10)%10 );
		x += tex_x ;
		drawNumString( x, y, num%10 );
	}
	else if ( num < 1000 ) {
		// 3桁
		x -= tex_x;
		drawNumString( x, y, (num/100)%10 );
		x += tex_x ;
		drawNumString( x, y, (num/10)%10 );
		x += tex_x ;
		drawNumString( x, y, num%10 );
	}
	else {
		// 4桁
		x -= tex_x * 3 / 2;
		drawNumString( x, y, (num/1000)%10 );
		x += tex_x ;
		drawNumString( x, y, (num/100)%10 );
		x += tex_x ;
		drawNumString( x, y, (num/10)%10 );
		x += tex_x ;
		drawNumString( x, y, num%10 );
	}

}

/*-----------------------------------------------------------------------------
  アイコンのシートを描画します。
  -----------------------------------------------------------------------------*/
static s32
loadIconTexture( s32 no, s32 fileNo , s32 tex_no )
{
	ResTIMG		tex_image;
	void*		tex;
	
	IconStateArrayPtr 	icon = getIconStateArray();

	tex = (char*)&icon[no][fileNo] + icon[no][fileNo].icon_offset[tex_no];

	if ( !icon[no][fileNo].icon_enable ) {
		return FALSE;
	}
	tex_image.enableLOD       = GX_DISABLE;
	tex_image.enableEdgeLOD   = GX_DISABLE;
	tex_image.enableBiasClamp = GX_DISABLE;
	tex_image.enableMaxAniso  = (u8)GX_ANISO_1;
	tex_image.width  = 32;
	tex_image.height = 32;
	tex_image.wrapS  = GX_CLAMP;
	tex_image.wrapT  = GX_CLAMP;
	tex_image.format = icon[no][fileNo].icon_format[tex_no];
	tex_image.minFilter = GX_LINEAR;
	tex_image.magFilter = GX_LINEAR;
	tex_image.minLOD    = 0;
	tex_image.maxLOD    = 0;
	tex_image.LODBias   = 0;

	if ( icon[no][fileNo].icon_format[tex_no] == GX_TF_RGB5A3 ) {
		//		DIReport("++++++++++++++++++++++++++++++++++++++++++++++++RGB5A3\n");
		tex_image.indexTexture    = FALSE;
		tex_image.imageOffset = (s32)( (u32)tex - (u32)&tex_image );

		loadTexture( 0, &tex_image );

		return TRUE;
	}
	else if ( icon[no][fileNo].icon_format[tex_no] == GX_TF_C8 ) {
		//		DIReport("++++++++++++++++++++++++++++++++++++++++++++++++C8\n");
		void*		tlut;
		tlut = (char*)&icon[no][fileNo] + icon[no][fileNo].icon_palette_offset;
		tex_image.indexTexture    = TRUE;
		tex_image.colorFormat     = (u8)GX_TL_RGB5A3;
		tex_image.numColors       = 256;
		tex_image.paletteOffset   = (s32)( (u32)tlut - (u32)&tex_image );
		tex_image.imageOffset     = (s32)( (u32)tex  - (u32)&tex_image );

		loadTexture( 0, &tex_image );

		return TRUE;
	}
	return FALSE;
}
s32
loadIconAnime( s32 no, s32 fileNo )
{
	IconStateArrayPtr 	icon = getIconStateArray();
	s16		i = 0;
	int		all_frame = 0;

	if ( no != 0 && no != 1 ) return FALSE;
	if ( fileNo < 0 || fileNo > 127 ) return FALSE;

	do {
		all_frame += CARDGetIconSpeed( &icon[no][fileNo], i ) * 4;
		i++;
	} while ( ( sIconAnime[no][fileNo].now_frame > all_frame ) &&
			  ( i <= 8 ) );

	return loadIconTexture( no, fileNo, (u8)( i-1 ) );
}

static void
drawIconSheet( u8 no, s16 posNo, u8 tex_no, u8 alpha )
{
	IconStateArrayPtr 	icon = getIconStateArray();
	int			icon_size = 32;
	GXColor		white = (GXColor){255,255,255,alpha};
	u32 		b_fileNo = sFileArray[sel_slot][ sel_x[ sel_slot ] + ( sel_y[ sel_slot ] + now_line[ sel_slot ] ) * 4 ].fileNo;
	s32 		fileNo = (s32)sFileArray[no][ posNo + now_line[no] * 4 ].fileNo;

	if ( !loadIconTexture( no, fileNo, tex_no ) ) {
		return;
	}

	if ( no == sel_slot &&
		 posNo == sel_x[no] + sel_y[no] * 4 ) {
		icon_size = 64;
	}

	gSetGXforDirect( TRUE, FALSE, TRUE );
	if ( b_fileNo == fileNo &&
		 no == sel_slot &&
		 !icon[no][fileNo].banner_enable ) {
		glrDrawPictureTex( 'ic00', mem_lay, &white, NULL );
	}
}

/*-----------------------------------------------------------------------------
  アイコンのアニメーションを描画
  -----------------------------------------------------------------------------*/
static void
drawIconAnime( u8 no, s16 posNo, u8 alpha )
{
	IconStateArrayPtr 	icon = getIconStateArray();
	s16		i = 0;
	int		all_frame = 0;
	u32 	fileNo = sFileArray[no][ posNo + now_line[no] * 4 ].fileNo;

	do {
		all_frame += CARDGetIconSpeed( &icon[no][fileNo], i ) * 4;
		i++;
	} while ( ( sIconAnime[no][fileNo].now_frame > all_frame ) &&
			  ( i <= 8 ) );
//	} while ( sIconAnime[no][fileNo].now_frame > all_frame );

	drawIconSheet( no, posNo, (u8)( i-1 ), alpha );

	return;
}

/*-----------------------------------------------------------------------------
  四角を描画。
  -----------------------------------------------------------------------------*/
static void
drawIconQuad( u8 no, s16 posNo, u8 alpha )
{
	int		icon_size = 24;
	s16		temp_alpha;
	u32		color = 0x202060c0;

	temp_alpha = (s16)( ( color & 0xff ) * alpha / 255 );

	color = color & 0xffffff00 + (u8)temp_alpha;

	if ( no == sel_slot &&
		 posNo == ( sel_x[no] + sel_y[no] * 4 ) ) {
		icon_size = 48;
	}

	gSetGXforDirect( FALSE, TRUE, FALSE );

	GXBegin(GX_QUADS, GX_VTXFMT1, 4);
	GXPosition3s16((s16)( (icon_x[no][posNo]-icon_size*8)  ),
				   (s16)( (icon_y[no][posNo]-icon_size*8)  ),
				   0 );
	GXColor1u32( color );
	GXPosition3s16((s16)( (icon_x[no][posNo]+icon_size*8)  ),
				   (s16)( (icon_y[no][posNo]-icon_size*8)  ),
				   0 );
	GXColor1u32( color );

	GXPosition3s16((s16)( (icon_x[no][posNo]+icon_size*8)  ),
				   (s16)( (icon_y[no][posNo]+icon_size*8)  ),
				   0 );
	GXColor1u32( color );

	GXPosition3s16((s16)( (icon_x[no][posNo]-icon_size*8)  ),
				   (s16)( (icon_y[no][posNo]+icon_size*8)  ),
				   0 );
	GXColor1u32( color );
	GXEnd();
}
/*-----------------------------------------------------------------------------
  2Dの描画
  -----------------------------------------------------------------------------*/
static s16
getArrowSlideY( void )
{
	s16	y;

	y = (s16)gHermiteInterpolation( arrow_slide_frame,
								0, 0, 0,
								arrow_slide_frame_max, 8*16, 0 );
	return y;
}

static void
drawUpDownArrow( u8 no, GXColor* arrow )
{
	s16		alpha_up, alpha_down;
	u8		base_alpha = arrow->a;
	
	glrGetOffsetAlpha( &arrow_up[no]  , &alpha_up  , NULL );
	glrGetOffsetAlpha( &arrow_down[no], &alpha_down, NULL );

	arrow->a = (u8)( base_alpha * alpha_up / 255 );
	glrDrawPictureOffset( arrow_up[no].tag, mem_lay, arrow, 0, getArrowSlideY() );

	arrow->a = (u8)( base_alpha * alpha_down / 255 );
	glrDrawPictureOffset( arrow_down[no].tag, mem_lay, arrow, 0, (s16)(-getArrowSlideY()) );

	arrow->a = base_alpha;
}

static void
drawSlotInfo( u8 no, SlotState* tSlot, GXColor color )
{
	CardState*		csp = getCardState();

	if ( csp->enable_slot_effect & IPL_CARD_EFFECT_DEACTIVE_FADEOUT ) {
		u8 alpha_;
		s16		temp_alpha;
		glrGetOffsetAlpha( &active_slot_fade[no], &temp_alpha, NULL );
		alpha_ = (u8)( csp->slot_blink_base * temp_alpha / 255 + ( 255 - csp->slot_blink_base ) );
		color.a = (u8)( color.a * alpha_ / 255 );
	}
	if ( csp->enable_slot_effect & IPL_CARD_EFFECT_ACTIVE_BLINK ) {
		if ( no == sel_slot ) {
			u8	alpha_ = (u8)( csp->slot_blink_base * slot_blink_frame / slot_blink_frame_max + ( 255 - csp->slot_blink_base ) );
			color.a = (u8)( color.a * alpha_ / 255 );
		}
	}
	// スロットテクスチャ（A/B）表示。
	glrDrawPicture( 'texa' + no, mem_lay, &color );

	// 空き容量表示。
	GXSetChanMatColor( GX_COLOR0A0, color );
	drawTextureNum( num_timg_x[0+no], num_timg_y[0+no], tSlot[ no ].free_blocks );

	glrDrawWindow ( 'ffra' + no, mem_lay, &color );
	setGXforglrText();
	glrDrawString( (u16)( MSG_D700 + no ), mem_str, mem_lay, &color );
}

static void
drawMemEditTexture( u8 no, SlotState* tSlot, GXColor* color )
{
	s16		fade_alpha;
	
	u8		backup_alpha;

	setGXforglrPicture();

	glrGetOffsetAlpha( &slot_icon[no], &fade_alpha, NULL );

	backup_alpha = color->a;
	color->a = (u8)( color->a * fade_alpha / 255 );

	drawSlotInfo( no, tSlot, *color );

	setGXforglrPicture();
	drawUpDownArrow( no, color );

	// バナー表示。
	if ( no == sel_slot ) {
		gSetGXforDirect( TRUE, FALSE, TRUE );
		if ( tSlot[ sel_slot ].state == SLOT_READY ) {
			if ( loadbannerTexture() ) {
				glrDrawPictureTex( 'bana', mem_lay, color, NULL );
			}
			drawFileInfo( color->a );
		}
	}

	color->a = backup_alpha;
}

/*-----------------------------------------------------------------------------
  アイコン群を描画します。
  -----------------------------------------------------------------------------*/
static void
drawFileIconSub( u8 no, u8 alpha )//, u8 flag )
{
	s16 i;
	s16 fade_alpha;
	DirStateArrayPtr dirState = getCardDirState();

	glrGetOffsetAlpha( &slot_icon[no], &fade_alpha, NULL );

	alpha = (u8)( alpha * fade_alpha / 255 );

	// モデルを描画する。
	
	setGXforglrPicture();
	for ( i = 0 ; i < IPL_MAX_ICON_DISPLAY_NUM ; i++ )	{
		u32 fileNo = sFileArray[no][ i + now_line[no] * 4 ].fileNo;
		if ( dirState[no][fileNo].validate ) {
			drawIconAnime( no, i ,alpha);//, (u8)GX_TEXMAP0 );
		}
		else {
			// 何もない四角を書く。
//			drawIconQuad( no, i ,alpha);
		}
	}
}



/*-----------------------------------------------------------------------------
  フォーマット確認（再確認）ウィンドウを開く。
  -----------------------------------------------------------------------------*/
static void
getCmdWinWidthHeight(
	u16		id,
	gLRStringHeader* s_hdr,
	gLayoutHeader* l_hdr,
	u16*	width,
	u16*	height,
	u16*	one_height
	)
{
	u16		w, max_w, i;
	u8*		string;
	gLRTextBoxData*	tbp = (gLRTextBoxData*)( (u8*)l_hdr + l_hdr->textbox_offset );
	gLRStringData*  sdp = (gLRStringData* )( (u8*)s_hdr + sizeof(gLRStringHeader) );

	max_w = 0;
	w = 0;
	for ( i = 0 ; i < 2 ; i ++ ) {
		string = (u8*)&sdp[ MSG_D100 + i ] + sdp[ MSG_D100 + i ].string_offset;	// はい
		gfGetWidthHeight( &tbp[ sdp[ id ].textbox_id ], (char*)string, &w, one_height );	//[はい/いいえ]の縦横幅取得。
		if ( max_w < w ) max_w = w;
	}
	*height = (u16)( *one_height + tbp[ sdp[id].textbox_id ].line_space * 16 ); // 2行分
	*width = max_w;
}

static void
drawFmtVerifyWindow( u16 id, u8 no, u8 alpha )
{
	MainState*	    msp = getMainState();
	CardState*		csp = getCardState();
	u16	total_height;
	u16	mes_width = 0, mes_height = 0;
	u16 cmd_width, cmd_height = 0, cmd_one_height = 0;
	s16	offset_y;
	u16	cmd_x, cmd_y;
	u16 i;
	s16	glr_alpha;
	int glr_offset;
	gLRTextBoxData*	tbp = (gLRTextBoxData*)( (u8*)mem_lay + mem_lay->textbox_offset );
	gLRStringData*  sdp = (gLRStringData* )( (u8*)mem_str + sizeof(gLRStringHeader) );
	gLRTextBoxData  temp_tbd;
	GXColor		white;//  = {255,255,255,255};
	GXColor		yellow;// = {192,192,  0,255};

	white.r = (u8)csp->moji_sel_r;
	white.g = (u8)csp->moji_sel_g;
	white.b = (u8)csp->moji_sel_b;
	white.a = 255;
	yellow.r = (u8)csp->moji_back_r;
	yellow.g = (u8)csp->moji_back_g;
	yellow.b = (u8)csp->moji_back_b;
	yellow.a = 255;
	
	gSet2DCamera();

	glrGetOffsetAlpha( &mes[ id + no ], &glr_alpha, &glr_offset );
	white.a = (u8)( alpha * glr_alpha / 255 );
	yellow.a =(u8)( alpha * glr_alpha / 255 );
	
	setGXforglrText();
	
	gfGetWidthHeight( &tbp[ sdp[ id + no ].textbox_id ],
					  (char*)( (u8*)&sdp[id+no] + sdp[id+no].string_offset ),
					  &mes_width,
					  &mes_height );
	getCmdWinWidthHeight( (u16)( id + no ),
						  mem_str,
						  mem_lay,
						  &cmd_width,
						  &cmd_height,
						  &cmd_one_height );
	mes_height += msp->window_mergin * 16 * 2;	// ウィンドウの隙間を取る。
	cmd_height += msp->window_mergin * 16 * 2;	// ウィンドウの隙間を取る。
	total_height = (u16)( mes_height + cmd_height + getCardState()->fmtwnd_mergin * 16 );

	offset_y = (s16)( - total_height / 2  + mes_height / 2 );
	// メッセージ文字の表示
	glrDrawStringOffset( (u16)( id + no ), mem_str, mem_lay, &white, 0, offset_y );
	
	if ( mes[id+no].now_frame > (mes[id+no].fade_frame/2) ) {
		white.a  = (u8)( alpha * ( mes[id+no].now_frame - mes[id+no].fade_frame / 2 ) * 2 / mes[id+no].fade_frame );
		yellow.a = (u8)( alpha * ( mes[id+no].now_frame - mes[id+no].fade_frame / 2 ) * 2 / mes[id+no].fade_frame );
	} else {
		white.a  = 0;
		yellow.a = 0;
	}

	// コマンドウィンドウの表示
	cmd_x = (u16)( tbp[ sdp[ id + no ].textbox_id ].x - ( cmd_width / 2 + msp->window_mergin * 16 ) );
	cmd_y = (u16)( tbp[ sdp[ id + no ].textbox_id ].y + total_height / 2 - cmd_height );
	glrDrawWindowPosID( tbp[ sdp[ id + no ].textbox_id ].windowID,
						mem_lay,
						&white,
						cmd_x,
						cmd_y,
						cmd_width  + msp->window_mergin * 32,
						cmd_height ); //+ msp->window_mergin * 32 );
	drawEdgeAnimeSub( 	cmd_x,
						cmd_y,
						cmd_width  + msp->window_mergin * 32,
						cmd_height , //+ msp->window_mergin * 32,
						(u8)( mes[id+no].anime_frame * 5 & 0xff ),
						&white
						);
	temp_tbd = tbp[ sdp[ id + no ].textbox_id ];
	temp_tbd.y = (u16)( cmd_y + msp->window_mergin * 16 + cmd_one_height / 2 );
	temp_tbd.windowID = 0xffff;
	setGXforglrText();
	for ( i = 0 ; i < 2 ; i++ ) {
		BOOL	brite;
		gfSetBackFontGXColor( &yellow, &yellow );
		gfSetFontGXColor( &white, &white );
		if ( id == MSG_D406 ) {
			brite = ( ((( formatwin_runstate[no] & 0b10 ) == 0b10 && i == 0 ) ||
					   (( formatwin_runstate[no] & 0b10 ) != 0b10 && (formatwin_runstate[no] & 1) == i) )
					  && sel_slot == no );
		} else {
			brite = ( ( formatwin_runstate[no] & 1 ) == i && sel_slot == no );
		}
		if ( brite ) {
			gfBritePrint( &temp_tbd, (char*)( (u8*)&sdp[MSG_D100 + i] + sdp[MSG_D100 + i].string_offset ) );
		} else {
			gfPrint( &temp_tbd, (char*)( (u8*)&sdp[MSG_D100 + i] + sdp[MSG_D100 + i].string_offset ) );
		}
		temp_tbd.y += temp_tbd.line_space * 16;
	}
}

static void
drawFileLayout( u8 no, u8 alpha )
{
	SlotState* tSlot = getCardSlotState();
	GXColor	white = { 255, 255, 255, 255 };

	white.a = (u8)( white.a * alpha / 255 );

	// 各スロット毎の情報の表示（アイコンを除く）
	drawMemEditTexture( no, tSlot, &white );
	drawFileIconSub( no, alpha );

	// 各スロット毎のメッセージの描画。
	setGXforglrText();
	glrDrawFadeString( &mes[ MSG_D400 + no ], mem_str, mem_lay, &white );
	glrDrawFadeString( &mes[ MSG_D402 + no ], mem_str, mem_lay, &white );
	drawFmtVerifyWindow( MSG_D406 , no, alpha );
	drawFmtVerifyWindow( MSG_D408 , no, alpha );
}










/*-----------------------------------------------------------------------------
  コマンドウィンドウを表示するシーケンス。
  -----------------------------------------------------------------------------*/
static void
initCmdFade( CmdFade* wp )
{
	wp->now_fade_frame = 0;
	wp->end_fade_frame = 20;
	wp->now_mini_frame = 0;
	wp->end_mini_frame = 30;
	wp->moji_frame  = 10;
	wp->veri_frame	= 20;
	wp->max_alpha = 255;

	wp->slide_y = 40;	// 10pixels
	wp->veri_slide_y = 20;
	wp->tag = 0;
}

static void
updateCmdFade( CmdFade* wp, u8 flag )
{
	switch ( flag )
	{
	case IPL_CARD_CMDWND_FADEIN:
		wp->now_fade_frame++;
		if ( wp->now_fade_frame > wp->end_fade_frame ) wp->now_fade_frame = wp->end_fade_frame;
		wp->now_mini_frame--;
		if ( wp->now_mini_frame < 0 ) wp->now_mini_frame = 0;
		break;

	case IPL_CARD_CMDWND_VERIFY:
		wp->now_fade_frame++;
		if ( wp->now_fade_frame > wp->end_fade_frame ) wp->now_fade_frame = wp->end_fade_frame;
		wp->now_mini_frame++;
		if ( wp->now_mini_frame > wp->end_mini_frame ) wp->now_mini_frame = wp->end_mini_frame;
		break;

	case IPL_CARD_CMDWND_FADEOUT:
		wp->now_fade_frame--;
		if ( wp->now_fade_frame < 0 ) wp->now_fade_frame = 0;
		wp->now_mini_frame--;
		if ( wp->now_mini_frame < 0 ) wp->now_mini_frame = 0;
		break;
	}
	wp->anime_frame += getCardState()->cmdwnd_effect_speed;
}

static void
getCmdOffsetAlpha( CmdFade* wp, s16* alpha, int* offset_y)
{
	*alpha = (s16)( wp->max_alpha * wp->now_fade_frame / wp->end_fade_frame );
	*offset_y = wp->slide_y * 16 * ( wp->end_fade_frame - wp->now_fade_frame ) / wp->end_fade_frame;
}


static void
getVerifyOffsetAlpha( CmdFade* wp, s16* alpha, int* offset_y)
{
	if ( wp->now_mini_frame >= wp->veri_frame ) {
		*alpha = (s16)( wp->max_alpha *
						( wp->now_mini_frame - wp->veri_frame )	/ (wp->end_mini_frame - wp->veri_frame) );
		*offset_y = wp->veri_slide_y * 16 *
						( wp->end_mini_frame - wp->now_mini_frame )	/ (wp->end_mini_frame - wp->veri_frame) ;
	}
	else {
		*alpha = 0;
		*offset_y = 0;
	}
}






static void
drawEdgeAnimeSub( int x, int y, int dx, int dy, int delta, GXColor* rasc )
{
	u32 alpha;
	u32	color = 0xc0c0c000;

	setGXforLine();

	x -= delta + 16;
	y -= delta + 16;
	dx += delta * 2 + 32;
	dy += delta * 2 + 32;
	alpha = (u32)( 255 * ( 16 * 16 - delta ) / ( 16 * 16 ) );

	if ( alpha > 255 ) alpha = 255;
	if ( alpha < 0   ) alpha = 0;
	
	alpha = alpha * rasc->a / 255;
	color += alpha & 0xff;

	GXSetLineWidth( 18 , GX_TO_ZERO );
	GXBegin( GX_LINES, GX_VTXFMT1, 8 );
	GXPosition3s16( (s16)( x      ), (s16)( y      ),0 ); GXColor1u32( color );
	GXPosition3s16( (s16)( x + dx ), (s16)( y      ),0 ); GXColor1u32( color );
	GXPosition3s16( (s16)( x      ), (s16)( y      ),0 ); GXColor1u32( color );
	GXPosition3s16( (s16)( x      ), (s16)( y + dy ),0 ); GXColor1u32( color );

	GXPosition3s16( (s16)( x      ), (s16)( y + dy ),0 ); GXColor1u32( color );
	GXPosition3s16( (s16)( x + dx ), (s16)( y + dy ),0 ); GXColor1u32( color );
	GXPosition3s16( (s16)( x + dx ), (s16)( y      ),0 ); GXColor1u32( color );
	GXPosition3s16( (s16)( x + dx ), (s16)( y + dy ),0 ); GXColor1u32( color );
	GXEnd();
}

static void
drawEdgeAnim(
	CmdFade* wp,
	int x, int y, int dx, int dy, GXColor* rasc )
{
	int	delta;

	delta = wp->anime_frame & 0xff;
	drawEdgeAnimeSub( x,y,dx,dy, delta, rasc );
}

static void
getWindowWidthHeight(
	CmdFade*		wp,
	gLRStringHeader* s_hdr,
	gLayoutHeader* l_hdr,
	u16*	width
	)
{
	u16		w, max_w, id, i;
	u8*		string;
	u16		temp_height;
	gLRTextBoxData*	tbp = (gLRTextBoxData*)( (u8*)l_hdr + l_hdr->textbox_offset );
	gLRStringData*  sdp = (gLRStringData* )( (u8*)s_hdr + sizeof(gLRStringHeader) );

	// ウィンドウの幅を取得するために
	id = MSG_D000;

	max_w = 0;
	for ( i = 0 ; i < 3 ; i ++ ) {
		string = (u8*)&sdp[id+i] + sdp[id+i].string_offset;	// 移動
		gfGetWidthHeight( &tbp[ sdp[id+i].textbox_id ], (char*)string, &w, &wp->cmd_height );	//[移動]の縦横幅取得。
		if ( max_w < w ) max_w = w;
	}
	wp->full_height =  (u16)( wp->cmd_height +
							  tbp[ sdp[id].textbox_id ].line_space * 32 );
	if ( wp->now_mini_frame < wp->moji_frame ) {
		temp_height = (u16)( tbp[ sdp[id].textbox_id ].line_space * 32 );
	}
	else if ( wp->now_mini_frame >= wp->moji_frame &&
			  wp->now_mini_frame < wp->veri_frame ) {
		temp_height = (u16)( tbp[ sdp[id].textbox_id ].line_space * 32
							 * ( wp->veri_frame - wp->now_mini_frame) / ( wp->veri_frame - wp->moji_frame ) );
	}
	else if ( wp->now_mini_frame >= wp->veri_frame ) {
		temp_height = 0;
	}
	wp->select_height = (u16)( wp->cmd_height + temp_height );
	wp->verify_height = (u16)( wp->cmd_height +
							   tbp[ sdp[id].textbox_id ].line_space * 16 );
	// コマンドの3行分の高さ(heightは毎行同じとする。)
	*width = max_w;
}

static void
getStringColorforCmdWin(
	GXColor*	color,
	u8			alpha
	)
{
	CardState*	csp = getCardState();
	color->r = (u8)( color->r * ( csp->moji_half + alpha ) / ( csp->moji_half + 127 ) );
	color->g = (u8)( color->g * ( csp->moji_half + alpha ) / ( csp->moji_half + 127 ) );
	color->b = (u8)( color->b * ( csp->moji_half + alpha ) / ( csp->moji_half + 127 ) );
}

static void
drawStringforCmdWin(
	u8				no,
	CmdFade*		wp,
	gLRStringHeader* s_hdr,
	gLayoutHeader* l_hdr,
	GXColor* rasc,
	int center_x,
	int top_y,
	int	width )
{
	CardState*	csp = getCardState();
	u16		i, id, temp_height;
	s16		temp_alpha;
	GXColor	white;//  = { 255, 255, 255, 255 };
	GXColor yellow;// = { 192, 192,   0, 255 };
	GXColor cmd_yellow, cmd_white;
	s16		cm_alpha, flag_alpha[2];
	gLRTextBoxData*	tbp = (gLRTextBoxData*)( (u8*)l_hdr + l_hdr->textbox_offset );
	gLRStringData*  sdp = (gLRStringData*)( (u8*)s_hdr + sizeof(gLRStringHeader) );
	gLRTextBoxData  temp_tbd;

	white.r = (u8)csp->moji_sel_r;
	white.g = (u8)csp->moji_sel_g;
	white.b = (u8)csp->moji_sel_b;
	white.a = 255;
	yellow.r = (u8)csp->moji_back_r;
	yellow.g = (u8)csp->moji_back_g;
	yellow.b = (u8)csp->moji_back_b;
	yellow.a = 255;

	id = MSG_D000;
	temp_tbd = tbp[ sdp[id].textbox_id ];

	temp_tbd.x  = (u16)center_x;
	temp_tbd.y  = (u16)( top_y + wp->cmd_height / 2 );
	temp_tbd.dx = (u16)width;
	temp_tbd.dy = (u16)wp->cmd_height;

	if ( wp->now_mini_frame < wp->moji_frame ) {
		temp_alpha = (s16)( rasc->a * ( wp->moji_frame - wp->now_mini_frame ) / wp->moji_frame );
	} else {
		temp_alpha = 0;
	}
	white.a = (u8)temp_alpha;
	yellow.a = rasc->a;

	setGXforglrText();
	
	if ( wp->now_mini_frame < wp->moji_frame ) {
		temp_height = (u16)( temp_tbd.line_space * 16 );
	}
	else if ( wp->now_mini_frame >= wp->moji_frame &&
			  wp->now_mini_frame < wp->veri_frame ) {
		temp_height = (u16)( temp_tbd.line_space * 16
							 * ( wp->veri_frame - wp->now_mini_frame) / ( wp->veri_frame - wp->moji_frame ) );
	}
	else if ( wp->now_mini_frame >= wp->veri_frame ) {
		temp_height = 0;
	}

	glrGetOffsetAlpha( &slot_icon[no^1] , &cm_alpha,   NULL );
	glrGetOffsetAlpha( &copy_mes[no]    , &flag_alpha[IPL_CARD_WINDOW_COPY], NULL );
	glrGetOffsetAlpha( &move_mes[no]    , &flag_alpha[IPL_CARD_WINDOW_MOVE], NULL );
	cm_alpha /= 2;
	flag_alpha[IPL_CARD_WINDOW_COPY] /= 2;
	flag_alpha[IPL_CARD_WINDOW_MOVE] /= 2;

	for ( i = 0 ; i < 3 ; i++ ) {
		if ( i == 0 || i == 1 ) {
			cmd_yellow = yellow;
			cmd_white  = white;
			//getStringColorforCmdWin( &cmd_yellow, (u8)flag_alpha[i] );
			getStringColorforCmdWin( &cmd_white , (u8)flag_alpha[i] );
			
			if ( cardwin_runstate == i ) {
				if ( card_runstate == IPL_CARD_FILE_WINDOW ) {
					gfSetBackFontGXColor( &cmd_yellow, &cmd_yellow );
					cmd_white.a = cmd_yellow.a;
					gfSetFontGXColor( &cmd_white, &cmd_white );
					gfBritePrint( &temp_tbd, (char*)( (u8*)&sdp[id + i] + sdp[id + i].string_offset ) );
				} else {
					//gfSetFontGXColor( &cmd_white, &cmd_white );
					gfSetFontColor( 0xffffff00 + rasc->a , 0xffffff00 + rasc->a );
					gfPrint( &temp_tbd, (char*)( (u8*)&sdp[id + i] + sdp[id + i].string_offset ) );
				}
			} else {
				gfSetFontGXColor( &cmd_white, &cmd_white );
				gfPrint( &temp_tbd, (char*)( (u8*)&sdp[id + i] + sdp[id + i].string_offset ) );
			}
		} else {
			if ( cardwin_runstate == i ) {
				if ( card_runstate == IPL_CARD_FILE_WINDOW ) {
					gfSetBackFontGXColor( &yellow, &yellow );
					gfSetFontColor( 0xffffff00 + rasc->a , 0xffffff00 + rasc->a );
					gfBritePrint( &temp_tbd, (char*)( (u8*)&sdp[id + i] + sdp[id + i].string_offset ) );
				} else {
					gfSetFontColor( 0xffffff00 + rasc->a , 0xffffff00 + rasc->a );
					gfPrint( &temp_tbd, (char*)( (u8*)&sdp[id + i] + sdp[id + i].string_offset ) );
				}
			} else {
				gfSetFontGXColor( &white, &white );
				gfPrint( &temp_tbd, (char*)( (u8*)&sdp[id + i] + sdp[id + i].string_offset ) );
			}
		}
		temp_tbd.y  += temp_height;
	}
}

static void
drawStringforVerifyWin(
	CmdFade*		wp,
	gLRStringHeader* s_hdr,
	gLayoutHeader*	 l_hdr,
	GXColor* rasc,
	int center_x,
	int top_y,
	int	width )
{
	CardState*	csp = getCardState();
	u16		i, id;
//	u32		white  = 0xffffff00 + rasc->a;
//	u32		yellow = 0xc0c00000 + rasc->a;
	GXColor white, yellow;
	gLRTextBoxData*	tbp = (gLRTextBoxData*)( (u8*)l_hdr + l_hdr->textbox_offset );
	gLRStringData* sdp = (gLRStringData*)( (u8*)s_hdr + sizeof(gLRStringHeader) );
	gLRTextBoxData temp_tbd;

	white.r = (u8)csp->moji_sel_r;
	white.g = (u8)csp->moji_sel_g;
	white.b = (u8)csp->moji_sel_b;
	white.a = rasc->a;
	yellow.r = (u8)csp->moji_back_r;
	yellow.g = (u8)csp->moji_back_g;
	yellow.b = (u8)csp->moji_back_b;
	yellow.a = rasc->a;

	id = MSG_D100;
	temp_tbd = tbp[ sdp[id].textbox_id ];

	temp_tbd.x  = (u16)center_x;
	temp_tbd.y  = (u16)( top_y + wp->cmd_height / 2 );
	temp_tbd.dx = (u16)width;
	temp_tbd.dy = (u16)wp->cmd_height;

	setGXforglrText();

	for ( i = 0 ; i < 2 ; i++ ) {
		gfSetFontGXColor( &white, &white );
		gfSetBackFontGXColor( &yellow, &yellow );
		if ( verify_command == i ) {
			gfBritePrint( &temp_tbd, (char*)( (u8*)&sdp[id + i] + sdp[id + i].string_offset ) );
		} else {
			gfPrint( &temp_tbd, (char*)( (u8*)&sdp[id + i] + sdp[id + i].string_offset ) );
		}
		temp_tbd.y  += temp_tbd.line_space * 16;// * ( wp->end_mini_frame - wp->now_mini_frame ) / wp->end_mini_frame ;
	}
}

static void
drawCommandWindowSub(
	u8		no,
	CmdFade* wp,
	gLRStringHeader* s_hdr,
	gLayoutHeader*   l_hdr,
	GXColor* rasc )
{
	s16		alpha;
	u16		window_id, width;
	u8		backup_alpha;
	int		x, y, dy, offset_y;
	gLRWindowData*  wdp = (gLRWindowData* )( (u8*)l_hdr + l_hdr->window_offset    );
	gLRTextBoxData*	tbp = (gLRTextBoxData*)( (u8*)l_hdr + l_hdr->textbox_offset   );
	gLRStringData*  sdp = (gLRStringData* )( (u8*)s_hdr + sizeof(gLRStringHeader) );
	MainState*	    mesp = getMainState();

	for ( window_id = 0 ; window_id < l_hdr->window_num ; window_id++ ) {
		// コマンドウィンドウのタグは、mes0に固定。
		if ( wdp[ window_id ].tag == 'mes0' ) break;
	}
	x = icon_x[ no ][ sel_x[no] + sel_y[no] * 4 ];
	y = icon_y[ no ][ sel_x[no] + sel_y[no] * 4 ];

	getWindowWidthHeight( wp, s_hdr, l_hdr, &width );//, &height, &s_height );
	getCmdOffsetAlpha( wp, &alpha, &offset_y );
	if ( alpha == 0 ) return;

	if ( sel_x[ no ] < 2 ) { //左半分
		x += ( IPL_CMDWIN_MERGIN_X * 16 );
	}
	else {		//右半分
		x -= ( IPL_CMDWIN_MERGIN_X + ( width / 16 + mesp->window_mergin * 2 ) ) * 16;
	}

	if ( sel_y[ no ] < 2 ) { // 下半分
		dy = offset_y;
	}
	else {		//上半分
		y -= ( wp->full_height + mesp->window_mergin * 8 );
		dy = -offset_y;
	}
	// コマンドウィンドウの描画。
	backup_alpha = rasc->a;
	rasc->a = (u8)( rasc->a * alpha / 255 );
	glrDrawWindowPosID( window_id,
						l_hdr,
						rasc,
						x,
						y + dy ,
						width  + mesp->window_mergin * 32,
						wp->select_height + mesp->window_mergin * 32 );
	drawStringforCmdWin( no,
						 wp,
						 s_hdr,
						 l_hdr,
						 rasc,
						 x + width / 2 + mesp->window_mergin * 16 ,
						 y + dy + mesp->window_mergin * 16 ,
						 width );
	rasc->a = (u8)( rasc->a * (wp->end_mini_frame - wp->now_mini_frame) / wp->end_mini_frame );
	drawEdgeAnim( wp,
				  x ,
				  y + dy ,
				  width  + mesp->window_mergin * 32,
				  wp->select_height  + mesp->window_mergin * 32,
				  rasc  );
	rasc->a = backup_alpha;

	// ベリファイウィンドウの描画。
	getVerifyOffsetAlpha( wp, &alpha, &offset_y );
	rasc->a = (u8)( rasc->a * alpha / 255 );
	glrDrawWindowPosID( window_id,
						l_hdr,
						rasc,
						x,
						y + wp->select_height + mesp->window_mergin * 32 + offset_y + 128,
						width  + mesp->window_mergin * 32,
						wp->verify_height + mesp->window_mergin * 32 );
	drawEdgeAnim( wp,
				  x,
				  y + wp->select_height + mesp->window_mergin * 32 + offset_y + 128,
				  width  + mesp->window_mergin * 32,
				  wp->verify_height + mesp->window_mergin * 32,
				  rasc );
	drawStringforVerifyWin( wp,
							s_hdr,
							l_hdr,
							rasc,
							x + width / 2 + mesp->window_mergin * 16 ,
							y + mesp->window_mergin * 16  + wp->cmd_height + mesp->window_mergin * 32 + offset_y + 128,
							width );
	rasc->a = backup_alpha;
}

static void
drawWindows( u8	alpha )
{
	GXColor white = { 255, 255, 255, 255 };

	white.a = (u8)( white.a * alpha / 255 );

	drawCommandWindowSub( 0, &cmdwnd[0], mem_str, mem_lay, &white );
	drawCommandWindowSub( 1, &cmdwnd[1], mem_str, mem_lay, &white );
}

static u32
loadbannerTexture( void )
{
	IconStateArrayPtr 	icon = getIconStateArray();
	DirStateArrayPtr dirState = getCardDirState();
	int			icon_size = 32;
	void*		tex;
	int 		no = sel_slot;
	u32 		fileNo = sFileArray[no][ sel_x[ no ] + ( sel_y[ no ] + now_line[ no ] ) * 4 ].fileNo;
	ResTIMG		tex_image;

	if ( !dirState[no][fileNo].validate ) return FALSE;
	
	tex = (char*)&icon[no][fileNo] + icon[no][fileNo].banner_offset;

	if ( !icon[no][fileNo].banner_enable ) {
		return FALSE;
	}
	tex_image.enableLOD       = GX_DISABLE;
	tex_image.enableEdgeLOD   = GX_DISABLE;
	tex_image.enableBiasClamp = GX_DISABLE;
	tex_image.enableMaxAniso  = (u8)GX_ANISO_1;
	tex_image.width  = 96;
	tex_image.height = 32;
	tex_image.wrapS  = GX_CLAMP;
	tex_image.wrapT  = GX_CLAMP;
	tex_image.format = icon[no][fileNo].banner_format;
	tex_image.minFilter = GX_LINEAR;
	tex_image.magFilter = GX_LINEAR;
	tex_image.minLOD    = 0;
	tex_image.maxLOD    = 0;
	tex_image.LODBias   = 0;

	if ( icon[no][fileNo].banner_format == GX_TF_RGB5A3 ) {
		//		DIReport("++++++++++++++++++++++++++++++++++++++++++++++++RGB5A3\n");
		tex_image.indexTexture    = FALSE;
		tex_image.imageOffset = (s32)( (u32)tex - (u32)&tex_image );

		loadTexture( 0, &tex_image );

		return TRUE;
	}
	else if ( icon[no][fileNo].banner_format == GX_TF_C8 ) {
		//		DIReport("++++++++++++++++++++++++++++++++++++++++++++++++C8\n");
		void*		tlut;
		tlut = (char*)&icon[no][fileNo] + icon[no][fileNo].banner_palette_offset;
		tex_image.indexTexture    = TRUE;
		tex_image.colorFormat = (u8)GX_TL_RGB5A3;
		tex_image.numColors = 256;
		tex_image.paletteOffset = (s32)( (u32)tlut - (u32)&tex_image );
		tex_image.imageOffset   = (s32)( (u32)tex  - (u32)&tex_image );

		loadTexture( 0, &tex_image );

		return TRUE;
	}
	// ありえないけど、一応。
	return FALSE;
}

static void
drawComment( u8 no, s16 fileNo, u8 alpha )
{
	GXColor white = { 255, 255, 255, 255 };

	white.a = (u8)( white.a * alpha / 255 );

	glrDrawTextBoxNStringOneLine( 'titl', mem_lay, &white, getIconComment( no, fileNo )   , 31 );
	glrDrawTextBoxNStringOneLine( 'info', mem_lay, &white, getIconComment( no, fileNo )+32, 31 );
}

static void
drawFileInfo( u8 alpha )
{
	u8 			no     = sel_slot;
	s16 		fileNo = (s16)sFileArray[no][ sel_x[ no ] + ( sel_y[ no ] + now_line[ no ] ) * 4 ].fileNo;
	GXColor		white  = (GXColor){ 255, 255, 255, alpha };
	DirStateArrayPtr dirState = getCardDirState();

	setGXforglrPicture();
	if ( dirState[no][fileNo].validate ) {
		drawComment( no , fileNo, alpha );
		glrDrawWindow( 'frmc', mem_lay, &white );
		drawTextureNum( num_timg_x[2], num_timg_y[2], dirState[no][fileNo].blocks );
	}
}

static void
drawLayout( u8 alpha )
{
	GXColor white = { 255, 255, 255, 255 };
	SlotState* tSlot = getCardSlotState();

	white.a = (u8)( white.a * alpha / 255 );

	glrDrawWindow( 'mes6', mem_lay, &white );
}

static void
drawMessage( u8 alpha )
{
	GXColor	white = { 255, 255, 255, 255 };
	white.a = (u8)( white.a * alpha / 255 );

	setGXforglrText();
	// copy
	glrDrawFadeString( &mes[ MSG_D200 ], mem_str, mem_lay, &white );
	glrDrawFadeString( &mes[ MSG_D201 ], mem_str, mem_lay, &white );
	glrDrawFadeString( &mes[ MSG_D202 ], mem_str, mem_lay, &white );
	glrDrawFadeString( &mes[ MSG_D203 ], mem_str, mem_lay, &white );
	glrDrawFadeString( &mes[ MSG_D204 ], mem_str, mem_lay, &white );
	glrDrawFadeString( &mes[ MSG_D205 ], mem_str, mem_lay, &white );
	glrDrawFadeString( &mes[ MSG_D208 ], mem_str, mem_lay, &white );
	glrDrawFadeString( &mes[ MSG_D209 ], mem_str, mem_lay, &white );
	glrDrawFadeString( &mes[ MSG_D210 ], mem_str, mem_lay, &white );
	glrDrawFadeString( &mes[ MSG_D211 ], mem_str, mem_lay, &white );
	// move
	glrDrawFadeString( &mes[ MSG_D600 ], mem_str, mem_lay, &white );
	glrDrawFadeString( &mes[ MSG_D601 ], mem_str, mem_lay, &white );
	glrDrawFadeString( &mes[ MSG_D602 ], mem_str, mem_lay, &white );
	glrDrawFadeString( &mes[ MSG_D603 ], mem_str, mem_lay, &white );
	glrDrawFadeString( &mes[ MSG_D604 ], mem_str, mem_lay, &white );
	glrDrawFadeString( &mes[ MSG_D605 ], mem_str, mem_lay, &white );
	glrDrawFadeString( &mes[ MSG_D608 ], mem_str, mem_lay, &white );
	glrDrawFadeString( &mes[ MSG_D609 ], mem_str, mem_lay, &white );
	glrDrawFadeString( &mes[ MSG_D610 ], mem_str, mem_lay, &white );
	glrDrawFadeString( &mes[ MSG_D611 ], mem_str, mem_lay, &white );
	// delete
	glrDrawFadeString( &mes[ MSG_D300 ], mem_str, mem_lay, &white );
	glrDrawFadeString( &mes[ MSG_D301 ], mem_str, mem_lay, &white );
	// format
	glrDrawFadeString( &mes[ MSG_D410 ], mem_str, mem_lay, &white );
	glrDrawFadeString( &mes[ MSG_D411 ], mem_str, mem_lay, &white );
	glrDrawFadeString( &mes[ MSG_D412 ], mem_str, mem_lay, &white );
	glrDrawFadeString( &mes[ MSG_D413 ], mem_str, mem_lay, &white );
}

static void
getSlotPosVec( u8 no, VecPtr	pos )
{
	CardState*	csp = getCardState();
	
	pos->x = ( (f32)(icon_x[no][5] + icon_x[no][6]) / 32 );
	pos->y = ( (f32)(icon_y[no][5] + icon_y[no][9]) / 32 );
	pos->z = csp->select_light_dist;
}

/*-----------------------------------------------------------------------------
  アイコン選択時の描画。
  -----------------------------------------------------------------------------*/
void
drawCardMenu( u8 all_alpha , u8 grid_alpha, u8 layout_alpha )
{
	MainState* msp  = getMainState();
	SlotState* tSlot = getCardSlotState();
	J3DTransformInfo ti = { 1.f, -1.f, 1.f, 0,0,0,0, -292, 224, 0 };
	Vec     up      = {   0.f,   1.f,   0.f };
	Vec     camLoc	= {   0.f,   0.f,   0.f };
	Vec		objPt	= {   0.f,   0.f,   0.f };
	Vec		slot_pos[2];
	s16		alpha_[2], main_light_alpha_;
	Mtx m, cm, inv;

	camLoc.z = 224.0f / tanf( msp->fovy * 3.141592f / 360.f ); // ( fovy / 2 )  *  (3.14 / 180)
	MTXLookAt( cm, &camLoc, &up, &objPt);
	gGetTransformMtx( &ti , m );
	MTXConcat( cm, m , m );
	gGetInverseTranspose( m, inv );
	GXLoadPosMtxImm( m , GX_PNMTX0 );
	GXLoadNrmMtxImm( inv , GX_PNMTX0 );
	GXSetCurrentMtx( GX_PNMTX0 );

	// GRID -------------------------------
	getSlotPosVec( 0, &slot_pos[0] );
	getSlotPosVec( 1, &slot_pos[1] );
	glrGetOffsetAlpha( &active_slot_fade[0], &alpha_[0], NULL );
	glrGetOffsetAlpha( &active_slot_fade[1], &alpha_[1], NULL );
	glrGetOffsetAlpha( &main_light, &main_light_alpha_, NULL );
	glrDraw2dGridCard( m, (u8)grid_alpha, &slot_pos[0], &slot_pos[1], (u8)alpha_[0], (u8)alpha_[1], (u8)main_light_alpha_ );
	// GRID -------------------------------

	// 2D関連==============================
	drawLayout( (u8)layout_alpha );
	// 2D関連==============================

	// 3D関連++++++++++++++++++++++++++++++
	if ( sel_slot == 0 ) {
		drawCardIconMove( 0 , (u8)all_alpha );
		drawCardIconMove( 1 , (u8)all_alpha );
	} else {
		drawCardIconMove( 1 , (u8)all_alpha );
		drawCardIconMove( 0 , (u8)all_alpha );
	}
	// 3D関連++++++++++++++++++++++++++++++

	GXLoadPosMtxImm( m , GX_PNMTX0 );
	GXLoadNrmMtxImm( inv , GX_PNMTX0 );
	GXSetCurrentMtx( GX_PNMTX0 );

	// 2D関連==============================
	if ( sel_slot == 0 ) {
		drawFileLayout( 1 , (u8)layout_alpha );
		drawFileLayout( 0 , (u8)layout_alpha );
	} else {
		drawFileLayout( 0 , (u8)layout_alpha );
		drawFileLayout( 1 , (u8)layout_alpha );
	}
	// 2D関連==============================
	
	drawWindows( (u8)layout_alpha );
	drawMessage( (u8)layout_alpha );
}

/*-----------------------------------------------------------------------------
  オペレーションを表示するために、現在のステートを分かりやすく返す。
  -----------------------------------------------------------------------------*/
int
getCardRunstate( void )
{
	if ( card_runstate == IPL_CARD_FILE_SELECTING ) {
		return TRUE;
	}

	return FALSE;
}
