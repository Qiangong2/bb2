#include <dolphin.h>
#include <string.h>
#include <math.h>

#include "smallCubeFaderNumberEffect.h"
#include "smallCubeFader.h"
#include "smallCubeAnimation.h"
#include "smallCubeFaderData.h"
#include "OptionUpdate.h"
#include "TimeUpdate.h"
#include "MenuUpdate.h"
#include "gRand.h"

static f32 get1stFrame(void);
static f32 get2ndFrame(void);
static void setChangedAnim(Vec dir, s16 oldval, s16 newval );
static void setCorrectCurStateNumber( void );
static void setCorrectOffsetNumber(void);

static s16 sNumber[2];
static s16 sOldNum[2];
static s16 sFrame[2];


#define NUMBEREFFECTCUBENUM 112
// この中に、左(古),左(新)、右（古）、右（新）の順で数字の場所をいれる
static J3DTransformInfo sNumberPos[NUMBEREFFECTCUBENUM];
static u8 sAlpha[NUMBEREFFECTCUBENUM];
static s16 sCubeNum;

static s16 sMoveOffset[NUMBEREFFECTCUBENUM];
static Vec sMoveDir;

u32 scfGetNumberEffectNum(void)
{
    return sCubeNum;
}
u8 *scfGetNumberEffectAlpha(void)
{
    return sAlpha;
}
J3DTransformInfo* scfGetNumberEffectPos(void)
{
    return sNumberPos;
}

void scfInvalidateOptionNumberEffect(void)
{
    sFrame[0] = getMenuState()->scf_numeffectframe;// 終わった状態にしとく
    sFrame[1] = getMenuState()->scf_numeffectframe;
}



void scfUpdateOptionNumberEffect( J3DTransformInfo *pos )
{
    u16 *offset1st, *offset2nd, *numberidx;
    u16 offsetnum; // 1st,2ndは同じ数

    int i;

    if( scfGetState()->optionrunstate == IPL_OPTION_OFFSET ){
	if( scfGetPrevState()->optionrunstate != IPL_OPTION_OFFSET ){ /* 移行した後１フレーム
								      待ってからでないと、
								      エフェクトが発生してまう */
	    setCorrectOffsetNumber();
	    sCubeNum = 0;
	    return;
	}else{
	    sCubeNum = NUMBEREFFECTCUBENUM;
	}
    }else{
	sCubeNum = 0;
	return;
    }

    offset1st = scfGetOFFSET1STIndex();
    offsetnum = scfGetOFFSET1STNum();
    offset2nd = scfGetOFFSET2NDIndex();

    for( i=0;i<NUMBEREFFECTCUBENUM;i++ ) sAlpha[i] = 0;

    // 左（古）を作る
    numberidx = scfGetDIGITIndex( sOldNum[1] );
    for( i=0;i<offsetnum;i++ ){
	sNumberPos[i].tx = pos[offset1st[i]].tx
	    + getMenuState()->scf_numeffect_tenmag*sMoveOffset[i]*(-sMoveDir.x)*get1stFrame();
	sNumberPos[i].ty = pos[offset1st[i]].ty
	    + getMenuState()->scf_numeffect_tenmag*sMoveOffset[i]*(-sMoveDir.y)*get1stFrame();
	sNumberPos[i].tz = pos[offset1st[i]].tz
	    + getMenuState()->scf_numeffect_tenmag*sMoveOffset[i]*(-sMoveDir.z)*get1stFrame();
	sNumberPos[i].rx = pos[offset1st[i]].rx;
	sNumberPos[i].ry = pos[offset1st[i]].ry;
	sNumberPos[i].rz = pos[offset1st[i]].rz;
	sNumberPos[i].sx = pos[offset1st[i]].sx;
	sNumberPos[i].sy = pos[offset1st[i]].sy;
	sNumberPos[i].sz = pos[offset1st[i]].sz;
	
	sAlpha[numberidx[i]] = (u8)(255*(1.0f-get1stFrame()));
	
    }
    // 左（新）を作る
    numberidx = scfGetDIGITIndex( sNumber[1] );
    for( i=0;i<offsetnum;i++ ){
	sNumberPos[i+offsetnum].tx = pos[offset1st[i]].tx
	    + getMenuState()->scf_numeffect_tenmag*sMoveOffset[i+offsetnum]*sMoveDir.x*(1.0f-get1stFrame());
	sNumberPos[i+offsetnum].ty = pos[offset1st[i]].ty
	    + getMenuState()->scf_numeffect_tenmag*sMoveOffset[i+offsetnum]*sMoveDir.y*(1.0f-get1stFrame());
	sNumberPos[i+offsetnum].tz = pos[offset1st[i]].tz
	    + getMenuState()->scf_numeffect_tenmag*sMoveOffset[i+offsetnum]*sMoveDir.z*(1.0f-get1stFrame());
	sNumberPos[i+offsetnum].rx = pos[offset1st[i]].rx;
	sNumberPos[i+offsetnum].ry = pos[offset1st[i]].ry;
	sNumberPos[i+offsetnum].rz = pos[offset1st[i]].rz;
	sNumberPos[i+offsetnum].sx = pos[offset1st[i]].sx;
	sNumberPos[i+offsetnum].sy = pos[offset1st[i]].sy;
	sNumberPos[i+offsetnum].sz = pos[offset1st[i]].sz;
	
	sAlpha[numberidx[i]+offsetnum] = (u8)(255*get1stFrame());
    }

    // 右（古）を作る
    numberidx = scfGetDIGITIndex( sOldNum[0] );
    for( i=0;i<offsetnum;i++ ){
	sNumberPos[i+offsetnum*2].tx = pos[offset2nd[i]].tx + sMoveOffset[i+offsetnum*2]*(-sMoveDir.x)*get2ndFrame();
	sNumberPos[i+offsetnum*2].ty = pos[offset2nd[i]].ty + sMoveOffset[i+offsetnum*2]*(-sMoveDir.y)*get2ndFrame();
	sNumberPos[i+offsetnum*2].tz = pos[offset2nd[i]].tz + sMoveOffset[i+offsetnum*2]*(-sMoveDir.z)*get2ndFrame();
	sNumberPos[i+offsetnum*2].rx = pos[offset2nd[i]].rx;
	sNumberPos[i+offsetnum*2].ry = pos[offset2nd[i]].ry;
	sNumberPos[i+offsetnum*2].rz = pos[offset2nd[i]].rz;
	sNumberPos[i+offsetnum*2].sx = pos[offset2nd[i]].sx;
	sNumberPos[i+offsetnum*2].sy = pos[offset2nd[i]].sy;
	sNumberPos[i+offsetnum*2].sz = pos[offset2nd[i]].sz;
	
	sAlpha[numberidx[i]+offsetnum*2] = (u8)(255*(1.0f-get2ndFrame()));
	
    }
    // 右（新）を作る
    numberidx = scfGetDIGITIndex( sNumber[0] );
    for( i=0;i<offsetnum;i++ ){
	sNumberPos[i+offsetnum*3].tx = pos[offset2nd[i]].tx + sMoveOffset[i+offsetnum*3]*sMoveDir.x*(1.0f-get2ndFrame());
	sNumberPos[i+offsetnum*3].ty = pos[offset2nd[i]].ty + sMoveOffset[i+offsetnum*3]*sMoveDir.y*(1.0f-get2ndFrame());
	sNumberPos[i+offsetnum*3].tz = pos[offset2nd[i]].tz + sMoveOffset[i+offsetnum*3]*sMoveDir.z*(1.0f-get2ndFrame());
	sNumberPos[i+offsetnum*3].rx = pos[offset2nd[i]].rx;
	sNumberPos[i+offsetnum*3].ry = pos[offset2nd[i]].ry;
	sNumberPos[i+offsetnum*3].rz = pos[offset2nd[i]].rz;
	sNumberPos[i+offsetnum*3].sx = pos[offset2nd[i]].sx;
	sNumberPos[i+offsetnum*3].sy = pos[offset2nd[i]].sy;
	sNumberPos[i+offsetnum*3].sz = pos[offset2nd[i]].sz;
	
	sAlpha[numberidx[i]+offsetnum*3] = (u8)(255*get2ndFrame());
    }
    
    setChangedAnim( (Vec){-1,0,0},
		    scfGetPrevState()->optionoffset,
		    scfGetState()->optionoffset );

    if( sFrame[0] < getMenuState()->scf_numeffectframe ) sFrame[0] ++;
    if( sFrame[1] < getMenuState()->scf_numeffectframe ) sFrame[1] ++;

}

void scfInvalidateTimeNumberEffect(void)
{
    sFrame[0] = getMenuState()->scf_numeffectframe;// 終わった状態にしとく
    sFrame[1] = getMenuState()->scf_numeffectframe;
}


void scfUpdateTimeNumberEffect( J3DTransformInfo *pos )
{

    u16 *num1st, *num2nd, *numberidx;
    u16 numcubenum;
    int i;
    s16 numdisplay[6], prevnum[6], e[4];


    if( scfGetState()->timerunstate != IPL_TIME_SELECTING ){
	if( scfGetPrevState()->timerunstate != IPL_TIME_SELECTING ){
	    sCubeNum = NUMBEREFFECTCUBENUM;
	}else{
	    setCorrectCurStateNumber();
	    sCubeNum = 0;
	    return;
	}
    }else{
	sCubeNum = 0;
	return;
    }

    numcubenum = scfGetYEAR1STNum();

    scfGetTimeNumberToDisplay( numdisplay,
			       scfGetState()->timerunstate,
			       scfGetState()->timeeditnum );
    scfGetTimeNumberToDisplay( prevnum,
			       scfGetPrevState()->timerunstate,
			       scfGetPrevState()->timeeditnum );

    if( scfGetState()->timerunstate == IPL_TIME_DAY ){
	switch( scfGetState()->timeday ){
	case 0:
	    // 数字がループするときの、エフェクトの向きの不整合に対処
	    if( numdisplay[0] == 2099 && prevnum[0] == 2000
		|| numdisplay[0] == 2000 && prevnum[0] == 2099 ){
		setChangedAnim( (Vec){0,1,0},
				prevnum[0], numdisplay[0] );
		
	    }else{
		setChangedAnim( (Vec){0,-1,0},
				prevnum[0], numdisplay[0] );
	    }
	    num1st = scfGetYEAR3RDIndex();
	    num2nd = scfGetYEAR4THIndex();
	    break;
	case 1:
	    if( numdisplay[1] == 12 && prevnum[1] == 1
		|| numdisplay[1] == 1 && prevnum[1] == 12 ){
		setChangedAnim( (Vec){0,1,0},
				prevnum[1], numdisplay[1] );
	    }else{
		setChangedAnim( (Vec){0,-1,0},
				prevnum[1], numdisplay[1] );
	    }
	    num1st = scfGetMONTH1STIndex();
	    num2nd = scfGetMONTH2NDIndex();
	    break;
	case 2:
	    if( (numdisplay[2] == 31 || numdisplay[2] == 30 || numdisplay[2] == 29 || numdisplay[2] == 28 )
		&& prevnum[2] == 1
		|| numdisplay[2] == 1
		&& (prevnum[2] == 31 || prevnum[2] == 30 || prevnum[2] == 29 || prevnum[2] == 28 ) ){
		setChangedAnim( (Vec){0,1,0},
				prevnum[2], numdisplay[2] );
	    }else{
		setChangedAnim( (Vec){0,-1,0},
				prevnum[2], numdisplay[2] );
		
	    }
	    num1st = scfGetDAY1STIndex();
	    num2nd = scfGetDAY2NDIndex();
	    break;
	}
    }else if( scfGetState()->timerunstate == IPL_TIME_CLOCK ){
	switch( scfGetState()->timeclock ){
	case 0:
	    if( numdisplay[3] == 23 && prevnum[3] == 0
		|| numdisplay[3] == 0 && prevnum[3] == 23 ){
		setChangedAnim( (Vec){0,1,0},
				prevnum[3], numdisplay[3] );
	    }else{
		setChangedAnim( (Vec){0,-1,0},
				prevnum[3], numdisplay[3] );
	    }
	    num1st = scfGetHOUR1STIndex();
	    num2nd = scfGetHOUR2NDIndex();
	    break;
	case 1:
	    if( numdisplay[4] == 59 && prevnum[4] == 0
		|| numdisplay[4] == 0 && prevnum[4] == 59 ){
		setChangedAnim( (Vec){0,1,0},
				prevnum[4], numdisplay[4] );
	    }else{
		setChangedAnim( (Vec){0,-1,0},
				prevnum[4], numdisplay[4] );
	    }
	    num1st = scfGetMINUTE1STIndex();
	    num2nd = scfGetMINUTE2NDIndex();
	    break;
	case 2:
	    if( numdisplay[5] == 59 && prevnum[5] == 0
		|| numdisplay[5] == 0 && prevnum[5] == 59 ){
		setChangedAnim( (Vec){0,1,0},
				prevnum[5], numdisplay[5] );
	    }else{
		setChangedAnim( (Vec){0,-1,0},
				prevnum[5], numdisplay[5] );
	    }
	    num1st = scfGetSECOND1STIndex();
	    num2nd = scfGetSECOND2NDIndex();
	    break;
	}
    }else{
	return;
    }

    for( i=0;i<NUMBEREFFECTCUBENUM;i++ ) sAlpha[i] = 0;

    // カーソル位置の変化に対処
    if( scfGetState()->timeday != scfGetPrevState()->timeday ){
	scfInvalidateTimeNumberEffect();
	switch( scfGetState()->timeday ){
	case 0: scfIntTo4Number( e, numdisplay[0] ); break;
	case 1: scfIntTo4Number( e, numdisplay[1] ); break;
	case 2: scfIntTo4Number( e, numdisplay[2] ); break;
	}
	sOldNum[0] = sNumber[0] = e[0];
	sOldNum[1] = sNumber[1] = e[1];
    }
    if( scfGetState()->timeclock != scfGetPrevState()->timeclock ){
	scfInvalidateTimeNumberEffect();
	switch( scfGetState()->timeclock ){
	case 0: scfIntTo4Number( e, numdisplay[3] ); break;
	case 1: scfIntTo4Number( e, numdisplay[4] ); break;
	case 2: scfIntTo4Number( e, numdisplay[5] ); break;
	}
	sOldNum[0] = sNumber[0] = e[0];
	sOldNum[1] = sNumber[1] = e[1];
    }
    
    // 左（古）を作る
    numberidx = scfGetDIGITIndex( sOldNum[1] );
    for( i=0;i<numcubenum;i++ ){
	sNumberPos[i].tx = pos[num1st[i]].tx + getMenuState()->scf_numeffect_tenmag*sMoveOffset[i]*(-sMoveDir.x)*get1stFrame();
	sNumberPos[i].ty = pos[num1st[i]].ty + getMenuState()->scf_numeffect_tenmag*sMoveOffset[i]*(-sMoveDir.y)*get1stFrame();
	sNumberPos[i].tz = pos[num1st[i]].tz + getMenuState()->scf_numeffect_tenmag*sMoveOffset[i]*(-sMoveDir.z)*get1stFrame();
	sNumberPos[i].rx = pos[num1st[i]].rx;
	sNumberPos[i].ry = pos[num1st[i]].ry;
	sNumberPos[i].rz = pos[num1st[i]].rz;
	sNumberPos[i].sx = pos[num1st[i]].sx;
	sNumberPos[i].sy = pos[num1st[i]].sy;
	sNumberPos[i].sz = pos[num1st[i]].sz;
	
	sAlpha[numberidx[i]] = (u8)(255*(1.0f-get1stFrame()));
	
    }
    // 左（新）を作る
    numberidx = scfGetDIGITIndex( sNumber[1] );
    for( i=0;i<numcubenum;i++ ){
	sNumberPos[i+numcubenum].tx = pos[num1st[i]].tx + getMenuState()->scf_numeffect_tenmag*sMoveOffset[i+numcubenum]*sMoveDir.x*(1.0f-get1stFrame());
	sNumberPos[i+numcubenum].ty = pos[num1st[i]].ty + getMenuState()->scf_numeffect_tenmag*sMoveOffset[i+numcubenum]*sMoveDir.y*(1.0f-get1stFrame());
	sNumberPos[i+numcubenum].tz = pos[num1st[i]].tz + getMenuState()->scf_numeffect_tenmag*sMoveOffset[i+numcubenum]*sMoveDir.z*(1.0f-get1stFrame());
	sNumberPos[i+numcubenum].rx = pos[num1st[i]].rx;
	sNumberPos[i+numcubenum].ry = pos[num1st[i]].ry;
	sNumberPos[i+numcubenum].rz = pos[num1st[i]].rz;
	sNumberPos[i+numcubenum].sx = pos[num1st[i]].sx;
	sNumberPos[i+numcubenum].sy = pos[num1st[i]].sy;
	sNumberPos[i+numcubenum].sz = pos[num1st[i]].sz;
	
	sAlpha[numberidx[i]+numcubenum] = (u8)(255*get1stFrame());
    }

    // 右（古）を作る
    numberidx = scfGetDIGITIndex( sOldNum[0] );
    for( i=0;i<numcubenum;i++ ){
	sNumberPos[i+numcubenum*2].tx = pos[num2nd[i]].tx + sMoveOffset[i+numcubenum*2]*(-sMoveDir.x)*get2ndFrame();
	sNumberPos[i+numcubenum*2].ty = pos[num2nd[i]].ty + sMoveOffset[i+numcubenum*2]*(-sMoveDir.y)*get2ndFrame();
	sNumberPos[i+numcubenum*2].tz = pos[num2nd[i]].tz + sMoveOffset[i+numcubenum*2]*(-sMoveDir.z)*get2ndFrame();
	sNumberPos[i+numcubenum*2].rx = pos[num2nd[i]].rx;
	sNumberPos[i+numcubenum*2].ry = pos[num2nd[i]].ry;
	sNumberPos[i+numcubenum*2].rz = pos[num2nd[i]].rz;
	sNumberPos[i+numcubenum*2].sx = pos[num2nd[i]].sx;
	sNumberPos[i+numcubenum*2].sy = pos[num2nd[i]].sy;
	sNumberPos[i+numcubenum*2].sz = pos[num2nd[i]].sz;
	
	sAlpha[numberidx[i]+numcubenum*2] = (u8)(255*(1.0f-get2ndFrame()));
	
    }
    // 右（新）を作る
    numberidx = scfGetDIGITIndex( sNumber[0] );
    for( i=0;i<numcubenum;i++ ){
	sNumberPos[i+numcubenum*3].tx = pos[num2nd[i]].tx + sMoveOffset[i+numcubenum*3]*sMoveDir.x*(1.0f-get2ndFrame());
	sNumberPos[i+numcubenum*3].ty = pos[num2nd[i]].ty + sMoveOffset[i+numcubenum*3]*sMoveDir.y*(1.0f-get2ndFrame());
	sNumberPos[i+numcubenum*3].tz = pos[num2nd[i]].tz + sMoveOffset[i+numcubenum*3]*sMoveDir.z*(1.0f-get2ndFrame());
	sNumberPos[i+numcubenum*3].rx = pos[num2nd[i]].rx;
	sNumberPos[i+numcubenum*3].ry = pos[num2nd[i]].ry;
	sNumberPos[i+numcubenum*3].rz = pos[num2nd[i]].rz;
	sNumberPos[i+numcubenum*3].sx = pos[num2nd[i]].sx;
	sNumberPos[i+numcubenum*3].sy = pos[num2nd[i]].sy;
	sNumberPos[i+numcubenum*3].sz = pos[num2nd[i]].sz;
	
	sAlpha[numberidx[i]+numcubenum*3] = (u8)(255*get2ndFrame());
    }

    if( sFrame[0] < getMenuState()->scf_numeffectframe ) sFrame[0] ++;
    if( sFrame[1] < getMenuState()->scf_numeffectframe ) sFrame[1] ++;
    
}


static f32 get1stFrame(void)
{
    return (f32)sFrame[1]/getMenuState()->scf_numeffectframe;
}

static f32 get2ndFrame(void)
{
    return (f32)sFrame[0]/getMenuState()->scf_numeffectframe;
}

static void setChangedAnim(Vec dir, s16 oldval, s16 newval )
{
    s16 ov[4], nv[4], i;
    s16 diff = (s16)(newval - oldval);

    if( oldval == newval ) return;

    if( diff > 0 ){
	sMoveDir.x = dir.x;
	sMoveDir.y = dir.y;
	sMoveDir.z = dir.z;
    }else{
	sMoveDir.x = -dir.x;
	sMoveDir.y = -dir.y;
	sMoveDir.z = -dir.z;
    }

    for( i=0;i<NUMBEREFFECTCUBENUM;i++ ){
	sMoveOffset[i] = (s16)(getMenuState()->scf_numeffect_width*(f32)(200+urand()%600)/10.0f);
    }
	    
    scfIntTo4Number( ov, (int)fabs(oldval) );
    scfIntTo4Number( nv, (int)fabs(newval) );
    if( ov[1] != nv[1] ){
	sFrame[1] = 0;
	sOldNum[1] = ov[1];
	sNumber[1] = nv[1];
    }
    if( ov[0] != nv[0] ){
	sFrame[0] = 0;
	sOldNum[0] = ov[0];
	sNumber[0] = nv[0];
    }
    
}

static void setCorrectCurStateNumber( void )
{
    s16 numdisplay[6], e[4];

    scfGetTimeNumberToDisplay( numdisplay,
			       scfGetState()->timerunstate,
			       scfGetState()->timeeditnum );

    if( scfGetState()->timerunstate == IPL_TIME_DAY ){
	scfIntTo4Number( e, numdisplay[scfGetState()->timeday] );
    }else if( scfGetState()->timerunstate == IPL_TIME_CLOCK ){
	scfIntTo4Number( e, numdisplay[3+scfGetState()->timeclock] );
    }else{
	return;
    }
    sNumber[0] = sOldNum[0] = e[0];
    sNumber[1] = sOldNum[1] = e[1];
}

static void setCorrectOffsetNumber(void)
{
    s16 e[4];
    
    scfIntTo4Number( e, (int)fabs(scfGetState()->optionoffset));
    sOldNum[0] = sNumber[0] = e[0];
    sOldNum[1] = sNumber[1] = e[1];
    
}
