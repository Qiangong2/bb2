/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: メニューモード時の各面のパラメータ
  -----------------------------------------------------------------------------*/
#include <dolphin.h>

#include "CardState.h"
#include "CardUpdate.h"

#ifdef JHOSTIO
#include "IHICardState.h"
#endif

/*-----------------------------------------------------------------------------
  ファイルローカル変数
  -----------------------------------------------------------------------------*/
static CardState* stp;

#ifndef JHOSTIO
static CardState st;
#endif

/*-----------------------------------------------------------------------------
  初期化
  -----------------------------------------------------------------------------*/
void
initCard( BOOL first )
{
	if ( first ) {
#ifndef JHOSTIO
		stp = &st;
		initCardState( stp );
#else
		stp = getIHICardState();
#endif
	}

	initCardMenu( first );
}

/*-----------------------------------------------------------------------------
  パラメータ初期化
  -----------------------------------------------------------------------------*/
void
initCardState( CardState* p )
{
#pragma unused ( p )
	/*
	  p->param = 0;
	  */
	p->cube_r = -45;
	p->cube_g = -40;
	p->cube_b = 50;
	p->cube_a = 255;

	p->scale = 0.7f;

	p->cmdwnd_effect_speed = 5;
	p->fmtwnd_mergin = 8;


	p->icon_sel_file_r[0] = 0;
	p->icon_sel_file_g[0] = 150;
	p->icon_sel_file_b[0] = 255;

	p->icon_nosel_file_r[0] = 0;
	p->icon_nosel_file_g[0] = 50;
	p->icon_nosel_file_b[0] = 200;

	p->icon_sel_nofile_r[0] = 0;
	p->icon_sel_nofile_g[0] = 30;
	p->icon_sel_nofile_b[0] = 90;

	p->icon_nosel_nofile_r[0] = 0;
	p->icon_nosel_nofile_g[0] = 20;
	p->icon_nosel_nofile_b[0] = 60;


	p->icon_sel_file_r[1] = 0;
	p->icon_sel_file_g[1] = 150;
	p->icon_sel_file_b[1] = 255;

	p->icon_nosel_file_r[1] = 0;
	p->icon_nosel_file_g[1] = 50;
	p->icon_nosel_file_b[1] = 200;

	p->icon_sel_nofile_r[1] = 0;
	p->icon_sel_nofile_g[1] = 30;
	p->icon_sel_nofile_b[1] = 90;

	p->icon_nosel_nofile_r[1] = 0;
	p->icon_nosel_nofile_g[1] = 20;
	p->icon_nosel_nofile_b[1] = 60;

	p->icon_max_scale = 2.0f;
	p->icon_min_scale = 1.3f;

	p->moji_sel_r = 255;
	p->moji_sel_g = 255;
	p->moji_sel_b = 255;
	p->moji_back_r = 240;
	p->moji_back_g = 230;
	p->moji_back_b = 0;
	p->moji_half = 70;

	p->concent_dec_pos = 5;
	p->concent_dec_alpha = 13;

	p->swing_x = 10.f;
	p->swing_y = 5.f;

	p->enable_slot_effect = (u16) (
		IPL_CARD_EFFECT_DEACTIVE_FADEOUT
//		| IPL_CARD_EFFECT_ACTIVE_BLINK
		);
	p->slot_blink_base = 128;

	p->select_light_dist = 320.f;
}

/*-----------------------------------------------------------------------------
  パラメータポインタ取得
  -----------------------------------------------------------------------------*/
CardState*
getCardState( void )
{
	return stp;
}

