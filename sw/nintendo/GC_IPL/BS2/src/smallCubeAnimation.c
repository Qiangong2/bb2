/*---------------------------------------------------------------------------*
  File:    cubeAnimation.c
  Copyright 2000 Nintendo.  All rights reserved.

  キューブのアニメーションを作る。timeRouteDataで指定されるrouteData構造体
  を、順番につないでいく、というだけのもの。

 *---------------------------------------------------------------------------*/
#include "smallCubeAnimation.h"
#include "smallCubeFader.h"
#include "gTrans.h"
#include "gMath.h"
#include "menuUpdate.h"

#include <string.h>

typedef struct cubeAnimFaderData
{
    u32 signature; // 'CAFD'
    u32 animnum;
    u32 offsetRouteData;
    u32 offsetAnimData;
}cubeAnimFaderData;

typedef struct routeData
{
    Vec pos;
    Vec lslope;
    Vec rslope;
}routeData;

typedef struct animData
{
    u16 cubenum;
    u16 routenum;
    u32 offsetEachCube;
}animData;

typedef struct eachCubeData
{
    u8  starttime;
    u8  endtime;
    u8  padding[2];
    u32 offsetRouteIndex;
}eachCubeData;

// ファイル中のデータを、使いやすい形で置いておくための変数
static cubeAnimFaderData *sAnimData;
static animData  *sAnims;
static routeData *sRoutes;

/*---------------------------------------------------------------------------*
  Name:
  gInitCubeAnimation()
  
  Description:
  アニメーションデータの初期化。
              
 *---------------------------------------------------------------------------*/
void
initCubeAnimation(void* data)
{
    ASSERTMSG( ((u32*)data)[0] == 'CAFD', "invalid animdata\n" );
    sAnimData = data;
    sAnims = (animData*)((u32)sAnimData + sAnimData->offsetAnimData );
    sRoutes = (routeData*)((u32)sAnimData + sAnimData->offsetRouteData );
    
}





/*---------------------------------------------------------------------------*
  Name:
  void getAnimCubePos( u32 kind, f32 frame,
                        J3DTransformInfo *cubepos, u32 *cubenum );

  Description:
  キューブの位置と数を返します。
  cubeposの、tx,ty,tzにだけ値を設定して、他のメンバはいじりません。
              
 *---------------------------------------------------------------------------*/
void
getAnimCubePos( u32 kind, f32 frame,
		J3DTransformInfo *cubepos, u32 *cubenum )
{

    int i;
    f32 sectionlength;
    eachCubeData *eachdata;

    ASSERT( sAnims[kind].routenum >= 2 );

    *cubenum = sAnims[kind].cubenum;
    sectionlength = 1.0f/(sAnims[kind].routenum-1);
    eachdata = (eachCubeData*)((u32)&sAnims[kind] + sAnims[kind].offsetEachCube );

    if( frame < 0.0f ){
	frame = 0.0f;
    }

    if( frame > 1.0f ){
	frame = 1.0f;
    }

    for( i=0;i<*cubenum;i++ ){
	f32 s, e, localframe;
	Vec result;
	u32 sectionnow;
	u16 *routeindex;

	routeindex = (u16*)((u32)&eachdata[i] + eachdata[i].offsetRouteIndex);

	s = 0.4f*(1.0f-eachdata[i].starttime/255.0f); // 0.0〜0.4の範囲
	e = 0.6f + 0.4f*(1.0f-eachdata[i].endtime/255.0f); // 0.6~1.0の範囲
		
	localframe = (frame - s)/(e-s);
	sectionnow = (u32)(localframe/sectionlength);
	if( sectionnow >= sAnims[kind].routenum ){
	    sectionnow = (u32)(sAnims[kind].routenum-1);
	}

	if( localframe < 0 ){ localframe=0;}
	if( localframe > 1 ){ localframe=1;}

	gGetVecInterp( localframe,
		       sectionnow*sectionlength,
		       &sRoutes[routeindex[sectionnow]].pos,
		       &sRoutes[routeindex[sectionnow]].rslope,
		       (sectionnow+1)*sectionlength,
		       &sRoutes[routeindex[sectionnow+1]].pos,
		       &sRoutes[routeindex[sectionnow+1]].lslope,
		       &result );
	
	cubepos[i].tx = result.x;
	cubepos[i].ty = result.y;
	cubepos[i].tz = result.z;
	cubepos[i].rx = cubepos[i].ry = cubepos[i].rz = 0;
	cubepos[i].sx = cubepos[i].sy
	    = cubepos[i].sz = getMenuState()->scf_cubescale;
    }    

}
     

u32
getAnimCubeNum( u32 kind )
{
    return sAnims[kind].cubenum;
}
