/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: J3DBinary 1.5 Renderer
  
  Material処理
  -----------------------------------------------------------------------------*/
#include <dolphin.h>

#include "gBase.h"
#include "gTrans.h"
#include "gCamera.h"

#include "gModelRender.h"
#include "gMRmaterial.h"

/*-----------------------------------------------------------------------------
  
  -----------------------------------------------------------------------------*/
static u8  setValue8 ( int index , void* dataArray );
static u16 setValue16( int index , void* dataArray );
static void setNBTScaleInfo(J3DNBTScaleInfo* info);

/*-----------------------------------------------------------------------------
  staticモノ
  -----------------------------------------------------------------------------*/
static gmrModel* 		currentModel;
static gmrModelData* 	currentModelData;
static VecPtr 			currentNBTScale;

/*-----------------------------------------------------------------------------
  
  -----------------------------------------------------------------------------*/
void
parseMaterial( u16 material )
{
#pragma unused(material)
}

/*-----------------------------------------------------------------------------
  buildMaterial関連関数
  -----------------------------------------------------------------------------*/
static u8
setValue8( int index , void* dataArray )
{
	u8* dp = (u8*)dataArray;
	if ( index == 0xff ) return 0xff;

	return *(dp + index );
}

static u16
setValue16( int index , void* dataArray )
{
	u16* dp = (u16*)dataArray;
	if ( index == 0xffff ) return 0xffff;

	return *(dp + index );
}

static u8
setValue8u32( int index , void* dataArray )
{
	u32* dp = (u32*)dataArray;
	if ( index == 0xff ) return 0xff;

	return (u8)*(dp + index );
}


static gmrZModeInfo
setValueZMI( int index , J3DZModeInfo* info )
{
	gmrZModeInfo zmi = { 0xff };
	
	if ( index == 0xff ) {
		zmi.ce_ue_func = 0xff;
	}
	else {
		gmrZMI_setCompareEnable( zmi , info[index].compare_enable );
		gmrZMI_setUpdateEnable( zmi , info[index].update_enable );
		gmrZMI_setFunc( zmi , info[index].func );
	}

	return zmi;
}

/*-----------------------------------------------------------------------------
  マテリアル情報配列にパラメータをセット
  -----------------------------------------------------------------------------*/
void
buildMaterial(
	J3DMaterialBlock* mbPtr ,
	gmrMaterial* mArray )
{
	int i , j;
	int index;

	// マテリアル情報配列にパラメータを入れていく。
	// ...めんどくさいっちゅーに。
	for ( i=0 ; i < mbPtr->materialNum ; i++ ) {
		mArray[i].MaterialMode = mbPtr->materialInit[ mbPtr->materialID[i] ].mMaterialMode;
		mArray[i].CullMode     = setValue8u32( mbPtr->materialInit[ mbPtr->materialID[i] ].mCullModeID     , mbPtr->cullMode	);
		mArray[i].colorChanNum = setValue8   ( mbPtr->materialInit[ mbPtr->materialID[i] ].mColorChanNumID , mbPtr->colorChanNum);
		mArray[i].texGenNum    = setValue8   ( mbPtr->materialInit[ mbPtr->materialID[i] ].mTexGenNumID    , mbPtr->texGenNum	);
		mArray[i].tevStageNum  = setValue8   ( mbPtr->materialInit[ mbPtr->materialID[i] ].mTevStageNumID  , mbPtr->tevStageNum	);
		mArray[i].dither       = setValue8u32( mbPtr->materialInit[ mbPtr->materialID[i] ].mDitherID       , mbPtr->dither		);
		mArray[i].zCompLoc     = setValue8u32( mbPtr->materialInit[ mbPtr->materialID[i] ].mZCompLocID     , mbPtr->zcompLoc	);
		mArray[i].zModeInfo    = setValueZMI ( mbPtr->materialInit[ mbPtr->materialID[i] ].mZModeID        , mbPtr->zmodeInfo	);

		mArray[i].colorPtr[0]	= ( (index = mbPtr->materialInit[ mbPtr->materialID[i] ].mColorID[0]) == 0xffff )? NULL:mbPtr->color + index;
		mArray[i].colorPtr[1]	= ( (index = mbPtr->materialInit[ mbPtr->materialID[i] ].mColorID[1]) == 0xffff )? NULL:mbPtr->color + index;

		for ( j=0 ; j<4 ; j++ ){
			mArray[i].colorChanPtr[j] = ( (index = mbPtr->materialInit[ mbPtr->materialID[i] ].mColorChanID[j]) == 0xffff )? NULL:mbPtr->colorChanInfo + index;
			mArray[i].tevColorPtr[j] = ( (index = mbPtr->materialInit[ mbPtr->materialID[i] ].mTevColorID[j]) == 0xffff )? NULL:mbPtr->tevColorInfo + index;
		}
		for ( j=0 ; j<8 ; j++ ){
			mArray[i].texNo[j] = setValue16( mbPtr->materialInit[ mbPtr->materialID[i] ].mTexNoID[j] , mbPtr->texNo );
			mArray[i].texCoordPtr[j] = ( (index = mbPtr->materialInit[ mbPtr->materialID[i] ].mTexCoordID[j]) == 0xffff )? NULL:mbPtr->texCoordInfo + index;
			mArray[i].tevStagePtr[j] = ( (index = mbPtr->materialInit[ mbPtr->materialID[i] ].mTevStageID[j]) == 0xffff )? NULL:mbPtr->tevStageInfo + index;
			mArray[i].tevOrderPtr[j] = ( (index = mbPtr->materialInit[ mbPtr->materialID[i] ].mTevOrderID[j]) == 0xffff )? NULL:mbPtr->tevOrderInfo + index;
		}
		for ( j=0 ; j<10 ; j++ ){
			mArray[i].texMtxPtr[j] = ( (index = mbPtr->materialInit[ mbPtr->materialID[i] ].mTexMtxID[j]) == 0xffff )? NULL:mbPtr->texMtxInfo + index;
		}
		
		mArray[i].fogPtr       = ( (index = mbPtr->materialInit[ mbPtr->materialID[i] ].mFogID )      == 0xffff )? NULL:mbPtr->fogInfo + index;
		mArray[i].alphaCompPtr = ( (index = mbPtr->materialInit[ mbPtr->materialID[i] ].mAlphaCompID) == 0xffff )? NULL:mbPtr->alphaCompInfo + index;
		mArray[i].blendPtr     = ( (index = mbPtr->materialInit[ mbPtr->materialID[i] ].mBlendID)     == 0xffff )? NULL:mbPtr->blendInfo + index;
		mArray[i].nbtScalePtr  = ( (index = mbPtr->materialInit[ mbPtr->materialID[i] ].mNBTScaleID)  == 0xffff )? NULL:mbPtr->nbtScaleInfo + index;
	}
}

/*-----------------------------------------------------------------------------
  マテリアルロード時に使用するカレントモデルの設定
  -----------------------------------------------------------------------------*/
void
setMaterialModel( gmrModel *model )
{
	currentModel = model;
	currentModelData = model->gmrmd;
}


/*-----------------------------------------------------------------------------
  テクスチャをロードしまくる。
  -----------------------------------------------------------------------------*/
void
loadTexture( u8 no , ResTIMG* ti )
{
    // ResTIMG
    GXTexObj to;
    GXTlutObj tlut;

    if (ti->indexTexture == FALSE) {
        // テクスチャルックアップテーブルなし
        GXInitTexObj(
            &to,
            (void*)((u8*)ti+ti->imageOffset),
            ti->width,
            ti->height,
            (GXTexFmt)ti->format,
            (GXTexWrapMode)ti->wrapS,
            (GXTexWrapMode)ti->wrapT,
            ti->enableLOD);
    } else {
        // テクスチャルックアップテーブルあり
        GXInitTexObjCI(
            &to,
            (void*)((u8*)ti+ti->imageOffset),
            ti->width,
            ti->height,
            (GXCITexFmt)ti->format,
            (GXTexWrapMode)ti->wrapS,
            (GXTexWrapMode)ti->wrapT,
            ti->enableLOD,
            (u32)(GX_TLUT0+no));
    }
    GXInitTexObjLOD(
        &to,
        (GXTexFilter)ti->minFilter,
        (GXTexFilter)ti->magFilter,
        ti->minLOD*0.125f,
        ti->maxLOD*0.125f,
        ti->LODBias*0.01f,
        ti->enableBiasClamp,
        ti->enableEdgeLOD,
        (GXAnisotropy)ti->enableMaxAniso);
    
    if (ti->indexTexture == TRUE) {
        GXInitTlutObj(&tlut, (void*)((u8*)ti+ti->paletteOffset), (GXTlutFmt)ti->colorFormat, ti->numColors);
        GXLoadTlut(&tlut, (u32)(GX_TLUT0+no));
    }
    
    GXLoadTexObj(&to, (GXTexMapID)(GX_TEXMAP0+no));
}

/*-----------------------------------------------------------------------------
  NBTの設定
  -----------------------------------------------------------------------------*/
static void
setNBTScaleInfo(J3DNBTScaleInfo* info)
{
//    J3DModelData* pModelData = mModel->getModelData();

    GXVtxAttrFmtList*	vatPt = currentModelData->VertexP->vtxAttr;
			//pModelData->getVtxAttrFmtList();
    vatPt++;	// GX_POS, GX_NRMの順に並んでいることが必須

    if (info->enable == TRUE) {
        u8 unitSize = 9;
        GXSetVtxAttrFmt(GX_VTXFMT0, GX_VA_NBT, GX_NRM_NBT, vatPt->type, vatPt->frac);
        switch(vatPt->type) {
        case GX_S8:
            unitSize *= sizeof(s8);
            break;
        case GX_S16:
            unitSize *= sizeof(s16);
            break;
        case GX_F32:
            unitSize *= sizeof(f32);
            break;
        }
        GXSetArray(GX_VA_NBT,
				   currentModelData->VertexP->vtxNbt,
				   unitSize);
        
        currentNBTScale = &(info->scale);
    } else {
        u8 unitSize = 3;
        GXSetVtxAttrFmt(GX_VTXFMT0, GX_VA_NRM, GX_NRM_XYZ, vatPt->type, vatPt->frac);
        switch(vatPt->type) {
        case GX_S8:
            unitSize *= sizeof(s8);
            break;
        case GX_S16:
            unitSize *= sizeof(s16);
            break;
        case GX_F32:
            unitSize *= sizeof(f32);
            break;
        }
        GXSetArray(GX_VA_NRM,
				   currentModelData->VertexP->vtxNrm,
				   unitSize);

        currentNBTScale = NULL;
    }
}

/*-----------------------------------------------------------------------------
  環境マップ用のマトリックスロード
  -----------------------------------------------------------------------------*/
static void loadEnvMtx(
     f32             mtxPtr[][4], 
     u32             id, 
     GXTexMtxType    type )
{
#pragma unused(mtxPtr)
#pragma unused(id)
#pragma unused(type)

	/*
	実際には、draw時にしか決定しないので、ここでloadするのはあきらめる。
	*/

}

/*-----------------------------------------------------------------------------
  投影マップ用のマトリックスロード
  -----------------------------------------------------------------------------*/
static void loadProjMtx(
     f32             mtxPtr[][4], 
     u32             id, 
     GXTexMtxType    type )
{
#pragma unused(mtxPtr)
#pragma unused(id)
#pragma unused(type)

	/*
	実際には、draw時にしか決定しないので、ここでloadするのはあきらめる。
	*/

}

/*-----------------------------------------------------------------------------
  マテリアルの設定
  -----------------------------------------------------------------------------*/
void
loadMaterial( u16 i )
{
	int j;
	u8	texMapFlag[10];

	gmrMaterial* mArray = &currentModelData->material[i];

	for ( j=0 ; j<10 ; j++ ){
		texMapFlag[j] = cTexMapNormal;
	}

	if ( mArray->CullMode != 0xff ) { 
		GXSetCullMode( (GXCullMode)mArray->CullMode );
	}

	if ( mArray->colorChanNum != 0xff ) GXSetNumChans( mArray->colorChanNum );
	if ( mArray->colorPtr[0] != NULL ) GXSetChanMatColor( GX_COLOR0A0 , *mArray->colorPtr[0] );
	if ( mArray->colorPtr[1] != NULL ) GXSetChanMatColor( GX_COLOR1A1 , *mArray->colorPtr[1] );

	if ( mArray->colorChanPtr[0] != NULL ) {
		GXSetChanCtrl(GX_COLOR0, mArray->colorChanPtr[0]->enable, GX_SRC_REG, (GXColorSrc) mArray->colorChanPtr[0]->matSrc, mArray->colorChanPtr[0]->lightMask, (GXDiffuseFn)mArray->colorChanPtr[0]->diffuseFn, (GXAttnFn)mArray->colorChanPtr[0]->attnFn);
	}
	if ( mArray->colorChanPtr[1] != NULL ) {
		GXSetChanCtrl(GX_ALPHA0, mArray->colorChanPtr[1]->enable, GX_SRC_REG, (GXColorSrc) mArray->colorChanPtr[1]->matSrc, mArray->colorChanPtr[1]->lightMask, (GXDiffuseFn)mArray->colorChanPtr[1]->diffuseFn, (GXAttnFn)mArray->colorChanPtr[1]->attnFn);
	}
	if ( mArray->colorChanPtr[2] != NULL ) {
		GXSetChanCtrl(GX_COLOR1, mArray->colorChanPtr[2]->enable, GX_SRC_REG, (GXColorSrc) mArray->colorChanPtr[2]->matSrc, mArray->colorChanPtr[2]->lightMask, (GXDiffuseFn)mArray->colorChanPtr[2]->diffuseFn, (GXAttnFn)mArray->colorChanPtr[2]->attnFn);
	}
	if ( mArray->colorChanPtr[3] != NULL ) {
		GXSetChanCtrl(GX_ALPHA1, mArray->colorChanPtr[3]->enable, GX_SRC_REG, (GXColorSrc) mArray->colorChanPtr[3]->matSrc, mArray->colorChanPtr[3]->lightMask, (GXDiffuseFn)mArray->colorChanPtr[3]->diffuseFn, (GXAttnFn)mArray->colorChanPtr[3]->attnFn);
	}
	if ( mArray->texGenNum != 0xff ) {
		GXSetNumTexGens( mArray->texGenNum );
	}

	for ( j=0 ; j<mArray->texGenNum ; j++ ) {
		if ( mArray->texCoordPtr[j] != NULL ) {
			GXSetTexCoordGen((GXTexCoordID)(GX_TEXCOORD0+j),
							 (GXTexGenType)mArray->texCoordPtr[j]->texGenType,
							 (GXTexGenSrc)mArray->texCoordPtr[j]->texGenSrc,
							 (GXTexMtx)mArray->texCoordPtr[j]->texMtx);
		}
	}
	for ( j=0 ; j<10 ; j++ ){
		if ( mArray->texMtxPtr[j] != NULL ) {
			if ( texMapFlag[j] == cTexMapNormal ) {
				GXLoadTexMtxImm(mArray->texMtxPtr[j]->mtx, (u32)(GX_TEXMTX0+j*3), (GXTexMtxType)mArray->texMtxPtr[j]->texGenType);
			}
		}
	}

	if ( mArray->tevStageNum != 0xff ) {
		GXSetNumTevStages( mArray->tevStageNum );
	}

	for ( j=0 ; j<4 ; j++ ) {
		if ( mArray->tevColorPtr[j] != NULL ) {
			GXSetTevColorS10((GXTevRegID)(GX_TEVREG0+j), *mArray->tevColorPtr[j] );
		}
	}
	for ( j=0 ; j < 8/*mArray->tevStageNum*/ ; j++ ) {
		if ( mArray->tevStagePtr[j] != NULL ) {
			J3DTevStageInfo* info = mArray->tevStagePtr[j];
			if ( info->aA == GX_CA_KONST ||
				 info->aB == GX_CA_KONST ||
				 info->aC == GX_CA_KONST ||
				 info->aD == GX_CA_KONST ) {
			}
			GXSetTevKColorSel( (GXTevStageID)(GX_TEVSTAGE0+j), GX_TEV_KCSEL_1_4 );
			GXSetTevKAlphaSel( (GXTevStageID)(GX_TEVSTAGE0+j), GX_TEV_KASEL_1 );
			switch(info->mode) {
			case GX_DECAL:
			case GX_MODULATE:
			case GX_REPLACE:
			case GX_PASSCLR:
			case GX_BLEND:
				GXSetTevOp((GXTevStageID)(GX_TEVSTAGE0+j), (GXTevMode)info->mode);
				break;
			default:
				GXSetTevColorIn((GXTevStageID)(GX_TEVSTAGE0+j), (GXTevColorArg)info->cA, (GXTevColorArg)info->cB, (GXTevColorArg)info->cC, (GXTevColorArg)info->cD);
				GXSetTevColorOp((GXTevStageID)(GX_TEVSTAGE0+j), (GXTevOp)info->cOp, (GXTevBias)info->cBias, (GXTevScale)info->cScale, (GXBool)info->cClamp, (GXTevRegID)info->cOutReg);
				GXSetTevAlphaIn((GXTevStageID)(GX_TEVSTAGE0+j), (GXTevAlphaArg)info->aA, (GXTevAlphaArg)info->aB, (GXTevAlphaArg)info->aC, (GXTevAlphaArg)info->aD);
				GXSetTevAlphaOp((GXTevStageID)(GX_TEVSTAGE0+j), (GXTevOp)info->aOp, (GXTevBias)info->aBias, (GXTevScale)info->aScale, (GXBool)info->aClamp, (GXTevRegID)info->aOutReg);
				break;
			}
		}
		if ( mArray->tevOrderPtr[j] != NULL ) {
			GXSetTevOrder((GXTevStageID)(GX_TEVSTAGE0+j), (GXTexCoordID)mArray->tevOrderPtr[j]->coord, (GXTexMapID)mArray->tevOrderPtr[j]->map, (GXChannelID)mArray->tevOrderPtr[j]->color);
		} else {
			GXSetTevOrder((GXTevStageID)(GX_TEVSTAGE0+j), GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
		}
		if ( mArray->texNo[j] != 0xffff ) {
			loadTexture( (u8)j, &(currentModelData->TextureP->textureImage[ mArray->texNo[j] ]) );
		}
	}

	if ( mArray->nbtScalePtr != NULL ) setNBTScaleInfo( mArray->nbtScalePtr );
	

	if ( currentModel->modelAlpha != 0xff )
	{
		// TeVの最後のstageにalphaを追加する。
		GXColor color = (GXColor){0x00,0x00,0x00,(u8)currentModel->modelAlpha};
		
		if ( mArray->tevStageNum != 0xff ) {
//			DIReport("tevStageNum:%d\n",mArray->tevStageNum);
			GXSetNumTevStages( (u8)(mArray->tevStageNum + 1) );
			GXSetTevKColor( GX_KCOLOR0  , color );
			GXSetTevKAlphaSel( (GXTevStageID)(GX_TEVSTAGE0 + mArray->tevStageNum) , GX_TEV_KASEL_K0_A );
			GXSetTevColorIn( (GXTevStageID)(GX_TEVSTAGE0 + mArray->tevStageNum) , GX_CC_ZERO, GX_CC_ONE , GX_CC_CPREV, GX_CC_ZERO );
			GXSetTevColorOp( (GXTevStageID)(GX_TEVSTAGE0 + mArray->tevStageNum) , GX_TEV_ADD, GX_TB_ZERO, GX_CS_SCALE_1, GX_TRUE, GX_TEVPREV );

			GXSetTevAlphaIn( (GXTevStageID)(GX_TEVSTAGE0 + mArray->tevStageNum) , GX_CA_ZERO, GX_CA_KONST , GX_CA_APREV, GX_CA_ZERO );
			GXSetTevAlphaOp( (GXTevStageID)(GX_TEVSTAGE0 + mArray->tevStageNum) , GX_TEV_ADD, GX_TB_ZERO, GX_CS_SCALE_1, GX_TRUE, GX_TEVPREV );

			GXSetTevOrder( (GXTevStageID)(GX_TEVSTAGE0 + mArray->tevStageNum) ,
						   GX_TEXCOORD_NULL,
						   GX_TEXMAP_NULL,
						   GX_COLOR0A0 );

		}
		GXSetZMode( GX_FALSE , GX_LEQUAL , GX_FALSE );
		GXSetZCompLoc( GX_FALSE ); // after texture
	}
	else {
		GXColor color = (GXColor){ 0x00,0x00,0x00,0xff };
		GXSetTevKColor( GX_KCOLOR0  , color );

		if ( (mArray->MaterialMode & gmrDrawMode_Opa) == gmrDrawMode_Opa ) {
			GXSetZMode( GX_TRUE , GX_LEQUAL , GX_TRUE );
			GXSetZCompLoc( GX_TRUE ); // after texture
		}
		else {
			GXSetZMode( GX_FALSE , GX_LEQUAL , GX_FALSE );
			GXSetZCompLoc( GX_FALSE ); // after texture
		}
	}

}

void
loadMaterialSmallCube( u16 i )
{
	gmrMaterial* mArray = &currentModelData->material[i];
	int j;

	for ( j=0 ; j<4 ; j++ ) {
		if ( mArray->tevColorPtr[j] != NULL ) {
			GXSetTevColorS10((GXTevRegID)(GX_TEVREG0+j), *mArray->tevColorPtr[j] );
		}
	}
	
	if ( currentModel->modelAlpha != 0xff )
	{
		// TeVの最後のstageにalphaを追加する。
		GXColor color = (GXColor){0x00,0x00,0x00,(u8)currentModel->modelAlpha};
		
		if ( mArray->tevStageNum != 0xff ) {
			GXSetNumTevStages( (u8)(mArray->tevStageNum + 1) );
			GXSetTevKColor( GX_KCOLOR0  , color );
			GXSetTevKAlphaSel( (GXTevStageID)(GX_TEVSTAGE0 + mArray->tevStageNum) , GX_TEV_KASEL_K0_A );
			GXSetTevColorIn( (GXTevStageID)(GX_TEVSTAGE0 + mArray->tevStageNum) , GX_CC_ZERO, GX_CC_ONE , GX_CC_CPREV, GX_CC_ZERO );
			GXSetTevColorOp( (GXTevStageID)(GX_TEVSTAGE0 + mArray->tevStageNum) , GX_TEV_ADD, GX_TB_ZERO, GX_CS_SCALE_1, GX_TRUE, GX_TEVPREV );

			GXSetTevAlphaIn( (GXTevStageID)(GX_TEVSTAGE0 + mArray->tevStageNum) , GX_CA_ZERO, GX_CA_KONST , GX_CA_APREV, GX_CA_ZERO );
			GXSetTevAlphaOp( (GXTevStageID)(GX_TEVSTAGE0 + mArray->tevStageNum) , GX_TEV_ADD, GX_TB_ZERO, GX_CS_SCALE_1, GX_TRUE, GX_TEVPREV );

			GXSetTevOrder( (GXTevStageID)(GX_TEVSTAGE0 + mArray->tevStageNum) ,
						   GX_TEXCOORD_NULL,
						   GX_TEXMAP_NULL,
						   GX_COLOR0A0 );

		}
		GXSetZMode( GX_FALSE , GX_LEQUAL , GX_FALSE );
		GXSetZCompLoc( GX_FALSE ); // after texture
	}
	else {
		GXColor color = (GXColor){ 0x00,0x00,0x00,0xff };
		GXSetTevKColor( GX_KCOLOR0  , color );

		if ( (mArray->MaterialMode & gmrDrawMode_Opa) == gmrDrawMode_Opa ) {
			GXSetZMode( GX_TRUE , GX_LEQUAL , GX_TRUE );
			GXSetZCompLoc( GX_TRUE ); // after texture
		}
		else {
			GXSetZMode( GX_FALSE , GX_LEQUAL , GX_FALSE );
			GXSetZCompLoc( GX_FALSE ); // after texture
		}
	}
}

VecPtr
getNBTScale(void)
{
	return currentNBTScale;
}
