/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: メニューモード時の各面のパラメータ
  -----------------------------------------------------------------------------*/
#include <dolphin.h>

#include "TimeState.h"
#include "TimeUpdate.h"

#ifdef JHOSTIO
#include "IHITimeState.h"
#endif

/*-----------------------------------------------------------------------------
  ファイルローカル変数
  -----------------------------------------------------------------------------*/
static TimeState* stp;

#ifndef JHOSTIO
static TimeState st;
#endif

/*-----------------------------------------------------------------------------
  初期化
  -----------------------------------------------------------------------------*/
void
initTime( BOOL first )
{
	if ( first ) {
#ifndef JHOSTIO
		stp = &st;
		initTimeState( stp );
#else
		stp = getIHITimeState();
#endif
	}

	initTimeMenu( first );
}

/*-----------------------------------------------------------------------------
  パラメータ初期化
  -----------------------------------------------------------------------------*/
void
initTimeState( TimeState* p )
{
#pragma unused ( p )
	/*
	  p->param = 0;
	  */
	/*
	p->cube_hari_r = 255;
	p->cube_hari_g = 255;
	p->cube_hari_b = 0;
	p->cube_hari_a = 255;

	p->cube_other_r = 255;
	p->cube_other_g = 180;
	p->cube_other_b = 0;
	p->cube_other_a = 255;
	  */
	p->cube_hari_r = 40;
	p->cube_hari_g = 20;
	p->cube_hari_b = -50;
	p->cube_hari_a = 255;


	p->cube_other_r = 80;
	p->cube_other_g = 0;
	p->cube_other_b = -60;
	p->cube_other_a = 255;

	p->year_x = 251 * 16;
	p->mon_x  = 292 * 16;
	p->day_x  = 372 * 16;
	p->hour_x = 255 * 16;
	p->min_x  = 292 * 16;
	p->sec_x  = 349 * 16;

	p->moji_r = 255;
	p->moji_g = 255;
	p->moji_b = 0;

	p->concent_dec_alpha = 14;
	p->concent_dec_pos = 6;
}

/*-----------------------------------------------------------------------------
  パラメータポインタ取得
  -----------------------------------------------------------------------------*/
TimeState*
getTimeState( void )
{
	return stp;
}

