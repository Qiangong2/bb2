/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: アーカイブをロードするためのルーチン。
  -----------------------------------------------------------------------------*/
#include <dolphin.h>
#include <private/flipper.h>
#include <private/OSRtc.h>

#include "gBase.h"
#include "gLoader.h"

#ifdef PAL
#include "CardUpdate.h"
#include "MenuUpdate.h"
#include "SplashDraw.h"
#include "StartUpdate.h"
#include "TimeUpdate.h"
#include "OptionUpdate.h"
#endif

#ifdef IPL
#if defined(MPAL)
#include "gcarchive_mpal.h"
#elif defined(PAL)
#include "gcarchive_pal.h"
#else
#include "gcarchive_ntsc.h"
#endif // MPAL/PAL/NTSC
#endif // IPL

#if defined(MPAL)
#include "mpal_mesg.h"
#elif defined(PAL)
#include "pal_mesg.h"
#else // NTSC
#include "ntsc_mesg.h"
#endif
/*-----------------------------------------------------------------------------
  static value
  -----------------------------------------------------------------------------*/
static char*	sFileBuf;

static int		d2_memory_layout		;
static int		d2_gamestart_layout		;
static int		d2_calendar_layout		;
static int		d2_demo_layout			;
static int		d2_operation_layout		;
static int		d2_option_layout		;

static int		d2_memory_string		[ COUNTRY_PAL_MAX_LANGUAGE ];
static int		d2_gamestart_string		[ COUNTRY_PAL_MAX_LANGUAGE ];
static int		d2_calendar_string		[ COUNTRY_PAL_MAX_LANGUAGE ];
static int		d2_demo_string			[ COUNTRY_PAL_MAX_LANGUAGE ];
static int		d2_operation_string		[ COUNTRY_PAL_MAX_LANGUAGE ];
static int		d2_option_string		[ COUNTRY_PAL_MAX_LANGUAGE ];

static int		d3_memory_layout		[ COUNTRY_PAL_MAX_LANGUAGE ];
static int		d3_option_layout		[ COUNTRY_PAL_MAX_LANGUAGE ];
static int		d3_gamestart_layout		[ COUNTRY_PAL_MAX_LANGUAGE ];
static int		d3_calendar_layout		[ COUNTRY_PAL_MAX_LANGUAGE ];
static int		d3_main_layout			[ COUNTRY_PAL_MAX_LANGUAGE ];

static int		texture_archive			;

static int		calendar_num_tex_base	;
static int		calendar_week_tex_base	[ COUNTRY_PAL_MAX_LANGUAGE ];
static int		option_sound_mono_tex	[ COUNTRY_PAL_MAX_LANGUAGE ];
static int		option_sound_stereo_tex	[ COUNTRY_PAL_MAX_LANGUAGE ];
static int		blocknumber_tex			;

static int		smallcubefaderdata      [ COUNTRY_PAL_MAX_LANGUAGE ];

static u16		sCountry;

/*-----------------------------------------------------------------------------
  国情報を返します。
  
  -----------------------------------------------------------------------------*/
u16
gGetNowCountry( void )
{
	return sCountry;
}

s16
gGetPalCountry( void )
{
#ifdef PAL
	return (s16)( sCountry - COUNTRY_PAL_OFFSET );
#else
	return 0;
#endif
}

void
gSetPalCountry( int country )
{
#ifdef PAL
	OSSram*	sram;
	BOOL	saveSram = FALSE;
	
	sram = __OSLockSram();

	ASSERT(sram);

	if ( sram->language != country ) {
		saveSram = TRUE;
		sram->language = (u8)country;
		sCountry = (u16)( COUNTRY_PAL_OFFSET + country );
	}
	
	__OSUnlockSram( saveSram );
	
	if ( saveSram ) {
		while (!__OSSyncSram()) {}
		initCardMenu_Lang();
		initMenuDraw_Lang();
		initSplashDraw_Lang();
		initStartMenu_Lang();
		initTimeMenu_Lang();
		initOptionMenu_Lang();
		setOperationLanguage( country );
	}
#else
#pragma unused ( country )
#endif
}

static void
setCountryID( void )
{
#if defined(MPAL)
	if ( sCountry == COUNTRY_BRAZIL ) {
		// 自動性成分（選別は人がやる）。
		d2_memory_layout		= GC_2d_memorycard_3_lay;
		d2_operation_layout		= GC_mpal_2d_operation_lay;
		d2_option_layout		= GC_2d_option_2_j_lay;
		d2_gamestart_layout		= GC_2d_gameplay_2_lay;
		d2_calendar_layout		= GC_2d_calendar_2_lay;
		d2_demo_layout			= GC_2d_demo_2_lay;

		d2_memory_string		[0] = GC_2d_memorycard_3_MPAL_Portuguese_str;
		d2_operation_string		[0] = GC_mpal_2d_operation_MPAL_Portuguese_str;
		d2_option_string		[0] = GC_2d_option_2_j_MPAL_Portuguese_str;
		d2_gamestart_string		[0] = GC_2d_gameplay_2_MPAL_Portuguese_str;
		d2_calendar_string		[0] = GC_2d_calendar_2_MPAL_Portuguese_str;
		d2_demo_string			[0] = GC_2d_demo_2_MPAL_Portuguese_str;

		d3_memory_layout		[0] = GC_mpal_3d_memorycard_lay;
		d3_option_layout		[0] = GC_mpal_3d_option_lay;
		d3_gamestart_layout		[0] = GC_mpal_3d_gameplay_lay;
		d3_calendar_layout		[0] = GC_mpal_3d_calendar_lay;
		d3_main_layout			[0] = GC_mpal_3d_main_lay;
		texture_archive			= GC_mpal_tex_szp;

		// 手動追加分。
		calendar_num_tex_base	= GC_j_num_0_16x22_bti;
		calendar_week_tex_base	[0] = GC_mp_week_0_48x24_bti;
		option_sound_mono_tex	[0] = GC_mp_sound_mono_120x24_bti;
		option_sound_stereo_tex	[0] = GC_mp_sound_stereo_120x24_bti;
		blocknumber_tex			= GC_j_blocknumber_110x14_bti;
		smallcubefaderdata		[0] = GC_smallcubefaderdata_mpal_szp;
	}
#elif defined(PAL)
	// Common
	d2_memory_layout		= GC_2d_memorycard_3_lay;
	d2_gamestart_layout		= GC_2d_gameplay_2_lay;
	d2_calendar_layout		= GC_2d_calendar_2_lay;
	d2_demo_layout			= GC_pal_2d_demo_lay;
	d2_operation_layout		= GC_pal_2d_operation_lay;
	d2_option_layout		= GC_pal_2d_option_lay;

	texture_archive			= GC_pal_tex_szp;
	calendar_num_tex_base	= GC_j_num_0_16x22_bti;
	blocknumber_tex			= GC_j_blocknumber_110x14_bti;

	// Language
	d2_memory_string		[ COUNTRY_PAL_ENGLISH  - COUNTRY_PAL_OFFSET ] = GC_2d_memorycard_3_PAL_English_str;
	d2_memory_string		[ COUNTRY_PAL_FRENCH   - COUNTRY_PAL_OFFSET ] = GC_2d_memorycard_3_PAL_French_str;
	d2_memory_string		[ COUNTRY_PAL_GERMAN   - COUNTRY_PAL_OFFSET ] = GC_2d_memorycard_3_PAL_German_str;
	d2_memory_string		[ COUNTRY_PAL_DUTCH    - COUNTRY_PAL_OFFSET ] = GC_2d_memorycard_3_PAL_Dutch_str;
	d2_memory_string		[ COUNTRY_PAL_SPANISH  - COUNTRY_PAL_OFFSET ] = GC_2d_memorycard_3_PAL_Spanish_str;
	d2_memory_string		[ COUNTRY_PAL_ITALIAN  - COUNTRY_PAL_OFFSET ] = GC_2d_memorycard_3_PAL_Italian_str;
	
	d2_operation_string		[ COUNTRY_PAL_ENGLISH  - COUNTRY_PAL_OFFSET ] = GC_pal_2d_operation_PAL_English_str;
	d2_operation_string		[ COUNTRY_PAL_FRENCH   - COUNTRY_PAL_OFFSET ] = GC_pal_2d_operation_PAL_French_str;
	d2_operation_string		[ COUNTRY_PAL_GERMAN   - COUNTRY_PAL_OFFSET ] = GC_pal_2d_operation_PAL_German_str;
	d2_operation_string		[ COUNTRY_PAL_DUTCH    - COUNTRY_PAL_OFFSET ] = GC_pal_2d_operation_PAL_Dutch_str;
	d2_operation_string		[ COUNTRY_PAL_SPANISH  - COUNTRY_PAL_OFFSET ] = GC_pal_2d_operation_PAL_Spanish_str;
	d2_operation_string		[ COUNTRY_PAL_ITALIAN  - COUNTRY_PAL_OFFSET ] = GC_pal_2d_operation_PAL_Italian_str;
	
	d2_option_string		[ COUNTRY_PAL_ENGLISH  - COUNTRY_PAL_OFFSET ] = GC_pal_2d_option_PAL_English_str;
	d2_option_string		[ COUNTRY_PAL_FRENCH   - COUNTRY_PAL_OFFSET ] = GC_pal_2d_option_PAL_French_str;
	d2_option_string		[ COUNTRY_PAL_GERMAN   - COUNTRY_PAL_OFFSET ] = GC_pal_2d_option_PAL_German_str;
	d2_option_string		[ COUNTRY_PAL_DUTCH    - COUNTRY_PAL_OFFSET ] = GC_pal_2d_option_PAL_Dutch_str;
	d2_option_string		[ COUNTRY_PAL_SPANISH  - COUNTRY_PAL_OFFSET ] = GC_pal_2d_option_PAL_Spanish_str;
	d2_option_string		[ COUNTRY_PAL_ITALIAN  - COUNTRY_PAL_OFFSET ] = GC_pal_2d_option_PAL_Italian_str;

	d2_gamestart_string		[ COUNTRY_PAL_ENGLISH  - COUNTRY_PAL_OFFSET ] = GC_2d_gameplay_2_PAL_English_str;
	d2_gamestart_string		[ COUNTRY_PAL_FRENCH   - COUNTRY_PAL_OFFSET ] = GC_2d_gameplay_2_PAL_French_str;
	d2_gamestart_string		[ COUNTRY_PAL_GERMAN   - COUNTRY_PAL_OFFSET ] = GC_2d_gameplay_2_PAL_German_str;
	d2_gamestart_string		[ COUNTRY_PAL_DUTCH    - COUNTRY_PAL_OFFSET ] = GC_2d_gameplay_2_PAL_Dutch_str;
	d2_gamestart_string		[ COUNTRY_PAL_SPANISH  - COUNTRY_PAL_OFFSET ] = GC_2d_gameplay_2_PAL_Spanish_str;
	d2_gamestart_string		[ COUNTRY_PAL_ITALIAN  - COUNTRY_PAL_OFFSET ] = GC_2d_gameplay_2_PAL_Italian_str;

	d2_calendar_string		[ COUNTRY_PAL_ENGLISH  - COUNTRY_PAL_OFFSET ] = GC_2d_calendar_2_PAL_English_str;
	d2_calendar_string		[ COUNTRY_PAL_FRENCH   - COUNTRY_PAL_OFFSET ] = GC_2d_calendar_2_PAL_French_str;
	d2_calendar_string		[ COUNTRY_PAL_GERMAN   - COUNTRY_PAL_OFFSET ] = GC_2d_calendar_2_PAL_German_str;
	d2_calendar_string		[ COUNTRY_PAL_DUTCH    - COUNTRY_PAL_OFFSET ] = GC_2d_calendar_2_PAL_Dutch_str;
	d2_calendar_string		[ COUNTRY_PAL_SPANISH  - COUNTRY_PAL_OFFSET ] = GC_2d_calendar_2_PAL_Spanish_str;
	d2_calendar_string		[ COUNTRY_PAL_ITALIAN  - COUNTRY_PAL_OFFSET ] = GC_2d_calendar_2_PAL_Italian_str;

	d2_demo_string		[ COUNTRY_PAL_ENGLISH  - COUNTRY_PAL_OFFSET ] = GC_pal_2d_demo_PAL_English_str;
	d2_demo_string		[ COUNTRY_PAL_FRENCH   - COUNTRY_PAL_OFFSET ] = GC_pal_2d_demo_PAL_French_str;
	d2_demo_string		[ COUNTRY_PAL_GERMAN   - COUNTRY_PAL_OFFSET ] = GC_pal_2d_demo_PAL_German_str;
	d2_demo_string		[ COUNTRY_PAL_DUTCH    - COUNTRY_PAL_OFFSET ] = GC_pal_2d_demo_PAL_Dutch_str;
	d2_demo_string		[ COUNTRY_PAL_SPANISH  - COUNTRY_PAL_OFFSET ] = GC_pal_2d_demo_PAL_Spanish_str;
	d2_demo_string		[ COUNTRY_PAL_ITALIAN  - COUNTRY_PAL_OFFSET ] = GC_pal_2d_demo_PAL_Italian_str;

	d3_memory_layout		[ COUNTRY_PAL_ENGLISH  - COUNTRY_PAL_OFFSET ] = GC_english_3d_memorycard_lay;
	d3_memory_layout		[ COUNTRY_PAL_FRENCH   - COUNTRY_PAL_OFFSET ] = GC_pal_f_3d_memorycard_lay;
	d3_memory_layout		[ COUNTRY_PAL_GERMAN   - COUNTRY_PAL_OFFSET ] = GC_pal_g_3d_memorycard_lay;
	d3_memory_layout		[ COUNTRY_PAL_DUTCH    - COUNTRY_PAL_OFFSET ] = GC_pal_d_3d_memorycard_lay;
	d3_memory_layout		[ COUNTRY_PAL_SPANISH  - COUNTRY_PAL_OFFSET ] = GC_pal_s_3d_memorycard_lay;
	d3_memory_layout		[ COUNTRY_PAL_ITALIAN  - COUNTRY_PAL_OFFSET ] = GC_pal_i_3d_memorycard_lay;

	d3_gamestart_layout		[ COUNTRY_PAL_ENGLISH  - COUNTRY_PAL_OFFSET ] = GC_english_3d_gameplay_lay;
	d3_gamestart_layout		[ COUNTRY_PAL_FRENCH   - COUNTRY_PAL_OFFSET ] = GC_pal_f_3d_gameplay_lay;
	d3_gamestart_layout		[ COUNTRY_PAL_GERMAN   - COUNTRY_PAL_OFFSET ] = GC_pal_g_3d_gameplay_lay;
	d3_gamestart_layout		[ COUNTRY_PAL_DUTCH    - COUNTRY_PAL_OFFSET ] = GC_pal_d_3d_gameplay_lay;
	d3_gamestart_layout		[ COUNTRY_PAL_SPANISH  - COUNTRY_PAL_OFFSET ] = GC_pal_s_3d_gameplay_lay;
	d3_gamestart_layout		[ COUNTRY_PAL_ITALIAN  - COUNTRY_PAL_OFFSET ] = GC_pal_i_3d_gameplay_lay;
	
	d3_calendar_layout		[ COUNTRY_PAL_ENGLISH  - COUNTRY_PAL_OFFSET ] = GC_pal_e_3d_calendar_lay;
	d3_calendar_layout		[ COUNTRY_PAL_FRENCH   - COUNTRY_PAL_OFFSET ] = GC_pal_f_3d_calendar_lay;
	d3_calendar_layout		[ COUNTRY_PAL_GERMAN   - COUNTRY_PAL_OFFSET ] = GC_pal_g_3d_calendar_lay;
	d3_calendar_layout		[ COUNTRY_PAL_DUTCH    - COUNTRY_PAL_OFFSET ] = GC_pal_d_3d_calendar_lay;
	d3_calendar_layout		[ COUNTRY_PAL_SPANISH  - COUNTRY_PAL_OFFSET ] = GC_pal_s_3d_calendar_lay;
	d3_calendar_layout		[ COUNTRY_PAL_ITALIAN  - COUNTRY_PAL_OFFSET ] = GC_pal_i_3d_calendar_lay;

	d3_main_layout			[ COUNTRY_PAL_ENGLISH  - COUNTRY_PAL_OFFSET ] = GC_english_3d_main_lay;
	d3_main_layout			[ COUNTRY_PAL_FRENCH   - COUNTRY_PAL_OFFSET ] = GC_pal_f_3d_main_lay;
	d3_main_layout			[ COUNTRY_PAL_GERMAN   - COUNTRY_PAL_OFFSET ] = GC_pal_g_3d_main_lay;
	d3_main_layout			[ COUNTRY_PAL_DUTCH    - COUNTRY_PAL_OFFSET ] = GC_pal_d_3d_main_lay;
	d3_main_layout			[ COUNTRY_PAL_SPANISH  - COUNTRY_PAL_OFFSET ] = GC_pal_s_3d_main_lay;
	d3_main_layout			[ COUNTRY_PAL_ITALIAN  - COUNTRY_PAL_OFFSET ] = GC_pal_i_3d_main_lay;

	d3_option_layout		[ COUNTRY_PAL_ENGLISH  - COUNTRY_PAL_OFFSET ] = GC_pal_e_3d_option_lay;
	d3_option_layout		[ COUNTRY_PAL_FRENCH   - COUNTRY_PAL_OFFSET ] = GC_pal_f_3d_option_lay;
	d3_option_layout		[ COUNTRY_PAL_GERMAN   - COUNTRY_PAL_OFFSET ] = GC_pal_g_3d_option_lay;
	d3_option_layout		[ COUNTRY_PAL_DUTCH    - COUNTRY_PAL_OFFSET ] = GC_pal_d_3d_option_lay;
	d3_option_layout		[ COUNTRY_PAL_SPANISH  - COUNTRY_PAL_OFFSET ] = GC_pal_s_3d_option_lay;
	d3_option_layout		[ COUNTRY_PAL_ITALIAN  - COUNTRY_PAL_OFFSET ] = GC_pal_i_3d_option_lay;

	// 手動追加分。
	calendar_week_tex_base	[ COUNTRY_PAL_ENGLISH  - COUNTRY_PAL_OFFSET ] = GC_e_week_0_48x24_bti;
	calendar_week_tex_base	[ COUNTRY_PAL_FRENCH   - COUNTRY_PAL_OFFSET ] = GC_f_week_0_48x24_bti;
	calendar_week_tex_base	[ COUNTRY_PAL_GERMAN   - COUNTRY_PAL_OFFSET ] = GC_g_week_0_48x24_bti;
	calendar_week_tex_base	[ COUNTRY_PAL_DUTCH    - COUNTRY_PAL_OFFSET ] = GC_d_week_0_48x24_bti;
	calendar_week_tex_base	[ COUNTRY_PAL_SPANISH  - COUNTRY_PAL_OFFSET ] = GC_s_week_0_48x24_bti;
	calendar_week_tex_base	[ COUNTRY_PAL_ITALIAN  - COUNTRY_PAL_OFFSET ] = GC_i_week_0_48x24_bti;

	option_sound_mono_tex	[ COUNTRY_PAL_ENGLISH  - COUNTRY_PAL_OFFSET ] = GC_e_sound_mono_120x24_szp;
	option_sound_mono_tex	[ COUNTRY_PAL_FRENCH   - COUNTRY_PAL_OFFSET ] = GC_f_sound_mono_120x24_szp;
	option_sound_mono_tex	[ COUNTRY_PAL_GERMAN   - COUNTRY_PAL_OFFSET ] = GC_g_sound_mono_120x24_szp;
	option_sound_mono_tex	[ COUNTRY_PAL_DUTCH    - COUNTRY_PAL_OFFSET ] = GC_d_sound_mono_120x24_szp;
	option_sound_mono_tex	[ COUNTRY_PAL_SPANISH  - COUNTRY_PAL_OFFSET ] = GC_s_sound_mono_120x24_szp;
	option_sound_mono_tex	[ COUNTRY_PAL_ITALIAN  - COUNTRY_PAL_OFFSET ] = GC_i_sound_mono_120x24_szp;

	option_sound_stereo_tex	[ COUNTRY_PAL_ENGLISH  - COUNTRY_PAL_OFFSET ] = GC_e_sound_stereo_120x24_szp;
	option_sound_stereo_tex	[ COUNTRY_PAL_FRENCH   - COUNTRY_PAL_OFFSET ] = GC_f_sound_stereo_120x24_szp;
	option_sound_stereo_tex	[ COUNTRY_PAL_GERMAN   - COUNTRY_PAL_OFFSET ] = GC_g_sound_stereo_120x24_szp;
	option_sound_stereo_tex	[ COUNTRY_PAL_DUTCH    - COUNTRY_PAL_OFFSET ] = GC_d_sound_stereo_120x24_szp;
	option_sound_stereo_tex	[ COUNTRY_PAL_SPANISH  - COUNTRY_PAL_OFFSET ] = GC_s_sound_stereo_120x24_szp;
	option_sound_stereo_tex	[ COUNTRY_PAL_ITALIAN  - COUNTRY_PAL_OFFSET ] = GC_i_sound_stereo_120x24_szp;

	smallcubefaderdata		[ COUNTRY_PAL_ENGLISH  - COUNTRY_PAL_OFFSET ] = GC_smallcubefaderdata_eng_szp;
	smallcubefaderdata		[ COUNTRY_PAL_FRENCH   - COUNTRY_PAL_OFFSET ] = GC_smallcubefaderdata_fra_szp;
	smallcubefaderdata		[ COUNTRY_PAL_GERMAN   - COUNTRY_PAL_OFFSET ] = GC_smallcubefaderdata_ger_szp;
	smallcubefaderdata		[ COUNTRY_PAL_DUTCH    - COUNTRY_PAL_OFFSET ] = GC_smallcubefaderdata_dut_szp;
	smallcubefaderdata		[ COUNTRY_PAL_SPANISH  - COUNTRY_PAL_OFFSET ] = GC_smallcubefaderdata_spa_szp;
	smallcubefaderdata		[ COUNTRY_PAL_ITALIAN  - COUNTRY_PAL_OFFSET ] = GC_smallcubefaderdata_ita_szp;


							 


#else // NTSC
	if ( sCountry == COUNTRY_JAPAN ) {
		// 自動性成分（選別は人がやる）。
		d2_memory_layout		= GC_2d_memorycard_3_lay;
		d2_operation_layout		= GC_2d_operation_1_lay;
		d2_option_layout		= GC_2d_option_2_j_lay;
		d2_gamestart_layout		= GC_2d_gameplay_2_lay;
		d2_calendar_layout		= GC_2d_calendar_2_lay;
		d2_demo_layout			= GC_2d_demo_2_lay;

		d2_memory_string		[0] = GC_2d_memorycard_3_Japanese_str;
		d2_operation_string		[0] = GC_2d_operation_1_Japanese_str;
		d2_option_string		[0] = GC_2d_option_2_j_Japanese_str;
		d2_gamestart_string		[0] = GC_2d_gameplay_2_Japanese_str;
		d2_calendar_string		[0] = GC_2d_calendar_2_Japanese_str;
		d2_demo_string			[0] = GC_2d_demo_2_Japanese_str;

		d3_memory_layout		[0] = GC_3d_memorycard_lay;
		d3_option_layout		[0] = GC_3d_option_lay;
		d3_gamestart_layout		[0] = GC_3d_gameplay_lay;
		d3_calendar_layout		[0] = GC_3d_calendar_lay;
		d3_main_layout			[0] = GC_3d_main_lay;
		texture_archive			= GC_ntsc_tex_szp;

		// 手動追加分。
		calendar_num_tex_base	= GC_j_num_0_16x22_bti;
		calendar_week_tex_base	[0] = GC_j_week_0_24x24_bti;
		option_sound_mono_tex	[0] = GC_j_sound_mono_120x24_bti;
		option_sound_stereo_tex	[0] = GC_j_sound_stereo_120x24_bti;
		blocknumber_tex			= GC_j_blocknumber_110x14_bti;
		smallcubefaderdata		[0] = GC_smallcubefaderdata_jpn_szp;
	}
	else if ( sCountry == COUNTRY_US ) {
		// 自動性成分（選別は人がやる）。
		d2_memory_layout		= GC_2d_memorycard_3_lay;
		d2_operation_layout		= GC_english_2d_operation_1_lay;
		d2_option_layout		= GC_2d_option_2_j_lay;
		d2_gamestart_layout		= GC_2d_gameplay_2_lay;
		d2_calendar_layout		= GC_2d_calendar_2_lay;
		d2_demo_layout			= GC_2d_demo_2_lay;

		d2_memory_string		[0] = GC_2d_memorycard_3_English_str;
		d2_operation_string		[0] = GC_english_2d_operation_1_English_str;
		d2_option_string		[0] = GC_2d_option_2_j_English_str;
		d2_gamestart_string		[0] = GC_2d_gameplay_2_English_str;
		d2_calendar_string		[0] = GC_2d_calendar_2_English_str;
		d2_demo_string			[0] = GC_2d_demo_2_English_str;
		
		d3_memory_layout		[0] = GC_english_3d_memorycard_lay;
		d3_option_layout		[0] = GC_english_3d_option_lay;
		d3_gamestart_layout		[0] = GC_english_3d_gameplay_lay;
		d3_calendar_layout		[0] = GC_english_3d_calendar_lay;
		d3_main_layout			[0] = GC_english_3d_main_lay;

		texture_archive			= GC_ntsc_tex_szp;

		// 手動追加分。
		calendar_num_tex_base	= GC_j_num_0_16x22_bti;
		calendar_week_tex_base	[0] = GC_e_week_0_48x24_bti;
		option_sound_mono_tex	[0] = GC_e_sound_mono_120x24_bti;
		option_sound_stereo_tex	[0] = GC_e_sound_stereo_120x24_bti;
		blocknumber_tex			= GC_j_blocknumber_110x14_bti;

		smallcubefaderdata		[0] = GC_smallcubefaderdata_eng_szp;
	}
#endif
}

void
gResolveLanguage( void )
{
#ifndef DDH

#if defined(MPAL)
	sCountry = COUNTRY_BRAZIL;
#elif defined(PAL)
	OSSram*	sram;
	sram = __OSLockSram();
	sCountry = (u16)( sram->language + COUNTRY_PAL_OFFSET );
	__OSUnlockSram( FALSE );
#else
	sCountry = (u16) (__VIRegs[VI_DTVSTATUS] & 0x02);
	DIReport1("country:%s\n",
			 (sCountry == COUNTRY_JAPAN)? "Japan":"US");
#endif
	
#else

#if defined(MPAL)
	sCountry = COUNTRY_BRAZIL;
#elif defined(PAL)
	OSSram*	sram;
	sram = __OSLockSram();
	sCountry = sram->language + COUNTRY_PAL_OFFSET;
	__OSUnlockSram( FALSE );
#else
	sCountry = COUNTRY_JAPAN;
	//sCountry = COUNTRY_US;
#endif
	
#endif

	setCountryID();
}

int 	getID_2D_memory_layout( void ) 		{ return	d2_memory_layout	; }
int 	getID_2D_operation_layout( void ) 	{ return	d2_operation_layout	; }
int 	getID_2D_option_layout( void )	 	{ return	d2_option_layout	; }
int 	getID_2D_gamestart_layout( void ) 	{ return	d2_gamestart_layout	; }
int 	getID_2D_calendar_layout( void ) 	{ return	d2_calendar_layout	; }
int 	getID_2D_demo_layout( void ) 		{ return	d2_demo_layout		; }

int 	getID_2D_memory_string( void ) 		{ return	d2_memory_string	[ gGetPalCountry() ];	}
int 	getID_2D_operation_string( void ) 	{ return	d2_operation_string	[ gGetPalCountry() ]; }
int 	getID_2D_option_string( void ) 		{ return	d2_option_string	[ gGetPalCountry() ];	}
int 	getID_2D_gamestart_string( void ) 	{ return	d2_gamestart_string	[ gGetPalCountry() ]; }
int 	getID_2D_calendar_string( void ) 	{ return	d2_calendar_string	[ gGetPalCountry() ];	}
int 	getID_2D_demo_string( void ) 		{ return	d2_demo_string		[ gGetPalCountry() ];	}

int 	getID_3D_memory_layout( void ) 		{ return	d3_memory_layout	[ gGetPalCountry() ];	}
int 	getID_3D_option_layout( void ) 		{ return	d3_option_layout	[ gGetPalCountry() ];	}
int 	getID_3D_gamestart_layout( void ) 	{ return	d3_gamestart_layout	[ gGetPalCountry() ]; }
int 	getID_3D_calendar_layout( void ) 	{ return	d3_calendar_layout	[ gGetPalCountry() ];	}
int 	getID_3D_main_layout( void ) 		{ return	d3_main_layout		[ gGetPalCountry() ];	}

int 	getID_texture_archive( void )			{ return	texture_archive;		}
int 	getID_calendar_num_tex_base( void )		{ return	calendar_num_tex_base	; }
int 	getID_calendar_week_tex_base( void )	{ return	calendar_week_tex_base	[ gGetPalCountry() ];	}
int 	getID_option_sound_mono_tex( void )		{ return	option_sound_mono_tex	[ gGetPalCountry() ];	}
int 	getID_option_sound_stereo_tex( void )	{ return	option_sound_stereo_tex	[ gGetPalCountry() ];   }
int 	getID_blocknumber_tex( void )			{ return	blocknumber_tex			; }
int 	getID_smallcubefaderdata( void )		{ return	smallcubefaderdata[ gGetPalCountry() ];	}

int 	getID_2D_operation_string_pal( int country ) 	{ return	d2_operation_string	[ country ]; }
int 	getID_2D_option_string_pal( int country ) 		{ return	d2_option_string	[ country ]; }

int 	getID_smallcubefaderdata_pal( int country ) 	{ return	smallcubefaderdata	[ country ]; }



/*-----------------------------------------------------------------------------
  functions
  -----------------------------------------------------------------------------*/
static void
decode(u8 *s,u8 *d)
{
	int i,j,k,p,q,r7,r25,cnt,os;
	unsigned flag,code;
	os=(s[4]<<24)|(s[5]<<16)|(s[6]<<8)|s[7];
	r7=(s[8]<<24)|(s[9]<<16)|(s[10]<<8)|s[11];
	r25=(s[12]<<24)|(s[13]<<16)|(s[14]<<8)|s[15];
	q=0;flag=0;p=16;
	do{
		if(flag==0){
			code=(unsigned)(s[p]<<24)|(s[p+1]<<16)|(s[p+2]<<8)|s[p+3];p+=4;
			flag=32;
		}
		if(code&0x80000000)d[q++]=s[r25++];
		else{
			j=(s[r7]<<8)|s[r7+1];r7+=2;
			k=q-(j&0xFFF);cnt=j>>12;
			if(cnt==0)cnt=s[r25++]+18;
			else cnt+=2;
			for(i=0;i<cnt;i++,q++,k++)d[q]=d[k-1];
		}
		code<<=1;flag--;
	}while(q<os);
}

static int
checkCompressed( u8* buf )
{
	if ( buf[0] == 'Y' &&
		 buf[1] == 'a' && 
		 buf[2] == 'y' ) {
		
//		DIReport("Compressed:%08x\n",buf );
		return ( (buf[4]<<24) |
				 (buf[5]<<16) |
				 (buf[6]<<8)  |
				 buf[7] );
	}

	return 0;
}
static void
resolvCompressed( void* buf )
{
	int file_max_num, i, size;
	int*	tBuf = buf;
	void*	tExpandBuf;

	file_max_num = *tBuf;
	tBuf++;

	for ( i = 0 ; i < file_max_num ; i++ ) {
		if ( ( size = checkCompressed( (u8*)(sFileBuf + tBuf[i]) ) ) != 0 ) {
			tExpandBuf = gAlloc( OSRoundUp32B( (u32)size ) );
			decode( (u8*)(sFileBuf + tBuf[i]), tExpandBuf );
			tBuf[i] = (int)tExpandBuf - (int)sFileBuf;
			DCStoreRange( tExpandBuf, (u32)size );
		}
	}
}

void
gLoadBinary( void )
{
#ifndef IPL
	DVDFileInfo	fileInfo;
	
	DVDInit();
	switch ( sCountry ) {
	case COUNTRY_JAPAN:
	case COUNTRY_US:
		DVDOpen("gcntsc.bin", &fileInfo );
		break;
	case COUNTRY_BRAZIL:
		DVDOpen("gcmpal.bin", &fileInfo );
		break;
	default:
		DVDOpen("gcpal.bin", &fileInfo );
		break;
	}
	sFileBuf = gAlloc( OSRoundUp32B( DVDGetLength(&fileInfo)) );
	DVDRead(&fileInfo, sFileBuf, (s32)OSRoundUp32B(DVDGetLength(&fileInfo)), 0);
	DVDClose( &fileInfo );
#else
#if defined(MPAL)
	sFileBuf = (char*)gcarchive_mpalImage;
#elif defined(PAL)
	sFileBuf = (char*)gcarchive_palImage;
#else // NTSC
	sFileBuf = (char*)gcarchive_ntscImage;
#endif
#endif

	resolvCompressed( sFileBuf );
}

void*
gGetBufferPtr( int id )
{
	int*	tBuf = (int*)sFileBuf;
	tBuf++;	// 0には、ファイル数が入っているため。

	return (void*)( sFileBuf + tBuf[id] );
}
/*
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<sys/time.h>
void decode(unsigned char *,unsigned char *);

void main(int argc,char *argv[])
{
	int j,osize;
struct timeval tv;
double x,y;
	char fn[64],fc[64];
	unsigned char *bz,*dt;
	FILE *fp;
	if(argc<2){
	    printf("slidec <FILE>\n");exit(1);
	}
	strcpy(fn,argv[1]);
	for(j=0;fn[j];j++)if(fn[j]=='.')break;
	if((fp=fopen(fn,"rb"))==0){
	    printf("FILE READ ERROR! [%s]\n",fn);exit(1);
	}
	fn[j]=0;
	fseek(fp,0L,2);j=(int)ftell(fp);fseek(fp,0L,0);
	dt=(unsigned char *)malloc(j);
	fread(dt,1,j,fp);fclose(fp);
printf("%X %X %X\n",dt[0],dt[1],dt[2]);
	if(dt[0]=='Y' && dt[1]=='a' && dt[2]=='y'){
		osize=(dt[4]<<24)|(dt[5]<<16)|(dt[6]<<8)|dt[7];
		bz=(unsigned char *)malloc(osize);
printf("START!");fflush(stdout);
(void)gettimeofday(&tv,(struct timezone *)NULL);
x=tv.tv_sec+tv.tv_usec/1000000.0;
		decode(dt,bz);
(void)gettimeofday(&tv,(struct timezone *)NULL);
y=tv.tv_sec+tv.tv_usec/1000000.0;
printf("END!");
printf("decode time=[%f]\n",y-x);fflush(stdout);
		strcpy(fc,fn);strcat(fc,".out");
		if((fp=fopen(fc,"wb"))==0){
		    printf("FILE READ ERROR! [%s]\n",fc);exit(1);
		}
		fwrite(bz,1,osize,fp);
		fclose(fp);
		free(bz);
	}
	free(dt);
}

void decode(unsigned char *s,unsigned char *d)
{
	int i,j,k,p,q,r7,r25,cnt,os;
	unsigned flag,code;
	os=(s[4]<<24)|(s[5]<<16)|(s[6]<<8)|s[7];
	r7=(s[8]<<24)|(s[9]<<16)|(s[10]<<8)|s[11];
	r25=(s[12]<<24)|(s[13]<<16)|(s[14]<<8)|s[15];
	q=0;flag=0;p=16;
	do{
		if(flag==0){
			code=(s[p]<<24)|(s[p+1]<<16)|(s[p+2]<<8)|s[p+3];p+=4;
			flag=32;
		}
		if(code&0x80000000)d[q++]=s[r25++];
		else{
			j=(s[r7]<<8)|s[r7+1];r7+=2;
			k=q-(j&0xFFF);cnt=j>>12;
			if(cnt==0)cnt=s[r25++]+18;
			else cnt+=2;
			for(i=0;i<cnt;i++,q++,k++)d[q]=d[k-1];
		}
		code<<=1;flag--;
	}while(q<os);
}
*/
