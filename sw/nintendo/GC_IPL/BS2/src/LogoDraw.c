/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: ロゴを表示する部分のスプラッシュ
  
  GAMECUBEのロゴを表示するのに特化した管理
  描画関連
  -----------------------------------------------------------------------------*/
#include <dolphin.h>

#include "gTrans.h"
#include "gCamera.h"
#include "gBase.h"
#include "gInit.h"
#include "gLoader.h"

#include "gModelRender.h"

#include "LogoUpdate.h"
#include "LogoDraw.h"

#include "SplashMain.h"
#include "SplashUpdate.h"

/*-----------------------------------------------------------------------------
  staticモノ
  -----------------------------------------------------------------------------*/

static gmrModelData smallCubeData;
static gmrModel		smallCube;

static void*		locusTexture;

/*-----------------------------------------------------------------------------
  static関数
  -----------------------------------------------------------------------------*/
static void lgSetGridGx( void );
static void lgDrawShadow( u8 alpha );
static void lgFaceToPos( s16 x, s16 z, s32 face );
static void lgDrawShadowQuad( s32 x , s32 z , s32 face , u8 alpha );

/*-----------------------------------------------------------------------------
  grid用のgxを設定
  -----------------------------------------------------------------------------*/
static void
lgSetGridGx( void )
{
	gSetGXforDirect( TRUE, FALSE, FALSE );
	
	GXSetVtxAttrFmt(GX_VTXFMT1, GX_VA_TEX0, GX_TEX_ST, GX_S16 , 13);
	GXSetCullMode( GX_CULL_NONE );
}

/*-----------------------------------------------------------------------------
  軌跡を書く
  -----------------------------------------------------------------------------*/
static void
lgDrawShadow( u8 base_alpha )
{
	s32 face , x, z;
	Mtx nm , load;
	int	alpha;
	LogoState*	csP = lgGetCubeState();
	lgGridArrayPtr	sga = lgGetGridState();

	gGetTranslateRotateMtx( 0,0,0, cube_size*3/2,cube_size*3/2,cube_size*3/2, load );
	MTXConcat ( inSplashGetMtxArray() , load , load);
	MTXConcat ( getCurrentCamera() , load , load );
	gGetInverseTranspose( load , nm );
	GXLoadPosMtxImm( load  , GX_PNMTX0 );
	GXLoadNrmMtxImm( nm    , GX_PNMTX0 );
	GXSetCurrentMtx( GX_PNMTX0 );

	for ( face = 0 ; face < 3 ; face ++ ) {
		for ( x = 0 ; x < LG_GRID_MAX_X ; x++ ) {
			for ( z = 0 ; z < LG_GRID_MAX_Z ; z++ ) {
				if ( sga[face][x][z].color != 0 ) { // & 0xf0 ) {
					alpha = 0xff;
					if ( sga[face][x][z].state == 1 ) { // fadein
						alpha = 0xff * csP->nowFrame / csP->animFrame;
					}
					if ( sga[face][x][z].state == 2 ) { // fadeout
						alpha = 0xff * (csP->animFrame - csP->nowFrame) / csP->animFrame;
					}
					if ( alpha > 0xff ) alpha = 0xff;
					if ( alpha < 0 ) alpha = 0;
					lgDrawShadowQuad( x , z, face ,/* (u8)(sga[face][x][z].color - 1),*/ (u8)( alpha * base_alpha / 255 ) );
				}
			}
		}
	}
}

/*-----------------------------------------------------------------------------
  面を選択して、GXコマンドに変換
  -----------------------------------------------------------------------------*/
static void
lgFaceToPos( s16 x, s16 z, s32 face )
{
	switch ( face )
	{
	case 0: GXPosition3s16( (s16)(-1*x), 0         , (s16)(-1*z) ); break;
	case 1: GXPosition3s16( (s16)(-1*z),(s16)(-1*x), 0           ); break;
	case 2: GXPosition3s16(           0,(s16)(-1*z), (s16)(-1*x) ); break;
	}
}

/*-----------------------------------------------------------------------------
  軌跡の四角をalpha付で書く。
  -----------------------------------------------------------------------------*/
static void
lgDrawShadowQuad( s32 x , s32 z , s32 face , u8 alpha )
{
	LogoState*	csP = lgGetCubeState();
	int gridMergin  = csP->gridMergin;
	lgGridArrayPtr	sga = lgGetGridState();
	s32 dx,dz;
	s32 shift_cs = cube_size << 4;
	u8	face_idx;
	GXColor color;

	dx = ( ( x - LG_GRID_CENTER_X ) * cube_size ) << 4;
	dz = ( ( z - LG_GRID_CENTER_Z ) * cube_size ) << 4;

	color = *(GXColor*)(&sga[face][x][z].real_color);
	color.r= csP->grid_r;
	color.g= csP->grid_g;
	color.b= csP->grid_b;
	color.a = alpha;
	GXSetChanMatColor( GX_COLOR0A0 , color );

	switch ( face ) {
	case 0: face_idx = 2; break;
	case 1: face_idx = 4; break;
	case 2: face_idx = 0; break;
	}

	loadTexture( GX_TEXMAP0 , locusTexture );

	GXBegin(GX_QUADS, GX_VTXFMT1, 4);
	{
		lgFaceToPos( (s16)(dx + gridMergin), (s16)(dz + gridMergin), face );
		GXTexCoord2u16( 0, 0 );

		lgFaceToPos( (s16)(dx + shift_cs ), (s16)(dz + gridMergin), face );
		GXTexCoord2u16( 0, 2 << 13 );
		
		lgFaceToPos( (s16)(dx + shift_cs ), (s16)(dz + shift_cs ), face );
		GXTexCoord2u16( 2 << 13, 2 << 13 );
		
		lgFaceToPos( (s16)(dx + gridMergin), (s16)(dz + shift_cs ), face );
		GXTexCoord2u16( 2 << 13, 0 );
	}
	GXEnd();
}

/*-----------------------------------------------------------------------------
  描画処理
  -----------------------------------------------------------------------------*/
void
lgDraw( u8 alpha )
{
	Mtx drawMtx;
	LogoState*	csP = lgGetCubeState();

	MTXConcat( getCurrentCamera() , lgGetSmallCubeMtx() , drawMtx );

	lgSetGridGx();
	lgDrawShadow( alpha );
	
	smallCube.modelAlpha = (s16)( csP->cubeAlpha * alpha / 255 );
	drawModel( &smallCube );
}

/*-----------------------------------------------------------------------------
  描画初期化
  -----------------------------------------------------------------------------*/
void
lgDrawInit( void )
{
	locusTexture = gGetBufferPtr( GC_locus_szp );

	smallCubeData.modeldata = gGetBufferPtr( GC_s_cube_szp );
//	DIReport("---------------%08x\n", smallCubeData.modeldata );
	smallCube.gmrmd = &smallCubeData;
	smallCube.modelAlpha = 0xff;
	buildModel( &smallCube , TRUE );
}

/*-----------------------------------------------------------------------------
  モデルインスタンスを返す。
  -----------------------------------------------------------------------------*/
gmrModel*
lgGetSmallCubeModel( void )
{
	return &smallCube;
}
