/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: メニューモード時の各面のパラメータ
  -----------------------------------------------------------------------------*/
#include <dolphin.h>
#include <math.h>
#include <string.h>
#include <private/OSRtc.h>
#include <private/viprivate.h>

#include "gCont.h"
#include "gBase.h"
#include "gFont.h"
#include "gCamera.h"
#include "gInit.h"
#include "gTrans.h"
#include "gLoader.h"

#include "gLayoutRender.h"
#include "gModelRender.h"

#include "MenuUpdate.h"	// メインメニューの動作を決定するEnumが入っている。
#include "MenuOptionAnim.h"
#include "OptionState.h"
#include "OptionUpdate.h"
#include "gMainState.h"
#include "SplashMain.h"

#ifdef PAL
#include "CardUpdate.h"
#include "MenuUpdate.h"
#include "SplashDraw.h"
#include "StartUpdate.h"
#include "TimeUpdate.h"
#endif

#if defined(MPAL)
#include "mpal_mesg.h"
#elif defined(PAL)
#include "pal_mesg.h"
#else // NTSC
#include "ntsc_mesg.h"
#endif
//#include "2d_option_2_j_Japanese.h"
#include "jaudio.h"

// start nakamura's edit
#include "smallCubeFader.h"
#include "smallCubeDraw.h"
// end

#define PAL_ACTIVE_CHANGE

extern int	snprintf(char* s, size_t n, const char * format,...);

/*-----------------------------------------------------------------------------
  コントローラの入力を外部参照する。
  -----------------------------------------------------------------------------*/
extern DIPadStatus	DIPad;

static gLayoutHeader*	opt_lay;
static gLRStringHeader*	opt_str[ COUNTRY_PAL_MAX_LANGUAGE ];

static u32	option_runstate;
static u32	pre_option_runstate;
static s16	select_curstate;
static s16	offset_curstate;
static s16	sound_curstate;
static s16	language_curstate;


static s16	win_alpha;
static s8	a_bounds;

#ifdef PAL
static gLRFade	lang[ COUNTRY_PAL_MAX_LANGUAGE ];
static gLRFade	lang_str[ COUNTRY_PAL_MAX_LANGUAGE ];
static s16		lang_pos_x[ COUNTRY_PAL_MAX_LANGUAGE ];
#endif

/*-----------------------------------------------------------------------------
  static function
  -----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
  初期化
  -----------------------------------------------------------------------------*/
#ifdef PAL
static void
resolvLanguageTexturePos( gLayoutHeader* header )
{
	u32	tag;
	int	i, j;
	gLRPictureData*	pdp = (gLRPictureData*)( (u8*)header + header->picture_offset );

	tag = 'lan1';
	for ( i = 0 ; i < COUNTRY_PAL_MAX_LANGUAGE ; i++ ) {
		for ( j = 0 ; j < header->picture_num ; j++ ) {
			if ( pdp[j].tag == tag + i ) {
				lang_pos_x[i] = (s16)( pdp[j].x >> 4 );
//				OSReport("lang - %d: %d\n", i, lang_pos_x[i]);
			}
		}
	}
}
#endif

void
initOptionMenu_Lang( void )
{
#ifdef PAL
	language_curstate = gGetPalCountry();
#endif
}

void
initOptionMenu( BOOL first )
{
	OSSram      *sram;
#ifdef PAL
	int			i;
#endif

	if ( first ) {
#if defined(PAL)
		int	country;
		opt_lay = gGetBufferPtr( getID_2D_option_layout() );
		for ( country = 0 ; country < COUNTRY_PAL_MAX_LANGUAGE ; country++ ) {
			opt_str[ country ] = gGetBufferPtr( getID_2D_option_string_pal( country ) );

		}

		resolvLanguageTexturePos( opt_lay );
#else
		opt_lay = gGetBufferPtr( getID_2D_option_layout() );
		opt_str[0] = gGetBufferPtr( getID_2D_option_string() );
#endif
	}

	option_runstate = IPL_OPTION_SELECTING;
	pre_option_runstate = option_runstate;

	select_curstate = 0;
	sound_curstate = (s16)OSGetSoundMode();

#ifdef PAL
	for ( i = 0 ; i < COUNTRY_PAL_MAX_LANGUAGE ; i++ ) {
		glrInitFadeState( &lang[ i ] );
		glrInitFadeState( &lang_str[ i ] );
	}
#endif

	sram = __OSLockSram();
	offset_curstate = sram->displayOffsetH;
	__OSUnlockSram(FALSE);

	initOptionMenu_Lang();

	win_alpha = 100;
	a_bounds = 2;
}

/*-----------------------------------------------------------------------------
  コントローラの入力から更新する
  -----------------------------------------------------------------------------*/
static s32
updateOptionMenuControl( void )
{
	s32	ret = IPL_MENU_OPTION_SELECT;

	if ( !isCubeAnimFinished( 0 ) ) return ret;

	if ( DIPad.dirsPressed & DI_STICK_UP ) {	// 上
		if ( select_curstate > 0 ) {
			Jac_PlaySe( JAC_SE_UD_MOVE );
			select_curstate--;
		}
	}
	if ( DIPad.dirsPressed & DI_STICK_DOWN ) {	// 下
#ifdef PAL
		if ( select_curstate < 2 ) {
#else
		if ( select_curstate < 1 ) {
#endif
			Jac_PlaySe( JAC_SE_UD_MOVE );
			select_curstate++;
		}
	}

	if ( DIPad.buttonDown & PAD_BUTTON_A ) {
		Jac_PlaySe( JAC_SE_DECIDE );
		switch ( select_curstate )
		{
		case 0:
			sound_curstate = (s16)OSGetSoundMode();
			pre_option_runstate = IPL_OPTION_SOUND;
			break;
		case 1:
			pre_option_runstate = IPL_OPTION_OFFSET;
			{
				OSSram      *sram;
				
				sram = __OSLockSram();
				offset_curstate = sram->displayOffsetH;
				__OSUnlockSram(FALSE);
			}
			break;
#ifdef PAL
		case 2:
			pre_option_runstate = IPL_OPTION_LANGUAGE;
			language_curstate = gGetPalCountry();
			break;
#endif
		}
	}
	if ( DIPad.buttonDown & PAD_BUTTON_B ) {
		MenuState*	mesp = getMenuState();
		if ( mesp->fadeout_z_param == 32767 || 
			 mesp->enable_pre_key_input ) {
			Jac_PlaySe( JAC_SE_TO_3D );
			ret = IPL_MENU_OPTION;
		}
	}

	return ret;
}

/*-----------------------------------------------------------------------------
  サウンドメニュー時
  -----------------------------------------------------------------------------*/
static s32
updateOptionSoundControl( void )
{
	s32	ret = IPL_MENU_OPTION_SELECT;

	if ( DIPad.dirsPressed & DI_STICK_LEFT ) {
		if ( sound_curstate != 0 ) Jac_PlaySe( JAC_SE_VAL_MOVE );
		else if ( !gIsPadRepeating() ) Jac_PlaySe( JAC_SE_VAL_LIMIT );
		sound_curstate = 0;
	}

	if ( DIPad.dirsPressed & DI_STICK_RIGHT ) {
		if ( sound_curstate != 1 ) Jac_PlaySe( JAC_SE_VAL_MOVE );
		else if ( !gIsPadRepeating() ) Jac_PlaySe( JAC_SE_VAL_LIMIT );
		sound_curstate = 1;
	}
	
	if ( DIPad.buttonDown & PAD_BUTTON_A ) {
		if ( sound_curstate != OSGetSoundMode() ) {
			OSSetSoundMode( sound_curstate );
			Jac_OutputMode( sound_curstate );
		}
		Jac_PlaySe( JAC_SE_FINALDECIDE );
		pre_option_runstate = IPL_OPTION_SELECTING;
	}
	if ( DIPad.buttonDown & PAD_BUTTON_B ) {
		Jac_PlaySe( JAC_SE_CANCEL );
		sound_curstate = (s16)OSGetSoundMode();
		pre_option_runstate = IPL_OPTION_SELECTING;
	}

	return ret;
}

/*-----------------------------------------------------------------------------
  画面横位置メニュー時
  -----------------------------------------------------------------------------*/
static s32
updateOptionOffsetControl( void )
{
	s32	ret = IPL_MENU_OPTION_SELECT;
	OSSram      *sram;

	if ( DIPad.dirsPressed & DI_STICK_LEFT ) {	// 左
		offset_curstate--;
		if ( offset_curstate < -32 ) {
			offset_curstate = -32;
			if ( !gIsPadRepeating() ) Jac_PlaySe( JAC_SE_VAL_LIMIT );
		} else {
			Jac_PlaySe( JAC_SE_VAL_MOVE );
		}
		__VISetAdjustingValues( offset_curstate, 0 );
	}
	if ( DIPad.dirsPressed & DI_STICK_RIGHT ) {	// 右
		offset_curstate++;
		if ( offset_curstate > 32 ) {
			offset_curstate = 32;
			if ( !gIsPadRepeating() ) Jac_PlaySe( JAC_SE_VAL_LIMIT );
		} else {
			Jac_PlaySe( JAC_SE_VAL_MOVE );
		}
		__VISetAdjustingValues( offset_curstate, 0 );
	}
	
	if ( DIPad.buttonDown & PAD_BUTTON_A ) {
		Jac_PlaySe( JAC_SE_FINALDECIDE );
		sram = __OSLockSram();
		sram->displayOffsetH = (s8)offset_curstate;
		__OSUnlockSram(TRUE);
		while (!__OSSyncSram()) {}
		pre_option_runstate = IPL_OPTION_SELECTING;
	}
	if ( DIPad.buttonDown & PAD_BUTTON_B ) {
		Jac_PlaySe( JAC_SE_CANCEL );
		sram = __OSLockSram();
		offset_curstate = sram->displayOffsetH;
		__OSUnlockSram(FALSE);
		__VISetAdjustingValues( offset_curstate, 0 );
		
		pre_option_runstate = IPL_OPTION_SELECTING;
	}

	return ret;
}

#ifdef PAL
static s32
updateOptionLanguageControl( void )
{
	s32	ret = IPL_MENU_OPTION_SELECT;

	if ( DIPad.dirsPressed & DI_STICK_LEFT ) {	// 左
		language_curstate--;
		if ( language_curstate < 0 ) {
			language_curstate = 0;
			if ( !gIsPadRepeating() ) Jac_PlaySe( JAC_SE_VAL_LIMIT );
		} else {
			Jac_PlaySe( JAC_SE_VAL_MOVE );
		}
#ifdef	PAL_ACTIVE_CHANGE
		setOperationLanguage( language_curstate );
#endif
	}
	if ( DIPad.dirsPressed & DI_STICK_RIGHT ) {	// 右
		language_curstate++;
		if ( language_curstate > 5 ) {
			language_curstate = 5;
			if ( !gIsPadRepeating() ) Jac_PlaySe( JAC_SE_VAL_LIMIT );
		} else {
			Jac_PlaySe( JAC_SE_VAL_MOVE );
		}
#ifdef	PAL_ACTIVE_CHANGE
		setOperationLanguage( language_curstate );
#endif
	}

	if ( DIPad.buttonDown & PAD_BUTTON_A ) {
		Jac_PlaySe( JAC_SE_FINALDECIDE );

		gSetPalCountry( language_curstate );
		// update string file and layout file;
		/*
		initCardMenu_Lang();
		initMenuDraw_Lang();
		initSplashDraw_Lang();
		initStartMenu_Lang();
		initTimeMenu_Lang();
		setOperationLanguage( language_curstate );
		  */
		
		pre_option_runstate = IPL_OPTION_SELECTING;
	}
	if ( DIPad.buttonDown & PAD_BUTTON_B ) {
		Jac_PlaySe( JAC_SE_CANCEL );

		language_curstate = gGetPalCountry();
		setOperationLanguage( language_curstate );
		pre_option_runstate = IPL_OPTION_SELECTING;
	}

	return ret;
}
#endif

static void
updateOptionDraw( void )
{
#ifdef PAL
	int i;

	for ( i = 0 ; i < COUNTRY_PAL_MAX_LANGUAGE ; i++ ) {
		u8 fade =  ( i == language_curstate ) ? IPL_WINDOW_FADEIN : IPL_WINDOW_FADEHALF;
		glrUpdateFade( &lang[ i ] , fade );

#ifdef	PAL_ACTIVE_CHANGE
		fade =  ( i == language_curstate ) ? IPL_WINDOW_FADEIN : IPL_WINDOW_FADEOUT;
#else
		fade =  ( i == gGetPalCountry() ) ? IPL_WINDOW_FADEIN : IPL_WINDOW_FADEOUT;
#endif
		glrUpdateFade( &lang_str[ i ] , fade );
	}
#endif
}

s32
updateOptionMenu( void )
{
	s32	ret = IPL_MENU_OPTION_SELECT;
	SplashState* ssp = inSplashGetState();

	option_runstate = pre_option_runstate;

	if ( !ssp->force_fatalerror ) {
		switch ( option_runstate )
		{
		case IPL_OPTION_SELECTING:
			ret = updateOptionMenuControl();
			break;
		case IPL_OPTION_SOUND:
			ret = updateOptionSoundControl();
			break;
		case IPL_OPTION_OFFSET:
			ret = updateOptionOffsetControl();
			break;
#if PAL
		case IPL_OPTION_LANGUAGE:
			ret = updateOptionLanguageControl();
			break;
#endif
		}
	}
	
	if ( isConcentOCSmallCube() ) {
	// start nakamura's edit
	updateOptionSmallCube( option_runstate,
				select_curstate,
				offset_curstate,
				sound_curstate );
	updateSmallCubeDraw();
	// end
	}
	updateOptionDraw();

	win_alpha += a_bounds;
	if ( win_alpha > 120 ) a_bounds = -2;
	if ( win_alpha <  50 ) a_bounds =  2;
	
	return ret;
}

s32
getOptionRunstate( void )
{
	if ( option_runstate == IPL_OPTION_SELECTING ) {
		return TRUE;
	}

	return FALSE;
}

s16
getOptionLanguagePosX_lang( s16 language )
{
#ifdef PAL
	return lang_pos_x[ language ];
#else
#pragma unused ( language )
	return 0;
#endif
}

s16
getOptionLanguagePosX( void )
{
	return getOptionLanguagePosX_lang( language_curstate );
}

s16
getLanguageCurstate(void)
{
	return language_curstate;
}


































/*-----------------------------------------------------------------------------
  draw section
  -----------------------------------------------------------------------------*/
static void
drawLayout( u8 alpha )
{
	OptionState*	osp = getOptionState();
	GXColor white = { 255, 255, 255, 255 };
	GXColor bar1;
#ifdef PAL
	int		i;
#endif

	bar1.r = (u8)osp->moji_r;
	bar1.g = (u8)osp->moji_g;
	bar1.b = (u8)osp->moji_b;
	
	setGXforglrText();

#ifdef PAL
	for ( i = 0 ; i < COUNTRY_PAL_MAX_LANGUAGE ; i ++ ) {
		s16	lang_alpha;
		glrGetOffsetAlpha( &lang_str[i], &lang_alpha, NULL );
		bar1.a = (u8)( alpha * lang_alpha / 255 );
		white.a = (u8)( alpha * lang_alpha / 255 );
		glrDrawString( MSG_O000, opt_str[i], opt_lay, &white );
		glrDrawStringOffset( MSG_O001, opt_str[i], opt_lay, &bar1  , 0, (s16)(osp->sound_pos_y * 16) );
		glrDrawStringOffset( MSG_O004, opt_str[i], opt_lay, &bar1  , 0, (s16)(osp->yoko_pos_y * 16) );
		glrDrawStringOffset( MSG_O005, opt_str[i], opt_lay, &bar1 , 0, (s16)(osp->lang_y * 16) );
	}
#else
	bar1.a = alpha;
	white.a = alpha;
	glrDrawString( MSG_O000, opt_str[0], opt_lay, &white );
	glrDrawString( MSG_O001, opt_str[0], opt_lay, &bar1 );
	glrDrawString( MSG_O004, opt_str[0], opt_lay, &bar1 );
#endif

#ifdef PAL

	setGXforglrPicture();

	for ( i = 0 ; i < COUNTRY_PAL_MAX_LANGUAGE ; i ++ ) {
		u32	tag = 'lan1';
		s16	lang_alpha;
		glrGetOffsetAlpha( &lang[ i ] , &lang_alpha, NULL );
		white.a = (u8)( alpha * lang_alpha / 255 );
		glrDrawPictureOffset( (u32)( tag + i ), opt_lay, &white , 0, (s16)(osp->lang_sel_y * 16) );
	}

	white.a = (u8)( alpha * 128 / 255 );
	glrDrawPictureOffset( 'sla1', opt_lay, &white  , 0, (s16)(osp->lang_sel_y * 16) );
	glrDrawPictureOffset( 'sla2', opt_lay, &white  , 0, (s16)(osp->lang_sel_y * 16) );
	glrDrawPictureOffset( 'sla3', opt_lay, &white  , 0, (s16)(osp->lang_sel_y * 16) );
	glrDrawPictureOffset( 'sla4', opt_lay, &white  , 0, (s16)(osp->lang_sel_y * 16) );
	glrDrawPictureOffset( 'sla5', opt_lay, &white  , 0, (s16)(osp->lang_sel_y * 16) );
#endif
}

/*-----------------------------------------------------------------------------
  アイコン選択時の描画。
  -----------------------------------------------------------------------------*/
void
drawOptionMenu( u8 all_alpha , u8 grid_alpha, u8 layout_alpha )
{
	MainState* msp  = getMainState();
	J3DTransformInfo ti = { 1.f, -1.f, 1.f, 0,0,0,0, -292, 224, 0 };
	Vec     up      = {   0.f,   1.f,   0.f };
	Vec     camLoc	= {   0.f,   0.f,   0.f };
	Vec		objPt	= {   0.f,   0.f,   0.f };
	Mtx		m, cm, inv;

	camLoc.z = 224.0f / tanf( msp->fovy * 3.141592f / 360.f ); // ( fovy / 2 )  *  (3.14 / 180)
    MTXLookAt( cm, &camLoc, &up, &objPt);
	gGetTransformMtx( &ti , m );
	MTXConcat( cm, m , m );
	gGetInverseTranspose( m, inv );
	GXLoadPosMtxImm( m , GX_PNMTX0 );
	GXLoadNrmMtxImm( inv , GX_PNMTX0 );
	GXSetCurrentMtx( GX_PNMTX0 );
	
	glrDraw2dGrid( m, (u8)grid_alpha );
	
	drawLayout( (u8)layout_alpha );

	if ( isConcentOCSmallCube() ) {
	// start nakamura's edit
	drawSmallCubeDraw( (u8)all_alpha );
	// end
	}
}
