/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: メモリーカードの実際の処理
  -----------------------------------------------------------------------------*/
#include <dolphin.h>
#include <private/CARDDir.h>
#include <private/OSLoMem.h>
#include <string.h>
#include <stdio.h>

#include "gBase.h"

#include "CardState.h"
#include "CardSequence.h"

/*-----------------------------------------------------------------------------
  static Values
  -----------------------------------------------------------------------------*/
static SlotState		slot[ 2 ]; // Cardスロットの状態
static DirStateArray	card;
static void*			sCardWorkArea[ 2 ];	// カード処理用のワークエリア
static BOOL				sMounted[ 2 ];

static OSMessageQueue	sCardSendMesQueue;
static OSMessageQueue	sCardRecvMesQueue;
static OSMessage		sCardSendMesArray[ IPL_CARD_MES_SIZE ];
static OSMessage		sCardRecvMesArray[ IPL_CARD_MES_SIZE ];

static OSThread			CardThread;
static u8				CardThreadStack[32*1024];

static char				sTempFilename[32];	// Copy/Moveで使用するテンポラリファイル名。
static CARDFileInfo		sSrcFileInfo;
static CARDFileInfo		sDstFileInfo;
static void*			sTempBuffer;

static IconStateArray	icon;
static char*			sIconBuffer[ 2 ][ CARD_MAX_FILE ];
static char*			sIconComment[ 2 ][ CARD_MAX_FILE ];
static u8				temp_buf[ 1024 ] ATTRIBUTE_ALIGN(32);
static u8*				sIconReadBuf;

static u8				sLastSrcSlot;
static u8				sCardThreadValid;
static u8				sLastCmdFileNo;
static s32				sLastSendCmd;
static s32				sLastCmdResult;
static s32				sLastCMDFCmdResult;
static BOOL				sGetCodeCmd;
static BOOL				sNotChanged;
static vu32				sWriting;
/*-----------------------------------------------------------------------------
  static functions
  -----------------------------------------------------------------------------*/
static void		probeCardSub( u8 no );
static BOOL		sendCardInsertCmd( u8 no );
static BOOL		sendCardDetachCmd( u8 no );

static void*	cardMain( void* );
static void		sendValidState( u8 cmd, u8 valid );
static void		sendDoneMes( u8 no , s32 cmd , u8 fileNo );
static BOOL		processMountError( u8 no, s32 ret );
static BOOL		processError( u8 no, s32 cmd, s32 ret );
static BOOL		checkBrokenFile( CARDDir* dir );
static void		initCardArea( u8 no );
static void		mountCard( u8 no );
static void		unmountCard( u8 no );
static s32		getFileIcons  ( u8 no, s16 fileNo, CARDDir* dir, CARDFileInfo* );
static s32		getFileComment( u8 no, s16 fileNo, CARDDir* dir, CARDFileInfo* );
static s32		getFileInfo   ( u8 no, s16 fileNo, CARDDir* dir );
static s16		createDummyFile( u8 no , u32 fileSize );
static s32		copyFileData( u8 no , s16 fileNo );
static s32		checkSamefileExist( u8 no , s16 fileNo );
static void		updateSamefileFlag( void );

static s32		renameForCopy( u8 no , s16 srcFileNo , s16 dstFileNo , BOOL *dummyExist );
static s32		renameForMove( u8 no , s16 srcFileNo , s16 dstFileNo , BOOL *dummyExist );

static void		copymoveFile( u8 no , s16 fileNo, s32 cmd );
static void		formatCard( u8 no );
static void		deleteFile( u8 no , s16	fileNo );
static void		getGameCodeFileBlocks( void );

/*-----------------------------------------------------------------------------
  現在のステータスの取得。
  -----------------------------------------------------------------------------*/
DirStateArrayPtr
getCardDirState( void )
{
	return card;
}
SlotState*
getCardSlotState( void )
{
	return slot;
}
u8
getCardThreadState( void )
{
	return sCardThreadValid;
}

s32
getCardLastSrcSlot( void )
{
	return sLastSrcSlot;
}
s32
getCardLastSendCmd( void )
{
	return sLastSendCmd;
}
s32
getCardLastCmdResult( void )
{
	return sLastCmdResult;
}
s32
getCardLastCMDFCmdResult( void )
{
	return sLastCMDFCmdResult;
}
u8
getCardLastCmdFileNo( void )
{
    return sLastCmdFileNo;
}

/*-----------------------------------------------------------------------------
  カードが挿入されたときのメッセージ
  -----------------------------------------------------------------------------*/
static BOOL
sendCardInsertCmd( u8 no )
{
	u32	send = createCmdMes( no , IPL_CARD_CMD_MOUNT );
	
	sLastSrcSlot   = no;
	sLastSendCmd   = IPL_CARD_CMD_MOUNT;
	sLastCmdResult = IPL_CARD_RESULT_BUSY;

	return OSSendMessage( getCardSendMesQueue(), (OSMessage)send, OS_MESSAGE_NOBLOCK );
}

/*-----------------------------------------------------------------------------
  カードが取り出されたときのメッセージ
  -----------------------------------------------------------------------------*/
static BOOL
sendCardDetachCmd( u8 no )
{
	u32	send = createCmdMes( no , IPL_CARD_CMD_UNMOUNT );
	
	sLastSrcSlot   = no;
	sLastSendCmd   = IPL_CARD_CMD_UNMOUNT;
	sLastCmdResult = IPL_CARD_RESULT_BUSY;

	return OSSendMessage( getCardSendMesQueue(), (OSMessage)send, OS_MESSAGE_NOBLOCK );
}

/*-----------------------------------------------------------------------------
  フォーマットコマンドの送信
  -----------------------------------------------------------------------------*/
BOOL
sendCardFormatCmd( u8 no )
{
	u32	send = createCmdMes( no , IPL_CARD_CMD_FORMAT );

	sLastSrcSlot   = no;
	sLastSendCmd   = IPL_CARD_CMD_FORMAT;
	sLastCmdResult = IPL_CARD_RESULT_BUSY;
	sLastCMDFCmdResult = IPL_CARD_RESULT_BUSY;

	return OSSendMessage( getCardSendMesQueue(), (OSMessage)send, OS_MESSAGE_NOBLOCK );
}

/*-----------------------------------------------------------------------------
  Copyコマンドの送信
  -----------------------------------------------------------------------------*/
BOOL
sendCardCopyCmd( u8 no , s16 fileNo)
{
	u32	send = createCmdMesFileNo( no , IPL_CARD_CMD_FILECOPY , fileNo );

	sLastSrcSlot   = no;
	sLastSendCmd   = IPL_CARD_CMD_FILECOPY;
	sLastCmdResult = IPL_CARD_RESULT_BUSY;
	sLastCMDFCmdResult = IPL_CARD_RESULT_BUSY;

	return OSSendMessage( getCardSendMesQueue(), (OSMessage)send, OS_MESSAGE_NOBLOCK );
}

/*-----------------------------------------------------------------------------
  Moveコマンドの送信
  -----------------------------------------------------------------------------*/
BOOL
sendCardMoveCmd( u8 no , s16 fileNo )
{
	u32	send = createCmdMesFileNo( no , IPL_CARD_CMD_FILEMOVE , fileNo );

	sLastSrcSlot   = no;
	sLastSendCmd   = IPL_CARD_CMD_FILEMOVE;
	sLastCmdResult = IPL_CARD_RESULT_BUSY;
	sLastCMDFCmdResult = IPL_CARD_RESULT_BUSY;

	return OSSendMessage( getCardSendMesQueue(), (OSMessage)send, OS_MESSAGE_NOBLOCK );
}

/*-----------------------------------------------------------------------------
  Deleteコマンドの送信
  -----------------------------------------------------------------------------*/
BOOL
sendCardDeleteCmd( u8 no , s16 fileNo )
{
	u32	send = createCmdMesFileNo( no , IPL_CARD_CMD_FILEDELETE , fileNo );

	sLastSrcSlot   = no;
	sLastSendCmd   = IPL_CARD_CMD_FILEDELETE;
	sLastCmdResult = IPL_CARD_RESULT_BUSY;
	sLastCMDFCmdResult = IPL_CARD_RESULT_BUSY;

	return OSSendMessage( getCardSendMesQueue(), (OSMessage)send, OS_MESSAGE_NOBLOCK );
}

/*-----------------------------------------------------------------------------
  Gamecodeファイルブロック取得コマンドの送信
  -----------------------------------------------------------------------------*/
BOOL
sendCardGetGameCodeBlocks( void )
{
	u32	send = createCmdMesFileNo( 0 , IPL_CARD_CMD_GETCODESIZE , 0 );

	sLastSendCmd = IPL_CARD_CMD_GETCODESIZE;
	sGetCodeCmd = FALSE;
	sNotChanged = FALSE;

	return OSSendMessage( getCardSendMesQueue(), (OSMessage)send, OS_MESSAGE_NOBLOCK );
}

/*-----------------------------------------------------------------------------
  終了ステートの取得コマンド
  -----------------------------------------------------------------------------*/
BOOL
isDoneGameCodeBlocks( void )
{
	return sGetCodeCmd;
}
BOOL
isStateNotChanged( void )
{
	return sNotChanged;
}

/*-----------------------------------------------------------------------------
  スレッドを開始するときのメッセージ
  -----------------------------------------------------------------------------*/
BOOL
sendCardThreadStartCmd( void )
{
	u32	send = createCmdMes( 0 , IPL_CARD_CMD_START );
	return OSSendMessage( getCardSendMesQueue(), (OSMessage)send, OS_MESSAGE_NOBLOCK );
}

/*-----------------------------------------------------------------------------
  スレッドを終了するときのメッセージ
  -----------------------------------------------------------------------------*/
BOOL
sendCardThreadStopCmd( void )
{
	u32	send = createCmdMes( 0 , IPL_CARD_CMD_STOP );

	sendCardDetachCmd( 0 );
	sendCardDetachCmd( 1 );
	return OSSendMessage( getCardSendMesQueue(), (OSMessage)send, OS_MESSAGE_NOBLOCK );
}

/*-----------------------------------------------------------------------------
  カードチェックの内部ルーチン。
  -----------------------------------------------------------------------------*/
static void
probeCardSub( u8 no )
{
	BOOL probed;

	if ( sCardThreadValid != 1 ) return;
	
	probed = CARDProbe( no );
	// slot->state < SLOT_CARD_INSERT  --> カードがない状態。
	// slot->state > SLOT_CARD_INSERT  --> カードが入っている状態。
	if ( probed && slot[ no ].state < SLOT_CARD_INSERTED ) {
		if ( sendCardInsertCmd( no ) ) {
			// 挿入された。-- GET_ENTRY
			slot[ no ].state = SLOT_GETTING_ENTRY;//CARD_RESULT_BUSY;
			slot[ no ].changed = FALSE;
		}
	}
	else if ( !probed && slot[ no ].state > SLOT_CARD_INSERTED ) {
		// 抜かれた。  -- INVALIDATE
		if ( sendCardDetachCmd( no ) ) {
			//slot[ no ].state = SLOT_INVALID_ENTRY;//CARD_RESULT_BUSY;
			slot[ no ].changed = FALSE;
		}
	}
}

/*-----------------------------------------------------------------------------
  メモリーカードの中をダンプします。
  -----------------------------------------------------------------------------*/
static void
dumpCard( u8 no )
{
	s32		ret;
	u16		i;
	char	filename[33];
	CARDDir	tempDir;
	
	DIReport1("Slot %c\n", "AB"[no] );

	for ( i = 0 ; i < CARD_MAX_FILE ; i++ ) {
		ret = __CARDGetStatusEx( (s32)no, (s32)i, &tempDir );

		if ( ret == CARD_RESULT_READY ) {
			
			memcpy( filename,
					(char*)tempDir.fileName ,
					32 );
			filename[ 32 ] = 0;
			DIReport3("%d - %s: %d blocks\n" ,
					 i ,
					 filename,
					 tempDir.length
					 );
		}
	}
}

/*-----------------------------------------------------------------------------
  CardSequencs用のエラーコードを返します。
  -----------------------------------------------------------------------------*/
static s32
getErrorCode( s32 val )
{
	s32		ret = val;
	
	switch ( val ) {
	case CARD_RESULT_READY:			ret = IPL_CARD_RESULT_READY;			break;
	case CARD_RESULT_WRONGDEVICE:	ret = IPL_CARD_RESULT_FATAL_ERROR;		break;
	case CARD_RESULT_NOCARD:		ret = IPL_CARD_RESULT_FATAL_ERROR;		break;
	case CARD_RESULT_NOFILE:		ret = IPL_CARD_RESULT_FATAL_ERROR;		break;
	case CARD_RESULT_IOERROR:		ret = IPL_CARD_RESULT_FATAL_ERROR;		break;
	case CARD_RESULT_BROKEN:		ret = IPL_CARD_RESULT_FATAL_ERROR;		break;
	case CARD_RESULT_EXIST:			ret = IPL_CARD_RESULT_EXIST;			break;
	case CARD_RESULT_NOENT:			ret = IPL_CARD_RESULT_INSSPACE;			break;
	case CARD_RESULT_INSSPACE:		ret = IPL_CARD_RESULT_INSSPACE;			break;
	case CARD_RESULT_NOPERM:		ret = IPL_CARD_RESULT_FATAL_ERROR;		break;
	case CARD_RESULT_LIMIT:			ret = IPL_CARD_RESULT_FATAL_ERROR;		break;
	case CARD_RESULT_NAMETOOLONG:	ret = IPL_CARD_RESULT_FATAL_ERROR;		break;
	case CARD_RESULT_ENCODING:		ret = IPL_CARD_RESULT_FATAL_ERROR;		break;
	case IPL_CARD_RESULT_SECTOR:	ret = IPL_CARD_RESULT_SECTOR;			break;
	default:						ret = IPL_CARD_RESULT_FATAL_ERROR;		break;
	}

	return ret;
}

/*-----------------------------------------------------------------------------
  メモリーカードスロットの状態をチェックします。
  毎フレーム呼び出します。
  -----------------------------------------------------------------------------*/
void
probeCard( void )
{
	s32		mes;
	
	probeCardSub( 0 );
	probeCardSub( 1 );
	
	while ( OSReceiveMessage( getCardRecvMesQueue(), (OSMessage)(&mes), OS_MESSAGE_NOBLOCK ) ) {
		s16	sl = getCmdMesSlot( mes );
		// メッセージ受信
		if ( getCmdMesCmd( mes ) == IPL_CARD_CMD_START ||
			 getCmdMesCmd( mes ) == IPL_CARD_CMD_STOP ) {
			sCardThreadValid = (u8)getCmdMesResult( mes );
		}
		else if ( getCmdMesCmd( mes ) == IPL_CARD_CMD_GETCODESIZE ) {
			sGetCodeCmd = TRUE;
			sNotChanged = TRUE;
		}
		else {
			switch ( getCmdMesResult( mes ) )
			{
			case CARD_RESULT_READY:
			case CARD_RESULT_EXIST:
			case IPL_CARD_RESULT_SECTOR:
				DIReport("ready or exit\n");
				slot[ sl ].state = SLOT_READY;
				break;
			case CARD_RESULT_NOENT:
			case CARD_RESULT_INSSPACE:
				DIReport("Card hasn't enough space\n");
				slot[ sl ].state = SLOT_READY;
				break;
			case CARD_RESULT_WRONGDEVICE:
				DIReport("Wrong Device\n");
				slot[ sl ].state = SLOT_WRONGDEVICE;
				break;
			case CARD_RESULT_BROKEN:
				DIReport("Broken Card\n");
				slot[ sl ].state = SLOT_BROKEN;
				break;
			case CARD_RESULT_ENCODING:
				DIReport("Other Language\n");
				slot[ sl ].state = SLOT_ENCODING;
				break;
			case CARD_RESULT_NOCARD:
				DIReport("Detach Card\n");
				slot[ sl ].state = SLOT_NOCARD;
				break;
			default:
				DIReport("Other Error\n");
				slot[ sl ].state = SLOT_CANNOT_USE;
				break;
			}
			sLastCmdResult = getErrorCode( getCmdMesResult( mes ) );
			if ( getCmdMesCmd( mes ) != IPL_CARD_CMD_MOUNT &&
				 getCmdMesCmd( mes ) != IPL_CARD_CMD_UNMOUNT
				 ) {
				
				sLastCMDFCmdResult = getErrorCode( getCmdMesResult( mes ) );
				if ( sLastSrcSlot != getCmdMesSlot( mes ) ) {
					sLastCmdFileNo = (u8)getCmdMesFileno( mes );
				}
			}
			DIReport1("set slot state : ErrorCode: %d\n", sLastCmdResult );
			slot[ sl ].changed = TRUE;
			sNotChanged = FALSE;
		}
	}
}

/*-----------------------------------------------------------------------------
  スレッドの動作状況
  -----------------------------------------------------------------------------*/
void
initCardThread( void )
{
	char*		buf;
	char*		buf_com;
	int		i;
	// アイコンバッファの確保
	buf     = gAlloc( IPL_CARD_ICON_MAX_SIZE * CARD_MAX_FILE * 2 );
	buf_com = gAlloc( IPL_CARD_COMMENT_SIZE  * CARD_MAX_FILE * 2 );

	for ( i=0; i<CARD_MAX_FILE ;i++ ) {
		sIconBuffer[0][i]  = buf + i*IPL_CARD_ICON_MAX_SIZE;
		sIconComment[0][i] = buf_com + i*IPL_CARD_COMMENT_SIZE;
	}
	for ( i=0; i<CARD_MAX_FILE ;i++ ) {
		sIconBuffer[1][i] = buf + (i+CARD_MAX_FILE)*IPL_CARD_ICON_MAX_SIZE;
		sIconComment[1][i] = buf_com + (i+CARD_MAX_FILE)*IPL_CARD_COMMENT_SIZE;
	}

	sIconReadBuf = gAlloc( IPL_CARD_ICON_MAX_SIZE );

	sTempBuffer = gAlloc( IPL_CARD_MAX_FILEBUFFER_SIZE );

	// マウント時に使用されるワークエリアの確保。
	sCardWorkArea[ 0 ] = gAlloc( CARD_WORKAREA_SIZE );
	sCardWorkArea[ 1 ] = gAlloc( CARD_WORKAREA_SIZE );
	
	// メッセージキューの作成
    OSInitMessageQueue( &sCardSendMesQueue, sCardSendMesArray, sizeof( sCardSendMesArray ) / sizeof( sCardSendMesArray[0] ));
    OSInitMessageQueue( &sCardRecvMesQueue, sCardRecvMesArray, sizeof( sCardRecvMesArray ) / sizeof( sCardRecvMesArray[0] ));

	// スレッドの作成
    OSCreateThread(
        &CardThread,
        cardMain,   
        0,          
        CardThreadStack + sizeof( CardThreadStack ),
        sizeof( CardThreadStack ),
        IPL_CARD_THREAD_PRIORITY,
        OS_THREAD_ATTR_DETACH );

	// スレッドの実行
	OSResumeThread( &CardThread );

	sendCardThreadStartCmd();

	DIReport("initCardThread successfully\n");
}

/*-----------------------------------------------------------------------------
  Main -> Card の(外部からOSSendMessageする)ためのメッセージキューを返します。
  -----------------------------------------------------------------------------*/
OSMessageQueue*
getCardSendMesQueue( void )
{
	// 内部的にCardRecvメッセージキューは、Card側がRecvするための
	// メッセージキューです。
	return &sCardRecvMesQueue;
}

/*-----------------------------------------------------------------------------
  Card -> Mainの（外部からOSReceiveMessageする）ためのメッセージキューを返します。
  -----------------------------------------------------------------------------*/
OSMessageQueue*
getCardRecvMesQueue( void )
{
	// 内部的にCardSendメッセージキューは、Card側がsendするための
	// メッセージキューです。
	return &sCardSendMesQueue;
}

/*-----------------------------------------------------------------------------
  アイコンの情報を取得するためのポインタの取得。
  -----------------------------------------------------------------------------*/
IconStateArrayPtr
getIconStateArray( void )
{
	return icon;
}

/*-----------------------------------------------------------------------------
  アイコンバッファのベースポインタを返します。
  getIconBuffer( no, fileNo ) + icon[no][fileNo].banner_offsetでバナーのオフセット
  getIconBuffer( no, fileNo ) + icon[no][fileNo].icon_offset[0]でicon0のオフセット
  などが取得できます。
  -----------------------------------------------------------------------------*/
char*
getIconBuffer( u8 no, s16 fileNo )
{
	return sIconBuffer[no][fileNo];
}

char*
getIconComment( u8 no, s16 fileNo )
{
	return sIconComment[no][fileNo];
}























/*-----------------------------------------------------------------------------
  CardThread Main routine
  -----------------------------------------------------------------------------*/
static void
invalidDirState( u8 no, s16 fileNo )
{
	BOOL enable = OSDisableInterrupts();
	card[ no ][ fileNo ].validate = FALSE;
	card[ no ][ fileNo ].blocks = 0;
	card[ no ][ fileNo ].copy_enable = TRUE;
	card[ no ][ fileNo ].move_enable = TRUE;
	card[ no ][ fileNo ].same_file = FALSE;

	icon[ no ][ fileNo ].icon_enable = FALSE;
	icon[ no ][ fileNo ].banner_enable = FALSE;
	OSRestoreInterrupts( enable );
}

static void
updateFreeSizeInfo( s32 sl )
{
	u32		sector_size;
	s32		free_size, free_fileentry, ret;
	u16		mem_size;
	
	if ( (ret = CARDGetMemSize( sl, &mem_size )) < 0 ){
		DIReport3("Get Memsize Error-%d: %d: %d\n",sl,ret, mem_size);
	}
	if ( (ret = CARDGetSectorSize( sl, &sector_size )) < 0 ) {
		DIReport2("Get Sector Error- %d: %d\n",ret, sector_size);
	}
	if ( (ret = CARDFreeBlocks( sl, &free_size, &free_fileentry )) < 0 ) {
		DIReport2("Get FreeBlocks Error- %d %d\n",ret,free_size);
	}
	if ( CARDGetResultCode( sl ) >= 0 ) {
		BOOL enable = OSDisableInterrupts();
		slot[ sl ].full_blocks = (u16)( mem_size * 128 * 1024 / sector_size - 5 );
		slot[ sl ].sector_size = sector_size;
		slot[ sl ].free_fileentry = (u16)free_fileentry;
		slot[ sl ].free_blocks = (u16)( free_size / sector_size );
		OSRestoreInterrupts( enable );
		DIReport4("%d:%dbytes sectorsize , %d blocks , %d freeEntry\n", sl, slot[sl].sector_size, slot[sl].free_blocks, slot[sl].free_fileentry);
	}
}

static void*
cardMain( void* )
{
	u32		msg;
	u8		valid = 0;

	sMounted[ 0 ] = FALSE;
	sMounted[ 1 ] = FALSE;

	for (;;)
	{
		OSReceiveMessage( &sCardRecvMesQueue , (OSMessage*)&msg, OS_MESSAGE_BLOCK );

		if ( valid == 1 ) {	// スレッド有効。
			switch ( getCmdMesCmd( msg ) )
			{
			case IPL_CARD_CMD_START:		sendValidState( (u8)getCmdMesCmd( msg ), valid = 1 );	break;
			case IPL_CARD_CMD_STOP:			sendValidState( (u8)getCmdMesCmd( msg ), valid = 0 );	break;
			case IPL_CARD_CMD_MOUNT:		mountCard  ( (u8)getCmdMesSlot( msg ) );		break;
			case IPL_CARD_CMD_UNMOUNT:		unmountCard( (u8)getCmdMesSlot( msg ) );		break;
			case IPL_CARD_CMD_FORMAT:		formatCard ( (u8)getCmdMesSlot( msg ) );		break;
			case IPL_CARD_CMD_FILECOPY:		copymoveFile( (u8)getCmdMesSlot( msg ), (s16)getCmdMesFileno( msg ) , getCmdMesCmd( msg ));	break;
			case IPL_CARD_CMD_FILEMOVE:		copymoveFile( (u8)getCmdMesSlot( msg ), (s16)getCmdMesFileno( msg ) , getCmdMesCmd( msg ));	break;
			case IPL_CARD_CMD_FILEDELETE:	deleteFile ( (u8)getCmdMesSlot( msg ), (s16)getCmdMesFileno( msg ) );	break;
			case IPL_CARD_CMD_GETCODESIZE:	getGameCodeFileBlocks(); break;
			}
//			updateSamefileFlag();
		}
		else {	// リセット時。
			switch ( getCmdMesCmd( msg ) )
			{
			case IPL_CARD_CMD_START:		sendValidState( (u8)getCmdMesCmd( msg ), valid = 1 );	break;
			case IPL_CARD_CMD_STOP:			sendValidState( (u8)getCmdMesCmd( msg ), valid = 0 );	break;
			case IPL_CARD_CMD_GETCODESIZE:	getGameCodeFileBlocks(); break;
			default:
				processError( (u8)getCmdMesSlot( msg ),  getCmdMesCmd( msg ), CARD_RESULT_NOCARD );
			}
		}
	}

	return NULL;// don't reach here
}

/*-----------------------------------------------------------------------------
  同じファイルがあるフラグをチェックします。
  -----------------------------------------------------------------------------*/
static void
updateSamefileFlag( void )
{
	s32	no, fileNo;

	for ( no = 0 ; no < 2 ; no++ ) {
		for ( fileNo = 0 ; fileNo < CARD_MAX_FILE ; fileNo++ ) {
			card[no][fileNo].same_file = FALSE;
			
			if ( sMounted[ no ] && sMounted[ no^1 ] ) {
				if ( checkSamefileExist( (u8)no, (s16)fileNo ) < 0) {
					card[no][fileNo].same_file = TRUE;
				}
			}
		}
	}
}


/*-----------------------------------------------------------------------------
  カードスレッドが使用可能かどうかを確認する。
  -----------------------------------------------------------------------------*/
static void
sendValidState( u8 cmd, u8 valid )
{
	u32		send;

	DIReport1("card thread valid state changed:%d\n", valid);
	send = (u32)createResultMes( 0 , cmd , valid , 0);
	OSSendMessage( &sCardSendMesQueue, (OSMessage)send, OS_MESSAGE_BLOCK );
}

/*-----------------------------------------------------------------------------
  壊れたファイルかどうかのチェック
  -----------------------------------------------------------------------------*/
static BOOL
checkBrokenFile( CARDDir* dir )
{
	BOOL ret;
	u8	ipl_company[] = { 0, 0 };
	u8	ipl_gameName[] = { 0, 0, 0, 0 };

	if ( strncmp( (char*)dir->fileName,
				  (char*)IPL_CARD_BROKEN_FILENAME,
				  IPL_CARD_BROKEN_FILENAMESIZE ) == 0 &&
		 memcmp( dir->gameName, ipl_gameName, sizeof(dir->gameName) ) == 0 &&
		 memcmp( dir->company , ipl_company , sizeof(dir->company ) ) == 0 ) {
		ret = TRUE;
	}
	else {
		ret = FALSE;
	}

	return ret;
}


/*-----------------------------------------------------------------------------
  マウント時のエラー処理。
  戻り値
	TRUE:処理続行可能。
	FALSE:処理続行不能。
  -----------------------------------------------------------------------------*/
static BOOL
processMountError( u8 no, s32 ret )
{
	switch ( ret )
	{
	case CARD_RESULT_NOFILE:
		//DIReport("CardThread:ERROR NOFILE\n");
		return TRUE;
		break;

	case CARD_RESULT_BROKEN:
		if ( (ret = CARDCheck( (s32)no )) < 0 ) {
			return processError( no , IPL_CARD_CMD_MOUNT, ret );
		} else {
			DIReport("CardThread:Broken Card Repaired\n" );
			return TRUE;
		}
		break;
		
	default:
		return processError( no , IPL_CARD_CMD_MOUNT, ret );
		break;
	}
}

/*-----------------------------------------------------------------------------
  見逃しても良いエラーじゃないとき。
	基本的に、noで指定されたスロットは、使用できなくなります。
  -----------------------------------------------------------------------------*/
static BOOL
processError( u8 no, s32 cmd, s32 ret )
{
	u32	send;
//	s32	unmount_result;

	if ( ret != CARD_RESULT_EXIST &&
		 ret != IPL_CARD_RESULT_SECTOR &&
		 ret != CARD_RESULT_NOENT &&
		 ret != CARD_RESULT_INSSPACE ) {
		initCardArea( no );	// 途中まで読めていても、駄目ならクリアする。

		if ( ret != CARD_RESULT_BROKEN &&
			 ret != CARD_RESULT_ENCODING ) {
			DIReport1("DEBUG: UnmountCard %d \n", no );
			CARDUnmount( no );
			sMounted[ no ] = FALSE;
		}
	}
	
	DIReport1("DEBUG: CardThread error %d\n", ret );
	updateSamefileFlag();
	
	send = (u32)createResultMes( no, cmd, ret , 0 );
	OSSendMessage( &sCardSendMesQueue, (OSMessage)send, OS_MESSAGE_BLOCK );
	return FALSE;
}

/*-----------------------------------------------------------------------------
  終了メッセージを送ります。
  -----------------------------------------------------------------------------*/
static void
sendDoneMes( u8 no, s32 cmd, u8 fileNo )
{
	u32	send;

	updateSamefileFlag();
	
	send = (u32)createResultMes( no, cmd, CARD_RESULT_READY, fileNo );
	OSSendMessage( &sCardSendMesQueue, (OSMessage)send, OS_MESSAGE_BLOCK );
}

/*-----------------------------------------------------------------------------
  CARDDirを無効状態にする。
  -----------------------------------------------------------------------------*/
static void
initCardArea( u8 no )
{
	s16		i;

	for ( i = 0 ; i < CARD_MAX_FILE ; i++ ) {
		//card[ no ][ i ].validate = FALSE;
		invalidDirState( no, i );
	}
}

/*-----------------------------------------------------------------------------
  バナーのアドレス等を解決する。
  -----------------------------------------------------------------------------*/
static int
resolveBanner( u8 no, s16 fileNo, CARDDir* dir )
{
	int		read_size = 0;
	u32		offset = 0;

	switch ( CARDGetBannerFormat( dir ) )
	{
	case CARD_STAT_BANNER_C8:
		icon[no][fileNo].banner_enable = TRUE;
		icon[no][fileNo].banner_format = (u8)GX_TF_C8;
		read_size += CARD_BANNER_WIDTH * CARD_BANNER_HEIGHT + 256 * 2;

		icon[no][fileNo].banner_offset = (s32)( (u32)sIconBuffer[no][fileNo] - (u32)&icon[no][fileNo] + offset );
		icon[no][fileNo].banner_palette_offset = (s32)( (u32)icon[no][fileNo].banner_offset + 96 * 32 );
		icon[no][fileNo].icon_offset[0] = (s32)( (u32)icon[no][fileNo].banner_palette_offset + 512 );
		break;
	case CARD_STAT_BANNER_RGB5A3:
		icon[no][fileNo].banner_enable = TRUE;
		icon[no][fileNo].banner_format = (u8)GX_TF_RGB5A3;
		read_size += CARD_BANNER_WIDTH * CARD_BANNER_HEIGHT * 2;

		icon[no][fileNo].banner_offset = (s32)( (u32)sIconBuffer[no][fileNo] - (u32)&icon[no][fileNo] + offset );
		icon[no][fileNo].icon_offset[0] = (s32)( (u32)icon[no][fileNo].banner_offset + 96 * 32 * 2 );
		break;
	default:
		icon[no][fileNo].banner_enable = FALSE;
		icon[no][fileNo].banner_format = (u8)0;

		icon[no][fileNo].icon_offset[0] = (s32)( (u32)sIconBuffer[no][fileNo] - (u32)&icon[no][fileNo] + offset );
		break;
	}

	return read_size;
}
/*-----------------------------------------------------------------------------
  アイコンアドレスなどを解決する。
  -----------------------------------------------------------------------------*/
static int
resolveIcons( u8 no, s16 fileNo, CARDDir* dir )
{
	int		i;
	BOOL	icon_ci   = FALSE;
	int		read_size = 0;
	int		icon_size;
	
	icon[no][fileNo].icon_nums = 0;
	icon[no][fileNo].icon_all_frame = 0;

	icon[no][fileNo].iconSpeed = dir->iconSpeed;

    icon[no][fileNo].icon_anim_first_frame = (u8)( CARDGetIconSpeed( dir, 0 ) * 4 );
    
	for ( i = 0 ; i < 8 ; i++ ) {
		if ( CARDGetIconSpeed( dir, i ) != CARD_STAT_SPEED_END ) {
			icon[no][fileNo].icon_all_frame += CARDGetIconSpeed( dir, i ) * 4;
		}
		else {
            icon[no][fileNo].icon_anim_last_frame = (u8)( CARDGetIconSpeed( dir, (i-1) ) * 4 );
			goto Format;
		}
	}
	icon[no][fileNo].icon_anim_last_frame = (u8)( CARDGetIconSpeed( dir, 7 ) * 4 );

Format:
	if ( CARDGetIconSpeed ( dir, 0 ) == CARD_STAT_SPEED_END ||
		 CARDGetIconFormat( dir, 0 ) == CARD_STAT_ICON_NONE ) {
		icon[no][fileNo].icon_enable = FALSE;
		return 0;
	}
	
	for ( i = 0 ; i < 8 ; i++ ) {
		if ( CARDGetIconSpeed( dir, i ) != CARD_STAT_SPEED_END ) {
			switch ( CARDGetIconFormat( dir, i ) )
			{
			case CARD_STAT_ICON_C8:
				icon[no][fileNo].icon_format[i] = (u8)GX_TF_C8;
				icon_size = CARD_ICON_WIDTH * CARD_ICON_HEIGHT;
				icon_ci = TRUE;
				break;
			case CARD_STAT_ICON_RGB5A3:
				icon[no][fileNo].icon_format[i] = (u8)GX_TF_RGB5A3;
				icon_size = CARD_ICON_WIDTH * CARD_ICON_HEIGHT * 2;
				break;
			case CARD_STAT_ICON_NONE:
				icon[no][fileNo].icon_format[i] = icon[no][fileNo].icon_format[i-1];
				icon_size = 0;
				break;
			}
			
			if ( i < 7 ) {
				icon[no][fileNo].icon_offset[i+1] = (s32)( (u32)icon[no][fileNo].icon_offset[i] + icon_size );
			}
			else {
				icon[no][fileNo].icon_palette_offset = (s32)( (u32)icon[no][fileNo].icon_offset[i] + icon_size );
			}
			read_size += icon_size;
			icon[no][fileNo].icon_nums++;
		}
		else {
			icon[no][fileNo].icon_palette_offset = icon[no][fileNo].icon_offset[i];
			goto End;
		}
	}

End:
	if ( icon_ci ) {
		read_size += 512;
	}

	icon[no][fileNo].icon_enable = TRUE;

	icon[no][fileNo].icon_anim_style = (u8)CARDGetIconAnim( dir );
	icon[no][fileNo].icon_bounds = 1;

	return read_size;
}

/*-----------------------------------------------------------------------------
  アイコンデータのダンプ。
  -----------------------------------------------------------------------------*/
static void
dumpIconArea( u8 no )
{
	int j,k;
	for ( j = 0 ; j < CARD_MAX_FILE ; j++ ) {
		if ( icon[no][j].banner_enable ) {
			DIReport2("%03d:banner_format:%02d\n",j, icon[no][j].banner_format);
			DIReport2("%03d:banner_offset:%08x\n",j, icon[no][j].banner_offset);
		}
		if ( icon[no][j].icon_enable ) {
			for ( k = 0 ; k < icon[no][j].icon_nums ; k++ ) {
				DIReport3("%03d:icon_format:%d:%02d\n",j,k, icon[no][j].icon_format[k]);
			}
			for ( k = 0 ; k < icon[no][j].icon_nums ; k++ ) {
				DIReport3("%03d:icon_offset:%d:%08x\n",j,k, icon[no][j].icon_offset[k]);
			}
		}
	}
}

/*-----------------------------------------------------------------------------
  ファイルのアイコンとバナーを取得する。
  -----------------------------------------------------------------------------*/
static s32
getFileIcons( u8 no, s16 fileNo, CARDDir* dir, CARDFileInfo* tFileInfo )
{
	int 	read_size = 0;
	u32		align_read_from, align_read_size, sector_size;
	u32		offset;
	s32		ret;

	align_read_from	= gRoundDown512B( dir->iconAddr );
	offset = (u32)( dir->iconAddr - align_read_from );

	read_size = resolveBanner( no, fileNo, dir );
	read_size += resolveIcons( no, fileNo, dir );

	align_read_size = gRoundUp512B( read_size + offset );

	ret = CARDGetSectorSize( no, &sector_size );
	if ( ret < 0 ) {
		icon[no][fileNo].banner_enable = FALSE;
		icon[no][fileNo].icon_enable = FALSE;
		return 0;
	}

	if ( align_read_from < 0 || align_read_from > dir->length * sector_size ) {
		icon[no][fileNo].banner_enable = FALSE;
		icon[no][fileNo].icon_enable = FALSE;
		return 0;
	}

	if ( ( align_read_from + align_read_size ) < 0 ||
		 ( align_read_from + align_read_size ) > dir->length * sector_size ) {
		icon[no][fileNo].banner_enable = FALSE;
		icon[no][fileNo].icon_enable = FALSE;
		return 0;
	}

	if ( read_size > 0 ) {
		ret = CARDRead( tFileInfo, sIconReadBuf, (s32)align_read_size, (s32)align_read_from );
		if ( ret < 0 ) {
			return ret;
		}
		memcpy( sIconBuffer[no][fileNo], (u8*)sIconReadBuf + offset, (u32)read_size );
		DCStoreRange( sIconBuffer[no][fileNo], align_read_size );
	}
	
	return 0;
}

/*-----------------------------------------------------------------------------
  ファイルのコメントを取得します。
  -----------------------------------------------------------------------------*/
static s32
getFileComment( u8 no, s16 fileNo, CARDDir* dir, CARDFileInfo* tFileInfo )
{
	s32		ret;
	s32 	align_read_from, align_read_size, buf_offset;
	u32		sector_size;

	// コメントでーたを読みこむ。
	align_read_from	= (s32)gRoundDown512B( dir->commentAddr );
	buf_offset      = (s32)( (s32)dir->commentAddr - align_read_from );
	align_read_size = (s32)gRoundUp512B( (u32)dir->commentAddr + 64 ) - align_read_from;
	
	if ( ( ret = CARDGetSectorSize( no, &sector_size ) ) < 0 ) {
//		DIReport("%03d: can't get sectorsize.\n", no );
		goto NOCOMMENT;
	}
	
	if ( align_read_from < 0 || align_read_from > dir->length * sector_size ) {
//		DIReport("%03d-%03d: fatal error in comment addr %08x\n", no, fileNo, align_read_from );
		ret = 0;
		goto NOCOMMENT;
	}

	if ( ( align_read_from + align_read_size ) < 0 ||
		 ( align_read_from + align_read_size ) > dir->length * sector_size ) {
//		DIReport("%03d-%03d: fatal error in comment size %08x\n", no, fileNo, align_read_size );
		ret = 0;
		goto NOCOMMENT;
	}
	
	ret = CARDRead( tFileInfo,
					temp_buf,
					(s32)align_read_size,
					(s32)align_read_from );
	if ( ret < 0 ) {
//		DIReport("can't read comments.\n");
		goto NOCOMMENT;
	}
	
	memset( sIconComment[no][fileNo], 0 ,64 );
	memcpy( sIconComment[no][fileNo], temp_buf + buf_offset , 64 );

	return 0;

NOCOMMENT:
	memset( sIconComment[no][fileNo], 0 ,64 );

	return ret;
}

/*-----------------------------------------------------------------------------
  ファイルの情報（アイコン・バナー・コメント）を取得します。
  -----------------------------------------------------------------------------*/
static s32
getFileInfo( u8 no, s16 fileNo, CARDDir* dir )
{
	s32 			ret;
	CARDFileInfo	tFileInfo;

	ret = CARDFastOpen( no, fileNo, &tFileInfo );
	if ( ret < 0 ) {
//		DIReport("can't open file for reading icons & comments.\n");
		return ret;
	}
	
	if ( ( ret = getFileIcons( no, fileNo, dir, &tFileInfo ) ) < 0 ) {
		return ret;
	}

	if ( ( ret = getFileComment( no, fileNo, dir, &tFileInfo ) ) < 0 ) {
		return ret;
	}

	ret = CARDClose( &tFileInfo );
	if ( ret < 0 ) {
//		DIReport("can't close file.\n");
		return ret;
	}
	
	// ファイルエントリは有効。
	card[ no ][ fileNo ].validate   = TRUE;
	card[ no ][ fileNo ].blocks     = dir->length;
	card[ no ][ fileNo ].time       = dir->time;
	card[ no ][ fileNo ].copy_enable = (u8)( ( ( dir->permission & CARD_DIR_PERM_NO_COPY ) != CARD_DIR_PERM_NO_COPY  )? TRUE : FALSE );
	card[ no ][ fileNo ].move_enable = (u8)( ( ( dir->permission & CARD_DIR_PERM_NO_MOVE ) != CARD_DIR_PERM_NO_MOVE  )? TRUE : FALSE );
	
	return 0;
}

/*-----------------------------------------------------------------------------
  カードをアンマウントする。
  -----------------------------------------------------------------------------*/
static void
unmountCard( u8 no )
{
//	CARDUnmount( (s32)no ); // processError内で実行するので、必要なし。

	processError( no, IPL_CARD_CMD_UNMOUNT, CARD_RESULT_NOCARD );
}

/*-----------------------------------------------------------------------------
  ファイルエントリの取得。
  -----------------------------------------------------------------------------*/
static void
mountCard( u8 no )
{
	s32		ret;
	s16 	i;
	CARDDir	tempDir;

	if ( ( ret = CARDMount( (s32)no , sCardWorkArea[ no ] , NULL ) ) < 0 ) {
		// エラー処理
		if ( !processMountError( no, ret ) ) {
			return;
		}
	} else {
		// Mountしたら、とりあえずチェック。
		if ( (ret = CARDCheck( (s32)no )) < 0 ) goto Error;
	}

	// カードバッファを初期化。
	initCardArea( no );

	for ( i = 0 ; i < CARD_MAX_FILE ; i++ ) {
		if ( ( ret = __CARDGetStatusEx( (s32)no, (s32)i, &tempDir ) ) < 0 ) {
			// 一般エラー処理 ファイルエントリは無効。
			if ( !processMountError( no, ret ) )	{
				return;
			}
		}
		else {
			if ( checkBrokenFile( &tempDir ) ) {
				// 正常に読みこめたが、修復されたファイルで必要の無いとき。
//				DIReport("delete broken file\n");
				if ( ( ret = CARDFastDelete( (s32)no, (s32)i ) ) < 0 ) goto Error;
			}
			else {
				// ここから、アイコンデータの取得。
				if ( ( ret = getFileInfo( no , i, &tempDir ) ) < 0 ) goto Error;
			}
		}
	}

	updateFreeSizeInfo( no );
	sMounted[ no ] = TRUE;
	sendDoneMes( no , IPL_CARD_CMD_MOUNT, 0 );

	dumpCard( no );
	
	return;
	
Error:
	DIReport("MountError\n");
	processError( no, IPL_CARD_CMD_MOUNT ,ret );
	return;
}

/*-----------------------------------------------------------------------------
  コピー/移動用のダミーファイルを作成します。
  -----------------------------------------------------------------------------*/
static s16
createDummyFile( u8 no , u32 fileSize )
{
	s32		ret, num = 0;

	do {
		sprintf( sTempFilename, "Broken File%03d", num );
		ret = CARDCreate( no, sTempFilename, fileSize, &sDstFileInfo );
		if ( ret < 0 && ret != CARD_RESULT_EXIST ) {
			DIReport("Can't Create temp File with Error.");
			return (s16)ret;
		}
		num++;
	} while ( ret != CARD_RESULT_READY && num < 128 );

	if ( num < 128 ) {
		return (s16)CARDGetFileNo( &sDstFileInfo );
	}
	
	return (s16)CARD_RESULT_FATAL_ERROR;
}

/*-----------------------------------------------------------------------------
  作成したダミーファイルにファイルをコピーします。
  -----------------------------------------------------------------------------*/
static void
writeCallback( s32 chan, s32 result )
{
#pragma unused (chan)
#pragma unused (result)
	sWriting = 0;
}

static s32
copyFileData( u8 no , s16 fileNo )
{
	s32			ret,  offset = 0;
	u32			length, sector_size, blocks;
	s32			length_bytes, offset_bytes;
	CARDDir		tempDir;
	u16			error_state = 0;
	u16			cancel = FALSE;

	// ------------------------------------------------------------[ error_state: 0 ]
	if ( ( ret = __CARDGetStatusEx( (s32)no, (s32)fileNo, &tempDir ) ) < 0 ) goto Error;
	error_state++;
	// ------------------------------------------------------------[ error_state: 1 ]
	if ( ( ret = CARDGetSectorSize( (s32)no, &sector_size ) ) < 0 ) goto Error;
	blocks = IPL_CARD_MAX_FILEBUFFER_SIZE / sector_size;	// 一度にコピーできるブロック数。
	error_state++;
	// ------------------------------------------------------------[ error_state: 2 ]
	if ( ( ret = CARDFastOpen( no, fileNo, &sSrcFileInfo ) ) < 0 ) goto Error;
	error_state++;
	// ------------------------------------------------------------[ error_state: 3 ]
	for ( offset = 0 ; offset < tempDir.length ; offset += blocks ) {
		length = ( tempDir.length - offset > blocks )?
					blocks : tempDir.length - offset;
		length_bytes = (s32)( length * sector_size );
		offset_bytes = (s32)( offset * sector_size );
		
		if ( ( ret = CARDRead ( &sSrcFileInfo, sTempBuffer, length_bytes, offset_bytes ) ) < 0 ) goto Error;
		sWriting = 1;
		if ( ( ret = CARDWriteAsync( &sDstFileInfo, sTempBuffer, length_bytes, offset_bytes, writeCallback ) ) < 0 ) goto Error;
		while ( sWriting ) {
			if ( !CARDProbe( no ) ) {
				if ( !cancel ) {
					CARDCancel( &sDstFileInfo );
					cancel = TRUE;
				}
			}
		}
	}
	error_state++;
	// ------------------------------------------------------------[ error_state: 4 ]
	if ( ( ret = CARDClose( &sSrcFileInfo ) ) < 0 ) goto Error;
	error_state++;
	// ------------------------------------------------------------[ error_state: 5 ]
	if ( ( ret = CARDClose( &sDstFileInfo ) ) < 0 ) goto Error;
	
	return 0;

Error:
	switch ( error_state )
	{
	case 0:	DIReport("Can't get status of Copy source File.\n");		break;
	case 1:	DIReport("Can't get sector size of Copy source card.\n");	break;
	case 2: DIReport("Can't Open Copy source File.\n");					break;
	case 3: DIReport("Can't read or write data.\n");					break;
	case 4: DIReport("Can't Close Copy src File.\n");					break;
	case 5: DIReport("Can't Close Copy dst File.\n");					break;
	}
	CARDFastDelete( (s32)( no^1 ), CARDGetFileNo( &sDstFileInfo ) );

	return ret;
}

/*-----------------------------------------------------------------------------
  コピー向けのリネーム処理
  -----------------------------------------------------------------------------*/
static s32
renameForCopy( u8 no , s16 srcFileNo , s16 dstFileNo, BOOL *dummy )
{
	s32		ret;
    u32		real_time;
	CARDDir	tempDir;

	if ( ( ret = __CARDGetStatusEx( no, srcFileNo, &tempDir ) ) < 0 ) {
		DIReport("Can't get status for src file\n");
		return ret;
	}
	
	tempDir.copyTimes++;
	if ( ( ret = __CARDSetStatusEx( no  , srcFileNo, &tempDir ) ) < 0 ) {
		DIReport("Can't set status for src file\n");
		return ret;
	}

    real_time = tempDir.time;
    tempDir.time = (u32)OSTicksToSeconds( (OSTime)OSGetTime() );// 時間をコピーした時間にする。
	if ( ( ret = __CARDSetStatusEx( no^1, dstFileNo, &tempDir ) ) < 0 ) {
		DIReport("Can't set status for dst file (src file copyTimes--)\n");
		if ( ret != CARD_RESULT_NOCARD ) {
			tempDir.copyTimes--;
		}
        tempDir.time = real_time;
		if ( __CARDSetStatusEx( no  , srcFileNo, &tempDir ) < 0 ) {
			DIReport("Can't set status for src file(src file copyTimes--)\n");
		}
		return ret;
	}
	*dummy = TRUE;
	
	if ( ( ret = getFileInfo( (u8)( no^1 ), dstFileNo, &tempDir ) ) < 0 ) {
		DIReport("Can't read icon for move dstfile\n");
		return ret;
	}

	return 0;
}

/*-----------------------------------------------------------------------------
  move向けのリネーム処理
  -----------------------------------------------------------------------------*/
static s32
renameForMove( u8 no , s16 srcFileNo , s16 dstFileNo , BOOL *dummy )
{
	s32		ret;
	CARDDir srcDir, dstDir;// = card[ no ][ srcFileNo ].dir;
	s16		num = 0;
	s16		error_state = 0;
	// ------------------------------------------------------------[ error_state: 0 ]
	if ( ( ret = __CARDGetStatusEx( (s32)no, (s32)srcFileNo, &srcDir ) ) < 0 ) goto Error;
	error_state++;
	// ------------------------------------------------------------[ error_state: 1 ]
	dstDir = srcDir;
	// companyとgameNameをクリアします。
	memset( dstDir.company, 0, sizeof(dstDir.company) );
	memset( dstDir.gameName,0, sizeof(dstDir.gameName));
	// 移動元FilenameをBrokenFileにします。
	do {
		memset( dstDir.fileName, 0 , sizeof(dstDir.fileName) );
		sprintf( (char*)dstDir.fileName, "Broken File%03d", num );
		ret = __CARDSetStatusEx( (s32)no  , srcFileNo, &dstDir );
		if ( ret < 0 && ret != CARD_RESULT_EXIST ) goto Error;
		num++;
	} while ( ret != CARD_RESULT_READY && num < 128 );
	error_state++;
	// ------------------------------------------------------------[ error_state: 2 ]
	// 移動先ファイルのgameNameとcompanyをセット。
	// 移動先のファイルのリネーム。
	if ( ( ret = __CARDSetStatusEx( no^1, dstFileNo, &srcDir ) ) < 0 ) {
		// 移動元を修復する。（不測の事態を考えて、２重チェック）
		if ( ret == CARD_RESULT_NOCARD ) {
			CARDFastDelete( no, srcFileNo );
			invalidDirState( no, srcFileNo );
		} else if ( __CARDSetStatusEx( no  , srcFileNo, &srcDir ) < 0 ) {
			DIReport("Can't repair src file - carddir\n");
		}
		goto Error;
	}
	*dummy = TRUE;	// 移動先完成。
	error_state++;
	// ------------------------------------------------------------[ error_state: 3 ]
	if ( ( ret = CARDFastDelete( no, srcFileNo ) ) < 0 ) goto Error;
	//card[no][srcFileNo].validate = FALSE;
	invalidDirState( no, srcFileNo );
	error_state++;
	// ------------------------------------------------------------[ error_state: 4 ]
	if ( ( ret = __CARDGetStatusEx( (u8)( no^1 ), (s16)dstFileNo, &srcDir ) ) < 0 ) goto Error;
	error_state++;
	// ------------------------------------------------------------[ error_state: 5 ]
	if ( ( ret = getFileInfo( (u8)( no^1 ), dstFileNo, &srcDir  ) ) < 0 ) goto Error;

	return 0;

Error:
	switch ( error_state )
	{
	case 0:	DIReport("Cant' get status for src file.\n");			break;
	case 1: DIReport("Can't set status for src file\n");			break;
	case 2: DIReport("Can't rename dst file\n");					break;
	case 3: DIReport("Can't Delete src file\n");					break;
	case 4: DIReport("Cant' get status for dst file.\n");			break;
	case 5: DIReport("Can't read icon for move dstfile\n");			break;
	}
	return ret;
}

/*-----------------------------------------------------------------------------
  同名のファイルが相手側のデジカードにあるかどうかチェックします。
  -----------------------------------------------------------------------------*/
static s32
checkSamefileExist( u8 no , s16 fileNo )
{
	s32		ret;
	s32		i;
	CARDDir srcDir, dstDir;

	ret = __CARDGetStatusEx( (s32)no, (s32)fileNo, &srcDir );
	if ( ret < 0 ) {
		return ret;
	}

	for ( i = 0 ; i < CARD_MAX_FILE ; i++ ) {
		ret = __CARDGetStatusEx( (s32)(no^1), i, &dstDir );
		if ( ret < 0 && ret != CARD_RESULT_NOFILE ) {
//			DIReport("unrecoverable error\n");
			return ret;
		}
		if ( ret == CARD_RESULT_READY ) {
			if (memcmp(srcDir.gameName, dstDir.gameName, sizeof(srcDir.gameName)) == 0 &&
				memcmp(srcDir.company , dstDir.company , sizeof(srcDir.company )) == 0 &&
				memcmp(srcDir.fileName, dstDir.fileName, sizeof(srcDir.fileName)) == 0
				)
			{
/*				DIReport("srcfile:%d\n", no);
				DIReport("dstfile:%d\n", i);
				DIReport("src filename: %s\n", srcDir.fileName );
				DIReport("dst filename: %s\n", dstDir.fileName );*/
				return CARD_RESULT_EXIST;
			}
		}
	}


	return CARD_RESULT_READY;
}

/*-----------------------------------------------------------------------------
  両方のスロットのセクタサイズをチェックします。
  -----------------------------------------------------------------------------*/
static s32
checkSectorSize( u32* sector_size )
{
	u32		sector_a, sector_b;
	s32		ret;
	
	*sector_size = 0;
	
	if ( ( ret = CARDGetSectorSize( 0, &sector_a ) ) < 0 ) return ret;
	if ( ( ret = CARDGetSectorSize( 1, &sector_b ) ) < 0 ) return ret;

	if ( sector_a != sector_b ) {
		return IPL_CARD_RESULT_SECTOR;
	}

	*sector_size = sector_a;

	return CARD_RESULT_READY;
}


/*-----------------------------------------------------------------------------
  ファイルを移動します。
  -----------------------------------------------------------------------------*/
static BOOL
checkFileInfo( u8 no , s16 fileNo )
{
	CARDDir	tempDir;

	if ( __CARDGetStatusEx( no, fileNo, &tempDir ) < 0 ) goto Error;
	if ( getFileInfo( no, fileNo, &tempDir ) < 0 ) goto Error;

	return TRUE;

Error:
	invalidDirState( no, fileNo );
	return FALSE;
}

static s32
checkPermission( u8 no, s16 fileNo, s32 cmd )
{
	s32	ret = CARD_RESULT_READY;
	CARDDir	tempDir;

	if ( __CARDGetStatusEx( no, fileNo, &tempDir ) < 0 ) goto Error;
	
	switch( cmd )
	{
	case IPL_CARD_CMD_FILECOPY:
		if ( tempDir.permission & CARD_DIR_PERM_NO_COPY ) {
			ret = CARD_RESULT_NOPERM;
		}
		break;
	case IPL_CARD_CMD_FILEMOVE:
		if ( tempDir.permission & CARD_DIR_PERM_NO_MOVE ) {
			ret = CARD_RESULT_NOPERM;
		}
		break;
	}

	return ret;

Error:
	return CARDGetResultCode( no );
}

static void
copymoveFile( u8 no , s16 fileNo , s32 cmd )
{
	s32		ret;
	s16		dstFileNo = (s16)0xffff;
	u32		card_block_size;
	CARDDir tempDir;
	BOOL	dummyExist = FALSE;
	BOOL	Done = FALSE;

	if ( ( ret = checkSamefileExist( no, fileNo ) ) < 0 )	{
		processError( no, cmd, ret );
		return;
	}
	if ( ( ret = checkSectorSize( &card_block_size ) ) < 0 ) {
		processError( no, cmd, ret );
		return;
	}

	if ( ( ret = checkPermission( no, fileNo, cmd ) ) < 0 ) {
		processError( no, cmd, ret );
		return;
	}
	
	if ( ( ret = __CARDGetStatusEx( no, fileNo, &tempDir ) ) < 0 )	goto Error;

	dstFileNo = createDummyFile( (u8)(no^1), (u32)(tempDir.length * card_block_size) );
	if ( dstFileNo < 0 ) {
		processError( (u8)( no^1 ), cmd, (s32)dstFileNo );
		return;
	} else {
		dummyExist = TRUE;
	}
	
	if ( ( ret = copyFileData( no, fileNo ) ) < 0 )				goto Error;

	if ( cmd == IPL_CARD_CMD_FILECOPY ) {
		if ( ( ret = renameForCopy( no, fileNo, dstFileNo, &Done ) ) < 0 ) goto Error;
	} else if ( cmd == IPL_CARD_CMD_FILEMOVE ) {
		if ( ( ret = renameForMove( no, fileNo, dstFileNo, &Done ) ) < 0 )	goto Error;
	}

	updateFreeSizeInfo( no   );
	updateFreeSizeInfo( no^1 );
	sendDoneMes( no , IPL_CARD_CMD_MOUNT , (u8)fileNo );
	sendDoneMes( (u8)( no^1 ), cmd, (u8)dstFileNo );

	return;

Error:
	if ( ( ret = CARDGetResultCode( no^1 ) ) < 0 ) {
		processError( (u8)(no^1), cmd, ret );
	} else {
		if ( dummyExist && !Done ) CARDFastDelete( no^1, dstFileNo );
		if ( Done ) {
			if (  __CARDGetStatusEx( (u8)( no^1 ), (s16)dstFileNo, &tempDir ) == 0 ) {
				getFileInfo( (u8)( no^1 ), dstFileNo, &tempDir  );
			}
		}
		updateFreeSizeInfo( no^1 );
		sendDoneMes( (u8)(no^1), IPL_CARD_CMD_MOUNT, (u8)fileNo );
	}
	if ( ( ret = CARDGetResultCode( no   ) ) < 0 ) {
		processError( no, cmd, ret );
	} else {
		updateFreeSizeInfo( no   );
		sendDoneMes( no, IPL_CARD_CMD_MOUNT, (u8)fileNo );
	}
}

/*-----------------------------------------------------------------------------
  フォーマットします。
  -----------------------------------------------------------------------------*/
static void
formatCard( u8 no )
{
	s32		ret, cmd;

	cmd = IPL_CARD_CMD_FORMAT;
	
	if ( ( ret = CARDFormat( no ) ) < 0 ) {
		processError( no, cmd, ret );
		return;
	}

	sMounted[ no ] = TRUE;

	updateFreeSizeInfo( no );
	sendDoneMes( no , IPL_CARD_CMD_FORMAT , 0 );
}

/*-----------------------------------------------------------------------------
  ファイルを削除します。
  -----------------------------------------------------------------------------*/
static void
deleteFile( u8 no , s16	fileNo )
{
	s32		ret, cmd;

	cmd = IPL_CARD_CMD_FILEDELETE;

//	DIReport("DEBUG:file delete %d : %d",no , fileNo);
	ret = CARDFastDelete( no, fileNo );
//	DIReport("DEBUG:result %d", ret );
	if ( ret < 0 ) {
		processError( no, cmd, ret );
		return;
	}
	invalidDirState( no, fileNo );

	updateFreeSizeInfo( no );
	sendDoneMes( no , IPL_CARD_CMD_FILEDELETE , (u8)fileNo );
}

/*-----------------------------------------------------------------------------
  指定のゲームコードのファイルのサイズを取得します。
  -----------------------------------------------------------------------------*/
static void
getGameCodeFileBlocks( void )
{
	int chan, fileNo, chan_blocks;
	s32	ret;
	CARDDir		tempDir;
    DVDDiskID* diskId;

	diskId = (DVDDiskID*) OSPhysicalToCached(OS_BOOTINFO_ADDR);
	for ( chan = 0 ; chan < 2 ; chan++ ) {
		if ( sMounted[ chan ] ) {
			chan_blocks = 0;	// そのチャンネルのサイズは0
			
			for ( fileNo = 0 ; fileNo < CARD_MAX_FILE ; fileNo++ ) {
				ret = __CARDGetStatusEx( chan, fileNo, &tempDir );
				if ( ret < 0 && ret != CARD_RESULT_NOFILE ) {
					chan_blocks = -1;
					goto Loop;
				}

				if ( ret == CARD_RESULT_READY ) {
					if ( memcmp(tempDir.gameName, diskId->gameName, sizeof(tempDir.gameName)) == 0 &&
						 memcmp(tempDir.company , diskId->company , sizeof(tempDir.company )) == 0 ) {
						chan_blocks += tempDir.length;
					}
				}
				
			}
			Loop:
			slot[chan].game_blocks = (s16)chan_blocks;
		}
	}
	sendDoneMes( 0 , IPL_CARD_CMD_GETCODESIZE , 0 );
}

