/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: Menu StartAnim関連ルーチン
  -----------------------------------------------------------------------------*/
#include <dolphin.h>

#include "gMath.h"
#include "gBase.h"
#include "gTrans.h"
#include "gCamera.h"
#include "gLoader.h"
#include "gCont.h"

#include "gModelRender.h"

#include "MenuUpdate.h"
#include "MenuStartAnim.h"
#include "StartState.h"

/*-----------------------------------------------------------------------------
  
  -----------------------------------------------------------------------------*/
extern DIPadStatus	DIPad;

/*-----------------------------------------------------------------------------
  statics
  -----------------------------------------------------------------------------*/
static gmrModelData scmd;
static gmrModel		scm[16];
static SCAnimState	sca[16];

static s16 now_frame;
static s16 rot_x, rot_y, rot_z;

static GXColorS10	cube_color;

static BOOL			sConcent;

static s16 bounds_x = 100;
static s16 bounds_y = 50;

/*-----------------------------------------------------------------------------
  CardCubeがアニメーションするために必要な設定
  -----------------------------------------------------------------------------*/
void
initSCAnim( gmrModelData* md , BOOL first )
{
	s16 i;

	if ( first ) {
		scm[0].gmrmd = md;
		buildModel( &scm[0] , FALSE );
		
		for ( i = 1 ; i < 16 ; i++ ) {
			copyModel( &scm[i], &scm[0] );
		}
	}
	
	for ( i = 0 ; i < 16 ; i++ ) {
		scm[i].modelAlpha = 0;
	}

	bounds_y = 50;
	bounds_x = 100;
	
	now_frame = 0;
	initSCAnimState();
}

/*-----------------------------------------------------------------------------
  CardCubeがアニメーションするための初期値の設定
  -----------------------------------------------------------------------------*/
void
initSCAnimState( void )
{
	s16	i;
	
	for ( i = 0 ; i < 16 ; i++ ) {
		sca[i].start_frame = (s16)( i*10 );
		sca[i].end_frame = (s16)( i*10 + 20 );
		
		sca[i].base_rot_z = (s16)(65536 * i / 16);
	}
	rot_x = 0;
	rot_y = 0;
	rot_z = 0;
}

void
updateSCAnime( void )
{
	s16 i;
	StartState*	ssp = getStartState();

	for (i = 0 ; i < 16 ; i++ )	{
		s16 sf,ef;
		sf = sca[i].start_frame;
		ef = sca[i].end_frame;

		if ( now_frame > sca[i].start_frame &&
			 now_frame < sca[i].end_frame ) {
			sca[i].radius = (s16)( ssp->rad * ( now_frame - sf ) / ( ef - sf ) );
			scm[i].modelAlpha = (s16)( 255 * ( now_frame - sf ) / ( ef - sf ) );
		}
		else if ( now_frame <= sca[i].start_frame ) {
			sca[i].radius = 0;
			scm[i].modelAlpha = 0;
		}
		else if ( now_frame >= sca[i].end_frame ) {
			sca[i].radius = (s16)ssp->rad;
			scm[i].modelAlpha = 255;
		}
		sca[i].x = sca[i].radius * gSSin( (s16)(sca[i].base_rot_z - rot_z) );
		sca[i].y = sca[i].radius * gSCos( (s16)(sca[i].base_rot_z - rot_z) );
	}

	now_frame++;
	rot_z += 4096 / 10;
	if ( now_frame > 300 ) {
		now_frame = 300;

			rot_x += bounds_x;
			rot_y += bounds_y;

			if ( rot_x > 32000 ) bounds_x = -100;
			if ( rot_x < 1000 ) bounds_x = 100;
			if ( rot_y > 8192 ) bounds_y = -50;
			if ( rot_y < -8192 ) bounds_y = 50;
		if ( rot_z > 4096 ) rot_z %= 4096;
	}
}

void
updateSCAnimeMtx( void )
{
	s16	i;
	Mtx currentMtx, r;
	StartState*	ssp = getStartState();
	
	for (i = 0 ; i < 16 ; i++ )	{
		MTXTrans(currentMtx,
				 sca[i].x,
				 sca[i].y,
				 0);
		gGetTranslateRotateMtx( 
								rot_x ,0,rot_y,0,0,0, r);
		MTXConcat( r, currentMtx, currentMtx );
		
		gGetTranslateRotateMtx(	-16384,0,0,0,0,0,r );
		MTXConcat( r, currentMtx, currentMtx);
		MTXConcat(  getMainMenuMtx() ,currentMtx, currentMtx );
		setBaseMtx( &scm[i] , currentMtx , (Vec){
			ssp->scale,
			ssp->scale,
			ssp->scale
				} );
		setViewMtx( &scm[i] , getCurrentCamera() );
		updateModel( &scm[i] );
	}
}

void
drawSCAnime( void )
{
	s16	i;
	GXColorS10*	backup;
	StartState*	ssp = getStartState();

	cube_color.r = (s16)ssp->cube_r;
	cube_color.g = (s16)ssp->cube_g;
	cube_color.b = (s16)ssp->cube_b;
	cube_color.a = (s16)ssp->cube_a;

	backup = scm[0].gmrmd->material[0].tevColorPtr[0];
	scm[0].gmrmd->material[0].tevColorPtr[0] = &cube_color;
	
	if ( now_frame < 300 ) {
		for ( i = 0 ; i < 16 ; i++ ) {
			if ( scm[i].modelAlpha > 0 ) {
				drawModel( &scm[ i ] );
			}
		}
	}
	else {
		for ( i = 0 ; i < 8 ; i++ ) {
			if ( scm[7-i].modelAlpha > 0 ) {
				drawModel( &scm[7-i] ); //scm[ (20 - i) % 16] );
			}
			if ( scm[8+i].modelAlpha > 0 ) {
				drawModel( &scm[8+i] );
			}
		}
	}
	scm[0].gmrmd->material[0].tevColorPtr[0] = backup;
}

void
clearSCAnime( void )
{
	int i;
	int	all_zero = 0;
	
	for ( i = 0 ; i < 16 ; i++ ) {
		scm[i].modelAlpha -= 10;
		if ( scm[i].modelAlpha < 0 ) {
			scm[i].modelAlpha = 0;
			all_zero ++;
		}
	}
	now_frame = 0;

	if (all_zero==16) {
		rot_x = 0;
		rot_y = 0;
		rot_z = 0;
	}
}

static s16
decAlphaforConcent( s16 *ap )
{
	s16	alpha = *ap;
	s16 ret = 0;
	StartState* stp = getStartState();
	
	alpha -= stp->concent_dec_alpha;
	if ( alpha < 0 ) {
		alpha = 0;
		ret = 1;
	}

	*ap = alpha;
	return ret;
}

static s16
decPosition( f32 *xp )
{
	f32	x = *xp;
	StartState* stp = getStartState();

	if ( x > 0 ) {
		x -= stp->concent_dec_pos;
		if ( x < 0 ) x = 0;
	} else if ( x < 0 ) {
		x += stp->concent_dec_pos;
		if ( x > 0 ) x = 0;
	}

	*xp = x;
	if ( x == 0 ) return 1;
	return 0;
}

void
concentSCAnime( void )
{
	s16	all_zero = 0, i;
	for ( i = 0 ; i < 16 ; i++ ) {
		decAlphaforConcent( &scm[i].modelAlpha );
		all_zero += decPosition( &sca[i].x );
		all_zero += decPosition( &sca[i].y );
	}

	if ( all_zero == 32 ) {
		sConcent = TRUE;
		rot_x = 0;
		rot_y = 0;
		rot_z = 0;
	} else {
		sConcent = FALSE;
	}
	now_frame = 0;
}

BOOL
isConcentSCSmallCube( void )
{
	s16	all_zero = 0, i;
	for ( i = 0 ; i < 16 ; i++ ) {
		if ( sca[i].x == 0 &&
			 sca[i].y == 0 ) {
			all_zero++;
		}
	}
	if ( all_zero == 16 ) {
		sConcent = TRUE;
	} else {
		sConcent = FALSE;
	}
	
	return sConcent;
}
