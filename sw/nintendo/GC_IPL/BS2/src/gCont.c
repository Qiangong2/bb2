#include <dolphin.h>
#include <math.h>
#include <string.h>
#include "gCont.h"

#include "gMainState.h"

/*-----------------------------------------------------------------------------
  グローバル変数
  -----------------------------------------------------------------------------*/
DIPadStatus         DIPad;//[PAD_MAX_CONTROLLERS];
u32                 DINumValidPads;

/*-----------------------------------------------------------------------------
  ローカル変数
  -----------------------------------------------------------------------------*/
static PADStatus    Pad[PAD_MAX_CONTROLLERS];
static PADStatus    PrePad[PAD_MAX_CONTROLLERS];
static PADStatus    AllPad;
static BOOL         sRepeating = FALSE;
static u32          sPremium = 0;

static u32 PadChanMask[PAD_MAX_CONTROLLERS] =
{
    PAD_CHAN0_BIT, PAD_CHAN1_BIT, PAD_CHAN2_BIT, PAD_CHAN3_BIT
};

/*-----------------------------------------------------------------------------

  -----------------------------------------------------------------------------*/
static void DIPadCopy( PADStatus* pad, DIPadStatus* dipad )
{
    u16  dirs;
    static u16 pre_dirs = 0;
    static int repeating = 0;
    MainState* mesp = getMainState();

    // Detects which direction is the stick(s) pointing.
    // This can be used when we want to use a stick as direction pad.
    dirs = 0;
    /*
    if ( pad->stickX    < - DI_STICK_THRESHOLD )        dirs |= DI_STICK_LEFT;
    if ( pad->stickX    >   DI_STICK_THRESHOLD )        dirs |= DI_STICK_RIGHT;
    if ( pad->stickY    < - DI_STICK_THRESHOLD )        dirs |= DI_STICK_DOWN;
    if ( pad->stickY    >   DI_STICK_THRESHOLD )        dirs |= DI_STICK_UP;
    if ( pad->substickX < - DI_STICK_THRESHOLD )        dirs |= DI_STICK_LEFT;
    if ( pad->substickX >   DI_STICK_THRESHOLD )        dirs |= DI_STICK_RIGHT;
    if ( pad->substickY < - DI_STICK_THRESHOLD )        dirs |= DI_STICK_DOWN;
    if ( pad->substickY >   DI_STICK_THRESHOLD )        dirs |= DI_STICK_UP;
      */

    if ( ( pad->button & PAD_BUTTON_LEFT  ) == PAD_BUTTON_LEFT  ) dirs |= DI_STICK_LEFT;
    if ( ( pad->button & PAD_BUTTON_RIGHT ) == PAD_BUTTON_RIGHT ) dirs |= DI_STICK_RIGHT;
    if ( ( pad->button & PAD_BUTTON_DOWN  ) == PAD_BUTTON_DOWN  ) dirs |= DI_STICK_DOWN;
    if ( ( pad->button & PAD_BUTTON_UP    ) == PAD_BUTTON_UP    ) dirs |= DI_STICK_UP;

    sRepeating = FALSE;
    if ( pre_dirs == dirs && dirs != 0) {
        repeating++;
        if ( repeating >= mesp->key_repeat_threshold ) {
            if ( repeating == mesp->key_repeat_threshold ) {
                dipad->dirs = 0;
                sRepeating = TRUE;
            }
            else if ( repeating > mesp->key_repeat_threshold + mesp->key_repeat_repeating ) {
                dipad->dirs = 0;
                sRepeating = TRUE;
                repeating -= mesp->key_repeat_repeating;
            }
        }
    }
    else {
        repeating = 0;
    }
//  DIReport ("%d\n", repeating);
    pre_dirs = dirs;

    // Get the direction newly detected / released
    dipad->dirsPressed  = PADButtonDown(dipad->dirs, dirs);
    dipad->dirsReleased = PADButtonUp  (dipad->dirs, dirs);
    dipad->dirs         = dirs;

    // Get DOWN/UP status of all buttons
    dipad->buttonDown = PADButtonDown(dipad->pst.button, pad->button);
    dipad->buttonUp   = PADButtonUp  (dipad->pst.button, pad->button);

    // Get delta of analogs
    dipad->stickDeltaX      = (s16)(pad->stickX     - dipad->pst.stickX);
    dipad->stickDeltaY      = (s16)(pad->stickY     - dipad->pst.stickY);
    dipad->substickDeltaX   = (s16)(pad->substickX  - dipad->pst.substickX);
    dipad->substickDeltaY   = (s16)(pad->substickY  - dipad->pst.substickY);

    // スティックの倒した値
    dipad->value = sqrtf( pad->stickX * pad->stickX + pad->stickY * pad->stickY );

    if ( dipad->value > 1.f )
    {
        dipad->value = 1.f;
    }

    if ( dipad->value > 0.0f )
    {
        dipad->angle = (s16)(32768.0f * atan2( pad->stickX, -pad->stickY) / 3.1415926);
    }

    if ( dipad->buttonDown & PAD_BUTTON_B ) {
        if ( dipad->buttonDown & PAD_BUTTON_A ) {
            dipad->buttonDown ^= PAD_BUTTON_A;
        }
        if ( dipad->buttonDown & PAD_BUTTON_MENU ) {
            dipad->buttonDown ^= PAD_BUTTON_MENU;
        }
    }

    // Copy current status into DIPadStatus field
    dipad->pst = *pad;
}

static void
convertStickToButton( PADStatus* pad )
{
    if ( pad->stickX    < - DI_STICK_THRESHOLD )        pad->button |= PAD_BUTTON_LEFT;
    if ( pad->stickX    >   DI_STICK_THRESHOLD )        pad->button |= PAD_BUTTON_RIGHT;
    if ( pad->stickY    < - DI_STICK_THRESHOLD )        pad->button |= PAD_BUTTON_DOWN;
    if ( pad->stickY    >   DI_STICK_THRESHOLD )        pad->button |= PAD_BUTTON_UP;
    if ( pad->substickX < - DI_STICK_THRESHOLD )        pad->button |= PAD_BUTTON_LEFT;
    if ( pad->substickX >   DI_STICK_THRESHOLD )        pad->button |= PAD_BUTTON_RIGHT;
    if ( pad->substickY < - DI_STICK_THRESHOLD )        pad->button |= PAD_BUTTON_DOWN;
    if ( pad->substickY >   DI_STICK_THRESHOLD )        pad->button |= PAD_BUTTON_UP;
}

/*-----------------------------------------------------------------------------

  -----------------------------------------------------------------------------*/
void DIPadRead( void )
{
    s32 i;
    u32 resetReq;

    // Read current PAD status
    PADRead(Pad);

    // Clamp analog inputs
    PADClamp(Pad);

    DINumValidPads = 0;
    resetReq = 0;

    memset( &AllPad, 0, sizeof( AllPad ) );
    for ( i = 0 ; i < PAD_MAX_CONTROLLERS ; i++ )
    {
        // Connection check
        if ( Pad[i].err == PAD_ERR_NONE )
        {
            ++DINumValidPads;
            convertStickToButton( &Pad[i] );
            AllPad.button       |= Pad[i].button;
            AllPad.stickX       = 0;//|= Pad[i].stickX;
            AllPad.stickY       = 0;//|= Pad[i].stickY;
            AllPad.substickX    = 0;//|= Pad[i].substickX;
            AllPad.substickY    = 0;//|= Pad[i].substickY;
            AllPad.triggerLeft  = 0;//|= Pad[i].triggerLeft;
            AllPad.triggerRight = 0;//|= Pad[i].triggerRight;

            PrePad[i] = Pad[i];
            PrePad[i].err = TRUE;
        }
        else if ( Pad[i].err == PAD_ERR_NO_CONTROLLER )
        {
            resetReq |= PadChanMask[i];
            PrePad[i].err = FALSE;
        }
        else {
            if ( PrePad[i].err ) {
                ++DINumValidPads;
                AllPad.button       |= PrePad[i].button;
                AllPad.stickX       = 0;//|= Pad[i].stickX;
                AllPad.stickY       = 0;//|= Pad[i].stickY;
                AllPad.substickX    = 0;//|= Pad[i].substickX;
                AllPad.substickY    = 0;//|= Pad[i].substickY;
                AllPad.triggerLeft  = 0;//|= Pad[i].triggerLeft;
                AllPad.triggerRight = 0;//|= Pad[i].triggerRight;
            }
        }
    }

    // Try resetting pad channels which have been not valid
    if ( resetReq )
    {
        PADReset(resetReq);
    }

    DIPadCopy(&AllPad, &DIPad);

    if ( ( Pad[0].err == PAD_ERR_NONE && ( Pad[0].button & PAD_TRIGGER_Z ) ) &&
         ( Pad[1].err == PAD_ERR_NONE && ( Pad[1].button & PAD_TRIGGER_Z ) ) &&
         ( Pad[2].err == PAD_ERR_NONE && ( Pad[2].button & PAD_TRIGGER_Z ) ) &&
         ( Pad[3].err == PAD_ERR_NONE && ( Pad[3].button & PAD_TRIGGER_Z ) ) ) {
        sPremium = 2;
    }
    else if ( Pad[0].err == PAD_ERR_NONE && ( Pad[0].button & PAD_TRIGGER_Z ) ) {
        sPremium = 1;
    }
    else {
        sPremium = 0;
    }


    return;
}

/*-----------------------------------------------------------------------------

  -----------------------------------------------------------------------------*/
void DIPadInit( void )
{
    static u32  init = 0;
    int i;

    // Initialize pad interface (first time only)
    if ( !init )
    {
        PADInit();
        ++init;
    }
    for ( i = 0 ; i < PAD_MAX_CONTROLLERS ; i++ ) {
        PrePad[i].err = FALSE;
    }

    // Reset exported pad status
    DIPad.pst.button = 0;
    DIPad.pst.stickX = 0;
    DIPad.pst.stickY = 0;
    DIPad.pst.substickX = 0;
    DIPad.pst.substickY = 0;
    DIPad.pst.triggerLeft = 0;
    DIPad.pst.triggerRight = 0;
    DIPad.pst.analogA = 0;
    DIPad.pst.analogB = 0;
    DIPad.pst.err = 0;
    DIPad.buttonDown = 0;
    DIPad.buttonUp = 0;
    DIPad.dirs = 0;
    DIPad.dirsPressed = 0;
    DIPad.dirsReleased = 0;
    DIPad.stickDeltaX = 0;
    DIPad.stickDeltaY = 0;
    DIPad.substickDeltaX = 0;
    DIPad.substickDeltaY = 0;

    DIPad.angle = 0;
    DIPad.value = 0.f;
}

BOOL
gIsPadRepeating( void )
{
    return sRepeating;
}

u32
gGetPremiumState( void )
{
    return sPremium;
}
