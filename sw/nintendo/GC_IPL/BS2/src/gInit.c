/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: System function

  初期化関連
  -----------------------------------------------------------------------------*/
#include <dolphin.h>
#include <private/OSRtc.h>
#include <string.h>
#include <private/viprivate.h>
#include <private/flipper.h>

#include "BS2Private.h"

#include "gBase.h"
#include "gInit.h"
#include "gLoader.h"
#include "gCont.h"
#include "Splash.h"
#include "CardSequence.h"
#include "SplashMain.h"

#ifndef IPL
#include "gLoader.h"
#include "IHIroot.h"
#endif

#include "jaudio.h"

/*-----------------------------------------------------------------------------
  static functions
  -----------------------------------------------------------------------------*/
static void initFrameBuffer( GXRenderModeObj* rmode );
static void initRenderMode( GXRenderModeObj* rmode );
static void initGX( u32 fifo_size , GXRenderModeObj* mode );
static void initViewport( GXRenderModeObj* mode );

/*-----------------------------------------------------------------------------
  staticモノ
  -----------------------------------------------------------------------------*/
static void*            sFrameBuffer1;
static void*            sFrameBuffer2;
static void*            sCurrentBuffer;
static GXRenderModeObj* rmode;

static void*            gxFifo;
static GXFifoObj*       gxFifoObj;

static OSTime           beginRenderTime , endRenderTime;
static f32              currentRenderTime;

static u8               garbage[640*2*VI_DISPLAY_PIX_SZ] ATTRIBUTE_ALIGN(32);

static BOOL             sInvalidSram;
static BOOL             sInvalidCounterBias;
static BOOL             sResetState = FALSE;

static s16              run_fade_alpha = 0;
static GXBool           firstFrame = GX_TRUE;

static BS2State         bs2_state;
static char*            sBannerBuffer;
static BOOL             sound_stop_precall = FALSE;

/*-----------------------------------------------------------------------------
  フレームバッファの確保
  -----------------------------------------------------------------------------*/
static void initFrameBuffer( GXRenderModeObj* mode )
{
    u32     fbSize;

    fbSize = VIPadFrameBufferWidth(mode->fbWidth) * mode->xfbHeight * (u32)VI_DISPLAY_PIX_SZ;

    sFrameBuffer1 = (void*)gAlloc( fbSize );
    sFrameBuffer2 = (void*)gAlloc( fbSize );
    sCurrentBuffer = sFrameBuffer2;

}
/*-----------------------------------------------------------------------------
  RenderModeの初期化
  -----------------------------------------------------------------------------*/
static void
initRenderMode( GXRenderModeObj* mode )
{

    if (mode != NULL) {
        rmode = mode;
    }
    else {
        switch (VIGetTvFormat())
        {
        case VI_NTSC:
            rmode = &GXNtsc480IntDf;
            break;
        case VI_PAL:
            rmode = &GXPal528IntDf;
            break;
        case VI_MPAL:
            rmode = &GXMpal480IntDf;
            break;
        default:
            OSHalt("DEMOInit: invalid TV format\n");
            break;
        }
    }

    VIConfigure( mode );
}

/*-----------------------------------------------------------------------------
  Viewportの設定など
  -----------------------------------------------------------------------------*/
void
initViewport(
        GXRenderModeObj* mode )
{

    GXSetViewport(0.0F, 0.0F, (f32)mode->fbWidth, (f32)mode->xfbHeight,
                  0.0F, 1.0F);
    GXSetDispCopySrc(0, 0, mode->fbWidth, mode->efbHeight);
    GXSetDispCopyDst( VIPadFrameBufferWidth( mode->fbWidth ), mode->xfbHeight);
    GXSetCopyFilter(mode->aa, mode->sample_pattern, GX_TRUE, mode->vfilter);

#ifndef IPL
    // screen capture
    GXSetTexCopySrc(0, 0, mode->fbWidth, mode->efbHeight);
    GXSetTexCopyDst( VIPadFrameBufferWidth( mode->fbWidth ), mode->xfbHeight, GX_TF_RGB565, GX_FALSE);

    IHIsetScreenSize( VIPadFrameBufferWidth( mode->fbWidth ), mode->xfbHeight );
#endif
    if ( mode->aa && mode->xfbHeight >= 440 ) {
        GXSetPixelFmt(GX_PF_RGB565_Z16, GX_ZC_LINEAR);
        GXSetDispCopyYScale( 1.0f );
        GXSetDither(GX_ENABLE);
        GXSetCopyClamp((GXFBClamp)(GX_CLAMP_TOP | GX_CLAMP_BOTTOM));
    }
    else {
        GXSetPixelFmt(GX_PF_RGB8_Z24, GX_ZC_LINEAR);
        GXSetDispCopyYScale((f32)(mode->xfbHeight) / (f32)(mode->efbHeight));
        GXSetScissor(0, 0, (u32)mode->fbWidth, (u32)mode->efbHeight);
    }
}


/*-----------------------------------------------------------------------------
  GXの初期化
  -----------------------------------------------------------------------------*/
void
initGX( u32 fifo_size ,
        GXRenderModeObj* mode )
{
    u32 align_fifo_size = OSRoundUp32B(fifo_size);
    gxFifo     = gAlloc( align_fifo_size );
    gxFifoObj  = GXInit( gxFifo, align_fifo_size );

    __GXSetIndirectMask(0);     // 5/8/2003 SDK bug workaround [by Okasaka@iRD]

    initViewport( mode );

    GXSetCopyClear( (GXColor){0,0,0,0xff} , GX_MAX_Z24 );
    GXCopyDisp(sFrameBuffer1, GX_TRUE); // efbをクリアします。
    GXCopyDisp(sFrameBuffer1, GX_TRUE); // efbをクリアします。

    GXCopyDisp(sFrameBuffer2, GX_TRUE); // efbをクリアします。
    GXCopyDisp(sFrameBuffer2, GX_TRUE); // efbをクリアします。

    // Gamma correction
    GXSetDispCopyGamma(GX_GM_1_0);

    VISetBlack( GX_TRUE );
    VISetNextFrameBuffer(sCurrentBuffer);
    VIFlush();

    VIWaitForRetrace();
}

/*-----------------------------------------------------------------------------
  GXの初期化など
  -----------------------------------------------------------------------------*/
void
gInitGX( u32 fifo_size ,
         GXRenderModeObj* mode )
{
    initFrameBuffer( mode );
    initRenderMode( mode );
    initGX( fifo_size , mode );
}


/*-----------------------------------------------------------------------------
  描画前処理
  -----------------------------------------------------------------------------*/
void
gBeginRender(void)
{
    beginRenderTime = OSGetTime();

    // Set up viewport
    if (rmode->field_rendering) {
        OSHalt("not support field rendering\n");
        GXSetViewportJitter(
          0.0F, 0.0F, (float)rmode->fbWidth, (float)rmode->xfbHeight,
          0.0F, 1.0F, VIGetNextField());
    }
    else {
        GXSetViewport(
          0.0F, 0.0F, (float)rmode->fbWidth + .5f, (float)rmode->xfbHeight + .5f,
          0.0F, 1.0F);
    }

    GXInvalidateVtxCache();
    GXInvalidateTexAll();

    if ( rmode->aa && rmode->xfbHeight >= 440 ) {
        GXSetScissor(0, 0, rmode->fbWidth , (u32)(rmode->efbHeight ) );
        GXSetScissorBoxOffset(0, 0);
    }
}

/*-----------------------------------------------------------------------------
  下半分描画前処理
  -----------------------------------------------------------------------------*/
void
gPreBottomRender( void )
{
    if ( rmode->aa && rmode->xfbHeight >= 440 ) {
        GXSetScissor(0, (u32)(rmode->efbHeight -4),
                     (u32)(rmode->fbWidth), (u32)(rmode->efbHeight ));
        GXSetScissorBoxOffset(0, (s32)(rmode->efbHeight -4));
    }
}

/*-----------------------------------------------------------------------------
  上半分コピー処理
  -----------------------------------------------------------------------------*/
void
gCopyDispTop( void )
{
    GXSetZMode(GX_TRUE, GX_LEQUAL, GX_TRUE);
    GXSetColorUpdate(GX_TRUE);

#ifndef IPL
    // screen capture
    IHIcopyTopScreen();
#endif

    GXCopyDisp( sCurrentBuffer, GX_TRUE);
}

/*-----------------------------------------------------------------------------
  下半分コピー処理
  -----------------------------------------------------------------------------*/
void
gCopyDispBottom( void )
{
    u16 copyLines;
    u32 bufferOffset;

    copyLines    = (u16) (rmode->efbHeight - COPY_OVERLAP);
    bufferOffset = VIPadFrameBufferWidth(rmode->fbWidth) * copyLines * (u32) VI_DISPLAY_PIX_SZ;

    GXSetZMode(GX_TRUE, GX_LEQUAL, GX_TRUE);
    GXSetColorUpdate(GX_TRUE);

    GXSetCopyClamp(GX_CLAMP_BOTTOM); // not TOP
    GXSetDispCopySrc(0, COPY_OVERLAP, rmode->fbWidth, copyLines);

#ifndef IPL
    // screen capture
    GXSetTexCopySrc(0, COPY_OVERLAP, rmode->fbWidth, copyLines);
    IHIcopyBottomScreen( bufferOffset );
    GXSetTexCopySrc(0, 0, rmode->fbWidth, rmode->efbHeight);
#endif

    GXCopyDisp((void *)((u32)sCurrentBuffer+bufferOffset), GX_TRUE);

    GXSetCopyClamp((GXFBClamp)(GX_CLAMP_TOP | GX_CLAMP_BOTTOM));
    GXSetDispCopySrc(0, 0, rmode->fbWidth, COPY_OVERLAP);

    GXCopyDisp((void *)garbage, GX_TRUE);

    GXSetDispCopySrc(0, 0, rmode->fbWidth, rmode->efbHeight);
}

/*-----------------------------------------------------------------------------

  -----------------------------------------------------------------------------*/
void
gDoneRender( void )
{
    GXDrawDone();

#ifndef IPL
    IHIdoWriteFile();
#endif
    endRenderTime = OSGetTime();
    currentRenderTime = (f32)OSTicksToMicroseconds( endRenderTime - beginRenderTime ) / 1000.0f;

    VISetNextFrameBuffer(sCurrentBuffer);

    if ( firstFrame ) {
        VISetBlack(FALSE);
        firstFrame = GX_FALSE;
    }

    VIFlush();
    VIWaitForRetrace();
    // Swap buffers
    if(sCurrentBuffer == sFrameBuffer1)
        sCurrentBuffer = sFrameBuffer2;
    else
        sCurrentBuffer = sFrameBuffer1;
}

/*-----------------------------------------------------------------------------
  処理バーを描画します。
  ハイレゾアンチの場合は、下の時だけ描画したら良いです。
  -----------------------------------------------------------------------------*/
void
gDrawProcMeter( void )
{
    Mtx viewMatrix;
    Mtx44 projMatrix;
    Vec camPos, camUp,target;

    MTXOrtho( projMatrix,
        0.0f,  448.0f,
        0.0f,  584.0f,
        10.0f, 1000.0f  );
    GXSetProjection(projMatrix, GX_ORTHOGRAPHIC);
    camPos =  (Vec){0.0f,0.0f,100.0f};
    camUp  =  (Vec){0.0f,1.0f,0.0f};
    target =  (Vec){0.0f,0.0f,0.0f};

    MTXLookAt(viewMatrix ,
              &camPos,
              &camUp,
              &target
              );
//   MTXIdentity( viewMatrix );
    GXLoadPosMtxImm(viewMatrix, GX_PNMTX0);
//    GXLoadTexMtxImm(viewMatrix, GX_IDENTITY, GX_MTX3x4);
    GXSetCurrentMtx(GX_PNMTX0);
    GXClearVtxDesc();
    GXSetVtxDesc( GX_VA_POS     , GX_DIRECT );
    GXSetVtxDesc( GX_VA_CLR0    , GX_DIRECT );

    GXSetVtxAttrFmt(GX_VTXFMT1, GX_VA_POS , GX_POS_XYZ , GX_S16  , 0);
    GXSetVtxAttrFmt(GX_VTXFMT1, GX_VA_CLR0, GX_CLR_RGBA, GX_RGBA8, 0);

    GXSetNumChans(1); // default, color = vertex color
    GXSetChanCtrl( GX_COLOR0A0,
                   GX_DISABLE,
                   GX_SRC_VTX,
                   GX_SRC_VTX,
                   GX_NONE,
                   GX_DF_CLAMP,
                   GX_AF_NONE);
    GXSetNumTexGens(0);
    GXSetNumTevStages(1);
    GXSetTevOrder(GX_TEVSTAGE0, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR0A0);
//    GXSetTevOrder(GX_TEVSTAGE0, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
    GXSetTevOrder(GX_TEVSTAGE1, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
    GXSetTevOrder(GX_TEVSTAGE2, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
    GXSetTevOrder(GX_TEVSTAGE3, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
    GXSetTevOrder(GX_TEVSTAGE4, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
    GXSetTevOrder(GX_TEVSTAGE5, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
    GXSetTevOrder(GX_TEVSTAGE6, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
    GXSetTevOrder(GX_TEVSTAGE7, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
    GXSetTevOp(GX_TEVSTAGE0, GX_PASSCLR);

    GXSetAlphaUpdate(GX_TRUE);
    GXSetBlendMode( GX_BM_BLEND , GX_BL_SRCALPHA, GX_BL_INVSRCALPHA, GX_LO_CLEAR );

    GXSetCullMode( GX_CULL_NONE );
    GXSetLineWidth(12,GX_TO_ONE);
    GXBegin( GX_LINES , GX_VTXFMT1 ,8);
        GXPosition3s16( 10    ,420- 5 ,0); GXColor4u8( 0xff,0xff,0xff,0xff);
        GXPosition3s16( 10    ,420+ 5 ,0); GXColor4u8( 0xff,0xff,0xff,0xff);
        GXPosition3s16( 10+100,420- 5 ,0); GXColor4u8( 0xff,0xff,0xff,0xff);
        GXPosition3s16( 10+100,420+ 5 ,0); GXColor4u8( 0xff,0xff,0xff,0xff);

        GXPosition3s16( 10+200,420- 5 ,0); GXColor4u8( 0xff,0xff,0xff,0xff);
        GXPosition3s16( 10+200,420+ 5 ,0); GXColor4u8( 0xff,0xff,0xff,0xff);
        GXPosition3s16( 10+300,420- 5 ,0); GXColor4u8( 0xff,0xff,0xff,0xff);
        GXPosition3s16( 10+300,420+ 5 ,0); GXColor4u8( 0xff,0xff,0xff,0xff);
    GXEnd();

    GXSetLineWidth(30,GX_TO_ONE);
    GXSetVtxAttrFmt(GX_VTXFMT1, GX_VA_POS , GX_POS_XYZ , GX_F32  , 0);
    GXBegin( GX_LINES , GX_VTXFMT1 ,2);
        GXPosition3f32( 10.0f                               , 420.0f, 0.0f); GXColor4u8( 0x80,0x80,0xff,0xff);
        GXPosition3f32( 10.0f+(currentRenderTime*100/16.6f) , 420.0f, 0.0f); GXColor4u8( 0x80,0x80,0xff,0xff);
    GXEnd();
}

/*-----------------------------------------------------------------------------
  直接描画用のGXの設定をします。
  -----------------------------------------------------------------------------*/
void
gSetGXforDirect( int enable_texture, int enable_vtxcolor, int enable_texture_color )
{
    u16     tChanCtrl = GX_SRC_REG;

    if ( !enable_texture && !enable_vtxcolor ) return;

    GXClearVtxDesc();

    GXSetVtxDesc( GX_VA_POS     , GX_DIRECT );
    GXSetVtxAttrFmt(GX_VTXFMT1, GX_VA_POS , GX_POS_XYZ , GX_S16 , 4);

    if ( enable_vtxcolor ) {
        GXSetVtxDesc( GX_VA_CLR0    , GX_DIRECT );
        GXSetVtxAttrFmt(GX_VTXFMT1, GX_VA_CLR0, GX_CLR_RGBA, GX_RGBA8,0);

        tChanCtrl = GX_SRC_VTX;
    }
    if ( enable_texture ) {
        GXSetVtxDesc( GX_VA_TEX0    , GX_DIRECT );
        GXSetVtxAttrFmt(GX_VTXFMT1, GX_VA_TEX0, GX_TEX_ST  , GX_S16 , 14);
    }

    GXSetNumChans(1); // default, color = vertex color
    GXSetChanCtrl( GX_COLOR0A0,
                   GX_DISABLE,
                   (GXColorSrc)tChanCtrl,
                   (GXColorSrc)tChanCtrl,
                   GX_NONE,
                   GX_DF_CLAMP,
                   GX_AF_NONE);

    if ( enable_texture ) {
        GXSetNumTexGens( 1 );
        GXSetTexCoordGen( GX_TEXCOORD0, GX_TG_MTX2x4, GX_TG_TEX0, GX_IDENTITY );

        GXSetNumTevStages(1);
        GXSetTevOrder(GX_TEVSTAGE0, GX_TEXCOORD0, GX_TEXMAP0, GX_COLOR0A0);
        GXSetTevOrder(GX_TEVSTAGE1, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
        GXSetTevOrder(GX_TEVSTAGE2, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
        GXSetTevOrder(GX_TEVSTAGE3, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
        GXSetTevOrder(GX_TEVSTAGE4, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
        GXSetTevOrder(GX_TEVSTAGE5, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
        GXSetTevOrder(GX_TEVSTAGE6, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
        GXSetTevOrder(GX_TEVSTAGE7, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);

        if ( enable_texture_color ) {
            GXSetTevColorIn( GX_TEVSTAGE0, GX_CC_ZERO, GX_CC_TEXC,  GX_CC_RASC, GX_CC_ZERO );
        }
        else {
            GXSetTevColorIn( GX_TEVSTAGE0, GX_CC_ZERO, GX_CC_ONE,  GX_CC_RASC, GX_CC_ZERO );
        }

        GXSetTevColorOp( GX_TEVSTAGE0, GX_TEV_ADD, GX_TB_ZERO, GX_CS_SCALE_1, GX_TRUE, GX_TEVPREV);
        GXSetTevAlphaIn( GX_TEVSTAGE0, GX_CA_ZERO, GX_CA_TEXA, GX_CA_RASA, GX_CA_ZERO );
        GXSetTevAlphaOp( GX_TEVSTAGE0, GX_TEV_ADD, GX_TB_ZERO, GX_CS_SCALE_1, GX_TRUE, GX_TEVPREV);
    }
    else {
        GXSetNumTexGens(0);

        GXSetNumTevStages(1);
        GXSetTevOrder(GX_TEVSTAGE0, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR0A0);
        GXSetTevOrder(GX_TEVSTAGE1, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
        GXSetTevOrder(GX_TEVSTAGE2, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
        GXSetTevOrder(GX_TEVSTAGE3, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
        GXSetTevOrder(GX_TEVSTAGE4, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
        GXSetTevOrder(GX_TEVSTAGE5, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
        GXSetTevOrder(GX_TEVSTAGE6, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
        GXSetTevOrder(GX_TEVSTAGE7, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);

        GXSetTevColorIn( GX_TEVSTAGE0, GX_CC_ZERO, GX_CC_ONE,  GX_CC_RASC, GX_CC_ZERO );
        GXSetTevColorOp( GX_TEVSTAGE0, GX_TEV_ADD, GX_TB_ZERO, GX_CS_SCALE_1, GX_TRUE, GX_TEVPREV);
        GXSetTevAlphaIn( GX_TEVSTAGE0, GX_CA_ZERO, GX_CA_ONE, GX_CA_RASA, GX_CA_ZERO );
        GXSetTevAlphaOp( GX_TEVSTAGE0, GX_TEV_ADD, GX_TB_ZERO, GX_CS_SCALE_1, GX_TRUE, GX_TEVPREV);
    }

    GXSetColorUpdate(GX_TRUE);
    GXSetAlphaUpdate(GX_TRUE);
    GXSetBlendMode( GX_BM_BLEND , GX_BL_SRCALPHA, GX_BL_INVSRCALPHA, GX_LO_CLEAR );
    GXSetZMode( GX_FALSE, GX_LEQUAL ,GX_FALSE);

    GXSetCullMode( GX_CULL_BACK );
}

/*-----------------------------------------------------------------------------
  与えられたalphaで、全画面を覆うポリゴンを書きます。
  -----------------------------------------------------------------------------*/
void
gDrawFade( u8 alpha )
{
    Mtx     cameraMtx;
    Vec     up      = {   0.f,   1.f,   0.f };
    Vec     camLoc  = {   0.f,   0.f, 1700.f };
    Vec     objPt   = {   0.f,   0.f,   0.f };
    u32 color = (u32)(0x000000 | alpha);

    MTXLookAt( cameraMtx, &camLoc, &up, &objPt);
    GXLoadPosMtxImm( cameraMtx, GX_PNMTX0);

    GXSetCurrentMtx(GX_PNMTX0);
    GXClearVtxDesc();
    GXSetVtxDesc( GX_VA_POS     , GX_DIRECT );
    GXSetVtxDesc( GX_VA_CLR0    , GX_DIRECT );

    GXSetVtxAttrFmt(GX_VTXFMT1, GX_VA_POS , GX_POS_XYZ , GX_S16  , 0);
    GXSetVtxAttrFmt(GX_VTXFMT1, GX_VA_CLR0, GX_CLR_RGBA, GX_RGBA8, 0);

    GXSetNumChans(1); // default, color = vertex color
    GXSetChanCtrl( GX_COLOR0A0,
                   GX_DISABLE,
                   GX_SRC_VTX,
                   GX_SRC_VTX,
                   GX_NONE,
                   GX_DF_CLAMP,
                   GX_AF_NONE);
    GXSetNumTexGens(0);
    GXSetNumTevStages(1);
    GXSetTevOrder(GX_TEVSTAGE0, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR0A0);
    GXSetTevOrder(GX_TEVSTAGE1, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
    GXSetTevOrder(GX_TEVSTAGE2, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
    GXSetTevOrder(GX_TEVSTAGE3, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
    GXSetTevOrder(GX_TEVSTAGE4, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
    GXSetTevOrder(GX_TEVSTAGE5, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
    GXSetTevOrder(GX_TEVSTAGE6, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
    GXSetTevOrder(GX_TEVSTAGE7, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR_NULL);
    GXSetTevOp(GX_TEVSTAGE0, GX_PASSCLR);

    GXSetAlphaUpdate(GX_TRUE);
    GXSetColorUpdate(GX_TRUE);
    GXSetZCompLoc( GX_FALSE );
    GXSetBlendMode( GX_BM_BLEND , GX_BL_SRCALPHA, GX_BL_INVSRCALPHA, GX_LO_CLEAR );
    GXSetZMode( GX_FALSE, GX_ALWAYS, GX_FALSE );

    GXSetCullMode( GX_CULL_NONE );

    GXBegin( GX_QUADS , GX_VTXFMT1 ,4);
        GXPosition3s16( -400  ,  400 ,10 ); GXColor1u32( color );
        GXPosition3s16(  400  ,  400 ,10 ); GXColor1u32( color );
        GXPosition3s16(  400  , -400 ,10 ); GXColor1u32( color );
        GXPosition3s16( -400  , -400 ,10 ); GXColor1u32( color );
    GXEnd();
}

/*-----------------------------------------------------------------------------
  起動時のSRAMをチェックします。
  BS2.cで仕込んだ後、gGetInvalid*を呼び出して使用します。
  -----------------------------------------------------------------------------*/
/*
static u8
getCountryCode( void )
{
    u16     country;
    u8      country_code;

#if defined(MPAL)
#pragma unused ( country )
    country_code = OS_LANG_US_ENGLISH;  // MPAL ポルトガル語
#elif defined(PAL)

#else
    country = (u16) (__VIRegs[VI_DTVSTATUS] & 0x02);
    if ( country == COUNTRY_JAPAN ) {
        country_code = OS_LANG_JAPANESE;
    }
    else {
        country_code = OS_LANG_US_ENGLISH;
    }
#endif

    return country_code;
}*/

static void
setSramDefault( OSSram* sram )
{
    sram->flags |= OS_SRAM_INIT_FLAG;
    sram->flags |= OS_SRAM_SOUND_MODE;  // デフォルトステレオ。
//  sram->language = getCountryCode();
}

static void
initSram( OSSram* sram )
{
    u32     counterBias;
    u32     rtc;
    OSCalendarTime  ct;

    counterBias = sram->counterBias;

    if ( __OSGetRTC( &rtc ) ) {
        OSTicksToCalendarTime( OSSecondsToTicks( (OSTime)( rtc + counterBias ) ), &ct  );
        if ( ct.year < 2000 || ct.year > 2007 ) {
            counterBias = 0;
        }
    }
    else {
        counterBias = 0;
    }

    memset( sram, 0, sizeof( OSSram ) );
    sram->counterBias = counterBias;

    setSramDefault( sram );
    sInvalidSram = TRUE;
}
/*
static BOOL
checkSramCountry( OSSram* sram )
{
    BOOL    ret = FALSE;

    if ( sram->language != getCountryCode() )
    {
        setSramDefault( sram );
        sInvalidSram = TRUE;
        ret = TRUE;
    }

    return ret;
}*/

static void
checkCounterBias( OSSram* sram )
{
    if ( ( sram->flags & OS_SRAM_BIAS_FLAG ) == 0 ) {
        sInvalidCounterBias = TRUE;
    }
}

void
gCheckSram( void )
{
    OSSram* sram;
    BOOL    saveSram = FALSE;

    sInvalidSram = FALSE;
    sInvalidCounterBias = FALSE;

    sram = __OSLockSram();

    ASSERT(sram);
    if (!__OSCheckSram(sram)) {
        initSram( sram );
        saveSram = TRUE;
    }
    /*
    else if ( checkSramCountry( sram ) ) {
        saveSram = TRUE;
    }*/

    checkCounterBias( sram );

    __OSUnlockSram(saveSram);
    if (saveSram) {
        while (!__OSSyncSram()) {}
    }
}

void
gSetTimeBaseReg( void )
{
    OSSram*  sram;
    u32      rtc;

    sram = __OSLockSram();
    ASSERT(sram);

    if ( __OSGetRTC(&rtc) ) {
        rtc += sram->counterBias;
        __OSSetTime(OSSecondsToTicks((OSTime) rtc));
    }

    __OSUnlockSram( FALSE );
//  while (!__OSSyncSram()) ;
}

BOOL
gGetInvalidSram( void )
{
    return sInvalidSram;
}

BOOL
gGetInvalidCounterBias( void )
{
    return sInvalidCounterBias;
}

/*-----------------------------------------------------------------------------
  BS2tickを実行し、ステートを保存します。
  -----------------------------------------------------------------------------*/
#ifndef IPL
static BS2State
getDummyBs2State( void )
{
    static BS2State state = BS2_RUN_APP;
    static BOOL trigger = TRUE;
    SplashState* ssp = inSplashGetState();
    extern DIPadStatus  DIPad;

    if ( DIPad.buttonDown & PAD_TRIGGER_R ) {
        Jac_PlaySe( JAC_SE_DECIDE );
        switch ( state ) {
        case BS2_RUN_APP:
            state = BS2_NO_DISK;
            break;
        case BS2_NO_DISK:
//          state = BS2_FATAL_ERROR;
//          state = BS2_FATAL_ERROR;
            state = BS2_WRONG_DISK;
            break;
        case BS2_FATAL_ERROR:
            state = BS2_WRONG_DISK;
            ssp->force_fatalerror = TRUE;
            break;
        case BS2_WRONG_DISK:
            state = BS2_RETRY;
            break;
        case BS2_RETRY:
            state = BS2_RUN_APP;
            break;
        }
    }

    if ( DIPad.buttonDown & PAD_TRIGGER_L ) {
        if ( trigger ) {
            state = BS2_FATAL_ERROR;
            trigger = FALSE;
        }
        else {
            state = BS2_RUN_APP;
            ssp->force_fatalerror = FALSE;
            trigger = TRUE;
        }
    }

    return state;
}
#endif

void
gTickBs2( void )
{
#ifdef IPL
    bs2_state = BS2Tick();
#else
    bs2_state = getDummyBs2State();
#endif
}

BS2State
gGetBs2State( void )
{
    return bs2_state;
}

void
gRunAppFadeinSetup( void )
{
    run_fade_alpha = 255;
}

void
gRunAppFadein( void )
{
    if ( !sResetState ) {
        run_fade_alpha -= 255 / 40;
        if ( run_fade_alpha < 0 ) {
            run_fade_alpha = 0;
        }
    }
}

u8
gGetRunAppFadeAlpha( void )
{
    return (u8)(run_fade_alpha);
}

static void
gResetController( void )
{
    u32 mask;
    PADStatus temppad[ PAD_MAX_CONTROLLERS ];

    PADRead( temppad );

    mask = 0;
    if ( temppad[0].err != PAD_ERR_NO_CONTROLLER ) mask |= PAD_CHAN0_BIT;
    if ( temppad[1].err != PAD_ERR_NO_CONTROLLER ) mask |= PAD_CHAN1_BIT;
    if ( temppad[2].err != PAD_ERR_NO_CONTROLLER ) mask |= PAD_CHAN2_BIT;
    if ( temppad[3].err != PAD_ERR_NO_CONTROLLER ) mask |= PAD_CHAN3_BIT;

    PADRecalibrate( mask );
}

static BOOL
gStopSoundThread( void )
{
    static BOOL stoped = FALSE;

    if ( !sound_stop_precall ) {
        Jac_StopSoundAll();
        Jac_Freeze_Precall();
        sound_stop_precall = TRUE;
    }

    if ( !stoped ) {
        if ( Jac_Silence_Check() ) {
            Jac_Freeze();
            stoped = TRUE;
        }
    }

    return stoped;

//  __OSStopAudioSystem();
}

void
gRunAppUpdate( void )
{
    run_fade_alpha += 255 / 20;
    if ( run_fade_alpha > 255 ) {
        run_fade_alpha = 255;
        while ( !gStopSoundThread() ) {}
        GXDrawDone();
        VISetBlack( TRUE );
        VIFlush();
        VIWaitForRetrace();
#ifdef IPL
        while (!__OSSyncSram()) {}
        GXFlush();
        GXAbortFrame();
        BS2StartGame();
#endif
    }
}


void
gRunAppUpdateSlow( void )
{
    run_fade_alpha += 255 / 80;
    if ( run_fade_alpha > 255 ) {
        run_fade_alpha = 255;
        while ( !gStopSoundThread() ) {}
        GXDrawDone();
        VISetBlack( TRUE );
        VIFlush();
        VIWaitForRetrace();
#ifdef IPL
        while (!__OSSyncSram()) {}
        GXFlush();
        GXAbortFrame();
        BS2StartGame();
#endif
    }
}

void
gRunAppDraw( void )
{
    if ( run_fade_alpha > 0 ) {
        gDrawFade( (u8)run_fade_alpha );
    }
}

/*-----------------------------------------------------------------------------
  リセットスイッチ関連
  -----------------------------------------------------------------------------*/
static void
gResetVIAdjusting( void )
{
    OSSram      *sram;
    s16         offset;

    sram = __OSLockSram();
    offset = sram->displayOffsetH;
    __OSUnlockSram(FALSE);

    __VISetAdjustingValues( offset, 0 );
}

void
gWaitForConfirmCoverState( void )
{
    do {
        gTickBs2();
        VIWaitForRetrace();
    } while ( ( gGetBs2State() != BS2_COVER_OPEN      ) &&
              ( !( gGetBs2State() >= BS2_CONFIRMED_COVER_CLOSED && gGetBs2State() <= BS2_RUN_APP ) ) &&
              ( gGetBs2State() != BS2_NO_DISK         ) &&
              ( gGetBs2State() != BS2_STOP_MOTOR      ) &&
              ( gGetBs2State() != BS2_WAIT_STOP_MOTOR ) &&
              ( gGetBs2State() != BS2_WRONG_DISK      ) &&
              ( gGetBs2State() != BS2_FATAL_ERROR     ) &&
              ( gGetBs2State() != BS2_RETRY           )
              );
}

BOOL
gCheckReset( void )
{
    static BOOL pre_reset_state = FALSE;
    BOOL now_reset_state;
    int i;
    SplashState* ssp = inSplashGetState();

    now_reset_state = OSGetResetSwitchState();
//  OSReport("%d\n", now_reset_state);

    if ( sResetState ) {
        if ( getCardThreadState() != 1 ) {
            run_fade_alpha += 255 / 30;
            if ( run_fade_alpha > 255 ) {
                sResetState = FALSE;
                run_fade_alpha = 0;

                Jac_StopSoundAll();
                while ( !Jac_Silence_Check() ) {
                }
                for ( i=0 ; i < 10 ; i++  ) {
                    VIWaitForRetrace(); // サウンドリセット用のウェイト。
                }

                gResetVIAdjusting();
                gCheckSram();
                sendCardThreadStartCmd();
                gResetController();
#ifdef IPL
                BS2RestartStateMachine();
                gWaitForConfirmCoverState();
#endif
                SplashInit( FALSE );
                goto Reset;
            }
        }
    } else {
        if ( pre_reset_state && !now_reset_state &&
             !ssp->force_fatalerror ) {
            // リセットする。
            sResetState = TRUE;
            if ( getCardThreadState() == 1 ) {
                DIReport("send card thread stop cmd\n");
                sendCardThreadStopCmd();
            }
        }
    }
    pre_reset_state = now_reset_state;
    return TRUE;

Reset:
    pre_reset_state = FALSE;
    return TRUE;
}


/*-----------------------------------------------------------------------------
  バナー関連の関数。
  -----------------------------------------------------------------------------*/
void
gInitBannerBuffer( void )
{
#ifdef IPL
    sBannerBuffer = gAlloc( IPL_BANNER_SIZE );

    memset( sBannerBuffer, 0, IPL_BANNER_SIZE );

    BS2SetBannerBuffer( sBannerBuffer, IPL_BANNER_SIZE );
#else
//  sBannerBuffer = gGetBufferPtr( GC_bana_mj_bnr );
    DVDFileInfo     dfi;
    sBannerBuffer = gAlloc( IPL_BANNER_SIZE );

    memset( sBannerBuffer, 0, IPL_BANNER_SIZE );
    if ( DVDOpen( "/opening.bnr", &dfi ) ) {
        DVDRead( &dfi,
                 sBannerBuffer,
                 (s32)DVDGetLength( &dfi ),
                 0 );
        DVDClose( &dfi );
    }
#endif
}

BOOL
gIsBanner( void )
{
    if ( bs2_state == BS2_RUN_APP ) {
#ifdef IPL
        if ( BS2IsBannerAvailable() ) {
#endif
            if ( *((u32*)sBannerBuffer) == 'BNR1' ) return TRUE;
            if ( *((u32*)sBannerBuffer) == 'BNR2' ) return TRUE;
#ifdef IPL
        }
#endif
    }
    return FALSE;
}

BOOL
gGetBannerExInfo( u32* support_sector, u32* need_size )
{
    if ( !gIsBanner() ) return FALSE;

    if ( *((u32*)sBannerBuffer + 1) == 'MCS1' ) {
        *support_sector = *((u32*)sBannerBuffer + 2);
        *need_size      = *((u32*)sBannerBuffer + 3);

        return TRUE;
    }

    return FALSE;
}

BOOL
gGetBannerExInfoClockAdjust( void )
{
    if ( !gIsBanner() ) return FALSE;

    if ( *((u32*)sBannerBuffer + 4) == 'CLAD' ) {
        return TRUE;
    }

    return FALSE;
}

char*
gGetBannerBuffer( void )
{
    return sBannerBuffer;
}

u32
gGetBannerType( void )
{
    if ( *((u32*)sBannerBuffer) == 'BNR1' ) return cIPL_BannerType_OneLanguage;
    if ( *((u32*)sBannerBuffer) == 'BNR2' ) return cIPL_BannerType_SixLanguage;

    return cIPL_BannerType_Disable;
}


/*-----------------------------------------------------------------------------
  SRAMの中を見て、メニューに強制的に移るかどうかのチェック。
  -----------------------------------------------------------------------------*/
BOOL
gGetForceMenu( void )
{
    OSSram      *sram;
    BOOL        ret = FALSE , commit = FALSE;

    sram = __OSLockSram();
    if ( ( sram->flags & OS_SRAM_FORCEMENU_FLAG ) == OS_SRAM_FORCEMENU_FLAG ) {
        sram->flags ^= OS_SRAM_FORCEMENU_FLAG;
        ret = TRUE;
        commit = TRUE;
    }

    __OSUnlockSram( commit );

    if ( commit ) {
        while ( !__OSSyncSram() ) {}
    }

    return ret;
}
