/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: Dynamics関連ルーチン群
  -----------------------------------------------------------------------------*/
#include <dolphin.h>

#include <math.h>

#include "gDynamic.h"
#include "gMath.h"

/*-----------------------------------------------------------------------------
  ばね構造体の初期化
  -----------------------------------------------------------------------------*/
void
gInitSpringX( gSpringX* s )
{
	s->x = 0.f;
	s->v = 0.f;
	s->prev_v = 0.f;
	s->k = 0.01f;
	s->sf = 1.f;
	s->mf = 0.5f;
//	s->dev = 0.8f;
	s->frame = 0;
}

/*-----------------------------------------------------------------------------
  ばねの動きを更新する。
  -----------------------------------------------------------------------------*/
void
gUpdateSpringX( gSpringX* s , f32 ef )
{
	f32 flic , flicbase;
	f32 f, v, prev_v;

	v = s->v;
	prev_v = s->prev_v;
	
	if ( fabs( v ) <= 0.1f ||
		 ( prev_v > 0.f && v < 0.f ) ||
		 ( prev_v < 0.f && v > 0.f ) ) {
		// 速度が十分遅ければ、静止摩擦係数。
		flicbase = s->sf;
	}
	else {
		// 動摩擦係数。
		flicbase = s->mf;
	}

	if ( v > 0.f ) { // 速度の方向と逆向きに力がかかる。
		flic = -1 * flicbase;
	}
	else {
		flic = flicbase;
	}

	if ( flicbase > fabs( s->k * s->x - ef ) ) {
		// 摩擦力よりも力が小さいとかかる力は無し。（慣性で動く）
		f = 0.f;

		if ( flicbase == s->sf ) {
			// 静止摩擦力がかかっていたら、止まる。
			v = 0.f;
			if ( s->amp == 0.f ) {
				s->amp = s->amp = s->x;
				s->frame = 0;
			}
			if ( ef == 0.0f ) {
				// 外力がかかっていなかったら、原点に収束する。
				s->x = s->amp * ( 0.5f + 0.5f * gSCos( s->frame ) );
				s->frame += s->d_frame;
				if (  s->frame < 0 ) s->x = 0.f;
			}
		}
	}
	else {
		// 普通に力がかかる。（ばねの力は逆向きなので、マイナス）
		f = flic + ef - s->k * s->x;
		s->amp = 0.f;
	}

	prev_v = v;

	v = v + f;
	s->x = s->x + v;

	s->v = v;
	s->prev_v = prev_v;
}
