/*-----------------------------------------------------------------------------
  Project: DolphinIPL
  Description: J3DBinary 1.5 Renderer
  
  Mtx生成
  -----------------------------------------------------------------------------*/
#include <dolphin.h>

#include "gMath.h"
#include "gBase.h"
#include "gTrans.h"

#define  gDefaultTransformInfo (J3DTransformInfo){1.f, 1.f, 1.f, 0, 0, 0, 0, 0.f, 0.f, 0.f}

/*---------------------------------------------------------------------------*
	m	マトリクス（返り値）
	axis	軸'X''x''Y''y''Z''z'
	rot	回転角度
 *---------------------------------------------------------------------------*/
void
gMTXRotS16(Mtx m, char axis, s16 rot)
{
    switch(axis) {
    case 'X':
    case 'x':
        m[0][0] = 1.f;
        m[0][1] = 0.f;
        m[0][2] = 0.f;
        m[0][3] = 0.f;
        m[1][0] = 0.f;
        m[1][1] = gSCos(rot);
        m[1][2] = -gSSin(rot);
        m[1][3] = 0.f;
        m[2][0] = 0.f;
        m[2][1] = gSSin(rot);
        m[2][2] = gSCos(rot);
        m[2][3] = 0.f;
        break;
    case 'Y':
    case 'y':
        m[0][0] = gSCos(rot);
        m[0][1] = 0.f;
        m[0][2] = gSSin(rot);
        m[0][3] = 0.f;
        m[1][0] = 0.f;
        m[1][1] = 1.f;
        m[1][2] = 0.f;
        m[1][3] = 0.f;
        m[2][0] = -gSSin(rot);
        m[2][1] = 0.f;
        m[2][2] = gSCos(rot);
        m[2][3] = 0.f;
        break;
    case 'Z':
    case 'z':
        m[0][0] = gSCos(rot);
        m[0][1] = -gSSin(rot);
        m[0][2] = 0.f;
        m[0][3] = 0.f;
        m[1][0] = gSSin(rot);
        m[1][1] = gSCos(rot);
        m[1][2] = 0.f;
        m[1][3] = 0.f;
        m[2][0] = 0.f;
        m[2][1] = 0.f;
        m[2][2] = 1.f;
        m[2][3] = 0.f;
        break;
    default:
        break;
    }
}



/*---------------------------------------------------------------------------*
	rx	x軸を中心とした回転角度
	ry	y軸を中心とした回転角度
	rz	z軸を中心とした回転角度
	tx	x方向の移動距離
	ty	y方向の移動距離
	tz	z方向の移動距離
  	m	マトリクス（返り値）

	回転位置マトリクスを取得します。

 *---------------------------------------------------------------------------*/
//インライン化
void gGetTranslateRotateMtx(s16 rx, s16 ry, s16 rz, f32 tx, f32 ty, f32 tz, Mtx m)
{
    f32* mPt = (f32*)m;
	f32 sinx, siny, sinz, cosx, cosy, cosz;

     sinx = gSSin(rx);
     cosx = gSCos(rx);
     siny = gSSin(ry);
     cosy = gSCos(ry);
     sinz = gSSin(rz);
     cosz = gSCos(rz);

    *mPt++ =  cosz*cosy;
    *mPt++ = -sinz*cosx+cosz*siny*sinx;
    *mPt++ =  sinz*sinx+cosz*siny*cosx;
    *mPt++ = tx;
    *mPt++ =  sinz*cosy;
    *mPt++ =  cosz*cosx+sinz*siny*sinx;
    *mPt++ = -cosz*sinx+sinz*siny*cosx;
    *mPt++ = ty;
    *mPt++ = -siny;
    *mPt++ =  cosy*sinx;
    *mPt++ =  cosy*cosx;
    *mPt++ = tz;
}

/*---------------------------------------------------------------------------*
	rx	x軸を中心とした回転角度
	ry	y軸を中心とした回転角度
	rz	z軸を中心とした回転角度
  	m	マトリクス（返り値）

	回転マトリクスを取得します。

 *---------------------------------------------------------------------------*/
void
gGetRotateMtx(s16 rx, s16 ry, s16 rz, Mtx m)
{
    f32* mPt = (f32*)m;
	f32 sinx, siny, sinz, cosx, cosy, cosz;

     sinx = gSSin(rx);
     cosx = gSCos(rx);
     siny = gSSin(ry);
     cosy = gSCos(ry);
     sinz = gSSin(rz);
     cosz = gSCos(rz);
	
    *mPt++ =  cosz*cosy;
    *mPt++ = -sinz*cosx+cosz*siny*sinx;
    *mPt++ =  sinz*sinx+cosz*siny*cosx;
    *mPt++ = 0.f;
    *mPt++ =  sinz*cosy;
    *mPt++ =  cosz*cosx+sinz*siny*sinx;
    *mPt++ = -cosz*sinx+sinz*siny*cosx;
    *mPt++ = 0.f;
    *mPt++ = -siny;
    *mPt++ =  cosy*sinx;
    *mPt++ =  cosy*cosx;
    *mPt++ = 0.f;
}


/*---------------------------------------------------------------------------*
	ti	トランスフォーム情報（スケール、回転、位置）
	mtx	マトリクス（返り値）

	トランスフォーム情報からマトリクスを算出します。

 *---------------------------------------------------------------------------*/
void
gGetTransformMtx(J3DTransformInfo* ti, Mtx mtx)
{
    gGetTranslateRotateMtx(ti->rx, ti->ry, ti->rz, ti->tx, ti->ty, ti->tz, mtx);

        // T*R*S
	if ( ti->sx != 1.f ) {
        mtx[0][0] *= ti->sx;
        mtx[1][0] *= ti->sx;
        mtx[2][0] *= ti->sx;
	}
	if ( ti->sy != 1.f ) {
        mtx[0][1] *= ti->sy;
        mtx[1][1] *= ti->sy;
        mtx[2][1] *= ti->sy;
	}
	if ( ti->sz != 1.f ) {
        mtx[0][2] *= ti->sz;
        mtx[1][2] *= ti->sz;
        mtx[2][2] *= ti->sz;
	}
}

/*---------------------------------------------------------------------------*
  	m	法線用マトリクス（返り値）
  	v	拡大率

	法線用マトリクスを比較的高速に拡大縮小する
  	（トランス成分は扱わない）
 *---------------------------------------------------------------------------*/
void
gScaleNrmMtx(MtxPtr m, Vec* v)
{
    f32* mPt = (f32*)m;

    *mPt++ *= v->x;
    *mPt++ *= v->y;
    *mPt++ *= v->z;
    mPt++;
    *mPt++ *= v->x;
    *mPt++ *= v->y;
    *mPt++ *= v->z;
    *mPt++;
    *mPt++ *= v->x;
    *mPt++ *= v->y;
    *mPt++ *= v->z;
}


/*-----------------------------------------------------------------------------
  逆行列転置を求める。
  -----------------------------------------------------------------------------*/
u32
gGetInverseTranspose( Mtx src, Mtx inv )
{
    f32 det;
	f32* invPt;

    // compute the determinant of the upper 3x3 submatrix
    det =	src[0][0]*src[1][1]*src[2][2] + src[0][1]*src[1][2]*src[2][0] + src[0][2]*src[1][0]*src[2][1]
        - src[2][0]*src[1][1]*src[0][2] - src[1][0]*src[0][1]*src[2][2] - src[0][0]*src[2][1]*src[1][2];
    
    
    // check if matrix is singular
    if( det == 0.0f ) return FALSE;
    
    // compute the inverse of the upper submatrix:
    
    // find the transposed matrix of cofactors of the upper submatrix
    // and multiply by (1/det)
    
    det = 1.0f / det;
    
    invPt = (f32*)inv;
    
    *invPt++ =  (src[1][1]*src[2][2] - src[2][1]*src[1][2]) * det;
    *invPt++ = -(src[1][0]*src[2][2] - src[2][0]*src[1][2]) * det;
    *invPt++ =  (src[1][0]*src[2][1] - src[2][0]*src[1][1]) * det;
    *invPt++ = 0.f;
        
    *invPt++ = -(src[0][1]*src[2][2] - src[2][1]*src[0][2]) * det;
    *invPt++ =  (src[0][0]*src[2][2] - src[2][0]*src[0][2]) * det;
    *invPt++ = -(src[0][0]*src[2][1] - src[2][0]*src[0][1]) * det;
    *invPt++ = 0.f;
        
    *invPt++ =  (src[0][1]*src[1][2] - src[1][1]*src[0][2]) * det;
    *invPt++ = -(src[0][0]*src[1][2] - src[1][0]*src[0][2]) * det;
    *invPt++ =  (src[0][0]*src[1][1] - src[1][0]*src[0][1]) * det;
    *invPt++ = 0.f;

    return TRUE;
}


/*-----------------------------------------------------------------------------
  Vecのエルミート補完した値を返します。
  -----------------------------------------------------------------------------*/
void
gGetVecInterp( f32 time,
			   f32 f0, VecPtr v0, VecPtr s0, 
			   f32 f1, VecPtr v1, VecPtr s1,
			   VecPtr result )
{
	Vec		ret;
	f32		*rp  = (f32*)&ret;
	f32		*vp0 = (f32*)v0;
	f32		*sp0 = (f32*)s0;
	f32		*vp1 = (f32*)v1;
	f32		*sp1 = (f32*)s1;
	int		i;

	if ( time < f0 ) {
		*result = *v0;
	}
	else if ( time > f1 ) {
		*result = *v1;
	}
	else {
		for ( i=0 ; i<3 ; i++ ) {
			rp[i] = gHermiteInterpolation( time, f0, vp0[i], sp0[i], f1, vp1[i], sp1[i] );
		}
		*result = ret;
	}
}
