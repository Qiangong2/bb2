/*---------------------------------------------------------------------------*
  Project:  Dolphin OS BS2 splash
  File:     BS2Draw.c

  Copyright 1998, 1999 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: BS2Draw.c,v $
  Revision 1.1.1.1  2004/06/01 21:17:32  paulm
  GC IPLROM source from Nintendo

    
    1     9/08/00 4:07p Tian
    Initial checkin to production tree
    
    4     4/12/00 5:11p Hashida
    Black out the screen at the end of splash.
    
    3     4/06/00 5:20p Shiki
    Clean up.

    2     3/07/00 11:20a Hashida
    Changed VIDataToRenderMode so that it sets viHeight correctly.

    1     2/02/00 5:11p Tian
    Initial checkin - drawing component of splash screen.
  $NoKeywords: $
 *---------------------------------------------------------------------------*/
#include <dolphin.h>
#include "BS2Private.h"

#ifndef BRINGUP

#include "BS2Draw.h"

extern void SplashInit(void);
extern void SplashTick(BS2State state);

#define SPACING 400
#define LEFT_X (-600.0f)
#define LEFT_Y (-300.0f)

u8*         XFB;
s32         abs             ( s32 x );
void        ProjectArray    ( Mtx44 m, VecPtr srcBase, VecPtr dstBase,
                              u32 count );
void        PlotPixel       ( u16 x, u16 y, u8 color, u8 u, u8 v );
void        Line            ( s32 x, s32 y, s32 x2, s32 y2, u8 c, u8 u, u8 v);
void        DrawPoly        ( VecPtr start, u32 numElems, u8 c, u8 u , u8 v );
void        DrawLines       ( VecPtr start, u32 numElems, u8 c, u8 u , u8 v );
void        viDataToRM      ( VIData_s* vd, GXRenderModeObj* rm );

typedef enum
{
    SP_FADE_IN,
    SP_SPIN,
    SP_FADE_OUT,
    SP_DONE
} SplashState;

SplashState CurrentSplashState = SP_FADE_IN;

Vec Nintendo[]=
{
    // N1
    {-50.0f, -50.0f, 50.0f},
    {-20.0f, -50.0f, 50.0f},
    {20.0f, -50.0f, -5.0f},
    {20.0f, -50.0f, 50.0f},
    {50.0f, -50.0f, 50.0f},
    {50.0f, -50.0f, -50.0f},
    {20.0f, -50.0f, -50.0f},
    {-20.0f, -50.0f, 5.0f},
    {-20.0f, -50.0f, -50.0f},
    {-50.0f, -50.0f, -50.0f},// 10

    // N2
    {50.0f, -50.0f, 50.0f},
    {50.0f, -20.0f, 50.0f},
    {50.0f, 20.0f, -5.0f},
    {50.0f, 20.0f, 50.0f},
    {50.0f, 50.0f, 50.0f},
    {50.0f, 50.0f, -50.0f},
    {50.0f, 20.0f, -50.0f},
    {50.0f, -20.0f, -5.0f},
    {50.0f, -20.0f, -50.0f},
    {50.0f, -50.0f, -50.0f},// 20

    // N3
    {50.0f, 50.0f, 50.0f},
    {20.0f, 50.0f, 50.0f},
    {-20.0f, 50.0f, -5.0f},
    {-20.0f, 50.0f, 50.0f},
    {-50.0f, 50.0f, 50.0f},
    {-50.0f, 50.0f, -50.0f},
    {-20.0f, 50.0f, -50.0f},
    {20.0f, 50.0f, -5.0f},
    {20.0f, 50.0f, -50.0f},
    {50.0f, 50.0f, -50.0f},// 30

    // N4
    {-50.0f, 50.0f, 50.0f},
    {-50.0f, 20.0f, 50.0f},
    {-50.0f, -20.0f, -5.0f},
    {-50.0f, -20.0f, 50.0f},
    {-50.0f, -50.0f, 50.0f},
    {-50.0f, -50.0f, -50.0f},
    {-50.0f, -20.0f, -50.0f},
    {-50.0f, 20.0f, -5.0f},
    {-50.0f, 20.0f, -50.0f},
    {-50.0f, 50.0f, -50.0f},// 40

    // inside
    {20.0f, 20.0f, -50.0f},
    {20.0f, 50.0f, -50.0f},

    {-20.0f, 20.0f, -50.0f},
    {-20.0f, 50.0f, -50.0f},

    {-50.0f, -20.0f, 50.0f},
    {-20.0f, -20.0f, 50.0f},

    {-50.0f, 20.0f, 50.0f},
    {-20.0f, 20.0f, 50.0f},

    {-20.0f, 50.0f, 50.0f},
    {-20.0f, 20.0f, 50.0f},
    {-20.0f, -20.0f, 5.0f},

    {20.0f, 20.0f, -5.0f},
    {-20.0f, 20.0f, -50.0f},
    {-50.0f, 20.0f, -50.0f},

    {20.0f, 50.0f, 50.0f},
    {20.0f, 20.0f, 50.0f},
    {-20.0f, 20.0f, -50.0f},
    {-20.0f, -20.0f, -50.0f},
    {-50.0f, -20.0f, -50.0f}, // 59

    // inside2
    {-20.0f, -20.0f, -50.0f},
    {-20.0f, -50.0f, -50.0f},

    {20.0f, -20.0f, -50.0f},
    {20.0f, -50.0f, -50.0f},

    {50.0f, -20.0f, 50.0f},
    {20.0f, -20.0f, 50.0f},

    {50.0f, 20.0f, 50.0f},
    {20.0f, 20.0f, 50.0f},

    {20.0f, -50.0f, 50.0f},
    {20.0f, -20.0f, 50.0f},
    {20.0f, 20.0f, -5.0f},

    {-20.0f, -50.0f, 50.0f},
    {-20.0f, -20.0f, 50.0f},
    {20.0f, -20.0f, -5.0f},
    {20.0f, 20.0f, -50.0f},
    {50.0f, 20.0f, -50.0f},

    {-20.0f, -20.0f, 50.0f},
    {20.0f, -20.0f, -50.0f},
    {50.0f, -20.0f, -50.0f}


};

u32     VertIdx;
Vec  VertList[70];
Vec  TestList[70];
Mtx44   ProjMatrix;
Mtx     TheMatrix;
Mtx     TransMatrix;
Mtx     ScaleMatrix;
Mtx     RotMatrix;

/*---------------------------------------------------------------------------*
  Line rasterization and point plotting.
 *---------------------------------------------------------------------------*/

/** Public Domain mode 13h Bresenham line/circle algorithms
** By Brian Dessent
**
** Circle algorithm and other stuff rewritten by Kurt Kuzba
**
** Written for Borland, modified for others by Bob Stout
*/


/* draws a line from (x, y) to (x2, y2) in color c */

void PlotPixel (u16 x, u16 y, u8 color, u8 u, u8 v)
{
  u32 address;
  u32 colorData;
  u32 pix0,pix1;
  u32* dest;


  address = FB_START + y*SCREEN_WD*2 + x*2;
  colorData = *((u32*)(address&0xFFFFFFFC));
  pix0 = (colorData>>24)&0xFF;
  pix1 = ((colorData>>8)&0xFF);
  if (address & 0x02)
    pix1=color;
  else
    pix0=color;

  colorData = pix0<<24 | u<<16 | pix1<<8 | v;
  dest = (u32*)(address &0xFFFFFFFC);
  *dest = colorData;

//  busWrt32(address&0xFFFFFFFC,colorData);
}

void Line(s32 x, s32 y, s32 x2, s32 y2, u8 c, u8 u, u8 v)
{
    s32 i, steep = 0, sx, sy, dx, dy, e;

    dx = abs(x2 - x);
    sx = ((x2 - x) > 0) ? 1 : -1;
    dy = abs(y2 - y);
    sy = ((y2 - y) > 0) ? 1 : -1;

    if(dy > dx)
    {
        steep =  x;   x =  y;   y = steep;  /* swap  x and  y */
        steep = dx;  dx = dy;  dy = steep;  /* swap dx and dy */
        steep = sx;  sx = sy;  sy = steep;  /* swap sx and sy */
        steep = 1;
    }

    e = 2 * dy - dx;
    for(i = 0; i < dx; i++)
    {
        if(steep)
            PlotPixel((u16)y, (u16)x, c, u, v);
        else  PlotPixel((u16)x, (u16)y, c, u, v);
        while(e >= 0)
        {
            y += sy;
            e -= 2 * dx;
        }
        x += sx;
        e += 2 * dy;
    }
    PlotPixel((u16)x2, (u16)y2, c, u, v);
}


// draw projected polys to screen - z should be 0
void DrawPoly(VecPtr start, u32 numElems, u8 c, u8 u , u8 v)
{
    u32     i;
    VecPtr    thispt = start;
    VecPtr    nextpt = start+1;

    for (i=0; i < numElems; i++)
    {
        thispt = &start[i];
        nextpt = &start[(i+1)%numElems];

        Line((s32)thispt->x + (SCREEN_WD/2)- 100 ,
             (s32)thispt->y+(SCREEN_HT/2)-100,
             (s32)nextpt->x + (SCREEN_WD/2)-100,
             (s32)nextpt->y+(SCREEN_HT/2)-100,
             c, u , v);
    }
}

void DrawLines(VecPtr start, u32 numElems, u8 c, u8 u , u8 v)
{
    u32     i;
    VecPtr    thispt = start;
    VecPtr    nextpt = start+1;

    for (i=0; i < (numElems-1); i++)
    {
        thispt = &start[i];
        nextpt = &start[i+1];

        Line((s32)thispt->x + (SCREEN_WD/2)- 100 ,
             (s32)thispt->y+(SCREEN_HT/2)-100,
             (s32)nextpt->x + (SCREEN_WD/2)-100,
             (s32)nextpt->y+(SCREEN_HT/2)-100,
             c, u , v);
    }
}

// projection matrix
void ProjectArray ( Mtx44 m, VecPtr srcBase, VecPtr dstBase, u32 count )
{
    u32 i;
    Vec vTmp;
    f32 w;


    for(i=0; i< count; i++)
    {

        // Vec has a 4th implicit 'w' coordinate of 1
        vTmp.x = m[0][0]*srcBase->x + m[0][1]*srcBase->y + m[0][2]*srcBase->z + m[0][3];
        vTmp.y = m[1][0]*srcBase->x + m[1][1]*srcBase->y + m[1][2]*srcBase->z + m[1][3];
        vTmp.z = m[2][0]*srcBase->x + m[2][1]*srcBase->y + m[2][2]*srcBase->z + m[2][3];
        w =      m[3][0]*srcBase->x + m[3][1]*srcBase->y + m[3][2]*srcBase->z + m[3][3];

        // copy back
        dstBase->x = vTmp.x/w;
        dstBase->y = vTmp.y/w;
        dstBase->z = vTmp.z/w;

        srcBase++;
        dstBase++;
    }

}



// step animation and draw
u32 Count;

void DrawNintendo(u8 c, u8 u, u8 v)
{
    MTXRotDeg(TheMatrix, 'Z', Count++);
#ifdef GEKKO
    PSMTXConcat(RotMatrix, TheMatrix, TheMatrix);
    PSMTXConcat(ScaleMatrix, TheMatrix, TheMatrix);
    PSMTXConcat(TransMatrix, TheMatrix, TheMatrix);
    PSMTXMultVecArray(TheMatrix, (VecPtr)Nintendo, VertList+VertIdx, 40);
#else
    MTXConcat(RotMatrix, TheMatrix, TheMatrix);
    MTXConcat(ScaleMatrix, TheMatrix, TheMatrix);
    MTXConcat(TransMatrix, TheMatrix, TheMatrix);
    MTXMultVecArray(TheMatrix, (VecPtr)Nintendo, VertList+VertIdx, 40);
#endif
    ProjectArray(ProjMatrix, VertList+VertIdx,VertList+VertIdx, 40);

    DrawPoly(VertList+VertIdx, 10, c, u , v);
    DrawPoly(VertList+VertIdx+10, 10, c, u , v);
    DrawPoly(VertList+VertIdx+20, 10, c, u , v);
    DrawPoly(VertList+VertIdx+30, 10, c, u , v);

}


void viDataToRM(VIData_s* vd, GXRenderModeObj* rm)
{
    rm->viTVmode = (VITVMode)VI_TVMODE(vd->tvFmt, vd->tvInt);
    rm->fbWidth = vd->xfbW;
    rm->xfbHeight = vd->xfbH;
    rm->viXOrigin = vd->dispX;
    rm->viYOrigin = vd->dispY;
    rm->viWidth = vd->dispW;
    rm->xFBmode = vd->mode;

    rm->viHeight = (vd->mode == VI_XFBMODE_DF)? vd->xfbH : (u16)(vd->xfbH*2);
}
void FillXFB(u8 c, u8 b, u8 r)
{
    u32         x,y;
    u8*         ptr;

    ptr = XFB;

    for (y = 0; y < SCREEN_HT; y++)
    {
        for (x = 0; x < SCREEN_WD; x+=2)
        {
            *(u32*)ptr = (u32)((c<<24) + (b<<16) + (c<<8) + (r));
            // We handle two pixels at a time.
            ptr += PIXEL_SIZE * 2;
        }
    }
}

static void SplashVIInit(void)
{
    VIData_s        VIData;
    GXRenderModeObj rm;

    VIData.dispX = 0;
    VIData.dispY = 0;
    VIData.dispW = SCREEN_WD;
    VIData.tvFmt = VI_NTSC;
    VIData.tvInt = VI_INTERLACE;
    VIData.xfbW  = SCREEN_WD;
    VIData.xfbH  = SCREEN_HT;
    VIData.mode  = VI_XFBMODE_DF;
    VIData.panX  = 0;
    VIData.panY  = 0;
    VIData.panW  = SCREEN_WD;
    VIData.panH  = SCREEN_HT;
    VIData.black = FALSE;

    VIWaitForRetrace();

    if (VIGetCurrentLine() == 6)
        while (VIGetCurrentLine() == 6)
            ;

    viDataToRM(&VIData, &rm);
    VIConfigure(&rm);
    VIConfigurePan(VIData.panX, VIData.panY,
                   VIData.panW, VIData.panH);
    VISetBlack(VIData.black);

    FillXFB(0,0,0);
    VIFlush();
    VIWaitForRetrace();

    VISetNextFrameBuffer(XFB);

    MTXTrans(TransMatrix, LEFT_X + (2*SPACING),
             LEFT_Y + (2*SPACING),
             200.0f + (1*SPACING));
    MTXPerspective (ProjMatrix,
                    100.0f,                         // FOV
                    (f32)SCREEN_WD/SCREEN_HT,       // ASPECT
                    10.0f, 10000.0f);               // NEAR/FAR
    ProjMatrix[0][0] = 1.0f;
    ProjMatrix[1][1] = 1.0f;
    ProjMatrix[2][2] = 1.0f;
    ProjMatrix[2][3] = 0.0f;
    ProjMatrix[3][2] = 1.0f/115.0f;

    MTXScale(ScaleMatrix, 2.5f, 2.5f, 2.5f);
    MTXRotDeg(RotMatrix, 'X', 90.0f);
}

/*---------------------------------------------------------------------------*
  Name:         SplashInit

  Description:  Initializes memory allocator and the external frame buffer

  Arguments:    None.

  Returns:      None.
 *---------------------------------------------------------------------------*/
void SplashInit( void )
{
    void* arenaLo;
    void* arenaHi;

    arenaLo = OSGetArenaLo();
    arenaHi = OSGetArenaHi();

    // OSInitAlloc should only ever be invoked once.
    arenaLo = OSInitAlloc(arenaLo, arenaHi, 1); // 1 heap
    OSSetArenaLo(arenaLo);

    // The boundaries given to OSCreateHeap should be 32B aligned
    OSSetCurrentHeap(OSCreateHeap((void*)OSRoundUp32B(arenaLo),
                                  (void*)OSRoundDown32B(arenaHi)));
    // From here on out, OSAlloc and OSFree behave like malloc and free
    // respectively

    OSSetArenaLo(arenaLo = arenaHi);

    XFB = OSAlloc(SCREEN_HT * SCREEN_WD * PIXEL_SIZE);
    FillXFB(0,0,0);

    SplashVIInit();
}


void SplashTick(BS2State state)
{
    static u8 background = 0;

    switch (CurrentSplashState)
    {
      case SP_FADE_IN:
        background ++;
        FillXFB(background, 0, background);
        DrawNintendo(80,128,128);
        if (background >= 128)
        {
            CurrentSplashState = SP_SPIN;
        }
        break;

      case SP_SPIN:
        FillXFB(background, 0, background);
        DrawNintendo(128,128,0);
        if (state == BS2_RUN_APP)
        {
            CurrentSplashState = SP_FADE_OUT;
        }
        break;

      case SP_FADE_OUT:
        FillXFB(background, 0, background--);
        background--;
        DrawNintendo(128,128,0);
        if (background == 0)
        {
            CurrentSplashState = SP_DONE;

            // Black out screen
            VISetBlack(TRUE);
            VIFlush();
            VIWaitForRetrace();

            BS2StartGame();
        }
        break;

      case SP_DONE:
        FillXFB(0,0,0);
        break;
    }

    DCStoreRange((void*)XFB, SCREEN_HT * SCREEN_WD * PIXEL_SIZE);
    VIFlush();
    VIWaitForRetrace();
}

#endif  // BRINGUP
