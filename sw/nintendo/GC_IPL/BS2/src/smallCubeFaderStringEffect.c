#include <dolphin.h>
#include <string.h>
#include <math.h>

#include "smallCubeFaderStringEffect.h"
#include "smallCubeFader.h"
#include "smallCubeAnimation.h"
#include "smallCubeFaderData.h"
#include "OptionUpdate.h"
#include "TimeUpdate.h"
#include "MenuUpdate.h"
#include "gRand.h"
#include "gBase.h"

static f32 getFrame(void);
static void setChangedAnim(Vec dir, s16 oldval, s16 newval );
void scfUpdateOptionStringEffect( J3DTransformInfo *pos );
void scfUpdateStartStringEffect( J3DTransformInfo *pos );
static void initStartStringEffect(void);
static void initOptionStringEffect( void );

static s16 sFrame;

static s16 sOldState, sNewState;

#define STRINGEFFECTCUBENUM (((SCF_CUBEBASENUM*2)))
static J3DTransformInfo sEffectPos[STRINGEFFECTCUBENUM];
static u8 sAlpha[STRINGEFFECTCUBENUM];
static s16 sCubeNum;

s16 sMoveOffset[STRINGEFFECTCUBENUM];
Vec sMoveDir;
u32 scfGetStringEffectNum(void)
{
    return sCubeNum;
}
u8 *scfGetStringEffectAlpha(void)
{
    return sAlpha;
}
J3DTransformInfo* scfGetStringEffectPos(void)
{
    return sEffectPos;
}


void scfInvalidateStringEffect(void)
{
    sFrame = getMenuState()->scf_numeffectframe;// 終わった状態にしとく
}


static void initOptionStringEffect( void )
{
    sNewState = sOldState = scfGetState()->optionsound;
}


void scfUpdateOptionStringEffect( J3DTransformInfo *pos )
{
    u16 *data;
    u16 newnum, oldnum;

    int i;

    if( scfGetState()->optionrunstate == IPL_OPTION_SOUND
		&& scfGetPrevState()->optionrunstate == IPL_OPTION_SOUND ){ /* 移行した後１フレーム
																	   待ってからでないと、
																	   エフェクトが発生してまう */
		sCubeNum = STRINGEFFECTCUBENUM;
    }else{
		initOptionStringEffect();
		sCubeNum = 0;
		return;
    }

    for( i=0;i<STRINGEFFECTCUBENUM;i++ ) sAlpha[i] = 0;

    // (古)を作る
    switch( sOldState ){
    case 0://mono
		data = scfGetMONOIndex();
		oldnum = scfGetMONONum();
		break;
    case 1://stereo
		data = scfGetSTEREOIndex();
		oldnum = scfGetSTEREONum();
		break;
    default:
		DIReport2( "old: %d new: %d\n", sOldState, sNewState );
		ASSERTMSG( 0, "invalid old state for string effect\n" );
		break;
    }
    for( i=0;i<oldnum;i++ ){
		sEffectPos[i].tx = pos[data[i]].tx + sMoveOffset[i]*(-sMoveDir.x)*getFrame();
		sEffectPos[i].ty = pos[data[i]].ty + sMoveOffset[i]*(-sMoveDir.y)*getFrame();
		sEffectPos[i].tz = pos[data[i]].tz + sMoveOffset[i]*(-sMoveDir.z)*getFrame();
		sEffectPos[i].rx = pos[data[i]].rx;
		sEffectPos[i].ry = pos[data[i]].ry;
		sEffectPos[i].rz = pos[data[i]].rz;
		sEffectPos[i].sx = pos[data[i]].sx;
		sEffectPos[i].sy = pos[data[i]].sy;
		sEffectPos[i].sz = pos[data[i]].sz;
	
		sAlpha[i] = (u8)(255*(1.0f-getFrame()));
	
    }
    // (新)を作る
    switch( sNewState ){
    case 0://mono
		data = scfGetMONOIndex();
		newnum = scfGetMONONum();
		break;
    case 1://stereo
		data = scfGetSTEREOIndex();
		newnum = scfGetSTEREONum();
		break;
    default:
		DIReport2( "old: %d new: %d\n", sOldState, sNewState );
		ASSERTMSG( 0, "invalid new state for string effect\n" );
		break;
    }
    for( i=0;i<newnum;i++ ){
		sEffectPos[i+oldnum].tx = pos[data[i]].tx + sMoveOffset[i+oldnum]*sMoveDir.x*(1.0f-getFrame());
		sEffectPos[i+oldnum].ty = pos[data[i]].ty + sMoveOffset[i+oldnum]*sMoveDir.y*(1.0f-getFrame());
		sEffectPos[i+oldnum].tz = pos[data[i]].tz + sMoveOffset[i+oldnum]*sMoveDir.z*(1.0f-getFrame());
		sEffectPos[i+oldnum].rx = pos[data[i]].rx;
		sEffectPos[i+oldnum].ry = pos[data[i]].ry;
		sEffectPos[i+oldnum].rz = pos[data[i]].rz;
		sEffectPos[i+oldnum].sx = pos[data[i]].sx;
		sEffectPos[i+oldnum].sy = pos[data[i]].sy;
		sEffectPos[i+oldnum].sz = pos[data[i]].sz;
	
		sAlpha[i+oldnum] = (u8)(255*getFrame());
    }

    if( scfGetState()->optionsound ){
		setChangedAnim( (Vec){-1,0,0},
						scfGetPrevState()->optionsound,
						scfGetState()->optionsound );
    }else{
		setChangedAnim( (Vec){1,0,0},
						scfGetPrevState()->optionsound,
						scfGetState()->optionsound );
    }

    if( sFrame < getMenuState()->scf_numeffectframe ) sFrame ++;

}


static void initStartStringEffect(void)
{
    sNewState = sOldState = scfGetState()->startdvdstate;
}

void scfUpdateStartStringEffect( J3DTransformInfo *pos )
{
    u16 *data;
    u16 newnum, oldnum;

    int i;


    if( scfGetState()->state == SCFS_START
		&& scfGetPrevState()->state == SCFS_START ){
		sCubeNum = STRINGEFFECTCUBENUM;
    }else{
		initStartStringEffect();
		sCubeNum = 0;
		return;
    }

    for( i=0;i<STRINGEFFECTCUBENUM;i++ ) sAlpha[i] = 0;

    // (古)を作る

    switch( sOldState ){
    case 0://press start
    default: // スタートボタン押した後も
		data = scfGetPRESSSTARTIndex();
		oldnum = scfGetPRESSSTARTNum();
		break;
    case 1://no disk
		data = scfGetNODISCIndex();
		oldnum = scfGetNODISCNum();
		break;
    case 2:
		data = scfGetQUESIndex();
		oldnum = scfGetQUESNum();
		break;
    }
    for( i=0;i<oldnum;i++ ){
		sEffectPos[i].tx = pos[data[i]].tx + sMoveOffset[i]*(-sMoveDir.x)*getFrame();
		sEffectPos[i].ty = pos[data[i]].ty + sMoveOffset[i]*(-sMoveDir.y)*getFrame();
		sEffectPos[i].tz = pos[data[i]].tz + sMoveOffset[i]*(-sMoveDir.z)*getFrame();
		sEffectPos[i].rx = pos[data[i]].rx;
		sEffectPos[i].ry = pos[data[i]].ry;
		sEffectPos[i].rz = pos[data[i]].rz;
		sEffectPos[i].sx = pos[data[i]].sx;
		sEffectPos[i].sy = pos[data[i]].sy;
		sEffectPos[i].sz = pos[data[i]].sz;
	
		sAlpha[i] = (u8)(255*(1.0f-getFrame()));
	
    }
    // (新)を作る
    switch( sNewState ){
    case 0://press start
    default: // スタートボタン押した後も
		data = scfGetPRESSSTARTIndex();
		newnum = scfGetPRESSSTARTNum();
		break;
    case 1://no disk
		data = scfGetNODISCIndex();
		newnum = scfGetNODISCNum();
		break;
    case 2:
		data = scfGetQUESIndex();
		newnum = scfGetQUESNum();
		break;
    }
    for( i=0;i<newnum;i++ ){
		sEffectPos[i+oldnum].tx = pos[data[i]].tx + sMoveOffset[i+oldnum]*sMoveDir.x*(1.0f-getFrame());
		sEffectPos[i+oldnum].ty = pos[data[i]].ty + sMoveOffset[i+oldnum]*sMoveDir.y*(1.0f-getFrame());
		sEffectPos[i+oldnum].tz = pos[data[i]].tz + sMoveOffset[i+oldnum]*sMoveDir.z*(1.0f-getFrame());
		sEffectPos[i+oldnum].rx = pos[data[i]].rx;
		sEffectPos[i+oldnum].ry = pos[data[i]].ry;
		sEffectPos[i+oldnum].rz = pos[data[i]].rz;
		sEffectPos[i+oldnum].sx = pos[data[i]].sx;
		sEffectPos[i+oldnum].sy = pos[data[i]].sy;
		sEffectPos[i+oldnum].sz = pos[data[i]].sz;
	
		sAlpha[i+oldnum] = (u8)(255*getFrame());
    }

    setChangedAnim( (Vec){-1,0,0},
					scfGetPrevState()->startdvdstate,
					scfGetState()->startdvdstate );
    
    if( sFrame < getMenuState()->scf_numeffectframe ) sFrame ++;

}


static f32 getFrame(void)
{
    return (f32)sFrame/getMenuState()->scf_numeffectframe;
}

static void setChangedAnim(Vec dir, s16 old_stat, s16 new_stat)
{
    int i;

    if( old_stat == new_stat ) return;
    
    sMoveDir.x = dir.x;
    sMoveDir.y = dir.y;
    sMoveDir.z = dir.z;

    for( i=0;i<STRINGEFFECTCUBENUM;i++ ){
		sMoveOffset[i] = (s16)(getMenuState()->scf_numeffect_width*(f32)(200+urand()%600)/10.0f);
    }
    
    sFrame = 0;
    sOldState = old_stat;
    sNewState = new_stat;
}

