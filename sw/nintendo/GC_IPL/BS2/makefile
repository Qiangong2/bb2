###############################################################################
# Project: BS2 Production makefile
# File:    makefile
#
# Copyright 1998-2001 Nintendo.  All rights reserved.
#
# These coded instructions, statements, and computer programs contain
# proprietary information of Nintendo of America Inc. and/or Nintendo
# Company Ltd., and are protected by Federal copyright law.  They may
# not be disclosed to third parties or copied or duplicated in any form,
# in whole or in part, without the prior written consent of Nintendo.
#
# $Log: makefile,v $
# Revision 1.1.1.1  2004/06/01 21:17:32  paulm
# GC IPLROM source from Nintendo
#
#   
#   9     3/19/03 19:18 Shiki
#   Fixed not to define AMCDDKBIN and ODEMUSDKBIN.
#
#   8     11/15/01 13:20 Shiki
#   Merged DolphinIPL_09_Oct_2001.zip changes.
#
#   7     9/06/01 17:10 Shiki
#   Fixed the export rule.
#
#   6     5/11/01 12:55p Shiki
#   Modified IPLLIBS directory from lib to ipllib.
#
#   5     5/10/01 8:45p Shiki
#   Added IPLLIBS.
#
#   4     5/10/01 6:57p Shiki
#   Merged EAD code.
#
#   3     10/02/00 7:52p Tian
#   Added BootStart.c
#
#   2     9/28/00 2:38p Tian
#   From Shiki : Fixed to support up to 2MB image.
#
#   1     9/08/00 4:06p Tian
#   Initial checkin to production tree
#
#   6     4/06/00 5:25p Shiki
#   Added BS2Mach.c
#
#   5     2/02/00 5:11p Tian
#   Added BS2Draw
#
#   4     1/31/00 2:37p Tian
#   Moved dol utilities to /bin/X86
#
#   3     12/15/99 5:38p Shiki
#   Included BS2Init.c.
#
#   2     12/10/99 3:46p Shiki
#   Added an export rule for Minnow.
#
#   15    11/05/99 3:05p Shiki
#   Added extra clobber rules.
#
#   14    11/04/99 12:53a Shiki
#   Revised
#
#   13    11/03/99 2:39p Shiki
#   Revised to support multiple platforms.
# $NoKeywords: $
#
###############################################################################

all: build

# module name should be set to the name of this subdirectory
MODULENAME		= BS2
BOOT_PRODUCTION		= TRUE

IPL                     = TRUE

ifndef NDEBUG
IMGSUFFIX   = D.img
else
IMGSUFFIX   = .img
endif

# Perform NDEBUG build only
NDEBUG 		= TRUE
override DEBUG 	=

include $(subst build\,securebuild\,$(BUILDTOOLS_ROOT))/commondefs

override AMCDDKBIN=
override ODEMUSDKBIN=

ifeq ($(MPAL), TRUE)
CCFLAGS += -DMPAL
ASFLAGS += -DMPAL
DIR_LANG = MPAL
else
ifeq ($(PAL), TRUE)
CCFLAGS += -DPAL
ASFLAGS += -DPAL
DIR_LANG = PAL
else
CCFLAGS += -DNTSC
ASFLAGS += -DNTSC
DIR_LANG = NTSC
endif
endif

CSRCS		= 	BS2Init.c BS2.c BS2Mach.c BootStart.c ai_src_init.c

CSRCS	       += 	main.c \
			gInit.c \
			gCont.c \
			gCamera.c \
			gMRbuild.c \
			gMRdraw.c \
			gMRnode.c \
			gMRjoint.c \
			gMRshape.c \
			gMRmaterial.c \
			gMRanime.c \
			gTrans.c \
			gMath.c \
			gBase.c \
			gRand.c \
			gDynamic.c \
			gFont.c \
			gLayoutRender.c \
			gLoader.c \
			SplashMain.c \
			SplashUpdate.c \
			SplashDraw.c \
			SplashtoMenuUpdate.c \
			LogoUpdate.c \
			LogoDraw.c \
			MenuUpdate.c \
			StartState.c \
			CardState.c \
			CardUpdate.c \
			CardSequence.c \
			CardIconMove.c \
			OptionState.c \
			TimeState.c \
			MenuCardAnim.c \
			MenuStartAnim.c \
			MenuOptionAnim.c \
			MenuClockAnim.c \
			StartUpdate.c \
			OptionUpdate.c \
			TimeUpdate.c \
			smallCubeAnimation.c \
			smallCubeFader.c \
			smallCubeFaderData.c \
			smallCubeDraw.c \
			smallCubeFaderOffsetEffect.c \
			smallCubeMovement.c \
			smallCubeFaderNumberEffect.c \
			smallCubeFaderStringEffect.c

ifeq ($(MPAL), TRUE)
CSRCS += gcarchive_mpal.c
else
ifeq ($(PAL), TRUE)
CSRCS += gcarchive_pal.c
else
CSRCS += gcarchive_ntsc.c
endif
endif

BINNAMES 	= BS2
BS2_LCF_FILE	= $(INC_ROOT)/secure/BS2.$(ARCH_TARGET).lcf
LDFLAGS		+= -lcf $(BS2_LCF_FILE)

CCFLAGS += -DIPL

# Define DBOOT_IRD to check disk country code
CCFLAGS += -DBOOT_IRD

# directory prefix setting
DIR_IPL = _IPL_$(DIR_LANG)

LINCLUDES += -i $(MODULE_ROOT)/Jaudio

include $(subst build\,securebuild\,$(BUILDTOOLS_ROOT))/modulerules

# IPL specific libraries: Since sizeof(CARDControl) has been extended,
# Jaudio doesn't work anymore. We keep using the card library as of 9/8/2001.
IPLLIB_ROOT     = ipllib/$(ARCH_TARGET)
FULLIPLLIB_ROOT = $(MODULE_ROOT)/$(IPLLIB_ROOT)
IPLLIBS         = $(FULLIPLLIB_ROOT)/card$(LIBSUFFIX)

$(FULLBIN_ROOT)/BS2$(BINSUFFIX): $(CSRCS:.c=.o) $(MODULE_ROOT)/Jaudio/Jaudio.o $(IPLLIBS) $(DOLPHINLIBS)

# Export BS2 image to BS1

export: build
	@echo ">>> Exporting BS2 image"
	"$(ROOT)/X86/bin/makeimageD.exe" -i $(FULLBIN_ROOT)/BS2$(BINSUFFIX) -o $(FULLBIN_ROOT)/BS2$(IMGSUFFIX) -l 2096896 -s
	@if [ ! -d $(BOOT_ROOT)/BS1/bin/$(ARCH_TARGET) ] ; then \
		echo ">>> Creating $(BOOT_ROOT)/BS1/bin/$(ARCH_TARGET)" ; \
		$(MKDIR) $(BOOT_ROOT)/BS1/bin/$(ARCH_TARGET) ; \
	fi
	cp $(FULLBIN_ROOT)/BS2$(IMGSUFFIX) $(BOOT_ROOT)/BS1/bin/$(ARCH_TARGET)
