/*---------------------------------------------------------------------------*
	Project:	Dolphin MIC + MDM network test
	File:		pimpom.c

	Copyright 1998, 1999 Nintendo. All rights reserved.

	These coded instructions, statements, and computer programs contain
	proprietary information of Nintendo of America Inc. and/or Nintendo
	Company Ltd., and are protected by Federal copyright law.	 They may
	not be disclosed to third parties or copied or duplicated in any form,
	in whole or in part, without the prior written consent of Nintendo.

	$Log: pimpom.c,v $
	Revision 1.1.1.1  2004/03/15 21:58:02  paulm
	Network Game SDK 30-Sep-2003

    
    5     02/03/26 18:13 Ikedae
    change title
    
	
	4	  02/03/26 18:03 Ikedae
	try to recover when NO CARRIER occurs
	
	3	  02/03/26 16:06 Ikedae
	
	2	  02/03/22 18:19 Yama
	change "USE_ARG" 0 => 1 ( to use arguments )
	
	1	  02/03/22 18:06 Yama
	add to IRD VSS


	$NoKeywords: $
 *---------------------------------------------------------------------------*/


/*==================================================*
	Header file
 *==================================================*/
// standard library
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// dolphin library
#include <demo.h>

// dwango library
#include <dolphin/h2h/directengine.h>
#include <dolphin/h2h/netengine.h>

// self-use header file
#include "ctrl_bar.h"


/*==================================================*
	Macro definition
 *==================================================*/
// for main game
#define FIELD_X 		320
#define FIELD_Y 		240
#define	MAXPLAYER		2
#define MAXBALL 		1
#define MAXSCORE		10
#define BALL_SIZEMAG	1.0F

enum POM_GAME_STATUS
{
	POM_NOW_PLAYING = 0,
	POM_RESTART,
	POM_GAME_OVER,
	POM_GAME_EXIT
};

#define CONNECT_STATUS_CALL 	0x01
#define CONNECT_STATUS_WAIT 	0x02

// for dwango sample
#define STRMDATA_LEN			8		// max size of multiple data length
#define CLNT_NUM_MAX			2		// max number of clients
#define ASYNCDATA_LEN			13		// created by ikedae

// for mic stream
#define MAX_ARAM_BLOCKS 		2
#define COMP_MODE_MAXNUM		12
#define COMP_BUFF_NUM			50
#define COMP_FRAME_SIZE_MAX 	ASYNCDATA_LEN


/*==================================================*
	Function prototype declaration
 *==================================================*/
// for main game
static void cameraInit		( Mtx v );
static void drawInit		( Mtx v );
static void objectInit		( void );
static void lightInit		( Mtx v );
static void scoreInit		( void );
static void drawTick		( Mtx v );
static void drawStatus		( void );
static void moveObject		( PADStatus *pad );
static void moveCamera		( Mtx v, PADStatus *pad );
static void collisionCheck	( PADStatus *pad, PADStatus *padLast );
static BOOL signatureCheck	( f32 a, f32 b );

// for dwango sample (copied from simple2.c)
static void dwangoLibStatus 		( void );
static void printIntro				( void );

static BOOL InitializeDirectEngine	( void );
static void TerminateDirectEngine	( void );
static BOOL InitializeNetEngine 	( void );
static void TerminateNetEngine		( void );

static void DWAPI	OnDirectEngineEvent 			( _de_event_type type_event, const _de_event* p_event );
static void DWAPI	OnDirectEngineDebugMessage		( const char* msg );
static void DWAPI	OnNetEngineEvent				( _ne_event_type type_event, const _ne_event* p_event );
static BOOL DWAPI	OnNetEngineGetSyncData			( const _ne_get_sync_data* data_sync ); 
static void DWAPI	OnNetEngineGetCurrentSyncData	( u_int32 ID_clnt, u_int32 len_data, const void* data );
static void DWAPI	OnNetEngineWarning				( _ne_warning_type type_warn, const char* msg );
static void DWAPI	OnNetEngineDebugMessage 		( const char* msg );
static void DWAPI	OnNetEngineGetAsyncData 		( const _ne_get_async_data* data_async ); 	// created by ikedae

// for mic stream (compression mode : created by akagi)
static void storeVoiceData	( s32 channel, s32 result, u8 *voiceData, s32 length );
static void initForMicStart ( void );
static void drawMicStatus	( void );


/*==================================================*
	Global variables definition
 *==================================================*/
// for main game
f32 boundThetaBarToBall;
f32 barDiagonalLength;
f32	cameraZoomRate	= 1.0F;
f32 sizeTicks		= 5.0F;
u32 gameStatus		= POM_GAME_OVER;
u32 connectStatus	= (CONNECT_STATUS_CALL | CONNECT_STATUS_WAIT);

u8 score[MAXPLAYER];

Vec	camLoc	= { 0.0F, 0.0F, 500.0F };
Vec	camUp	= { 0.0F, 1.0F, 0.0F };
Vec	camTag	= { 0.0F, 0.0F, 0.0F };

Vec	ballPos[MAXBALL];
Vec ballSpd[MAXBALL];
Vec	barPos[MAXPLAYER];
Vec	barSpd[MAXPLAYER];

static Vec BallInitPos[MAXBALL] 	= { 0.0F, 0.0F, -15.0F };
static Vec BallInitSpd[MAXBALL] 	= { 1.0F, 0.0F, 0.0F };
static Vec BarInitPos[MAXPLAYER]	= { 300.0F, 0.0F, -15.0F,
										-300.0F, 0.0F, -15.0F };

// for dwango sample
static void*	sp_buf_de;
static void*	sp_buf_ne;

static u32		s_frm_cur;
static u32		s_frm_strm_cur;

static BOOL 	s_err_de;
static BOOL 	s_err_ne;
static BOOL 	s_term_de;
static BOOL 	s_term_ne;

static PADStatus	s_arry_padData_sync[CLNT_NUM_MAX];
static PADStatus	s_arry_padLastData_sync[CLNT_NUM_MAX];

static _de_country_type countryType;

// for mic stream
static u32	aramMemArray[MAX_ARAM_BLOCKS];
static u8	sendVoiceBuffer[COMP_BUFF_NUM][COMP_FRAME_SIZE_MAX];
static u8	recvVoiceBuffer[COMP_BUFF_NUM][COMP_FRAME_SIZE_MAX];
static u32	partnersID		= NE_ID_INVALID;

static BOOL startAsyncDataRecv	= FALSE;
static BOOL startStreamFlag 	= FALSE;

static u32	mdmOutCount = 0;
static u32	mdmInCount	= 0;
static u32	dspOutCount = 0;
u8 *		__cdec_heap = NULL;

// for debug
static u32		syncDataDisableCount	= 0;
static u32		asyncDataDisableCount	= 0;


/*==================================================*
	Main loop for "POM"
 *==================================================*/
#define USE_ORG 				0
#define USE_ARG 				1

void main(int argc, char *argv[])
{
	PADStatus	pad[MAXPLAYER]; 		// pad state
	PADStatus	padLast[MAXPLAYER]; 	// pad state at last step
	_de_connect con;

	static BOOL micIsNotInitialized = TRUE;

	Mtx	v;
	s32 i;

#if USE_ARG
	if(argc < 4)
	{
		OSReport("Usage : loadrun pimpom.elf -a [U(US)/J(JAPAN)]\n");
		OSReport("                              [tel number]\n");
		OSReport("                              [P(pulse)/T(tone)]\n");
		return;
	}
#endif

	DEMOInit(NULL); 			// Initialize the OS, pad, graphics and video.

	ARInit(aramMemArray, MAX_ARAM_BLOCKS);
	ARQInit();
	AIInit(NULL);
	AXInit();

	lightInit(v);				// light position = ball position(about x & y axis)
	cameraInit(v);				// Initialize the camera.
	drawInit(v);				// Initialize vertex formats and array pointers.
	objectInit();				// Initialize the positions of objects.
	scoreInit();

	// sustitute initial value
	sp_buf_de		= NULL;
	sp_buf_ne		= NULL;
	s_frm_cur		= 0;
	s_frm_strm_cur	= 0;

	s_err_de		= FALSE;
	s_err_ne		= FALSE;
	s_term_de		= FALSE;
	s_term_ne		= FALSE;

	for(i = 0; i < PAD_MAX_CONTROLLERS; i++)
	{
		pad[i].button		= 0;
		padLast[i].button	= 0;
	}

#if USE_ARG
	if(argv[1][0] == 'U')
	{
		countryType = DE_COUNTRY_US;
	}
	else	// JAPAN
	{
		countryType = DE_COUNTRY_JAPAN;
	}

	memset(&con, 0, sizeof(con));
	strcpy(con.phone_num, argv[2]);

	if(argv[3][0] == 'P')
	{
		con.type_dial = DE_DIAL_PULSE10;
	}
	else
	{
		con.type_dial = DE_DIAL_TONE;
	}

	if(strncmp(con.phone_num, "0,", 2) == 0)
	{
		con.interphone = TRUE;
	}
#else
	countryType = DE_COUNTRY_JAPAN;
	strcpy(con.phone_num, "21");
	con.type_dial = DE_DIAL_PULSE10;
#endif

	OSReport("Welcome to the \"PIMPOM for GAMECUBE\".\n");

	// display operation method
	printIntro();

	// initialize DirectEngine
	if(!InitializeDirectEngine())
	{
		return;
	}

	// loop until DirectEngine ends
	while(!s_term_de && !s_err_de)
	{
		de_Proceed();	// always use de_Proceed after DirectEngine

		s_frm_cur++;

		for(i = 0; i < MAXPLAYER; i++)
		{
			padLast[i] = pad[i];
		}

		DEMOBeforeRender();

		DEMOInitCaption(DM_FT_XLU, 320, 240);
		drawStatus();

		lightInit(v);	// light position = ball position
		cameraInit(v);	// Initialize the camera.
		drawInit(v);	// Initialize vertex formats and array pointers.
		drawTick(v);

		DEMODoneRender();

		PADRead(pad);

		if(ne_GetStatus() >= NE_STAT_INIT)
		{
			ne_Proceed();	// always use ne_Proceed after NetEngineinitialization

			if(ne_GetStatus() >= NE_STAT_STRM)
			{
				// always try to set data during stream communication
				static u8 data[STRMDATA_LEN];

				if(partnersID == NE_ID_INVALID)
				{
					do {
						partnersID = ne_GetHigherID(partnersID, TRUE);
					} while(partnersID == ne_GetOwnID());

					ASSERT(partnersID != NE_ID_INVALID);
				}

				if(ne_IsSetSyncDataEnable(0, STRMDATA_LEN))
				{
					// send pad data
					memcpy(data, &pad[0], sizeof(data));
					ne_SetSyncData(0, STRMDATA_LEN, data);
				}
				else
				{
					syncDataDisableCount++;
				}

				if(PADButtonDown(padLast[0].button, pad[0].button) & PAD_BUTTON_B)
				{
					if(ne_GetOwnID() == ne_GetAdministratorID() && ne_GetStatus() == NE_STAT_STRM)
					{
						ne_EndStream();
					}
				}
			}

			else if(ne_GetStatus() >= NE_STAT_JOINING && ne_GetStatus() != NE_STAT_LEAVING)
			{
				if(PADButtonDown(padLast[0].button, pad[0].button) & PAD_BUTTON_B)
				{
					// halt processing and try to end if B button pressed
					if(ne_GetOwnID() == ne_GetAdministratorID())
					{
						ne_DeleteSession();
					}
					else
					{
						ne_LeaveSession();
					}
				}
			}

			if(s_term_ne || s_err_ne)
			{
				// NetEngine end flag, end NetEngine if NetEngine error flag ON
				TerminateNetEngine();
			}
		}

		// ne_GetStatus() < NE_STAT_INIT
		else
		{
			if(PADButtonDown(padLast[0].button, pad[0].button) & PAD_BUTTON_B)
			{
				if(de_GetStatus() <= DE_STAT_INIT)
				{
					// end
					break;
				}
				else if(de_GetStatus() <= DE_STAT_CONNECTING)
				{
					// disconnect before end
					de_Disconnect();
				}
			}

			if(de_GetStatus() < DE_STAT_CONNECTING)
			{
				if(PADButtonDown(padLast[0].button, pad[0].button) & PAD_BUTTON_X)
				{
					// connect with X Button
					if(de_Connect(&con))
					{
						connectStatus &= CONNECT_STATUS_CALL;
						OSReport("connect start\n");
					}
				}

				if(PADButtonDown(padLast[0].button, pad[0].button) & PAD_BUTTON_Y)
				{
					// receive connect wait with Y Button
					if(de_WaitConnect(&con))
					{
						connectStatus &= CONNECT_STATUS_WAIT;
						OSReport("wait connect start\n");
					}
				}
			}
		}
	}

	TerminateNetEngine();		// end NetEngine (Call TerminateNetEngine twice if normal end. Set so relese is not called internally twice.)
	TerminateDirectEngine();	// end DirectEngine

	AXQuit();

	OSHalt("Thank you for your playing!");
}


/*==================================================*
	function for main game
 *==================================================*/
static void cameraInit(Mtx v)
{
	Mtx44 p;			// projection matrix

	f32 top 	= FIELD_Y;
	f32 left	= FIELD_X;
	f32 zNear	= 500.0F;
	f32 zFar	= 2000.0F;

#if 1
	MTXFrustum(p, top, -top, -left, left, zNear, zFar);
	GXSetProjection(p, GX_PERSPECTIVE);
#else
	MTXOrtho(p, FIELD_Y, -FIELD_Y, -FIELD_X, FIELD_X, 0.1F, 2000.0F);
	GXSetProjection(p, GX_ORTHOGRAPHIC);
#endif

	MTXLookAt(v, &camLoc, &camUp, &camTag);		
}


static void moveCamera(Mtx v, PADStatus *pad)
{
	const u8	triggerMargin = 5;
	s32 		i;

	for(i = 0; i < PAD_MAX_CONTROLLERS; i++)
	{
		if(pad[0].triggerLeft > triggerMargin || pad[0].triggerRight > triggerMargin)
		{
			cameraZoomRate = 1.0F + (pad[0].triggerLeft - pad[0].triggerRight) / 4000.0F;
			C_VECScale(&camLoc, &camLoc, cameraZoomRate);
		}
	}

	MTXLookAt(v, &camLoc, &camUp, &camTag);		
}


static void drawInit(Mtx v)
{
	GXSetCopyClear(BAR_Bk, 0x00FFFFFF);
	GXSetCullMode(GX_CULL_NONE);

	GXSetNumChans(1);		// default, color = vertex color
	GXSetNumTexGens(0); 	// it doesn't require any texture coord
	GXSetNumTevStages(1);	// only require one TEV stage

	// TEV should simply pass through color channel output
	GXSetTevOp(GX_TEVSTAGE0, GX_PASSCLR);
	GXSetTevOrder(GX_TEVSTAGE0, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR0A0);

	GXLoadPosMtxImm(v, GX_PNMTX0);

	GXClearVtxDesc();

	// set up vertex attributes
	GXSetVtxAttrFmt(GX_VTXFMT0, GX_VA_POS, GX_POS_XYZ, GX_F32, 0);
	GXSetVtxAttrFmt(GX_VTXFMT0, GX_VA_NRM, GX_NRM_XYZ, GX_F32, 0);
	GXSetVtxAttrFmt(GX_VTXFMT0, GX_VA_CLR0, GX_CLR_RGBA, GX_RGBA8, 0);

	GXSetVtxAttrFmt(GX_VTXFMT3, GX_VA_POS, GX_POS_XYZ, GX_F32, 0);
	GXSetVtxAttrFmt(GX_VTXFMT3, GX_VA_NRM, GX_NRM_XYZ, GX_F32, 0);
	GXSetVtxAttrFmt(GX_VTXFMT3, GX_VA_CLR0, GX_CLR_RGBA, GX_RGBA8, 0);

	// set up array pointers for indexed lookup
	GXSetArray(GX_VA_POS, BAR_PosArray, 3 * sizeof(f32));
	GXSetArray(GX_VA_NRM, BAR_NrmArray, 3 * sizeof(f32));
	GXSetArray(GX_VA_CLR0, BAR_ClrArray, 4 * sizeof(u8));
}


static void drawTick(Mtx v)
{
	Mtx	mP, mS, mV;
	s32 i, j;

	GXSetNumTexGens(0);
	GXSetNumTevStages(1);
	GXSetTevOp(GX_TEVSTAGE0, GX_PASSCLR);

	GXClearVtxDesc();
	GXSetVtxDesc(GX_VA_POS, GX_INDEX8);
	GXSetVtxDesc(GX_VA_NRM, GX_INDEX8);

	GXSetChanMatColor(GX_COLOR0A0, BAR_Wt);

	for(i = 0; i < MAXPLAYER; i++)
	{
		MTXTrans(mP, barPos[i].x, barPos[i].y, barPos[i].z);
		MTXConcat(v, mP, mV);
		GXLoadPosMtxImm(mV, GX_PNMTX0);

		GXBegin(GX_QUADS, GX_VTXFMT0, NUMOFBARVTX);

		for(j = 0; j < NUMOFBARVTX; j++)
		{
			GXPosition1x8((u8)j);
			GXNormal1x8((u8)j);
		}

		GXEnd();
	}

	GXClearVtxDesc();
	GXSetVtxDesc(GX_VA_POS, GX_DIRECT);
	GXSetVtxDesc(GX_VA_NRM, GX_DIRECT);

	// Draw balls
	for(i = 0; i < MAXBALL; i++)
	{
		MTXTrans(mP, ballPos[i].x, ballPos[i].y, ballPos[i].z);
		MTXConcat(v, mP, mV);

		MTXScale(mS, sizeTicks, sizeTicks, sizeTicks);
		MTXConcat(mV, mS, mV);

		GXLoadPosMtxImm(mV, GX_PNMTX0);

		GXDrawSphere(8, 16);
	}
}


static void objectInit()
{
	s32 i;

	for(i = 0; i < MAXPLAYER; i++)
	{
		barPos[i].x = BarInitPos[i].x;
		barPos[i].y = BarInitPos[i].y;
		barPos[i].z = BarInitPos[i].z;

		barSpd[i].x = 0;
		barSpd[i].y = 0;
		barSpd[i].z = 0;
	}

	for(i = 0; i < MAXBALL; i++)
	{
		ballPos[i].x = BallInitPos[i].x;
		ballPos[i].y = BallInitPos[i].y;
		ballPos[i].z = BallInitPos[i].z;

		ballSpd[i].x = BallInitSpd[i].x;
		ballSpd[i].y = BallInitSpd[i].y;
		ballSpd[i].z = BallInitSpd[i].z;
	}

	boundThetaBarToBall = atan2(BAR_HALF_Y, BAR_HALF_X);
	barDiagonalLength	= sqrt((BAR_HALF_X * BAR_HALF_X) + (BAR_HALF_Y * BAR_HALF_Y));
}


static void moveObject(PADStatus *pad)
{
	const s32	StickMargin = 10;
	s32 i;

	// bar controll(up / down)
	for(i = 0; i < MAXPLAYER; i++)
	{
		barSpd[i].y = 0;

		if(abs(pad[i].stickY) > StickMargin)
		{
			barPos[i].y += (pad[i].stickY / 10.0F);
			barSpd[i].y += (pad[i].stickY / 10.0F);

			if(barPos[i].y > (FIELD_Y - BAR_HALF_Y))
			{
				barPos[i].y = FIELD_Y - BAR_HALF_Y;
				barSpd[i].y = 0;
			}

			if(barPos[i].y < -(FIELD_Y - BAR_HALF_Y))
			{
				barPos[i].y = -(FIELD_Y - BAR_HALF_Y);
				barSpd[i].y = 0;
			}
		}
	}

	// moving balls
	for(i = 0; i < MAXBALL; i++)
	{
		ballPos[i].x += ballSpd[i].x;
		ballPos[i].y += ballSpd[i].y;
		ballPos[i].z += ballSpd[i].z;
	}

	sizeTicks *= BALL_SIZEMAG;
}


static void lightInit(Mtx v)
{
	Vec lightPos;
	Vec lightDir;

	GXColor AmbientColor = { 0x50, 0x50, 0x50, 0x50 };
	GXColor LightColor	 = { 0xFF, 0xFF, 0xFF, 0xFF };

	GXLightObj myLight;

	const f32 addZPos = 10.0F;

	lightPos.x = ballPos[0].x + 5 * ballSpd[0].x;
	lightPos.y = ballPos[0].y + 5 * ballSpd[0].y;
	lightPos.z = ballPos[0].z + addZPos;

	lightDir.x = -lightPos.x;
	lightDir.y = -lightPos.y;
	lightDir.z = -lightPos.z;

	C_VECNormalize(&lightDir, &lightDir);

	// If the light position is defined in the world space,
	// you should convert the coordinate because the lighting
	// HW is assuming view space coordinates.
	MTXMultVec(v, &lightPos, &lightPos);
	MTXMultVec(v, &lightDir, &lightDir);

	// Basical diffuse light requires position and color informations.
	GXInitLightSpot(&myLight, 80.0F, GX_SP_COS);
	GXInitLightPos(&myLight, lightPos.x, lightPos.y, lightPos.z);
	GXInitLightDir(&myLight, lightDir.x, lightDir.y, lightDir.z);
	GXInitLightDistAttn(&myLight, 10.0F, 0.5F, GX_DA_GENTLE);

	GXInitLightColor(&myLight, LightColor);

	// Once parameter initialization is done, you should load the
	// light object into hardware to make it working.
	// In this case, the object data is loaded into GX_LIGHT0.
	GXLoadLightObjImm(&myLight, GX_LIGHT0);

	// Lighting channel control
	GXSetNumChans(1);		// number of active color channels

	GXSetChanCtrl(
				GX_COLOR0,		// color channel 0
				GX_ENABLE,		// enable channel (use lighting)
				GX_SRC_REG, 	// use the register as ambient color source
				GX_SRC_REG, 	// use the register as material color source
				GX_LIGHT0,		// use GX_LIGHT0
				GX_DF_CLAMP,	// diffuse function (CLAMP = normal setting)
				GX_AF_SPOT);   // attenuation function (this case doesn't use)
	GXSetChanCtrl(
				GX_ALPHA0,		// alpha channel 0
				GX_ENABLE,		// not used in this program
				GX_SRC_REG, 	// ambient source (N/A in this case)
				GX_SRC_REG, 	// material source (N/A)
				GX_LIGHT0,		// light mask (N/A)
				GX_DF_CLAMP,	// diffuse function (N/A)
				GX_AF_SPOT);   // attenuation function (N/A)

	// ambient color register
	GXSetChanAmbColor(GX_COLOR0A0, AmbientColor);
	GXSetChanAmbColor(GX_ALPHA0, LightColor);
}


static void collisionCheck(PADStatus *pad, PADStatus *padLast)
{
#pragma unused(pad, padLast)

	f32 thetaBarToBall;
	f32 distanceBarToBall, xDistanceBarToBall, yDistanceBarToBall;
	f32 tickMargin = 15;

	f32 absBallSpdX, absBallSpdY;

	const f32 BoundDistance = barDiagonalLength + sizeTicks;

	static u8		refFlag = 1;
	static OSTick	refTick;

	static u32 pingMargin		= 48;
	static f32 ballSpdMag		= 4.0F;
	static f32 ballSpdMagMax	= 10.0F;

	s32 i, j;

	// collision check about balls and walls
	for(i = 0; i < MAXBALL; i++)
	{
		absBallSpdX = (ballSpd[i].x >= 0) ? ballSpd[i].x : -ballSpd[i].x;
		absBallSpdY = (ballSpd[i].y >= 0) ? ballSpd[i].y : -ballSpd[i].y;

		// top wall
		if(ballPos[i].y + ballSpd[i].y > FIELD_Y && ballSpd[i].y > 0)
		{
			ballSpd[i].y = -ballSpd[i].y;
		}

		// bottom wall
		if(ballPos[i].y + ballSpd[i].y < -FIELD_Y && ballSpd[i].y < 0)
		{
			ballSpd[i].y = -ballSpd[i].y;
		}

		// right wall
		if(ballPos[i].x < -FIELD_X)
		{
			score[0] += 1;
			BallInitSpd[i].x = -1.0F;
			ballSpdMag = 4.0F;
			objectInit();

			if(score[0] >= MAXSCORE)
			{
				gameStatus = POM_GAME_OVER;
			}
		}

		// left wall
		if(ballPos[i].x > FIELD_X)
		{
			score[1] += 1;
			BallInitSpd[i].x = 1.0F;
			ballSpdMag = 4.0F;
			objectInit();

			if(score[1] >= MAXSCORE)
			{
				gameStatus = POM_GAME_OVER;
			}
		}
	}

	// collision check about balls and bars
	if(refFlag == 1)
	{
		for(i = 0; i < MAXBALL; i++)
		{
			for(j = 0; j < MAXPLAYER; j++)
			{
				xDistanceBarToBall	= ballPos[i].x - barPos[j].x;
				xDistanceBarToBall	= (xDistanceBarToBall >= 0) ? xDistanceBarToBall : -xDistanceBarToBall;
				yDistanceBarToBall	= ballPos[i].y - barPos[j].y;
				yDistanceBarToBall	= (yDistanceBarToBall >= 0) ? yDistanceBarToBall : -yDistanceBarToBall;

				thetaBarToBall		= atan2(yDistanceBarToBall, xDistanceBarToBall);	
				distanceBarToBall	= sqrt((xDistanceBarToBall * xDistanceBarToBall) + (yDistanceBarToBall * yDistanceBarToBall));	

				// reflect by the top / bottom plane.
				if(thetaBarToBall >= boundThetaBarToBall)
				{
					if(distanceBarToBall <= BoundDistance
						&&	yDistanceBarToBall <= BAR_HALF_Y + sizeTicks + absBallSpdY)
					{
						if(signatureCheck(ballSpd[i].y, barSpd[j].y))
						{
							ballSpd[i].y += (barSpd[j].y * 0.5);
						}
						else
						{
							ballSpd[i].y = -ballSpd[i].y + (barSpd[j].y * 0.5F);
						}

						ballSpdMag = (ballSpdMag < ballSpdMagMax) ? ++ballSpdMag : ballSpdMagMax;

#if USE_ORG
						if(j + 1 == ne_GetOwnID())
						{
							PADStartMotor(PAD_CHAN0);
						}
#endif

						refFlag = 0;
						refTick = 0;
					}
				}

				// reflect by the side plane.
				else
				{
					if(distanceBarToBall <= BoundDistance
						&& xDistanceBarToBall <= BAR_HALF_X + sizeTicks + absBallSpdX)
					{
						// top half
						if(ballPos[i].y >= barPos[j].y)
						{
							ballSpd[i].y = sqrt(sqrt(yDistanceBarToBall));
						}
						// bottom half
						else
						{
							ballSpd[i].y = -sqrt(sqrt(yDistanceBarToBall));
						}

						ballSpd[i].y += (barSpd[j].y * 0.5F);

						// reflect by the right plane
						if(ballPos[i].x > barPos[j].x && ballSpd[i].x < 0)
						{
							ballSpd[i].x = -ballSpd[i].x;
						}

						// reflect by the left plane
						if(ballPos[i].x < barPos[j].x && ballSpd[i].x > 0)
						{
							ballSpd[i].x = -ballSpd[i].x;
						}

						ballSpdMag = (ballSpdMag < ballSpdMagMax) ? ++ballSpdMag : ballSpdMagMax;

#if USE_ORG
						if(j == ne_GetOwnID() - 1)
						{
							PADStartMotor(PAD_CHAN0);
						}

						if(j == 0)
						{
							if(padLast[j].substickX > pingMargin && pad[j].substickX < pingMargin)
							{
								ballSpdMag = 16.0F;
							}
						}
						else if(j == 1)
						{
							if(padLast[j].substickX < -pingMargin && pad[j].substickX > -pingMargin)
							{
								ballSpdMag = 16.0F;
							}
						}
#endif

						refFlag = 0;
						refTick = 0;
					}
				}

				absBallSpdX = (ballSpd[i].x >= 0) ? ballSpd[i].x : -ballSpd[i].x;
				absBallSpdY = (ballSpd[i].y >= 0) ? ballSpd[i].y : -ballSpd[i].y;

				if(absBallSpdY >= absBallSpdX * 2.0F)
				{
					ballSpd[i].y = (ballSpd[i].y >= 0) ? absBallSpdX * 2.0F : -(absBallSpdX * 2.0F);
				}

				C_VECNormalize(&ballSpd[i], &ballSpd[i]);
				C_VECScale(&ballSpd[i], &ballSpd[i], ballSpdMag);
			}
		}
	}

	else if(refTick++ > tickMargin)
	{
#if USE_ORG
		PADStopMotorHard(PAD_CHAN0);
#endif
		refFlag = 1;
	}

}


static void drawStatus()
{
#if 1
	DEMOPrintf(250, 10, 0, "SCORE:%d", score[0]);
	DEMOPrintf(10, 10, 0, "SCORE:%d", score[1]);

	if(ne_GetStatus() >= NE_STAT_STRM)
	{
//		DEMOPrintf(220, 220, 0, "FRAME : %d", ne_GetCurrentFrame());

		if(gameStatus == POM_GAME_OVER)
		{
			DEMOPrintf(90, 150, 0, "PRESS START BUTTON");
		}
	}
	else
	{
		dwangoLibStatus();

		if(connectStatus & CONNECT_STATUS_CALL)
		{
			DEMOPrintf(45, 160, 0, "X BUTTON : CONNECT ANOTHER GC");
		}
		if(connectStatus & CONNECT_STATUS_WAIT)
		{
			DEMOPrintf(45, 175, 0, "Y BUTTON : WAIT ANOTHER GC");
		}
	}
#else
	DEMOPrintf(250, 10, 0, "SYNC:%f", syncDataDisableCount);
	DEMOPrintf(10, 10, 0, "ASYNC:%f", asyncDataDisableCount);
#endif
}


static void scoreInit()
{
	s32 i;

	for(i = 0; i < MAXPLAYER; i++)
	{
		score[i] = 0;
	}
}


static BOOL signatureCheck(f32 a, f32 b)
{
	if((a > 0 && b > 0) || (a < 0 && b < 0))
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

/*==================================================*
	function for dwango library
 *==================================================*/
static BOOL InitializeDirectEngine(void)
{
	_de_initialize	init;
	u_int32 		len_buf;

	memset(&init, 0, sizeof(init));
	init.OnEventCallback		= OnDirectEngineEvent;
	init.OnDebugMessageCallback = OnDirectEngineDebugMessage;
	init.version				= DE_VERSION;			// substitute header version
	init.type_mode				= DE_MODE_RAW;			// room chat, shared memory not used so RAW mode
	init.type_country			= countryType;			// set country code

	// added by yama
	init.inf_usr.bandwidth		= 33600;
	init.inf_usr.version		= 1;
	init.inf_usr.type_platform	= 2;
	init.inf_usr.type_device	= 0;
	init.inf_usr.framerate		= 60;
	init.inf_usr.big_endian 	= 0;
	init.inf_usr.len_mem		= 24000;

	// calculate required buffer size
	len_buf = de_CalculateBufferSize(&init);
	if(len_buf == 0)
	{
		OSReport("directengine calculate buffer size failed (function call)\n");
		return (FALSE);
	}

	// set aside buffer size for memory
	sp_buf_de = OSAlloc(len_buf);
	if(sp_buf_de == NULL)
	{
		OSReport("OSAlloc failed (function call)\n");
		return (FALSE);
	}

	// initialize DirectEngine
	if (!de_Initialize(&init, sp_buf_de))
	{
		// release memory if fail and end
		OSReport("directengine initialize failed (function call)\n");
		OSFree(sp_buf_de);
		sp_buf_de	= NULL;
		return (FALSE);
	}

	return (TRUE);
}


static void TerminateDirectEngine(void)
{
	s_term_de	= FALSE;
	s_err_de	= FALSE;

	if (de_GetStatus() >= DE_STAT_INIT)
	{
		// end if initializing
		de_Terminate();
	}

	if (sp_buf_de != NULL)
	{
		// release if memory set aside
		OSFree(sp_buf_de);
		sp_buf_de	= NULL;
	}
}


// event callback function for DirectEngine
static void DWAPI OnDirectEngineEvent(_de_event_type type_event, const _de_event* p_event)
{
	// display event number
	OSReport("de event:%u\n", type_event);

	switch (type_event)
	{
		case DE_EVENT_CONNECTED:	// connection end
			if (p_event->connect.type_connect != DE_CONNECT_SUCCEED)
			{
				// connection failure  end (if succeeds, game starts automatically if RAW mode)
				OSReport("connect failed %d\n", p_event->connect.type_connect);
				connectStatus	= (CONNECT_STATUS_CALL | CONNECT_STATUS_WAIT);		// try again
			}
			break;

		case DE_EVENT_BPS:			// show connection speed
			// display
			OSReport("bps:%u\n", p_event->bps);
			break;

		case DE_EVENT_DISCONNECTED:	// break circuit
			// DirectEngine end flag ON (end in callback cannot be done)
			s_term_de	= TRUE;
			break;

		case DE_EVENT_BEGAN:		// game start
			// initialize NetEngine
			if (!InitializeNetEngine())
			{
				// initialization failure  disconnect and end
				if (de_IsHost())
				{
					// To end normally, start from game end for host.
					if (!de_EndGameMode())
					{
						// end failure  end
						OSReport("end game mode failed (function call)\n");
						s_err_de	= TRUE;
					}
				}
				else
				{
					if (!de_Disconnect())
					{
						// disconnect failure  end
						OSReport("disconnect failed (function call)\n");
						s_err_de	= TRUE;
					}
				}
			}
			break;
	}
}


// DEBUG output callback function for DirectEngine
static void DWAPI OnDirectEngineDebugMessage(const char* msg)
{
	// display with no change
	OSReport("de :%s\n", msg);
}


static BOOL InitializeNetEngine(void)
{
	_ne_initialize			init;
	_ne_stream_data_info	arry_inf_strm[ 2 ];
	_ne_session_info		inf_ssn;
	u_int32 				len_buf;

	memset(&init, 0, sizeof(init));

	init.version								= NE_VERSION;		// substitute header version
	init.bandwidth								= 33600;			// communication is 33600bps
	init.init_ssn.mode.p2p						= TRUE; 			// use P2P communication

	// Streaming communication, synchronized communication, asynchronous communication, and joining session in middle set to default
	init.init_ssn.mode.join_strm_auto			= TRUE;
	init.init_ssn.num_clnt_max					= CLNT_NUM_MAX; 	// Client maximum is CLNT_NUM_MAX
	init.init_ssn.num_ssn_max					= 1;				// only 1 section needed
	strcpy(init.init_ssn.name_game, "simple");
	init.init_strm.mode.sync					= TRUE;
	init.init_strm.mode.async					= TRUE;
	init.init_strm.inf.sync_async.num_inf_strm	= 2;				// synchronized data * 1 + ansynchronous data * 1
	init.init_strm.inf.sync_async.arry_inf_strm = arry_inf_strm;	// provide array for synchronized data to be used
	init.init_strm.sync.num_buf_sync_max		= 60;				// stack in buffer up to 60
	strcpy(init.name_own, "test name");
	init.OnDebugMessageCallback 				= OnNetEngineDebugMessage;
	init.OnEventCallback						= OnNetEngineEvent;
	init.OnGetCurrentSyncDataCallback			= OnNetEngineGetCurrentSyncData;
	init.OnGetSyncDataCallback					= OnNetEngineGetSyncData;
	init.OnGetAsyncDataCallback 				= OnNetEngineGetAsyncData;
	init.OnWarningCallback						= OnNetEngineWarning;
	init.time_interval							= 16;				// move forward at 16 milliseconds
	init.time_timeout							= 20000;			// basic value for timeout is 20 seconds
	init.type_proto 							= NE_PROTO_DIRECT;	// protocol is for DirectEngine

	memset(arry_inf_strm, 0, sizeof(arry_inf_strm));

	// synchronized data (PAD data for send/receive)
	arry_inf_strm[0].type_strm					= NE_STRM_SYNC; 	// streaming type is synchronized data
	arry_inf_strm[0].frm_interval				= 1;				// send in 1 frame (16 milliseconds)
	arry_inf_strm[0].len_data					= STRMDATA_LEN; 	// data size is STRMDATA_LEN
	arry_inf_strm[0].num_pack					= 1;				// pack number is 1
	arry_inf_strm[0].num_retry					= 2;				// retry 2 times (prevent packet loss)
	arry_inf_strm[0].var_len					= FALSE;			// data length is fixed (always STRMDATA_LEN)

	// asynchronous data (Voice data send)
	arry_inf_strm[ 1 ].type_strm				= NE_STRM_ASYNC;	// asynchronous data
	arry_inf_strm[ 1 ].frm_interval 			= 1;
	arry_inf_strm[ 1 ].len_data 				= ASYNCDATA_LEN;
	arry_inf_strm[ 1 ].num_pack 				= 1;
	arry_inf_strm[ 1 ].num_retry				= 1;
	arry_inf_strm[ 1 ].option.async.pri 		= 4;
	arry_inf_strm[ 1 ].option.async.type_frm	= NE_FRM_8;

	// calculate required buffer size
	len_buf = ne_CalculateBufferSize(&init);
	if(len_buf == 0)			
	{
		OSReport("netengine calculate buffer size failed (function call)\n");
		return (FALSE);
	}

	// 
	sp_buf_ne = OSAlloc(len_buf);
	if(sp_buf_ne == NULL)
	{
		OSReport("OSAlloc failed (function call)\n");
		return (FALSE);
	}

	// initialize NetEngine
	if(!ne_Initialize(&init, sp_buf_ne))
	{
		// if fail release memory and end
		OSReport("netengine initialize failed (function call)\n");
		OSFree(sp_buf_ne);
		sp_buf_ne = NULL;
		return (FALSE);
	}

	memset(&inf_ssn, 0, sizeof(inf_ssn));
	strcpy(inf_ssn.name, "test name");
	inf_ssn.num_clnt_max = CLNT_NUM_MAX;	// client maximum is CLNT_NUM_MAX

        // Join session (If NE_PROTO_DIRECT, second argument ignored. Depending on if host with DirectEngine
        // session created and able to join or not.
	if(!ne_JoinSession(&inf_ssn, ""))
	{
		// If fail end NetEngine and release memory
		OSReport("join session failed (function call)\n");
		ne_Terminate();
		OSFree(sp_buf_ne);
		sp_buf_ne = NULL;
		return (FALSE);
	}

	return (TRUE);
}


static void TerminateNetEngine(void)
{
	s_term_ne	= FALSE;
	s_err_ne	= FALSE;

	if (ne_GetStatus() >= NE_STAT_INIT)
	{
		// end if initialized
		ne_Terminate();
	}

	if (sp_buf_ne != NULL)
	{
		// releaes if memory set aside
		OSFree(sp_buf_ne);
		sp_buf_ne	= NULL;
	}

	if (de_IsHost())
	{
		// end game if host (Other: disconnect)
		de_EndGameMode();
	}
}


// NetEngine callback function : called when you got event information
static void DWAPI OnNetEngineEvent(_ne_event_type type_event, const _ne_event* p_event)
{
	// display event
	OSReport("ne event:%s\n", ne_EventToString(type_event));

	switch(type_event)
	{
		case NE_EVENT_SSN_CLNT_ADDED:	// Add client
			if(ne_GetAdministratorID() == ne_GetOwnID())
			{
				// If you are admin start streaming communication at point when target joins
				if(!ne_BeginStream(FALSE))	
				{
					// start failure  end and disconnect
					OSReport("begin stream failed (function call)\n");
					s_err_ne = TRUE;
				}
			}
			break;

		case NE_EVENT_SSN_JOINED:		// join session
			if(p_event->joined_ssn.type_join != NE_JOIN_SUCCEED)
			{
				// join failure  end and disconnect
				OSReport("join session failed %d\n", p_event->joined_ssn.type_join);
				s_err_ne = TRUE;
			}
			break;

		case NE_EVENT_SSN_LEFT: 		// Leave session
			// NetEngine end flag on (cannot end in callback)
			s_term_ne = TRUE;
			break;

		case NE_EVENT_STRM_BEGAN:		// begin streaming communication
			if(p_event->began_strm.type_begin != NE_BEGIN_SUCCEED)
			{
				// start failure  end and disconnect
				OSReport("begin stream failed %d\n", p_event->began_strm.type_begin);
				s_err_ne = TRUE;
			}
			break;

		case NE_EVENT_STRM_ENDED:		// end streaming communication
			if(ne_GetAdministratorID() == ne_GetOwnID())
			{
				// If you are admin delete session at point when streaming communication ends (if delete session, target automatically quits)
				if(!ne_DeleteSession())
				{
					// delete failure  end and disconnect
					OSReport("delete session failed (function call)\n");
					s_err_ne = TRUE;
				}
			}
			break;

		case NE_EVENT_SYNC_CURDATA_SET: // data request for synchronization match
			// Not significant, but set current frame (if game, exchange data to match info with people who joined in middle)	
			if(!ne_SetCurrentSyncData(p_event->data_cur_strm_set.ID_clnt, sizeof(s_frm_cur), &s_frm_cur))
			{
				// setting failure   end and disconnect
				OSReport("set current syncdata failed (function call)\n");
				s_err_ne = TRUE;
			}
			break;
	}
}


// NetEngine callback function : called when you got synchronous data
static BOOL DWAPI OnNetEngineGetSyncData(const _ne_get_sync_data* data_sync)
{
	static BOOL	startButtonFlag[CLNT_NUM_MAX] = {FALSE, FALSE};
	u32 		clientID;
	s32 		i;

	for(i = 0; i < CLNT_NUM_MAX; i++)
	{
		s_arry_padLastData_sync[i] = s_arry_padData_sync[i];
	}

	for(i = 0; i < data_sync->num_data; i++)
	{
		ASSERT(data_sync->arry_data[i].ID_clnt >= 1);
		clientID = data_sync->arry_data[i].ID_clnt - 1;

		// store pad data with "s_arry_padData_sync"
		memcpy(&s_arry_padData_sync[clientID], data_sync->arry_data[i].data, data_sync->arry_data[i].len_data);

		if(PADButtonDown(s_arry_padLastData_sync[clientID].button, s_arry_padData_sync[clientID].button) & PAD_BUTTON_MENU)
		{
			OSReport("Start button was pushed at client %d.\n", clientID);
			startButtonFlag[clientID] = TRUE;
		}
	}

	// move time ahead in game
	// If game, move characters, etc.
	if(startButtonFlag[0] == TRUE || startButtonFlag[1] == TRUE)
	{
		gameStatus = POM_NOW_PLAYING;		

		for(i = 0; i < CLNT_NUM_MAX; i++)
		{
			startButtonFlag[i] = FALSE;
		}
	}

	s_frm_strm_cur++;

	switch(gameStatus)
	{
		case POM_NOW_PLAYING:
			moveObject(s_arry_padData_sync);
			break;
		case POM_RESTART:
			objectInit();
			break;
		case POM_GAME_OVER:
			objectInit();
			scoreInit();
			break;
	}

	collisionCheck(s_arry_padData_sync, s_arry_padLastData_sync);

	return (TRUE);
}


// NetEngine callback function : called when you got asynchronous data
static void DWAPI OnNetEngineGetAsyncData(const _ne_get_async_data* data_async)
{
	static s32	asynccount = 0;

	if (data_async->ID_clnt == ne_GetOwnID())
	{
		return;
	}

	if(!startAsyncDataRecv)
	{
		startAsyncDataRecv = TRUE;
	}

	// move time ahead in game
	// If game, move characters, etc.
	asynccount++;
//	OSReport("async %d\n", asynccount);

	memset(recvVoiceBuffer[mdmInCount], 0x00, COMP_FRAME_SIZE_MAX);
	memcpy(recvVoiceBuffer[mdmInCount], data_async->data, data_async->len_data);

	mdmInCount++;
	mdmInCount = (mdmInCount >= COMP_BUFF_NUM) ? 0 : mdmInCount;
}


// NetEngine callback function : called when you got data to adjust game information
static void DWAPI OnNetEngineGetCurrentSyncData(u_int32 ID_clnt, u_int32 len_data, const void* data)
{
	u_int32	frm;

	len_data;

	// Not significant, but receive current frame (If game, exchange data to match info with people who joined in middle) 
	memcpy(&frm, data, sizeof(frm));
	OSReport("recv current frame ID %u frame %u\n", ID_clnt, frm);
}


// NetEngine callback function : called when you got warning
static void DWAPI OnNetEngineWarning(_ne_warning_type type_warn, const char* msg)
{
	// output warning content
	OSReport("ne warning:%s %s\n", ne_WarningToString(type_warn), msg);
}


// NetEngine callback function : called when you got DEBUG output information
static void DWAPI OnNetEngineDebugMessage(const char* msg)
{
	// display with no changes
	OSReport("ne :%s\n", msg);
}


// display current status of DirectEngine and NetEngine
static void dwangoLibStatus(void)
{
	char *	tmpString;

	// display program count
	DEMOPrintf(60, 30, 0, "FRAME : %u\n", s_frm_cur);

	// display DirectEngine status
	switch(de_GetStatus())
	{
		case DE_STAT_NOTINIT:		tmpString = "NOTINIT";			break;
		case DE_STAT_INIT:			tmpString = "INIT"; 			break;
		case DE_STAT_CONNECTING:	tmpString = "CONNECTING";		break;
		case DE_STAT_DISCONNECTING: tmpString = "DISCONNECTING";	break;
		case DE_STAT_CONNECTED: 	tmpString = "CONNECTED";		break;
		case DE_STAT_JOINING:		tmpString = "JOINING";			break;
		case DE_STAT_LEAVING:		tmpString = "LEAVING";			break;
		case DE_STAT_JOINED:		tmpString = "JOINED";			break;
		case DE_STAT_DATA_SENDING:	tmpString = "DATA_SENDING"; 	break;
		case DE_STAT_BEGINNING: 	tmpString = "BEGINNING";		break;
		case DE_STAT_ENDING:		tmpString = "ENDING";			break;
		case DE_STAT_GAME:			tmpString = "GAME"; 			break;
		default:					tmpString = "UNKNOWN";			break;
	}
	DEMOPrintf(60, 40, 0, "DE STATUS : %s", tmpString);

	// display NetEngine status
	switch(ne_GetStatus())
	{
		case NE_STAT_NOTINIT:		tmpString = "NOTINIT";			break;
		case NE_STAT_INIT:			tmpString = "INIT"; 			break;
		case NE_STAT_JOINING:		tmpString = "JOINING";			break;
		case NE_STAT_LEAVING:		tmpString = "LEAVING";			break;
		case NE_STAT_JOINED:		tmpString = "JOINED";			break;
		case NE_STAT_BEGINNING: 	tmpString = "BEGINNING";		break;
		case NE_STAT_STRM:			tmpString = "STRM"; 			break;
		case NE_STAT_ENDING:		tmpString = "ENDING";			break;
		default:					tmpString = "UNKNOWN";			break;
	}
	DEMOPrintf(60, 50, 0, "NE STATUS : %s", tmpString);
	
	if(ne_GetStatus() < NE_STAT_JOINED)
	{
		return;
	}
	else if(ne_GetStatus() < NE_STAT_STRM)
	{
		u32 tmpID	= 0;
		s32 count	= 0;

		// display client number
		DEMOPrintf(60, 70 + (CLNT_NUM_MAX * 10), 0, "CLIENT NUM : %u", ne_GetClientNum());

		// display client details
		count	= 0;
		tmpID	= ne_GetHigherID(NE_ID_INVALID, FALSE);
		while(tmpID != NE_ID_INVALID)
		{
			_ne_client_info	inf;

			ne_GetClientInfo(tmpID, &inf);
			DEMOPrintf(60, (s16)(80 + (CLNT_NUM_MAX * 10) + (count * 10)), 0, "ID : %u NAME : %s LATENCY : %u", tmpID, inf.name, inf.time_latency);
			tmpID = ne_GetHigherID(tmpID, FALSE);
			count++;
			ASSERT(count < 3);
		}
	}
	else
	{
		u32 tmpID  = 0;
		s32 count	= 0;

		// display counter during streaming communication
		DEMOPrintf(60, 70, 0, "STREAM FRAME : %u", s_frm_strm_cur);

		count	= 0;
		tmpID  = ne_GetHigherID(NE_ID_INVALID, TRUE);
		while(tmpID != NE_ID_INVALID)
		{
			// display results synchronized data
			DEMOPrintf(60, (s16)(80 + (count * 10)), 0, "ID : %u", tmpID);
			tmpID = ne_GetHigherID(tmpID, TRUE);
			count++;
			ASSERT(count < 3);
		}
	}
}


// output control method to DEBUG console
static void printIntro(void)
{
	OSReport("\n");
	OSReport("***************************************************************\n");
	OSReport(" pimpom    : demonstrate netEnigne / directEngine \n");
	OSReport("***************************************************************\n");
	OSReport("\n");
	OSReport("  X button : call de_Connect\n");
	OSReport("  Y button : call de_WaitConnect\n");
	OSReport("  B button : in streaming -> end, in connecting -> disconnect \n");
	OSReport("\n");
	OSReport("***************************************************************\n");
	OSReport("\n");
}
