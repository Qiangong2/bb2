/*--------------------------------------------------------------------------
//
//      Copyright (C) 2000 DWANGO Corporation.  All Rights Reserved.
//
//      $RCSfile: simple.c,v $
//      $Date: 2004/03/15 21:58:02 $ 
//      $Revision: 1.1.1.1 $
//      Content:        NetEngine & DirectEngine simple sample source.
//
//-------------------------------------------------------------------------*/

#include <dolphin.h>
#include <demo.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <dolphin/h2h/directengine.h>
#include <dolphin/h2h/netengine.h>

#define STRMDATA_LEN    8               // set streaming communication variable length data for test to maximum of 8
#define CLNT_NUM_MAX    2               // client maximum

static void*            sp_buf_de;              // set aside for internal buffer for DirectEngine
static void*            sp_buf_ne;              // set aside for internal buffer for NetEngine

static u_int32          s_frm_cur;              // frames for current, main loop (test program display)
static u_int32          s_frm_strm_cur; // frames for current streaming communication (test program display)

static BOOL                     s_err_de;               // if some processing for DirectEngine fails and abnormal end
static BOOL                     s_err_ne;               // if some processing for NetEngine fails and abnormal endNetEngine
static BOOL                     s_term_de;              // de_Terminate not called from callback so flag ON and end externally
static BOOL                     s_term_ne;              // ne_Terminate not called from callback so flag ON and end externally

static s_int32          s_arry_data_sync[ CLNT_NUM_MAX ];       // buffer (test program display) to save synchronized data(summy frame number)

static BOOL                     InitializeDirectEngine( void );
static void                     TerminateDirectEngine( void );
static BOOL                     InitializeNetEngine( void );
static void                     TerminateNetEngine( void );

static void                     ViewCurrentStatus( void );
static void                     PrintIntro( void );

static void DWAPI       OnDirectEngineEvent( _de_event_type type_event, const _de_event* p_event );
static void DWAPI       OnDirectEngineDebugMessage( const char* msg );
static void DWAPI       OnNetEngineEvent( _ne_event_type type_event, const _ne_event* p_event );
static BOOL DWAPI       OnNetEngineGetSyncData( const _ne_get_sync_data* data_sync ); 
static void DWAPI       OnNetEngineGetCurrentSyncData( u_int32 ID_clnt, u_int32 len_data, const void* data );
static void DWAPI       OnNetEngineWarning( _ne_warning_type type_warn, const char* msg );
static void DWAPI       OnNetEngineDebugMessage( const char* msg );

void main( void )
{
        // substitute initial value
        sp_buf_de               = NULL;
        sp_buf_ne               = NULL;
        s_frm_cur               = 0;
        s_frm_strm_cur  = 0;

        s_err_de                = FALSE;
        s_err_ne                = FALSE;
        s_term_de               = FALSE;
        s_term_ne               = FALSE;

        DEMOInit( NULL );

        // display operation method
        PrintIntro();

        // initialize DirectEngine
        if ( !InitializeDirectEngine() )        return;

        // Dloop until DirectEngine ends
        while ( !s_term_de && !s_err_de )
        {
                static PADStatus        pad_old;
                PADStatus                       arry_pad[ PAD_MAX_CONTROLLERS ];

                de_Proceed();   // always use de_Proceed after DirectEngine initialization

                s_frm_cur       ++;

                pad_old.button  = arry_pad[ 0 ].button;

                DEMOBeforeRender();
                GXSetCopyClear( ( GXColor ){ 0x00, 0x00, 0x00, 0x00 }, 0x00FFFFFF );
                DEMOInitCaption( DM_FT_XLU, 320, 240 );
                ViewCurrentStatus();
                DEMODoneRender();
                PADRead( arry_pad );

                if ( ne_GetStatus() >= NE_STAT_INIT )
                {
                        ne_Proceed();   // always use ne_Proceed after NetEngine initialization

                        if ( ne_GetStatus() >= NE_STAT_STRM )
                        {
                                // always try to set data during stream communication
                                u_int8  data[ STRMDATA_LEN ];

                                if ( ne_IsSetSyncDataEnable( 0, STRMDATA_LEN ) )
                                {
                                        // send dummy frame number if can set synchronized data 0
                                        memcpy( data, &s_frm_cur, sizeof( s_frm_cur ) );
                                        ne_SetSyncData( 0, STRMDATA_LEN, data );
                                }

                                if ( ( PADButtonDown( pad_old.button, arry_pad[ 0 ].button ) & PAD_BUTTON_B ) )
                                {
                                        if ( ne_GetOwnID() == ne_GetAdministratorID() && ne_GetStatus() == NE_STAT_STRM )
                                        {
                                                ne_EndStream();
                                        }
                                }
                        }
                        else if ( ne_GetStatus() >= NE_STAT_JOINING && ne_GetStatus() != NE_STAT_LEAVING )
                        {
                                if ( ( PADButtonDown( pad_old.button, arry_pad[ 0 ].button ) & PAD_BUTTON_B ) )
                                {
                                        // halt processing and try to end if B button pressed
                                        if ( ne_GetOwnID() == ne_GetAdministratorID() )
                                        {
                                                ne_DeleteSession();
                                        }
                                        else
                                        {
                                                ne_LeaveSession();
                                        }
                                }
                        }

                        if ( s_term_ne || s_err_ne )
                        {
                                // End NetEngine if NetEngine end flag or NetEngine Error flag ON
                                TerminateNetEngine();
                        }
                }
                else
                {
                        if ( ( PADButtonDown( pad_old.button, arry_pad[ 0 ].button ) & PAD_BUTTON_B ) )
                        {
                                if ( de_GetStatus() <= DE_STAT_INIT )
                                {
                                        // end
                                        break;
                                }
                                else if ( de_GetStatus() <= DE_STAT_CONNECTING )
                                {
                                        // disconnect in middle
                                        de_Disconnect();
                                }
                        }

                        if ( de_GetStatus() < DE_STAT_CONNECTING )
                        {
                                _de_connect     con;

                                memset( &con, 0, sizeof( con ) );

                                strcpy( con.phone_num, "11" );          // enter test telephone number (change to appropriate number if using this sample)
                                con.type_dial   = DE_DIAL_PULSE10;      // enter test circuit type (change to appropriate circuit type if using this sample)

                                if ( ( PADButtonDown( pad_old.button, arry_pad[ 0 ].button ) & PAD_BUTTON_X ) )
                                {
                                        // connect with X Button
                                        if ( de_Connect( &con ) )
                                        {
                                                OSReport( "connect start\n" );
                                        }
                                }
                                else if ( ( PADButtonDown( pad_old.button, arry_pad[ 0 ].button ) & PAD_BUTTON_Y ) )
                                {
                                        // receive connect wait with Y Button
                                        if ( de_WaitConnect( &con ) )
                                        {
                                                OSReport( "wait connect start\n" );
                                        }
                                }
                        }
                }
        }

        TerminateNetEngine();           // end NetEngine (if normal end, call TerminateNetEngine twice.  Avoid calling release twice internally.)
        TerminateDirectEngine();        // end DirectEngine
}

// initialize DirectEngine
static BOOL InitializeDirectEngine( void )
{
        _de_initialize  init;
        u_int32                 len_buf;

        memset( &init, 0, sizeof( init ) );
        init.OnEventCallback            = OnDirectEngineEvent;
        init.OnDebugMessageCallback     = OnDirectEngineDebugMessage;
        init.version                            = DE_VERSION;           // substitute header version
        init.type_mode                          = DE_MODE_RAW;          // room chat, shared memory not used so RAW mode
        init.type_country                       = DE_COUNTRY_JAPAN;     // set country to Japan

        // calculate required buffer size
        len_buf         = de_CalculateBufferSize( &init );
        if ( len_buf == 0 )
        {
                OSReport( "directengine calculate buffer size failed ( function call )\n" );
                return ( FALSE );
        }

        // set aside buffer size for memory
        sp_buf_de       = OSAlloc( len_buf );
        if ( sp_buf_de == NULL )
        {
                OSReport( "OSAlloc failed ( function call )\n" );
                return ( FALSE );
        }

        // initialize DirectEngine
        if ( !de_Initialize( &init, sp_buf_de ) )
        {
                // release memory if fail and end
                OSReport( "directengine initialize failed ( function call )\n" );
                OSFree( sp_buf_de );
                sp_buf_de       = NULL;
                return ( FALSE );
        }

        return ( TRUE );
}

// end DirectEngine
static void TerminateDirectEngine( void )
{
        s_term_de       = FALSE;
        s_err_de        = FALSE;

        if ( de_GetStatus() >= DE_STAT_INIT )
        {
                // end if initializing
                de_Terminate();
        }

        if ( sp_buf_de != NULL )
        {
                // release if memory set aside
                OSFree( sp_buf_de );
                sp_buf_de       = NULL;
        }
}

// event callback function for DirectEngine
static void DWAPI OnDirectEngineEvent( _de_event_type type_event, const _de_event* p_event )
{
        // display event number
        OSReport( "de event:%u\n", type_event );

        switch ( type_event )
        {
        case DE_EVENT_CONNECTED:        // connection end
                if ( p_event->connect.type_connect != DE_CONNECT_SUCCEED )
                {
                               // connection failure  end (if succeeds, game starts automatically if RAW mode)
                        OSReport( "connect failed %d\n", p_event->connect.type_connect );
                        s_err_de        = TRUE;
                }
                break;

        case DE_EVENT_BPS:                      // show connection speed
                        // display
                OSReport( "bps:%u\n", p_event->bps );
                break;

        case DE_EVENT_DISCONNECTED:     // break circuit
                // DirectEngine end flag ON (end in callback cannot be done)
                s_term_de       = TRUE;
                break;

        case DE_EVENT_BEGAN:            // game start
                // initialize NetEngine
                if ( !InitializeNetEngine() )
                {
                        // initialization failure  disconnect and end
                        if ( de_IsHost() )
                        {
                                // To end normally, start from game end for host.
                                if ( !de_EndGameMode() )
                                {
                                        // end failed  end
                                        OSReport( "end game mode failed ( function call )\n" );
                                        s_err_de        = TRUE;
                                }
                        }
                        else
                        {
                                if ( !de_Disconnect() )
                                {
                                        // disconnect failed  end
                                        OSReport( "disconnect failed ( function call )\n" );
                                        s_err_de        = TRUE;
                                }
                        }
                }
                break;
        }
}

// DirectEngine debug output callback
static void DWAPI OnDirectEngineDebugMessage( const char* msg )
{
        // display without change
        OSReport( "de :%s\n", msg );
}

// initialize NetEngine
static BOOL InitializeNetEngine( void )
{
        _ne_initialize                  init;
        _ne_stream_data_info    arry_inf_strm[ 1 ];
        _ne_session_info                inf_ssn;
        u_int32                                 len_buf;

        memset( &init, 0, sizeof( init ) );

        init.version                                                            = NE_VERSION;           // substitute header version
        init.bandwidth                                                          = 33600;                        // communication is 33600bps
        init.init_ssn.mode.p2p                                          = TRUE;                         // use P2P communication
        init.init_ssn.mode.join_strm_auto                       = TRUE;                         // those that join streaming communication in middle, join automatically
        init.init_ssn.num_clnt_max                                      = CLNT_NUM_MAX;         // Client maximum is CLNT_NUM_MAX
        init.init_ssn.num_ssn_max                                       = 1;                            // only one session needed
        strcpy( init.init_ssn.name_game, "simple" );
        init.init_strm.mode.sync                                        = TRUE;                         // use synchronized communication only
        init.init_strm.inf.sync_async.num_inf_strm      = 1;                            // use one synchronized data
        init.init_strm.inf.sync_async.arry_inf_strm     = arry_inf_strm;        // provide array for synchronized data to be used
        init.init_strm.sync.num_buf_sync_max            = 60;                           // 6stack in buffer up to 60
        strcpy( init.name_own, "test name" );
        init.OnDebugMessageCallback                                     = OnNetEngineDebugMessage;
        init.OnEventCallback                                            = OnNetEngineEvent;
        init.OnGetCurrentSyncDataCallback                       = OnNetEngineGetCurrentSyncData;
        init.OnGetSyncDataCallback                                      = OnNetEngineGetSyncData;
        init.OnWarningCallback                                          = OnNetEngineWarning;
        init.time_interval                                                      = 16;                           // move forward at 16 milliseconds
        init.time_timeout                                                       = 20000;                        // basic value for timeout is 20 seconds
        init.type_proto                                                         = NE_PROTO_DIRECT;      // protocol is for DirectEngine

        memset( arry_inf_strm, 0, sizeof( arry_inf_strm ) );

        // use one sychronoized data
        arry_inf_strm[ 0 ].type_strm                            = NE_STRM_SYNC;         // streaming type is synchronized data
        arry_inf_strm[ 0 ].frm_interval                         = 1;                            // send in 1 frame (16 milliseconds)
        arry_inf_strm[ 0 ].len_data                                     = STRMDATA_LEN;         // data size is STRMDATA_LEN
        arry_inf_strm[ 0 ].num_pack                                     = 1;                            // pack number is 1
        arry_inf_strm[ 0 ].num_retry                            = 2;                            // retry 2 times (prevent packet loss)
        arry_inf_strm[ 0 ].var_len                                      = FALSE;                        // data length is fixed (always STRMDATA_LEN)

        // calculate required buffer size
        len_buf         = ne_CalculateBufferSize( &init );
        if ( len_buf == 0 )                     
        {
                OSReport( "netengine calculate buffer size failed ( function call )\n" );
                return ( FALSE );
        }

        // set aside buffer size of memory
        sp_buf_ne       = OSAlloc( len_buf );
        if ( sp_buf_ne == NULL )
        {
                OSReport( "OSAlloc failed ( function call )\n" );
                return ( FALSE );
        }

        // initialize NetEngine
        if ( !ne_Initialize( &init, sp_buf_ne ) )
        {
                // if fail release memory and end
                OSReport( "netengine initialize failed ( function call )\n" );
                OSFree( sp_buf_ne );
                sp_buf_ne       = NULL;
                return ( FALSE );
        }

        memset( &inf_ssn, 0, sizeof( inf_ssn ) );
        strcpy( inf_ssn.name, "test name" );
        inf_ssn.num_clnt_max    = CLNT_NUM_MAX; // client maximum is CLNT_NUM_MAX

        // Join session (If NE_PROTO_DIRECT, second argument ignored. Depending on if host with DirectEngine
        // ession created and able to join or not.
        if ( !ne_JoinSession( &inf_ssn, "" ) )
        {
                // If fail end NetEngine and release memory
                OSReport( "join session failed ( function call )\n" );
                ne_Terminate();
                OSFree( sp_buf_ne );
                sp_buf_ne       = NULL;
                return ( FALSE );
        }

        return ( TRUE );
}

// end NetEngine
static void TerminateNetEngine( void )
{
        s_term_ne       = FALSE;
        s_err_ne        = FALSE;

        if ( ne_GetStatus() >= NE_STAT_INIT )
        {
                // end if initialized
                ne_Terminate();
        }

        if ( sp_buf_ne != NULL )
        {
                // release if memory set aside
                OSFree( sp_buf_ne );
                sp_buf_ne       = NULL;
        }

        if ( de_IsHost() )
        {
                // end game if host (Other: disconnect)
                de_EndGameMode();
        }
}

// NetEngine event callback
static void DWAPI OnNetEngineEvent( _ne_event_type type_event, const _ne_event* p_event )
{
        // display event
        OSReport( "ne event:%s\n", ne_EventToString( type_event ) );

        switch ( type_event )
        {
        case NE_EVENT_SSN_CLNT_ADDED:   // Add client
                if ( ne_GetAdministratorID() == ne_GetOwnID() )
                {
                        // If you are admin start streaming communication at point when target joins
                        if ( !ne_BeginStream( FALSE ) ) 
                        {
                                // start failure  end and disconnect
                                OSReport( "begin stream failed ( function call )\n" );
                                s_err_ne        = TRUE;
                        }
                }
                break;

        case NE_EVENT_SSN_JOINED:               // join session
                if ( p_event->joined_ssn.type_join != NE_JOIN_SUCCEED )
                {
                        // join failure  end and disconnect
                        OSReport( "join session failed %d\n", p_event->joined_ssn.type_join );
                        s_err_ne        = TRUE;
                }
                break;

        case NE_EVENT_SSN_LEFT:                 // Leave session
                // NetEngine end flag on (cannot end in callback)
                s_term_ne       = TRUE;
                break;

        case NE_EVENT_STRM_BEGAN:               // begin streaming communication
                if ( p_event->began_strm.type_begin != NE_BEGIN_SUCCEED )
                {
                        // start failure  end and disconnect
                        OSReport( "begin stream failed %d\n", p_event->began_strm.type_begin );
                        s_err_ne        = TRUE;
                }
                break;

        case NE_EVENT_STRM_ENDED:               // end streaming communication
                if ( ne_GetAdministratorID() == ne_GetOwnID() )
                {
                        // If you are admin delete session at point when streaming communication ends (if delete session, target automatically quits)
                        if ( !ne_DeleteSession() )
                        {
                                // delete failure  end and disconnect
                                OSReport( "delete session failed ( function call )\n" );
                                s_err_ne        = TRUE;
                        }
                }
                break;

        case NE_EVENT_SYNC_CURDATA_SET: // request data to match synchronization
                // Not significant, but set current frame (if game, exchange data to match info with people who joined in middle)
                if ( !ne_SetCurrentSyncData( p_event->data_cur_strm_set.ID_clnt, sizeof( s_frm_cur ), &s_frm_cur ) )
                {
                        // setting failure  end and disconnect
                        OSReport( "set current syncdata failed ( function call )\n" );
                        s_err_ne        = TRUE;
                }
                break;
        }
}

// callback of synchronized data acquire
static BOOL DWAPI OnNetEngineGetSyncData( const _ne_get_sync_data* data_sync )
{
        s_int32 i;

        for ( i = 0; i < data_sync->num_data; i ++ )
        {
                // copy dummy frame number
                memcpy( &s_arry_data_sync[ i ], data_sync->arry_data[ i ].data, sizeof( s_arry_data_sync[ i ] ) );
        }

                // move time ahead in game
        // If game, move characters, etc.
        s_frm_strm_cur  ++;

        return ( TRUE );
}

// callback of data acquired to match synchronization
static void DWAPI OnNetEngineGetCurrentSyncData( u_int32 ID_clnt, u_int32 len_data, const void* data )
{
        u_int32 frm;

        len_data;

        // Not significant, but acquire current frame (if game, exchange data to match info with people who joined in middle)
        memcpy( &frm, data, sizeof( frm ) );
        OSReport( "recv current frame ID %u frame %u\n", ID_clnt, frm );
}

// NetEngine warning callback
static void DWAPI OnNetEngineWarning( _ne_warning_type type_warn, const char* msg )
{
        // output warning content
        OSReport( "ne warning:%s %s\n", ne_WarningToString( type_warn ), msg );
}

// NetEngine debug output callback
static void DWAPI OnNetEngineDebugMessage( const char* msg )
{
        // display with no changes
        OSReport( "ne :%s\n", msg );
}

// display current status
static void ViewCurrentStatus( void )
{
        u_int32 ID_tmp;
        s_int32 cnt;

        // display program count
        DEMOPrintf( 50, 20, 0, "frame:%u\n", s_frm_cur );

        // display DirectEngine status
        switch ( de_GetStatus() )
        {
        case DE_STAT_NOTINIT:           DEMOPrintf( 50, 30, 0, "de stat:DE_STAT_NOTINIT\n" );           break;
        case DE_STAT_INIT:                      DEMOPrintf( 50, 30, 0, "de stat:DE_STAT_INIT\n" );                      break;
        case DE_STAT_CONNECTING:        DEMOPrintf( 50, 30, 0, "de stat:DE_STAT_CONNECTING\n" );        break;
        case DE_STAT_DISCONNECTING:     DEMOPrintf( 50, 30, 0, "de stat:DE_STAT_DISCONNECTING\n" );     break;
        case DE_STAT_CONNECTED:         DEMOPrintf( 50, 30, 0, "de stat:DE_STAT_CONNECTED\n" );         break;
        case DE_STAT_JOINING:           DEMOPrintf( 50, 30, 0, "de stat:DE_STAT_JOINING\n" );           break;
        case DE_STAT_LEAVING:           DEMOPrintf( 50, 30, 0, "de stat:DE_STAT_LEAVING\n" );           break;
        case DE_STAT_JOINED:            DEMOPrintf( 50, 30, 0, "de stat:DE_STAT_JOINED\n" );            break;
        case DE_STAT_DATA_SENDING:      DEMOPrintf( 50, 30, 0, "de stat:DE_STAT_DATA_SENDING\n" );      break;
        case DE_STAT_BEGINNING:         DEMOPrintf( 50, 30, 0, "de stat:DE_STAT_BEGINNING\n" );         break;
        case DE_STAT_ENDING:            DEMOPrintf( 50, 30, 0, "de stat:DE_STAT_ENDING\n" );            break;
        case DE_STAT_GAME:                      DEMOPrintf( 50, 30, 0, "de stat:DE_STAT_GAME\n" );                      break;
        }

        // display NetEngine status
        switch ( ne_GetStatus() )
        {
        case NE_STAT_NOTINIT:   DEMOPrintf( 50, 40, 0, "ne stat:NE_STAT_NOTINIT\n" );   break;
        case NE_STAT_INIT:              DEMOPrintf( 50, 40, 0, "ne stat:NE_STAT_INIT\n" );              break;
        case NE_STAT_JOINING:   DEMOPrintf( 50, 40, 0, "ne stat:NE_STAT_JOINING\n" );   break;
        case NE_STAT_LEAVING:   DEMOPrintf( 50, 40, 0, "ne stat:NE_STAT_LEAVING\n" );   break;
        case NE_STAT_JOINED:    DEMOPrintf( 50, 40, 0, "ne stat:NE_STAT_JOINED\n" );    break;
        case NE_STAT_BEGINNING: DEMOPrintf( 50, 40, 0, "ne stat:NE_STAT_BEGINNING\n" ); break;
        case NE_STAT_STRM:              DEMOPrintf( 50, 40, 0, "ne stat:NE_STAT_STRM\n" );              break;
        case NE_STAT_ENDING:    DEMOPrintf( 50, 40, 0, "ne stat:NE_STAT_ENDING\n" );    break;
        }
        
        if ( ne_GetStatus() < NE_STAT_JOINED )  return;
        if ( ne_GetStatus() < NE_STAT_STRM )    goto ssn;

        // display counter during streaming communication
        DEMOPrintf( 50, 50, 0, "stream frame:%u\n", s_frm_strm_cur );

        cnt             = 0;
        ID_tmp  = ne_GetHigherID( NE_ID_INVALID, TRUE );
        while ( ID_tmp != NE_ID_INVALID )
        {
                // display results of synchronized data
                DEMOPrintf( 50, ( s_int16 )( 60 + ( cnt * 10 ) ), 0, "ID:%u frm %u\n", ID_tmp, s_arry_data_sync[ cnt ] );
                ID_tmp  = ne_GetHigherID( ID_tmp, TRUE );
                cnt             ++;
                ASSERT( cnt < 3 );
        }

ssn:
        // display client number
        DEMOPrintf( 50, 60 + ( CLNT_NUM_MAX * 10 ), 0, "client num:%u\n", ne_GetClientNum() );

        // display client details
        cnt             = 0;
        ID_tmp  = ne_GetHigherID( NE_ID_INVALID, FALSE );
        while ( ID_tmp != NE_ID_INVALID )
        {
                _ne_client_info inf;

                ne_GetClientInfo( ID_tmp, &inf );
                DEMOPrintf( 50, ( s_int16 )( 70 + ( CLNT_NUM_MAX * 10 ) + ( cnt * 10 ) ), 0, "ID:%u name:%s latency:%u\n", ID_tmp, inf.name, inf.time_latency );
                ID_tmp  = ne_GetHigherID( ID_tmp, FALSE );
                cnt             ++;
                ASSERT( cnt < 3 );
        }
}

// display operation method
static void PrintIntro( void )
{
        OSReport("\n\n");
        OSReport("*******************************************************\n");
        OSReport("simple: demonstrate netenigne / directengine \n");
        OSReport("*******************************************************\n");
        OSReport("\n");
        OSReport("  X button     : call de_Connect\n");
        OSReport("  Y button     : call de_WaitConnect\n");
        OSReport("  B button     : in streaming -> end, in connecting -> disconnect \n");
        OSReport("\n");
        OSReport("*******************************************************\n\n");
}
