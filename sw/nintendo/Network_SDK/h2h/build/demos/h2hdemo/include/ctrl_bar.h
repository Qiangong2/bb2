/******************************
  Macro definition
******************************/

/* about controll bar */
#define BAR_HALF_X	5.0F
#define BAR_HALF_Y	50.0F
#define BAR_HALF_Z	5.0F

#define NUMOFBARVTX	24


/******************************
  Model matrix
******************************/

f32 BAR_PosArray[] ATTRIBUTE_ALIGN( 32 ) =
{

	/* x-y plane ( up ) */
	 BAR_HALF_X,  BAR_HALF_Y,  BAR_HALF_Z,
	 BAR_HALF_X, -BAR_HALF_Y,  BAR_HALF_Z,
	-BAR_HALF_X, -BAR_HALF_Y,  BAR_HALF_Z,
	-BAR_HALF_X,  BAR_HALF_Y,  BAR_HALF_Z,

	/* x-y plane ( down ) */
	 BAR_HALF_X,  BAR_HALF_Y, -BAR_HALF_Z,
	-BAR_HALF_X,  BAR_HALF_Y, -BAR_HALF_Z,
	-BAR_HALF_X, -BAR_HALF_Y, -BAR_HALF_Z,
	 BAR_HALF_X, -BAR_HALF_Y, -BAR_HALF_Z,

	/* y-z plane ( side ) */
	 BAR_HALF_X,  BAR_HALF_Y,  BAR_HALF_Z,
	 BAR_HALF_X,  BAR_HALF_Y, -BAR_HALF_Z,
	 BAR_HALF_X, -BAR_HALF_Y, -BAR_HALF_Z,
	 BAR_HALF_X, -BAR_HALF_Y,  BAR_HALF_Z,
	
	/* y-z plane ( side ) */
	-BAR_HALF_X,  BAR_HALF_Y,  BAR_HALF_Z,
	-BAR_HALF_X, -BAR_HALF_Y,  BAR_HALF_Z,
	-BAR_HALF_X, -BAR_HALF_Y, -BAR_HALF_Z,
	-BAR_HALF_X,  BAR_HALF_Y, -BAR_HALF_Z,

	/* x-z plane ( front ) */
	 BAR_HALF_X,  BAR_HALF_Y,  BAR_HALF_Z,
	-BAR_HALF_X,  BAR_HALF_Y,  BAR_HALF_Z,
	-BAR_HALF_X,  BAR_HALF_Y, -BAR_HALF_Z,
	 BAR_HALF_X,  BAR_HALF_Y, -BAR_HALF_Z,

	/* x-z plane ( back ) */
	 BAR_HALF_X, -BAR_HALF_Y,  BAR_HALF_Z,
	 BAR_HALF_X, -BAR_HALF_Y, -BAR_HALF_Z,
	-BAR_HALF_X, -BAR_HALF_Y, -BAR_HALF_Z,
	-BAR_HALF_X, -BAR_HALF_Y,  BAR_HALF_Z

};

u8 BAR_ClrArray[] ATTRIBUTE_ALIGN( 32 ) =
{
	255, 255, 255, 255,
	255, 255, 255, 255,
	255, 255, 255, 255,
	255, 255, 255, 255,

	255, 255, 255, 255,
	255, 255, 255, 255,
	255, 255, 255, 255,
	255, 255, 255, 255,

	255, 255, 255, 255,
	255, 255, 255, 255,
	255, 255, 255, 255,
	255, 255, 255, 255,

	255, 255, 255, 255,
	255, 255, 255, 255,
	255, 255, 255, 255,
	255, 255, 255, 255,

	255, 255, 255, 255,
	255, 255, 255, 255,
	255, 255, 255, 255,
	255, 255, 255, 255,

	255, 255, 255, 255,
	255, 255, 255, 255,
	255, 255, 255, 255,
	255, 255, 255, 255
};

f32 BAR_NrmArray[] ATTRIBUTE_ALIGN( 32 ) =
{
	 0.0F,  0.0F,  1.0F,
	 0.0F,  0.0F,  1.0F,
	 0.0F,  0.0F,  1.0F,
	 0.0F,  0.0F,  1.0F,

	 0.0F,  0.0F, -1.0F,
	 0.0F,  0.0F, -1.0F,
	 0.0F,  0.0F, -1.0F,
	 0.0F,  0.0F, -1.0F,

	 1.0F,  0.0F,  0.0F,
	 1.0F,  0.0F,  0.0F,
	 1.0F,  0.0F,  0.0F,
	 1.0F,  0.0F,  0.0F,

	-1.0F,  0.0F,  0.0F,
	-1.0F,  0.0F,  0.0F,
	-1.0F,  0.0F,  0.0F,
	-1.0F,  0.0F,  0.0F,

	 0.0F,  1.0F,  0.0F,
	 0.0F,  1.0F,  0.0F,
	 0.0F,  1.0F,  0.0F,
	 0.0F,  1.0F,  0.0F,

	 0.0F, -1.0F,  0.0F,
	 0.0F, -1.0F,  0.0F,
	 0.0F, -1.0F,  0.0F,
	 0.0F, -1.0F,  0.0F
};

GXColor BAR_Wt = { 0xFF, 0xFF, 0xFF, 0xFF };
GXColor BAR_Bk = { 0x00, 0x00, 0x00, 0x00 };
