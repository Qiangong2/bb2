Description of Samples 

simple --- Simple Sample for Synchronous Communications 
	Method of Operation 
	Y Button   --- Waiting for Connection 
	X Button   --- Connection 
	B Button   --- Terminate the connection, reference, start or streaming communications stop. 

How to set up the phone number of the machine you are communicating with and its communication line
        They are specified in the program as follows:
          strcpy( con.phone_num, "23" );
          con.type_dial	= DE_DIAL_PULSE10;

Process to establish the connection
        First, either one of the players presses Y button to get into the connection waiting state. Next, the other player presses X button, and the machine will dial to the other machine that is waiting to establish the connection.

pimpom --- Simple Sample for Synchronous + Asynchronous Communications 
	Method of Operation 
	Y Button   --- Waiting for Connection 
	X Button   --- Connection 
	B Button   --- Terminate the connection, reference, start or streaming communications stop. 

How to set up the phone number of the communicating partner
        (loadrun or odrun) -a (J:Japan U:US) (Phone Number) (T:Tone P:Pulse)
        Input the command as above and run the program.

        Example: loadrun -a J 23 T


When linking using the parameter checking version of the library (The elf file of the parameter checking version will be suffixed ~D.elf.), specify as follows at the command line: 
	make MODE="check" 

When linking using the debug version of the library, specify as follows at the command line:
	make MODE="debug" 

When linking using the release version of the library, specify as follows at the command line:
	make MODE="release" 

Entire Flow Using simple 
	DirectEngine Initialization Settings 
	DirectEngine Initialization 
	Connection, Waiting for Connection 
	Start Game
	NetEngine Initialization
	Session Participation, Creation
	Start Streaming Communications 
	Streaming Communications 
	Terminate Streaming Communications 

Entire Flow Using pimpom 
	DirectEngine Initialization Settings 
	DirectEngine Initialization 
	Connection, Waiting for Connection 
	Start Game
	NetEngine Initialization 
	Session Participation, Creation 
	Start Streaming Communications 
	Streaming Communications 
	Terminate Streaming Communications
