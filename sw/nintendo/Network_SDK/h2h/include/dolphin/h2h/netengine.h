/*--------------------------------------------------------------------------
//
//      Copyright (C) 2000 DWANGO Corporation.  All Rights Reserved.
//
//      $RCSfile: netengine.h,v $
//      $Date: 2004/03/15 21:58:02 $ 
//      $Revision: 1.1.1.1 $
//      Content:        NetEngine include file
//
//-------------------------------------------------------------------------*/

#if !defined( NETENGINE_INCLUDED )
#define NETENGINE_INCLUDED

#include <dolphin/h2h/dw_types.h>

#define NE_VERSION                                              0x02020802

#define NE_CLNT_NAME_LEN_MAX                    64
#define NE_SSN_NAME_LEN_MAX                             64
#define NE_GAME_NAME_LEN_MAX                    64
#define NE_STRM_DATA_LEN_MAX                    64
#define NE_SYNC_CURDATA_LEN_MAX                 255

#define NE_SSN_CLNT_NUM_MAX                             255                     // Max clients outside of object communication

#define NE_SHARED_DATA_REQ_NUM_MAX              32

#define NE_STRM_INF_NUM_MAX                             32
#define NE_STRM_PACK_NUM_MAX                    3                       // num_pack max value
#define NE_STRM_RETRY_NUM_MAX                   3                       // num_retry max value

#define NE_SYNC_BUF_NUM_MAX                             60

#define NE_OBJ_INF_NUM_MAX                              32
#define NE_OBJ_ID_CALC_NUM_MAX                  10000

#define NE_SEND_LEN_MAX                                 200

#define NE_PRI_HIGHEST                                  0
#define NE_PRI_LOWEST                                   7
#define NE_PRI_NUM_MAX                                  8

#define NE_ID_INVALID                                   0xFFFFFFFF
#define NE_ID_ALLCLNT                                   0xFFFFFFFE
#define NE_ID_SVR                                               0xFFFFFFFD

// Max number per frame size (NE_FRM_0 has no frame)
#define NE_FRM_8_MAX                                    240
#define NE_FRM_16_MAX                                   57600
#define NE_FRM_32_MAX                                   3317760000
#define NE_FRM_SYNC_MAX                                 NE_FRM_8_MAX

// Invalid numbers per frame size (NE_FRM_0 has no frame)
#define NE_FRM_8_INVALID                                0xFF
#define NE_FRM_16_INVALID                               0xFFFF
#define NE_FRM_32_INVALID                               0xFFFFFFFF
#define NE_FRM_SYNC_INVALID                             NE_FRM_8_INVALID

#define NE_ADDR_CLNT_DEFAULTPORT                0x1A1B                                  // Client initial value

#define NE_ADDR_SVR_DEFAULTPORT_OATCP   0xB3B4                                  // Initial value of server using OATCP
#define NE_ADDR_SVR_DEFAULTPORT_UDP             0xC3C4                                  // Initial value of server using UDP

typedef enum __ne_protocol_type
{
        NE_PROTO_UDP,
        NE_PROTO_OATCP,
        NE_PROTO_TCP,
        NE_PROTO_DIRECT
} _ne_protocol_type;

typedef enum __ne_stream_type
{
        NE_STRM_SYNC,
        NE_STRM_ASYNC,
        NE_STRM_OBJ             = NE_STRM_ASYNC
} _ne_stream_type;

typedef enum __ne_frame_type
{
        NE_FRM_0,               // Not using frame  In this case _ne_stream_data_info::frm_interval is 0
        NE_FRM_8,
        NE_FRM_16,
        NE_FRM_32
} _ne_frame_type;

typedef enum __ne_status_type
{
        NE_STAT_NOTINIT,                                // Not initialized
        NE_STAT_INIT,                                   // Initialization
        NE_STAT_JOINING,                                // Joining
        NE_STAT_LEAVING,                                // Leaving
        NE_STAT_JOINED,                                 // Joined
        NE_STAT_BEGINNING,                              // Starting streaming communication
        NE_STAT_STRM,                                   // Streaming communication going
        NE_STAT_ENDING                                  // Streaming communication ending
} _ne_status_type;

typedef enum __ne_event_type
{
        NE_EVENT_SYSTEM_ERR_INTERNAL,
        NE_EVENT_SSN_JOINED,
        NE_EVENT_SSN_LEFT,
        NE_EVENT_SSN_LOCKED,
        NE_EVENT_SSN_UNLOCKED,
        NE_EVENT_SSN_CLNT_ADDED,
        NE_EVENT_SSN_CLNT_REMOVED,
        NE_EVENT_SSN_ADMIN_CHANGED,
        NE_EVENT_SHARED_DATA_SET,
        NE_EVENT_SHARED_DATA_GOT,
        NE_EVENT_SHARED_DATA_LOCKED,
        NE_EVENT_SHARED_DATA_UNLOCKED,
        NE_EVENT_STRM_BEGAN,
        NE_EVENT_STRM_ENDED,
        NE_EVENT_STRM_CLNT_ADDED,
        NE_EVENT_STRM_CLNT_REMOVED,
        NE_EVENT_SYNC_SUBSTITUTED_CLNT,
        NE_EVENT_SYNC_CURDATA_SET,
        NE_EVENT_SYNC_CURDATA_COMPLETED,
        NE_EVENT_RECORD_BUFFULL,
        NE_EVENT_OBJ_CREATED_LOCAL,
        NE_EVENT_OBJ_CREATED_GLOBAL,
        NE_EVENT_OBJ_ADDED,
        NE_EVENT_OBJ_REMOVED,
        NE_EVENT_OBJ_LOCKED,
        NE_EVENT_OBJ_UNLOCKED,
        NE_EVENT_OBJ_TARGET_CAPTURED,
        NE_EVENT_GRTD_REACHED,
        NE_EVENT_GRTD_OVERFLOW_BUF
} _ne_event_type;

typedef enum __ne_warning_type
{
        NE_WARN_INVALID_VERSION,
        NE_WARN_INVALID_SETTING_SSN,
        NE_WARN_INVALID_SETTING_STRM,
        NE_WARN_FUNC_INVALID_MODE,
        NE_WARN_FUNC_NOTINIT,
        NE_WARN_FUNC_INIT,
        NE_WARN_FUNC_ADMIN,
        NE_WARN_FUNC_NOTADMIN,
        NE_WARN_FUNC_INSSN,
        NE_WARN_FUNC_NOTINSSN,
        NE_WARN_FUNC_LOCKED,
        NE_WARN_FUNC_NOTLOCKED,
        NE_WARN_FUNC_STRM,
        NE_WARN_FUNC_NOTSTRM,
        NE_WARN_FUNC_ALREADY,
        NE_WARN_ARG_NULLPOINTER,
        NE_WARN_ARG_INVALID_LEN,
        NE_WARN_ARG_INVALID_PARAMS
} _ne_warning_type;

typedef enum __ne_join_type
{
        NE_JOIN_SUCCEED,
        NE_JOIN_FAILED_SSN_NOTFOUND,
        NE_JOIN_FAILED_SSN_FULL,
        NE_JOIN_FAILED_SSN_LOCKED,
        NE_JOIN_FAILED_GAME_NOTFOUND,
        NE_JOIN_FAILED_KICKEDOUT,
        NE_JOIN_FAILED_TIMEOUT_DNS,
        NE_JOIN_FAILED_TIMEOUT,
        NE_JOIN_FAILED_OTHER
} _ne_join_type;

typedef enum __ne_leave_type
{
        NE_LEAVE_SUCCEED,
        NE_LEAVE_LOST,
        NE_LEAVE_KICKEDOUT,
        NE_LEAVE_OTHER
} _ne_leave_type;

typedef enum __ne_add_type
{
        NE_ADD_NORMAL,
        NE_ADD_FAILED_TIMEOUT,
        NE_ADD_FAILED_FULL,
        NE_ADD_FAILED_OTHER
} _ne_add_type;

typedef enum __ne_remove_type
{
        NE_REMOVE_NORMAL,
        NE_REMOVE_LOST
} _ne_remove_type;

typedef enum __ne_begin_type
{
        NE_BEGIN_SUCCEED,
        NE_BEGIN_FAILED_TIMEOUT,
        NE_BEGIN_FAILED_TIMEOUT_SETCURDATA,
        NE_BEGIN_FAILED_OTHER
} _ne_begin_type;

typedef enum __ne_end_type
{
        NE_END_SUCCEED,
        NE_END_FAILED_TIMEOUT,
        NE_END_FAILED_OTHER
} _ne_end_type;

typedef enum __ne_create_type
{
        NE_CREATE_SUCCEED,
        NE_CREATE_FAILED_FULL,
        NE_CREATE_FAILED_ALREADY,
        NE_CREATE_FAILED_LEFT,
        NE_CREATE_FAILED_TIMEOUT,
        NE_CREATE_FAILED_OTHER
} _ne_create_type;

typedef enum __ne_lock_type
{
        NE_LOCK_SUCCEED,
        NE_LOCK_FAILED_ALREADY,
        NE_LOCK_FAILED_NOTFOUND,
        NE_LOCK_FAILED_LEFT,
        NE_LOCK_FAILED_TIMEOUT,
        NE_LOCK_FAILED_OTHER
} _ne_lock_type;

typedef enum __ne_unlock_type
{
        NE_UNLOCK_SUCCEED,
        NE_UNLOCK_FAILED_LEFT,
        NE_UNLOCK_FAILED_TIMEOUT,
        NE_UNLOCK_FAILED_OTHER
} _ne_unlock_type;

typedef enum __ne_set_type
{
        NE_SET_SUCCEED,
        NE_SET_FAILED_LEFT,
        NE_SET_FAILED_OTHER
} _ne_set_type;

typedef enum __ne_get_type
{
        NE_GET_SUCCEED,
        NE_GET_FAILED_LEFT,
        NE_GET_FAILED_OTHER
} _ne_get_type;

typedef enum __ne_capture_type
{
        NE_CAPTURE_SUCCEED,
        NE_CAPTURE_FAILED_NOTFOUND,
        NE_CAPTURE_FAILED_TIMEOUT,
        NE_CAPTURE_FAILED_OTHER
} _ne_capture_type;

typedef union __ne_event
{
        struct
        {
                _ne_join_type                   type_join;
                char                                    name_ssn[ NE_SSN_NAME_LEN_MAX ];
                u_int32                                 ID_own;
                u_int32                                 ID_admin;
                BOOL                                    created : 1;
        } joined_ssn;

        struct
        {
                _ne_leave_type                  type_leave;
                char                                    name_ssn[ NE_SSN_NAME_LEN_MAX ];
                BOOL                                    deleted : 1;
        } left_ssn;

        struct
        {
                _ne_lock_type                   type_locked;
        } locked_ssn;

        struct
        {
                _ne_unlock_type                 type_unlock;
        } unlocked_ssn;

        struct
        {
                _ne_add_type                    type_add;
                u_int32                                 ID_clnt;
        } added_clnt;

        struct
        {
                _ne_remove_type                 type_remove;
                u_int32                                 ID_clnt;
        } removed_clnt;

        struct
        {
                _ne_begin_type                  type_begin;
        } began_strm;
        
        struct
        {
                _ne_end_type                    type_end;
        } ended_strm;

        struct
        {
                u_int32                                 ID_admin_new;
                u_int32                                 ID_admin_old;
        } change_admin;

        struct
        {
                _ne_lock_type                   type_lock;
        } locked_data_shared;

        struct
        {
                _ne_unlock_type                 type_unlock;
        } unlocked_data_shared;

        struct
        {
                _ne_set_type                    type_set;
                u_int32                                 ID_req;
        } set_data_shared;

        struct
        {
                _ne_get_type                    type_get;
                u_int32                                 ID_req;
        } got_data_shared;

        struct
        {
                u_int32                                 ID_clnt_lost;
                u_int32                                 ID_clnt_substituted;
        } substituted_clnt_sync;

        struct
        {
                u_int32                                 ID_clnt;
                BOOL                                    instantly       : 1;
        } data_cur_strm_set;

        struct
        {
                _ne_create_type                 type_create;
                u_int32                                 ID_obj;
                s_int32                                 type_obj;
        } created_obj_local;

        struct
        {
                _ne_create_type                 type_create;
                u_int32                                 ID_obj_local;
                u_int32                                 ID_obj_global;
                s_int32                                 type_obj;
        } created_obj_global;

        struct
        {
                u_int32                                 ID_obj;
                s_int32                                 type_obj;
        } added_obj;

        struct
        {
                u_int32                                 ID_obj;
                s_int32                                 type_obj;
        } removed_obj;

        struct
        {
                _ne_lock_type                   type_lock;
                u_int32                                 ID_owner;
                u_int32                                 ID_obj;
                s_int32                                 type_obj;
        } locked_obj;

        struct
        {
                _ne_unlock_type                 type_unlock;
                u_int32                                 ID_owner_prev;
                u_int32                                 ID_obj;
                s_int32                                 type_obj;
        } unlocked_obj;

        struct
        {
                _ne_capture_type                type_capture;
                u_int32                                 ID_clnt;
        } captured_target_obj;

        struct
        {
                u_int32                                 ID_req;
        } reached_grtd;

        u_int32                                         param_other;
} _ne_event;

typedef struct __ne_session_info
{
        char                                            name[ NE_SSN_NAME_LEN_MAX ];
        s_int32                                         num_clnt_max;                                   // Maximum number of people in this session is less than or equal to _ne_session_initialize::num_clnt_max
} _ne_session_info;

typedef struct __ne_client_info
{
        struct
        {
                BOOL                                    strm            : 1;
                BOOL                                    substituted     : 1;
        } stat;
        
        u_int32                                         time_latency;
        s_int32                                         num_buf_sync;
        char                                            name[ NE_CLNT_NAME_LEN_MAX ];
} _ne_client_info;

typedef struct __ne_sync_option
{
        u_int32                                         len_spl;                        // Supplemental data size
        void*                                           data_spl;                       // Stored data content
} _ne_sync_option;

typedef struct __ne_async_option
{
        _ne_frame_type                          type_frm;                       // Frame number size (Always NE_FRM_8 if synchornized communication)
        s_int32                                         pri;
        BOOL                                            grtd            : 1;    // Transmission, guarantee order
        BOOL                                            via_host        : 1;    // Send data via host

        // Internal data
        u_int32                                         frm_max;
        u_int32                                         frm_invalid;
} _ne_async_option;

typedef _ne_async_option        _ne_object_option;

typedef struct __ne_get_sync_data
{
        struct
        {
                u_int32                                 ID_clnt;
                u_int32                                 len_data;
                const void*                             data;
        } *arry_data;

        s_int32                                         type_data;
        s_int32                                         num_data;
        u_int32                                         frm;
} _ne_get_sync_data;

typedef struct __ne_get_async_data
{
        u_int32                                         ID_clnt;
        u_int32                                         len_data;
        const void*                                     data;

        s_int32                                         type_data;
        u_int32                                         frm;
} _ne_get_async_data;

typedef struct __ne_get_object_data
{
        u_int32                                         ID_obj;
        u_int32                                         ID_owner;
        u_int32                                         len_data;
        const void*                                     data;
        
        s_int32                                         type_obj;
        s_int32                                         type_data;
        u_int32                                         frm;
} _ne_get_object_data;

typedef struct __ne_stream_data_info
{
        union
        {
                _ne_sync_option                 sync;
                _ne_async_option                async;
                _ne_object_option               obj;
        } option;

        _ne_stream_type                         type_strm;
        u_int32                                         frm_interval;
        u_int32                                         len_data;
        s_int32                                         num_pack;
        s_int32                                         num_retry;
        BOOL                                            var_len : 1;

        // internal data
        s_int32                                         offset_data;
} _ne_stream_data_info;

typedef struct __ne_stream_info
{
        struct
        {
                struct
                {
                        BOOL                            own                             : 1;
                        BOOL                            clnt                    : 1;
                        BOOL                            ssn                             : 1;
                        BOOL                            filter                  : 1;
                } owner;

                s_int32                                 num_obj_max;
                s_int32                                 num_own_rough;
                u_int32                                 time_timeout;
        } obj;

        s_int32                                         num_inf_strm;
        _ne_stream_data_info*           arry_inf_strm;

        // Internal use
        u_int32                                         len_sync_total_internal;
        u_int32                                         len_async_total_internal;
} _ne_stream_info;

typedef struct __ne_object_info
{
        s_int32                                         num_inf_strm;
        _ne_stream_info*                        arry_inf_strm;
} _ne_object_info;

typedef struct __ne_session_initialize
{
        struct
        {
                BOOL                                    svr                             : 1;
                BOOL                                    p2p                             : 1;
                BOOL                                    obs                             : 1;
                BOOL                                    join_strm_auto  : 1;
                BOOL                                    lock_auto               : 1;
        } mode;

        s_int32                                         num_clnt_max;
        s_int32                                         num_ssn_max;
        u_int32                                         len_share_memory;
        char                                            name_game[ NE_GAME_NAME_LEN_MAX ];
} _ne_session_initialize;

typedef struct __ne_stream_initialize
{
        struct
        {
                BOOL                                    sync            : 1;
                BOOL                                    async           : 1;
                BOOL                                    obj                     : 1;

                // Internal use
                BOOL                                    strm            : 1;
        } mode;

        struct
        {
                struct
                {
                        BOOL                            record          : 1;
                        BOOL                            spl                     : 1;
                } mode;

                u_int32                                 time_record;
                s_int32                                 num_buf_sync_max;
        } sync;

        union
        {
                _ne_stream_info                 sync_async;
                _ne_object_info                 obj;
        } inf;

        // Internal use
        s_int32                                         num_clnt_max_internal;
} _ne_stream_initialize;

typedef void    ( DWAPI* NE_ONEVENT )( _ne_event_type type_event, const _ne_event* p_event );
typedef void    ( DWAPI* NE_ONWARN )( _ne_warning_type type_warn, const char* msg );
typedef void    ( DWAPI* NE_ONRECVCLNTMSG )( u_int32 ID_clnt, u_int32 len_data, const void* data );
typedef void    ( DWAPI* NE_ONRECVOBJMSG )( u_int32 ID_obj, s_int32 type_obj, u_int32 len_data, const void* data );
typedef BOOL    ( DWAPI* NE_ONGETSYNCDATA )( const _ne_get_sync_data* data_sync );
typedef void    ( DWAPI* NE_ONGETASYNCDATA )( const _ne_get_async_data* data_async );
typedef void    ( DWAPI* NE_ONGETOBJDATA )( const _ne_get_object_data* data_obj );
typedef void    ( DWAPI* NE_ONGETCURSYNCDATA )( u_int32 ID_clnt, u_int32 len_data, const void* data );
typedef void    ( DWAPI* NE_ONDBGMSG )( const char* msg );

typedef struct __ne_initialize
{
        u_int32                                         version;
        _ne_protocol_type                       type_proto;
        u_int32                                         time_interval;
        u_int32                                         time_timeout;
        u_int32                                         bandwidth;
        char                                            port_own[ 6 ];
        char                                            name_own[ NE_CLNT_NAME_LEN_MAX ];

        NE_ONEVENT                                      OnEventCallback;
        NE_ONWARN                                       OnWarningCallback;
        NE_ONRECVCLNTMSG                        OnRecvClientMessageCallback;
        NE_ONRECVOBJMSG                         OnRecvObjectMessageCallback;
        NE_ONGETSYNCDATA                        OnGetSyncDataCallback;
        NE_ONGETASYNCDATA                       OnGetAsyncDataCallback;
        NE_ONGETOBJDATA                         OnGetObjectDataCallback;
        NE_ONGETCURSYNCDATA                     OnGetCurrentSyncDataCallback;
        NE_ONDBGMSG                                     OnDebugMessageCallback;

        _ne_session_initialize          init_ssn;
        _ne_stream_initialize           init_strm;
} _ne_initialize;

EXPORT BOOL             DWAPI                   ne_Initialize( const _ne_initialize* p_init, void* buf_internal );
EXPORT BOOL             DWAPI                   ne_Terminate( void );
EXPORT void             DWAPI                   ne_Proceed( void );
EXPORT _ne_status_type DWAPI    ne_GetStatus( void );

EXPORT u_int32  DWAPI                   ne_CalculateBufferSize( const _ne_initialize* p_init );

EXPORT BOOL             DWAPI                   ne_CreateSession( const _ne_session_info* p_int_ssn );
EXPORT BOOL             DWAPI                   ne_DeleteSession( void );
EXPORT BOOL             DWAPI                   ne_JoinSession( const _ne_session_info* p_int_ssn, const char* name_host );
EXPORT BOOL             DWAPI                   ne_LeaveSession( void );
EXPORT BOOL             DWAPI                   ne_LockSession( void );
EXPORT BOOL             DWAPI                   ne_UnlockSession( void );

EXPORT u_int32  DWAPI                   ne_SetSharedData( u_int32 offset_data, u_int32 len_data, const void* data );
EXPORT u_int32  DWAPI                   ne_GetSharedData( u_int32 offset_data, u_int32 len_data, void* data );
EXPORT BOOL             DWAPI                   ne_LockSharedData( u_int32 offset_data, u_int32 len_data );
EXPORT BOOL             DWAPI                   ne_UnlockSharedData( void );

EXPORT u_int32  DWAPI                   ne_GetOwnID( void );
EXPORT u_int32  DWAPI                   ne_GetAdministratorID( void );
EXPORT u_int32  DWAPI                   ne_GetHigherID( u_int32 ID_lower, BOOL strming );
EXPORT BOOL             DWAPI                   ne_GetClientInfo( u_int32 ID_clnt, _ne_client_info* p_inf_clnt );
EXPORT u_int32  DWAPI                   ne_GetClientNum( void );
EXPORT BOOL             DWAPI                   ne_SendClientMessage( u_int32 ID_clnt, u_int32 len_data, const void* data, s_int32 pri, u_int32 time_timeout );
EXPORT u_int32  DWAPI                   ne_SendClientGuaranteedMessage( u_int32 ID_clnt, u_int32 len_data, const void* data );

EXPORT BOOL             DWAPI                   ne_BeginStream( BOOL record );
EXPORT BOOL             DWAPI                   ne_EndStream( void );
EXPORT u_int32  DWAPI                   ne_GetCurrentFrame( void );

EXPORT BOOL             DWAPI                   ne_SetSyncData( s_int32 type_data, u_int32 len_data, const void* data );
EXPORT BOOL             DWAPI                   ne_SetAsyncData( s_int32 type_data, u_int32 len_data, const void* data );
EXPORT BOOL             DWAPI                   ne_SetObjectData( u_int32 ID_obj, s_int32 type_data, u_int32 len_data, const void* data );
EXPORT BOOL             DWAPI                   ne_IsSetSyncDataEnable( s_int32 type_data, u_int32 len_data );
EXPORT BOOL             DWAPI                   ne_IsSetAsyncDataEnable( s_int32 type_data, u_int32 len_data );
EXPORT BOOL             DWAPI                   ne_IsSetObjectDataEnable( u_int32 ID_obj, s_int32 type_data, u_int32 len_data );
EXPORT BOOL             DWAPI                   ne_GetRecentAsyncData( u_int32 ID_clnt, s_int32 type_data, u_int32* p_len_data, void* data );
EXPORT BOOL             DWAPI                   ne_GetRecentObjectData( u_int32 ID_obj, s_int32 type_data, u_int32* p_len_data, void* data );

EXPORT BOOL             DWAPI                   ne_SetCurrentSyncData( u_int32 ID_clnt, u_int32 len_data, const void* data );

EXPORT u_int32  DWAPI                   ne_CreateObject( s_int32 type_obj, u_int32 ID_req );
EXPORT BOOL             DWAPI                   ne_DeleteObject( u_int32 ID_obj );
EXPORT BOOL             DWAPI                   ne_LockObject( u_int32 ID_obj );
EXPORT BOOL             DWAPI                   ne_UnlockObject( u_int32 ID_obj );
EXPORT u_int32  DWAPI                   ne_GetObjectOwnerID( u_int32 ID_obj );
EXPORT BOOL             DWAPI                   ne_SetObserveTargetID( u_int32 ID_clnt );
EXPORT u_int32  DWAPI                   ne_GetObserveTargetID( void );
EXPORT BOOL             DWAPI                   ne_SendObjectMessage( u_int32 ID_obj_from, u_int32 ID_obj_to, u_int32 len_data, const void* data, s_int32 pri, u_int32 time_timeout );
EXPORT u_int32  DWAPI                   ne_SendObjectGuaranteedMessage( u_int32 ID_obj_from, u_int32 ID_obj_to, u_int32 len_data, const void* data );

EXPORT u_int32  DWAPI                   ne_GetHigherObjectID( s_int32 type_obj, u_int32 ID_obj_lower );
EXPORT u_int32  DWAPI                   ne_CalculateObjectID( s_int32 type_obj, u_int32 ID_obj_seed );
EXPORT BOOL             DWAPI                   ne_IsLocalObjectID( u_int32 ID_obj );
EXPORT BOOL             DWAPI                   ne_IsServerID( u_int32 ID );
EXPORT BOOL             DWAPI                   ne_IsFilterID( u_int32 ID );

EXPORT const char* DWAPI                ne_EventToString( _ne_event_type type_event );
EXPORT const char* DWAPI                ne_WarningToString( _ne_warning_type type_warn );

#endif // !NETENGINE_INCLUDED
