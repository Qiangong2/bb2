/*--------------------------------------------------------------------------
//
//      Copyright (C) 2000 DWANGO Corporation.  All Rights Reserved.
//
//      $RCSfile: directengine.h,v $
//      $Date: 2004/03/15 21:58:02 $ 
//      $Revision: 1.1.1.1 $
//      Content:        DirectEngine include file
//
//-------------------------------------------------------------------------*/

#if !defined( DIRECTENGINE_INCLUDED )
#define DIRECTENGINE_INCLUDED

#include <dolphin/h2h/dw_types.h>

#define DE_VERSION                              0x01010201

#define DE_MODEM_INIT_LEN_MAX   128
#define DE_PHONE_LEN_MAX                32

// Mode used with DirectEngine
typedef enum __de_mode_type
{
        DE_MODE_RAW,                                            // No room  Start game automatically after connection  Disconnect automatically after game over
        DE_MODE_NORMAL                                          // Have room  Start game manually
} _de_mode_type;

// Type of event that occurs with DE_ONEVENT callback
typedef enum __de_event_type
{
        DE_EVENT_INITIALIZED,                           // Initialization end
        DE_EVENT_RING,                                          // Ring verification
        DE_EVENT_CONNECTED,                                     // Connection done
        DE_EVENT_BPS,                                           // BPS identified
        DE_EVENT_DISCONNECTED,                          // Disconnect done
        DE_EVENT_JOINED,                                        // Room join done
        DE_EVENT_LEFT,                                          // Room leave done
        DE_EVENT_CHAT_RECEIVED,                         // Receive chat
        DE_EVENT_DATA_RECEIVED,                         // Receive binary data
        DE_EVENT_SHAREDMEMORY_UPDATED,          // Shared data update complete, or updated
        DE_EVENT_BEGAN,                                         // Game start
        DE_EVENT_ENDED                                          // Game over
} _de_event_type;

// DirectEngine internal status
typedef enum __de_status_type
{
        DE_STAT_NOTINIT,                                        // Not initialized
        DE_STAT_INIT,                                           // Initialized
        DE_STAT_CONNECTING,                                     // Connecting
        DE_STAT_DISCONNECTING,                          // Disconnecting
        DE_STAT_CONNECTED,                                      // Connected
        DE_STAT_JOINING,                                        // Joining room
        DE_STAT_LEAVING,                                        // Leaving room
        DE_STAT_JOINED,                                         // Joined room
        DE_STAT_DATA_SENDING,                           // Sending chat, data, and shared memory data
        DE_STAT_BEGINNING,                                      // Game starting
        DE_STAT_ENDING,                                         // Game ending
        DE_STAT_GAME                                            // Game started
} _de_status_type;

// DE_EVENT_INITIALIZED details
typedef enum __de_initialize_type
{
        DE_INIT_SUCCEED,                                        // Success
        DE_INIT_FAILED_NOMODEM,                         // Failure  No modem
        DE_INIT_FAILED_OTHER                            // Failure  Other
} _de_initialize_type;

// DE_EVENT_CONNECTED details
typedef enum __de_connect_type
{
        DE_CONNECT_SUCCEED,                                     // Success
        DE_CONNECT_FAILED_BUSY,                         // Failure  Busy
        DE_CONNECT_FAILED_NOCARRIER,            // Failure  No connection
        DE_CONNECT_FAILED_NODIALTONE,           // Failure  Circuit doesn't connect
        DE_CONNECT_FAILED_INVALID_VERSION,      // Failure  Different protocol version from target
        DE_CONNECT_FAILED_NOANSWER,                     // Failure  No reply
        DE_CONNECT_FAILED_DELAYED,                      // Failure  Redial restriction
        DE_CONNECT_FAILED_BLACKLISTED,          // Failure  Specified number is blacklisted
        DE_CONNECT_FAILED_OTHER                         // Failure  Other
} _de_connect_type;

// DE_EVENT_DISCONNECTED details
typedef enum __de_disconnect_type
{
        DE_DISCONNECT_SUCCEED,                          // Success
        DE_DISCONNECT_OTHER                                     // Forced disconnection or other unknown disconnection
} _de_disconnect_type;

// DE_EVENT_JOINED details
typedef enum __de_join_type
{
        DE_JOIN_SUCCEED,                                        // Success
        DE_JOIN_FAILED_LOST,                            // Failure  Timeout
        DE_JOIN_FAILED_DISCONNECTED                     // Failure DISCONNECT occurred
} _de_join_type;

// DE_EVENT_LEFT details
typedef enum __de_leave_type
{
        DE_LEAVE_SUCCEED,                                       // Success
        DE_LEAVE_LOST,                                          // Leave due to timeout, etc.
        DE_LEAVE_DISCONNECTED                           // DISCONNECT occurred, leave
} _de_leave_type;

// DE_EVENT_BEGAN details
typedef enum __de_begin_type
{
        DE_BEGIN_SUCCEED,                                       // Success
        DE_BEGIN_FAILED_LOST,                           // Failure timeout
        DE_BEGIN_FAILED_DISCONNECTED            // Failure DISCONNECT occurred
} _de_begin_type;

// DE_EVENT_ENDED details
typedef enum __de_end_type
{
        DE_END_SUCCEED,                                         // Success
        DE_END_LOST,                                            // End due to timeout, etc.
        DE_END_DISCONNECTED                                     // DISCONNECT occured, end
} _de_end_type;

// Type when dialing
typedef enum __de_dial_type
{
        DE_DIAL_TONE,                                           // Tone
        DE_DIAL_PULSE10,                                        // Pulse 10
        DE_DIAL_PULSE20                                         // Pulse 20
} _de_dial_type;

// Dial volume
typedef enum __de_volume_type
{
        DE_VOLUME_NOVOLUME,                                     // No volume
        DE_VOLUME_MIN,                                          // Min volume
        DE_VOLUME_MID,                                          // Mid volume
        DE_VOLUME_MAX                                           // Max volume
} _de_volume_type;

// Specify country
typedef enum __de_country_type
{
        DE_COUNTRY_JAPAN,
        DE_COUNTRY_US
} _de_country_type;

// Data tied to event
typedef union __de_event
{
        u_int32                                 bps;                            // DE_EVENT_BPS                         bps
        _de_initialize_type             type_init;                      // DE_EVENT_INITIALIZE          Initialization details
        _de_disconnect_type             type_disconnect;        // DE_EVENT_DISCONNECTED        Disconnect details
        _de_join_type                   type_join;                      // DE_EVENT_JOINED                      Join room details
        _de_leave_type                  type_leave;                     // DE_EVENT_LEFT                        Leave room details
        _de_begin_type                  type_begin;                     // DE_EVENT_BEGAN                       Start game details
        _de_end_type                    type_end;                       // DE_EVENT_ENDED                       End game details
        char*                                   chat_recv;                      // DE_EVENT_CHAT_RECEIVED       Character string of chat received

        // DE_EVENT_CONNECTED
        struct
        {
                _de_connect_type        type_connect;           // Connect details
                u_int32                         len_share;                      // Shared memory size  Library sets shared memory requested of smaller size
        } connect;

        // DE_EVENT_DATA_RECEIVED
        struct
        {
                s_int32                         len_data;                       // Received data size
                u_int8*                         data;                           // Received data address
        } data_recv;

        // DE_EVENT_SHAREDMEMORY_UPDATED
        struct
        {
                s_int32                         offset;                         // Start position of updated shared memory
                s_int32                         len_update;                     // Updated size
                u_int8*                         data;                           // Updated data address
        } sharedmem_update;
} _de_event;

typedef void ( DWAPI* DE_ONEVENT )( _de_event_type type_event, const _de_event* p_event );      // Event occur callback
typedef void ( DWAPI* DE_ONDBGMSG )( const char* msg );                                                                         // Debug message output callback

// User connection information
typedef struct __de_user_info
{
        u_int32                         bandwidth;                              // Bandwidth desired
        u_int16                         version;                                // This application's version
        u_int8                          type_platform;                  // This application's platform ( application definition)
        u_int8                          type_device;                    // Type of device used in connection (application definition)
        u_int8                          framerate;                              // Game framerate
        u_int8                          big_endian;                             // Big-endian or not
        u_int32                         len_mem;                                // Usable memory size of hardware game using
} _de_user_info;

// Initialization structure used with de_Initialize
typedef struct __de_initialize
{
        _de_mode_type           type_mode;                              // DirectEngine Mode
        u_int32                         version;                                // Substitute this header's version DE_VERSION
        u_int32                         len_share;                              // Shared memory size requested  Library sets shared memory requested of smaller size
        u_int32                         len_buf_send;                   // Size of send buffer for guaranteed data
        s_int32                         num_comport;                    // COM Port number
        _de_user_info           inf_usr;                                // Connection info
        _de_country_type        type_country;                   // Country code

        DE_ONEVENT                      OnEventCallback;                // Event occur callback
        DE_ONDBGMSG                     OnDebugMessageCallback; // Debug output callback
} _de_initialize;

// Connection structure used with de_Connect and de_WaitConnect
typedef struct __de_connect
{
        char                            str_modem_init[ DE_MODEM_INIT_LEN_MAX ];        // Modem initialization character string
        char                            phone_num[ DE_PHONE_LEN_MAX ];                          // Telephone number (de_Connect only)
        _de_dial_type           type_dial;                                                                      // Type when dialing
        _de_volume_type         vol_speeker;                                                            // Speaker volume
        BOOL                            interphone;                                                                     // internal extension or not
} _de_connect;

// System function
EXPORT BOOL                             de_Initialize( const _de_initialize* p_init, void* buf_internal );                                      // Initialization
EXPORT BOOL                             de_Terminate( void );                                                                                                                           // End
EXPORT void                             de_Proceed( void );                                                                                                                                     // Update internal information  If in use always call
EXPORT _de_status_type  de_GetStatus( void );                                                                                                                           // Acquire internal status

EXPORT u_int32                  de_CalculateBufferSize( const _de_initialize* p_init );                                                         // Calculate required size of buffer internally

// Connect, Disconnect functions
EXPORT BOOL                             de_Connect( const _de_connect* p_connect );                                                                                     // Connection (dial)
EXPORT BOOL                             de_WaitConnect( const _de_connect* p_connect );                                                                         // Wait for connection
EXPORT BOOL                             de_Disconnect( void );                                                                                                                          // Disconnect

// Functions that can only be used after connection
EXPORT _de_user_info*   de_GetUserInfo( BOOL own );                                                                                                                     // Obtain user info
EXPORT BOOL                             de_IsHost( void );                                                                                                                                      // If you are host or not

// Join room, Leave room functions
EXPORT BOOL                             de_JoinRoom( void* buf_shared );                                                                                                        // Join room  Specify buffer for shared memory
EXPORT BOOL                             de_LeaveRoom( void );                                                                                                                           // Leave

// Functions that can only be used after joining room
EXPORT BOOL                             de_UpdateShareMemory( u_int32 offset_data, u_int32 len_update, const void* data );      // Update shared memory
EXPORT BOOL                             de_SendChat( u_int32 len_str, const char* str, BOOL grtd );                                                     // Send chat message   grtd is sent with guarantee or not
EXPORT BOOL                             de_SendData( u_int32 len_data, const void* data, BOOL grtd );                                           // Send binary data   grtd is sent with guarantee or not

// Game start, Game end
EXPORT BOOL                             de_BeginGameMode( void );                                                                                                                       // Start game
EXPORT BOOL                             de_EndGameMode( void );                                                                                                                         // End game

#endif // !DIRECTENGINE_INCLUDED
