/*-----------------------------------------------------------------------------
//
//  Copyright (C) 2000-2001 DWANGO Corporation.  All Rights Reserved.
//
//  $RCSfile: dw_types.h,v $
//  $Date: 2004/03/15 21:58:02 $ 
//  $Revision: 1.1.1.1 $
//  Content:    type define for DWANGO
//
//---------------------------------------------------------------------------*/

#if !defined( DW_TYPES_INCLUDED )
#define DW_TYPES_INCLUDED

  #define GAMECUBE

#ifndef DWAPI
	#define DWAPI 
#endif // !DWAPI

#if defined( __cplusplus )
	#define EXPORT							extern "C"
#else // __cplusplus 
	#define EXPORT							
#endif // !__cplusplus 

#if !defined( u_int8 )
	#define u_int8	u8
#endif // !u_int8

#if !defined( u_int16 )
	#define u_int16	u16
#endif // !u_int16

#if !defined( u_int32 )
	#define u_int32	u32
#endif // !u_int32

#if !defined( s_int8 )
	#define s_int8	s8
#endif // !s_int8

#if !defined( s_int16 )
	#define s_int16	s16
#endif // !s_int16

#if !defined( s_int32 )
	#define s_int32	s32
#endif // !s_int32

#if !defined( BOOL )
	#define BOOL	s32
#endif // !BOOL


/*
old address
typedef union __dw_IP
{
	u_int32	u_32;
	u_int8	u_8[ 4 ];
} _dw_IP;

typedef struct __dw_address
{
	_dw_IP	IP;
	u_int16	port;
} _dw_address;
*/

#define DW_IP_NUM_MAX 6

enum
{
	DW_IP_V4,
	DW_IP_V6
};

typedef union __dw_IP_v4
{
	u_int32		u_32;
	u_int8		u_8[ 4 ];
} _dw_IP_v4;

typedef union __dw_IP_v6
{
	u_int32		u_32[ 4 ];
	u_int8		u_8[ 32 ];
} _dw_IP_v6;

typedef union __dw_IP
{
	_dw_IP_v4	v4;
	_dw_IP_v6	v6;
} _dw_IP;

typedef struct __dw_address
{
	u_int16		type; // enter DW_IP_V4, DW_IP_V6
	u_int16		port;
	_dw_IP		IP;
} _dw_address;

typedef struct __dw_host_entry
{
	_dw_address	arry_addr[ DW_IP_NUM_MAX ]; // Is a variable better?
} _dw_host_entry;

#endif // !DW_TYPES_INCLUDED

