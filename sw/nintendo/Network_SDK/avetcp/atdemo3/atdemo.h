/*
 * AVE-TCP for Nintendo GameCube Sample Program
 *
 */

/*************************************************/
/* Set test environment */

/* Test contents */
#define UDP_DISCARD_TEST 1 /* Send to UDP DISCARD port */
#define PING_TEST 2 /* Send PING */
#define TCP_DISCARD_TEST 3 /* Send/receive TCP DISCARD port */

#define TEST_TYPE UDP_DISCARD_TEST /* <== Select this */

/*************************************************/

/* Loopback */
#define LOOPBACK_IPADDR 0x7f000001 /* 127.0.0.1 */
#define LOOPBACK_NETMASK 0xff000000 /* 255.0.0.0 */
#define localhost LOOPBACK_IPADDR

/* Test server host name */
#define TEST_SERVER_NAME "localhost"

#define DEV_LOOPBACK_STR "LOOPBACK"

extern AT_IF_INFO gIfinfo;
extern u16 gButton;
extern s16 x, y;
extern u32 gFrame;

#define RECV_BUF_SIZE 32767
extern AT_UBYTE recv_buf[];

#define SEND_BUF_SIZE 32767
extern AT_UBYTE send_buf[];

extern s16 ping_test(void);
extern s16 udp_discard_test(void);
extern s16 tcp_discard_test(void);
