/*
 * AVE-TCP for Nintendo GameCube Sample Program
 *
 */

#include <stdlib.h>
#include <string.h>
#include <demo.h>

#include "avetcp.h"
#include "atdemo.h"
/* 
 * UDP DISCARD TEST
 */

#define UDP_DISCARD_PORT 9
static u32 udp_discard_sent;
static u32 udp_discard_received;
static AT_SINT16 udp_receiver(AT_SINT16 func, AT_UDP_PRM *param)
{
	if (func == 0) {
		if (param->recvp.len > RECV_BUF_SIZE) {
			return 1;
		}
		param->recvp.recvbuf = recv_buf;
	} else {
		udp_discard_received++;
	}
	return 0;
}

s16 udp_discard_test(void)
{
	static s32 wait = 0;
	static s32 phase = 0;
	static AT_SINT16 nh;
	static AT_IP_ADDR dst_addr;
	AT_IP_ADDR my_addr;
	AT_SINT16 ret;
	AT_UINT32 data1;
	AT_UINT32 data2;
	AT_UINT32 data3;
	AT_SEND_BUFFS buff[3];
	s16 flag;
	enum {
		phase_udp_open = 0,
		phase_udp_send,
		phase_udp_close
	};

	flag = 0;
	DEMOPrintf(x, y += 16, 0, "UDP DISCARD ==> %s", TEST_SERVER_NAME);
	DEMOPrintf(x, y += 16, 0, "SENT     %d packets", udp_discard_sent);
	DEMOPrintf(x, y += 16, 0, "RECEIVED %d packets", udp_discard_received);

	switch (phase) {
	case phase_udp_open:
		my_addr.type = gIfinfo.type;
		my_addr.v4addr = gIfinfo.v4address;
		dst_addr.type = AT_IPv4;
		dst_addr.v4addr = localhost;
		/* Specify discard port for both send and receive sides */
		ret = udp_open(&my_addr, AT_HTONS(UDP_DISCARD_PORT), &dst_addr, AT_HTONS(UDP_DISCARD_PORT), AT_NULL, udp_receiver);
		if (ret < 0) {
			flag = 1;
			break;
		}
		nh = ret;
		phase = phase_udp_send;
		break;

	case phase_udp_send:
		if (gButton &PAD_BUTTON_A) {
			phase = phase_udp_close;
			break;
		}
		if (wait == 0) {
			udp_discard_sent++;
			data1 = 0xAAAAAAAA;
			data2 = 0xBBBBBBBB;
			data3 = 0xCCCCCCCC;
			buff[0].buff = (AT_UBYTE*)&data1;
			buff[0].len = sizeof(data1);
			buff[1].buff = (AT_UBYTE*)&data2;
			buff[1].len = sizeof(data2);
			buff[2].buff = (AT_UBYTE*)&data3;
			buff[2].len = sizeof(data3);
			ret = udp_send(nh, AT_NULL, AT_NULL, 0, AT_NULL, 3, buff);
			if (ret < 0) {
				udp_close(nh);
				flag = 1;
				break;
			}
			wait = 60;
		} else {
			wait--;
		}
		break;

	case phase_udp_close:
		ret = udp_close(nh);
		udp_discard_sent = 0;
		udp_discard_received = 0;
		flag = 1;
		break;
	}

	if (flag == 1) {
		phase = 0;
	}
	return (flag);
}
