/*
 * AVE-TCP for Nintendo GameCube Sample Program
 *
 */

#include <stdlib.h>
#include <string.h>
#include <demo.h>

#include "avetcp.h"
#include "atdemo.h"

#define DISCARD_PORT 9

#define MESNUM 10
static OSMessageQueue send_asrEventQueue;
static OSMessage send_asrEventMessage[MESNUM];
static AT_VOID send_asr(AT_SINT16 nh, AT_SINT16 event)
{

	OSReport("asr nh %d event %d\n", nh, event);
	
	OSSendMessage(&send_asrEventQueue, (OSMessage*)event, OS_MESSAGE_NOBLOCK);
	
}
static OSMessageQueue server_asrEventQueue;
static OSMessage server_asrEventMessage[MESNUM];
static AT_VOID server_asr(AT_SINT16 nh, AT_SINT16 event)
{

	OSReport("asr nh %d event %d\n", nh, event);
	
	OSSendMessage(&server_asrEventQueue, (OSMessage*)event, OS_MESSAGE_NOBLOCK);
	
}

/*
 * TCP DISCARD SEND
 *
 * Sends from the discard port of the host specified by TEST_SERVER_NAME.
 */
static AT_SINT16 td_nh;
static AT_UINT32 td_sent;
static int td_abort = 0;
static void* tcp_discard_send_thread(void* arg)
{
	AT_SINT16 ret;
	AT_SINT16 nh;
	AT_IP_ADDR *addr;
	AT_SEND_BUFFS buff;
	AT_SINT16 event;
	OSMessage msg;

	addr = (AT_IP_ADDR*)arg;
	
	ret = tcp_create();
	if (ret < 0) {
		goto tcp_discard_send_exit;
	}
	nh = ret;
	td_nh = ret;
	OSInitMessageQueue(&send_asrEventQueue, send_asrEventMessage, sizeof(send_asrEventMessage) / sizeof(send_asrEventMessage[0]));
	set_asr(nh, send_asr);
	ret = tcp_connect(nh, addr, AT_HTONS(DISCARD_PORT), AT_NULL);
	if (ret < 0) {
		goto tcp_discard_send_delete;
	}
	OSReceiveMessage(&send_asrEventQueue, &msg, OS_MESSAGE_BLOCK);
	event = (AT_SINT16)msg;
	if (event != AT_ASR_ESTABLISHED) {
		goto tcp_discard_send_abort;
	}
	
	for (;;) {
		if (td_abort) {
			goto tcp_discard_send_abort;
		}
		buff.buff = send_buf;
		buff.len = SEND_BUF_SIZE;
		ret = tcp_send(nh, AT_NULL, 1, &buff);
		if (ret < 0) {
			OSReport("tcp_send(%d) ret %d\n", nh, ret);
			break;
		}
		td_sent += SEND_BUF_SIZE;
	}

tcp_discard_send_abort:
	ret = tcp_abort(nh);
	OSReport("tcp_abort(%d) ret %d\n", nh, ret);
tcp_discard_send_delete:
	ret = tcp_delete(nh);
	OSReport("tcp_delete(%d) ret %d\n", nh, ret);
tcp_discard_send_exit:
	OSReport("tcp_discard_recv_thread end\n");
	td_abort = 0;

	return (NULL);
}

/*
 * TCP DISCARD SERVER
 *
 * Open DISCARD port and discard receive data after reading it.
 */
static AT_SINT16 ts_listen_nh = -1;
static AT_SINT16 ts_accept_nh = -1;
static AT_UINT32 ts_received;
static int ts_abort = 0;
static void* tcp_discard_server_thread(void* arg)
{
#pragma unused(arg)
	AT_SINT16 ret;
	AT_IP_ADDR addr;
	AT_SINT16 event;
	OSMessage msg;

	ret = tcp_create();
	if (ret < 0) {
		OSReport("tcp_create ret %d\n", ret);
		goto tcp_discard_server_exit;
	}
	ts_listen_nh = ret;

	addr.type = gIfinfo.type;
	addr.v4addr = gIfinfo.v4address;
	ret = tcp_bind(ts_listen_nh, &addr, AT_HTONS(DISCARD_PORT));
	if (ret < 0) {
		OSReport("tcp_bind ret %d\n", ret);
		goto tcp_discard_server_listen_delete;
	}

	ret = tcp_listen(ts_listen_nh, AT_NULL, 0, 1, AT_NULL, AT_NULL);
	if (ret < 0) {
		OSReport("tcp_listen ret %d\n", ret);
		goto tcp_discard_server_listen_delete;
	}

	OSInitMessageQueue(&server_asrEventQueue, server_asrEventMessage, sizeof(server_asrEventMessage) / sizeof(server_asrEventMessage[0]));
	ret = tcp_accept(ts_listen_nh, AT_NULL, server_asr);
	if (ret < 0) {
		OSReport("tcp_accept ret %d\n", ret);
		goto tcp_discard_server_listen_delete;
	}
	ts_accept_nh = ret;
	
	OSReceiveMessage(&server_asrEventQueue, &msg, OS_MESSAGE_BLOCK);
	event = (AT_SINT16)msg;
	if (event != AT_ASR_ESTABLISHED) {
		goto tcp_discard_server_abort;
	}

	for (;;) {
		if (ts_abort) {
			goto tcp_discard_server_abort;
		}
		ret = tcp_receive(ts_accept_nh, AT_NULL, RECV_BUF_SIZE, recv_buf);
		if (ret < 0) {
			OSReport("tcp_receive(%d) ret %d\n", ts_accept_nh, ret);
			break;
		}
		ts_received += ret;
	}

tcp_discard_server_abort:
	ret = tcp_abort(ts_accept_nh);
	OSReport("tcp_abort(%d) ret %d\n", ts_accept_nh, ret);
	ret = tcp_delete(ts_accept_nh);
	OSReport("tcp_delete(%d) ret %d\n", ts_accept_nh, ret);
tcp_discard_server_listen_delete:
	ret = tcp_delete(ts_listen_nh);
	OSReport("tcp_delete(%d) ret %d\n", ts_listen_nh, ret);
tcp_discard_server_exit:
	OSReport("tcp_discard_server_thread end\n");
	ts_abort = 0;
	ts_listen_nh = -1;
	ts_accept_nh = -1;

	return (NULL);
}

/*
 * TCP DISCARD TEST
 *
 * Send and receive to or from DISCARD port via LOOPBACK. 
 */
/* For DISCARD server thread */
static OSThread ts_th;
#define TS_STACKSIZE 4096
static AT_UBYTE ts_stack[TS_STACKSIZE];
/* For DISCARD client thread */
static OSThread td_th;
#define TD_STACKSIZE 4096
static AT_UBYTE td_stack[TD_STACKSIZE];
s16 tcp_discard_test(void)
{
	static int phase = 0;
	static int wait = 0;
	static AT_IP_ADDR dst_addr;
	AT_SINT16 ret;
	s16 flag;
	enum {
		phase_create_thread = 0,
		phase_abort_wait,
		phase_end
	};
	
	flag = 0;
	switch (phase) {
	case phase_create_thread:
		/* Start DISCARD server */
		OSCreateThread(&ts_th, tcp_discard_server_thread, AT_NULL, ts_stack + TS_STACKSIZE, TS_STACKSIZE, 16, OS_THREAD_ATTR_DETACH);
		OSResumeThread(&ts_th);
		/* Specify localhost for communication destination */
		dst_addr.type = AT_IPv4;
		dst_addr.v4addr = localhost;
		/* Start DISCARD client */
		OSCreateThread(&td_th, tcp_discard_send_thread, (void*)&dst_addr, td_stack + TD_STACKSIZE, TD_STACKSIZE, 16, OS_THREAD_ATTR_DETACH);
		OSResumeThread(&td_th);
		phase = phase_abort_wait;
		break;
	case phase_abort_wait:
		DEMOPrintf(x, y += 16, 0, "receive %u bytes", ts_received);
		DEMOPrintf(x, y += 16, 0, "send %u bytes", td_sent);
		DEMOPrintf(x, y += 16, 0, "PUSH A Button ==> stop");
		
		if (gButton & PAD_BUTTON_A) {
			td_abort = 1;
			ts_abort = 1;
			OSSendMessage(&send_asrEventQueue, (OSMessage*)(-1), OS_MESSAGE_NOBLOCK);
			OSSendMessage(&server_asrEventQueue, (OSMessage*)(-1), OS_MESSAGE_NOBLOCK);
			if (ts_listen_nh >= 0) {
				ret = tcp_cancel(ts_listen_nh, 4); /* interrupt tcp_accept() */
				OSReport("tcp_cancel(%d, 4) ret %d\n", ts_listen_nh, ret);
			}
			if (ts_accept_nh >= 0) {
				ret = tcp_cancel(ts_accept_nh, 2); /* interrupt tcp_receive() */
				OSReport("tcp_cancel(%d, 2) ret %d\n", ts_accept_nh, ret);
			}
			ret = tcp_cancel(td_nh, 1); /* interrupt tcp_send() */
			OSReport("tcp_cancel(%d) ret %d\n", td_nh, ret);
			phase = phase_end;
		}
		break;
	case phase_end:
		flag = 1;
		break;
	}
	if (flag) {
		phase = 0;
		wait = 0;
		td_sent = 0;
		ts_received = 0;
	}
	return (flag);
}
