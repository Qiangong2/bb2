/*
 * AVE-TCP for Nintendo GameCube Sample Program
 *
 */
 
#include <stdlib.h>
#include <string.h>
#include <demo.h>

#include "avetcp.h"
#include "atdemo.h"

AT_IF_INFO gIfinfo;
AT_UBYTE recv_buf[RECV_BUF_SIZE];
AT_UBYTE send_buf[SEND_BUF_SIZE];
u16 gButton;
s16 x, y;
u32 gFrame;

static PADStatus Pads[PAD_MAX_CONTROLLERS];
extern const char * const AveTcpGcBuild;

static void tcpproc(void);

void main()
{
	s32 chan;
	u32 padBit;
	u32 resetBits;
	u32 connectedBits;
	GXRenderModeObj* rmp;
	GXColor blue = {0, 0, 127, 0};
	u16 curbtn = 0;
	u16 oldbtn = 0;
	u8* device_type_str;

	gButton = 0;
	x = 0;
	y = 0;
	gFrame = 0;
	
	OSInit();
	
	OSReport ( "Welcome to the GAMECUBE C Stationery\n");
	OSReport ( "%s\n", AveTcpGcBuild);
	
	DEMOInit(NULL);
	connectedBits = 0;
	GXSetCopyClear(blue, 0x00ffffff);
	GXCopyDisp(DEMOGetCurrentBuffer(), GX_TRUE);
	rmp = DEMOGetRenderModeObj();
	DEMOInitCaption(DM_FT_XLU, (s16)rmp->fbWidth / 2, (s16)rmp->efbHeight / 2);
	
	device_type_str = (u8*)DEV_LOOPBACK_STR;
	
	for (;;) {
		DEMOBeforeRender();
		x = 10;
		y = 0;
		gFrame++;
		
		PADRead(Pads);
		resetBits = 0;
		for (chan = 0; chan < PAD_MAX_CONTROLLERS; ++chan) {
			padBit = PAD_CHAN0_BIT >> chan;
			switch (Pads[chan].err) {
				case PAD_ERR_NONE:
				case PAD_ERR_TRANSFER:
					connectedBits |= padBit;
					break;
				case PAD_ERR_NO_CONTROLLER:
					resetBits |= padBit;
					break;
				case PAD_ERR_NOT_READY:
				default:
					break;
			}
		}
		if (connectedBits) {
			resetBits &= connectedBits;
		}
		if (resetBits) {
			PADReset(resetBits);
		}
		curbtn = Pads[0].button;
		gButton = (u16)((curbtn ^ oldbtn) & curbtn);
		oldbtn = curbtn;
		
		DEMOPrintf(x, y += 16, 0, "AVE-TCP for GAMECUBE DEMO %d", gFrame);
		DEMOPrintf(x, y += 16, 0, "DEVICE [%s]", device_type_str);
		
		tcpproc();
		
        DEMODoneRender();
	}
}

enum {
	phase_init = 0,
	phase_dl_init,
	phase_if_config,

	phase_test,

	phase_if_down,
	phase_dl_term,

	phase_restart_wait,

	phase_term,
	phase_end,

	phase_assert
};

static void tcpproc(void) {

	static s32 phase = phase_init;
	static AT_UBYTE* workarea;
	static AT_SINT16 if_loopback = -1;
	static s32 wait = 0;
	AT_IF_INFO loopback;
	AT_UBYTE mac;
	AT_SINT16 ret;
	
	DEMOPrintf(x, y += 16, 0, "PHASE %d", phase);

	switch (phase) {
		case phase_init:
#ifdef AT_GC_DEBUG
			AT_SetDebugFlag(AT_DPLEVEL_PPP | AT_DPLEVEL_PPPOE);
#endif /* AT_GC_DEBUG */
			ret = (AT_SINT16)AT_SetTaskPriority(16);

			/* Allocate TCP work area */
			{
				AT_INIT_PARAM para;
				AT_SINT32 size;
				
				size = AT_GetInitParam(&para); /* Get default parameter */
				
				para.tcp_port_num = 4; /* Overwrite parameter you wish to change */
				
				size = AT_SetInitParam(&para); /* Reset parameter, get necessary area size */
				if (size < 0) {
					phase = phase_assert;
					break;
				}
				
				workarea = (AT_UBYTE*)OSAlloc((u32)size); 
				if (workarea) {
					ret = AT_SetWorkArea(workarea, size); /* Give secured area to TCPLib */
					if (ret < 0) {
						phase = phase_assert;
						break;
					}
				} else {
					phase = phase_assert;
					break;
				}
			}
			ret = avetcp_init();
			OSReport("avetcp_init ret %d\n", ret);
			if (ret < 0) {
				phase = phase_assert;
			} else {
				phase = phase_dl_init;
			}
			break;
			
		case phase_dl_init:
			/* Initialize loop back interface */
			ret = loopback_init();
			if (ret != 0) {
				phase = phase_term;
				break;
			}
			if_loopback = ret;
			phase = phase_if_config;
			break;
			
		case phase_if_config:
			/* Start loop back interface */
			gIfinfo.type = AT_IPv4;
			gIfinfo.v4address = AT_HTONL(LOOPBACK_IPADDR);
			gIfinfo.v4netmask = AT_HTONL(LOOPBACK_NETMASK);
			gIfinfo.v4brc = AT_HTONL(0);
			ret = if_config(if_loopback, &gIfinfo);
			if (ret < 0) {
				phase = phase_dl_term;
				break;
			}
			/* Get loop back interface information */
			ret = if_get(if_loopback, &loopback, sizeof(loopback), &mac);
			if (ret < 0) {
				phase = phase_if_down;
				break;
			}
			OSReport("ip addr  : %08x\n", loopback.v4address);
			OSReport("netmask  : %08x\n", loopback.v4netmask);
			OSReport("brc addr : %08x\n", loopback.v4brc);
			phase = phase_test;
			break;
			
		case phase_test:
#if (TEST_TYPE == UDP_DISCARD_TEST)
			ret = udp_discard_test(); /* UDP DISCARD */
#elif (TEST_TYPE == PING_TEST)
			ret = ping_test();   /* PING */
#else (TEST_TYPE == TCP_DISCARD_TEST)
			ret = tcp_discard_test(); /* TCP DISCARD SERVER*/
#endif
			if (ret) {
				phase = phase_if_down;
			}
			break;
			
		case phase_if_down:
			ret = if_down(if_loopback, AT_IPany);
			phase = phase_dl_term;
			break;
			
		case phase_dl_term:
			loopback_term();
			phase = phase_restart_wait;
			break;
			
		case phase_restart_wait:
			DEMOPrintf(x, y += 16, 0, "PUSH A Button ==> restart\n");
			if (gButton & PAD_BUTTON_A) {
				phase = phase_dl_init;
				break;
			}
			if (gButton & PAD_BUTTON_B) {
				phase = phase_term;
				break;
			}
			break;
			
		case phase_term:
			avetcp_term();
			OSReport("avetcp_term\n");
			OSFree(workarea);
			phase = phase_end;
			break;
			
		case phase_end:
			DEMOPrintf(x, y += 16, 0, "PROGRAM END");
			DEMOPrintf(x, y += 16, 0, "PUSH A Button ==> restart\n");
			if (gButton & PAD_BUTTON_A) {
				phase = phase_init;
			}
			break;
			
		case phase_assert:
			DEMOPrintf(x, y += 16, 0, "ASSERT");
			break;
	}
}
