/*
 * AVE-TCP for Nintendo GameCube Sample Program
 *
 */

#include <stdlib.h>
#include <string.h>
#include <demo.h>

#include "avetcp.h"
#include "atdemo.h"

#define PROTO_ICMP 1
#define TYPE_ECHO 8
#define TYPE_ECHO_REPLY 0
#define ICMP_DATA_LEN 100
#define ICMP_HEAD_LEN 8
#define INTERVAL_FRAME 30
static AT_UBYTE icmp_header[ICMP_HEAD_LEN];
static AT_UBYTE icmp_data[ICMP_DATA_LEN];
static u32 ping_seq;
static u32 ping_received;
static u32 ping_sframe;
static u32 ping_rframe;
static u32 ping_rdata;

static AT_SINT16 icmp_receiver(AT_SINT16 func, AT_IP4_PRM *param)
{
	AT_UBYTE *p;
	AT_NINT32 temp;

	if (func == 0) {
		if (param->recvp.len > RECV_BUF_SIZE) {
			return 1;
		}
		param->recvp.recvbuf = recv_buf;
	} else {
		p = recv_buf;
		if (*p == TYPE_ECHO_REPLY && *(p+1) == 0) {
			ping_received++;
			ping_rframe = gFrame;
			memcpy(&temp, p + ICMP_HEAD_LEN, sizeof(temp));
			ping_rdata = AT_NTOHL(temp);
		}
	}
	return 0;
}

s16 ping_test(void)
{
	static s32 wait = 0;
	static s32 phase = 0;
	static AT_SINT16 nh;
	static AT_IP_ADDR addr;
	static u16 ident;
	AT_SINT16 ret;
	AT_SEND_BUFFS buff[3];
	AT_UBYTE *p;
	AT_UINT32 time0;
	AT_NINT16 temp;
	AT_NINT32 temp32;
	s16 flag;
	enum {
		phase_icmp_open = 0,
		phase_icmp_send,
		phase_icmp_close
	};

	flag = 0;
	DEMOPrintf(x, y += 16, 0, "PING ==> %s", TEST_SERVER_NAME);
	DEMOPrintf(x, y += 16, 0, "Current    Reply      Diff");
	DEMOPrintf(x, y += 16, 0, "%010u %010u %010u", 
			ping_rframe, ping_rdata, ping_rframe - ping_rdata);
	switch (phase) {
	case phase_icmp_open:
		ping_seq = 0;
		ping_received = 0;
		ping_sframe = 0;
		ping_rframe = 0;
		ping_rdata = 0;

		ret = ip4_open (gIfinfo.v4address, 0, PROTO_ICMP, AT_NULL, icmp_receiver);
		if (ret < 0) {
			flag = 1;
			break;
		}
		nh = ret;
		phase = phase_icmp_send;
		AT_GetCurrentMiliSec(&time0);
		ident = (u16)(time0 & 0x0000ffff);
		break;

	case phase_icmp_send:
		if (gButton & PAD_BUTTON_A) {
			phase = phase_icmp_close;
			break;
		}
		if (wait == 0) {
			p = icmp_header;
			*p++ = TYPE_ECHO; /* type */
			*p++ = 0; /* code */
			temp = 0;
			memcpy(p, &temp, 2);
			p += 2; /* checksum */
			temp = AT_HTONS(ident);
			memcpy(p, &temp, 2);
			p += 2; /* ident */
			ping_seq++;
			temp = AT_HTONS((AT_UINT16)(ping_seq & 0x0000ffff));
			memcpy(p, &temp, 2);
			p += 2; /* seq */
			buff[0].buff = icmp_header;
			buff[0].len = ICMP_HEAD_LEN;

			ping_sframe = gFrame;
			temp32 = AT_HTONL(ping_sframe);
			buff[1].buff = (AT_UBYTE*)&temp32;
			buff[1].len = sizeof(temp32);

			memset(icmp_data, 'A', ICMP_DATA_LEN);
			buff[2].buff = icmp_data;
			buff[2].len = ICMP_DATA_LEN;

			temp = ip4_chksum(3, buff);
			p = icmp_header;
			p += 2;
			memcpy(p, &temp, 2); /* check_sum */

			ret = ip4_send(nh, gIfinfo.v4address, localhost, AT_NULL, 3, buff);
			if (ret < 0) {
				ip4_close(nh);
				flag = 1;
				break;
			}
			wait = INTERVAL_FRAME;
		} else {
			wait--;
		}
		break;
	
	case phase_icmp_close:
		DEMOPrintf(x, y += 16, 0, "SENT     %d packets", ping_seq);
		DEMOPrintf(x, y += 16, 0, "RECEIVED %d packets", ping_received);
		if (gButton & PAD_BUTTON_A) {
			ret = ip4_close(nh);
			flag = 1;
			break;
		}
		break;
	}
	
	if (flag == 1) {
		phase = 0;
	}
	return (flag);
}
