/*
 * avetcp.h
 */

#ifndef _AVETCP_H_
#define _AVETCP_H_

#include "target/at_modul.h"
#include "target/at_types.h"
#include "target/at_end.h"
#include "common/at_defs.h"
#include "common/at_api.h"
#include "common/at_dlapi.h"
#include "common/at_dns.h"
#include "ppp/common/ppp_api.h"
#include "common/avedhcp.h"
#include "target/at_rtos.h"
#include "target/at_debug.h"

#endif /* _AVETCP_H_ */
