/*
 * AVE-TCP 6.0 source code.
 *
 * at_dns.h
 *
 *
 * Copyright (C) 1992-2001 ACCESS CO.,LTD.
 * All rights are reserved by ACCESS CO.,LTD.,
 * whether the whole or part of the source code
 * including any modifications is concerned.
 */

#ifdef __cplusplus /* Support for C++ */
extern "C" {
#endif /* __cplusplus */

#ifndef AT_DNS_H
#define AT_DNS_H
	
/*
 * DNS error codes.
 */
#define AT_DNS_API_TOO_OPEN		(-64) /* 0xffc0 */
#define AT_DNS_API_CLOSED		(-65) /* 0xffbf */
#define AT_DNS_API_NO_CACHE		(-66) /* 0xffbe */
#define AT_DNS_API_NO_BUFFER	(-67) /* 0xffbd */
#define AT_DNS_API_NO_SERVER	(-68) /* 0xffbc */
#define AT_DNS_API_TIMEOUT		(-69) /* 0xffbb */
#define AT_DNS_API_FORMAT_ERR	(-70) /* 0xffba */
#define AT_DNS_API_NX_DOMAIN	(-71) /* 0xffb9 */

/*
 * Each notify function pointer.
 */
typedef AT_SINT16 (*AT_DNS_ADDR_NOTIFY) (AT_SINT16 dh,
										 AT_IP_ADDR *addr, AT_SINT16 count,
										 AT_SBYTE *cname, AT_SINT16 len);
typedef AT_SINT16 (*AT_DNS_NAME_NOTIFY) (AT_SINT16 dh,
										 AT_SBYTE *name, AT_SINT16 len);

/*
 * Notify function pointer for the polling operation.
 */
#define AT_DNS_ADDR_NOTIFY_POLLING ((AT_DNS_ADDR_NOTIFY) (-1))
#define AT_DNS_NAME_NOTIFY_POLLING ((AT_DNS_NAME_NOTIFY) (-1))

/*
 * DNS APIs.
 */
extern AT_SINT16 dns_init (AT_VOID);
extern AT_SINT16 dns_term (AT_VOID);
extern AT_SINT16 dns_set_server (AT_IP_ADDR *server);
extern AT_SINT16 dns_clear_server (AT_VOID);
extern AT_SINT16 dns_open_addr (AT_SBYTE *name, AT_SINT16 len, AT_UINT32 type);
extern AT_SINT16 dns_get_addr (AT_SINT16 dh,
							   AT_IP_ADDR *addr, AT_SINT16 *count,
							   AT_SBYTE *cname, AT_SINT16 *len,
							   AT_DNS_ADDR_NOTIFY notify);
extern AT_SINT16 dns_open_name (AT_IP_ADDR *addr);
extern AT_SINT16 dns_get_name (AT_SINT16 dh, AT_SBYTE *name, AT_SINT16 *len,
							   AT_DNS_NAME_NOTIFY notify);
extern AT_SINT16 dns_close (AT_SINT16 dh);
extern AT_SINT16 dns_clear_cache (AT_VOID);

#endif /* !AT_DNS_H */

#ifdef __cplusplus
} /* End of extern "C" { */
#endif /* __cplusplus */
