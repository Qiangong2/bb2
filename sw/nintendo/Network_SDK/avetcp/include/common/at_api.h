/*
 * AVE-TCP 6.0 source code.
 *
 * at_api.h
 *
 *
 * Copyright (C) 1992-2001 ACCESS CO.,LTD.
 * All rights are reserved by ACCESS CO.,LTD.,
 * whether the whole or part of the source code
 * including any modifications is concerned.
 */

#ifdef __cplusplus /* Support for C++ */
extern "C" {
#endif /* __cplusplus */

#ifndef AT_API_H
#define AT_API_H

typedef AT_SINT16 (*AT_IER_FUNCTION) (AT_SINT16 ifn, AT_UINT32 event, AT_VOID *info);

typedef struct at_if_info
{
	AT_UINT32 type;
	union
	{
		struct
		{
			AT_NINT32 address;
			AT_NINT32 netmask;
			AT_NINT32 brc;
		} ip4;
		struct
		{
			AT_UBYTE address[16];
			AT_SINT16 prefix_len;
			AT_SINT16 type;
#define AT_IP6_ADDR_UNICAST		0
#define AT_IP6_ADDR_ANYCAST		1
			AT_IER_FUNCTION ier;
#define AT_IER_ANS_TRUE		AT_TRUE
#define AT_IER_ANS_FALSE	AT_FALSE

#define AT_IER_DAD_SUCCESS		0x0000
#define AT_IER_DAD_FAIL			0x0001
#define AT_IER_PREFIX			0x0002
#define AT_IER_HOPLIMIT			0x0003
#define AT_IER_MTU				0x0004
#define AT_IER_DEFAULT_ADD		0x0005
#define AT_IER_DEFAULT_TOUT		0x0006
#define AT_IER_DEFAULT_CHANGE	0x0007
#define AT_IER_H2R				0x0008
#define AT_IER_R2H				0x0009
#define AT_IER_REDIRECT			0x000a
#define AT_IER_RECV_NS_ANYCAST	0x000b
#define AT_IER_RECV_RS			0x000c
		} ip6;
	} ip46;
} AT_IF_INFO;

#define v4address		ip46.ip4.address
#define v4netmask		ip46.ip4.netmask
#define v4brc			ip46.ip4.brc
#define v6address		ip46.ip6.address
#define v6prefix_len	ip46.ip6.prefix_len
#define v6type			ip46.ip6.type
#define v6ier			ip46.ip6.ier

typedef struct at_ier_info_addr
{
	AT_UBYTE *addr; /* IPv6 address(16 bytes) */
	AT_SINT16 plen;
} AT_IER_INFO_ADDR;

typedef struct at_ier_info_prefix
{
	AT_UBYTE *prefix; /* IPv6 address prefix(16 bytes) */
	AT_SINT16 plen;
	AT_UINT32 plifetime;
	AT_UINT32 vlifetime;
} AT_IER_INFO_PREFIX;

typedef struct at_ier_info_hoplimit
{
	AT_UBYTE *addr; /* router IPv6 address(16 bytes) */
	AT_UBYTE hop_limit;
} AT_IER_INFO_HOPLIMIT;

typedef struct at_ier_info_mtu
{
	AT_UBYTE *addr; /* router IPv6 address(16 bytes) */
	AT_SINT32 mtu;
} AT_IER_INFO_MTU;

typedef struct at_ier_info_default_change
{
	AT_UBYTE *oldr; /* old default router IPv6 address(16 bytes) */
	AT_UBYTE *newr; /* new default router IPv6 address(16 bytes) */
} AT_IER_INFO_DEFAULT_CHANGE;

typedef struct at_ier_info_rd
{
	AT_UBYTE *dst; /* destination IPv6 address(16 bytes) */
	AT_SINT16 plen; /* destination plefix length */
	AT_UBYTE *oldr; /* old router IPv6 address(16 bytes) */ 
	AT_UBYTE *newr; /* new router IPv6 address(16 bytes) */ 
} AT_IER_INFO_RD;

typedef struct at_ier_info_ns_anycast
{
	AT_UBYTE *src; /* NS source IPv6 address(16 bytes) */
	AT_UBYTE *dst; /* NS destination IPv6 address(16 bytes) */
} AT_IER_INFO_NS_ANYCAST;

extern AT_SINT16 if_config (AT_SINT16 ifn, AT_IF_INFO *ifinf);
extern AT_SINT16 if_alias (AT_SINT16 ifn, AT_IF_INFO *ifinf);
extern AT_SINT16 if_unalias (AT_SINT16 ifn, AT_IF_INFO *ifinf);
extern AT_SINT16 if_down (AT_SINT16 ifn, AT_UINT32 type);
extern AT_SINT16 if_get (AT_SINT16 ifn, AT_IF_INFO *ifinf, AT_SINT16 n_byte,
						 AT_UBYTE *mac);

typedef AT_VOID (*AT_ASR_FUNCTION) (AT_SINT16 nh, AT_SINT16 event);
extern AT_SINT16 set_asr (AT_SINT16 nh, AT_ASR_FUNCTION asr);
#define AT_ASR_ESTABLISHED		1
#define AT_ASR_FIN				2
#define AT_ASR_FIN2				3
#define AT_ASR_DISCONNECT		4
#define AT_ASR_RST				10
#define AT_ASR_TIMEOUT			11
#define AT_ASR_HOST				12
#define AT_ASR_RST2				14
#define AT_ASR_ADDR_UNREACH		128
#define AT_ASR_PORT_UNREACH		129
#define AT_ASR_LAST				255

extern AT_SINT16 tcp_create (AT_VOID);
extern AT_SINT16 tcp_bind (AT_SINT16 nh, AT_IP_ADDR *my_addr,
						   AT_NINT16 my_port);

typedef struct at_ip4_opt
{
	AT_UBYTE ttl;
	AT_UBYTE svctype;
	AT_UBYTE df_flag;
} AT_IP4_OPT;

typedef struct at_ip6_opt
{
	AT_UBYTE traffic_class;
	AT_NINT32 flow_label;
	AT_UBYTE hop_limit;
} AT_IP6_OPT;

typedef struct at_ip_option
{
	AT_UINT32 type;
	union
	{
		AT_IP4_OPT ip4;
		AT_IP6_OPT ip6;
	} ip46;
} AT_IP_OPTION;

#define v4opt					ip46.ip4
#define v4ttl					ip46.ip4.ttl
#define v4svctype				ip46.ip4.svctype
#define v4df_flag				ip46.ip4.df_flag
#define v6opt					ip46.ip6
#define v6traffic_class			ip46.ip6.traffic_class
#define v6flow_label			ip46.ip6.flow_label
#define v6hop_limit				ip46.ip6.hop_limit

extern AT_SINT16 tcp_connect (AT_SINT16 nh, AT_IP_ADDR *dst, AT_NINT16 port,
							  AT_IP_OPTION *option);
extern AT_SINT16 tcp_listen (AT_SINT16 nh, AT_IP_ADDR *his_addr,
							 AT_NINT16 his_port, AT_SINT16 limit,
							 AT_IP4_OPT *ip4,  AT_IP6_OPT *ip6);

typedef AT_VOID (*AT_ACCEPT_NOTIFY) (AT_SINT16 result,
												AT_SINT16 listen_nh,
												AT_IP_ADDR *his_addr,
												AT_NINT16 his_port);
extern AT_SINT16 tcp_accept (AT_SINT16 nh,
							 AT_ACCEPT_NOTIFY notify,
							 AT_ASR_FUNCTION asr);
extern AT_SINT16 tcp_get_addr (AT_SINT16 nh,
							   AT_IP_ADDR *my_addr, AT_NINT16 *my_port,
							   AT_IP_ADDR *his_addr, AT_NINT16 *his_port);
extern AT_SINT16 tcp_stat (AT_SINT16 nh, AT_SINT16 *stat, AT_SINT16 *backlog,
						   AT_datasize_t *sendwin, AT_datasize_t *recvwin);
#define AT_STAT_CLOSED			0
#define AT_STAT_LISTEN			1
#define AT_STAT_SYN_SENT		2
#define AT_STAT_SYN_RECVD		3
#define AT_STAT_ESTABLISHED		4
#define AT_STAT_CLOSE_WAIT		5
#define AT_STAT_FIN_WAIT_1		6
#define AT_STAT_CLOSING			7
#define AT_STAT_LAST_ACK		8
#define AT_STAT_FIN_WAIT_2		9
#define AT_STAT_TIME_WAIT		10
#define AT_STAT_FATAL			-1
extern AT_SINT16 tcp_get_opt (AT_SINT16 nh, AT_SINT16 type, AT_UINT32 *opt);
extern AT_SINT16 tcp_set_opt (AT_SINT16 nh, AT_SINT16 type, AT_UINT32 *opt);
#define AT_TCPOT_KEEPALIVE 0x0008
#define AT_TCPOT_SNDBUF 0x1001
#define AT_TCPOT_RCVBUF 0x1002
#define AT_TCPOT_NODELACK 0x2001
#define AT_TCPOT_SACK 0x2002
#define AT_TCPOT_INIT_CWND 0x2004
#define		AT_TCPOV_INIT_CWND_MAX 0 /* Same as the send window size */
#define		AT_TCPOV_INIT_CWND_1MSS 1 /* 1MSS */
#define		AT_TCPOV_INIT_CWND_2MSS 2 /* 2MSS */
#define		AT_TCPOV_INIT_CWND_RFC2414 3 /* RFC2414 */
#define AT_TCPOT_NONAGLE 0x2008
#define AT_TCPOT_MAXSEG 0x2010
#define AT_TCPOT_NB2 0x2020
#define AT_TCPOT_RECEIVE_TIMEOUT 0x2040
#define AT_TCPOT_SEND_TIMEOUT 0x2080
#define AT_TCPOT_ACCEPT_TIMEOUT 0x2100
extern AT_SINT16 tcp_close (AT_SINT16 nh);
extern AT_SINT16 tcp_shutdown (AT_SINT16 nh);
extern AT_SINT16 tcp_abort (AT_SINT16 nh);
extern AT_SINT16 tcp_delete (AT_SINT16 nh);

typedef struct send_buffs
{
	AT_SINT16 len;
	AT_UBYTE *buff;
} AT_SEND_BUFFS;

#define AT_MAX_SEND_BUF_NUM		3

typedef AT_VOID (*AT_SEND_NOTIFY) (AT_SINT16 result, AT_SINT16 nh);
extern AT_SINT16 tcp_send (AT_SINT16 nh,
						   AT_SEND_NOTIFY notify,
						   AT_SBYTE buf_num, AT_SEND_BUFFS *buffs);

typedef AT_VOID (*AT_RECEIVE_NOTIFY) (AT_SINT16 result, AT_SINT16 nh);
extern AT_SINT16 tcp_receive (AT_SINT16 nh,
							  AT_RECEIVE_NOTIFY notify,
							  AT_SINT16 len, AT_UBYTE *buf);

typedef AT_VOID (*AT_SEND_F_NOTIFY) (AT_SINT16 result, AT_SINT16 nh, AT_UBYTE *buf);
extern AT_SINT16 tcp_send_f (AT_SINT16 nh,
							 AT_SEND_F_NOTIFY notify,
							 AT_SINT16 len, AT_UBYTE *buf);

extern AT_SINT16 tcp_cancel (AT_SINT16 nh, AT_SINT16 what);
#define AT_CANCEL_SEND			1
#define AT_CANCEL_RECEIVE		2
#define AT_CANCEL_ACCEPT		4

typedef struct at_recv_prm
{
	AT_SINT16 nh;
	AT_SINT16 len;
	AT_UBYTE *recvbuf;
} AT_RECV_PRM;

typedef AT_SINT16 (*AT_API_RECEIVER) (AT_SINT16 func, AT_RECV_PRM *param);

typedef struct at_udpprm
{
	AT_RECV_PRM recvp;
	AT_IP_ADDR his_addr;
	AT_IP_ADDR my_addr;
	AT_NINT16 his_port;
} AT_UDP_PRM;

typedef AT_SINT16 (*AT_UDP_RECEIVER) (AT_SINT16 func, AT_UDP_PRM *param);
extern AT_SINT16 udp_open (AT_IP_ADDR *src, AT_NINT16 src_port,
						   AT_IP_ADDR *dst, AT_NINT16 dst_port,
						   AT_ASR_FUNCTION asr,
						   AT_UDP_RECEIVER receiver);
extern AT_SINT16 udp_send (AT_SINT16 nh, AT_IP_ADDR *src, AT_IP_ADDR *dst,
						   AT_NINT16 port, AT_IP_OPTION *option,
						   AT_SBYTE buf_num, AT_SEND_BUFFS *sb);
#define AT_UDP_SEND_MAX_SIZE 32739
extern AT_SINT16 udp_close (AT_SINT16 nh);

#ifdef AT_MODUL_IPv4
typedef struct at_ip4_prm
{
	AT_RECV_PRM recvp;
	AT_NINT32 his_addr;
	AT_NINT32 my_addr;
} AT_IP4_PRM;

typedef struct at_route4_list
{
	AT_NINT32 dst;
	AT_NINT32 mask;
	AT_NINT32 gw;
} AT_ROUTE4_LIST;

typedef AT_SINT16 (*AT_IP4_RECEIVER) (AT_SINT16 func, AT_IP4_PRM *param);
extern AT_SINT16 ip4_open (AT_NINT32 my_addr, AT_NINT32 his_addr,
						   AT_UBYTE proto,
						   AT_ASR_FUNCTION asr,
						   AT_IP4_RECEIVER receiver);
extern AT_SINT16 ip4_send (AT_SINT16 nh, AT_NINT32 my_addr, AT_NINT32 his_addr,
						   AT_IP4_OPT *option, AT_SBYTE buf_num,
						   AT_SEND_BUFFS *buffs);
#define AT_IP4_SEND_MAX_SIZE 32747
extern AT_SINT16 ip4_close (AT_SINT16 nh);
extern AT_UINT16 ip4_chksum (AT_SBYTE bufnum, AT_SEND_BUFFS *sb);
extern AT_SINT16 route4_add (AT_NINT32 dst, AT_NINT32 mask, AT_NINT32 gw);
extern AT_SINT16 route4_del (AT_NINT32 dst, AT_NINT32 mask, AT_NINT32 gw);
extern AT_SINT16 route4_list (AT_ROUTE4_LIST *list, AT_SINT16 n_byte);
#endif /* AT_MODUL_IPv4 */

#ifdef AT_MODUL_IPv6
typedef struct at_ip6_prm
{
	AT_RECV_PRM recvp;
	AT_UBYTE his_addr[16];
	AT_UBYTE my_addr[16];
} AT_IP6_PRM;

typedef struct at_route6_list
{
	AT_UBYTE dst[16];
	AT_SINT16 p_len;
	AT_UBYTE gw[16];
} AT_ROUTE6_LIST;

typedef struct at_ndp_list
{
	AT_UBYTE addr[16];
	AT_UBYTE mac[32];
	AT_SINT16 state;
	AT_SINT16 lifetime;
	AT_SINT16 ifn;
} AT_NDP_LIST;

typedef AT_SINT16 (*AT_IP6_RECEIVER) (AT_SINT16 func, AT_IP6_PRM *param);
extern AT_SINT16 ip6_open (AT_UBYTE *my_addr, AT_UBYTE *his_addr,
						   AT_UBYTE proto,
						   AT_ASR_FUNCTION asr,
						   AT_IP6_RECEIVER receiver);
extern AT_SINT16 ip6_send (AT_SINT16 nh, AT_UBYTE *my_addr, AT_UBYTE *his_addr,
						   AT_IP6_OPT *option, AT_SBYTE buf_num,
						   AT_SEND_BUFFS *buffs);
extern AT_SINT16 ip6_close (AT_SINT16 nh);
extern AT_UINT16 ip6_chksum (AT_UBYTE *src, AT_UBYTE *dst, AT_UBYTE proto,
							 AT_SBYTE bufnum, AT_SEND_BUFFS *sb);
extern AT_SINT16 route6_add (AT_UBYTE *dst, AT_SINT16 p_len, AT_UBYTE *gw,
							 AT_SINT16 ifn);
extern AT_SINT16 route6_del (AT_UBYTE *dst, AT_SINT16 p_len, AT_UBYTE *gw);
extern AT_SINT16 route6_list (AT_ROUTE6_LIST *list, AT_SINT16 n_byte);
extern AT_SINT16 ndp_add (AT_UBYTE *addr, AT_UBYTE *mac, AT_SINT16 maclen,
						  AT_SINT16 ifn);
extern AT_SINT16 ndp_del (AT_UBYTE *addr);
extern AT_VOID ndp_flush (AT_VOID);

extern AT_SINT16 ndp_list (AT_NDP_LIST *list, AT_SINT16 n_byte);
#endif /* AT_MODUL_IPv6 */

typedef AT_VOID (*AT_TIMER_FUNCTION) (AT_VOID);
extern AT_SINT16 set_at_timer (AT_TIMER_FUNCTION func, AT_SINT32 milisec);
extern AT_SINT16 reset_at_timer (AT_TIMER_FUNCTION func);

extern AT_SINT16 lock_at_sem (AT_VOID);
extern AT_SINT16 unlock_at_sem (AT_VOID);

/*
 * Error Code Spec.
 */
#define AT_API_SUCCESS				(  0)
#define AT_API_PENDING				( -1) /* 0xffff */
#define AT_API_INVALIDARG			( -2) /* 0xfffe */
#define AT_API_DUPSOCK				( -3) /* 0xfffd */
#define AT_API_INVALIDHANDLE		( -4) /* 0xfffc */
#define AT_API_CONNRESET			( -5) /* 0xfffb */
#define AT_API_UNREACHABLE			( -6) /* 0xfffa */
#define AT_API_RECVFIN				( -7) /* 0xfff9 */
#define AT_API_TIMEOUT				( -8) /* 0xfff8 */
#define AT_API_INVALIDREQUEST		( -9) /* 0xfff7 */
#define AT_API_CANCELED				(-10) /* 0xfff6 */
#define AT_API_FATAL				(-11) /* 0xfff5 */

#define AT_API_ERR_TASK				(-256) /* 0xff00 */
#define AT_API_ERR_BUFFS			(-257) /* 0xfeff */
#define AT_API_ERR_MEMPL			(-258) /* 0xfefe */
#define AT_API_ERR_TCB				(-259) /* 0xfefd */
#define AT_API_ERR_UCB				(-260) /* 0xfefc */
#define AT_API_ERR_ICB4				(-261) /* 0xfefb */
#define AT_API_ERR_ICB6				(-262) /* 0xfefa */
#define AT_API_ERR_RT4				(-263) /* 0xfef9 */
#define AT_API_ERR_RT6				(-264) /* 0xfef8 */
#define AT_API_ERR_IP4				(-265) /* 0xfef7 */
#define AT_API_ERR_IP6				(-266) /* 0xfef6 */
#define AT_API_ERR_AL4				(-267) /* 0xfef5 */
#define AT_API_ERR_AL6				(-268) /* 0xfef4 */
#define AT_API_ERR_ARP				(-269) /* 0xfef3 */
#define AT_API_ERR_NDP				(-270) /* 0xfef2 */
#define AT_API_ERR_DLS				(-271) /* 0xfef1 */
#define AT_API_ERR_LPB				(-272) /* 0xfef0 */
#define AT_API_ERR_DIX				(-273) /* 0xfeef */
#define AT_API_ERR_PPP				(-274) /* 0xfeee */
#define AT_API_ERR_DEV				(-275) /* 0xfeed */

extern AT_SINT16 avetcp_init (AT_VOID);
extern AT_SINT16 avetcp_term (AT_VOID);

extern AT_SINT16 if_set_opt (AT_SINT16 ifn, AT_UINT32 type, AT_UINT32 value);
extern AT_SINT16 if_get_opt (AT_SINT16 ifn, AT_UINT32 type, AT_UINT32 *value);
#define AT_IF_OPT_MTU 1

#endif /* !AT_API_H */

#ifdef __cplusplus
} /* End of extern "C" { */
#endif /* __cplusplus */
