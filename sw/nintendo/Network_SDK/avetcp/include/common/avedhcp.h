/*
 *	avedhcp.h
 *
 *	Copyright (C) 1997-1998 ACCESS CO.,LTD.
 *	Copyright (C) 2000-2001 SEGAACCESS CO.,LTD.
 * 	All rights are reserved by ACCESS CO.,LTD., 
 * 	whether the whole or part of the source code 
 * 	including any modifications is concerned. 
 */

#ifdef __cplusplus	/* Support for C++ */
extern "C" {
#endif /* __cplusplus */

#ifndef AVEDHCP_H
#define AVEDHCP_H

/* Patches */
#define ATS_T_297 /* To improve the DHCP_Request() interface 
specifications, use the interface number (ifNum) with the function 
instead of (HW) address. Return ipaddress, netmask, and broadcastflavor 
and do not use IFCONFIG internally. This solves problem of T-257 and 
DHCP doing IFCONFIG by themselves. */ /* 7/28,29/99 buchi */

/* see RFC 1531	*/
typedef struct _AT_bootpkt {
	struct _header {
		AT_UBYTE op;	/* opcode/message type */
		AT_UBYTE htype;	/* hardware type */
		AT_UBYTE hlen;	/* hardware address length */
		AT_UBYTE hops;	/* for cross-gateway booting */
		AT_UINT32 xid;	/* transaction ID */
		AT_UINT16 secs;	/* elapsed time */
		AT_SINT16 flags;	/* broadcast or not */
		AT_UINT32 ciaddr;	/* client IP address */
		AT_UINT32 yiaddr;	/* client IP address filled by server */
		AT_UINT32 siaddr;	/* server IP address */
		AT_UINT32 giaddr;	/* gateway IP address */
		AT_UBYTE chaddr[16]; /* client hardware address */
		AT_UBYTE sname[64];	/* server host name */
		AT_UBYTE file[128];	/* boot file name */
	} header;
#define	OPTION_LEN	1236   /* MAXLEN - IP - UDP - DHCP header */
	AT_UBYTE option[OPTION_LEN]; /* this field name was changed for DHCP */
}AT_bootpkt;

/* input value */
#define DHCP_DYNAMIC			0		/* USE DHCP Server Dfine		*/

/* return values */						/* Return Status Code			*/
#define	DHCP_ERROR				(-1)		/* Abnormal End				*/
#define	DHCP_SUCCESS			0			/* Normal End				*/
#define	DHCP_ERR_NORESPONSE		1			/* No Response(TimeOut)		*/
#define	DHCP_ERR_OTHER			2			/* Int. Status Error		*/

/* DHCP state */						/* Internal status Code			*/
#define DHCP_STATE_INIT			0			/* INIT						*/
#define DHCP_STATE_SELECTING	1			/* SELECTING				*/
#define DHCP_STATE_REQUESTING	2			/* REQUESTING				*/
#define DHCP_STATE_BOUND		3			/* BOUND					*/
#define DHCP_STATE_RENEWING		4			/* RENEWING					*/
#define DHCP_STATE_REBINDING	5			/* REBINDING				*/
#define DHCP_STATE_RELEASING	6			/* RELEASING				*/
#define DHCP_STATE_TIMEOUT		(-1)		/* REQUEST TIMEOUT or		*/
											/* LEASE END				*/
#define DHCP_STATE_ERROR		(-2)		/* PROCCESING ERROR			*/

/* error code */
// extern volatile int dhcp_errno;		/* error code buffer			*/
typedef enum {							/* errno error(datile) code		*/
	DHCP_NO_ERR = 0,						/* No Error					*/
	DHCP_PARA_ERR,							/* Parameter Error			*/
	DHCP_STATE_ERR,							/* Internal Status Error	*/
	DHCP_HW_ERR,							/* Hardware Error			*/
	DHCP_UPORT_ERR,							/* UDP Port Error			*/
	DHCP_USEND_ERR,							/* UDP Send Error			*/
	DHCP_DISC_ERR,							/* Discover Error			*/
	DHCP_REQ_ERR,							/* Request Error			*/
	DHCP_RENEW_ERR,							/* Renew Error				*/
	DHCP_REBIND_ERR,						/* Rebind Error				*/
	DHCP_NO_BROADCAST,						/* Broadcast Addr. Invalid	*/
	DHCP_NO_NETMASK,						/* No Sub Netmask			*/
	DHCP_NO_BCADDR,							/* No Broadcast Address		*/
	DHCP_NO_DOMAIN,							/* No Domain Name Address	*/
	DHCP_NO_DNS1,							/* No DNS Server1 Address	*/
	DHCP_NO_DNS2,							/* No DNS Server2 Address	*/
	DHCP_NO_GATEWAY,						/* No Gateway Address		*/
	DHCP_NO_LEASE,							/* No Lease Time			*/
	DHCP_NO_T1,								/* No T1 Time				*/
	DHCP_NO_T2,								/* No T2 Time				*/
	DHCP_NO_NTP,							/* No NTP Server Address	*/
	DHCP_NO_SMTP,							/* No SMTP Server Address	*/
	DHCP_NO_POP3,							/* No POP3 Server Address	*/
	DHCP_NO_WWW,							/* No WWW Server Address	*/
	DHCP_NO_SVRADDR,						/* No Server Address		*/
	DHCP_NO_IPADDR,							/* No IP Address			*/
	DHCP_NO_HOSTN,							/* No Host Name				*/
	DHCP_UNKNOWN_ERR						/* Other Code				*/
} DHCP_ERROR_STATE;
typedef enum {							/* Search Code					*/
	DHCP_BROADCAST = 0,						/* Broadcast Validity		*/
	DHCP_NETMASK,							/* Sub Netmask				*/
	DHCP_BCADDR,							/* Broadcast Address		*/
	DHCP_DOMAIN,							/* Domain Name				*/
	DHCP_DNS1,								/* DNS Server1 Address		*/
	DHCP_DNS2,								/* DNS Server2 Address		*/
	DHCP_GATEWAY,							/* Gateway Address			*/
	DHCP_LEASE,								/* Lease Time				*/
	DHCP_T1,								/* T1 Time					*/
	DHCP_T2,								/* T2 Time					*/
	DHCP_NTP,								/* NTP Server Address		*/
	DHCP_SMTP,								/* SMTP Server Address		*/
	DHCP_POP3,								/* POP3 Server Address		*/
	DHCP_WWW,								/* WWW Server Address		*/
	DHCP_SVRADDR,							/* DHCP Server Address		*/
	DHCP_IPADDR,							/* IP Address				*/
	DHCP_HOSTN,								/* Host Name				*/
	DHCP_CODE_END							/* Search Code END			*/
} DHCP_INF_CODE;

extern AT_SINT32  DHCP_init(AT_SINT16 ifnum);

extern AT_SINT32  DHCP_request(
    AT_SINT16 ifNum,                /* Network interface number used */
	AT_UINT32 ip_addr,				/* =DHCP_DYNAMIC */
    AT_UINT32 lease_time,           /* Lease time (s) */
    AT_bootpkt *dhcp_reply,         /* Address of region DHCP reply packet stored */
    AT_UINT32 *obtained_ip_addr,    /* IP address obtained */
    AT_UINT32 *obtained_netmask,    /* Network mask obtained */
    AT_UINT32 *obtained_bcaddr,     /* Broadcast obtained */
    AT_SINT32 timeout				/* timeout (s) */
	);
	
extern AT_SINT32  DHCP_request_nb(
    AT_SINT16 ifNum,                /* Network interface number used */
	AT_UINT32 ip_addr,				/* =DHCP_DYNAMIC */
    AT_UINT32 lease_time,           /* Lease time (s) */
    AT_bootpkt *dhcp_reply,         /* Address of region DHCP reply packet stored */
    AT_UINT32 *obtained_ip_addr,    /* IP address obtained */
    AT_UINT32 *obtained_netmask,    /* Network mask obtained */
    AT_UINT32 *obtained_bcaddr,     /* Broadcast obtained */
    AT_SINT32 timeout				/* timeout (s) */
	);

extern AT_SINT32  DHCP_get_dns(
    AT_bootpkt *dhcp_reply,         /* Address of region DHCP reply packet stored */
    AT_UBYTE   *domain,             /* Domain name */
    AT_UINT32  *pr_server,          /* Primary DNS server address */
    AT_UINT32  *se_server           /* Secondary DNS server address */
	);

extern AT_SINT32  DHCP_get_gateway(
    AT_bootpkt *dhcp_reply,         /* Address of region DHCP reply packet stored */
    AT_UINT32  *gateway             /* Primary DNS server address */
	);

extern AT_SINT32  DHCP_release(
    AT_SINT16 ifNum,                /* Network interface number released */
    AT_UINT32 ip_addr,              /* IP address allocated  */
    AT_SINT32 retrans_cnt           /* Resend count */
	);

extern AT_SINT32  DHCP_release_nb(
    AT_SINT16 ifNum,                /* Network interface number released */
    AT_UINT32 ip_addr,              /* IP address allocated */
    AT_SINT32 retrans_cnt           /* Resend count */
	);

extern 	AT_VOID DHCP_terminate(AT_VOID);

extern AT_SINT32 DHCP_get_leasetime(
    AT_UINT32 *leasetime,           /* Lease time */
    AT_UINT32 *t1,                  /* T1 time */
    AT_UINT32 *t2                   /* T2 time */
	);

extern AT_SINT32 DHCP_timer(AT_VOID);

extern AT_SINT32 DHCP_hostname(		/* Hostname Set Request			*/
	AT_UBYTE   *hostname			/* Hostname String.Buf.Addr	*/
	);

extern AT_SINT32 DHCP_get_state(AT_VOID);
									/* Internal Status Get Request	*/

extern AT_SINT32 DHCP_get_infostat(	/* Information Stat. Get Request*/
	AT_SINT16  code,				/* Search Code				*/
	AT_VOID	   *buf					/* Get Data Buffer Address	*/
	);

extern AT_SINT32 DHCP_get_leasetime2(
    AT_UINT32 *start,               /* Lease starting time (time in seconds since 1/1/2000 00:00) */
    AT_UINT32 *end,                 /* Lease ending time (time in seconds since 1/1/2000 00:00) */
    AT_UINT32 *remain               /* Remaining time before the end of lease (seconds) */
	);

extern AT_SINT32 DHCP_add_option_list(
	AT_UBYTE option
	);

extern AT_SINT32 DHCP_get_option(
	AT_UBYTE option,
	AT_VOID *buf,
	AT_UINT32 buflen
	);
#define DHCP_OPT_INTERFACE_MTU	26

#endif /* AVEDHCP_H */

#ifdef __cplusplus
} /* End of extern "C" { */
#endif /* __cplusplus */
