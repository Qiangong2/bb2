/*
 * AVE-TCP 6.0 source code.
 *
 * at_defs.h
 *
 *
 * Copyright (C) 1992-2001 ACCESS CO.,LTD.
 * All rights are reserved by ACCESS CO.,LTD.,
 * whether the whole or part of the source code
 * including any modifications is concerned.
 */

#ifdef __cplusplus /* Support for C++ */
extern "C" {
#endif /* __cplusplus */

#ifndef AT_DEFS_H
#define AT_DEFS_H

#define AT_MIN(a,b) (((a)<(b)) ? (a) : (b))
#define AT_MAX(a,b) (((a)>(b)) ? (a) : (b))

#if AT_ENDIAN == AT_LITTLE_ENDIAN 

#define AT_HTONS(s) \
(AT_NINT16)((((AT_UINT16)(s)<<8)&0xff00U)|(((AT_UINT16)(s)>>8)&0x00ffU))
#define AT_HTONL(l) \
(AT_NINT32)((((AT_UINT32)(l)>>24)&0x00ffU)|(((AT_UINT32)(l)>>8)&0xff00U) \
|(((AT_UINT32)(l)<<8)&0xff0000U)|(((AT_UINT32)(l)<<24)&0xff000000U))

#else

#define AT_HTONS(s) (s)
#define AT_HTONL(l) (l)

#endif /* AT_LITTLE_ENDIAN */

#define AT_NTOHS(s) AT_HTONS(s)
#define AT_NTOHL(l) AT_HTONL(l)

#define	AT_NULL			0

#define	AT_TRUE			1
#define	AT_FALSE		0

#endif /* !AT_DEFS_H */

#ifdef __cplusplus
} /* End of extern "C" { */
#endif /* __cplusplus */
