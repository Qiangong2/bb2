/*
 * AVE-TCP 6.0 source code.
 *
 * at_dlapi.h
 *
 *
 * Copyright (C) 1992-2001 ACCESS CO.,LTD.
 * All rights are reserved by ACCESS CO.,LTD.,
 * whether the whole or part of the source code
 * including any modifications is concerned.
 */

#ifdef __cplusplus /* Support for C++ */
extern "C" {
#endif /* __cplusplus */

#ifndef AT_DLAPI_H
#define AT_DLAPI_H

/*
 * Data Linke Layer Interface
 */
typedef struct AT_DlSwitch
{
#ifdef AT_MODUL_IPv6
	AT_SINT16 (*get_EUI64) (AT_SINT16 ifn, AT_UBYTE *eui64);
#endif /* AT_MODUL_IPv6 */
	AT_SINT16 (*open) (AT_SINT16 ifn, AT_NINT16 type,
						AT_SINT16 addrlen, AT_UBYTE *myadd);

#define AT_TYPE_IP4 AT_HTONS(0x0800)
#define AT_TYPE_ARP AT_HTONS(0x0806)
#define AT_TYPE_IP6 AT_HTONS(0x86dd)
	AT_SINT16 (*close) (AT_SINT16 dh);
	AT_SINT16 (*get_MTU) (AT_SINT16 dh);
	AT_SINT16 (*get_MacAddressLen) (AT_SINT16 dh);
	AT_SINT16 (*get_MacAddress) (AT_SINT16 dh, AT_UBYTE *mac);
	AT_SINT16 (*ctl_UniCast) (AT_SINT16 dh, AT_SINT16 cmd, AT_UBYTE *addr);
#define AT_SET_UNICAST 1
#define AT_DEL_UNICAST 2
	AT_SINT16 (*ctl_MultiCast) (AT_SINT16 dh, AT_SINT16 cmd, AT_UBYTE *addr);
#define AT_SET_MULTICAST 1
#define AT_DEL_MULTICAST 2
	AT_SINT16 (*free) (AT_SINT16 dh, AT_UBYTE *packet);
	AT_SINT16 (*send) (AT_SINT16 dh, AT_UBYTE *addr, AT_VOID *packet);
	AT_SINT16 (*rawsend) (AT_SINT16 dh, AT_SINT16 mode,
						AT_UBYTE *addr, AT_VOID *packet);
#define AT_UNICAST 0
#define AT_BROADCAST 1
#define AT_MULTICAST 2
} AT_DLSWITCH;

#ifdef AT_MODUL_ARP
typedef struct at_arp_list
{
	AT_NINT32	addr;
	AT_UBYTE	mac[6];
} AT_ARP_LIST;

extern AT_SINT16 arp_init (AT_VOID);
extern AT_SINT16 arp_term (AT_VOID);
extern AT_SINT16 arp_open (AT_SINT16 ifn, AT_SINT16 dh,
						   AT_NINT16 type, AT_NINT16 protocol,
						   AT_SINT16 hard, AT_UBYTE *myhaddr,
						   AT_SINT16 soft, AT_UBYTE *mysaddr);
extern AT_SINT16 arp_resolve (AT_SINT16 ah, AT_UBYTE *address, void *packet);
extern AT_SINT16 arp_add_addr (AT_SINT16 ah, AT_UBYTE *address);
extern AT_SINT16 arp_del_addr (AT_SINT16 ah, AT_UBYTE *address);
extern AT_SINT16 arp_close (AT_SINT16 ah);
extern AT_SINT16 arp_add (AT_NINT32 addr, AT_UBYTE *mac);
extern AT_SINT16 arp_del (AT_NINT32 addr);
extern AT_SINT16 arp_list (AT_ARP_LIST *list, AT_SINT16 n_byte);

typedef AT_VOID (*AT_ARP_PROBE_NOTIFY) (AT_SINT16 result, AT_UBYTE* mac);
extern AT_SINT16 arp_probe(AT_SINT16 ifn, AT_NINT32 address,
						   AT_SINT16 timeout, AT_ARP_PROBE_NOTIFY notify);
extern AT_SINT16 arp_probe_cancel (AT_SINT16 ifn);

#endif /* AT_MODUL_ARP */
#ifdef AT_MODUL_DIX
extern AT_SINT16 dix_init (AT_VOID);
extern AT_SINT16 dix_term (AT_VOID);
#endif /* AT_MODUL_DIX */
#ifdef AT_MODUL_LOOPBACK
extern AT_SINT16 loopback_init (AT_VOID);
extern AT_SINT16 loopback_term (AT_VOID);
#endif /* AT_MODUL_LOOPBACK */

extern AT_SINT16 dl_add (AT_DLSWITCH *dl, AT_SINT16 type);
#define AT_DL_LOOPBACK		0x0001
#define AT_DL_PPP			0x0002
#define AT_DL_HAVEMACADDR	0x0004
#define AT_DL_USEARP		0x0008
#define AT_DL_USEDAD		0x0010
#define AT_DL_USEND			0x0020
#define AT_DL_USERD			0x0040
#define AT_DL_USENDP		(AT_DL_USEDAD|AT_DL_USEND|AT_DL_USERD)

#define AT_DL_TYPE_LOOPBACK	(AT_DL_LOOPBACK)
#define AT_DL_TYPE_DIXETHER	(AT_DL_HAVEMACADDR|AT_DL_USEARP|AT_DL_USENDP)
#define AT_DL_TYPE_PPP		(AT_DL_PPP)

extern AT_SINT16 dl_del (AT_SINT16 ifn);


extern AT_SINT16 getrevbufflen (AT_VOID *revbuff);
extern AT_SINT16 copyrevbufftoflat (AT_UBYTE *flat, AT_VOID *revbuf);
extern AT_VOID dl_receiver (AT_UBYTE *packet, AT_SINT16 len, AT_SINT16 ifn,
							AT_SINT16 dh, AT_NINT16 type);

#endif /* !AT_DLAPI_H */

#ifdef __cplusplus
} /* End of extern "C" { */
#endif /* __cplusplus */
