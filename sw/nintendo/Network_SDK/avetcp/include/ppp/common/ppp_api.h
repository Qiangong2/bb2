/*
 * AVE-TCPv6.0 source code.
 *
 * ppp_api.h
 *
 *
 * Copyright (C) 1992-2001 ACCESS CO.,LTD.
 * All rights are reserved by ACCESS CO.,LTD.,
 * whether the whole or part of the source code
 * including any modifications is concerned.
 */

#ifdef __cplusplus /* Support for C++ */
extern "C" {
#endif /* __cplusplus */

#ifndef PPP_API_H
#define PPP_API_H

/*
 * Modem property.
 */
typedef struct AT_ppp_modem_property
{
	AT_SINT16 needToInit;		/* Need to initialize */
	AT_SINT16 needToDial;		/* Need to dial */
	AT_SINT16 needToCd;			/* Need carrier detection */
	AT_SINT16 needToDisc;		/* Need disconnection */
	AT_SINT16 connectHasSpeedReply; /* Has a speed word in greetings */
	AT_UBYTE *toneDialCommand;	/* Tone dial command (eg. "ATDT") */
	AT_UBYTE *pulseDialCommand; /* Palse dial command (eg. "ATDP") */
	AT_UBYTE *answerCommand;	/* Answer command (eg. "ATA") */
	AT_UBYTE *okReply;			/* Okay reply (eg. "OK") */
	AT_UBYTE *connectReply;		/* Connection reply (eg. "CONNECT") */
	AT_UBYTE *busyReply;		/* Busy reply (eg. "BUSY") */
	AT_UBYTE *errorReply;		/* Error reply (eg. "ERROR") */
	AT_UBYTE *delayedReply;		/* Delay reply (eg. "DELAYED") */
	AT_UBYTE *noCarrierReply;	/* Non carrer reply (eg. "NO CARRIER") */
	AT_UBYTE *noDialToneReply;	/* Non tone reply (eg. "NO DIALTONE") */
	AT_UBYTE *ringinMsg;		/* Ring reply (eg. "RING") */
	AT_UBYTE newLineChar;		/* New line charactor (eg. '\r') */
	AT_UBYTE *initScript;		/* Modem initialize script */
	AT_UBYTE *connScript;		/* Connection script */
	AT_UBYTE *discScript;		/* Disconnect script */

	/* GAMECUBE */
        AT_UBYTE *countryCode;          /* country code is Japan when NULL */
#define AT_GC_MDM_CC_JAPAN "00"
#define AT_GC_MDM_CC_US "B5"

        AT_SINT32 connectMode;          /* connection standards */
#define AT_GC_MDM_CN_DEFAULT 0
#define AT_GC_MDM_CN_V90 1
#define AT_GC_MDM_CN_V34 2
#define AT_GC_MDM_CN_V32B 3
#define AT_GC_MDM_CN_V22B 4
#define AT_GC_MDM_CN_PIAFS 5

        AT_SINT32 errorCorrectMode;     /* error correction standards */
#define AT_GC_MDM_EC_DEFAULT 0
#define AT_GC_MDM_EC_V42 1
#define AT_GC_MDM_EC_MNP 2
#define AT_GC_MDM_EC_DIRECT 3

        AT_SINT32 compressMode;         /* compression standards */
#define AT_GC_MDM_CM_DEFAULT 0
#define AT_GC_MDM_CM_V42B 1
#define AT_GC_MDM_CM_MNP5 2
#define AT_GC_MDM_CM_NONE 3
#define AT_GC_MDM_CM_BOTH 4

        AT_SINT32 atcommand_count;      /* AT command count */
        AT_UBYTE *atcommand;            /* AT command
                                                                 * attach \r to end of command
                                                                 * separate with '\0' with multiple commands */
} AT_PPP_MODEM_PROPERTY;

/*
 * Serial parameter.
 */
typedef struct AT_ppp_serial_param
{
	AT_SINT16 speed;	/* Boud rate 0:2400, 1:4800, 2:9600,
						 * 3:19200, 4:38400 5:57600 6:115200 */
	AT_SINT16 stop;		/* Stop bit 0:1bit, 1:2bit */
	AT_SINT16 parity;	/* Parity 0: Non, 1: Odd, 2: Even */
	AT_SINT16 databit;	/* Data length 2: 7 bit, 3: 8bit */
	AT_SINT16 flow;		/* Hardware flow control 0: Non, 1: RTS, 2: DTR */
} AT_PPP_SERIAL_PARAM;

/*
 * Telephone parameter.
 */
typedef struct AT_ppp_telephone_param
{
	AT_SINT16 dialtype;		/* Dial type 0: tone, 1: Pulse */
	AT_SINT16 outside_line;	/* Outside line 0: No, 1: Yes */
	AT_SINT16 timeout;		/* Connection timeout (NOT USED) */
	AT_SINT16 dial_retry;	/* Dial retry (1-99) (NOT USED) */
	AT_SINT16 dial_interval;/* Dial retry interval (sec < 30) */
	AT_UBYTE *outside_number;/* Outside line number */
} AT_PPP_TELEPHONE_PARAM;

/*
 * Connection parameter.
 */
typedef struct AT_ppp_connect_param
{
	AT_SINT16 recognize;	/* Authentication
							 * 0:PAP, 1: No auth 2:CHAP, 4: Remote demand
							 */
	AT_SINT16 mru;			/* Muxmum Receive Unit (default 1500) (< 2048) */
	AT_SINT32 magic_number;	/* Magic number (Hex 8 digit) */
	AT_SINT16 acfcomp;		/* HDLC header compression 0: enable, 1: disable */
	AT_SINT16 protocomp;	/* PPP header compression 0: enable, 1: disable */
	AT_SINT16 vjcomp;		/* VJ header compression 0: enable, 1: disable */
} AT_PPP_CONNECT_PARAM;

/*
 * IPCP option parameter.
 */
typedef struct AT_ppp_ipcp_param
{
	AT_NINT32 local_ip;		/* Local IP address */
	AT_NINT32 remote_ip;	/* Remote IP address */
	AT_NINT32 local_dns1;	/* Local DNS server address #1 */
	AT_NINT32 local_dns2;	/* Local DNS server address #2 */
	AT_NINT32 remote_dns1;	/* Remote DNS server address #1 */
	AT_NINT32 remote_dns2;	/* Remote DNS server address #2 */
} AT_PPP_IPCP_PARAM;

#ifdef AT_MODUL_IPv6
/*
 * IPv6CP option parameter.
 */
typedef struct AT_ppp_ipv6cp_param
{
	AT_UBYTE local_id[8];		/* Local interface identifier */
	AT_UBYTE remote_id[8];		/* Remote interface identifier */
} AT_PPP_IPV6CP_PARAM;
#endif /* AT_MODUL_IPv6 */

/*
 * ppp_start() argument.
 */
typedef AT_VOID (*AT_PP_START_NOTIFY) (AT_SINT32, AT_SINT32);

typedef struct AT_ppp_start_arg
{
	AT_UINT16 mode;				/* PPP mode (Default: PP_MODE_DIALOUT) */
#define PP_MODE_DIALOUT 1
#define PP_MODE_DIALIN 2
#define PP_MODE_PPPOE 4
#define PP_MODE_DIALIN_OUT (PP_MODE_DIALOUT|PP_MODE_DIALIN)
	AT_SINT16 iptype;			/* IP type (IPv4/IPv6/Dual) */
	AT_SINT16 wait;				/* blocking/non-blocking */
	AT_UBYTE *login;			/* account */
	AT_UBYTE *password;			/* password */
	AT_UBYTE *tele_number;		/* telephone # */
	AT_PP_START_NOTIFY notify;	/* notify function */
	AT_PPP_SERIAL_PARAM serial;	/* serial parameter */
	AT_PPP_TELEPHONE_PARAM tele;/* telephone parameter */
	AT_PPP_CONNECT_PARAM conn;	/* connection parameter */
	AT_PPP_IPCP_PARAM ipcp;		/* IPCP option */
#ifdef AT_MODUL_IPv6
	AT_PPP_IPV6CP_PARAM ipv6cp;	/* IPv6CP option */
#endif /* AT_MODUL_IPv6 */
} AT_PPP_START_ARG;

/*
 * ppp_stop() argument.
 */
typedef struct AT_ppp_stop_arg
{
	AT_SINT16 wait; /* blocking/non-blocking */
} AT_PPP_STOP_ARG;

/*
 * ppp_stat() argument.
 */
typedef struct AT_ppp_stat_arg
{
	AT_SINT16 ip4_valid;
#ifdef AT_MODUL_IPv6
	AT_SINT16 ip6_valid;
#endif /* AT_MODUL_IPv6 */
	AT_SINT16 state;
#define AT_PPP_STAT_UNUSED 0
#define AT_PPP_STAT_OPEN_STANDBY 1
#define AT_PPP_STAT_DIALING 2
#define AT_PPP_STAT_AUTH 3
#define AT_PPP_STAT_ESTABLISHED 4
#define AT_PPP_STAT_DISCONNECTED 5
#define AT_PPP_STAT_CLOSE_STANDBY 6
#define AT_PPP_STAT_HANG_UP 7
#define AT_PPP_STAT_FAIL 8
#define AT_PPP_STAT_TERMINAL_MODE 9
#define AT_PPP_STAT_LISTEN 10
	AT_SINT16 error;
#define AT_PPP_API_SUCCESS			0
#define AT_PPP_API_ERR_MODEM		1
#define AT_PPP_API_ERR_BUSY			2
#define AT_PPP_API_ERR_NO_DIAL_TONE 3
#define AT_PPP_API_ERR_SCRIPT		4
#define AT_PPP_API_ERR_LCP			5
#define AT_PPP_API_ERR_AUTH			6
#define AT_PPP_API_ERR_IPCP			7
#define AT_PPP_API_ERR_NO_CARRIER	8		/* 2001/07/20 */
#define AT_PPP_API_ERR_NO_ANSWER	9		/* 2001/08/02 */
#define AT_PPP_API_ERR_DELAYED		10		/* 2001/08/02 */
#define AT_PPP_API_ERR_BLACKLISTED	11		/* 2001/08/02 */
#define AT_PPP_API_ERR_TIME_OUT		12		/* 2001/07/20 */
#define AT_PPP_API_ERR_USER_REQ		13		/* 2001/07/20 */
#define AT_PPP_API_ERR_PADI_TIMEOUT 14
#define AT_PPP_API_ERR_PADR_TIMEOUT 15
#define AT_PPP_API_ERR_SERVICE_NAME_ERROR 16
#define AT_PPP_API_ERR_AC_SYSTEM_ERROR 17
#define AT_PPP_API_ERR_GENERIC_ERROR 18
	AT_SINT32 baud_rate;	/* Baud rate */
	AT_SINT16 port_inuse;	/* Modem status */
	AT_PPP_IPCP_PARAM ipcp;
#ifdef AT_MODUL_IPv6
	AT_PPP_IPV6CP_PARAM ipv6cp;
#endif /* AT_MODUL_IPv6 */
} AT_PPP_STAT_ARG;

extern AT_SINT16 ppp_init (AT_VOID);
extern AT_SINT16 ppp_term (AT_VOID);
extern AT_SINT16 ppp_start (AT_PPP_START_ARG *p);
extern AT_SINT16 ppp_stop (AT_PPP_STOP_ARG *p);
extern AT_SINT16 ppp_stat (AT_PPP_STAT_ARG *p);
extern AT_PPP_MODEM_PROPERTY *ppp_set_modem_property (AT_PPP_MODEM_PROPERTY *m);
extern AT_PPP_MODEM_PROPERTY *ppp_get_modem_property (AT_VOID);
extern AT_UINT32 ppp_idle_time (AT_VOID);

extern AT_SINT16 ppp_set_opt(AT_UINT32 type, AT_UINT32 value);
extern AT_SINT16 ppp_get_opt(AT_UINT32 type, AT_UINT32 *value);
#define AT_PPP_OPT_TIMER_INTERVAL 1

extern AT_SINT32 ppp_get_info(AT_UINT32 type, AT_VOID *buf, AT_UINT32 buflen);
#define AT_PPP_INFO_AUTH_MESSAGE 1

#endif /* !PPP_API_H */

#ifdef __cplusplus
} /* End of extern "C" { */
#endif /* __cplusplus */
