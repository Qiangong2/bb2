/*
 * AVE-TCPv6.0 source code.
 *
 * at_cfg.h
 *
 *
 * Copyright (C) 1992-2001 ACCESS CO.,LTD.
 * All rights are reserved by ACCESS CO.,LTD.,
 * whether the whole or part of the source code
 * including any modifications is concerned.
 */

#ifdef __cplusplus /* Support for C++ */
extern "C" {
#endif /* __cplusplus */

#ifndef AT_CFG_H
#define AT_CFG_H

#ifdef LB_H
extern AT_VOID AT_LB_Assign (AT_LB_PARAM *p);
extern AT_VOID AT_LB_Unassign (AT_LB_PARAM *p);
#endif /* LB_H */

#ifdef DIX_H
extern AT_VOID AT_DIX_Assign (AT_DIX_PARAM *p);
extern AT_VOID AT_DIX_Unassign (AT_DIX_PARAM *p);
#endif /* DIX_H */

#ifdef DL_H
extern AT_VOID AT_DL_Assign (AT_DL_PARAM *p);
extern AT_VOID AT_DL_Unassign (AT_DL_PARAM *p);
#endif /* DL_H */

#ifdef ARP_H
extern AT_VOID AT_ARP_Assign (AT_ARP_PARAM *p);
extern AT_VOID AT_ARP_Unassign (AT_ARP_PARAM *p);
#endif /* ARP_H */

#ifdef MP_H
extern AT_VOID AT_MP_Assign (AT_MP_PARAM *p);
extern AT_VOID AT_MP_Unassign (AT_MP_PARAM *p);
#endif /* MP_H */

#ifdef VPTR_H
extern AT_VOID AT_VP_Assign (AT_BUFFS_PARAM *p);
extern AT_VOID AT_VP_Unassign (AT_BUFFS_PARAM *p);
#endif /* VPTR_H */

#ifdef REASM_H
extern AT_VOID AT_REASM_Assign (AT_REASM_PARAM *p);
extern AT_VOID AT_REASM_Unassign (AT_REASM_PARAM *p);
#endif /* REASM_H */

#ifdef IP4ROUTE_H
extern AT_VOID AT_IP4_ROUTE_Assign (AT_IP4_ROUTE_PARAM *p);
extern AT_VOID AT_IP4_ROUTE_Unassign (AT_IP4_ROUTE_PARAM *p);
#endif /* IP4ROUTE_H */

#ifdef IP4_H
extern AT_VOID AT_IP4_Assign (AT_IP4_PARAM *p);
extern AT_VOID AT_IP4_Unassign (AT_IP4_PARAM *p);
#endif /* IP4_H */

#ifdef NDP_H
extern AT_VOID AT_NDP_Assign (AT_NDP_PARAM *p);
extern AT_VOID AT_NDP_Unassign (AT_NDP_PARAM *p);
#endif /* NDP_H */

#ifdef IP6ROUTE_H
extern AT_VOID AT_IP6_ROUTE_Assign (AT_IP6_ROUTE_PARAM *p);
extern AT_VOID AT_IP6_ROUTE_Unassign (AT_IP6_ROUTE_PARAM *p);
#endif /* IP6ROUTE_H */

#ifdef IP6_H
extern AT_VOID AT_IP6_Assign (AT_IP6_PARAM *p);
extern AT_VOID AT_IP6_Unassign (AT_IP6_PARAM *p);
#endif /* IP6_H */

#ifdef UDP_H
extern AT_VOID AT_UDP_Assign (AT_UDP_PARAM *p);
extern AT_VOID AT_UDP_Unassign (AT_UDP_PARAM *p);
#endif /* UDP_H */

#ifdef TCP_H
extern AT_VOID AT_TCP_Assign (AT_TCP_PARAM *p);
extern AT_VOID AT_TCP_Unassign (AT_TCP_PARAM *p);
#endif /* TCP_H */

#ifdef DHCP4_H
extern AT_VOID AT_DHCP4_Assign (AT_DHCP4_PARAM *p);
extern AT_VOID AT_DHCP4_Unassign (AT_DHCP4_PARAM *p);
#endif /* DHCP4_H */

#ifdef DNS_H
extern AT_VOID AT_DNS_Assign (AT_DNS_PARAM *p);
extern AT_VOID AT_DNS_Unassign (AT_DNS_PARAM *p);
#endif /* DNS_H */

extern AT_SINT16 AT_GC_CheckCfgFlag(AT_SINT16 flag);
extern AT_VOID AT_GC_SetCfgFlag(AT_SINT16 flag, AT_SINT16 onoff);
#define AT_GC_CFG_LB 0x1
#define AT_GC_CFG_DIX 0x2
#define AT_GC_CFG_ARP 0x4
#define AT_GC_CFG_DNS 0x8

#endif /* !AT_CFG_H */

#ifdef __cplusplus
} /* End of extern "C" { */
#endif /* __cplusplus */
