/*
 * AVE-TCPv6.0 source code.
 *
 * at_stdlib.h
 *
 *
 * Copyright (C) 1992-2001 ACCESS CO.,LTD.
 * All rights are reserved by ACCESS CO.,LTD.,
 * whether the whole or part of the source code
 * including any modifications is concerned.
 */

#ifdef __cplusplus /* Support for C++ */
extern "C" {
#endif /* __cplusplus */

#ifndef AT_STDLIB_H
#define AT_STDLIB_H

AT_VOID *AT_memcpy (AT_VOID *d, AT_VOID *s, AT_SINT32 n);
AT_VOID *AT_memset (AT_VOID *d, AT_SINT32 c, AT_SINT32 n);
AT_SINT32 AT_memcmp (AT_VOID *b1, AT_VOID *b2, AT_SINT32 n);

#ifdef AT_MODUL_PPP
AT_VOID *AT_memmove (AT_VOID *d, AT_VOID *s, AT_UINT32 n);
AT_UINT32 AT_strlen (AT_UBYTE *s);
AT_UBYTE *AT_strcpy (AT_UBYTE *d, AT_UBYTE *s);
AT_SINT32 AT_strcmp (AT_UBYTE *s1, AT_UBYTE *s2);
AT_SINT32 AT_strncmp (AT_UBYTE *s1, AT_UBYTE *s2, AT_UINT32 n);
AT_SINT32 AT_strnicmp (AT_UBYTE *s1, AT_UBYTE *s2, AT_UINT32 n);
AT_UBYTE *AT_strcat (AT_UBYTE *s1, AT_UBYTE *s2);
#endif /* AT_MODUL_PPP */

#endif /* !AT_STDLIB_H */

#ifdef __cplusplus
} /* End of extern "C" { */
#endif /* __cplusplus */
