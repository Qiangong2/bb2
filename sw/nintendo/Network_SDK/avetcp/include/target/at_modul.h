/*
 * AVE-TCPv6.0 source code.
 *
 * at_modul.h
 *
 *
 * Copyright (C) 1992-2001 ACCESS CO.,LTD.
 * All rights are reserved by ACCESS CO.,LTD.,
 * whether the whole or part of the source code
 * including any modifications is concerned.
 */

#ifdef __cplusplus /* Support for C++ */
extern "C" {
#endif /* __cplusplus */

#ifndef AT_MODUL_H
#define AT_MODUL_H

#define AT_MODUL_LOOPBACK
#define AT_MODUL_DIX
#define AT_MODUL_ARP
#define AT_MODUL_PPP
#undef AT_MODUL_MIB /* XXX MIB is not yet supported */
#define AT_MODUL_IPv4
#define AT_MODUL_IPv4_FORWARD
#define AT_MODUL_IPv6
#define AT_MODUL_IPv6_FORWARD
#define AT_MODUL_TCP_FAST_RECOVERY
#define AT_MODUL_TCP_KEEP_OOO_SEGMENT
#define AT_MODUL_TCP_SACK

#define AT_MODUL_TCP_LARGE_WINDOW
#undef AT_MODUL_DHCPv4
#define AT_MODUL_DNS

#define AT_GAMECUBE
#define AT_GC_VERSION "1.25"

#ifdef _DEBUG
#define AT_GC_DEBUG
#endif

#define AT_DPLEVEL_API 1
#define AT_DPLEVEL_TCP 2
#define AT_DPLEVEL_UDP 4
#define AT_DPLEVEL_ICMP 8
#define AT_DPLEVEL_IP 16
#define AT_DPLEVEL_ARP 32
#define AT_DPLEVEL_DNS 64
#define AT_DPLEVEL_PKT 128
#define AT_DPLEVEL_OS 256
#define AT_DPLEVEL_PPP 512
#define AT_DPLEVEL_PPPOE 1024
#define AT_DPLEVEL_PPP2 2048
#define AT_DPLEVEL_DHCP 4096

#ifdef AT_GC_DEBUG
extern int AT_GC_debug_flag;
#define AT_DP_MSG(x) {AT_DebugPrint("avetcp: ");AT_DebugPrint x;}
#define AT_DP(x) {if (AT_GC_debug_flag & AT_DPLEVEL_API) {AT_DebugPrint("avetcp:");AT_DebugPrint x;}}
#define AT_DP_TCP(x) {if (AT_GC_debug_flag & AT_DPLEVEL_TCP) {AT_DebugPrint x;}}
#define AT_DP_UDP(x) {if (AT_GC_debug_flag & AT_DPLEVEL_UDP) {AT_DebugPrint x;}}
#define AT_DP_ICMP(x) {if (AT_GC_debug_flag & AT_DPLEVEL_ICMP) {AT_DebugPrint x;}}
#define AT_DP_IP(x) {if (AT_GC_debug_flag & AT_DPLEVEL_IP) {AT_DebugPrint x;}}
#define AT_DP_ARP(x) {if (AT_GC_debug_flag & AT_DPLEVEL_ARP) {AT_DebugPrint x;}}
#define AT_DP_DNS(x) {if (AT_GC_debug_flag & AT_DPLEVEL_DNS) {AT_DebugPrint x;}}
#define AT_DP_PKT(x) {if (AT_GC_debug_flag & AT_DPLEVEL_PKT) {AT_DebugPrint x;}}
#define AT_DP_OS(x) {if (AT_GC_debug_flag & AT_DPLEVEL_OS) {AT_DebugPrint x;}}
#define AT_DP_PPP(x) {if (AT_GC_debug_flag & AT_DPLEVEL_PPP) {AT_DebugPrint x;}}
#define AT_DP_PPPOE(x) {if (AT_GC_debug_flag & AT_DPLEVEL_PPPOE) {AT_DebugPrint x;}}
#define AT_DP_PPP2(x) {if (AT_GC_debug_flag & AT_DPLEVEL_PPP2) {AT_DebugPrint x;}}
#define AT_DP_DHCP(x) {if (AT_GC_debug_flag & AT_DPLEVEL_DHCP) {AT_DebugPrint x;}}
#define AT_DP_DUMP(level, p, len) { \
	if (AT_GC_debug_flag & (level)) { \
		AT_SINT32 i; \
		AT_DebugSetColor(1); \
		for (i = 0; i < (len); i++) { \
			AT_DebugPrint("%02x ", *((p)+i)); \
			if ((i % 16) == 15) AT_DebugPrint("\n"); \
		} \
		if ((i % 16) != 0) AT_DebugPrint("\n"); \
		AT_DebugSetColor(0); \
	} \
}
#define AT_DP_TIMESTAMP(level) {\
	if (AT_GC_debug_flag & (level)) { \
		AT_DebugSetColor(3); \
		AT_DebugPrintTimeStamp(); \
		AT_DebugSetColor(0); \
	} \
}
#define AT_DE(x) AT_DebugEvent(x)
#else /* AT_GC_DEBUG */
#define AT_DP_MSG(x)
#define AT_DP(x)
#define AT_DP_TCP(x)
#define AT_DP_UDP(x)
#define AT_DP_ICMP(x)
#define AT_DP_IP(x)
#define AT_DP_ARP(x)
#define AT_DP_DNS(x)
#define AT_DP_PKT(x)
#define AT_DP_OS(x)
#define AT_DP_PPP(x)
#define AT_DP_PPPOE(x)
#define AT_DP_PPP2(x)
#define AT_DP_DHCP(x)
#define AT_DP_DUMP(level, p, len)
#define AT_DP_TIMESTAMP(level)
#define AT_DE(x)
#endif /* AT_GC_DEBUG */

/*
 * Module dependency rules.
 */
#ifdef AT_MODUL_TCP_SACK
#ifndef AT_MODUL_TCP_KEEP_OOO_SEGMENT
#error You should define AT_MODUL_TCP_KEEP_OOO_SEGMENT!!
#endif /* !AT_MODUL_TCP_KEEP_OOO_SEGMENT */
#endif /* AT_MODUL_TCP_SACK */

#endif /* !AT_MODUL_H */

#ifdef __cplusplus
} /* End of extern "C" { */
#endif /* __cplusplus */
