/*
 * AVE-TCPv6.0 source code.
 *
 * at_prm.h
 *
 *
 * Copyright (C) 1992-2001 ACCESS CO.,LTD.
 * All rights are reserved by ACCESS CO.,LTD.,
 * whether the whole or part of the source code
 * including any modifications is concerned.
 */

#ifndef AT_PRM_H
#define AT_PRM_H

#ifdef AT_MODUL_LOOPBACK
/*
 * Loopback module.
 */
/* AT_LB_BUFFER_LENGTH buffer size definition IPv4 576-4096 IPv6 1280-4096 */
#define AT_LB_BUFFER_LENGTH 1280

/* AT_LB_BUFFER_NUMBER buffer number definition 2-127 */
#define AT_LB_BUFFER_NUMBER 2

#endif /* AT_MODUL_LOOPBACK */

#ifdef AT_MODUL_DIX
/*
 * DIX module.
 */
/* AT_DIX_SWITCH_NUMBER device number definition 1-127 */
#define AT_DIX_SWITCH_NUMBER 1

#define AT_DIX_DEV_NUMBER AT_DIX_SWITCH_NUMBER

/* AT_DIX_DH_NUMBER device number  * upper protocol number 1-127 */
/* e.g. DIX(n) * ( IPv4(1) + ARP(1) + IPv6(1) ) = 3n */
#define AT_DIX_DH_NUMBER (3 * AT_DIX_DEV_NUMBER)

/* AT_DIX_BUFF_NUMBER device receive buffer number 1-127 */
/* e.g. DIX(n) * 8 */
#define AT_DIX_BUFF_NUMBER (8 * AT_DIX_DEV_NUMBER)

#endif /* AT_MODUL_DIX */

/*
 * Datalink module.
 */
/* AT_DL_NUMBER data link number 1-127 */
/* e.g.  LOOPBACK(1) + DIX(1) + PPP(1) = 3 */
#define AT_DL_NUMBER 3

#ifdef AT_MODUL_ARP
/*
 * ARP module.
 */
/* AT_ARP_CACHE_NUMBER ARP cache entry number 1-127 */
#define AT_ARP_CACHE_NUMBER 8

/* AT_ARP_CTRL_NUMBER ARP control table entry number 1-127 */
/* table link number * (use ARP) upper protocol number */
/* e.g. DL(3) * IPv4(1)  = 3 */
#define AT_ARP_CTRL_NUMBER 3

/* AT_ARP_ALIAS_NUMBER ARP separate name(additional IP address) entry number 0-127 */
/* e.g. CTRL(3) * additional address(3)  = 9 */
#define AT_ARP_ALIAS_NUMBER 9

/* AT_ARP_PENDING_NUMBER ARP resolution wait entry number 0-AT_ARP_CACHE_NUMBER */
#define AT_ARP_PENDING_NUMBER 2

/* AT_ARP_PENDING_DATASIZE ARP resolution wait packet avoid region size 1500- */
/* data link MTU maximum value, e.g. DIX 1500Byte */
#define AT_ARP_PENDING_DATASIZE 1500

/* AT_ARP_RESOLV_TTL TTL after ARP cache resolution, 1-127 */
#define AT_ARP_RESOLV_TTL 20

/* AT_ARP_PENDING_TTL ARP cache resolution wait TTL 1-127 */
#define AT_ARP_PENDING_TTL 3

#endif /* AT_MODUL_ARP */

#ifdef AT_MODUL_IPv6
/*
 * NDP module.
 */
/* AT_NDP_NEIGH_NUMBER NDP neighbor cache entry number 1-127 */
#define AT_NDP_NEIGH_NUMBER 8

/* AT_NDP_DEFAULT_NUMBER NDP default router list entry number 1-127 */
#define AT_NDP_DEFAULT_NUMBER 8

/* AT_NDP_PENDING_NUMBER NDP resolution wait entry number 0-AT_NDP_NEIGH_NUMBER */
#define AT_NDP_PENDING_NUMBER 2

/* AT_NDP_PENDING_DATALEN NDP resolution wait packet avoid area size 1500- */
/* data link MTU maximum value, e.g. DIX 1500Byte */
#define AT_NDP_PENDING_DATALEN 1500

#endif /* AT_MODUL_IPv6 */

/*
 * IP reassemble module.
 */
/* AT_REASM_FRG_LIMIT ressembly possible fragment number 1-8 */
#define AT_REASM_FRG_LIMIT 8

/* AT_REASM_PKT_NUMBER packets reassembled 0-127 */
#define AT_REASM_PKT_NUMBER 2

/* AT_REASM_FRG_NUMBER fragmented packet number 0-127 */
#define AT_REASM_FRG_NUMBER 16

#ifdef AT_MODUL_IPv4
/*
 * IPv4 module.
 */
/* AT_IP4_NH_NUMBER net handle using IP 1-127 */
#define AT_IP4_NH_NUMBER 1

/* AT_IP4_DEFAULT_SERVICE ServiceType value in IPv4 header */
#define AT_IP4_DEFAULT_SERVICE 0

/* AT_IP4_DEFAULT_TTL TimeToLive value in IPv4 header */
#define AT_IP4_DEFAULT_TTL 120 /* 2001.11.30 */

#endif /* AT_MODUL_IPv4 */

#ifdef AT_MODUL_IPv6
/*
 * IPv6 module.
 */
/* AT_IP6_NH_NUMBER net handle number using IP 1-127 */
#define AT_IP6_NH_NUMBER 1

/* AT_IP6_DEFAULT_TRAFFIC_CLASS traffic class value in IPv6 header */
#define AT_IP6_DEFAULT_TRAFFIC_CLASS 0

/* AT_IP6_DEFAULT_FLOW_LABEL traffic class value in IPv6 header */
#define AT_IP6_DEFAULT_FLOW_LABEL 0

/* AT_IP6_DEFAULT_HOP_LIMIT hop limit value in IPv6 header */
#define AT_IP6_DEFAULT_HOP_LIMIT 64

#endif /* AT_MODUL_IPv6 */

#ifdef AT_MODUL_IPv4
/*
 * IPv4 routing module.
 */
/* AT_IP4_ROUTE_ADDR_NUMBER IPv4 address table entry number 1-127 */
/* e.g. DL(3) = 3 */
#define AT_IP4_ROUTE_ADDR_NUMBER AT_DL_NUMBER

/* AT_IP4_ROUTE_ALIAS_NUMBER IPv4 additional address table entry number 0-127 */
/* e.g. IP4ADDR(3)  * additional address(3) = 9 */
#define AT_IP4_ROUTE_ALIAS_NUMBER (AT_DL_NUMBER * 3)

/* AT_IP4_ROUTE_TABLE_NUMBER IPv4 routing table entry number 1-127 */
/* e.g. (IP4ADDR(3)  + AT_IP4_ROUTE_ALIAS_NUMBER(9) ) * 2 + router(2) = 26 */
#define AT_IP4_ROUTE_TABLE_NUMBER ((AT_IP4_ROUTE_ADDR_NUMBER + AT_IP4_ROUTE_ALIAS_NUMBER) * 2 + 2)

#endif /* AT_MODUL_IPv4 */

#ifdef AT_MODUL_IPv6
/*
 * IPv6 routing module.
 */
/* AT_IP6_ROUTE_ADDR_NUMBER IPv6 address table entry number 1-127 */
/* e.g. DL(3) = 3 */
#define AT_IP6_ROUTE_ADDR_NUMBER AT_DL_NUMBER

/* AT_IP6_ROUTE_ALIAS_NUMBER IPv6 additional address table entry number 0-127 */
/* e.g. IP6ADDR(3)  * additional address (3) = 9 */
#define AT_IP6_ROUTE_ALIAS_NUMBER (AT_DL_NUMBER * 3)

/* AT_IP6_ROUTE_PMTU_NUMBER IPv6 Path MTU table entry number 1-127 */
#define AT_IP6_ROUTE_PMTU_NUMBER (AT_DL_NUMBER * 3)

/* AT_IP6_ROUTE_TABLE_NUMBER IPv6 routing table entry number 1-127 */
/* e.g. (IP6ADDR(3)  + AT_IP6_ROUTE_ALIAS_NUMBER(9) ) * 2 + router(2) = 26 */
#define AT_IP6_ROUTE_TABLE_NUMBER ((AT_IP6_ROUTE_ADDR_NUMBER + AT_IP6_ROUTE_ALIAS_NUMBER) * 2 + 2)

/* AT_IP6_ROUTE_DAD_DELAY number of seconds until DAD start 1- */
#define AT_IP6_ROUTE_DAD_DELAY 1

/* AT_IP6_ROUTE_DAD_INTERVAL number of seconds of DAD interval 1- */
#define AT_IP6_ROUTE_DAD_INTERVAL 1

/* AT_IP6_ROUTE_DAD_TIMES DAD number 1- */
#define AT_IP6_ROUTE_DAD_TIMES 1

/* AT_IP6_ROUTE_RD_DELAY number of seconds until RD start 1- */
#define AT_IP6_ROUTE_RD_DELAY 1

/* AT_IP6_ROUTE_RD_INTERVAL number of seconds of AD interval 1- */
#define AT_IP6_ROUTE_RD_INTERVAL 1

/* AT_IP6_ROUTE_RD_TIMES RD count 1- */
#define AT_IP6_ROUTE_RD_TIMES 2

#endif /* AT_MODUL_IPv6 */

/*
 * UDP module.
 */
/* AT_UDP_NH_NUMBER net handle number using UDP 1-127 */
#define AT_UDP_NH_NUMBER 8

/*
 * TCP module.
 */
/* AT_TCP_NH_NUMBER net handle number using TCP 1-127 */
#define AT_TCP_NH_NUMBER 8

/* AT_TCP_RCV_WIN_SIZE receive window size maximum value */
/* Maximum value is AT_MP_PAGE_SIZE when set aside window from internal memory pool. */
/* AT_MODUL_TCP_EXPOOL maximum value is 65535 when defining. */
#define AT_TCP_RCV_WIN_SIZE 8192

/* AT_TCP_SND_WIN_SIZE maximum value of send window size */
/* Maximum value is AT_MP_PAGE_SIZE when set aside window from internal memory pool. */
/* AT_MODUL_TCP_EXPOOL maximum value is 65535 when defining.*/
#define AT_TCP_SND_WIN_SIZE 8192

/* delay ACK time */
#define AT_TCP_DELAY_ACK_TIME 100

/*
 * Vptr module.
 */
/* AT_BUFFS_NUMBER X entry number 5 */
/* reference vptr.h */

/*
 * Memory pool module.
 */
/* AT_MP_BLK_NUMBER memory pool entry number 1-256 */
/* TcpWinsize * (Send + Resv) / Pagesize + reassemble */
/* PageSize references mempool.h. */

#ifdef AT_MODUL_TCP_LARGE_WINDOW
#define AT_MP_BLK_NUMBER AT_REASM_PKT_NUMBER
#else
/* When window size of receive and send differ, excess may be seen in memory pool in following format. */
/* Set optimal page number to avoid problem of splitting across memory pool pages and not being able to set aside window. */
#define AT_MP_BLK_NUMBER \
	(((AT_TCP_NH_NUMBER * (AT_TCP_SND_WIN_SIZE + AT_TCP_RCV_WIN_SIZE) + (AT_MP_PAGE_SIZE - 1)) / AT_MP_PAGE_SIZE) + AT_REASM_PKT_NUMBER)
#endif /* AT_MODUL_TCP_EXPOOL */

/* AT_MP_AREA_SIZE  actual memory pool */
/* (AT_MP_BLK_NUMBER * AT_MP_PAGE_SIZE) */
#define AT_MP_AREA_SIZE (AT_MP_BLK_NUMBER * AT_MP_PAGE_SIZE)

#ifdef AT_MODUL_DHCPv4
/*
 * Parameters for DHCPv4 module.
 */
/* Interval timer */
#define AT_DHCP4_INTERVAL_TIME 	2000	// org 100 by t.niioka
/* Timeout */
#define AT_DHCP4_FIRST_TIME_OUT 30000	// org 500 by t.niioka
/* Retry number */
#define AT_DHCP4_MAX_RETRY 		10		// org 4 by t.niioka

#endif /* AT_MODUL_DHCPv4 */

#ifdef AT_MODUL_DNS
/*
 * Parameters for DNS module.
 */
#define AT_DNS_INITIAL_ID 0
#define AT_DNS_INTERVAL_NUMBER 	3
#define AT_DNS_INTERVAL_TIME_1 	2
#define AT_DNS_INTERVAL_TIME_2 	4
#define AT_DNS_INTERVAL_TIME_3 	8
#define AT_DNS_NAME_SERVER_NUMBER 2
#define AT_DNS_BUFFER_SIZE 512
#define AT_DNS_BUFFER_NUMBER 9
#define AT_DNS_ADDR_CACHE_NUMBER 16
#define AT_DNS_NAME_CACHE_NUMBER 16
#define AT_DNS_ADDR_NUMBER (AT_DNS_ADDR_CACHE_NUMBER * 2)
#define AT_DNS_CONTROL_BLOCK_NUMBER 8

#endif /* AT_MODUL_DNS */

#endif /* !AT_PRM_H */
