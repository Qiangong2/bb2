/*
 * AVE-TCPv6.0 source code.
 *
 * at_end.h
 *
 *
 * Copyright (C) 1992-2001 ACCESS CO.,LTD.
 * All rights are reserved by ACCESS CO.,LTD.,
 * whether the whole or part of the source code
 * including any modifications is concerned.
 */

#ifdef __cplusplus /* Support for C++ */
extern "C" {
#endif /* __cplusplus */

#ifndef AT_END_H
#define AT_END_H

/*
 * Byte order.
 */
#define AT_LITTLE_ENDIAN	(-1)
#define AT_BIG_ENDIAN		( 1)

#define AT_ENDIAN AT_BIG_ENDIAN

#endif /* !AT_END_H */

#ifdef __cplusplus
} /* End of extern "C" { */
#endif /* __cplusplus */
