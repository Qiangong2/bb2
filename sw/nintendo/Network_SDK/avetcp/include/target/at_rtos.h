/*
 * AVE-TCPv6.0 source code.
 *
 * at_rtos.h
 *
 *
 * Copyright (C) 1992-2001 ACCESS CO.,LTD.
 * All rights are reserved by ACCESS CO.,LTD.,
 * whether the whole or part of the source code
 * including any modifications is concerned.
 */

#ifdef __cplusplus /* Support for C++ */
extern "C" {
#endif /* __cplusplus */

#ifndef AT_RTOS_H
#define AT_RTOS_H

extern AT_SINT16 AT_CreateTask (AT_VOID (*entry) (AT_VOID), AT_UINT32 *tid);
extern AT_VOID AT_DeleteTask (AT_UINT32 tid);
extern AT_VOID AT_StartTask (AT_UINT32 tid);
extern AT_VOID AT_TerminateTask (AT_UINT32 tid);
extern AT_VOID AT_SleepTask (AT_VOID);
extern AT_VOID AT_WakeUpTask (AT_UINT32 tid);
extern AT_VOID AT_YieldExecution (AT_VOID);
extern AT_VOID AT_WaitTaskWithMiliSec (AT_SINT32 msec);
extern AT_VOID AT_GetTaskId (AT_UINT32 *tidp);
extern AT_SINT16 AT_CreateSemaphore (AT_UINT32 *sid);
extern AT_VOID AT_DeleteSemaphore (AT_UINT32 sid);
extern AT_VOID AT_WaitSemaphore (AT_UINT32 sid);
extern AT_VOID AT_SignalSemaphore (AT_UINT32 sid);
extern AT_SINT16 AT_CreateMessageBox (AT_UINT32 *mbox);
extern AT_VOID AT_DeleteMessageBox (AT_UINT32 mbox);
extern AT_VOID AT_SendMessage (AT_UINT32 mbox, AT_VOID *msg, AT_SINT32 len);
extern AT_VOID AT_ReceiveMessage (AT_UINT32 mbox, AT_VOID *msg, AT_SINT32 len);
extern AT_VOID AT_GetCounter (AT_UINT32 *count);
extern AT_VOID AT_GetCurrentMiliSec (AT_UINT32 *msec);
extern AT_VOID AT_GetCurrentMicroSec (AT_UINT32 *usec);
extern AT_VOID AT_GetCurrentSec (AT_UINT32 *sec);
extern AT_SINT16 AT_InitializeRTOS (AT_VOID);
extern AT_SINT16 AT_TerminateRTOS (AT_VOID);
extern AT_VOID *AT_Malloc(AT_SINT32 size);
extern AT_VOID AT_Free(AT_VOID *address);
extern AT_SINT32 AT_SetTaskPriority(AT_SINT32 pri);
extern AT_SINT16 AT_LinkUpStatus(AT_VOID);
extern AT_SINT16 AT_SetDummyMacAddress(AT_UBYTE *mac);
extern AT_SINT16 AT_SetEtherNegoMode(AT_SINT32 mode);
extern AT_SINT16 AT_GetEtherNegoMode(AT_SINT32* mode);
#define AT_ETH_MODE_AUTO 0
#define AT_ETH_MODE_100FULL 1
#define AT_ETH_MODE_100HALF 2
#define AT_ETH_MODE_10FULL 3
#define AT_ETH_MODE_10HALF 4

typedef struct AT_init_param {
	AT_SINT16 use_dix;
	AT_SINT16 use_ppp;
	AT_UINT16 udp_port_num;
	AT_UINT16 tcp_port_num;
	AT_SINT32 tcp_window_size_max;
	AT_SINT32 os_thread_stack_size;
	AT_UINT32 serial_send_buffer_size;
	AT_UINT32 debug_buffer_size;
	AT_UINT32 dump_buffer_size;
	AT_UINT16 ether_buffer_num;
} AT_INIT_PARAM;

extern AT_SINT32 AT_GetComSendBufFreeSize(AT_VOID);
extern AT_SINT32 AT_GetRtosBuffSize(AT_INIT_PARAM* p);
extern AT_UBYTE* AT_SetRtosBuff(AT_INIT_PARAM* p, AT_UBYTE* buf, AT_UBYTE* buf_end);
AT_SINT32 AT_GetInitParam(AT_INIT_PARAM* arg);
AT_SINT32 AT_SetInitParam(AT_INIT_PARAM* p);
AT_SINT16 AT_SetWorkArea(AT_UBYTE* area, AT_SINT32 size);

#ifdef AT_GC_DEBUG
extern AT_VOID AT_DebugSaveData(AT_VOID* data);
extern AT_VOID AT_CheckThread(AT_VOID);
#endif

#endif /* !AT_RTOS_H */

#ifdef __cplusplus
} /* End of extern "C" { */
#endif /* __cplusplus */
