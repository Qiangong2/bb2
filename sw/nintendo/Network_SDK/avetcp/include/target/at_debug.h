/*
 * AVE-TCP source code.
 *
 * at_debug.h
 *
 *
 * Copyright (C) 2002 ACCESS CO.,LTD.
 * All rights are reserved by ACCESS CO.,LTD.,
 * whether the whole or part of the source code
 * including any modifications is concerned.
 */

#ifdef __cplusplus /* Support for C++ */
extern "C" {
#endif /* __cplusplus */

#ifndef AT_DEBUG_H
#define AT_DEBUG_H

extern AT_SINT16 AT_GC_DebugInit(AT_VOID);
extern AT_VOID AT_GC_DebugTerm(AT_VOID);
extern AT_VOID AT_SetDebugFlag(AT_SINT32 flag);
extern AT_VOID AT_DebugPrintMode(AT_SINT32 mode);
#define AT_DPMODE_SERIAL_OFF 1 /* Halt serial output */
#define AT_DPMODE_BUFFER_OFF 2 /* Halt output to buffer */
#define AT_DPMODE_HTML 4 /* Replace linefeed code with tag. Specify color by tag */
extern AT_UINT32 AT_ReadDebugBuffer(AT_UBYTE** buffer);

extern char* AT_GC_DebugBufferCurrent;
extern char* AT_GC_DebugBufferDisplay;
extern int AT_GC_DebugBufferSize;
extern void AT_DebugPrint(const char *in_format, ...);
extern void AT_DebugSetColor(int color);
extern void AT_DebugPrintTimeStamp(void);
extern AT_VOID AT_DebugSaveData(AT_VOID* data);
extern AT_VOID AT_CheckThread(AT_VOID);

/* Numerical process */
enum {
	AT_DE_ETHER_F0_IN_D0 = 0,
	AT_DE_ETHER_F0_IN_D1,
	AT_DE_ETHER_F0_IN_DNUM,

	AT_DE_ETHER_F1_IN_D0,
	AT_DE_ETHER_F1_IN_D1,
	AT_DE_ETHER_F1_IN_D2,
	AT_DE_ETHER_F1_IN_D3,
	AT_DE_ETHER_F1_IN_D4,
	AT_DE_ETHER_F1_IN_DNUM,

	AT_DE_PPPOE_IN_D0,
	AT_DE_PPPOE_IN_D1,
	AT_DE_PPPOE_IN_D2,
	AT_DE_PPPOE_IN_D3,
	AT_DE_PPPOE_IN_D4,
	AT_DE_PPPOE_IN_D5,
	AT_DE_PPPOE_IN_D6,
	AT_DE_PPPOE_IN_DNUM,

	AT_DE_PPP_IN_D0,
	AT_DE_PPP_IN_D1,
	AT_DE_PPP_IN_D2,
	AT_DE_PPP_IN_D3,
	AT_DE_PPP_IN_D4,
	AT_DE_PPP_IN_D5,
	AT_DE_PPP_IN_D6,
	AT_DE_PPP_IN_D7,
	AT_DE_PPP_IN_D8,
	AT_DE_PPP_IN_D9,
	AT_DE_PPP_IN_D10,
	AT_DE_PPP_IN_D11,
	AT_DE_PPP_IN_D12,
	AT_DE_PPP_IN_D13,
	AT_DE_PPP_IN_DNUM,

	AT_DE_ARP_IN_D0,
	AT_DE_ARP_IN_D1,
	AT_DE_ARP_IN_D2,
	AT_DE_ARP_IN_D3,
	AT_DE_ARP_IN_DNUM,

	AT_DE_IP4_IN_D0,
	AT_DE_IP4_IN_D1,
	AT_DE_IP4_IN_D2,
	AT_DE_IP4_IN_D3,
	AT_DE_IP4_IN_D4,
	AT_DE_IP4_IN_D5,
	AT_DE_IP4_IN_D6,
	AT_DE_IP4_IN_D7,
	AT_DE_IP4_IN_D8,
	AT_DE_IP4_IN_D9,
	AT_DE_IP4_IN_D10,
	AT_DE_IP4_IN_DNUM,

	AT_DE_IP4_FORWARD_D0,
	AT_DE_IP4_FORWARD_D1,
	AT_DE_IP4_FORWARD_D2,
	AT_DE_IP4_FORWARD_D3,
	AT_DE_IP4_FORWARD_D4,
	AT_DE_IP4_FORWARD_D5,
	AT_DE_IP4_FORWARD_DNUM,

	AT_DE_ICMP4_IN_D0,
	AT_DE_ICMP4_IN_DNUM,

	AT_DE_UDP_IN_D0,
	AT_DE_UDP_IN_D1,
	AT_DE_UDP_IN_DNUM,

	AT_DE_TCP_IN_D0,
	AT_DE_TCP_IN_D1,
	AT_DE_TCP_IN_D2,
	AT_DE_TCP_IN_D3,
	AT_DE_TCP_IN_D4,
	AT_DE_TCP_IN_D5,
	AT_DE_TCP_IN_DNUM,

	AT_DE_DROP_EVENT_NUM,

    AT_DE_ETHER_F0_IN,                          //  Counter related
	AT_DE_ETHER_F0_IN_DROP,
	AT_DE_ETHER_F1_IN,
	AT_DE_ETHER_F1_IN_DROP,
	AT_DE_ETHER_OUT,

	AT_DE_ARP_IN,
	AT_DE_ARP_IN_DROP,
	AT_DE_ARP_OUT,

	AT_DE_PPPOE_IN,
	AT_DE_PPPOE_IN_DROP,
	AT_DE_PPPOE_OUT,

	AT_DE_PPP_IN,
	AT_DE_PPP_IN_DROP,
	AT_DE_PPP_IN_IP,
	AT_DE_PPP_IN_IPV6,
	AT_DE_PPP_IN_COMP,
	AT_DE_PPP_IN_UNCOMP,
	AT_DE_PPP_IN_LCP,
	AT_DE_PPP_IN_PAP,
	AT_DE_PPP_IN_CHAP,
	AT_DE_PPP_IN_IPCP,
	AT_DE_PPP_IN_IPV6CP,
	AT_DE_PPP_OUT,
	AT_DE_PPP_OUT_IP,
	AT_DE_PPP_OUT_IPV6,
	AT_DE_PPP_OUT_COMP,
	AT_DE_PPP_OUT_UNCOMP,
	AT_DE_PPP_OUT_LCP,
	AT_DE_PPP_OUT_PAP,
	AT_DE_PPP_OUT_CHAP,
	AT_DE_PPP_OUT_IPCP,
	AT_DE_PPP_OUT_IPV6CP,

	AT_DE_IP4_IN,
	AT_DE_IP4_IN_DROP,
	AT_DE_IP4_FORWARD,
	AT_DE_IP4_FORWARD_DROP,
	AT_DE_IP4_OUT,

	AT_DE_ICMP4_IN,
	AT_DE_ICMP4_IN_DROP,
	AT_DE_ICMP4_OUT,

	AT_DE_UDP_IN,
	AT_DE_UDP_IN_DROP,
	AT_DE_UDP_OUT,

	AT_DE_TCP_IN,
	AT_DE_TCP_IN_DROP,
	AT_DE_TCP_OUT,
	AT_DE_TCP_REXMT,

	AT_DE_TOTAL_NUM
};

void AT_DebugEvent(int type);
void AT_DebugEventSetCallBackFunc(void (*)(int), void (*)(int));

/* Packet dump */
#define AT_GC_PDUMP_FILE_HEADER_LEN 24
#define AT_GC_PDUMP_PACKET_HEADER_LEN 16

#define AT_GC_PDUMP_DLT_EN10MB 1
#define AT_GC_PDUMP_DLT_PPP 9

extern char* AT_GC_PdumpBuffer;
extern int AT_GC_PdumpBufferSize;
extern AT_SINT16 AT_PacketDumpStart(AT_SINT16 snaplen, AT_SINT16 flag);
extern AT_VOID AT_PacketDumpStop(AT_VOID);
extern AT_VOID AT_PacketDumpStat(AT_VOID** out_bufp, AT_UINT32* out_bytes, AT_UINT32* out_counts);

extern AT_VOID AT_PacketDumpWrite(AT_UBYTE* buf, AT_UINT16 len, AT_UINT32 linktype);

#endif /* !AT_DEBUG_H */

#ifdef __cplusplus
} /* End of extern "C" { */
#endif /* __cplusplus */
