/*
 * AVE-TCPv6.0 source code.
 *
 * at_types.h
 *
 *
 * Copyright (C) 1992-2001 ACCESS CO.,LTD.
 * All rights are reserved by ACCESS CO.,LTD.,
 * whether the whole or part of the source code
 * including any modifications is concerned.
 */

#ifdef __cplusplus /* Support for C++ */
extern "C" {
#endif /* __cplusplus */

#ifndef AT_TYPES_H
#define AT_TYPES_H

typedef void			AT_VOID;
typedef unsigned char	AT_UBYTE;
typedef char			AT_SBYTE;
typedef unsigned short	AT_UINT16;
typedef short			AT_SINT16;
typedef unsigned long	AT_UINT32;
typedef long			AT_SINT32;
typedef AT_UINT32		AT_NINT32;
typedef AT_UINT16		AT_NINT16;
typedef AT_VOID			AT_TASK;
typedef AT_UINT32		AT_datasize_t;
typedef	AT_UINT32		tcp_seq;

/*
 * IP Address structure.
 */
typedef struct at_ip_addr
{
	AT_UINT32	type;
#define AT_IPany			0
#define AT_IPv4				4
#define AT_IPv6				6
	union
	{
		AT_NINT32	ip4;
		AT_UBYTE	ip6[16];
	} ip46;
#define AT_IPv4_ADDR_LEN	4
#define AT_IPv6_ADDR_LEN	16
} AT_IP_ADDR;
#define v4addr			ip46.ip4
#define v6addr			ip46.ip6

#endif /* !AT_TYPES_H */

#ifdef __cplusplus
} /* End of extern "C" { */
#endif /* __cplusplus */
