/*
 * AVE-TCP for Nintendo GameCube Sample Program
 *
 */

#include <stdlib.h>
#include <string.h>
#include <demo.h>

#include "avetcp.h"
#include "atdemo.h"

#define HTTP_PORT 80
#define URL_REQUEST "GET / HTTP/1.0\r\n"
#define USER_AGENT "User-Agent: AveTcp (GAMECUBE)\r\n"
#define CRLF "\r\n"

/*
 * HTTP TEST
 *
 * Send HTTP GET request to the host that is specified by TEST_SERVER_NAME.
 * Response data is serial output.
 */
s16 http_test(void)
{
	AT_SINT16 nh;
	AT_IP_ADDR addr;
	AT_SINT16 stat;
	AT_SINT16 ret;
	AT_SEND_BUFFS buff[3];
	
	ret = dns_get_addr_proc(TEST_SERVER_NAME, &addr);
	if (ret < 0) {
		goto tcp_http_get_exit;
	}
	
	ret = tcp_create();
	if (ret < 0) {
		goto tcp_http_get_exit;
	}
	nh = ret;
	gTest_nh = ret;

	ret = tcp_connect(nh, &addr, AT_HTONS(HTTP_PORT), AT_NULL);
	if (ret < 0) {
		goto tcp_http_get_delete;
	}
	for (;;) {
		if (gTestAbort) {
			goto tcp_http_get_abort;
		}
		ret = tcp_stat(nh, &stat, AT_NULL, AT_NULL, AT_NULL);
		if (ret < 0 || stat == AT_STAT_FATAL) {
			goto tcp_http_get_abort;
		}
		if (stat == AT_STAT_ESTABLISHED) {
			break;
		}
		VIWaitForRetrace();
	}
	
	buff[2].buff = (AT_UBYTE*)URL_REQUEST;
	buff[2].len = (AT_SINT16)strlen(URL_REQUEST);
	buff[1].buff = (AT_UBYTE*)USER_AGENT;
	buff[1].len = (AT_SINT16)strlen(USER_AGENT);
	buff[0].buff = (AT_UBYTE*)CRLF;
	buff[0].len = (AT_SINT16)strlen(CRLF);
	ret = tcp_send(nh, AT_NULL, 3, buff);
	if (ret < 0) {
		goto tcp_http_get_abort;
	}
	
	memset(recv_buf, 0x00, RECV_BUF_SIZE);
	http_received = 0;
	
	for (;;) {
		if (gTestAbort) {
			goto tcp_http_get_abort;
		}
		ret = tcp_receive(nh, AT_NULL, RECV_BUF_SIZE, recv_buf);
		if (ret < 0) {
			goto tcp_http_get_abort;
		}
		OSReport("%s\n", recv_buf);
		http_received += ret;
	}
	
tcp_http_get_abort:
	ret = tcp_abort(nh);
	OSReport("tcp_abort(%d) ret %d\n", nh, ret);
tcp_http_get_delete:
	ret = tcp_delete(nh);
	OSReport("tcp_delete(%d) ret %d\n", nh, ret);
tcp_http_get_exit:
	OSReport("http_test end\n");
	
	return (NULL);
}
