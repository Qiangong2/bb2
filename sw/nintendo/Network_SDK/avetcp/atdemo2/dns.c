 /*
 * AVE-TCP for Nintendo GameCube Sample Program
 *
 */

#include <stdlib.h>
#include <string.h>
#include <demo.h>

#include "avetcp.h"
#include "atdemo.h"

/* Host name resolution DNS look up */
s16 dns_get_addr_proc(const char* in_name, AT_IP_ADDR* out_addr) {

	static AT_SINT16 dns_handle;
	AT_SINT16 addr_num;
	AT_SINT16 cname_len;
	AT_SINT16 ret;
	
	/* Start DNS look up */
	ret = dns_open_addr((AT_SBYTE*)in_name, (AT_SINT16)strlen(in_name), AT_IPv4);
	if (ret < 0) {
		OSReport("dns_open_addr error %d\n", ret);
		return -1;
	}
	gDns_handle = ret;
	OSReport("dns_open_addr : %s\n", in_name);
	addr_num = 1;
	cname_len = 0;
	/* Get DNS look up information */
	ret = dns_get_addr(gDns_handle, out_addr, &addr_num, AT_NULL, &cname_len, AT_NULL);
	if (ret < 0 || addr_num <= 0) {
		dns_close(gDns_handle);
		OSReport("dns_get_addr error %d\n", ret);
		return -1;
	}
	dns_close(gDns_handle);
	OSReport("dns_get_addr : %08x\n", AT_NTOHL(out_addr->v4addr));
	
	return 1;
}
