/*
 * AVE-TCP for Nintendo GameCube Sample Program
 *
 */

#include <stdlib.h>
#include <string.h>
#include <demo.h>

#include "avetcp.h"
#include "atdemo.h"

#define DISCARD_PORT 9

/*
 * TCP DISCARD TEST
 *
 * Receives from the discard port of the host specified by TEST_SERVER_NAME.
 */
s16 tcp_discard_send_test(void)
{
	AT_SINT16 ret;
	AT_SINT16 nh;
	AT_SINT16 stat;
	AT_IP_ADDR addr;
	AT_SEND_BUFFS buff;
	u32 s_frame;
	u32 d_frame;

	ret = dns_get_addr_proc(TEST_SERVER_NAME, &addr);
	if (ret < 0) {
		goto tcp_discard_send_exit;
	}
	
	ret = tcp_create();
	if (ret < 0) {
		goto tcp_discard_send_exit;
	}
	nh = ret;
	gTest_nh = ret;

	ret = tcp_connect(nh, &addr, AT_HTONS(DISCARD_PORT), AT_NULL);
	if (ret < 0) {
		goto tcp_discard_send_delete;
	}
	for (;;) {
		if (gTestAbort) {
			goto tcp_discard_send_abort;
		}
		ret = tcp_stat(nh, &stat, AT_NULL, AT_NULL, AT_NULL);
		if (ret < 0 || stat == AT_STAT_FATAL) {
			goto tcp_discard_send_abort;
		}
		if (stat == AT_STAT_ESTABLISHED) {
			break;
		}
		VIWaitForRetrace();
	}
	
	discard_sent = 0;
	s_frame = gFrame;
	
	for (;;) {
		if (gTestAbort) {
			goto tcp_discard_send_abort;
		}
		buff.buff = send_buf;
		buff.len = SEND_BUF_SIZE;
		ret = tcp_send(nh, AT_NULL, 1, &buff);
		if (ret < 0) {
			OSReport("tcp_send(%d) ret %d\n", nh, ret);
			break;
		}
		discard_sent += SEND_BUF_SIZE;
	}
	
	d_frame = gFrame - s_frame;
	gSec = d_frame / 60;
	gThru = discard_sent / gSec;

tcp_discard_send_abort:
	ret = tcp_abort(nh);
	OSReport("tcp_abort(%d) ret %d\n", nh, ret);
tcp_discard_send_delete:
	ret = tcp_delete(nh);
	OSReport("tcp_delete(%d) ret %d\n", nh, ret);
tcp_discard_send_exit:
	OSReport("tcp_discard_send_test end\n");

	return (NULL);
}
