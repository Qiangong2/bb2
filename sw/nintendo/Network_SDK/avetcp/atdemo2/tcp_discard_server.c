/*
 * AVE-TCP for Nintendo GameCube Sample Program
 *
 */

#include <stdlib.h>
#include <string.h>
#include <demo.h>

#include "avetcp.h"
#include "atdemo.h"

#define DISCARD_PORT 9

/*
 * TCP DISCARD SERVER
 *
 * Open DISCARD port and discard receive data after reading it.
 */
s16 tcp_discard_server_test(void)
{
	AT_SINT16 ret;
	AT_IP_ADDR addr;
	AT_SINT16 stat;
	
	discard_received = 0;
	ret = tcp_create();
	if (ret < 0) {
		OSReport("tcp_create ret %d\n", ret);
		goto tcp_discard_server_exit;
	}
	gListen_nh = ret;

	addr.type = gIfinfo.type;
	addr.v4addr = gIfinfo.v4address;
	ret = tcp_bind(gListen_nh, &addr, AT_HTONS(DISCARD_PORT));
	if (ret < 0) {
		OSReport("tcp_bind ret %d\n", ret);
		goto tcp_discard_server_listen_delete;
	}

	ret = tcp_listen(gListen_nh, AT_NULL, 0, 1, AT_NULL, AT_NULL);
	if (ret < 0) {
		OSReport("tcp_listen ret %d\n", ret);
		goto tcp_discard_server_listen_delete;
	}

	ret = tcp_accept(gListen_nh, AT_NULL, AT_NULL);
	if (ret < 0) {
		OSReport("tcp_accept ret %d\n", ret);
		goto tcp_discard_server_listen_delete;
	}
	gAccept_nh = ret;
	for (;;) {
		if (gTestAbort) {
			goto tcp_discard_server_abort;
		}
		ret = tcp_stat(gAccept_nh, &stat, AT_NULL, AT_NULL, AT_NULL);
		if (ret < 0 || stat == AT_STAT_FATAL) {
			goto tcp_discard_server_abort;
		}
		if (stat == AT_STAT_ESTABLISHED) {
			break;
		}
		VIWaitForRetrace();
	}

	for (;;) {
		if (gTestAbort) {
			goto tcp_discard_server_abort;
		}
		ret = tcp_receive(gAccept_nh, AT_NULL, RECV_BUF_SIZE, recv_buf);
		if (ret < 0) {
			OSReport("tcp_receive(%d) ret %d\n", gAccept_nh, ret);
			break;
		}
		discard_received += ret;
	}

tcp_discard_server_abort:
	ret = tcp_abort(gAccept_nh);
	OSReport("tcp_abort(%d) ret %d\n", gAccept_nh, ret);
	ret = tcp_delete(gAccept_nh);
	OSReport("tcp_delete(%d) ret %d\n", gAccept_nh, ret);
tcp_discard_server_listen_delete:
	ret = tcp_delete(gListen_nh);
	OSReport("tcp_delete(%d) ret %d\n", gListen_nh, ret);
tcp_discard_server_exit:
	OSReport("tcp_discard_server_test end\n");
	gListen_nh = -1;
	gAccept_nh = -1;

	return (NULL);
}
