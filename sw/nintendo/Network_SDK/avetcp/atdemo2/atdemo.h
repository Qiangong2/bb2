/*
 * AVE-TCP for Nintendo GameCube Sample Program
 *
 */

/*************************************************/
/* Set test environment */

/* LAN fixed IP */
#define MY_IPADDR 0xc0a80120
#define MY_NETMASK 0xffffff00
#define MY_GATEWAY 0xc0a80101
#define MY_DNS1 0xc0a80101
#define MY_DNS2 0x0

/* DHCP */
#define DHCP_HOSTNAME "avetcpgc"

/* PPP */
#define LOGIN_NAME "guest@flets"
#define LOGIN_PASSWD "guest"
#define TELEPHONE_NUM "012345678"

/* Test server host name*/
#define TEST_SERVER_NAME "www.yahoo.co.jp"

/* Test contents */
#define HTTP_GET_TEST 1 /* Get top page from HTTP server */
#define TCP_DISCARD_TEST 2 /* Send to TCP DISCARD port */
#define TCP_DISCARD_SERVER 3 /* TCP DISCARD server*/

#define TEST_TYPE HTTP_GET_TEST /* <== Select this */

/*************************************************/

#define DEV_UNKNOWN 0
#define DEV_UNKNOWN_STR "unknown"
#define DEV_BBA 1
#define DEV_BBA_STR "BBA"
#define DEV_MODEM 2
#define DEV_MODEM_STR "MODEM"
extern s16 gDeviceType;

#define IP_SETTING_MANUAL 0
#define IP_SETTING_DHCP 1
#define IP_SETTING_PPPOE 2

extern AT_IF_INFO gIfinfo;
extern AT_SINT16 gDns_handle;
extern AT_SINT16 gTest_nh;
extern AT_SINT16 gListen_nh;
extern AT_SINT16 gAccept_nh;
extern s32 gTestAbort;
extern s32 gMakeOffline;
extern u32 gSec;
extern u32 gThru;
extern u32 gFrame;

#define RECV_BUF_SIZE 32767
extern AT_UBYTE recv_buf[];
extern AT_UINT32 http_received;
extern AT_UINT32 discard_received;

#define SEND_BUF_SIZE 32767
extern AT_UBYTE send_buf[];
extern AT_UINT32 discard_sent;

extern s16 http_test(void);
extern s16 tcp_discard_send_test(void);
extern s16 tcp_discard_server_test(void);
extern s16 dns_get_addr_proc(const char* in_name, AT_IP_ADDR* out_addr);
