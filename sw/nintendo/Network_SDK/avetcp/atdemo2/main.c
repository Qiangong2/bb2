/*
 * AVE-TCP for Nintendo GameCube Sample Program
 *
 */

#include <stdlib.h>
#include <string.h>
#include <demo.h>

#include "avetcp.h"
#include "atdemo.h"

s16 gDeviceType = DEV_UNKNOWN;
s16 gIpSetting = -1;
AT_IF_INFO gIfinfo;
AT_SINT16 gDns_handle;
AT_SINT16 gTest_nh;
AT_SINT16 gListen_nh;
AT_SINT16 gAccept_nh;
s32 gTestAbort = 0;
s32 gMakeOffline = 0;
u32 gSec;
u32 gThru;
u32 gFrame;

AT_UBYTE recv_buf[RECV_BUF_SIZE];
AT_UINT32 http_received;
AT_UINT32 discard_received;

AT_UBYTE send_buf[SEND_BUF_SIZE];
AT_UINT32 discard_sent;

static AT_PPP_MODEM_PROPERTY pmprop;
static AT_PPP_START_ARG psarg;
static AT_PPP_STAT_ARG pstat;
static s32 online = 0;
static u16 button;
static s16 x, y;
static PADStatus Pads[PAD_MAX_CONTROLLERS];
extern const char * const AveTcpGcBuild;

/* Parameter for thread for setting network */
static OSThread netconf_thread;
#define STACKSIZE 4096
static AT_UBYTE netconf_stack[STACKSIZE];

/* Parameter for thread for communication test */
static OSThread nettest_thread;
#define STACKSIZE 4096
static AT_UBYTE nettest_stack[STACKSIZE];

static void* network_config_thread(void* arg);
static void* network_test_thread(void* arg);
static void set_pmprop(AT_PPP_MODEM_PROPERTY* pmprop);
static void set_psarg(AT_PPP_START_ARG* psarg, s32 pppoe);
static s16 selectIpSetting(void);

void main()
{
	s32 chan;
	u32 padBit;
	u32 resetBits;
	u32 connectedBits;
	GXRenderModeObj* rmp;
	GXColor blue = {0, 0, 127, 0};
	s32 result;
	u32 type;
	u16 curbtn = 0;
	u16 oldbtn = 0;
	u8* device_type_str;
	int phase = 0;
	enum {
		phase_1 = 0,
		phase_2,
		phase_3,
		phase_4
	};
		
	button = 0;
	x = 0;
	y = 0;
	gFrame = 0;
	
	DEMOInit(NULL);
	
	OSReport ( "Welcome to the GAMECUBE C Stationery\n");
	OSReport ( "%s\n", AveTcpGcBuild);
	
	connectedBits = 0;
	GXSetCopyClear(blue, 0x00ffffff);
	GXCopyDisp(DEMOGetCurrentBuffer(), GX_TRUE);
	rmp = DEMOGetRenderModeObj();
	DEMOInitCaption(DM_FT_XLU, (s16)rmp->fbWidth / 2, (s16)rmp->efbHeight / 2);
	
	/* Decide device type */
	result = EXIGetType(0, 2, &type);
	OSReport("EXIGetType: result %d type %08x\n", result, type);
	switch (type) {
		case EXI_ETHER:
			gDeviceType = DEV_BBA;
			device_type_str = (u8*)DEV_BBA_STR;
			break;
		case EXI_MODEM:
			gDeviceType = DEV_MODEM;
			device_type_str = (u8*)DEV_MODEM_STR;
			break;
		default:
			gDeviceType = DEV_UNKNOWN;
			device_type_str = (u8*)DEV_UNKNOWN_STR;
			break;
	}
	
	for (;;) {
		DEMOBeforeRender();
		x = 10;
		y = 0;
		gFrame++;
		
		PADRead(Pads);
		resetBits = 0;
		for (chan = 0; chan < PAD_MAX_CONTROLLERS; ++chan) {
			padBit = PAD_CHAN0_BIT >> chan;
			switch (Pads[chan].err) {
				case PAD_ERR_NONE:
				case PAD_ERR_TRANSFER:
					connectedBits |= padBit;
					break;
				case PAD_ERR_NO_CONTROLLER:
					resetBits |= padBit;
					break;
				case PAD_ERR_NOT_READY:
				default:
					break;
			}
		}
		if (connectedBits) {
			resetBits &= connectedBits;
		}
		if (resetBits) {
			PADReset(resetBits);
		}
		curbtn = Pads[0].button;
		button = (u16)((curbtn ^ oldbtn) & curbtn);
		oldbtn = curbtn;

		DEMOPrintf(x, y += 16, 0, "AVE-TCP for GAMECUBE DEMO %d", gFrame);
		DEMOPrintf(x, y += 16, 0, "DEVICE [%s]", device_type_str);
		
		switch (phase) {
		case phase_1:
			switch (gDeviceType) {
			case DEV_BBA:
				gIpSetting = selectIpSetting();
				if (gIpSetting < 0) {
					break;
				}
			case DEV_MODEM:
				phase = phase_2;
				break;
			case DEV_UNKNOWN:
				break;
			}
			break;
		case phase_2:
			DEMOPrintf(x, y += 16, 0, "---OFFLINE---");
			DEMOPrintf(x, y += 16, 0, "PUSH A Button ==> Test Start");
			if (button & PAD_BUTTON_A) {
				/* Start thread for setting network */
				OSCreateThread(&netconf_thread, network_config_thread, AT_NULL, netconf_stack + STACKSIZE, STACKSIZE, 16, OS_THREAD_ATTR_DETACH);
				OSResumeThread(&netconf_thread);
				phase = phase_3;
			}
			break;
		case phase_3:
			if (online) {
				DEMOPrintf(x, y += 16, 0, "---ONLINE---");
				switch(TEST_TYPE) {
				case HTTP_GET_TEST:
					DEMOPrintf(x, y += 16, 0, "HTTP Get Test");
					DEMOPrintf(x, y += 16, 0, "Test Server : %s", TEST_SERVER_NAME);
					DEMOPrintf(x, y += 16, 0, "receive %u bytes", http_received);
					DEMOPrintf(x, y += 16, 0, "PUSH A Button ==> offline");
					if (!gTestAbort)
						DEMOPrintf(x, y += 16, 0, "PUSH B Button ==> tcp_cancel");
					break;
				case TCP_DISCARD_TEST:
					DEMOPrintf(x, y += 16, 0, "Discard Send Test");
					DEMOPrintf(x, y += 16, 0, "Test Server : %s", TEST_SERVER_NAME);
					DEMOPrintf(x, y += 16, 0, "send %u bytes", discard_sent);
					DEMOPrintf(x, y += 16, 0, "PUSH A Button ==> offline");
					if (!gTestAbort)
						DEMOPrintf(x, y += 16, 0, "PUSH B Button ==> tcp_cancel");
					else
						DEMOPrintf(x, y += 16, 0, "%u sec %u bytes/sec", gSec, gThru);
					break;
				case TCP_DISCARD_SERVER:
					DEMOPrintf(x, y += 16, 0, "Discard Server Test");
					DEMOPrintf(x, y += 16, 0, "My IP Address : %0x", gIfinfo.v4address);
					DEMOPrintf(x, y += 16, 0, "receive %u bytes", discard_received);
					DEMOPrintf(x, y += 16, 0, "PUSH A Button ==> offline");
					if (!gTestAbort)
						DEMOPrintf(x, y += 16, 0, "PUSH B Button ==> tcp_cancel");
					break;
				}
				if (button & PAD_BUTTON_A) {
					if (gTestAbort == 0) {
						gTestAbort = 1;
					}
					gMakeOffline = 1;
					phase = phase_4;
				}
				if (button & PAD_BUTTON_B && gTestAbort == 0) {
					gTestAbort = 1;
				}
				break;
			} else {
				DEMOPrintf(x, y += 16, 0, "---OFFLINE---");
				if (OSIsThreadTerminated(&netconf_thread)) {
					phase = phase_4;
				}
			}
			break;
		case phase_4:
			if (online) {
				DEMOPrintf(x, y += 16, 0, "---ONLINE---");
			} else {
				gMakeOffline = 0;
				gTestAbort = 0;
				phase = phase_1;
			}
			break;
		}

        DEMODoneRender();
	}
	
}

/* Thread for setting network */
static void* network_config_thread(void* arg) {
#pragma unused(arg)
	AT_SINT16 ret;
	static AT_UBYTE* workarea;
	static AT_SINT16 if_num = -1;
	static AT_SINT16 if_num_ppp = -1;
	static AT_SINT16 if_num_dix = -1;
	static AT_NINT32 gateway = 0;
	static AT_IP_ADDR dns1;
	static AT_IP_ADDR dns2;
	
#ifdef AT_GC_DEBUG
	AT_SetDebugFlag(AT_DPLEVEL_PPP | AT_DPLEVEL_PPPOE);
#endif /* AT_GC_DEBUG */
	ret = (AT_SINT16)AT_SetTaskPriority(16);
	
	/* Allocate TCP work area */
	{
		AT_INIT_PARAM para;
		AT_SINT32 size;
			
		size = AT_GetInitParam(&para); /* Get default parameter */
			
		para.tcp_port_num = 4; /* Overwrite parameter you wish to change */
				
		size = AT_SetInitParam(&para); /* Reset parameter, get necessary area size */
		if (size < 0) {
			goto network_config_exit;
		}
		
		workarea = (AT_UBYTE*)OSAlloc((u32)size); 
		if (workarea) {
			ret = AT_SetWorkArea(workarea, size); /* Give secured area to TCPLib */
			if (ret < 0) {
				goto network_config_exit;
			}
		} else {
			goto network_config_exit;
		}
	}
	/* Initialize AVE-TCP module */
	ret = avetcp_init();
	OSReport("avetcp_init ret %d\n", ret);
	if (ret < 0) {
		goto network_config_exit;
	}
	
	/* Initialize DNS */
	ret = dns_init();
	OSReport("dns_init ret %d\n", ret);
	if (ret < 0) {
		goto avetcp_terminate;
	}
	
	/* Initialize PPP */
	ret = ppp_init();
	OSReport("ppp_init ret %d\n", ret);
	if (ret < 0) {
		goto dns_terminate;
	}
	if_num_ppp = ret;
	
	if (gDeviceType == DEV_BBA) {
		/* Initialize ARP */
		ret = arp_init();
		OSReport("arp_init ret %d\n", ret);
		if (ret < 0) {
			goto ppp_terminate;
		}
		/* Initialize DIX */
		ret = dix_init();
		OSReport("dix_init ret %d\n", ret);
		if (ret < 0) {
			ret = arp_term();
			OSReport("arp_term ret %d\n", ret);
			goto ppp_terminate;
		}
		if_num_dix = ret;
	}
	
	if (gDeviceType == DEV_MODEM ||
		(gDeviceType == DEV_BBA && gIpSetting == IP_SETTING_PPPOE)) {
		/* ISP verification connection process by PPP and PPPoE */
		s16 pppoe;
		if_num = if_num_ppp;
		set_pmprop(&pmprop);
		ppp_set_modem_property(&pmprop);
		pppoe = (s16)((gDeviceType == DEV_BBA) ? 1 : 0);
		set_psarg(&psarg, pppoe);
		ret = ppp_start(&psarg);
		OSReport("ppp_start ret %d\n", ret);
		if (ret < 0) {
			goto dl_terminate;
		}
		ret = ppp_stat(&pstat);
		OSReport("ppp_stat ret %d\n", ret);
		if (pstat.state == AT_PPP_STAT_FAIL) {
			goto dl_terminate;	
		} else if (pstat.state == AT_PPP_STAT_ESTABLISHED) {
			gIfinfo.type = AT_IPv4;
			gIfinfo.v4address = pstat.ipcp.local_ip;
			gIfinfo.v4netmask = AT_HTONL(0);
			gIfinfo.v4brc = AT_HTONL(0);
			gateway = 0;
			dns1.type = AT_IPv4;
			dns1.v4addr = pstat.ipcp.local_dns1;
			dns2.type = AT_IPv4;
			dns2.v4addr = pstat.ipcp.local_dns2;
		}
	} else if (gIpSetting == IP_SETTING_DHCP) {
		/* Get interface information dynamically by DHCP */
		if_num = if_num_dix;
		/* Initialize DHCP module */
		ret = (AT_SINT16)DHCP_init(if_num);
		OSReport("DHCP_init ret %d\n", ret);
		if (ret < 0) {
			goto dix_terminate;
		}
		/* Set host name */
		ret = (AT_SINT16)DHCP_hostname((AT_UBYTE*)DHCP_HOSTNAME);
		OSReport("DHCP_hostname ret %d\n", ret);
		if (ret < 0) {
			DHCP_terminate();
			goto dix_terminate;
		}
		gIfinfo.type = AT_IPv4;
		/* Get interface information by DHCP */
		ret = (AT_SINT16)DHCP_request(0, 0, 0, AT_NULL, &gIfinfo.v4address, &gIfinfo.v4netmask, &gIfinfo.v4brc, 30);
		OSReport("DHCP_request ret %d\n", ret);
		if (ret != DHCP_SUCCESS) {
			DHCP_terminate();
			goto dix_terminate;
		}
		/* Set default gateway address gotten by DHCP */
		ret = (AT_SINT16)DHCP_get_gateway(AT_NULL, &gateway);
		OSReport("DHCP_get_gateway ret %d\n", ret);
		/* Set DNS address gotten by DHCP */
		dns1.type = AT_IPv4;
		dns2.type = AT_IPv4;
		ret = (AT_SINT16)DHCP_get_dns(AT_NULL, AT_NULL, &dns1.v4addr, &dns2.v4addr);
		OSReport("DHCP_get_dns ret %d\n", ret);
	} else if (gIpSetting == IP_SETTING_MANUAL) {
		/* Set interface by fixed value that does not use DHCP */
		if_num = if_num_dix;
		gIfinfo.type = AT_IPv4;
		/* Set IP address */
		gIfinfo.v4address = AT_HTONL(MY_IPADDR);
		/* Set net mask */
		gIfinfo.v4netmask = AT_HTONL(MY_NETMASK);
		/* Set broadcast address */
		gIfinfo.v4brc = AT_HTONL(0);
		/* Set default gateway */
		gateway = AT_HTONL(MY_GATEWAY);
		/* Set DNS address (primary, secondary) */
		dns1.type = AT_IPv4;
		dns1.v4addr = AT_HTONL(MY_DNS1);
		dns2.type = AT_IPv4;
		dns2.v4addr = AT_HTONL(MY_DNS2);

		/* Send a gratuitous ARP */
		ret = arp_probe(if_num, gIfinfo.v4address, 5000, AT_NULL);
		if (ret == 1) {
			OSReport("The ARP reply is received from %08x.\n", MY_IPADDR);
			goto dix_terminate;
		}
		if (ret == AT_API_ERR_DEV) {
			OSReport("Link down.\n");
			goto dix_terminate;
		}
		if (ret < 0) {
			OSReport("arp_probe ret %d\n", ret);
			goto dix_terminate;
		}
	} else {
		/* Other device */
		goto dl_terminate;
	}
	
	OSReport("ip addr: %08x\n", gIfinfo.v4address);
	OSReport("gateway: %08x\n", gateway);
	OSReport("dns 1  : %08x\n", dns1.v4addr);
	OSReport("dns 2  : %08x\n", dns2.v4addr);
	/* Start interface ifconfig */
	ret = if_config(if_num, &gIfinfo);
	OSReport("if_config ret %d\n", ret);
	if (ret < 0) {
		goto dl_terminate;
	}
	/* Set default gateway address */
	if (gateway) {
		ret = route4_add(0, 0, gateway);
		OSReport("route4_add ret %d\n", ret);
	}
	/* Set DNS server address */
	dns_clear_server();
	if(dns1.v4addr)
		dns_set_server(&dns1);
	if(dns2.v4addr)
		dns_set_server(&dns2);
	
	online = 1;
	
	/* Start communication test thread */
	OSCreateThread(&nettest_thread, network_test_thread, AT_NULL, nettest_stack + STACKSIZE, STACKSIZE, 16, OS_THREAD_ATTR_DETACH);
	OSResumeThread(&nettest_thread);
	
	for (;;) {
		/* Process to monitor condition of BBA circuit */
		if (gDeviceType == DEV_BBA) {
			/* Monitor link status */
			if (AT_LinkUpStatus() == 0) {
				gTestAbort = 1;
				gMakeOffline = 1;
			}
			/* DHCP lease extension process */
			if (gIpSetting == IP_SETTING_DHCP) {
				ret = (AT_SINT16)DHCP_timer();
				if (ret == DHCP_STATE_TIMEOUT || ret == DHCP_STATE_ERROR) {
					gTestAbort = 1;
					gMakeOffline = 1;
				}
			}
		}
		/* Process to monitor condition of PPP circuit */
		if (gDeviceType == DEV_MODEM ||
			(gDeviceType == DEV_BBA && gIpSetting == IP_SETTING_PPPOE)) {
			ret = ppp_stat(&pstat);
			/* Interrupt process of PPP */
			if (pstat.state != AT_PPP_STAT_ESTABLISHED) {
				gTestAbort = 1;
				gMakeOffline = 1;
			}
		}

		if (gTestAbort == 1) {
			/* Interrupt calling DNS blocking */
			ret = dns_close(gDns_handle);
			OSReport("dns_close(%d) ret %d\n", gDns_handle, ret);
			/* Interrupt calling TCP blocking */
			if (TEST_TYPE == HTTP_GET_TEST || TEST_TYPE == TCP_DISCARD_TEST) {
				ret = tcp_cancel(gTest_nh, 3); /* Interrupt tcp_send and tcp_receive */
				OSReport("tcp_cancel(%d, SEND | RECEIVE) ret %d\n", gTest_nh, ret);
			} else {
				if (gListen_nh >= 0) {
					/* Interrupt tcp_accept */
					ret = tcp_cancel(gListen_nh, 4);
					OSReport("tcp_cancel(%d, ACCEPT) ret %d\n", gListen_nh, ret);
				}
				if (gAccept_nh >= 0) {
					/* Interrupt tcp_receive */
					ret = tcp_cancel(gAccept_nh, 2);
					OSReport("tcp_cancel(%d, RECEIVE) ret %d\n", gAccept_nh, ret);
				}
			}
			gTestAbort = 2;
		}

		if (gMakeOffline) {
			if (OSIsThreadTerminated(&nettest_thread)) {
				goto interface_down;
			}
		}

		VIWaitForRetrace();
	}

interface_down:
	if (gateway) {
		ret = route4_del(0, 0, gateway);
		OSReport("route4_del ret %d\n", ret);
	}
	ret = if_down(if_num, AT_IPany);
	OSReport("if_down ret %d\n", ret);
dl_terminate:
	if (gDeviceType == DEV_MODEM ||
		(gDeviceType == DEV_BBA && gIpSetting == IP_SETTING_PPPOE)) {
		AT_PPP_STOP_ARG stoparg;
		stoparg.wait = 1;	/* Wait for disconnection to complete */
		ret = ppp_stop(&stoparg);
		OSReport("ppp_stop ret %d\n", ret);
	} else if (gIpSetting == IP_SETTING_DHCP) {
		ret = (AT_SINT16)DHCP_release(0, gIfinfo.v4address, 5);
		OSReport("DHCP_release ret %d\n", ret);
		DHCP_terminate();
	}
dix_terminate:
	if (gDeviceType == DEV_BBA) {
		ret = dix_term();
		OSReport("dix_term ret %d\n", ret);
		ret = arp_term();
		OSReport("arp_term ret %d\n", ret);
	}
ppp_terminate:
	ret = ppp_term();
	OSReport("ppp_term ret %d\n", ret);
dns_terminate:
	ret = dns_term();
	OSReport("dns_term ret %d\n", ret);
avetcp_terminate:
	ret = avetcp_term();
	OSReport("avetcp_term ret %d\n", ret);
	OSFree(workarea);
network_config_exit:
	OSReport("network_config_thread end\n");
	online = 0;
	
	return (NULL);
}

/* Communication test thread */
static void* network_test_thread(void* arg) {
#pragma unused(arg)

	switch(TEST_TYPE) {
	case HTTP_GET_TEST:
		http_test();
		break;
	case TCP_DISCARD_TEST:
		tcp_discard_send_test();
		break;
	case TCP_DISCARD_SERVER:
		tcp_discard_server_test();
	}
	gTestAbort = 3;
	return (NULL);
}

static void set_pmprop(AT_PPP_MODEM_PROPERTY* pmprop)
{

	pmprop->needToInit			= 1;
	pmprop->needToDial			= 1;
	pmprop->needToCd			= 1;
	pmprop->needToDisc			= 0;
	pmprop->countryCode			= AT_NULL;
	pmprop->connectMode			= AT_GC_MDM_CN_DEFAULT;
	pmprop->errorCorrectMode	= AT_GC_MDM_EC_DEFAULT;
	pmprop->compressMode		= AT_GC_MDM_CM_DEFAULT;
	pmprop->atcommand_count		= 0;
	pmprop->atcommand			= AT_NULL;
}

static void set_psarg(AT_PPP_START_ARG* psarg, s32 pppoe)
{

	psarg->mode					= PP_MODE_DIALOUT;
	if (pppoe)
		psarg->mode				|= PP_MODE_PPPOE;
	psarg->iptype				= AT_IPv4;
	psarg->wait					= 1;			/* Blocking call */
	psarg->login				= (unsigned char *)LOGIN_NAME;
	psarg->password				= (unsigned char *)LOGIN_PASSWD;
	psarg->tele_number			= (unsigned char *)TELEPHONE_NUM;
	psarg->notify				= AT_NULL;		/* No Notify function */
	psarg->tele.dialtype		= 0;			/* Tone:0, pulse:1 */
	psarg->tele.outside_line	= 0;
	psarg->tele.outside_number	= AT_NULL;
	psarg->tele.timeout			= 180;
	psarg->conn.recognize		= 4;			/* Set authentication (depending upon other party) */
	psarg->conn.mru				= 1500;
	psarg->conn.acfcomp			= 0;
	psarg->conn.protocomp		= 0;
	psarg->conn.vjcomp			= 0;
	psarg->ipcp.local_ip		= 0;
	psarg->ipcp.remote_ip		= 0;
	psarg->ipcp.local_dns1		= 0;
	psarg->ipcp.local_dns2		= 0;
	psarg->ipcp.remote_dns1		= 0;
	psarg->ipcp.remote_dns2		= 0;
}

static s16 selectIpSetting(void)
{
	static s16 sel = 0;

	if (button & PAD_BUTTON_DOWN) {
		sel++;
		if (sel > 2) sel = 0;
	}
	if (button & PAD_BUTTON_UP) {
		sel--;
		if (sel < 0) sel = 2;
	}
	DEMOPrintf(x, y += 16, 0, "%c IP Manual Setting (IP: %08x)", (sel == 0) ? '*' : ' ', MY_IPADDR);
	DEMOPrintf(x, y += 16, 0, "%c DHCP", (sel == 1) ? '*' : ' ');
	DEMOPrintf(x, y += 16, 0, "%c PPPoE (ID: %s)", (sel == 2) ? '*' : ' ', LOGIN_NAME);
				
	if (button & PAD_BUTTON_A) {
		return (sel);
	} else {
		return (-1);
	}
}
