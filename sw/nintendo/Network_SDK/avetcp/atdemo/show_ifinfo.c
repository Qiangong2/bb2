/*
 * AVE-TCP for Nintendo GameCube Sample Program
 *
 */

#include <stdlib.h>
#include <string.h>
#include <demo.h>

#include "avetcp.h"
#include "atdemo.h"

s16 show_ifinfo(s32 abort_flag)
{
	s16 x1;
	AT_UINT32 leasetime;
	AT_UINT32 t1;
	AT_UINT32 t2;
	AT_UINT32 start;
	AT_UINT32 end;
	AT_UINT32 remain;
	AT_UINT32 idletime;

	if (gIpSetting == IP_SETTING_DHCP) {
		DHCP_get_leasetime(&leasetime, &t1, &t2);
		DHCP_get_leasetime2(&start, &end, &remain);
		DEMOPrintf(x, y += 16, 0, "DHCP leasetime:");
		DEMOPrintf(x, y += 16, 0, "  lease  %d t1 %d t2 %d", leasetime, t1, t2);
		DEMOPrintf(x, y += 16, 0, "  start  %s", tm2timestr(wave_gmtime(start)));
		DEMOPrintf(x, y += 16, 0, "  end    %s", tm2timestr(wave_gmtime(end)));
		DEMOPrintf(x, y += 16, 0, "  remain %d", remain);
	} else 
	if (gDeviceType == DEV_MODEM || gIpSetting == IP_SETTING_PPPOE) {
		idletime = ppp_idle_time();
		DEMOPrintf(x, y += 16, 0, "PPP info:");
		DEMOPrintf(x, y += 16, 0, "  local address : %s", nint2ipdot(gPstat.ipcp.local_ip));
		DEMOPrintf(x, y += 16, 0, "  remote address: %s", nint2ipdot(gPstat.ipcp.remote_ip));
		DEMOPrintf(x, y += 16, 0, "  idle time %d", idletime);
	}

	y += 16;
	DEMOPrintf(x, y += 16, 0, "IP Configuration:");
	x1 = (s16)(x + 16);
	y += 16;
	DEMOPrintf(x1,           y, 0, "address");
	DEMOPrintf(x1 += 17 * 8, y, 0, "netmask");
	DEMOPrintf(x1 += 17 * 8, y, 0, "broadcast");
	x1 = (s16)(x + 16);
	y += 16;
	DEMOPrintf(x1,           y, 0, "%s ", nint2ipdot(gIfinfo.v4address));
	DEMOPrintf(x1 += 17 * 8, y, 0, "%s ", nint2ipdot(gIfinfo.v4netmask));
	DEMOPrintf(x1 += 17 * 8, y, 0, "%s ", nint2ipdot(gIfinfo.v4brc));
	x1 = (s16)(x + 16);
	y += 16;
	DEMOPrintf(x1,           y, 0, "default gateway");
	DEMOPrintf(x1 += 17 * 8, y, 0, "dns server1");
	DEMOPrintf(x1 += 17 * 8, y, 0, "dns server2");
	x1 = (s16)(x + 16);
	y += 16;
	DEMOPrintf(x1,           y, 0, "%s ", nint2ipdot(gGateway));
	DEMOPrintf(x1 += 17 * 8, y, 0, "%s ", nint2ipdot(gDns1.v4addr));
	DEMOPrintf(x1 += 17 * 8, y, 0, "%s ", nint2ipdot(gDns2.v4addr));

	if (gButton & PAD_BUTTON_A) {
		return 1;
	}
	if (abort_flag) {
		return 1;
	}
	return 0;
}

