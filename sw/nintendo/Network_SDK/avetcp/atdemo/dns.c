/*
 * AVE-TCP for Nintendo GameCube Sample Program
 *
 */

#include <stdlib.h>
#include <string.h>
#include <demo.h>

#include "avetcp.h"
#include "atdemo.h"

s16 dns_get_name_test(s32 abort_flag)
{
	static s32 wait = 0;
	static s32 phase = 0;
	static AT_SINT16 nh;
	static AT_SINT16 dns_handle;
	static AT_IP_ADDR query;
	static AT_SBYTE rname[256];
	static s32 err = 0;
	static AT_SINT16 err_val;
	AT_SINT16 ret;
	s16 flag;
	enum {
		phase_dns = 0,
		phase_result
	};

	DEMOPrintf(x, y += 16, 0, "dns_get_name %s", nint2ipdot(AT_HTONL(QUERY_ADDRESS)));

	flag = 0;
	if (abort_flag) {
		switch (phase) {
		case phase_dns:
			ret = dns_get_name_proc(AT_NULL, AT_NULL, 1);
			break;
		case phase_result:
			break;
		}
		flag = 1;
	} else {
		switch (phase) {
		case phase_dns:
			query.type = AT_IPv4;
			query.v4addr = QUERY_ADDRESS;
			ret = dns_get_name_proc(&query, rname, 0);
			if (ret == 0) {
				break;
			} else {
				if (ret < 0) {
					err = ret;
				}
				phase = phase_result;
			}
			break;

		case phase_result:
			if (err) {
				DEMOPrintf(x, y += 16, 0, "  error %d", err);
			} else {
				DEMOPrintf(x, y += 16, 0, "  ==> %s", rname);
			}

			if (gButton & PAD_BUTTON_A) {
				flag = 1;
			}
			break;
		}
	}

	if (flag == 1) {
		err = 0;
		phase = 0;
	}
	return (flag);
}

s16 dns_get_addr_proc(const char* in_name, AT_IP_ADDR* out_addr, s32 abort_flag)
{
	static s32 wait = 0;
	static s32 phase = 0;
	static AT_SINT16 dns_handle;
	AT_SINT16 addr_num;
	AT_SINT16 cname_len;
	AT_SINT16 ret;
	s16 flag;
	enum {
		phase_dns_request = 0,
		phase_dns_lookup
	};
	
	flag = 0;
	if (abort_flag) {
		switch (phase) {
		case phase_dns_request:
			break;
		case phase_dns_lookup:
			dns_close(dns_handle);
			break;
		}
		flag = -1;
	} else {
		switch (phase) {
		case phase_dns_request:
			ret = dns_open_addr((AT_SBYTE*)in_name, (AT_SINT16)strlen(in_name), AT_IPv4);
			if (ret < 0) {
				flag = -1;
				break;
			}
			dns_handle = ret;
			phase = phase_dns_lookup;
			break;

		case phase_dns_lookup:
			DEMOPrintf(x, y += 16, 0, "waiting DNS response");	
			if (wait == 0) {
				addr_num = 1;
				cname_len = 0;
				ret = dns_get_addr(dns_handle, out_addr, &addr_num, AT_NULL, &cname_len, AT_DNS_ADDR_NOTIFY_POLLING);
				if (ret == AT_API_PENDING) {
					wait = 6;
					break;
				}
				if (ret < 0 || addr_num <= 0) {
					dns_close(dns_handle);
					flag = -1;
					break;
				}
				dns_close(dns_handle);
				flag = 1;
			} else {
				wait--;
			}
			break;
		}
	}
	if (flag) {
		phase = 0;
		wait = 0;
	}
	return (flag);
}

s16 dns_get_name_proc(AT_IP_ADDR* in_addr, AT_SBYTE* out_name, s32 abort_flag)
{
	static s32 wait = 0;
	static s32 phase = 0;
	static AT_SINT16 dns_handle;
	AT_SINT16 rname_len;
	AT_SINT16 ret;
	s16 flag;
	enum {
		phase_dns_request = 0,
		phase_dns_lookup
	};
	
	flag = 0;
	if (abort_flag) {
		switch (phase) {
		case phase_dns_request:
			break;
		case phase_dns_lookup:
			dns_close(dns_handle);
			break;
		}
		flag = -1;
	} else {
		switch (phase) {
		case phase_dns_request:
			ret = dns_open_name(in_addr);
			if (ret < 0) {
				flag = ret;
				break;
			}
			dns_handle = ret;
			phase = phase_dns_lookup;
			break;

		case phase_dns_lookup:
			DEMOPrintf(x, y += 16, 0, "waiting DNS response");	
			if (wait == 0) {
				rname_len = 256;
				ret = dns_get_name(dns_handle, out_name, &rname_len, AT_DNS_NAME_NOTIFY_POLLING);
				if (ret == AT_API_PENDING) {
					wait = 6;
					break;
				}
				if (ret < 0) {
					dns_close(dns_handle);
					flag = ret;
					break;
				}
				dns_close(dns_handle);
				flag = 1;
			} else {
				wait--;
			}
			break;
		}
	}
	if (flag) {
		phase = 0;
		wait = 0;
	}
	return (flag);
}

