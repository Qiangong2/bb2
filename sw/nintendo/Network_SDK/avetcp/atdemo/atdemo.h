/*
 * AVE-TCP for Nintendo GameCube Sample Program
 *
 */

/*************************************************/
/* Set test environment */

/* LAN fixed IP */
#define MY_IPADDR 0xc0a80120
#define MY_NETMASK 0xffffff00
#define MY_GATEWAY 0xc0a80101
#define MY_DNS1 0xc0a80101
#define MY_DNS2 0x0

/* DHCP */
#define DHCP_HOSTNAME "avetcpgc"

/* PPP */
#define LOGIN_NAME "guest@flets"
#define LOGIN_PASSWD "guest"
#define TELEPHONE_NUM "012345678"

/* Test server host name */
#define TEST_SERVER_NAME "www.yahoo.co.jp"

/* Reverse lookup test */
#define QUERY_ADDRESS 0xd40e0d41 /* 212.14.13.65 */

/*************************************************/

#define DEV_UNKNOWN 0
#define DEV_UNKNOWN_STR "unknown"
#define DEV_BBA 1
#define DEV_BBA_STR "BBA"
#define DEV_MODEM 2
#define DEV_MODEM_STR "MODEM"
extern s16 gDeviceType;

#define IP_SETTING_MANUAL 0
#define IP_SETTING_DHCP 1
#define IP_SETTING_PPPOE 2
extern s16 gIpSetting;

extern AT_IF_INFO gIfinfo;
extern AT_UBYTE gMacaddr[];
extern AT_NINT32 gGateway;
extern AT_IP_ADDR gDns1;
extern AT_IP_ADDR gDns2;
extern AT_ROUTE4_LIST gRouteList[];
extern AT_SINT16 gRouteNum;
extern AT_PPP_MODEM_PROPERTY gPmprop;
extern AT_PPP_START_ARG gPsarg;
extern AT_PPP_STAT_ARG gPstat;
extern AT_SINT16 gDhcpStat;
extern u16 gButton;
extern s16 x, y;
extern u32 gFrame;

#define RECV_BUF_SIZE 32767
extern AT_UBYTE recv_buf[];

#define SEND_BUF_SIZE 32767
extern AT_UBYTE send_buf[];

#define TCP_PORT_NUM 6

enum testtype {
	tt_if_down = 0,
	tt_show_ifinfo,
	tt_show_route,
	tt_dns_get_name,
	tt_udp_echo,
	tt_http_get,
	tt_tcp_echo1,
	tt_tcp_echo2,
	tt_tcp_echo3,
	tt_tcp_chargen,
	tt_tcp_discard,
	tt_tcp_discard_server,
	tt_ping,
	tt_test_num
};

struct wave_tm_ {
	int tm_sec;     /* seconds after the minute - [0,59] */
	int tm_min;     /* minutes after the hour - [0,59] */
	int tm_hour;    /* hours since midnight - [0,23] */
	int tm_mday;    /* day of the month - [1,31] */
	int tm_mon;     /* months since January - [0,11] */
	int tm_year;    /* years since 2000 */
	int tm_wday;    /* days since Sunday - [0,6] */
	int tm_yday;    /* days since January 1 - [0,365] */
	int tm_isdst;   /* daylight savings time flag */
};
typedef struct wave_tm_ wave_tm;

extern s16 ping_test(s32 abort_flag);
extern s16 http_test(s32 abort_flag);
extern s16 udp_echo_test(s32 abort_flag);
extern s16 tcp_echo_test1(s32 abort_flag);
extern s16 tcp_echo_test2(s32 abort_flag);
extern s16 tcp_echo_test3(s32 abort_flag);
extern s16 tcp_chargen_recv_test(s32 abort_flag);
extern s16 tcp_discard_send_test(s32 abort_flag);
extern s16 tcp_discard_server_test(s32 abort_flag);
extern s16 dns_get_name_test(s32 abort_flag);
extern s16 show_ifinfo(s32 abort_flag);
extern s16 show_route(s32 abort_flag);
extern s16 dns_get_addr_proc(const char* in_name, AT_IP_ADDR* out_addr, s32 abort_flag);
extern s16 dns_get_name_proc(AT_IP_ADDR* in_addr, AT_SBYTE* out_name, s32 abort_flag);
extern char* nint2ipdot(AT_NINT32 data);
extern char* mac2str(AT_UBYTE* mac);
extern void print_dhcp_stat(AT_SINT16 stat);
extern void print_ppp_stat(AT_SINT16 stat);
extern void print_ppp_error(AT_SINT16 err);
extern char* tcpstat2str(AT_SINT16 stat);
extern char* uint2timestr(AT_UINT32 in_sec);
extern char* tm2timestr(wave_tm *ptm);
extern wave_tm* wave_gmtime(u32 t);

