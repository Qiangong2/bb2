/*
 * AVE-TCP for Nintendo GameCube Sample Program
 *
 */

#include <stdlib.h>
#include <string.h>
#include <demo.h>

#include "avetcp.h"
#include "atdemo.h"

#define ECHO_PORT 7

#define SEND_F_MAX 64
#define DATA_SIZE 10
#define SEND_INTERVAL 2

static AT_UBYTE sbuf[SEND_F_MAX][DATA_SIZE + 1];
static AT_UBYTE rbuf[DATA_SIZE + 1];
static s16 notify_result[SEND_F_MAX];
static u32 sentlen;
static u32 rcvdlen;
static u32 ack_diff;
static u32 echo_diff;
static u32 send_miss;

static AT_VOID send_f_notify(AT_SINT16 result, AT_SINT16 nh, AT_UBYTE* buf)
{
#pragma unused(nh)
	s16 i;
	u32 sframe;

	for (i = 0; i < SEND_F_MAX; i++) {
		if (sbuf[i] == buf) {
			break;
		}
	}
	if (i >= SEND_F_MAX) {
		OSReport("send_f_notify: unknown buffer\n");
		return;
	}
	notify_result[i] = result;
	*(buf + DATA_SIZE) = '\0';
	sframe = (u32)atoi((const char*)buf);
	ack_diff = gFrame - sframe;
	sentlen += DATA_SIZE;
}

s16 tcp_echo_test3(s32 abort_flag)
{
	static s32 wait = 0;
	static s32 phase = 0;
	static AT_SINT16 nh;
	static AT_IP_ADDR addr;
	AT_SINT16 ret;
	AT_SINT16 stat;
	AT_UINT32 optval;
	AT_SINT16 len;
	AT_datasize_t sendwin;
	AT_datasize_t recvwin;
	AT_UBYTE* buf;
	s16 bufnum;
	s16 i;
	s16 flag;
	s16 x1;
	u32 sframe;
	enum {
		phase_dns = 0,
		phase_tcp_open,
		phase_tcp_estb_wait,
		phase_send_recv,
		phase_tcp_close,
		phase_tcp_close_wait,
		phase_tcp_delete
	};

	flag = 0;
	if (abort_flag) {
		switch (phase) {
		case phase_dns:
			ret = dns_get_addr_proc(AT_NULL, AT_NULL, 1);
			break;
		case phase_tcp_open:
			break;
		case phase_tcp_estb_wait:
			tcp_delete(nh);
			break;
		case phase_send_recv:
		case phase_tcp_close:
		case phase_tcp_close_wait:
			tcp_abort(nh);
			tcp_delete(nh);
			break;
		case phase_tcp_delete:
			tcp_delete(nh);
			break;
		}
		flag = 1;
	} else {
		switch (phase) {
		case phase_dns:
			ret = dns_get_addr_proc(TEST_SERVER_NAME, &addr, 0);
			if (ret == 1) {
				phase = phase_tcp_open;
			} else if (ret == -1) {
				flag = 1;
			}
			break;
	
		case phase_tcp_open:
			ret = tcp_create();
			if (ret < 0) {
				flag = 1;
				break;
			}
			nh = ret;

			optval = 1;
			ret = tcp_set_opt(nh, AT_TCPOT_NONAGLE, &optval);
			if (ret < 0) {
				tcp_delete(nh);
				flag = 1;
				break;
			}

			ret = tcp_connect(nh, &addr, AT_HTONS(ECHO_PORT), AT_NULL);
			if (ret < 0) {
				tcp_delete(nh);
				flag = 1;
				break;
			}
			phase = phase_tcp_estb_wait;
			break;

		case phase_tcp_estb_wait:
			DEMOPrintf(x, y += 16, 0, "TCP SYN_SENT");
			if (wait == 0) {
				ret = tcp_stat(nh, &stat, AT_NULL, AT_NULL, AT_NULL);
				if (ret < 0 || stat == AT_STAT_CLOSED || stat == AT_STAT_FATAL) {
					tcp_delete(nh);
					flag = 1;
				} else if (stat == AT_STAT_ESTABLISHED) {
					bufnum = 0;
					sentlen = 0;
					rcvdlen = 0;
					ack_diff = 0;
					echo_diff = 0;
					send_miss = 0;
					for (i = 0; i < SEND_F_MAX; i++) {
						notify_result[i] = 0;
					}
					phase = phase_send_recv;
				}
				wait = 6;
			} else {
				wait--;
			}
			break;

		case phase_send_recv:
			if (gButton & PAD_BUTTON_A) {
				tcp_abort(nh);
				tcp_delete(nh);
				flag = 1;
				break;
			}

			x1 = x;
			y += 16;
			for (i = 0, bufnum = -1; i < SEND_F_MAX; i++) {
				if (notify_result[i] != AT_API_PENDING) {
					DEMOPrintf(x1, y, 0, "O");
					bufnum = i;
				} else {
					DEMOPrintf(x1, y, 0, ".");
				}
				x1 += 8;
				if (i % 32 == 31) {
					x1 = x;
					y += 16;
				}
			}
			if (wait == 0) {
				if (bufnum > 0) {
					buf = sbuf[bufnum];
					sprintf((char*)buf, "%010d", gFrame);
					len = DATA_SIZE;
					notify_result[bufnum] = AT_API_PENDING;
					ret = tcp_send_f(nh, send_f_notify, len, buf);
					if (ret < 0) {
						tcp_abort(nh);
						tcp_delete(nh);
						flag = 1;
						break;
					}
				} else {
					send_miss++;
				}
				wait = SEND_INTERVAL;
			} else {
				wait--;
			}
			ret = tcp_stat(nh, AT_NULL, AT_NULL, &sendwin, &recvwin);
			if (ret < 0) {
				tcp_abort(nh);
				tcp_delete(nh);
				flag = 1;
				break;
			}
			if (recvwin >= DATA_SIZE) {
				ret = tcp_receive(nh, AT_NULL, DATA_SIZE, rbuf);
				if (ret < 0) {
					phase = phase_tcp_close;
				}
				*(rbuf + DATA_SIZE) = '\0';
				sframe = (u32)atoi((const char*)rbuf);
				echo_diff = gFrame - sframe;
				rcvdlen += ret;
			}
			DEMOPrintf(x, y += 16, 0, "swin %d", sendwin);
			DEMOPrintf(x, y += 16, 0, "rwin %d", recvwin);
			DEMOPrintf(x, y += 16, 0, "sent     %d bytes\n", sentlen);
			DEMOPrintf(x, y += 16, 0, "received %d bytes\n", rcvdlen);
			DEMOPrintf(x, y += 16, 0, "ack diff  %d\n", ack_diff);
			DEMOPrintf(x, y += 16, 0, "echo diff %d\n", echo_diff);
			DEMOPrintf(x, y += 16, 0, "send miss %d\n", send_miss);

			break;
		
		case phase_tcp_close:
			ret = tcp_close(nh);
			if (ret < 0) {
				phase = phase_tcp_delete;
			} else {
				phase = phase_tcp_close_wait;
			}
			break;
	
		case phase_tcp_close_wait:
			if (wait == 0) {
				ret = tcp_stat(nh, &stat, AT_NULL, AT_NULL, AT_NULL);
				if (ret < 0 || stat == AT_STAT_CLOSED || stat == AT_STAT_FATAL) {
					phase = phase_tcp_delete;
				} else if (stat == AT_STAT_TIME_WAIT) {
					flag = 1;
				}
				wait = 6;
			} else {
				wait--;
			}
			break;

		case phase_tcp_delete:
			tcp_delete(nh);
			flag = 1;
			break;
		}
	}
	if (flag == 1) {
		phase = 0;
	}
	return (flag);
}

