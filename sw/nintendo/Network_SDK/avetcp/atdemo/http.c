/*
 * AVE-TCP for Nintendo GameCube Sample Program
 *
 */

#include <stdlib.h>
#include <string.h>
#include <demo.h>

#include "avetcp.h"
#include "atdemo.h"

#define HTTP_PORT 80
#define URL_REQUEST "GET / HTTP/1.0\r\n"
#define USER_AGENT "User-Agent: AveTcp (GAMECUBE)\r\n"
#define CRLF "\r\n"

static s16 gSendNotifyResult;
static AT_VOID send_notify(AT_SINT16 result, AT_SINT16 nh)
{
#pragma unused(nh)
	gSendNotifyResult = result;
}
static s16 gRecvNotifyResult;
static AT_VOID recv_notify(AT_SINT16 result, AT_SINT16 nh)
{
#pragma unused(nh)
	gRecvNotifyResult = result;
}

s16 http_test(s32 abort_flag)
{
	static s32 wait = 0;
	static s32 phase = 0;
	static AT_SINT16 nh;
	static AT_IP_ADDR addr;
	AT_SINT16 ret;
	AT_SINT16 stat;
	AT_SEND_BUFFS buff[3];
	s16 flag;
	enum {
		phase_dns = 0,
		phase_tcp_open,
		phase_tcp_estb_wait,
		phase_url_send,
		phase_url_send_wait,
		phase_contents_recv,
		phase_contents_recv_wait,
		phase_tcp_close,
		phase_tcp_close_wait
	};

	flag = 0;

	if (abort_flag) {
		switch (phase) {
		case phase_dns:
			ret = dns_get_addr_proc(AT_NULL, AT_NULL, 1);
			break;
		case phase_tcp_open:
			break;
		case phase_tcp_estb_wait:
		case phase_url_send:
		case phase_url_send_wait:
		case phase_contents_recv:
		case phase_contents_recv_wait:
		case phase_tcp_close:
		case phase_tcp_close_wait:
			tcp_abort(nh);
			tcp_delete(nh);
			break;
		}
		flag = 1;
	} else {
		switch (phase) {
		case phase_dns:
			ret = dns_get_addr_proc(TEST_SERVER_NAME, &addr, 0);
			if (ret == 1) {
				phase = phase_tcp_open;
			} else if (ret == -1) {
				flag = 1;
			}
			break;

		case phase_tcp_open:
			ret = tcp_create();
			if (ret < 0) {
				flag = 1;
				break;
			}
			nh = ret;

			ret = tcp_connect(nh, &addr, AT_HTONS(HTTP_PORT), AT_NULL);
			if (ret < 0) {
				tcp_delete(nh);
				flag = 1;
				break;
			}
			phase = phase_tcp_estb_wait;
			break;

		case phase_tcp_estb_wait:
			DEMOPrintf(x, y += 16, 0, "TCP SYN_SENT");	
			if (wait == 0) {
				ret = tcp_stat(nh, &stat, AT_NULL, AT_NULL, AT_NULL);
				if (ret < 0 || stat == AT_STAT_FATAL) {
					tcp_delete(nh);
					flag = 1;
				} else if (stat == AT_STAT_ESTABLISHED) {
					phase = phase_url_send;
				}
				wait = 6;
			} else {
				wait--;
			}
			break;

		case phase_url_send:
			buff[2].buff = (AT_UBYTE*)URL_REQUEST;
			buff[2].len = (AT_SINT16)strlen(URL_REQUEST);
			buff[1].buff = (AT_UBYTE*)USER_AGENT;
			buff[1].len = (AT_SINT16)strlen(USER_AGENT);
			buff[0].buff = (AT_UBYTE*)CRLF;
			buff[0].len = (AT_SINT16)strlen(CRLF);
			gSendNotifyResult = AT_API_PENDING;
			ret = tcp_send(nh, send_notify, 3, buff);
			if (ret == AT_API_PENDING) {
				phase = phase_url_send_wait;
				break;
			}
			if (ret < 0) {
				tcp_abort(nh);
				tcp_delete(nh);
				flag = 1;
				break;
			}
			break;

		case phase_url_send_wait:
			if (gSendNotifyResult != AT_API_PENDING) {
				if (gSendNotifyResult == 0) {
					phase = phase_contents_recv;
				} else {
					tcp_abort(nh);
					tcp_delete(nh);
					flag = 1;
				}
			}
			break;

		case phase_contents_recv:
			memset(recv_buf, 0x00, RECV_BUF_SIZE);
			gRecvNotifyResult = AT_API_PENDING;
			ret = tcp_receive(nh, recv_notify, RECV_BUF_SIZE, recv_buf);
			if (ret == AT_API_PENDING) {
				phase = phase_contents_recv_wait;
			} else if (ret < 0) {
				phase = phase_tcp_close;
			}
			break;

		case phase_contents_recv_wait:
			if (gRecvNotifyResult == AT_API_PENDING) {
				;
			} else if (gRecvNotifyResult >= 0) {
				OSReport("%s", recv_buf);
				phase = phase_contents_recv;
			} else if (gRecvNotifyResult < 0) {
				phase = phase_tcp_close;
			}
			break;

		case phase_tcp_close:
			ret = tcp_close(nh);
			phase = phase_tcp_close_wait;
			break;
	
		case phase_tcp_close_wait:
			if (wait == 0) {
				ret = tcp_stat(nh, &stat, AT_NULL, AT_NULL, AT_NULL);
				if (ret < 0 || stat == AT_STAT_CLOSED || stat == AT_STAT_FATAL) {
					tcp_delete(nh);
					flag = 1;
				} else if (stat == AT_STAT_TIME_WAIT) {
					flag = 1;
				}
				wait = 6;
			} else {
				wait--;
			}
			break;
		}
	}
	if (flag == 1) {
		phase = 0;
	}
	return (flag);
}

