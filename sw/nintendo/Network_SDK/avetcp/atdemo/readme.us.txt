AVE-TCP for Nintendo GameCube
Sample Program

1. Build
(1) Set the communication environment
Edit definitions at the beginning of atdemo.h.

(2) Build
Link avetcp.a, mdm.a, and eth.a.

2. Execute
Execute it after connecting the communication device (modem 
adapter or broadband adapter).
If a broadband adapter is connected, "Select screen for connection 
method" is displayed on the TV screen. Select using the up and down 
controls of the +Control Pad and confirm using the A button.
"Select screen for test" is displayed after the interface is up.  
Select using the up and down controls of the +Control Pad and 
confirm using the A button.


