/*
 * AVE-TCP for Nintendo GameCube Sample Program
 *
 */

#include <stdlib.h>
#include <string.h>
#include <demo.h>

#include "avetcp.h"
#include "atdemo.h"

s16 show_route(s32 abort_flag)
{
	s32 i;
	s16 x1;

	DEMOPrintf(x, y += 16, 0, "Routes:");
	x1 = x;
	y += 16;
	DEMOPrintf(x1,           y, 0, "destination");
	DEMOPrintf(x1 += 17 * 8, y, 0, "netmask");
	DEMOPrintf(x1 += 17 * 8, y, 0, "gateway");
	for (i = 0; i < gRouteNum; i++) {
		x1 = x;
		y += 16;
		DEMOPrintf(x1,           y, 0, "%s ", nint2ipdot(gRouteList[i].dst));
		DEMOPrintf(x1 += 17 * 8, y, 0, "%s ", nint2ipdot(gRouteList[i].mask));
		DEMOPrintf(x1 += 17 * 8, y, 0, "%s ", nint2ipdot(gRouteList[i].gw));
	}

	if (gButton & PAD_BUTTON_A) {
		return 1;
	}
	if (abort_flag) {
		return 1;
	}
	return 0;

}

