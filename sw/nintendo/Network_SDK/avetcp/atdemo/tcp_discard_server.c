/*
 * AVE-TCP for Nintendo GameCube Sample Program
 *
 */

#include <stdlib.h>
#include <string.h>
#include <demo.h>

#include "avetcp.h"
#include "atdemo.h"
#define DISCARD_PORT 9

#define TS_STACKSIZE 4096
struct _nhinfo {
	s32 use;
	AT_SINT16 nh;
	OSThread th;
	u8 stack[TS_STACKSIZE];
	u32 received;
};
static struct _nhinfo ts_nhinfo[TCP_PORT_NUM];
static OSMessageQueue ts_notify_message_queue;
static OSMessage ts_notify_message[TCP_PORT_NUM];

static AT_SINT16 ts_listen_nh = -1;
static AT_SINT16 ts_accept_nh = -1;
static int ts_abort = 0;

static void* tcp_discard_server_thread(void* arg)
{
	AT_SINT16 nh;
	AT_SINT16 ret;
	AT_SINT16 stat;
	struct _nhinfo* nhinfo;

	nhinfo = (struct _nhinfo*)arg;
	nh = nhinfo->nh;
	nhinfo->received = 0;

	for (;!ts_abort;) {
		ret = tcp_stat(nh, &stat, AT_NULL, AT_NULL, AT_NULL);
		if (ret < 0) {
			goto tcp_discard_server_abort;
		}
		if (stat == AT_STAT_ESTABLISHED) {
			break;
		}
		VIWaitForRetrace();
	}

	for (;!ts_abort;) {
		ret = tcp_receive(nh, AT_NULL, RECV_BUF_SIZE, recv_buf);
		if (ret < 0) {
			OSReport("tcp_receive(%d) ret %d\n", nh, ret);
			break;
		}
		nhinfo->received += ret;
	}

tcp_discard_server_abort:
	ret = tcp_abort(nh);
	OSReport("tcp_abort(%d) ret %d\n", nh, ret);
	ret = tcp_delete(nh);
	OSReport("tcp_delete(%d) ret %d\n", nh, ret);

	return (NULL);
}

static AT_VOID accept_notify(AT_SINT16 result, AT_SINT16 listen_nh, AT_IP_ADDR* his_addr, AT_NINT16 his_port)
{
#pragma unused(listen_nh, his_addr, his_port)
	if (result >= 0) {
		OSReport("new nh %d\n", result);
		OSSendMessage(&ts_notify_message_queue, (OSMessage*)result, OS_MESSAGE_NOBLOCK);
	} else {
		OSReport("accept_notify(): result = %d\n", result);
	}
}

static void stop_server(AT_SINT16 listen_nh)
{
	AT_SINT16 ret;
	AT_SINT16 nh;
	OSMessage msg;
	s16 i;

	ret = tcp_cancel(listen_nh, 4); /* interrupt tcp_accept() */
	OSReport("tcp_cancel(nh = %d, ACCEPT) ret %d\n", listen_nh, ret);
	tcp_delete(listen_nh);
	OSReport("tcp_delete(%d)\n", listen_nh);

	for (;;) {
		if (OSReceiveMessage(&ts_notify_message_queue, &msg, OS_MESSAGE_NOBLOCK) == FALSE) {
			break;
		}
		nh = (AT_SINT16)msg;
		tcp_abort(nh);
		tcp_delete(nh);
	}

	ts_abort = 1;
	for (i = 1; i < TCP_PORT_NUM; i++) {
		if (ts_nhinfo[i].use) {
			ret = tcp_cancel(ts_nhinfo[i].nh, 2); /* interrupt tcp_receive() */
			OSReport("tcp_cancel(nh = %d, RECEIVE) ret %d\n", ts_nhinfo[i].nh, ret);
			ts_nhinfo[i].use = 0;
		}
	}
}

s16 tcp_discard_server_test(s32 abort_flag) 
{
	static int phase = 0;
	static int wait = 0;
	static AT_SINT16 listen_nh;
	AT_IP_ADDR addr;
	AT_SINT16 ret;
	AT_SINT16 stat;
	AT_SINT16 backlog;
	AT_datasize_t sendwin;
	AT_datasize_t recvwin;
	AT_IP_ADDR myaddr;
	AT_NINT16 myport;
	AT_IP_ADDR hisaddr;
	AT_NINT16 hisport;
	OSMessage msg;
	s16 i;
	s16 flag;
	s16 err = 0;
	enum {
		phase_listen = 0,
		phase_accept_wait,
		phase_end
	};

	flag = 0;
	if (abort_flag) {
		switch (phase) {
		case phase_listen:
			break;
		case phase_accept_wait:
			stop_server(listen_nh);
			break;
		case phase_end:
			break;
		}
		flag = 1;
	} else {
		switch (phase) {
		case phase_listen:
			for (i = 0; i < TCP_PORT_NUM; i++) {
				ts_nhinfo[i].use = 0;
			}
			ts_abort = 0;
			ret = tcp_create();
			if (ret < 0) {
				flag = 1;
				break;
			}
			listen_nh = ret;
			ts_nhinfo[0].use = 1;
			ts_nhinfo[0].nh = ret;
			addr.type = gIfinfo.type;
			addr.v4addr = gIfinfo.v4address;
			ret = tcp_bind(listen_nh, &addr, AT_HTONS(DISCARD_PORT));
			if (ret < 0) {
				tcp_delete(listen_nh);
				flag = 1;
				break;
			}
			ret = tcp_listen(listen_nh, AT_NULL, 0, 1, AT_NULL, AT_NULL);
			if (ret < 0) {
				tcp_delete(listen_nh);
				flag = 1;
				break;
			}
			OSInitMessageQueue(&ts_notify_message_queue, ts_notify_message, sizeof(ts_notify_message) / sizeof(ts_notify_message[0]));
			ret = tcp_accept(listen_nh, accept_notify, AT_NULL);
			if ((ret != AT_API_PENDING) && (ret < 0)) {
				tcp_delete(listen_nh);
				flag = 1;
				break;
			}
			phase = phase_accept_wait;
			break;

		case phase_accept_wait:
			if (OSReceiveMessage(&ts_notify_message_queue, &msg, OS_MESSAGE_NOBLOCK) == TRUE) {
				for (i = 0; i < TCP_PORT_NUM; i++) {
					if (!ts_nhinfo[i].use) {
						ts_nhinfo[i].use = 1;
						ts_nhinfo[i].nh = (AT_SINT16)msg;
						OSCreateThread(&ts_nhinfo[i].th, tcp_discard_server_thread, (void*)&ts_nhinfo[i], 
								ts_nhinfo[i].stack + TS_STACKSIZE, TS_STACKSIZE, 16, OS_THREAD_ATTR_DETACH);
						OSResumeThread(&ts_nhinfo[i].th);

						ret = tcp_accept(listen_nh, accept_notify, AT_NULL);
						if ((ret != AT_API_PENDING) && (ret < 0)) {
							err = 1;
						}
						break;
					}
				}
			}

			DEMOPrintf(x, y += 16, 0, "nh       status            addr  port received");
			for (i = 0; i < TCP_PORT_NUM; i++) {
				if (ts_nhinfo[i].use) {
					ret = tcp_stat(ts_nhinfo[i].nh, &stat, &backlog, &sendwin, &recvwin);
					if (ret < 0) {
						ts_nhinfo[i].use = 0;
						continue;
					}
					ret = tcp_get_addr(ts_nhinfo[i].nh, &myaddr, &myport, &hisaddr, &hisport);
					DEMOPrintf(x, y += 16, 0, "%2d %12s %15s %5d %u",
							ts_nhinfo[i].nh,
							tcpstat2str(stat), nint2ipdot(hisaddr.v4addr), AT_NTOHS(hisport),
							ts_nhinfo[i].received);
				}
			}

			DEMOPrintf(x, y += 16, 0, "PUSH A Button ==> stop");
			if (gButton & PAD_BUTTON_A || err) {
				stop_server(listen_nh);
				phase = phase_end;
			}
			break;

		case phase_end:
			flag = 1;
			break;
		}
	}
	if (flag) {
		phase = 0;
		wait = 0;
	}
	return (flag);
}

