/*
 * AVE-TCP for Nintendo GameCube Sample Program
 *
 */

#include <stdlib.h>
#include <string.h>
#include <demo.h>

#include "avetcp.h"
#include "atdemo.h"

#define ECHO_PORT 7
static AT_SINT16 tcp_send_result;
static AT_VOID tcp_send_notify(AT_SINT16 result, AT_SINT16 nh)
{
#pragma unused(nh)
	tcp_send_result = result;
}

static AT_SINT16 tcp_receive_result;
static AT_VOID tcp_receive_notify(AT_SINT16 result, AT_SINT16 nh)
{
#pragma unused(nh)
	tcp_receive_result = result;
}

s16 tcp_echo_test1(s32 abort_flag)
{
	static s32 wait = 0;
	static s32 phase = 0;
	static AT_SINT16 nh;
	static AT_IP_ADDR addr;
	AT_SINT16 ret;
	AT_SINT16 stat;
	AT_SEND_BUFFS buff;
	s16 i;
	s16 flag;
	static AT_UBYTE ch;
	static AT_UINT32 sentsize;
	static AT_UINT32 rcvdsize;
	enum {
		phase_dns = 0,
		phase_tcp_open,
		phase_tcp_estb_wait,
		phase_send_recv,
		phase_tcp_close,
		phase_tcp_close_wait,
		phase_tcp_delete
	};

	flag = 0;
	if (abort_flag) {
		switch (phase) {
		case phase_dns:
			ret = dns_get_addr_proc(AT_NULL, AT_NULL, 1);
			break;
		case phase_tcp_open:
			break;
		case phase_tcp_estb_wait:
			tcp_delete(nh);
			break;
		case phase_send_recv:
		case phase_tcp_close:
		case phase_tcp_close_wait:
			tcp_abort(nh);
			tcp_delete(nh);
			break;
		case phase_tcp_delete:
			tcp_delete(nh);
			break;
		}
		flag = 1;
	} else {
		switch (phase) {
		case phase_dns:
			ret = dns_get_addr_proc(TEST_SERVER_NAME, &addr, 0);
			if (ret == 1) {
				phase = phase_tcp_open;
			} else if (ret == -1) {
				flag = 1;
			}
			break;
		
		case phase_tcp_open:
			ret = tcp_create();
			if (ret < 0) {
				flag = 1;
				break;
			}
			nh = ret;

			ret = tcp_connect(nh, &addr, AT_HTONS(ECHO_PORT), AT_NULL);
			if (ret < 0) {
				tcp_delete(nh);
				flag = 1;
				break;
			}
			phase = phase_tcp_estb_wait;
			break;

		case phase_tcp_estb_wait:
			DEMOPrintf(x, y += 16, 0, "TCP SYN_SENT");
			if (wait == 0) {
				ret = tcp_stat(nh, &stat, AT_NULL, AT_NULL, AT_NULL);
				if (ret < 0 || stat == AT_STAT_CLOSED || stat == AT_STAT_FATAL) {
					phase = phase_tcp_close;
				} else if (stat == AT_STAT_ESTABLISHED) {
					ch = 'A';
					sentsize = 0;
					rcvdsize = 0;
					tcp_send_result = 0;
					tcp_receive_result = 0;
					phase = phase_send_recv;
				}
				wait = 6;
			} else {
				wait--;
			}
			break;

		case phase_send_recv:
			DEMOPrintf(x, y += 16, 0, "peer %s (%s)", TEST_SERVER_NAME, nint2ipdot(addr.v4addr));
			if (gButton & PAD_BUTTON_A) {
				tcp_abort(nh);
				tcp_delete(nh);
				flag = 1;
				break;
			}

			if (tcp_send_result != AT_API_PENDING) {
				if (tcp_send_result == 0) {
					buff.len = SEND_BUF_SIZE;
					buff.buff = send_buf;
					for (i = 0; i < buff.len; i++) {
						*(buff.buff + i) = ch++;
						if (ch > 'Z') ch = 'A';
					}
					tcp_send_result = AT_API_PENDING;
					ret = tcp_send(nh, tcp_send_notify, 1, &buff);
					sentsize += buff.len;
				} else {
					phase = phase_tcp_close;
					break;
				}
			}
			if (tcp_receive_result != AT_API_PENDING) {
				if (tcp_receive_result >= 0) {
					rcvdsize += tcp_receive_result;
					tcp_receive_result = AT_API_PENDING;
					ret = tcp_receive(nh, tcp_receive_notify, RECV_BUF_SIZE, recv_buf);
				} else {
					phase = phase_tcp_close;
					break;
				}
			}
			DEMOPrintf(x, y += 16, 0, "sent     %d bytes", sentsize);
			DEMOPrintf(x, y += 16, 0, "received %d bytes", rcvdsize);
			DEMOPrintf(x, y += 16, 0, "PUSH A Button ==> stop");
			break;
		
		case phase_tcp_close:
			ret = tcp_close(nh);
			if (ret < 0) {
				phase = phase_tcp_delete;
			} else {
				phase = phase_tcp_close_wait;
			}
			break;
	
		case phase_tcp_close_wait:
			if (wait == 0) {
				ret = tcp_stat(nh, &stat, AT_NULL, AT_NULL, AT_NULL);
				if (ret < 0 || stat == AT_STAT_CLOSED || stat == AT_STAT_FATAL) {
					phase = phase_tcp_delete;
				} else if (stat == AT_STAT_TIME_WAIT) {
					flag = 1;
				}
				wait = 6;
			} else {
				wait--;
			}
			break;

		case phase_tcp_delete:
			tcp_delete(nh);
			flag = 1;
			break;
		}
	}

	if (flag == 1) {
		phase = 0;
	}
	return (flag);

}

