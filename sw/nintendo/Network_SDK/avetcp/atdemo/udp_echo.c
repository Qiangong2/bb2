/*
 * AVE-TCP for Nintendo GameCube Sample Program
 *
 */

#include <stdlib.h>
#include <string.h>
#include <demo.h>

#include "avetcp.h"
#include "atdemo.h"
/* 
 * UDP ECHO TEST
 */

#define UDP_ECHO_PORT 7
static u32 udp_echo_sent;
static u32 udp_echo_received;
static AT_SINT16 udp_receiver(AT_SINT16 func, AT_UDP_PRM *param)
{
	if (func == 0) {
		if (param->recvp.len > RECV_BUF_SIZE) {
			return 1;
		}
		param->recvp.recvbuf = recv_buf;
	} else {
		udp_echo_received++;
	}
	return 0;
}

s16 udp_echo_test(s32 abort_flag)
{
	static s32 wait = 0;
	static s32 phase = 0;
	static AT_SINT16 nh;
	static AT_IP_ADDR dst_addr;
	AT_IP_ADDR my_addr;
	AT_SINT16 ret;
	AT_UINT32 data1;
	AT_UINT32 data2;
	AT_UINT32 data3;
	AT_SEND_BUFFS buff[3];
	s16 flag;
	enum {
		phase_dns = 0,
		phase_udp_open,
		phase_udp_send,
		phase_udp_close
	};

	flag = 0;
	DEMOPrintf(x, y += 16, 0, "UDP ECHO ==> %s", TEST_SERVER_NAME);
	DEMOPrintf(x, y += 16, 0, "SENT     %d packets", udp_echo_sent);
	DEMOPrintf(x, y += 16, 0, "RECEIVED %d packets", udp_echo_received);

	if (abort_flag) {
		switch (phase) {
		case phase_dns:
			ret = dns_get_addr_proc(AT_NULL, AT_NULL, 1);
			break;
		case phase_udp_open:
			break;
		case phase_udp_send:
		case phase_udp_close:
			ret = udp_close(nh);
			break;
		}
		flag = 1;
	} else {
		switch (phase) {
		case phase_dns:
			ret = dns_get_addr_proc(TEST_SERVER_NAME, &dst_addr, 0);
			if (ret == 1) {
				phase = phase_udp_open;
			} else if (ret == -1) {
				flag = 1;
			}
			break;

		case phase_udp_open:
			my_addr.type = gIfinfo.type;
			my_addr.v4addr = gIfinfo.v4address;
			ret = udp_open(&my_addr, 0, &dst_addr, AT_HTONS(UDP_ECHO_PORT),
					AT_NULL, udp_receiver);
			if (ret < 0) {
				flag = 1;
				break;
			}
			nh = ret;
			phase = phase_udp_send;
			break;

		case phase_udp_send:
			if (gButton &PAD_BUTTON_A) {
				phase = phase_udp_close;
				break;
			}
			if (wait == 0) {
				udp_echo_sent++;
				data1 = 0xAAAAAAAA;
				data2 = 0xBBBBBBBB;
				data3 = 0xCCCCCCCC;
				buff[0].buff = (AT_UBYTE*)&data1;
				buff[0].len = sizeof(data1);
				buff[1].buff = (AT_UBYTE*)&data2;
				buff[1].len = sizeof(data2);
				buff[2].buff = (AT_UBYTE*)&data3;
				buff[2].len = sizeof(data3);
				ret = udp_send(nh, AT_NULL, AT_NULL, 0, AT_NULL, 3, buff);
				if (ret < 0) {
					udp_close(nh);
					flag = 1;
					break;
				}
				wait = 60;
			} else {
				wait--;
			}
			break;

		case phase_udp_close:
			ret = udp_close(nh);
			flag = 1;
			break;
		}
	}

	if (flag == 1) {
		phase = 0;
		udp_echo_sent = 0;
		udp_echo_received = 0;
	}
	return (flag);
}
